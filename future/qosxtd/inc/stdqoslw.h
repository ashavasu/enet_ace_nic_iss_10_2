/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: stdqoslw.h,v 1.3 2011/10/25 10:04:11 siva Exp $
*
* Description: Proto types for Low Level  Routines
*********************************************************************/

/* Proto Validate Index Instance for DiffServDataPathTable. */
INT1
nmhValidateIndexInstanceDiffServDataPathTable ARG_LIST((INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for DiffServDataPathTable  */

INT1
nmhGetFirstIndexDiffServDataPathTable ARG_LIST((INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexDiffServDataPathTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDiffServDataPathStart ARG_LIST((INT4  , INT4 ,tSNMP_OID_TYPE * ));

INT1
nmhGetDiffServDataPathStorage ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetDiffServDataPathStatus ARG_LIST((INT4  , INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetDiffServDataPathStart ARG_LIST((INT4  , INT4  ,tSNMP_OID_TYPE *));

INT1
nmhSetDiffServDataPathStorage ARG_LIST((INT4  , INT4  ,INT4 ));

INT1
nmhSetDiffServDataPathStatus ARG_LIST((INT4  , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2DiffServDataPathStart ARG_LIST((UINT4 *  ,INT4  , INT4  ,tSNMP_OID_TYPE *));

INT1
nmhTestv2DiffServDataPathStorage ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

INT1
nmhTestv2DiffServDataPathStatus ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2DiffServDataPathTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDiffServClfrNextFree ARG_LIST((UINT4 *));

/* Proto Validate Index Instance for DiffServClfrTable. */
INT1
nmhValidateIndexInstanceDiffServClfrTable ARG_LIST((UINT4 ));

/* Proto Type for Low Level GET FIRST fn for DiffServClfrTable  */

INT1
nmhGetFirstIndexDiffServClfrTable ARG_LIST((UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexDiffServClfrTable ARG_LIST((UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDiffServClfrStorage ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetDiffServClfrStatus ARG_LIST((UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetDiffServClfrStorage ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetDiffServClfrStatus ARG_LIST((UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2DiffServClfrStorage ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2DiffServClfrStatus ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2DiffServClfrTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDiffServClfrElementNextFree ARG_LIST((UINT4 *));

/* Proto Validate Index Instance for DiffServClfrElementTable. */
INT1
nmhValidateIndexInstanceDiffServClfrElementTable ARG_LIST((UINT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for DiffServClfrElementTable  */

INT1
nmhGetFirstIndexDiffServClfrElementTable ARG_LIST((UINT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexDiffServClfrElementTable ARG_LIST((UINT4 , UINT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDiffServClfrElementPrecedence ARG_LIST((UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetDiffServClfrElementNext ARG_LIST((UINT4  , UINT4 ,tSNMP_OID_TYPE * ));

INT1
nmhGetDiffServClfrElementSpecific ARG_LIST((UINT4  , UINT4 ,tSNMP_OID_TYPE * ));

INT1
nmhGetDiffServClfrElementStorage ARG_LIST((UINT4  , UINT4 ,INT4 *));

INT1
nmhGetDiffServClfrElementStatus ARG_LIST((UINT4  , UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetDiffServClfrElementPrecedence ARG_LIST((UINT4  , UINT4  ,UINT4 ));

INT1
nmhSetDiffServClfrElementNext ARG_LIST((UINT4  , UINT4  ,tSNMP_OID_TYPE *));

INT1
nmhSetDiffServClfrElementSpecific ARG_LIST((UINT4  , UINT4  ,tSNMP_OID_TYPE *));

INT1
nmhSetDiffServClfrElementStorage ARG_LIST((UINT4  , UINT4  ,INT4 ));

INT1
nmhSetDiffServClfrElementStatus ARG_LIST((UINT4  , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2DiffServClfrElementPrecedence ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2DiffServClfrElementNext ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,tSNMP_OID_TYPE *));

INT1
nmhTestv2DiffServClfrElementSpecific ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,tSNMP_OID_TYPE *));

INT1
nmhTestv2DiffServClfrElementStorage ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2DiffServClfrElementStatus ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2DiffServClfrElementTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDiffServMultiFieldClfrNextFree ARG_LIST((UINT4 *));

/* Proto Validate Index Instance for DiffServMultiFieldClfrTable. */
INT1
nmhValidateIndexInstanceDiffServMultiFieldClfrTable ARG_LIST((UINT4 ));

/* Proto Type for Low Level GET FIRST fn for DiffServMultiFieldClfrTable  */

INT1
nmhGetFirstIndexDiffServMultiFieldClfrTable ARG_LIST((UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexDiffServMultiFieldClfrTable ARG_LIST((UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDiffServMultiFieldClfrAddrType ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetDiffServMultiFieldClfrDstAddr ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetDiffServMultiFieldClfrDstPrefixLength ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDiffServMultiFieldClfrSrcAddr ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetDiffServMultiFieldClfrSrcPrefixLength ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDiffServMultiFieldClfrDscp ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetDiffServMultiFieldClfrFlowId ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDiffServMultiFieldClfrProtocol ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDiffServMultiFieldClfrDstL4PortMin ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDiffServMultiFieldClfrDstL4PortMax ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDiffServMultiFieldClfrSrcL4PortMin ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDiffServMultiFieldClfrSrcL4PortMax ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDiffServMultiFieldClfrStorage ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetDiffServMultiFieldClfrStatus ARG_LIST((UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetDiffServMultiFieldClfrAddrType ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetDiffServMultiFieldClfrDstAddr ARG_LIST((UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetDiffServMultiFieldClfrDstPrefixLength ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetDiffServMultiFieldClfrSrcAddr ARG_LIST((UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetDiffServMultiFieldClfrSrcPrefixLength ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetDiffServMultiFieldClfrDscp ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetDiffServMultiFieldClfrFlowId ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetDiffServMultiFieldClfrProtocol ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetDiffServMultiFieldClfrDstL4PortMin ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetDiffServMultiFieldClfrDstL4PortMax ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetDiffServMultiFieldClfrSrcL4PortMin ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetDiffServMultiFieldClfrSrcL4PortMax ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetDiffServMultiFieldClfrStorage ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetDiffServMultiFieldClfrStatus ARG_LIST((UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2DiffServMultiFieldClfrAddrType ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2DiffServMultiFieldClfrDstAddr ARG_LIST((UINT4 *  ,UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2DiffServMultiFieldClfrDstPrefixLength ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2DiffServMultiFieldClfrSrcAddr ARG_LIST((UINT4 *  ,UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2DiffServMultiFieldClfrSrcPrefixLength ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2DiffServMultiFieldClfrDscp ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2DiffServMultiFieldClfrFlowId ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2DiffServMultiFieldClfrProtocol ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2DiffServMultiFieldClfrDstL4PortMin ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2DiffServMultiFieldClfrDstL4PortMax ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2DiffServMultiFieldClfrSrcL4PortMin ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2DiffServMultiFieldClfrSrcL4PortMax ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2DiffServMultiFieldClfrStorage ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2DiffServMultiFieldClfrStatus ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2DiffServMultiFieldClfrTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDiffServMeterNextFree ARG_LIST((UINT4 *));

/* Proto Validate Index Instance for DiffServMeterTable. */
INT1
nmhValidateIndexInstanceDiffServMeterTable ARG_LIST((UINT4 ));

/* Proto Type for Low Level GET FIRST fn for DiffServMeterTable  */

INT1
nmhGetFirstIndexDiffServMeterTable ARG_LIST((UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexDiffServMeterTable ARG_LIST((UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDiffServMeterSucceedNext ARG_LIST((UINT4 ,tSNMP_OID_TYPE * ));

INT1
nmhGetDiffServMeterFailNext ARG_LIST((UINT4 ,tSNMP_OID_TYPE * ));

INT1
nmhGetDiffServMeterSpecific ARG_LIST((UINT4 ,tSNMP_OID_TYPE * ));

INT1
nmhGetDiffServMeterStorage ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetDiffServMeterStatus ARG_LIST((UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetDiffServMeterSucceedNext ARG_LIST((UINT4  ,tSNMP_OID_TYPE *));

INT1
nmhSetDiffServMeterFailNext ARG_LIST((UINT4  ,tSNMP_OID_TYPE *));

INT1
nmhSetDiffServMeterSpecific ARG_LIST((UINT4  ,tSNMP_OID_TYPE *));

INT1
nmhSetDiffServMeterStorage ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetDiffServMeterStatus ARG_LIST((UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2DiffServMeterSucceedNext ARG_LIST((UINT4 *  ,UINT4  ,tSNMP_OID_TYPE *));

INT1
nmhTestv2DiffServMeterFailNext ARG_LIST((UINT4 *  ,UINT4  ,tSNMP_OID_TYPE *));

INT1
nmhTestv2DiffServMeterSpecific ARG_LIST((UINT4 *  ,UINT4  ,tSNMP_OID_TYPE *));

INT1
nmhTestv2DiffServMeterStorage ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2DiffServMeterStatus ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2DiffServMeterTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDiffServTBParamNextFree ARG_LIST((UINT4 *));

/* Proto Validate Index Instance for DiffServTBParamTable. */
INT1
nmhValidateIndexInstanceDiffServTBParamTable ARG_LIST((UINT4 ));

/* Proto Type for Low Level GET FIRST fn for DiffServTBParamTable  */

INT1
nmhGetFirstIndexDiffServTBParamTable ARG_LIST((UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexDiffServTBParamTable ARG_LIST((UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDiffServTBParamType ARG_LIST((UINT4 ,tSNMP_OID_TYPE * ));

INT1
nmhGetDiffServTBParamRate ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDiffServTBParamBurstSize ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetDiffServTBParamInterval ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDiffServTBParamStorage ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetDiffServTBParamStatus ARG_LIST((UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetDiffServTBParamType ARG_LIST((UINT4  ,tSNMP_OID_TYPE *));

INT1
nmhSetDiffServTBParamRate ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetDiffServTBParamBurstSize ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetDiffServTBParamInterval ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetDiffServTBParamStorage ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetDiffServTBParamStatus ARG_LIST((UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2DiffServTBParamType ARG_LIST((UINT4 *  ,UINT4  ,tSNMP_OID_TYPE *));

INT1
nmhTestv2DiffServTBParamRate ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2DiffServTBParamBurstSize ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2DiffServTBParamInterval ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2DiffServTBParamStorage ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2DiffServTBParamStatus ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2DiffServTBParamTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDiffServActionNextFree ARG_LIST((UINT4 *));

/* Proto Validate Index Instance for DiffServActionTable. */
INT1
nmhValidateIndexInstanceDiffServActionTable ARG_LIST((UINT4 ));

/* Proto Type for Low Level GET FIRST fn for DiffServActionTable  */

INT1
nmhGetFirstIndexDiffServActionTable ARG_LIST((UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexDiffServActionTable ARG_LIST((UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDiffServActionInterface ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetDiffServActionNext ARG_LIST((UINT4 ,tSNMP_OID_TYPE * ));

INT1
nmhGetDiffServActionSpecific ARG_LIST((UINT4 ,tSNMP_OID_TYPE * ));

INT1
nmhGetDiffServActionStorage ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetDiffServActionStatus ARG_LIST((UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetDiffServActionInterface ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetDiffServActionNext ARG_LIST((UINT4  ,tSNMP_OID_TYPE *));

INT1
nmhSetDiffServActionSpecific ARG_LIST((UINT4  ,tSNMP_OID_TYPE *));

INT1
nmhSetDiffServActionStorage ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetDiffServActionStatus ARG_LIST((UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2DiffServActionInterface ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2DiffServActionNext ARG_LIST((UINT4 *  ,UINT4  ,tSNMP_OID_TYPE *));

INT1
nmhTestv2DiffServActionSpecific ARG_LIST((UINT4 *  ,UINT4  ,tSNMP_OID_TYPE *));

INT1
nmhTestv2DiffServActionStorage ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2DiffServActionStatus ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2DiffServActionTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for DiffServDscpMarkActTable. */
INT1
nmhValidateIndexInstanceDiffServDscpMarkActTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for DiffServDscpMarkActTable  */

INT1
nmhGetFirstIndexDiffServDscpMarkActTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexDiffServDscpMarkActTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDiffServCountActNextFree ARG_LIST((UINT4 *));

/* Proto Validate Index Instance for DiffServCountActTable. */
INT1
nmhValidateIndexInstanceDiffServCountActTable ARG_LIST((UINT4 ));

/* Proto Type for Low Level GET FIRST fn for DiffServCountActTable  */

INT1
nmhGetFirstIndexDiffServCountActTable ARG_LIST((UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexDiffServCountActTable ARG_LIST((UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDiffServCountActOctets ARG_LIST((UINT4 ,tSNMP_COUNTER64_TYPE *));

INT1
nmhGetDiffServCountActPkts ARG_LIST((UINT4 ,tSNMP_COUNTER64_TYPE *));

INT1
nmhGetDiffServCountActStorage ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetDiffServCountActStatus ARG_LIST((UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetDiffServCountActStorage ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetDiffServCountActStatus ARG_LIST((UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2DiffServCountActStorage ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2DiffServCountActStatus ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2DiffServCountActTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDiffServAlgDropNextFree ARG_LIST((UINT4 *));

/* Proto Validate Index Instance for DiffServAlgDropTable. */
INT1
nmhValidateIndexInstanceDiffServAlgDropTable ARG_LIST((UINT4 ));

/* Proto Type for Low Level GET FIRST fn for DiffServAlgDropTable  */

INT1
nmhGetFirstIndexDiffServAlgDropTable ARG_LIST((UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexDiffServAlgDropTable ARG_LIST((UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDiffServAlgDropType ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetDiffServAlgDropNext ARG_LIST((UINT4 ,tSNMP_OID_TYPE * ));

INT1
nmhGetDiffServAlgDropQMeasure ARG_LIST((UINT4 ,tSNMP_OID_TYPE * ));

INT1
nmhGetDiffServAlgDropQThreshold ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDiffServAlgDropSpecific ARG_LIST((UINT4 ,tSNMP_OID_TYPE * ));

INT1
nmhGetDiffServAlgDropOctets ARG_LIST((UINT4 ,tSNMP_COUNTER64_TYPE *));

INT1
nmhGetDiffServAlgDropPkts ARG_LIST((UINT4 ,tSNMP_COUNTER64_TYPE *));

INT1
nmhGetDiffServAlgRandomDropOctets ARG_LIST((UINT4 ,tSNMP_COUNTER64_TYPE *));

INT1
nmhGetDiffServAlgRandomDropPkts ARG_LIST((UINT4 ,tSNMP_COUNTER64_TYPE *));

INT1
nmhGetDiffServAlgDropStorage ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetDiffServAlgDropStatus ARG_LIST((UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetDiffServAlgDropType ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetDiffServAlgDropNext ARG_LIST((UINT4  ,tSNMP_OID_TYPE *));

INT1
nmhSetDiffServAlgDropQMeasure ARG_LIST((UINT4  ,tSNMP_OID_TYPE *));

INT1
nmhSetDiffServAlgDropQThreshold ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetDiffServAlgDropSpecific ARG_LIST((UINT4  ,tSNMP_OID_TYPE *));

INT1
nmhSetDiffServAlgDropStorage ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetDiffServAlgDropStatus ARG_LIST((UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2DiffServAlgDropType ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2DiffServAlgDropNext ARG_LIST((UINT4 *  ,UINT4  ,tSNMP_OID_TYPE *));

INT1
nmhTestv2DiffServAlgDropQMeasure ARG_LIST((UINT4 *  ,UINT4  ,tSNMP_OID_TYPE *));

INT1
nmhTestv2DiffServAlgDropQThreshold ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2DiffServAlgDropSpecific ARG_LIST((UINT4 *  ,UINT4  ,tSNMP_OID_TYPE *));

INT1
nmhTestv2DiffServAlgDropStorage ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2DiffServAlgDropStatus ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2DiffServAlgDropTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDiffServRandomDropNextFree ARG_LIST((UINT4 *));

/* Proto Validate Index Instance for DiffServRandomDropTable. */
INT1
nmhValidateIndexInstanceDiffServRandomDropTable ARG_LIST((UINT4 ));

/* Proto Type for Low Level GET FIRST fn for DiffServRandomDropTable  */

INT1
nmhGetFirstIndexDiffServRandomDropTable ARG_LIST((UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexDiffServRandomDropTable ARG_LIST((UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDiffServRandomDropMinThreshBytes ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDiffServRandomDropMinThreshPkts ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDiffServRandomDropMaxThreshBytes ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDiffServRandomDropMaxThreshPkts ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDiffServRandomDropProbMax ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDiffServRandomDropWeight ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDiffServRandomDropSamplingRate ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDiffServRandomDropStorage ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetDiffServRandomDropStatus ARG_LIST((UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetDiffServRandomDropMinThreshBytes ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetDiffServRandomDropMinThreshPkts ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetDiffServRandomDropMaxThreshBytes ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetDiffServRandomDropMaxThreshPkts ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetDiffServRandomDropProbMax ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetDiffServRandomDropWeight ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetDiffServRandomDropSamplingRate ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetDiffServRandomDropStorage ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetDiffServRandomDropStatus ARG_LIST((UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2DiffServRandomDropMinThreshBytes ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2DiffServRandomDropMinThreshPkts ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2DiffServRandomDropMaxThreshBytes ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2DiffServRandomDropMaxThreshPkts ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2DiffServRandomDropProbMax ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2DiffServRandomDropWeight ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2DiffServRandomDropSamplingRate ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2DiffServRandomDropStorage ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2DiffServRandomDropStatus ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2DiffServRandomDropTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDiffServQNextFree ARG_LIST((UINT4 *));

/* Proto Validate Index Instance for DiffServQTable. */
INT1
nmhValidateIndexInstanceDiffServQTable ARG_LIST((UINT4 ));

/* Proto Type for Low Level GET FIRST fn for DiffServQTable  */

INT1
nmhGetFirstIndexDiffServQTable ARG_LIST((UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexDiffServQTable ARG_LIST((UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDiffServQNext ARG_LIST((UINT4 ,tSNMP_OID_TYPE * ));

INT1
nmhGetDiffServQMinRate ARG_LIST((UINT4 ,tSNMP_OID_TYPE * ));

INT1
nmhGetDiffServQMaxRate ARG_LIST((UINT4 ,tSNMP_OID_TYPE * ));

INT1
nmhGetDiffServQStorage ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetDiffServQStatus ARG_LIST((UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetDiffServQNext ARG_LIST((UINT4  ,tSNMP_OID_TYPE *));

INT1
nmhSetDiffServQMinRate ARG_LIST((UINT4  ,tSNMP_OID_TYPE *));

INT1
nmhSetDiffServQMaxRate ARG_LIST((UINT4  ,tSNMP_OID_TYPE *));

INT1
nmhSetDiffServQStorage ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetDiffServQStatus ARG_LIST((UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2DiffServQNext ARG_LIST((UINT4 *  ,UINT4  ,tSNMP_OID_TYPE *));

INT1
nmhTestv2DiffServQMinRate ARG_LIST((UINT4 *  ,UINT4  ,tSNMP_OID_TYPE *));

INT1
nmhTestv2DiffServQMaxRate ARG_LIST((UINT4 *  ,UINT4  ,tSNMP_OID_TYPE *));

INT1
nmhTestv2DiffServQStorage ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2DiffServQStatus ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2DiffServQTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDiffServSchedulerNextFree ARG_LIST((UINT4 *));

/* Proto Validate Index Instance for DiffServSchedulerTable. */
INT1
nmhValidateIndexInstanceDiffServSchedulerTable ARG_LIST((UINT4 ));

/* Proto Type for Low Level GET FIRST fn for DiffServSchedulerTable  */

INT1
nmhGetFirstIndexDiffServSchedulerTable ARG_LIST((UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexDiffServSchedulerTable ARG_LIST((UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDiffServSchedulerNext ARG_LIST((UINT4 ,tSNMP_OID_TYPE * ));

INT1
nmhGetDiffServSchedulerMethod ARG_LIST((UINT4 ,tSNMP_OID_TYPE * ));

INT1
nmhGetDiffServSchedulerMinRate ARG_LIST((UINT4 ,tSNMP_OID_TYPE * ));

INT1
nmhGetDiffServSchedulerMaxRate ARG_LIST((UINT4 ,tSNMP_OID_TYPE * ));

INT1
nmhGetDiffServSchedulerStorage ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetDiffServSchedulerStatus ARG_LIST((UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetDiffServSchedulerNext ARG_LIST((UINT4  ,tSNMP_OID_TYPE *));

INT1
nmhSetDiffServSchedulerMethod ARG_LIST((UINT4  ,tSNMP_OID_TYPE *));

INT1
nmhSetDiffServSchedulerMinRate ARG_LIST((UINT4  ,tSNMP_OID_TYPE *));

INT1
nmhSetDiffServSchedulerMaxRate ARG_LIST((UINT4  ,tSNMP_OID_TYPE *));

INT1
nmhSetDiffServSchedulerStorage ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetDiffServSchedulerStatus ARG_LIST((UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2DiffServSchedulerNext ARG_LIST((UINT4 *  ,UINT4  ,tSNMP_OID_TYPE *));

INT1
nmhTestv2DiffServSchedulerMethod ARG_LIST((UINT4 *  ,UINT4  ,tSNMP_OID_TYPE *));

INT1
nmhTestv2DiffServSchedulerMinRate ARG_LIST((UINT4 *  ,UINT4  ,tSNMP_OID_TYPE *));

INT1
nmhTestv2DiffServSchedulerMaxRate ARG_LIST((UINT4 *  ,UINT4  ,tSNMP_OID_TYPE *));

INT1
nmhTestv2DiffServSchedulerStorage ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2DiffServSchedulerStatus ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2DiffServSchedulerTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDiffServMinRateNextFree ARG_LIST((UINT4 *));

/* Proto Validate Index Instance for DiffServMinRateTable. */
INT1
nmhValidateIndexInstanceDiffServMinRateTable ARG_LIST((UINT4 ));

/* Proto Type for Low Level GET FIRST fn for DiffServMinRateTable  */

INT1
nmhGetFirstIndexDiffServMinRateTable ARG_LIST((UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexDiffServMinRateTable ARG_LIST((UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDiffServMinRatePriority ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDiffServMinRateAbsolute ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDiffServMinRateRelative ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetDiffServMinRateStorage ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetDiffServMinRateStatus ARG_LIST((UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetDiffServMinRatePriority ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetDiffServMinRateAbsolute ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetDiffServMinRateRelative ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetDiffServMinRateStorage ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetDiffServMinRateStatus ARG_LIST((UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2DiffServMinRatePriority ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2DiffServMinRateAbsolute ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2DiffServMinRateRelative ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2DiffServMinRateStorage ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2DiffServMinRateStatus ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2DiffServMinRateTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDiffServMaxRateNextFree ARG_LIST((UINT4 *));

/* Proto Validate Index Instance for DiffServMaxRateTable. */
INT1
nmhValidateIndexInstanceDiffServMaxRateTable ARG_LIST((UINT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for DiffServMaxRateTable  */

INT1
nmhGetFirstIndexDiffServMaxRateTable ARG_LIST((UINT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexDiffServMaxRateTable ARG_LIST((UINT4 , UINT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDiffServMaxRateAbsolute ARG_LIST((UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetDiffServMaxRateRelative ARG_LIST((UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetDiffServMaxRateThreshold ARG_LIST((UINT4  , UINT4 ,INT4 *));

INT1
nmhGetDiffServMaxRateStorage ARG_LIST((UINT4  , UINT4 ,INT4 *));

INT1
nmhGetDiffServMaxRateStatus ARG_LIST((UINT4  , UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetDiffServMaxRateAbsolute ARG_LIST((UINT4  , UINT4  ,UINT4 ));

INT1
nmhSetDiffServMaxRateRelative ARG_LIST((UINT4  , UINT4  ,UINT4 ));

INT1
nmhSetDiffServMaxRateThreshold ARG_LIST((UINT4  , UINT4  ,INT4 ));

INT1
nmhSetDiffServMaxRateStorage ARG_LIST((UINT4  , UINT4  ,INT4 ));

INT1
nmhSetDiffServMaxRateStatus ARG_LIST((UINT4  , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2DiffServMaxRateAbsolute ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2DiffServMaxRateRelative ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2DiffServMaxRateThreshold ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2DiffServMaxRateStorage ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2DiffServMaxRateStatus ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2DiffServMaxRateTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Low Level Routine for ACL Functions calls */
extern INT1
nmhGetIssExtL3FilterStatus  ARG_LIST ((INT4 ,INT4 *));

extern INT1
nmhSetIssExtL3FilterStatus  ARG_LIST ((INT4  ,INT4 ));

extern INT1
nmhTestv2IssExtL3FilterStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

extern INT1 nmhValidateIndexInstanceIssExtL3FilterTable ARG_LIST ((INT4));

extern INT1
nmhGetFirstIndexIssExtL3FilterTable ARG_LIST ((INT4 *));

extern INT1
nmhGetNextIndexIssExtL3FilterTable ARG_LIST ((INT4,
                                 INT4 *));

extern INT1
nmhGetIssExtL3FilterSrcIpAddr ARG_LIST ((INT4, UINT4 *));

extern INT1
nmhGetIssExtL3FilterProtocol ARG_LIST ((INT4, INT4 *));

extern INT1
nmhGetIssExtL3FilterDstIpAddr ARG_LIST ((INT4, UINT4 *));

extern INT1
nmhGetIssExtL3FilterDscp ARG_LIST ((INT4, INT4 *));

extern INT1
nmhGetIssExtL3FilterMinDstProtPort ARG_LIST ((INT4, UINT4 *));

extern INT1
nmhGetIssExtL3FilterMaxDstProtPort ARG_LIST ((INT4, UINT4 *));

extern INT1
nmhGetIssExtL3FilterMinSrcProtPort ARG_LIST ((INT4, UINT4 *));

extern INT1
nmhSetIssExtL3FilterDstIpAddr ARG_LIST ((INT4, UINT4 ));

extern INT1 nmhGetIssExtL3FilterMaxSrcProtPort ARG_LIST ((INT4 , UINT4*));

extern INT1
nmhSetIssExtL3FilterSrcIpAddr ARG_LIST((INT4  ,UINT4 ));

extern INT1
nmhSetIssExtL3FilterDscp ARG_LIST((INT4  ,INT4 ));

extern INT1
nmhSetIssExtL3FilterProtocolType ARG_LIST((INT4  ,UINT4 ));

extern INT1
nmhSetIssExtL3FilterMinDstProtPort ARG_LIST((INT4  ,UINT4 ));

extern INT1
nmhSetIssExtL3FilterMaxDstProtPort ARG_LIST((INT4  ,UINT4 ));

extern INT1
nmhSetIssExtL3FilterMinSrcProtPort ARG_LIST((INT4  ,UINT4 ));

extern INT1
nmhSetIssExtL3FilterMaxSrcProtPort ARG_LIST((INT4  ,UINT4 ));

extern INT1
nmhTestv2IssExtL3FilterDstIpAddr ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

extern INT1
nmhTestv2IssExtL3FilterSrcIpAddr ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

extern INT1
nmhTestv2IssExtL3FilterMinDstProtPort ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

extern INT1
nmhTestv2IssExtL3FilterMaxDstProtPort ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

extern INT1
nmhTestv2IssExtL3FilterMinSrcProtPort ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

extern INT1
nmhTestv2IssExtL3FilterMaxSrcProtPort ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

extern INT1
nmhTestv2IssExtL3FilterDscp ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

extern INT1
nmhTestv2IssExtL3FilterDirection ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

extern INT1
nmhTestv2IssExtL3FilterProtocolType ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

extern INT1
nmhSetIssExtL3FilterProtocol ARG_LIST ((INT4 i4IssExtL3FilterNo, INT4 i4Element));


extern INT1
nmhTestv2IssExtL3FilterProtocol ARG_LIST ((UINT4 *pu4Error, 
                      INT4 i4IssExtL3FilterNo,
                             INT4 i4Element));

/*
extern tIssExtL3FilterEntry* IssGetL3FilterEntry ARG_LIST ((INT4));


extern  tIssExtL3FilterEntry* pIssExtL3FilterEntry;
*/
