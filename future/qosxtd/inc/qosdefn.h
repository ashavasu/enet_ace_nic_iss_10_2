/* $Id: qosdefn.h,v 1.33 2016/07/05 07:38:43 siva Exp $ */
/****************************************************************************/
/* Copyright (C) 2007 Aricent Inc . All Rights Reserved                     */
/*                                                                          */
/*  FILE NAME             : qosdefn.h                                       */
/*  PRINCIPAL AUTHOR      : Aricent                                         */
/*  SUBSYSTEM NAME        : QoS                                             */
/*  MODULE NAME           : QoS-DEFS                                        */
/*  LANGUAGE              : C                                               */
/*  TARGET ENVIRONMENT    : Linux                                           */
/*  DATE OF FIRST RELEASE :                                                 */
/*  AUTHOR                : Aricent                                         */
/*  FUNCTIONS DEFINED(Applicable for Source files) :                        */
/*  DESCRIPTION           : This File contain the constants and macros for  */
/*                          the Aricent QoS Module.                         */
/****************************************************************************/
/*                                                                          */
/*  Change History                                                          */
/*  Version               :                                                 */
/*  Date(DD/MM/YYYY)      :                                                 */
/*  Modified by           :                                                 */
/*  Description of change :                                                 */
/****************************************************************************/

#ifndef __QOS_DEFS_H__
#define __QOS_DEFS_H__ 

#ifndef __FUNCTION__
#define __FUNCTION__ "FunName"
#endif

/* Common Macros and Constants */
#define QOS_FILTER_INFO_MAX_ENTRIES    QOS_CLS_MAP_TBL_MAX_ENTRIES
#define QOS_MAX_TABLE_NAME             32
#define QOS_SEM_NAME                   ("QSMS")
#define QOS_INIT_COMPLETE(u4Status)    lrInitComplete(u4Status)
#define QOS_MODULE_SHUTDOW_ERROR_MSG \
                           " QoS Should Be Started Before Accessing It !.\r\n"
#define QOS_FOREVER()                  while(1) 
#define QOS_CFG_TYPE_CREATE            1
#define QOS_CFG_TYPE_DELETE            2
#define QOS_SEND_EVENT                 OsixEvtSend
#define QOS_RECV_EVENT                 OsixEvtRecv
#define QOS_TASK_ID                    gQoSGlobalInfo.QoSTaskId
#define QOS_GET_TASK_ID                OsixTskIdSelf
#define QOS_EVENT_WAIT_FLAGS           OSIX_WAIT                           
/*Maximum Traffic Class*/
#define QOS_MAX_TC_SUPPORT            8
/* Trace Level Macros */
#define QOS_TRC_MIN_LEVEL              0
#define QOS_TRC_MAX_LEVEL              0x000000FF

/* Range Checks for Table Entries */
#define QOS_PRI_MAP_TBL_MAX_INDEX_RANGE          65535     
#define QOS_VLAN_MAP_TBL_MAX_INDEX_RANGE         65535     
#define QOS_CLS_MAP_TBL_MAX_INDEX_RANGE          65535 
/* CLASS Range and ClassToPriority Map Tbl Range are same. */
#define QOS_DEF_CLASS                            1
#define QOS_CLS2PRI_MAP_TBL_MAX_INDEX_RANGE      65535  
#define QOS_METER_TBL_MAX_INDEX_RANGE            65535     
#define QOS_Q_TBL_MAX_INDEX_RANGE                65535     
#define QOS_PLY_MAP_TBL_MAX_INDEX_RANGE          65535     
#define QOS_Q_TEMP_TBL_MAX_INDEX_RANGE           65535
                           
#define QOS_SHAPE_TEMP_TBL_MAX_INDEX_RANGE       65535
#define QOS_SCHED_TBL_MAX_INDEX_RANGE            65535
#define QOS_MINRATE_TBL_MAX_INDEX_RANGE          65535
#define QOS_MAXRATE_TBL_MAX_INDEX_RANGE          65535
#define QOS_Q_TBL_MAX_INDEX_RANGE                65535
#define QOS_Q_MAP_TBL_MAX_INDEX_RANGE            65535
#define QOS_PORT_TBL_MAX_INDEX_RANGE             2112 /*24*[16+8+64]*/
#define QOS_PCP_MAX_ENTRY                        2112
                           
/* QOS_MAX_LENGTH_DROP is used in SNPRINTF where we copy string "Drop." of length 5 */
#define QOS_MAX_LENGTH_DROP   5
/* QOS_MAX_BUF_LENGTH is used in SNPRINTF for dynamic string copy to max buf length*/
#define QOS_MAX_BUF_LENGTH  100                           
#define QOS_CHECK_TABLE_REF_COUNT(pTableEntry) \
        (pTableEntry->u4RefCount != 0) ? QOS_FAILURE : QOS_SUCCESS

/* Should be Null Teriminated pu1Str */
#define QOS_UTL_COPY_TABLE_NAME(pSNMPOctSrt,pu1Str) \
      pSNMPOctSrt->i4_Length = STRLEN (pu1Str); \
      STRNCPY (pSNMPOctSrt->pu1_OctetList, pu1Str,(pSNMPOctSrt->i4_Length))

#define QOS_UTL_SET_TABLE_NAME(pu1Str,pSNMPOctSrt) \
    MEMSET (pu1Str, 0, QOS_MAX_TABLE_NAME); \
    STRNCPY (pu1Str,pSNMPOctSrt->pu1_OctetList,(pSNMPOctSrt->i4_Length))


                           
                           
/*  1. Priority Map Table  */
#define QOS_IN_PRI_TYPE_MIN_VAL               0
#define QOS_IN_PRI_TYPE_MAX_VAL               6


#define QOS_IN_PRIORITY_MIN_VAL               0
#define QOS_IN_PRIORITY_MAX_VAL              63
#define QOS_REGEN_PRIORITY_MIN_VAL            0
#define QOS_REGEN_PRIORITY_MAX_VAL           63
/* Vlan Pri (3) + Drop eligible BIT (1) */
#define QOS_IN_PRIORITY_VLAN_PRI_MIN          0
#define QOS_IN_PRIORITY_VLAN_PRI_MAX          7
/* IP TOS-PRI BIT(3) */
#define QOS_IN_PRIORITY_IP_TOS_MIN            0
#define QOS_IN_PRIORITY_IP_TOS_MAX            7
/* IP DSCP BIT(6) */                           
#define QOS_IN_PRIORITY_IP_DSCP_MIN            0
#define QOS_IN_PRIORITY_IP_DSCP_MAX           63
/* MPLS EXP  BIT(3) */                         
#define QOS_IN_PRIORITY_MPLS_EXP_MIN           0
#define QOS_IN_PRIORITY_MPLS_EXP_MAX           7
/* Vlan Drop eligible BIT (1) */   
#define QOS_IN_PRIORITY_VLAN_DEI_MIN           0
#define QOS_IN_PRIORITY_VLAN_DEI_MAX           1

                                                
#define QOS_REGEN_INNER_PRIORITY_MIN_VAL       0
#define QOS_REGEN_INNER_PRIORITY_MAX_VAL       8
                                                
#define QOS_PRI_MAP_TBL_CONF_STATUS_SYS        1
#define QOS_PRI_MAP_TBL_CONF_STATUS_MGNT       2

#define QOS_PCP_ENTRY_TYPE_IMPLICIT            1
#define QOS_PCP_ENTRY_TYPE_EXPLICIT            2

/*    1. Priority Map Table Index  - Priority Map Id */
#define QOS_CHECK_PRI_MAP_TBL_INDEX_RANGE(PMId) \
          ((PMId > 0) && (PMId <= QOS_PRI_MAP_TBL_MAX_INDEX_RANGE)) ?\
                                  (QOS_SUCCESS) : (QOS_FAILURE)

/*    2. Vlan Queuing Map Table Index  - Vlan Map Id */
#define QOS_CHECK_VLAN_MAP_TBL_INDEX_RANGE(PMId) \
          ((PMId > 1) && (PMId <= QOS_VLAN_MAP_TBL_MAX_INDEX_RANGE)) ?\
                                  (QOS_SUCCESS) : (QOS_FAILURE)

#define QOS_CLASSTBL_VLAN_MAP_DEFAULT_VAL       0

/* 2. Class Map Table  */
#define QOS_CLS2PRI_PRIORITY_MIN_VAL            0
#define QOS_CLS2PRI_PRIORITY_MAX_VAL            7
#define QOS_CLS_PRE_COLOR_MIN_VAL               0
#define QOS_CLS_PRE_COLOR_MAX_VAL               3


/*  Class Map Table Index  - Class Map Id */ 
#define QOS_CHECK_CLS_MAP_TBL_INDEX_RANGE(ClsMId) \
          ((ClsMId > 0) && (ClsMId <= QOS_CLS_MAP_TBL_MAX_INDEX_RANGE)) ?\
                                  (QOS_SUCCESS) : (QOS_FAILURE)

/* 3. Class2Pri Map Table  */
/* Class 2 Priority Table Index  - Class Map Id */
#define QOS_CHECK_CLS2PRI_MAP_TBL_INDEX_RANGE(ClsId) \
          ((ClsId > 0) && (ClsId <= QOS_CLS2PRI_MAP_TBL_MAX_INDEX_RANGE)) ?\
                                  (QOS_SUCCESS) : (QOS_FAILURE)

/*    4. Meter Table  */
#define QOS_METER_INTERVAL_MIN_VAL            0
#define QOS_METER_INTERVAL_MAX_VAL            1000
#define QOS_METER_COLOR_MIN_VAL               1
#define QOS_METER_COLOR_MAX_VAL               2

#define QOS_METER_CIR_MIN_VAL                 0
#define QOS_METER_CIR_MAX_VAL                 10485760
#define QOS_METER_CBS_MIN_VAL                 0
#define QOS_METER_CBS_MAX_VAL                 10485760
#define QOS_METER_EIR_MIN_VAL                 0
#define QOS_METER_EIR_MAX_VAL                 10485760
#define QOS_METER_EBS_MIN_VAL                 0
#define QOS_METER_EBS_MAX_VAL                 10485760

#define QOS_METER_TYPE_MIN_VAL                1
#define QOS_METER_TYPE_MAX_VAL                7

#define QOS_METER_PARAM_TYPE_INTERVAL         1
#define QOS_METER_PARAM_TYPE_CIR              2 
#define QOS_METER_PARAM_TYPE_CBS              3
#define QOS_METER_PARAM_TYPE_EIR              4
#define QOS_METER_PARAM_TYPE_EBS              5


    
 /*  Meter Table Index  - Meter Id */ 
#define QOS_CHECK_METER_TBL_INDEX_RANGE(MId) \
          (((MId <= 0) || (MId > QOS_METER_TBL_MAX_INDEX_RANGE)) ?\
                                (QOS_FAILURE) : (QOS_SUCCESS))

#define QOS_CHECK_Q_TBL_INDEX_RANGE(QId) \
          ((((QId <= 0) || (QId > QOS_Q_TBL_MAX_INDEX_RANGE)) ?\
                                (QOS_FAILURE) : (QOS_SUCCESS)))

#define QOS_CHECK_SCHEDULER_TBL_INDEX_RANGE(SchedulerId) \
          (((SchedulerId <= 0) || (SchedulerId > QOS_SCHEDULER_TBL_MAX_INDEX_RANGE)) ?\
                                (QOS_FAILURE) : (QOS_SUCCESS))


#define QOS_CHECK_MINRATE_TBL_INDEX_RANGE(MinRateId) \
          (((MinRateId <= 0) || (MinRateId > QOS_MINRATE_TBL_MAX_INDEX_RANGE)) ?\
                                (QOS_FAILURE) : (QOS_SUCCESS))

#define QOS_CHECK_MAXRATE_TBL_INDEX_RANGE(MaxRateId) \
          (((MaxRateId <= 0) || (MaxRateId > QOS_MAXRATE_TBL_MAX_INDEX_RANGE)) ?\
                                (QOS_FAILURE) : (QOS_SUCCESS))



#define QOS_IF_IDX_FROM_GLOBAL_QID(GlobalQid) (((GlobalQid - 1) / QOS_QUEUE_ENTRY_MAX) + 1)

#define QOS_IF_QID_FROM_GLOBAL_QID(GlobalQid) (((GlobalQid % QOS_QUEUE_ENTRY_MAX)) == 0) ?\
                                (QOS_QUEUE_ENTRY_MAX) : (GlobalQid % QOS_QUEUE_ENTRY_MAX)


/*    5. Policy Table  */
#define QOS_POLICY_ACTION_FLAG_LENGTH         1

#define QOS_POLICY_PHB_TYPE_MIN_VAL           0
#define QOS_POLICY_PHB_TYPE_MAX_VAL           5

#define QOS_POLICY_PHB_DEFAULT_MIN_VAL        0
#define QOS_POLICY_PHB_DEFAULT_MAX_VAL        63

/* Min Max Vlaues */
#define QOS_DSCP_MIN_VAL                      0
#define QOS_DSCP_MAX_VAL                     63

#define QOS_PRI_MIN_VAL                       0
#define QOS_PRI_MAX_VAL                       7

#define QOS_VLAN_DE_MIN_VAL                   0
#define QOS_VLAN_DE_MAX_VAL                   1

#define QOS_TRAF_CLASS_MIN_VAL                0
#define QOS_TRAF_CLASS_MAX_VAL                8
   

#define QOS_GET_POLICY_ACTIONS_MASK(pSNMPOctSrt,u1Mask) \
    pSNMPOctSrt->i4_Length = QOS_POLICY_ACTION_FLAG_LENGTH; \
    MEMCPY (pSNMPOctSrt->pu1_OctetList,&u1Mask, pSNMPOctSrt->i4_Length);

#define QOS_SET_POLICY_ACTIONS_MASK(pActFlag,pSNMPOctSrt) \
    MEMCPY (pActFlag,pSNMPOctSrt->pu1_OctetList,pSNMPOctSrt->i4_Length); \

/*  Policy Map Table Index  - PolicyId */ 
#define QOS_CHECK_PLY_MAP_TBL_INDEX_RANGE(PlyId) \
          ((PlyId > 0) && (PlyId <= QOS_PLY_MAP_TBL_MAX_INDEX_RANGE)) ? \
                                (QOS_SUCCESS) : (QOS_FAILURE)

/* 6. Q Template Table */
#define QOS_Q_TEMP_SIZE_MIN_VAL           1
#define QOS_Q_TEMP_SIZE_MAX_VAL           0xBFFF40
    
#define QOS_Q_TEMP_DROP_TYPE_MIN_VAL      QOS_Q_TEMP_DROP_TYPE_OTHER
#define QOS_Q_TEMP_DROP_TYPE_MAX_VAL      QOS_Q_TEMP_DROP_TYPE_WRED
                                          

#define QOS_CHECK_Q_TEMP_TBL_INDEX_RANGE(QTempId) \
          ((QTempId <= 0) || (QTempId > QOS_Q_TEMP_TBL_MAX_INDEX_RANGE)) ?\
                                  (QOS_FAILURE) : (QOS_SUCCESS)

/* 7. RD Cfg Table */
#define QOS_DROP_PRECEDENCE                        6
#define QOS_RD_CONFG_DP_MIN_VAL                    0
#define QOS_RD_CONFG_DP_MAX_VAL                    5
#define QOS_RD_CONFG_MIN_AVG_TH_MIN_VAL              1
#define QOS_RD_CONFG_MIN_AVG_TH_MAX_VAL          65535
#define QOS_RD_CONFG_MAX_AVG_TH_MIN_VAL              1
#define QOS_RD_CONFG_MAX_AVG_TH_MAX_VAL          65535 

#define QOS_RD_CONFG_MIN_AVG_TH_BYTES_MIN_VAL    1
#define QOS_RD_CONFG_MIN_AVG_TH_BYTES_MAX_VAL    0xBFFF40

#define QOS_RD_CONFG_MAX_AVG_TH_BYTES_MIN_VAL    1
#define QOS_RD_CONFG_MAX_AVG_TH_BYTES_MAX_VAL    0xBFFF40

#define QOS_RD_CONFG_MAX_PKT_SIZE_MIN_VAL            1 
#define QOS_RD_CONFG_MAX_PKT_SIZE_MAX_VAL        65535
#define QOS_RD_CONFG_MAX_PROB_MIN_VAL              1
#define QOS_RD_CONFG_MAX_PROB_MAX_VAL            100
#define QOS_RD_CONFG_EXP_WEIGHT_MIN_VAL            0 
#define QOS_RD_CONFG_EXP_WEIGHT_MAX_VAL           31
#define QOS_RD_CONFG_GAIN_MIN_VAL                 0
#define QOS_RD_CONFG_GAIN_MAX_VAL                 100
#define QOS_RD_CONFG_ECN_THRESH_MIN_VAL           0
#define QOS_RD_CONFG_ECN_THRESH_MAX_VAL           65535
#define QOS_RD_CONFIG_ACTION_FLAG_LENGTH          1

#define QOS_CHECK_RD_CFG_TBL_QT_INDEX_RANGE(QTempId) \
          ((QTempId <= 0) || (QTempId > QOS_Q_TEMP_TBL_MAX_INDEX_RANGE)) ?\
                                  (QOS_FAILURE) : (QOS_SUCCESS)


#define QOS_RD_CFG_TBL_MAX_ENTRIES  ((QOS_Q_TEMP_TBL_MAX_ENTRIES) * \
                                      (QOS_RD_CONFG_DP_MAX_VAL))

#define QOS_GET_RD_CFG_ACTION_MASK(pSNMPOctSrt,u1Mask) \
    pSNMPOctSrt->i4_Length = QOS_RD_CONFIG_ACTION_FLAG_LENGTH; \
    MEMCPY (pSNMPOctSrt->pu1_OctetList,&u1Mask, pSNMPOctSrt->i4_Length);

#define QOS_SET_RD_CFG_ACTION_MASK(pRDCfgFlag,pSNMPOctSrt) \
    MEMCPY (pRDCfgFlag,pSNMPOctSrt->pu1_OctetList,pSNMPOctSrt->i4_Length); \

/* RD Config Action Flags */
#define QOS_RD_CONFIG_NONE                0x0000
#define QOS_RD_CONFIG_CAP_AVERAGE         0x0001
#define QOS_RD_CONFIG_MARK_ECN            0x0002
/*Rang of RD Cfg Action Flags */
#define QOS_RD_CONFIG_ACTION_MIN_VAL      0x0000
#define QOS_RD_CONFIG_ACTION_MAX_VAL      0x0003

typedef enum
{
    QOS_RD_CONFIG_DROP_THRESH_TYPE_PKTS=1,
    QOS_RD_CONFIG_DROP_THRESH_TYPE_BYTES
}eRdCfgDropThershType;

/* 8. Shape template Table */
#define QOS_SHAPE_CIR_MIN_VAL                      1
#define QOS_SHAPE_CIR_MAX_VAL               10485760
#define QOS_SHAPE_CBS_MIN_VAL                      0
#define QOS_SHAPE_CBS_MAX_VAL               10485760   
#define QOS_SHAPE_EIR_MIN_VAL                      0
#define QOS_SHAPE_EIR_MAX_VAL               10485760
#define QOS_SHAPE_EBS_MIN_VAL                      0
#define QOS_SHAPE_EBS_MAX_VAL               10485760

#define QOS_CHECK_SHAPE_TEMP_TBL_INDEX_RANGE(ShapeTempId) \
          ((ShapeTempId > 0) && \
           (ShapeTempId <= QOS_SHAPE_TEMP_TBL_MAX_INDEX_RANGE)) ?\
                                  (QOS_SUCCESS) : (QOS_FAILURE)
    

/* 9. Scheduler Table */
#define QOS_SCHED_Q_COUNT_MIN_VAL               1
#define QOS_SCHED_Q_COUNT_MAX_VAL               8
#define QOS_SCHED_ALGO_MIN_VAL                  1
#define QOS_SCHED_ALGO_MAX_VAL                  8
#define QOS_SCHED_HL_MIN_VAL                    0
#define QOS_SCHED_HL_MAX_VAL                   10
                                              

#define QOS_CHECK_SCHED_TBL_INDEX_RANGE(SchedId) \
          ((SchedId> 0) && (SchedId <= QOS_SCHED_TBL_MAX_INDEX_RANGE)) ?\
                                  (QOS_SUCCESS) : (QOS_FAILURE)

/* 10. Q Table */
#define QOS_Q_TBL_Q_WEIGHT_MIN_VAL              0 
#define QOS_Q_TBL_Q_WEIGHT_MAX_VAL           1000
#define QOS_Q_TBL_Q_PRIO_MIN_VAL                0
#define QOS_Q_TBL_Q_PRIO_MAX_VAL               15


/* 11. QMap Table */
    
#define QOS_QMAP_PRI_VLAN_PRI_MIN        0
#define QOS_QMAP_PRI_VLAN_PRI_MAX        15
#define QOS_QMAP_PRI_IP_TOS_MIN          0
#define QOS_QMAP_PRI_IP_TOS_MAX          7
#define QOS_QMAP_PRI_IP_DSCP_MIN         0
#define QOS_QMAP_PRI_IP_DSCP_MAX         63 
#define QOS_QMAP_PRI_MPLS_EXP_MIN        0
#define QOS_QMAP_PRI_MPLS_EXP_MAX        7
#define QOS_QMAP_PRI_VLAN_DEI_MIN        0
#define QOS_QMAP_PRI_VLAN_DEI_MAX        1

#define QOS_QMAP_SW_HW                   1
#define QOS_QMAP_HW_ONLY                 2

#define QOS_CHECK_Q_MAP_TBL_INDEX_RANGE(QMapId) \
          ((QMapId> 0) && (QMapId <= QOS_Q_MAP_TBL_MAX_INDEX_RANGE)) ?\
                                  (QOS_SUCCESS) : (QOS_FAILURE)

/* 12. Hierarchy Table */
#define QOS_HIERARCHY_WEIGHT_MIN_VAL        0
#define QOS_HIERARCHY_WEIGHT_MAX_VAL        1000
#define QOS_HIERARCHY_PRI_MIN_VAL           0
#define QOS_HIERARCHY_PRI_MAX_VAL           15
#define QOS_HL_DEF_SCHED_CFG_SUPPORTED      1 /*To be compared with QOS_DEF_HL_SCHED_CONFIG_SUPPORT to */
#define QOS_HL_DEF_SCHED_CFG_NOT_SUPPORTED  0 /*decide,if Hierarchical Scheduling is supported */
#define QOS_HL_DEF_S1_SCHEDULER_ID          1 /*Default Scheduler ID for Level1 Scheduler*/
#define QOS_HL_DEF_S2_SCHEDULER_ID          2 /*Default Scheduler ID for Level2 Scheduler*/
#define QOS_HL_DEF_S3_SCHEDULER1_ID         3 /*Default Scheduler ID for Level3 Scheduler*/
#define QOS_HL_DEF_S3_SCHEDULER2_ID         4 /*Default Scheduler ID for Level3 Scheduler*/

#endif /* __QOS_DEFS_H__ */

#define QOS_TBL_TYPE_PRI_MAP       1
#define QOS_TBL_TYPE_CLS_MAP       2
#define QOS_TBL_TYPE_METER         3
#define QOS_TBL_TYPE_PLY_MAP       4
#define QOS_TBL_TYPE_Q_TEMP        5
#define QOS_TBL_TYPE_SHAPE_TEMP    6
                
#define QOS_QUEUE_SIZE             50
#define QOS_DEF_SCHED_ID             1
#define QOS_MSG_QUEUE_ID           gQosPktQId
#define QOS_DEF_MSG_LEN            OSIX_DEF_MSG_LEN
#define QOS_SEND_TO_QUEUE          OsixQueSend
#define QOS_SEND_EVENT             OsixEvtSend

#define QOS_DATA_PATH_SIZE        100
#define QOS_CLFR_SIZE             100
#define QOS_CLFR_ELEMENT_SIZE     100




#define QOS_CREATE_MEM_POOL        MemCreateMemPool 
#define QOS_QMSG_ALLOC_MEMBLK      MemAllocMemBlk(gQoSGlobalInfo.QosQPktMemPoolId)
#define QOS_RELEASE_QMSG_MEM_BLOCK(pPkt) \
         MemReleaseMemBlock(gQoSGlobalInfo.QosQPktMemPoolId, (UINT1 *) pPkt)


#define QOS_DISABLE_PORT_MSG       1
#define QOS_ENABLE_PORT_MSG        2
#define QOS_DELETE_PORT_MSG        3 
#define QOS_CREATE_PORT_MSG        4
#define QOS_RM_MSG     5

#define QOS_MSG_EVENT       0x01
#define QOS_TRUE                        1
#define QOS_FALSE                       0
#define QOS_MIN_FILTER_ID               1000
#define QOS_MAX_FILTER_ID               65535
#define QOS_START_INITIAL_INDEX         0
#define QOS_END_INDEX                   100
#define QOS_INDEX_FULL                  0
#define QOS_MF_PERFIX_IPV4_MIN          0
#define QOS_MF_PERFIX_IPV4_MAX          32
#define QOS_MF_PERFIX_IPV6_MIN          0
#define QOS_MF_PERFIX_IPV6_MAX          128
#define QOS_FLOW_ID_MIN                 0
#define QOS_FLOW_ID_MAX                 1048575
#define QOS_STORAGE_VOLATILE            2
#define QOS_STORAGE_NONVOLATILE         3
#define QOS_CLFR_ELEMENT_MIN            1
#define QOS_CLFR_ELEMENT_MAX            65535
#define QOS_CLFR_MIN                    1
#define QOS_CLFR_MAX                    65535
#define QOS_DATA_PATH_MIN               1
#define QOS_DATA_PATH_MAX               65535
#define QOS_CLFR_ELEMENT_PREC_MIN       1
#define QOS_CLFR_ELEMENT_PREC_MAX       65535
#define QOS_DATA_IN                     1
#define QOS_DATA_OUT                    2
#define QOS_ROWPOINTER_DEF              0
#define QOS_DEF_CPU_MIN_RATE            1
#define QOS_DEF_CPU_MAX_RATE            65535

/* RowStatus values */
#define   QOS_ACTIVE             0x01
#define   QOS_NOT_IN_SERVICE     0x02
#define   QOS_NOT_READY          0x03
#define   QOS_CREATE_AND_GO      0x04
#define   QOS_CREATE_AND_WAIT    0x05
#define   QOS_DESTROY            0x06

/*    for Layer3 range */
#define QOS_L3FILTER_ID_VALID(FILTERId) \
        (((FILTERId >= QOS_MIN_FILTER_ID) && \
        (FILTERId <= QOS_MAX_FILTER_ID)) ? QOS_SUCCESS : QOS_FAILURE)

/*   Check for Clfr Element  range */
#define QOS_CLFR_ELEMENT_ID_VALID(Index1,Index2) \
        (((Index1 >= QOS_CLFR_MIN) && \
        (Index1 <= QOS_CLFR_MAX) && (Index2 >= QOS_CLFR_ELEMENT_MIN) && (Index2 <= QOS_CLFR_ELEMENT_MAX) ) ? QOS_SUCCESS : QOS_FAILURE)

/*   Check for Clfr  range */
#define QOS_CLFR_ID_VALID(Index1) \
        (((Index1 >= QOS_CLFR_MIN) && \
        (Index1 <= QOS_CLFR_MAX)) ? QOS_SUCCESS : QOS_FAILURE)

/*   Check for Data Path  range */
#define QOS_DATA_PATH_ID_VALID(Index1) \
        (((Index1 >= QOS_DATA_PATH_MIN) && \
        (Index1 <= QOS_DATA_PATH_MAX))  ? QOS_SUCCESS : QOS_FAILURE)


#define QOSX_CONVERT_IPADDR_TO_STR(pString, u4Value)\
{\
    tUtlInAddr          IpAddr;\
\
         IpAddr.u4Addr = OSIX_NTOHL (u4Value);\
\
         pString = (CHR1 *) UtlInetNtoa(IpAddr);\
\
}

#define QOS_CLFR_NEXT_FREE() gQoSGlobalInfo.DSNextFree.u4ClfrNextFree

#define QOS_CLASS_NEXT_FREE() gQoSGlobalInfo.DSNextFree.u4CLASSNextFree
#define QOS_CLASSMAP_NEXT_FREE() gQoSGlobalInfo.DSNextFree.u4ClassMapNextFree

#define QOS_CLFR_ELEM_NEXT_FREE() \
   gQoSGlobalInfo.DSNextFree.u4ClfrElementNextFree

#define QOS_MFC_NEXT_FREE() gQoSGlobalInfo.DSNextFree.u4MultiFieldNextFree

#define QOS_METER_NEXT_FREE() gQoSGlobalInfo.DSNextFree.u4MeterNextFree

#define QOS_TBPARM_NEXT_FREE() gQoSGlobalInfo.DSNextFree.u4TBParamNextFree

#define QOS_ACTION_NEXT_FREE() gQoSGlobalInfo.DSNextFree.u4TBParamNextFree

#define QOS_CNTACT_NEXT_FREE() gQoSGlobalInfo.DSNextFree.u4CountActNextFree

#define QOS_ALGDROP_NEXT_FREE() gQoSGlobalInfo.DSNextFree.u4AlgoDropNextFree

#define QOS_RD_NEXT_FREE() gQoSGlobalInfo.DSNextFree.u4RandomDropNextFree

#define QOS_Q_NEXT_FREE() gQoSGlobalInfo.DSNextFree.u4QNextFree

#define QOS_SCHED_NEXT_FREE() gQoSGlobalInfo.DSNextFree.u4SchedulerNextFree

#define QOS_MINRATE_NEXT_FREE() gQoSGlobalInfo.DSNextFree.u4MinRateNextFree

#define QOS_MAXRATE_NEXT_FREE() gQoSGlobalInfo.DSNextFree.u4MaxRateNextFree
#define QOS_TEMPLATE_NEXT_FREE() gQoSGlobalInfo.DSNextFree.u4TemplateNextFree


#define QOS_CLFR_COUNT gQoSGlobalInfo.DSCount.u4ClfrCount

#define QOS_CLASS_COUNT gQoSGlobalInfo.DSCount.u4CLASSCount
#define QOS_CLASSMAP_COUNT gQoSGlobalInfo.DSCount.u4ClassMapCount

#define QOS_CLFR_ELEM_COUNT \
   gQoSGlobalInfo.DSCount.u4ClfrElementCount

#define QOS_MFC_COUNT gQoSGlobalInfo.DSCount.u4MultiFieldCount

#define QOS_METER_COUNT gQoSGlobalInfo.DSCount.u4MeterCount

#define QOS_TBPARM_COUNT gQoSGlobalInfo.DSCount.u4TBParamCount

#define QOS_ACTION_COUNT gQoSGlobalInfo.DSCount.u4TBParamCount

#define QOS_CNTACT_COUNT gQoSGlobalInfo.DSCount.u4CountActCount

#define QOS_ALGDROP_COUNT gQoSGlobalInfo.DSCount.u4AlgoDropCount

#define QOS_RD_COUNT gQoSGlobalInfo.DSCount.u4RandomDropCount

#define QOS_Q_COUNT gQoSGlobalInfo.DSCount.u4QCount

#define QOS_SCHED_COUNT gQoSGlobalInfo.DSCount.u4SchedulerCount

#define QOS_MINRATE_COUNT gQoSGlobalInfo.DSCount.u4MinRateCount

#define QOS_MAXRATE_COUNT gQoSGlobalInfo.DSCount.u4MaxRateCount
#define QOS_TEMPLATE_COUNT gQoSGlobalInfo.DSCount.u4TemplateCount

/*Range for All the Standard Tables*/
#define QOS_ACTION_TBL_MAX_INDEX_RANGE          65535
#define QOS_COUNT_ACT_TBL_MAX_INDEX_RANGE          65535
#define QOS_RND_DROP_TBL_MAX_INDEX_RANGE          65535
#define QOS_MAX_SAMPLING_RATE                   1000000
#define QOS_ACTION_TBL_MAX_ENTRIES         (100)
#define QOS_COUNTACT_TBL_MAX_ENTRIES         (100)
#define QOS_INVALID_NEXT_FREE                   0xffffffff

/*Range Check Macros for all standard tables*/
#define QOS_CHECK_ACTION_TBL_INDEX_RANGE(ActionId) \
 (((ActionId <= 0) || (ActionId > QOS_ACTION_TBL_MAX_INDEX_RANGE)) ? \
                                (QOS_FAILURE) : (QOS_SUCCESS))
#define QOS_CHECK_COUNT_ACT_TBL_INDEX_RANGE(CountActId) \
       (((CountActId < 1) || (CountActId >= QOS_COUNT_ACT_TBL_MAX_INDEX_RANGE)) ? \
                                (QOS_FAILURE) : (QOS_SUCCESS))
#define QOS_CHECK_RANDOM_TBL_INDEX_RANGE(RandomDropId) \
       ((RandomDropId > 0) && (RandomDropId <= QOS_RND_DROP_TBL_MAX_INDEX_RANGE))\
        ?  (QOS_SUCCESS) : (QOS_FAILURE)
/* CountAct Stats */
#define QOS_COUNT_ACT_OCTETS                   1
#define QOS_COUNT_ACT_PKTS                     2
/* AlgDrop Stats */
#define QOS_ALG_DROP_OCTETS                    3
#define QOS_ALG_DROP_PKTS                      4
/* RandomDrop Stats */
#define QOS_RANDOM_DROP_OCTETS                 5
#define QOS_RANDOM_DROP_PKTS                   6

#define QOS_MIN_VALUE                       1
#define QOS_MAX_PRIORITY                       65535
#define QOS_MAX_ABSOLUTE                       65535
#define QOS_MAX_THRESHOLD                      65535
#define QOS_MAX_RELATIVE                       65535
#define QOS_MAX_RATE_ABSOLUTE_DEFAULT          1
#define QOS_MAX_RATE_ABSOLUTE_DEFAULT          1
#define QOS_MAX_RATE_THRESHOLD_DEFAULT         1
#define QOS_MIN_RATE_PRIORITY_DEFAULT          1
#define QOS_MIN_RATE_ABSOLUTE_DEFAULT          1 
#define QOS_MIN_RATE_RELATIVE_DEFAULT          1
#define QOS_MAX_POINTED_TO_ENTRIES             1000
#define QOS_MAX_CPU_Q_ENTRIES                  QOS_QUEUE_ENTRY_MAX 
#define QOS_MAX_PORT_TBL_ENTRIES               2112 

#define QOS_ADD 0
#define QOS_DEL 1
#define QOS_CHECK 2
#define QOS_NEXT_INDEX 1
/*1. Port Table Index  - Interface Index */
#define QOS_CHECK_PORT_TBL_INDEX_RANGE(PMId,PktType) \
          ((PMId > 0) && (PMId <= QOS_PORT_TBL_MAX_INDEX_RANGE))? \
                                  (QOS_SUCCESS) : (QOS_FAILURE)
#define QOS_PCP_SEL_ROW_MAX_VAL 3
#define QOS_PCP_SEL_ROW_MIN_VAL 0
 
#define QOS_PCP_PKT_TYPE_MAX_VAL 2
#define QOS_PCP_PKT_TYPE_MIN_VAL 0
 
#define QOS_GET_PCP_TBL_INDEX(i4IfIndex,i4PktType, i4PcpIndex){\
    if(i4PktType == QOS_PCP_PKT_TYPE_DOT1P){\
      i4PcpIndex = ((QOS_PCP_1P_TBL_DEF_ENTRY_MIN) + ((i4IfIndex - 1)*16));\
    } \
    else if(i4PktType == QOS_PCP_PKT_TYPE_IP){\
      i4PcpIndex = ((QOS_PCP_IP_TBL_DEF_ENTRY_MIN) + ((i4IfIndex - 1)*64));\
    } \
    else {\
      i4PcpIndex = ((QOS_PCP_MPLS_TBL_DEF_ENTRY_MIN) + ((i4IfIndex - 1)*8));\
    } \
}
#define QOS_GET_PCP_OL_INDEX(i4PktType, u1OutLoopIndex)\
{\
  if (i4PktType == QOS_PCP_PKT_TYPE_DOT1P)\
  {\
      u1OutLoopIndex = 7;\
  }\
  else if (i4PktType == QOS_PCP_PKT_TYPE_IP)\
  {\
      u1OutLoopIndex=63;\
  }\
  else\
  {\
      u1OutLoopIndex=7;\
  }\
}
                     
#define QOS_GET_PCP_IL_INDEX(i4PktType, u1InLoopIndex)\
{\
  if (i4PktType == QOS_PCP_PKT_TYPE_DOT1P)\
  {\
      u1InLoopIndex = 1;\
  }\
  else if (i4PktType == QOS_PCP_PKT_TYPE_IP)\
  {\
      u1InLoopIndex=0;\
  }\
  else\
  {\
      u1InLoopIndex=0;\
  }\
}
   
    
#define QOS_PORTS_PER_BYTE 8

#define QOS_SET_MEMBER_PORT(au1PortArray, u2Port) \
           {\
              UINT2 u2PortBytePos;\
              UINT2 u2PortBitPos;\
              u2PortBytePos = (UINT2)(u2Port / VLAN_PORTS_PER_BYTE);\
              u2PortBitPos  = (UINT2)(u2Port % VLAN_PORTS_PER_BYTE);\
       if (u2PortBitPos  == 0) {u2PortBytePos -= 1;} \
           \
              au1PortArray[u2PortBytePos] = (UINT1)(au1PortArray[u2PortBytePos] \
                             | gau1QosBitMaskMap[u2PortBitPos]);\
           }
/* 
 * The macro QOS_RESET_MEMBER_PORT (), removes u2Port from the member 
 * list of au1PortArray. 
 * Warning!!! - Do not call the macro with u2Port as 0 or Invalid Port.
 */
#define QOS_RESET_MEMBER_PORT(au1PortArray, u2Port) \
              {\
                 UINT2 u2PortBytePos;\
                 UINT2 u2PortBitPos;\
                 u2PortBytePos = (UINT2)(u2Port / VLAN_PORTS_PER_BYTE);\
                 u2PortBitPos  = (UINT2)(u2Port % VLAN_PORTS_PER_BYTE);\
   if (u2PortBitPos  == 0) {u2PortBytePos -= 1;} \
                 \
                 au1PortArray[u2PortBytePos] = (UINT1)(au1PortArray[u2PortBytePos] \
                               & ~gau1QosBitMaskMap[u2PortBitPos]);\
              }

#define QOS_INIT_VAL 0
#define QOS_MAX_VLAN_PRI  8
#define QOS_CHANGE_HUN_NOTATION 10

#define QOS_RBTREE_KEY_LESSER                 (-1)
#define QOS_RBTREE_KEY_GREATER                (1)
#define QOS_RBTREE_KEY_EQUAL                  (0)
#define QOS_STD_CLFR_PORT_BASIC               1


#ifdef QOS_ARRAY_TO_RBTREE_WANTED

#define QOS_STD_CLFR_PORT_ENTRIES             (QOS_CLFR_SIZE *    \
                                               BRG_MAX_PHY_PLUS_LAG_INT_PORTS)

#define QOS_CREATE_STD_CLFR_PORT_RBTREE()     QosCreateQosStdClfrPortTbl()
#define QOS_DELETE_STD_CLFR_PORT_RBTREE()     QosDeleteQosStdClfrPortTbl() 

#define QOS_SET_CLFR_MEMBER_PORT(pEntry,u2Port)                             \
        QosAddPortNodeInStdClfrPortTbl (pEntry->u4DsClfrId, u2Port,         \
            QOS_STD_CLFR_PORT_BASIC)

#define QOS_RESET_CLFR_MEMBER_PORT(pEntry,u2Port)                           \
        QosDelPortNodeInStdClfrPortTbl (pEntry->u4DsClfrId, u2Port,         \
            QOS_STD_CLFR_PORT_BASIC)

#else /* QOS_ARRAY_TO_RBTREE_WANTED */

/* the following macro is 1. Because tPortList will
 * be used in the code and hence the data strucutre
 * tQosStdClfrPortNode will not be required and size is 1.
 * This macro should not be used anywhere in the code
 * for arrays. Because this is mapped to sizing variable
 * MAX_QOS_STD_CLFR_PORT_ENTRIES.
 * NOTE: the reason to keep this memory size as 1 is, when pool creation is
 * being done, the number of blocks should be atleast 1 otherwise, the pool
 * creation will fail.
 */
#define QOS_STD_CLFR_PORT_ENTRIES             1

#define QOS_CREATE_STD_CLFR_PORT_RBTREE()     QosUtilRBTreeStub()
#define QOS_DELETE_STD_CLFR_PORT_RBTREE()     QosUtilRBTreeStub()
      
#define QOS_SET_CLFR_MEMBER_PORT(pEntry,u2Port)                             \
        QOS_SET_MEMBER_PORT (pEntry->DPPortList, u2Port)

#define QOS_RESET_CLFR_MEMBER_PORT(pEntry,u2Port)                           \
        QOS_RESET_MEMBER_PORT (pEntry->DPPortList, u2Port)

#define QOS_VLAN_MAP_TBL_MAX_ENTRIES       (64)
#endif /* QOS_ARRAY_TO_RBTREE_WANTED */
