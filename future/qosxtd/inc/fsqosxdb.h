/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*$Id: fsqosxdb.h,v 1.20 2016/07/05 07:38:42 siva Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/
#ifndef _FSQOSXDB_H
#define _FSQOSXDB_H

UINT1 FsQoSPriorityMapTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32};
UINT1 FsQoSClassMapTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32};
UINT1 FsQoSClassToPriorityTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32};
UINT1 FsQoSMeterTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32};
UINT1 FsQoSPolicyMapTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32};
UINT1 FsQoSQTemplateTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32};
UINT1 FsQoSRandomDetectCfgTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_INTEGER32};
UINT1 FsQoSShapeTemplateTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32};
UINT1 FsQoSQMapTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 FsQoSQTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 FsQoSSchedulerTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 FsQoSHierarchyTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 FsQoSDefUserPriorityTableINDEX [] = {SNMP_DATA_TYPE_INTEGER};
UINT1 FsQoSPolicerStatsTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32};
UINT1 FsQoSCoSQStatsTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 FsQosHwCpuRateLimitTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32};
UINT1 FsQoSVlanMapTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32};
UINT1 FsQoSPortTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_INTEGER};

UINT4 fsqosx [] ={1,3,6,1,4,1,29601,2,6};
tSNMP_OID_TYPE fsqosxOID = {9, fsqosx};


UINT4 FsQoSSystemControl [ ] ={1,3,6,1,4,1,29601,2,6,1,1,1};
UINT4 FsQoSStatus [ ] ={1,3,6,1,4,1,29601,2,6,1,1,2};
UINT4 FsQoSTrcFlag [ ] ={1,3,6,1,4,1,29601,2,6,1,1,3};
UINT4 FsQoSRateUnit [ ] ={1,3,6,1,4,1,29601,2,6,1,1,4};
UINT4 FsQoSRateGranularity [ ] ={1,3,6,1,4,1,29601,2,6,1,1,5};
UINT4 FsQoSVlanQueueingStatus [ ] ={1,3,6,1,4,1,29601,2,6,1,1,6};
UINT4 FsQoSPriorityMapID [ ] ={1,3,6,1,4,1,29601,2,6,1,2,1,1,1};
UINT4 FsQoSPriorityMapName [ ] ={1,3,6,1,4,1,29601,2,6,1,2,1,1,2};
UINT4 FsQoSPriorityMapIfIndex [ ] ={1,3,6,1,4,1,29601,2,6,1,2,1,1,3};
UINT4 FsQoSPriorityMapVlanId [ ] ={1,3,6,1,4,1,29601,2,6,1,2,1,1,4};
UINT4 FsQoSPriorityMapInPriority [ ] ={1,3,6,1,4,1,29601,2,6,1,2,1,1,5};
UINT4 FsQoSPriorityMapInPriType [ ] ={1,3,6,1,4,1,29601,2,6,1,2,1,1,6};
UINT4 FsQoSPriorityMapRegenPriority [ ] ={1,3,6,1,4,1,29601,2,6,1,2,1,1,7};
UINT4 FsQoSPriorityMapRegenInnerPriority [ ] ={1,3,6,1,4,1,29601,2,6,1,2,1,1,8};
UINT4 FsQoSPriorityMapConfigStatus [ ] ={1,3,6,1,4,1,29601,2,6,1,2,1,1,9};
UINT4 FsQoSPriorityMapStatus [ ] ={1,3,6,1,4,1,29601,2,6,1,2,1,1,10};
UINT4 FsQoSPriorityMapInDEI [ ] ={1,3,6,1,4,1,29601,2,6,1,2,1,1,11};
UINT4 FsQoSPriorityMapRegenColor [ ] ={1,3,6,1,4,1,29601,2,6,1,2,1,1,12};
UINT4 FsQoSClassMapId [ ] ={1,3,6,1,4,1,29601,2,6,1,2,2,1,1};
UINT4 FsQoSClassMapName [ ] ={1,3,6,1,4,1,29601,2,6,1,2,2,1,2};
UINT4 FsQoSClassMapL2FilterId [ ] ={1,3,6,1,4,1,29601,2,6,1,2,2,1,3};
UINT4 FsQoSClassMapL3FilterId [ ] ={1,3,6,1,4,1,29601,2,6,1,2,2,1,4};
UINT4 FsQoSClassMapPriorityMapId [ ] ={1,3,6,1,4,1,29601,2,6,1,2,2,1,5};
UINT4 FsQoSClassMapCLASS [ ] ={1,3,6,1,4,1,29601,2,6,1,2,2,1,6};
UINT4 FsQoSClassMapClfrId [ ] ={1,3,6,1,4,1,29601,2,6,1,2,2,1,7};
UINT4 FsQoSClassMapPreColor [ ] ={1,3,6,1,4,1,29601,2,6,1,2,2,1,8};
UINT4 FsQoSClassMapStatus [ ] ={1,3,6,1,4,1,29601,2,6,1,2,2,1,9};
UINT4 FsQoSClassMapVlanMapId [ ] ={1,3,6,1,4,1,29601,2,6,1,2,2,1,10};
UINT4 FsQoSClassToPriorityCLASS [ ] ={1,3,6,1,4,1,29601,2,6,1,2,3,1,1};
UINT4 FsQoSClassToPriorityRegenPri [ ] ={1,3,6,1,4,1,29601,2,6,1,2,3,1,2};
UINT4 FsQoSClassToPriorityGroupName [ ] ={1,3,6,1,4,1,29601,2,6,1,2,3,1,3};
UINT4 FsQoSClassToPriorityStatus [ ] ={1,3,6,1,4,1,29601,2,6,1,2,3,1,4};
UINT4 FsQoSMeterId [ ] ={1,3,6,1,4,1,29601,2,6,1,3,1,1,1};
UINT4 FsQoSMeterName [ ] ={1,3,6,1,4,1,29601,2,6,1,3,1,1,2};
UINT4 FsQoSMeterType [ ] ={1,3,6,1,4,1,29601,2,6,1,3,1,1,3};
UINT4 FsQoSMeterInterval [ ] ={1,3,6,1,4,1,29601,2,6,1,3,1,1,4};
UINT4 FsQoSMeterColorMode [ ] ={1,3,6,1,4,1,29601,2,6,1,3,1,1,5};
UINT4 FsQoSMeterCIR [ ] ={1,3,6,1,4,1,29601,2,6,1,3,1,1,6};
UINT4 FsQoSMeterCBS [ ] ={1,3,6,1,4,1,29601,2,6,1,3,1,1,7};
UINT4 FsQoSMeterEIR [ ] ={1,3,6,1,4,1,29601,2,6,1,3,1,1,8};
UINT4 FsQoSMeterEBS [ ] ={1,3,6,1,4,1,29601,2,6,1,3,1,1,9};
UINT4 FsQoSMeterNext [ ] ={1,3,6,1,4,1,29601,2,6,1,3,1,1,10};
UINT4 FsQoSMeterStatus [ ] ={1,3,6,1,4,1,29601,2,6,1,3,1,1,11};
UINT4 FsQoSPolicyMapId [ ] ={1,3,6,1,4,1,29601,2,6,1,3,2,1,1};
UINT4 FsQoSPolicyMapName [ ] ={1,3,6,1,4,1,29601,2,6,1,3,2,1,2};
UINT4 FsQoSPolicyMapIfIndex [ ] ={1,3,6,1,4,1,29601,2,6,1,3,2,1,3};
UINT4 FsQoSPolicyMapCLASS [ ] ={1,3,6,1,4,1,29601,2,6,1,3,2,1,4};
UINT4 FsQoSPolicyMapPHBType [ ] ={1,3,6,1,4,1,29601,2,6,1,3,2,1,5};
UINT4 FsQoSPolicyMapDefaultPHB [ ] ={1,3,6,1,4,1,29601,2,6,1,3,2,1,6};
UINT4 FsQoSPolicyMapMeterTableId [ ] ={1,3,6,1,4,1,29601,2,6,1,3,2,1,7};
UINT4 FsQoSPolicyMapFailMeterTableId [ ] ={1,3,6,1,4,1,29601,2,6,1,3,2,1,8};
UINT4 FsQoSPolicyMapInProfileConformActionFlag [ ] ={1,3,6,1,4,1,29601,2,6,1,3,2,1,9};
UINT4 FsQoSPolicyMapInProfileConformActionId [ ] ={1,3,6,1,4,1,29601,2,6,1,3,2,1,10};
UINT4 FsQoSPolicyMapInProfileActionSetPort [ ] ={1,3,6,1,4,1,29601,2,6,1,3,2,1,11};
UINT4 FsQoSPolicyMapConformActionSetIpTOS [ ] ={1,3,6,1,4,1,29601,2,6,1,3,2,1,12};
UINT4 FsQoSPolicyMapConformActionSetDscp [ ] ={1,3,6,1,4,1,29601,2,6,1,3,2,1,13};
UINT4 FsQoSPolicyMapConformActionSetVlanPrio [ ] ={1,3,6,1,4,1,29601,2,6,1,3,2,1,14};
UINT4 FsQoSPolicyMapConformActionSetVlanDE [ ] ={1,3,6,1,4,1,29601,2,6,1,3,2,1,15};
UINT4 FsQoSPolicyMapConformActionSetInnerVlanPrio [ ] ={1,3,6,1,4,1,29601,2,6,1,3,2,1,16};
UINT4 FsQoSPolicyMapConformActionSetMplsExp [ ] ={1,3,6,1,4,1,29601,2,6,1,3,2,1,17};
UINT4 FsQoSPolicyMapConformActionSetNewCLASS [ ] ={1,3,6,1,4,1,29601,2,6,1,3,2,1,18};
UINT4 FsQoSPolicyMapInProfileExceedActionFlag [ ] ={1,3,6,1,4,1,29601,2,6,1,3,2,1,19};
UINT4 FsQoSPolicyMapInProfileExceedActionId [ ] ={1,3,6,1,4,1,29601,2,6,1,3,2,1,20};
UINT4 FsQoSPolicyMapExceedActionSetIpTOS [ ] ={1,3,6,1,4,1,29601,2,6,1,3,2,1,21};
UINT4 FsQoSPolicyMapExceedActionSetDscp [ ] ={1,3,6,1,4,1,29601,2,6,1,3,2,1,22};
UINT4 FsQoSPolicyMapExceedActionSetInnerVlanPrio [ ] ={1,3,6,1,4,1,29601,2,6,1,3,2,1,23};
UINT4 FsQoSPolicyMapExceedActionSetVlanPrio [ ] ={1,3,6,1,4,1,29601,2,6,1,3,2,1,24};
UINT4 FsQoSPolicyMapExceedActionSetVlanDE [ ] ={1,3,6,1,4,1,29601,2,6,1,3,2,1,25};
UINT4 FsQoSPolicyMapExceedActionSetMplsExp [ ] ={1,3,6,1,4,1,29601,2,6,1,3,2,1,26};
UINT4 FsQoSPolicyMapExceedActionSetNewCLASS [ ] ={1,3,6,1,4,1,29601,2,6,1,3,2,1,27};
UINT4 FsQoSPolicyMapOutProfileActionFlag [ ] ={1,3,6,1,4,1,29601,2,6,1,3,2,1,28};
UINT4 FsQoSPolicyMapOutProfileActionId [ ] ={1,3,6,1,4,1,29601,2,6,1,3,2,1,29};
UINT4 FsQoSPolicyMapOutProfileActionSetIPTOS [ ] ={1,3,6,1,4,1,29601,2,6,1,3,2,1,30};
UINT4 FsQoSPolicyMapOutProfileActionSetDscp [ ] ={1,3,6,1,4,1,29601,2,6,1,3,2,1,31};
UINT4 FsQoSPolicyMapOutProfileActionSetInnerVlanPrio [ ] ={1,3,6,1,4,1,29601,2,6,1,3,2,1,32};
UINT4 FsQoSPolicyMapOutProfileActionSetVlanPrio [ ] ={1,3,6,1,4,1,29601,2,6,1,3,2,1,33};
UINT4 FsQoSPolicyMapOutProfileActionSetVlanDE [ ] ={1,3,6,1,4,1,29601,2,6,1,3,2,1,34};
UINT4 FsQoSPolicyMapOutProfileActionSetMplsExp [ ] ={1,3,6,1,4,1,29601,2,6,1,3,2,1,35};
UINT4 FsQoSPolicyMapOutProfileActionSetNewCLASS [ ] ={1,3,6,1,4,1,29601,2,6,1,3,2,1,36};
UINT4 FsQoSPolicyMapStatus [ ] ={1,3,6,1,4,1,29601,2,6,1,3,2,1,37};
UINT4 FsQoSPolicyMapConformActionSetInnerVlanDE [ ] ={1,3,6,1,4,1,29601,2,6,1,3,2,1,38};
UINT4 FsQoSPolicyMapExceedActionSetInnerVlanDE [ ] ={1,3,6,1,4,1,29601,2,6,1,3,2,1,39};
UINT4 FsQoSPolicyMapOutProfileActionSetInnerVlanDE [ ] ={1,3,6,1,4,1,29601,2,6,1,3,2,1,40};
UINT4 FsQoSPolicyDefaultVlanDE [ ] ={1,3,6,1,4,1,29601,2,6,1,3,2,1,41};
UINT4 FsQoSQTemplateId [ ] ={1,3,6,1,4,1,29601,2,6,1,4,1,1,1};
UINT4 FsQoSQTemplateName [ ] ={1,3,6,1,4,1,29601,2,6,1,4,1,1,2};
UINT4 FsQoSQTemplateDropType [ ] ={1,3,6,1,4,1,29601,2,6,1,4,1,1,3};
UINT4 FsQoSQTemplateDropAlgoEnableFlag [ ] ={1,3,6,1,4,1,29601,2,6,1,4,1,1,4};
UINT4 FsQoSQTemplateSize [ ] ={1,3,6,1,4,1,29601,2,6,1,4,1,1,5};
UINT4 FsQoSQTemplateStatus [ ] ={1,3,6,1,4,1,29601,2,6,1,4,1,1,6};
UINT4 FsQoSRandomDetectCfgDP [ ] ={1,3,6,1,4,1,29601,2,6,1,4,2,1,1};
UINT4 FsQoSRandomDetectCfgMinAvgThresh [ ] ={1,3,6,1,4,1,29601,2,6,1,4,2,1,2};
UINT4 FsQoSRandomDetectCfgMaxAvgThresh [ ] ={1,3,6,1,4,1,29601,2,6,1,4,2,1,3};
UINT4 FsQoSRandomDetectCfgMaxPktSize [ ] ={1,3,6,1,4,1,29601,2,6,1,4,2,1,4};
UINT4 FsQoSRandomDetectCfgMaxProb [ ] ={1,3,6,1,4,1,29601,2,6,1,4,2,1,5};
UINT4 FsQoSRandomDetectCfgExpWeight [ ] ={1,3,6,1,4,1,29601,2,6,1,4,2,1,6};
UINT4 FsQoSRandomDetectCfgStatus [ ] ={1,3,6,1,4,1,29601,2,6,1,4,2,1,7};
UINT4 FsQoSRandomDetectCfgGain [ ] ={1,3,6,1,4,1,29601,2,6,1,4,2,1,8};
UINT4 FsQoSRandomDetectCfgDropThreshType [ ] ={1,3,6,1,4,1,29601,2,6,1,4,2,1,9};
UINT4 FsQoSRandomDetectCfgECNThresh [ ] ={1,3,6,1,4,1,29601,2,6,1,4,2,1,10};
UINT4 FsQoSRandomDetectCfgActionFlag [ ] ={1,3,6,1,4,1,29601,2,6,1,4,2,1,11};
UINT4 FsQoSShapeTemplateId [ ] ={1,3,6,1,4,1,29601,2,6,1,4,3,1,1};
UINT4 FsQoSShapeTemplateName [ ] ={1,3,6,1,4,1,29601,2,6,1,4,3,1,2};
UINT4 FsQoSShapeTemplateCIR [ ] ={1,3,6,1,4,1,29601,2,6,1,4,3,1,3};
UINT4 FsQoSShapeTemplateCBS [ ] ={1,3,6,1,4,1,29601,2,6,1,4,3,1,4};
UINT4 FsQoSShapeTemplateEIR [ ] ={1,3,6,1,4,1,29601,2,6,1,4,3,1,5};
UINT4 FsQoSShapeTemplateEBS [ ] ={1,3,6,1,4,1,29601,2,6,1,4,3,1,6};
UINT4 FsQoSShapeTemplateStatus [ ] ={1,3,6,1,4,1,29601,2,6,1,4,3,1,7};
UINT4 FsQoSQMapCLASS [ ] ={1,3,6,1,4,1,29601,2,6,1,4,4,1,1};
UINT4 FsQoSQMapRegenPriType [ ] ={1,3,6,1,4,1,29601,2,6,1,4,4,1,2};
UINT4 FsQoSQMapRegenPriority [ ] ={1,3,6,1,4,1,29601,2,6,1,4,4,1,3};
UINT4 FsQoSQMapQId [ ] ={1,3,6,1,4,1,29601,2,6,1,4,4,1,4};
UINT4 FsQoSQMapStatus [ ] ={1,3,6,1,4,1,29601,2,6,1,4,4,1,5};
UINT4 FsQoSQMapRegenDEI [ ] ={1,3,6,1,4,1,29601,2,6,1,4,4,1,6};
UINT4 FsQoSQMapRegenColor [ ] ={1,3,6,1,4,1,29601,2,6,1,4,4,1,7};
UINT4 FsQoSQId [ ] ={1,3,6,1,4,1,29601,2,6,1,4,5,1,1};
UINT4 FsQoSQCfgTemplateId [ ] ={1,3,6,1,4,1,29601,2,6,1,4,5,1,2};
UINT4 FsQoSQSchedulerId [ ] ={1,3,6,1,4,1,29601,2,6,1,4,5,1,3};
UINT4 FsQoSQWeight [ ] ={1,3,6,1,4,1,29601,2,6,1,4,5,1,4};
UINT4 FsQoSQPriority [ ] ={1,3,6,1,4,1,29601,2,6,1,4,5,1,5};
UINT4 FsQoSQShapeId [ ] ={1,3,6,1,4,1,29601,2,6,1,4,5,1,6};
UINT4 FsQoSQStatus [ ] ={1,3,6,1,4,1,29601,2,6,1,4,5,1,7};
UINT4 FsQoSQType [ ] ={1,3,6,1,4,1,29601,2,6,1,4,5,1,8};
UINT4 FsQoSSchedulerId [ ] ={1,3,6,1,4,1,29601,2,6,1,4,6,1,1};
UINT4 FsQoSSchedulerSchedAlgorithm [ ] ={1,3,6,1,4,1,29601,2,6,1,4,6,1,2};
UINT4 FsQoSSchedulerShapeId [ ] ={1,3,6,1,4,1,29601,2,6,1,4,6,1,3};
UINT4 FsQoSSchedulerHierarchyLevel [ ] ={1,3,6,1,4,1,29601,2,6,1,4,6,1,4};
UINT4 FsQoSSchedulerGlobalId [ ] ={1,3,6,1,4,1,29601,2,6,1,4,6,1,5};
UINT4 FsQoSSchedulerStatus [ ] ={1,3,6,1,4,1,29601,2,6,1,4,6,1,6};
UINT4 FsQoSHierarchyLevel [ ] ={1,3,6,1,4,1,29601,2,6,1,4,7,1,1};
UINT4 FsQoSHierarchyQNext [ ] ={1,3,6,1,4,1,29601,2,6,1,4,7,1,2};
UINT4 FsQoSHierarchySchedNext [ ] ={1,3,6,1,4,1,29601,2,6,1,4,7,1,3};
UINT4 FsQoSHierarchyWeight [ ] ={1,3,6,1,4,1,29601,2,6,1,4,7,1,4};
UINT4 FsQoSHierarchyPriority [ ] ={1,3,6,1,4,1,29601,2,6,1,4,7,1,5};
UINT4 FsQoSHierarchyStatus [ ] ={1,3,6,1,4,1,29601,2,6,1,4,7,1,6};
UINT4 FsQoSPortDefaultUserPriority [ ] ={1,3,6,1,4,1,29601,2,6,1,4,8,1,1};
UINT4 FsQoSPortPbitPrefOverDscp [ ] ={1,3,6,1,4,1,29601,2,6,1,4,8,1,2};
UINT4 FsQoSPolicerStatsConformPkts [ ] ={1,3,6,1,4,1,29601,2,6,1,5,1,1,1};
UINT4 FsQoSPolicerStatsConformOctets [ ] ={1,3,6,1,4,1,29601,2,6,1,5,1,1,2};
UINT4 FsQoSPolicerStatsExceedPkts [ ] ={1,3,6,1,4,1,29601,2,6,1,5,1,1,3};
UINT4 FsQoSPolicerStatsExceedOctets [ ] ={1,3,6,1,4,1,29601,2,6,1,5,1,1,4};
UINT4 FsQoSPolicerStatsViolatePkts [ ] ={1,3,6,1,4,1,29601,2,6,1,5,1,1,5};
UINT4 FsQoSPolicerStatsViolateOctets [ ] ={1,3,6,1,4,1,29601,2,6,1,5,1,1,6};
UINT4 FsQoSPolicerStatsStatus [ ] ={1,3,6,1,4,1,29601,2,6,1,5,1,1,7};
UINT4 FsQoSPolicerStatsClearCounter [ ] ={1,3,6,1,4,1,29601,2,6,1,5,1,1,8};
UINT4 FsQoSCoSQId [ ] ={1,3,6,1,4,1,29601,2,6,1,5,2,1,1};
UINT4 FsQoSCoSQStatsEnQPkts [ ] ={1,3,6,1,4,1,29601,2,6,1,5,2,1,2};
UINT4 FsQoSCoSQStatsEnQBytes [ ] ={1,3,6,1,4,1,29601,2,6,1,5,2,1,3};
UINT4 FsQoSCoSQStatsDeQPkts [ ] ={1,3,6,1,4,1,29601,2,6,1,5,2,1,4};
UINT4 FsQoSCoSQStatsDeQBytes [ ] ={1,3,6,1,4,1,29601,2,6,1,5,2,1,5};
UINT4 FsQoSCoSQStatsDiscardPkts [ ] ={1,3,6,1,4,1,29601,2,6,1,5,2,1,6};
UINT4 FsQoSCoSQStatsDiscardBytes [ ] ={1,3,6,1,4,1,29601,2,6,1,5,2,1,7};
UINT4 FsQoSCoSQStatsOccupancy [ ] ={1,3,6,1,4,1,29601,2,6,1,5,2,1,8};
UINT4 FsQoSCoSQStatsCongMgntAlgoDrop [ ] ={1,3,6,1,4,1,29601,2,6,1,5,2,1,9};
UINT4 FsQosHwCpuQId [ ] ={1,3,6,1,4,1,29601,2,6,1,6,1,1,1};
UINT4 FsQosHwCpuMinRate [ ] ={1,3,6,1,4,1,29601,2,6,1,6,1,1,2};
UINT4 FsQosHwCpuMaxRate [ ] ={1,3,6,1,4,1,29601,2,6,1,6,1,1,3};
UINT4 FsQosHwCpuRowStatus [ ] ={1,3,6,1,4,1,29601,2,6,1,6,1,1,4};
UINT4 FsQoSVlanMapID [ ] ={1,3,6,1,4,1,29601,2,6,1,2,4,1,1};
UINT4 FsQoSVlanMapIfIndex [ ] ={1,3,6,1,4,1,29601,2,6,1,2,4,1,2};
UINT4 FsQoSVlanMapVlanId [ ] ={1,3,6,1,4,1,29601,2,6,1,2,4,1,3};
UINT4 FsQoSVlanMapStatus [ ] ={1,3,6,1,4,1,29601,2,6,1,2,4,1,4};
UINT4 FsQoSPcpPacketType [ ] ={1,3,6,1,4,1,29601,2,6,1,2,5,1,1};
UINT4 FsQoSPcpSelRow [ ] ={1,3,6,1,4,1,29601,2,6,1,2,5,1,2};
UINT4 FsQoSPcpRowStatus [ ] ={1,3,6,1,4,1,29601,2,6,1,2,5,1,3};





tMbDbEntry fsqosxMibEntry[]= {

{{12,FsQoSSystemControl}, NULL, FsQoSSystemControlGet, FsQoSSystemControlSet, FsQoSSystemControlTest, FsQoSSystemControlDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{12,FsQoSStatus}, NULL, FsQoSStatusGet, FsQoSStatusSet, FsQoSStatusTest, FsQoSStatusDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "1"},

{{12,FsQoSTrcFlag}, NULL, FsQoSTrcFlagGet, FsQoSTrcFlagSet, FsQoSTrcFlagTest, FsQoSTrcFlagDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{12,FsQoSRateUnit}, NULL, FsQoSRateUnitGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{12,FsQoSRateGranularity}, NULL, FsQoSRateGranularityGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{12,FsQoSVlanQueueingStatus}, NULL, FsQoSVlanQueueingStatusGet, FsQoSVlanQueueingStatusSet, FsQoSVlanQueueingStatusTest, FsQoSVlanQueueingStatusDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "0"},

{{14,FsQoSPriorityMapID}, GetNextIndexFsQoSPriorityMapTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, FsQoSPriorityMapTableINDEX, 1, 0, 0, NULL},

{{14,FsQoSPriorityMapName}, GetNextIndexFsQoSPriorityMapTable, FsQoSPriorityMapNameGet, FsQoSPriorityMapNameSet, FsQoSPriorityMapNameTest, FsQoSPriorityMapTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsQoSPriorityMapTableINDEX, 1, 0, 0, NULL},

{{14,FsQoSPriorityMapIfIndex}, GetNextIndexFsQoSPriorityMapTable, FsQoSPriorityMapIfIndexGet, FsQoSPriorityMapIfIndexSet, FsQoSPriorityMapIfIndexTest, FsQoSPriorityMapTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsQoSPriorityMapTableINDEX, 1, 0, 0, "0"},

{{14,FsQoSPriorityMapVlanId}, GetNextIndexFsQoSPriorityMapTable, FsQoSPriorityMapVlanIdGet, FsQoSPriorityMapVlanIdSet, FsQoSPriorityMapVlanIdTest, FsQoSPriorityMapTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsQoSPriorityMapTableINDEX, 1, 0, 0, "0"},

{{14,FsQoSPriorityMapInPriority}, GetNextIndexFsQoSPriorityMapTable, FsQoSPriorityMapInPriorityGet, FsQoSPriorityMapInPrioritySet, FsQoSPriorityMapInPriorityTest, FsQoSPriorityMapTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsQoSPriorityMapTableINDEX, 1, 0, 0, "0"},

{{14,FsQoSPriorityMapInPriType}, GetNextIndexFsQoSPriorityMapTable, FsQoSPriorityMapInPriTypeGet, FsQoSPriorityMapInPriTypeSet, FsQoSPriorityMapInPriTypeTest, FsQoSPriorityMapTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsQoSPriorityMapTableINDEX, 1, 0, 0, "0"},

{{14,FsQoSPriorityMapRegenPriority}, GetNextIndexFsQoSPriorityMapTable, FsQoSPriorityMapRegenPriorityGet, FsQoSPriorityMapRegenPrioritySet, FsQoSPriorityMapRegenPriorityTest, FsQoSPriorityMapTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsQoSPriorityMapTableINDEX, 1, 0, 0, "0"},

{{14,FsQoSPriorityMapRegenInnerPriority}, GetNextIndexFsQoSPriorityMapTable, FsQoSPriorityMapRegenInnerPriorityGet, FsQoSPriorityMapRegenInnerPrioritySet, FsQoSPriorityMapRegenInnerPriorityTest, FsQoSPriorityMapTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsQoSPriorityMapTableINDEX, 1, 0, 0, NULL},

{{14,FsQoSPriorityMapConfigStatus}, GetNextIndexFsQoSPriorityMapTable, FsQoSPriorityMapConfigStatusGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsQoSPriorityMapTableINDEX, 1, 0, 0, "1"},

{{14,FsQoSPriorityMapStatus}, GetNextIndexFsQoSPriorityMapTable, FsQoSPriorityMapStatusGet, FsQoSPriorityMapStatusSet, FsQoSPriorityMapStatusTest, FsQoSPriorityMapTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsQoSPriorityMapTableINDEX, 1, 0, 1, NULL},

{{14,FsQoSPriorityMapInDEI}, GetNextIndexFsQoSPriorityMapTable, FsQoSPriorityMapInDEIGet, FsQoSPriorityMapInDEISet, FsQoSPriorityMapInDEITest, FsQoSPriorityMapTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsQoSPriorityMapTableINDEX, 1, 0, 0, "0"},

{{14,FsQoSPriorityMapRegenColor}, GetNextIndexFsQoSPriorityMapTable, FsQoSPriorityMapRegenColorGet, FsQoSPriorityMapRegenColorSet, FsQoSPriorityMapRegenColorTest, FsQoSPriorityMapTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsQoSPriorityMapTableINDEX, 1, 0, 0, "0"},
{{14,FsQoSClassMapId}, GetNextIndexFsQoSClassMapTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, FsQoSClassMapTableINDEX, 1, 0, 0, NULL},

{{14,FsQoSClassMapName}, GetNextIndexFsQoSClassMapTable, FsQoSClassMapNameGet, FsQoSClassMapNameSet, FsQoSClassMapNameTest, FsQoSClassMapTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsQoSClassMapTableINDEX, 1, 0, 0, NULL},

{{14,FsQoSClassMapL2FilterId}, GetNextIndexFsQoSClassMapTable, FsQoSClassMapL2FilterIdGet, FsQoSClassMapL2FilterIdSet, FsQoSClassMapL2FilterIdTest, FsQoSClassMapTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsQoSClassMapTableINDEX, 1, 0, 0, "0"},

{{14,FsQoSClassMapL3FilterId}, GetNextIndexFsQoSClassMapTable, FsQoSClassMapL3FilterIdGet, FsQoSClassMapL3FilterIdSet, FsQoSClassMapL3FilterIdTest, FsQoSClassMapTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsQoSClassMapTableINDEX, 1, 0, 0, "0"},

{{14,FsQoSClassMapPriorityMapId}, GetNextIndexFsQoSClassMapTable, FsQoSClassMapPriorityMapIdGet, FsQoSClassMapPriorityMapIdSet, FsQoSClassMapPriorityMapIdTest, FsQoSClassMapTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsQoSClassMapTableINDEX, 1, 0, 0, "0"},

{{14,FsQoSClassMapCLASS}, GetNextIndexFsQoSClassMapTable, FsQoSClassMapCLASSGet, FsQoSClassMapCLASSSet, FsQoSClassMapCLASSTest, FsQoSClassMapTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsQoSClassMapTableINDEX, 1, 0, 0, "0"},

{{14,FsQoSClassMapClfrId}, GetNextIndexFsQoSClassMapTable, FsQoSClassMapClfrIdGet, FsQoSClassMapClfrIdSet, FsQoSClassMapClfrIdTest, FsQoSClassMapTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsQoSClassMapTableINDEX, 1, 0, 0, "0"},

{{14,FsQoSClassMapPreColor}, GetNextIndexFsQoSClassMapTable, FsQoSClassMapPreColorGet, FsQoSClassMapPreColorSet, FsQoSClassMapPreColorTest, FsQoSClassMapTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsQoSClassMapTableINDEX, 1, 0, 0, NULL},

{{14,FsQoSClassMapStatus}, GetNextIndexFsQoSClassMapTable, FsQoSClassMapStatusGet, FsQoSClassMapStatusSet, FsQoSClassMapStatusTest, FsQoSClassMapTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsQoSClassMapTableINDEX, 1, 0, 1, NULL},

{{14,FsQoSClassMapVlanMapId}, GetNextIndexFsQoSClassMapTable, FsQoSClassMapVlanMapIdGet, FsQoSClassMapVlanMapIdSet, FsQoSClassMapVlanMapIdTest, FsQoSClassMapTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsQoSClassMapTableINDEX, 1, 0, 0, NULL},

{{14,FsQoSClassToPriorityCLASS}, GetNextIndexFsQoSClassToPriorityTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, FsQoSClassToPriorityTableINDEX, 1, 0, 0, NULL},

{{14,FsQoSClassToPriorityRegenPri}, GetNextIndexFsQoSClassToPriorityTable, FsQoSClassToPriorityRegenPriGet, FsQoSClassToPriorityRegenPriSet, FsQoSClassToPriorityRegenPriTest, FsQoSClassToPriorityTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsQoSClassToPriorityTableINDEX, 1, 0, 0, NULL},

{{14,FsQoSClassToPriorityGroupName}, GetNextIndexFsQoSClassToPriorityTable, FsQoSClassToPriorityGroupNameGet, FsQoSClassToPriorityGroupNameSet, FsQoSClassToPriorityGroupNameTest, FsQoSClassToPriorityTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsQoSClassToPriorityTableINDEX, 1, 0, 0, NULL},

{{14,FsQoSClassToPriorityStatus}, GetNextIndexFsQoSClassToPriorityTable, FsQoSClassToPriorityStatusGet, FsQoSClassToPriorityStatusSet, FsQoSClassToPriorityStatusTest, FsQoSClassToPriorityTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsQoSClassToPriorityTableINDEX, 1, 0, 1, NULL},

{{14,FsQoSVlanMapID}, GetNextIndexFsQoSVlanMapTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, FsQoSVlanMapTableINDEX, 1, 0, 0, NULL},

{{14,FsQoSVlanMapIfIndex}, GetNextIndexFsQoSVlanMapTable, FsQoSVlanMapIfIndexGet, FsQoSVlanMapIfIndexSet, FsQoSVlanMapIfIndexTest, FsQoSVlanMapTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsQoSVlanMapTableINDEX, 1, 0, 0, "0"},

{{14,FsQoSVlanMapVlanId}, GetNextIndexFsQoSVlanMapTable, FsQoSVlanMapVlanIdGet, FsQoSVlanMapVlanIdSet, FsQoSVlanMapVlanIdTest, FsQoSVlanMapTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsQoSVlanMapTableINDEX, 1, 0, 0, "0"},

{{14,FsQoSVlanMapStatus}, GetNextIndexFsQoSVlanMapTable, FsQoSVlanMapStatusGet, FsQoSVlanMapStatusSet, FsQoSVlanMapStatusTest, FsQoSVlanMapTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsQoSVlanMapTableINDEX, 1, 0, 1, NULL},

{{14,FsQoSPcpPacketType}, GetNextIndexFsQoSPortTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsQoSPortTableINDEX, 2, 0, 0, NULL},

{{14,FsQoSPcpSelRow}, GetNextIndexFsQoSPortTable, FsQoSPcpSelRowGet, FsQoSPcpSelRowSet, FsQoSPcpSelRowTest, FsQoSPortTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsQoSPortTableINDEX, 2, 0, 0, NULL},

{{14,FsQoSPcpRowStatus}, GetNextIndexFsQoSPortTable, FsQoSPcpRowStatusGet, FsQoSPcpRowStatusSet, FsQoSPcpRowStatusTest, FsQoSPortTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsQoSPortTableINDEX, 2, 0, 1, NULL},

{{14,FsQoSMeterId}, GetNextIndexFsQoSMeterTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, FsQoSMeterTableINDEX, 1, 0, 0, NULL},

{{14,FsQoSMeterName}, GetNextIndexFsQoSMeterTable, FsQoSMeterNameGet, FsQoSMeterNameSet, FsQoSMeterNameTest, FsQoSMeterTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsQoSMeterTableINDEX, 1, 0, 0, NULL},

{{14,FsQoSMeterType}, GetNextIndexFsQoSMeterTable, FsQoSMeterTypeGet, FsQoSMeterTypeSet, FsQoSMeterTypeTest, FsQoSMeterTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsQoSMeterTableINDEX, 1, 0, 0, NULL},

{{14,FsQoSMeterInterval}, GetNextIndexFsQoSMeterTable, FsQoSMeterIntervalGet, FsQoSMeterIntervalSet, FsQoSMeterIntervalTest, FsQoSMeterTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsQoSMeterTableINDEX, 1, 0, 0, "0"},

{{14,FsQoSMeterColorMode}, GetNextIndexFsQoSMeterTable, FsQoSMeterColorModeGet, FsQoSMeterColorModeSet, FsQoSMeterColorModeTest, FsQoSMeterTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsQoSMeterTableINDEX, 1, 0, 0, "2"},

{{14,FsQoSMeterCIR}, GetNextIndexFsQoSMeterTable, FsQoSMeterCIRGet, FsQoSMeterCIRSet, FsQoSMeterCIRTest, FsQoSMeterTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsQoSMeterTableINDEX, 1, 0, 0, NULL},

{{14,FsQoSMeterCBS}, GetNextIndexFsQoSMeterTable, FsQoSMeterCBSGet, FsQoSMeterCBSSet, FsQoSMeterCBSTest, FsQoSMeterTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsQoSMeterTableINDEX, 1, 0, 0, NULL},

{{14,FsQoSMeterEIR}, GetNextIndexFsQoSMeterTable, FsQoSMeterEIRGet, FsQoSMeterEIRSet, FsQoSMeterEIRTest, FsQoSMeterTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsQoSMeterTableINDEX, 1, 0, 0, NULL},

{{14,FsQoSMeterEBS}, GetNextIndexFsQoSMeterTable, FsQoSMeterEBSGet, FsQoSMeterEBSSet, FsQoSMeterEBSTest, FsQoSMeterTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsQoSMeterTableINDEX, 1, 0, 0, NULL},

{{14,FsQoSMeterNext}, GetNextIndexFsQoSMeterTable, FsQoSMeterNextGet, FsQoSMeterNextSet, FsQoSMeterNextTest, FsQoSMeterTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsQoSMeterTableINDEX, 1, 0, 0, "0"},

{{14,FsQoSMeterStatus}, GetNextIndexFsQoSMeterTable, FsQoSMeterStatusGet, FsQoSMeterStatusSet, FsQoSMeterStatusTest, FsQoSMeterTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsQoSMeterTableINDEX, 1, 0, 1, NULL},

{{14,FsQoSPolicyMapId}, GetNextIndexFsQoSPolicyMapTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, FsQoSPolicyMapTableINDEX, 1, 0, 0, NULL},

{{14,FsQoSPolicyMapName}, GetNextIndexFsQoSPolicyMapTable, FsQoSPolicyMapNameGet, FsQoSPolicyMapNameSet, FsQoSPolicyMapNameTest, FsQoSPolicyMapTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsQoSPolicyMapTableINDEX, 1, 0, 0, NULL},

{{14,FsQoSPolicyMapIfIndex}, GetNextIndexFsQoSPolicyMapTable, FsQoSPolicyMapIfIndexGet, FsQoSPolicyMapIfIndexSet, FsQoSPolicyMapIfIndexTest, FsQoSPolicyMapTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsQoSPolicyMapTableINDEX, 1, 0, 0, "0"},

{{14,FsQoSPolicyMapCLASS}, GetNextIndexFsQoSPolicyMapTable, FsQoSPolicyMapCLASSGet, FsQoSPolicyMapCLASSSet, FsQoSPolicyMapCLASSTest, FsQoSPolicyMapTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsQoSPolicyMapTableINDEX, 1, 0, 0, "0"},

{{14,FsQoSPolicyMapPHBType}, GetNextIndexFsQoSPolicyMapTable, FsQoSPolicyMapPHBTypeGet, FsQoSPolicyMapPHBTypeSet, FsQoSPolicyMapPHBTypeTest, FsQoSPolicyMapTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsQoSPolicyMapTableINDEX, 1, 0, 0, NULL},

{{14,FsQoSPolicyMapDefaultPHB}, GetNextIndexFsQoSPolicyMapTable, FsQoSPolicyMapDefaultPHBGet, FsQoSPolicyMapDefaultPHBSet, FsQoSPolicyMapDefaultPHBTest, FsQoSPolicyMapTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsQoSPolicyMapTableINDEX, 1, 0, 0, NULL},

{{14,FsQoSPolicyMapMeterTableId}, GetNextIndexFsQoSPolicyMapTable, FsQoSPolicyMapMeterTableIdGet, FsQoSPolicyMapMeterTableIdSet, FsQoSPolicyMapMeterTableIdTest, FsQoSPolicyMapTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsQoSPolicyMapTableINDEX, 1, 0, 0, "0"},

{{14,FsQoSPolicyMapFailMeterTableId}, GetNextIndexFsQoSPolicyMapTable, FsQoSPolicyMapFailMeterTableIdGet, FsQoSPolicyMapFailMeterTableIdSet, FsQoSPolicyMapFailMeterTableIdTest, FsQoSPolicyMapTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsQoSPolicyMapTableINDEX, 1, 0, 0, "0"},

{{14,FsQoSPolicyMapInProfileConformActionFlag}, GetNextIndexFsQoSPolicyMapTable, FsQoSPolicyMapInProfileConformActionFlagGet, FsQoSPolicyMapInProfileConformActionFlagSet, FsQoSPolicyMapInProfileConformActionFlagTest, FsQoSPolicyMapTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsQoSPolicyMapTableINDEX, 1, 0, 0, NULL},

{{14,FsQoSPolicyMapInProfileConformActionId}, GetNextIndexFsQoSPolicyMapTable, FsQoSPolicyMapInProfileConformActionIdGet, FsQoSPolicyMapInProfileConformActionIdSet, FsQoSPolicyMapInProfileConformActionIdTest, FsQoSPolicyMapTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsQoSPolicyMapTableINDEX, 1, 0, 0, "0"},

{{14,FsQoSPolicyMapInProfileActionSetPort}, GetNextIndexFsQoSPolicyMapTable, FsQoSPolicyMapInProfileActionSetPortGet, FsQoSPolicyMapInProfileActionSetPortSet, FsQoSPolicyMapInProfileActionSetPortTest, FsQoSPolicyMapTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsQoSPolicyMapTableINDEX, 1, 0, 0, NULL},

{{14,FsQoSPolicyMapConformActionSetIpTOS}, GetNextIndexFsQoSPolicyMapTable, FsQoSPolicyMapConformActionSetIpTOSGet, FsQoSPolicyMapConformActionSetIpTOSSet, FsQoSPolicyMapConformActionSetIpTOSTest, FsQoSPolicyMapTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsQoSPolicyMapTableINDEX, 1, 0, 0, NULL},

{{14,FsQoSPolicyMapConformActionSetDscp}, GetNextIndexFsQoSPolicyMapTable, FsQoSPolicyMapConformActionSetDscpGet, FsQoSPolicyMapConformActionSetDscpSet, FsQoSPolicyMapConformActionSetDscpTest, FsQoSPolicyMapTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsQoSPolicyMapTableINDEX, 1, 0, 0, NULL},

{{14,FsQoSPolicyMapConformActionSetVlanPrio}, GetNextIndexFsQoSPolicyMapTable, FsQoSPolicyMapConformActionSetVlanPrioGet, FsQoSPolicyMapConformActionSetVlanPrioSet, FsQoSPolicyMapConformActionSetVlanPrioTest, FsQoSPolicyMapTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsQoSPolicyMapTableINDEX, 1, 0, 0, "0"},

{{14,FsQoSPolicyMapConformActionSetVlanDE}, GetNextIndexFsQoSPolicyMapTable, FsQoSPolicyMapConformActionSetVlanDEGet, FsQoSPolicyMapConformActionSetVlanDESet, FsQoSPolicyMapConformActionSetVlanDETest, FsQoSPolicyMapTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsQoSPolicyMapTableINDEX, 1, 0, 0, "0"},

{{14,FsQoSPolicyMapConformActionSetInnerVlanPrio}, GetNextIndexFsQoSPolicyMapTable, FsQoSPolicyMapConformActionSetInnerVlanPrioGet, FsQoSPolicyMapConformActionSetInnerVlanPrioSet, FsQoSPolicyMapConformActionSetInnerVlanPrioTest, FsQoSPolicyMapTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsQoSPolicyMapTableINDEX, 1, 0, 0, "0"},

{{14,FsQoSPolicyMapConformActionSetMplsExp}, GetNextIndexFsQoSPolicyMapTable, FsQoSPolicyMapConformActionSetMplsExpGet, FsQoSPolicyMapConformActionSetMplsExpSet, FsQoSPolicyMapConformActionSetMplsExpTest, FsQoSPolicyMapTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsQoSPolicyMapTableINDEX, 1, 0, 0, "0"},

{{14,FsQoSPolicyMapConformActionSetNewCLASS}, GetNextIndexFsQoSPolicyMapTable, FsQoSPolicyMapConformActionSetNewCLASSGet, FsQoSPolicyMapConformActionSetNewCLASSSet, FsQoSPolicyMapConformActionSetNewCLASSTest, FsQoSPolicyMapTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsQoSPolicyMapTableINDEX, 1, 0, 0, NULL},

{{14,FsQoSPolicyMapInProfileExceedActionFlag}, GetNextIndexFsQoSPolicyMapTable, FsQoSPolicyMapInProfileExceedActionFlagGet, FsQoSPolicyMapInProfileExceedActionFlagSet, FsQoSPolicyMapInProfileExceedActionFlagTest, FsQoSPolicyMapTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsQoSPolicyMapTableINDEX, 1, 0, 0, NULL},

{{14,FsQoSPolicyMapInProfileExceedActionId}, GetNextIndexFsQoSPolicyMapTable, FsQoSPolicyMapInProfileExceedActionIdGet, FsQoSPolicyMapInProfileExceedActionIdSet, FsQoSPolicyMapInProfileExceedActionIdTest, FsQoSPolicyMapTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsQoSPolicyMapTableINDEX, 1, 0, 0, "0"},

{{14,FsQoSPolicyMapExceedActionSetIpTOS}, GetNextIndexFsQoSPolicyMapTable, FsQoSPolicyMapExceedActionSetIpTOSGet, FsQoSPolicyMapExceedActionSetIpTOSSet, FsQoSPolicyMapExceedActionSetIpTOSTest, FsQoSPolicyMapTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsQoSPolicyMapTableINDEX, 1, 0, 0, NULL},

{{14,FsQoSPolicyMapExceedActionSetDscp}, GetNextIndexFsQoSPolicyMapTable, FsQoSPolicyMapExceedActionSetDscpGet, FsQoSPolicyMapExceedActionSetDscpSet, FsQoSPolicyMapExceedActionSetDscpTest, FsQoSPolicyMapTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsQoSPolicyMapTableINDEX, 1, 0, 0, NULL},

{{14,FsQoSPolicyMapExceedActionSetInnerVlanPrio}, GetNextIndexFsQoSPolicyMapTable, FsQoSPolicyMapExceedActionSetInnerVlanPrioGet, FsQoSPolicyMapExceedActionSetInnerVlanPrioSet, FsQoSPolicyMapExceedActionSetInnerVlanPrioTest, FsQoSPolicyMapTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsQoSPolicyMapTableINDEX, 1, 0, 0, "0"},

{{14,FsQoSPolicyMapExceedActionSetVlanPrio}, GetNextIndexFsQoSPolicyMapTable, FsQoSPolicyMapExceedActionSetVlanPrioGet, FsQoSPolicyMapExceedActionSetVlanPrioSet, FsQoSPolicyMapExceedActionSetVlanPrioTest, FsQoSPolicyMapTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsQoSPolicyMapTableINDEX, 1, 0, 0, "0"},

{{14,FsQoSPolicyMapExceedActionSetVlanDE}, GetNextIndexFsQoSPolicyMapTable, FsQoSPolicyMapExceedActionSetVlanDEGet, FsQoSPolicyMapExceedActionSetVlanDESet, FsQoSPolicyMapExceedActionSetVlanDETest, FsQoSPolicyMapTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsQoSPolicyMapTableINDEX, 1, 0, 0, "0"},

{{14,FsQoSPolicyMapExceedActionSetMplsExp}, GetNextIndexFsQoSPolicyMapTable, FsQoSPolicyMapExceedActionSetMplsExpGet, FsQoSPolicyMapExceedActionSetMplsExpSet, FsQoSPolicyMapExceedActionSetMplsExpTest, FsQoSPolicyMapTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsQoSPolicyMapTableINDEX, 1, 0, 0, "0"},

{{14,FsQoSPolicyMapExceedActionSetNewCLASS}, GetNextIndexFsQoSPolicyMapTable, FsQoSPolicyMapExceedActionSetNewCLASSGet, FsQoSPolicyMapExceedActionSetNewCLASSSet, FsQoSPolicyMapExceedActionSetNewCLASSTest, FsQoSPolicyMapTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsQoSPolicyMapTableINDEX, 1, 0, 0, NULL},

{{14,FsQoSPolicyMapOutProfileActionFlag}, GetNextIndexFsQoSPolicyMapTable, FsQoSPolicyMapOutProfileActionFlagGet, FsQoSPolicyMapOutProfileActionFlagSet, FsQoSPolicyMapOutProfileActionFlagTest, FsQoSPolicyMapTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsQoSPolicyMapTableINDEX, 1, 0, 0, NULL},

{{14,FsQoSPolicyMapOutProfileActionId}, GetNextIndexFsQoSPolicyMapTable, FsQoSPolicyMapOutProfileActionIdGet, FsQoSPolicyMapOutProfileActionIdSet, FsQoSPolicyMapOutProfileActionIdTest, FsQoSPolicyMapTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsQoSPolicyMapTableINDEX, 1, 0, 0, "0"},

{{14,FsQoSPolicyMapOutProfileActionSetIPTOS}, GetNextIndexFsQoSPolicyMapTable, FsQoSPolicyMapOutProfileActionSetIPTOSGet, FsQoSPolicyMapOutProfileActionSetIPTOSSet, FsQoSPolicyMapOutProfileActionSetIPTOSTest, FsQoSPolicyMapTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsQoSPolicyMapTableINDEX, 1, 0, 0, NULL},

{{14,FsQoSPolicyMapOutProfileActionSetDscp}, GetNextIndexFsQoSPolicyMapTable, FsQoSPolicyMapOutProfileActionSetDscpGet, FsQoSPolicyMapOutProfileActionSetDscpSet, FsQoSPolicyMapOutProfileActionSetDscpTest, FsQoSPolicyMapTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsQoSPolicyMapTableINDEX, 1, 0, 0, NULL},

{{14,FsQoSPolicyMapOutProfileActionSetInnerVlanPrio}, GetNextIndexFsQoSPolicyMapTable, FsQoSPolicyMapOutProfileActionSetInnerVlanPrioGet, FsQoSPolicyMapOutProfileActionSetInnerVlanPrioSet, FsQoSPolicyMapOutProfileActionSetInnerVlanPrioTest, FsQoSPolicyMapTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsQoSPolicyMapTableINDEX, 1, 0, 0, "0"},

{{14,FsQoSPolicyMapOutProfileActionSetVlanPrio}, GetNextIndexFsQoSPolicyMapTable, FsQoSPolicyMapOutProfileActionSetVlanPrioGet, FsQoSPolicyMapOutProfileActionSetVlanPrioSet, FsQoSPolicyMapOutProfileActionSetVlanPrioTest, FsQoSPolicyMapTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsQoSPolicyMapTableINDEX, 1, 0, 0, "0"},

{{14,FsQoSPolicyMapOutProfileActionSetVlanDE}, GetNextIndexFsQoSPolicyMapTable, FsQoSPolicyMapOutProfileActionSetVlanDEGet, FsQoSPolicyMapOutProfileActionSetVlanDESet, FsQoSPolicyMapOutProfileActionSetVlanDETest, FsQoSPolicyMapTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsQoSPolicyMapTableINDEX, 1, 0, 0, "0"},

{{14,FsQoSPolicyMapOutProfileActionSetMplsExp}, GetNextIndexFsQoSPolicyMapTable, FsQoSPolicyMapOutProfileActionSetMplsExpGet, FsQoSPolicyMapOutProfileActionSetMplsExpSet, FsQoSPolicyMapOutProfileActionSetMplsExpTest, FsQoSPolicyMapTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsQoSPolicyMapTableINDEX, 1, 0, 0, "0"},

{{14,FsQoSPolicyMapOutProfileActionSetNewCLASS}, GetNextIndexFsQoSPolicyMapTable, FsQoSPolicyMapOutProfileActionSetNewCLASSGet, FsQoSPolicyMapOutProfileActionSetNewCLASSSet, FsQoSPolicyMapOutProfileActionSetNewCLASSTest, FsQoSPolicyMapTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsQoSPolicyMapTableINDEX, 1, 0, 0, NULL},

{{14,FsQoSPolicyMapStatus}, GetNextIndexFsQoSPolicyMapTable, FsQoSPolicyMapStatusGet, FsQoSPolicyMapStatusSet, FsQoSPolicyMapStatusTest, FsQoSPolicyMapTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsQoSPolicyMapTableINDEX, 1, 0, 1, NULL},

{{14,FsQoSPolicyMapConformActionSetInnerVlanDE}, GetNextIndexFsQoSPolicyMapTable, FsQoSPolicyMapConformActionSetInnerVlanDEGet, FsQoSPolicyMapConformActionSetInnerVlanDESet, FsQoSPolicyMapConformActionSetInnerVlanDETest, FsQoSPolicyMapTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsQoSPolicyMapTableINDEX, 1, 0, 0, "0"},

{{14,FsQoSPolicyMapExceedActionSetInnerVlanDE}, GetNextIndexFsQoSPolicyMapTable, FsQoSPolicyMapExceedActionSetInnerVlanDEGet, FsQoSPolicyMapExceedActionSetInnerVlanDESet, FsQoSPolicyMapExceedActionSetInnerVlanDETest, FsQoSPolicyMapTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsQoSPolicyMapTableINDEX, 1, 0, 0, "0"},

{{14,FsQoSPolicyMapOutProfileActionSetInnerVlanDE}, GetNextIndexFsQoSPolicyMapTable, FsQoSPolicyMapOutProfileActionSetInnerVlanDEGet, FsQoSPolicyMapOutProfileActionSetInnerVlanDESet, FsQoSPolicyMapOutProfileActionSetInnerVlanDETest, FsQoSPolicyMapTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsQoSPolicyMapTableINDEX, 1, 0, 0, "0"},

{{14,FsQoSPolicyDefaultVlanDE}, GetNextIndexFsQoSPolicyMapTable, FsQoSPolicyDefaultVlanDEGet, FsQoSPolicyDefaultVlanDESet, FsQoSPolicyDefaultVlanDETest, FsQoSPolicyMapTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsQoSPolicyMapTableINDEX, 1, 0, 0, "0"},
{{14,FsQoSQTemplateId}, GetNextIndexFsQoSQTemplateTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, FsQoSQTemplateTableINDEX, 1, 0, 0, NULL},

{{14,FsQoSQTemplateName}, GetNextIndexFsQoSQTemplateTable, FsQoSQTemplateNameGet, FsQoSQTemplateNameSet, FsQoSQTemplateNameTest, FsQoSQTemplateTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsQoSQTemplateTableINDEX, 1, 0, 0, NULL},

{{14,FsQoSQTemplateDropType}, GetNextIndexFsQoSQTemplateTable, FsQoSQTemplateDropTypeGet, FsQoSQTemplateDropTypeSet, FsQoSQTemplateDropTypeTest, FsQoSQTemplateTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsQoSQTemplateTableINDEX, 1, 0, 0, NULL},

{{14,FsQoSQTemplateDropAlgoEnableFlag}, GetNextIndexFsQoSQTemplateTable, FsQoSQTemplateDropAlgoEnableFlagGet, FsQoSQTemplateDropAlgoEnableFlagSet, FsQoSQTemplateDropAlgoEnableFlagTest, FsQoSQTemplateTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsQoSQTemplateTableINDEX, 1, 0, 0, "2"},

{{14,FsQoSQTemplateSize}, GetNextIndexFsQoSQTemplateTable, FsQoSQTemplateSizeGet, FsQoSQTemplateSizeSet, FsQoSQTemplateSizeTest, FsQoSQTemplateTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsQoSQTemplateTableINDEX, 1, 0, 0, NULL},

{{14,FsQoSQTemplateStatus}, GetNextIndexFsQoSQTemplateTable, FsQoSQTemplateStatusGet, FsQoSQTemplateStatusSet, FsQoSQTemplateStatusTest, FsQoSQTemplateTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsQoSQTemplateTableINDEX, 1, 0, 1, NULL},

{{14,FsQoSRandomDetectCfgDP}, GetNextIndexFsQoSRandomDetectCfgTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsQoSRandomDetectCfgTableINDEX, 2, 0, 0, NULL},

{{14,FsQoSRandomDetectCfgMinAvgThresh}, GetNextIndexFsQoSRandomDetectCfgTable, FsQoSRandomDetectCfgMinAvgThreshGet, FsQoSRandomDetectCfgMinAvgThreshSet, FsQoSRandomDetectCfgMinAvgThreshTest, FsQoSRandomDetectCfgTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsQoSRandomDetectCfgTableINDEX, 2, 0, 0, NULL},

{{14,FsQoSRandomDetectCfgMaxAvgThresh}, GetNextIndexFsQoSRandomDetectCfgTable, FsQoSRandomDetectCfgMaxAvgThreshGet, FsQoSRandomDetectCfgMaxAvgThreshSet, FsQoSRandomDetectCfgMaxAvgThreshTest, FsQoSRandomDetectCfgTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsQoSRandomDetectCfgTableINDEX, 2, 0, 0, NULL},

{{14,FsQoSRandomDetectCfgMaxPktSize}, GetNextIndexFsQoSRandomDetectCfgTable, FsQoSRandomDetectCfgMaxPktSizeGet, FsQoSRandomDetectCfgMaxPktSizeSet, FsQoSRandomDetectCfgMaxPktSizeTest, FsQoSRandomDetectCfgTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsQoSRandomDetectCfgTableINDEX, 2, 0, 0, NULL},

{{14,FsQoSRandomDetectCfgMaxProb}, GetNextIndexFsQoSRandomDetectCfgTable, FsQoSRandomDetectCfgMaxProbGet, FsQoSRandomDetectCfgMaxProbSet, FsQoSRandomDetectCfgMaxProbTest, FsQoSRandomDetectCfgTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsQoSRandomDetectCfgTableINDEX, 2, 0, 0, "100"},

{{14,FsQoSRandomDetectCfgExpWeight}, GetNextIndexFsQoSRandomDetectCfgTable, FsQoSRandomDetectCfgExpWeightGet, FsQoSRandomDetectCfgExpWeightSet, FsQoSRandomDetectCfgExpWeightTest, FsQoSRandomDetectCfgTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsQoSRandomDetectCfgTableINDEX, 2, 0, 0, "0"},

{{14,FsQoSRandomDetectCfgStatus}, GetNextIndexFsQoSRandomDetectCfgTable, FsQoSRandomDetectCfgStatusGet, FsQoSRandomDetectCfgStatusSet, FsQoSRandomDetectCfgStatusTest, FsQoSRandomDetectCfgTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsQoSRandomDetectCfgTableINDEX, 2, 0, 1, NULL},

{{14,FsQoSRandomDetectCfgGain}, GetNextIndexFsQoSRandomDetectCfgTable, FsQoSRandomDetectCfgGainGet, FsQoSRandomDetectCfgGainSet, FsQoSRandomDetectCfgGainTest, FsQoSRandomDetectCfgTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsQoSRandomDetectCfgTableINDEX, 2, 0, 0, NULL},

{{14,FsQoSRandomDetectCfgDropThreshType}, GetNextIndexFsQoSRandomDetectCfgTable, FsQoSRandomDetectCfgDropThreshTypeGet, FsQoSRandomDetectCfgDropThreshTypeSet, FsQoSRandomDetectCfgDropThreshTypeTest, FsQoSRandomDetectCfgTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsQoSRandomDetectCfgTableINDEX, 2, 0, 0, "1"},

{{14,FsQoSRandomDetectCfgECNThresh}, GetNextIndexFsQoSRandomDetectCfgTable, FsQoSRandomDetectCfgECNThreshGet, FsQoSRandomDetectCfgECNThreshSet, FsQoSRandomDetectCfgECNThreshTest, FsQoSRandomDetectCfgTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsQoSRandomDetectCfgTableINDEX, 2, 0, 0, NULL},

{{14,FsQoSRandomDetectCfgActionFlag}, GetNextIndexFsQoSRandomDetectCfgTable, FsQoSRandomDetectCfgActionFlagGet, FsQoSRandomDetectCfgActionFlagSet, FsQoSRandomDetectCfgActionFlagTest, FsQoSRandomDetectCfgTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsQoSRandomDetectCfgTableINDEX, 2, 0, 0, "0"},

{{14,FsQoSShapeTemplateId}, GetNextIndexFsQoSShapeTemplateTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, FsQoSShapeTemplateTableINDEX, 1, 0, 0, NULL},

{{14,FsQoSShapeTemplateName}, GetNextIndexFsQoSShapeTemplateTable, FsQoSShapeTemplateNameGet, FsQoSShapeTemplateNameSet, FsQoSShapeTemplateNameTest, FsQoSShapeTemplateTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsQoSShapeTemplateTableINDEX, 1, 0, 0, NULL},

{{14,FsQoSShapeTemplateCIR}, GetNextIndexFsQoSShapeTemplateTable, FsQoSShapeTemplateCIRGet, FsQoSShapeTemplateCIRSet, FsQoSShapeTemplateCIRTest, FsQoSShapeTemplateTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsQoSShapeTemplateTableINDEX, 1, 0, 0, NULL},

{{14,FsQoSShapeTemplateCBS}, GetNextIndexFsQoSShapeTemplateTable, FsQoSShapeTemplateCBSGet, FsQoSShapeTemplateCBSSet, FsQoSShapeTemplateCBSTest, FsQoSShapeTemplateTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsQoSShapeTemplateTableINDEX, 1, 0, 0, NULL},

{{14,FsQoSShapeTemplateEIR}, GetNextIndexFsQoSShapeTemplateTable, FsQoSShapeTemplateEIRGet, FsQoSShapeTemplateEIRSet, FsQoSShapeTemplateEIRTest, FsQoSShapeTemplateTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsQoSShapeTemplateTableINDEX, 1, 0, 0, NULL},

{{14,FsQoSShapeTemplateEBS}, GetNextIndexFsQoSShapeTemplateTable, FsQoSShapeTemplateEBSGet, FsQoSShapeTemplateEBSSet, FsQoSShapeTemplateEBSTest, FsQoSShapeTemplateTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsQoSShapeTemplateTableINDEX, 1, 0, 0, NULL},

{{14,FsQoSShapeTemplateStatus}, GetNextIndexFsQoSShapeTemplateTable, FsQoSShapeTemplateStatusGet, FsQoSShapeTemplateStatusSet, FsQoSShapeTemplateStatusTest, FsQoSShapeTemplateTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsQoSShapeTemplateTableINDEX, 1, 0, 1, NULL},

{{14,FsQoSQMapCLASS}, GetNextIndexFsQoSQMapTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, FsQoSQMapTableINDEX, 4, 0, 0, NULL},

{{14,FsQoSQMapRegenPriType}, GetNextIndexFsQoSQMapTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsQoSQMapTableINDEX, 4, 0, 0, NULL},

{{14,FsQoSQMapRegenPriority}, GetNextIndexFsQoSQMapTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, FsQoSQMapTableINDEX, 4, 0, 0, NULL},

{{14,FsQoSQMapQId}, GetNextIndexFsQoSQMapTable, FsQoSQMapQIdGet, FsQoSQMapQIdSet, FsQoSQMapQIdTest, FsQoSQMapTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsQoSQMapTableINDEX, 4, 0, 0, NULL},

{{14,FsQoSQMapStatus}, GetNextIndexFsQoSQMapTable, FsQoSQMapStatusGet, FsQoSQMapStatusSet, FsQoSQMapStatusTest, FsQoSQMapTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsQoSQMapTableINDEX, 4, 0, 1, NULL},
{{14,FsQoSQMapRegenDEI}, GetNextIndexFsQoSQMapTable, FsQoSQMapRegenDEIGet, FsQoSQMapRegenDEISet, FsQoSQMapRegenDEITest, FsQoSQMapTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsQoSQMapTableINDEX, 4, 0, 0, "0"},

{{14,FsQoSQMapRegenColor}, GetNextIndexFsQoSQMapTable, FsQoSQMapRegenColorGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsQoSQMapTableINDEX, 4, 0, 0, NULL},

{{14,FsQoSQId}, GetNextIndexFsQoSQTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, FsQoSQTableINDEX, 2, 0, 0, NULL},

{{14,FsQoSQCfgTemplateId}, GetNextIndexFsQoSQTable, FsQoSQCfgTemplateIdGet, FsQoSQCfgTemplateIdSet, FsQoSQCfgTemplateIdTest, FsQoSQTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsQoSQTableINDEX, 2, 0, 0, NULL},

{{14,FsQoSQSchedulerId}, GetNextIndexFsQoSQTable, FsQoSQSchedulerIdGet, FsQoSQSchedulerIdSet, FsQoSQSchedulerIdTest, FsQoSQTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsQoSQTableINDEX, 2, 0, 0, NULL},

{{14,FsQoSQWeight}, GetNextIndexFsQoSQTable, FsQoSQWeightGet, FsQoSQWeightSet, FsQoSQWeightTest, FsQoSQTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsQoSQTableINDEX, 2, 0, 0, "1"},

{{14,FsQoSQPriority}, GetNextIndexFsQoSQTable, FsQoSQPriorityGet, FsQoSQPrioritySet, FsQoSQPriorityTest, FsQoSQTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsQoSQTableINDEX, 2, 0, 0, "1"},

{{14,FsQoSQShapeId}, GetNextIndexFsQoSQTable, FsQoSQShapeIdGet, FsQoSQShapeIdSet, FsQoSQShapeIdTest, FsQoSQTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsQoSQTableINDEX, 2, 0, 0, NULL},

{{14,FsQoSQStatus}, GetNextIndexFsQoSQTable, FsQoSQStatusGet, FsQoSQStatusSet, FsQoSQStatusTest, FsQoSQTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsQoSQTableINDEX, 2, 0, 1, NULL},

{{14,FsQoSQType}, GetNextIndexFsQoSQTable, FsQoSQTypeGet, FsQoSQTypeSet, FsQoSQTypeTest, FsQoSQTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsQoSQTableINDEX, 2, 0, 0, "1"},

{{14,FsQoSSchedulerId}, GetNextIndexFsQoSSchedulerTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, FsQoSSchedulerTableINDEX, 2, 0, 0, NULL},

{{14,FsQoSSchedulerSchedAlgorithm}, GetNextIndexFsQoSSchedulerTable, FsQoSSchedulerSchedAlgorithmGet, FsQoSSchedulerSchedAlgorithmSet, FsQoSSchedulerSchedAlgorithmTest, FsQoSSchedulerTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsQoSSchedulerTableINDEX, 2, 0, 0, "1"},

{{14,FsQoSSchedulerShapeId}, GetNextIndexFsQoSSchedulerTable, FsQoSSchedulerShapeIdGet, FsQoSSchedulerShapeIdSet, FsQoSSchedulerShapeIdTest, FsQoSSchedulerTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsQoSSchedulerTableINDEX, 2, 0, 0, NULL},

{{14,FsQoSSchedulerHierarchyLevel}, GetNextIndexFsQoSSchedulerTable, FsQoSSchedulerHierarchyLevelGet, FsQoSSchedulerHierarchyLevelSet, FsQoSSchedulerHierarchyLevelTest, FsQoSSchedulerTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsQoSSchedulerTableINDEX, 2, 0, 0, "0"},

{{14,FsQoSSchedulerGlobalId}, GetNextIndexFsQoSSchedulerTable, FsQoSSchedulerGlobalIdGet, FsQoSSchedulerGlobalIdSet, FsQoSSchedulerGlobalIdTest, FsQoSSchedulerTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsQoSSchedulerTableINDEX, 2, 0, 0, NULL},

{{14,FsQoSSchedulerStatus}, GetNextIndexFsQoSSchedulerTable, FsQoSSchedulerStatusGet, FsQoSSchedulerStatusSet, FsQoSSchedulerStatusTest, FsQoSSchedulerTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsQoSSchedulerTableINDEX, 2, 0, 1, NULL},

{{14,FsQoSHierarchyLevel}, GetNextIndexFsQoSHierarchyTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, FsQoSHierarchyTableINDEX, 3, 0, 0, NULL},

{{14,FsQoSHierarchyQNext}, GetNextIndexFsQoSHierarchyTable, FsQoSHierarchyQNextGet, FsQoSHierarchyQNextSet, FsQoSHierarchyQNextTest, FsQoSHierarchyTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsQoSHierarchyTableINDEX, 3, 0, 0, NULL},

{{14,FsQoSHierarchySchedNext}, GetNextIndexFsQoSHierarchyTable, FsQoSHierarchySchedNextGet, FsQoSHierarchySchedNextSet, FsQoSHierarchySchedNextTest, FsQoSHierarchyTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsQoSHierarchyTableINDEX, 3, 0, 0, NULL},

{{14,FsQoSHierarchyWeight}, GetNextIndexFsQoSHierarchyTable, FsQoSHierarchyWeightGet, FsQoSHierarchyWeightSet, FsQoSHierarchyWeightTest, FsQoSHierarchyTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsQoSHierarchyTableINDEX, 3, 0, 0, NULL},

{{14,FsQoSHierarchyPriority}, GetNextIndexFsQoSHierarchyTable, FsQoSHierarchyPriorityGet, FsQoSHierarchyPrioritySet, FsQoSHierarchyPriorityTest, FsQoSHierarchyTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsQoSHierarchyTableINDEX, 3, 0, 0, "0"},

{{14,FsQoSHierarchyStatus}, GetNextIndexFsQoSHierarchyTable, FsQoSHierarchyStatusGet, FsQoSHierarchyStatusSet, FsQoSHierarchyStatusTest, FsQoSHierarchyTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsQoSHierarchyTableINDEX, 3, 0, 1, NULL},

{{14,FsQoSPortDefaultUserPriority}, GetNextIndexFsQoSDefUserPriorityTable, FsQoSPortDefaultUserPriorityGet, FsQoSPortDefaultUserPrioritySet, FsQoSPortDefaultUserPriorityTest, FsQoSDefUserPriorityTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsQoSDefUserPriorityTableINDEX, 1, 0, 0, NULL},

{{14,FsQoSPortPbitPrefOverDscp}, GetNextIndexFsQoSDefUserPriorityTable, FsQoSPortPbitPrefOverDscpGet, FsQoSPortPbitPrefOverDscpSet, FsQoSPortPbitPrefOverDscpTest, FsQoSDefUserPriorityTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsQoSDefUserPriorityTableINDEX, 1, 0, 0, "1"},

{{14,FsQoSPolicerStatsConformPkts}, GetNextIndexFsQoSPolicerStatsTable, FsQoSPolicerStatsConformPktsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER64, SNMP_READONLY, FsQoSPolicerStatsTableINDEX, 1, 0, 0, NULL},

{{14,FsQoSPolicerStatsConformOctets}, GetNextIndexFsQoSPolicerStatsTable, FsQoSPolicerStatsConformOctetsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER64, SNMP_READONLY, FsQoSPolicerStatsTableINDEX, 1, 0, 0, NULL},

{{14,FsQoSPolicerStatsExceedPkts}, GetNextIndexFsQoSPolicerStatsTable, FsQoSPolicerStatsExceedPktsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER64, SNMP_READONLY, FsQoSPolicerStatsTableINDEX, 1, 0, 0, NULL},

{{14,FsQoSPolicerStatsExceedOctets}, GetNextIndexFsQoSPolicerStatsTable, FsQoSPolicerStatsExceedOctetsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER64, SNMP_READONLY, FsQoSPolicerStatsTableINDEX, 1, 0, 0, NULL},

{{14,FsQoSPolicerStatsViolatePkts}, GetNextIndexFsQoSPolicerStatsTable, FsQoSPolicerStatsViolatePktsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER64, SNMP_READONLY, FsQoSPolicerStatsTableINDEX, 1, 0, 0, NULL},

{{14,FsQoSPolicerStatsViolateOctets}, GetNextIndexFsQoSPolicerStatsTable, FsQoSPolicerStatsViolateOctetsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER64, SNMP_READONLY, FsQoSPolicerStatsTableINDEX, 1, 0, 0, NULL},

{{14,FsQoSPolicerStatsStatus}, GetNextIndexFsQoSPolicerStatsTable, FsQoSPolicerStatsStatusGet, FsQoSPolicerStatsStatusSet, FsQoSPolicerStatsStatusTest, FsQoSPolicerStatsTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsQoSPolicerStatsTableINDEX, 1, 0, 0, "2"},

{{14,FsQoSPolicerStatsClearCounter}, GetNextIndexFsQoSPolicerStatsTable, FsQoSPolicerStatsClearCounterGet, FsQoSPolicerStatsClearCounterSet, FsQoSPolicerStatsClearCounterTest, FsQoSPolicerStatsTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsQoSPolicerStatsTableINDEX, 1, 0, 0, "2"},

{{14,FsQoSCoSQId}, GetNextIndexFsQoSCoSQStatsTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, FsQoSCoSQStatsTableINDEX, 2, 0, 0, NULL},

{{14,FsQoSCoSQStatsEnQPkts}, GetNextIndexFsQoSCoSQStatsTable, FsQoSCoSQStatsEnQPktsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER64, SNMP_READONLY, FsQoSCoSQStatsTableINDEX, 2, 0, 0, NULL},

{{14,FsQoSCoSQStatsEnQBytes}, GetNextIndexFsQoSCoSQStatsTable, FsQoSCoSQStatsEnQBytesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER64, SNMP_READONLY, FsQoSCoSQStatsTableINDEX, 2, 0, 0, NULL},

{{14,FsQoSCoSQStatsDeQPkts}, GetNextIndexFsQoSCoSQStatsTable, FsQoSCoSQStatsDeQPktsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER64, SNMP_READONLY, FsQoSCoSQStatsTableINDEX, 2, 0, 0, NULL},

{{14,FsQoSCoSQStatsDeQBytes}, GetNextIndexFsQoSCoSQStatsTable, FsQoSCoSQStatsDeQBytesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER64, SNMP_READONLY, FsQoSCoSQStatsTableINDEX, 2, 0, 0, NULL},

{{14,FsQoSCoSQStatsDiscardPkts}, GetNextIndexFsQoSCoSQStatsTable, FsQoSCoSQStatsDiscardPktsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER64, SNMP_READONLY, FsQoSCoSQStatsTableINDEX, 2, 0, 0, NULL},

{{14,FsQoSCoSQStatsDiscardBytes}, GetNextIndexFsQoSCoSQStatsTable, FsQoSCoSQStatsDiscardBytesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER64, SNMP_READONLY, FsQoSCoSQStatsTableINDEX, 2, 0, 0, NULL},

{{14,FsQoSCoSQStatsOccupancy}, GetNextIndexFsQoSCoSQStatsTable, FsQoSCoSQStatsOccupancyGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER64, SNMP_READONLY, FsQoSCoSQStatsTableINDEX, 2, 0, 0, NULL},

{{14,FsQoSCoSQStatsCongMgntAlgoDrop}, GetNextIndexFsQoSCoSQStatsTable, FsQoSCoSQStatsCongMgntAlgoDropGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER64, SNMP_READONLY, FsQoSCoSQStatsTableINDEX, 2, 0, 0, NULL},

{{14,FsQosHwCpuQId}, GetNextIndexFsQosHwCpuRateLimitTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, FsQosHwCpuRateLimitTableINDEX, 1, 0, 0, NULL},

{{14,FsQosHwCpuMinRate}, GetNextIndexFsQosHwCpuRateLimitTable, FsQosHwCpuMinRateGet, FsQosHwCpuMinRateSet, FsQosHwCpuMinRateTest, FsQosHwCpuRateLimitTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsQosHwCpuRateLimitTableINDEX, 1, 0, 0, NULL},

{{14,FsQosHwCpuMaxRate}, GetNextIndexFsQosHwCpuRateLimitTable, FsQosHwCpuMaxRateGet, FsQosHwCpuMaxRateSet, FsQosHwCpuMaxRateTest, FsQosHwCpuRateLimitTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsQosHwCpuRateLimitTableINDEX, 1, 0, 0, NULL},

{{14,FsQosHwCpuRowStatus}, GetNextIndexFsQosHwCpuRateLimitTable, FsQosHwCpuRowStatusGet, FsQosHwCpuRowStatusSet, FsQosHwCpuRowStatusTest, FsQosHwCpuRateLimitTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsQosHwCpuRateLimitTableINDEX, 1, 0, 1, NULL},
};
tMibData fsqosxEntry = { 157, fsqosxMibEntry };

#endif /* _FSQOSXDB_H */

