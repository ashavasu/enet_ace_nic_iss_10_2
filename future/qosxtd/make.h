#!/bin/csh

# $Id: make.h,v 1.6 2016/07/15 08:08:12 siva Exp $

# Copyright (C) 2006 Aricent Inc . All Rights Reserved
# +--------------------------------------------------------------------------+
# |   FILE  NAME             : make.h                                        |
# |                                                                          |
# |   PRINCIPAL AUTHOR       : Aricent Inc.                                  |
# |                                                                          |
# |   MAKE TOOL(S) USED      : GNU make                                      |
# |                                                                          |
# |   TARGET ENVIRONMENT     : LINUX                                         |
# |                                                                          |
# |   DATE                   : 30/01/2008                                    |
# |                                                                          |
# |   DESCRIPTION            : Specifies the options and modules to be       |
# |                            including for building the FutureISS          |
# |                            product.                                      |
# |                                                                          |
# +--------------------------------------------------------------------------+
#

# Set the PROJ_BASE_DIR as the directory where you untar the project files

QOS_SWITCHES += -UDEFAULT_PCP_WANTED
TOTAL_OPNS =  ${QOS_SWITCHES} $(GENERAL_COMPILATION_SWITCHES) $(SYSTEM_COMPILATION_SWITCHES)

PROJECT_NAME  = FutureQoSX
PROJECT_BASE_DIR = ${BASE_DIR}/qosxtd
PROJECT_SOURCE_DIR = ${PROJECT_BASE_DIR}/src
PROJECT_INCLUDE_DIR = ${PROJECT_BASE_DIR}/inc
PROJECT_OBJECT_DIR = ${PROJECT_BASE_DIR}/obj
FUTURE_INC_DIR  = $(BASE_DIR)/inc

# Specify the project include directories and dependencies
PROJECT_INCLUDE_FILES  = \
    $(PROJECT_INCLUDE_DIR)/fsqosxdb.h   \
    $(PROJECT_INCLUDE_DIR)/fsqosxlw.h   \
    $(PROJECT_INCLUDE_DIR)/fsqosxwr.h   \
    $(PROJECT_INCLUDE_DIR)/qosdefn.h    \
    $(PROJECT_INCLUDE_DIR)/qosglob.h    \
    $(PROJECT_INCLUDE_DIR)/qosinc.h     \
    $(PROJECT_INCLUDE_DIR)/qosproto.h   \
    $(PROJECT_INCLUDE_DIR)/qostdfs.h    \
    $(PROJECT_INCLUDE_DIR)/qossz.h      \
    $(PROJECT_INCLUDE_DIR)/qostrc.h


PROJECT_FINAL_INCLUDES_DIRS =  -I$(PROJECT_INCLUDE_DIR) \
     $(COMMON_INCLUDE_DIRS)\
    -I$(FUTURE_INC_DIR)    \
    -I$(ISS_EXT_DIR)/inc

PROJECT_FINAL_INCLUDE_FILES    +=  $(PROJECT_INCLUDE_FILES)

PROJECT_DEPENDENCIES =    $(COMMON_DEPENDENCIES)\
            $(PROJECT_FINAL_INCLUDE_FILES) \
    $(PROJECT_BASE_DIR)/Makefile \
    $(PROJECT_BASE_DIR)/make.h

