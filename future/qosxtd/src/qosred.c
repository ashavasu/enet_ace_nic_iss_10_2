/* $Id: qosred.c,v 1.19 2016/06/14 12:33:27 siva Exp $ */
/****************************************************************************/
/* Copyright (C) 2010 Aricent Inc . All Rights Reserved                     */
/*                                                                          */
/*  FILE NAME             : qosred.c                                        */
/*  PRINCIPAL AUTHOR      : Aricent                                         */
/*  SUBSYSTEM NAME        : QoS                                             */
/*  MODULE NAME           : QoS-RED                                         */
/*  LANGUAGE              : C                                               */
/*  TARGET ENVIRONMENT    : Linux                                           */
/*  DATE OF FIRST RELEASE :                                                 */
/*  AUTHOR                : Aricent                                         */
/*  FUNCTIONS DEFINED(Applicable for Source files) :                        */
/*  Description: This file contains the QOS Redundancy support routines and */
/* utility routines.                                                        */
/****************************************************************************/
/*                                                                          */
/*  Change History                                                          */
/*  Version               :                                                 */
/*  Date(DD/MM/YYYY)      :                                                 */
/*  Modified by           :                                                 */
/*  Description of change :                                                 */
/****************************************************************************/
#ifndef _QOSRED_C_
#define _QOSRED_C_

#include "qosinc.h"

/***************************************************************************
 * FUNCTION NAME    : QosRedHwAuditIncBlkCounter  
 *
 * DESCRIPTION      : This routine increments the NP Sync Block Counter by one.
 *                    If the value of this Block flag is not equal to 0, then
 *                    it is intended to block NP Sync-ups.
 *
 * INPUT            : None
 *
 * OUTPUT           : None
 * 
 * RETURNS          : VOID 
 * 
 **************************************************************************/
PUBLIC VOID
QosRedHwAuditIncBlkCounter (VOID)
{
    gQoSGlobalInfo.RedGlobalInfo.u1NpSyncBlockCount++;
    QOS_TRC_ARG1 (MGMT_TRC,
                  "QosRedHwAuditIncBlkCounter: "
                  "Incrementing the NP Sync Block Counter Value to %d \r\n",
                  gQoSGlobalInfo.RedGlobalInfo.u1NpSyncBlockCount);
    return;
}

/***************************************************************************
 * FUNCTION NAME    : QosRedHwAuditDecBlkCounter  
 *
 * DESCRIPTION      : This routine decrements the NP Sync Block Counter by one.
 *                    If the value of this Block flag is not equal to 0, then
 *                    it is intended to block NP Sync-ups. This routine also
 *                    checks if the value is lesser than 0, displayes critical
 *                    trace and sets the value to 0.
 *
 * INPUT            : None
 *
 * OUTPUT           : None
 * 
 * RETURNS          : VOID 
 * 
 **************************************************************************/
PUBLIC VOID
QosRedHwAuditDecBlkCounter (VOID)
{
    if (gQoSGlobalInfo.RedGlobalInfo.u1NpSyncBlockCount == 0)
    {
        QOS_TRC (MGMT_TRC,
                 "QosRedHwAuditDecBlkCounter: NP Sync Block Counter"
                 " Value is being decremented less than 0 "
                 "Resetting the NP Sync Block Counter Value to 0\r\n");
        return;
    }
    gQoSGlobalInfo.RedGlobalInfo.u1NpSyncBlockCount--;
    QOS_TRC_ARG1 (MGMT_TRC,
                  "QosRedHwAuditDecBlkCounter: "
                  "Decrementing the NP Sync Block Counter Value to %d \r\n",
                  gQoSGlobalInfo.RedGlobalInfo.u1NpSyncBlockCount);
    return;
}

/***************************************************************************
 * FUNCTION NAME    : QosRedInitGlobalInfo   
 *
 * DESCRIPTION        : Initializes redundancy global variables.This function 
 *                    will get invoked during BOOTUP. It registers with RM.
 *
 * INPUT            : None
 *
 * OUTPUT            : None
 * 
 * RETURNS            : OSIX_SUCCESS/OSIX_FAILURE 
 * 
 **************************************************************************/
PUBLIC INT4
QosRedInitGlobalInfo (VOID)
{
    tRmRegParams        RmRegParams;

    RmRegParams.u4EntId = RM_QOS_APP_ID;
    RmRegParams.pFnRcvPkt = QosRedRmCallBack;

    if (QOSRmRegisterProtocols (&RmRegParams) == OSIX_FAILURE)
    {
        QOS_TRC (INIT_SHUT_TRC | ALL_FAILURE_TRC, "QosRedInitGlobalInfo:"
                 " Registration with RM FAILED !!!\r\n");
        return OSIX_FAILURE;
    }
    /* Default value of Node Status is IDLE */
    gQoSGlobalInfo.RedGlobalInfo.u1NodeStatus = RM_INIT;
    gQoSGlobalInfo.RedGlobalInfo.u1NumOfStandbyNodesUp =
        QosPortRmGetStandbyNodeCount ();
    gQoSGlobalInfo.RedGlobalInfo.u1NpSyncBlockCount = QOS_INIT_VAL;

    /* NP-Syncups are not to be sent from Idle node. NPSync buffer Table 
     * should not be accessed when the node is Idle. Set the Count value to
     * 1 so that the Np sync table is blocked.
     */
    QosRedHwAuditIncBlkCounter ();

    /* Hardware Audit Np-Sync Buffer Mempool with the size as 15 */

    if (MemCreateMemPool (sizeof (tQosRedNpSyncEntry),
                          QOS_MAX_HW_NP_BUF_ENTRY, MEM_DEFAULT_MEMORY_TYPE,
                          &(gQoSGlobalInfo.RedGlobalInfo.HwAuditTablePoolId))
        == MEM_FAILURE)
    {
        QOS_TRC (BUFFER_TRC | ALL_FAILURE_TRC, "QosRedInitGlobalInfo : "
                 "Memory Pool Creation for NP Sync Buffer Entry Failed\n");
        QOSRmDeRegisterProtocols ();
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/******************************************************************************
 * FUNCTION NAME    : QosRedDeInitGlobalInfo  
 *
 * DESCRIPTION        : Deinitializes redundancy global variables.This function 
 *                      will get invoked during BOOTUP failure. Also De-registers
 *                    with RM.
 *
 * INPUT            : None
 *
 * OUTPUT              : None
 * 
 * RETURNS             : VOID 
 * 
 *****************************************************************************/
PUBLIC VOID
QosRedDeInitGlobalInfo (VOID)
{
    QOSRmDeRegisterProtocols ();
    if (gQoSGlobalInfo.RedGlobalInfo.HwAuditTablePoolId != QOS_INIT_VAL)
    {
        MemDeleteMemPool (gQoSGlobalInfo.RedGlobalInfo.HwAuditTablePoolId);
        gQoSGlobalInfo.RedGlobalInfo.HwAuditTablePoolId = QOS_INIT_VAL;
    }
    return;
}

/******************************************************************
 * FUNCTION NAME    : QosRedRmCallBack  
 *
 * DESCRIPTION      : This function will be invoked by the RM module to
 *                    enqueue events and post messages to QOS module.
 *
 * INPUT            : u1Event - Event given by RM module.
 *                    pData   - Msg to be enqueued, if u1Event is
 *                              valid.
 *                    u2DataLen - Size of the update message.
 *
 * OUTPUT           : None
 * 
 * RETURNS          : RM_SUCCESS/RM_FAILURE
 * 
 ******************************************************************/
PUBLIC INT4
QosRedRmCallBack (UINT1 u1Event, tRmMsg * pData, UINT2 u2DataLen)
{
    tQosQueueMsg       *pMsg = NULL;

    /* If the received event is not any of the following, then just return
     * without processing the event */
    if ((u1Event != RM_MESSAGE) &&
        (u1Event != GO_ACTIVE) &&
        (u1Event != GO_STANDBY) &&
        (u1Event != RM_STANDBY_UP) &&
        (u1Event != RM_STANDBY_DOWN) &&
        (u1Event != L2_INITIATE_BULK_UPDATES) &&
        (u1Event != RM_CONFIG_RESTORE_COMPLETE) &&
        (u1Event != RM_INIT_HW_AUDIT))
    {
        QOS_TRC (BUFFER_TRC | ALL_FAILURE_TRC,
                 "QosRedRmCallBack : This "
                 "Event is not associated with RM !!!\r\n");
        return OSIX_FAILURE;
    }

    if (((u1Event == RM_MESSAGE) || (u1Event == RM_STANDBY_UP) ||
         (u1Event == RM_STANDBY_DOWN)) && (pData == NULL))
    {
        /* Message absent and hence no need to process and no need
         * to send these events to QOS task. */
        QOS_TRC (BUFFER_TRC | ALL_FAILURE_TRC,
                 "QosRedRmCallBack : Queue Message "
                 "associated with Event is not sent by RM !!!\r\n");
        return OSIX_FAILURE;
    }

    if ((pMsg = (tQosQueueMsg *) QOS_QMSG_ALLOC_MEMBLK) == NULL)
    {
        if (u1Event == RM_MESSAGE)
        {
            /* RM CRU Buffer Memory Release */
            RM_FREE (pData);
        }
        else if ((u1Event == RM_STANDBY_UP) || (u1Event == RM_STANDBY_DOWN))
        {
            /* Call RM API to release memory of data of these events */
            QosRmReleaseMemoryForMsg ((UINT1 *) pData);
        }

        QOS_TRC (OS_RESOURCE_TRC,
                 "QosRedRmCallBack:Memory allocation failed for"
                 "queue message\r \n");
        return OSIX_FAILURE;
    }

    MEMSET (pMsg, QOS_INIT_VAL, sizeof (tQosQueueMsg));

    pMsg->u2MsgType = QOS_RM_MSG;
    pMsg->RmMsg.pData = pData;
    pMsg->RmMsg.u1Event = u1Event;
    pMsg->RmMsg.u2DataLen = u2DataLen;
    if (QOS_SEND_TO_QUEUE (QOS_MSG_QUEUE_ID, (UINT1 *) &pMsg,
                           QOS_DEF_MSG_LEN) != OSIX_SUCCESS)
    {
        if (u1Event == RM_MESSAGE)
        {
            /* RM CRU Buffer Memory Release */
            RM_FREE (pData);
        }
        else if ((u1Event == RM_STANDBY_UP) || (u1Event == RM_STANDBY_DOWN))
        {
            /* Call RM API to release memory of data of these events */
            QosRmReleaseMemoryForMsg ((UINT1 *) pData);
        }

        /* Memory for the Queue Message will be released inside the 
         * QOS_SEND_TO_QUEUE routine itself. 
         */
        QOS_TRC (OS_RESOURCE_TRC, "QosRedRmCallBack:"
                 "QOS_SEND_TO_QUEUE Failed !!!!\r\n");
        return OSIX_FAILURE;
    }
    if (QOS_SEND_EVENT (QOS_TASK_ID, QOS_MSG_EVENT) != OSIX_SUCCESS)
    {
        QOS_TRC_ARG1 (OS_RESOURCE_TRC,
                      "%s : Send To QOS Queue failed, releasing the queue message for port oper"
                      "status indication.\r\n", __FUNCTION__);
        return QOS_FAILURE;
    }
    return OSIX_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : QosRedHandleRmEvents
 *
 * DESCRIPTION      : This function process all the events and messages from 
 *                    RM module.
 *
 * INPUT            : pMsg - pointer to Qos Queue message.
 *
 * OUTPUT           : None.
 * 
 * RETURNS          : None.
 * 
 **************************************************************************/
VOID
QosRedHandleRmEvents (tQosQueueMsg * pMsg)
{
    tRmProtoAck         ProtoAck;
    tRmProtoEvt         ProtoEvt;
    tRmNodeInfo        *pData = NULL;
    UINT4               u4SeqNum = QOS_INIT_VAL;

    ProtoEvt.u4AppId = RM_QOS_APP_ID;
    ProtoEvt.u4Error = RM_NONE;
    switch (pMsg->RmMsg.u1Event)
    {
        case GO_STANDBY:
            QosRedHandleGoStandby ();
            break;
        case GO_ACTIVE:
            QosRedHandleGoActive ();
            break;

        case L2_INITIATE_BULK_UPDATES:
            QosRedHwAuditDecBlkCounter ();
            if (gQoSGlobalInfo.eSysControl != QOS_SYS_CNTL_START)
            {
                ProtoEvt.u4Event = RM_PROTOCOL_BULK_UPDT_COMPLETION;
                if (QosPortRmApiHandleProtocolEvent (&ProtoEvt) == OSIX_FAILURE)
                {
                    QOS_TRC (ALL_FAILURE_TRC,
                             "QosRedHandleRmEvents: Received Bulk request in Qos"
                             "disabled state...!!!!\r\n");
                }
                break;
            }
            QosRedSendBulkReqMsg ();
            break;

        case RM_STANDBY_UP:
            QOS_TRC (MGMT_TRC, "QosRedHandleRmEvents :"
                     "RM_STANDBY_UP event receivedupdating Standby Node"
                     "count.\r \n");
            pData = (tRmNodeInfo *) pMsg->RmMsg.pData;
            if (pMsg->RmMsg.u2DataLen != RM_NODE_COUNT_MSG_SIZE)
            {
                /* Data length is incorrect */
                QosRmReleaseMemoryForMsg ((UINT1 *) pData);
                break;
            }
            gQoSGlobalInfo.RedGlobalInfo.u1NumOfStandbyNodesUp =
                pData->u1NumStandby;
            QosRmReleaseMemoryForMsg ((UINT1 *) pData);

            if (gQoSGlobalInfo.RedGlobalInfo.u1BulkReqRcvd == OSIX_TRUE)
            {
                gQoSGlobalInfo.RedGlobalInfo.u1BulkReqRcvd = OSIX_FALSE;
                QosRedHandleStandbyUpEvent ();
            }
            QosPortRmSetBulkUpdatesStatus ();
            break;
        case RM_STANDBY_DOWN:
            QOS_TRC (MGMT_TRC, "QosRedHandleRmEvents :"
                     "RM_STANDBY_DOWN event received updating Standby Node"
                     "count.\r\n");
            pData = (tRmNodeInfo *) pMsg->RmMsg.pData;
            if (pMsg->RmMsg.u2DataLen != RM_NODE_COUNT_MSG_SIZE)
            {
                /* Data length is incorrect */
                QosRmReleaseMemoryForMsg ((UINT1 *) pData);
                break;
            }
            gQoSGlobalInfo.RedGlobalInfo.u1NumOfStandbyNodesUp =
                pData->u1NumStandby;
            QosRmReleaseMemoryForMsg ((UINT1 *) pData);
            break;
        case RM_INIT_HW_AUDIT:
            if (gQoSGlobalInfo.RedGlobalInfo.u1NodeStatus == RM_ACTIVE)
            {
                QOS_TRC (MGMT_TRC, "QosRedHandleRmEvents:"
                         "RM_INIT_HW_AUDIT event reached");
                QosRedInitHardwareAudit ();
            }
            break;
        case RM_CONFIG_RESTORE_COMPLETE:
            if (gQoSGlobalInfo.RedGlobalInfo.u1NodeStatus == RM_INIT)
            {
                if (QosPortRmGetNodeState () == RM_STANDBY)
                {
                    QosRedHandleIdleToStandby ();
                    ProtoEvt.u4Event = RM_STANDBY_EVT_PROCESSED;
                    if (QosPortRmApiHandleProtocolEvent (&ProtoEvt)
                        == OSIX_FAILURE)
                    {
                        QOS_TRC (ALL_FAILURE_TRC,
                                 "QosRedHandleGoStandby: Acknowledgement to RM"
                                 "for GO_STANDBY event failed!!!!\r\n");
                    }

                }
            }
            if (gQoSGlobalInfo.RedGlobalInfo.u1NodeStatus == RM_ACTIVE)
            {
                /* CONFIG_RESTORE_COMPLETE event reached active node. Reset
                 * the Np sync block flag so that the Np Sync-ups are sent 
                 * on active node. By default it is blocked because during 
                 * MSR Restoration in the active node, Np sync-ups should
                 * not be sent.
                 */
                QosRedHwAuditDecBlkCounter ();
            }
            break;
        case RM_MESSAGE:
            /* read the sequence number from RM Header */
            RM_PKT_GET_SEQNUM (pMsg->RmMsg.pData, &u4SeqNum);
            /* remove RM header */
            RM_STRIP_OFF_RM_HDR (pMsg->RmMsg.pData, pMsg->RmMsg.u2DataLen);
            ProtoAck.u4AppId = RM_QOS_APP_ID;
            ProtoAck.u4SeqNumber = u4SeqNum;
            if (gQoSGlobalInfo.eSysControl == QOS_SYS_CNTL_START)
            {
                QosRedHandleUpdates (pMsg->RmMsg.pData, pMsg->RmMsg.u2DataLen);
            }
            RM_FREE (pMsg->RmMsg.pData);

            /*Sending Ack to RM */
            QosPortRmApiSendProtoAckToRM (&ProtoAck);

            break;
        default:
            break;
    }
}

/*****************************************************************************/
/* Function Name      : QosRedHandleUpdates                                 */
/*                                                                           */
/* Description        : This function handles the update messages from the   */
/*                      RM module. This function invokes appropriate fns.    */
/*                      to store the received update messsage.               */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
QosRedHandleUpdates (tRmMsg * pMsg, UINT2 u2DataLen)
{
    tRmProtoEvt         ProtoEvt;
    UINT2               u2OffSet = 0;
    UINT1               u1MsgType = 0;

    ProtoEvt.u4AppId = RM_QOS_APP_ID;
    ProtoEvt.u4Event = RM_BULK_UPDT_ABORT;

    QOS_RM_GET_1_BYTE (pMsg, &u2OffSet, u1MsgType);

    if (u1MsgType == QOS_BULK_UPD_TAIL_MESSAGE)
    {
        QOS_TRC (CONTROL_PLANE_TRC,
                 "Bulk update tail message received, which "
                 "indicates that bulk updates are completed "
                 "in QOS module. It should be indicated to "
                 "higher layer modules. \n");
        ProtoEvt.u4Error = RM_NONE;
        ProtoEvt.u4Event = RM_PROTOCOL_BULK_UPDT_COMPLETION;
        QosPortRmApiHandleProtocolEvent (&ProtoEvt);
        return;
    }

    /* Only bulk req msg shud be received */
    if (u1MsgType == QOS_BULK_REQ_MESSAGE)
    {
        QOS_TRC (CONTROL_PLANE_TRC,
                 "Bulk request message received from the "
                 "standby node. Treating as if STANDBY_UP"
                 " event is received \n");

        if (QosPortRmGetStandbyNodeCount () == 0)
        {
            /* This is a special case, where bulk request msg from
             * standby is coming before RM_STANDBY_UP. So no need to
             * process the bulk request now. Bulk updates will be send
             * on RM_STANDBY_UP event.
             */
            QOS_TRC (ALL_FAILURE_TRC,
                     "Bulk request message received from the "
                     "standby node before RM_STANDBY_UP"
                     " Ignoring the message !! \n");

            gQoSGlobalInfo.RedGlobalInfo.u1BulkReqRcvd = OSIX_TRUE;
            QosRedHandleStandbyUpEvent ();
            return;
        }

        gQoSGlobalInfo.RedGlobalInfo.u1BulkReqRcvd = OSIX_FALSE;
        /* On recieving QOS_BULK_REQ_MESSAGE, Bulk updation process */
        QosRedHandleStandbyUpEvent ();
        return;
    }

    if (QosPortRmGetNodeState () != RM_STANDBY)
    {
        QOS_TRC (ALL_FAILURE_TRC, " Node status not standby !! \n");
        return;
    }

    if (gQoSGlobalInfo.RedGlobalInfo.u1NodeStatus == RM_STANDBY)
    {
        QosRedProcessPeerMsgAtStandby (pMsg, u2DataLen);
    }
    return;
}

/*****************************************************************************/
/* Function Name      : QosRedHandleStandbyUpEvent                           */
/*                                                                           */
/* Description        : This function handles the standby UP event from the  */
/*                      RM module. This function sends the bulk updates      */
/*                      containing the list of Schedular Id and the hardware */
/*                      Ids for the applicable interfaces.                   */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
QosRedHandleStandbyUpEvent (VOID)
{
    if (QosPortRmGetNodeState () != RM_ACTIVE)
    {
        gQoSGlobalInfo.RedGlobalInfo.u1BulkReqRcvd = OSIX_TRUE;
        return;
    }

    QosRedSendBulkUpdates ();
    return;
}

/*****************************************************************************/
/* Function Name      : QosRedSendBulkUpdates                                 */
/*                                                                           */
/* Description        : This function handles the update messages from the   */
/*                      RM module. This function invokes appropriate fns.    */
/*                      to store the received update messsage.               */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
QosRedSendBulkUpdates (VOID)
{
    tRmMsg             *pMsg = NULL;
    tQoSSchedNode      *pSchedNode = NULL;
    tQoSQNode          *pQueueNode = NULL;
    tQoSSchedNode       TempSchedNode;
    tQoSQNode           TempQueueNode;
    tRmProtoEvt         ProtoEvt;
    UINT2               u2BufSize = 0;
    UINT2               u2OffSet = 0;
    UINT2               u2HwSyncLen = 0;

    MEMSET (&TempSchedNode, 0, sizeof (tQoSSchedNode));
    MEMSET (&TempQueueNode, 0, sizeof (tQoSQNode));

    ProtoEvt.u4AppId = RM_QOS_APP_ID;
    ProtoEvt.u4Event = RM_BULK_UPDT_ABORT;

    u2BufSize = QOS_RED_MAX_MSG_SIZE;
    u2HwSyncLen = QOS_SYNC_HW_ID_MSG_SIZE;
    pMsg = RM_ALLOC_TX_BUF (u2BufSize);

    if (pMsg == NULL)
    {
        QOS_TRC (ALL_FAILURE_TRC, "RM alloc failed. Bulk updates not sent\n");
        ProtoEvt.u4Error = RM_MEMALLOC_FAIL;
        QosPortRmApiHandleProtocolEvent (&ProtoEvt);
        QosPortRmSetBulkUpdatesStatus ();
        return;
    }
    u2OffSet = 0;

    /* Syncing the Schedular Entries in Bulk */
    while (NULL != (pSchedNode =
                    RBTreeGetNext (gQoSGlobalInfo.pRbSchedTbl, &TempSchedNode,
                                   NULL)))
    {

        if ((u2BufSize - u2OffSet) < u2HwSyncLen)
        {
            /* no room for the current message */
            if (QosPortEnqMsgToRm (pMsg, u2OffSet) == OSIX_FAILURE)
            {
                ProtoEvt.u4Error = RM_SENDTO_FAIL;
                QosPortRmApiHandleProtocolEvent (&ProtoEvt);
                return;
            }

            pMsg = RM_ALLOC_TX_BUF (u2BufSize);

            if (pMsg == NULL)
            {
                QosPortRmSetBulkUpdatesStatus ();
                QOS_TRC (ALL_FAILURE_TRC,
                         "RM alloc failed. Bulk updates not sent\n");
                ProtoEvt.u4Error = RM_MEMALLOC_FAIL;
                QosPortRmApiHandleProtocolEvent (&ProtoEvt);
                return;
            }
            u2OffSet = 0;
        }

        QOS_RM_PUT_1_BYTE (pMsg, &u2OffSet, QOSX_SYNC_SCHED_HW_ID);
        QOS_RM_PUT_2_BYTE (pMsg, &u2OffSet, QOS_SYNC_HW_ID_MSG_SIZE);
        QOS_RM_PUT_4_BYTE (pMsg, &u2OffSet, pSchedNode->i4IfIndex);
        QOS_RM_PUT_4_BYTE (pMsg, &u2OffSet, pSchedNode->u4Id);
        QOS_RM_PUT_4_BYTE (pMsg, &u2OffSet, pSchedNode->u4SchedulerHwId);
        TempSchedNode.u4Id = pSchedNode->u4Id;
        TempSchedNode.i4IfIndex = pSchedNode->i4IfIndex;
    }

    /* Syncing the Queue Hardware Entries in Bulk */
    while (NULL != (pQueueNode =
                    RBTreeGetNext (gQoSGlobalInfo.pRbQTbl, &TempQueueNode,
                                   NULL)))
    {

        if ((u2BufSize - u2OffSet) < u2HwSyncLen)
        {
            /* no room for the current message */
            if (QosPortEnqMsgToRm (pMsg, u2OffSet) == OSIX_FAILURE)
            {
                ProtoEvt.u4Error = RM_SENDTO_FAIL;
                QosPortRmApiHandleProtocolEvent (&ProtoEvt);
                return;
            }

            pMsg = RM_ALLOC_TX_BUF (u2BufSize);

            if (pMsg == NULL)
            {
                QosPortRmSetBulkUpdatesStatus ();
                QOS_TRC (ALL_FAILURE_TRC,
                         "RM alloc failed. Bulk updates not sent\n");
                ProtoEvt.u4Error = RM_MEMALLOC_FAIL;
                QosPortRmApiHandleProtocolEvent (&ProtoEvt);
                return;
            }
            u2OffSet = 0;
        }

        QOS_RM_PUT_1_BYTE (pMsg, &u2OffSet, QOSX_SYNC_QUEUE_HW_ID);
        QOS_RM_PUT_2_BYTE (pMsg, &u2OffSet, QOS_SYNC_HW_ID_MSG_SIZE);
        QOS_RM_PUT_4_BYTE (pMsg, &u2OffSet, pQueueNode->i4IfIndex);
        QOS_RM_PUT_4_BYTE (pMsg, &u2OffSet, pQueueNode->u4Id);
        QOS_RM_PUT_4_BYTE (pMsg, &u2OffSet, pQueueNode->u4QueueHwId);
        TempQueueNode.u4Id = pQueueNode->u4Id;
        TempQueueNode.i4IfIndex = pQueueNode->i4IfIndex;
    }

    if (u2OffSet != 0)
    {
        /* Send the buffer out */
        if (QosPortEnqMsgToRm (pMsg, u2OffSet) == OSIX_FAILURE)
        {
            ProtoEvt.u4Error = RM_SENDTO_FAIL;
            QosPortRmApiHandleProtocolEvent (&ProtoEvt);
        }
    }
    else if (pMsg)
    {
        /* Empty buffer created without any update messages filled */
        RM_FREE (pMsg);
    }

    QosPortRmSetBulkUpdatesStatus ();

    /* Send the tail msg to indicate the completion of Bulk
     * update process.
     */
    QosRedSendBulkUpdTailMsg ();

    return;
}

/*****************************************************************************/
/* Function Name      : QosRedSendBulkUpdTailMsg                             */
/*                                                                           */
/* Description        : This function will send the tail msg to the standy   */
/*                      node, which indicates the completion of Bulk update  */
/*                      process.                                             */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
QosRedSendBulkUpdTailMsg (VOID)
{
    tRmMsg             *pMsg;
    tRmProtoEvt         ProtoEvt;
    UINT2               u2OffSet;

    ProtoEvt.u4AppId = RM_QOS_APP_ID;
    ProtoEvt.u4Event = RM_BULK_UPDT_ABORT;

    /* Form a bulk update tail message. 

     *        <------------1 Byte----------><--2 Byte-->
     ***************************************************
     *        *                            *           *
     * RM Hdr * QOS_BULK_UPD_TAIL_MESSAGE *Msg Length *
     *        *                            *           *
     ***************************************************

     * The RM Hdr shall be included by RM.
     */

    if ((pMsg = RM_ALLOC_TX_BUF (QOS_RED_BULK_UPD_TAIL_MSG_SIZE)) == NULL)
    {
        QOS_TRC (ALL_FAILURE_TRC,
                 "RM alloc failed. Bulk Update Tail message not sent \n");
        ProtoEvt.u4Error = RM_MEMALLOC_FAIL;
        QosPortRmApiHandleProtocolEvent (&ProtoEvt);
        return;
    }

    QOS_TRC (CONTROL_PLANE_TRC, "Sending Bulk Update tail message \n");
    u2OffSet = 0;

    QOS_RM_PUT_1_BYTE (pMsg, &u2OffSet, QOS_BULK_UPD_TAIL_MESSAGE);
    QOS_RM_PUT_2_BYTE (pMsg, &u2OffSet, QOS_RED_BULK_UPD_TAIL_MSG_SIZE);

    if (QosPortEnqMsgToRm (pMsg, u2OffSet) == OSIX_FAILURE)
    {
        ProtoEvt.u4Error = RM_SENDTO_FAIL;
        QosPortRmApiHandleProtocolEvent (&ProtoEvt);
    }

    return;
}

/***********************************************************************
 * FUNCTION NAME    : QosRedHandleGoActive 
 *
 * DESCRIPTION      : This routine handles the GO_ACTIVE event and responds
 *                    to RM with an acknowledgement.
 *
 * INPUT            : None
 *
 * OUTPUT           : None
 * 
 * RETURNS          : VOID
 * 
 **********************************************************************/
PUBLIC VOID
QosRedHandleGoActive (VOID)
{
    tRmProtoEvt         ProtoEvt;

    ProtoEvt.u4AppId = RM_QOS_APP_ID;
    ProtoEvt.u4Error = RM_NONE;
    /* check for node state (if active) */
    if (gQoSGlobalInfo.RedGlobalInfo.u1NodeStatus == RM_ACTIVE)
    {
        QOS_TRC (MGMT_TRC,
                 "QosRedHandleGoActive: GO_ACTIVE event reached"
                 " when node is already active!!!!\r\n");
        return;
    }
    /* check for node state (if idle)
       update node status from idle to active &update RM event
     */
    if (gQoSGlobalInfo.RedGlobalInfo.u1NodeStatus == RM_INIT)
    {
        QOS_TRC (MGMT_TRC, 
                 "QosRedHandleGoActive: Idle to Active" " transition...\r\n");
        QosRedHandleIdleToActive ();
        ProtoEvt.u4Event = RM_IDLE_TO_ACTIVE_EVT_PROCESSED;
    }
    /* check for node state (if standby)
       update node status from standby to active &update RM event
     */
    if (gQoSGlobalInfo.RedGlobalInfo.u1NodeStatus == RM_STANDBY)
    {
        QOS_TRC (MGMT_TRC, 
                 "QosRedHandleGoActive: Standby to Active"
                 " transition...\r\n");
        QosRedHandleStandbyToActive ();
        ProtoEvt.u4Event = RM_STANDBY_TO_ACTIVE_EVT_PROCESSED;
    }
    if (gQoSGlobalInfo.RedGlobalInfo.u1BulkReqRcvd == OSIX_TRUE)
    {
        gQoSGlobalInfo.RedGlobalInfo.u1BulkReqRcvd = OSIX_FALSE;
        QosRedHandleStandbyUpEvent ();
    }
    /* send RM event to RM */

    if (QosPortRmApiHandleProtocolEvent (&ProtoEvt) == OSIX_FAILURE)
    {
        QOS_TRC (ALL_FAILURE_TRC,
                 "QosRedHandleGoActive: Acknowledgement to RM for"
                 "GO_ACTIVE event failed!!!!\r\n");
    }
    return;
}

/*******************************************************************
 * FUNCTION NAME    : QosRedHandleIdleToActive 
 *
 * DESCRIPTION      : This routine updates the node status on transition from
 *                    Idle to Active.
 *
 * INPUT            : None
 *
 * OUTPUT           : None
 * 
 * RETURNS          : VOID
 * 
 ****************************************************************/
PUBLIC VOID
QosRedHandleIdleToActive (VOID)
{
    INT4                i4RetStatus = 0;
    gQoSGlobalInfo.RedGlobalInfo.u1NodeStatus = RM_ACTIVE;
    gQoSGlobalInfo.RedGlobalInfo.u1NumOfStandbyNodesUp =
        QosPortRmGetStandbyNodeCount ();
    /* Np Sync-ups must be sent from Active node, hence the unblocking 
     * by decrementing the counter. It was blocked on Boot-up.
     */
    QosRedHwAuditDecBlkCounter ();
    i4RetStatus = QoSInitWithMBSMandRM ();
    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (OS_RESOURCE_TRC,
                      "%s : ERROR: QoSInitWithMBSMandRM () Failed!. " "\r\n",
                      __FUNCTION__);
    }
    return;
}

/*****************************************************************
 * FUNCTION NAME    : QosRedHandleStandbyToActive 
 *
 * DESCRIPTION      : On Standby to Active the following actions are performed
 *                    1. Update the Node Status and standby node count
 *                    
 *
 * INPUT            : None
 *
 * OUTPUT           : None
 * 
 * RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 * 
 ******************************************************************/
PUBLIC VOID
QosRedHandleStandbyToActive (VOID)
{
    gQoSGlobalInfo.RedGlobalInfo.u1NodeStatus = RM_ACTIVE;
    gQoSGlobalInfo.RedGlobalInfo.u1NumOfStandbyNodesUp =
        QosPortRmGetStandbyNodeCount ();
    return;
}

/******************************************************************
 * FUNCTION NAME    : QosRedHandleGoStandby  
 *
 * DESCRIPTION      : This routine handles the GO_STANDBY event and responds
 *                    to RM with an acknowledgement.
 *
 * INPUT            : None
 *
 * OUTPUT           : None
 * 
 * RETURNS          : VOID
 * 
 ******************************************************************/
PUBLIC VOID
QosRedHandleGoStandby (VOID)
{
    tRmProtoEvt         ProtoEvt;
    ProtoEvt.u4AppId = RM_QOS_APP_ID;
    ProtoEvt.u4Error = RM_NONE;

    if (gQoSGlobalInfo.RedGlobalInfo.u1NodeStatus == RM_STANDBY)
    {
        QOS_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                 "QosRedHandleGoStandby: GO_STANDBY event reached"
                 " when node is already standby!!!!\r\n");
        return;
    }

    gQoSGlobalInfo.RedGlobalInfo.u1BulkReqRcvd = OSIX_FALSE;
    /* Create the Hardware Audit Np sync Buffer table */
    QosRedHwAudCrtNpSyncBufferTable ();

    if (gQoSGlobalInfo.RedGlobalInfo.u1NodeStatus == RM_INIT)
    {
        QOS_TRC (CONTROL_PLANE_TRC,
                 "QosRedHandleGoStandby: GO_STANDBY event received"
                 " when state is Idle.\r\n");
        /* GO_STANDBY event is not processed here. It is done when 
         * CONFIG_RESTORE_COMPLETE event is received. Since static bulk 
         * update will be completed only by then, the acknowledgement can be 
         * sent during CONFIG_RESTORE_COMPLETE handling, which will trigger RM 
         * to send dynamic bulk update event to modules.
         */

        /* During Initialisation of the QOS Module,Default entries will be added
         * and the same will be synced up by the MSR module during static bulk
         * update. So Shutdown the qos module and start again to delete all the
         * default entries and the same will be restored by static bulk update
         * from the Active Node */

    }
    else if (gQoSGlobalInfo.RedGlobalInfo.u1NodeStatus == RM_ACTIVE)
    {
        QOS_TRC (MGMT_TRC,
                 "QosRedHandleGoStandby: Active to Standby"
                 " transition...\r\n");
        QosRedHandleActiveToStandby ();
        ProtoEvt.u4Event = RM_STANDBY_EVT_PROCESSED;
        if (QosPortRmApiHandleProtocolEvent (&ProtoEvt) == OSIX_FAILURE)
        {
            QOS_TRC (ALL_FAILURE_TRC,
                     "QosRedHandleGoStandby: Acknowledgement to RM for"
                     "GO_STANDBY event failed!!!!\r\n");
        }
    }
    return;
}

/*****************************************************************
 * FUNCTION NAME    : QosRedHandleActiveToStandby  
 *
 * DESCRIPTION      : On Active to Standby transition, the following actions 
 *                            are performed,
 *                               1. Update the Node Status
 *                                2. Disable and enable the module
 *                              
 *
 * INPUT            : None
 *
 * OUTPUT           : None
 * 
 * RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 * 
 *****************************************************************/
PUBLIC VOID
QosRedHandleActiveToStandby (VOID)
{
    gQoSGlobalInfo.RedGlobalInfo.u1NodeStatus = RM_STANDBY;
    gQoSGlobalInfo.RedGlobalInfo.u1NumOfStandbyNodesUp = QOS_INIT_VAL;
    QosRedHwAuditIncBlkCounter ();
    return;
}

/******************************************************************
 * FUNCTION NAME    : QosRedHandleIdleToStandby 
 *
 * DESCRIPTION      : This routine updates the node status from idle to standby.
 *
 * INPUT            : None
 *
 * OUTPUT           : None
 * 
 * RETURNS          : VOID
 * 
 ******************************************************************/
PUBLIC VOID
QosRedHandleIdleToStandby (VOID)
{
    gQoSGlobalInfo.RedGlobalInfo.u1NodeStatus = RM_STANDBY;
    gQoSGlobalInfo.RedGlobalInfo.u1NumOfStandbyNodesUp = QOS_INIT_VAL;
    return;
}

/*****************************************************************
 * FUNCTION NAME    : QosRedHwAudCrtNpSyncBufferTable 
 *
 * DESCRIPTION      : This routine creates Hardware audit buffer table in the
 *                    standby node. Previosly present entries are delete
 *                    before creating the table.
 *
 * INPUT              : None
 *
 * OUTPUT           : None
 * 
 * RETURNS             : VOID
 * 
 *******************************************************************/
PUBLIC VOID
QosRedHwAudCrtNpSyncBufferTable (VOID)
{
    tQosRedNpSyncEntry *pBuf = NULL;
    tQosRedNpSyncEntry *pTempBuf = NULL;

    /* If the SLL is not empty then delete all the buffer entries. 
     */
    if (TMO_SLL_Count (&gQoSGlobalInfo.RedGlobalInfo.HwAuditTable) != 0)
    {
        TMO_DYN_SLL_Scan (&gQoSGlobalInfo.RedGlobalInfo.HwAuditTable, pBuf,
                          pTempBuf, tQosRedNpSyncEntry *)
        {
            TMO_SLL_Delete (&gQoSGlobalInfo.RedGlobalInfo.HwAuditTable,
                            &(pBuf->Node));
            MemReleaseMemBlock (gQoSGlobalInfo.RedGlobalInfo.
                                HwAuditTablePoolId, (UINT1 *) pBuf);
            pBuf = pTempBuf;
        }
    }
    TMO_SLL_Init (&(gQoSGlobalInfo.RedGlobalInfo.HwAuditTable));
    return;
}

/************************************************************************
 * FUNCTION NAME    : QosRedProcessPeerMsgAtStandby 
 *
 * DESCRIPTION      : This routine process the message in standby node,received
 *                    from     peer Active node.
 *                     After Process,this function has to send ACK to RM module.                            
 *                              
 *                    
 * INPUT            : pMsg - RM Data buffer holding messages
 *                            u2DataLen - Length of data in buffer.
 *
 * OUTPUT           : None
 * 
 * RETURNS          : VOID
 * 
 ***********************************************************************/
PUBLIC VOID
QosRedProcessPeerMsgAtStandby (tRmMsg * pMsg, UINT2 u2DataLen)
{
    UINT2               u2OffSet = QOS_INIT_VAL;
    UINT2               u2Length = QOS_INIT_VAL;
    UINT2               u2RemMsgLen = QOS_INIT_VAL;
    UINT2               u2MinLen = QOS_INIT_VAL;
    UINT1               u1MsgType = QOS_INIT_VAL;

    u2MinLen = NPSYNC_MSGTYPE_SIZE + NPSYNC_MSG_LENGTH;

    while ((u2OffSet + u2MinLen) <= u2DataLen)
    {
        QOS_RM_GET_1_BYTE (pMsg, &u2OffSet, u1MsgType);

        QOS_RM_GET_2_BYTE (pMsg, &u2OffSet, u2Length);

        if (u2Length < u2MinLen)
        {
            /* The Length field in the RM packet is less than minimum 
             * number of bytes, which is MessageType + Length. 
             */
            u2OffSet += u2Length;
            continue;
        }

        u2RemMsgLen = (UINT2) (u2Length - u2MinLen);

        if ((u2OffSet + u2RemMsgLen) > u2DataLen)
        {
            /* The Length field in the RM packet is wrong, hence continuing 
             * with the next packet */
            u2OffSet = u2DataLen;
            continue;
        }
        if (u1MsgType == NPSYNC_MESSAGE)
        {
            QOS_TRC (MGMT_TRC,
                     "QosRedProcessPeerMsgAtStandby: "
                     "received QOS_RED_NP_SYNC_INFO_MSG message .. \r\n");
            QosHwProcessNpSyncMsg (pMsg, &u2OffSet);
        }
        else if (u1MsgType == QOSX_SYNC_SCHED_HW_ID)
        {
            QOS_TRC (MGMT_TRC,
                     "QosRedProcessPeerMsgAtStandby: "
                     "received QOSX_SYNC_SCHED_HW_ID message .. \r\n");

            QosRedProcessSchedHwSyncMsg (pMsg, &u2OffSet);
        }
        else if (u1MsgType == QOSX_SYNC_QUEUE_HW_ID)
        {
            QOS_TRC (MGMT_TRC,
                     "QosRedProcessPeerMsgAtStandby: "
                     "received QOSX_SYNC_QUEUE_HW_ID message .. \r\n");

            QosRedProcessQueueHwSyncMsg (pMsg, &u2OffSet);
        }
        else
        {
            u2OffSet += u2Length;
        }

    }

    return;
}

/*******************************************************************
 * FUNCTION NAME    : QosRedProcessQueueHwSyncMsg 
 *
 * DESCRIPTION      : This routine processes the dynamic sync message
 *                    received from active node for Queue Hardware ID 
 *                    update in standby node. 
 *
 * INPUT            : tRmMsg - RM Message
 *
 * OUTPUT           : pu2Offset - Offset within the RM Message input
 * 
 * RETURNS          : None
 * 
 ********************************************************************/
VOID
QosRedProcessQueueHwSyncMsg (tRmMsg * pMsg, UINT2 *pu2OffSet)
{

    tQoSQNode          *pQNode = NULL;

    INT4                i4IfIndex = 0;
    UINT4               u4QueueId = 0;
    UINT4               u4QosQueueHwId = 0;

    QOS_RM_GET_4_BYTE (pMsg, pu2OffSet, i4IfIndex);
    QOS_RM_GET_4_BYTE (pMsg, pu2OffSet, u4QueueId);
    QOS_RM_GET_4_BYTE (pMsg, pu2OffSet, u4QosQueueHwId);

    pQNode = QoSUtlGetQNode (i4IfIndex, u4QueueId);
    if (pQNode != NULL)
    {
        /* Fill in the Scheduler HW ID */
        pQNode->u4QueueHwId = u4QosQueueHwId;
    }
    return;
}

/*******************************************************************
 * FUNCTION NAME    : QosRedProcessSchedHwSyncMsg 
 *
 * DESCRIPTION      : This routine processes the dynamic sync message
 *                    received from active node for Queue Hardware ID 
 *                    update in standby node. 
 *
 * INPUT            : tRmMsg - RM Message
 *
 * OUTPUT           : pu2Offset - Offset within the RM Message input
 * 
 * RETURNS          : None
 * 
 ********************************************************************/
VOID
QosRedProcessSchedHwSyncMsg (tRmMsg * pMsg, UINT2 *pu2OffSet)
{

    tQoSSchedNode      *pSchedNode = NULL;
    INT4                i4IfIndex = 0;
    UINT4               u4SchedId = 0;
    UINT4               u4QosSchedHwId = 0;

    QOS_RM_GET_4_BYTE (pMsg, pu2OffSet, i4IfIndex);
    QOS_RM_GET_4_BYTE (pMsg, pu2OffSet, u4SchedId);
    QOS_RM_GET_4_BYTE (pMsg, pu2OffSet, u4QosSchedHwId);

    pSchedNode = QoSUtlGetSchedNode (i4IfIndex, u4SchedId);
    if (pSchedNode != NULL)
    {
        /* Fill in the Scheduler HW ID */
        pSchedNode->u4SchedulerHwId = u4QosSchedHwId;
    }
    return;
}

/*******************************************************************
 * FUNCTION NAME    : QosRedSyncQueueHwId 
 *
 * DESCRIPTION      : This routine handles the Hardware audit process 
 *                    of the active node after switchover. 
 *                      It scans the Np Sync buffer table and performs hardware
 *                      audit for the entries present in them. The precedence 
 *                      is given to software and the hardware is programmed
 *                      for the software entries.
 * INPUT            : None
 *
 * OUTPUT           : None
 * 
 * RETURNS          : None
 * 
 ********************************************************************/
VOID
QosRedSyncQueueHwId (INT4 i4IfIndex, UINT4 u4QosQueueId, UINT4 u4QosQueueHwId)
{
    tRmProtoEvt         ProtoEvt;
    tRmMsg             *pMsg = NULL;
    UINT2               u2OffSet = QOS_INIT_VAL;
    UINT2               u2MsgSize = QOS_INIT_VAL;

    ProtoEvt.u4AppId = RM_QOS_APP_ID;
    ProtoEvt.u4Event = RM_BULK_UPDT_ABORT;

    if (QosPortRmGetNodeState () != RM_ACTIVE)
    {
        QOS_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                 "QosRedSyncQueueHwId: "
                 "Node currently not active. Sync messages are not allowed \n");
        return;
    }

    /*
     *    QOSX Scheduler Hw ID Dynamic Update message
     *
     *    <- 1 byte ->|<- 2 B ->|<-  4B   ->|<-   4B   ->|<-   4B   ->|
     *    ------------------------------------------------------
     *    | Msg. Type | Length  |  IfIndex  |  QosID     |<-QueueHwId->|      
     *    |           |         |           |            |             |     
     *    --------------------------------------------------------------
     */

    u2MsgSize = QOS_SYNC_HW_ID_MSG_SIZE;
    if ((pMsg = RM_ALLOC_TX_BUF (u2MsgSize)) == NULL)
    {
        ProtoEvt.u4Error = RM_MEMALLOC_FAIL;
        QosPortRmApiHandleProtocolEvent (&ProtoEvt);
    }
    else
    {
        u2OffSet = QOS_INIT_VAL;

        QOS_RM_PUT_1_BYTE (pMsg, &u2OffSet, QOSX_SYNC_QUEUE_HW_ID);
        QOS_RM_PUT_2_BYTE (pMsg, &u2OffSet, u2MsgSize);
        QOS_RM_PUT_4_BYTE (pMsg, &u2OffSet, (UINT4) i4IfIndex);
        QOS_RM_PUT_4_BYTE (pMsg, &u2OffSet, u4QosQueueId);
        QOS_RM_PUT_4_BYTE (pMsg, &u2OffSet, u4QosQueueHwId);
        if (QosPortEnqMsgToRm (pMsg, u2OffSet) == RM_FAILURE)
        {
            ProtoEvt.u4Error = RM_SENDTO_FAIL;
            QosPortRmApiHandleProtocolEvent (&ProtoEvt);
        }
    }
    return;
}

/*******************************************************************
 * FUNCTION NAME    : QosRedSyncSchedHwId 
 *
 * DESCRIPTION      : This routine handles the Hardware audit process 
 *                    of the active node after switchover. 
 *                      It scans the Np Sync buffer table and performs hardware
 *                      audit for the entries present in them. The precedence 
 *                      is given to software and the hardware is programmed
 *                      for the software entries.
 * INPUT            : None
 *
 * OUTPUT           : None
 * 
 * RETURNS          : None
 * 
 ********************************************************************/
VOID
QosRedSyncSchedHwId (INT4 i4IfIndex, UINT4 u4QosSchedId, UINT4 u4QosSchedHwId)
{
    tRmProtoEvt         ProtoEvt;
    tRmMsg             *pMsg = NULL;
    UINT2               u2OffSet = QOS_INIT_VAL;
    UINT2               u2MsgSize = QOS_INIT_VAL;

    if (QosPortRmGetNodeState () != RM_ACTIVE)
    {
        QOS_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                 "QosRedSyncSchedHwId: "
                 "Node currently not active. Sync messages are not allowed \n");
        return;
    }
    ProtoEvt.u4AppId = RM_QOS_APP_ID;
    ProtoEvt.u4Event = RM_BULK_UPDT_ABORT;

    /*
     *    QOSX Scheduler Hw ID Dynamic Update message
     *
     *    <- 1 byte ->|<- 2 B ->|<-  4B   ->|<-   4B   ->|<-   4B   ->|
     *    ------------------------------------------------------
     *    | Msg. Type | Length  |  IfIndex  |  SchedId   |<-SchedHwId->|      
     *    |           |         |           |            |             |     
     *    --------------------------------------------------------------
     */

    u2MsgSize = QOS_SYNC_HW_ID_MSG_SIZE;
    if ((pMsg = RM_ALLOC_TX_BUF (u2MsgSize)) == NULL)
    {
        ProtoEvt.u4Error = RM_MEMALLOC_FAIL;
        QosPortRmApiHandleProtocolEvent (&ProtoEvt);
    }
    else
    {
        u2OffSet = QOS_INIT_VAL;

        QOS_RM_PUT_1_BYTE (pMsg, &u2OffSet, QOSX_SYNC_SCHED_HW_ID);
        QOS_RM_PUT_2_BYTE (pMsg, &u2OffSet, u2MsgSize);
        QOS_RM_PUT_4_BYTE (pMsg, &u2OffSet, (UINT4) i4IfIndex);
        QOS_RM_PUT_4_BYTE (pMsg, &u2OffSet, u4QosSchedId);
        QOS_RM_PUT_4_BYTE (pMsg, &u2OffSet, u4QosSchedHwId);
        if (QosPortEnqMsgToRm (pMsg, u2OffSet) == RM_FAILURE)
        {
            ProtoEvt.u4Error = RM_SENDTO_FAIL;
            QosPortRmApiHandleProtocolEvent (&ProtoEvt);
        }
    }
    return;
}

/*****************************************************************************/
/* Function Name      : QosRedSendBulkReqMsg                                 */
/*                                                                           */
/* Description        : This function sends bulk request message to the      */
/*                      active node.                                         */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
QosRedSendBulkReqMsg (VOID)
{
    tRmMsg             *pMsg;
    tRmProtoEvt         ProtoEvt;
    UINT2               u2OffSet;

    ProtoEvt.u4AppId = RM_QOS_APP_ID;
    if (QosPortRmGetNodeState () != RM_STANDBY)
    {
        QOS_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                 "QosRedSendBulkReqMsg: "
                 "Node currently not standby. Bulk Request not sent \n");
        return;
    }

    /*
     *    QOS Bulk Request message
     *    <- 1 byte ->|<- 2 bytes->|
     *    --------------------------
     *    | Msg. Type |  Length    |
     *    |     (7)   |   3        |
     *    |-------------------------
     *
     */

    if ((pMsg = RM_ALLOC_TX_BUF (QOS_RED_BULK_REQ_MSG_SIZE)) == NULL)
    {
        QOS_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                 "QosRedSendBulkReqMsg: "
                 "RM Alloc failed. Bulk Request not sent \n");
        return;
    }

    QOS_TRC (CONTROL_PLANE_TRC, "Sending Bulk request message \n");

    u2OffSet = 0;
    QOS_RM_PUT_1_BYTE (pMsg, &u2OffSet, QOS_BULK_REQ_MESSAGE);

    QOS_RM_PUT_2_BYTE (pMsg, &u2OffSet, QOS_RED_BULK_REQ_MSG_SIZE);

    if (QosPortEnqMsgToRm (pMsg, u2OffSet) == OSIX_FAILURE)
    {
        QOS_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                           "QosRedSendBulkReqMsg: "
                           "EnqMsgToRm failed \n");
        ProtoEvt.u4Error = RM_SENDTO_FAIL;
        QosPortRmApiHandleProtocolEvent (&ProtoEvt);
        return;
    }

    return;
}

/*******************************************************************
 * FUNCTION NAME    : QosRedInitHardwareAudit 
 *
 * DESCRIPTION      : This routine handles the Hardware audit process 
 *                    of the active node after switchover. 
 *                      It scans the Np Sync buffer table and performs hardware
 *                      audit for the entries present in them. The precedence 
 *                      is given to software and the hardware is programmed
 *                      for the software entries.
 * INPUT            : None
 *
 * OUTPUT           : None
 * 
 * RETURNS          : None
 * 
 ********************************************************************/
PUBLIC VOID
QosRedInitHardwareAudit (VOID)
{
    tQosRedNpSyncEntry *pBuf = NULL;
    tQosRedNpSyncEntry *pTempBuf = NULL;

    TMO_DYN_SLL_Scan (&gQoSGlobalInfo.RedGlobalInfo.HwAuditTable, pBuf,
                      pTempBuf, tQosRedNpSyncEntry *)
    {
        switch (pBuf->u4NpApiId)
        {
            case NPSYNC_QO_S_HW_INIT:
                break;
            case NPSYNC_QO_S_HW_MAP_CLASS_TO_POLICY:
                QoSRedHwAuditHandleMapClsToPlcy ((tQoSNpSyncClassMapNode *)
                                                 & (pBuf->unNpData.
                                                    QoSHwMapClassToPolicy.
                                                    ClassMapNode.u4L2FilterId),
                                                 pBuf->unNpData.
                                                 QoSHwMapClassToPolicy.
                                                 u4PlyMapId);
                break;
            case NPSYNC_QO_S_HW_UPDATE_POLICY_MAP_FOR_CLASS:
                QoSRedHwAuditHandleMapClsToPlcy ((tQoSNpSyncClassMapNode *)
                                                 & (pBuf->unNpData.
                                                    QoSHwUpdatePolicyMapForClass.
                                                    ClassMapNode.u4L2FilterId),
                                                 pBuf->unNpData.
                                                 QoSHwUpdatePolicyMapForClass.
                                                 u4PlyMapId);
                break;
            case NPSYNC_QO_S_HW_UNMAP_CLASS_FROM_POLICY:
                QoSRedHwAuditHandleMapClsToPlcy ((tQoSNpSyncClassMapNode *)
                                                 & (pBuf->unNpData.
                                                    QoSHwUnmapClassFromPolicy.
                                                    ClassMapNode.u4L2FilterId),
                                                 pBuf->unNpData.
                                                 QoSHwUnmapClassFromPolicy.
                                                 u4PlyMapId);
                break;
            case NPSYNC_QO_S_HW_DELETE_CLASS_MAP_ENTRY:
                QoSHwAuditHandleDelClsMapEntry ((tQoSNpSyncClassMapNode *)
                                                & (pBuf->unNpData.
                                                   QoSHwDeleteClassMapEntry.
                                                   ClassMapNode.u4L2FilterId));
                break;
            case NPSYNC_QO_S_HW_METER_CREATE:
                QoSRedHwAuditHandleMeterAudit (pBuf->unNpData.QoSHwMeterCreate.
                                               u4MeterId);
                break;
            case NPSYNC_QO_S_HW_METER_DELETE:
                QoSRedHwAuditHandleMeterAudit (pBuf->unNpData.QoSHwMeterDelete.
                                               u4MeterId);
                break;
            case NPSYNC_QO_S_HW_SCHEDULER_ADD:
                QoSRedHwAuditHandleSchedAudit (pBuf->unNpData.QoSHwSchedulerAdd.
                                               u4IfIndex,
                                               pBuf->unNpData.QoSHwSchedulerAdd.
                                               u4SchedId);
                break;
            case NPSYNC_QO_S_HW_SCHEDULER_UPDATE_PARAMS:
                QoSRedHwAuditHandleSchedAudit (pBuf->unNpData.
                                               QoSHwSchedulerUpdateParams.
                                               u4IfIndex,
                                               pBuf->unNpData.
                                               QoSHwSchedulerUpdateParams.
                                               u4SchedId);
                break;
            case NPSYNC_QO_S_HW_SCHEDULER_DELETE:
                QoSRedHwAuditHandleSchedAudit (pBuf->unNpData.
                                               QoSHwSchedulerDelete.u4IfIndex,
                                               pBuf->unNpData.
                                               QoSHwSchedulerDelete.u4SchedId);
                break;
            case NPSYNC_QO_S_HW_QUEUE_CREATE:
                QosRedHwAuditHandleQueueAudit (pBuf->unNpData.QoSHwQueueCreate.
                                               u4IfIndex,
                                               pBuf->unNpData.QoSHwQueueCreate.
                                               u4Id);
                break;
            case NPSYNC_QO_S_HW_QUEUE_DELETE:
                QosRedHwAuditHandleQueueAudit (pBuf->unNpData.QoSHwQueueDelete.
                                               u4IfIndex,
                                               pBuf->unNpData.QoSHwQueueDelete.
                                               u4Id);
                break;
            case NPSYNC_QO_S_HW_MAP_CLASS_TO_QUEUE:
                QosRedHwAuditHandleQmapAudit (pBuf->unNpData.
                                              QoSHwMapClassToQueue.u4IfIndex,
                                              pBuf->unNpData.
                                              QoSHwMapClassToQueue.
                                              u4ClsOrPriType,
                                              pBuf->unNpData.
                                              QoSHwMapClassToQueue.u4ClsOrPri,
                                              pBuf->unNpData.
                                              QoSHwMapClassToQueue.u4QId,
                                              pBuf->unNpData.
                                              QoSHwMapClassToQueue.u1Flag);
                break;
            case NPSYNC_QO_S_HW_SCHEDULER_HIERARCHY_MAP:
                QoSRedHwAuditHandleHierAudit (pBuf->unNpData.
                                              QoSHwSchedulerHierarchyMap.
                                              u4IfIndex,
                                              pBuf->unNpData.
                                              QoSHwSchedulerHierarchyMap.
                                              u4SchedId,
                                              pBuf->unNpData.
                                              QoSHwSchedulerHierarchyMap.
                                              u2Sweight,
                                              pBuf->unNpData.
                                              QoSHwSchedulerHierarchyMap.
                                              u1Spriority,
                                              pBuf->unNpData.
                                              QoSHwSchedulerHierarchyMap.
                                              u4NextSchedId,
                                              pBuf->unNpData.
                                              QoSHwSchedulerHierarchyMap.
                                              u4NextQId,
                                              pBuf->unNpData.
                                              QoSHwSchedulerHierarchyMap.u2HL,
                                              pBuf->unNpData.
                                              QoSHwSchedulerHierarchyMap.
                                              u1Flag);
                break;
            case NPSYNC_QO_S_HW_SET_DEF_USER_PRIORITY:
                QoSRedHwAuditHandleSetDefUsrPri (pBuf->unNpData.
                                                 QoSHwSetDefUserPriority.u4Port,
                                                 pBuf->unNpData.
                                                 QoSHwSetDefUserPriority.
                                                 u4DefPriority);
                break;
            case NPSYNC_QO_S_HW_MAP_CLASS_TO_QUEUE_ID:
                QoSHwAuditHandleMapClsToQId (pBuf->unNpData.
                                             QoSHwMapClassToQueueId.
                                             u4ClassMapId,
                                             pBuf->unNpData.
                                             QoSHwMapClassToQueueId.u4IfIndex,
                                             pBuf->unNpData.
                                             QoSHwMapClassToQueueId.
                                             u4ClsOrPriType,
                                             pBuf->unNpData.
                                             QoSHwMapClassToQueueId.u4ClsOrPri,
                                             pBuf->unNpData.
                                             QoSHwMapClassToQueueId.u4QId,
                                             pBuf->unNpData.
                                             QoSHwMapClassToQueueId.u1Flag);
                break;
            case NPSYNC_FS_QOS_HW_CONFIG_PFC:
                QosRedHwAuditHandleConfigPfc (pBuf->unNpData.FsQosHwConfigPfc.
                                              i4IfIndex,
                                              pBuf->unNpData.FsQosHwConfigPfc.
                                              u1PfcPriority,
                                              pBuf->unNpData.FsQosHwConfigPfc.
                                              u1Profile,
                                              pBuf->unNpData.FsQosHwConfigPfc.
                                              u1Flag);
                break;
            default:
                break;
        }
        TMO_SLL_Delete (&gQoSGlobalInfo.RedGlobalInfo.HwAuditTable,
                        &(pBuf->Node));
        MemReleaseMemBlock (gQoSGlobalInfo.RedGlobalInfo.HwAuditTablePoolId,
                            (UINT1 *) pBuf);
        pBuf = pTempBuf;
    }

    return;
}

/***********************************************************************
 * FUNCTION NAME    : QoSRedHwAuditHandleMapClsToPlcy  
 *
 * DESCRIPTION       : This routine handles the hardware audit for adding  
 *                       Class to policy Map Entry.Based on the Class Map Entry
 *                       status in the software the hardware is programmed
 *
 * INPUT                : u4ClassMapId -  ClassMapEntry ID     
 *                   : u4PlyMapId -  PolicyMapEntry ID             
 *                   : u1Flag -    QOS_PLY_ADD / QOS_PLY_MAP
 *                        QOS_PLY_ADD - Policy needs to be created and map     
 *                      the Class and Policy                   
 *                      QOS_PLY_MAP - Map the Class with already created     
 *                     Policy.   
 *
 * OUTPUT            : None
 * 
 * RETURNS           : None
 * **********************************************************************/
PUBLIC VOID
     
     
     
     
    QoSRedHwAuditHandleMapClsToPlcy
    (tQoSNpSyncClassMapNode * pSyncClassMapNode, UINT4 u4PlyMapId)
{

    INT4                i4RetStatus = QOS_FAILURE;
    tQoSPolicyMapNode  *pPlyMapNode = NULL;
    tQoSClassMapNode   *pClassMapNode = NULL;
    tQoSClassMapNode    ClassMapNode;
    pPlyMapNode = QoSUtlGetPolicyMapNode (u4PlyMapId);
    pClassMapNode = QoSUtlGetClassMapNode (pSyncClassMapNode->u4ClassId);
    if ((pPlyMapNode != NULL) && (pPlyMapNode->u1Status == ACTIVE))
    {
        i4RetStatus = QoSHwWrMapClassToPolicy (pPlyMapNode,
                                               pClassMapNode, QOS_PLY_MAP);
        if (i4RetStatus != QOS_SUCCESS)
        {
            QOS_TRC_ARG1 (ALL_FAILURE_TRC, "In "
                     "QoSRedHwAuditHandleMapClsToPlcy :"
                     "QoSHwWrMapClassToPolicy () Returns FAILURE"
                     " for PolicyMap Id = %d \r\n", u4PlyMapId);
        }
    }
    else
    {
        MEMSET (&ClassMapNode, QOS_INIT_VAL, sizeof (tQoSClassMapNode));
        ClassMapNode.u4ClassId = pSyncClassMapNode->u4ClassId;
        ClassMapNode.u4L2FilterId = pSyncClassMapNode->u4L2FilterId;
        ClassMapNode.u4L3FilterId = pSyncClassMapNode->u4L3FilterId;
        ClassMapNode.u4PriorityMapId = pSyncClassMapNode->u4PriorityMapId;
        i4RetStatus = QoSHwWrDeleteClassMapEntry (&ClassMapNode);
        if (i4RetStatus != QOS_SUCCESS)
        {
            QOS_TRC_ARG1 (ALL_FAILURE_TRC, "In"
                     "QoSRedHwAuditHandleMapClsToPlcy :"
                     "QoSHwWrDeleteClassMapEntry () Returns FAILURE"
                     " for PolicyMap Id = %d \r\n", u4PlyMapId);
        }

    }
    return;
}

/************************************************************************
 * FUNCTION NAME    : QoSHwAuditHandleDelClsMapEntry  
 *
 * DESCRIPTION      : This routine handles the hardware audit for a 
 *                    class Map Entry.Based on the class Map Entry status 
 *                    in the software the hardware is programmed
 *
 * INPUT                  : u4ClassMapId    --    ClassMapEntry ID 
 *                      
 *
 * OUTPUT              : None
 * 
 * RETURNS             : None
 * 
 ************************************************************************/
PUBLIC VOID
QoSHwAuditHandleDelClsMapEntry (tQoSNpSyncClassMapNode * pSyncClassMapNode)
{

    INT4                i4RetStatus = QOS_FAILURE;
    tQoSClassMapNode   *pClassMapNode = NULL;
    tQoSClassMapNode    ClassMapNode;

    pClassMapNode = QoSUtlGetClassMapNode (pSyncClassMapNode->u4ClassId);
    if (pClassMapNode != NULL)
    {
        i4RetStatus = QoSDeleteClsMapTblEntry (pClassMapNode);
        if (i4RetStatus != QOS_SUCCESS)
        {
            QOS_TRC (ALL_FAILURE_TRC, "In"
                     "  QoSHwAuditHandleDelClsMapEntry :"
                     "QoSDeleteClsMapTblEntry () Returns FAILURE. \r\n");
        }

    }
    else
    {
        MEMSET (&ClassMapNode, QOS_INIT_VAL, sizeof (tQoSClassMapNode));
        ClassMapNode.u4ClassId = pSyncClassMapNode->u4ClassId;
        ClassMapNode.u4L2FilterId = pSyncClassMapNode->u4L2FilterId;
        ClassMapNode.u4L3FilterId = pSyncClassMapNode->u4L3FilterId;
        ClassMapNode.u4PriorityMapId = pSyncClassMapNode->u4PriorityMapId;
        i4RetStatus = QoSHwWrDeleteClassMapEntry (&ClassMapNode);
        if (i4RetStatus != QOS_SUCCESS)
        {
            QOS_TRC (ALL_FAILURE_TRC, "In"
                     "  QoSHwAuditHandleDelClsMapEntry :"
                     "QoSHwWrDeleteClassMapEntry () Returns FAILURE. \r\n");
        }
    }
    return;
}

/************************************************************************
 * FUNCTION NAME    : QoSRedHwAuditHandleMeterAudit  
 *
 * DESCRIPTION      : This routine handles the hardware audit for a Meter Entry.
 *                    Based on the Meter  Entry status in the software the 
 *                    hardware     is programmed
 *
 * INPUT               : u4MeterId    --    Meter Map Identifier
 *                    :  u1MeterStatus    --    Meter Map Status
 *
 * OUTPUT           : None
 * 
 * RETURNS          : None
 * 
 **************************************************************************/
PUBLIC VOID
QoSRedHwAuditHandleMeterAudit (INT4 u4MeterId)
{
    INT4                i4RetStatus = QOS_FAILURE;
    UINT4               u4ErrorCode = SNMP_ERR_NO_ERROR;
    tQoSMeterNode      *pMeterNode = NULL;
    i4RetStatus = QoSUtlValidateMeter (u4MeterId, &u4ErrorCode);
    /* if entry is present in S/W table */
    pMeterNode = QoSUtlGetMeterNode (u4MeterId);
    if (pMeterNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In"
                 "QoSRedHwAuditHandleMeterAudit() :"
                 "ERROR: QoSUtlGetMeterNode ()  Returns FAILURE"
                 " for Meter Id = %d \r\n", u4MeterId);
        return;
    }
    if ((i4RetStatus != QOS_FAILURE) && (pMeterNode->u1Status == ACTIVE))
    {
        QOS_TRC_ARG1 (ALL_FAILURE_TRC, "In "
                 "QoSRedHwAuditHandleMeterAudit() :"
                 "ERROR:Meter entry is  present in S/W table"
                 " for Meter Id = %d \r\n", u4MeterId);
        i4RetStatus = QoSHwWrMeterCreate (pMeterNode);
        if (i4RetStatus != QOS_SUCCESS)
        {
            QOS_TRC_ARG1 (ALL_FAILURE_TRC, "In"
                     "QoSRedHwAuditHandleMeterAudit() :"
                     "ERROR: QoSHwMeterCreate ()  Returns FAILURE"
                     " for Meter Id = %d \r\n", u4MeterId);
        }
    }
    else
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In"
                 " QoSRedHwAuditHandleMeterAudit() : ERROR:"
                 "Meter entry is not present in S/W table"
                 " for Meter Id = %d \r\n", u4MeterId);
#ifdef NPAPI_WANTED
        i4RetStatus = QosxQoSHwMeterDelete (u4MeterId);
        if (i4RetStatus != FNP_SUCCESS)
        {
            QOS_TRC_ARG1 (ALL_FAILURE_TRC, "In"
                     "QoSRedHwAuditHandleMeterAudit() : ERROR:"
                     "QoSHwMeterDelete ()  Returns FAILURE"
                     " for Meter Id = %d \r\n", u4MeterId);
        }
#else
        UNUSED_PARAM (u4MeterId);
#endif
    }
    return;
}

/*************************************************************************
 * FUNCTION NAME    : QoSRedHwAuditHandleSchedAudit  
 *
 * DESCRIPTION      : This routine handles the hardware audit for a
 *                    scheduler Table  Entry.Based on the scheduler Entry
 *                    status in the software the hardware is programmed
 *
 * INPUT               : i4QosIfIndex        --    Interface IDr
 *                      i4QosSchedulerId    --    Scheduler  Table Identifier
 *                    
 *
 * OUTPUT           : None
 * 
 * RETURNS          : None
 * 
 ************************************************************************/
PUBLIC VOID
QoSRedHwAuditHandleSchedAudit (INT4 i4QosIfIndex, INT4 i4QosSchedulerId)
{
    INT4                i4RetStatus = QOS_FAILURE;
    UINT4               u4ErrorCode = SNMP_ERR_NO_ERROR;
    tQoSSchedNode      *pSchedNode = NULL;

    pSchedNode = QoSUtlGetSchedNode (i4QosIfIndex, i4QosSchedulerId);
    i4RetStatus = QoSUtlValidateSched (i4QosIfIndex,
                                       i4QosSchedulerId, &u4ErrorCode);
    if (pSchedNode == NULL)
    {
        return;
    }
    if ((i4RetStatus != QOS_FAILURE) && (pSchedNode->u1Status == ACTIVE))
        /* if failed scheduler entry is present in s/w table so add
           entry in H/W */
    {
        QOS_TRC_ARG2 (MGMT_TRC,
                 "In QoSRedHwAuditHandleSchedAudit()"
                 " Entry is  present in S/W table "
                 " with Interface = %d and Scheduler Id = %d \r\n",
                 i4QosIfIndex, i4QosSchedulerId );
        i4RetStatus = QoSHwWrSchedulerAdd (i4QosIfIndex, i4QosSchedulerId);
        if (i4RetStatus != QOS_SUCCESS)
        {
            QOS_TRC_ARG2 (ALL_FAILURE_TRC,
                     "In QoSRedHwAuditHandleSchedAudit():"
                     "QoSHwWrSchedulerAdd () Returns FAILURE"
                     " for Interface = %d and Scheduler Id = %d \r\n",
                     i4QosIfIndex, i4QosSchedulerId );
        }

    }
    else                        /* Schedule entry is not present in s/w table so delete entry in H/W */
    {
        QOS_TRC_ARG2 (MGMT_TRC,
                 "In QoSRedHwAuditHandleSchedAudit()"
                 " Entry is not present in S/W table"
                 " for Interface = %d and Scheduler Id = %d \r\n",
                 i4QosIfIndex, i4QosSchedulerId );
        i4RetStatus = QoSHwWrSchedulerDelete (i4QosIfIndex, i4QosSchedulerId);
        if (i4RetStatus != QOS_SUCCESS)
        {
            QOS_TRC_ARG2 (ALL_FAILURE_TRC,
                     "In QoSRedHwAuditHandleSchedAudit():"
                     "QoSHwWrSchedulerDelete () Returns FAILURE"
                     " for Interface = %d and Scheduler Id = %d \r\n",
                     i4QosIfIndex, i4QosSchedulerId );
        }

    }
    return;

}

/***************************************************************************
 * FUNCTION NAME    : QosRedHwAuditHandleQueueAudit  
 *
 * DESCRIPTION      : This routine handles the hardware audit for a 
 *                    Queue Table  Entry.Based on the Queue Entry status
 *                    in the software the hardware is programmed
 *
 * INPUT               : i4IfIndex        --    Interface Index
 *                      u4QId        --    Queue  Table Identifier
 *
 * OUTPUT           : None
 * 
 * RETURNS          : None
 * 
 **************************************************************************/

PUBLIC VOID
QosRedHwAuditHandleQueueAudit (INT4 i4IfIndex, UINT4 u4QId)
{
    UINT4               u4RetStatus = QOS_INIT_VAL;
    tQoSQNode          *pQNode = NULL;

    pQNode = QoSUtlGetQNode (i4IfIndex, u4QId);
    if ((pQNode != NULL) && (pQNode->u1Status == ACTIVE))
    {
        /* Entry found in the Table */
        u4RetStatus = QoSHwWrQueueCreate (i4IfIndex, u4QId);
        if (u4RetStatus != QOS_SUCCESS)
        {
            QOS_TRC_ARG2 (ALL_FAILURE_TRC,
                     "In QosRedHwAuditHandleQueueAudit :"
                     "ERROR: QoSHwQueueCreate ()  Returns FAILURE"
                     " for Interface = %d and Queue Id = %d \r\n",
                     i4IfIndex, u4QId);
        }
    }
    else
    {
        u4RetStatus = QoSHwWrQueueDelete (i4IfIndex, u4QId);
        if (u4RetStatus != QOS_SUCCESS)
        {
            QOS_TRC_ARG2 (ALL_FAILURE_TRC,
                     "In QosRedHwAuditHandleQueueAudit :"
                     "ERROR: QoSHwQueueDelete ()  Returns FAILURE"
                     " for Interface = %d and Queue Id = %d \r\n",
                     i4IfIndex, u4QId);
        }
    }
    return;
}

/***************************************************************************
 * FUNCTION NAME    : QosRedHwAuditHandleQmapAudit  
 *
 * DESCRIPTION      : This routine handles the hardware audit for a
 *                    QMap Table  Entry.Based on the QMap Entry status in the
 *                    software the hardware    is programmed
 *
 * Input(s)            : i4IfIndex   - Scheduler and Q Interface.            
 *                  : i4ClsOrPriType - Map Type of the 'u4ClsOrPri'       
 *                        NONE  means CLASS  otherwise                
 *                        VLAN_PRI/IP_TOS/IP_DSCP/MPLS_EXP/VLAN_DEI   
 *                  : u4ClsOrPri  - Map Type Value                         
 *                  : u4QId       - Q id                                   
 *                    : u1Flag      - Map / Unmap                            
 *                                                                               
 *
 * OUTPUT           : None
 * 
 * RETURNS          : None
 * 
 **************************************************************************/

PUBLIC VOID
     
     
     
     
    QosRedHwAuditHandleQmapAudit
    (INT4 i4IfIndex, INT4 i4ClsOrPriType, UINT4 u4ClsOrPri,
     UINT4 u4QId, UINT1 u1Flag)
{
    UINT4               u4RetStatus = QOS_INIT_VAL;
    UINT4               u4ErrorCode = SNMP_ERR_NO_ERROR;
    UNUSED_PARAM (u1Flag);
    u4RetStatus = QoSUtlValidateQMapQId (i4IfIndex, u4QId, &u4ErrorCode);
    if (u4RetStatus != QOS_FAILURE)
    {
        QOS_TRC_ARG4 (MGMT_TRC,
                 "In QosRedHwAuditHandleQmapAudit() : entry is found "
                 " in S/W table. Add entry in H/W with"
                 " Interface = %d Priority Type = %d Priority = %d Queue Id = %d \r\n",
                 i4IfIndex, i4ClsOrPriType, u4ClsOrPri, u4QId);
#ifdef NPAPI_WANTED
        u4RetStatus = QoSHwMapClassToQueue (i4IfIndex,
                                            i4ClsOrPriType, u4ClsOrPri, u4QId,
                                            QOS_QMAP_ADD);
        if (u4RetStatus != FNP_SUCCESS)
        {
            QOS_TRC_ARG4 (ALL_FAILURE_TRC,
                     "In QosRedHwAuditHandleQmapAudit() : "
                     "QoSHwWrMapClassToQueue() :" "Add Returns FAILURE "
                     " for Interface = %d Priority Type = %d Priority = %d Queue Id = %d \r\n",
                     i4IfIndex, i4ClsOrPriType, u4ClsOrPri, u4QId);

        }
#else
        UNUSED_PARAM (i4IfIndex);
        UNUSED_PARAM (i4ClsOrPriType);
        UNUSED_PARAM (u4ClsOrPri);
        UNUSED_PARAM (u4QId);
        UNUSED_PARAM (u1Flag);
#endif
    }
    else
    {
        QOS_TRC_ARG4 (MGMT_TRC,
                 "In QosRedHwAuditHandleQmapAudit() :"
                 "entry is not found  in S/W table."
                 "Remove entry in H/W"
                 " for Interface = %d Priority Type = %d Priority = %d Queue Id = %d \r\n",
                 i4IfIndex, i4ClsOrPriType, u4ClsOrPri, u4QId);
#ifdef NPAPI_WANTED
        u4RetStatus = QoSHwMapClassToQueue (i4IfIndex, i4ClsOrPriType,
                                            u4ClsOrPri, u4QId, QOS_QMAP_DEL);
        if (u4RetStatus != FNP_SUCCESS)
        {
            QOS_TRC_ARG4 (ALL_FAILURE_TRC,
                     "In QosRedHwAuditHandleQmapAudit() : "
                     "QoSHwWrMapClassToQueue() : "
                     "Del Returns FAILURE "
                     "for Interface = %d Priority Type = %d Priority = %d Queue Id = %d \r\n",
                     i4IfIndex, i4ClsOrPriType, u4ClsOrPri, u4QId);
        }
#else
        UNUSED_PARAM (i4IfIndex);
        UNUSED_PARAM (i4ClsOrPriType);
        UNUSED_PARAM (u4ClsOrPri);
        UNUSED_PARAM (u4QId);
        UNUSED_PARAM (u1Flag);
#endif
    }
    return;
}

/*******************************************************************
 * FUNCTION NAME    : QoSRedHwAuditHandleSetDefUsrPri  
 *
 * DESCRIPTION      : This routine handles the hardware audit for a Default
 *                    User Priority Table  Entry. Based on the Default User 
 *                    Priority Entry status in the software the hardware is 
 *                    programmed
 *
 * Input(s)           : i4Port        - Interface Index                      
 *                  : i4DefPriority - Default Priority for the given Port 
 *
 * OUTPUT           : None
 * 
 * RETURNS          : None
 * 
 *********************************************************************/
PUBLIC VOID
QoSRedHwAuditHandleSetDefUsrPri (INT4 i4Port, INT4 i4DefPriority)
{
    UINT1              *pu1PriVal = NULL;
    INT4                i4RetStatus = QOS_FAILURE;

    pu1PriVal = QoSUtlGetDUPNode (i4Port);
    if (pu1PriVal != NULL)
    {
#ifdef NPAPI_WANTED
        i4RetStatus = QosxQoSHwSetDefUserPriority (i4Port, i4DefPriority);
        if (i4RetStatus != FNP_SUCCESS)
        {
            QOS_TRC_ARG2 (ALL_FAILURE_TRC,
                     "In QoSRedHwAuditHandleSetDefUsrPri() :"
                     "QoSHwWrSetDefUserPriority () Returns FAILURE "
                     "for Port = %d & Default Priority = %d \r\n",
                      i4Port, i4DefPriority);
        }
#else
        UNUSED_PARAM (i4Port);
        UNUSED_PARAM (i4RetStatus);
        UNUSED_PARAM (i4DefPriority);
#endif
    }

    return;
}

/*****************************************************************
 * FUNCTION NAME    : QoSRedHwAuditHandleHierAudit  
 *
 * DESCRIPTION      : This routine handles the hardware audit for a Scheduler
 *                       Hierarchy Table  Entry. Based on the Scheduler Hierarchy
 *                       Entry status in the software the hardware
 *                       is programmed
 *
 * Input(s)         : i4IfIndex     - Interface Index                      
 *                  : u4SchedId     - Scheduler Id                         
 *                  : u2Sweight     - Scheduler Weight for NextLevel       
 *                  : u1Spriority   - Scheduler Priority for NextLevel     
 *                  : i4NextSchedId - NextLevel Scheduler Id               
 *                  : i4NextQId     - NextLevel Q Id                       
 *                  : i2HL          - Hierarchy Level                      
 *                  : u1Flag        - Map(1) or UnMap(2)                   
 * OUTPUT           : None
 * 
 * RETURNS          : None
 * 
 ******************************************************************/

PUBLIC VOID
QoSRedHwAuditHandleHierAudit (INT4 i4IfIndex,
                              INT4 i4SchedulerId, UINT2 u2Sweight,
                              UINT1 u1Spriority, INT4 i4NextSchedId,
                              INT4 i4NextQId, UINT2 u2HL, UINT1 u1Flag)
{
    tQoSHierarchyNode  *pHierarchyNode = NULL;
    INT4                i4RetStatus = QOS_FAILURE;
    UNUSED_PARAM (u1Flag);

    pHierarchyNode = QoSUtlGetHierarchyNode (i4IfIndex,
                                             (UINT4) u2HL, i4SchedulerId);
    /* if entry is present in s/w table */
    if ((pHierarchyNode != NULL) && (pHierarchyNode->u1Status == ACTIVE))
    {
        i4RetStatus = QoSHwWrSchedulerHierarchyMap (i4IfIndex, i4SchedulerId,
                                                    u2Sweight, u1Spriority,
                                                    i4NextSchedId, i4NextQId,
                                                    (UINT1) u2HL,
                                                    QOS_HIERARCHY_ADD);
        if (i4RetStatus != QOS_SUCCESS)
        {
            QOS_TRC_ARG5 (ALL_FAILURE_TRC, "In "
                     "QoSRedHwAuditHandleHierAudit() : "
                     "QoSHwWrSchedulerHierarchyMap() : "
                     "Add Returns FAILURE "
                     "for Interface = %d Scheduler Id = %d Weight = %d "
                     "Priority = %d Heirarchy Level = %d \r\n",
                     i4IfIndex, i4SchedulerId, u2Sweight, u1Spriority, u2HL);
        }
    }
    else
    {
        i4RetStatus = QoSHwWrSchedulerHierarchyMap (i4IfIndex, i4SchedulerId,
                                                    u2Sweight, u1Spriority,
                                                    i4NextSchedId, i4NextQId,
                                                    (UINT1) u2HL,
                                                    QOS_HIERARCHY_DEL);
        if (i4RetStatus != QOS_SUCCESS)
        {
            QOS_TRC_ARG5 (ALL_FAILURE_TRC, "In"
                     "QoSRedHwAuditHandleHierAudit() : "
                     "QoSHwWrSchedulerHierarchyMap() :"
                     "Delete Returns FAILURE "
                     "for Interface = %d Scheduler Id = %d Weight = %d "
                     "Priority = %d Heirarchy Level = %d \r\n",
                     i4IfIndex, i4SchedulerId, u2Sweight, u1Spriority, u2HL);
        }
    }
    return;
}

/***************************************************************************
 * FUNCTION NAME    : QoSHwAuditHandleMapClsToQId  
 *
 * DESCRIPTION      : This routine handles the hardware audit for class 
 *                    to queueID Entry.Based on the class to queueID Entry
 *                    status in the software the hardware is programmed
 *
 * INPUT               : u4ClassMapId        --    Class Map Identifier
 *                    : i4IfIndex   - Scheduler and Q Interface.            
 *                  : i4ClsOrPriType - Map Type of the 'u4ClsOrPri'       
 *                              NONE  means CLASS  otherwise                
 *                             VLAN_PRI/IP_TOS/IP_DSCP/MPLS_EXP/VLAN_DEI   
 *                  : u4ClsOrPri  - Map Type Value                         
 *                  : u4QId       - Q id                                   
 *                    : u1Flag      - Map / Unmap     
 *
 * OUTPUT           : None
 * 
 * RETURNS          : None
 * 
 **************************************************************************/
PUBLIC VOID
QoSHwAuditHandleMapClsToQId (UINT4 u4ClassMapId,
                             INT4 u4IfIndex, INT4 i4ClsOrPriType,
                             UINT4 u4ClsOrPri, UINT4 u4QId, UINT1 u1Flag)
{
    INT4                i4RetStatus = QOS_INIT_VAL;
    UINT4               u4ErrorCode = SNMP_ERR_NO_ERROR;
    tQoSClassMapEntry  *pClassMapEntry = NULL;
    tQoSClassMapNode   *pClassMapNode = NULL;

    UNUSED_PARAM (u1Flag);

    pClassMapNode = QoSUtlGetClassMapNode (u4ClassMapId);
    if (pClassMapNode == NULL)
    {
        QOS_TRC_ARG4 (MGMT_TRC,
                 "QoSHwAuditHandleMapClsToQId():"
                 "QoSUtlGetClassMapNode() returns NULL "
                 "for Class Id = %d Interface = %d Priority Type = %d Queue Id = %d\r\n",
                  u4ClassMapId, u4IfIndex, i4ClsOrPriType, u4QId);
        return;
    }
#ifdef NPAPI_WANTED
    pClassMapEntry = QoSFiltersForClassMapEntry (pClassMapNode);
    if (pClassMapEntry == NULL)
    {
        QOS_TRC_ARG4 (MGMT_TRC,
                 "QoSHwAuditHandleMapClsToQId():"
                 "QoSFiltersForClassMapEntry() returns NULL "
                 "for Class Id = %d Interface = %d Priority Type = %d Queue Id = %d\r\n",
                  u4ClassMapId, u4IfIndex, i4ClsOrPriType, u4QId);
        return;
    }
#endif

    i4RetStatus = QoSUtlValidateQMapQId (u4IfIndex, u4QId, &u4ErrorCode);
    if (i4RetStatus != QOS_FAILURE)
    {
        QOS_TRC_ARG4 (MGMT_TRC, "In"
                 "QoSHwAuditHandleMapClsToQId() :"
                 "entry is found  in S/W table.Add entry in to H/W "
                 "with Class Id = %d Interface = %d Priority Type = %d Queue Id = %d\r\n",
                  u4ClassMapId, u4IfIndex, i4ClsOrPriType, u4QId);
#ifdef NPAPI_WANTED
        i4RetStatus = QosxQoSHwMapClassToQueueId (pClassMapEntry, u4IfIndex,
                                                  i4ClsOrPriType, u4ClsOrPri,
                                                  u4QId, QOS_QMAP_ADD);
        if (i4RetStatus != FNP_SUCCESS)
        {
            QOS_TRC_ARG4 (ALL_FAILURE_TRC,
                     "In QoSHwAuditHandleMapClsToQId() : "
                     "QoSHwWrMapClassToQueue() :" "Add Returns FAILURE "
                     "for Class Id = %d Interface = %d Priority Type = %d Queue Id = %d\r\n",
                     u4ClassMapId, u4IfIndex, i4ClsOrPriType, u4QId);

        }
#else
        UNUSED_PARAM (u4ClassMapId);
        UNUSED_PARAM (pClassMapEntry);
        UNUSED_PARAM (i4ClsOrPriType);
        UNUSED_PARAM (u4ClsOrPri);
        UNUSED_PARAM (u4IfIndex);
        UNUSED_PARAM (u4QId);
#endif
    }
    else
    {
#ifdef NPAPI_WANTED
        QOS_TRC_ARG4 (MGMT_TRC,
                 "In QoSHwAuditHandleMapClsToQId() :"
                 "entry is not found  in S/W table.Remove entry in to H/W "
                 "with Class Id = %d Interface = %d Priority Type = %d Queue Id = %d\r\n",
                  u4ClassMapId, u4IfIndex, i4ClsOrPriType, u4QId);

        i4RetStatus = QosxQoSHwMapClassToQueueId (pClassMapEntry, u4IfIndex,
                                                  i4ClsOrPriType, u4ClsOrPri,
                                                  u4QId, QOS_QMAP_DEL);
        if (i4RetStatus != FNP_SUCCESS)
        {
            QOS_TRC_ARG4 (ALL_FAILURE_TRC,
                     "In QoSHwAuditHandleMapClsToQId() : "
                     "QoSHwWrMapClassToQueue() :" "Del  Returns FAILURE "
                     "for Class Id = %d Interface = %d Priority Type = %d Queue Id = %d\r\n",
                      u4ClassMapId, u4IfIndex, i4ClsOrPriType, u4QId);
        }
#else
        UNUSED_PARAM (u4ClassMapId);
        UNUSED_PARAM (i4ClsOrPriType);
        UNUSED_PARAM (u4ClsOrPri);
        UNUSED_PARAM (u4IfIndex);
        UNUSED_PARAM (u4QId);
#endif
    }
    return;
}

/***************************************************************************
 * FUNCTION NAME    : QosRedHwAuditHandleConfigPfc  
 *
 * DESCRIPTION      : This routine handles the hardware audit for a PFC
 *                    Config  Entry.Based on the PFC Profile  status in the
 *                    software the hardware is programmed
 *
 * INPUT               :  i4IfIndex     --    Interface Index    
 *                       u1PfcPriority --    Priority for which PFC is 
 *                       enabled/disabled 
 *                       u1Profile        --    Hardware Profile Id
 *                       u1Flag        --    Hardware Flag 
 *
 * OUTPUT           : None
 * 
 * RETURNS          : None
 * 
 **************************************************************************/
PUBLIC VOID
QosRedHwAuditHandleConfigPfc (UINT4 i4IfIndex, UINT1 u1PfcPriority,
                              UINT1 u1Profile, UINT1 u1Flag)
{
#ifdef DCBX_WANTED
    tQosPfcHwEntry      PfcHwEntry;

    MEMSET (&PfcHwEntry, QOS_INIT_VAL, sizeof (tQosPfcHwEntry));

    PfcHwEntry.u4Ifindex = (UINT4) i4IfIndex;
    PfcHwEntry.u1PfcProfileBmp = u1Profile;
    PfcHwEntry.u1PfcPriority = u1PfcPriority;
    PfcHwEntry.u1PfcHwCfgFlag = u1Flag;

    DcbxApiGetPfcInfo (PfcHwEntry.u1PfcProfileBmp,
                       &(PfcHwEntry.u1PfcHwCfgFlag),
                       &(PfcHwEntry.u4PfcMinThreshold),
                       &(PfcHwEntry.u4PfcMaxThreshold),
                       &(PfcHwEntry.i4PfcHwProfileId),
                       DCBX_PFC_FILL_PROFILE_ENTRY);
    if (PfcHwEntry.u1PfcHwCfgFlag == QOS_PFC_PORT_CFG)
    {
        QosxUtlFillPriToQidMap (PfcHwEntry.u4Ifindex,
                                PfcHwEntry.au4PriToQueMap);
    }
#ifdef NPAPI_WANTED
#undef FsQosHwConfigPfc
    QosxFsQosHwConfigPfc (&PfcHwEntry);
#endif
    DcbxApiGetPfcInfo (PfcHwEntry.u1PfcProfileBmp,
                       &(PfcHwEntry.u1PfcHwCfgFlag),
                       &(PfcHwEntry.u4PfcMinThreshold),
                       &(PfcHwEntry.u4PfcMaxThreshold),
                       &(PfcHwEntry.i4PfcHwProfileId),
                       DCBX_PFC_UPDATE_PROFILE_ID);
#else
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (u1PfcPriority);
    UNUSED_PARAM (u1Profile);
    UNUSED_PARAM (u1Flag);
#endif
    return;
}

#endif /* _QOSRED_C_ */
/*-----------------------------------------------------------------------*/
/*                       End of the file  qosred.c                      */
/*-----------------------------------------------------------------------*/
