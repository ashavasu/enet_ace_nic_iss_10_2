/********************************************************************
 * Copyright (C) 2011 Aricent Inc . All Rights Reserved
 *
 * $Id: qossz.c,v 1.1 2014/01/24 12:17:14 siva Exp $
 *
 *******************************************************************/

#define _QOSXSZ_C
#include "qosinc.h"
extern INT4  IssSzRegisterModuleSizingParams( CHR1 *pu1ModName, tFsModSizingParams *pModSizingParams); 
INT4  QosxSizingMemCreateMemPools()
{
    INT4 i4RetVal;
    INT4 i4SizingId;

    for( i4SizingId=0; i4SizingId < QOSX_MAX_SIZING_ID; i4SizingId++) {
        i4RetVal = (INT4) MemCreateMemPool( 
                          FsQOSXSizingParams[i4SizingId].u4StructSize,
                          FsQOSXSizingParams[i4SizingId].u4PreAllocatedUnits,
                          MEM_DEFAULT_MEMORY_TYPE,
                          &(QOSXMemPoolIds[ i4SizingId]));
        if( i4RetVal == (INT4) MEM_FAILURE) {
            QosxSizingMemDeleteMemPools();
            return OSIX_FAILURE;
        }
    }
    return OSIX_SUCCESS;
}


INT4   QosxSzRegisterModuleSizingParams( CHR1 *pu1ModName)
{
      /* Copy the Module Name */ 
       IssSzRegisterModuleSizingParams( pu1ModName, FsQOSXSizingParams); 
      return OSIX_SUCCESS; 
}


VOID  QosxSizingMemDeleteMemPools()
{
    INT4 i4SizingId;

    for( i4SizingId=0; i4SizingId < QOSX_MAX_SIZING_ID; i4SizingId++) {
        if(QOSXMemPoolIds[i4SizingId] != 0)
        {
            MemDeleteMemPool( QOSXMemPoolIds[ i4SizingId] );
            QOSXMemPoolIds[i4SizingId] = 0;
        }
    }
    return;
}
