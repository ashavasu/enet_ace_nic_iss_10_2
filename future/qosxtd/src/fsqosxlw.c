/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsqosxlw.c,v 1.103 2018/01/04 09:55:15 siva Exp $
*
* Description: Protocol Low Level Routines
*********************************************************************/

# include  "qosinc.h"

extern UINT4        fsqosx[9];
extern UINT4        FsQoSSystemControl[12];
PRIVATE VOID        QosxNotifyProtocolShutdownStatus (VOID);
/* Low Level GET Routine for All Objects  */
/****************************************************************************
 Function    :  nmhGetFsQoSSystemControl
 Input       :  The Indices

                The Object retValFsQoSSystemControl
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsQoSSystemControl (INT4 *pi4RetValFsQoSSystemControl)
{
    *pi4RetValFsQoSSystemControl = gQoSGlobalInfo.eSysControl;

    QOS_TRC_ARG2 (MGMT_TRC, "%s: System Control = %s SUCCESS\r\n", __FUNCTION__,
                  (*pi4RetValFsQoSSystemControl) == 1 ? "Start" : "Shutdown");

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFsQoSStatus
 Input       :  The Indices

                The Object 
                retValFsQoSStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsQoSStatus (INT4 *pi4RetValFsQoSStatus)
{
    *pi4RetValFsQoSStatus = gQoSGlobalInfo.eSysStatus;

    QOS_TRC_ARG2 (MGMT_TRC, "%s : System Status = %s SUCCESS \r\n",
                  __FUNCTION__,
                  (*pi4RetValFsQoSStatus) == 1 ? "Enabled " : "Disabled");

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFsQoSTrcFlag
 Input       :  The Indices

                The Object 
                retValFsQoSTrcFlag
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsQoSTrcFlag (UINT4 *pu4RetValFsQoSTrcFlag)
{
    *pu4RetValFsQoSTrcFlag = gQoSGlobalInfo.u4TrcFlag;

    QOS_TRC_ARG2 (MGMT_TRC, "%s: Trace Level = %d SUCCESS \r\n",
                  __FUNCTION__, *pu4RetValFsQoSTrcFlag);

    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhGetFsQoSRateUnit
 Input       :  The Indices

                The Object 
                retValFsQoSRateUnit
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsQoSRateUnit (INT4 *pi4RetValFsQoSRateUnit)
{
    *pi4RetValFsQoSRateUnit = gQoSGlobalInfo.u4RateUnit;

    QOS_TRC_ARG2 (MGMT_TRC, "%s : Rate Unit = %d SUCCESS.\r\n",
                  __FUNCTION__, *pi4RetValFsQoSRateUnit);

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFsQoSRateGranularity
 Input       :  The Indices

                The Object 
                retValFsQoSRateGranularity
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsQoSRateGranularity (UINT4 *pu4RetValFsQoSRateGranularity)
{
    *pu4RetValFsQoSRateGranularity = gQoSGlobalInfo.u4RateGranularity;

    QOS_TRC_ARG2 (MGMT_TRC, "%s : Rate Granularity = %d SUCCESS.\r\n",
                  __FUNCTION__, *pu4RetValFsQoSRateGranularity);

    return (SNMP_SUCCESS);
}

/* Low Level SET Routine for All Objects  */
/****************************************************************************
 Function    :  nmhSetFsQoSSystemControl
 Input       :  The Indices

                The Object 
                setValFsQoSSystemControl
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsQoSSystemControl (INT4 i4SetValFsQoSSystemControl)
{
    INT4                i4RetStats = QOS_FAILURE;

    if (gQoSGlobalInfo.eSysControl == (UINT4) i4SetValFsQoSSystemControl)
    {
        QOS_TRC_ARG2 (MGMT_TRC, "%s : Already in the same status System "
                      "Control = %d SUCCESS.\r\n", __FUNCTION__,
                      i4SetValFsQoSSystemControl);

        return (SNMP_SUCCESS);
    }

    if (i4SetValFsQoSSystemControl == QOS_SYS_CNTL_START)
    {
        i4RetStats = QoSStart ();

        if (i4RetStats != QOS_SUCCESS)
        {
            QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSStart () - FAILED \r\n",
                          __FUNCTION__);

            return (SNMP_FAILURE);
        }

        /* Add Default Table Entries */
        i4RetStats = QoSAddDefTblEntries ();
        if (i4RetStats != QOS_SUCCESS)
        {
            QOS_TRC_ARG1 (MGMT_TRC, "%s : QoSAddDefTblEntries () Failed!.\r\n",
                          __FUNCTION__);

            i4RetStats = QoSShutdown ();
            if (i4RetStats != QOS_SUCCESS)
            {
                QOS_TRC_ARG1 (MGMT_TRC, "%s : QoSShutdown () Failed!.\r\n",
                              __FUNCTION__);
            }

            return (SNMP_FAILURE);
        }
    }
    else
    {
        i4RetStats = QoSShutdown ();

        if (i4RetStats != QOS_SUCCESS)
        {
            QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSShutdown () - FAILED \r\n",
                          __FUNCTION__);
            return (SNMP_FAILURE);
        }
        /* Notify MSR with the Qosxtd oids */
        QosxNotifyProtocolShutdownStatus ();
    }

    gQoSGlobalInfo.eSysControl = i4SetValFsQoSSystemControl;

    QOS_TRC_ARG2 (MGMT_TRC, "%s : System Control = %d SUCCESS \r\n",
                  __FUNCTION__, i4SetValFsQoSSystemControl);

    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhSetFsQoSStatus
 Input       :  The Indices

                The Object 
                setValFsQoSStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsQoSStatus (INT4 i4SetValFsQoSStatus)
{
    INT4                i4RetStats = QOS_FAILURE;

    if (gQoSGlobalInfo.eSysStatus == (UINT4) i4SetValFsQoSStatus)
    {
        QOS_TRC_ARG2 (MGMT_TRC, "%s : Already in the same status "
                      "System Status = %d SUCCESS \r\n",
                      __FUNCTION__, i4SetValFsQoSStatus);

        return (SNMP_SUCCESS);

    }

    if (i4SetValFsQoSStatus == QOS_SYS_STATUS_ENABLE)
    {
        i4RetStats = QoSEnable ();

        if (i4RetStats != QOS_SUCCESS)
        {
            QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSEnable () - FAILED. \r\n",
                          __FUNCTION__);

            return (SNMP_FAILURE);
        }

    }
    else
    {
        i4RetStats = QoSDisable ();

        if (i4RetStats != QOS_SUCCESS)
        {
            QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSDisable () - FAILED.\r\n",
                          __FUNCTION__);

            return (SNMP_FAILURE);
        }
    }

    gQoSGlobalInfo.eSysStatus = i4SetValFsQoSStatus;

    QOS_TRC_ARG2 (MGMT_TRC, "%s : System Status = %d SUCCESS.\r\n",
                  __FUNCTION__, i4SetValFsQoSStatus);
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhSetFsQoSTrcFlag
 Input       :  The Indices

                The Object 
                setValFsQoSTrcFlag
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsQoSTrcFlag (UINT4 u4SetValFsQoSTrcFlag)
{

    gQoSGlobalInfo.u4TrcFlag = u4SetValFsQoSTrcFlag;

    QOS_TRC_ARG2 (MGMT_TRC, "%s: Trace Level = %d SUCCESS. \r\n",
                  __FUNCTION__, u4SetValFsQoSTrcFlag);

    return (SNMP_SUCCESS);

}

/* Low Level TEST Routines for All Objects  */
/****************************************************************************
 Function    :  nmhTestv2FsQoSSystemControl
 Input       :  The Indices

                The Object 
                testValFsQoSSystemControl
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsQoSSystemControl (UINT4 *pu4ErrorCode,
                             INT4 i4TestValFsQoSSystemControl)
{
    if ((i4TestValFsQoSSystemControl != QOS_SYS_CNTL_SHUTDOWN) &&
        (i4TestValFsQoSSystemControl != QOS_SYS_CNTL_START))
    {
        QOS_TRC_ARG2 (MGMT_TRC, "%s : System Control= %d FAILED.\r\n",
                      __FUNCTION__, i4TestValFsQoSSystemControl);

        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;

        CLI_SET_ERR (QOS_CLI_ERR_INVALID_SYS_CONTROL);

        return (SNMP_FAILURE);
    }

    QOS_TRC_ARG2 (MGMT_TRC, "%s : System Control= %d SUCCESS.\r\n",
                  __FUNCTION__, i4TestValFsQoSSystemControl);

    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhTestv2FsQoSStatus
 Input       :  The Indices

                The Object 
                testValFsQoSStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsQoSStatus (UINT4 *pu4ErrorCode, INT4 i4TestValFsQoSStatus)
{

    if (gQoSGlobalInfo.eSysControl != QOS_SYS_CNTL_START)
    {
        QOS_TRC (MGMT_TRC, QOS_MODULE_SHUTDOW_ERROR_MSG);

        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;

        CLI_SET_ERR (QOS_CLI_ERR_MODULE_SHUTDOW);

        return (SNMP_FAILURE);
    }

    if ((i4TestValFsQoSStatus != QOS_SYS_STATUS_DISABLE) &&
        (i4TestValFsQoSStatus != QOS_SYS_STATUS_ENABLE))
    {
        QOS_TRC_ARG2 (MGMT_TRC, "%s : System Status %d FAILED.\r\n",
                      __FUNCTION__, i4TestValFsQoSStatus);

        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;

        CLI_SET_ERR (QOS_CLI_ERR_INVALID_SYS_STATUS);

        return (SNMP_FAILURE);

    }

    QOS_TRC_ARG2 (MGMT_TRC, "%s : System Status = %d SUCCESS.\r\n",
                  __FUNCTION__, i4TestValFsQoSStatus);

    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhTestv2FsQoSTrcFlag
 Input       :  The Indices

                The Object 
                testValFsQoSTrcFlag
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsQoSTrcFlag (UINT4 *pu4ErrorCode, UINT4 u4TestValFsQoSTrcFlag)
{
    if (u4TestValFsQoSTrcFlag > QOS_TRC_MAX_LEVEL)
    {
        QOS_TRC_ARG2 (MGMT_TRC, "%s : Trace Level = %d FAILED.\r\n",
                      __FUNCTION__, u4TestValFsQoSTrcFlag);

        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;

        CLI_SET_ERR (QOS_CLI_ERR_INVALID_TRC_FLAG);

        return (SNMP_FAILURE);
    }

    QOS_TRC_ARG2 (MGMT_TRC, "%s : Trace Level = %d SUCCESS.\r\n",
                  __FUNCTION__, u4TestValFsQoSTrcFlag);

    return (SNMP_SUCCESS);
}

/* LOW LEVEL Routines for Table : FsQoSPriorityMapTable. */
/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsQoSPriorityMapTable
 Input       :  The Indices
                FsQoSPriorityMapID
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsQoSPriorityMapTable (UINT4 u4FsQoSPriorityMapID)
{
    INT4                i4RetStatus = 0;
    tQoSInPriorityMapNode *pPriMapNode = NULL;

    if (gQoSGlobalInfo.eSysControl != QOS_SYS_CNTL_START)
    {
        QOS_TRC (MGMT_TRC, QOS_MODULE_SHUTDOW_ERROR_MSG);
        return (SNMP_FAILURE);
    }

    /* Check the Range of Index Values (Min-Max) */
    i4RetStatus = QOS_CHECK_PRI_MAP_TBL_INDEX_RANGE (u4FsQoSPriorityMapID);

    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG3 (MGMT_TRC, "%s : Priority Map Id %d is out of Range."
                      " The Range Should be (1-%d). \r\n", __FUNCTION__,
                      u4FsQoSPriorityMapID, QOS_PRI_MAP_TBL_MAX_INDEX_RANGE);

        return (SNMP_FAILURE);
    }

    pPriMapNode = QoSUtlGetPriorityMapNode (u4FsQoSPriorityMapID);

    if (pPriMapNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetPriorityMapNode () Returns "
                      "FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    return (SNMP_SUCCESS);

}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsQoSSystemControl
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsQoSSystemControl (UINT4 *pu4ErrorCode,
                            tSnmpIndexList * pSnmpIndexList,
                            tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsQoSStatus
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsQoSStatus (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                     tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsQoSTrcFlag
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsQoSTrcFlag (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                      tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsQoSPriorityMapTable
 Input       :  The Indices
                FsQoSPriorityMapID
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsQoSPriorityMapTable (UINT4 *pu4FsQoSPriorityMapID)
{
    INT4                i4RetStatus = QOS_FAILURE;

    if (gQoSGlobalInfo.eSysControl != QOS_SYS_CNTL_START)
    {
        QOS_TRC (MGMT_TRC, QOS_MODULE_SHUTDOW_ERROR_MSG);
        return (SNMP_FAILURE);
    }

    i4RetStatus = QoSGetNextInPriMapTblEntryIndex (0, pu4FsQoSPriorityMapID);

    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSGetNextInPriMapTblEntryIndex () "
                      "Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsQoSPriorityMapTable
 Input       :  The Indices
                FsQoSPriorityMapID
                nextFsQoSPriorityMapID
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsQoSPriorityMapTable (UINT4 u4FsQoSPriorityMapID,
                                      UINT4 *pu4NextFsQoSPriorityMapID)
{
    INT4                i4RetStatus = QOS_FAILURE;

    if (gQoSGlobalInfo.eSysControl != QOS_SYS_CNTL_START)
    {
        QOS_TRC (MGMT_TRC, QOS_MODULE_SHUTDOW_ERROR_MSG);
        return (SNMP_FAILURE);
    }

    /* Check the Range of Index Values (Min-Max) */
    i4RetStatus = QOS_CHECK_PRI_MAP_TBL_INDEX_RANGE (u4FsQoSPriorityMapID);

    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG3 (MGMT_TRC, "%s : Priority Map Id %d is out of Range."
                      " The Range Should be (1-%d). \r\n", __FUNCTION__,
                      u4FsQoSPriorityMapID, QOS_PRI_MAP_TBL_MAX_INDEX_RANGE);

        return (SNMP_FAILURE);
    }

    i4RetStatus =
        QoSGetNextInPriMapTblEntryIndex (u4FsQoSPriorityMapID,
                                         pu4NextFsQoSPriorityMapID);
    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSGetNextInPriMapTblEntryIndex () "
                      "Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    return (SNMP_SUCCESS);
}

/* Low Level GET Routine for All Objects  */
/****************************************************************************
 Function    :  nmhGetFsQoSPriorityMapName
 Input       :  The Indices
                FsQoSPriorityMapID

                The Object 
                retValFsQoSPriorityMapName
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsQoSPriorityMapName (UINT4 u4FsQoSPriorityMapID,
                            tSNMP_OCTET_STRING_TYPE *
                            pRetValFsQoSPriorityMapName)
{
    tQoSInPriorityMapNode *pPriMapNode = NULL;
    UINT1              *pu1Str = NULL;

    if ((pRetValFsQoSPriorityMapName == NULL) ||
        (pRetValFsQoSPriorityMapName->pu1_OctetList == NULL))
    {
        return (SNMP_FAILURE);
    }

    pPriMapNode = QoSUtlGetPriorityMapNode (u4FsQoSPriorityMapID);

    if (pPriMapNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetPriorityMapNode () "
                      "Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    pu1Str = &(pPriMapNode->au1Name[0]);
    QOS_UTL_COPY_TABLE_NAME (pRetValFsQoSPriorityMapName, pu1Str);

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFsQoSPriorityMapIfIndex
 Input       :  The Indices
                FsQoSPriorityMapID

                The Object 
                retValFsQoSPriorityMapIfIndex
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsQoSPriorityMapIfIndex (UINT4 u4FsQoSPriorityMapID,
                               UINT4 *pu4RetValFsQoSPriorityMapIfIndex)
{
    tQoSInPriorityMapNode *pPriMapNode = NULL;

    pPriMapNode = QoSUtlGetPriorityMapNode (u4FsQoSPriorityMapID);

    if (pPriMapNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetPriorityMapNode () "
                      "Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    *pu4RetValFsQoSPriorityMapIfIndex = pPriMapNode->u4IfIndex;

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFsQoSPriorityMapVlanId
 Input       :  The Indices
                FsQoSPriorityMapID

                The Object 
                retValFsQoSPriorityMapVlanId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsQoSPriorityMapVlanId (UINT4 u4FsQoSPriorityMapID,
                              UINT4 *pu4RetValFsQoSPriorityMapVlanId)
{
    tQoSInPriorityMapNode *pPriMapNode = NULL;

    pPriMapNode = QoSUtlGetPriorityMapNode (u4FsQoSPriorityMapID);

    if (pPriMapNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetPriorityMapNode () "
                      "Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }
    *pu4RetValFsQoSPriorityMapVlanId = (UINT4) (pPriMapNode->u2VlanId);

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFsQoSPriorityMapInPriority
 Input       :  The Indices
                FsQoSPriorityMapID

                The Object 
                retValFsQoSPriorityMapInPriority
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsQoSPriorityMapInPriority (UINT4 u4FsQoSPriorityMapID,
                                  INT4 *pi4RetValFsQoSPriorityMapInPriority)
{
    tQoSInPriorityMapNode *pPriMapNode = NULL;

    pPriMapNode = QoSUtlGetPriorityMapNode (u4FsQoSPriorityMapID);

    if (pPriMapNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetPriorityMapNode () "
                      "Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    *pi4RetValFsQoSPriorityMapInPriority = (INT4) (pPriMapNode->u1InPriority);

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFsQoSPriorityMapInPriType
 Input       :  The Indices
                FsQoSPriorityMapID

                The Object 
                retValFsQoSPriorityMapInPriType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsQoSPriorityMapInPriType (UINT4 u4FsQoSPriorityMapID,
                                 INT4 *pi4RetValFsQoSPriorityMapInPriType)
{
    tQoSInPriorityMapNode *pPriMapNode = NULL;

    pPriMapNode = QoSUtlGetPriorityMapNode (u4FsQoSPriorityMapID);

    if (pPriMapNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetPriorityMapNode () "
                      "Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    *pi4RetValFsQoSPriorityMapInPriType = (INT4) (pPriMapNode->u1InPriType);

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFsQoSPriorityMapRegenPriority
 Input       :  The Indices
                FsQoSPriorityMapID

                The Object 
                retValFsQoSPriorityMapRegenPriority
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsQoSPriorityMapRegenPriority (UINT4 u4FsQoSPriorityMapID,
                                     UINT4
                                     *pu4RetValFsQoSPriorityMapRegenPriority)
{
    tQoSInPriorityMapNode *pPriMapNode = NULL;

    pPriMapNode = QoSUtlGetPriorityMapNode (u4FsQoSPriorityMapID);

    if (pPriMapNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetPriorityMapNode () "
                      "Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    *pu4RetValFsQoSPriorityMapRegenPriority =
        (UINT4) (pPriMapNode->u1RegenPriority);

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFsQoSPriorityMapRegenInnerPriority
 Input       :  The Indices
                FsQoSPriorityMapID

                The Object 
                retValFsQoSPriorityMapRegenInnerPriority
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhGetFsQoSPriorityMapRegenInnerPriority
    (UINT4 u4FsQoSPriorityMapID,
     UINT4 *pu4RetValFsQoSPriorityMapRegenInnerPriority)
{
    tQoSInPriorityMapNode *pPriMapNode = NULL;

    pPriMapNode = QoSUtlGetPriorityMapNode (u4FsQoSPriorityMapID);

    if (pPriMapNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetPriorityMapNode () "
                      "Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    *pu4RetValFsQoSPriorityMapRegenInnerPriority =
        (UINT4) (pPriMapNode->u1RegenInnerPriority);

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFsQoSPriorityMapConfigStatus
 Input       :  The Indices
                FsQoSPriorityMapID

                The Object 
                retValFsQoSPriorityMapConfigStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsQoSPriorityMapConfigStatus (UINT4 u4FsQoSPriorityMapID,
                                    INT4 *pi4RetValFsQoSPriorityMapConfigStatus)
{
    tQoSInPriorityMapNode *pPriMapNode = NULL;

    pPriMapNode = QoSUtlGetPriorityMapNode (u4FsQoSPriorityMapID);

    if (pPriMapNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetPriorityMapNode () "
                      "Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    *pi4RetValFsQoSPriorityMapConfigStatus = (INT4) (pPriMapNode->u1ConfStatus);

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFsQoSPriorityMapStatus
 Input       :  The Indices
                FsQoSPriorityMapID

                The Object 
                retValFsQoSPriorityMapStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsQoSPriorityMapStatus (UINT4 u4FsQoSPriorityMapID,
                              INT4 *pi4RetValFsQoSPriorityMapStatus)
{
    tQoSInPriorityMapNode *pPriMapNode = NULL;

    pPriMapNode = QoSUtlGetPriorityMapNode (u4FsQoSPriorityMapID);

    if (pPriMapNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetPriorityMapNode () "
                      "Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    *pi4RetValFsQoSPriorityMapStatus = (INT4) (pPriMapNode->u1Status);

    return (SNMP_SUCCESS);
}

/****************************************************************************
 * Function    :  nmhGetFsQoSPriorityMapInDEI
 * Input       :  The Indices
 *                FsQoSPriorityMapID
 * 
 *                The Object
 *                retValFsQoSPriorityMapInDEI
 * Output      :  The Get Low Lev Routine Take the Indices &
 *                store the Value requested in the Return val.
 * Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 *****************************************************************************/
INT1
nmhGetFsQoSPriorityMapInDEI (UINT4 u4FsQoSPriorityMapID,
                             INT4 *pi4RetValFsQoSPriorityMapInDEI)
{
    tQoSInPriorityMapNode *pPriMapNode = NULL;

    pPriMapNode = QoSUtlGetPriorityMapNode (u4FsQoSPriorityMapID);

    if (pPriMapNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetPriorityMapNode () "
                      "Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    *pi4RetValFsQoSPriorityMapInDEI = pPriMapNode->i4InDEI;

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFsQoSPriorityMapRegenColor
 Input       :  The Indices
                FsQoSPriorityMapID
 
                The Object
                retValFsQoSPriorityMapRegenColor
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsQoSPriorityMapRegenColor (UINT4 u4FsQoSPriorityMapID,
                                  INT4 *pi4RetValFsQoSPriorityMapRegenColor)
{
    tQoSInPriorityMapNode *pPriMapNode = NULL;

    pPriMapNode = QoSUtlGetPriorityMapNode (u4FsQoSPriorityMapID);

    if (pPriMapNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetPriorityMapNode () "
                      "Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    *pi4RetValFsQoSPriorityMapRegenColor = pPriMapNode->i4RegenColor;

    return (SNMP_SUCCESS);
}

/* Low Level SET Routine for All Objects  */
/****************************************************************************
 Function    :  nmhSetFsQoSPriorityMapName
 Input       :  The Indices
                FsQoSPriorityMapID

                The Object 
                setValFsQoSPriorityMapName
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsQoSPriorityMapName (UINT4 u4FsQoSPriorityMapID,
                            tSNMP_OCTET_STRING_TYPE *
                            pSetValFsQoSPriorityMapName)
{
    tQoSInPriorityMapNode *pPriMapNode = NULL;
    UINT1              *pu1Str = NULL;

    pPriMapNode = QoSUtlGetPriorityMapNode (u4FsQoSPriorityMapID);

    if (pPriMapNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetPriorityMapNode () "
                      "Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    pu1Str = &(pPriMapNode->au1Name[0]);
    QOS_UTL_SET_TABLE_NAME (pu1Str, pSetValFsQoSPriorityMapName);

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhSetFsQoSPriorityMapIfIndex
 Input       :  The Indices
                FsQoSPriorityMapID

                The Object 
                setValFsQoSPriorityMapIfIndex
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsQoSPriorityMapIfIndex (UINT4 u4FsQoSPriorityMapID,
                               UINT4 u4SetValFsQoSPriorityMapIfIndex)
{
    tQoSInPriorityMapNode *pPriMapNode = NULL;

    pPriMapNode = QoSUtlGetPriorityMapNode (u4FsQoSPriorityMapID);

    if (pPriMapNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetPriorityMapNode () "
                      "Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    pPriMapNode->u4IfIndex = u4SetValFsQoSPriorityMapIfIndex;
    pPriMapNode->u1ConfStatus = QOS_PRI_MAP_TBL_CONF_STATUS_MGNT;

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhSetFsQoSPriorityMapVlanId
 Input       :  The Indices
                FsQoSPriorityMapID

                The Object 
                setValFsQoSPriorityMapVlanId
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsQoSPriorityMapVlanId (UINT4 u4FsQoSPriorityMapID,
                              UINT4 u4SetValFsQoSPriorityMapVlanId)
{
    tQoSInPriorityMapNode *pPriMapNode = NULL;

    pPriMapNode = QoSUtlGetPriorityMapNode (u4FsQoSPriorityMapID);

    if (pPriMapNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetPriorityMapNode () "
                      "Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    pPriMapNode->u2VlanId = (UINT2) u4SetValFsQoSPriorityMapVlanId;
    pPriMapNode->u1ConfStatus = QOS_PRI_MAP_TBL_CONF_STATUS_MGNT;

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhSetFsQoSPriorityMapInPriority
 Input       :  The Indices
                FsQoSPriorityMapID

                The Object 
                setValFsQoSPriorityMapInPriority
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsQoSPriorityMapInPriority (UINT4 u4FsQoSPriorityMapID,
                                  INT4 i4SetValFsQoSPriorityMapInPriority)
{
    tQoSInPriorityMapNode *pPriMapNode = NULL;

    pPriMapNode = QoSUtlGetPriorityMapNode (u4FsQoSPriorityMapID);

    if (pPriMapNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetPriorityMapNode () "
                      "Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    pPriMapNode->u1InPriority = (UINT1) i4SetValFsQoSPriorityMapInPriority;
    pPriMapNode->u1ConfStatus = QOS_PRI_MAP_TBL_CONF_STATUS_MGNT;

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhSetFsQoSPriorityMapInPriType
 Input       :  The Indices
                FsQoSPriorityMapID

                The Object 
                setValFsQoSPriorityMapInPriType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsQoSPriorityMapInPriType (UINT4 u4FsQoSPriorityMapID,
                                 INT4 i4SetValFsQoSPriorityMapInPriType)
{
    tQoSInPriorityMapNode *pPriMapNode = NULL;

    pPriMapNode = QoSUtlGetPriorityMapNode (u4FsQoSPriorityMapID);

    if (pPriMapNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetPriorityMapNode () "
                      "Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    pPriMapNode->u1InPriType = (UINT1) i4SetValFsQoSPriorityMapInPriType;
    pPriMapNode->u1ConfStatus = QOS_PRI_MAP_TBL_CONF_STATUS_MGNT;

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhSetFsQoSPriorityMapRegenPriority
 Input       :  The Indices
                FsQoSPriorityMapID

                The Object 
                setValFsQoSPriorityMapRegenPriority
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsQoSPriorityMapRegenPriority (UINT4 u4FsQoSPriorityMapID,
                                     UINT4
                                     u4SetValFsQoSPriorityMapRegenPriority)
{
    tQoSInPriorityMapNode *pPriMapNode = NULL;

    pPriMapNode = QoSUtlGetPriorityMapNode (u4FsQoSPriorityMapID);

    if (pPriMapNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetPriorityMapNode () "
                      "Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    pPriMapNode->u1RegenPriority =
        (UINT1) u4SetValFsQoSPriorityMapRegenPriority;
    pPriMapNode->u1ConfStatus = QOS_PRI_MAP_TBL_CONF_STATUS_MGNT;

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhSetFsQoSPriorityMapRegenInnerPriority
 Input       :  The Indices
                FsQoSPriorityMapID

                The Object 
                setValFsQoSPriorityMapRegenInnerPriority
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhSetFsQoSPriorityMapRegenInnerPriority
    (UINT4 u4FsQoSPriorityMapID,
     UINT4 u4SetValFsQoSPriorityMapRegenInnerPriority)
{
    tQoSInPriorityMapNode *pPriMapNode = NULL;

    pPriMapNode = QoSUtlGetPriorityMapNode (u4FsQoSPriorityMapID);

    if (pPriMapNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetPriorityMapNode () "
                      "Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    pPriMapNode->u1RegenInnerPriority =
        (UINT1) u4SetValFsQoSPriorityMapRegenInnerPriority;
    pPriMapNode->u1ConfStatus = QOS_PRI_MAP_TBL_CONF_STATUS_MGNT;

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhSetFsQoSPriorityMapStatus
 Input       :  The Indices
                FsQoSPriorityMapID

                The Object 
                setValFsQoSPriorityMapStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsQoSPriorityMapStatus (UINT4 u4FsQoSPriorityMapID,
                              INT4 i4SetValFsQoSPriorityMapStatus)
{
    tQoSInPriorityMapNode *pPriMapNode = NULL;
    INT4                i4RetStatus = QOS_FAILURE;
    INT4                i4RowStatus = QOS_FAILURE;
    INT4                i4ReturnValue = SNMP_SUCCESS;

    pPriMapNode = QoSUtlGetPriorityMapNode (u4FsQoSPriorityMapID);
    if (pPriMapNode != NULL)
    {
        i4RowStatus = (UINT4) pPriMapNode->u1Status;

        /* Entry found in the Table */
        if (i4RowStatus == i4SetValFsQoSPriorityMapStatus)
        {
            /* same value */
            return (SNMP_SUCCESS);
        }
    }
    else
    {
        /* New Entry need to be created in the Table, only if
         * i4TestValFsQoSPriorityMapStatus = CREATE_AND_WAIT */
        /* Optional */
        if (i4SetValFsQoSPriorityMapStatus == DESTROY)
        {
            return (SNMP_SUCCESS);
        }
    }

    switch (i4SetValFsQoSPriorityMapStatus)
    {
            /* For a New Entry */
        case CREATE_AND_WAIT:

            pPriMapNode = QoSCreatePriMapTblEntry (u4FsQoSPriorityMapID);

            if (pPriMapNode == NULL)
            {
                QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSCreatePriMapTblEntry () "
                              "Returns FAILURE. \r\n", __FUNCTION__);

                return (SNMP_FAILURE);
            }
            if (pPriMapNode != NULL)
            {
                pPriMapNode->u1Status = NOT_IN_SERVICE;
            }

            i4ReturnValue = SNMP_SUCCESS;

            break;

            /* Modify an Entry */
        case ACTIVE:

            if ((u4FsQoSPriorityMapID <= QOS_SCHED_Q_COUNT_MAX_VAL)
                && (QosPortRmGetNodeState () == RM_STANDBY)
                && (pPriMapNode == NULL))
            {
                /* Default entries will be created via init flow */
                return (SNMP_SUCCESS);
            }
            if (pPriMapNode != NULL)
            {
                /* Add the Unique Entry in the PriorityMap Unique Tbl */
                i4RetStatus = QoSAddPriMapUniTblEntry (pPriMapNode);
                if (i4RetStatus == QOS_FAILURE)
                {
                    QOS_TRC_ARG1 (MGMT_TRC,
                                  "In %s : QoSAddPriMapUniTblEntry () "
                                  "Returns FAILURE. \r\n", __FUNCTION__);
                    return (SNMP_FAILURE);
                }

                i4RetStatus = QoSHwWrMapClasstoPriMap (pPriMapNode, QOS_NP_ADD);
                if ((i4RetStatus == QOS_NP_NOT_SUPPORTED)
                    || (i4RetStatus == QOS_FAILURE))
                {
                    if (i4RetStatus == QOS_NP_NOT_SUPPORTED)
                    {
                        CLI_SET_ERR (QOS_CLI_ERR_INVALID_PRIORITY_VALUE);
                    }
                    QOS_TRC_ARG1 (MGMT_TRC,
                                  "In %s : ERROR: QoSHwWrMapClasstoPriMap () "
                                  " Returns FAILURE. \r\n", __FUNCTION__);
                    return (SNMP_FAILURE);
                }

                pPriMapNode->u1Status = ACTIVE;
            }

            break;

        case NOT_IN_SERVICE:
            if (pPriMapNode != NULL)
            {
                i4RetStatus = QoSHwWrMapClasstoPriMap (pPriMapNode, QOS_NP_DEL);
                if ((i4RetStatus == QOS_NP_NOT_SUPPORTED)
                    || (i4RetStatus == QOS_FAILURE))
                {
                    if (i4RetStatus == QOS_NP_NOT_SUPPORTED)
                    {
                        CLI_SET_ERR (QOS_CLI_ERR_INVALID_PRIORITY_VALUE);
                    }
                    QOS_TRC_ARG1 (MGMT_TRC,
                                  "In %s : ERROR: QoSHwWrMapClasstoPriMap () "
                                  " Returns FAILURE. \r\n", __FUNCTION__);
                    return (SNMP_FAILURE);
                }
            }

            /* 1a. Remove Entry from the Priority Map Unique Table */
            i4RetStatus = QoSDeletePriMapUniTblEntry (pPriMapNode);
            if (i4RetStatus == QOS_FAILURE)
            {
                QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSDeletePriMapUniTblEntry () "
                              "Returns FAILURE. \r\n", __FUNCTION__);
                return (SNMP_FAILURE);
            }
            if (pPriMapNode != NULL)
            {
                pPriMapNode->u1Status = NOT_IN_SERVICE;
            }

            break;

        case DESTROY:
            if (pPriMapNode != NULL)
            {
                i4RetStatus = QoSHwWrMapClasstoPriMap (pPriMapNode, QOS_NP_DEL);
                if ((i4RetStatus == QOS_NP_NOT_SUPPORTED)
                    || (i4RetStatus == QOS_FAILURE))
                {
                    if (i4RetStatus == QOS_NP_NOT_SUPPORTED)
                    {
                        CLI_SET_ERR (QOS_CLI_ERR_INVALID_PRIORITY_VALUE);
                    }
                    QOS_TRC_ARG1 (MGMT_TRC,
                                  "In %s : ERROR: QoSHwWrMapClasstoPriMap () "
                                  " Returns FAILURE. \r\n", __FUNCTION__);
                    return (SNMP_FAILURE);
                }
            }

            i4RetStatus = QoSDeletePriMapTblEntry (pPriMapNode);

            if (i4RetStatus == QOS_FAILURE)
            {
                QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSDeletePriMapTblEntry () "
                              "Returns FAILURE. \r\n", __FUNCTION__);

                return (SNMP_FAILURE);
            }

            break;

            /* Not Supported */
        case CREATE_AND_GO:
            i4ReturnValue = SNMP_FAILURE;
            break;

        default:
            i4ReturnValue = SNMP_FAILURE;
            break;
    }

    return ((INT1) i4ReturnValue);

}

/****************************************************************************
 Function    :  nmhSetFsQoSPriorityMapInDEI
 Input       :  The Indices
                FsQoSPriorityMapID
 
                The Object
                setValFsQoSPriorityMapInDEI
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsQoSPriorityMapInDEI (UINT4 u4FsQoSPriorityMapID,
                             INT4 i4SetValFsQoSPriorityMapInDEI)
{
    tQoSInPriorityMapNode *pPriMapNode = NULL;

    pPriMapNode = QoSUtlGetPriorityMapNode (u4FsQoSPriorityMapID);

    if (pPriMapNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetPriorityMapNode () "
                      "Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    pPriMapNode->i4InDEI = i4SetValFsQoSPriorityMapInDEI;
    pPriMapNode->u1ConfStatus = QOS_PRI_MAP_TBL_CONF_STATUS_MGNT;

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhSetFsQoSPriorityMapRegenColor
 Input       :  The Indices
                FsQoSPriorityMapID
 
                The Object
                setValFsQoSPriorityMapRegenColor
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsQoSPriorityMapRegenColor (UINT4 u4FsQoSPriorityMapID,
                                  INT4 i4SetValFsQoSPriorityMapRegenColor)
{
    tQoSInPriorityMapNode *pPriMapNode = NULL;

    pPriMapNode = QoSUtlGetPriorityMapNode (u4FsQoSPriorityMapID);

    if (pPriMapNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetPriorityMapNode () "
                      "Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    pPriMapNode->i4RegenColor = i4SetValFsQoSPriorityMapRegenColor;
    pPriMapNode->u1ConfStatus = QOS_PRI_MAP_TBL_CONF_STATUS_MGNT;

    return (SNMP_SUCCESS);

}

/* Low Level TEST Routines for All Objects  */
/****************************************************************************
 Function    :  nmhTestv2FsQoSPriorityMapName
 Input       :  The Indices
                FsQoSPriorityMapID

                The Object 
                testValFsQoSPriorityMapName
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsQoSPriorityMapName (UINT4 *pu4ErrorCode,
                               UINT4 u4FsQoSPriorityMapID,
                               tSNMP_OCTET_STRING_TYPE *
                               pTestValFsQoSPriorityMapName)
{
    INT4                i4RetStatus = QOS_FAILURE;
    tQoSInPriorityMapNode *pPriMapNode = NULL;

    *pu4ErrorCode = SNMP_ERR_NO_ERROR;

    /* Check Index */
    i4RetStatus = QoSUtlValidatePriMapTblIdxInst (u4FsQoSPriorityMapID,
                                                  pu4ErrorCode);

    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlValidatePriMapTblIdxInst () "
                      "Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    /* Check  Object's Value */
    i4RetStatus = QoSUtlValidateTableName (pTestValFsQoSPriorityMapName,
                                           pu4ErrorCode);
    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlValidateTableName () "
                      "Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    pPriMapNode = QoSUtlGetPriorityMapNode (u4FsQoSPriorityMapID);
    if (pPriMapNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetPriorityMapNode () Returns "
                      "FAILURE. \r\n", __FUNCTION__);

        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (QOS_CLI_ERR_PRI_MAP_NO_ENTRY);
        return (SNMP_FAILURE);
    }

    /* Check the Name is same as Current Name */
    if ((STRLEN (pPriMapNode->au1Name)) == (UINT4)
        pTestValFsQoSPriorityMapName->i4_Length)
    {
        if ((STRNCMP (pPriMapNode->au1Name,
                      pTestValFsQoSPriorityMapName->pu1_OctetList,
                      pTestValFsQoSPriorityMapName->i4_Length)) == 0)
        {
            return (SNMP_SUCCESS);
        }
    }

    /* Check the name is Unique or Not */
    i4RetStatus = QoSUtlIsUniqueName (pTestValFsQoSPriorityMapName,
                                      QOS_TBL_TYPE_PRI_MAP);
    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlIsUniqueName () Returns "
                      "FAILURE. \r\n", __FUNCTION__);

        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return (SNMP_FAILURE);
    }

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhTestv2FsQoSPriorityMapIfIndex
 Input       :  The Indices
                FsQoSPriorityMapID

                The Object 
                testValFsQoSPriorityMapIfIndex
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsQoSPriorityMapIfIndex (UINT4 *pu4ErrorCode,
                                  UINT4 u4FsQoSPriorityMapID,
                                  UINT4 u4TestValFsQoSPriorityMapIfIndex)
{
    INT4                i4RetStatus = QOS_FAILURE;

    *pu4ErrorCode = SNMP_ERR_NO_ERROR;

    /* Check Index */
    i4RetStatus = QoSUtlValidatePriMapTblIdxInst (u4FsQoSPriorityMapID,
                                                  pu4ErrorCode);

    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlValidatePriMapTblIdxInst () "
                      "Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    if (u4TestValFsQoSPriorityMapIfIndex == 0)
    {
        /* Reset the IfIndex */
        return (SNMP_SUCCESS);
    }

    /* Check  Object's Value */
    i4RetStatus = QoSUtlValidateIfIndex (u4TestValFsQoSPriorityMapIfIndex,
                                         pu4ErrorCode);
    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlValidateIfIndex () "
                      "Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhTestv2FsQoSPriorityMapVlanId
 Input       :  The Indices
                FsQoSPriorityMapID

                The Object 
                testValFsQoSPriorityMapVlanId
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsQoSPriorityMapVlanId (UINT4 *pu4ErrorCode,
                                 UINT4 u4FsQoSPriorityMapID,
                                 UINT4 u4TestValFsQoSPriorityMapVlanId)
{
    INT4                i4RetStatus = QOS_FAILURE;

    *pu4ErrorCode = SNMP_ERR_NO_ERROR;

    /* Check Index */
    i4RetStatus = QoSUtlValidatePriMapTblIdxInst (u4FsQoSPriorityMapID,
                                                  pu4ErrorCode);

    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlValidatePriMapTblIdxInst () "
                      "Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    if (u4TestValFsQoSPriorityMapVlanId == 0)
    {
        /* Reset the VlanId */
        return (SNMP_SUCCESS);
    }

    /* Check  Object's Value */
    i4RetStatus = QoSUtlValidateVlanId (u4TestValFsQoSPriorityMapVlanId,
                                        pu4ErrorCode);
    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlValidateVlanId () "
                      "Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhTestv2FsQoSPriorityMapInPriority
 Input       :  The Indices
                FsQoSPriorityMapID

                The Object 
                testValFsQoSPriorityMapInPriority
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsQoSPriorityMapInPriority (UINT4 *pu4ErrorCode,
                                     UINT4 u4FsQoSPriorityMapID,
                                     INT4 i4TestValFsQoSPriorityMapInPriority)
{
    tQoSInPriorityMapNode *pPriMapNode = NULL;
    INT4                i4RetStatus = QOS_FAILURE;
    INT4                i4InPriority = QOS_FAILURE;
    INT4                i4ReturnValue = SNMP_SUCCESS;

    *pu4ErrorCode = SNMP_ERR_NO_ERROR;

    /* Check Index */
    i4RetStatus = QoSUtlValidatePriMapTblIdxInst (u4FsQoSPriorityMapID,
                                                  pu4ErrorCode);

    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlValidatePriMapTblIdxInst () "
                      "Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    /* Check  Object's Value */
    pPriMapNode = QoSUtlGetPriorityMapNode (u4FsQoSPriorityMapID);

    if (pPriMapNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetPriorityMapNode () Returns "
                      "FAILURE. \r\n", __FUNCTION__);

        *pu4ErrorCode = SNMP_ERR_NO_CREATION;

        CLI_SET_ERR (QOS_CLI_ERR_PRI_MAP_NO_ENTRY);

        return (SNMP_FAILURE);
    }

    i4InPriority = i4TestValFsQoSPriorityMapInPriority;

    switch (pPriMapNode->u1InPriType)
    {
        case QOS_IN_PRI_TYPE_VLAN_PRI:
        case QOS_IN_PRI_TYPE_DOT1P:

            if ((i4InPriority < QOS_IN_PRIORITY_VLAN_PRI_MIN) ||
                (i4InPriority > QOS_IN_PRIORITY_VLAN_PRI_MAX))
            {
                QOS_TRC_ARG3 (MGMT_TRC, "%s :InPriority value is out of range."
                              " for Configured InPriType. Range should be Min"
                              " %d - Max %d. \r\n", __FUNCTION__,
                              QOS_IN_PRIORITY_VLAN_PRI_MIN,
                              QOS_IN_PRIORITY_VLAN_PRI_MAX);

                CLI_SET_ERR (QOS_CLI_ERR_INVALID_VLAN_PRI);

                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;

                return (SNMP_FAILURE);
            }
            if ((pPriMapNode->u4Id >= QOS_DEFAULT_PRI_MAP_MIN_VAL)
                && (pPriMapNode->u4Id <= QOS_DEFAULT_PRI_MAP_MAX_VAL))
            {
                /*Default Priority Map in-priority must be same as default value */
                if ((pPriMapNode->u1InPriority) != ((pPriMapNode->u4Id) - 1))
                {
                    QOS_TRC_ARG3 (MGMT_TRC, "%s :InPriority must be same as"
                                  " default value for default priority map Min"
                                  " %d - Max %d. \r\n", __FUNCTION__,
                                  QOS_DEFAULT_PRI_MAP_MIN_VAL,
                                  QOS_DEFAULT_PRI_MAP_MAX_VAL);

                    CLI_SET_ERR (QOS_CLI_ERR_DEF_PRI_MAP_INV_IN_PRI);
                    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                    return (SNMP_FAILURE);
                }
            }
            break;

        case QOS_IN_PRI_TYPE_IP_TOS:
            /* IP TOS-PRI BIT(3) */
            if ((i4InPriority < QOS_IN_PRIORITY_IP_TOS_MIN) ||
                (i4InPriority > QOS_IN_PRIORITY_IP_TOS_MAX))
            {
                QOS_TRC_ARG3 (MGMT_TRC, "%s :InPriority value is out of range."
                              " for Configured InPriType. Range should be Min"
                              " %d - Max %d. \r\n", __FUNCTION__,
                              QOS_IN_PRIORITY_IP_TOS_MIN,
                              QOS_IN_PRIORITY_IP_TOS_MAX);

                CLI_SET_ERR (QOS_CLI_ERR_INVALID_IP_TOS);

                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;

                return (SNMP_FAILURE);
            }
            break;

        case QOS_IN_PRI_TYPE_IP_DSCP:

            /* IP DSCP BIT(6) */
            if ((i4InPriority < QOS_IN_PRIORITY_IP_DSCP_MIN) ||
                (i4InPriority > QOS_IN_PRIORITY_IP_DSCP_MAX))
            {
                QOS_TRC_ARG3 (MGMT_TRC, "%s :InPriority value is out of range."
                              " for Configured InPriType. Range should be Min"
                              " %d - Max %d. \r\n", __FUNCTION__,
                              QOS_IN_PRIORITY_IP_DSCP_MIN,
                              QOS_IN_PRIORITY_IP_DSCP_MAX);

                CLI_SET_ERR (QOS_CLI_ERR_INVALID_IP_DSCP);

                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;

                return (SNMP_FAILURE);
            }
            break;

        case QOS_IN_PRI_TYPE_MPLS_EXP:
            /* MPLS EXP  BIT(3) */
            if ((i4InPriority < QOS_IN_PRIORITY_MPLS_EXP_MIN) ||
                (i4InPriority > QOS_IN_PRIORITY_MPLS_EXP_MAX))
            {
                QOS_TRC_ARG3 (MGMT_TRC, "%s :InPriority value is out of range."
                              " for Configured InPriType. Range should be Min"
                              " %d - Max %d. \r\n", __FUNCTION__,
                              QOS_IN_PRIORITY_MPLS_EXP_MIN,
                              QOS_IN_PRIORITY_MPLS_EXP_MAX);

                CLI_SET_ERR (QOS_CLI_ERR_INVALID_MPLS_EXP);

                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;

                return (SNMP_FAILURE);
            }
            break;

        case QOS_IN_PRI_TYPE_VLAN_DEI:
            /* VLAN DEI BIT(1) */
            if ((i4InPriority != QOS_IN_PRIORITY_VLAN_DEI_MIN) &&
                (i4InPriority != QOS_IN_PRIORITY_VLAN_DEI_MAX))
            {
                QOS_TRC_ARG3 (MGMT_TRC, "%s :InPriority value is out of range."
                              " for Configured InPriType. Range should be Min"
                              " %d - Max %d. \r\n", __FUNCTION__,
                              QOS_IN_PRIORITY_VLAN_DEI_MIN,
                              QOS_IN_PRIORITY_VLAN_DEI_MAX);

                CLI_SET_ERR (QOS_CLI_ERR_INVALID_VLAN_DE);
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                return (SNMP_FAILURE);
            }
            break;

        default:
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            CLI_SET_ERR (QOS_CLI_ERR_INVALID_PRI_TYPE);
            i4ReturnValue = SNMP_FAILURE;
            break;
    }

    return ((INT1) i4ReturnValue);
}

/****************************************************************************
 Function    :  nmhTestv2FsQoSPriorityMapInPriType
 Input       :  The Indices
                FsQoSPriorityMapID

                The Object 
                testValFsQoSPriorityMapInPriType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsQoSPriorityMapInPriType (UINT4 *pu4ErrorCode,
                                    UINT4 u4FsQoSPriorityMapID,
                                    INT4 i4TestValFsQoSPriorityMapInPriType)
{
    INT4                i4RetStatus = QOS_FAILURE;

    *pu4ErrorCode = SNMP_ERR_NO_ERROR;

    /* Check Index */
    i4RetStatus = QoSUtlValidatePriMapTblIdxInst (u4FsQoSPriorityMapID,
                                                  pu4ErrorCode);

    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlValidatePriMapTblIdxInst () "
                      "Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    /* Check  Object's Value */
    if ((i4TestValFsQoSPriorityMapInPriType < QOS_IN_PRI_TYPE_MIN_VAL) ||
        (i4TestValFsQoSPriorityMapInPriType > QOS_IN_PRI_TYPE_MAX_VAL))
    {
        QOS_TRC_ARG3 (MGMT_TRC, "%s :InPriType value is out of range. The range"
                      " should be Min %d - Max %d. \r\n", __FUNCTION__,
                      QOS_IN_PRI_TYPE_MIN_VAL, QOS_IN_PRI_TYPE_MAX_VAL);

        CLI_SET_ERR (QOS_CLI_ERR_INVALID_PRI_TYPE);

        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;

        return (SNMP_FAILURE);
    }

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhTestv2FsQoSPriorityMapRegenPriority
 Input       :  The Indices
                FsQoSPriorityMapID

                The Object 
                testValFsQoSPriorityMapRegenPriority
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsQoSPriorityMapRegenPriority (UINT4 *pu4ErrorCode,
                                        UINT4 u4FsQoSPriorityMapID,
                                        UINT4
                                        u4TestValFsQoSPriorityMapRegenPriority)
{
    tQoSInPriorityMapNode *pPriMapNode = NULL;
    INT4                i4RetStatus = QOS_FAILURE;
    INT4                i4RegnPriority = QOS_FAILURE;
    INT4                i4ReturnValue = SNMP_SUCCESS;

    *pu4ErrorCode = SNMP_ERR_NO_ERROR;

    /* Check Index */
    i4RetStatus = QoSUtlValidatePriMapTblIdxInst (u4FsQoSPriorityMapID,
                                                  pu4ErrorCode);

    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlValidatePriMapTblIdxInst () "
                      "Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    /* Check  Object's Value */
    pPriMapNode = QoSUtlGetPriorityMapNode (u4FsQoSPriorityMapID);
    if (pPriMapNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetPriorityMapNode () Returns "
                      "FAILURE. \r\n", __FUNCTION__);

        *pu4ErrorCode = SNMP_ERR_NO_CREATION;

        CLI_SET_ERR (QOS_CLI_ERR_PRI_MAP_NO_ENTRY);

        return (SNMP_FAILURE);
    }

    i4RegnPriority = (INT4) u4TestValFsQoSPriorityMapRegenPriority;

    switch (pPriMapNode->u1InPriType)
    {
        case QOS_IN_PRI_TYPE_VLAN_PRI:
        case QOS_IN_PRI_TYPE_DOT1P:

            if ((i4RegnPriority < QOS_IN_PRIORITY_VLAN_PRI_MIN) ||
                (i4RegnPriority > QOS_IN_PRIORITY_VLAN_PRI_MAX))
            {
                QOS_TRC_ARG3 (MGMT_TRC, "%s :RegnPri value is out of range."
                              " for Configured InPriType. Range should be Min"
                              " %d - Max %d. \r\n", __FUNCTION__,
                              QOS_IN_PRIORITY_VLAN_PRI_MIN,
                              QOS_IN_PRIORITY_VLAN_PRI_MAX);

                CLI_SET_ERR (QOS_CLI_ERR_INVALID_VLAN_PRI);

                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;

                return (SNMP_FAILURE);
            }
            break;

        case QOS_IN_PRI_TYPE_IP_TOS:
            /* IP TOS-PRI BIT(3) */
            if ((i4RegnPriority < QOS_IN_PRIORITY_IP_TOS_MIN) ||
                (i4RegnPriority > QOS_IN_PRIORITY_IP_TOS_MAX))
            {
                QOS_TRC_ARG3 (MGMT_TRC, "%s :RegnPri value is out of range."
                              " for Configured InPriType. Range should be Min"
                              " %d - Max %d. \r\n", __FUNCTION__,
                              QOS_IN_PRIORITY_IP_TOS_MIN,
                              QOS_IN_PRIORITY_IP_TOS_MAX);

                CLI_SET_ERR (QOS_CLI_ERR_INVALID_VLAN_PRI);

                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;

                return (SNMP_FAILURE);
            }
            break;

        case QOS_IN_PRI_TYPE_IP_DSCP:

            /* IP DSCP BIT(6) */
            if ((i4RegnPriority < QOS_IN_PRIORITY_IP_DSCP_MIN) ||
                (i4RegnPriority > QOS_IN_PRIORITY_IP_DSCP_MAX))
            {
                QOS_TRC_ARG3 (MGMT_TRC, "%s :RegnPri value is out of range."
                              " for Configured InPriType. Range should be Min"
                              " %d - Max %d. \r\n", __FUNCTION__,
                              QOS_IN_PRIORITY_IP_DSCP_MAX,
                              QOS_IN_PRIORITY_IP_DSCP_MAX);

                CLI_SET_ERR (QOS_CLI_ERR_INVALID_IP_DSCP);

                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;

                return (SNMP_FAILURE);
            }
            break;

        case QOS_IN_PRI_TYPE_MPLS_EXP:
            /* MPLS EXP  BIT(3) */
            if ((i4RegnPriority < QOS_IN_PRIORITY_MPLS_EXP_MIN) ||
                (i4RegnPriority > QOS_IN_PRIORITY_MPLS_EXP_MAX))
            {
                QOS_TRC_ARG3 (MGMT_TRC, "%s :RegnPri value is out of range."
                              " for Configured InPriType. Range should be Min"
                              " %d - Max %d. \r\n", __FUNCTION__,
                              QOS_IN_PRIORITY_MPLS_EXP_MIN,
                              QOS_IN_PRIORITY_MPLS_EXP_MAX);

                CLI_SET_ERR (QOS_CLI_ERR_INVALID_MPLS_EXP);

                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;

                return (SNMP_FAILURE);
            }
            break;

        case QOS_IN_PRI_TYPE_VLAN_DEI:
            /* VLAN DEI BIT(1) */
            if ((i4RegnPriority != QOS_IN_PRIORITY_VLAN_DEI_MIN) &&
                (i4RegnPriority != QOS_IN_PRIORITY_VLAN_DEI_MAX))
            {
                QOS_TRC_ARG3 (MGMT_TRC, "%s :RegnPri value is out of range."
                              " for Configured InPriType. Range should be Min"
                              " %d - Max %d. \r\n", __FUNCTION__,
                              QOS_IN_PRIORITY_VLAN_DEI_MIN,
                              QOS_IN_PRIORITY_VLAN_DEI_MAX);

                CLI_SET_ERR (QOS_CLI_ERR_INVALID_VLAN_DE);
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                return (SNMP_FAILURE);
            }
            break;

        default:
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            CLI_SET_ERR (QOS_CLI_ERR_INVALID_PRI_TYPE);
            i4ReturnValue = SNMP_FAILURE;
            break;
    }

    return ((INT1) i4ReturnValue);
}

/****************************************************************************
 Function    :  nmhTestv2FsQoSPriorityMapRegenInnerPriority
 Input       :  The Indices
                FsQoSPriorityMapID

                The Object 
                testValFsQoSPriorityMapRegenInnerPriority
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
     
     
     
     
     
     
     
    nmhTestv2FsQoSPriorityMapRegenInnerPriority
    (UINT4 *pu4ErrorCode,
     UINT4 u4FsQoSPriorityMapID,
     UINT4 u4TestValFsQoSPriorityMapRegenInnerPriority)
{
    INT4                i4RetStatus = QOS_FAILURE;

    *pu4ErrorCode = SNMP_ERR_NO_ERROR;

    /* Check Index */
    i4RetStatus = QoSUtlValidatePriMapTblIdxInst (u4FsQoSPriorityMapID,
                                                  pu4ErrorCode);

    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlValidatePriMapTblIdxInst () "
                      "Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    /* Check  Object's Value */
    if (u4TestValFsQoSPriorityMapRegenInnerPriority >
        QOS_REGEN_INNER_PRIORITY_MAX_VAL)
    {
        QOS_TRC_ARG3 (MGMT_TRC, "%s :Inner Regen Priority value is out of "
                      "range. The range should be Min %d - Max %d. \r\n",
                      __FUNCTION__, QOS_REGEN_INNER_PRIORITY_MIN_VAL,
                      QOS_REGEN_INNER_PRIORITY_MAX_VAL);

        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;

        CLI_SET_ERR (QOS_CLI_ERR_INVALID_INNER_PRI);

        return (SNMP_FAILURE);
    }

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhTestv2FsQoSPriorityMapStatus
 Input       :  The Indices
                FsQoSPriorityMapID

                The Object 
                testValFsQoSPriorityMapStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsQoSPriorityMapStatus (UINT4 *pu4ErrorCode,
                                 UINT4 u4FsQoSPriorityMapID,
                                 INT4 i4TestValFsQoSPriorityMapStatus)
{
    tQoSInPriorityMapNode *pPriMapNode = NULL;
    tQoSInPriorityMapNode *pPriMapUniqueNode = NULL;
    UINT4               u4Count = 0;
    INT4                i4RetStatus = QOS_FAILURE;
    INT4                i4ReturnValue = SNMP_SUCCESS;

    if (gQoSGlobalInfo.eSysControl != QOS_SYS_CNTL_START)
    {
        QOS_TRC (MGMT_TRC, QOS_MODULE_SHUTDOW_ERROR_MSG);
        CLI_SET_ERR (QOS_CLI_ERR_MODULE_SHUTDOW);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return (SNMP_FAILURE);
    }

    *pu4ErrorCode = SNMP_ERR_NO_ERROR;

    /* Check the Range of Index Values (Min-Max) */
    i4RetStatus = QOS_CHECK_PRI_MAP_TBL_INDEX_RANGE (u4FsQoSPriorityMapID);

    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG3 (MGMT_TRC, "%s : Priority Map Id %d is out of Range."
                      " The Range Should be (1-%d). \r\n", __FUNCTION__,
                      u4FsQoSPriorityMapID, QOS_PRI_MAP_TBL_MAX_INDEX_RANGE);

        CLI_SET_ERR (QOS_CLI_ERR_PRI_MAP_RANGE);

        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }

    pPriMapNode = QoSUtlGetPriorityMapNode (u4FsQoSPriorityMapID);

    if (pPriMapNode == NULL)
    {
        /* Entry Not Found In The Table */
        switch (i4TestValFsQoSPriorityMapStatus)
        {
            case CREATE_AND_GO:
            case ACTIVE:
            case NOT_IN_SERVICE:
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                i4ReturnValue = SNMP_FAILURE;
                break;

            case CREATE_AND_WAIT:
                /* Check the No of Entries in the Table */
                i4RetStatus =
                    (INT4) RBTreeCount (gQoSGlobalInfo.pRbInPriMapTbl,
                                        &u4Count);

                if ((i4RetStatus == RB_FAILURE) ||
                    (u4Count > QOS_PRI_MAP_TBL_MAX_ENTRIES))
                {
                    QOS_TRC_ARG2 (MGMT_TRC, "%s : Max No of Priority Map Table"
                                  " Entries are Configured. No of Max Entries"
                                  " %d. \r\n", __FUNCTION__,
                                  QOS_PRI_MAP_TBL_MAX_ENTRIES);

                    CLI_SET_ERR (QOS_CLI_ERR_PRI_MAP_MAX_ENTRY);

                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;

                    return (SNMP_FAILURE);
                }

                break;

            case DESTROY:
                CLI_SET_ERR (QOS_CLI_ERR_PRI_MAP_NO_ENTRY);
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                i4ReturnValue = SNMP_FAILURE;

                break;

            default:
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                i4ReturnValue = SNMP_FAILURE;
                break;
        }
    }
    else
    {

        /* Entry Found In The Table */
        switch (i4TestValFsQoSPriorityMapStatus)
        {
            case ACTIVE:

                if ((pPriMapNode->u1Status == NOT_READY))
                {
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                    return (SNMP_FAILURE);
                }

                /* Check the Priority Map Table uniqueness of 
                 * (If,Vlan,InPriType,InPriVal) 
                 * if it is Unique then Return SUCCESS otherwise FAILURE */
                pPriMapUniqueNode =
                    QoSUtlGetPriorityMapUniNode (pPriMapNode->u4IfIndex,
                                                 pPriMapNode->u2VlanId,
                                                 pPriMapNode->u1InPriority,
                                                 pPriMapNode->u1InPriType);
                if ((pPriMapUniqueNode != NULL) &&
                    (pPriMapUniqueNode->u4Id != pPriMapNode->u4Id))
                {
                    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                    CLI_SET_ERR (QOS_CLI_ERR_PRI_MAP_DUP_ENTRY);
                    return (SNMP_FAILURE);
                }

                break;

            case NOT_IN_SERVICE:

                if ((pPriMapNode->u1Status == NOT_READY))
                {
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                    return (SNMP_FAILURE);
                }

                /* 1. Check(Test) Dependency for this Entry. */
                i4RetStatus = QOS_CHECK_TABLE_REF_COUNT (pPriMapNode);

                if (i4RetStatus == QOS_FAILURE)
                {
                    QOS_TRC_ARG1 (MGMT_TRC, "In %s : ERROR - This Entry is "
                                  "referenced by Class Map Table Entry.\r\n",
                                  __FUNCTION__);

                    CLI_SET_ERR (QOS_CLI_ERR_PRI_MAP_RFE);
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;

                    return (SNMP_FAILURE);
                }

                break;

            case DESTROY:

                /* Check(Test) is it Default Entry. */
                i4RetStatus = QoSIsDefPriorityMapTblEntry (pPriMapNode);
                if (i4RetStatus == QOS_SUCCESS)
                {
                    QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSIsPriorityMapDefEntry  "
                                  "Returns FAILURE.\r\n", __FUNCTION__);
                    CLI_SET_ERR (QOS_CLI_ERR_DEF_ENTRY);
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                    return (SNMP_FAILURE);
                }

                /* 1. Check(Test) Dependency for this Entry. */
                i4RetStatus = QOS_CHECK_TABLE_REF_COUNT (pPriMapNode);

                if (i4RetStatus == QOS_FAILURE)
                {
                    QOS_TRC_ARG1 (MGMT_TRC, "In %s : ERROR - This Entry is "
                                  "referenced by Class Map Table Entry.\r\n",
                                  __FUNCTION__);

                    CLI_SET_ERR (QOS_CLI_ERR_PRI_MAP_RFE);
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;

                    return (SNMP_FAILURE);
                }

                break;

            case CREATE_AND_WAIT:
            case CREATE_AND_GO:
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                i4ReturnValue = SNMP_FAILURE;
                break;

            default:
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                i4ReturnValue = SNMP_FAILURE;
                break;

        }                        /* End of switch */

    }                            /* End of if */

    return ((INT1) i4ReturnValue);
}

/****************************************************************************
 Function    :  nmhTestv2FsQoSPriorityMapInDEI
 Input       :  The Indices
                FsQoSPriorityMapID
 
                The Object
                testValFsQoSPriorityMapInDEI
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsQoSPriorityMapInDEI (UINT4 *pu4ErrorCode, UINT4 u4FsQoSPriorityMapID,
                                INT4 i4TestValFsQoSPriorityMapInDEI)
{
    INT4                i4RetStatus = QOS_FAILURE;

    *pu4ErrorCode = SNMP_ERR_NO_ERROR;

    /* Check Index */
    i4RetStatus = QoSUtlValidatePriMapTblIdxInst (u4FsQoSPriorityMapID,
                                                  pu4ErrorCode);

    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlValidatePriMapTblIdxInst () "
                      "Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    /* Check  Object's Value */
    if (i4TestValFsQoSPriorityMapInDEI > QOS_IN_DEI_MAX_VAL)
    {
        QOS_TRC_ARG3 (MGMT_TRC, "%s :Incoming DEI value is out of "
                      "range. The range should be Min %d - Max %d. \r\n",
                      __FUNCTION__, QOS_IN_DEI_MIN_VAL, QOS_IN_DEI_MAX_VAL);

        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;

        CLI_SET_ERR (QOS_CLI_ERR_INVALID_IN_DEI);

        return (SNMP_FAILURE);
    }

    return (SNMP_SUCCESS);

}

/********`********************************************************************
 Function    :  nmhTestv2FsQoSPriorityMapRegenColor
 Input       :  The Indices
                FsQoSPriorityMapID
 
                The Object
                testValFsQoSPriorityMapRegenColor
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsQoSPriorityMapRegenColor (UINT4 *pu4ErrorCode,
                                     UINT4 u4FsQoSPriorityMapID,
                                     INT4 i4TestValFsQoSPriorityMapRegenColor)
{
    INT4                i4RetStatus = QOS_FAILURE;

    *pu4ErrorCode = SNMP_ERR_NO_ERROR;

    /* Check Index */
    i4RetStatus = QoSUtlValidatePriMapTblIdxInst (u4FsQoSPriorityMapID,
                                                  pu4ErrorCode);

    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlValidatePriMapTblIdxInst () "
                      "Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    /* Check  Object's Value */
    if (i4TestValFsQoSPriorityMapRegenColor > QOS_REGEN_COLOR_MAX_VAL)
    {
        QOS_TRC_ARG3 (MGMT_TRC, "%s :Regenerated Color value is out of "
                      "range. The range should be Min %d - Max %d. \r\n",
                      __FUNCTION__, QOS_REGEN_COLOR_MIN_VAL,
                      QOS_REGEN_COLOR_MAX_VAL);

        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;

        CLI_SET_ERR (QOS_CLI_ERR_INVALID_REGEN_COLOR);

        return (SNMP_FAILURE);
    }

    return (SNMP_SUCCESS);

}

/* LOW LEVEL Routines for Table : FsQoSClassMapTable. */
/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsQoSClassMapTable
 Input       :  The Indices
                FsQoSClassMapId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsQoSClassMapTable (UINT4 u4FsQoSClassMapId)
{
    INT4                i4RetStatus = 0;
    tQoSClassMapNode   *pClsMapNode = NULL;

    if (gQoSGlobalInfo.eSysControl != QOS_SYS_CNTL_START)
    {
        QOS_TRC (MGMT_TRC, QOS_MODULE_SHUTDOW_ERROR_MSG);
        return (SNMP_FAILURE);
    }

    /* Check the Range of Index Values (Min-Max) */
    i4RetStatus = QOS_CHECK_CLS_MAP_TBL_INDEX_RANGE (u4FsQoSClassMapId);

    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG3 (MGMT_TRC, "%s : Class Map Id %d is out of Range."
                      " The Range Should be (1-%d). \r\n", __FUNCTION__,
                      u4FsQoSClassMapId, QOS_CLS_MAP_TBL_MAX_INDEX_RANGE);

        return (SNMP_FAILURE);
    }

    pClsMapNode = QoSUtlGetClassMapNode (u4FsQoSClassMapId);

    if (pClsMapNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetClassMapNode () "
                      "Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    return (SNMP_SUCCESS);
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsQoSPriorityMapTable
 Input       :  The Indices
                FsQoSPriorityMapID
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsQoSPriorityMapTable (UINT4 *pu4ErrorCode,
                               tSnmpIndexList * pSnmpIndexList,
                               tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsQoSClassMapTable
 Input       :  The Indices
                FsQoSClassMapId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsQoSClassMapTable (UINT4 *pu4FsQoSClassMapId)
{
    INT4                i4RetStatus = QOS_FAILURE;

    if (gQoSGlobalInfo.eSysControl != QOS_SYS_CNTL_START)
    {
        QOS_TRC (MGMT_TRC, QOS_MODULE_SHUTDOW_ERROR_MSG);
        return (SNMP_FAILURE);
    }

    i4RetStatus = QoSGetNextClsMapTblEntryIndex (0, pu4FsQoSClassMapId);

    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSGetNextClsMapTblEntryIndex () "
                      "Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsQoSClassMapTable
 Input       :  The Indices
                FsQoSClassMapId
                nextFsQoSClassMapId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsQoSClassMapTable (UINT4 u4FsQoSClassMapId,
                                   UINT4 *pu4NextFsQoSClassMapId)
{
    INT4                i4RetStatus = QOS_FAILURE;

    if (gQoSGlobalInfo.eSysControl != QOS_SYS_CNTL_START)
    {
        QOS_TRC (MGMT_TRC, QOS_MODULE_SHUTDOW_ERROR_MSG);
        return (SNMP_FAILURE);
    }

    /* Check the Range of Index Values (Min-Max) */
    i4RetStatus = QOS_CHECK_CLS_MAP_TBL_INDEX_RANGE (u4FsQoSClassMapId);

    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG3 (MGMT_TRC, "%s : Class Map Id %d is out of Range."
                      " The Range Should be (1-%d). \r\n", __FUNCTION__,
                      u4FsQoSClassMapId, QOS_CLS_MAP_TBL_MAX_INDEX_RANGE);

        return (SNMP_FAILURE);
    }

    i4RetStatus = QoSGetNextClsMapTblEntryIndex (u4FsQoSClassMapId,
                                                 pu4NextFsQoSClassMapId);
    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSGetNextClsMapTblEntryIndex () "
                      "Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    return (SNMP_SUCCESS);
}

/* Low Level GET Routine for All Objects  */
/****************************************************************************
 Function    :  nmhGetFsQoSClassMapName
 Input       :  The Indices
                FsQoSClassMapId

                The Object 
                retValFsQoSClassMapName
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsQoSClassMapName (UINT4 u4FsQoSClassMapId,
                         tSNMP_OCTET_STRING_TYPE * pRetValFsQoSClassMapName)
{
    tQoSClassMapNode   *pClsMapNode = NULL;
    UINT1              *pu1Str = NULL;

    if ((pRetValFsQoSClassMapName == NULL) ||
        (pRetValFsQoSClassMapName->pu1_OctetList == NULL))
    {
        return (SNMP_FAILURE);
    }

    pClsMapNode = QoSUtlGetClassMapNode (u4FsQoSClassMapId);

    if (pClsMapNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetClassMapNode () "
                      "Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    pu1Str = &(pClsMapNode->au1Name[0]);
    QOS_UTL_COPY_TABLE_NAME (pRetValFsQoSClassMapName, pu1Str);

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFsQoSClassMapL2FilterId
 Input       :  The Indices
                FsQoSClassMapId

                The Object 
                retValFsQoSClassMapL2FilterId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsQoSClassMapL2FilterId (UINT4 u4FsQoSClassMapId,
                               UINT4 *pu4RetValFsQoSClassMapL2FilterId)
{
    tQoSClassMapNode   *pClsMapNode = NULL;

    pClsMapNode = QoSUtlGetClassMapNode (u4FsQoSClassMapId);

    if (pClsMapNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetClassMapNode () "
                      "Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    *pu4RetValFsQoSClassMapL2FilterId = pClsMapNode->u4L2FilterId;

    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhGetFsQoSClassMapL3FilterId
 Input       :  The Indices
                FsQoSClassMapId

                The Object 
                retValFsQoSClassMapL3FilterId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsQoSClassMapL3FilterId (UINT4 u4FsQoSClassMapId,
                               UINT4 *pu4RetValFsQoSClassMapL3FilterId)
{
    tQoSClassMapNode   *pClsMapNode = NULL;

    pClsMapNode = QoSUtlGetClassMapNode (u4FsQoSClassMapId);

    if (pClsMapNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetClassMapNode () "
                      "Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    *pu4RetValFsQoSClassMapL3FilterId = pClsMapNode->u4L3FilterId;

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFsQoSClassMapPriorityMapId
 Input       :  The Indices
                FsQoSClassMapId

                The Object 
                retValFsQoSClassMapPriorityMapId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsQoSClassMapPriorityMapId (UINT4 u4FsQoSClassMapId,
                                  UINT4 *pu4RetValFsQoSClassMapPriorityMapId)
{
    tQoSClassMapNode   *pClsMapNode = NULL;

    pClsMapNode = QoSUtlGetClassMapNode (u4FsQoSClassMapId);

    if (pClsMapNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetClassMapNode () "
                      "Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    *pu4RetValFsQoSClassMapPriorityMapId = pClsMapNode->u4PriorityMapId;

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFsQoSClassMapCLASS
 Input       :  The Indices
                FsQoSClassMapId

                The Object 
                retValFsQoSClassMapCLASS
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsQoSClassMapCLASS (UINT4 u4FsQoSClassMapId,
                          UINT4 *pu4RetValFsQoSClassMapCLASS)
{
    tQoSClassMapNode   *pClsMapNode = NULL;

    pClsMapNode = QoSUtlGetClassMapNode (u4FsQoSClassMapId);

    if (pClsMapNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetClassMapNode () "
                      "Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    *pu4RetValFsQoSClassMapCLASS = pClsMapNode->u4ClassId;

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFsQoSClassMapClfrId
 Input       :  The Indices
                FsQoSClassMapId

                The Object 
                retValFsQoSClassMapClfrId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsQoSClassMapClfrId (UINT4 u4FsQoSClassMapId,
                           UINT4 *pu4RetValFsQoSClassMapClfrId)
{
    tQoSClassMapNode   *pClsMapNode = NULL;

    pClsMapNode = QoSUtlGetClassMapNode (u4FsQoSClassMapId);

    if (pClsMapNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetClassMapNode () Returns "
                      "FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    *pu4RetValFsQoSClassMapClfrId = (INT4) pClsMapNode->u4ClassMapClfrId;

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFsQoSClassMapPreColor
 Input       :  The Indices
                FsQoSClassMapId

                The Object 
                retValFsQoSClassMapPreColor
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsQoSClassMapPreColor (UINT4 u4FsQoSClassMapId,
                             INT4 *pi4RetValFsQoSClassMapPreColor)
{
    tQoSClassMapNode   *pClsMapNode = NULL;

    pClsMapNode = QoSUtlGetClassMapNode (u4FsQoSClassMapId);

    if (pClsMapNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetClassMapNode () Returns "
                      "FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    *pi4RetValFsQoSClassMapPreColor = (INT4) pClsMapNode->u1PreColor;

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFsQoSClassMapStatus
 Input       :  The Indices
                FsQoSClassMapId

                The Object 
                retValFsQoSClassMapStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsQoSClassMapStatus (UINT4 u4FsQoSClassMapId,
                           INT4 *pi4RetValFsQoSClassMapStatus)
{
    tQoSClassMapNode   *pClsMapNode = NULL;

    pClsMapNode = QoSUtlGetClassMapNode (u4FsQoSClassMapId);

    if (pClsMapNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetClassMapNode () "
                      "Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    *pi4RetValFsQoSClassMapStatus = (INT4) (pClsMapNode->u1Status);

    return (SNMP_SUCCESS);

}

/* Low Level SET Routine for All Objects  */
/****************************************************************************
 Function    :  nmhSetFsQoSClassMapName
 Input       :  The Indices
                FsQoSClassMapId

                The Object 
                setValFsQoSClassMapName
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsQoSClassMapName (UINT4 u4FsQoSClassMapId,
                         tSNMP_OCTET_STRING_TYPE * pSetValFsQoSClassMapName)
{
    tQoSClassMapNode   *pClsMapNode = NULL;
    UINT1              *pu1Str = NULL;

    pClsMapNode = QoSUtlGetClassMapNode (u4FsQoSClassMapId);

    if (pClsMapNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetClassMapNode () "
                      "Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    pu1Str = &(pClsMapNode->au1Name[0]);
    QOS_UTL_SET_TABLE_NAME (pu1Str, pSetValFsQoSClassMapName);

    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhSetFsQoSClassMapL2FilterId
 Input       :  The Indices
                FsQoSClassMapId

                The Object 
                setValFsQoSClassMapL2FilterId
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsQoSClassMapL2FilterId (UINT4 u4FsQoSClassMapId,
                               UINT4 u4SetValFsQoSClassMapL2FilterId)
{
    tQoSClassMapNode   *pClsMapNode = NULL;
    UINT4               u4TmpFilterId = 0;
    UINT4               u4RetStatus = QOS_FAILURE;

    pClsMapNode = QoSUtlGetClassMapNode (u4FsQoSClassMapId);

    if (pClsMapNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetClassMapNode () "
                      "Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    /* If the Same value is configured again no need to incrment the 
     * RefCount and Set */
    if (pClsMapNode->u4L2FilterId == u4SetValFsQoSClassMapL2FilterId)
    {
        return (SNMP_SUCCESS);
    }

    /* Decrement the existing L2 Filter Id's reference count */
    u4TmpFilterId = pClsMapNode->u4L2FilterId;
    if (u4TmpFilterId != 0)
    {
        u4RetStatus = QoSUtlUpdateFilterRefCount (u4TmpFilterId, QOS_L2FILTER,
                                                  QOS_DECR);
        if (u4RetStatus == QOS_FAILURE)
        {
            QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlUpdateFilterRefCount () "
                          "Returns FAILURE. \r\n", __FUNCTION__);
            return (SNMP_FAILURE);
        }
    }

    if (u4SetValFsQoSClassMapL2FilterId != 0)
    {
        u4RetStatus =
            QoSUtlUpdateFilterRefCount (u4SetValFsQoSClassMapL2FilterId,
                                        QOS_L2FILTER, QOS_INCR);
        if (u4RetStatus == QOS_FAILURE)
        {
            QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlUpdateFilterRefCount () "
                          "Returns FAILURE. \r\n", __FUNCTION__);
            return (SNMP_FAILURE);
        }
    }

    pClsMapNode->u4L2FilterId = u4SetValFsQoSClassMapL2FilterId;

    QoSValidateClsMapTblEntry (pClsMapNode);

    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhSetFsQoSClassMapL3FilterId
 Input       :  The Indices
                FsQoSClassMapId

                The Object 
                setValFsQoSClassMapL3FilterId
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsQoSClassMapL3FilterId (UINT4 u4FsQoSClassMapId,
                               UINT4 u4SetValFsQoSClassMapL3FilterId)
{
    tQoSClassMapNode   *pClsMapNode = NULL;
    UINT4               u4TmpFilterId = 0;
    UINT4               u4RetStatus = QOS_FAILURE;
    UINT4               u4Type = QOS_ROWPOINTER_DEF;
    UINT4               u4Id = QOS_ROWPOINTER_DEF;
    tSNMP_OID_TYPE      ClfrOId;
    UINT4               au4TmpArray[14];
    tIssL3FilterEntry  *pIssL3FilterEntry = NULL;
    UINT2               u2Index;
    INT4                i4DataPathStatus;
    BOOL1               bResult = OSIX_FALSE;

    ClfrOId.pu4_OidList = au4TmpArray;

    pClsMapNode = QoSUtlGetClassMapNode (u4FsQoSClassMapId);

    if (pClsMapNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetClassMapNode () "
                      "Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    /* If the Same value is configured again no need to incrment the 
     * RefCount and Set */
    if (pClsMapNode->u4L3FilterId == u4SetValFsQoSClassMapL3FilterId)
    {
        return (SNMP_SUCCESS);
    }

    /* Decrement the existing L3 Filter Id's reference count */
    u4TmpFilterId = pClsMapNode->u4L3FilterId;
    if (u4TmpFilterId != 0)
    {
        u4RetStatus = QoSUtlUpdateFilterRefCount (u4TmpFilterId, QOS_L3FILTER,
                                                  QOS_DECR);
        if (u4RetStatus == QOS_FAILURE)
        {
            QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlUpdateFilterRefCount () "
                          "Returns FAILURE. \r\n", __FUNCTION__);
            return (SNMP_FAILURE);
        }
    }

    if (u4SetValFsQoSClassMapL3FilterId != 0)
    {
        u4RetStatus =
            QoSUtlUpdateFilterRefCount (u4SetValFsQoSClassMapL3FilterId,
                                        QOS_L3FILTER, QOS_INCR);
        if (u4RetStatus == QOS_FAILURE)
        {
            QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlUpdateFilterRefCount () "
                          "Returns FAILURE. \r\n", __FUNCTION__);
            return (SNMP_FAILURE);
        }
    }

    pClsMapNode->u4L3FilterId = u4SetValFsQoSClassMapL3FilterId;

    pIssL3FilterEntry = IssGetL3FilterEntry (u4SetValFsQoSClassMapL3FilterId);

    if (pIssL3FilterEntry == NULL)
    {
        if (u4SetValFsQoSClassMapL3FilterId == QOS_ROWPOINTER_DEF)
        {
            return SNMP_SUCCESS;
        }
        else
        {
            return SNMP_FAILURE;
        }
    }

    if (pClsMapNode->u4ClassMapClfrId == 0)
    {
        nmhGetDiffServClfrNextFree (&pClsMapNode->u4ClassMapClfrId);
        nmhSetDiffServClfrStatus (pClsMapNode->u4ClassMapClfrId,
                                  CREATE_AND_WAIT);
        nmhSetDiffServClfrStatus (pClsMapNode->u4ClassMapClfrId, ACTIVE);
    }

    if ((pIssL3FilterEntry =
         IssGetL3FilterEntry (pClsMapNode->u4L3FilterId)) != NULL)

    {
        for (u2Index = 1; u2Index <= BRG_MAX_PHY_PORTS; u2Index++)
        {
            /* Update the Prtlist by checking whether any datapath entry is pointing */
            MEMSET (ClfrOId.pu4_OidList, 0, sizeof (au4TmpArray));
            ClfrOId.u4_Length = 0;
            /* Update the type and Id to default values before getting values */
            u4Type = QOS_ROWPOINTER_DEF;
            u4Id = QOS_ROWPOINTER_DEF;

            if (nmhGetDiffServDataPathStart (u2Index, QOS_DATA_IN,
                                             &ClfrOId) == SNMP_FAILURE)
            {
                break;
            }

            QosGetTypeIdFromOid (&ClfrOId, &u4Type, &u4Id);

            if ((u4Type == DS_CLFR) && (u4Id == pClsMapNode->u4ClassMapClfrId))
            {
                QoSUtilOsixBitlistSetBit (pIssL3FilterEntry->
                                          IssL3FilterInPortList,
                                          u2Index,
                                          (INT4) sizeof (tIssPortList));

            }

            MEMSET (ClfrOId.pu4_OidList, 0, ClfrOId.u4_Length);

            nmhGetDiffServDataPathStart (u2Index, QOS_DATA_OUT, &ClfrOId);

            QosGetTypeIdFromOid (&ClfrOId, &u4Type, &u4Id);

            if ((u4Type == DS_CLFR) && u4Id == pClsMapNode->u4ClassMapClfrId)
            {
                QoSUtilOsixBitlistSetBit (pIssL3FilterEntry->
                                          IssL3FilterInPortList,
                                          u2Index,
                                          (INT4) sizeof (tIssPortList));

            }

            /* Now Update the Datapath entries with values configured Here */
            QoSUtilOsixBitlistIsBitSet (pIssL3FilterEntry->
                                        IssL3FilterInPortList, u2Index,
                                        BRG_PHY_PORT_LIST_SIZE, &bResult);

            if (bResult == OSIX_TRUE)
            {

                QosGetOidFromTypeId (pClsMapNode->u4ClassMapClfrId, DS_CLFR,
                                     &ClfrOId);
                nmhGetDiffServDataPathStatus (u2Index, QOS_DATA_IN,
                                              &i4DataPathStatus);
                if (i4DataPathStatus == QOS_ACTIVE)
                {
                    nmhSetDiffServDataPathStatus (u2Index, QOS_DATA_IN,
                                                  QOS_NOT_IN_SERVICE);
                }

                nmhSetDiffServDataPathStart (u2Index, QOS_DATA_IN, &ClfrOId);

                nmhGetDiffServDataPathStatus (u2Index, QOS_DATA_IN,
                                              &i4DataPathStatus);

                if (i4DataPathStatus == QOS_NOT_IN_SERVICE)
                {
                    nmhSetDiffServDataPathStatus (u2Index, QOS_DATA_IN,
                                                  QOS_ACTIVE);
                }
            }
        }
    }

    QoSValidateClsMapTblEntry (pClsMapNode);

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhSetFsQoSClassMapPriorityMapId
 Input       :  The Indices
                FsQoSClassMapId

                The Object 
                setValFsQoSClassMapPriorityMapId
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsQoSClassMapPriorityMapId (UINT4 u4FsQoSClassMapId,
                                  UINT4 u4SetValFsQoSClassMapPriorityMapId)
{
    tQoSClassMapNode   *pClsMapNode = NULL;
    tQoSInPriorityMapNode *pPriMapNode = NULL;
    tQoSInPriorityMapNode *pTmpPriMapNode = NULL;
    UINT4               u4TmpPriId = 0;
    pClsMapNode = QoSUtlGetClassMapNode (u4FsQoSClassMapId);

    if (pClsMapNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetClassMapNode () "
                      "Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    /* If the Same value is configured again no need to incrment the 
     * RefCount and Set */
    if (pClsMapNode->u4PriorityMapId == u4SetValFsQoSClassMapPriorityMapId)
    {
        return (SNMP_SUCCESS);
    }

    /* Store the old value ,Reset corresponding ref count if the id not '0' */
    u4TmpPriId = pClsMapNode->u4PriorityMapId;
    if (u4TmpPriId != 0)
    {
        pTmpPriMapNode = QoSUtlGetPriorityMapNode (u4TmpPriId);
        if (pTmpPriMapNode == NULL)
        {
            QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetPriorityMapNode () "
                          "Returns FAILURE. \r\n", __FUNCTION__);

            return (SNMP_FAILURE);
        }
        pTmpPriMapNode->u4RefCount = (pTmpPriMapNode->u4RefCount) - 1;
    }

    if (u4SetValFsQoSClassMapPriorityMapId != 0)
    {
        pPriMapNode =
            QoSUtlGetPriorityMapNode (u4SetValFsQoSClassMapPriorityMapId);
        if (pPriMapNode == NULL)
        {
            QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetPriorityMapNode () "
                          "Returns FAILURE. \r\n", __FUNCTION__);

            return (SNMP_FAILURE);
        }

        /* Incremnet the RefCount of PriMapId  Entry in the PriorityMap Table */
        pPriMapNode->u4RefCount = (pPriMapNode->u4RefCount) + 1;
    }

    pClsMapNode->u4PriorityMapId = u4SetValFsQoSClassMapPriorityMapId;

    QoSValidateClsMapTblEntry (pClsMapNode);

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhSetFsQoSClassMapCLASS
 Input       :  The Indices
                FsQoSClassMapId

                The Object 
                setValFsQoSClassMapCLASS
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsQoSClassMapCLASS (UINT4 u4FsQoSClassMapId,
                          UINT4 u4SetValFsQoSClassMapCLASS)
{
    tQoSClassMapNode   *pClsMapNode = NULL;
    UINT4               u4RetStatus = 0;

    pClsMapNode = QoSUtlGetClassMapNode (u4FsQoSClassMapId);

    if (pClsMapNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetClassMapNode () "
                      "Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    /* This Function is used to Add ClassMapIds to the respective 
     * CLASS using the internal DataSrtuct 'tQoSClassInfoNode'
     */
    u4RetStatus =
        QoSClsInfoAddFilters (u4SetValFsQoSClassMapCLASS, pClsMapNode);
    if (u4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSClsInfoAddFilters () "
                      "Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    pClsMapNode->u4ClassId = u4SetValFsQoSClassMapCLASS;

    QoSValidateClsMapTblEntry (pClsMapNode);

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhSetFsQoSClassMapClfrId
 Input       :  The Indices
                FsQoSClassMapId

                The Object 
                setValFsQoSClassMapClfrId
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsQoSClassMapClfrId (UINT4 u4FsQoSClassMapId,
                           UINT4 u4SetValFsQoSClassMapClfrId)
{
    tQoSClassMapNode   *pClsMapNode = NULL;

    pClsMapNode = QoSUtlGetClassMapNode (u4FsQoSClassMapId);
    if (pClsMapNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetClassMapNode () Returns "
                      "FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    pClsMapNode->u4ClassMapClfrId = (UINT1) u4SetValFsQoSClassMapClfrId;

    QoSValidateClsMapTblEntry (pClsMapNode);

    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhSetFsQoSClassMapPreColor
 Input       :  The Indices
                FsQoSClassMapId

                The Object 
                setValFsQoSClassMapPreColor
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsQoSClassMapPreColor (UINT4 u4FsQoSClassMapId,
                             INT4 i4SetValFsQoSClassMapPreColor)
{
    tQoSClassMapNode   *pClsMapNode = NULL;

    pClsMapNode = QoSUtlGetClassMapNode (u4FsQoSClassMapId);
    if (pClsMapNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetClassMapNode () Returns "
                      "FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    pClsMapNode->u1PreColor = (UINT1) i4SetValFsQoSClassMapPreColor;

    QoSValidateClsMapTblEntry (pClsMapNode);

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhSetFsQoSClassMapStatus
 Input       :  The Indices
                FsQoSClassMapId

                The Object 
                setValFsQoSClassMapStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsQoSClassMapStatus (UINT4 u4FsQoSClassMapId,
                           INT4 i4SetValFsQoSClassMapStatus)
{
    tQoSClassMapNode   *pClsMapNode = NULL;
    tQoSClassInfoNode  *pClsInfoNode = NULL;
    INT4                i4RetStatus = QOS_FAILURE;
    INT4                i4RowStatus = QOS_FAILURE;
    tIssL3FilterEntry  *pIssL3FilterEntry = NULL;
    UINT4               u4Type = QOS_ROWPOINTER_DEF;
    UINT4               u4Id = QOS_ROWPOINTER_DEF;
    UINT2               u2Index;
    tSNMP_OID_TYPE      ClfrOId;
    UINT4               au4TmpArray[14];
    INT4                i4ReturnValue = SNMP_SUCCESS;
    tQoSClass2PriorityNode *pCls2PriNode = NULL;
    tQoSInVlanMapNode  *pVlanQMapNode = NULL;

    ClfrOId.pu4_OidList = au4TmpArray;

    pClsMapNode = QoSUtlGetClassMapNode (u4FsQoSClassMapId);

    if (pClsMapNode != NULL)
    {
        i4RowStatus = (UINT4) pClsMapNode->u1Status;

        if (i4SetValFsQoSClassMapStatus != NOT_READY)
        {                        /* Entry found in the Table */
            if (i4RowStatus == i4SetValFsQoSClassMapStatus)
            {
                /* same value */
                return (SNMP_SUCCESS);
            }
        }
    }
    else
    {
        /* New Entry need to be created in the Table, only if 
         * i4TestValFsQoSClassMapStatus = CREATE_AND_WAIT */
        /* Optional */
        if (i4SetValFsQoSClassMapStatus == DESTROY)
        {
            return (SNMP_SUCCESS);
        }
        else if (i4SetValFsQoSClassMapStatus != CREATE_AND_WAIT)
        {
            return (SNMP_FAILURE);
        }
    }

    switch (i4SetValFsQoSClassMapStatus)
    {
            /* For a New Entry */
        case CREATE_AND_WAIT:

            pClsMapNode = QoSCreateClsMapTblEntry (u4FsQoSClassMapId);

            if (pClsMapNode == NULL)
            {
                QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSCreateClsMapTblEntry () "
                              "Returns FAILURE. \r\n", __FUNCTION__);

                return (SNMP_FAILURE);
            }

            pClsMapNode->u1Status = NOT_READY;

            break;

            /* Modify an Entry */
        case ACTIVE:

            if (gQoSGlobalInfo.eSysStatus == QOS_SYS_STATUS_ENABLE)
            {
                /* 1. Add the entry into the HARDWARE. */
                pClsInfoNode = QoSUtlGetClassInfoNode (pClsMapNode->u4ClassId);

                /* Update the PortList As per the data plane entries pointing to the Clfr */
                for (u2Index = 1; u2Index <= SYS_DEF_MAX_PHYSICAL_INTERFACES;
                     u2Index++)
                {
                    MEMSET (ClfrOId.pu4_OidList, 0, sizeof (au4TmpArray));
                    ClfrOId.u4_Length = 0;
                    /* Update the type and Id to default values before getting values */
                    u4Type = QOS_ROWPOINTER_DEF;
                    u4Id = QOS_ROWPOINTER_DEF;

                    nmhGetDiffServDataPathStart (u2Index, QOS_DATA_IN,
                                                 &ClfrOId);

                    QosGetTypeIdFromOid (&ClfrOId, &u4Type, &u4Id);

                    if ((u4Type == DS_CLFR)
                        && u4Id == pClsMapNode->u4ClassMapClfrId)
                    {
                        if ((pIssL3FilterEntry =
                             IssGetL3FilterEntry (pClsMapNode->u4L3FilterId)) !=
                            NULL)
                        {
                            QoSUtilOsixBitlistSetBit (pIssL3FilterEntry->
                                                      IssL3FilterInPortList,
                                                      u2Index,
                                                      (INT4)
                                                      sizeof (tIssPortList));

                        }
                    }

                    MEMSET (ClfrOId.pu4_OidList, 0, ClfrOId.u4_Length);

                    nmhGetDiffServDataPathStart (u2Index, QOS_DATA_OUT,
                                                 &ClfrOId);

                    QosGetTypeIdFromOid (&ClfrOId, &u4Type, &u4Id);

                    if ((u4Type == DS_CLFR)
                        && u4Id == pClsMapNode->u4ClassMapClfrId)
                    {
                        if ((pIssL3FilterEntry =
                             IssGetL3FilterEntry (pClsMapNode->u4L3FilterId)) !=
                            NULL)
                        {
                            QoSUtilOsixBitlistSetBit (pIssL3FilterEntry->
                                                      IssL3FilterInPortList,
                                                      u2Index,
                                                      (INT4)
                                                      sizeof (tIssPortList));

                        }
                    }
                }
                if ((pClsInfoNode != NULL) &&
                    (pClsInfoNode->pPlyMapNode != NULL) &&
                    (pClsInfoNode->pPlyMapNode->u1Status == ACTIVE))
                {
                    /* if a Policy is already create for this Class 
                     * Get the Policy Map Add the Class (Filters) to it */
                    i4RetStatus =
                        QoSHwWrMapClassToPolicy (pClsInfoNode->pPlyMapNode,
                                                 pClsMapNode, QOS_PLY_MAP);
                    if (i4RetStatus == QOS_FAILURE)
                    {
                        QOS_TRC_ARG1 (MGMT_TRC,
                                      "In %s : QoSHwWrMapClassToPolicy "
                                      " () Returns FAILURE. \r\n",
                                      __FUNCTION__);

                        return (SNMP_FAILURE);
                    }

                }                /* End of If */

                /*Program class to priority map in hw if class to pri entry
                 * present*/
                pCls2PriNode =
                    QoSUtlGetClass2PriorityNode (pClsMapNode->u4ClassId);

                if ((pCls2PriNode != NULL)
                    && (pCls2PriNode->u1Status == ACTIVE))
                {
                    i4RetStatus = QoSHwWrMapClassToIntPriority (pClsMapNode,
                                                                pCls2PriNode->
                                                                u1RegenPri,
                                                                QOS_NP_ADD);
                    if ((i4RetStatus == QOS_FAILURE)
                        || (i4RetStatus == QOS_NP_NOT_SUPPORTED))
                    {
                        if (i4RetStatus == QOS_NP_NOT_SUPPORTED)
                        {
                            CLI_SET_ERR (QOS_CLI_ERR_CLS_MAP_NP_NOT_SUPPORTED);
                        }
                        QOS_TRC_ARG1 (MGMT_TRC,
                                      "In %s :  QoSHwWrMapClassToIntPriority "
                                      " () Returns FAILURE. \r\n",
                                      __FUNCTION__);

                        return (SNMP_FAILURE);
                    }
                }
            }
            pClsMapNode->u1Status = ACTIVE;

            break;

        case NOT_IN_SERVICE:

            if ((pClsMapNode->u1Status == ACTIVE) &&
                (gQoSGlobalInfo.eSysStatus == QOS_SYS_STATUS_ENABLE))
            {
                /* 1. Remove the entry from the HARDWARE. */
                /* Get Policy for the CLASS */
                pClsInfoNode = QoSUtlGetClassInfoNode (pClsMapNode->u4ClassId);

                if ((pClsInfoNode != NULL) &&
                    (pClsInfoNode->pPlyMapNode != NULL) &&
                    (pClsMapNode->u4ClassId != QOS_CM_TBL_DEF_CLASS))

                {
                    /* IF the CLASS already linked with a Policy
                       Un Link the Class withe Policy */
                    i4RetStatus =
                        QoSHwWrUnmapClassFromPolicy (pClsInfoNode->pPlyMapNode,
                                                     pClsMapNode,
                                                     QOS_PLY_UNMAP);
                    if (i4RetStatus == QOS_FAILURE)
                    {
                        QOS_TRC_ARG1 (MGMT_TRC, "In %s : "
                                      "QoSHwWrUnmapClassFromPolicy "
                                      "() Returns FAILURE. \r\n", __FUNCTION__);

                        return (SNMP_FAILURE);
                    }

                    /*Program class to priority map in hw if class to pri entry
                     * present*/
                    pCls2PriNode =
                        QoSUtlGetClass2PriorityNode (pClsMapNode->u4ClassId);

                    if (pCls2PriNode != NULL)
                    {
                        i4RetStatus = QoSHwWrMapClassToIntPriority (pClsMapNode,
                                                                    pCls2PriNode->
                                                                    u1RegenPri,
                                                                    QOS_NP_DEL);
                        if ((i4RetStatus == QOS_FAILURE)
                            || (i4RetStatus == QOS_NP_NOT_SUPPORTED))
                        {
                            if (i4RetStatus == QOS_NP_NOT_SUPPORTED)
                            {
                                CLI_SET_ERR
                                    (QOS_CLI_ERR_CLS_MAP_NP_NOT_SUPPORTED);
                            }
                            QOS_TRC_ARG1 (MGMT_TRC,
                                          "In %s :  QoSHwWrMapClassToIntPriority "
                                          " () Returns FAILURE. \r\n",
                                          __FUNCTION__);

                            return (SNMP_FAILURE);
                        }
                    }

                    i4RetStatus = QoSHwWrDeleteClassMapEntry (pClsMapNode);
                    if (i4RetStatus == QOS_FAILURE)
                    {
                        QOS_TRC_ARG1 (MGMT_TRC, "In %s : "
                                      "QoSHwWrDeleteClassMapEntry () Returns "
                                      "FAILURE. \r\n", __FUNCTION__);

                        return (SNMP_FAILURE);
                    }
                }

            }

            pClsMapNode->u1Status = NOT_IN_SERVICE;

            break;

        case DESTROY:

            if ((pClsMapNode->u1Status == ACTIVE) &&
                (gQoSGlobalInfo.eSysStatus == QOS_SYS_STATUS_ENABLE))
            {
                /* 1. Remove the entry from the HARDWARE. */
                /* Get Policy for the CLASS */
                pClsInfoNode = QoSUtlGetClassInfoNode (pClsMapNode->u4ClassId);

                if (pClsInfoNode != NULL)
                {
                    /* IF the CLASS already linked with a Policy
                       Un Link the Class withe Policy */

                    if (pClsInfoNode->pPlyMapNode != NULL)
                    {
                        i4RetStatus =
                            QoSHwWrUnmapClassFromPolicy (pClsInfoNode->
                                                         pPlyMapNode,
                                                         pClsMapNode,
                                                         QOS_PLY_UNMAP);
                        if (i4RetStatus == QOS_FAILURE)
                        {
                            QOS_TRC_ARG1 (MGMT_TRC, "In %s : "
                                          "QoSHwWrUnmapClassFromPolicy "
                                          "() Returns FAILURE. \r\n",
                                          __FUNCTION__);

                            return (SNMP_FAILURE);
                        }
                        if (pClsInfoNode->pPlyMapNode->u4ClassId !=
                            QOS_CM_TBL_DEF_CLASS)
                        {
                            pClsInfoNode->pPlyMapNode->u4ClassId = 0;
                            pClsInfoNode->pPlyMapNode = NULL;
                            pClsInfoNode->u4ClassId = 0;
                        }

                    }

                    /* Delete class to priority map from hw if class to pri entry
                     * present */
                    pCls2PriNode =
                        QoSUtlGetClass2PriorityNode (pClsMapNode->u4ClassId);

                    if (pCls2PriNode != NULL)
                    {
                        i4RetStatus = QoSHwWrMapClassToIntPriority (pClsMapNode,
                                                                    pCls2PriNode->
                                                                    u1RegenPri,
                                                                    QOS_NP_DEL);
                        if ((i4RetStatus == QOS_FAILURE)
                            || (i4RetStatus == QOS_NP_NOT_SUPPORTED))
                        {
                            if (i4RetStatus == QOS_NP_NOT_SUPPORTED)
                            {
                                CLI_SET_ERR
                                    (QOS_CLI_ERR_CLS_MAP_NP_NOT_SUPPORTED);
                            }
                            QOS_TRC_ARG1 (MGMT_TRC,
                                          "In %s :  QoSHwWrMapClassToIntPriority "
                                          " () Returns FAILURE. \r\n",
                                          __FUNCTION__);

                            return (SNMP_FAILURE);
                        }
                    }
                    /* Remove the Class Entry */
                    i4RetStatus = QoSHwWrDeleteClassMapEntry (pClsMapNode);
                    if (i4RetStatus == QOS_FAILURE)
                    {
                        QOS_TRC_ARG1 (MGMT_TRC, "In %s : "
                                      "QoSHwWrDeleteClassMapEntry () "
                                      "Returns FAILURE. \r\n", __FUNCTION__);
                        return (SNMP_FAILURE);
                    }
                }
            }

            /* Remove Vlan map node reference */
            if (pClsMapNode->u4VlanMapId != 0)
            {
                pVlanQMapNode =
                    QoSUtlGetVlanQMapNode (pClsMapNode->u4VlanMapId);
                if (pVlanQMapNode != NULL)
                {
                    pVlanQMapNode->u4RefCount = (pVlanQMapNode->u4RefCount) - 1;
                    pVlanQMapNode->u4ClassMapNode = 0;
                }
                pClsMapNode->u4VlanMapId = 0;
            }

            /* 2. Remove the entry from the Internal Reference. */
            i4RetStatus = QoSClsInfoDelFilters (pClsMapNode);

            if (i4RetStatus == QOS_FAILURE)
            {
                QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSClsInfoDelFilters () "
                              "Returns FAILURE. \r\n", __FUNCTION__);

                return (SNMP_FAILURE);
            }

            /* 3. Remove the entry from the SOFTWARE. */
            i4RetStatus = QoSDeleteClsMapTblEntry (pClsMapNode);

            if (i4RetStatus == QOS_FAILURE)
            {
                QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSDeleteClsMapTblEntry () "
                              "Returns FAILURE. \r\n", __FUNCTION__);

                i4ReturnValue = SNMP_FAILURE;
            }

            break;
        case NOT_READY:
            /* IF the CLASS already linked with a Policy
               Dont allow to configure or modify the CLASS value 
               unless unmapping it from the policy map */
            pClsInfoNode = QoSUtlGetClassInfoNode (pClsMapNode->u4ClassId);
            if ((pClsInfoNode != NULL) && (pClsInfoNode->pPlyMapNode != NULL))
            {
                if (pClsInfoNode->pPlyMapNode->u4ClassId !=
                    QOS_CM_TBL_DEF_CLASS)
                {
                    CLI_SET_ERR (QOS_CLI_ERR_CLS_MAP_RFE);
                    i4ReturnValue = SNMP_FAILURE;
                }
            }
            break;

        default:
            i4ReturnValue = SNMP_FAILURE;
            break;
    }

    return ((INT1) i4ReturnValue);

}

/* Low Level TEST Routines for All Objects  */
/****************************************************************************
 Function    :  nmhTestv2FsQoSClassMapName
 Input       :  The Indices
                FsQoSClassMapId

                The Object 
                testValFsQoSClassMapName
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsQoSClassMapName (UINT4 *pu4ErrorCode,
                            UINT4 u4FsQoSClassMapId,
                            tSNMP_OCTET_STRING_TYPE * pTestValFsQoSClassMapName)
{
    INT4                i4RetStatus = QOS_FAILURE;
    tQoSClassMapNode   *pClsMapNode = NULL;

    *pu4ErrorCode = SNMP_ERR_NO_ERROR;

    /* Check Index */
    i4RetStatus = QoSUtlValidateClsMapTblIdxInst (u4FsQoSClassMapId,
                                                  pu4ErrorCode);

    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlValidateClsMapTblIdxInst () "
                      "Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    /* Check  Object's Value */
    i4RetStatus = QoSUtlValidateTableName (pTestValFsQoSClassMapName,
                                           pu4ErrorCode);

    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlValidateTableName () "
                      "Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    pClsMapNode = QoSUtlGetClassMapNode (u4FsQoSClassMapId);
    if (pClsMapNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetClassMapNode () Returns "
                      "FAILURE. \r\n", __FUNCTION__);

        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (QOS_CLI_ERR_CLS_MAP_NO_ENTRY);
        return (SNMP_FAILURE);
    }

    /* Check the Name is same as Current Name */
    if ((STRLEN (pClsMapNode->au1Name)) == (UINT4)
        pTestValFsQoSClassMapName->i4_Length)
    {
        if ((STRNCMP (pClsMapNode->au1Name,
                      pTestValFsQoSClassMapName->pu1_OctetList,
                      pTestValFsQoSClassMapName->i4_Length)) == 0)
        {
            return (SNMP_SUCCESS);
        }
    }

    /* Check the name is Unique or Not */
    i4RetStatus = QoSUtlIsUniqueName (pTestValFsQoSClassMapName,
                                      QOS_TBL_TYPE_CLS_MAP);
    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlIsUniqueName () Returns "
                      "FAILURE. \r\n", __FUNCTION__);

        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return (SNMP_FAILURE);
    }

    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhTestv2FsQoSClassMapL2FilterId
 Input       :  The Indices
                FsQoSClassMapId

                The Object 
                testValFsQoSClassMapL2FilterId
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsQoSClassMapL2FilterId (UINT4 *pu4ErrorCode,
                                  UINT4 u4FsQoSClassMapId,
                                  UINT4 u4TestValFsQoSClassMapL2FilterId)
{
    tQoSClassMapNode   *pClsMapNode = NULL;
    INT4                i4RetStatus = QOS_FAILURE;

    *pu4ErrorCode = SNMP_ERR_NO_ERROR;

    /* Check Index */
    i4RetStatus = QoSUtlValidateClsMapTblIdxInst (u4FsQoSClassMapId,
                                                  pu4ErrorCode);
    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlValidateClsMapTblIdxInst () "
                      "Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    /* Reset L2 Filter ID */
    if (u4TestValFsQoSClassMapL2FilterId == QOS_ROWPOINTER_DEF)
    {
        return (SNMP_SUCCESS);
    }

    /* Check  Object's Value */
    i4RetStatus = QoSUtlValidateL2Filter (u4TestValFsQoSClassMapL2FilterId,
                                          pu4ErrorCode);
    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlValidateL2Filter () "
                      "Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    pClsMapNode = QoSUtlGetClassMapNode (u4FsQoSClassMapId);
    if (pClsMapNode == NULL)
    {
        CLI_SET_ERR (QOS_CLI_ERR_CLS_MAP_NO_ENTRY);

        *pu4ErrorCode = SNMP_ERR_NO_CREATION;

        return (SNMP_FAILURE);
    }

    i4RetStatus = QoSUtlValidateL2FilterIdForClassMapId (u4FsQoSClassMapId,
                                                         u4TestValFsQoSClassMapL2FilterId,
                                                         pu4ErrorCode);
    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC,
                      "In %s : QoSUtlValidateL2FilterIdForClassMapId () "
                      "Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }
    /* Check PriMapId Field Should be NULL */
    if (pClsMapNode->u4PriorityMapId != 0)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "%s :Either L2 and/or L3 Filters or "
                      "PriMapId is associated with this class-map .\r\n",
                      __FUNCTION__);

        CLI_SET_ERR (QOS_CLI_ERR_L2_L3_OR_PRI);

        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;

        return (SNMP_FAILURE);
    }

    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhTestv2FsQoSClassMapL3FilterId
 Input       :  The Indices
                FsQoSClassMapId

                The Object 
                testValFsQoSClassMapL3FilterId
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsQoSClassMapL3FilterId (UINT4 *pu4ErrorCode,
                                  UINT4 u4FsQoSClassMapId,
                                  UINT4 u4TestValFsQoSClassMapL3FilterId)
{
    tQoSClassMapNode   *pClsMapNode = NULL;
    INT4                i4RetStatus = QOS_FAILURE;

    *pu4ErrorCode = SNMP_ERR_NO_ERROR;

    /* Check Index */
    i4RetStatus = QoSUtlValidateClsMapTblIdxInst (u4FsQoSClassMapId,
                                                  pu4ErrorCode);
    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlValidateClsMapTblIdxInst () "
                      "Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    /* Reset L3 Filter ID */
    if (u4TestValFsQoSClassMapL3FilterId == QOS_ROWPOINTER_DEF)
    {
        return (SNMP_SUCCESS);
    }

    /* Check  Object's Value */
    i4RetStatus = QoSUtlValidateL3Filter (u4TestValFsQoSClassMapL3FilterId,
                                          pu4ErrorCode);
    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlValidateL3Filter () "
                      "Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    pClsMapNode = QoSUtlGetClassMapNode (u4FsQoSClassMapId);
    if (pClsMapNode == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;

        CLI_SET_ERR (QOS_CLI_ERR_CLS_MAP_NO_ENTRY);

        return (SNMP_FAILURE);
    }

    i4RetStatus = QoSUtlValidateL3FilterIdForClassMapId (u4FsQoSClassMapId,
                                                         u4TestValFsQoSClassMapL3FilterId,
                                                         pu4ErrorCode);
    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC,
                      "In %s : QoSUtlValidateL3FilterIdForClassMapId () "
                      "Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    /* Check PriMapId Field Should be NULL */
    if (pClsMapNode->u4PriorityMapId != 0)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "%s :Either L2 and/or L3 Filters or "
                      "PriMapId is associated with this class-map .\r\n",
                      __FUNCTION__);

        CLI_SET_ERR (QOS_CLI_ERR_L2_L3_OR_PRI);

        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;

        return (SNMP_FAILURE);
    }

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhTestv2FsQoSClassMapPriorityMapId
 Input       :  The Indices
                FsQoSClassMapId

                The Object 
                testValFsQoSClassMapPriorityMapId
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsQoSClassMapPriorityMapId (UINT4 *pu4ErrorCode,
                                     UINT4 u4FsQoSClassMapId,
                                     UINT4 u4TestValFsQoSClassMapPriorityMapId)
{
    tQoSClassMapNode   *pClsMapNode = NULL;
    INT4                i4RetStatus = QOS_FAILURE;

    *pu4ErrorCode = SNMP_ERR_NO_ERROR;

    /* Check Index */
    i4RetStatus = QoSUtlValidateClsMapTblIdxInst (u4FsQoSClassMapId,
                                                  pu4ErrorCode);
    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlValidateClsMapTblIdxInst () "
                      "Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    /* Reset PriMapId */
    if (u4TestValFsQoSClassMapPriorityMapId == QOS_ROWPOINTER_DEF)
    {
        return (SNMP_SUCCESS);
    }

    /* Check  Object's Value */
    i4RetStatus =
        QoSUtlValidatePriorityMapId (u4TestValFsQoSClassMapPriorityMapId,
                                     pu4ErrorCode);
    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlValidatePriorityMapId () "
                      "Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    pClsMapNode = QoSUtlGetClassMapNode (u4FsQoSClassMapId);
    if (pClsMapNode == NULL)
    {
        CLI_SET_ERR (QOS_CLI_ERR_CLS_MAP_NO_ENTRY);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (SNMP_FAILURE);
    }

    /* Check L2 and L3 Fields Should be NULL */
    if ((pClsMapNode->u4L2FilterId != 0) || (pClsMapNode->u4L3FilterId != 0))
    {
        QOS_TRC_ARG1 (MGMT_TRC, "%s :Either L2 and/or L3 Filters or "
                      "PriMapId is associated with this class-map .\r\n",
                      __FUNCTION__);

        CLI_SET_ERR (QOS_CLI_ERR_L2_L3_OR_PRI);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return (SNMP_FAILURE);
    }

    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhTestv2FsQoSClassMapCLASS
 Input       :  The Indices
                FsQoSClassMapId

                The Object 
                testValFsQoSClassMapCLASS
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsQoSClassMapCLASS (UINT4 *pu4ErrorCode,
                             UINT4 u4FsQoSClassMapId,
                             UINT4 u4TestValFsQoSClassMapCLASS)
{
    INT4                i4RetStatus = QOS_FAILURE;
    tQoSClassMapNode   *pClsMapNode = NULL;
    tIssL3FilterEntry  *pIssL3FilterEntry = NULL;
    tIssL2FilterEntry  *pIssL2FilterEntry = NULL;

    *pu4ErrorCode = SNMP_ERR_NO_ERROR;

    /* Check Index */
    i4RetStatus = QoSUtlValidateClsMapTblIdxInst (u4FsQoSClassMapId,
                                                  pu4ErrorCode);

    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlValidateClsMapTblIdxInst () "
                      "Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    /* Check  Object's Value */
    if ((u4TestValFsQoSClassMapCLASS < 1) ||
        (u4TestValFsQoSClassMapCLASS > QOS_CLS2PRI_MAP_TBL_MAX_INDEX_RANGE))
    {
        QOS_TRC_ARG3 (MGMT_TRC, "%s : CLASS %d is out of Range. "
                      "The Range Should be (%1-%d). \r\n", __FUNCTION__,
                      u4TestValFsQoSClassMapCLASS,
                      QOS_CLS2PRI_MAP_TBL_MAX_INDEX_RANGE);

        CLI_SET_ERR (QOS_CLI_ERR_CLS_MAP_INVALID_CLS);

        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;

        return (SNMP_FAILURE);
    }

    pClsMapNode = QoSUtlGetClassMapNode (u4FsQoSClassMapId);

    if (pClsMapNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetClassMapNode () "
                      "Returns FAILURE. \r\n", __FUNCTION__);

        CLI_SET_ERR (QOS_CLI_ERR_CLS_MAP_NO_ENTRY);

        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;

        return (SNMP_FAILURE);
    }

    /* A CLASS should not be set on a filter which is configured
     * to drop the packets(Deny Filter). */
    if (pClsMapNode->u4L3FilterId != 0)
    {

        pIssL3FilterEntry =
            IssGetL3FilterTableEntry ((INT4) pClsMapNode->u4L3FilterId);

        if (pIssL3FilterEntry != NULL)
        {
            if (pIssL3FilterEntry->IssL3FilterAction == ISS_DROP)
            {
                QOS_TRC_ARG1 (MGMT_TRC, "In %s : QOS cannot be applied for "
                              "a deny filter. \r\n", __FUNCTION__);

                CLI_SET_ERR (QOS_CLI_ERR_CLS_MAP_INVALID_FILTER_ACTION);

                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;

                return (SNMP_FAILURE);
            }
        }
    }
    if (pClsMapNode->u4L2FilterId != 0)
    {

        pIssL2FilterEntry =
            IssGetL2FilterTableEntry ((INT4) pClsMapNode->u4L2FilterId);

        if (pIssL2FilterEntry != NULL)
        {
            if (pIssL2FilterEntry->IssL2FilterAction == ISS_DROP)
            {
                QOS_TRC_ARG1 (MGMT_TRC, "In %s : QOS cannot be applied for "
                              "a deny filter. \r\n", __FUNCTION__);

                CLI_SET_ERR (QOS_CLI_ERR_CLS_MAP_INVALID_FILTER_ACTION);

                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;

                return (SNMP_FAILURE);
            }
        }
    }

    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhTestv2FsQoSClassMapClfrId
 Input       :  The Indices
                FsQoSClassMapId

                The Object 
                testValFsQoSClassMapClfrId
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsQoSClassMapClfrId (UINT4 *pu4ErrorCode, UINT4 u4FsQoSClassMapId,
                              UINT4 u4TestValFsQoSClassMapClfrId)
{
    UNUSED_PARAM (u4FsQoSClassMapId);
    if (QOS_CLFR_ID_VALID (u4TestValFsQoSClassMapClfrId) != ISS_TRUE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsQoSClassMapPreColor
 Input       :  The Indices
                FsQoSClassMapId

                The Object 
                testValFsQoSClassMapPreColor
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsQoSClassMapPreColor (UINT4 *pu4ErrorCode, UINT4 u4FsQoSClassMapId,
                                INT4 i4TestValFsQoSClassMapPreColor)
{
    INT4                i4RetStatus = QOS_FAILURE;

    *pu4ErrorCode = SNMP_ERR_NO_ERROR;

    /* Check Index */
    i4RetStatus = QoSUtlValidateClsMapTblIdxInst (u4FsQoSClassMapId,
                                                  pu4ErrorCode);
    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlValidateClsMapTblIdxInst () "
                      "Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    /* Check  Object's Value */
    if ((i4TestValFsQoSClassMapPreColor < QOS_CLS_PRE_COLOR_MIN_VAL) ||
        (i4TestValFsQoSClassMapPreColor > QOS_CLS_PRE_COLOR_MAX_VAL))
    {
        QOS_TRC_ARG3 (MGMT_TRC, "%s :Policy Pre Color value is out of range."
                      " The range should be Min %d - Max %d. \r\n",
                      __FUNCTION__, QOS_CLS_PRE_COLOR_MIN_VAL,
                      QOS_CLS_PRE_COLOR_MAX_VAL);

        CLI_SET_ERR (QOS_CLI_ERR_PRE_COLOR_INVALID);

        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;

        return (SNMP_FAILURE);
    }

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhTestv2FsQoSClassMapStatus
 Input       :  The Indices
                FsQoSClassMapId

                The Object 
                testValFsQoSClassMapStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsQoSClassMapStatus (UINT4 *pu4ErrorCode,
                              UINT4 u4FsQoSClassMapId,
                              INT4 i4TestValFsQoSClassMapStatus)
{
    INT4                i4RetStatus = QOS_FAILURE;
    UINT4               u4Count = 0;
    tQoSClassMapNode   *pClsMapNode = NULL;
    INT4                i4ReturnValue = SNMP_SUCCESS;

    if (gQoSGlobalInfo.eSysControl != QOS_SYS_CNTL_START)
    {
        QOS_TRC (MGMT_TRC, QOS_MODULE_SHUTDOW_ERROR_MSG);
        CLI_SET_ERR (QOS_CLI_ERR_MODULE_SHUTDOW);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return (SNMP_FAILURE);
    }

    if ((i4TestValFsQoSClassMapStatus == QOS_CREATE_AND_WAIT) &&
        (QOS_CLFR_ELEM_COUNT >= QOS_CLS_MAP_TBL_MAX_ENTRIES))
    {
        CLI_SET_ERR (QOS_CLI_ERR_CLS_MAP_MAX_ENTRY);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return (SNMP_FAILURE);
    }

    *pu4ErrorCode = SNMP_ERR_NO_ERROR;

    /* Check the Range of Index Values (Min-Max) */
    i4RetStatus = QOS_CHECK_CLS_MAP_TBL_INDEX_RANGE (u4FsQoSClassMapId);

    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG3 (MGMT_TRC, "%s : Class Map Id %d is out of Range."
                      " The Range Should be (1-%d). \r\n", __FUNCTION__,
                      u4FsQoSClassMapId, QOS_CLS_MAP_TBL_MAX_INDEX_RANGE);

        CLI_SET_ERR (QOS_CLI_ERR_CLS_MAP_RANGE);

        return (SNMP_FAILURE);
    }

    pClsMapNode = QoSUtlGetClassMapNode (u4FsQoSClassMapId);

    if (pClsMapNode == NULL)
    {
        /* Entry Not Found In The Table */
        switch (i4TestValFsQoSClassMapStatus)
        {
            case CREATE_AND_GO:
            case ACTIVE:
            case NOT_IN_SERVICE:
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                i4ReturnValue = SNMP_FAILURE;
                break;

            case CREATE_AND_WAIT:
                /* Check the No of Entries in the Table */
                i4RetStatus =
                    RBTreeCount (gQoSGlobalInfo.pRbClsMapTbl, &u4Count);

                if ((i4RetStatus == RB_FAILURE) ||
                    (u4Count > QOS_CLS_MAP_TBL_MAX_ENTRIES))
                {
                    QOS_TRC_ARG2 (MGMT_TRC, "%s : Max No of Class Map Table "
                                  "Entries are Configured. No of Max Entries"
                                  " %d. \r\n", __FUNCTION__,
                                  QOS_CLS_MAP_TBL_MAX_ENTRIES);

                    CLI_SET_ERR (QOS_CLI_ERR_CLS_MAP_MAX_ENTRY);

                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;

                    return (SNMP_FAILURE);
                }
                break;

            case DESTROY:
                CLI_SET_ERR (QOS_CLI_ERR_CLS_MAP_NO_ENTRY);
                *pu4ErrorCode = SNMP_ERR_NO_CREATION;
                i4ReturnValue = SNMP_FAILURE;
                break;

            default:
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                i4ReturnValue = SNMP_FAILURE;
                break;
        }
    }
    else
    {
        /* Entry Found In The Table */
        switch (i4TestValFsQoSClassMapStatus)
        {
            case ACTIVE:

                if ((pClsMapNode->u1Status == NOT_READY))
                {
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                    i4ReturnValue = SNMP_FAILURE;
                }

                break;

            case NOT_IN_SERVICE:

                if ((pClsMapNode->u1Status == NOT_READY))
                {
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                    return (SNMP_FAILURE);
                }

                /* 1. Check the entry in the Internal Reference.
                 * can we delete the node or not */
                i4RetStatus =
                    QoSTestClsInfoDelFilters (pClsMapNode,
                                              i4TestValFsQoSClassMapStatus);

                if (i4RetStatus == QOS_FAILURE)
                {
                    QOS_TRC_ARG1 (MGMT_TRC, "In %s : ERROR - This Entry is "
                                  "referenced by Policy Map Table Entry.\r\n",
                                  __FUNCTION__);

                    QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSTestClsInfoDelFilters"
                                  " () Returns FAILURE. \r\n", __FUNCTION__);

                    CLI_SET_ERR (QOS_CLI_ERR_CLS_MAP_RFE);

                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;

                    return (SNMP_FAILURE);
                }

                /* check whether Class Map is mapped to QMAP */
                if (QoSUtlIsClassMaptoQMapEntry (pClsMapNode->u4ClassId)
                    == TRUE)
                {
                    QOS_TRC_ARG1 (MGMT_TRC, "In %s : ERROR - This Entry is "
                                  "referenced by Queue Map Table Entry.\r\n",
                                  __FUNCTION__);
                    QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSTestClsInfoDelFilters"
                                  " () Returns FAILURE. \r\n", __FUNCTION__);

                    CLI_SET_ERR (QOS_CLI_ERR_CLS_MAP_RFE_QMAP);
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                    return (SNMP_FAILURE);
                }

                break;

            case DESTROY:

                /* 1. Check the entry in the Internal Reference.
                 * can we delete the node or not */
                i4RetStatus =
                    QoSTestClsInfoDelFilters (pClsMapNode,
                                              i4TestValFsQoSClassMapStatus);

                if (i4RetStatus == QOS_FAILURE)
                {
                    QOS_TRC_ARG1 (MGMT_TRC, "In %s : ERROR - This Entry is "
                                  "referenced by Policy Map Table Entry.\r\n",
                                  __FUNCTION__);

                    QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSTestClsInfoDelFilters"
                                  " () Returns FAILURE. \r\n", __FUNCTION__);

                    CLI_SET_ERR (QOS_CLI_ERR_CLS_MAP_RFE);

                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                    return (SNMP_FAILURE);
                }

                /* check whether Class Map is mapped to QMAP */
                if (QoSUtlIsClassMaptoQMapEntry (pClsMapNode->u4ClassId)
                    == TRUE)
                {
                    QOS_TRC_ARG1 (MGMT_TRC, "In %s : ERROR - This Entry is "
                                  "referenced by Queue Map Table Entry.\r\n",
                                  __FUNCTION__);
                    QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSTestClsInfoDelFilters"
                                  " () Returns FAILURE. \r\n", __FUNCTION__);

                    CLI_SET_ERR (QOS_CLI_ERR_CLS_MAP_RFE_QMAP);
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                    return (SNMP_FAILURE);
                }
                break;

            case CREATE_AND_WAIT:
            case CREATE_AND_GO:
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                i4ReturnValue = SNMP_FAILURE;
                break;

            default:
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                i4ReturnValue = SNMP_FAILURE;
                break;

        }                        /* End of switch */

    }                            /* End of if */

    return ((INT1) i4ReturnValue);
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsQoSClassMapTable
 Input       :  The Indices
                FsQoSClassMapId
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsQoSClassMapTable (UINT4 *pu4ErrorCode,
                            tSnmpIndexList * pSnmpIndexList,
                            tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsQoSClassToPriorityTable. */
/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsQoSClassToPriorityTable
 Input       :  The Indices
                FsQoSClassToPriorityCLASS
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */
INT1
nmhValidateIndexInstanceFsQoSClassToPriorityTable (UINT4
                                                   u4FsQoSClassToPriorityCLASS)
{
    tQoSClass2PriorityNode *pCls2PriNode = NULL;

    if (gQoSGlobalInfo.eSysControl != QOS_SYS_CNTL_START)
    {
        QOS_TRC (MGMT_TRC, QOS_MODULE_SHUTDOW_ERROR_MSG);
        return (SNMP_FAILURE);
    }

    pCls2PriNode = QoSUtlGetClass2PriorityNode (u4FsQoSClassToPriorityCLASS);
    if (pCls2PriNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetClass2PriorityNode () "
                      " Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsQoSClassToPriorityTable
 Input       :  The Indices
                FsQoSClassToPriorityCLASS
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsQoSClassToPriorityTable (UINT4 *pu4FsQoSClassToPriorityCLASS)
{
    INT4                i4RetStatus = QOS_FAILURE;

    if (gQoSGlobalInfo.eSysControl != QOS_SYS_CNTL_START)
    {
        QOS_TRC (MGMT_TRC, QOS_MODULE_SHUTDOW_ERROR_MSG);
        return (SNMP_FAILURE);
    }

    i4RetStatus = QoSGetNextCls2PriTblEntryIndex (0,
                                                  pu4FsQoSClassToPriorityCLASS);
    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSGetNextCls2PriTblEntryIndex () "
                      " Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhGetNextIndexFsQoSClassToPriorityTable
 Input       :  The Indices
                FsQoSClassToPriorityCLASS
                nextFsQoSClassToPriorityCLASS
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsQoSClassToPriorityTable (UINT4 u4FsQoSClassToPriorityCLASS,
                                          UINT4
                                          *pu4NextFsQoSClassToPriorityCLASS)
{
    INT4                i4RetStatus = QOS_FAILURE;

    if (gQoSGlobalInfo.eSysControl != QOS_SYS_CNTL_START)
    {
        QOS_TRC (MGMT_TRC, QOS_MODULE_SHUTDOW_ERROR_MSG);
        return (SNMP_FAILURE);
    }

    i4RetStatus = QoSGetNextCls2PriTblEntryIndex
        (u4FsQoSClassToPriorityCLASS, pu4NextFsQoSClassToPriorityCLASS);
    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSGetNextCls2PriTblEntryIndex () "
                      " Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    return (SNMP_SUCCESS);
}

/* Low Level GET Routine for All Objects  */
/****************************************************************************
 Function    :  nmhGetFsQoSClassToPriorityRegenPri
 Input       :  The Indices
                FsQoSClassToPriorityCLASS

                The Object 
                retValFsQoSClassToPriorityRegenPri
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsQoSClassToPriorityRegenPri (UINT4 u4FsQoSClassToPriorityCLASS,
                                    UINT4
                                    *pu4RetValFsQoSClassToPriorityRegenPri)
{
    tQoSClass2PriorityNode *pCls2PriNode = NULL;

    pCls2PriNode = QoSUtlGetClass2PriorityNode (u4FsQoSClassToPriorityCLASS);

    if (pCls2PriNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetClass2PriorityNode () "
                      " Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    *pu4RetValFsQoSClassToPriorityRegenPri = (UINT4) pCls2PriNode->u1RegenPri;

    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhGetFsQoSClassToPriorityGroupName
 Input       :  The Indices
                FsQoSClassToPriorityCLASS

                The Object 
                retValFsQoSClassToPriorityGroupName
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsQoSClassToPriorityGroupName (UINT4
                                     u4FsQoSClassToPriorityCLASS,
                                     tSNMP_OCTET_STRING_TYPE *
                                     pRetValFsQoSClassToPriorityGroupName)
{
    tQoSClass2PriorityNode *pCls2PriNode = NULL;
    UINT1              *pu1Str = NULL;

    if ((pRetValFsQoSClassToPriorityGroupName == NULL) ||
        (pRetValFsQoSClassToPriorityGroupName->pu1_OctetList == NULL))
    {
        return (SNMP_FAILURE);
    }

    pCls2PriNode = QoSUtlGetClass2PriorityNode (u4FsQoSClassToPriorityCLASS);
    if (pCls2PriNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetClass2PriorityNode () "
                      " Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    pu1Str = &(pCls2PriNode->au1GroupName[0]);
    QOS_UTL_COPY_TABLE_NAME (pRetValFsQoSClassToPriorityGroupName, pu1Str);

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFsQoSClassToPriorityStatus
 Input       :  The Indices
                FsQoSClassToPriorityCLASS

                The Object 
                retValFsQoSClassToPriorityStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsQoSClassToPriorityStatus (UINT4 u4FsQoSClassToPriorityCLASS,
                                  INT4 *pi4RetValFsQoSClassToPriorityStatus)
{
    tQoSClass2PriorityNode *pCls2PriNode = NULL;

    pCls2PriNode = QoSUtlGetClass2PriorityNode (u4FsQoSClassToPriorityCLASS);

    if (pCls2PriNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetClass2PriorityNode () "
                      " Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    *pi4RetValFsQoSClassToPriorityStatus = (INT4) (pCls2PriNode->u1Status);

    return (SNMP_SUCCESS);

}

/* Low Level SET Routine for All Objects  */
/****************************************************************************
 Function    :  nmhSetFsQoSClassToPriorityRegenPri
 Input       :  The Indices
                FsQoSClassToPriorityCLASS

                The Object 
                setValFsQoSClassToPriorityRegenPri
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsQoSClassToPriorityRegenPri (UINT4 u4FsQoSClassToPriorityCLASS,
                                    UINT4 u4SetValFsQoSClassToPriorityRegenPri)
{
    tQoSClass2PriorityNode *pCls2PriNode = NULL;

    pCls2PriNode = QoSUtlGetClass2PriorityNode (u4FsQoSClassToPriorityCLASS);

    if (pCls2PriNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetClass2PriorityNode () "
                      " Returns FAILURE. \r\n", __FUNCTION__);
        return (SNMP_FAILURE);
    }

    pCls2PriNode->u1RegenPri = (UINT1) u4SetValFsQoSClassToPriorityRegenPri;

    /* 1. Check(Test) Dependency for this Entry.   */
    QoSValidateCls2PriTblEntry (pCls2PriNode);

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhSetFsQoSClassToPriorityGroupName
 Input       :  The Indices
                FsQoSClassToPriorityCLASS

                The Object 
                setValFsQoSClassToPriorityGroupName
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsQoSClassToPriorityGroupName (UINT4 u4FsQoSClassToPriorityCLASS,
                                     tSNMP_OCTET_STRING_TYPE *
                                     pSetValFsQoSClassToPriorityGroupName)
{
    tQoSClass2PriorityNode *pCls2PriNode = NULL;
    UINT1              *pu1Str = NULL;

    pCls2PriNode = QoSUtlGetClass2PriorityNode (u4FsQoSClassToPriorityCLASS);

    if (pCls2PriNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetClass2PriorityNode () "
                      " Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    pu1Str = &(pCls2PriNode->au1GroupName[0]);
    QOS_UTL_SET_TABLE_NAME (pu1Str, pSetValFsQoSClassToPriorityGroupName);

    /* 1. Check(Test) Dependency for this Entry.   */
    QoSValidateCls2PriTblEntry (pCls2PriNode);

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhSetFsQoSClassToPriorityStatus
 Input       :  The Indices
                FsQoSClassToPriorityCLASS

                The Object 
                setValFsQoSClassToPriorityStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsQoSClassToPriorityStatus (UINT4 u4FsQoSClassToPriorityCLASS,
                                  INT4 i4SetValFsQoSClassToPriorityStatus)
{
    tQoSClass2PriorityNode *pCls2PriNode = NULL;
    INT4                i4RetStatus = QOS_FAILURE;
    INT4                i4RowStatus = QOS_FAILURE;
    INT4                i4ReturnValue = SNMP_SUCCESS;
    tQoSClassInfoNode  *pClsInfoNode = NULL;
    tQoSFilterInfoNode *pFltInfoNode = NULL;

    pCls2PriNode = QoSUtlGetClass2PriorityNode (u4FsQoSClassToPriorityCLASS);
    if (pCls2PriNode != NULL)
    {
        i4RowStatus = (UINT4) pCls2PriNode->u1Status;

        /* Entry found in the Table */
        if (i4RowStatus == i4SetValFsQoSClassToPriorityStatus)
        {
            /* same value */
            return (SNMP_SUCCESS);
        }
    }
    else
    {
        /* New Entry need to be created in the Table, only if 
         * i4TestValFsQoSClassMapStatus = CREATE_AND_WAIT */
        /* Optional */
        if (i4SetValFsQoSClassToPriorityStatus == DESTROY)
        {
            return (SNMP_SUCCESS);
        }
    }

    switch (i4SetValFsQoSClassToPriorityStatus)
    {
            /* For a New Entry */
        case CREATE_AND_WAIT:

            pCls2PriNode =
                QoSCreateCls2PriTblEntry (u4FsQoSClassToPriorityCLASS);
            if (pCls2PriNode == NULL)
            {
                QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSCreateCls2PriTblEntry () "
                              " Returns FAILURE. \r\n", __FUNCTION__);

                return (SNMP_FAILURE);
            }
            if (pCls2PriNode != NULL)
            {
                pCls2PriNode->u1Status = NOT_READY;
            }

            break;

            /* Modify an Entry */
        case ACTIVE:

            if (pCls2PriNode != NULL)
            {

                /* Get all The Class Map for a given Policy */
                pClsInfoNode =
                    QoSUtlGetClassInfoNode (u4FsQoSClassToPriorityCLASS);

                if (pClsInfoNode == NULL)
                {
                    pCls2PriNode->u1Status = ACTIVE;
                    break;
                }

                /* Scan the Filter list in the ClassInfo */
                TMO_SLL_Scan (&(pClsInfoNode->SllFltInfo), pFltInfoNode,
                              tQoSFilterInfoNode *)
                {
                    i4RetStatus = QoSHwWrMapClassToIntPriority (pFltInfoNode->
                                                                pClsMapNode,
                                                                pCls2PriNode->
                                                                u1RegenPri,
                                                                QOS_NP_ADD);
                    if ((i4RetStatus == QOS_FAILURE)
                        || (i4RetStatus == QOS_NP_NOT_SUPPORTED))
                    {
                        if (i4RetStatus == QOS_NP_NOT_SUPPORTED)
                        {
                            CLI_SET_ERR (QOS_CLI_ERR_CLS_MAP_NP_NOT_SUPPORTED);
                        }
                        QOS_TRC_ARG1 (MGMT_TRC,
                                      "In %s :  QoSHwWrMapClassToIntPriority "
                                      " () Returns FAILURE. \r\n",
                                      __FUNCTION__);
                        return (SNMP_FAILURE);
                    }
                }
                pCls2PriNode->u1Status = ACTIVE;
            }
            break;

        case NOT_IN_SERVICE:

            if (pCls2PriNode != NULL)
            {
                /* No need to program into HARDWARE */
                pCls2PriNode->u1Status = NOT_IN_SERVICE;
            }

            break;

        case DESTROY:
            /* Delete class to priority map from hw if class to pri entry
             * present */

            pClsInfoNode = QoSUtlGetClassInfoNode (u4FsQoSClassToPriorityCLASS);
            if (pClsInfoNode != NULL)
            {
                /* Scan the Filter list in the ClassInfo */
                TMO_SLL_Scan (&(pClsInfoNode->SllFltInfo), pFltInfoNode,
                              tQoSFilterInfoNode *)
                {

                    i4RetStatus = QoSHwWrMapClassToIntPriority (pFltInfoNode->
                                                                pClsMapNode,
                                                                pCls2PriNode->
                                                                u1RegenPri,
                                                                QOS_NP_DEL);
                    if (i4RetStatus == QOS_FAILURE)
                    {
                        QOS_TRC_ARG1 (MGMT_TRC,
                                      "In %s :  QoSHwWrMapClassToIntPriority "
                                      " () Returns FAILURE. \r\n",
                                      __FUNCTION__);

                        i4ReturnValue = SNMP_FAILURE;
                    }
                }
            }
            i4RetStatus = QoSDeleteCls2PriTblEntry (pCls2PriNode);

            if (i4RetStatus == QOS_FAILURE)
            {
                QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSDeleteCls2PriTblEntry ()"
                              " Returns FAILURE. \r\n", __FUNCTION__);

                i4ReturnValue = SNMP_FAILURE;
            }

            break;

            /* Not Supported */
        case CREATE_AND_GO:
            i4ReturnValue = SNMP_FAILURE;
            break;

        default:
            i4ReturnValue = SNMP_FAILURE;
            break;
    }

    return ((INT1) i4ReturnValue);

}

/* Low Level TEST Routines for All Objects  */
/****************************************************************************
 Function    :  nmhTestv2FsQoSClassToPriorityRegenPri
 Input       :  The Indices
                FsQoSClassToPriorityCLASS

                The Object 
                testValFsQoSClassToPriorityRegenPri
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsQoSClassToPriorityRegenPri (UINT4 *pu4ErrorCode,
                                       UINT4 u4FsQoSClassToPriorityCLASS,
                                       UINT4
                                       u4TestValFsQoSClassToPriorityRegenPri)
{
    INT4                i4RetStatus = QOS_FAILURE;

    *pu4ErrorCode = SNMP_ERR_NO_ERROR;

    /* Check Index */
    i4RetStatus = QoSUtlValidateCls2PriMapTblIdxInst
        (u4FsQoSClassToPriorityCLASS, pu4ErrorCode);

    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlValidateCls2PriMapTblIdxInst "
                      " () Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    /* Check  Object's Value */
    if (u4TestValFsQoSClassToPriorityRegenPri > QOS_CLS2PRI_PRIORITY_MAX_VAL)
    {
        QOS_TRC_ARG3 (MGMT_TRC, "%s : Regen Priority value is out of range."
                      " The range should be Min %d - Max %d. \r\n",
                      __FUNCTION__, QOS_CLS2PRI_PRIORITY_MIN_VAL,
                      QOS_CLS2PRI_PRIORITY_MAX_VAL);

        CLI_SET_ERR (QOS_CLI_ERR_INVALID_REGEN_PRI);

        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;

        return (SNMP_FAILURE);
    }

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhTestv2FsQoSClassToPriorityGroupName
 Input       :  The Indices
                FsQoSClassToPriorityCLASS

                The Object 
                testValFsQoSClassToPriorityGroupName
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsQoSClassToPriorityGroupName (UINT4 *pu4ErrorCode,
                                        UINT4 u4FsQoSClassToPriorityCLASS,
                                        tSNMP_OCTET_STRING_TYPE *
                                        pTestValFsQoSClassToPriorityGroupName)
{
    INT4                i4RetStatus = QOS_FAILURE;

    *pu4ErrorCode = SNMP_ERR_NO_ERROR;

    /* Check Index */
    i4RetStatus = QoSUtlValidateCls2PriMapTblIdxInst
        (u4FsQoSClassToPriorityCLASS, pu4ErrorCode);

    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlValidateCls2PriMapTblIdxInst "
                      " () Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    /* Check  Object's Value */
    i4RetStatus =
        QoSUtlValidateTableName (pTestValFsQoSClassToPriorityGroupName,
                                 pu4ErrorCode);

    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlValidateTableName () "
                      " Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhTestv2FsQoSClassToPriorityStatus
 Input       :  The Indices
                FsQoSClassToPriorityCLASS

                The Object 
                testValFsQoSClassToPriorityStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsQoSClassToPriorityStatus (UINT4 *pu4ErrorCode,
                                     UINT4 u4FsQoSClassToPriorityCLASS,
                                     INT4 i4TestValFsQoSClassToPriorityStatus)
{
    tQoSClass2PriorityNode *pCls2PriNode = NULL;
    INT4                i4RetStatus = QOS_FAILURE;
    UINT4               u4Count = 0;
    INT4                i4ReturnValue = SNMP_SUCCESS;
    tQoSClassInfoNode  *pClsInfoNode = NULL;

    if (gQoSGlobalInfo.eSysControl != QOS_SYS_CNTL_START)
    {
        QOS_TRC (MGMT_TRC, QOS_MODULE_SHUTDOW_ERROR_MSG);

        CLI_SET_ERR (QOS_CLI_ERR_MODULE_SHUTDOW);

        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return (SNMP_FAILURE);
    }

    *pu4ErrorCode = SNMP_ERR_NO_ERROR;

    /* Check the Range of Index Values (Min-Max) */
    i4RetStatus =
        QOS_CHECK_CLS2PRI_MAP_TBL_INDEX_RANGE (u4FsQoSClassToPriorityCLASS);

    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG3 (MGMT_TRC, "%s : Class 2 Priority Map Id %d is out of "
                      "Range. The Range Should be (1-%d). \r\n", __FUNCTION__,
                      u4FsQoSClassToPriorityCLASS,
                      QOS_CLS2PRI_MAP_TBL_MAX_INDEX_RANGE);

        CLI_SET_ERR (QOS_CLI_ERR_CLS_MAP_INVALID_CLS);

        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;

        return (SNMP_FAILURE);
    }

    pCls2PriNode = QoSUtlGetClass2PriorityNode (u4FsQoSClassToPriorityCLASS);

    if (pCls2PriNode == NULL)
    {
        /* Entry Not Found In The Table */
        switch (i4TestValFsQoSClassToPriorityStatus)
        {
            case CREATE_AND_GO:
            case ACTIVE:
            case NOT_IN_SERVICE:
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                i4ReturnValue = SNMP_FAILURE;
                break;

            case CREATE_AND_WAIT:
                /* Check the No of Entries in the Table */
                i4RetStatus =
                    RBTreeCount (gQoSGlobalInfo.pRbCls2PriMapTbl, &u4Count);

                if ((i4RetStatus == RB_FAILURE) ||
                    (u4Count > QOS_MAX_NUM_OF_CLASSES))
                {
                    QOS_TRC_ARG2 (MGMT_TRC, "%s : Max No of Cls2Pri Map Table"
                                  " Entries (CLASSES) are Configured. No of"
                                  " Max CLASSES %d. \r\n", __FUNCTION__,
                                  QOS_MAX_NUM_OF_CLASSES);

                    CLI_SET_ERR (QOS_CLI_ERR_CLS2PRI_MAP_MAX_ENTRY);

                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;

                    return (SNMP_FAILURE);
                }
                break;

            case DESTROY:
                break;

            default:
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                i4ReturnValue = SNMP_FAILURE;
                break;
        }
    }
    else
    {
        /* Entry Found In The Table */
        switch (i4TestValFsQoSClassToPriorityStatus)
        {
            case ACTIVE:
            case NOT_IN_SERVICE:
                if ((pCls2PriNode->u1Status == NOT_READY))
                {
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                    return (SNMP_FAILURE);
                }

                break;

            case DESTROY:
                break;

            case CREATE_AND_WAIT:
            case CREATE_AND_GO:
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                i4ReturnValue = SNMP_FAILURE;
                break;

            default:
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                i4ReturnValue = SNMP_FAILURE;
                break;

        }                        /* End of switch */

    }                            /* End of if */

    pClsInfoNode = QoSUtlGetClassInfoNode (u4FsQoSClassToPriorityCLASS);

    /*If the class is mapped with any policy map,then return failure */
    if ((pClsInfoNode != NULL) && (pClsInfoNode->pPlyMapNode != NULL))
    {
        if (pClsInfoNode->pPlyMapNode->u4ClassId != QOS_CM_TBL_DEF_CLASS)
        {
            CLI_SET_ERR (QOS_CLI_ERR_CLS_MAP_RFE);
            i4ReturnValue = SNMP_FAILURE;
        }
    }

    return ((INT1) i4ReturnValue);
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsQoSClassToPriorityTable
 Input       :  The Indices
                FsQoSClassToPriorityCLASS
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsQoSClassToPriorityTable (UINT4 *pu4ErrorCode,
                                   tSnmpIndexList * pSnmpIndexList,
                                   tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsQoSMeterTable. */
/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsQoSMeterTable
 Input       :  The Indices
                FsQoSMeterId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsQoSMeterTable (UINT4 u4FsQoSMeterId)
{
    INT4                i4RetStatus = 0;
    tQoSMeterNode      *pMeterNode = NULL;

    if (gQoSGlobalInfo.eSysControl != QOS_SYS_CNTL_START)
    {
        QOS_TRC (MGMT_TRC, QOS_MODULE_SHUTDOW_ERROR_MSG);
        return (SNMP_FAILURE);
    }

    /* Check the Range of Index Values (Min-Max) */
    i4RetStatus = QOS_CHECK_METER_TBL_INDEX_RANGE (u4FsQoSMeterId);

    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG3 (MGMT_TRC, "%s : Meter Id %d is out of Range."
                      " The Range Should be (1-%d). \r\n", __FUNCTION__,
                      u4FsQoSMeterId, QOS_METER_TBL_MAX_INDEX_RANGE);

        return (SNMP_FAILURE);
    }

    pMeterNode = QoSUtlGetMeterNode (u4FsQoSMeterId);

    if (pMeterNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetMeterNode () "
                      " Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsQoSMeterTable
 Input       :  The Indices
                FsQoSMeterId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsQoSMeterTable (UINT4 *pu4FsQoSMeterId)
{
    INT4                i4RetStatus = QOS_FAILURE;

    if (gQoSGlobalInfo.eSysControl != QOS_SYS_CNTL_START)
    {
        QOS_TRC (MGMT_TRC, QOS_MODULE_SHUTDOW_ERROR_MSG);
        return (SNMP_FAILURE);
    }

    i4RetStatus = QoSGetNextMeterTblEntryIndex (0, pu4FsQoSMeterId);

    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSGetNextMeterTblEntryIndex () "
                      " Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsQoSMeterTable
 Input       :  The Indices
                FsQoSMeterId
                nextFsQoSMeterId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsQoSMeterTable (UINT4 u4FsQoSMeterId,
                                UINT4 *pu4NextFsQoSMeterId)
{
    INT4                i4RetStatus = QOS_FAILURE;

    if (gQoSGlobalInfo.eSysControl != QOS_SYS_CNTL_START)
    {
        QOS_TRC (MGMT_TRC, QOS_MODULE_SHUTDOW_ERROR_MSG);
        return (SNMP_FAILURE);
    }

    i4RetStatus = QoSGetNextMeterTblEntryIndex (u4FsQoSMeterId,
                                                pu4NextFsQoSMeterId);
    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSGetNextMeterTblEntryIndex () "
                      " Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    return (SNMP_SUCCESS);

}

/* Low Level GET Routine for All Objects  */
/****************************************************************************
 Function    :  nmhGetFsQoSMeterName
 Input       :  The Indices
                FsQoSMeterId

                The Object 
                retValFsQoSMeterName
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsQoSMeterName (UINT4 u4FsQoSMeterId,
                      tSNMP_OCTET_STRING_TYPE * pRetValFsQoSMeterName)
{
    tQoSMeterNode      *pMeterNode = NULL;
    UINT1              *pu1Str = NULL;

    pMeterNode = QoSUtlGetMeterNode (u4FsQoSMeterId);

    if (pMeterNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetMeterNode () "
                      " Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    pu1Str = &(pMeterNode->au1Name[0]);
    QOS_UTL_COPY_TABLE_NAME (pRetValFsQoSMeterName, pu1Str);

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFsQoSMeterType
 Input       :  The Indices
                FsQoSMeterId

                The Object 
                retValFsQoSMeterType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsQoSMeterType (UINT4 u4FsQoSMeterId, INT4 *pi4RetValFsQoSMeterType)
{
    tQoSMeterNode      *pMeterNode = NULL;

    pMeterNode = QoSUtlGetMeterNode (u4FsQoSMeterId);

    if (pMeterNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetMeterNode () "
                      " Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    *pi4RetValFsQoSMeterType = (INT4) pMeterNode->u1Type;

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFsQoSMeterInterval
 Input       :  The Indices
                FsQoSMeterId

                The Object 
                retValFsQoSMeterInterval
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsQoSMeterInterval (UINT4 u4FsQoSMeterId,
                          UINT4 *pu4RetValFsQoSMeterInterval)
{
    tQoSMeterNode      *pMeterNode = NULL;

    pMeterNode = QoSUtlGetMeterNode (u4FsQoSMeterId);

    if (pMeterNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetMeterNode () "
                      " Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    *pu4RetValFsQoSMeterInterval = pMeterNode->u4Interval;

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFsQoSMeterColorMode
 Input       :  The Indices
                FsQoSMeterId

                The Object 
                retValFsQoSMeterColorMode
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsQoSMeterColorMode (UINT4 u4FsQoSMeterId,
                           INT4 *pi4RetValFsQoSMeterColorMode)
{
    tQoSMeterNode      *pMeterNode = NULL;

    pMeterNode = QoSUtlGetMeterNode (u4FsQoSMeterId);

    if (pMeterNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetMeterNode () "
                      " Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    *pi4RetValFsQoSMeterColorMode = (INT4) pMeterNode->u1ColorMode;

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFsQoSMeterCIR
 Input       :  The Indices
                FsQoSMeterId

                The Object 
                retValFsQoSMeterCIR
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsQoSMeterCIR (UINT4 u4FsQoSMeterId, UINT4 *pu4RetValFsQoSMeterCIR)
{
    tQoSMeterNode      *pMeterNode = NULL;

    pMeterNode = QoSUtlGetMeterNode (u4FsQoSMeterId);

    if (pMeterNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetMeterNode () "
                      " Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    *pu4RetValFsQoSMeterCIR = pMeterNode->u4CIR;

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFsQoSMeterCBS
 Input       :  The Indices
                FsQoSMeterId

                The Object 
                retValFsQoSMeterCBS
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsQoSMeterCBS (UINT4 u4FsQoSMeterId, UINT4 *pu4RetValFsQoSMeterCBS)
{
    tQoSMeterNode      *pMeterNode = NULL;

    pMeterNode = QoSUtlGetMeterNode (u4FsQoSMeterId);

    if (pMeterNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetMeterNode () "
                      " Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    *pu4RetValFsQoSMeterCBS = pMeterNode->u4CBS;

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFsQoSMeterEIR
 Input       :  The Indices
                FsQoSMeterId

                The Object 
                retValFsQoSMeterEIR
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsQoSMeterEIR (UINT4 u4FsQoSMeterId, UINT4 *pu4RetValFsQoSMeterEIR)
{
    tQoSMeterNode      *pMeterNode = NULL;

    pMeterNode = QoSUtlGetMeterNode (u4FsQoSMeterId);

    if (pMeterNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetMeterNode () "
                      " Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    *pu4RetValFsQoSMeterEIR = pMeterNode->u4EIR;

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFsQoSMeterEBS
 Input       :  The Indices
                FsQoSMeterId

                The Object 
                retValFsQoSMeterEBS
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsQoSMeterEBS (UINT4 u4FsQoSMeterId, UINT4 *pu4RetValFsQoSMeterEBS)
{
    tQoSMeterNode      *pMeterNode = NULL;

    pMeterNode = QoSUtlGetMeterNode (u4FsQoSMeterId);

    if (pMeterNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetMeterNode () "
                      " Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    *pu4RetValFsQoSMeterEBS = pMeterNode->u4EBS;

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFsQoSMeterNext
 Input       :  The Indices
                FsQoSMeterId

                The Object 
                retValFsQoSMeterNext
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsQoSMeterNext (UINT4 u4FsQoSMeterId, UINT4 *pu4RetValFsQoSMeterNext)
{
    tQoSMeterNode      *pMeterNode = NULL;

    pMeterNode = QoSUtlGetMeterNode (u4FsQoSMeterId);

    if (pMeterNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetMeterNode () "
                      " Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    *pu4RetValFsQoSMeterNext = pMeterNode->u4Next;

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFsQoSMeterStatus
 Input       :  The Indices
                FsQoSMeterId

                The Object 
                retValFsQoSMeterStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsQoSMeterStatus (UINT4 u4FsQoSMeterId, INT4 *pi4RetValFsQoSMeterStatus)
{
    tQoSMeterNode      *pMeterNode = NULL;

    pMeterNode = QoSUtlGetMeterNode (u4FsQoSMeterId);

    if (pMeterNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetMeterNode () "
                      " Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    *pi4RetValFsQoSMeterStatus = (INT4) pMeterNode->u1Status;

    return (SNMP_SUCCESS);
}

/* Low Level SET Routine for All Objects  */
/****************************************************************************
 Function    :  nmhSetFsQoSMeterName
 Input       :  The Indices
                FsQoSMeterId

                The Object 
                setValFsQoSMeterName
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsQoSMeterName (UINT4 u4FsQoSMeterId,
                      tSNMP_OCTET_STRING_TYPE * pSetValFsQoSMeterName)
{
    tQoSMeterNode      *pMeterNode = NULL;
    UINT1              *pu1Str = NULL;

    if ((pSetValFsQoSMeterName == NULL) ||
        (pSetValFsQoSMeterName->pu1_OctetList == NULL))
    {
        return (SNMP_FAILURE);
    }

    pMeterNode = QoSUtlGetMeterNode (u4FsQoSMeterId);

    if (pMeterNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetMeterNode () "
                      " Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    pu1Str = &(pMeterNode->au1Name[0]);
    QOS_UTL_SET_TABLE_NAME (pu1Str, pSetValFsQoSMeterName);

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhSetFsQoSMeterType
 Input       :  The Indices
                FsQoSMeterId

                The Object 
                setValFsQoSMeterType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsQoSMeterType (UINT4 u4FsQoSMeterId, INT4 i4SetValFsQoSMeterType)
{
    tQoSMeterNode      *pMeterNode = NULL;

    pMeterNode = QoSUtlGetMeterNode (u4FsQoSMeterId);

    if (pMeterNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetMeterNode () "
                      " Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    pMeterNode->u1Type = (UINT1) i4SetValFsQoSMeterType;

    QoSValidateMeterTblEntry (pMeterNode);

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhSetFsQoSMeterInterval
 Input       :  The Indices
                FsQoSMeterId

                The Object 
                setValFsQoSMeterInterval
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsQoSMeterInterval (UINT4 u4FsQoSMeterId,
                          UINT4 u4SetValFsQoSMeterInterval)
{
    tQoSMeterNode      *pMeterNode = NULL;

    pMeterNode = QoSUtlGetMeterNode (u4FsQoSMeterId);

    if (pMeterNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetMeterNode () "
                      " Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    pMeterNode->u4Interval = u4SetValFsQoSMeterInterval;

    QoSValidateMeterTblEntry (pMeterNode);

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhSetFsQoSMeterColorMode
 Input       :  The Indices
                FsQoSMeterId

                The Object 
                setValFsQoSMeterColorMode
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsQoSMeterColorMode (UINT4 u4FsQoSMeterId,
                           INT4 i4SetValFsQoSMeterColorMode)
{
    tQoSMeterNode      *pMeterNode = NULL;

    pMeterNode = QoSUtlGetMeterNode (u4FsQoSMeterId);

    if (pMeterNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetMeterNode () "
                      " Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    pMeterNode->u1ColorMode = (UINT1) i4SetValFsQoSMeterColorMode;

    QoSValidateMeterTblEntry (pMeterNode);

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhSetFsQoSMeterCIR
 Input       :  The Indices
                FsQoSMeterId

                The Object 
                setValFsQoSMeterCIR
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsQoSMeterCIR (UINT4 u4FsQoSMeterId, UINT4 u4SetValFsQoSMeterCIR)
{
    tQoSMeterNode      *pMeterNode = NULL;

    pMeterNode = QoSUtlGetMeterNode (u4FsQoSMeterId);

    if (pMeterNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetMeterNode () "
                      " Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    pMeterNode->u4CIR = u4SetValFsQoSMeterCIR;

    QoSValidateMeterTblEntry (pMeterNode);

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhSetFsQoSMeterCBS
 Input       :  The Indices
                FsQoSMeterId

                The Object 
                setValFsQoSMeterCBS
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsQoSMeterCBS (UINT4 u4FsQoSMeterId, UINT4 u4SetValFsQoSMeterCBS)
{
    tQoSMeterNode      *pMeterNode = NULL;

    pMeterNode = QoSUtlGetMeterNode (u4FsQoSMeterId);

    if (pMeterNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetMeterNode () "
                      " Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    pMeterNode->u4CBS = u4SetValFsQoSMeterCBS;

    QoSValidateMeterTblEntry (pMeterNode);

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhSetFsQoSMeterEIR
 Input       :  The Indices
                FsQoSMeterId

                The Object 
                setValFsQoSMeterEIR
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsQoSMeterEIR (UINT4 u4FsQoSMeterId, UINT4 u4SetValFsQoSMeterEIR)
{
    tQoSMeterNode      *pMeterNode = NULL;

    pMeterNode = QoSUtlGetMeterNode (u4FsQoSMeterId);

    if (pMeterNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetMeterNode () "
                      " Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    pMeterNode->u4EIR = u4SetValFsQoSMeterEIR;

    QoSValidateMeterTblEntry (pMeterNode);

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhSetFsQoSMeterEBS
 Input       :  The Indices
                FsQoSMeterId

                The Object 
                setValFsQoSMeterEBS
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsQoSMeterEBS (UINT4 u4FsQoSMeterId, UINT4 u4SetValFsQoSMeterEBS)
{
    tQoSMeterNode      *pMeterNode = NULL;

    pMeterNode = QoSUtlGetMeterNode (u4FsQoSMeterId);

    if (pMeterNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetMeterNode () "
                      " Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    pMeterNode->u4EBS = u4SetValFsQoSMeterEBS;

    QoSValidateMeterTblEntry (pMeterNode);

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhSetFsQoSMeterNext
 Input       :  The Indices
                FsQoSMeterId

                The Object 
                setValFsQoSMeterNext
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsQoSMeterNext (UINT4 u4FsQoSMeterId, UINT4 u4SetValFsQoSMeterNext)
{
    tQoSMeterNode      *pMeterNode = NULL;
    tQoSMeterNode      *pTmpMeterNode = NULL;
    tQoSMeterNode      *pNextMeterNode = NULL;
    UINT4               u4TmpMeterId = 0;

    pMeterNode = QoSUtlGetMeterNode (u4FsQoSMeterId);

    if (pMeterNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetMeterNode () "
                      " Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    /* If the Same value is configured again no need to incrment the 
     * RefCount and Set */
    if (pMeterNode->u4Next == u4SetValFsQoSMeterNext)
    {
        return (SNMP_SUCCESS);
    }

    /* Store the old value ,Reset corresponding ref count if the id not '0' */
    u4TmpMeterId = pMeterNode->u4Next;
    if (u4TmpMeterId != 0)
    {
        pTmpMeterNode = QoSUtlGetMeterNode (u4TmpMeterId);
        if (pTmpMeterNode == NULL)
        {
            QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetMeterNode () "
                          "Returns FAILURE. \r\n", __FUNCTION__);

            return (SNMP_FAILURE);
        }

        pTmpMeterNode->u4RefCount = (pTmpMeterNode->u4RefCount) - 1;
    }

    if (u4SetValFsQoSMeterNext != 0)
    {
        pNextMeterNode = QoSUtlGetMeterNode (u4SetValFsQoSMeterNext);

        if (pNextMeterNode == NULL)
        {
            QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetMeterNode () "
                          "Returns FAILURE. \r\n", __FUNCTION__);

            return (SNMP_FAILURE);
        }

        /* Incremnet the RefCount of Meter Id  Entry in the Meter Table */
        pNextMeterNode->u4RefCount = (pNextMeterNode->u4RefCount) + 1;
    }

    pMeterNode->u4Next = u4SetValFsQoSMeterNext;

    QoSValidateMeterTblEntry (pMeterNode);

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhSetFsQoSMeterStatus
 Input       :  The Indices
                FsQoSMeterId

                The Object 
                setValFsQoSMeterStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsQoSMeterStatus (UINT4 u4FsQoSMeterId, INT4 i4SetValFsQoSMeterStatus)
{
    tQoSMeterNode      *pMeterNode = NULL;
    INT4                i4RetStatus = QOS_FAILURE;
    INT4                i4RowStatus = QOS_FAILURE;
    INT4                i4ReturnValue = SNMP_SUCCESS;

    pMeterNode = QoSUtlGetMeterNode (u4FsQoSMeterId);

    if (pMeterNode != NULL)
    {
        i4RowStatus = (UINT4) pMeterNode->u1Status;

        /* Entry found in the Table */
        if (i4RowStatus == i4SetValFsQoSMeterStatus)
        {
            /* same value */
            return (SNMP_SUCCESS);
        }
    }
    else
    {
        /* New Entry need to be created in the Table, only if 
         * i4TestValFsQoSMeterStatus = CREATE_AND_WAIT */
        /* Optional */
        if (i4SetValFsQoSMeterStatus == DESTROY)
        {
            return (SNMP_SUCCESS);
        }
        else if (i4SetValFsQoSMeterStatus != QOS_CREATE_AND_WAIT)
        {
            return (SNMP_FAILURE);
        }
    }

    switch (i4SetValFsQoSMeterStatus)
    {
            /* For a New Entry */
        case CREATE_AND_WAIT:

            pMeterNode = QoSCreateMeterTblEntry (u4FsQoSMeterId);

            if (pMeterNode == NULL)
            {
                QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSCreateMeterTblEntry () "
                              " Returns FAILURE. \r\n", __FUNCTION__);

                return (SNMP_FAILURE);
            }
            pMeterNode->u1Status = NOT_READY;

            break;

            /* Modify an Entry */
        case ACTIVE:

            if (gQoSGlobalInfo.eSysStatus == QOS_SYS_STATUS_ENABLE)
            {
                /* 1. Add the entry into the HARDWARE. */
                i4RetStatus = QoSHwWrMeterCreate (pMeterNode);

                if (i4RetStatus == QOS_FAILURE)
                {
                    QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSHwWrMeterCreate () "
                                  " Returns FAILURE. \r\n", __FUNCTION__);

                    return (SNMP_FAILURE);
                }
            }
            pMeterNode->u1Status = ACTIVE;

            break;

        case NOT_IN_SERVICE:

            if ((pMeterNode->u1Status == ACTIVE) &&
                (gQoSGlobalInfo.eSysStatus == QOS_SYS_STATUS_ENABLE))
            {
                /* 1. Remove the entry from the HARDWARE. */
                i4RetStatus = QoSHwWrMeterDelete (pMeterNode);

                if (i4RetStatus == QOS_FAILURE)
                {
                    QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSHwWrMeterDelete () "
                                  " Returns FAILURE. \r\n", __FUNCTION__);

                    return (SNMP_FAILURE);
                }
            }

            pMeterNode->u1Status = NOT_IN_SERVICE;

            break;

        case DESTROY:

            if ((pMeterNode->u1Status == ACTIVE) &&
                (gQoSGlobalInfo.eSysStatus == QOS_SYS_STATUS_ENABLE))
            {
                /* 1. Remove the entry from the HARDWARE. */
                i4RetStatus = QoSHwWrMeterDelete (pMeterNode);

                if (i4RetStatus == QOS_FAILURE)
                {
                    QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSHwWrMeterDelete () "
                                  " Returns FAILURE. \r\n", __FUNCTION__);

                    return (SNMP_FAILURE);
                }
            }

            /* 2. Remove the entry from the SOFTWARE. */
            i4RetStatus = QoSDeleteMeterTblEntry (pMeterNode);

            if (i4RetStatus == QOS_FAILURE)
            {
                QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSDeleteMeterTblEntry () "
                              " Returns FAILURE. \r\n", __FUNCTION__);

                return (SNMP_FAILURE);
            }

            break;

            /* Not Supported */
        case CREATE_AND_GO:
            i4ReturnValue = SNMP_FAILURE;
            break;

        default:
            i4ReturnValue = SNMP_FAILURE;
            break;
    }
    if (i4ReturnValue == SNMP_SUCCESS)
    {
        QosUpdateDiffServTBParamNextFree (u4FsQoSMeterId,
                                          i4SetValFsQoSMeterStatus);
    }

    return ((INT1) i4ReturnValue);

}

/* Low Level TEST Routines for All Objects  */
/****************************************************************************
 Function    :  nmhTestv2FsQoSMeterName
 Input       :  The Indices
                FsQoSMeterId

                The Object 
                testValFsQoSMeterName
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsQoSMeterName (UINT4 *pu4ErrorCode,
                         UINT4 u4FsQoSMeterId,
                         tSNMP_OCTET_STRING_TYPE * pTestValFsQoSMeterName)
{
    tQoSMeterNode      *pMeterNode = NULL;
    INT4                i4RetStatus = QOS_FAILURE;

    *pu4ErrorCode = SNMP_ERR_NO_ERROR;

    /* Check Index */
    i4RetStatus = QoSUtlValidateMeterTblIdxInst (u4FsQoSMeterId, pu4ErrorCode);
    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlValidateMeterTblIdxInst () "
                      " Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    /* Check  Object's Value */
    i4RetStatus = QoSUtlValidateTableName (pTestValFsQoSMeterName,
                                           pu4ErrorCode);
    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlValidateTableName () "
                      " Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    pMeterNode = QoSUtlGetMeterNode (u4FsQoSMeterId);
    if (pMeterNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetMeterNode () Returns "
                      "FAILURE. \r\n", __FUNCTION__);

        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (QOS_CLI_ERR_METER_NO_ENTRY);
        return (SNMP_FAILURE);
    }

    /* Check the Name is same as Current Name */
    if ((STRLEN (pMeterNode->au1Name)) == (UINT4)
        pTestValFsQoSMeterName->i4_Length)
    {
        if ((STRNCMP (pMeterNode->au1Name,
                      pTestValFsQoSMeterName->pu1_OctetList,
                      pTestValFsQoSMeterName->i4_Length)) == 0)
        {
            return (SNMP_SUCCESS);
        }
    }

    /* Check the name is Unique or Not */
    i4RetStatus = QoSUtlIsUniqueName (pTestValFsQoSMeterName,
                                      QOS_TBL_TYPE_METER);
    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlIsUniqueName () Returns "
                      "FAILURE. \r\n", __FUNCTION__);

        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return (SNMP_FAILURE);
    }

    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhTestv2FsQoSMeterType
 Input       :  The Indices
                FsQoSMeterId

                The Object 
                testValFsQoSMeterType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsQoSMeterType (UINT4 *pu4ErrorCode, UINT4 u4FsQoSMeterId,
                         INT4 i4TestValFsQoSMeterType)
{
    INT4                i4RetStatus = QOS_FAILURE;

    *pu4ErrorCode = SNMP_ERR_NO_ERROR;

    /* Check Index */
    i4RetStatus = QoSUtlValidateMeterTblIdxInst (u4FsQoSMeterId, pu4ErrorCode);
    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlValidateMeterTblIdxInst () "
                      " Returns FAILURE. \r\n", __FUNCTION__);
        return (SNMP_FAILURE);
    }

    /* Check  Object's Value */
    if ((i4TestValFsQoSMeterType < QOS_METER_TYPE_MIN_VAL) ||
        (i4TestValFsQoSMeterType > QOS_METER_TYPE_MAX_VAL))
    {
        QOS_TRC_ARG3 (MGMT_TRC, "%s :Meter Type value is out of range. The "
                      "range should be Min %d - Max %d. \r\n", __FUNCTION__,
                      QOS_METER_TYPE_MIN_VAL, QOS_METER_TYPE_MAX_VAL);

        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (QOS_CLI_ERR_METER_INVALID_TYPE);

        return (SNMP_FAILURE);
    }

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhTestv2FsQoSMeterInterval
 Input       :  The Indices
                FsQoSMeterId

                The Object 
                testValFsQoSMeterInterval
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsQoSMeterInterval (UINT4 *pu4ErrorCode, UINT4 u4FsQoSMeterId,
                             UINT4 u4TestValFsQoSMeterInterval)
{
    INT4                i4RetStatus = QOS_FAILURE;

    *pu4ErrorCode = SNMP_ERR_NO_ERROR;

    /* Check Index */
    i4RetStatus = QoSUtlValidateMeterTblIdxInst (u4FsQoSMeterId, pu4ErrorCode);
    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlValidateMeterTblIdxInst () "
                      " Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }
    /* Reset Interval  to '0' */
    if (u4TestValFsQoSMeterInterval == 0)
    {
        return (SNMP_SUCCESS);
    }

    /* Check  Object's Value */
    if (u4TestValFsQoSMeterInterval > QOS_METER_INTERVAL_MAX_VAL)
    {
        QOS_TRC_ARG3 (MGMT_TRC, "%s : Meter Interval value is out of range. The"
                      " range should be Min %d - Max %d. \r\n", __FUNCTION__,
                      QOS_METER_INTERVAL_MIN_VAL, QOS_METER_INTERVAL_MAX_VAL);

        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (QOS_CLI_ERR_METER_INVALID_INTERVAL);

        return (SNMP_FAILURE);
    }

    i4RetStatus = QoSValidateMeterTypeParams (u4FsQoSMeterId,
                                              QOS_METER_PARAM_TYPE_INTERVAL,
                                              pu4ErrorCode);
    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSValidateMeterTypeParams () "
                      " Returns FAILURE. \r\n", __FUNCTION__);
        return (SNMP_FAILURE);
    }

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhTestv2FsQoSMeterColorMode
 Input       :  The Indices
                FsQoSMeterId

                The Object 
                testValFsQoSMeterColorMode
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsQoSMeterColorMode (UINT4 *pu4ErrorCode, UINT4 u4FsQoSMeterId,
                              INT4 i4TestValFsQoSMeterColorMode)
{
    INT4                i4RetStatus = QOS_FAILURE;

    *pu4ErrorCode = SNMP_ERR_NO_ERROR;

    /* Check Index */
    i4RetStatus = QoSUtlValidateMeterTblIdxInst (u4FsQoSMeterId, pu4ErrorCode);
    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlValidateMeterTblIdxInst () "
                      " Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    /* Check  Object's Value */
    if ((i4TestValFsQoSMeterColorMode < QOS_METER_COLOR_MIN_VAL) ||
        (i4TestValFsQoSMeterColorMode > QOS_METER_COLOR_MAX_VAL))
    {
        QOS_TRC_ARG3 (MGMT_TRC, "%s : Meter Color value is out of range. The"
                      " range should be Min %d - Max %d. \r\n", __FUNCTION__,
                      QOS_METER_COLOR_MIN_VAL, QOS_METER_COLOR_MAX_VAL);

        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (QOS_CLI_ERR_METER_INVALID_COLOR_MODE);

        return (SNMP_FAILURE);
    }

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhTestv2FsQoSMeterCIR
 Input       :  The Indices
                FsQoSMeterId

                The Object 
                testValFsQoSMeterCIR
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsQoSMeterCIR (UINT4 *pu4ErrorCode, UINT4 u4FsQoSMeterId,
                        UINT4 u4TestValFsQoSMeterCIR)
{
    INT4                i4RetStatus = QOS_FAILURE;

    tQoSMeterNode      *pMeterNode = NULL;

    *pu4ErrorCode = SNMP_ERR_NO_ERROR;

    /* Check Index */
    i4RetStatus = QoSUtlValidateMeterTblIdxInst (u4FsQoSMeterId, pu4ErrorCode);
    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlValidateMeterTblIdxInst () "
                      " Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    /* Reset CIR Value to '0' */
    if (u4TestValFsQoSMeterCIR == 0)
    {
        return (SNMP_SUCCESS);
    }

    /* Check  Object's Value */
    if (((INT4) u4TestValFsQoSMeterCIR < QOS_METER_CIR_MIN_VAL) ||
        (u4TestValFsQoSMeterCIR > QOS_METER_CIR_MAX_VAL))
    {
        QOS_TRC_ARG3 (MGMT_TRC, "%s : Meter CIR value is out of range. The"
                      " range should be Min %d - Max %d. \r\n", __FUNCTION__,
                      QOS_METER_CIR_MIN_VAL, QOS_METER_CIR_MAX_VAL);

        CLI_SET_ERR (QOS_CLI_ERR_INVALID_CIR);

        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }

    i4RetStatus = QoSValidateMeterTypeParams (u4FsQoSMeterId,
                                              QOS_METER_PARAM_TYPE_CIR,
                                              pu4ErrorCode);
    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSValidateMeterTypeParams () "
                      " Returns FAILURE. \r\n", __FUNCTION__);
        return (SNMP_FAILURE);
    }

    pMeterNode = QoSUtlGetMeterNode (u4FsQoSMeterId);

    if (pMeterNode == NULL)
    {
        CLI_SET_ERR (QOS_CLI_ERR_METER_NO_ENTRY);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (SNMP_FAILURE);
    }

    if (pMeterNode->u4EIR > 0)
    {
        if (u4TestValFsQoSMeterCIR > pMeterNode->u4EIR)
        {
            CLI_SET_ERR (QOS_CLI_ERR_METER_CIR_LESS_THAN_EIR);

            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            /*CIR should be less than or equal to EIR */
            return (SNMP_FAILURE);
        }
    }

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhTestv2FsQoSMeterCBS
 Input       :  The Indices
                FsQoSMeterId

                The Object 
                testValFsQoSMeterCBS
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsQoSMeterCBS (UINT4 *pu4ErrorCode, UINT4 u4FsQoSMeterId,
                        UINT4 u4TestValFsQoSMeterCBS)
{
    INT4                i4RetStatus = QOS_FAILURE;

    *pu4ErrorCode = SNMP_ERR_NO_ERROR;

    /* Check Index */
    i4RetStatus = QoSUtlValidateMeterTblIdxInst (u4FsQoSMeterId, pu4ErrorCode);
    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlValidateMeterTblIdxInst () "
                      " Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }
    /* Reset CBS Value to '0' */
    if (u4TestValFsQoSMeterCBS == 0)
    {
        return (SNMP_SUCCESS);
    }

    /* Check  Object's Value */
    if (((INT4) u4TestValFsQoSMeterCBS < QOS_METER_CBS_MIN_VAL) ||
        (u4TestValFsQoSMeterCBS > QOS_METER_CBS_MAX_VAL))
    {
        QOS_TRC_ARG3 (MGMT_TRC, "%s : Meter CBS value is out of range. The"
                      " range should be Min %d - Max %d. \r\n", __FUNCTION__,
                      QOS_METER_CBS_MIN_VAL, QOS_METER_CBS_MAX_VAL);

        CLI_SET_ERR (QOS_CLI_ERR_INVALID_CBS);

        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }

    i4RetStatus = QoSValidateMeterTypeParams (u4FsQoSMeterId,
                                              QOS_METER_PARAM_TYPE_CBS,
                                              pu4ErrorCode);
    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSValidateMeterTypeParams () "
                      " Returns FAILURE. \r\n", __FUNCTION__);
        return (SNMP_FAILURE);
    }

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhTestv2FsQoSMeterEIR
 Input       :  The Indices
                FsQoSMeterId

                The Object 
                testValFsQoSMeterEIR
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsQoSMeterEIR (UINT4 *pu4ErrorCode, UINT4 u4FsQoSMeterId,
                        UINT4 u4TestValFsQoSMeterEIR)
{
    INT4                i4RetStatus = QOS_FAILURE;
    tQoSMeterNode      *pMeterNode = NULL;

    *pu4ErrorCode = SNMP_ERR_NO_ERROR;

    /* Check Index */
    i4RetStatus = QoSUtlValidateMeterTblIdxInst (u4FsQoSMeterId, pu4ErrorCode);
    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlValidateMeterTblIdxInst () "
                      " Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    /* Reset EIR Value to '0' */
    if (u4TestValFsQoSMeterEIR == 0)
    {
        return (SNMP_SUCCESS);
    }

    /* Check  Object's Value */
    if (((INT4) u4TestValFsQoSMeterEIR < QOS_METER_EIR_MIN_VAL) ||
        (u4TestValFsQoSMeterEIR > QOS_METER_EIR_MAX_VAL))
    {
        QOS_TRC_ARG3 (MGMT_TRC, "%s : Meter EIR value is out of range. The"
                      " range should be Min %d - Max %d. \r\n", __FUNCTION__,
                      QOS_METER_EIR_MIN_VAL, QOS_METER_EIR_MAX_VAL);

        CLI_SET_ERR (QOS_CLI_ERR_INVALID_EIR);

        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;

        return (SNMP_FAILURE);
    }

    i4RetStatus = QoSValidateMeterTypeParams (u4FsQoSMeterId,
                                              QOS_METER_PARAM_TYPE_EIR,
                                              pu4ErrorCode);
    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSValidateMeterTypeParams () "
                      " Returns FAILURE. \r\n", __FUNCTION__);
        return (SNMP_FAILURE);
    }

    pMeterNode = QoSUtlGetMeterNode (u4FsQoSMeterId);

    if (pMeterNode == NULL)
    {
        CLI_SET_ERR (QOS_CLI_ERR_METER_NO_ENTRY);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (SNMP_FAILURE);
    }

    if (u4TestValFsQoSMeterEIR > 0)
    {
        if (pMeterNode->u4CIR > u4TestValFsQoSMeterEIR)
        {
            CLI_SET_ERR (QOS_CLI_ERR_METER_CIR_LESS_THAN_EIR);

            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            /*CIR should be less than or equal to EIR */
            return (SNMP_FAILURE);
        }
    }

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhTestv2FsQoSMeterEBS
 Input       :  The Indices
                FsQoSMeterId

                The Object 
                testValFsQoSMeterEBS
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsQoSMeterEBS (UINT4 *pu4ErrorCode, UINT4 u4FsQoSMeterId,
                        UINT4 u4TestValFsQoSMeterEBS)
{
    INT4                i4RetStatus = QOS_FAILURE;

    *pu4ErrorCode = SNMP_ERR_NO_ERROR;

    /* Check Index */
    i4RetStatus = QoSUtlValidateMeterTblIdxInst (u4FsQoSMeterId, pu4ErrorCode);
    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlValidateMeterTblIdxInst () "
                      " Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    /* Reset EBS Value to '0' */
    if (u4TestValFsQoSMeterEBS == 0)
    {
        return (SNMP_SUCCESS);
    }

    /* Check  Object's Value */
    if (((INT4) u4TestValFsQoSMeterEBS < QOS_METER_EBS_MIN_VAL) ||
        (u4TestValFsQoSMeterEBS > QOS_METER_EBS_MAX_VAL))
    {
        QOS_TRC_ARG3 (MGMT_TRC,
                      "%s : Meter EBS value is out of range. The"
                      " range should be Min %d - Max %d. \r\n", __FUNCTION__,
                      QOS_METER_EBS_MIN_VAL, QOS_METER_EBS_MAX_VAL);

        CLI_SET_ERR (QOS_CLI_ERR_INVALID_EBS);

        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;

        return (SNMP_FAILURE);
    }

    i4RetStatus = QoSValidateMeterTypeParams (u4FsQoSMeterId,
                                              QOS_METER_PARAM_TYPE_EBS,
                                              pu4ErrorCode);
    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSValidateMeterTypeParams () "
                      " Returns FAILURE. \r\n", __FUNCTION__);
        return (SNMP_FAILURE);
    }

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhTestv2FsQoSMeterNext
 Input       :  The Indices
                FsQoSMeterId

                The Object 
                testValFsQoSMeterNext
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsQoSMeterNext (UINT4 *pu4ErrorCode, UINT4 u4FsQoSMeterId,
                         UINT4 u4TestValFsQoSMeterNext)
{
    INT4                i4RetStatus = QOS_FAILURE;

    *pu4ErrorCode = SNMP_ERR_NO_ERROR;

    if (u4TestValFsQoSMeterNext == 0)
    {
        /* For Reset */
        return (SNMP_SUCCESS);
    }

    /* Check Index */
    i4RetStatus = QoSUtlValidateMeterTblIdxInst (u4FsQoSMeterId, pu4ErrorCode);
    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlValidateMeterTblIdxInst () "
                      " Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    /* Check  Object's Value */
    i4RetStatus = QoSUtlValidateMeter (u4TestValFsQoSMeterNext, pu4ErrorCode);
    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlValidateMeter () "
                      " Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhTestv2FsQoSMeterStatus
 Input       :  The Indices
                FsQoSMeterId

                The Object 
                testValFsQoSMeterStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsQoSMeterStatus (UINT4 *pu4ErrorCode, UINT4 u4FsQoSMeterId,
                           INT4 i4TestValFsQoSMeterStatus)
{
    tQoSMeterNode      *pMeterNode = NULL;
    INT4                i4RetStatus = QOS_FAILURE;
    UINT4               u4Count = 0;
    INT4                i4ReturnValue = SNMP_SUCCESS;

    if (gQoSGlobalInfo.eSysControl != QOS_SYS_CNTL_START)
    {
        QOS_TRC (MGMT_TRC, QOS_MODULE_SHUTDOW_ERROR_MSG);
        CLI_SET_ERR (QOS_CLI_ERR_MODULE_SHUTDOW);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return (SNMP_FAILURE);
    }

    *pu4ErrorCode = SNMP_ERR_NO_ERROR;

    /* Check the Range of Index Values (Min-Max) */
    i4RetStatus = QOS_CHECK_METER_TBL_INDEX_RANGE (u4FsQoSMeterId);

    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG3 (MGMT_TRC, "%s : Meter Id %d is out of Range."
                      " The Range Should be (1-%d). \r\n", __FUNCTION__,
                      u4FsQoSMeterId, QOS_METER_TBL_MAX_INDEX_RANGE);

        CLI_SET_ERR (QOS_CLI_ERR_METER_RANGE);

        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;

        return (SNMP_FAILURE);
    }

    pMeterNode = QoSUtlGetMeterNode (u4FsQoSMeterId);

    if (pMeterNode == NULL)
    {
        /* Entry Not Found In The Table */
        switch (i4TestValFsQoSMeterStatus)
        {
            case CREATE_AND_GO:
            case ACTIVE:
            case NOT_IN_SERVICE:
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                i4ReturnValue = SNMP_FAILURE;
                break;

            case CREATE_AND_WAIT:
                /* Check the No of Entries in the Table */
                i4RetStatus =
                    RBTreeCount (gQoSGlobalInfo.pRbMeterTbl, &u4Count);

                if ((i4RetStatus == RB_FAILURE) ||
                    (u4Count > QOS_METER_TBL_MAX_ENTRIES))
                {
                    QOS_TRC_ARG2 (MGMT_TRC, "%s : Max No of Meter Table Entries"
                                  " are Configured. No of Max Entries %d. \r\n",
                                  __FUNCTION__, QOS_METER_TBL_MAX_ENTRIES);

                    CLI_SET_ERR (QOS_CLI_ERR_METER_MAX_ENTRY);

                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;

                    return (SNMP_FAILURE);
                }

                break;

            case DESTROY:
                CLI_SET_ERR (QOS_CLI_ERR_METER_NO_ENTRY);
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                i4ReturnValue = SNMP_FAILURE;

                break;

            default:
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                i4ReturnValue = SNMP_FAILURE;
                break;
        }
    }
    else
    {
        /* Entry Found In The Table */
        switch (i4TestValFsQoSMeterStatus)
        {
            case ACTIVE:

                if ((pMeterNode->u1Status == NOT_READY))
                {
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                    CLI_SET_ERR (QOS_CLI_ERR_METER_MANDATORY);
                    return (SNMP_FAILURE);
                }

                break;

            case NOT_IN_SERVICE:

                if ((pMeterNode->u1Status == NOT_READY))
                {
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                    CLI_SET_ERR (QOS_CLI_ERR_METER_MANDATORY);
                    return (SNMP_FAILURE);
                }

                i4RetStatus = QOS_CHECK_TABLE_REF_COUNT (pMeterNode);

                if (i4RetStatus == QOS_FAILURE)
                {
                    QOS_TRC_ARG1 (MGMT_TRC, "In %s : ERROR - This Entry is "
                                  "referenced by Policy Entry or Meter "
                                  "Entry.\r\n", __FUNCTION__);

                    CLI_SET_ERR (QOS_CLI_ERR_METER_RFE);
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;

                    return (SNMP_FAILURE);
                }
                break;

            case DESTROY:

                i4RetStatus = QOS_CHECK_TABLE_REF_COUNT (pMeterNode);

                if (i4RetStatus == QOS_FAILURE)
                {
                    QOS_TRC_ARG1 (MGMT_TRC, "In %s : ERROR - This Entry is "
                                  "referenced by Policy Entry or Meter "
                                  "Entry.\r\n", __FUNCTION__);

                    CLI_SET_ERR (QOS_CLI_ERR_METER_RFE);
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;

                    return (SNMP_FAILURE);
                }
                break;

            case CREATE_AND_WAIT:
            case CREATE_AND_GO:
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                i4ReturnValue = SNMP_FAILURE;
                break;

            default:
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                i4ReturnValue = SNMP_FAILURE;
                break;

        }                        /* End of switch */

    }                            /* End of if */

    return ((INT1) i4ReturnValue);
}

/* LOW LEVEL Routines for Table : FsQoSPolicyMapTable. */
/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsQoSPolicyMapTable
 Input       :  The Indices
                FsQoSPolicyMapId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsQoSPolicyMapTable (UINT4 u4FsQoSPolicyMapId)
{
    INT4                i4RetStatus = QOS_FAILURE;
    tQoSPolicyMapNode  *pPlyMapNode = NULL;

    if (gQoSGlobalInfo.eSysControl != QOS_SYS_CNTL_START)
    {
        QOS_TRC (MGMT_TRC, QOS_MODULE_SHUTDOW_ERROR_MSG);
        return (SNMP_FAILURE);
    }

    /* Check the Range of Index Values (Min-Max) */
    i4RetStatus = QOS_CHECK_PLY_MAP_TBL_INDEX_RANGE (u4FsQoSPolicyMapId);

    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG3 (MGMT_TRC, "%s : Policy Map Id %d is out of Range."
                      " The Range Should be (1-%d). \r\n", __FUNCTION__,
                      u4FsQoSPolicyMapId, QOS_PLY_MAP_TBL_MAX_INDEX_RANGE);

        return (SNMP_FAILURE);
    }

    pPlyMapNode = QoSUtlGetPolicyMapNode (u4FsQoSPolicyMapId);

    if (pPlyMapNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetPolicyMapNode Returns "
                      "FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    return (SNMP_SUCCESS);
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsQoSMeterTable
 Input       :  The Indices
                FsQoSMeterId
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsQoSMeterTable (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                         tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsQoSPolicyMapTable
 Input       :  The Indices
                FsQoSPolicyMapId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsQoSPolicyMapTable (UINT4 *pu4FsQoSPolicyMapId)
{
    INT4                i4RetStatus = QOS_FAILURE;

    if (gQoSGlobalInfo.eSysControl != QOS_SYS_CNTL_START)
    {
        QOS_TRC (MGMT_TRC, QOS_MODULE_SHUTDOW_ERROR_MSG);
        return (SNMP_FAILURE);
    }

    i4RetStatus = QoSGetNextPlyMapTblEntryIndex (0, pu4FsQoSPolicyMapId);

    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSGetNextPlyMapTblEntryIndex () "
                      "Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhGetNextIndexFsQoSPolicyMapTable
 Input       :  The Indices
                FsQoSPolicyMapId
                nextFsQoSPolicyMapId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsQoSPolicyMapTable (UINT4 u4FsQoSPolicyMapId,
                                    UINT4 *pu4NextFsQoSPolicyMapId)
{
    INT4                i4RetStatus = QOS_FAILURE;

    if (gQoSGlobalInfo.eSysControl != QOS_SYS_CNTL_START)
    {
        QOS_TRC (MGMT_TRC, QOS_MODULE_SHUTDOW_ERROR_MSG);
        return (SNMP_FAILURE);
    }

    i4RetStatus = QoSGetNextPlyMapTblEntryIndex (u4FsQoSPolicyMapId,
                                                 pu4NextFsQoSPolicyMapId);
    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSGetNextPlyMapTblEntryIndex () "
                      "Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    return (SNMP_SUCCESS);
}

/* Low Level GET Routine for All Objects  */
/****************************************************************************
 Function    :  nmhGetFsQoSPolicyMapName
 Input       :  The Indices
                FsQoSPolicyMapId

                The Object 
                retValFsQoSPolicyMapName
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsQoSPolicyMapName (UINT4 u4FsQoSPolicyMapId,
                          tSNMP_OCTET_STRING_TYPE * pRetValFsQoSPolicyMapName)
{
    tQoSPolicyMapNode  *pPlyMapNode = NULL;
    UINT1              *pu1Str = NULL;

    if ((pRetValFsQoSPolicyMapName == NULL) ||
        (pRetValFsQoSPolicyMapName->pu1_OctetList == NULL))
    {
        return (SNMP_FAILURE);
    }

    pPlyMapNode = QoSUtlGetPolicyMapNode (u4FsQoSPolicyMapId);

    if (pPlyMapNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetPolicyMapNode () Returns "
                      "FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    pu1Str = &(pPlyMapNode->au1Name[0]);
    QOS_UTL_COPY_TABLE_NAME (pRetValFsQoSPolicyMapName, pu1Str);

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFsQoSPolicyMapIfIndex
 Input       :  The Indices
                FsQoSPolicyMapId

                The Object 
                retValFsQoSPolicyMapIfIndex
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsQoSPolicyMapIfIndex (UINT4 u4FsQoSPolicyMapId,
                             UINT4 *pu4RetValFsQoSPolicyMapIfIndex)
{
    tQoSPolicyMapNode  *pPlyMapNode = NULL;

    pPlyMapNode = QoSUtlGetPolicyMapNode (u4FsQoSPolicyMapId);

    if (pPlyMapNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetPolicyMapNode () Returns "
                      "FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    *pu4RetValFsQoSPolicyMapIfIndex = pPlyMapNode->u4IfIndex;

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFsQoSPolicyMapCLASS
 Input       :  The Indices
                FsQoSPolicyMapId

                The Object 
                retValFsQoSPolicyMapCLASS
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsQoSPolicyMapCLASS (UINT4 u4FsQoSPolicyMapId,
                           UINT4 *pu4RetValFsQoSPolicyMapCLASS)
{
    tQoSPolicyMapNode  *pPlyMapNode = NULL;

    pPlyMapNode = QoSUtlGetPolicyMapNode (u4FsQoSPolicyMapId);

    if (pPlyMapNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetPolicyMapNode () Returns "
                      "FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    *pu4RetValFsQoSPolicyMapCLASS = pPlyMapNode->u4ClassId;

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFsQoSPolicyMapPHBType
 Input       :  The Indices
                FsQoSPolicyMapId

                The Object 
                retValFsQoSPolicyMapPHBType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsQoSPolicyMapPHBType (UINT4 u4FsQoSPolicyMapId,
                             INT4 *pi4RetValFsQoSPolicyMapPHBType)
{
    tQoSPolicyMapNode  *pPlyMapNode = NULL;

    pPlyMapNode = QoSUtlGetPolicyMapNode (u4FsQoSPolicyMapId);

    if (pPlyMapNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetPolicyMapNode () Returns "
                      "FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    *pi4RetValFsQoSPolicyMapPHBType = (INT4) pPlyMapNode->u1PHBType;

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFsQoSPolicyMapDefaultPHB
 Input       :  The Indices
                FsQoSPolicyMapId

                The Object 
                retValFsQoSPolicyMapDefaultPHB
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsQoSPolicyMapDefaultPHB (UINT4 u4FsQoSPolicyMapId,
                                UINT4 *pu4RetValFsQoSPolicyMapDefaultPHB)
{
    tQoSPolicyMapNode  *pPlyMapNode = NULL;

    pPlyMapNode = QoSUtlGetPolicyMapNode (u4FsQoSPolicyMapId);

    if (pPlyMapNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetPolicyMapNode () Returns "
                      "FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    *pu4RetValFsQoSPolicyMapDefaultPHB = (INT4) pPlyMapNode->u2DefaultPHB;

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFsQoSPolicyMapMeterTableId
 Input       :  The Indices
                FsQoSPolicyMapId

                The Object 
                retValFsQoSPolicyMapMeterTableId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsQoSPolicyMapMeterTableId (UINT4 u4FsQoSPolicyMapId,
                                  UINT4 *pu4RetValFsQoSPolicyMapMeterTableId)
{
    tQoSPolicyMapNode  *pPlyMapNode = NULL;

    pPlyMapNode = QoSUtlGetPolicyMapNode (u4FsQoSPolicyMapId);

    if (pPlyMapNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetPolicyMapNode () Returns "
                      "FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    *pu4RetValFsQoSPolicyMapMeterTableId = pPlyMapNode->u4MeterTableId;

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFsQoSPolicyMapFailMeterTableId
 Input       :  The Indices
                FsQoSPolicyMapId

                The Object 
                retValFsQoSPolicyMapFailMeterTableId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsQoSPolicyMapFailMeterTableId (UINT4 u4FsQoSPolicyMapId,
                                      UINT4
                                      *pu4RetValFsQoSPolicyMapFailMeterTableId)
{
    tQoSPolicyMapNode  *pPlyMapNode = NULL;

    pPlyMapNode = QoSUtlGetPolicyMapNode (u4FsQoSPolicyMapId);

    if (pPlyMapNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetPolicyMapNode () Returns "
                      "FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    *pu4RetValFsQoSPolicyMapFailMeterTableId = pPlyMapNode->u4FailMeterTableId;

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFsQoSPolicyMapInProfileConformActionFlag
 Input       :  The Indices
                FsQoSPolicyMapId

                The Object 
                retValFsQoSPolicyMapInProfileConformActionFlag
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
     
     
     
     
     
     
     
    nmhGetFsQoSPolicyMapInProfileConformActionFlag
    (UINT4 u4FsQoSPolicyMapId,
     tSNMP_OCTET_STRING_TYPE * pRetValFsQoSPolicyMapInProfileConformActionFlag)
{
    tQoSPolicyMapNode  *pPlyMapNode = NULL;
    UINT1               u1ActFlag = 0;

    if ((pRetValFsQoSPolicyMapInProfileConformActionFlag == NULL) ||
        (pRetValFsQoSPolicyMapInProfileConformActionFlag->pu1_OctetList ==
         NULL))
    {
        return (SNMP_FAILURE);
    }

    pPlyMapNode = QoSUtlGetPolicyMapNode (u4FsQoSPolicyMapId);

    if (pPlyMapNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetPolicyMapNode () Returns "
                      "FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    u1ActFlag = pPlyMapNode->u1InProfConfActFlag;

    QOS_GET_POLICY_ACTIONS_MASK
        (pRetValFsQoSPolicyMapInProfileConformActionFlag, u1ActFlag);

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFsQoSPolicyMapInProfileConformActionId
 Input       :  The Indices
                FsQoSPolicyMapId

                The Object 
                retValFsQoSPolicyMapInProfileConformActionId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsQoSPolicyMapInProfileConformActionId (UINT4 u4FsQoSPolicyMapId,
                                              UINT4
                                              *pu4RetValFsQoSPolicyMapInProfileConformActionId)
{
    tQoSPolicyMapNode  *pPlyMapNode = NULL;

    pPlyMapNode = QoSUtlGetPolicyMapNode (u4FsQoSPolicyMapId);

    if (pPlyMapNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetPolicyMapNode () Returns "
                      "FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    *pu4RetValFsQoSPolicyMapInProfileConformActionId =
        pPlyMapNode->u4InProfileActionId;

    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhGetFsQoSPolicyMapInProfileActionSetPort
 Input       :  The Indices
                FsQoSPolicyMapId

                The Object 
                retValFsQoSPolicyMapInProfileActionSetPort
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
     
     
     
     
     
     
     
    nmhGetFsQoSPolicyMapInProfileActionSetPort
    (UINT4 u4FsQoSPolicyMapId,
     UINT4 *pu4RetValFsQoSPolicyMapInProfileActionSetPort)
{
    tQoSPolicyMapNode  *pPlyMapNode = NULL;

    pPlyMapNode = QoSUtlGetPolicyMapNode (u4FsQoSPolicyMapId);

    if (pPlyMapNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetPolicyMapNode () Returns "
                      "FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    *pu4RetValFsQoSPolicyMapInProfileActionSetPort =
        pPlyMapNode->u4InProfileActionPort;

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFsQoSPolicyMapConformActionSetIpTOS
 Input       :  The Indices
                FsQoSPolicyMapId

                The Object 
                retValFsQoSPolicyMapConformActionSetIpTOS
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
     
     
     
     
     
     
     
    nmhGetFsQoSPolicyMapConformActionSetIpTOS
    (UINT4 u4FsQoSPolicyMapId,
     UINT4 *pu4RetValFsQoSPolicyMapConformActionSetIpTOS)
{
    tQoSPolicyMapNode  *pPlyMapNode = NULL;

    pPlyMapNode = QoSUtlGetPolicyMapNode (u4FsQoSPolicyMapId);

    if (pPlyMapNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetPolicyMapNode () Returns "
                      "FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    *pu4RetValFsQoSPolicyMapConformActionSetIpTOS = (UINT4)
        pPlyMapNode->u1InProActConfDscpOrToS;

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFsQoSPolicyMapConformActionSetDscp
 Input       :  The Indices
                FsQoSPolicyMapId

                The Object 
                retValFsQoSPolicyMapConformActionSetDscp
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
     
     
     
     
     
     
     
    nmhGetFsQoSPolicyMapConformActionSetDscp
    (UINT4 u4FsQoSPolicyMapId,
     INT4 *pi4RetValFsQoSPolicyMapConformActionSetDscp)
{
    tQoSPolicyMapNode  *pPlyMapNode = NULL;

    pPlyMapNode = QoSUtlGetPolicyMapNode (u4FsQoSPolicyMapId);

    if (pPlyMapNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetPolicyMapNode () Returns "
                      "FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    *pi4RetValFsQoSPolicyMapConformActionSetDscp = (INT4)
        pPlyMapNode->u1InProActConfDscpOrToS;

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFsQoSPolicyMapConformActionSetVlanPrio
 Input       :  The Indices
                FsQoSPolicyMapId

                The Object 
                retValFsQoSPolicyMapConformActionSetVlanPrio
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
     
     
     
     
     
     
     
    nmhGetFsQoSPolicyMapConformActionSetVlanPrio
    (UINT4 u4FsQoSPolicyMapId,
     UINT4 *pu4RetValFsQoSPolicyMapConformActionSetVlanPrio)
{
    tQoSPolicyMapNode  *pPlyMapNode = NULL;

    pPlyMapNode = QoSUtlGetPolicyMapNode (u4FsQoSPolicyMapId);

    if (pPlyMapNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetPolicyMapNode () Returns "
                      "FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    *pu4RetValFsQoSPolicyMapConformActionSetVlanPrio = (UINT4)
        pPlyMapNode->u2InProActConfVlanPrio;

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFsQoSPolicyMapConformActionSetVlanDE
 Input       :  The Indices
                FsQoSPolicyMapId

                The Object 
                retValFsQoSPolicyMapConformActionSetVlanDE
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
     
     
     
     
     
     
     
    nmhGetFsQoSPolicyMapConformActionSetVlanDE
    (UINT4 u4FsQoSPolicyMapId,
     UINT4 *pu4RetValFsQoSPolicyMapConformActionSetVlanDE)
{
    tQoSPolicyMapNode  *pPlyMapNode = NULL;

    pPlyMapNode = QoSUtlGetPolicyMapNode (u4FsQoSPolicyMapId);

    if (pPlyMapNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetPolicyMapNode () Returns "
                      "FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    *pu4RetValFsQoSPolicyMapConformActionSetVlanDE = (UINT4)
        pPlyMapNode->u2InProActConfVlanDE;

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFsQoSPolicyMapConformActionSetInnerVlanPrio
 Input       :  The Indices
                FsQoSPolicyMapId

                The Object 
                retValFsQoSPolicyMapConformActionSetInnerVlanPrio
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
     
     
     
     
     
     
     
    nmhGetFsQoSPolicyMapConformActionSetInnerVlanPrio
    (UINT4 u4FsQoSPolicyMapId,
     UINT4 *pu4RetValFsQoSPolicyMapConformActionSetInnerVlanPrio)
{
    tQoSPolicyMapNode  *pPlyMapNode = NULL;

    pPlyMapNode = QoSUtlGetPolicyMapNode (u4FsQoSPolicyMapId);

    if (pPlyMapNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetPolicyMapNode () Returns "
                      "FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    *pu4RetValFsQoSPolicyMapConformActionSetInnerVlanPrio = (UINT4)
        pPlyMapNode->u2InProActConfInnerVlanPrio;

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFsQoSPolicyMapConformActionSetMplsExp
 Input       :  The Indices
                FsQoSPolicyMapId

                The Object 
                retValFsQoSPolicyMapConformActionSetMplsExp
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
     
     
     
     
     
     
     
    nmhGetFsQoSPolicyMapConformActionSetMplsExp
    (UINT4 u4FsQoSPolicyMapId,
     UINT4 *pu4RetValFsQoSPolicyMapConformActionSetMplsExp)
{
    tQoSPolicyMapNode  *pPlyMapNode = NULL;

    pPlyMapNode = QoSUtlGetPolicyMapNode (u4FsQoSPolicyMapId);

    if (pPlyMapNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetPolicyMapNode () Returns "
                      "FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    *pu4RetValFsQoSPolicyMapConformActionSetMplsExp = (UINT4)
        pPlyMapNode->u1InProActConfMplsExp;

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFsQoSPolicyMapConformActionSetNewCLASS
 Input       :  The Indices
                FsQoSPolicyMapId

                The Object 
                retValFsQoSPolicyMapConformActionSetNewCLASS
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
     
     
     
     
     
     
     
    nmhGetFsQoSPolicyMapConformActionSetNewCLASS
    (UINT4 u4FsQoSPolicyMapId,
     UINT4 *pu4RetValFsQoSPolicyMapConformActionSetNewCLASS)
{
    tQoSPolicyMapNode  *pPlyMapNode = NULL;

    pPlyMapNode = QoSUtlGetPolicyMapNode (u4FsQoSPolicyMapId);

    if (pPlyMapNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetPolicyMapNode () Returns "
                      "FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    *pu4RetValFsQoSPolicyMapConformActionSetNewCLASS = (UINT4)
        pPlyMapNode->u4InProActConfNewClassId;

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFsQoSPolicyMapInProfileExceedActionFlag
 Input       :  The Indices
                FsQoSPolicyMapId

                The Object 
                retValFsQoSPolicyMapInProfileExceedActionFlag
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
     
     
     
     
     
     
     
    nmhGetFsQoSPolicyMapInProfileExceedActionFlag
    (UINT4 u4FsQoSPolicyMapId,
     tSNMP_OCTET_STRING_TYPE * pRetValFsQoSPolicyMapInProfileExceedActionFlag)
{
    tQoSPolicyMapNode  *pPlyMapNode = NULL;
    UINT1               u1ActFlag = 0;

    if ((pRetValFsQoSPolicyMapInProfileExceedActionFlag == NULL) ||
        (pRetValFsQoSPolicyMapInProfileExceedActionFlag->pu1_OctetList == NULL))
    {
        return (SNMP_FAILURE);
    }

    pPlyMapNode = QoSUtlGetPolicyMapNode (u4FsQoSPolicyMapId);

    if (pPlyMapNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetPolicyMapNode () Returns "
                      "FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    u1ActFlag = pPlyMapNode->u1InProfExcActFlag;

    QOS_GET_POLICY_ACTIONS_MASK (pRetValFsQoSPolicyMapInProfileExceedActionFlag,
                                 u1ActFlag);
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFsQoSPolicyMapInProfileExceedActionId
 Input       :  The Indices
                FsQoSPolicyMapId

                The Object 
                retValFsQoSPolicyMapInProfileExceedActionId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsQoSPolicyMapInProfileExceedActionId (UINT4 u4FsQoSPolicyMapId,
                                             UINT4
                                             *pu4RetValFsQoSPolicyMapInProfileExceedActionId)
{
    tQoSPolicyMapNode  *pPlyMapNode = NULL;

    pPlyMapNode = QoSUtlGetPolicyMapNode (u4FsQoSPolicyMapId);

    if (pPlyMapNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetPolicyMapNode () Returns "
                      "FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    *pu4RetValFsQoSPolicyMapInProfileExceedActionId = (UINT4)
        pPlyMapNode->u4InProActExceedActionId;

    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhGetFsQoSPolicyMapExceedActionSetIpTOS
 Input       :  The Indices
                FsQoSPolicyMapId

                The Object 
                retValFsQoSPolicyMapExceedActionSetIpTOS
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
     
     
     
     
     
     
     
    nmhGetFsQoSPolicyMapExceedActionSetIpTOS
    (UINT4 u4FsQoSPolicyMapId,
     UINT4 *pu4RetValFsQoSPolicyMapExceedActionSetIpTOS)
{
    tQoSPolicyMapNode  *pPlyMapNode = NULL;

    pPlyMapNode = QoSUtlGetPolicyMapNode (u4FsQoSPolicyMapId);

    if (pPlyMapNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetPolicyMapNode () Returns "
                      "FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    *pu4RetValFsQoSPolicyMapExceedActionSetIpTOS = (UINT4)
        pPlyMapNode->u1InProActExceedDscpOrToS;

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFsQoSPolicyMapExceedActionSetDscp
 Input       :  The Indices
                FsQoSPolicyMapId

                The Object 
                retValFsQoSPolicyMapExceedActionSetDscp
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
     
     
     
     
     
     
     
    nmhGetFsQoSPolicyMapExceedActionSetDscp
    (UINT4 u4FsQoSPolicyMapId, INT4 *pi4RetValFsQoSPolicyMapExceedActionSetDscp)
{
    tQoSPolicyMapNode  *pPlyMapNode = NULL;

    pPlyMapNode = QoSUtlGetPolicyMapNode (u4FsQoSPolicyMapId);

    if (pPlyMapNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetPolicyMapNode () Returns "
                      "FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    *pi4RetValFsQoSPolicyMapExceedActionSetDscp = (INT4)
        pPlyMapNode->u1InProActExceedDscpOrToS;

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFsQoSPolicyMapExceedActionSetInnerVlanPrio
 Input       :  The Indices
                FsQoSPolicyMapId

                The Object 
                retValFsQoSPolicyMapExceedActionSetInnerVlanPrio
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
     
     
     
     
     
     
     
    nmhGetFsQoSPolicyMapExceedActionSetInnerVlanPrio
    (UINT4 u4FsQoSPolicyMapId,
     UINT4 *pu4RetValFsQoSPolicyMapExceedActionSetInnerVlanPrio)
{
    tQoSPolicyMapNode  *pPlyMapNode = NULL;

    pPlyMapNode = QoSUtlGetPolicyMapNode (u4FsQoSPolicyMapId);

    if (pPlyMapNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetPolicyMapNode () Returns "
                      "FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    *pu4RetValFsQoSPolicyMapExceedActionSetInnerVlanPrio = (UINT4)
        pPlyMapNode->u2InProActExceedInnerVlanPrio;

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFsQoSPolicyMapExceedActionSetVlanPrio
 Input       :  The Indices
                FsQoSPolicyMapId

                The Object 
                retValFsQoSPolicyMapExceedActionSetVlanPrio
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
     
     
     
     
     
     
     
    nmhGetFsQoSPolicyMapExceedActionSetVlanPrio
    (UINT4 u4FsQoSPolicyMapId,
     UINT4 *pu4RetValFsQoSPolicyMapExceedActionSetVlanPrio)
{
    tQoSPolicyMapNode  *pPlyMapNode = NULL;

    pPlyMapNode = QoSUtlGetPolicyMapNode (u4FsQoSPolicyMapId);

    if (pPlyMapNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetPolicyMapNode () Returns "
                      "FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    *pu4RetValFsQoSPolicyMapExceedActionSetVlanPrio = (UINT4)
        pPlyMapNode->u2InProActExceedVlanPrio;

    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhGetFsQoSPolicyMapExceedActionSetVlanDE
 Input       :  The Indices
                FsQoSPolicyMapId

                The Object 
                retValFsQoSPolicyMapExceedActionSetVlanDE
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
     
     
     
     
     
     
     
    nmhGetFsQoSPolicyMapExceedActionSetVlanDE
    (UINT4 u4FsQoSPolicyMapId,
     UINT4 *pu4RetValFsQoSPolicyMapExceedActionSetVlanDE)
{
    tQoSPolicyMapNode  *pPlyMapNode = NULL;

    pPlyMapNode = QoSUtlGetPolicyMapNode (u4FsQoSPolicyMapId);

    if (pPlyMapNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetPolicyMapNode () Returns "
                      "FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    *pu4RetValFsQoSPolicyMapExceedActionSetVlanDE = (UINT4)
        pPlyMapNode->u2InProActExceedVlanDE;

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFsQoSPolicyMapExceedActionSetMplsExp
 Input       :  The Indices
                FsQoSPolicyMapId

                The Object 
                retValFsQoSPolicyMapExceedActionSetMplsExp
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
     
     
     
     
     
     
     
    nmhGetFsQoSPolicyMapExceedActionSetMplsExp
    (UINT4 u4FsQoSPolicyMapId,
     UINT4 *pu4RetValFsQoSPolicyMapExceedActionSetMplsExp)
{
    tQoSPolicyMapNode  *pPlyMapNode = NULL;

    pPlyMapNode = QoSUtlGetPolicyMapNode (u4FsQoSPolicyMapId);

    if (pPlyMapNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetPolicyMapNode () Returns "
                      "FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    *pu4RetValFsQoSPolicyMapExceedActionSetMplsExp = (UINT4)
        pPlyMapNode->u1InProActExceedMplsExp;

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFsQoSPolicyMapExceedActionSetNewCLASS
 Input       :  The Indices
                FsQoSPolicyMapId

                The Object 
                retValFsQoSPolicyMapExceedActionSetNewCLASS
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
     
     
     
     
     
     
     
    nmhGetFsQoSPolicyMapExceedActionSetNewCLASS
    (UINT4 u4FsQoSPolicyMapId,
     UINT4 *pu4RetValFsQoSPolicyMapExceedActionSetNewCLASS)
{
    tQoSPolicyMapNode  *pPlyMapNode = NULL;

    pPlyMapNode = QoSUtlGetPolicyMapNode (u4FsQoSPolicyMapId);

    if (pPlyMapNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetPolicyMapNode () Returns "
                      "FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    *pu4RetValFsQoSPolicyMapExceedActionSetNewCLASS = (UINT4)
        pPlyMapNode->u4InProActExccedNewClassId;

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFsQoSPolicyMapOutProfileActionFlag
 Input       :  The Indices
                FsQoSPolicyMapId

                The Object 
                retValFsQoSPolicyMapOutProfileActionFlag
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
     
     
     
     
     
     
     
    nmhGetFsQoSPolicyMapOutProfileActionFlag
    (UINT4 u4FsQoSPolicyMapId,
     tSNMP_OCTET_STRING_TYPE * pRetValFsQoSPolicyMapOutProfileActionFlag)
{
    tQoSPolicyMapNode  *pPlyMapNode = NULL;
    UINT1               u1ActFlag = 0;

    pPlyMapNode = QoSUtlGetPolicyMapNode (u4FsQoSPolicyMapId);

    if (pPlyMapNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetPolicyMapNode () Returns "
                      "FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    u1ActFlag = pPlyMapNode->u1OutProActionFlag;

    QOS_GET_POLICY_ACTIONS_MASK (pRetValFsQoSPolicyMapOutProfileActionFlag,
                                 u1ActFlag);

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFsQoSPolicyMapOutProfileActionId
 Input       :  The Indices
                FsQoSPolicyMapId

                The Object 
                retValFsQoSPolicyMapOutProfileActionId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsQoSPolicyMapOutProfileActionId (UINT4 u4FsQoSPolicyMapId,
                                        UINT4
                                        *pu4RetValFsQoSPolicyMapOutProfileActionId)
{
    tQoSPolicyMapNode  *pPlyMapNode = NULL;

    pPlyMapNode = QoSUtlGetPolicyMapNode (u4FsQoSPolicyMapId);

    if (pPlyMapNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetPolicyMapNode () Returns "
                      "FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    *pu4RetValFsQoSPolicyMapOutProfileActionId = (UINT4)
        pPlyMapNode->u4OutProActionId;

    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhGetFsQoSPolicyMapOutProfileActionSetIPTOS
 Input       :  The Indices
                FsQoSPolicyMapId

                The Object 
                retValFsQoSPolicyMapOutProfileActionSetIPTOS
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
     
     
     
     
     
     
     
    nmhGetFsQoSPolicyMapOutProfileActionSetIPTOS
    (UINT4 u4FsQoSPolicyMapId,
     UINT4 *pu4RetValFsQoSPolicyMapOutProfileActionSetIPTOS)
{
    tQoSPolicyMapNode  *pPlyMapNode = NULL;

    pPlyMapNode = QoSUtlGetPolicyMapNode (u4FsQoSPolicyMapId);

    if (pPlyMapNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetPolicyMapNode () Returns "
                      "FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    *pu4RetValFsQoSPolicyMapOutProfileActionSetIPTOS = (UINT4)
        pPlyMapNode->u1OutProActDscpOrTos;

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFsQoSPolicyMapOutProfileActionSetDscp
 Input       :  The Indices
                FsQoSPolicyMapId

                The Object 
                retValFsQoSPolicyMapOutProfileActionSetDscp
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
     
     
     
     
     
     
     
    nmhGetFsQoSPolicyMapOutProfileActionSetDscp
    (UINT4 u4FsQoSPolicyMapId,
     INT4 *pi4RetValFsQoSPolicyMapOutProfileActionSetDscp)
{
    tQoSPolicyMapNode  *pPlyMapNode = NULL;

    pPlyMapNode = QoSUtlGetPolicyMapNode (u4FsQoSPolicyMapId);

    if (pPlyMapNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetPolicyMapNode () Returns "
                      "FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    *pi4RetValFsQoSPolicyMapOutProfileActionSetDscp = (INT4)
        pPlyMapNode->u1OutProActDscpOrTos;

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFsQoSPolicyMapOutProfileActionSetInnerVlanPrio
 Input       :  The Indices
                FsQoSPolicyMapId

                The Object 
                retValFsQoSPolicyMapOutProfileActionSetInnerVlanPrio
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
     
     
     
     
     
     
     
    nmhGetFsQoSPolicyMapOutProfileActionSetInnerVlanPrio
    (UINT4 u4FsQoSPolicyMapId,
     UINT4 *pu4RetValFsQoSPolicyMapOutProfileActionSetInnerVlanPrio)
{

    tQoSPolicyMapNode  *pPlyMapNode = NULL;

    pPlyMapNode = QoSUtlGetPolicyMapNode (u4FsQoSPolicyMapId);

    if (pPlyMapNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetPolicyMapNode () Returns "
                      "FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    *pu4RetValFsQoSPolicyMapOutProfileActionSetInnerVlanPrio = (UINT4)
        pPlyMapNode->u2OutProActInnerVlanPrio;

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFsQoSPolicyMapOutProfileActionSetVlanPrio
 Input       :  The Indices
                FsQoSPolicyMapId

                The Object 
                retValFsQoSPolicyMapOutProfileActionSetVlanPrio
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
     
     
     
     
     
     
     
    nmhGetFsQoSPolicyMapOutProfileActionSetVlanPrio
    (UINT4 u4FsQoSPolicyMapId,
     UINT4 *pu4RetValFsQoSPolicyMapOutProfileActionSetVlanPrio)
{
    tQoSPolicyMapNode  *pPlyMapNode = NULL;

    pPlyMapNode = QoSUtlGetPolicyMapNode (u4FsQoSPolicyMapId);

    if (pPlyMapNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetPolicyMapNode () Returns "
                      "FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    *pu4RetValFsQoSPolicyMapOutProfileActionSetVlanPrio = (UINT4)
        pPlyMapNode->u1OutProActVlanPrio;

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFsQoSPolicyMapOutProfileActionSetVlanDE
 Input       :  The Indices
                FsQoSPolicyMapId

                The Object 
                retValFsQoSPolicyMapOutProfileActionSetVlanDE
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
     
     
     
     
     
     
     
    nmhGetFsQoSPolicyMapOutProfileActionSetVlanDE
    (UINT4 u4FsQoSPolicyMapId,
     UINT4 *pu4RetValFsQoSPolicyMapOutProfileActionSetVlanDE)
{
    tQoSPolicyMapNode  *pPlyMapNode = NULL;

    pPlyMapNode = QoSUtlGetPolicyMapNode (u4FsQoSPolicyMapId);

    if (pPlyMapNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetPolicyMapNode () Returns "
                      "FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    *pu4RetValFsQoSPolicyMapOutProfileActionSetVlanDE = (UINT4)
        pPlyMapNode->u2OutProActVlanDE;

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFsQoSPolicyMapOutProfileActionSetMplsExp
 Input       :  The Indices
                FsQoSPolicyMapId

                The Object 
                retValFsQoSPolicyMapOutProfileActionSetMplsExp
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
     
     
     
     
     
     
     
    nmhGetFsQoSPolicyMapOutProfileActionSetMplsExp
    (UINT4 u4FsQoSPolicyMapId,
     UINT4 *pu4RetValFsQoSPolicyMapOutProfileActionSetMplsExp)
{
    tQoSPolicyMapNode  *pPlyMapNode = NULL;

    pPlyMapNode = QoSUtlGetPolicyMapNode (u4FsQoSPolicyMapId);

    if (pPlyMapNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetPolicyMapNode () Returns "
                      "FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    *pu4RetValFsQoSPolicyMapOutProfileActionSetMplsExp = (UINT4)
        pPlyMapNode->u1OutProActMplsExp;

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFsQoSPolicyMapOutProfileActionSetNewCLASS
 Input       :  The Indices
                FsQoSPolicyMapId

                The Object 
                retValFsQoSPolicyMapOutProfileActionSetNewCLASS
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
     
     
     
     
     
     
     
    nmhGetFsQoSPolicyMapOutProfileActionSetNewCLASS
    (UINT4 u4FsQoSPolicyMapId,
     UINT4 *pu4RetValFsQoSPolicyMapOutProfileActionSetNewCLASS)
{
    tQoSPolicyMapNode  *pPlyMapNode = NULL;

    pPlyMapNode = QoSUtlGetPolicyMapNode (u4FsQoSPolicyMapId);

    if (pPlyMapNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetPolicyMapNode () Returns "
                      "FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    *pu4RetValFsQoSPolicyMapOutProfileActionSetNewCLASS =
        pPlyMapNode->u4OutProActNewClassId;

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFsQoSPolicyMapStatus
 Input       :  The Indices
                FsQoSPolicyMapId

                The Object 
                retValFsQoSPolicyMapStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsQoSPolicyMapStatus (UINT4 u4FsQoSPolicyMapId,
                            INT4 *pi4RetValFsQoSPolicyMapStatus)
{
    tQoSPolicyMapNode  *pPlyMapNode = NULL;

    pPlyMapNode = QoSUtlGetPolicyMapNode (u4FsQoSPolicyMapId);

    if (pPlyMapNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetPolicyMapNode () Returns "
                      "FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    *pi4RetValFsQoSPolicyMapStatus = (INT4) (pPlyMapNode->u1Status);

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFsQoSPolicyMapConformActionSetInnerVlanDE
 Input       :  The Indices
                FsQoSPolicyMapId

                The Object
                retValFsQoSPolicyMapConformActionSetInnerVlanDE
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsQoSPolicyMapConformActionSetInnerVlanDE (UINT4 u4FsQoSPolicyMapId,
                                                 UINT4
                                                 *pu4RetValFsQoSPolicyMapConformActionSetInnerVlanDE)
{
    tQoSPolicyMapNode  *pPlyMapNode = NULL;

    pPlyMapNode = QoSUtlGetPolicyMapNode (u4FsQoSPolicyMapId);

    if (pPlyMapNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetPolicyMapNode () Returns "
                      "FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    *pu4RetValFsQoSPolicyMapConformActionSetInnerVlanDE = (UINT4)
        pPlyMapNode->u2InProActConfInnerVlanDE;

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFsQoSPolicyMapExceedActionSetInnerVlanDE
 Input       :  The Indices
                FsQoSPolicyMapId

                The Object
                retValFsQoSPolicyMapExceedActionSetInnerVlanDE
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsQoSPolicyMapExceedActionSetInnerVlanDE (UINT4 u4FsQoSPolicyMapId,
                                                UINT4
                                                *pu4RetValFsQoSPolicyMapExceedActionSetInnerVlanDE)
{
    tQoSPolicyMapNode  *pPlyMapNode = NULL;

    pPlyMapNode = QoSUtlGetPolicyMapNode (u4FsQoSPolicyMapId);

    if (pPlyMapNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetPolicyMapNode () Returns "
                      "FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    *pu4RetValFsQoSPolicyMapExceedActionSetInnerVlanDE = (UINT4)
        pPlyMapNode->u2InProActExceedInnerVlanDE;

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFsQoSPolicyDefaultVlanDE
 Input       :  The Indices
                FsQoSPolicyMapId
        8693    
        n    8694                    The Object
                retValFsQoSPolicyDefaultVlanDE
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE              
****************************************************************************/
INT1
nmhGetFsQoSPolicyDefaultVlanDE (UINT4 u4FsQoSPolicyMapId,
                                INT4 *pi4RetValFsQoSPolicyDefaultVlanDE)
{
    tQoSPolicyMapNode  *pPlyMapNode = NULL;

    pPlyMapNode = QoSUtlGetPolicyMapNode (u4FsQoSPolicyMapId);

    if (pPlyMapNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetPolicyMapNode () Returns "
                      "FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    *pi4RetValFsQoSPolicyDefaultVlanDE = pPlyMapNode->i4VlanDE;

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFsQoSPolicyMapOutProfileActionSetInnerVlanDE
 Input       :  The Indices
                FsQoSPolicyMapId

                The Object
                retValFsQoSPolicyMapOutProfileActionSetInnerVlanDE
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsQoSPolicyMapOutProfileActionSetInnerVlanDE (UINT4 u4FsQoSPolicyMapId,
                                                    UINT4
                                                    *pu4RetValFsQoSPolicyMapOutProfileActionSetInnerVlanDE)
{
    tQoSPolicyMapNode  *pPlyMapNode = NULL;

    pPlyMapNode = QoSUtlGetPolicyMapNode (u4FsQoSPolicyMapId);

    if (pPlyMapNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetPolicyMapNode () Returns "
                      "FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    *pu4RetValFsQoSPolicyMapOutProfileActionSetInnerVlanDE = (UINT4)
        pPlyMapNode->u2OutProActInnerVlanDE;

    return (SNMP_SUCCESS);
}

/* Low Level SET Routine for All Objects  */
/****************************************************************************
 Function    :  nmhSetFsQoSPolicyMapName
 Input       :  The Indices
                FsQoSPolicyMapId

                The Object 
                setValFsQoSPolicyMapName
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsQoSPolicyMapName (UINT4 u4FsQoSPolicyMapId,
                          tSNMP_OCTET_STRING_TYPE * pSetValFsQoSPolicyMapName)
{
    tQoSPolicyMapNode  *pPlyMapNode = NULL;
    UINT1              *pu1Str = NULL;

    pPlyMapNode = QoSUtlGetPolicyMapNode (u4FsQoSPolicyMapId);

    if (pPlyMapNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetPolicyMapNode () Returns "
                      "FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    pu1Str = &(pPlyMapNode->au1Name[0]);
    QOS_UTL_SET_TABLE_NAME (pu1Str, pSetValFsQoSPolicyMapName);

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhSetFsQoSPolicyMapIfIndex
 Input       :  The Indices
                FsQoSPolicyMapId

                The Object 
                setValFsQoSPolicyMapIfIndex
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsQoSPolicyMapIfIndex (UINT4 u4FsQoSPolicyMapId,
                             UINT4 u4SetValFsQoSPolicyMapIfIndex)
{
    tQoSPolicyMapNode  *pPlyMapNode = NULL;
    INT4                i4RetStatus = QOS_FAILURE;
    UINT4               u4CurrVal = 0;

    pPlyMapNode = QoSUtlGetPolicyMapNode (u4FsQoSPolicyMapId);

    if (pPlyMapNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetPolicyMapNode () Returns "
                      "FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    u4CurrVal = pPlyMapNode->u4IfIndex;
    /* If the current value is same as value to be configured return SUCCESS */
    if (u4CurrVal == u4SetValFsQoSPolicyMapIfIndex)
    {
        return SNMP_SUCCESS;
    }

    pPlyMapNode->u4IfIndex = u4SetValFsQoSPolicyMapIfIndex;

    /* if the RowStatus is ACTIVE then we have to Modify the Existing Policy 
     * otherwise it will be added during Policy ACTIVE */
    if ((pPlyMapNode->u1Status == ACTIVE) &&
        (gQoSGlobalInfo.eSysStatus == QOS_SYS_STATUS_ENABLE))
    {
        i4RetStatus = QoSHwWrUpdatePolicyMapForClass (pPlyMapNode,
                                                      QOS_UPDATE_PLY_IF);
        if (i4RetStatus == QOS_FAILURE)
        {
            QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSHwWrUpdatePolicyMapForClass () "
                          "Returns FAILURE. \r\n", __FUNCTION__);

            pPlyMapNode->u4IfIndex = u4CurrVal;

            return (SNMP_FAILURE);
        }
    }

    QoSValidatePlyMapTblEntry (pPlyMapNode);

    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhSetFsQoSPolicyMapCLASS
 Input       :  The Indices
                FsQoSPolicyMapId

                The Object 
                setValFsQoSPolicyMapCLASS
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsQoSPolicyMapCLASS (UINT4 u4FsQoSPolicyMapId,
                           UINT4 u4SetValFsQoSPolicyMapCLASS)
{
    tQoSPolicyMapNode  *pPlyMapNode = NULL;
    UINT4               u4CurrVal = 0;
    UINT4               u4RetStatus = QOS_FAILURE;

    pPlyMapNode = QoSUtlGetPolicyMapNode (u4FsQoSPolicyMapId);

    if (pPlyMapNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetPolicyMapNode () Returns "
                      "FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    /* if the RowStatus is ACTIVE then we have to Modify the Existing Policy 
     * otherwise it will be added during Policy ACTIVE */
    if ((pPlyMapNode->u1Status == ACTIVE) &&
        (gQoSGlobalInfo.eSysStatus == QOS_SYS_STATUS_ENABLE))
    {
        /* 1. Remove the entry from the HARDWARE. */
        u4RetStatus = QoSHwWrUnmapClassFromPolicy (pPlyMapNode, NULL,
                                                   QOS_PLY_UNMAP);
        if (u4RetStatus == QOS_FAILURE)
        {
            QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSHwWrUnmapClassFromPolicy "
                          "() Returns FAILURE. \r\n", __FUNCTION__);

            return (SNMP_FAILURE);
        }
        /*Delete All Class Map Entries in the HW */
        if (pPlyMapNode->u4ClassId != 0)
        {
            u4RetStatus =
                QoSHwWrUtilDeleteAllClsMapEntry (pPlyMapNode->u4ClassId);
            if (u4RetStatus == QOS_FAILURE)
            {
                QOS_TRC_ARG1 (MGMT_TRC, "In %s : "
                              "QoSHwWrUtilDeleteAllClsMapEntry () Returns "
                              "FAILURE. \r\n", __FUNCTION__);
                return (SNMP_FAILURE);
            }
        }
    }
    /* 2. Remove the entry from the Internal Reference. */
    u4RetStatus = QoSClsInfoDelPolicy (pPlyMapNode);
    if (u4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSClsInfoDelPolicy () "
                      "Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    if (u4SetValFsQoSPolicyMapCLASS != 0)
    {
        u4RetStatus = QoSClsInfoAddPolicy (u4SetValFsQoSPolicyMapCLASS,
                                           pPlyMapNode);
        if (u4RetStatus == QOS_FAILURE)
        {
            QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSClsInfoAddPolicy () "
                          "Returns FAILURE. \r\n", __FUNCTION__);

            return (SNMP_FAILURE);
        }
    }
    u4CurrVal = pPlyMapNode->u4ClassId;
    pPlyMapNode->u4ClassId = u4SetValFsQoSPolicyMapCLASS;

    /* if the RowStatus is ACTIVE then we have to Modify the Existing Policy 
     * otherwise it will be added during Policy ACTIVE */
    if ((pPlyMapNode->u1Status == ACTIVE) &&
        (gQoSGlobalInfo.eSysStatus == QOS_SYS_STATUS_ENABLE))
    {
        /* program the HARDWARE Given Class to this Policy */
        u4RetStatus = QoSHwWrMapClassToPolicy (pPlyMapNode, NULL, QOS_PLY_MAP);
        if (u4RetStatus == QOS_FAILURE || u4RetStatus == QOS_NP_NOT_SUPPORTED)
        {
            QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSHwWrMapClassToPolicy "
                          " () Returns FAILURE. \r\n", __FUNCTION__);
            CLI_SET_ERR (QOS_CLI_ERR_UNSUPPORTED_BY_HW);
            /* Revert the CLASS Changes */
            pPlyMapNode->u4ClassId = u4CurrVal;

            /* Reinstall old CLASS */
            if (u4CurrVal != 0)
            {
                u4RetStatus = QoSClsInfoAddPolicy (u4CurrVal, pPlyMapNode);
                if (u4RetStatus == QOS_FAILURE)
                {
                    QOS_TRC_ARG1 (MGMT_TRC, "In %s :ReInstall: "
                                  "QoSClsInfoAddPolicy"
                                  " () Returns FAILURE. \r\n", __FUNCTION__);
                    return (SNMP_FAILURE);
                }
            }
            /* program the HARDWARE Given Class to this Policy */
            u4RetStatus = QoSHwWrMapClassToPolicy (pPlyMapNode, NULL,
                                                   QOS_PLY_MAP);
            if (u4RetStatus == QOS_FAILURE)
            {
                QOS_TRC_ARG1 (MGMT_TRC, "In %s :ReInstall: "
                              "QoSHwWrMapClassToPolicy () Returns FAILURE."
                              " \r\n", __FUNCTION__);
            }

            return (SNMP_FAILURE);
        }
    }

    QoSValidatePlyMapTblEntry (pPlyMapNode);

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhSetFsQoSPolicyMapPHBType
 Input       :  The Indices
                FsQoSPolicyMapId

                The Object 
                setValFsQoSPolicyMapPHBType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsQoSPolicyMapPHBType (UINT4 u4FsQoSPolicyMapId,
                             INT4 i4SetValFsQoSPolicyMapPHBType)
{
    tQoSPolicyMapNode  *pPlyMapNode = NULL;
    UINT1               u1CurrVal = 0;
    INT4                i4RetStatus = QOS_FAILURE;

    pPlyMapNode = QoSUtlGetPolicyMapNode (u4FsQoSPolicyMapId);

    if (pPlyMapNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetPolicyMapNode () Returns "
                      "FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    u1CurrVal = pPlyMapNode->u1PHBType;

    /* If the existing PHBType and New PHBType are same,
     * then no need to program the HW.
     */
    if (u1CurrVal == i4SetValFsQoSPolicyMapPHBType)
    {
        return (SNMP_SUCCESS);
    }
    /* Check the Priority range for the PBH type.
     * When PHB type is changes NP programming is triggered with 
     * the new PHB Type and existing PHB value. */
    if (QoSUtlValidatePriority ((UINT1) i4SetValFsQoSPolicyMapPHBType,
                                pPlyMapNode->u2DefaultPHB) != QOS_SUCCESS)
    {
        /* Reset Default PHB value when that value exceeds to new PHB type. */
        pPlyMapNode->u2DefaultPHB = 0;
    }

    pPlyMapNode->u1PHBType = (UINT1) i4SetValFsQoSPolicyMapPHBType;

    /* if the RowStatus is ACTIVE then we have to Modify the Existing Policy 
     * otherwise it will be added during Policy ACTIVE */
    if ((pPlyMapNode->u1Status == ACTIVE) &&
        (gQoSGlobalInfo.eSysStatus == QOS_SYS_STATUS_ENABLE))
    {
        i4RetStatus = QoSHwWrUpdatePolicyMapForClass (pPlyMapNode,
                                                      QOS_UPDATE_PLY_PHB_TYPE);
        if (i4RetStatus == QOS_FAILURE)
        {
            QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSHwWrUpdatePolicyMapForClass () "
                          "Returns FAILURE. \r\n", __FUNCTION__);

            pPlyMapNode->u1PHBType = u1CurrVal;

            return (SNMP_FAILURE);
        }
    }

    QoSValidatePlyMapTblEntry (pPlyMapNode);

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhSetFsQoSPolicyMapDefaultPHB
 Input       :  The Indices
                FsQoSPolicyMapId

                The Object 
                setValFsQoSPolicyMapDefaultPHB
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsQoSPolicyMapDefaultPHB (UINT4 u4FsQoSPolicyMapId,
                                UINT4 u4SetValFsQoSPolicyMapDefaultPHB)
{
    tQoSPolicyMapNode  *pPlyMapNode = NULL;
    INT4                i4RetStatus = QOS_FAILURE;
    UINT2               u2CurrVal = 0;

    pPlyMapNode = QoSUtlGetPolicyMapNode (u4FsQoSPolicyMapId);

    if (pPlyMapNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetPolicyMapNode () Returns "
                      "FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    u2CurrVal = pPlyMapNode->u2DefaultPHB;
    pPlyMapNode->u2DefaultPHB = (UINT2) u4SetValFsQoSPolicyMapDefaultPHB;

    /* if the RowStatus is ACTIVE then we have to Modify the Existing Policy 
     * otherwise it will be added during Policy ACTIVE 
     * When PHB vlaue is changed NP programming is triggered with 
     * the existing PHB Type and new PHB value. */
    if ((pPlyMapNode->u1Status == ACTIVE) &&
        (gQoSGlobalInfo.eSysStatus == QOS_SYS_STATUS_ENABLE))
    {
        i4RetStatus = QoSHwWrUpdatePolicyMapForClass (pPlyMapNode,
                                                      QOS_UPDATE_PLY_PHB_VAL);
        if (i4RetStatus == QOS_FAILURE)
        {
            QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSHwWrUpdatePolicyMapForClass () "
                          "Returns FAILURE. \r\n", __FUNCTION__);

            pPlyMapNode->u2DefaultPHB = u2CurrVal;

            return (SNMP_FAILURE);
        }
    }

    QoSValidatePlyMapTblEntry (pPlyMapNode);

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhSetFsQoSPolicyMapMeterTableId
 Input       :  The Indices
                FsQoSPolicyMapId

                The Object 
                setValFsQoSPolicyMapMeterTableId
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsQoSPolicyMapMeterTableId (UINT4 u4FsQoSPolicyMapId,
                                  UINT4 u4SetValFsQoSPolicyMapMeterTableId)
{
    tQoSPolicyMapNode  *pPlyMapNode = NULL;
    tQoSMeterNode      *pMeterNode = NULL;
    tQoSMeterNode      *pTmpMeterNode = NULL;
    INT4                i4RetStatus = QOS_FAILURE;
    UINT4               u4CurrVal = 0;

    pPlyMapNode = QoSUtlGetPolicyMapNode (u4FsQoSPolicyMapId);

    if (pPlyMapNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetPolicyMapNode () Returns "
                      "FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    /* If the Same value is configured again no need to incrment the 
     * RefCount and Set */
    if (pPlyMapNode->u4MeterTableId == u4SetValFsQoSPolicyMapMeterTableId)
    {
        return (SNMP_SUCCESS);
    }

    u4CurrVal = pPlyMapNode->u4MeterTableId;
    pPlyMapNode->u4MeterTableId = u4SetValFsQoSPolicyMapMeterTableId;

    /* if the RowStatus is ACTIVE then we have to Modify the Existing Policy 
     * otherwise it will be added during Policy ACTIVE */
    if ((pPlyMapNode->u1Status == ACTIVE) &&
        (gQoSGlobalInfo.eSysStatus == QOS_SYS_STATUS_ENABLE))
    {
        i4RetStatus = QoSHwWrUpdatePolicyMapForClass (pPlyMapNode,
                                                      QOS_UPDATE_PLY_METER_ID);
        if (i4RetStatus == QOS_FAILURE)
        {
            QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSHwWrUpdatePolicyMapForClass () "
                          "Returns FAILURE. \r\n", __FUNCTION__);

            pPlyMapNode->u4MeterTableId = u4CurrVal;

            return (SNMP_FAILURE);
        }
    }

    /* Reset corresponding  old Meter ref count if the id not '0' */
    if (u4CurrVal != 0)
    {
        pTmpMeterNode = QoSUtlGetMeterNode (u4CurrVal);
        if (pTmpMeterNode == NULL)
        {
            QOS_TRC_ARG1 (MGMT_TRC, "In %s :Reset: QoSUtlGetMeterNode () "
                          "Returns FAILURE. \r\n", __FUNCTION__);

            return (SNMP_FAILURE);
        }

        pTmpMeterNode->u4RefCount = (pTmpMeterNode->u4RefCount) - 1;
    }

    if (u4SetValFsQoSPolicyMapMeterTableId != 0)
    {
        pMeterNode = QoSUtlGetMeterNode (u4SetValFsQoSPolicyMapMeterTableId);
        if (pMeterNode == NULL)
        {
            QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetMeterNode () "
                          "Returns FAILURE. \r\n", __FUNCTION__);

            return (SNMP_FAILURE);
        }

        /* Incremnet the RefCount of Meter Id  Entry in the Meter Table */
        pMeterNode->u4RefCount = (pMeterNode->u4RefCount) + 1;
    }
    else
    {
        QoSResetPlyMap (pPlyMapNode);
        pPlyMapNode->u2RefCount = -1;
    }

    QoSValidatePlyMapTblEntry (pPlyMapNode);

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhSetFsQoSPolicyMapFailMeterTableId
 Input       :  The Indices
                FsQoSPolicyMapId

                The Object 
                setValFsQoSPolicyMapFailMeterTableId
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsQoSPolicyMapFailMeterTableId (UINT4 u4FsQoSPolicyMapId,
                                      UINT4
                                      u4SetValFsQoSPolicyMapFailMeterTableId)
{
    tQoSPolicyMapNode  *pPlyMapNode = NULL;

    pPlyMapNode = QoSUtlGetPolicyMapNode (u4FsQoSPolicyMapId);

    if (pPlyMapNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetPolicyMapNode () Returns "
                      "FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    pPlyMapNode->u4FailMeterTableId = u4SetValFsQoSPolicyMapFailMeterTableId;

    /* This Meter Id is the Fail MeterId which is essentially a dummy meter
     * This Id is used only for storing the Id configure by the StdQosMIB
     * Hence, we do not mae any hardware calls*/

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsQoSPolicyMapInProfileConformActionFlag
 Input       :  The Indices
                FsQoSPolicyMapId

                The Object 
                setValFsQoSPolicyMapInProfileConformActionFlag
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
     
     
     
     
     
     
     
    nmhSetFsQoSPolicyMapInProfileConformActionFlag
    (UINT4 u4FsQoSPolicyMapId,
     tSNMP_OCTET_STRING_TYPE * pSetValFsQoSPolicyMapInProfileConformActionFlag)
{
    tQoSPolicyMapNode  *pPlyMapNode = NULL;
    INT4                i4RetStatus = QOS_FAILURE;
    UINT1               u1CurrVal = 0;

    pPlyMapNode = QoSUtlGetPolicyMapNode (u4FsQoSPolicyMapId);

    if (pPlyMapNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetPolicyMapNode () Returns "
                      "FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    u1CurrVal = pPlyMapNode->u1InProfConfActFlag;
    pPlyMapNode->u1InProfConfActFlag = 0;
    QOS_SET_POLICY_ACTIONS_MASK
        (&(pPlyMapNode->u1InProfConfActFlag),
         pSetValFsQoSPolicyMapInProfileConformActionFlag);

    /* if the RowStatus is ACTIVE then we have to Modify the Existing Policy 
     * otherwise it will be added during Policy ACTIVE */
    if ((pPlyMapNode->u1Status == ACTIVE) &&
        (gQoSGlobalInfo.eSysStatus == QOS_SYS_STATUS_ENABLE))
    {
        i4RetStatus =
            QoSHwWrUpdatePolicyMapForClass (pPlyMapNode,
                                            QOS_UPDATE_PLY_ACT_CONF_FLAG);
        if (i4RetStatus == QOS_FAILURE)
        {
            QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSHwWrUpdatePolicyMapForClass () "
                          "Returns FAILURE. \r\n", __FUNCTION__);

            pPlyMapNode->u1InProfConfActFlag = u1CurrVal;

            return (SNMP_FAILURE);
        }
    }

    QoSValidatePlyMapTblEntry (pPlyMapNode);

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhSetFsQoSPolicyMapInProfileConformActionId
 Input       :  The Indices
                FsQoSPolicyMapId

                The Object 
                setValFsQoSPolicyMapInProfileConformActionId
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsQoSPolicyMapInProfileConformActionId (UINT4 u4FsQoSPolicyMapId,
                                              UINT4
                                              u4SetValFsQoSPolicyMapInProfileConformActionId)
{
    tQoSPolicyMapNode  *pPlyMapNode = NULL;
    INT4                i4RetStatus = QOS_FAILURE;
    UINT4               u4CurrVal = 0;

    pPlyMapNode = QoSUtlGetPolicyMapNode (u4FsQoSPolicyMapId);

    if (pPlyMapNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetPolicyMapNode () Returns "
                      "FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }
    u4CurrVal = pPlyMapNode->u4InProfileActionId;
    pPlyMapNode->u4InProfileActionId =
        u4SetValFsQoSPolicyMapInProfileConformActionId;

    /* if the RowStatus is ACTIVE then we have to Modify the Existing Policy 
     * otherwise it will be added during Policy ACTIVE */
    if ((pPlyMapNode->u1Status == ACTIVE) &&
        (gQoSGlobalInfo.eSysStatus == QOS_SYS_STATUS_ENABLE))
    {
        i4RetStatus =
            QoSHwWrUpdatePolicyMapForClass (pPlyMapNode,
                                            QOS_UPDATE_PLY_ACT_CONF_PORT);
        if (i4RetStatus == QOS_FAILURE)
        {
            QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSHwWrUpdatePolicyMapForClass () "
                          "Returns FAILURE. \r\n", __FUNCTION__);

            pPlyMapNode->u4InProfileActionId = u4CurrVal;

            return (SNMP_FAILURE);
        }
    }

    QoSValidatePlyMapTblEntry (pPlyMapNode);

    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhSetFsQoSPolicyMapInProfileActionSetPort
 Input       :  The Indices
                FsQoSPolicyMapId

                The Object 
                setValFsQoSPolicyMapInProfileActionSetPort
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
     
     
     
     
     
     
     
    nmhSetFsQoSPolicyMapInProfileActionSetPort
    (UINT4 u4FsQoSPolicyMapId,
     UINT4 u4SetValFsQoSPolicyMapInProfileActionSetPort)
{
    tQoSPolicyMapNode  *pPlyMapNode = NULL;
    INT4                i4RetStatus = QOS_FAILURE;
    UINT4               u4CurrVal = 0;

    pPlyMapNode = QoSUtlGetPolicyMapNode (u4FsQoSPolicyMapId);

    if (pPlyMapNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetPolicyMapNode () Returns "
                      "FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    u4CurrVal = pPlyMapNode->u4InProfileActionPort;
    pPlyMapNode->u4InProfileActionPort =
        u4SetValFsQoSPolicyMapInProfileActionSetPort;

    /* if the RowStatus is ACTIVE then we have to Modify the Existing Policy 
     * otherwise it will be added during Policy ACTIVE */
    if ((pPlyMapNode->u1Status == ACTIVE) &&
        (gQoSGlobalInfo.eSysStatus == QOS_SYS_STATUS_ENABLE))
    {
        i4RetStatus =
            QoSHwWrUpdatePolicyMapForClass (pPlyMapNode,
                                            QOS_UPDATE_PLY_ACT_CONF_PORT);
        if (i4RetStatus == QOS_FAILURE)
        {
            QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSHwWrUpdatePolicyMapForClass () "
                          "Returns FAILURE. \r\n", __FUNCTION__);

            pPlyMapNode->u4InProfileActionPort = u4CurrVal;

            return (SNMP_FAILURE);
        }
    }

    QoSValidatePlyMapTblEntry (pPlyMapNode);

    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhSetFsQoSPolicyMapConformActionSetIpTOS
 Input       :  The Indices
                FsQoSPolicyMapId

                The Object 
                setValFsQoSPolicyMapConformActionSetIpTOS
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
     
     
     
     
     
     
     
    nmhSetFsQoSPolicyMapConformActionSetIpTOS
    (UINT4 u4FsQoSPolicyMapId,
     UINT4 u4SetValFsQoSPolicyMapConformActionSetIpTOS)
{
    tQoSPolicyMapNode  *pPlyMapNode = NULL;
    INT4                i4RetStatus = QOS_FAILURE;
    UINT1               u1CurrVal = 0;

    pPlyMapNode = QoSUtlGetPolicyMapNode (u4FsQoSPolicyMapId);

    if (pPlyMapNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetPolicyMapNode Returns "
                      "FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    u1CurrVal = pPlyMapNode->u1InProActConfDscpOrToS;
    pPlyMapNode->u1InProActConfDscpOrToS = (UINT1)
        u4SetValFsQoSPolicyMapConformActionSetIpTOS;

    /* if the RowStatus is ACTIVE then we have to Modify the Existing Policy 
     * otherwise it will be added during Policy ACTIVE */
    if ((pPlyMapNode->u1Status == ACTIVE) &&
        (gQoSGlobalInfo.eSysStatus == QOS_SYS_STATUS_ENABLE))
    {
        i4RetStatus =
            QoSHwWrUpdatePolicyMapForClass (pPlyMapNode,
                                            QOS_UPDATE_PLY_ACT_CONF_IPTOS);
        if (i4RetStatus == QOS_FAILURE)
        {
            QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSHwWrUpdatePolicyMapForClass () "
                          "Returns FAILURE. \r\n", __FUNCTION__);

            pPlyMapNode->u1InProActConfDscpOrToS = u1CurrVal;

            return (SNMP_FAILURE);
        }
    }

    QoSValidatePlyMapTblEntry (pPlyMapNode);

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhSetFsQoSPolicyMapConformActionSetDscp
 Input       :  The Indices
                FsQoSPolicyMapId

                The Object 
                setValFsQoSPolicyMapConformActionSetDscp
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
     
     
     
     
     
     
     
    nmhSetFsQoSPolicyMapConformActionSetDscp
    (UINT4 u4FsQoSPolicyMapId, INT4 i4SetValFsQoSPolicyMapConformActionSetDscp)
{
    tQoSPolicyMapNode  *pPlyMapNode = NULL;
    INT4                i4RetStatus = QOS_FAILURE;
    UINT1               u1CurrVal = 0;
    UINT4               u4ActionIndex = 0;
    UINT4               u4PrevActionIndex = 0;
    tQosStdActionEntry *pActionEntry = NULL;
    UINT1               u1ActionFound = QOS_FALSE;
    tSNMP_OID_TYPE      Oid;
    UINT4               au4TmpArray[14];

    Oid.pu4_OidList = au4TmpArray;

    pPlyMapNode = QoSUtlGetPolicyMapNode (u4FsQoSPolicyMapId);

    if (pPlyMapNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetPolicyMapNode () Returns "
                      "FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    u1CurrVal = pPlyMapNode->u1InProActConfDscpOrToS;
    pPlyMapNode->u1InProActConfDscpOrToS = (INT1)
        i4SetValFsQoSPolicyMapConformActionSetDscp;

    if (pPlyMapNode->u1InProfConfActFlag == QOS_PLY_IN_PROF_CONF_IP_DSCP)
    {
        /* Check whether such an action is already created. If yes, then update the action Id.
         * Else, create an action with Action Next pointing to the mentioned DSCP Value */
        while (QoSGetNextActionTblEntryIndex (u4PrevActionIndex, &u4ActionIndex)
               == QOS_SUCCESS)
        {
            pActionEntry = QoSUtlGetActionNode (u4ActionIndex);
            if (pActionEntry != NULL)
            {
                if ((pActionEntry->u4DSActionSpecificType == DS_STDDSCP) &&
                    (pActionEntry->u4DSActionSpecificIndex ==
                     (UINT4) i4SetValFsQoSPolicyMapConformActionSetDscp))
                {
                    u1ActionFound = QOS_TRUE;
                    break;
                }
                u4PrevActionIndex = u4ActionIndex;
            }
        }

        if (u1ActionFound == QOS_TRUE)
        {
            /* The action was found. Update the action Id */
            pPlyMapNode->u4InProfileActionId = u4ActionIndex;
        }
        else
        {
            u4ActionIndex = 0;
            nmhGetDiffServActionNextFree (&u4ActionIndex);
            nmhSetDiffServActionStatus (u4ActionIndex, CREATE_AND_WAIT);
            QosGetOidFromTypeId (i4SetValFsQoSPolicyMapConformActionSetDscp,
                                 DS_STDDSCP, &Oid);
            nmhSetDiffServActionSpecific (u4ActionIndex, &Oid);
            nmhSetDiffServActionStatus (u4ActionIndex, ACTIVE);
            pPlyMapNode->u4InProfileActionId = u4ActionIndex;

        }
    }
    /* if the RowStatus is ACTIVE then we have to Modify the Existing Policy 
     * otherwise it will be added during Policy ACTIVE */
    if ((pPlyMapNode->u1Status == ACTIVE) &&
        (gQoSGlobalInfo.eSysStatus == QOS_SYS_STATUS_ENABLE))
    {
        i4RetStatus =
            QoSHwWrUpdatePolicyMapForClass (pPlyMapNode,
                                            QOS_UPDATE_PLY_ACT_CONF_IPDSCP);
        if (i4RetStatus == QOS_FAILURE)
        {
            QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSHwWrUpdatePolicyMapForClass () "
                          "Returns FAILURE. \r\n", __FUNCTION__);

            pPlyMapNode->u1InProActConfDscpOrToS = u1CurrVal;

            return (SNMP_FAILURE);
        }
    }

    QoSValidatePlyMapTblEntry (pPlyMapNode);

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhSetFsQoSPolicyMapConformActionSetVlanPrio
 Input       :  The Indices
                FsQoSPolicyMapId

                The Object 
                setValFsQoSPolicyMapConformActionSetVlanPrio
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
     
     
     
     
     
     
     
    nmhSetFsQoSPolicyMapConformActionSetVlanPrio
    (UINT4 u4FsQoSPolicyMapId,
     UINT4 u4SetValFsQoSPolicyMapConformActionSetVlanPrio)
{
    tQoSPolicyMapNode  *pPlyMapNode = NULL;
    INT4                i4RetStatus = QOS_FAILURE;
    UINT2               u2CurrVal = 0;

    pPlyMapNode = QoSUtlGetPolicyMapNode (u4FsQoSPolicyMapId);

    if (pPlyMapNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetPolicyMapNode () Returns "
                      "FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    u2CurrVal = pPlyMapNode->u2InProActConfVlanPrio;
    pPlyMapNode->u2InProActConfVlanPrio = (UINT2)
        u4SetValFsQoSPolicyMapConformActionSetVlanPrio;

    /* if the RowStatus is ACTIVE then we have to Modify the Existing Policy 
     * otherwise it will be added during Policy ACTIVE */
    if ((pPlyMapNode->u1Status == ACTIVE) &&
        (gQoSGlobalInfo.eSysStatus == QOS_SYS_STATUS_ENABLE))
    {
        i4RetStatus =
            QoSHwWrUpdatePolicyMapForClass (pPlyMapNode,
                                            QOS_UPDATE_PLY_ACT_CONF_VLAN_PRI);
        if (i4RetStatus == QOS_FAILURE)
        {
            QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSHwWrUpdatePolicyMapForClass () "
                          "Returns FAILURE. \r\n", __FUNCTION__);

            pPlyMapNode->u2InProActConfVlanPrio = u2CurrVal;

            return (SNMP_FAILURE);
        }
    }

    QoSValidatePlyMapTblEntry (pPlyMapNode);

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhSetFsQoSPolicyMapConformActionSetVlanDE
 Input       :  The Indices
                FsQoSPolicyMapId

                The Object 
                setValFsQoSPolicyMapConformActionSetVlanDE
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
     
     
     
     
     
     
     
    nmhSetFsQoSPolicyMapConformActionSetVlanDE
    (UINT4 u4FsQoSPolicyMapId,
     UINT4 u4SetValFsQoSPolicyMapConformActionSetVlanDE)
{
    tQoSPolicyMapNode  *pPlyMapNode = NULL;
    INT4                i4RetStatus = QOS_FAILURE;
    UINT2               u2CurrVal = 0;

    pPlyMapNode = QoSUtlGetPolicyMapNode (u4FsQoSPolicyMapId);

    if (pPlyMapNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetPolicyMapNode () Returns "
                      "FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    u2CurrVal = pPlyMapNode->u2InProActConfVlanDE;
    pPlyMapNode->u2InProActConfVlanDE = (UINT2)
        u4SetValFsQoSPolicyMapConformActionSetVlanDE;

    /* if the RowStatus is ACTIVE then we have to Modify the Existing Policy 
     * otherwise it will be added during Policy ACTIVE */
    if ((pPlyMapNode->u1Status == ACTIVE) &&
        (gQoSGlobalInfo.eSysStatus == QOS_SYS_STATUS_ENABLE))
    {
        i4RetStatus =
            QoSHwWrUpdatePolicyMapForClass (pPlyMapNode,
                                            QOS_UPDATE_PLY_ACT_CONF_VLAN_DE);
        if (i4RetStatus == QOS_FAILURE)
        {
            QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSHwWrUpdatePolicyMapForClass () "
                          "Returns FAILURE. \r\n", __FUNCTION__);

            pPlyMapNode->u2InProActConfVlanDE = u2CurrVal;

            return (SNMP_FAILURE);
        }
    }

    QoSValidatePlyMapTblEntry (pPlyMapNode);

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhSetFsQoSPolicyMapConformActionSetInnerVlanPrio
 Input       :  The Indices
                FsQoSPolicyMapId

                The Object 
                setValFsQoSPolicyMapConformActionSetInnerVlanPrio
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
     
     
     
     
     
     
     
    nmhSetFsQoSPolicyMapConformActionSetInnerVlanPrio
    (UINT4 u4FsQoSPolicyMapId,
     UINT4 u4SetValFsQoSPolicyMapConformActionSetInnerVlanPrio)
{
    tQoSPolicyMapNode  *pPlyMapNode = NULL;
    INT4                i4RetStatus = QOS_FAILURE;
    UINT2               u2CurrVal = 0;

    pPlyMapNode = QoSUtlGetPolicyMapNode (u4FsQoSPolicyMapId);

    if (pPlyMapNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetPolicyMapNode () Returns "
                      "FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    u2CurrVal = pPlyMapNode->u2InProActConfInnerVlanPrio;
    pPlyMapNode->u2InProActConfInnerVlanPrio = (UINT2)
        u4SetValFsQoSPolicyMapConformActionSetInnerVlanPrio;

    /* if the RowStatus is ACTIVE then we have to Modify the Existing Policy 
     * otherwise it will be added during Policy ACTIVE */
    if ((pPlyMapNode->u1Status == ACTIVE) &&
        (gQoSGlobalInfo.eSysStatus == QOS_SYS_STATUS_ENABLE))
    {
        i4RetStatus =
            QoSHwWrUpdatePolicyMapForClass
            (pPlyMapNode, QOS_UPDATE_PLY_ACT_CONF_INNER_VLAN_PRI);
        if (i4RetStatus == QOS_FAILURE)
        {
            QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSHwWrUpdatePolicyMapForClass () "
                          "Returns FAILURE. \r\n", __FUNCTION__);

            pPlyMapNode->u2InProActConfInnerVlanPrio = u2CurrVal;

            return (SNMP_FAILURE);
        }
    }

    QoSValidatePlyMapTblEntry (pPlyMapNode);

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhSetFsQoSPolicyMapConformActionSetMplsExp
 Input       :  The Indices
                FsQoSPolicyMapId

                The Object 
                setValFsQoSPolicyMapConformActionSetMplsExp
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
     
     
     
     
     
     
     
    nmhSetFsQoSPolicyMapConformActionSetMplsExp
    (UINT4 u4FsQoSPolicyMapId,
     UINT4 u4SetValFsQoSPolicyMapConformActionSetMplsExp)
{
    tQoSPolicyMapNode  *pPlyMapNode = NULL;
    INT4                i4RetStatus = QOS_FAILURE;
    UINT1               u1CurrVal = 0;

    pPlyMapNode = QoSUtlGetPolicyMapNode (u4FsQoSPolicyMapId);

    if (pPlyMapNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetPolicyMapNode () Returns "
                      "FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    u1CurrVal = pPlyMapNode->u1InProActConfMplsExp;
    pPlyMapNode->u1InProActConfMplsExp = (UINT1)
        u4SetValFsQoSPolicyMapConformActionSetMplsExp;

    /* if the RowStatus is ACTIVE then we have to Modify the Existing Policy 
     * otherwise it will be added during Policy ACTIVE */
    if ((pPlyMapNode->u1Status == ACTIVE) &&
        (gQoSGlobalInfo.eSysStatus == QOS_SYS_STATUS_ENABLE))
    {
        i4RetStatus =
            QoSHwWrUpdatePolicyMapForClass (pPlyMapNode,
                                            QOS_UPDATE_PLY_ACT_CONF_MPLS_EXP);
        if (i4RetStatus == QOS_FAILURE)
        {
            QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSHwWrUpdatePolicyMapForClass () "
                          "Returns FAILURE. \r\n", __FUNCTION__);

            pPlyMapNode->u1InProActConfMplsExp = u1CurrVal;

            return (SNMP_FAILURE);
        }
    }

    QoSValidatePlyMapTblEntry (pPlyMapNode);

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhSetFsQoSPolicyMapConformActionSetNewCLASS
 Input       :  The Indices
                FsQoSPolicyMapId

                The Object 
                setValFsQoSPolicyMapConformActionSetNewCLASS
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
     
     
     
     
     
     
     
    nmhSetFsQoSPolicyMapConformActionSetNewCLASS
    (UINT4 u4FsQoSPolicyMapId,
     UINT4 u4SetValFsQoSPolicyMapConformActionSetNewCLASS)
{
    tQoSPolicyMapNode  *pPlyMapNode = NULL;

    pPlyMapNode = QoSUtlGetPolicyMapNode (u4FsQoSPolicyMapId);

    if (pPlyMapNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetPolicyMapNode () Returns "
                      "FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    pPlyMapNode->u4InProActConfNewClassId =
        u4SetValFsQoSPolicyMapConformActionSetNewCLASS;

    QoSValidatePlyMapTblEntry (pPlyMapNode);

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhSetFsQoSPolicyMapInProfileExceedActionFlag
 Input       :  The Indices
                FsQoSPolicyMapId

                The Object 
                setValFsQoSPolicyMapInProfileExceedActionFlag
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
     
     
     
     
     
     
     
    nmhSetFsQoSPolicyMapInProfileExceedActionFlag
    (UINT4 u4FsQoSPolicyMapId,
     tSNMP_OCTET_STRING_TYPE * pSetValFsQoSPolicyMapInProfileExceedActionFlag)
{
    tQoSPolicyMapNode  *pPlyMapNode = NULL;
    INT4                i4RetStatus = QOS_FAILURE;
    UINT1               u1CurrVal = 0;
    UINT1               u1NewVal = 0;

    pPlyMapNode = QoSUtlGetPolicyMapNode (u4FsQoSPolicyMapId);

    if (pPlyMapNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetPolicyMapNode () Returns "
                      "FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    u1CurrVal = pPlyMapNode->u1InProfExcActFlag;

    QOS_SET_POLICY_ACTIONS_MASK
        (&(u1NewVal), pSetValFsQoSPolicyMapInProfileExceedActionFlag);

    if (u1NewVal != QOS_PLY_IN_PROF_EXC_DROP)
    {
        QosUtlUpdInProExcActionDrop (pPlyMapNode);
    }

    QOS_SET_POLICY_ACTIONS_MASK
        (&(pPlyMapNode->u1InProfExcActFlag),
         pSetValFsQoSPolicyMapInProfileExceedActionFlag);

    /* if the RowStatus is ACTIVE then we have to Modify the Existing Policy 
     * otherwise it will be added during Policy ACTIVE */
    if ((pPlyMapNode->u1Status == ACTIVE) &&
        (gQoSGlobalInfo.eSysStatus == QOS_SYS_STATUS_ENABLE))
    {
        i4RetStatus =
            QoSHwWrUpdatePolicyMapForClass (pPlyMapNode,
                                            QOS_UPDATE_PLY_ACT_EXC_FLAG);
        if (i4RetStatus == QOS_FAILURE)
        {
            QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSHwWrUpdatePolicyMapForClass () "
                          "Returns FAILURE. \r\n", __FUNCTION__);

            pPlyMapNode->u1InProfExcActFlag = u1CurrVal;

            return (SNMP_FAILURE);
        }
    }

    QoSValidatePlyMapTblEntry (pPlyMapNode);

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhSetFsQoSPolicyMapInProfileExceedActionId
 Input       :  The Indices
                FsQoSPolicyMapId

                The Object 
                setValFsQoSPolicyMapInProfileExceedActionId
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsQoSPolicyMapInProfileExceedActionId (UINT4 u4FsQoSPolicyMapId,
                                             UINT4
                                             u4SetValFsQoSPolicyMapInProfileExceedActionId)
{
    tQoSPolicyMapNode  *pPlyMapNode = NULL;
    INT4                i4RetStatus = QOS_FAILURE;
    UINT4               u4CurrVal = 0;

    pPlyMapNode = QoSUtlGetPolicyMapNode (u4FsQoSPolicyMapId);

    if (pPlyMapNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetPolicyMapNode () Returns "
                      "FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    u4CurrVal = pPlyMapNode->u4InProActExceedActionId;
    pPlyMapNode->u4InProActExceedActionId = (UINT1)
        u4SetValFsQoSPolicyMapInProfileExceedActionId;

    /* if the RowStatus is ACTIVE then we have to Modify the Existing Policy 
     * otherwise it will be added during Policy ACTIVE */
    if ((pPlyMapNode->u1Status == ACTIVE) &&
        (gQoSGlobalInfo.eSysStatus == QOS_SYS_STATUS_ENABLE))
    {
        i4RetStatus =
            QoSHwWrUpdatePolicyMapForClass (pPlyMapNode,
                                            QOS_UPDATE_PLY_ACT_EXC_IPTOS);
        if (i4RetStatus == QOS_FAILURE)
        {
            QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSHwWrUpdatePolicyMapForClass () "
                          "Returns FAILURE. \r\n", __FUNCTION__);

            pPlyMapNode->u4InProActExceedActionId = u4CurrVal;

            return (SNMP_FAILURE);
        }
    }

    QoSValidatePlyMapTblEntry (pPlyMapNode);

    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhSetFsQoSPolicyMapExceedActionSetIpTOS
 Input       :  The Indices
                FsQoSPolicyMapId

                The Object 
                setValFsQoSPolicyMapExceedActionSetIpTOS
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
     
     
     
     
     
     
     
    nmhSetFsQoSPolicyMapExceedActionSetIpTOS
    (UINT4 u4FsQoSPolicyMapId, UINT4 u4SetValFsQoSPolicyMapExceedActionSetIpTOS)
{
    tQoSPolicyMapNode  *pPlyMapNode = NULL;
    INT4                i4RetStatus = QOS_FAILURE;
    UINT1               u1CurrVal = 0;

    pPlyMapNode = QoSUtlGetPolicyMapNode (u4FsQoSPolicyMapId);

    if (pPlyMapNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetPolicyMapNode () Returns "
                      "FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    u1CurrVal = pPlyMapNode->u1InProActExceedDscpOrToS;
    pPlyMapNode->u1InProActExceedDscpOrToS = (UINT1)
        u4SetValFsQoSPolicyMapExceedActionSetIpTOS;

    /* if the RowStatus is ACTIVE then we have to Modify the Existing Policy 
     * otherwise it will be added during Policy ACTIVE */
    if ((pPlyMapNode->u1Status == ACTIVE) &&
        (gQoSGlobalInfo.eSysStatus == QOS_SYS_STATUS_ENABLE))
    {
        i4RetStatus =
            QoSHwWrUpdatePolicyMapForClass (pPlyMapNode,
                                            QOS_UPDATE_PLY_ACT_EXC_IPTOS);
        if (i4RetStatus == QOS_FAILURE)
        {
            QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSHwWrUpdatePolicyMapForClass () "
                          "Returns FAILURE. \r\n", __FUNCTION__);

            pPlyMapNode->u1InProActExceedDscpOrToS = u1CurrVal;

            return (SNMP_FAILURE);
        }
    }

    QoSValidatePlyMapTblEntry (pPlyMapNode);

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhSetFsQoSPolicyMapExceedActionSetDscp
 Input       :  The Indices
                FsQoSPolicyMapId

                The Object 
                setValFsQoSPolicyMapExceedActionSetDscp
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
     
     
     
     
     
     
     
    nmhSetFsQoSPolicyMapExceedActionSetDscp
    (UINT4 u4FsQoSPolicyMapId, INT4 i4SetValFsQoSPolicyMapExceedActionSetDscp)
{
    tQoSPolicyMapNode  *pPlyMapNode = NULL;
    INT4                i4RetStatus = QOS_FAILURE;
    UINT1               u1CurrVal = 0;
    UINT4               u4ActionIndex = 0;
    UINT4               u4PrevActionIndex = 0;
    tQosStdActionEntry *pActionEntry = NULL;
    UINT1               u1ActionFound = QOS_FALSE;
    tSNMP_OID_TYPE      Oid;
    UINT4               au4TmpArray[14];

    Oid.pu4_OidList = au4TmpArray;

    pPlyMapNode = QoSUtlGetPolicyMapNode (u4FsQoSPolicyMapId);

    if (pPlyMapNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetPolicyMapNode () Returns "
                      "FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    u1CurrVal = pPlyMapNode->u1InProActExceedDscpOrToS;
    pPlyMapNode->u1InProActExceedDscpOrToS = (UINT1)
        i4SetValFsQoSPolicyMapExceedActionSetDscp;

    if (pPlyMapNode->u1InProfExcActFlag == QOS_PLY_IN_PROF_EXC_IP_DSCP)
    {

        /* Check whether such an action is already created. If yes, then update the action Id.
         * Else, create an action with Action Next pointing to the mentioned DSCP Value */
        while (QoSGetNextActionTblEntryIndex (u4PrevActionIndex, &u4ActionIndex)
               == QOS_SUCCESS)
        {
            pActionEntry = QoSUtlGetActionNode (u4ActionIndex);
            if (pActionEntry != NULL)
            {
                if ((pActionEntry->u4DSActionSpecificType == DS_STDDSCP) &&
                    (pActionEntry->u4DSActionSpecificIndex ==
                     (UINT4) i4SetValFsQoSPolicyMapExceedActionSetDscp))
                {
                    u1ActionFound = QOS_TRUE;
                    break;
                }
                u4PrevActionIndex = u4ActionIndex;
            }
        }

        if (u1ActionFound == QOS_TRUE)
        {
            /* The action was found. Update the action Id */
            pPlyMapNode->u4InProActExceedActionId = u4ActionIndex;
        }
        else
        {
            u4ActionIndex = 0;
            nmhGetDiffServActionNextFree (&u4ActionIndex);
            nmhSetDiffServActionStatus (u4ActionIndex, CREATE_AND_WAIT);
            QosGetOidFromTypeId (i4SetValFsQoSPolicyMapExceedActionSetDscp,
                                 DS_STDDSCP, &Oid);
            nmhSetDiffServActionSpecific (u4ActionIndex, &Oid);
            nmhSetDiffServActionStatus (u4ActionIndex, ACTIVE);
            pPlyMapNode->u4InProActExceedActionId = u4ActionIndex;

        }
    }

    /* if the RowStatus is ACTIVE then we have to Modify the Existing Policy 
     * otherwise it will be added during Policy ACTIVE */
    if ((pPlyMapNode->u1Status == ACTIVE) &&
        (gQoSGlobalInfo.eSysStatus == QOS_SYS_STATUS_ENABLE))
    {
        i4RetStatus =
            QoSHwWrUpdatePolicyMapForClass (pPlyMapNode,
                                            QOS_UPDATE_PLY_ACT_EXC_IPDSCP);
        if (i4RetStatus == QOS_FAILURE)
        {
            QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSHwWrUpdatePolicyMapForClass () "
                          "Returns FAILURE. \r\n", __FUNCTION__);

            pPlyMapNode->u1InProActExceedDscpOrToS = u1CurrVal;

            return (SNMP_FAILURE);
        }
    }

    QoSValidatePlyMapTblEntry (pPlyMapNode);

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhSetFsQoSPolicyMapExceedActionSetInnerVlanPrio
 Input       :  The Indices
                FsQoSPolicyMapId

                The Object 
                setValFsQoSPolicyMapExceedActionSetInnerVlanPrio
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
     
     
     
     
     
     
     
    nmhSetFsQoSPolicyMapExceedActionSetInnerVlanPrio
    (UINT4 u4FsQoSPolicyMapId,
     UINT4 u4SetValFsQoSPolicyMapExceedActionSetInnerVlanPrio)
{
    tQoSPolicyMapNode  *pPlyMapNode = NULL;
    INT4                i4RetStatus = QOS_FAILURE;
    UINT2               u2CurrVal = 0;

    pPlyMapNode = QoSUtlGetPolicyMapNode (u4FsQoSPolicyMapId);

    if (pPlyMapNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetPolicyMapNode () Returns "
                      "FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    u2CurrVal = pPlyMapNode->u2InProActExceedInnerVlanPrio;
    pPlyMapNode->u2InProActExceedInnerVlanPrio = (UINT2)
        u4SetValFsQoSPolicyMapExceedActionSetInnerVlanPrio;

    /* if the RowStatus is ACTIVE then we have to Modify the Existing Policy 
     * otherwise it will be added during Policy ACTIVE */
    if ((pPlyMapNode->u1Status == ACTIVE) &&
        (gQoSGlobalInfo.eSysStatus == QOS_SYS_STATUS_ENABLE))
    {
        i4RetStatus =
            QoSHwWrUpdatePolicyMapForClass
            (pPlyMapNode, QOS_UPDATE_PLY_ACT_EXC_INNER_VLAN_PRI);
        if (i4RetStatus == QOS_FAILURE)
        {
            QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSHwWrUpdatePolicyMapForClass () "
                          "Returns FAILURE. \r\n", __FUNCTION__);

            pPlyMapNode->u2InProActExceedInnerVlanPrio = u2CurrVal;

            return (SNMP_FAILURE);
        }
    }

    QoSValidatePlyMapTblEntry (pPlyMapNode);

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhSetFsQoSPolicyMapExceedActionSetVlanPrio
 Input       :  The Indices
                FsQoSPolicyMapId

                The Object 
                setValFsQoSPolicyMapExceedActionSetVlanPrio
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
     
     
     
     
     
     
     
    nmhSetFsQoSPolicyMapExceedActionSetVlanPrio
    (UINT4 u4FsQoSPolicyMapId,
     UINT4 u4SetValFsQoSPolicyMapExceedActionSetVlanPrio)
{
    tQoSPolicyMapNode  *pPlyMapNode = NULL;
    INT4                i4RetStatus = QOS_FAILURE;
    UINT2               u2CurrVal = 0;

    pPlyMapNode = QoSUtlGetPolicyMapNode (u4FsQoSPolicyMapId);

    if (pPlyMapNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetPolicyMapNode () Returns "
                      "FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    u2CurrVal = pPlyMapNode->u2InProActExceedVlanPrio;
    pPlyMapNode->u2InProActExceedVlanPrio = (UINT2)
        u4SetValFsQoSPolicyMapExceedActionSetVlanPrio;

    /* if the RowStatus is ACTIVE then we have to Modify the Existing Policy 
     * otherwise it will be added during Policy ACTIVE */
    if ((pPlyMapNode->u1Status == ACTIVE) &&
        (gQoSGlobalInfo.eSysStatus == QOS_SYS_STATUS_ENABLE))
    {
        i4RetStatus =
            QoSHwWrUpdatePolicyMapForClass (pPlyMapNode,
                                            QOS_UPDATE_PLY_ACT_EXC_VLAN_PRI);
        if (i4RetStatus == QOS_FAILURE)
        {
            QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSHwWrUpdatePolicyMapForClass () "
                          "Returns FAILURE. \r\n", __FUNCTION__);

            pPlyMapNode->u2InProActExceedVlanPrio = u2CurrVal;

            return (SNMP_FAILURE);
        }
    }

    QoSValidatePlyMapTblEntry (pPlyMapNode);

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhSetFsQoSPolicyMapExceedActionSetVlanDE
 Input       :  The Indices
                FsQoSPolicyMapId

                The Object 
                setValFsQoSPolicyMapExceedActionSetVlanDE
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
     
     
     
     
     
     
     
    nmhSetFsQoSPolicyMapExceedActionSetVlanDE
    (UINT4 u4FsQoSPolicyMapId,
     UINT4 u4SetValFsQoSPolicyMapExceedActionSetVlanDE)
{
    tQoSPolicyMapNode  *pPlyMapNode = NULL;
    INT4                i4RetStatus = QOS_FAILURE;
    UINT2               u2CurrVal = 0;

    pPlyMapNode = QoSUtlGetPolicyMapNode (u4FsQoSPolicyMapId);

    if (pPlyMapNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetPolicyMapNode () Returns "
                      "FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    u2CurrVal = pPlyMapNode->u2InProActExceedVlanDE;
    pPlyMapNode->u2InProActExceedVlanDE = (UINT2)
        u4SetValFsQoSPolicyMapExceedActionSetVlanDE;

    /* if the RowStatus is ACTIVE then we have to Modify the Existing Policy 
     * otherwise it will be added during Policy ACTIVE */
    if ((pPlyMapNode->u1Status == ACTIVE) &&
        (gQoSGlobalInfo.eSysStatus == QOS_SYS_STATUS_ENABLE))
    {
        i4RetStatus =
            QoSHwWrUpdatePolicyMapForClass (pPlyMapNode,
                                            QOS_UPDATE_PLY_ACT_EXC_VLAN_DE);
        if (i4RetStatus == QOS_FAILURE)
        {
            QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSHwWrUpdatePolicyMapForClass () "
                          "Returns FAILURE. \r\n", __FUNCTION__);

            pPlyMapNode->u2InProActExceedVlanDE = u2CurrVal;

            return (SNMP_FAILURE);
        }
    }

    QoSValidatePlyMapTblEntry (pPlyMapNode);

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhSetFsQoSPolicyMapExceedActionSetMplsExp
 Input       :  The Indices
                FsQoSPolicyMapId

                The Object 
                setValFsQoSPolicyMapExceedActionSetMplsExp
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
     
     
     
     
     
     
     
    nmhSetFsQoSPolicyMapExceedActionSetMplsExp
    (UINT4 u4FsQoSPolicyMapId,
     UINT4 u4SetValFsQoSPolicyMapExceedActionSetMplsExp)
{
    tQoSPolicyMapNode  *pPlyMapNode = NULL;
    INT4                i4RetStatus = QOS_FAILURE;
    UINT1               u1CurrVal = 0;

    pPlyMapNode = QoSUtlGetPolicyMapNode (u4FsQoSPolicyMapId);

    if (pPlyMapNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetPolicyMapNode () Returns "
                      "FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    u1CurrVal = pPlyMapNode->u1InProActExceedMplsExp;
    pPlyMapNode->u1InProActExceedMplsExp = (UINT1)
        u4SetValFsQoSPolicyMapExceedActionSetMplsExp;

    /* if the RowStatus is ACTIVE then we have to Modify the Existing Policy 
     * otherwise it will be added during Policy ACTIVE */
    if ((pPlyMapNode->u1Status == ACTIVE) &&
        (gQoSGlobalInfo.eSysStatus == QOS_SYS_STATUS_ENABLE))
    {
        i4RetStatus =
            QoSHwWrUpdatePolicyMapForClass (pPlyMapNode,
                                            QOS_UPDATE_PLY_ACT_EXC_MPLS_EXP);
        if (i4RetStatus == QOS_FAILURE)
        {
            QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSHwWrUpdatePolicyMapForClass () "
                          "Returns FAILURE. \r\n", __FUNCTION__);

            pPlyMapNode->u1InProActExceedMplsExp = u1CurrVal;

            return (SNMP_FAILURE);
        }
    }

    QoSValidatePlyMapTblEntry (pPlyMapNode);

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhSetFsQoSPolicyMapExceedActionSetNewCLASS
 Input       :  The Indices
                FsQoSPolicyMapId

                The Object 
                setValFsQoSPolicyMapExceedActionSetNewCLASS
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
     
     
     
     
     
     
     
    nmhSetFsQoSPolicyMapExceedActionSetNewCLASS
    (UINT4 u4FsQoSPolicyMapId,
     UINT4 u4SetValFsQoSPolicyMapExceedActionSetNewCLASS)
{
    tQoSPolicyMapNode  *pPlyMapNode = NULL;

    pPlyMapNode = QoSUtlGetPolicyMapNode (u4FsQoSPolicyMapId);

    if (pPlyMapNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetPolicyMapNode () Returns "
                      "FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    pPlyMapNode->u4InProActExccedNewClassId =
        u4SetValFsQoSPolicyMapExceedActionSetNewCLASS;

    QoSValidatePlyMapTblEntry (pPlyMapNode);

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhSetFsQoSPolicyMapOutProfileActionFlag
 Input       :  The Indices
                FsQoSPolicyMapId

                The Object 
                setValFsQoSPolicyMapOutProfileActionFlag
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
     
     
     
     
     
     
     
    nmhSetFsQoSPolicyMapOutProfileActionFlag
    (UINT4 u4FsQoSPolicyMapId,
     tSNMP_OCTET_STRING_TYPE * pSetValFsQoSPolicyMapOutProfileActionFlag)
{
    tQoSPolicyMapNode  *pPlyMapNode = NULL;
    INT4                i4RetStatus = QOS_FAILURE;
    UINT1               u1CurrVal = 0;

    pPlyMapNode = QoSUtlGetPolicyMapNode (u4FsQoSPolicyMapId);

    if (pPlyMapNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetPolicyMapNode () Returns "
                      "FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    u1CurrVal = pPlyMapNode->u1OutProActionFlag;
    pPlyMapNode->u1OutProActionFlag = 0;
    QOS_SET_POLICY_ACTIONS_MASK (&(pPlyMapNode->u1OutProActionFlag),
                                 pSetValFsQoSPolicyMapOutProfileActionFlag);

    /* if the RowStatus is ACTIVE then we have to Modify the Existing Policy 
     * otherwise it will be added during Policy ACTIVE */
    if ((pPlyMapNode->u1Status == ACTIVE) &&
        (gQoSGlobalInfo.eSysStatus == QOS_SYS_STATUS_ENABLE))
    {
        i4RetStatus =
            QoSHwWrUpdatePolicyMapForClass (pPlyMapNode,
                                            QOS_UPDATE_PLY_ACT_VIO_ACT_FLAG);
        if (i4RetStatus == QOS_FAILURE)
        {
            QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSHwWrUpdatePolicyMapForClass () "
                          "Returns FAILURE. \r\n", __FUNCTION__);

            pPlyMapNode->u1OutProActionFlag = u1CurrVal;

            return (SNMP_FAILURE);
        }
    }

    QoSValidatePlyMapTblEntry (pPlyMapNode);

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhSetFsQoSPolicyMapOutProfileActionId
 Input       :  The Indices
                FsQoSPolicyMapId

                The Object 
                setValFsQoSPolicyMapOutProfileActionId
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsQoSPolicyMapOutProfileActionId (UINT4 u4FsQoSPolicyMapId,
                                        UINT4
                                        u4SetValFsQoSPolicyMapOutProfileActionId)
{
    tQoSPolicyMapNode  *pPlyMapNode = NULL;
    INT4                i4RetStatus = QOS_FAILURE;
    UINT4               u4CurrVal = 0;

    pPlyMapNode = QoSUtlGetPolicyMapNode (u4FsQoSPolicyMapId);

    if (pPlyMapNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetPolicyMapNode () Returns "
                      "FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    u4CurrVal = pPlyMapNode->u4OutProActionId;
    pPlyMapNode->u4OutProActionId = (UINT1)
        u4SetValFsQoSPolicyMapOutProfileActionId;

    /* if the RowStatus is ACTIVE then we have to Modify the Existing Policy 
     * otherwise it will be added during Policy ACTIVE */
    if ((pPlyMapNode->u1Status == ACTIVE) &&
        (gQoSGlobalInfo.eSysStatus == QOS_SYS_STATUS_ENABLE))
    {
        i4RetStatus =
            QoSHwWrUpdatePolicyMapForClass (pPlyMapNode,
                                            QOS_UPDATE_PLY_ACT_VIO_IPTOS);
        if (i4RetStatus == QOS_FAILURE)
        {
            QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSHwWrUpdatePolicyMapForClass () "
                          "Returns FAILURE. \r\n", __FUNCTION__);

            pPlyMapNode->u4OutProActionId = u4CurrVal;

            return (SNMP_FAILURE);
        }
    }

    QoSValidatePlyMapTblEntry (pPlyMapNode);

    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhSetFsQoSPolicyMapOutProfileActionSetIPTOS
 Input       :  The Indices
                FsQoSPolicyMapId

                The Object 
                setValFsQoSPolicyMapOutProfileActionSetIPTOS
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
     
     
     
     
     
     
     
    nmhSetFsQoSPolicyMapOutProfileActionSetIPTOS
    (UINT4 u4FsQoSPolicyMapId,
     UINT4 u4SetValFsQoSPolicyMapOutProfileActionSetIPTOS)
{
    tQoSPolicyMapNode  *pPlyMapNode = NULL;
    INT4                i4RetStatus = QOS_FAILURE;
    UINT1               u1CurrVal = 0;

    pPlyMapNode = QoSUtlGetPolicyMapNode (u4FsQoSPolicyMapId);

    if (pPlyMapNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetPolicyMapNode () Returns "
                      "FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    u1CurrVal = pPlyMapNode->u1OutProActDscpOrTos;
    pPlyMapNode->u1OutProActDscpOrTos = (UINT1)
        u4SetValFsQoSPolicyMapOutProfileActionSetIPTOS;

    /* if the RowStatus is ACTIVE then we have to Modify the Existing Policy 
     * otherwise it will be added during Policy ACTIVE */
    if ((pPlyMapNode->u1Status == ACTIVE) &&
        (gQoSGlobalInfo.eSysStatus == QOS_SYS_STATUS_ENABLE))
    {
        i4RetStatus =
            QoSHwWrUpdatePolicyMapForClass (pPlyMapNode,
                                            QOS_UPDATE_PLY_ACT_VIO_IPTOS);
        if (i4RetStatus == QOS_FAILURE)
        {
            QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSHwWrUpdatePolicyMapForClass () "
                          "Returns FAILURE. \r\n", __FUNCTION__);

            pPlyMapNode->u1OutProActDscpOrTos = u1CurrVal;

            return (SNMP_FAILURE);
        }
    }

    QoSValidatePlyMapTblEntry (pPlyMapNode);

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhSetFsQoSPolicyMapOutProfileActionSetDscp
 Input       :  The Indices
                FsQoSPolicyMapId

                The Object 
                setValFsQoSPolicyMapOutProfileActionSetDscp
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
     
     
     
     
     
     
     
    nmhSetFsQoSPolicyMapOutProfileActionSetDscp
    (UINT4 u4FsQoSPolicyMapId,
     INT4 i4SetValFsQoSPolicyMapOutProfileActionSetDscp)
{
    tQoSPolicyMapNode  *pPlyMapNode = NULL;
    INT4                i4RetStatus = QOS_FAILURE;
    UINT1               u1CurrVal = 0;
    UINT4               u4ActionIndex = 0;
    UINT4               u4PrevActionIndex = 0;
    tQosStdActionEntry *pActionEntry = NULL;
    UINT1               u1ActionFound = QOS_FALSE;
    tSNMP_OID_TYPE      Oid;
    UINT4               au4TmpArray[14];

    Oid.pu4_OidList = au4TmpArray;

    pPlyMapNode = QoSUtlGetPolicyMapNode (u4FsQoSPolicyMapId);

    if (pPlyMapNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetPolicyMapNode () Returns "
                      "FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    u1CurrVal = pPlyMapNode->u1OutProActDscpOrTos;
    pPlyMapNode->u1OutProActDscpOrTos = (UINT1)
        i4SetValFsQoSPolicyMapOutProfileActionSetDscp;

    if (pPlyMapNode->u1OutProActionFlag == QOS_PLY_OUT_PROF_VIO_IP_DSCP)
    {
        /* Check whether such an action is already created. If yes, then update the action Id.
         * Else, create an action with Action Next pointing to the mentioned DSCP Value */
        while (QoSGetNextActionTblEntryIndex (u4PrevActionIndex, &u4ActionIndex)
               == QOS_SUCCESS)
        {
            pActionEntry = QoSUtlGetActionNode (u4ActionIndex);
            if (pActionEntry != NULL)
            {
                if ((pActionEntry->u4DSActionSpecificType == DS_STDDSCP) &&
                    (pActionEntry->u4DSActionSpecificIndex ==
                     (UINT4) i4SetValFsQoSPolicyMapOutProfileActionSetDscp))
                {
                    u1ActionFound = QOS_TRUE;
                    break;
                }
                u4PrevActionIndex = u4ActionIndex;
            }
        }

        if (u1ActionFound == QOS_TRUE)
        {
            /* The action was found. Update the action Id */
            pPlyMapNode->u4OutProActionId = u4ActionIndex;
        }
        else
        {
            u4ActionIndex = 0;
            nmhGetDiffServActionNextFree (&u4ActionIndex);
            nmhSetDiffServActionStatus (u4ActionIndex, CREATE_AND_WAIT);
            QosGetOidFromTypeId (i4SetValFsQoSPolicyMapOutProfileActionSetDscp,
                                 DS_STDDSCP, &Oid);
            nmhSetDiffServActionSpecific (u4ActionIndex, &Oid);
            nmhSetDiffServActionStatus (u4ActionIndex, ACTIVE);
            pPlyMapNode->u4OutProActionId = u4ActionIndex;

        }
    }

    /* if the RowStatus is ACTIVE then we have to Modify the Existing Policy 
     * otherwise it will be added during Policy ACTIVE */
    if ((pPlyMapNode->u1Status == ACTIVE) &&
        (gQoSGlobalInfo.eSysStatus == QOS_SYS_STATUS_ENABLE))
    {
        i4RetStatus =
            QoSHwWrUpdatePolicyMapForClass (pPlyMapNode,
                                            QOS_UPDATE_PLY_ACT_VIO_IPDSCP);
        if (i4RetStatus == QOS_FAILURE)
        {
            QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSHwWrUpdatePolicyMapForClass () "
                          "Returns FAILURE. \r\n", __FUNCTION__);

            pPlyMapNode->u1OutProActDscpOrTos = u1CurrVal;

            return (SNMP_FAILURE);
        }
    }

    QoSValidatePlyMapTblEntry (pPlyMapNode);

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhSetFsQoSPolicyMapOutProfileActionSetInnerVlanPrio
 Input       :  The Indices
                FsQoSPolicyMapId

                The Object 
                setValFsQoSPolicyMapOutProfileActionSetInnerVlanPrio
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
     
     
     
     
     
     
     
    nmhSetFsQoSPolicyMapOutProfileActionSetInnerVlanPrio
    (UINT4 u4FsQoSPolicyMapId,
     UINT4 u4SetValFsQoSPolicyMapOutProfileActionSetInnerVlanPrio)
{
    tQoSPolicyMapNode  *pPlyMapNode = NULL;
    INT4                i4RetStatus = QOS_FAILURE;
    UINT2               u2CurrVal = 0;

    pPlyMapNode = QoSUtlGetPolicyMapNode (u4FsQoSPolicyMapId);

    if (pPlyMapNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetPolicyMapNode () Returns "
                      "FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    u2CurrVal = pPlyMapNode->u2OutProActInnerVlanPrio;
    pPlyMapNode->u2OutProActInnerVlanPrio = (UINT2)
        u4SetValFsQoSPolicyMapOutProfileActionSetInnerVlanPrio;

    /* if the RowStatus is ACTIVE then we have to Modify the Existing Policy 
     * otherwise it will be added during Policy ACTIVE */
    if ((pPlyMapNode->u1Status == ACTIVE) &&
        (gQoSGlobalInfo.eSysStatus == QOS_SYS_STATUS_ENABLE))
    {
        i4RetStatus =
            QoSHwWrUpdatePolicyMapForClass
            (pPlyMapNode, QOS_UPDATE_PLY_ACT_VIO_INNER_VLAN_PRI);
        if (i4RetStatus == QOS_FAILURE)
        {
            QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSHwWrUpdatePolicyMapForClass () "
                          "Returns FAILURE. \r\n", __FUNCTION__);

            pPlyMapNode->u2OutProActInnerVlanPrio = u2CurrVal;

            return (SNMP_FAILURE);
        }
    }

    QoSValidatePlyMapTblEntry (pPlyMapNode);

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhSetFsQoSPolicyMapOutProfileActionSetVlanPrio
 Input       :  The Indices
                FsQoSPolicyMapId

                The Object 
                setValFsQoSPolicyMapOutProfileActionSetVlanPrio
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
     
     
     
     
     
     
     
    nmhSetFsQoSPolicyMapOutProfileActionSetVlanPrio
    (UINT4 u4FsQoSPolicyMapId,
     UINT4 u4SetValFsQoSPolicyMapOutProfileActionSetVlanPrio)
{

    tQoSPolicyMapNode  *pPlyMapNode = NULL;
    INT4                i4RetStatus = QOS_FAILURE;
    UINT1               u1CurrVal = 0;

    pPlyMapNode = QoSUtlGetPolicyMapNode (u4FsQoSPolicyMapId);

    if (pPlyMapNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetPolicyMapNode () Returns "
                      "FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    u1CurrVal = pPlyMapNode->u1OutProActVlanPrio;
    pPlyMapNode->u1OutProActVlanPrio = (UINT1)
        u4SetValFsQoSPolicyMapOutProfileActionSetVlanPrio;

    /* if the RowStatus is ACTIVE then we have to Modify the Existing Policy 
     * otherwise it will be added during Policy ACTIVE */
    if ((pPlyMapNode->u1Status == ACTIVE) &&
        (gQoSGlobalInfo.eSysStatus == QOS_SYS_STATUS_ENABLE))
    {
        i4RetStatus =
            QoSHwWrUpdatePolicyMapForClass (pPlyMapNode,
                                            QOS_UPDATE_PLY_ACT_VIO_VLAN_PRI);
        if (i4RetStatus == QOS_FAILURE)
        {
            QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSHwWrUpdatePolicyMapForClass () "
                          "Returns FAILURE. \r\n", __FUNCTION__);

            pPlyMapNode->u1OutProActVlanPrio = u1CurrVal;

            return (SNMP_FAILURE);
        }
    }

    QoSValidatePlyMapTblEntry (pPlyMapNode);

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhSetFsQoSPolicyMapOutProfileActionSetVlanDE
 Input       :  The Indices
                FsQoSPolicyMapId

                The Object 
                setValFsQoSPolicyMapOutProfileActionSetVlanDE
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
     
     
     
     
     
     
     
    nmhSetFsQoSPolicyMapOutProfileActionSetVlanDE
    (UINT4 u4FsQoSPolicyMapId,
     UINT4 u4SetValFsQoSPolicyMapOutProfileActionSetVlanDE)
{
    tQoSPolicyMapNode  *pPlyMapNode = NULL;
    INT4                i4RetStatus = QOS_FAILURE;
    UINT2               u2CurrVal = 0;

    pPlyMapNode = QoSUtlGetPolicyMapNode (u4FsQoSPolicyMapId);

    if (pPlyMapNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetPolicyMapNode () Returns "
                      "FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    u2CurrVal = pPlyMapNode->u2OutProActVlanDE;
    pPlyMapNode->u2OutProActVlanDE = (UINT2)
        u4SetValFsQoSPolicyMapOutProfileActionSetVlanDE;

    /* if the RowStatus is ACTIVE then we have to Modify the Existing Policy 
     * otherwise it will be added during Policy ACTIVE */
    if ((pPlyMapNode->u1Status == ACTIVE) &&
        (gQoSGlobalInfo.eSysStatus == QOS_SYS_STATUS_ENABLE))
    {
        i4RetStatus =
            QoSHwWrUpdatePolicyMapForClass (pPlyMapNode,
                                            QOS_UPDATE_PLY_ACT_VIO_VLAN_DE);
        if (i4RetStatus == QOS_FAILURE)
        {
            QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSHwWrUpdatePolicyMapForClass () "
                          "Returns FAILURE. \r\n", __FUNCTION__);

            pPlyMapNode->u2OutProActVlanDE = u2CurrVal;

            return (SNMP_FAILURE);
        }
    }

    QoSValidatePlyMapTblEntry (pPlyMapNode);

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhSetFsQoSPolicyMapOutProfileActionSetMplsExp
 Input       :  The Indices
                FsQoSPolicyMapId

                The Object 
                setValFsQoSPolicyMapOutProfileActionSetMplsExp
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
     
     
     
     
     
     
     
    nmhSetFsQoSPolicyMapOutProfileActionSetMplsExp
    (UINT4 u4FsQoSPolicyMapId,
     UINT4 u4SetValFsQoSPolicyMapOutProfileActionSetMplsExp)
{
    tQoSPolicyMapNode  *pPlyMapNode = NULL;
    INT4                i4RetStatus = QOS_FAILURE;
    UINT1               u1CurrVal = 0;

    pPlyMapNode = QoSUtlGetPolicyMapNode (u4FsQoSPolicyMapId);

    if (pPlyMapNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetPolicyMapNode () Returns "
                      "FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    u1CurrVal = pPlyMapNode->u1OutProActMplsExp;

    pPlyMapNode->u1OutProActMplsExp = (UINT1)
        u4SetValFsQoSPolicyMapOutProfileActionSetMplsExp;

    /* if the RowStatus is ACTIVE then we have to Modify the Existing Policy 
     * otherwise it will be added during Policy ACTIVE */
    if ((pPlyMapNode->u1Status == ACTIVE) &&
        (gQoSGlobalInfo.eSysStatus == QOS_SYS_STATUS_ENABLE))
    {
        i4RetStatus =
            QoSHwWrUpdatePolicyMapForClass (pPlyMapNode,
                                            QOS_UPDATE_PLY_ACT_VIO_MPLS_EXP);
        if (i4RetStatus == QOS_FAILURE)
        {
            QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSHwWrUpdatePolicyMapForClass () "
                          "Returns FAILURE. \r\n", __FUNCTION__);

            pPlyMapNode->u1OutProActVlanPrio = u1CurrVal;

            return (SNMP_FAILURE);
        }
    }

    QoSValidatePlyMapTblEntry (pPlyMapNode);

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhSetFsQoSPolicyMapOutProfileActionSetNewCLASS
 Input       :  The Indices
                FsQoSPolicyMapId

                The Object 
                setValFsQoSPolicyMapOutProfileActionSetNewCLASS
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
     
     
     
     
     
     
     
    nmhSetFsQoSPolicyMapOutProfileActionSetNewCLASS
    (UINT4 u4FsQoSPolicyMapId,
     UINT4 u4SetValFsQoSPolicyMapOutProfileActionSetNewCLASS)
{
    tQoSPolicyMapNode  *pPlyMapNode = NULL;

    pPlyMapNode = QoSUtlGetPolicyMapNode (u4FsQoSPolicyMapId);

    if (pPlyMapNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetPolicyMapNode () Returns "
                      "FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    pPlyMapNode->u4OutProActNewClassId =
        u4SetValFsQoSPolicyMapOutProfileActionSetNewCLASS;

    QoSValidatePlyMapTblEntry (pPlyMapNode);

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhSetFsQoSPolicyMapStatus
 Input       :  The Indices
                FsQoSPolicyMapId

                The Object 
                setValFsQoSPolicyMapStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsQoSPolicyMapStatus (UINT4 u4FsQoSPolicyMapId,
                            INT4 i4SetValFsQoSPolicyMapStatus)
{
    tQoSPolicyMapNode  *pPlyMapNode = NULL;
    tQoSMeterNode      *pMeterNode = NULL;
    INT4                i4RetStatus = QOS_FAILURE;
    INT4                i4RowStatus = QOS_FAILURE;
    INT4                i4ReturnValue = SNMP_SUCCESS;

    pPlyMapNode = QoSUtlGetPolicyMapNode (u4FsQoSPolicyMapId);

    if (pPlyMapNode != NULL)
    {
        i4RowStatus = (UINT4) pPlyMapNode->u1Status;

        /* Entry found in the Table */
        if (i4RowStatus == i4SetValFsQoSPolicyMapStatus)
        {
            /* same value */
            return (SNMP_SUCCESS);
        }
    }
    else
    {
        /* New Entry need to be created in the Table, only if 
         *  i4SetValFsQoSPolicyMapStatus = CREATE_AND_WAIT */
        /* Optional */

        if (i4SetValFsQoSPolicyMapStatus == DESTROY)
        {
            return (SNMP_SUCCESS);
        }

        if (i4SetValFsQoSPolicyMapStatus != CREATE_AND_WAIT)
        {
            return SNMP_FAILURE;
        }

    }

    /*Meter stats needs to be removed for the policer that is going to be destroyed */
    if ((pPlyMapNode != NULL) && (i4SetValFsQoSPolicyMapStatus == DESTROY))
    {
        if (pPlyMapNode->u1StatsEnabled == QOS_TRUE)
        {
            pMeterNode = QoSUtlGetMeterNode (pPlyMapNode->u4MeterTableId);
            if (pMeterNode != NULL)
            {
                /* Meter Statistics has been made as disiable for control plane level.
                   In the H/W stats removel was taken care in porting layer. */
                pMeterNode->u1StatsStatus = QOS_STATS_DISABLE;
            }
        }
    }
    switch (i4SetValFsQoSPolicyMapStatus)
    {
            /* For a New Entry */
        case CREATE_AND_WAIT:

            pPlyMapNode = QoSCreatePlyMapTblEntry (u4FsQoSPolicyMapId);

            if (pPlyMapNode == NULL)
            {
                return (SNMP_FAILURE);
            }

            pPlyMapNode->u1Status = NOT_IN_SERVICE;

            break;

            /* Modify an Entry */
        case ACTIVE:

            if (gQoSGlobalInfo.eSysStatus == QOS_SYS_STATUS_ENABLE)
            {
                /* 1. Add the entry into:the HARDWARE. */
                i4RetStatus = QoSHwWrMapClassToPolicy (pPlyMapNode, NULL,
                                                       QOS_PLY_ADD);
                if (i4RetStatus == QOS_FAILURE)
                {
                    QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSHwWrMapClassToPolicy "
                                  " () Returns FAILURE. \r\n", __FUNCTION__);

                    return (SNMP_FAILURE);
                }

#ifdef WLC_WANTED
                /*Code changes for WSS to get in-priority and ifIndex */
                if (QosWssWrMapClassToPolicy
                    (pPlyMapNode, i4SetValFsQoSPolicyMapStatus) == QOS_FAILURE)
                {
                    QOS_TRC_ARG1 (MGMT_TRC, "In %s : QosWssWrMapClassToPolicy "
                                  " () Returns FAILURE. \r\n", __FUNCTION__);
                }
#endif
            }

            pPlyMapNode->u1Status = ACTIVE;

            break;

        case NOT_IN_SERVICE:

            if ((pPlyMapNode->u1Status == ACTIVE) &&
                (gQoSGlobalInfo.eSysStatus == QOS_SYS_STATUS_ENABLE))
            {
#ifdef WLC_WANTED
                /*Code changes for WSS to get in-priority and ifIndex */
                if (QosWssWrMapClassToPolicy
                    (pPlyMapNode, i4SetValFsQoSPolicyMapStatus) == QOS_FAILURE)
                {
                    QOS_TRC_ARG1 (MGMT_TRC, "In %s : QosWssWrMapClassToPolicy "
                                  " () Returns FAILURE. \r\n", __FUNCTION__);
                }
#endif
                /* 1. Remove the entry from the HARDWARE. */
                i4RetStatus = QoSHwWrUnmapClassFromPolicy (pPlyMapNode, NULL,
                                                           QOS_PLY_DEL);
                if (i4RetStatus == QOS_FAILURE)
                {
                    QOS_TRC_ARG1 (MGMT_TRC, "In %s : "
                                  "QoSHwWrUnmapClassFromPolicy () Returns"
                                  " FAILURE. \r\n", __FUNCTION__);

                    return (SNMP_FAILURE);
                }

                /*Delete All Class Map Entries in the HW */
                if (pPlyMapNode->u4ClassId != 0)
                {
                    i4RetStatus = QoSHwWrUtilDeleteAllClsMapEntry (pPlyMapNode->
                                                                   u4ClassId);
                    if (i4RetStatus == QOS_FAILURE)
                    {
                        QOS_TRC_ARG1 (MGMT_TRC, "In %s : "
                                      "QoSHwWrUtilDeleteAllClsMapEntry () "
                                      "Returns FAILURE. \r\n", __FUNCTION__);
                        return (SNMP_FAILURE);
                    }
                }
            }

            pPlyMapNode->u1Status = NOT_IN_SERVICE;

            break;

        case DESTROY:

            if ((pPlyMapNode->u1Status == ACTIVE) &&
                (gQoSGlobalInfo.eSysStatus == QOS_SYS_STATUS_ENABLE))
            {
#ifdef WLC_WANTED
                /*Code changes for WSS to get in-priority and ifIndex */
                if (QosWssWrMapClassToPolicy
                    (pPlyMapNode, i4SetValFsQoSPolicyMapStatus) == QOS_FAILURE)
                {
                    QOS_TRC_ARG1 (MGMT_TRC, "In %s : QosWssWrMapClassToPolicy "
                                  " () Returns FAILURE. \r\n", __FUNCTION__);
                }
#endif
                /* 1. Remove the entry from the HARDWARE. */
                i4RetStatus = QoSHwWrUnmapClassFromPolicy (pPlyMapNode, NULL,
                                                           QOS_PLY_DEL);
                if (i4RetStatus == QOS_FAILURE)
                {
                    QOS_TRC_ARG1 (MGMT_TRC, "In %s :"
                                  "QoSHwWrUnmapClassFromPolicy () Returns "
                                  "FAILURE. \r\n", __FUNCTION__);

                    return (SNMP_FAILURE);
                }

                /*Delete All Class Map Entries in the HW */
                if (pPlyMapNode->u4ClassId != 0)
                {
                    i4RetStatus = QoSHwWrUtilDeleteAllClsMapEntry (pPlyMapNode->
                                                                   u4ClassId);
                    if (i4RetStatus == QOS_FAILURE)
                    {
                        QOS_TRC_ARG1 (MGMT_TRC, "In %s : "
                                      "QoSHwWrUtilDeleteAllClsMapEntry () "
                                      "Returns FAILURE. \r\n", __FUNCTION__);
                        return (SNMP_FAILURE);
                    }
                }
            }

            /* 2. Remove the entry from the Internal Reference. */
            i4RetStatus = QoSClsInfoDelPolicy (pPlyMapNode);
            if (i4RetStatus == QOS_FAILURE)
            {
                QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSClsInfoDelPolicy () "
                              "Returns FAILURE. \r\n", __FUNCTION__);

                return SNMP_FAILURE;
            }

            /* 3. Remove the entry from the SOFTWARE. */
            i4RetStatus = QoSDeletePlyMapTblEntry (pPlyMapNode);
            if (i4RetStatus == QOS_FAILURE)
            {
                return SNMP_FAILURE;
            }

            break;

            /* Not Supported */
        case CREATE_AND_GO:
            i4ReturnValue = SNMP_FAILURE;
            break;

        default:
            i4ReturnValue = SNMP_FAILURE;
            break;
    }
    if (i4ReturnValue == SNMP_SUCCESS)
    {
        QosUpdateDiffServMeterNextFree (u4FsQoSPolicyMapId,
                                        i4SetValFsQoSPolicyMapStatus);
    }

    return ((INT1) i4ReturnValue);
}

/****************************************************************************
 Function    :  nmhSetFsQoSPolicyMapConformActionSetInnerVlanDE
 Input       :  The Indices
                FsQoSPolicyMapId

                The Object
                setValFsQoSPolicyMapConformActionSetInnerVlanDE
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsQoSPolicyMapConformActionSetInnerVlanDE (UINT4 u4FsQoSPolicyMapId,
                                                 UINT4
                                                 u4SetValFsQoSPolicyMapConformActionSetInnerVlanDE)
{
    tQoSPolicyMapNode  *pPlyMapNode = NULL;
    INT4                i4RetStatus = QOS_FAILURE;
    UINT2               u2CurrVal = 0;

    pPlyMapNode = QoSUtlGetPolicyMapNode (u4FsQoSPolicyMapId);

    if (pPlyMapNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetPolicyMapNode () Returns "
                      "FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    u2CurrVal = pPlyMapNode->u2InProActConfInnerVlanDE;
    pPlyMapNode->u2InProActConfInnerVlanDE = (UINT2)
        u4SetValFsQoSPolicyMapConformActionSetInnerVlanDE;

    /* if the RowStatus is ACTIVE then we have to Modify the Existing Policy 
     * otherwise it will be added during Policy ACTIVE */
    if ((pPlyMapNode->u1Status == ACTIVE) &&
        (gQoSGlobalInfo.eSysStatus == QOS_SYS_STATUS_ENABLE))
    {
        i4RetStatus =
            QoSHwWrUpdatePolicyMapForClass
            (pPlyMapNode, QOS_UPDATE_PLY_ACT_CONF_INNER_VLAN_DE);
        if (i4RetStatus == QOS_FAILURE)
        {
            QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSHwWrUpdatePolicyMapForClass () "
                          "Returns FAILURE. \r\n", __FUNCTION__);

            pPlyMapNode->u2InProActConfInnerVlanDE = u2CurrVal;

            return (SNMP_FAILURE);
        }
    }

    QoSValidatePlyMapTblEntry (pPlyMapNode);

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhSetFsQoSPolicyMapExceedActionSetInnerVlanDE
 Input       :  The Indices
                FsQoSPolicyMapId

                The Object
                setValFsQoSPolicyMapExceedActionSetInnerVlanDE
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsQoSPolicyMapExceedActionSetInnerVlanDE (UINT4 u4FsQoSPolicyMapId,
                                                UINT4
                                                u4SetValFsQoSPolicyMapExceedActionSetInnerVlanDE)
{
    tQoSPolicyMapNode  *pPlyMapNode = NULL;
    INT4                i4RetStatus = QOS_FAILURE;
    UINT2               u2CurrVal = 0;

    pPlyMapNode = QoSUtlGetPolicyMapNode (u4FsQoSPolicyMapId);

    if (pPlyMapNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetPolicyMapNode () Returns "
                      "FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    u2CurrVal = pPlyMapNode->u2InProActExceedInnerVlanDE;
    pPlyMapNode->u2InProActExceedInnerVlanDE = (UINT2)
        u4SetValFsQoSPolicyMapExceedActionSetInnerVlanDE;

    /* if the RowStatus is ACTIVE then we have to Modify the Existing Policy 
     * otherwise it will be added during Policy ACTIVE */
    if ((pPlyMapNode->u1Status == ACTIVE) &&
        (gQoSGlobalInfo.eSysStatus == QOS_SYS_STATUS_ENABLE))
    {
        i4RetStatus =
            QoSHwWrUpdatePolicyMapForClass
            (pPlyMapNode, QOS_UPDATE_PLY_ACT_EXC_INNER_VLAN_DE);
        if (i4RetStatus == QOS_FAILURE)
        {
            QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSHwWrUpdatePolicyMapForClass () "
                          "Returns FAILURE. \r\n", __FUNCTION__);

            pPlyMapNode->u2InProActExceedInnerVlanPrio = u2CurrVal;

            return (SNMP_FAILURE);
        }
    }

    QoSValidatePlyMapTblEntry (pPlyMapNode);

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhSetFsQoSPolicyMapOutProfileActionSetInnerVlanDE
 Input       :  The Indices
                FsQoSPolicyMapId

                The Object
                setValFsQoSPolicyMapOutProfileActionSetInnerVlanDE
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsQoSPolicyMapOutProfileActionSetInnerVlanDE (UINT4 u4FsQoSPolicyMapId,
                                                    UINT4
                                                    u4SetValFsQoSPolicyMapOutProfileActionSetInnerVlanDE)
{
    tQoSPolicyMapNode  *pPlyMapNode = NULL;
    INT4                i4RetStatus = QOS_FAILURE;
    UINT2               u2CurrVal = 0;

    pPlyMapNode = QoSUtlGetPolicyMapNode (u4FsQoSPolicyMapId);

    if (pPlyMapNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetPolicyMapNode () Returns "
                      "FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    u2CurrVal = pPlyMapNode->u2OutProActInnerVlanDE;
    pPlyMapNode->u2OutProActInnerVlanDE = (UINT2)
        u4SetValFsQoSPolicyMapOutProfileActionSetInnerVlanDE;

    /* if the RowStatus is ACTIVE then we have to Modify the Existing Policy 
     * otherwise it will be added during Policy ACTIVE */
    if ((pPlyMapNode->u1Status == ACTIVE) &&
        (gQoSGlobalInfo.eSysStatus == QOS_SYS_STATUS_ENABLE))
    {
        i4RetStatus =
            QoSHwWrUpdatePolicyMapForClass
            (pPlyMapNode, QOS_UPDATE_PLY_ACT_VIO_INNER_VLAN_DE);
        if (i4RetStatus == QOS_FAILURE)
        {
            QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSHwWrUpdatePolicyMapForClass () "
                          "Returns FAILURE. \r\n", __FUNCTION__);

            pPlyMapNode->u2OutProActInnerVlanDE = u2CurrVal;

            return (SNMP_FAILURE);
        }
    }

    QoSValidatePlyMapTblEntry (pPlyMapNode);

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhSetFsQoSPolicyDefaultVlanDE
 Input       :  The Indices
                FsQoSPolicyMapId   
                                                                                                                                                                       The Object
                setValFsQoSPolicyDefaultVlanDE
 Output      :  The Set Low Lev Routine Take the Indices &       
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
***************************************************************************/
INT1
nmhSetFsQoSPolicyDefaultVlanDE (UINT4 u4FsQoSPolicyMapId,
                                INT4 i4SetValFsQoSPolicyDefaultVlanDE)
{
    tQoSPolicyMapNode  *pPlyMapNode = NULL;
    INT4                i4RetStatus = QOS_FAILURE;
    INT4                i4CurrVal = 0;

    pPlyMapNode = QoSUtlGetPolicyMapNode (u4FsQoSPolicyMapId);

    if (pPlyMapNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetPolicyMapNode () Returns "
                      "FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    i4CurrVal = pPlyMapNode->i4VlanDE;

    if (i4SetValFsQoSPolicyDefaultVlanDE != 0)
    {
        i4RetStatus = QosUtlValidateDEValue (pPlyMapNode);
        if (i4RetStatus != QOS_SUCCESS)
        {
            QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlValidateDEValue () Returns "
                          "FAILURE. \r\n", __FUNCTION__);

            CLI_SET_ERR (QOS_CLI_ERR_MAX_DE_CONFIGURED);
            return (SNMP_FAILURE);

        }
    }
    pPlyMapNode->i4VlanDE = i4SetValFsQoSPolicyDefaultVlanDE;

    /* if the RowStatus is ACTIVE then we have to Modify the Existing Policy 
     * otherwise it will be added during Policy ACTIVE */
    if ((pPlyMapNode->u1Status == ACTIVE) &&
        (gQoSGlobalInfo.eSysStatus == QOS_SYS_STATUS_ENABLE))
    {
        i4RetStatus =
            QoSHwWrUpdatePolicyMapForClass
            (pPlyMapNode, QOS_UPDATE_PLY_DEFAULT_VLAN_DE);
        if (i4RetStatus == QOS_FAILURE)
        {
            QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSHwWrUpdatePolicyMapForClass () "
                          "Returns FAILURE. \r\n", __FUNCTION__);

            pPlyMapNode->i4VlanDE = i4CurrVal;

            return (SNMP_FAILURE);
        }
    }

    QoSValidatePlyMapTblEntry (pPlyMapNode);

    return (SNMP_SUCCESS);
}

/* Low Level TEST Routines for All Objects  */
/****************************************************************************
 Function    :  nmhTestv2FsQoSPolicyMapName
 Input       :  The Indices
                FsQoSPolicyMapId

                The Object 
                testValFsQoSPolicyMapName
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsQoSPolicyMapName (UINT4 *pu4ErrorCode,
                             UINT4 u4FsQoSPolicyMapId,
                             tSNMP_OCTET_STRING_TYPE *
                             pTestValFsQoSPolicyMapName)
{
    tQoSPolicyMapNode  *pPlyMapNode = NULL;
    INT4                i4RetStatus = QOS_FAILURE;

    *pu4ErrorCode = SNMP_ERR_NO_ERROR;

    /* Check Index */
    i4RetStatus = QoSUtlValidatePolicyTblIdxInst (u4FsQoSPolicyMapId,
                                                  pu4ErrorCode);
    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlValidatePolicyTblIdxInst () "
                      "Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    /* Check  Object's Value */
    i4RetStatus = QoSUtlValidateTableName (pTestValFsQoSPolicyMapName,
                                           pu4ErrorCode);
    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlValidateTableName () "
                      "Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    pPlyMapNode = QoSUtlGetPolicyMapNode (u4FsQoSPolicyMapId);
    if (pPlyMapNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetPolicyMapNode () Returns "
                      "FAILURE. \r\n", __FUNCTION__);

        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (QOS_CLI_ERR_PLY_MAP_NO_ENTRY);
        return (SNMP_FAILURE);
    }

    /* Check the Name is same as Current Name */
    if ((STRLEN (pPlyMapNode->au1Name)) == (UINT4)
        pTestValFsQoSPolicyMapName->i4_Length)
    {
        if ((STRNCMP (pPlyMapNode->au1Name,
                      pTestValFsQoSPolicyMapName->pu1_OctetList,
                      pTestValFsQoSPolicyMapName->i4_Length)) == 0)
        {
            return (SNMP_SUCCESS);
        }
    }

    /* Check the name is Unique or Not */
    i4RetStatus = QoSUtlIsUniqueName (pTestValFsQoSPolicyMapName,
                                      QOS_TBL_TYPE_PLY_MAP);
    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlIsUniqueName () Returns "
                      "FAILURE. \r\n", __FUNCTION__);

        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return (SNMP_FAILURE);
    }

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhTestv2FsQoSPolicyMapIfIndex
 Input       :  The Indices
                FsQoSPolicyMapId

                The Object 
                testValFsQoSPolicyMapIfIndex
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsQoSPolicyMapIfIndex (UINT4 *pu4ErrorCode,
                                UINT4 u4FsQoSPolicyMapId,
                                UINT4 u4TestValFsQoSPolicyMapIfIndex)
{
    INT4                i4RetStatus = QOS_FAILURE;

    *pu4ErrorCode = SNMP_ERR_NO_ERROR;

    /* Check Index */
    i4RetStatus = QoSUtlValidatePolicyTblIdxInst (u4FsQoSPolicyMapId,
                                                  pu4ErrorCode);
    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlValidatePolicyTblIdxInst () "
                      "Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    if (u4TestValFsQoSPolicyMapIfIndex == 0)
    {
        /* Reset the IfIndex */
        return (SNMP_SUCCESS);
    }

    /* Check  Object's Value */
    i4RetStatus = QoSUtlValidateIfIndex (u4TestValFsQoSPolicyMapIfIndex,
                                         pu4ErrorCode);
    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlValidateIfIndex () "
                      "Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhTestv2FsQoSPolicyMapCLASS
 Input       :  The Indices
                FsQoSPolicyMapId

                The Object 
                testValFsQoSPolicyMapCLASS
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsQoSPolicyMapCLASS (UINT4 *pu4ErrorCode, UINT4 u4FsQoSPolicyMapId,
                              UINT4 u4TestValFsQoSPolicyMapCLASS)
{
    INT4                i4RetStatus = QOS_FAILURE;

    *pu4ErrorCode = SNMP_ERR_NO_ERROR;

    /* Check Index */
    i4RetStatus = QoSUtlValidatePolicyTblIdxInst (u4FsQoSPolicyMapId,
                                                  pu4ErrorCode);

    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlValidatePolicyTblIdxInst () "
                      "Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    if (u4TestValFsQoSPolicyMapCLASS == 0)
    {
        /* Reset the CLASS */
        return (SNMP_SUCCESS);
    }

    /* Check  Object's Value */
    i4RetStatus = QoSUtlValidateClass (u4TestValFsQoSPolicyMapCLASS,
                                       pu4ErrorCode);
    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlValidateClass () "
                      "Returns FAILURE. \r\n", __FUNCTION__);
        return (SNMP_FAILURE);
    }

    /*The CLASS and Policy one to one map */
    i4RetStatus = QoSTestClsInfoAddPolicy (u4TestValFsQoSPolicyMapCLASS,
                                           u4FsQoSPolicyMapId, pu4ErrorCode);
    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSTestClsInfoAddPolicy () "
                      "Returns FAILURE. \r\n", __FUNCTION__);
        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhTestv2FsQoSPolicyMapPHBType
 Input       :  The Indices
                FsQoSPolicyMapId

                The Object 
                testValFsQoSPolicyMapPHBType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsQoSPolicyMapPHBType (UINT4 *pu4ErrorCode,
                                UINT4 u4FsQoSPolicyMapId,
                                INT4 i4TestValFsQoSPolicyMapPHBType)
{
    INT4                i4RetStatus = QOS_FAILURE;

    *pu4ErrorCode = SNMP_ERR_NO_ERROR;

    /* Check Index */
    i4RetStatus = QoSUtlValidatePolicyTblIdxInst (u4FsQoSPolicyMapId,
                                                  pu4ErrorCode);
    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlValidatePolicyTblIdxInst () "
                      "Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    /* Check  Object's Value */

    if ((i4TestValFsQoSPolicyMapPHBType < QOS_POLICY_PHB_TYPE_MIN_VAL) ||
        (i4TestValFsQoSPolicyMapPHBType > QOS_POLICY_PHB_TYPE_MAX_VAL))
    {
        QOS_TRC_ARG3 (MGMT_TRC, "%s :Policy PHBType value is out of range. The"
                      " range should be Min %d - Max %d. \r\n", __FUNCTION__,
                      QOS_POLICY_PHB_TYPE_MIN_VAL, QOS_POLICY_PHB_TYPE_MAX_VAL);

        CLI_SET_ERR (QOS_CLI_ERR_PHB_TYPE_INVALID);

        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;

        return (SNMP_FAILURE);
    }

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhTestv2FsQoSPolicyMapDefaultPHB
 Input       :  The Indices
                FsQoSPolicyMapId

                The Object 
                testValFsQoSPolicyMapDefaultPHB
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsQoSPolicyMapDefaultPHB (UINT4 *pu4ErrorCode,
                                   UINT4 u4FsQoSPolicyMapId,
                                   UINT4 u4TestValFsQoSPolicyMapDefaultPHB)
{
    tQoSPolicyMapNode  *pPlyMapNode = NULL;
    INT4                i4RetStatus = QOS_FAILURE;
    UINT4               u4DefaultPHB = 0;
    INT4                i4ReturnValue = SNMP_SUCCESS;

    *pu4ErrorCode = SNMP_ERR_NO_ERROR;

    /* Check Index */
    i4RetStatus = QoSUtlValidatePolicyTblIdxInst (u4FsQoSPolicyMapId,
                                                  pu4ErrorCode);
    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlValidatePolicyTblIdxInst () "
                      "Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    /* Check  Object's Value */
    pPlyMapNode = QoSUtlGetPolicyMapNode (u4FsQoSPolicyMapId);
    if (pPlyMapNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetPolicyMapNode () Returns "
                      "FAILURE. \r\n", __FUNCTION__);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (QOS_CLI_ERR_PLY_MAP_NO_ENTRY);
        return (SNMP_FAILURE);
    }

    u4DefaultPHB = u4TestValFsQoSPolicyMapDefaultPHB;

    /* check the range of the Priority vlaue based on the priority type */
    if (QoSUtlValidatePriority (pPlyMapNode->u1PHBType, u4DefaultPHB)
        != QOS_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        i4ReturnValue = SNMP_FAILURE;
    }

    return ((INT1) i4ReturnValue);
}

/****************************************************************************
 Function    :  nmhTestv2FsQoSPolicyMapMeterTableId
 Input       :  The Indices
                FsQoSPolicyMapId

                The Object 
                testValFsQoSPolicyMapMeterTableId
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsQoSPolicyMapMeterTableId (UINT4 *pu4ErrorCode,
                                     UINT4 u4FsQoSPolicyMapId,
                                     UINT4 u4TestValFsQoSPolicyMapMeterTableId)
{
    INT4                i4RetStatus = QOS_FAILURE;

    *pu4ErrorCode = SNMP_ERR_NO_ERROR;

    /* Check Index */
    i4RetStatus = QoSUtlValidatePolicyTblIdxInst (u4FsQoSPolicyMapId,
                                                  pu4ErrorCode);
    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlValidatePolicyTblIdxInst () "
                      "Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    /* Reset Meter Id */
    if (u4TestValFsQoSPolicyMapMeterTableId == 0)
    {
        return (SNMP_SUCCESS);
    }

    /* Check  Object's Value */
    i4RetStatus = QoSUtlValidateMeter (u4TestValFsQoSPolicyMapMeterTableId,
                                       pu4ErrorCode);
    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlValidateMeter () "
                      "Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhTestv2FsQoSPolicyMapFailMeterTableId
 Input       :  The Indices
                FsQoSPolicyMapId

                The Object 
                testValFsQoSPolicyMapFailMeterTableId
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsQoSPolicyMapFailMeterTableId (UINT4 *pu4ErrorCode,
                                         UINT4 u4FsQoSPolicyMapId,
                                         UINT4
                                         u4TestValFsQoSPolicyMapFailMeterTableId)
{
    INT4                i4RetStatus = QOS_FAILURE;

    *pu4ErrorCode = SNMP_ERR_NO_ERROR;

    /* Check Index */
    i4RetStatus = QoSUtlValidatePolicyTblIdxInst (u4FsQoSPolicyMapId,
                                                  pu4ErrorCode);
    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlValidatePolicyTblIdxInst () "
                      "Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    /* Reset Meter Id */
    if (u4TestValFsQoSPolicyMapFailMeterTableId == 0)
    {
        return (SNMP_SUCCESS);
    }

    /* Check  Object's Value */
    i4RetStatus = QoSUtlValidateMeter (u4TestValFsQoSPolicyMapFailMeterTableId,
                                       pu4ErrorCode);
    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlValidateMeter () "
                      "Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhTestv2FsQoSPolicyMapInProfileConformActionFlag
 Input       :  The Indices
                FsQoSPolicyMapId

                The Object 
                testValFsQoSPolicyMapInProfileConformActionFlag
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
     
     
     
     
     
     
     
    nmhTestv2FsQoSPolicyMapInProfileConformActionFlag
    (UINT4 *pu4ErrorCode,
     UINT4 u4FsQoSPolicyMapId,
     tSNMP_OCTET_STRING_TYPE * pTestValFsQoSPolicyMapInProfileConformActionFlag)
{
    INT4                i4RetStatus = QOS_FAILURE;

    *pu4ErrorCode = SNMP_ERR_NO_ERROR;

    /* Check Index */
    i4RetStatus = QoSUtlValidatePolicyTblIdxInst (u4FsQoSPolicyMapId,
                                                  pu4ErrorCode);
    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlValidatePolicyTblIdxInst () "
                      "Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    /* Check  Object's Value */
    i4RetStatus = QoSValidateInProConfActFlag
        (u4FsQoSPolicyMapId,
         pTestValFsQoSPolicyMapInProfileConformActionFlag, pu4ErrorCode);

    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSValidateInProConfActFlag () "
                      "Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhTestv2FsQoSPolicyMapInProfileConformActionId
 Input       :  The Indices
                FsQoSPolicyMapId

                The Object 
                testValFsQoSPolicyMapInProfileConformActionId
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsQoSPolicyMapInProfileConformActionId (UINT4 *pu4ErrorCode,
                                                 UINT4 u4FsQoSPolicyMapId,
                                                 UINT4
                                                 u4TestValFsQoSPolicyMapInProfileConformActionId)
{
    INT4                i4RetStatus = QOS_FAILURE;

    *pu4ErrorCode = SNMP_ERR_NO_ERROR;

    /* Check Index */
    i4RetStatus = QoSUtlValidatePolicyTblIdxInst (u4FsQoSPolicyMapId,
                                                  pu4ErrorCode);
    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlValidatePolicyTblIdxInst () "
                      "Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    /* Check  Object's Value */
    if (QosValidateIndex
        (u4TestValFsQoSPolicyMapInProfileConformActionId,
         DS_ACTION) == QOS_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return (SNMP_FAILURE);
    }

    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhTestv2FsQoSPolicyMapInProfileActionSetPort
 Input       :  The Indices
                FsQoSPolicyMapId

                The Object 
                testValFsQoSPolicyMapInProfileActionSetPort
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
     
     
     
     
     
     
     
    nmhTestv2FsQoSPolicyMapInProfileActionSetPort
    (UINT4 *pu4ErrorCode,
     UINT4 u4FsQoSPolicyMapId,
     UINT4 u4TestValFsQoSPolicyMapInProfileActionSetPort)
{
    INT4                i4RetStatus = QOS_FAILURE;

    *pu4ErrorCode = SNMP_ERR_NO_ERROR;

    /* Check Index */
    i4RetStatus = QoSUtlValidatePolicyTblIdxInst (u4FsQoSPolicyMapId,
                                                  pu4ErrorCode);
    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlValidatePolicyTblIdxInst () "
                      "Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    /* Check  Object's Value */
    i4RetStatus =
        QoSUtlValidateIfIndex (u4TestValFsQoSPolicyMapInProfileActionSetPort,
                               pu4ErrorCode);
    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlValidateIfIndex () "
                      "Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhTestv2FsQoSPolicyMapConformActionSetIpTOS
 Input       :  The Indices
                FsQoSPolicyMapId

                The Object 
                testValFsQoSPolicyMapConformActionSetIpTOS
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
     
     
     
     
     
     
     
    nmhTestv2FsQoSPolicyMapConformActionSetIpTOS
    (UINT4 *pu4ErrorCode,
     UINT4 u4FsQoSPolicyMapId,
     UINT4 u4TestValFsQoSPolicyMapConformActionSetIpTOS)
{
    INT4                i4RetStatus = QOS_FAILURE;

    *pu4ErrorCode = SNMP_ERR_NO_ERROR;

    /* Check Index */
    i4RetStatus = QoSUtlValidatePolicyTblIdxInst (u4FsQoSPolicyMapId,
                                                  pu4ErrorCode);
    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlValidatePolicyTblIdxInst () "
                      "Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    /* Check  Object's Value */
    i4RetStatus = QoSValidateInProConfAction
        (u4FsQoSPolicyMapId,
         u4TestValFsQoSPolicyMapConformActionSetIpTOS,
         QOS_PLY_IN_PROF_CONF_IP_TOS, pu4ErrorCode);

    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSValidateInProConfAction () "
                      "Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhTestv2FsQoSPolicyMapConformActionSetDscp
 Input       :  The Indices
                FsQoSPolicyMapId

                The Object 
                testValFsQoSPolicyMapConformActionSetDscp
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
     
     
     
     
     
     
     
    nmhTestv2FsQoSPolicyMapConformActionSetDscp
    (UINT4 *pu4ErrorCode,
     UINT4 u4FsQoSPolicyMapId, INT4 i4TestValFsQoSPolicyMapConformActionSetDscp)
{
    INT4                i4RetStatus = QOS_FAILURE;

    *pu4ErrorCode = SNMP_ERR_NO_ERROR;

    /* Check Index */
    i4RetStatus = QoSUtlValidatePolicyTblIdxInst (u4FsQoSPolicyMapId,
                                                  pu4ErrorCode);
    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlValidatePolicyTblIdxInst () "
                      "Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    /* Check  Object's Value */
    i4RetStatus = QoSValidateInProConfAction
        (u4FsQoSPolicyMapId,
         i4TestValFsQoSPolicyMapConformActionSetDscp,
         QOS_PLY_IN_PROF_CONF_IP_DSCP, pu4ErrorCode);

    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSValidateInProConfAction () "
                      "Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhTestv2FsQoSPolicyMapConformActionSetVlanPrio
 Input       :  The Indices
                FsQoSPolicyMapId

                The Object 
                testValFsQoSPolicyMapConformActionSetVlanPrio
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
     
     
     
     
     
     
     
    nmhTestv2FsQoSPolicyMapConformActionSetVlanPrio
    (UINT4 *pu4ErrorCode,
     UINT4 u4FsQoSPolicyMapId,
     UINT4 u4TestValFsQoSPolicyMapConformActionSetVlanPrio)
{
    INT4                i4RetStatus = QOS_FAILURE;

    *pu4ErrorCode = SNMP_ERR_NO_ERROR;

    /* Check Index */
    i4RetStatus = QoSUtlValidatePolicyTblIdxInst (u4FsQoSPolicyMapId,
                                                  pu4ErrorCode);
    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlValidatePolicyTblIdxInst () "
                      "Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    /* Check  Object's Value */
    i4RetStatus = QoSValidateInProConfAction
        (u4FsQoSPolicyMapId,
         u4TestValFsQoSPolicyMapConformActionSetVlanPrio,
         QOS_PLY_IN_PROF_CONF_VLAN_PRI, pu4ErrorCode);

    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSValidateInProConfAction () "
                      "Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhTestv2FsQoSPolicyMapConformActionSetVlanDE
 Input       :  The Indices
                FsQoSPolicyMapId

                The Object 
                testValFsQoSPolicyMapConformActionSetVlanDE
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
     
     
     
     
     
     
     
    nmhTestv2FsQoSPolicyMapConformActionSetVlanDE
    (UINT4 *pu4ErrorCode,
     UINT4 u4FsQoSPolicyMapId,
     UINT4 u4TestValFsQoSPolicyMapConformActionSetVlanDE)
{
    INT4                i4RetStatus = QOS_FAILURE;

    *pu4ErrorCode = SNMP_ERR_NO_ERROR;

    /* Check Index */
    i4RetStatus = QoSUtlValidatePolicyTblIdxInst (u4FsQoSPolicyMapId,
                                                  pu4ErrorCode);
    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlValidatePolicyTblIdxInst () "
                      "Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    /* Check  Object's Value */
    i4RetStatus = QoSValidateInProConfAction
        (u4FsQoSPolicyMapId,
         u4TestValFsQoSPolicyMapConformActionSetVlanDE,
         QOS_PLY_IN_PROF_CONF_VLAN_DE, pu4ErrorCode);

    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSValidateInProConfAction () "
                      "Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhTestv2FsQoSPolicyMapConformActionSetInnerVlanPrio
 Input       :  The Indices
                FsQoSPolicyMapId

                The Object 
                testValFsQoSPolicyMapConformActionSetInnerVlanPrio
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
     
     
     
     
     
     
     
    nmhTestv2FsQoSPolicyMapConformActionSetInnerVlanPrio
    (UINT4 *pu4ErrorCode,
     UINT4 u4FsQoSPolicyMapId,
     UINT4 u4TestValFsQoSPolicyMapConformActionSetInnerVlanPrio)
{
    INT4                i4RetStatus = QOS_FAILURE;

    *pu4ErrorCode = SNMP_ERR_NO_ERROR;

    /* Check Index */
    i4RetStatus = QoSUtlValidatePolicyTblIdxInst (u4FsQoSPolicyMapId,
                                                  pu4ErrorCode);
    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlValidatePolicyTblIdxInst () "
                      "Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    /* Check  Object's Value */
    i4RetStatus = QoSValidateInProConfAction
        (u4FsQoSPolicyMapId,
         u4TestValFsQoSPolicyMapConformActionSetInnerVlanPrio,
         QOS_PLY_IN_PROF_CONF_INNER_VLAN_PRI, pu4ErrorCode);

    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSValidateInProConfAction () "
                      "Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhTestv2FsQoSPolicyMapConformActionSetMplsExp
 Input       :  The Indices
                FsQoSPolicyMapId

                The Object 
                testValFsQoSPolicyMapConformActionSetMplsExp
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
     
     
     
     
     
     
     
    nmhTestv2FsQoSPolicyMapConformActionSetMplsExp
    (UINT4 *pu4ErrorCode,
     UINT4 u4FsQoSPolicyMapId,
     UINT4 u4TestValFsQoSPolicyMapConformActionSetMplsExp)
{
    INT4                i4RetStatus = QOS_FAILURE;

    *pu4ErrorCode = SNMP_ERR_NO_ERROR;

    /* Check Index */
    i4RetStatus = QoSUtlValidatePolicyTblIdxInst (u4FsQoSPolicyMapId,
                                                  pu4ErrorCode);
    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlValidatePolicyTblIdxInst () "
                      "Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    /* Check  Object's Value */
    i4RetStatus = QoSValidateInProConfAction
        (u4FsQoSPolicyMapId,
         u4TestValFsQoSPolicyMapConformActionSetMplsExp,
         QOS_PLY_IN_PROF_CONF_MPLS_EXP, pu4ErrorCode);

    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSValidateInProConfAction () "
                      "Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhTestv2FsQoSPolicyMapConformActionSetNewCLASS
 Input       :  The Indices
                FsQoSPolicyMapId

                The Object 
                testValFsQoSPolicyMapConformActionSetNewCLASS
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
     
     
     
     
     
     
     
    nmhTestv2FsQoSPolicyMapConformActionSetNewCLASS
    (UINT4 *pu4ErrorCode,
     UINT4 u4FsQoSPolicyMapId,
     UINT4 u4TestValFsQoSPolicyMapConformActionSetNewCLASS)
{
    INT4                i4RetStatus = QOS_FAILURE;

    *pu4ErrorCode = SNMP_ERR_NO_ERROR;

    if (u4TestValFsQoSPolicyMapConformActionSetNewCLASS == 0)
    {
        return (SNMP_SUCCESS);
    }
    /* Check Index */
    i4RetStatus = QoSUtlValidatePolicyTblIdxInst (u4FsQoSPolicyMapId,
                                                  pu4ErrorCode);
    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlValidatePolicyTblIdxInst () "
                      "Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    /* Check  Object's Value */
    i4RetStatus =
        QoSUtlValidateClass (u4TestValFsQoSPolicyMapConformActionSetNewCLASS,
                             pu4ErrorCode);

    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlValidateClass () "
                      "Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhTestv2FsQoSPolicyMapInProfileExceedActionFlag
 Input       :  The Indices
                FsQoSPolicyMapId

                The Object 
                testValFsQoSPolicyMapInProfileExceedActionFlag
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhTestv2FsQoSPolicyMapInProfileExceedActionFlag
    (UINT4 *pu4ErrorCode,
     UINT4 u4FsQoSPolicyMapId,
     tSNMP_OCTET_STRING_TYPE * pTestValFsQoSPolicyMapInProfileExceedActionFlag)
{
    INT4                i4RetStatus = QOS_FAILURE;
    *pu4ErrorCode = SNMP_ERR_NO_ERROR;

    /* Check Index */
    i4RetStatus = QoSUtlValidatePolicyTblIdxInst (u4FsQoSPolicyMapId,
                                                  pu4ErrorCode);
    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlValidatePolicyTblIdxInst () "
                      "Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    /* Check  Object's Value */
    i4RetStatus =
        QoSValidateInProExcActFlag
        (u4FsQoSPolicyMapId,
         pTestValFsQoSPolicyMapInProfileExceedActionFlag, pu4ErrorCode);

    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSValidateInProExcActFlag () "
                      "Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhTestv2FsQoSPolicyMapInProfileExceedActionId
 Input       :  The Indices
                FsQoSPolicyMapId

                The Object 
                testValFsQoSPolicyMapInProfileExceedActionId
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsQoSPolicyMapInProfileExceedActionId (UINT4 *pu4ErrorCode,
                                                UINT4 u4FsQoSPolicyMapId,
                                                UINT4
                                                u4TestValFsQoSPolicyMapInProfileExceedActionId)
{
    INT4                i4RetStatus = QOS_FAILURE;

    *pu4ErrorCode = SNMP_ERR_NO_ERROR;

    /* Check Index */
    i4RetStatus = QoSUtlValidatePolicyTblIdxInst (u4FsQoSPolicyMapId,
                                                  pu4ErrorCode);
    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlValidatePolicyTblIdxInst () "
                      "Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    /* Check  Object's Value */
    if (QosValidateIndex
        (u4TestValFsQoSPolicyMapInProfileExceedActionId,
         DS_ACTION) == QOS_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return (SNMP_FAILURE);
    }

    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhTestv2FsQoSPolicyMapExceedActionSetIpTOS
 Input       :  The Indices
                FsQoSPolicyMapId

                The Object 
                testValFsQoSPolicyMapExceedActionSetIpTOS
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
     
     
     
     
     
     
     
    nmhTestv2FsQoSPolicyMapExceedActionSetIpTOS
    (UINT4 *pu4ErrorCode,
     UINT4 u4FsQoSPolicyMapId,
     UINT4 u4TestValFsQoSPolicyMapExceedActionSetIpTOS)
{
    INT4                i4RetStatus = QOS_FAILURE;

    *pu4ErrorCode = SNMP_ERR_NO_ERROR;

    /* Check Index */
    i4RetStatus = QoSUtlValidatePolicyTblIdxInst (u4FsQoSPolicyMapId,
                                                  pu4ErrorCode);
    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlValidatePolicyTblIdxInst () "
                      "Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    /* Check  Object's Value */
    i4RetStatus = QoSValidateInProExcAction
        (u4FsQoSPolicyMapId,
         u4TestValFsQoSPolicyMapExceedActionSetIpTOS,
         QOS_PLY_IN_PROF_EXC_IP_TOS, pu4ErrorCode);

    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSValidateInProExcAction () "
                      "Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhTestv2FsQoSPolicyMapExceedActionSetDscp
 Input       :  The Indices
                FsQoSPolicyMapId

                The Object 
                testValFsQoSPolicyMapExceedActionSetDscp
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
     
     
     
     
     
     
     
    nmhTestv2FsQoSPolicyMapExceedActionSetDscp
    (UINT4 *pu4ErrorCode,
     UINT4 u4FsQoSPolicyMapId, INT4 i4TestValFsQoSPolicyMapExceedActionSetDscp)
{
    INT4                i4RetStatus = QOS_FAILURE;

    *pu4ErrorCode = SNMP_ERR_NO_ERROR;

    /* Check Index */
    i4RetStatus = QoSUtlValidatePolicyTblIdxInst (u4FsQoSPolicyMapId,
                                                  pu4ErrorCode);
    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlValidatePolicyTblIdxInst () "
                      "Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    /* Check  Object's Value */
    i4RetStatus = QoSValidateInProExcAction
        (u4FsQoSPolicyMapId,
         i4TestValFsQoSPolicyMapExceedActionSetDscp,
         QOS_PLY_IN_PROF_EXC_IP_DSCP, pu4ErrorCode);
    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSValidateInProExcAction () "
                      "Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhTestv2FsQoSPolicyMapExceedActionSetInnerVlanPrio
 Input       :  The Indices
                FsQoSPolicyMapId

                The Object 
                testValFsQoSPolicyMapExceedActionSetInnerVlanPrio
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
     
     
     
     
     
     
     
    nmhTestv2FsQoSPolicyMapExceedActionSetInnerVlanPrio
    (UINT4 *pu4ErrorCode,
     UINT4 u4FsQoSPolicyMapId,
     UINT4 u4TestValFsQoSPolicyMapExceedActionSetInnerVlanPrio)
{
    INT4                i4RetStatus = QOS_FAILURE;

    *pu4ErrorCode = SNMP_ERR_NO_ERROR;

    /* Check Index */
    i4RetStatus = QoSUtlValidatePolicyTblIdxInst (u4FsQoSPolicyMapId,
                                                  pu4ErrorCode);
    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlValidatePolicyTblIdxInst () "
                      "Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    /* Check  Object's Value */
    i4RetStatus = QoSValidateInProExcAction
        (u4FsQoSPolicyMapId,
         u4TestValFsQoSPolicyMapExceedActionSetInnerVlanPrio,
         QOS_PLY_IN_PROF_EXC_INNER_VLAN_PRI, pu4ErrorCode);
    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSValidateInProExcAction () "
                      "Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhTestv2FsQoSPolicyMapExceedActionSetVlanPrio
 Input       :  The Indices
                FsQoSPolicyMapId

                The Object 
                testValFsQoSPolicyMapExceedActionSetVlanPrio
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
     
     
     
     
     
     
     
    nmhTestv2FsQoSPolicyMapExceedActionSetVlanPrio
    (UINT4 *pu4ErrorCode,
     UINT4 u4FsQoSPolicyMapId,
     UINT4 u4TestValFsQoSPolicyMapExceedActionSetVlanPrio)
{
    INT4                i4RetStatus = QOS_FAILURE;

    *pu4ErrorCode = SNMP_ERR_NO_ERROR;

    /* Check Index */
    i4RetStatus = QoSUtlValidatePolicyTblIdxInst (u4FsQoSPolicyMapId,
                                                  pu4ErrorCode);
    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlValidatePolicyTblIdxInst () "
                      "Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    /* Check  Object's Value */
    i4RetStatus = QoSValidateInProExcAction
        (u4FsQoSPolicyMapId,
         u4TestValFsQoSPolicyMapExceedActionSetVlanPrio,
         QOS_PLY_IN_PROF_EXC_VLAN_PRI, pu4ErrorCode);
    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSValidateInProExcAction () "
                      "Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhTestv2FsQoSPolicyMapExceedActionSetVlanDE
 Input       :  The Indices
                FsQoSPolicyMapId

                The Object 
                testValFsQoSPolicyMapExceedActionSetVlanDE
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
     
     
     
     
     
     
     
    nmhTestv2FsQoSPolicyMapExceedActionSetVlanDE
    (UINT4 *pu4ErrorCode,
     UINT4 u4FsQoSPolicyMapId,
     UINT4 u4TestValFsQoSPolicyMapExceedActionSetVlanDE)
{
    INT4                i4RetStatus = QOS_FAILURE;

    *pu4ErrorCode = SNMP_ERR_NO_ERROR;

    /* Check Index */
    i4RetStatus = QoSUtlValidatePolicyTblIdxInst (u4FsQoSPolicyMapId,
                                                  pu4ErrorCode);
    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlValidatePolicyTblIdxInst () "
                      "Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    /* Check  Object's Value */
    i4RetStatus = QoSValidateInProExcAction
        (u4FsQoSPolicyMapId,
         u4TestValFsQoSPolicyMapExceedActionSetVlanDE,
         QOS_PLY_IN_PROF_EXC_VLAN_DE, pu4ErrorCode);
    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSValidateInProExcAction () "
                      "Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhTestv2FsQoSPolicyMapExceedActionSetMplsExp
 Input       :  The Indices
                FsQoSPolicyMapId

                The Object 
                testValFsQoSPolicyMapExceedActionSetMplsExp
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
     
     
     
     
     
     
     
    nmhTestv2FsQoSPolicyMapExceedActionSetMplsExp
    (UINT4 *pu4ErrorCode,
     UINT4 u4FsQoSPolicyMapId,
     UINT4 u4TestValFsQoSPolicyMapExceedActionSetMplsExp)
{
    INT4                i4RetStatus = QOS_FAILURE;

    *pu4ErrorCode = SNMP_ERR_NO_ERROR;

    /* Check Index */
    i4RetStatus = QoSUtlValidatePolicyTblIdxInst (u4FsQoSPolicyMapId,
                                                  pu4ErrorCode);
    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlValidatePolicyTblIdxInst () "
                      "Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    /* Check  Object's Value */
    i4RetStatus = QoSValidateInProExcAction
        (u4FsQoSPolicyMapId,
         u4TestValFsQoSPolicyMapExceedActionSetMplsExp,
         QOS_PLY_IN_PROF_EXC_MPLS_EXP, pu4ErrorCode);
    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSValidateInProExcAction () "
                      "Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhTestv2FsQoSPolicyMapExceedActionSetNewCLASS
 Input       :  The Indices
                FsQoSPolicyMapId

                The Object 
                testValFsQoSPolicyMapExceedActionSetNewCLASS
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
     
     
     
     
     
     
     
    nmhTestv2FsQoSPolicyMapExceedActionSetNewCLASS
    (UINT4 *pu4ErrorCode,
     UINT4 u4FsQoSPolicyMapId,
     UINT4 u4TestValFsQoSPolicyMapExceedActionSetNewCLASS)
{
    INT4                i4RetStatus = QOS_FAILURE;

    *pu4ErrorCode = SNMP_ERR_NO_ERROR;

    if (u4TestValFsQoSPolicyMapExceedActionSetNewCLASS == 0)
    {
        return (SNMP_SUCCESS);
    }

    /* Check Index */
    i4RetStatus = QoSUtlValidatePolicyTblIdxInst (u4FsQoSPolicyMapId,
                                                  pu4ErrorCode);
    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlValidatePolicyTblIdxInst () "
                      "Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    /* Check  Object's Value */
    i4RetStatus =
        QoSUtlValidateClass (u4TestValFsQoSPolicyMapExceedActionSetNewCLASS,
                             pu4ErrorCode);
    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlValidateClass () "
                      "Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhTestv2FsQoSPolicyMapOutProfileActionFlag
 Input       :  The Indices
                FsQoSPolicyMapId

                The Object 
                testValFsQoSPolicyMapOutProfileActionFlag
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
     
     
     
     
     
     
     
    nmhTestv2FsQoSPolicyMapOutProfileActionFlag
    (UINT4 *pu4ErrorCode,
     UINT4 u4FsQoSPolicyMapId,
     tSNMP_OCTET_STRING_TYPE * pTestValFsQoSPolicyMapOutProfileActionFlag)
{
    INT4                i4RetStatus = QOS_FAILURE;

    *pu4ErrorCode = SNMP_ERR_NO_ERROR;

    /* Check Index */
    i4RetStatus = QoSUtlValidatePolicyTblIdxInst (u4FsQoSPolicyMapId,
                                                  pu4ErrorCode);
    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlValidatePolicyTblIdxInst () "
                      "Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    /* Check  Object's Value */
    i4RetStatus = QoSValidateOutProfileActionFlag
        (u4FsQoSPolicyMapId,
         pTestValFsQoSPolicyMapOutProfileActionFlag, pu4ErrorCode);
    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSValidateOutProfileActionFlag () "
                      "Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhTestv2FsQoSPolicyMapOutProfileActionId
 Input       :  The Indices
                FsQoSPolicyMapId

                The Object 
                testValFsQoSPolicyMapOutProfileActionId
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsQoSPolicyMapOutProfileActionId (UINT4 *pu4ErrorCode,
                                           UINT4 u4FsQoSPolicyMapId,
                                           UINT4
                                           u4TestValFsQoSPolicyMapOutProfileActionId)
{
    INT4                i4RetStatus = QOS_FAILURE;

    *pu4ErrorCode = SNMP_ERR_NO_ERROR;

    /* Check Index */
    i4RetStatus = QoSUtlValidatePolicyTblIdxInst (u4FsQoSPolicyMapId,
                                                  pu4ErrorCode);
    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlValidatePolicyTblIdxInst () "
                      "Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    /* Check  Object's Value */
    if (QosValidateIndex (u4TestValFsQoSPolicyMapOutProfileActionId, DS_ACTION)
        == QOS_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return (SNMP_FAILURE);
    }

    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhTestv2FsQoSPolicyMapOutProfileActionSetIPTOS
 Input       :  The Indices
                FsQoSPolicyMapId

                The Object 
                testValFsQoSPolicyMapOutProfileActionSetIPTOS
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
     
     
     
     
     
     
     
    nmhTestv2FsQoSPolicyMapOutProfileActionSetIPTOS
    (UINT4 *pu4ErrorCode,
     UINT4 u4FsQoSPolicyMapId,
     UINT4 u4TestValFsQoSPolicyMapOutProfileActionSetIPTOS)
{
    INT4                i4RetStatus = QOS_FAILURE;

    *pu4ErrorCode = SNMP_ERR_NO_ERROR;

    /* Check Index */
    i4RetStatus = QoSUtlValidatePolicyTblIdxInst (u4FsQoSPolicyMapId,
                                                  pu4ErrorCode);
    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlValidatePolicyTblIdxInst () "
                      "Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    /* Check  Object's Value */
    i4RetStatus = QoSValidateOutProfileAction
        (u4FsQoSPolicyMapId,
         u4TestValFsQoSPolicyMapOutProfileActionSetIPTOS,
         QOS_PLY_OUT_PROF_VIO_IP_TOS, pu4ErrorCode);
    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSValidateOutProfileActionFlag () "
                      "Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhTestv2FsQoSPolicyMapOutProfileActionSetDscp
 Input       :  The Indices
                FsQoSPolicyMapId

                The Object 
                testValFsQoSPolicyMapOutProfileActionSetDscp
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
     
     
     
     
     
     
     
    nmhTestv2FsQoSPolicyMapOutProfileActionSetDscp
    (UINT4 *pu4ErrorCode,
     UINT4 u4FsQoSPolicyMapId,
     INT4 i4TestValFsQoSPolicyMapOutProfileActionSetDscp)
{
    INT4                i4RetStatus = QOS_FAILURE;

    *pu4ErrorCode = SNMP_ERR_NO_ERROR;

    /* Check Index */
    i4RetStatus = QoSUtlValidatePolicyTblIdxInst (u4FsQoSPolicyMapId,
                                                  pu4ErrorCode);
    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlValidatePolicyTblIdxInst () "
                      "Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    /* Check  Object's Value */
    i4RetStatus = QoSValidateOutProfileAction
        (u4FsQoSPolicyMapId,
         i4TestValFsQoSPolicyMapOutProfileActionSetDscp,
         QOS_PLY_OUT_PROF_VIO_IP_DSCP, pu4ErrorCode);
    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSValidateOutProfileActionFlag () "
                      "Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhTestv2FsQoSPolicyMapOutProfileActionSetInnerVlanPrio
 Input       :  The Indices
                FsQoSPolicyMapId

                The Object 
                testValFsQoSPolicyMapOutProfileActionSetInnerVlanPrio
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
     
     
     
     
     
     
     
    nmhTestv2FsQoSPolicyMapOutProfileActionSetInnerVlanPrio
    (UINT4 *pu4ErrorCode,
     UINT4
     u4FsQoSPolicyMapId,
     UINT4 u4TestValFsQoSPolicyMapOutProfileActionSetInnerVlanPrio)
{
    INT4                i4RetStatus = QOS_FAILURE;

    *pu4ErrorCode = SNMP_ERR_NO_ERROR;

    /* Check Index */
    i4RetStatus = QoSUtlValidatePolicyTblIdxInst (u4FsQoSPolicyMapId,
                                                  pu4ErrorCode);
    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlValidatePolicyTblIdxInst () "
                      "Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    /* Check  Object's Value */
    i4RetStatus = QoSValidateOutProfileAction
        (u4FsQoSPolicyMapId,
         u4TestValFsQoSPolicyMapOutProfileActionSetInnerVlanPrio,
         QOS_PLY_OUT_PROF_VIO_INNER_VLAN_PRI, pu4ErrorCode);
    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSValidateOutProfileActionFlag () "
                      "Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhTestv2FsQoSPolicyMapOutProfileActionSetVlanPrio
 Input       :  The Indices
                FsQoSPolicyMapId

                The Object 
                testValFsQoSPolicyMapOutProfileActionSetVlanPrio
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
     
     
     
     
     
     
     
    nmhTestv2FsQoSPolicyMapOutProfileActionSetVlanPrio
    (UINT4 *pu4ErrorCode,
     UINT4 u4FsQoSPolicyMapId,
     UINT4 u4TestValFsQoSPolicyMapOutProfileActionSetVlanPrio)
{
    INT4                i4RetStatus = QOS_FAILURE;

    *pu4ErrorCode = SNMP_ERR_NO_ERROR;

    /* Check Index */
    i4RetStatus = QoSUtlValidatePolicyTblIdxInst (u4FsQoSPolicyMapId,
                                                  pu4ErrorCode);
    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlValidatePolicyTblIdxInst () "
                      "Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    /* Check  Object's Value */
    i4RetStatus = QoSValidateOutProfileAction
        (u4FsQoSPolicyMapId,
         u4TestValFsQoSPolicyMapOutProfileActionSetVlanPrio,
         QOS_PLY_OUT_PROF_VIO_VLAN_PRI, pu4ErrorCode);
    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSValidateOutProfileActionFlag () "
                      "Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhTestv2FsQoSPolicyMapOutProfileActionSetVlanDE
 Input       :  The Indices
                FsQoSPolicyMapId

                The Object 
                testValFsQoSPolicyMapOutProfileActionSetVlanDE
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
     
     
     
     
     
     
     
    nmhTestv2FsQoSPolicyMapOutProfileActionSetVlanDE
    (UINT4 *pu4ErrorCode,
     UINT4 u4FsQoSPolicyMapId,
     UINT4 u4TestValFsQoSPolicyMapOutProfileActionSetVlanDE)
{
    INT4                i4RetStatus = QOS_FAILURE;

    *pu4ErrorCode = SNMP_ERR_NO_ERROR;

    /* Check Index */
    i4RetStatus = QoSUtlValidatePolicyTblIdxInst (u4FsQoSPolicyMapId,
                                                  pu4ErrorCode);
    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlValidatePolicyTblIdxInst () "
                      "Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    /* Check  Object's Value */
    i4RetStatus = QoSValidateOutProfileAction
        (u4FsQoSPolicyMapId,
         u4TestValFsQoSPolicyMapOutProfileActionSetVlanDE,
         QOS_PLY_OUT_PROF_VIO_VLAN_DE, pu4ErrorCode);

    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSValidateOutProfileActionFlag () "
                      "Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhTestv2FsQoSPolicyMapOutProfileActionSetMplsExp
 Input       :  The Indices
                FsQoSPolicyMapId

                The Object 
                testValFsQoSPolicyMapOutProfileActionSetMplsExp
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
     
     
     
     
     
     
     
    nmhTestv2FsQoSPolicyMapOutProfileActionSetMplsExp
    (UINT4 *pu4ErrorCode,
     UINT4 u4FsQoSPolicyMapId,
     UINT4 u4TestValFsQoSPolicyMapOutProfileActionSetMplsExp)
{
    INT4                i4RetStatus = QOS_FAILURE;

    *pu4ErrorCode = SNMP_ERR_NO_ERROR;

    /* Check Index */
    i4RetStatus = QoSUtlValidatePolicyTblIdxInst (u4FsQoSPolicyMapId,
                                                  pu4ErrorCode);
    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlValidatePolicyTblIdxInst () "
                      "Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    /* Check  Object's Value */
    i4RetStatus = QoSValidateOutProfileAction
        (u4FsQoSPolicyMapId,
         u4TestValFsQoSPolicyMapOutProfileActionSetMplsExp,
         QOS_PLY_OUT_PROF_VIO_MPLS_EXP, pu4ErrorCode);
    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSValidateOutProfileActionFlag () "
                      "Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhTestv2FsQoSPolicyMapOutProfileActionSetNewCLASS
 Input       :  The Indices
                FsQoSPolicyMapId

                The Object 
                testValFsQoSPolicyMapOutProfileActionSetNewCLASS
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
     
     
     
     
     
     
     
    nmhTestv2FsQoSPolicyMapOutProfileActionSetNewCLASS
    (UINT4 *pu4ErrorCode,
     UINT4 u4FsQoSPolicyMapId,
     UINT4 u4TestValFsQoSPolicyMapOutProfileActionSetNewCLASS)
{
    INT4                i4RetStatus = QOS_FAILURE;

    *pu4ErrorCode = SNMP_ERR_NO_ERROR;

    if (u4TestValFsQoSPolicyMapOutProfileActionSetNewCLASS == 0)
    {
        return (SNMP_SUCCESS);
    }
    /* Check Index */
    i4RetStatus = QoSUtlValidatePolicyTblIdxInst (u4FsQoSPolicyMapId,
                                                  pu4ErrorCode);
    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlValidatePolicyTblIdxInst () "
                      "Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    /* Check  Object's Value */
    i4RetStatus =
        QoSUtlValidateClass (u4TestValFsQoSPolicyMapOutProfileActionSetNewCLASS,
                             pu4ErrorCode);
    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlValidateClass () "
                      "Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhTestv2FsQoSPolicyMapStatus
 Input       :  The Indices
                FsQoSPolicyMapId

                The Object 
                testValFsQoSPolicyMapStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsQoSPolicyMapStatus (UINT4 *pu4ErrorCode,
                               UINT4 u4FsQoSPolicyMapId,
                               INT4 i4TestValFsQoSPolicyMapStatus)
{
    INT4                i4RetStatus = QOS_FAILURE;
    UINT4               u4Count = 0;
    INT4                i4ReturnValue = SNMP_SUCCESS;
    tQoSMeterNode      *pMeterNode = NULL;
    tQoSPolicyMapNode  *pPlyMapNode = NULL;

    if (gQoSGlobalInfo.eSysControl != QOS_SYS_CNTL_START)
    {
        QOS_TRC (MGMT_TRC, QOS_MODULE_SHUTDOW_ERROR_MSG);
        CLI_SET_ERR (QOS_CLI_ERR_MODULE_SHUTDOW);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return (SNMP_FAILURE);
    }

    *pu4ErrorCode = SNMP_ERR_NO_ERROR;

    /* Check the Range of Index Values (Min-Max) */
    i4RetStatus = QOS_CHECK_PLY_MAP_TBL_INDEX_RANGE (u4FsQoSPolicyMapId);

    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG3 (MGMT_TRC, "%s : Policy Map Id %d is out of Range."
                      " The Range Should be (1-%d).", __FUNCTION__,
                      u4FsQoSPolicyMapId, QOS_PLY_MAP_TBL_MAX_INDEX_RANGE);
        CLI_SET_ERR (QOS_CLI_ERR_PLY_MAP_RANGE);
        return (SNMP_FAILURE);
    }

    pPlyMapNode = QoSUtlGetPolicyMapNode (u4FsQoSPolicyMapId);

    if (pPlyMapNode == NULL)
    {
        /* Entry Not Found In The Table */
        switch (i4TestValFsQoSPolicyMapStatus)
        {
            case CREATE_AND_GO:
            case ACTIVE:
            case NOT_IN_SERVICE:
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                i4ReturnValue = SNMP_FAILURE;
                break;

            case CREATE_AND_WAIT:
                /* Check the No of Entries in the Table */
                i4RetStatus =
                    RBTreeCount (gQoSGlobalInfo.pRbPlyMapTbl, &u4Count);

                if ((i4RetStatus == RB_FAILURE) ||
                    (u4Count > QOS_PLY_MAP_TBL_MAX_ENTRIES))
                {
                    QOS_TRC_ARG2 (MGMT_TRC, "%s : Max No of Policy Map Table"
                                  " Entries are Configured. No of Max Entries"
                                  " %d. \r\n", __FUNCTION__,
                                  QOS_PLY_MAP_TBL_MAX_ENTRIES);

                    CLI_SET_ERR (QOS_CLI_ERR_PLY_MAP_MAX_ENTRY);
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;

                    i4ReturnValue = SNMP_FAILURE;
                }

                break;

            case DESTROY:
                CLI_SET_ERR (QOS_CLI_ERR_PLY_MAP_NO_ENTRY);
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                i4ReturnValue = SNMP_FAILURE;
                break;

            default:
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                i4ReturnValue = SNMP_FAILURE;
                break;
        }
    }
    else
    {
        /* Entry Found In The Table */
        switch (i4TestValFsQoSPolicyMapStatus)
        {
            case ACTIVE:
                /*Exceed Action is not applicable for Simple Token Bucket meter */
                pMeterNode = QoSUtlGetMeterNode (pPlyMapNode->u4MeterTableId);
                if (pMeterNode != NULL)
                {
                    if (pMeterNode->u1Type == QOS_METER_TYPE_SIMPLE_TB)
                    {
                        if (pPlyMapNode->u1InProfExcActFlag != FNP_ZERO)
                        {
                            QOS_TRC_ARG1 (MGMT_TRC,
                                          "In %s : QoSValidateInProExcActFlag () "
                                          "Returns FAILURE. \r\n",
                                          __FUNCTION__);

                            CLI_SET_ERR (QOS_CLI_ERR_PLY_INVALID_METER_PARAM);
                            pPlyMapNode->u1InProfExcActFlag = FNP_ZERO;
                            return (SNMP_FAILURE);
                        }
                    }
                }
                if ((pPlyMapNode->u1Status == NOT_READY))
                {
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                    i4ReturnValue = SNMP_FAILURE;
                }

                break;

            case NOT_IN_SERVICE:

                if ((pPlyMapNode->u1Status == NOT_READY))
                {
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                    i4ReturnValue = SNMP_FAILURE;
                }

                break;

            case DESTROY:
                /* Check(Test) is it Default Entry. */
                i4RetStatus = QoSIsDefPolicyMapTblEntry (pPlyMapNode);
                if (i4RetStatus == QOS_SUCCESS)
                {
                    QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSIsPolicyMapDefEntry  "
                                  "Returns FAILURE.\r\n", __FUNCTION__);
                    CLI_SET_ERR (QOS_CLI_ERR_DEF_ENTRY);
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                    i4ReturnValue = SNMP_FAILURE;
                }
                break;

            case CREATE_AND_WAIT:
            case CREATE_AND_GO:
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                i4ReturnValue = SNMP_FAILURE;
                break;

            default:
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                i4ReturnValue = SNMP_FAILURE;
                break;

        }                        /* End of switch */

    }                            /* End of if */

    return ((INT1) i4ReturnValue);
}

/****************************************************************************
 Function    :  nmhTestv2FsQoSPolicyMapConformActionSetInnerVlanDE
 Input       :  The Indices
                FsQoSPolicyMapId

                The Object
                testValFsQoSPolicyMapConformActionSetInnerVlanDE
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val                                                   Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsQoSPolicyMapConformActionSetInnerVlanDE (UINT4 *pu4ErrorCode,
                                                    UINT4 u4FsQoSPolicyMapId,
                                                    UINT4
                                                    u4TestValFsQoSPolicyMapConformActionSetInnerVlanDE)
{
    INT4                i4RetStatus = QOS_FAILURE;

    *pu4ErrorCode = SNMP_ERR_NO_ERROR;

    /* Check Index */
    i4RetStatus = QoSUtlValidatePolicyTblIdxInst (u4FsQoSPolicyMapId,
                                                  pu4ErrorCode);
    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlValidatePolicyTblIdxInst () "
                      "Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    /* Check  Object's Value */
    i4RetStatus = QoSValidateInProConfAction
        (u4FsQoSPolicyMapId,
         (INT4) u4TestValFsQoSPolicyMapConformActionSetInnerVlanDE,
         QOS_PLY_IN_PROF_CONF_INNER_VLAN_DE, pu4ErrorCode);

    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSValidateInProConfAction () "
                      "Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhTestv2FsQoSPolicyMapExceedActionSetInnerVlanDE
 Input       :  The Indices
                FsQoSPolicyMapId

                The Object
                testValFsQoSPolicyMapExceedActionSetInnerVlanDE
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsQoSPolicyMapExceedActionSetInnerVlanDE (UINT4 *pu4ErrorCode,
                                                   UINT4 u4FsQoSPolicyMapId,
                                                   UINT4
                                                   u4TestValFsQoSPolicyMapExceedActionSetInnerVlanDE)
{
    INT4                i4RetStatus = QOS_FAILURE;

    *pu4ErrorCode = SNMP_ERR_NO_ERROR;

    /* Check Index */
    i4RetStatus = QoSUtlValidatePolicyTblIdxInst (u4FsQoSPolicyMapId,
                                                  pu4ErrorCode);
    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlValidatePolicyTblIdxInst () "
                      "Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    /* Check  Object's Value */
    i4RetStatus = QoSValidateInProExcAction
        (u4FsQoSPolicyMapId,
         (INT4) u4TestValFsQoSPolicyMapExceedActionSetInnerVlanDE,
         QOS_PLY_IN_PROF_EXC_INNER_VLAN_DE, pu4ErrorCode);
    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSValidateInProExcAction () "
                      "Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhTestv2FsQoSPolicyMapOutProfileActionSetInnerVlanDE
 Input       :  The Indices
                FsQoSPolicyMapId

                The Object
                testValFsQoSPolicyMapOutProfileActionSetInnerVlanDE
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsQoSPolicyMapOutProfileActionSetInnerVlanDE (UINT4 *pu4ErrorCode,
                                                       UINT4 u4FsQoSPolicyMapId,
                                                       UINT4
                                                       u4TestValFsQoSPolicyMapOutProfileActionSetInnerVlanDE)
{
    INT4                i4RetStatus = QOS_FAILURE;

    *pu4ErrorCode = SNMP_ERR_NO_ERROR;

    /* Check Index */
    i4RetStatus = QoSUtlValidatePolicyTblIdxInst (u4FsQoSPolicyMapId,
                                                  pu4ErrorCode);
    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlValidatePolicyTblIdxInst () "
                      "Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    /* Check  Object's Value */
    i4RetStatus = QoSValidateOutProfileAction
        (u4FsQoSPolicyMapId,
         (INT4) u4TestValFsQoSPolicyMapOutProfileActionSetInnerVlanDE,
         QOS_PLY_OUT_PROF_VIO_INNER_VLAN_DE, pu4ErrorCode);
    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSValidateOutProfileActionFlag () "
                      "Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    return (SNMP_SUCCESS);

}

/****************************************************************************
Function    :  nmhTestv2FsQoSPolicyDefaultVlanDE
Input       :  The Indices
               FsQoSPolicyMapId 
               The Object
               testValFsQoSPolicyDefaultVlanDE
Output      :  The Test Low Lev Routine Take the Indices &                                                                               Test whether that Value is Valid Input for Set.
               Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
               SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
               SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
               SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
               SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
               SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2FsQoSPolicyDefaultVlanDE (UINT4 *pu4ErrorCode,
                                   UINT4 u4FsQoSPolicyMapId,
                                   INT4 i4TestValFsQoSPolicyDefaultVlanDE)
{
    INT4                i4RetStatus = QOS_FAILURE;

    *pu4ErrorCode = SNMP_ERR_NO_ERROR;

    /* Check Index */
    i4RetStatus = QoSUtlValidatePolicyTblIdxInst (u4FsQoSPolicyMapId,
                                                  pu4ErrorCode);
    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlValidatePolicyTblIdxInst () "
                      "Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }
    if ((i4TestValFsQoSPolicyDefaultVlanDE < QOS_VLAN_DE_MIN_VAL) ||
        (i4TestValFsQoSPolicyDefaultVlanDE > QOS_VLAN_DE_MAX_VAL))
    {
        QOS_TRC_ARG4 (MGMT_TRC, "%s : DE value %d, is out of "
                      " range. The range is Min %d to Max %d.\r\n",
                      __FUNCTION__, i4TestValFsQoSPolicyDefaultVlanDE,
                      QOS_VLAN_DE_MIN_VAL, QOS_VLAN_DE_MAX_VAL);

        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;

        i4RetStatus = QOS_FAILURE;
    }

    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : DEI validation () "
                      "Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    return (SNMP_SUCCESS);

}

/* LOW LEVEL Routines for Table : FsQoSQTemplateTable. */
/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsQoSQTemplateTable
 Input       :  The Indices
                FsQoSQTemplateId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsQoSQTemplateTable (UINT4 u4FsQoSQTemplateId)
{
    INT4                i4RetStatus = 0;
    tQoSQTemplateNode  *pQTempNode = NULL;

    if (gQoSGlobalInfo.eSysControl != QOS_SYS_CNTL_START)
    {
        QOS_TRC (MGMT_TRC, QOS_MODULE_SHUTDOW_ERROR_MSG);
        return (SNMP_FAILURE);
    }

    /* Check the Range of Index Values (Min-Max) */
    i4RetStatus = QOS_CHECK_Q_TEMP_TBL_INDEX_RANGE (u4FsQoSQTemplateId);

    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG3 (MGMT_TRC, "%s : Q Template Id %d is out of Range."
                      " The Range Should be (1-%d). \r\n", __FUNCTION__,
                      u4FsQoSQTemplateId, QOS_Q_TEMP_TBL_MAX_INDEX_RANGE);

        return (SNMP_FAILURE);
    }

    pQTempNode = QoSUtlGetQTempNode (u4FsQoSQTemplateId);

    if (pQTempNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetQTempNode () Returns "
                      "FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    return (SNMP_SUCCESS);

}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsQoSPolicyMapTable
 Input       :  The Indices
                FsQoSPolicyMapId
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsQoSPolicyMapTable (UINT4 *pu4ErrorCode,
                             tSnmpIndexList * pSnmpIndexList,
                             tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsQoSQTemplateTable
 Input       :  The Indices
                FsQoSQTemplateId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsQoSQTemplateTable (UINT4 *pu4FsQoSQTemplateId)
{
    INT4                i4RetStatus = QOS_FAILURE;

    if (gQoSGlobalInfo.eSysControl != QOS_SYS_CNTL_START)
    {
        QOS_TRC (MGMT_TRC, QOS_MODULE_SHUTDOW_ERROR_MSG);
        return (SNMP_FAILURE);
    }

    i4RetStatus = QoSGetNextQTempTblEntryIndex (0, pu4FsQoSQTemplateId);

    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSGetNextQTempTblEntryIndex () "
                      "Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsQoSQTemplateTable
 Input       :  The Indices
                FsQoSQTemplateId
                nextFsQoSQTemplateId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsQoSQTemplateTable (UINT4 u4FsQoSQTemplateId,
                                    UINT4 *pu4NextFsQoSQTemplateId)
{
    INT4                i4RetStatus = QOS_FAILURE;

    if (gQoSGlobalInfo.eSysControl != QOS_SYS_CNTL_START)
    {
        QOS_TRC (MGMT_TRC, QOS_MODULE_SHUTDOW_ERROR_MSG);
        return (SNMP_FAILURE);
    }

    /* Check the Range of Index Values (Min-Max) */
    i4RetStatus = QOS_CHECK_Q_TEMP_TBL_INDEX_RANGE (u4FsQoSQTemplateId);

    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG3 (MGMT_TRC, "%s : Q Template Id %d is out of Range."
                      " The Range Should be (1-%d). \r\n", __FUNCTION__,
                      u4FsQoSQTemplateId, QOS_Q_TEMP_TBL_MAX_INDEX_RANGE);

        return (SNMP_FAILURE);
    }

    i4RetStatus = QoSGetNextQTempTblEntryIndex (u4FsQoSQTemplateId,
                                                pu4NextFsQoSQTemplateId);
    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSGetNextQTempTblEntryIndex () "
                      "Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    return (SNMP_SUCCESS);
}

/* Low Level GET Routine for All Objects  */
/****************************************************************************
 Function    :  nmhGetFsQoSQTemplateName
 Input       :  The Indices
                FsQoSQTemplateId

                The Object 
                retValFsQoSQTemplateName
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsQoSQTemplateName (UINT4 u4FsQoSQTemplateId,
                          tSNMP_OCTET_STRING_TYPE * pRetValFsQoSQTemplateName)
{
    tQoSQTemplateNode  *pQTempNode = NULL;
    UINT1              *pu1Str = NULL;

    if ((pRetValFsQoSQTemplateName == NULL) ||
        (pRetValFsQoSQTemplateName->pu1_OctetList == NULL))
    {
        return (SNMP_FAILURE);
    }

    pQTempNode = QoSUtlGetQTempNode (u4FsQoSQTemplateId);

    if (pQTempNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetQTempNode () "
                      "Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    pu1Str = &(pQTempNode->au1Name[0]);
    QOS_UTL_COPY_TABLE_NAME (pRetValFsQoSQTemplateName, pu1Str);

    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhGetFsQoSQTemplateDropType
 Input       :  The Indices
                FsQoSQTemplateId

                The Object 
                retValFsQoSQTemplateDropType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsQoSQTemplateDropType (UINT4 u4FsQoSQTemplateId,
                              INT4 *pi4RetValFsQoSQTemplateDropType)
{
    tQoSQTemplateNode  *pQTempNode = NULL;

    pQTempNode = QoSUtlGetQTempNode (u4FsQoSQTemplateId);

    if (pQTempNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetQTempNode () "
                      "Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    *pi4RetValFsQoSQTemplateDropType = (INT4) pQTempNode->u2DropType;

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFsQoSQTemplateDropAlgoEnableFlag
 Input       :  The Indices
                FsQoSQTemplateId

                The Object 
                retValFsQoSQTemplateDropAlgoEnableFlag
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
     
     
     
     
     
     
     
    nmhGetFsQoSQTemplateDropAlgoEnableFlag
    (UINT4 u4FsQoSQTemplateId, INT4 *pi4RetValFsQoSQTemplateDropAlgoEnableFlag)
{
    tQoSQTemplateNode  *pQTempNode = NULL;

    pQTempNode = QoSUtlGetQTempNode (u4FsQoSQTemplateId);

    if (pQTempNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetQTempNode () "
                      "Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    *pi4RetValFsQoSQTemplateDropAlgoEnableFlag =
        (INT4) pQTempNode->u1DropAlgoEnableFlag;

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFsQoSQTemplateSize
 Input       :  The Indices
                FsQoSQTemplateId

                The Object 
                retValFsQoSQTemplateSize
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsQoSQTemplateSize (UINT4 u4FsQoSQTemplateId,
                          UINT4 *pu4RetValFsQoSQTemplateSize)
{
    tQoSQTemplateNode  *pQTempNode = NULL;

    pQTempNode = QoSUtlGetQTempNode (u4FsQoSQTemplateId);

    if (pQTempNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetQTempNode () "
                      "Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    *pu4RetValFsQoSQTemplateSize = pQTempNode->u4Size;

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFsQoSQTemplateStatus
 Input       :  The Indices
                FsQoSQTemplateId

                The Object 
                retValFsQoSQTemplateStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsQoSQTemplateStatus (UINT4 u4FsQoSQTemplateId,
                            INT4 *pi4RetValFsQoSQTemplateStatus)
{
    tQoSQTemplateNode  *pQTempNode = NULL;

    pQTempNode = QoSUtlGetQTempNode (u4FsQoSQTemplateId);

    if (pQTempNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetQTempNode () "
                      "Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    *pi4RetValFsQoSQTemplateStatus = pQTempNode->u1Status;

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFsQoSQMapRegenDEI
 Input       :  The Indices
                IfIndex
                FsQoSQMapCLASS
                FsQoSQMapRegenPriType
                FsQoSQMapRegenPriority
                    
                 The Object 
                retValFsQoSQMapRegenDEI
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsQoSQMapRegenDEI (INT4 i4IfIndex, UINT4 u4FsQoSQMapCLASS,
                         INT4 i4FsQoSQMapRegenPriType,
                         UINT4 u4FsQoSQMapRegenPriority,
                         INT4 *pi4RetValFsQoSQMapRegenDEI)
{
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (u4FsQoSQMapCLASS);
    UNUSED_PARAM (i4FsQoSQMapRegenPriType);
    UNUSED_PARAM (u4FsQoSQMapRegenPriority);
    UNUSED_PARAM (*pi4RetValFsQoSQMapRegenDEI);
    return FNP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsQoSQMapRegenColor
 Input       :  The Indices
                IfIndex
                FsQoSQMapCLASS
                FsQoSQMapRegenPriType
                FsQoSQMapRegenPriority
 
                The Object 
                retValFsQoSQMapRegenColor
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsQoSQMapRegenColor (INT4 i4IfIndex, UINT4 u4FsQoSQMapCLASS,
                           INT4 i4FsQoSQMapRegenPriType,
                           UINT4 u4FsQoSQMapRegenPriority,
                           INT4 *pi4RetValFsQoSQMapRegenColor)
{
    tQoSQMapNode       *pQMapNode = NULL;

    pQMapNode = QoSUtlGetQMapNode (i4IfIndex, u4FsQoSQMapCLASS,
                                   i4FsQoSQMapRegenPriType,
                                   u4FsQoSQMapRegenPriority);
    if (pQMapNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetQMapNode () "
                      "Returns FAILURE. \r\n", __FUNCTION__);
        return (SNMP_FAILURE);
    }

    *pi4RetValFsQoSQMapRegenColor = pQMapNode->i4RegenColor;

    return (SNMP_SUCCESS);

}

/* Low Level SET Routine for All Objects  */
/****************************************************************************
 Function    :  nmhSetFsQoSQTemplateName
 Input       :  The Indices
                FsQoSQTemplateId

                The Object 
                setValFsQoSQTemplateName
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsQoSQTemplateName (UINT4 u4FsQoSQTemplateId,
                          tSNMP_OCTET_STRING_TYPE * pSetValFsQoSQTemplateName)
{
    tQoSQTemplateNode  *pQTempNode = NULL;
    UINT1              *pu1Str = NULL;

    pQTempNode = QoSUtlGetQTempNode (u4FsQoSQTemplateId);

    if (pQTempNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetQTempNode () "
                      "Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    pu1Str = &(pQTempNode->au1Name[0]);
    QOS_UTL_SET_TABLE_NAME (pu1Str, pSetValFsQoSQTemplateName);

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhSetFsQoSQTemplateDropType
 Input       :  The Indices
                FsQoSQTemplateId

                The Object 
                setValFsQoSQTemplateDropType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsQoSQTemplateDropType (UINT4 u4FsQoSQTemplateId,
                              INT4 i4SetValFsQoSQTemplateDropType)
{
    tQoSQTemplateNode  *pQTempNode = NULL;

    pQTempNode = QoSUtlGetQTempNode (u4FsQoSQTemplateId);

    if (pQTempNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetQTempNode () "
                      "Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    pQTempNode->u2DropType = (UINT2) i4SetValFsQoSQTemplateDropType;

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhSetFsQoSQTemplateDropAlgoEnableFlag
 Input       :  The Indices
                FsQoSQTemplateId

                The Object 
                setValFsQoSQTemplateDropAlgoEnableFlag
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
     
     
     
     
     
     
     
    nmhSetFsQoSQTemplateDropAlgoEnableFlag
    (UINT4 u4FsQoSQTemplateId, INT4 i4SetValFsQoSQTemplateDropAlgoEnableFlag)
{
    tQoSQTemplateNode  *pQTempNode = NULL;

    pQTempNode = QoSUtlGetQTempNode (u4FsQoSQTemplateId);

    if (pQTempNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetQTempNode () "
                      "Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    pQTempNode->u1DropAlgoEnableFlag =
        (UINT1) i4SetValFsQoSQTemplateDropAlgoEnableFlag;

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhSetFsQoSQTemplateSize
 Input       :  The Indices
                FsQoSQTemplateId

                The Object 
                setValFsQoSQTemplateSize
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsQoSQTemplateSize (UINT4 u4FsQoSQTemplateId,
                          UINT4 u4SetValFsQoSQTemplateSize)
{
    tQoSQTemplateNode  *pQTempNode = NULL;

    pQTempNode = QoSUtlGetQTempNode (u4FsQoSQTemplateId);

    if (pQTempNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetQTempNode () "
                      "Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    pQTempNode->u4Size = u4SetValFsQoSQTemplateSize;

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhSetFsQoSQTemplateStatus
 Input       :  The Indices
                FsQoSQTemplateId

                The Object 
                setValFsQoSQTemplateStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsQoSQTemplateStatus (UINT4 u4FsQoSQTemplateId,
                            INT4 i4SetValFsQoSQTemplateStatus)
{
    tQoSQTemplateNode  *pQTempNode = NULL;
    INT4                i4RetStatus = QOS_FAILURE;
    INT4                i4ReturnValue = SNMP_SUCCESS;

    pQTempNode = QoSUtlGetQTempNode (u4FsQoSQTemplateId);

    if (pQTempNode != NULL)
    {
        /* Entry found in the Table */
        if (pQTempNode->u1Status == i4SetValFsQoSQTemplateStatus)
        {
            /* same value */
            return (SNMP_SUCCESS);
        }
    }
    else
    {
        /* New Entry need to be created in the Table, only if 
         * i4SetValFsQoSQTemplateStatus = CREATE_AND_WAIT */
        /* Optional */
        if (i4SetValFsQoSQTemplateStatus == DESTROY)
        {
            return (SNMP_SUCCESS);
        }
    }
    switch (i4SetValFsQoSQTemplateStatus)
    {
            /* For a New Entry */
        case CREATE_AND_GO:

            pQTempNode = QoSCreateQTempTblEntry (u4FsQoSQTemplateId);
            if (pQTempNode == NULL)
            {
                QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSCreateQTempTblEntry () "
                              "Returns FAILURE. \r\n", __FUNCTION__);

                return (SNMP_FAILURE);
            }
            if (pQTempNode != NULL)
            {
                pQTempNode->u1Status = ACTIVE;
            }

            i4ReturnValue = SNMP_SUCCESS;
            break;

        case CREATE_AND_WAIT:

            pQTempNode = QoSCreateQTempTblEntry (u4FsQoSQTemplateId);

            if (pQTempNode == NULL)
            {
                QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSCreateQTempTblEntry () "
                              "Returns FAILURE. \r\n", __FUNCTION__);

                return (SNMP_FAILURE);
            }
            if (pQTempNode != NULL)
            {
                pQTempNode->u1Status = NOT_IN_SERVICE;
            }

            i4ReturnValue = SNMP_SUCCESS;

            break;

            /* Modify an Entry */
        case ACTIVE:

            if (pQTempNode != NULL)
            {
                pQTempNode->u1Status = ACTIVE;
            }

            break;

        case NOT_IN_SERVICE:

            if (pQTempNode != NULL)
            {
                pQTempNode->u1Status = NOT_IN_SERVICE;
            }

            break;

        case DESTROY:

            /* 1. Remove the entry from the SOFTWARE. */
            i4RetStatus = QoSDeleteQTempTblEntry (pQTempNode);

            if (i4RetStatus == QOS_FAILURE)
            {
                QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSDeleteQTempTblEntry () "
                              "Returns FAILURE. \r\n", __FUNCTION__);

                i4ReturnValue = SNMP_FAILURE;
            }

            break;

        default:
            i4ReturnValue = SNMP_FAILURE;
            break;
    }
    if (i4ReturnValue == SNMP_SUCCESS)
    {
        QosUpdateDiffServAlgoDropNextFree (u4FsQoSQTemplateId,
                                           i4SetValFsQoSQTemplateStatus);
    }
    return ((INT1) i4ReturnValue);

}

/****************************************************************************
 Function    :  nmhSetFsQoSQMapRegenDEI
 Input       :  The Indices
                IfIndex
                FsQoSQMapCLASS
                FsQoSQMapRegenPriType
                FsQoSQMapRegenPriority
     
                The Object
                setValFsQoSQMapRegenDEI
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsQoSQMapRegenDEI (INT4 i4IfIndex, UINT4 u4FsQoSQMapCLASS,
                         INT4 i4FsQoSQMapRegenPriType,
                         UINT4 u4FsQoSQMapRegenPriority,
                         INT4 i4SetValFsQoSQMapRegenDEI)
{
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (u4FsQoSQMapCLASS);
    UNUSED_PARAM (i4FsQoSQMapRegenPriType);
    UNUSED_PARAM (u4FsQoSQMapRegenPriority);
    UNUSED_PARAM (i4SetValFsQoSQMapRegenDEI);
    return FNP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */
/****************************************************************************
 Function    :  nmhTestv2FsQoSQTemplateName
 Input       :  The Indices
                FsQoSQTemplateId

                The Object 
                testValFsQoSQTemplateName
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsQoSQTemplateName (UINT4 *pu4ErrorCode,
                             UINT4 u4FsQoSQTemplateId,
                             tSNMP_OCTET_STRING_TYPE *
                             pTestValFsQoSQTemplateName)
{
    tQoSQTemplateNode  *pQTempNode = NULL;
    INT4                i4RetStatus = QOS_FAILURE;

    *pu4ErrorCode = SNMP_ERR_NO_ERROR;

    /* Check Index */
    i4RetStatus = QoSUtlValidateQTempTblIdxInst (u4FsQoSQTemplateId,
                                                 pu4ErrorCode);

    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlValidateQTempTblIdxInst () "
                      "Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    /* Check  Object's Value */
    i4RetStatus = QoSUtlValidateTableName (pTestValFsQoSQTemplateName,
                                           pu4ErrorCode);
    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlValidateTableName () "
                      "Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    pQTempNode = QoSUtlGetQTempNode (u4FsQoSQTemplateId);
    if (pQTempNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetQTempNode () Returns "
                      "FAILURE. \r\n", __FUNCTION__);

        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (QOS_CLI_ERR_QTEMP_NOT_CREATION);
        return (SNMP_FAILURE);
    }

    /* Check the Name is same as Current Name */
    if ((STRLEN (pQTempNode->au1Name)) == (UINT4)
        pTestValFsQoSQTemplateName->i4_Length)
    {
        if ((STRNCMP (pQTempNode->au1Name,
                      pTestValFsQoSQTemplateName->pu1_OctetList,
                      pTestValFsQoSQTemplateName->i4_Length)) == 0)
        {
            return (SNMP_SUCCESS);
        }
    }

    /* Check the name is Unique or Not */
    i4RetStatus = QoSUtlQTempIsUniqueName (pTestValFsQoSQTemplateName);
    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlQTempIsUniqueName () Returns "
                      "FAILURE. \r\n", __FUNCTION__);

        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return (SNMP_FAILURE);
    }

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhTestv2FsQoSQTemplateDropType
 Input       :  The Indices
                FsQoSQTemplateId

                The Object 
                testValFsQoSQTemplateDropType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsQoSQTemplateDropType (UINT4 *pu4ErrorCode,
                                 UINT4 u4FsQoSQTemplateId,
                                 INT4 i4TestValFsQoSQTemplateDropType)
{
    INT4                i4RetStatus = QOS_FAILURE;

    *pu4ErrorCode = SNMP_ERR_NO_ERROR;

    /* Check Index */
    i4RetStatus = QoSUtlValidateQTempTblIdxInst (u4FsQoSQTemplateId,
                                                 pu4ErrorCode);
    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlValidateQTempTblIdxInst () "
                      "Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    /* Check  Object's Value */
    if ((i4TestValFsQoSQTemplateDropType < QOS_Q_TEMP_DROP_TYPE_MIN_VAL) ||
        (i4TestValFsQoSQTemplateDropType > QOS_Q_TEMP_DROP_TYPE_MAX_VAL))
    {
        QOS_TRC_ARG3 (MGMT_TRC, "%s : Q Template Drop Type value is out of "
                      "range. Range should be Min %d - Max %d. \r\n",
                      __FUNCTION__, QOS_Q_TEMP_DROP_TYPE_MIN_VAL,
                      QOS_Q_TEMP_DROP_TYPE_MAX_VAL);
        CLI_SET_ERR (QOS_CLI_ERR_QTEMP_DROP_TYPE);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;

        return (SNMP_FAILURE);
    }

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhTestv2FsQoSQTemplateDropAlgoEnableFlag
 Input       :  The Indices
                FsQoSQTemplateId

                The Object 
                testValFsQoSQTemplateDropAlgoEnableFlag
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
     
     
     
     
     
     
     
    nmhTestv2FsQoSQTemplateDropAlgoEnableFlag
    (UINT4 *pu4ErrorCode,
     UINT4 u4FsQoSQTemplateId, INT4 i4TestValFsQoSQTemplateDropAlgoEnableFlag)
{
    INT4                i4RetStatus = QOS_FAILURE;

    tQoSQTemplateNode  *pQTempNode = NULL;
    *pu4ErrorCode = SNMP_ERR_NO_ERROR;

    /* Check Index */
    i4RetStatus = QoSUtlValidateQTempTblIdxInst (u4FsQoSQTemplateId,
                                                 pu4ErrorCode);
    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlValidateQTempTblIdxInst () "
                      "Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    pQTempNode = QoSUtlGetQTempNode (u4FsQoSQTemplateId);
    if (pQTempNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetQTempNode () Returns "
                      "FAILURE. \r\n", __FUNCTION__);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (QOS_CLI_ERR_QTEMP_NOT_CREATION);
        return (SNMP_FAILURE);
    }

    /* Check  Object's Value */
    if ((i4TestValFsQoSQTemplateDropAlgoEnableFlag !=
         QOS_Q_TEMP_DROP_ALGO_ENABLE) &&
        (i4TestValFsQoSQTemplateDropAlgoEnableFlag !=
         QOS_Q_TEMP_DROP_ALGO_DISABLE))
    {
        QOS_TRC_ARG3 (MGMT_TRC, "%s : Q Template Drop Algo Option is out of "
                      "range. Range should be Min %d - Max %d. \r\n",
                      __FUNCTION__, QOS_Q_TEMP_DROP_ALGO_ENABLE,
                      QOS_Q_TEMP_DROP_ALGO_DISABLE);

        CLI_SET_ERR (QOS_CLI_ERR_QTEMP_DROP_ALGO);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }

    /* To Enable RED the RED Cfg Should be configured Already. */
    if (i4TestValFsQoSQTemplateDropAlgoEnableFlag ==
        QOS_Q_TEMP_DROP_ALGO_ENABLE)
    {
        if (pQTempNode->u2DropType == QOS_Q_TEMP_DROP_TYPE_RED ||
            pQTempNode->u2DropType == QOS_Q_TEMP_DROP_TYPE_WRED)
        {
            /* Check the RD Table for this QTempId */
            i4RetStatus = QoSUtlCheckRDTblEntry (u4FsQoSQTemplateId);
            if (i4RetStatus == QOS_FAILURE)
            {
                QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlCheckRDTblEntry () "
                              "Returns FAILURE. RD Tbl Entry for this QTempId "
                              "is NULL \r\n", __FUNCTION__);

                CLI_SET_ERR (QOS_CLI_ERR_QTEMP_NO_RDCFG);
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return (SNMP_FAILURE);
            }
        }
    }

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhTestv2FsQoSQTemplateSize
 Input       :  The Indices
                FsQoSQTemplateId

                The Object 
                testValFsQoSQTemplateSize
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsQoSQTemplateSize (UINT4 *pu4ErrorCode,
                             UINT4 u4FsQoSQTemplateId,
                             UINT4 u4TestValFsQoSQTemplateSize)
{
    INT4                i4RetStatus = QOS_FAILURE;

    *pu4ErrorCode = SNMP_ERR_NO_ERROR;

    /* Check Index */
    i4RetStatus = QoSUtlValidateQTempTblIdxInst (u4FsQoSQTemplateId,
                                                 pu4ErrorCode);
    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlValidateQTempTblIdxInst () "
                      "Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    /* Check  Object's Value */
    if ((u4TestValFsQoSQTemplateSize < QOS_Q_TEMP_SIZE_MIN_VAL) ||
        (u4TestValFsQoSQTemplateSize > QOS_Q_TEMP_SIZE_MAX_VAL))
    {
        QOS_TRC_ARG3 (MGMT_TRC, "%s : Q Template Size value is out of "
                      "range. Range should be Min %d - Max %d. \r\n",
                      __FUNCTION__, QOS_Q_TEMP_SIZE_MIN_VAL,
                      QOS_Q_TEMP_SIZE_MAX_VAL);
        CLI_SET_ERR (QOS_CLI_ERR_QTEMP_QSIZE_INVALID);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;

        return (SNMP_FAILURE);
    }

    i4RetStatus = QoSUtlValidateQTemplateSize (u4FsQoSQTemplateId,
                                               u4TestValFsQoSQTemplateSize,
                                               pu4ErrorCode);
    if (i4RetStatus == QOS_FAILURE)
    {
        CLI_SET_ERR (QOS_CLI_ERR_QTEMP_QSIZE_INVALID);
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlValidateQTemplateSize () "
                      "Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhTestv2FsQoSQTemplateStatus
 Input       :  The Indices
                FsQoSQTemplateId

                The Object 
                testValFsQoSQTemplateStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsQoSQTemplateStatus (UINT4 *pu4ErrorCode,
                               UINT4 u4FsQoSQTemplateId,
                               INT4 i4TestValFsQoSQTemplateStatus)
{
    INT4                i4RetStatus = QOS_FAILURE;
    UINT4               u4Count = 0;
    tQoSQTemplateNode  *pQTempNode = NULL;
    INT4                i4ReturnValue = SNMP_SUCCESS;

    if (gQoSGlobalInfo.eSysControl != QOS_SYS_CNTL_START)
    {
        QOS_TRC (MGMT_TRC, QOS_MODULE_SHUTDOW_ERROR_MSG);
        CLI_SET_ERR (QOS_CLI_ERR_MODULE_SHUTDOW);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return (SNMP_FAILURE);
    }

    *pu4ErrorCode = SNMP_ERR_NO_ERROR;

    /* Check the Range of Index Values (Min-Max) */
    i4RetStatus = QOS_CHECK_Q_TEMP_TBL_INDEX_RANGE (u4FsQoSQTemplateId);

    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG3 (MGMT_TRC, "%s : Q Template Id %d is out of Range."
                      " The Range Should be (1-%d). \r\n", __FUNCTION__,
                      u4FsQoSQTemplateId, QOS_Q_TEMP_TBL_MAX_INDEX_RANGE);
        CLI_SET_ERR (QOS_CLI_ERR_QTEMP_RANGE);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        i4ReturnValue = SNMP_FAILURE;
    }

    pQTempNode = QoSUtlGetQTempNode (u4FsQoSQTemplateId);

    if (pQTempNode == NULL)
    {
        /* Entry Not Found In The Table */
        switch (i4TestValFsQoSQTemplateStatus)
        {
            case ACTIVE:
            case NOT_IN_SERVICE:
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                i4ReturnValue = SNMP_FAILURE;
                break;

            case CREATE_AND_GO:
            case CREATE_AND_WAIT:

                /* Check the No of Entries in the Table */
                u4Count = TMO_SLL_Count (&(gQoSGlobalInfo.SllQTempTbl));

                if (u4Count > QOS_Q_TEMP_TBL_MAX_ENTRIES)
                {
                    QOS_TRC_ARG2 (MGMT_TRC, "%s : Max No of Q Template Table "
                                  "Entries are Configured. No of Max Entries"
                                  " %d. \r\n", __FUNCTION__,
                                  QOS_Q_TEMP_TBL_MAX_ENTRIES);
                    CLI_SET_ERR (QOS_CLI_ERR_QTEMP_MAX_ENTRY);
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;

                    i4ReturnValue = SNMP_FAILURE;
                }

                break;

            case DESTROY:
                CLI_SET_ERR (QOS_CLI_ERR_QTEMP_NOT_CREATION);
                *pu4ErrorCode = SNMP_ERR_NO_CREATION;
                i4ReturnValue = SNMP_FAILURE;
                break;

            default:
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                i4ReturnValue = SNMP_FAILURE;
                break;
        }
    }
    else
    {
        /* Entry Found In The Table */
        switch (i4TestValFsQoSQTemplateStatus)
        {
            case ACTIVE:

                if ((pQTempNode->u1Status == NOT_READY))
                {
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                    i4ReturnValue = SNMP_FAILURE;
                }

                break;

            case NOT_IN_SERVICE:

                if ((pQTempNode->u1Status == NOT_READY))
                {
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                    i4ReturnValue = SNMP_FAILURE;
                }

                /* 1. Check the entry in the Internal Reference.
                 * can we delete the node or not */
                i4RetStatus = QOS_CHECK_TABLE_REF_COUNT (pQTempNode);
                if (i4RetStatus == QOS_FAILURE)
                {
                    QOS_TRC_ARG1 (MGMT_TRC, "In %s : ERROR - This Entry is "
                                  "referenced by Q Table Entry.\r\n",
                                  __FUNCTION__);
                    CLI_SET_ERR (QOS_CLI_ERR_QTEMP_RFE);
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;

                    i4ReturnValue = SNMP_FAILURE;
                }

                break;

            case DESTROY:
                /* Check(Test) is it Default Entry. */
                i4RetStatus = QoSIsDefQTempTblEntry (pQTempNode);
                if (i4RetStatus == QOS_SUCCESS)
                {
                    QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSIsQTDefEntry  "
                                  "Returns FAILURE.\r\n", __FUNCTION__);
                    CLI_SET_ERR (QOS_CLI_ERR_DEF_ENTRY);
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                    i4ReturnValue = SNMP_FAILURE;
                }

                /* 1. Check the entry in the Internal Reference.
                 * can we delete the node or not */
                i4RetStatus = QOS_CHECK_TABLE_REF_COUNT (pQTempNode);
                if (i4RetStatus == QOS_FAILURE)
                {
                    QOS_TRC_ARG1 (MGMT_TRC, "In %s : ERROR - This Entry is "
                                  "referenced by Q Table Entry.\r\n",
                                  __FUNCTION__);
                    CLI_SET_ERR (QOS_CLI_ERR_QTEMP_RFE);
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;

                    i4ReturnValue = SNMP_FAILURE;
                }

                i4RetStatus = QoSValidateDeleteQTempTblEntry (pQTempNode);

                if (i4RetStatus == QOS_FAILURE)
                {
                    QOS_TRC_ARG1 (MGMT_TRC, "In %s : ERROR - This Entry is "
                                  "referenced by RD Table entry.\r\n",
                                  __FUNCTION__);
                    CLI_SET_ERR (QOS_CLI_ERR_QTEMP_RD_CFG_RFE);
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;

                    i4ReturnValue = SNMP_FAILURE;
                }

                break;

            case CREATE_AND_WAIT:
            case CREATE_AND_GO:
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                i4ReturnValue = SNMP_FAILURE;
                break;

            default:
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                i4ReturnValue = SNMP_FAILURE;
                break;

        }                        /* End of switch */

    }                            /* End of if */

    return ((INT1) i4ReturnValue);

}

/* LOW LEVEL Routines for Table : FsQoSRandomDetectCfgTable. */
/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsQoSRandomDetectCfgTable
 Input       :  The Indices
                FsQoSQTemplateId
                FsQoSRandomDetectCfgDP
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsQoSRandomDetectCfgTable (UINT4 u4FsQoSQTemplateId,
                                                   INT4
                                                   i4FsQoSRandomDetectCfgDP)
{
    INT4                i4RetStatus = 0;
    tQoSRDCfgNode      *pRDCfgNode = NULL;

    if (gQoSGlobalInfo.eSysControl != QOS_SYS_CNTL_START)
    {
        QOS_TRC (MGMT_TRC, QOS_MODULE_SHUTDOW_ERROR_MSG);
        return (SNMP_FAILURE);
    }

    /* Check the Range of Index (QTEMP) Values (Min-Max) */
    i4RetStatus = QOS_CHECK_RD_CFG_TBL_QT_INDEX_RANGE (u4FsQoSQTemplateId);
    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG3 (MGMT_TRC, "%s : RD Cfg Q Template Id %d is out of Range."
                      " The Range Should be (1-%d). \r\n", __FUNCTION__,
                      u4FsQoSQTemplateId, QOS_Q_TEMP_TBL_MAX_INDEX_RANGE);

        return (SNMP_FAILURE);
    }

    /* Check the Range of Index (DP) Values (Min-Max) */
    if ((i4FsQoSRandomDetectCfgDP < QOS_RD_CONFG_DP_MIN_VAL) ||
        (i4FsQoSRandomDetectCfgDP > QOS_RD_CONFG_DP_MAX_VAL))
    {
        QOS_TRC_ARG3 (MGMT_TRC, "%s : RD Cfg DP %d is out of Range."
                      " The Range Should be (1-%d). \r\n", __FUNCTION__,
                      i4FsQoSRandomDetectCfgDP, QOS_RD_CONFG_DP_MAX_VAL);

        return (SNMP_FAILURE);
    }

    pRDCfgNode = QoSUtlGetRDCfgNode (u4FsQoSQTemplateId,
                                     i4FsQoSRandomDetectCfgDP);
    if (pRDCfgNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetQTempNode () Returns "
                      "FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    return (SNMP_SUCCESS);

}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsQoSQTemplateTable
 Input       :  The Indices
                FsQoSQTemplateId
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsQoSQTemplateTable (UINT4 *pu4ErrorCode,
                             tSnmpIndexList * pSnmpIndexList,
                             tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsQoSRandomDetectCfgTable
 Input       :  The Indices
                FsQoSQTemplateId
                FsQoSRandomDetectCfgDP
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsQoSRandomDetectCfgTable (UINT4 *pu4FsQoSQTemplateId,
                                           INT4 *pi4FsQoSRandomDetectCfgDP)
{
    INT4                i4RetStatus = QOS_FAILURE;

    if (gQoSGlobalInfo.eSysControl != QOS_SYS_CNTL_START)
    {
        QOS_TRC (MGMT_TRC, QOS_MODULE_SHUTDOW_ERROR_MSG);
        return (SNMP_FAILURE);
    }

    i4RetStatus = QoSGetNextRDCfgTblEntryIndex (0, pu4FsQoSQTemplateId,
                                                0, pi4FsQoSRandomDetectCfgDP);
    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSGetNextRDCfgTblEntryIndex () "
                      "Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhGetNextIndexFsQoSRandomDetectCfgTable
 Input       :  The Indices
                FsQoSQTemplateId
                nextFsQoSQTemplateId
                FsQoSRandomDetectCfgDP
                nextFsQoSRandomDetectCfgDP
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsQoSRandomDetectCfgTable (UINT4 u4FsQoSQTemplateId,
                                          UINT4 *pu4NextFsQoSQTemplateId,
                                          INT4 i4FsQoSRandomDetectCfgDP,
                                          INT4 *pi4NextFsQoSRandomDetectCfgDP)
{
    INT4                i4RetStatus = QOS_FAILURE;

    if (gQoSGlobalInfo.eSysControl != QOS_SYS_CNTL_START)
    {
        QOS_TRC (MGMT_TRC, QOS_MODULE_SHUTDOW_ERROR_MSG);
        return (SNMP_FAILURE);
    }

    /* Check the Range of Index (QTEMP) Values (Min-Max) */
    i4RetStatus = QOS_CHECK_RD_CFG_TBL_QT_INDEX_RANGE (u4FsQoSQTemplateId);

    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG3 (MGMT_TRC, "%s : Q Template Id %d is out of Range."
                      " The Range Should be (1-%d). \r\n", __FUNCTION__,
                      u4FsQoSQTemplateId, QOS_Q_TEMP_TBL_MAX_INDEX_RANGE);

        return (SNMP_FAILURE);
    }

    /* Check the Range of Index (DP) Values (Min-Max) */

    if ((i4FsQoSRandomDetectCfgDP < QOS_RD_CONFG_DP_MIN_VAL) ||
        (i4FsQoSRandomDetectCfgDP > QOS_RD_CONFG_DP_MAX_VAL))
    {
        QOS_TRC_ARG3 (MGMT_TRC, "%s : RD Cfg DP %d is out of Range."
                      " The Range Should be (1-%d). \r\n", __FUNCTION__,
                      i4FsQoSRandomDetectCfgDP, QOS_RD_CONFG_DP_MAX_VAL);

        return (SNMP_FAILURE);
    }

    i4RetStatus = QoSGetNextRDCfgTblEntryIndex (u4FsQoSQTemplateId,
                                                pu4NextFsQoSQTemplateId,
                                                i4FsQoSRandomDetectCfgDP,
                                                pi4NextFsQoSRandomDetectCfgDP);
    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSGetNextRDCfgTblEntryIndex () "
                      "Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    return (SNMP_SUCCESS);

}

/* Low Level GET Routine for All Objects  */
/****************************************************************************
 Function    :  nmhGetFsQoSRandomDetectCfgMinAvgThresh
 Input       :  The Indices
                FsQoSQTemplateId
                FsQoSRandomDetectCfgDP

                The Object 
                retValFsQoSRandomDetectCfgMinAvgThresh
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
     
     
     
     
     
     
     
    nmhGetFsQoSRandomDetectCfgMinAvgThresh
    (UINT4 u4FsQoSQTemplateId,
     INT4 i4FsQoSRandomDetectCfgDP,
     UINT4 *pu4RetValFsQoSRandomDetectCfgMinAvgThresh)
{
    tQoSRDCfgNode      *pRDCfgNode = NULL;

    pRDCfgNode = QoSUtlGetRDCfgNode (u4FsQoSQTemplateId,
                                     i4FsQoSRandomDetectCfgDP);
    if (pRDCfgNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetRDCfgNode () "
                      "Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    *pu4RetValFsQoSRandomDetectCfgMinAvgThresh = pRDCfgNode->u4MinAvgThresh;

    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhGetFsQoSRandomDetectCfgMaxAvgThresh
 Input       :  The Indices
                FsQoSQTemplateId
                FsQoSRandomDetectCfgDP

                The Object 
                retValFsQoSRandomDetectCfgMaxAvgThresh
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
     
     
     
     
     
     
     
    nmhGetFsQoSRandomDetectCfgMaxAvgThresh
    (UINT4 u4FsQoSQTemplateId,
     INT4 i4FsQoSRandomDetectCfgDP,
     UINT4 *pu4RetValFsQoSRandomDetectCfgMaxAvgThresh)
{
    tQoSRDCfgNode      *pRDCfgNode = NULL;

    pRDCfgNode = QoSUtlGetRDCfgNode (u4FsQoSQTemplateId,
                                     i4FsQoSRandomDetectCfgDP);
    if (pRDCfgNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetRDCfgNode () "
                      "Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    *pu4RetValFsQoSRandomDetectCfgMaxAvgThresh = pRDCfgNode->u4MaxAvgThresh;

    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhGetFsQoSRandomDetectCfgMaxPktSize
 Input       :  The Indices
                FsQoSQTemplateId
                FsQoSRandomDetectCfgDP

                The Object 
                retValFsQoSRandomDetectCfgMaxPktSize
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsQoSRandomDetectCfgMaxPktSize (UINT4 u4FsQoSQTemplateId,
                                      INT4 i4FsQoSRandomDetectCfgDP,
                                      UINT4
                                      *pu4RetValFsQoSRandomDetectCfgMaxPktSize)
{
    tQoSRDCfgNode      *pRDCfgNode = NULL;

    pRDCfgNode = QoSUtlGetRDCfgNode (u4FsQoSQTemplateId,
                                     i4FsQoSRandomDetectCfgDP);
    if (pRDCfgNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetRDCfgNode () "
                      "Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    *pu4RetValFsQoSRandomDetectCfgMaxPktSize = pRDCfgNode->u4MaxPktSize;

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFsQoSRandomDetectCfgMaxProb
 Input       :  The Indices
                FsQoSQTemplateId
                FsQoSRandomDetectCfgDP

                The Object 
                retValFsQoSRandomDetectCfgMaxProb
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsQoSRandomDetectCfgMaxProb (UINT4 u4FsQoSQTemplateId,
                                   INT4 i4FsQoSRandomDetectCfgDP,
                                   UINT4 *pu4RetValFsQoSRandomDetectCfgMaxProb)
{
    tQoSRDCfgNode      *pRDCfgNode = NULL;

    pRDCfgNode = QoSUtlGetRDCfgNode (u4FsQoSQTemplateId,
                                     i4FsQoSRandomDetectCfgDP);
    if (pRDCfgNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetRDCfgNode () "
                      "Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    *pu4RetValFsQoSRandomDetectCfgMaxProb = (UINT4) pRDCfgNode->u1MaxProb;

    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhGetFsQoSRandomDetectCfgExpWeight
 Input       :  The Indices
                FsQoSQTemplateId
                FsQoSRandomDetectCfgDP

                The Object 
                retValFsQoSRandomDetectCfgExpWeight
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsQoSRandomDetectCfgExpWeight (UINT4 u4FsQoSQTemplateId,
                                     INT4 i4FsQoSRandomDetectCfgDP,
                                     UINT4
                                     *pu4RetValFsQoSRandomDetectCfgExpWeight)
{
    tQoSRDCfgNode      *pRDCfgNode = NULL;

    pRDCfgNode = QoSUtlGetRDCfgNode (u4FsQoSQTemplateId,
                                     i4FsQoSRandomDetectCfgDP);
    if (pRDCfgNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetRDCfgNode () "
                      "Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    *pu4RetValFsQoSRandomDetectCfgExpWeight = (UINT4) pRDCfgNode->u1ExpWeight;

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFsQoSRandomDetectCfgStatus
 Input       :  The Indices
                FsQoSQTemplateId
                FsQoSRandomDetectCfgDP

                The Object 
                retValFsQoSRandomDetectCfgStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsQoSRandomDetectCfgStatus (UINT4 u4FsQoSQTemplateId,
                                  INT4 i4FsQoSRandomDetectCfgDP,
                                  INT4 *pi4RetValFsQoSRandomDetectCfgStatus)
{
    tQoSRDCfgNode      *pRDCfgNode = NULL;

    pRDCfgNode = QoSUtlGetRDCfgNode (u4FsQoSQTemplateId,
                                     i4FsQoSRandomDetectCfgDP);
    if (pRDCfgNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetRDCfgNode () "
                      "Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    *pi4RetValFsQoSRandomDetectCfgStatus = (UINT4) pRDCfgNode->u1Status;

    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhGetFsQoSRandomDetectCfgGain
 Input       :  The Indices
                FsQoSQTemplateId
                FsQoSRandomDetectCfgDP

                The Object
                retValFsQoSRandomDetectCfgGain
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsQoSRandomDetectCfgGain (UINT4 u4FsQoSQTemplateId,
                                INT4 i4FsQoSRandomDetectCfgDP,
                                UINT4 *pu4RetValFsQoSRandomDetectCfgGain)
{
    tQoSRDCfgNode      *pRDCfgNode = NULL;

    pRDCfgNode = QoSUtlGetRDCfgNode (u4FsQoSQTemplateId,
                                     i4FsQoSRandomDetectCfgDP);
    if (pRDCfgNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetRDCfgNode () "
                      "Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    *pu4RetValFsQoSRandomDetectCfgGain = pRDCfgNode->u4Gain;

    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhGetFsQoSRandomDetectCfgDropThreshType
 Input       :  The Indices
                FsQoSQTemplateId
                FsQoSRandomDetectCfgDP

                The Object
                retValFsQoSRandomDetectCfgDropThreshType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsQoSRandomDetectCfgDropThreshType (UINT4 u4FsQoSQTemplateId,
                                          INT4 i4FsQoSRandomDetectCfgDP,
                                          INT4
                                          *pi4RetValFsQoSRandomDetectCfgDropThreshType)
{
    tQoSRDCfgNode      *pRDCfgNode = NULL;

    pRDCfgNode = QoSUtlGetRDCfgNode (u4FsQoSQTemplateId,
                                     i4FsQoSRandomDetectCfgDP);
    if (pRDCfgNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetRDCfgNode () "
                      "Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    *pi4RetValFsQoSRandomDetectCfgDropThreshType = pRDCfgNode->u1DropThreshType;

    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhGetFsQoSRandomDetectCfgECNThresh
 Input       :  The Indices
                FsQoSQTemplateId
                FsQoSRandomDetectCfgDP

                The Object
                retValFsQoSRandomDetectCfgECNThresh
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsQoSRandomDetectCfgECNThresh (UINT4 u4FsQoSQTemplateId,
                                     INT4 i4FsQoSRandomDetectCfgDP,
                                     UINT4
                                     *pu4RetValFsQoSRandomDetectCfgECNThresh)
{
    tQoSRDCfgNode      *pRDCfgNode = NULL;

    pRDCfgNode = QoSUtlGetRDCfgNode (u4FsQoSQTemplateId,
                                     i4FsQoSRandomDetectCfgDP);
    if (pRDCfgNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetRDCfgNode () "
                      "Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    *pu4RetValFsQoSRandomDetectCfgECNThresh = pRDCfgNode->u4ECNThresh;

    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhGetFsQoSRandomDetectCfgActionFlag
 Input       :  The Indices
                FsQoSQTemplateId
                FsQoSRandomDetectCfgDP

                The Object
                retValFsQoSRandomDetectCfgActionFlag
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsQoSRandomDetectCfgActionFlag (UINT4 u4FsQoSQTemplateId,
                                      INT4 i4FsQoSRandomDetectCfgDP,
                                      tSNMP_OCTET_STRING_TYPE *
                                      pRetValFsQoSRandomDetectCfgActionFlag)
{
    tQoSRDCfgNode      *pRDCfgNode = NULL;
    UINT1               u1ActionFlag = 0;

    pRDCfgNode = QoSUtlGetRDCfgNode (u4FsQoSQTemplateId,
                                     i4FsQoSRandomDetectCfgDP);
    if (pRDCfgNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetRDCfgNode () "
                      "Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }
    u1ActionFlag = pRDCfgNode->u1RDActionFlag;

    QOS_GET_RD_CFG_ACTION_MASK (pRetValFsQoSRandomDetectCfgActionFlag,
                                u1ActionFlag);

    return (SNMP_SUCCESS);

}

/* Low Level SET Routine for All Objects  */
/****************************************************************************
 Function    :  nmhSetFsQoSRandomDetectCfgMinAvgThresh
 Input       :  The Indices
                FsQoSQTemplateId
                FsQoSRandomDetectCfgDP

                The Object 
                setValFsQoSRandomDetectCfgMinAvgThresh
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
     
     
     
     
     
     
     
    nmhSetFsQoSRandomDetectCfgMinAvgThresh
    (UINT4 u4FsQoSQTemplateId,
     INT4 i4FsQoSRandomDetectCfgDP,
     UINT4 u4SetValFsQoSRandomDetectCfgMinAvgThresh)
{
    tQoSRDCfgNode      *pRDCfgNode = NULL;

    pRDCfgNode = QoSUtlGetRDCfgNode (u4FsQoSQTemplateId,
                                     i4FsQoSRandomDetectCfgDP);
    if (pRDCfgNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetRDCfgNode () "
                      "Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    pRDCfgNode->u4MinAvgThresh = u4SetValFsQoSRandomDetectCfgMinAvgThresh;

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhSetFsQoSRandomDetectCfgMaxAvgThresh
 Input       :  The Indices
                FsQoSQTemplateId
                FsQoSRandomDetectCfgDP

                The Object 
                setValFsQoSRandomDetectCfgMaxAvgThresh
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
     
     
     
     
     
     
     
    nmhSetFsQoSRandomDetectCfgMaxAvgThresh
    (UINT4 u4FsQoSQTemplateId,
     INT4 i4FsQoSRandomDetectCfgDP,
     UINT4 u4SetValFsQoSRandomDetectCfgMaxAvgThresh)
{
    tQoSRDCfgNode      *pRDCfgNode = NULL;

    pRDCfgNode = QoSUtlGetRDCfgNode (u4FsQoSQTemplateId,
                                     i4FsQoSRandomDetectCfgDP);
    if (pRDCfgNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetRDCfgNode () "
                      "Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    pRDCfgNode->u4MaxAvgThresh = u4SetValFsQoSRandomDetectCfgMaxAvgThresh;

    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhSetFsQoSRandomDetectCfgMaxPktSize
 Input       :  The Indices
                FsQoSQTemplateId
                FsQoSRandomDetectCfgDP

                The Object 
                setValFsQoSRandomDetectCfgMaxPktSize
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsQoSRandomDetectCfgMaxPktSize (UINT4 u4FsQoSQTemplateId,
                                      INT4 i4FsQoSRandomDetectCfgDP,
                                      UINT4
                                      u4SetValFsQoSRandomDetectCfgMaxPktSize)
{
    tQoSRDCfgNode      *pRDCfgNode = NULL;

    pRDCfgNode = QoSUtlGetRDCfgNode (u4FsQoSQTemplateId,
                                     i4FsQoSRandomDetectCfgDP);
    if (pRDCfgNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetRDCfgNode () "
                      "Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    pRDCfgNode->u4MaxPktSize = u4SetValFsQoSRandomDetectCfgMaxPktSize;

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhSetFsQoSRandomDetectCfgMaxProb
 Input       :  The Indices
                FsQoSQTemplateId
                FsQoSRandomDetectCfgDP

                The Object 
                setValFsQoSRandomDetectCfgMaxProb
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsQoSRandomDetectCfgMaxProb (UINT4 u4FsQoSQTemplateId,
                                   INT4 i4FsQoSRandomDetectCfgDP,
                                   UINT4 u4SetValFsQoSRandomDetectCfgMaxProb)
{
    tQoSRDCfgNode      *pRDCfgNode = NULL;

    pRDCfgNode = QoSUtlGetRDCfgNode (u4FsQoSQTemplateId,
                                     i4FsQoSRandomDetectCfgDP);
    if (pRDCfgNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetRDCfgNode () "
                      "Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    pRDCfgNode->u1MaxProb = (UINT1) u4SetValFsQoSRandomDetectCfgMaxProb;

    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhSetFsQoSRandomDetectCfgExpWeight
 Input       :  The Indices
                FsQoSQTemplateId
                FsQoSRandomDetectCfgDP

                The Object 
                setValFsQoSRandomDetectCfgExpWeight
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsQoSRandomDetectCfgExpWeight (UINT4 u4FsQoSQTemplateId,
                                     INT4 i4FsQoSRandomDetectCfgDP,
                                     UINT4
                                     u4SetValFsQoSRandomDetectCfgExpWeight)
{
    tQoSRDCfgNode      *pRDCfgNode = NULL;

    pRDCfgNode = QoSUtlGetRDCfgNode (u4FsQoSQTemplateId,
                                     i4FsQoSRandomDetectCfgDP);
    if (pRDCfgNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetRDCfgNode () "
                      "Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    pRDCfgNode->u1ExpWeight = (UINT1) u4SetValFsQoSRandomDetectCfgExpWeight;

    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhSetFsQoSRandomDetectCfgStatus
 Input       :  The Indices
                FsQoSQTemplateId
                FsQoSRandomDetectCfgDP

                The Object 
                setValFsQoSRandomDetectCfgStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsQoSRandomDetectCfgStatus (UINT4 u4FsQoSQTemplateId,
                                  INT4 i4FsQoSRandomDetectCfgDP,
                                  INT4 i4SetValFsQoSRandomDetectCfgStatus)
{
    tQoSRDCfgNode      *pRDCfgNode = NULL;
    INT4                i4RetStatus = QOS_FAILURE;
    INT4                i4ReturnValue = SNMP_SUCCESS;

    pRDCfgNode = QoSUtlGetRDCfgNode (u4FsQoSQTemplateId,
                                     i4FsQoSRandomDetectCfgDP);
    if (pRDCfgNode != NULL)
    {
        /* Entry found in the Table */
        if (pRDCfgNode->u1Status == i4SetValFsQoSRandomDetectCfgStatus)
        {
            /* same value */
            return (SNMP_SUCCESS);
        }
    }
    else
    {
        /* New Entry need to be created in the Table, only if 
         * i4SetValFsQoSRandomDetectCfgStatus = CREATE_AND_WAIT */
        /* Optional */
        if (i4SetValFsQoSRandomDetectCfgStatus == DESTROY)
        {
            return (SNMP_SUCCESS);
        }
    }

    switch (i4SetValFsQoSRandomDetectCfgStatus)
    {
            /* For a New Entry */
        case CREATE_AND_GO:

            pRDCfgNode = QoSCreateRDCfgTblEntry (u4FsQoSQTemplateId,
                                                 i4FsQoSRandomDetectCfgDP);
            if (pRDCfgNode == NULL)
            {
                QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSCreateRDCfgTblEntry () "
                              "Returns FAILURE. \r\n", __FUNCTION__);

                return (SNMP_FAILURE);
            }
            if (pRDCfgNode != NULL)
            {
                pRDCfgNode->u1Status = ACTIVE;
            }

            i4ReturnValue = SNMP_SUCCESS;
            break;

        case CREATE_AND_WAIT:

            pRDCfgNode = QoSCreateRDCfgTblEntry (u4FsQoSQTemplateId,
                                                 i4FsQoSRandomDetectCfgDP);
            if (pRDCfgNode == NULL)
            {
                QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSCreateRDCfgTblEntry () "
                              "Returns FAILURE. \r\n", __FUNCTION__);

                return (SNMP_FAILURE);
            }
            if (pRDCfgNode != NULL)
            {
                pRDCfgNode->u1Status = NOT_IN_SERVICE;
            }
            i4ReturnValue = SNMP_SUCCESS;

            break;

            /* Modify an Entry */
        case ACTIVE:

            if (pRDCfgNode != NULL)
            {
                pRDCfgNode->u1Status = ACTIVE;
            }

            break;

        case NOT_IN_SERVICE:
            if (pRDCfgNode != NULL)
            {
                pRDCfgNode->u1Status = NOT_IN_SERVICE;
            }

            break;

        case DESTROY:

            /* 1. Remove the entry from the SOFTWARE. */
            i4RetStatus = QoSDeleteRDCfgTblEntry (pRDCfgNode);
            if (i4RetStatus == QOS_FAILURE)
            {
                QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSDeleteRDCfgTblEntry () "
                              "Returns FAILURE. \r\n", __FUNCTION__);

                i4ReturnValue = SNMP_FAILURE;
            }

            break;

        default:
            i4ReturnValue = SNMP_FAILURE;
            break;
    }
    if (i4ReturnValue == SNMP_SUCCESS)
    {
        QosUpdateDiffServRandomDropNextFree (u4FsQoSQTemplateId,
                                             i4SetValFsQoSRandomDetectCfgStatus);
    }
    return ((INT1) i4ReturnValue);
}

/****************************************************************************
 Function    :  nmhSetFsQoSRandomDetectCfgGain
 Input       :  The Indices
                FsQoSQTemplateId
                FsQoSRandomDetectCfgDP

                The Object
                setValFsQoSRandomDetectCfgGain
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsQoSRandomDetectCfgGain (UINT4 u4FsQoSQTemplateId,
                                INT4 i4FsQoSRandomDetectCfgDP,
                                UINT4 u4SetValFsQoSRandomDetectCfgGain)
{
    tQoSRDCfgNode      *pRDCfgNode = NULL;

    pRDCfgNode = QoSUtlGetRDCfgNode (u4FsQoSQTemplateId,
                                     i4FsQoSRandomDetectCfgDP);
    if (pRDCfgNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetRDCfgNode () "
                      "Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    pRDCfgNode->u4Gain = u4SetValFsQoSRandomDetectCfgGain;

    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhSetFsQoSRandomDetectCfgDropThreshType
 Input       :  The Indices
                FsQoSQTemplateId
                FsQoSRandomDetectCfgDP

                The Object
                setValFsQoSRandomDetectCfgDropThreshType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsQoSRandomDetectCfgDropThreshType (UINT4 u4FsQoSQTemplateId,
                                          INT4 i4FsQoSRandomDetectCfgDP,
                                          INT4
                                          i4SetValFsQoSRandomDetectCfgDropThreshType)
{
    tQoSRDCfgNode      *pRDCfgNode = NULL;

    pRDCfgNode = QoSUtlGetRDCfgNode (u4FsQoSQTemplateId,
                                     i4FsQoSRandomDetectCfgDP);
    if (pRDCfgNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetRDCfgNode () "
                      "Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    pRDCfgNode->u1DropThreshType =
        (UINT1) i4SetValFsQoSRandomDetectCfgDropThreshType;

    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhSetFsQoSRandomDetectCfgECNThresh
 Input       :  The Indices
                FsQoSQTemplateId
                FsQoSRandomDetectCfgDP

                The Object
                setValFsQoSRandomDetectCfgECNThresh
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsQoSRandomDetectCfgECNThresh (UINT4 u4FsQoSQTemplateId,
                                     INT4 i4FsQoSRandomDetectCfgDP,
                                     UINT4
                                     u4SetValFsQoSRandomDetectCfgECNThresh)
{
    tQoSRDCfgNode      *pRDCfgNode = NULL;

    pRDCfgNode = QoSUtlGetRDCfgNode (u4FsQoSQTemplateId,
                                     i4FsQoSRandomDetectCfgDP);
    if (pRDCfgNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetRDCfgNode () "
                      "Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    pRDCfgNode->u4ECNThresh = u4SetValFsQoSRandomDetectCfgECNThresh;

    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhSetFsQoSRandomDetectCfgActionFlag
 Input       :  The Indices
                FsQoSQTemplateId
                FsQoSRandomDetectCfgDP

                The Object
                setValFsQoSRandomDetectCfgActionFlag
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsQoSRandomDetectCfgActionFlag (UINT4 u4FsQoSQTemplateId,
                                      INT4 i4FsQoSRandomDetectCfgDP,
                                      tSNMP_OCTET_STRING_TYPE *
                                      pSetValFsQoSRandomDetectCfgActionFlag)
{
    tQoSRDCfgNode      *pRDCfgNode = NULL;

    pRDCfgNode = QoSUtlGetRDCfgNode (u4FsQoSQTemplateId,
                                     i4FsQoSRandomDetectCfgDP);
    if (pRDCfgNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetRDCfgNode () "
                      "Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    pRDCfgNode->u1RDActionFlag = 0;
    QOS_SET_RD_CFG_ACTION_MASK (&(pRDCfgNode->u1RDActionFlag),
                                pSetValFsQoSRandomDetectCfgActionFlag);

    return (SNMP_SUCCESS);

}

/* Low Level TEST Routines for All Objects  */
/****************************************************************************
 Function    :  nmhTestv2FsQoSRandomDetectCfgMinAvgThresh
 Input       :  The Indices
                FsQoSQTemplateId
                FsQoSRandomDetectCfgDP

                The Object 
                testValFsQoSRandomDetectCfgMinAvgThresh
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
     
     
     
     
     
     
     
    nmhTestv2FsQoSRandomDetectCfgMinAvgThresh
    (UINT4 *pu4ErrorCode,
     UINT4 u4FsQoSQTemplateId,
     INT4 i4FsQoSRandomDetectCfgDP,
     UINT4 u4TestValFsQoSRandomDetectCfgMinAvgThresh)
{
    tQoSRDCfgNode      *pRDCfgNode = NULL;
    tQoSQTemplateNode  *pQTempNode = NULL;
    INT4                i4RetStatus = QOS_FAILURE;
    UINT4               u4MaxTH = 0;
    *pu4ErrorCode = SNMP_ERR_NO_ERROR;

    /* Check Index */
    i4RetStatus = QoSUtlValidateRDCfgTblIdxInst (u4FsQoSQTemplateId,
                                                 i4FsQoSRandomDetectCfgDP,
                                                 pu4ErrorCode);
    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlValidateRDCfgTblIdxInst () "
                      "Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    pRDCfgNode = QoSUtlGetRDCfgNode (u4FsQoSQTemplateId,
                                     i4FsQoSRandomDetectCfgDP);
    if (pRDCfgNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetRDCfgNode () "
                      "Returns FAILURE. \r\n", __FUNCTION__);
        return (SNMP_FAILURE);
    }

    pQTempNode = QoSUtlGetQTempNode (u4FsQoSQTemplateId);

    if (pQTempNode != NULL)
    {
        /* MinTh must be less than or equal to the QTemplate Size */
        if (u4TestValFsQoSRandomDetectCfgMinAvgThresh > pQTempNode->u4Size)
        {
            QOS_TRC_ARG1 (MGMT_TRC,
                          "%s : RD Cfg MinAvgThresh value is greater than "
                          "Q Template Q-limit size. \r\n", __FUNCTION__);

            CLI_SET_ERR (QOS_CLI_ERR_RDCFG_MIN_AVG_TH);
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return (SNMP_FAILURE);
        }
    }

    /* Check  Object's Value */
    if (pRDCfgNode->u1DropThreshType == QOS_RD_CONFIG_DROP_THRESH_TYPE_PKTS)
    {
        if ((u4TestValFsQoSRandomDetectCfgMinAvgThresh <
             QOS_RD_CONFG_MIN_AVG_TH_MIN_VAL) ||
            (u4TestValFsQoSRandomDetectCfgMinAvgThresh >
             QOS_RD_CONFG_MIN_AVG_TH_MAX_VAL))
        {
            QOS_TRC_ARG3 (MGMT_TRC, "%s : RD Cfg MinAvgThresh  value is out of "
                          "range. Range should be Min %d - Max %d. \r\n",
                          __FUNCTION__, QOS_RD_CONFG_MIN_AVG_TH_MIN_VAL,
                          QOS_RD_CONFG_MIN_AVG_TH_MAX_VAL);

            CLI_SET_ERR (QOS_CLI_ERR_RDCFG_MIN_AVG_TH);
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return (SNMP_FAILURE);
        }
    }

    else if (pRDCfgNode->u1DropThreshType ==
             QOS_RD_CONFIG_DROP_THRESH_TYPE_BYTES)
    {
        if ((u4TestValFsQoSRandomDetectCfgMinAvgThresh <
             QOS_RD_CONFG_MIN_AVG_TH_BYTES_MIN_VAL) ||
            (u4TestValFsQoSRandomDetectCfgMinAvgThresh >
             QOS_RD_CONFG_MIN_AVG_TH_BYTES_MAX_VAL))
        {
            QOS_TRC_ARG3 (MGMT_TRC, "%s : RD Cfg MinAvgThresh value is out of "
                          "range. Range should be Min %d - Max %d for DropThreshtype Bytes. \r\n",
                          __FUNCTION__, QOS_RD_CONFG_MIN_AVG_TH_BYTES_MIN_VAL,
                          QOS_RD_CONFG_MIN_AVG_TH_BYTES_MAX_VAL);

            CLI_SET_ERR (QOS_CLI_ERR_RDCFG_MIN_AVG_TH);
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;

            return (SNMP_FAILURE);
        }
    }

    /*If only min threshold value is given by the user, check whether the min threshold
     * is less than the max thershold value present in the system */
    nmhGetFsQoSRandomDetectCfgMaxAvgThresh (u4FsQoSQTemplateId,
                                            i4FsQoSRandomDetectCfgDP, &u4MaxTH);
    if (u4TestValFsQoSRandomDetectCfgMinAvgThresh > u4MaxTH)
    {
        CLI_SET_ERR (QOS_CLI_ERR_INVALID_MIN_AVG_TH);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhTestv2FsQoSRandomDetectCfgMaxAvgThresh
 Input       :  The Indices
                FsQoSQTemplateId
                FsQoSRandomDetectCfgDP

                The Object 
                testValFsQoSRandomDetectCfgMaxAvgThresh
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
     
     
     
     
     
     
     
    nmhTestv2FsQoSRandomDetectCfgMaxAvgThresh
    (UINT4 *pu4ErrorCode,
     UINT4 u4FsQoSQTemplateId,
     INT4 i4FsQoSRandomDetectCfgDP,
     UINT4 u4TestValFsQoSRandomDetectCfgMaxAvgThresh)
{
    tQoSRDCfgNode      *pRDCfgNode = NULL;
    tQoSQTemplateNode  *pQTempNode = NULL;
    INT4                i4RetStatus = QOS_FAILURE;
    UINT4               u4MinTH = 0;
    *pu4ErrorCode = SNMP_ERR_NO_ERROR;

    /* Check Index */
    i4RetStatus = QoSUtlValidateRDCfgTblIdxInst (u4FsQoSQTemplateId,
                                                 i4FsQoSRandomDetectCfgDP,
                                                 pu4ErrorCode);
    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlValidateRDCfgTblIdxInst () "
                      "Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    pRDCfgNode = QoSUtlGetRDCfgNode (u4FsQoSQTemplateId,
                                     i4FsQoSRandomDetectCfgDP);
    if (pRDCfgNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetRDCfgNode () "
                      "Returns FAILURE. \r\n", __FUNCTION__);
        return (SNMP_FAILURE);
    }

    pQTempNode = QoSUtlGetQTempNode (u4FsQoSQTemplateId);

    if (pQTempNode != NULL)
    {
        /* MaxTh must be less than or equal to the QTemplate Size */
        if (u4TestValFsQoSRandomDetectCfgMaxAvgThresh > pQTempNode->u4Size)
        {
            QOS_TRC_ARG1 (MGMT_TRC,
                          "%s : RD Cfg MaxAvgThresh value is greater than "
                          "Q Template Q-limit size. \r\n", __FUNCTION__);

            CLI_SET_ERR (QOS_CLI_ERR_RDCFG_MAX_AVG_TH);
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return (SNMP_FAILURE);
        }
    }

    /* Check  Object's Value */
    if (pRDCfgNode->u1DropThreshType == QOS_RD_CONFIG_DROP_THRESH_TYPE_PKTS)
    {
        if ((u4TestValFsQoSRandomDetectCfgMaxAvgThresh <
             QOS_RD_CONFG_MAX_AVG_TH_MIN_VAL) ||
            (u4TestValFsQoSRandomDetectCfgMaxAvgThresh >
             QOS_RD_CONFG_MAX_AVG_TH_MAX_VAL))
        {
            QOS_TRC_ARG3 (MGMT_TRC, "%s : RD Cfg MaxAvgThresh  value is out of "
                          "range. Range should be Min %d - Max %d. \r\n",
                          __FUNCTION__, QOS_RD_CONFG_MAX_AVG_TH_MIN_VAL,
                          QOS_RD_CONFG_MAX_AVG_TH_MAX_VAL);

            CLI_SET_ERR (QOS_CLI_ERR_RDCFG_MAX_AVG_TH);
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;

            return (SNMP_FAILURE);
        }
    }

    else if (pRDCfgNode->u1DropThreshType ==
             QOS_RD_CONFIG_DROP_THRESH_TYPE_BYTES)
    {
        if ((u4TestValFsQoSRandomDetectCfgMaxAvgThresh <
             QOS_RD_CONFG_MAX_AVG_TH_BYTES_MIN_VAL) ||
            (u4TestValFsQoSRandomDetectCfgMaxAvgThresh >
             QOS_RD_CONFG_MAX_AVG_TH_BYTES_MAX_VAL))
        {
            QOS_TRC_ARG3 (MGMT_TRC, "%s : RD Cfg MaxAvgThresh  value is out of "
                          "range. Range should be Min %d - Max %d for DropThreshtype Bytes. \r\n",
                          __FUNCTION__, QOS_RD_CONFG_MAX_AVG_TH_BYTES_MIN_VAL,
                          QOS_RD_CONFG_MAX_AVG_TH_BYTES_MAX_VAL);

            CLI_SET_ERR (QOS_CLI_ERR_RDCFG_MAX_AVG_TH);
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;

            return (SNMP_FAILURE);
        }
    }

    /*If only max threshold value is given by the user, check whether the max threshold
     * is greater than the min thershold value present in the system*/
    nmhGetFsQoSRandomDetectCfgMinAvgThresh (u4FsQoSQTemplateId,
                                            i4FsQoSRandomDetectCfgDP, &u4MinTH);
    if (u4TestValFsQoSRandomDetectCfgMaxAvgThresh < u4MinTH)
    {
        CLI_SET_ERR (QOS_CLI_ERR_INVALID_MAX_AVG_TH);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }

    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhTestv2FsQoSRandomDetectCfgMaxPktSize
 Input       :  The Indices
                FsQoSQTemplateId
                FsQoSRandomDetectCfgDP

                The Object 
                testValFsQoSRandomDetectCfgMaxPktSize
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
     
     
     
     
     
     
     
    nmhTestv2FsQoSRandomDetectCfgMaxPktSize
    (UINT4 *pu4ErrorCode,
     UINT4 u4FsQoSQTemplateId,
     INT4 i4FsQoSRandomDetectCfgDP,
     UINT4 u4TestValFsQoSRandomDetectCfgMaxPktSize)
{
    INT4                i4RetStatus = QOS_FAILURE;

    *pu4ErrorCode = SNMP_ERR_NO_ERROR;

    /* Check Index */
    i4RetStatus = QoSUtlValidateRDCfgTblIdxInst (u4FsQoSQTemplateId,
                                                 i4FsQoSRandomDetectCfgDP,
                                                 pu4ErrorCode);
    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlValidateRDCfgTblIdxInst () "
                      "Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    /* Check  Object's Value */
    if ((u4TestValFsQoSRandomDetectCfgMaxPktSize <
         QOS_RD_CONFG_MAX_PKT_SIZE_MIN_VAL) ||
        (u4TestValFsQoSRandomDetectCfgMaxPktSize >
         QOS_RD_CONFG_MAX_PKT_SIZE_MAX_VAL))
    {
        QOS_TRC_ARG3 (MGMT_TRC, "%s : RD Cfg MaxPktSize  value is out of "
                      "range. Range should be Min %d - Max %d. \r\n",
                      __FUNCTION__, QOS_RD_CONFG_MAX_PKT_SIZE_MIN_VAL,
                      QOS_RD_CONFG_MAX_PKT_SIZE_MAX_VAL);

        CLI_SET_ERR (QOS_CLI_ERR_RDCFG_MAX_PKT);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;

        return (SNMP_FAILURE);
    }

    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhTestv2FsQoSRandomDetectCfgMaxProb
 Input       :  The Indices
                FsQoSQTemplateId
                FsQoSRandomDetectCfgDP

                The Object 
                testValFsQoSRandomDetectCfgMaxProb
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsQoSRandomDetectCfgMaxProb (UINT4 *pu4ErrorCode,
                                      UINT4 u4FsQoSQTemplateId,
                                      INT4 i4FsQoSRandomDetectCfgDP,
                                      UINT4
                                      u4TestValFsQoSRandomDetectCfgMaxProb)
{
    INT4                i4RetStatus = QOS_FAILURE;

    *pu4ErrorCode = SNMP_ERR_NO_ERROR;

    /* Check Index */
    i4RetStatus = QoSUtlValidateRDCfgTblIdxInst (u4FsQoSQTemplateId,
                                                 i4FsQoSRandomDetectCfgDP,
                                                 pu4ErrorCode);
    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlValidateRDCfgTblIdxInst () "
                      "Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    /* Check  Object's Value */
    if ((u4TestValFsQoSRandomDetectCfgMaxProb <
         QOS_RD_CONFG_MAX_PROB_MIN_VAL) ||
        (u4TestValFsQoSRandomDetectCfgMaxProb > QOS_RD_CONFG_MAX_PROB_MAX_VAL))
    {
        QOS_TRC_ARG3 (MGMT_TRC,
                      "%s : RD Cfg Max Drop Probability  value is "
                      "out of range. Range should be Min %d - Max %d. \r\n",
                      __FUNCTION__, QOS_RD_CONFG_MAX_PROB_MIN_VAL,
                      QOS_RD_CONFG_MAX_PROB_MAX_VAL);

        CLI_SET_ERR (QOS_CLI_ERR_RDCFG_MAX_PROB);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }

    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhTestv2FsQoSRandomDetectCfgExpWeight
 Input       :  The Indices
                FsQoSQTemplateId
                FsQoSRandomDetectCfgDP

                The Object 
                testValFsQoSRandomDetectCfgExpWeight
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsQoSRandomDetectCfgExpWeight (UINT4 *pu4ErrorCode,
                                        UINT4 u4FsQoSQTemplateId,
                                        INT4 i4FsQoSRandomDetectCfgDP,
                                        UINT4
                                        u4TestValFsQoSRandomDetectCfgExpWeight)
{
    INT4                i4RetStatus = QOS_FAILURE;

    *pu4ErrorCode = SNMP_ERR_NO_ERROR;

    /* Check Index */
    i4RetStatus = QoSUtlValidateRDCfgTblIdxInst (u4FsQoSQTemplateId,
                                                 i4FsQoSRandomDetectCfgDP,
                                                 pu4ErrorCode);
    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlValidateRDCfgTblIdxInst () "
                      "Returns FAILURE. \r\n", __FUNCTION__);
        return (SNMP_FAILURE);
    }

    /* Check  Object's Value */
    if (u4TestValFsQoSRandomDetectCfgExpWeight >
        QOS_RD_CONFG_EXP_WEIGHT_MAX_VAL)
    {
        QOS_TRC_ARG3 (MGMT_TRC, "%s : RD Cfg Exponential Weight value is "
                      "out of range. Range should be Min %d - Max %d. \r\n",
                      __FUNCTION__, QOS_RD_CONFG_EXP_WEIGHT_MIN_VAL,
                      QOS_RD_CONFG_EXP_WEIGHT_MAX_VAL);
        CLI_SET_ERR (QOS_CLI_ERR_RDCFG_EXP_WEIGHT);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }

    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhTestv2FsQoSRandomDetectCfgStatus
 Input       :  The Indices
                FsQoSQTemplateId
                FsQoSRandomDetectCfgDP

                The Object 
                testValFsQoSRandomDetectCfgStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsQoSRandomDetectCfgStatus (UINT4 *pu4ErrorCode,
                                     UINT4 u4FsQoSQTemplateId,
                                     INT4 i4FsQoSRandomDetectCfgDP,
                                     INT4 i4TestValFsQoSRandomDetectCfgStatus)
{
    INT4                i4RetStatus = QOS_FAILURE;
    UINT4               u4Count = 0;
    tQoSRDCfgNode      *pRDCfgNode = NULL;
    INT4                i4ReturnValue = SNMP_SUCCESS;

    if (gQoSGlobalInfo.eSysControl != QOS_SYS_CNTL_START)
    {
        QOS_TRC (MGMT_TRC, QOS_MODULE_SHUTDOW_ERROR_MSG);
        CLI_SET_ERR (QOS_CLI_ERR_MODULE_SHUTDOW);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return (SNMP_FAILURE);
    }

    *pu4ErrorCode = SNMP_ERR_NO_ERROR;

    /* Check the Range of Index (QTEMP) Values (Min-Max) */
    i4RetStatus = QOS_CHECK_RD_CFG_TBL_QT_INDEX_RANGE (u4FsQoSQTemplateId);
    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG3 (MGMT_TRC, "%s : Q Template Id %d is out of Range."
                      " The Range Should be (1-%d). \r\n", __FUNCTION__,
                      u4FsQoSQTemplateId, QOS_Q_TEMP_TBL_MAX_INDEX_RANGE);
        CLI_SET_ERR (QOS_CLI_ERR_QTEMP_RANGE);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }

    /* Check the Range of Index (DP) Values (Min-Max) */
    if ((i4FsQoSRandomDetectCfgDP < QOS_RD_CONFG_DP_MIN_VAL) ||
        (i4FsQoSRandomDetectCfgDP > QOS_RD_CONFG_DP_MAX_VAL))
    {
        QOS_TRC_ARG3 (MGMT_TRC, "%s : RD Cfg DP %d is out of Range."
                      " The Range Should be (1-%d). \r\n", __FUNCTION__,
                      i4FsQoSRandomDetectCfgDP, QOS_RD_CONFG_DP_MAX_VAL);
        CLI_SET_ERR (QOS_CLI_ERR_RDCFG_DP_RANGE);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }

    pRDCfgNode = QoSUtlGetRDCfgNode (u4FsQoSQTemplateId,
                                     i4FsQoSRandomDetectCfgDP);
    if (pRDCfgNode == NULL)
    {
        /* Entry Not Found In The Table */
        switch (i4TestValFsQoSRandomDetectCfgStatus)
        {
            case ACTIVE:
            case NOT_IN_SERVICE:
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                i4ReturnValue = SNMP_FAILURE;
                break;

            case CREATE_AND_GO:
            case CREATE_AND_WAIT:

                /* Check the No of Entries in the Table */
                u4Count = TMO_SLL_Count (&(gQoSGlobalInfo.SllRDTbl));
                if (u4Count > QOS_RD_CFG_TBL_MAX_ENTRIES)
                {
                    QOS_TRC_ARG2 (MGMT_TRC, "%s : Max No of RD Cfg Table "
                                  "Entries are Configured. No of Max Entries"
                                  " %d. \r\n", __FUNCTION__,
                                  QOS_RD_CFG_TBL_MAX_ENTRIES);
                    CLI_SET_ERR (QOS_CLI_ERR_RDCFG_MAX_ENTRY);
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                    i4ReturnValue = SNMP_FAILURE;
                }

                break;

            case DESTROY:
                break;

            default:
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                i4ReturnValue = SNMP_FAILURE;
                break;
        }
    }
    else
    {
        /* Entry Found In The Table */
        switch (i4TestValFsQoSRandomDetectCfgStatus)
        {
            case ACTIVE:

                if ((pRDCfgNode->u1Status == NOT_READY))
                {
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                    i4ReturnValue = SNMP_FAILURE;
                }

                break;

            case NOT_IN_SERVICE:

                if ((pRDCfgNode->u1Status == NOT_READY))
                {
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                    i4ReturnValue = SNMP_FAILURE;
                }

                /* 1. Check the entry in the Internal Reference.
                 * can we delete the node or not */
                /* Remove RefCount for the Struct */
                /* i4RetStatus = QOS_CHECK_TABLE_REF_COUNT (pRDCfgNode); */

                /* Using the QTempId check the QTempId No DropAlgo is Enabled
                 * or not if not referenced then Remove */
                i4RetStatus = QoSUtlCheckQTempReference (u4FsQoSQTemplateId);
                if (i4RetStatus == QOS_SUCCESS)
                {
                    QOS_TRC_ARG1 (MGMT_TRC, "In %s : ERROR - This Entry is "
                                  "referenced by QTemp Table Entry.\r\n",
                                  __FUNCTION__);
                    CLI_SET_ERR (QOS_CLI_ERR_RDCFG_REF);
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                    i4ReturnValue = SNMP_FAILURE;
                }

                break;

            case DESTROY:
                /* 1. Check the entry in the Internal Reference.
                 * Using the QTempId check the QTempId No DropAlgo is Enabled
                 * or not if not referenced then Remove */
                i4RetStatus = QoSUtlCheckQTempReference (u4FsQoSQTemplateId);
                if (i4RetStatus == QOS_SUCCESS)
                {
                    QOS_TRC_ARG1 (MGMT_TRC, "In %s : ERROR - This Entry is "
                                  "referenced by QTemp Table Entry.\r\n",
                                  __FUNCTION__);
                    CLI_SET_ERR (QOS_CLI_ERR_RDCFG_REF);
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                    i4ReturnValue = SNMP_FAILURE;
                }

                break;

            case CREATE_AND_WAIT:
            case CREATE_AND_GO:
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                i4ReturnValue = SNMP_FAILURE;
                break;

            default:
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                i4ReturnValue = SNMP_FAILURE;
                break;

        }                        /* End of switch */

    }                            /* End of if */

    return ((INT1) i4ReturnValue);
}

/****************************************************************************
 Function    :  nmhTestv2FsQoSRandomDetectCfgGain
 Input       :  The Indices
                FsQoSQTemplateId                                                                                                  FsQoSRandomDetectCfgDP

                The Object
                testValFsQoSRandomDetectCfgGain
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsQoSRandomDetectCfgGain (UINT4 *pu4ErrorCode,
                                   UINT4 u4FsQoSQTemplateId,
                                   INT4 i4FsQoSRandomDetectCfgDP,
                                   UINT4 u4TestValFsQoSRandomDetectCfgGain)
{
    INT4                i4RetStatus = QOS_FAILURE;

    *pu4ErrorCode = SNMP_ERR_NO_ERROR;

    /* Check Index */
    i4RetStatus = QoSUtlValidateRDCfgTblIdxInst (u4FsQoSQTemplateId,
                                                 i4FsQoSRandomDetectCfgDP,
                                                 pu4ErrorCode);
    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlValidateRDCfgTblIdxInst () "
                      "Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    /* Check  Object's Value */
    if (u4TestValFsQoSRandomDetectCfgGain > QOS_RD_CONFG_GAIN_MAX_VAL)
    {
        QOS_TRC_ARG3 (MGMT_TRC,
                      "%s : RD Cfg Gain value is "
                      "out of range. Range should be Min %d - Max %d. \r\n",
                      __FUNCTION__, QOS_RD_CONFG_GAIN_MIN_VAL,
                      QOS_RD_CONFG_GAIN_MAX_VAL);

        CLI_SET_ERR (QOS_CLI_ERR_RDCFG_GAIN);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }

    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhTestv2FsQoSRandomDetectCfgDropThreshType
 Input       :  The Indices
                FsQoSQTemplateId
                FsQoSRandomDetectCfgDP

                The Object
                testValFsQoSRandomDetectCfgDropThreshType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsQoSRandomDetectCfgDropThreshType (UINT4 *pu4ErrorCode,
                                             UINT4 u4FsQoSQTemplateId,
                                             INT4 i4FsQoSRandomDetectCfgDP,
                                             INT4
                                             i4TestValFsQoSRandomDetectCfgDropThreshType)
{
    INT4                i4RetStatus = QOS_FAILURE;

    *pu4ErrorCode = SNMP_ERR_NO_ERROR;

    /* Check Index */
    i4RetStatus = QoSUtlValidateRDCfgTblIdxInst (u4FsQoSQTemplateId,
                                                 i4FsQoSRandomDetectCfgDP,
                                                 pu4ErrorCode);
    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlValidateRDCfgTblIdxInst () "
                      "Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    /* Check  Object's Value */
    if ((i4TestValFsQoSRandomDetectCfgDropThreshType !=
         QOS_RD_CONFIG_DROP_THRESH_TYPE_PKTS)
        && (i4TestValFsQoSRandomDetectCfgDropThreshType !=
            QOS_RD_CONFIG_DROP_THRESH_TYPE_BYTES))
    {
        QOS_TRC_ARG3 (MGMT_TRC,
                      "%s : RD Cfg Drop Threshold Type should be %d or %d \r\n",
                      __FUNCTION__, QOS_RD_CONFIG_DROP_THRESH_TYPE_PKTS,
                      QOS_RD_CONFIG_DROP_THRESH_TYPE_BYTES);

        CLI_SET_ERR (QOS_CLI_ERR_RDCFG_DROP_THRESH_TYPE);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }

    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhTestv2FsQoSRandomDetectCfgECNThresh
 Input       :  The Indices
                FsQoSQTemplateId
                FsQoSRandomDetectCfgDP

                The Object
                testValFsQoSRandomDetectCfgECNThresh
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsQoSRandomDetectCfgECNThresh (UINT4 *pu4ErrorCode,
                                        UINT4 u4FsQoSQTemplateId,
                                        INT4 i4FsQoSRandomDetectCfgDP,
                                        UINT4
                                        u4TestValFsQoSRandomDetectCfgECNThresh)
{
    INT4                i4RetStatus = QOS_FAILURE;

    *pu4ErrorCode = SNMP_ERR_NO_ERROR;

    /* Check Index */
    i4RetStatus = QoSUtlValidateRDCfgTblIdxInst (u4FsQoSQTemplateId,
                                                 i4FsQoSRandomDetectCfgDP,
                                                 pu4ErrorCode);
    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlValidateRDCfgTblIdxInst () "
                      "Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    /* Check  Object's Value */
    if (u4TestValFsQoSRandomDetectCfgECNThresh >
        QOS_RD_CONFG_ECN_THRESH_MAX_VAL)

    {
        QOS_TRC_ARG3 (MGMT_TRC,
                      "%s : RD Cfg Max Drop Probability  value is "
                      "out of range. Range should be Min %d - Max %d. \r\n",
                      __FUNCTION__, QOS_RD_CONFG_ECN_THRESH_MIN_VAL,
                      QOS_RD_CONFG_ECN_THRESH_MAX_VAL);

        CLI_SET_ERR (QOS_CLI_ERR_RDCFG_ECN_THRESH);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }

    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhTestv2FsQoSRandomDetectCfgActionFlag
 Input       :  The Indices
                FsQoSQTemplateId
                FsQoSRandomDetectCfgDP

                The Object
                testValFsQoSRandomDetectCfgActionFlag
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsQoSRandomDetectCfgActionFlag (UINT4 *pu4ErrorCode,
                                         UINT4 u4FsQoSQTemplateId,
                                         INT4 i4FsQoSRandomDetectCfgDP,
                                         tSNMP_OCTET_STRING_TYPE
                                         *
                                         pTestValFsQoSRandomDetectCfgActionFlag)
{
    INT4                i4RetStatus = QOS_FAILURE;

    *pu4ErrorCode = SNMP_ERR_NO_ERROR;

    /* Check Index */
    i4RetStatus = QoSUtlValidateRDCfgTblIdxInst (u4FsQoSQTemplateId,
                                                 i4FsQoSRandomDetectCfgDP,
                                                 pu4ErrorCode);
    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlValidateRDCfgTblIdxInst () "
                      "Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    /* Check  Object's Value */
    i4RetStatus = QoSUtlValidateRDCfgActionFlag (u4FsQoSQTemplateId,
                                                 i4FsQoSRandomDetectCfgDP,
                                                 pTestValFsQoSRandomDetectCfgActionFlag,
                                                 pu4ErrorCode);
    if (i4RetStatus == QOS_FAILURE)
    {
        CLI_SET_ERR (QOS_CLI_ERR_RDCFG_ACT_FLG_LEN_INVALID);
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlValidateRDCfgActionFlag () "
                      "Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    return (SNMP_SUCCESS);

}

/* LOW LEVEL Routines for Table : FsQoSShapeTemplateTable. */
/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsQoSShapeTemplateTable
 Input       :  The Indices
                FsQoSShapeTemplateId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsQoSShapeTemplateTable (UINT4 u4FsQoSShapeTemplateId)
{
    INT4                i4RetStatus = 0;
    tQoSShapeTemplateNode *pShapeTempNode = NULL;

    if (gQoSGlobalInfo.eSysControl != QOS_SYS_CNTL_START)
    {
        QOS_TRC (MGMT_TRC, QOS_MODULE_SHUTDOW_ERROR_MSG);
        return (SNMP_FAILURE);
    }

    /* Check the Range of Index Values (Min-Max) */
    i4RetStatus = QOS_CHECK_SHAPE_TEMP_TBL_INDEX_RANGE (u4FsQoSShapeTemplateId);
    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG3 (MGMT_TRC, "%s : Shape Template Id %d is out of Range."
                      " The Range Should be (1-%d). \r\n", __FUNCTION__,
                      u4FsQoSShapeTemplateId,
                      QOS_SHAPE_TEMP_TBL_MAX_INDEX_RANGE);

        return (SNMP_FAILURE);
    }

    pShapeTempNode = QoSUtlGetShapeTempNode (u4FsQoSShapeTemplateId);
    if (pShapeTempNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetShapeTempNode () Returns "
                      "FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    return (SNMP_SUCCESS);

}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsQoSRandomDetectCfgTable
 Input       :  The Indices
                FsQoSQTemplateId
                FsQoSRandomDetectCfgDP
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsQoSRandomDetectCfgTable (UINT4 *pu4ErrorCode,
                                   tSnmpIndexList * pSnmpIndexList,
                                   tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsQoSShapeTemplateTable
 Input       :  The Indices
                FsQoSShapeTemplateId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsQoSShapeTemplateTable (UINT4 *pu4FsQoSShapeTemplateId)
{
    INT4                i4RetStatus = QOS_FAILURE;

    if (gQoSGlobalInfo.eSysControl != QOS_SYS_CNTL_START)
    {
        QOS_TRC (MGMT_TRC, QOS_MODULE_SHUTDOW_ERROR_MSG);
        return (SNMP_FAILURE);
    }

    i4RetStatus = QoSGetNextShapeTempTblEntryIndex (0, pu4FsQoSShapeTemplateId);

    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSGetNextShapeTempTblEntryIndex () "
                      "Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsQoSShapeTemplateTable
 Input       :  The Indices
                FsQoSShapeTemplateId
                nextFsQoSShapeTemplateId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsQoSShapeTemplateTable (UINT4 u4FsQoSShapeTemplateId,
                                        UINT4 *pu4NextFsQoSShapeTemplateId)
{
    INT4                i4RetStatus = QOS_FAILURE;

    if (gQoSGlobalInfo.eSysControl != QOS_SYS_CNTL_START)
    {
        QOS_TRC (MGMT_TRC, QOS_MODULE_SHUTDOW_ERROR_MSG);
        return (SNMP_FAILURE);
    }

    i4RetStatus = QoSGetNextShapeTempTblEntryIndex
        (u4FsQoSShapeTemplateId, pu4NextFsQoSShapeTemplateId);
    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSGetNextShapeTempTblEntryIndex () "
                      "Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    return (SNMP_SUCCESS);
}

/* Low Level GET Routine for All Objects  */
/****************************************************************************
 Function    :  nmhGetFsQoSShapeTemplateName
 Input       :  The Indices
                FsQoSShapeTemplateId

                The Object 
                retValFsQoSShapeTemplateName
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsQoSShapeTemplateName (UINT4 u4FsQoSShapeTemplateId,
                              tSNMP_OCTET_STRING_TYPE *
                              pRetValFsQoSShapeTemplateName)
{
    tQoSShapeTemplateNode *pShapeTempNode = NULL;
    UINT1              *pu1Str = NULL;

    pShapeTempNode = QoSUtlGetShapeTempNode (u4FsQoSShapeTemplateId);

    if (pShapeTempNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetShapeTempNode () "
                      "Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    pu1Str = &(pShapeTempNode->au1Name[0]);
    QOS_UTL_COPY_TABLE_NAME (pRetValFsQoSShapeTemplateName, pu1Str);

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFsQoSShapeTemplateCIR
 Input       :  The Indices
                FsQoSShapeTemplateId

                The Object 
                retValFsQoSShapeTemplateCIR
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsQoSShapeTemplateCIR (UINT4 u4FsQoSShapeTemplateId,
                             UINT4 *pu4RetValFsQoSShapeTemplateCIR)
{
    tQoSShapeTemplateNode *pShapeTempNode = NULL;

    pShapeTempNode = QoSUtlGetShapeTempNode (u4FsQoSShapeTemplateId);

    if (pShapeTempNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetShapeTempNode () "
                      "Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    *pu4RetValFsQoSShapeTemplateCIR = pShapeTempNode->u4CIR;

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFsQoSShapeTemplateCBS
 Input       :  The Indices
                FsQoSShapeTemplateId

                The Object 
                retValFsQoSShapeTemplateCBS
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsQoSShapeTemplateCBS (UINT4 u4FsQoSShapeTemplateId,
                             UINT4 *pu4RetValFsQoSShapeTemplateCBS)
{
    tQoSShapeTemplateNode *pShapeTempNode = NULL;

    pShapeTempNode = QoSUtlGetShapeTempNode (u4FsQoSShapeTemplateId);

    if (pShapeTempNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetShapeTempNode () "
                      "Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    *pu4RetValFsQoSShapeTemplateCBS = pShapeTempNode->u4CBS;

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFsQoSShapeTemplateEIR
 Input       :  The Indices
                FsQoSShapeTemplateId

                The Object 
                retValFsQoSShapeTemplateEIR
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsQoSShapeTemplateEIR (UINT4 u4FsQoSShapeTemplateId,
                             UINT4 *pu4RetValFsQoSShapeTemplateEIR)
{
    tQoSShapeTemplateNode *pShapeTempNode = NULL;

    pShapeTempNode = QoSUtlGetShapeTempNode (u4FsQoSShapeTemplateId);

    if (pShapeTempNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetShapeTempNode () "
                      "Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    *pu4RetValFsQoSShapeTemplateEIR = pShapeTempNode->u4EIR;

    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhGetFsQoSShapeTemplateEBS
 Input       :  The Indices
                FsQoSShapeTemplateId

                The Object 
                retValFsQoSShapeTemplateEBS
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsQoSShapeTemplateEBS (UINT4 u4FsQoSShapeTemplateId,
                             UINT4 *pu4RetValFsQoSShapeTemplateEBS)
{
    tQoSShapeTemplateNode *pShapeTempNode = NULL;

    pShapeTempNode = QoSUtlGetShapeTempNode (u4FsQoSShapeTemplateId);

    if (pShapeTempNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetShapeTempNode () "
                      "Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    *pu4RetValFsQoSShapeTemplateEBS = pShapeTempNode->u4EBS;

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFsQoSShapeTemplateStatus
 Input       :  The Indices
                FsQoSShapeTemplateId

                The Object 
                retValFsQoSShapeTemplateStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsQoSShapeTemplateStatus (UINT4 u4FsQoSShapeTemplateId,
                                INT4 *pi4RetValFsQoSShapeTemplateStatus)
{
    tQoSShapeTemplateNode *pShapeTempNode = NULL;

    pShapeTempNode = QoSUtlGetShapeTempNode (u4FsQoSShapeTemplateId);

    if (pShapeTempNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetShapeTempNode () "
                      "Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    *pi4RetValFsQoSShapeTemplateStatus = pShapeTempNode->u1Status;

    return (SNMP_SUCCESS);

}

/* Low Level SET Routine for All Objects  */
/****************************************************************************
 Function    :  nmhSetFsQoSShapeTemplateName
 Input       :  The Indices
                FsQoSShapeTemplateId

                The Object 
                setValFsQoSShapeTemplateName
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsQoSShapeTemplateName (UINT4 u4FsQoSShapeTemplateId,
                              tSNMP_OCTET_STRING_TYPE *
                              pSetValFsQoSShapeTemplateName)
{
    tQoSShapeTemplateNode *pShapeTempNode = NULL;
    UINT1              *pu1Str = NULL;

    pShapeTempNode = QoSUtlGetShapeTempNode (u4FsQoSShapeTemplateId);

    if (pShapeTempNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetShapeTempNode () "
                      "Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    pu1Str = &(pShapeTempNode->au1Name[0]);
    QOS_UTL_SET_TABLE_NAME (pu1Str, pSetValFsQoSShapeTemplateName);

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhSetFsQoSShapeTemplateCIR
 Input       :  The Indices
                FsQoSShapeTemplateId

                The Object 
                setValFsQoSShapeTemplateCIR
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsQoSShapeTemplateCIR (UINT4 u4FsQoSShapeTemplateId,
                             UINT4 u4SetValFsQoSShapeTemplateCIR)
{
    tQoSShapeTemplateNode *pShapeTempNode = NULL;

    pShapeTempNode = QoSUtlGetShapeTempNode (u4FsQoSShapeTemplateId);

    if (pShapeTempNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetShapeTempNode () "
                      "Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    pShapeTempNode->u4CIR = u4SetValFsQoSShapeTemplateCIR;

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhSetFsQoSShapeTemplateCBS
 Input       :  The Indices
                FsQoSShapeTemplateId

                The Object 
                setValFsQoSShapeTemplateCBS
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsQoSShapeTemplateCBS (UINT4 u4FsQoSShapeTemplateId,
                             UINT4 u4SetValFsQoSShapeTemplateCBS)
{
    tQoSShapeTemplateNode *pShapeTempNode = NULL;

    pShapeTempNode = QoSUtlGetShapeTempNode (u4FsQoSShapeTemplateId);

    if (pShapeTempNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetShapeTempNode () "
                      "Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    pShapeTempNode->u4CBS = u4SetValFsQoSShapeTemplateCBS;

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhSetFsQoSShapeTemplateEIR
 Input       :  The Indices
                FsQoSShapeTemplateId

                The Object 
                setValFsQoSShapeTemplateEIR
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsQoSShapeTemplateEIR (UINT4 u4FsQoSShapeTemplateId,
                             UINT4 u4SetValFsQoSShapeTemplateEIR)
{
    tQoSShapeTemplateNode *pShapeTempNode = NULL;

    pShapeTempNode = QoSUtlGetShapeTempNode (u4FsQoSShapeTemplateId);

    if (pShapeTempNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetShapeTempNode () "
                      "Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    pShapeTempNode->u4EIR = u4SetValFsQoSShapeTemplateEIR;

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhSetFsQoSShapeTemplateEBS
 Input       :  The Indices
                FsQoSShapeTemplateId

                The Object 
                setValFsQoSShapeTemplateEBS
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsQoSShapeTemplateEBS (UINT4 u4FsQoSShapeTemplateId,
                             UINT4 u4SetValFsQoSShapeTemplateEBS)
{
    tQoSShapeTemplateNode *pShapeTempNode = NULL;

    pShapeTempNode = QoSUtlGetShapeTempNode (u4FsQoSShapeTemplateId);

    if (pShapeTempNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetShapeTempNode () "
                      "Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    pShapeTempNode->u4EBS = u4SetValFsQoSShapeTemplateEBS;

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhSetFsQoSShapeTemplateStatus
 Input       :  The Indices
                FsQoSShapeTemplateId

                The Object 
                setValFsQoSShapeTemplateStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsQoSShapeTemplateStatus (UINT4 u4FsQoSShapeTemplateId,
                                INT4 i4SetValFsQoSShapeTemplateStatus)
{
    tQoSShapeTemplateNode *pShapeTempNode = NULL;
    INT4                i4RetStatus = QOS_FAILURE;
    INT4                i4ReturnValue = SNMP_SUCCESS;

    pShapeTempNode = QoSUtlGetShapeTempNode (u4FsQoSShapeTemplateId);
    if (pShapeTempNode != NULL)
    {
        /* Entry found in the Table */
        if (pShapeTempNode->u1Status == i4SetValFsQoSShapeTemplateStatus)
        {
            /* same value */
            return (SNMP_SUCCESS);
        }
    }
    else
    {
        /* New Entry need to be created in the Table, only if 
         * i4SetValFsQoSShapeTemplateStatus = CREATE_AND_WAIT */
        /* Optional */
        if (i4SetValFsQoSShapeTemplateStatus == DESTROY)
        {
            return (SNMP_SUCCESS);
        }
    }

    switch (i4SetValFsQoSShapeTemplateStatus)
    {
            /* For a New Entry */
        case CREATE_AND_GO:

            pShapeTempNode =
                QoSCreateShapeTempTblEntry (u4FsQoSShapeTemplateId);
            if (pShapeTempNode == NULL)
            {
                QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSCreateShapeTempTblEntry () "
                              "Returns FAILURE. \r\n", __FUNCTION__);

                return (SNMP_FAILURE);
            }
            if (pShapeTempNode != NULL)
            {
                pShapeTempNode->u1Status = ACTIVE;
            }

            i4ReturnValue = SNMP_SUCCESS;
            break;

        case CREATE_AND_WAIT:

            pShapeTempNode =
                QoSCreateShapeTempTblEntry (u4FsQoSShapeTemplateId);

            if (pShapeTempNode == NULL)
            {
                QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSCreateShapeTempTblEntry ()"
                              " Returns FAILURE. \r\n", __FUNCTION__);

                return (SNMP_FAILURE);
            }
            if (pShapeTempNode != NULL)
            {
                pShapeTempNode->u1Status = NOT_IN_SERVICE;
            }

            i4ReturnValue = SNMP_SUCCESS;

            break;

            /* Modify an Entry */
        case ACTIVE:

            if (pShapeTempNode != NULL)
            {
                pShapeTempNode->u1Status = ACTIVE;
            }

            break;

        case NOT_IN_SERVICE:
            if (pShapeTempNode != NULL)
            {
                pShapeTempNode->u1Status = NOT_IN_SERVICE;
            }

            break;

        case DESTROY:

            /* 1. Remove the entry from the SOFTWARE. */
            i4RetStatus = QoSDeleteShapeTempTblEntry (pShapeTempNode);
            if (i4RetStatus == QOS_FAILURE)
            {
                QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSDeleteShapeTempTblEntry ()"
                              " Returns FAILURE. \r\n", __FUNCTION__);

                return (SNMP_FAILURE);
            }

            break;

        default:
            i4ReturnValue = SNMP_FAILURE;
            break;
    }
    if (i4ReturnValue == SNMP_SUCCESS)
    {
        QosShapeTemplateUpdateNextFree (u4FsQoSShapeTemplateId,
                                        i4SetValFsQoSShapeTemplateStatus);
    }

    return ((INT1) i4ReturnValue);
}

/* Low Level TEST Routines for All Objects  */
/****************************************************************************
 Function    :  nmhTestv2FsQoSShapeTemplateName
 Input       :  The Indices
                FsQoSShapeTemplateId

                The Object 
                testValFsQoSShapeTemplateName
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsQoSShapeTemplateName (UINT4 *pu4ErrorCode,
                                 UINT4 u4FsQoSShapeTemplateId,
                                 tSNMP_OCTET_STRING_TYPE *
                                 pTestValFsQoSShapeTemplateName)
{
    tQoSShapeTemplateNode *pShapeTempNode = NULL;
    INT4                i4RetStatus = QOS_FAILURE;

    *pu4ErrorCode = SNMP_ERR_NO_ERROR;

    /* Check Index */
    i4RetStatus = QoSUtlValidateShapeTempTblIdxInst (u4FsQoSShapeTemplateId,
                                                     pu4ErrorCode);
    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlValidateShapeTempTblIdxInst ()"
                      " Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    /* Check  Object's Value */
    i4RetStatus = QoSUtlValidateTableName (pTestValFsQoSShapeTemplateName,
                                           pu4ErrorCode);
    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlValidateTableName () "
                      "Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    pShapeTempNode = QoSUtlGetShapeTempNode (u4FsQoSShapeTemplateId);
    if (pShapeTempNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetShapeTempNode () Returns "
                      "FAILURE. \r\n", __FUNCTION__);

        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (QOS_CLI_ERR_SHAPE_NOT_CREATED);
        return (SNMP_FAILURE);
    }

    /* Check the Name is same as Current Name */
    if ((STRLEN (pShapeTempNode->au1Name)) == (UINT4)
        pTestValFsQoSShapeTemplateName->i4_Length)
    {
        if ((STRNCMP (pShapeTempNode->au1Name,
                      pTestValFsQoSShapeTemplateName->pu1_OctetList,
                      pTestValFsQoSShapeTemplateName->i4_Length)) == 0)
        {
            return (SNMP_SUCCESS);
        }
    }

    /* Check the name is Unique or Not */
    i4RetStatus = QoSUtlShapeIsUniqueName (pTestValFsQoSShapeTemplateName);
    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlShapeIsUniqueName () Returns "
                      "FAILURE. \r\n", __FUNCTION__);

        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return (SNMP_FAILURE);
    }

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhTestv2FsQoSShapeTemplateCIR
 Input       :  The Indices
                FsQoSShapeTemplateId

                The Object 
                testValFsQoSShapeTemplateCIR
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsQoSShapeTemplateCIR (UINT4 *pu4ErrorCode,
                                UINT4 u4FsQoSShapeTemplateId,
                                UINT4 u4TestValFsQoSShapeTemplateCIR)
{
    INT4                i4RetStatus = QOS_FAILURE;
    tQoSShapeTemplateNode *pShapeTempNode = NULL;

    *pu4ErrorCode = SNMP_ERR_NO_ERROR;

    /* Check Index */
    i4RetStatus = QoSUtlValidateShapeTempTblIdxInst (u4FsQoSShapeTemplateId,
                                                     pu4ErrorCode);
    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlValidateShapeTempTblIdxInst ()"
                      " Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    /* Check  Object's Value */
    if ((u4TestValFsQoSShapeTemplateCIR < QOS_SHAPE_CIR_MIN_VAL) ||
        (u4TestValFsQoSShapeTemplateCIR > QOS_SHAPE_CIR_MAX_VAL))
    {
        QOS_TRC_ARG3 (MGMT_TRC, "%s : Shape CIR value is out of range. The"
                      " range should be Min %d - Max %d. \r\n", __FUNCTION__,
                      QOS_SHAPE_CIR_MIN_VAL, QOS_SHAPE_CIR_MAX_VAL);
        CLI_SET_ERR (QOS_CLI_ERR_INVALID_CIR);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;

        return (SNMP_FAILURE);
    }

    pShapeTempNode = QoSUtlGetShapeTempNode (u4FsQoSShapeTemplateId);
    if (pShapeTempNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetShapeTempNode () "
                      "Returns FAILURE. \r\n", __FUNCTION__);

        *pu4ErrorCode = SNMP_ERR_NO_CREATION;

        return (QOS_FAILURE);
    }
#ifdef NPAPI_WANTED
    /*check hardware capability */
    if ((ISS_HW_SUPPORTED ==
         IssGetHwCapabilities (ISS_HW_SHAPE_PARAM_EIR_SUPPORT)))
    {
#endif

        if (pShapeTempNode->u4EIR > 0)
        {
            if (u4TestValFsQoSShapeTemplateCIR > pShapeTempNode->u4EIR)
            {
                CLI_SET_ERR (QOS_CLI_ERR_METER_CIR_LESS_THAN_EIR);

                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                /*CIR should be less than or equal to EIR */
                return (SNMP_FAILURE);
            }
        }
#ifdef NPAPI_WANTED
    }
#endif

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhTestv2FsQoSShapeTemplateCBS
 Input       :  The Indices
                FsQoSShapeTemplateId

                The Object 
                testValFsQoSShapeTemplateCBS
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsQoSShapeTemplateCBS (UINT4 *pu4ErrorCode,
                                UINT4 u4FsQoSShapeTemplateId,
                                UINT4 u4TestValFsQoSShapeTemplateCBS)
{
    INT4                i4RetStatus = QOS_FAILURE;

    *pu4ErrorCode = SNMP_ERR_NO_ERROR;

    /* Check Index */
    i4RetStatus = QoSUtlValidateShapeTempTblIdxInst (u4FsQoSShapeTemplateId,
                                                     pu4ErrorCode);
    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlValidateShapeTempTblIdxInst ()"
                      " Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    /* Check  Object's Value */
    if (u4TestValFsQoSShapeTemplateCBS > QOS_SHAPE_CBS_MAX_VAL)
    {
        QOS_TRC_ARG3 (MGMT_TRC, "%s : Shape CBS value is out of range. The"
                      " range should be Min %d - Max %d. \r\n", __FUNCTION__,
                      QOS_SHAPE_CBS_MIN_VAL, QOS_SHAPE_CBS_MAX_VAL);

        CLI_SET_ERR (QOS_CLI_ERR_INVALID_CBS);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhTestv2FsQoSShapeTemplateEIR
 Input       :  The Indices
                FsQoSShapeTemplateId

                The Object 
                testValFsQoSShapeTemplateEIR
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsQoSShapeTemplateEIR (UINT4 *pu4ErrorCode,
                                UINT4 u4FsQoSShapeTemplateId,
                                UINT4 u4TestValFsQoSShapeTemplateEIR)
{
    INT4                i4RetStatus = QOS_FAILURE;
    tQoSShapeTemplateNode *pShapeTempNode = NULL;

    *pu4ErrorCode = SNMP_ERR_NO_ERROR;

#ifdef NPAPI_WANTED
    /*check hardware capability */
    if (ISS_HW_SUPPORTED !=
        IssGetHwCapabilities (ISS_HW_SHAPE_PARAM_EIR_SUPPORT))
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : IssGetHwCapabilities ()"
                      "Returns SUCCESS. EIR is not supported in platform. \r\n",
                      __FUNCTION__);
        CLI_SET_ERR (QOS_CLI_ERR_SHAPE_TEMP_EIR_SUPPORT);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
#endif

    /* Check Index */
    i4RetStatus = QoSUtlValidateShapeTempTblIdxInst (u4FsQoSShapeTemplateId,
                                                     pu4ErrorCode);
    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlValidateShapeTempTblIdxInst ()"
                      " Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    /* Check  Object's Value */
    if (u4TestValFsQoSShapeTemplateEIR > QOS_SHAPE_EIR_MAX_VAL)
    {
        QOS_TRC_ARG3 (MGMT_TRC, "%s : Shape EIR value is out of range. The"
                      " range should be Min %d - Max %d. \r\n", __FUNCTION__,
                      QOS_SHAPE_EIR_MIN_VAL, QOS_SHAPE_EIR_MAX_VAL);

        CLI_SET_ERR (QOS_CLI_ERR_INVALID_EIR);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }
    pShapeTempNode = QoSUtlGetShapeTempNode (u4FsQoSShapeTemplateId);
    if (pShapeTempNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetShapeTempNode () "
                      "Returns FAILURE. \r\n", __FUNCTION__);

        *pu4ErrorCode = SNMP_ERR_NO_CREATION;

        return (QOS_FAILURE);
    }
    if (u4TestValFsQoSShapeTemplateEIR > 0)
    {
        if (pShapeTempNode->u4CIR > u4TestValFsQoSShapeTemplateEIR)
        {
            CLI_SET_ERR (QOS_CLI_ERR_METER_CIR_LESS_THAN_EIR);

            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            /*CIR should be less than or equal to EIR */
            return (SNMP_FAILURE);
        }
    }

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhTestv2FsQoSShapeTemplateEBS
 Input       :  The Indices
                FsQoSShapeTemplateId

                The Object 
                testValFsQoSShapeTemplateEBS
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsQoSShapeTemplateEBS (UINT4 *pu4ErrorCode,
                                UINT4 u4FsQoSShapeTemplateId,
                                UINT4 u4TestValFsQoSShapeTemplateEBS)
{
    INT4                i4RetStatus = QOS_FAILURE;

    *pu4ErrorCode = SNMP_ERR_NO_ERROR;

#ifdef NPAPI_WANTED
    /*check hardware capability */
    if (ISS_HW_SUPPORTED !=
        IssGetHwCapabilities (ISS_HW_SHAPE_PARAM_EBS_SUPPORT))
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : IssGetHwCapabilities ()"
                      "Returns SUCCESS. EBS is not supported in platform. \r\n",
                      __FUNCTION__);
        CLI_SET_ERR (QOS_CLI_ERR_SHAPE_TEMP_EBS_SUPPORT);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
#endif

    /* Check Index */
    i4RetStatus = QoSUtlValidateShapeTempTblIdxInst (u4FsQoSShapeTemplateId,
                                                     pu4ErrorCode);
    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlValidateShapeTempTblIdxInst ()"
                      " Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    /* Check  Object's Value */
    if (u4TestValFsQoSShapeTemplateEBS > QOS_SHAPE_EBS_MAX_VAL)
    {
        QOS_TRC_ARG3 (MGMT_TRC, "%s : Shape EBS value is out of range. The"
                      " range should be Min %d - Max %d. \r\n", __FUNCTION__,
                      QOS_SHAPE_EBS_MIN_VAL, QOS_SHAPE_EBS_MAX_VAL);

        CLI_SET_ERR (QOS_CLI_ERR_INVALID_EBS);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;

        return (SNMP_FAILURE);
    }

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhTestv2FsQoSShapeTemplateStatus
 Input       :  The Indices
                FsQoSShapeTemplateId

                The Object 
                testValFsQoSShapeTemplateStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsQoSShapeTemplateStatus (UINT4 *pu4ErrorCode,
                                   UINT4 u4FsQoSShapeTemplateId,
                                   INT4 i4TestValFsQoSShapeTemplateStatus)
{
    tQoSShapeTemplateNode *pShapeTempNode = NULL;
    INT4                i4RetStatus = QOS_FAILURE;
    UINT4               u4Count = 0;
    INT4                i4ReturnValue = SNMP_SUCCESS;

    if (gQoSGlobalInfo.eSysControl != QOS_SYS_CNTL_START)
    {
        QOS_TRC (MGMT_TRC, QOS_MODULE_SHUTDOW_ERROR_MSG);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return (SNMP_FAILURE);
    }

    *pu4ErrorCode = SNMP_ERR_NO_ERROR;

    /* Check the Range of Index Values (Min-Max) */
    i4RetStatus = QOS_CHECK_SHAPE_TEMP_TBL_INDEX_RANGE (u4FsQoSShapeTemplateId);
    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG3 (MGMT_TRC, "%s : Shape Template Id %d is out of Range."
                      " The Range Should be (1-%d). \r\n", __FUNCTION__,
                      u4FsQoSShapeTemplateId,
                      QOS_SHAPE_TEMP_TBL_MAX_INDEX_RANGE);

        CLI_SET_ERR (QOS_CLI_ERR_SHAPE_TEMP_RANGE);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;

        return (SNMP_FAILURE);
    }

    pShapeTempNode = QoSUtlGetShapeTempNode (u4FsQoSShapeTemplateId);

    if (pShapeTempNode == NULL)
    {
        /* Entry Not Found In The Table */
        switch (i4TestValFsQoSShapeTemplateStatus)
        {
            case ACTIVE:
            case NOT_IN_SERVICE:
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                i4ReturnValue = SNMP_FAILURE;
                break;

            case CREATE_AND_GO:
            case CREATE_AND_WAIT:
                /* Check the No of Entries in the Table */
                u4Count = TMO_SLL_Count (&(gQoSGlobalInfo.SllShapeTempTbl));

                if (u4Count > QOS_SHAPE_TEMP_TBL_MAX_ENTRIES)
                {
                    QOS_TRC_ARG2 (MGMT_TRC, "%s : Max No of Shape Template "
                                  "Table Entries are Configured. No of Max "
                                  "Entries %d. \r\n", __FUNCTION__,
                                  QOS_SHAPE_TEMP_TBL_MAX_ENTRIES);

                    CLI_SET_ERR (QOS_CLI_ERR_SHAPE_MAX_ENTRY);
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;

                    return (SNMP_FAILURE);
                }

                break;

            case DESTROY:
                break;

            default:
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                i4ReturnValue = SNMP_FAILURE;
                break;
        }
    }
    else
    {
        /* Entry Found In The Table */
        switch (i4TestValFsQoSShapeTemplateStatus)
        {
            case ACTIVE:

                if ((pShapeTempNode->u1Status == NOT_READY))
                {
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                    return (SNMP_FAILURE);
                }

                break;

            case NOT_IN_SERVICE:

                if ((pShapeTempNode->u1Status == NOT_READY))
                {
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                    return (SNMP_FAILURE);
                }

                i4RetStatus = QOS_CHECK_TABLE_REF_COUNT (pShapeTempNode);

                if (i4RetStatus == QOS_FAILURE)
                {
                    QOS_TRC_ARG1 (MGMT_TRC, "In %s : ERROR - This Entry is "
                                  "referenced by Q Table Entry or Scheduler "
                                  "Table Entry.\r\n", __FUNCTION__);
                    CLI_SET_ERR (QOS_CLI_ERR_SHAPE_TEMP_RFE);
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;

                    return (SNMP_FAILURE);
                }
                break;

            case DESTROY:

                i4RetStatus = QOS_CHECK_TABLE_REF_COUNT (pShapeTempNode);

                if (i4RetStatus == QOS_FAILURE)
                {
                    QOS_TRC_ARG1 (MGMT_TRC, "In %s : ERROR - This Entry is "
                                  "referenced by Q Table Entry or Scheduler "
                                  "Table Entry.\r\n", __FUNCTION__);

                    CLI_SET_ERR (QOS_CLI_ERR_SHAPE_TEMP_RFE);
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                    return (SNMP_FAILURE);
                }
                break;

            case CREATE_AND_WAIT:
            case CREATE_AND_GO:
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                i4ReturnValue = SNMP_FAILURE;
                break;

            default:
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                i4ReturnValue = SNMP_FAILURE;
                break;

        }                        /* End of switch */

    }                            /* End of if */

    return ((INT1) i4ReturnValue);
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsQoSShapeTemplateTable
 Input       :  The Indices
                FsQoSShapeTemplateId
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsQoSShapeTemplateTable (UINT4 *pu4ErrorCode,
                                 tSnmpIndexList * pSnmpIndexList,
                                 tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsQoSQMapTable. */
/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsQoSQMapTable
 Input       :  The Indices
                IfIndex
                FsQoSQMapCLASS
                FsQoSQMapRegenPriType
                FsQoSQMapRegenPriority
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsQoSQMapTable (INT4 i4IfIndex, UINT4 u4FsQoSQMapCLASS,
                                        INT4 i4FsQoSQMapRegenPriType,
                                        UINT4 u4FsQoSQMapRegenPriority)
{
    tQoSQMapNode       *pQMapNode = NULL;

    if (gQoSGlobalInfo.eSysControl != QOS_SYS_CNTL_START)
    {
        QOS_TRC (MGMT_TRC, QOS_MODULE_SHUTDOW_ERROR_MSG);
        return (SNMP_FAILURE);
    }

    pQMapNode = QoSUtlGetQMapNode (i4IfIndex, u4FsQoSQMapCLASS,
                                   i4FsQoSQMapRegenPriType,
                                   u4FsQoSQMapRegenPriority);
    if (pQMapNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetQMapNode () Returns "
                      "FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsQoSQMapTable
 Input       :  The Indices
                IfIndex
                FsQoSQMapCLASS
                FsQoSQMapRegenPriType
                FsQoSQMapRegenPriority
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsQoSQMapTable (INT4 *pi4IfIndex, UINT4 *pu4FsQoSQMapCLASS,
                                INT4 *pi4FsQoSQMapRegenPriType,
                                UINT4 *pu4FsQoSQMapRegenPriority)
{
    INT4                i4RetStatus = QOS_FAILURE;

    if (gQoSGlobalInfo.eSysControl != QOS_SYS_CNTL_START)
    {
        QOS_TRC (MGMT_TRC, QOS_MODULE_SHUTDOW_ERROR_MSG);
        return (SNMP_FAILURE);
    }

    i4RetStatus = QoSGetNextQMapTblEntryIndex (0, pi4IfIndex,
                                               0, pu4FsQoSQMapCLASS,
                                               0, pi4FsQoSQMapRegenPriType,
                                               0, pu4FsQoSQMapRegenPriority);
    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSGetNextQMapTblEntryIndex () "
                      "Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsQoSQMapTable
 Input       :  The Indices
                IfIndex
                nextIfIndex
                FsQoSQMapCLASS
                nextFsQoSQMapCLASS
                FsQoSQMapRegenPriType
                nextFsQoSQMapRegenPriType
                FsQoSQMapRegenPriority
                nextFsQoSQMapRegenPriority
                
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsQoSQMapTable (INT4 i4IfIndex, INT4 *pi4NextIfIndex,
                               UINT4 u4FsQoSQMapCLASS,
                               UINT4 *pu4NextFsQoSQMapCLASS,
                               INT4 i4FsQoSQMapRegenPriType,
                               INT4 *pi4NextFsQoSQMapRegenPriType,
                               UINT4 u4FsQoSQMapRegenPriority,
                               UINT4 *pu4NextFsQoSQMapRegenPriority)
{
    INT4                i4RetStatus = QOS_FAILURE;

    if (gQoSGlobalInfo.eSysControl != QOS_SYS_CNTL_START)
    {
        QOS_TRC (MGMT_TRC, QOS_MODULE_SHUTDOW_ERROR_MSG);
        return (SNMP_FAILURE);
    }

    i4RetStatus = QoSGetNextQMapTblEntryIndex (i4IfIndex, pi4NextIfIndex,
                                               u4FsQoSQMapCLASS,
                                               pu4NextFsQoSQMapCLASS,
                                               i4FsQoSQMapRegenPriType,
                                               pi4NextFsQoSQMapRegenPriType,
                                               u4FsQoSQMapRegenPriority,
                                               pu4NextFsQoSQMapRegenPriority);
    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSGetNextQMapTblEntryIndex () "
                      "Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    return (SNMP_SUCCESS);

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsQoSQMapQId
 Input       :  The Indices
                IfIndex
                FsQoSQMapCLASS
                FsQoSQMapRegenPriType
                FsQoSQMapRegenPriority

                The Object 
                retValFsQoSQMapQId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsQoSQMapQId (INT4 i4IfIndex, UINT4 u4FsQoSQMapCLASS,
                    INT4 i4FsQoSQMapRegenPriType,
                    UINT4 u4FsQoSQMapRegenPriority,
                    UINT4 *pu4RetValFsQoSQMapQId)
{
    tQoSQMapNode       *pQMapNode = NULL;

    pQMapNode = QoSUtlGetQMapNode (i4IfIndex, u4FsQoSQMapCLASS,
                                   i4FsQoSQMapRegenPriType,
                                   u4FsQoSQMapRegenPriority);
    if (pQMapNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetQMapNode () "
                      "Returns FAILURE. \r\n", __FUNCTION__);
        return (SNMP_FAILURE);
    }

    *pu4RetValFsQoSQMapQId = pQMapNode->u4QId;

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFsQoSQMapStatus
 Input       :  The Indices
                IfIndex
                FsQoSQMapCLASS
                FsQoSQMapRegenPriType
                FsQoSQMapRegenPriority

                The Object 
                retValFsQoSQMapStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsQoSQMapStatus (INT4 i4IfIndex, UINT4 u4FsQoSQMapCLASS,
                       INT4 i4FsQoSQMapRegenPriType,
                       UINT4 u4FsQoSQMapRegenPriority,
                       INT4 *pi4RetValFsQoSQMapStatus)
{
    tQoSQMapNode       *pQMapNode = NULL;

    pQMapNode = QoSUtlGetQMapNode (i4IfIndex, u4FsQoSQMapCLASS,
                                   i4FsQoSQMapRegenPriType,
                                   u4FsQoSQMapRegenPriority);
    if (pQMapNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetQMapNode () "
                      "Returns FAILURE. \r\n", __FUNCTION__);
        return (SNMP_FAILURE);
    }

    *pi4RetValFsQoSQMapStatus = (INT4) pQMapNode->u1Status;

    return (SNMP_SUCCESS);
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsQoSQMapQId
 Input       :  The Indices
                IfIndex
                FsQoSQMapCLASS
                FsQoSQMapRegenPriType
                FsQoSQMapRegenPriority

                The Object 
                setValFsQoSQMapQId
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsQoSQMapQId (INT4 i4IfIndex, UINT4 u4FsQoSQMapCLASS,
                    INT4 i4FsQoSQMapRegenPriType,
                    UINT4 u4FsQoSQMapRegenPriority, UINT4 u4SetValFsQoSQMapQId)
{
    tQoSQMapNode       *pQMapNode = NULL;

    pQMapNode = QoSUtlGetQMapNode (i4IfIndex, u4FsQoSQMapCLASS,
                                   i4FsQoSQMapRegenPriType,
                                   u4FsQoSQMapRegenPriority);
    if (pQMapNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetQMapNode () "
                      "Returns FAILURE. \r\n", __FUNCTION__);
        return (SNMP_FAILURE);
    }

    pQMapNode->u4QId = u4SetValFsQoSQMapQId;

    QoSValidateQMapTblEntry (pQMapNode);

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhSetFsQoSQMapStatus
 Input       :  The Indices
                IfIndex
                FsQoSQMapCLASS
                FsQoSQMapRegenPriType
                FsQoSQMapRegenPriority

                The Object 
                setValFsQoSQMapStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsQoSQMapStatus (INT4 i4IfIndex, UINT4 u4FsQoSQMapCLASS,
                       INT4 i4FsQoSQMapRegenPriType,
                       UINT4 u4FsQoSQMapRegenPriority,
                       INT4 i4SetValFsQoSQMapStatus)
{
    tQoSQMapNode       *pQMapNode = NULL;
    INT4                i4RetStatus = QOS_FAILURE;

    pQMapNode = QoSUtlGetQMapNode (i4IfIndex, u4FsQoSQMapCLASS,
                                   i4FsQoSQMapRegenPriType,
                                   u4FsQoSQMapRegenPriority);
    if (pQMapNode != NULL)
    {
        /* Entry found in the Table */
        if (pQMapNode->u1Status == i4SetValFsQoSQMapStatus)
        {
            /* same value */
            return (SNMP_SUCCESS);
        }
    }
    else
    {
        /* New Entry need to be created in the Table, only if 
         * i4SetValFsQoSQMapStatus = CREATE_AND_WAIT */
        /* Optional */
        if (i4SetValFsQoSQMapStatus == DESTROY)
        {
            return (SNMP_SUCCESS);
        }
    }

    switch (i4SetValFsQoSQMapStatus)
    {
            /* For a New Entry */
        case CREATE_AND_WAIT:

            pQMapNode = QoSCreateQMapTblEntry (i4IfIndex, u4FsQoSQMapCLASS,
                                               i4FsQoSQMapRegenPriType,
                                               u4FsQoSQMapRegenPriority);
            if (pQMapNode == NULL)
            {
                QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSCreateQMapTblEntry ()"
                              " Returns FAILURE. \r\n", __FUNCTION__);
                return (SNMP_FAILURE);
            }
            if (pQMapNode != NULL)
            {
                pQMapNode->u1Status = NOT_READY;
            }

            break;

            /* Modify an Entry */
        case ACTIVE:

            /* 1. Add the entry into the HARDWARE. */
            if (gQoSGlobalInfo.eSysStatus == QOS_SYS_STATUS_ENABLE)
            {
                if (pQMapNode != NULL)
                {
                    i4RetStatus = QoSHwWrMapClassToQueue (pQMapNode->i4IfIndex,
                                                          pQMapNode->u4CLASS,
                                                          pQMapNode->
                                                          u1RegenPriType,
                                                          pQMapNode->u1RegenPri,
                                                          pQMapNode->u4QId,
                                                          QOS_QMAP_ADD);
                    if (i4RetStatus == QOS_FAILURE)
                    {
                        QOS_TRC_ARG1 (MGMT_TRC, "In %s : "
                                      "QoSHwWrMapClassToQueue() : Add "
                                      "Returns FAILURE. \r\n", __FUNCTION__);
                        return (SNMP_FAILURE);

                    }
                    pQMapNode->u1Status = ACTIVE;
                }
            }

            break;

        case NOT_IN_SERVICE:

            /* 1. Remove the entry from the HARDWARE. */
            if (pQMapNode != NULL)
            {
                if ((pQMapNode->u1Status == ACTIVE) &&
                    (gQoSGlobalInfo.eSysStatus == QOS_SYS_STATUS_ENABLE))
                {
                    i4RetStatus = QoSHwWrMapClassToQueue (pQMapNode->i4IfIndex,
                                                          pQMapNode->u4CLASS,
                                                          pQMapNode->
                                                          u1RegenPriType,
                                                          pQMapNode->u1RegenPri,
                                                          pQMapNode->u4QId,
                                                          QOS_QMAP_DEL);
                    if (i4RetStatus == QOS_FAILURE)
                    {
                        QOS_TRC_ARG1 (MGMT_TRC, "In %s : "
                                      "QoSHwWrMapClassToQueue() : Del "
                                      "Returns FAILURE. \r\n", __FUNCTION__);
                        return (SNMP_FAILURE);

                    }
                }
                pQMapNode->u1Status = NOT_IN_SERVICE;
            }

            break;

        case DESTROY:

            /* 1. Remove the entry from the HARDWARE. */
            if ((pQMapNode->u1Status == ACTIVE) &&
                (gQoSGlobalInfo.eSysStatus == QOS_SYS_STATUS_ENABLE))
            {
                /* To map the default Priority to Queue */
                if (pQMapNode->u4CLASS == 0)
                {
                    pQMapNode->u4QId = pQMapNode->u1RegenPri + 1;
                }

                i4RetStatus = QoSHwWrMapClassToQueue (pQMapNode->i4IfIndex,
                                                      pQMapNode->u4CLASS,
                                                      pQMapNode->u1RegenPriType,
                                                      pQMapNode->u1RegenPri,
                                                      pQMapNode->u4QId,
                                                      QOS_QMAP_DEL);
                if (i4RetStatus == QOS_FAILURE)
                {
                    QOS_TRC_ARG1 (MGMT_TRC, "In %s : "
                                  "QoSHwWrMapClassToQueue() : Del "
                                  "Returns FAILURE. \r\n", __FUNCTION__);
                    return (SNMP_FAILURE);

                }
            }

            /* 2. Remove the entry from the SOFTWARE. */
            i4RetStatus = QoSDeleteQMapTblEntry (pQMapNode);
            if (i4RetStatus == QOS_FAILURE)
            {
                QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSDeleteQMapTblEntry ()"
                              " Returns FAILURE. \r\n", __FUNCTION__);
                return (SNMP_FAILURE);
            }

            break;

        case CREATE_AND_GO:
            break;

        default:
            break;
    }

    return (SNMP_SUCCESS);
}

/* Low Level TEST Routines for All Objects  */
/****************************************************************************
 Function    :  nmhTestv2FsQoSQMapQId
 Input       :  The Indices
                IfIndex
                FsQoSQMapCLASS
                FsQoSQMapRegenPriType
                FsQoSQMapRegenPriority

                The Object 
                testValFsQoSQMapQId
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsQoSQMapQId (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                       UINT4 u4FsQoSQMapCLASS, INT4 i4FsQoSQMapRegenPriType,
                       UINT4 u4FsQoSQMapRegenPriority,
                       UINT4 u4TestValFsQoSQMapQId)
{
    INT4                i4RetStatus = QOS_FAILURE;
    UINT2               u2VlanId = 0;
    tQoSQNode          *pQNode = NULL;

    pQNode = QoSUtlGetQNode (i4IfIndex, u4TestValFsQoSQMapQId);

    QoSCheckAndUpdateVlanMapIfIndex (u4FsQoSQMapCLASS, &u2VlanId);
    *pu4ErrorCode = SNMP_ERR_NO_ERROR;

    /* Check Index */
    i4RetStatus = QoSUtlValidateQMapTblIdxInst (i4IfIndex, u4FsQoSQMapCLASS,
                                                i4FsQoSQMapRegenPriType,
                                                u4FsQoSQMapRegenPriority,
                                                pu4ErrorCode);
    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlValidateQMapTblIdxInst ()"
                      " Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    /* Independent Table no need to check the Q Id */
    if ((u4TestValFsQoSQMapQId == 0) ||
        (u4TestValFsQoSQMapQId >
         (QOS_QUEUE_ENTRY_MAX + QOS_QUEUE_MAX_NUM_SUBQ)))
    {
        CLI_SET_ERR (QOS_CLI_ERR_Q_RANGE);
        *pu4ErrorCode = SNMP_ERR_WRONG_TYPE;
        return (SNMP_FAILURE);
    }

    if (pQNode != NULL)
    {
        if ((pQNode->u4QueueType == QOS_Q_SUBSCRIBER_TYPE) && (0 == u2VlanId))
        {
            CLI_SET_ERR (QOS_CLI_ERR_Q_RANGE);
            return (SNMP_FAILURE);
        }
    }

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhTestv2FsQoSQMapStatus
 Input       :  The Indices
                IfIndex
                FsQoSQMapCLASS
                FsQoSQMapRegenPriType
                FsQoSQMapRegenPriority

                The Object 
                testValFsQoSQMapStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsQoSQMapStatus (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                          UINT4 u4FsQoSQMapCLASS,
                          INT4 i4FsQoSQMapRegenPriType,
                          UINT4 u4FsQoSQMapRegenPriority,
                          INT4 i4TestValFsQoSQMapStatus)
{
    tQoSQMapNode       *pQMapNode = NULL;
    tQoSQMapNode        QMapTmpNode;
    INT4                i4RetStatus = QOS_FAILURE;
    UINT4               u4Count = 0;
    INT4                i4ReturnValue = SNMP_SUCCESS;

    MEMSET (&QMapTmpNode, 0, (sizeof (tQoSQMapNode)));

    if (gQoSGlobalInfo.eSysControl != QOS_SYS_CNTL_START)
    {
        CLI_SET_ERR (QOS_CLI_ERR_MODULE_SHUTDOW);
        QOS_TRC (MGMT_TRC, QOS_MODULE_SHUTDOW_ERROR_MSG);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return (SNMP_FAILURE);
    }

    *pu4ErrorCode = SNMP_ERR_NO_ERROR;

    if (i4IfIndex != 0)
    {
        i4RetStatus = QoSUtlValidateIfIndex (i4IfIndex, pu4ErrorCode);
        if (i4RetStatus == QOS_FAILURE)
        {
            CLI_SET_ERR (QOS_CLI_ERR_INVALID_IF_INDEX);
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return (SNMP_FAILURE);
        }
    }

    /* Validate QMap parameters against CLASS type */
    i4RetStatus = QoSUtlValidateClassTypeParams (u4FsQoSQMapCLASS, i4IfIndex);
    if (i4RetStatus == QOS_FAILURE)
    {
        return (SNMP_FAILURE);
    }

    /* Check the Range of Index Values (Min-Max) */
    if (u4FsQoSQMapCLASS > QOS_CLS2PRI_MAP_TBL_MAX_INDEX_RANGE)
    {
        QOS_TRC_ARG3 (MGMT_TRC, "%s : CLASS %d is out of Range."
                      " The Range Should be (0-%d). \r\n", __FUNCTION__,
                      u4FsQoSQMapCLASS, QOS_CLS2PRI_MAP_TBL_MAX_INDEX_RANGE);

        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (QOS_CLI_ERR_CLS_MAP_RANGE);
        return (QOS_FAILURE);
    }

    if (i4FsQoSQMapRegenPriType != QOS_QMAP_PRI_TYPE_NONE)
    {
        if (u4FsQoSQMapCLASS != 0)
        {
            CLI_SET_ERR (QOS_CLI_ERR_INVAL_QUEUE_PARAM);
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return (SNMP_FAILURE);
        }
    }

    switch (i4FsQoSQMapRegenPriType)
    {
        case QOS_QMAP_PRI_TYPE_NONE:
            /* CLASS specific Q */
            if ((u4FsQoSQMapCLASS == 0) || (u4FsQoSQMapRegenPriority != 0))
            {
                CLI_SET_ERR (QOS_CLI_ERR_INVAL_QUEUE_PARAM);
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                return (SNMP_FAILURE);
            }

            if (QoSUtlValidateClass (u4FsQoSQMapCLASS, pu4ErrorCode) ==
                QOS_FAILURE)
            {
                return (SNMP_FAILURE);
            }
            break;

        case QOS_QMAP_PRI_TYPE_INT_PRI:
        case QOS_QMAP_PRI_TYPE_VLAN_PRI:
            if (u4FsQoSQMapRegenPriority > QOS_MAX_REGEN_INNER_PRIORITY_VAL)
            {
                CLI_SET_ERR (QOS_CLI_ERR_QMAP_PRI_RANGE);
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                return (SNMP_FAILURE);
            }
            break;

        case QOS_QMAP_PRI_TYPE_IP_TOS:
            /* IP TOS-PRI BIT(3) */
            if (u4FsQoSQMapRegenPriority > QOS_QMAP_PRI_IP_TOS_MAX)
            {
                CLI_SET_ERR (QOS_CLI_ERR_QMAP_PRI_RANGE);
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                return (SNMP_FAILURE);
            }
            break;

        case QOS_QMAP_PRI_TYPE_IP_DSCP:

            /* IP DSCP BIT(6) */
            if (u4FsQoSQMapRegenPriority > QOS_QMAP_PRI_IP_DSCP_MAX)
            {
                CLI_SET_ERR (QOS_CLI_ERR_QMAP_PRI_RANGE);
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                return (SNMP_FAILURE);
            }
            break;

        case QOS_QMAP_PRI_TYPE_MPLS_EXP:
            /* MPLS EXP  BIT(3) */
            if (u4FsQoSQMapRegenPriority > QOS_QMAP_PRI_MPLS_EXP_MAX)
            {
                CLI_SET_ERR (QOS_CLI_ERR_QMAP_PRI_RANGE);
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                return (SNMP_FAILURE);
            }
            break;

        case QOS_QMAP_PRI_TYPE_VLAN_DEI:
            /* VLAN DEI BIT(1) */
            if ((u4FsQoSQMapRegenPriority != QOS_QMAP_PRI_VLAN_DEI_MIN) &&
                (u4FsQoSQMapRegenPriority != QOS_QMAP_PRI_VLAN_DEI_MAX))
            {
                CLI_SET_ERR (QOS_CLI_ERR_QMAP_PRI_RANGE);
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                return (SNMP_FAILURE);
            }
            break;
        case QOS_QMAP_PRI_TYPE_DOT1P:
            /*VLAN Priority */
            if (u4FsQoSQMapRegenPriority > QOS_QMAP_PRI_VLAN_PRI_MAX)
            {
                CLI_SET_ERR (QOS_CLI_ERR_QMAP_PRI_RANGE);
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                return (SNMP_FAILURE);
            }
            break;
        default:
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            CLI_SET_ERR (QOS_CLI_ERR_INVALID_PRI_TYPE);
            return (SNMP_FAILURE);
    }

    pQMapNode = QoSUtlGetQMapNode (i4IfIndex, u4FsQoSQMapCLASS,
                                   i4FsQoSQMapRegenPriType,
                                   u4FsQoSQMapRegenPriority);
    if (pQMapNode == NULL)
    {
        /* Entry Not Found In The Table */
        switch (i4TestValFsQoSQMapStatus)
        {
            case ACTIVE:
            case NOT_IN_SERVICE:
            case CREATE_AND_GO:
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                i4ReturnValue = SNMP_FAILURE;
                break;

            case CREATE_AND_WAIT:
                /* Check the No of Entries in the Table */
                i4RetStatus = RBTreeCount (gQoSGlobalInfo.pRbQMapTbl, &u4Count);
                if ((i4RetStatus == RB_FAILURE) ||
                    (u4Count > QOS_Q_MAP_TBL_MAX_ENTRIES))
                {
                    QOS_TRC_ARG2 (MGMT_TRC, "%s : Max No of QMap Table Entries "
                                  "are Configured. No of Max Entries %d. \r\n",
                                  __FUNCTION__, QOS_Q_MAP_TBL_MAX_ENTRIES);
                    CLI_SET_ERR (QOS_CLI_ERR_QMAP_MAX_ENTRY);
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;

                    return (SNMP_FAILURE);
                }

                break;

            case DESTROY:
                CLI_SET_ERR (QOS_CLI_ERR_QMAP_NO_ENTRY);
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                return (SNMP_FAILURE);
                break;

            default:
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                i4ReturnValue = SNMP_FAILURE;
                break;
        }
    }
    else
    {
        /* Entry Found In The Table */
        switch (i4TestValFsQoSQMapStatus)
        {
            case ACTIVE:

                if ((pQMapNode->u1Status == NOT_READY))
                {
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                    return (SNMP_FAILURE);
                }

                break;

            case NOT_IN_SERVICE:

                if ((pQMapNode->u1Status == NOT_READY))
                {
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                    return (SNMP_FAILURE);
                }

                break;

            case DESTROY:
                /* Check(Test) is it Default Entry. */
                i4RetStatus = QoSIsDefQMapTblEntry (pQMapNode);
                if (i4RetStatus == QOS_SUCCESS)
                {
                    QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSIsQMapDefEntry  "
                                  "Returns FAILURE.\r\n", __FUNCTION__);
                    CLI_SET_ERR (QOS_CLI_ERR_DEF_ENTRY);
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                    return (SNMP_FAILURE);
                }
                break;

            case CREATE_AND_WAIT:
            case CREATE_AND_GO:
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                i4ReturnValue = SNMP_FAILURE;
                break;

            default:
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                i4ReturnValue = SNMP_FAILURE;
                break;

        }                        /* End of switch */

    }                            /* End of if */

    return ((INT1) i4ReturnValue);
}

/****************************************************************************
 Function    :  nmhTestv2FsQoSQMapRegenDEI
 Input       :  The Indices
                IfIndex
                FsQoSQMapCLASS
                FsQoSQMapRegenPriType
                FsQoSQMapRegenPriority

                The Object
                testValFsQoSQMapRegenDEI      
Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsQoSQMapRegenDEI (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                            UINT4 u4FsQoSQMapCLASS,
                            INT4 i4FsQoSQMapRegenPriType,
                            UINT4 u4FsQoSQMapRegenPriority,
                            INT4 i4TestValFsQoSQMapRegenDEI)
{
    UNUSED_PARAM (*pu4ErrorCode);
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (u4FsQoSQMapCLASS);
    UNUSED_PARAM (i4FsQoSQMapRegenPriType);
    UNUSED_PARAM (u4FsQoSQMapRegenPriority);
    UNUSED_PARAM (i4TestValFsQoSQMapRegenDEI);
    return FNP_SUCCESS;

}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsQoSQMapTable
 Input       :  The Indices
                IfIndex
                FsQoSQMapCLASS
                FsQoSQMapRegenPriType
                FsQoSQMapRegenPriority
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsQoSQMapTable (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                        tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsQoSQTable. */
/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsQoSQTable
 Input       :  The Indices
                IfIndex
                FsQoSQId

 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsQoSQTable (INT4 i4IfIndex, UINT4 u4FsQoSQId)
{
    tQoSQNode          *pQNode = NULL;

    if (gQoSGlobalInfo.eSysControl != QOS_SYS_CNTL_START)
    {
        QOS_TRC (MGMT_TRC, QOS_MODULE_SHUTDOW_ERROR_MSG);
        return (SNMP_FAILURE);
    }

    pQNode = QoSUtlGetQNode (i4IfIndex, u4FsQoSQId);
    if (pQNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetQNode () Returns "
                      "FAILURE. \r\n", __FUNCTION__);
        return (SNMP_FAILURE);
    }

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsQoSQTable
 Input       :  The Indices
                IfIndex
                FsQoSQId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsQoSQTable (INT4 *pi4IfIndex, UINT4 *pu4FsQoSQId)
{
    INT4                i4RetStatus = QOS_FAILURE;

    if (gQoSGlobalInfo.eSysControl != QOS_SYS_CNTL_START)
    {
        QOS_TRC (MGMT_TRC, QOS_MODULE_SHUTDOW_ERROR_MSG);
        return (SNMP_FAILURE);
    }

    i4RetStatus = QoSGetNextQTblEntryIndex (0, pi4IfIndex, 0, pu4FsQoSQId);
    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSGetNextQTblEntryIndex () "
                      "Returns FAILURE. \r\n", __FUNCTION__);
        return (SNMP_FAILURE);
    }

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsQoSQTable
 Input       :  The Indices
                IfIndex
                nextIfIndex
                FsQoSQId
                nextFsQoSQId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsQoSQTable (INT4 i4IfIndex, INT4 *pi4NextIfIndex,
                            UINT4 u4FsQoSQId, UINT4 *pu4NextFsQoSQId)
{
    INT4                i4RetStatus = QOS_FAILURE;

    if (gQoSGlobalInfo.eSysControl != QOS_SYS_CNTL_START)
    {
        QOS_TRC (MGMT_TRC, QOS_MODULE_SHUTDOW_ERROR_MSG);
        return (SNMP_FAILURE);
    }

    i4RetStatus = QoSGetNextQTblEntryIndex (i4IfIndex, pi4NextIfIndex,
                                            u4FsQoSQId, pu4NextFsQoSQId);
    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSGetNextQTblEntryIndex () "
                      "Returns FAILURE. \r\n", __FUNCTION__);
        return (SNMP_FAILURE);
    }

    return (SNMP_SUCCESS);
}

/* Low Level GET Routine for All Objects  */
/****************************************************************************
 Function    :  nmhGetFsQoSQCfgTemplateId
 Input       :  The Indices
                IfIndex
                FsQoSQId

                The Object 
                retValFsQoSQCfgTemplateId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsQoSQCfgTemplateId (INT4 i4IfIndex, UINT4 u4FsQoSQId,
                           UINT4 *pu4RetValFsQoSQCfgTemplateId)
{
    tQoSQNode          *pQNode = NULL;

    pQNode = QoSUtlGetQNode (i4IfIndex, u4FsQoSQId);
    if (pQNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetQNode () "
                      "Returns FAILURE. \r\n", __FUNCTION__);
        return (SNMP_FAILURE);
    }

    *pu4RetValFsQoSQCfgTemplateId = pQNode->u4CfgTemplateId;

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFsQoSQSchedulerId
 Input       :  The Indices
                IfIndex
                FsQoSQId

                The Object 
                retValFsQoSQSchedulerId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsQoSQSchedulerId (INT4 i4IfIndex, UINT4 u4FsQoSQId,
                         UINT4 *pu4RetValFsQoSQSchedulerId)
{
    tQoSQNode          *pQNode = NULL;

    pQNode = QoSUtlGetQNode (i4IfIndex, u4FsQoSQId);
    if (pQNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetQNode () "
                      "Returns FAILURE. \r\n", __FUNCTION__);
        return (SNMP_FAILURE);
    }

    *pu4RetValFsQoSQSchedulerId = pQNode->u4SchedulerId;

    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhGetFsQoSQWeight
 Input       :  The Indices
                IfIndex
                FsQoSQId

                The Object 
                retValFsQoSQWeight
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsQoSQWeight (INT4 i4IfIndex, UINT4 u4FsQoSQId,
                    UINT4 *pu4RetValFsQoSQWeight)
{
    tQoSQNode          *pQNode = NULL;

    pQNode = QoSUtlGetQNode (i4IfIndex, u4FsQoSQId);
    if (pQNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetQNode () "
                      "Returns FAILURE. \r\n", __FUNCTION__);
        return (SNMP_FAILURE);
    }

    *pu4RetValFsQoSQWeight = (UINT4) pQNode->u2Weight;

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFsQoSQPriority
 Input       :  The Indices
                IfIndex
                FsQoSQId

                The Object 
                retValFsQoSQPriority
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsQoSQPriority (INT4 i4IfIndex, UINT4 u4FsQoSQId,
                      UINT4 *pu4RetValFsQoSQPriority)
{
    tQoSQNode          *pQNode = NULL;

    pQNode = QoSUtlGetQNode (i4IfIndex, u4FsQoSQId);
    if (pQNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetQNode () "
                      "Returns FAILURE. \r\n", __FUNCTION__);
        return (SNMP_FAILURE);
    }

    *pu4RetValFsQoSQPriority = (UINT4) pQNode->u1Priority;

    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhGetFsQoSQShapeId
 Input       :  The Indices
                IfIndex
                FsQoSQId

                The Object 
                retValFsQoSQShapeId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsQoSQShapeId (INT4 i4IfIndex, UINT4 u4FsQoSQId,
                     UINT4 *pu4RetValFsQoSQShapeId)
{
    tQoSQNode          *pQNode = NULL;

    pQNode = QoSUtlGetQNode (i4IfIndex, u4FsQoSQId);
    if (pQNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetQNode () "
                      "Returns FAILURE. \r\n", __FUNCTION__);
        return (SNMP_FAILURE);
    }

    *pu4RetValFsQoSQShapeId = pQNode->u4ShapeId;

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFsQoSQStatus
 Input       :  The Indices
                IfIndex
                FsQoSQId

                The Object 
                retValFsQoSQStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsQoSQStatus (INT4 i4IfIndex, UINT4 u4FsQoSQId,
                    INT4 *pi4RetValFsQoSQStatus)
{
    tQoSQNode          *pQNode = NULL;

    pQNode = QoSUtlGetQNode (i4IfIndex, u4FsQoSQId);
    if (pQNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetQNode () "
                      "Returns FAILURE. \r\n", __FUNCTION__);
        return (SNMP_FAILURE);
    }

    *pi4RetValFsQoSQStatus = (INT4) pQNode->u1Status;

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFsQoSQType
 Input       :  The Indices
                IfIndex
                FsQoSQId

                The Object 
                retValFsQoSQType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsQoSQType (INT4 i4IfIndex, UINT4 u4FsQoSQId, UINT4 *pu4RetValFsQoSQType)
{
    tQoSQNode          *pQNode = NULL;

    pQNode = QoSUtlGetQNode (i4IfIndex, u4FsQoSQId);
    if (pQNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetQNode () "
                      "Returns FAILURE. \r\n", __FUNCTION__);
        return (SNMP_FAILURE);
    }

    *pu4RetValFsQoSQType = pQNode->u4QueueType;

    return (SNMP_SUCCESS);
}

/* Low Level SET Routine for All Objects  */
/****************************************************************************
 Function    :  nmhSetFsQoSQCfgTemplateId
 Input       :  The Indices
                IfIndex
                FsQoSQId

                The Object 
                setValFsQoSQCfgTemplateId
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsQoSQCfgTemplateId (INT4 i4IfIndex, UINT4 u4FsQoSQId,
                           UINT4 u4SetValFsQoSQCfgTemplateId)
{
    tQoSQNode          *pQNode = NULL;
    tQoSQTemplateNode  *pQTempNode = NULL;
    tQoSQTemplateNode  *pTmpQTempNode = NULL;
    UINT4               u4TmpQTempId = 0;

    pQNode = QoSUtlGetQNode (i4IfIndex, u4FsQoSQId);
    if (pQNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetQNode () "
                      "Returns FAILURE. \r\n", __FUNCTION__);
        return (SNMP_FAILURE);
    }

    /* If the Same value is configured again no need to incrment the 
     * RefCount and Set */
    if (pQNode->u4CfgTemplateId == u4SetValFsQoSQCfgTemplateId)
    {
        return (SNMP_SUCCESS);
    }

    if (u4SetValFsQoSQCfgTemplateId != 0)
    {
        pQTempNode = QoSUtlGetQTempNode (u4SetValFsQoSQCfgTemplateId);
        if (pQTempNode == NULL)
        {
            QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetQTempNode () "
                          "Returns FAILURE. \r\n", __FUNCTION__);
            return (SNMP_FAILURE);
        }
        /* Incremnet the RefCount of QTempId  Entry in the QTemp Table */
        pQTempNode->u4RefCount = (pQTempNode->u4RefCount) + 1;
    }

    /* Store the old value ,Reset corresponding ref count if the id not '0' */
    u4TmpQTempId = pQNode->u4CfgTemplateId;
    if (u4TmpQTempId != 0)
    {
        pTmpQTempNode = QoSUtlGetQTempNode (u4TmpQTempId);
        if (pTmpQTempNode == NULL)
        {
            QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetQTempNode () "
                          "Returns FAILURE. \r\n", __FUNCTION__);

            return (SNMP_FAILURE);
        }
        pTmpQTempNode->u4RefCount = (pTmpQTempNode->u4RefCount) - 1;
    }

    pQNode->u4CfgTemplateId = u4SetValFsQoSQCfgTemplateId;

    QoSValidateQTblEntry (pQNode);

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhSetFsQoSQSchedulerId
 Input       :  The Indices
                IfIndex
                FsQoSQId

                The Object 
                setValFsQoSQSchedulerId
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsQoSQSchedulerId (INT4 i4IfIndex, UINT4 u4FsQoSQId,
                         UINT4 u4SetValFsQoSQSchedulerId)
{
    tQoSQNode          *pQNode = NULL;
    tQoSSchedNode      *pSchedNode = NULL;
    tQoSSchedNode      *pTmpSchedNode = NULL;
    UINT4               u4TmpSchedId = 0;
    UINT4               u4IfIndex, u4SchedulerId;

    pQNode = QoSUtlGetQNode (i4IfIndex, u4FsQoSQId);
    if (pQNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetQNode () "
                      "Returns FAILURE. \r\n", __FUNCTION__);
        return (SNMP_FAILURE);
    }

    /* If the Same value is configured again no need to incrment the 
     * RefCount and Set */
    if (pQNode->u4SchedulerId == u4SetValFsQoSQSchedulerId)
    {
        return (SNMP_SUCCESS);
    }

    if (u4SetValFsQoSQSchedulerId != 0)
    {
        pSchedNode = QoSUtlGetSchedNode (i4IfIndex, u4SetValFsQoSQSchedulerId);
        if (pSchedNode == NULL)
        {
            /* This could be a General Scheduler that can be on any port */
            if (QosSchedulerGetIfIdFromGlobalId (u4SetValFsQoSQSchedulerId,
                                                 &u4IfIndex, &u4SchedulerId)
                == QOS_SUCCESS)
            {
                if (u4IfIndex == 0)
                {
                    if ((pSchedNode =
                         QoSUtlGetSchedNode (u4IfIndex, u4SchedulerId)) == NULL)
                    {
                        CLI_SET_ERR (QOS_CLI_ERR_SCHED_NO_ENTRY);
                        return (SNMP_FAILURE);
                    }
                }
                else
                {
                    CLI_SET_ERR (QOS_CLI_ERR_SCHED_NO_ENTRY);
                    return (SNMP_FAILURE);
                }
            }

        }
        /* Incremnet the RefCount of SchedId  Entry in the Sched Table */
        if (pSchedNode != NULL)
        {
            pSchedNode->u4RefCount = (pSchedNode->u4RefCount) + 1;
        }
    }

    /* Store the old value ,Reset corresponding ref count if the id not '0' */
    u4TmpSchedId = pQNode->u4SchedulerId;
    if (u4TmpSchedId != 0)
    {
        pTmpSchedNode = QoSUtlGetSchedNode (i4IfIndex, u4TmpSchedId);
        if (pTmpSchedNode == NULL)
        {
            QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetSchedNode () "
                          "Returns FAILURE. \r\n", __FUNCTION__);

            return (SNMP_FAILURE);
        }
        pTmpSchedNode->u4RefCount = (pTmpSchedNode->u4RefCount) - 1;

    }

    pQNode->u4SchedulerId = u4SetValFsQoSQSchedulerId;

    QoSValidateQTblEntry (pQNode);

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhSetFsQoSQWeight
 Input       :  The Indices
                IfIndex
                FsQoSQId

                The Object 
                setValFsQoSQWeight
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsQoSQWeight (INT4 i4IfIndex, UINT4 u4FsQoSQId,
                    UINT4 u4SetValFsQoSQWeight)
{
    tQoSQNode          *pQNode = NULL;

    pQNode = QoSUtlGetQNode (i4IfIndex, u4FsQoSQId);
    if (pQNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetQNode () "
                      "Returns FAILURE. \r\n", __FUNCTION__);
        return (SNMP_FAILURE);
    }

    pQNode->u2Weight = (UINT2) u4SetValFsQoSQWeight;

    QoSValidateQTblEntry (pQNode);

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhSetFsQoSQPriority
 Input       :  The Indices
                IfIndex
                FsQoSQId

                The Object 
                setValFsQoSQPriority
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsQoSQPriority (INT4 i4IfIndex, UINT4 u4FsQoSQId,
                      UINT4 u4SetValFsQoSQPriority)
{
    tQoSQNode          *pQNode = NULL;

    pQNode = QoSUtlGetQNode (i4IfIndex, u4FsQoSQId);
    if (pQNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetQNode () "
                      "Returns FAILURE. \r\n", __FUNCTION__);
        return (SNMP_FAILURE);
    }

    pQNode->u1Priority = (UINT1) u4SetValFsQoSQPriority;

    QoSValidateQTblEntry (pQNode);

    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhSetFsQoSQShapeId
 Input       :  The Indices
                IfIndex
                FsQoSQId

                The Object 
                setValFsQoSQShapeId
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsQoSQShapeId (INT4 i4IfIndex, UINT4 u4FsQoSQId,
                     UINT4 u4SetValFsQoSQShapeId)
{
    tQoSQNode          *pQNode = NULL;
    tQoSShapeTemplateNode *pShapeNode = NULL;
    tQoSShapeTemplateNode *pTmpShapeNode = NULL;
    UINT4               u4TmpShapeId = 0;

    pQNode = QoSUtlGetQNode (i4IfIndex, u4FsQoSQId);
    if (pQNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetQNode () "
                      "Returns FAILURE. \r\n", __FUNCTION__);
        return (SNMP_FAILURE);
    }

    /* If the Same value is configured again no need to incrment the 
     * RefCount and Set */
    if (pQNode->u4ShapeId == u4SetValFsQoSQShapeId)
    {
        return (SNMP_SUCCESS);
    }

    /* Store the old value ,Reset corresponding ref count if the id not '0' */
    u4TmpShapeId = pQNode->u4ShapeId;
    if (u4TmpShapeId != 0)
    {
        pTmpShapeNode = QoSUtlGetShapeTempNode (u4TmpShapeId);
        if (pTmpShapeNode == NULL)
        {
            QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetShapeTempNode () "
                          "Returns FAILURE. \r\n", __FUNCTION__);

            return (SNMP_FAILURE);
        }
        pTmpShapeNode->u4RefCount = (pTmpShapeNode->u4RefCount) - 1;
    }

    if (u4SetValFsQoSQShapeId != 0)
    {
        pShapeNode = QoSUtlGetShapeTempNode (u4SetValFsQoSQShapeId);
        if (pShapeNode == NULL)
        {
            QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetShapeTempNode () "
                          "Returns FAILURE. \r\n", __FUNCTION__);
            return (SNMP_FAILURE);
        }

        /* Incremnet the RefCount of ShapeId  Entry in the ShapeTempTable */
        pShapeNode->u4RefCount = (pShapeNode->u4RefCount) + 1;
    }

    pQNode->u4ShapeId = u4SetValFsQoSQShapeId;

    QoSValidateQTblEntry (pQNode);

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhSetFsQoSQStatus
 Input       :  The Indices
                IfIndex
                FsQoSQId

                The Object 
                setValFsQoSQStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsQoSQStatus (INT4 i4IfIndex, UINT4 u4FsQoSQId, INT4 i4SetValFsQoSQStatus)
{
    tQoSQNode          *pQNode = NULL;
    INT4                i4RetStatus = QOS_FAILURE;
    UINT4               u4GlobalQid;
    INT4                i4ReturnValue = SNMP_SUCCESS;

    pQNode = QoSUtlGetQNode (i4IfIndex, u4FsQoSQId);
    if (pQNode != NULL)
    {
        /* Entry found in the Table */
        if (pQNode->u1Status == i4SetValFsQoSQStatus)
        {
            /* same value */
            return (SNMP_SUCCESS);
        }
    }
    else
    {
        /* New Entry need to be created in the Table, only if 
         * i4SetValFsQoSQStatus = CREATE_AND_WAIT */
        /* Optional */
        if (i4SetValFsQoSQStatus == DESTROY)
        {
            return (SNMP_SUCCESS);
        }
    }

    switch (i4SetValFsQoSQStatus)
    {
            /* For a New Entry */
        case CREATE_AND_WAIT:

            pQNode = QoSCreateQTblEntry (i4IfIndex, u4FsQoSQId);
            if (pQNode == NULL)
            {
                QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSCreateQTblEntry ()"
                              " Returns FAILURE. \r\n", __FUNCTION__);
                return (SNMP_FAILURE);
            }
            if (pQNode != NULL)
            {
                pQNode->u1Status = NOT_READY;
            }

            i4ReturnValue = SNMP_SUCCESS;

            break;

            /* Modify an Entry */
        case ACTIVE:

            if (pQNode != NULL)
            {
                /* Boundary check for COSQ's . COSQ 1-8 are UC queues                   
                 * and COSQ 9-12 are MC queues.
                 */
                if ((u4FsQoSQId <= QOS_QUEUE_MAX_NUM_UCOSQ) &&
                    (pQNode->u4QueueType != QOS_Q_UNICAST_TYPE))
                {
                    UtlTrcLog (1, 1, "% QOS", "Cannot configure COSQ's "
                               "1-%d as MC queues\r\n",
                               QOS_QUEUE_MAX_NUM_UCOSQ);
                    CLI_SET_ERR (QOS_CLI_ERR_QTYPE_MULTICAST);
                    return (SNMP_FAILURE);
                }
                else if (((u4FsQoSQId > QOS_QUEUE_MAX_NUM_UCOSQ) &&
                          (u4FsQoSQId <= QOS_QUEUE_ENTRY_MAX)) &&
                         (pQNode->u4QueueType != QOS_Q_MULTICAST_TYPE))
                {

                    UtlTrcLog (1, 1, "% QOS", "Cannot configure COSQ's "
                               "%d-%d as UC queues\r\n",
                               (QOS_QUEUE_MAX_NUM_UCOSQ + 1),
                               QOS_QUEUE_ENTRY_MAX);
                    CLI_SET_ERR (QOS_CLI_ERR_QTYPE_UNICAST);
                    return (SNMP_FAILURE);
                }

                /* 1. Add the entry into the HARDWARE. */
                if (gQoSGlobalInfo.eSysStatus == QOS_SYS_STATUS_ENABLE)
                {
                    i4RetStatus = QoSHwWrQueueCreate (pQNode->i4IfIndex,
                                                      pQNode->u4Id);
                    if ((i4RetStatus == QOS_FAILURE)
                        || (i4RetStatus == QOS_NP_NOT_SUPPORTED))
                    {
                        if (i4RetStatus == QOS_NP_NOT_SUPPORTED)
                        {
                            CLI_SET_ERR (QOS_CLI_ERR_INVAL_QUEUE_PARAM);
                        }
                        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSHwWrQueueCreate ()"
                                      " Returns FAILURE.\r\n", __FUNCTION__);
                        return (SNMP_FAILURE);
                    }
                }
                pQNode->u1Status = ACTIVE;
            }

            break;

        case NOT_IN_SERVICE:

            /* 1. Remove the entry from the HARDWARE. */
            if (pQNode != NULL)
            {
                pQNode->u1Status = NOT_IN_SERVICE;
            }

            break;

        case DESTROY:

            /* 1. Remove the entry from the HARDWARE. */
            if ((pQNode->u1Status == ACTIVE) &&
                (gQoSGlobalInfo.eSysStatus == QOS_SYS_STATUS_ENABLE))
            {
                i4RetStatus = QoSHwWrQueueDelete (pQNode->i4IfIndex,
                                                  pQNode->u4Id);
                if (i4RetStatus == QOS_FAILURE)
                {
                    QOS_TRC_ARG1 (MGMT_TRC, "In %s : "
                                  "QoSHwWrQueueDelete () Returns FAILURE.\r\n",
                                  __FUNCTION__);
                    return (SNMP_FAILURE);
                }
            }

            /* 2. Remove the entry from the SOFTWARE. */
            /* and Decrement the RefCount for QTemp, Sched,Shape Tables. */
            i4RetStatus = QoSDeleteQTblEntry (pQNode);
            if (i4RetStatus == QOS_FAILURE)
            {
                QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSDeleteQTblEntry ()"
                              " Returns FAILURE. \r\n", __FUNCTION__);
                return (SNMP_FAILURE);
            }

            break;

        case CREATE_AND_GO:
            i4ReturnValue = SNMP_FAILURE;
            break;

        default:
            i4ReturnValue = SNMP_FAILURE;
            break;
    }

    /* Update the Next free with the global Qid */
    if (i4ReturnValue == SNMP_SUCCESS)
    {
        u4GlobalQid = QOS_GLOBAL_QID_FROM_IF_QID (i4IfIndex, u4FsQoSQId);
        QosUpdateDiffServQNextFree (u4GlobalQid, i4SetValFsQoSQStatus);
    }

    return ((INT1) i4ReturnValue);
}

/****************************************************************************
 Function    :  nmhSetFsQoSQType
 Input       :  The Indices
                IfIndex
                FsQoSQId

                The Object 
                setValFsQoSQType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsQoSQType (INT4 i4IfIndex, UINT4 u4FsQoSQId, UINT4 u4SetValFsQoSQType)
{
    tQoSQNode          *pQNode = NULL;

    pQNode = QoSUtlGetQNode (i4IfIndex, u4FsQoSQId);
    if (pQNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetQNode () "
                      "Returns FAILURE. \r\n", __FUNCTION__);
        return (SNMP_FAILURE);
    }

    pQNode->u4QueueType = u4SetValFsQoSQType;

    QoSValidateQTblEntry (pQNode);

    return (SNMP_SUCCESS);
}

/* Low Level TEST Routines for All Objects  */
/****************************************************************************
 Function    :  nmhTestv2FsQoSQCfgTemplateId
 Input       :  The Indices
                IfIndex
                FsQoSQId

                The Object 
                testValFsQoSQCfgTemplateId
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsQoSQCfgTemplateId (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                              UINT4 u4FsQoSQId,
                              UINT4 u4TestValFsQoSQCfgTemplateId)
{
    INT4                i4RetStatus = QOS_FAILURE;

    *pu4ErrorCode = SNMP_ERR_NO_ERROR;

    /* Check Index */
    i4RetStatus = QoSUtlValidateQTblIdxInst (i4IfIndex, u4FsQoSQId,
                                             pu4ErrorCode);
    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlValidateQTblIdxInst ()"
                      " Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    /* Check  Object's Value */
    i4RetStatus = QoSUtlValidateQTemp (u4TestValFsQoSQCfgTemplateId,
                                       pu4ErrorCode);
    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlValidateQTemp ()"
                      " Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhTestv2FsQoSQSchedulerId
 Input       :  The Indices
                IfIndex
                FsQoSQId

                The Object 
                testValFsQoSQSchedulerId
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsQoSQSchedulerId (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                            UINT4 u4FsQoSQId, UINT4 u4TestValFsQoSQSchedulerId)
{
    INT4                i4RetStatus = QOS_FAILURE;
    tQoSQNode          *pQNode = NULL;
    *pu4ErrorCode = SNMP_ERR_NO_ERROR;

    /* Check Index */
    i4RetStatus = QoSUtlValidateQTblIdxInst (i4IfIndex, u4FsQoSQId,
                                             pu4ErrorCode);
    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlValidateQTblIdxInst ()"
                      " Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    /* Check  Object's Value */
    i4RetStatus = QoSUtlValidateSched (i4IfIndex, u4TestValFsQoSQSchedulerId,
                                       pu4ErrorCode);
    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlValidateSched ()"
                      " Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }
    pQNode = QoSUtlGetQNode (i4IfIndex, u4FsQoSQId);
    if (pQNode != NULL)
    {
        if (pQNode->u4SchedulerId != u4TestValFsQoSQSchedulerId)
        {
            CLI_SET_ERR (QOS_CLI_ERR_Q_ID_SCHED_ENTRY);
            return (QOS_FAILURE);
        }
    }

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhTestv2FsQoSQWeight
 Input       :  The Indices
                IfIndex
                FsQoSQId

                The Object 
                testValFsQoSQWeight
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsQoSQWeight (UINT4 *pu4ErrorCode, INT4 i4IfIndex, UINT4 u4FsQoSQId,
                       UINT4 u4TestValFsQoSQWeight)
{
    INT4                i4RetStatus = QOS_FAILURE;

    *pu4ErrorCode = SNMP_ERR_NO_ERROR;

    /* Check Index */
    i4RetStatus = QoSUtlValidateQTblIdxInst (i4IfIndex, u4FsQoSQId,
                                             pu4ErrorCode);
    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlValidateQTblIdxInst ()"
                      " Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    /* Check  Object's Value */
    if (u4TestValFsQoSQWeight > QOS_Q_TBL_Q_WEIGHT_MAX_VAL)
    {
        QOS_TRC_ARG3 (MGMT_TRC, "%s : Q Weight value is out of range. The"
                      " range should be Min %d - Max %d. \r\n", __FUNCTION__,
                      QOS_Q_TBL_Q_WEIGHT_MIN_VAL, QOS_Q_TBL_Q_WEIGHT_MAX_VAL);

        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (QOS_CLI_ERR_WEIGHT);
        return (SNMP_FAILURE);
    }

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhTestv2FsQoSQPriority
 Input       :  The Indices
                IfIndex
                FsQoSQId

                The Object 
                testValFsQoSQPriority
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsQoSQPriority (UINT4 *pu4ErrorCode, INT4 i4IfIndex, UINT4 u4FsQoSQId,
                         UINT4 u4TestValFsQoSQPriority)
{
    INT4                i4RetStatus = QOS_FAILURE;

    *pu4ErrorCode = SNMP_ERR_NO_ERROR;

    /* Check Index */
    i4RetStatus = QoSUtlValidateQTblIdxInst (i4IfIndex, u4FsQoSQId,
                                             pu4ErrorCode);
    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlValidateQTblIdxInst ()"
                      " Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    /* Check  Object's Value */
    if (u4TestValFsQoSQPriority > QOS_Q_TBL_Q_PRIO_MAX_VAL)
    {
        QOS_TRC_ARG3 (MGMT_TRC, "%s : Q Priority value is out of range. The"
                      " range should be Min %d - Max %d. \r\n", __FUNCTION__,
                      QOS_Q_TBL_Q_PRIO_MIN_VAL, QOS_Q_TBL_Q_PRIO_MAX_VAL);

        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (QOS_CLI_ERR_PRIORITY);
        return (SNMP_FAILURE);
    }

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhTestv2FsQoSQShapeId
 Input       :  The Indices
                IfIndex
                FsQoSQId

                The Object 
                testValFsQoSQShapeId
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsQoSQShapeId (UINT4 *pu4ErrorCode, INT4 i4IfIndex, UINT4 u4FsQoSQId,
                        UINT4 u4TestValFsQoSQShapeId)
{
    INT4                i4RetStatus = QOS_FAILURE;

    *pu4ErrorCode = SNMP_ERR_NO_ERROR;

    /* Check Index */
    i4RetStatus = QoSUtlValidateQTblIdxInst (i4IfIndex, u4FsQoSQId,
                                             pu4ErrorCode);
    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlValidateQTblIdxInst ()"
                      " Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    /* Check  Object's Value */
    if (u4TestValFsQoSQShapeId == 0)
    {
        /* Reset the ShapeId */
        return (SNMP_SUCCESS);
    }

    i4RetStatus = QoSUtlValidateShapeId (u4TestValFsQoSQShapeId, pu4ErrorCode);
    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlValidateShapeId ()"
                      " Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhTestv2FsQoSQStatus
 Input       :  The Indices
                IfIndex
                FsQoSQId

                The Object 
                testValFsQoSQStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsQoSQStatus (UINT4 *pu4ErrorCode, INT4 i4IfIndex, UINT4 u4FsQoSQId,
                       INT4 i4TestValFsQoSQStatus)
{
    tQoSQNode          *pQNode = NULL;
    INT4                i4RetStatus = QOS_FAILURE;
    UINT4               u4Count = 0;
    INT4                i4ReturnValue = SNMP_SUCCESS;
    if (gQoSGlobalInfo.eSysControl != QOS_SYS_CNTL_START)
    {
        CLI_SET_ERR (QOS_CLI_ERR_MODULE_SHUTDOW);
        QOS_TRC (MGMT_TRC, QOS_MODULE_SHUTDOW_ERROR_MSG);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return (SNMP_FAILURE);
    }

    *pu4ErrorCode = SNMP_ERR_NO_ERROR;

    /* Check the Range of Index Values (Min-Max) */
    i4RetStatus = QoSUtlValidateIfIndex (i4IfIndex, pu4ErrorCode);
    if (i4RetStatus == QOS_FAILURE)
    {
        return (SNMP_FAILURE);
    }

    /* Check the Range of Index Values (Min-Max) */
    i4RetStatus = QOS_CHECK_Q_TBL_INDEX_RANGE (u4FsQoSQId);
    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG3 (MGMT_TRC, "%s : Q Id %d is out of Range."
                      " The Range Should be (1-%d). \r\n", __FUNCTION__,
                      u4FsQoSQId, QOS_Q_TBL_MAX_INDEX_RANGE);
        CLI_SET_ERR (QOS_CLI_ERR_Q_RANGE);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;

        return (SNMP_FAILURE);
    }

    /* When HwQueueConfigOnLaPortSup is not supported, 
     * Block Sceduler and  Queue Configuration on a port which is member of port-channel port
     */
    if (ISS_HW_SUPPORTED !=
        IssGetHwCapabilities (ISS_HW_QUEUE_CONFIG_ON_LA_PORT))
    {
        if (L2IwfIsPortInPortChannel ((UINT4) i4IfIndex) == L2IWF_SUCCESS)
        {
            CLI_SET_ERR (QOS_CLI_ERR_PORTCHANNEL);
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return (SNMP_FAILURE);
        }
    }
    pQNode = QoSUtlGetQNode (i4IfIndex, u4FsQoSQId);
    if (pQNode == NULL)
    {
        /* Entry Not Found In The Table */
        switch (i4TestValFsQoSQStatus)
        {
            case ACTIVE:
            case NOT_IN_SERVICE:
            case CREATE_AND_GO:
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                i4ReturnValue = SNMP_FAILURE;
                break;

            case CREATE_AND_WAIT:
                /* Check the No of Entries in the Table */
                i4RetStatus = RBTreeCount (gQoSGlobalInfo.pRbQTbl, &u4Count);
                if ((i4RetStatus == RB_FAILURE) ||
                    (u4Count > QOS_Q_TBL_MAX_ENTRIES))
                {
                    QOS_TRC_ARG2 (MGMT_TRC, "%s : Max No of Q Table Entries "
                                  "are Configured. No of Max Entries %d. \r\n",
                                  __FUNCTION__, QOS_Q_TBL_MAX_ENTRIES);
                    CLI_SET_ERR (QOS_CLI_ERR_Q_MAX_ENTRY);
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;

                    return (SNMP_FAILURE);
                }
                if (u4FsQoSQId >= QOS_QUEUE_ENTRY_MAX)
                {
                    CLI_SET_ERR (QOS_CLI_ERR_Q_RANGE);
                    return (SNMP_FAILURE);
                }
                break;

            case DESTROY:
                CLI_SET_ERR (QOS_CLI_ERR_Q_NO_ENTRY);
                *pu4ErrorCode = SNMP_ERR_NO_CREATION;
                i4ReturnValue = SNMP_FAILURE;
                break;

            default:
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                i4ReturnValue = SNMP_FAILURE;
                break;
        }
    }
    else
    {
        /* Entry Found In The Table */
        switch (i4TestValFsQoSQStatus)
        {
            case ACTIVE:

                if ((pQNode->u1Status == NOT_READY))
                {
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                    CLI_SET_ERR (QOS_CLI_ERR_Q_MANDATORY);
                    return (SNMP_FAILURE);
                }

                break;

            case NOT_IN_SERVICE:

                if ((pQNode->u1Status == NOT_READY))
                {
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                    return (SNMP_FAILURE);
                }

                i4RetStatus = QOS_CHECK_TABLE_REF_COUNT (pQNode);
                if (i4RetStatus == QOS_FAILURE)
                {
                    QOS_TRC_ARG1 (MGMT_TRC, "In %s : ERROR - This Entry is "
                                  "referenced by Hierarchy Table Entry .\r\n",
                                  __FUNCTION__);
                    CLI_SET_ERR (QOS_CLI_ERR_Q_RFE);
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                    return (SNMP_FAILURE);
                }
                break;

            case DESTROY:
                /* Check(Test) is it Default Entry. */
                i4RetStatus = QoSIsDefQTblEntry (pQNode);
                if (i4RetStatus == QOS_SUCCESS)
                {
                    QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSIsQTblDefEntry  "
                                  "Returns FAILURE.\r\n", __FUNCTION__);
                    CLI_SET_ERR (QOS_CLI_ERR_DEF_ENTRY);
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                    return (SNMP_FAILURE);
                }

                i4RetStatus = QOS_CHECK_TABLE_REF_COUNT (pQNode);
                if (i4RetStatus == QOS_FAILURE)
                {
                    QOS_TRC_ARG1 (MGMT_TRC, "In %s : ERROR - This Entry is "
                                  "referenced by Hierarchy Table Entry .\r\n",
                                  __FUNCTION__);
                    CLI_SET_ERR (QOS_CLI_ERR_Q_RFE);
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                    return (SNMP_FAILURE);
                }
                if (pQNode != NULL)
                {
                    if (pQNode->u4QueueType == QOS_Q_SUBSCRIBER_TYPE)
                    {
                        QOS_TRC_ARG1 (MGMT_TRC, "In %s : ERROR - This Entry is "
                                      "referenced by Hierarchy Table Entry .\r\n",
                                      __FUNCTION__);
                        CLI_SET_ERR (QOS_CLI_ERR_QTYPE_SUBQ_DEL);
                        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                        return (SNMP_FAILURE);

                    }
                }
                break;

            case CREATE_AND_WAIT:
            case CREATE_AND_GO:
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                i4ReturnValue = SNMP_FAILURE;
                break;

            default:
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                i4ReturnValue = SNMP_FAILURE;
                break;

        }                        /* End of switch */

    }                            /* End of if */

    return ((INT1) i4ReturnValue);
}

/****************************************************************************
 Function    :  nmhTestv2FsQoSQType
 Input       :  The Indices
                IfIndex
                FsQoSQId

                The Object 
                testValFsQoSQType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsQoSQType (UINT4 *pu4ErrorCode, INT4 i4IfIndex, UINT4 u4FsQoSQId,
                     UINT4 u4TestValFsQoSQType)
{
    INT4                i4RetStatus = QOS_FAILURE;

    *pu4ErrorCode = SNMP_ERR_NO_ERROR;

    /* Check Index */
    i4RetStatus = QoSUtlValidateQTblIdxInst (i4IfIndex, u4FsQoSQId,
                                             pu4ErrorCode);
    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlValidateQTblIdxInst ()"
                      " Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    /* Check  Object's Value */
    if ((u4TestValFsQoSQType != QOS_Q_UNICAST_TYPE) &&
        (u4TestValFsQoSQType != QOS_Q_MULTICAST_TYPE))
    {
        QOS_TRC_ARG1 (MGMT_TRC, "%s : Q Type is not in range."
                      "Value can be Unicast (1) or Multicast (2)\r\n",
                      __FUNCTION__);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;

        return (SNMP_FAILURE);
    }

    return (SNMP_SUCCESS);
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsQoSQTable
 Input       :  The Indices
                IfIndex
                FsQoSQId
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsQoSQTable (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                     tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsQoSSchedulerTable. */
/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsQoSSchedulerTable
 Input       :  The Indices
                IfIndex
                FsQoSSchedulerId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsQoSSchedulerTable (INT4 i4IfIndex,
                                             UINT4 u4FsQoSSchedulerId)
{
    tQoSSchedNode      *pSchedNode = NULL;

    if (gQoSGlobalInfo.eSysControl != QOS_SYS_CNTL_START)
    {
        QOS_TRC (MGMT_TRC, QOS_MODULE_SHUTDOW_ERROR_MSG);
        return (SNMP_FAILURE);
    }

    pSchedNode = QoSUtlGetSchedNode (i4IfIndex, u4FsQoSSchedulerId);
    if (pSchedNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetSchedNode () Returns "
                      "FAILURE. \r\n", __FUNCTION__);
        return (SNMP_FAILURE);
    }

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsQoSSchedulerTable
 Input       :  The Indices
                IfIndex
                FsQoSSchedulerId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */
INT1
nmhGetFirstIndexFsQoSSchedulerTable (INT4 *pi4IfIndex,
                                     UINT4 *pu4FsQoSSchedulerId)
{
    INT4                i4RetStatus = QOS_FAILURE;

    if (gQoSGlobalInfo.eSysControl != QOS_SYS_CNTL_START)
    {
        QOS_TRC (MGMT_TRC, QOS_MODULE_SHUTDOW_ERROR_MSG);
        return (SNMP_FAILURE);
    }

    i4RetStatus = QoSGetNextSchedTblEntryIndex (0, pi4IfIndex,
                                                0, pu4FsQoSSchedulerId);
    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSGetNextSchedTblEntryIndex () "
                      "Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsQoSSchedulerTable
 Input       :  The Indices
                IfIndex
                nextIfIndex
                FsQoSSchedulerId
                nextFsQoSSchedulerId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsQoSSchedulerTable (INT4 i4IfIndex, INT4 *pi4NextIfIndex,
                                    UINT4 u4FsQoSSchedulerId,
                                    UINT4 *pu4NextFsQoSSchedulerId)
{
    INT4                i4RetStatus = QOS_FAILURE;

    if (gQoSGlobalInfo.eSysControl != QOS_SYS_CNTL_START)
    {
        QOS_TRC (MGMT_TRC, QOS_MODULE_SHUTDOW_ERROR_MSG);
        return (SNMP_FAILURE);
    }

    i4RetStatus = QoSGetNextSchedTblEntryIndex (i4IfIndex, pi4NextIfIndex,
                                                u4FsQoSSchedulerId,
                                                pu4NextFsQoSSchedulerId);
    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSGetNextSchedTblEntryIndex () "
                      "Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    return (SNMP_SUCCESS);
}

/* Low Level GET Routine for All Objects  */
/****************************************************************************
 Function    :  nmhGetFsQoSSchedulerSchedAlgorithm
 Input       :  The Indices
                IfIndex
                FsQoSSchedulerId

                The Object 
                retValFsQoSSchedulerSchedAlgorithm
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsQoSSchedulerSchedAlgorithm (INT4 i4IfIndex, UINT4 u4FsQoSSchedulerId,
                                    INT4 *pi4RetValFsQoSSchedulerSchedAlgorithm)
{
    tQoSSchedNode      *pSchedNode = NULL;

    pSchedNode = QoSUtlGetSchedNode (i4IfIndex, u4FsQoSSchedulerId);

    if (pSchedNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetSchedNode () "
                      "Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    *pi4RetValFsQoSSchedulerSchedAlgorithm = (UINT4)
        pSchedNode->u1SchedAlgorithm;

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFsQoSSchedulerShapeId
 Input       :  The Indices
                IfIndex
                FsQoSSchedulerId

                The Object 
                retValFsQoSSchedulerShapeId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsQoSSchedulerShapeId (INT4 i4IfIndex, UINT4 u4FsQoSSchedulerId,
                             UINT4 *pu4RetValFsQoSSchedulerShapeId)
{
    tQoSSchedNode      *pSchedNode = NULL;

    pSchedNode = QoSUtlGetSchedNode (i4IfIndex, u4FsQoSSchedulerId);

    if (pSchedNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetSchedNode () "
                      "Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    *pu4RetValFsQoSSchedulerShapeId = pSchedNode->u4ShapeId;

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFsQoSSchedulerHierarchyLevel
 Input       :  The Indices
                IfIndex
                FsQoSSchedulerId

                The Object 
                retValFsQoSSchedulerHierarchyLevel
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsQoSSchedulerHierarchyLevel (INT4 i4IfIndex, UINT4 u4FsQoSSchedulerId,
                                    UINT4
                                    *pu4RetValFsQoSSchedulerHierarchyLevel)
{
    tQoSSchedNode      *pSchedNode = NULL;

    pSchedNode = QoSUtlGetSchedNode (i4IfIndex, u4FsQoSSchedulerId);

    if (pSchedNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetSchedNode () "
                      "Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    *pu4RetValFsQoSSchedulerHierarchyLevel = (UINT4)
        pSchedNode->u1HierarchyLevel;

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFsQoSSchedulerGlobalId
 Input       :  The Indices
                IfIndex
                FsQoSSchedulerId

                The Object 
                retValFsQoSSchedulerGlobalId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsQoSSchedulerGlobalId (INT4 i4IfIndex, UINT4 u4FsQoSSchedulerId,
                              UINT4 *pu4RetValFsQoSSchedulerGlobalId)
{
    tQoSSchedNode      *pSchedNode = NULL;

    pSchedNode = QoSUtlGetSchedNode (i4IfIndex, u4FsQoSSchedulerId);

    if (pSchedNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetSchedNode () "
                      "Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    *pu4RetValFsQoSSchedulerGlobalId = (UINT4) pSchedNode->u4GlobalId;

    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhGetFsQoSSchedulerStatus
 Input       :  The Indices
                IfIndex
                FsQoSSchedulerId

                The Object 
                retValFsQoSSchedulerStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsQoSSchedulerStatus (INT4 i4IfIndex, UINT4 u4FsQoSSchedulerId,
                            INT4 *pi4RetValFsQoSSchedulerStatus)
{
    tQoSSchedNode      *pSchedNode = NULL;

    pSchedNode = QoSUtlGetSchedNode (i4IfIndex, u4FsQoSSchedulerId);

    if (pSchedNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetSchedNode () "
                      "Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    *pi4RetValFsQoSSchedulerStatus = (INT4) pSchedNode->u1Status;

    return (SNMP_SUCCESS);
}

/* Low Level SET Routine for All Objects  */
/****************************************************************************
 Function    :  nmhSetFsQoSSchedulerSchedAlgorithm
 Input       :  The Indices
                IfIndex
                FsQoSSchedulerId

                The Object 
                setValFsQoSSchedulerSchedAlgorithm
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsQoSSchedulerSchedAlgorithm (INT4 i4IfIndex, UINT4 u4FsQoSSchedulerId,
                                    INT4 i4SetValFsQoSSchedulerSchedAlgorithm)
{
    tQoSSchedNode      *pSchedNode = NULL;
    UINT4               u4CurrVal = 0;
    INT4                i4RetStatus = QOS_FAILURE;

    pSchedNode = QoSUtlGetSchedNode (i4IfIndex, u4FsQoSSchedulerId);

    if (pSchedNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetSchedNode () "
                      "Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    u4CurrVal = (UINT4) pSchedNode->u1SchedAlgorithm;
    pSchedNode->u1SchedAlgorithm = (UINT1) i4SetValFsQoSSchedulerSchedAlgorithm;
    /* if the RowStatus is ACTIVE then we have to Modify the Existing Scheduler
     * otherwise it will be added during QMap Active */
    if ((pSchedNode->u1Status == ACTIVE) &&
        (gQoSGlobalInfo.eSysStatus == QOS_SYS_STATUS_ENABLE))
    {
        i4RetStatus = QoSHwWrSchedulerUpdateParams (pSchedNode->i4IfIndex,
                                                    pSchedNode->u4Id,
                                                    QOS_SCHED_UPDATE_ALGO);
        if (i4RetStatus == QOS_FAILURE)
        {
            QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSHwWrSchedulerUpdateParams () "
                          "Returns FAILURE. \r\n", __FUNCTION__);

            pSchedNode->u1SchedAlgorithm = (UINT1) u4CurrVal;

            return (SNMP_FAILURE);
        }
    }

    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhSetFsQoSSchedulerShapeId
 Input       :  The Indices
                IfIndex
                FsQoSSchedulerId

                The Object 
                setValFsQoSSchedulerShapeId
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsQoSSchedulerShapeId (INT4 i4IfIndex, UINT4 u4FsQoSSchedulerId,
                             UINT4 u4SetValFsQoSSchedulerShapeId)
{
    tQoSSchedNode      *pSchedNode = NULL;
    tQoSShapeTemplateNode *pShapeNode = NULL;
    tQoSShapeTemplateNode *pTmpShapeNode = NULL;
    UINT4               u4CurrVal = 0;
    INT4                i4RetStatus = QOS_FAILURE;

    pSchedNode = QoSUtlGetSchedNode (i4IfIndex, u4FsQoSSchedulerId);
    if (pSchedNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetSchedNode () "
                      "Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    /* If the Same value is configured again no need to incrment the 
     * RefCount and Set */
    if (pSchedNode->u4ShapeId == u4SetValFsQoSSchedulerShapeId)
    {
        return (SNMP_SUCCESS);
    }

    u4CurrVal = pSchedNode->u4ShapeId;

    pSchedNode->u4ShapeId = u4SetValFsQoSSchedulerShapeId;
    /* if the RowStatus is ACTIVE then we have to Modify the Existing Scheduler
     * otherwise it will be added during QMap Active */
    if ((pSchedNode->u1Status == ACTIVE) &&
        (gQoSGlobalInfo.eSysStatus == QOS_SYS_STATUS_ENABLE))
    {
        i4RetStatus = QoSHwWrSchedulerUpdateParams (pSchedNode->i4IfIndex,
                                                    pSchedNode->u4Id,
                                                    QOS_SCHED_UPDATE_SHAPE);

        if (i4RetStatus == QOS_FAILURE)
        {
            QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSHwWrSchedulerUpdateParams () "
                          "Returns FAILURE. \r\n", __FUNCTION__);

            pSchedNode->u4ShapeId = u4CurrVal;

            return (SNMP_FAILURE);
        }
    }

    /* Store the old value ,Reset corresponding ref count if the id not '0' */
    if (u4CurrVal != 0)
    {
        pTmpShapeNode = QoSUtlGetShapeTempNode (u4CurrVal);
        if (pTmpShapeNode == NULL)
        {
            QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetShapeTempNode () "
                          "Returns FAILURE. \r\n", __FUNCTION__);

            return (SNMP_FAILURE);
        }
        pTmpShapeNode->u4RefCount = (pTmpShapeNode->u4RefCount) - 1;
    }

    if (u4SetValFsQoSSchedulerShapeId != 0)
    {
        pShapeNode = QoSUtlGetShapeTempNode (u4SetValFsQoSSchedulerShapeId);
        if (pShapeNode == NULL)
        {
            QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetShapeTempNode () "
                          "Returns FAILURE. \r\n", __FUNCTION__);
            return (SNMP_FAILURE);
        }

        /* Incremnet the RefCount of ShapeId  Entry in the ShapeTempTable */
        pShapeNode->u4RefCount = (pShapeNode->u4RefCount) + 1;
    }

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhSetFsQoSSchedulerHierarchyLevel
 Input       :  The Indices
                IfIndex
                FsQoSSchedulerId

                The Object 
                setValFsQoSSchedulerHierarchyLevel
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsQoSSchedulerHierarchyLevel (INT4 i4IfIndex, UINT4 u4FsQoSSchedulerId,
                                    UINT4 u4SetValFsQoSSchedulerHierarchyLevel)
{
    tQoSSchedNode      *pSchedNode = NULL;
    UINT1               u1CurrVal = 0;
    INT4                i4RetStatus = QOS_FAILURE;

    pSchedNode = QoSUtlGetSchedNode (i4IfIndex, u4FsQoSSchedulerId);
    if (pSchedNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetSchedNode () "
                      "Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    u1CurrVal = pSchedNode->u1HierarchyLevel;
    pSchedNode->u1HierarchyLevel = (UINT1) u4SetValFsQoSSchedulerHierarchyLevel;
    /* if the RowStatus is ACTIVE then we have to Modify the Existing Scheduler
     * otherwise it will be added during QMap Active */
    if ((pSchedNode->u1Status == ACTIVE) &&
        (gQoSGlobalInfo.eSysStatus == QOS_SYS_STATUS_ENABLE))
    {
        i4RetStatus = QoSHwWrSchedulerUpdateParams (pSchedNode->i4IfIndex,
                                                    pSchedNode->u4Id,
                                                    QOS_SCHED_UPDATE_HL);
        if (i4RetStatus == QOS_FAILURE)
        {
            QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSHwWrSchedulerUpdateParams () "
                          "Returns FAILURE. \r\n", __FUNCTION__);

            pSchedNode->u1HierarchyLevel = u1CurrVal;

            return (SNMP_FAILURE);
        }
    }

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhSetFsQoSSchedulerGlobalId
 Input       :  The Indices
                IfIndex
                FsQoSSchedulerId

                The Object 
                setValFsQoSSchedulerGlobalId
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsQoSSchedulerGlobalId (INT4 i4IfIndex, UINT4 u4FsQoSSchedulerId,
                              UINT4 u4SetValFsQoSSchedulerGlobalId)
{
    tQoSSchedNode      *pSchedNode = NULL;

    pSchedNode = QoSUtlGetSchedNode (i4IfIndex, u4FsQoSSchedulerId);
    if (pSchedNode == NULL)
    {
        return SNMP_FAILURE;
    }

    pSchedNode->u4GlobalId = u4SetValFsQoSSchedulerGlobalId;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsQoSSchedulerStatus
 Input       :  The Indices
                IfIndex
                FsQoSSchedulerId

                The Object 
                setValFsQoSSchedulerStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsQoSSchedulerStatus (INT4 i4IfIndex, UINT4 u4FsQoSSchedulerId,
                            INT4 i4SetValFsQoSSchedulerStatus)
{
    tQoSSchedNode      *pSchedNode = NULL;
    tQoSSchedNode      *pSchedNodeNxt = NULL;
    INT4                i4RetStatus = QOS_FAILURE;
    INT4                iReturnValue = SNMP_SUCCESS;
    INT4                i4NextIfIndex = 0;
    INT4                i4CurrIfIndex = 0;
    UINT4               u4NextSchedId = 0;
    UINT4               u4CurrSchedId = 0;
    UINT4               u4FsQoSQId = 0;
    tQoSQNode          *pQNode = NULL;

    pSchedNode = QoSUtlGetSchedNode (i4IfIndex, u4FsQoSSchedulerId);
    if (pSchedNode != NULL)
    {
        /* Entry found in the Table */
        if (pSchedNode->u1Status == i4SetValFsQoSSchedulerStatus)
        {
            /* same value */
            return (SNMP_SUCCESS);
        }
    }
    else
    {
        /* New Entry need to be created in the Table, only if 
         * i4SetValFsQoSSchedulerStatus = CREATE_AND_WAIT */
        /* Optional */
        if (i4SetValFsQoSSchedulerStatus == DESTROY)
        {
            return (SNMP_SUCCESS);
        }
        else if (i4SetValFsQoSSchedulerStatus != QOS_CREATE_AND_WAIT)
        {
            return (SNMP_FAILURE);
        }
    }

    switch (i4SetValFsQoSSchedulerStatus)
    {
            /* For a New Entry */
        case CREATE_AND_WAIT:

            pSchedNode = QoSCreateSchedTblEntry (i4IfIndex, u4FsQoSSchedulerId);
            if (pSchedNode == NULL)
            {
                QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSCreateSchedTblEntry ()"
                              " Returns FAILURE. \r\n", __FUNCTION__);

                return (SNMP_FAILURE);
            }

            pSchedNode->u1Status = NOT_IN_SERVICE;

            break;

            /* Modify an Entry */
        case ACTIVE:

            /* 1. Add the entry into the HARDWARE. */
            if (gQoSGlobalInfo.eSysStatus == QOS_SYS_STATUS_ENABLE)
            {
                /* If Hierarchical-scheduling is not supported, only one (port) scheduler
                 * can be active at a time.*/
                if (QOS_HL_DEF_SCHED_CFG_SUPPORTED != gu1HLSupportFlag)
                {
                    i4CurrIfIndex = pSchedNode->i4IfIndex;
                    while (QoSGetNextSchedTblEntryIndex
                           (i4CurrIfIndex, &i4NextIfIndex, u4CurrSchedId,
                            &u4NextSchedId) == QOS_SUCCESS)
                    {
                        if (i4NextIfIndex != pSchedNode->i4IfIndex)
                        {
                            break;
                        }
                        if (u4NextSchedId == pSchedNode->u4Id)
                        {
                            u4CurrSchedId = u4NextSchedId;
                            continue;
                        }
                        pSchedNodeNxt =
                            QoSUtlGetSchedNode (i4NextIfIndex, u4NextSchedId);
                        if (pSchedNodeNxt != NULL)
                        {
                            /* Entry found in the Table */
                            if (pSchedNodeNxt->u1Status == ACTIVE)
                            {
                                i4RetStatus =
                                    QoSHwWrSchedulerDelete (i4NextIfIndex,
                                                            u4NextSchedId);
                                if (i4RetStatus == QOS_FAILURE)
                                {
                                    QOS_TRC_ARG1 (MGMT_TRC,
                                                  "In %s : QoSHwWrSchedulerDelete"
                                                  " () Returns FAILURE. \r\n",
                                                  __FUNCTION__);
                                    return (SNMP_FAILURE);
                                }
                                pSchedNodeNxt->u1Status = NOT_IN_SERVICE;
                            }
                        }
                        u4CurrSchedId = u4NextSchedId;
                    }
                }
                i4RetStatus = QoSHwWrSchedulerAdd (pSchedNode->i4IfIndex,
                                                   pSchedNode->u4Id);
                if ((i4RetStatus == QOS_FAILURE)
                    || (i4RetStatus == QOS_NP_NOT_SUPPORTED))
                {
                    if (i4RetStatus == QOS_NP_NOT_SUPPORTED)
                    {
                        CLI_SET_ERR (QOS_CLI_ERR_UNSUPPORTED_BY_HW);
                    }
                    QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSHwWrSchedulerAdd ()"
                                  " Returns FAILURE.\r\n", __FUNCTION__);
                    return (SNMP_FAILURE);
                }

                if (QOS_HL_DEF_SCHED_CFG_SUPPORTED != gu1HLSupportFlag)
                {
                    for (u4FsQoSQId = 1; u4FsQoSQId <= VLAN_DEV_MAX_NUM_COSQ;
                         u4FsQoSQId++)
                    {
                        pQNode =
                            QoSUtlGetQNode (pSchedNode->i4IfIndex, u4FsQoSQId);
                        if (pQNode != NULL)
                        {
                            if (pQNode->u4SchedulerId != pSchedNode->u4Id)
                            {
                                pQNode->u4SchedulerId = pSchedNode->u4Id;
                                pSchedNode->u4RefCount =
                                    (pSchedNode->u4RefCount) + 1;
                            }
                        }
                    }
                }
            }

            pSchedNode->u1Status = ACTIVE;

            break;

        case NOT_IN_SERVICE:

            /* 1. Remove the entry from the HARDWARE. */
            if ((pSchedNode->u1Status == ACTIVE) &&
                (gQoSGlobalInfo.eSysStatus == QOS_SYS_STATUS_ENABLE))
            {
                i4RetStatus = QoSHwWrSchedulerDelete (pSchedNode->i4IfIndex,
                                                      pSchedNode->u4Id);
                if (i4RetStatus == QOS_FAILURE)
                {
                    QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSHwWrSchedulerDelete"
                                  " () Returns FAILURE. \r\n", __FUNCTION__);
                    return (SNMP_FAILURE);
                }
            }

            pSchedNode->u1Status = NOT_IN_SERVICE;

            break;

        case DESTROY:

            /* 1. Remove the entry from the HARDWARE. */
            if ((pSchedNode->u1Status == ACTIVE) &&
                (gQoSGlobalInfo.eSysStatus == QOS_SYS_STATUS_ENABLE))
            {
                i4RetStatus = QoSHwWrSchedulerDelete (pSchedNode->i4IfIndex,
                                                      pSchedNode->u4Id);
                if (i4RetStatus == QOS_FAILURE)
                {
                    QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSHwWrSchedulerDelete"
                                  " () Returns FAILURE. \r\n", __FUNCTION__);
                    return (SNMP_FAILURE);
                }
                else if (i4RetStatus == QOS_NP_NOT_SUPPORTED)
                {
                    CLI_SET_ERR (QOS_CLI_ERR_UNSUPPORTED_BY_HW);
                    QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSHwWrSchedulerDelete"
                                  " () Returns NOT SUPPORTED. \r\n",
                                  __FUNCTION__);
                    return (SNMP_FAILURE);
                }

            }

            /* Deletion of QNode Schedular Entry and Restoring with Default
               entry in QNode Table for given index */
            i4RetStatus = QoSHwWrSchedulerAdd (pSchedNode->i4IfIndex,
                                               QOS_DEF_SCHED_ID);
            if ((i4RetStatus == QOS_FAILURE)
                || (i4RetStatus == QOS_NP_NOT_SUPPORTED))
            {
                if (i4RetStatus == QOS_NP_NOT_SUPPORTED)
                {
                    CLI_SET_ERR (QOS_CLI_ERR_UNSUPPORTED_BY_HW);
                }
                QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSHwWrSchedulerAdd ()"
                              " Returns FAILURE.\r\n", __FUNCTION__);
                return (SNMP_FAILURE);
            }
            if (QOS_HL_DEF_SCHED_CFG_SUPPORTED != gu1HLSupportFlag)
            {
                for (u4FsQoSQId = 1; u4FsQoSQId <= VLAN_DEV_MAX_NUM_COSQ;
                     u4FsQoSQId++)
                {
                    pQNode = QoSUtlGetQNode (pSchedNode->i4IfIndex, u4FsQoSQId);
                    if (pQNode != NULL)
                    {
                        pQNode->u4SchedulerId = QOS_DEF_SCHED_ID;
                    }
                }
            }

            /* 2. Remove the entry from the SOFTWARE. */
            i4RetStatus = QoSDeleteSchedTblEntry (pSchedNode);
            if (i4RetStatus == QOS_FAILURE)
            {
                QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSDeleteSchedTblEntry ()"
                              " Returns FAILURE. \r\n", __FUNCTION__);

                return (SNMP_FAILURE);
            }

            break;

        case CREATE_AND_GO:
            iReturnValue = SNMP_FAILURE;
            break;

        default:
            iReturnValue = SNMP_FAILURE;
            break;
    }

    return ((INT1) iReturnValue);
}

/* Low Level TEST Routines for All Objects  */
/****************************************************************************
 Function    :  nmhTestv2FsQoSSchedulerSchedAlgorithm
 Input       :  The Indices
                IfIndex
                FsQoSSchedulerId

                The Object 
                testValFsQoSSchedulerSchedAlgorithm
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsQoSSchedulerSchedAlgorithm (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                                       UINT4 u4FsQoSSchedulerId,
                                       INT4
                                       i4TestValFsQoSSchedulerSchedAlgorithm)
{
    INT4                i4RetStatus = QOS_FAILURE;

    *pu4ErrorCode = SNMP_ERR_NO_ERROR;

    /* Check Index */
    i4RetStatus = QoSUtlValidateSchedTblIdxInst (i4IfIndex, u4FsQoSSchedulerId,
                                                 pu4ErrorCode);
    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlValidateSchedTblIdxInst ()"
                      " Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    /* Check  Object's Value */
    if ((i4TestValFsQoSSchedulerSchedAlgorithm < QOS_SCHED_ALGO_MIN_VAL) ||
        (i4TestValFsQoSSchedulerSchedAlgorithm > QOS_SCHED_ALGO_MAX_VAL))
    {
        QOS_TRC_ARG3 (MGMT_TRC, "%s : Sched Algo value is out of range. The"
                      " range should be Min %d - Max %d. \r\n", __FUNCTION__,
                      QOS_SCHED_ALGO_MIN_VAL, QOS_SCHED_ALGO_MAX_VAL);

        CLI_SET_ERR (QOS_CLI_ERR_SCHED_INVALID_ALGO);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;

        return (SNMP_FAILURE);
    }

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhTestv2FsQoSSchedulerShapeId
 Input       :  The Indices
                IfIndex
                FsQoSSchedulerId

                The Object 
                testValFsQoSSchedulerShapeId
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsQoSSchedulerShapeId (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                                UINT4 u4FsQoSSchedulerId,
                                UINT4 u4TestValFsQoSSchedulerShapeId)
{
    INT4                i4RetStatus = QOS_FAILURE;

    *pu4ErrorCode = SNMP_ERR_NO_ERROR;

    /* Check Index */
    i4RetStatus = QoSUtlValidateSchedTblIdxInst (i4IfIndex, u4FsQoSSchedulerId,
                                                 pu4ErrorCode);
    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlValidateSchedTblIdxInst ()"
                      " Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    if (u4TestValFsQoSSchedulerShapeId == 0)
    {
        /* Reset To NULL */
        return (SNMP_SUCCESS);
    }

    /* Check  Object's Value */
    i4RetStatus = QoSUtlValidateShapeId (u4TestValFsQoSSchedulerShapeId,
                                         pu4ErrorCode);
    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlValidateShapeId ()"
                      " Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }
    /* More than one scheduler(In addition to Default schedular) is not supported */
    if (ISS_HW_SUPPORTED !=
        IssGetHwCapabilities (ISS_HW_MORE_SCHEDULER_SUPPORT))
    {
        if ((u4FsQoSSchedulerId == 1) && (u4TestValFsQoSSchedulerShapeId != 0))
        {
            QOS_TRC_ARG1 (MGMT_TRC,
                          "%s : Default scheduler cannot be modified. \r\n",
                          __FUNCTION__);
            CLI_SET_ERR (QOS_CLI_ERR_DEF_SCHED_MODIFY);
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return (SNMP_FAILURE);
        }
    }
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhTestv2FsQoSSchedulerHierarchyLevel
 Input       :  The Indices
                IfIndex
                FsQoSSchedulerId

                The Object 
                testValFsQoSSchedulerHierarchyLevel
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsQoSSchedulerHierarchyLevel (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                                       UINT4 u4FsQoSSchedulerId,
                                       UINT4
                                       u4TestValFsQoSSchedulerHierarchyLevel)
{
    tQoSSchedNode      *pSchedNode = NULL;
    INT4                i4RetStatus = QOS_FAILURE;

    *pu4ErrorCode = SNMP_ERR_NO_ERROR;

    /* Check Index */
    i4RetStatus = QoSUtlValidateSchedTblIdxInst (i4IfIndex, u4FsQoSSchedulerId,
                                                 pu4ErrorCode);
    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlValidateSchedTblIdxInst ()"
                      " Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    /* Check  Object's Value */
    if (u4TestValFsQoSSchedulerHierarchyLevel > QOS_SCHED_HL_MAX_VAL)
    {
        QOS_TRC_ARG3 (MGMT_TRC,
                      "%s : Sched Hierarchy Level is out of range. The"
                      " range should be Min %d - Max %d. \r\n", __FUNCTION__,
                      QOS_SCHED_HL_MIN_VAL, QOS_SCHED_HL_MAX_VAL);

        CLI_SET_ERR (QOS_CLI_ERR_HL_INVALID);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }

    pSchedNode = QoSUtlGetSchedNode (i4IfIndex, u4FsQoSSchedulerId);
    if (pSchedNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetSchedNode () "
                      "Returns FAILURE. \r\n", __FUNCTION__);

        CLI_SET_ERR (QOS_CLI_ERR_SCHED_NO_ENTRY);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }
    if (pSchedNode->u1Status == ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (QOS_CLI_ERR_STATUS_ACTIVE);
        return (SNMP_FAILURE);
    }
    if (QOS_HL_DEF_SCHED_CFG_SUPPORTED == gu1HLSupportFlag)
    {
        if (pSchedNode->u1HierarchyLevel !=
            u4TestValFsQoSSchedulerHierarchyLevel)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            CLI_SET_ERR (QOS_CLI_ERR_INVALID_HL_VALUE);
            return (SNMP_FAILURE);
        }
    }
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhTestv2FsQoSSchedulerGlobalId
 Input       :  The Indices
                IfIndex
                FsQoSSchedulerId

                The Object 
                testValFsQoSSchedulerGlobalId
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsQoSSchedulerGlobalId (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                                 UINT4 u4FsQoSSchedulerId,
                                 UINT4 u4TestValFsQoSSchedulerGlobalId)
{
    INT4                i4RetStatus = QOS_FAILURE;

    *pu4ErrorCode = SNMP_ERR_NO_ERROR;

    /* Check Index */
    i4RetStatus = QoSUtlValidateSchedTblIdxInst (i4IfIndex, u4FsQoSSchedulerId,
                                                 pu4ErrorCode);
    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlValidateSchedTblIdxInst ()"
                      " Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    if (u4TestValFsQoSSchedulerGlobalId > QOS_SCHED_TBL_MAX_INDEX_RANGE)
    {

        QOS_TRC_ARG2 (MGMT_TRC, "%s Scheduler"
                      " range should be Max %d. \r\n", __FUNCTION__,
                      QOS_SCHED_TBL_MAX_INDEX_RANGE);

        CLI_SET_ERR (QOS_CLI_ERR_HL_INVALID);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsQoSSchedulerStatus
 Input       :  The Indices
                IfIndex
                FsQoSSchedulerId

                The Object 
                testValFsQoSSchedulerStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsQoSSchedulerStatus (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                               UINT4 u4FsQoSSchedulerId,
                               INT4 i4TestValFsQoSSchedulerStatus)
{
    tQoSSchedNode      *pSchedNode = NULL;
    INT4                i4RetStatus = QOS_FAILURE;
    UINT4               u4Count = 0;
    UINT4               u4RefCount = 0;
    UINT4               u4FsQoSQId = 0;
    INT4                i4ReturnValue = SNMP_SUCCESS;
    tQoSQNode          *pQNode = NULL;
#ifdef NPAPI_WANTED
    tQoSSchedNode       SchedNode;
    UINT1               u1SchedCount = 0;
    MEMSET (&SchedNode, 0, sizeof (tQoSSchedNode));
    SchedNode.i4IfIndex = i4IfIndex;
    SchedNode.u4Id = 0;
#endif

    if (gQoSGlobalInfo.eSysControl != QOS_SYS_CNTL_START)
    {
        CLI_SET_ERR (QOS_CLI_ERR_MODULE_SHUTDOW);
        QOS_TRC (MGMT_TRC, QOS_MODULE_SHUTDOW_ERROR_MSG);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return (SNMP_FAILURE);
    }

    *pu4ErrorCode = SNMP_ERR_NO_ERROR;

    /* Check the Range of Index Values (Min-Max) */
    i4RetStatus = QoSUtlValidateIfIndex (i4IfIndex, pu4ErrorCode);
    if (i4RetStatus == QOS_FAILURE)
    {
        /* IfIndex of 0 in scheduler Implies All Ports */
        if (i4IfIndex != 0)
        {
            return (SNMP_FAILURE);
        }
    }

    /* Check the Range of Index Values (Min-Max) */
    i4RetStatus = QOS_CHECK_SCHED_TBL_INDEX_RANGE (u4FsQoSSchedulerId);
    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG3 (MGMT_TRC, "%s : Scheduler Id %d is out of Range."
                      " The Range Should be (1-%d). \r\n", __FUNCTION__,
                      u4FsQoSSchedulerId, QOS_SCHED_TBL_MAX_INDEX_RANGE);
        CLI_SET_ERR (QOS_CLI_ERR_SCHED_RANGE);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;

        return (SNMP_FAILURE);
    }

    /* When HwQueueConfigOnLaPortSup is not supported,
     * Block Sceduler and  Queue Configuration on a port which is member of port-channel port
     */
    if (ISS_HW_SUPPORTED !=
        IssGetHwCapabilities (ISS_HW_QUEUE_CONFIG_ON_LA_PORT))
    {
        if (L2IwfIsPortInPortChannel ((UINT4) i4IfIndex) == L2IWF_SUCCESS)
        {
            CLI_SET_ERR (QOS_CLI_ERR_PORTCHANNEL);
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return (SNMP_FAILURE);
        }
    }
    pSchedNode = QoSUtlGetSchedNode (i4IfIndex, u4FsQoSSchedulerId);
    if (pSchedNode == NULL)
    {
        /* Entry Not Found In The Table */
        switch (i4TestValFsQoSSchedulerStatus)
        {
            case ACTIVE:
            case NOT_IN_SERVICE:
            case CREATE_AND_GO:
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                i4ReturnValue = SNMP_FAILURE;
                break;

            case CREATE_AND_WAIT:
                /* Check the No of Entries in the Table */
                i4RetStatus =
                    RBTreeCount (gQoSGlobalInfo.pRbSchedTbl, &u4Count);

                if ((i4RetStatus == RB_FAILURE) ||
                    (u4Count > QOS_SCHED_TBL_MAX_ENTRIES))
                {
                    QOS_TRC_ARG2 (MGMT_TRC, "%s : Max No of Scheduler "
                                  "Table Entries are Configured. No of Max "
                                  "Entries %d. \r\n", __FUNCTION__,
                                  QOS_SCHED_TBL_MAX_ENTRIES);

                    CLI_SET_ERR (QOS_CLI_ERR_SCHED_MAX_ENTRY);
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;

                    return (SNMP_FAILURE);
                }

                if (QOS_HL_DEF_SCHED_CFG_SUPPORTED != gu1HLSupportFlag)
                {
                    if (u4FsQoSSchedulerId > (QOS_HL_DEF_NUM_S1_SCHEDULERS +
                                              QOS_HL_DEF_NUM_S2_SCHEDULERS +
                                              QOS_HL_DEF_NUM_S3_SCHEDULERS))
                    {
                        CLI_SET_ERR (QOS_CLI_ERR_UNSUPPORTED_BY_HW);
                        return (SNMP_FAILURE);
                    }
                }
#ifdef NPAPI_WANTED
                /* More than one scheduler(In addition to Default schedular) is not supported */
                if (ISS_HW_SUPPORTED !=
                    IssGetHwCapabilities (ISS_HW_MORE_SCHEDULER_SUPPORT))
                {
                    while ((pSchedNode =
                            (tQoSSchedNode *) RBTreeGetNext (gQoSGlobalInfo.
                                                             pRbSchedTbl,
                                                             &SchedNode,
                                                             QoSSchedCmpFun)) !=
                           NULL)
                    {
                        if (pSchedNode->i4IfIndex != SchedNode.i4IfIndex)
                        {
                            break;
                        }
                        u1SchedCount++;

                        if (u1SchedCount == QOS_MAX_SCHED_PER_PORT)
                        {
                            QOS_TRC_ARG2 (MGMT_TRC, "%s : Max No of Scheduler "
                                          "Entries per port are over. Max "
                                          "Entries %d. \r\n", __FUNCTION__,
                                          QOS_MAX_SCHED_PER_PORT);

                            CLI_SET_ERR (QOS_CLI_ERR_MAX_SCHED_PER_PORT);
                            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;

                            return (SNMP_FAILURE);
                        }
                        SchedNode.u4Id = pSchedNode->u4Id;
                    }
                    pSchedNode = NULL;
                }
                else
                {
                    UNUSED_PARAM (SchedNode);
                    UNUSED_PARAM (u1SchedCount);
                }
#endif
                break;

            case DESTROY:
                CLI_SET_ERR (QOS_CLI_ERR_SCHED_NO_ENTRY);
                *pu4ErrorCode = SNMP_ERR_NO_CREATION;
                i4ReturnValue = SNMP_FAILURE;
                break;

            default:
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                i4ReturnValue = SNMP_FAILURE;
                break;
        }
    }
    else
    {
        /* Entry Found In The Table */
        switch (i4TestValFsQoSSchedulerStatus)
        {
            case ACTIVE:

                if ((pSchedNode->u1Status == NOT_READY))
                {
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                    return (SNMP_FAILURE);
                }

                break;

            case NOT_IN_SERVICE:

                if ((pSchedNode->u1Status == NOT_READY))
                {
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                    return (SNMP_FAILURE);
                }

                i4RetStatus = QOS_CHECK_TABLE_REF_COUNT (pSchedNode);
                if (i4RetStatus == QOS_FAILURE)
                {
                    QOS_TRC_ARG1 (MGMT_TRC, "In %s : ERROR - This Entry is "
                                  "referenced by Q Table Entry or hierarchy "
                                  "Scheduler Table Entry.\r\n", __FUNCTION__);

                    CLI_SET_ERR (QOS_CLI_ERR_SCHED_RFE);
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;

                    return (SNMP_FAILURE);
                }
                break;

            case DESTROY:
                /* Check(Test) is it Default Entry. */
                i4RetStatus = QoSIsDefSchedTblEntry (pSchedNode);
                if (i4RetStatus == QOS_SUCCESS)
                {
                    QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSIsSchedTblDefEntry  "
                                  "Returns FAILURE.\r\n", __FUNCTION__);
                    CLI_SET_ERR (QOS_CLI_ERR_DEF_ENTRY);
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                    return (SNMP_FAILURE);
                }
                /* Check for default scheduler ID , algorithm ,shaper ID and Hierarchy level */
                if (((pSchedNode->u4Id == QOS_HL_DEF_S1_SCHEDULER_ID)
                     && (QOS_S1_SCHEDULER == pSchedNode->u1HierarchyLevel))
                    || ((pSchedNode->u4Id == QOS_HL_DEF_S2_SCHEDULER_ID)
                        && (QOS_S2_SCHEDULER == pSchedNode->u1HierarchyLevel))
                    ||
                    (((pSchedNode->u4Id == QOS_HL_DEF_S3_SCHEDULER1_ID)
                      || (pSchedNode->u4Id == QOS_HL_DEF_S3_SCHEDULER2_ID))
                     && (QOS_S3_SCHEDULER == pSchedNode->u1HierarchyLevel)))
                {
                    CLI_SET_ERR (QOS_CLI_ERR_DEF_ENTRY);
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                    return (SNMP_FAILURE);
                }

                /* Checks for Hierarchy level Scheduler, if not then the Scheduler Node Ref Count
                   is compared to the QNode Scheduler Entries count.If both count matches then deletion
                   of Scheduler from Queue Node is Performed */

                if (QOS_HL_DEF_SCHED_CFG_SUPPORTED != gu1HLSupportFlag)
                {
                    for (u4FsQoSQId = 1; u4FsQoSQId <= VLAN_DEV_MAX_NUM_COSQ;
                         u4FsQoSQId++)
                    {
                        pQNode =
                            QoSUtlGetQNode (pSchedNode->i4IfIndex, u4FsQoSQId);
                        if (pQNode != NULL)
                        {
                            if (pQNode->u4SchedulerId == pSchedNode->u4Id)
                            {
                                u4RefCount++;
                            }
                        }
                    }
                }

                if (u4RefCount == pSchedNode->u4RefCount)
                {
                    i4ReturnValue = SNMP_SUCCESS;
                }
                else
                {
                    if (i4RetStatus == QOS_FAILURE)
                    {
                        QOS_TRC_ARG1 (MGMT_TRC, "In %s : ERROR - This Entry is "
                                      "referenced by Q Table Entry or hierarchy "
                                      "Scheduler Table Entry.\r\n",
                                      __FUNCTION__);

                        CLI_SET_ERR (QOS_CLI_ERR_SCHED_RFE);
                        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;

                        return (SNMP_FAILURE);
                    }
                }
                break;

            case CREATE_AND_WAIT:
            case CREATE_AND_GO:
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                i4ReturnValue = SNMP_FAILURE;
                break;

            default:
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                i4ReturnValue = SNMP_FAILURE;
                break;

        }                        /* End of switch */

    }                            /* End of if */

    return ((INT1) i4ReturnValue);

}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsQoSSchedulerTable
 Input       :  The Indices
                IfIndex
                FsQoSSchedulerId
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsQoSSchedulerTable (UINT4 *pu4ErrorCode,
                             tSnmpIndexList * pSnmpIndexList,
                             tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsQoSHierarchyTable. */
/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsQoSHierarchyTable
 Input       :  The Indices
                IfIndex
                FsQoSHierarchyLevel
                FsQoSSchedulerId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */
INT1
nmhValidateIndexInstanceFsQoSHierarchyTable (INT4 i4IfIndex,
                                             UINT4 u4FsQoSHierarchyLevel,
                                             UINT4 u4FsQoSSchedulerId)
{
    tQoSHierarchyNode  *pHierarchyNode = NULL;

    if (gQoSGlobalInfo.eSysControl != QOS_SYS_CNTL_START)
    {
        QOS_TRC (MGMT_TRC, QOS_MODULE_SHUTDOW_ERROR_MSG);
        return (SNMP_FAILURE);
    }

    pHierarchyNode = QoSUtlGetHierarchyNode (i4IfIndex, u4FsQoSHierarchyLevel,
                                             u4FsQoSSchedulerId);
    if (pHierarchyNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetHierarchyNode () Returns "
                      "FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsQoSHierarchyTable
 Input       :  The Indices
                IfIndex
                FsQoSHierarchyLevel
                FsQoSSchedulerId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */
INT1
nmhGetFirstIndexFsQoSHierarchyTable (INT4 *pi4IfIndex,
                                     UINT4 *pu4FsQoSHierarchyLevel,
                                     UINT4 *pu4FsQoSSchedulerId)
{
    INT4                i4RetStatus = QOS_FAILURE;

    if (gQoSGlobalInfo.eSysControl != QOS_SYS_CNTL_START)
    {
        QOS_TRC (MGMT_TRC, QOS_MODULE_SHUTDOW_ERROR_MSG);
        return (SNMP_FAILURE);
    }

    i4RetStatus = QoSGetNextHierarchyTblEntryIndex (0, pi4IfIndex,
                                                    0, pu4FsQoSHierarchyLevel,
                                                    0, pu4FsQoSSchedulerId);
    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSGetNextHierarchyTblEntryIndex () "
                      "Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsQoSHierarchyTable
 Input       :  The Indices
                IfIndex
                nextIfIndex
                FsQoSHierarchyLevel
                nextFsQoSHierarchyLevel
                FsQoSSchedulerId
                nextFsQoSSchedulerId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsQoSHierarchyTable (INT4 i4IfIndex,
                                    INT4 *pi4NextIfIndex,
                                    UINT4 u4FsQoSHierarchyLevel,
                                    UINT4 *pu4NextFsQoSHierarchyLevel,
                                    UINT4 u4FsQoSSchedulerId,
                                    UINT4 *pu4NextFsQoSSchedulerId)
{
    INT4                i4RetStatus = QOS_FAILURE;

    if (gQoSGlobalInfo.eSysControl != QOS_SYS_CNTL_START)
    {
        QOS_TRC (MGMT_TRC, QOS_MODULE_SHUTDOW_ERROR_MSG);
        return (SNMP_FAILURE);
    }

    i4RetStatus = QoSGetNextHierarchyTblEntryIndex (i4IfIndex, pi4NextIfIndex,
                                                    u4FsQoSHierarchyLevel,
                                                    pu4NextFsQoSHierarchyLevel,
                                                    u4FsQoSSchedulerId,
                                                    pu4NextFsQoSSchedulerId);
    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSGetNextHierarchyTblEntryIndex () "
                      "Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    return (SNMP_SUCCESS);
}

/* Low Level GET Routine for All Objects  */
/****************************************************************************
 Function    :  nmhGetFsQoSHierarchyQNext
 Input       :  The Indices
                IfIndex
                FsQoSHierarchyLevel
                FsQoSSchedulerId

                The Object 
                retValFsQoSHierarchyQNext
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsQoSHierarchyQNext (INT4 i4IfIndex, UINT4 u4FsQoSHierarchyLevel,
                           UINT4 u4FsQoSSchedulerId,
                           UINT4 *pu4RetValFsQoSHierarchyQNext)
{
    tQoSHierarchyNode  *pHierarchyNode = NULL;

    pHierarchyNode = QoSUtlGetHierarchyNode (i4IfIndex, u4FsQoSHierarchyLevel,
                                             u4FsQoSSchedulerId);
    if (pHierarchyNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetHierarchyNode () "
                      "Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    *pu4RetValFsQoSHierarchyQNext = pHierarchyNode->u4QNext;

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFsQoSHierarchySchedNext
 Input       :  The Indices
                IfIndex
                FsQoSHierarchyLevel
                FsQoSSchedulerId

                The Object 
                retValFsQoSHierarchySchedNext
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsQoSHierarchySchedNext (INT4 i4IfIndex, UINT4 u4FsQoSHierarchyLevel,
                               UINT4 u4FsQoSSchedulerId,
                               UINT4 *pu4RetValFsQoSHierarchySchedNext)
{
    tQoSHierarchyNode  *pHierarchyNode = NULL;

    pHierarchyNode = QoSUtlGetHierarchyNode (i4IfIndex, u4FsQoSHierarchyLevel,
                                             u4FsQoSSchedulerId);
    if (pHierarchyNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetHierarchyNode () "
                      "Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    *pu4RetValFsQoSHierarchySchedNext = pHierarchyNode->u4SchedNext;

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFsQoSHierarchyWeight
 Input       :  The Indices
                IfIndex
                FsQoSHierarchyLevel
                FsQoSSchedulerId

                The Object 
                retValFsQoSHierarchyWeight
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsQoSHierarchyWeight (INT4 i4IfIndex, UINT4 u4FsQoSHierarchyLevel,
                            UINT4 u4FsQoSSchedulerId,
                            UINT4 *pu4RetValFsQoSHierarchyWeight)
{
    tQoSHierarchyNode  *pHierarchyNode = NULL;

    pHierarchyNode = QoSUtlGetHierarchyNode (i4IfIndex, u4FsQoSHierarchyLevel,
                                             u4FsQoSSchedulerId);
    if (pHierarchyNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetHierarchyNode () "
                      "Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    *pu4RetValFsQoSHierarchyWeight = (UINT4) pHierarchyNode->u2Weight;

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFsQoSHierarchyPriority
 Input       :  The Indices
                IfIndex
                FsQoSHierarchyLevel
                FsQoSSchedulerId

                The Object 
                retValFsQoSHierarchyPriority
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsQoSHierarchyPriority (INT4 i4IfIndex, UINT4 u4FsQoSHierarchyLevel,
                              UINT4 u4FsQoSSchedulerId,
                              INT4 *pi4RetValFsQoSHierarchyPriority)
{
    tQoSHierarchyNode  *pHierarchyNode = NULL;

    pHierarchyNode = QoSUtlGetHierarchyNode (i4IfIndex, u4FsQoSHierarchyLevel,
                                             u4FsQoSSchedulerId);
    if (pHierarchyNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetHierarchyNode () "
                      "Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    *pi4RetValFsQoSHierarchyPriority = (INT4) pHierarchyNode->u1Priority;

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFsQoSHierarchyStatus
 Input       :  The Indices
                IfIndex
                FsQoSHierarchyLevel
                FsQoSSchedulerId

                The Object 
                retValFsQoSHierarchyStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsQoSHierarchyStatus (INT4 i4IfIndex, UINT4 u4FsQoSHierarchyLevel,
                            UINT4 u4FsQoSSchedulerId,
                            INT4 *pi4RetValFsQoSHierarchyStatus)
{
    tQoSHierarchyNode  *pHierarchyNode = NULL;

    pHierarchyNode = QoSUtlGetHierarchyNode (i4IfIndex, u4FsQoSHierarchyLevel,
                                             u4FsQoSSchedulerId);
    if (pHierarchyNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetHierarchyNode () "
                      "Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    *pi4RetValFsQoSHierarchyStatus = (INT4) pHierarchyNode->u1Status;

    return (SNMP_SUCCESS);
}

/* Low Level SET Routine for All Objects  */
/****************************************************************************
 Function    :  nmhSetFsQoSHierarchyQNext
 Input       :  The Indices
                IfIndex
                FsQoSHierarchyLevel
                FsQoSSchedulerId

                The Object 
                setValFsQoSHierarchyQNext
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsQoSHierarchyQNext (INT4 i4IfIndex, UINT4 u4FsQoSHierarchyLevel,
                           UINT4 u4FsQoSSchedulerId,
                           UINT4 u4SetValFsQoSHierarchyQNext)
{
    tQoSHierarchyNode  *pHierarchyNode = NULL;
    tQoSQNode          *pQNode = NULL;
    tQoSQNode          *pTmpQNode = NULL;
    UINT4               u4TmpQId = 0;

    pHierarchyNode = QoSUtlGetHierarchyNode (i4IfIndex, u4FsQoSHierarchyLevel,
                                             u4FsQoSSchedulerId);
    if (pHierarchyNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetHierarchyNode () "
                      "Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    /* If the Same value is configured again no need to incrment the 
     * RefCount and Set */
    if (pHierarchyNode->u4QNext == u4SetValFsQoSHierarchyQNext)
    {
        return (SNMP_SUCCESS);
    }

    u4TmpQId = pHierarchyNode->u4QNext;
    /* Store the old value ,Reset corresponding ref count if the id not '0' */
    if (u4TmpQId != 0)
    {
        pTmpQNode = QoSUtlGetQNode (i4IfIndex, u4TmpQId);
        if (pTmpQNode == NULL)
        {
            QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetQNode () "
                          "Returns FAILURE. \r\n", __FUNCTION__);

            return (SNMP_FAILURE);
        }
        pTmpQNode->u4RefCount = (pTmpQNode->u4RefCount) - 1;
    }

    if (u4SetValFsQoSHierarchyQNext != 0)
    {
        pQNode = QoSUtlGetQNode (i4IfIndex, u4SetValFsQoSHierarchyQNext);
        if (pQNode == NULL)
        {
            QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetQTempNode () "
                          "Returns FAILURE. \r\n", __FUNCTION__);
            return (SNMP_FAILURE);
        }

        /* Incremnet the RefCount of QId  Entry in the Q Table */
        pQNode->u4RefCount = (pQNode->u4RefCount) + 1;
    }

    pHierarchyNode->u4QNext = u4SetValFsQoSHierarchyQNext;

    QoSValidateHierarchyTblEntry (pHierarchyNode);

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhSetFsQoSHierarchySchedNext
 Input       :  The Indices
                IfIndex
                FsQoSHierarchyLevel
                FsQoSSchedulerId

                The Object 
                setValFsQoSHierarchySchedNext
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsQoSHierarchySchedNext (INT4 i4IfIndex, UINT4 u4FsQoSHierarchyLevel,
                               UINT4 u4FsQoSSchedulerId,
                               UINT4 u4SetValFsQoSHierarchySchedNext)
{
    tQoSHierarchyNode  *pHierarchyNode = NULL;
    tQoSSchedNode      *pSchedNode = NULL;
    tQoSSchedNode      *pTmpSchedNode = NULL;
    UINT4               u4TmpSchedId = 0;

    pHierarchyNode = QoSUtlGetHierarchyNode (i4IfIndex, u4FsQoSHierarchyLevel,
                                             u4FsQoSSchedulerId);
    if (pHierarchyNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetHierarchyNode () "
                      "Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    /* If the Same value is configured again no need to incrment the 
     * RefCount and Set */
    if (pHierarchyNode->u4SchedNext == u4SetValFsQoSHierarchySchedNext)
    {
        return (SNMP_SUCCESS);
    }

    u4TmpSchedId = pHierarchyNode->u4SchedNext;
    /* Store the old value ,Reset corresponding ref count if the id not '0' */
    if (u4TmpSchedId != 0)
    {
        pTmpSchedNode = QoSUtlGetSchedNode (i4IfIndex, u4TmpSchedId);
        if (pTmpSchedNode == NULL)
        {
            QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetSchedNode () "
                          "Returns FAILURE. \r\n", __FUNCTION__);

            return (SNMP_FAILURE);
        }
        pTmpSchedNode->u4RefCount = (pTmpSchedNode->u4RefCount) - 1;
    }

    if (u4SetValFsQoSHierarchySchedNext != 0)
    {
        pSchedNode = QoSUtlGetSchedNode (i4IfIndex,
                                         u4SetValFsQoSHierarchySchedNext);
        if (pSchedNode == NULL)
        {
            QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetSchedNode () "
                          "Returns FAILURE. \r\n", __FUNCTION__);
            return (SNMP_FAILURE);
        }

        /* Incremnet the RefCount of SchedId  Entry in the Sched Table */
        pSchedNode->u4RefCount = (pSchedNode->u4RefCount) + 1;
    }

    pHierarchyNode->u4SchedNext = u4SetValFsQoSHierarchySchedNext;

    QoSValidateHierarchyTblEntry (pHierarchyNode);

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhSetFsQoSHierarchyWeight
 Input       :  The Indices
                IfIndex
                FsQoSHierarchyLevel
                FsQoSSchedulerId

                The Object 
                setValFsQoSHierarchyWeight
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsQoSHierarchyWeight (INT4 i4IfIndex, UINT4 u4FsQoSHierarchyLevel,
                            UINT4 u4FsQoSSchedulerId,
                            UINT4 u4SetValFsQoSHierarchyWeight)
{
    tQoSHierarchyNode  *pHierarchyNode = NULL;

    pHierarchyNode = QoSUtlGetHierarchyNode (i4IfIndex, u4FsQoSHierarchyLevel,
                                             u4FsQoSSchedulerId);
    if (pHierarchyNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetHierarchyNode () "
                      "Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    pHierarchyNode->u2Weight = (UINT2) u4SetValFsQoSHierarchyWeight;

    QoSValidateHierarchyTblEntry (pHierarchyNode);

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhSetFsQoSHierarchyPriority
 Input       :  The Indices
                IfIndex
                FsQoSHierarchyLevel
                FsQoSSchedulerId

                The Object 
                setValFsQoSHierarchyPriority
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsQoSHierarchyPriority (INT4 i4IfIndex, UINT4 u4FsQoSHierarchyLevel,
                              UINT4 u4FsQoSSchedulerId,
                              INT4 i4SetValFsQoSHierarchyPriority)
{
    tQoSHierarchyNode  *pHierarchyNode = NULL;

    pHierarchyNode = QoSUtlGetHierarchyNode (i4IfIndex, u4FsQoSHierarchyLevel,
                                             u4FsQoSSchedulerId);
    if (pHierarchyNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetHierarchyNode () "
                      "Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    pHierarchyNode->u1Priority = (UINT1) i4SetValFsQoSHierarchyPriority;

    QoSValidateHierarchyTblEntry (pHierarchyNode);

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhSetFsQoSHierarchyStatus
 Input       :  The Indices
                IfIndex
                FsQoSHierarchyLevel
                FsQoSSchedulerId

                The Object 
                setValFsQoSHierarchyStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsQoSHierarchyStatus (INT4 i4IfIndex, UINT4 u4FsQoSHierarchyLevel,
                            UINT4 u4FsQoSSchedulerId,
                            INT4 i4SetValFsQoSHierarchyStatus)
{
    tQoSHierarchyNode  *pHierarchyNode = NULL;
    INT4                i4RetStatus = QOS_FAILURE;
    INT4                i4ReturnValue = SNMP_SUCCESS;
    tQoSSchedNode      *pSchedNode = NULL;
    tQoSSchedNode      *pSchedNextNode = NULL;
    UINT4               u4NextLevelSchedulerId = 0;
    UINT4               u4NextLevelSchedulerHwId = 0;
    pHierarchyNode = QoSUtlGetHierarchyNode (i4IfIndex, u4FsQoSHierarchyLevel,
                                             u4FsQoSSchedulerId);

    if (pHierarchyNode != NULL)
    {
        /* Entry found in the Table */
        if (pHierarchyNode->u1Status == i4SetValFsQoSHierarchyStatus)
        {
            /* same value */
            return (SNMP_SUCCESS);
        }
    }
    else
    {
        /* New Entry need to be created in the Table, only if 
         * i4SetValFsQoSHierarchyStatus = CREATE_AND_WAIT */
        /* Optional */
        if (i4SetValFsQoSHierarchyStatus == DESTROY)
        {
            return (SNMP_SUCCESS);
        }
    }

    switch (i4SetValFsQoSHierarchyStatus)
    {
            /* For a New Entry */
        case CREATE_AND_WAIT:

            pHierarchyNode =
                QoSCreateHierarchyTblEntry (i4IfIndex, u4FsQoSHierarchyLevel,
                                            u4FsQoSSchedulerId);
            if (pHierarchyNode == NULL)
            {
                QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSCreateHierarchyTblEntry ()"
                              " Returns FAILURE. \r\n", __FUNCTION__);

                return (SNMP_FAILURE);
            }
            if (pHierarchyNode != NULL)
            {
                pHierarchyNode->u1Status = NOT_READY;
            }

            i4ReturnValue = SNMP_SUCCESS;

            break;

            /* Modify an Entry */
        case ACTIVE:
            /* 1. Add the entry into the HARDWARE. */
            if (gQoSGlobalInfo.eSysStatus == QOS_SYS_STATUS_ENABLE)
            {
                if (pHierarchyNode != NULL)
                {
                    /* If hierarchical scheduling is not supported */
                    if (u4FsQoSHierarchyLevel == 0)
                    {
                        i4RetStatus =
                            QoSHwWrSchedulerHierarchyMap (pHierarchyNode->
                                                          i4IfIndex,
                                                          pHierarchyNode->
                                                          u4SchedId,
                                                          pHierarchyNode->
                                                          u2Weight,
                                                          pHierarchyNode->
                                                          u1Priority,
                                                          pHierarchyNode->
                                                          u4SchedNext,
                                                          pHierarchyNode->
                                                          u4QNext,
                                                          pHierarchyNode->
                                                          u1Level,
                                                          QOS_HIERARCHY_ADD);
                        if (i4RetStatus == QOS_FAILURE)
                        {
                            QOS_TRC_ARG1 (MGMT_TRC, "In %s : "
                                          "QoSHwWrSchedulerHierarchyMap : Add "
                                          " () Returns FAILURE. \r\n",
                                          __FUNCTION__);

                            return (SNMP_FAILURE);
                        }
                    }
                    else
                    {
                        pSchedNode =
                            QoSUtlGetSchedNode (i4IfIndex, u4FsQoSSchedulerId);
                        if (pSchedNode == NULL)
                        {
                            return SNMP_FAILURE;
                        }
                        if (pHierarchyNode->u1Level == 1)
                        {
                            /* Map the S1 Scheduler to Port */
                            u4NextLevelSchedulerHwId = 0;
                        }
                        else
                        {
                            /*For other than S1 schedulers get the next level scheduler */
                            i4RetStatus =
                                nmhGetFsQoSHierarchySchedNext (i4IfIndex,
                                                               u4FsQoSHierarchyLevel,
                                                               u4FsQoSSchedulerId,
                                                               &u4NextLevelSchedulerId);

                            pSchedNextNode =
                                QoSUtlGetSchedNode (i4IfIndex,
                                                    u4NextLevelSchedulerId);
                            if (pSchedNextNode == NULL)
                            {
                                return SNMP_FAILURE;
                            }
                            /* Map S2 or S3 Schedulers to next level scheduler */
                            u4NextLevelSchedulerHwId =
                                pSchedNextNode->u4SchedulerHwId;
                        }
                        i4RetStatus =
                            QoSHwWrSchedulerHierarchyMap (pHierarchyNode->
                                                          i4IfIndex,
                                                          pSchedNode->
                                                          u4SchedulerHwId,
                                                          pHierarchyNode->
                                                          u2Weight,
                                                          pHierarchyNode->
                                                          u1Priority,
                                                          u4NextLevelSchedulerHwId,
                                                          pHierarchyNode->
                                                          u4QNext,
                                                          pHierarchyNode->
                                                          u1Level,
                                                          QOS_HIERARCHY_ADD);
                        if (i4RetStatus == QOS_FAILURE)
                        {
                            QOS_TRC_ARG1 (MGMT_TRC, "In %s : "
                                          "QoSHwWrSchedulerHierarchyMap : Add "
                                          " () Returns FAILURE. \r\n",
                                          __FUNCTION__);

                            return (SNMP_FAILURE);
                        }
                        else
                        {
                            /*Increment the count of children associated with the
                             * scheduler.
                             */
                            if (u4NextLevelSchedulerHwId != 0)
                            {
                                pSchedNextNode->u4SchedChildren++;
                            }
                        }
                    }
                }
            }
            if (pHierarchyNode != NULL)
            {
                pHierarchyNode->u1Status = ACTIVE;
            }
            break;

        case NOT_IN_SERVICE:

            /* 1. Remove the entry from the HARDWARE. */
            if (pHierarchyNode != NULL)
            {
                if ((pHierarchyNode->u1Status == ACTIVE) &&
                    (gQoSGlobalInfo.eSysStatus == QOS_SYS_STATUS_ENABLE))
                {
                    if (u4FsQoSHierarchyLevel == 0)
                    {
                        i4RetStatus =
                            QoSHwWrSchedulerHierarchyMap (pHierarchyNode->
                                                          i4IfIndex,
                                                          pHierarchyNode->
                                                          u4SchedId,
                                                          pHierarchyNode->
                                                          u2Weight,
                                                          pHierarchyNode->
                                                          u1Priority,
                                                          pHierarchyNode->
                                                          u4SchedNext,
                                                          pHierarchyNode->
                                                          u4QNext,
                                                          pHierarchyNode->
                                                          u1Level,
                                                          QOS_HIERARCHY_DEL);
                        if (i4RetStatus == QOS_FAILURE)
                        {
                            QOS_TRC_ARG1 (MGMT_TRC, "In %s : "
                                          "QoSHwWrSchedulerHierarchyMap : Del "
                                          " () Returns FAILURE. \r\n",
                                          __FUNCTION__);

                            return (SNMP_FAILURE);
                        }
                    }
                    else
                    {
                        pSchedNode =
                            QoSUtlGetSchedNode (i4IfIndex, u4FsQoSSchedulerId);
                        if (pSchedNode == NULL)
                        {
                            return SNMP_FAILURE;
                        }
                        if (pHierarchyNode->u1Level == 1)
                        {
                            /* Unmap the S1 Scheduler to Port */
                            u4NextLevelSchedulerHwId = 0;
                        }
                        else
                        {
                            /*For other than S1 schedulers get the next level scheduler */
                            i4RetStatus =
                                nmhGetFsQoSHierarchySchedNext (i4IfIndex,
                                                               u4FsQoSHierarchyLevel,
                                                               u4FsQoSSchedulerId,
                                                               &u4NextLevelSchedulerId);

                            pSchedNextNode =
                                QoSUtlGetSchedNode (i4IfIndex,
                                                    u4NextLevelSchedulerId);
                            if (pSchedNextNode == NULL)
                            {
                                return SNMP_FAILURE;
                            }
                            /* Unmap S2 or S3 Schedulers to next level scheduler */
                            u4NextLevelSchedulerHwId =
                                pSchedNextNode->u4SchedulerHwId;
                        }
                        i4RetStatus =
                            QoSHwWrSchedulerHierarchyMap (pHierarchyNode->
                                                          i4IfIndex,
                                                          pSchedNode->
                                                          u4SchedulerHwId,
                                                          pHierarchyNode->
                                                          u2Weight,
                                                          pHierarchyNode->
                                                          u1Priority,
                                                          u4NextLevelSchedulerHwId,
                                                          pHierarchyNode->
                                                          u4QNext,
                                                          pHierarchyNode->
                                                          u1Level,
                                                          QOS_HIERARCHY_DEL);
                        if (i4RetStatus == QOS_FAILURE)
                        {
                            QOS_TRC_ARG1 (MGMT_TRC, "In %s : "
                                          "QoSHwWrSchedulerHierarchyMap : Del "
                                          " () Returns FAILURE. \r\n",
                                          __FUNCTION__);
                            return (SNMP_FAILURE);
                        }
                        else
                        {
                            /* On successful detach, decrement the count of 
                             * children associated with the scheduler.*/

                            if (u4NextLevelSchedulerHwId != 0)
                            {
                                pSchedNextNode->u4SchedChildren--;
                            }
                        }
                    }
                }
            }
            if (pHierarchyNode != NULL)
            {
                pHierarchyNode->u1Status = NOT_IN_SERVICE;
            }
            break;

        case DESTROY:

            /* 1. Remove the entry from the HARDWARE. */
            if (pHierarchyNode != NULL)
            {
                if ((pHierarchyNode->u1Status == ACTIVE) &&
                    (gQoSGlobalInfo.eSysStatus == QOS_SYS_STATUS_ENABLE))
                {
                    if (u4FsQoSHierarchyLevel == 0)
                    {
                        i4RetStatus =
                            QoSHwWrSchedulerHierarchyMap (pHierarchyNode->
                                                          i4IfIndex,
                                                          pHierarchyNode->
                                                          u4SchedId,
                                                          pHierarchyNode->
                                                          u2Weight,
                                                          pHierarchyNode->
                                                          u1Priority,
                                                          pHierarchyNode->
                                                          u4SchedNext,
                                                          pHierarchyNode->
                                                          u4QNext,
                                                          pHierarchyNode->
                                                          u1Level,
                                                          QOS_HIERARCHY_DEL);
                        if (i4RetStatus == QOS_FAILURE)
                        {
                            QOS_TRC_ARG1 (MGMT_TRC, "In %s : "
                                          "QoSHwWrSchedulerHierarchyMap : Del "
                                          " () Returns FAILURE. \r\n",
                                          __FUNCTION__);

                            return (SNMP_FAILURE);
                        }
                    }
                    else
                    {
                        pSchedNode =
                            QoSUtlGetSchedNode (i4IfIndex, u4FsQoSSchedulerId);
                        if (pSchedNode == NULL)
                        {
                            return SNMP_FAILURE;
                        }
                        if (pHierarchyNode->u1Level == 1)
                        {
                            /* Unmap the S1 Scheduler to Port */
                            u4NextLevelSchedulerHwId = 0;
                        }
                        else
                        {
                            /*For other than S1 schedulers get the next level scheduler */
                            i4RetStatus =
                                nmhGetFsQoSHierarchySchedNext (i4IfIndex,
                                                               u4FsQoSHierarchyLevel,
                                                               u4FsQoSSchedulerId,
                                                               &u4NextLevelSchedulerId);

                            pSchedNextNode =
                                QoSUtlGetSchedNode (i4IfIndex,
                                                    u4NextLevelSchedulerId);
                            if (pSchedNextNode == NULL)
                            {
                                return SNMP_FAILURE;
                            }
                            /* Unmap S2 or S3 Schedulers to next level scheduler */
                            u4NextLevelSchedulerHwId =
                                pSchedNextNode->u4SchedulerHwId;
                        }
                        i4RetStatus =
                            QoSHwWrSchedulerHierarchyMap (pHierarchyNode->
                                                          i4IfIndex,
                                                          pSchedNode->
                                                          u4SchedulerHwId,
                                                          pHierarchyNode->
                                                          u2Weight,
                                                          pHierarchyNode->
                                                          u1Priority,
                                                          u4NextLevelSchedulerHwId,
                                                          pHierarchyNode->
                                                          u4QNext,
                                                          pHierarchyNode->
                                                          u1Level,
                                                          QOS_HIERARCHY_DEL);
                        if (i4RetStatus == QOS_FAILURE)
                        {
                            QOS_TRC_ARG1 (MGMT_TRC, "In %s : "
                                          "QoSHwWrSchedulerHierarchyMap : Del "
                                          " () Returns FAILURE. \r\n",
                                          __FUNCTION__);

                            return (SNMP_FAILURE);
                        }
                        else
                        {
                            /* On success, decrement the count of
                             * children associated with the scheduler.*/
                            if (u4NextLevelSchedulerHwId != 0)
                            {
                                pSchedNextNode->u4SchedChildren--;
                            }

                        }
                    }
                }
            }
            /* 2. Remove the entry from the SOFTWARE. */
            i4RetStatus = QoSDeleteHierarchyTblEntry (pHierarchyNode);
            if (i4RetStatus == QOS_FAILURE)
            {
                QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSDeleteHierarchyTblEntry ()"
                              " Returns FAILURE. \r\n", __FUNCTION__);

                return (SNMP_FAILURE);
            }

            break;

        case CREATE_AND_GO:
            i4ReturnValue = SNMP_FAILURE;
            break;

        default:
            i4ReturnValue = SNMP_FAILURE;
            break;
    }

    return ((INT1) i4ReturnValue);
}

/* Low Level TEST Routines for All Objects  */
/****************************************************************************
 Function    :  nmhTestv2FsQoSHierarchyQNext
 Input       :  The Indices
                IfIndex
                FsQoSHierarchyLevel
                FsQoSSchedulerId

                The Object 
                testValFsQoSHierarchyQNext
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsQoSHierarchyQNext (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                              UINT4 u4FsQoSHierarchyLevel,
                              UINT4 u4FsQoSSchedulerId,
                              UINT4 u4TestValFsQoSHierarchyQNext)
{
    INT4                i4RetStatus = QOS_FAILURE;

    *pu4ErrorCode = SNMP_ERR_NO_ERROR;

    /* Check Index */
    i4RetStatus = QoSUtlValidateHierarchyTblIdxInst (i4IfIndex,
                                                     u4FsQoSHierarchyLevel,
                                                     u4FsQoSSchedulerId,
                                                     pu4ErrorCode);
    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlValidateHierarchyTblIdxInst ()"
                      " Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    /* Reset NextQ Id */
    if (u4TestValFsQoSHierarchyQNext == 0)
    {
        return (SNMP_SUCCESS);
    }
    /* Check  Object's Value */
    i4RetStatus = QoSUtlValidateNextQ (i4IfIndex, u4TestValFsQoSHierarchyQNext,
                                       pu4ErrorCode);
    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlValidateNextQ ()"
                      " Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhTestv2FsQoSHierarchySchedNext
 Input       :  The Indices
                IfIndex
                FsQoSHierarchyLevel
                FsQoSSchedulerId

                The Object 
                testValFsQoSHierarchySchedNext
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsQoSHierarchySchedNext (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                                  UINT4 u4FsQoSHierarchyLevel,
                                  UINT4 u4FsQoSSchedulerId,
                                  UINT4 u4TestValFsQoSHierarchySchedNext)
{
    INT4                i4RetStatus = QOS_FAILURE;

    *pu4ErrorCode = SNMP_ERR_NO_ERROR;

    /* Check Index */
    i4RetStatus = QoSUtlValidateHierarchyTblIdxInst (i4IfIndex,
                                                     u4FsQoSHierarchyLevel,
                                                     u4FsQoSSchedulerId,
                                                     pu4ErrorCode);
    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlValidateHierarchyTblIdxInst ()"
                      " Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    /* Self Ref -  NextSched Id */
    if (u4TestValFsQoSHierarchySchedNext == u4FsQoSSchedulerId)
    {
        CLI_SET_ERR (QOS_CLI_ERR_HS_SELF_REF);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return (SNMP_FAILURE);
    }

    /* Reset NextSched Id */
    if (u4TestValFsQoSHierarchySchedNext == 0)
    {
        return (SNMP_SUCCESS);
    }

    /* Check  Object's Value */
    i4RetStatus = QoSUtlValidateSched (i4IfIndex,
                                       u4TestValFsQoSHierarchySchedNext,
                                       pu4ErrorCode);
    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlValidateSched ()"
                      " Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhTestv2FsQoSHierarchyWeight
 Input       :  The Indices
                IfIndex
                FsQoSHierarchyLevel
                FsQoSSchedulerId

                The Object 
                testValFsQoSHierarchyWeight
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsQoSHierarchyWeight (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                               UINT4 u4FsQoSHierarchyLevel,
                               UINT4 u4FsQoSSchedulerId,
                               UINT4 u4TestValFsQoSHierarchyWeight)
{
    INT4                i4RetStatus = QOS_FAILURE;

    *pu4ErrorCode = SNMP_ERR_NO_ERROR;

    /* Check Index */
    i4RetStatus = QoSUtlValidateHierarchyTblIdxInst (i4IfIndex,
                                                     u4FsQoSHierarchyLevel,
                                                     u4FsQoSSchedulerId,
                                                     pu4ErrorCode);
    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlValidateHierarchyTblIdxInst ()"
                      " Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    /* Check  Object's Value */
    if (u4TestValFsQoSHierarchyWeight > QOS_HIERARCHY_WEIGHT_MAX_VAL)
    {
        QOS_TRC_ARG4 (MGMT_TRC, "%s : Hierarchy weight %d is out of Range."
                      " The Range Should be (%d-%d). \r\n", __FUNCTION__,
                      u4TestValFsQoSHierarchyWeight,
                      QOS_HIERARCHY_WEIGHT_MIN_VAL,
                      QOS_HIERARCHY_WEIGHT_MAX_VAL);
        CLI_SET_ERR (QOS_CLI_ERR_WEIGHT);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;

        return (SNMP_FAILURE);
    }

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhTestv2FsQoSHierarchyPriority
 Input       :  The Indices
                IfIndex
                FsQoSHierarchyLevel
                FsQoSSchedulerId

                The Object 
                testValFsQoSHierarchyPriority
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsQoSHierarchyPriority (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                                 UINT4 u4FsQoSHierarchyLevel,
                                 UINT4 u4FsQoSSchedulerId,
                                 INT4 i4TestValFsQoSHierarchyPriority)
{
    INT4                i4RetStatus = QOS_FAILURE;

    *pu4ErrorCode = SNMP_ERR_NO_ERROR;

    /* Check Index */
    i4RetStatus = QoSUtlValidateHierarchyTblIdxInst (i4IfIndex,
                                                     u4FsQoSHierarchyLevel,
                                                     u4FsQoSSchedulerId,
                                                     pu4ErrorCode);
    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlValidateHierarchyTblIdxInst ()"
                      " Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    /* Check  Object's Value */
    if ((i4TestValFsQoSHierarchyPriority < QOS_HIERARCHY_PRI_MIN_VAL) ||
        (i4TestValFsQoSHierarchyPriority > QOS_HIERARCHY_PRI_MAX_VAL))
    {
        QOS_TRC_ARG4 (MGMT_TRC, "%s : Hierarchy Priority %d is out of Range."
                      " The Range Should be (%d-%d). \r\n", __FUNCTION__,
                      i4TestValFsQoSHierarchyPriority,
                      QOS_HIERARCHY_PRI_MIN_VAL, QOS_HIERARCHY_PRI_MAX_VAL);

        CLI_SET_ERR (QOS_CLI_ERR_PRIORITY);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;

        return (SNMP_FAILURE);
    }

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhTestv2FsQoSHierarchyStatus
 Input       :  The Indices
                IfIndex
                FsQoSHierarchyLevel
                FsQoSSchedulerId

                The Object 
                testValFsQoSHierarchyStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsQoSHierarchyStatus (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                               UINT4 u4FsQoSHierarchyLevel,
                               UINT4 u4FsQoSSchedulerId,
                               INT4 i4TestValFsQoSHierarchyStatus)
{
    tQoSHierarchyNode  *pHierarchyNode = NULL;
    INT4                i4RetStatus = QOS_FAILURE;
    UINT4               u4Count = 0;
    INT4                i4ReturnValue = SNMP_SUCCESS;

    if (gQoSGlobalInfo.eSysControl != QOS_SYS_CNTL_START)
    {
        CLI_SET_ERR (QOS_CLI_ERR_MODULE_SHUTDOW);
        QOS_TRC (MGMT_TRC, QOS_MODULE_SHUTDOW_ERROR_MSG);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return (SNMP_FAILURE);
    }

    *pu4ErrorCode = SNMP_ERR_NO_ERROR;

    /* Check the Range of Index Values (Min-Max) */
    i4RetStatus = QoSUtlValidateIfIndex (i4IfIndex, pu4ErrorCode);
    if (i4RetStatus == QOS_FAILURE)
    {
        return (SNMP_FAILURE);
    }

    /* Check the Range of Index Values (Min-Max) */
    i4RetStatus = QOS_CHECK_SCHED_TBL_INDEX_RANGE (u4FsQoSSchedulerId);
    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG3 (MGMT_TRC, "%s : HierarchySched Id %d is out of Range."
                      " The Range Should be (1-%d). \r\n", __FUNCTION__,
                      u4FsQoSSchedulerId, QOS_SCHED_TBL_MAX_INDEX_RANGE);
        CLI_SET_ERR (QOS_CLI_ERR_SCHED_RANGE);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;

        return (SNMP_FAILURE);
    }

    /* Check the Range of Index(HL) Values (Min-Max) */
    if ((u4FsQoSHierarchyLevel <= QOS_SCHED_HL_MIN_VAL) ||
        (u4FsQoSHierarchyLevel > QOS_SCHED_HL_MAX_VAL))
    {
        QOS_TRC_ARG4 (MGMT_TRC, "%s : Hierarchy Level %d is out of Range."
                      " The Range Should be (%d-%d). \r\n", __FUNCTION__,
                      u4FsQoSHierarchyLevel, (QOS_SCHED_HL_MIN_VAL + 1),
                      QOS_SCHED_HL_MAX_VAL);

        CLI_SET_ERR (QOS_CLI_ERR_HL_INVALID);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }

    /* Check the Sched */
    i4RetStatus = QoSUtlValidateHTIndex (i4IfIndex, u4FsQoSSchedulerId,
                                         u4FsQoSHierarchyLevel, pu4ErrorCode);
    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG3 (MGMT_TRC, "%s : QoSUtlValidateHTIndex () Returns FAILURE"
                      " for Sched Id  %d and HL %d. \r\n", __FUNCTION__,
                      u4FsQoSSchedulerId, u4FsQoSHierarchyLevel);
        return (SNMP_FAILURE);
    }

    pHierarchyNode = QoSUtlGetHierarchyNode (i4IfIndex, u4FsQoSHierarchyLevel,
                                             u4FsQoSSchedulerId);
    if (pHierarchyNode == NULL)
    {
        /* Entry Not Found In The Table */
        switch (i4TestValFsQoSHierarchyStatus)
        {
            case ACTIVE:
            case NOT_IN_SERVICE:
            case CREATE_AND_GO:
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                i4ReturnValue = SNMP_FAILURE;
                break;

            case CREATE_AND_WAIT:
                /* Check the No of Entries in the Table */
                i4RetStatus =
                    RBTreeCount (gQoSGlobalInfo.pRbHierarchyTbl, &u4Count);

                if ((i4RetStatus == RB_FAILURE) ||
                    (u4Count > QOS_HIERARCHY_TBL_MAX_ENTRIES))
                {
                    QOS_TRC_ARG2 (MGMT_TRC, "%s : Max No of Hierarchy "
                                  "Table Entries are Configured. No of Max "
                                  "Entries %d. \r\n", __FUNCTION__,
                                  QOS_HIERARCHY_TBL_MAX_ENTRIES);
                    CLI_SET_ERR (QOS_CLI_ERR_HS_MAX_ENTRY);
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;

                    return (SNMP_FAILURE);
                }

                break;

            case DESTROY:
                break;

            default:
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                i4ReturnValue = SNMP_FAILURE;
                break;
        }
    }
    else
    {
        /* Entry Found In The Table */
        switch (i4TestValFsQoSHierarchyStatus)
        {
            case ACTIVE:

                if ((pHierarchyNode->u1Status == NOT_READY))
                {
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                    return (SNMP_FAILURE);
                }

                break;

            case NOT_IN_SERVICE:

                if ((pHierarchyNode->u1Status == NOT_READY))
                {
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                    return (SNMP_FAILURE);
                }
                break;

            case DESTROY:
                break;

            case CREATE_AND_WAIT:
            case CREATE_AND_GO:
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                i4ReturnValue = SNMP_FAILURE;
                break;

            default:
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                i4ReturnValue = SNMP_FAILURE;
                break;

        }                        /* End of switch */

    }                            /* End of if */

    return ((INT1) i4ReturnValue);
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsQoSHierarchyTable
 Input       :  The Indices
                IfIndex
                FsQoSHierarchyLevel
                FsQoSSchedulerId
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsQoSHierarchyTable (UINT4 *pu4ErrorCode,
                             tSnmpIndexList * pSnmpIndexList,
                             tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsQoSDefUserPriorityTable. */
/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsQoSDefUserPriorityTable
 Input       :  The Indices
                IfIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsQoSDefUserPriorityTable (INT4 i4IfIndex)
{
    INT4                i4RetStatus = QOS_FAILURE;
    UINT4               u4ErrorCode = 0;

    if (gQoSGlobalInfo.eSysControl != QOS_SYS_CNTL_START)
    {
        QOS_TRC (MGMT_TRC, QOS_MODULE_SHUTDOW_ERROR_MSG);
        return (SNMP_FAILURE);
    }

    i4RetStatus = QoSUtlValidateIfIndex (i4IfIndex, &u4ErrorCode);
    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlValidateIfIndex () "
                      "Returns FAILURE. \r\n", __FUNCTION__);
        return (SNMP_FAILURE);
    }

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsQoSDefUserPriorityTable
 Input       :  The Indices
                IfIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsQoSDefUserPriorityTable (INT4 *pi4IfIndex)
{
    INT4                i4RetStatus = QOS_FAILURE;

    if (gQoSGlobalInfo.eSysControl != QOS_SYS_CNTL_START)
    {
        CLI_SET_ERR (QOS_CLI_ERR_MODULE_SHUTDOW);
        QOS_TRC (MGMT_TRC, QOS_MODULE_SHUTDOW_ERROR_MSG);
        return (SNMP_FAILURE);
    }

    /* Give the First Interfacae */
    i4RetStatus = QoSGetNextDUPTblEntryIndex (0, pi4IfIndex);
    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSGetNextDUPTblEntryIndex () "
                      "Returns FAILURE. \r\n", __FUNCTION__);
        return (SNMP_FAILURE);
    }

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsQoSDefUserPriorityTable
 Input       :  The Indices
                IfIndex
                nextIfIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsQoSDefUserPriorityTable (INT4 i4IfIndex, INT4 *pi4NextIfIndex)
{
    INT4                i4RetStatus = QOS_FAILURE;

    if (gQoSGlobalInfo.eSysControl != QOS_SYS_CNTL_START)
    {
        CLI_SET_ERR (QOS_CLI_ERR_MODULE_SHUTDOW);
        QOS_TRC (MGMT_TRC, QOS_MODULE_SHUTDOW_ERROR_MSG);
        return (SNMP_FAILURE);
    }

    i4RetStatus = QoSGetNextDUPTblEntryIndex (i4IfIndex, pi4NextIfIndex);
    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSGetNextDUPTblEntryIndex () "
                      "Returns FAILURE. \r\n", __FUNCTION__);
        return (SNMP_FAILURE);
    }

    return (SNMP_SUCCESS);
}

/* Low Level GET Routine for All Objects  */
/****************************************************************************
 Function    :  nmhGetFsQoSPortDefaultUserPriority
 Input       :  The Indices
                IfIndex

                The Object 
                retValFsQoSPortDefaultUserPriority
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsQoSPortDefaultUserPriority (INT4 i4IfIndex,
                                    INT4 *pi4RetValFsQoSPortDefaultUserPriority)
{
    UINT1              *pu1PriVal = NULL;
    INT4                i4ReturnValue = SNMP_FAILURE;

    pu1PriVal = QoSUtlGetDUPNode (i4IfIndex);
    if (pu1PriVal != NULL)
    {
        *pi4RetValFsQoSPortDefaultUserPriority = *pu1PriVal;
        i4ReturnValue = SNMP_SUCCESS;
    }
    else
    {
        i4ReturnValue = SNMP_FAILURE;
    }
    return ((INT1) i4ReturnValue);
}

/****************************************************************************
 Function    :  nmhGetFsQoSPortPbitPrefOverDscp
 Input       :  The Indices
                IfIndex

                The Object
                retValFsQoSPortPbitPrefOverDscp
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsQoSPortPbitPrefOverDscp (INT4 i4IfIndex,
                                 INT4 *pi4RetValFsQoSPortPbitPrefOverDscp)
{
    UINT1              *pu1Pref = NULL;
    INT4                i4ReturnValue = SNMP_FAILURE;

    pu1Pref = QoSGetPrefNode (i4IfIndex);
    if (pu1Pref != NULL)
    {
        *pi4RetValFsQoSPortPbitPrefOverDscp = *pu1Pref;
        i4ReturnValue = SNMP_SUCCESS;
    }
    else
    {
        i4ReturnValue = SNMP_FAILURE;
    }
    return ((INT1) i4ReturnValue);

}

/* Low Level SET Routine for All Objects  */
/****************************************************************************
 Function    :  nmhSetFsQoSPortDefaultUserPriority
 Input       :  The Indices
                IfIndex

                The Object 
                setValFsQoSPortDefaultUserPriority
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsQoSPortDefaultUserPriority (INT4 i4IfIndex,
                                    INT4 i4SetValFsQoSPortDefaultUserPriority)
{
    UINT1              *pu1PriVal = NULL;
    INT4                i4RetStatus = SNMP_FAILURE;

    if (gQoSGlobalInfo.eSysStatus == QOS_SYS_STATUS_ENABLE)
    {
        i4RetStatus =
            QoSHwWrSetDefUserPriority (i4IfIndex,
                                       i4SetValFsQoSPortDefaultUserPriority);
        if (i4RetStatus == QOS_FAILURE)
        {
            QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSHwWrSetDefUserPriority "
                          " () Returns FAILURE. \r\n", __FUNCTION__);
            return (SNMP_FAILURE);
        }
    }

    pu1PriVal = QoSUtlGetDUPNode (i4IfIndex);
    if (pu1PriVal == NULL)
    {
        i4RetStatus = SNMP_FAILURE;
    }
    else
    {
        *pu1PriVal = (UINT1) i4SetValFsQoSPortDefaultUserPriority;
        i4RetStatus = SNMP_SUCCESS;
    }

    return ((INT1) i4RetStatus);
}

/****************************************************************************
 Function    :  nmhSetFsQoSPortPbitPrefOverDscp
 Input       :  The Indices
                IfIndex

                The Object
                setValFsQoSPortPbitPrefOverDscp
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsQoSPortPbitPrefOverDscp (INT4 i4IfIndex,
                                 INT4 i4SetValFsQoSPortPbitPrefOverDscp)
{

    UINT1              *pu1PrefVal = NULL;
    INT4                i4RetStatus = SNMP_FAILURE;

    i4RetStatus =
        QoSHwWrSetPbitPreferenceOverDscp (i4IfIndex,
                                          i4SetValFsQoSPortPbitPrefOverDscp);
    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSHwWrSetPbitPreferenceOverDscp "
                      " () Returns FAILURE. \r\n", __FUNCTION__);
        return (SNMP_FAILURE);
    }

    pu1PrefVal = QoSGetPrefNode (i4IfIndex);
    if (pu1PrefVal == NULL)
    {
        return (SNMP_FAILURE);
    }
    else
    {
        *pu1PrefVal = (UINT1) i4SetValFsQoSPortPbitPrefOverDscp;
        i4RetStatus = SNMP_SUCCESS;
    }

    return ((INT1) i4RetStatus);

}

/* Low Level TEST Routines for All Objects  */
/****************************************************************************
 Function    :  nmhTestv2FsQoSPortDefaultUserPriority
 Input       :  The Indices
                IfIndex

                The Object 
                testValFsQoSPortDefaultUserPriority
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsQoSPortDefaultUserPriority (UINT4 *pu4ErrorCode,
                                       INT4 i4IfIndex,
                                       INT4
                                       i4TestValFsQoSPortDefaultUserPriority)
{
    INT4                i4RetStatus = QOS_FAILURE;

    *pu4ErrorCode = SNMP_ERR_NO_ERROR;

    if (gQoSGlobalInfo.eSysControl != QOS_SYS_CNTL_START)
    {
        CLI_SET_ERR (QOS_CLI_ERR_MODULE_SHUTDOW);
        QOS_TRC (MGMT_TRC, QOS_MODULE_SHUTDOW_ERROR_MSG);
        return (SNMP_FAILURE);
    }

    /* Check Index */
    i4RetStatus = QoSUtlValidateIfIndex (i4IfIndex, pu4ErrorCode);
    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlValidateIfIndex () "
                      "Returns FAILURE. \r\n", __FUNCTION__);
        return (SNMP_FAILURE);
    }

    /* Check  Object's Value */
    if ((i4TestValFsQoSPortDefaultUserPriority < QOS_PDUP_TBL_MIN_PRI_VAL) ||
        (i4TestValFsQoSPortDefaultUserPriority > QOS_PDUP_TBL_MAX_PRI_VAL))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhTestv2FsQoSPortPbitPrefOverDscp
 Input       :  The Indices
                IfIndex

                The Object
                testValFsQoSPortPbitPrefOverDscp
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsQoSPortPbitPrefOverDscp (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                                    INT4 i4TestValFsQoSPortPbitPrefOverDscp)
{
    INT4                i4RetStatus = QOS_FAILURE;

    *pu4ErrorCode = SNMP_ERR_NO_ERROR;
    /*check object's value */
    if (i4TestValFsQoSPortPbitPrefOverDscp != QOS_PBIT_PREF_ENABLE &&
        i4TestValFsQoSPortPbitPrefOverDscp != QOS_PBIT_PREF_DISABLE)
    {
        return (SNMP_FAILURE);
    }
    /*check index */
    i4RetStatus = QoSUtlValidateIfIndex (i4IfIndex, pu4ErrorCode);
    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlValidateIfIndex () "
                      "Returns FAILURE. \r\n", __FUNCTION__);
        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);

}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsQoSDefUserPriorityTable
 Input       :  The Indices
                IfIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsQoSDefUserPriorityTable (UINT4 *pu4ErrorCode,
                                   tSnmpIndexList * pSnmpIndexList,
                                   tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsQoSPolicerStatsTable. */
/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsQoSPolicerStatsTable
 Input       :  The Indices
                FsQoSMeterId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsQoSPolicerStatsTable (UINT4 u4FsQoSMeterId)
{
    INT4                i4RetStatus = 0;
    tQoSMeterNode      *pMeterNode = NULL;

    if (gQoSGlobalInfo.eSysControl != QOS_SYS_CNTL_START)
    {
        QOS_TRC (MGMT_TRC, QOS_MODULE_SHUTDOW_ERROR_MSG);
        return (SNMP_FAILURE);
    }

    /* Check the Range of Index Values (Min-Max) */
    i4RetStatus = QOS_CHECK_METER_TBL_INDEX_RANGE (u4FsQoSMeterId);

    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG3 (MGMT_TRC, "%s : Policer Id %d is out of Range."
                      " The Range Should be (1-%d). \r\n", __FUNCTION__,
                      u4FsQoSMeterId, QOS_METER_TBL_MAX_INDEX_RANGE);

        return (SNMP_FAILURE);
    }

    pMeterNode = QoSUtlGetMeterNode (u4FsQoSMeterId);

    if (pMeterNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetMeterNode () "
                      " Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsQoSPolicerStatsTable
 Input       :  The Indices
                FsQoSMeterId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsQoSPolicerStatsTable (UINT4 *pu4FsQoSMeterId)
{
    INT4                i4RetStatus = QOS_FAILURE;

    if (gQoSGlobalInfo.eSysControl != QOS_SYS_CNTL_START)
    {
        CLI_SET_ERR (QOS_CLI_ERR_MODULE_SHUTDOW);
        QOS_TRC (MGMT_TRC, QOS_MODULE_SHUTDOW_ERROR_MSG);
        return (SNMP_FAILURE);
    }

    i4RetStatus = QoSGetNextMeterStatsTblEntryIndex (0, pu4FsQoSMeterId);

    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSGetNextMeterStatsTblEntryIndex ()"
                      " Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsQoSPolicerStatsTable
 Input       :  The Indices
                FsQoSMeterId
                nextFsQoSMeterId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsQoSPolicerStatsTable (UINT4 u4FsQoSMeterId,
                                       UINT4 *pu4NextFsQoSMeterId)
{
    INT4                i4RetStatus = QOS_FAILURE;

    if (gQoSGlobalInfo.eSysControl != QOS_SYS_CNTL_START)
    {
        CLI_SET_ERR (QOS_CLI_ERR_MODULE_SHUTDOW);
        QOS_TRC (MGMT_TRC, QOS_MODULE_SHUTDOW_ERROR_MSG);
        return (SNMP_FAILURE);
    }

    i4RetStatus = QoSGetNextMeterStatsTblEntryIndex (u4FsQoSMeterId,
                                                     pu4NextFsQoSMeterId);
    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSGetNextMeterStatsTblEntryIndex ()"
                      " Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    return (SNMP_SUCCESS);

}

/* Low Level GET Routine for All Objects  */
/****************************************************************************
 Function    :  nmhGetFsQoSPolicerStatsConformPkts
 Input       :  The Indices
                FsQoSMeterId

                The Object 
                retValFsQoSPolicerStatsConformPkts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsQoSPolicerStatsConformPkts (UINT4 u4FsQoSMeterId,
                                    tSNMP_COUNTER64_TYPE *
                                    pu8RetValFsQoSPolicerStatsConformPkts)
{
    INT4                i4RetStatus = QOS_FAILURE;

    i4RetStatus = QoSHwWrGetMeterStats (u4FsQoSMeterId,
                                        QOS_POLICER_STATS_CONF_PKTS,
                                        pu8RetValFsQoSPolicerStatsConformPkts);
    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG2 (MGMT_TRC, "In %s : ERROR: QoSHwWrGetMeterStats () "
                      " Returns FAILURE for a Meter Id %d Stats type of "
                      " QOS_POLICER_STATS_CONF_PKTS .\r\n",
                      u4FsQoSMeterId, __FUNCTION__);

        return (SNMP_FAILURE);
    }

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFsQoSPolicerStatsConformOctets
 Input       :  The Indices
                FsQoSMeterId

                The Object 
                retValFsQoSPolicerStatsConformOctets
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsQoSPolicerStatsConformOctets (UINT4 u4FsQoSMeterId,
                                      tSNMP_COUNTER64_TYPE *
                                      pu8RetValFsQoSPolicerStatsConformOctets)
{
    INT4                i4RetStatus = QOS_FAILURE;

    i4RetStatus = QoSHwWrGetMeterStats
        (u4FsQoSMeterId,
         QOS_POLICER_STATS_CONF_OCTETS,
         pu8RetValFsQoSPolicerStatsConformOctets);
    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG2 (MGMT_TRC, "In %s : ERROR: QoSHwWrGetMeterStats () "
                      " Returns FAILURE for a Meter Id %d Stats type of "
                      " QOS_POLICER_STATS_CONF_OCTETS .\r\n",
                      u4FsQoSMeterId, __FUNCTION__);

        return (SNMP_FAILURE);
    }

    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhGetFsQoSPolicerStatsExceedPkts
 Input       :  The Indices
                FsQoSMeterId

                The Object 
                retValFsQoSPolicerStatsExceedPkts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsQoSPolicerStatsExceedPkts (UINT4 u4FsQoSMeterId,
                                   tSNMP_COUNTER64_TYPE *
                                   pu8RetValFsQoSPolicerStatsExceedPkts)
{
    INT4                i4RetStatus = QOS_FAILURE;

    i4RetStatus = QoSHwWrGetMeterStats (u4FsQoSMeterId,
                                        QOS_POLICER_STATS_EXC_PKTS,
                                        pu8RetValFsQoSPolicerStatsExceedPkts);
    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG2 (MGMT_TRC, "In %s : ERROR: QoSHwWrGetMeterStats () "
                      " Returns FAILURE for a Meter Id %d Stats type of "
                      " QOS_POLICER_STATS_EXC_PKTS .\r\n",
                      u4FsQoSMeterId, __FUNCTION__);

        return (SNMP_FAILURE);
    }

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFsQoSPolicerStatsExceedOctets
 Input       :  The Indices
                FsQoSMeterId

                The Object 
                retValFsQoSPolicerStatsExceedOctets
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsQoSPolicerStatsExceedOctets (UINT4 u4FsQoSMeterId,
                                     tSNMP_COUNTER64_TYPE *
                                     pu8RetValFsQoSPolicerStatsExceedOctets)
{
    INT4                i4RetStatus = QOS_FAILURE;

    i4RetStatus = QoSHwWrGetMeterStats (u4FsQoSMeterId,
                                        QOS_POLICER_STATS_EXC_OCTETS,
                                        pu8RetValFsQoSPolicerStatsExceedOctets);
    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG2 (MGMT_TRC, "In %s : ERROR: QoSHwWrGetMeterStats () "
                      " Returns FAILURE for a Meter Id %d Stats type of "
                      " QOS_POLICER_STATS_EXC_OCTETS .\r\n",
                      u4FsQoSMeterId, __FUNCTION__);

        return (SNMP_FAILURE);
    }

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFsQoSPolicerStatsViolatePkts
 Input       :  The Indices
                FsQoSMeterId

                The Object 
                retValFsQoSPolicerStatsViolatePkts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsQoSPolicerStatsViolatePkts (UINT4 u4FsQoSMeterId,
                                    tSNMP_COUNTER64_TYPE *
                                    pu8RetValFsQoSPolicerStatsViolatePkts)
{
    INT4                i4RetStatus = QOS_FAILURE;

    i4RetStatus = QoSHwWrGetMeterStats (u4FsQoSMeterId,
                                        QOS_POLICER_STATS_VIO_PKTS,
                                        pu8RetValFsQoSPolicerStatsViolatePkts);
    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG2 (MGMT_TRC, "In %s : ERROR: QoSHwWrGetMeterStats () "
                      " Returns FAILURE for a Meter Id %d Stats type of "
                      " QOS_POLICER_STATS_VIO_PKTS .\r\n",
                      u4FsQoSMeterId, __FUNCTION__);

        return (SNMP_FAILURE);
    }

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFsQoSPolicerStatsViolateOctets
 Input       :  The Indices
                FsQoSMeterId

                The Object 
                retValFsQoSPolicerStatsViolateOctets
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsQoSPolicerStatsViolateOctets (UINT4 u4FsQoSMeterId,
                                      tSNMP_COUNTER64_TYPE *
                                      pu8RetValFsQoSPolicerStatsViolateOctets)
{
    INT4                i4RetStatus = QOS_FAILURE;

    i4RetStatus = QoSHwWrGetMeterStats
        (u4FsQoSMeterId,
         QOS_POLICER_STATS_VIO_OCTETS, pu8RetValFsQoSPolicerStatsViolateOctets);
    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG2 (MGMT_TRC, "In %s : ERROR: QoSHwWrGetMeterStats () "
                      " Returns FAILURE for a Meter Id %d Stats type of "
                      " QOS_POLICER_STATS_VIO_OCTETS .\r\n",
                      u4FsQoSMeterId, __FUNCTION__);

        return (SNMP_FAILURE);
    }

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFsQoSPolicerStatsStatus
 Input       :  The Indices
                FsQoSMeterId

                The Object
                retValFsQoSPolicerStatsStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsQoSPolicerStatsStatus (UINT4 u4FsQoSMeterId,
                               INT4 *pi4RetValFsQoSPolicerStatsStatus)
{
    tQoSMeterNode      *pMeterNode = NULL;

    pMeterNode = QoSUtlGetMeterNode (u4FsQoSMeterId);
    if (pMeterNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetMeterNode () Returns "
                      "FAILURE. \r\n", __FUNCTION__);
        return (SNMP_FAILURE);
    }
    *pi4RetValFsQoSPolicerStatsStatus = pMeterNode->u1StatsStatus;

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFsQoSPolicerStatsClearCounter
 Input       :  The Indices
                FsQoSMeterId

                The Object
                retValFsQoSPolicerStatsClearCounter
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsQoSPolicerStatsClearCounter (UINT4 u4FsQoSMeterId,
                                     INT4
                                     *pi4RetValFsQoSPolicerStatsClearCounter)
{
    UNUSED_PARAM (u4FsQoSMeterId);
    UNUSED_PARAM (pi4RetValFsQoSPolicerStatsClearCounter);
    return (SNMP_SUCCESS);

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsQoSPolicerStatsStatus
 Input       :  The Indices
                FsQoSMeterId

                The Object
                setValFsQoSPolicerStatsStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsQoSPolicerStatsStatus (UINT4 u4FsQoSMeterId,
                               INT4 i4SetValFsQoSPolicerStatsStatus)
{
    tQoSClassMapEntry  *pClsMapEnt = NULL;
    tQoSClassInfoNode  *pClsInfoNode = NULL;
    tQoSFilterInfoNode *pFltInfoNode = NULL;
    tQoSPolicyMapNode  *pPlyMapNode = NULL;
    tQoSMeterNode      *pMeterNode = NULL;
    UINT4               u4HwFilterId = 0;
    UINT4               u4PrevPlyId = 0;
    UINT4               u4CurPlyId = 0;
    INT4                i4RetStatus = QOS_FAILURE;
    INT4                i4PolicyIdRetStatus = QOS_FAILURE;

    /*Checking whether the Meter was created for the given Id */
    pMeterNode = QoSUtlGetMeterNode (u4FsQoSMeterId);
    if (pMeterNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetMeterNode () Returns "
                      "FAILURE. \r\n", __FUNCTION__);

        CLI_SET_ERR (QOS_CLI_ERR_METER_NO_ENTRY);
        return (SNMP_FAILURE);
    }

    do
    {
        /*Assigning the new policy Id to the previous Policy Id for traversing */
        u4PrevPlyId = u4CurPlyId;

        /*Fetching  policy Ids */
        i4PolicyIdRetStatus = QoSUtlGetPolicyMapId (u4PrevPlyId, &u4CurPlyId);

        if ((i4PolicyIdRetStatus != QOS_FAILURE) && (u4PrevPlyId != u4CurPlyId))
        {
            /* Fetching policy map node for the given id */
            pPlyMapNode = QoSUtlGetPolicyMapNode (u4CurPlyId);

            /*checking whether the meter id is same in the policy map node */
            if ((pPlyMapNode != NULL)
                && (pPlyMapNode->u4MeterTableId == u4FsQoSMeterId))
            {

                pClsInfoNode = QoSUtlGetClassInfoNode (pPlyMapNode->u4ClassId);

                if (pClsInfoNode == NULL)
                {
                    CLI_SET_ERR (QOS_CLI_ERR_CLS_NOT_CREATED);
                    return (SNMP_FAILURE);
                }
#ifdef NPAPI_WANTED
                /* Scan the Filter list in the ClassInfo */
                TMO_SLL_Scan (&(pClsInfoNode->SllFltInfo), pFltInfoNode,
                              tQoSFilterInfoNode *)
                {
                    /*checking whether the class id is matching with the policy map class id.
                       if  it is matching, filling the hardware filter id */

                    if ((pFltInfoNode != NULL) &&
                        (pFltInfoNode->pClsMapNode->u4ClassId ==
                         pPlyMapNode->u4ClassId))
                    {
                        pClsMapEnt =
                            QoSFiltersForClassMapEntry (pFltInfoNode->
                                                        pClsMapNode);
                        if (pClsMapEnt == NULL)
                        {

                            CLI_SET_ERR (QOS_CLI_ERR_NO_FILTER);
                            return (SNMP_FAILURE);
                        }
                        if (pClsMapEnt->pL3FilterPtr != NULL)
                        {
                            u4HwFilterId =
                                pClsMapEnt->pL3FilterPtr->pIssFilterShadowInfo->
                                au4HwHandle[0];
                        }
                        else if (pClsMapEnt->pL2FilterPtr != NULL)
                        {
                            u4HwFilterId =
                                pClsMapEnt->pL2FilterPtr->pIssFilterShadowInfo->
                                au4HwHandle[0];
                        }
                    }

                }

                /*Checking whether the hardware filter id is properly filled */
                if (u4HwFilterId == 0)
                {
                    CLI_SET_ERR (QOS_CLI_ERR_CLS_MAP_SET_CLS_NOT_SUPPORTED);
                    return (SNMP_FAILURE);
                }
#else
                UNUSED_PARAM (u4HwFilterId);
                UNUSED_PARAM (pFltInfoNode);
                UNUSED_PARAM (pClsMapEnt);
                CLI_SET_ERR (QOS_CLI_ERR_NO_FILTER);
#endif

                if (!
                    ((pMeterNode->u1StatsStatus ==
                      i4SetValFsQoSPolicerStatsStatus)
                     && (pPlyMapNode->u1StatsEnabled == QOS_TRUE)))
                {
                    /*Already Statistics status enabled for the corresponding policer. So hardware trigger is avoided */
                    pMeterNode->u1StatsStatus = i4SetValFsQoSPolicerStatsStatus;
                    pPlyMapNode->u1StatsEnabled = QOS_TRUE;

                    if (gQoSGlobalInfo.eSysStatus == QOS_SYS_STATUS_ENABLE)
                    {

#ifdef NPAPI_WANTED
                        /* 1. Update the entry into the HARDWARE. */
                        i4RetStatus =
                            QoSHwWrMeterStatsUpdate (u4HwFilterId, pMeterNode);

                        if (i4RetStatus == QOS_FAILURE)
                        {
                            QOS_TRC_ARG1 (MGMT_TRC,
                                          "In %s : QoSHwWrMeterCreate () "
                                          " Returns FAILURE. \r\n",
                                          __FUNCTION__);

                            return (SNMP_FAILURE);
                        }
#else
                        UNUSED_PARAM (i4RetStatus);
#endif

                    }
                }
            }
        }
    }
    while ((i4PolicyIdRetStatus != QOS_FAILURE) || (u4PrevPlyId != u4CurPlyId));

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhSetFsQoSPolicerStatsClearCounter
 Input       :  The Indices
                FsQoSMeterId

                The Object
                setValFsQoSPolicerStatsClearCounter
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsQoSPolicerStatsClearCounter (UINT4 u4FsQoSMeterId,
                                     INT4 i4SetValFsQoSPolicerStatsClearCounter)
{
    tQoSMeterNode      *pMeterNode = NULL;

    /*Checking whether the Meter was created for the given Id */
    pMeterNode = QoSUtlGetMeterNode (u4FsQoSMeterId);
    if (pMeterNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetMeterNode () Returns "
                      "FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }
    if ((i4SetValFsQoSPolicerStatsClearCounter == QOS_TRUE) &&
        (pMeterNode->u1StatsStatus == QOS_STATS_ENABLE))
    {
        if (QoSHwWrMeterStatsClear (pMeterNode) != QOS_SUCCESS)
        {
            return SNMP_FAILURE;
        }
    }
    return (SNMP_SUCCESS);
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsQoSPolicerStatsStatus
 Input       :  The Indices
                FsQoSMeterId

                The Object
                testValFsQoSPolicerStatsStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsQoSPolicerStatsStatus (UINT4 *pu4ErrorCode, UINT4 u4FsQoSMeterId,
                                  INT4 i4TestValFsQoSPolicerStatsStatus)
{
    tQoSMeterNode      *pMeterNode = NULL;
    INT4                i4RetStatus = QOS_FAILURE;

    *pu4ErrorCode = SNMP_ERR_NO_ERROR;

    /* Check the Range of Index Values (Min-Max) */
    i4RetStatus = QOS_CHECK_METER_TBL_INDEX_RANGE (u4FsQoSMeterId);

    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG3 (MGMT_TRC, "%s : Meter Id %d is out of Range."
                      " The Range Should be (1-%d). \r\n", __FUNCTION__,
                      u4FsQoSMeterId, QOS_METER_TBL_MAX_INDEX_RANGE);

        CLI_SET_ERR (QOS_CLI_ERR_METER_RANGE);

        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;

        return (SNMP_FAILURE);
    }

    /*checking the Status value */
    if ((i4TestValFsQoSPolicerStatsStatus != QOS_STATS_ENABLE)
        && (i4TestValFsQoSPolicerStatsStatus != QOS_STATS_DISABLE))
    {
        QOS_TRC (MGMT_TRC, " Invalid status is given !\r\n");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }

    /*Checking whether the Meter was created for the given Id */
    pMeterNode = QoSUtlGetMeterNode (u4FsQoSMeterId);
    if (pMeterNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetMeterNode () Returns "
                      "FAILURE. \r\n", __FUNCTION__);

        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (QOS_CLI_ERR_METER_NO_ENTRY);
        return (SNMP_FAILURE);
    }

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhTestv2FsQoSPolicerStatsClearCounter
 Input       :  The Indices
                FsQoSMeterId

                The Object
                testValFsQoSPolicerStatsClearCounter
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsQoSPolicerStatsClearCounter (UINT4 *pu4ErrorCode,
                                        UINT4 u4FsQoSMeterId,
                                        INT4
                                        i4TestValFsQoSPolicerStatsClearCounter)
{
    tQoSMeterNode      *pMeterNode = NULL;

    *pu4ErrorCode = SNMP_ERR_NO_ERROR;

    if ((i4TestValFsQoSPolicerStatsClearCounter != TRUE) &&
        (i4TestValFsQoSPolicerStatsClearCounter != FALSE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    /*Checking whether the Meter was created for the given Id */
    pMeterNode = QoSUtlGetMeterNode (u4FsQoSMeterId);
    if (pMeterNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetMeterNode () Returns "
                      "FAILURE. \r\n", __FUNCTION__);

        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (QOS_CLI_ERR_METER_NO_ENTRY);
        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);

}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsQoSPolicerStatsTable
 Input       :  The Indices
                FsQoSMeterId
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsQoSPolicerStatsTable (UINT4 *pu4ErrorCode,
                                tSnmpIndexList * pSnmpIndexList,
                                tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsQoSCoSQStatsTable. */
/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsQoSCoSQStatsTable
 Input       :  The Indices
                IfIndex
                FsQoSCoSQId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsQoSCoSQStatsTable (INT4 i4IfIndex,
                                             UINT4 u4FsQoSCoSQId)
{
    INT4                i4RetStatus = QOS_FAILURE;

    if (gQoSGlobalInfo.eSysControl != QOS_SYS_CNTL_START)
    {
        QOS_TRC (MGMT_TRC, QOS_MODULE_SHUTDOW_ERROR_MSG);
        return (SNMP_FAILURE);
    }

    i4RetStatus = QoSUtlValidateCoSQStatsTblIdxInst (i4IfIndex, u4FsQoSCoSQId);
    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlValidateCoSQStatsTblIdxInst () "
                      " Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsQoSCoSQStatsTable
 Input       :  The Indices
                IfIndex
                FsQoSCoSQId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsQoSCoSQStatsTable (INT4 *pi4IfIndex, UINT4 *pu4FsQoSCoSQId)
{
    INT4                i4RetStatus = QOS_FAILURE;

    if (gQoSGlobalInfo.eSysControl != QOS_SYS_CNTL_START)
    {
        CLI_SET_ERR (QOS_CLI_ERR_MODULE_SHUTDOW);
        QOS_TRC (MGMT_TRC, QOS_MODULE_SHUTDOW_ERROR_MSG);
        return (SNMP_FAILURE);
    }

    i4RetStatus = QoSGetNextCoSQStatsTblEntryIndex (0, pi4IfIndex,
                                                    0, pu4FsQoSCoSQId);
    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSGetNextCoSQStatsTblEntryIndex ()"
                      " Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsQoSCoSQStatsTable
 Input       :  The Indices
                IfIndex
                nextIfIndex
                FsQoSCoSQId
                nextFsQoSCoSQId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsQoSCoSQStatsTable (INT4 i4IfIndex, INT4 *pi4NextIfIndex,
                                    UINT4 u4FsQoSCoSQId,
                                    UINT4 *pu4NextFsQoSCoSQId)
{
    INT4                i4RetStatus = QOS_FAILURE;

    if (gQoSGlobalInfo.eSysControl != QOS_SYS_CNTL_START)
    {
        CLI_SET_ERR (QOS_CLI_ERR_MODULE_SHUTDOW);
        QOS_TRC (MGMT_TRC, QOS_MODULE_SHUTDOW_ERROR_MSG);
        return (SNMP_FAILURE);
    }

    i4RetStatus =
        QoSGetNextCoSQStatsTblEntryIndex (i4IfIndex, pi4NextIfIndex,
                                          u4FsQoSCoSQId, pu4NextFsQoSCoSQId);
    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSGetNextCoSQStatsTblEntryIndex ()"
                      " Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    return (SNMP_SUCCESS);

}

/* Low Level GET Routine for All Objects  */
/****************************************************************************
 Function    :  nmhGetFsQoSCoSQStatsEnQPkts
 Input       :  The Indices
                IfIndex
                FsQoSCoSQId

                The Object 
                retValFsQoSCoSQStatsEnQPkts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsQoSCoSQStatsEnQPkts (INT4 i4IfIndex, UINT4 u4FsQoSCoSQId,
                             tSNMP_COUNTER64_TYPE *
                             pu8RetValFsQoSCoSQStatsEnQPkts)
{
    INT4                i4RetStatus = QOS_FAILURE;

    i4RetStatus = QoSHwWrGetCoSQStats (i4IfIndex, u4FsQoSCoSQId,
                                       QOS_COSQ_STATS_ENQ_PKTS,
                                       pu8RetValFsQoSCoSQStatsEnQPkts);
    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG3 (MGMT_TRC, "In %s : ERROR: QoSHwWrGetCoSQStats () "
                      " Returns FAILURE for a Q %d in IfIndex %d Stats type "
                      " of QOS_COSQ_STATS_ENQ_PKTS .\r\n",
                      i4IfIndex, u4FsQoSCoSQId, __FUNCTION__);

        return (SNMP_FAILURE);
    }

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFsQoSCoSQStatsEnQBytes
 Input       :  The Indices
                IfIndex
                FsQoSCoSQId

                The Object 
                retValFsQoSCoSQStatsEnQBytes
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsQoSCoSQStatsEnQBytes (INT4 i4IfIndex, UINT4 u4FsQoSCoSQId,
                              tSNMP_COUNTER64_TYPE *
                              pu8RetValFsQoSCoSQStatsEnQBytes)
{
    INT4                i4RetStatus = QOS_FAILURE;

    i4RetStatus = QoSHwWrGetCoSQStats (i4IfIndex, u4FsQoSCoSQId,
                                       QOS_COSQ_STATS_ENQ_OCTETS,
                                       pu8RetValFsQoSCoSQStatsEnQBytes);
    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG3 (MGMT_TRC, "In %s : ERROR: QoSHwWrGetCoSQStats () "
                      " Returns FAILURE for a Q %d in IfIndex %d Stats type "
                      " of QOS_COSQ_STATS_ENQ_OCTETS .\r\n",
                      i4IfIndex, u4FsQoSCoSQId, __FUNCTION__);

        return (SNMP_FAILURE);
    }

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFsQoSCoSQStatsDeQPkts
 Input       :  The Indices
                IfIndex
                FsQoSCoSQId

                The Object 
                retValFsQoSCoSQStatsDeQPkts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsQoSCoSQStatsDeQPkts (INT4 i4IfIndex, UINT4 u4FsQoSCoSQId,
                             tSNMP_COUNTER64_TYPE *
                             pu8RetValFsQoSCoSQStatsDeQPkts)
{
    INT4                i4RetStatus = QOS_FAILURE;

    i4RetStatus = QoSHwWrGetCoSQStats (i4IfIndex, u4FsQoSCoSQId,
                                       QOS_COSQ_STATS_DEQ_PKTS,
                                       pu8RetValFsQoSCoSQStatsDeQPkts);
    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG3 (MGMT_TRC, "In %s : ERROR: QoSHwWrGetCoSQStats () "
                      " Returns FAILURE for a Q %d in IfIndex %d Stats type "
                      " of QOS_COSQ_STATS_DEQ_PKTS .\r\n",
                      i4IfIndex, u4FsQoSCoSQId, __FUNCTION__);

        return (SNMP_FAILURE);
    }

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFsQoSCoSQStatsDeQBytes
 Input       :  The Indices
                IfIndex
                FsQoSCoSQId

                The Object 
                retValFsQoSCoSQStatsDeQBytes
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsQoSCoSQStatsDeQBytes (INT4 i4IfIndex, UINT4 u4FsQoSCoSQId,
                              tSNMP_COUNTER64_TYPE *
                              pu8RetValFsQoSCoSQStatsDeQBytes)
{
    INT4                i4RetStatus = QOS_FAILURE;

    i4RetStatus = QoSHwWrGetCoSQStats (i4IfIndex, u4FsQoSCoSQId,
                                       QOS_COSQ_STATS_DEQ_OCTETS,
                                       pu8RetValFsQoSCoSQStatsDeQBytes);
    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG3 (MGMT_TRC, "In %s : ERROR: QoSHwWrGetCoSQStats () "
                      " Returns FAILURE for a Q %d in IfIndex %d Stats type "
                      " of QOS_COSQ_STATS_DEQ_OCTETS .\r\n",
                      i4IfIndex, u4FsQoSCoSQId, __FUNCTION__);

        return (SNMP_FAILURE);
    }

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFsQoSCoSQStatsDiscardPkts
 Input       :  The Indices
                IfIndex
                FsQoSCoSQId

                The Object 
                retValFsQoSCoSQStatsDiscardPkts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsQoSCoSQStatsDiscardPkts (INT4 i4IfIndex, UINT4 u4FsQoSCoSQId,
                                 tSNMP_COUNTER64_TYPE *
                                 pu8RetValFsQoSCoSQStatsDiscardPkts)
{
    INT4                i4RetStatus = QOS_FAILURE;

    i4RetStatus = QoSHwWrGetCoSQStats (i4IfIndex, u4FsQoSCoSQId,
                                       QOS_COSQ_STATS_DISCARD_PKTS,
                                       pu8RetValFsQoSCoSQStatsDiscardPkts);
    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG3 (MGMT_TRC, "In %s : ERROR: QoSHwWrGetCoSQStats () "
                      " Returns FAILURE for a Q %d in IfIndex %d Stats type "
                      " of QOS_COSQ_STATS_DISCARD_PKTS .\r\n",
                      i4IfIndex, u4FsQoSCoSQId, __FUNCTION__);

        return (SNMP_FAILURE);
    }

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFsQoSCoSQStatsDiscardBytes
 Input       :  The Indices
                IfIndex
                FsQoSCoSQId

                The Object 
                retValFsQoSCoSQStatsDiscardBytes
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsQoSCoSQStatsDiscardBytes (INT4 i4IfIndex, UINT4 u4FsQoSCoSQId,
                                  tSNMP_COUNTER64_TYPE *
                                  pu8RetValFsQoSCoSQStatsDiscardBytes)
{
    INT4                i4RetStatus = QOS_FAILURE;

    i4RetStatus = QoSHwWrGetCoSQStats (i4IfIndex, u4FsQoSCoSQId,
                                       QOS_COSQ_STATS_DISCARD_OCTETS,
                                       pu8RetValFsQoSCoSQStatsDiscardBytes);
    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG3 (MGMT_TRC, "In %s : ERROR: QoSHwWrGetCoSQStats () "
                      " Returns FAILURE for a Q %d in IfIndex %d Stats type "
                      " of QOS_COSQ_STATS_DISCARD_OCTETS .\r\n",
                      i4IfIndex, u4FsQoSCoSQId, __FUNCTION__);

        return (SNMP_FAILURE);
    }

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFsQoSCoSQStatsOccupancy
 Input       :  The Indices
                IfIndex
                FsQoSCoSQId

                The Object 
                retValFsQoSCoSQStatsOccupancy
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsQoSCoSQStatsOccupancy (INT4 i4IfIndex, UINT4 u4FsQoSCoSQId,
                               tSNMP_COUNTER64_TYPE *
                               pu8RetValFsQoSCoSQStatsOccupancy)
{
    INT4                i4RetStatus = QOS_FAILURE;

    i4RetStatus = QoSHwWrGetCoSQStats (i4IfIndex, u4FsQoSCoSQId,
                                       QOS_COSQ_STATS_OCCUPANCY_BYTES,
                                       pu8RetValFsQoSCoSQStatsOccupancy);
    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG3 (MGMT_TRC, "In %s : ERROR: QoSHwWrGetCoSQStats () "
                      " Returns FAILURE for a Q %d in IfIndex %d Stats type "
                      " of QOS_COSQ_STATS_OCCUPANCY_BYTES .\r\n",
                      i4IfIndex, u4FsQoSCoSQId, __FUNCTION__);

        return (SNMP_FAILURE);
    }

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFsQoSCoSQStatsCongMgntAlgoDrop
 Input       :  The Indices
                IfIndex
                FsQoSCoSQId

                The Object 
                retValFsQoSCoSQStatsCongMgntAlgoDrop
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsQoSCoSQStatsCongMgntAlgoDrop (INT4 i4IfIndex, UINT4 u4FsQoSCoSQId,
                                      tSNMP_COUNTER64_TYPE *
                                      pu8RetValFsQoSCoSQStatsCongMgntAlgoDrop)
{
    INT4                i4RetStatus = QOS_FAILURE;

    i4RetStatus = QoSHwWrGetCoSQStats (i4IfIndex, u4FsQoSCoSQId,
                                       QOS_COSQ_STATS_CONG_MGNT_DROP_BYTES,
                                       pu8RetValFsQoSCoSQStatsCongMgntAlgoDrop);
    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG3 (MGMT_TRC, "In %s : ERROR: QoSHwWrGetCoSQStats () "
                      " Returns FAILURE for a Q %d in IfIndex %d Stats type "
                      " of QOS_COSQ_STATS_CONG_MGNT_DROP_BYTES .\r\n",
                      i4IfIndex, u4FsQoSCoSQId, __FUNCTION__);

        return (SNMP_FAILURE);
    }

    return (SNMP_SUCCESS);
}

/* LOW LEVEL Routines for Table : FsQoSCosHwCpuRateLimitTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsQosHwCpuRateLimitTable
 Input       :  The Indices
                FsQosHwCpuQId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsQosHwCpuRateLimitTable (UINT4 u4FsQosHwCpuQId)
{
    tQosCpuQEntry      *pCpuQNode = NULL;

    if (gQoSGlobalInfo.eSysControl != QOS_SYS_CNTL_START)
    {
        QOS_TRC (MGMT_TRC, QOS_MODULE_SHUTDOW_ERROR_MSG);
        return (SNMP_FAILURE);
    }

    pCpuQNode = QoSUtlGetCpuQNode (u4FsQosHwCpuQId);
    if (pCpuQNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetCpuQNode () Returns "
                      "FAILURE. \r\n", __FUNCTION__);
        return (SNMP_FAILURE);
    }

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsQosHwCpuRateLimitTable
 Input       :  The Indices
                FsQosHwCpuQId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsQosHwCpuRateLimitTable (UINT4 *pu4FsQosHwCpuQId)
{
    INT4                i4RetStatus = QOS_FAILURE;

    if (gQoSGlobalInfo.eSysControl != QOS_SYS_CNTL_START)
    {
        QOS_TRC (MGMT_TRC, QOS_MODULE_SHUTDOW_ERROR_MSG);
        return (SNMP_FAILURE);
    }

    i4RetStatus = QoSUtlGetNextCpuQTblEntryIndex (0, pu4FsQosHwCpuQId);
    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetNextCpuQTblEntryIndex () "
                      "Returns FAILURE. \r\n", __FUNCTION__);
        return (SNMP_FAILURE);
    }

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsQosHwCpuRateLimitTable
 Input       :  The Indices
                FsQosHwCpuQId
                nextFsQosHwCpuQId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsQosHwCpuRateLimitTable (UINT4 u4FsQosHwCpuQId,
                                         UINT4 *pu4NextFsQosHwCpuQId)
{
    INT4                i4RetStatus = QOS_FAILURE;

    if (gQoSGlobalInfo.eSysControl != QOS_SYS_CNTL_START)
    {
        QOS_TRC (MGMT_TRC, QOS_MODULE_SHUTDOW_ERROR_MSG);
        return (SNMP_FAILURE);
    }

    i4RetStatus = QoSUtlGetNextCpuQTblEntryIndex (u4FsQosHwCpuQId,
                                                  pu4NextFsQosHwCpuQId);
    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetNextCpuQTblEntryIndex () "
                      "Returns FAILURE. \r\n", __FUNCTION__);
        return (SNMP_FAILURE);
    }

    return (SNMP_SUCCESS);
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsQosHwCpuMinRate
 Input       :  The Indices
                FsQosHwCpuQId

                The Object 
                retValFsQosHwCpuMinRate
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsQosHwCpuMinRate (UINT4 u4FsQosHwCpuQId,
                         UINT4 *pu4RetValFsQosHwCpuMinRate)
{
    tQosCpuQEntry      *pCpuQNode = NULL;

    pCpuQNode = QoSUtlGetCpuQNode (u4FsQosHwCpuQId);
    if (pCpuQNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetCpuQNode () "
                      "Returns FAILURE. \r\n", __FUNCTION__);
        return (SNMP_FAILURE);
    }

    *pu4RetValFsQosHwCpuMinRate = pCpuQNode->u4CpuQMinRate;

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFsQosHwCpuMaxRate
 Input       :  The Indices
                FsQosHwCpuQId

                The Object 
                retValFsQosHwCpuMaxRate
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsQosHwCpuMaxRate (UINT4 u4FsQosHwCpuQId,
                         UINT4 *pu4RetValFsQosHwCpuMaxRate)
{
    tQosCpuQEntry      *pCpuQNode = NULL;

    pCpuQNode = QoSUtlGetCpuQNode (u4FsQosHwCpuQId);
    if (pCpuQNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetCpuQNode () "
                      "Returns FAILURE. \r\n", __FUNCTION__);
        return (SNMP_FAILURE);
    }

    *pu4RetValFsQosHwCpuMaxRate = pCpuQNode->u4CpuQMaxRate;

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFsQosHwCpuRowStatus
 Input       :  The Indices
                FsQosHwCpuQId

                The Object 
                retValFsQosHwCpuRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsQosHwCpuRowStatus (UINT4 u4FsQosHwCpuQId,
                           INT4 *pi4RetValFsQosHwCpuRowStatus)
{
    tQosCpuQEntry      *pCpuQNode = NULL;

    pCpuQNode = QoSUtlGetCpuQNode (u4FsQosHwCpuQId);
    if (pCpuQNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetCpuQNode () "
                      "Returns FAILURE. \r\n", __FUNCTION__);
        return (SNMP_FAILURE);
    }

    *pi4RetValFsQosHwCpuRowStatus = pCpuQNode->u1CpuQStatus;

    return (SNMP_SUCCESS);
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsQosHwCpuMinRate
 Input       :  The Indices
                FsQosHwCpuQId

                The Object 
                setValFsQosHwCpuMinRate
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsQosHwCpuMinRate (UINT4 u4FsQosHwCpuQId, UINT4 u4SetValFsQosHwCpuMinRate)
{
    tQosCpuQEntry      *pCpuQNode = NULL;

    pCpuQNode = QoSUtlGetCpuQNode (u4FsQosHwCpuQId);
    if (pCpuQNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetCpuQNode () "
                      "Returns FAILURE. \r\n", __FUNCTION__);
        return (SNMP_FAILURE);
    }

    pCpuQNode->u4CpuQMinRate = u4SetValFsQosHwCpuMinRate;

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhSetFsQosHwCpuMaxRate
 Input       :  The Indices
                FsQosHwCpuQId

                The Object 
                setValFsQosHwCpuMaxRate
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsQosHwCpuMaxRate (UINT4 u4FsQosHwCpuQId, UINT4 u4SetValFsQosHwCpuMaxRate)
{
    tQosCpuQEntry      *pCpuQNode = NULL;

    pCpuQNode = QoSUtlGetCpuQNode (u4FsQosHwCpuQId);
    if (pCpuQNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetCpuQNode () "
                      "Returns FAILURE. \r\n", __FUNCTION__);
        return (SNMP_FAILURE);
    }

    pCpuQNode->u4CpuQMaxRate = u4SetValFsQosHwCpuMaxRate;

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhSetFsQosHwCpuRowStatus
 Input       :  The Indices
                FsQosHwCpuQId

                The Object 
                setValFsQosHwCpuRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsQosHwCpuRowStatus (UINT4 u4FsQosHwCpuQId,
                           INT4 i4SetValFsQosHwCpuRowStatus)
{
    tQosCpuQEntry      *pCpuQNode = NULL;
    INT4                i4ReturnValue = SNMP_SUCCESS;

    pCpuQNode = QoSUtlGetCpuQNode (u4FsQosHwCpuQId);
    if (pCpuQNode != NULL)
    {
        /* Entry found in the Table */
        if (pCpuQNode->u1CpuQStatus == i4SetValFsQosHwCpuRowStatus)
        {
            /* same value */
            return (SNMP_SUCCESS);
        }
    }

    switch (i4SetValFsQosHwCpuRowStatus)
    {
        case CREATE_AND_WAIT:

            QOS_TRC_ARG1 (MGMT_TRC, "In %s : CreateAndWait"
                          " not allowed. \r\n", __FUNCTION__);
            i4ReturnValue = SNMP_FAILURE;

            break;

        case ACTIVE:
            i4ReturnValue = SNMP_FAILURE;
            if (pCpuQNode != NULL)
            {
                /* Program the Hardware */
                if (QoSHwWrSetCpuRateLimit ((INT4) u4FsQosHwCpuQId,
                                            pCpuQNode->u4CpuQMinRate,
                                            pCpuQNode->u4CpuQMaxRate)
                    == QOS_FAILURE)
                {
                    QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSHwWrSetCpuRateLimit "
                                  " () Returns FAILURE. \r\n", __FUNCTION__);
                    return (SNMP_FAILURE);
                }

                pCpuQNode->u1CpuQStatus = ACTIVE;
                i4ReturnValue = SNMP_SUCCESS;
            }
            break;

        case NOT_IN_SERVICE:

            if (pCpuQNode != NULL)
            {
                if ((pCpuQNode->u1CpuQStatus == ACTIVE) &&
                    (gQoSGlobalInfo.eSysStatus == QOS_SYS_STATUS_ENABLE))
                {
                    pCpuQNode->u1CpuQStatus = NOT_IN_SERVICE;
                }
            }
            break;

        case DESTROY:
            QOS_TRC_ARG1 (MGMT_TRC, "In %s : Destroy is"
                          " not allowed. \r\n", __FUNCTION__);
            i4ReturnValue = SNMP_FAILURE;
            break;

        case CREATE_AND_GO:
            i4ReturnValue = SNMP_FAILURE;
            break;

        default:
            i4ReturnValue = SNMP_FAILURE;
            break;
    }

    return ((INT1) i4ReturnValue);
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsQosHwCpuMinRate
 Input       :  The Indices
                FsQosHwCpuQId

                The Object 
                testValFsQosHwCpuMinRate
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsQosHwCpuMinRate (UINT4 *pu4ErrorCode, UINT4 u4FsQosHwCpuQId,
                            UINT4 u4TestValFsQosHwCpuMinRate)
{
    tQosCpuQEntry      *pCpuQNode = NULL;

    *pu4ErrorCode = SNMP_ERR_NO_ERROR;

    if (gQoSGlobalInfo.eSysControl != QOS_SYS_CNTL_START)
    {
        QOS_TRC (MGMT_TRC, QOS_MODULE_SHUTDOW_ERROR_MSG);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (QOS_CLI_ERR_MODULE_SHUTDOW);

        return (QOS_FAILURE);
    }

    pCpuQNode = QoSUtlGetCpuQNode (u4FsQosHwCpuQId);
    if (pCpuQNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetCpuQNode () Returns "
                      "FAILURE. \r\n", __FUNCTION__);

        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (QOS_CLI_ERR_Q_NO_ENTRY);
        return (QOS_FAILURE);
    }

    if (pCpuQNode->u1CpuQStatus == ACTIVE)
    {
        QOS_TRC (MGMT_TRC, "Can not change the value in ACTIVE State.\r\n");

        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (QOS_CLI_ERR_STATUS_ACTIVE);
        return (QOS_FAILURE);
    }

    /* Check  Object's Value */
    if (u4TestValFsQosHwCpuMinRate < QOS_DEF_CPU_MIN_RATE)
    {
        QOS_TRC_ARG3 (MGMT_TRC, "%s : CpuQ MinRate is out of range. The"
                      " range should be Min %d - Max %d. \r\n", __FUNCTION__,
                      QOS_DEF_CPU_MIN_RATE, QOS_DEF_CPU_MAX_RATE);

        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (QOS_CLI_ERR_CPUQ_RATE_RANGE);
        return (SNMP_FAILURE);
    }

    if (u4TestValFsQosHwCpuMinRate > pCpuQNode->u4CpuQMaxRate)
    {
        QOS_TRC_ARG1 (MGMT_TRC,
                      "%s : CpuQ MinRate is greater than MaxRate \r\n",
                      __FUNCTION__);

        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (QOS_CLI_ERR_CPUQ_MIN_GRT_MAX);
        return (SNMP_FAILURE);
    }

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhTestv2FsQosHwCpuMaxRate
 Input       :  The Indices
                FsQosHwCpuQId

                The Object 
                testValFsQosHwCpuMaxRate
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsQosHwCpuMaxRate (UINT4 *pu4ErrorCode, UINT4 u4FsQosHwCpuQId,
                            UINT4 u4TestValFsQosHwCpuMaxRate)
{
    tQosCpuQEntry      *pCpuQNode = NULL;

    *pu4ErrorCode = SNMP_ERR_NO_ERROR;

    if (gQoSGlobalInfo.eSysControl != QOS_SYS_CNTL_START)
    {
        QOS_TRC (MGMT_TRC, QOS_MODULE_SHUTDOW_ERROR_MSG);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (QOS_CLI_ERR_MODULE_SHUTDOW);

        return (QOS_FAILURE);
    }

    pCpuQNode = QoSUtlGetCpuQNode (u4FsQosHwCpuQId);
    if (pCpuQNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetCpuQNode () Returns "
                      "FAILURE. \r\n", __FUNCTION__);

        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (QOS_CLI_ERR_Q_NO_ENTRY);
        return (QOS_FAILURE);
    }

    if (pCpuQNode->u1CpuQStatus == ACTIVE)
    {
        QOS_TRC (MGMT_TRC, "Can not change the value in ACTIVE State.\r\n");

        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (QOS_CLI_ERR_STATUS_ACTIVE);
        return (QOS_FAILURE);
    }

    /* Check  Object's Value */
    if (u4TestValFsQosHwCpuMaxRate > QOS_DEF_CPU_MAX_RATE)
    {
        QOS_TRC_ARG3 (MGMT_TRC, "%s : CpuQ MaxRate is out of range. The"
                      " range should be Min %d - Max %d. \r\n", __FUNCTION__,
                      QOS_DEF_CPU_MIN_RATE, QOS_DEF_CPU_MAX_RATE);

        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (QOS_CLI_ERR_CPUQ_RATE_RANGE);
        return (SNMP_FAILURE);
    }

    if (u4TestValFsQosHwCpuMaxRate < pCpuQNode->u4CpuQMinRate)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "%s : CpuQ MaxRate is lesser than MinRate \r\n",
                      __FUNCTION__);

        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (QOS_CLI_ERR_CPUQ_MAX_LES_MIN);
        return (SNMP_FAILURE);
    }

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhTestv2FsQosHwCpuRowStatus
 Input       :  The Indices
                FsQosHwCpuQId

                The Object 
                testValFsQosHwCpuRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsQosHwCpuRowStatus (UINT4 *pu4ErrorCode, UINT4 u4FsQosHwCpuQId,
                              INT4 i4TestValFsQosHwCpuRowStatus)
{
    tQosCpuQEntry      *pCpuQNode = NULL;
    INT4                i4ReturnValue = SNMP_SUCCESS;

    if (gQoSGlobalInfo.eSysControl != QOS_SYS_CNTL_START)
    {
        CLI_SET_ERR (QOS_CLI_ERR_MODULE_SHUTDOW);
        QOS_TRC (MGMT_TRC, QOS_MODULE_SHUTDOW_ERROR_MSG);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return (SNMP_FAILURE);
    }

    *pu4ErrorCode = SNMP_ERR_NO_ERROR;

    /* Validate the index value (Min-Max) */
    if (u4FsQosHwCpuQId > QOS_QUEUE_ENTRY_MAX)
    {
        QOS_TRC_ARG3 (MGMT_TRC, "%s : Q Id %d is out of Range."
                      " The Range Should be (1-%d). \r\n", __FUNCTION__,
                      u4FsQosHwCpuQId, QOS_QUEUE_ENTRY_MAX);
        CLI_SET_ERR (QOS_CLI_ERR_Q_RANGE);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;

        return (SNMP_FAILURE);
    }

    pCpuQNode = QoSUtlGetCpuQNode (u4FsQosHwCpuQId);
    if (pCpuQNode == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    /* Entry Found In The Table */
    switch (i4TestValFsQosHwCpuRowStatus)
    {
        case ACTIVE:

            if ((pCpuQNode->u1CpuQStatus == NOT_READY))
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return (SNMP_FAILURE);
            }
            i4ReturnValue = SNMP_SUCCESS;
            break;

        case NOT_IN_SERVICE:

            if ((pCpuQNode->u1CpuQStatus == NOT_READY))
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return (SNMP_FAILURE);
            }
            i4ReturnValue = SNMP_SUCCESS;
            break;

        case DESTROY:
        case CREATE_AND_WAIT:
        case CREATE_AND_GO:
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            i4ReturnValue = SNMP_FAILURE;
            break;

        default:
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            i4ReturnValue = SNMP_FAILURE;
            break;

    }                            /* End of switch */

    return ((INT1) i4ReturnValue);
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsQosHwCpuRateLimitTable
 Input       :  The Indices
                FsQosHwCpuQId
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsQosHwCpuRateLimitTable (UINT4 *pu4ErrorCode,
                                  tSnmpIndexList * pSnmpIndexList,
                                  tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  QosxNotifyProtocolShutdownStatus
 Input       :  None
 Output      :  None
 Returns     :  None
****************************************************************************/

VOID
QosxNotifyProtocolShutdownStatus (VOID)
{
#ifdef ISS_WANTED
    UINT1               au1ObjectOid[2][SNMP_MAX_OID_LENGTH];

    /* The oid list contains the list of Oids that has been registered 
     * for the protocol + the oid of the SystemControl object. */
    SNMPGetOidString (fsqosx, (sizeof (fsqosx) / sizeof (UINT4)),
                      au1ObjectOid[0]);
    SNMPGetOidString (FsQoSSystemControl,
                      (sizeof (FsQoSSystemControl) / sizeof (UINT4)),
                      au1ObjectOid[1]);

    /* Send a notification to MSR to process the Qosxtd shutdown, with 
     * Qosxtd oid and its system control object 
     * As the protocol does not have MI support, invalid context id is sent */
    MSR_NOTIFY_PROTOCOL_SHUT (au1ObjectOid, 2, MSR_INVALID_CNTXT);
#endif
    return;
}

/****************************************************************************
 Function    :  nmhGetFsQoSDscpQueueMap
 Input       :  u4QoSDscpVal
                u4QoSQueueID
 Output      :  None
 Returns     :  SNMP_FAILURE/SNMP_SUCCESS
****************************************************************************/

INT1
nmhGetFsQoSDscpQueueMap (INT4 i4QosDscpVal, INT4 *i4QoSQueueID)
{

    INT4                i4RetStats = QOS_FAILURE;

    i4RetStats = QoSHwWrGetDscpQueueMap (i4QosDscpVal, i4QoSQueueID);

    if (i4RetStats != QOS_SUCCESS)
    {
        QOS_TRC_ARG2 (MGMT_TRC, "In %s : QoSHwWrMapClasstoPriMap () ",
                      "Returns FAILURE. \r\n", __FUNCTION__);
        return (SNMP_FAILURE);
    }

    return SNMP_SUCCESS;
}

/* VLAN BASED SHAPING */

/****************************************************************************
 Function    :  nmhGetFsQoSVlanQueueingStatus
 Input       :  The Indices

                The Object
                retValFsQoSVlanQueueingStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsQoSVlanQueueingStatus (INT4 *pi4RetValFsQoSVlanQueueingStatus)
{

    *pi4RetValFsQoSVlanQueueingStatus = gQoSGlobalInfo.eVlanQStatus;

    QOS_TRC_ARG2 (MGMT_TRC, "%s : System Status = %s SUCCESS \r\n",
                  __FUNCTION__,
                  (*pi4RetValFsQoSVlanQueueingStatus) ==
                  1 ? "Enabled " : "Disabled");
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhSetFsQoSVlanQueueingStatus
 Input       :  The Indices

                The Object
                setValFsQoSVlanQueueingStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsQoSVlanQueueingStatus (INT4 i4SetValFsQoSVlanQueueingStatus)
{

    INT4                i4RetStats = QOS_FAILURE;

    /* Check for current status and the value being passed */
    if (gQoSGlobalInfo.eVlanQStatus == (UINT4) i4SetValFsQoSVlanQueueingStatus)
    {
        QOS_TRC_ARG2 (MGMT_TRC, "%s : Already in the same status "
                      "System Status = %d SUCCESS \r\n",
                      __FUNCTION__, i4SetValFsQoSVlanQueueingStatus);
        return (SNMP_SUCCESS);
    }

    /* Enabling global status for Vlan Queueing */
    if (VLAN_QUEUEING_SYS_STATUS_ENABLE == i4SetValFsQoSVlanQueueingStatus)
    {
        /* Subscriber queus should be attached before enabling Vlan Queueing.
         * This will be configured only once after boot up and sets a flag to
         * indicate the status. */

        /* The schema and queue creation will be programmed to Hardware only
         * after MSR. The subscriber queues will be created and attached to
         * ports during next boot up */

        if (MsrIsMibRestoreInProgress () == MSR_TRUE)
        {
            i4RetStats = QosUtilCreateSubscriberQueues ();
            if (i4RetStats != QOS_SUCCESS)
            {
                QOS_TRC_ARG1 (MGMT_TRC,
                              "In %s : QosUtilCreateSubscriberQueues () - FAILED. \r\n",
                              __FUNCTION__);
                return (SNMP_FAILURE);
            }
        }
        i4RetStats = QoSVlanQueueingEnable ();
        if (i4RetStats != QOS_SUCCESS)
        {
            QOS_TRC_ARG1 (MGMT_TRC,
                          "In %s : QoSVlanQueueingEnable () - FAILED. \r\n",
                          __FUNCTION__);
            return (SNMP_FAILURE);
        }
    }

    /* Disabling global status of Vlan Queuing.
     * This will remove the vlan-map configurations
     * from the hardware and preserves the entries
     * in software */
    else
    {
        i4RetStats = QoSVlanQueueingDisable ();

        if (i4RetStats != QOS_SUCCESS)
        {
            QOS_TRC_ARG1 (MGMT_TRC,
                          "In %s : QoSVlanQueueingDisable () - FAILED.\r\n",
                          __FUNCTION__);
            return (SNMP_FAILURE);
        }
    }
    /* Assiging new status to global variable */
    gQoSGlobalInfo.eVlanQStatus = i4SetValFsQoSVlanQueueingStatus;

    QOS_TRC_ARG2 (MGMT_TRC, "%s : System Status = %d SUCCESS.\r\n",
                  __FUNCTION__, i4SetValFsQoSVlanQueueingStatus);
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhTestv2FsQoSVlanQueueingStatus
 Input       :  The Indices

                The Object
                testValFsQoSVlanQueueingStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsQoSVlanQueueingStatus (UINT4 *pu4ErrorCode,
                                  INT4 i4TestValFsQoSVlanQueueingStatus)
{

    /* check for QoS module status
     * if QoS module is diabled/shutdown globally
     * then return failure */

    if (gQoSGlobalInfo.eSysControl != QOS_SYS_CNTL_START)
    {
        QOS_TRC (MGMT_TRC, QOS_MODULE_SHUTDOW_ERROR_MSG);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (QOS_CLI_ERR_MODULE_SHUTDOW);
        return (SNMP_FAILURE);
    }

    /* Check if Hierarchial scheduling is supported.
     * If HL is not supported Do no enable Vlan queuing */
    if (QOS_HL_DEF_SCHED_CFG_SUPPORTED != gu1HLSupportFlag)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (QOS_CLI_ERR_VLAN_MAP_HL_NOT_SUPPORTED);
        return (SNMP_FAILURE);
    }

    /* Test for the input */
    if ((i4TestValFsQoSVlanQueueingStatus != VLAN_QUEUEING_SYS_STATUS_DISABLE)
        && (i4TestValFsQoSVlanQueueingStatus !=
            VLAN_QUEUEING_SYS_STATUS_ENABLE))
    {
        QOS_TRC_ARG2 (MGMT_TRC, "%s : System Status %d FAILED.\r\n",
                      __FUNCTION__, i4TestValFsQoSVlanQueueingStatus);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (QOS_CLI_ERR_INVALID_SYS_STATUS);
        return (SNMP_FAILURE);
    }
    QOS_TRC_ARG2 (MGMT_TRC, "%s : System Status = %d SUCCESS.\r\n",
                  __FUNCTION__, i4TestValFsQoSVlanQueueingStatus);
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhTestv2FsQoSClassMapVlanMapId
 Input       :  The Indices
                FsQoSClassMapId

                The Object
                testValFsQoSClassMapVlanMapId
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsQoSClassMapVlanMapId (UINT4 *pu4ErrorCode, UINT4 u4FsQoSClassMapId,
                                 UINT4 u4TestValFsQoSClassMapVlanMapId)
{

    tQoSClassMapNode   *pClsMapNode = NULL;
    INT4                i4RetStatus = QOS_FAILURE;

    *pu4ErrorCode = SNMP_ERR_NO_ERROR;
    /* Check Index */
    i4RetStatus = QoSUtlValidateClsMapTblIdxInst (u4FsQoSClassMapId,
                                                  pu4ErrorCode);
    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlValidateClsMapTblIdxInst () "
                      "Returns FAILURE. \r\n", __FUNCTION__);
        return (SNMP_FAILURE);
    }

    /* Reset VlanMapId */
    if (u4TestValFsQoSClassMapVlanMapId == QOS_ROWPOINTER_DEF)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return (SNMP_FAILURE);
    }

    /* Check  Object's Value */
    i4RetStatus =
        QoSUtlValidateVlanQMapId (u4TestValFsQoSClassMapVlanMapId,
                                  pu4ErrorCode);
    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlValidateVlanQMapId () "
                      "Returns FAILURE. \r\n", __FUNCTION__);
        return (SNMP_FAILURE);
    }
    pClsMapNode = QoSUtlGetClassMapNode (u4FsQoSClassMapId);
    if (pClsMapNode == NULL)
    {
        CLI_SET_ERR (QOS_CLI_ERR_CLS_MAP_NO_ENTRY);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (SNMP_FAILURE);
    }

    /* Check L2 and L3 Fields Should be NULL */
    if ((pClsMapNode->u4L2FilterId != 0) || (pClsMapNode->u4L3FilterId != 0)
        || (pClsMapNode->u4PriorityMapId != 0))
    {
        QOS_TRC_ARG1 (MGMT_TRC, "%s :Either L2 and/or L3 Filters or "
                      "PriMapId is associated with this class-map .\r\n",
                      __FUNCTION__);
        CLI_SET_ERR (QOS_CLI_ERR_L2_L3_OR_PRI);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsQoSVlanMapTable
 Input       :  The Indices
                FsQoSVlanMapID
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsQoSVlanMapTable (UINT4 u4FsQoSVlanMapID)
{
    INT4                i4RetStatus = 0;
    tQoSInVlanMapNode  *pVlanQMapNode = NULL;

    if (gQoSGlobalInfo.eSysControl != QOS_SYS_CNTL_START)
    {
        QOS_TRC (MGMT_TRC, QOS_MODULE_SHUTDOW_ERROR_MSG);
        return (SNMP_FAILURE);
    }

    /* Check if Hierarchial scheduling is supported.
     * If HL is not supported Do no enable Vlan queuing */
    if (QOS_HL_DEF_SCHED_CFG_SUPPORTED != gu1HLSupportFlag)
    {
        CLI_SET_ERR (QOS_CLI_ERR_VLAN_MAP_HL_NOT_SUPPORTED);
        return (SNMP_FAILURE);
    }

    /* Check the Range of Index Values (Min-Max) */
    i4RetStatus = QOS_CHECK_VLAN_MAP_TBL_INDEX_RANGE (u4FsQoSVlanMapID);

    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG3 (MGMT_TRC, "%s : Vlan Map Id %d is out of Range."
                      " The Range Should be (1-%d). \r\n", __FUNCTION__,
                      u4FsQoSVlanMapID, QOS_VLAN_MAP_TBL_MAX_INDEX_RANGE);
        return (SNMP_FAILURE);
    }

    pVlanQMapNode = QoSUtlGetVlanQMapNode (u4FsQoSVlanMapID);

    if (pVlanQMapNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetVlanQMapNode () Returns "
                      "FAILURE. \r\n", __FUNCTION__);
        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhDepv2FsQoSVlanQueueingStatus
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsQoSVlanQueueingStatus (UINT4 *pu4ErrorCode,
                                 tSnmpIndexList * pSnmpIndexList,
                                 tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsQoSVlanMapTable
 Input       :  The Indices
                FsQoSVlanMapID
                nextFsQoSVlanMapID
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsQoSVlanMapTable (UINT4 u4FsQoSVlanMapID,
                                  UINT4 *pu4NextFsQoSVlanMapID)
{

    INT4                i4RetStatus = QOS_FAILURE;

    if ((gQoSGlobalInfo.eSysControl != QOS_SYS_CNTL_START) &&
        (gQoSGlobalInfo.eVlanQStatus != VLAN_QUEUEING_SYS_STATUS_ENABLE))
    {
        QOS_TRC (MGMT_TRC, QOS_MODULE_SHUTDOW_ERROR_MSG);
        return (SNMP_FAILURE);
    }

    /* Check the Range of Index Values (Min-Max) */
    i4RetStatus = QOS_CHECK_VLAN_MAP_TBL_INDEX_RANGE (u4FsQoSVlanMapID);

    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG3 (MGMT_TRC,
                      "%s :  u4FsQoSVlanMapID Map Id %d is out of Range."
                      " The Range Should be (1-%d). \r\n", __FUNCTION__,
                      u4FsQoSVlanMapID, QOS_VLAN_MAP_TBL_MAX_INDEX_RANGE);
        return (SNMP_FAILURE);
    }

    i4RetStatus =
        QoSGetNextVlanQMapTblEntryIndex (u4FsQoSVlanMapID,
                                         pu4NextFsQoSVlanMapID);
    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSGetNextVlanQMapTblEntryIndex () "
                      "Returns FAILURE. \r\n", __FUNCTION__);
        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);
}

/****************************************************************************
*                 VLAN QUEUEING MAP TABLE FUNCTIONS
*****************************************************************************/

/****************************************************************************
 Function    :  nmhGetFirstIndexFsQoSVlanMapTable
 Input       :  The Indices
                FsQoSVlanMapID
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsQoSVlanMapTable (UINT4 *pu4FsQoSVlanMapID)
{
    INT4                i4RetStatus = QOS_FAILURE;

    if ((gQoSGlobalInfo.eSysControl != QOS_SYS_CNTL_START) &&
        (gQoSGlobalInfo.eVlanQStatus != VLAN_QUEUEING_SYS_STATUS_ENABLE))
    {
        QOS_TRC (MGMT_TRC, QOS_MODULE_SHUTDOW_ERROR_MSG);
        return (SNMP_FAILURE);
    }

    i4RetStatus = QoSGetNextVlanQMapTblEntryIndex (0, pu4FsQoSVlanMapID);

    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSGetNextVlanQMapTblEntryIndex () "
                      "Returns FAILURE. \r\n", __FUNCTION__);
        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFsQoSVlanMapIfIndex
 Input       :  The Indices
                FsQoSVlanMapID

                The Object
                retValFsQoSVlanMapIfIndex
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsQoSVlanMapIfIndex (UINT4 u4FsQoSVlanMapID,
                           UINT4 *pu4RetValFsQoSVlanMapIfIndex)
{
    tQoSInVlanMapNode  *pVlanQMapNode = NULL;

    pVlanQMapNode = QoSUtlGetVlanQMapNode (u4FsQoSVlanMapID);
    if (pVlanQMapNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetVlanQMapNode () "
                      "Returns FAILURE. \r\n", __FUNCTION__);
        return (SNMP_FAILURE);
    }
    *pu4RetValFsQoSVlanMapIfIndex = pVlanQMapNode->u4IfIndex;
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFsQoSVlanMapVlanId
 Input       :  The Indices
                FsQoSVlanMapID

                The Object
                retValFsQoSVlanMapVlanId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsQoSVlanMapVlanId (UINT4 u4FsQoSVlanMapID,
                          UINT4 *pu4RetValFsQoSVlanMapVlanId)
{

    tQoSInVlanMapNode  *pVlanQMapNode = NULL;

    pVlanQMapNode = QoSUtlGetVlanQMapNode (u4FsQoSVlanMapID);

    if (pVlanQMapNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetPriorityMapNode () "
                      "Returns FAILURE. \r\n", __FUNCTION__);
        return (SNMP_FAILURE);
    }
    *pu4RetValFsQoSVlanMapVlanId = (UINT4) (pVlanQMapNode->u2VlanId);
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFsQoSVlanMapStatus
 Input       :  The Indices
                FsQoSVlanMapID

                The Object
                retValFsQoSVlanMapStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsQoSVlanMapStatus (UINT4 u4FsQoSVlanMapID,
                          INT4 *pi4RetValFsQoSVlanMapStatus)
{
    tQoSInVlanMapNode  *pVlanQMapNode = NULL;

    pVlanQMapNode = QoSUtlGetVlanQMapNode (u4FsQoSVlanMapID);

    if (pVlanQMapNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetVlanQMapNode () "
                      "Returns FAILURE. \r\n", __FUNCTION__);
        return (SNMP_FAILURE);
    }
    *pi4RetValFsQoSVlanMapStatus = (INT4) (pVlanQMapNode->u1Status);
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhSetFsQoSVlanMapIfIndex
 Input       :  The Indices
                FsQoSVlanMapID

                The Object
                setValFsQoSVlanMapIfIndex
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsQoSVlanMapIfIndex (UINT4 u4FsQoSVlanMapID,
                           UINT4 u4SetValFsQoSVlanMapIfIndex)
{
    tQoSInVlanMapNode  *pVlanQMapNode = NULL;

    pVlanQMapNode = QoSUtlGetVlanQMapNode (u4FsQoSVlanMapID);
    if (pVlanQMapNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetVlanQMapNode () "
                      "Returns FAILURE. \r\n", __FUNCTION__);
        return (SNMP_FAILURE);
    }
    pVlanQMapNode->u4IfIndex = u4SetValFsQoSVlanMapIfIndex;
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhSetFsQoSVlanMapVlanId
 Input       :  The Indices
                FsQoSVlanMapID

                The Object
                setValFsQoSVlanMapVlanId
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsQoSVlanMapVlanId (UINT4 u4FsQoSVlanMapID,
                          UINT4 u4SetValFsQoSVlanMapVlanId)
{
    tQoSInVlanMapNode  *pVlanQMapNode = NULL;

    pVlanQMapNode = QoSUtlGetVlanQMapNode (u4FsQoSVlanMapID);
    if (pVlanQMapNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetVlanQMapNode () "
                      "Returns FAILURE. \r\n", __FUNCTION__);
        return (SNMP_FAILURE);
    }

    pVlanQMapNode->u2VlanId = (UINT2) u4SetValFsQoSVlanMapVlanId;
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhSetFsQoSVlanMapStatus
 Input       :  The Indices
                FsQoSVlanMapID

                The Object
                setValFsQoSVlanMapStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsQoSVlanMapStatus (UINT4 u4FsQoSVlanMapID,
                          INT4 i4SetValFsQoSVlanMapStatus)
{

    tQoSInVlanMapNode  *pVlanQMapNode = NULL;
    INT4                i4RetStatus = QOS_FAILURE;
    INT4                i4RowStatus = QOS_FAILURE;
    INT4                i4ReturnValue = SNMP_SUCCESS;

    pVlanQMapNode = QoSUtlGetVlanQMapNode (u4FsQoSVlanMapID);
    if (pVlanQMapNode != NULL)
    {
        i4RowStatus = (UINT4) pVlanQMapNode->u1Status;
        /* Entry found in the Table */
        if (i4RowStatus == i4SetValFsQoSVlanMapStatus)
        {
            /* same value */
            return (SNMP_SUCCESS);
        }
    }
    else
    {
        /* New Entry need to be created in the Table, only if
         * i4TestValFsQoSVlanMapStatus = CREATE_AND_WAIT */
        /* Optional */
        if (i4SetValFsQoSVlanMapStatus == DESTROY)
        {
            return (SNMP_SUCCESS);
        }
    }

    switch (i4SetValFsQoSVlanMapStatus)
    {
            /* For a New Entry */
        case CREATE_AND_WAIT:

            pVlanQMapNode = QoSCreateVlanQMapTblEntry (u4FsQoSVlanMapID);

            if (pVlanQMapNode == NULL)
            {
                QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSCreateVlanQMapTblEntry () "
                              "Returns FAILURE. \r\n", __FUNCTION__);

                return (SNMP_FAILURE);
            }
            if (pVlanQMapNode != NULL)
            {
                pVlanQMapNode->u1Status = NOT_IN_SERVICE;
            }
            i4ReturnValue = SNMP_SUCCESS;
            break;

            /* Modify an Entry */
        case ACTIVE:

            if (pVlanQMapNode != NULL)
            {
                pVlanQMapNode->u1Status = ACTIVE;
            }
            break;

        case NOT_IN_SERVICE:

            if (pVlanQMapNode != NULL)
            {
                pVlanQMapNode->u1Status = NOT_IN_SERVICE;
            }
            break;

        case DESTROY:

            i4RetStatus = QoSDeleteVlanQMapTblEntry (pVlanQMapNode);
            if (i4RetStatus == QOS_FAILURE)
            {
                QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSDeleteVlanQMapTblEntry () "
                              "Returns FAILURE. \r\n", __FUNCTION__);
                return (SNMP_FAILURE);
            }
            break;

            /* Not Supported */
        case CREATE_AND_GO:
            i4ReturnValue = SNMP_FAILURE;
            break;

        default:
            i4ReturnValue = SNMP_FAILURE;
            break;
    }
    return ((INT1) i4ReturnValue);
}

/****************************************************************************
 Function    :  nmhTestv2FsQoSVlanMapStatus
 Input       :  The Indices
                FsQoSVlanMapID

                The Object
                testValFsQoSVlanMapStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsQoSVlanMapStatus (UINT4 *pu4ErrorCode, UINT4 u4FsQoSVlanMapID,
                             INT4 i4TestValFsQoSVlanMapStatus)
{
    tQoSInVlanMapNode  *pVlanQMapNode = NULL;

    UINT4               u4Count = 0;
    INT4                i4RetStatus = QOS_FAILURE;
    INT4                i4ReturnValue = SNMP_SUCCESS;

    if (gQoSGlobalInfo.eSysControl != QOS_SYS_CNTL_START)
    {
        QOS_TRC (MGMT_TRC, QOS_MODULE_SHUTDOW_ERROR_MSG);
        CLI_SET_ERR (QOS_CLI_ERR_MODULE_SHUTDOW);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return (SNMP_FAILURE);
    }

    /* Check if Hierarchial scheduling is supported.
     * If HL is not supported Do no enable Vlan queuing */
    if (QOS_HL_DEF_SCHED_CFG_SUPPORTED != gu1HLSupportFlag)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (QOS_CLI_ERR_VLAN_MAP_HL_NOT_SUPPORTED);
        return (SNMP_FAILURE);
    }

    *pu4ErrorCode = SNMP_ERR_NO_ERROR;

    /* Check the Range of Index Values (Min-Max) */
    i4RetStatus = QOS_CHECK_VLAN_MAP_TBL_INDEX_RANGE (u4FsQoSVlanMapID);
    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG3 (MGMT_TRC, "%s : Vlan Map Id %d is out of Range."
                      " The Range Should be (1-%d). \r\n", __FUNCTION__,
                      u4FsQoSVlanMapID, QOS_VLAN_MAP_TBL_MAX_INDEX_RANGE);
        CLI_SET_ERR (QOS_CLI_ERR_VLAN_MAP_RANGE);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }

    pVlanQMapNode = QoSUtlGetVlanQMapNode (u4FsQoSVlanMapID);

    if (pVlanQMapNode == NULL)
    {
        /* Entry Not Found In The Table */
        switch (i4TestValFsQoSVlanMapStatus)
        {
            case CREATE_AND_GO:
            case ACTIVE:
            case NOT_IN_SERVICE:
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                i4ReturnValue = SNMP_FAILURE;
                break;

            case CREATE_AND_WAIT:
                /* Check the No of Entries in the Table */
                i4RetStatus =
                    (INT4) (RBTreeCount
                            (gQoSGlobalInfo.pRbVlanQueueingMapTbl, &u4Count));

                if ((i4RetStatus == RB_FAILURE) ||
                    (u4Count > QOS_VLAN_MAP_TBL_MAX_ENTRIES))
                {
                    QOS_TRC_ARG2 (MGMT_TRC, "%s : Max No of Vlan Map Table"
                                  " Entries are Configured. No of Max Entries"
                                  " %d. \r\n", __FUNCTION__,
                                  QOS_VLAN_MAP_TBL_MAX_ENTRIES);

                    CLI_SET_ERR (QOS_CLI_ERR_VLAN_MAP_MAX_ENTRY);
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                    return (SNMP_FAILURE);
                }

                break;

            case DESTROY:
                CLI_SET_ERR (QOS_CLI_ERR_VLAN_MAP_NO_ENTRY);
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                i4ReturnValue = SNMP_FAILURE;

                break;

            default:
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                i4ReturnValue = SNMP_FAILURE;
                break;
        }
    }
    else
    {

        /* Entry Found In The Table */
        switch (i4TestValFsQoSVlanMapStatus)
        {
            case ACTIVE:

                if ((pVlanQMapNode->u1Status == NOT_READY))
                {
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                    return (SNMP_FAILURE);
                }

                break;

            case NOT_IN_SERVICE:

                if ((pVlanQMapNode->u1Status == NOT_READY))
                {
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                    return (SNMP_FAILURE);
                }

                /* 1. Check(Test) Dependency for this Entry. */
                i4RetStatus = QOS_CHECK_TABLE_REF_COUNT (pVlanQMapNode);

                if (i4RetStatus == QOS_FAILURE)
                {
                    QOS_TRC_ARG1 (MGMT_TRC, "In %s : ERROR - This Entry is "
                                  "referenced by Class Map Table Entry.\r\n",
                                  __FUNCTION__);

                    CLI_SET_ERR (QOS_CLI_ERR_VLAN_MAP_RFE);
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;

                    return (SNMP_FAILURE);
                }

                break;

            case DESTROY:

                /* 1. Check(Test) Dependency for this Entry. */
                i4RetStatus = QOS_CHECK_TABLE_REF_COUNT (pVlanQMapNode);

                if (i4RetStatus == QOS_FAILURE)
                {
                    QOS_TRC_ARG1 (MGMT_TRC, "In %s : ERROR - This Entry is "
                                  "referenced by Class Map Table Entry.\r\n",
                                  __FUNCTION__);

                    CLI_SET_ERR (QOS_CLI_ERR_VLAN_MAP_RFE);
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                    return (SNMP_FAILURE);
                }

                break;

            case CREATE_AND_WAIT:
            case CREATE_AND_GO:
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                i4ReturnValue = SNMP_FAILURE;
                break;

            default:
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                i4ReturnValue = SNMP_FAILURE;
                break;

        }                        /* End of switch */
    }                            /* End of if */
    return ((INT1) i4ReturnValue);
}

/****************************************************************************
 Function    :  nmhTestv2FsQoSVlanMapIfIndex
 Input       :  The Indices
                FsQoSVlanMapID

                The Object
                testValFsQoSVlanMapIfIndex
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsQoSVlanMapIfIndex (UINT4 *pu4ErrorCode, UINT4 u4FsQoSVlanMapID,
                              UINT4 u4TestValFsQoSVlanMapIfIndex)
{

    INT4                i4RetStatus = QOS_FAILURE;

    *pu4ErrorCode = SNMP_ERR_NO_ERROR;

    /* Check Index */
    i4RetStatus = QoSUtlValidateVlanQMapTblIdxInst (u4FsQoSVlanMapID,
                                                    pu4ErrorCode);
    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlValidateVlanQMapTblIdxInst () "
                      "Returns FAILURE. \r\n", __FUNCTION__);
        return (SNMP_FAILURE);
    }

    /* Check  Object's Value */
    i4RetStatus = QoSUtlValidateIfIndex (u4TestValFsQoSVlanMapIfIndex,
                                         pu4ErrorCode);
    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlValidateIfIndex () "
                      "Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhTestv2FsQoSVlanMapVlanId
 Input       :  The Indices
                FsQoSVlanMapID

                The Object
                testValFsQoSVlanMapVlanId
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsQoSVlanMapVlanId (UINT4 *pu4ErrorCode, UINT4 u4FsQoSVlanMapID,
                             UINT4 u4TestValFsQoSVlanMapVlanId)
{

    INT4                i4RetStatus = QOS_FAILURE;
    *pu4ErrorCode = SNMP_ERR_NO_ERROR;

    /* Check Index */
    i4RetStatus = QoSUtlValidateVlanQMapTblIdxInst (u4FsQoSVlanMapID,
                                                    pu4ErrorCode);

    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlValidateVlanQMapTblIdxInst () "
                      "Returns FAILURE. \r\n", __FUNCTION__);
        return (SNMP_FAILURE);
    }

    /* Check  Object's Value */
    i4RetStatus = QoSUtlValidateVlanId (u4TestValFsQoSVlanMapVlanId,
                                        pu4ErrorCode);
    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlValidateVlanId () "
                      "Returns FAILURE. \r\n", __FUNCTION__);
        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhDepv2FsQoSVlanMapTable
 Input       :  The Indices
                FsQoSVlanMapID
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsQoSVlanMapTable (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                           tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsQoSClassMapVlanMapId
 Input       :  The Indices
                FsQoSClassMapId

                The Object
                retValFsQoSClassMapVlanMapId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsQoSClassMapVlanMapId (UINT4 u4FsQoSClassMapId,
                              UINT4 *pu4RetValFsQoSClassMapVlanMapId)
{
    tQoSClassMapNode   *pClsMapNode = NULL;

    pClsMapNode = QoSUtlGetClassMapNode (u4FsQoSClassMapId);

    if (pClsMapNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetClassMapNode () "
                      "Returns FAILURE. \r\n", __FUNCTION__);
        return (SNMP_FAILURE);
    }
    *pu4RetValFsQoSClassMapVlanMapId = pClsMapNode->u4VlanMapId;
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhSetFsQoSClassMapVlanMapId
 Input       :  The Indices
                FsQoSClassMapId

                The Object
                setValFsQoSClassMapVlanMapId
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsQoSClassMapVlanMapId (UINT4 u4FsQoSClassMapId,
                              UINT4 u4SetValFsQoSClassMapVlanMapId)
{
    tQoSClassMapNode   *pClsMapNode = NULL;
    tQoSInVlanMapNode  *pVlanMapNode = NULL;

    pClsMapNode = QoSUtlGetClassMapNode (u4FsQoSClassMapId);
    if (pClsMapNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetClassMapNode () "
                      "Returns FAILURE. \r\n", __FUNCTION__);
        return (SNMP_FAILURE);
    }

    /* If the Same value is configured again no need to incrment the
     * RefCount and Set */
    if (pClsMapNode->u4VlanMapId == u4SetValFsQoSClassMapVlanMapId)
    {
        return (SNMP_SUCCESS);
    }

    pVlanMapNode = QoSUtlGetVlanQMapNode (u4SetValFsQoSClassMapVlanMapId);
    if (pVlanMapNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetVlanQMapNode () "
                      "Returns FAILURE. \r\n", __FUNCTION__);
        return (SNMP_FAILURE);
    }
    pVlanMapNode->u4RefCount = (pVlanMapNode->u4RefCount) + 1;
    pClsMapNode->u4VlanMapId = u4SetValFsQoSClassMapVlanMapId;
    pVlanMapNode->u4ClassMapNode = u4FsQoSClassMapId;
    QoSValidateClsMapTblEntry (pClsMapNode);
    return (SNMP_SUCCESS);
}

#ifdef WLC_WANTED
/****************************************************************************
 Function    :  QosWssWrMapClassToPolicy
 Input       :  pPlyMapNode 
 Output      :  None
 Returns     :  QOS_FAILURE/QOS_SUCCESS
****************************************************************************/
INT4
QosWssWrMapClassToPolicy (tQoSPolicyMapNode * pPlyMapNode,
                          INT4 i4SetValFsQoSPolicyMapStatus)
{
    tQoSClassInfoNode  *pClsInfoNode = NULL;
    tQoSFilterInfoNode *pFltInfoNode = NULL;
    tQoSInPriorityMapNode *pQoSInPriMapNode = NULL;
    tWssIfCapDB        *pWssIfCapwapDB = NULL;

    WSS_IF_DB_TEMP_MEM_ALLOC (pWssIfCapwapDB, OSIX_FAILURE)
        pClsInfoNode = QoSUtlGetClassInfoNode (pPlyMapNode->u4ClassId);

    if (pClsInfoNode == NULL)
    {
        WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
        return (QOS_FAILURE);
    }

    /* Scan the Filter list in the ClassInfo */
    TMO_SLL_Scan (&(pClsInfoNode->SllFltInfo), pFltInfoNode,
                  tQoSFilterInfoNode *)
    {
        /* Fill the Values for that Entry */
        pQoSInPriMapNode =
            QoSUtlGetPriorityMapNode (pFltInfoNode->pClsMapNode->
                                      u4PriorityMapId);
        if (pQoSInPriMapNode == NULL)
        {
            WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
            return (QOS_FAILURE);
        }

        MEMSET (pWssIfCapwapDB, 0, sizeof (tWssIfCapDB));
        pWssIfCapwapDB->u1OutDscp = (UINT1) pPlyMapNode->u2DefaultPHB;
        pWssIfCapwapDB->u1InPriority = pQoSInPriMapNode->u1InPriority;
        pWssIfCapwapDB->u4IfIndex = pQoSInPriMapNode->u4IfIndex;

        if (i4SetValFsQoSPolicyMapStatus == ACTIVE)
        {
            WssIfProcessCapwapDBMsg (WSS_CAPWAP_CREATE_DSCP_MAP_ENTRY,
                                     pWssIfCapwapDB);
        }
        else if ((i4SetValFsQoSPolicyMapStatus == DESTROY) ||
                 (i4SetValFsQoSPolicyMapStatus == NOT_IN_SERVICE))
        {
            WssIfProcessCapwapDBMsg (WSS_CAPWAP_DELETE_DSCP_MAP_ENTRY,
                                     pWssIfCapwapDB);
        }
    }
    WSS_IF_DB_TEMP_MEM_RELEASE (pWssIfCapwapDB);
    return (QOS_SUCCESS);
}
#endif
/* LOW LEVEL Routines for Table : FsQoSPortTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsQoSPortTable
 Input       :  The Indices
                IfIndex
                FsQoSPcpPacketType
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsQoSPortTable (INT4 i4IfIndex,
                                        INT4 i4FsQoSPcpPacketType)
{
    tQoSPortTblEntry   *pQoSPortNode = NULL;

    if (gQoSGlobalInfo.eSysControl != QOS_SYS_CNTL_START)
    {
        QOS_TRC (MGMT_TRC, QOS_MODULE_SHUTDOW_ERROR_MSG);
        return (SNMP_FAILURE);
    }

    pQoSPortNode = QoSUtlGetPortTblNode (i4IfIndex, i4FsQoSPcpPacketType);
    if (pQoSPortNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetPortNode () Returns "
                      "FAILURE. \r\n", __FUNCTION__);
        return (SNMP_FAILURE);
    }

    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsQoSPortTable
 Input       :  The Indices
                IfIndex
                FsQoSPcpPacketType
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsQoSPortTable (INT4 *pi4IfIndex, INT4 *pi4FsQoSPcpPacketType)
{
    INT4                i4RetStatus = QOS_FAILURE;

    if (gQoSGlobalInfo.eSysControl != QOS_SYS_CNTL_START)
    {
        QOS_TRC (MGMT_TRC, QOS_MODULE_SHUTDOW_ERROR_MSG);
        return (SNMP_FAILURE);
    }

    i4RetStatus = QoSGetNextPortTblEntryIndex (0, pi4IfIndex, 0,
                                               pi4FsQoSPcpPacketType);

    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSGetNextPortTblEntryIndex () "
                      "Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhGetNextIndexFsQoSPortTable
 Input       :  The Indices
                IfIndex
                nextIfIndex
                FsQoSPcpPacketType
                nextFsQoSPcpPacketType
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsQoSPortTable (INT4 i4IfIndex, INT4 *pi4NextIfIndex,
                               INT4 i4FsQoSPcpPacketType,
                               INT4 *pi4NextFsQoSPcpPacketType)
{
    INT4                i4RetStatus = QOS_FAILURE;

    if (gQoSGlobalInfo.eSysControl != QOS_SYS_CNTL_START)
    {
        QOS_TRC (MGMT_TRC, QOS_MODULE_SHUTDOW_ERROR_MSG);
        return (SNMP_FAILURE);
    }

    /* Check the Range of Index Values (Min-Max) */
    i4RetStatus = QOS_CHECK_PORT_TBL_INDEX_RANGE (i4IfIndex,
                                                  i4FsQoSPcpPacketType);

    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG3 (MGMT_TRC, "%s : Interface Index %d is out of Range."
                      " The Range Should be (1-%d). \r\n", __FUNCTION__,
                      i4IfIndex, QOS_PORT_TBL_MAX_INDEX_RANGE);

        return (SNMP_FAILURE);
    }

    i4RetStatus =
        QoSGetNextPortTblEntryIndex (i4IfIndex,
                                     pi4NextIfIndex,
                                     i4FsQoSPcpPacketType,
                                     pi4NextFsQoSPcpPacketType);
    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSGetNextPortTblEntryIndex () "
                      "Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    return (SNMP_SUCCESS);
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsQoSPcpSelRow
 Input       :  The Indices
                IfIndex
                FsQoSPcpPacketType
 
                The Object 
                retValFsQoSPcpSelRow
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsQoSPcpSelRow (INT4 i4IfIndex, INT4 i4FsQoSPcpPacketType,
                      INT4 *pi4RetValFsQoSPcpSelRow)
{
    tQoSPortTblEntry   *pPortTblNode = NULL;

    pPortTblNode = QoSUtlGetPortTblNode (i4IfIndex, i4FsQoSPcpPacketType);

    if (pPortTblNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetPortTblNode () "
                      "Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    *pi4RetValFsQoSPcpSelRow = pPortTblNode->i4PcpSelRow;

    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhGetFsQoSPcpRowStatus
 Input       :  The Indices
                IfIndex
                FsQoSPcpPacketType
 
                The Object 
                retValFsQoSPcpRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsQoSPcpRowStatus (INT4 i4IfIndex, INT4 i4FsQoSPcpPacketType,
                         INT4 *pi4RetValFsQoSPcpRowStatus)
{
    tQoSPortTblEntry   *pPortTblNode = NULL;

    pPortTblNode = QoSUtlGetPortTblNode (i4IfIndex, i4FsQoSPcpPacketType);

    if (pPortTblNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetPortTblNode () "
                      "Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    *pi4RetValFsQoSPcpRowStatus = (INT4) (pPortTblNode->u1Status);

    return (SNMP_SUCCESS);

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsQoSPcpSelRow
 Input       :  The Indices
                IfIndex
                FsQoSPcpPacketType
 
                The Object 
                setValFsQoSPcpSelRow
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsQoSPcpSelRow (INT4 i4IfIndex, INT4 i4FsQoSPcpPacketType,
                      INT4 i4SetValFsQoSPcpSelRow)
{
    tQoSPortTblEntry   *pPortTblNode = NULL;

    pPortTblNode = QoSUtlGetPortTblNode (i4IfIndex, i4FsQoSPcpPacketType);

    if (pPortTblNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetPortTblNode () "
                      "Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    pPortTblNode->i4PcpSelRow = i4SetValFsQoSPcpSelRow;

    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhSetFsQoSPcpRowStatus
 Input       :  The Indices
                IfIndex
                FsQoSPcpPacketType
 
                The Object 
                setValFsQoSPcpRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsQoSPcpRowStatus (INT4 i4IfIndex, INT4 i4FsQoSPcpPacketType,
                         INT4 i4SetValFsQoSPcpRowStatus)
{
    tQoSPortTblEntry   *pPortTblNode = NULL;
    INT4                i4RowStatus = QOS_FAILURE;
    INT4                i4RetStatus = SNMP_SUCCESS;

    pPortTblNode = QoSUtlGetPortTblNode (i4IfIndex, i4FsQoSPcpPacketType);
    if (pPortTblNode != NULL)
    {
        i4RowStatus = (INT4) pPortTblNode->u1Status;

        /* Entry found in the Table */
        if (i4RowStatus == i4SetValFsQoSPcpRowStatus)
        {
            /* same value */
            return (SNMP_SUCCESS);
        }
    }
    else
    {
        /* New Entry need to be created in the Table, only if
         * i4TestValFsQoSPcpRowStatus = CREATE_AND_WAIT */
        /* Optional */
        if (i4SetValFsQoSPcpRowStatus == DESTROY)
        {
            return (SNMP_SUCCESS);
        }
    }

    switch (i4SetValFsQoSPcpRowStatus)
    {
            /* For a New Entry */
        case CREATE_AND_WAIT:
            pPortTblNode = QoSCreatePortTblEntry (i4IfIndex,
                                                  i4FsQoSPcpPacketType);

            if (pPortTblNode == NULL)
            {
                QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSCreatePriMapTblEntry () "
                              "Returns FAILURE. \r\n", __FUNCTION__);

                return (SNMP_FAILURE);
            }
            i4RetStatus = SNMP_SUCCESS;

            break;
            /* Modify an Entry */
        case ACTIVE:
            if ((i4IfIndex <= QOS_CLI_ERR_PORT_TBL_MAX_ENTRY)
                && (QosPortRmGetNodeState () == RM_STANDBY)
                && (pPortTblNode == NULL))
            {
                return (SNMP_SUCCESS);
            }
            if (pPortTblNode != NULL)
            {
                if (pPortTblNode->i4PcpTblIndex != 0)
                {
                    i4RetStatus = QoSConfigPcpTblEntries (pPortTblNode,
                                                          QOS_PCP_TBL_ACTION_MODIFY);
                    if (i4RetStatus == QOS_FAILURE)
                    {
                        QOS_TRC_ARG1 (MGMT_TRC,
                                      "In %s : QoSAddConfPortTblEntry () "
                                      "Returns FAILURE. \r\n", __FUNCTION__);
                        return (SNMP_FAILURE);
                    }
                }
                else
                {
                    i4RetStatus = QoSConfigPcpTblEntries (pPortTblNode,
                                                          QOS_PCP_TBL_ACTION_CREATE);
                    if (i4RetStatus == QOS_FAILURE)
                    {
                        QOS_TRC_ARG1 (MGMT_TRC,
                                      "In %s : QoSAddConfPortTblEntry () "
                                      "Returns FAILURE. \r\n", __FUNCTION__);
                        return (SNMP_FAILURE);
                    }
                }
                pPortTblNode->u1Status = ACTIVE;
            }
            break;

        case NOT_IN_SERVICE:
            /* Need to delete the Hardware Entries */
            if ((i4IfIndex <= QOS_CLI_ERR_PORT_TBL_MAX_ENTRY)
                && (QosPortRmGetNodeState () == RM_STANDBY)
                && (pPortTblNode == NULL))
            {
                return (SNMP_SUCCESS);
            }
            if (pPortTblNode != NULL)
            {
                i4RetStatus = QoSConfigPcpTblEntries (pPortTblNode,
                                                      QOS_PCP_TBL_ACTION_DELETE);
                if (i4RetStatus == QOS_FAILURE)
                {
                    QOS_TRC_ARG1 (MGMT_TRC,
                                  "In %s : QoSAddConfPortTblEntry () "
                                  "Returns FAILURE. \r\n", __FUNCTION__);
                    return (SNMP_FAILURE);
                }
                pPortTblNode->u1Status = NOT_IN_SERVICE;
            }

            break;

        default:
            i4RetStatus = SNMP_FAILURE;
            break;
    }

    return ((INT1) i4RetStatus);

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsQoSPcpSelRow
 Input       :  The Indices
                IfIndex
                FsQoSPcpPacketType
 
                The Object 
                testValFsQoSPcpSelRow
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsQoSPcpSelRow (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                         INT4 i4FsQoSPcpPacketType,
                         INT4 i4TestValFsQoSPcpSelRow)
{

    INT4                i4RetStatus = QOS_FAILURE;

    *pu4ErrorCode = SNMP_ERR_NO_ERROR;

    /* Check Index */
    i4RetStatus = QoSUtlValidatePortTblIdxInst (i4IfIndex,
                                                i4FsQoSPcpPacketType,
                                                pu4ErrorCode);

    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlValidatePortTblIdxInst () "
                      "Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    /* Check  Object's Value */
    if (i4TestValFsQoSPcpSelRow > QOS_PCP_SEL_ROW_MAX_VAL)
    {
        QOS_TRC_ARG3 (MGMT_TRC, "%s :PCP Selection Row is out of "
                      "range. The range should be Min %d - Max %d. \r\n",
                      __FUNCTION__, QOS_PCP_SEL_ROW_MIN_VAL,
                      QOS_PCP_SEL_ROW_MAX_VAL);

        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;

        CLI_SET_ERR (QOS_CLI_ERR_INVALID_PCP_SEL_ROW);

        return (SNMP_FAILURE);
    }

    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhTestv2FsQoSPcpRowStatus
 Input       :  The Indices
                IfIndex
                FsQoSPcpPacketType
 
                The Object 
                testValFsQoSPcpRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsQoSPcpRowStatus (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                            INT4 i4FsQoSPcpPacketType,
                            INT4 i4TestValFsQoSPcpRowStatus)
{
    tQoSPortTblEntry   *pPortTblNode = NULL;
    INT4                i4RetStatus = QOS_FAILURE;
    INT4                i4ReturnValue = SNMP_SUCCESS;

    if (gQoSGlobalInfo.eSysControl != QOS_SYS_CNTL_START)
    {
        QOS_TRC (MGMT_TRC, QOS_MODULE_SHUTDOW_ERROR_MSG);
        CLI_SET_ERR (QOS_CLI_ERR_MODULE_SHUTDOW);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return (SNMP_FAILURE);
    }

    *pu4ErrorCode = SNMP_ERR_NO_ERROR;

    /* Check the Range of Index Values (Min-Max) */
    i4RetStatus = QOS_CHECK_PORT_TBL_INDEX_RANGE (i4IfIndex,
                                                  i4FsQoSPcpPacketType);

    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG3 (MGMT_TRC, "%s : PortMap Table Index %d is out of Range."
                      " The Range Should be (1-%d). \r\n", __FUNCTION__,
                      i4IfIndex, QOS_CLI_ERR_PORT_TBL_MAX_ENTRY);

        CLI_SET_ERR (QOS_CLI_ERR_PORT_TBL_MAX_ENTRY);

        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }

    pPortTblNode = QoSUtlGetPortTblNode (i4IfIndex, i4FsQoSPcpPacketType);

    if (pPortTblNode == NULL)
    {
        /* Entry Not Found In The Table */
        switch (i4TestValFsQoSPcpRowStatus)
        {
            case ACTIVE:
            case NOT_IN_SERVICE:
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                i4ReturnValue = SNMP_FAILURE;
                break;

            case CREATE_AND_WAIT:
                i4ReturnValue = SNMP_SUCCESS;
                break;

            default:
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                i4ReturnValue = SNMP_FAILURE;
                break;
        }
    }
    else
    {

        /* Entry Found In The Table */
        switch (i4TestValFsQoSPcpRowStatus)
        {
            case ACTIVE:

                if ((pPortTblNode->u1Status == NOT_READY))
                {
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                    return (SNMP_FAILURE);
                }

                break;

            case NOT_IN_SERVICE:

                if ((pPortTblNode->u1Status == NOT_READY))
                {
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                    return (SNMP_FAILURE);
                }

                break;

            default:
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                i4ReturnValue = SNMP_FAILURE;
                break;

        }                        /* End of switch */

    }                            /* End of if */

    return ((INT1) i4ReturnValue);
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsQoSPortTable
 Input       :  The Indices
                IfIndex
                FsQoSPcpPacketType
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsQoSPortTable (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                        tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}
