/* $Id: qosapi.c,v 1.23 2016/05/25 10:04:42 siva Exp $ */
/*****************************************************************************/
/*    FILE  NAME            : qosapi.c                                       */
/*    PRINCIPAL AUTHOR      : Aricent Inc.                                   */
/*    MODULE NAME           : QOSX                                           */
/*    LANGUAGE              : C                                              */
/*    TARGET ENVIRONMENT    : Any                                            */
/*    DATE OF FIRST RELEASE :                                                */
/*    AUTHOR                : Aricent Inc.                                   */
/*    DESCRIPTION           : This file contains the exported APIs of        */
/*                            qos module                                     */
/*---------------------------------------------------------------------------*/
#include "qosinc.h"

/*****************************************************************************/
/* Function Name      : QosPortOperIndication                                */
/*                                                                           */
/* Description        : This function is called from the L2Iwf Module        */
/*                      to indicate the port status indication               */
/*                                                                           */
/* Input(s)           : u2Port - Port number                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : QOS_SUCCESS / QOS_FAILURE                            */
/*                                                                           */
/* Called By          : L2iwf Module                                         */
/*****************************************************************************/
INT4
QosPortOperIndication (UINT4 u4Port, UINT1 u1Status)
{
    tQosQueueMsg       *pQosMsg = NULL;

    /* If the u4Port is LA Port Do Nothing */
    if (u4Port > SYS_DEF_MAX_PHYSICAL_INTERFACES)
    {
        return (QOS_SUCCESS);
    }

    if (gQoSGlobalInfo.eSysControl != QOS_SYS_CNTL_START)
    {
        QOS_TRC (MGMT_TRC, QOS_MODULE_SHUTDOW_ERROR_MSG);
        return QOS_SUCCESS;
    }

    if ((pQosMsg = (tQosQueueMsg *) QOS_QMSG_ALLOC_MEMBLK) == NULL)
    {
        QOS_TRC (OS_RESOURCE_TRC | ALL_FAILURE_TRC,
                 "Memory allocation failed for queue message\r\n");

        return QOS_FAILURE;
    }

    MEMSET (pQosMsg, 0, sizeof (tQosQueueMsg));

    if (u1Status == CFA_IF_DOWN)
    {
        pQosMsg->u2MsgType = QOS_DISABLE_PORT_MSG;
    }
    if (u1Status == CFA_IF_UP)
    {
        pQosMsg->u2MsgType = QOS_ENABLE_PORT_MSG;
    }
    pQosMsg->u4Port = u4Port;

    if (QOS_SEND_TO_QUEUE (QOS_MSG_QUEUE_ID, (UINT1 *) &pQosMsg,
                           QOS_DEF_MSG_LEN) != OSIX_SUCCESS)
    {
        QOS_TRC_ARG1 (OS_RESOURCE_TRC,
                      "%s : Send To QOS Queue failed, releasing the queue message for port oper"
                      "status indication.\r\n", __FUNCTION__);
        QOS_RELEASE_QMSG_MEM_BLOCK (pQosMsg);
        return QOS_FAILURE;
    }

    if (QOS_SEND_EVENT (QOS_TASK_ID, QOS_MSG_EVENT) != OSIX_SUCCESS)
    {
        QOS_TRC_ARG1 (OS_RESOURCE_TRC,
                      "%s : Send To QOS Queue failed, releasing the queue message for port oper"
                      "status indication.\r\n", __FUNCTION__);
        return QOS_FAILURE;
    }

    return QOS_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : QosDeletePort                                        */
/*                                                                           */
/* Description        : This function is called from the L2IWF Module        */
/*                      to indicate the deletion of a Port.                  */
/*                                                                           */
/* Input(s)           : u2Port - The Port number of the port to be deleted   */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : QOS_SUCCESS / QOS_FAILURE                            */
/*                                                                           */
/* Called By          : L2IWF Module                                         */
/*****************************************************************************/
INT4
QosDeletePort (UINT4 u4Port)
{
    tQosQueueMsg       *pQosMsg = NULL;

    /* If the u4Port is LA Port Do Nothing */
    if (QosL2IwfIsPortInPortChannel (u4Port) == L2IWF_SUCCESS)
    {
        return QOS_SUCCESS;
    }
    if (gQoSGlobalInfo.eSysControl != QOS_SYS_CNTL_START)
    {
        QOS_TRC (MGMT_TRC, QOS_MODULE_SHUTDOW_ERROR_MSG);
        return QOS_SUCCESS;
    }

    if ((pQosMsg = (tQosQueueMsg *) QOS_QMSG_ALLOC_MEMBLK) == NULL)
    {
        QOS_TRC (OS_RESOURCE_TRC | ALL_FAILURE_TRC,
                 "Memory allocation failed for queue message\r\n");

        return QOS_FAILURE;
    }

    MEMSET (pQosMsg, 0, sizeof (tQosQueueMsg));

    pQosMsg->u2MsgType = QOS_DELETE_PORT_MSG;
    pQosMsg->u4Port = u4Port;

    if (QOS_SEND_TO_QUEUE (QOS_MSG_QUEUE_ID, (UINT1 *) &pQosMsg,
                           QOS_DEF_MSG_LEN) != OSIX_SUCCESS)
    {
        QOS_TRC_ARG1 (OS_RESOURCE_TRC,
                      "%s : Send To QOS Queue failed, releasing the queue message for port oper"
                      "status indication.\r\n", __FUNCTION__);
        QOS_RELEASE_QMSG_MEM_BLOCK (pQosMsg);
        return QOS_FAILURE;
    }

    if (QOS_SEND_EVENT (QOS_TASK_ID, QOS_MSG_EVENT) != OSIX_SUCCESS)
    {
        QOS_TRC (OS_RESOURCE_TRC | ALL_FAILURE_TRC,
                 "Sending QOS_MSG_EVENT to QOS Task failed\r\n");
        return QOS_FAILURE;
    }
    return QOS_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : QosCreatePort                                        */
/*                                                                           */
/* Description        : This function is called from the L2IWF               */
/*                      to indicate the creation of a port                   */
/*                                                                           */
/* Input(s)           : u2Port     - Physical index of the port           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : QOS_SUCCESS/QOS_FAILURE                              */
/*                                                                           */
/* Called By          : CFA Module                                           */
/*****************************************************************************/
INT4
QosCreatePort (UINT4 u4Port)
{
    tQosQueueMsg       *pQosMsg = NULL;

    /* If the u4Port is LA Port Do Nothing */
    if (QosL2IwfIsPortInPortChannel (u4Port) == L2IWF_SUCCESS)
    {
        return QOS_SUCCESS;
    }
    if (gQoSGlobalInfo.eSysControl != QOS_SYS_CNTL_START)
    {
        QOS_TRC (MGMT_TRC, QOS_MODULE_SHUTDOW_ERROR_MSG);
        return QOS_SUCCESS;
    }

    if ((pQosMsg = (tQosQueueMsg *) QOS_QMSG_ALLOC_MEMBLK) == NULL)
    {
        QOS_TRC (OS_RESOURCE_TRC | ALL_FAILURE_TRC,
                 "Memory allocation failed for queue message\r\n");
        return QOS_FAILURE;
    }

    MEMSET (pQosMsg, 0, sizeof (tQosQueueMsg));

    pQosMsg->u2MsgType = QOS_CREATE_PORT_MSG;
    pQosMsg->u4Port = u4Port;

    if (QOS_SEND_TO_QUEUE (QOS_MSG_QUEUE_ID, (UINT1 *) &pQosMsg,
                           QOS_DEF_MSG_LEN) != OSIX_SUCCESS)
    {
        QOS_TRC_ARG1 (OS_RESOURCE_TRC, "%s : Send To QOS Queue failed,"
                      "releasing the queue message for port create indication.\r\n",
                      __FUNCTION__);
        QOS_RELEASE_QMSG_MEM_BLOCK (pQosMsg);
        return QOS_FAILURE;
    }

    if (QOS_SEND_EVENT (QOS_TASK_ID, QOS_MSG_EVENT) != OSIX_SUCCESS)
    {
        QOS_TRC (OS_RESOURCE_TRC | ALL_FAILURE_TRC,
                 "Sending QOS_MSG_EVENT to QOS Task failed\r\n");
        return QOS_FAILURE;
    }
    return QOS_SUCCESS;
}

/***************************************************************************/
/* Function Name      : QoSApiModuleStart                                  */
/* Description        : This function is used to Start the QoS Module.It   */
/*                      will do the following Actions                      */
/*                          1. Allocate Memory from the Resource Management   */
/*                          Module.                                        */
/*                         2. All Tables fro QoS Module will be Created      */
/* Input(s)           : None.                                              */
/* Output(s)          : None.                                              */
/* Return Value(s)    : QOS_SUCCESS or QOS_FAILURE                         */
/* Called By          : RM                                                 */
/* Calling Function   : QoSStart                                           */
/***************************************************************************/
INT4
QoSApiModuleStart (VOID)
{
    if (QoSStart () != QOS_SUCCESS)
    {
        QOS_TRC (MGMT_TRC, "In QoSApiModuleStart() :"
                 "QoSStart () - FAILED \r\n");
        return (QOS_FAILURE);

    }
    if (QosRedInitGlobalInfo () == OSIX_FAILURE)
    {
        QOS_TRC (INIT_SHUT_TRC | ALL_FAILURE_TRC, "QoSInit:"
                 " Redundancy Global Info Initialization FAILED !!!\r\n");
        QosRedDeInitGlobalInfo ();
        return (QOS_FAILURE);
    }

    return (QOS_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSApiModuleShutdown                                 */
/* Description        : This function is used to Shutdown the QoS Module.It  */
/*                      will do the following Actions                        */
/*                      1. Deleting the Hardware Configurations.             */
/*                      2. Relasing the allocated Memory to the Resource     */
/*                         Management Module.                                */
/*                      3. System Control and System Status will set it as   */
/*                          SHUTDOWN and DISABEL Respectively.               */
/* Input(s)           : None.                                                */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Return Value(s)    : QOS_SUCCESS or QOS_FAILURE                           */
/* Called By          : RM                                                   */
/* Calling Function   : QoSShutdown                                          */
/*****************************************************************************/
INT4
QoSApiModuleShutdown (VOID)
{
    if (QoSShutdown () != QOS_SUCCESS)
    {
        QOS_TRC (MGMT_TRC, "In QoSApiModuleShutdown() :"
                 "QoSShutdown () - FAILED \r\n");
        return (QOS_FAILURE);

    }
    /* DeRegister RM Module */
    QosRedDeInitGlobalInfo ();
    return (QOS_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QosApiGetPortPriToTCMapping                          */
/* Description        : This function is called to get Traffic class (Q Id)  */
/*                      of given port and priority                           */
/* Input(s)           : u4Port - Port Number                                 */
/*                      u1VlanPri - dot1q Priority                           */
/* Output(s)          : pu4TrafficClass - Traffic Class                      */
/* Global Variables   : None                                                 */
/* Return Value(s)    : OSIX_SUCCESS or OSIX_FAILURE                         */
/*****************************************************************************/
UINT4
QosApiGetPortPriToTCMapping (UINT4 u4Port, UINT1 u1VlanPri,
                             UINT4 *pu4TrafficClass)
{
    tQoSQMapNode       *pQMapNode = NULL;
    tQoSQNode          *pQNode = NULL;

    *pu4TrafficClass = u1VlanPri;

    /* Since QOSX doesnt support LAGs, return the incoming
     * priority as traffic class for LAGs. Below condition needs to be
     * removed once QOSX supports configurations on LAG ports */
    if ((u4Port > BRG_MAX_PHY_PORTS) && (u4Port <= BRG_MAX_PHY_PLUS_LOG_PORTS))
    {
        QOS_TRC (ALL_FAILURE_TRC, "QosApiGetPortPriToTCMapping:"
                 " Invoked for unsupported port type !!!\r\n");

        return OSIX_SUCCESS;
    }

    QoSLock ();
    /*Get the Queue Id from from Qmap Node using the Priority */

    pQMapNode = QoSUtlGetQMapNode ((INT4)u4Port, QOS_INIT_VAL,
                                   QOS_QMAP_PRI_TYPE_VLAN_PRI, u1VlanPri);
    if (pQMapNode == NULL)
    {
        /* If search fails, then fetch the default entry. 
         * Query with port id as 0 */
        pQMapNode = QoSUtlGetQMapNode (0, QOS_INIT_VAL,
                                       QOS_QMAP_PRI_TYPE_VLAN_PRI, u1VlanPri);
        if (pQMapNode == NULL)
        {
            QoSUnLock ();
            return OSIX_FAILURE;
        }
    }
    /*Get the Q Node */
    pQNode = QoSUtlGetQNode ((INT4)u4Port, pQMapNode->u4QId);
    if (pQNode == NULL)
    {
        QoSUnLock ();
        return OSIX_FAILURE;
    }
    *pu4TrafficClass = pQNode->u4Id;
    QoSUnLock ();
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : QosApiHandlePriorityMapRequest                       */
/* Description        : This function can be invoked by other modules to     */
/*                      perform the following,                               */
/*                      1. Add a priority map entry in QOSX module           */
/*                      2. Remove a priority map entry from QOSX module      */
/*                      3. Get whether priority map entry already exists for */
/*                         the InPri on a given Port                         */
/*                      4. Update priority map entry from QOSX module        */
/* Input(s)           : u4Port - Port Number                                 */
/*                      pu1InPri - Incoming VLAN priority                    */
/*                      pu1OutPri - Outgoing VLAN priority                   */
/*                      u1Request - ADD / DEL / GET priority map entry       */
/* Output(s)          : For Get request alone, pu1OutPri will be output      */
/* Global Variables   : None                                                 */
/* Return Value(s)    : OSIX_SUCCESS or OSIX_FAILURE                         */
/*****************************************************************************/
UINT4
QosApiHandlePriorityMapRequest (UINT4 u4Port, UINT1 u1InPri,
                                UINT1 *pu1OutPri, UINT1 u1Request)
{
    UINT4                u4Retval = OSIX_SUCCESS;
    tQoSInPriorityMapNode *pPriMapUniqueNode = NULL;
    QoSLock ();
    switch (u1Request)
    {

        case QOS_ADD_PRIORITY_MAP:
            /* The logic to these functions need to be implemented */
            break;

        case QOS_DEL_PRIORITY_MAP:
            /* The logic to these functions need to be implemented */
            break;

        case QOS_UPDATE_PRIORITY_MAP:
            /* The logic to these functions need to be implemented */
            break;

        case QOS_GET_PRIORITY_MAP:
            pPriMapUniqueNode =
                QoSUtlGetPriorityMapUniNode (u4Port, 0 /* All VLANs */ ,
                                             u1InPri, QOS_IN_PRI_TYPE_VLAN_PRI);
            if (pPriMapUniqueNode != NULL)
            {
                *pu1OutPri = pPriMapUniqueNode->u1RegenPriority;
                u4Retval = OSIX_SUCCESS;
                break;
            }
            u4Retval = OSIX_FAILURE;

            break;
        default:
            break;
    }
    QoSUnLock ();
    return u4Retval;
}

/*****************************************************************************/
/* Function Name      : QosApiAddMfcRateLimiter                                 */
/*                                                                           */
/* Description        : This function is called from the ACL Module          */
/*                            to add MFC QoS configuration parameters        */
/*                            for the added ACL rule                         */
/*                                                                           */
/* Input(s)           : u4FilterType - Acl filter type (L2 or L3)            */
/*                          pAclQosMapInfo - Pointer to Acl QoS common info  */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : QOS_SUCCESS / QOS_FAILURE                            */
/*                                                                           */
/* Called By          : ACL Module                                           */
/*****************************************************************************/
INT4
QosApiAddMfcRateLimiter (UINT4 u4FilterType, tAclQosMapInfo * pAclQosMapInfo)
{

    INT4                i4RowStatus = SNMP_FAILURE;
    /* simpleTokenBucket */
    INT4                i4MetrType = QOS_METER_TYPE_SIMPLE_TB;
    /* fsQoSPolicyMapPHBType */
    INT4                i4PriType = QOS_PLY_DEFAULT_PHB_TYPE;
    UINT4               u4ErrorCode = SNMP_ERR_NO_ERROR;
    tAclQosMapInfo     *pAclQosInfo = NULL;
    tSNMP_OCTET_STRING_TYPE *pOctStrActFlag = NULL;
    UINT1               u1InProfConfrAct = QOS_PLY_IN_PROF_CONF_VLAN_PRI;
    UINT1               u1InProExcdAct = QOS_PLY_IN_PROF_EXC_DROP;
    UINT1               u1OutProAct = QOS_PLY_OUT_PROF_VIO_DROP;

    QOS_TRC (MGMT_TRC, "QOSMFC Function QosApiAddMfcRateLimiter Entry\n");

    if (pAclQosMapInfo == NULL)
    {
        QOS_TRC_ARG1 (ALL_FAILURE_TRC,
                      "QOSMFC pAclQosMapInfo is NULL line:%d\n", __LINE__);
        return (QOS_FAILURE);
    }
    QoSLock ();
    pAclQosInfo = pAclQosMapInfo;

    /*Table Name:  fsQoSClassMapTable */
    if (nmhGetFsQoSClassMapStatus (pAclQosInfo->QosIds.u4ClassMapId,
                                   &i4RowStatus) == SNMP_FAILURE)
    {
        /* Create new Entry */
        if (nmhTestv2FsQoSClassMapStatus (&u4ErrorCode,
                                          pAclQosInfo->QosIds.u4ClassMapId,
                                          CREATE_AND_WAIT) == SNMP_FAILURE)
        {
            QOS_TRC_ARG2 (ALL_FAILURE_TRC, "QOSMFC:Failed "
                          "nmhTestv2FsQoSClassMapStatus in %s and line %d\n",
                          __FUNCTION__, __LINE__);
            QOS_TRC_ARG3 (ALL_FAILURE_TRC, "ErrorCode[%d] ClassMapId[%d]"
                          "RowStatus[%d]\n", u4ErrorCode,
                          pAclQosInfo->QosIds.u4ClassMapId, CREATE_AND_WAIT);
            QoSUnLock ();
            return (QOS_FAILURE);
        }

        if (nmhSetFsQoSClassMapStatus (pAclQosInfo->QosIds.u4ClassMapId,
                                       CREATE_AND_WAIT) == SNMP_FAILURE)
        {
            QOS_TRC_ARG3 (ALL_FAILURE_TRC, "QOSMFC:Failed "
                          "nmhSetFsQoSClassMapStatus in %s and line %d"
                          "& ClassMapId[%d]\n", __FUNCTION__, __LINE__,
                          pAclQosInfo->QosIds.u4ClassMapId);
            QoSUnLock ();
            return (QOS_FAILURE);
        }
    }
    else if (i4RowStatus == ACTIVE)
    {
        if (nmhSetFsQoSClassMapStatus (pAclQosInfo->QosIds.u4ClassMapId,
                                       NOT_IN_SERVICE) == SNMP_FAILURE)
        {
            QOS_TRC_ARG3 (ALL_FAILURE_TRC, "QOSMFC:Failed in "
                          "nmhSetFsQoSClassMapStatus in %s and line %d"
                          "ClassMapId[%d]\n", __FUNCTION__, __LINE__,
                          pAclQosInfo->QosIds.u4ClassMapId);
            QoSUnLock ();
            return (QOS_FAILURE);
        }
    }

    /* Add ACL L2Filer Id */
    if (u4FilterType == QOS_ACL_L2_CONFIG)
    {
        if (nmhTestv2FsQoSClassMapL2FilterId
            (&u4ErrorCode,
             pAclQosInfo->QosIds.u4ClassMapId,
             pAclQosInfo->u4FilterId) == SNMP_FAILURE)
        {
            QOS_TRC_ARG2 (ALL_FAILURE_TRC, "QOSMFC:Failed in "
                          "nmhTestv2FsQoSClassMapL2FilterId in %s & line"
                          " %d\n", __FUNCTION__, __LINE__);
            QOS_TRC_ARG3 (ALL_FAILURE_TRC, "ErrorCode[%d] ClassMapId[%d]"
                          "FilterId[%d]\n", u4ErrorCode,
                          pAclQosInfo->QosIds.u4ClassMapId,
                          pAclQosInfo->u4FilterId);
            QoSUnLock ();
            return (QOS_FAILURE);
        }

        nmhSetFsQoSClassMapL2FilterId (pAclQosInfo->QosIds.u4ClassMapId,
                                       pAclQosInfo->u4FilterId);
    }
    /* Add ACL L3Filer Id */
    else if (u4FilterType == QOS_ACL_L3_CONFIG)
    {
        if (nmhTestv2FsQoSClassMapL3FilterId (&u4ErrorCode,
                                              pAclQosInfo->QosIds.u4ClassMapId,
                                              pAclQosInfo->u4FilterId)
            == SNMP_FAILURE)
        {
            QOS_TRC_ARG2 (ALL_FAILURE_TRC, "QOSMFC:Failed in"
                          "nmhTestv2FsQoSClassMapL3FilterId in %s and line %d\n",
                          __FUNCTION__, __LINE__);
            QOS_TRC_ARG3 (ALL_FAILURE_TRC,
                          "ErrorCode[%d] ClassMapId[%d]" "FilterId[%d]\n",
                          u4ErrorCode, pAclQosInfo->QosIds.u4ClassMapId,
                          pAclQosInfo->u4FilterId);
            QoSUnLock ();
            return (QOS_FAILURE);
        }

        nmhSetFsQoSClassMapL3FilterId (pAclQosInfo->QosIds.u4ClassMapId,
                                       pAclQosInfo->u4FilterId);
    }

    /* Set CLASS in the Class Map Table */
    if (nmhTestv2FsQoSClassMapCLASS (&u4ErrorCode,
                                     pAclQosInfo->QosIds.u4ClassMapId,
                                     pAclQosInfo->QosIds.u4ClassId)
        == SNMP_FAILURE)
    {
        QOS_TRC_ARG2 (ALL_FAILURE_TRC, "QOSMFC:Failed in "
                      "nmhTestv2FsQoSClassMapCLASS in %s and line %d\n",
                      __FUNCTION__, __LINE__);
        QOS_TRC_ARG3 (ALL_FAILURE_TRC, "ErrorCode[%d] ClassMapId[%d]"
                      "ClassId[%d]\n", u4ErrorCode,
                      pAclQosInfo->QosIds.u4ClassMapId,
                      pAclQosInfo->QosIds.u4ClassId);
        QoSUnLock ();
        return (QOS_FAILURE);
    }

    nmhSetFsQoSClassMapCLASS (pAclQosInfo->QosIds.u4ClassMapId,
                              pAclQosInfo->QosIds.u4ClassId);

    /* Enough now make it active!!!! */
    nmhSetFsQoSClassMapStatus (pAclQosInfo->QosIds.u4ClassMapId, ACTIVE);

    /* Table Name:  fsQoSMeterTable */
    if (nmhGetFsQoSMeterStatus (pAclQosInfo->QosIds.u4MeterId,
                                &i4RowStatus) == SNMP_FAILURE)
    {
        /* Create new Entry */
        if (nmhTestv2FsQoSMeterStatus (&u4ErrorCode,
                                       pAclQosInfo->QosIds.u4MeterId,
                                       CREATE_AND_WAIT) == SNMP_FAILURE)
        {
            QOS_TRC_ARG2 (ALL_FAILURE_TRC, "QOSMFC:Failed in "
                          "nmhTestv2FsQoSMeterStatus in %s and line %d\n",
                          __FUNCTION__, __LINE__);
            QOS_TRC_ARG3 (ALL_FAILURE_TRC, "ErrorCode[%d] MeterId[%d]"
                          "RowStatus[%d]\n", u4ErrorCode,
                          pAclQosInfo->QosIds.u4MeterId, CREATE_AND_WAIT);
            QoSUnLock ();
            return (QOS_FAILURE);
        }

        if (nmhSetFsQoSMeterStatus (pAclQosInfo->QosIds.u4MeterId,
                                    CREATE_AND_WAIT) == SNMP_FAILURE)
        {
            QOS_TRC_ARG3 (ALL_FAILURE_TRC, "QOSMFC:Failed in "
                          "nmhSetFsQoSMeterStatus in %s and line %d"
                          "and 4MeterId[%d]\n", __FUNCTION__, __LINE__,
                          pAclQosInfo->QosIds.u4MeterId);
            QoSUnLock ();
            return (QOS_FAILURE);
        }
    }
    else if (i4RowStatus == ACTIVE)
    {
        if (nmhTestv2FsQoSMeterStatus (&u4ErrorCode,
                                       pAclQosInfo->QosIds.u4MeterId,
                                       NOT_IN_SERVICE) == SNMP_FAILURE)
        {
            QOS_TRC_ARG2 (ALL_FAILURE_TRC, "QOSMFC:Failed in "
                          "nmhTestv2FsQoSMeterStatus in %s and line %d\n",
                          __FUNCTION__, __LINE__);
            QOS_TRC_ARG3 (ALL_FAILURE_TRC, "ErrorCode[%d] MeterId[%d]"
                          "RowStatus[%d]\n", u4ErrorCode,
                          pAclQosInfo->QosIds.u4MeterId, NOT_IN_SERVICE);
            QoSUnLock ();
            return (QOS_FAILURE);
        }

        if (nmhSetFsQoSMeterStatus (pAclQosInfo->QosIds.u4MeterId,
                                    NOT_IN_SERVICE) == SNMP_FAILURE)
        {
            QOS_TRC_ARG3 (ALL_FAILURE_TRC, "QOSMFC:Failed in "
                          "nmhSetFsQoSMeterStatus in %s and line %d"
                          "and MeterId[%d]\n", __FUNCTION__, __LINE__,
                          pAclQosInfo->QosIds.u4MeterId);
            QoSUnLock ();
            return (QOS_FAILURE);
        }
    }

    /* Default meter type for all the ACL/QoS ruls are simpleTokenBucket */
    if (nmhTestv2FsQoSMeterType (&u4ErrorCode, pAclQosInfo->QosIds.u4MeterId,
                                 i4MetrType) == SNMP_FAILURE)
    {
        QOS_TRC_ARG2 (ALL_FAILURE_TRC, "QOSMFC:Failed in "
                      "nmhTestv2FsQoSMeterType in %s and line %d\n",
                      __FUNCTION__, __LINE__);
        QOS_TRC_ARG3 (ALL_FAILURE_TRC, "ErrorCode[%d] MeterId[%d]"
                      "MeterType[%d]\n", u4ErrorCode,
                      pAclQosInfo->QosIds.u4MeterId, i4MetrType);
        QoSUnLock ();
        return (QOS_FAILURE);
    }

    nmhSetFsQoSMeterType (pAclQosInfo->QosIds.u4MeterId, i4MetrType);

    /* Meter interval is default -  1 Second */
    if (nmhTestv2FsQoSMeterInterval
        (&u4ErrorCode, pAclQosInfo->QosIds.u4MeterId,
         QOS_ACL_METER_INTERVAL) == SNMP_FAILURE)
    {
        QOS_TRC_ARG2 (ALL_FAILURE_TRC, "QOSMFC:Failed in "
                      "nmhTestv2FsQoSMeterInterval in %s and line %d\n",
                      __FUNCTION__, __LINE__);
        QOS_TRC_ARG3 (ALL_FAILURE_TRC, "ErrorCode[%d] MeterId[%d]"
                      "MeterInterval[%d]\n", u4ErrorCode,
                      pAclQosInfo->QosIds.u4MeterId, QOS_ACL_METER_INTERVAL);
        QoSUnLock ();
        return (QOS_FAILURE);
    }

    nmhSetFsQoSMeterInterval (pAclQosInfo->QosIds.u4MeterId,
                              QOS_ACL_METER_INTERVAL);

    /* Set the meter interval CIR */
    if (nmhTestv2FsQoSMeterCIR (&u4ErrorCode, pAclQosInfo->QosIds.u4MeterId,
                                pAclQosInfo->u4ProtocolRate) == SNMP_FAILURE)
    {
        QOS_TRC_ARG2 (ALL_FAILURE_TRC, "QOSMFC:Failed in "
                      "nmhTestv2FsQoSMeterCIR in %s and line %d\n",
                      __FUNCTION__, __LINE__);
        QOS_TRC_ARG3 (ALL_FAILURE_TRC, "ErrorCode[%d] MeterId[%d]"
                      "ProtocolRate[%d]\n", u4ErrorCode,
                      pAclQosInfo->QosIds.u4MeterId,
                      pAclQosInfo->u4ProtocolRate);
        QoSUnLock ();
        return (QOS_FAILURE);
    }

    nmhSetFsQoSMeterCIR (pAclQosInfo->QosIds.u4MeterId,
                         pAclQosInfo->u4ProtocolRate);

    nmhSetFsQoSMeterStatus (pAclQosInfo->QosIds.u4MeterId, ACTIVE);

    /*Table Name: fsQoSPolicyMapTable */
    if (nmhGetFsQoSPolicyMapStatus (pAclQosInfo->QosIds.u4PolicyMapId,
                                    &i4RowStatus) == SNMP_FAILURE)
    {
        /* Create new Entry */
        if (nmhTestv2FsQoSPolicyMapStatus (&u4ErrorCode,
                                           pAclQosInfo->QosIds.u4PolicyMapId,
                                           CREATE_AND_WAIT) == SNMP_FAILURE)
        {
            QOS_TRC_ARG3 (ALL_FAILURE_TRC, "QOSMFC:Failed in "
                          "nmhTestv2FsQoSPolicyMapStatus in %s and line %d"
                          "and PolicyMapId[%d]\n", __FUNCTION__, __LINE__,
                          pAclQosInfo->QosIds.u4PolicyMapId);
            QoSUnLock ();
            return (QOS_FAILURE);
        }

        if (nmhSetFsQoSPolicyMapStatus (pAclQosInfo->QosIds.u4PolicyMapId,
                                        CREATE_AND_WAIT) == SNMP_FAILURE)
        {
            QOS_TRC_ARG3 (ALL_FAILURE_TRC, "QOSMFC:Failed in "
                          "nmhSetFsQoSPolicyMapStatus in %s and line %d"
                          "and PolicyMapId[%d]\n", __FUNCTION__, __LINE__,
                          pAclQosInfo->QosIds.u4PolicyMapId);
            QoSUnLock ();
            return (QOS_FAILURE);
        }
    }
    else if (i4RowStatus == ACTIVE)
    {
        if (nmhTestv2FsQoSPolicyMapStatus (&u4ErrorCode,
                                           pAclQosInfo->QosIds.u4PolicyMapId,
                                           NOT_IN_SERVICE) == SNMP_FAILURE)
        {
            QOS_TRC_ARG3 (ALL_FAILURE_TRC, "QOSMFC:Failed in "
                          "nmhTestv2FsQoSPolicyMapStatus in %s and line %d"
                          "and PolicyMapId[%d]\n", __FUNCTION__, __LINE__,
                          pAclQosInfo->QosIds.u4PolicyMapId);
            QoSUnLock ();
            return (QOS_FAILURE);
        }

        if (nmhSetFsQoSPolicyMapStatus (pAclQosInfo->QosIds.u4PolicyMapId,
                                        NOT_IN_SERVICE) == SNMP_FAILURE)
        {
            QOS_TRC_ARG3 (ALL_FAILURE_TRC, "QOSMFC:Failed in "
                          "nmhSetFsQoSPolicyMapStatus in %s and line %d"
                          "and PolicyMapId[%d]\n", __FUNCTION__, __LINE__,
                          pAclQosInfo->QosIds.u4PolicyMapId);
            QoSUnLock ();
            return (QOS_FAILURE);
        }
    }

    /* FsQoSPolicyMapCLASS */
    if (nmhTestv2FsQoSPolicyMapCLASS (&u4ErrorCode,
                                      pAclQosInfo->QosIds.u4PolicyMapId,
                                      pAclQosInfo->QosIds.u4ClassId)
        == SNMP_FAILURE)
    {
        QOS_TRC_ARG2 (ALL_FAILURE_TRC, "QOSMFC:Failed in "
                      "nmhTestv2FsQoSPolicyMapCLASS in %s and line %d\n",
                      __FUNCTION__, __LINE__);
        QOS_TRC_ARG3 (ALL_FAILURE_TRC, "ErrorCode[%d] PolicyMapId[%d]"
                      "ClassId[%d]\n", u4ErrorCode,
                      pAclQosInfo->QosIds.u4PolicyMapId,
                      pAclQosInfo->QosIds.u4ClassId);
        QoSUnLock ();
        return (QOS_FAILURE);
    }
    nmhSetFsQoSPolicyMapCLASS (pAclQosInfo->QosIds.u4PolicyMapId,
                               pAclQosInfo->QosIds.u4ClassId);
    /* Set fsQoSPolicyMapPHBType value to zero (0) */
    if (nmhTestv2FsQoSPolicyMapPHBType (&u4ErrorCode,
                                        pAclQosInfo->QosIds.u4PolicyMapId,
                                        i4PriType) == SNMP_FAILURE)
    {
        QOS_TRC_ARG2 (ALL_FAILURE_TRC, "QOSMFC:Failed in "
                      "nmhTestv2FsQoSPolicyMapPHBType in %s and line %d\n",
                      __FUNCTION__, __LINE__);
        QOS_TRC_ARG3 (ALL_FAILURE_TRC, "ErrorCode[%d] PolicyMapId[%d]"
                      "PriType[%d]\n", u4ErrorCode,
                      pAclQosInfo->QosIds.u4PolicyMapId, i4PriType);
        QoSUnLock ();
        return (QOS_FAILURE);
    }

    nmhSetFsQoSPolicyMapPHBType (pAclQosInfo->QosIds.u4PolicyMapId, i4PriType);

    /*Set Meter ID */
    if (nmhTestv2FsQoSPolicyMapMeterTableId (&u4ErrorCode,
                                             pAclQosInfo->QosIds.u4PolicyMapId,
                                             pAclQosInfo->QosIds.u4MeterId)
        == SNMP_FAILURE)
    {
        QOS_TRC_ARG2 (ALL_FAILURE_TRC, "QOSMFC:Failed in "
                      "nmhTestv2FsQoSPolicyMapMeterTableId in %s and line %d\n",
                      __FUNCTION__, __LINE__);
        QOS_TRC_ARG3 (ALL_FAILURE_TRC, "ErrorCode[%d] PolicyMapId[%d]"
                      "MeterId[%d]\n", u4ErrorCode,
                      pAclQosInfo->QosIds.u4PolicyMapId,
                      pAclQosInfo->QosIds.u4MeterId);
        QoSUnLock ();
        return (QOS_FAILURE);
    }

    nmhSetFsQoSPolicyMapMeterTableId (pAclQosInfo->QosIds.u4PolicyMapId,
                                      pAclQosInfo->QosIds.u4MeterId);

    /* InProfile Confirm Action flag-FsQoSPolicyMapInProfileConformActionFlag */
    pOctStrActFlag = allocmem_octetstring (QOS_POLICY_ACTION_FLAG_LENGTH);
    if (pOctStrActFlag == NULL)
    {
        QOS_TRC (ALL_FAILURE_TRC,
                 "pOctStrActFlag:allocmem_octetstring is NULL!!\n");
        QoSUnLock ();
        return (QOS_FAILURE);
    }
    MEMSET (pOctStrActFlag->pu1_OctetList, 0x0, pOctStrActFlag->i4_Length);
    MEMCPY (pOctStrActFlag->pu1_OctetList, &u1InProfConfrAct,
            QOS_POLICY_ACTION_FLAG_LENGTH);
    if (nmhTestv2FsQoSPolicyMapInProfileConformActionFlag
        (&u4ErrorCode,
         pAclQosInfo->QosIds.u4PolicyMapId, pOctStrActFlag) == SNMP_FAILURE)
    {
        QOS_TRC_ARG2 (ALL_FAILURE_TRC, "QOSMFC:Failed in "
                      "nmhTestv2FsQoSPolicyMapInProfileConformActionFlag"
                      "in %s and line %d\n", __FUNCTION__, __LINE__);
        QOS_TRC_ARG3 (ALL_FAILURE_TRC, "ErrorCode[%d] PolicyMapId[%d]"
                      "InProfConfrAct[%d]\n", u4ErrorCode,
                      pAclQosInfo->QosIds.u4PolicyMapId, u1InProfConfrAct);
        free_octetstring (pOctStrActFlag);
        QoSUnLock ();
        return (QOS_FAILURE);
    }
    if (nmhSetFsQoSPolicyMapInProfileConformActionFlag
        (pAclQosInfo->QosIds.u4PolicyMapId, pOctStrActFlag) == SNMP_FAILURE)
    {
        QOS_TRC_ARG2 (ALL_FAILURE_TRC, "QOSMFC:Failed in "
                      "nmhSetFsQoSPolicyMapInProfileConformActionFlag"
                      "in %s and line %d\n", __FUNCTION__, __LINE__);
        QOS_TRC_ARG2 (ALL_FAILURE_TRC, "PolicyMapId[%d] InProExcdAct[%d]\n",
                      pAclQosInfo->QosIds.u4PolicyMapId, u1InProfConfrAct);
        free_octetstring (pOctStrActFlag);
        QoSUnLock ();
        return (QOS_FAILURE);
    }

    /* Confirm action fsQoSPolicyMapConformActionSetVlanPrio */
    if (nmhTestv2FsQoSPolicyMapConformActionSetVlanPrio
        (&u4ErrorCode,
         pAclQosInfo->QosIds.u4PolicyMapId,
         pAclQosInfo->u1CpuQueueNo) == SNMP_FAILURE)
    {
        QOS_TRC_ARG2 (ALL_FAILURE_TRC, "QOSMFC:Failed in "
                      "nmhTestv2FsQoSPolicyMapConformActionSetVlanPrio"
                      "in %s and line %d\n", __FUNCTION__, __LINE__);
        QOS_TRC_ARG3 (ALL_FAILURE_TRC, "ErrorCode[%d] PolicyMapId[%d]"
                      "CpuQueueNo[%d]\n", u4ErrorCode,
                      pAclQosInfo->QosIds.u4PolicyMapId,
                      pAclQosInfo->u1CpuQueueNo);
        free_octetstring (pOctStrActFlag);
        QoSUnLock ();
        return (QOS_FAILURE);
    }

    nmhSetFsQoSPolicyMapConformActionSetVlanPrio
        (pAclQosInfo->QosIds.u4PolicyMapId, pAclQosInfo->u1CpuQueueNo);

    /* FsQoSPolicyMapInProfileExceedActionFlag - made as DROP */
    MEMSET (pOctStrActFlag->pu1_OctetList, 0x0, pOctStrActFlag->i4_Length);
    MEMCPY (pOctStrActFlag->pu1_OctetList, &u1InProExcdAct,
            QOS_POLICY_ACTION_FLAG_LENGTH);
    if (nmhTestv2FsQoSPolicyMapInProfileExceedActionFlag
        (&u4ErrorCode,
         pAclQosInfo->QosIds.u4PolicyMapId, pOctStrActFlag) == SNMP_FAILURE)
    {
        QOS_TRC_ARG2 (ALL_FAILURE_TRC, "QOSMFC:Failed in "
                      "nmhTestv2FsQoSPolicyMapInProfileExceedActionFlag"
                      "in %s and line %d\n", __FUNCTION__, __LINE__);
        QOS_TRC_ARG3 (ALL_FAILURE_TRC, "ErrorCode[%d] PolicyMapId[%d]"
                      "InProExcdAct[%d]\n", u4ErrorCode,
                      pAclQosInfo->QosIds.u4PolicyMapId, u1InProExcdAct);
        free_octetstring (pOctStrActFlag);
        QoSUnLock ();
        return (QOS_FAILURE);
    }

    nmhSetFsQoSPolicyMapInProfileExceedActionFlag
        (pAclQosInfo->QosIds.u4PolicyMapId, pOctStrActFlag);

    /* FsQoSPolicyMapOutProfileActionFlag - Made as DROP */
    MEMSET (pOctStrActFlag->pu1_OctetList, 0x0, pOctStrActFlag->i4_Length);
    MEMCPY (pOctStrActFlag->pu1_OctetList, &u1OutProAct,
            QOS_POLICY_ACTION_FLAG_LENGTH);
    if (nmhTestv2FsQoSPolicyMapOutProfileActionFlag
        (&u4ErrorCode,
         pAclQosInfo->QosIds.u4PolicyMapId, pOctStrActFlag) == SNMP_FAILURE)
    {
        QOS_TRC_ARG2 (ALL_FAILURE_TRC, "QOSMFC:Failed in "
                      "nmhTestv2FsQoSPolicyMapOutProfileActionFlag"
                      "in %s and line %d\n", __FUNCTION__, __LINE__);
        QOS_TRC_ARG3 (ALL_FAILURE_TRC, "ErrorCode[%d] PolicyMapId[%d]"
                      "u1OutProAct[%d]\n", u4ErrorCode,
                      pAclQosInfo->QosIds.u4PolicyMapId, u1OutProAct);
        free_octetstring (pOctStrActFlag);
        QoSUnLock ();
        return (QOS_FAILURE);
    }
    nmhSetFsQoSPolicyMapOutProfileActionFlag
        (pAclQosInfo->QosIds.u4PolicyMapId, pOctStrActFlag);
    free_octetstring (pOctStrActFlag);

    /* FsQoSPolicyMapStatus make ACTIVE */
    if (nmhSetFsQoSPolicyMapStatus (pAclQosInfo->QosIds.u4PolicyMapId,
                                    ACTIVE) == SNMP_FAILURE)
    {
        QOS_TRC_ARG3 (ALL_FAILURE_TRC, "QOSMFC:Failed in "
                      "nmhSetFsQoSPolicyMapStatus in %s and line %d"
                      "and PolicyMapId[%d]\n", __FUNCTION__, __LINE__,
                      pAclQosInfo->QosIds.u4PolicyMapId);
        QoSUnLock ();
        return (QOS_FAILURE);
    }

    QOS_TRC (MGMT_TRC, "QOSMFC Function QosApiAddMfcRateLimiter Exit\n");

    QoSUnLock ();
    return (QOS_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QosApiDeleteMfcRateLimiter                              */
/*                                                                           */
/* Description        : This function is called from the ACL Module          */
/*                            to delete  MFC QoS configuration parameters    */
/*                            for added ACL rule                             */
/*                                                                           */
/* Input(s)           : u4FilterType - Acl filter type (L2 or L3)            */
/*                      pAclQosMapInfo - Pointer to Acl QoS common info      */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : QOS_SUCCESS / QOS_FAILURE                            */
/*                                                                           */
/* Called By          : ACL Module                                           */
/*****************************************************************************/
INT4
QosApiDeleteMfcRateLimiter (UINT4 u4FilterType, tAclQosMapInfo * pAclQosMapInfo)
{
    UINT4               u4ErrorCode = SNMP_ERR_NO_ERROR;
    tAclQosMapInfo     *pAclQosInfo = NULL;

    UNUSED_PARAM (u4FilterType);

    QOS_TRC (MGMT_TRC, "QOSMFC Function QosApiDeleteMfcRateLimiter Entry\n");

    if (pAclQosMapInfo == NULL)
    {
        QOS_TRC_ARG1 (ALL_FAILURE_TRC,
                      "QOSMFC pAclQosMapInfo is NULL line:%d\n", __LINE__);
        return (QOS_FAILURE);
    }
    QoSLock ();
    pAclQosInfo = pAclQosMapInfo;

    /* DESTROY:fsQoSPolicyMapTable */
    if (nmhTestv2FsQoSPolicyMapStatus (&u4ErrorCode,
                                       pAclQosInfo->QosIds.u4PolicyMapId,
                                       DESTROY) == SNMP_FAILURE)
    {
        QOS_TRC_ARG3 (ALL_FAILURE_TRC, "QOSMFC:Failed in "
                      "nmhTestv2FsQoSPolicyMapStatus in %s and line %d"
                      "PolicyMapId[%d]\n", __FUNCTION__, __LINE__,
                      pAclQosInfo->QosIds.u4PolicyMapId);
        QoSUnLock ();
        return (QOS_FAILURE);
    }

    nmhSetFsQoSPolicyMapStatus (pAclQosInfo->QosIds.u4PolicyMapId, DESTROY);

    /* DESTROY:fsQoSMeterTable */
    if (nmhTestv2FsQoSMeterStatus (&u4ErrorCode,
                                   pAclQosInfo->QosIds.u4MeterId,
                                   DESTROY) == SNMP_FAILURE)
    {
        QOS_TRC_ARG3 (ALL_FAILURE_TRC, "QOSMFC:Failed in "
                      "nmhTestv2FsQoSMeterStatus in %s and line %d"
                      "PolicyMapId[%d]\n", __FUNCTION__, __LINE__,
                      pAclQosInfo->QosIds.u4PolicyMapId);
        QoSUnLock ();
        return (QOS_FAILURE);
    }

    nmhSetFsQoSMeterStatus (pAclQosInfo->QosIds.u4MeterId, DESTROY);

    /* DESTROY:fsQoSClassMapTable */
    if (nmhTestv2FsQoSClassMapStatus (&u4ErrorCode,
                                      pAclQosInfo->QosIds.u4ClassMapId,
                                      DESTROY) == SNMP_FAILURE)
    {
        QOS_TRC_ARG3 (ALL_FAILURE_TRC, "QOSMFC:Failed in "
                      "nmhTestv2FsQoSClassMapStatus in %s and line %d"
                      "PolicyMapId[%d]\n", __FUNCTION__, __LINE__,
                      pAclQosInfo->QosIds.u4PolicyMapId);
        QoSUnLock ();
        return (QOS_FAILURE);
    }

    nmhSetFsQoSClassMapStatus (pAclQosInfo->QosIds.u4ClassMapId, DESTROY);

    QOS_TRC (MGMT_TRC, "QOSMFC Function QosApiDeleteMfcRateLimiter Exit\n");

    QoSUnLock ();
    return (QOS_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QosApiConfigEts                                      */
/* Description        : This function is used to configure the bandwidth for */
/*                      queue associated with the give priority.             */
/*                      1. Get the Queue Id from the priority                */
/*                      2. Call QoSHwQueueCreate API for                     */
/*                           Configuring the bandwidth in hardware           */
/* Input(s)           : u4IfIndex -Interface Index.                          */
/*                    : u4PriIdx  - Priority value. There should be a        */
/*                                  one to one mapping between priority      */
/*                                  grouping in ETS and QOS. With that       */
/*                                  priority information only corresponding  */
/*                                  queue will be identified by QOS Module   */
/*                                  (from QMap Table)                        */
/*                                  for configuring the ETS bandwidth value  */
/*                                  for the queue.                           */
/*                    : u1EtsTCGID- Traffic Class Group ID.There should be a */
/*                                  one to one mapping between traffic class */
/*                                  grouping in ETS and QOS queue.           */
/*                                  Depending on the TCGID the scheduling    */
/*                                  algoriithm will be selected for the      */
/*                                  queue.                                   */
/*                    : u2EtsBw   - ETS Bandwidth which will be configured   */
/*                                  as queue weight.The value will           */
/*                                  come as % value                          */
/*                                  this value is percentage.                */
/*                    : u1EtsPortStatus  - ETS Enabled Status on Port.       */
/*                                  If ETS is enabled then ETS bandwidth     */
/*                                  will be configured as queue weight.      */
/*                                  Otherwise queue will be programmed       */
/*                                  with its default vale.                   */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/* Return Value(s)    : QOS_SUCCESS or QOS_FAILURE                           */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QosApiConfigEts (UINT4 u4IfIndex, UINT4 u4PriIdx,
                 UINT1 u1EtsTCGID, UINT2 u2EtsBw, UINT1 u1EtsPortStatus)
{
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (u4PriIdx);
    UNUSED_PARAM (u1EtsTCGID);
    UNUSED_PARAM (u2EtsBw);
    UNUSED_PARAM (u1EtsPortStatus);
    return QOS_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : QosApiConfigEtsTsa                                   */
/* Description        : This function is used to configure the bandwidth for */
/*                      queue associated with the give priority.             */
/*                      1. Get the Queue Id from the priority                */
/*                      2. Call QoSHwQueueCreate API for                     */
/*                           Configuring the bandwidth in hardware           */
/* Input(s)           : pPortEntry - Containing the Required data            */
/*                      to be programmed into HW.                            */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/* Return Value(s)    : QOS_SUCCESS or QOS_FAILURE                           */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QosApiConfigEtsTsa (tQosEtsHwEntry * pPortEntry)
{
    tQoSQNode          *pQNode = NULL;
    tQoSSchedNode      *pSchedNode = NULL;
    UINT1               u1ConFlag = QOS_FALSE;
    UINT1               u1EtsTCGID = QOS_FALSE;
    UINT2               u2EtsBw = QOS_FALSE;
    UINT1               u1EtsTsa = QOS_FALSE;

    QoSLock ();
    if (pPortEntry->u1ETSTsa == QOS_SCHED_NOT_SUPPORTED_ALGO)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : TSA Algorithm is not supported\r\n",
                      __FUNCTION__);
        QoSUnLock ();
        return QOS_FAILURE;
    }

    u2EtsBw = pPortEntry->u1ETSBw;
    u1EtsTsa = pPortEntry->u1ETSTsa;
    u1EtsTCGID = pPortEntry->u1EtsTCGID;

    /*Get the Queue Id using the Incoming Priority from the QMap Table */
    pQNode = QosGetQFromPri ((INT4)pPortEntry->u4IfIndex, pPortEntry->u4PriIdx);
    if (pQNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetQFromPri () "
                      " Returns FAILURE. \r\n", __FUNCTION__);
        QoSUnLock ();
        return QOS_FAILURE;
    }

    /*Update the Queue Weight and Scheduler Id */
    pSchedNode =
        QoSUtlGetSchedNode ((INT4)pPortEntry->u4IfIndex, pQNode->u4SchedulerId);
    if (pSchedNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetSchedNode () "
                      " Returns FAILURE. \r\n", __FUNCTION__);
        QoSUnLock ();
        return QOS_FAILURE;
    }

    if (pPortEntry->u1EtsPortStatus == ETS_ENABLED)
    {
        /*Configure the Queue with the configured ETS Bandwidth */
        /*Default Traffic Class Group ID Configuration */
        if (u1EtsTCGID == ETS_DEF_TCGID)
        {
            pQNode->u2Weight = QOS_INIT_VAL;
            u1ConFlag = QOS_TRUE;
        }
        else
        {
            if (u1EtsTCGID < ETS_MAX_TCGID_CONF)
            {
                /*Since Qnode Weight is configured in 1000 notataion
                   Converting DCB Weight (100 notation) to 1000 notation. */
                u2EtsBw = (u2EtsBw * QOS_CHANGE_HUN_NOTATION);
                if (pQNode->u2Weight != u2EtsBw)
                {
                    pQNode->u2Weight = u2EtsBw;
                    u1ConFlag = QOS_TRUE;
                }
            }
        }

        /* Set the SchedAlgorithm as per the ETS TSA table entry */
        if (pSchedNode->u1SchedAlgorithm != u1EtsTsa)
        {
            pSchedNode->u1SchedAlgorithm = u1EtsTsa;
        }
    }
    else
    {
        /*if ETS is disbaled ,Configure the Queue with the deafult Values */
        if (pSchedNode->u1SchedAlgorithm != QOS_SCHED_ALGO_SRR)
        {
            pSchedNode->u1SchedAlgorithm = QOS_SCHED_ALGO_SRR;
            pQNode->u2Weight = QOS_INIT_VAL;
            u1ConFlag = QOS_TRUE;
        }
    }

    /*Program the Hw with the Configured ETS Values */
    if (u1ConFlag == QOS_TRUE)
    {
        QosUtlHwCfgEtsParams (pQNode);
        u1ConFlag = QOS_FALSE;
    }
    QoSUnLock ();
    return QOS_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : QosApiConfigPfc                                      */
/* Description        : This function is used to call the PFC NPAPI          */
/*                      The u1PfcHwCfgFlag in tQosPfcHwEntry will            */
/*                      tell about the PFC operation.                        */
/*                                                                           */
/*                      QOS_PFC_PORT_CFG - PFC Enabled/Disabled              */
/*                                         configuration for                 */
/*                                         all priorities on port            */
/*                      QOS_PFC_DEL_PROFILE - PFC Profile Delete             */
/*                      QOS_PFC_PORT_CFG - PFC Threshold configuration       */
/*                                         for a switch                      */
/* Input(s)           : pPfcHwEntry - Pointer to the Pfc Hardware Entry      */
/*                              to be updated in the Hardware                */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/* Return Value(s)    : QOS_SUCCESS or QOS_FAILURE                           */
/* Called By          : PFC Utility functions                                */
/* Calling Function   :                                                      */
/*****************************************************************************/

INT4
QosApiConfigPfc (tQosPfcHwEntry * pPfcHwEntry)
{

    QoSLock ();
    if (pPfcHwEntry->u1PfcHwCfgFlag == QOS_PFC_PORT_CFG)
    {
        QosxUtlFillPriToQidMap (pPfcHwEntry->u4Ifindex,
                                pPfcHwEntry->au4PriToQueMap);
    }
#ifdef NPAPI_WANTED
    if (QosxFsQosHwConfigPfc (pPfcHwEntry) != QOS_SUCCESS)
    {
        QoSUnLock ();
        return OSIX_FAILURE;
    }
#endif

    QoSUnLock ();
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : QosApiGetQoSPortDefUsrPri                            */
/* Description        : This function is used to get the Port's Default      */
/*                      User priority                                        */
/* Input(s)           : i4IfIndex -Port                                      */
/* Output(s)          : u1UDPri - Default User priority of the Port          */
/* Global Variables                                                          */
/* Referred           : QoSUtlGetDUPNode                                     */
/* Return Value(s)    : QOS_SUCCESS or QOS_FAILURE                           */
/*****************************************************************************/
INT4
QosApiGetQoSPortDefUsrPri (INT4 i4IfIndex, UINT1 *u1UDPri)
{
    UINT1              *pu1PriVal = NULL;
    INT4                i4ReturnValue = QOS_FAILURE;

    QoSLock ();
    pu1PriVal = QoSUtlGetDUPNode (i4IfIndex);
    if (pu1PriVal != NULL)
    {
        *u1UDPri = *pu1PriVal;
        i4ReturnValue = QOS_SUCCESS;
    }
    else
    {
        i4ReturnValue = QOS_FAILURE;
    }
    QoSUnLock ();
    return i4ReturnValue;
}

/*****************************************************************************/
/* Function Name      : QosApiGetMaxTCSupport                                */
/* Description        : This function is used to give the maximum            */
/*                      number of Traffic Class Supported by QOS.            */
/* Input(s)           : u4Ifindex -Interface Index                           */
/* Output(s)          : u4MaxNumTcSupport -Maximum number of Traffic         */
/*                       Class Supported by the interface                    */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/* Return Value(s)    : QOS_SUCCESS or QOS_FAILURE                           */
/* Called By          : PFC Utility functions                                */
/* Calling Function   :                                                      */
/*****************************************************************************/

INT4
QosApiGetMaxTCSupport (UINT4 u4IfIndex, UINT4 *pu4MaxNumTcSupport)
{
    UNUSED_PARAM (u4IfIndex);
    *pu4MaxNumTcSupport = QOS_MAX_TC_SUPPORT;
    return QOS_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : QosApiConfigAppPri                                   */
/* Description        : This function is used to create a Class Map entry    */
/*                      with next free class map id and map the incoming     */
/*                      Filter Id with that class-map Id. Also it creates an */
/*                      entry in the class to priority map and assigns       */
/*                      priority for the class-map created.                  */
/* Input(s)           : pClassMapPriInfo - pointer to ClassMapInfo        */
/* Output(s)          :                                                      */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/* Return Value(s)    : QOS_SUCCESS or QOS_FAILURE                           */
/* Called By          : Applicationm Priority Utility functions              */
/* Calling Function   :                                                      */
/*****************************************************************************/

INT4
QosApiConfigAppPri (tQosClassMapPriInfo * pClassMapPriInfo,
                    UINT1 u1FilterAction)
{
    tQoSClassMapNode       ClsMapNode;
    tQoSClassMapNode       *pClsMapNode = NULL;
    tQoSClass2PriorityNode *pCls2PriNode = NULL;
    tQoSClassInfoNode      *pClsInfoNode = NULL;
    tQoSPolicyMapNode      *pPlyMapNode = NULL;
    INT4                   i4RetStatus = 0;
    UINT1                  au1FilterName[QOS_MAX_TABLE_NAME] = { '\0' };
    UINT4                  u4ClassMapIndex = 0;
    UINT4                  u4QoSPolicyMapId = 1; /* 1-65535 */
    UINT4                  u4Class = 0;
    UINT1                  u1Flag = FALSE;

    QoSLock ();

    if (u1FilterAction == ISS_FILTER_CREATE)
    {
        /* Get Free ClassMap Index */
        while (u4ClassMapIndex < QOS_CLS_MAP_TBL_MAX_INDEX_RANGE)
        {
            u4ClassMapIndex++;
            ClsMapNode.u4Id = u4ClassMapIndex;
            pClsMapNode = (tQoSClassMapNode *)
                RBTreeGet (gQoSGlobalInfo.pRbClsMapTbl, &ClsMapNode);
            if (pClsMapNode == NULL)
            {
                u1Flag = TRUE;
                break;
            }

        }
        if (u1Flag != TRUE)
        {
            QOS_TRC_ARG1 (MGMT_TRC, "In %s : "
                          "No free Class Map Index Found \r\n", __FUNCTION__);
            QoSUnLock ();
            return QOS_FAILURE;
        }

        /* Get next Free Class available 
         * For each class map entry created, a new class is mapped.*/
        while (u4Class < QOS_CLS2PRI_MAP_TBL_MAX_INDEX_RANGE)
        {
            u4Class++;
            pClsInfoNode = QoSUtlGetClassInfoNode (u4Class);
            if (pClsInfoNode == NULL)
            {
                u1Flag = TRUE;
                break;
            }

        }

        if (u1Flag != TRUE)
        {
            QOS_TRC_ARG1 (MGMT_TRC, "In %s : "
                          "No free Class Found \r\n", __FUNCTION__);
            QoSUnLock ();
            return QOS_FAILURE;
        }

        QOS_TRC_ARG2 (MGMT_TRC, "In %s : "
                "Create Class Map entry with Free Index %d\r\n", __func__, u4ClassMapIndex);

        /*Create a Class map entry with Next Free Calss Map Index */
        pClsMapNode = QoSCreateClsMapTblEntry (u4ClassMapIndex);

        if (pClsMapNode == NULL)
        {
            QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSCreateClsMapTblEntry () "
                          "Returns FAILURE. \r\n", __FUNCTION__);
            QoSUnLock ();
            return (QOS_FAILURE);
        }
        
        QOS_TRC_ARG3 (MGMT_TRC, "In %s : "
                "ClassMap entry created with\r\n pClsMapNode->u4Id = %d FilterId = %d\r\n",
                __func__, pClsMapNode->u4Id, pClassMapPriInfo->u4FilterId);

        pClsMapNode->u1Status = NOT_READY;
        pClsMapNode->b1IsClsMapConfFromExt = TRUE;

        if (pClassMapPriInfo->u1FilterType == ISS_L2_FILTER)
        {
            /*Map ClassId with Filter Id */
            pClsMapNode->u4L2FilterId = pClassMapPriInfo->u4FilterId;
        }
        else                    /* ISS_L3_FILTER */
        {
            pClsMapNode->u4L3FilterId = pClassMapPriInfo->u4FilterId;
        }

        QOS_TRC_ARG2 (MGMT_TRC, "In %s : "
                "Add Filter with Free class Index (pClsMapNode->u4ClassId) = "
                "%d\r\n", __func__, u4Class);

        /* Setting Class for ClassMap Entry */
        i4RetStatus = QoSClsInfoAddFilters (u4Class, pClsMapNode);
        if (i4RetStatus == QOS_FAILURE)
        {
            QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSClsInfoAddFilters () "
                          "Returns FAILURE. \r\n", __FUNCTION__);

            QoSUnLock ();
            return (QOS_FAILURE);
        }
        pClsMapNode->u4ClassId = u4Class;

        /* Validate the class map entry created */
        if (QoSValidateClsMapTblEntry (pClsMapNode) == QOS_FAILURE)
        {
            QOS_TRC (MGMT_TRC,
                     "QoSValidateClsMapTblEntry returned Failure\r\n");
            QoSUnLock ();
            return QOS_FAILURE;
        }
        pClsMapNode->u1Status = ACTIVE;

        QOS_TRC_ARG2 (MGMT_TRC, "In %s : "
                "Create Class2Pri Tbl entry with Index %d\r\n", __FUNCTION__, u4Class);
        /* Create an entry in Class to Priority Map table to map the priority
         * with the class created*/

        pCls2PriNode = QoSCreateCls2PriTblEntry (u4Class);

        if (pCls2PriNode == NULL)
        {
            QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSCreateCls2PriTblEntry () "
                          " Returns FAILURE. \r\n", __FUNCTION__);
            QoSUnLock ();
            return (QOS_FAILURE);
        }

        /*Assign a Group name for the class to priority map entry created */
        SPRINTF ((CHR1 *) au1FilterName, "CLASS2PRIMAP-%d",
                 pClassMapPriInfo->u4FilterId);
        STRNCPY (pCls2PriNode->au1GroupName, au1FilterName,
                 MEM_MAX_BYTES (STRLEN (au1FilterName), QOS_MAX_TABLE_NAME));

        pCls2PriNode->u1Status = NOT_READY;

        /* 1. Check(Test) Dependency for this Entry.   */
        if (QoSValidateCls2PriTblEntry (pCls2PriNode) == QOS_FAILURE)
        {
            QOS_TRC (MGMT_TRC,
                     "QoSValidateCls2PriTblEntry returned failure\r\n");
            QoSUnLock ();
            return QOS_FAILURE;
        }
        /* Assign Priority for this class */
        pCls2PriNode->u1RegenPri = pClassMapPriInfo->u1Priority;
        pCls2PriNode->u1Status = ACTIVE;
    }/* ISS_FILTER_CREATE */
    else if ((u1FilterAction == ISS_FILTER_UPDATE) ||
             (u1FilterAction == ISS_FILTER_DELETE))
    {
        /* Get the Class Map Id corresponding to the Filter Id */
        pClsMapNode =
            (tQoSClassMapNode *) RBTreeGetFirst (gQoSGlobalInfo.pRbClsMapTbl);
        while (pClsMapNode != NULL)
        {
            /* Verify FilterId is associated to this Class Map node. */
            if (pClassMapPriInfo->u1FilterType == ISS_L2_FILTER)
            {
                if (pClsMapNode->u4L2FilterId == pClassMapPriInfo->u4FilterId)
                {
                    break;
                }
            }
            else if (pClassMapPriInfo->u1FilterType == ISS_L3_FILTER)
            {
                if (pClsMapNode->u4L3FilterId == pClassMapPriInfo->u4FilterId)
                {
                    break;
                }
            }

            pClsMapNode = (tQoSClassMapNode *)
                RBTreeGetNext (gQoSGlobalInfo.pRbClsMapTbl,
                               pClsMapNode, QoSClsMapCmpFun);
        }
        if (pClsMapNode == NULL)
        {
            QOS_TRC (MGMT_TRC, "Class Map ID for Filter not found to delete.\r\n");
            QoSUnLock ();
            return QOS_SUCCESS;
        }
        
        /* Get the Class Map Id corresponding to the Filter Id */
        pPlyMapNode = (tQoSPolicyMapNode *)
            RBTreeGetFirst (gQoSGlobalInfo.pRbPlyMapTbl);
        while (pPlyMapNode != NULL)
        {
            if ((pPlyMapNode->u4ClassId == pClsMapNode->u4ClassId) &&
                (pPlyMapNode->u1PHBType == QOS_PLY_PHB_TYPE_VLAN_PRI))
            {
                break; 
            }

            pPlyMapNode = (tQoSPolicyMapNode *)
                RBTreeGetNext (gQoSGlobalInfo.pRbPlyMapTbl,
                        pPlyMapNode, NULL);
        }
        if (pPlyMapNode == NULL)
        {
            QOS_TRC (MGMT_TRC, "In QosApiConfigAppPri:"
                    "No matching policy map node found.\r\n");
            QoSUnLock ();
            return QOS_FAILURE;
        }

        if (u1FilterAction == ISS_FILTER_UPDATE)
        {
            pPlyMapNode->u2DefaultPHB = pClassMapPriInfo->u1Priority;

            i4RetStatus = QoSHwWrUpdatePolicyMapForClass (pPlyMapNode,
                                        QOS_UPDATE_PLY_PHB_TYPE);
            if (i4RetStatus == QOS_FAILURE)
            {
                QOS_TRC (MGMT_TRC, "In QosApiConfigAppPri:"
                        "QoSHwWrUpdatePolicyMapForClass() Returns "
                        "FAILURE. \r\n");

                QoSUnLock ();
                return QOS_FAILURE;
            }
        } /* UPDATE */
        else
        {
            /* Remove the entry from the HARDWARE. */
            i4RetStatus = QoSHwWrUnmapClassFromPolicy (pPlyMapNode, NULL,
                    QOS_PLY_DEL);
            if (i4RetStatus == QOS_FAILURE)
            {
                QOS_TRC (MGMT_TRC, "In QosApiConfigAppPri:"
                        "QoSHwWrUnmapClassFromPolicy () Returns "
                        "FAILURE. \r\n");

                QoSUnLock ();
                return QOS_FAILURE;
            }

            /* Remove the entry from the SOFTWARE. */
            i4RetStatus = QoSDeletePlyMapTblEntry (pPlyMapNode);
            if (i4RetStatus == QOS_FAILURE)
            {
                QOS_TRC (MGMT_TRC, "In QosApiConfigAppPri:"
                        "QoSDeletePlyMapTblEntry() Returns "
                        "FAILURE. \r\n");
                QoSUnLock ();
                return QOS_FAILURE;
            }
        } /* DELETE - Policy Map */

        /* Get the corresponding Class */
        u4Class = pClsMapNode->u4ClassId;

        QOS_TRC_ARG2 (MGMT_TRC, "In %s : "
                "Get the Class2Pri node with class Index (pClsMapNode->u4ClassId) = %d\r\n", __FUNCTION__, u4Class);
        /* Get Corresponding Class to Priority map entry and update 
         * the priority */
        pCls2PriNode = QoSUtlGetClass2PriorityNode (u4Class);

        if (pCls2PriNode == NULL)
        {
            QOS_TRC (MGMT_TRC,
                     "QoSUtlGetClass2PriorityNode returned null \r\n");
            QoSUnLock ();
            return QOS_FAILURE;
        }
        if (u1FilterAction == ISS_FILTER_UPDATE)
        {
            /* Assign Priority for this class */
            pCls2PriNode->u1RegenPri = pClassMapPriInfo->u1Priority;
        }
        else                    /*if (u1FilterAction == ISS_FILTER_DELETE) */
        {
            QOS_TRC_ARG1 (MGMT_TRC, "In %s : "
                    "Deleting the entries \r\n",__FUNCTION__);
            /*Delete the Cls2PriMapping */
            if (QoSDeleteCls2PriTblEntry (pCls2PriNode) != QOS_SUCCESS)
            {
                QOS_TRC_ARG1 (MGMT_TRC,
                      "QoSDeleteCls2PriTblEntry returned failure Line = %d\r\n", 
                      __LINE__);
                QoSUnLock ();
                return QOS_FAILURE;
            }
            /*Delete ClassMapInfo Node */
            if ( QoSClsInfoDelFilters (pClsMapNode) != QOS_SUCCESS)
            {
                QOS_TRC_ARG1 (MGMT_TRC,
                      "QoSClsInfoDelFilters returned Failure Line = %d\r\n", 
                      __LINE__);
                QoSUnLock ();
                return QOS_FAILURE;
            }
            /* Delete Class Map Table Entry */
            if (QoSDeleteClsMapTblEntry (pClsMapNode) != QOS_SUCCESS)
            {
                QOS_TRC_ARG1 (MGMT_TRC,
                      "QoSDeleteClsMapTblEntry returned Failure Line = %d\r\n", 
                       __LINE__);
                QoSUnLock ();
                return QOS_FAILURE;
            }
        }
    }

    /* Policy-Map related functionality */
    if (u1FilterAction == ISS_FILTER_CREATE)
    {
        /* Get next Free policy map available */
        while (QOS_CHECK_PLY_MAP_TBL_INDEX_RANGE(u4QoSPolicyMapId))
        {
            u4QoSPolicyMapId++;
            pPlyMapNode = QoSUtlGetPolicyMapNode (u4QoSPolicyMapId);
            if (pPlyMapNode == NULL)
            {
                u1Flag = TRUE;
                break;
            }
        }
        if (u1Flag != TRUE)
        {
            QOS_TRC_ARG1 (MGMT_TRC, "In %s : "
                    "No free Policy map Found \r\n", __FUNCTION__);
            QoSUnLock ();
            return QOS_FAILURE;
        }

        /*Create a policy map entry with Next Free Map Index */
        pPlyMapNode = QoSCreatePlyMapTblEntry (u4QoSPolicyMapId);
        if (pPlyMapNode == NULL)
        {
            QOS_TRC_ARG1 (MGMT_TRC, "In %s : "
                    "Unable to create Policy map entry\r\n", __FUNCTION__);
            QoSUnLock ();
            return QOS_FAILURE;
        }

        /* Update the required fields to be set in the HW */
        pPlyMapNode->u1PHBType = QOS_PLY_PHB_TYPE_VLAN_PRI; /* Only VLAN PRI is applicable */
        pPlyMapNode->u2DefaultPHB = pClassMapPriInfo->u1Priority; /* Priority */
        pPlyMapNode->u4ClassId = pClsMapNode->u4ClassId;
        pPlyMapNode->u4IfIndex = pClassMapPriInfo->u4IfIndex;
        pPlyMapNode->b1IsPlyMapConfFromExt = TRUE;

        i4RetStatus = QoSHwWrMapClassToPolicy (pPlyMapNode, NULL, QOS_PLY_ADD);
        if (i4RetStatus == QOS_FAILURE)
        {
            QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSHwWrMapClassToPolicy() "
                    "Returns FAILURE. \r\n", __FUNCTION__);

            QoSUnLock ();
            return QOS_FAILURE;
        }
        pPlyMapNode->u1Status = ACTIVE;
    }

    QoSUnLock ();
    return QOS_SUCCESS;
}
/*****************************************************************************/
/* Function Name      : QosGetMplsExpProfileForPortAtIngress                 */
/* Description        : This function is used to Get the Priority map table  */
/*                      Id which is associated with the given  port number   */
/*                      1. Get the First Entry in Priority table,if MPLS     */
/*                      port  value matches the value got, it return MAPID   */
/*                      2. Else, it will traverse through the RB tree for    */
/*                      specific Port                                        */
/* Input(s)           : u4IfNum  - Port Number                               */
/*                      pi4HwExpMapId - Pointer to Priority Map Id           */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : gQoSGlobalInfo                                       */
/* Global Variables                                                          */
/* Modified           : gQoSGlobalInfo                                       */
/* Return Value(s)    : QOS_SUCCESS or QOS_FAILURE                           */
/* Called By          : MPLS                                                 */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QosGetMplsExpProfileForPortAtIngress (UINT4 u4IfNum, INT4 *pi4HwExpMapId)
{
    tQoSInPriorityMapNode *pInPriMapNode = NULL;
    tQoSInPriorityMapNode *pNextInPriMapNode = NULL;
    tQoSInPriorityMapNode InPriMapNode;
    MEMSET (&InPriMapNode, 0, sizeof(tQoSInPriorityMapNode));
    InPriMapNode.u4IfIndex = u4IfNum;
    *pi4HwExpMapId = 0;

    QoSLock();
    pInPriMapNode = (tQoSInPriorityMapNode *)
            RBTreeGetNext (gQoSGlobalInfo.pRbInPriMapTbl,
                            &InPriMapNode, QoSInPriMapCmpFun);

    while (NULL != pInPriMapNode)
    {
            InPriMapNode.u4Id = pInPriMapNode->u4Id;
            if (u4IfNum == pInPriMapNode->u4IfIndex)
            {
                    *pi4HwExpMapId = pInPriMapNode->i4HwExpMapId;
                    break;
            }
            pNextInPriMapNode = (tQoSInPriorityMapNode *)
                    RBTreeGetNext (gQoSGlobalInfo.pRbInPriMapTbl,
                                    &InPriMapNode, QoSInPriMapCmpFun);
            pInPriMapNode = pNextInPriMapNode;

    }
    QoSUnLock();
    return (QOS_SUCCESS);
}
/*****************************************************************************/
/* Function Name      : QosGetMplsExpProfileForPortAtEgress                  */
/* Description        : This function is used to Get the Policy map table    */
/*                      Id which is associated with the given   MPLS tunnel  */
/*                      1. Get the First Entry in Policy table,if MPLS       */
/*                      portIndex  matches the value got, it return MAPID    */
/*                      2. Else, it will traverse through the RB tree for    */
/*                      specific Port                                        */
/* Input(s)           : u4IfNum  - Port Number                               */
/*                      pi4HwExpMapId - Pointer to Priority Map Id           */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : gQoSGlobalInfo                                       */
/* Global Variables                                                          */
/* Modified           : gQoSGlobalInfo                                       */
/* Return Value(s)    : QOS_SUCCESS or QOS_FAILURE                           */
/* Called By          : MPLS                                                 */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QosGetMplsExpProfileForPortAtEgress (UINT4 u4IfNum, INT4 *pi4HwExpMapId)
{
    tQoSPolicyMapNode  *pPlyMapNode = NULL;
    tQoSPolicyMapNode *pNextPlyMapNode = NULL;
    tQoSPolicyMapNode PlyMapNode;

    MEMSET (&PlyMapNode, 0, sizeof(tQoSPolicyMapNode));
    PlyMapNode.u4IfIndex = u4IfNum;
    *pi4HwExpMapId = 0;
    QoSLock();
    pPlyMapNode = (tQoSPolicyMapNode *)
            RBTreeGetNext (gQoSGlobalInfo.pRbPlyMapTbl,
                            &PlyMapNode, QoSPlyMapCmpFun);

    while (NULL != pPlyMapNode)
    {
            PlyMapNode.u4Id = pPlyMapNode->u4Id;
            if (u4IfNum == pPlyMapNode->u4IfIndex)
            {
                    *pi4HwExpMapId = pPlyMapNode->i4HwExpMapId;
                    break;
            }
            pNextPlyMapNode = (tQoSPolicyMapNode *)
                    RBTreeGetNext (gQoSGlobalInfo.pRbPlyMapTbl,
                                    &PlyMapNode, QoSPlyMapCmpFun);
            pPlyMapNode = pNextPlyMapNode;

    }
    QoSUnLock();
    return (QOS_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QosApiCreateSchedular                                */
/* Description        : This function is called from the ETS subSystem       */
/*                      to Create Scheduler Node                             */
/* Input(s)           : u4IfIndex - Interface Index                          */
/*                    : u4SchedulerId - Scheduler ID                         */
/*                    : u4HierarLevel - Scheduler-Hierar Level               */
/*                    : u1Tsa - Scheduler Algorithm                          */
/* Output(s)          : None                                                 */
/* Return Value(s)    : QOS_SUCCESS / QOS_FAILURE                            */
/* Called By          :                                                      */
/*****************************************************************************/
INT4
QosApiCreateSchedular (UINT4 u4IfIndex, UINT4 u4SchedulerId, 
                       UINT4 u4HierarLevel, UINT1 u1Tsa)
{
    INT4               i4RetStatus = QOS_FAILURE;
    UINT4              u4NextLevelSchedulerHwId = 0;
    tQoSSchedNode      *pSchedNode = NULL;
    tQoSSchedNode      *pSchedNextNode = NULL;
    tQoSHierarchyNode  *pHierarchyNode = NULL;
    tQoSHierarchyNode  *pHierarchyTmpNode = NULL;

    if (gQoSGlobalInfo.eSysStatus == QOS_SYS_STATUS_ENABLE)
    {
        /* Create an Entry in the Scheduler Table */
        pSchedNode = QoSCreateSchedTblEntry ((INT4) u4IfIndex,
                u4SchedulerId);
        if (pSchedNode == NULL)
        {
            QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSCreateSchedTblEntry()"
                    " Returns FAILURE.\r\n", __FUNCTION__);
            return (QOS_FAILURE);
        }
        pSchedNode->u1HierarchyLevel = (UINT1) u4HierarLevel;
        pSchedNode->u1SchedAlgorithm = u1Tsa;
        /* Program the Hardware with the Scheduler Parameters */
        i4RetStatus = QoSHwWrSchedulerAdd (pSchedNode->i4IfIndex,
                pSchedNode->u4Id);
        if ((i4RetStatus == QOS_FAILURE) || 
                (i4RetStatus == QOS_NP_NOT_SUPPORTED))
        {
            if (i4RetStatus == QOS_NP_NOT_SUPPORTED)
            {
                CLI_SET_ERR (QOS_CLI_ERR_SCHED_IMPROPER_CFG);
            }
            QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSHwWrSchedulerAdd ()"
                    " Returns FAILURE.\r\n", __FUNCTION__);
            return (QOS_FAILURE);
        }
        pSchedNode->u1Status = ACTIVE;
        /*Creation of Hierarchy Node and Updation of it's Parameters */
        pHierarchyNode =
            QoSCreateHierarchyTblEntry ((INT4) u4IfIndex, u4HierarLevel,
                    u4SchedulerId);
        if (pHierarchyNode == NULL)
        {
            QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSCreateHierarchyTblEntry()"
                    " Returns FAILURE.\r\n", __FUNCTION__);
            return (QOS_FAILURE);
        }
        pSchedNode = NULL;
        pSchedNode =  QoSUtlGetSchedNode ((INT4) u4IfIndex,
                u4SchedulerId);
        if ((pSchedNode != NULL) && 
                (pSchedNode->u1Status == ACTIVE))
        {
            pHierarchyTmpNode = NULL;
            /*In S3 get the next level scheduler(S2) */
            pHierarchyTmpNode =
                QoSUtlGetHierarchyNode ((INT4) u4IfIndex, QOS_S2_SCHEDULER,
                        QOS_HL_DEF_S2_SCHEDULER_ID);
            if (pHierarchyTmpNode == NULL)
            {
                QOS_TRC_ARG1 (MGMT_TRC, "In %s : NULL check failed \r\n",
                        __FUNCTION__);
                return QOS_FAILURE;
            }
            pSchedNextNode = QoSUtlGetSchedNode ((INT4) pHierarchyTmpNode->i4IfIndex,
                    pHierarchyTmpNode->u4SchedId);
            if (pSchedNextNode != NULL)
            {
                /* Map S2 or S3 Schedulers to next level scheduler */
                u4NextLevelSchedulerHwId = pSchedNextNode->u4SchedulerHwId;
                i4RetStatus =
                    QoSHwWrSchedulerHierarchyMap (pHierarchyNode->i4IfIndex,
                            (INT4) pSchedNode->u4SchedulerHwId,
                            pHierarchyNode->u2Weight,
                            pHierarchyNode->u1Priority,
                            u4NextLevelSchedulerHwId,
                            (INT4)pHierarchyNode->u4QNext,
                            pHierarchyNode->u1Level,
                            (INT4)QOS_HIERARCHY_ADD);
                if (i4RetStatus == QOS_FAILURE)
                {
                    QOS_TRC_ARG1 (MGMT_TRC, "In %s : "
                            "QoSHwWrSchedulerHierarchyMap : Add "
                            " () Returns FAILURE. \r\n",
                            __FUNCTION__);
                    return (QOS_FAILURE);
                }
                else
                {
                    /*Increment the count of children associated with the
                     * scheduler.
                     */
                    if (u4NextLevelSchedulerHwId != 0)
                    {
                        pSchedNextNode->u4SchedChildren++;
                    }
                }
                pHierarchyNode->u1Status = ACTIVE;
                QOS_TRC_ARG1 (MGMT_TRC, "In %s : QosApiCreateSchedular ()"
                        " Returns SUCCESS. \r\n", __FUNCTION__);
                return (QOS_SUCCESS);
            }
        }
    }

    QOS_TRC_ARG1 (MGMT_TRC, "In %s : QosApiCreateSchedular ()"
            " Returns FAILURE. \r\n", __FUNCTION__);

    return (QOS_FAILURE);
}

/*****************************************************************************/
/* Function Name      : QosApiUpdateSchedularAlgo                            */
/* Description        : This function is called from the ETS subSystem       */
/*                      to update Scheduler Algorithm                        */
/* Input(s)           : u4IfIndex - Interface Index                          */
/*                    : u4SchedulerId - Scheduler ID                         */
/*                    : u1Tsa - Scheduler Algorithm                          */
/* Output(s)          : None                                                 */
/* Return Value(s)    : QOS_SUCCESS / QOS_FAILURE                            */
/* Called By          :                                                      */
/*****************************************************************************/
INT4
QosApiUpdateSchedularAlgo (UINT4 u4IfIndex, UINT4 u4SchedulerId, UINT1 u1Tsa)
{
    INT4               i4RetStatus = 0;
    UINT4              u4CurrVal = 0;
    tQoSSchedNode      *pSchedNode = NULL;

    pSchedNode =  QoSUtlGetSchedNode ((INT4) u4IfIndex, u4SchedulerId);
    if ((pSchedNode != NULL) && 
            (pSchedNode->u1Status == ACTIVE))
    {
        u4CurrVal = (UINT4) pSchedNode->u1SchedAlgorithm;
        pSchedNode->u1SchedAlgorithm = u1Tsa;

        /* Set Scheduling Algorithm to the Scheduler in the Hierarchical Schema */
        i4RetStatus = QoSHwWrSchedulerUpdateParams (pSchedNode->i4IfIndex,
                pSchedNode->u4Id, 
                QOS_SCHED_UPDATE_ALGO);
        if (i4RetStatus != QOS_FAILURE)
        {
            QOS_TRC_ARG1 (MGMT_TRC, "In %s : QosApiUpdateSchedularAlgo"
                    " () Returns SUCCESS. \r\n", __FUNCTION__);
            return (QOS_SUCCESS);
        }
        pSchedNode->u1SchedAlgorithm = (UINT1) u4CurrVal;
    }

    QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSHwWrSchedulerUpdateParams()"
            " Returns FAILURE.\r\n", __FUNCTION__);
    return (QOS_FAILURE);
}

/*****************************************************************************/
/* Function Name      : QosApiDeleteSchedular                                */
/* Description        : This function is called from the ETS subSystem       */
/*                      to delete the scheduler entry                        */
/* Input(s)           : u4IfIndex - Interface Index                          */
/*                    : u4SchedulerId - Scheduler ID                         */
/* Output(s)          : None                                                 */
/* Return Value(s)    : QOS_SUCCESS / QOS_FAILURE                            */
/* Called By          :                                                      */
/*****************************************************************************/
INT4
QosApiDeleteSchedular (UINT4 u4IfIndex, UINT4 u4SchedulerId)
{
    INT4               i4RetStatus = 0;
    UINT4              u4NextLevelSchedulerId = 2;
    UINT4              u4NextLevelSchedulerHwId = 0;
    tQoSSchedNode      *pSchedNode = NULL;
    tQoSSchedNode      *pSchedNextNode = NULL;
    tQoSHierarchyNode  *pHierarchyNode = NULL;

    if(u4SchedulerId == 2)
      u4NextLevelSchedulerId =1;

    if(u4SchedulerId == 1)
      u4NextLevelSchedulerId =0;

    if (gQoSGlobalInfo.eSysStatus == QOS_SYS_STATUS_ENABLE)
    {
        pSchedNode = QoSUtlGetSchedNode ((INT4) u4IfIndex, u4SchedulerId);
        if (pSchedNode == NULL)
        {
            QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetSchedNode() is NULL"
                    " Returns FAILURE.\r\n", __FUNCTION__);
            return (QOS_FAILURE);
        }
        /* Entry found in the Table */
        if (pSchedNode->u1Status == ACTIVE)
        {
            pHierarchyNode = QoSUtlGetHierarchyNode((INT4) u4IfIndex, pSchedNode->u1HierarchyLevel, u4SchedulerId);
            if (pHierarchyNode == NULL)
            {
                QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetHierarchyNode() is NULL"
                        " Returns FAILURE.\r\n", __FUNCTION__);
                return (QOS_FAILURE);
            }
            /*In S2 get the next level scheduler */
            if(u4NextLevelSchedulerId != 0)
            {
                pSchedNextNode = QoSUtlGetSchedNode ((INT4) u4IfIndex,
                        u4NextLevelSchedulerId);
                if (pSchedNextNode == NULL)
                {
                    QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetSchedNode() is NULL"
                            " Returns FAILURE.\r\n", __FUNCTION__);
                    return (QOS_FAILURE);
                }
                /* Un Map S2 or S3 Schedulers to next level scheduler */
                u4NextLevelSchedulerHwId = pSchedNextNode->u4SchedulerHwId;
                i4RetStatus =
                    QoSHwWrSchedulerHierarchyMap (pHierarchyNode->i4IfIndex,
                            (INT4)pSchedNode->u4SchedulerHwId,
                            pHierarchyNode->u2Weight,
                            pHierarchyNode->u1Priority,
                            u4NextLevelSchedulerHwId,
                            (INT4)pHierarchyNode->u4QNext,
                            pHierarchyNode->u1Level,
                            (INT4)QOS_HIERARCHY_DEL);
                if (i4RetStatus == QOS_FAILURE)
                {
                    QOS_TRC_ARG1 (MGMT_TRC, "In %s : "
                            "QoSHwWrSchedulerHierarchyMap : Delete "
                            " () Returns FAILURE. \r\n",
                            __FUNCTION__);
                    return (QOS_FAILURE);
                }
                else
                {
                    /*Decrement the count of children associated with the
                     * scheduler.
                     */
                    if (u4NextLevelSchedulerHwId != 0)
                    {
                        pSchedNextNode->u4SchedChildren--;
                    }
                }
            }
            /* Remove the HierarchyTblEntry from the SOFTWARE. */
            i4RetStatus = QoSDeleteHierarchyTblEntry(pHierarchyNode);
            if (i4RetStatus == QOS_FAILURE)
            {
                QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSDeleteHierarchyTblEntry ()"
                        " Returns FAILURE. \r\n", __FUNCTION__);
                return (QOS_FAILURE);
            }

            /* Remove the Scheduler entry from the HARDWARE */
            i4RetStatus = QoSHwWrSchedulerDelete (u4IfIndex, 
                    (INT4)u4SchedulerId);
            if (i4RetStatus == QOS_FAILURE)
            {
                QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSHwWrSchedulerDelete ()"
                        " Returns FAILURE. \r\n", __FUNCTION__);
                return (QOS_FAILURE);
            }
            /* Remove the SchedTblEntry from the SOFTWARE. */
            i4RetStatus = QoSDeleteSchedTblEntry (pSchedNode);
            if (i4RetStatus == QOS_FAILURE)
            {
                QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSDeleteSchedTblEntry ()"
                        " Returns FAILURE. \r\n", __FUNCTION__);
                return (QOS_FAILURE);
            }
        }
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QosApiDeleteSchedular"
                " () Returns SUCCESS. \r\n", __FUNCTION__);
        return (QOS_SUCCESS);
    }
    QOS_TRC_ARG1 (MGMT_TRC, "In %s : QosApiDeleteSchedular"
            " () Returns FAILURE. \r\n", __FUNCTION__);
    return (QOS_FAILURE);
}

/*****************************************************************************/
/* Function Name      : QosApiAttachQueue                                    */
/* Description        : This function is called from the ETS subSystem       */
/*                      to attached queue in scheduler node                  */
/* Input(s)           : u4IfIndex - Interface Index                          */
/*                    : u4QueueId - Queue ID                                 */
/*                    : u4SchedulerId - Scheduler ID                         */
/*                    : u1EtsTsa - Scheduler Algorithm                       */
/*                    : u2EtsBw - Queue Bandwidth                            */
/* Output(s)          : None                                                 */
/* Return Value(s)    : QOS_SUCCESS / QOS_FAILURE                            */
/* Called By          :                                                      */
/*****************************************************************************/
INT4
QosApiAttachQueue (UINT4 u4IfIndex, UINT4 u4QueueId,  
                   UINT4 u4SchedulerId, UINT1 u1EtsTsa, UINT2 u2EtsBw)
{
    INT4               i4RetStatus = QOS_FAILURE;
    UINT4              u4CfgTempId = 1;
    tQoSQNode          *pQNode = NULL;
    tQoSQTemplateNode  *pQTempNode = NULL;
    tQoSSchedNode      *pSchedNode = NULL;

    if (gQoSGlobalInfo.eSysStatus == QOS_SYS_STATUS_ENABLE)
    {
        pQTempNode = QoSUtlGetQTempNode (u4CfgTempId);
        if (pQTempNode == NULL)
        {
            QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetQTempNode () "
                          "Returns FAILURE. \r\n", __FUNCTION__);
            return (QOS_FAILURE);
        }
        /* Get an Entry in the Q-Table */
        pQNode = QoSUtlGetQNode ((INT4) u4IfIndex, u4QueueId);
        if (pQNode == NULL)
        {
            QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSCreateQTblEntry ()"
                    " Returns FAILURE. \r\n", __FUNCTION__);
            return (QOS_FAILURE);
        }
        pQNode->u4Id = u4QueueId;
        pQNode->i4IfIndex = u4IfIndex;
        pQNode->u4CfgTemplateId = u4CfgTempId;
        pQNode->u4SchedulerId = u4SchedulerId;
        pQNode->u4ShapeId = 0;
        pQNode->u2Weight = u2EtsBw;
        pQNode->u1Priority = QOS_Q_TABLE_PRIORITY_DEFAULT;

        /* Refcount for SchedulerNode */
        pSchedNode = QoSUtlGetSchedNode ((INT4) u4IfIndex, u4SchedulerId);
        if (pSchedNode == NULL)
        {
            QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetSchedNode () "
                    "Returns FAILURE. \r\n", __FUNCTION__);
            return (QOS_FAILURE);
        }
        /* Incremnet the RefCount of SchedId  Entry in the Sched Table */
        pSchedNode->u4RefCount = (pSchedNode->u4RefCount) + 1;
        pSchedNode->u1SchedAlgorithm = u1EtsTsa;

        /* Add the entry into the HARDWARE. */
        i4RetStatus = QoSHwWrQueueCreate ((INT4) pQNode->i4IfIndex,
                pQNode->u4Id);
        if ((i4RetStatus == QOS_FAILURE)
                || (i4RetStatus == QOS_NP_NOT_SUPPORTED))
        {
            if (i4RetStatus == QOS_NP_NOT_SUPPORTED)
            {
                CLI_SET_ERR (QOS_CLI_ERR_INVAL_QUEUE_PARAM);
            }
            QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSHwWrQueueCreate ()"
                    " Returns FAILURE.\r\n", __FUNCTION__);
            return (QOS_FAILURE);
        }
        pQNode->u1Status = ACTIVE;

        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QosApiAttachQueue"
                " () Returns SUCCESS. \r\n", __FUNCTION__);
        return (QOS_SUCCESS);
    }

    QOS_TRC_ARG1 (MGMT_TRC, "In %s : QosApiAttachQueue"
            " () Returns FAILURE. \r\n", __FUNCTION__);
    return (QOS_FAILURE);
}

/*****************************************************************************/
/* Function Name      : QosApiDeAttachQueue                                  */
/* Description        : This function is called from the ETS subSystem       */
/*                      to detached queue from scheduler node                */
/* Input(s)           : u4IfIndex - Interface Index                          */
/*                    : u4SchedulerId - Scheduler ID                         */
/*                    : u4HierarLevel - Scheduler-Hierar Level               */
/*                    : u1Tsa - Scheduler Algorithm                          */
/* Output(s)          : None                                                 */
/* Return Value(s)    : QOS_SUCCESS / QOS_FAILURE                            */
/* Called By          :                                                      */
/*****************************************************************************/
INT4
QosApiDeAttachQueue (UINT4 u4IfIndex, UINT4 u4QueueId) 
{
    tQoSQNode          *pQNode = NULL;
    INT4               i4RetStatus = QOS_FAILURE;

    if (gQoSGlobalInfo.eSysStatus == QOS_SYS_STATUS_ENABLE)
    {
        pQNode = QoSUtlGetQNode ((INT4) u4IfIndex, u4QueueId);
        if (pQNode == NULL)
        {
            QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetQNode()"
                    " Returns FAILURE. \r\n", __FUNCTION__);
            return (QOS_FAILURE);
        }
        /* Remove the entry from the HARDWARE. */
        if ((pQNode->u1Status == ACTIVE) &&
                (gQoSGlobalInfo.eSysStatus == QOS_SYS_STATUS_ENABLE))
        {
            i4RetStatus = QoSHwWrQueueDelete ((INT4)pQNode->i4IfIndex,
                    pQNode->u4Id);
            if (i4RetStatus == QOS_FAILURE)
            {
                QOS_TRC_ARG1 (MGMT_TRC, "In %s : "
                        "QoSHwWrQueueDelete () Returns FAILURE.\r\n",
                        __FUNCTION__);
                return (QOS_FAILURE);
            }
        }    
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QosApiDeAttachQueue"
                " () Returns SUCCESS. \r\n", __FUNCTION__);
        return (QOS_SUCCESS);
    }

    QOS_TRC_ARG1 (MGMT_TRC, "In %s : QosApiAttachQueue"
            " () Returns FAILURE. \r\n", __FUNCTION__);
    return (QOS_FAILURE);
}

#ifdef DCBX_ETS_QOS_WANTED
INT4
QosApiQoSAddDefHierarchyTblEntries (UINT4 u4IfIndex)
{
    if (QoSAddDefHierarchyTblEntries(u4IfIndex, QOS_HL_DEF_NUM_LEVELS) == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : "
                "QoSAddDefHierarchyTblEntries () Returns FAILURE.\r\n",
                __FUNCTION__);
        return (QOS_FAILURE);
    }
    QOS_TRC_ARG1 (MGMT_TRC, "In %s : QosApiQoSAddDefHierarchyTblEntries"
            " () Returns SUCCESS. \r\n", __FUNCTION__);
    return (QOS_SUCCESS);
}
#endif
INT4
QosApiQosHwGetPfcStats (tQosPfcStats *pQosPfcStats)
{
    if (QosHwWrGetPfcStats (pQosPfcStats) == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : "
                "QosHwWrGetPfcStats() Returns FAILURE.\r\n",
                __FUNCTION__);
        return (QOS_FAILURE);
    }
    QOS_TRC_ARG1 (MGMT_TRC, "In %s : QosApiQosHwGetPfcStats"
            " () Returns SUCCESS. \r\n", __FUNCTION__);
    return (QOS_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QosApiValidateClsMapConfigFlag                       */
/*                                                                           */
/* Description        : This function is called from msrval for validating   */
/*                      Qos class map config Flag (which is set based on     */
/*                      configuration) done through QOS or other protocol    */
/*                      a protocol.                                          */
/*                                                                           */
/* Input(s)           : u4QoSClassMapId - class map id                       */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/

VOID QosApiValidateClsMapConfigFlag (UINT4 u4QoSClassMapId, 
        BOOL1 *pb1IsClsMapConfFromExt)
{
    tQoSClassMapNode      *pClsMapNode = NULL;

    pClsMapNode = QoSUtlGetClassMapNode (u4QoSClassMapId);
    if (pClsMapNode != NULL)
    {
        *pb1IsClsMapConfFromExt = pClsMapNode->b1IsClsMapConfFromExt;
    }
    return;
}

/*****************************************************************************/
/* Function Name      : QosApiValidatePlyMapConfigFlag                       */
/*                                                                           */
/* Description        : This function is called from msrval for validating   */
/*                      Qos policy map config Flag (which is set based on    */
/*                      configuration) done through QOS or other protocol    */
/*                      a protocol.                                          */
/*                                                                           */
/* Input(s)           : u4QoSPolicyMapId - policy map id                     */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/

VOID QosApiValidatePlyMapConfigFlag (UINT4 u4QoSPolicyMapId, 
        BOOL1 *pb1IsPlyMapConfFromExt)
{
    tQoSPolicyMapNode      *pPlyMapNode = NULL;

    pPlyMapNode = QoSUtlGetPolicyMapNode (u4QoSPolicyMapId);
    if (pPlyMapNode != NULL)
    {
        *pb1IsPlyMapConfFromExt = pPlyMapNode->b1IsPlyMapConfFromExt;
    }

    return;
}

/* End of File */
