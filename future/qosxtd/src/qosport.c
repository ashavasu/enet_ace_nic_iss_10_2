/* $Id: qosport.c,v 1.7 2014/03/14 12:36:59 siva Exp $ */
/*****************************************************************************/
/*    FILE  NAME            : qosport.c                                      */
/*    PRINCIPAL AUTHOR      : Aricent Inc.                                   */
/*    MODULE NAME           : QOSX                                           */
/*    LANGUAGE              : C                                              */
/*    TARGET ENVIRONMENT    : Any                                            */
/*    DATE OF FIRST RELEASE :                                                */
/*    AUTHOR                : Aricent Inc.                                   */
/*    DESCRIPTION           : This file contains the functions to be ported  */
/*                             for qos module                                */
/*---------------------------------------------------------------------------*/
#include "qosinc.h"

/*****************************************************************************/
/* Function Name      : QosGetNextValidPort                                  */
/*                                                                           */
/* Description        : This routine returns the next active port in the     */
/*                      system                                               */
/*                                                                           */
/* Input(s)           : i4IfIndex - Port Index whose next port is to be      */
/*                                    determined                             */
/*                                                                           */
/* Output(s)          : pi4NextPort - The next active port                   */
/*                                                                           */
/*                                                                           */
/* Return Value(s)    : QOS_SUCCESS / QOS_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
QosGetNextValidPort (UINT4 u4IfIndex, UINT4 *pu4NextPort)
{
    UINT2               u2NextPort;
    if (L2IwfGetNextValidPort (u4IfIndex, &u2NextPort) != L2IWF_SUCCESS)
    {
        return QOS_FAILURE;
    }
    *pu4NextPort = (UINT4) u2NextPort;

    return QOS_SUCCESS;
}

/************************************************************************/
/* Function Name      : QOSRmRegisterProtocols                          */
/*                                                                           */
/* Description        : This function calls the RM module to register.       */
/*                                                                           */
/* Input(s)           : tRmRegParams - Reg. params to be provided by         */
/*                      protocols.                                           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/***********************************************************************/
PUBLIC UINT4
QOSRmRegisterProtocols (tRmRegParams * pRmReg)
{
#ifdef L2RED_WANTED
    INT4                i4RetVal = OSIX_FAILURE;
    i4RetVal = RmRegisterProtocols (pRmReg);
    return (((i4RetVal == RM_FAILURE) ? OSIX_FAILURE : OSIX_SUCCESS));
#else
    UNUSED_PARAM (pRmReg);
    return OSIX_SUCCESS;
#endif

}

/***********************************************************************
 * Function Name      : QOSRmDeRegisterProtocols                       
 *                                                                           
 * Description        : This function deregisters PBB-TE with RM. 
 *                                                                           
 * Input(s)           : None                                          
 *                                                                           
 * Output(s)          : None                                         
 *                                                                           
 * Return Value(s)    : RM_SUCCESS/RM_FAILURE     
 *                                                                           
 *********************************************************************/
PUBLIC UINT4
QOSRmDeRegisterProtocols (VOID)
{
#ifdef L2RED_WANTED
    INT4                i4RetVal = OSIX_FAILURE;
    i4RetVal = RmDeRegisterProtocols (RM_QOS_APP_ID);
    return (((i4RetVal == RM_FAILURE) ? OSIX_FAILURE : OSIX_SUCCESS));
#else
    return OSIX_SUCCESS;
#endif

}

/***********************************************************************
 * Function Name      : QostRmReleaseMemoryForMsg                      
 *                                                                          
 * Description        : This function calls the RM module to release the     
 *                      memory allocated for sending the Standby node count. 
 *                                                                           
 * Input(s)           : pu1Block - Mmemory block.                          
 *                                                                        
 * Output(s)          : None                                  
 *                                                                 
 * Return Value(s)    : None                                  
 *                                                                   
 ***********************************************************************/
PUBLIC INT4
QosRmReleaseMemoryForMsg (UINT1 *pu1Block)
{
#ifdef L2RED_WANTED

    INT4                i4RetVal = OSIX_FAILURE;
    i4RetVal = RmReleaseMemoryForMsg (pu1Block);
    return (((i4RetVal == RM_FAILURE) ? OSIX_FAILURE : OSIX_SUCCESS));
#else
    UNUSED_PARAM (pu1Block);
    return OSIX_SUCCESS;
#endif

}

/*********************************************************
 * Function Name      : QosPortRmGetNodeState               
 *                                                                               
 * Description        : This function calls the RM module       
 *                         to get the node state.                          
 *                                                                          
 * Input(s)           : None                                         
 *                                                                          
 * Output(s)          : None                                        
 *                                                                          
 * Return Value(s)    : RM_ACTIVE/RM_INIT_/RM_STANDBY     
 *                                                                          
 ****************************************************************/
PUBLIC UINT4
QosPortRmGetNodeState (VOID)
{
#ifdef L2RED_WANTED
    return (RmGetNodeState ());
#else
    return RM_ACTIVE;
#endif

}

/*******************************************************************
 * Function Name      : QosPortRmGetStandbyNodeCount                        
 *                                                                           
 * Description        : This function calls the RM module to get the number  
 *                            of peer nodes that are up.                           
 *                                                                           
 * Input(s)           : None                                          
 *                                                                           
 * Output(s)          : None                                         
 *                                                                           
 * Return Value(s)    : Number of Booted up standby nodes     
 *                                                                           
 *********************************************************************/
PUBLIC UINT1
QosPortRmGetStandbyNodeCount (VOID)
{
#ifdef L2RED_WANTED
    return (RmGetStandbyNodeCount ());
#else
    return QOS_INIT_VAL;
#endif

}

/*********************************************************************
 * Function Name      : QosPortRmApiHandleProtocolEvent                    
 *                                                                 
 * Description        : This function calls the RM module to intimate about  
 *                      the protocol operations                         
 *                                                                     
 * Input(s)           : pEvt->u4AppId - Application Id                     
 *                      pEvt->u4Event - Event send by protocols to RM       
 *                                      RM_STANDBY_TO_ACTIVE_EVT_PROCESSED 
 *                                      RM_IDLE_TO_ACTIVE_EVT_PROCESSED 
 *                                      RM_STANDBY_EVT_PROCESSED)          
 *                      pEvt->u4Err   - Error code (RM_MEMALLOC_FAIL   
 *                                      RM_SENDTO_FAIL / RM_PROCESS_FAIL)    
 *                                                                           
 * Output(s)          : None                                                 
 *                                                                          
 * Return Value(s)    : None                                                
 *                                                                           
 *********************************************************************/
PUBLIC UINT4
QosPortRmApiHandleProtocolEvent (tRmProtoEvt * pEvt)
{
#ifdef L2RED_WANTED

    INT4                i4RetVal = OSIX_FAILURE;
    i4RetVal = RmApiHandleProtocolEvent (pEvt);
    return (((i4RetVal == RM_FAILURE) ? OSIX_FAILURE : OSIX_SUCCESS));
#else
    UNUSED_PARAM (pEvt);
    return OSIX_SUCCESS;
#endif

}

/**********************************************************************
 * FUNCTION NAME    : QosPortRmApiSendProtoAckToRM 
 *
 * DESCRIPTION      : This is the function used by protocols/applications 
 *                            to send acknowledgement to RM after processing the
 *                               sync-up message.
 *
 * INPUT            : tRmProtoAck contains
 *                    u4AppId  - Protocol Identifier
 *                    u4SeqNum - Sequence number of the RM message for
 *                    which this ACK is generated.
 *
 * OUTPUT           : None
 * 
 * RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 * 
 **************************************************************************/
PUBLIC INT4
QosPortRmApiSendProtoAckToRM (tRmProtoAck * pProtoAck)
{
#ifdef L2RED_WANTED

    INT4                i4RetVal = OSIX_FAILURE;
    i4RetVal = RmApiSendProtoAckToRM (pProtoAck);
    return (((i4RetVal == RM_FAILURE) ? OSIX_FAILURE : OSIX_SUCCESS));
#else
    UNUSED_PARAM (pProtoAck);
    return OSIX_SUCCESS;
#endif

}

/************************************************************************
 *
 * FUNCTION NAME      : QosPortEnqMsgToRm 
 * 
 * DESCRIPTION        : This function calls the RM Module to enqueue the
 *                      message from applications to RM task.
 *
 * INPUT              : pRmMsg - msg from application
 *                      u2DataLen - Len of msg
 * 
 * OUTPUT             : NONE
 *
 * RETURNS            : OSIX_SUCCESS/OSIX_FAILURE
 *
 *************************************************************************/
PUBLIC INT4
QosPortEnqMsgToRm (tRmMsg * pRmMsg, UINT2 u2DataLen)
{
#ifdef L2RED_WANTED
    INT4                i4RetVal = OSIX_SUCCESS;

    if (RmEnqMsgToRmFromAppl (pRmMsg, u2DataLen, RM_QOS_APP_ID, RM_QOS_APP_ID)
        != RM_SUCCESS)
    {
        QOS_TRC (MGMT_TRC | ALL_FAILURE_TRC,
                 "QosPortEnqMsgToRm: Enqueuing sync up "
                 "message to RM failed\r\n");
        /* memory allocated for pRmMsg is freed here, only in failure case. 
         * In success case RM will free the memory */
        RM_FREE (pRmMsg);
        i4RetVal = OSIX_FAILURE;
    }
    return i4RetVal;
#else
    UNUSED_PARAM (pRmMsg);
    UNUSED_PARAM (u2DataLen);
    return OSIX_SUCCESS;
#endif
}

/*********************************************************
 * Function Name      : QosPortRmSetBulkUpdatesStatus
 *                                                                               
 * Description        : This function calls the RM Bulk Status       
 *                         Update funciotn.                          
 *                                                                          
 * Input(s)           : None                                         
 *                                                                          
 * Output(s)          : None                                        
 *                                                                          
 * Return Value(s)    : None
 *                                                                          
 ****************************************************************/
PUBLIC VOID
QosPortRmSetBulkUpdatesStatus (VOID)
{
#ifdef L2RED_WANTED
    RmSetBulkUpdatesStatus (RM_QOS_APP_ID);
    return;
#else
    return;
#endif
}

/*****************************************************************************/
/* Function Name      : QosL2IwfIsPortInPortChannel                         */
/*                                                                           */
/* Description        : This function calls the L2IWF module to check        */
/*                      whether the port is in port-channel.                 */
/*                                                                           */
/* Input(s)           : u4IfIndex - Interface Identifier                     */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
INT4
QosL2IwfIsPortInPortChannel (UINT4 u4IfIndex)
{
    return (L2IwfIsPortInPortChannel (u4IfIndex));
}
