/* $Id: qoscli.c,v 1.89 2018/01/09 11:00:19 siva Exp $ */
/****************************************************************************/
/* Copyright (C) 2007 Aricent Inc . All Rights Reserved                     */
/*                                                                          */
/*  FILE NAME             : qoscli.c                                        */
/*  PRINCIPAL AUTHOR      : Aricent                                         */
/*  SUBSYSTEM NAME        : QoS                                             */
/*  MODULE NAME           : QoS-CLI                                         */
/*  LANGUAGE              : C                                               */
/*  TARGET ENVIRONMENT    : Linux                                           */
/*  DATE OF FIRST RELEASE :                                                 */
/*  AUTHOR                : Aricent                                         */
/*  FUNCTIONS DEFINED(Applicable for Source files) :                        */
/*  DESCRIPTION           : This module provide the CLI Mangement Interface */
/*                          for the Aricent QoS Module.                     */
/****************************************************************************/
/*                                                                          */
/*  Change History                                                          */
/*  Version               :                                                 */
/*  Date(DD/MM/YYYY)      :                                                 */
/*  Modified by           :                                                 */
/*  Description of change :                                                 */
/****************************************************************************/
#ifndef __QOS_CLI_C__
#define __QOS_CLI_C__

#include "qosinc.h"
#include "fsqosxcli.h"

/*****************************************************************************/
/* Function Name      : cli_process_qos_cmd                                  */
/* Description        : This function is Classify the Given Command and      */
/*                      configure the QoS module through CLI using setof     */
/*                      nmh routines.                                        */
/* Input(s)           : CliHandle - Handle to CLI session                    */
/*                    : u4Command - Command Type to Classify.                */
/*                    : ...       - Variable number of Arg for the above     */
/*                    :             Command type if possible.                */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : gapi1QoSCliErrString.                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : CLI_SUCCESS/CLI_FAILURE                              */
/* Called By          : CLI Thread.                                          */
/* Calling Function   :                                                      */
/*****************************************************************************/
PUBLIC INT4
cli_process_qos_cmd (tCliHandle CliHandle, UINT4 u4Command, ...)
{
    va_list             ap;
    UINT4              *apu4Args[QOS_CLI_MAX_ARGS];
    INT1                i1ArgNo = 0;
    UINT4               u4ErrCode = 0;
    INT4                i4Inst = 0;
    INT4                i4RetStatus = CLI_SUCCESS;
    INT4                i4Priority = 0;

    MEMSET (apu4Args, 0, QOS_CLI_MAX_ARGS);

    va_start (ap, u4Command);

    /* third argument is always interface name/index */
    i4Inst = CLI_PTR_TO_I4 (va_arg (ap, UINT1 *));

    if (i4Inst != 0)
    {
    }

    /* Walk through the rest of the arguments and store in apu4Args array. */
    while (1)
    {
        apu4Args[i1ArgNo++] = va_arg (ap, UINT4 *);

        if (i1ArgNo == QOS_CLI_MAX_ARGS)
        {
            break;
        }

    }
    va_end (ap);

    /* Register Lock with CLI */
    CliRegisterLock (CliHandle, QoSLock, QoSUnLock);
    QoSLock ();

    switch (u4Command)
    {
        case CLI_QOS_SET_SYS_CNTRL_DISABLE:

            i4RetStatus = QoSCliSetSysControl (CliHandle,
                                               QOS_SYS_CNTL_SHUTDOWN);
            break;

        case CLI_QOS_SET_SYS_CNTRL_ENABLE:

            i4RetStatus = QoSCliSetSysControl (CliHandle, QOS_SYS_CNTL_START);

            break;

        case CLI_QOS_SET_SYS_STATUS:
            /* apu4Args[0] = enable */
            /* apu4Args[0] = disable */
            if (apu4Args[0] != NULL)
            {
                i4RetStatus = QoSCliSetStatus (CliHandle,
                                               QOS_SYS_STATUS_ENABLE);
            }
            else
            {
                i4RetStatus = QoSCliSetStatus (CliHandle,
                                               QOS_SYS_STATUS_DISABLE);
            }

            break;

        case CLI_QOS_SET_VLAN_QUEUING_STATUS:
            /* apu4Args[0] = enable */
            /* apu4Args[0] = disable */
            if (apu4Args[0] != NULL)
            {
                i4RetStatus = VlanQueueingCliSetStatus (CliHandle,
                                                        VLAN_QUEUEING_SYS_STATUS_ENABLE);
            }
            else
            {
                i4RetStatus = VlanQueueingCliSetStatus (CliHandle,
                                                        VLAN_QUEUEING_SYS_STATUS_DISABLE);
            }

            break;

        case CLI_QOS_ADD_PRI_MAP:
            /* apu4Args[0] = priority-map-id */
            i4RetStatus = QoSCliAddPriMapEntry (CliHandle,
                                                CLI_PTR_TO_U4 (apu4Args[0]));

            break;

        case CLI_QOS_ADD_VLAN_MAP:
            /* apu4Args[0] = vlan-map-id */
            i4RetStatus = QoSCliAddVlanMapEntry (CliHandle,
                                                 CLI_PTR_TO_U4 (apu4Args[0]));

            break;

        case CLI_QOS_SET_PRI_MAP_PARAMS:
            /* apu4Args[0] = IfNum     */
            /* apu4Args[1] = VlanId    */
            /* apu4Args[2] = InPriType */
            /* apu4Args[3] = InPri     */
            /* apu4Args[4] = ReGenPri  */
            /* apu4Args[5] = InnerReGenPri  */
            i4RetStatus = QoSCliSetPriMapParams (CliHandle,
                                                 (CLI_PTR_TO_U4 (apu4Args[0])),
                                                 (CLI_PTR_TO_U4 (apu4Args[1])),
                                                 (UINT1) (CLI_PTR_TO_U4
                                                          (apu4Args[2])),
                                                 (UINT1) *apu4Args[3],
                                                 (UINT1) *apu4Args[4],
                                                 (INT1) (CLI_PTR_TO_I4
                                                         (apu4Args[5])),
                                                 (CLI_PTR_TO_I4 (apu4Args[6])),
                                                 (CLI_PTR_TO_I4 (apu4Args[7])));
            break;

        case CLI_QOS_SET_VLAN_MAP_PARAMS:
            /* apu4Args[0] = IfNum     */
            /* apu4Args[1] = VlanId    */
            i4RetStatus = QoSCliSetVlanMapParams (CliHandle,
                                                  (CLI_PTR_TO_U4 (apu4Args[0])),
                                                  (CLI_PTR_TO_U4
                                                   (apu4Args[1])));
            break;

        case CLI_QOS_NO_SET_PRI_MAP_PARAMS:
            /* apu4Args[0] = Interface      */
            /* apu4Args[1] = Vlan           */
            /* apu4Args[2] = InnerReGenPri  */
            i4RetStatus =
                QoSCliNoSetPriMapParams (CliHandle,
                                         (CLI_PTR_TO_U4 (apu4Args[0])),
                                         (CLI_PTR_TO_U4 (apu4Args[1])),
                                         (CLI_PTR_TO_U4 (apu4Args[2])));
            break;

        case CLI_QOS_SHOW_PRI_MAP:
            /* apu4Args[0] = priority-map-id */
            i4RetStatus = QoSCliShowPriMapEntry (CliHandle,
                                                 CLI_PTR_TO_U4 (apu4Args[0]));

            break;

        case CLI_QOS_SHOW_VLAN_MAP:
            /* apu4Args[0] = vlan-map-id */
            i4RetStatus = QoSCliShowVlanQMapEntry (CliHandle,
                                                   CLI_PTR_TO_U4 (apu4Args[0]));

            break;

        case CLI_QOS_DEL_PRI_MAP:
            /* apu4Args[0] = priority-map-id */
            i4RetStatus = QoSCliDelPriMapEntry (CliHandle,
                                                CLI_PTR_TO_U4 (apu4Args[0]));

            break;

        case CLI_QOS_DEL_VLAN_MAP:
            /* apu4Args[0] = vlan-map-id */
            i4RetStatus = QoSCliDelVlanMapEntry (CliHandle,
                                                 CLI_PTR_TO_U4 (apu4Args[0]));

            break;

        case CLI_QOS_ADD_CLS_MAP:
            /* apu4Args[0] = Class-map-id */
            i4RetStatus = QoSCliAddClsMapEntry (CliHandle,
                                                CLI_PTR_TO_U4 (apu4Args[0]));

            break;

        case CLI_QOS_SET_CLS_MAP_PARAMS:
            /* apu4Args[0] = L2 FilterId     */
            /* apu4Args[1] = L2 FilterId     */
            /* apu4Args[2] = Priority Map Id */
            /* apu4Args[3] = VLan Map Id     */
            i4RetStatus = QoSCliSetClsMapParams (CliHandle,
                                                 (CLI_PTR_TO_I4 (apu4Args[0])),
                                                 (CLI_PTR_TO_I4 (apu4Args[1])),
                                                 (CLI_PTR_TO_I4 (apu4Args[2])),
                                                 (CLI_PTR_TO_I4 (apu4Args[3])));
            break;

        case CLI_QOS_SET_CLS_MAP_CLASS:
            /* apu4Args[0] = CLASS     */
            /* apu4Args[1] = i4PreColor */
            /* apu4Args[2] = ReGenPri   */
            /* apu4Args[3] = GroupName  */
            i4RetStatus = QoSCliSetClsMapClass (CliHandle,
                                                (*apu4Args[0]),
                                                (CLI_PTR_TO_I4 (apu4Args[1])),
                                                (CLI_PTR_TO_I4 (apu4Args[2])),
                                                ((UINT1 *) apu4Args[3]));
            break;

        case CLI_QOS_SET_NO_CLS_MAP_CLASS:
            /* apu4Args[0] = CLASS     */
            i4RetStatus = QoSCliDelClsToPriMap (CliHandle, *apu4Args[0]);
            break;

        case CLI_QOS_SHOW_CLS_MAP:
            /* apu4Args[0] = Class-map-id */
            i4RetStatus = QoSCliShowClsMapEntry (CliHandle,
                                                 CLI_PTR_TO_U4 (apu4Args[0]));

            break;

        case CLI_QOS_DEL_CLS_MAP:
            /* apu4Args[0] = Class-map-id */
            i4RetStatus = QoSCliDelClsMapEntry (CliHandle,
                                                CLI_PTR_TO_U4 (apu4Args[0]));

            break;

        case CLI_QOS_SHOW_CLS2PRI_MAP:
            /* apu4Args[0] = Class-to-priorit-group-name */
            i4RetStatus = QoSCliShowClsToPriMapEntry (CliHandle,
                                                      (UINT1 *) apu4Args[0]);

            break;

        case CLI_QOS_ADD_METER:
            /* apu4Args[0] = Meter-id */
            i4RetStatus = QoSCliAddMeterEntry (CliHandle,
                                               CLI_PTR_TO_U4 (apu4Args[0]));

            break;

        case CLI_QOS_SET_METER_PARAMS:
            /* apu4Args[0] = MeterType  */
            /* apu4Args[1] = ClolorMode */
            /* apu4Args[2] = Interval   */
            /* apu4Args[3] = CIR        */
            /* apu4Args[4] = CBS        */
            /* apu4Args[5] = EIR        */
            /* apu4Args[6] = EBS        */
            /* apu4Args[7] = NextMeter  */
            i4RetStatus = QoSCliSetMeterParams (CliHandle,
                                                (CLI_PTR_TO_U4 (apu4Args[0])),
                                                (CLI_PTR_TO_U4 (apu4Args[1])),
                                                (CLI_PTR_TO_I4 (apu4Args[2])),
                                                (CLI_PTR_TO_I4 (apu4Args[3])),
                                                (CLI_PTR_TO_I4 (apu4Args[4])),
                                                (CLI_PTR_TO_I4 (apu4Args[5])),
                                                (CLI_PTR_TO_I4 (apu4Args[6])),
                                                (CLI_PTR_TO_I4 (apu4Args[7])));

            break;

        case CLI_QOS_SHOW_METER:
            /* apu4Args[0] = Meter-id */
            i4RetStatus = QoSCliShowMeterEntry (CliHandle,
                                                CLI_PTR_TO_U4 (apu4Args[0]));

            break;

        case CLI_QOS_DEL_METER:
            /* apu4Args[0] = meter-id */
            i4RetStatus = QoSCliDelMeterEntry (CliHandle,
                                               CLI_PTR_TO_U4 (apu4Args[0]));

            break;

        case CLI_QOS_ADD_PLY_MAP:
            /* apu4Args[0] = Policy-map-id */
            i4RetStatus = QoSCliAddPlyMapEntry (CliHandle,
                                                CLI_PTR_TO_U4 (apu4Args[0]));

            break;

        case CLI_QOS_SET_PLY_CLS:
            /* apu4Args[0] = Class */
            /* apu4Args[1] = IfIndex    */
            /* apu4Args[2] = PriType    */
            /* apu4Args[3] = PriVal     */
            i4RetStatus = QoSCliSetPlyMapClass (CliHandle,
                                                (CLI_PTR_TO_I4 (apu4Args[0])),
                                                (CLI_PTR_TO_U4 (apu4Args[1])),
                                                (CLI_PTR_TO_I4 (apu4Args[2])),
                                                (CLI_PTR_TO_I4 (apu4Args[3])),
                                                (CLI_PTR_TO_I4 (apu4Args[4])));

            break;

        case CLI_QOS_NO_SET_PLY_IF:

            i4RetStatus = QoSCliNoSetPlyIf (CliHandle);

            break;

        case CLI_QOS_SET_PLY_NO_METER:

            i4RetStatus = QoSCliSetPlyNoMeter (CliHandle);

            break;

        case CLI_QOS_SET_PLY_METER_ACTION:
            /* apu4Args[0] = MeterId         */
            /* apu4Args[1] = u1InProCongAct  */
            /* apu4Args[2] = u1InProExcAct   */
            /* apu4Args[3] = u1OutProAct     */
            /* apu4Args[4] = &MetActPtr      */
            i4RetStatus = QoSCliSetPlyMapMeterAct (CliHandle,
                                                   (CLI_PTR_TO_I4
                                                    (apu4Args[0])),
                                                   (UINT1) (CLI_PTR_TO_I4
                                                            (apu4Args[1])),
                                                   (UINT1) (CLI_PTR_TO_I4
                                                            (apu4Args[2])),
                                                   (UINT1) (CLI_PTR_TO_I4
                                                            (apu4Args[3])),
                                                   ((tQoSCliMeterAction *)
                                                    apu4Args[4]));

            break;

        case CLI_QOS_SHOW_PLY_MAP:
            /* apu4Args[0] = policy-map-id */
            i4RetStatus = QoSCliShowPlyMapEntry (CliHandle,
                                                 CLI_PTR_TO_U4 (apu4Args[0]));

            break;

        case CLI_QOS_DEL_PLY_MAP:
            /* apu4Args[0] = policy-map-id */
            i4RetStatus = QoSCliDelPlyMapEntry (CliHandle,
                                                CLI_PTR_TO_U4 (apu4Args[0]));

            break;

        case CLI_QOS_SHOW_GLB_INFO:
            i4RetStatus = QoSCliShowGlobalInfo (CliHandle);

            break;

        case CLI_QOS_ADD_Q_TEMP:
            /* apu4Args[0] = Q Template Id */
            i4RetStatus = QoSCliAddQTempEntry (CliHandle, (*apu4Args[0]));
            break;

        case CLI_QOS_DEL_Q_TEMP:
            /* apu4Args[0] = Q Template Id */
            i4RetStatus = QoSCliDelQTempEntry (CliHandle, (*apu4Args[0]));
            break;

        case CLI_QOS_SET_Q_TEMP_PARAMS:
            /* apu4Args[0] = DropType  */
            /* apu4Args[1] = QTempSize */
            /* apu4Args[2] = DropAlgo  */
            i4RetStatus = QoSCliSetQTempParams (CliHandle,
                                                CLI_PTR_TO_I4 (apu4Args[0]),
                                                CLI_PTR_TO_I4 (apu4Args[1]),
                                                CLI_PTR_TO_I4 (apu4Args[2]));
            break;

        case CLI_QOS_SET_RD_CFG_PARAMS:
            /* apu4Args[0] = DP Value  */
            /* apu4Args[1] = i4MinTH   */
            /* apu4Args[2] = i4MaxTH   */
            /* apu4Args[3] = i4MaxPktSize */
            /* apu4Args[4] = i4MaxProb    */
            /* apu4Args[5] = i4ExpWeight  */
            /* apu4Args[6] = i4Gain       */
            /* apu4Args[7] = i4DropTHType */
            /* apu4Args[8] = i4ECNTH      */
            /* apu4Args[9] = i4Flag       */

            i4RetStatus = QoSCliSetRDCfgParams (CliHandle,
                                                (*(apu4Args[0])),
                                                CLI_PTR_TO_I4 (apu4Args[1]),
                                                CLI_PTR_TO_I4 (apu4Args[2]),
                                                CLI_PTR_TO_I4 (apu4Args[3]),
                                                CLI_PTR_TO_I4 (apu4Args[4]),
                                                CLI_PTR_TO_I4 (apu4Args[5]),
                                                CLI_PTR_TO_I4 (apu4Args[6]),
                                                CLI_PTR_TO_I4 (apu4Args[7]),
                                                CLI_PTR_TO_I4 (apu4Args[8]),
                                                (UINT1)
                                                CLI_PTR_TO_I4 (apu4Args[9]));
            break;

        case CLI_QOS_DEL_RD_CFG:
            /* apu4Args[0] = DP Value  */
            i4RetStatus = QoSCliDelRDCfgEntry (CliHandle, (*apu4Args[0]));
            break;

        case CLI_QOS_SHOW_QTEMP:
            /* apu4Args[0] = QTemplate ID */
            i4RetStatus = QoSCliShowQTempEntry (CliHandle,
                                                CLI_PTR_TO_U4 (apu4Args[0]));
            break;

        case CLI_QOS_ADD_SHAPE_TEMP:
            /* apu4Args[0] = u4Idx  */
            /* apu4Args[1] = i4CIR  */
            /* apu4Args[2] = i4CBS  */
            /* apu4Args[3] = i4EIR  */
            /* apu4Args[4] = i4EBS  */
            i4RetStatus = QoSCliAddShapeTempEntry (CliHandle,
                                                   CLI_PTR_TO_U4 (apu4Args[0]),
                                                   CLI_PTR_TO_I4 (apu4Args[1]),
                                                   CLI_PTR_TO_I4 (apu4Args[2]),
                                                   CLI_PTR_TO_I4 (apu4Args[3]),
                                                   CLI_PTR_TO_I4 (apu4Args[4]));
            break;

        case CLI_QOS_DEL_SHAPE_TEMP:
            /* apu4Args[0] = u4Idx  */
            i4RetStatus = QoSCliDelShapeTempEntry (CliHandle,
                                                   CLI_PTR_TO_U4 (apu4Args[0]));
            break;

        case CLI_QOS_SHOW_SHAPE_TEMP:
            /* apu4Args[0] = u4Idx  */
            i4RetStatus = QoSCliShowShapeTempEntry (CliHandle,
                                                    CLI_PTR_TO_U4 (apu4Args
                                                                   [0]));
            break;

        case CLI_QOS_ADD_SCHED:
            /* apu4Args[0] = u4Idx       */
            /* apu4Args[1] = u4IfIdx     */
            /* apu4Args[2] = i4SchedAlgo */
            /* apu4Args[3] = i4ShapeId   */
            /* apu4Args[4] = i4HL        */
            i4RetStatus = QoSCliAddSchedEntry (CliHandle,
                                               CLI_PTR_TO_U4 (apu4Args[0]),
                                               CLI_PTR_TO_I4 (apu4Args[1]),
                                               CLI_PTR_TO_I4 (apu4Args[2]),
                                               CLI_PTR_TO_I4 (apu4Args[3]),
                                               CLI_PTR_TO_I4 (apu4Args[4]));

            break;

        case CLI_QOS_DEL_SCHED:
            /* apu4Args[0] = u4Idx  */
            /* apu4Args[1] = u4IfIdx     */
            i4RetStatus = QoSCliDelSchedEntry (CliHandle,
                                               CLI_PTR_TO_U4 (apu4Args[0]),
                                               CLI_PTR_TO_I4 (apu4Args[1]));
            break;

        case CLI_QOS_SHOW_SCHED:
            /* apu4Args[0] = u4IfIndex */
            i4RetStatus = QoSCliShowSchedEntry (CliHandle,
                                                CLI_PTR_TO_I4 (apu4Args[0]));
            break;

        case CLI_QOS_ADD_Q:
            /* apu4Args[0] = &QParam      */
            i4RetStatus = QoSCliAddQEntry (CliHandle,
                                           ((tQoSCliQParam *) apu4Args[0]));

            break;

        case CLI_QOS_DEL_Q:
            /* apu4Args[0] = u4QIdx   */
            /* apu4Args[1] = u4IfIdx  */
            i4RetStatus = QoSCliDelQEntry (CliHandle,
                                           CLI_PTR_TO_U4 (apu4Args[0]),
                                           CLI_PTR_TO_I4 (apu4Args[1]));
            break;

        case CLI_QOS_SHOW_Q:
            /* apu4Args[0] = u4IfIndex */
            i4RetStatus = QoSCliShowQEntry (CliHandle,
                                            CLI_PTR_TO_I4 (apu4Args[0]));
            break;

        case CLI_QOS_ADD_QMAP:
            /* apu4Args[0] = u4IfIdx      */
            /* apu4Args[1] = i4MapType    */
            /* apu4Args[2] = u4MapVal     */
            /* apu4Args[3] = u4QIdx       */
            /* apu4Args[4] = u4Color     */
            i4RetStatus = QoSCliAddQMapEntry (CliHandle,
                                              CLI_PTR_TO_I4 (apu4Args[0]),
                                              CLI_PTR_TO_I4 (apu4Args[1]),
                                              CLI_PTR_TO_U4 (apu4Args[2]),
                                              CLI_PTR_TO_U4 (apu4Args[3]),
                                              CLI_PTR_TO_I4 (apu4Args[4]));

            break;

        case CLI_QOS_DEL_QMAP:
            /* apu4Args[0] = u4IfIdx     */
            /* apu4Args[1] = i4MapType   */
            /* apu4Args[2] = u4MapVal    */
            i4RetStatus = QoSCliDelQMapEntry (CliHandle,
                                              CLI_PTR_TO_I4 (apu4Args[0]),
                                              CLI_PTR_TO_I4 (apu4Args[1]),
                                              CLI_PTR_TO_U4 (apu4Args[2]));
            break;

        case CLI_QOS_SHOW_QMAP:
            /* apu4Args[0] = u4IfIndex */
            i4RetStatus = QoSCliShowQMapEntry (CliHandle,
                                               CLI_PTR_TO_I4 (apu4Args[0]));
            break;

        case CLI_QOS_ADD_HS:
            /* apu4Args[0] = HSParam      */
            i4RetStatus = QoSCliAddHSEntry (CliHandle,
                                            ((tQoSCliHSParam *) apu4Args[0]));

            break;

        case CLI_QOS_DEL_HS:
            /* apu4Args[0] = u4IfIdx   */
            /* apu4Args[1] = u4HL      */
            /* apu4Args[2] = u4SchedId */
            i4RetStatus = QoSCliDelHSEntry (CliHandle,
                                            CLI_PTR_TO_I4 (apu4Args[0]),
                                            CLI_PTR_TO_U4 (apu4Args[1]),
                                            CLI_PTR_TO_U4 (apu4Args[2]));
            break;

        case CLI_QOS_SHOW_HS:
            /* apu4Args[0] = u4IfIndex */
            i4RetStatus = QoSCliShowHSEntry (CliHandle,
                                             CLI_PTR_TO_I4 (apu4Args[0]));
            break;

        case CLI_QOS_SET_DUP:
            /* apu4Args[0] = u4IfIndex    */
            /* apu4Args[1] = i4UDPri      */
            i4RetStatus = QoSCliSetDUPEntry (CliHandle,
                                             CLI_PTR_TO_I4 (apu4Args[0]),
                                             CLI_PTR_TO_I4 (apu4Args[1]));
            break;

        case CLI_QOS_SET_PBIT_PREF:
            /* apu4Args[0] = u4IfIndex    */

            if (apu4Args[1] != NULL)
            {
                i4RetStatus = QoSCliSetPbitPreferenceOverDscp (CliHandle,
                                                               CLI_PTR_TO_I4
                                                               (apu4Args[0]),
                                                               QOS_PBIT_PREF_ENABLE);
            }
            else
            {
                i4RetStatus = QoSCliSetPbitPreferenceOverDscp (CliHandle,
                                                               CLI_PTR_TO_I4
                                                               (apu4Args[0]),
                                                               QOS_PBIT_PREF_DISABLE);
            }

            break;

        case CLI_QOS_TRCLASS_TO_PCP:
            i4RetStatus = QoSCliSetTrafficClassToPcp (CliHandle,
                                                      CLI_PTR_TO_I4
                                                      (apu4Args[0]),
                                                      CLI_PTR_TO_I4
                                                      (apu4Args[1]),
                                                      CLI_PTR_TO_I4
                                                      (apu4Args[2]));
            break;

        case CLI_QOS_NO_TRCLASS_TO_PCP:
            switch (CLI_PTR_TO_I4 (apu4Args[0]))
            {
                case 0:
                    i4Priority = 1;
                    break;
                case 1:
                    i4Priority = 2;
                    break;
                case 2:
                    i4Priority = 0;
                    break;
                default:
                    i4Priority = CLI_PTR_TO_I4 (apu4Args[0]);
            }
            i4RetStatus = QoSCliSetTrafficClassToPcp (CliHandle,
                                                      CLI_PTR_TO_I4
                                                      (apu4Args[0]),
                                                      i4Priority,
                                                      CLI_PTR_TO_I4
                                                      (apu4Args[1]));
            break;

        case CLI_QOS_DEBUG:
            i4RetStatus = QosCliSetTrace (CliHandle,
                                          CLI_PTR_TO_U4 (apu4Args[0]),
                                          QOS_TRUE);
            break;
        case CLI_QOS_NO_DEBUG:
            i4RetStatus = QosCliSetTrace (CliHandle,
                                          CLI_PTR_TO_U4 (apu4Args[0]),
                                          QOS_FALSE);
            break;

        case CLI_QOS_SHOW_DUP:
            /* apu4Args[0] = u4IfIndex */
            i4RetStatus = QoSCliShowDUPEntry (CliHandle,
                                              CLI_PTR_TO_I4 (apu4Args[0]));
            break;

        case CLI_QOS_SHOW_PBIT_PREF:
            i4RetStatus = QoSCliShowPbitPreference (CliHandle,
                                                    CLI_PTR_TO_I4 (apu4Args
                                                                   [0]));
            break;

        case CLI_QOS_SHOW_MET_STATS:
            /* apu4Args[0] = u4MeterIdx */
            i4RetStatus = QoSCliShowMetStats (CliHandle,
                                              CLI_PTR_TO_U4 (apu4Args[0]));
            break;

        case CLI_QOS_SHOW_Q_STATS:
            /* apu4Args[0] = u4IfIndex  */
            i4RetStatus = QoSCliShowQStats (CliHandle,
                                            CLI_PTR_TO_I4 (apu4Args[0]));
            break;

        case CLI_QOS_SHOW_DSCP_Q_MAP:
            i4RetStatus = QoSCliShowDscptoQMap (CliHandle);
            break;

        case CLI_QOS_CPU_RATE_CONF:
            i4RetStatus = QoSCliConfCpuRateLimit (CliHandle,
                                                  CLI_PTR_TO_U4 (apu4Args[0]),
                                                  CLI_PTR_TO_U4 (apu4Args[1]),
                                                  CLI_PTR_TO_U4 (apu4Args[2]));

            break;

        case CLI_QOS_SHOW_CPU_Q:
            i4RetStatus = QoSCliShowCpuRateLimit (CliHandle);

            break;

        case CLI_QOS_SET_METER_STATS:
            i4RetStatus = QosCliSetMeterStatStatus (CliHandle,
                                                    CLI_PTR_TO_U4 (apu4Args[0]),
                                                    CLI_PTR_TO_I4 (apu4Args
                                                                   [1]));
            break;
        case CLI_QOS_CLEAR_METER_STATS:
            i4RetStatus = QosCliClearMeterStats (CliHandle,
                                                 CLI_PTR_TO_U4 (apu4Args[0]));
            break;
        case CLI_QOS_SET_PCP_SEL_ROW:
            i4RetStatus = QoSCliSetPcpSelRow (CliHandle,
                                              CLI_PTR_TO_I4 (apu4Args[0]),
                                              CLI_PTR_TO_I4 (apu4Args[1]),
                                              CLI_PTR_TO_I4 (apu4Args[2]));
            break;

        case CLI_QOS_SHOW_PCP_SEL_ROW:
            i4RetStatus = QoSCliShowPcpSelRow (CliHandle,
                                               CLI_PTR_TO_I4 (apu4Args[0]),
                                               CLI_PTR_TO_I4 (apu4Args[1]));
            break;

        case CLI_QOS_SHOW_PORT_PCP_ENCODE:
            i4RetStatus = QoSCliShowPcpEncodingTable (CliHandle,
                                                      CLI_PTR_TO_I4 (apu4Args
                                                                     [0]),
                                                      CLI_PTR_TO_I4 (apu4Args
                                                                     [1]));
            break;
        case CLI_QOS_SHOW_PORT_PCP_DECODE:
            i4RetStatus = QoSCliShowPcpDecodingTable (CliHandle,
                                                      CLI_PTR_TO_I4 (apu4Args
                                                                     [0]),
                                                      CLI_PTR_TO_I4 (apu4Args
                                                                     [1]));
            break;
        case CLI_QOS_SHOW_PCP_QMAP:
            i4RetStatus = QoSCliShowPcpQueueMapping (CliHandle);
            break;

        default:

            CliPrintf (CliHandle, "\r%% Unknown QoS command. \r\n");
            QoSUnLock ();
            CliUnRegisterLock (CliHandle);

            return CLI_FAILURE;
    }

    if ((i4RetStatus == CLI_FAILURE) &&
        (CLI_GET_ERR (&u4ErrCode) == CLI_SUCCESS))
    {
        if (u4ErrCode >= CLI_ERR_START_ID_QOS)
        {
            CliPrintf (CliHandle, "\r%% %s",
                       gapi1QoSCliErrString[CLI_ERR_OFFSET_QOS (u4ErrCode)]);
        }
        CLI_SET_ERR (0);
    }

    CLI_SET_CMD_STATUS (i4RetStatus);
    QoSUnLock ();
    CliUnRegisterLock (CliHandle);

    return (i4RetStatus);

}                                /* End of  cli_process_qos_cmd () */

/*****************************************************************************/
/* Function Name      : QoSCliSetSysControl                                  */
/* Description        : This function is used to set the System Contol of    */
/*                      the QoS Module as Start or Shutdown.                 */
/* Input(s)           : CliHandle    - Handle to CLI session                 */
/*                    : i4SysControl - Start  / Shutdown                     */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : CLI_SUCCESS/CLI_FAILURE                              */
/* Called By          : cli_process_qos_cmd                                  */
/* Calling Function   : SNMP nmh Routines                                    */
/*****************************************************************************/
INT4
QoSCliSetSysControl (tCliHandle CliHandle, INT4 i4SysControl)
{
    INT4                i4RetStatus = SNMP_FAILURE;
    UINT4               u4ErrorCode = SNMP_ERR_NO_ERROR;

    UNUSED_PARAM (CliHandle);

    /* Test the Given Vlaue if it success then Set */
    i4RetStatus = nmhTestv2FsQoSSystemControl (&u4ErrorCode, i4SysControl);
    if (i4RetStatus == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    i4RetStatus = nmhSetFsQoSSystemControl (i4SysControl);
    if (i4RetStatus == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }

    return (CLI_SUCCESS);

}

/*****************************************************************************/
/* Function Name      : QoSCliSetStatus                                      */
/* Description        : This function is used to set the System status of    */
/*                      the QoS Module as Enable or Disable.                 */
/* Input(s)           : CliHandle    - Handle to CLI session                 */
/*                    : i4SysStatus  - Enable / Disable                      */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : CLI_SUCCESS/CLI_FAILURE                              */
/* Called By          : cli_process_qos_cmd                                  */
/* Calling Function   : SNMP nmh Routines                                    */
/*****************************************************************************/
INT4
QoSCliSetStatus (tCliHandle CliHandle, INT4 i4SysStatus)
{
    INT4                i4RetStatus = SNMP_FAILURE;
    UINT4               u4ErrorCode = SNMP_ERR_NO_ERROR;

    UNUSED_PARAM (CliHandle);

    /* Test the Given Vlaue if it success then Set */
    i4RetStatus = nmhTestv2FsQoSStatus (&u4ErrorCode, i4SysStatus);
    if (i4RetStatus == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    i4RetStatus = nmhSetFsQoSStatus (i4SysStatus);
    if (i4RetStatus == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }

    return (CLI_SUCCESS);

}

/*****************************************************************************/
/* Function Name      : VlanQueueingCliSetStatus                             */
/* Description        : This function is used to set the System status of    */
/*                      the Vlan based Queueing as Enable or Disable.        */
/* Input(s)           : CliHandle    - Handle to CLI session                 */
/*                    : i4SysStatus  - Enable / Disable                      */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : CLI_SUCCESS/CLI_FAILURE                              */
/* Called By          : cli_process_qos_cmd                                  */
/* Calling Function   : SNMP nmh Routines                                    */
/*****************************************************************************/
INT4
VlanQueueingCliSetStatus (tCliHandle CliHandle, INT4 i4SysStatus)
{
    INT4                i4RetStatus = SNMP_FAILURE;
    UINT4               u4ErrorCode = SNMP_ERR_NO_ERROR;
    UNUSED_PARAM (CliHandle);

    /* Test the Given Vlaue if it success then Set */
    i4RetStatus = nmhTestv2FsQoSVlanQueueingStatus (&u4ErrorCode, i4SysStatus);
    if (i4RetStatus == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    i4RetStatus = nmhSetFsQoSVlanQueueingStatus (i4SysStatus);
    if (i4RetStatus == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }
    if (i4SysStatus == VLAN_QUEUEING_SYS_STATUS_ENABLE)
    {
        CliPrintf (CliHandle, "\r\n <Information> Vlan Queueing enabled.\n\
                   Feature can be realised afetr Mib Save and Restore.\r\n");
    }
    else
    {
        CliPrintf (CliHandle, "\r\n <Information> Vlan Queueing disabled.\r\n");
    }
    return (CLI_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSCliShowGlobalInfo                                 */
/* Description        : This function is used to get the System Global       */
/*                      informations of QoS module.                          */
/* Input(s)           : CliHandle    - Handle to CLI session                 */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : CLI_SUCCESS/CLI_FAILURE                              */
/* Called By          : cli_process_qos_cmd                                  */
/* Calling Function   : SNMP nmh Routines                                    */
/*****************************************************************************/
INT4
QoSCliShowGlobalInfo (tCliHandle CliHandle)
{
    INT4                i4SysControl = 0;
    INT4                i4SysStatus = 0;
    INT4                i4RateUnit = 0;
    UINT4               u4TrcFlag = 0;
    UINT4               u4Granularity = 0;

    nmhGetFsQoSSystemControl (&i4SysControl);

    nmhGetFsQoSStatus (&i4SysStatus);

    nmhGetFsQoSTrcFlag (&u4TrcFlag);

    nmhGetFsQoSRateUnit (&i4RateUnit);

    nmhGetFsQoSRateGranularity (&u4Granularity);

    /* Print */
    CliPrintf (CliHandle, "QoS Global Information \r\n");
    CliPrintf (CliHandle, "---------------------- \r\n");

    if (i4SysControl == QOS_SYS_CNTL_START)
    {
        CliPrintf (CliHandle, "%-28s : %s\r\n", "System Control", "Start");
    }
    else
    {
        CliPrintf (CliHandle, "%-28s : %s\r\n", "System Control", "Shutdown");
    }

    if (i4SysStatus == QOS_SYS_STATUS_ENABLE)
    {
        CliPrintf (CliHandle, "%-28s : %s\r\n", "System Status", "Enable");
    }
    else
    {
        CliPrintf (CliHandle, "%-28s : %s\r\n", "System Status", "Disable");
    }

    switch (i4RateUnit)
    {
        case QOS_RATE_UNIT_BPS:
            CliPrintf (CliHandle, "%-28s : %s\r\n", "Rate Unit", "bps");
            break;
        case QOS_RATE_UNIT_KBPS:
            CliPrintf (CliHandle, "%-28s : %s\r\n", "Rate Unit", "kbps");
            break;
        case QOS_RATE_UNIT_MBPS:
            CliPrintf (CliHandle, "%-28s : %s\r\n", "Rate Unit", "mbps");
            break;
        case QOS_RATE_UNIT_GBPS:
            CliPrintf (CliHandle, "%-28s : %s\r\n", "Rate Unit", "gbps");
            break;
        default:
            CliPrintf (CliHandle, "%-28s : %s\r\n", "Rate Unit", "Invalid");
            break;
    }

    CliPrintf (CliHandle, "%-28s : %d\r\n", "Rate Granularity", u4Granularity);
    CliPrintf (CliHandle, "%-28s : %d\r\n", "Trace Flag", u4TrcFlag);
    CliPrintf (CliHandle, "\r\n");

    return (CLI_SUCCESS);

}

/*****************************************************************************/
/*                     PRIORITY MAP TABLE FUNCTIONS                          */
/*****************************************************************************/

/*****************************************************************************/
/* Function Name      : QoSPriMapPrompt                                      */
/* Description        : This function is used to set CLI Prompt string for   */
/*                      Priority map Table                                   */
/* Input(s)           : pi1ModeName   - Prompt Name from CLI                 */
/*                    : pi1PromptStr  - Prompt String to Display             */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : CLI_SUCCESS/CLI_FAILURE                              */
/* Called By          : CLI Task                                             */
/* Calling Function   : None.                                                */
/*****************************************************************************/
PUBLIC INT1
QoSPriMapPrompt (INT1 *pi1ModeName, INT1 *pi1PromptStr)
{
    UINT4               u4Idx;
    UINT4               u4Len = STRLEN (QOS_CLI_PRI_MAP_MODE);

    if ((!pi1PromptStr) || (!pi1ModeName))
    {
        return FALSE;
    }

    if (STRNCMP (pi1ModeName, QOS_CLI_PRI_MAP_MODE, u4Len) != 0)
    {
        return FALSE;
    }

    pi1ModeName = pi1ModeName + u4Len;
    u4Idx = CLI_ATOI (pi1ModeName);
    CLI_SET_PRI_MAP_ID (u4Idx);
    STRNCPY (pi1PromptStr, "(config-pri-map)#", STRLEN ("(config-pri-map)#"));
    pi1PromptStr[STRLEN ("(config-pri-map)#")] = '\0';

    return TRUE;
}

/*****************************************************************************/
/* Function Name      : QoSCliGetPriMapIndexFromName                         */
/* Description        : This function is used to get the Table Index from    */
/*                      the Given Table Name. If Name is not found it give   */
/*                      the next free Index                                  */
/* Input(s)           : pInOctStrName - Table Name.                          */
/*                    : pu4Idx        - Ptr to the Index of the Table for    */
/*                    :                 the given Name                       */
/*                    : pu4Status     - Status says that the  Entry needs to */
/*                    :                 be create or modify                  */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : CLI_SUCCESS/CLI_FAILURE                              */
/* Called By          : PriMapTble Functions                                 */
/* Calling Function   : SNMP nmh Routines                                    */
/*****************************************************************************/
INT4
QoSCliGetPriMapIndexFromName (tSNMP_OCTET_STRING_TYPE * pInOctStrName,
                              UINT4 *pu4Idx, UINT4 *pu4Status)
{
    tSNMP_OCTET_STRING_TYPE *pOctStrName = NULL;
    UINT4               u4CurrIdx = 0;
    UINT4               u4PreIdx = 0;
    UINT4               u4IdxFound = CLI_FAILURE;
    INT4                i4SysControl = 0;
    INT4                i4RetStatus = SNMP_FAILURE;

    /* System Contol map be QOS_SYS_CNTL_SHUTDOWN */
    i4RetStatus = nmhGetFsQoSSystemControl (&i4SysControl);
    if ((i4RetStatus == SNMP_FAILURE)
        || (i4SysControl == QOS_SYS_CNTL_SHUTDOWN))
    {
        CLI_SET_ERR (QOS_CLI_ERR_MODULE_SHUTDOW);
        return (CLI_FAILURE);
    }

    /* No Entry Found Create first Entry  idx = 1 */
    i4RetStatus = nmhGetFirstIndexFsQoSPriorityMapTable (&u4CurrIdx);
    if (i4RetStatus == SNMP_FAILURE)
    {
        *pu4Idx = 1;
        *pu4Status = QOS_CLI_NEW_ENTRY;
        return (CLI_SUCCESS);
    }

    /* Allocate Memory For Name */
    pOctStrName = allocmem_octetstring (QOS_MAX_TABLE_NAME);
    if (pOctStrName == NULL)
    {
        return (CLI_FAILURE);
    }
    MEMSET (pOctStrName->pu1_OctetList, 0, pOctStrName->i4_Length);

    do
    {
        /* Get the Name of the Current Entry */
        i4RetStatus = nmhGetFsQoSPriorityMapName (u4CurrIdx, pOctStrName);
        if (i4RetStatus == SNMP_FAILURE)
        {
            free_octetstring (pOctStrName);
            return (CLI_FAILURE);
        }

        if (STRCMP (pOctStrName->pu1_OctetList, pInOctStrName->pu1_OctetList)
            == 0)
        {
            *pu4Idx = u4CurrIdx;
            *pu4Status = QOS_CLI_MODIFY_ENTRY;

            free_octetstring (pOctStrName);
            return (CLI_SUCCESS);
        }

        if (((u4CurrIdx - u4PreIdx) > 1) && (u4IdxFound == CLI_FAILURE))
        {
            *pu4Idx = u4PreIdx + 1;
            *pu4Status = QOS_CLI_NEW_ENTRY;
            u4IdxFound = CLI_SUCCESS;
        }

        u4PreIdx = u4CurrIdx;

        i4RetStatus = nmhGetNextIndexFsQoSPriorityMapTable (u4CurrIdx,
                                                            &u4CurrIdx);
    }
    while (i4RetStatus == SNMP_SUCCESS);

    if ((u4CurrIdx == u4PreIdx) && (u4IdxFound == CLI_FAILURE))
    {
        *pu4Idx = u4CurrIdx + 1;
        *pu4Status = QOS_CLI_NEW_ENTRY;
    }

    free_octetstring (pOctStrName);
    return (CLI_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSCliAddPriMapEntry                                 */
/* Description        : This function is used to create Priority Map table   */
/*                      Entry and change the CLI Prompt.                     */
/* Input(s)           : CliHandle    - Handle to CLI session                 */
/*                    : pu1Name      - Table Entry Name to Create.           */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : CLI_SUCCESS/CLI_FAILURE                              */
/* Called By          : cli_process_qos_cmd                                  */
/* Calling Function   : SNMP nmh Routines                                    */
/*****************************************************************************/
INT4
QoSCliAddPriMapEntry (tCliHandle CliHandle, UINT4 u4Idx)
{
    UINT1               au1PriMap[QOS_CLI_MAX_PROMPT_LENGTH];
    INT4                i4RetStatus = SNMP_FAILURE;
    INT4                i4RowStatus = SNMP_FAILURE;
    UINT4               u4ErrorCode = SNMP_ERR_NO_ERROR;

    UNUSED_PARAM (CliHandle);

    i4RetStatus = nmhGetFsQoSPriorityMapStatus (u4Idx, &i4RowStatus);
    if (i4RetStatus == SNMP_FAILURE)
    {
        /* Create new Entry */
        i4RetStatus = nmhTestv2FsQoSPriorityMapStatus (&u4ErrorCode,
                                                       u4Idx, CREATE_AND_WAIT);
        if (i4RetStatus == SNMP_FAILURE)
        {
            return (CLI_FAILURE);
        }

        i4RetStatus = nmhSetFsQoSPriorityMapStatus (u4Idx, CREATE_AND_WAIT);
        if (i4RetStatus == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }
    }

    /* Go to the Priority Map Mode of Cli Prompt */
    SPRINTF ((CHR1 *) au1PriMap, "%s%u", QOS_CLI_PRI_MAP_MODE, u4Idx);

    if (CliChangePath ((CHR1 *) au1PriMap) == CLI_FAILURE)
    {
        CliPrintf (CliHandle, "\r%% Unable to enter Priority Map mode.\r\n");
        return (CLI_FAILURE);
    }

    return (CLI_SUCCESS);

}

/*****************************************************************************/
/* Function Name      : QoSCliAddVlanMapEntry                                */
/* Description        : This function is used to create Vlan Map table       */
/*                      Entry and change the CLI Prompt.                     */
/* Input(s)           : CliHandle    - Handle to CLI session                 */
/*                    : pu1Name      - Table Entry Name to Create.           */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : CLI_SUCCESS/CLI_FAILURE                              */
/* Called By          : cli_process_qos_cmd                                  */
/* Calling Function   : SNMP nmh Routines                                    */
/*****************************************************************************/
INT4
QoSCliAddVlanMapEntry (tCliHandle CliHandle, UINT4 u4Idx)
{
    UINT1               au1VlanQMap[QOS_CLI_MAX_PROMPT_LENGTH];
    INT4                i4RetStatus = SNMP_FAILURE;
    INT4                i4RowStatus = SNMP_FAILURE;
    UINT4               u4ErrorCode = SNMP_ERR_NO_ERROR;

    UNUSED_PARAM (CliHandle);

    i4RetStatus = nmhGetFsQoSVlanMapStatus (u4Idx, &i4RowStatus);
    if (i4RetStatus == SNMP_FAILURE)
    {
        /* Create new Entry */
        i4RetStatus = nmhTestv2FsQoSVlanMapStatus (&u4ErrorCode,
                                                   u4Idx, CREATE_AND_WAIT);
        if (i4RetStatus == SNMP_FAILURE)
        {
            return (CLI_FAILURE);
        }
        i4RetStatus = nmhSetFsQoSVlanMapStatus (u4Idx, CREATE_AND_WAIT);
        if (i4RetStatus == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }
    }

    /* Go to the Vlan Map Mode of Cli Prompt */
    SPRINTF ((CHR1 *) au1VlanQMap, "%s%u", QOS_CLI_VLAN_MAP_MODE, u4Idx);
    if (CliChangePath ((CHR1 *) au1VlanQMap) == CLI_FAILURE)
    {
        CliPrintf (CliHandle, "\r%% Unable to enter Vlan Map mode.\r\n");
        return (CLI_FAILURE);
    }
    return (CLI_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSCliDelVlanMapEntry                                */
/* Description        : This function is used to delete Vlan Map table       */
/*                      Entry.                                               */
/* Input(s)           : CliHandle    - Handle to CLI session                 */
/*                    : pu1Name      - Table Entry Name to Delete.           */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : CLI_SUCCESS/CLI_FAILURE                              */
/* Called By          : cli_process_qos_cmd                                  */
/* Calling Function   : SNMP nmh Routines                                    */
/*****************************************************************************/
INT4
QoSCliDelVlanMapEntry (tCliHandle CliHandle, UINT4 u4Idx)
{
    INT4                i4RetStatus = SNMP_FAILURE;
    UINT4               u4ErrorCode = SNMP_ERR_NO_ERROR;

    UNUSED_PARAM (CliHandle);

    /* Delete a Entry */
    i4RetStatus = nmhTestv2FsQoSVlanMapStatus (&u4ErrorCode, u4Idx, DESTROY);
    if (i4RetStatus == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    i4RetStatus = nmhSetFsQoSVlanMapStatus (u4Idx, DESTROY);
    if (i4RetStatus == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }
    return (CLI_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSCliDelPriMapEntry                                 */
/* Description        : This function is used to delete Priority Map table   */
/*                      Entry.                                               */
/* Input(s)           : CliHandle    - Handle to CLI session                 */
/*                    : pu1Name      - Table Entry Name to Delete.           */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : CLI_SUCCESS/CLI_FAILURE                              */
/* Called By          : cli_process_qos_cmd                                  */
/* Calling Function   : SNMP nmh Routines                                    */
/*****************************************************************************/
INT4
QoSCliDelPriMapEntry (tCliHandle CliHandle, UINT4 u4Idx)
{
    INT4                i4RetStatus = SNMP_FAILURE;
    UINT4               u4ErrorCode = SNMP_ERR_NO_ERROR;

    UNUSED_PARAM (CliHandle);

    /* Delete a Entry */
    i4RetStatus = nmhTestv2FsQoSPriorityMapStatus (&u4ErrorCode, u4Idx,
                                                   DESTROY);
    if (i4RetStatus == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    i4RetStatus = nmhSetFsQoSPriorityMapStatus (u4Idx, DESTROY);
    if (i4RetStatus == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }

    return (CLI_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSCliRSPriMapTblEntry                               */
/* Description        : This function is used to reset Priority Map table    */
/*                      Entry Parameter                                      */
/*                    : u4IfNum      - Interface Name.                       */
/*                    : u4VlanId     - Vlan Id                               */
/*                    : u1InPriType  - Incomming Priority Type               */
/*                    : u1InPri      - Incomming Priority Value              */
/*                    : u1TrafCls    - Regene Priority Value                 */
/*                    : i1InnerReGenPri - Inner Regene Priority Value        */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : CLI_SUCCESS/CLI_FAILURE                              */
/* Called By          : cli_process_qos_cmd                                  */
/* Calling Function   : SNMP nmh Routines                                    */
/*****************************************************************************/
VOID
QoSCliRSPriMapTblEntry (UINT4 u4Idx, UINT4 u4CurIfNum, UINT4 u4CurVlanId,
                        UINT4 u4CurInPriType, UINT4 u4CurInPri,
                        UINT4 u4CurRegenPri, UINT4 u4CurInnerRegenPri,
                        UINT4 u4CurInDEI, UINT4 u4CurRegenColor,
                        INT4 i4RowStatus)
{
    INT4                i4RetStatus = SNMP_FAILURE;

    if (u4CurIfNum != (UINT4) CLI_RS_INVALID_MASK)
    {
        i4RetStatus = nmhSetFsQoSPriorityMapIfIndex (u4Idx, u4CurIfNum);
    }
    if (u4CurVlanId != (UINT4) CLI_RS_INVALID_MASK)
    {
        i4RetStatus = nmhSetFsQoSPriorityMapVlanId (u4Idx, u4CurVlanId);
    }

    if (u4CurInPriType != (UINT4) CLI_RS_INVALID_MASK)
    {
        i4RetStatus = nmhSetFsQoSPriorityMapInPriType (u4Idx, u4CurInPriType);
    }

    if (u4CurInPri != (UINT4) CLI_RS_INVALID_MASK)
    {
        i4RetStatus = nmhSetFsQoSPriorityMapInPriority (u4Idx, u4CurInPri);
    }

    if (u4CurRegenPri != (UINT4) CLI_RS_INVALID_MASK)
    {
        i4RetStatus = nmhSetFsQoSPriorityMapRegenPriority (u4Idx,
                                                           u4CurRegenPri);
    }

    if (u4CurInnerRegenPri != (UINT4) CLI_RS_INVALID_MASK)
    {
        i4RetStatus =
            nmhSetFsQoSPriorityMapRegenInnerPriority (u4Idx,
                                                      u4CurInnerRegenPri);
    }
    if (u4CurInDEI != (UINT4) CLI_RS_INVALID_MASK)
    {
        i4RetStatus = nmhSetFsQoSPriorityMapInDEI (u4Idx, u4CurInDEI);
    }

    if (u4CurRegenColor != (UINT4) CLI_RS_INVALID_MASK)
    {
        i4RetStatus = nmhSetFsQoSPriorityMapRegenColor (u4Idx, u4CurRegenColor);
    }

    i4RetStatus = nmhSetFsQoSPriorityMapStatus (u4Idx, i4RowStatus);

    UNUSED_PARAM (i4RetStatus);

}

/*****************************************************************************/
/* Function Name      : QoSCliRSVlanMapTblEntry                              */
/* Description        : This function is used to reset VLan Map table Entry  */
/*                      Parameter                                            */
/*                    : u4IfNum      - Interface Name.                       */
/*                    : u4VlanId     - Vlan Id                               */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : CLI_SUCCESS/CLI_FAILURE                              */
/* Called By          : cli_process_qos_cmd                                  */
/* Calling Function   : SNMP nmh Routines                                    */
/*****************************************************************************/
VOID
QoSCliRSVlanMapTblEntry (UINT4 u4Idx, UINT4 u4CurIfNum,
                         UINT4 u4CurVlanId, INT4 i4RowStatus)
{
    INT4                i4RetStatus = SNMP_FAILURE;

    if (u4CurIfNum != (UINT4) CLI_RS_INVALID_MASK)
    {
        i4RetStatus = nmhSetFsQoSVlanMapIfIndex (u4Idx, u4CurIfNum);
    }
    if (u4CurVlanId != (UINT4) CLI_RS_INVALID_MASK)
    {
        i4RetStatus = nmhSetFsQoSVlanMapVlanId (u4Idx, u4CurVlanId);
    }
    i4RetStatus = nmhSetFsQoSVlanMapStatus (u4Idx, i4RowStatus);
    UNUSED_PARAM (i4RetStatus);
}

/*****************************************************************************/
/* Function Name      : QoSCliSetPriMapParams                                */
/* Description        : This function is used to Set Priority Map table Entry*/
/*                      Parameter                                            */
/* Input(s)           : CliHandle    - Handle to CLI session                 */
/*                    : u4IfNum      - Interface Name.                       */
/*                    : u4VlanId     - Vlan Id                               */
/*                    : u1InPriType  - Incomming Priority Type               */
/*                    : u1InPri      - Incomming Priority Value              */
/*                    : u1TrafCls    - Regene Priority Value                 */
/*                    : i1InnerReGenPri - Inner Regene Priority Value        */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : CLI_SUCCESS/CLI_FAILURE                              */
/* Called By          : cli_process_qos_cmd                                  */
/* Calling Function   : SNMP nmh Routines                                    */
/*****************************************************************************/

INT4
QoSCliSetPriMapParams (tCliHandle CliHandle, UINT4 u4IfNum, UINT4 u4VlanId,
                       UINT1 u1InPriType, UINT1 u1InPri, UINT1 u1TrafCls,
                       INT1 i1InnerReGenPri, INT4 i4InDEI, INT4 i4RegenColor)
{
    UINT4               u4Idx = 0;
    INT4                i4RetStatus = SNMP_FAILURE;
    INT4                i4RowStatus = SNMP_FAILURE;
    INT4                i4CurInDEI = 0;
    UINT4               u4ErrorCode = SNMP_ERR_NO_ERROR;

    UINT4               u4CurIfNum = (UINT4) CLI_RS_INVALID_MASK;
    UINT4               u4CurVlanId = (UINT4) CLI_RS_INVALID_MASK;
    UINT4               u4CurInPriType = (UINT4) CLI_RS_INVALID_MASK;
    UINT4               u4CurInPri = (UINT4) CLI_RS_INVALID_MASK;
    UINT4               u4CurRegenPri = (UINT4) CLI_RS_INVALID_MASK;
    UINT4               u4CurInnerRegenPri = (UINT4) CLI_RS_INVALID_MASK;
    UINT4               u4CurInDEI = (UINT4) CLI_RS_INVALID_MASK;
    UINT4               u4CurRegenColor = (UINT4) CLI_RS_INVALID_MASK;

    /* Get the Priority Map Id */
    u4Idx = CLI_GET_PRI_MAP_ID ();

    /* Get the Status of this Entry */
    i4RetStatus = nmhGetFsQoSPriorityMapStatus (u4Idx, &i4RowStatus);
    if (i4RetStatus == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }
    /* if RowStatus = NOT_IN_SERVICE or NOT_READY test and set the Values. 
     * if RowStatus = ACTIVE Set the RowStatus as NOT_IN_SERVICE and than   
     * test and set the Values 
     */
    if (i4RowStatus == ACTIVE)
    {
        i4RetStatus = nmhTestv2FsQoSPriorityMapStatus (&u4ErrorCode,
                                                       u4Idx, NOT_IN_SERVICE);
        if (i4RetStatus == SNMP_FAILURE)
        {
            return (CLI_FAILURE);
        }

        i4RetStatus = nmhSetFsQoSPriorityMapStatus (u4Idx, NOT_IN_SERVICE);
        if (i4RetStatus == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }
    }

    /*
     * 1. Test New value 
     * 2. Get Current value
     * 3. Set New value
     * 4. if Set Failed Restore Old Value else goto #1
     */

    if (u4IfNum != 0)
    {
        i4RetStatus = nmhTestv2FsQoSPriorityMapIfIndex (&u4ErrorCode, u4Idx,
                                                        u4IfNum);
        if (i4RetStatus == SNMP_FAILURE)
        {
            QoSCliRSPriMapTblEntry (u4Idx, u4CurIfNum, u4CurVlanId,
                                    u4CurInPriType, u4CurInPri, u4CurRegenPri,
                                    u4CurInnerRegenPri, u4CurInDEI,
                                    u4CurRegenColor, i4RowStatus);

            return (CLI_FAILURE);
        }

        i4RetStatus = nmhGetFsQoSPriorityMapIfIndex (u4Idx, &u4CurIfNum);
        if (i4RetStatus == SNMP_FAILURE)
        {
            return (CLI_FAILURE);
        }

        i4RetStatus = nmhSetFsQoSPriorityMapIfIndex (u4Idx, u4IfNum);
        if (i4RetStatus == SNMP_FAILURE)
        {
            QoSCliRSPriMapTblEntry (u4Idx, u4CurIfNum, u4CurVlanId,
                                    u4CurInPriType, u4CurInPri, u4CurRegenPri,
                                    u4CurInnerRegenPri, u4CurInDEI,
                                    u4CurRegenColor, i4RowStatus);

            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }
    }

    if (u4VlanId != 0)
    {
        i4RetStatus = nmhTestv2FsQoSPriorityMapVlanId (&u4ErrorCode, u4Idx,
                                                       u4VlanId);
        if (i4RetStatus == SNMP_FAILURE)
        {
            QoSCliRSPriMapTblEntry (u4Idx, u4CurIfNum, u4CurVlanId,
                                    u4CurInPriType, u4CurInPri, u4CurRegenPri,
                                    u4CurInnerRegenPri, u4CurInDEI,
                                    u4CurRegenColor, i4RowStatus);

            return (CLI_FAILURE);
        }

        i4RetStatus = nmhGetFsQoSPriorityMapVlanId (u4Idx, &u4CurVlanId);
        if (i4RetStatus == SNMP_FAILURE)
        {
            return (CLI_FAILURE);
        }

        i4RetStatus = nmhSetFsQoSPriorityMapVlanId (u4Idx, u4VlanId);
        if (i4RetStatus == SNMP_FAILURE)
        {
            QoSCliRSPriMapTblEntry (u4Idx, u4CurIfNum, u4CurVlanId,
                                    u4CurInPriType, u4CurInPri, u4CurRegenPri,
                                    u4CurInnerRegenPri, u4CurInDEI,
                                    u4CurRegenColor, i4RowStatus);

            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }
    }

    i4RetStatus = nmhTestv2FsQoSPriorityMapInPriType (&u4ErrorCode, u4Idx,
                                                      u1InPriType);
    if (i4RetStatus == SNMP_FAILURE)
    {
        QoSCliRSPriMapTblEntry (u4Idx, u4CurIfNum, u4CurVlanId,
                                u4CurInPriType, u4CurInPri, u4CurRegenPri,
                                u4CurInnerRegenPri, u4CurInDEI,
                                u4CurRegenColor, i4RowStatus);

        return (CLI_FAILURE);
    }

    i4RetStatus = nmhGetFsQoSPriorityMapInPriType (u4Idx,
                                                   (INT4 *) &u4CurInPriType);
    if (i4RetStatus == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    i4RetStatus = nmhSetFsQoSPriorityMapInPriType (u4Idx, u1InPriType);
    if (i4RetStatus == SNMP_FAILURE)
    {
        QoSCliRSPriMapTblEntry (u4Idx, u4CurIfNum, u4CurVlanId,
                                u4CurInPriType, u4CurInPri, u4CurRegenPri,
                                u4CurInnerRegenPri, u4CurInDEI,
                                u4CurRegenColor, i4RowStatus);

        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }

    i4RetStatus = nmhTestv2FsQoSPriorityMapInPriority (&u4ErrorCode, u4Idx,
                                                       u1InPri);
    if (i4RetStatus == SNMP_FAILURE)
    {
        QoSCliRSPriMapTblEntry (u4Idx, u4CurIfNum, u4CurVlanId,
                                u4CurInPriType, u4CurInPri, u4CurRegenPri,
                                u4CurInnerRegenPri, u4CurInDEI,
                                u4CurRegenColor, i4RowStatus);
        return (CLI_FAILURE);
    }
    if (u1InPriType == QOS_CLI_IN_PRI_TYPE_DOT1P)
    {
        i4RetStatus = nmhTestv2FsQoSPriorityMapInDEI (&u4ErrorCode, u4Idx,
                                                      i4InDEI);
        if (i4RetStatus == SNMP_FAILURE)
        {
            QoSCliRSPriMapTblEntry (u4Idx, u4CurIfNum, u4CurVlanId,
                                    u4CurInPriType, u4CurInPri,
                                    u4CurRegenPri,
                                    u4CurInnerRegenPri, u4CurInDEI,
                                    u4CurRegenColor, i4RowStatus);
            return (CLI_FAILURE);
        }
    }

    i4RetStatus =
        nmhGetFsQoSPriorityMapInPriority (u4Idx, (INT4 *) &u4CurInPri);
    if (i4RetStatus == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }
    if (u1InPriType == QOS_CLI_IN_PRI_TYPE_DOT1P)
    {
        i4RetStatus = nmhGetFsQoSPriorityMapInDEI (u4Idx, &i4CurInDEI);
        if (i4RetStatus == SNMP_FAILURE)
        {
            QoSCliRSPriMapTblEntry (u4Idx, u4CurIfNum, u4CurVlanId,
                                    u4CurInPriType, u4CurInPri,
                                    u4CurRegenPri,
                                    u4CurInnerRegenPri, u4CurInDEI,
                                    u4CurRegenColor, i4RowStatus);
            return (CLI_FAILURE);
        }
    }
    i4RetStatus = nmhSetFsQoSPriorityMapInPriority (u4Idx, u1InPri);
    if (i4RetStatus == SNMP_FAILURE)
    {
        QoSCliRSPriMapTblEntry (u4Idx, u4CurIfNum, u4CurVlanId,
                                u4CurInPriType, u4CurInPri, u4CurRegenPri,
                                u4CurInnerRegenPri, u4CurInDEI,
                                u4CurRegenColor, i4RowStatus);

        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }
    if (u1InPriType == QOS_CLI_IN_PRI_TYPE_DOT1P)
    {
        i4RetStatus = nmhSetFsQoSPriorityMapInDEI (u4Idx, i4InDEI);
        if (i4RetStatus == SNMP_FAILURE)
        {
            QoSCliRSPriMapTblEntry (u4Idx, u4CurIfNum, u4CurVlanId,
                                    u4CurInPriType, u4CurInPri,
                                    u4CurRegenPri,
                                    u4CurInnerRegenPri, u4CurInDEI,
                                    u4CurRegenColor, i4RowStatus);
            return (CLI_FAILURE);
        }
    }

    i4RetStatus = nmhTestv2FsQoSPriorityMapRegenPriority (&u4ErrorCode, u4Idx,
                                                          u1TrafCls);
    if (i4RetStatus == SNMP_FAILURE)
    {
        QoSCliRSPriMapTblEntry (u4Idx, u4CurIfNum, u4CurVlanId,
                                u4CurInPriType, u4CurInPri, u4CurRegenPri,
                                u4CurInnerRegenPri, u4CurInDEI,
                                u4CurRegenColor, i4RowStatus);

        return (CLI_FAILURE);
    }

    i4RetStatus = nmhGetFsQoSPriorityMapRegenPriority (u4Idx, &u4CurRegenPri);
    if (i4RetStatus == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    i4RetStatus = nmhSetFsQoSPriorityMapRegenPriority (u4Idx, u1TrafCls);
    if (i4RetStatus == SNMP_FAILURE)
    {
        QoSCliRSPriMapTblEntry (u4Idx, u4CurIfNum, u4CurVlanId,
                                u4CurInPriType, u4CurInPri, u4CurRegenPri,
                                u4CurInnerRegenPri, u4CurInDEI,
                                u4CurRegenColor, i4RowStatus);

        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }

    if (i1InnerReGenPri != -1)
    {
        i4RetStatus =
            nmhTestv2FsQoSPriorityMapRegenInnerPriority (&u4ErrorCode, u4Idx,
                                                         i1InnerReGenPri);
        if (i4RetStatus == SNMP_FAILURE)
        {
            QoSCliRSPriMapTblEntry (u4Idx, u4CurIfNum, u4CurVlanId,
                                    u4CurInPriType, u4CurInPri, u4CurRegenPri,
                                    u4CurInnerRegenPri, u4CurInDEI,
                                    u4CurRegenColor, i4RowStatus);

            return (CLI_FAILURE);
        }

        i4RetStatus =
            nmhGetFsQoSPriorityMapRegenInnerPriority (u4Idx,
                                                      &u4CurInnerRegenPri);
        if (i4RetStatus == SNMP_FAILURE)
        {
            return (CLI_FAILURE);
        }

        i4RetStatus =
            nmhSetFsQoSPriorityMapRegenInnerPriority (u4Idx, i1InnerReGenPri);

        if (i4RetStatus == SNMP_FAILURE)
        {
            QoSCliRSPriMapTblEntry (u4Idx, u4CurIfNum, u4CurVlanId,
                                    u4CurInPriType, u4CurInPri, u4CurRegenPri,
                                    u4CurInnerRegenPri, u4CurInDEI,
                                    u4CurRegenColor, i4RowStatus);

            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }
    }
    if (i4RegenColor != 0)
    {
        i4RetStatus =
            nmhTestv2FsQoSPriorityMapRegenColor (&u4ErrorCode, u4Idx,
                                                 i4RegenColor);
        if (i4RetStatus == SNMP_FAILURE)
        {
            QoSCliRSPriMapTblEntry (u4Idx, u4CurIfNum, u4CurVlanId,
                                    u4CurInPriType, u4CurInPri, u4CurRegenPri,
                                    u4CurInnerRegenPri, u4CurInDEI,
                                    u4CurRegenColor, i4RowStatus);
            return (CLI_FAILURE);
        }

        i4RetStatus = nmhGetFsQoSPriorityMapRegenColor (u4Idx, &i4RegenColor);
        if (i4RetStatus == SNMP_FAILURE)
        {
            return (CLI_FAILURE);
        }

        i4RetStatus = nmhSetFsQoSPriorityMapRegenColor (u4Idx, i4RegenColor);

        if (i4RetStatus == SNMP_FAILURE)
        {
            QoSCliRSPriMapTblEntry (u4Idx, u4CurIfNum, u4CurVlanId,
                                    u4CurInPriType, u4CurInPri, u4CurRegenPri,
                                    u4CurInnerRegenPri, u4CurInDEI,
                                    u4CurRegenColor, i4RowStatus);
            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }
    }

    /* Set Entry as ACTIVE */
    i4RetStatus = nmhTestv2FsQoSPriorityMapStatus (&u4ErrorCode, u4Idx, ACTIVE);
    if (i4RetStatus == SNMP_FAILURE)
    {
        QoSCliRSPriMapTblEntry (u4Idx, u4CurIfNum, u4CurVlanId,
                                u4CurInPriType, u4CurInPri, u4CurRegenPri,
                                u4CurInnerRegenPri, u4CurInDEI,
                                u4CurRegenColor, i4RowStatus);

        return (CLI_FAILURE);
    }

    i4RetStatus = nmhSetFsQoSPriorityMapStatus (u4Idx, ACTIVE);
    if (i4RetStatus == SNMP_FAILURE)
    {
        QoSCliRSPriMapTblEntry (u4Idx, u4CurIfNum, u4CurVlanId,
                                u4CurInPriType, u4CurInPri, u4CurRegenPri,
                                u4CurInnerRegenPri, u4CurInDEI,
                                u4CurRegenColor, i4RowStatus);

        ISSCliCheckAndThrowFatalError (CliHandle);
        return (CLI_FAILURE);
    }
    if (u1InPriType == QOS_CLI_IN_PRI_TYPE_DOT1P)
    {
        QoSUtlStoreOperEncodingInfo (u4IfNum, u1InPri,
                                     (UINT1) i4InDEI, u1TrafCls, 0,
                                     QOS_PCP_PKT_TYPE_DOT1P,
                                     QOS_PCP_TBL_ACTION_MODIFY);
    }
    if (u1InPriType == QOS_CLI_IN_PRI_TYPE_IP_DSCP)
    {
        QoSUtlStoreOperEncodingInfo (u4IfNum, u1InPri,
                                     0, u1TrafCls, 0,
                                     QOS_PCP_PKT_TYPE_IP,
                                     QOS_PCP_TBL_ACTION_MODIFY);
    }
    if (u1InPriType == QOS_CLI_IN_PRI_TYPE_MPLS_EXP)
    {
        QoSUtlStoreOperEncodingInfo (u4IfNum, u1InPri,
                                     0, u1TrafCls, 0,
                                     QOS_PCP_PKT_TYPE_MPLS,
                                     QOS_PCP_TBL_ACTION_MODIFY);
    }

    return (CLI_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSCliSetVlanMapParams                               */
/* Description        : This function is used to Set Vlan Map table Entry    */
/*                      Parameter                                            */
/* Input(s)           : CliHandle    - Handle to CLI session                 */
/*                    : u4IfNum      - Interface Name.                       */
/*                    : u4VlanId     - Vlan Id                               */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : CLI_SUCCESS/CLI_FAILURE                              */
/* Called By          : cli_process_qos_cmd                                  */
/* Calling Function   : SNMP nmh Routines                                    */
/*****************************************************************************/
INT4
QoSCliSetVlanMapParams (tCliHandle CliHandle, UINT4 u4IfNum, UINT4 u4VlanId)
{

    UINT4               u4Idx = 0;
    INT4                i4RetStatus = SNMP_FAILURE;
    INT4                i4RowStatus = SNMP_FAILURE;
    UINT4               u4ErrorCode = SNMP_ERR_NO_ERROR;
    UINT4               u4CurIfNum = (UINT4) CLI_RS_INVALID_MASK;
    UINT4               u4CurVlanId = (UINT4) CLI_RS_INVALID_MASK;

    /* Get the Priority Map Id */
    u4Idx = (UINT4) CLI_GET_VLAN_MAP_ID ();

    /* Get the Status of this Entry */
    i4RetStatus = nmhGetFsQoSVlanMapStatus (u4Idx, &i4RowStatus);
    if (i4RetStatus == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }
    /* if RowStatus = NOT_IN_SERVICE or NOT_READY test and set the Values.
     * if RowStatus = ACTIVE Set the RowStatus as NOT_IN_SERVICE and then
     * test and set the Values
     */
    if (i4RowStatus == ACTIVE)
    {
        i4RetStatus = nmhTestv2FsQoSVlanMapStatus (&u4ErrorCode,
                                                   u4Idx, NOT_IN_SERVICE);
        if (i4RetStatus == SNMP_FAILURE)
        {
            return (CLI_FAILURE);
        }

        i4RetStatus = nmhSetFsQoSVlanMapStatus (u4Idx, NOT_IN_SERVICE);
        if (i4RetStatus == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }
    }

    /*
     * 1. Test New value
     * 2. Get Current value
     * 3. Set New value
     * 4. if Set Failed Restore Old Value else goto #1
     */

    /* Interface Index  */
    if (u4IfNum != 0)
    {
        i4RetStatus = nmhTestv2FsQoSVlanMapIfIndex (&u4ErrorCode, u4Idx,
                                                    u4IfNum);
        if (i4RetStatus == SNMP_FAILURE)
        {
            QoSCliRSVlanMapTblEntry (u4Idx, u4CurIfNum, u4CurVlanId,
                                     i4RowStatus);
            return (CLI_FAILURE);
        }

        i4RetStatus = nmhGetFsQoSVlanMapIfIndex (u4Idx, &u4CurIfNum);
        if (i4RetStatus == SNMP_FAILURE)
        {
            return (CLI_FAILURE);
        }

        i4RetStatus = nmhSetFsQoSVlanMapIfIndex (u4Idx, u4IfNum);
        if (i4RetStatus == SNMP_FAILURE)
        {
            QoSCliRSVlanMapTblEntry (u4Idx, u4CurIfNum, u4CurVlanId,
                                     i4RowStatus);
            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }
    }

    /* VLAN ID */
    if (u4VlanId != 0)
    {
        i4RetStatus = nmhTestv2FsQoSVlanMapVlanId (&u4ErrorCode, u4Idx,
                                                   u4VlanId);
        if (i4RetStatus == SNMP_FAILURE)
        {
            QoSCliRSVlanMapTblEntry (u4Idx, u4CurIfNum, u4CurVlanId,
                                     i4RowStatus);
            return (CLI_FAILURE);
        }

        i4RetStatus = nmhGetFsQoSVlanMapVlanId (u4Idx, &u4CurVlanId);
        if (i4RetStatus == SNMP_FAILURE)
        {
            return (CLI_FAILURE);
        }

        i4RetStatus = nmhSetFsQoSVlanMapVlanId (u4Idx, u4VlanId);
        if (i4RetStatus == SNMP_FAILURE)
        {
            QoSCliRSVlanMapTblEntry (u4Idx, u4CurIfNum, u4CurVlanId,
                                     i4RowStatus);
            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }
    }

    /* Set Entry as ACTIVE */
    i4RetStatus = nmhTestv2FsQoSVlanMapStatus (&u4ErrorCode, u4Idx, ACTIVE);
    if (i4RetStatus == SNMP_FAILURE)
    {
        QoSCliRSVlanMapTblEntry (u4Idx, u4CurIfNum, u4CurVlanId, i4RowStatus);
        return (CLI_FAILURE);
    }
    i4RetStatus = nmhSetFsQoSVlanMapStatus (u4Idx, ACTIVE);
    if (i4RetStatus == SNMP_FAILURE)
    {
        QoSCliRSVlanMapTblEntry (u4Idx, u4CurIfNum, u4CurVlanId, i4RowStatus);
        ISSCliCheckAndThrowFatalError (CliHandle);
        return (CLI_FAILURE);
    }
    return (CLI_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSCliRSNoSetPriMapParams                            */
/* Description        : This function is used to Reset default value for     */
/*                      an Entry in the Priority Map Table                   */
/* Input(s)           : CliHandle    - Handle to CLI session                 */
/*                    : u4Interface  - Interface                             */
/*                    : u4Vlan       - Vlan Id                               */
/*                    : u4InnerReGenPri - Inner Regene Priority Value        */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : CLI_SUCCESS/CLI_FAILURE                              */
/* Called By          : cli_process_qos_cmd                                  */
/* Calling Function   : SNMP nmh Routines                                    */
/*****************************************************************************/

VOID
QoSCliRSNoSetPriMapParams (UINT4 u4Idx, UINT4 u4CurIfNum, UINT4 u4CurVlanId,
                           UINT4 u4CurInRegenPri, INT4 i4RowStatus)
{
    INT4                i4RetStatus = SNMP_FAILURE;

    if (u4CurIfNum != (UINT4) CLI_RS_INVALID_MASK)
    {
        i4RetStatus = nmhSetFsQoSPriorityMapIfIndex (u4Idx, u4CurIfNum);
    }
    if (u4CurVlanId != (UINT4) CLI_RS_INVALID_MASK)
    {
        i4RetStatus = nmhSetFsQoSPriorityMapVlanId (u4Idx, u4CurVlanId);
    }
    if (u4CurInRegenPri != (UINT4) CLI_RS_INVALID_MASK)
    {
        i4RetStatus =
            nmhSetFsQoSPriorityMapRegenInnerPriority (u4Idx, u4CurInRegenPri);
    }

    i4RetStatus = nmhSetFsQoSPriorityMapStatus (u4Idx, i4RowStatus);

    UNUSED_PARAM (i4RetStatus);

}

/*****************************************************************************/
/* Function Name      : QoSCliNoSetPriMapParams                              */
/* Description        : This function is used to Set default value for       */
/*                      an Entry in the Priority Map Table                   */
/* Input(s)           : CliHandle    - Handle to CLI session                 */
/*                    : u4Interface  - Interface                             */
/*                    : u4Vlan       - Vlan Id                               */
/*                    : u4InnerReGenPri - Inner Regene Priority Value        */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : CLI_SUCCESS/CLI_FAILURE                              */
/* Called By          : cli_process_qos_cmd                                  */
/* Calling Function   : SNMP nmh Routines                                    */
/*****************************************************************************/

INT4
QoSCliNoSetPriMapParams (tCliHandle CliHandle, UINT4 u4Interface, UINT4 u4Vlan,
                         UINT4 u4InnerReGenPri)
{
    UINT4               u4Idx = 0;
    INT4                i4RetStatus = SNMP_FAILURE;
    INT4                i4RowStatus = SNMP_FAILURE;
    UINT4               u4ErrorCode = SNMP_ERR_NO_ERROR;
    UINT4               u4CurIfNum = (UINT4) CLI_RS_INVALID_MASK;
    UINT4               u4CurVlanId = (UINT4) CLI_RS_INVALID_MASK;
    UINT4               u4CurInRegenPri = (UINT4) CLI_RS_INVALID_MASK;

    /* Get the Priority Map Id */
    u4Idx = CLI_GET_PRI_MAP_ID ();

    /* Get the Status of this Entry */
    i4RetStatus = nmhGetFsQoSPriorityMapStatus (u4Idx, &i4RowStatus);
    if (i4RetStatus == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }
    /* if RowStatus = NOT_IN_SERVICE or NOT_READY test and set the Values. 
     * if RowStatus = ACTIVE Set the RowStatus as NOT_IN_SERVICE and than   
     * test and set the Values 
     */
    if (i4RowStatus == ACTIVE)
    {
        i4RetStatus = nmhTestv2FsQoSPriorityMapStatus (&u4ErrorCode,
                                                       u4Idx, NOT_IN_SERVICE);
        if (i4RetStatus == SNMP_FAILURE)
        {
            return (CLI_FAILURE);
        }

        i4RetStatus = nmhSetFsQoSPriorityMapStatus (u4Idx, NOT_IN_SERVICE);
        if (i4RetStatus == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }
    }

    /* Test & Set the each value */
    if (u4Interface == 1)
    {
        i4RetStatus = nmhTestv2FsQoSPriorityMapIfIndex (&u4ErrorCode, u4Idx, 0);
        if (i4RetStatus == SNMP_FAILURE)
        {
            QoSCliRSNoSetPriMapParams (u4Idx, u4CurIfNum, u4CurVlanId,
                                       u4CurInRegenPri, i4RowStatus);
            return (CLI_FAILURE);
        }

        i4RetStatus = nmhGetFsQoSPriorityMapIfIndex (u4Idx, &u4CurIfNum);
        if (i4RetStatus == SNMP_FAILURE)
        {
            return (CLI_FAILURE);
        }

        i4RetStatus = nmhSetFsQoSPriorityMapIfIndex (u4Idx, 0);
        if (i4RetStatus == SNMP_FAILURE)
        {
            QoSCliRSNoSetPriMapParams (u4Idx, u4CurIfNum, u4CurVlanId,
                                       u4CurInRegenPri, i4RowStatus);
            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }
    }

    if (u4Vlan == 1)
    {
        i4RetStatus = nmhTestv2FsQoSPriorityMapVlanId (&u4ErrorCode, u4Idx, 0);
        if (i4RetStatus == SNMP_FAILURE)
        {
            QoSCliRSNoSetPriMapParams (u4Idx, u4CurIfNum, u4CurVlanId,
                                       u4CurInRegenPri, i4RowStatus);
            return (CLI_FAILURE);
        }

        i4RetStatus = nmhGetFsQoSPriorityMapVlanId (u4Idx, &u4CurVlanId);
        if (i4RetStatus == SNMP_FAILURE)
        {
            return (CLI_FAILURE);
        }

        i4RetStatus = nmhSetFsQoSPriorityMapVlanId (u4Idx, 0);
        if (i4RetStatus == SNMP_FAILURE)
        {
            QoSCliRSNoSetPriMapParams (u4Idx, u4CurIfNum, u4CurVlanId,
                                       u4CurInRegenPri, i4RowStatus);
            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }
    }

    if (u4InnerReGenPri == 1)
    {
        i4RetStatus =
            nmhTestv2FsQoSPriorityMapRegenInnerPriority (&u4ErrorCode, u4Idx,
                                                         QOS_PRI_MAP_TBL_INVLD_INREGPRI);
        if (i4RetStatus == SNMP_FAILURE)
        {
            QoSCliRSNoSetPriMapParams (u4Idx, u4CurIfNum, u4CurVlanId,
                                       u4CurInRegenPri, i4RowStatus);
            return (CLI_FAILURE);
        }

        i4RetStatus =
            nmhGetFsQoSPriorityMapRegenInnerPriority (u4Idx, &u4CurInRegenPri);
        if (i4RetStatus == SNMP_FAILURE)
        {
            return (CLI_FAILURE);
        }

        i4RetStatus =
            nmhSetFsQoSPriorityMapRegenInnerPriority (u4Idx,
                                                      QOS_PRI_MAP_TBL_INVLD_INREGPRI);
        if (i4RetStatus == SNMP_FAILURE)
        {
            QoSCliRSNoSetPriMapParams (u4Idx, u4CurIfNum, u4CurVlanId,
                                       u4CurInRegenPri, i4RowStatus);
            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }
    }
    /*Do not make the Priority Map Status as ACTIVE for modification
     *made in Params table, since the entry is not ready .
     */
    return (CLI_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSCliUtlDisplayPriMapEntry                          */
/* Description        : This function is used to Display the Priority Map    */
/*                      Table Entries Parameters                             */
/* Input(s)           : CliHandle    - Handle to CLI session                 */
/*                    : u4Idx        - Table Entries Index                   */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : CLI_SUCCESS/CLI_FAILURE                              */
/* Called By          : QoSCliShowPriMapEntry                                */
/* Calling Function   : SNMP nmh Routines                                    */
/*****************************************************************************/
INT4
QoSCliUtlDisplayPriMapEntry (tCliHandle CliHandle, UINT4 u4Idx)
{
    INT4                i4RetStatus = SNMP_FAILURE;
    UINT4               u4IfNum = 0;
    UINT4               u4VlanId = 0;
    INT4                i4InPri = 0;
    INT4                i4InPriType = 0;
    UINT4               u4TrafCls = 0;
    UINT4               u4InnerReGenPri = 0;
    UINT1               au1IfName[CFA_MAX_PORT_NAME_LENGTH];
    INT4                i4CurInDEI = 0;

    /* Get All Fields */
    i4RetStatus = nmhGetFsQoSPriorityMapIfIndex (u4Idx, &u4IfNum);
    if (i4RetStatus == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }
    MEMSET (au1IfName, 0, CFA_MAX_PORT_NAME_LENGTH);
    if (CfaCliGetIfName (u4IfNum, (INT1 *) au1IfName) == CLI_FAILURE)
    {
        return (CLI_FAILURE);
    }

    i4RetStatus = nmhGetFsQoSPriorityMapVlanId (u4Idx, &u4VlanId);
    if (i4RetStatus == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    i4RetStatus = nmhGetFsQoSPriorityMapInPriType (u4Idx, &i4InPriType);
    if (i4RetStatus == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    i4RetStatus = nmhGetFsQoSPriorityMapInDEI (u4Idx, &i4CurInDEI);
    if (i4RetStatus == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    i4RetStatus = nmhGetFsQoSPriorityMapInPriority (u4Idx, &i4InPri);
    if (i4RetStatus == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    i4RetStatus = nmhGetFsQoSPriorityMapRegenPriority (u4Idx, &u4TrafCls);
    if (i4RetStatus == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    i4RetStatus =
        nmhGetFsQoSPriorityMapRegenInnerPriority (u4Idx, &u4InnerReGenPri);

    if (i4RetStatus == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }
    /* Print */
    CliPrintf (CliHandle, "%-28s : %d\r\n", "PriorityMapId", u4Idx);
    CliPrintf (CliHandle, "%-28s : %s\r\n", "IfIndex", au1IfName);
    CliPrintf (CliHandle, "%-28s : %d\r\n", "VlanId", u4VlanId);

    switch (i4InPriType)
    {
        case QOS_CLI_IN_PRI_TYPE_VLAN_PRI:
            CliPrintf (CliHandle, "%-28s : %s\r\n", "InPriorityType",
                       "VlanPriority");
            break;

        case QOS_CLI_IN_PRI_TYPE_IP_TOS:
            CliPrintf (CliHandle, "%-28s : %s\r\n", "InPriorityType", "IP TOS");
            break;

        case QOS_CLI_IN_PRI_TYPE_IP_DSCP:
            CliPrintf (CliHandle, "%-28s : %s\r\n", "InPriorityType",
                       "IP DSCP");
            break;

        case QOS_CLI_IN_PRI_TYPE_MPLS_EXP:
            CliPrintf (CliHandle, "%-28s : %s\r\n", "InPriorityType",
                       "MPLS Exp");
            break;

        case QOS_CLI_IN_PRI_TYPE_VLAN_DEI:
            CliPrintf (CliHandle, "%-28s : %s\r\n", "InPriorityType",
                       "Vlan DEI");
            break;

        case QOS_CLI_IN_PRI_TYPE_DOT1P:
            CliPrintf (CliHandle, "%-28s : %s\r\n", "InPriorityType", "Dot1P");
            break;

        default:
            CliPrintf (CliHandle, "%-28s : %s --- %d \r\n", "InPriorityType",
                       "Invalid", i4InPriType);
            break;
    }

    CliPrintf (CliHandle, "%-28s : %d\r\n", "InPriority", i4InPri);
    CliPrintf (CliHandle, "%-28s : %d\r\n", "RegenPriority", u4TrafCls);
    if (i4InPriType == QOS_CLI_IN_PRI_TYPE_DOT1P)
    {
        CliPrintf (CliHandle, "%-28s : %d\r\n", "DEI Value", i4CurInDEI);
    }

    if (u4InnerReGenPri == QOS_PRI_MAP_TBL_INVLD_INREGPRI)
    {
        CliPrintf (CliHandle, "%-28s : %s\r\n", "InnerRegenPriority", "None");
    }
    else
    {
        CliPrintf (CliHandle, "%-28s : %d\r\n", "InnerRegenPriority",
                   u4InnerReGenPri);
    }
    CliPrintf (CliHandle, "\r\n");

    return (CLI_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSCliShowPriMapEntry                                */
/* Description        : This function is used to Display the Priority Map    */
/*                      Table Entries Parameters for the name pu1Name, if    */
/*                      it is NULL then All Entries will be displayed        */
/* Input(s)           : CliHandle    - Handle to CLI session                 */
/*                    : pu1Name      - Table Entry Name to Display           */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : CLI_SUCCESS/CLI_FAILURE                              */
/* Called By          : cli_process_qos_cmd                                  */
/* Calling Function   : QoSCliUtlDisplayPriMapEntry                          */
/*****************************************************************************/
INT4
QoSCliShowPriMapEntry (tCliHandle CliHandle, UINT4 u4Idx)
{
    INT4                i4RetStatus = SNMP_FAILURE;
    INT4                i4RowStatus = SNMP_FAILURE;

    /* Display All */
    CliPrintf (CliHandle, "QoS Priority Map Entries\r\n");
    CliPrintf (CliHandle, "------------------------\r\n");

    if (u4Idx == 0)
    {
        i4RetStatus = nmhGetFirstIndexFsQoSPriorityMapTable (&u4Idx);
        if (i4RetStatus == SNMP_FAILURE)
        {
            return (CLI_FAILURE);
        }

        do
        {
            /* Check the Status */
            i4RetStatus = nmhGetFsQoSPriorityMapStatus (u4Idx, &i4RowStatus);
            if (i4RetStatus == SNMP_FAILURE)
            {
                return (CLI_FAILURE);
            }

            if (i4RowStatus == ACTIVE)
            {
                /* Display Entry */
                i4RetStatus = QoSCliUtlDisplayPriMapEntry (CliHandle, u4Idx);
                if (i4RetStatus == CLI_FAILURE)
                {
                    return (CLI_FAILURE);
                }
            }

            i4RetStatus = nmhGetNextIndexFsQoSPriorityMapTable (u4Idx, &u4Idx);

        }
        while (i4RetStatus == SNMP_SUCCESS);

        return (CLI_SUCCESS);
    }
    else
    {
        i4RetStatus = nmhGetFsQoSPriorityMapStatus (u4Idx, &i4RowStatus);
        if (i4RetStatus == SNMP_FAILURE)
        {
            return (CLI_FAILURE);
        }

        if (i4RowStatus == ACTIVE)
        {
            /* Display Entry */
            i4RetStatus = QoSCliUtlDisplayPriMapEntry (CliHandle, u4Idx);
            if (i4RetStatus == CLI_FAILURE)
            {
                return (CLI_FAILURE);
            }
        }
    }

    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*                     VLAN QUEUEING MAP TABLE FUNCTIONS                     */
/*****************************************************************************/

/*****************************************************************************/
/* Function Name      : QoSVlanQMapPrompt                                    */
/* Description        : This function is used to set CLI Prompt string for   */
/*                      Vlan map Table                                       */
/* Input(s)           : pi1ModeName   - Prompt Name from CLI                 */
/*                    : pi1PromptStr  - Prompt String to Display             */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : CLI_SUCCESS/CLI_FAILURE                              */
/* Called By          : CLI Task                                             */
/* Calling Function   : None.                                                */
/*****************************************************************************/
PUBLIC INT1
QoSVlanQMapPrompt (INT1 *pi1ModeName, INT1 *pi1PromptStr)
{
    UINT4               u4Idx;
    UINT4               u4Len = STRLEN (QOS_CLI_VLAN_MAP_MODE);

    if ((!pi1PromptStr) || (!pi1ModeName))
    {
        return FALSE;
    }

    if (STRNCMP (pi1ModeName, QOS_CLI_VLAN_MAP_MODE, u4Len) != 0)
    {
        return FALSE;
    }

    pi1ModeName = pi1ModeName + u4Len;
    u4Idx = (UINT4) CLI_ATOI (pi1ModeName);
    CLI_SET_PRI_MAP_ID ((INT4) u4Idx);
    STRNCPY (pi1PromptStr, "(config-vlan-map)#", STRLEN ("(config-vlan-map)#"));
    pi1PromptStr[STRLEN ("(config-vlan-map)#")] = '\0';
    return TRUE;
}

/*****************************************************************************/
/* Function Name      : QoSCliShowVlanQMapEntry                              */
/* Description        : This function is used to Display the Vlan Map        */
/*                      Table Entries Parameters for the name pu1Name, if    */
/*                      it is NULL then All Entries will be displayed        */
/* Input(s)           : CliHandle    - Handle to CLI session                 */
/*                    : pu1Name      - Table Entry Name to Display           */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : CLI_SUCCESS/CLI_FAILURE                              */
/* Called By          : cli_process_qos_cmd                                  */
/* Calling Function   : QoSCliUtlDisplayPriMapEntry                          */
/*****************************************************************************/
INT4
QoSCliShowVlanQMapEntry (tCliHandle CliHandle, UINT4 u4Idx)
{
    INT4                i4RetStatus = SNMP_FAILURE;
    INT4                i4RowStatus = SNMP_FAILURE;

    /* QoS module is shutdown */
    if (gQoSGlobalInfo.eSysControl != QOS_SYS_CNTL_START)
    {
        QOS_TRC (MGMT_TRC, QOS_MODULE_SHUTDOW_ERROR_MSG);
        CLI_SET_ERR (QOS_CLI_ERR_MODULE_SHUTDOW);
        return (CLI_FAILURE);
    }

    /* Vlan Queueing not enabled */
    if (gQoSGlobalInfo.eVlanQStatus != VLAN_QUEUEING_SYS_STATUS_ENABLE)
    {
        CLI_SET_ERR (QOS_CLI_ERR_VLAN_MAP_DISABLED);
        return (CLI_FAILURE);
    }

    /* Display All */
    CliPrintf (CliHandle, "QoS Vlan Map Entries\r\n");
    CliPrintf (CliHandle, "------------------------\r\n");

    if (u4Idx == 0)
    {
        i4RetStatus = nmhGetFirstIndexFsQoSVlanMapTable (&u4Idx);
        if (i4RetStatus == SNMP_FAILURE)
        {
            return (CLI_FAILURE);
        }

        do
        {
            /* Check the Status */
            i4RetStatus = nmhGetFsQoSVlanMapStatus (u4Idx, &i4RowStatus);
            if (i4RetStatus == SNMP_FAILURE)
            {
                return (CLI_FAILURE);
            }

            if (i4RowStatus == ACTIVE)
            {
                /* Display Entry */
                i4RetStatus = QoSCliUtlDisplayVlanQMapEntry (CliHandle, u4Idx);
                if (i4RetStatus == CLI_FAILURE)
                {
                    return (CLI_FAILURE);
                }
            }
            i4RetStatus = nmhGetNextIndexFsQoSVlanMapTable (u4Idx, &u4Idx);
        }
        while (i4RetStatus == SNMP_SUCCESS);
        return (CLI_SUCCESS);
    }
    else
    {
        i4RetStatus = nmhGetFsQoSVlanMapStatus (u4Idx, &i4RowStatus);
        if (i4RetStatus == SNMP_FAILURE)
        {
            return (CLI_FAILURE);
        }

        if (i4RowStatus == ACTIVE)
        {
            /* Display Entry */
            i4RetStatus = QoSCliUtlDisplayVlanQMapEntry (CliHandle, u4Idx);
            if (i4RetStatus == CLI_FAILURE)
            {
                return (CLI_FAILURE);
            }
        }
    }
    return (CLI_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSCliUtlDisplayVlanQMapEntry                        */
/* Description        : This function is used to Display the Vlan Map        */
/*                      Table Entries Parameters                             */
/* Input(s)           : CliHandle    - Handle to CLI session                 */
/*                    : u4Idx        - Table Entries Index                   */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : CLI_SUCCESS/CLI_FAILURE                              */
/* Called By          : QoSCliShowPriMapEntry                                */
/* Calling Function   : SNMP nmh Routines                                    */
/*****************************************************************************/
INT4
QoSCliUtlDisplayVlanQMapEntry (tCliHandle CliHandle, UINT4 u4Idx)
{
    INT4                i4RetStatus = SNMP_FAILURE;
    UINT4               u4IfNum = 0;
    UINT4               u4VlanId = 0;
    UINT1               au1IfName[CFA_MAX_PORT_NAME_LENGTH];

    /* Get All Fields */
    i4RetStatus = nmhGetFsQoSVlanMapIfIndex (u4Idx, &u4IfNum);
    if (i4RetStatus == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }
    MEMSET (au1IfName, 0, CFA_MAX_PORT_NAME_LENGTH);
    if (CfaCliGetIfName (u4IfNum, (INT1 *) au1IfName) == CLI_FAILURE)
    {
        return (CLI_FAILURE);
    }

    i4RetStatus = nmhGetFsQoSVlanMapVlanId (u4Idx, &u4VlanId);
    if (i4RetStatus == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    /* Print */
    CliPrintf (CliHandle, "%-28s : %d\r\n", "VlanMapId", u4Idx);
    CliPrintf (CliHandle, "%-28s : %s\r\n", "IfIndex", au1IfName);
    CliPrintf (CliHandle, "%-28s : %d\r\n", "VlanId", u4VlanId);
    CliPrintf (CliHandle, "\r\n");
    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*                     CLASS MAP TABLE FUNCTIONS                             */
/*****************************************************************************/

/*****************************************************************************/
/* Function Name      : QoSClsMapPrompt                                      */
/* Description        : This function is used to set CLI Prompt string for   */
/*                      Class map Table                                      */
/* Input(s)           : pi1ModeName   - Prompt Name from CLI                 */
/*                    : pi1PromptStr  - Prompt String to Display             */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : CLI_SUCCESS/CLI_FAILURE                              */
/* Called By          : CLI Task                                             */
/* Calling Function   : None.                                                */
/*****************************************************************************/
PUBLIC INT1
QoSClsMapPrompt (INT1 *pi1ModeName, INT1 *pi1PromptStr)
{
    UINT4               u4Idx;
    UINT4               u4Len = STRLEN (QOS_CLI_CLS_MAP_MODE);

    if ((!pi1PromptStr) || (!pi1ModeName))
    {
        return FALSE;
    }

    if (STRNCMP (pi1ModeName, QOS_CLI_CLS_MAP_MODE, u4Len) != 0)
    {
        return FALSE;
    }

    pi1ModeName = pi1ModeName + u4Len;
    u4Idx = CLI_ATOI (pi1ModeName);
    CLI_SET_CLS_MAP_ID (u4Idx);
    STRNCPY (pi1PromptStr, "(config-cls-map)#", STRLEN ("(config-cls-map)#"));
    pi1PromptStr[STRLEN ("(config-cls-map)#")] = '\0';

    return TRUE;
}

/*****************************************************************************/
/* Function Name      : QoSCliGetClsMapIndexFromName                         */
/* Description        : This function is used to get the Table Index from    */
/*                      the Given Table Name. If Name is not found it give   */
/*                      the next free Index                                  */
/* Input(s)           : pInOctStrName - Table Name.                          */
/*                    : pu4Idx        - Ptr to the Index of the Table for    */
/*                    :                 the given Name                       */
/*                    : pu4Status     - Status says that the  Entry needs to */
/*                    :                 be create or modify                  */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : CLI_SUCCESS/CLI_FAILURE                              */
/* Called By          : ClassMap Tbl Function                                */
/* Calling Function   : SNMP nmh Routines                                    */
/*****************************************************************************/
INT4
QoSCliGetClsMapIndexFromName (tSNMP_OCTET_STRING_TYPE * pInOctStrName,
                              UINT4 *pu4Idx, UINT4 *pu4Status)
{
    tSNMP_OCTET_STRING_TYPE *pOctStrName = NULL;
    UINT4               u4CurrIdx = 0;
    UINT4               u4PreIdx = 0;
    UINT4               u4IdxFound = CLI_FAILURE;
    INT4                i4SysControl = 0;
    INT4                i4RetStatus = SNMP_FAILURE;

    /* System Contol map be QOS_SYS_CNTL_SHUTDOWN */
    i4RetStatus = nmhGetFsQoSSystemControl (&i4SysControl);
    if ((i4RetStatus == SNMP_FAILURE)
        || (i4SysControl == QOS_SYS_CNTL_SHUTDOWN))
    {
        CLI_SET_ERR (QOS_CLI_ERR_MODULE_SHUTDOW);
        return (CLI_FAILURE);
    }

    /* No Entry Found Create first Entry  idx = 1 */
    i4RetStatus = nmhGetFirstIndexFsQoSClassMapTable (&u4CurrIdx);
    if (i4RetStatus == SNMP_FAILURE)
    {
        *pu4Idx = 1;
        *pu4Status = QOS_CLI_NEW_ENTRY;
        return (CLI_SUCCESS);
    }

    /* Allocate Memory For Name */
    pOctStrName = allocmem_octetstring (QOS_MAX_TABLE_NAME);
    if (pOctStrName == NULL)
    {
        return (CLI_FAILURE);
    }
    MEMSET (pOctStrName->pu1_OctetList, 0, pOctStrName->i4_Length);

    do
    {
        /* Get the Name of the Current Entry */
        i4RetStatus = nmhGetFsQoSClassMapName (u4CurrIdx, pOctStrName);
        if (i4RetStatus == SNMP_FAILURE)
        {
            free_octetstring (pOctStrName);
            return (CLI_FAILURE);
        }

        if (STRCMP (pOctStrName->pu1_OctetList, pInOctStrName->pu1_OctetList)
            == 0)
        {
            *pu4Idx = u4CurrIdx;
            *pu4Status = QOS_CLI_MODIFY_ENTRY;

            free_octetstring (pOctStrName);
            return (CLI_SUCCESS);
        }

        if (((u4CurrIdx - u4PreIdx) > 1) && (u4IdxFound == CLI_FAILURE))
        {
            *pu4Idx = u4PreIdx + 1;
            *pu4Status = QOS_CLI_NEW_ENTRY;
            u4IdxFound = CLI_SUCCESS;
        }

        u4PreIdx = u4CurrIdx;

        i4RetStatus = nmhGetNextIndexFsQoSClassMapTable (u4CurrIdx, &u4CurrIdx);

    }
    while (i4RetStatus == SNMP_SUCCESS);

    if ((u4CurrIdx == u4PreIdx) && (u4IdxFound == CLI_FAILURE))
    {
        *pu4Idx = u4CurrIdx + 1;
        *pu4Status = QOS_CLI_NEW_ENTRY;
    }

    free_octetstring (pOctStrName);
    return (CLI_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSCliAddClsMapEntry                                 */
/* Description        : This function is used to create Class Map table      */
/*                      Entry and change the CLI Prompt.                     */
/* Input(s)           : CliHandle    - Handle to CLI session                 */
/*                    : pu1Name      - Table Entry Name to Create.           */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : CLI_SUCCESS/CLI_FAILURE                              */
/* Called By          : cli_process_qos_cmd                                  */
/* Calling Function   : SNMP nmh Routines                                    */
/*****************************************************************************/
INT4
QoSCliAddClsMapEntry (tCliHandle CliHandle, UINT4 u4Idx)
{
    UINT1               au1ClsMap[QOS_CLI_MAX_PROMPT_LENGTH];
    INT4                i4RetStatus = SNMP_FAILURE;
    INT4                i4RowStatus = SNMP_FAILURE;
    UINT4               u4ErrorCode = SNMP_ERR_NO_ERROR;

    UNUSED_PARAM (CliHandle);

    i4RetStatus = nmhGetFsQoSClassMapStatus (u4Idx, &i4RowStatus);
    if (i4RetStatus == SNMP_FAILURE)
    {
        /* Create new Entry */
        i4RetStatus = nmhTestv2FsQoSClassMapStatus (&u4ErrorCode,
                                                    u4Idx, CREATE_AND_WAIT);
        if (i4RetStatus == SNMP_FAILURE)
        {
            return (CLI_FAILURE);
        }

        i4RetStatus = nmhSetFsQoSClassMapStatus (u4Idx, CREATE_AND_WAIT);
        if (i4RetStatus == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }
    }

    /* Go to the Class Map Mode of Cli Prompt */
    SPRINTF ((CHR1 *) au1ClsMap, "%s%u", QOS_CLI_CLS_MAP_MODE, u4Idx);

    if (CliChangePath ((CHR1 *) au1ClsMap) == CLI_FAILURE)
    {
        CliPrintf (CliHandle, "\r%% Unable to enter Class Map mode.\r\n");
        return (CLI_FAILURE);
    }

    return (CLI_SUCCESS);

}

/*****************************************************************************/
/* Function Name      : QoSCliSetClsMapParams                                */
/* Description        : This function is used to Set Class Map table Entry   */
/*                      Parameter                                            */
/* Input(s)           : CliHandle    - Handle to CLI session                 */
/*                    : i4L2FltrId   - L2 Filterf Entry Id                   */
/*                    : i4L3FltrId   - L3 Filterf Entry Id                   */
/*                    : i4PriMapId   - Priority Map Entry Id                 */
/*                    : i4VlanMapId  - Vlan Map Entry Id                     */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : CLI_SUCCESS/CLI_FAILURE                              */
/* Called By          : cli_process_qos_cmd                                  */
/* Calling Function   : SNMP nmh Routines                                    */
/*****************************************************************************/
INT4
QoSCliSetClsMapParams (tCliHandle CliHandle, INT4 i4L2FltrId, INT4 i4L3FltrId,
                       INT4 i4PriMapId, INT4 i4VlanMapId)
{
    UINT4               u4Idx = 0;
    INT4                i4RetStatus = SNMP_FAILURE;
    INT4                i4RowStatus = SNMP_FAILURE;
    UINT4               u4ErrorCode = SNMP_ERR_NO_ERROR;

    /* Get the Class Map Id */
    u4Idx = (UINT4) CLI_GET_CLS_MAP_ID ();

    /* Get the Status of this Entry */
    i4RetStatus = nmhGetFsQoSClassMapStatus (u4Idx, &i4RowStatus);
    if (i4RetStatus == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }
    /* if RowStatus = NOT_IN_SERVICE or NOT_READY test and set the Values. 
     * if RowStatus = ACTIVE Set the RowStatus as NOT_IN_SERVICE and than   
     * test and set the Values 
     */
    if (i4RowStatus == ACTIVE)
    {
        i4RetStatus = nmhTestv2FsQoSClassMapStatus (&u4ErrorCode,
                                                    u4Idx, NOT_IN_SERVICE);
        if (i4RetStatus == SNMP_FAILURE)
        {
            return (CLI_FAILURE);
        }

        i4RetStatus = nmhSetFsQoSClassMapStatus (u4Idx, NOT_IN_SERVICE);
        if (i4RetStatus == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }
    }

    /* Test & Set the each value */

    if (i4L2FltrId != -1)
    {
        i4RetStatus = nmhTestv2FsQoSClassMapL2FilterId (&u4ErrorCode, u4Idx,
                                                        (UINT4) i4L2FltrId);
        if (i4RetStatus == SNMP_FAILURE)
        {
            if (i4RowStatus == ACTIVE)
            {
                i4RetStatus = nmhSetFsQoSClassMapStatus (u4Idx, ACTIVE);
                if (i4RetStatus == SNMP_FAILURE)
                {
                    CLI_FATAL_ERROR (CliHandle);
                    return (CLI_FAILURE);
                }
            }
            return (CLI_FAILURE);
        }

        i4RetStatus = nmhSetFsQoSClassMapL2FilterId (u4Idx, (UINT4) i4L2FltrId);
        if (i4RetStatus == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }
    }
    else
    {
        /* Resetting i4L2FltrId to default. */
        i4L2FltrId = QOS_ROWPOINTER_DEF;
        i4RetStatus = nmhSetFsQoSClassMapL2FilterId (u4Idx, (UINT4) i4L2FltrId);
        if (i4RetStatus == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }
    }

    if (i4L3FltrId != -1)
    {
        i4RetStatus = nmhTestv2FsQoSClassMapL3FilterId (&u4ErrorCode, u4Idx,
                                                        (UINT4) i4L3FltrId);
        if (i4RetStatus == SNMP_FAILURE)
        {
            if (i4RowStatus == ACTIVE)
            {
                i4RetStatus = nmhSetFsQoSClassMapStatus (u4Idx, ACTIVE);
                if (i4RetStatus == SNMP_FAILURE)
                {
                    CLI_FATAL_ERROR (CliHandle);
                    return (CLI_FAILURE);
                }
            }
            return (CLI_FAILURE);
        }

        i4RetStatus = nmhSetFsQoSClassMapL3FilterId (u4Idx, (UINT4) i4L3FltrId);
        if (i4RetStatus == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }
    }
    else
    {
        /* Resetting i4L3FltrId to default. */
        i4L3FltrId = QOS_ROWPOINTER_DEF;
        i4RetStatus = nmhSetFsQoSClassMapL3FilterId (u4Idx, (UINT4) i4L3FltrId);
        if (i4RetStatus == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }

    }

    if (i4PriMapId != -1)
    {
        i4RetStatus = nmhTestv2FsQoSClassMapPriorityMapId (&u4ErrorCode, u4Idx,
                                                           (UINT4) i4PriMapId);
        if (i4RetStatus == SNMP_FAILURE)
        {
            nmhSetFsQoSClassMapStatus (u4Idx, i4RowStatus);
            return (CLI_FAILURE);
        }

        i4RetStatus = nmhSetFsQoSClassMapPriorityMapId (u4Idx,
                                                        (UINT4) i4PriMapId);
        if (i4RetStatus == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }
    }
    else
    {
        /* Resetting i4PriMapId to default. */
        i4PriMapId = QOS_ROWPOINTER_DEF;
        i4RetStatus = nmhSetFsQoSClassMapPriorityMapId (u4Idx,
                                                        (UINT4) i4PriMapId);
        if (i4RetStatus == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }
    }

    if (i4VlanMapId != -1)
    {
        i4RetStatus = nmhTestv2FsQoSClassMapVlanMapId (&u4ErrorCode, u4Idx,
                                                       (UINT4) i4VlanMapId);
        if (i4RetStatus == SNMP_FAILURE)
        {
            nmhSetFsQoSClassMapStatus (u4Idx, i4RowStatus);
            return (CLI_FAILURE);
        }

        i4RetStatus = nmhSetFsQoSClassMapVlanMapId (u4Idx, (UINT4) i4VlanMapId);
        if (i4RetStatus == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }
    }
    else
    {
        /* Resetting i4VlanMapId to default. */
        i4VlanMapId = QOS_ROWPOINTER_DEF;
        i4RetStatus = nmhSetFsQoSClassMapVlanMapId (u4Idx, (UINT4) i4VlanMapId);
        if (i4RetStatus == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }
    }
    /* Get the Status of this Entry  if it is NOT_IN_SERVICE  all Mandatory
     * values are set, so made it ACTIVE */
    i4RetStatus = nmhGetFsQoSClassMapStatus (u4Idx, &i4RowStatus);
    if (i4RetStatus == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }
    if (i4RowStatus == NOT_IN_SERVICE)
    {
        i4RetStatus = nmhTestv2FsQoSClassMapStatus (&u4ErrorCode, u4Idx,
                                                    ACTIVE);
        if (i4RetStatus == SNMP_FAILURE)
        {
            return (CLI_FAILURE);
        }

        i4RetStatus = nmhSetFsQoSClassMapStatus (u4Idx, ACTIVE);
        if (i4RetStatus == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }
    }

    return (CLI_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSCliSetClsMapClass                                 */
/* Description        : This function is used to Set Class Map table Entry   */
/*                      Parameter and Class to Priority Table Entry.         */
/* Input(s)           : CliHandle    - Handle to CLI session                 */
/*                    : u4Class      - CLASS Id                              */
/*                    : i4PreColor   - Pre Color for the Entry               */
/*                    : i4ReGenPri   - Regen Priority Value                  */
/*                    : pu1Name      - Group Name.                           */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : CLI_SUCCESS/CLI_FAILURE                              */
/* Called By          : cli_process_qos_cmd                                  */
/* Calling Function   : SNMP nmh Routines                                    */
/*****************************************************************************/
INT4
QoSCliSetClsMapClass (tCliHandle CliHandle, UINT4 u4Class, INT4 i4PreColor,
                      INT4 i4ReGenPri, UINT1 *pu1Name)
{
    tSNMP_OCTET_STRING_TYPE *pOctStrGroupName = NULL;
    UINT4               u4Idx = 0;
    INT4                i4RetStatus = SNMP_FAILURE;
    INT4                i4RowStatus = SNMP_FAILURE;
    INT4                i4BkpClassStatus = SNMP_FAILURE;
    UINT4               u4ErrorCode = SNMP_ERR_NO_ERROR;
    tQoSClassMapNode   *pClsMapNode;
    /* Get the Class Map Id */
    u4Idx = CLI_GET_CLS_MAP_ID ();

    /* Get the Status of this Entry */
    i4RetStatus = nmhGetFsQoSClassMapStatus (u4Idx, &i4RowStatus);
    if (i4RetStatus == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    /*If row status = NOT_READY & ACTIVE  and if the CLASS has a policy map mapped to it
       then unmap the policy map */
    if (i4RowStatus == ACTIVE || i4RowStatus == NOT_READY)
    {
        i4RetStatus = nmhSetFsQoSClassMapStatus (u4Idx, NOT_READY);
        if (i4RetStatus == SNMP_FAILURE)
        {
            pClsMapNode = QoSUtlGetClassMapNode (u4Idx);
            /*check whether the new CLASS is not the old one */
            if (pClsMapNode != NULL)
            {
                if (pClsMapNode->u4ClassId != u4Class)
                {
                    CLI_FATAL_ERROR (CliHandle);
                    return (CLI_FAILURE);
                }
                else
                {
                    /*If it is already created class just return failure */
                    return (CLI_FAILURE);
                }
            }
        }
    }
    /* if RowStatus = NOT_IN_SERVICE or NOT_READY test and set the Values. 
     * if RowStatus = ACTIVE Set the RowStatus as NOT_IN_SERVICE and than   
     * test and set the Values 
     */
    if (i4RowStatus == ACTIVE)
    {
        i4RetStatus = nmhTestv2FsQoSClassMapStatus (&u4ErrorCode,
                                                    u4Idx, NOT_IN_SERVICE);
        if (i4RetStatus == SNMP_FAILURE)
        {
            return (CLI_FAILURE);
        }

        i4RetStatus = nmhSetFsQoSClassMapStatus (u4Idx, NOT_IN_SERVICE);
        if (i4RetStatus == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }
    }

    i4BkpClassStatus = i4RowStatus;

    /* Test & Set the each value */
    /* Set CLASS in the Class Map Table */
    i4RetStatus = nmhTestv2FsQoSClassMapCLASS (&u4ErrorCode, u4Idx, u4Class);
    if (i4RetStatus == SNMP_FAILURE)
    {
        i4RetStatus = nmhSetFsQoSClassMapStatus (u4Idx, i4BkpClassStatus);
        return (CLI_FAILURE);
    }

    i4RetStatus = nmhSetFsQoSClassMapCLASS (u4Idx, u4Class);
    if (i4RetStatus == SNMP_FAILURE)
    {
        CLI_SET_ERR (QOS_CLI_ERR_CLS_MAP_SET_CLS_NOT_SUPPORTED);
        i4RetStatus = nmhSetFsQoSClassMapStatus (u4Idx, i4BkpClassStatus);
        return (CLI_FAILURE);
    }

    if (i4PreColor != -1)
    {
        i4RetStatus =
            nmhTestv2FsQoSClassMapPreColor (&u4ErrorCode, u4Idx, i4PreColor);
        if (i4RetStatus == SNMP_FAILURE)
        {
            i4RetStatus = nmhSetFsQoSClassMapStatus (u4Idx, i4BkpClassStatus);
            return (CLI_FAILURE);
        }

        i4RetStatus = nmhSetFsQoSClassMapPreColor (u4Idx, i4PreColor);
        if (i4RetStatus == SNMP_FAILURE)
        {
            CLI_SET_ERR (QOS_CLI_ERR_CLS_MAP_SET_CLS_NOT_SUPPORTED);
            i4RetStatus = nmhSetFsQoSClassMapStatus (u4Idx, i4BkpClassStatus);
            return (CLI_FAILURE);
        }
    }

    /* Create Class To Priority Table Entry */
    if (i4ReGenPri != -1)
    {
        i4RowStatus = SNMP_FAILURE;
        /* Get the Status of this Entry */
        i4RetStatus = nmhGetFsQoSClassToPriorityStatus (u4Class, &i4RowStatus);
        if (i4RetStatus == SNMP_FAILURE)
        {
            /* Create  New Entry */
            /* The CLASS is Index for ClassToPriority Map Table */
            i4RetStatus = nmhTestv2FsQoSClassToPriorityStatus (&u4ErrorCode,
                                                               u4Class,
                                                               CREATE_AND_WAIT);
            if (i4RetStatus == SNMP_FAILURE)
            {
                i4RetStatus =
                    nmhSetFsQoSClassMapStatus (u4Idx, i4BkpClassStatus);
                return (CLI_FAILURE);
            }

            i4RetStatus = nmhSetFsQoSClassToPriorityStatus (u4Class,
                                                            CREATE_AND_WAIT);
            if (i4RetStatus == SNMP_FAILURE)
            {
                CLI_FATAL_ERROR (CliHandle);
                i4RetStatus =
                    nmhSetFsQoSClassMapStatus (u4Idx, i4BkpClassStatus);
                return (CLI_FAILURE);
            }
        }
        /* if RowStatus = NOT_IN_SERVICE or NOT_READY test and set the Values. 
         * if RowStatus = ACTIVE Set the RowStatus as NOT_IN_SERVICE and than   
         * test and set the Values 
         */
        if (i4RowStatus == ACTIVE)
        {
            i4RetStatus =
                nmhTestv2FsQoSClassToPriorityStatus (&u4ErrorCode, u4Class,
                                                     NOT_IN_SERVICE);
            if (i4RetStatus == SNMP_FAILURE)
            {
                i4RetStatus =
                    nmhSetFsQoSClassMapStatus (u4Idx, i4BkpClassStatus);
                return (CLI_FAILURE);
            }

            i4RetStatus = nmhSetFsQoSClassToPriorityStatus (u4Class,
                                                            NOT_IN_SERVICE);
            if (i4RetStatus == SNMP_FAILURE)
            {
                CLI_FATAL_ERROR (CliHandle);
                i4RetStatus =
                    nmhSetFsQoSClassMapStatus (u4Idx, i4BkpClassStatus);
                return (CLI_FAILURE);
            }
        }

        i4RetStatus =
            nmhTestv2FsQoSClassToPriorityRegenPri (&u4ErrorCode, u4Class,
                                                   i4ReGenPri);
        if (i4RetStatus == SNMP_FAILURE)
        {
            i4RetStatus = nmhSetFsQoSClassMapStatus (u4Idx, i4BkpClassStatus);
            return (CLI_FAILURE);
        }

        i4RetStatus = nmhSetFsQoSClassToPriorityRegenPri (u4Class, i4ReGenPri);
        if (i4RetStatus == SNMP_FAILURE)
        {
            CLI_SET_ERR (QOS_CLI_ERR_CLS_MAP_SET_CLS_NOT_SUPPORTED);
            i4RetStatus = nmhSetFsQoSClassMapStatus (u4Idx, i4BkpClassStatus);
            return (CLI_FAILURE);
        }

        /* Allocate Memory For Name */
        pOctStrGroupName = allocmem_octetstring (QOS_MAX_TABLE_NAME);
        if (pOctStrGroupName == NULL)
        {
            i4RetStatus = nmhSetFsQoSClassMapStatus (u4Idx, i4BkpClassStatus);
            return (CLI_FAILURE);
        }

        MEMSET (pOctStrGroupName->pu1_OctetList, 0,
                pOctStrGroupName->i4_Length);
        pOctStrGroupName->i4_Length = STRLEN (pu1Name);
        if (pOctStrGroupName->i4_Length <= QOS_MAX_TABLE_NAME)
        {
            MEMCPY (pOctStrGroupName->pu1_OctetList, pu1Name,
                    pOctStrGroupName->i4_Length);
        }
        else
        {
            free_octetstring (pOctStrGroupName);
            i4RetStatus = nmhSetFsQoSClassMapStatus (u4Idx, i4BkpClassStatus);
            return (CLI_FAILURE);
        }

        i4RetStatus =
            nmhTestv2FsQoSClassToPriorityGroupName (&u4ErrorCode, u4Class,
                                                    pOctStrGroupName);
        if (i4RetStatus == SNMP_FAILURE)
        {
            free_octetstring (pOctStrGroupName);
            i4RetStatus = nmhSetFsQoSClassMapStatus (u4Idx, i4BkpClassStatus);
            return (CLI_FAILURE);
        }

        i4RetStatus =
            nmhSetFsQoSClassToPriorityGroupName (u4Class, pOctStrGroupName);
        if (i4RetStatus == SNMP_FAILURE)
        {
            free_octetstring (pOctStrGroupName);
            CLI_SET_ERR (QOS_CLI_ERR_CLS_MAP_SET_CLS_NOT_SUPPORTED);
            i4RetStatus = nmhSetFsQoSClassMapStatus (u4Idx, i4BkpClassStatus);
            return (CLI_FAILURE);
        }

        /* set Rowstatus ACTIVE */
        i4RetStatus = nmhTestv2FsQoSClassToPriorityStatus (&u4ErrorCode,
                                                           u4Class, ACTIVE);
        if (i4RetStatus == SNMP_FAILURE)
        {
            free_octetstring (pOctStrGroupName);
            i4RetStatus = nmhSetFsQoSClassMapStatus (u4Idx, i4BkpClassStatus);
            return (CLI_FAILURE);
        }

        i4RetStatus = nmhSetFsQoSClassToPriorityStatus (u4Class, ACTIVE);
        if (i4RetStatus == SNMP_FAILURE)
        {
            free_octetstring (pOctStrGroupName);
            CLI_SET_ERR (QOS_CLI_ERR_CLS_MAP_SET_CLS_NOT_SUPPORTED);
            i4RetStatus = nmhSetFsQoSClassMapStatus (u4Idx, i4BkpClassStatus);
            return (CLI_FAILURE);
        }
    }

    /* Get the Status of this Entry  if it is NOT_IN_SERVICE  all Mandatory
     * values are set, so made it ACTIVE */
    i4RetStatus = nmhGetFsQoSClassMapStatus (u4Idx, &i4RowStatus);
    if (i4RetStatus == SNMP_FAILURE)
    {
        free_octetstring (pOctStrGroupName);
        return (CLI_FAILURE);
    }

    if (i4RowStatus == NOT_IN_SERVICE)
    {
        i4RetStatus = nmhTestv2FsQoSClassMapStatus (&u4ErrorCode, u4Idx,
                                                    ACTIVE);
        if (i4RetStatus == SNMP_FAILURE)
        {
            free_octetstring (pOctStrGroupName);
            return (CLI_FAILURE);
        }

        i4RetStatus = nmhSetFsQoSClassMapStatus (u4Idx, ACTIVE);
        if (i4RetStatus == SNMP_FAILURE)
        {
            free_octetstring (pOctStrGroupName);
            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }
    }
    if (pOctStrGroupName != NULL)
    {
        free_octetstring (pOctStrGroupName);
    }
    return (CLI_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSCliUtlDisplayClsMapEntry                          */
/* Description        : This function is used to Display the class Map       */
/*                      Table Entries Parameters                             */
/* Input(s)           : CliHandle    - Handle to CLI session                 */
/*                    : u4Idx        - Table Entries Index                   */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : CLI_SUCCESS/CLI_FAILURE                              */
/* Called By          : QoSCliShowClsMapEntry                                */
/* Calling Function   : SNMP nmh Routines                                    */
/*****************************************************************************/
INT4
QoSCliUtlDisplayClsMapEntry (tCliHandle CliHandle, UINT4 u4Idx)
{
    INT4                i4RetStatus = SNMP_FAILURE;
    INT4                i4RowStatus = SNMP_FAILURE;
    UINT4               u4L2FltrId = 0;
    UINT4               u4L3FltrId = 0;
    UINT4               u4PriMapId = 0;
    UINT4               u4VlanMapId = 0;
    UINT4               u4Class = 0;
    INT4                i4PreColor = -1;
    tQoSClassInfoNode  *pClsInfoNode = NULL;

    /* Get All Fields */
    i4RetStatus = nmhGetFsQoSClassMapL2FilterId (u4Idx, &u4L2FltrId);
    if (i4RetStatus == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    i4RetStatus = nmhGetFsQoSClassMapL3FilterId (u4Idx, &u4L3FltrId);
    if (i4RetStatus == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    i4RetStatus = nmhGetFsQoSClassMapPriorityMapId (u4Idx, &u4PriMapId);
    if (i4RetStatus == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    i4RetStatus = nmhGetFsQoSClassMapVlanMapId (u4Idx, &u4VlanMapId);
    if (i4RetStatus == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    i4RetStatus = nmhGetFsQoSClassMapCLASS (u4Idx, &u4Class);
    if (i4RetStatus == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    i4RetStatus = nmhGetFsQoSClassMapPreColor (u4Idx, &i4PreColor);
    if (i4RetStatus == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    i4RetStatus = nmhGetFsQoSClassMapStatus (u4Idx, &i4RowStatus);
    if (i4RetStatus == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }
    /* Print */
    CliPrintf (CliHandle, "%-28s : %d\r\n", "ClassMapId", u4Idx);
    if (u4L2FltrId == 0)
    {
        CliPrintf (CliHandle, "%-28s : %s\r\n", "L2FilterId", "None");
    }
    else
    {
        CliPrintf (CliHandle, "%-28s : %d\r\n", "L2FilterId", u4L2FltrId);
    }

    if (u4L3FltrId == 0)
    {
        CliPrintf (CliHandle, "%-28s : %s\r\n", "L3FilterId", "None");
    }
    else
    {
        CliPrintf (CliHandle, "%-28s : %d\r\n", "L3FilterId", u4L3FltrId);
    }

    if (u4PriMapId == 0)
    {
        CliPrintf (CliHandle, "%-28s : %s\r\n", "PriorityMapId", "None");
    }
    else
    {
        CliPrintf (CliHandle, "%-28s : %d\r\n", "PriorityMapId", u4PriMapId);
    }

    if (u4VlanMapId == 0)
    {
        CliPrintf (CliHandle, "%-28s : %s\r\n", "VlanMapId", "None");
    }
    else
    {
        CliPrintf (CliHandle, "%-28s : %d\r\n", "VlanMapId", u4VlanMapId);
    }

    CliPrintf (CliHandle, "%-28s : %d\r\n", "CLASS", u4Class);

    pClsInfoNode = QoSUtlGetClassInfoNode (u4Class);
    if ((pClsInfoNode != NULL))
    {
        if (pClsInfoNode->pPlyMapNode != NULL)
        {
            if (pClsInfoNode->pPlyMapNode->u4Id == 0)
            {
                CliPrintf (CliHandle, "%-28s : %s\r\n", "PolicyMapId", "None");
            }
            else
            {
                CliPrintf (CliHandle, "%-28s : %d\r\n", "PolicyMapId",
                           pClsInfoNode->pPlyMapNode->u4Id);
            }
        }
        else
        {
            CliPrintf (CliHandle, "%-28s : %s\r\n", "PolicyMapId", "None");
        }
    }
    else
    {
        CliPrintf (CliHandle, "%-28s : %s\r\n", "PolicyMapId", "None");
    }

    switch (i4PreColor)
    {
        case QOS_CLI_CLS_COLOR_GREEN:
            CliPrintf (CliHandle, "%-28s : %s\r\n", "PreColor", "Green");
            break;

        case QOS_CLI_CLS_COLOR_YELLOW:
            CliPrintf (CliHandle, "%-28s : %s\r\n", "PreColor", "Yellow");
            break;

        case QOS_CLI_CLS_COLOR_RED:
            CliPrintf (CliHandle, "%-28s : %s\r\n", "PreColor", "Red");
            break;

        case QOS_CLI_CLS_COLOR_NONE:
            CliPrintf (CliHandle, "%-28s : %s\r\n", "PreColor", "None");
            break;

        default:
            CliPrintf (CliHandle, "%-28s : %s\r\n", "PreColor", "Invalid");
            break;
    }

    if (i4RowStatus == ACTIVE)
    {
        CliPrintf (CliHandle, "%-28s : %s\r\n", "Status", "Active");
    }
    else
    {
        CliPrintf (CliHandle, "%-28s : %s\r\n", "Status", "InActive");
    }

    CliPrintf (CliHandle, "\r\n");

    return (CLI_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSCliShowClsMapEntry                                */
/* Description        : This function is used to Display the Class Map Table */
/*                      Entries Parameters for the name pu1Name, if it is    */
/*                      NULL then All Entries will be displayed              */
/* Input(s)           : CliHandle    - Handle to CLI session                 */
/*                    : pu1Name      - Table Entry Name to Display           */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : CLI_SUCCESS/CLI_FAILURE                              */
/* Called By          : cli_process_qos_cmd                                  */
/* Calling Function   : QoSCliUtlDisplayClsMapEntry                          */
/*****************************************************************************/
INT4
QoSCliShowClsMapEntry (tCliHandle CliHandle, UINT4 u4Idx)
{
    INT4                i4RetStatus = SNMP_FAILURE;

    CliPrintf (CliHandle, "QoS Class Map Entries\r\n");
    CliPrintf (CliHandle, "---------------------\r\n");

    /* Display All */
    if (u4Idx == 0)
    {
        i4RetStatus = nmhGetFirstIndexFsQoSClassMapTable (&u4Idx);
        if (i4RetStatus == SNMP_FAILURE)
        {
            return (CLI_FAILURE);
        }

        do
        {
            /* Display Entry */
            i4RetStatus = QoSCliUtlDisplayClsMapEntry (CliHandle, u4Idx);
            if (i4RetStatus == CLI_FAILURE)
            {
                return (CLI_FAILURE);
            }

            i4RetStatus = nmhGetNextIndexFsQoSClassMapTable (u4Idx, &u4Idx);

        }
        while (i4RetStatus == SNMP_SUCCESS);

        return (CLI_SUCCESS);
    }
    else
    {
        i4RetStatus = QoSCliUtlDisplayClsMapEntry (CliHandle, u4Idx);
        if (i4RetStatus == CLI_FAILURE)
        {
            return (CLI_FAILURE);
        }
    }
    return (CLI_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSCliDelClsMapEntry                                 */
/* Description        : This function is used to delete Class Map table      */
/*                      Entry.                                               */
/* Input(s)           : CliHandle    - Handle to CLI session                 */
/*                    : pu1Name      - Table Entry Name to Delete            */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : CLI_SUCCESS/CLI_FAILURE                              */
/* Called By          : cli_process_qos_cmd                                  */
/* Calling Function   : SNMP nmh Routines                                    */
/*****************************************************************************/
INT4
QoSCliDelClsMapEntry (tCliHandle CliHandle, UINT4 u4Idx)
{
    INT4                i4RetStatus = SNMP_FAILURE;
    UINT4               u4ErrorCode = SNMP_ERR_NO_ERROR;

    UNUSED_PARAM (CliHandle);

    /* Delete a Entry */
    i4RetStatus = nmhTestv2FsQoSClassMapStatus (&u4ErrorCode, u4Idx, DESTROY);
    if (i4RetStatus == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    i4RetStatus = nmhSetFsQoSClassMapStatus (u4Idx, DESTROY);
    if (i4RetStatus == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }

    return (CLI_SUCCESS);

}

/*****************************************************************************/
/*               CLASS 2 PRI MAP TABLE FUNCTIONS                             */
/*****************************************************************************/

/*****************************************************************************/
/* Function Name      : QoSCliDelClsToPriMap                                 */
/* Description        : This function is used to delete Class to Priority    */
/*                      Map Table Entry                                      */
/* Input(s)           : CliHandle    - Handle to CLI session                 */
/*                    : u4Class      - Class Id                              */
/*                    : i4ReGenPri   - Regen Priority                        */
/*                    : pu1Name      - Group Name.                           */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : CLI_SUCCESS/CLI_FAILURE                              */
/* Called By          : cli_process_qos_cmd                                  */
/* Calling Function   : SNMP nmh Routines                                    */
/*****************************************************************************/
INT4
QoSCliDelClsToPriMap (tCliHandle CliHandle, UINT4 u4Class)
{
    UINT4               u4Idx = 0;
    INT4                i4RetStatus = SNMP_FAILURE;
    UINT4               u4ErrorCode = SNMP_ERR_NO_ERROR;
    INT4                i4RowStatus = SNMP_FAILURE;
    UINT4               u4ClsMapClass = SNMP_FAILURE;
    /* Get the Class Map Id */
    u4Idx = CLI_GET_CLS_MAP_ID ();
    i4RetStatus = nmhGetFsQoSClassMapCLASS (u4Idx, &u4ClsMapClass);
    if (u4ClsMapClass != QOS_DEF_CLASS)
    {
        if (i4RetStatus == SNMP_FAILURE)
        {
            CLI_SET_ERR (QOS_CLI_ERR_NO_CLS_CLSMAP);
            return (CLI_FAILURE);
        }
    }
    else
    {
        return (CLI_SUCCESS);
    }
    if (u4Class == u4ClsMapClass)
    {

        /* Delete Class To Priority Table Entry */
        i4RetStatus =
            nmhTestv2FsQoSClassToPriorityStatus (&u4ErrorCode, u4Class,
                                                 DESTROY);
        if (i4RetStatus == SNMP_FAILURE)
        {
            return (CLI_FAILURE);
        }

        i4RetStatus = nmhSetFsQoSClassToPriorityStatus (u4Class, DESTROY);
        if (i4RetStatus == SNMP_FAILURE)
        {
            CLI_SET_ERR (QOS_CLI_ERR_CLS_MAP_NO_ENTRY);
            return (CLI_FAILURE);
        }
        i4RetStatus = nmhGetFsQoSClassMapStatus (u4Idx, &i4RowStatus);
        if (i4RetStatus == SNMP_FAILURE)
        {
            return (CLI_FAILURE);
        }
        /* if RowStatus = NOT_IN_SERVICE or NOT_READY test and set the Values.
         * if RowStatus = ACTIVE Set the RowStatus as NOT_IN_SERVICE and than
         * test and set the Values*/
        if (i4RowStatus == ACTIVE)
        {
            i4RetStatus = nmhTestv2FsQoSClassMapStatus (&u4ErrorCode,
                                                        u4Idx, NOT_IN_SERVICE);
            if (i4RetStatus == SNMP_FAILURE)
            {
                return (CLI_FAILURE);
            }

            i4RetStatus = nmhSetFsQoSClassMapStatus (u4Idx, NOT_IN_SERVICE);
            if (i4RetStatus == SNMP_FAILURE)
            {
                CLI_FATAL_ERROR (CliHandle);
                return (CLI_FAILURE);
            }
        }
        /* Test & Set the each value */
        /* Set default class CLASS in the Class Map Table */

        i4RetStatus =
            nmhTestv2FsQoSClassMapCLASS (&u4ErrorCode, u4Idx, QOS_DEF_CLASS);
        if (i4RetStatus == SNMP_FAILURE)
        {
            return (CLI_FAILURE);
        }
        i4RetStatus = nmhSetFsQoSClassMapCLASS (u4Idx, QOS_DEF_CLASS);
        if (i4RetStatus == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }

        if (i4RowStatus == ACTIVE)
        {
            i4RetStatus = nmhTestv2FsQoSClassMapStatus (&u4ErrorCode,
                                                        u4Idx, ACTIVE);
            if (i4RetStatus == SNMP_FAILURE)
            {
                return (CLI_FAILURE);
            }

            i4RetStatus = nmhSetFsQoSClassMapStatus (u4Idx, ACTIVE);
            if (i4RetStatus == SNMP_FAILURE)
            {
                CLI_FATAL_ERROR (CliHandle);
                return (CLI_FAILURE);
            }

        }
    }
    else
    {
        CLI_SET_ERR (QOS_CLI_ERR_NO_CLS_CLSMAP);
        return (CLI_FAILURE);
    }
    return (CLI_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSCliUtlDisplayClsToPriMapEntry                     */
/* Description        : This function is used to Display the ClassToPriority */
/*                      Map Table Entries Parameters                         */
/* Input(s)           : CliHandle    - Handle to CLI session                 */
/*                    : u4Idx        - Table Entries Index                   */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : CLI_SUCCESS/CLI_FAILURE                              */
/* Called By          : QoSCliShowClsToPriMapEntry                           */
/* Calling Function   : SNMP nmh Routines                                    */
/*****************************************************************************/
INT4
QoSCliUtlDisplayClsToPriMapEntry (tCliHandle CliHandle, UINT4 u4Idx)
{
    INT4                i4RetStatus = SNMP_FAILURE;
    UINT4               u4ReGenPri = 0;
    INT4                i4Status = 0;

    /* Get All Fields */
    i4RetStatus = nmhGetFsQoSClassToPriorityRegenPri (u4Idx, &u4ReGenPri);
    if (i4RetStatus == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    i4RetStatus = nmhGetFsQoSClassToPriorityStatus (u4Idx, &i4Status);
    if (i4RetStatus == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }
    /* Print */
    CliPrintf (CliHandle, "%-20ld %d\r\n", u4Idx, u4ReGenPri);

    return (CLI_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSCliShowClsToPriMapEntry                           */
/* Description        : This function is used to Display the ClassToPriority */
/*                      Map Table Entries Parameters for the Given Group name*/
/* Input(s)           : CliHandle    - Handle to CLI session                 */
/*                    : pu1Name      - Group Name to Display                 */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : CLI_SUCCESS/CLI_FAILURE                              */
/* Called By          : cli_process_qos_cmd                                  */
/* Calling Function   : QoSCliUtlDisplayClsToPriMapEntry                     */
/*****************************************************************************/
INT4
QoSCliShowClsToPriMapEntry (tCliHandle CliHandle, UINT1 *pu1Name)
{
    tSNMP_OCTET_STRING_TYPE *pOctStrName = NULL;
    UINT4               u4Idx = 0;
    INT4                i4RetStatus = SNMP_FAILURE;
    INT4                i4RowStatus = SNMP_FAILURE;

    CliPrintf (CliHandle, "QoS Class To Priority Map Entries\r\n");
    CliPrintf (CliHandle, "---------------------------------\r\n");

    CliPrintf (CliHandle, "%-12s : %s\r\n", "GroupName", pu1Name);
    CliPrintf (CliHandle, "%-20s %s\r\n", "Class", "LocalPriority");

    /* Display All */
    i4RetStatus = nmhGetFirstIndexFsQoSClassToPriorityTable (&u4Idx);
    if (i4RetStatus == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    /* Allocate Memory For Name */
    pOctStrName = allocmem_octetstring (QOS_MAX_TABLE_NAME);
    if (pOctStrName == NULL)
    {
        return (CLI_FAILURE);
    }

    do
    {
        MEMSET (pOctStrName->pu1_OctetList, 0, QOS_MAX_TABLE_NAME);

        /* Get the Group and Check */
        i4RetStatus = nmhGetFsQoSClassToPriorityGroupName (u4Idx, pOctStrName);
        if (i4RetStatus == SNMP_FAILURE)
        {
            free_octetstring (pOctStrName);
            return (CLI_FAILURE);
        }

        if ((STRCMP (pOctStrName->pu1_OctetList, pu1Name)) == 0)
        {
            /* Check the Status */
            i4RetStatus =
                nmhGetFsQoSClassToPriorityStatus (u4Idx, &i4RowStatus);
            if (i4RetStatus == SNMP_FAILURE)
            {
                free_octetstring (pOctStrName);
                return (CLI_FAILURE);
            }
            if (i4RowStatus == ACTIVE)
            {
                /* Display Entry */
                i4RetStatus = QoSCliUtlDisplayClsToPriMapEntry (CliHandle,
                                                                u4Idx);
                if (i4RetStatus == CLI_FAILURE)
                {
                    free_octetstring (pOctStrName);
                    return (CLI_FAILURE);
                }
            }
        }

        i4RetStatus = nmhGetNextIndexFsQoSClassToPriorityTable (u4Idx, &u4Idx);
    }
    while (i4RetStatus == SNMP_SUCCESS);

    CliPrintf (CliHandle, "\r\n");

    free_octetstring (pOctStrName);
    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*                         METER TABLE FUNCTIONS                             */
/*****************************************************************************/

/*****************************************************************************/
/* Function Name      : QoSMeterPrompt                                       */
/* Description        : This function is used to set CLI Prompt string for   */
/*                      Meter Table                                          */
/* Input(s)           : pi1ModeName   - Prompt Name from CLI                 */
/*                    : pi1PromptStr  - Prompt String to Display             */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : CLI_SUCCESS/CLI_FAILURE                              */
/* Called By          : CLI Task                                             */
/* Calling Function   : None.                                                */
/*****************************************************************************/
PUBLIC INT1
QoSMeterPrompt (INT1 *pi1ModeName, INT1 *pi1PromptStr)
{
    UINT4               u4Idx;
    UINT4               u4Len = STRLEN (QOS_CLI_METER_MODE);

    if ((!pi1PromptStr) || (!pi1ModeName))
    {
        return FALSE;
    }

    if (STRNCMP (pi1ModeName, QOS_CLI_METER_MODE, u4Len) != 0)
    {
        return FALSE;
    }

    pi1ModeName = pi1ModeName + u4Len;
    u4Idx = CLI_ATOI (pi1ModeName);
    CLI_SET_METER_ID (u4Idx);
    STRNCPY (pi1PromptStr, "(config-meter)#", STRLEN ("(config-meter)#"));
    pi1PromptStr[STRLEN ("(config-meter)#")] = '\0';

    return TRUE;
}

/*****************************************************************************/
/* Function Name      : QoSCliGetMeterIndexFromName                          */
/* Description        : This function is used to get the Table Index from    */
/*                      the Given Table Name. If Name is not found it give   */
/*                      the next free Index                                  */
/* Input(s)           : pInOctStrName - Table Name.                          */
/*                    : pu4Idx        - Ptr to the Index of the Table for    */
/*                    :                 the given Name                       */
/*                    : pu4Status     - Status says that the  Entry needs to */
/*                    :                 be create or modify                  */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : CLI_SUCCESS/CLI_FAILURE                              */
/* Called By          : Meter Tbl Function                                   */
/* Calling Function   : SNMP nmh Routines                                    */
/*****************************************************************************/
INT4
QoSCliGetMeterIndexFromName (tSNMP_OCTET_STRING_TYPE * pInOctStrName,
                             UINT4 *pu4Idx, UINT4 *pu4Status)
{
    tSNMP_OCTET_STRING_TYPE *pOctStrName = NULL;
    UINT4               u4CurrIdx = 0;
    UINT4               u4PreIdx = 0;
    UINT4               u4IdxFound = CLI_FAILURE;
    INT4                i4SysControl = 0;
    INT4                i4RetStatus = SNMP_FAILURE;

    /* System Contol map be QOS_SYS_CNTL_SHUTDOWN */
    i4RetStatus = nmhGetFsQoSSystemControl (&i4SysControl);
    if ((i4RetStatus == SNMP_FAILURE)
        || (i4SysControl == QOS_SYS_CNTL_SHUTDOWN))
    {
        CLI_SET_ERR (QOS_CLI_ERR_MODULE_SHUTDOW);
        return (CLI_FAILURE);
    }

    /* No Entry Found Create first Entry  idx = 1 */
    i4RetStatus = nmhGetFirstIndexFsQoSMeterTable (&u4CurrIdx);
    if (i4RetStatus == SNMP_FAILURE)
    {
        *pu4Idx = 1;
        *pu4Status = QOS_CLI_NEW_ENTRY;
        return (CLI_SUCCESS);
    }

    /* Allocate Memory For Name */
    pOctStrName = allocmem_octetstring (QOS_MAX_TABLE_NAME);
    if (pOctStrName == NULL)
    {
        return (CLI_FAILURE);
    }
    MEMSET (pOctStrName->pu1_OctetList, 0, pOctStrName->i4_Length);

    do
    {
        /* Get the Name of the Current Entry */
        i4RetStatus = nmhGetFsQoSMeterName (u4CurrIdx, pOctStrName);
        if (i4RetStatus == SNMP_FAILURE)
        {
            free_octetstring (pOctStrName);
            return (CLI_FAILURE);
        }

        if (STRCMP (pOctStrName->pu1_OctetList, pInOctStrName->pu1_OctetList)
            == 0)
        {
            *pu4Idx = u4CurrIdx;
            *pu4Status = QOS_CLI_MODIFY_ENTRY;

            free_octetstring (pOctStrName);
            return (CLI_SUCCESS);
        }

        if (((u4CurrIdx - u4PreIdx) > 1) && (u4IdxFound == CLI_FAILURE))
        {
            *pu4Idx = u4PreIdx + 1;
            *pu4Status = QOS_CLI_NEW_ENTRY;
            u4IdxFound = CLI_SUCCESS;
        }

        u4PreIdx = u4CurrIdx;

        i4RetStatus = nmhGetNextIndexFsQoSMeterTable (u4CurrIdx, &u4CurrIdx);

    }
    while (i4RetStatus == SNMP_SUCCESS);

    if ((u4CurrIdx == u4PreIdx) && (u4IdxFound == CLI_FAILURE))
    {
        *pu4Idx = u4CurrIdx + 1;
        *pu4Status = QOS_CLI_NEW_ENTRY;
    }

    free_octetstring (pOctStrName);
    return (CLI_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSCliAddMeterEntry                                  */
/* Description        : This function is used to create Meter table          */
/*                      Entry and change the CLI Prompt.                     */
/* Input(s)           : CliHandle    - Handle to CLI session                 */
/*                    : pu1Name      - Table Entry Name to Create.           */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : CLI_SUCCESS/CLI_FAILURE                              */
/* Called By          : cli_process_qos_cmd                                  */
/* Calling Function   : SNMP nmh Routines                                    */
/*****************************************************************************/
INT4
QoSCliAddMeterEntry (tCliHandle CliHandle, UINT4 u4Idx)
{
    UINT1               au1Meter[QOS_CLI_MAX_PROMPT_LENGTH];
    INT4                i4RetStatus = SNMP_FAILURE;
    INT4                i4RowStatus = SNMP_FAILURE;
    UINT4               u4ErrorCode = SNMP_ERR_NO_ERROR;

    UNUSED_PARAM (CliHandle);

    i4RetStatus = nmhGetFsQoSMeterStatus (u4Idx, &i4RowStatus);
    if (i4RetStatus == SNMP_FAILURE)
    {
        /* Create new Entry */
        i4RetStatus = nmhTestv2FsQoSMeterStatus (&u4ErrorCode, u4Idx,
                                                 CREATE_AND_WAIT);
        if (i4RetStatus == SNMP_FAILURE)
        {
            return (CLI_FAILURE);
        }

        i4RetStatus = nmhSetFsQoSMeterStatus (u4Idx, CREATE_AND_WAIT);
        if (i4RetStatus == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }
    }

    /* Go to the Meter Mode of Cli Prompt */
    SPRINTF ((CHR1 *) au1Meter, "%s%u", QOS_CLI_METER_MODE, u4Idx);

    if (CliChangePath ((CHR1 *) au1Meter) == CLI_FAILURE)
    {
        CliPrintf (CliHandle, "\r%% Unable to enter Meter mode.\r\n");
        return (CLI_FAILURE);
    }

    return (CLI_SUCCESS);

}

/*****************************************************************************/
/* Function Name      : QoSCliRSSetMeterParams                               */
/* Description        : This function is used to Reset Meter table Entry     */
/*                      Parameter                                            */
/* Input(s)           : u4Idx - Table Index                                  */
/*                    : tQoSCliRSMeterParam  -  Pointer to the meter entry   */
/*                      i4RowStatus - RowStatus                              */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : CLI_SUCCESS/CLI_FAILURE                              */
/* Called By          : cli_process_qos_cmd                                  */
/* Calling Function   : SNMP nmh Routines                                    */
/*****************************************************************************/
VOID
QoSCliRSMeterTblEntry (UINT4 u4Idx, tQoSCliRSMeterParam * pQoSRSMetParam,
                       INT4 i4RowStatus)
{
    INT4                i4RetStatus = SNMP_FAILURE;

    if (pQoSRSMetParam->u4MeterType != (UINT4) CLI_RS_INVALID_MASK)
    {
        i4RetStatus = nmhSetFsQoSMeterType (u4Idx, pQoSRSMetParam->u4MeterType);
    }
    if (pQoSRSMetParam->u4ColorMode != (UINT4) CLI_RS_INVALID_MASK)
    {
        i4RetStatus = nmhSetFsQoSMeterColorMode (u4Idx,
                                                 pQoSRSMetParam->u4ColorMode);
    }

    if (pQoSRSMetParam->u4Interval != (UINT4) CLI_RS_INVALID_MASK)
    {
        i4RetStatus = nmhSetFsQoSMeterInterval (u4Idx,
                                                pQoSRSMetParam->u4Interval);
    }

    if (pQoSRSMetParam->u4CIR != (UINT4) CLI_RS_INVALID_MASK)
    {
        i4RetStatus = nmhSetFsQoSMeterCIR (u4Idx, pQoSRSMetParam->u4CIR);
    }

    if (pQoSRSMetParam->u4CBS != (UINT4) CLI_RS_INVALID_MASK)
    {
        i4RetStatus = nmhSetFsQoSMeterCBS (u4Idx, pQoSRSMetParam->u4CBS);
    }

    if (pQoSRSMetParam->u4EIR != (UINT4) CLI_RS_INVALID_MASK)
    {
        i4RetStatus = nmhSetFsQoSMeterEIR (u4Idx, pQoSRSMetParam->u4EIR);
    }

    if (pQoSRSMetParam->u4EBS != (UINT4) CLI_RS_INVALID_MASK)
    {
        i4RetStatus = nmhSetFsQoSMeterEBS (u4Idx, pQoSRSMetParam->u4EBS);
    }

    if (pQoSRSMetParam->u4NextMeter != (UINT4) CLI_RS_INVALID_MASK)
    {
        i4RetStatus = nmhSetFsQoSMeterNext (u4Idx, pQoSRSMetParam->u4NextMeter);
    }

    i4RetStatus = nmhSetFsQoSMeterStatus (u4Idx, i4RowStatus);

    UNUSED_PARAM (i4RetStatus);

}

/*****************************************************************************/
/* Function Name      : QoSCliSetMeterParams                                 */
/* Description        : This function is used to Set Meter table Entry       */
/*                      Parameter                                            */
/* Input(s)           : CliHandle    - Handle to CLI session                 */
/*                    : u4MeterType  - Meter Type                            */
/*                    : u4ColorMode  - Color Mode of the Meter               */
/*                    : u4Interval   - Interval for AvgRate Meter            */
/*                    : i4CIR        - CIR Value                             */
/*                    : i4CBS        - CBS Value                             */
/*                    : i4EIR        - EIR Value                             */
/*                    : i4EBS        - EBS Value                             */
/*                    : i4NextMeter  - Next Meter Id                         */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : CLI_SUCCESS/CLI_FAILURE                              */
/* Called By          : cli_process_qos_cmd                                  */
/* Calling Function   : SNMP nmh Routines                                    */
/*****************************************************************************/
INT4
QoSCliSetMeterParams (tCliHandle CliHandle, UINT4 u4MeterType,
                      UINT4 u4ColorMode, INT4 i4Interval, INT4 i4CIR,
                      INT4 i4CBS, INT4 i4EIR, INT4 i4EBS, INT4 i4NextMeter)
{
    tQoSCliRSMeterParam QoSRSMetParam;
    UINT4               u4Idx = 0;
    INT4                i4RetStatus = SNMP_FAILURE;
    INT4                i4RowStatus = SNMP_FAILURE;
    UINT4               u4ErrorCode = SNMP_ERR_NO_ERROR;

    /* To set all bits as 1 */
    MEMSET (&(QoSRSMetParam), 0xFF, sizeof (tQoSCliRSMeterParam));

    /* Get the Meter Id */
    u4Idx = CLI_GET_METER_ID ();

    /* Get the Status of this Entry */
    i4RetStatus = nmhGetFsQoSMeterStatus (u4Idx, &i4RowStatus);
    if (i4RetStatus == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }
    /* if RowStatus = NOT_IN_SERVICE or NOT_READY test and set the Values. 
     * if RowStatus = ACTIVE Set the RowStatus as NOT_IN_SERVICE and than   
     * test and set the Values 
     */
    if (i4RowStatus == ACTIVE)
    {
        i4RetStatus = nmhTestv2FsQoSMeterStatus (&u4ErrorCode, u4Idx,
                                                 NOT_IN_SERVICE);
        if (i4RetStatus == SNMP_FAILURE)
        {
            return (CLI_FAILURE);
        }

        i4RetStatus = nmhSetFsQoSMeterStatus (u4Idx, NOT_IN_SERVICE);
        if (i4RetStatus == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }
    }
    /*
     * 1. Test New value 
     * 2. Get Current value
     * 3. Set New value
     * 4. if Set Failed Restore Old Value else goto #1
     */
    i4RetStatus = nmhTestv2FsQoSMeterType (&u4ErrorCode, u4Idx, u4MeterType);
    if (i4RetStatus == SNMP_FAILURE)
    {
        QoSCliRSMeterTblEntry (u4Idx, &(QoSRSMetParam), i4RowStatus);
        return (CLI_FAILURE);
    }

    i4RetStatus = nmhGetFsQoSMeterType (u4Idx,
                                        (INT4 *) &(QoSRSMetParam.u4MeterType));
    if (i4RetStatus == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    i4RetStatus = nmhSetFsQoSMeterType (u4Idx, u4MeterType);
    if (i4RetStatus == SNMP_FAILURE)
    {
        QoSCliRSMeterTblEntry (u4Idx, &(QoSRSMetParam), i4RowStatus);
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }

    if (u4ColorMode != 0)
    {
        i4RetStatus = nmhTestv2FsQoSMeterColorMode (&u4ErrorCode, u4Idx,
                                                    u4ColorMode);
        if (i4RetStatus == SNMP_FAILURE)
        {
            QoSCliRSMeterTblEntry (u4Idx, &(QoSRSMetParam), i4RowStatus);
            return (CLI_FAILURE);
        }

        i4RetStatus =
            nmhGetFsQoSMeterColorMode (u4Idx,
                                       (INT4 *) &(QoSRSMetParam.u4ColorMode));
        if (i4RetStatus == SNMP_FAILURE)
        {
            return (CLI_FAILURE);
        }

        i4RetStatus = nmhSetFsQoSMeterColorMode (u4Idx, u4ColorMode);
        if (i4RetStatus == SNMP_FAILURE)
        {
            QoSCliRSMeterTblEntry (u4Idx, &(QoSRSMetParam), i4RowStatus);
            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }
    }

    if (i4Interval != -1)
    {
        i4RetStatus = nmhTestv2FsQoSMeterInterval (&u4ErrorCode, u4Idx,
                                                   (UINT4) i4Interval);
        if (i4RetStatus == SNMP_FAILURE)
        {
            QoSCliRSMeterTblEntry (u4Idx, &(QoSRSMetParam), i4RowStatus);
            return (CLI_FAILURE);
        }

        i4RetStatus = nmhGetFsQoSMeterInterval (u4Idx,
                                                &(QoSRSMetParam.u4Interval));
        if (i4RetStatus == SNMP_FAILURE)
        {
            return (CLI_FAILURE);
        }

        i4RetStatus = nmhSetFsQoSMeterInterval (u4Idx, (UINT4) i4Interval);
        if (i4RetStatus == SNMP_FAILURE)
        {
            QoSCliRSMeterTblEntry (u4Idx, &(QoSRSMetParam), i4RowStatus);
            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }
    }

    if (i4CIR != -1)
    {
        i4RetStatus = QoSUtilTestFsQoSMeterCIR (&u4ErrorCode, u4Idx,
                                                (UINT4) i4CIR, (UINT4) i4EIR);
        if (i4RetStatus == QOS_FAILURE)
        {
            QoSCliRSMeterTblEntry (u4Idx, &(QoSRSMetParam), i4RowStatus);
            return (CLI_FAILURE);
        }

        i4RetStatus = nmhGetFsQoSMeterCIR (u4Idx, &(QoSRSMetParam.u4CIR));
        if (i4RetStatus == SNMP_FAILURE)
        {
            return (CLI_FAILURE);
        }

        i4RetStatus = nmhSetFsQoSMeterCIR (u4Idx, (UINT4) i4CIR);
        if (i4RetStatus == SNMP_FAILURE)
        {
            QoSCliRSMeterTblEntry (u4Idx, &(QoSRSMetParam), i4RowStatus);
            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }
    }

    if (i4CBS != -1)
    {
        i4RetStatus = nmhTestv2FsQoSMeterCBS (&u4ErrorCode, u4Idx,
                                              (UINT4) i4CBS);
        if (i4RetStatus == SNMP_FAILURE)
        {
            QoSCliRSMeterTblEntry (u4Idx, &(QoSRSMetParam), i4RowStatus);
            return (CLI_FAILURE);
        }

        i4RetStatus = nmhGetFsQoSMeterCBS (u4Idx, &(QoSRSMetParam.u4CBS));
        if (i4RetStatus == SNMP_FAILURE)
        {
            return (CLI_FAILURE);
        }

        i4RetStatus = nmhSetFsQoSMeterCBS (u4Idx, (UINT4) i4CBS);
        if (i4RetStatus == SNMP_FAILURE)
        {
            QoSCliRSMeterTblEntry (u4Idx, &(QoSRSMetParam), i4RowStatus);
            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }
    }

    if (i4EIR != -1)
    {
        i4RetStatus = QoSUtilTestFsQoSMeterEIR (&u4ErrorCode, u4Idx,
                                                (UINT4) i4EIR, (UINT4) i4CIR);
        if (i4RetStatus == SNMP_FAILURE)
        {
            QoSCliRSMeterTblEntry (u4Idx, &(QoSRSMetParam), i4RowStatus);
            return (CLI_FAILURE);
        }

        i4RetStatus = nmhGetFsQoSMeterEIR (u4Idx, &(QoSRSMetParam.u4EIR));
        if (i4RetStatus == SNMP_FAILURE)
        {
            return (CLI_FAILURE);
        }

        i4RetStatus = nmhSetFsQoSMeterEIR (u4Idx, (UINT4) i4EIR);
        if (i4RetStatus == SNMP_FAILURE)
        {
            QoSCliRSMeterTblEntry (u4Idx, &(QoSRSMetParam), i4RowStatus);
            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }
    }

    if (i4EBS != -1)
    {
        i4RetStatus = nmhTestv2FsQoSMeterEBS (&u4ErrorCode, u4Idx,
                                              (UINT4) i4EBS);
        if (i4RetStatus == SNMP_FAILURE)
        {
            QoSCliRSMeterTblEntry (u4Idx, &(QoSRSMetParam), i4RowStatus);
            return (CLI_FAILURE);
        }

        i4RetStatus = nmhGetFsQoSMeterEBS (u4Idx, &(QoSRSMetParam.u4EBS));
        if (i4RetStatus == SNMP_FAILURE)
        {
            return (CLI_FAILURE);
        }

        i4RetStatus = nmhSetFsQoSMeterEBS (u4Idx, (UINT4) i4EBS);
        if (i4RetStatus == SNMP_FAILURE)
        {
            QoSCliRSMeterTblEntry (u4Idx, &(QoSRSMetParam), i4RowStatus);
            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }
    }

    if (i4NextMeter != -1)
    {
        i4RetStatus = nmhTestv2FsQoSMeterNext (&u4ErrorCode, u4Idx,
                                               (UINT4) i4NextMeter);
        if (i4RetStatus == SNMP_FAILURE)
        {
            QoSCliRSMeterTblEntry (u4Idx, &(QoSRSMetParam), i4RowStatus);
            return (CLI_FAILURE);
        }

        i4RetStatus = nmhGetFsQoSMeterNext (u4Idx,
                                            &(QoSRSMetParam.u4NextMeter));
        if (i4RetStatus == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }

        i4RetStatus = nmhSetFsQoSMeterNext (u4Idx, (UINT4) i4NextMeter);
        if (i4RetStatus == SNMP_FAILURE)
        {
            QoSCliRSMeterTblEntry (u4Idx, &(QoSRSMetParam), i4RowStatus);
            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }
    }

    /* Active the Row */
    i4RetStatus = nmhTestv2FsQoSMeterStatus (&u4ErrorCode, u4Idx, ACTIVE);
    if (i4RetStatus == SNMP_FAILURE)
    {
        switch (u4MeterType)
        {
            case QOS_METER_TYPE_SIMPLE_TB:
                CliPrintf (CliHandle, "\r%% Valid value for Given Meter Type "
                           "are CIR and CBS.\r\n");
                break;
            case QOS_METER_TYPE_AVG_RATE:
                CliPrintf (CliHandle, "\r%% Valid value for Given Meter Type "
                           "are Interval and CIR.\r\n");
                break;

            case QOS_METER_TYPE_SRTCM:
            case QOS_METER_TYPE_MEF_COUPLED:
                CliPrintf (CliHandle, "\r%% Valid value for Given Meter Type "
                           "are CIR, CBS and EBS.\r\n");
                break;
            case QOS_METER_TYPE_TRTCM:
            case QOS_METER_TYPE_MEF_DECOUPLED:
                CliPrintf (CliHandle, "\r%% Valid value for Given Meter Type "
                           "are CIR, CBS  EIR, and EBS.\r\n");
                break;

            case QOS_METER_TYPE_TSWTCM:
                CliPrintf (CliHandle, "\r%% Valid value for Given Meter Type "
                           "are CIR, EIR and Interval.\r\n");
                break;
            default:
                break;
        }

        QoSCliRSMeterTblEntry (u4Idx, &(QoSRSMetParam), i4RowStatus);
        return (CLI_FAILURE);
    }

    i4RetStatus = nmhSetFsQoSMeterStatus (u4Idx, ACTIVE);
    if (i4RetStatus == SNMP_FAILURE)
    {
        QoSCliRSMeterTblEntry (u4Idx, &(QoSRSMetParam), i4RowStatus);
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }

    return (CLI_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSCliUtlDisplayMeterEntry                           */
/* Description        : This function is used to Display the Meter Map       */
/*                      Table Entries Parameters                             */
/* Input(s)           : CliHandle    - Handle to CLI session                 */
/*                    : u4Idx        - Table Entries Index                   */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : CLI_SUCCESS/CLI_FAILURE                              */
/* Called By          : QoSCliShowMeterEntry                                 */
/* Calling Function   : SNMP nmh Routines                                    */
/*****************************************************************************/
INT4
QoSCliUtlDisplayMeterEntry (tCliHandle CliHandle, UINT4 u4Idx)
{
    INT4                i4RetStatus = SNMP_FAILURE;
    INT4                i4RowStatus = SNMP_FAILURE;
    INT4                i4MeterType = 0;
    INT4                i4ColorMode = 0;
    UINT4               u4Interval = 0;
    UINT4               u4CIR = 0;
    UINT4               u4CBS = 0;
    UINT4               u4EIR = 0;
    UINT4               u4EBS = 0;
    UINT4               u4NextMeter = 0;

    /* Get All Fields */
    i4RetStatus = nmhGetFsQoSMeterType (u4Idx, &i4MeterType);
    if (i4RetStatus == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    i4RetStatus = nmhGetFsQoSMeterInterval (u4Idx, &u4Interval);
    if (i4RetStatus == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    i4RetStatus = nmhGetFsQoSMeterColorMode (u4Idx, &i4ColorMode);
    if (i4RetStatus == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    i4RetStatus = nmhGetFsQoSMeterCIR (u4Idx, &u4CIR);
    if (i4RetStatus == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    i4RetStatus = nmhGetFsQoSMeterCBS (u4Idx, &u4CBS);
    if (i4RetStatus == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    i4RetStatus = nmhGetFsQoSMeterEIR (u4Idx, &u4EIR);
    if (i4RetStatus == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    i4RetStatus = nmhGetFsQoSMeterEBS (u4Idx, &u4EBS);
    if (i4RetStatus == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    i4RetStatus = nmhGetFsQoSMeterNext (u4Idx, &u4NextMeter);
    if (i4RetStatus == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    i4RetStatus = nmhGetFsQoSMeterStatus (u4Idx, &i4RowStatus);
    if (i4RetStatus == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    /* Print */
    CliPrintf (CliHandle, "%-28s : %d\r\n", "MeterId", u4Idx);

    switch (i4MeterType)
    {
        case QOS_CLI_METER_TYPE_STB:
            CliPrintf (CliHandle, "%-28s : %s\r\n", "Type",
                       "Simple Token Bucket");
            break;

        case QOS_CLI_METER_TYPE_AVGRATE:
            CliPrintf (CliHandle, "%-28s : %s\r\n", "Type", "AvgRate");
            break;

        case QOS_CLI_METER_TYPE_SRTCM:
            CliPrintf (CliHandle, "%-28s : %s\r\n", "Type", "SRTCM");
            break;

        case QOS_CLI_METER_TYPE_TRTCM:
            CliPrintf (CliHandle, "%-28s : %s\r\n", "Type", "TRTCM");
            break;

        case QOS_CLI_METER_TYPE_TSWTCM:
            CliPrintf (CliHandle, "%-28s : %s\r\n", "Type", "STWTCM");
            break;

        case QOS_CLI_METER_TYPE_MEFCOUPLED:
            CliPrintf (CliHandle, "%-28s : %s\r\n", "Type", "MEFCoupled");
            break;

        case QOS_CLI_METER_TYPE_MEFDECOUPLED:
            CliPrintf (CliHandle, "%-28s : %s\r\n", "Type", "MEFDeCoupled");
            break;

        default:
            CliPrintf (CliHandle, "%-28s : %s\r\n", "Type", "Invalid");
            break;
    }

    if (i4ColorMode == QOS_CLI_COLOR_MODE_AWARE)
    {
        CliPrintf (CliHandle, "%-28s : %s\r\n", "Color Mode", "Color Aware");
    }
    else
    {
        CliPrintf (CliHandle, "%-28s : %s\r\n", "Color Mode", "Color Blind");
    }

    if (u4Interval == 0)
    {
        CliPrintf (CliHandle, "%-28s : %s\r\n", "Interval", "None");
    }
    else
    {
        CliPrintf (CliHandle, "%-28s : %d\r\n", "Interval", u4Interval);
    }

    if (u4CIR == 0)
    {
        CliPrintf (CliHandle, "%-28s : %s\r\n", "CIR", "None");
    }
    else
    {
        CliPrintf (CliHandle, "%-28s : %d\r\n", "CIR", u4CIR);
    }

    if (u4CBS == 0)
    {
        CliPrintf (CliHandle, "%-28s : %s\r\n", "CBS", "None");
    }
    else
    {
        CliPrintf (CliHandle, "%-28s : %d\r\n", "CBS", u4CBS);
    }

    if (u4EIR == 0)
    {
        CliPrintf (CliHandle, "%-28s : %s\r\n", "EIR", "None");
    }
    else
    {
        CliPrintf (CliHandle, "%-28s : %d\r\n", "EIR", u4EIR);
    }

    if (u4EBS == 0)
    {
        CliPrintf (CliHandle, "%-28s : %s\r\n", "EBS", "None");
    }
    else
    {
        CliPrintf (CliHandle, "%-28s : %d\r\n", "EBS", u4EBS);
    }

    if (u4NextMeter == 0)
    {
        CliPrintf (CliHandle, "%-28s : %s\r\n", "NextMeter", "None");
    }
    else
    {
        CliPrintf (CliHandle, "%-28s : %d\r\n", "NextMeter", u4NextMeter);
    }

    if (i4RowStatus == ACTIVE)
    {
        CliPrintf (CliHandle, "%-28s : %s\r\n", "Status", "Active");
    }
    else
    {
        CliPrintf (CliHandle, "%-28s : %s\r\n", "Status", "InActive");
    }

    CliPrintf (CliHandle, "\r\n");

    return (CLI_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSCliShowMeterEntry                                 */
/* Description        : This function is used to Display the Meter           */
/*                      Table Entries Parameters for the name pu1Name, if    */
/*                      it is NULL then All Entries will be displayed        */
/* Input(s)           : CliHandle    - Handle to CLI session                 */
/*                    : pu1Name      - Table Entry Name to Display           */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : CLI_SUCCESS/CLI_FAILURE                              */
/* Called By          : cli_process_qos_cmd                                  */
/* Calling Function   : QoSCliUtlDisplayMeterEntry                           */
/*****************************************************************************/
INT4
QoSCliShowMeterEntry (tCliHandle CliHandle, UINT4 u4Idx)
{
    INT4                i4RetStatus = SNMP_FAILURE;

    CliPrintf (CliHandle, "QoS Meter Entries\r\n");
    CliPrintf (CliHandle, "-----------------\r\n");

    /* Display All */
    if (u4Idx == 0)
    {
        i4RetStatus = nmhGetFirstIndexFsQoSMeterTable (&u4Idx);
        if (i4RetStatus == SNMP_FAILURE)
        {
            return (CLI_FAILURE);
        }

        do
        {
            /* Display Entry */
            i4RetStatus = QoSCliUtlDisplayMeterEntry (CliHandle, u4Idx);
            if (i4RetStatus == CLI_FAILURE)
            {
                return (CLI_FAILURE);
            }

            i4RetStatus = nmhGetNextIndexFsQoSMeterTable (u4Idx, &u4Idx);
        }
        while (i4RetStatus == SNMP_SUCCESS);

        return (CLI_SUCCESS);
    }
    else
    {
        i4RetStatus = QoSCliUtlDisplayMeterEntry (CliHandle, u4Idx);
        if (i4RetStatus == CLI_FAILURE)
        {
            return (CLI_FAILURE);
        }
    }

    return (CLI_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSCliDelMeterEntry                                  */
/* Description        : This function is used to delete Meter Map table      */
/*                      Entry.                                               */
/* Input(s)           : CliHandle    - Handle to CLI session                 */
/*                    : pu1Name      - Table Entry Name to Delete.           */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : CLI_SUCCESS/CLI_FAILURE                              */
/* Called By          : cli_process_qos_cmd                                  */
/* Calling Function   : SNMP nmh Routines                                    */
/*****************************************************************************/
INT4
QoSCliDelMeterEntry (tCliHandle CliHandle, UINT4 u4Idx)
{
    INT4                i4RetStatus = SNMP_FAILURE;
    UINT4               u4ErrorCode = SNMP_ERR_NO_ERROR;

    UNUSED_PARAM (CliHandle);

    /* Delete a Entry */
    i4RetStatus = nmhTestv2FsQoSMeterStatus (&u4ErrorCode, u4Idx, DESTROY);
    if (i4RetStatus == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    i4RetStatus = nmhSetFsQoSMeterStatus (u4Idx, DESTROY);
    if (i4RetStatus == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }

    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*                       PLYAMP TABLE FUNCTIONS                              */
/*****************************************************************************/

/*****************************************************************************/
/* Function Name      : QoSPlyMapPrompt                                      */
/* Description        : This function is used to set CLI Prompt string for   */
/*                      Policy map Table                                     */
/* Input(s)           : pi1ModeName   - Prompt Name from CLI                 */
/*                    : pi1PromptStr  - Prompt String to Display             */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : CLI_SUCCESS/CLI_FAILURE                              */
/* Called By          : CLI Task                                             */
/* Calling Function   : None.                                                */
/*****************************************************************************/
PUBLIC INT1
QoSPlyMapPrompt (INT1 *pi1ModeName, INT1 *pi1PromptStr)
{
    UINT4               u4Idx;
    UINT4               u4Len = STRLEN (QOS_CLI_PLY_MAP_MODE);

    if ((!pi1PromptStr) || (!pi1ModeName))
    {
        return FALSE;
    }

    if (STRNCMP (pi1ModeName, QOS_CLI_PLY_MAP_MODE, u4Len) != 0)
    {
        return FALSE;
    }

    pi1ModeName = pi1ModeName + u4Len;
    u4Idx = CLI_ATOI (pi1ModeName);
    CLI_SET_PLY_MAP_ID (u4Idx);
    STRNCPY (pi1PromptStr, "(config-ply-map)#", STRLEN ("(config-ply-map)#"));
    pi1PromptStr[STRLEN ("(config-ply-map)#")] = '\0';

    return TRUE;
}

/*****************************************************************************/
/* Function Name      : QoSCliGetPlyMapIndexFromName                         */
/* Description        : This function is used to get the Table Index from    */
/*                      the Given Table Name. If Name is not found it give   */
/*                      the next free Index                                  */
/* Input(s)           : pInOctStrName - Table Name.                          */
/*                    : pu4Idx        - Ptr to the Index of the Table for    */
/*                    :                 the given Name                       */
/*                    : pu4Status     - Status says that the  Entry needs to */
/*                    :                 be create or modify                  */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : CLI_SUCCESS/CLI_FAILURE                              */
/* Called By          : PlyMap Tbl Function                                  */
/* Calling Function   : SNMP nmh Routines                                    */
/*****************************************************************************/
INT4
QoSCliGetPlyMapIndexFromName (tSNMP_OCTET_STRING_TYPE * pInOctStrName,
                              UINT4 *pu4Idx, UINT4 *pu4Status)
{
    tSNMP_OCTET_STRING_TYPE *pOctStrName = NULL;
    UINT4               u4CurrIdx = 0;
    UINT4               u4PreIdx = 0;
    UINT4               u4IdxFound = CLI_FAILURE;
    INT4                i4SysControl = 0;
    INT4                i4RetStatus = SNMP_FAILURE;

    /* System Contol map be QOS_SYS_CNTL_SHUTDOWN */
    i4RetStatus = nmhGetFsQoSSystemControl (&i4SysControl);
    if ((i4RetStatus == SNMP_FAILURE)
        || (i4SysControl == QOS_SYS_CNTL_SHUTDOWN))
    {
        CLI_SET_ERR (QOS_CLI_ERR_MODULE_SHUTDOW);
        return (CLI_FAILURE);
    }

    /* No Entry Found Create first Entry  idx = 1 */
    i4RetStatus = nmhGetFirstIndexFsQoSPolicyMapTable (&u4CurrIdx);
    if (i4RetStatus == SNMP_FAILURE)
    {
        *pu4Idx = 1;
        *pu4Status = QOS_CLI_NEW_ENTRY;
        return (CLI_SUCCESS);
    }

    /* Allocate Memory For Name */
    pOctStrName = allocmem_octetstring (QOS_MAX_TABLE_NAME);
    if (pOctStrName == NULL)
    {
        return (CLI_FAILURE);
    }
    MEMSET (pOctStrName->pu1_OctetList, 0, pOctStrName->i4_Length);

    do
    {
        /* Get the Name of the Current Entry */
        i4RetStatus = nmhGetFsQoSPolicyMapName (u4CurrIdx, pOctStrName);
        if (i4RetStatus == SNMP_FAILURE)
        {
            free_octetstring (pOctStrName);
            return (CLI_FAILURE);
        }

        if (STRCMP (pOctStrName->pu1_OctetList, pInOctStrName->pu1_OctetList)
            == 0)
        {
            *pu4Idx = u4CurrIdx;
            *pu4Status = QOS_CLI_MODIFY_ENTRY;

            free_octetstring (pOctStrName);
            return (CLI_SUCCESS);
        }

        if (((u4CurrIdx - u4PreIdx) > 1) && (u4IdxFound == CLI_FAILURE))
        {
            *pu4Idx = u4PreIdx + 1;
            *pu4Status = QOS_CLI_NEW_ENTRY;
            u4IdxFound = CLI_SUCCESS;
        }

        u4PreIdx = u4CurrIdx;

        i4RetStatus = nmhGetNextIndexFsQoSPolicyMapTable (u4CurrIdx,
                                                          &u4CurrIdx);

    }
    while (i4RetStatus == SNMP_SUCCESS);

    if ((u4CurrIdx == u4PreIdx) && (u4IdxFound == CLI_FAILURE))
    {
        *pu4Idx = u4CurrIdx + 1;
        *pu4Status = QOS_CLI_NEW_ENTRY;
    }

    free_octetstring (pOctStrName);
    return (CLI_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSCliAddPlyMapEntry                                 */
/* Description        : This function is used to create Policy Map table     */
/*                      Entry and change the CLI Prompt.                     */
/* Input(s)           : CliHandle    - Handle to CLI session                 */
/*                    : pu1Name      - Table Entry Name to Create.           */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : CLI_SUCCESS/CLI_FAILURE                              */
/* Called By          : cli_process_qos_cmd                                  */
/* Calling Function   : SNMP nmh Routines                                    */
/*****************************************************************************/
INT4
QoSCliAddPlyMapEntry (tCliHandle CliHandle, UINT4 u4Idx)
{
    UINT1               au1PlyMap[QOS_CLI_MAX_PROMPT_LENGTH];
    INT4                i4RetStatus = SNMP_FAILURE;
    INT4                i4RowStatus = SNMP_FAILURE;
    UINT4               u4ErrorCode = SNMP_ERR_NO_ERROR;

    UNUSED_PARAM (CliHandle);

    i4RetStatus = nmhGetFsQoSPolicyMapStatus (u4Idx, &i4RowStatus);
    if (i4RetStatus == SNMP_FAILURE)
    {
        /* Create new Entry */
        i4RetStatus = nmhTestv2FsQoSPolicyMapStatus (&u4ErrorCode, u4Idx,
                                                     CREATE_AND_WAIT);
        if (i4RetStatus == SNMP_FAILURE)
        {
            return (CLI_FAILURE);
        }

        i4RetStatus = nmhSetFsQoSPolicyMapStatus (u4Idx, CREATE_AND_WAIT);
        if (i4RetStatus == SNMP_FAILURE)
        {
            ISSCliCheckAndThrowFatalError (CliHandle);
            return (CLI_FAILURE);
        }
    }

    /* Go to the Policy Map Mode of Cli Prompt */
    SPRINTF ((CHR1 *) au1PlyMap, "%s%u", QOS_CLI_PLY_MAP_MODE, u4Idx);

    if (CliChangePath ((CHR1 *) au1PlyMap) == CLI_FAILURE)
    {
        CliPrintf (CliHandle, "\r%% Unable to enter Policy Map mode.\r\n");
        return (CLI_FAILURE);
    }

    return (CLI_SUCCESS);

}

/*****************************************************************************/
/* Function Name      : QoSCliRSPlyMapClass                                  */
/* Description        : This function is used to Reset Policy Map table Entry*/
/*                      Parameter                                            */
/* Input(s)           : u4Idx        - Table Index                           */
/*                    : u4Class      - CLASS Id                              */
/*                    : u4IfIndex    - Interface Name.                       */
/*                    : i4PriType    - Priority Type                         */
/*                    : u4PriVal     - Priority Value                        */
/*                    : i4RowStatus  - Row Status                            */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : CLI_SUCCESS/CLI_FAILURE                              */
/* Called By          : cli_process_qos_cmd                                  */
/* Calling Function   : SNMP nmh Routines                                    */
/*****************************************************************************/
VOID
QoSCliRSPlyMapClass (UINT4 u4Idx, UINT4 u4Class, UINT4 u4IfIndex,
                     UINT4 u4PriType, UINT4 u4PriVal, UINT4 u4DEI,
                     INT4 i4RowStatus)
{
    INT4                i4RetStatus = SNMP_FAILURE;

    if (u4Class != (UINT4) CLI_RS_INVALID_MASK)
    {
        i4RetStatus = nmhSetFsQoSPolicyMapCLASS (u4Idx, u4Class);
    }

    if (u4IfIndex != (UINT4) CLI_RS_INVALID_MASK)
    {
        i4RetStatus = nmhSetFsQoSPolicyMapIfIndex (u4Idx, u4IfIndex);
    }

    if (u4PriType != (UINT4) CLI_RS_INVALID_MASK)
    {
        i4RetStatus = nmhSetFsQoSPolicyMapPHBType (u4Idx, u4PriType);
    }

    if (u4PriVal != (UINT4) CLI_RS_INVALID_MASK)
    {
        i4RetStatus = nmhSetFsQoSPolicyMapDefaultPHB (u4Idx, u4PriVal);
    }
    if (u4DEI != (UINT4) CLI_RS_INVALID_MASK)
    {
        i4RetStatus = nmhSetFsQoSPolicyDefaultVlanDE (u4Idx, u4DEI);

    }

    i4RetStatus = nmhSetFsQoSPolicyMapStatus (u4Idx, i4RowStatus);

    UNUSED_PARAM (i4RetStatus);

}

/*****************************************************************************/
/* Function Name      : QoSCliSetPlyMapClass                                 */
/* Description        : This function is used to Set Policy Map table Entry  */
/*                      Parameter                                            */
/* Input(s)           : CliHandle    - Handle to CLI session                 */
/*                    : i4Class      - CLASS Id                              */
/*                    : u4IfIndex    - Interface Name.                       */
/*                    : i4PriType    - Incomming Priority Type               */
/*                    : u1InPri      - Incomming Priority Value              */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : CLI_SUCCESS/CLI_FAILURE                              */
/* Called By          : cli_process_qos_cmd                                  */
/* Calling Function   : SNMP nmh Routines                                    */
/*****************************************************************************/
INT4
QoSCliSetPlyMapClass (tCliHandle CliHandle, INT4 i4Class,
                      UINT4 u4IfIndex, INT4 i4PriType, INT4 i4PriVal,
                      INT4 i4DEI)
{
    UINT4               u4Idx = 0;
    INT4                i4RetStatus = SNMP_FAILURE;
    INT4                i4RowStatus = SNMP_FAILURE;
    UINT4               u4ErrorCode = SNMP_ERR_NO_ERROR;
    UINT4               u4CurClass = (UINT4) CLI_RS_INVALID_MASK;
    UINT4               u4CurIfIndex = (UINT4) CLI_RS_INVALID_MASK;
    UINT4               u4CurPriType = (UINT4) CLI_RS_INVALID_MASK;
    UINT4               u4CurPriVal = (UINT4) CLI_RS_INVALID_MASK;
    UINT4               u4CurDEI = (UINT4) CLI_RS_INVALID_MASK;

    /* Get the Policy Map Id */
    u4Idx = CLI_GET_PLY_MAP_ID ();

    i4RetStatus = nmhGetFsQoSPolicyMapStatus (u4Idx, &i4RowStatus);
    if (i4RetStatus == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }
    /* if RowStatus = NOT_IN_SERVICE or NOT_READY test and set the Values.
     * if RowStatus = ACTIVE Set the RowStatus as NOT_IN_SERVICE and than
     * test and set the Values
     */
    if (i4RowStatus == ACTIVE)
    {
        i4RetStatus = nmhTestv2FsQoSPolicyMapStatus (&u4ErrorCode,
                                                     u4Idx, NOT_IN_SERVICE);
        if (i4RetStatus == SNMP_FAILURE)
        {
            return (CLI_FAILURE);
        }

        i4RetStatus = nmhSetFsQoSPolicyMapStatus (u4Idx, NOT_IN_SERVICE);
        if (i4RetStatus == SNMP_FAILURE)
        {
            ISSCliCheckAndThrowFatalError (CliHandle);
            return (CLI_FAILURE);
        }
        i4RowStatus = NOT_IN_SERVICE;
    }

    /* Test & Set the each value */
    if (i4Class != -1)
    {
        i4RetStatus = nmhTestv2FsQoSPolicyMapCLASS (&u4ErrorCode, u4Idx,
                                                    (UINT4) i4Class);
        if (i4RetStatus == SNMP_FAILURE)
        {
            return (CLI_FAILURE);
        }

        i4RetStatus = nmhGetFsQoSPolicyMapCLASS (u4Idx, &u4CurClass);
        if (i4RetStatus == SNMP_FAILURE)
        {
            return (CLI_FAILURE);
        }

        i4RetStatus = nmhSetFsQoSPolicyMapCLASS (u4Idx, (UINT4) i4Class);
        if (i4RetStatus == SNMP_FAILURE)
        {
            ISSCliCheckAndThrowFatalError (CliHandle);
            return (CLI_FAILURE);
        }
    }
    else
    {
        /* Resetting to Default Class */
        i4Class = 0;
        i4RetStatus = nmhSetFsQoSPolicyMapCLASS (u4Idx, (UINT4) i4Class);
        if (i4RetStatus == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }
    }

    if (u4IfIndex != 0)
    {
        i4RetStatus = nmhTestv2FsQoSPolicyMapIfIndex (&u4ErrorCode, u4Idx,
                                                      u4IfIndex);
        if (i4RetStatus == SNMP_FAILURE)
        {
            QoSCliRSPlyMapClass (u4Idx, u4CurClass, u4CurIfIndex, u4CurPriType,
                                 u4CurPriVal, u4CurDEI, i4RowStatus);

            return (CLI_FAILURE);
        }

        i4RetStatus = nmhGetFsQoSPolicyMapIfIndex (u4Idx, &u4CurIfIndex);
        if (i4RetStatus == SNMP_FAILURE)
        {
            return (CLI_FAILURE);
        }

        i4RetStatus = nmhSetFsQoSPolicyMapIfIndex (u4Idx, u4IfIndex);
        if (i4RetStatus == SNMP_FAILURE)
        {
            QoSCliRSPlyMapClass (u4Idx, u4CurClass, u4CurIfIndex, u4CurPriType,
                                 u4CurPriVal, u4CurDEI, i4RowStatus);

            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }
    }
    else
    {
        /* Resetting to default value which is zero for Policy Map Interface index
         */
        u4IfIndex = 0;
        i4RetStatus = nmhSetFsQoSPolicyMapIfIndex (u4Idx, u4IfIndex);
        if (i4RetStatus == SNMP_FAILURE)
        {
            QoSCliRSPlyMapClass (u4Idx, u4CurClass, u4CurIfIndex, u4CurPriType,
                                 u4CurPriVal, u4CurDEI, i4RowStatus);

            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }
    }

    if (i4PriType != -1)
    {
        i4RetStatus = nmhTestv2FsQoSPolicyMapPHBType (&u4ErrorCode, u4Idx,
                                                      i4PriType);
        if (i4RetStatus == SNMP_FAILURE)
        {
            QoSCliRSPlyMapClass (u4Idx, u4CurClass, u4CurIfIndex, u4CurPriType,
                                 u4CurPriVal, u4CurDEI, i4RowStatus);

            return (CLI_FAILURE);
        }

        i4RetStatus = nmhGetFsQoSPolicyMapPHBType (u4Idx,
                                                   (INT4 *) &u4CurPriType);
        if (i4RetStatus == SNMP_FAILURE)
        {
            return (CLI_FAILURE);
        }

        i4RetStatus = nmhSetFsQoSPolicyMapPHBType (u4Idx, i4PriType);
        if (i4RetStatus == SNMP_FAILURE)
        {
            QoSCliRSPlyMapClass (u4Idx, u4CurClass, u4CurIfIndex, u4CurPriType,
                                 u4CurPriVal, u4CurDEI, i4RowStatus);

            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }

        if (i4PriVal != -1)
        {
            i4RetStatus = nmhTestv2FsQoSPolicyMapDefaultPHB (&u4ErrorCode,
                                                             u4Idx, i4PriVal);
            if (i4RetStatus == SNMP_FAILURE)
            {
                QoSCliRSPlyMapClass (u4Idx, u4CurClass, u4CurIfIndex,
                                     u4CurPriType, u4CurPriVal, u4CurDEI,
                                     i4RowStatus);
                CLI_FATAL_ERROR (CliHandle);
                return (CLI_FAILURE);
            }
            i4RetStatus = nmhGetFsQoSPolicyMapDefaultPHB (u4Idx, &u4CurPriVal);
            if (i4RetStatus == SNMP_FAILURE)
            {
                return (CLI_FAILURE);
            }

            i4RetStatus = nmhSetFsQoSPolicyMapDefaultPHB (u4Idx, i4PriVal);
            if (i4RetStatus == SNMP_FAILURE)
            {
                QoSCliRSPlyMapClass (u4Idx, u4CurClass, u4CurIfIndex,
                                     u4CurPriType, u4CurPriVal, u4CurDEI,
                                     i4RowStatus);

                CLI_FATAL_ERROR (CliHandle);
                return (CLI_FAILURE);
            }
        }
        if (i4PriType == QOS_CLI_PLY_PHB_TYPE_DOT1P)
        {
            i4RetStatus = nmhTestv2FsQoSPolicyDefaultVlanDE (&u4ErrorCode,
                                                             u4Idx, i4DEI);
            if (i4RetStatus == SNMP_FAILURE)
            {
                QoSCliRSPlyMapClass (u4Idx, u4CurClass, u4CurIfIndex,
                                     u4CurPriType, u4CurPriVal, u4CurDEI,
                                     i4RowStatus);
                return (CLI_FAILURE);
            }

            i4RetStatus =
                nmhGetFsQoSPolicyDefaultVlanDE (u4Idx, (INT4 *) (&u4CurDEI));
            if (i4RetStatus == SNMP_FAILURE)
            {
                return (CLI_FAILURE);
            }

            i4RetStatus = nmhSetFsQoSPolicyDefaultVlanDE (u4Idx, i4DEI);
            if (i4RetStatus == SNMP_FAILURE)
            {
                QoSCliRSPlyMapClass (u4Idx, u4CurClass, u4CurIfIndex,
                                     u4CurPriType, u4CurPriVal, u4CurDEI,
                                     i4RowStatus);
                return (CLI_FAILURE);
            }
        }
    }

    if (i4RowStatus == NOT_IN_SERVICE)
    {
        /* Active the Row */
        i4RetStatus = nmhTestv2FsQoSPolicyMapStatus (&u4ErrorCode, u4Idx,
                                                     ACTIVE);
        if (i4RetStatus == SNMP_FAILURE)
        {
            QoSCliRSPlyMapClass (u4Idx, u4CurClass, u4CurIfIndex,
                                 u4CurPriType, u4CurPriVal, u4CurDEI,
                                 i4RowStatus);

            return (CLI_FAILURE);
        }

        i4RetStatus = nmhSetFsQoSPolicyMapStatus (u4Idx, ACTIVE);
        if (i4RetStatus == SNMP_FAILURE)
        {
            QoSCliRSPlyMapClass (u4Idx, u4CurClass, u4CurIfIndex,
                                 u4CurPriType, u4CurPriVal, u4CurDEI,
                                 i4RowStatus);
            ISSCliCheckAndThrowFatalError (CliHandle);
            return (CLI_FAILURE);
        }
    }
    return (CLI_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSCliNoSetPlyIf                                     */
/* Description        : This function is used to Set Policy Ifnum as 0       */
/* Input(s)           : CliHandle    - Handle to CLI session                 */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : CLI_SUCCESS/CLI_FAILURE                              */
/* Called By          : cli_process_qos_cmd                                  */
/* Calling Function   : SNMP nmh Routines                                    */
/*****************************************************************************/
INT4
QoSCliNoSetPlyIf (tCliHandle CliHandle)
{
    UINT4               u4Idx = 0;
    INT4                i4RetStatus = SNMP_FAILURE;
    UINT4               u4ErrorCode = SNMP_ERR_NO_ERROR;

    /* Get the Policy Map Id */
    u4Idx = CLI_GET_PLY_MAP_ID ();

    i4RetStatus = nmhTestv2FsQoSPolicyMapStatus (&u4ErrorCode, u4Idx,
                                                 NOT_IN_SERVICE);
    if (i4RetStatus == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }
    i4RetStatus = nmhSetFsQoSPolicyMapStatus (u4Idx, NOT_IN_SERVICE);
    if (i4RetStatus == SNMP_FAILURE)
    {
        ISSCliCheckAndThrowFatalError (CliHandle);
        return (CLI_FAILURE);
    }
    i4RetStatus = nmhTestv2FsQoSPolicyMapIfIndex (&u4ErrorCode, u4Idx, 0);
    if (i4RetStatus == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    i4RetStatus = nmhSetFsQoSPolicyMapIfIndex (u4Idx, 0);
    if (i4RetStatus == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }

    /* Active the Row */
    i4RetStatus = nmhTestv2FsQoSPolicyMapStatus (&u4ErrorCode, u4Idx, ACTIVE);
    if (i4RetStatus == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    i4RetStatus = nmhSetFsQoSPolicyMapStatus (u4Idx, ACTIVE);
    if (i4RetStatus == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }
    return (CLI_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSCliSetPlyNoMeter                                  */
/* Description        : This function is used to Set Policy's Meter as NULL  */
/*                    : and set action as NULL                               */
/* Input(s)           : CliHandle    - Handle to CLI session                 */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : CLI_SUCCESS/CLI_FAILURE                              */
/* Called By          : cli_process_qos_cmd                                  */
/* Calling Function   : SNMP nmh Routines                                    */
/*****************************************************************************/
INT4
QoSCliSetPlyNoMeter (tCliHandle CliHandle)
{
    UINT4               u4Idx = 0;
    INT4                i4RetStatus = SNMP_FAILURE;
    UINT4               u4ErrorCode = SNMP_ERR_NO_ERROR;
    tSNMP_OCTET_STRING_TYPE *pOctStrActFlag = NULL;
    UINT1               u1InProConAct = 0;
    UINT1               u1InProExcAct = 0;
    UINT1               u1OutProAct = 0;
    tQoSPolicyMapNode  *pPlyMapNode = NULL;

    /* Get the Policy Map Id */
    u4Idx = CLI_GET_PLY_MAP_ID ();
    pPlyMapNode = QoSUtlGetPolicyMapNode (u4Idx);
    i4RetStatus = nmhTestv2FsQoSPolicyMapStatus (&u4ErrorCode, u4Idx,
                                                 NOT_IN_SERVICE);
    if (i4RetStatus == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }
    i4RetStatus = nmhSetFsQoSPolicyMapStatus (u4Idx, NOT_IN_SERVICE);
    if (i4RetStatus == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }

    /* To reset confirm action flag */
    if (pPlyMapNode->u1InProfConfActFlag != 0)
    {
        pOctStrActFlag = allocmem_octetstring (QOS_POLICY_ACTION_FLAG_LENGTH);
        if (pOctStrActFlag == NULL)
        {
            return (CLI_FAILURE);
        }
        MEMSET (pOctStrActFlag->pu1_OctetList, 0, pOctStrActFlag->i4_Length);
        MEMCPY (pOctStrActFlag->pu1_OctetList, &u1InProConAct,
                QOS_POLICY_ACTION_FLAG_LENGTH);

        i4RetStatus =
            nmhSetFsQoSPolicyMapInProfileConformActionFlag (u4Idx,
                                                            pOctStrActFlag);
        if (i4RetStatus == SNMP_FAILURE)
        {
            free_octetstring (pOctStrActFlag);
            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }
        free_octetstring (pOctStrActFlag);
    }

    /* to reset exceed action flag */
    if (pPlyMapNode->u1InProfExcActFlag != 0)
    {
        pOctStrActFlag = allocmem_octetstring (QOS_POLICY_ACTION_FLAG_LENGTH);
        if (pOctStrActFlag == NULL)
        {
            return (CLI_FAILURE);
        }
        MEMSET (pOctStrActFlag->pu1_OctetList, 0, pOctStrActFlag->i4_Length);
        MEMCPY (pOctStrActFlag->pu1_OctetList, &u1InProExcAct,
                QOS_POLICY_ACTION_FLAG_LENGTH);

        i4RetStatus =
            nmhSetFsQoSPolicyMapInProfileExceedActionFlag (u4Idx,
                                                           pOctStrActFlag);
        if (i4RetStatus == SNMP_FAILURE)
        {
            free_octetstring (pOctStrActFlag);
            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }

        free_octetstring (pOctStrActFlag);
    }

    /* to reset violate action flag */
    if (pPlyMapNode->u1OutProActionFlag != 0)
    {
        pOctStrActFlag = allocmem_octetstring (QOS_POLICY_ACTION_FLAG_LENGTH);
        if (pOctStrActFlag == NULL)
        {
            return (CLI_FAILURE);
        }
        MEMSET (pOctStrActFlag->pu1_OctetList, 0, pOctStrActFlag->i4_Length);
        MEMCPY (pOctStrActFlag->pu1_OctetList, &u1OutProAct,
                QOS_POLICY_ACTION_FLAG_LENGTH);

        i4RetStatus =
            nmhSetFsQoSPolicyMapOutProfileActionFlag (u4Idx, pOctStrActFlag);

        if (i4RetStatus == SNMP_FAILURE)
        {
            free_octetstring (pOctStrActFlag);
            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }

        free_octetstring (pOctStrActFlag);
    }

    /* to reset meter id */

    i4RetStatus = nmhTestv2FsQoSPolicyMapMeterTableId (&u4ErrorCode, u4Idx, 0);
    if (i4RetStatus == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    i4RetStatus = nmhSetFsQoSPolicyMapMeterTableId (u4Idx, 0);
    if (i4RetStatus == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }

    /* Active the Row */
    i4RetStatus = nmhTestv2FsQoSPolicyMapStatus (&u4ErrorCode, u4Idx, ACTIVE);
    if (i4RetStatus == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    i4RetStatus = nmhSetFsQoSPolicyMapStatus (u4Idx, ACTIVE);
    if (i4RetStatus == SNMP_FAILURE)
    {
        ISSCliCheckAndThrowFatalError (CliHandle);
        return (CLI_FAILURE);
    }
    return (CLI_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSCliRSPlyMapMeterAct                               */
/* Description        : This function is used to Reset Policy Map table Entry*/
/*                      Meter and Meter Actions.                             */
/* Input(s)           : u4Idx        - Table Index                           */
/*                    : u4MetIdx     - Meter Id                              */
/*                    : u1InProConAct- In profile Conf Action Flag           */
/*                    : u1InProExcAct- In profile Exc Action Flag            */
/*                    : u1OutProAct  - Out of profile Action Flag            */
/*                    : i4RowStatus  - RowStatus                             */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : CLI_SUCCESS/CLI_FAILURE                              */
/* Called By          : cli_process_qos_cmd                                  */
/* Calling Function   : SNMP nmh Routines                                    */
/*****************************************************************************/
VOID
QoSCliRSPlyMapMeterAct (UINT4 u4Idx, UINT4 u4MetIdx, UINT1 u1InProConAct,
                        UINT1 u1InProExcAct, UINT1 u1OutProAct,
                        tQoSCliRSMetAct * pMetAct, INT4 i4RowStatus)
{
    tSNMP_OCTET_STRING_TYPE *pOctStrActFlag = NULL;
    INT4                i4RetStatus = SNMP_FAILURE;

    if (u4MetIdx != (UINT4) CLI_RS_INVALID_MASK)
    {
        i4RetStatus = nmhSetFsQoSPolicyMapMeterTableId (u4Idx, u4MetIdx);
    }

    if (u1InProConAct != 0)
    {
        /* Allocate Memory For InActFlag */
        pOctStrActFlag = allocmem_octetstring (QOS_POLICY_ACTION_FLAG_LENGTH);
        if (pOctStrActFlag == NULL)
        {
            return;
        }
        MEMSET (pOctStrActFlag->pu1_OctetList, 0, pOctStrActFlag->i4_Length);
        MEMCPY (pOctStrActFlag->pu1_OctetList, &u1InProConAct,
                QOS_POLICY_ACTION_FLAG_LENGTH);

        i4RetStatus =
            nmhSetFsQoSPolicyMapInProfileConformActionFlag (u4Idx,
                                                            pOctStrActFlag);
        free_octetstring (pOctStrActFlag);

        /* Set the Valid Action */
        if (pMetAct->u4ConfVlanPri != (UINT4) CLI_RS_INVALID_MASK)
        {
            i4RetStatus =
                nmhSetFsQoSPolicyMapConformActionSetVlanPrio (u4Idx,
                                                              pMetAct->
                                                              u4ConfVlanPri);
        }

        if (pMetAct->u4ConfVlanDe != (UINT4) CLI_RS_INVALID_MASK)
        {
            i4RetStatus =
                nmhSetFsQoSPolicyMapConformActionSetVlanDE (u4Idx, pMetAct->
                                                            u4ConfVlanDe);
        }

        if (pMetAct->u4ConfIPDscp != (UINT4) CLI_RS_INVALID_MASK)
        {
            i4RetStatus =
                nmhSetFsQoSPolicyMapConformActionSetDscp (u4Idx, pMetAct->
                                                          u4ConfIPDscp);
        }

        if (pMetAct->u4ConfIPTos != (UINT4) CLI_RS_INVALID_MASK)
        {
            i4RetStatus =
                nmhSetFsQoSPolicyMapConformActionSetIpTOS (u4Idx, pMetAct->
                                                           u4ConfIPTos);
        }

        if (pMetAct->u4ConfMPLSExp != (UINT4) CLI_RS_INVALID_MASK)
        {
            i4RetStatus =
                nmhSetFsQoSPolicyMapConformActionSetMplsExp (u4Idx, pMetAct->
                                                             u4ConfMPLSExp);
        }
        if (pMetAct->u4ConfSetPort != (UINT4) CLI_RS_INVALID_MASK)
        {
            i4RetStatus =
                nmhSetFsQoSPolicyMapInProfileActionSetPort (u4Idx, pMetAct->
                                                            u4ConfSetPort);
        }

        if (pMetAct->u4ConfInVlanPri != (UINT4) CLI_RS_INVALID_MASK)
        {
            i4RetStatus =
                nmhSetFsQoSPolicyMapConformActionSetInnerVlanPrio (u4Idx,
                                                                   pMetAct->
                                                                   u4ConfInVlanPri);
        }
        if (pMetAct->u4ConfInVlanDE != (UINT4) CLI_RS_INVALID_MASK)
        {
            i4RetStatus =
                nmhSetFsQoSPolicyMapConformActionSetInnerVlanDE (u4Idx,
                                                                 pMetAct->
                                                                 u4ConfInVlanDE);
        }

    }

    if (u1InProExcAct != 0)
    {

        /* Allocate Memory For InActFlag */
        pOctStrActFlag = allocmem_octetstring (QOS_POLICY_ACTION_FLAG_LENGTH);
        if (pOctStrActFlag == NULL)
        {
            return;
        }
        MEMSET (pOctStrActFlag->pu1_OctetList, 0, pOctStrActFlag->i4_Length);
        MEMCPY (pOctStrActFlag->pu1_OctetList, &u1InProExcAct,
                QOS_POLICY_ACTION_FLAG_LENGTH);

        i4RetStatus =
            nmhSetFsQoSPolicyMapInProfileExceedActionFlag (u4Idx,
                                                           pOctStrActFlag);
        free_octetstring (pOctStrActFlag);

        /* Set the Valid Action  - Exceed */
        if (pMetAct->u4ExcVlanPri != (UINT4) CLI_RS_INVALID_MASK)
        {
            i4RetStatus =
                nmhSetFsQoSPolicyMapExceedActionSetVlanPrio (u4Idx, pMetAct->
                                                             u4ExcVlanPri);
        }

        if (pMetAct->u4ExcVlanDe != (UINT4) CLI_RS_INVALID_MASK)
        {
            i4RetStatus =
                nmhSetFsQoSPolicyMapExceedActionSetVlanDE (u4Idx, pMetAct->
                                                           u4ExcVlanDe);
        }

        if (pMetAct->u4ExcIPDscp != (UINT4) CLI_RS_INVALID_MASK)
        {
            i4RetStatus = nmhSetFsQoSPolicyMapExceedActionSetDscp (u4Idx,
                                                                   pMetAct->
                                                                   u4ExcIPDscp);
        }

        if (pMetAct->u4ExcIPTos != (UINT4) CLI_RS_INVALID_MASK)
        {
            i4RetStatus = nmhSetFsQoSPolicyMapExceedActionSetIpTOS (u4Idx,
                                                                    pMetAct->
                                                                    u4ExcIPTos);
        }

        if (pMetAct->u4ExcMPLSExp != (UINT4) CLI_RS_INVALID_MASK)
        {
            i4RetStatus =
                nmhSetFsQoSPolicyMapExceedActionSetMplsExp (u4Idx,
                                                            pMetAct->
                                                            u4ExcMPLSExp);
        }
        if (pMetAct->u4ExcInVlanPri != (UINT4) CLI_RS_INVALID_MASK)
        {
            i4RetStatus =
                nmhSetFsQoSPolicyMapExceedActionSetInnerVlanPrio
                (u4Idx, pMetAct->u4ExcInVlanPri);
        }
        if (pMetAct->u4ExcInVlanDE != (UINT4) CLI_RS_INVALID_MASK)
        {
            i4RetStatus =
                nmhSetFsQoSPolicyMapExceedActionSetInnerVlanDE
                (u4Idx, pMetAct->u4ExcInVlanDE);
        }

    }

    if (u1OutProAct != 0)
    {
        /* Allocate Memory For OutActFlag */
        pOctStrActFlag = allocmem_octetstring (QOS_POLICY_ACTION_FLAG_LENGTH);
        if (pOctStrActFlag == NULL)
        {
            return;
        }
        MEMSET (pOctStrActFlag->pu1_OctetList, 0, pOctStrActFlag->i4_Length);
        MEMCPY (pOctStrActFlag->pu1_OctetList, &u1OutProAct,
                QOS_POLICY_ACTION_FLAG_LENGTH);

        i4RetStatus = nmhSetFsQoSPolicyMapOutProfileActionFlag (u4Idx,
                                                                pOctStrActFlag);
        free_octetstring (pOctStrActFlag);

        /* Set the Valid Action */
        if (pMetAct->u4VioVlanPri != (UINT4) CLI_RS_INVALID_MASK)
        {
            i4RetStatus =
                nmhSetFsQoSPolicyMapOutProfileActionSetVlanPrio (u4Idx,
                                                                 pMetAct->
                                                                 u4VioVlanPri);
        }

        if (pMetAct->u4VioVlanDe != (UINT4) CLI_RS_INVALID_MASK)
        {
            i4RetStatus =
                nmhSetFsQoSPolicyMapOutProfileActionSetVlanDE (u4Idx, pMetAct->
                                                               u4VioVlanDe);
        }

        if (pMetAct->u4VioIPDscp != (UINT4) CLI_RS_INVALID_MASK)
        {
            i4RetStatus =
                nmhSetFsQoSPolicyMapOutProfileActionSetDscp (u4Idx, pMetAct->
                                                             u4VioIPDscp);
        }

        if (pMetAct->u4VioIPTos != (UINT4) CLI_RS_INVALID_MASK)
        {
            i4RetStatus =
                nmhSetFsQoSPolicyMapOutProfileActionSetIPTOS (u4Idx, pMetAct->
                                                              u4VioIPTos);
        }

        if (pMetAct->u4VioMPLSExp != (UINT4) CLI_RS_INVALID_MASK)
        {
            i4RetStatus =
                nmhSetFsQoSPolicyMapOutProfileActionSetMplsExp (u4Idx, pMetAct->
                                                                u4VioMPLSExp);
        }
        if (pMetAct->u4VioInVlanPri != (UINT4) CLI_RS_INVALID_MASK)
        {
            i4RetStatus =
                nmhSetFsQoSPolicyMapOutProfileActionSetInnerVlanPrio
                (u4Idx, pMetAct->u4VioInVlanPri);
        }
        if (pMetAct->u4VioInVlanDE != (UINT4) CLI_RS_INVALID_MASK)
        {
            i4RetStatus =
                nmhSetFsQoSPolicyMapOutProfileActionSetInnerVlanDE
                (u4Idx, pMetAct->u4VioInVlanDE);
        }

    }
    if (pMetAct->u4ConNClass != (UINT4) CLI_RS_INVALID_MASK)
    {
        i4RetStatus =
            nmhSetFsQoSPolicyMapConformActionSetNewCLASS (u4Idx, pMetAct->
                                                          u4ConNClass);
    }

    if (pMetAct->u4ExcNClass != (UINT4) CLI_RS_INVALID_MASK)
    {
        i4RetStatus =
            nmhSetFsQoSPolicyMapExceedActionSetNewCLASS (u4Idx, pMetAct->
                                                         u4ExcNClass);
    }
    if (pMetAct->u4VioNClass != (UINT4) CLI_RS_INVALID_MASK)
    {
        i4RetStatus =
            nmhSetFsQoSPolicyMapOutProfileActionSetNewCLASS (u4Idx,
                                                             pMetAct->
                                                             u4VioNClass);
    }

    i4RetStatus = nmhSetFsQoSPolicyMapStatus (u4Idx, i4RowStatus);

    UNUSED_PARAM (i4RetStatus);

}

/*****************************************************************************/
/* Function Name      : QoSCliSetPlyMapMeterAct                              */
/* Description        : This function is used to Set Policy Map table Entry  */
/*                      Meter and Meter Actions.                             */
/* Input(s)           : CliHandle    - Handle to CLI session                 */
/*                    : pu1MetName   - Meter name                            */
/*                    : u1InProConAct- In profile Conf Action Flag           */
/*                    : u1InProExcAct- In profile Exc Action Flag            */
/*                    : u1OutProAct  - Out of profile Action Flag            */
/*                    : pMetAct      - Ptr to tQoSCliMeterAction             */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : CLI_SUCCESS/CLI_FAILURE                              */
/* Called By          : cli_process_qos_cmd                                  */
/* Calling Function   : SNMP nmh Routines                                    */
/*****************************************************************************/
INT4
QoSCliSetPlyMapMeterAct (tCliHandle CliHandle, UINT4 u4MetIdx,
                         UINT1 u1InProConAct, UINT1 u1InProExcAct,
                         UINT1 u1OutProAct, tQoSCliMeterAction * pMetAct)
{
    tQoSPolicyMapNode  *pPlyMapNode = NULL;
    tQoSCliRSMetAct     RSMetAct;
    tSNMP_OCTET_STRING_TYPE *pOctStrActFlag = NULL;
    UINT4               u4Idx = 0;
    INT4                i4RetStatus = SNMP_FAILURE;
    INT4                i4RowStatus = SNMP_FAILURE;
    UINT4               u4ErrorCode = SNMP_ERR_NO_ERROR;
    UINT4               u4CurMetIdx = (UINT4) CLI_RS_INVALID_MASK;
    UINT1               u1CurInProConAct = 0;
    UINT1               u1CurInProExcAct = 0;
    UINT1               u1CurOutProAct = 0;

    /* To set all bits as 1 */
    MEMSET (&(RSMetAct), 0xFF, (sizeof (tQoSCliMeterAction)));

    /* Get the Policy Map Id */
    u4Idx = CLI_GET_PLY_MAP_ID ();
    i4RetStatus = nmhGetFsQoSPolicyMapStatus (u4Idx, &i4RowStatus);
    if (i4RetStatus == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    /* if RowStatus = NOT_IN_SERVICE or NOT_READY test and set the Values.
     * if RowStatus = ACTIVE Set the RowStatus as NOT_IN_SERVICE and than
     * test and set the Values
     */
    if (i4RowStatus == ACTIVE)
    {
        i4RetStatus = nmhTestv2FsQoSPolicyMapStatus (&u4ErrorCode,
                                                     u4Idx, NOT_IN_SERVICE);
        if (i4RetStatus == SNMP_FAILURE)
        {
            return (CLI_FAILURE);
        }

        i4RetStatus = nmhSetFsQoSPolicyMapStatus (u4Idx, NOT_IN_SERVICE);
        if (i4RetStatus == SNMP_FAILURE)
        {
            ISSCliCheckAndThrowFatalError (CliHandle);
            return (CLI_FAILURE);
        }
        i4RowStatus = NOT_IN_SERVICE;
    }

    pPlyMapNode = QoSUtlGetPolicyMapNode (u4Idx);
    if (pPlyMapNode != NULL)
    {
        if (pPlyMapNode->u4MeterTableId != 0)
        {
            QoSResetPlyMap (pPlyMapNode);

            i4RetStatus = nmhSetFsQoSPolicyMapMeterTableId (u4Idx, 0);
            if (i4RetStatus == SNMP_FAILURE)
            {
                CLI_FATAL_ERROR (CliHandle);
                return (CLI_FAILURE);
            }
        }
    }

    /*Set Meter */
    i4RetStatus = nmhTestv2FsQoSPolicyMapMeterTableId (&u4ErrorCode,
                                                       u4Idx, u4MetIdx);
    if (i4RetStatus == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    i4RetStatus = nmhGetFsQoSPolicyMapMeterTableId (u4Idx, &u4CurMetIdx);
    if (i4RetStatus == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    i4RetStatus = nmhSetFsQoSPolicyMapMeterTableId (u4Idx, u4MetIdx);
    if (i4RetStatus == SNMP_FAILURE)
    {
        ISSCliCheckAndThrowFatalError (CliHandle);
        return (CLI_FAILURE);
    }

    if (pMetAct->i4ConNClass != -1)
    {
        i4RetStatus =
            nmhTestv2FsQoSPolicyMapConformActionSetNewCLASS (&u4ErrorCode,
                                                             u4Idx,
                                                             pMetAct->
                                                             i4ConNClass);
        if (i4RetStatus == SNMP_FAILURE)
        {
            QoSCliRSPlyMapMeterAct (u4Idx, u4CurMetIdx, u1CurInProConAct,
                                    u1CurInProExcAct, u1CurOutProAct,
                                    &(RSMetAct), i4RowStatus);
            return (CLI_FAILURE);
        }

        i4RetStatus =
            nmhGetFsQoSPolicyMapConformActionSetNewCLASS (u4Idx,
                                                          &(RSMetAct.
                                                            u4ConNClass));
        if (i4RetStatus == SNMP_FAILURE)
        {
            return (CLI_FAILURE);
        }

        i4RetStatus =
            nmhSetFsQoSPolicyMapConformActionSetNewCLASS (u4Idx,
                                                          pMetAct->i4ConNClass);
        if (i4RetStatus == SNMP_FAILURE)
        {
            QoSCliRSPlyMapMeterAct (u4Idx, u4CurMetIdx, u1CurInProConAct,
                                    u1CurInProExcAct, u1CurOutProAct,
                                    &(RSMetAct), i4RowStatus);
            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }

    }
    if (pMetAct->i4ExcNClass != -1)
    {

        if ((u1InProExcAct & QOS_CLI_IN_EXC_ACT_DROP) ==
            QOS_CLI_IN_EXC_ACT_DROP)
        {
            CLI_SET_ERR (QOS_CLI_ERR_DROP_FLG);
            return (CLI_FAILURE);
        }
        i4RetStatus =
            nmhTestv2FsQoSPolicyMapExceedActionSetNewCLASS (&u4ErrorCode,
                                                            u4Idx,
                                                            pMetAct->
                                                            i4ExcNClass);
        if (i4RetStatus == SNMP_FAILURE)
        {
            QoSCliRSPlyMapMeterAct (u4Idx, u4CurMetIdx, u1CurInProConAct,
                                    u1CurInProExcAct, u1CurOutProAct,
                                    &(RSMetAct), i4RowStatus);
            return (CLI_FAILURE);
        }

        i4RetStatus =
            nmhGetFsQoSPolicyMapExceedActionSetNewCLASS (u4Idx,
                                                         &(RSMetAct.
                                                           u4ExcNClass));
        if (i4RetStatus == SNMP_FAILURE)
        {
            return (CLI_FAILURE);
        }

        i4RetStatus =
            nmhSetFsQoSPolicyMapExceedActionSetNewCLASS (u4Idx,
                                                         pMetAct->i4ExcNClass);
        if (i4RetStatus == SNMP_FAILURE)
        {
            QoSCliRSPlyMapMeterAct (u4Idx, u4CurMetIdx, u1CurInProConAct,
                                    u1CurInProExcAct, u1CurOutProAct,
                                    &(RSMetAct), i4RowStatus);
            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }

    }
    if (pMetAct->i4VioNClass != -1)
    {
        if ((u1OutProAct & QOS_CLI_OUT_ACT_DROP) == QOS_CLI_OUT_ACT_DROP)
        {
            CLI_SET_ERR (QOS_CLI_ERR_DROP_FLG);
            return (CLI_FAILURE);
        }

        i4RetStatus =
            nmhTestv2FsQoSPolicyMapOutProfileActionSetNewCLASS (&u4ErrorCode,
                                                                u4Idx,
                                                                pMetAct->
                                                                i4VioNClass);
        if (i4RetStatus == SNMP_FAILURE)
        {
            QoSCliRSPlyMapMeterAct (u4Idx, u4CurMetIdx, u1CurInProConAct,
                                    u1CurInProExcAct, u1CurOutProAct,
                                    &(RSMetAct), i4RowStatus);
            return (CLI_FAILURE);
        }

        i4RetStatus =
            nmhGetFsQoSPolicyMapOutProfileActionSetNewCLASS (u4Idx,
                                                             &(RSMetAct.
                                                               u4VioNClass));
        if (i4RetStatus == SNMP_FAILURE)
        {
            return (CLI_FAILURE);
        }

        i4RetStatus =
            nmhSetFsQoSPolicyMapOutProfileActionSetNewCLASS (u4Idx,
                                                             pMetAct->
                                                             i4VioNClass);
        if (i4RetStatus == SNMP_FAILURE)
        {
            QoSCliRSPlyMapMeterAct (u4Idx, u4CurMetIdx, u1CurInProConAct,
                                    u1CurInProExcAct, u1CurOutProAct,
                                    &(RSMetAct), i4RowStatus);
            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }

    }

    if (u1InProConAct != 0)
    {
        /* Allocate Memory For InActFlag */
        pOctStrActFlag = allocmem_octetstring (QOS_POLICY_ACTION_FLAG_LENGTH);
        if (pOctStrActFlag == NULL)
        {
            QoSCliRSPlyMapMeterAct (u4Idx, u4CurMetIdx, u1CurInProConAct,
                                    u1CurInProExcAct, u1CurOutProAct,
                                    &(RSMetAct), i4RowStatus);
            return (CLI_FAILURE);
        }

        MEMSET (pOctStrActFlag->pu1_OctetList, 0, pOctStrActFlag->i4_Length);

        i4RetStatus =
            nmhGetFsQoSPolicyMapInProfileConformActionFlag (u4Idx,
                                                            pOctStrActFlag);
        if (i4RetStatus == SNMP_FAILURE)
        {
            free_octetstring (pOctStrActFlag);
            return (CLI_FAILURE);
        }
        /* Cpy Curr Act Flag to */
        MEMCPY (&u1CurInProConAct, pOctStrActFlag->pu1_OctetList,
                QOS_POLICY_ACTION_FLAG_LENGTH);

        MEMCPY (pOctStrActFlag->pu1_OctetList, &u1InProConAct,
                QOS_POLICY_ACTION_FLAG_LENGTH);

        i4RetStatus =
            nmhTestv2FsQoSPolicyMapInProfileConformActionFlag (&u4ErrorCode,
                                                               u4Idx,
                                                               pOctStrActFlag);
        if (i4RetStatus == SNMP_FAILURE)
        {
            u1CurInProConAct = 0;

            QoSCliRSPlyMapMeterAct (u4Idx, u4CurMetIdx, u1CurInProConAct,
                                    u1CurInProExcAct, u1CurOutProAct,
                                    &(RSMetAct), i4RowStatus);
            free_octetstring (pOctStrActFlag);
            return (CLI_FAILURE);
        }

        i4RetStatus =
            nmhSetFsQoSPolicyMapInProfileConformActionFlag (u4Idx,
                                                            pOctStrActFlag);
        if (i4RetStatus == SNMP_FAILURE)
        {
            QoSCliRSPlyMapMeterAct (u4Idx, u4CurMetIdx, u1CurInProConAct,
                                    u1CurInProExcAct, u1CurOutProAct,
                                    &(RSMetAct), i4RowStatus);
            free_octetstring (pOctStrActFlag);
            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }

        free_octetstring (pOctStrActFlag);

        /* Set the Valid Action */
        if (pMetAct->i1ConfVlanDe != -1)
        {
            i4RetStatus =
                nmhTestv2FsQoSPolicyMapConformActionSetVlanDE (&u4ErrorCode,
                                                               u4Idx,
                                                               pMetAct->
                                                               i1ConfVlanDe);
            if (i4RetStatus == SNMP_FAILURE)
            {
                QoSCliRSPlyMapMeterAct (u4Idx, u4CurMetIdx, u1CurInProConAct,
                                        u1CurInProExcAct, u1CurOutProAct,
                                        &(RSMetAct), i4RowStatus);
                return (CLI_FAILURE);
            }

            i4RetStatus =
                nmhGetFsQoSPolicyMapConformActionSetVlanDE (u4Idx,
                                                            &(RSMetAct.
                                                              u4ConfVlanDe));
            if (i4RetStatus == SNMP_FAILURE)
            {
                return (CLI_FAILURE);
            }

            i4RetStatus =
                nmhSetFsQoSPolicyMapConformActionSetVlanDE (u4Idx, pMetAct->
                                                            i1ConfVlanDe);
            if (i4RetStatus == SNMP_FAILURE)
            {
                QoSCliRSPlyMapMeterAct (u4Idx, u4CurMetIdx, u1CurInProConAct,
                                        u1CurInProExcAct, u1CurOutProAct,
                                        &(RSMetAct), i4RowStatus);
                CLI_FATAL_ERROR (CliHandle);
                return (CLI_FAILURE);
            }

        }

        if (pMetAct->i1ConfVlanPri != -1)
        {
            i4RetStatus =
                nmhTestv2FsQoSPolicyMapConformActionSetVlanPrio (&u4ErrorCode,
                                                                 u4Idx,
                                                                 pMetAct->
                                                                 i1ConfVlanPri);
            if (i4RetStatus == SNMP_FAILURE)
            {
                QoSCliRSPlyMapMeterAct (u4Idx, u4CurMetIdx, u1CurInProConAct,
                                        u1CurInProExcAct, u1CurOutProAct,
                                        &(RSMetAct), i4RowStatus);
                return (CLI_FAILURE);
            }

            i4RetStatus =
                nmhGetFsQoSPolicyMapConformActionSetVlanPrio (u4Idx,
                                                              &(RSMetAct.
                                                                u4ConfVlanPri));
            if (i4RetStatus == SNMP_FAILURE)
            {
                return (CLI_FAILURE);
            }

            i4RetStatus =
                nmhSetFsQoSPolicyMapConformActionSetVlanPrio (u4Idx,
                                                              pMetAct->
                                                              i1ConfVlanPri);
            if (i4RetStatus == SNMP_FAILURE)
            {
                QoSCliRSPlyMapMeterAct (u4Idx, u4CurMetIdx, u1CurInProConAct,
                                        u1CurInProExcAct, u1CurOutProAct,
                                        &(RSMetAct), i4RowStatus);
                CLI_FATAL_ERROR (CliHandle);
                return (CLI_FAILURE);
            }

        }

        if (pMetAct->i1ConfIPDscp != -1)
        {
            i4RetStatus =
                nmhTestv2FsQoSPolicyMapConformActionSetDscp (&u4ErrorCode,
                                                             u4Idx, pMetAct->
                                                             i1ConfIPDscp);
            if (i4RetStatus == SNMP_FAILURE)
            {
                QoSCliRSPlyMapMeterAct (u4Idx, u4CurMetIdx, u1CurInProConAct,
                                        u1CurInProExcAct, u1CurOutProAct,
                                        &(RSMetAct), i4RowStatus);
                return (CLI_FAILURE);
            }

            i4RetStatus =
                nmhGetFsQoSPolicyMapConformActionSetDscp
                (u4Idx, (INT4 *) &(RSMetAct.u4ConfIPDscp));
            if (i4RetStatus == SNMP_FAILURE)
            {
                return (CLI_FAILURE);
            }

            i4RetStatus =
                nmhSetFsQoSPolicyMapConformActionSetDscp (u4Idx, pMetAct->
                                                          i1ConfIPDscp);
            if (i4RetStatus == SNMP_FAILURE)
            {
                QoSCliRSPlyMapMeterAct (u4Idx, u4CurMetIdx, u1CurInProConAct,
                                        u1CurInProExcAct, u1CurOutProAct,
                                        &(RSMetAct), i4RowStatus);
                CLI_FATAL_ERROR (CliHandle);
                return (CLI_FAILURE);
            }

        }
        if (pMetAct->i1ConfSetPort != -1)
        {
            i4RetStatus =
                nmhTestv2FsQoSPolicyMapInProfileActionSetPort (&u4ErrorCode,
                                                               u4Idx, pMetAct->
                                                               i1ConfSetPort);
            if (i4RetStatus == SNMP_FAILURE)
            {
                QoSCliRSPlyMapMeterAct (u4Idx, u4CurMetIdx, u1CurInProConAct,
                                        u1CurInProExcAct, u1CurOutProAct,
                                        &(RSMetAct), i4RowStatus);
                return (CLI_FAILURE);
            }

            i4RetStatus =
                nmhGetFsQoSPolicyMapInProfileActionSetPort (u4Idx,
                                                            &(RSMetAct.
                                                              u4ConfSetPort));
            if (i4RetStatus == SNMP_FAILURE)
            {
                return (CLI_FAILURE);
            }

            i4RetStatus =
                nmhSetFsQoSPolicyMapInProfileActionSetPort (u4Idx, pMetAct->
                                                            i1ConfSetPort);
            if (i4RetStatus == SNMP_FAILURE)
            {
                QoSCliRSPlyMapMeterAct (u4Idx, u4CurMetIdx, u1CurInProConAct,
                                        u1CurInProExcAct, u1CurOutProAct,
                                        &(RSMetAct), i4RowStatus);
                CLI_FATAL_ERROR (CliHandle);
                return (CLI_FAILURE);
            }

        }
        if (pMetAct->i1ConfInVlanPri != -1)
        {
            i4RetStatus =
                nmhTestv2FsQoSPolicyMapConformActionSetInnerVlanPrio
                (&u4ErrorCode, u4Idx, pMetAct->i1ConfInVlanPri);
            if (i4RetStatus == SNMP_FAILURE)
            {
                QoSCliRSPlyMapMeterAct (u4Idx, u4CurMetIdx, u1CurInProConAct,
                                        u1CurInProExcAct, u1CurOutProAct,
                                        &(RSMetAct), i4RowStatus);
                return (CLI_FAILURE);
            }

            i4RetStatus =
                nmhGetFsQoSPolicyMapConformActionSetInnerVlanPrio
                (u4Idx, &(RSMetAct.u4ConfInVlanPri));
            if (i4RetStatus == SNMP_FAILURE)
            {
                return (CLI_FAILURE);
            }

            i4RetStatus =
                nmhSetFsQoSPolicyMapConformActionSetInnerVlanPrio
                (u4Idx, pMetAct->i1ConfInVlanPri);
            if (i4RetStatus == SNMP_FAILURE)
            {
                QoSCliRSPlyMapMeterAct (u4Idx, u4CurMetIdx, u1CurInProConAct,
                                        u1CurInProExcAct, u1CurOutProAct,
                                        &(RSMetAct), i4RowStatus);
                CLI_FATAL_ERROR (CliHandle);
                return (CLI_FAILURE);
            }

        }

        if (pMetAct->i1ConfIPTos != -1)
        {
            i4RetStatus =
                nmhTestv2FsQoSPolicyMapConformActionSetIpTOS (&u4ErrorCode,
                                                              u4Idx, pMetAct->
                                                              i1ConfIPTos);
            if (i4RetStatus == SNMP_FAILURE)
            {
                QoSCliRSPlyMapMeterAct (u4Idx, u4CurMetIdx, u1CurInProConAct,
                                        u1CurInProExcAct, u1CurOutProAct,
                                        &(RSMetAct), i4RowStatus);
                return (CLI_FAILURE);
            }

            i4RetStatus =
                nmhGetFsQoSPolicyMapConformActionSetIpTOS
                (u4Idx, &(RSMetAct.u4ConfIPTos));
            if (i4RetStatus == SNMP_FAILURE)
            {
                return (CLI_FAILURE);
            }

            i4RetStatus =
                nmhSetFsQoSPolicyMapConformActionSetIpTOS (u4Idx, pMetAct->
                                                           i1ConfIPTos);
            if (i4RetStatus == SNMP_FAILURE)
            {
                QoSCliRSPlyMapMeterAct (u4Idx, u4CurMetIdx, u1CurInProConAct,
                                        u1CurInProExcAct, u1CurOutProAct,
                                        &(RSMetAct), i4RowStatus);
                CLI_FATAL_ERROR (CliHandle);
                return (CLI_FAILURE);
            }

        }

        if (pMetAct->i1ConfMPLSExp != -1)
        {
            i4RetStatus =
                nmhTestv2FsQoSPolicyMapConformActionSetMplsExp (&u4ErrorCode,
                                                                u4Idx, pMetAct->
                                                                i1ConfMPLSExp);
            if (i4RetStatus == SNMP_FAILURE)
            {
                QoSCliRSPlyMapMeterAct (u4Idx, u4CurMetIdx, u1CurInProConAct,
                                        u1CurInProExcAct, u1CurOutProAct,
                                        &(RSMetAct), i4RowStatus);
                return (CLI_FAILURE);
            }

            i4RetStatus =
                nmhGetFsQoSPolicyMapConformActionSetMplsExp
                (u4Idx, &(RSMetAct.u4ConfMPLSExp));
            if (i4RetStatus == SNMP_FAILURE)
            {
                CLI_FATAL_ERROR (CliHandle);
                return (CLI_FAILURE);
            }

            i4RetStatus =
                nmhSetFsQoSPolicyMapConformActionSetMplsExp (u4Idx, pMetAct->
                                                             i1ConfMPLSExp);
            if (i4RetStatus == SNMP_FAILURE)
            {
                QoSCliRSPlyMapMeterAct (u4Idx, u4CurMetIdx, u1CurInProConAct,
                                        u1CurInProExcAct, u1CurOutProAct,
                                        &(RSMetAct), i4RowStatus);
                CLI_FATAL_ERROR (CliHandle);
                return (CLI_FAILURE);
            }

        }

        if (pMetAct->i1ConfInVlanDE != -1)
        {
            i4RetStatus =
                nmhTestv2FsQoSPolicyMapConformActionSetInnerVlanDE
                (&u4ErrorCode, u4Idx, pMetAct->i1ConfInVlanDE);
            if (i4RetStatus == SNMP_FAILURE)
            {
                QoSCliRSPlyMapMeterAct (u4Idx, u4CurMetIdx, u1CurInProConAct,
                                        u1CurInProExcAct, u1CurOutProAct,
                                        &(RSMetAct), i4RowStatus);
                return (CLI_FAILURE);
            }

            i4RetStatus =
                nmhGetFsQoSPolicyMapConformActionSetInnerVlanDE
                (u4Idx, &(RSMetAct.u4ConfInVlanDE));
            if (i4RetStatus == SNMP_FAILURE)
            {
                return (CLI_FAILURE);
            }

            i4RetStatus =
                nmhSetFsQoSPolicyMapConformActionSetInnerVlanDE
                (u4Idx, pMetAct->i1ConfInVlanDE);
            if (i4RetStatus == SNMP_FAILURE)
            {
                QoSCliRSPlyMapMeterAct (u4Idx, u4CurMetIdx, u1CurInProConAct,
                                        u1CurInProExcAct, u1CurOutProAct,
                                        &(RSMetAct), i4RowStatus);
                CLI_FATAL_ERROR (CliHandle);
                return (CLI_FAILURE);
            }

        }

    }

    if (u1InProExcAct != 0)
    {

        /* Allocate Memory For InActFlag */
        pOctStrActFlag = allocmem_octetstring (QOS_POLICY_ACTION_FLAG_LENGTH);
        if (pOctStrActFlag == NULL)
        {
            QoSCliRSPlyMapMeterAct (u4Idx, u4CurMetIdx, u1CurInProConAct,
                                    u1CurInProExcAct, u1CurOutProAct,
                                    &(RSMetAct), i4RowStatus);
            return (CLI_FAILURE);
        }
        MEMSET (pOctStrActFlag->pu1_OctetList, 0, pOctStrActFlag->i4_Length);

        i4RetStatus =
            nmhGetFsQoSPolicyMapInProfileExceedActionFlag (u4Idx,
                                                           pOctStrActFlag);
        if (i4RetStatus == SNMP_FAILURE)
        {
            free_octetstring (pOctStrActFlag);
            return (CLI_FAILURE);
        }
        /* Cpy Curr Act Flag to */
        MEMCPY (&u1CurInProExcAct, pOctStrActFlag->pu1_OctetList,
                QOS_POLICY_ACTION_FLAG_LENGTH);

        MEMCPY (pOctStrActFlag->pu1_OctetList, &u1InProExcAct,
                QOS_POLICY_ACTION_FLAG_LENGTH);

        i4RetStatus =
            nmhTestv2FsQoSPolicyMapInProfileExceedActionFlag (&u4ErrorCode,
                                                              u4Idx,
                                                              pOctStrActFlag);
        if (i4RetStatus == SNMP_FAILURE)
        {
            u1CurInProExcAct = 0;
            QoSCliRSPlyMapMeterAct (u4Idx, u4CurMetIdx, u1CurInProConAct,
                                    u1CurInProExcAct, u1CurOutProAct,
                                    &(RSMetAct), i4RowStatus);
            free_octetstring (pOctStrActFlag);
            return (CLI_FAILURE);
        }

        i4RetStatus =
            nmhSetFsQoSPolicyMapInProfileExceedActionFlag (u4Idx,
                                                           pOctStrActFlag);
        if (i4RetStatus == SNMP_FAILURE)
        {
            QoSCliRSPlyMapMeterAct (u4Idx, u4CurMetIdx, u1CurInProConAct,
                                    u1CurInProExcAct, u1CurOutProAct,
                                    &(RSMetAct), i4RowStatus);
            free_octetstring (pOctStrActFlag);
            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }

        free_octetstring (pOctStrActFlag);

        /* Set the Valid Action  - Exceed */
        if (pMetAct->i1ExcVlanDe != -1)
        {
            i4RetStatus =
                nmhTestv2FsQoSPolicyMapExceedActionSetVlanDE (&u4ErrorCode,
                                                              u4Idx,
                                                              pMetAct->
                                                              i1ExcVlanDe);
            if (i4RetStatus == SNMP_FAILURE)
            {
                QoSCliRSPlyMapMeterAct (u4Idx, u4CurMetIdx, u1CurInProConAct,
                                        u1CurInProExcAct, u1CurOutProAct,
                                        &(RSMetAct), i4RowStatus);
                return (CLI_FAILURE);
            }

            i4RetStatus =
                nmhGetFsQoSPolicyMapExceedActionSetVlanDE
                (u4Idx, &(RSMetAct.u4ExcVlanDe));
            if (i4RetStatus == SNMP_FAILURE)
            {
                return (CLI_FAILURE);
            }

            i4RetStatus =
                nmhSetFsQoSPolicyMapExceedActionSetVlanDE (u4Idx, pMetAct->
                                                           i1ExcVlanDe);
            if (i4RetStatus == SNMP_FAILURE)
            {
                QoSCliRSPlyMapMeterAct (u4Idx, u4CurMetIdx, u1CurInProConAct,
                                        u1CurInProExcAct, u1CurOutProAct,
                                        &(RSMetAct), i4RowStatus);
                CLI_FATAL_ERROR (CliHandle);
                return (CLI_FAILURE);
            }

        }
        if (pMetAct->i1ExcVlanPri != -1)
        {
            i4RetStatus =
                nmhTestv2FsQoSPolicyMapExceedActionSetVlanPrio (&u4ErrorCode,
                                                                u4Idx,
                                                                pMetAct->
                                                                i1ExcVlanPri);
            if (i4RetStatus == SNMP_FAILURE)
            {
                QoSCliRSPlyMapMeterAct (u4Idx, u4CurMetIdx, u1CurInProConAct,
                                        u1CurInProExcAct, u1CurOutProAct,
                                        &(RSMetAct), i4RowStatus);
                return (CLI_FAILURE);
            }

            i4RetStatus =
                nmhGetFsQoSPolicyMapExceedActionSetVlanPrio
                (u4Idx, &(RSMetAct.u4ExcVlanPri));
            if (i4RetStatus == SNMP_FAILURE)
            {
                return (CLI_FAILURE);
            }

            i4RetStatus =
                nmhSetFsQoSPolicyMapExceedActionSetVlanPrio (u4Idx,
                                                             pMetAct->
                                                             i1ExcVlanPri);
            if (i4RetStatus == SNMP_FAILURE)
            {
                QoSCliRSPlyMapMeterAct (u4Idx, u4CurMetIdx, u1CurInProConAct,
                                        u1CurInProExcAct, u1CurOutProAct,
                                        &(RSMetAct), i4RowStatus);
                CLI_FATAL_ERROR (CliHandle);
                return (CLI_FAILURE);
            }

        }

        if (pMetAct->i1ExcInVlanPri != -1)
        {
            i4RetStatus =
                nmhTestv2FsQoSPolicyMapExceedActionSetInnerVlanPrio
                (&u4ErrorCode, u4Idx, pMetAct->i1ExcInVlanPri);
            if (i4RetStatus == SNMP_FAILURE)
            {
                QoSCliRSPlyMapMeterAct (u4Idx, u4CurMetIdx, u1CurInProConAct,
                                        u1CurInProExcAct, u1CurOutProAct,
                                        &(RSMetAct), i4RowStatus);
                return (CLI_FAILURE);
            }

            i4RetStatus =
                nmhGetFsQoSPolicyMapExceedActionSetInnerVlanPrio
                (u4Idx, &(RSMetAct.u4ExcInVlanPri));
            if (i4RetStatus == SNMP_FAILURE)
            {
                return (CLI_FAILURE);
            }

            i4RetStatus =
                nmhSetFsQoSPolicyMapExceedActionSetInnerVlanPrio
                (u4Idx, pMetAct->i1ExcInVlanPri);
            if (i4RetStatus == SNMP_FAILURE)
            {
                QoSCliRSPlyMapMeterAct (u4Idx, u4CurMetIdx, u1CurInProConAct,
                                        u1CurInProExcAct, u1CurOutProAct,
                                        &(RSMetAct), i4RowStatus);
                CLI_FATAL_ERROR (CliHandle);
                return (CLI_FAILURE);
            }

        }

        if (pMetAct->i1ExcIPDscp != -1)
        {
            i4RetStatus =
                nmhTestv2FsQoSPolicyMapExceedActionSetDscp (&u4ErrorCode,
                                                            u4Idx,
                                                            pMetAct->
                                                            i1ExcIPDscp);
            if (i4RetStatus == SNMP_FAILURE)
            {
                QoSCliRSPlyMapMeterAct (u4Idx, u4CurMetIdx, u1CurInProConAct,
                                        u1CurInProExcAct, u1CurOutProAct,
                                        &(RSMetAct), i4RowStatus);
                return (CLI_FAILURE);
            }

            i4RetStatus =
                nmhGetFsQoSPolicyMapExceedActionSetDscp
                (u4Idx, (INT4 *) &(RSMetAct.u4ExcIPDscp));
            if (i4RetStatus == SNMP_FAILURE)
            {
                return (CLI_FAILURE);
            }

            i4RetStatus = nmhSetFsQoSPolicyMapExceedActionSetDscp (u4Idx,
                                                                   pMetAct->
                                                                   i1ExcIPDscp);
            if (i4RetStatus == SNMP_FAILURE)
            {
                QoSCliRSPlyMapMeterAct (u4Idx, u4CurMetIdx, u1CurInProConAct,
                                        u1CurInProExcAct, u1CurOutProAct,
                                        &(RSMetAct), i4RowStatus);
                CLI_FATAL_ERROR (CliHandle);
                return (CLI_FAILURE);
            }

        }

        if (pMetAct->i1ExcIPTos != -1)
        {
            i4RetStatus =
                nmhTestv2FsQoSPolicyMapExceedActionSetIpTOS (&u4ErrorCode,
                                                             u4Idx,
                                                             pMetAct->
                                                             i1ExcIPTos);
            if (i4RetStatus == SNMP_FAILURE)
            {
                QoSCliRSPlyMapMeterAct (u4Idx, u4CurMetIdx, u1CurInProConAct,
                                        u1CurInProExcAct, u1CurOutProAct,
                                        &(RSMetAct), i4RowStatus);
                return (CLI_FAILURE);
            }

            i4RetStatus =
                nmhGetFsQoSPolicyMapExceedActionSetIpTOS (u4Idx, &(RSMetAct.
                                                                   u4ExcIPTos));
            if (i4RetStatus == SNMP_FAILURE)
            {
                return (CLI_FAILURE);
            }

            i4RetStatus = nmhSetFsQoSPolicyMapExceedActionSetIpTOS (u4Idx,
                                                                    pMetAct->
                                                                    i1ExcIPTos);
            if (i4RetStatus == SNMP_FAILURE)
            {
                QoSCliRSPlyMapMeterAct (u4Idx, u4CurMetIdx, u1CurInProConAct,
                                        u1CurInProExcAct, u1CurOutProAct,
                                        &(RSMetAct), i4RowStatus);
                CLI_FATAL_ERROR (CliHandle);
                return (CLI_FAILURE);
            }

        }

        if (pMetAct->i1ExcMPLSExp != -1)
        {
            i4RetStatus =
                nmhTestv2FsQoSPolicyMapExceedActionSetMplsExp (&u4ErrorCode,
                                                               u4Idx,
                                                               pMetAct->
                                                               i1ExcMPLSExp);
            if (i4RetStatus == SNMP_FAILURE)
            {
                QoSCliRSPlyMapMeterAct (u4Idx, u4CurMetIdx, u1CurInProConAct,
                                        u1CurInProExcAct, u1CurOutProAct,
                                        &(RSMetAct), i4RowStatus);
                return (CLI_FAILURE);
            }

            i4RetStatus =
                nmhGetFsQoSPolicyMapExceedActionSetMplsExp
                (u4Idx, &(RSMetAct.u4ExcMPLSExp));
            if (i4RetStatus == SNMP_FAILURE)
            {
                return (CLI_FAILURE);
            }

            i4RetStatus =
                nmhSetFsQoSPolicyMapExceedActionSetMplsExp (u4Idx, pMetAct->
                                                            i1ExcMPLSExp);
            if (i4RetStatus == SNMP_FAILURE)
            {
                QoSCliRSPlyMapMeterAct (u4Idx, u4CurMetIdx, u1CurInProConAct,
                                        u1CurInProExcAct, u1CurOutProAct,
                                        &(RSMetAct), i4RowStatus);
                CLI_FATAL_ERROR (CliHandle);
                return (CLI_FAILURE);
            }

        }
        if (pMetAct->i1ExcInVlanDE != -1)
        {
            i4RetStatus =
                nmhTestv2FsQoSPolicyMapExceedActionSetInnerVlanDE
                (&u4ErrorCode, u4Idx, pMetAct->i1ExcInVlanDE);
            if (i4RetStatus == SNMP_FAILURE)
            {
                QoSCliRSPlyMapMeterAct (u4Idx, u4CurMetIdx, u1CurInProConAct,
                                        u1CurInProExcAct, u1CurOutProAct,
                                        &(RSMetAct), i4RowStatus);
                return (CLI_FAILURE);
            }

            i4RetStatus =
                nmhGetFsQoSPolicyMapExceedActionSetInnerVlanDE
                (u4Idx, &(RSMetAct.u4ExcInVlanDE));
            if (i4RetStatus == SNMP_FAILURE)
            {
                return (CLI_FAILURE);
            }

            i4RetStatus =
                nmhSetFsQoSPolicyMapExceedActionSetInnerVlanDE
                (u4Idx, pMetAct->i1ExcInVlanDE);
            if (i4RetStatus == SNMP_FAILURE)
            {
                QoSCliRSPlyMapMeterAct (u4Idx, u4CurMetIdx, u1CurInProConAct,
                                        u1CurInProExcAct, u1CurOutProAct,
                                        &(RSMetAct), i4RowStatus);
                CLI_FATAL_ERROR (CliHandle);
                return (CLI_FAILURE);
            }

        }

    }

    if (u1OutProAct != 0)
    {
        /* Allocate Memory For OutActFlag */
        pOctStrActFlag = allocmem_octetstring (QOS_POLICY_ACTION_FLAG_LENGTH);
        if (pOctStrActFlag == NULL)
        {
            QoSCliRSPlyMapMeterAct (u4Idx, u4CurMetIdx, u1CurInProConAct,
                                    u1CurInProExcAct, u1CurOutProAct,
                                    &(RSMetAct), i4RowStatus);
            return (CLI_FAILURE);
        }
        MEMSET (pOctStrActFlag->pu1_OctetList, 0, pOctStrActFlag->i4_Length);

        i4RetStatus =
            nmhGetFsQoSPolicyMapOutProfileActionFlag (u4Idx, pOctStrActFlag);
        if (i4RetStatus == SNMP_FAILURE)
        {
            free_octetstring (pOctStrActFlag);
            return (CLI_FAILURE);
        }
        /* Cpy Curr Act Flag to */
        MEMCPY (&u1CurOutProAct, pOctStrActFlag->pu1_OctetList,
                QOS_POLICY_ACTION_FLAG_LENGTH);

        MEMCPY (pOctStrActFlag->pu1_OctetList, &u1OutProAct,
                QOS_POLICY_ACTION_FLAG_LENGTH);

        i4RetStatus =
            nmhTestv2FsQoSPolicyMapOutProfileActionFlag (&u4ErrorCode,
                                                         u4Idx, pOctStrActFlag);
        if (i4RetStatus == SNMP_FAILURE)
        {
            u1CurOutProAct = 0;
            QoSCliRSPlyMapMeterAct (u4Idx, u4CurMetIdx, u1CurInProConAct,
                                    u1CurInProExcAct, u1CurOutProAct,
                                    &(RSMetAct), i4RowStatus);
            free_octetstring (pOctStrActFlag);
            return (CLI_FAILURE);
        }

        i4RetStatus = nmhSetFsQoSPolicyMapOutProfileActionFlag (u4Idx,
                                                                pOctStrActFlag);
        if (i4RetStatus == SNMP_FAILURE)
        {
            QoSCliRSPlyMapMeterAct (u4Idx, u4CurMetIdx, u1CurInProConAct,
                                    u1CurInProExcAct, u1CurOutProAct,
                                    &(RSMetAct), i4RowStatus);
            free_octetstring (pOctStrActFlag);
            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }

        free_octetstring (pOctStrActFlag);

        /* Set the Valid Action */
        if (pMetAct->i1VioVlanDe != -1)
        {
            i4RetStatus =
                nmhTestv2FsQoSPolicyMapOutProfileActionSetVlanDE (&u4ErrorCode,
                                                                  u4Idx,
                                                                  (UINT4)
                                                                  (pMetAct->
                                                                   i1VioVlanDe));
            if (i4RetStatus == SNMP_FAILURE)
            {
                QoSCliRSPlyMapMeterAct (u4Idx, u4CurMetIdx, u1CurInProConAct,
                                        u1CurInProExcAct, u1CurOutProAct,
                                        &(RSMetAct), i4RowStatus);
                return (CLI_FAILURE);
            }

            i4RetStatus =
                nmhGetFsQoSPolicyMapOutProfileActionSetVlanDE (u4Idx,
                                                               &(RSMetAct.
                                                                 u4VioVlanDe));
            if (i4RetStatus == SNMP_FAILURE)
            {
                return (CLI_FAILURE);
            }

            i4RetStatus =
                nmhSetFsQoSPolicyMapOutProfileActionSetVlanDE (u4Idx, pMetAct->
                                                               i1VioVlanDe);
            if (i4RetStatus == SNMP_FAILURE)
            {
                QoSCliRSPlyMapMeterAct (u4Idx, u4CurMetIdx, u1CurInProConAct,
                                        u1CurInProExcAct, u1CurOutProAct,
                                        &(RSMetAct), i4RowStatus);
                CLI_FATAL_ERROR (CliHandle);
                return (CLI_FAILURE);
            }

        }
        if (pMetAct->i1VioVlanPri != -1)
        {
            i4RetStatus =
                nmhTestv2FsQoSPolicyMapOutProfileActionSetVlanPrio
                (&u4ErrorCode, u4Idx, pMetAct->i1VioVlanPri);
            if (i4RetStatus == SNMP_FAILURE)
            {
                QoSCliRSPlyMapMeterAct (u4Idx, u4CurMetIdx, u1CurInProConAct,
                                        u1CurInProExcAct, u1CurOutProAct,
                                        &(RSMetAct), i4RowStatus);
                return (CLI_FAILURE);
            }

            i4RetStatus =
                nmhGetFsQoSPolicyMapOutProfileActionSetVlanPrio
                (u4Idx, &(RSMetAct.u4VioVlanPri));
            if (i4RetStatus == SNMP_FAILURE)
            {
                return (CLI_FAILURE);
            }

            i4RetStatus =
                nmhSetFsQoSPolicyMapOutProfileActionSetVlanPrio (u4Idx,
                                                                 pMetAct->
                                                                 i1VioVlanPri);
            if (i4RetStatus == SNMP_FAILURE)
            {
                QoSCliRSPlyMapMeterAct (u4Idx, u4CurMetIdx, u1CurInProConAct,
                                        u1CurInProExcAct, u1CurOutProAct,
                                        &(RSMetAct), i4RowStatus);
                CLI_FATAL_ERROR (CliHandle);
                return (CLI_FAILURE);
            }

        }

        if (pMetAct->i1VioInVlanPri != -1)
        {
            i4RetStatus =
                nmhTestv2FsQoSPolicyMapOutProfileActionSetInnerVlanPrio
                (&u4ErrorCode, u4Idx, (UINT4) (pMetAct->i1VioInVlanPri));
            if (i4RetStatus == SNMP_FAILURE)
            {
                QoSCliRSPlyMapMeterAct (u4Idx, u4CurMetIdx, u1CurInProConAct,
                                        u1CurInProExcAct, u1CurOutProAct,
                                        &(RSMetAct), i4RowStatus);
                return (CLI_FAILURE);
            }

            i4RetStatus =
                nmhGetFsQoSPolicyMapOutProfileActionSetInnerVlanPrio
                (u4Idx, &(RSMetAct.u4VioInVlanPri));
            if (i4RetStatus == SNMP_FAILURE)
            {
                return (CLI_FAILURE);
            }

            i4RetStatus =
                nmhSetFsQoSPolicyMapOutProfileActionSetInnerVlanPrio
                (u4Idx, pMetAct->i1VioInVlanPri);
            if (i4RetStatus == SNMP_FAILURE)
            {
                QoSCliRSPlyMapMeterAct (u4Idx, u4CurMetIdx, u1CurInProConAct,
                                        u1CurInProExcAct, u1CurOutProAct,
                                        &(RSMetAct), i4RowStatus);
                CLI_FATAL_ERROR (CliHandle);
                return (CLI_FAILURE);
            }

        }

        if (pMetAct->i1VioIPDscp != -1)
        {
            i4RetStatus =
                nmhTestv2FsQoSPolicyMapOutProfileActionSetDscp (&u4ErrorCode,
                                                                u4Idx, pMetAct->
                                                                i1VioIPDscp);
            if (i4RetStatus == SNMP_FAILURE)
            {
                QoSCliRSPlyMapMeterAct (u4Idx, u4CurMetIdx, u1CurInProConAct,
                                        u1CurInProExcAct, u1CurOutProAct,
                                        &(RSMetAct), i4RowStatus);
                return (CLI_FAILURE);
            }

            i4RetStatus =
                nmhGetFsQoSPolicyMapOutProfileActionSetDscp (u4Idx,
                                                             (INT4 *)
                                                             &(RSMetAct.
                                                               u4VioIPDscp));
            if (i4RetStatus == SNMP_FAILURE)
            {
                return (CLI_FAILURE);
            }

            i4RetStatus =
                nmhSetFsQoSPolicyMapOutProfileActionSetDscp (u4Idx, pMetAct->
                                                             i1VioIPDscp);
            if (i4RetStatus == SNMP_FAILURE)
            {
                QoSCliRSPlyMapMeterAct (u4Idx, u4CurMetIdx, u1CurInProConAct,
                                        u1CurInProExcAct, u1CurOutProAct,
                                        &(RSMetAct), i4RowStatus);
                CLI_FATAL_ERROR (CliHandle);
                return (CLI_FAILURE);
            }

        }

        if (pMetAct->i1VioIPTos != -1)
        {
            i4RetStatus =
                nmhTestv2FsQoSPolicyMapOutProfileActionSetIPTOS (&u4ErrorCode,
                                                                 u4Idx,
                                                                 (UINT4)
                                                                 (pMetAct->
                                                                  i1VioIPTos));
            if (i4RetStatus == SNMP_FAILURE)
            {
                QoSCliRSPlyMapMeterAct (u4Idx, u4CurMetIdx, u1CurInProConAct,
                                        u1CurInProExcAct, u1CurOutProAct,
                                        &(RSMetAct), i4RowStatus);
                return (CLI_FAILURE);
            }

            i4RetStatus =
                nmhGetFsQoSPolicyMapOutProfileActionSetIPTOS (u4Idx,
                                                              &(RSMetAct.
                                                                u4VioIPTos));
            if (i4RetStatus == SNMP_FAILURE)
            {
                return (CLI_FAILURE);
            }

            i4RetStatus =
                nmhSetFsQoSPolicyMapOutProfileActionSetIPTOS (u4Idx, pMetAct->
                                                              i1VioIPTos);
            if (i4RetStatus == SNMP_FAILURE)
            {
                QoSCliRSPlyMapMeterAct (u4Idx, u4CurMetIdx, u1CurInProConAct,
                                        u1CurInProExcAct, u1CurOutProAct,
                                        &(RSMetAct), i4RowStatus);
                CLI_FATAL_ERROR (CliHandle);
                return (CLI_FAILURE);
            }

        }

        if (pMetAct->i1VioMPLSExp != -1)
        {
            i4RetStatus =
                nmhTestv2FsQoSPolicyMapOutProfileActionSetMplsExp
                (&u4ErrorCode, u4Idx, (UINT4) (pMetAct->i1VioMPLSExp));
            if (i4RetStatus == SNMP_FAILURE)
            {
                QoSCliRSPlyMapMeterAct (u4Idx, u4CurMetIdx, u1CurInProConAct,
                                        u1CurInProExcAct, u1CurOutProAct,
                                        &(RSMetAct), i4RowStatus);
                return (CLI_FAILURE);
            }

            i4RetStatus =
                nmhGetFsQoSPolicyMapOutProfileActionSetMplsExp
                (u4Idx, &(RSMetAct.u4VioMPLSExp));
            if (i4RetStatus == SNMP_FAILURE)
            {
                return (CLI_FAILURE);
            }

            i4RetStatus =
                nmhSetFsQoSPolicyMapOutProfileActionSetMplsExp (u4Idx, pMetAct->
                                                                i1VioMPLSExp);
            if (i4RetStatus == SNMP_FAILURE)
            {
                QoSCliRSPlyMapMeterAct (u4Idx, u4CurMetIdx, u1CurInProConAct,
                                        u1CurInProExcAct, u1CurOutProAct,
                                        &(RSMetAct), i4RowStatus);
                CLI_FATAL_ERROR (CliHandle);
                return (CLI_FAILURE);
            }

        }
        if (pMetAct->i1VioInVlanDE != -1)
        {
            i4RetStatus =
                nmhTestv2FsQoSPolicyMapOutProfileActionSetInnerVlanDE
                (&u4ErrorCode, u4Idx, (UINT4) (pMetAct->i1VioInVlanDE));
            if (i4RetStatus == SNMP_FAILURE)
            {
                QoSCliRSPlyMapMeterAct (u4Idx, u4CurMetIdx, u1CurInProConAct,
                                        u1CurInProExcAct, u1CurOutProAct,
                                        &(RSMetAct), i4RowStatus);
                return (CLI_FAILURE);
            }

            i4RetStatus =
                nmhGetFsQoSPolicyMapOutProfileActionSetInnerVlanDE
                (u4Idx, &(RSMetAct.u4VioInVlanDE));
            if (i4RetStatus == SNMP_FAILURE)
            {
                return (CLI_FAILURE);
            }

            i4RetStatus =
                nmhSetFsQoSPolicyMapOutProfileActionSetInnerVlanDE
                (u4Idx, (UINT4) (pMetAct->i1VioInVlanDE));
            if (i4RetStatus == SNMP_FAILURE)
            {
                QoSCliRSPlyMapMeterAct (u4Idx, u4CurMetIdx, u1CurInProConAct,
                                        u1CurInProExcAct, u1CurOutProAct,
                                        &(RSMetAct), i4RowStatus);
                CLI_FATAL_ERROR (CliHandle);
                return (CLI_FAILURE);
            }

        }

    }

    if (i4RowStatus == NOT_IN_SERVICE)
    {
        /* Active the Row */
        i4RetStatus = nmhTestv2FsQoSPolicyMapStatus (&u4ErrorCode, u4Idx,
                                                     ACTIVE);
        if (i4RetStatus == SNMP_FAILURE)
        {
            QoSCliRSPlyMapMeterAct (u4Idx, u4CurMetIdx, u1CurInProConAct,
                                    u1CurInProExcAct, u1CurOutProAct,
                                    &(RSMetAct), i4RowStatus);
            return (CLI_FAILURE);
        }

        i4RetStatus = nmhSetFsQoSPolicyMapStatus (u4Idx, ACTIVE);
        if (i4RetStatus == SNMP_FAILURE)
        {
            QoSCliRSPlyMapMeterAct (u4Idx, u4CurMetIdx, u1CurInProConAct,
                                    u1CurInProExcAct, u1CurOutProAct,
                                    &(RSMetAct), i4RowStatus);
            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }
    }

    return (CLI_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSCliUtlGetPlyMapInProConfAct                       */
/* Description        : This function is used to Get the In profile Actions  */
/*                      (Conform )from the Policy Map Table Entry            */
/* Input(s)           : CliHandle    - Handle to CLI session                 */
/*                    : u4Idx        - Table Entry Index.                    */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : CLI_SUCCESS/CLI_FAILURE                              */
/* Called By          : QoSCliUtlDisplayPlyMapEntry                          */
/* Calling Function   : SNMP nmh Routines                                    */
/*****************************************************************************/
INT4
QoSCliUtlGetPlyMapInProConfAct (tCliHandle CliHandle, UINT4 u4Idx)
{
    UINT1               au1ConDispStr[100];
    tSNMP_OCTET_STRING_TYPE *pOctStrFlag = NULL;
    UINT1              *pu1ConDispStr = NULL;
    INT4                i4RetStatus = SNMP_FAILURE;
    UINT4               u4GetVal = 0;
    INT4                i4GetVal = 0;
    UINT1               u1InActFlag = 0;
    UINT2               u2ActMask = 0x1;
    UINT1               au1IfName[CFA_MAX_PORT_NAME_LENGTH];
    /* OutProfile Actions */
    /* Allocate Memory For Name */
    pOctStrFlag = allocmem_octetstring (QOS_POLICY_ACTION_FLAG_LENGTH);
    if (pOctStrFlag == NULL)
    {
        return (CLI_FAILURE);
    }

    MEMSET (pOctStrFlag->pu1_OctetList, 0, pOctStrFlag->i4_Length);

    i4RetStatus = nmhGetFsQoSPolicyMapInProfileConformActionFlag (u4Idx,
                                                                  pOctStrFlag);
    if (i4RetStatus == SNMP_FAILURE)
    {
        free_octetstring (pOctStrFlag);
        return (CLI_FAILURE);
    }

    /* Get the Action Flag */
    MEMCPY (&u1InActFlag, pOctStrFlag->pu1_OctetList, pOctStrFlag->i4_Length);
    free_octetstring (pOctStrFlag);

    if (u1InActFlag == 0)
    {
        CliPrintf (CliHandle, "ConfAct      : None.\r\n");
        return (CLI_SUCCESS);
    }

    MEMSET (au1ConDispStr, 0, 100);
    pu1ConDispStr = &(au1ConDispStr[0]);

    SPRINTF ((CHR1 *) pu1ConDispStr, "%s", "ConfAct      : ");
    pu1ConDispStr = pu1ConDispStr + (STRLEN (au1ConDispStr));

    while (u2ActMask < 0xFF)
    {
        switch ((u1InActFlag & u2ActMask))
        {

            case QOS_PLY_IN_PROF_DROP:
                SNPRINTF ((CHR1 *) pu1ConDispStr, QOS_MAX_LENGTH_DROP, "%s",
                          "Drop.");
                pu1ConDispStr = pu1ConDispStr + (STRLEN (pu1ConDispStr));
                break;

            case QOS_PLY_IN_PROF_CONF_IP_TOS:

                i4RetStatus =
                    nmhGetFsQoSPolicyMapConformActionSetIpTOS (u4Idx,
                                                               &u4GetVal);
                if (i4RetStatus == SNMP_FAILURE)
                {
                    return (CLI_FAILURE);
                }
                SNPRINTF ((CHR1 *) pu1ConDispStr, QOS_MAX_BUF_LENGTH, "%s%d, ",
                          "IP Tos ", u4GetVal);
                pu1ConDispStr = pu1ConDispStr + (STRLEN (pu1ConDispStr));

                break;

            case QOS_PLY_IN_PROF_CONF_IP_DSCP:

                i4RetStatus =
                    nmhGetFsQoSPolicyMapConformActionSetDscp (u4Idx, &i4GetVal);
                if (i4RetStatus == SNMP_FAILURE)
                {
                    return (CLI_FAILURE);
                }
                SNPRINTF ((CHR1 *) pu1ConDispStr, QOS_MAX_BUF_LENGTH, "%s%d, ",
                          "IP Dscp ", i4GetVal);
                pu1ConDispStr = pu1ConDispStr + (STRLEN (pu1ConDispStr));

                break;

            case QOS_PLY_IN_PROF_PORT:

                i4RetStatus =
                    nmhGetFsQoSPolicyMapInProfileActionSetPort
                    (u4Idx, (UINT4 *) &i4GetVal);
                if (i4RetStatus == SNMP_FAILURE)
                {
                    return (CLI_FAILURE);
                }
                MEMSET (au1IfName, 0, CFA_MAX_PORT_NAME_LENGTH);
                if (CfaCliGetIfName (i4GetVal, (INT1 *) au1IfName) ==
                    CLI_FAILURE)
                {
                    return (CLI_FAILURE);
                }

                SNPRINTF ((CHR1 *) pu1ConDispStr, QOS_MAX_BUF_LENGTH, "%s%s, ",
                          "Port ", au1IfName);
                pu1ConDispStr = pu1ConDispStr + (STRLEN (pu1ConDispStr));

                break;

            case QOS_PLY_IN_PROF_CONF_INNER_VLAN_PRI:

                i4RetStatus =
                    nmhGetFsQoSPolicyMapConformActionSetInnerVlanPrio
                    (u4Idx, &u4GetVal);
                if (i4RetStatus == SNMP_FAILURE)
                {
                    return (CLI_FAILURE);
                }
                SNPRINTF ((CHR1 *) pu1ConDispStr, QOS_MAX_BUF_LENGTH, "%s%d, ",
                          "InVlan Pri ", u4GetVal);
                pu1ConDispStr = pu1ConDispStr + (STRLEN (pu1ConDispStr));

                break;

            case QOS_PLY_IN_PROF_CONF_VLAN_PRI:

                i4RetStatus =
                    nmhGetFsQoSPolicyMapConformActionSetVlanPrio (u4Idx,
                                                                  &u4GetVal);
                if (i4RetStatus == SNMP_FAILURE)
                {
                    return (CLI_FAILURE);
                }
                SNPRINTF ((CHR1 *) pu1ConDispStr, QOS_MAX_BUF_LENGTH, "%s%d, ",
                          "Vlan Pri ", u4GetVal);
                pu1ConDispStr = pu1ConDispStr + (STRLEN (pu1ConDispStr));

                break;

            case QOS_PLY_IN_PROF_CONF_VLAN_DE:

                i4RetStatus =
                    nmhGetFsQoSPolicyMapConformActionSetVlanDE (u4Idx,
                                                                &u4GetVal);
                if (i4RetStatus == SNMP_FAILURE)
                {
                    return (CLI_FAILURE);
                }

                SNPRINTF ((CHR1 *) pu1ConDispStr, QOS_MAX_BUF_LENGTH, "%s%d, ",
                          "Vlan DE ", u4GetVal);
                pu1ConDispStr = pu1ConDispStr + (STRLEN (pu1ConDispStr));

                break;

            case QOS_PLY_IN_PROF_CONF_MPLS_EXP:

                i4RetStatus =
                    nmhGetFsQoSPolicyMapConformActionSetMplsExp (u4Idx,
                                                                 &u4GetVal);
                if (i4RetStatus == SNMP_FAILURE)
                {
                    return (CLI_FAILURE);
                }
                SNPRINTF ((CHR1 *) pu1ConDispStr, QOS_MAX_BUF_LENGTH, "%s%d, ",
                          "MPLS Exp ", u4GetVal);
                pu1ConDispStr = pu1ConDispStr + (STRLEN (pu1ConDispStr));
                break;

            case QOS_PLY_IN_PROF_CONF_INNER_VLAN_DE:

                i4RetStatus =
                    nmhGetFsQoSPolicyMapConformActionSetInnerVlanDE
                    (u4Idx, &u4GetVal);
                if (i4RetStatus == SNMP_FAILURE)
                {
                    return (CLI_FAILURE);
                }
                SNPRINTF ((CHR1 *) pu1ConDispStr, QOS_MAX_BUF_LENGTH, "%s%d, ",
                          "InVlan DE ", u4GetVal);
                pu1ConDispStr = pu1ConDispStr + (STRLEN (pu1ConDispStr));

                break;

            default:
                break;

        }                        /* End of Switch */

        u2ActMask = (UINT2) (u2ActMask << 1);

    }                            /* End of While */

    /* Print Conf In Profile Action */
    if (*(pu1ConDispStr - 2) == ',')
    {
        *(pu1ConDispStr - 2) = '\0';
    }

    CliPrintf (CliHandle, "%s \r\n", au1ConDispStr);

    return (CLI_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSCliUtlGetPlyMapInProExcAct                        */
/* Description        : This function is used to Get the In profile Actions  */
/*                      (Exceed )from the Policy Map Table Entry             */
/* Input(s)           : CliHandle    - Handle to CLI session                 */
/*                    : u4Idx        - Table Entry Index.                    */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : CLI_SUCCESS/CLI_FAILURE                              */
/* Called By          : QoSCliUtlDisplayPlyMapEntry                          */
/* Calling Function   : SNMP nmh Routines                                    */
/*****************************************************************************/
INT4
QoSCliUtlGetPlyMapInProExcAct (tCliHandle CliHandle, UINT4 u4Idx)
{
    UINT1               au1ExcDispStr[100];
    tSNMP_OCTET_STRING_TYPE *pOctStrFlag = NULL;
    UINT1              *pu1ExcDispStr = NULL;
    INT4                i4RetStatus = SNMP_FAILURE;
    UINT4               u4GetVal = 0;
    INT4                i4GetVal = 0;
    UINT1               u1InActFlag = 0;
    UINT2               u2ActMask = 0x1;

    /* OutProfile Actions */
    /* Allocate Memory For Name */
    pOctStrFlag = allocmem_octetstring (QOS_POLICY_ACTION_FLAG_LENGTH);
    if (pOctStrFlag == NULL)
    {
        return (CLI_FAILURE);
    }

    MEMSET (pOctStrFlag->pu1_OctetList, 0, pOctStrFlag->i4_Length);

    i4RetStatus = nmhGetFsQoSPolicyMapInProfileExceedActionFlag (u4Idx,
                                                                 pOctStrFlag);
    if (i4RetStatus == SNMP_FAILURE)
    {
        free_octetstring (pOctStrFlag);
        return (CLI_FAILURE);
    }

    /* Get the Action Flag */
    MEMCPY (&u1InActFlag, pOctStrFlag->pu1_OctetList, pOctStrFlag->i4_Length);
    free_octetstring (pOctStrFlag);

    if (u1InActFlag == 0)
    {
        CliPrintf (CliHandle, "ExcAct       : None.\r\n");
        return (CLI_SUCCESS);
    }

    MEMSET (au1ExcDispStr, 0, 100);
    pu1ExcDispStr = &(au1ExcDispStr[0]);

    SPRINTF ((CHR1 *) pu1ExcDispStr, "%s", "ExcAct       : ");
    pu1ExcDispStr = pu1ExcDispStr + (STRLEN (au1ExcDispStr));

    while (u2ActMask < 0xFF)
    {
        switch ((u1InActFlag & u2ActMask))
        {
            case QOS_PLY_IN_PROF_EXC_DROP:
                SNPRINTF ((CHR1 *) pu1ExcDispStr, QOS_MAX_LENGTH_DROP, "%s",
                          "Drop.");
                pu1ExcDispStr = pu1ExcDispStr + (STRLEN (pu1ExcDispStr));
                break;

            case QOS_PLY_IN_PROF_EXC_IP_TOS:

                i4RetStatus =
                    nmhGetFsQoSPolicyMapExceedActionSetIpTOS (u4Idx, &u4GetVal);
                if (i4RetStatus == SNMP_FAILURE)
                {
                    return (CLI_FAILURE);
                }
                SNPRINTF ((CHR1 *) pu1ExcDispStr, QOS_MAX_BUF_LENGTH, "%s%d, ",
                          "IP Tos ", u4GetVal);
                pu1ExcDispStr = pu1ExcDispStr + (STRLEN (pu1ExcDispStr));

                break;

            case QOS_PLY_IN_PROF_EXC_IP_DSCP:

                i4RetStatus =
                    nmhGetFsQoSPolicyMapExceedActionSetDscp (u4Idx, &i4GetVal);
                if (i4RetStatus == SNMP_FAILURE)
                {
                    return (CLI_FAILURE);
                }
                SNPRINTF ((CHR1 *) pu1ExcDispStr, QOS_MAX_BUF_LENGTH, "%s%d, ",
                          "IP Dscp ", i4GetVal);
                pu1ExcDispStr = pu1ExcDispStr + (STRLEN (pu1ExcDispStr));

                break;

            case QOS_PLY_IN_PROF_EXC_INNER_VLAN_PRI:

                i4RetStatus =
                    nmhGetFsQoSPolicyMapExceedActionSetInnerVlanPrio
                    (u4Idx, &u4GetVal);
                if (i4RetStatus == SNMP_FAILURE)
                {
                    return (CLI_FAILURE);
                }
                SNPRINTF ((CHR1 *) pu1ExcDispStr, QOS_MAX_BUF_LENGTH, "%s%d, ",
                          "InVlan Pri ", u4GetVal);
                pu1ExcDispStr = pu1ExcDispStr + (STRLEN (pu1ExcDispStr));

                break;

            case QOS_PLY_IN_PROF_EXC_VLAN_PRI:

                i4RetStatus =
                    nmhGetFsQoSPolicyMapExceedActionSetVlanPrio (u4Idx,
                                                                 &u4GetVal);
                if (i4RetStatus == SNMP_FAILURE)
                {
                    return (CLI_FAILURE);
                }
                SNPRINTF ((CHR1 *) pu1ExcDispStr, QOS_MAX_BUF_LENGTH, "%s%d, ",
                          "Vlan Pri ", u4GetVal);
                pu1ExcDispStr = pu1ExcDispStr + (STRLEN (pu1ExcDispStr));

                break;

            case QOS_PLY_IN_PROF_EXC_VLAN_DE:

                i4RetStatus =
                    nmhGetFsQoSPolicyMapExceedActionSetVlanDE (u4Idx,
                                                               &u4GetVal);
                if (i4RetStatus == SNMP_FAILURE)
                {
                    return (CLI_FAILURE);
                }

                SNPRINTF ((CHR1 *) pu1ExcDispStr, QOS_MAX_BUF_LENGTH, "%s%d, ",
                          "Vlan DE ", u4GetVal);
                pu1ExcDispStr = pu1ExcDispStr + (STRLEN (pu1ExcDispStr));

                break;

            case QOS_PLY_IN_PROF_EXC_MPLS_EXP:

                i4RetStatus =
                    nmhGetFsQoSPolicyMapExceedActionSetMplsExp (u4Idx,
                                                                &u4GetVal);
                if (i4RetStatus == SNMP_FAILURE)
                {
                    return (CLI_FAILURE);
                }
                SNPRINTF ((CHR1 *) pu1ExcDispStr, QOS_MAX_BUF_LENGTH, "%s%d, ",
                          "MPLS Exp ", u4GetVal);
                pu1ExcDispStr = pu1ExcDispStr + (STRLEN (pu1ExcDispStr));

                break;

            case QOS_PLY_IN_PROF_EXC_INNER_VLAN_DE:

                i4RetStatus =
                    nmhGetFsQoSPolicyMapExceedActionSetInnerVlanDE
                    (u4Idx, &u4GetVal);
                if (i4RetStatus == SNMP_FAILURE)
                {
                    return (CLI_FAILURE);
                }
                SNPRINTF ((CHR1 *) pu1ExcDispStr, QOS_MAX_BUF_LENGTH, "%s%d, ",
                          "InVlan DE ", u4GetVal);
                pu1ExcDispStr = pu1ExcDispStr + (STRLEN (pu1ExcDispStr));

                break;

            default:
                break;

        }                        /* End of Switch */

        u2ActMask = (UINT2) (u2ActMask << 1);

    }                            /* End of While */

    /* Print Exceed In Profile Action */
    if (*(pu1ExcDispStr - 2) == ',')
    {
        *(pu1ExcDispStr - 2) = '\0';
    }

    CliPrintf (CliHandle, "%s \r\n", au1ExcDispStr);

    return (CLI_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSCliUtlGetPlyMapOutProAct                          */
/* Description        : This function is used to Get the Out profile Actions */
/*                      from the Policy Map Table Entry                      */
/* Input(s)           : CliHandle    - Handle to CLI session                 */
/*                    : u4Idx        - Table Entry Index.                    */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : CLI_SUCCESS/CLI_FAILURE                              */
/* Called By          : QoSCliUtlDisplayPlyMapEntry                          */
/* Calling Function   : SNMP nmh Routines                                    */
/*****************************************************************************/
INT4
QoSCliUtlGetPlyMapOutProAct (tCliHandle CliHandle, UINT4 u4Idx)
{
    UINT1               au1DispStr[100];
    tSNMP_OCTET_STRING_TYPE *pOctStrFlag = NULL;
    UINT1              *pu1DispStr = NULL;
    INT4                i4RetStatus = SNMP_FAILURE;
    UINT4               u4GetVal = 0;
    INT4                i4GetVal = 0;
    UINT1               u1OutActFlag = 0;
    UINT2               u2ActMask = 0x1;

    /* OutProfile Actions */
    /* Allocate Memory For Name */
    pOctStrFlag = allocmem_octetstring (QOS_POLICY_ACTION_FLAG_LENGTH);
    if (pOctStrFlag == NULL)
    {
        return (CLI_FAILURE);
    }

    MEMSET (pOctStrFlag->pu1_OctetList, 0, pOctStrFlag->i4_Length);

    i4RetStatus = nmhGetFsQoSPolicyMapOutProfileActionFlag (u4Idx, pOctStrFlag);
    if (i4RetStatus == SNMP_FAILURE)
    {
        free_octetstring (pOctStrFlag);
        return (CLI_FAILURE);
    }

    /* Get the Action Flag */
    MEMCPY (&u1OutActFlag, pOctStrFlag->pu1_OctetList, pOctStrFlag->i4_Length);
    free_octetstring (pOctStrFlag);

    if (u1OutActFlag == 0)
    {
        CliPrintf (CliHandle, "VioAct       : None.\r\n");
        return (CLI_SUCCESS);
    }

    MEMSET (au1DispStr, 0, 100);
    pu1DispStr = &(au1DispStr[0]);

    SPRINTF ((CHR1 *) pu1DispStr, "%s", "VioAct       : ");
    pu1DispStr = pu1DispStr + (STRLEN (au1DispStr));

    while (u2ActMask < 0xFF)
    {
        switch ((u1OutActFlag & u2ActMask))
        {
            case QOS_PLY_OUT_PROF_VIO_DROP:
                SNPRINTF ((CHR1 *) pu1DispStr, QOS_MAX_LENGTH_DROP, "%s",
                          "Drop.");
                pu1DispStr = pu1DispStr + (STRLEN (pu1DispStr));
                break;

            case QOS_PLY_OUT_PROF_VIO_IP_TOS:

                i4RetStatus =
                    nmhGetFsQoSPolicyMapOutProfileActionSetIPTOS (u4Idx,
                                                                  &u4GetVal);
                if (i4RetStatus == SNMP_FAILURE)
                {
                    return (CLI_FAILURE);
                }
                SPRINTF ((CHR1 *) pu1DispStr, "%s%u, ", "IP Tos ", u4GetVal);
                pu1DispStr = pu1DispStr + (STRLEN (pu1DispStr));

                break;

            case QOS_PLY_OUT_PROF_VIO_IP_DSCP:

                i4RetStatus =
                    nmhGetFsQoSPolicyMapOutProfileActionSetDscp (u4Idx,
                                                                 &i4GetVal);
                if (i4RetStatus == SNMP_FAILURE)
                {
                    return (CLI_FAILURE);
                }
                SNPRINTF ((CHR1 *) pu1DispStr, QOS_MAX_BUF_LENGTH, "%s%d, ",
                          "IP Dscp ", i4GetVal);
                pu1DispStr = pu1DispStr + (STRLEN (pu1DispStr));

                break;

            case QOS_PLY_OUT_PROF_VIO_INNER_VLAN_PRI:

                i4RetStatus =
                    nmhGetFsQoSPolicyMapOutProfileActionSetInnerVlanPrio
                    (u4Idx, &u4GetVal);
                if (i4RetStatus == SNMP_FAILURE)
                {
                    return (CLI_FAILURE);
                }
                SNPRINTF ((CHR1 *) pu1DispStr, QOS_MAX_BUF_LENGTH, "%s%d, ",
                          "InVlan Pri ", u4GetVal);
                pu1DispStr = pu1DispStr + (STRLEN (pu1DispStr));

                break;

            case QOS_PLY_OUT_PROF_VIO_VLAN_PRI:

                i4RetStatus =
                    nmhGetFsQoSPolicyMapOutProfileActionSetVlanPrio (u4Idx,
                                                                     &u4GetVal);
                if (i4RetStatus == SNMP_FAILURE)
                {
                    return (CLI_FAILURE);
                }
                SNPRINTF ((CHR1 *) pu1DispStr, QOS_MAX_BUF_LENGTH, "%s%d, ",
                          "Vlan Pri ", u4GetVal);
                pu1DispStr = pu1DispStr + (STRLEN (pu1DispStr));

                break;

            case QOS_PLY_OUT_PROF_VIO_VLAN_DE:

                i4RetStatus =
                    nmhGetFsQoSPolicyMapOutProfileActionSetVlanDE (u4Idx,
                                                                   &u4GetVal);
                if (i4RetStatus == SNMP_FAILURE)
                {
                    return (CLI_FAILURE);
                }

                SNPRINTF ((CHR1 *) pu1DispStr, QOS_MAX_BUF_LENGTH, "%s%d, ",
                          "Vlan DE ", u4GetVal);
                pu1DispStr = pu1DispStr + (STRLEN (pu1DispStr));

                break;

            case QOS_PLY_OUT_PROF_VIO_MPLS_EXP:

                i4RetStatus =
                    nmhGetFsQoSPolicyMapOutProfileActionSetMplsExp (u4Idx,
                                                                    &u4GetVal);
                if (i4RetStatus == SNMP_FAILURE)
                {
                    return (CLI_FAILURE);
                }
                SNPRINTF ((CHR1 *) pu1DispStr, QOS_MAX_BUF_LENGTH, "%s%d, ",
                          "MPLS Exp ", u4GetVal);
                pu1DispStr = pu1DispStr + (STRLEN (pu1DispStr));

                break;

            case QOS_PLY_OUT_PROF_VIO_INNER_VLAN_DE:

                i4RetStatus =
                    nmhGetFsQoSPolicyMapOutProfileActionSetInnerVlanDE
                    (u4Idx, &u4GetVal);
                if (i4RetStatus == SNMP_FAILURE)
                {
                    return (CLI_FAILURE);
                }
                SNPRINTF ((CHR1 *) pu1DispStr, QOS_MAX_BUF_LENGTH, "%s%d, ",
                          "InVlan DE ", u4GetVal);
                pu1DispStr = pu1DispStr + (STRLEN (pu1DispStr));

                break;

            default:
                break;

        }                        /* End of Switch */

        u2ActMask = (UINT2) (u2ActMask << 1);

    }                            /* End of While */

    /* Print Out Profile Action */
    if (*(pu1DispStr - 2) == ',')
    {
        *(pu1DispStr - 2) = '\0';
    }

    CliPrintf (CliHandle, "%s \r\n", au1DispStr);

    return (CLI_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSCliUtlDisplayPlyMapEntry                          */
/* Description        : This function is used to Display the Policy   Map    */
/*                      Table Entries Parameters                             */
/* Input(s)           : CliHandle    - Handle to CLI session                 */
/*                    : u4Idx        - Table Entries Index                   */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : CLI_SUCCESS/CLI_FAILURE                              */
/* Called By          : QoSCliShowPlyMapEntry                                */
/* Calling Function   : SNMP nmh Routines                                    */
/*****************************************************************************/
INT4
QoSCliUtlDisplayPlyMapEntry (tCliHandle CliHandle, UINT4 u4Idx)
{
    INT4                i4RetStatus = SNMP_FAILURE;
    UINT4               u4IfIndex = 0;
    UINT4               u4Class = 0;
    INT4                i4PHBType = 0;
    UINT4               u4DefPHB = 0;
    UINT4               u4MeterId = 0;
    UINT4               u4ConNClass = 0;
    UINT4               u4ExcNClass = 0;
    UINT4               u4VioNClass = 0;
    UINT1               au1IfName[CFA_MAX_PORT_NAME_LENGTH];
    UINT4               u4CurDEI = (UINT4) CLI_RS_INVALID_MASK;

    /* Get All Fields */
    i4RetStatus = nmhGetFsQoSPolicyMapIfIndex (u4Idx, &u4IfIndex);
    if (i4RetStatus == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }
    MEMSET (au1IfName, 0, CFA_MAX_PORT_NAME_LENGTH);
    if (CfaCliGetIfName (u4IfIndex, (INT1 *) au1IfName) == CLI_FAILURE)
    {
        return (CLI_FAILURE);
    }
    i4RetStatus = nmhGetFsQoSPolicyMapCLASS (u4Idx, &u4Class);
    if (i4RetStatus == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    i4RetStatus = nmhGetFsQoSPolicyMapPHBType (u4Idx, &i4PHBType);
    if (i4RetStatus == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    i4RetStatus = nmhGetFsQoSPolicyMapDefaultPHB (u4Idx, &u4DefPHB);
    if (i4RetStatus == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    i4RetStatus = nmhGetFsQoSPolicyDefaultVlanDE (u4Idx, (INT4 *) (&u4CurDEI));
    if (i4RetStatus == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    i4RetStatus = nmhGetFsQoSPolicyMapMeterTableId (u4Idx, &u4MeterId);
    if (i4RetStatus == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    CliPrintf (CliHandle, "%-12s : %d\r\n", "PolicyMapId", u4Idx);
    CliPrintf (CliHandle, "%-12s : %s\r\n", "IfIndex", au1IfName);
    CliPrintf (CliHandle, "%-12s : %d\r\n", "Class", u4Class);

    switch (i4PHBType)
    {
        case QOS_CLI_PLY_PHB_TYPE_VLAN_PRI:
            CliPrintf (CliHandle, "%-12s : %s\r\n", "DefaultPHB", "VlanPri");
            CliPrintf (CliHandle, "%-12s : %d\r\n", "PHB Value", u4DefPHB);
            break;

        case QOS_CLI_PLY_PHB_TYPE_IP_TOS:
            CliPrintf (CliHandle, "%-12s : %s\r\n", "DefaultPHB", "IP TOS");
            CliPrintf (CliHandle, "%-12s : %d\r\n", "PHB Value", u4DefPHB);

            break;

        case QOS_CLI_PLY_PHB_TYPE_IP_DSCP:
            CliPrintf (CliHandle, "%-12s : %s\r\n", "DefaultPHB", "IP DSCP");
            CliPrintf (CliHandle, "%-12s : %d\r\n", "PHB Value", u4DefPHB);

            break;

        case QOS_CLI_PLY_PHB_TYPE_MPLS_EXP:
            CliPrintf (CliHandle, "%-12s : %s\r\n", "DefaultPHB", "MPLS Exp");
            CliPrintf (CliHandle, "%-12s : %d\r\n", "PHB Value", u4DefPHB);

            break;

        case QOS_CLI_PLY_PHB_TYPE_NONE:
            CliPrintf (CliHandle, "%-12s : %s\r\n", "DefaultPHB", "None.");
            break;

        case QOS_CLI_PLY_PHB_TYPE_DOT1P:
            CliPrintf (CliHandle, "%-12s : %s\r\n", "DefaultPHB", "Dot1P");
            CliPrintf (CliHandle, "%-12s : %d\r\n", "PHB Value", u4DefPHB);

            break;

        default:
            CliPrintf (CliHandle, "%-12s : %s\r\n", "DefaultPHB", "Invalid");
            CliPrintf (CliHandle, "%-12s : %d\r\n", "PHB Value", u4DefPHB);

            break;
    }

    if (i4PHBType == QOS_CLI_PLY_PHB_TYPE_DOT1P)
    {
        CliPrintf (CliHandle, "%-12s : %d\r\n", "DE Value", u4CurDEI);
    }

    CliPrintf (CliHandle, "%-12s : %d\r\n", "MeterId", u4MeterId);
    /* print New Class */

    i4RetStatus =
        nmhGetFsQoSPolicyMapConformActionSetNewCLASS (u4Idx, &u4ConNClass);
    if (i4RetStatus == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    i4RetStatus =
        nmhGetFsQoSPolicyMapExceedActionSetNewCLASS (u4Idx, &u4ExcNClass);
    if (i4RetStatus == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    i4RetStatus = nmhGetFsQoSPolicyMapOutProfileActionSetNewCLASS
        (u4Idx, &u4VioNClass);
    if (i4RetStatus == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    CliPrintf (CliHandle, "%-12s : %d\r\n", "ConNClass", u4ConNClass);
    CliPrintf (CliHandle, "%-12s : %d\r\n", "ExcNClass", u4ExcNClass);
    CliPrintf (CliHandle, "%-12s : %d\r\n", "VioNClass", u4VioNClass);
    /*Print In Profile  Conform */
    i4RetStatus = QoSCliUtlGetPlyMapInProConfAct (CliHandle, u4Idx);
    if (i4RetStatus == CLI_FAILURE)
    {
        return (CLI_FAILURE);
    }

    /*Print In Profile Exceed */
    i4RetStatus = QoSCliUtlGetPlyMapInProExcAct (CliHandle, u4Idx);
    if (i4RetStatus == CLI_FAILURE)
    {
        return (CLI_FAILURE);
    }

    /*Print Out Profile */
    i4RetStatus = QoSCliUtlGetPlyMapOutProAct (CliHandle, u4Idx);
    if (i4RetStatus == CLI_FAILURE)
    {
        return (CLI_FAILURE);
    }

    /* Print */
    CliPrintf (CliHandle, "\r\n");

    return (CLI_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSCliShowPlyMapEntry                                */
/* Description        : This function is used to Display the Policy Map      */
/*                      Table Entries Parameters for the name pu1Name, if    */
/*                      it is NULL then All Entries will be displayed        */
/* Input(s)           : CliHandle    - Handle to CLI session                 */
/*                    : pu1Name      - Table Entry Name to Display           */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : CLI_SUCCESS/CLI_FAILURE                              */
/* Called By          : cli_process_qos_cmd                                  */
/* Calling Function   : QoSCliUtlDisplayPlyMapEntry                          */
/*****************************************************************************/
INT4
QoSCliShowPlyMapEntry (tCliHandle CliHandle, UINT4 u4Idx)
{
    INT4                i4RetStatus = SNMP_FAILURE;
    INT4                i4RowStatus = SNMP_FAILURE;

    CliPrintf (CliHandle, "QoS Policy Map Entries\r\n");
    CliPrintf (CliHandle, "----------------------\r\n");

    /* Display All */
    if (u4Idx == 0)
    {
        i4RetStatus = nmhGetFirstIndexFsQoSPolicyMapTable (&u4Idx);
        if (i4RetStatus == SNMP_FAILURE)
        {
            return (CLI_FAILURE);
        }

        do
        {
            /* Check the Status */
            i4RetStatus = nmhGetFsQoSPolicyMapStatus (u4Idx, &i4RowStatus);
            if (i4RetStatus == SNMP_FAILURE)
            {
                return (CLI_FAILURE);
            }

            if (i4RowStatus == ACTIVE)
            {
                /* Display Entry */
                i4RetStatus = QoSCliUtlDisplayPlyMapEntry (CliHandle, u4Idx);
                if (i4RetStatus == CLI_FAILURE)
                {
                    return (CLI_FAILURE);
                }
            }

            i4RetStatus = nmhGetNextIndexFsQoSPolicyMapTable (u4Idx, &u4Idx);

        }
        while (i4RetStatus == SNMP_SUCCESS);

        return (CLI_SUCCESS);
    }
    else
    {
        i4RetStatus = nmhGetFsQoSPolicyMapStatus (u4Idx, &i4RowStatus);
        if (i4RetStatus == SNMP_FAILURE)
        {
            return (CLI_FAILURE);
        }

        /* Check the Status */
        if (i4RowStatus == ACTIVE)
        {
            i4RetStatus = QoSCliUtlDisplayPlyMapEntry (CliHandle, u4Idx);
            if (i4RetStatus == CLI_FAILURE)
            {
                return (CLI_FAILURE);
            }
        }
    }

    return (CLI_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSCliDelPlyMapEntry                                 */
/* Description        : This function is used to delete Policy Map table     */
/*                      Entry.                                               */
/* Input(s)           : CliHandle    - Handle to CLI session                 */
/*                    : pu1Name      - Table Entry Name to Delete.           */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : CLI_SUCCESS/CLI_FAILURE                              */
/* Called By          : cli_process_qos_cmd                                  */
/* Calling Function   : SNMP nmh Routines                                    */
/*****************************************************************************/
INT4
QoSCliDelPlyMapEntry (tCliHandle CliHandle, UINT4 u4Idx)
{
    INT4                i4RetStatus = SNMP_FAILURE;
    UINT4               u4ErrorCode = SNMP_ERR_NO_ERROR;

    UNUSED_PARAM (CliHandle);

    /* Delete a Entry */
    i4RetStatus = nmhTestv2FsQoSPolicyMapStatus (&u4ErrorCode, u4Idx, DESTROY);
    if (i4RetStatus == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    i4RetStatus = nmhSetFsQoSPolicyMapStatus (u4Idx, DESTROY);
    if (i4RetStatus == SNMP_FAILURE)
    {
        ISSCliCheckAndThrowFatalError (CliHandle);
        return (CLI_FAILURE);
    }

    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*                     Q TEMPLATE TABLE FUNCTIONS                            */
/*****************************************************************************/

/*****************************************************************************/
/* Function Name      : QoSQTempPrompt                                       */
/* Description        : This function is used to set CLI Prompt string for   */
/*                      QTemplate Table                                      */
/* Input(s)           : pi1ModeName   - Prompt Name from CLI                 */
/*                    : pi1PromptStr  - Prompt String to Display             */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : CLI_SUCCESS/CLI_FAILURE                              */
/* Called By          : CLI Task                                             */
/* Calling Function   : None.                                                */
/*****************************************************************************/
PUBLIC INT1
QoSQTempPrompt (INT1 *pi1ModeName, INT1 *pi1PromptStr)
{
    UINT4               u4Idx;
    UINT4               u4Len = STRLEN (QOS_CLI_QTEMP_MODE);

    if ((!pi1PromptStr) || (!pi1ModeName))
    {
        return FALSE;
    }

    if (STRNCMP (pi1ModeName, QOS_CLI_QTEMP_MODE, u4Len) != 0)
    {
        return FALSE;
    }

    pi1ModeName = pi1ModeName + u4Len;
    u4Idx = CLI_ATOI (pi1ModeName);
    CLI_SET_QTEMP_ID (u4Idx);
    STRNCPY (pi1PromptStr, "(config-qtype)#", STRLEN ("(config-qtype)#"));
    pi1PromptStr[STRLEN ("(config-qtype)#")] = '\0';

    return TRUE;
}

/*****************************************************************************/
/* Function Name      : QoSCliAddQTempEntry                                  */
/* Description        : This function is used to create QTemplate table      */
/*                      Entry and change the CLI Prompt.                     */
/* Input(s)           : CliHandle    - Handle to CLI session                 */
/*                    : u4QTemp      - Q Template Id to Create               */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : CLI_SUCCESS/CLI_FAILURE                              */
/* Called By          : cli_process_qos_cmd                                  */
/* Calling Function   : SNMP nmh Routines                                    */
/*****************************************************************************/
INT4
QoSCliAddQTempEntry (tCliHandle CliHandle, UINT4 u4QTemp)
{
    UINT1               au1QTemp[QOS_CLI_MAX_PROMPT_LENGTH];
    INT4                i4RetStatus = SNMP_FAILURE;
    INT4                i4RowStatus = SNMP_FAILURE;
    UINT4               u4ErrorCode = SNMP_ERR_NO_ERROR;

    UNUSED_PARAM (CliHandle);

    i4RetStatus = nmhGetFsQoSQTemplateStatus (u4QTemp, &i4RowStatus);
    if (i4RetStatus == SNMP_FAILURE)
    {
        /* Create new Entry */
        i4RetStatus = nmhTestv2FsQoSQTemplateStatus (&u4ErrorCode, u4QTemp,
                                                     CREATE_AND_WAIT);
        if (i4RetStatus == SNMP_FAILURE)
        {
            return (CLI_FAILURE);
        }

        i4RetStatus = nmhSetFsQoSQTemplateStatus (u4QTemp, CREATE_AND_WAIT);
        if (i4RetStatus == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }
    }

    /* Go to the QTemplate Mode of Cli Prompt */
    SPRINTF ((CHR1 *) au1QTemp, "%s%u", QOS_CLI_QTEMP_MODE, u4QTemp);

    if (CliChangePath ((CHR1 *) au1QTemp) == CLI_FAILURE)
    {
        CliPrintf (CliHandle, "\r%% Unable to enter Q Template mode.\r\n");
        return (CLI_FAILURE);
    }

    return (CLI_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSCliSetQTempParams                                 */
/* Description        : This function is used to Set QTemplate Table Entry   */
/*                      Parameter                                            */
/* Input(s)           : CliHandle    - Handle to CLI session                 */
/*                    : i4DropType   - Q Drop Type                           */
/*                    : i4QTempSize  - Q Size                                */
/*                    : i4DropAlgo   - Q Drop Algorithm                      */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : CLI_SUCCESS/CLI_FAILURE                              */
/* Called By          : cli_process_qos_cmd                                  */
/* Calling Function   : SNMP nmh Routines                                    */
/*****************************************************************************/
VOID
QoSCliRSSetQTempParams (UINT4 u4Idx, INT4 i4DropType, INT4 i4QTempSize,
                        INT4 i4DropAlgo, INT4 i4RowStatus)
{
    INT4                i4RetStatus = SNMP_FAILURE;

    if (i4DropType != -1)
    {
        i4RetStatus = nmhSetFsQoSQTemplateDropType (u4Idx, i4DropType);
    }

    if (i4QTempSize != -1)
    {
        i4RetStatus = nmhSetFsQoSQTemplateSize (u4Idx, i4QTempSize);
    }
    if (i4DropAlgo != -1)
    {
        i4RetStatus = nmhSetFsQoSQTemplateDropAlgoEnableFlag (u4Idx,
                                                              i4DropAlgo);
    }

    i4RetStatus = nmhSetFsQoSQTemplateStatus (u4Idx, i4RowStatus);

    UNUSED_PARAM (i4RetStatus);

}

/*****************************************************************************/
/* Function Name      : QoSCliSetQTempParams                                 */
/* Description        : This function is used to Set QTemplate Table Entry   */
/*                      Parameter                                            */
/* Input(s)           : CliHandle    - Handle to CLI session                 */
/*                    : i4DropType   - Q Drop Type                           */
/*                    : i4QTempSize  - Q Size                                */
/*                    : i4DropAlgo   - Q Drop Algorithm                      */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : CLI_SUCCESS/CLI_FAILURE                              */
/* Called By          : cli_process_qos_cmd                                  */
/* Calling Function   : SNMP nmh Routines                                    */
/*****************************************************************************/
INT4
QoSCliSetQTempParams (tCliHandle CliHandle, INT4 i4DropType, INT4 i4QTempSize,
                      INT4 i4DropAlgo)
{
    UINT4               u4Idx = 0;
    INT4                i4RetStatus = SNMP_FAILURE;
    INT4                i4RowStatus = SNMP_FAILURE;
    UINT4               u4ErrorCode = SNMP_ERR_NO_ERROR;
    INT4                i4CurDropType = -1;
    INT4                i4CurQTempSize = -1;
    INT4                i4CurDropAlgo = -1;

    /* Get the Class Map Id */
    u4Idx = CLI_GET_QTEMP_ID ();

    /* Get the Status of this Entry */
    i4RetStatus = nmhGetFsQoSQTemplateStatus (u4Idx, &i4RowStatus);
    if (i4RetStatus == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    /* if RowStatus = NOT_IN_SERVICE or NOT_READY test and set the Values. 
     * if RowStatus = ACTIVE Set the RowStatus as NOT_IN_SERVICE and than   
     * test and set the Values 
     */
    if (i4RowStatus == ACTIVE)
    {
        i4RetStatus = nmhTestv2FsQoSQTemplateStatus (&u4ErrorCode,
                                                     u4Idx, NOT_IN_SERVICE);
        if (i4RetStatus == SNMP_FAILURE)
        {
            return (CLI_FAILURE);
        }

        i4RetStatus = nmhSetFsQoSQTemplateStatus (u4Idx, NOT_IN_SERVICE);
        if (i4RetStatus == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }
    }

    /* Test & Set the each value */
    if (i4DropType != -1)
    {
        i4RetStatus = nmhTestv2FsQoSQTemplateDropType (&u4ErrorCode, u4Idx,
                                                       i4DropType);
        if (i4RetStatus == SNMP_FAILURE)
        {
            QoSCliRSSetQTempParams (u4Idx, i4CurDropType, i4CurQTempSize,
                                    i4CurDropAlgo, i4RowStatus);
            return (CLI_FAILURE);
        }

        i4RetStatus = nmhGetFsQoSQTemplateDropType (u4Idx, &i4CurDropType);
        if (i4RetStatus == SNMP_FAILURE)
        {
            return (CLI_FAILURE);
        }

        i4RetStatus = nmhSetFsQoSQTemplateDropType (u4Idx, i4DropType);
        if (i4RetStatus == SNMP_FAILURE)
        {
            QoSCliRSSetQTempParams (u4Idx, i4CurDropType, i4CurQTempSize,
                                    i4CurDropAlgo, i4RowStatus);
            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }
    }

    if (i4QTempSize != -1)
    {
        i4RetStatus = nmhTestv2FsQoSQTemplateSize (&u4ErrorCode, u4Idx,
                                                   i4QTempSize);
        if (i4RetStatus == SNMP_FAILURE)
        {
            QoSCliRSSetQTempParams (u4Idx, i4CurDropType, i4CurQTempSize,
                                    i4CurDropAlgo, i4RowStatus);
            return (CLI_FAILURE);
        }

        i4RetStatus = nmhGetFsQoSQTemplateSize (u4Idx,
                                                (UINT4 *) &i4CurQTempSize);
        if (i4RetStatus == SNMP_FAILURE)
        {
            return (CLI_FAILURE);
        }

        i4RetStatus = nmhSetFsQoSQTemplateSize (u4Idx, i4QTempSize);
        if (i4RetStatus == SNMP_FAILURE)
        {
            QoSCliRSSetQTempParams (u4Idx, i4CurDropType, i4CurQTempSize,
                                    i4CurDropAlgo, i4RowStatus);
            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }
    }

    if (i4DropAlgo != -1)
    {
        i4RetStatus = nmhTestv2FsQoSQTemplateDropAlgoEnableFlag (&u4ErrorCode,
                                                                 u4Idx,
                                                                 i4DropAlgo);
        if (i4RetStatus == SNMP_FAILURE)
        {
            QoSCliRSSetQTempParams (u4Idx, i4CurDropType, i4CurQTempSize,
                                    i4CurDropAlgo, i4RowStatus);
            return (CLI_FAILURE);
        }

        i4RetStatus = nmhGetFsQoSQTemplateDropAlgoEnableFlag (u4Idx,
                                                              &i4CurDropAlgo);
        if (i4RetStatus == SNMP_FAILURE)
        {
            return (CLI_FAILURE);
        }

        i4RetStatus = nmhSetFsQoSQTemplateDropAlgoEnableFlag (u4Idx,
                                                              i4DropAlgo);
        if (i4RetStatus == SNMP_FAILURE)
        {
            QoSCliRSSetQTempParams (u4Idx, i4CurDropType, i4CurQTempSize,
                                    i4CurDropAlgo, i4RowStatus);
            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }
    }

    /* Active the Row */
    i4RetStatus = nmhTestv2FsQoSQTemplateStatus (&u4ErrorCode, u4Idx, ACTIVE);
    if (i4RetStatus == SNMP_FAILURE)
    {
        QoSCliRSSetQTempParams (u4Idx, i4CurDropType, i4CurQTempSize,
                                i4CurDropAlgo, i4RowStatus);
        return (CLI_FAILURE);
    }

    i4RetStatus = nmhSetFsQoSQTemplateStatus (u4Idx, ACTIVE);
    if (i4RetStatus == SNMP_FAILURE)
    {
        QoSCliRSSetQTempParams (u4Idx, i4CurDropType, i4CurQTempSize,
                                i4CurDropAlgo, i4RowStatus);
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }
    return (CLI_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSCliUtlDisplayQTempEntry                           */
/* Description        : This function is used to Display the QTemplate       */
/*                      Table Entries Parameters and RD Cfg also.            */
/* Input(s)           : CliHandle    - Handle to CLI session                 */
/*                    : u4Idx        - Table Entries Index                   */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : CLI_SUCCESS/CLI_FAILURE                              */
/* Called By          : QoSCliShowQTempEntry                                 */
/* Calling Function   : SNMP nmh Routines                                    */
/*****************************************************************************/
INT4
QoSCliUtlDisplayQTempEntry (tCliHandle CliHandle, UINT4 u4Idx)
{
    INT4                i4RetStatus = SNMP_FAILURE;
    INT4                i4RowStatus = SNMP_FAILURE;
    INT4                i4DropType = 0;
    INT4                i4DropAlgoEnable = 0;
    UINT4               u4QTempSize = 0;
    INT4                i4DP = 0;
    UINT4               u4MinTH = 0;
    UINT4               u4MaxTH = 0;
    UINT4               u4MaxPktSize = 0;
    UINT4               u4MaxProb = 0;
    UINT4               u4ExpWeight = 0;
    UINT4               u4TmpIdx = 0;
    UINT4               u4Gain = 0;
    INT4                i4DropThType = 0;
    UINT4               u4ECNThresh = 0;
    tSNMP_OCTET_STRING_TYPE *pOctStrFlag = NULL;
    UINT2               u2ActMask = 0x1;
    UINT1               u1RDActFlag = 0;
    u4TmpIdx = u4Idx;

    /* Get All Fields */
    i4RetStatus = nmhGetFsQoSQTemplateDropType (u4Idx, &i4DropType);
    if (i4RetStatus == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    i4RetStatus = nmhGetFsQoSQTemplateDropAlgoEnableFlag (u4Idx,
                                                          &i4DropAlgoEnable);
    if (i4RetStatus == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    i4RetStatus = nmhGetFsQoSQTemplateSize (u4Idx, &u4QTempSize);
    if (i4RetStatus == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    /* Print */
    CliPrintf (CliHandle, "%-28s : %d\r\n", "Q Template Id", u4Idx);
    CliPrintf (CliHandle, "%-28s : %d\r\n", "Q Limit", u4QTempSize);

    switch (i4DropType)
    {
        case QOS_Q_TEMP_DROP_TYPE_TAIL:
            CliPrintf (CliHandle, "%-28s : %s\r\n", "Drop Type", "Tail Drop");
            break;

        case QOS_Q_TEMP_DROP_TYPE_HEAD:
            CliPrintf (CliHandle, "%-28s : %s\r\n", "Drop Type", "Head Drop");
            break;

        case QOS_Q_TEMP_DROP_TYPE_RED:
            CliPrintf (CliHandle, "%-28s : %s\r\n", "Drop Type", "RED");
            break;

        case QOS_Q_TEMP_DROP_TYPE_WRED:
            CliPrintf (CliHandle, "%-28s : %s\r\n", "Drop Type", "WRED");
            break;

        case QOS_Q_TEMP_DROP_TYPE_OTHER:
            CliPrintf (CliHandle, "%-28s : %s\r\n", "Drop Type", "OTHER");
            break;

        case QOS_Q_TEMP_DROP_TYPE_ALWAYS:
            CliPrintf (CliHandle, "%-28s : %s\r\n", "Drop Type", "Always Drop");
            break;
        default:
            CliPrintf (CliHandle, "%-28s : %s\r\n", "Drop Type", "Invalid");
            break;
    }

    if (i4DropAlgoEnable == QOS_Q_TEMP_DROP_ALGO_ENABLE)
    {
        CliPrintf (CliHandle, "%-28s : %s\r\n", "Drop Algo Status", "Enable");
    }
    else
    {
        CliPrintf (CliHandle, "%-28s : %s\r\n", "Drop Algo Status", "Disable");
    }

    /* Display RD Cfg */

    i4RetStatus = nmhGetFirstIndexFsQoSRandomDetectCfgTable (&u4Idx, &i4DP);
    if (i4RetStatus == SNMP_FAILURE)
    {
        return (QOS_FAILURE);
    }

    do
    {
        i4RetStatus = nmhGetFsQoSRandomDetectCfgStatus (u4Idx, i4DP,
                                                        &i4RowStatus);
        if (i4RetStatus == SNMP_FAILURE)
        {
            return (CLI_FAILURE);
        }

        if ((i4RetStatus == ACTIVE) && (u4Idx == u4TmpIdx))
        {
            i4RetStatus = nmhGetFsQoSRandomDetectCfgMinAvgThresh (u4Idx, i4DP,
                                                                  &u4MinTH);
            if (i4RetStatus == SNMP_FAILURE)
            {
                return (CLI_FAILURE);
            }

            i4RetStatus = nmhGetFsQoSRandomDetectCfgMaxAvgThresh (u4Idx, i4DP,
                                                                  &u4MaxTH);
            if (i4RetStatus == SNMP_FAILURE)
            {
                return (CLI_FAILURE);
            }

            i4RetStatus = nmhGetFsQoSRandomDetectCfgMaxPktSize (u4Idx, i4DP,
                                                                &u4MaxPktSize);
            if (i4RetStatus == SNMP_FAILURE)
            {
                return (CLI_FAILURE);
            }

            i4RetStatus = nmhGetFsQoSRandomDetectCfgMaxProb (u4Idx, i4DP,
                                                             &u4MaxProb);
            if (i4RetStatus == SNMP_FAILURE)
            {
                return (CLI_FAILURE);
            }

            i4RetStatus = nmhGetFsQoSRandomDetectCfgExpWeight (u4Idx, i4DP,
                                                               &u4ExpWeight);
            if (i4RetStatus == SNMP_FAILURE)
            {
                return (CLI_FAILURE);
            }

            i4RetStatus = nmhGetFsQoSRandomDetectCfgGain (u4Idx, i4DP, &u4Gain);
            if (i4RetStatus == SNMP_FAILURE)
            {
                return (CLI_FAILURE);
            }

            i4RetStatus = nmhGetFsQoSRandomDetectCfgDropThreshType (u4Idx, i4DP,
                                                                    &i4DropThType);
            if (i4RetStatus == SNMP_FAILURE)
            {
                return (CLI_FAILURE);
            }

            i4RetStatus = nmhGetFsQoSRandomDetectCfgECNThresh (u4Idx, i4DP,
                                                               &u4ECNThresh);
            if (i4RetStatus == SNMP_FAILURE)
            {
                return (CLI_FAILURE);
            }
            /* RD Config Actions */
            /* Allocate Memory For Name */
            pOctStrFlag =
                allocmem_octetstring (QOS_RD_CONFIG_ACTION_FLAG_LENGTH);
            if (pOctStrFlag == NULL)
            {
                return (CLI_FAILURE);
            }

            MEMSET (pOctStrFlag->pu1_OctetList, 0, pOctStrFlag->i4_Length);
            i4RetStatus = nmhGetFsQoSRandomDetectCfgActionFlag (u4Idx, i4DP,
                                                                pOctStrFlag);
            if (i4RetStatus == SNMP_FAILURE)
            {
                free_octetstring (pOctStrFlag);
                return (CLI_FAILURE);
            }

            /* Get the Action Flag */
            MEMCPY (&u1RDActFlag, pOctStrFlag->pu1_OctetList,
                    pOctStrFlag->i4_Length);
            free_octetstring (pOctStrFlag);

            CliPrintf (CliHandle, "DP %d, MinTH %d, MaxTH %d, MaxPktSize %d,"
                       "MaxDropProb %d, ExpWeight %d, Gain %d, ECN Threshold %d\r\n",
                       i4DP, u4MinTH, u4MaxTH, u4MaxPktSize, u4MaxProb,
                       u4ExpWeight, u4Gain, u4ECNThresh);
            if (i4DropThType == QOS_CLI_RD_CFG_DISCARD_PKTS)
            {
                CliPrintf (CliHandle,
                           "Drop threshold type : Discard Packets\r\n");
            }
            else if (i4DropThType == QOS_CLI_RD_CFG_DISCARD_BYTES)
            {
                CliPrintf (CliHandle,
                           "Drop threshold type : Discard Bytes\r\n");
            }
            if (u1RDActFlag != 0x00)
            {
                CliPrintf (CliHandle, "RD Cfg Flag : ");
                while (u2ActMask < QOS_RD_CONFIG_ACTION_MAX_VAL)
                {
                    switch ((u1RDActFlag & u2ActMask))
                    {
                        case QOS_RD_CONFIG_CAP_AVERAGE:
                            CliPrintf (CliHandle, "cap-average ");
                            break;
                        case QOS_RD_CONFIG_MARK_ECN:
                            CliPrintf (CliHandle, "mark-ecn ");
                            break;
                        default:
                            break;
                    }
                    u2ActMask = (UINT2) (u2ActMask << 1);
                }
            }
            else
            {
                CliPrintf (CliHandle, "RD Cfg Flag : none");
            }
            CliPrintf (CliHandle, "\r\n");

        }

        i4RetStatus = nmhGetNextIndexFsQoSRandomDetectCfgTable (u4Idx, &u4Idx,
                                                                i4DP, &i4DP);
        u2ActMask = 0x1;
    }
    while (i4RetStatus == SNMP_SUCCESS);

    CliPrintf (CliHandle, "\r\n");

    return (CLI_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSCliShowQTempEntry                                 */
/* Description        : This function is used to Display the QTemplate       */
/*                      Table Entries Parameters for the QTemplate Id, if    */
/*                      it is NULL then All Entries will be displayed        */
/* Input(s)           : CliHandle    - Handle to CLI session                 */
/*                    : u4Idx        - Table Entries Name to Display         */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : CLI_SUCCESS/CLI_FAILURE                              */
/* Called By          : cli_process_qos_cmd                                  */
/* Calling Function   : QoSCliUtlDisplayPriMapEntry                          */
/*****************************************************************************/
INT4
QoSCliShowQTempEntry (tCliHandle CliHandle, UINT4 u4Idx)
{
    INT4                i4RetStatus = SNMP_FAILURE;
    INT4                i4RowStatus = SNMP_FAILURE;

    CliPrintf (CliHandle, "Queue Template Entries\r\n");
    CliPrintf (CliHandle, "----------------------\r\n");

    /* Display All */
    if (u4Idx == 0)
    {
        i4RetStatus = nmhGetFirstIndexFsQoSQTemplateTable (&u4Idx);
        if (i4RetStatus == SNMP_FAILURE)
        {
            return (CLI_FAILURE);
        }

        do
        {
            /* Check the Status */
            i4RetStatus = nmhGetFsQoSQTemplateStatus (u4Idx, &i4RowStatus);
            if (i4RetStatus == SNMP_FAILURE)
            {
                return (CLI_FAILURE);
            }

            if (i4RowStatus == ACTIVE)
            {
                /* Display Entry */
                i4RetStatus = QoSCliUtlDisplayQTempEntry (CliHandle, u4Idx);
                if (i4RetStatus == CLI_FAILURE)
                {
                    return (CLI_FAILURE);
                }
            }

            i4RetStatus = nmhGetNextIndexFsQoSQTemplateTable (u4Idx, &u4Idx);

        }
        while (i4RetStatus == SNMP_SUCCESS);

        return (CLI_SUCCESS);
    }
    else
    {
        /* Check the Status */
        i4RetStatus = nmhGetFsQoSQTemplateStatus (u4Idx, &i4RowStatus);
        if (i4RetStatus == SNMP_FAILURE)
        {
            return (CLI_FAILURE);
        }

        if (i4RowStatus == ACTIVE)
        {
            i4RetStatus = QoSCliUtlDisplayQTempEntry (CliHandle, u4Idx);
            if (i4RetStatus == CLI_FAILURE)
            {
                return (CLI_FAILURE);
            }
        }
    }

    return (CLI_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSCliDelQTempEntry                                  */
/* Description        : This function is used to delete QTemplate table      */
/*                      Entry.                                               */
/* Input(s)           : CliHandle    - Handle to CLI session                 */
/*                    : u4QTemp      - Q Template Id to Delete               */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : CLI_SUCCESS/CLI_FAILURE                              */
/* Called By          : cli_process_qos_cmd                                  */
/* Calling Function   : SNMP nmh Routines                                    */
/*****************************************************************************/
INT4
QoSCliDelQTempEntry (tCliHandle CliHandle, UINT4 u4QTemp)
{
    INT4                i4RetStatus = SNMP_FAILURE;
    UINT4               u4ErrorCode = SNMP_ERR_NO_ERROR;

    UNUSED_PARAM (CliHandle);

    /* Delete a Entry */
    i4RetStatus = nmhTestv2FsQoSQTemplateStatus (&u4ErrorCode, u4QTemp,
                                                 DESTROY);
    if (i4RetStatus == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    i4RetStatus = nmhSetFsQoSQTemplateStatus (u4QTemp, DESTROY);
    if (i4RetStatus == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }

    return (CLI_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSCliRSSetRDCfgParams                               */
/* Description        : This function is used to Reset RD Cfg table Entry    */
/*                      Parameter                                            */
/* Input(s)           : u4Idx        - Table Index                           */
/*                    : i4DP         - Drop prec. Value.                     */
/*                    : i4MinTH      - RD Cfg min threshold                  */
/*                    : i4MaxTH      - RD Cfg max threshold                  */
/*                    : i4MaxPktSize - RD Cfg Max Packet Size                */
/*                    : i4MaxProb    - RD Cfg Max Drop Probability           */
/*                    : i4ExpWeight  - RD Cfg Exp Weight                     */
/*                    : i4RowStatus  - RowStatus                             */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : CLI_SUCCESS/CLI_FAILURE                              */
/* Called By          : cli_process_qos_cmd                                  */
/* Calling Function   : SNMP nmh Routines                                    */
/*****************************************************************************/
VOID
QoSCliRSSetRDCfgParams (UINT4 u4Idx, INT4 i4DP, INT4 i4MinTH, INT4 i4MaxTH,
                        INT4 i4MaxPktSize, INT4 i4MaxProb, INT4 i4ExpWeight,
                        INT4 i4RowStatus)
{
    INT4                i4RetStatus = SNMP_FAILURE;

    if (i4RowStatus == 0)
    {
        i4RetStatus = nmhSetFsQoSRandomDetectCfgStatus (u4Idx, i4DP, DESTROY);
        return;
    }

    if (i4MinTH != -1)
    {
        i4RetStatus = nmhSetFsQoSRandomDetectCfgMinAvgThresh (u4Idx, i4DP,
                                                              (UINT4) i4MinTH);
    }

    if (i4MaxTH != -1)
    {
        i4RetStatus = nmhSetFsQoSRandomDetectCfgMaxAvgThresh (u4Idx, i4DP,
                                                              (UINT4) i4MaxTH);
    }

    if (i4MaxPktSize != -1)
    {
        i4RetStatus = nmhSetFsQoSRandomDetectCfgMaxPktSize (u4Idx, i4DP,
                                                            (UINT4)
                                                            i4MaxPktSize);
    }

    if (i4MaxProb != -1)
    {
        i4RetStatus = nmhSetFsQoSRandomDetectCfgMaxProb (u4Idx, i4DP,
                                                         (UINT4) i4MaxProb);
    }

    if (i4ExpWeight != -1)
    {
        i4RetStatus = nmhSetFsQoSRandomDetectCfgExpWeight (u4Idx, i4DP,
                                                           (UINT4) i4ExpWeight);
    }

    i4RetStatus = nmhSetFsQoSRandomDetectCfgStatus (u4Idx, i4DP, i4RowStatus);

    UNUSED_PARAM (i4RetStatus);

}

/*****************************************************************************/
/* Function Name      : QoSCliSetRDCfgParams                                 */
/* Description        : This function is used to Set RD Cfg table Entry      */
/*                      Parameter                                            */
/* Input(s)           : CliHandle    - Handle to CLI session                 */
/*                    : i4DP         - Drop prec. Value.                     */
/*                    : i4MinTH      - RD Cfg min threshold                  */
/*                    : i4MaxTH      - RD Cfg max threshold                  */
/*                    : i4MaxPktSize - RD Cfg Max Packet Size                */
/*                    : i4MaxProb    - RD Cfg Max Drop Probability           */
/*                    : i4ExpWeight  - RD Cfg Exp Weight                     */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : CLI_SUCCESS/CLI_FAILURE                              */
/* Called By          : cli_process_qos_cmd                                  */
/* Calling Function   : SNMP nmh Routines                                    */
/*****************************************************************************/
INT4
QoSCliSetRDCfgParams (tCliHandle CliHandle, INT4 i4DP, INT4 i4MinTH,
                      INT4 i4MaxTH, INT4 i4MaxPktSize, INT4 i4MaxProb,
                      INT4 i4ExpWeight, INT4 i4Gain, INT4 i4DropTHType,
                      INT4 i4ECNTH, UINT1 u1Flag)
{
    UINT4               u4Idx = 0;
    INT4                i4RetStatus = SNMP_FAILURE;
    INT4                i4RowStatus = 0;
    UINT4               u4ErrorCode = SNMP_ERR_NO_ERROR;
    INT4                i4CurMinTH = -1;
    INT4                i4CurMaxTH = -1;
    INT4                i4CurMaxPktSize = -1;
    INT4                i4CurMaxProb = -1;
    INT4                i4CurExpWeight = -1;
    INT4                i4PreMinTH = 0;
    INT4                i4PreMaxTH = 0;
    tSNMP_OCTET_STRING_TYPE *pOctStrActFlag = NULL;
    /* Get the QTemplate Id */
    u4Idx = CLI_GET_QTEMP_ID ();

    /* Get the Status of this Entry */
    /* if i4RetStatus = SNMP_FAILURE  Not yet Created  Set CREATE_AND_WAIT */
    i4RetStatus = nmhGetFsQoSRandomDetectCfgStatus (u4Idx, i4DP, &i4RowStatus);
    if (i4RetStatus == SNMP_FAILURE)
    {
        i4RetStatus = nmhTestv2FsQoSRandomDetectCfgStatus (&u4ErrorCode,
                                                           u4Idx, i4DP,
                                                           CREATE_AND_WAIT);
        if (i4RetStatus == SNMP_FAILURE)
        {
            return (CLI_FAILURE);
        }

        i4RetStatus = nmhSetFsQoSRandomDetectCfgStatus (u4Idx, i4DP,
                                                        CREATE_AND_WAIT);
        if (i4RetStatus == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }

    }
    /* if RowStatus = NOT_IN_SERVICE or NOT_READY test and set the Values. 
     * if RowStatus = ACTIVE Set the RowStatus as NOT_IN_SERVICE and than   
     * test and set the Values 
     */
    else if (i4RowStatus == ACTIVE)
    {
        i4RetStatus = nmhTestv2FsQoSRandomDetectCfgStatus (&u4ErrorCode,
                                                           u4Idx, i4DP,
                                                           NOT_IN_SERVICE);
        if (i4RetStatus == SNMP_FAILURE)
        {
            return (CLI_FAILURE);
        }

        i4RetStatus = nmhSetFsQoSRandomDetectCfgStatus (u4Idx, i4DP,
                                                        NOT_IN_SERVICE);
        if (i4RetStatus == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }
    }

    /* The Drop Threshold Type has to be set first, as the Min and Max threshold values
     * are dependent on the DropThresh Type. */
    if (i4DropTHType != -1)
    {
        i4RetStatus =
            nmhTestv2FsQoSRandomDetectCfgDropThreshType (&u4ErrorCode, u4Idx,
                                                         i4DP, i4DropTHType);
        if (i4RetStatus == SNMP_FAILURE)
        {
            QoSCliRSSetRDCfgParams (u4Idx, i4DP, i4CurMinTH, i4CurMaxTH,
                                    i4CurMaxPktSize, i4CurMaxProb,
                                    i4CurExpWeight, i4RowStatus);
            return (CLI_FAILURE);
        }

        i4RetStatus = nmhSetFsQoSRandomDetectCfgDropThreshType (u4Idx, i4DP,
                                                                i4DropTHType);
        if (i4RetStatus == SNMP_FAILURE)
        {
            QoSCliRSSetRDCfgParams (u4Idx, i4DP, i4CurMinTH, i4CurMaxTH,
                                    i4CurMaxPktSize, i4CurMaxProb,
                                    i4CurExpWeight, i4RowStatus);
            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }
    }

    /* When minimum and maximum threshold values are given by the user. 
     * Check if minimum threshold value is less than maximum threshold value */

    if ((i4MinTH != -1) && (i4MaxTH != -1))
    {
        if (i4MinTH > i4MaxTH)
        {
            CliPrintf (CliHandle,
                       "\r%% Min threshold should be less than Max threshold \r\n");
            QoSCliRSSetRDCfgParams (u4Idx, i4DP, i4CurMinTH, i4CurMaxTH,
                                    i4CurMaxPktSize, i4CurMaxProb,
                                    i4CurExpWeight, i4RowStatus);
            return (CLI_FAILURE);
        }
        i4RetStatus = nmhGetFsQoSRandomDetectCfgMaxAvgThresh (u4Idx,
                                                              i4DP,
                                                              (UINT4 *)
                                                              &i4PreMaxTH);
        if (i4RetStatus == SNMP_FAILURE)
        {
            return (CLI_FAILURE);
        }
        i4RetStatus = nmhGetFsQoSRandomDetectCfgMinAvgThresh (u4Idx,
                                                              i4DP,
                                                              (UINT4 *)
                                                              &i4PreMinTH);
        if (i4RetStatus == SNMP_FAILURE)
        {
            return (CLI_FAILURE);
        }
    }
    if (i4MinTH > i4PreMaxTH)
    {
        /*call set of max thershold first */
        if (i4MaxTH != -1)
        {
            i4RetStatus =
                nmhTestv2FsQoSRandomDetectCfgMaxAvgThresh (&u4ErrorCode, u4Idx,
                                                           i4DP,
                                                           (UINT4) i4MaxTH);
            if (i4RetStatus == SNMP_FAILURE)
            {
                QoSCliRSSetRDCfgParams (u4Idx, i4DP, i4CurMinTH, i4CurMaxTH,
                                        i4CurMaxPktSize, i4CurMaxProb,
                                        i4CurExpWeight, i4RowStatus);
                return (CLI_FAILURE);
            }
            i4RetStatus = nmhSetFsQoSRandomDetectCfgMaxAvgThresh (u4Idx, i4DP,
                                                                  (UINT4)
                                                                  i4MaxTH);
            if (i4RetStatus == SNMP_FAILURE)
            {
                QoSCliRSSetRDCfgParams (u4Idx, i4DP, i4CurMinTH, i4CurMaxTH,
                                        i4CurMaxPktSize, i4CurMaxProb,
                                        i4CurExpWeight, i4RowStatus);
                CLI_FATAL_ERROR (CliHandle);
                return (CLI_FAILURE);
            }
        }
        if (i4MinTH != -1)
        {
            i4RetStatus =
                nmhTestv2FsQoSRandomDetectCfgMinAvgThresh (&u4ErrorCode, u4Idx,
                                                           i4DP,
                                                           (UINT4) i4MinTH);
            if (i4RetStatus == SNMP_FAILURE)
            {
                QoSCliRSSetRDCfgParams (u4Idx, i4DP, i4CurMinTH, i4CurMaxTH,
                                        i4CurMaxPktSize, i4CurMaxProb,
                                        i4CurExpWeight, i4RowStatus);
                return (CLI_FAILURE);
            }
            i4RetStatus = nmhSetFsQoSRandomDetectCfgMinAvgThresh (u4Idx, i4DP,
                                                                  (UINT4)
                                                                  i4MinTH);
            if (i4RetStatus == SNMP_FAILURE)
            {
                QoSCliRSSetRDCfgParams (u4Idx, i4DP, i4CurMinTH, i4CurMaxTH,
                                        i4CurMaxPktSize, i4CurMaxProb,
                                        i4CurExpWeight, i4RowStatus);
                CLI_FATAL_ERROR (CliHandle);
                return (CLI_FAILURE);
            }
        }
    }
    else
    {
        /* Test & Set the each value */
        if (i4MinTH != -1)
        {
            i4RetStatus =
                nmhTestv2FsQoSRandomDetectCfgMinAvgThresh (&u4ErrorCode, u4Idx,
                                                           i4DP,
                                                           (UINT4) i4MinTH);
            if (i4RetStatus == SNMP_FAILURE)
            {
                QoSCliRSSetRDCfgParams (u4Idx, i4DP, i4CurMinTH, i4CurMaxTH,
                                        i4CurMaxPktSize, i4CurMaxProb,
                                        i4CurExpWeight, i4RowStatus);
                return (CLI_FAILURE);
            }

            i4RetStatus = nmhSetFsQoSRandomDetectCfgMinAvgThresh (u4Idx, i4DP,
                                                                  (UINT4)
                                                                  i4MinTH);
            if (i4RetStatus == SNMP_FAILURE)
            {
                QoSCliRSSetRDCfgParams (u4Idx, i4DP, i4CurMinTH, i4CurMaxTH,
                                        i4CurMaxPktSize, i4CurMaxProb,
                                        i4CurExpWeight, i4RowStatus);
                CLI_FATAL_ERROR (CliHandle);
                return (CLI_FAILURE);
            }
        }

        if (i4MaxTH != -1)
        {
            i4RetStatus =
                nmhTestv2FsQoSRandomDetectCfgMaxAvgThresh (&u4ErrorCode, u4Idx,
                                                           i4DP,
                                                           (UINT4) i4MaxTH);
            if (i4RetStatus == SNMP_FAILURE)
            {
                QoSCliRSSetRDCfgParams (u4Idx, i4DP, i4CurMinTH, i4CurMaxTH,
                                        i4CurMaxPktSize, i4CurMaxProb,
                                        i4CurExpWeight, i4RowStatus);
                return (CLI_FAILURE);
            }

            i4RetStatus = nmhSetFsQoSRandomDetectCfgMaxAvgThresh (u4Idx, i4DP,
                                                                  (UINT4)
                                                                  i4MaxTH);
            if (i4RetStatus == SNMP_FAILURE)
            {
                QoSCliRSSetRDCfgParams (u4Idx, i4DP, i4CurMinTH, i4CurMaxTH,
                                        i4CurMaxPktSize, i4CurMaxProb,
                                        i4CurExpWeight, i4RowStatus);
                CLI_FATAL_ERROR (CliHandle);
                return (CLI_FAILURE);
            }
        }

    }
    if (i4MaxPktSize != -1)
    {
        i4RetStatus =
            nmhTestv2FsQoSRandomDetectCfgMaxPktSize (&u4ErrorCode, u4Idx, i4DP,
                                                     (UINT4) i4MaxPktSize);
        if (i4RetStatus == SNMP_FAILURE)
        {
            QoSCliRSSetRDCfgParams (u4Idx, i4DP, i4CurMinTH, i4CurMaxTH,
                                    i4CurMaxPktSize, i4CurMaxProb,
                                    i4CurExpWeight, i4RowStatus);
            return (CLI_FAILURE);
        }

        i4RetStatus = nmhSetFsQoSRandomDetectCfgMaxPktSize (u4Idx, i4DP,
                                                            (UINT4)
                                                            i4MaxPktSize);
        if (i4RetStatus == SNMP_FAILURE)
        {
            QoSCliRSSetRDCfgParams (u4Idx, i4DP, i4CurMinTH, i4CurMaxTH,
                                    i4CurMaxPktSize, i4CurMaxProb,
                                    i4CurExpWeight, i4RowStatus);
            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }
    }

    if (i4MaxProb != -1)
    {
        i4RetStatus =
            nmhTestv2FsQoSRandomDetectCfgMaxProb (&u4ErrorCode, u4Idx, i4DP,
                                                  (UINT4) i4MaxProb);
        if (i4RetStatus == SNMP_FAILURE)
        {
            QoSCliRSSetRDCfgParams (u4Idx, i4DP, i4CurMinTH, i4CurMaxTH,
                                    i4CurMaxPktSize, i4CurMaxProb,
                                    i4CurExpWeight, i4RowStatus);
            return (CLI_FAILURE);
        }

        i4RetStatus = nmhSetFsQoSRandomDetectCfgMaxProb (u4Idx, i4DP,
                                                         (UINT4) i4MaxProb);
        if (i4RetStatus == SNMP_FAILURE)
        {
            QoSCliRSSetRDCfgParams (u4Idx, i4DP, i4CurMinTH, i4CurMaxTH,
                                    i4CurMaxPktSize, i4CurMaxProb,
                                    i4CurExpWeight, i4RowStatus);
            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }
    }

    if (i4ExpWeight != -1)
    {
        i4RetStatus =
            nmhTestv2FsQoSRandomDetectCfgExpWeight (&u4ErrorCode, u4Idx, i4DP,
                                                    (UINT4) i4ExpWeight);
        if (i4RetStatus == SNMP_FAILURE)
        {
            QoSCliRSSetRDCfgParams (u4Idx, i4DP, i4CurMinTH, i4CurMaxTH,
                                    i4CurMaxPktSize, i4CurMaxProb,
                                    i4CurExpWeight, i4RowStatus);
            return (CLI_FAILURE);
        }

        i4RetStatus = nmhSetFsQoSRandomDetectCfgExpWeight (u4Idx, i4DP,
                                                           (UINT4) i4ExpWeight);
        if (i4RetStatus == SNMP_FAILURE)
        {
            QoSCliRSSetRDCfgParams (u4Idx, i4DP, i4CurMinTH, i4CurMaxTH,
                                    i4CurMaxPktSize, i4CurMaxProb,
                                    i4CurExpWeight, i4RowStatus);
            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }
    }

    if (i4Gain != -1)
    {
        i4RetStatus =
            nmhTestv2FsQoSRandomDetectCfgGain (&u4ErrorCode, u4Idx, i4DP,
                                               (UINT4) i4Gain);
        if (i4RetStatus == SNMP_FAILURE)
        {
            QoSCliRSSetRDCfgParams (u4Idx, i4DP, i4CurMinTH, i4CurMaxTH,
                                    i4CurMaxPktSize, i4CurMaxProb,
                                    i4CurExpWeight, i4RowStatus);
            return (CLI_FAILURE);
        }

        i4RetStatus = nmhSetFsQoSRandomDetectCfgGain (u4Idx, i4DP,
                                                      (UINT4) i4Gain);
        if (i4RetStatus == SNMP_FAILURE)
        {
            QoSCliRSSetRDCfgParams (u4Idx, i4DP, i4CurMinTH, i4CurMaxTH,
                                    i4CurMaxPktSize, i4CurMaxProb,
                                    i4CurExpWeight, i4RowStatus);
            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }
    }

    if (i4ECNTH != -1)
    {
        i4RetStatus =
            nmhTestv2FsQoSRandomDetectCfgECNThresh (&u4ErrorCode, u4Idx, i4DP,
                                                    (UINT4) i4ECNTH);
        if (i4RetStatus == SNMP_FAILURE)
        {
            QoSCliRSSetRDCfgParams (u4Idx, i4DP, i4CurMinTH, i4CurMaxTH,
                                    i4CurMaxPktSize, i4CurMaxProb,
                                    i4CurExpWeight, i4RowStatus);
            return (CLI_FAILURE);
        }
        i4RetStatus = nmhSetFsQoSRandomDetectCfgECNThresh (u4Idx, i4DP,
                                                           (UINT4) i4ECNTH);
        if (i4RetStatus == SNMP_FAILURE)
        {
            QoSCliRSSetRDCfgParams (u4Idx, i4DP, i4CurMinTH, i4CurMaxTH,
                                    i4CurMaxPktSize, i4CurMaxProb,
                                    i4CurExpWeight, i4RowStatus);
            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }
    }

    if (u1Flag != 0)
    {
        /* Allocate Memory For InActFlag */
        pOctStrActFlag =
            allocmem_octetstring (QOS_RD_CONFIG_ACTION_FLAG_LENGTH);
        if (pOctStrActFlag == NULL)
        {
            return (CLI_FAILURE);
        }
        MEMSET (pOctStrActFlag->pu1_OctetList, 0, pOctStrActFlag->i4_Length);
        MEMCPY (pOctStrActFlag->pu1_OctetList, &u1Flag,
                QOS_RD_CONFIG_ACTION_FLAG_LENGTH);

        i4RetStatus =
            nmhTestv2FsQoSRandomDetectCfgActionFlag (&u4ErrorCode, u4Idx, i4DP,
                                                     pOctStrActFlag);
        if (i4RetStatus == SNMP_FAILURE)
        {
            QoSCliRSSetRDCfgParams (u4Idx, i4DP, i4CurMinTH, i4CurMaxTH,
                                    i4CurMaxPktSize, i4CurMaxProb,
                                    i4CurExpWeight, i4RowStatus);
            free_octetstring (pOctStrActFlag);
            return (CLI_FAILURE);
        }
        i4RetStatus = nmhSetFsQoSRandomDetectCfgActionFlag (u4Idx, i4DP,
                                                            pOctStrActFlag);
        if (i4RetStatus == SNMP_FAILURE)
        {
            QoSCliRSSetRDCfgParams (u4Idx, i4DP, i4CurMinTH, i4CurMaxTH,
                                    i4CurMaxPktSize, i4CurMaxProb,
                                    i4CurExpWeight, i4RowStatus);
            free_octetstring (pOctStrActFlag);
            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }
        free_octetstring (pOctStrActFlag);
    }

    /* Active the Row */
    i4RetStatus = nmhTestv2FsQoSRandomDetectCfgStatus (&u4ErrorCode, u4Idx,
                                                       i4DP, ACTIVE);
    if (i4RetStatus == SNMP_FAILURE)
    {
        QoSCliRSSetRDCfgParams (u4Idx, i4DP, i4CurMinTH, i4CurMaxTH,
                                i4CurMaxPktSize, i4CurMaxProb, i4CurExpWeight,
                                i4RowStatus);
        return (CLI_FAILURE);
    }

    i4RetStatus = nmhSetFsQoSRandomDetectCfgStatus (u4Idx, i4DP, ACTIVE);
    if (i4RetStatus == SNMP_FAILURE)
    {
        QoSCliRSSetRDCfgParams (u4Idx, i4DP, i4CurMinTH, i4CurMaxTH,
                                i4CurMaxPktSize, i4CurMaxProb, i4CurExpWeight,
                                i4RowStatus);
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }

    return (CLI_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSCliDelRDCfgEntry                                  */
/* Description        : This function is used to delete RD Cfg table         */
/*                      Entry.                                               */
/* Input(s)           : CliHandle    - Handle to CLI session                 */
/*                    : i4DP         - Drop prec. Value.                     */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : CLI_SUCCESS/CLI_FAILURE                              */
/* Called By          : cli_process_qos_cmd                                  */
/* Calling Function   : SNMP nmh Routines                                    */
/*****************************************************************************/
INT4
QoSCliDelRDCfgEntry (tCliHandle CliHandle, INT4 i4DP)
{
    INT4                i4RetStatus = SNMP_FAILURE;
    UINT4               u4ErrorCode = SNMP_ERR_NO_ERROR;
    UINT4               u4Idx = 0;

    UNUSED_PARAM (CliHandle);

    /* Get the QTemplate Id */
    u4Idx = CLI_GET_QTEMP_ID ();

    /* Delete a Entry */
    i4RetStatus = nmhTestv2FsQoSRandomDetectCfgStatus (&u4ErrorCode, u4Idx,
                                                       i4DP, DESTROY);
    if (i4RetStatus == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    i4RetStatus = nmhSetFsQoSRandomDetectCfgStatus (u4Idx, i4DP, DESTROY);
    if (i4RetStatus == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }

    return (CLI_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSCliRSShapeTempEntry                               */
/* Description        : This function is used to Reset Shape Template table  */
/*                      Entry .                                              */
/* Input(s)           : u4Idx            - Shape Tbl Entry index             */
/*                    : u4CurrCIR        - CIR Value                         */
/*                    : u4CurrCBS        - CBS Value                         */
/*                    : u4CurrEIR        - EIR Value                         */
/*                    : u4CurrEBS        - EBS Value                         */
/*                    : i4RowStatus      - Row Status                        */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : CLI_SUCCESS/CLI_FAILURE                              */
/* Called By          : cli_process_qos_cmd                                  */
/* Calling Function   : SNMP nmh Routines                                    */
/*****************************************************************************/
VOID
QoSCliRSShapeTempEntry (UINT4 u4Idx, UINT4 u4CurrCIR, UINT4 u4CurrCBS,
                        UINT4 u4CurrEIR, UINT4 u4CurrEBS, INT4 i4RowStatus)
{
    INT4                i4RetStatus = SNMP_FAILURE;

    if (i4RowStatus == 0)
    {
        i4RetStatus = nmhSetFsQoSShapeTemplateStatus (u4Idx, DESTROY);
        return;
    }

    if (u4CurrCIR != (UINT4) CLI_RS_INVALID_MASK)
    {
        i4RetStatus = nmhSetFsQoSShapeTemplateCIR (u4Idx, u4CurrCIR);
    }

    if (u4CurrCBS != (UINT4) CLI_RS_INVALID_MASK)
    {
        i4RetStatus = nmhSetFsQoSShapeTemplateCBS (u4Idx, u4CurrCBS);
    }

    if (u4CurrEIR != (UINT4) CLI_RS_INVALID_MASK)
    {
        i4RetStatus = nmhSetFsQoSShapeTemplateEIR (u4Idx, u4CurrEIR);
    }

    if (u4CurrEBS != (UINT4) CLI_RS_INVALID_MASK)
    {
        i4RetStatus = nmhSetFsQoSShapeTemplateEBS (u4Idx, u4CurrEBS);
    }

    i4RetStatus = nmhSetFsQoSShapeTemplateStatus (u4Idx, i4RowStatus);

    UNUSED_PARAM (i4RetStatus);

}

/*****************************************************************************/
/* Function Name      : QoSCliAddShapeTempEntry                              */
/* Description        : This function is used to create Shape Tmplate table  */
/*                      Entry .                                              */
/* Input(s)           : CliHandle    - Handle to CLI session                 */
/*                    : u4Idx        - Shape Tbl Entry index                 */
/*                    : i4CIR        - CIR Value                             */
/*                    : i4CBS        - CBS Value                             */
/*                    : i4EIR        - EIR Value                             */
/*                    : i4EBS        - CIR Value                             */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : CLI_SUCCESS/CLI_FAILURE                              */
/* Called By          : cli_process_qos_cmd                                  */
/* Calling Function   : SNMP nmh Routines                                    */
/*****************************************************************************/
INT4
QoSCliAddShapeTempEntry (tCliHandle CliHandle, UINT4 u4Idx, INT4 i4CIR,
                         INT4 i4CBS, INT4 i4EIR, INT4 i4EBS)
{
    UINT4               u4ErrorCode = 0;
    INT4                i4RetStatus = SNMP_FAILURE;
    INT4                i4RowStatus = 0;
    UINT4               u4CurCIR = (UINT4) CLI_RS_INVALID_MASK;
    UINT4               u4CurCBS = (UINT4) CLI_RS_INVALID_MASK;
    UINT4               u4CurEIR = (UINT4) CLI_RS_INVALID_MASK;
    UINT4               u4CurEBS = (UINT4) CLI_RS_INVALID_MASK;

    /* 1. Check the Table Entry 
     * 2. If the Entry exists Set it as NOT_IN_SERVICE
     * 3. Configure the Given Values.
     * 4. If not exists create and Configure the Values.
     * 5. Any Failure in the New Entry Configurations Delete the Entry.
     * 6. Any Failure in the Modification of an Entry will Restore the 
     *    old Values.
     */
    i4RetStatus = nmhGetFsQoSShapeTemplateStatus (u4Idx, &i4RowStatus);
    if (i4RetStatus == QOS_FAILURE)
    {
        i4RetStatus = nmhTestv2FsQoSShapeTemplateStatus (&u4ErrorCode,
                                                         u4Idx,
                                                         CREATE_AND_WAIT);
        if (i4RetStatus == SNMP_FAILURE)
        {
            return (CLI_FAILURE);
        }

        i4RetStatus = nmhSetFsQoSShapeTemplateStatus (u4Idx, CREATE_AND_WAIT);
        if (i4RetStatus == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }
    }
    else if (i4RowStatus == ACTIVE)
    {
        i4RetStatus = nmhTestv2FsQoSShapeTemplateStatus (&u4ErrorCode,
                                                         u4Idx, NOT_IN_SERVICE);
        if (i4RetStatus == SNMP_FAILURE)
        {
            return (CLI_FAILURE);
        }

        i4RetStatus = nmhSetFsQoSShapeTemplateStatus (u4Idx, NOT_IN_SERVICE);
        if (i4RetStatus == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }
    }

    if (i4CIR != -1)
    {
        i4RetStatus = QoSUtilTestFsQoSShapeTemplateCIR (&u4ErrorCode, u4Idx,
                                                        (UINT4) i4CIR,
                                                        (UINT4) i4EIR);
        if (i4RetStatus == SNMP_FAILURE)
        {
            QoSCliRSShapeTempEntry (u4Idx, u4CurCIR, u4CurCBS, u4CurEIR,
                                    u4CurEBS, i4RowStatus);
            return (CLI_FAILURE);
        }

        i4RetStatus = nmhGetFsQoSShapeTemplateCIR (u4Idx, &u4CurCIR);
        if (i4RetStatus == SNMP_FAILURE)
        {
            return (CLI_FAILURE);
        }

        i4RetStatus = nmhSetFsQoSShapeTemplateCIR (u4Idx, (UINT4) i4CIR);
        if (i4RetStatus == SNMP_FAILURE)
        {
            QoSCliRSShapeTempEntry (u4Idx, u4CurCIR, u4CurCBS, u4CurEIR,
                                    u4CurEBS, i4RowStatus);
            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }
    }

    if (i4CBS != -1)
    {
        i4RetStatus = nmhTestv2FsQoSShapeTemplateCBS (&u4ErrorCode, u4Idx,
                                                      (UINT4) i4CBS);
        if (i4RetStatus == SNMP_FAILURE)
        {
            QoSCliRSShapeTempEntry (u4Idx, u4CurCIR, u4CurCBS, u4CurEIR,
                                    u4CurEBS, i4RowStatus);
            return (CLI_FAILURE);
        }

        i4RetStatus = nmhGetFsQoSShapeTemplateCBS (u4Idx, &u4CurCBS);
        if (i4RetStatus == SNMP_FAILURE)
        {
            return (CLI_FAILURE);
        }

        i4RetStatus = nmhSetFsQoSShapeTemplateCBS (u4Idx, (UINT4) i4CBS);
        if (i4RetStatus == SNMP_FAILURE)
        {
            QoSCliRSShapeTempEntry (u4Idx, u4CurCIR, u4CurCBS, u4CurEIR,
                                    u4CurEBS, i4RowStatus);
            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }
    }

    if (i4EIR != -1)
    {
        i4RetStatus = QoSUtilTestFsQoSShapeTemplateEIR (&u4ErrorCode, u4Idx,
                                                        (UINT4) i4EIR,
                                                        (UINT4) i4CIR);
        if (i4RetStatus == SNMP_FAILURE)
        {
            QoSCliRSShapeTempEntry (u4Idx, u4CurCIR, u4CurCBS, u4CurEIR,
                                    u4CurEBS, i4RowStatus);
            return (CLI_FAILURE);
        }

        i4RetStatus = nmhGetFsQoSShapeTemplateEIR (u4Idx, &u4CurEIR);
        if (i4RetStatus == SNMP_FAILURE)
        {
            return (CLI_FAILURE);
        }

        i4RetStatus = nmhSetFsQoSShapeTemplateEIR (u4Idx, (UINT4) i4EIR);
        if (i4RetStatus == SNMP_FAILURE)
        {
            QoSCliRSShapeTempEntry (u4Idx, u4CurCIR, u4CurCBS, u4CurEIR,
                                    u4CurEBS, i4RowStatus);
            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }
    }

    if (i4EBS != -1)
    {
        i4RetStatus = nmhTestv2FsQoSShapeTemplateEBS (&u4ErrorCode, u4Idx,
                                                      (UINT4) i4EBS);
        if (i4RetStatus == SNMP_FAILURE)
        {
            QoSCliRSShapeTempEntry (u4Idx, u4CurCIR, u4CurCBS, u4CurEIR,
                                    u4CurEBS, i4RowStatus);
            return (CLI_FAILURE);
        }

        i4RetStatus = nmhGetFsQoSShapeTemplateEBS (u4Idx, &u4CurEBS);
        if (i4RetStatus == SNMP_FAILURE)
        {
            return (CLI_FAILURE);
        }

        i4RetStatus = nmhSetFsQoSShapeTemplateEBS (u4Idx, (UINT4) i4EBS);
        if (i4RetStatus == SNMP_FAILURE)
        {
            QoSCliRSShapeTempEntry (u4Idx, u4CurCIR, u4CurCBS, u4CurEIR,
                                    u4CurEBS, i4RowStatus);
            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }
    }

    i4RetStatus = nmhTestv2FsQoSShapeTemplateStatus (&u4ErrorCode, u4Idx,
                                                     ACTIVE);
    if (i4RetStatus == SNMP_FAILURE)
    {
        QoSCliRSShapeTempEntry (u4Idx, u4CurCIR, u4CurCBS, u4CurEIR,
                                u4CurEBS, i4RowStatus);
        return (CLI_FAILURE);
    }

    i4RetStatus = nmhSetFsQoSShapeTemplateStatus (u4Idx, ACTIVE);
    if (i4RetStatus == SNMP_FAILURE)
    {
        QoSCliRSShapeTempEntry (u4Idx, u4CurCIR, u4CurCBS, u4CurEIR,
                                u4CurEBS, i4RowStatus);
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }

    return (CLI_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSCliDelShapeTempEntry                              */
/* Description        : This function is used to delete Shape Tmplate table  */
/*                      Entry .                                              */
/* Input(s)           : CliHandle    - Handle to CLI session                 */
/*                    : u4Idx        - Shape Tbl Entry index                 */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : CLI_SUCCESS/CLI_FAILURE                              */
/* Called By          : cli_process_qos_cmd                                  */
/* Calling Function   : SNMP nmh Routines                                    */
/*****************************************************************************/
INT4
QoSCliDelShapeTempEntry (tCliHandle CliHandle, UINT4 u4Idx)
{
    UINT4               u4ErrorCode = 0;
    INT4                i4RetStatus = SNMP_FAILURE;

    i4RetStatus = nmhTestv2FsQoSShapeTemplateStatus (&u4ErrorCode,
                                                     u4Idx, DESTROY);
    if (i4RetStatus == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    i4RetStatus = nmhSetFsQoSShapeTemplateStatus (u4Idx, DESTROY);
    if (i4RetStatus == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }

    return (CLI_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSCliUtlDisplayShapeTempEntry                       */
/* Description        : This function is used to Display the Shape Tmplate   */
/*                      Table Entries Parameters                             */
/* Input(s)           : CliHandle    - Handle to CLI session                 */
/*                    : u4Idx        - Table Entries Index                   */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : CLI_SUCCESS/CLI_FAILURE                              */
/* Called By          : QoSCliShowShapeTempEntry                             */
/* Calling Function   : SNMP nmh Routines                                    */
/*****************************************************************************/
INT4
QoSCliUtlDisplayShapeTempEntry (tCliHandle CliHandle, UINT4 u4Idx)
{
    INT4                i4RetStatus = SNMP_FAILURE;
    UINT4               u4CIR = 0;
    UINT4               u4CBS = 0;
    UINT4               u4EIR = 0;
    UINT4               u4EBS = 0;

    /* Get All Fields */
    i4RetStatus = nmhGetFsQoSShapeTemplateCIR (u4Idx, &u4CIR);
    if (i4RetStatus == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    i4RetStatus = nmhGetFsQoSShapeTemplateCBS (u4Idx, &u4CBS);
    if (i4RetStatus == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    i4RetStatus = nmhGetFsQoSShapeTemplateEIR (u4Idx, &u4EIR);
    if (i4RetStatus == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    i4RetStatus = nmhGetFsQoSShapeTemplateEBS (u4Idx, &u4EBS);
    if (i4RetStatus == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    /* Print */
    CliPrintf (CliHandle, "%-16d %-14d %-14d %-14d %-14d\r\n",
               u4Idx, u4CIR, u4CBS, u4EIR, u4EBS);

    return (CLI_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSCliShowShapeTempEntry                             */
/* Description        : This function is used to Display Shape Tmplate table */
/*                      Entry .                                              */
/* Input(s)           : CliHandle    - Handle to CLI session                 */
/*                    : u4Idx        - Shape Tbl Entry index                 */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : CLI_SUCCESS/CLI_FAILURE                              */
/* Called By          : cli_process_qos_cmd                                  */
/* Calling Function   : SNMP nmh Routines,QoSCliUtlDisplayShapeTempEntry     */
/*****************************************************************************/
INT4
QoSCliShowShapeTempEntry (tCliHandle CliHandle, UINT4 u4Idx)
{
    INT4                i4RetStatus = SNMP_FAILURE;
    INT4                i4RowStatus = SNMP_FAILURE;

    /* Display All */
    CliPrintf (CliHandle, "QoS Shape Template Entries\r\n");
    CliPrintf (CliHandle, "--------------------------\r\n");

    CliPrintf (CliHandle, "%-16s %-14s %-14s %-14s %-14s\r\n",
               "ShapeTemplate Id", "CIR", "CBS", "EIR", "EBS");
    CliPrintf (CliHandle, "%-16s %-14s %-14s %-14s %-14s\r\n",
               "----------------", "--------------", "--------------",
               "--------------", "--------------");

    if (u4Idx == 0)
    {
        i4RetStatus = nmhGetFirstIndexFsQoSShapeTemplateTable (&u4Idx);
        if (i4RetStatus == SNMP_FAILURE)
        {
            return (CLI_FAILURE);
        }

        do
        {
            /* Check the Status */
            i4RetStatus = nmhGetFsQoSShapeTemplateStatus (u4Idx, &i4RowStatus);
            if (i4RetStatus == SNMP_FAILURE)
            {
                return (CLI_FAILURE);
            }

            if (i4RowStatus == ACTIVE)
            {
                /* Display Entry */
                i4RetStatus = QoSCliUtlDisplayShapeTempEntry (CliHandle, u4Idx);
                if (i4RetStatus == CLI_FAILURE)
                {
                    return (CLI_FAILURE);
                }
            }

            i4RetStatus =
                nmhGetNextIndexFsQoSShapeTemplateTable (u4Idx, &u4Idx);
        }
        while (i4RetStatus == SNMP_SUCCESS);

        return (CLI_SUCCESS);
    }
    else
    {
        i4RetStatus = nmhGetFsQoSShapeTemplateStatus (u4Idx, &i4RowStatus);
        if (i4RetStatus == SNMP_FAILURE)
        {
            return (CLI_FAILURE);
        }

        if (i4RowStatus == ACTIVE)
        {
            /* Display Entry */
            i4RetStatus = QoSCliUtlDisplayShapeTempEntry (CliHandle, u4Idx);
            if (i4RetStatus == CLI_FAILURE)
            {
                return (CLI_FAILURE);
            }
        }
    }

    return (CLI_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSCliRSSchedEntry                                   */
/* Description        : This function is used to Reset the Scheduler table   */
/*                      Entry .                                              */
/* Input(s)           : i4IfIdx      - Interface Idx                         */
/*                    : u4SchedIdx   - Scheduler Idx                         */
/*                    : i4CurSA      - Scheduler Algo Value                  */
/*                    : u4CurShapeId - Shape id for this Scheduler           */
/*                    : u4CurHL      - Hierarchy Level of Scheduler          */
/*                    : i4RowStatus  - Rowstatus                             */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : CLI_SUCCESS/CLI_FAILURE                              */
/* Called By          : cli_process_qos_cmd                                  */
/* Calling Function   : SNMP nmh Routines                                    */
/*****************************************************************************/
VOID
QoSCliRSSchedEntry (INT4 i4IfIdx, UINT4 u4SchedIdx, INT4 i4CurSA,
                    UINT4 u4CurShapeId, UINT4 u4CurHL, INT4 i4RowStatus)
{
    INT4                i4RetStatus = SNMP_FAILURE;

    if (i4RowStatus == 0)
    {
        i4RetStatus = nmhSetFsQoSSchedulerStatus (i4IfIdx, u4SchedIdx, DESTROY);
        return;
    }

    if (i4CurSA != CLI_RS_INVALID_I_MASK)
    {
        i4RetStatus = nmhSetFsQoSSchedulerSchedAlgorithm (i4IfIdx, u4SchedIdx,
                                                          i4CurSA);
    }

    if (u4CurShapeId != (UINT4) CLI_RS_INVALID_MASK)
    {
        i4RetStatus = nmhSetFsQoSSchedulerShapeId (i4IfIdx, u4SchedIdx,
                                                   u4CurShapeId);
    }

    if (u4CurHL != (UINT4) CLI_RS_INVALID_MASK)
    {
        i4RetStatus = nmhSetFsQoSSchedulerHierarchyLevel (i4IfIdx, u4SchedIdx,
                                                          u4CurHL);
    }

    i4RetStatus = nmhSetFsQoSSchedulerStatus (i4IfIdx, u4SchedIdx, i4RowStatus);

    UNUSED_PARAM (i4RetStatus);

}

/*****************************************************************************/
/* Function Name      : QoSCliAddSchedEntry                                  */
/* Description        : This function is used to create Scheduler table      */
/*                      Entry .                                              */
/* Input(s)           : CliHandle    - Handle to CLI session                 */
/*                    : i4IfIdx      - Interface Idx                         */
/*                    : u4SchedIdx   - Scheduler Idx                         */
/*                    : i4SchedAlgo  - Scheduler Algo Value                  */
/*                    : i4ShapeId    - Shape id for this Scheduler           */
/*                    : i4HL         - Hierarchy Level of Scheduler          */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : CLI_SUCCESS/CLI_FAILURE                              */
/* Called By          : cli_process_qos_cmd                                  */
/* Calling Function   : SNMP nmh Routines                                    */
/*****************************************************************************/
INT4
QoSCliAddSchedEntry (tCliHandle CliHandle, UINT4 u4SchedIdx, INT4 i4IfIdx,
                     INT4 i4SchedAlgo, INT4 i4ShapeId, INT4 i4HL)
{
    UINT4               u4ErrorCode = 0;
    INT4                i4RetStatus = SNMP_FAILURE;
    INT4                i4RowStatus = 0;
    INT4                i4CurSAlgo = CLI_RS_INVALID_I_MASK;
    UINT4               u4CurShapeIdx = (UINT4) CLI_RS_INVALID_MASK;
    UINT4               u4CurHL = (UINT4) CLI_RS_INVALID_MASK;

    /* 1. Check the Table Entry 
     * 2. If the Entry exists Set it as NOT_IN_SERVICE
     * 3. Configure the Given Values.
     * 4. If not exists create and Configure the Values.
     * 5. Any Failure in the New Entry Configurations Delete the Entry.
     * 6. Any Failure in the Modification of an Entry will Restore the 
     *    old Values.
     */
    i4RetStatus =
        nmhGetFsQoSSchedulerStatus (i4IfIdx, u4SchedIdx, &i4RowStatus);
    if (i4RetStatus == QOS_FAILURE)
    {
        i4RetStatus =
            nmhTestv2FsQoSSchedulerStatus (&u4ErrorCode, i4IfIdx, u4SchedIdx,
                                           CREATE_AND_WAIT);
        if (i4RetStatus == SNMP_FAILURE)
        {
            return (CLI_FAILURE);
        }

        i4RetStatus = nmhSetFsQoSSchedulerStatus (i4IfIdx, u4SchedIdx,
                                                  CREATE_AND_WAIT);
        if (i4RetStatus == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }
    }
    if (i4SchedAlgo != -1)
    {
        i4RetStatus =
            nmhTestv2FsQoSSchedulerSchedAlgorithm (&u4ErrorCode, i4IfIdx,
                                                   u4SchedIdx, i4SchedAlgo);
        if (i4RetStatus == SNMP_FAILURE)
        {
            QoSCliRSSchedEntry (i4IfIdx, u4SchedIdx, i4CurSAlgo, u4CurShapeIdx,
                                u4CurHL, i4RowStatus);
            return (CLI_FAILURE);
        }

        i4RetStatus = nmhGetFsQoSSchedulerSchedAlgorithm (i4IfIdx, u4SchedIdx,
                                                          &i4CurSAlgo);
        if (i4RetStatus == SNMP_FAILURE)
        {
            return (CLI_FAILURE);
        }

        i4RetStatus = nmhSetFsQoSSchedulerSchedAlgorithm (i4IfIdx, u4SchedIdx,
                                                          i4SchedAlgo);
        if (i4RetStatus == SNMP_FAILURE)
        {
            QoSCliRSSchedEntry (i4IfIdx, u4SchedIdx, i4CurSAlgo, u4CurShapeIdx,
                                u4CurHL, i4RowStatus);
            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }
    }

    if (i4ShapeId != -1)
    {
        i4RetStatus = nmhTestv2FsQoSSchedulerShapeId (&u4ErrorCode, i4IfIdx,
                                                      u4SchedIdx,
                                                      (UINT4) i4ShapeId);
        if (i4RetStatus == SNMP_FAILURE)
        {
            QoSCliRSSchedEntry (i4IfIdx, u4SchedIdx, i4CurSAlgo, u4CurShapeIdx,
                                u4CurHL, i4RowStatus);
            return (CLI_FAILURE);
        }

        i4RetStatus = nmhGetFsQoSSchedulerShapeId (i4IfIdx, u4SchedIdx,
                                                   &u4CurShapeIdx);
        if (i4RetStatus == SNMP_FAILURE)
        {
            return (CLI_FAILURE);
        }

        i4RetStatus = nmhSetFsQoSSchedulerShapeId (i4IfIdx, u4SchedIdx,
                                                   (UINT4) i4ShapeId);
        if (i4RetStatus == SNMP_FAILURE)
        {
            QoSCliRSSchedEntry (i4IfIdx, u4SchedIdx, i4CurSAlgo, u4CurShapeIdx,
                                u4CurHL, i4RowStatus);
            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }
    }

    if (i4HL != -1)
    {
        i4RetStatus =
            nmhTestv2FsQoSSchedulerHierarchyLevel (&u4ErrorCode, i4IfIdx,
                                                   u4SchedIdx, (UINT4) i4HL);
        if (i4RetStatus == SNMP_FAILURE)
        {
            QoSCliRSSchedEntry (i4IfIdx, u4SchedIdx, i4CurSAlgo, u4CurShapeIdx,
                                u4CurHL, i4RowStatus);
            return (CLI_FAILURE);
        }

        i4RetStatus = nmhGetFsQoSSchedulerHierarchyLevel (i4IfIdx, u4SchedIdx,
                                                          &u4CurHL);
        if (i4RetStatus == SNMP_FAILURE)
        {
            return (CLI_FAILURE);
        }

        i4RetStatus = nmhSetFsQoSSchedulerHierarchyLevel (i4IfIdx, u4SchedIdx,
                                                          (UINT4) i4HL);
        if (i4RetStatus == SNMP_FAILURE)
        {
            QoSCliRSSchedEntry (i4IfIdx, u4SchedIdx, i4CurSAlgo, u4CurShapeIdx,
                                u4CurHL, i4RowStatus);
            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }
    }

    i4RetStatus = nmhTestv2FsQoSSchedulerStatus (&u4ErrorCode, i4IfIdx,
                                                 u4SchedIdx, ACTIVE);
    if (i4RetStatus == SNMP_FAILURE)
    {
        QoSCliRSSchedEntry (i4IfIdx, u4SchedIdx, i4CurSAlgo, u4CurShapeIdx,
                            u4CurHL, i4RowStatus);
        return (CLI_FAILURE);
    }

    i4RetStatus = nmhSetFsQoSSchedulerStatus (i4IfIdx, u4SchedIdx, ACTIVE);
    if (i4RetStatus == SNMP_FAILURE)
    {
        QoSCliRSSchedEntry (i4IfIdx, u4SchedIdx, i4CurSAlgo, u4CurShapeIdx,
                            u4CurHL, i4RowStatus);
        return (CLI_FAILURE);
    }

    return (CLI_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSCliDelSchedEntry                                  */
/* Description        : This function is used to delete Scheduler table      */
/*                      Entry .                                              */
/* Input(s)           : CliHandle    - Handle to CLI session                 */
/*                    : u4SchedIdx   - Scheduler Idx                         */
/*                    : i4IfIdx      - Interface Idx                         */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : CLI_SUCCESS/CLI_FAILURE                              */
/* Called By          : cli_process_qos_cmd                                  */
/* Calling Function   : SNMP nmh Routines                                    */
/*****************************************************************************/
INT4
QoSCliDelSchedEntry (tCliHandle CliHandle, UINT4 u4SchedIdx, INT4 i4IfIdx)
{
    UINT4               u4ErrorCode = 0;
    INT4                i4RetStatus = SNMP_FAILURE;

    i4RetStatus = nmhTestv2FsQoSSchedulerStatus (&u4ErrorCode, i4IfIdx,
                                                 u4SchedIdx, DESTROY);
    if (i4RetStatus == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    i4RetStatus = nmhSetFsQoSSchedulerStatus (i4IfIdx, u4SchedIdx, DESTROY);
    if (i4RetStatus == SNMP_FAILURE)
    {
        ISSCliCheckAndThrowFatalError (CliHandle);
        return (CLI_FAILURE);
    }

    return (CLI_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSCliUtlDisplaySchedEntry                           */
/* Description        : This function is used to Display the Scheduler       */
/*                      Table Entries Parameters                             */
/* Input(s)           : CliHandle    - Handle to CLI session                 */
/*                    : i4IfIdx      - Interface Idx                         */
/*                    : u4SchedIdx   - Scheduler Idx                         */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : CLI_SUCCESS/CLI_FAILURE                              */
/* Called By          : QoSCliShowSchedEntry                                 */
/* Calling Function   : SNMP nmh Routines                                    */
/*****************************************************************************/
INT4
QoSCliUtlDisplaySchedEntry (tCliHandle CliHandle, INT4 i4IfIdx,
                            UINT4 u4SchedIdx)
{
    INT4                i4RetStatus = SNMP_FAILURE;
    INT4                i4SAlgo = 0;
    UINT4               u4ShapeId = 0;
    UINT4               u4HL = 0;
    UINT1               au1IfName[CFA_MAX_PORT_NAME_LENGTH];
    UINT4               u4GlobalId;
    INT4                i4ReturnValue = CLI_SUCCESS;
    /* Get All Fields */
    i4RetStatus = nmhGetFsQoSSchedulerSchedAlgorithm (i4IfIdx, u4SchedIdx,
                                                      &i4SAlgo);
    if (i4RetStatus == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    i4RetStatus = nmhGetFsQoSSchedulerShapeId (i4IfIdx, u4SchedIdx, &u4ShapeId);
    if (i4RetStatus == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    i4RetStatus = nmhGetFsQoSSchedulerHierarchyLevel (i4IfIdx, u4SchedIdx,
                                                      &u4HL);
    if (i4RetStatus == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    i4RetStatus = nmhGetFsQoSSchedulerGlobalId (i4IfIdx, u4SchedIdx,
                                                &u4GlobalId);
    if (i4RetStatus == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    MEMSET (au1IfName, 0, CFA_MAX_PORT_NAME_LENGTH);
    if (i4IfIdx == 0)
    {
        STRNCPY (au1IfName, "All Ports", STRLEN ("All Ports"));
        au1IfName[STRLEN ("All Ports")] = '\0';
    }
    else if (CfaCliGetIfName (i4IfIdx, (INT1 *) au1IfName) == CLI_FAILURE)
    {
        return (CLI_FAILURE);
    }

    /* Print */
    switch (i4SAlgo)
    {
        case QOS_CLI_SCHED_ALGO_SP:
            CliPrintf (CliHandle, "%-10s %-15d %-20s %-11d %-12d %-12d\r\n",
                       au1IfName, u4SchedIdx, "strictPriority", u4ShapeId,
                       u4HL, u4GlobalId);
            break;

        case QOS_CLI_SCHED_ALGO_RR:
            CliPrintf (CliHandle, "%-10s %-15d %-20s %-11d %-12d %-12d\r\n",
                       au1IfName, u4SchedIdx, "roundRobin", u4ShapeId, u4HL,
                       u4GlobalId);
            break;

        case QOS_CLI_SCHED_ALGO_WRR:
            if (ISS_HW_SUPPORTED == IssGetHwCapabilities (ISS_HW_DWRR_SUPPORT))
            {
                CliPrintf (CliHandle, "%-10s %-15d %-20s %-11d %-12d %-12d\r\n",
                           au1IfName, u4SchedIdx, "differedweightedRoundRobin",
                           u4ShapeId, u4HL, u4GlobalId);
            }
            else
            {
                CliPrintf (CliHandle, "%-10s %-15d %-20s %-11d %-12d %-12d\r\n",
                           au1IfName, u4SchedIdx, "weightedRoundRobin",
                           u4ShapeId, u4HL, u4GlobalId);
            }
            break;

        case QOS_CLI_SCHED_ALGO_WFQ:
            CliPrintf (CliHandle, "%-10s %-15d %-20s %-11d %-12d %-12d\r\n",
                       au1IfName, u4SchedIdx, "weightedFairQueing", u4ShapeId,
                       u4HL, u4GlobalId);
            break;

        case QOS_CLI_SCHED_ALGO_SRR:
            CliPrintf (CliHandle, "%-10s %-15d %-20s %-11d %-12d %-12d\r\n",
                       au1IfName, u4SchedIdx, "strictRoundRobin", u4ShapeId,
                       u4HL, u4GlobalId);
            break;

        case QOS_CLI_SCHED_ALGO_SWRR:
            CliPrintf (CliHandle, "%-10s %-15d %-20s %-11d %-12d %-12d\r\n",
                       au1IfName, u4SchedIdx, "strictWeightedRoundRobin",
                       u4ShapeId, u4HL, u4GlobalId);
            break;

        case QOS_CLI_SCHED_ALGO_SWFQ:
            CliPrintf (CliHandle, "%-10s %-15d %-20s %-11d %-12d %-12d\r\n",
                       au1IfName, u4SchedIdx, "strictWeightedFairQueing",
                       u4ShapeId, u4HL, u4GlobalId);
            break;

        case QOS_CLI_SCHED_ALGO_DRR:
            CliPrintf (CliHandle, "%-10s %-15d %-20s %-11d %-12d %-12d\r\n",
                       au1IfName, u4SchedIdx, "deficitRoundRobin", u4ShapeId,
                       u4HL, u4GlobalId);
            break;

        default:
            CliPrintf (CliHandle, "%-10s %-15d %-20s %-11d %-12d %-12d\r\n",
                       au1IfName, u4SchedIdx, "Invalid", u4ShapeId, u4HL,
                       u4GlobalId);
            i4ReturnValue = CLI_FAILURE;
            break;
    }

    return i4ReturnValue;
}

/*****************************************************************************/
/* Function Name      : QoSCliShowSchedEntry                                 */
/* Description        : This function is used to Display Scheduler table     */
/*                      Entry .                                              */
/* Input(s)           : CliHandle    - Handle to CLI session                 */
/*                    : i4IfIdx      - Interface Idx                         */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : CLI_SUCCESS/CLI_FAILURE                              */
/* Called By          : cli_process_qos_cmd                                  */
/* Calling Function   : SNMP nmh Routines,QoSCliUtlDisplaySchedEntry         */
/*****************************************************************************/
INT4
QoSCliShowSchedEntry (tCliHandle CliHandle, INT4 i4IfIdx)
{
    UINT4               u4SchedIdx = 0;
    INT4                i4TmpIfIdx = 0;
    INT4                i4RetStatus = SNMP_FAILURE;
    INT4                i4RowStatus = SNMP_FAILURE;

    /* Display All */
    CliPrintf (CliHandle, "QoS Scheduler Entries\r\n");
    CliPrintf (CliHandle, "---------------------\r\n");

    CliPrintf (CliHandle, "%-10s %-15s %-20s %-11s %-12s\r\n",
               "IfIndex", "Scheduler Index", "Scheduler Algo", "Shape Index",
               "Scheduler HL  GlobalId");
    CliPrintf (CliHandle, "%-10s %-15s %-20s %-11s %-12s %-12s\r\n",
               "----------", "---------------", "--------------------",
               "-----------", "------------", "------------");

    i4TmpIfIdx = i4IfIdx;

    i4RetStatus = nmhGetFirstIndexFsQoSSchedulerTable (&i4IfIdx, &u4SchedIdx);
    if (i4RetStatus == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    do
    {
        /* Check the Status */
        i4RetStatus = nmhGetFsQoSSchedulerStatus (i4IfIdx, u4SchedIdx,
                                                  &i4RowStatus);
        if (i4RetStatus == SNMP_FAILURE)
        {
            return (CLI_FAILURE);
        }

        if (i4RowStatus == ACTIVE)
        {
            if ((i4TmpIfIdx == 0) || (i4IfIdx == i4TmpIfIdx))
            {
                /* Display Entry */
                i4RetStatus = QoSCliUtlDisplaySchedEntry (CliHandle, i4IfIdx,
                                                          u4SchedIdx);
                if (i4RetStatus == CLI_FAILURE)
                {
                    return (CLI_FAILURE);
                }
            }
        }

        i4RetStatus =
            nmhGetNextIndexFsQoSSchedulerTable (i4IfIdx, &i4IfIdx, u4SchedIdx,
                                                &u4SchedIdx);
    }
    while (i4RetStatus == SNMP_SUCCESS);

    CliPrintf (CliHandle, "\r\n");
    return (CLI_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSCliRSQEntry                                       */
/* Description        : This function is used to Reset Queue table Entry     */
/* Input(s)           : pQParam      - Ptr to tQoSCliQParam                  */
/*                    : i4RowStatus  - RowStatus                             */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : CLI_SUCCESS/CLI_FAILURE                              */
/* Called By          : cli_process_qos_cmd                                  */
/* Calling Function   : SNMP nmh Routines                                    */
/*****************************************************************************/
VOID
QoSCliRSQEntry (tQoSCliQParam * pQCurParam, INT4 i4RowStatus)
{
    INT4                i4RetStatus = SNMP_FAILURE;
    UINT4               u4QIdx = 0;
    INT4                i4IfIdx = 0;

    u4QIdx = pQCurParam->u4QIdx;
    i4IfIdx = pQCurParam->u4IfIdx;

    if (i4RowStatus == 0)
    {
        i4RetStatus = nmhSetFsQoSQStatus (i4IfIdx, u4QIdx, DESTROY);
        return;
    }
    if (pQCurParam->u4QType != (UINT4) CLI_RS_INVALID_MASK)
    {
        i4RetStatus = nmhSetFsQoSQCfgTemplateId (i4IfIdx, u4QIdx,
                                                 pQCurParam->u4QType);
    }
    if (pQCurParam->u4SchedIdx != (UINT4) CLI_RS_INVALID_MASK)
    {
        i4RetStatus = nmhSetFsQoSQSchedulerId (i4IfIdx, u4QIdx,
                                               pQCurParam->u4SchedIdx);
    }
    if (pQCurParam->u4QWeight != (UINT4) CLI_RS_INVALID_MASK)
    {
        i4RetStatus = nmhSetFsQoSQWeight (i4IfIdx, u4QIdx,
                                          pQCurParam->u4QWeight);
    }
    if (pQCurParam->u4QPri != (UINT4) CLI_RS_INVALID_MASK)
    {
        i4RetStatus = nmhSetFsQoSQPriority (i4IfIdx, u4QIdx,
                                            pQCurParam->u4QPri);
    }
    if (pQCurParam->u4ShapeIdx != (UINT4) CLI_RS_INVALID_MASK)
    {
        i4RetStatus = nmhSetFsQoSQShapeId (i4IfIdx, u4QIdx,
                                           pQCurParam->u4ShapeIdx);
    }
    if (pQCurParam->u4QueueType != (UINT4) CLI_RS_INVALID_MASK)
    {
        i4RetStatus = nmhSetFsQoSQType (i4IfIdx, u4QIdx,
                                        pQCurParam->u4QueueType);
    }

    i4RetStatus = nmhSetFsQoSQStatus (i4IfIdx, u4QIdx, i4RowStatus);

    UNUSED_PARAM (i4RetStatus);

}

/*****************************************************************************/
/* Function Name      : QoSCliAddQEntry                                      */
/* Description        : This function is used to create Queue table Entry    */
/* Input(s)           : CliHandle    - Handle to CLI session                 */
/*                    : pQParam      - Ptr to tQoSCliQParam                  */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : CLI_SUCCESS/CLI_FAILURE                              */
/* Called By          : cli_process_qos_cmd                                  */
/* Calling Function   : SNMP nmh Routines                                    */
/*****************************************************************************/
INT4
QoSCliAddQEntry (tCliHandle CliHandle, tQoSCliQParam * pQParam)
{
    tQoSCliQParam       QCurParam;
    UINT4               u4ErrorCode = 0;
    INT4                i4RetStatus = SNMP_FAILURE;
    INT4                i4RowStatus = 0;
    INT4                i4IfIdx = 0;
    UINT4               u4QIdx = 0;

    MEMSET (&QCurParam, 0xFF, (sizeof (tQoSCliQParam)));

    /* 1. Check the Table Entry 
     * 2. If the Entry exists Set it as NOT_IN_SERVICE
     * 3. Configure the Given Values.
     * 4. If not exists create and Configure the Values.
     * 5. Any Failure in the New Entry Configurations Delete the Entry.
     * 6. Any Failure in the Modification of an Entry will Restore the 
     *    old Values.
     */

    i4IfIdx = (INT4) pQParam->u4IfIdx;
    u4QIdx = pQParam->u4QIdx;

    QCurParam.u4IfIdx = pQParam->u4IfIdx;
    QCurParam.u4QIdx = pQParam->u4QIdx;

    i4RetStatus = nmhGetFsQoSQStatus (i4IfIdx, u4QIdx, &i4RowStatus);
    if (i4RetStatus == QOS_FAILURE)
    {
        i4RetStatus = nmhTestv2FsQoSQStatus (&u4ErrorCode, i4IfIdx, u4QIdx,
                                             CREATE_AND_WAIT);
        if (i4RetStatus == SNMP_FAILURE)
        {
            return (CLI_FAILURE);
        }

        i4RetStatus = nmhSetFsQoSQStatus (i4IfIdx, u4QIdx, CREATE_AND_WAIT);
        if (i4RetStatus == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }
    }
    else if (i4RowStatus == ACTIVE)
    {
        i4RetStatus = nmhTestv2FsQoSQStatus (&u4ErrorCode, i4IfIdx, u4QIdx,
                                             NOT_IN_SERVICE);
        if (i4RetStatus == SNMP_FAILURE)
        {
            return (CLI_FAILURE);
        }

        i4RetStatus = nmhSetFsQoSQStatus (i4IfIdx, u4QIdx, NOT_IN_SERVICE);
        if (i4RetStatus == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }
    }

    if (pQParam->u4QType != (UINT4) CLI_RS_INVALID_MASK)
    {
        i4RetStatus =
            nmhTestv2FsQoSQCfgTemplateId (&u4ErrorCode, i4IfIdx, u4QIdx,
                                          pQParam->u4QType);
        if (i4RetStatus == SNMP_FAILURE)
        {
            QoSCliRSQEntry (&QCurParam, i4RowStatus);
            return (CLI_FAILURE);
        }

        i4RetStatus = nmhGetFsQoSQCfgTemplateId (i4IfIdx, u4QIdx,
                                                 &(QCurParam.u4QType));
        if (i4RetStatus == SNMP_FAILURE)
        {
            return (CLI_FAILURE);
        }

        i4RetStatus = nmhSetFsQoSQCfgTemplateId (i4IfIdx, u4QIdx,
                                                 pQParam->u4QType);
        if (i4RetStatus == SNMP_FAILURE)
        {
            QoSCliRSQEntry (&QCurParam, i4RowStatus);
            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }
    }

    if (pQParam->u4SchedIdx != (UINT4) CLI_RS_INVALID_MASK)
    {
        i4RetStatus = nmhTestv2FsQoSQSchedulerId (&u4ErrorCode, i4IfIdx,
                                                  u4QIdx, pQParam->u4SchedIdx);
        if (i4RetStatus == SNMP_FAILURE)
        {
            QoSCliRSQEntry (&QCurParam, i4RowStatus);
            return (CLI_FAILURE);
        }

        i4RetStatus = nmhGetFsQoSQSchedulerId (i4IfIdx, u4QIdx,
                                               &(QCurParam.u4SchedIdx));
        if (i4RetStatus == SNMP_FAILURE)
        {
            return (CLI_FAILURE);
        }

        i4RetStatus = nmhSetFsQoSQSchedulerId (i4IfIdx, u4QIdx,
                                               pQParam->u4SchedIdx);
        if (i4RetStatus == SNMP_FAILURE)
        {
            QoSCliRSQEntry (&QCurParam, i4RowStatus);
            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }
    }

    if (pQParam->u4QWeight != (UINT4) CLI_RS_INVALID_MASK)
    {
        i4RetStatus = nmhTestv2FsQoSQWeight (&u4ErrorCode, i4IfIdx,
                                             u4QIdx, pQParam->u4QWeight);
        if (i4RetStatus == SNMP_FAILURE)
        {
            QoSCliRSQEntry (&QCurParam, i4RowStatus);
            return (CLI_FAILURE);
        }

        i4RetStatus = nmhGetFsQoSQWeight (i4IfIdx, u4QIdx,
                                          &(QCurParam.u4QWeight));
        if (i4RetStatus == SNMP_FAILURE)
        {
            return (CLI_FAILURE);
        }

        i4RetStatus = nmhSetFsQoSQWeight (i4IfIdx, u4QIdx, pQParam->u4QWeight);
        if (i4RetStatus == SNMP_FAILURE)
        {
            QoSCliRSQEntry (&QCurParam, i4RowStatus);
            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }
    }

    if (pQParam->u4QPri != (UINT4) CLI_RS_INVALID_MASK)
    {
        i4RetStatus = nmhTestv2FsQoSQPriority (&u4ErrorCode, i4IfIdx,
                                               u4QIdx, pQParam->u4QPri);
        if (i4RetStatus == SNMP_FAILURE)
        {
            QoSCliRSQEntry (&QCurParam, i4RowStatus);
            return (CLI_FAILURE);
        }

        i4RetStatus = nmhGetFsQoSQPriority (i4IfIdx, u4QIdx,
                                            &(QCurParam.u4QPri));
        if (i4RetStatus == SNMP_FAILURE)
        {
            return (CLI_FAILURE);
        }

        i4RetStatus = nmhSetFsQoSQPriority (i4IfIdx, u4QIdx, pQParam->u4QPri);
        if (i4RetStatus == SNMP_FAILURE)
        {
            QoSCliRSQEntry (&QCurParam, i4RowStatus);
            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }
    }

    if (pQParam->u4ShapeIdx != (UINT4) CLI_RS_INVALID_MASK)
    {
        i4RetStatus = nmhTestv2FsQoSQShapeId (&u4ErrorCode, i4IfIdx,
                                              u4QIdx, pQParam->u4ShapeIdx);
        if (i4RetStatus == SNMP_FAILURE)
        {
            QoSCliRSQEntry (&QCurParam, i4RowStatus);
            return (CLI_FAILURE);
        }

        i4RetStatus = nmhGetFsQoSQShapeId (i4IfIdx, u4QIdx,
                                           &(QCurParam.u4ShapeIdx));
        if (i4RetStatus == SNMP_FAILURE)
        {
            return (CLI_FAILURE);
        }

        i4RetStatus = nmhSetFsQoSQShapeId (i4IfIdx, u4QIdx,
                                           pQParam->u4ShapeIdx);
        if (i4RetStatus == SNMP_FAILURE)
        {
            QoSCliRSQEntry (&QCurParam, i4RowStatus);
            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }
    }
    if (pQParam->u4QueueType != (UINT4) CLI_RS_INVALID_MASK)
    {
        i4RetStatus = nmhGetFsQoSQType (i4IfIdx, u4QIdx,
                                        &(QCurParam.u4QueueType));
        if (i4RetStatus == SNMP_FAILURE)
        {
            return (CLI_FAILURE);
        }

        i4RetStatus = nmhTestv2FsQoSQType (&u4ErrorCode, i4IfIdx,
                                           u4QIdx, pQParam->u4QueueType);
        if (i4RetStatus == SNMP_FAILURE)
        {
            QoSCliRSQEntry (&QCurParam, i4RowStatus);
            return (CLI_FAILURE);
        }

        i4RetStatus = nmhSetFsQoSQType (i4IfIdx, u4QIdx, pQParam->u4QueueType);
        if (i4RetStatus == SNMP_FAILURE)
        {
            QoSCliRSQEntry (&QCurParam, i4RowStatus);
            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }
    }
    i4RetStatus = nmhTestv2FsQoSQStatus (&u4ErrorCode, i4IfIdx, u4QIdx, ACTIVE);
    if (i4RetStatus == SNMP_FAILURE)
    {
        QoSCliRSQEntry (&QCurParam, i4RowStatus);
        return (CLI_FAILURE);
    }

    i4RetStatus = nmhSetFsQoSQStatus (i4IfIdx, u4QIdx, ACTIVE);
    if (i4RetStatus == SNMP_FAILURE)
    {
        QoSCliRSQEntry (&QCurParam, i4RowStatus);
        return (CLI_FAILURE);
    }

    return (CLI_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSCliDelQEntry                                      */
/* Description        : This function is used to delete Queue table Entry    */
/* Input(s)           : CliHandle    - Handle to CLI session                 */
/*                    : u4QIdx       - Queue     Idx                         */
/*                    : i4IfIdx      - Interface Idx                         */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : CLI_SUCCESS/CLI_FAILURE                              */
/* Called By          : cli_process_qos_cmd                                  */
/* Calling Function   : SNMP nmh Routines                                    */
/*****************************************************************************/
INT4
QoSCliDelQEntry (tCliHandle CliHandle, UINT4 u4QIdx, INT4 i4IfIdx)
{
    UINT4               u4ErrorCode = 0;
    INT4                i4RetStatus = SNMP_FAILURE;

    i4RetStatus = nmhTestv2FsQoSQStatus (&u4ErrorCode, i4IfIdx, u4QIdx,
                                         DESTROY);
    if (i4RetStatus == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    i4RetStatus = nmhSetFsQoSQStatus (i4IfIdx, u4QIdx, DESTROY);
    if (i4RetStatus == SNMP_FAILURE)
    {
        ISSCliCheckAndThrowFatalError (CliHandle);
        return (CLI_FAILURE);
    }

    return (CLI_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSCliUtlDisplayQEntry                               */
/* Description        : This function is used to Display the Queue           */
/*                      Table Entries Parameters                             */
/* Input(s)           : CliHandle    - Handle to CLI session                 */
/*                    : i4IfIdx      - Interface Idx                         */
/*                    : u4QIdx       - Q Idx                                 */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : CLI_SUCCESS/CLI_FAILURE                              */
/* Called By          : QoSCliShowQEntry                                     */
/* Calling Function   : SNMP nmh Routines                                    */
/*****************************************************************************/
INT4
QoSCliUtlDisplayQEntry (tCliHandle CliHandle, INT4 i4IfIdx, UINT4 u4QIdx)
{
    INT4                i4RetStatus = SNMP_FAILURE;
    INT4                i4SAlgo = 0;
    UINT4               u4QType = 0;
    UINT4               u4SchedId = 0;
    UINT4               u4QWeight = 0;
    UINT4               u4QPri = 0;
    UINT4               u4QueueType = 0;
    UINT4               u4QShapeId = 0;
    UINT1               au1IfName[CFA_MAX_PORT_NAME_LENGTH];
    UINT4               u4GlobalId;
    /* Get All Fields */
    i4RetStatus = nmhGetFsQoSQCfgTemplateId (i4IfIdx, u4QIdx, &u4QType);
    if (i4RetStatus == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    i4RetStatus = nmhGetFsQoSQSchedulerId (i4IfIdx, u4QIdx, &u4SchedId);
    if (i4RetStatus == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    i4RetStatus = nmhGetFsQoSQWeight (i4IfIdx, u4QIdx, &u4QWeight);
    if (i4RetStatus == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    i4RetStatus = nmhGetFsQoSQPriority (i4IfIdx, u4QIdx, &u4QPri);
    if (i4RetStatus == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    i4RetStatus = nmhGetFsQoSQType (i4IfIdx, u4QIdx, &u4QueueType);
    if (i4RetStatus == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    i4RetStatus = nmhGetFsQoSQShapeId (i4IfIdx, u4QIdx, &u4QShapeId);
    if (i4RetStatus == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }
    MEMSET (au1IfName, 0, CFA_MAX_PORT_NAME_LENGTH);
    if (CfaCliGetIfName (i4IfIdx, (INT1 *) au1IfName) == CLI_FAILURE)
    {
        return (CLI_FAILURE);
    }
    u4GlobalId = QOS_GLOBAL_QID_FROM_IF_QID (i4IfIdx, u4QIdx);
    i4RetStatus =
        nmhGetFsQoSSchedulerSchedAlgorithm (i4IfIdx, u4SchedId, &i4SAlgo);
    if (i4RetStatus == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }
    /* Print */
    if (u4QShapeId == 0)
    {
        if (u4QueueType == QOS_Q_UNICAST_TYPE)
        {
            if (i4SAlgo == QOS_CLI_SCHED_ALGO_SP ||
                i4SAlgo == QOS_CLI_SCHED_ALGO_RR
                || i4SAlgo == QOS_CLI_SCHED_ALGO_SRR)
            {
                CliPrintf (CliHandle,
                           "%-8s %-6d %-10d %-10d %-7s %-9d %-6s %-9s %-9d\r\n",
                           au1IfName, u4QIdx, u4QType, u4SchedId, "NA", u4QPri,
                           "UC", "none", u4GlobalId);
            }
            else if (i4SAlgo == QOS_CLI_SCHED_ALGO_WRR
                     || i4SAlgo == QOS_CLI_SCHED_ALGO_RR)
            {
                CliPrintf (CliHandle,
                           "%-8s %-6d %-10d %-10d %-7d %-9s %-6s %-9s %-9d\r\n",
                           au1IfName, u4QIdx, u4QType, u4SchedId, u4QWeight,
                           "NA", "UC", "none", u4GlobalId);
            }
            else
            {
                CliPrintf (CliHandle,
                           "%-8s %-6d %-10d %-10d %-7d %-9d %-6s %-9s %-9d\r\n",
                           au1IfName, u4QIdx, u4QType, u4SchedId, u4QWeight,
                           u4QPri, "UC", "none", u4GlobalId);
            }
        }

        else if (u4QueueType == QOS_Q_SUBSCRIBER_TYPE)
        {
            if (i4SAlgo == QOS_CLI_SCHED_ALGO_SP ||
                i4SAlgo == QOS_CLI_SCHED_ALGO_RR
                || i4SAlgo == QOS_CLI_SCHED_ALGO_SRR)
            {
                CliPrintf (CliHandle,
                           "%-8s %-6d %-10d %-10d %-7s %-9d %-6s %-9s %-9d\r\n",
                           au1IfName, u4QIdx, u4QType, u4SchedId, "NA", u4QPri,
                           "SUB", "none", u4GlobalId);
            }
            else if (i4SAlgo == QOS_CLI_SCHED_ALGO_WRR
                     || i4SAlgo == QOS_CLI_SCHED_ALGO_RR)
            {
                CliPrintf (CliHandle,
                           "%-8s %-6d %-10d %-10d %-7d %-9s %-6s %-9s %-9d\r\n",
                           au1IfName, u4QIdx, u4QType, u4SchedId, u4QWeight,
                           "NA", "SUB", "none", u4GlobalId);
            }
            else
            {
                CliPrintf (CliHandle,
                           "%-8s %-6d %-10d %-10d %-7d %-9d %-6s %-9s %-9d\r\n",
                           au1IfName, u4QIdx, u4QType, u4SchedId, u4QWeight,
                           u4QPri, "SUB", "none", u4GlobalId);
            }
        }

        else
        {
            if (i4SAlgo == QOS_CLI_SCHED_ALGO_SP ||
                i4SAlgo == QOS_CLI_SCHED_ALGO_RR
                || i4SAlgo == QOS_CLI_SCHED_ALGO_SRR)
            {
                CliPrintf (CliHandle,
                           "%-8s %-6d %-10d %-10d %-7s %-9d %-6s %-9s %-9d\r\n",
                           au1IfName, u4QIdx, u4QType, u4SchedId, "NA", u4QPri,
                           "MC", "none", u4GlobalId);
            }
            else if (i4SAlgo == QOS_CLI_SCHED_ALGO_WRR
                     || i4SAlgo == QOS_CLI_SCHED_ALGO_RR)
            {
                CliPrintf (CliHandle,
                           "%-8s %-6d %-10d %-10d %-7d %-9s %-6s %-9s %-9d\r\n",
                           au1IfName, u4QIdx, u4QType, u4SchedId, u4QWeight,
                           "NA", "MC", "none", u4GlobalId);
            }
            else
            {
                CliPrintf (CliHandle,
                           "%-8s %-6d %-10d %-10d %-7d %-9d %-6s %-9s %-9d\r\n",
                           au1IfName, u4QIdx, u4QType, u4SchedId, u4QWeight,
                           u4QPri, "MC", "none", u4GlobalId);
            }
        }
    }
    else
    {
        if (u4QueueType == QOS_Q_UNICAST_TYPE)
        {
            if (i4SAlgo == QOS_CLI_SCHED_ALGO_SP ||
                i4SAlgo == QOS_CLI_SCHED_ALGO_RR
                || i4SAlgo == QOS_CLI_SCHED_ALGO_SRR)
            {
                CliPrintf (CliHandle,
                           "%-8s %-6d %-10d %-10d %-7s %-9d %-6s %-9d %-9d\r\n",
                           au1IfName, u4QIdx, u4QType, u4SchedId, "NA", u4QPri,
                           "UC", u4QShapeId, u4GlobalId);
            }
            else if (i4SAlgo == QOS_CLI_SCHED_ALGO_WRR
                     || i4SAlgo == QOS_CLI_SCHED_ALGO_RR)
            {
                CliPrintf (CliHandle,
                           "%-8s %-6d %-10d %-10d %-7d %-9s %-6s %-9d %-9d\r\n",
                           au1IfName, u4QIdx, u4QType, u4SchedId, u4QWeight,
                           "NA", "UC", u4QShapeId, u4GlobalId);
            }
            else
            {
                CliPrintf (CliHandle,
                           "%-8s %-6d %-10d %-10d %-7d %-9d %-6s %-9d %-9d\r\n",
                           au1IfName, u4QIdx, u4QType, u4SchedId, u4QWeight,
                           u4QPri, "UC", u4QShapeId, u4GlobalId);
            }
        }
        else
        {
            if (i4SAlgo == QOS_CLI_SCHED_ALGO_SP ||
                i4SAlgo == QOS_CLI_SCHED_ALGO_RR
                || i4SAlgo == QOS_CLI_SCHED_ALGO_SRR)
            {
                CliPrintf (CliHandle,
                           "%-8s %-6d %-10d %-10d %-7s %-9d %-6s %-9d %-9d\r\n",
                           au1IfName, u4QIdx, u4QType, u4SchedId, "NA", u4QPri,
                           "MC", u4QShapeId, u4GlobalId);
            }
            else if (i4SAlgo == QOS_CLI_SCHED_ALGO_WRR
                     || i4SAlgo == QOS_CLI_SCHED_ALGO_RR)
            {
                CliPrintf (CliHandle,
                           "%-8s %-6d %-10d %-10d %-7d %-9s %-6s %-9d %-9d\r\n",
                           au1IfName, u4QIdx, u4QType, u4SchedId, u4QWeight,
                           "NA", "MC", u4QShapeId, u4GlobalId);
            }
            else
            {
                CliPrintf (CliHandle,
                           "%-8s %-6d %-10d %-10d %-7d %-9d %-6s %-9d %-9d\r\n",
                           au1IfName, u4QIdx, u4QType, u4SchedId, u4QWeight,
                           u4QPri, "MC", u4QShapeId, u4GlobalId);
            }
        }
    }

    return (CLI_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSCliShowQEntry                                     */
/* Description        : This function is used to Display Q table Entry       */
/* Input(s)           : CliHandle    - Handle to CLI session                 */
/*                    : i4IfIdx      - Interface Idx                         */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : CLI_SUCCESS/CLI_FAILURE                              */
/* Called By          : cli_process_qos_cmd                                  */
/* Calling Function   : SNMP nmh Routines,QoSCliUtlDisplayQEntry             */
/*****************************************************************************/
INT4
QoSCliShowQEntry (tCliHandle CliHandle, INT4 i4IfIdx)
{
    UINT4               u4QIdx = 0;
    INT4                i4TmpIfIdx = 0;
    INT4                i4RetStatus = SNMP_FAILURE;
    INT4                i4RowStatus = SNMP_FAILURE;

    /* Display All */
    CliPrintf (CliHandle, "QoS Queue Entries\r\n");
    CliPrintf (CliHandle, "-----------------\r\n");

    CliPrintf (CliHandle, "%-8s %-6s %-10s %-10s %-7s %-9s %-6s %-9s %-9s \r\n",
               "IfIndex", "Queue", "QTemplate", "Scheduler",
               "Weight", "Priority", "QType", "ShapeIdx", "GlobalId");
    CliPrintf (CliHandle, "%-8s %-6s %-10s %-10s %-7s %-9s %-6s %-9s %-9s\r\n",
               "-------", "-----", "---------", "---------",
               "------", "--------", "-----", "--------", "--------");

    i4TmpIfIdx = i4IfIdx;

    i4RetStatus = nmhGetFirstIndexFsQoSQTable (&i4IfIdx, &u4QIdx);
    if (i4RetStatus == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    do
    {
        /* Check the Status */
        i4RetStatus = nmhGetFsQoSQStatus (i4IfIdx, u4QIdx, &i4RowStatus);
        if (i4RetStatus == SNMP_FAILURE)
        {
            return (CLI_FAILURE);
        }

        if (i4RowStatus == ACTIVE)
        {
            if ((i4TmpIfIdx == 0) || (i4IfIdx == i4TmpIfIdx))
            {
                /* Display Entry */
                i4RetStatus = QoSCliUtlDisplayQEntry (CliHandle, i4IfIdx,
                                                      u4QIdx);
                if (i4RetStatus == CLI_FAILURE)
                {
                    return (CLI_FAILURE);
                }
            }
        }

        i4RetStatus = nmhGetNextIndexFsQoSQTable (i4IfIdx, &i4IfIdx,
                                                  u4QIdx, &u4QIdx);
    }
    while (i4RetStatus == SNMP_SUCCESS);

    CliPrintf (CliHandle, "\r\n");
    return (CLI_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSCliAddQMapEntry                                   */
/* Description        : This function is used to create QueueMap table Entry */
/* Input(s)           : CliHandle    - Handle to CLI session                 */
/*                    : i4IfIdx      - Interface Idx                         */
/*                    : i4MapType    - MapTye   CLASS or PRI_TYPE            */
/*                    : u4MapVal     - Value for the Type                    */
/*                    : u4QId        - Q Id for the Map                      */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : CLI_SUCCESS/CLI_FAILURE                              */
/* Called By          : cli_process_qos_cmd                                  */
/* Calling Function   : SNMP nmh Routines                                    */
/*****************************************************************************/
INT4
QoSCliAddQMapEntry (tCliHandle CliHandle, INT4 i4IfIdx, INT4 i4MapType,
                    UINT4 u4MapVal, UINT4 u4QIdx, INT4 i4DEIVal)
{
    UINT4               u4ErrorCode = 0;
    INT4                i4RetStatus = SNMP_FAILURE;
    INT4                i4RowStatus = 0;
    UINT4               u4CLASS = 0;
    UINT4               u4PriVal = 0;
    UINT4               u4CurQIdx = (UINT4) CLI_RS_INVALID_MASK;

    UNUSED_PARAM (i4DEIVal);

    /* 1. Check the Table Entry 
     * 2. If the Entry exists Set it as NOT_IN_SERVICE
     * 3. Configure the Given Values.
     * 4. If not exists create and Configure the Values.
     * 5. Any Failure in the New Entry Configurations Delete the Entry.
     * 6. Any Failure in the Modification of an Entry will Restore the 
     *    old Values.
     */

    if (i4MapType == QOS_CLI_QMAP_PRI_TYPE_NONE)
    {
        CliPrintf (CliHandle,
                   "\r Delete and re-create the policy-maps of this CLASS (if any)."
                   "The meter entries with conform/exceed/violate New CLASS values"
                   "as this CLASS also require to be re-created.\r\n");

        u4CLASS = u4MapVal;
        u4PriVal = 0;
    }
    else
    {
        u4CLASS = 0;
        u4PriVal = u4MapVal;
    }

    i4RetStatus = nmhGetFsQoSQMapStatus (i4IfIdx, u4CLASS, i4MapType, u4PriVal,
                                         &i4RowStatus);
    if (i4RetStatus == QOS_FAILURE)
    {
        i4RetStatus =
            nmhTestv2FsQoSQMapStatus (&u4ErrorCode, i4IfIdx, u4CLASS,
                                      i4MapType, u4PriVal, CREATE_AND_WAIT);

        if (i4RetStatus == SNMP_FAILURE)
        {
            return (CLI_FAILURE);
        }

        i4RetStatus = nmhSetFsQoSQMapStatus (i4IfIdx, u4CLASS, i4MapType,
                                             u4PriVal, CREATE_AND_WAIT);
        if (i4RetStatus == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }
    }
    else if (i4RowStatus == ACTIVE)
    {
        i4RetStatus =
            nmhTestv2FsQoSQMapStatus (&u4ErrorCode, i4IfIdx, u4CLASS, i4MapType,
                                      u4PriVal, NOT_IN_SERVICE);
        if (i4RetStatus == SNMP_FAILURE)
        {
            return (CLI_FAILURE);
        }

        i4RetStatus = nmhSetFsQoSQMapStatus (i4IfIdx, u4CLASS, i4MapType,
                                             u4PriVal, NOT_IN_SERVICE);
        if (i4RetStatus == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }
    }

    i4RetStatus = nmhTestv2FsQoSQMapQId (&u4ErrorCode, i4IfIdx, u4CLASS,
                                         i4MapType, u4PriVal, u4QIdx);

    if (i4RetStatus == SNMP_FAILURE)
    {
        if (u4ErrorCode == SNMP_ERR_WRONG_TYPE)
        {
            CliPrintf (CliHandle,
                       "! Supported Queue ID range is between 1 and %u\r\n",
                       (QOS_QUEUE_ENTRY_MAX + QOS_QUEUE_MAX_NUM_SUBQ));
        }
        QoSCliRSQMapEntry (i4IfIdx, u4CLASS, i4MapType, u4PriVal,
                           u4CurQIdx, i4RowStatus);
        return (CLI_FAILURE);
    }

    i4RetStatus = nmhGetFsQoSQMapQId (i4IfIdx, u4CLASS, i4MapType, u4PriVal,
                                      &u4CurQIdx);
    if (i4RetStatus == SNMP_FAILURE)
    {
        QoSCliRSQMapEntry (i4IfIdx, u4CLASS, i4MapType, u4PriVal,
                           u4CurQIdx, i4RowStatus);
        return (CLI_FAILURE);
    }

    i4RetStatus = nmhSetFsQoSQMapQId (i4IfIdx, u4CLASS, i4MapType,
                                      u4PriVal, u4QIdx);
    if (i4RetStatus == SNMP_FAILURE)
    {
        QoSCliRSQMapEntry (i4IfIdx, u4CLASS, i4MapType, u4PriVal, u4CurQIdx,
                           i4RowStatus);
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }

    i4RetStatus = nmhTestv2FsQoSQMapStatus (&u4ErrorCode, i4IfIdx, u4CLASS,
                                            i4MapType, u4PriVal, ACTIVE);
    if (i4RetStatus == SNMP_FAILURE)
    {
        QoSCliRSQMapEntry (i4IfIdx, u4CLASS, i4MapType, u4PriVal, u4CurQIdx,
                           i4RowStatus);
        return (CLI_FAILURE);
    }

    i4RetStatus = nmhSetFsQoSQMapStatus (i4IfIdx, u4CLASS, i4MapType, u4PriVal,
                                         ACTIVE);
    if (i4RetStatus == SNMP_FAILURE)
    {
        QoSCliRSQMapEntry (i4IfIdx, u4CLASS, i4MapType, u4PriVal, u4CurQIdx,
                           i4RowStatus);
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }

    if ((i4MapType == QOS_CLI_QMAP_PRI_TYPE_NONE) &&
        (ISS_HW_SUPPORTED !=
         IssGetHwCapabilities (ISS_HW_QOS_CLASS_TO_POLICER_AND_Q)))
    {
        CliPrintf (CliHandle,
                   "\r Delete and re-create the policy-maps of this CLASS (if any)."
                   "The meter entries with conform/exceed/violate New CLASS values"
                   "as this CLASS also require to be re-created.\r\n");
    }

    return (CLI_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSCliDelQMapEntry                                   */
/* Description        : This function is used to delete QueueMap table Entry */
/* Input(s)           : CliHandle    - Handle to CLI session                 */
/*                    : i4IfIdx      - Interface Idx                         */
/*                    : i4MapType    - MapTye   CLASS or PRI_TYPE            */
/*                    : u4MapVal     - Value for the Type                    */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : CLI_SUCCESS/CLI_FAILURE                              */
/* Called By          : cli_process_qos_cmd                                  */
/* Calling Function   : SNMP nmh Routines                                    */
/*****************************************************************************/
INT4
QoSCliDelQMapEntry (tCliHandle CliHandle, INT4 i4IfIdx, INT4 i4MapType,
                    UINT4 u4MapVal)
{
    UINT4               u4ErrorCode = 0;
    INT4                i4RetStatus = SNMP_FAILURE;
    UINT4               u4CLASS = 0;
    UINT4               u4PriVal = 0;

    if (i4MapType == QOS_CLI_QMAP_PRI_TYPE_NONE)
    {
        if (ISS_HW_SUPPORTED !=
            IssGetHwCapabilities (ISS_HW_QOS_CLASS_TO_POLICER_AND_Q))
        {
            CliPrintf (CliHandle,
                       "\r Delete and re-create the policy-maps of this CLASS (if any)."
                       "The meter entries with conform/exceed/violate New CLASS values"
                       "as this CLASS also require to be re-created.\r\n");
        }
        u4CLASS = u4MapVal;
        u4PriVal = 0;
    }
    else
    {
        u4CLASS = 0;
        u4PriVal = u4MapVal;
    }

    i4RetStatus = nmhTestv2FsQoSQMapStatus (&u4ErrorCode, i4IfIdx, u4CLASS,
                                            i4MapType, u4PriVal, DESTROY);
    if (i4RetStatus == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    i4RetStatus = nmhSetFsQoSQMapStatus (i4IfIdx, u4CLASS, i4MapType,
                                         u4PriVal, DESTROY);
    if (i4RetStatus == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }

    return (CLI_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSCliShowQMapEntry                                  */
/* Description        : This function is used to Display Q Map table Entry   */
/* Input(s)           : CliHandle    - Handle to CLI session                 */
/*                    : i4IfIdx      - Interface Idx                         */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : CLI_SUCCESS/CLI_FAILURE                              */
/* Called By          : cli_process_qos_cmd                                  */
/* Calling Function   : SNMP nmh Routines                                    */
/*****************************************************************************/
INT4
QoSCliShowQMapEntry (tCliHandle CliHandle, INT4 i4IfIdx)
{
    UINT4               u4PriVal = 0;
    INT4                i4PriType = 0;
    UINT4               u4CLASS = 0;
    UINT4               u4QId = 0;
    INT4                i4TmpIfIdx = 0;
    INT4                i4RetStatus = SNMP_FAILURE;
    INT4                i4RowStatus = SNMP_FAILURE;
    UINT1               au1IfName[CFA_MAX_PORT_NAME_LENGTH];
    /* Display All */
    CliPrintf (CliHandle, "QoS Queue Map Entries\r\n");
    CliPrintf (CliHandle, "---------------------\r\n");

    CliPrintf (CliHandle, "%-10s %-10s %-15s %-15s %-15s \r\n",
               "IfIndex", "CLASS", "PriorityType", "Priority Value",
               "Mapped Queue");
    CliPrintf (CliHandle, "%-10s %-10s %-15s %-15s %-15s \r\n",
               "----------", "----------", "---------------", "---------------",
               "---------------");

    i4TmpIfIdx = i4IfIdx;

    i4RetStatus = nmhGetFirstIndexFsQoSQMapTable (&i4IfIdx, &u4CLASS,
                                                  &i4PriType, &u4PriVal);
    if (i4RetStatus == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    do
    {
        /* Check the Status */
        i4RetStatus = nmhGetFsQoSQMapStatus (i4IfIdx, u4CLASS, i4PriType,
                                             u4PriVal, &i4RowStatus);
        if (i4RetStatus == SNMP_FAILURE)
        {
            return (CLI_FAILURE);
        }
        MEMSET (au1IfName, 0, CFA_MAX_PORT_NAME_LENGTH);
        if (CfaCliGetIfName (i4IfIdx, (INT1 *) au1IfName) == CLI_FAILURE)
        {
            return (CLI_FAILURE);
        }

        if (i4RowStatus == ACTIVE)
        {
            if ((i4TmpIfIdx == 0) || (i4IfIdx == i4TmpIfIdx))
            {
                /* Display Entry */
                i4RetStatus = nmhGetFsQoSQMapQId (i4IfIdx, u4CLASS, i4PriType,
                                                  u4PriVal, &u4QId);
                if (i4RetStatus == SNMP_FAILURE)
                {
                    return (CLI_FAILURE);
                }

                switch (i4PriType)
                {
                    case QOS_CLI_QMAP_PRI_TYPE_NONE:
                        CliPrintf (CliHandle, "%-10s %-10d %-15s %-15d %-15d"
                                   "\r\n", au1IfName, u4CLASS, "none", u4PriVal,
                                   u4QId);
                        break;
                    case QOS_CLI_QMAP_PRI_TYPE_VLAN_PRI:
                        CliPrintf (CliHandle, "%-10s %-10s %-15s %-15d %-15d"
                                   "\r\n", au1IfName, "none", "VlanPri",
                                   u4PriVal, u4QId);
                        break;
                    case QOS_CLI_QMAP_PRI_TYPE_DOT1P:
                        CliPrintf (CliHandle, "%-10s %-10s %-15s %-15d %-15d"
                                   "\r\n", au1IfName, "none", "Dot1P", u4PriVal,
                                   u4QId);
                        break;
                    case QOS_CLI_QMAP_PRI_TYPE_IP_TOS:
                        CliPrintf (CliHandle, "%-10s %-10s %-15s %-15d %-15d"
                                   "\r\n", au1IfName, "none", "IPToS", u4PriVal,
                                   u4QId);
                        break;
                    case QOS_CLI_QMAP_PRI_TYPE_IP_DSCP:
                        CliPrintf (CliHandle, "%-10s %-10s %-15s %-15d %-15d"
                                   "\r\n", au1IfName, "none", "IPDSCP",
                                   u4PriVal, u4QId);
                        break;
                    case QOS_CLI_QMAP_PRI_TYPE_MPLS_EXP:
                        CliPrintf (CliHandle, "%-10s %-10s %-15s %-15d %-15d"
                                   "\r\n", au1IfName, "none", "MPLSExp",
                                   u4PriVal, u4QId);
                        break;
                    case QOS_CLI_QMAP_PRI_TYPE_VLAN_DEI:
                        CliPrintf (CliHandle, "%-10s %-10s %-15s %-15d %-15d"
                                   "\r\n", au1IfName, "none", "VlanDEI",
                                   u4PriVal, u4QId);
                        break;
                    case QOS_CLI_QMAP_PRI_TYPE_INT_PRI:
                        CliPrintf (CliHandle, "%-10s %-10s %-15s %-15d %-15d"
                                   "\r\n", au1IfName, "none", "InternalPri",
                                   u4PriVal, u4QId);
                        break;

                    default:
                        CliPrintf (CliHandle, "%-10s %-10s %-15s %-15d %-15d"
                                   "\r\n", au1IfName, "none", "Invalid",
                                   u4PriVal, u4QId);
                        break;
                }
            }
        }

        i4RetStatus = nmhGetNextIndexFsQoSQMapTable (i4IfIdx, &i4IfIdx,
                                                     u4CLASS, &u4CLASS,
                                                     i4PriType, &i4PriType,
                                                     u4PriVal, &u4PriVal);
    }
    while (i4RetStatus == SNMP_SUCCESS);

    CliPrintf (CliHandle, "\r\n");
    return (CLI_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSCliRSHSEntry                                      */
/* Description        : This function is used to Reset HS table Entry       */
/* Input(s)           : pHSCurParam   - Ptr to tQoSCliHSParam                */
/*                    : i4RowStatus   - RowStatus                            */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : CLI_SUCCESS/CLI_FAILURE                              */
/* Called By          : cli_process_qos_cmd                                  */
/* Calling Function   : SNMP nmh Routines                                    */
/*****************************************************************************/
VOID
QoSCliRSHSEntry (tQoSCliHSParam * pHSCurParam, INT4 i4RowStatus)
{
    INT4                i4RetStatus = SNMP_FAILURE;
    INT4                i4IfIdx = 0;
    UINT4               u4SchedId = 0;
    UINT4               u4HL = 0;

    i4IfIdx = (INT4) pHSCurParam->u4IfIdx;
    u4HL = pHSCurParam->u4HL;
    u4SchedId = pHSCurParam->u4SchedId;

    if (i4RowStatus == 0)
    {
        i4RetStatus = nmhSetFsQoSHierarchyStatus (i4IfIdx, u4HL, u4SchedId,
                                                  DESTROY);
        return;
    }
    if (pHSCurParam->u4NextQ != (UINT4) CLI_RS_INVALID_MASK)
    {
        i4RetStatus = nmhSetFsQoSHierarchyQNext (i4IfIdx, u4HL, u4SchedId,
                                                 pHSCurParam->u4NextQ);
    }
    if (pHSCurParam->u4NextSched != (UINT4) CLI_RS_INVALID_MASK)
    {
        i4RetStatus = nmhSetFsQoSHierarchySchedNext (i4IfIdx, u4HL, u4SchedId,
                                                     pHSCurParam->u4NextSched);
    }
    if (pHSCurParam->u4Weight != (UINT4) CLI_RS_INVALID_MASK)
    {
        i4RetStatus = nmhSetFsQoSHierarchyWeight (i4IfIdx, u4HL, u4SchedId,
                                                  pHSCurParam->u4Weight);
    }
    if (pHSCurParam->u4Pri != (UINT4) CLI_RS_INVALID_MASK)
    {
        i4RetStatus = nmhSetFsQoSHierarchyPriority (i4IfIdx, u4HL, u4SchedId,
                                                    (INT4) pHSCurParam->u4Pri);
    }

    i4RetStatus = nmhSetFsQoSHierarchyStatus (i4IfIdx, u4HL, u4SchedId,
                                              i4RowStatus);
    UNUSED_PARAM (i4RetStatus);

}

/*****************************************************************************/
/* Function Name      : QoSCliAddHSEntry                                     */
/* Description        : This function is used to Add HS table Entry          */
/* Input(s)           : CliHandle    - Handle to CLI session                 */
/*                    : pHSCurParam   - Ptr to tQoSCliHSParam                */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : CLI_SUCCESS/CLI_FAILURE                              */
/* Called By          : cli_process_qos_cmd                                  */
/* Calling Function   : SNMP nmh Routines                                    */
/*****************************************************************************/

INT4
QoSCliAddHSEntry (tCliHandle CliHandle, tQoSCliHSParam * pHSParam)
{
    tQoSCliHSParam      HSCurParam;
    UINT4               u4ErrorCode = 0;
    INT4                i4RetStatus = SNMP_FAILURE;
    INT4                i4RowStatus = 0;
    INT4                i4IfIdx = 0;
    UINT4               u4HL = 0;
    UINT4               u4SchedId = 0;
    tQoSHierarchyNode  *pHierarchyNode = NULL;

    MEMSET (&HSCurParam, 0xFF, (sizeof (tQoSCliHSParam)));

    /* 1. Check the Table Entry 
     * 2. If the Entry exists Set it as NOT_IN_SERVICE
     * 3. Configure the Given Values.
     * 4. If not exists create and Configure the Values.
     * 5. Any Failure in the New Entry Configurations Delete the Entry.
     * 6. Any Failure in the Modification of an Entry will Restore the 
     *    old Values.
     */

    i4IfIdx = (INT4) pHSParam->u4IfIdx;
    u4HL = pHSParam->u4HL;
    u4SchedId = pHSParam->u4SchedId;

    HSCurParam.u4IfIdx = pHSParam->u4IfIdx;
    HSCurParam.u4HL = pHSParam->u4HL;
    HSCurParam.u4SchedId = pHSParam->u4SchedId;
    HSCurParam.u4NextSched = pHSParam->u4NextSched;

    i4RetStatus = nmhGetFsQoSHierarchyStatus (i4IfIdx, u4HL, u4SchedId,
                                              &i4RowStatus);
    if (i4RetStatus == QOS_FAILURE)
    {
        i4RetStatus =
            nmhTestv2FsQoSHierarchyStatus (&u4ErrorCode, i4IfIdx, u4HL,
                                           u4SchedId, CREATE_AND_WAIT);
        if (i4RetStatus == SNMP_FAILURE)
        {
            return (CLI_FAILURE);
        }

        i4RetStatus = nmhSetFsQoSHierarchyStatus (i4IfIdx, u4HL, u4SchedId,
                                                  CREATE_AND_WAIT);
        if (i4RetStatus == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }
    }
    else if (i4RowStatus == ACTIVE)
    {
        i4RetStatus =
            nmhTestv2FsQoSHierarchyStatus (&u4ErrorCode, i4IfIdx, u4HL,
                                           u4SchedId, NOT_IN_SERVICE);
        if (i4RetStatus == SNMP_FAILURE)
        {
            return (CLI_FAILURE);
        }

        i4RetStatus = nmhSetFsQoSHierarchyStatus (i4IfIdx, u4HL, u4SchedId,
                                                  NOT_IN_SERVICE);
        if (i4RetStatus == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }
    }

    if (pHSParam->u4NextQ != (UINT4) CLI_RS_INVALID_MASK)
    {
        i4RetStatus =
            nmhTestv2FsQoSHierarchyQNext (&u4ErrorCode, i4IfIdx, u4HL,
                                          u4SchedId, pHSParam->u4NextQ);
        if (i4RetStatus == SNMP_FAILURE)
        {
            QoSCliRSHSEntry (&HSCurParam, i4RowStatus);
            return (CLI_FAILURE);
        }

        i4RetStatus = nmhGetFsQoSHierarchyQNext (i4IfIdx, u4HL, u4SchedId,
                                                 &(HSCurParam.u4NextQ));
        if (i4RetStatus == SNMP_FAILURE)
        {
            CLI_SET_ERR (QOS_CLI_ERR_NEXT_SCHED_Q_NO_ENTRY);
            return (CLI_FAILURE);
        }

        i4RetStatus = nmhSetFsQoSHierarchyQNext (i4IfIdx, u4HL, u4SchedId,
                                                 pHSParam->u4NextQ);
        if (i4RetStatus == SNMP_FAILURE)
        {
            QoSCliRSHSEntry (&HSCurParam, i4RowStatus);
            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }
    }

    if (pHSParam->u4NextSched != (UINT4) CLI_RS_INVALID_MASK)
    {
        i4RetStatus =
            nmhTestv2FsQoSHierarchySchedNext (&u4ErrorCode, i4IfIdx, u4HL,
                                              u4SchedId, pHSParam->u4NextSched);
        if (i4RetStatus == SNMP_FAILURE)
        {
            QoSCliRSHSEntry (&HSCurParam, i4RowStatus);
            return (CLI_FAILURE);
        }

        i4RetStatus = nmhGetFsQoSHierarchySchedNext (i4IfIdx, u4HL, u4SchedId,
                                                     &(HSCurParam.u4NextSched));
        if (i4RetStatus == SNMP_FAILURE)
        {
            CLI_SET_ERR (QOS_CLI_ERR_NEXT_SCHED_Q_NO_ENTRY);
            return (CLI_FAILURE);
        }

        i4RetStatus = nmhSetFsQoSHierarchySchedNext (i4IfIdx, u4HL, u4SchedId,
                                                     pHSParam->u4NextSched);
        if (i4RetStatus == SNMP_FAILURE)
        {
            QoSCliRSHSEntry (&HSCurParam, i4RowStatus);
            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }
    }
    if (HSCurParam.u4HL == 1)
    {
        pHierarchyNode = QoSUtlGetHierarchyNode (i4IfIdx, u4HL, u4SchedId);
        if (pHierarchyNode != NULL)
        {
            pHierarchyNode->u1Status = NOT_IN_SERVICE;
        }

    }

    if (pHSParam->u4Weight != (UINT4) CLI_RS_INVALID_MASK)
    {
        i4RetStatus =
            nmhTestv2FsQoSHierarchyWeight (&u4ErrorCode, i4IfIdx, u4HL,
                                           u4SchedId, pHSParam->u4Weight);
        if (i4RetStatus == SNMP_FAILURE)
        {
            QoSCliRSHSEntry (&HSCurParam, i4RowStatus);
            return (CLI_FAILURE);
        }

        i4RetStatus = nmhGetFsQoSHierarchyWeight (i4IfIdx, u4HL, u4SchedId,
                                                  &(HSCurParam.u4Weight));
        if (i4RetStatus == SNMP_FAILURE)
        {
            return (CLI_FAILURE);
        }

        i4RetStatus = nmhSetFsQoSHierarchyWeight (i4IfIdx, u4HL, u4SchedId,
                                                  pHSParam->u4Weight);
        if (i4RetStatus == SNMP_FAILURE)
        {
            QoSCliRSHSEntry (&HSCurParam, i4RowStatus);
            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }
    }

    if (pHSParam->u4Pri != (UINT4) CLI_RS_INVALID_MASK)
    {
        i4RetStatus =
            nmhTestv2FsQoSHierarchyPriority (&u4ErrorCode, i4IfIdx, u4HL,
                                             u4SchedId, pHSParam->u4Pri);
        if (i4RetStatus == SNMP_FAILURE)
        {
            QoSCliRSHSEntry (&HSCurParam, i4RowStatus);
            return (CLI_FAILURE);
        }

        i4RetStatus = nmhGetFsQoSHierarchyPriority (i4IfIdx, u4HL, u4SchedId,
                                                    (INT4 *) &(HSCurParam.
                                                               u4Pri));
        if (i4RetStatus == SNMP_FAILURE)
        {
            return (CLI_FAILURE);
        }

        i4RetStatus = nmhSetFsQoSHierarchyPriority (i4IfIdx, u4HL, u4SchedId,
                                                    pHSParam->u4Pri);
        if (i4RetStatus == SNMP_FAILURE)
        {
            QoSCliRSHSEntry (&HSCurParam, i4RowStatus);
            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }
    }

    i4RetStatus = nmhTestv2FsQoSHierarchyStatus (&u4ErrorCode, i4IfIdx, u4HL,
                                                 u4SchedId, ACTIVE);
    if (i4RetStatus == SNMP_FAILURE)
    {
        QoSCliRSHSEntry (&HSCurParam, i4RowStatus);
        CLI_SET_ERR (QOS_CLI_ERR_NEXT_SCHED_Q_NO_ENTRY);
        return (CLI_FAILURE);
    }

    i4RetStatus = nmhSetFsQoSHierarchyStatus (i4IfIdx, u4HL, u4SchedId, ACTIVE);
    if (i4RetStatus == SNMP_FAILURE)
    {
        QoSCliRSHSEntry (&HSCurParam, i4RowStatus);
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }

    if (QOS_S3_SCHEDULER == u4HL)
    {
        i4RetStatus = QoSHwWrSchedulerUpdateParams (i4IfIdx,
                                                    pHSParam->u4NextSched,
                                                    QOS_SCHED_UPDATE_ALGO);
        if (i4RetStatus == QOS_FAILURE)
        {
            QoSCliRSHSEntry (&HSCurParam, i4RowStatus);
            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }
    }

    return (CLI_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSCliDelHSEntry                                     */
/* Description        : This function is used to delete HS table Entry       */
/* Input(s)           : CliHandle    - Handle to CLI session                 */
/*                    : i4IfIdx      - Interface Idx                         */
/*                    : u4HL         - Hierarchy Level                       */
/*                    : u4SchedId    - Scheduler Idx                         */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : CLI_SUCCESS/CLI_FAILURE                              */
/* Called By          : cli_process_qos_cmd                                  */
/* Calling Function   : SNMP nmh Routines                                    */
/*****************************************************************************/
INT4
QoSCliDelHSEntry (tCliHandle CliHandle, INT4 i4IfIdx, UINT4 u4HL,
                  UINT4 u4SchedId)
{
    UINT4               u4ErrorCode = 0;
    INT4                i4RetStatus = SNMP_FAILURE;

    i4RetStatus = nmhTestv2FsQoSHierarchyStatus (&u4ErrorCode, i4IfIdx, u4HL,
                                                 u4SchedId, DESTROY);
    if (i4RetStatus == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    i4RetStatus = nmhSetFsQoSHierarchyStatus (i4IfIdx, u4HL, u4SchedId,
                                              DESTROY);
    if (i4RetStatus == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }

    return (CLI_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSCliUtlDisplayHSEntry                              */
/* Description        : This function is used to Display the Hierarchy Sched */
/*                      Table Entries Parameters                             */
/* Input(s)           : CliHandle    - Handle to CLI session                 */
/*                    : i4IfIdx      - Interface Idx                         */
/*                    : u4HL         - Hierarchy Level                       */
/*                    : u4SchedId    - Scheduler Idx                         */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : CLI_SUCCESS/CLI_FAILURE                              */
/* Called By          : QoSCliShowQEntry                                     */
/* Calling Function   : SNMP nmh Routines                                    */
/*****************************************************************************/
INT4
QoSCliUtlDisplayHSEntry (tCliHandle CliHandle, INT4 i4IfIdx, UINT4 u4HL,
                         UINT4 u4SchedId)
{
    INT4                i4RetStatus = SNMP_FAILURE;
    UINT4               u4NextQ = 0;
    UINT4               u4NextSchedId = 0;
    UINT4               u4Weight = 0;
    INT4                i4Pri = 0;
    UINT1               au1IfName[CFA_MAX_PORT_NAME_LENGTH];
    /* Get All Fields */
    i4RetStatus = nmhGetFsQoSHierarchyQNext (i4IfIdx, u4HL, u4SchedId,
                                             &u4NextQ);
    if (i4RetStatus == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    i4RetStatus = nmhGetFsQoSHierarchySchedNext (i4IfIdx, u4HL, u4SchedId,
                                                 &u4NextSchedId);
    if (i4RetStatus == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    i4RetStatus = nmhGetFsQoSHierarchyWeight (i4IfIdx, u4HL, u4SchedId,
                                              &u4Weight);
    if (i4RetStatus == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    i4RetStatus = nmhGetFsQoSHierarchyPriority (i4IfIdx, u4HL, u4SchedId,
                                                &i4Pri);
    if (i4RetStatus == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }
    MEMSET (au1IfName, 0, CFA_MAX_PORT_NAME_LENGTH);
    if (CfaCliGetIfName (i4IfIdx, (INT1 *) au1IfName) == CLI_FAILURE)
    {
        return (CLI_FAILURE);
    }

    CliPrintf (CliHandle, "%-8s %-15d %-11d %-12d %-12d %-7d %-8d\r\n",
               au1IfName, u4HL, u4SchedId, u4NextQ, u4NextSchedId, u4Weight,
               i4Pri);

    return (CLI_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSCliShowHSEntry                                    */
/* Description        : This function is used to Display HS table Entry      */
/* Input(s)           : CliHandle    - Handle to CLI session                 */
/*                    : i4IfIdx      - Interface Idx                         */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : CLI_SUCCESS/CLI_FAILURE                              */
/* Called By          : cli_process_qos_cmd                                  */
/* Calling Function   : SNMP nmh Routines,QoSCliUtlDisplayHSEntry            */
/*****************************************************************************/
INT4
QoSCliShowHSEntry (tCliHandle CliHandle, INT4 i4IfIdx)
{
    UINT4               u4HL = 0;
    UINT4               u4SchedId = 0;
    INT4                i4TmpIfIdx = 0;
    INT4                i4RetStatus = SNMP_FAILURE;
    INT4                i4RowStatus = SNMP_FAILURE;

    /* Display All */
    CliPrintf (CliHandle, "QoS Hierarchy Scheduler Entries\r\n");
    CliPrintf (CliHandle, "-------------------------------\r\n");

    CliPrintf (CliHandle, "%-8s %-15s %-11s %-12s %-12s %-7s %-8s\r\n",
               "IfIndex", "Hierarchy Level", "Sched Index",
               "NextQueue Id", "NextSched Id", "Weight", "Priority");
    CliPrintf (CliHandle, "%-8s %-15s %-11s %-12s %-12s %-7s %-8s\r\n",
               "--------", "---------------", "-----------",
               "------------", "------------", "-------", "--------");
    i4TmpIfIdx = i4IfIdx;

    i4RetStatus = nmhGetFirstIndexFsQoSHierarchyTable (&i4IfIdx, &u4HL,
                                                       &u4SchedId);
    if (i4RetStatus == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    do
    {
        /* Check the Status */
        i4RetStatus = nmhGetFsQoSHierarchyStatus (i4IfIdx, u4HL, u4SchedId,
                                                  &i4RowStatus);
        if (i4RetStatus == SNMP_FAILURE)
        {
            return (CLI_FAILURE);
        }

        if (i4RowStatus == ACTIVE)
        {
            if ((i4TmpIfIdx == 0) || (i4IfIdx == i4TmpIfIdx))
            {
                /* Display Entry */
                i4RetStatus = QoSCliUtlDisplayHSEntry (CliHandle, i4IfIdx,
                                                       u4HL, u4SchedId);
                if (i4RetStatus == CLI_FAILURE)
                {
                    return (CLI_FAILURE);
                }
            }
        }

        i4RetStatus =
            nmhGetNextIndexFsQoSHierarchyTable (i4IfIdx, &i4IfIdx, u4HL,
                                                &u4HL, u4SchedId, &u4SchedId);
    }
    while (i4RetStatus == SNMP_SUCCESS);

    return (CLI_SUCCESS);
}

INT4
QoSCliSetPbitPreferenceOverDscp (tCliHandle CliHandle, INT4 i4IfIdx,
                                 INT4 i4Pref)
{
    UINT4               u4ErrorCode = 0;
    INT4                i4RetStatus = SNMP_FAILURE;

    i4RetStatus = nmhTestv2FsQoSPortPbitPrefOverDscp (&u4ErrorCode,
                                                      i4IfIdx, i4Pref);
    if (i4RetStatus == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    i4RetStatus = nmhSetFsQoSPortPbitPrefOverDscp (i4IfIdx, i4Pref);
    if (i4RetStatus == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }

    return (CLI_SUCCESS);
}

INT4
QoSCliShowPbitPreference (tCliHandle CliHandle, INT4 i4IfIdx)
{

    INT4                i4Pref = 0;
    INT4                i4TmpIfIdx = 0;
    INT4                i4RetStatus = SNMP_FAILURE;
    UINT1               au1IfName[CFA_MAX_PORT_NAME_LENGTH];
    /* Display All */
    CliPrintf (CliHandle, "QoS Default Pbit Preference Entries\r\n");
    CliPrintf (CliHandle, "---------------------------------\r\n");
    CliPrintf (CliHandle, "%-8s %s\r\n", "IfIndex",
               "Pbit preference over DSCP");
    CliPrintf (CliHandle, "%-8s %s\r\n", "--------",
               "-------------------------");

    i4TmpIfIdx = i4IfIdx;

    i4RetStatus = nmhGetFirstIndexFsQoSDefUserPriorityTable (&i4IfIdx);
    if (i4RetStatus == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    do
    {
        if ((i4TmpIfIdx == 0) || (i4IfIdx == i4TmpIfIdx))
        {
            MEMSET (au1IfName, 0, CFA_MAX_PORT_NAME_LENGTH);
            if (CfaCliGetIfName (i4IfIdx, (INT1 *) au1IfName) == CLI_FAILURE)
            {
                return (CLI_FAILURE);
            }
            /* Display Entry */
            i4RetStatus = nmhGetFsQoSPortPbitPrefOverDscp (i4IfIdx, &i4Pref);
            if (i4Pref == QOS_PBIT_PREF_ENABLE)
            {
                CliPrintf (CliHandle, "%-8s Enabled\r\n", au1IfName);
            }
            else
            {
                CliPrintf (CliHandle, "%-8s Disabled\r\n", au1IfName);
            }
        }

        i4RetStatus = nmhGetNextIndexFsQoSDefUserPriorityTable (i4IfIdx,
                                                                &i4IfIdx);
    }
    while (i4RetStatus == SNMP_SUCCESS);

    CliPrintf (CliHandle, "\r\n");

    return (CLI_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSCliSetDUPEntry                                    */
/* Description        : This function is used to set Default user Priority   */
/* Input(s)           : CliHandle    - Handle to CLI session                 */
/*                    : i4IfIdx      - Interface Idx                         */
/*                    : i4UDPri      - Default User Priority                 */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : CLI_SUCCESS/CLI_FAILURE                              */
/* Called By          : cli_process_qos_cmd                                  */
/* Calling Function   : SNMP nmh Routines                                    */
/*****************************************************************************/
INT4
QoSCliSetDUPEntry (tCliHandle CliHandle, INT4 i4IfIdx, INT4 i4UDPri)
{
    UINT4               u4ErrorCode = 0;
    INT4                i4RetStatus = SNMP_FAILURE;

    i4RetStatus = nmhTestv2FsQoSPortDefaultUserPriority (&u4ErrorCode,
                                                         i4IfIdx, i4UDPri);
    if (i4RetStatus == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    i4RetStatus = nmhSetFsQoSPortDefaultUserPriority (i4IfIdx, i4UDPri);
    if (i4RetStatus == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }

    return (CLI_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSCliShowDUPEntry                                   */
/* Description        : This function is used to Display DUP table Entry     */
/* Input(s)           : CliHandle    - Handle to CLI session                 */
/*                    : i4IfIdx      - Interface Idx                         */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : CLI_SUCCESS/CLI_FAILURE                              */
/* Called By          : cli_process_qos_cmd                                  */
/* Calling Function   : SNMP nmh Routines,                                   */
/*****************************************************************************/
INT4
QoSCliShowDUPEntry (tCliHandle CliHandle, INT4 i4IfIdx)
{
    INT4                i4UDPri = 0;
    INT4                i4TmpIfIdx = 0;
    INT4                i4RetStatus = SNMP_FAILURE;
    UINT1               au1IfName[CFA_MAX_PORT_NAME_LENGTH];
    /* Display All */
    CliPrintf (CliHandle, "QoS Default User Priority Entries\r\n");
    CliPrintf (CliHandle, "---------------------------------\r\n");
    CliPrintf (CliHandle, "%-8s %s\r\n", "IfIndex", "Default User Priority");
    CliPrintf (CliHandle, "%-8s %s\r\n", "--------", "---------------------");

    i4TmpIfIdx = i4IfIdx;

    i4RetStatus = nmhGetFirstIndexFsQoSDefUserPriorityTable (&i4IfIdx);
    if (i4RetStatus == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    do
    {
        if ((i4TmpIfIdx == 0) || (i4IfIdx == i4TmpIfIdx))
        {
            MEMSET (au1IfName, 0, CFA_MAX_PORT_NAME_LENGTH);
            if (CfaCliGetIfName (i4IfIdx, (INT1 *) au1IfName) == CLI_FAILURE)
            {
                return (CLI_FAILURE);
            }
            /* Display Entry */
            i4RetStatus = nmhGetFsQoSPortDefaultUserPriority (i4IfIdx,
                                                              &i4UDPri);

            CliPrintf (CliHandle, "%-8s %d\r\n", au1IfName, i4UDPri);
        }

        i4RetStatus = nmhGetNextIndexFsQoSDefUserPriorityTable (i4IfIdx,
                                                                &i4IfIdx);
    }
    while (i4RetStatus == SNMP_SUCCESS);

    CliPrintf (CliHandle, "\r\n");

    return (CLI_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSCliUtlDisplayMetStatsEntry                        */
/* Description        : This function is used to Display the Meter (Policer) */
/*                      Table Entries Parameters                             */
/* Input(s)           : CliHandle    - Handle to CLI session                 */
/*                    : u4Idx        - Table Entries Index                   */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : CLI_SUCCESS/CLI_FAILURE                              */
/* Called By          : QoSCliShowMetStats                                   */
/* Calling Function   : SNMP nmh Routines                                    */
/*****************************************************************************/
INT4
QoSCliUtlDisplayMetStatsEntry (tCliHandle CliHandle, UINT4 u4Idx)
{
    INT4                i4RetStatus = SNMP_FAILURE;
    tSNMP_COUNTER64_TYPE u8ConPkt;
    tSNMP_COUNTER64_TYPE u8ExcPkt;
    tSNMP_COUNTER64_TYPE u8VioPkt;
    tSNMP_COUNTER64_TYPE u8ConOct;
    tSNMP_COUNTER64_TYPE u8ExcOct;
    tSNMP_COUNTER64_TYPE u8VioOct;

    MEMSET (&u8ConPkt, 0, sizeof (tSNMP_COUNTER64_TYPE));
    MEMSET (&u8ExcPkt, 0, sizeof (tSNMP_COUNTER64_TYPE));
    MEMSET (&u8VioPkt, 0, sizeof (tSNMP_COUNTER64_TYPE));
    MEMSET (&u8ConOct, 0, sizeof (tSNMP_COUNTER64_TYPE));
    MEMSET (&u8ExcOct, 0, sizeof (tSNMP_COUNTER64_TYPE));
    MEMSET (&u8VioOct, 0, sizeof (tSNMP_COUNTER64_TYPE));

    i4RetStatus = nmhGetFsQoSPolicerStatsConformPkts (u4Idx, &u8ConPkt);
    if (i4RetStatus == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    i4RetStatus = nmhGetFsQoSPolicerStatsConformOctets (u4Idx, &u8ConOct);
    if (i4RetStatus == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    i4RetStatus = nmhGetFsQoSPolicerStatsExceedPkts (u4Idx, &u8ExcPkt);
    if (i4RetStatus == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    i4RetStatus = nmhGetFsQoSPolicerStatsExceedOctets (u4Idx, &u8ExcOct);
    if (i4RetStatus == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    i4RetStatus = nmhGetFsQoSPolicerStatsViolatePkts (u4Idx, &u8VioPkt);
    if (i4RetStatus == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    i4RetStatus = nmhGetFsQoSPolicerStatsViolateOctets (u4Idx, &u8VioOct);
    if (i4RetStatus == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    CliPrintf (CliHandle, "%-28s : %d\r\n", "Meter Index", u4Idx);
    CliPrintf (CliHandle, "%-28s : %d%d\r\n", "Conform Packets",
               u8ConPkt.msn, u8ConPkt.lsn);
    CliPrintf (CliHandle, "%-28s : %d%d\r\n", "Conform Octets",
               u8ConOct.msn, u8ConOct.lsn);
    CliPrintf (CliHandle, "%-28s : %d%d\r\n", "Exceed Packets",
               u8ExcPkt.msn, u8ExcPkt.lsn);
    CliPrintf (CliHandle, "%-28s : %d%d\r\n", "Exceed Octets",
               u8ExcOct.msn, u8ExcOct.lsn);
    CliPrintf (CliHandle, "%-28s : %d%d\r\n", "Violate Packets",
               u8VioPkt.msn, u8VioPkt.lsn);
    CliPrintf (CliHandle, "%-28s : %d%d\r\n", "Violate Octets",
               u8VioOct.msn, u8VioOct.lsn);

    CliPrintf (CliHandle, "\r\n");
    return (CLI_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSCliShowMetStats                                   */
/* Description        : This function is used to Display Meter (Policer)     */
/*                       stats Table Entries                                 */
/* Input(s)           : CliHandle    - Handle to CLI session                 */
/*                    : u4MeterId    - Meter Idx                             */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : CLI_SUCCESS/CLI_FAILURE                              */
/* Called By          : cli_process_qos_cmd                                  */
/* Calling Function   : SNMP nmh Routines,                                   */
/*****************************************************************************/
INT4
QoSCliShowMetStats (tCliHandle CliHandle, UINT4 u4MeterId)
{
    INT4                i4RetStatus = SNMP_FAILURE;
    INT4                i4PolicerStatsStatus = 0;

    /* Display All */
    CliPrintf (CliHandle, "QoS Meter (Policer) Stats\r\n");
    CliPrintf (CliHandle, "-------------------------\r\n");

    if (u4MeterId == 0)
    {
        i4RetStatus = nmhGetFirstIndexFsQoSPolicerStatsTable (&u4MeterId);
        if (i4RetStatus == SNMP_FAILURE)
        {
            return (CLI_FAILURE);
        }

        do
        {
            /* Display Entry */
            i4RetStatus =
                nmhGetFsQoSPolicerStatsStatus (u4MeterId,
                                               &i4PolicerStatsStatus);
            if (i4RetStatus == SNMP_FAILURE)
            {
                return (CLI_FAILURE);
            }

            if (i4PolicerStatsStatus == QOS_STATS_DISABLE)
            {
                CliPrintf (CliHandle,
                           "Meter %d statistics status is disabled \r\n\n",
                           u4MeterId);
            }
            else
            {
                i4RetStatus =
                    QoSCliUtlDisplayMetStatsEntry (CliHandle, u4MeterId);
                if (i4RetStatus == CLI_FAILURE)
                {
                    return (CLI_FAILURE);
                }
            }
            i4RetStatus = nmhGetNextIndexFsQoSPolicerStatsTable (u4MeterId,
                                                                 &u4MeterId);
        }
        while (i4RetStatus == SNMP_SUCCESS);

        return (CLI_SUCCESS);
    }
    else
    {
        /* Display Entry */
        i4RetStatus =
            nmhGetFsQoSPolicerStatsStatus (u4MeterId, &i4PolicerStatsStatus);
        if (i4RetStatus == SNMP_FAILURE)
        {
            return (CLI_FAILURE);
        }

        if (i4PolicerStatsStatus == QOS_STATS_DISABLE)
        {
            CliPrintf (CliHandle,
                       "Meter %d statistics status is disabled \r\n\n",
                       u4MeterId);
        }
        else
        {

            i4RetStatus = QoSCliUtlDisplayMetStatsEntry (CliHandle, u4MeterId);
            if (i4RetStatus == CLI_FAILURE)
            {
                return (CLI_FAILURE);
            }
        }
    }

    return (CLI_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSCliUtlDisplayQStats                               */
/* Description        : This function is used to Display the Queue Stats     */
/*                      Table Entries Parameters                             */
/* Input(s)           : CliHandle    - Handle to CLI session                 */
/*                    : i4IfIdx      - Interface Idx                         */
/*                    : u4QIdx       - Q Idx                                 */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : CLI_SUCCESS/CLI_FAILURE                              */
/* Called By          : QoSCliShowQStats                                     */
/* Calling Function   : SNMP nmh Routines                                    */
/*****************************************************************************/
INT4
QoSCliUtlDisplayQStats (tCliHandle CliHandle, INT4 i4IfIdx, UINT4 u4QIdx)
{
    FS_UINT8            u8EnQPktCount;
    FS_UINT8            u8DeQPktCount;
    FS_UINT8            u8DisPktCount;
    FS_UINT8            u8EnQOctCount;
    FS_UINT8            u8DeQOctCount;
    FS_UINT8            u8DisOctCount;
    FS_UINT8            u8OpnOctCount;
    FS_UINT8            u8CMADOctCount;
    INT4                i4RetStatus = SNMP_FAILURE;
    tSNMP_COUNTER64_TYPE u8EnQPkt;
    tSNMP_COUNTER64_TYPE u8DeQPkt;
    tSNMP_COUNTER64_TYPE u8DisPkt;
    tSNMP_COUNTER64_TYPE u8EnQOct;
    tSNMP_COUNTER64_TYPE u8DeQOct;
    tSNMP_COUNTER64_TYPE u8DisOct;
    tSNMP_COUNTER64_TYPE u8OpnOct;
    tSNMP_COUNTER64_TYPE u8CMADOct;
    UINT1               au1IfName[CFA_MAX_PORT_NAME_LENGTH];
    UINT1               au1EnQPkt[CFA_CLI_U8_STR_LENGTH];
    UINT1               au1DeQPkt[CFA_CLI_U8_STR_LENGTH];
    UINT1               au1DisPkt[CFA_CLI_U8_STR_LENGTH];
    UINT1               au1EnQOct[CFA_CLI_U8_STR_LENGTH];
    UINT1               au1DeQOct[CFA_CLI_U8_STR_LENGTH];
    UINT1               au1DisOct[CFA_CLI_U8_STR_LENGTH];
    UINT1               au1OpnOct[CFA_CLI_U8_STR_LENGTH];
    UINT1               au1CMADOct[CFA_CLI_U8_STR_LENGTH];

    MEMSET (&u8EnQPkt, 0, sizeof (tSNMP_COUNTER64_TYPE));
    MEMSET (&u8DeQPkt, 0, sizeof (tSNMP_COUNTER64_TYPE));
    MEMSET (&u8DisPkt, 0, sizeof (tSNMP_COUNTER64_TYPE));
    MEMSET (&u8EnQOct, 0, sizeof (tSNMP_COUNTER64_TYPE));
    MEMSET (&u8DeQOct, 0, sizeof (tSNMP_COUNTER64_TYPE));
    MEMSET (&u8DisOct, 0, sizeof (tSNMP_COUNTER64_TYPE));
    MEMSET (&u8OpnOct, 0, sizeof (tSNMP_COUNTER64_TYPE));
    MEMSET (&u8CMADOct, 0, sizeof (tSNMP_COUNTER64_TYPE));
    MEMSET (au1EnQPkt, 0, CFA_CLI_U8_STR_LENGTH);
    MEMSET (au1EnQOct, 0, CFA_CLI_U8_STR_LENGTH);
    MEMSET (au1DeQPkt, 0, CFA_CLI_U8_STR_LENGTH);
    MEMSET (au1DeQOct, 0, CFA_CLI_U8_STR_LENGTH);
    MEMSET (au1DisPkt, 0, CFA_CLI_U8_STR_LENGTH);
    MEMSET (au1DisOct, 0, CFA_CLI_U8_STR_LENGTH);
    MEMSET (au1OpnOct, 0, CFA_CLI_U8_STR_LENGTH);
    MEMSET (au1CMADOct, 0, CFA_CLI_U8_STR_LENGTH);

    i4RetStatus = nmhGetFsQoSCoSQStatsEnQPkts (i4IfIdx, u4QIdx, &u8EnQPkt);
    if (i4RetStatus == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }
    FSAP_U8_ASSIGN_HI (&u8EnQPktCount, u8EnQPkt.msn);
    FSAP_U8_ASSIGN_LO (&u8EnQPktCount, u8EnQPkt.lsn);
    /* Converts the UINT8 value to string */
    FSAP_U8_2STR (&u8EnQPktCount, (CHR1 *) & au1EnQPkt);

    i4RetStatus = nmhGetFsQoSCoSQStatsEnQBytes (i4IfIdx, u4QIdx, &u8EnQOct);
    if (i4RetStatus == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }
    FSAP_U8_ASSIGN_HI (&u8EnQOctCount, u8EnQOct.msn);
    FSAP_U8_ASSIGN_LO (&u8EnQOctCount, u8EnQOct.lsn);
    /* Converts the UINT8 value to string */
    FSAP_U8_2STR (&u8EnQOctCount, (CHR1 *) & au1EnQOct);

    i4RetStatus = nmhGetFsQoSCoSQStatsDeQPkts (i4IfIdx, u4QIdx, &u8DeQPkt);
    if (i4RetStatus == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }
    FSAP_U8_ASSIGN_HI (&u8DeQPktCount, u8DeQPkt.msn);
    FSAP_U8_ASSIGN_LO (&u8DeQPktCount, u8DeQPkt.lsn);
    /* Converts the UINT8 value to string */
    FSAP_U8_2STR (&u8DeQPktCount, (CHR1 *) & au1DeQPkt);

    i4RetStatus = nmhGetFsQoSCoSQStatsDeQBytes (i4IfIdx, u4QIdx, &u8DeQOct);
    if (i4RetStatus == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }
    FSAP_U8_ASSIGN_HI (&u8DeQOctCount, u8DeQOct.msn);
    FSAP_U8_ASSIGN_LO (&u8DeQOctCount, u8DeQOct.lsn);
    /* Converts the UINT8 value to string */
    FSAP_U8_2STR (&u8DeQOctCount, (CHR1 *) & au1DeQOct);

    i4RetStatus = nmhGetFsQoSCoSQStatsDiscardPkts (i4IfIdx, u4QIdx, &u8DisPkt);
    if (i4RetStatus == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }
    FSAP_U8_ASSIGN_HI (&u8DisPktCount, u8DisPkt.msn);
    FSAP_U8_ASSIGN_LO (&u8DisPktCount, u8DisPkt.lsn);
    /* Converts the UINT8 value to string */
    FSAP_U8_2STR (&u8DisPktCount, (CHR1 *) & au1DisPkt);

    i4RetStatus = nmhGetFsQoSCoSQStatsDiscardBytes (i4IfIdx, u4QIdx, &u8DisOct);
    if (i4RetStatus == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }
    FSAP_U8_ASSIGN_HI (&u8DisOctCount, u8DisOct.msn);
    FSAP_U8_ASSIGN_LO (&u8DisOctCount, u8DisOct.lsn);
    /* Converts the UINT8 value to string */
    FSAP_U8_2STR (&u8DisOctCount, (CHR1 *) & au1DisOct);

    i4RetStatus = nmhGetFsQoSCoSQStatsOccupancy (i4IfIdx, u4QIdx, &u8OpnOct);
    if (i4RetStatus == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }
    FSAP_U8_ASSIGN_HI (&u8OpnOctCount, u8OpnOct.msn);
    FSAP_U8_ASSIGN_LO (&u8OpnOctCount, u8OpnOct.lsn);
    /* Converts the UINT8 value to string */
    FSAP_U8_2STR (&u8OpnOctCount, (CHR1 *) & au1OpnOct);

    i4RetStatus = nmhGetFsQoSCoSQStatsCongMgntAlgoDrop (i4IfIdx, u4QIdx,
                                                        &u8CMADOct);
    if (i4RetStatus == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }
    FSAP_U8_ASSIGN_HI (&u8CMADOctCount, u8CMADOct.msn);
    FSAP_U8_ASSIGN_LO (&u8CMADOctCount, u8CMADOct.lsn);
    /* Converts the UINT8 value to string */
    FSAP_U8_2STR (&u8CMADOctCount, (CHR1 *) & au1CMADOct);

    MEMSET (au1IfName, 0, CFA_MAX_PORT_NAME_LENGTH);
    if (CfaCliGetIfName (i4IfIdx, (INT1 *) au1IfName) == CLI_FAILURE)
    {
        return (CLI_FAILURE);
    }

    CliPrintf (CliHandle, "%-28s : %s\r\n", "Interface Index", au1IfName);
    CliPrintf (CliHandle, "%-28s : %d\r\n", "Queue Index", u4QIdx);
    CliPrintf (CliHandle, "%-28s : %s\r\n", "EnQ Packets", au1EnQPkt);
    CliPrintf (CliHandle, "%-28s : %s\r\n", "EnQ Octets", au1EnQOct);
    CliPrintf (CliHandle, "%-28s : %s\r\n", "DeQ Packets", au1DeQPkt);
    CliPrintf (CliHandle, "%-28s : %s\r\n", "DeQ Octets", au1DeQOct);
    CliPrintf (CliHandle, "%-28s : %s\r\n", "Discard Packets", au1DisPkt);
    CliPrintf (CliHandle, "%-28s : %s\r\n", "Discard Octets", au1DisOct);
    CliPrintf (CliHandle, "%-28s : %s\r\n", "Occupancy Octets", au1OpnOct);
    CliPrintf (CliHandle, "%-28s : %s\r\n", "CongMgntAlgoDrop Octets",
               au1CMADOct);

    CliPrintf (CliHandle, "\r\n");

    return (CLI_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSCliShowQStats                                     */
/* Description        : This function is used to Display Queue Stats         */
/*                      Table Entries                                        */
/* Input(s)           : CliHandle    - Handle to CLI session                 */
/*                    : i4IfIdx      - Interface Idx                         */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : CLI_SUCCESS/CLI_FAILURE                              */
/* Called By          : cli_process_qos_cmd                                  */
/* Calling Function   : SNMP nmh Routines, QoSCliUtlDisplayQStats            */
/*****************************************************************************/
INT4
QoSCliShowQStats (tCliHandle CliHandle, INT4 i4IfIdx)
{
    INT4                i4RetStatus = SNMP_FAILURE;
    INT4                i4TmpIfIdx = 0;
    UINT4               u4QIdx = 0;

    /* Display All */
    CliPrintf (CliHandle, "QoS Queue Stats\r\n");
    CliPrintf (CliHandle, "---------------\r\n");

    i4TmpIfIdx = i4IfIdx;

    i4RetStatus = nmhGetFirstIndexFsQoSCoSQStatsTable (&i4IfIdx, &u4QIdx);
    if (i4RetStatus == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    do
    {
        if ((i4TmpIfIdx == 0) || (i4IfIdx == i4TmpIfIdx))
        {
            /* Display Entry */
            i4RetStatus = QoSCliUtlDisplayQStats (CliHandle, i4IfIdx, u4QIdx);
            if (i4RetStatus == CLI_FAILURE)
            {
                return (CLI_FAILURE);
            }
        }

        i4RetStatus = nmhGetNextIndexFsQoSCoSQStatsTable (i4IfIdx, &i4IfIdx,
                                                          u4QIdx, &u4QIdx);
    }
    while (i4RetStatus == SNMP_SUCCESS);

    CliPrintf (CliHandle, "\r\n");

    return (CLI_SUCCESS);
}

/****************************************************************************
 *                                                                           
 *     FUNCTION NAME    : QoSCliShowRunningConfig                              
 *                                                                           
 *     DESCRIPTION      : This function displays current QoS  configurations    
 *                                                                           
 *     INPUT            : CliHandle - CliContext ID                             
 *                          
 *                                                                           
 *     OUTPUT           : None                                               
 *                                                                           
 *     RETURNS          : None                            
 *                                                                           
 *****************************************************************************/
INT4
QoSShowRunningConfig (tCliHandle CliHandle)
{
    INT4                i4SysControl = 0;
    INT4                i4SysStatus = 0;
    CliRegisterLock (CliHandle, QoSLock, QoSUnLock);
    QoSLock ();
    nmhGetFsQoSSystemControl (&i4SysControl);
    if (i4SysControl != QOS_SYS_CNTL_START)
    {
        CliPrintf (CliHandle, "shutdown qos\r\n");
        CliPrintf (CliHandle, "!\r\n");
        QoSUnLock ();
        CliUnRegisterLock (CliHandle);
        return (CLI_SUCCESS);
    }
    nmhGetFsQoSStatus (&i4SysStatus);
    if (i4SysStatus != QOS_SYS_STATUS_ENABLE)
    {
        CliPrintf (CliHandle, "qos disable\r\n");
        CliPrintf (CliHandle, "!\r\n");
    }
    /* If QoS module status is globally disabled configurations     */
    /* preserved in control plane database should not be displayed  */
    /* since all hardware configurations are erased already         */
    if (i4SysStatus == QOS_SYS_STATUS_ENABLE)
    {
        QoSCliShowRunningConfigTables (CliHandle);
    }
    QoSUnLock ();
    CliUnRegisterLock (CliHandle);
    return (CLI_SUCCESS);

}

/****************************************************************************
 *                                                                           
 *     FUNCTION NAME    : QoSCliShowRunningConfigTables                          *  
 *                                                                           
 *     DESCRIPTION      : This function displays current QoS  configurations    
 *                                                                           
 *     INPUT            : CliHandle - CliContext ID                             
 *                          
 *                                                                           
 *     OUTPUT           : None                                               
 *                                                                           
 *     RETURNS          : None                            
 *                                                                           
 *****************************************************************************/

INT4
QoSCliShowRunningConfigTables (tCliHandle CliHandle)
{

    QoSPriorityMapShowRunningConfig (CliHandle);
    QoSClassMapShowRunningConfig (CliHandle);
    QoSMeterShowRunningConfig (CliHandle);
    QoSPolicyMapShowRunningConfig (CliHandle);
    QoSPolicyMapStatsShowRunningConfig (CliHandle);
    QoSQueueTemplateShowRunningConfig (CliHandle);
    QoSShapeTemplateShowRunningConfig (CliHandle);
    QoSSchedulerTemplateShowRunningConfig (CliHandle);
    QoSQueueTableShowRunningConfig (CliHandle);
    QoSQueueMapShowRunningConfig (CliHandle);
    QoSHierarchyTableShowRunningConfig (CliHandle);
    QoSDefaultUserPriShowRunningConfig (CliHandle);
    QoSCpuRateLimitShowRunningConfig (CliHandle);
    QoSPortTableShowRunningConfig (CliHandle);
    QoSVlanQMapShowRunningConfig (CliHandle);
    return (CLI_SUCCESS);
}

/****************************************************************************
 *                                                                           
 *     FUNCTION NAME    : QoSShowRunningConfigInterfaceDetails                      
 *                                                                           
 *     DESCRIPTION      : This function displays current QOS   
 *                        configurations for teh interface
 *                                                                           
 *     INPUT            : CliHandle - CliContext ID                             
 *                          
 *                                                                           
 *     OUTPUT           : None                                               
 *                                                                           
 *     RETURNS          : None                            
 *                                                                           
 *****************************************************************************/
INT4                QoSShowRunningConfigInterfaceDetails
PROTO ((tCliHandle CliHandle, INT4 i4Index))
{
    INT4                i4FsQoSPcpPacketType = 0;
    INT4                i4FsQoSPcpSelRow = 0;
    UINT1               au1IfName[CFA_MAX_PORT_NAME_LENGTH];
    INT4                i4TmpIdx = 0;
#ifdef QOS_WANTED
    INT4                i4TempIndex = i4Index;
    INT4                i4Pref = 0;
#endif

    i4TmpIdx = i4Index;

    if (nmhGetFirstIndexFsQoSPortTable (&i4Index, &i4FsQoSPcpPacketType)
        != SNMP_FAILURE)
    {
        do
        {
            if (nmhGetFsQoSPcpSelRow (i4Index, i4FsQoSPcpPacketType,
                                      &i4FsQoSPcpSelRow) != SNMP_FAILURE)
            {
                if (i4Index == i4TmpIdx)
                {
                    MEMSET (au1IfName, 0, CFA_MAX_PORT_NAME_LENGTH);
                    CfaCliConfGetIfName ((UINT4) i4Index, (INT1 *) au1IfName);
                    CliPrintf (CliHandle, "interface %s \r\n", au1IfName);
                    switch (i4FsQoSPcpPacketType)
                    {
                        case QOS_CLI_ETHERNET_PKT:
                            CliPrintf (CliHandle, "qos packet-type Ethernet");
                            break;
                        case QOS_CLI_IP_PKT:
                            CliPrintf (CliHandle, "qos packet-type IP");
                            break;
                        case QOS_CLI_MPLS_PKT:
                            CliPrintf (CliHandle, "qos packet-type MPLS");
                            break;
                        default:
                            break;
                    }
                    switch (i4FsQoSPcpSelRow)
                    {
                        case QOS_CLI_8P0D_SEL_ROW:
                            CliPrintf (CliHandle,
                                       " pcp-selection-row 8P0D \r\n");
                            break;
                        case QOS_CLI_7P1D_SEL_ROW:
                            CliPrintf (CliHandle,
                                       " pcp-selection-row 7P1D \r\n");
                            break;
                        case QOS_CLI_6P2D_SEL_ROW:
                            CliPrintf (CliHandle,
                                       " pcp-selection-row 6P2D \r\n");
                            break;
                        case QOS_CLI_5P3D_SEL_ROW:
                            CliPrintf (CliHandle,
                                       " pcp-selection-row 5P3D \r\n");
                            break;
                        default:
                            break;
                    }

                }
                CliPrintf (CliHandle, "!\r\n");
            }
        }
        while (nmhGetNextIndexFsQoSPortTable (i4Index,
                                              &i4Index,
                                              i4FsQoSPcpPacketType,
                                              &i4FsQoSPcpPacketType) ==
               SNMP_SUCCESS);

    }
    /* the mode is set to disable  */
#ifdef QOS_WANTED
    if (nmhGetFsQoSPortPbitPrefOverDscp (i4TempIndex, &i4Pref) != SNMP_FAILURE)
    {
        if (i4Pref == QOS_PBIT_PREF_DISABLE)
        {
            MEMSET (au1IfName, 0, CFA_MAX_PORT_NAME_LENGTH);
            CfaCliConfGetIfName ((UINT4) i4TempIndex, (INT1 *) au1IfName);
            CliPrintf (CliHandle, "interface %s \r\n", au1IfName);
            CliPrintf (CliHandle, "qos pbit-preference disable\r\n");
        }
        CliPrintf (CliHandle, "!\r\n");
    }
#endif
    return FNP_SUCCESS;

}

/****************************************************************************
 *                                                                           
 *     FUNCTION NAME    : QoSPriorityMapShowRunningConfig                      
 *                                                                           
 *     DESCRIPTION      : This function displays current PriorityMap  
 *                        configurations    
 *                                                                           
 *     INPUT            : CliHandle - CliContext ID                             
 *                          
 *                                                                           
 *     OUTPUT           : None                                               
 *                                                                           
 *     RETURNS          : None                            
 *                                                                           
 *****************************************************************************/
INT4
QoSPriorityMapShowRunningConfig (tCliHandle CliHandle)
{
    INT4                i4RowStatus = SNMP_FAILURE;
    INT4                i4EntryType = 0;
    UINT4               u4IfNum = 0;
    UINT4               u4Idx = 0;
    UINT4               u4VlanId = 0;
    INT4                i4InPri = 0;
    INT4                i4InPriType = 0;
    UINT4               u4TrafCls = 0;
    UINT4               u4InnerReGenPri = 0;
    UINT1               au1IfName[CFA_MAX_PORT_NAME_LENGTH];
    tQoSInPriorityMapNode *pPriMapNode = NULL;
    UINT1               u1Flag = FALSE;
    UINT1               u1StatusFlag = FALSE;

    if (nmhGetFirstIndexFsQoSPriorityMapTable (&u4Idx) != SNMP_FAILURE)
    {

        do
        {
            u1Flag = FALSE;
            pPriMapNode = QoSUtlGetPriorityMapNode (u4Idx);
            if (pPriMapNode != NULL)
            {
                if (QoSIsDefPriorityMapTblEntry (pPriMapNode) == QOS_SUCCESS)
                {
                    u1Flag = TRUE;
                }
                if (QosUtlGetPriMapEntryType (u4Idx, &i4EntryType)
                    == QOS_SUCCESS)
                {
                    if (i4EntryType == QOS_PCP_ENTRY_TYPE_IMPLICIT)
                    {
                        u1Flag = TRUE;
                    }
                }
            }
            if (u1Flag == FALSE)
            {
                if (nmhGetFsQoSPriorityMapStatus (u4Idx, &i4RowStatus) !=
                    SNMP_FAILURE)
                {
                    u1StatusFlag = FALSE;
                    CliPrintf (CliHandle, "priority-map %d\r\n", u4Idx);
                    if (i4RowStatus != ACTIVE)
                    {
                        u1StatusFlag = TRUE;
                    }
                    if (u1StatusFlag == FALSE)
                    {
                        CliPrintf (CliHandle, " map ");
                        if (nmhGetFsQoSPriorityMapIfIndex (u4Idx, &u4IfNum) !=
                            SNMP_FAILURE)
                        {
                            if (u4IfNum != 0)
                            {
                                MEMSET (au1IfName, 0, CFA_MAX_PORT_NAME_LENGTH);
                                if (CfaCliConfGetIfName
                                    (u4IfNum,
                                     (INT1 *) au1IfName) == CLI_FAILURE)
                                {
                                    return (CLI_FAILURE);
                                }
                                CliPrintf (CliHandle, "interface %s ",
                                           au1IfName);
                            }
                        }

                        if (nmhGetFsQoSPriorityMapVlanId (u4Idx, &u4VlanId) !=
                            SNMP_FAILURE)
                        {
                            if (u4VlanId != 0)
                            {
                                CliPrintf (CliHandle, "Vlan %ld ", u4VlanId);
                            }
                        }
                        if (nmhGetFsQoSPriorityMapInPriType
                            (u4Idx, &i4InPriType) != SNMP_FAILURE)
                        {
                            CliPrintf (CliHandle, "in-Priority-type ");
                            if (i4InPriType == QOS_CLI_IN_PRI_TYPE_VLAN_PRI)
                            {
                                CliPrintf (CliHandle, "vlanPri ");
                            }
                            else if (i4InPriType == QOS_CLI_IN_PRI_TYPE_IP_TOS)
                            {
                                CliPrintf (CliHandle, "ipTos ");
                            }
                            else if (i4InPriType == QOS_CLI_IN_PRI_TYPE_IP_DSCP)
                            {
                                CliPrintf (CliHandle, "ipDscp ");
                            }
                            else if (i4InPriType ==
                                     QOS_CLI_IN_PRI_TYPE_MPLS_EXP)
                            {
                                CliPrintf (CliHandle, "mplsExp ");
                            }
                            else if (i4InPriType ==
                                     QOS_CLI_IN_PRI_TYPE_VLAN_DEI)
                            {
                                CliPrintf (CliHandle, "vlanDEI ");
                            }
                            else if (i4InPriType == QOS_CLI_IN_PRI_TYPE_DOT1P)
                            {
                                CliPrintf (CliHandle, "DOT1P ");
                            }
                        }
                        if (nmhGetFsQoSPriorityMapInPriority (u4Idx, &i4InPri)
                            != SNMP_FAILURE)
                        {
                            CliPrintf (CliHandle, "in-priority %ld ", i4InPri);
                        }

                        if (nmhGetFsQoSPriorityMapRegenPriority
                            (u4Idx, &u4TrafCls) != SNMP_FAILURE)
                        {
                            CliPrintf (CliHandle, "regen-priority %ld ",
                                       u4TrafCls);
                        }
                        if (nmhGetFsQoSPriorityMapRegenInnerPriority (u4Idx,
                                                                      &u4InnerReGenPri)
                            != SNMP_FAILURE)
                        {
                            if (u4InnerReGenPri !=
                                QOS_PRI_MAP_TBL_INVLD_INREGPRI)

                            {
                                CliPrintf (CliHandle,
                                           "regen-inner-priority %ld ",
                                           u4InnerReGenPri);
                            }
                        }
                        CliPrintf (CliHandle, "\r\n");

                        CliPrintf (CliHandle, "!\r\n");
                    }

                }
            }
        }

        while ((nmhGetNextIndexFsQoSPriorityMapTable (u4Idx, &u4Idx)
                == SNMP_SUCCESS));
        CliPrintf (CliHandle, "!\r\n");
    }

    return (CLI_SUCCESS);
}

/****************************************************************************
 *                                                                           
 *     FUNCTION NAME    : QoSClassMapShowRunningConfig                      
 *                                                                           
 *     DESCRIPTION      : This function displays current ClassMap configurations *   
 *                                                                           
 *     INPUT            : CliHandle - CliContext ID                             
 *                       
 *                                                                           
 *     OUTPUT           : None                                               
 *                                                                           
 *     RETURNS          : None                            
 *                                                                           
 *****************************************************************************/

INT4
QoSClassMapShowRunningConfig (tCliHandle CliHandle)
{
    INT4                i4RowStatus = SNMP_FAILURE;
    INT4                i4EntryType = 0;
    UINT4               u4Idx = 0;
    UINT4               u4ReGenPri = 0;
    UINT4               u4L2FltrId = 0;
    UINT4               u4L3FltrId = 0;
    UINT4               u4PriMapId = 0;
    UINT4               u4VlanQMapId = 0;
    UINT4               u4Class = 0;
    INT4                i4PreColor = -1;
    tSNMP_OCTET_STRING_TYPE *pOctStrName = NULL;
    tQoSClassMapNode   *pClsMapNode;
    UINT1               u1Flag = FALSE;
    UINT1               u1StatusFlag = FALSE;

    if (nmhGetFirstIndexFsQoSClassMapTable (&u4Idx) != SNMP_FAILURE)
    {
        do
        {
            u1Flag = FALSE;
            pClsMapNode = QoSUtlGetClassMapNode (u4Idx);
            if (pClsMapNode != NULL)
            {
                if (QoSIsDefClassMapTblEntry (pClsMapNode) == QOS_SUCCESS)
                {

                    u1Flag = TRUE;
                }
                if (QosUtlGetClassMapEntryType (u4Idx, &i4EntryType)
                    == QOS_SUCCESS)
                {
                    if (i4EntryType == QOS_PCP_ENTRY_TYPE_IMPLICIT)
                    {
                        u1Flag = TRUE;
                    }
                }
            }
            if (u1Flag == FALSE)
            {
                if (nmhGetFsQoSClassMapStatus (u4Idx, &i4RowStatus) !=
                    SNMP_FAILURE)
                {
                    u1StatusFlag = FALSE;
                    CliPrintf (CliHandle, "class-map %d\r\n", u4Idx);
                    if (i4RowStatus != ACTIVE)
                    {
                        u1StatusFlag = TRUE;
                    }
                    if (u1StatusFlag == FALSE)
                    {
                        /*To Show Class Map Parameters L2and/L3 or Priority Map Id */
                        CliPrintf (CliHandle, " match access-group ");
                        if (nmhGetFsQoSClassMapL2FilterId (u4Idx, &u4L2FltrId)
                            != SNMP_FAILURE)
                        {

                            if (u4L2FltrId != 0)
                            {
                                CliPrintf (CliHandle, "mac-access-list %ld ",
                                           u4L2FltrId);
                            }
                        }

                        if (nmhGetFsQoSClassMapL3FilterId (u4Idx, &u4L3FltrId)
                            != SNMP_FAILURE)
                        {
                            if (u4L3FltrId != 0)
                            {
                                CliPrintf (CliHandle, "ip-access-list %ld ",
                                           u4L3FltrId);
                            }
                        }

                        if (nmhGetFsQoSClassMapPriorityMapId
                            (u4Idx, &u4PriMapId) != SNMP_FAILURE)
                        {
                            if (u4PriMapId != 0)
                            {
                                CliPrintf (CliHandle, "priority-map %ld ",
                                           u4PriMapId);

                            }
                        }

                        if (nmhGetFsQoSClassMapVlanMapId
                            (u4Idx, &u4VlanQMapId) != SNMP_FAILURE)
                        {
                            if (u4VlanQMapId != 0)
                            {
                                CliPrintf (CliHandle, "vlan-map %ld ",
                                           u4VlanQMapId);

                            }
                        }
                        CliPrintf (CliHandle, "\r\n");
                        /* End of Class Map Parameters L2and/L3 or Priority Map Id */

                        if (nmhGetFsQoSClassMapCLASS (u4Idx, &u4Class) !=
                            SNMP_FAILURE)
                        {
                            if (u4Class != QOS_DEF_CLASS)
                            {
                                CliPrintf (CliHandle, " set class %ld ",
                                           u4Class);
                            }
                            /*To Show the  CLASS for L2and/L3 or Priority Map Id and Add a CLASS to Priority Map Entry with regen Priority. */
                            if (nmhGetFsQoSClassMapPreColor (u4Idx, &i4PreColor)
                                != SNMP_FAILURE)
                            {
                                if (i4PreColor != QOS_CLI_CLS_COLOR_NONE)
                                {
                                    CliPrintf (CliHandle, "pre-color ");
                                }
                                if (i4PreColor == QOS_CLI_CLS_COLOR_GREEN)
                                {
                                    CliPrintf (CliHandle, "green ");
                                }
                                else if (i4PreColor == QOS_CLI_CLS_COLOR_YELLOW)
                                {
                                    CliPrintf (CliHandle, "yellow ");
                                }
                                else if (i4PreColor == QOS_CLI_CLS_COLOR_RED)
                                {
                                    CliPrintf (CliHandle, "red ");
                                }
                            }
                            if (nmhGetFsQoSClassToPriorityRegenPri
                                (u4Class, &u4ReGenPri) != SNMP_FAILURE)
                            {
                                if (u4ReGenPri != 0)

                                {
                                    CliPrintf (CliHandle, "regen-priority %ld ",
                                               u4ReGenPri);
                                }
                            }
                            pOctStrName =
                                allocmem_octetstring (QOS_MAX_TABLE_NAME);
                            if (pOctStrName == NULL)
                            {
                                return (CLI_FAILURE);
                            }
                            MEMSET (pOctStrName->pu1_OctetList, 0,
                                    QOS_MAX_TABLE_NAME);
                            if (nmhGetFsQoSClassToPriorityGroupName
                                (u4Class, pOctStrName) != SNMP_FAILURE)
                            {

                                CliPrintf (CliHandle, "group-name %s ",
                                           pOctStrName->pu1_OctetList);
                            }
                            free_octetstring (pOctStrName);
                        }
                        CliPrintf (CliHandle, "\r\n");
                    }
                    CliPrintf (CliHandle, "!\r\n");
                }
            }
        }
        while ((nmhGetNextIndexFsQoSClassMapTable (u4Idx, &u4Idx)
                == SNMP_SUCCESS));
    }
    return (CLI_SUCCESS);
}

/****************************************************************************
 *                                                                           
 *     FUNCTION NAME    : QoSMeterShowRunningConfig                      
 *                                                                           
 *     DESCRIPTION      : This function displays current MeterTable   
 *                        configurations    
 *                                                                           
 *     INPUT            : CliHandle - CliContext ID                             
 *                          
 *                                                                           
 *     OUTPUT           : None                                               
 *                                                                           
 *     RETURNS          : None                            
 *                                                                           
 *****************************************************************************/
INT4
QoSMeterShowRunningConfig (tCliHandle CliHandle)
{

    INT4                i4RowStatus = SNMP_FAILURE;
    INT4                i4MeterType = 0;
    INT4                i4ColorMode = 0;
    UINT4               u4Idx = 0;
    UINT4               u4Interval = 0;
    UINT4               u4CIR = 0;
    UINT4               u4CBS = 0;
    UINT4               u4EIR = 0;
    UINT4               u4EBS = 0;
    UINT4               u4NextMeter = 0;
    UINT1               u1StatusFlag = FALSE;

    if (nmhGetFirstIndexFsQoSMeterTable (&u4Idx) != SNMP_FAILURE)
    {
        do
        {
            if (nmhGetFsQoSMeterStatus (u4Idx, &i4RowStatus) != SNMP_FAILURE)
            {
                u1StatusFlag = FALSE;
                if (i4RowStatus != ACTIVE)
                {
                    u1StatusFlag = TRUE;
                }
                if (u1StatusFlag == FALSE)
                {
                    CliPrintf (CliHandle, "meter %d\r\n", u4Idx);
                    if (nmhGetFsQoSMeterType (u4Idx, &i4MeterType) !=
                        SNMP_FAILURE)
                    {
                        CliPrintf (CliHandle, " meter-type ");
                        if (i4MeterType == QOS_CLI_METER_TYPE_STB)
                        {
                            CliPrintf (CliHandle, "simpleTokenBucket ");
                        }
                        else if (i4MeterType == QOS_CLI_METER_TYPE_AVGRATE)
                        {
                            CliPrintf (CliHandle, "avgRate ");
                        }
                        else if (i4MeterType == QOS_CLI_METER_TYPE_SRTCM)
                        {
                            CliPrintf (CliHandle, "srTCM ");
                        }
                        else if (i4MeterType == QOS_CLI_METER_TYPE_TRTCM)
                        {
                            CliPrintf (CliHandle, "trTCM ");
                        }
                        else if (i4MeterType == QOS_CLI_METER_TYPE_TSWTCM)
                        {
                            CliPrintf (CliHandle, "tswTCM ");
                        }
                        else if (i4MeterType == QOS_CLI_METER_TYPE_MEFCOUPLED)
                        {
                            CliPrintf (CliHandle, "mefCoupled ");
                        }
                        else if (i4MeterType == QOS_CLI_METER_TYPE_MEFDECOUPLED)
                        {
                            CliPrintf (CliHandle, "mefDeCoupled ");
                        }
                    }

                    if (nmhGetFsQoSMeterColorMode (u4Idx, &i4ColorMode) !=
                        SNMP_FAILURE)
                    {
                        /* by default colormode will be colorblind,so no need 
                           to add in show running config */
                        if (i4ColorMode == QOS_CLI_COLOR_MODE_AWARE)
                        {
                            CliPrintf (CliHandle, "color-mode aware ");
                        }
                    }

                    if (nmhGetFsQoSMeterInterval (u4Idx, &u4Interval) !=
                        SNMP_FAILURE)
                    {
                        if (u4Interval != 0)
                        {
                            CliPrintf (CliHandle, "interval %ld ", u4Interval);
                        }
                    }
                    if (nmhGetFsQoSMeterCIR (u4Idx, &u4CIR) != SNMP_FAILURE)
                    {
                        if (u4CIR != 0)
                        {
                            CliPrintf (CliHandle, "cir %ld ", u4CIR);
                        }
                    }
                    if (nmhGetFsQoSMeterCBS (u4Idx, &u4CBS) != SNMP_FAILURE)
                    {
                        if (u4CBS != 0)
                        {
                            CliPrintf (CliHandle, "cbs %ld ", u4CBS);
                        }
                    }
                    if (nmhGetFsQoSMeterEIR (u4Idx, &u4EIR) != SNMP_FAILURE)
                    {
                        if (u4EIR != 0)
                        {
                            CliPrintf (CliHandle, "eir %ld ", u4EIR);
                        }
                    }
                    if (nmhGetFsQoSMeterEBS (u4Idx, &u4EBS) != SNMP_FAILURE)
                    {
                        if (u4EBS != 0)
                        {
                            CliPrintf (CliHandle, "ebs %ld ", u4EBS);
                        }
                    }
                    if (nmhGetFsQoSMeterNext (u4Idx, &u4NextMeter) !=
                        SNMP_FAILURE)
                    {
                        if (u4NextMeter != 0)
                        {
                            CliPrintf (CliHandle, "next-meter %ld ",
                                       u4NextMeter);
                        }
                    }
                    CliPrintf (CliHandle, "\r\n");
                }
                CliPrintf (CliHandle, "!\r\n");
            }
        }
        while ((nmhGetNextIndexFsQoSMeterTable (u4Idx, &u4Idx)
                == SNMP_SUCCESS));

    }
    return (CLI_SUCCESS);
}

/****************************************************************************
 *                                                                           
 *     FUNCTION NAME    : QoSPolicyMapShowRunningConfig                      
 *                                                                           
 *     DESCRIPTION      : This function displays current PolicyMap  
 *                        configurations    
 *                                                                           
 *     INPUT            : CliHandle - CliContext ID                             
 *                         
 *                                                                           
 *     OUTPUT           : None                                               
 *                                                                           
 *     RETURNS          : None                            
 *                                                                           
 *****************************************************************************/
INT4
QoSPolicyMapShowRunningConfig (tCliHandle CliHandle)
{
    INT4                i4RetStatus = SNMP_FAILURE;
    INT4                i4RowStatus = SNMP_FAILURE;
    INT4                i4PHBType = 0;
    INT4                i4EntryType = 0;
    UINT1               au1IfName[CFA_MAX_PORT_NAME_LENGTH];
    UINT4               u4Idx = 0;
    UINT4               u4MeterId = 0;
    UINT4               u4ConNClass = 0;
    UINT4               u4ExcNClass = 0;
    UINT4               u4VioNClass = 0;
    UINT4               u4IfIndex = 0;
    UINT4               u4Class = 0;
    UINT4               u4DefPHB = 0;
    UINT4               u4Flag = 0;
    tQoSPolicyMapNode  *pPlyMapNode;
    UINT1               u1Flag = FALSE;
    UINT1               u1StatusFlag = FALSE;
    UINT4               u4CurDEI = (UINT4) CLI_RS_INVALID_MASK;

    if (nmhGetFirstIndexFsQoSPolicyMapTable (&u4Idx) != SNMP_FAILURE)
    {
        do
        {
            u1Flag = FALSE;
            pPlyMapNode = QoSUtlGetPolicyMapNode (u4Idx);
            if (pPlyMapNode != NULL)
            {
                if (QoSIsDefPolicyMapTblEntry (pPlyMapNode) == QOS_SUCCESS)
                {
                    u1Flag = TRUE;
                }
                if (QosUtlGetPlyMapEntryType (u4Idx, &i4EntryType)
                    == QOS_SUCCESS)
                {
                    if (i4EntryType == QOS_PCP_ENTRY_TYPE_IMPLICIT)
                    {
                        u1Flag = TRUE;
                    }
                }

            }
            if (u1Flag == FALSE)
            {
                if (nmhGetFsQoSPolicyMapStatus (u4Idx, &i4RowStatus) !=
                    SNMP_FAILURE)
                {
                    u1StatusFlag = FALSE;
                    CliPrintf (CliHandle, "policy-map %d\r\n", u4Idx);
                    if (i4RowStatus != ACTIVE)
                    {
                        u1StatusFlag = TRUE;
                    }
                    if (u1StatusFlag == FALSE)
                    {
                        /* To show the Meter and its Actions */
                        u4Flag = 0;    /*To check Whether meter actions configured
                                       for the particular Policy map. */
                        if (nmhGetFsQoSPolicyMapMeterTableId (u4Idx, &u4MeterId)
                            != SNMP_FAILURE)
                        {
                            if (u4MeterId != 0)
                            {
                                u4Flag = 1;
                                CliPrintf (CliHandle, " set meter %d ",
                                           u4MeterId);
                                /* To show the InProfile Conform Actions */
                                i4RetStatus =
                                    QoSCliUtlGetPlyMapInProConfActShowRunConfig
                                    (CliHandle, u4Idx);
                                if (i4RetStatus == CLI_FAILURE)
                                {
                                    return (CLI_FAILURE);
                                }
                                /* To show the InProfile Exceed Actions */
                                i4RetStatus =
                                    QoSCliUtlGetPlyMapInProExcActShowRunConfig
                                    (CliHandle, u4Idx);
                                if (i4RetStatus == CLI_FAILURE)
                                {
                                    return (CLI_FAILURE);
                                }
                                /* To show the OutProfile Violate Actions */
                                i4RetStatus =
                                    QoSCliUtlGetPlyMapOutProActShowRunConfig
                                    (CliHandle, u4Idx);
                                if (i4RetStatus == CLI_FAILURE)
                                {
                                    return (CLI_FAILURE);
                                }
                                if (nmhGetFsQoSPolicyMapConformActionSetNewCLASS
                                    (u4Idx, &u4ConNClass) != SNMP_FAILURE)
                                {
                                    if (u4ConNClass != 0)
                                    {
                                        CliPrintf (CliHandle,
                                                   "set-conform-newclass %d ",
                                                   u4ConNClass);
                                    }
                                }

                                if (nmhGetFsQoSPolicyMapExceedActionSetNewCLASS
                                    (u4Idx, &u4ExcNClass) != SNMP_FAILURE)
                                {
                                    if (u4ExcNClass != 0)
                                    {
                                        CliPrintf (CliHandle,
                                                   "set-exceed-newclass %d ",
                                                   u4ExcNClass);
                                    }
                                }

                                if (nmhGetFsQoSPolicyMapOutProfileActionSetNewCLASS (u4Idx, &u4VioNClass) != SNMP_FAILURE)
                                {
                                    if (u4VioNClass != 0)
                                    {
                                        CliPrintf (CliHandle,
                                                   "set-violate-newclass %d ",
                                                   u4VioNClass);
                                    }
                                }
                                if (u4Flag == 1)
                                {
                                    CliPrintf (CliHandle, "\n");
                                }
                            }
                        }
                        /* To show the policy configurations */
                        CliPrintf (CliHandle, " set policy ");
                        if (nmhGetFsQoSPolicyMapCLASS (u4Idx, &u4Class) !=
                            SNMP_FAILURE)
                        {
                            if (u4Class != 0)
                            {
                                CliPrintf (CliHandle, "class %d ", u4Class);
                            }
                        }

                        if (nmhGetFsQoSPolicyMapIfIndex (u4Idx, &u4IfIndex) !=
                            SNMP_FAILURE)
                        {
                            if (u4IfIndex != 0)
                            {
                                MEMSET (au1IfName, 0, CFA_MAX_PORT_NAME_LENGTH);
                                if (CfaCliConfGetIfName
                                    (u4IfIndex,
                                     (INT1 *) au1IfName) == CLI_FAILURE)
                                {
                                    return (CLI_FAILURE);
                                }
                                CliPrintf (CliHandle, "interface %s ",
                                           au1IfName);

                            }
                        }
                        if (nmhGetFsQoSPolicyMapPHBType (u4Idx, &i4PHBType) !=
                            SNMP_FAILURE)
                        {
                            CliPrintf (CliHandle, "default-priority-type ");
                            nmhGetFsQoSPolicyDefaultVlanDE (u4Idx,
                                                            (INT4
                                                             *) (&u4CurDEI));
                            if (nmhGetFsQoSPolicyMapDefaultPHB
                                (u4Idx, &u4DefPHB) != SNMP_FAILURE)
                            {
                                switch (i4PHBType)
                                {
                                    case QOS_CLI_PLY_PHB_TYPE_VLAN_PRI:
                                        CliPrintf (CliHandle, "vlanPri %ld ",
                                                   u4DefPHB);
                                        break;

                                    case QOS_CLI_PLY_PHB_TYPE_IP_TOS:
                                        CliPrintf (CliHandle, "ipTos %ld ",
                                                   u4DefPHB);
                                        break;

                                    case QOS_CLI_PLY_PHB_TYPE_IP_DSCP:
                                        CliPrintf (CliHandle, "ipDscp %ld ",
                                                   u4DefPHB);
                                        break;

                                    case QOS_CLI_PLY_PHB_TYPE_MPLS_EXP:
                                        CliPrintf (CliHandle, "mplsExp %ld ",
                                                   u4DefPHB);
                                        break;

                                    case QOS_CLI_PLY_PHB_TYPE_NONE:
                                        CliPrintf (CliHandle, "none ");
                                        break;
                                    case QOS_CLI_PLY_PHB_TYPE_DOT1P:
                                        CliPrintf (CliHandle, "dot1P %1d %1d",
                                                   u4DefPHB, u4CurDEI);
                                        break;

                                    default:
                                        break;
                                }
                            }
                        }

                        CliPrintf (CliHandle, "\r\n");
                    }
                    CliPrintf (CliHandle, "!\r\n");
                }
            }
        }
        while ((nmhGetNextIndexFsQoSPolicyMapTable (u4Idx, &u4Idx)
                == SNMP_SUCCESS));

    }
    return (CLI_SUCCESS);
}

/****************************************************************************
 *
 *     FUNCTION NAME    : QoSPortTableShowRunningConfig                 
 *                                                                           
 *     DESCRIPTION      : This function displays current PcP Selection Row  
 *                        configurations    
 *                                                                           
 *     INPUT            : CliHandle - CliContext ID                             
 *                         
 *                                                                           
 *     OUTPUT           : None                                               
 *                                                                           
 *     RETURNS          : None                            
 *                                                                           
 *****************************************************************************/
INT4
QoSPortTableShowRunningConfig (tCliHandle CliHandle)
{
    INT4                i4IfIndex = 0;
    INT4                i4FsQoSPcpPacketType = 0;
    INT4                i4FsQoSPcpSelRow = 0;
    UINT1               au1IfName[CFA_MAX_PORT_NAME_LENGTH];
    UINT1               u1Flag = FALSE;

    if (nmhGetFirstIndexFsQoSPortTable (&i4IfIndex, &i4FsQoSPcpPacketType)
        != SNMP_FAILURE)
    {
        do
        {
            if (nmhGetFsQoSPcpSelRow (i4IfIndex, i4FsQoSPcpPacketType,
                                      &i4FsQoSPcpSelRow) != SNMP_FAILURE)
            {
                if (i4FsQoSPcpPacketType == QOS_CLI_ETHERNET_PKT
                    && i4FsQoSPcpSelRow == QOS_CLI_8P0D_SEL_ROW)
                {
                    u1Flag = TRUE;
                }
                if (u1Flag == FALSE)
                {
                    MEMSET (au1IfName, 0, CFA_MAX_PORT_NAME_LENGTH);
                    CfaCliConfGetIfName ((UINT4) i4IfIndex, (INT1 *) au1IfName);
                    CliPrintf (CliHandle, "interface %s \r\n", au1IfName);
                    switch (i4FsQoSPcpPacketType)
                    {
                        case QOS_CLI_ETHERNET_PKT:
                            CliPrintf (CliHandle, "qos packet-type Ethernet");
                            break;
                        case QOS_CLI_IP_PKT:
                            CliPrintf (CliHandle, "qos packet-type IP");
                            break;
                        case QOS_CLI_MPLS_PKT:
                            CliPrintf (CliHandle, "qos packet-type MPLS");
                            break;
                        default:
                            break;
                    }
                    switch (i4FsQoSPcpSelRow)
                    {
                        case QOS_CLI_8P0D_SEL_ROW:
                            CliPrintf (CliHandle,
                                       " pcp-selection-row 8P0D \r\n");
                            break;
                        case QOS_CLI_7P1D_SEL_ROW:
                            CliPrintf (CliHandle,
                                       " pcp-selection-row 7P1D \r\n");
                            break;
                        case QOS_CLI_6P2D_SEL_ROW:
                            CliPrintf (CliHandle,
                                       " pcp-selection-row 6P2D \r\n");
                            break;
                        case QOS_CLI_5P3D_SEL_ROW:
                            CliPrintf (CliHandle,
                                       " pcp-selection-row 5P3D \r\n");
                            break;
                        default:
                            break;
                    }

                    CliPrintf (CliHandle, "!\r\n");
                }
            }
        }
        while (nmhGetNextIndexFsQoSPortTable (i4IfIndex,
                                              &i4IfIndex,
                                              i4FsQoSPcpPacketType,
                                              &i4FsQoSPcpPacketType) ==
               SNMP_SUCCESS);

    }
    return (CLI_SUCCESS);

}

/****************************************************************************
 *
 *     FUNCTION NAME    : QoSPolicyMapStatsShowRunningConfig
 *
 *     DESCRIPTION      : This function displays current PolicyMap
 *                        configurations
 *
 *     INPUT            : CliHandle - CliContext ID
 *
 *
 *     OUTPUT           : None
 *
 *     RETURNS          : None
 *
 *****************************************************************************/
INT4
QoSPolicyMapStatsShowRunningConfig (tCliHandle CliHandle)
{
    INT4                i4MeterStatsStatus = 0;
    UINT4               u4Idx = 0;

    if (nmhGetFirstIndexFsQoSPolicerStatsTable (&u4Idx) != SNMP_FAILURE)
    {
        do
        {
            if ((nmhGetFsQoSPolicerStatsStatus (u4Idx, &i4MeterStatsStatus)
                 == SNMP_SUCCESS))
            {
                if (i4MeterStatsStatus == QOS_STATS_ENABLE)
                {
                    CliPrintf (CliHandle,
                               "set meter-stats enable meter-id %d\r\n", u4Idx);
                }
            }

        }
        while ((nmhGetNextIndexFsQoSPolicerStatsTable (u4Idx, &u4Idx)
                == SNMP_SUCCESS));
        CliPrintf (CliHandle, "!\r\n");
    }
    return (CLI_SUCCESS);
}

/****************************************************************************
 *                                                                           
 *     FUNCTION NAME    : QoSCliUtlGetPlyMapInProConfActShowRunConfig            *        
 *                                                                           
 *     DESCRIPTION      : This function displays current PolicyMap  
 *                        InProfile-Conform  configurations    
 *                                                                           
 *     INPUT            : CliHandle - CliContext ID   
 *                        u4Idx-PriorityMap Id
 *                          
 *                                                                           
 *     OUTPUT           : None                                               
 *                                                                           
 *     RETURNS          : None                            
 *                                                                           
 *****************************************************************************/
INT4
QoSCliUtlGetPlyMapInProConfActShowRunConfig (tCliHandle CliHandle, UINT4 u4Idx)
{
    tSNMP_OCTET_STRING_TYPE *pOctStrFlag = NULL;
    UINT4               u4GetVal = 0;
    INT4                i4GetVal = 0;
    UINT1               u1InActFlag = 0;
    UINT2               u2ActMask = 0x1;
    UINT1               au1IfName[CFA_MAX_PORT_NAME_LENGTH];
    INT1                i1RetVal = 0;
    pOctStrFlag = allocmem_octetstring (QOS_POLICY_ACTION_FLAG_LENGTH);
    if (pOctStrFlag == NULL)
    {
        return (CLI_FAILURE);
    }

    MEMSET (pOctStrFlag->pu1_OctetList, 0, pOctStrFlag->i4_Length);

    if (nmhGetFsQoSPolicyMapInProfileConformActionFlag (u4Idx, pOctStrFlag)
        != SNMP_FAILURE)
    {
        /* Get the Action Flag */
        MEMCPY (&u1InActFlag, pOctStrFlag->pu1_OctetList,
                pOctStrFlag->i4_Length);
        if (u1InActFlag == 0x00)
        {
            free_octetstring (pOctStrFlag);
            return (CLI_SUCCESS);
        }
        CliPrintf (CliHandle, "conform-action ");
        while (u2ActMask < 0xFF)
        {
            switch ((u1InActFlag & u2ActMask))
            {
                case QOS_PLY_IN_PROF_DROP:
                    CliPrintf (CliHandle, "Drop.");
                    break;

                case QOS_PLY_IN_PROF_CONF_IP_TOS:

                    if (nmhGetFsQoSPolicyMapConformActionSetIpTOS (u4Idx,
                                                                   &u4GetVal) !=
                        SNMP_FAILURE)
                    {
                        CliPrintf (CliHandle, "set-ip-prec-transmit %ld",
                                   u4GetVal);
                    }

                    break;

                case QOS_PLY_IN_PROF_CONF_IP_DSCP:

                    if (nmhGetFsQoSPolicyMapConformActionSetDscp
                        (u4Idx, &i4GetVal) != SNMP_FAILURE)
                    {
                        CliPrintf (CliHandle, "set-ip-dscp-transmit %ld ",
                                   i4GetVal);
                    }
                    break;

                case QOS_PLY_IN_PROF_PORT:

                    if (nmhGetFsQoSPolicyMapInProfileActionSetPort
                        (u4Idx, (UINT4 *) &i4GetVal) != SNMP_FAILURE)
                    {
                        if (u4Idx != 0)
                        {
                            MEMSET (au1IfName, 0, CFA_MAX_PORT_NAME_LENGTH);

                            if (CfaCliConfGetIfName (i4GetVal,
                                                     (INT1 *) au1IfName)
                                == CLI_FAILURE)
                            {
                                free_octetstring (pOctStrFlag);
                                return (CLI_FAILURE);
                            }

                            CliPrintf (CliHandle, "set-port %s ", au1IfName);
                        }
                    }
                    break;

                case QOS_PLY_IN_PROF_CONF_INNER_VLAN_PRI:
                    i1RetVal =
                        nmhGetFsQoSPolicyMapConformActionSetInnerVlanDE (u4Idx,
                                                                         &u4GetVal);
                    if (i1RetVal == SNMP_FAILURE)
                    {
                        /* do nothing - this check is for avoiding coverity warning
                         * u4Getval parameter will be used for our requirement.
                         */
                    }
                    if (u4GetVal != 0)
                    {
                        if (nmhGetFsQoSPolicyMapConformActionSetInnerVlanPrio
                            (u4Idx, &u4GetVal) != SNMP_FAILURE)
                        {
                            CliPrintf (CliHandle, "set-inner-vlan-pri %ld ",
                                       u4GetVal);
                        }
                    }
                    else
                    {
                        if (nmhGetFsQoSPolicyMapConformActionSetInnerVlanPrio
                            (u4Idx, &u4GetVal) != SNMP_FAILURE)
                        {
                            CliPrintf (CliHandle, "inner-vlan-pri-set %ld ",
                                       u4GetVal);
                        }
                    }
                    break;

                case QOS_PLY_IN_PROF_CONF_VLAN_PRI:
                    i1RetVal =
                        nmhGetFsQoSPolicyMapConformActionSetVlanDE (u4Idx,
                                                                    &u4GetVal);
                    if (i1RetVal == SNMP_FAILURE)
                    {
                        /* do nothing - this check is for avoiding coverity warning
                         * u4Getval parameter will be used for our requirement.
                         */
                    }
                    if (u4GetVal != 0)
                    {
                        if (nmhGetFsQoSPolicyMapConformActionSetVlanPrio
                            (u4Idx, &u4GetVal) != SNMP_FAILURE)
                        {
                            CliPrintf (CliHandle, "set-cos-transmit %ld ",
                                       u4GetVal);
                        }
                    }
                    else
                    {
                        if (nmhGetFsQoSPolicyMapConformActionSetVlanPrio
                            (u4Idx, &u4GetVal) != SNMP_FAILURE)
                        {
                            CliPrintf (CliHandle, "cos-transmit-set %ld ",
                                       u4GetVal);
                        }
                    }
                    break;

                case QOS_PLY_IN_PROF_CONF_VLAN_DE:
                    i1RetVal =
                        nmhGetFsQoSPolicyMapConformActionSetVlanPrio (u4Idx,
                                                                      &u4GetVal);
                    if (i1RetVal == SNMP_FAILURE)
                    {
                        /* do nothing - this check is for avoiding coverity warning
                         * u4Getval parameter will be used for our requirement.
                         */
                    }
                    if (u4GetVal != 0)
                    {
                        if (nmhGetFsQoSPolicyMapConformActionSetVlanDE (u4Idx,
                                                                        &u4GetVal)
                            != SNMP_FAILURE)
                        {
                            CliPrintf (CliHandle, "set-de-transmit %ld ",
                                       u4GetVal);
                        }
                    }
                    else
                    {
                        if (nmhGetFsQoSPolicyMapConformActionSetVlanDE (u4Idx,
                                                                        &u4GetVal)
                            != SNMP_FAILURE)
                        {
                            CliPrintf (CliHandle, "de-transmit-set %ld ",
                                       u4GetVal);
                        }
                    }
                    break;

                case QOS_PLY_IN_PROF_CONF_MPLS_EXP:

                    if (nmhGetFsQoSPolicyMapConformActionSetMplsExp (u4Idx,
                                                                     &u4GetVal)
                        != SNMP_FAILURE)
                    {
                        CliPrintf (CliHandle, "set-mpls-exp-transmit %ld ",
                                   u4GetVal);
                    }
                    break;

                case QOS_PLY_IN_PROF_CONF_INNER_VLAN_DE:
                    i1RetVal =
                        nmhGetFsQoSPolicyMapConformActionSetInnerVlanPrio
                        (u4Idx, &u4GetVal);
                    if (i1RetVal == SNMP_FAILURE)
                    {
                        /* do nothing - this check is for avoiding coverity warning
                         * u4Getval parameter will be used for our requirement.
                         */
                    }

                    if (u4GetVal != 0)
                    {
                        if (nmhGetFsQoSPolicyMapConformActionSetInnerVlanDE
                            (u4Idx, &u4GetVal) != SNMP_FAILURE)
                        {
                            CliPrintf (CliHandle, "set-inner-vlan-de %ld ",
                                       u4GetVal);
                        }
                    }
                    else
                    {
                        if (nmhGetFsQoSPolicyMapConformActionSetInnerVlanDE
                            (u4Idx, &u4GetVal) != SNMP_FAILURE)
                        {
                            CliPrintf (CliHandle, "inner-vlan-de-set %ld ",
                                       u4GetVal);
                        }
                    }
                    break;

                default:
                    break;

            }                    /* End of Switch */

            u2ActMask = (UINT2) (u2ActMask << 1);

        }                        /* End of While */

    }
    free_octetstring (pOctStrFlag);
    return (CLI_SUCCESS);
}

/****************************************************************************
 *                                                                           
 *     FUNCTION NAME    : QoSCliUtlGetPlyMapInProExcActShowRunConfig             *        
 *                                                                           
 *     DESCRIPTION      : This function displays current PolicyMap  
 *                        InProfile-Exceed  configurations    
 *                                                                           
 *     INPUT            : CliHandle - CliContext ID                             
 *                        u4Idx-PriorityMap Id  
 *                                                                           
 *     OUTPUT           : None                                               
 *                                                                           
 *     RETURNS          : None                            
 *                                                                           
 *****************************************************************************/
INT4
QoSCliUtlGetPlyMapInProExcActShowRunConfig (tCliHandle CliHandle, UINT4 u4Idx)
{
    tSNMP_OCTET_STRING_TYPE *pOctStrFlag = NULL;
    UINT4               u4GetVal = 0;
    INT4                i4GetVal = 0;
    UINT1               u1InActFlag = 0;
    UINT2               u2ActMask = 0x1;
    INT1                i1RetVal = 0;
    /* OutProfile Actions */
    /* Allocate Memory For Name */
    pOctStrFlag = allocmem_octetstring (QOS_POLICY_ACTION_FLAG_LENGTH);
    if (pOctStrFlag == NULL)
    {
        return (CLI_FAILURE);
    }

    MEMSET (pOctStrFlag->pu1_OctetList, 0, pOctStrFlag->i4_Length);

    if (nmhGetFsQoSPolicyMapInProfileExceedActionFlag (u4Idx, pOctStrFlag)
        != SNMP_FAILURE)
    {
        /* Get the Action Flag */
        MEMCPY (&u1InActFlag, pOctStrFlag->pu1_OctetList,
                pOctStrFlag->i4_Length);
        if (u1InActFlag == 0x00)
        {
            free_octetstring (pOctStrFlag);
            return (CLI_SUCCESS);
        }
        CliPrintf (CliHandle, "exceed-action ");

        while (u2ActMask < 0xFF)
        {
            switch ((u1InActFlag & u2ActMask))
            {
                case QOS_PLY_IN_PROF_EXC_DROP:
                    CliPrintf (CliHandle, "drop ");
                    break;

                case QOS_PLY_IN_PROF_EXC_IP_TOS:

                    if (nmhGetFsQoSPolicyMapExceedActionSetIpTOS (u4Idx,
                                                                  &u4GetVal) !=
                        SNMP_FAILURE)
                    {
                        CliPrintf (CliHandle, "set-ip-prec-transmit %ld ",
                                   u4GetVal);
                    }

                    break;

                case QOS_PLY_IN_PROF_EXC_IP_DSCP:

                    if (nmhGetFsQoSPolicyMapExceedActionSetDscp (u4Idx,
                                                                 &i4GetVal) !=
                        SNMP_FAILURE)
                    {
                        CliPrintf (CliHandle, "set-ip-dscp-transmit %ld ",
                                   i4GetVal);
                    }

                    break;

                case QOS_PLY_IN_PROF_EXC_INNER_VLAN_PRI:
                    i1RetVal =
                        nmhGetFsQoSPolicyMapExceedActionSetInnerVlanDE (u4Idx,
                                                                        &u4GetVal);
                    if (i1RetVal == SNMP_FAILURE)
                    {
                        /* do nothing - this check is for avoiding coverity warning
                         * u4Getval parameter will be used for our requirement.
                         */
                    }
                    if (u4GetVal != 0)
                    {
                        if (nmhGetFsQoSPolicyMapExceedActionSetInnerVlanPrio
                            (u4Idx, &u4GetVal) != SNMP_FAILURE)
                        {
                            CliPrintf (CliHandle, "set-inner-vlan-pri %ld ",
                                       u4GetVal);
                        }
                    }
                    else
                    {
                        if (nmhGetFsQoSPolicyMapExceedActionSetInnerVlanPrio
                            (u4Idx, &u4GetVal) != SNMP_FAILURE)
                        {
                            CliPrintf (CliHandle, "inner-vlan-pri-set %ld ",
                                       u4GetVal);
                        }
                    }
                    break;

                case QOS_PLY_IN_PROF_EXC_VLAN_PRI:
                    i1RetVal =
                        nmhGetFsQoSPolicyMapExceedActionSetVlanDE (u4Idx,
                                                                   &u4GetVal);
                    if (i1RetVal == SNMP_FAILURE)
                    {
                        /* do nothing - this check is for avoiding coverity warning
                         * u4Getval parameter will be used for our requirement.
                         */
                    }
                    if (u4GetVal != 0)
                    {
                        if (nmhGetFsQoSPolicyMapExceedActionSetVlanPrio (u4Idx,
                                                                         &u4GetVal)
                            != SNMP_FAILURE)
                        {
                            CliPrintf (CliHandle, "set-cos-transmit %ld ",
                                       u4GetVal);
                        }
                    }
                    else
                    {
                        if (nmhGetFsQoSPolicyMapExceedActionSetVlanPrio (u4Idx,
                                                                         &u4GetVal)
                            != SNMP_FAILURE)
                        {
                            CliPrintf (CliHandle, "cos-transmit-set %ld ",
                                       u4GetVal);
                        }
                    }
                    break;

                case QOS_PLY_IN_PROF_EXC_VLAN_DE:
                    i1RetVal =
                        nmhGetFsQoSPolicyMapExceedActionSetVlanPrio (u4Idx,
                                                                     &u4GetVal);
                    if (u4GetVal != 0)
                    {
                        if (nmhGetFsQoSPolicyMapExceedActionSetVlanDE (u4Idx,
                                                                       &u4GetVal)
                            != SNMP_FAILURE)
                        {
                            CliPrintf (CliHandle, "set-de-transmit %ld ",
                                       u4GetVal);
                        }
                    }
                    else
                    {
                        if (nmhGetFsQoSPolicyMapExceedActionSetVlanDE (u4Idx,
                                                                       &u4GetVal)
                            != SNMP_FAILURE)
                        {
                            CliPrintf (CliHandle, "de-transmit-set %ld ",
                                       u4GetVal);
                        }
                    }
                    break;

                case QOS_PLY_IN_PROF_EXC_MPLS_EXP:

                    if (nmhGetFsQoSPolicyMapExceedActionSetMplsExp (u4Idx,
                                                                    &u4GetVal)
                        != SNMP_FAILURE)
                    {
                        CliPrintf (CliHandle, "set-mpls-exp-transmit %ld ",
                                   u4GetVal);
                    }

                    break;

                case QOS_PLY_IN_PROF_EXC_INNER_VLAN_DE:
                    i1RetVal =
                        nmhGetFsQoSPolicyMapExceedActionSetInnerVlanPrio (u4Idx,
                                                                          &u4GetVal);
                    if (i1RetVal == SNMP_FAILURE)
                    {
                        /* do nothing - this check is for avoiding coverity warning
                         * u4Getval parameter will be used for our requirement.
                         */
                    }
                    if (u4GetVal != 0)
                    {
                        if (nmhGetFsQoSPolicyMapExceedActionSetInnerVlanDE
                            (u4Idx, &u4GetVal) != SNMP_FAILURE)
                        {
                            CliPrintf (CliHandle, "set-inner-vlan-de %ld ",
                                       u4GetVal);
                        }
                    }
                    else
                    {
                        if (nmhGetFsQoSPolicyMapExceedActionSetInnerVlanDE
                            (u4Idx, &u4GetVal) != SNMP_FAILURE)
                        {
                            CliPrintf (CliHandle, "inner-vlan-de-set %ld ",
                                       u4GetVal);
                        }
                    }
                    break;

                default:
                    break;

            }                    /* End of Switch */

            u2ActMask = (UINT2) (u2ActMask << 1);
        }
    }
    free_octetstring (pOctStrFlag);
    return (CLI_SUCCESS);
}

/****************************************************************************
 *                                                                           
 *     FUNCTION NAME    : QoSCliUtlGetPlyMapOutProActShowRunConfig               *        
 *                                                                           
 *     DESCRIPTION      : This function displays current PolicyMap
 *     
 *                        OutProfile-Violate  configurations    
 *                                                                           
 *     INPUT            : CliHandle - CliContext ID                             
 *                        u4Idx-PriorityMap Id  
 *                                                                           
 *     OUTPUT           : None                                               
 *                                                                           
 *     RETURNS          : None                            
 *                                                                           
 *****************************************************************************/
INT4
QoSCliUtlGetPlyMapOutProActShowRunConfig (tCliHandle CliHandle, UINT4 u4Idx)
{
    tSNMP_OCTET_STRING_TYPE *pOctStrFlag = NULL;
    UINT4               u4GetVal = 0;
    INT4                i4GetVal = 0;
    UINT1               u1OutActFlag = 0;
    UINT2               u2ActMask = 0x1;
    INT1                i1RetVal = 0;

    /* OutProfile Actions */
    /* Allocate Memory For Name */
    pOctStrFlag = allocmem_octetstring (QOS_POLICY_ACTION_FLAG_LENGTH);
    if (pOctStrFlag == NULL)
    {
        return (CLI_FAILURE);
    }

    MEMSET (pOctStrFlag->pu1_OctetList, 0, pOctStrFlag->i4_Length);

    if (nmhGetFsQoSPolicyMapOutProfileActionFlag (u4Idx, pOctStrFlag)
        != SNMP_FAILURE)
    {
        /* Get the Action Flag */
        MEMCPY (&u1OutActFlag, pOctStrFlag->pu1_OctetList,
                pOctStrFlag->i4_Length);
        if (u1OutActFlag == 0x00)
        {
            free_octetstring (pOctStrFlag);
            return (CLI_SUCCESS);
        }
        CliPrintf (CliHandle, "violate-action ");
        while (u2ActMask < 0xFF)
        {
            switch ((u1OutActFlag & u2ActMask))
            {
                case QOS_PLY_OUT_PROF_VIO_DROP:
                    CliPrintf (CliHandle, "drop ");
                    break;

                case QOS_PLY_OUT_PROF_VIO_IP_TOS:

                    if (nmhGetFsQoSPolicyMapOutProfileActionSetIPTOS (u4Idx,
                                                                      &u4GetVal)
                        != SNMP_FAILURE)
                    {
                        CliPrintf (CliHandle, "set-ip-prec-transmit %ld ",
                                   u4GetVal);
                    }

                    break;

                case QOS_PLY_OUT_PROF_VIO_IP_DSCP:

                    if (nmhGetFsQoSPolicyMapOutProfileActionSetDscp (u4Idx,
                                                                     &i4GetVal)
                        != SNMP_FAILURE)
                    {
                        CliPrintf (CliHandle, "set-ip-dscp-transmit %ld ",
                                   i4GetVal);
                    }

                    break;

                case QOS_PLY_OUT_PROF_VIO_INNER_VLAN_PRI:
                    i1RetVal =
                        nmhGetFsQoSPolicyMapOutProfileActionSetInnerVlanDE
                        (u4Idx, &u4GetVal);
                    if (i1RetVal == SNMP_FAILURE)
                    {
                        /* do nothing - this check is for avoiding coverity warning
                         * u4Getval parameter will be used for our requirement.
                         */
                    }
                    if (u4GetVal != 0)
                    {
                        if (nmhGetFsQoSPolicyMapOutProfileActionSetInnerVlanPrio
                            (u4Idx, &u4GetVal) != SNMP_FAILURE)
                        {
                            CliPrintf (CliHandle, "set-inner-vlan-pri %ld ",
                                       u4GetVal);
                        }
                    }
                    else
                    {
                        if (nmhGetFsQoSPolicyMapOutProfileActionSetInnerVlanPrio
                            (u4Idx, &u4GetVal) != SNMP_FAILURE)
                        {
                            CliPrintf (CliHandle, "inner-vlan-pri-set %ld ",
                                       u4GetVal);
                        }
                    }
                    break;

                case QOS_PLY_OUT_PROF_VIO_VLAN_PRI:
                    i1RetVal =
                        nmhGetFsQoSPolicyMapOutProfileActionSetVlanDE (u4Idx,
                                                                       &u4GetVal);
                    if (i1RetVal == SNMP_FAILURE)
                    {
                        /* do nothing - this check is for avoiding coverity warning
                         * u4Getval parameter will be used for our requirement.
                         */
                    }
                    if (u4GetVal != 0)
                    {
                        if (nmhGetFsQoSPolicyMapOutProfileActionSetVlanPrio
                            (u4Idx, &u4GetVal) != SNMP_FAILURE)
                        {
                            CliPrintf (CliHandle, "set-cos-transmit %ld ",
                                       u4GetVal);
                        }
                    }
                    else
                    {
                        if (nmhGetFsQoSPolicyMapOutProfileActionSetVlanPrio
                            (u4Idx, &u4GetVal) != SNMP_FAILURE)
                        {
                            CliPrintf (CliHandle, "cos-transmit-set %ld ",
                                       u4GetVal);
                        }
                    }
                    break;

                case QOS_PLY_OUT_PROF_VIO_VLAN_DE:

                    if (nmhGetFsQoSPolicyMapOutProfileActionSetVlanDE (u4Idx,
                                                                       &u4GetVal)
                        != SNMP_FAILURE)
                    {
                        CliPrintf (CliHandle, "set-de-transmit %ld ", u4GetVal);
                    }

                    break;

                case QOS_PLY_OUT_PROF_VIO_MPLS_EXP:

                    if (nmhGetFsQoSPolicyMapOutProfileActionSetMplsExp (u4Idx,
                                                                        &u4GetVal)
                        != SNMP_FAILURE)
                    {
                        CliPrintf (CliHandle, "set-mpls-exp-transmit %ld ",
                                   u4GetVal);
                    }

                    break;

                case QOS_PLY_OUT_PROF_VIO_INNER_VLAN_DE:
                    i1RetVal =
                        nmhGetFsQoSPolicyMapOutProfileActionSetInnerVlanPrio
                        (u4Idx, &u4GetVal);
                    if (i1RetVal == SNMP_FAILURE)
                    {
                        /* do nothing - this check is for avoiding coverity warning
                         * u4Getval parameter will be used for our requirement.
                         */
                    }
                    if (u4GetVal != 0)
                    {
                        if (nmhGetFsQoSPolicyMapOutProfileActionSetInnerVlanDE
                            (u4Idx, &u4GetVal) != SNMP_FAILURE)
                        {
                            CliPrintf (CliHandle, "set-inner-vlan-de %ld ",
                                       u4GetVal);
                        }
                    }
                    else
                    {
                        if (nmhGetFsQoSPolicyMapOutProfileActionSetInnerVlanDE
                            (u4Idx, &u4GetVal) != SNMP_FAILURE)
                        {
                            CliPrintf (CliHandle, "inner-vlan-de-set %ld ",
                                       u4GetVal);
                        }
                    }
                    break;

                default:
                    break;

            }                    /* End of Switch */

            u2ActMask = (UINT2) (u2ActMask << 1);

        }
        /* End of While */
    }
    free_octetstring (pOctStrFlag);
    return (CLI_SUCCESS);
}

/****************************************************************************
 *                                                                           
 *     FUNCTION NAME    : QoSQueueTemplateShowRunningConfig                     
 *                                                                           
 *     DESCRIPTION      : This function displays current QueueTempalte  
 *                        configurations    
 *                                                                           
 *     INPUT            : CliHandle - CliContext ID                             
 *                         
 *                                                                           
 *     OUTPUT           : None                                               
 *                                                                           
 *     RETURNS          : None                            
 *                                                                           
 *****************************************************************************/
INT4
QoSQueueTemplateShowRunningConfig (tCliHandle CliHandle)
{
    INT4                i4DropType = 0;
    INT4                i4DropAlgoEnable = 0;
    INT4                i4RetStatus = SNMP_FAILURE;
    INT4                i4RowStatus = SNMP_FAILURE;
    UINT4               u4Idx = 0;
    UINT4               u4QTempSize = 0;
    tQoSQTemplateNode  *pQTempNode = NULL;
    UINT1               u1Flag = FALSE;
    UINT1               u1StatusFlag = FALSE;

    if (nmhGetFirstIndexFsQoSQTemplateTable (&u4Idx) != SNMP_FAILURE)
    {
        do
        {
            u1Flag = FALSE;
            pQTempNode = QoSUtlGetQTempNode (u4Idx);
            if (pQTempNode != NULL)
            {
                if (QoSIsDefQTempTblEntry (pQTempNode) == QOS_SUCCESS)
                {
                    u1Flag = TRUE;
                }
            }
            if (u1Flag == FALSE)
            {
                if (nmhGetFsQoSQTemplateStatus (u4Idx, &i4RowStatus) !=
                    SNMP_FAILURE)
                {
                    u1StatusFlag = FALSE;
                    CliPrintf (CliHandle, "queue-type %d\r\n", u4Idx);
                    if (i4RowStatus != ACTIVE)
                    {
                        u1StatusFlag = TRUE;
                    }
                    if (u1StatusFlag == FALSE)
                    {
                        if (nmhGetFsQoSQTemplateDropType (u4Idx, &i4DropType)
                            != SNMP_FAILURE)
                        {
                            CliPrintf (CliHandle, " set ");
                            CliPrintf (CliHandle, "algo-type ");

                            switch (i4DropType)
                            {
                                case QOS_Q_TEMP_DROP_TYPE_TAIL:
                                    CliPrintf (CliHandle, "tailDrop ");
                                    break;

                                case QOS_Q_TEMP_DROP_TYPE_HEAD:
                                    CliPrintf (CliHandle, "headDrop ");
                                    break;

                                case QOS_Q_TEMP_DROP_TYPE_RED:
                                    CliPrintf (CliHandle, "red ");
                                    break;

                                case QOS_Q_TEMP_DROP_TYPE_WRED:
                                    CliPrintf (CliHandle, "wred ");
                                    break;

                                default:
                                    break;
                            }

                        }
                        if (nmhGetFsQoSQTemplateSize (u4Idx, &u4QTempSize) !=
                            SNMP_FAILURE)
                        {
                            if (u4QTempSize != QOS_Q_TEMP_SIZE_DEFAULT)
                            {
                                CliPrintf (CliHandle, "queue-limit %ld ",
                                           u4QTempSize);
                            }
                        }
                        if (nmhGetFsQoSQTemplateDropAlgoEnableFlag (u4Idx,
                                                                    &i4DropAlgoEnable)
                            != SNMP_FAILURE)
                        {
                            if (i4DropAlgoEnable !=
                                QOS_Q_TEMP_DROP_ALGO_DEFAULT)
                            {
                                CliPrintf (CliHandle,
                                           "queue-drop-algo enable  ");
                            }
                        }

                        CliPrintf (CliHandle, "\r\n");

                        /* To display red configuration for the Particular
                         * queuetemplate */
                        i4RetStatus = QoSCliDisplayRedConfShowRunConfig
                            (CliHandle, u4Idx);
                    }
                    CliPrintf (CliHandle, "!\r\n");
                }

            }
        }
        while (nmhGetNextIndexFsQoSQTemplateTable (u4Idx, &u4Idx) ==
               SNMP_SUCCESS);
    }

    UNUSED_PARAM (i4RetStatus);

    return (CLI_SUCCESS);
}

 /****************************************************************************
 *                                                                           
 *     FUNCTION NAME    : QoSCliDisplayRedConfShowRunConfig                     
 *                                                                           
 *     DESCRIPTION      : This function displays current RedConf  
 *                        configurations    
 *                                                                           
 *     INPUT            : CliHandle - CliContext ID                             
 *                          
 *                                                                           
 *     OUTPUT           : None                                               
 *                                                                           
 *     RETURNS          : None                            
 *                                                                           
 *****************************************************************************/
INT4
QoSCliDisplayRedConfShowRunConfig (tCliHandle CliHandle, UINT4 u4Idx)
{
    INT4                i4RowStatus = SNMP_FAILURE;
    INT4                i4DP = 0;
    UINT4               u4MinTH = 0;
    UINT4               u4MaxTH = 0;
    UINT4               u4MaxPktSize = 0;
    UINT4               u4MaxProb = 0;
    UINT4               u4ExpWeight = 0;
    UINT4               u4TmpIdx = 0;
    UINT4               u4Flag = 0;
    UINT4               u4Gain = 0;
    INT4                i4DropThType = 0;
    UINT4               u4ECNThresh = 0;
    tSNMP_OCTET_STRING_TYPE *pOctStrFlag = NULL;
    UINT1               u1RDActFlag = 0;
    UINT2               u2ActMask = 1;

    u4TmpIdx = u4Idx;
    if (nmhGetFirstIndexFsQoSRandomDetectCfgTable (&u4Idx, &i4DP) !=
        SNMP_FAILURE)
    {
        do
        {
            if (nmhGetFsQoSRandomDetectCfgStatus (u4Idx, i4DP, &i4RowStatus) !=
                SNMP_FAILURE)
            {

                if ((i4RowStatus == ACTIVE) && (u4Idx == u4TmpIdx))
                {
                    u4Flag = 1;

                    /*This Flag is to Check whether there is redConf for the 
                       queuetemplate,if it is there then newline will be added. */
                    CliPrintf (CliHandle, " random-detect ");
                    CliPrintf (CliHandle, "dp %d ", i4DP);
                    if (nmhGetFsQoSRandomDetectCfgMinAvgThresh (u4Idx, i4DP,
                                                                &u4MinTH) !=
                        SNMP_FAILURE)
                    {
                        if ((u4MinTH != 0)
                            && (u4MinTH != QOS_RD_CFG_MIN_AVG_TH_DEFAULT))
                        {
                            CliPrintf (CliHandle, "min-threshold %ld ",
                                       u4MinTH);
                        }
                    }

                    if (nmhGetFsQoSRandomDetectCfgMaxAvgThresh (u4Idx, i4DP,
                                                                &u4MaxTH) !=
                        SNMP_FAILURE)
                    {
                        if ((u4MaxTH != 0)
                            && (u4MaxTH != QOS_RD_CFG_MAX_AVG_TH_DEFAULT))
                        {
                            CliPrintf (CliHandle, "max-threshold %ld ",
                                       u4MaxTH);
                        }
                    }

                    if (nmhGetFsQoSRandomDetectCfgMaxPktSize (u4Idx, i4DP,
                                                              &u4MaxPktSize) !=
                        SNMP_FAILURE)
                    {
                        if ((u4MaxPktSize != 0)
                            && (u4MaxPktSize !=
                                QOS_RD_CFG_MAX_PKT_SIZE_DEFAULT))
                        {
                            CliPrintf (CliHandle, "max-pkt-size %ld ",
                                       u4MaxPktSize);
                        }
                    }

                    if (nmhGetFsQoSRandomDetectCfgMaxProb (u4Idx, i4DP,
                                                           &u4MaxProb) !=
                        SNMP_FAILURE)

                    {
                        if ((u4MaxProb != 0)
                            && (u4MaxProb != QOS_RD_CFG_MAX_PROB_DEFAULT))
                        {
                            CliPrintf (CliHandle,
                                       "mark-probability-denominator %ld ",
                                       u4MaxProb);
                        }
                    }
                    if (nmhGetFsQoSRandomDetectCfgExpWeight (u4Idx, i4DP,
                                                             &u4ExpWeight) !=
                        SNMP_FAILURE)
                    {
                        if (u4ExpWeight != 0)
                        {
                            CliPrintf (CliHandle, "exponential-weight %ld ",
                                       u4ExpWeight);
                        }
                    }
                    if (nmhGetFsQoSRandomDetectCfgGain (u4Idx, i4DP,
                                                        &u4Gain) !=
                        SNMP_FAILURE)
                    {
                        if (u4Gain != 0)
                        {
                            CliPrintf (CliHandle, "gain %ld ", u4Gain);
                        }
                    }
                    if (nmhGetFsQoSRandomDetectCfgDropThreshType (u4Idx, i4DP,
                                                                  &i4DropThType)
                        != SNMP_FAILURE)
                    {
                        if (i4DropThType == QOS_CLI_RD_CFG_DISCARD_BYTES)
                        {
                            CliPrintf (CliHandle, "drop-threshold-type bytes ");
                        }
                    }
                    if (nmhGetFsQoSRandomDetectCfgECNThresh
                        (u4Idx, i4DP, &u4ECNThresh) != SNMP_FAILURE)
                    {
                        if (u4ECNThresh != 0)
                        {
                            CliPrintf (CliHandle, "ecn-threshold %ld ",
                                       u4ECNThresh);
                        }
                    }
                    pOctStrFlag =
                        allocmem_octetstring (QOS_RD_CONFIG_ACTION_FLAG_LENGTH);
                    if (pOctStrFlag == NULL)
                    {
                        return (CLI_FAILURE);
                    }

                    MEMSET (pOctStrFlag->pu1_OctetList, 0,
                            pOctStrFlag->i4_Length);
                    if (nmhGetFsQoSRandomDetectCfgActionFlag
                        (u4Idx, i4DP, pOctStrFlag) == SNMP_FAILURE)
                    {
                        free_octetstring (pOctStrFlag);
                        return (CLI_FAILURE);
                    }

                    /* Get the Action Flag */
                    MEMCPY (&u1RDActFlag, pOctStrFlag->pu1_OctetList,
                            pOctStrFlag->i4_Length);
                    free_octetstring (pOctStrFlag);
                    if (u1RDActFlag != 0x00)
                    {
                        CliPrintf (CliHandle, "flag ");
                        while (u2ActMask < QOS_RD_CONFIG_ACTION_MAX_VAL)
                        {
                            switch ((u1RDActFlag & u2ActMask))
                            {
                                case QOS_RD_CONFIG_CAP_AVERAGE:
                                    CliPrintf (CliHandle, "cap-average ");
                                    break;
                                case QOS_RD_CONFIG_MARK_ECN:
                                    CliPrintf (CliHandle, "mark-congestion ");
                                    break;
                                default:
                                    break;
                            }
                            u2ActMask = (UINT2) (u2ActMask << 1);
                        }
                    }
                }
            }

            if (u4Flag == 1)
            {
                CliPrintf (CliHandle, "\r\n");
                u4Flag = 0;
            }
            u2ActMask = 0x1;
        }

        while (nmhGetNextIndexFsQoSRandomDetectCfgTable
               (u4Idx, &u4Idx, i4DP, &i4DP) == SNMP_SUCCESS);

        /* End of while */

    }
    return (CLI_SUCCESS);
}

/****************************************************************************
 *                                                                           
 *     FUNCTION NAME    : QoSShapeTemplateShowRunningConfig                     
 *                                                                           
 *     DESCRIPTION      : This function displays current ShapeTemplate  
 *                        configurations    
 *                                                                           
 *     INPUT            : CliHandle - CliContext ID                             
 *                          
 *                                                                           
 *     OUTPUT           : None                                               
 *                                                                           
 *     RETURNS          : None                            
 *                                                                           
 *****************************************************************************/
INT4
QoSShapeTemplateShowRunningConfig (tCliHandle CliHandle)
{
    INT4                i4RowStatus = SNMP_FAILURE;
    UINT4               u4CIR = 0;
    UINT4               u4CBS = 0;
    UINT4               u4EIR = 0;
    UINT4               u4EBS = 0;
    UINT4               u4Idx = 0;
    UINT1               u1StatusFlag = FALSE;

    if (nmhGetFirstIndexFsQoSShapeTemplateTable (&u4Idx) != SNMP_FAILURE)
    {
        do
        {
            if (nmhGetFsQoSShapeTemplateStatus (u4Idx, &i4RowStatus) !=
                SNMP_FAILURE)
            {
                u1StatusFlag = FALSE;
                CliPrintf (CliHandle, "shape-template %ld ", u4Idx);
                if (i4RowStatus != ACTIVE)
                {
                    u1StatusFlag = TRUE;
                }
                if (u1StatusFlag == FALSE)
                {
                    if (nmhGetFsQoSShapeTemplateCIR (u4Idx, &u4CIR) !=
                        SNMP_FAILURE)
                    {
                        if ((u4CIR != 0)
                            && (u4CIR != QOS_SHAPE_TEMP_CIR_DEFAULT))
                        {
                            CliPrintf (CliHandle, "cir %ld ", u4CIR);
                        }
                    }
                    if (nmhGetFsQoSShapeTemplateCBS (u4Idx, &u4CBS) !=
                        SNMP_FAILURE)
                    {
                        if ((u4CBS != 0)
                            && (u4CBS != QOS_SHAPE_TEMP_CBS_DEFAULT))
                        {
                            CliPrintf (CliHandle, "cbs %ld ", u4CBS);
                        }
                    }

                    if (nmhGetFsQoSShapeTemplateEIR (u4Idx, &u4EIR) !=
                        SNMP_FAILURE)
                    {
                        if ((u4EIR != 0)
                            && (u4EIR != QOS_SHAPE_TEMP_EIR_DEFAULT))
                        {
                            CliPrintf (CliHandle, "eir %ld ", u4EIR);
                        }
                    }

                    if (nmhGetFsQoSShapeTemplateEBS (u4Idx, &u4EBS) !=
                        SNMP_FAILURE)
                    {
                        if ((u4EBS != 0)
                            && (u4EBS != QOS_SHAPE_TEMP_EBS_DEFAULT))
                        {
                            CliPrintf (CliHandle, "ebs %ld ", u4EBS);
                        }
                    }

                    CliPrintf (CliHandle, "\r\n");
                    CliPrintf (CliHandle, "!\r\n");
                }

            }

        }
        while (nmhGetNextIndexFsQoSShapeTemplateTable (u4Idx, &u4Idx)
               == SNMP_SUCCESS);
    }
    return (SNMP_SUCCESS);
}

/****************************************************************************
 *                                                                           
 *     FUNCTION NAME    : QoSSchedulerTemplateShowRunningConfig                 
 *                                                                           
 *     DESCRIPTION      : This function displays current SchedulerTemplate  
 *                        configurations    
 *                                                                           
 *     INPUT            : CliHandle - CliContext ID                             
 *                          
 *                                                                           
 *     OUTPUT           : None                                               
 *                                                                           
 *     RETURNS          : None                            
 *                                                                           
 *****************************************************************************/
INT4
QoSSchedulerTemplateShowRunningConfig (tCliHandle CliHandle)
{
    INT4                i4RowStatus = SNMP_FAILURE;
    INT4                i4SAlgo = 0;
    UINT4               u4ShapeId = 0;
    UINT4               u4HL = 0;
    UINT4               u4SchedIdx = 0;
    UINT1               au1IfName[CFA_MAX_PORT_NAME_LENGTH];
    UINT1               u1Flag = FALSE;
    UINT1               u1StatusFlag = FALSE;
    INT4                i4IfIdx = 0;

    if (nmhGetFirstIndexFsQoSSchedulerTable
        (&i4IfIdx, (UINT4 *) &u4SchedIdx) != SNMP_FAILURE)
    {
        do
        {
            u1Flag = FALSE;
            nmhGetFsQoSSchedulerSchedAlgorithm (i4IfIdx, u4SchedIdx, &i4SAlgo);
            nmhGetFsQoSSchedulerShapeId (i4IfIdx, u4SchedIdx, &u4ShapeId);
            nmhGetFsQoSSchedulerHierarchyLevel (i4IfIdx, u4SchedIdx, &u4HL);

            if ((u4SchedIdx == 1) &&
                (i4SAlgo == QOS_SCHED_ALGO_DEFAULT) &&
                (u4ShapeId == 0) && (u4HL == 0))

            {
                u1Flag = TRUE;
            }
            /* Check for default scheduler ID , algorithm ,shaper ID and Hierarchy level */
            if ((((u4SchedIdx == QOS_HL_DEF_S1_SCHEDULER_ID)
                  && (QOS_S1_SCHEDULER == u4HL))
                 || ((u4SchedIdx == QOS_HL_DEF_S2_SCHEDULER_ID)
                     && (QOS_S2_SCHEDULER == u4HL))
                 ||
                 (((u4SchedIdx == QOS_HL_DEF_S3_SCHEDULER1_ID)
                   || (u4SchedIdx == QOS_HL_DEF_S3_SCHEDULER2_ID))
                  && (QOS_S3_SCHEDULER == u4HL))))
            {
                if ((i4SAlgo == QOS_SCHED_ALGO_DEFAULT) && (u4ShapeId == 0))

                {
                    u1Flag = TRUE;
                }
            }
            if (u1Flag == FALSE)
            {
                if (nmhGetFsQoSSchedulerStatus
                    (i4IfIdx, u4SchedIdx, &i4RowStatus) != SNMP_FAILURE)
                {
                    u1StatusFlag = FALSE;
                    if (i4RowStatus != ACTIVE)
                    {
                        u1StatusFlag = TRUE;
                    }
                    if (u1StatusFlag == FALSE)
                    {
                        CliPrintf (CliHandle, "scheduler %ld ", u4SchedIdx);
                        if (i4IfIdx != 0)
                        {
                            MEMSET (au1IfName, 0, CFA_MAX_PORT_NAME_LENGTH);
                            if (CfaCliConfGetIfName
                                ((UINT4) i4IfIdx,
                                 (INT1 *) au1IfName) == CLI_FAILURE)
                            {

                                CliPrintf (CliHandle, "\r\n");
                                CliPrintf (CliHandle, "!\r\n");
                                return (CLI_FAILURE);
                            }

                            CliPrintf (CliHandle, "interface %s ", au1IfName);
                        }

                        if (nmhGetFsQoSSchedulerSchedAlgorithm
                            (i4IfIdx, u4SchedIdx, &i4SAlgo) != SNMP_FAILURE)
                        {
                            CliPrintf (CliHandle, "sched-algo ");
                            switch (i4SAlgo)
                            {
                                case QOS_CLI_SCHED_ALGO_SP:
                                    CliPrintf (CliHandle, "strict-priority ");
                                    break;

                                case QOS_CLI_SCHED_ALGO_RR:
                                    CliPrintf (CliHandle, "rr ");
                                    break;

                                case QOS_CLI_SCHED_ALGO_WRR:
                                    if (ISS_HW_SUPPORTED ==
                                        IssGetHwCapabilities
                                        (ISS_HW_DWRR_SUPPORT))
                                    {
                                        CliPrintf (CliHandle, "dwrr ");
                                    }

                                    else
                                    {
                                        CliPrintf (CliHandle, "wrr ");
                                    }
                                    break;

                                case QOS_CLI_SCHED_ALGO_WFQ:
                                    CliPrintf (CliHandle, "wfq ");
                                    break;

                                case QOS_CLI_SCHED_ALGO_SRR:
                                    CliPrintf (CliHandle, "strict-rr ");
                                    break;

                                case QOS_CLI_SCHED_ALGO_SWRR:
                                    CliPrintf (CliHandle, "strict-wrr ");
                                    break;

                                case QOS_CLI_SCHED_ALGO_SWFQ:
                                    CliPrintf (CliHandle, "strict-wfq ");
                                    break;

                                case QOS_CLI_SCHED_ALGO_DRR:
                                    CliPrintf (CliHandle, "deficit-rr ");
                                    break;

                                default:
                                    break;
                            }
                        }
                        if (nmhGetFsQoSSchedulerShapeId (i4IfIdx, u4SchedIdx,
                                                         &u4ShapeId) !=
                            SNMP_FAILURE)
                        {
                            if (u4ShapeId != 0)
                            {
                                CliPrintf (CliHandle, "shaper %ld ", u4ShapeId);
                            }
                        }

                        if (nmhGetFsQoSSchedulerHierarchyLevel
                            (i4IfIdx, u4SchedIdx, &u4HL) != SNMP_FAILURE)
                        {
                            if (u4HL != 0)
                            {
                                CliPrintf (CliHandle, "hierarchy-level %ld ",
                                           u4HL);
                            }
                        }

                        CliPrintf (CliHandle, "\r\n");
                        CliPrintf (CliHandle, "!\r\n");
                    }

                }
            }
        }
        while (nmhGetNextIndexFsQoSSchedulerTable
               (i4IfIdx, &i4IfIdx, u4SchedIdx, &u4SchedIdx) == SNMP_SUCCESS);
    }

    return (CLI_SUCCESS);
}

/****************************************************************************
 *                                                                           
 *     FUNCTION NAME    : QoSQueueTableShowRunningConfig                 
 *                                                                           
 *     DESCRIPTION      : This function displays current SchedulerTemplate  
 *                        configurations    
 *                                                                           
 *     INPUT            : CliHandle - CliContext ID                             
 *                         
 *                                                                           
 *     OUTPUT           : None                                               
 *                                                                           
 *     RETURNS          : None                            
 *                                                                           
 *****************************************************************************/
INT4
QoSQueueTableShowRunningConfig (tCliHandle CliHandle)
{
    INT4                i4RowStatus = SNMP_FAILURE;
    INT4                i4IfIdx = 0;
    INT4                i4SAlgo = 0;
    INT4                i4RetStatus = 0;
    UINT4               u4QIdx = 0;
    UINT4               u4QType = 0;
    UINT4               u4SchedId = 0;
    UINT4               u4QWeight = 0;
    UINT4               u4QPri = 0;
    UINT4               u4QShapeId = 0;
    UINT4               u4QueueType = 0;
    UINT1               au1IfName[CFA_MAX_PORT_NAME_LENGTH];
    UINT1               u1Flag = FALSE;
    UINT1               u1StatusFlag = FALSE;

    if (nmhGetFirstIndexFsQoSQTable (&i4IfIdx, &u4QIdx) != SNMP_FAILURE)
    {
        do
        {
            u1Flag = FALSE;
            nmhGetFsQoSQCfgTemplateId (i4IfIdx, u4QIdx, &u4QType);
            nmhGetFsQoSQSchedulerId (i4IfIdx, u4QIdx, &u4SchedId);
            nmhGetFsQoSQWeight (i4IfIdx, u4QIdx, &u4QWeight);
            nmhGetFsQoSQPriority (i4IfIdx, u4QIdx, &u4QPri);
            nmhGetFsQoSQShapeId (i4IfIdx, u4QIdx, &u4QShapeId);
            nmhGetFsQoSQType (i4IfIdx, u4QIdx, &u4QueueType);
            i4RetStatus =
                nmhGetFsQoSSchedulerSchedAlgorithm (i4IfIdx, u4SchedId,
                                                    &i4SAlgo);
            if (i4RetStatus == SNMP_FAILURE)
            {
                i4SAlgo = 0;
            }

            if ((u4QType == 1) && (u4SchedId == 1) && (u4QWeight == 1)
                && (u4QPri == (u4QIdx - 1)) && (u4QShapeId == 0)
                && (u4QueueType == 1))
            {
                u1Flag = TRUE;
            }
            else if ((u4QType == 1) && (u4SchedId == 1)
                     && (i4SAlgo == QOS_CLI_SCHED_ALGO_SP)
                     && (u4QPri == (u4QIdx - 1)) && (u4QShapeId == 0)
                     && (u4QueueType == 1))

            {
                u1Flag = TRUE;
            }

            /*Check for default hierarchy level, queue type and scheduler ID */
            if (((gu1HLSupportFlag == QOS_HL_DEF_SCHED_CFG_SUPPORTED) &&
                 (((u4QueueType == QOS_Q_UNICAST_TYPE)
                   && (u4SchedId == QOS_HL_DEF_S3_SCHEDULER1_ID))
                  || ((u4QueueType == QOS_Q_MULTICAST_TYPE)
                      && (u4SchedId == QOS_HL_DEF_S3_SCHEDULER2_ID)))
                 && ((u4QPri == (u4QIdx - 1)) && (u4QShapeId == 0)
                     && (u4QType == 1) && (u4QWeight == 1))))
            {
                if (QOS_QUEUE_ENTRY_MAX <= QOS_QUEUE_MAX_NUM_UCOSQ)
                {
                    if ((u4QueueType == QOS_Q_UNICAST_TYPE)
                        && (u4QIdx <= QOS_QUEUE_MAX_NUM_UCOSQ))
                    {
                        u1Flag = TRUE;
                    }

                }
                else
                {
                    if ((u4QueueType == QOS_Q_UNICAST_TYPE)
                        && (u4QIdx <= QOS_QUEUE_MAX_NUM_UCOSQ))
                    {
                        u1Flag = TRUE;
                    }
                    else if ((u4QIdx > QOS_QUEUE_MAX_NUM_UCOSQ)
                             && (u4QIdx <= QOS_QUEUE_ENTRY_MAX))
                    {
                        if (u4QueueType == QOS_Q_MULTICAST_TYPE)
                        {
                            u1Flag = TRUE;
                        }
                    }
                }
            }
            if (u1Flag == FALSE)
            {

                if (nmhGetFsQoSQStatus (i4IfIdx, u4QIdx, &i4RowStatus) !=
                    SNMP_FAILURE)
                {
                    u1StatusFlag = FALSE;
                    CliPrintf (CliHandle, "queue %ld ", u4QIdx);
                    if (i4RowStatus != ACTIVE)
                    {
                        u1StatusFlag = TRUE;
                    }
                    if (u1StatusFlag == FALSE)
                    {
                        if (i4IfIdx != 0)
                        {
                            MEMSET (au1IfName, 0, CFA_MAX_PORT_NAME_LENGTH);
                            if (CfaCliConfGetIfName
                                ((UINT4) i4IfIdx,
                                 (INT1 *) au1IfName) == CLI_FAILURE)
                            {

                                CliPrintf (CliHandle, "\r\n");
                                CliPrintf (CliHandle, "!\r\n");
                                return (CLI_FAILURE);
                            }
                            CliPrintf (CliHandle, "interface %s ", au1IfName);
                        }
                        if (nmhGetFsQoSQCfgTemplateId
                            (i4IfIdx, u4QIdx, &u4QType) != SNMP_FAILURE)
                        {
                            /*Checked with default value */
                            if (u4QType > QOS_QT_TBL_DEF_ENTRY_MAX)
                            {
                                CliPrintf (CliHandle, "qtype %ld ", u4QType);
                            }
                        }

                        if (nmhGetFsQoSQSchedulerId
                            (i4IfIdx, u4QIdx, &u4SchedId) != SNMP_FAILURE)
                        {
                            if (u4SchedId != 0)
                            {
                                CliPrintf (CliHandle, "scheduler %ld ",
                                           u4SchedId);
                            }
                        }

                        if (nmhGetFsQoSQWeight (i4IfIdx, u4QIdx, &u4QWeight)
                            != SNMP_FAILURE)
                        {
                            /*Checked with default value of mib */
                            if (u4QWeight != QOS_Q_TABLE_WEIGHT_DEFAULT)
                            {
                                CliPrintf (CliHandle, "weight %ld ", u4QWeight);
                            }
                        }

                        if (nmhGetFsQoSQPriority (i4IfIdx, u4QIdx, &u4QPri)
                            != SNMP_FAILURE)
                        {
                            if (u4QPri != 0)
                            {
                                CliPrintf (CliHandle, "priority %ld ", u4QPri);
                            }
                        }

                        if (nmhGetFsQoSQShapeId (i4IfIdx, u4QIdx, &u4QShapeId)
                            != SNMP_FAILURE)
                        {
                            if (u4QShapeId != 0)
                            {
                                CliPrintf (CliHandle, "shaper %ld ",
                                           u4QShapeId);
                            }
                        }
                        if (nmhGetFsQoSQType (i4IfIdx, u4QIdx, &u4QueueType)
                            != SNMP_FAILURE)
                        {
                            /*Checked with default value of mib */
                            if (u4QueueType != QOS_Q_UNICAST_TYPE)
                            {
                                if (u4QueueType == QOS_Q_SUBSCRIBER_TYPE)
                                {
                                    CliPrintf (CliHandle, "queue-type %s ",
                                               "subscriber");
                                }
                                else
                                {
                                    CliPrintf (CliHandle, "queue-type %s ",
                                               "multicast");
                                }
                            }

                        }
                        CliPrintf (CliHandle, "\r\n");
                        CliPrintf (CliHandle, "!\r\n");
                    }

                }
            }
        }

        while (nmhGetNextIndexFsQoSQTable (i4IfIdx, &i4IfIdx, u4QIdx, &u4QIdx)
               == SNMP_SUCCESS);
    }
    return (CLI_SUCCESS);
}

/****************************************************************************
 *                                                                           
 *     FUNCTION NAME    : QoSQueueMapShowRunningConfig                 
 *                                                                           
 *     DESCRIPTION      : This function displays current QueueMap  
 *                        configurations    
 *                                                                           
 *     INPUT            : CliHandle - CliContext ID                             
 *                          
 *                                                                           
 *     OUTPUT           : None                                               
 *                                                                           
 *     RETURNS          : None                            
 *                                                                           
 *****************************************************************************/
INT4
QoSQueueMapShowRunningConfig (tCliHandle CliHandle)
{
    INT4                i4PriType = 0;
    INT4                i4IfIdx = 0;
    INT4                i4RowStatus = SNMP_FAILURE;
    INT4                i4EntryType = 0;
    UINT4               u4CLASS = 0;
    UINT4               u4PriVal = 0;
    UINT4               u4QId = 0;
    UINT1               au1IfName[CFA_MAX_PORT_NAME_LENGTH];
    tQoSQMapNode       *pQMapNode;
    UINT1               u1Flag = FALSE;
    UINT1               u1StatusFlag = FALSE;

    if (nmhGetFirstIndexFsQoSQMapTable (&i4IfIdx, &u4CLASS, &i4PriType,
                                        &u4PriVal) != SNMP_FAILURE)
    {
        do
        {
            u1Flag = FALSE;
            pQMapNode =
                QoSUtlGetQMapNode (i4IfIdx, u4CLASS, i4PriType, u4PriVal);
            if (pQMapNode != NULL)
            {
                if (QoSIsDefQMapTblEntry (pQMapNode) == QOS_SUCCESS)
                {
                    u1Flag = TRUE;
                }
                if (QosUtlGetQMapEntryType (i4IfIdx, u4CLASS, i4PriType,
                                            u4PriVal, &i4EntryType)
                    == QOS_SUCCESS)
                {
                    if (i4EntryType == QOS_PCP_ENTRY_TYPE_IMPLICIT)
                    {
                        u1Flag = TRUE;
                    }
                }
            }
            if (u1Flag == FALSE)
            {
                if (nmhGetFsQoSQMapStatus (i4IfIdx, u4CLASS, i4PriType,
                                           u4PriVal,
                                           &i4RowStatus) != SNMP_FAILURE)
                {
                    if (nmhGetFsQoSQMapQId (i4IfIdx, u4CLASS, i4PriType,
                                            u4PriVal, &u4QId) != SNMP_FAILURE)
                    {
                        if ((u4QId == (u4PriVal + 1))
                            && (u4QId <= DEFAULT_MAX_ARRAYSIZE_AVIALABLEQUEUE)
                            && (u4PriVal < DEFAULT_MAX_ARRAYSIZE_PRI))
                        {
                            continue;
                        }
                        u1StatusFlag = FALSE;
                        CliPrintf (CliHandle, "queue-map ");
                        if (i4RowStatus != ACTIVE)
                        {
                            u1StatusFlag = TRUE;
                        }
                        if (u1StatusFlag == FALSE)
                        {
                            if (u4CLASS != 0)
                            {
                                CliPrintf (CliHandle, "CLASS %ld ", u4CLASS);
                            }
                            if (i4PriType != 0)
                            {
                                CliPrintf (CliHandle, "regn-priority ");
                                switch (i4PriType)
                                {
                                    case QOS_CLI_QMAP_PRI_TYPE_VLAN_PRI:
                                        CliPrintf (CliHandle, "vlanPri %ld ",
                                                   u4PriVal);
                                        break;
                                    case QOS_CLI_QMAP_PRI_TYPE_DOT1P:
                                        CliPrintf (CliHandle, "dot1P %ld ",
                                                   u4PriVal);
                                        break;
                                    case QOS_CLI_QMAP_PRI_TYPE_IP_TOS:
                                        CliPrintf (CliHandle, "ipTos %ld ",
                                                   u4PriVal);
                                        break;
                                    case QOS_CLI_QMAP_PRI_TYPE_IP_DSCP:
                                        CliPrintf (CliHandle, "ipDscp %ld ",
                                                   u4PriVal);
                                        break;
                                    case QOS_CLI_QMAP_PRI_TYPE_MPLS_EXP:
                                        CliPrintf (CliHandle, "mplsExp %ld ",
                                                   u4PriVal);
                                        break;
                                    case QOS_CLI_QMAP_PRI_TYPE_VLAN_DEI:
                                        CliPrintf (CliHandle, "vlanDEI %ld ",
                                                   u4PriVal);
                                        break;
                                    case QOS_CLI_QMAP_PRI_TYPE_INT_PRI:
                                        CliPrintf (CliHandle,
                                                   "InternalPri %ld ",
                                                   u4PriVal);
                                        break;

                                    default:
                                        break;
                                }
                            }
                            if (i4IfIdx != 0)
                            {
                                MEMSET (au1IfName, 0, CFA_MAX_PORT_NAME_LENGTH);
                                if (CfaCliConfGetIfName
                                    ((UINT4) i4IfIdx,
                                     (INT1 *) au1IfName) == CLI_FAILURE)
                                {

                                    CliPrintf (CliHandle, "\r\n");
                                    CliPrintf (CliHandle, "!\r\n");
                                    return (CLI_FAILURE);
                                }
                                CliPrintf (CliHandle, "interface %s ",
                                           au1IfName);
                            }

                            CliPrintf (CliHandle, "queue-id %ld ", u4QId);
                        }
                    }

                    CliPrintf (CliHandle, "\r\n");
                    CliPrintf (CliHandle, "!\r\n");
                }

            }
        }

        while (nmhGetNextIndexFsQoSQMapTable (i4IfIdx, &i4IfIdx,
                                              u4CLASS, &u4CLASS,
                                              i4PriType, &i4PriType,
                                              u4PriVal, &u4PriVal)
               == SNMP_SUCCESS);
    }
    return (CLI_SUCCESS);
}

/****************************************************************************
 *                                                                           
 *     FUNCTION NAME    : QoSHierarchyTableShowRunningConfig                 
 *                                                                           
 *     DESCRIPTION      : This function displays current HierarchyTable  
 *                        configurations    
 *                                                                           
 *     INPUT            : CliHandle - CliContext ID                             
 *                     
 *                                                                           
 *     OUTPUT           : None                                               
 *                                                                           
 *     RETURNS          : None                            
 *                                                                           
 *****************************************************************************/
INT4
QoSHierarchyTableShowRunningConfig (tCliHandle CliHandle)
{
    INT4                i4RowStatus = SNMP_FAILURE;
    INT4                i4Pri = 0;
    INT4                i4IfIdx = 0;
    UINT4               u4HL = 0;
    UINT4               u4SchedId = 0;
    UINT4               u4NextQ = 0;
    UINT4               u4NextSchedId = 0;
    UINT4               u4Weight = 0;
    UINT1               au1IfName[CFA_MAX_PORT_NAME_LENGTH];
    UINT1               u1Flag = FALSE;

    if (nmhGetFirstIndexFsQoSHierarchyTable (&i4IfIdx, &u4HL, &u4SchedId)
        != SNMP_FAILURE)
    {
        do
        {
            /* Check for default scheduler ID and Hierarchy level */
            if ((((u4SchedId == QOS_HL_DEF_S1_SCHEDULER_ID)
                  && (QOS_S1_SCHEDULER == u4HL))
                 || ((u4SchedId == QOS_HL_DEF_S2_SCHEDULER_ID)
                     && (QOS_S2_SCHEDULER == u4HL))
                 ||
                 (((u4SchedId == QOS_HL_DEF_S3_SCHEDULER1_ID)
                   || (u4SchedId == QOS_HL_DEF_S3_SCHEDULER2_ID))
                  && (QOS_S3_SCHEDULER == u4HL))))
            {
                u1Flag = TRUE;
            }
            if (u1Flag == FALSE)
            {
                if (nmhGetFsQoSHierarchyStatus (i4IfIdx, u4HL, u4SchedId,
                                                &i4RowStatus) != SNMP_FAILURE)
                {
                    CliPrintf (CliHandle, "sched-hierarchy ");
                    if (i4RowStatus != ACTIVE)
                    {
                        CliPrintf (CliHandle, "!\r\n");
                        return (CLI_SUCCESS);
                    }
                    if (i4IfIdx != 0)
                    {
                        MEMSET (au1IfName, 0, CFA_MAX_PORT_NAME_LENGTH);
                        if (CfaCliConfGetIfName
                            ((UINT4) i4IfIdx,
                             (INT1 *) au1IfName) == CLI_FAILURE)
                        {
                            CliPrintf (CliHandle, "\r\n");
                            CliPrintf (CliHandle, "!\r\n");
                            return (CLI_FAILURE);
                        }
                        CliPrintf (CliHandle, "interface %s ", au1IfName);
                    }

                    CliPrintf (CliHandle, "hierarchy-level %ld ", u4HL);
                    CliPrintf (CliHandle, "sched-id %ld ", u4SchedId);

                    if (nmhGetFsQoSHierarchyQNext (i4IfIdx, u4HL, u4SchedId,
                                                   &u4NextQ) != SNMP_FAILURE)
                    {
                        if (u4NextQ != 0)
                        {
                            CliPrintf (CliHandle, "next-level-queue %ld ",
                                       u4NextQ);
                        }
                    }

                    if (nmhGetFsQoSHierarchySchedNext (i4IfIdx, u4HL, u4SchedId,
                                                       &u4NextSchedId) !=
                        SNMP_FAILURE)
                    {
                        if (u4NextSchedId != 0)
                        {
                            CliPrintf (CliHandle, "next-level-scheduler %ld ",
                                       u4NextSchedId);
                        }
                    }
                    if (nmhGetFsQoSHierarchyPriority (i4IfIdx, u4HL, u4SchedId,
                                                      &i4Pri) != SNMP_FAILURE)
                    {
                        if (i4Pri != 0)
                        {
                            CliPrintf (CliHandle, "priority %ld ", i4Pri);
                        }
                    }

                    if (nmhGetFsQoSHierarchyWeight (i4IfIdx, u4HL, u4SchedId,
                                                    &u4Weight) != SNMP_FAILURE)
                    {
                        if (u4Weight != 0)
                        {
                            CliPrintf (CliHandle, "weight %ld ", u4Weight);
                        }
                    }

                    CliPrintf (CliHandle, "\r\n");
                    CliPrintf (CliHandle, "!\r\n");
                }
            }
        }
        while (nmhGetNextIndexFsQoSHierarchyTable (i4IfIdx, &i4IfIdx, u4HL,
                                                   &u4HL, u4SchedId, &u4SchedId)
               == SNMP_SUCCESS);
    }
    return (CLI_SUCCESS);
}

/****************************************************************************
 *                                                                           
 *     FUNCTION NAME    : QoSDefaultUserPriShowRunningConfig                 
 *                                                                           
 *     DESCRIPTION      : This function displays current DefaultUserPri  
 *                        configurations    
 *                                                                           
 *     INPUT            : CliHandle - CliContext ID                             
 *                         
 *                                                                           
 *     OUTPUT           : None                                               
 *                                                                           
 *     RETURNS          : None                            
 *                                                                           
 *****************************************************************************/
INT4
QoSDefaultUserPriShowRunningConfig (tCliHandle CliHandle)
{
    INT4                i4UDPri = 0;
    INT4                i4IfIdx = 0;
    INT4                i4Pref = 0;
    UINT1               au1IfName[CFA_MAX_PORT_NAME_LENGTH];
    UINT1               u1Flag = FALSE;

    if (nmhGetFirstIndexFsQoSDefUserPriorityTable (&i4IfIdx) != SNMP_FAILURE)
    {
        do
        {
            u1Flag = FALSE;
            if (nmhGetFsQoSPortDefaultUserPriority (i4IfIdx, &i4UDPri)
                != SNMP_FAILURE)
            {
                if (i4UDPri == 0)
                {
                    u1Flag = TRUE;
                }
                if (u1Flag == FALSE)
                {
                    if (i4IfIdx != 0)
                    {
                        MEMSET (au1IfName, 0, CFA_MAX_PORT_NAME_LENGTH);
                        if (CfaCliConfGetIfName
                            ((UINT4) i4IfIdx,
                             (INT1 *) au1IfName) == CLI_FAILURE)
                        {
                            CliPrintf (CliHandle, "!\r\n");
                            return (CLI_FAILURE);
                        }
                        CliPrintf (CliHandle, "qos ");
                        CliPrintf (CliHandle, "interface %s ", au1IfName);
                        CliPrintf (CliHandle, "def-user-priority %ld \r\n",
                                   i4UDPri);
                    }
                    CliPrintf (CliHandle, "!\r\n");
                }
            }
            /*Show running config for pbit preference,when 
             * the mode is set to disable  */
            if (nmhGetFsQoSPortPbitPrefOverDscp (i4IfIdx, &i4Pref)
                != SNMP_FAILURE)
            {
                if (i4Pref == QOS_PBIT_PREF_DISABLE)
                {
                    MEMSET (au1IfName, 0, CFA_MAX_PORT_NAME_LENGTH);
                    CfaCliConfGetIfName ((UINT4) i4IfIdx, (INT1 *) au1IfName);
                    CliPrintf (CliHandle, "interface %s \r\n", au1IfName);
                    CliPrintf (CliHandle, "qos pbit-preference disable\r\n");
                }
                CliPrintf (CliHandle, "!\r\n");
            }
        }
        while (nmhGetNextIndexFsQoSDefUserPriorityTable (i4IfIdx, &i4IfIdx) ==
               SNMP_SUCCESS);
    }
    return (CLI_SUCCESS);

}

/****************************************************************************
 *                                                                           
 *     FUNCTION NAME    : QoSCpuRateLimitShowRunningConfig                 
 *                                                                           
 *     DESCRIPTION      : This function displays current CPU rate limiting  
 *                        configurations    
 *                                                                           
 *     INPUT            : CliHandle - CliContext ID                             
 *                         
 *                                                                           
 *     OUTPUT           : None                                               
 *                                                                           
 *     RETURNS          : None                            
 *                                                                           
 *****************************************************************************/
INT4
QoSCpuRateLimitShowRunningConfig (tCliHandle CliHandle)
{
    UINT4               u4CpuQId = 0;
    UINT4               u4MinRate = 0;
    UINT4               u4MaxRate = 0;
    UINT1               u1Flag = FALSE;

    if (nmhGetFirstIndexFsQosHwCpuRateLimitTable (&u4CpuQId) != SNMP_FAILURE)
    {
        do
        {
            u1Flag = FALSE;
            if (nmhGetFsQosHwCpuMinRate (u4CpuQId, &u4MinRate) != SNMP_FAILURE)
            {
                if (u4MinRate != QOS_DEF_CPU_MIN_RATE)
                {
                    u1Flag = TRUE;
                }
            }
            if (nmhGetFsQosHwCpuMaxRate (u4CpuQId, &u4MaxRate) != SNMP_FAILURE)
            {
                if (u4MaxRate != QOS_DEF_CPU_MAX_RATE)
                {
                    u1Flag = TRUE;
                }
            }
            if (u1Flag == TRUE)
            {
                CliPrintf (CliHandle, "cpu rate limit queue %ld ", u4CpuQId);
                CliPrintf (CliHandle, "minrate %ld ", u4MinRate);
                CliPrintf (CliHandle, "maxrate %ld ", u4MaxRate);

                CliPrintf (CliHandle, "\r\n");
                CliPrintf (CliHandle, "!\r\n");
            }
        }
        while (nmhGetNextIndexFsQosHwCpuRateLimitTable (u4CpuQId, &u4CpuQId) ==
               SNMP_SUCCESS);

    }
    return (CLI_SUCCESS);

}

/****************************************************************************
 *                                                                           
 *     FUNCTION NAME    : QoSVlanQMapShowRunningConfig                      
 *                                                                           
 *     DESCRIPTION      : This function displays current VlanQMap
 *                        configurations    
 *                                                                           
 *     INPUT            : CliHandle - CliContext ID                             
 *                          
 *                                                                           
 *     OUTPUT           : None                                               
 *                                                                           
 *     RETURNS          : None                            
 *                                                                           
 *****************************************************************************/
INT4
QoSVlanQMapShowRunningConfig (tCliHandle CliHandle)
{
    INT4                i4RowStatus = SNMP_FAILURE;
    UINT4               u4IfNum = 0;
    UINT4               u4Idx = 0;
    UINT4               u4VlanId = 0;
    UINT1               au1IfName[CFA_MAX_PORT_NAME_LENGTH];
    UINT1               u1Flag = FALSE;
    UINT1               u1StatusFlag = FALSE;

    if (nmhGetFirstIndexFsQoSVlanMapTable (&u4Idx) != SNMP_FAILURE)
    {
        do
        {
            u1Flag = FALSE;
            if (u1Flag == FALSE)
            {
                if (nmhGetFsQoSVlanMapStatus (u4Idx, &i4RowStatus) !=
                    SNMP_FAILURE)
                {
                    u1StatusFlag = FALSE;
                    CliPrintf (CliHandle, "vlan-map %d\r\n", u4Idx);
                    if (i4RowStatus != ACTIVE)
                    {
                        u1StatusFlag = TRUE;
                    }
                    if (u1StatusFlag == FALSE)
                    {
                        CliPrintf (CliHandle, " map ");
                        if (nmhGetFsQoSVlanMapIfIndex (u4Idx, &u4IfNum) !=
                            SNMP_FAILURE)
                        {
                            if (u4IfNum != 0)
                            {
                                MEMSET (au1IfName, 0, CFA_MAX_PORT_NAME_LENGTH);
                                if (CfaCliConfGetIfName
                                    (u4IfNum,
                                     (INT1 *) au1IfName) == CLI_FAILURE)
                                {
                                    return (CLI_FAILURE);
                                }
                                CliPrintf (CliHandle, "interface %s ",
                                           au1IfName);
                            }
                        }

                        if (nmhGetFsQoSVlanMapVlanId (u4Idx, &u4VlanId) !=
                            SNMP_FAILURE)
                        {
                            if (u4VlanId != 0)
                            {
                                CliPrintf (CliHandle, "Vlan %ld ", u4VlanId);
                            }
                        }
                        CliPrintf (CliHandle, "\r\n");

                        CliPrintf (CliHandle, "!\r\n");
                    }

                }
            }
        }
        while ((nmhGetNextIndexFsQoSVlanMapTable (u4Idx, &u4Idx)
                == SNMP_SUCCESS));
        CliPrintf (CliHandle, "!\r\n");
    }
    return (CLI_SUCCESS);
}

/************************************************************************/
/*  FUNCTION NAME    :QosCliSetTrace                                    */
/*                                                                      */
/*  DESCRIPTION      : This function is used to enable/disbale          */
/*                       the trace flag for QOS Module                  */
/* Input(s)          : CliHandle    - Handle to CLI session             */
/*                   : u4Flag  - Trace Flag                             */
/* Output(s)         : None.                                            */
/* Global Variables                                                     */
/* Referred          : None.                                            */
/* Global Variables                                                     */
/* Modified          : None.                                            */
/* Return Value(s)   : CLI_SUCCESS/CLI_FAILURE                          */
/* Called By         : cli_process_qos_cmd                              */
/* Calling Function  : SNMP nmh Routines                                */
/*************************************************************************/

INT4
QosCliSetTrace (tCliHandle CliHandle, UINT4 u4TraceLevel, UINT1 u1Flag)
{
    UINT4               u4ErrorCode = QOS_INIT_VAL;
    UINT4               u4OriginalTrace = QOS_INIT_VAL;

    if (u1Flag == QOS_FALSE)
    {
        if (nmhGetFsQoSTrcFlag (&u4OriginalTrace) == SNMP_SUCCESS)
        {
            u4TraceLevel = u4OriginalTrace & (~u4TraceLevel);
        }
        else
        {
            return (CLI_FAILURE);
        }
    }
    else
    {
        if (nmhGetFsQoSTrcFlag (&u4OriginalTrace) == SNMP_SUCCESS)
        {
            u4TraceLevel = u4OriginalTrace | u4TraceLevel;
        }
    }

    if (nmhTestv2FsQoSTrcFlag (&u4ErrorCode, u4TraceLevel) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }
    if (nmhSetFsQoSTrcFlag (u4TraceLevel) != SNMP_SUCCESS)
    {

        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : QoSCliConfCpuRateLimit                               */
/* Description        : This function is used to configure rate limiting     */
/*                      value for CPU                                        */
/* Input(s)           : CliHandle    - Handle to CLI session                 */
/*                    : i4IfIdx      - Interface Idx                         */
/*                    : i4MapType    - MapTye   CLASS or PRI_TYPE            */
/*                    : u4MapVal     - Value for the Type                    */
/*                    : u4QId        - Q Id for the Map                      */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : CLI_SUCCESS/CLI_FAILURE                              */
/* Called By          : cli_process_qos_cmd                                  */
/* Calling Function   : SNMP nmh Routines                                    */
/*****************************************************************************/
INT4
QoSCliConfCpuRateLimit (tCliHandle CliHandle, UINT4 u4CpuQId, UINT4 u4MinRate,
                        UINT4 u4MaxRate)
{
    UINT4               u4ErrorCode = 0;
    INT4                i4RowStatus = 0;

    /* 1. Check the Table Entry 
     * 2. If the Entry exists Set it as NOT_IN_SERVICE
     * 3. Configure the Given Values.
     */

    nmhGetFsQosHwCpuRowStatus (u4CpuQId, &i4RowStatus);
    if (i4RowStatus == ACTIVE)
    {
        if (nmhTestv2FsQosHwCpuRowStatus (&u4ErrorCode, u4CpuQId,
                                          NOT_IN_SERVICE) == SNMP_FAILURE)
        {
            return (CLI_FAILURE);
        }

        if (nmhSetFsQosHwCpuRowStatus (u4CpuQId, NOT_IN_SERVICE)
            == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            nmhSetFsQosHwCpuRowStatus (u4CpuQId, ACTIVE);
            return CLI_FAILURE;
        }
    }
    /* Configure MinRate */
    if ((nmhTestv2FsQosHwCpuMinRate (&u4ErrorCode, u4CpuQId,
                                     u4MinRate) == SNMP_FAILURE))
    {
        nmhSetFsQosHwCpuRowStatus (u4CpuQId, ACTIVE);
        return (CLI_FAILURE);
    }

    if ((nmhSetFsQosHwCpuMinRate (u4CpuQId, u4MinRate) == SNMP_FAILURE))
    {
        nmhSetFsQosHwCpuRowStatus (u4CpuQId, ACTIVE);
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }

    /* Configure MaxRate */
    if ((nmhTestv2FsQosHwCpuMaxRate (&u4ErrorCode, u4CpuQId,
                                     u4MaxRate) == SNMP_FAILURE))
    {
        nmhSetFsQosHwCpuRowStatus (u4CpuQId, ACTIVE);
        return (CLI_FAILURE);
    }

    if ((nmhSetFsQosHwCpuMaxRate (u4CpuQId, u4MaxRate) == SNMP_FAILURE))
    {
        nmhSetFsQosHwCpuRowStatus (u4CpuQId, ACTIVE);
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }

    /* Make the RowStatus Active */
    if (nmhTestv2FsQosHwCpuRowStatus (&u4ErrorCode, u4CpuQId,
                                      ACTIVE) == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    if (nmhSetFsQosHwCpuRowStatus (u4CpuQId, ACTIVE) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }

    return (CLI_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSCliShowCpuRateLimit                               */
/* Description        : This function is used to get the rate limit          */
/*                      configuration of CPU.                                */
/* Input(s)           : CliHandle    - Handle to CLI session                 */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : CLI_SUCCESS/CLI_FAILURE                              */
/* Called By          : cli_process_qos_cmd                                  */
/* Calling Function   : SNMP nmh Routines                                    */
/*****************************************************************************/
INT4
QoSCliShowCpuRateLimit (tCliHandle CliHandle)
{
    UINT4               u4CpuQId = 0;
    UINT4               u4MinRate = 0;
    UINT4               u4MaxRate = 0;
    INT4                i4RowStatus = SNMP_FAILURE;
    INT4                i4RetStatus = SNMP_FAILURE;

    /* Display All */
    CliPrintf (CliHandle, "\r\n");
    CliPrintf (CliHandle, "QoS CPU Queue Rate Limit Table\r\n");
    CliPrintf (CliHandle, "------------------------------\r\n");

    CliPrintf (CliHandle, "%-10s %-10s %-10s \r\n",
               "Queue ID", "MinRate", "MaxRate");
    CliPrintf (CliHandle, "%-10s %-10s %-10s \r\n",
               "--------", "-------", "-------");

    i4RetStatus = nmhGetFirstIndexFsQosHwCpuRateLimitTable (&u4CpuQId);
    if (i4RetStatus == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    do
    {
        /* Check the Status */
        if (nmhGetFsQosHwCpuRowStatus (u4CpuQId, &i4RowStatus) == SNMP_FAILURE)
        {
            return (CLI_FAILURE);
        }

        if (i4RowStatus == ACTIVE)
        {
            /* Display Entry */
            if (nmhGetFsQosHwCpuMinRate (u4CpuQId, &u4MinRate) == SNMP_FAILURE)
            {
                return (CLI_FAILURE);
            }
            if (nmhGetFsQosHwCpuMaxRate (u4CpuQId, &u4MaxRate) == SNMP_FAILURE)
            {
                return (CLI_FAILURE);
            }
            CliPrintf (CliHandle,
                       "%-10d %-10d %-10d\r\n", u4CpuQId, u4MinRate, u4MaxRate);
        }
        i4RetStatus =
            nmhGetNextIndexFsQosHwCpuRateLimitTable (u4CpuQId, &u4CpuQId);
    }
    while (i4RetStatus == SNMP_SUCCESS);

    CliPrintf (CliHandle, "\r\n");
    return (CLI_SUCCESS);

}

/************************************************************************/
/*  FUNCTION NAME    :IssQosShowDebugging                               */
/*                                                                      */
/*  DESCRIPTION      : This function prints the RMON2 debug level       */
/* Input(s)          : CliHandle    - Handle to CLI session             */
/* Output(s)         : None.                                            */
/* Global Variables                                                     */
/* Referred          : None.                                            */
/* Global Variables                                                     */
/* Modified          : None.                                            */
/* Return Value(s)   : None                                             */
/*************************************************************************/

VOID
IssQosShowDebugging (tCliHandle CliHandle)
{
    UINT4               u4TraceVal = QOS_INIT_VAL;

    if (nmhGetFsQoSTrcFlag (&u4TraceVal) != SNMP_SUCCESS)
    {
        return;
    }

    if (u4TraceVal == QOS_INIT_VAL)
    {
        return;
    }

    CliPrintf (CliHandle, "\rQOS : ");
    if ((u4TraceVal & INIT_SHUT_TRC) != 0)
    {
        CliPrintf (CliHandle, "\r\n  QOS init and shutdown debugging is on");
    }
    if ((u4TraceVal & MGMT_TRC) != 0)
    {
        CliPrintf (CliHandle, "\r\n  QOS management debugging is on");
    }
    if ((u4TraceVal & CONTROL_PLANE_TRC) != 0)
    {
        CliPrintf (CliHandle, "\r\n  QOS control plane debugging is on");
    }
    if ((u4TraceVal & DUMP_TRC) != 0)
    {
        CliPrintf (CliHandle, "\r\n  QOS packet dump debugging is on");
    }
    if ((u4TraceVal & OS_RESOURCE_TRC) != 0)
    {
        CliPrintf (CliHandle,
                   "\r\n  QOS all resources except buffers debugging is on");
    }
    if ((u4TraceVal & ALL_FAILURE_TRC) != 0)
    {
        CliPrintf (CliHandle, "\r\n  QOS all failure debugging is on");
    }
    if ((u4TraceVal & BUFFER_TRC) != 0)
    {
        CliPrintf (CliHandle,
                   "\r\n  QOS buffer allocation / release debugging is on");
    }

    CliPrintf (CliHandle, "\r\n");
    return;
}

/*****************************************************************************/
/* Function Name      : QoSCliShowDscptoQMap                                 */
/* Description        : This function is used to display the Dscp to Queue   */
/*                      mapping information                                  */
/* Input(s)           : CliHandle    - Handle to CLI session                 */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : CLI_SUCCESS/CLI_FAILURE                              */
/* Called By          : cli_process_qos_cmd                                  */
/*****************************************************************************/
INT4
QoSCliShowDscptoQMap (tCliHandle CliHandle)
{
    INT4                i4QosDscp = 0;
    INT4                i4QosQueue = 0;

    CliPrintf (CliHandle, "\r\n");
    CliPrintf (CliHandle, "Default DSCP to Queue Map \r\n");
    CliPrintf (CliHandle, "--------------------------\r\n");

    CliPrintf (CliHandle, "%-10s %-10s \r\n", "DSCP", "Queue ID");
    CliPrintf (CliHandle, "%-10s %-10s \r\n", "--------", "-------");
    for (i4QosDscp = QOS_DSCP_MIN_VAL; i4QosDscp <= QOS_DSCP_MAX_VAL;
         i4QosDscp++)
    {
        if (SNMP_FAILURE == nmhGetFsQoSDscpQueueMap (i4QosDscp, &i4QosQueue))
        {
            return CLI_FAILURE;
        }

        CliPrintf (CliHandle, "%-12d %-12d \r\n", i4QosDscp, i4QosQueue);
    }

    return (CLI_SUCCESS);

}

/*****************************************************************************/
/* Function Name      : QoSCliSetTrafficClassToPcp                           */
/* Description        : This function is used to map traffic class to        */
/*                      priority                                             */
/* Input(s)           : CliHandle    - Handle to CLI session                 */
/*                      i4TrafficClass                                       */
/*                      i4Priority                                           */
/*                      i4PortId                                             */
/*                      u1Action                                             */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : CLI_SUCCESS/CLI_FAILURE                              */
/* Called By          : cli_process_qos_cmd                                  */
/*****************************************************************************/
INT4
QoSCliSetTrafficClassToPcp (tCliHandle CliHandle, INT4 i4TrafficClass,
                            INT4 i4Priority, INT4 i4PortId)
{
    UNUSED_PARAM (CliHandle);

    if ((i4Priority < QOS_PRI_MIN_VAL) || (i4Priority > QOS_PRI_MAX_VAL))
    {
        return CLI_FAILURE;
    }

    if ((i4TrafficClass < QOS_TRAF_CLASS_MIN_VAL) ||
        (i4TrafficClass >= QOS_TRAF_CLASS_MAX_VAL))
    {
        return CLI_FAILURE;
    }

    if (QosHwWrSetTrafficClassToPcp (i4PortId,
                                     i4Priority, i4TrafficClass) != QOS_SUCCESS)
    {
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : QosCliSetMeterStatStatus                         */
/*                                                                           */
/*    Description         : This function is used to enable/disable the      */
/*                          statistics collection on the configured Meter    */
/*                                                                           */
/*    Input(s)            : CliHandle       - Cli Handle                     */
/*                          u4MeterId       - Meter ID                       */
/*                          i4StatStatus - Stat Status Enable/Disable        */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : CLI_SUCCESS/CLI_FAILURE                          */
/*                                                                           */
/*****************************************************************************/
INT4
QosCliSetMeterStatStatus (tCliHandle CliHandle, UINT4 u4MeterId,
                          INT4 i4StatStatus)
{
    UINT4               u4ErrorCode = 0;

    UNUSED_PARAM (CliHandle);

    if (nmhTestv2FsQoSPolicerStatsStatus
        (&u4ErrorCode, u4MeterId, i4StatStatus) != QOS_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsQoSPolicerStatsStatus (u4MeterId, i4StatStatus) != QOS_SUCCESS)
    {
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : QosCliClearMeterStats                            */
/*                                                                           */
/*    Description         : This function is used to enable/disable the      */
/*                          statistics collection on the configured Meter    */
/*                                                                           */
/*    Input(s)            : CliHandle       - Cli Handle                     */
/*                          u4MeterId       - Meter ID                       */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : CLI_SUCCESS/CLI_FAILURE                          */
/*                                                                           */
/*****************************************************************************/
INT4
QosCliClearMeterStats (tCliHandle CliHandle, UINT4 u4MeterId)
{
    UINT4               u4CurrMeterId = 0;
    UINT4               u4NextMeterId = 0;
    UINT4               u4ErrorCode;

    UNUSED_PARAM (CliHandle);

    if (u4MeterId == 0)
    {
        while (nmhGetNextIndexFsQoSPolicerStatsTable
               (u4CurrMeterId, &u4NextMeterId) == QOS_SUCCESS)
        {
            if (nmhTestv2FsQoSPolicerStatsClearCounter
                (&u4ErrorCode, u4NextMeterId, QOS_TRUE) != QOS_SUCCESS)
            {
                return CLI_FAILURE;
            }

            if (nmhSetFsQoSPolicerStatsClearCounter
                (u4NextMeterId, QOS_TRUE) != QOS_SUCCESS)
            {
                return CLI_FAILURE;
            }

            u4CurrMeterId = u4NextMeterId;
        }
    }
    else
    {
        if (nmhTestv2FsQoSPolicerStatsClearCounter
            (&u4ErrorCode, u4MeterId, QOS_TRUE) != QOS_SUCCESS)
        {
            return CLI_FAILURE;
        }

        if (nmhSetFsQoSPolicerStatsClearCounter
            (u4MeterId, QOS_TRUE) != QOS_SUCCESS)
        {
            return CLI_FAILURE;
        }

    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : QosCliSetPcpSelRow                               */
/*                                                                           */
/*    Description         : This function is used to set the Priority Code   */
/*                          Point Selection row for a Port                   */
/*                                                                           */
/*    Input(s)            : CliHandle       - Cli Handle                     */
/*                          u4IfIndex       - If Index                       */
/*                          u4PktType       - Packet Type                    */
/*                          i4PcpSelRow     - PCP Selection Row              */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : CLI_SUCCESS/CLI_FAILURE                          */
/*                                                                           */
/*****************************************************************************/
INT4
QoSCliSetPcpSelRow (tCliHandle CliHandle, INT4 i4IfIndex,
                    INT4 i4PktType, INT4 i4PcpSelRow)
{
    UINT4               u4ErrorCode = 0;
    INT4                i4RetStatus = SNMP_FAILURE;
    INT4                i4RowStatus = 0;
    INT4                i4CurPcpSelRow = (INT4) CLI_RS_INVALID_MASK;

    /* 1. Check the Table Entry 
     * 2. If the Entry exists Set it as NOT_IN_SERVICE
     * 3. Configure the Given Values.
     * 4. If not exists create and Configure the Values.
     * 5. Any Failure in the New Entry Configurations Delete the Entry.
     * 6. Any Failure in the Modification of an Entry will Restore the 
     *    old Values.
     */

    i4RetStatus = nmhGetFsQoSPcpRowStatus (i4IfIndex, i4PktType, &i4RowStatus);
    if (i4RetStatus == SNMP_FAILURE)
    {
        i4RetStatus =
            nmhTestv2FsQoSPcpRowStatus (&u4ErrorCode, i4IfIndex,
                                        i4PktType, CREATE_AND_WAIT);
        if (i4RetStatus == SNMP_FAILURE)
        {
            return (CLI_FAILURE);
        }

        i4RetStatus = nmhSetFsQoSPcpRowStatus (i4IfIndex,
                                               i4PktType, CREATE_AND_WAIT);
        if (i4RetStatus == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }
    }
    else if (i4RowStatus == ACTIVE)
    {
        i4RetStatus =
            nmhTestv2FsQoSPcpRowStatus (&u4ErrorCode, i4IfIndex,
                                        i4PktType, NOT_IN_SERVICE);
        if (i4RetStatus == SNMP_FAILURE)
        {
            return (CLI_FAILURE);
        }

        i4RetStatus = nmhSetFsQoSPcpRowStatus (i4IfIndex,
                                               i4PktType, NOT_IN_SERVICE);
        if (i4RetStatus == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }
    }

    i4RetStatus = nmhTestv2FsQoSPcpSelRow (&u4ErrorCode, i4IfIndex,
                                           i4PktType, i4PcpSelRow);
    if (i4RetStatus == SNMP_FAILURE)
    {
        QoSCliRSPortTblEntry (i4IfIndex, i4PktType,
                              i4CurPcpSelRow, i4RowStatus);
        return (CLI_FAILURE);
    }

    i4RetStatus = nmhGetFsQoSPcpSelRow (i4IfIndex, i4PktType, &i4CurPcpSelRow);
    if (i4RetStatus == SNMP_FAILURE)
    {
        QoSCliRSPortTblEntry (i4IfIndex, i4PktType,
                              i4CurPcpSelRow, i4RowStatus);
        return (CLI_FAILURE);
    }

    i4RetStatus = nmhSetFsQoSPcpSelRow (i4IfIndex, i4PktType, i4PcpSelRow);
    if (i4RetStatus == SNMP_FAILURE)
    {
        QoSCliRSPortTblEntry (i4IfIndex, i4PktType,
                              i4CurPcpSelRow, i4RowStatus);
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }

    i4RetStatus = nmhTestv2FsQoSPcpRowStatus (&u4ErrorCode, i4IfIndex,
                                              i4PktType, ACTIVE);
    if (i4RetStatus == SNMP_FAILURE)
    {
        QoSCliRSPortTblEntry (i4IfIndex, i4PktType,
                              i4CurPcpSelRow, i4RowStatus);
        return (CLI_FAILURE);
    }

    i4RetStatus = nmhSetFsQoSPcpRowStatus (i4IfIndex, i4PktType, ACTIVE);
    if (i4RetStatus == SNMP_FAILURE)
    {
        QoSCliRSPortTblEntry (i4IfIndex, i4PktType,
                              i4CurPcpSelRow, i4RowStatus);
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }

    return (CLI_SUCCESS);

}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : QosCliShowPcpSelRow                              */
/*                                                                           */
/*    Description         : This function is used to show the Priority Code  */
/*                          Point Selection row for a Port                   */
/*                                                                           */
/*    Input(s)            : CliHandle       - Cli Handle                     */
/*                          u4IfIndex       - If Index                       */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : CLI_SUCCESS/CLI_FAILURE                          */
/*                                                                           */
/*****************************************************************************/
INT4
QoSCliShowPcpSelRow (tCliHandle CliHandle, INT4 i4Idx, INT4 i4PktType)
{
    INT4                i4RetStatus = SNMP_FAILURE;

    CliPrintf (CliHandle, "QoS Port Table Entries\r\n");
    CliPrintf (CliHandle, "----------------------\r\n");

    /* Display All */
    if ((i4Idx == 0) && (i4PktType == 0))
    {
        i4RetStatus = nmhGetFirstIndexFsQoSPortTable (&i4Idx, &i4PktType);
        if (i4RetStatus == SNMP_FAILURE)
        {
            return (CLI_FAILURE);
        }

        do
        {
            /* Display Entry */
            i4RetStatus = QoSCliUtlDisplayPortTblEntry (CliHandle, i4Idx,
                                                        i4PktType);
            if (i4RetStatus == CLI_FAILURE)
            {
                return (CLI_FAILURE);
            }

            i4RetStatus = nmhGetNextIndexFsQoSPortTable (i4Idx, &i4Idx,
                                                         i4PktType, &i4PktType);
        }
        while (i4RetStatus == SNMP_SUCCESS);

        return (CLI_SUCCESS);
    }
    else
    {
        i4RetStatus = QoSCliUtlDisplayPortTblEntry (CliHandle, i4Idx,
                                                    i4PktType);
        if (i4RetStatus == CLI_FAILURE)
        {
            return (CLI_FAILURE);
        }
    }

    return (CLI_SUCCESS);

}

/*****************************************************************************/
/* Function Name      : QoSCliUtlDisplayPortTblEntry                         */
/* Description        : This function is used to Display the Port PCP        */
/*                      Table Entries Parameters                             */
/* Input(s)           : CliHandle    - Handle to CLI session                 */
/*                    : i4Idx        - Table Entries Index                   */
/*                    : i4PktType    - Table Entries Index                   */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : CLI_SUCCESS/CLI_FAILURE                              */
/* Called By          : QoSCliShowPcpSelRow                                  */
/* Calling Function   : SNMP nmh Routines                                    */
/*****************************************************************************/
INT4
QoSCliUtlDisplayPortTblEntry (tCliHandle CliHandle, INT4 i4Idx, INT4 i4PktType)
{
    INT4                i4RetStatus = SNMP_FAILURE;
    INT4                i4RowStatus = SNMP_FAILURE;
    INT4                i4PcpSelRow = 0;

    /* Get PCP Selection Row for the Interface and packet Type */
    i4RetStatus = nmhGetFsQoSPcpSelRow (i4Idx, i4PktType, &i4PcpSelRow);
    if (i4RetStatus == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    i4RetStatus = nmhGetFsQoSPcpRowStatus (i4Idx, i4PktType, &i4RowStatus);
    if (i4RetStatus == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    /* Print */
    CliPrintf (CliHandle, "%-28s : %d \r\n", "Port Number", i4Idx);

    switch (i4PktType)
    {
        case QOS_CLI_ETHERNET_PKT:
            CliPrintf (CliHandle, "%-28s : %s\r\n", "Packet Type", "ETHERNET");
            break;

        case QOS_CLI_IP_PKT:
            CliPrintf (CliHandle, "%-28s : %s\r\n", "Packet Type", "IP");
            break;

        case QOS_CLI_MPLS_PKT:
            CliPrintf (CliHandle, "%-28s : %s\r\n", "Packet Type", "MPLS");
            break;

        default:
            CliPrintf (CliHandle, "%-28s : %s\r\n", "Packet Type", "Invalid");
            break;
    }
    switch (i4PcpSelRow)
    {
        case QOS_CLI_8P0D_SEL_ROW:
            CliPrintf (CliHandle, "%-28s : %s\r\n", "PCP Selection Row",
                       "8P0D");
            break;

        case QOS_CLI_7P1D_SEL_ROW:
            CliPrintf (CliHandle, "%-28s : %s\r\n", "PCP Selection Row",
                       "7P1D");
            break;

        case QOS_CLI_6P2D_SEL_ROW:
            CliPrintf (CliHandle, "%-28s : %s\r\n", "PCP Selection Row",
                       "6P2D");
            break;

        case QOS_CLI_5P3D_SEL_ROW:
            CliPrintf (CliHandle, "%-28s : %s\r\n", "PCP Selection Row",
                       "5P3D");
            break;

        default:
            CliPrintf (CliHandle, "%-28s : %s\r\n", "PCP Selection Row",
                       "Invalid");
            break;
    }

    if (i4RowStatus == ACTIVE)
    {
        CliPrintf (CliHandle, "%-28s : %s\r\n", "Status", "Active");
    }
    else
    {
        CliPrintf (CliHandle, "%-28s : %s\r\n", "Status", "InActive");
    }

    CliPrintf (CliHandle, "\r\n");

    return (CLI_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSCliRSPortTblEntry                                 */
/* Description        : This function is used to Reset Port table Entry      */
/*                      Parameter                                            */
/* Input(s)           : i4Idx        - Interface Index                       */
/*                    : i4PktType    - Packet Type                           */
/*                    : i4PcpSelRow  - PCP Sel Row                           */
/*                    : i4RowStatus  - Row Status                            */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : CLI_SUCCESS/CLI_FAILURE                              */
/* Called By          : cli_process_qos_cmd                                  */
/* Calling Function   : SNMP nmh Routines                                    */
/*****************************************************************************/
VOID
QoSCliRSPortTblEntry (INT4 i4Idx, INT4 i4PktType, INT4 i4PcpSelRow,
                      INT4 i4RowStatus)
{
    INT4                i4RetStatus = SNMP_FAILURE;

    if (i4PcpSelRow != (INT4) CLI_RS_INVALID_MASK)
    {
        i4RetStatus = nmhSetFsQoSPcpSelRow (i4Idx, i4PktType, i4PcpSelRow);
    }

    i4RetStatus = nmhSetFsQoSPcpRowStatus (i4Idx, i4PktType, i4RowStatus);

    UNUSED_PARAM (i4RetStatus);

}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : QoSCliShowPcpEncodingTable                       */
/*                                                                           */
/*    Description         : This function is used to show the PCP            */
/*                          Encoding Table for a Port                        */
/*                                                                           */
/*    Input(s)            : CliHandle       - Cli Handle                     */
/*                          u4IfIndex       - If Index                       */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : CLI_SUCCESS/CLI_FAILURE                          */
/*                                                                           */
/*****************************************************************************/
INT4
QoSCliShowPcpEncodingTable (tCliHandle CliHandle, INT4 i4Idx, INT4 i4PktType)
{
    INT4                i4Index = 0;
    INT4                i4PcpSelRow = 0;
    INT4                i4TempVal1, i4TempVal2 = 0;
    UINT1               au1IfName[CFA_MAX_PORT_NAME_LENGTH];
    UINT1               u1Priority = 0;
    UINT1               u1DEI = 0;
    UINT1               u1CurrPcpValue = 0;

    MEMSET (au1IfName, 0, CFA_MAX_PORT_NAME_LENGTH);
    if (CfaCliGetIfName ((UINT4) i4Idx, (INT1 *) au1IfName) == CLI_FAILURE)
    {
        return (CLI_FAILURE);
    }
    CliPrintf (CliHandle, "QoS PCP Encoding Table Entries %s\r\n", au1IfName);
    CliPrintf (CliHandle, "-----------------------------------------\r\n");
    nmhGetFsQoSPcpSelRow (i4Idx, i4PktType, &i4PcpSelRow);
    switch (i4PcpSelRow)
    {
        case QOS_CLI_8P0D_SEL_ROW:
            CliPrintf (CliHandle, "PCP Selection Row: 8P0D \r\n");
            break;
        case QOS_CLI_7P1D_SEL_ROW:
            CliPrintf (CliHandle, "PCP Selection Row: 7P1D \r\n");
            break;
        case QOS_CLI_6P2D_SEL_ROW:
            CliPrintf (CliHandle, "PCP Selection Row: 6P2D \r\n");
            break;
        case QOS_CLI_5P3D_SEL_ROW:
            CliPrintf (CliHandle, "PCP Selection Row: 5P3D \r\n");
            break;
        default:
            break;
    }
    /*Printing the Priority & DE Values */
    switch (i4PktType)
    {
        case QOS_CLI_ETHERNET_PKT:
            CliPrintf (CliHandle, "PCP Packet Type  : %s\r\n", "ETHERNET");
            /*Fetching the PCP Selection Row for the Port Number */
            CliPrintf (CliHandle,
                       "---------------------------------------------------------------");
            CliPrintf (CliHandle, "---------------\r\n");
            CliPrintf (CliHandle, "|DOT1P value: |");
            for (u1Priority = 0; u1Priority <= QOS_IN_PRIORITY_VLAN_PRI_MAX;
                 u1Priority++)
            {
                CliPrintf (CliHandle, "%-3d", u1Priority);
                CliPrintf (CliHandle, "|");
                CliPrintf (CliHandle, "%d%-2s", u1Priority, "DE");
                CliPrintf (CliHandle, "|");

            }
            CliPrintf (CliHandle, "\r\n");
            CliPrintf (CliHandle,
                       "---------------------------------------------------------------");
            CliPrintf (CliHandle, "---------------\r\n");
            CliPrintf (CliHandle, "|PCP value  : |");
            /* Display PCP Encoding Information for the port */
            for (u1Priority = 0; u1Priority <= QOS_IN_PRIORITY_VLAN_PRI_MAX;
                 u1Priority++)
            {
                for (u1DEI = 0; u1DEI <= QOS_IN_PRIORITY_VLAN_DEI_MAX; u1DEI++)
                {
                    for (i4Index = 0; i4Index < QOS_PCP_MAX_ENTRY; i4Index++)
                    {
                        if ((gaQoSPcpEncodingInfo[i4Index].u4IfIndex ==
                             (UINT4) i4Idx)
                            && (gaQoSPcpEncodingInfo[i4Index].u1InPriority ==
                                u1Priority)
                            && (gaQoSPcpEncodingInfo[i4Index].u1InDEI == u1DEI)
                            && (gaQoSPcpEncodingInfo[i4Index].i4PktType ==
                                i4PktType))
                        {
                            u1CurrPcpValue =
                                gaQoSPcpEncodingInfo[i4Index].u1PcpValue;
                            CliPrintf (CliHandle, "%-3d", u1CurrPcpValue);
                            CliPrintf (CliHandle, "|");
                            break;

                        }
                    }
                }
            }
            CliPrintf (CliHandle, "\r\n");
            CliPrintf (CliHandle,
                       "---------------------------------------------------------------");
            CliPrintf (CliHandle, "---------------\r\n");
            break;
        case QOS_CLI_IP_PKT:
            /* Display Incoming IP DSCP Vlaue */
            CliPrintf (CliHandle, "PCP Packet Type  : %s\r\n", "IP");
            CliPrintf (CliHandle,
                       "----------------------------------------------------------------------------\r\n");
            CliPrintf (CliHandle, "|DSCP value:|");
            for (u1Priority = 0; u1Priority <= QOS_IN_PRIORITY_IP_DSCP_MAX;
                 u1Priority++)
            {
                CliPrintf (CliHandle, "%-2d |", u1Priority);
                if ((u1Priority + 1) % 16 == 0)
                {
                    CliPrintf (CliHandle, "\r\n");
                    CliPrintf (CliHandle,
                               "-----------------------------------------------------------------------------\r\n");
                    i4TempVal1 = (u1Priority - 15);
                    CliPrintf (CliHandle, "|PCP value :|");
                    for (i4TempVal2 = i4TempVal1; i4TempVal2 <= u1Priority;
                         i4TempVal2++)
                    {
                        /* Display the Internal Priority mapped for DSCP based on
                         * the Model*/
                        for (i4Index = 0; i4Index < QOS_PCP_MAX_ENTRY;
                             i4Index++)
                        {
                            if ((gaQoSPcpEncodingInfo[i4Index].u4IfIndex ==
                                 (UINT4) i4Idx)
                                && (gaQoSPcpEncodingInfo[i4Index].
                                    u1InPriority == i4TempVal2)
                                && (gaQoSPcpEncodingInfo[i4Index].i4PktType ==
                                    i4PktType))
                            {
                                u1CurrPcpValue =
                                    gaQoSPcpEncodingInfo[i4Index].u1PcpValue;
                                CliPrintf (CliHandle, "%-2d |", u1CurrPcpValue);
                                break;

                            }
                        }
                        if (i4TempVal2 == u1Priority)
                        {
                            CliPrintf (CliHandle, "\r\n");
                            CliPrintf (CliHandle,
                                       "-----------------------------------------------------------------------------\r\n");
                            CliPrintf (CliHandle, "\r\n");
                        }

                    }
                    if (u1Priority != QOS_IN_PRIORITY_IP_DSCP_MAX)
                    {
                        CliPrintf (CliHandle,
                                   "-----------------------------------------------------------------------------\r\n");
                        CliPrintf (CliHandle, "|DSCP value:|");
                    }
                }
            }

            break;
        case QOS_CLI_MPLS_PKT:
            /* Display Incoming MPLS EXP Value */
            CliPrintf (CliHandle, "PCP Packet Type  : %s\r\n", "MPLS");
            CliPrintf (CliHandle,
                       " ---------------------------------------------------\r\n");
            CliPrintf (CliHandle, "|EXP value: ");
            for (u1Priority = 0; u1Priority <= QOS_IN_PRIORITY_MPLS_EXP_MAX;
                 u1Priority++)
            {
                CliPrintf (CliHandle, "%-3d |", u1Priority);

            }
            CliPrintf (CliHandle, "\r\n");
            CliPrintf (CliHandle,
                       " ---------------------------------------------------\r\n");
            CliPrintf (CliHandle, "|PCP value: ");

            /* Display the Internal Priority mapped for DSCP based on
             * the Model*/
            for (u1Priority = 0; u1Priority <= QOS_IN_PRIORITY_MPLS_EXP_MAX;
                 u1Priority++)
            {
                for (i4Index = 0; i4Index < QOS_PCP_MAX_ENTRY; i4Index++)
                {
                    if ((gaQoSPcpEncodingInfo[i4Index].u4IfIndex ==
                         (UINT4) i4Idx)
                        && (gaQoSPcpEncodingInfo[i4Index].u1InPriority ==
                            u1Priority)
                        && (gaQoSPcpEncodingInfo[i4Index].i4PktType ==
                            i4PktType))
                    {
                        u1CurrPcpValue =
                            gaQoSPcpEncodingInfo[i4Index].u1PcpValue;
                        CliPrintf (CliHandle, "%-3d |", u1CurrPcpValue);
                        break;

                    }
                }
            }
            CliPrintf (CliHandle, "\r\n");
            CliPrintf (CliHandle,
                       " ---------------------------------------------------\r\n");

            break;
        default:
            break;
    }
    return (CLI_SUCCESS);

}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : QoSCliShowPcpDecodingTable                       */
/*                                                                           */
/*    Description         : This function is used to show the PCP            */
/*                          Decoding Table for a Port                        */
/*                                                                           */
/*    Input(s)            : CliHandle       - Cli Handle                     */
/*                          u4IfIndex       - If Index                       */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : CLI_SUCCESS/CLI_FAILURE                          */
/*                                                                           */
/*****************************************************************************/
INT4
QoSCliShowPcpDecodingTable (tCliHandle CliHandle, INT4 i4Idx, INT4 i4PktType)
{
    INT4                i4Index = 0;
    INT4                i4PcpSelRow = 0;
    UINT1               au1IfName[CFA_MAX_PORT_NAME_LENGTH];
    UINT1               u1Priority = 0;
    UINT1               u1OutPriority = 0;
    UINT1               u1OutDEI = 0;
    UINT1               u1PcpValue = 0;
    UINT1               u1CurrPcpValue = 0;

    MEMSET (au1IfName, 0, CFA_MAX_PORT_NAME_LENGTH);
    if (CfaCliGetIfName ((UINT4) i4Idx, (INT1 *) au1IfName) == CLI_FAILURE)
    {
        return (CLI_FAILURE);
    }
    CliPrintf (CliHandle, "QoS PCP Decoding Table Entries %s\r\n", au1IfName);
    CliPrintf (CliHandle,
               "----------------------------------------------------\r\n");
    nmhGetFsQoSPcpSelRow (i4Idx, i4PktType, &i4PcpSelRow);
    switch (i4PktType)
    {
        case QOS_CLI_ETHERNET_PKT:
            /*Printing the Priority & DE Values */
            CliPrintf (CliHandle, "|PCP value:  |");
            for (u1Priority = 0; u1Priority <= QOS_IN_PRIORITY_VLAN_PRI_MAX;
                 u1Priority++)
            {
                CliPrintf (CliHandle, "%-4d|", u1Priority);

            }
            CliPrintf (CliHandle, "\r\n");
            CliPrintf (CliHandle,
                       "-----------------------------------------------------\r\n");
            CliPrintf (CliHandle, "|DOT1P value:|");

            for (u1PcpValue = 0; u1PcpValue <= QOS_IN_PRIORITY_VLAN_PRI_MAX;
                 u1PcpValue++)
            {
                for (i4Index = 0; i4Index < QOS_PCP_MAX_ENTRY; i4Index++)
                {
                    if ((gaQoSPcpDecodingInfo[i4Index].u4IfIndex ==
                         (UINT4) i4Idx)
                        && (gaQoSPcpDecodingInfo[i4Index].u1PcpValue ==
                            u1PcpValue))
                    {
                        u1OutPriority =
                            gaQoSPcpDecodingInfo[i4Index].u1OutPriority;
                        u1OutDEI = gaQoSPcpDecodingInfo[i4Index].u1OutDEI;
                        CliPrintf (CliHandle, "%d", u1OutPriority);
                        if (u1OutDEI != 0)
                        {
                            CliPrintf (CliHandle, "%-3s", "DE");
                        }
                        else
                        {
                            CliPrintf (CliHandle, "%-3s", " ");
                        }
                        CliPrintf (CliHandle, "|");
                        break;

                    }
                }
            }
            CliPrintf (CliHandle, "\r\n");
            CliPrintf (CliHandle,
                       "----------------------------------------------------\r\n");
            break;
        case QOS_CLI_IP_PKT:
            CliPrintf (CliHandle,
                       "For IP packet Type, Outgoing DSCP Value will be same as DSCP value in the incoming Packet \r\n");
            break;
        case QOS_CLI_MPLS_PKT:
            /* Display Incoming MPLS EXP Value */
            CliPrintf (CliHandle, "PCP Packet Type  : %s\r\n", "MPLS");
            CliPrintf (CliHandle,
                       " ---------------------------------------------------\r\n");
            CliPrintf (CliHandle, "|PCP value: ");
            for (u1Priority = 0; u1Priority <= QOS_IN_PRIORITY_MPLS_EXP_MAX;
                 u1Priority++)
            {
                CliPrintf (CliHandle, "%-3d |", u1Priority);

            }
            CliPrintf (CliHandle, "\r\n");
            CliPrintf (CliHandle,
                       " ---------------------------------------------------\r\n");
            CliPrintf (CliHandle, "|EXP value: ");

            /* Display the Internal Priority mapped for DSCP based on
             * the Model*/
            for (u1Priority = 0; u1Priority <= QOS_IN_PRIORITY_MPLS_EXP_MAX;
                 u1Priority++)
            {
                for (i4Index = 0; i4Index < QOS_PCP_MAX_ENTRY; i4Index++)
                {
                    if ((gaQoSPcpEncodingInfo[i4Index].u4IfIndex ==
                         (UINT4) i4Idx)
                        && (gaQoSPcpEncodingInfo[i4Index].u1InPriority ==
                            u1Priority)
                        && (gaQoSPcpEncodingInfo[i4Index].i4PktType ==
                            i4PktType))
                    {
                        u1CurrPcpValue =
                            gaQoSPcpEncodingInfo[i4Index].u1PcpValue;
                        CliPrintf (CliHandle, "%-3d |", u1CurrPcpValue);
                        break;

                    }
                }
            }
            CliPrintf (CliHandle, "\r\n");
            CliPrintf (CliHandle,
                       " ---------------------------------------------------\r\n");

            break;
        default:
            break;
    }
    CliPrintf (CliHandle, "\r\n");
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : QoSCliShowPcpQueueMapping                        */
/*                                                                           */
/*    Description         : This function is used to show the Queue Mapping  */
/*                          for a particular PCP Selection Row               */
/*                                                                           */
/*    Input(s)            : CliHandle       - Cli Handle                     */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : CLI_SUCCESS/CLI_FAILURE                          */
/*                                                                           */
/*****************************************************************************/
INT4
QoSCliShowPcpQueueMapping (tCliHandle CliHandle)
{
    INT4                i4PcpSelRow = 0;
    INT4                u1Priority = 0;

    CliPrintf (CliHandle, "QoS PCP Queue Mapping Details\r\n");
    CliPrintf (CliHandle,
               "___________________________________________________\r\n");
    CliPrintf (CliHandle, "|PCP Value:|");
    for (u1Priority = 0; u1Priority <= QOS_IN_PRIORITY_VLAN_PRI_MAX;
         u1Priority++)
    {
        /* Printing the Internal Priority Values for which Queue Mapping is
           to be displayed */
        CliPrintf (CliHandle, "%-3d |", u1Priority);
    }

    CliPrintf (CliHandle, "\r\n");
    CliPrintf (CliHandle,
               "---------------------------------------------------\r\n");
    for (i4PcpSelRow = QOS_CLI_8P0D_SEL_ROW;
         i4PcpSelRow <= QOS_CLI_5P3D_SEL_ROW; i4PcpSelRow++)
    {
        if (i4PcpSelRow == QOS_CLI_8P0D_SEL_ROW)
        {
            CliPrintf (CliHandle, "| 8P0D    :|");
        }
        else if (i4PcpSelRow == QOS_CLI_7P1D_SEL_ROW)
        {
            CliPrintf (CliHandle, "| 7P1D    :|");
        }
        else if (i4PcpSelRow == QOS_CLI_6P2D_SEL_ROW)
        {
            CliPrintf (CliHandle, "| 6P2D    :|");
        }
        else
        {
            CliPrintf (CliHandle, "| 5P3D    :|");
        }

        for (u1Priority = 0; u1Priority <= QOS_IN_PRIORITY_VLAN_PRI_MAX;
             u1Priority++)
        {
            /* Printing the Queue Mapping for the Internal Priority Value */
            CliPrintf (CliHandle, "Q%-2d |", gau1QVal[i4PcpSelRow][u1Priority]);
        }
        CliPrintf (CliHandle, "\r\n");
        CliPrintf (CliHandle,
                   "----------------------------------------------------\r\n");
    }
    return CLI_SUCCESS;
}
#endif /* __QOS_CLI_C__ */
