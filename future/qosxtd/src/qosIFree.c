/*****************************************************************************/
/*    FILE  NAME            : qosIFree.c                                     */
/*    PRINCIPAL AUTHOR      : Aricent Inc.                                   */
/*    MODULE NAME           : QOSX                                           */
/*    LANGUAGE              : C                                              */
/*    TARGET ENVIRONMENT    : Any                                            */
/*    DATE OF FIRST RELEASE :                                                */
/*    AUTHOR                : Aricent Inc.                                   */
/*    DESCRIPTION           : This file contains the exported APIs of        */
/*                            qos module                                     */
/*---------------------------------------------------------------------------*/
#include "qosinc.h"
/*****************************************************************************/
/* Function Name      : QosUpdateDiffServClfrNextFree                        */
/*                                                                           */
/* Description        :                                                      */
/*                                                                           */
/* Input(s)           : u4Index,i4Status                                     */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Return Value(s)    : SUCCESS / FAILURE                                    */
/*****************************************************************************/
INT4
QosUpdateDiffServClfrNextFree (UINT4 u4Index, INT4 i4Status)
{
    UINT2               u2Index = QOS_START_INITIAL_INDEX;
    switch (i4Status)
    {
        case QOS_CREATE_AND_WAIT:
            QOS_CLFR_NEXT_FREE () = QOS_INDEX_FULL;
            QOS_CLFR_COUNT++;
            if (QOS_CLFR_COUNT >= QOS_CLFR_SIZE)
            {
                break;
            }
            for (u2Index = QOS_START_INITIAL_INDEX + QOS_MIN_VALUE;
                 u2Index <= QOS_CLFR_SIZE; u2Index++)
            {
                if (QosGetClfrEntry (u2Index) == NULL)
                {
                    QOS_CLFR_NEXT_FREE () = u2Index;
                    break;
                }
            }
            break;

        case QOS_DESTROY:
            QOS_CLFR_COUNT--;
            if ((QOS_CLFR_NEXT_FREE () == QOS_INDEX_FULL) ||
                (QOS_CLFR_NEXT_FREE () > u4Index))
            {
                QOS_CLFR_NEXT_FREE () = u4Index;
            }
            break;
        default:
            break;

    }
    return QOS_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : QosUpdateDiffServCLASSNextFree                        */
/*                                                                           */
/* Description        :                                                      */
/*                                                                           */
/* Input(s)           : u4Index,i4Status                                     */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Return Value(s)    : SUCCESS / FAILURE                                    */
/*****************************************************************************/
INT4
QosUpdateDiffServCLASSNextFree (UINT4 u4Index, INT4 i4Status)
{
    UINT2               u2Index = QOS_START_INITIAL_INDEX;
    switch (i4Status)
    {
        case QOS_CREATE_AND_WAIT:
            QOS_CLASS_NEXT_FREE () = QOS_INDEX_FULL;
            QOS_CLASS_COUNT++;
            if (QOS_CLASS_COUNT >= QOS_MAX_NUM_OF_CLASSES)
            {
                break;
            }

            for (u2Index = QOS_START_INITIAL_INDEX + QOS_MIN_VALUE;
                 u2Index <= QOS_MAX_NUM_OF_CLASSES; u2Index++)
            {
                if (QoSUtlGetClassInfoNode (u2Index) == NULL)
                {
                    QOS_CLASS_NEXT_FREE () = u2Index;
                    break;
                }
            }
            break;

        case QOS_DESTROY:
            QOS_CLASS_COUNT--;
            if ((QOS_CLASS_NEXT_FREE () == QOS_INDEX_FULL) ||
                (QOS_CLASS_NEXT_FREE () > u4Index))
            {
                QOS_CLASS_NEXT_FREE () = u4Index;
            }
            break;
        default:
            break;

    }
    return QOS_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : QosUpdDSClfrElementNextFree                          */
/*                                                                           */
/* Description        :                                                      */
/*                                                                           */
/* Input(s)           :  u4Index ,i4Status                                   */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Return Value(s)    : SUCCESS / FAILURE                                    */
/*****************************************************************************/
INT4
QosUpdDSClfrElementNextFree (UINT4 u4Index, INT4 i4Status)
{
    UINT2               u2Index = QOS_START_INITIAL_INDEX;
    switch (i4Status)
    {
        case QOS_CREATE_AND_WAIT:
            QOS_CLFR_ELEM_NEXT_FREE () = QOS_INDEX_FULL;
            QOS_CLFR_ELEM_COUNT++;
            if (QOS_CLFR_ELEM_COUNT >= QOS_CLS_MAP_TBL_MAX_ENTRIES)
            {
                break;
            }

            for (u2Index = QOS_START_INITIAL_INDEX + QOS_MIN_VALUE;
                 u2Index <= QOS_END_INDEX; u2Index++)
            {
                if (QoSUtlGetClassMapNode (u2Index) == NULL)
                {
                    QOS_CLFR_ELEM_NEXT_FREE () = u2Index;
                    break;
                }
            }
            break;
        case QOS_DESTROY:
            QOS_CLFR_ELEM_COUNT--;
            if ((QOS_CLFR_ELEM_NEXT_FREE () == QOS_INDEX_FULL) ||
                (QOS_CLFR_ELEM_NEXT_FREE () > u4Index))
            {
                QOS_CLFR_ELEM_NEXT_FREE () = u4Index;
            }
            break;
        default:
            break;
    }
    return QOS_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : QosUpdateDiffServMFCNextFree                         */
/*                                                                           */
/* Description        :                                                      */
/*                                                                           */
/* Input(s)           : u4Index ,i4Status                                    */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Return Value(s)    : SUCCESS / FAILURE                                    */
/*****************************************************************************/
INT4
QosUpdateDiffServMFCNextFree (UINT4 u4IfIndex, INT4 i4Status)
{

    UINT4               u4Index = QOS_START_INITIAL_INDEX;
    switch (i4Status)
    {
        case QOS_CREATE_AND_WAIT:
            QOS_MFC_NEXT_FREE () = QOS_INDEX_FULL;
            QOS_MFC_COUNT++;
            if (QOS_MFC_COUNT >= ISS_MAX_L3_FILTERS)
            {
                break;
            }
            for (u4Index = QOS_MIN_FILTER_ID + QOS_MIN_VALUE;
                 u4Index <= QOS_MAX_FILTER_ID; u4Index++)
            {
                if (QosGetMultiFieldClfrEntry (u4Index) == NULL)
                {
                    QOS_MFC_NEXT_FREE () = u4Index;
                    break;
                }
            }

            break;

        case QOS_DESTROY:
            QOS_MFC_COUNT--;
            if ((QOS_MFC_NEXT_FREE () == QOS_INDEX_FULL) ||
                (QOS_MFC_NEXT_FREE () > u4IfIndex))
            {
                QOS_MFC_NEXT_FREE () = u4IfIndex;
            }
            break;
        default:
            break;
    }
    return QOS_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : QosUpdateDiffServMeterNextFree                       */
/*                                                                           */
/* Description        :                                                      */
/*                                                                           */
/* Input(s)           :  u4Index ,i4Status                                   */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Return Value(s)    : SUCCESS / FAILURE                                    */
/*****************************************************************************/
INT4
QosUpdateDiffServMeterNextFree (UINT4 u4Index, INT4 i4Status)
{

    UINT2               u2Index = QOS_START_INITIAL_INDEX;
    switch (i4Status)
    {
        case QOS_CREATE_AND_WAIT:
            QOS_METER_NEXT_FREE () = QOS_INDEX_FULL;
            QOS_METER_COUNT++;
            if (QOS_METER_COUNT >= QOS_PLY_MAP_TBL_MAX_ENTRIES)
            {
                break;
            }
            for (u2Index = QOS_START_INITIAL_INDEX + QOS_MIN_VALUE;
                 u2Index <= QOS_PLY_MAP_TBL_MAX_ENTRIES; u2Index++)
            {
                if (QoSUtlGetStdMeterNode (u2Index) == NULL)
                {
                    QOS_METER_NEXT_FREE () = u2Index;
                    break;
                }
            }
            break;
        case QOS_DESTROY:
            QOS_METER_COUNT--;
            if ((QOS_METER_NEXT_FREE () == QOS_INDEX_FULL) ||
                (QOS_METER_NEXT_FREE () > u4Index))
            {
                QOS_METER_NEXT_FREE () = u4Index;
            }
            break;
        default:
            break;
    }
    return QOS_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : QosUpdateDiffServTBParamNextFree                     */
/*                                                                           */
/* Description        :                                                      */
/*                                                                           */
/* Input(s)           :  u4Index ,i4Status                                   */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Return Value(s)    : SUCCESS / FAILURE                                    */
/*****************************************************************************/
INT4
QosUpdateDiffServTBParamNextFree (UINT4 u4Index, INT4 i4Status)
{

    UINT2               u2Index = QOS_START_INITIAL_INDEX;
    switch (i4Status)
    {
        case QOS_CREATE_AND_WAIT:
            QOS_TBPARM_NEXT_FREE () = QOS_INDEX_FULL;
            QOS_TBPARM_COUNT++;
            if (QOS_TBPARM_COUNT >= QOS_METER_TBL_MAX_ENTRIES)
            {
                break;
            }
            for (u2Index = QOS_START_INITIAL_INDEX + QOS_MIN_VALUE;
                 u2Index <= QOS_METER_TBL_MAX_ENTRIES; u2Index++)
            {
                if (QoSUtlGetMeterNode (u2Index) == NULL)
                {
                    QOS_TBPARM_NEXT_FREE () = u2Index;
                    break;
                }
            }
            break;
        case QOS_DESTROY:
            QOS_TBPARM_COUNT--;
            if ((QOS_TBPARM_NEXT_FREE () == QOS_INDEX_FULL) ||
                (QOS_TBPARM_NEXT_FREE () > u4Index))
            {
                QOS_TBPARM_NEXT_FREE () = u4Index;
            }
            break;
        default:
            break;
    }
    return QOS_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : QosUpdateDiffServActionNextFree                      */
/*                                                                           */
/* Description        :                                                      */
/*                                                                           */
/* Input(s)           :  u4Index ,i4Status                                   */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Return Value(s)    : SUCCESS / FAILURE                                    */
/*****************************************************************************/
INT4
QosUpdateDiffServActionNextFree (UINT4 u4Index, INT4 i4Status)
{

    UINT2               u2Index = QOS_START_INITIAL_INDEX;
    switch (i4Status)
    {
        case QOS_CREATE_AND_WAIT:
            QOS_ACTION_NEXT_FREE () = QOS_INDEX_FULL;
            QOS_ACTION_COUNT++;
            if (QOS_ACTION_COUNT >= QOS_ACTION_TBL_MAX_ENTRIES)
            {
                break;
            }
            for (u2Index = QOS_START_INITIAL_INDEX + QOS_MIN_VALUE;
                 u2Index <= QOS_ACTION_TBL_MAX_ENTRIES; u2Index++)
            {
                if (QoSUtlGetActionNode (u2Index) == NULL)
                {
                    QOS_ACTION_NEXT_FREE () = u2Index;
                    break;
                }
            }
            break;
        case QOS_DESTROY:
            QOS_ACTION_COUNT--;
            if ((QOS_ACTION_NEXT_FREE () == QOS_INDEX_FULL) ||
                (QOS_ACTION_NEXT_FREE () > u4Index))
            {
                QOS_ACTION_NEXT_FREE () = u4Index;
            }
            break;
        default:
            break;
    }
    return QOS_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : QosUpdateDiffServCountActNextFree                    */
/*                                                                           */
/* Description        :                                                      */
/*                                                                           */
/* Input(s)           :  u4Index ,i4Status                                   */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Return Value(s)    : SUCCESS / FAILURE                                    */
/*****************************************************************************/
INT4
QosUpdateDiffServCountActNextFree (UINT4 u4Index, INT4 i4Status)
{

    UINT2               u2Index = QOS_START_INITIAL_INDEX;
    switch (i4Status)
    {
        case QOS_CREATE_AND_WAIT:
            QOS_CNTACT_NEXT_FREE () = QOS_INDEX_FULL;
            QOS_CNTACT_COUNT++;
            if (QOS_CNTACT_COUNT >= QOS_COUNTACT_TBL_MAX_ENTRIES)
            {
                break;
            }
            for (u2Index = QOS_START_INITIAL_INDEX + QOS_MIN_VALUE;
                 u2Index <= QOS_COUNTACT_TBL_MAX_ENTRIES; u2Index++)
            {
                if (QoSUtlGetCountActionNode (u2Index) == NULL)
                {
                    QOS_CNTACT_NEXT_FREE () = u2Index;
                    break;
                }
            }
            break;
        case QOS_DESTROY:
            QOS_CNTACT_COUNT--;
            if ((QOS_CNTACT_NEXT_FREE () == QOS_INDEX_FULL) ||
                (QOS_CNTACT_NEXT_FREE () > u4Index))
            {
                QOS_CNTACT_NEXT_FREE () = u4Index;
            }
            break;
        default:
            break;
    }
    return QOS_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : QosUpdateDiffServAlgoDropNextFree                    */
/*                                                                           */
/* Description        :                                                      */
/*                                                                           */
/* Input(s)           :  u4Index ,i4Status                                   */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Return Value(s)    : SUCCESS / FAILURE                                    */
/*****************************************************************************/
INT4
QosUpdateDiffServAlgoDropNextFree (UINT4 u4Index, INT4 i4Status)
{

    UINT2               u2Index = QOS_START_INITIAL_INDEX;
    switch (i4Status)
    {
        case QOS_CREATE_AND_WAIT:
            QOS_ALGDROP_NEXT_FREE () = QOS_INDEX_FULL;
            QOS_ALGDROP_COUNT++;
            if (QOS_ALGDROP_COUNT >= QOS_Q_TEMP_TBL_MAX_ENTRIES)
            {
                break;
            }
            for (u2Index = QOS_START_INITIAL_INDEX + QOS_MIN_VALUE;
                 u2Index <= QOS_Q_TEMP_TBL_MAX_ENTRIES; u2Index++)
            {
                if (QoSUtlGetQTempNode (u2Index) == NULL)

                {
                    QOS_ALGDROP_NEXT_FREE () = u2Index;
                    break;
                }
            }
            break;
        case QOS_DESTROY:
            QOS_ALGDROP_COUNT--;
            if ((QOS_ALGDROP_NEXT_FREE () == QOS_INDEX_FULL) ||
                (QOS_ALGDROP_NEXT_FREE () > u4Index))
            {
                QOS_ALGDROP_NEXT_FREE () = u4Index;
            }
            break;
        default:
            break;
    }
    return QOS_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : QosUpdateDiffServRandomDropNextFree                  */
/*                                                                           */
/* Description        :                                                      */
/*                                                                           */
/* Input(s)           :  u4Index ,i4Status                                   */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Return Value(s)    : SUCCESS / FAILURE                                    */
/*****************************************************************************/
INT4
QosUpdateDiffServRandomDropNextFree (UINT4 u4Index, INT4 i4Status)
{

    UINT2               u2Index = QOS_START_INITIAL_INDEX;
    switch (i4Status)
    {
        case QOS_CREATE_AND_WAIT:
            QOS_RD_NEXT_FREE () = QOS_INDEX_FULL;
            QOS_RD_COUNT++;
            if (QOS_RD_COUNT >= QOS_RD_TBL_MAX_ENTRIES)
            {
                break;
            }
            for (u2Index = QOS_START_INITIAL_INDEX + QOS_MIN_VALUE;
                 u2Index <= QOS_RD_TBL_MAX_ENTRIES; u2Index++)
            {
                if (QoSUtlGetRDCfgNode (u2Index, QOS_START_INITIAL_INDEX)
                    == NULL)
                {
                    QOS_RD_NEXT_FREE () = u2Index;
                    break;
                }
            }
            break;
        case QOS_DESTROY:
            QOS_RD_COUNT--;
            if ((QOS_RD_NEXT_FREE () == QOS_INDEX_FULL) ||
                (QOS_RD_NEXT_FREE () > u4Index))
            {
                QOS_RD_NEXT_FREE () = u4Index;
            }
            break;
        default:
            break;
    }
    return QOS_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : QosUpdateDiffServQNextFree                           */
/*                                                                           */
/* Description        :                                                      */
/*                                                                           */
/* Input(s)           :  u4Index ,i4Status                                   */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Return Value(s)    : SUCCESS / FAILURE                                    */
/*****************************************************************************/
INT4
QosUpdateDiffServQNextFree (UINT4 u4Index, INT4 i4Status)
{

    UINT2               u2Index = QOS_START_INITIAL_INDEX;

    switch (i4Status)
    {
        case QOS_CREATE_AND_WAIT:
            QOS_Q_NEXT_FREE () = QOS_INDEX_FULL;
            QOS_Q_COUNT++;
            if (QOS_Q_COUNT >= QOS_Q_TBL_MAX_ENTRIES)
            {
                break;
            }
            for (u2Index = QOS_START_INITIAL_INDEX + QOS_MIN_VALUE;
                 u2Index <= QOS_Q_TBL_MAX_ENTRIES; u2Index++)
            {
                if (QosGetQEntry (u2Index) == NULL)
                {
                    QOS_Q_NEXT_FREE () = u2Index;
                    break;
                }
            }
            break;
        case QOS_DESTROY:
            QOS_Q_COUNT--;
            if ((QOS_Q_NEXT_FREE () == QOS_INDEX_FULL) ||
                (QOS_Q_NEXT_FREE () > u4Index))
            {
                QOS_Q_NEXT_FREE () = u4Index;
            }
            break;
        default:
            break;
    }
    return QOS_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : QosUpdateDiffServSchedulerNextFree                      */
/*                                                                           */
/* Description        :                                                      */
/*                                                                           */
/* Input(s)           :  u4Index ,i4Status                                   */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Return Value(s)    : SUCCESS / FAILURE                                    */
/*****************************************************************************/
INT4
QosUpdateDiffServSchedulerNextFree (UINT4 u4Index, INT4 i4Status)
{

    UINT4               u4SchedIndex = QOS_START_INITIAL_INDEX;
    UINT4               u4IfIndex;
    UINT4               u4SchedId;

    switch (i4Status)
    {
        case QOS_CREATE_AND_WAIT:
            QOS_SCHED_NEXT_FREE () = QOS_INDEX_FULL;
            QOS_SCHED_COUNT++;
            if (QOS_SCHED_COUNT >= QOS_SCHED_TBL_MAX_ENTRIES)
            {
                break;
            }
            for (u4SchedIndex = QOS_START_INITIAL_INDEX + QOS_MIN_VALUE;
                 u4SchedIndex <= QOS_SCHED_TBL_MAX_ENTRIES; u4SchedIndex++)
            {
                if (QosSchedulerGetIfIdFromGlobalId
                    (u4SchedIndex, &u4IfIndex, &u4SchedId) == QOS_FAILURE)
                {
                    QOS_SCHED_NEXT_FREE () = u4SchedIndex;
                    break;
                }
            }
            break;

        case QOS_DESTROY:
            QOS_SCHED_COUNT--;
            if ((QOS_SCHED_NEXT_FREE () == QOS_INDEX_FULL) ||
                (QOS_SCHED_NEXT_FREE () > u4Index))
            {
                QOS_SCHED_NEXT_FREE () = u4Index;
            }
            break;
        default:
            break;
    }

    return QOS_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : QosUpdateDiffServMinRateNextFree                        */
/*                                                                           */
/* Description        :                                                      */
/*                                                                           */
/* Input(s)           :  u4Index ,i4Status                                   */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Return Value(s)    : SUCCESS / FAILURE                                    */
/*****************************************************************************/
INT4
QosUpdateDiffServMinRateNextFree (UINT4 u4Index, INT4 i4Status)
{

    UINT2               u2Index = QOS_START_INITIAL_INDEX;
    switch (i4Status)
    {
        case QOS_CREATE_AND_WAIT:
            QOS_MINRATE_NEXT_FREE () = QOS_INDEX_FULL;
            QOS_MINRATE_COUNT++;
            if (QOS_MINRATE_COUNT >= QOS_SHAPE_TEMP_TBL_MAX_ENTRIES)
            {
                break;
            }
            for (u2Index = QOS_START_INITIAL_INDEX + QOS_MIN_VALUE;
                 u2Index <= QOS_SHAPE_TEMP_TBL_MAX_ENTRIES; u2Index++)
            {
                if (QosGetMinRateEntry (u2Index) == NULL)
                {
                    QOS_MINRATE_NEXT_FREE () = u2Index;
                    break;
                }
            }
            break;
        case QOS_DESTROY:
            QOS_MINRATE_COUNT--;
            if ((QOS_MINRATE_NEXT_FREE () == QOS_INDEX_FULL) ||
                (QOS_MINRATE_NEXT_FREE () > u4Index))
            {
                QOS_MINRATE_NEXT_FREE () = u4Index;
            }
            break;
        default:
            break;
    }
    return QOS_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : QosUpdateDiffServMaxRateNextFree                        */
/*                                                                           */
/* Description        :                                                      */
/*                                                                           */
/* Input(s)           :  u4Index ,i4Status                                   */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Return Value(s)    : SUCCESS / FAILURE                                    */
/*****************************************************************************/
INT4
QosUpdateDiffServMaxRateNextFree (UINT4 u4Index, UINT4 u4Level, INT4 i4Status)
{

    UINT2               u2Index = QOS_START_INITIAL_INDEX;
    UNUSED_PARAM (u4Level);
    switch (i4Status)
    {
        case QOS_CREATE_AND_WAIT:
            QOS_MAXRATE_NEXT_FREE () = QOS_INDEX_FULL;
            QOS_MAXRATE_COUNT++;
            if (QOS_MAXRATE_COUNT >= QOS_SHAPE_TEMP_TBL_MAX_ENTRIES)
            {
                break;
            }
            for (u2Index = QOS_START_INITIAL_INDEX + QOS_MIN_VALUE;
                 u2Index <= QOS_SHAPE_TEMP_TBL_MAX_ENTRIES; u2Index++)
            {
                if (QosGetMaxRateEntry (u2Index, 2) == NULL)
                {
                    QOS_MAXRATE_NEXT_FREE () = u2Index;
                    break;
                }
            }
            break;
        case QOS_DESTROY:
            QOS_MAXRATE_COUNT--;
            if ((QOS_MAXRATE_NEXT_FREE () == QOS_INDEX_FULL) ||
                (QOS_MAXRATE_NEXT_FREE () > u4Index))
            {
                QOS_MAXRATE_NEXT_FREE () = u4Index;
            }
            break;
        default:
            break;
    }
    return QOS_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : QosShapeTemplateUpdateNextFree                      */
/*                                                                           */
/* Description        :                                                      */
/*                                                                           */
/* Input(s)           :  u4Index ,i4Status                                   */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Return Value(s)    : SUCCESS / FAILURE                                    */
/*****************************************************************************/
INT4
QosShapeTemplateUpdateNextFree (UINT4 u4Index, INT4 i4Status)
{

    UINT2               u2Index = QOS_START_INITIAL_INDEX;
    switch (i4Status)
    {
        case QOS_CREATE_AND_WAIT:
            QOS_TEMPLATE_NEXT_FREE () = QOS_INDEX_FULL;
            QOS_TEMPLATE_COUNT++;
            if (QOS_TEMPLATE_COUNT >= QOS_SHAPE_TEMP_TBL_MAX_ENTRIES)
            {
                break;
            }

            for (u2Index = QOS_START_INITIAL_INDEX + QOS_MIN_VALUE;
                 u2Index <= QOS_SHAPE_TEMP_TBL_MAX_ENTRIES; u2Index++)
            {
                if (QoSUtlGetShapeTempNode ((UINT4) u2Index) == NULL)
                {
                    QOS_TEMPLATE_NEXT_FREE () = u2Index;
                    break;
                }
            }
            break;
        case QOS_DESTROY:
            QOS_TEMPLATE_COUNT--;
            if ((QOS_TEMPLATE_NEXT_FREE () == QOS_INDEX_FULL) ||
                (QOS_TEMPLATE_NEXT_FREE () > u4Index))
            {
                QOS_TEMPLATE_NEXT_FREE () = u4Index;
            }
            break;
        default:
            break;
    }
    return QOS_SUCCESS;
}
