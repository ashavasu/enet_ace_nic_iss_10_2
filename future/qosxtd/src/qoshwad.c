/* $Id: qoshwad.c,v 1.10 2014/07/24 10:33:17 siva Exp $ */

#include "qosinc.h"
/* -------------------------------------------------------------
 *
 * Function: NpSyncQoSHwInit
 *
 * -------------------------------------------------------------
 */
PUBLIC INT4
NpSyncQoSHwInit (VOID)
{
    unNpSync            NpSync;
    INT4                i4RetVal = FNP_SUCCESS;
    UINT4               u4NodeState = QOS_INIT_VAL;

    MEMSET (&NpSync, QOS_INIT_VAL, sizeof (NpSync));
    u4NodeState = QosPortRmGetNodeState ();
    if (u4NodeState == RM_ACTIVE)
    {
        if (QosPortRmGetStandbyNodeCount () != QOS_INIT_VAL)
        {
            if (QOS_NPSYNC_BLK () == OSIX_FALSE)
            {
                NpSyncQoSHwInitSync (RM_QOS_APP_ID, NPSYNC_QO_S_HW_INIT);
            }
        }
#ifdef NPAPI_WANTED
#undef QosxQoSHwInit
        i4RetVal = QosxQoSHwInit ();

        if (i4RetVal == FNP_FAILURE)
        {
            if (QosPortRmGetStandbyNodeCount () != QOS_INIT_VAL)
            {
                if (QOS_NPSYNC_BLK () == OSIX_FALSE)
                {
                    NpSyncQoSHwInitSync (RM_QOS_APP_ID, NPSYNC_QO_S_HW_INIT);
                }
            }
        }
#endif
    }
    else if (u4NodeState == RM_STANDBY)
    {
        if (QOS_NPSYNC_BLK () == OSIX_FALSE)
        {
            QosHwAuditCreateOrFlushBuffer (&NpSync, NPSYNC_QO_S_HW_INIT,
                                           QOS_INIT_VAL);
        }
    }
    return (i4RetVal);
}

/* -------------------------------------------------------------
 *
 * Function: NpSyncQoSHwMapClassToPolicy
 *
 * -------------------------------------------------------------
 */
PUBLIC INT4
NpSyncQoSHwMapClassToPolicy (tQoSClassMapEntry * pClsMapEntry,
                             tQoSPolicyMapEntry * pPlyMapEntry,
                             tQoSInProfileActionEntry * pInProActEntry,
                             tQoSOutProfileActionEntry * pOutProActEntry,
                             tQoSMeterEntry * pMeterEntry, UINT1 u1Flag)
{
    unNpSync            NpSync;
    INT4                i4RetVal = FNP_SUCCESS;
    UINT4               u4NodeState = QOS_INIT_VAL;

    MEMSET (&NpSync, QOS_INIT_VAL, sizeof (NpSync));
    UNUSED_PARAM (u1Flag);
    if (pClsMapEntry != NULL)
    {
        if (pClsMapEntry->pL2FilterPtr != NULL)
        {
            NpSync.QoSHwMapClassToPolicy.ClassMapNode.u4L2FilterId =
                pClsMapEntry->pL2FilterPtr->i4IssL2FilterNo;
        }
        if (pClsMapEntry->pL3FilterPtr != NULL)
        {
            NpSync.QoSHwMapClassToPolicy.ClassMapNode.u4L3FilterId =
                pClsMapEntry->pL3FilterPtr->i4IssL3FilterNo;
        }
        if (pClsMapEntry->pPriorityMapPtr != NULL)
        {
            NpSync.QoSHwMapClassToPolicy.ClassMapNode.u4PriorityMapId =
                pClsMapEntry->pPriorityMapPtr->u4QoSPriorityMapId;
        }
        NpSync.QoSHwMapClassToPolicy.ClassMapNode.u4ClassId =
            pClsMapEntry->u4QoSMFClass;
    }
    if (pPlyMapEntry != NULL)
    {

        NpSync.QoSHwMapClassToPolicy.u4PlyMapId =
            pPlyMapEntry->i4QoSPolicyMapId;
    }
    u4NodeState = QosPortRmGetNodeState ();
    if (u4NodeState == RM_ACTIVE)
    {
        if (QosPortRmGetStandbyNodeCount () != QOS_INIT_VAL)
        {
            if (QOS_NPSYNC_BLK () == OSIX_FALSE)
            {
                NpSyncQoSHwMapClassToPolicySync (NpSync.QoSHwMapClassToPolicy,
                                                 RM_QOS_APP_ID,
                                                 NPSYNC_QO_S_HW_MAP_CLASS_TO_POLICY);
            }
        }
#ifdef NPAPI_WANTED
#undef QosxQoSHwMapClassToPolicy
        i4RetVal = QosxQoSHwMapClassToPolicy (pClsMapEntry, pPlyMapEntry,
                                              pInProActEntry, pOutProActEntry,
                                              pMeterEntry, u1Flag);
        if (i4RetVal == FNP_FAILURE)
        {
            if (QosPortRmGetStandbyNodeCount () != QOS_INIT_VAL)
            {
                if (QOS_NPSYNC_BLK () == OSIX_FALSE)
                {
                    NpSyncQoSHwMapClassToPolicySync
                        (NpSync.QoSHwMapClassToPolicy, RM_QOS_APP_ID,
                         NPSYNC_QO_S_HW_MAP_CLASS_TO_POLICY);
                }
            }
        }
#endif
        UNUSED_PARAM (pInProActEntry);
        UNUSED_PARAM (pOutProActEntry);
        UNUSED_PARAM (pMeterEntry);
    }
    else if (u4NodeState == RM_STANDBY)
    {
        if (QOS_NPSYNC_BLK () == OSIX_FALSE)
        {
            QosHwAuditCreateOrFlushBuffer (&NpSync,
                                           NPSYNC_QO_S_HW_MAP_CLASS_TO_POLICY,
                                           QOS_INIT_VAL);
        }
    }
    return (i4RetVal);
}

/* -------------------------------------------------------------
 *
 * Function: NpSyncQoSHwUpdatePolicyMapForClass
 *
 * -------------------------------------------------------------
 */
PUBLIC INT4
NpSyncQoSHwUpdatePolicyMapForClass (tQoSClassMapEntry * pClsMapEntry,
                                    tQoSPolicyMapEntry * pPlyMapEntry,
                                    tQoSInProfileActionEntry * pInProActEntry,
                                    tQoSOutProfileActionEntry * pOutProActEntry,
                                    tQoSMeterEntry * pMeterEntry, UINT1 u1Flag)
{
    unNpSync            NpSync;
    INT4                i4RetVal = FNP_SUCCESS;
    UINT4               u4NodeState = QOS_INIT_VAL;
    UNUSED_PARAM (u1Flag);
    MEMSET (&NpSync, QOS_INIT_VAL, sizeof (NpSync));
    if (pClsMapEntry != NULL)
    {

        if (pClsMapEntry->pL2FilterPtr != NULL)
        {
            NpSync.QoSHwUpdatePolicyMapForClass.ClassMapNode.u4L2FilterId =
                pClsMapEntry->pL2FilterPtr->i4IssL2FilterNo;
        }
        if (pClsMapEntry->pL3FilterPtr != NULL)
        {
            NpSync.QoSHwUpdatePolicyMapForClass.ClassMapNode.u4L3FilterId =
                pClsMapEntry->pL3FilterPtr->i4IssL3FilterNo;
        }
        if (pClsMapEntry->pPriorityMapPtr != NULL)
        {
            NpSync.QoSHwUpdatePolicyMapForClass.ClassMapNode.u4PriorityMapId =
                pClsMapEntry->pPriorityMapPtr->u4QoSPriorityMapId;
        }
        NpSync.QoSHwUpdatePolicyMapForClass.ClassMapNode.u4ClassId =
            pClsMapEntry->u4QoSMFClass;
    }
    if (pPlyMapEntry != NULL)
    {

        NpSync.QoSHwUpdatePolicyMapForClass.u4PlyMapId =
            pPlyMapEntry->i4QoSPolicyMapId;
    }
    u4NodeState = QosPortRmGetNodeState ();
    if (u4NodeState == RM_ACTIVE)
    {
        if (QosPortRmGetStandbyNodeCount () != QOS_INIT_VAL)
        {
            if (QOS_NPSYNC_BLK () == OSIX_FALSE)
            {
                NpSyncQoSHwUpdatePolicyMapForClassSync
                    (NpSync.QoSHwUpdatePolicyMapForClass, RM_QOS_APP_ID,
                     NPSYNC_QO_S_HW_UPDATE_POLICY_MAP_FOR_CLASS);
            }
        }
#ifdef NPAPI_WANTED
#undef QosxQoSHwUpdatePolicyMapForClass
        i4RetVal = QosxQoSHwUpdatePolicyMapForClass (pClsMapEntry, pPlyMapEntry,
                                                     pInProActEntry,
                                                     pOutProActEntry,
                                                     pMeterEntry, u1Flag);
        if (i4RetVal == FNP_FAILURE)
        {
            if (QosPortRmGetStandbyNodeCount () != QOS_INIT_VAL)
            {
                if (QOS_NPSYNC_BLK () == OSIX_FALSE)
                {
                    NpSyncQoSHwUpdatePolicyMapForClassSync
                        (NpSync.QoSHwUpdatePolicyMapForClass, RM_QOS_APP_ID,
                         NPSYNC_QO_S_HW_UPDATE_POLICY_MAP_FOR_CLASS);
                }
            }
        }
#endif
        UNUSED_PARAM (pInProActEntry);
        UNUSED_PARAM (pOutProActEntry);
        UNUSED_PARAM (pMeterEntry);
    }
    else if (u4NodeState == RM_STANDBY)
    {
        if (QOS_NPSYNC_BLK () == OSIX_FALSE)
        {
            QosHwAuditCreateOrFlushBuffer (&NpSync,
                                           NPSYNC_QO_S_HW_UPDATE_POLICY_MAP_FOR_CLASS,
                                           QOS_INIT_VAL);
        }
    }
    return (i4RetVal);
}

/* -------------------------------------------------------------
 *
 * Function: NpSyncQoSHwUnmapClassFromPolicy
 *
 * -------------------------------------------------------------
 */
PUBLIC INT4
NpSyncQoSHwUnmapClassFromPolicy (tQoSClassMapEntry * pClsMapEntry,
                                 tQoSPolicyMapEntry * pPlyMapEntry,
                                 tQoSMeterEntry * pMeterEntry, UINT1 u1Flag)
{
    unNpSync            NpSync;
    INT4                i4RetVal = FNP_SUCCESS;
    UINT4               u4NodeState = QOS_INIT_VAL;
    UNUSED_PARAM (u1Flag);
    MEMSET (&NpSync, QOS_INIT_VAL, sizeof (NpSync));
    if (pClsMapEntry != NULL)
    {

        if (pClsMapEntry->pL2FilterPtr != NULL)
        {
            NpSync.QoSHwUnmapClassFromPolicy.ClassMapNode.u4L2FilterId =
                pClsMapEntry->pL2FilterPtr->i4IssL2FilterNo;
        }
        if (pClsMapEntry->pL3FilterPtr != NULL)
        {
            NpSync.QoSHwUnmapClassFromPolicy.ClassMapNode.u4L3FilterId =
                pClsMapEntry->pL3FilterPtr->i4IssL3FilterNo;
        }
        if (pClsMapEntry->pPriorityMapPtr != NULL)
        {
            NpSync.QoSHwUnmapClassFromPolicy.ClassMapNode.u4PriorityMapId =
                pClsMapEntry->pPriorityMapPtr->u4QoSPriorityMapId;
        }
        NpSync.QoSHwUnmapClassFromPolicy.ClassMapNode.u4ClassId =
            pClsMapEntry->u4QoSMFClass;
    }
    if (pPlyMapEntry != NULL)
    {

        NpSync.QoSHwUnmapClassFromPolicy.u4PlyMapId =
            pPlyMapEntry->i4QoSPolicyMapId;
    }
    u4NodeState = QosPortRmGetNodeState ();
    if (u4NodeState == RM_ACTIVE)
    {
        if (QosPortRmGetStandbyNodeCount () != QOS_INIT_VAL)
        {
            if (QOS_NPSYNC_BLK () == OSIX_FALSE)
            {
                NpSyncQoSHwUnmapClassFromPolicySync
                    (NpSync.QoSHwUnmapClassFromPolicy,
                     RM_QOS_APP_ID, NPSYNC_QO_S_HW_UNMAP_CLASS_FROM_POLICY);
            }
        }
#ifdef NPAPI_WANTED
#undef QosxQoSHwUnmapClassFromPolicy
        i4RetVal = QosxQoSHwUnmapClassFromPolicy (pClsMapEntry, pPlyMapEntry,
                                                  pMeterEntry, u1Flag);
        if (i4RetVal == FNP_FAILURE)
        {
            if (QosPortRmGetStandbyNodeCount () != QOS_INIT_VAL)
            {
                if (QOS_NPSYNC_BLK () == OSIX_FALSE)
                {
                    NpSyncQoSHwUnmapClassFromPolicySync
                        (NpSync.QoSHwUnmapClassFromPolicy,
                         RM_QOS_APP_ID, NPSYNC_QO_S_HW_UNMAP_CLASS_FROM_POLICY);
                }
            }
        }
#endif
        UNUSED_PARAM (pMeterEntry);
    }
    else if (u4NodeState == RM_STANDBY)
    {
        if (QOS_NPSYNC_BLK () == OSIX_FALSE)
        {
            QosHwAuditCreateOrFlushBuffer
                (&NpSync, NPSYNC_QO_S_HW_UNMAP_CLASS_FROM_POLICY, QOS_INIT_VAL);
        }
    }
    return (i4RetVal);
}

/* -------------------------------------------------------------
 *
 * Function: NpSyncQoSHwDeleteClassMapEntry
 *
 * -------------------------------------------------------------
 */
PUBLIC INT4
NpSyncQoSHwDeleteClassMapEntry (tQoSClassMapEntry * pClsMapEntry)
{
    unNpSync            NpSync;
    INT4                i4RetVal = FNP_SUCCESS;
    UINT4               u4NodeState = QOS_INIT_VAL;

    MEMSET (&NpSync, QOS_INIT_VAL, sizeof (NpSync));
    if (pClsMapEntry != NULL)
    {

        if (pClsMapEntry->pL2FilterPtr != NULL)
        {
            NpSync.QoSHwDeleteClassMapEntry.ClassMapNode.u4L2FilterId =
                pClsMapEntry->pL2FilterPtr->i4IssL2FilterNo;
        }
        if (pClsMapEntry->pL3FilterPtr != NULL)
        {
            NpSync.QoSHwDeleteClassMapEntry.ClassMapNode.u4L3FilterId =
                pClsMapEntry->pL3FilterPtr->i4IssL3FilterNo;
        }
        if (pClsMapEntry->pPriorityMapPtr != NULL)
        {
            NpSync.QoSHwDeleteClassMapEntry.ClassMapNode.u4PriorityMapId =
                pClsMapEntry->pPriorityMapPtr->u4QoSPriorityMapId;
        }
        NpSync.QoSHwDeleteClassMapEntry.ClassMapNode.u4ClassId =
            pClsMapEntry->u4QoSMFClass;
    }
    u4NodeState = QosPortRmGetNodeState ();
    if (u4NodeState == RM_ACTIVE)
    {
        if (QosPortRmGetStandbyNodeCount () != QOS_INIT_VAL)
        {
            if (QOS_NPSYNC_BLK () == OSIX_FALSE)
            {
                NpSyncQoSHwDeleteClassMapEntrySync (NpSync.
                                                    QoSHwDeleteClassMapEntry,
                                                    RM_QOS_APP_ID,
                                                    NPSYNC_QO_S_HW_DELETE_CLASS_MAP_ENTRY);
            }
        }
#ifdef NPAPI_WANTED
#undef QosxQoSHwDeleteClassMapEntry
        i4RetVal = QosxQoSHwDeleteClassMapEntry (pClsMapEntry);
        if (i4RetVal == FNP_FAILURE)
        {
            if (QosPortRmGetStandbyNodeCount () != QOS_INIT_VAL)
            {
                if (QOS_NPSYNC_BLK () == OSIX_FALSE)
                {
                    NpSyncQoSHwDeleteClassMapEntrySync (NpSync.
                                                        QoSHwDeleteClassMapEntry,
                                                        RM_QOS_APP_ID,
                                                        NPSYNC_QO_S_HW_DELETE_CLASS_MAP_ENTRY);
                }
            }
        }
#endif
    }
    else if (u4NodeState == RM_STANDBY)
    {
        if (QOS_NPSYNC_BLK () == OSIX_FALSE)
        {
            QosHwAuditCreateOrFlushBuffer (&NpSync,
                                           NPSYNC_QO_S_HW_DELETE_CLASS_MAP_ENTRY,
                                           QOS_INIT_VAL);
        }
    }
    return (i4RetVal);
}

/* -------------------------------------------------------------
 *
 * Function: NpSyncQoSHwMeterCreate
 *
 * -------------------------------------------------------------
 */
PUBLIC INT4
NpSyncQoSHwMeterCreate (tQoSMeterEntry * pMeterEntry)
{
    unNpSync            NpSync;
    INT4                i4RetVal = FNP_SUCCESS;
    UINT4               u4NodeState = QOS_INIT_VAL;
    MEMSET (&NpSync, QOS_INIT_VAL, sizeof (NpSync));
    if (pMeterEntry != NULL)
    {

        NpSync.QoSHwMeterCreate.u4MeterId = pMeterEntry->i4QoSMeterId;
    }
    u4NodeState = QosPortRmGetNodeState ();
    if (u4NodeState == RM_ACTIVE)
    {
        if (QosPortRmGetStandbyNodeCount () != QOS_INIT_VAL)
        {
            if (QOS_NPSYNC_BLK () == OSIX_FALSE)
            {
                NpSyncQoSHwMeterCreateSync (NpSync.QoSHwMeterCreate,
                                            RM_QOS_APP_ID,
                                            NPSYNC_QO_S_HW_METER_CREATE);
            }
        }
#ifdef NPAPI_WANTED
#undef QosxQoSHwMeterCreate
        i4RetVal = QosxQoSHwMeterCreate (pMeterEntry);
        if (i4RetVal == FNP_FAILURE)
        {
            if (QosPortRmGetStandbyNodeCount () != QOS_INIT_VAL)
            {
                if (QOS_NPSYNC_BLK () == OSIX_FALSE)
                {
                    NpSyncQoSHwMeterCreateSync (NpSync.QoSHwMeterCreate,
                                                RM_QOS_APP_ID,
                                                NPSYNC_QO_S_HW_METER_CREATE);
                }
            }
        }
#endif
    }
    else if (u4NodeState == RM_STANDBY)
    {
        if (QOS_NPSYNC_BLK () == OSIX_FALSE)
        {
            QosHwAuditCreateOrFlushBuffer (&NpSync,
                                           NPSYNC_QO_S_HW_METER_CREATE,
                                           QOS_INIT_VAL);
        }
    }
    return (i4RetVal);
}

/* -------------------------------------------------------------
 *
 * Function: NpSyncQoSHwMeterDelete
 *
 * -------------------------------------------------------------
 */
PUBLIC INT4
NpSyncQoSHwMeterDelete (INT4 u4MeterId)
{
    unNpSync            NpSync;
    INT4                i4RetVal = FNP_SUCCESS;
    UINT4               u4NodeState = QOS_INIT_VAL;

    MEMSET (&NpSync, QOS_INIT_VAL, sizeof (NpSync));
    NpSync.QoSHwMeterDelete.u4MeterId = u4MeterId;
    u4NodeState = QosPortRmGetNodeState ();
    if (u4NodeState == RM_ACTIVE)
    {
        if (QosPortRmGetStandbyNodeCount () != QOS_INIT_VAL)
        {
            if (QOS_NPSYNC_BLK () == OSIX_FALSE)
            {
                NpSyncQoSHwMeterDeleteSync (NpSync.QoSHwMeterDelete,
                                            RM_QOS_APP_ID,
                                            NPSYNC_QO_S_HW_METER_DELETE);
            }
        }
#ifdef NPAPI_WANTED
#undef QosxQoSHwMeterDelete
        i4RetVal = QosxQoSHwMeterDelete (u4MeterId);
        if (i4RetVal == FNP_FAILURE)
        {
            if (QosPortRmGetStandbyNodeCount () != QOS_INIT_VAL)
            {
                if (QOS_NPSYNC_BLK () == OSIX_FALSE)
                {
                    NpSyncQoSHwMeterDeleteSync (NpSync.QoSHwMeterDelete,
                                                RM_QOS_APP_ID,
                                                NPSYNC_QO_S_HW_METER_DELETE);
                }
            }
        }
#endif
    }
    else if (u4NodeState == RM_STANDBY)
    {
        if (QOS_NPSYNC_BLK () == OSIX_FALSE)
        {
            QosHwAuditCreateOrFlushBuffer (&NpSync,
                                           NPSYNC_QO_S_HW_METER_DELETE,
                                           QOS_INIT_VAL);
        }
    }
    return (i4RetVal);
}

/* -------------------------------------------------------------
 *
 * Function: NpSyncQoSHwSchedulerAdd
 *
 * -------------------------------------------------------------
 */
PUBLIC INT4
NpSyncQoSHwSchedulerAdd (tQoSSchedulerEntry * pSchedEntry)
{
    unNpSync            NpSync;
    INT4                i4RetVal = FNP_SUCCESS;
    UINT4               u4NodeState = QOS_INIT_VAL;

    MEMSET (&NpSync, QOS_INIT_VAL, sizeof (NpSync));
    if (pSchedEntry != NULL)
    {
        NpSync.QoSHwSchedulerAdd.u4IfIndex = pSchedEntry->i4QosIfIndex;
        NpSync.QoSHwSchedulerAdd.u4SchedId = pSchedEntry->i4QosSchedulerId;
    }
    u4NodeState = QosPortRmGetNodeState ();
    if (u4NodeState == RM_ACTIVE)
    {
        if (QosPortRmGetStandbyNodeCount () != QOS_INIT_VAL)
        {
            if (QOS_NPSYNC_BLK () == OSIX_FALSE)
            {
                NpSyncQoSHwSchedulerAddSync (NpSync.QoSHwSchedulerAdd,
                                             RM_QOS_APP_ID,
                                             NPSYNC_QO_S_HW_SCHEDULER_ADD);
            }
        }
#ifdef NPAPI_WANTED
#undef QosxQoSHwSchedulerAdd
        i4RetVal = QosxQoSHwSchedulerAdd (pSchedEntry);
        if ((i4RetVal == FNP_FAILURE) || (i4RetVal == FNP_NOT_SUPPORTED))
        {
            if (QosPortRmGetStandbyNodeCount () != QOS_INIT_VAL)
            {
                if (QOS_NPSYNC_BLK () == OSIX_FALSE)
                {
                    NpSyncQoSHwSchedulerAddSync (NpSync.QoSHwSchedulerAdd,
                                                 RM_QOS_APP_ID,
                                                 NPSYNC_QO_S_HW_SCHEDULER_ADD);
                }
            }
        }
#endif
    }
    else if (u4NodeState == RM_STANDBY)
    {
        if (QOS_NPSYNC_BLK () == OSIX_FALSE)
        {
            QosHwAuditCreateOrFlushBuffer (&NpSync,
                                           NPSYNC_QO_S_HW_SCHEDULER_ADD,
                                           QOS_INIT_VAL);
        }
    }
    return (i4RetVal);
}

/* -------------------------------------------------------------
 *
 * Function: NpSyncQoSHwSchedulerUpdateParams
 *
 * -------------------------------------------------------------
 */
PUBLIC INT4
NpSyncQoSHwSchedulerUpdateParams (tQoSSchedulerEntry * pSchedEntry)
{
    unNpSync            NpSync;
    INT4                i4RetVal = FNP_SUCCESS;
    UINT4               u4NodeState = QOS_INIT_VAL;

    MEMSET (&NpSync, QOS_INIT_VAL, sizeof (NpSync));
    if (pSchedEntry != NULL)
    {
        NpSync.QoSHwSchedulerUpdateParams.u4IfIndex = pSchedEntry->i4QosIfIndex;
        NpSync.QoSHwSchedulerUpdateParams.u4SchedId =
            pSchedEntry->i4QosSchedulerId;
    }
    u4NodeState = QosPortRmGetNodeState ();
    if (u4NodeState == RM_ACTIVE)
    {
        if (QosPortRmGetStandbyNodeCount () != QOS_INIT_VAL)
        {
            if (QOS_NPSYNC_BLK () == OSIX_FALSE)
            {
                NpSyncQoSHwSchedulerUpdateParamsSync (NpSync.
                                                      QoSHwSchedulerUpdateParams,
                                                      RM_QOS_APP_ID,
                                                      NPSYNC_QO_S_HW_SCHEDULER_UPDATE_PARAMS);
            }
        }
#ifdef NPAPI_WANTED
#undef QosxQoSHwSchedulerUpdateParams
        i4RetVal = QosxQoSHwSchedulerUpdateParams (pSchedEntry);
        if (i4RetVal == FNP_FAILURE)
        {
            if (QosPortRmGetStandbyNodeCount () != QOS_INIT_VAL)
            {
                if (QOS_NPSYNC_BLK () == OSIX_FALSE)
                {
                    NpSyncQoSHwSchedulerUpdateParamsSync (NpSync.
                                                          QoSHwSchedulerUpdateParams,
                                                          RM_QOS_APP_ID,
                                                          NPSYNC_QO_S_HW_SCHEDULER_UPDATE_PARAMS);
                }
            }
        }
#endif
    }
    else if (u4NodeState == RM_STANDBY)
    {
        if (QOS_NPSYNC_BLK () == OSIX_FALSE)
        {
            QosHwAuditCreateOrFlushBuffer (&NpSync,
                                           NPSYNC_QO_S_HW_SCHEDULER_UPDATE_PARAMS,
                                           QOS_INIT_VAL);
        }
    }
    return (i4RetVal);
}

/* -------------------------------------------------------------
 *
 * Function: NpSyncQoSHwSchedulerDelete
 *
 * -------------------------------------------------------------
 */
PUBLIC INT4
NpSyncQoSHwSchedulerDelete (INT4 u4IfIndex, UINT4 u4SchedId)
{
    unNpSync            NpSync;
    INT4                i4RetVal = FNP_SUCCESS;
    UINT4               u4NodeState = QOS_INIT_VAL;

    MEMSET (&NpSync, QOS_INIT_VAL, sizeof (NpSync));
    NpSync.QoSHwSchedulerDelete.u4IfIndex = u4IfIndex;
    NpSync.QoSHwSchedulerDelete.u4SchedId = u4SchedId;
    u4NodeState = QosPortRmGetNodeState ();
    if (u4NodeState == RM_ACTIVE)
    {
        if (QosPortRmGetStandbyNodeCount () != QOS_INIT_VAL)
        {
            if (QOS_NPSYNC_BLK () == OSIX_FALSE)
            {
                NpSyncQoSHwSchedulerDeleteSync (NpSync.QoSHwSchedulerDelete,
                                                RM_QOS_APP_ID,
                                                NPSYNC_QO_S_HW_SCHEDULER_DELETE);
            }
        }
#ifdef NPAPI_WANTED
#undef QosxQoSHwSchedulerDelete
        i4RetVal = QosxQoSHwSchedulerDelete (u4IfIndex, u4SchedId);
        if (i4RetVal == FNP_FAILURE)
        {
            if (QosPortRmGetStandbyNodeCount () != QOS_INIT_VAL)
            {
                if (QOS_NPSYNC_BLK () == OSIX_FALSE)
                {
                    NpSyncQoSHwSchedulerDeleteSync (NpSync.QoSHwSchedulerDelete,
                                                    RM_QOS_APP_ID,
                                                    NPSYNC_QO_S_HW_SCHEDULER_DELETE);
                }
            }
        }
#endif
    }
    else if (u4NodeState == RM_STANDBY)
    {
        if (QOS_NPSYNC_BLK () == OSIX_FALSE)
        {
            QosHwAuditCreateOrFlushBuffer (&NpSync,
                                           NPSYNC_QO_S_HW_SCHEDULER_DELETE,
                                           QOS_INIT_VAL);
        }
    }
    return (i4RetVal);
}

/* -------------------------------------------------------------
 *
 * Function: NpSyncQoSHwQueueCreate
 *
 * -------------------------------------------------------------
 */
PUBLIC INT4
NpSyncQoSHwQueueCreate (INT4 u4IfIndex,
                        UINT4 u4QId,
                        tQoSQEntry * pQEntry,
                        tQoSQtypeEntry * pQTypeEntry,
                        tQoSREDCfgEntry * papRDCfgEntry[], INT2 u2HL)
{
    unNpSync            NpSync;
    INT4                i4RetVal = FNP_SUCCESS;
    UINT4               u4NodeState = QOS_INIT_VAL;

    MEMSET (&NpSync, QOS_INIT_VAL, sizeof (NpSync));
    NpSync.QoSHwQueueCreate.u4IfIndex = u4IfIndex;
    NpSync.QoSHwQueueCreate.u4Id = u4QId;

    u4NodeState = QosPortRmGetNodeState ();
    if (u4NodeState == RM_ACTIVE)
    {
        if (QosPortRmGetStandbyNodeCount () != QOS_INIT_VAL)
        {
            if (QOS_NPSYNC_BLK () == OSIX_FALSE)
            {
                NpSyncQoSHwQueueCreateSync (NpSync.QoSHwQueueCreate,
                                            RM_QOS_APP_ID,
                                            NPSYNC_QO_S_HW_QUEUE_CREATE);
            }
        }
#ifdef NPAPI_WANTED
#undef QosxQoSHwQueueCreate
        i4RetVal = QosxQoSHwQueueCreate (u4IfIndex, u4QId, pQEntry,
                                         pQTypeEntry, papRDCfgEntry, u2HL);
        if (i4RetVal == FNP_FAILURE)
        {
            if (QosPortRmGetStandbyNodeCount () != QOS_INIT_VAL)
            {
                if (QOS_NPSYNC_BLK () == OSIX_FALSE)
                {
                    NpSyncQoSHwQueueCreateSync (NpSync.QoSHwQueueCreate,
                                                RM_QOS_APP_ID,
                                                NPSYNC_QO_S_HW_QUEUE_CREATE);
                }
            }
        }
#endif
        UNUSED_PARAM (pQEntry);
        UNUSED_PARAM (pQTypeEntry);
        UNUSED_PARAM (papRDCfgEntry);
        UNUSED_PARAM (u2HL);
    }
    else if (u4NodeState == RM_STANDBY)
    {
        if (QOS_NPSYNC_BLK () == OSIX_FALSE)
        {
            QosHwAuditCreateOrFlushBuffer (&NpSync,
                                           NPSYNC_QO_S_HW_QUEUE_CREATE,
                                           QOS_INIT_VAL);
        }
    }
    return (i4RetVal);
}

/* -------------------------------------------------------------
 *
 * Function: NpSyncQoSHwQueueDelete
 *
 * -------------------------------------------------------------
 */
PUBLIC INT4
NpSyncQoSHwQueueDelete (INT4 u4IfIndex, UINT4 u4Id)
{
    unNpSync            NpSync;
    INT4                i4RetVal = FNP_SUCCESS;
    UINT4               u4NodeState = QOS_INIT_VAL;

    MEMSET (&NpSync, QOS_INIT_VAL, sizeof (NpSync));
    NpSync.QoSHwQueueDelete.u4IfIndex = u4IfIndex;
    NpSync.QoSHwQueueDelete.u4Id = u4Id;
    u4NodeState = QosPortRmGetNodeState ();
    if (u4NodeState == RM_ACTIVE)
    {
        if (QosPortRmGetStandbyNodeCount () != QOS_INIT_VAL)
        {
            if (QOS_NPSYNC_BLK () == OSIX_FALSE)
            {
                NpSyncQoSHwQueueDeleteSync (NpSync.QoSHwQueueDelete,
                                            RM_QOS_APP_ID,
                                            NPSYNC_QO_S_HW_QUEUE_DELETE);
            }
        }
#ifdef NPAPI_WANTED
#undef QosxQoSHwQueueDelete
        i4RetVal = QosxQoSHwQueueDelete (u4IfIndex, u4Id);
        if (i4RetVal == FNP_FAILURE)
        {
            if (QosPortRmGetStandbyNodeCount () != QOS_INIT_VAL)
            {
                if (QOS_NPSYNC_BLK () == OSIX_FALSE)
                {
                    NpSyncQoSHwQueueDeleteSync (NpSync.QoSHwQueueDelete,
                                                RM_QOS_APP_ID,
                                                NPSYNC_QO_S_HW_QUEUE_DELETE);
                }
            }
        }
#endif
    }
    else if (u4NodeState == RM_STANDBY)
    {
        if (QOS_NPSYNC_BLK () == OSIX_FALSE)
        {
            QosHwAuditCreateOrFlushBuffer (&NpSync,
                                           NPSYNC_QO_S_HW_QUEUE_DELETE,
                                           QOS_INIT_VAL);
        }
    }
    return (i4RetVal);
}

/* -------------------------------------------------------------
 *
 * Function: NpSyncQoSHwMapClassToQueue
 *
 * -------------------------------------------------------------
 */
PUBLIC INT4
NpSyncQoSHwMapClassToQueue (INT4 u4IfIndex,
                            INT4 u4ClsOrPriType,
                            UINT4 u4ClsOrPri, UINT4 u4QId, UINT1 u1Flag)
{
    unNpSync            NpSync;
    INT4                i4RetVal = FNP_SUCCESS;
    UINT4               u4NodeState = QOS_INIT_VAL;

    MEMSET (&NpSync, QOS_INIT_VAL, sizeof (NpSync));
    NpSync.QoSHwMapClassToQueue.u4IfIndex = u4IfIndex;
    NpSync.QoSHwMapClassToQueue.u4ClsOrPriType = u4ClsOrPriType;
    NpSync.QoSHwMapClassToQueue.u4ClsOrPri = u4ClsOrPri;
    NpSync.QoSHwMapClassToQueue.u4QId = u4QId;
    NpSync.QoSHwMapClassToQueue.u1Flag = u1Flag;
    u4NodeState = QosPortRmGetNodeState ();
    if (u4NodeState == RM_ACTIVE)
    {
        if (QosPortRmGetStandbyNodeCount () != QOS_INIT_VAL)
        {
            if (QOS_NPSYNC_BLK () == OSIX_FALSE)
            {
                NpSyncQoSHwMapClassToQueueSync (NpSync.QoSHwMapClassToQueue,
                                                RM_QOS_APP_ID,
                                                NPSYNC_QO_S_HW_MAP_CLASS_TO_QUEUE);
            }
        }
#ifdef NPAPI_WANTED
#undef QosxQoSHwMapClassToQueue
        i4RetVal = QosxQoSHwMapClassToQueue (u4IfIndex,
                                             u4ClsOrPriType, u4ClsOrPri, u4QId,
                                             u1Flag);
        if (i4RetVal == FNP_FAILURE)
        {
            if (QosPortRmGetStandbyNodeCount () != QOS_INIT_VAL)
            {
                if (QOS_NPSYNC_BLK () == OSIX_FALSE)
                {
                    NpSyncQoSHwMapClassToQueueSync (NpSync.QoSHwMapClassToQueue,
                                                    RM_QOS_APP_ID,
                                                    NPSYNC_QO_S_HW_MAP_CLASS_TO_QUEUE);
                }
            }
        }
#endif
    }
    else if (u4NodeState == RM_STANDBY)
    {
        if (QOS_NPSYNC_BLK () == OSIX_FALSE)
        {
            QosHwAuditCreateOrFlushBuffer (&NpSync,
                                           NPSYNC_QO_S_HW_MAP_CLASS_TO_QUEUE,
                                           QOS_INIT_VAL);
        }
    }
    return (i4RetVal);
}

/* -------------------------------------------------------------
 *
 * Function: NpSyncQoSHwSchedulerHierarchyMap
 *
 * -------------------------------------------------------------
 */
PUBLIC INT4
NpSyncQoSHwSchedulerHierarchyMap (INT4 u4IfIndex,
                                  UINT4 u4SchedId,
                                  UINT2 u2Sweight,
                                  UINT1 u1Spriority,
                                  UINT4 u4NextSchedId,
                                  UINT4 u4NextQId, INT2 u2HL, UINT1 u1Flag)
{
    unNpSync            NpSync;
    INT4                i4RetVal = FNP_SUCCESS;
    UINT4               u4NodeState = QOS_INIT_VAL;

    MEMSET (&NpSync, QOS_INIT_VAL, sizeof (NpSync));
    NpSync.QoSHwSchedulerHierarchyMap.u4IfIndex = u4IfIndex;
    NpSync.QoSHwSchedulerHierarchyMap.u4SchedId = u4SchedId;
    NpSync.QoSHwSchedulerHierarchyMap.u2Sweight = u2Sweight;
    NpSync.QoSHwSchedulerHierarchyMap.u1Spriority = u1Spriority;
    NpSync.QoSHwSchedulerHierarchyMap.u4NextSchedId = u4NextSchedId;
    NpSync.QoSHwSchedulerHierarchyMap.u4NextQId = u4NextQId;
    NpSync.QoSHwSchedulerHierarchyMap.u2HL = u2HL;
    NpSync.QoSHwSchedulerHierarchyMap.u1Flag = u1Flag;
    u4NodeState = QosPortRmGetNodeState ();
    if (u4NodeState == RM_ACTIVE)
    {
        if (QosPortRmGetStandbyNodeCount () != QOS_INIT_VAL)
        {
            if (QOS_NPSYNC_BLK () == OSIX_FALSE)
            {
                NpSyncQoSHwSchedulerHierarchyMapSync (NpSync.
                                                      QoSHwSchedulerHierarchyMap,
                                                      RM_QOS_APP_ID,
                                                      NPSYNC_QO_S_HW_SCHEDULER_HIERARCHY_MAP);
            }
        }
#ifdef NPAPI_WANTED
#undef QosxQoSHwSchedulerHierarchyMap
        i4RetVal = QosxQoSHwSchedulerHierarchyMap (u4IfIndex,
                                                   u4SchedId, u2Sweight,
                                                   u1Spriority, u4NextSchedId,
                                                   u4NextQId, u2HL, u1Flag);
        if (i4RetVal == FNP_FAILURE)
        {
            if (QosPortRmGetStandbyNodeCount () != QOS_INIT_VAL)
            {
                if (QOS_NPSYNC_BLK () == OSIX_FALSE)
                {
                    NpSyncQoSHwSchedulerHierarchyMapSync (NpSync.
                                                          QoSHwSchedulerHierarchyMap,
                                                          RM_QOS_APP_ID,
                                                          NPSYNC_QO_S_HW_SCHEDULER_HIERARCHY_MAP);
                }
            }
        }
#endif
    }
    else if (u4NodeState == RM_STANDBY)
    {
        if (QOS_NPSYNC_BLK () == OSIX_FALSE)
        {
            QosHwAuditCreateOrFlushBuffer (&NpSync,
                                           NPSYNC_QO_S_HW_SCHEDULER_HIERARCHY_MAP,
                                           QOS_INIT_VAL);
        }
    }
    return (i4RetVal);
}

/* -------------------------------------------------------------
 *
 * Function: NpSyncQoSHwSetDefUserPriority
 *
 * -------------------------------------------------------------
 */
PUBLIC INT4
NpSyncQoSHwSetDefUserPriority (INT4 u4Port, INT4 u4DefPriority)
{
    unNpSync            NpSync;
    INT4                i4RetVal = FNP_SUCCESS;
    UINT4               u4NodeState = QOS_INIT_VAL;

    MEMSET (&NpSync, QOS_INIT_VAL, sizeof (NpSync));
    NpSync.QoSHwSetDefUserPriority.u4Port = u4Port;
    NpSync.QoSHwSetDefUserPriority.u4DefPriority = u4DefPriority;
    u4NodeState = QosPortRmGetNodeState ();
    if (u4NodeState == RM_ACTIVE)
    {
        if (QosPortRmGetStandbyNodeCount () != QOS_INIT_VAL)
        {
            if (QOS_NPSYNC_BLK () == OSIX_FALSE)
            {
                NpSyncQoSHwSetDefUserPrioritySync (NpSync.
                                                   QoSHwSetDefUserPriority,
                                                   RM_QOS_APP_ID,
                                                   NPSYNC_QO_S_HW_SET_DEF_USER_PRIORITY);
            }
        }
#ifdef NPAPI_WANTED
#undef QosxQoSHwSetDefUserPriority
        i4RetVal = QosxQoSHwSetDefUserPriority (u4Port, u4DefPriority);
        if (i4RetVal == FNP_FAILURE)
        {
            if (QosPortRmGetStandbyNodeCount () != QOS_INIT_VAL)
            {
                if (QOS_NPSYNC_BLK () == OSIX_FALSE)
                {
                    NpSyncQoSHwSetDefUserPrioritySync (NpSync.
                                                       QoSHwSetDefUserPriority,
                                                       RM_QOS_APP_ID,
                                                       NPSYNC_QO_S_HW_SET_DEF_USER_PRIORITY);
                }
            }
        }
#endif
    }
    else if (u4NodeState == RM_STANDBY)
    {
        if (QOS_NPSYNC_BLK () == OSIX_FALSE)
        {
            QosHwAuditCreateOrFlushBuffer (&NpSync,
                                           NPSYNC_QO_S_HW_SET_DEF_USER_PRIORITY,
                                           QOS_INIT_VAL);
        }
    }
    return (i4RetVal);
}

/* -------------------------------------------------------------
 *
 * Function: NpSyncQoSHwMapClassToQueueId
 *
 * -------------------------------------------------------------
 */
PUBLIC INT4
NpSyncQoSHwMapClassToQueueId (tQoSClassMapEntry * pClassMapEntry,
                              INT4 u4IfIndex,
                              INT4 u4ClsOrPriType,
                              UINT4 u4ClsOrPri, UINT4 u4QId, UINT1 u1Flag)
{
    unNpSync            NpSync;
    INT4                i4RetVal = FNP_SUCCESS;
    UINT4               u4NodeState = QOS_INIT_VAL;

    MEMSET (&NpSync, QOS_INIT_VAL, sizeof (NpSync));
    if (pClassMapEntry != NULL)
    {
        NpSync.QoSHwMapClassToQueueId.u4ClassMapId
            = pClassMapEntry->u4QoSMFClass;
    }
    NpSync.QoSHwMapClassToQueueId.u4IfIndex = u4IfIndex;
    NpSync.QoSHwMapClassToQueueId.u4ClsOrPriType = u4ClsOrPriType;
    NpSync.QoSHwMapClassToQueueId.u4ClsOrPri = u4ClsOrPri;
    NpSync.QoSHwMapClassToQueueId.u4QId = u4QId;
    NpSync.QoSHwMapClassToQueueId.u1Flag = u1Flag;
    u4NodeState = QosPortRmGetNodeState ();
    if (u4NodeState == RM_ACTIVE)
    {
        if (QosPortRmGetStandbyNodeCount () != QOS_INIT_VAL)
        {
            if (QOS_NPSYNC_BLK () == OSIX_FALSE)
            {
                NpSyncQoSHwMapClassToQueueIdSync (NpSync.QoSHwMapClassToQueueId,
                                                  RM_QOS_APP_ID,
                                                  NPSYNC_QO_S_HW_MAP_CLASS_TO_QUEUE_ID);
            }
        }
#ifdef NPAPI_WANTED
#undef QosxQoSHwMapClassToQueueId
        i4RetVal = QosxQoSHwMapClassToQueueId (pClassMapEntry,
                                               u4IfIndex, u4ClsOrPriType,
                                               u4ClsOrPri, u4QId, u1Flag);
        if (i4RetVal == FNP_FAILURE)
        {
            if (QosPortRmGetStandbyNodeCount () != QOS_INIT_VAL)
            {
                if (QOS_NPSYNC_BLK () == OSIX_FALSE)
                {
                    NpSyncQoSHwMapClassToQueueIdSync (NpSync.
                                                      QoSHwMapClassToQueueId,
                                                      RM_QOS_APP_ID,
                                                      NPSYNC_QO_S_HW_MAP_CLASS_TO_QUEUE_ID);
                }
            }
        }
#endif
    }
    else if (u4NodeState == RM_STANDBY)
    {
        if (QOS_NPSYNC_BLK () == OSIX_FALSE)
        {
            QosHwAuditCreateOrFlushBuffer (&NpSync,
                                           NPSYNC_QO_S_HW_MAP_CLASS_TO_QUEUE_ID,
                                           QOS_INIT_VAL);
        }
    }
    return (i4RetVal);
}

/* -------------------------------------------------------------
 *
 * Function: NpSyncFsQosHwConfigPfc
 *
 * -------------------------------------------------------------
 */
PUBLIC INT4
NpSyncFsQosHwConfigPfc (tQosPfcHwEntry * pQosPfcHwEntry)
{
    unNpSync            NpSync;
    INT4                i4RetVal = FNP_SUCCESS;
    UINT4               u4NodeState = QOS_INIT_VAL;

    MEMSET (&NpSync, QOS_INIT_VAL, sizeof (NpSync));
    if (pQosPfcHwEntry != NULL)
    {

        NpSync.FsQosHwConfigPfc.i4IfIndex = pQosPfcHwEntry->u4Ifindex;
        NpSync.FsQosHwConfigPfc.u1PfcPriority = pQosPfcHwEntry->u1PfcPriority;
        NpSync.FsQosHwConfigPfc.u1Profile = pQosPfcHwEntry->u1PfcProfileBmp;
        NpSync.FsQosHwConfigPfc.u1Flag = pQosPfcHwEntry->u1PfcHwCfgFlag;
    }
    u4NodeState = QosPortRmGetNodeState ();
    if (u4NodeState == RM_ACTIVE)
    {
        if (QosPortRmGetStandbyNodeCount () != QOS_INIT_VAL)
        {
            if (QOS_NPSYNC_BLK () == OSIX_FALSE)
            {
                NpSyncFsQosHwConfigPfcSync (NpSync.FsQosHwConfigPfc,
                                            RM_QOS_APP_ID,
                                            NPSYNC_FS_QOS_HW_CONFIG_PFC);
            }
        }
#ifdef NPAPI_WANTED
#undef QosxFsQosHwConfigPfc
        i4RetVal = QosxFsQosHwConfigPfc (pQosPfcHwEntry);
#endif
        /*       if (i4RetVal == FNP_FAILURE) */
        {
            if (QosPortRmGetStandbyNodeCount () != QOS_INIT_VAL)
            {
                if (QOS_NPSYNC_BLK () == OSIX_FALSE)
                {
                    NpSyncFsQosHwConfigPfcSync (NpSync.FsQosHwConfigPfc,
                                                RM_QOS_APP_ID,
                                                NPSYNC_FS_QOS_HW_CONFIG_PFC);
                }
            }
        }
    }
    else if (u4NodeState == RM_STANDBY)
    {
        if (QOS_NPSYNC_BLK () == OSIX_FALSE)
        {
            /*
               QosHwAuditCreateOrFlushBuffer (&NpSync,
               NPSYNC_FS_QOS_HW_CONFIG_PFC,
               QOS_INIT_VAL); */
        }
    }
    return (i4RetVal);
}

/* -------------------------------------------------------------
 *
 * Function: NpSyncQoSHwSetCpuRateLimit
 *
 * -------------------------------------------------------------
 */
PUBLIC INT4
NpSyncQoSHwSetCpuRateLimit (INT4 i4CpuQueueId, UINT4 u4MinRate, UINT4 u4MaxRate)
{
    unNpSync            NpSync;
    INT4                i4RetVal = FNP_SUCCESS;
    UINT4               u4NodeState = QOS_INIT_VAL;

    MEMSET (&NpSync, QOS_INIT_VAL, sizeof (NpSync));
    NpSync.QoSHwSetCpuRateLimit.i4CpuQueueId = i4CpuQueueId;
    NpSync.QoSHwSetCpuRateLimit.u4MinRate = u4MinRate;
    NpSync.QoSHwSetCpuRateLimit.u4MaxRate = u4MaxRate;
    u4NodeState = QosPortRmGetNodeState ();
    if (u4NodeState == RM_ACTIVE)
    {
        if (QosPortRmGetStandbyNodeCount () != QOS_INIT_VAL)
        {
            if (QOS_NPSYNC_BLK () == OSIX_FALSE)
            {
                NpSyncQoSHwSetCpuRateLimitSync (NpSync.
                                                QoSHwSetCpuRateLimit,
                                                RM_QOS_APP_ID,
                                                NPSYNC_QO_S_HW_SET_CPU_RATE_LIMIT);
            }
        }
#ifdef NPAPI_WANTED
#undef QosxQoSHwSetCpuRateLimit
        i4RetVal =
            QosxQoSHwSetCpuRateLimit (i4CpuQueueId, u4MinRate, u4MaxRate);
        if (i4RetVal == FNP_FAILURE)
        {
            if (QosPortRmGetStandbyNodeCount () != QOS_INIT_VAL)
            {
                if (QOS_NPSYNC_BLK () == OSIX_FALSE)
                {
                    NpSyncQoSHwSetCpuRateLimitSync (NpSync.
                                                    QoSHwSetCpuRateLimit,
                                                    RM_QOS_APP_ID,
                                                    NPSYNC_QO_S_HW_SET_CPU_RATE_LIMIT);
                }
            }
        }
#endif
    }
    else if (u4NodeState == RM_STANDBY)
    {
        if (QOS_NPSYNC_BLK () == OSIX_FALSE)
        {
            QosHwAuditCreateOrFlushBuffer (&NpSync,
                                           NPSYNC_QO_S_HW_SET_CPU_RATE_LIMIT,
                                           QOS_INIT_VAL);
        }
    }
    return (i4RetVal);
}

/* -------------------------------------------------------------
 *
 * Function: NpSyncQoSHwInitSync
 *
 * -------------------------------------------------------------
 */
VOID
NpSyncQoSHwInitSync (UINT4 u4AppId, UINT4 u4NpApiId)
{
    tRmProtoEvt         ProtoEvt;
    tRmMsg             *pMsg = NULL;
    UINT2               u2OffSet = QOS_INIT_VAL;
    UINT2               u2MsgSize = QOS_INIT_VAL;

    ProtoEvt.u4AppId = u4AppId;
    ProtoEvt.u4Event = RM_BULK_UPDT_ABORT;

    u2MsgSize = NPSYNC_MSGTYPE_SIZE + NPSYNC_MSG_LENGTH +
        NPSYNC_MSG_NPAPI_ID_SIZE;
    if ((pMsg = RM_ALLOC_TX_BUF (u2MsgSize)) == NULL)
    {
        ProtoEvt.u4Error = RM_MEMALLOC_FAIL;
        QosPortRmApiHandleProtocolEvent (&ProtoEvt);
    }
    else
    {
        u2OffSet = QOS_INIT_VAL;

        NPSYNC_RM_PUT_1_BYTE (pMsg, &u2OffSet, NPSYNC_MESSAGE);
        NPSYNC_RM_PUT_2_BYTE (pMsg, &u2OffSet, u2MsgSize);
        NPSYNC_RM_PUT_4_BYTE (pMsg, &u2OffSet, u4NpApiId);
        if (QosPortEnqMsgToRm (pMsg, u2OffSet) == RM_FAILURE)
        {
            ProtoEvt.u4Error = RM_SENDTO_FAIL;
            QosPortRmApiHandleProtocolEvent (&ProtoEvt);
        }
    }
    return;
}

/* ---------------------------------------------------
 *
 * Function: NpSyncQoSHwMapClassToPolicySync
 *
 * ---------------------------------------------------
 */
VOID                NpSyncQoSHwMapClassToPolicySync
    (tNpSyncQoSHwMapClassToPolicy wMapClassToPolicy, UINT4 u4AppId,
     UINT4 u4NpApiId)
{
    tRmProtoEvt         ProtoEvt;
    tRmMsg             *pMsg = NULL;
    UINT2               u2OffSet = QOS_INIT_VAL;
    UINT2               u2MsgSize = QOS_INIT_VAL;

    ProtoEvt.u4AppId = u4AppId;
    ProtoEvt.u4Event = RM_BULK_UPDT_ABORT;

    u2MsgSize = NPSYNC_MSGTYPE_SIZE + NPSYNC_MSG_LENGTH +
        NPSYNC_MSG_NPAPI_ID_SIZE + sizeof (UINT4) + sizeof (UINT4) +
        sizeof (UINT4) + sizeof (UINT4) + sizeof (UINT4);
    if ((pMsg = RM_ALLOC_TX_BUF (u2MsgSize)) == NULL)
    {
        ProtoEvt.u4Error = RM_MEMALLOC_FAIL;
        QosPortRmApiHandleProtocolEvent (&ProtoEvt);
    }
    else
    {
        u2OffSet = QOS_INIT_VAL;

        NPSYNC_RM_PUT_1_BYTE (pMsg, &u2OffSet, NPSYNC_MESSAGE);
        NPSYNC_RM_PUT_2_BYTE (pMsg, &u2OffSet, u2MsgSize);
        NPSYNC_RM_PUT_4_BYTE (pMsg, &u2OffSet, u4NpApiId);
        NPSYNC_RM_PUT_4_BYTE (pMsg, &u2OffSet,
                              wMapClassToPolicy.ClassMapNode.u4PriorityMapId);
        NPSYNC_RM_PUT_4_BYTE (pMsg, &u2OffSet,
                              wMapClassToPolicy.ClassMapNode.u4L2FilterId);
        NPSYNC_RM_PUT_4_BYTE (pMsg, &u2OffSet,
                              wMapClassToPolicy.ClassMapNode.u4L3FilterId);
        NPSYNC_RM_PUT_4_BYTE (pMsg, &u2OffSet,
                              wMapClassToPolicy.ClassMapNode.u4ClassId);
        NPSYNC_RM_PUT_4_BYTE (pMsg, &u2OffSet, wMapClassToPolicy.u4PlyMapId);

        if (QosPortEnqMsgToRm (pMsg, u2OffSet) == RM_FAILURE)
        {
            ProtoEvt.u4Error = RM_SENDTO_FAIL;
            QosPortRmApiHandleProtocolEvent (&ProtoEvt);
        }
    }
    return;
}

/* -----------------------------------------------------
 *
 * Function: NpSyncQoSHwUpdatePolicyMapForClassSync
 *
 * ----------------------------------------------------
 */
VOID                NpSyncQoSHwUpdatePolicyMapForClassSync
    (tNpSyncQoSHwUpdatePolicyMapForClass wUpdatePolicyMapForClass,
     UINT4 u4AppId, UINT4 u4NpApiId)
{
    tRmProtoEvt         ProtoEvt;
    tRmMsg             *pMsg = NULL;
    UINT2               u2OffSet = QOS_INIT_VAL;
    UINT2               u2MsgSize = QOS_INIT_VAL;

    ProtoEvt.u4AppId = u4AppId;
    ProtoEvt.u4Event = RM_BULK_UPDT_ABORT;

    u2MsgSize = NPSYNC_MSGTYPE_SIZE + NPSYNC_MSG_LENGTH +
        NPSYNC_MSG_NPAPI_ID_SIZE + sizeof (UINT4) + sizeof (UINT4)
        + sizeof (UINT4) + sizeof (UINT4) + sizeof (UINT4);
    if ((pMsg = RM_ALLOC_TX_BUF (u2MsgSize)) == NULL)
    {
        ProtoEvt.u4Error = RM_MEMALLOC_FAIL;
        QosPortRmApiHandleProtocolEvent (&ProtoEvt);
    }
    else
    {
        u2OffSet = QOS_INIT_VAL;

        NPSYNC_RM_PUT_1_BYTE (pMsg, &u2OffSet, NPSYNC_MESSAGE);
        NPSYNC_RM_PUT_2_BYTE (pMsg, &u2OffSet, u2MsgSize);
        NPSYNC_RM_PUT_4_BYTE (pMsg, &u2OffSet, u4NpApiId);
        NPSYNC_RM_PUT_4_BYTE (pMsg, &u2OffSet,
                              wUpdatePolicyMapForClass.ClassMapNode.
                              u4PriorityMapId);
        NPSYNC_RM_PUT_4_BYTE (pMsg, &u2OffSet,
                              wUpdatePolicyMapForClass.ClassMapNode.
                              u4L2FilterId);
        NPSYNC_RM_PUT_4_BYTE (pMsg, &u2OffSet,
                              wUpdatePolicyMapForClass.ClassMapNode.
                              u4L3FilterId);
        NPSYNC_RM_PUT_4_BYTE (pMsg, &u2OffSet,
                              wUpdatePolicyMapForClass.ClassMapNode.u4ClassId);
        NPSYNC_RM_PUT_4_BYTE (pMsg, &u2OffSet,
                              wUpdatePolicyMapForClass.u4PlyMapId);
        if (QosPortEnqMsgToRm (pMsg, u2OffSet) == RM_FAILURE)
        {
            ProtoEvt.u4Error = RM_SENDTO_FAIL;
            QosPortRmApiHandleProtocolEvent (&ProtoEvt);
        }
    }
    return;
}

/* -------------------------------------------------------------
 *
 * Function: NpSyncQoSHwUnmapClassFromPolicySync
 *
 * -------------------------------------------------------------
 */
VOID                NpSyncQoSHwUnmapClassFromPolicySync
    (tNpSyncQoSHwUnmapClassFromPolicy wUnmapClassFromPolicy,
     UINT4 u4AppId, UINT4 u4NpApiId)
{
    tRmProtoEvt         ProtoEvt;
    tRmMsg             *pMsg = NULL;
    UINT2               u2OffSet = QOS_INIT_VAL;
    UINT2               u2MsgSize = QOS_INIT_VAL;

    ProtoEvt.u4AppId = u4AppId;
    ProtoEvt.u4Event = RM_BULK_UPDT_ABORT;

    u2MsgSize = NPSYNC_MSGTYPE_SIZE + NPSYNC_MSG_LENGTH +
        NPSYNC_MSG_NPAPI_ID_SIZE + sizeof (UINT4) + sizeof (UINT4) +
        sizeof (UINT4) + sizeof (UINT4) + sizeof (UINT4);
    if ((pMsg = RM_ALLOC_TX_BUF (u2MsgSize)) == NULL)
    {
        ProtoEvt.u4Error = RM_MEMALLOC_FAIL;
        QosPortRmApiHandleProtocolEvent (&ProtoEvt);
    }
    else
    {
        u2OffSet = QOS_INIT_VAL;

        NPSYNC_RM_PUT_1_BYTE (pMsg, &u2OffSet, NPSYNC_MESSAGE);
        NPSYNC_RM_PUT_2_BYTE (pMsg, &u2OffSet, u2MsgSize);
        NPSYNC_RM_PUT_4_BYTE (pMsg, &u2OffSet, u4NpApiId);
        NPSYNC_RM_PUT_4_BYTE (pMsg, &u2OffSet,
                              wUnmapClassFromPolicy.ClassMapNode.
                              u4PriorityMapId);
        NPSYNC_RM_PUT_4_BYTE (pMsg, &u2OffSet,
                              wUnmapClassFromPolicy.ClassMapNode.u4L2FilterId);
        NPSYNC_RM_PUT_4_BYTE (pMsg, &u2OffSet,
                              wUnmapClassFromPolicy.ClassMapNode.u4L3FilterId);
        NPSYNC_RM_PUT_4_BYTE (pMsg, &u2OffSet,
                              wUnmapClassFromPolicy.ClassMapNode.u4ClassId);
        NPSYNC_RM_PUT_4_BYTE (pMsg, &u2OffSet,
                              wUnmapClassFromPolicy.u4PlyMapId);

        if (QosPortEnqMsgToRm (pMsg, u2OffSet) == RM_FAILURE)
        {
            ProtoEvt.u4Error = RM_SENDTO_FAIL;
            QosPortRmApiHandleProtocolEvent (&ProtoEvt);
        }
    }
    return;
}

/* -------------------------------------------------------------
 *
 * Function: NpSyncQoSHwDeleteClassMapEntrySync
 *
 * -------------------------------------------------------------
 */
VOID                NpSyncQoSHwDeleteClassMapEntrySync
    (tNpSyncQoSHwDeleteClassMapEntry wDeleteClassMapEntry,
     UINT4 u4AppId, UINT4 u4NpApiId)
{
    tRmProtoEvt         ProtoEvt;
    tRmMsg             *pMsg = NULL;
    UINT2               u2OffSet = QOS_INIT_VAL;
    UINT2               u2MsgSize = QOS_INIT_VAL;

    ProtoEvt.u4AppId = u4AppId;
    ProtoEvt.u4Event = RM_BULK_UPDT_ABORT;

    u2MsgSize = NPSYNC_MSGTYPE_SIZE + NPSYNC_MSG_LENGTH
        + NPSYNC_MSG_NPAPI_ID_SIZE + sizeof (UINT4) +
        sizeof (UINT4) + sizeof (UINT4) + sizeof (UINT4);

    if ((pMsg = RM_ALLOC_TX_BUF (u2MsgSize)) == NULL)
    {
        ProtoEvt.u4Error = RM_MEMALLOC_FAIL;
        QosPortRmApiHandleProtocolEvent (&ProtoEvt);
    }
    else
    {
        u2OffSet = QOS_INIT_VAL;

        NPSYNC_RM_PUT_1_BYTE (pMsg, &u2OffSet, NPSYNC_MESSAGE);
        NPSYNC_RM_PUT_2_BYTE (pMsg, &u2OffSet, u2MsgSize);
        NPSYNC_RM_PUT_4_BYTE (pMsg, &u2OffSet, u4NpApiId);
        NPSYNC_RM_PUT_4_BYTE (pMsg, &u2OffSet,
                              wDeleteClassMapEntry.ClassMapNode.
                              u4PriorityMapId);
        NPSYNC_RM_PUT_4_BYTE (pMsg, &u2OffSet,
                              wDeleteClassMapEntry.ClassMapNode.u4L2FilterId);
        NPSYNC_RM_PUT_4_BYTE (pMsg, &u2OffSet,
                              wDeleteClassMapEntry.ClassMapNode.u4L3FilterId);
        NPSYNC_RM_PUT_4_BYTE (pMsg, &u2OffSet,
                              wDeleteClassMapEntry.ClassMapNode.u4ClassId);

        if (QosPortEnqMsgToRm (pMsg, u2OffSet) == RM_FAILURE)
        {
            ProtoEvt.u4Error = RM_SENDTO_FAIL;
            QosPortRmApiHandleProtocolEvent (&ProtoEvt);
        }
    }
    return;
}

/* -------------------------------------------------------------
 *
 * Function: NpSyncQoSHwMeterCreateSync
 *
 * -------------------------------------------------------------
 */
VOID                NpSyncQoSHwMeterCreateSync
    (tNpSyncQoSHwMeterCreate wMeterCreate, UINT4 u4AppId, UINT4 u4NpApiId)
{
    tRmProtoEvt         ProtoEvt;
    tRmMsg             *pMsg = NULL;
    UINT2               u2OffSet = QOS_INIT_VAL;
    UINT2               u2MsgSize = QOS_INIT_VAL;

    ProtoEvt.u4AppId = u4AppId;
    ProtoEvt.u4Event = RM_BULK_UPDT_ABORT;

    u2MsgSize = NPSYNC_MSGTYPE_SIZE + NPSYNC_MSG_LENGTH +
        NPSYNC_MSG_NPAPI_ID_SIZE + sizeof (INT4);
    if ((pMsg = RM_ALLOC_TX_BUF (u2MsgSize)) == NULL)
    {
        ProtoEvt.u4Error = RM_MEMALLOC_FAIL;
        QosPortRmApiHandleProtocolEvent (&ProtoEvt);
    }
    else
    {
        u2OffSet = QOS_INIT_VAL;

        NPSYNC_RM_PUT_1_BYTE (pMsg, &u2OffSet, NPSYNC_MESSAGE);
        NPSYNC_RM_PUT_2_BYTE (pMsg, &u2OffSet, u2MsgSize);
        NPSYNC_RM_PUT_4_BYTE (pMsg, &u2OffSet, u4NpApiId);
        NPSYNC_RM_PUT_4_BYTE (pMsg, &u2OffSet, wMeterCreate.u4MeterId);
        if (QosPortEnqMsgToRm (pMsg, u2OffSet) == RM_FAILURE)
        {
            ProtoEvt.u4Error = RM_SENDTO_FAIL;
            QosPortRmApiHandleProtocolEvent (&ProtoEvt);
        }
    }
    return;
}

/* -------------------------------------------------------------
 *
 * Function: NpSyncQoSHwMeterDeleteSync
 *
 * -------------------------------------------------------------
 */
VOID                NpSyncQoSHwMeterDeleteSync
    (tNpSyncQoSHwMeterDelete wMeterDelete, UINT4 u4AppId, UINT4 u4NpApiId)
{
    tRmProtoEvt         ProtoEvt;
    tRmMsg             *pMsg = NULL;
    UINT2               u2OffSet = QOS_INIT_VAL;
    UINT2               u2MsgSize = QOS_INIT_VAL;

    ProtoEvt.u4AppId = u4AppId;
    ProtoEvt.u4Event = RM_BULK_UPDT_ABORT;

    u2MsgSize = NPSYNC_MSGTYPE_SIZE + NPSYNC_MSG_LENGTH
        + NPSYNC_MSG_NPAPI_ID_SIZE + sizeof (INT4);
    if ((pMsg = RM_ALLOC_TX_BUF (u2MsgSize)) == NULL)
    {
        ProtoEvt.u4Error = RM_MEMALLOC_FAIL;
        QosPortRmApiHandleProtocolEvent (&ProtoEvt);
    }
    else
    {
        u2OffSet = QOS_INIT_VAL;

        NPSYNC_RM_PUT_1_BYTE (pMsg, &u2OffSet, NPSYNC_MESSAGE);
        NPSYNC_RM_PUT_2_BYTE (pMsg, &u2OffSet, u2MsgSize);
        NPSYNC_RM_PUT_4_BYTE (pMsg, &u2OffSet, u4NpApiId);
        NPSYNC_RM_PUT_4_BYTE (pMsg, &u2OffSet, wMeterDelete.u4MeterId);
        if (QosPortEnqMsgToRm (pMsg, u2OffSet) == RM_FAILURE)
        {
            ProtoEvt.u4Error = RM_SENDTO_FAIL;
            QosPortRmApiHandleProtocolEvent (&ProtoEvt);
        }
    }
    return;
}

/* -------------------------------------------------------------
 *
 * Function: NpSyncQoSHwSchedulerAddSync
 *
 * -------------------------------------------------------------
 */
VOID                NpSyncQoSHwSchedulerAddSync
    (tNpSyncQoSHwSchedulerAdd wSchedulerAdd, UINT4 u4AppId, UINT4 u4NpApiId)
{
    tRmProtoEvt         ProtoEvt;
    tRmMsg             *pMsg = NULL;
    UINT2               u2OffSet = QOS_INIT_VAL;
    UINT2               u2MsgSize = QOS_INIT_VAL;

    ProtoEvt.u4AppId = u4AppId;
    ProtoEvt.u4Event = RM_BULK_UPDT_ABORT;

    u2MsgSize = NPSYNC_MSGTYPE_SIZE + NPSYNC_MSG_LENGTH +
        NPSYNC_MSG_NPAPI_ID_SIZE + sizeof (INT4) + sizeof (UINT4);
    if ((pMsg = RM_ALLOC_TX_BUF (u2MsgSize)) == NULL)
    {
        ProtoEvt.u4Error = RM_MEMALLOC_FAIL;
        QosPortRmApiHandleProtocolEvent (&ProtoEvt);
    }
    else
    {
        u2OffSet = QOS_INIT_VAL;

        NPSYNC_RM_PUT_1_BYTE (pMsg, &u2OffSet, NPSYNC_MESSAGE);
        NPSYNC_RM_PUT_2_BYTE (pMsg, &u2OffSet, u2MsgSize);
        NPSYNC_RM_PUT_4_BYTE (pMsg, &u2OffSet, u4NpApiId);
        NPSYNC_RM_PUT_4_BYTE (pMsg, &u2OffSet, wSchedulerAdd.u4IfIndex);
        NPSYNC_RM_PUT_4_BYTE (pMsg, &u2OffSet, wSchedulerAdd.u4SchedId);
        if (QosPortEnqMsgToRm (pMsg, u2OffSet) == RM_FAILURE)
        {
            ProtoEvt.u4Error = RM_SENDTO_FAIL;
            QosPortRmApiHandleProtocolEvent (&ProtoEvt);
        }
    }
    return;
}

/* -------------------------------------------------------------
 *
 * Function: NpSyncQoSHwSchedulerUpdateParamsSync
 *
 * -------------------------------------------------------------
 */
VOID                NpSyncQoSHwSchedulerUpdateParamsSync
    (tNpSyncQoSHwSchedulerUpdateParams wSchedulerUpdateParams,
     UINT4 u4AppId, UINT4 u4NpApiId)
{
    tRmProtoEvt         ProtoEvt;
    tRmMsg             *pMsg = NULL;
    UINT2               u2OffSet = QOS_INIT_VAL;
    UINT2               u2MsgSize = QOS_INIT_VAL;

    ProtoEvt.u4AppId = u4AppId;
    ProtoEvt.u4Event = RM_BULK_UPDT_ABORT;

    u2MsgSize = NPSYNC_MSGTYPE_SIZE + NPSYNC_MSG_LENGTH +
        NPSYNC_MSG_NPAPI_ID_SIZE + sizeof (INT4) + sizeof (UINT4);
    if ((pMsg = RM_ALLOC_TX_BUF (u2MsgSize)) == NULL)
    {
        ProtoEvt.u4Error = RM_MEMALLOC_FAIL;
        QosPortRmApiHandleProtocolEvent (&ProtoEvt);
    }
    else
    {
        u2OffSet = QOS_INIT_VAL;

        NPSYNC_RM_PUT_1_BYTE (pMsg, &u2OffSet, NPSYNC_MESSAGE);
        NPSYNC_RM_PUT_2_BYTE (pMsg, &u2OffSet, u2MsgSize);
        NPSYNC_RM_PUT_4_BYTE (pMsg, &u2OffSet, u4NpApiId);
        NPSYNC_RM_PUT_4_BYTE (pMsg, &u2OffSet,
                              wSchedulerUpdateParams.u4IfIndex);
        NPSYNC_RM_PUT_4_BYTE (pMsg, &u2OffSet,
                              wSchedulerUpdateParams.u4SchedId);
        if (QosPortEnqMsgToRm (pMsg, u2OffSet) == RM_FAILURE)
        {
            ProtoEvt.u4Error = RM_SENDTO_FAIL;
            QosPortRmApiHandleProtocolEvent (&ProtoEvt);
        }
    }
    return;
}

/* -------------------------------------------------------------
 *
 * Function: NpSyncQoSHwSchedulerDeleteSync
 *
 * -------------------------------------------------------------
 */
VOID                NpSyncQoSHwSchedulerDeleteSync
    (tNpSyncQoSHwSchedulerDelete wSchedulerDelete,
     UINT4 u4AppId, UINT4 u4NpApiId)
{
    tRmProtoEvt         ProtoEvt;
    tRmMsg             *pMsg = NULL;
    UINT2               u2OffSet = QOS_INIT_VAL;
    UINT2               u2MsgSize = QOS_INIT_VAL;

    ProtoEvt.u4AppId = u4AppId;
    ProtoEvt.u4Event = RM_BULK_UPDT_ABORT;

    u2MsgSize = NPSYNC_MSGTYPE_SIZE + NPSYNC_MSG_LENGTH +
        NPSYNC_MSG_NPAPI_ID_SIZE + sizeof (INT4) + sizeof (UINT4);
    if ((pMsg = RM_ALLOC_TX_BUF (u2MsgSize)) == NULL)
    {
        ProtoEvt.u4Error = RM_MEMALLOC_FAIL;
        QosPortRmApiHandleProtocolEvent (&ProtoEvt);
    }
    else
    {
        u2OffSet = QOS_INIT_VAL;

        NPSYNC_RM_PUT_1_BYTE (pMsg, &u2OffSet, NPSYNC_MESSAGE);
        NPSYNC_RM_PUT_2_BYTE (pMsg, &u2OffSet, u2MsgSize);
        NPSYNC_RM_PUT_4_BYTE (pMsg, &u2OffSet, u4NpApiId);
        NPSYNC_RM_PUT_4_BYTE (pMsg, &u2OffSet, wSchedulerDelete.u4IfIndex);
        NPSYNC_RM_PUT_4_BYTE (pMsg, &u2OffSet, wSchedulerDelete.u4SchedId);
        if (QosPortEnqMsgToRm (pMsg, u2OffSet) == RM_FAILURE)
        {
            ProtoEvt.u4Error = RM_SENDTO_FAIL;
            QosPortRmApiHandleProtocolEvent (&ProtoEvt);
        }
    }
    return;
}

/* -------------------------------------------------------------
 *
 * Function: NpSyncQoSHwQueueCreateSync
 *
 * -------------------------------------------------------------
 */
VOID                NpSyncQoSHwQueueCreateSync
    (tNpSyncQoSHwQueueCreate wQueueCreate, UINT4 u4AppId, UINT4 u4NpApiId)
{
    tRmProtoEvt         ProtoEvt;
    tRmMsg             *pMsg = NULL;
    UINT2               u2OffSet = QOS_INIT_VAL;
    UINT2               u2MsgSize = QOS_INIT_VAL;

    ProtoEvt.u4AppId = u4AppId;
    ProtoEvt.u4Event = RM_BULK_UPDT_ABORT;

    u2MsgSize = NPSYNC_MSGTYPE_SIZE + NPSYNC_MSG_LENGTH +
        NPSYNC_MSG_NPAPI_ID_SIZE + sizeof (INT4) + sizeof (UINT4);
    if ((pMsg = RM_ALLOC_TX_BUF (u2MsgSize)) == NULL)
    {
        ProtoEvt.u4Error = RM_MEMALLOC_FAIL;
        QosPortRmApiHandleProtocolEvent (&ProtoEvt);
    }
    else
    {
        u2OffSet = QOS_INIT_VAL;

        NPSYNC_RM_PUT_1_BYTE (pMsg, &u2OffSet, NPSYNC_MESSAGE);
        NPSYNC_RM_PUT_2_BYTE (pMsg, &u2OffSet, u2MsgSize);
        NPSYNC_RM_PUT_4_BYTE (pMsg, &u2OffSet, u4NpApiId);
        NPSYNC_RM_PUT_4_BYTE (pMsg, &u2OffSet, wQueueCreate.u4IfIndex);
        NPSYNC_RM_PUT_4_BYTE (pMsg, &u2OffSet, wQueueCreate.u4Id);
        if (QosPortEnqMsgToRm (pMsg, u2OffSet) == RM_FAILURE)
        {
            ProtoEvt.u4Error = RM_SENDTO_FAIL;
            QosPortRmApiHandleProtocolEvent (&ProtoEvt);
        }
    }
    return;
}

/* -------------------------------------------------------------
 *
 * Function: NpSyncQoSHwQueueDeleteSync
 *
 * -------------------------------------------------------------
 */
VOID                NpSyncQoSHwQueueDeleteSync
    (tNpSyncQoSHwQueueDelete wQueueDelete, UINT4 u4AppId, UINT4 u4NpApiId)
{
    tRmProtoEvt         ProtoEvt;
    tRmMsg             *pMsg = NULL;
    UINT2               u2OffSet = QOS_INIT_VAL;
    UINT2               u2MsgSize = QOS_INIT_VAL;

    ProtoEvt.u4AppId = u4AppId;
    ProtoEvt.u4Event = RM_BULK_UPDT_ABORT;

    u2MsgSize = NPSYNC_MSGTYPE_SIZE + NPSYNC_MSG_LENGTH
        + NPSYNC_MSG_NPAPI_ID_SIZE + sizeof (INT4) + sizeof (UINT4);
    if ((pMsg = RM_ALLOC_TX_BUF (u2MsgSize)) == NULL)
    {
        ProtoEvt.u4Error = RM_MEMALLOC_FAIL;
        QosPortRmApiHandleProtocolEvent (&ProtoEvt);
    }
    else
    {
        u2OffSet = QOS_INIT_VAL;

        NPSYNC_RM_PUT_1_BYTE (pMsg, &u2OffSet, NPSYNC_MESSAGE);
        NPSYNC_RM_PUT_2_BYTE (pMsg, &u2OffSet, u2MsgSize);
        NPSYNC_RM_PUT_4_BYTE (pMsg, &u2OffSet, u4NpApiId);
        NPSYNC_RM_PUT_4_BYTE (pMsg, &u2OffSet, wQueueDelete.u4IfIndex);
        NPSYNC_RM_PUT_4_BYTE (pMsg, &u2OffSet, wQueueDelete.u4Id);
        if (QosPortEnqMsgToRm (pMsg, u2OffSet) == RM_FAILURE)
        {
            ProtoEvt.u4Error = RM_SENDTO_FAIL;
            QosPortRmApiHandleProtocolEvent (&ProtoEvt);
        }
    }
    return;
}

/* ----------------------------------------------------
 *
 * Function: NpSyncQoSHwMapClassToQueueSync
 *
 * ---------------------------------------------------
 */
VOID                NpSyncQoSHwMapClassToQueueSync
    (tNpSyncQoSHwMapClassToQueue wMapClassToQueue,
     UINT4 u4AppId, UINT4 u4NpApiId)
{
    tRmProtoEvt         ProtoEvt;
    tRmMsg             *pMsg = NULL;
    UINT2               u2OffSet = QOS_INIT_VAL;
    UINT2               u2MsgSize = QOS_INIT_VAL;

    ProtoEvt.u4AppId = u4AppId;
    ProtoEvt.u4Event = RM_BULK_UPDT_ABORT;

    u2MsgSize = NPSYNC_MSGTYPE_SIZE + NPSYNC_MSG_LENGTH
        + NPSYNC_MSG_NPAPI_ID_SIZE + sizeof (INT4) + sizeof (INT4) +
        sizeof (UINT4) + sizeof (UINT4) + sizeof (UINT1);
    if ((pMsg = RM_ALLOC_TX_BUF (u2MsgSize)) == NULL)
    {
        ProtoEvt.u4Error = RM_MEMALLOC_FAIL;
        QosPortRmApiHandleProtocolEvent (&ProtoEvt);
    }
    else
    {
        u2OffSet = QOS_INIT_VAL;

        NPSYNC_RM_PUT_1_BYTE (pMsg, &u2OffSet, NPSYNC_MESSAGE);
        NPSYNC_RM_PUT_2_BYTE (pMsg, &u2OffSet, u2MsgSize);
        NPSYNC_RM_PUT_4_BYTE (pMsg, &u2OffSet, u4NpApiId);
        NPSYNC_RM_PUT_4_BYTE (pMsg, &u2OffSet, wMapClassToQueue.u4IfIndex);
        NPSYNC_RM_PUT_4_BYTE (pMsg, &u2OffSet, wMapClassToQueue.u4ClsOrPriType);
        NPSYNC_RM_PUT_4_BYTE (pMsg, &u2OffSet, wMapClassToQueue.u4ClsOrPri);
        NPSYNC_RM_PUT_4_BYTE (pMsg, &u2OffSet, wMapClassToQueue.u4QId);
        NPSYNC_RM_PUT_1_BYTE (pMsg, &u2OffSet, wMapClassToQueue.u1Flag);
        if (QosPortEnqMsgToRm (pMsg, u2OffSet) == RM_FAILURE)
        {
            ProtoEvt.u4Error = RM_SENDTO_FAIL;
            QosPortRmApiHandleProtocolEvent (&ProtoEvt);
        }
    }
    return;
}

/* ------------------------------------------------------
 *
 * Function: NpSyncQoSHwSchedulerHierarchyMapSync
 *
 * -------------------------------------------------------
 */
VOID                NpSyncQoSHwSchedulerHierarchyMapSync
    (tNpSyncQoSHwSchedulerHierarchyMap wSchedulerHierarchyMap,
     UINT4 u4AppId, UINT4 u4NpApiId)
{
    tRmProtoEvt         ProtoEvt;
    tRmMsg             *pMsg = NULL;
    UINT2               u2OffSet = QOS_INIT_VAL;
    UINT2               u2MsgSize = QOS_INIT_VAL;

    ProtoEvt.u4AppId = u4AppId;
    ProtoEvt.u4Event = RM_BULK_UPDT_ABORT;

    u2MsgSize = NPSYNC_MSGTYPE_SIZE + NPSYNC_MSG_LENGTH +
        NPSYNC_MSG_NPAPI_ID_SIZE + sizeof (INT4) + sizeof (UINT4) +
        sizeof (UINT2) + sizeof (UINT1) + sizeof (UINT4) +
        sizeof (UINT4) + sizeof (INT2) + sizeof (UINT1);
    if ((pMsg = RM_ALLOC_TX_BUF (u2MsgSize)) == NULL)
    {
        ProtoEvt.u4Error = RM_MEMALLOC_FAIL;
        QosPortRmApiHandleProtocolEvent (&ProtoEvt);
    }
    else
    {
        u2OffSet = QOS_INIT_VAL;

        NPSYNC_RM_PUT_1_BYTE (pMsg, &u2OffSet, NPSYNC_MESSAGE);
        NPSYNC_RM_PUT_2_BYTE (pMsg, &u2OffSet, u2MsgSize);
        NPSYNC_RM_PUT_4_BYTE (pMsg, &u2OffSet, u4NpApiId);
        NPSYNC_RM_PUT_4_BYTE (pMsg, &u2OffSet,
                              wSchedulerHierarchyMap.u4IfIndex);
        NPSYNC_RM_PUT_4_BYTE (pMsg, &u2OffSet,
                              wSchedulerHierarchyMap.u4SchedId);
        NPSYNC_RM_PUT_2_BYTE (pMsg, &u2OffSet,
                              wSchedulerHierarchyMap.u2Sweight);
        NPSYNC_RM_PUT_1_BYTE (pMsg, &u2OffSet,
                              wSchedulerHierarchyMap.u1Spriority);
        NPSYNC_RM_PUT_4_BYTE (pMsg, &u2OffSet,
                              wSchedulerHierarchyMap.u4NextSchedId);
        NPSYNC_RM_PUT_4_BYTE (pMsg, &u2OffSet,
                              wSchedulerHierarchyMap.u4NextQId);
        NPSYNC_RM_PUT_2_BYTE (pMsg, &u2OffSet, wSchedulerHierarchyMap.u2HL);
        NPSYNC_RM_PUT_1_BYTE (pMsg, &u2OffSet, wSchedulerHierarchyMap.u1Flag);
        if (QosPortEnqMsgToRm (pMsg, u2OffSet) == RM_FAILURE)
        {
            ProtoEvt.u4Error = RM_SENDTO_FAIL;
            QosPortRmApiHandleProtocolEvent (&ProtoEvt);
        }
    }
    return;
}

/* -------------------------------------------------------------
 *
 * Function: NpSyncQoSHwSetDefUserPrioritySync
 *
 * -------------------------------------------------------------
 */
VOID                NpSyncQoSHwSetDefUserPrioritySync
    (tNpSyncQoSHwSetDefUserPriority wSetDefUserPriority,
     UINT4 u4AppId, UINT4 u4NpApiId)
{
    tRmProtoEvt         ProtoEvt;
    tRmMsg             *pMsg = NULL;
    UINT2               u2OffSet = QOS_INIT_VAL;
    UINT2               u2MsgSize = QOS_INIT_VAL;

    ProtoEvt.u4AppId = u4AppId;
    ProtoEvt.u4Event = RM_BULK_UPDT_ABORT;

    u2MsgSize = NPSYNC_MSGTYPE_SIZE + NPSYNC_MSG_LENGTH +
        NPSYNC_MSG_NPAPI_ID_SIZE + sizeof (INT4) + sizeof (INT4);
    if ((pMsg = RM_ALLOC_TX_BUF (u2MsgSize)) == NULL)
    {
        ProtoEvt.u4Error = RM_MEMALLOC_FAIL;
        QosPortRmApiHandleProtocolEvent (&ProtoEvt);
    }
    else
    {
        u2OffSet = QOS_INIT_VAL;

        NPSYNC_RM_PUT_1_BYTE (pMsg, &u2OffSet, NPSYNC_MESSAGE);
        NPSYNC_RM_PUT_2_BYTE (pMsg, &u2OffSet, u2MsgSize);
        NPSYNC_RM_PUT_4_BYTE (pMsg, &u2OffSet, u4NpApiId);
        NPSYNC_RM_PUT_4_BYTE (pMsg, &u2OffSet, wSetDefUserPriority.u4Port);
        NPSYNC_RM_PUT_4_BYTE (pMsg, &u2OffSet,
                              wSetDefUserPriority.u4DefPriority);
        if (QosPortEnqMsgToRm (pMsg, u2OffSet) == RM_FAILURE)
        {
            ProtoEvt.u4Error = RM_SENDTO_FAIL;
            QosPortRmApiHandleProtocolEvent (&ProtoEvt);
        }
    }
    return;
}

/* -------------------------------------------------------------
 *
 * Function: NpSyncQoSHwMapClassToQueueIdSync
 *
 * -------------------------------------------------------------
 */
VOID                NpSyncQoSHwMapClassToQueueIdSync
    (tNpSyncQoSHwMapClassToQueueId wMapClassToQueueId, UINT4 u4AppId,
     UINT4 u4NpApiId)
{
    tRmProtoEvt         ProtoEvt;
    tRmMsg             *pMsg = NULL;
    UINT2               u2OffSet = QOS_INIT_VAL;
    UINT2               u2MsgSize = QOS_INIT_VAL;

    ProtoEvt.u4AppId = u4AppId;
    ProtoEvt.u4Event = RM_BULK_UPDT_ABORT;

    u2MsgSize = NPSYNC_MSGTYPE_SIZE + NPSYNC_MSG_LENGTH
        + NPSYNC_MSG_NPAPI_ID_SIZE + sizeof (INT4) +
        sizeof (INT4) + sizeof (INT4) + sizeof (UINT4) + sizeof (UINT4) +
        sizeof (UINT1);
    if ((pMsg = RM_ALLOC_TX_BUF (u2MsgSize)) == NULL)
    {
        ProtoEvt.u4Error = RM_MEMALLOC_FAIL;
        QosPortRmApiHandleProtocolEvent (&ProtoEvt);
    }
    else
    {
        u2OffSet = QOS_INIT_VAL;

        NPSYNC_RM_PUT_1_BYTE (pMsg, &u2OffSet, NPSYNC_MESSAGE);
        NPSYNC_RM_PUT_2_BYTE (pMsg, &u2OffSet, u2MsgSize);
        NPSYNC_RM_PUT_4_BYTE (pMsg, &u2OffSet, u4NpApiId);
        NPSYNC_RM_PUT_4_BYTE (pMsg, &u2OffSet, wMapClassToQueueId.u4ClassMapId);
        NPSYNC_RM_PUT_4_BYTE (pMsg, &u2OffSet, wMapClassToQueueId.u4IfIndex);
        NPSYNC_RM_PUT_4_BYTE (pMsg, &u2OffSet,
                              wMapClassToQueueId.u4ClsOrPriType);
        NPSYNC_RM_PUT_4_BYTE (pMsg, &u2OffSet, wMapClassToQueueId.u4ClsOrPri);
        NPSYNC_RM_PUT_4_BYTE (pMsg, &u2OffSet, wMapClassToQueueId.u4QId);
        NPSYNC_RM_PUT_1_BYTE (pMsg, &u2OffSet, wMapClassToQueueId.u1Flag);
        if (QosPortEnqMsgToRm (pMsg, u2OffSet) == RM_FAILURE)
        {
            ProtoEvt.u4Error = RM_SENDTO_FAIL;
            QosPortRmApiHandleProtocolEvent (&ProtoEvt);
        }
    }
    return;
}

/* -------------------------------------------------------------
 *
 * Function: NpSyncFsQosHwConfigPfcSync
 *
 * -------------------------------------------------------------
 */
VOID                NpSyncFsQosHwConfigPfcSync
    (tNpSyncFsQosHwConfigPfc sHwConfigPfc, UINT4 u4AppId, UINT4 u4NpApiId)
{
    tRmProtoEvt         ProtoEvt;
    tRmMsg             *pMsg = NULL;
    UINT2               u2OffSet = QOS_INIT_VAL;
    UINT2               u2MsgSize = QOS_INIT_VAL;

    ProtoEvt.u4AppId = u4AppId;
    ProtoEvt.u4Event = RM_BULK_UPDT_ABORT;

    u2MsgSize = NPSYNC_MSGTYPE_SIZE + NPSYNC_MSG_LENGTH +
        NPSYNC_MSG_NPAPI_ID_SIZE + sizeof (INT4) + sizeof (UINT1)
        + sizeof (UINT1) + sizeof (UINT1);
    if ((pMsg = RM_ALLOC_TX_BUF (u2MsgSize)) == NULL)
    {
        ProtoEvt.u4Error = RM_MEMALLOC_FAIL;
        QosPortRmApiHandleProtocolEvent (&ProtoEvt);
    }
    else
    {
        u2OffSet = QOS_INIT_VAL;

        NPSYNC_RM_PUT_1_BYTE (pMsg, &u2OffSet, NPSYNC_MESSAGE);
        NPSYNC_RM_PUT_2_BYTE (pMsg, &u2OffSet, u2MsgSize);
        NPSYNC_RM_PUT_4_BYTE (pMsg, &u2OffSet, u4NpApiId);
        NPSYNC_RM_PUT_4_BYTE (pMsg, &u2OffSet, sHwConfigPfc.i4IfIndex);
        NPSYNC_RM_PUT_1_BYTE (pMsg, &u2OffSet, sHwConfigPfc.u1PfcPriority);
        NPSYNC_RM_PUT_1_BYTE (pMsg, &u2OffSet, sHwConfigPfc.u1Profile);
        NPSYNC_RM_PUT_1_BYTE (pMsg, &u2OffSet, sHwConfigPfc.u1Flag);
        if (QosPortEnqMsgToRm (pMsg, u2OffSet) == RM_FAILURE)
        {
            ProtoEvt.u4Error = RM_SENDTO_FAIL;
            QosPortRmApiHandleProtocolEvent (&ProtoEvt);
        }
    }
    return;
}

/* -------------------------------------------------------------
 *
 * Function: NpSyncQoSHwSetCpuRateLimitSync
 *
 * -------------------------------------------------------------
 */
VOID                NpSyncQoSHwSetCpuRateLimitSync
    (tNpSyncQoSHwSetCpuRateLimit wSetCpuRateLimit,
     UINT4 u4AppId, UINT4 u4NpApiId)
{
    tRmProtoEvt         ProtoEvt;
    tRmMsg             *pMsg = NULL;
    UINT2               u2OffSet = QOS_INIT_VAL;
    UINT2               u2MsgSize = QOS_INIT_VAL;

    ProtoEvt.u4AppId = u4AppId;
    ProtoEvt.u4Event = RM_BULK_UPDT_ABORT;

    u2MsgSize = NPSYNC_MSGTYPE_SIZE + NPSYNC_MSG_LENGTH +
        NPSYNC_MSG_NPAPI_ID_SIZE + sizeof (INT4) + sizeof (UINT4)
        + sizeof (UINT4);
    if ((pMsg = RM_ALLOC_TX_BUF (u2MsgSize)) == NULL)
    {
        ProtoEvt.u4Error = RM_MEMALLOC_FAIL;
        QosPortRmApiHandleProtocolEvent (&ProtoEvt);
    }
    else
    {
        u2OffSet = QOS_INIT_VAL;

        NPSYNC_RM_PUT_1_BYTE (pMsg, &u2OffSet, NPSYNC_MESSAGE);
        NPSYNC_RM_PUT_2_BYTE (pMsg, &u2OffSet, u2MsgSize);
        NPSYNC_RM_PUT_4_BYTE (pMsg, &u2OffSet, u4NpApiId);
        NPSYNC_RM_PUT_4_BYTE (pMsg, &u2OffSet, wSetCpuRateLimit.i4CpuQueueId);
        NPSYNC_RM_PUT_4_BYTE (pMsg, &u2OffSet, wSetCpuRateLimit.u4MinRate);
        NPSYNC_RM_PUT_4_BYTE (pMsg, &u2OffSet, wSetCpuRateLimit.u4MaxRate);
        if (QosPortEnqMsgToRm (pMsg, u2OffSet) == RM_FAILURE)
        {
            ProtoEvt.u4Error = RM_SENDTO_FAIL;
            QosPortRmApiHandleProtocolEvent (&ProtoEvt);
        }
    }
    return;
}

/*------------------------------------------------*/

/* -------------------------------------------------------------
 *
 * Function: NpSyncProcessSyncMsg
 *
 * -------------------------------------------------------------
 */
PUBLIC VOID
QosHwProcessNpSyncMsg (tRmMsg * pMsg, UINT2 *pu2OffSet)
{
    INT4                i4NpApiId = QOS_INIT_VAL;
    unNpSync            NpSync;

    MEMSET (&NpSync, QOS_INIT_VAL, sizeof (unNpSync));
    NPSYNC_RM_GET_4_BYTE (pMsg, pu2OffSet, i4NpApiId);
    switch (i4NpApiId)
    {
        case NPSYNC_QO_S_HW_INIT:
            break;

        case NPSYNC_QO_S_HW_MAP_CLASS_TO_POLICY:
            NPSYNC_RM_GET_4_BYTE (pMsg, pu2OffSet,
                                  NpSync.QoSHwMapClassToPolicy.ClassMapNode.
                                  u4PriorityMapId);
            NPSYNC_RM_GET_4_BYTE (pMsg, pu2OffSet,
                                  NpSync.QoSHwMapClassToPolicy.ClassMapNode.
                                  u4L2FilterId);
            NPSYNC_RM_GET_4_BYTE (pMsg, pu2OffSet,
                                  NpSync.QoSHwMapClassToPolicy.ClassMapNode.
                                  u4L3FilterId);
            NPSYNC_RM_GET_4_BYTE (pMsg, pu2OffSet,
                                  NpSync.QoSHwMapClassToPolicy.ClassMapNode.
                                  u4ClassId);
            NPSYNC_RM_GET_4_BYTE (pMsg, pu2OffSet,
                                  NpSync.QoSHwMapClassToPolicy.u4PlyMapId);

            break;

        case NPSYNC_QO_S_HW_UPDATE_POLICY_MAP_FOR_CLASS:
            NPSYNC_RM_GET_4_BYTE (pMsg, pu2OffSet,
                                  NpSync.QoSHwUpdatePolicyMapForClass.
                                  ClassMapNode.u4PriorityMapId);
            NPSYNC_RM_GET_4_BYTE (pMsg, pu2OffSet,
                                  NpSync.QoSHwUpdatePolicyMapForClass.
                                  ClassMapNode.u4L2FilterId);
            NPSYNC_RM_GET_4_BYTE (pMsg, pu2OffSet,
                                  NpSync.QoSHwUpdatePolicyMapForClass.
                                  ClassMapNode.u4L3FilterId);
            NPSYNC_RM_GET_4_BYTE (pMsg, pu2OffSet,
                                  NpSync.QoSHwUpdatePolicyMapForClass.
                                  ClassMapNode.u4ClassId);
            NPSYNC_RM_GET_4_BYTE (pMsg, pu2OffSet,
                                  NpSync.QoSHwUpdatePolicyMapForClass.
                                  u4PlyMapId);
            break;

        case NPSYNC_QO_S_HW_UNMAP_CLASS_FROM_POLICY:
            NPSYNC_RM_GET_4_BYTE (pMsg, pu2OffSet,
                                  NpSync.QoSHwUnmapClassFromPolicy.ClassMapNode.
                                  u4PriorityMapId);
            NPSYNC_RM_GET_4_BYTE (pMsg, pu2OffSet,
                                  NpSync.QoSHwUnmapClassFromPolicy.ClassMapNode.
                                  u4L2FilterId);
            NPSYNC_RM_GET_4_BYTE (pMsg, pu2OffSet,
                                  NpSync.QoSHwUnmapClassFromPolicy.ClassMapNode.
                                  u4L3FilterId);
            NPSYNC_RM_GET_4_BYTE (pMsg, pu2OffSet,
                                  NpSync.QoSHwUnmapClassFromPolicy.ClassMapNode.
                                  u4ClassId);
            NPSYNC_RM_GET_4_BYTE (pMsg, pu2OffSet,
                                  NpSync.QoSHwUnmapClassFromPolicy.u4PlyMapId);
            break;

        case NPSYNC_QO_S_HW_DELETE_CLASS_MAP_ENTRY:
            NPSYNC_RM_GET_4_BYTE (pMsg, pu2OffSet,
                                  NpSync.QoSHwDeleteClassMapEntry.ClassMapNode.
                                  u4PriorityMapId);
            NPSYNC_RM_GET_4_BYTE (pMsg, pu2OffSet,
                                  NpSync.QoSHwDeleteClassMapEntry.ClassMapNode.
                                  u4L2FilterId);
            NPSYNC_RM_GET_4_BYTE (pMsg, pu2OffSet,
                                  NpSync.QoSHwDeleteClassMapEntry.ClassMapNode.
                                  u4L3FilterId);
            NPSYNC_RM_GET_4_BYTE (pMsg, pu2OffSet,
                                  NpSync.QoSHwDeleteClassMapEntry.ClassMapNode.
                                  u4ClassId);
            break;

        case NPSYNC_QO_S_HW_METER_CREATE:
            NPSYNC_RM_GET_4_BYTE (pMsg, pu2OffSet,
                                  NpSync.QoSHwMeterCreate.u4MeterId);
            break;

        case NPSYNC_QO_S_HW_METER_DELETE:
            NPSYNC_RM_GET_4_BYTE (pMsg, pu2OffSet,
                                  NpSync.QoSHwMeterDelete.u4MeterId);
            break;

        case NPSYNC_QO_S_HW_SCHEDULER_ADD:
            NPSYNC_RM_GET_4_BYTE (pMsg, pu2OffSet,
                                  NpSync.QoSHwSchedulerAdd.u4IfIndex);
            NPSYNC_RM_GET_4_BYTE (pMsg, pu2OffSet,
                                  NpSync.QoSHwSchedulerAdd.u4SchedId);
            break;

        case NPSYNC_QO_S_HW_SCHEDULER_UPDATE_PARAMS:
            NPSYNC_RM_GET_4_BYTE (pMsg, pu2OffSet,
                                  NpSync.QoSHwSchedulerUpdateParams.u4IfIndex);
            NPSYNC_RM_GET_4_BYTE (pMsg, pu2OffSet,
                                  NpSync.QoSHwSchedulerUpdateParams.u4SchedId);
            break;

        case NPSYNC_QO_S_HW_SCHEDULER_DELETE:
            NPSYNC_RM_GET_4_BYTE (pMsg, pu2OffSet,
                                  NpSync.QoSHwSchedulerDelete.u4IfIndex);
            NPSYNC_RM_GET_4_BYTE (pMsg, pu2OffSet,
                                  NpSync.QoSHwSchedulerDelete.u4SchedId);
            break;

        case NPSYNC_QO_S_HW_QUEUE_CREATE:
            NPSYNC_RM_GET_4_BYTE (pMsg, pu2OffSet,
                                  NpSync.QoSHwQueueCreate.u4IfIndex);
            NPSYNC_RM_GET_4_BYTE (pMsg, pu2OffSet,
                                  NpSync.QoSHwQueueCreate.u4Id);
            break;

        case NPSYNC_QO_S_HW_QUEUE_DELETE:
            NPSYNC_RM_GET_4_BYTE (pMsg, pu2OffSet,
                                  NpSync.QoSHwQueueDelete.u4IfIndex);
            NPSYNC_RM_GET_4_BYTE (pMsg, pu2OffSet,
                                  NpSync.QoSHwQueueDelete.u4Id);
            break;

        case NPSYNC_QO_S_HW_MAP_CLASS_TO_QUEUE:
            NPSYNC_RM_GET_4_BYTE (pMsg, pu2OffSet,
                                  NpSync.QoSHwMapClassToQueue.u4IfIndex);
            NPSYNC_RM_GET_4_BYTE (pMsg, pu2OffSet,
                                  NpSync.QoSHwMapClassToQueue.u4ClsOrPriType);
            NPSYNC_RM_GET_4_BYTE (pMsg, pu2OffSet,
                                  NpSync.QoSHwMapClassToQueue.u4ClsOrPri);
            NPSYNC_RM_GET_4_BYTE (pMsg, pu2OffSet,
                                  NpSync.QoSHwMapClassToQueue.u4QId);
            NPSYNC_RM_GET_1_BYTE (pMsg, pu2OffSet,
                                  NpSync.QoSHwMapClassToQueue.u1Flag);
            break;

        case NPSYNC_QO_S_HW_SCHEDULER_HIERARCHY_MAP:
            NPSYNC_RM_GET_4_BYTE (pMsg, pu2OffSet,
                                  NpSync.QoSHwSchedulerHierarchyMap.u4IfIndex);
            NPSYNC_RM_GET_4_BYTE (pMsg, pu2OffSet,
                                  NpSync.QoSHwSchedulerHierarchyMap.u4SchedId);
            NPSYNC_RM_GET_2_BYTE (pMsg, pu2OffSet,
                                  NpSync.QoSHwSchedulerHierarchyMap.u2Sweight);
            NPSYNC_RM_GET_1_BYTE (pMsg, pu2OffSet,
                                  NpSync.QoSHwSchedulerHierarchyMap.
                                  u1Spriority);
            NPSYNC_RM_GET_4_BYTE (pMsg, pu2OffSet,
                                  NpSync.QoSHwSchedulerHierarchyMap.
                                  u4NextSchedId);
            NPSYNC_RM_GET_4_BYTE (pMsg, pu2OffSet,
                                  NpSync.QoSHwSchedulerHierarchyMap.u4NextQId);
            NPSYNC_RM_GET_2_BYTE (pMsg, pu2OffSet,
                                  NpSync.QoSHwSchedulerHierarchyMap.u2HL);
            NPSYNC_RM_GET_1_BYTE (pMsg, pu2OffSet,
                                  NpSync.QoSHwSchedulerHierarchyMap.u1Flag);
            break;

        case NPSYNC_QO_S_HW_SET_DEF_USER_PRIORITY:
            NPSYNC_RM_GET_4_BYTE (pMsg, pu2OffSet,
                                  NpSync.QoSHwSetDefUserPriority.u4Port);
            NPSYNC_RM_GET_4_BYTE (pMsg, pu2OffSet,
                                  NpSync.QoSHwSetDefUserPriority.u4DefPriority);
            break;

        case NPSYNC_QO_S_HW_MAP_CLASS_TO_QUEUE_ID:
            NPSYNC_RM_GET_4_BYTE (pMsg, pu2OffSet,
                                  NpSync.QoSHwMapClassToQueueId.u4ClassMapId);
            NPSYNC_RM_GET_4_BYTE (pMsg, pu2OffSet,
                                  NpSync.QoSHwMapClassToQueueId.u4IfIndex);
            NPSYNC_RM_GET_4_BYTE (pMsg, pu2OffSet,
                                  NpSync.QoSHwMapClassToQueueId.u4ClsOrPriType);
            NPSYNC_RM_GET_4_BYTE (pMsg, pu2OffSet,
                                  NpSync.QoSHwMapClassToQueueId.u4ClsOrPri);
            NPSYNC_RM_GET_4_BYTE (pMsg, pu2OffSet,
                                  NpSync.QoSHwMapClassToQueueId.u4QId);
            NPSYNC_RM_GET_1_BYTE (pMsg, pu2OffSet,
                                  NpSync.QoSHwMapClassToQueueId.u1Flag);
            break;
        case NPSYNC_FS_QOS_HW_CONFIG_PFC:
            NPSYNC_RM_GET_4_BYTE (pMsg, pu2OffSet,
                                  NpSync.FsQosHwConfigPfc.i4IfIndex);
            NPSYNC_RM_GET_1_BYTE (pMsg, pu2OffSet,
                                  NpSync.FsQosHwConfigPfc.u1PfcPriority);
            NPSYNC_RM_GET_1_BYTE (pMsg, pu2OffSet,
                                  NpSync.FsQosHwConfigPfc.u1Profile);
            NPSYNC_RM_GET_1_BYTE (pMsg, pu2OffSet,
                                  NpSync.FsQosHwConfigPfc.u1Flag);
            break;

        default:
            break;

    }                            /* switch */
    QosHwAuditCreateOrFlushBuffer (&NpSync, i4NpApiId, QOS_INIT_VAL);
    return;
}

/**********************************************************
 *                                                                            
 *  Function Name   : QosHwAuditCreateOrFlushBuffer                           
 *                                                                           
 *  Description     : This function performs checks whether a buffer entry    
 *                      
 *                                                                            
 *  Input(s)        : pBuf - Buffer Entry                                   
 *                                                                          
 *  Output(s)       : None                                                    
 *                                                                            
 *  Global Variables Referred :None                                         
 *                                                                            
 *  Global variables Modified :None                                         
 *                                                                            
 *  Exceptions or Operating System Error Handling : None                      
 *                                                                            
 *  Use of Recursion : None                                                   
 *                                                                          
 *  Returns         : None                                                    
 *************************************************************/

VOID
QosHwAuditCreateOrFlushBuffer (unNpSync * pNpSync, UINT4 u4NpApiId,
                               UINT4 u4EventId)
{
    tQosRedNpSyncEntry *pBuf = NULL;
    tQosRedNpSyncEntry *pTempBuf = NULL;
    tQosRedNpSyncEntry *pPrevNode = NULL;
    UINT1               u1Match = OSIX_FALSE;

    UNUSED_PARAM (u4EventId);

    if (u4NpApiId == QOS_INIT_VAL)
    {
        /* Currently we are not having any Event sync up QOS */
        return;
    }

    /* Scan the Buffer entries and delete it */
    TMO_DYN_SLL_Scan (&gQoSGlobalInfo.RedGlobalInfo.HwAuditTable,
                      pBuf, pTempBuf, tQosRedNpSyncEntry *)
    {
        if (u4NpApiId == pBuf->u4NpApiId)
        {
            if (MEMCMP (&(pBuf->unNpData), pNpSync, sizeof (unNpSync))
                == QOS_FAILURE)
            {
                TMO_SLL_Delete (&gQoSGlobalInfo.
                                RedGlobalInfo.HwAuditTable, &(pBuf->Node));
                MemReleaseMemBlock (gQoSGlobalInfo.RedGlobalInfo.
                                    HwAuditTablePoolId, (UINT1 *) pBuf);
                pBuf = NULL;
                u1Match = OSIX_TRUE;
                break;
            }
        }

        pPrevNode = pBuf;
    }

    if (u1Match == OSIX_FALSE)
    {
        pTempBuf = NULL;
        /* allocate for the tunnel table entry */
        pTempBuf = (tQosRedNpSyncEntry *) MemAllocMemBlk
            (gQoSGlobalInfo.RedGlobalInfo.HwAuditTablePoolId);
        if (pTempBuf == NULL)
        {
            QOS_TRC (ALL_FAILURE_TRC, "QosHwAuditCreateOrFlushBuffer:"
                     "Hardware Audit entry memory allocation failed !!!\n");
            return;
        }

        TMO_SLL_Init_Node (&(pTempBuf->Node));

        MEMCPY (&pTempBuf->unNpData, pNpSync, sizeof (unNpSync));
        pTempBuf->u4NpApiId = u4NpApiId;
        pTempBuf->u4EventId = u4EventId;
        if (pPrevNode == NULL)
        {
            /* Node to be inserted in first position */
            TMO_SLL_Insert (&gQoSGlobalInfo.RedGlobalInfo.HwAuditTable, NULL,
                            (tTMO_SLL_NODE *) & (pTempBuf->Node));
        }
        else
        {
            /* Node to be inserted in last position */
            TMO_SLL_Insert (&gQoSGlobalInfo.RedGlobalInfo.HwAuditTable,
                            (tTMO_SLL_NODE *) & (pPrevNode->Node),
                            (tTMO_SLL_NODE *) & (pTempBuf->Node));
        }
    }
}
