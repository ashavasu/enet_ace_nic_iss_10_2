/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: stdqoslw.c,v 1.37 2015/10/31 12:19:54 siva Exp $
*
* Description: Protocol Low Level Routines
*********************************************************************/
# include  "qosinc.h"
# include  "stdqoscli.h"
# include  "fsqosxcli.h"
/* This file is using ACL nmh routines. When HA feature of ACL is supported
 * wrappers are created for these nmh routines. So following header file is 
 * required */
# include  "fsissawr.h"
#ifdef NPAPI_WANTED
#if defined (XCAT) || defined (LION) || defined (LION_DB) || defined(DX167)
# include  "xcat/fsissacli.h"
#elif FULCRUM_WANTED
# include "fulcrum/fsissacli.h"
#else
# include "bcm/fsissacli.h"
#endif
#else
# include  "others/fsissacli.h"
#endif
/* LOW LEVEL Routines for Table : DiffServDataPathTable. */
/****************************************************************************
 Function    :  nmhValidateIndexInstanceDiffServDataPathTable
 Input       :  The Indices
                IfIndex
                DiffServDataPathIfDirection
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceDiffServDataPathTable (INT4 i4IfIndex,
                                               INT4
                                               i4DiffServDataPathIfDirection)
{
    if (gQoSGlobalInfo.eSysControl != QOS_SYS_CNTL_START)
    {
        QOS_TRC (MGMT_TRC, QOS_MODULE_SHUTDOW_ERROR_MSG);
        return (SNMP_FAILURE);
    }
    if (CfaValidateIfIndex (i4IfIndex) == CFA_FAILURE)
    {
        return SNMP_FAILURE;
    }
    if ((i4DiffServDataPathIfDirection != QOS_DATA_IN) &&
        (i4DiffServDataPathIfDirection != QOS_DATA_OUT))
    {
        return SNMP_FAILURE;
    }
    if (QosGetDataPathEntry (i4IfIndex, i4DiffServDataPathIfDirection) == NULL)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexDiffServDataPathTable
 Input       :  The Indices
                IfIndex
                DiffServDataPathIfDirection
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexDiffServDataPathTable (INT4 *pi4IfIndex,
                                       INT4 *pi4DiffServDataPathIfDirection)
{
    tQosStdDataPathEntry *pDataPathEntry = NULL;
    if (gQoSGlobalInfo.eSysControl != QOS_SYS_CNTL_START)
    {
        QOS_TRC (MGMT_TRC, QOS_MODULE_SHUTDOW_ERROR_MSG);
        return (SNMP_FAILURE);
    }
    pDataPathEntry = (tQosStdDataPathEntry *)
        RBTreeGetFirst (gQoSGlobalInfo.DsDataPathTbl);
    if (pDataPathEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    *pi4IfIndex = pDataPathEntry->i4IfIndex;
    *pi4DiffServDataPathIfDirection = pDataPathEntry->i4DSDataPathIfDir;
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetNextIndexDiffServDataPathTable
 Input       :  The Indices
                IfIndex
                nextIfIndex
                DiffServDataPathIfDirection
                nextDiffServDataPathIfDirection
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexDiffServDataPathTable (INT4 i4IfIndex, INT4 *pi4NextIfIndex,
                                      INT4 i4DiffServDataPathIfDirection,
                                      INT4 *pi4NextDiffServDataPathIfDirection)
{
    tQosStdDataPathEntry *pDataPathEntry = NULL;
    tQosStdDataPathEntry DataPathEntry;

    if (gQoSGlobalInfo.eSysControl != QOS_SYS_CNTL_START)
    {
        QOS_TRC (MGMT_TRC, QOS_MODULE_SHUTDOW_ERROR_MSG);
        return (SNMP_FAILURE);
    }
    if ((i4IfIndex == QOS_START_INITIAL_INDEX) &&
        (i4DiffServDataPathIfDirection == QOS_START_INITIAL_INDEX))
    {
        pDataPathEntry = (tQosStdDataPathEntry *)
            RBTreeGetFirst (gQoSGlobalInfo.DsDataPathTbl);
    }
    else
    {
        DataPathEntry.i4IfIndex = i4IfIndex;
        DataPathEntry.i4DSDataPathIfDir = i4DiffServDataPathIfDirection;
        pDataPathEntry = (tQosStdDataPathEntry *)
            RBTreeGetNext (gQoSGlobalInfo.DsDataPathTbl,
                           &DataPathEntry, QoSDataPathEntryCmpFun);
    }

    if (pDataPathEntry == NULL)
    {
        QOS_TRC_ARG3 (MGMT_TRC, "In %s : RBTreeGetNext () Failed, "
                      " for Data Path (Port) Id %d and Direction Id %d.\r\n",
                      __FUNCTION__, i4IfIndex, i4DiffServDataPathIfDirection);

        return SNMP_FAILURE;
    }

    *pi4NextIfIndex = pDataPathEntry->i4IfIndex;
    *pi4NextDiffServDataPathIfDirection = pDataPathEntry->i4DSDataPathIfDir;
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetDiffServDataPathStart
 Input       :  The Indices
                IfIndex
                DiffServDataPathIfDirection

                The Object 
                retValDiffServDataPathStart
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDiffServDataPathStart (INT4 i4IfIndex, INT4 i4DiffServDataPathIfDirection,
                             tSNMP_OID_TYPE * pRetValDiffServDataPathStart)
{
    tQosStdDataPathEntry *pDataPathEntry = NULL;
    UINT4               u4Id = 0;
    UINT4               u4Type = 0;

    if (gQoSGlobalInfo.eSysControl != QOS_SYS_CNTL_START)
    {
        QOS_TRC (MGMT_TRC, QOS_MODULE_SHUTDOW_ERROR_MSG);
        return (SNMP_FAILURE);
    }
    pDataPathEntry = QosGetDataPathEntry (i4IfIndex,
                                          i4DiffServDataPathIfDirection);
    if (pDataPathEntry == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QosGetDataPathEntry () "
                      " Returns FAILURE. \r\n", __FUNCTION__);
        return SNMP_FAILURE;
    }

    u4Id = pDataPathEntry->u4RowPointerId;
    u4Type = pDataPathEntry->u4RowPointerType;

    if (QosGetOidFromTypeId (u4Id, u4Type, pRetValDiffServDataPathStart)
        != QOS_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDiffServDataPathStorage
 Input       :  The Indices
                IfIndex
                DiffServDataPathIfDirection

                The Object 
                retValDiffServDataPathStorage
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDiffServDataPathStorage (INT4 i4IfIndex,
                               INT4 i4DiffServDataPathIfDirection,
                               INT4 *pi4RetValDiffServDataPathStorage)
{
    tQosStdDataPathEntry *pDataPathEntry = NULL;

    if (gQoSGlobalInfo.eSysControl != QOS_SYS_CNTL_START)
    {
        QOS_TRC (MGMT_TRC, QOS_MODULE_SHUTDOW_ERROR_MSG);
        return (SNMP_FAILURE);
    }
    pDataPathEntry = QosGetDataPathEntry (i4IfIndex,
                                          i4DiffServDataPathIfDirection);
    if (pDataPathEntry == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QosGetDataPathEntry () "
                      " Returns FAILURE. \r\n", __FUNCTION__);
        return SNMP_FAILURE;
    }
    if ((pDataPathEntry->i4IfIndex == i4IfIndex) &&
        ((pDataPathEntry->i4DSDataPathIfDir == i4DiffServDataPathIfDirection)))
    {
        *pi4RetValDiffServDataPathStorage = pDataPathEntry->i4StorageType;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetDiffServDataPathStatus
 Input       :  The Indices
                IfIndex
                DiffServDataPathIfDirection

                The Object 
                retValDiffServDataPathStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDiffServDataPathStatus (INT4 i4IfIndex,
                              INT4 i4DiffServDataPathIfDirection,
                              INT4 *pi4RetValDiffServDataPathStatus)
{
    tQosStdDataPathEntry *pDataPathEntry = NULL;

    if (gQoSGlobalInfo.eSysControl != QOS_SYS_CNTL_START)
    {
        QOS_TRC (MGMT_TRC, QOS_MODULE_SHUTDOW_ERROR_MSG);
        return (SNMP_FAILURE);
    }
    pDataPathEntry = QosGetDataPathEntry (i4IfIndex,
                                          i4DiffServDataPathIfDirection);
    if (pDataPathEntry == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QosGetDataPathEntry () "
                      " Returns FAILURE. \r\n", __FUNCTION__);
        return SNMP_FAILURE;
    }
    if ((pDataPathEntry->i4IfIndex == i4IfIndex) &&
        ((pDataPathEntry->i4DSDataPathIfDir == i4DiffServDataPathIfDirection)))
    {
        *pi4RetValDiffServDataPathStatus = pDataPathEntry->u1DataPathStatus;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetDiffServDataPathStart
 Input       :  The Indices
                IfIndex
                DiffServDataPathIfDirection

                The Object 
                setValDiffServDataPathStart
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDiffServDataPathStart (INT4 i4IfIndex, INT4 i4DiffServDataPathIfDirection,
                             tSNMP_OID_TYPE * pSetValDiffServDataPathStart)
{
    tQosStdDataPathEntry *pDataPathEntry = NULL;
    UINT4               u4Id = QOS_ROWPOINTER_DEF;
    UINT4               u4Type = QOS_ROWPOINTER_DEF;
    UINT4               u4CurrentType = DS_DATAPATH;
    tSnmpNotifyInfo     SnmpNotifyInfo;

    pDataPathEntry = QosGetDataPathEntry (i4IfIndex,
                                          i4DiffServDataPathIfDirection);
    if (pDataPathEntry == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QosGetDataPathEntry () "
                      " Returns FAILURE. \r\n", __FUNCTION__);
        return SNMP_FAILURE;
    }
    if (QosGetTypeIdFromOid (pSetValDiffServDataPathStart, &u4Type,
                             &u4Id) == QOS_SUCCESS)
    {
        pDataPathEntry->u4RowPointerId = u4Id;
        pDataPathEntry->u4RowPointerType = u4Type;
        QosUtlPointedToStatus (&u4CurrentType, (UINT4 *) &i4IfIndex,
                               (UINT4 *) &i4DiffServDataPathIfDirection, u4Type,
                               u4Id, 0, QOS_ADD);
        QoSValidateDataPathTableStatus (pDataPathEntry);
        SnmpNotifyInfo.pu4ObjectId = DiffServDataPathStart;
        SnmpNotifyInfo.u4OidLen =
            sizeof (DiffServDataPathStart) / sizeof (UINT4);
        SnmpNotifyInfo.u4SeqNum = 0;
        SnmpNotifyInfo.u1RowStatus = FALSE;
        SnmpNotifyInfo.pLockPointer = QoSLock;
        SnmpNotifyInfo.pUnLockPointer = QoSUnLock;
        SnmpNotifyInfo.u4Indices = 2;
        SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;

        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i %o", i4IfIndex,
                          i4DiffServDataPathIfDirection,
                          pSetValDiffServDataPathStart));
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetDiffServDataPathStorage
 Input       :  The Indices
                IfIndex
                DiffServDataPathIfDirection

                The Object 
                setValDiffServDataPathStorage
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDiffServDataPathStorage (INT4 i4IfIndex,
                               INT4 i4DiffServDataPathIfDirection,
                               INT4 i4SetValDiffServDataPathStorage)
{
    tQosStdDataPathEntry *pDataPathEntry = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;

    pDataPathEntry = QosGetDataPathEntry (i4IfIndex,
                                          i4DiffServDataPathIfDirection);
    if (pDataPathEntry == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QosGetDataPathEntry () "
                      " Returns FAILURE. \r\n", __FUNCTION__);
        return SNMP_FAILURE;
    }
    if ((pDataPathEntry->i4IfIndex != i4IfIndex) ||
        ((pDataPathEntry->i4DSDataPathIfDir != i4DiffServDataPathIfDirection)))
    {
        return SNMP_FAILURE;
    }
    pDataPathEntry->i4StorageType = i4SetValDiffServDataPathStorage;
    QoSValidateDataPathTableStatus (pDataPathEntry);
    SnmpNotifyInfo.pu4ObjectId = DiffServDataPathStorage;
    SnmpNotifyInfo.u4OidLen = sizeof (DiffServDataPathStorage) / sizeof (UINT4);
    SnmpNotifyInfo.u4SeqNum = 0;
    SnmpNotifyInfo.u1RowStatus = FALSE;
    SnmpNotifyInfo.pLockPointer = QoSLock;
    SnmpNotifyInfo.pUnLockPointer = QoSUnLock;
    SnmpNotifyInfo.u4Indices = 2;
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i %i", i4IfIndex,
                      i4DiffServDataPathIfDirection,
                      i4SetValDiffServDataPathStorage));
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetDiffServDataPathStatus
 Input       :  The Indices
                IfIndex
                DiffServDataPathIfDirection

                The Object 
                setValDiffServDataPathStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDiffServDataPathStatus (INT4 i4IfIndex,
                              INT4 i4DiffServDataPathIfDirection,
                              INT4 i4SetValDiffServDataPathStatus)
{
    tQosStdDataPathEntry *pDataPathEntry = NULL;
    tQosStdClfrEntry   *pClfrEntry = NULL;
    UINT4               u4RowPointerType, u4RowPointerId;
    UINT4               u4CurrentType = DS_DATAPATH;
    INT4                i4RetStatus = SNMP_FAILURE;
    INT4                i4RowStatus = SNMP_FAILURE;
    tSnmpNotifyInfo     SnmpNotifyInfo;

    pDataPathEntry = QosGetDataPathEntry (i4IfIndex,
                                          i4DiffServDataPathIfDirection);
    if (pDataPathEntry != NULL)
    {
        i4RowStatus = (INT4) pDataPathEntry->u1DataPathStatus;
        /* Entry found in the Table */
        if (i4RowStatus == i4SetValDiffServDataPathStatus)
        {
            /* same value */
            return (SNMP_SUCCESS);
        }
    }
    else
    {
        /* New Entry need to be created in the Table, only if *
         * i4SetValDiffServDataPathStatus = QOS_CREATE_AND_WAIT      */
        if (i4SetValDiffServDataPathStatus != QOS_CREATE_AND_WAIT)
        {
            return SNMP_FAILURE;
        }
    }
    switch (i4SetValDiffServDataPathStatus)
    {
            /* For a New Entry */
        case QOS_CREATE_AND_WAIT:
            pDataPathEntry = QoSCreateDataPathEntry (i4IfIndex,
                                                     i4DiffServDataPathIfDirection);
            if (pDataPathEntry == NULL)
            {
                QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSCreateDataPathEntry () "
                              " Returns FAILURE. \r\n", __FUNCTION__);
                return SNMP_FAILURE;
            }
            break;
        case QOS_ACTIVE:
            if (pDataPathEntry->u1DataPathStatus == QOS_NOT_IN_SERVICE)
            {
                /* This currently doesnt have anything to do with hardware. *
                 * So we just have to set the RowStatus to Active*/

                pDataPathEntry->u1DataPathStatus = QOS_ACTIVE;
            }
            if (pDataPathEntry->u4RowPointerType == DS_CLFR)
            {
                if ((pClfrEntry =
                     QosGetClfrEntry (pDataPathEntry->u4RowPointerId)) != NULL)
                {
                    QOS_SET_CLFR_MEMBER_PORT (pClfrEntry, (UINT4) i4IfIndex);
                }

            }
            break;

        case QOS_NOT_IN_SERVICE:

            if ((pDataPathEntry->u1DataPathStatus == QOS_ACTIVE) &&
                (gQoSGlobalInfo.eSysStatus == QOS_SYS_STATUS_ENABLE))
            {
                if (pDataPathEntry->u4RowPointerType == DS_CLFR)
                {
                    if ((pClfrEntry =
                         QosGetClfrEntry (pDataPathEntry->u4RowPointerId)) !=
                        NULL)
                    {
                        QOS_RESET_CLFR_MEMBER_PORT (pClfrEntry,
                                                    (UINT4) i4IfIndex);
                    }
                }

                /* This currently doesnt have anything to do with hardware. *
                 * So we just have to set the RowStatus to Not in Service*/
                pDataPathEntry->u1DataPathStatus = QOS_NOT_IN_SERVICE;
            }
            break;

        case QOS_DESTROY:
            u4RowPointerType = pDataPathEntry->u4RowPointerType;
            u4RowPointerId = pDataPathEntry->u4RowPointerId;

            i4RetStatus = QoSDeleteDataPathEntry (pDataPathEntry);

            QosUtlPointedToStatus (&u4CurrentType, (UINT4 *) &i4IfIndex,
                                   (UINT4 *) &i4DiffServDataPathIfDirection,
                                   u4RowPointerType, u4RowPointerId, 0,
                                   QOS_DEL);
            if (u4RowPointerType == DS_CLFR)
            {
                if ((pClfrEntry =
                     QosGetClfrEntry (u4RowPointerId)) != NULL)
                {
                    QOS_RESET_CLFR_MEMBER_PORT (pClfrEntry, (UINT4) i4IfIndex);
                }
            }

            if (i4RetStatus == QOS_FAILURE)
            {
                QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSDeleteDataPathEntry () "
                              " Returns FAILURE. \r\n", __FUNCTION__);

                return (SNMP_FAILURE);
            }
            break;
        case QOS_CREATE_AND_GO:
            return (SNMP_FAILURE);
        default:
            return (SNMP_FAILURE);
    }
    SnmpNotifyInfo.pu4ObjectId = DiffServDataPathStatus;
    SnmpNotifyInfo.u4OidLen = sizeof (DiffServDataPathStatus) / sizeof (UINT4);
    SnmpNotifyInfo.u4SeqNum = 0;
    SnmpNotifyInfo.u1RowStatus = TRUE;
    SnmpNotifyInfo.pLockPointer = QoSLock;
    SnmpNotifyInfo.pUnLockPointer = QoSUnLock;
    SnmpNotifyInfo.u4Indices = 2;
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i %i", i4IfIndex,
                      i4DiffServDataPathIfDirection,
                      i4SetValDiffServDataPathStatus));
    return (SNMP_SUCCESS);
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2DiffServDataPathStart
 Input       :  The Indices
                IfIndex
                DiffServDataPathIfDirection

                The Object 
                testValDiffServDataPathStart
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2DiffServDataPathStart (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                                INT4 i4DiffServDataPathIfDirection,
                                tSNMP_OID_TYPE * pTestValDiffServDataPathStart)
{
    UINT4               u4Type = 0;
    UINT4               u4Id = 0;
    INT1                i1ReturnStatus = SNMP_FAILURE;

    if (QosUtlValidateDataPathEntry (i4IfIndex,
                                     i4DiffServDataPathIfDirection,
                                     pu4ErrorCode) == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QosUtlValidateDataPathEntry () "
                      "Returns FAILURE. \r\n", __FUNCTION__);
        return SNMP_FAILURE;
    }
    if (QosGetTypeIdFromOid (pTestValDiffServDataPathStart, &u4Type, &u4Id)
        == QOS_SUCCESS)
    {
#ifdef NPAPI_WANTED
        /*1st param : Currentype, 2nd param: 1stNextType
         * 3rd param: 2ndNextType if any, 4thParam: 3rdNexttype
         * if any , 5thParm : Specific type if any*/
        if (QosIsTypeValid
            (DS_DATAPATH, u4Type, QOS_ROWPOINTER_DEF, QOS_ROWPOINTER_DEF,
             QOS_ROWPOINTER_DEF) != QOS_SUCCESS)
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
        }
#endif
        /*To check whether the given Index is valid for the given type */
        if (QosValidateIndex (u4Id, u4Type) != QOS_SUCCESS)
        {
            return SNMP_FAILURE;
        }
        i1ReturnStatus = SNMP_SUCCESS;
    }
    switch (i4DiffServDataPathIfDirection)
    {
        case QOS_DATA_IN:

            if ((u4Type == DS_CLFR) || (u4Type == DS_METER) ||
                (u4Type == DS_ACTION))
            {
                i1ReturnStatus = SNMP_SUCCESS;
            }
            break;
        case QOS_DATA_OUT:

            if ((u4Type == DS_QUEUE) || (u4Type == DS_ALG_DROP))
            {
                i1ReturnStatus = SNMP_SUCCESS;
            }
            break;
        default:
            break;
    }
    if (i1ReturnStatus == SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
    /*The OID Could no be found in the Static Table */
    return i1ReturnStatus;
}

/****************************************************************************
 Function    :  nmhTestv2DiffServDataPathStorage
 Input       :  The Indices
                IfIndex
                DiffServDataPathIfDirection

                The Object 
                testValDiffServDataPathStorage
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2DiffServDataPathStorage (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                                  INT4 i4DiffServDataPathIfDirection,
                                  INT4 i4TestValDiffServDataPathStorage)
{
    if (QosUtlValidateDataPathEntry (i4IfIndex,
                                     i4DiffServDataPathIfDirection,
                                     pu4ErrorCode) == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QosUtlValidateDataPathEntry () "
                      "Returns FAILURE. \r\n", __FUNCTION__);
        return SNMP_FAILURE;
    }
    if ((i4TestValDiffServDataPathStorage != QOS_STORAGE_VOLATILE) &&
        (i4TestValDiffServDataPathStorage != QOS_STORAGE_NONVOLATILE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2DiffServDataPathStatus
 Input       :  The Indices
                IfIndex
                DiffServDataPathIfDirection

                The Object 
                testValDiffServDataPathStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2DiffServDataPathStatus (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                                 INT4 i4DiffServDataPathIfDirection,
                                 INT4 i4TestValDiffServDataPathStatus)
{
    tQosStdDataPathEntry *pDataPathEntry = NULL;
    INT4                i4ReturnStatus = SNMP_FAILURE;

    if (gQoSGlobalInfo.eSysControl != QOS_SYS_CNTL_START)
    {
        QOS_TRC (MGMT_TRC, QOS_MODULE_SHUTDOW_ERROR_MSG);
        return (SNMP_FAILURE);
    }
    if (CfaValidateIfIndex (i4IfIndex) == CFA_FAILURE)
    {
        return SNMP_FAILURE;
    }
    if ((i4DiffServDataPathIfDirection != QOS_DATA_IN) &&
        (i4DiffServDataPathIfDirection != QOS_DATA_OUT))
    {
        return SNMP_FAILURE;
    }
    if (QOS_DATA_PATH_ID_VALID (i4IfIndex) != QOS_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    pDataPathEntry = QosGetDataPathEntry (i4IfIndex,
                                          i4DiffServDataPathIfDirection);
    if (pDataPathEntry == NULL)
    {
        /*Entry Not found in the table */
        switch (i4TestValDiffServDataPathStatus)
        {
            case QOS_CREATE_AND_WAIT:
                i4ReturnStatus = SNMP_SUCCESS;
                break;
            default:
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                i4ReturnStatus = SNMP_FAILURE;
                break;
        }
    }
    else
    {
        switch (i4TestValDiffServDataPathStatus)
        {
                /*Entry Found in the table */
            case QOS_ACTIVE:
                if ((pDataPathEntry->u1DataPathStatus == QOS_NOT_READY))
                {
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                    i4ReturnStatus = SNMP_FAILURE;
                }
                else
                {
                    i4ReturnStatus = SNMP_SUCCESS;
                }
                break;
            case QOS_NOT_IN_SERVICE:

                if ((pDataPathEntry->u1DataPathStatus == QOS_NOT_READY))
                {
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                    i4ReturnStatus = SNMP_FAILURE;
                }
                i4ReturnStatus = SNMP_SUCCESS;
                break;

            case QOS_CREATE_AND_WAIT:
                i4ReturnStatus = SNMP_SUCCESS;
                break;
            case QOS_CREATE_AND_GO:
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                i4ReturnStatus = SNMP_FAILURE;
                break;
            case QOS_DESTROY:
                i4ReturnStatus = SNMP_SUCCESS;
                break;

            default:
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                i4ReturnStatus = SNMP_FAILURE;
                break;
        }
    }
    if (i4ReturnStatus == SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2DiffServDataPathTable
 Input       :  The Indices
                IfIndex
                DiffServDataPathIfDirection
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2DiffServDataPathTable (UINT4 *pu4ErrorCode,
                               tSnmpIndexList * pSnmpIndexList,
                               tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetDiffServClfrNextFree
 Input       :  The Indices

                The Object 
                retValDiffServClfrNextFree
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDiffServClfrNextFree (UINT4 *pu4RetValDiffServClfrNextFree)
{
    if (gQoSGlobalInfo.eSysControl != QOS_SYS_CNTL_START)
    {
        QOS_TRC (MGMT_TRC, QOS_MODULE_SHUTDOW_ERROR_MSG);
        *pu4RetValDiffServClfrNextFree = 0;
        return SNMP_SUCCESS;
    }
    *pu4RetValDiffServClfrNextFree = QOS_CLFR_NEXT_FREE ();
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : DiffServClfrTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceDiffServClfrTable
 Input       :  The Indices
                DiffServClfrId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceDiffServClfrTable (UINT4 u4DiffServClfrId)
{

    if (QOS_CLFR_ID_VALID (u4DiffServClfrId) != ISS_TRUE)
    {
        return SNMP_FAILURE;
    }
    if (QosGetClfrEntry (u4DiffServClfrId) == NULL)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexDiffServClfrTable
 Input       :  The Indices
                DiffServClfrId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexDiffServClfrTable (UINT4 *pu4DiffServClfrId)
{
    tQosStdClfrEntry   *pClfrEntry = NULL;

    if (gQoSGlobalInfo.eSysControl != QOS_SYS_CNTL_START)
    {
        QOS_TRC (MGMT_TRC, QOS_MODULE_SHUTDOW_ERROR_MSG);
        return (SNMP_FAILURE);
    }
    pClfrEntry = (tQosStdClfrEntry *) RBTreeGetFirst (gQoSGlobalInfo.DsClfrTbl);
    if (pClfrEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    *pu4DiffServClfrId = pClfrEntry->u4DsClfrId;
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetNextIndexDiffServClfrTable
 Input       :  The Indices
                DiffServClfrId
                nextDiffServClfrId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexDiffServClfrTable (UINT4 u4DiffServClfrId,
                                  UINT4 *pu4NextDiffServClfrId)
{
    if (gQoSGlobalInfo.eSysControl != QOS_SYS_CNTL_START)
    {
        QOS_TRC (MGMT_TRC, QOS_MODULE_SHUTDOW_ERROR_MSG);
        return (SNMP_FAILURE);
    }
    if (QoSGetNextClfrEntryValidateIndex (u4DiffServClfrId,
                                          pu4NextDiffServClfrId) != QOS_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetDiffServClfrStorage
 Input       :  The Indices
                DiffServClfrId

                The Object 
                retValDiffServClfrStorage
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDiffServClfrStorage (UINT4 u4DiffServClfrId,
                           INT4 *pi4RetValDiffServClfrStorage)
{
    tQosStdClfrEntry   *pClfrEntry = NULL;

    if (gQoSGlobalInfo.eSysControl != QOS_SYS_CNTL_START)
    {
        QOS_TRC (MGMT_TRC, QOS_MODULE_SHUTDOW_ERROR_MSG);
        return (SNMP_FAILURE);
    }
    pClfrEntry = QosGetClfrEntry (u4DiffServClfrId);
    if (pClfrEntry == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QosGetClfrEntry () "
                      " Returns FAILURE. \r\n", __FUNCTION__);
        return SNMP_FAILURE;
    }
    if (pClfrEntry->u4DsClfrId == u4DiffServClfrId)
    {
        *pi4RetValDiffServClfrStorage = pClfrEntry->i4StorageType;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetDiffServClfrStatus
 Input       :  The Indices
                DiffServClfrId

                The Object 
                retValDiffServClfrStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDiffServClfrStatus (UINT4 u4DiffServClfrId,
                          INT4 *pi4RetValDiffServClfrStatus)
{
    tQosStdClfrEntry   *pClfrEntry = NULL;

    if (gQoSGlobalInfo.eSysControl != QOS_SYS_CNTL_START)
    {
        QOS_TRC (MGMT_TRC, QOS_MODULE_SHUTDOW_ERROR_MSG);
        return (SNMP_FAILURE);
    }

    pClfrEntry = QosGetClfrEntry (u4DiffServClfrId);
    if (pClfrEntry == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QosGetClfrEntry () "
                      " Returns FAILURE. \r\n", __FUNCTION__);
        return SNMP_FAILURE;
    }
    if (pClfrEntry->u4DsClfrId == u4DiffServClfrId)
    {
        *pi4RetValDiffServClfrStatus = pClfrEntry->u1DsClfrStatus;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/* Low Level SET Routine for All Objects  */
/****************************************************************************
 Function    :  nmhSetDiffServClfrStorage
 Input       :  The Indices
                DiffServClfrId

                The Object 
                setValDiffServClfrStorage
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDiffServClfrStorage (UINT4 u4DiffServClfrId,
                           INT4 i4SetValDiffServClfrStorage)
{
    tQosStdClfrEntry   *pClfrEntry = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;

    pClfrEntry = QosGetClfrEntry (u4DiffServClfrId);
    if (pClfrEntry == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QosGetClfrEntry () "
                      " Returns FAILURE. \r\n", __FUNCTION__);
        return SNMP_FAILURE;
    }
    if (pClfrEntry->u4DsClfrId == u4DiffServClfrId)
    {
        pClfrEntry->i4StorageType = i4SetValDiffServClfrStorage;
        SnmpNotifyInfo.pu4ObjectId = DiffServClfrStorage;
        SnmpNotifyInfo.u4OidLen = sizeof (DiffServClfrStorage) / sizeof (UINT4);
        SnmpNotifyInfo.u4SeqNum = 0;
        SnmpNotifyInfo.u1RowStatus = FALSE;
        SnmpNotifyInfo.pLockPointer = QoSLock;
        SnmpNotifyInfo.pUnLockPointer = QoSUnLock;
        SnmpNotifyInfo.u4Indices = 1;
        SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;

        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%u %i", u4DiffServClfrId,
                          i4SetValDiffServClfrStorage));
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetDiffServClfrStatus
 Input       :  The Indices
                DiffServClfrId

                The Object 
                setValDiffServClfrStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDiffServClfrStatus (UINT4 u4DiffServClfrId,
                          INT4 i4SetValDiffServClfrStatus)
{
    INT4                i4RetStatus = SNMP_FAILURE;
    INT4                i4RowStatus = SNMP_FAILURE;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4ClfrId = 0;
    UINT4               u4ClfrElementId = 0;
    UINT4               u4NextClfrElementId = 0;
    UINT4               u4ExitFlag = 0;
    INT4                i4Return = SNMP_FAILURE;
    tQosStdClfrEntry   *pClfrEntry = NULL;

    if (QOS_CLFR_ID_VALID (u4DiffServClfrId) != QOS_SUCCESS)
    {
        return (SNMP_FAILURE);
    }
    pClfrEntry = QosGetClfrEntry (u4DiffServClfrId);
    if (pClfrEntry != NULL)
    {
        i4RowStatus = (UINT4) pClfrEntry->u1DsClfrStatus;

        /* Entry found in the Table */
        if (i4RowStatus == i4SetValDiffServClfrStatus)
        {
            /* same value */
            return (SNMP_SUCCESS);
        }
    }
    else
    {
        if (i4SetValDiffServClfrStatus != QOS_CREATE_AND_WAIT)
        {
            return SNMP_FAILURE;
        }
    }
    switch (i4SetValDiffServClfrStatus)
    {
            /* For a New Entry */
        case QOS_CREATE_AND_WAIT:

            pClfrEntry = QoSCreateClfrEntry (u4DiffServClfrId);

            if (pClfrEntry == NULL)
            {
                QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSCreateClfrEntry () "
                              " Returns FAILURE. \r\n", __FUNCTION__);

                return (SNMP_FAILURE);
            }
            QosUpdateDiffServClfrNextFree (u4DiffServClfrId,
                                           i4SetValDiffServClfrStatus);
            break;
        case QOS_ACTIVE:
            if (pClfrEntry->u1DsClfrStatus == QOS_NOT_IN_SERVICE)
            {
                /* This currently has nothing to do with hardware. *
                 * So just set the RowStatus to Active*/

                pClfrEntry->u1DsClfrStatus = QOS_ACTIVE;
                break;
            }
            return SNMP_FAILURE;

        case QOS_NOT_IN_SERVICE:

            if ((pClfrEntry->u1DsClfrStatus == QOS_ACTIVE) &&
                (gQoSGlobalInfo.eSysStatus == QOS_SYS_STATUS_ENABLE))
            {
                /* This currently doesnt have anything to do with hardware. *
                 * So we just have to set the RowStatus to Not in Service*/
                pClfrEntry->u1DsClfrStatus = QOS_NOT_IN_SERVICE;
            }
            break;

        case QOS_DESTROY:

            i4Return =
                (INT1) nmhGetFirstIndexFsQoSClassMapTable (&u4ClfrElementId);

            do
            {
                u4ExitFlag = 0;
                if (nmhGetNextIndexFsQoSClassMapTable
                    (u4ClfrElementId, &u4NextClfrElementId) == SNMP_FAILURE)
                {
                    u4ExitFlag = 1;
                }
                nmhGetFsQoSClassMapClfrId (u4ClfrElementId, &u4ClfrId);

                if (u4ClfrId == u4DiffServClfrId)
                {
                    nmhSetFsQoSClassMapStatus (u4ClfrElementId, QOS_DESTROY);
                }
                u4ClfrElementId = u4NextClfrElementId;

            }
            while (u4ExitFlag == 0);

            i4RetStatus = QoSDeleteClfrEntry (pClfrEntry);

            if (i4RetStatus == QOS_FAILURE)
            {
                QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSDeleteClfrEntry () "
                              " Returns FAILURE. \r\n", __FUNCTION__);

                return (SNMP_FAILURE);
            }
            i4Return = QosUpdateDiffServClfrNextFree (u4DiffServClfrId,
                                                      i4SetValDiffServClfrStatus);
            break;
        case QOS_CREATE_AND_GO:
            return (SNMP_FAILURE);
        default:
            return (SNMP_FAILURE);
    }
    SnmpNotifyInfo.pu4ObjectId = DiffServClfrStatus;
    SnmpNotifyInfo.u4OidLen = sizeof (DiffServClfrStatus) / sizeof (UINT4);
    SnmpNotifyInfo.u4SeqNum = 0;
    SnmpNotifyInfo.u1RowStatus = TRUE;
    SnmpNotifyInfo.pLockPointer = QoSLock;
    SnmpNotifyInfo.pUnLockPointer = QoSUnLock;
    SnmpNotifyInfo.u4Indices = 1;
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%u %i", u4DiffServClfrId,
                      i4SetValDiffServClfrStatus));
    UNUSED_PARAM (i4Return);
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2DiffServClfrStorage
 Input       :  The Indices
                DiffServClfrId

                The Object 
                testValDiffServClfrStorage
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2DiffServClfrStorage (UINT4 *pu4ErrorCode, UINT4 u4DiffServClfrId,
                              INT4 i4TestValDiffServClfrStorage)
{
    if (QosUtlValidateClfrEntry (u4DiffServClfrId, pu4ErrorCode) == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QosUtlValidateClfrEntry () "
                      "Returns FAILURE. \r\n", __FUNCTION__);
        return SNMP_FAILURE;
    }
    if ((i4TestValDiffServClfrStorage == QOS_STORAGE_VOLATILE) ||
        (i4TestValDiffServClfrStorage == QOS_STORAGE_NONVOLATILE))
    {
        return SNMP_SUCCESS;
    }
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2DiffServClfrStatus
 Input       :  The Indices
                DiffServClfrId

                The Object 
                testValDiffServClfrStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2DiffServClfrStatus (UINT4 *pu4ErrorCode, UINT4 u4DiffServClfrId,
                             INT4 i4TestValDiffServClfrStatus)
{
    UINT4               u4CurrentType = DS_CLFR;
    UINT4               u4DummyId = 0;

    if (gQoSGlobalInfo.eSysControl != QOS_SYS_CNTL_START)
    {
        QOS_TRC (MGMT_TRC, QOS_MODULE_SHUTDOW_ERROR_MSG);
        return (SNMP_FAILURE);
    }
    if (QOS_CLFR_ID_VALID (u4DiffServClfrId) != QOS_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if ((i4TestValDiffServClfrStatus == QOS_CREATE_AND_WAIT) &&
        (QOS_CLFR_COUNT >= QOS_CLFR_SIZE))
    {
        return (SNMP_FAILURE);
    }

    if ((i4TestValDiffServClfrStatus < QOS_ACTIVE) ||
        (i4TestValDiffServClfrStatus > QOS_DESTROY))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    if (i4TestValDiffServClfrStatus == QOS_DESTROY)
    {
        return ((INT1) (QosUtlPointedToStatus
                        (&u4CurrentType, &u4DiffServClfrId, &u4DummyId, DS_CLFR,
                         u4DiffServClfrId, 0, QOS_CHECK)));
    }
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2DiffServClfrTable
 Input       :  The Indices
                DiffServClfrId
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2DiffServClfrTable (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                           tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetDiffServClfrElementNextFree
 Input       :  The Indices

                The Object 
                retValDiffServClfrElementNextFree
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDiffServClfrElementNextFree (UINT4 *pu4RetValDiffServClfrElementNextFree)
{
    if (gQoSGlobalInfo.eSysControl != QOS_SYS_CNTL_START)
    {
        QOS_TRC (MGMT_TRC, QOS_MODULE_SHUTDOW_ERROR_MSG);
        *pu4RetValDiffServClfrElementNextFree = 0;
        return SNMP_SUCCESS;
    }
    *pu4RetValDiffServClfrElementNextFree = QOS_CLFR_ELEM_NEXT_FREE ();
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : DiffServClfrElementTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceDiffServClfrElementTable
 Input       :  The Indices
                DiffServClfrId
                DiffServClfrElementId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceDiffServClfrElementTable (UINT4 u4DiffServClfrId,
                                                  UINT4 u4DiffServClfrElementId)
{
    UNUSED_PARAM (u4DiffServClfrId);
    /* We just need to validate the class elem Id. ClfrId, we are hust using for saving */
    return (nmhValidateIndexInstanceFsQoSClassMapTable
            (u4DiffServClfrElementId));
}

/****************************************************************************
 Function    :  nmhGetFirstIndexDiffServClfrElementTable
 Input       :  The Indices
                DiffServClfrId
                DiffServClfrElementId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexDiffServClfrElementTable (UINT4 *pu4DiffServClfrId,
                                          UINT4 *pu4DiffServClfrElementId)
{

    if (nmhGetFirstIndexFsQoSClassMapTable (pu4DiffServClfrElementId) ==
        SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (nmhGetFsQoSClassMapClfrId (*pu4DiffServClfrElementId, pu4DiffServClfrId)
        == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexDiffServClfrElementTable
 Input       :  The Indices
                DiffServClfrId
                nextDiffServClfrId
                DiffServClfrElementId
                nextDiffServClfrElementId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexDiffServClfrElementTable (UINT4 u4DiffServClfrId,
                                         UINT4 *pu4NextDiffServClfrId,
                                         UINT4 u4DiffServClfrElementId,
                                         UINT4 *pu4NextDiffServClfrElementId)
{

    UNUSED_PARAM (u4DiffServClfrId);
    if (nmhGetNextIndexFsQoSClassMapTable
        (u4DiffServClfrElementId, pu4NextDiffServClfrElementId) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    if (nmhGetFsQoSClassMapClfrId
        (*pu4NextDiffServClfrElementId, pu4NextDiffServClfrId) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetDiffServClfrElementPrecedence
 Input       :  The Indices
                DiffServClfrId
                DiffServClfrElementId

                The Object 
                retValDiffServClfrElementPrecedence
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDiffServClfrElementPrecedence (UINT4 u4DiffServClfrId,
                                     UINT4 u4DiffServClfrElementId,
                                     UINT4
                                     *pu4RetValDiffServClfrElementPrecedence)
{
    UNUSED_PARAM (u4DiffServClfrId);
    UNUSED_PARAM (u4DiffServClfrElementId);
    UNUSED_PARAM (pu4RetValDiffServClfrElementPrecedence);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDiffServClfrElementNext
 Input       :  The Indices
                DiffServClfrId
                DiffServClfrElementId

                The Object 
                retValDiffServClfrElementNext
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDiffServClfrElementNext (UINT4 u4DiffServClfrId,
                               UINT4 u4DiffServClfrElementId,
                               tSNMP_OID_TYPE * pRetValDiffServClfrElementNext)
{
    UINT4               u4Id = QOS_ROWPOINTER_DEF;
    UINT4               u4Type = QOS_ROWPOINTER_DEF;
    UINT4               u4Class = 0;
    UINT4               u4CurCLASS = 0;
    UINT4               u4PriVal = 0;
    UINT4               u4QId = 0;
    tQoSClassInfoNode  *pClsInfoNode = NULL;
    INT4                i4IfIdx = 0;
    INT4                i4PriType = 0;
    INT4                i4RetStatus;

    UNUSED_PARAM (u4DiffServClfrId);

    /* This has to be a Meter (Policy). We get the policy Id  and return with type meter 
     * If the get fails, it implies that we haven't configured anything. So we return default value (0.0) */

    nmhGetFsQoSClassMapCLASS (u4DiffServClfrElementId, &u4Class);

    pClsInfoNode = QoSUtlGetClassInfoNode (u4Class);

    if (pClsInfoNode != NULL)
    {
        if (pClsInfoNode->pPlyMapNode != NULL)
        {
            u4Id = pClsInfoNode->pPlyMapNode->u4Id;
            u4Type = DS_METER;

            QosGetOidFromTypeId (u4Id, u4Type, pRetValDiffServClfrElementNext);

            return SNMP_SUCCESS;
        }
    }

    i4RetStatus = nmhGetFirstIndexFsQoSQMapTable (&i4IfIdx, &u4CurCLASS,
                                                  &i4PriType, &u4PriVal);
    if (i4RetStatus != SNMP_FAILURE)
    {
        do
        {
            if (u4CurCLASS == u4Class)
            {
                nmhGetFsQoSQMapQId (i4IfIdx, u4CurCLASS,
                                    i4PriType, u4PriVal, &u4QId);
                u4Id = QOS_GLOBAL_QID_FROM_IF_QID (i4IfIdx, u4QId);
                u4Type = DS_QUEUE;
                QosGetOidFromTypeId (u4Id, u4Type,
                                     pRetValDiffServClfrElementNext);

                return SNMP_SUCCESS;
            }

        }
        while (nmhGetNextIndexFsQoSQMapTable (i4IfIdx, &i4IfIdx,
                                              u4CurCLASS, &u4CurCLASS,
                                              i4PriType, &i4PriType,
                                              u4PriVal, &u4PriVal)
               == SNMP_SUCCESS);
    }

    QosGetOidFromTypeId (u4Id, u4Type, pRetValDiffServClfrElementNext);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDiffServClfrElementSpecific
 Input       :  The Indices
                DiffServClfrId
                DiffServClfrElementId

                The Object 
                retValDiffServClfrElementSpecific
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDiffServClfrElementSpecific (UINT4 u4DiffServClfrId,
                                   UINT4 u4DiffServClfrElementId,
                                   tSNMP_OID_TYPE *
                                   pRetValDiffServClfrElementSpecific)
{

    UINT4               u4L3FilterId = QOS_ROWPOINTER_DEF;
    UINT4               u4Type = QOS_ROWPOINTER_DEF;

    UNUSED_PARAM (u4DiffServClfrId);

    nmhGetFsQoSClassMapL3FilterId (u4DiffServClfrElementId, &u4L3FilterId);

    if (u4L3FilterId != 0)
    {
        u4Type = DS_MF_CLFR;
    }

    QosGetOidFromTypeId (u4L3FilterId, u4Type,
                         pRetValDiffServClfrElementSpecific);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDiffServClfrElementStorage
 Input       :  The Indices
                DiffServClfrId
                DiffServClfrElementId

                The Object 
                retValDiffServClfrElementStorage
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDiffServClfrElementStorage (UINT4 u4DiffServClfrId,
                                  UINT4 u4DiffServClfrElementId,
                                  INT4 *pi4RetValDiffServClfrElementStorage)
{
    UNUSED_PARAM (u4DiffServClfrId);
    UNUSED_PARAM (u4DiffServClfrElementId);
    UNUSED_PARAM (pi4RetValDiffServClfrElementStorage);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDiffServClfrElementStatus
 Input       :  The Indices
                DiffServClfrId
                DiffServClfrElementId

                The Object 
                retValDiffServClfrElementStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDiffServClfrElementStatus (UINT4 u4DiffServClfrId,
                                 UINT4 u4DiffServClfrElementId,
                                 INT4 *pi4RetValDiffServClfrElementStatus)
{

    UNUSED_PARAM (u4DiffServClfrId);
    return (nmhGetFsQoSClassMapStatus
            (u4DiffServClfrElementId, pi4RetValDiffServClfrElementStatus));
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetDiffServClfrElementPrecedence
 Input       :  The Indices
                DiffServClfrId
                DiffServClfrElementId

                The Object 
                setValDiffServClfrElementPrecedence
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDiffServClfrElementPrecedence (UINT4 u4DiffServClfrId,
                                     UINT4 u4DiffServClfrElementId,
                                     UINT4
                                     u4SetValDiffServClfrElementPrecedence)
{
    UNUSED_PARAM (u4DiffServClfrId);
    UNUSED_PARAM (u4DiffServClfrElementId);
    UNUSED_PARAM (u4SetValDiffServClfrElementPrecedence);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetDiffServClfrElementNext
 Input       :  The Indices
                DiffServClfrId
                DiffServClfrElementId

                The Object 
                setValDiffServClfrElementNext
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDiffServClfrElementNext (UINT4 u4DiffServClfrId,
                               UINT4 u4DiffServClfrElementId,
                               tSNMP_OID_TYPE * pSetValDiffServClfrElementNext)
{
    UINT4               u4Id = QOS_ROWPOINTER_DEF;
    UINT4               u4Type = QOS_ROWPOINTER_DEF;
    UINT4               u4CurrentIndex = 0;
    UINT4               u4NextIndex = 0;
    UINT4               u4ErrorCode = 0;
    UINT4               u4CLASS = 0;
    UINT4               u4PriVal = 0;
    UINT4               u4CurQIdx = (UINT4) CLI_RS_INVALID_MASK;
    UINT4               u4QIdx = 0;
    INT4                i4MapType = 0;
    INT4                i4RetStatus = SNMP_FAILURE;
    INT4                i4RowStatus = 0;
    INT4                i4IfIdx = 0;
    tQoSClassInfoNode  *pClsInfoNode = NULL;

    UNUSED_PARAM (u4DiffServClfrId);
    if (QosGetTypeIdFromOid (pSetValDiffServClfrElementNext, &u4Type, &u4Id)
        == QOS_FAILURE)
    {
        return SNMP_FAILURE;
    }
    if ((u4Type == 0) && (u4Id == 0))
    {
        u4Type = DS_METER;
        if (nmhGetFirstIndexDiffServMeterTable (&u4Id) != SNMP_SUCCESS)
        {
            return SNMP_FAILURE;
        }
    }
    if (u4Type == DS_METER)
    {

        while (QoSGetNextClsInfoTblEntryIndex (u4CurrentIndex, &u4NextIndex) ==
               QOS_SUCCESS)
        {

            if ((pClsInfoNode = QoSUtlGetClassInfoNode (u4NextIndex)) != NULL)
            {
                if (pClsInfoNode->pPlyMapNode != NULL)
                {
                    if (u4Id == pClsInfoNode->pPlyMapNode->u4Id)
                    {
                        /* Found a CLASS with the meter we have to configure as next */
                        if ((nmhSetFsQoSClassMapCLASS (u4DiffServClfrElementId,
                                                       u4NextIndex)) ==
                            SNMP_FAILURE)
                        {
                            return SNMP_FAILURE;
                        }
                        if ((nmhSetFsQoSPolicyMapCLASS (u4Id, u4NextIndex)) ==
                            SNMP_FAILURE)
                        {
                            return SNMP_FAILURE;
                        }
                        return SNMP_SUCCESS;
                    }
                }
            }
            u4CurrentIndex = u4NextIndex;
        }
        u4CurrentIndex = 0;

        while (nmhGetNextIndexFsQoSPolicyMapTable (u4CurrentIndex, &u4NextIndex)
               == QOS_SUCCESS)
        {
            if (u4NextIndex == u4Id)
            {
                /* We have a meter (policy) entry with given Index. We have to create a CLASS with *
                 * the Policy Id and add the CLASS to the Class-map table */
                u4CLASS = QOS_CLASS_NEXT_FREE ();
                /* We have to set the class. The entry does not exist. But the function below will take
                 * care of Creating the Class InfoNode */
                nmhSetFsQoSClassMapCLASS (u4DiffServClfrElementId, u4CLASS);
                /* Now we add the desired meter into the CLASS */
                nmhSetFsQoSPolicyMapCLASS (u4Id, u4CLASS);

                return SNMP_SUCCESS;
            }
            u4CurrentIndex = u4NextIndex;
        }
    }
    else if (u4Type == DS_QUEUE)
    {
        /* Map the CLASS in the corresponding Class-map to the queue */

        u4QIdx = QOS_IF_QID_FROM_GLOBAL_QID (u4Id);

        /* Get the Iinterface index of the queue from the glbal Qid */
        i4IfIdx = QOS_IF_IDX_FROM_GLOBAL_QID (u4Id);

        if (nmhGetFsQoSClassMapCLASS (u4DiffServClfrElementId, &u4CLASS)
            == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
        if (u4CLASS == 0)
        {
            /* The classifier is not mapped to any class. Create a New Class */
            u4CLASS = QOS_CLASS_NEXT_FREE ();

            nmhSetFsQoSClassMapCLASS (u4DiffServClfrElementId, u4CLASS);
        }

        /* Update the MapType to CLASS */
        i4MapType = QOS_CLI_QMAP_PRI_TYPE_NONE;

        u4PriVal = 0;

        i4RetStatus = nmhGetFsQoSQMapStatus (i4IfIdx, u4CLASS, i4MapType,
                                             u4PriVal, &i4RowStatus);
        if (i4RetStatus == QOS_FAILURE)
        {
            i4RetStatus =
                nmhTestv2FsQoSQMapStatus (&u4ErrorCode, i4IfIdx, u4CLASS,
                                          i4MapType, u4PriVal, CREATE_AND_WAIT);
            if (i4RetStatus == SNMP_FAILURE)
            {
                return (SNMP_FAILURE);
            }

            i4RetStatus = nmhSetFsQoSQMapStatus (i4IfIdx, u4CLASS, i4MapType,
                                                 u4PriVal, CREATE_AND_WAIT);
            if (i4RetStatus == SNMP_FAILURE)
            {
                return (SNMP_FAILURE);
            }
        }
        else if (i4RowStatus == ACTIVE)
        {
            i4RetStatus =
                nmhTestv2FsQoSQMapStatus (&u4ErrorCode, i4IfIdx,
                                          u4CLASS, i4MapType,
                                          u4PriVal, NOT_IN_SERVICE);
            if (i4RetStatus == SNMP_FAILURE)
            {
                return (SNMP_FAILURE);
            }

            i4RetStatus = nmhSetFsQoSQMapStatus (i4IfIdx, u4CLASS, i4MapType,
                                                 u4PriVal, NOT_IN_SERVICE);
            if (i4RetStatus == SNMP_FAILURE)
            {
                return (SNMP_FAILURE);
            }
        }

        i4RetStatus = nmhTestv2FsQoSQMapQId (&u4ErrorCode, i4IfIdx, u4CLASS,
                                             i4MapType, u4PriVal, u4QIdx);
        if (i4RetStatus == SNMP_FAILURE)
        {
            QoSCliRSQMapEntry (i4IfIdx, u4CLASS, i4MapType, u4PriVal, u4CurQIdx,
                               i4RowStatus);
            return (SNMP_FAILURE);
        }

        i4RetStatus = nmhGetFsQoSQMapQId (i4IfIdx, u4CLASS, i4MapType, u4PriVal,
                                          &u4CurQIdx);
        if (i4RetStatus == SNMP_FAILURE)
        {
            QoSCliRSQMapEntry (i4IfIdx, u4CLASS, i4MapType, u4PriVal, u4CurQIdx,
                               i4RowStatus);
            return (SNMP_FAILURE);
        }

        i4RetStatus = nmhSetFsQoSQMapQId (i4IfIdx, u4CLASS, i4MapType,
                                          u4PriVal, u4QIdx);
        if (i4RetStatus == SNMP_FAILURE)
        {
            QoSCliRSQMapEntry (i4IfIdx, u4CLASS, i4MapType, u4PriVal, u4CurQIdx,
                               i4RowStatus);
            return (SNMP_FAILURE);
        }

        i4RetStatus = nmhTestv2FsQoSQMapStatus (&u4ErrorCode, i4IfIdx, u4CLASS,
                                                i4MapType, u4PriVal, ACTIVE);
        if (i4RetStatus == SNMP_FAILURE)
        {
            QoSCliRSQMapEntry (i4IfIdx, u4CLASS, i4MapType, u4PriVal, u4CurQIdx,
                               i4RowStatus);
            return (SNMP_FAILURE);
        }

        i4RetStatus =
            nmhSetFsQoSQMapStatus (i4IfIdx, u4CLASS, i4MapType, u4PriVal,
                                   ACTIVE);
        if (i4RetStatus == SNMP_FAILURE)
        {
            QoSCliRSQMapEntry (i4IfIdx, u4CLASS, i4MapType, u4PriVal, u4CurQIdx,
                               i4RowStatus);
            return (SNMP_FAILURE);
        }

        return SNMP_SUCCESS;
    }
    else if (u4Type == QOS_ROWPOINTER_DEF)
    {
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetDiffServClfrElementSpecific
 Input       :  The Indices
                DiffServClfrId
                DiffServClfrElementId

                The Object 
                setValDiffServClfrElementSpecific
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDiffServClfrElementSpecific (UINT4 u4DiffServClfrId,
                                   UINT4 u4DiffServClfrElementId,
                                   tSNMP_OID_TYPE *
                                   pSetValDiffServClfrElementSpecific)
{
    UINT4               u4Id = QOS_ROWPOINTER_DEF;
    UINT4               u4Type = QOS_ROWPOINTER_DEF;

    UNUSED_PARAM (u4DiffServClfrId);

    if (QosGetTypeIdFromOid (pSetValDiffServClfrElementSpecific,
                             &u4Type, &u4Id) == QOS_FAILURE)
    {
        return SNMP_FAILURE;
    }
    /* Classifier Element Table can have only MFC Table as specific */
    if ((u4Type != DS_MF_CLFR) && (u4Type != QOS_ROWPOINTER_DEF))
    {
        return SNMP_FAILURE;
    }

    /* Multi Field Clfr is mapped to L3Acl and hence L3FilterId is same as MFC Id */
    return (nmhSetFsQoSClassMapL3FilterId (u4DiffServClfrElementId, u4Id));
}

/****************************************************************************
 Function    :  nmhSetDiffServClfrElementStorage
 Input       :  The Indices
                DiffServClfrId
                DiffServClfrElementId

                The Object 
                setValDiffServClfrElementStorage
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDiffServClfrElementStorage (UINT4 u4DiffServClfrId,
                                  UINT4 u4DiffServClfrElementId,
                                  INT4 i4SetValDiffServClfrElementStorage)
{
    UNUSED_PARAM (u4DiffServClfrId);
    UNUSED_PARAM (u4DiffServClfrElementId);
    UNUSED_PARAM (i4SetValDiffServClfrElementStorage);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetDiffServClfrElementStatus
 Input       :  The Indices
                DiffServClfrId
                DiffServClfrElementId

                The Object 
                setValDiffServClfrElementStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDiffServClfrElementStatus (UINT4 u4DiffServClfrId,
                                 UINT4 u4DiffServClfrElementId,
                                 INT4 i4SetValDiffServClfrElementStatus)
{

    if (nmhSetFsQoSClassMapStatus
        (u4DiffServClfrElementId,
         i4SetValDiffServClfrElementStatus) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (i4SetValDiffServClfrElementStatus == QOS_CREATE_AND_WAIT)
    {
        nmhSetFsQoSClassMapClfrId (u4DiffServClfrElementId, u4DiffServClfrId);
    }

    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2DiffServClfrElementPrecedence
 Input       :  The Indices
                DiffServClfrId
                DiffServClfrElementId

                The Object 
                testValDiffServClfrElementPrecedence
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2DiffServClfrElementPrecedence (UINT4 *pu4ErrorCode,
                                        UINT4 u4DiffServClfrId,
                                        UINT4 u4DiffServClfrElementId,
                                        UINT4
                                        u4TestValDiffServClfrElementPrecedence)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (u4DiffServClfrId);
    UNUSED_PARAM (u4DiffServClfrElementId);
    if ((u4TestValDiffServClfrElementPrecedence < QOS_CLFR_ELEMENT_PREC_MIN) ||
        u4TestValDiffServClfrElementPrecedence > QOS_CLFR_ELEMENT_PREC_MAX)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2DiffServClfrElementNext
 Input       :  The Indices
                DiffServClfrId
                DiffServClfrElementId

                The Object 
                testValDiffServClfrElementNext
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2DiffServClfrElementNext (UINT4 *pu4ErrorCode, UINT4 u4DiffServClfrId,
                                  UINT4 u4DiffServClfrElementId,
                                  tSNMP_OID_TYPE *
                                  pTestValDiffServClfrElementNext)
{
    UINT4               u4Id = QOS_ROWPOINTER_DEF;
    UINT4               u4Type = QOS_ROWPOINTER_DEF;

    UNUSED_PARAM (u4DiffServClfrId);
    UNUSED_PARAM (u4DiffServClfrElementId);
    if (QosGetTypeIdFromOid (pTestValDiffServClfrElementNext, &u4Type, &u4Id)
        != QOS_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if ((u4Type != DS_METER) && (u4Type != DS_QUEUE)
        && (u4Type != QOS_ROWPOINTER_DEF))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2DiffServClfrElementSpecific
 Input       :  The Indices
                DiffServClfrId
                DiffServClfrElementId

                The Object 
                testValDiffServClfrElementSpecific
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2DiffServClfrElementSpecific (UINT4 *pu4ErrorCode,
                                      UINT4 u4DiffServClfrId,
                                      UINT4 u4DiffServClfrElementId,
                                      tSNMP_OID_TYPE *
                                      pTestValDiffServClfrElementSpecific)
{
    UINT4               u4Id = QOS_ROWPOINTER_DEF;
    UINT4               u4Type = QOS_ROWPOINTER_DEF;

    UNUSED_PARAM (u4DiffServClfrId);
    UNUSED_PARAM (u4DiffServClfrElementId);
    if (QosGetTypeIdFromOid
        (pTestValDiffServClfrElementSpecific, &u4Type, &u4Id) != QOS_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if ((u4Type != DS_MF_CLFR) && (u4Type != QOS_ROWPOINTER_DEF))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2DiffServClfrElementStorage
 Input       :  The Indices
                DiffServClfrId
                DiffServClfrElementId

                The Object 
                testValDiffServClfrElementStorage
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2DiffServClfrElementStorage (UINT4 *pu4ErrorCode,
                                     UINT4 u4DiffServClfrId,
                                     UINT4 u4DiffServClfrElementId,
                                     INT4 i4TestValDiffServClfrElementStorage)
{
    UNUSED_PARAM (u4DiffServClfrId);
    UNUSED_PARAM (u4DiffServClfrElementId);
    if ((i4TestValDiffServClfrElementStorage != QOS_STORAGE_VOLATILE) &&
        (i4TestValDiffServClfrElementStorage != QOS_STORAGE_NONVOLATILE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2DiffServClfrElementStatus
 Input       :  The Indices
                DiffServClfrId
                DiffServClfrElementId

                The Object 
                testValDiffServClfrElementStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2DiffServClfrElementStatus (UINT4 *pu4ErrorCode, UINT4 u4DiffServClfrId,
                                    UINT4 u4DiffServClfrElementId,
                                    INT4 i4TestValDiffServClfrElementStatus)
{
    UNUSED_PARAM (u4DiffServClfrId);
    return (nmhTestv2FsQoSClassMapStatus
            (pu4ErrorCode, u4DiffServClfrElementId,
             i4TestValDiffServClfrElementStatus));
}

/* Low Level Dependency Routines for All Objects  */
/****************************************************************************
 Function    :  nmhDepv2DiffServClfrElementTable
 Input       :  The Indices
                DiffServClfrId
                DiffServClfrElementId
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2DiffServClfrElementTable (UINT4 *pu4ErrorCode,
                                  tSnmpIndexList * pSnmpIndexList,
                                  tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetDiffServMultiFieldClfrNextFree
 Input       :  The Indices

                The Object 
                retValDiffServMultiFieldClfrNextFree
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDiffServMultiFieldClfrNextFree (UINT4
                                      *pu4RetValDiffServMultiFieldClfrNextFree)
{
    if (gQoSGlobalInfo.eSysControl != QOS_SYS_CNTL_START)
    {
        QOS_TRC (MGMT_TRC, QOS_MODULE_SHUTDOW_ERROR_MSG);
        *pu4RetValDiffServMultiFieldClfrNextFree = 0;
        return SNMP_SUCCESS;
    }
    *pu4RetValDiffServMultiFieldClfrNextFree = QOS_MFC_NEXT_FREE ();
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : DiffServMultiFieldClfrTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceDiffServMultiFieldClfrTable
 Input       :  The Indices
                DiffServMultiFieldClfrId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceDiffServMultiFieldClfrTable (UINT4
                                                     u4DiffServMultiFieldClfrId)
{
    return (nmhValidateIndexInstanceIssAclL3FilterTable
            (u4DiffServMultiFieldClfrId));
}

/****************************************************************************
 Function    :  nmhGetFirstIndexDiffServMultiFieldClfrTable
 Input       :  The Indices
                DiffServMultiFieldClfrId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexDiffServMultiFieldClfrTable (UINT4 *pu4DiffServMultiFieldClfrId)
{
    return (nmhGetFirstIndexIssAclL3FilterTable
            ((INT4 *) pu4DiffServMultiFieldClfrId));
}

/****************************************************************************
 Function    :  nmhGetNextIndexDiffServMultiFieldClfrTable
 Input       :  The Indices
                DiffServMultiFieldClfrId
                nextDiffServMultiFieldClfrId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexDiffServMultiFieldClfrTable (UINT4 u4DiffServMultiFieldClfrId,
                                            UINT4
                                            *pu4NextDiffServMultiFieldClfrId)
{
    return (nmhGetNextIndexIssAclL3FilterTable
            (u4DiffServMultiFieldClfrId,
             (INT4 *) pu4NextDiffServMultiFieldClfrId));
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetDiffServMultiFieldClfrAddrType
 Input       :  The Indices
                DiffServMultiFieldClfrId

                The Object 
                retValDiffServMultiFieldClfrAddrType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDiffServMultiFieldClfrAddrType (UINT4 u4DiffServMultiFieldClfrId,
                                      INT4
                                      *pi4RetValDiffServMultiFieldClfrAddrType)
{
    return (nmhGetIssAclL3FilteAddrType
            (u4DiffServMultiFieldClfrId,
             pi4RetValDiffServMultiFieldClfrAddrType));
}

/****************************************************************************
 Function    :  nmhGetDiffServMultiFieldClfrDstAddr
 Input       :  The Indices
                DiffServMultiFieldClfrId

                The Object 
                retValDiffServMultiFieldClfrDstAddr
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDiffServMultiFieldClfrDstAddr (UINT4 u4DiffServMultiFieldClfrId,
                                     tSNMP_OCTET_STRING_TYPE *
                                     pRetValDiffServMultiFieldClfrDstAddr)
{
    tIssL3FilterEntry  *pIssL3FilterEntry = NULL;

    if (gQoSGlobalInfo.eSysControl != QOS_SYS_CNTL_START)
    {
        QOS_TRC (MGMT_TRC, QOS_MODULE_SHUTDOW_ERROR_MSG);
        return (SNMP_FAILURE);
    }
    pIssL3FilterEntry = QosGetMultiFieldClfrEntry (u4DiffServMultiFieldClfrId);
    UNUSED_PARAM (pIssL3FilterEntry);
    return (nmhGetIssAclL3FilterDstIpAddr (u4DiffServMultiFieldClfrId,
                                           pRetValDiffServMultiFieldClfrDstAddr));
}

/****************************************************************************
 Function    :  nmhGetDiffServMultiFieldClfrDstPrefixLength
 Input       :  The Indices
                DiffServMultiFieldClfrId

                The Object 
                retValDiffServMultiFieldClfrDstPrefixLength
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDiffServMultiFieldClfrDstPrefixLength (UINT4 u4DiffServMultiFieldClfrId,
                                             UINT4
                                             *pu4RetValDiffServMultiFieldClfrDstPrefixLength)
{
    if (gQoSGlobalInfo.eSysControl != QOS_SYS_CNTL_START)
    {
        QOS_TRC (MGMT_TRC, QOS_MODULE_SHUTDOW_ERROR_MSG);
        return (SNMP_FAILURE);
    }

    return (nmhGetIssAclL3FilterDstIpAddrPrefixLength
            (u4DiffServMultiFieldClfrId,
             pu4RetValDiffServMultiFieldClfrDstPrefixLength));
}

/****************************************************************************
 Function    :  nmhGetDiffServMultiFieldClfrSrcAddr
 Input       :  The Indices
                DiffServMultiFieldClfrId

                The Object 
                retValDiffServMultiFieldClfrSrcAddr
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDiffServMultiFieldClfrSrcAddr (UINT4 u4DiffServMultiFieldClfrId,
                                     tSNMP_OCTET_STRING_TYPE *
                                     pRetValDiffServMultiFieldClfrSrcAddr)
{

    return (nmhGetIssAclL3FilterSrcIpAddr (u4DiffServMultiFieldClfrId,
                                           pRetValDiffServMultiFieldClfrSrcAddr));
}

/****************************************************************************
 Function    :  nmhGetDiffServMultiFieldClfrSrcPrefixLength
 Input       :  The Indices
                DiffServMultiFieldClfrId

                The Object 
                retValDiffServMultiFieldClfrSrcPrefixLength
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDiffServMultiFieldClfrSrcPrefixLength (UINT4 u4DiffServMultiFieldClfrId,
                                             UINT4
                                             *pu4RetValDiffServMultiFieldClfrSrcPrefixLength)
{

    if (gQoSGlobalInfo.eSysControl != QOS_SYS_CNTL_START)
    {
        QOS_TRC (MGMT_TRC, QOS_MODULE_SHUTDOW_ERROR_MSG);
        return (SNMP_FAILURE);
    }

    return (nmhGetIssAclL3FilterSrcIpAddrPrefixLength
            (u4DiffServMultiFieldClfrId,
             pu4RetValDiffServMultiFieldClfrSrcPrefixLength));
}

/****************************************************************************
 Function    :  nmhGetDiffServMultiFieldClfrDscp
 Input       :  The Indices
                DiffServMultiFieldClfrId

                The Object 
                retValDiffServMultiFieldClfrDscp
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDiffServMultiFieldClfrDscp (UINT4 u4DiffServMultiFieldClfrId,
                                  INT4 *pi4RetValDiffServMultiFieldClfrDscp)
{
    if (gQoSGlobalInfo.eSysControl != QOS_SYS_CNTL_START)
    {
        QOS_TRC (MGMT_TRC, QOS_MODULE_SHUTDOW_ERROR_MSG);
        return (SNMP_FAILURE);
    }

    return (nmhGetIssAclL3FilterDscp
            (u4DiffServMultiFieldClfrId, pi4RetValDiffServMultiFieldClfrDscp));
}

/****************************************************************************
 Function    :  nmhGetDiffServMultiFieldClfrFlowId
 Input       :  The Indices
                DiffServMultiFieldClfrId

                The Object 
                retValDiffServMultiFieldClfrFlowId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDiffServMultiFieldClfrFlowId (UINT4 u4DiffServMultiFieldClfrId,
                                    UINT4
                                    *pu4RetValDiffServMultiFieldClfrFlowId)
{
    if (gQoSGlobalInfo.eSysControl != QOS_SYS_CNTL_START)
    {
        QOS_TRC (MGMT_TRC, QOS_MODULE_SHUTDOW_ERROR_MSG);
        return (SNMP_FAILURE);
    }

    return (nmhGetIssAclL3FilterFlowId
            (u4DiffServMultiFieldClfrId,
             pu4RetValDiffServMultiFieldClfrFlowId));
}

/****************************************************************************
 Function    :  nmhGetDiffServMultiFieldClfrProtocol
 Input       :  The Indices
                DiffServMultiFieldClfrId

                The Object 
                retValDiffServMultiFieldClfrProtocol
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDiffServMultiFieldClfrProtocol (UINT4 u4DiffServMultiFieldClfrId,
                                      UINT4
                                      *pu4RetValDiffServMultiFieldClfrProtocol)
{

    if (gQoSGlobalInfo.eSysControl != QOS_SYS_CNTL_START)
    {
        QOS_TRC (MGMT_TRC, QOS_MODULE_SHUTDOW_ERROR_MSG);
        return (SNMP_FAILURE);
    }

    return (nmhGetIssAclL3FilterProtocol
            (u4DiffServMultiFieldClfrId,
             (INT4 *) pu4RetValDiffServMultiFieldClfrProtocol));;
}

/****************************************************************************
 Function    :  nmhGetDiffServMultiFieldClfrDstL4PortMin
 Input       :  The Indices
                DiffServMultiFieldClfrId

                The Object 
                retValDiffServMultiFieldClfrDstL4PortMin
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDiffServMultiFieldClfrDstL4PortMin (UINT4 u4DiffServMultiFieldClfrId,
                                          UINT4
                                          *pu4RetValDiffServMultiFieldClfrDstL4PortMin)
{
    if (gQoSGlobalInfo.eSysControl != QOS_SYS_CNTL_START)
    {
        QOS_TRC (MGMT_TRC, QOS_MODULE_SHUTDOW_ERROR_MSG);
        return (SNMP_FAILURE);
    }

    return (nmhGetIssAclL3FilterMinDstProtPort
            (u4DiffServMultiFieldClfrId,
             pu4RetValDiffServMultiFieldClfrDstL4PortMin));

}

/****************************************************************************
 Function    :  nmhGetDiffServMultiFieldClfrDstL4PortMax
 Input       :  The Indices
                DiffServMultiFieldClfrId

                The Object 
                retValDiffServMultiFieldClfrDstL4PortMax
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDiffServMultiFieldClfrDstL4PortMax (UINT4 u4DiffServMultiFieldClfrId,
                                          UINT4
                                          *pu4RetValDiffServMultiFieldClfrDstL4PortMax)
{

    if (gQoSGlobalInfo.eSysControl != QOS_SYS_CNTL_START)
    {
        QOS_TRC (MGMT_TRC, QOS_MODULE_SHUTDOW_ERROR_MSG);
        return (SNMP_FAILURE);
    }

    return (nmhGetIssAclL3FilterMaxDstProtPort
            (u4DiffServMultiFieldClfrId,
             pu4RetValDiffServMultiFieldClfrDstL4PortMax));
}

/****************************************************************************
 Function    :  nmhGetDiffServMultiFieldClfrSrcL4PortMin
 Input       :  The Indices
                DiffServMultiFieldClfrId

                The Object 
                retValDiffServMultiFieldClfrSrcL4PortMin
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDiffServMultiFieldClfrSrcL4PortMin (UINT4 u4DiffServMultiFieldClfrId,
                                          UINT4
                                          *pu4RetValDiffServMultiFieldClfrSrcL4PortMin)
{

    if (gQoSGlobalInfo.eSysControl != QOS_SYS_CNTL_START)
    {
        QOS_TRC (MGMT_TRC, QOS_MODULE_SHUTDOW_ERROR_MSG);
        return (SNMP_FAILURE);
    }

    return (nmhGetIssAclL3FilterMinSrcProtPort
            (u4DiffServMultiFieldClfrId,
             pu4RetValDiffServMultiFieldClfrSrcL4PortMin));
}

/****************************************************************************
 Function    :  nmhGetDiffServMultiFieldClfrSrcL4PortMax
 Input       :  The Indices
                DiffServMultiFieldClfrId

                The Object 
                retValDiffServMultiFieldClfrSrcL4PortMax
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDiffServMultiFieldClfrSrcL4PortMax (UINT4 u4DiffServMultiFieldClfrId,
                                          UINT4
                                          *pu4RetValDiffServMultiFieldClfrSrcL4PortMax)
{

    if (gQoSGlobalInfo.eSysControl != QOS_SYS_CNTL_START)
    {
        QOS_TRC (MGMT_TRC, QOS_MODULE_SHUTDOW_ERROR_MSG);
        return (SNMP_FAILURE);
    }

    return (nmhGetIssAclL3FilterMaxSrcProtPort
            (u4DiffServMultiFieldClfrId,
             pu4RetValDiffServMultiFieldClfrSrcL4PortMax));

}

/****************************************************************************
 Function    :  nmhGetDiffServMultiFieldClfrStorage
 Input       :  The Indices
                DiffServMultiFieldClfrId

                The Object 
                retValDiffServMultiFieldClfrStorage
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDiffServMultiFieldClfrStorage (UINT4 u4DiffServMultiFieldClfrId,
                                     INT4
                                     *pi4RetValDiffServMultiFieldClfrStorage)
{
    tIssL3FilterEntry  *pIssL3FilterEntry = NULL;

    if (gQoSGlobalInfo.eSysControl != QOS_SYS_CNTL_START)
    {
        QOS_TRC (MGMT_TRC, QOS_MODULE_SHUTDOW_ERROR_MSG);
        return (SNMP_FAILURE);
    }
    pIssL3FilterEntry = QosGetMultiFieldClfrEntry (u4DiffServMultiFieldClfrId);

    if (pIssL3FilterEntry != NULL)
    {
        *pi4RetValDiffServMultiFieldClfrStorage =
            pIssL3FilterEntry->i4StorageType;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetDiffServMultiFieldClfrStatus
 Input       :  The Indices
                DiffServMultiFieldClfrId

                The Object 
                retValDiffServMultiFieldClfrStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDiffServMultiFieldClfrStatus (UINT4 u4DiffServMultiFieldClfrId,
                                    INT4 *pi4RetValDiffServMultiFieldClfrStatus)
{
    if (gQoSGlobalInfo.eSysControl != QOS_SYS_CNTL_START)
    {
        QOS_TRC (MGMT_TRC, QOS_MODULE_SHUTDOW_ERROR_MSG);
        return (SNMP_FAILURE);
    }
    return (nmhGetIssAclL3FilterStatus ((INT4) u4DiffServMultiFieldClfrId,
                                        pi4RetValDiffServMultiFieldClfrStatus));
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetDiffServMultiFieldClfrAddrType
 Input       :  The Indices
                DiffServMultiFieldClfrId

                The Object 
                setValDiffServMultiFieldClfrAddrType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDiffServMultiFieldClfrAddrType (UINT4 u4DiffServMultiFieldClfrId,
                                      INT4
                                      i4SetValDiffServMultiFieldClfrAddrType)
{
    return (nmhSetIssAclL3FilteAddrType
            (u4DiffServMultiFieldClfrId,
             i4SetValDiffServMultiFieldClfrAddrType));
}

/****************************************************************************
 Function    :  nmhSetDiffServMultiFieldClfrDstAddr
 Input       :  The Indices
                DiffServMultiFieldClfrId

                The Object 
                setValDiffServMultiFieldClfrDstAddr
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDiffServMultiFieldClfrDstAddr (UINT4 u4DiffServMultiFieldClfrId,
                                     tSNMP_OCTET_STRING_TYPE *
                                     pSetValDiffServMultiFieldClfrDstAddr)
{
    return (nmhSetIssAclL3FilterDstIpAddr (u4DiffServMultiFieldClfrId,
                                           pSetValDiffServMultiFieldClfrDstAddr));
}

/****************************************************************************
 Function    :  nmhSetDiffServMultiFieldClfrDstPrefixLength
 Input       :  The Indices
                DiffServMultiFieldClfrId

                The Object 
                setValDiffServMultiFieldClfrDstPrefixLength
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDiffServMultiFieldClfrDstPrefixLength (UINT4 u4DiffServMultiFieldClfrId,
                                             UINT4
                                             u4SetValDiffServMultiFieldClfrDstPrefixLength)
{
    return (nmhSetIssAclL3FilterDstIpAddrPrefixLength
            (u4DiffServMultiFieldClfrId,
             u4SetValDiffServMultiFieldClfrDstPrefixLength));;
}

/****************************************************************************
 Function    :  nmhSetDiffServMultiFieldClfrSrcAddr
 Input       :  The Indices
                DiffServMultiFieldClfrId

                The Object 
                setValDiffServMultiFieldClfrSrcAddr
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDiffServMultiFieldClfrSrcAddr (UINT4 u4DiffServMultiFieldClfrId,
                                     tSNMP_OCTET_STRING_TYPE *
                                     pSetValDiffServMultiFieldClfrSrcAddr)
{
    return (nmhSetIssAclL3FilterSrcIpAddr (u4DiffServMultiFieldClfrId,
                                           pSetValDiffServMultiFieldClfrSrcAddr));
}

/****************************************************************************
 Function    :  nmhSetDiffServMultiFieldClfrSrcPrefixLength
 Input       :  The Indices
                DiffServMultiFieldClfrId

                The Object 
                setValDiffServMultiFieldClfrSrcPrefixLength
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDiffServMultiFieldClfrSrcPrefixLength (UINT4 u4DiffServMultiFieldClfrId,
                                             UINT4
                                             u4SetValDiffServMultiFieldClfrSrcPrefixLength)
{
    return (nmhSetIssAclL3FilterSrcIpAddrPrefixLength
            (u4DiffServMultiFieldClfrId,
             u4SetValDiffServMultiFieldClfrSrcPrefixLength));
}

/****************************************************************************
 Function    :  nmhSetDiffServMultiFieldClfrDscp
 Input       :  The Indices
                DiffServMultiFieldClfrId

                The Object 
                setValDiffServMultiFieldClfrDscp
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDiffServMultiFieldClfrDscp (UINT4 u4DiffServMultiFieldClfrId,
                                  INT4 i4SetValDiffServMultiFieldClfrDscp)
{
    return (nmhSetIssAclL3FilterDscp
            (u4DiffServMultiFieldClfrId, i4SetValDiffServMultiFieldClfrDscp));
}

/****************************************************************************
 Function    :  nmhSetDiffServMultiFieldClfrFlowId
 Input       :  The Indices
                DiffServMultiFieldClfrId

                The Object 
                setValDiffServMultiFieldClfrFlowId
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDiffServMultiFieldClfrFlowId (UINT4 u4DiffServMultiFieldClfrId,
                                    UINT4 u4SetValDiffServMultiFieldClfrFlowId)
{
    return (nmhSetIssAclL3FilterFlowId
            (u4DiffServMultiFieldClfrId, u4SetValDiffServMultiFieldClfrFlowId));
}

/****************************************************************************
 Function    :  nmhSetDiffServMultiFieldClfrProtocol
 Input       :  The Indices
                DiffServMultiFieldClfrId

                The Object 
                setValDiffServMultiFieldClfrProtocol
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDiffServMultiFieldClfrProtocol (UINT4 u4DiffServMultiFieldClfrId,
                                      UINT4
                                      u4SetValDiffServMultiFieldClfrProtocol)
{
    return (nmhSetIssAclL3FilterProtocol
            (u4DiffServMultiFieldClfrId,
             u4SetValDiffServMultiFieldClfrProtocol));
}

/****************************************************************************
 Function    :  nmhSetDiffServMultiFieldClfrDstL4PortMin
 Input       :  The Indices
                DiffServMultiFieldClfrId

                The Object 
                setValDiffServMultiFieldClfrDstL4PortMin
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDiffServMultiFieldClfrDstL4PortMin (UINT4 u4DiffServMultiFieldClfrId,
                                          UINT4
                                          u4SetValDiffServMultiFieldClfrDstL4PortMin)
{
    return (nmhSetIssAclL3FilterMinDstProtPort
            (u4DiffServMultiFieldClfrId,
             u4SetValDiffServMultiFieldClfrDstL4PortMin));
}

/****************************************************************************
 Function    :  nmhSetDiffServMultiFieldClfrDstL4PortMax
 Input       :  The Indices
                DiffServMultiFieldClfrId

                The Object 
                setValDiffServMultiFieldClfrDstL4PortMax
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDiffServMultiFieldClfrDstL4PortMax (UINT4 u4DiffServMultiFieldClfrId,
                                          UINT4
                                          u4SetValDiffServMultiFieldClfrDstL4PortMax)
{
    return (nmhSetIssAclL3FilterMaxDstProtPort
            (u4DiffServMultiFieldClfrId,
             u4SetValDiffServMultiFieldClfrDstL4PortMax));
}

/****************************************************************************
 Function    :  nmhSetDiffServMultiFieldClfrSrcL4PortMin
 Input       :  The Indices
                DiffServMultiFieldClfrId

                The Object 
                setValDiffServMultiFieldClfrSrcL4PortMin
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDiffServMultiFieldClfrSrcL4PortMin (UINT4 u4DiffServMultiFieldClfrId,
                                          UINT4
                                          u4SetValDiffServMultiFieldClfrSrcL4PortMin)
{
    return (nmhSetIssAclL3FilterMinSrcProtPort
            (u4DiffServMultiFieldClfrId,
             u4SetValDiffServMultiFieldClfrSrcL4PortMin));
}

/****************************************************************************
 Function    :  nmhSetDiffServMultiFieldClfrSrcL4PortMax
 Input       :  The Indices
                DiffServMultiFieldClfrId

                The Object 
                setValDiffServMultiFieldClfrSrcL4PortMax
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDiffServMultiFieldClfrSrcL4PortMax (UINT4 u4DiffServMultiFieldClfrId,
                                          UINT4
                                          u4SetValDiffServMultiFieldClfrSrcL4PortMax)
{
    return (nmhSetIssAclL3FilterMaxSrcProtPort
            (u4DiffServMultiFieldClfrId,
             u4SetValDiffServMultiFieldClfrSrcL4PortMax));
}

/****************************************************************************
 Function    :  nmhSetDiffServMultiFieldClfrStorage
 Input       :  The Indices
                DiffServMultiFieldClfrId

                The Object 
                setValDiffServMultiFieldClfrStorage
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDiffServMultiFieldClfrStorage (UINT4 u4DiffServMultiFieldClfrId,
                                     INT4 i4SetValDiffServMultiFieldClfrStorage)
{
    UNUSED_PARAM (u4DiffServMultiFieldClfrId);
    UNUSED_PARAM (i4SetValDiffServMultiFieldClfrStorage);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetDiffServMultiFieldClfrStatus
 Input       :  The Indices
                DiffServMultiFieldClfrId

                The Object 
                setValDiffServMultiFieldClfrStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDiffServMultiFieldClfrStatus (UINT4 u4DiffServMultiFieldClfrId,
                                    INT4 i4SetValDiffServMultiFieldClfrStatus)
{
    return (nmhSetIssAclL3FilterStatus
            (u4DiffServMultiFieldClfrId, i4SetValDiffServMultiFieldClfrStatus));
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2DiffServMultiFieldClfrAddrType
 Input       :  The Indices
                DiffServMultiFieldClfrId

                The Object 
                testValDiffServMultiFieldClfrAddrType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2DiffServMultiFieldClfrAddrType (UINT4 *pu4ErrorCode,
                                         UINT4 u4DiffServMultiFieldClfrId,
                                         INT4
                                         i4TestValDiffServMultiFieldClfrAddrType)
{
    return (nmhTestv2IssAclL3FilteAddrType
            (pu4ErrorCode, u4DiffServMultiFieldClfrId,
             i4TestValDiffServMultiFieldClfrAddrType));;

}

/****************************************************************************
 Function    :  nmhTestv2DiffServMultiFieldClfrDstAddr
 Input       :  The Indices
                DiffServMultiFieldClfrId

                The Object 
                testValDiffServMultiFieldClfrDstAddr
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2DiffServMultiFieldClfrDstAddr (UINT4 *pu4ErrorCode,
                                        UINT4 u4DiffServMultiFieldClfrId,
                                        tSNMP_OCTET_STRING_TYPE *
                                        pTestValDiffServMultiFieldClfrDstAddr)
{
    return (nmhTestv2IssAclL3FilterDstIpAddr (pu4ErrorCode,
                                              u4DiffServMultiFieldClfrId,
                                              pTestValDiffServMultiFieldClfrDstAddr));

}

/****************************************************************************
 Function    :  nmhTestv2DiffServMultiFieldClfrDstPrefixLength
 Input       :  The Indices
                DiffServMultiFieldClfrId

                The Object 
                testValDiffServMultiFieldClfrDstPrefixLength
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2DiffServMultiFieldClfrDstPrefixLength (UINT4 *pu4ErrorCode,
                                                UINT4
                                                u4DiffServMultiFieldClfrId,
                                                UINT4
                                                u4TestValDiffServMultiFieldClfrDstPrefixLength)
{
    return (nmhTestv2IssAclL3FilterDstIpAddrPrefixLength
            (pu4ErrorCode, u4DiffServMultiFieldClfrId,
             u4TestValDiffServMultiFieldClfrDstPrefixLength));;
}

/****************************************************************************
 Function    :  nmhTestv2DiffServMultiFieldClfrSrcAddr
 Input       :  The Indices
                DiffServMultiFieldClfrId

                The Object 
                testValDiffServMultiFieldClfrSrcAddr
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2DiffServMultiFieldClfrSrcAddr (UINT4 *pu4ErrorCode,
                                        UINT4 u4DiffServMultiFieldClfrId,
                                        tSNMP_OCTET_STRING_TYPE *
                                        pTestValDiffServMultiFieldClfrSrcAddr)
{
    return (nmhTestv2IssAclL3FilterSrcIpAddr (pu4ErrorCode,
                                              u4DiffServMultiFieldClfrId,
                                              pTestValDiffServMultiFieldClfrSrcAddr));
}

/****************************************************************************
 Function    :  nmhTestv2DiffServMultiFieldClfrSrcPrefixLength
 Input       :  The Indices
                DiffServMultiFieldClfrId

                The Object 
                testValDiffServMultiFieldClfrSrcPrefixLength
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2DiffServMultiFieldClfrSrcPrefixLength (UINT4 *pu4ErrorCode,
                                                UINT4
                                                u4DiffServMultiFieldClfrId,
                                                UINT4
                                                u4TestValDiffServMultiFieldClfrSrcPrefixLength)
{
    return (nmhTestv2IssAclL3FilterSrcIpAddrPrefixLength
            (pu4ErrorCode, u4DiffServMultiFieldClfrId,
             u4TestValDiffServMultiFieldClfrSrcPrefixLength));;
}

/****************************************************************************
 Function    :  nmhTestv2DiffServMultiFieldClfrDscp
 Input       :  The Indices
                DiffServMultiFieldClfrId

                The Object 
                testValDiffServMultiFieldClfrDscp
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2DiffServMultiFieldClfrDscp (UINT4 *pu4ErrorCode,
                                     UINT4 u4DiffServMultiFieldClfrId,
                                     INT4 i4TestValDiffServMultiFieldClfrDscp)
{
    tIssL3FilterEntry  *pIssL3FilterEntry = NULL;

    pIssL3FilterEntry = QosGetMultiFieldClfrEntry (u4DiffServMultiFieldClfrId);

    if (pIssL3FilterEntry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }
    if (QosUtlValidateMultiFieldClfrEntry (u4DiffServMultiFieldClfrId,
                                           pIssL3FilterEntry, pu4ErrorCode)
        == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QosUtlValidateMultiFieldClfrEntry () "
                      "Returns FAILURE. \r\n", __FUNCTION__);
        return SNMP_FAILURE;
    }
    if (nmhTestv2IssAclL3FilterDscp (pu4ErrorCode, u4DiffServMultiFieldClfrId,
                                     i4TestValDiffServMultiFieldClfrDscp)
        != SNMP_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2DiffServMultiFieldClfrFlowId
 Input       :  The Indices
                DiffServMultiFieldClfrId

                The Object 
                testValDiffServMultiFieldClfrFlowId
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2DiffServMultiFieldClfrFlowId (UINT4 *pu4ErrorCode,
                                       UINT4 u4DiffServMultiFieldClfrId,
                                       UINT4
                                       u4TestValDiffServMultiFieldClfrFlowId)
{
    return (nmhTestv2IssAclL3FilterFlowId
            (pu4ErrorCode, u4DiffServMultiFieldClfrId,
             u4TestValDiffServMultiFieldClfrFlowId));
}

/****************************************************************************
 Function    :  nmhTestv2DiffServMultiFieldClfrProtocol
 Input       :  The Indices
                DiffServMultiFieldClfrId

                The Object 
                testValDiffServMultiFieldClfrProtocol
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2DiffServMultiFieldClfrProtocol (UINT4 *pu4ErrorCode,
                                         UINT4 u4DiffServMultiFieldClfrId,
                                         UINT4
                                         u4TestValDiffServMultiFieldClfrProtocol)
{
    tIssL3FilterEntry  *pIssL3FilterEntry = NULL;

    pIssL3FilterEntry = QosGetMultiFieldClfrEntry (u4DiffServMultiFieldClfrId);

    if (pIssL3FilterEntry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }
    if (QosUtlValidateMultiFieldClfrEntry (u4DiffServMultiFieldClfrId,
                                           pIssL3FilterEntry, pu4ErrorCode)
        == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QosUtlValidateMultiFieldClfrEntry () "
                      "Returns FAILURE. \r\n", __FUNCTION__);
        return SNMP_FAILURE;
    }
    return (nmhTestv2IssAclL3FilterProtocol (pu4ErrorCode,
                                             u4DiffServMultiFieldClfrId,
                                             u4TestValDiffServMultiFieldClfrProtocol));
}

/****************************************************************************
 Function    :  nmhTestv2DiffServMultiFieldClfrDstL4PortMin
 Input       :  The Indices
                DiffServMultiFieldClfrId

                The Object 
                testValDiffServMultiFieldClfrDstL4PortMin
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2DiffServMultiFieldClfrDstL4PortMin (UINT4 *pu4ErrorCode,
                                             UINT4 u4DiffServMultiFieldClfrId,
                                             UINT4
                                             u4TestValDiffServMultiFieldClfrDstL4PortMin)
{
    tIssL3FilterEntry  *pIssL3FilterEntry = NULL;

    pIssL3FilterEntry = QosGetMultiFieldClfrEntry (u4DiffServMultiFieldClfrId);

    if (pIssL3FilterEntry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }
    if (QosUtlValidateMultiFieldClfrEntry (u4DiffServMultiFieldClfrId,
                                           pIssL3FilterEntry, pu4ErrorCode)
        == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QosUtlValidateMultiFieldClfrEntry () "
                      "Returns FAILURE. \r\n", __FUNCTION__);
        return SNMP_FAILURE;
    }
    if (nmhTestv2IssAclL3FilterMinDstProtPort (pu4ErrorCode,
                                               u4DiffServMultiFieldClfrId,
                                               u4TestValDiffServMultiFieldClfrDstL4PortMin)
        != SNMP_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2DiffServMultiFieldClfrDstL4PortMax
 Input       :  The Indices
                DiffServMultiFieldClfrId

                The Object 
                testValDiffServMultiFieldClfrDstL4PortMax
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2DiffServMultiFieldClfrDstL4PortMax (UINT4 *pu4ErrorCode,
                                             UINT4 u4DiffServMultiFieldClfrId,
                                             UINT4
                                             u4TestValDiffServMultiFieldClfrDstL4PortMax)
{
    tIssL3FilterEntry  *pIssL3FilterEntry = NULL;

    pIssL3FilterEntry = QosGetMultiFieldClfrEntry (u4DiffServMultiFieldClfrId);

    if (pIssL3FilterEntry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }
    if (QosUtlValidateMultiFieldClfrEntry (u4DiffServMultiFieldClfrId,
                                           pIssL3FilterEntry, pu4ErrorCode)
        == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QosUtlValidateMultiFieldClfrEntry () "
                      "Returns FAILURE. \r\n", __FUNCTION__);
        return SNMP_FAILURE;
    }
    if (nmhTestv2IssAclL3FilterMaxDstProtPort (pu4ErrorCode,
                                               u4DiffServMultiFieldClfrId,
                                               u4TestValDiffServMultiFieldClfrDstL4PortMax)
        != SNMP_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2DiffServMultiFieldClfrSrcL4PortMin
 Input       :  The Indices
                DiffServMultiFieldClfrId

                The Object 
                testValDiffServMultiFieldClfrSrcL4PortMin
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2DiffServMultiFieldClfrSrcL4PortMin (UINT4 *pu4ErrorCode,
                                             UINT4 u4DiffServMultiFieldClfrId,
                                             UINT4
                                             u4TestValDiffServMultiFieldClfrSrcL4PortMin)
{
    tIssL3FilterEntry  *pIssL3FilterEntry = NULL;
    pIssL3FilterEntry = QosGetMultiFieldClfrEntry (u4DiffServMultiFieldClfrId);

    if (pIssL3FilterEntry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }
    if (QosUtlValidateMultiFieldClfrEntry (u4DiffServMultiFieldClfrId,
                                           pIssL3FilterEntry, pu4ErrorCode)
        == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QosUtlValidateMultiFieldClfrEntry () "
                      "Returns FAILURE. \r\n", __FUNCTION__);
        return SNMP_FAILURE;
    }
    if (nmhTestv2IssAclL3FilterMinSrcProtPort (pu4ErrorCode,
                                               u4DiffServMultiFieldClfrId,
                                               u4TestValDiffServMultiFieldClfrSrcL4PortMin)
        != SNMP_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2DiffServMultiFieldClfrSrcL4PortMax
 Input       :  The Indices
                DiffServMultiFieldClfrId

                The Object 
                testValDiffServMultiFieldClfrSrcL4PortMax
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2DiffServMultiFieldClfrSrcL4PortMax (UINT4 *pu4ErrorCode,
                                             UINT4 u4DiffServMultiFieldClfrId,
                                             UINT4
                                             u4TestValDiffServMultiFieldClfrSrcL4PortMax)
{
    tIssL3FilterEntry  *pIssL3FilterEntry = NULL;
    pIssL3FilterEntry = QosGetMultiFieldClfrEntry (u4DiffServMultiFieldClfrId);

    if (pIssL3FilterEntry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }
    if (QosUtlValidateMultiFieldClfrEntry (u4DiffServMultiFieldClfrId,
                                           pIssL3FilterEntry, pu4ErrorCode)
        == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QosUtlValidateMultiFieldClfrEntry () "
                      "Returns FAILURE. \r\n", __FUNCTION__);
        return SNMP_FAILURE;
    }
    if (nmhTestv2IssAclL3FilterMaxSrcProtPort (pu4ErrorCode,
                                               u4DiffServMultiFieldClfrId,
                                               u4TestValDiffServMultiFieldClfrSrcL4PortMax)
        != SNMP_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2DiffServMultiFieldClfrStorage
 Input       :  The Indices
                DiffServMultiFieldClfrId

                The Object 
                testValDiffServMultiFieldClfrStorage
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2DiffServMultiFieldClfrStorage (UINT4 *pu4ErrorCode,
                                        UINT4 u4DiffServMultiFieldClfrId,
                                        INT4
                                        i4TestValDiffServMultiFieldClfrStorage)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (u4DiffServMultiFieldClfrId);
    UNUSED_PARAM (i4TestValDiffServMultiFieldClfrStorage);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2DiffServMultiFieldClfrStatus
 Input       :  The Indices
                DiffServMultiFieldClfrId

                The Object 
                testValDiffServMultiFieldClfrStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2DiffServMultiFieldClfrStatus (UINT4 *pu4ErrorCode,
                                       UINT4 u4DiffServMultiFieldClfrId,
                                       INT4
                                       i4TestValDiffServMultiFieldClfrStatus)
{
    UINT4               u4CurrentType = DS_MF_CLFR;
    UINT4               u4DummyId = 0;
    if (gQoSGlobalInfo.eSysControl != QOS_SYS_CNTL_START)
    {
        QOS_TRC (MGMT_TRC, QOS_MODULE_SHUTDOW_ERROR_MSG);
        return (SNMP_FAILURE);
    }

    if (u4DiffServMultiFieldClfrId <= QOS_MIN_FILTER_ID)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    if ((i4TestValDiffServMultiFieldClfrStatus == QOS_DESTROY) &&
        (QosUtlPointedToStatus
         (&u4CurrentType, &u4DiffServMultiFieldClfrId, &u4DummyId, DS_MF_CLFR,
          u4DiffServMultiFieldClfrId, 0, QOS_CHECK) != QOS_SUCCESS))
    {
        return SNMP_FAILURE;
    }

    if ((i4TestValDiffServMultiFieldClfrStatus == QOS_CREATE_AND_WAIT) &&
        (QOS_MFC_COUNT >= ISS_MAX_L3_FILTERS))
    {
        return (SNMP_FAILURE);
    }

    return (nmhTestv2IssAclL3FilterStatus (pu4ErrorCode,
                                           (INT4) u4DiffServMultiFieldClfrId,
                                           i4TestValDiffServMultiFieldClfrStatus));
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2DiffServMultiFieldClfrTable
 Input       :  The Indices
                DiffServMultiFieldClfrId
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2DiffServMultiFieldClfrTable (UINT4 *pu4ErrorCode,
                                     tSnmpIndexList * pSnmpIndexList,
                                     tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetDiffServeterNextFree
 Input       :  The Indices

                The Object 
                retValDiffServMeterNextFree
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDiffServMeterNextFree (UINT4 *pu4RetValDiffServMeterNextFree)
{
    if (gQoSGlobalInfo.eSysControl != QOS_SYS_CNTL_START)
    {
        QOS_TRC (MGMT_TRC, QOS_MODULE_SHUTDOW_ERROR_MSG);
        *pu4RetValDiffServMeterNextFree = 0;
        return SNMP_SUCCESS;
    }
    *pu4RetValDiffServMeterNextFree = QOS_METER_NEXT_FREE ();
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : DiffServMeterTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceDiffServMeterTable
 Input       :  The Indices
                DiffServMeterId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceDiffServMeterTable (UINT4 u4DiffServMeterId)
{
    return (nmhValidateIndexInstanceFsQoSPolicyMapTable (u4DiffServMeterId));
}

/****************************************************************************
 Function    :  nmhGetFirstIndexDiffServMeterTable
 Input       :  The Indices
                DiffServMeterId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexDiffServMeterTable (UINT4 *pu4DiffServMeterId)
{
    if (gQoSGlobalInfo.eSysControl != QOS_SYS_CNTL_START)
    {
        QOS_TRC (MGMT_TRC, QOS_MODULE_SHUTDOW_ERROR_MSG);
        return (SNMP_FAILURE);
    }
    return (nmhGetFirstIndexFsQoSPolicyMapTable (pu4DiffServMeterId));
}

/****************************************************************************
 Function    :  nmhGetNextIndexDiffServMeterTable
 Input       :  The Indices
                DiffServMeterId
                nextDiffServMeterId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexDiffServMeterTable (UINT4 u4DiffServMeterId,
                                   UINT4 *pu4NextDiffServMeterId)
{

    if (gQoSGlobalInfo.eSysControl != QOS_SYS_CNTL_START)
    {
        QOS_TRC (MGMT_TRC, QOS_MODULE_SHUTDOW_ERROR_MSG);
        return (SNMP_FAILURE);
    }
    return (nmhGetNextIndexFsQoSPolicyMapTable
            (u4DiffServMeterId, pu4NextDiffServMeterId));
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetDiffServMeterSucceedNext
 Input       :  The Indices
                DiffServMeterId

                The Object 
                retValDiffServMeterSucceedNext
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDiffServMeterSucceedNext (UINT4 u4DiffServMeterId,
                                tSNMP_OID_TYPE *
                                pRetValDiffServMeterSucceedNext)
{
    UINT4               u4Id = QOS_ROWPOINTER_DEF;
    UINT4               u4Type = QOS_ROWPOINTER_DEF;
    UINT4               u4ActionId;
    INT1                i1ReturnValue = SNMP_FAILURE;

    if (gQoSGlobalInfo.eSysControl != QOS_SYS_CNTL_START)
    {
        QOS_TRC (MGMT_TRC, QOS_MODULE_SHUTDOW_ERROR_MSG);
        return (SNMP_FAILURE);
    }
    /* We have saved the action Id in the FsQos Policy Table. Retreive it */
    nmhGetFsQoSPolicyMapInProfileConformActionId (u4DiffServMeterId,
                                                  &u4ActionId);
    if (u4ActionId != 0)
    {
        u4Id = u4ActionId;
        u4Type = DS_ACTION;
    }

    if (QosValidateIndex (u4Id, u4Type) == QOS_FAILURE)
    {
        return (SNMP_FAILURE);
    }

    if (QosGetOidFromTypeId (u4Id, u4Type, pRetValDiffServMeterSucceedNext)
        == QOS_SUCCESS)
    {
        i1ReturnValue = SNMP_SUCCESS;
    }
    return (i1ReturnValue);
}

/****************************************************************************
 Function    :  nmhGetDiffServMeterFailNext
 Input       :  The Indices
                DiffServMeterId

                The Object 
                retValDiffServMeterFailNext
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDiffServMeterFailNext (UINT4 u4DiffServMeterId,
                             tSNMP_OID_TYPE * pRetValDiffServMeterFailNext)
{
    UINT4               u4Id = QOS_ROWPOINTER_DEF;
    UINT4               u4Type = QOS_ROWPOINTER_DEF;
    UINT4               u4ActionId = QOS_ROWPOINTER_DEF;
    UINT4               u4MeterId = QOS_ROWPOINTER_DEF;
    INT1                i1ReturnValue = SNMP_FAILURE;

    if (gQoSGlobalInfo.eSysControl != QOS_SYS_CNTL_START)
    {
        QOS_TRC (MGMT_TRC, QOS_MODULE_SHUTDOW_ERROR_MSG);
        return (SNMP_FAILURE);
    }
    nmhGetFsQoSPolicyMapInProfileExceedActionId (u4DiffServMeterId,
                                                 &u4ActionId);

    nmhGetFsQoSPolicyMapFailMeterTableId (u4DiffServMeterId, &u4MeterId);

    if (u4MeterId != 0)
    {
        u4Id = u4MeterId;
        u4Type = DS_METER;
    }
    else if (u4ActionId != 0)
    {
        u4Id = u4ActionId;
        u4Type = DS_ACTION;
    }

    if (QosValidateIndex (u4Id, u4Type) == QOS_FAILURE)
    {
        return (SNMP_FAILURE);
    }
    if (QosGetOidFromTypeId (u4Id, u4Type,
                             pRetValDiffServMeterFailNext) == QOS_SUCCESS)
    {
        i1ReturnValue = SNMP_SUCCESS;
    }
    return i1ReturnValue;
}

/****************************************************************************
 Function    :  nmhGetDiffServMeterSpecific
 Input       :  The Indices
                DiffServMeterId

                The Object 
                retValDiffServMeterSpecific
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDiffServMeterSpecific (UINT4 u4DiffServMeterId,
                             tSNMP_OID_TYPE * pRetValDiffServMeterSpecific)
{
    UINT4               u4Type = QOS_ROWPOINTER_DEF;
    UINT4               u4MeterSpecificId = QOS_ROWPOINTER_DEF;
    INT1                i1ReturnValue = SNMP_FAILURE;

    if (gQoSGlobalInfo.eSysControl != QOS_SYS_CNTL_START)
    {
        QOS_TRC (MGMT_TRC, QOS_MODULE_SHUTDOW_ERROR_MSG);
        return (SNMP_FAILURE);
    }

    nmhGetFsQoSPolicyMapMeterTableId (u4DiffServMeterId, &u4MeterSpecificId);

    if (u4MeterSpecificId != 0)
    {
        u4Type = DS_TBPARM;
    }
    /* Meter Specific is always TB Param */
    if (QosGetOidFromTypeId (u4MeterSpecificId, u4Type,
                             pRetValDiffServMeterSpecific) == QOS_SUCCESS)
    {
        i1ReturnValue = SNMP_SUCCESS;
    }
    return i1ReturnValue;
}

/****************************************************************************
 Function    :  nmhGetDiffServMeterStorage
 Input       :  The Indices
                DiffServMeterId

                The Object 
                retValDiffServMeterStorage
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDiffServMeterStorage (UINT4 u4DiffServMeterId,
                            INT4 *pi4RetValDiffServMeterStorage)
{
    UNUSED_PARAM (u4DiffServMeterId);
    UNUSED_PARAM (pi4RetValDiffServMeterStorage);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDiffServMeterStatus
 Input       :  The Indices
                DiffServMeterId

                The Object 
                retValDiffServMeterStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDiffServMeterStatus (UINT4 u4DiffServMeterId,
                           INT4 *pi4RetValDiffServMeterStatus)
{
    return (nmhGetFsQoSPolicyMapStatus
            (u4DiffServMeterId, pi4RetValDiffServMeterStatus));
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetDiffServMeterSucceedNext
 Input       :  The Indices
                DiffServMeterId

                The Object 
                setValDiffServMeterSucceedNext
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDiffServMeterSucceedNext (UINT4 u4DiffServMeterId,
                                tSNMP_OID_TYPE *
                                pSetValDiffServMeterSucceedNext)
{
    tQosStdMeterEntry  *pMeterNode = NULL;
    UINT4               u4Id = QOS_ROWPOINTER_DEF;
    UINT4               u4Type = QOS_ROWPOINTER_DEF;
    UINT4               u4CurrentType = DS_METER;
    UINT4               u4DummyId = 0;
    UINT4               u4PrevActionId = 0;
    tSNMP_OID_TYPE      Oid;
    UINT4               u4Id1 = QOS_ROWPOINTER_DEF;
    UINT4               u4Type1 = QOS_ROWPOINTER_DEF;
    UINT4               au4Temp[14];
    tSNMP_OCTET_STRING_TYPE *pOctStrActFlag = NULL;
    UINT1               u1InProConAct = 0x0;

    Oid.pu4_OidList = au4Temp;

    if (QosGetTypeIdFromOid (pSetValDiffServMeterSucceedNext, &u4Type, &u4Id) ==
        QOS_FAILURE)
    {
        return SNMP_FAILURE;
    }
    if ((u4Type != DS_ACTION) && (u4Type != QOS_ROWPOINTER_DEF))
    {
        return SNMP_FAILURE;
    }

    /* Get the current Action ID. Free this from the pointed to table */
    nmhGetFsQoSPolicyMapInProfileConformActionId (u4DiffServMeterId,
                                                  &u4PrevActionId);

    if (u4Type == DS_ACTION)
    {

        if (nmhGetDiffServActionSpecific (u4Id, &Oid) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
        QosGetTypeIdFromOid (&Oid, &u4Type1, &u4Id1);

        nmhSetFsQoSPolicyMapInProfileConformActionId (u4DiffServMeterId, u4Id);

        if ((u4Type1 != DS_STDDSCP) &&
            (u4Type1 != DS_COUNT_ACT) && (u4Type1 != DS_ALG_DROP))
        {
            return SNMP_FAILURE;
        }

        if (u4Type1 == DS_STDDSCP)
        {

            pOctStrActFlag =
                allocmem_octetstring (QOS_POLICY_ACTION_FLAG_LENGTH);
            if (pOctStrActFlag == NULL)
            {
                return SNMP_FAILURE;
            }
            u1InProConAct = QOS_PLY_IN_PROF_CONF_IP_DSCP;
            MEMSET (pOctStrActFlag->pu1_OctetList, 0,
                    pOctStrActFlag->i4_Length);
            MEMCPY (pOctStrActFlag->pu1_OctetList, &u1InProConAct,
                    QOS_POLICY_ACTION_FLAG_LENGTH);
            nmhSetFsQoSPolicyMapInProfileConformActionFlag (u4DiffServMeterId,
                                                            pOctStrActFlag);

            free_octetstring (pOctStrActFlag);

            nmhSetFsQoSPolicyMapConformActionSetDscp (u4DiffServMeterId, u4Id1);
        }
        if (u4Type1 == DS_COUNT_ACT)
        {

            /*There is no option for Conform action Counter in FsQos */
        }

        if (u4Type1 == DS_ALG_DROP)
        {
            pOctStrActFlag =
                allocmem_octetstring (QOS_POLICY_ACTION_FLAG_LENGTH);
            if (pOctStrActFlag == NULL)
            {
                return SNMP_FAILURE;
            }
            u1InProConAct = QOS_PLY_IN_PROF_DROP;
            MEMSET (pOctStrActFlag->pu1_OctetList, 0,
                    pOctStrActFlag->i4_Length);
            MEMCPY (pOctStrActFlag->pu1_OctetList, &u1InProConAct,
                    QOS_POLICY_ACTION_FLAG_LENGTH);
            nmhSetFsQoSPolicyMapInProfileConformActionFlag (u4DiffServMeterId,
                                                            pOctStrActFlag);

            free_octetstring (pOctStrActFlag);

        }
    }
    else
    {
        nmhSetFsQoSPolicyMapInProfileConformActionId (u4DiffServMeterId, u4Id);

        pOctStrActFlag = allocmem_octetstring (QOS_POLICY_ACTION_FLAG_LENGTH);
        if (pOctStrActFlag == NULL)
        {
            return SNMP_FAILURE;
        }
        u1InProConAct = QOS_PLY_IN_PROF_DROP;
        MEMSET (pOctStrActFlag->pu1_OctetList, 0, pOctStrActFlag->i4_Length);
        MEMCPY (pOctStrActFlag->pu1_OctetList, &u1InProConAct,
                QOS_POLICY_ACTION_FLAG_LENGTH);
        nmhSetFsQoSPolicyMapInProfileConformActionFlag (u4DiffServMeterId,
                                                        pOctStrActFlag);

        free_octetstring (pOctStrActFlag);

    }

    if (u4PrevActionId != 0)
    {
        /* If the meter was pointing to an action, remove it from the Pointed list */
        QosUtlPointedToStatus (&u4CurrentType, &u4DiffServMeterId, &u4DummyId,
                               DS_ACTION, u4PrevActionId, 0, QOS_DEL);
    }

    QosUtlPointedToStatus (&u4CurrentType, &u4DiffServMeterId, &u4DummyId,
                           u4Type, u4Id, 0, QOS_ADD);
    QoSValidateMeterTableStatus (pMeterNode);
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetDiffServMeterFailNext
 Input       :  The Indices
                DiffServMeterId

                The Object 
                setValDiffServMeterFailNext
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDiffServMeterFailNext (UINT4 u4DiffServMeterId,
                             tSNMP_OID_TYPE * pSetValDiffServMeterFailNext)
{
    tQosStdMeterEntry  *pMeterNode = NULL;
    UINT4               u4Id = QOS_ROWPOINTER_DEF;
    UINT4               u4Type = QOS_ROWPOINTER_DEF;
    UINT4               u4CurrentType = DS_METER;
    UINT4               u4DummyId = 0;
    tSNMP_OID_TYPE      Oid;
    UINT4               u4Id1 = QOS_ROWPOINTER_DEF;
    UINT4               u4Type1 = QOS_ROWPOINTER_DEF;
    UINT4               u4Id2 = QOS_ROWPOINTER_DEF;
    UINT4               u4Type2 = QOS_ROWPOINTER_DEF;
    UINT4               u4Id3 = QOS_ROWPOINTER_DEF;
    UINT4               u4Type3 = QOS_ROWPOINTER_DEF;
    UINT4               au4Temp[14];
    UINT4               u4PrevExceedActionId;
    UINT4               u4PrevOutProfileActionId;
    UINT4               u4FailNextMeterId = 0;
    tSNMP_OCTET_STRING_TYPE *pOctStrActFlag = NULL;
    UINT1               u1InProExcdAct = 0x0;

    tQoSMeterNode      *pTBMeterNode1 = NULL;
    tQoSMeterNode      *pTBMeterNode2 = NULL;

    Oid.pu4_OidList = au4Temp;

    if (QosGetTypeIdFromOid (pSetValDiffServMeterFailNext, &u4Type, &u4Id) ==
        QOS_FAILURE)
    {
        return SNMP_FAILURE;
    }

    nmhGetFsQoSPolicyMapInProfileExceedActionId (u4DiffServMeterId,
                                                 &u4PrevExceedActionId);
    nmhGetFsQoSPolicyMapOutProfileActionId (u4DiffServMeterId,
                                            &u4PrevOutProfileActionId);

    if (u4Type == DS_METER)
    {
        u4FailNextMeterId = u4Id;
        /*Get the Succeed next for the second meter */
        nmhGetDiffServMeterSucceedNext (u4Id, &Oid);
        QosGetTypeIdFromOid (&Oid, &u4Type1, &u4Id1);

        /* This is the Exceed action that we have to configure */
        if (u4Type1 != DS_ACTION)
        {
            return SNMP_FAILURE;
        }
        if (nmhGetDiffServActionSpecific (u4Id1, &Oid) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
        QosGetTypeIdFromOid (&Oid, &u4Type1, &u4Id1);
        nmhSetFsQoSPolicyMapInProfileExceedActionId (u4DiffServMeterId, u4Id1);

        QosUtlPointedToStatus (&u4CurrentType, &u4DiffServMeterId, &u4DummyId,
                               u4Type1, u4Id1, 0, QOS_ADD);
        /* This is the OutProfile action that we have to configure */
        if (u4Type1 == DS_STDDSCP)
        {
            pOctStrActFlag =
                allocmem_octetstring (QOS_POLICY_ACTION_FLAG_LENGTH);
            if (pOctStrActFlag == NULL)
            {
                return SNMP_FAILURE;
            }
            u1InProExcdAct = QOS_PLY_IN_PROF_EXC_IP_DSCP;
            MEMSET (pOctStrActFlag->pu1_OctetList, 0,
                    pOctStrActFlag->i4_Length);
            MEMCPY (pOctStrActFlag->pu1_OctetList, &u1InProExcdAct,
                    QOS_POLICY_ACTION_FLAG_LENGTH);
            nmhSetFsQoSPolicyMapInProfileExceedActionFlag (u4DiffServMeterId,
                                                           pOctStrActFlag);

            free_octetstring (pOctStrActFlag);

            nmhSetFsQoSPolicyMapExceedActionSetDscp (u4DiffServMeterId, u4Id1);
        }
        else if (u4Type1 == DS_COUNT_ACT)
        {
            /*Counter not supported in fsqos */
        }
        else if (u4Type1 == DS_ALG_DROP)
        {
            pOctStrActFlag =
                allocmem_octetstring (QOS_POLICY_ACTION_FLAG_LENGTH);
            if (pOctStrActFlag == NULL)
            {
                return SNMP_FAILURE;
            }
            u1InProExcdAct = QOS_PLY_IN_PROF_EXC_DROP;
            MEMSET (pOctStrActFlag->pu1_OctetList, 0,
                    pOctStrActFlag->i4_Length);
            MEMCPY (pOctStrActFlag->pu1_OctetList, &u1InProExcdAct,
                    QOS_POLICY_ACTION_FLAG_LENGTH);
            nmhSetFsQoSPolicyMapInProfileExceedActionFlag (u4DiffServMeterId,
                                                           pOctStrActFlag);

            free_octetstring (pOctStrActFlag);
        }

        /*Get the Fail next for the second meter */
        nmhGetDiffServMeterFailNext (u4Id, &Oid);
        QosGetTypeIdFromOid (&Oid, &u4Type1, &u4Id1);

        /* This is the Violate action that we have to configure */
        if ((u4Type1 != DS_ACTION) && (u4Type1 != QOS_ROWPOINTER_DEF))
        {
            return SNMP_FAILURE;
        }

        if (u4Type1 == DS_ACTION)
        {
            if (nmhGetDiffServActionSpecific (u4Id1, &Oid) == SNMP_FAILURE)
            {
                return SNMP_FAILURE;
            }
            QosGetTypeIdFromOid (&Oid, &u4Type1, &u4Id1);
            nmhSetFsQoSPolicyMapOutProfileActionId (u4DiffServMeterId, u4Id1);
            QosUtlPointedToStatus (&u4CurrentType, &u4DiffServMeterId,
                                   &u4DummyId, u4Type1, u4Id1, 0, QOS_ADD);
            /* This is the OutProfile action that we have to configure */
            if (u4Type1 == DS_STDDSCP)
            {
                pOctStrActFlag =
                    allocmem_octetstring (QOS_POLICY_ACTION_FLAG_LENGTH);
                if (pOctStrActFlag == NULL)
                {
                    return SNMP_FAILURE;
                }
                u1InProExcdAct = QOS_PLY_IN_PROF_EXC_IP_DSCP;
                MEMSET (pOctStrActFlag->pu1_OctetList, 0,
                        pOctStrActFlag->i4_Length);
                MEMCPY (pOctStrActFlag->pu1_OctetList, &u1InProExcdAct,
                        QOS_POLICY_ACTION_FLAG_LENGTH);
                nmhSetFsQoSPolicyMapOutProfileActionFlag (u4DiffServMeterId,
                                                          pOctStrActFlag);

                free_octetstring (pOctStrActFlag);

                nmhSetFsQoSPolicyMapOutProfileActionSetDscp (u4DiffServMeterId,
                                                             u4Id1);
            }
            else if (u4Type1 == DS_COUNT_ACT)
            {
                /*Counter not supported in fsqos */
            }
            else if (u4Type1 == DS_ALG_DROP)
            {
                pOctStrActFlag =
                    allocmem_octetstring (QOS_POLICY_ACTION_FLAG_LENGTH);
                if (pOctStrActFlag == NULL)
                {
                    return SNMP_FAILURE;
                }
                u1InProExcdAct = QOS_PLY_IN_PROF_EXC_DROP;
                MEMSET (pOctStrActFlag->pu1_OctetList, 0,
                        pOctStrActFlag->i4_Length);
                MEMCPY (pOctStrActFlag->pu1_OctetList, &u1InProExcdAct,
                        QOS_POLICY_ACTION_FLAG_LENGTH);
                nmhSetFsQoSPolicyMapOutProfileActionFlag (u4DiffServMeterId,
                                                          pOctStrActFlag);

                free_octetstring (pOctStrActFlag);
            }
        }
        nmhGetDiffServMeterSpecific (u4DiffServMeterId, &Oid);
        QosGetTypeIdFromOid (&Oid, &u4Type2, &u4Id2);
        if (u4Type2 != DS_TBPARM)
        {
            return SNMP_FAILURE;
        }

        pTBMeterNode1 = QoSUtlGetMeterNode (u4Id2);

        /*Get the Meter Specific for the second meter .This is required in
         * order to be able to configure the MeterTypes SrTcm. TrTcm and TswTcm
         * This is basically used to set the rate and busrt size of the second
         * TB Meter in the Meter node of the 1st Meter which is the basepolicy map 
         * for this CLASS.*/
        nmhGetDiffServMeterSpecific (u4Id, &Oid);
        QosGetTypeIdFromOid (&Oid, &u4Type3, &u4Id3);
        if (u4Type3 != DS_TBPARM)
        {
            return SNMP_FAILURE;
        }
        pTBMeterNode2 = QoSUtlGetMeterNode (u4Id3);
        if (pTBMeterNode2 != NULL)
        {
            if (pTBMeterNode1 != NULL)
            {
                if ((pTBMeterNode1->u1Type == QOS_METER_TYPE_SRTCM) ||
                    (pTBMeterNode1->u1Type == QOS_METER_TYPE_TRTCM))
                {
                    pTBMeterNode1->u4EIR = pTBMeterNode2->u4CIR;
                    pTBMeterNode1->u4EBS = pTBMeterNode2->u4CBS;

                }
            }
        }
        /* This is the OutProfile action that we have to configure */
        if (u4Type1 == DS_STDDSCP)
        {
            pOctStrActFlag =
                allocmem_octetstring (QOS_POLICY_ACTION_FLAG_LENGTH);
            if (pOctStrActFlag == NULL)
            {
                return SNMP_FAILURE;
            }
            u1InProExcdAct = QOS_PLY_IN_PROF_EXC_IP_DSCP;
            MEMSET (pOctStrActFlag->pu1_OctetList, 0,
                    pOctStrActFlag->i4_Length);
            MEMCPY (pOctStrActFlag->pu1_OctetList, &u1InProExcdAct,
                    QOS_POLICY_ACTION_FLAG_LENGTH);
            nmhSetFsQoSPolicyMapOutProfileActionFlag (u4DiffServMeterId,
                                                      pOctStrActFlag);

            free_octetstring (pOctStrActFlag);

            nmhSetFsQoSPolicyMapOutProfileActionSetDscp (u4DiffServMeterId,
                                                         u4Id1);
        }
        else if (u4Type1 == DS_COUNT_ACT)
        {
            /*Counter not supported in fsqos */
        }
        else if (u4Type1 == DS_ALG_DROP)
        {
            pOctStrActFlag =
                allocmem_octetstring (QOS_POLICY_ACTION_FLAG_LENGTH);
            if (pOctStrActFlag == NULL)
            {
                return SNMP_FAILURE;
            }
            u1InProExcdAct = QOS_PLY_IN_PROF_EXC_DROP;
            MEMSET (pOctStrActFlag->pu1_OctetList, 0,
                    pOctStrActFlag->i4_Length);
            MEMCPY (pOctStrActFlag->pu1_OctetList, &u1InProExcdAct,
                    QOS_POLICY_ACTION_FLAG_LENGTH);
            nmhSetFsQoSPolicyMapOutProfileActionFlag (u4DiffServMeterId,
                                                      pOctStrActFlag);

            free_octetstring (pOctStrActFlag);
        }

        /* Update the value of the FailNext Dummy Meter in the FsQosMIb */

        nmhSetFsQoSPolicyMapFailMeterTableId (u4DiffServMeterId,
                                              u4FailNextMeterId);

    }
    else if (u4Type == DS_ACTION)
    {
        if (nmhGetDiffServActionSpecific (u4Id, &Oid) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
        QosGetTypeIdFromOid (&Oid, &u4Type1, &u4Id1);

        nmhSetFsQoSPolicyMapInProfileExceedActionId (u4DiffServMeterId, u4Id);

        QosUtlPointedToStatus (&u4CurrentType, &u4DiffServMeterId, &u4DummyId,
                               u4Type, u4Id, 0, QOS_ADD);

        if ((u4Type1 != DS_STDDSCP) && (u4Type1 != QOS_ROWPOINTER_DEF) &&
            (u4Type1 != DS_COUNT_ACT) && (u4Type1 != DS_ALG_DROP))
        {
            return SNMP_FAILURE;
        }

        if (u4Type1 == DS_STDDSCP)
        {
            pOctStrActFlag =
                allocmem_octetstring (QOS_POLICY_ACTION_FLAG_LENGTH);
            if (pOctStrActFlag == NULL)
            {
                return SNMP_FAILURE;
            }
            u1InProExcdAct = QOS_PLY_IN_PROF_EXC_IP_DSCP;
            MEMSET (pOctStrActFlag->pu1_OctetList, 0,
                    pOctStrActFlag->i4_Length);
            MEMCPY (pOctStrActFlag->pu1_OctetList, &u1InProExcdAct,
                    QOS_POLICY_ACTION_FLAG_LENGTH);
            nmhSetFsQoSPolicyMapInProfileExceedActionFlag (u4DiffServMeterId,
                                                           pOctStrActFlag);

            free_octetstring (pOctStrActFlag);

            nmhSetFsQoSPolicyMapExceedActionSetDscp (u4DiffServMeterId, u4Id1);
        }
        if (u4Type1 == DS_COUNT_ACT)
        {

            /*There is no option for Conform action Counter in FsQos */
        }

        if (u4Type1 == DS_ALG_DROP)
        {
            pOctStrActFlag =
                allocmem_octetstring (QOS_POLICY_ACTION_FLAG_LENGTH);
            if (pOctStrActFlag == NULL)
            {
                return SNMP_FAILURE;
            }
            u1InProExcdAct = QOS_PLY_IN_PROF_EXC_DROP;
            MEMSET (pOctStrActFlag->pu1_OctetList, 0,
                    pOctStrActFlag->i4_Length);
            MEMCPY (pOctStrActFlag->pu1_OctetList, &u1InProExcdAct,
                    QOS_POLICY_ACTION_FLAG_LENGTH);
            nmhSetFsQoSPolicyMapInProfileExceedActionFlag (u4DiffServMeterId,
                                                           pOctStrActFlag);

            free_octetstring (pOctStrActFlag);

        }
    }
    else if (u4Type == QOS_ROWPOINTER_DEF)
    {
        nmhSetFsQoSPolicyMapInProfileExceedActionId (u4DiffServMeterId, u4Id);

        QosUtlPointedToStatus (&u4CurrentType, &u4DiffServMeterId, &u4DummyId,
                               u4Type, u4Id, 0, QOS_ADD);

        pOctStrActFlag = allocmem_octetstring (QOS_POLICY_ACTION_FLAG_LENGTH);
        if (pOctStrActFlag == NULL)
        {
            return SNMP_FAILURE;
        }
        u1InProExcdAct = 0x0;
        MEMSET (pOctStrActFlag->pu1_OctetList, 0, pOctStrActFlag->i4_Length);
        MEMCPY (pOctStrActFlag->pu1_OctetList, &u1InProExcdAct,
                QOS_POLICY_ACTION_FLAG_LENGTH);
        nmhSetFsQoSPolicyMapInProfileExceedActionFlag (u4DiffServMeterId,
                                                       pOctStrActFlag);

        free_octetstring (pOctStrActFlag);

    }

    if (u4PrevExceedActionId != 0)
    {
        /* If the meter was pointing to an action, remove it from the Pointed list */
        QosUtlPointedToStatus (&u4CurrentType, &u4DiffServMeterId, &u4DummyId,
                               DS_ACTION, u4PrevExceedActionId, 0, QOS_DEL);
    }

    if (u4PrevOutProfileActionId != 0)
    {
        /* If the meter was pointing to an action, remove it from the Pointed list */
        QosUtlPointedToStatus (&u4CurrentType, &u4DiffServMeterId, &u4DummyId,
                               DS_ACTION, u4PrevOutProfileActionId, 0, QOS_DEL);
    }

    QoSValidateMeterTableStatus (pMeterNode);

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetDiffServMeterSpecific
 Input       :  The Indices
                DiffServMeterId

                The Object 
                setValDiffServMeterSpecific
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDiffServMeterSpecific (UINT4 u4DiffServMeterId,
                             tSNMP_OID_TYPE * pSetValDiffServMeterSpecific)
{
    tQosStdMeterEntry  *pMeterNode = NULL;
    UINT4               u4Id = QOS_ROWPOINTER_DEF;
    UINT4               u4Type = QOS_ROWPOINTER_DEF;
    UINT4               u4CurrentType = DS_METER;
    UINT4               u4DummyId = 0;
    UINT4               u4PrevTBParamId = 0;

    if (QosGetTypeIdFromOid (pSetValDiffServMeterSpecific, &u4Type, &u4Id) ==
        QOS_FAILURE)
    {
        return SNMP_FAILURE;
    }
    /*Currently. Meter Table can have only TBPARAM Table as Specific */
    if ((u4Type != DS_TBPARM) && (u4Type != QOS_ROWPOINTER_DEF))
    {
        return SNMP_FAILURE;
    }

    /* Get the TB param previously pointed to, we have to free it fr deletion */
    nmhGetFsQoSPolicyMapMeterTableId (u4DiffServMeterId, &u4PrevTBParamId);
    /* TBParam is fsqosx meter */
    nmhSetFsQoSPolicyMapMeterTableId (u4DiffServMeterId, u4Id);

    QosUtlPointedToStatus (&u4CurrentType, &u4DiffServMeterId, &u4DummyId,
                           DS_TBPARM, u4PrevTBParamId, 0, QOS_DEL);
    QosUtlPointedToStatus (&u4CurrentType, &u4DiffServMeterId, &u4DummyId,
                           u4Type, u4Id, 0, QOS_ADD);
    QoSValidateMeterTableStatus (pMeterNode);

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetDiffServMeterStorage
 Input       :  The Indices
                DiffServMeterId

                The Object 
                setValDiffServMeterStorage
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDiffServMeterStorage (UINT4 u4DiffServMeterId,
                            INT4 i4SetValDiffServMeterStorage)
{

    UNUSED_PARAM (u4DiffServMeterId);
    if (!((i4SetValDiffServMeterStorage == QOS_STORAGE_VOLATILE) ||
          (i4SetValDiffServMeterStorage == QOS_STORAGE_NONVOLATILE)))
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetDiffServMeterStatus
 Input       :  The Indices
                DiffServMeterId

                The Object 
                setValDiffServMeterStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDiffServMeterStatus (UINT4 u4DiffServMeterId,
                           INT4 i4SetValDiffServMeterStatus)
{
    UINT4               u4PrevActionId = 0;
    UINT4               u4PrevExceedActionId = 0;
    UINT4               u4PrevOutProfileActionId = 0;
    UINT4               u4PrevTBParamId = 0;
    UINT4               u4CurrentType = DS_METER;
    UINT4               u4DummyId = 0;

    /* Get the Id's of the actions we are pointing to, we have to free them after deletion */
    nmhGetFsQoSPolicyMapInProfileExceedActionId (u4DiffServMeterId,
                                                 &u4PrevExceedActionId);
    nmhGetFsQoSPolicyMapInProfileConformActionId (u4DiffServMeterId,
                                                  &u4PrevActionId);
    nmhGetFsQoSPolicyMapOutProfileActionId (u4DiffServMeterId,
                                            &u4PrevOutProfileActionId);
    nmhGetFsQoSPolicyMapMeterTableId (u4DiffServMeterId, &u4PrevTBParamId);

    if (nmhSetFsQoSPolicyMapStatus
        (u4DiffServMeterId, i4SetValDiffServMeterStatus) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (i4SetValDiffServMeterStatus == QOS_DESTROY)
    {
        if (u4PrevActionId != 0)
        {
            /* If the meter was pointing to an action, remove it from the Pointed list */
            QosUtlPointedToStatus (&u4CurrentType, &u4DiffServMeterId,
                                   &u4DummyId, DS_ACTION, u4PrevActionId, 0,
                                   QOS_DEL);
        }

        if (u4PrevExceedActionId != 0)
        {
            /* If the meter was pointing to an action, remove it from the Pointed list */
            QosUtlPointedToStatus (&u4CurrentType, &u4DiffServMeterId,
                                   &u4DummyId, DS_ACTION, u4PrevExceedActionId,
                                   0, QOS_DEL);
        }

        if (u4PrevOutProfileActionId != 0)
        {
            /* If the meter was pointing to an action, remove it from the Pointed list */
            QosUtlPointedToStatus (&u4CurrentType, &u4DiffServMeterId,
                                   &u4DummyId, DS_ACTION,
                                   u4PrevOutProfileActionId, 0, QOS_DEL);
        }

        if (u4PrevTBParamId != 0)
        {
            /* If the meter was pointing to an action, remove it from the Pointed list */
            QosUtlPointedToStatus (&u4CurrentType, &u4DiffServMeterId,
                                   &u4DummyId, DS_TBPARM, u4PrevTBParamId, 0,
                                   QOS_DEL);
        }

    }
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2DiffServMeterSucceedNext
 Input       :  The Indices
                DiffServMeterId

                The Object 
                testValDiffServMeterSucceedNext
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2DiffServMeterSucceedNext (UINT4 *pu4ErrorCode, UINT4 u4DiffServMeterId,
                                   tSNMP_OID_TYPE *
                                   pTestValDiffServMeterSucceedNext)
{
    UINT4               u4Type;
    UINT4               u4Id;
    UNUSED_PARAM (u4DiffServMeterId);
    if (nmhValidateIndexInstanceFsQoSPolicyMapTable (u4DiffServMeterId) ==
        SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (QosGetTypeIdFromOid (pTestValDiffServMeterSucceedNext, &u4Type, &u4Id)
        == QOS_SUCCESS)
    {
        if ((u4Type == DS_ACTION) || (u4Type == QOS_ROWPOINTER_DEF))
        {
            return SNMP_SUCCESS;
        }
    }
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2DiffServMeterFailNext
 Input       :  The Indices
                DiffServMeterId

                The Object 
                testValDiffServMeterFailNext
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2DiffServMeterFailNext (UINT4 *pu4ErrorCode, UINT4 u4DiffServMeterId,
                                tSNMP_OID_TYPE * pTestValDiffServMeterFailNext)
{
    UINT4               u4Type, u4Id;
    UNUSED_PARAM (u4DiffServMeterId);

    if (nmhValidateIndexInstanceFsQoSPolicyMapTable (u4DiffServMeterId) ==
        SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (QosGetTypeIdFromOid (pTestValDiffServMeterFailNext, &u4Type, &u4Id)
        == QOS_SUCCESS)
    {
        if ((u4Type == DS_ACTION) || (u4Type == DS_METER)
            || (u4Type == QOS_ROWPOINTER_DEF))
        {
            return SNMP_SUCCESS;
        }
    }
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2DiffServMeterSpecific
 Input       :  The Indices
                DiffServMeterId

                The Object 
                testValDiffServMeterSpecific
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2DiffServMeterSpecific (UINT4 *pu4ErrorCode, UINT4 u4DiffServMeterId,
                                tSNMP_OID_TYPE * pTestValDiffServMeterSpecific)
{
    UINT4               u4Type, u4Id;
    UNUSED_PARAM (u4DiffServMeterId);

    if (nmhValidateIndexInstanceFsQoSPolicyMapTable (u4DiffServMeterId) ==
        SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    if (QosGetTypeIdFromOid (pTestValDiffServMeterSpecific, &u4Type, &u4Id)
        == QOS_SUCCESS)
    {
        if ((u4Type == DS_TBPARM) || (u4Type == QOS_ROWPOINTER_DEF))
        {
            return SNMP_SUCCESS;
        }
    }
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2DiffServMeterStorage
 Input       :  The Indices
                DiffServMeterId

                The Object 
                testValDiffServMeterStorage
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2DiffServMeterStorage (UINT4 *pu4ErrorCode, UINT4 u4DiffServMeterId,
                               INT4 i4TestValDiffServMeterStorage)
{

    if (nmhValidateIndexInstanceFsQoSPolicyMapTable (u4DiffServMeterId) ==
        SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    if ((i4TestValDiffServMeterStorage == QOS_STORAGE_VOLATILE) ||
        (i4TestValDiffServMeterStorage == QOS_STORAGE_NONVOLATILE))
    {
        return SNMP_SUCCESS;
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
}

/****************************************************************************
 Function    :  nmhTestv2DiffServMeterStatus
 Input       :  The Indices
                DiffServMeterId

                The Object 
                testValDiffServMeterStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2DiffServMeterStatus (UINT4 *pu4ErrorCode, UINT4 u4DiffServMeterId,
                              INT4 i4TestValDiffServMeterStatus)
{

    if (gQoSGlobalInfo.eSysControl != QOS_SYS_CNTL_START)
    {
        QOS_TRC (MGMT_TRC, QOS_MODULE_SHUTDOW_ERROR_MSG);
        return (SNMP_FAILURE);
    }

    if ((i4TestValDiffServMeterStatus == QOS_CREATE_AND_WAIT) &&
        (QOS_METER_COUNT >= QOS_METER_TBL_MAX_ENTRIES))
    {
        return (SNMP_FAILURE);
    }

    if (QOS_CHECK_METER_TBL_INDEX_RANGE (u4DiffServMeterId) == QOS_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }

    return (nmhTestv2FsQoSPolicyMapStatus
            (pu4ErrorCode, u4DiffServMeterId, i4TestValDiffServMeterStatus));
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2DiffServMeterTable
 Input       :  The Indices
                DiffServMeterId
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2DiffServMeterTable (UINT4 *pu4ErrorCode,
                            tSnmpIndexList * pSnmpIndexList,
                            tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetDiffServTBParamNextFree
 Input       :  The Indices

                The Object 
                retValDiffServTBParamNextFree
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDiffServTBParamNextFree (UINT4 *pu4RetValDiffServTBParamNextFree)
{
    if (gQoSGlobalInfo.eSysControl != QOS_SYS_CNTL_START)
    {
        QOS_TRC (MGMT_TRC, QOS_MODULE_SHUTDOW_ERROR_MSG);
        *pu4RetValDiffServTBParamNextFree = 0;
        return SNMP_SUCCESS;
    }
    *pu4RetValDiffServTBParamNextFree = QOS_TBPARM_NEXT_FREE ();
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : DiffServTBParamTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceDiffServTBParamTable
 Input       :  The Indices
                DiffServTBParamId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceDiffServTBParamTable (UINT4 u4DiffServTBParamId)
{
    if (gQoSGlobalInfo.eSysControl != QOS_SYS_CNTL_START)
    {
        QOS_TRC (MGMT_TRC, QOS_MODULE_SHUTDOW_ERROR_MSG);
        return (SNMP_FAILURE);
    }
    return (nmhValidateIndexInstanceFsQoSMeterTable (u4DiffServTBParamId));
}

/****************************************************************************
 Function    :  nmhGetFirstIndexDiffServTBParamTable
 Input       :  The Indices
                DiffServTBParamId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexDiffServTBParamTable (UINT4 *pu4DiffServTBParamId)
{
    if (gQoSGlobalInfo.eSysControl != QOS_SYS_CNTL_START)
    {
        QOS_TRC (MGMT_TRC, QOS_MODULE_SHUTDOW_ERROR_MSG);
        return (SNMP_FAILURE);
    }
    return (nmhGetFirstIndexFsQoSMeterTable (pu4DiffServTBParamId));
}

/****************************************************************************
 Function    :  nmhGetNextIndexDiffServTBParamTable
 Input       :  The Indices
                DiffServTBParamId
                nextDiffServTBParamId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexDiffServTBParamTable (UINT4 u4DiffServTBParamId,
                                     UINT4 *pu4NextDiffServTBParamId)
{
    if (gQoSGlobalInfo.eSysControl != QOS_SYS_CNTL_START)
    {
        QOS_TRC (MGMT_TRC, QOS_MODULE_SHUTDOW_ERROR_MSG);
        return (SNMP_FAILURE);
    }
    return (nmhGetNextIndexFsQoSMeterTable
            (u4DiffServTBParamId, pu4NextDiffServTBParamId));
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetDiffServTBParamType
 Input       :  The Indices
                DiffServTBParamId

                The Object 
                retValDiffServTBParamType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDiffServTBParamType (UINT4 u4DiffServTBParamId,
                           tSNMP_OID_TYPE * pRetValDiffServTBParamType)
{
    INT4                i4QosMeterType = QOS_METER_TYPE_SIMPLE_TB;
    INT4                i4QosMeterColorMode = QOS_METER_COLOR_BLIND;
    UINT4               u4TBParamType = DS_TB_STB;
    INT1                i1ReturnValue = SNMP_FAILURE;

    if (gQoSGlobalInfo.eSysControl != QOS_SYS_CNTL_START)
    {
        QOS_TRC (MGMT_TRC, QOS_MODULE_SHUTDOW_ERROR_MSG);
        return (SNMP_FAILURE);
    }
    if (nmhGetFsQoSMeterType (u4DiffServTBParamId, &i4QosMeterType)
        == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    if (nmhGetFsQoSMeterColorMode (u4DiffServTBParamId,
                                   &i4QosMeterColorMode) == QOS_FAILURE)
    {
        return SNMP_FAILURE;
    }

    switch (i4QosMeterType)
    {
        case QOS_METER_TYPE_SIMPLE_TB:
            u4TBParamType = DS_TB_STB;
            break;

        case QOS_METER_TYPE_AVG_RATE:
            u4TBParamType = DS_TB_AVGRATE;
            break;

        case QOS_METER_TYPE_SRTCM:
            if (i4QosMeterColorMode == QOS_METER_COLOR_BLIND)
            {
                u4TBParamType = DS_TB_SR_TCM_BLIND;
            }
            else
            {
                u4TBParamType = DS_TB_SR_TCM_AWARE;
            }
            break;

        case QOS_METER_TYPE_TRTCM:
            if (i4QosMeterColorMode == QOS_METER_COLOR_BLIND)
            {
                u4TBParamType = DS_TB_TR_TCM_BLIND;
            }
            else
            {
                u4TBParamType = DS_TB_TR_TCM_AWARE;
            }
            break;

        case QOS_METER_TYPE_TSWTCM:
            u4TBParamType = DS_TB_TSQ_TCM;
            break;

        default:

            return SNMP_FAILURE;
    }
    if (QosGetOidFromTBParamType (u4TBParamType, pRetValDiffServTBParamType)
        == QOS_SUCCESS)
    {
        i1ReturnValue = SNMP_SUCCESS;
    }
    return i1ReturnValue;
}

/****************************************************************************
 Function    :  nmhGetDiffServTBParamRate
 Input       :  The Indices
                DiffServTBParamId

                The Object 
                retValDiffServTBParamRate
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDiffServTBParamRate (UINT4 u4DiffServTBParamId,
                           UINT4 *pu4RetValDiffServTBParamRate)
{
    if (gQoSGlobalInfo.eSysControl != QOS_SYS_CNTL_START)
    {
        QOS_TRC (MGMT_TRC, QOS_MODULE_SHUTDOW_ERROR_MSG);
        return (SNMP_FAILURE);
    }
    return nmhGetFsQoSMeterCIR (u4DiffServTBParamId,
                                pu4RetValDiffServTBParamRate);
}

/****************************************************************************
 Function    :  nmhGetDiffServTBParamBurstSize
 Input       :  The Indices
                DiffServTBParamId

                The Object 
                retValDiffServTBParamBurstSize
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDiffServTBParamBurstSize (UINT4 u4DiffServTBParamId,
                                INT4 *pi4RetValDiffServTBParamBurstSize)
{
    if (gQoSGlobalInfo.eSysControl != QOS_SYS_CNTL_START)
    {
        QOS_TRC (MGMT_TRC, QOS_MODULE_SHUTDOW_ERROR_MSG);
        return (SNMP_FAILURE);
    }
    return nmhGetFsQoSMeterCBS (u4DiffServTBParamId,
                                (UINT4 *) pi4RetValDiffServTBParamBurstSize);
}

/****************************************************************************
 Function    :  nmhGetDiffServTBParamInterval
 Input       :  The Indices
                DiffServTBParamId

                The Object 
                retValDiffServTBParamInterval
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDiffServTBParamInterval (UINT4 u4DiffServTBParamId,
                               UINT4 *pu4RetValDiffServTBParamInterval)
{
    if (gQoSGlobalInfo.eSysControl != QOS_SYS_CNTL_START)
    {
        QOS_TRC (MGMT_TRC, QOS_MODULE_SHUTDOW_ERROR_MSG);
        return (SNMP_FAILURE);
    }
    return nmhGetFsQoSMeterInterval (u4DiffServTBParamId,
                                     pu4RetValDiffServTBParamInterval);
}

/****************************************************************************
 Function    :  nmhGetDiffServTBParamStorage
 Input       :  The Indices
                DiffServTBParamId

                The Object 
                retValDiffServTBParamStorage
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDiffServTBParamStorage (UINT4 u4DiffServTBParamId,
                              INT4 *pi4RetValDiffServTBParamStorage)
{
    tQoSMeterNode      *pMeterNode = NULL;

    if (gQoSGlobalInfo.eSysControl != QOS_SYS_CNTL_START)
    {
        QOS_TRC (MGMT_TRC, QOS_MODULE_SHUTDOW_ERROR_MSG);
        return (SNMP_FAILURE);
    }

    pMeterNode = QoSUtlGetMeterNode (u4DiffServTBParamId);
    if (pMeterNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetMeterNode () "
                      " Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }
    *pi4RetValDiffServTBParamStorage = pMeterNode->i4StorageType;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDiffServTBParamStatus
 Input       :  The Indices
                DiffServTBParamId

                The Object 
                retValDiffServTBParamStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDiffServTBParamStatus (UINT4 u4DiffServTBParamId,
                             INT4 *pi4RetValDiffServTBParamStatus)
{
    if (gQoSGlobalInfo.eSysControl != QOS_SYS_CNTL_START)
    {
        QOS_TRC (MGMT_TRC, QOS_MODULE_SHUTDOW_ERROR_MSG);
        return (SNMP_FAILURE);
    }
    return (nmhGetFsQoSMeterStatus (u4DiffServTBParamId,
                                    pi4RetValDiffServTBParamStatus));
}

/* Low Level SET Routine for All Objects  */
/****************************************************************************
 Function    :  nmhSetDiffServTBParamType
 Input       :  The Indices
                DiffServTBParamId

                The Object 
                setValDiffServTBParamType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDiffServTBParamType (UINT4 u4DiffServTBParamId,
                           tSNMP_OID_TYPE * pSetValDiffServTBParamType)
{
    UINT4               u4TBParamType = DS_TB_STB;

    if (QosGetTBparamTypeFromOid (pSetValDiffServTBParamType, &u4TBParamType)
        != QOS_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    switch (u4TBParamType)
    {
        case DS_TB_STB:
            nmhSetFsQoSMeterType (u4DiffServTBParamId,
                                  QOS_METER_TYPE_SIMPLE_TB);
            break;

        case DS_TB_AVGRATE:
            nmhSetFsQoSMeterType (u4DiffServTBParamId, QOS_METER_TYPE_AVG_RATE);
            break;

        case DS_TB_SR_TCM_BLIND:
            nmhSetFsQoSMeterColorMode (u4DiffServTBParamId,
                                       QOS_METER_COLOR_BLIND);
            nmhSetFsQoSMeterType (u4DiffServTBParamId, QOS_METER_TYPE_SRTCM);
            break;

        case DS_TB_SR_TCM_AWARE:
            nmhSetFsQoSMeterColorMode (u4DiffServTBParamId,
                                       QOS_METER_COLOR_AWARE);
            nmhSetFsQoSMeterType (u4DiffServTBParamId, QOS_METER_TYPE_SRTCM);
            break;

        case DS_TB_TR_TCM_BLIND:
            nmhSetFsQoSMeterColorMode (u4DiffServTBParamId,
                                       QOS_METER_COLOR_BLIND);
            nmhSetFsQoSMeterType (u4DiffServTBParamId, QOS_METER_TYPE_TRTCM);
            break;

        case DS_TB_TR_TCM_AWARE:
            nmhSetFsQoSMeterColorMode (u4DiffServTBParamId,
                                       QOS_METER_COLOR_AWARE);
            nmhSetFsQoSMeterType (u4DiffServTBParamId, QOS_METER_TYPE_TRTCM);
            break;

        case DS_TB_TSQ_TCM:
            nmhSetFsQoSMeterType (u4DiffServTBParamId, QOS_METER_TYPE_TSWTCM);
            break;
        default:
            return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetDiffServTBParamRate
 Input       :  The Indices
                DiffServTBParamId

                The Object 
                setValDiffServTBParamRate
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDiffServTBParamRate (UINT4 u4DiffServTBParamId,
                           UINT4 u4SetValDiffServTBParamRate)
{
    tQoSMeterNode      *pMeterNode = NULL;
    INT4                i4RetStatus = SNMP_FAILURE;

    pMeterNode = QoSUtlGetMeterNode (u4DiffServTBParamId);
    if (pMeterNode != NULL)
    {

        i4RetStatus = nmhSetFsQoSMeterCIR (u4DiffServTBParamId,
                                           u4SetValDiffServTBParamRate);
        if (i4RetStatus == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetDiffServTBParamBurstSize
 Input       :  The Indices
                DiffServTBParamId

                The Object 
                setValDiffServTBParamBurstSize
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDiffServTBParamBurstSize (UINT4 u4DiffServTBParamId,
                                INT4 i4SetValDiffServTBParamBurstSize)
{
    tQoSMeterNode      *pMeterNode = NULL;
    INT4                i4RetStatus = SNMP_FAILURE;

    pMeterNode = QoSUtlGetMeterNode (u4DiffServTBParamId);
    if (pMeterNode != NULL)
    {
        if (pMeterNode->u1Type != QOS_METER_TYPE_SIMPLE_TB)
        {
            i4RetStatus = nmhSetFsQoSMeterCBS (u4DiffServTBParamId,
                                               i4SetValDiffServTBParamBurstSize);
        }
        if (i4RetStatus == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetDiffServTBParamInterval
 Input       :  The Indices
                DiffServTBParamId

                The Object 
                setValDiffServTBParamInterval
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDiffServTBParamInterval (UINT4 u4DiffServTBParamId,
                               UINT4 u4SetValDiffServTBParamInterval)
{
    INT4                i4RetStatus = SNMP_FAILURE;

    i4RetStatus = nmhSetFsQoSMeterInterval (u4DiffServTBParamId,
                                            u4SetValDiffServTBParamInterval);
    if (i4RetStatus == SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetDiffServTBParamStorage
 Input       :  The Indices
                DiffServTBParamId

                The Object 
                setValDiffServTBParamStorage
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDiffServTBParamStorage (UINT4 u4DiffServTBParamId,
                              INT4 i4SetValDiffServTBParamStorage)
{
    tQoSMeterNode      *pMeterNode = NULL;

    pMeterNode = QoSUtlGetMeterNode (u4DiffServTBParamId);
    if (pMeterNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetMeterNode () "
                      " Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }
    pMeterNode->i4StorageType = i4SetValDiffServTBParamStorage;
    QoSValidateMeterTblEntry (pMeterNode);

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhSetDiffServTBParamStatus
 Input       :  The Indices
                DiffServTBParamId

                The Object 
                setValDiffServTBParamStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDiffServTBParamStatus (UINT4 u4DiffServTBParamId,
                             INT4 i4SetValDiffServTBParamStatus)
{
    if (nmhSetFsQoSMeterStatus (u4DiffServTBParamId,
                                i4SetValDiffServTBParamStatus) != SNMP_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2DiffServTBParamType
 Input       :  The Indices
                DiffServTBParamId

                The Object 
                testValDiffServTBParamType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2DiffServTBParamType (UINT4 *pu4ErrorCode, UINT4 u4DiffServTBParamId,
                              tSNMP_OID_TYPE * pTestValDiffServTBParamType)
{
    UINT4               u4TBParamType = DS_TB_STB;

    if (QosUtlValidateTBParamEntry (u4DiffServTBParamId, pu4ErrorCode)
        == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QosUtlValidateTBParamEntry () "
                      "Returns FAILURE. \r\n", __FUNCTION__);
        return SNMP_FAILURE;
    }
    if (QosGetTBparamTypeFromOid (pTestValDiffServTBParamType,
                                  &u4TBParamType) != QOS_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2DiffServTBParamRate
 Input       :  The Indices
                DiffServTBParamId

                The Object 
                testValDiffServTBParamRate
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2DiffServTBParamRate (UINT4 *pu4ErrorCode, UINT4 u4DiffServTBParamId,
                              UINT4 u4TestValDiffServTBParamRate)
{
    if (QosUtlValidateTBParamEntry (u4DiffServTBParamId, pu4ErrorCode)
        == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QosUtlValidateTBParamEntry () "
                      "Returns FAILURE. \r\n", __FUNCTION__);
        return SNMP_FAILURE;
    }
    return (nmhTestv2FsQoSMeterCIR (pu4ErrorCode,
                                    u4DiffServTBParamId,
                                    u4TestValDiffServTBParamRate));
}

/****************************************************************************
 Function    :  nmhTestv2DiffServTBParamBurstSize
 Input       :  The Indices
                DiffServTBParamId

                The Object 
                testValDiffServTBParamBurstSize
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2DiffServTBParamBurstSize (UINT4 *pu4ErrorCode,
                                   UINT4 u4DiffServTBParamId,
                                   INT4 i4TestValDiffServTBParamBurstSize)
{
    if (QosUtlValidateTBParamEntry (u4DiffServTBParamId, pu4ErrorCode)
        == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QosUtlValidateTBParamEntry () "
                      "Returns FAILURE. \r\n", __FUNCTION__);
        return SNMP_FAILURE;
    }
    return (nmhTestv2FsQoSMeterCBS (pu4ErrorCode, u4DiffServTBParamId,
                                    i4TestValDiffServTBParamBurstSize));
}

/****************************************************************************
 Function    :  nmhTestv2DiffServTBParamInterval
 Input       :  The Indices
                DiffServTBParamId

                The Object 
                testValDiffServTBParamInterval
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2DiffServTBParamInterval (UINT4 *pu4ErrorCode,
                                  UINT4 u4DiffServTBParamId,
                                  UINT4 u4TestValDiffServTBParamInterval)
{
    if (QosUtlValidateTBParamEntry (u4DiffServTBParamId, pu4ErrorCode)
        == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QosUtlValidateTBParamEntry () "
                      "Returns FAILURE. \r\n", __FUNCTION__);
        return SNMP_FAILURE;
    }
    return (nmhTestv2FsQoSMeterInterval (pu4ErrorCode, u4DiffServTBParamId,
                                         u4TestValDiffServTBParamInterval));
}

/****************************************************************************
 Function    :  nmhTestv2DiffServTBParamStorage
 Input       :  The Indices
                DiffServTBParamId

                The Object 
                testValDiffServTBParamStorage
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2DiffServTBParamStorage (UINT4 *pu4ErrorCode, UINT4 u4DiffServTBParamId,
                                 INT4 i4TestValDiffServTBParamStorage)
{
    if (QosUtlValidateTBParamEntry (u4DiffServTBParamId, pu4ErrorCode)
        == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QosUtlValidateTBParamEntry () "
                      "Returns FAILURE. \r\n", __FUNCTION__);
        return SNMP_FAILURE;
    }
    if ((i4TestValDiffServTBParamStorage == QOS_STORAGE_VOLATILE) ||
        (i4TestValDiffServTBParamStorage == QOS_STORAGE_NONVOLATILE))
    {
        return SNMP_SUCCESS;
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

}

/****************************************************************************
 Function    :  nmhTestv2DiffServTBParamStatus
 Input       :  The Indices
                DiffServTBParamId

                The Object 
                testValDiffServTBParamStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2DiffServTBParamStatus (UINT4 *pu4ErrorCode, UINT4 u4DiffServTBParamId,
                                INT4 i4TestValDiffServTBParamStatus)
{
    UINT4               u4CurrentType = DS_TBPARM;
    UINT4               u4DummyId = 0;

    if (gQoSGlobalInfo.eSysControl != QOS_SYS_CNTL_START)
    {
        QOS_TRC (MGMT_TRC, QOS_MODULE_SHUTDOW_ERROR_MSG);
        return (SNMP_FAILURE);
    }
    if ((i4TestValDiffServTBParamStatus == QOS_DESTROY) &&
        (QosUtlPointedToStatus
         (&u4CurrentType, &u4DiffServTBParamId, &u4DummyId, DS_TBPARM,
          u4DiffServTBParamId, 0, QOS_CHECK) != QOS_SUCCESS))
    {
        return SNMP_FAILURE;
    }

    if ((i4TestValDiffServTBParamStatus == QOS_CREATE_AND_WAIT) &&
        (QOS_TBPARM_COUNT >= QOS_METER_TBL_MAX_ENTRIES))
    {
        return (SNMP_FAILURE);
    }

    return (nmhTestv2FsQoSMeterStatus (pu4ErrorCode, u4DiffServTBParamId,
                                       i4TestValDiffServTBParamStatus));
}

/* Low Level Dependency Routines for All Objects  */
/****************************************************************************
 Function    :  nmhDepv2DiffServTBParamTable
 Input       :  The Indices
                DiffServTBParamId
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2DiffServTBParamTable (UINT4 *pu4ErrorCode,
                              tSnmpIndexList * pSnmpIndexList,
                              tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetDiffServActionNextFree
 Input       :  The Indices

                The Object 
                retValDiffServActionNextFree
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDiffServActionNextFree (UINT4 *pu4RetValDiffServActionNextFree)
{
    if (gQoSGlobalInfo.eSysControl != QOS_SYS_CNTL_START)
    {
        QOS_TRC (MGMT_TRC, QOS_MODULE_SHUTDOW_ERROR_MSG);
        *pu4RetValDiffServActionNextFree = 0;
        return (SNMP_SUCCESS);
    }
    *pu4RetValDiffServActionNextFree = QOS_ACTION_NEXT_FREE ();
    return (SNMP_SUCCESS);
}

/* LOW LEVEL Routines for Table : DiffServActionTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceDiffServActionTable
 Input       :  The Indices
                DiffServActionId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceDiffServActionTable (UINT4 u4DiffServActionId)
{
    tQosStdActionEntry *pActionEntry = NULL;

    pActionEntry = QoSUtlGetActionNode (u4DiffServActionId);
    if (pActionEntry == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetActionNode () Returns "
                      "FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFirstIndexDiffServActionTable
 Input       :  The Indices
                DiffServActionId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexDiffServActionTable (UINT4 *pu4DiffServActionId)
{
    INT4                i4RetStatus = SNMP_FAILURE;

    *pu4DiffServActionId = QOS_START_INITIAL_INDEX;
    if (gQoSGlobalInfo.eSysControl != QOS_SYS_CNTL_START)
    {
        QOS_TRC (MGMT_TRC, QOS_MODULE_SHUTDOW_ERROR_MSG);
        return (SNMP_FAILURE);
    }
    i4RetStatus = QoSGetNextActionTblEntryIndex (QOS_START_INITIAL_INDEX,
                                                 pu4DiffServActionId);
    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSGetNextActionTblEntryIndex () "
                      "Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetNextIndexDiffServActionTable
 Input       :  The Indices
                DiffServActionId
                nextDiffServActionId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexDiffServActionTable (UINT4 u4DiffServActionId,
                                    UINT4 *pu4NextDiffServActionId)
{
    INT4                i4RetStatus = SNMP_FAILURE;

    if (gQoSGlobalInfo.eSysControl != QOS_SYS_CNTL_START)
    {
        QOS_TRC (MGMT_TRC, QOS_MODULE_SHUTDOW_ERROR_MSG);
        return (SNMP_FAILURE);
    }
    if (QosValidateIndex (u4DiffServActionId, DS_ACTION) == QOS_FAILURE)
    {
        return (SNMP_FAILURE);
    }

    i4RetStatus =
        QoSGetNextActionTblEntryIndex (u4DiffServActionId,
                                       pu4NextDiffServActionId);
    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSGetNextActionTblEntryIndex () "
                      "Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);
}

/* Low Level GET Routine for All Objects */

/****************************************************************************
 Function    :  nmhGetDiffServActionInterface
 Input       :  The Indices
                DiffServActionId

                The Object 
                retValDiffServActionInterface
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDiffServActionInterface (UINT4 u4DiffServActionId,
                               INT4 *pi4RetValDiffServActionInterface)
{
    tQosStdActionEntry *pActionEntry = NULL;

    if (gQoSGlobalInfo.eSysControl != QOS_SYS_CNTL_START)
    {
        QOS_TRC (MGMT_TRC, QOS_MODULE_SHUTDOW_ERROR_MSG);
        return (SNMP_FAILURE);
    }
    pActionEntry = QoSUtlGetActionNode (u4DiffServActionId);

    if (pActionEntry == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetActionNode () Returns "
                      "FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }
    *pi4RetValDiffServActionInterface = pActionEntry->i4DSActionInterface;
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetDiffServActionNext
 Input       :  The Indices
                DiffServActionId

                The Object 
                retValDiffServActionNext
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDiffServActionNext (UINT4 u4DiffServActionId,
                          tSNMP_OID_TYPE * pRetValDiffServActionNext)
{
    tQosStdActionEntry *pActionNode = NULL;
    UINT4               u4NextActionId = QOS_ROWPOINTER_DEF;
    UINT4               u4NextActionType = QOS_ROWPOINTER_DEF;

    if (gQoSGlobalInfo.eSysControl != QOS_SYS_CNTL_START)
    {
        QOS_TRC (MGMT_TRC, QOS_MODULE_SHUTDOW_ERROR_MSG);
        return (SNMP_FAILURE);
    }
    pActionNode = QoSUtlGetActionNode (u4DiffServActionId);

    if (pActionNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetActionNode () "
                      " Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    u4NextActionId = pActionNode->u4DsActionNextId;
    u4NextActionType = pActionNode->u4DSActionNextType;
    if (QosValidateIndex (u4NextActionId, u4NextActionType) == QOS_FAILURE)
    {
        return (SNMP_FAILURE);
    }

    if (QosGetOidFromTypeId (u4NextActionId, u4NextActionType,
                             pRetValDiffServActionNext) == QOS_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetDiffServActionSpecific
 Input       :  The Indices
                DiffServActionId

                The Object 
                retValDiffServActionSpecific
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDiffServActionSpecific (UINT4 u4DiffServActionId,
                              tSNMP_OID_TYPE * pRetValDiffServActionSpecific)
{
    tQosStdActionEntry *pActionNode = NULL;
    UINT4               u4ActionSpecificId = QOS_ROWPOINTER_DEF;
    UINT4               u4ActionSpecificType = QOS_ROWPOINTER_DEF;

    if (gQoSGlobalInfo.eSysControl != QOS_SYS_CNTL_START)
    {
        QOS_TRC (MGMT_TRC, QOS_MODULE_SHUTDOW_ERROR_MSG);
        return (SNMP_FAILURE);
    }
    pActionNode = QoSUtlGetActionNode (u4DiffServActionId);
    if (pActionNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetActionNode () "
                      " Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    u4ActionSpecificId = pActionNode->u4DSActionSpecificIndex;
    u4ActionSpecificType = pActionNode->u4DSActionSpecificType;

    if (QosValidateIndex (u4ActionSpecificId, u4ActionSpecificType)
        == QOS_FAILURE)
    {
        return (SNMP_FAILURE);
    }
    if (QosGetOidFromTypeId (u4ActionSpecificId, u4ActionSpecificType,
                             pRetValDiffServActionSpecific) == QOS_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetDiffServActionStorage
 Input       :  The Indices
                DiffServActionId

                The Object 
                retValDiffServActionStorage
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDiffServActionStorage (UINT4 u4DiffServActionId,
                             INT4 *pi4RetValDiffServActionStorage)
{
    tQosStdActionEntry *pActionNode = NULL;

    if (gQoSGlobalInfo.eSysControl != QOS_SYS_CNTL_START)
    {
        QOS_TRC (MGMT_TRC, QOS_MODULE_SHUTDOW_ERROR_MSG);
        return (SNMP_FAILURE);
    }
    pActionNode = QoSUtlGetActionNode (u4DiffServActionId);
    if (pActionNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetActionNode () "
                      " Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }
    *pi4RetValDiffServActionStorage = pActionNode->i4StorageType;
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetDiffServActionStatus
 Input       :  The Indices
                DiffServActionId

                The Object 
                retValDiffServActionStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDiffServActionStatus (UINT4 u4DiffServActionId,
                            INT4 *pi4RetValDiffServActionStatus)
{
    tQosStdActionEntry *pActionNode = NULL;

    if (gQoSGlobalInfo.eSysControl != QOS_SYS_CNTL_START)
    {
        QOS_TRC (MGMT_TRC, QOS_MODULE_SHUTDOW_ERROR_MSG);
        return (SNMP_FAILURE);
    }
    pActionNode = QoSUtlGetActionNode (u4DiffServActionId);
    if (pActionNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetActionNode () "
                      " Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }
    *pi4RetValDiffServActionStatus = pActionNode->u1DSActionStatus;
    return (SNMP_SUCCESS);
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetDiffServActionInterface
 Input       :  The Indices
                DiffServActionId

                The Object 
                setValDiffServActionInterface
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDiffServActionInterface (UINT4 u4DiffServActionId,
                               INT4 i4SetValDiffServActionInterface)
{
    tQosStdActionEntry *pActionNode = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;

    pActionNode = QoSUtlGetActionNode (u4DiffServActionId);
    if (pActionNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetActionNode () "
                      " Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }
    pActionNode->i4DSActionInterface = i4SetValDiffServActionInterface;
    QoSValidateActionTableStatus (pActionNode);

    SnmpNotifyInfo.pu4ObjectId = DiffServActionInterface;
    SnmpNotifyInfo.u4OidLen = sizeof (DiffServActionInterface) / sizeof (UINT4);
    SnmpNotifyInfo.u4SeqNum = 0;
    SnmpNotifyInfo.u1RowStatus = FALSE;
    SnmpNotifyInfo.pLockPointer = QoSLock;
    SnmpNotifyInfo.pUnLockPointer = QoSUnLock;
    SnmpNotifyInfo.u4Indices = 1;
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%u %i",
                      u4DiffServActionId, i4SetValDiffServActionInterface));
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhSetDiffServActionNext
 Input       :  The Indices
                DiffServActionId

                The Object 
                setValDiffServActionNext
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDiffServActionNext (UINT4 u4DiffServActionId,
                          tSNMP_OID_TYPE * pSetValDiffServActionNext)
{
    tQosStdActionEntry *pActionNode = NULL;
    UINT4               u4ActionNextId = QOS_ROWPOINTER_DEF;
    UINT4               u4ActionNextType = QOS_ROWPOINTER_DEF;
    UINT4               u4CurrentType = DS_ACTION;
    UINT4               u4DummyId = 0;
    tSnmpNotifyInfo     SnmpNotifyInfo;

    pActionNode = QoSUtlGetActionNode (u4DiffServActionId);
    if (pActionNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetActionNode () "
                      " Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }
    if (QosGetTypeIdFromOid (pSetValDiffServActionNext, &u4ActionNextType,
                             &u4ActionNextId) == QOS_FAILURE)
    {
        return SNMP_FAILURE;
    }
    QosUtlPointedToStatus (&u4CurrentType, &u4DiffServActionId, &u4DummyId,
                           pActionNode->u4DSActionNextType,
                           pActionNode->u4DsActionNextId, 0, QOS_DEL);
    pActionNode->u4DsActionNextId = u4ActionNextId;
    pActionNode->u4DSActionNextType = u4ActionNextType;
    QosUtlPointedToStatus (&u4CurrentType, &u4DiffServActionId, &u4DummyId,
                           u4ActionNextType, u4ActionNextId, 0, QOS_ADD);
    QoSValidateActionTableStatus (pActionNode);

    SnmpNotifyInfo.pu4ObjectId = DiffServActionNext;
    SnmpNotifyInfo.u4OidLen = sizeof (DiffServActionNext) / sizeof (UINT4);
    SnmpNotifyInfo.u4SeqNum = 0;
    SnmpNotifyInfo.u1RowStatus = FALSE;
    SnmpNotifyInfo.pLockPointer = QoSLock;
    SnmpNotifyInfo.pUnLockPointer = QoSUnLock;
    SnmpNotifyInfo.u4Indices = 1;
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%u %o",
                      u4DiffServActionId, pSetValDiffServActionNext));
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetDiffServActionSpecific
 Input       :  The Indices
                DiffServActionId

                The Object 
                setValDiffServActionSpecific
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDiffServActionSpecific (UINT4 u4DiffServActionId,
                              tSNMP_OID_TYPE * pSetValDiffServActionSpecific)
{
    tQosStdActionEntry *pActionNode = NULL;
    UINT4               u4ActionSpecificId = QOS_ROWPOINTER_DEF;
    UINT4               u4ActionSpecificType = QOS_ROWPOINTER_DEF;
    UINT4               u4CurrentType = DS_ACTION;
    UINT4               u4DummyId = 0;
    INT4                i4AlgoDropType = 0;
    tSnmpNotifyInfo     SnmpNotifyInfo;

    pActionNode = QoSUtlGetActionNode (u4DiffServActionId);
    if (pActionNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetActionNode () "
                      " Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }
    if (QosGetTypeIdFromOid
        (pSetValDiffServActionSpecific, &u4ActionSpecificType,
         &u4ActionSpecificId) == QOS_FAILURE)
    {
        return (SNMP_FAILURE);
    }

    if ((u4ActionSpecificType != DS_STDDSCP) &&
        (u4ActionSpecificType != DS_COUNT_ACT) &&
        (u4ActionSpecificType != DS_ALG_DROP) &&
        (u4ActionSpecificType != QOS_ROWPOINTER_DEF))
    {
        return SNMP_FAILURE;
    }

    if (u4ActionSpecificType == DS_ALG_DROP)
    {
        nmhGetDiffServAlgDropType (u4ActionSpecificId, &i4AlgoDropType);
#ifdef NPAPI_WANTED
        if (QosIsDropTypeValid (i4AlgoDropType) == QOS_FAILURE)
        {
            return SNMP_FAILURE;
        }
#endif
    }

    if (QosValidateIndex (u4ActionSpecificId, u4ActionSpecificType)
        == QOS_FAILURE)
    {
        return (SNMP_FAILURE);
    }
    QosUtlPointedToStatus (&u4CurrentType, &u4DiffServActionId, &u4DummyId,
                           pActionNode->u4DSActionSpecificType,
                           pActionNode->u4DSActionSpecificIndex, 0, QOS_DEL);
    pActionNode->u4DSActionSpecificIndex = u4ActionSpecificId;
    pActionNode->u4DSActionSpecificType = u4ActionSpecificType;
    QosUtlPointedToStatus (&u4CurrentType, &u4DiffServActionId, &u4DummyId,
                           u4ActionSpecificType, u4ActionSpecificId, 0,
                           QOS_ADD);
    QoSValidateActionTableStatus (pActionNode);

    SnmpNotifyInfo.pu4ObjectId = DiffServActionSpecific;
    SnmpNotifyInfo.u4OidLen = sizeof (DiffServActionSpecific) / sizeof (UINT4);
    SnmpNotifyInfo.u4SeqNum = 0;
    SnmpNotifyInfo.u1RowStatus = FALSE;
    SnmpNotifyInfo.pLockPointer = QoSLock;
    SnmpNotifyInfo.pUnLockPointer = QoSUnLock;
    SnmpNotifyInfo.u4Indices = 1;
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%u %o",
                      u4DiffServActionId, pSetValDiffServActionSpecific));
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetDiffServActionStorage
 Input       :  The Indices
                DiffServActionId

                The Object 
                setValDiffServActionStorage
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDiffServActionStorage (UINT4 u4DiffServActionId,
                             INT4 i4SetValDiffServActionStorage)
{
    tQosStdActionEntry *pActionNode = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;

    pActionNode = QoSUtlGetActionNode (u4DiffServActionId);
    if (pActionNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetActionNode () "
                      " Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }
    pActionNode->i4StorageType = i4SetValDiffServActionStorage;
    QoSValidateActionTableStatus (pActionNode);

    SnmpNotifyInfo.pu4ObjectId = DiffServActionStorage;
    SnmpNotifyInfo.u4OidLen = sizeof (DiffServActionStorage) / sizeof (UINT4);
    SnmpNotifyInfo.u4SeqNum = 0;
    SnmpNotifyInfo.u1RowStatus = FALSE;
    SnmpNotifyInfo.pLockPointer = QoSLock;
    SnmpNotifyInfo.pUnLockPointer = QoSUnLock;
    SnmpNotifyInfo.u4Indices = 1;
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%u %i",
                      u4DiffServActionId, i4SetValDiffServActionStorage));
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhSetDiffServActionStatus
 Input       :  The Indices
                DiffServActionId

                The Object 
                setValDiffServActionStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDiffServActionStatus (UINT4 u4DiffServActionId,
                            INT4 i4SetValDiffServActionStatus)
{
    tQosStdActionEntry *pActionNode = NULL;
    INT4                i4RowStatus = 0;
    INT4                i4RetStatus = SNMP_FAILURE;
    UINT4               u4DSActionNextType = QOS_ROWPOINTER_DEF;
    UINT4               u4DsActionNextId = QOS_ROWPOINTER_DEF;
    UINT4               u4DSActionSpecificType = QOS_ROWPOINTER_DEF;
    UINT4               u4DSActionSpecificIndex = QOS_ROWPOINTER_DEF;
    UINT4               u4CurrentType = DS_ACTION;
    UINT4               u4DummyId = 0;
    tSnmpNotifyInfo     SnmpNotifyInfo;

    pActionNode = QoSUtlGetActionNode (u4DiffServActionId);
    if (pActionNode != NULL)
    {
        i4RowStatus = (UINT4) pActionNode->u1DSActionStatus;

        /* Entry found in the Table */
        if (i4RowStatus == i4SetValDiffServActionStatus)
        {
            /* same value */
            return (SNMP_SUCCESS);
        }
        else if (i4SetValDiffServActionStatus == QOS_CREATE_AND_WAIT)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (i4SetValDiffServActionStatus != QOS_CREATE_AND_WAIT)
        {
            return SNMP_FAILURE;
        }
    }
    switch (i4SetValDiffServActionStatus)
    {
            /* For a New Entry */
        case QOS_CREATE_AND_WAIT:

            pActionNode = QoSCreateActionTblEntry (u4DiffServActionId);

            if (pActionNode == NULL)
            {
                return (SNMP_FAILURE);
            }

            pActionNode->u1DSActionStatus = QOS_NOT_READY;
            break;
            /* Modify an Entry */
        case QOS_ACTIVE:
            if ((pActionNode->u1DSActionStatus == QOS_NOT_IN_SERVICE) &&
                (gQoSGlobalInfo.eSysStatus == QOS_SYS_STATUS_ENABLE))
            {
                QosUtlUpdateClfrElemStatus (DS_ACTION, u4DiffServActionId, 0,
                                            QOS_ACTIVE);

                pActionNode->u1DSActionStatus = QOS_ACTIVE;
            }

            break;

        case QOS_NOT_IN_SERVICE:

            if ((pActionNode->u1DSActionStatus == QOS_ACTIVE) &&
                (gQoSGlobalInfo.eSysStatus == QOS_SYS_STATUS_ENABLE))
            {
                QosUtlUpdateClfrElemStatus (DS_ACTION, u4DiffServActionId, 0,
                                            QOS_NOT_IN_SERVICE);
                pActionNode->u1DSActionStatus = QOS_NOT_IN_SERVICE;
            }
            break;

        case QOS_DESTROY:
            u4DSActionNextType = pActionNode->u4DSActionNextType;
            u4DsActionNextId = pActionNode->u4DsActionNextId;
            u4DSActionSpecificType = pActionNode->u4DSActionSpecificType;
            u4DSActionSpecificIndex = pActionNode->u4DSActionSpecificIndex;

            if (gQoSGlobalInfo.eSysStatus == QOS_SYS_STATUS_ENABLE)
            {
                /* 1. Remove the entry */
                i4RetStatus = QoSDeleteActionTblEntry (pActionNode);
                if (i4RetStatus == QOS_FAILURE)
                {
                    QOS_TRC_ARG1 (MGMT_TRC,
                                  "In %s : QoSDeleteActionTblEntry () "
                                  " Returns FAILURE. \r\n", __FUNCTION__);

                    return (SNMP_FAILURE);
                }
            }
            QosUtlPointedToStatus (&u4CurrentType, &u4DiffServActionId,
                                   &u4DummyId, u4DSActionNextType,
                                   u4DsActionNextId, 0, QOS_DEL);
            QosUtlPointedToStatus (&u4CurrentType, &u4DiffServActionId,
                                   &u4DummyId, u4DSActionSpecificType,
                                   u4DSActionSpecificIndex, 0, QOS_DEL);
            break;
            /* Not Supported */
        case QOS_CREATE_AND_GO:
            return (SNMP_FAILURE);

        default:
            return (SNMP_FAILURE);
    }
    QosUpdateDiffServActionNextFree (u4DiffServActionId,
                                     i4SetValDiffServActionStatus);

    SnmpNotifyInfo.pu4ObjectId = DiffServActionStatus;
    SnmpNotifyInfo.u4OidLen = sizeof (DiffServActionStatus) / sizeof (UINT4);
    SnmpNotifyInfo.u4SeqNum = 0;
    SnmpNotifyInfo.u1RowStatus = TRUE;
    SnmpNotifyInfo.pLockPointer = QoSLock;
    SnmpNotifyInfo.pUnLockPointer = QoSUnLock;
    SnmpNotifyInfo.u4Indices = 1;
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%u %i",
                      u4DiffServActionId, i4SetValDiffServActionStatus));
    return (SNMP_SUCCESS);
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2DiffServActionInterface
 Input       :  The Indices
                DiffServActionId

                The Object 
                testValDiffServActionInterface
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2DiffServActionInterface (UINT4 *pu4ErrorCode,
                                  UINT4 u4DiffServActionId,
                                  INT4 i4TestValDiffServActionInterface)
{
    INT4                i4RetStatus = SNMP_FAILURE;

    if (QosUtlValidateActionEntry (u4DiffServActionId, pu4ErrorCode)
        == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QosUtlValidateActionEntry () "
                      "Returns FAILURE. \r\n", __FUNCTION__);
        return SNMP_FAILURE;
    }
    /* Check Object's Value */
    i4RetStatus =
        QoSUtlValidateIfIndex (i4TestValDiffServActionInterface, pu4ErrorCode);
    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlValidateIfIndex ()"
                      "Returns FAILURE. \r\n", __FUNCTION__);
        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhTestv2DiffServActionNext
 Input       :  The Indices
                DiffServActionId

                The Object 
                testValDiffServActionNext
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2DiffServActionNext (UINT4 *pu4ErrorCode, UINT4 u4DiffServActionId,
                             tSNMP_OID_TYPE * pTestValDiffServActionNext)
{
    UINT4               u4ActionNextType = QOS_ROWPOINTER_DEF;
    UINT4               u4ActionNextId = QOS_ROWPOINTER_DEF;

    if (QosUtlValidateActionEntry (u4DiffServActionId, pu4ErrorCode)
        == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QosUtlValidateTBParamEntry () "
                      "Returns FAILURE. \r\n", __FUNCTION__);
        return SNMP_FAILURE;
    }
    if (QosGetTypeIdFromOid (pTestValDiffServActionNext, &u4ActionNextType,
                             &u4ActionNextId) == QOS_SUCCESS)
    {
#ifdef NPAPI_WANTED
        /*1st param : Currentype, 2nd param: 1stNextType
         * 3rd param: 2ndNextType if any, 4thParam: 3rdNexttype
         * if any , 5thParm : Specific type if any*/
        if (QosIsTypeValid (DS_ACTION, u4ActionNextType, QOS_ROWPOINTER_DEF,
                            QOS_ROWPOINTER_DEF, DS_STDDSCP) != QOS_SUCCESS)
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return (SNMP_FAILURE);
        }
#endif
        if (QosValidateIndex (u4ActionNextId, u4ActionNextType) == QOS_FAILURE)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return (SNMP_FAILURE);
        }
        return SNMP_SUCCESS;
    }
    return (SNMP_FAILURE);
}

/****************************************************************************
 Function    :  nmhTestv2DiffServActionSpecific
 Input       :  The Indices
                DiffServActionId

                The Object 
                testValDiffServActionSpecific
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2DiffServActionSpecific (UINT4 *pu4ErrorCode, UINT4 u4DiffServActionId,
                                 tSNMP_OID_TYPE *
                                 pTestValDiffServActionSpecific)
{
    UINT4               u4ActionSpecificType = QOS_ROWPOINTER_DEF;
    UINT4               u4ActionSpecificId = QOS_ROWPOINTER_DEF;

    if (QosUtlValidateActionEntry (u4DiffServActionId, pu4ErrorCode)
        == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QosUtlValidateTBParamEntry () "
                      "Returns FAILURE. \r\n", __FUNCTION__);
        return SNMP_FAILURE;
    }
    if (QosGetTypeIdFromOid (pTestValDiffServActionSpecific,
                             &u4ActionSpecificType, &u4ActionSpecificId)
        == QOS_SUCCESS)
    {
#ifdef NPAPI_WANTED
        /*1st param : Currentype, 2nd param: 1stNextType
         * 3rd param: 2ndNextType if any, 4thParam: 3rdNexttype
         * if any , 5thParm : Specific type if any*/
        if (QosIsTypeValid
            (DS_ACTION, QOS_ROWPOINTER_DEF, QOS_ROWPOINTER_DEF,
             QOS_ROWPOINTER_DEF, u4ActionSpecificType) != QOS_SUCCESS)
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
        }
#endif
        if (QosValidateIndex (u4ActionSpecificId, u4ActionSpecificType)
            == QOS_FAILURE)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return (SNMP_FAILURE);
        }
        return (SNMP_SUCCESS);
    }
    return (SNMP_FAILURE);
}

/****************************************************************************
 Function    :  nmhTestv2DiffServActionStorage
 Input       :  The Indices
                DiffServActionId

                The Object 
                testValDiffServActionStorage
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2DiffServActionStorage (UINT4 *pu4ErrorCode, UINT4 u4DiffServActionId,
                                INT4 i4TestValDiffServActionStorage)
{
    if (QosUtlValidateActionEntry (u4DiffServActionId, pu4ErrorCode)
        == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QosUtlValidateTBParamEntry () "
                      "Returns FAILURE. \r\n", __FUNCTION__);
        return SNMP_FAILURE;
    }
    if ((i4TestValDiffServActionStorage != QOS_STORAGE_VOLATILE) &&
        (i4TestValDiffServActionStorage != QOS_STORAGE_NONVOLATILE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhTestv2DiffServActionStatus
 Input       :  The Indices
                DiffServActionId

                The Object 
                testValDiffServActionStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2DiffServActionStatus (UINT4 *pu4ErrorCode, UINT4 u4DiffServActionId,
                               INT4 i4TestValDiffServActionStatus)
{
    tQosStdActionEntry *pActionNode = NULL;
    UINT4               u4CurrentType = DS_ACTION;
    UINT4               u4DummyId = 0;

    if (gQoSGlobalInfo.eSysControl != QOS_SYS_CNTL_START)
    {
        QOS_TRC (MGMT_TRC, QOS_MODULE_SHUTDOW_ERROR_MSG);
        return (SNMP_FAILURE);
    }

    if ((i4TestValDiffServActionStatus == QOS_CREATE_AND_WAIT) &&
        (QOS_ACTION_COUNT >= QOS_ACTION_TBL_MAX_ENTRIES))
    {
        return (SNMP_FAILURE);
    }

    if ((i4TestValDiffServActionStatus == QOS_DESTROY) &&
        (QosUtlPointedToStatus
         (&u4CurrentType, &u4DiffServActionId, &u4DummyId, DS_ACTION,
          u4DiffServActionId, 0, QOS_CHECK) != QOS_SUCCESS))
    {
        return SNMP_FAILURE;
    }
    pActionNode = QoSUtlGetActionNode (u4DiffServActionId);
    if (pActionNode == NULL)
    {
        /*Entry Not found in the table */
        switch (i4TestValDiffServActionStatus)
        {
            case CREATE_AND_WAIT:
                if (gQoSGlobalInfo.DSNextFree.u4ActionNextFree
                    == QOS_INDEX_FULL)
                {
                    return SNMP_FAILURE;
                }
                break;
            default:
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return (SNMP_FAILURE);
        }
    }
    else
    {
        switch (i4TestValDiffServActionStatus)
        {
                /*Entry Found in the table */
            case ACTIVE:
                if ((pActionNode->u1DSActionStatus == NOT_READY))
                {
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                    return (SNMP_FAILURE);
                }
                break;

            case NOT_IN_SERVICE:
                break;

            case CREATE_AND_WAIT:
            case CREATE_AND_GO:
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return (SNMP_FAILURE);

            case DESTROY:
                break;
            default:
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                return (SNMP_FAILURE);
        }
    }
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2DiffServActionTable
 Input       :  The Indices
                DiffServActionId
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2DiffServActionTable (UINT4 *pu4ErrorCode,
                             tSnmpIndexList * pSnmpIndexList,
                             tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : DiffServDscpMarkActTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceDiffServDscpMarkActTable
 Input       :  The Indices
                DiffServDscpMarkActDscp
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceDiffServDscpMarkActTable (INT4
                                                  i4DiffServDscpMarkActDscp)
{
    if ((i4DiffServDscpMarkActDscp < QOS_DSCP_MIN_VAL) ||
        (i4DiffServDscpMarkActDscp > QOS_DSCP_MAX_VAL))
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexDiffServDscpMarkActTable
 Input       :  The Indices
                DiffServDscpMarkActDscp
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexDiffServDscpMarkActTable (INT4 *pi4DiffServDscpMarkActDscp)
{

    if (gQoSGlobalInfo.eSysControl != QOS_SYS_CNTL_START)
    {
        QOS_TRC (MGMT_TRC, QOS_MODULE_SHUTDOW_ERROR_MSG);
        return (SNMP_FAILURE);
    }
    *pi4DiffServDscpMarkActDscp = QOS_START_INITIAL_INDEX;

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetNextIndexDiffServDscpMarkActTable
 Input       :  The Indices
                DiffServDscpMarkActDscp
                nextDiffServDscpMarkActDscp
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexDiffServDscpMarkActTable (INT4 i4DiffServDscpMarkActDscp,
                                         INT4 *pi4NextDiffServDscpMarkActDscp)
{

    if (gQoSGlobalInfo.eSysControl != QOS_SYS_CNTL_START)
    {
        QOS_TRC (MGMT_TRC, QOS_MODULE_SHUTDOW_ERROR_MSG);
        return (SNMP_FAILURE);
    }
    if ((i4DiffServDscpMarkActDscp < QOS_DSCP_MIN_VAL) ||
        (i4DiffServDscpMarkActDscp >= QOS_DSCP_MAX_VAL))
    {
        return SNMP_FAILURE;
    }
    *pi4NextDiffServDscpMarkActDscp =
        i4DiffServDscpMarkActDscp + QOS_NEXT_INDEX;
    return (SNMP_SUCCESS);
}

/* Low Level GET Routine for All Objects  */
/****************************************************************************
 Function    :  nmhGetDiffServCountActNextFree
 Input       :  The Indices

                The Object 
                retValDiffServCountActNextFree
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDiffServCountActNextFree (UINT4 *pu4RetValDiffServCountActNextFree)
{
    if (gQoSGlobalInfo.eSysControl != QOS_SYS_CNTL_START)
    {
        QOS_TRC (MGMT_TRC, QOS_MODULE_SHUTDOW_ERROR_MSG);
        *pu4RetValDiffServCountActNextFree = 0;
        return (SNMP_SUCCESS);
    }
    *pu4RetValDiffServCountActNextFree = QOS_CNTACT_NEXT_FREE ();
    return (SNMP_SUCCESS);
}

/* LOW LEVEL Routines for Table : DiffServCountActTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceDiffServCountActTable
 Input       :  The Indices
                DiffServCountActId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceDiffServCountActTable (UINT4 u4DiffServCountActId)
{
    tQosStdCountActEntry *pCountActEntry = NULL;

    pCountActEntry = QoSUtlGetCountActionNode (u4DiffServCountActId);

    if (pCountActEntry == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetCountActionNode () Returns "
                      "FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFirstIndexDiffServCountActTable
 Input       :  The Indices
                DiffServCountActId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */
INT1
nmhGetFirstIndexDiffServCountActTable (UINT4 *pu4DiffServCountActId)
{
    INT4                i4RetStatus = SNMP_FAILURE;

    if (gQoSGlobalInfo.eSysControl != QOS_SYS_CNTL_START)
    {
        QOS_TRC (MGMT_TRC, QOS_MODULE_SHUTDOW_ERROR_MSG);
        return (SNMP_FAILURE);
    }

    i4RetStatus = QoSGetNextCountActEntryIndex (QOS_START_INITIAL_INDEX,
                                                pu4DiffServCountActId);
    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSGetNextCountActEntryIndex () "
                      "Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetNextIndexDiffServCountActTable
 Input       :  The Indices
                DiffServCountActId
                nextDiffServCountActId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexDiffServCountActTable (UINT4 u4DiffServCountActId,
                                      UINT4 *pu4NextDiffServCountActId)
{
    INT4                i4RetStatus = SNMP_FAILURE;

    if (gQoSGlobalInfo.eSysControl != QOS_SYS_CNTL_START)
    {
        QOS_TRC (MGMT_TRC, QOS_MODULE_SHUTDOW_ERROR_MSG);
        return (SNMP_FAILURE);
    }
    if (QosValidateIndex (u4DiffServCountActId, DS_COUNT_ACT) == QOS_FAILURE)
    {
        return (SNMP_FAILURE);
    }
    i4RetStatus =
        QoSGetNextCountActEntryIndex (u4DiffServCountActId,
                                      pu4NextDiffServCountActId);
    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSGetNextCountActEntryIndex () "
                      "Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetDiffServCountActOctets
 Input       :  The Indices
                DiffServCountActId

                The Object 
                retValDiffServCountActOctets
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDiffServCountActOctets (UINT4 u4DiffServCountActId,
                              tSNMP_COUNTER64_TYPE *
                              pu8RetValDiffServCountActOctets)
{
    INT4                i4RetStatus = SNMP_FAILURE;

    if (gQoSGlobalInfo.eSysControl != QOS_SYS_CNTL_START)
    {
        QOS_TRC (MGMT_TRC, QOS_MODULE_SHUTDOW_ERROR_MSG);
        return (SNMP_FAILURE);
    }
    i4RetStatus = QoSHwWrGetCountActOctets (u4DiffServCountActId,
                                            QOS_COUNT_ACT_OCTETS,
                                            pu8RetValDiffServCountActOctets);
    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG2 (MGMT_TRC, "In %s : ERROR: QoSHwWrGetCountActOctets () "
                      " Returns FAILURE for a CountAct Id %d Stats type of "
                      " QOS_COUNT_ACT_OCTETS .\r\n",
                      u4DiffServCountActId, __FUNCTION__);

        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetDiffServCountActPkts
 Input       :  The Indices
                DiffServCountActId

                The Object 
                retValDiffServCountActPkts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDiffServCountActPkts (UINT4 u4DiffServCountActId,
                            tSNMP_COUNTER64_TYPE *
                            pu8RetValDiffServCountActPkts)
{
    INT4                i4RetStatus = SNMP_FAILURE;

    if (gQoSGlobalInfo.eSysControl != QOS_SYS_CNTL_START)
    {
        QOS_TRC (MGMT_TRC, QOS_MODULE_SHUTDOW_ERROR_MSG);
        return (SNMP_FAILURE);
    }
    i4RetStatus = QoSHwWrGetCountActPkts (u4DiffServCountActId,
                                          QOS_COUNT_ACT_PKTS,
                                          pu8RetValDiffServCountActPkts);
    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG2 (MGMT_TRC, "In %s : ERROR: QoSHwWrGetCountActPkts () "
                      " Returns FAILURE for a CountAct Id %d Stats type of "
                      " QOS_COUNT_ACT_PKTS .\r\n",
                      u4DiffServCountActId, __FUNCTION__);

        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetDiffServCountActStorage
 Input       :  The Indices
                DiffServCountActId

                The Object 
                retValDiffServCountActStorage
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDiffServCountActStorage (UINT4 u4DiffServCountActId,
                               INT4 *pi4RetValDiffServCountActStorage)
{
    tQosStdCountActEntry *pCountActEntry = NULL;

    if (gQoSGlobalInfo.eSysControl != QOS_SYS_CNTL_START)
    {
        QOS_TRC (MGMT_TRC, QOS_MODULE_SHUTDOW_ERROR_MSG);
        return (SNMP_FAILURE);
    }
    pCountActEntry = QoSUtlGetCountActionNode (u4DiffServCountActId);
    if (pCountActEntry == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetCountActionNode () Returns "
                      "FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }
    *pi4RetValDiffServCountActStorage = pCountActEntry->i4StorageType;
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetDiffServCountActStatus
 Input       :  The Indices
                DiffServCountActId

                The Object 
                retValDiffServCountActStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDiffServCountActStatus (UINT4 u4DiffServCountActId,
                              INT4 *pi4RetValDiffServCountActStatus)
{
    tQosStdCountActEntry *pCountActEntry = NULL;
    if (gQoSGlobalInfo.eSysControl != QOS_SYS_CNTL_START)
    {
        QOS_TRC (MGMT_TRC, QOS_MODULE_SHUTDOW_ERROR_MSG);
        return (SNMP_FAILURE);
    }

    pCountActEntry = QoSUtlGetCountActionNode (u4DiffServCountActId);
    if (pCountActEntry == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetCountActionNode () Returns "
                      "FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }
    *pi4RetValDiffServCountActStatus = pCountActEntry->u1DSCountActStatus;
    return (SNMP_SUCCESS);
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetDiffServCountActStorage
 Input       :  The Indices
                DiffServCountActId

                The Object 
                setValDiffServCountActStorage
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDiffServCountActStorage (UINT4 u4DiffServCountActId,
                               INT4 i4SetValDiffServCountActStorage)
{
    tQosStdCountActEntry *pCountActEntry = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;

    pCountActEntry = QoSUtlGetCountActionNode (u4DiffServCountActId);

    if (pCountActEntry == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetCountActionNode () Returns "
                      "FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }
    pCountActEntry->i4StorageType = i4SetValDiffServCountActStorage;

    SnmpNotifyInfo.pu4ObjectId = DiffServCountActStorage;
    SnmpNotifyInfo.u4OidLen = sizeof (DiffServCountActStorage) / sizeof (UINT4);
    SnmpNotifyInfo.u4SeqNum = 0;
    SnmpNotifyInfo.u1RowStatus = FALSE;
    SnmpNotifyInfo.pLockPointer = QoSLock;
    SnmpNotifyInfo.pUnLockPointer = QoSUnLock;
    SnmpNotifyInfo.u4Indices = 1;
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%u %i",
                      u4DiffServCountActId, i4SetValDiffServCountActStorage));
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhSetDiffServCountActStatus
 Input       :  The Indices
                DiffServCountActId

                The Object 
                setValDiffServCountActStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDiffServCountActStatus (UINT4 u4DiffServCountActId,
                              INT4 i4SetValDiffServCountActStatus)
{
    tQosStdCountActEntry *pCountActEntry = NULL;
    INT4                i4RowStatus = 0;
    INT4                i4RetStatus = SNMP_FAILURE;
    tSnmpNotifyInfo     SnmpNotifyInfo;

    pCountActEntry = QoSUtlGetCountActionNode (u4DiffServCountActId);
    if (pCountActEntry != NULL)
    {
        i4RowStatus = (UINT4) pCountActEntry->u1DSCountActStatus;

        /* Entry found in the Table */
        if (i4RowStatus == i4SetValDiffServCountActStatus)
        {
            /* same value */
            return (SNMP_SUCCESS);
        }
    }
    else
    {
        if (i4SetValDiffServCountActStatus != QOS_CREATE_AND_WAIT)
        {
            return SNMP_FAILURE;
        }
    }
    switch (i4SetValDiffServCountActStatus)
    {
            /* For a New Entry */
        case QOS_CREATE_AND_WAIT:
            pCountActEntry = QoSCreateCountActTblEntry (u4DiffServCountActId);

            if (pCountActEntry == NULL)
            {
                return (SNMP_FAILURE);
            }
            break;

            /* Modify an Entry */
        case QOS_ACTIVE:

            if (pCountActEntry->u1DSCountActStatus == QOS_NOT_IN_SERVICE)
            {
                if (gQoSGlobalInfo.eSysStatus == QOS_SYS_STATUS_ENABLE)
                {
                    /* 1. Add the entry into:the HARDWARE. */
                    pCountActEntry->u1DSCountActStatus = QOS_ACTIVE;
                }
                break;
            }
            return SNMP_FAILURE;

        case QOS_NOT_IN_SERVICE:

            if ((pCountActEntry->u1DSCountActStatus == QOS_ACTIVE) &&
                (gQoSGlobalInfo.eSysStatus == QOS_SYS_STATUS_ENABLE))
            {
                /* 1. Remove the entry from the HARDWARE. */
                pCountActEntry->u1DSCountActStatus = QOS_NOT_IN_SERVICE;
            }
            break;

        case QOS_DESTROY:
            if ((pCountActEntry->u1DSCountActStatus == QOS_ACTIVE) &&
                (gQoSGlobalInfo.eSysStatus == QOS_SYS_STATUS_ENABLE))
            {
                /* 1. Remove the entry */
                i4RetStatus = QoSDeleteCountActTblEntry (pCountActEntry);
                if (i4RetStatus == QOS_FAILURE)
                {
                    QOS_TRC_ARG1 (MGMT_TRC,
                                  "In %s : QoSDeleteCountActTblEntry () "
                                  " Returns FAILURE. \r\n", __FUNCTION__);
                    return (SNMP_FAILURE);
                }
            }
            break;
            /* Not Supported */
        case QOS_CREATE_AND_GO:
            return (SNMP_FAILURE);

        default:
            return (SNMP_FAILURE);
    }
    QosUpdateDiffServCountActNextFree (u4DiffServCountActId,
                                       i4SetValDiffServCountActStatus);

    SnmpNotifyInfo.pu4ObjectId = DiffServCountActStatus;
    SnmpNotifyInfo.u4OidLen = sizeof (DiffServCountActStatus) / sizeof (UINT4);
    SnmpNotifyInfo.u4SeqNum = 0;
    SnmpNotifyInfo.u1RowStatus = TRUE;
    SnmpNotifyInfo.pLockPointer = QoSLock;
    SnmpNotifyInfo.pUnLockPointer = QoSUnLock;
    SnmpNotifyInfo.u4Indices = 1;
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%u %i",
                      u4DiffServCountActId, i4SetValDiffServCountActStatus));
    return (SNMP_SUCCESS);
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2DiffServCountActStorage
 Input       :  The Indices
                DiffServCountActId

                The Object 
                testValDiffServCountActStorage
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2DiffServCountActStorage (UINT4 *pu4ErrorCode,
                                  UINT4 u4DiffServCountActId,
                                  INT4 i4TestValDiffServCountActStorage)
{
    if (QosUtlValidateCountActEntry (u4DiffServCountActId, pu4ErrorCode)
        == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QosUtlValidateTBParamEntry () "
                      "Returns FAILURE. \r\n", __FUNCTION__);
        return SNMP_FAILURE;
    }
    if ((i4TestValDiffServCountActStorage != QOS_STORAGE_VOLATILE) &&
        (i4TestValDiffServCountActStorage != QOS_STORAGE_NONVOLATILE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhTestv2DiffServCountActStatus
 Input       :  The Indices
                DiffServCountActId

                The Object 
                testValDiffServCountActStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2DiffServCountActStatus (UINT4 *pu4ErrorCode,
                                 UINT4 u4DiffServCountActId,
                                 INT4 i4TestValDiffServCountActStatus)
{
    tQosStdCountActEntry *pCountActEntry = NULL;
    UINT4               u4CurrentType = DS_COUNT_ACT;
    UINT4               u4DummyId = 0;

    if (gQoSGlobalInfo.eSysControl != QOS_SYS_CNTL_START)
    {
        QOS_TRC (MGMT_TRC, QOS_MODULE_SHUTDOW_ERROR_MSG);
        return (SNMP_FAILURE);
    }
    if (QOS_CHECK_ACTION_TBL_INDEX_RANGE (u4DiffServCountActId) == QOS_FAILURE)
    {
        return (SNMP_FAILURE);
    }
    if ((i4TestValDiffServCountActStatus == QOS_DESTROY) &&
        (QosUtlPointedToStatus
         (&u4CurrentType, &u4DiffServCountActId, &u4DummyId, DS_COUNT_ACT,
          u4DiffServCountActId, 0, QOS_CHECK) != QOS_SUCCESS))
    {
        return SNMP_FAILURE;
    }
    if ((i4TestValDiffServCountActStatus == QOS_CREATE_AND_WAIT) &&
        (QOS_CNTACT_COUNT >= QOS_COUNTACT_TBL_MAX_ENTRIES))
    {
        return (SNMP_FAILURE);
    }

    pCountActEntry = QoSUtlGetCountActionNode (u4DiffServCountActId);

    if (pCountActEntry == NULL)
    {
        /*Entry Not found in the table */
        switch (i4TestValDiffServCountActStatus)
        {
            case QOS_CREATE_AND_WAIT:
                if (gQoSGlobalInfo.DSNextFree.u4CountActNextFree ==
                    QOS_INDEX_FULL)
                {
                    return SNMP_FAILURE;
                }
                break;
            default:
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return (SNMP_FAILURE);
        }
    }
    else
    {
        switch (i4TestValDiffServCountActStatus)
        {
                /*Entry Found in the table */
            case QOS_ACTIVE:
                if ((pCountActEntry->u1DSCountActStatus == QOS_NOT_READY))
                {
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                    return (SNMP_FAILURE);
                }
                break;
            case QOS_NOT_IN_SERVICE:
                break;

            case CREATE_AND_WAIT:
            case CREATE_AND_GO:
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return (SNMP_FAILURE);
            case DESTROY:
                break;
            default:
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                return (SNMP_FAILURE);
        }
    }
    return (SNMP_SUCCESS);
}

/* Low Level Dependency Routines for All Objects  */
/****************************************************************************
 Function    :  nmhDepv2DiffServCountActTable
 Input       :  The Indices
                DiffServCountActId
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2DiffServCountActTable (UINT4 *pu4ErrorCode,
                               tSnmpIndexList * pSnmpIndexList,
                               tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */
/****************************************************************************
 Function    :  nmhGetDiffServAlgDropNextFree
 Input       :  The Indices

                The Object 
                retValDiffServAlgDropNextFree
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDiffServAlgDropNextFree (UINT4 *pu4RetValDiffServAlgDropNextFree)
{
    if (gQoSGlobalInfo.eSysControl != QOS_SYS_CNTL_START)
    {
        QOS_TRC (MGMT_TRC, QOS_MODULE_SHUTDOW_ERROR_MSG);
        *pu4RetValDiffServAlgDropNextFree = 0;
        return (SNMP_SUCCESS);
    }
    *pu4RetValDiffServAlgDropNextFree = QOS_ALGDROP_NEXT_FREE ();
    return (SNMP_SUCCESS);
}

/* LOW LEVEL Routines for Table : DiffServAlgDropTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceDiffServAlgDropTable
 Input       :  The Indices
                DiffServAlgDropId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceDiffServAlgDropTable (UINT4 u4DiffServAlgDropId)
{
    if (gQoSGlobalInfo.eSysControl != QOS_SYS_CNTL_START)
    {
        QOS_TRC (MGMT_TRC, QOS_MODULE_SHUTDOW_ERROR_MSG);
        return (SNMP_FAILURE);
    }
    return (nmhValidateIndexInstanceFsQoSQTemplateTable (u4DiffServAlgDropId));
}

/****************************************************************************
 Function    :  nmhGetFirstIndexDiffServAlgDropTable
 Input       :  The Indices
                DiffServAlgDropId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexDiffServAlgDropTable (UINT4 *pu4DiffServAlgDropId)
{
    if (gQoSGlobalInfo.eSysControl != QOS_SYS_CNTL_START)
    {
        QOS_TRC (MGMT_TRC, QOS_MODULE_SHUTDOW_ERROR_MSG);
        return (SNMP_FAILURE);
    }
    return (nmhGetFirstIndexFsQoSQTemplateTable (pu4DiffServAlgDropId));
}

/****************************************************************************
 Function    :  nmhGetNextIndexDiffServAlgDropTable
 Input       :  The Indices
                DiffServAlgDropId
                nextDiffServAlgDropId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexDiffServAlgDropTable (UINT4 u4DiffServAlgDropId,
                                     UINT4 *pu4NextDiffServAlgDropId)
{
    if (gQoSGlobalInfo.eSysControl != QOS_SYS_CNTL_START)
    {
        QOS_TRC (MGMT_TRC, QOS_MODULE_SHUTDOW_ERROR_MSG);
        return (SNMP_FAILURE);
    }
    return (nmhGetNextIndexFsQoSQTemplateTable (u4DiffServAlgDropId,
                                                pu4NextDiffServAlgDropId));
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetDiffServAlgDropType
 Input       :  The Indices
                DiffServAlgDropId

                The Object 
                retValDiffServAlgDropType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDiffServAlgDropType (UINT4 u4DiffServAlgDropId,
                           INT4 *pi4RetValDiffServAlgDropType)
{
    if (gQoSGlobalInfo.eSysControl != QOS_SYS_CNTL_START)
    {
        QOS_TRC (MGMT_TRC, QOS_MODULE_SHUTDOW_ERROR_MSG);
        return (SNMP_FAILURE);
    }
    /*Standard supports other,tailDrop,headDrop,randomDrop and alwaysDrop */
    /*Prop supports tailDrop, headDrop, red, wred */
    return (nmhGetFsQoSQTemplateDropType (u4DiffServAlgDropId,
                                          pi4RetValDiffServAlgDropType));
}

/****************************************************************************
 Function    :  nmhGetDiffServAlgDropNext
 Input       :  The Indices
                DiffServAlgDropId

                The Object 
                retValDiffServAlgDropNext
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDiffServAlgDropNext (UINT4 u4DiffServAlgDropId,
                           tSNMP_OID_TYPE * pRetValDiffServAlgDropNext)
{
    tQoSQTemplateNode  *pQTempNode = NULL;
    UINT4               u4AlgDropNextId = QOS_ROWPOINTER_DEF;
    UINT4               u4AlgDropNextType = QOS_ROWPOINTER_DEF;

    if (gQoSGlobalInfo.eSysControl != QOS_SYS_CNTL_START)
    {
        QOS_TRC (MGMT_TRC, QOS_MODULE_SHUTDOW_ERROR_MSG);
        return (SNMP_FAILURE);
    }
    pQTempNode = QoSUtlGetQTempNode (u4DiffServAlgDropId);

    if (pQTempNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetQTempNode () "
                      "Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    u4AlgDropNextId = pQTempNode->u4DsAlgoDropNextId;
    u4AlgDropNextType = pQTempNode->u4DSAlgoDropNextType;

    if (QosValidateIndex (u4AlgDropNextId, u4AlgDropNextType) == QOS_FAILURE)
    {
        return (SNMP_FAILURE);
    }

    if (QosGetOidFromTypeId (u4AlgDropNextId, u4AlgDropNextType,
                             pRetValDiffServAlgDropNext) == QOS_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetDiffServAlgDropQMeasure
 Input       :  The Indices
                DiffServAlgDropId

                The Object 
                retValDiffServAlgDropQMeasure
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDiffServAlgDropQMeasure (UINT4 u4DiffServAlgDropId,
                               tSNMP_OID_TYPE * pRetValDiffServAlgDropQMeasure)
{
    tQoSQTemplateNode  *pQTempNode = NULL;
    UINT4               u4AlgDropQMeasureId = QOS_ROWPOINTER_DEF;
    UINT4               u4AlgDropQMeasureType = QOS_ROWPOINTER_DEF;

    if (gQoSGlobalInfo.eSysControl != QOS_SYS_CNTL_START)
    {
        QOS_TRC (MGMT_TRC, QOS_MODULE_SHUTDOW_ERROR_MSG);
        return (SNMP_FAILURE);
    }
    pQTempNode = QoSUtlGetQTempNode (u4DiffServAlgDropId);

    if (pQTempNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetQTempNode () "
                      "Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    u4AlgDropQMeasureId = pQTempNode->u4DSAlgDropQMeasureId;
    u4AlgDropQMeasureType = pQTempNode->u4DSAlgoDropQMeasureType;
    if (QosValidateIndex (u4AlgDropQMeasureId, u4AlgDropQMeasureType)
        == QOS_FAILURE)
    {
        return (SNMP_FAILURE);
    }

    if (QosGetOidFromTypeId (u4AlgDropQMeasureId, u4AlgDropQMeasureType,
                             pRetValDiffServAlgDropQMeasure) == QOS_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetDiffServAlgDropQThreshold
 Input       :  The Indices
                DiffServAlgDropId

                The Object 
                retValDiffServAlgDropQThreshold
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDiffServAlgDropQThreshold (UINT4 u4DiffServAlgDropId,
                                 UINT4 *pu4RetValDiffServAlgDropQThreshold)
{
    if (gQoSGlobalInfo.eSysControl != QOS_SYS_CNTL_START)
    {
        QOS_TRC (MGMT_TRC, QOS_MODULE_SHUTDOW_ERROR_MSG);
        return (SNMP_FAILURE);
    }
    return (nmhGetFsQoSQTemplateSize (u4DiffServAlgDropId,
                                      pu4RetValDiffServAlgDropQThreshold));
}

/****************************************************************************
 Function    :  nmhGetDiffServAlgDropSpecific
 Input       :  The Indices
                DiffServAlgDropId

                The Object 
                retValDiffServAlgDropSpecific
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDiffServAlgDropSpecific (UINT4 u4DiffServAlgDropId,
                               tSNMP_OID_TYPE * pRetValDiffServAlgDropSpecific)
{
    tQoSQTemplateNode  *pQTempNode = NULL;
    UINT4               u4AlgDropSpecificId = QOS_ROWPOINTER_DEF;
    UINT4               u4AlgDropSpecificType = QOS_ROWPOINTER_DEF;

    if (gQoSGlobalInfo.eSysControl != QOS_SYS_CNTL_START)
    {
        QOS_TRC (MGMT_TRC, QOS_MODULE_SHUTDOW_ERROR_MSG);
        return (SNMP_FAILURE);
    }
    pQTempNode = QoSUtlGetQTempNode (u4DiffServAlgDropId);

    if (pQTempNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetQTempNode () "
                      "Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    u4AlgDropSpecificId = pQTempNode->u4DSAlgDropSpecificId;
    u4AlgDropSpecificType = pQTempNode->u4DSAlgDropSpecificType;
    if (QosValidateIndex (u4AlgDropSpecificId, u4AlgDropSpecificType)
        == QOS_FAILURE)
    {
        return (SNMP_FAILURE);
    }

    if (QosGetOidFromTypeId (u4AlgDropSpecificId, u4AlgDropSpecificType,
                             pRetValDiffServAlgDropSpecific) == QOS_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetDiffServAlgDropOctets
 Input       :  The Indices
                DiffServAlgDropId

                The Object 
                retValDiffServAlgDropOctets
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDiffServAlgDropOctets (UINT4 u4DiffServAlgDropId,
                             tSNMP_COUNTER64_TYPE *
                             pu8RetValDiffServAlgDropOctets)
{
    INT4                i4RetStatus = SNMP_FAILURE;

    if (gQoSGlobalInfo.eSysControl != QOS_SYS_CNTL_START)
    {
        QOS_TRC (MGMT_TRC, QOS_MODULE_SHUTDOW_ERROR_MSG);
        return (SNMP_FAILURE);
    }
    i4RetStatus = QoSHwWrGetAlgDropOctets (u4DiffServAlgDropId,
                                           QOS_ALG_DROP_OCTETS,
                                           pu8RetValDiffServAlgDropOctets);
    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG2 (MGMT_TRC, "In %s : ERROR: QoSHwWrGetAlgDropOctets () "
                      " Returns FAILURE for a AlgDrop Id %d Stats type of "
                      " QOS_ALG_DROP_OCTETS .\r\n",
                      u4DiffServAlgDropId, __FUNCTION__);

        return (SNMP_FAILURE);
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDiffServAlgDropPkts
 Input       :  The Indices
                DiffServAlgDropId

                The Object 
                retValDiffServAlgDropPkts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDiffServAlgDropPkts (UINT4 u4DiffServAlgDropId,
                           tSNMP_COUNTER64_TYPE * pu8RetValDiffServAlgDropPkts)
{
    INT4                i4RetStatus = SNMP_FAILURE;

    if (gQoSGlobalInfo.eSysControl != QOS_SYS_CNTL_START)
    {
        QOS_TRC (MGMT_TRC, QOS_MODULE_SHUTDOW_ERROR_MSG);
        return (SNMP_FAILURE);
    }
    i4RetStatus = QoSHwWrGetAlgDropPkts (u4DiffServAlgDropId,
                                         QOS_ALG_DROP_PKTS,
                                         pu8RetValDiffServAlgDropPkts);
    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG2 (MGMT_TRC, "In %s : ERROR: QoSHwWrGetAlgDropPkts () "
                      " Returns FAILURE for a AlgDrop Id %d Stats type of "
                      " QOS_ALG_DROP_PKTS .\r\n",
                      u4DiffServAlgDropId, __FUNCTION__);

        return (SNMP_FAILURE);
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDiffServAlgRandomDropOctets
 Input       :  The Indices
                DiffServAlgDropId

                The Object 
                retValDiffServAlgRandomDropOctets
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDiffServAlgRandomDropOctets (UINT4 u4DiffServAlgDropId,
                                   tSNMP_COUNTER64_TYPE *
                                   pu8RetValDiffServAlgRandomDropOctets)
{
    INT4                i4RetStatus = SNMP_FAILURE;

    if (gQoSGlobalInfo.eSysControl != QOS_SYS_CNTL_START)
    {
        QOS_TRC (MGMT_TRC, QOS_MODULE_SHUTDOW_ERROR_MSG);
        return (SNMP_FAILURE);
    }
    i4RetStatus = QoSHwWrGetRandomDropOctets (u4DiffServAlgDropId,
                                              QOS_RANDOM_DROP_OCTETS,
                                              pu8RetValDiffServAlgRandomDropOctets);
    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG2 (MGMT_TRC, "In %s : ERROR: QoSHwWrGetRandomDropOctets () "
                      " Returns FAILURE for a AlgDrop Id %d Stats type of "
                      " QOS_ALG_DROP_PKTS .\r\n",
                      u4DiffServAlgDropId, __FUNCTION__);

        return (SNMP_FAILURE);
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDiffServAlgRandomDropPkts
 Input       :  The Indices
                DiffServAlgDropId

                The Object 
                retValDiffServAlgRandomDropPkts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDiffServAlgRandomDropPkts (UINT4 u4DiffServAlgDropId,
                                 tSNMP_COUNTER64_TYPE *
                                 pu8RetValDiffServAlgRandomDropPkts)
{
    INT4                i4RetStatus = SNMP_FAILURE;

    if (gQoSGlobalInfo.eSysControl != QOS_SYS_CNTL_START)
    {
        QOS_TRC (MGMT_TRC, QOS_MODULE_SHUTDOW_ERROR_MSG);
        return (SNMP_FAILURE);
    }
    i4RetStatus = QoSHwWrGetRandomDropPkts (u4DiffServAlgDropId,
                                            QOS_RANDOM_DROP_PKTS,
                                            pu8RetValDiffServAlgRandomDropPkts);
    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG2 (MGMT_TRC, "In %s : ERROR: QoSHwWrGetRandomDropPkts () "
                      " Returns FAILURE for a AlgDrop Id %d Stats type of "
                      " QOS_ALG_DROP_PKTS .\r\n",
                      u4DiffServAlgDropId, __FUNCTION__);

        return (SNMP_FAILURE);
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDiffServAlgDropStorage
 Input       :  The Indices
                DiffServAlgDropId

                The Object 
                retValDiffServAlgDropStorage
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDiffServAlgDropStorage (UINT4 u4DiffServAlgDropId,
                              INT4 *pi4RetValDiffServAlgDropStorage)
{
    tQoSQTemplateNode  *pQTempNode = NULL;

    if (gQoSGlobalInfo.eSysControl != QOS_SYS_CNTL_START)
    {
        QOS_TRC (MGMT_TRC, QOS_MODULE_SHUTDOW_ERROR_MSG);
        return (SNMP_FAILURE);
    }
    pQTempNode = QoSUtlGetQTempNode (u4DiffServAlgDropId);

    if (pQTempNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetQTempNode () "
                      "Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }
    *pi4RetValDiffServAlgDropStorage = pQTempNode->i4StorageType;
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetDiffServAlgDropStatus
 Input       :  The Indices
                DiffServAlgDropId

                The Object 
                retValDiffServAlgDropStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDiffServAlgDropStatus (UINT4 u4DiffServAlgDropId,
                             INT4 *pi4RetValDiffServAlgDropStatus)
{
    tQoSQTemplateNode  *pQTempNode = NULL;

    if (gQoSGlobalInfo.eSysControl != QOS_SYS_CNTL_START)
    {
        QOS_TRC (MGMT_TRC, QOS_MODULE_SHUTDOW_ERROR_MSG);
        return (SNMP_FAILURE);
    }
    pQTempNode = QoSUtlGetQTempNode (u4DiffServAlgDropId);

    if (pQTempNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetQTempNode () "
                      "Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }
    *pi4RetValDiffServAlgDropStatus = pQTempNode->u1Status;
    return (SNMP_SUCCESS);
}

/* Low Level SET Routine for All Objects  */
/****************************************************************************
 Function    :  nmhSetDiffServAlgDropType
 Input       :  The Indices
                DiffServAlgDropId

                The Object 
                setValDiffServAlgDropType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDiffServAlgDropType (UINT4 u4DiffServAlgDropId,
                           INT4 i4SetValDiffServAlgDropType)
{
    tQoSQTemplateNode  *pQTempNode = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;

    pQTempNode = QoSUtlGetQTempNode (u4DiffServAlgDropId);

    if (pQTempNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetQTempNode () "
                      "Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }
    pQTempNode->u2DropType = (UINT2) i4SetValDiffServAlgDropType;
    SnmpNotifyInfo.pu4ObjectId = DiffServAlgDropType;
    SnmpNotifyInfo.u4OidLen = sizeof (DiffServAlgDropType) / sizeof (UINT4);
    SnmpNotifyInfo.u4SeqNum = 0;
    SnmpNotifyInfo.u1RowStatus = FALSE;
    SnmpNotifyInfo.pLockPointer = QoSLock;
    SnmpNotifyInfo.pUnLockPointer = QoSUnLock;
    SnmpNotifyInfo.u4Indices = 1;
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%u %i",
                      u4DiffServAlgDropId, i4SetValDiffServAlgDropType));
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhSetDiffServAlgDropNext
 Input       :  The Indices
                DiffServAlgDropId

                The Object 
                setValDiffServAlgDropNext
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDiffServAlgDropNext (UINT4 u4DiffServAlgDropId,
                           tSNMP_OID_TYPE * pSetValDiffServAlgDropNext)
{
    tQoSQTemplateNode  *pQTempNode = NULL;
    UINT4               u4AlgDropNextId = QOS_ROWPOINTER_DEF;
    UINT4               u4AlgDropNextType = QOS_ROWPOINTER_DEF;
    tSnmpNotifyInfo     SnmpNotifyInfo;

    pQTempNode = QoSUtlGetQTempNode (u4DiffServAlgDropId);
    if (pQTempNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetActionNode () "
                      " Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }
    if (QosGetTypeIdFromOid (pSetValDiffServAlgDropNext, &u4AlgDropNextType,
                             &u4AlgDropNextId) == QOS_SUCCESS)
    {
        pQTempNode->u4DsAlgoDropNextId = u4AlgDropNextId;
        pQTempNode->u4DSAlgoDropNextType = u4AlgDropNextType;
        SnmpNotifyInfo.pu4ObjectId = DiffServAlgDropNext;
        SnmpNotifyInfo.u4OidLen = sizeof (DiffServAlgDropNext) / sizeof (UINT4);
        SnmpNotifyInfo.u4SeqNum = 0;
        SnmpNotifyInfo.u1RowStatus = FALSE;
        SnmpNotifyInfo.pLockPointer = QoSLock;
        SnmpNotifyInfo.pUnLockPointer = QoSUnLock;
        SnmpNotifyInfo.u4Indices = 1;
        SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;

        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%u %o",
                          u4DiffServAlgDropId, pSetValDiffServAlgDropNext));
        return SNMP_SUCCESS;
    }
    return (SNMP_FAILURE);
}

/****************************************************************************
 Function    :  nmhSetDiffServAlgDropQMeasure
 Input       :  The Indices
                DiffServAlgDropId

                The Object 
                setValDiffServAlgDropQMeasure
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDiffServAlgDropQMeasure (UINT4 u4DiffServAlgDropId,
                               tSNMP_OID_TYPE * pSetValDiffServAlgDropQMeasure)
{
    tQoSQTemplateNode  *pQTempNode = NULL;
    UINT4               u4AlgDropQMeasureId = QOS_ROWPOINTER_DEF;
    UINT4               u4AlgDropQMeasureType = QOS_ROWPOINTER_DEF;
    tSnmpNotifyInfo     SnmpNotifyInfo;

    pQTempNode = QoSUtlGetQTempNode (u4DiffServAlgDropId);
    if (pQTempNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetActionNode () "
                      " Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }
    if (QosGetTypeIdFromOid
        (pSetValDiffServAlgDropQMeasure, &u4AlgDropQMeasureType,
         &u4AlgDropQMeasureId) == QOS_SUCCESS)
    {
        pQTempNode->u4DSAlgDropQMeasureId = u4AlgDropQMeasureId;
        pQTempNode->u4DSAlgoDropQMeasureType = u4AlgDropQMeasureType;
        SnmpNotifyInfo.pu4ObjectId = DiffServAlgDropQMeasure;
        SnmpNotifyInfo.u4OidLen =
            sizeof (DiffServAlgDropQMeasure) / sizeof (UINT4);
        SnmpNotifyInfo.u4SeqNum = 0;
        SnmpNotifyInfo.u1RowStatus = FALSE;
        SnmpNotifyInfo.pLockPointer = QoSLock;
        SnmpNotifyInfo.pUnLockPointer = QoSUnLock;
        SnmpNotifyInfo.u4Indices = 1;
        SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;

        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%u %o",
                          u4DiffServAlgDropId, pSetValDiffServAlgDropQMeasure));
        return SNMP_SUCCESS;
    }
    return (SNMP_FAILURE);
}

/****************************************************************************
 Function    :  nmhSetDiffServAlgDropQThreshold
 Input       :  The Indices
                DiffServAlgDropId

                The Object 
                setValDiffServAlgDropQThreshold
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDiffServAlgDropQThreshold (UINT4 u4DiffServAlgDropId,
                                 UINT4 u4SetValDiffServAlgDropQThreshold)
{
    INT4                i4RetStatus = SNMP_FAILURE;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    i4RetStatus = nmhSetFsQoSQTemplateSize (u4DiffServAlgDropId,
                                            u4SetValDiffServAlgDropQThreshold);
    if (i4RetStatus == SNMP_SUCCESS)
    {
        SnmpNotifyInfo.pu4ObjectId = DiffServAlgDropQThreshold;
        SnmpNotifyInfo.u4OidLen =
            sizeof (DiffServAlgDropQThreshold) / sizeof (UINT4);
        SnmpNotifyInfo.u4SeqNum = 0;
        SnmpNotifyInfo.u1RowStatus = FALSE;
        SnmpNotifyInfo.pLockPointer = QoSLock;
        SnmpNotifyInfo.pUnLockPointer = QoSUnLock;
        SnmpNotifyInfo.u4Indices = 1;
        SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;

        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%u %u",
                          u4DiffServAlgDropId,
                          u4SetValDiffServAlgDropQThreshold));
        return SNMP_SUCCESS;
    }
    else
    {
        return SNMP_FAILURE;
    }
}

/****************************************************************************
 Function    :  nmhSetDiffServAlgDropSpecific
 Input       :  The Indices
                DiffServAlgDropId

                The Object 
                setValDiffServAlgDropSpecific
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDiffServAlgDropSpecific (UINT4 u4DiffServAlgDropId,
                               tSNMP_OID_TYPE * pSetValDiffServAlgDropSpecific)
{
    tQoSQTemplateNode  *pQTempNode = NULL;
    UINT4               u4AlgDropSpecificId = QOS_ROWPOINTER_DEF;
    UINT4               u4AlgDropSpecificType = QOS_ROWPOINTER_DEF;
    tSnmpNotifyInfo     SnmpNotifyInfo;

    pQTempNode = QoSUtlGetQTempNode (u4DiffServAlgDropId);
    if (pQTempNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetActionNode () "
                      " Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }
    if (QosGetTypeIdFromOid (pSetValDiffServAlgDropSpecific,
                             &u4AlgDropSpecificType,
                             &u4AlgDropSpecificId) == QOS_SUCCESS)
    {
        pQTempNode->u4DSAlgDropSpecificId = u4AlgDropSpecificId;
        pQTempNode->u4DSAlgDropSpecificType = u4AlgDropSpecificType;
        SnmpNotifyInfo.pu4ObjectId = DiffServAlgDropSpecific;
        SnmpNotifyInfo.u4OidLen =
            sizeof (DiffServAlgDropSpecific) / sizeof (UINT4);
        SnmpNotifyInfo.u4SeqNum = 0;
        SnmpNotifyInfo.u1RowStatus = FALSE;
        SnmpNotifyInfo.pLockPointer = QoSLock;
        SnmpNotifyInfo.pUnLockPointer = QoSUnLock;
        SnmpNotifyInfo.u4Indices = 1;
        SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;

        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%u %o",
                          u4DiffServAlgDropId, pSetValDiffServAlgDropSpecific));
        return SNMP_SUCCESS;
    }
    return (SNMP_FAILURE);
}

/****************************************************************************
 Function    :  nmhSetDiffServAlgDropStorage
 Input       :  The Indices
                DiffServAlgDropId

                The Object 
                setValDiffServAlgDropStorage
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDiffServAlgDropStorage (UINT4 u4DiffServAlgDropId,
                              INT4 i4SetValDiffServAlgDropStorage)
{
    tQoSQTemplateNode  *pQTempNode = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;

    pQTempNode = QoSUtlGetQTempNode (u4DiffServAlgDropId);
    if (pQTempNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetActionNode () "
                      " Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }
    pQTempNode->i4StorageType = i4SetValDiffServAlgDropStorage;
    SnmpNotifyInfo.pu4ObjectId = DiffServAlgDropStorage;
    SnmpNotifyInfo.u4OidLen = sizeof (DiffServAlgDropStorage) / sizeof (UINT4);
    SnmpNotifyInfo.u4SeqNum = 0;
    SnmpNotifyInfo.u1RowStatus = FALSE;
    SnmpNotifyInfo.pLockPointer = QoSLock;
    SnmpNotifyInfo.pUnLockPointer = QoSUnLock;
    SnmpNotifyInfo.u4Indices = 1;
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%u %i",
                      u4DiffServAlgDropId, i4SetValDiffServAlgDropStorage));
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhSetDiffServAlgDropStatus
 Input       :  The Indices
                DiffServAlgDropId

                The Object 
                setValDiffServAlgDropStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDiffServAlgDropStatus (UINT4 u4DiffServAlgDropId,
                             INT4 i4SetValDiffServAlgDropStatus)
{
    tQoSQTemplateNode  *pQTempNode = NULL;
    UINT4               u4DsAlgoDropNextId;
    UINT4               u4DSAlgoDropNextType;
    UINT4               u4DSAlgDropQMeasureId;
    UINT4               u4DSAlgoDropQMeasureType;
    UINT4               u4DSAlgDropSpecificId;
    UINT4               u4DSAlgDropSpecificType;
    UINT4               u4CurrentType = DS_ALG_DROP;
    UINT4               u4DummyId = 0;
    if (i4SetValDiffServAlgDropStatus == QOS_CREATE_AND_WAIT)
    {
        pQTempNode = QoSUtlGetQTempNode (u4DiffServAlgDropId);
        if (pQTempNode != NULL)
        {
            return SNMP_FAILURE;
        }
    }
    if (i4SetValDiffServAlgDropStatus == QOS_DESTROY)
    {

        pQTempNode = QoSUtlGetQTempNode (u4DiffServAlgDropId);
        if (pQTempNode == NULL)
        {
            return SNMP_FAILURE;
        }
        u4DsAlgoDropNextId = pQTempNode->u4DsAlgoDropNextId;
        u4DSAlgoDropNextType = pQTempNode->u4DSAlgoDropNextType;
        u4DSAlgDropQMeasureId = pQTempNode->u4DSAlgDropQMeasureId;
        u4DSAlgoDropQMeasureType = pQTempNode->u4DSAlgoDropQMeasureType;
        u4DSAlgDropSpecificId = pQTempNode->u4DSAlgDropSpecificId;
        u4DSAlgDropSpecificType = pQTempNode->u4DSAlgDropSpecificType;

        QosUtlPointedToStatus (&u4CurrentType, &u4DiffServAlgDropId, &u4DummyId,
                               u4DSAlgoDropNextType, u4DsAlgoDropNextId, 0,
                               QOS_DEL);
        QosUtlPointedToStatus (&u4CurrentType, &u4DiffServAlgDropId, &u4DummyId,
                               u4DSAlgoDropQMeasureType,
                               u4DSAlgDropQMeasureId, 0, QOS_DEL);
        QosUtlPointedToStatus (&u4CurrentType, &u4DiffServAlgDropId, &u4DummyId,
                               u4DSAlgDropSpecificType,
                               u4DSAlgDropSpecificId, 0, QOS_DEL);
    }
    return (nmhSetFsQoSQTemplateStatus (u4DiffServAlgDropId,
                                        i4SetValDiffServAlgDropStatus));
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2DiffServAlgDropType
 Input       :  The Indices
                DiffServAlgDropId

                The Object 
                testValDiffServAlgDropType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2DiffServAlgDropType (UINT4 *pu4ErrorCode, UINT4 u4DiffServAlgDropId,
                              INT4 i4TestValDiffServAlgDropType)
{
    if (QosUtlValidateAlgDropEntry (u4DiffServAlgDropId, pu4ErrorCode)
        == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QosUtlValidateAlgDropEntry () "
                      "Returns FAILURE. \r\n", __FUNCTION__);
        return SNMP_FAILURE;
    }
    if ((i4TestValDiffServAlgDropType != QOS_Q_TEMP_DROP_TYPE_OTHER) &&
        (i4TestValDiffServAlgDropType != QOS_Q_TEMP_DROP_TYPE_TAIL) &&
        (i4TestValDiffServAlgDropType != QOS_Q_TEMP_DROP_TYPE_HEAD) &&
        (i4TestValDiffServAlgDropType != QOS_Q_TEMP_DROP_TYPE_RED) &&
        (i4TestValDiffServAlgDropType != QOS_Q_TEMP_DROP_TYPE_WRED) &&
        (i4TestValDiffServAlgDropType != QOS_Q_TEMP_DROP_TYPE_ALWAYS))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2DiffServAlgDropNext
 Input       :  The Indices
                DiffServAlgDropId

                The Object 
                testValDiffServAlgDropNext
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2DiffServAlgDropNext (UINT4 *pu4ErrorCode, UINT4 u4DiffServAlgDropId,
                              tSNMP_OID_TYPE * pTestValDiffServAlgDropNext)
{
    UINT4               u4AlgDropNextId = QOS_ROWPOINTER_DEF;
    UINT4               u4AlgDropNextType = QOS_ROWPOINTER_DEF;

    if (QosUtlValidateAlgDropEntry (u4DiffServAlgDropId, pu4ErrorCode)
        == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QosUtlValidateAlgDropEntry () "
                      "Returns FAILURE. \r\n", __FUNCTION__);
        return SNMP_FAILURE;
    }
    if (QosGetTypeIdFromOid (pTestValDiffServAlgDropNext, &u4AlgDropNextType,
                             &u4AlgDropNextId) == QOS_SUCCESS)
    {
#ifdef NPAPI_WANTED
        /*1st param : Currentype, 2nd param: 1stNextType
         * 3rd param: 2ndNextType if any, 4thParam: 3rdNexttype
         * if any , 5thParm : Specific type if any*/
        if (QosIsTypeValid (DS_ALG_DROP, u4AlgDropNextType,
                            QOS_ROWPOINTER_DEF, QOS_ROWPOINTER_DEF,
                            QOS_ROWPOINTER_DEF) != QOS_SUCCESS)
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return (SNMP_FAILURE);
        }
#endif
        if (QosValidateIndex (u4AlgDropNextId, u4AlgDropNextType)
            == QOS_FAILURE)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return (SNMP_FAILURE);
        }
        return SNMP_SUCCESS;
    }
    return (SNMP_FAILURE);
}

/****************************************************************************
 Function    :  nmhTestv2DiffServAlgDropQMeasure
 Input       :  The Indices
                DiffServAlgDropId

                The Object 
                testValDiffServAlgDropQMeasure
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2DiffServAlgDropQMeasure (UINT4 *pu4ErrorCode,
                                  UINT4 u4DiffServAlgDropId,
                                  tSNMP_OID_TYPE *
                                  pTestValDiffServAlgDropQMeasure)
{
    UINT4               u4AlgDropQMeasureId = QOS_ROWPOINTER_DEF;
    UINT4               u4AlgDropQMeasureType = QOS_ROWPOINTER_DEF;

    if (QosUtlValidateAlgDropEntry (u4DiffServAlgDropId, pu4ErrorCode)
        == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QosUtlValidateAlgDropEntry () "
                      "Returns FAILURE. \r\n", __FUNCTION__);
        return SNMP_FAILURE;
    }
    if (QosGetTypeIdFromOid (pTestValDiffServAlgDropQMeasure,
                             &u4AlgDropQMeasureType,
                             &u4AlgDropQMeasureId) == QOS_SUCCESS)
    {
#ifdef NPAPI_WANTED
        /*1st param : Currentype, 2nd param: 1stNextType
         * 3rd param: 2ndNextType if any, 4thParam: 3rdNexttype
         * if any , 5thParm : Specific type if any*/
        if (QosIsTypeValid (DS_ALG_DROP, DS_QUEUE, u4AlgDropQMeasureType,
                            DS_RANDOM_DROP, QOS_ROWPOINTER_DEF) != QOS_SUCCESS)
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return (SNMP_FAILURE);
        }
#endif
        if (QosValidateIndex (u4AlgDropQMeasureId, u4AlgDropQMeasureType)
            == QOS_FAILURE)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return (SNMP_FAILURE);
        }
        return SNMP_SUCCESS;
    }
    return (SNMP_FAILURE);
}

/****************************************************************************
 Function    :  nmhTestv2DiffServAlgDropQThreshold
 Input       :  The Indices
                DiffServAlgDropId

                The Object 
                testValDiffServAlgDropQThreshold
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2DiffServAlgDropQThreshold (UINT4 *pu4ErrorCode,
                                    UINT4 u4DiffServAlgDropId,
                                    UINT4 u4TestValDiffServAlgDropQThreshold)
{
    if (QosUtlValidateAlgDropEntry (u4DiffServAlgDropId, pu4ErrorCode)
        == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QosUtlValidateAlgDropEntry () "
                      "Returns FAILURE. \r\n", __FUNCTION__);
        return SNMP_FAILURE;
    }
    return (nmhTestv2FsQoSQTemplateSize (pu4ErrorCode,
                                         u4DiffServAlgDropId,
                                         u4TestValDiffServAlgDropQThreshold));
}

/****************************************************************************
 Function    :  nmhTestv2DiffServAlgDropSpecific
 Input       :  The Indices
                DiffServAlgDropId

                The Object 
                testValDiffServAlgDropSpecific
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2DiffServAlgDropSpecific (UINT4 *pu4ErrorCode,
                                  UINT4 u4DiffServAlgDropId,
                                  tSNMP_OID_TYPE *
                                  pTestValDiffServAlgDropSpecific)
{
    UINT4               u4AlgDropSpecificId = QOS_ROWPOINTER_DEF;
    UINT4               u4AlgDropSpecificType = QOS_ROWPOINTER_DEF;

    tQoSQTemplateNode  *pQTempNode = NULL;

    pQTempNode = QoSUtlGetQTempNode (u4DiffServAlgDropId);
    if (pQTempNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetQTempNode () "
                      " Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }
    if (QosUtlValidateAlgDropEntry (u4DiffServAlgDropId, pu4ErrorCode)
        == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QosUtlValidateAlgDropEntry () "
                      "Returns FAILURE. \r\n", __FUNCTION__);
        return SNMP_FAILURE;
    }
    if (QosGetTypeIdFromOid (pTestValDiffServAlgDropSpecific,
                             &u4AlgDropSpecificType,
                             &u4AlgDropSpecificId) == QOS_SUCCESS)
    {
        if (QosValidateType (pQTempNode->u2DropType, u4AlgDropSpecificType)
            == QOS_FAILURE)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return (SNMP_FAILURE);
        }
        if (QosValidateIndex (u4AlgDropSpecificId, u4AlgDropSpecificType)
            == QOS_FAILURE)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return (SNMP_FAILURE);
        }
        return SNMP_SUCCESS;
    }
    return (SNMP_FAILURE);
}

/****************************************************************************
 Function    :  nmhTestv2DiffServAlgDropStorage
 Input       :  The Indices
                DiffServAlgDropId

                The Object 
                testValDiffServAlgDropStorage
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2DiffServAlgDropStorage (UINT4 *pu4ErrorCode, UINT4 u4DiffServAlgDropId,
                                 INT4 i4TestValDiffServAlgDropStorage)
{

    if (QosUtlValidateAlgDropEntry (u4DiffServAlgDropId, pu4ErrorCode)
        == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QosUtlValidateAlgDropEntry () "
                      "Returns FAILURE. \r\n", __FUNCTION__);
        return SNMP_FAILURE;
    }
    if ((i4TestValDiffServAlgDropStorage != QOS_STORAGE_VOLATILE) &&
        (i4TestValDiffServAlgDropStorage != QOS_STORAGE_NONVOLATILE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2DiffServAlgDropStatus
 Input       :  The Indices
                DiffServAlgDropId

                The Object 
                testValDiffServAlgDropStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2DiffServAlgDropStatus (UINT4 *pu4ErrorCode, UINT4 u4DiffServAlgDropId,
                                INT4 i4TestValDiffServAlgDropStatus)
{
    UINT4               u4CurrentType = DS_ALG_DROP;
    UINT4               u4DummyId = 0;

    if (gQoSGlobalInfo.eSysControl != QOS_SYS_CNTL_START)
    {
        QOS_TRC (MGMT_TRC, QOS_MODULE_SHUTDOW_ERROR_MSG);
        return (SNMP_FAILURE);
    }

    if ((i4TestValDiffServAlgDropStatus == QOS_DESTROY) &&
        (QosUtlPointedToStatus
         (&u4CurrentType, &u4DiffServAlgDropId, &u4DummyId, DS_ALG_DROP,
          u4DiffServAlgDropId, 0, QOS_CHECK) != QOS_SUCCESS))
    {
        return SNMP_FAILURE;
    }
    if ((i4TestValDiffServAlgDropStatus == QOS_CREATE_AND_WAIT) &&
        (QOS_ALGDROP_COUNT >= QOS_Q_TEMP_TBL_MAX_ENTRIES))
    {
        return (SNMP_FAILURE);
    }

    return (nmhTestv2FsQoSQTemplateStatus (pu4ErrorCode, u4DiffServAlgDropId,
                                           i4TestValDiffServAlgDropStatus));
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2DiffServAlgDropTable
 Input       :  The Indices
                DiffServAlgDropId
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2DiffServAlgDropTable (UINT4 *pu4ErrorCode,
                              tSnmpIndexList * pSnmpIndexList,
                              tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetDiffServRandomDropNextFree
 Input       :  The Indices

                The Object 
                retValDiffServRandomDropNextFree
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDiffServRandomDropNextFree (UINT4 *pu4RetValDiffServRandomDropNextFree)
{
    if (gQoSGlobalInfo.eSysControl != QOS_SYS_CNTL_START)
    {
        QOS_TRC (MGMT_TRC, QOS_MODULE_SHUTDOW_ERROR_MSG);
        *pu4RetValDiffServRandomDropNextFree = 0;
        return (SNMP_SUCCESS);
    }
    *pu4RetValDiffServRandomDropNextFree = QOS_RD_NEXT_FREE ();
    return (SNMP_SUCCESS);
}

/* LOW LEVEL Routines for Table : DiffServRandomDropTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceDiffServRandomDropTable
 Input       :  The Indices
                DiffServRandomDropId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceDiffServRandomDropTable (UINT4 u4DiffServRandomDropId)
{
    /* u4DiffServRandomDropId is same as u4FsQoSQTemplateId
     * 0 is passed since there is no support for drop precedence
     * standard MIB. So 0 is passed to indicate low-drop precedence
     * as specified in proprietary MIB.*/
    if (gQoSGlobalInfo.eSysControl != QOS_SYS_CNTL_START)
    {
        QOS_TRC (MGMT_TRC, QOS_MODULE_SHUTDOW_ERROR_MSG);
        return (SNMP_FAILURE);
    }
    return (nmhValidateIndexInstanceFsQoSRandomDetectCfgTable
            (u4DiffServRandomDropId, QOS_START_INITIAL_INDEX));
}

/****************************************************************************
Function    :  nmhGetFirstIndexDiffServRandomDropTable
 Input       :  The Indices
                DiffServRandomDropId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexDiffServRandomDropTable (UINT4 *pu4DiffServRandomDropId)
{
    INT4                i4FsQoSRandomDetectCfgDP = 0;
    INT4                i4NextFsQoSRandomDetectCfgDP = 0;
    UINT4 		u4NextDiffServRandomDropId =0;
    if (gQoSGlobalInfo.eSysControl != QOS_SYS_CNTL_START)
    {
        QOS_TRC (MGMT_TRC, QOS_MODULE_SHUTDOW_ERROR_MSG);
        return (SNMP_FAILURE);
    }

    if (nmhGetFirstIndexFsQoSRandomDetectCfgTable (pu4DiffServRandomDropId,
                                                   &i4FsQoSRandomDetectCfgDP) ==
        SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    while (i4FsQoSRandomDetectCfgDP != QOS_START_INITIAL_INDEX)
    {
    	if (nmhGetNextIndexFsQoSRandomDetectCfgTable (*pu4DiffServRandomDropId,
        	                                          &u4NextDiffServRandomDropId,
                	                                  i4FsQoSRandomDetectCfgDP,
                        	                          &i4NextFsQoSRandomDetectCfgDP)
	        == SNMP_FAILURE)
    	{
        	return SNMP_FAILURE;
    	}
	*pu4DiffServRandomDropId = u4NextDiffServRandomDropId;
	i4FsQoSRandomDetectCfgDP = i4NextFsQoSRandomDetectCfgDP;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexDiffServRandomDropTable
 Input       :  The Indices
                DiffServRandomDropId
                nextDiffServRandomDropId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexDiffServRandomDropTable (UINT4 u4DiffServRandomDropId,
                                        UINT4 *pu4NextDiffServRandomDropId)
{
    INT4                i4NextFsQoSRandomDetectCfgDP = 0;
    INT4		i4FsQoSRandomDetectCfgDP = 0;
    if (gQoSGlobalInfo.eSysControl != QOS_SYS_CNTL_START)
    {
        QOS_TRC (MGMT_TRC, QOS_MODULE_SHUTDOW_ERROR_MSG);
        return (SNMP_FAILURE);
    }
    if (nmhGetNextIndexFsQoSRandomDetectCfgTable (u4DiffServRandomDropId,
                                                  pu4NextDiffServRandomDropId,
                                                  QOS_START_INITIAL_INDEX,
                                                  &i4NextFsQoSRandomDetectCfgDP)
        == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    u4DiffServRandomDropId = *pu4NextDiffServRandomDropId;
    i4FsQoSRandomDetectCfgDP = i4NextFsQoSRandomDetectCfgDP;
    do 
    {
    	if (nmhGetNextIndexFsQoSRandomDetectCfgTable (u4DiffServRandomDropId,
        	                                          pu4NextDiffServRandomDropId,
                	                                  i4FsQoSRandomDetectCfgDP,
                        	                          &i4NextFsQoSRandomDetectCfgDP)
	        == SNMP_FAILURE)
    	{
        	return SNMP_FAILURE;
    	}
    	u4DiffServRandomDropId = *pu4NextDiffServRandomDropId;
    	i4FsQoSRandomDetectCfgDP = i4NextFsQoSRandomDetectCfgDP;
	
    } while (i4NextFsQoSRandomDetectCfgDP != QOS_START_INITIAL_INDEX);
	
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetDiffServRandomDropMinThreshBytes
 Input       :  The Indices
                DiffServRandomDropId

                The Object 
                retValDiffServRandomDropMinThreshBytes
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDiffServRandomDropMinThreshBytes (UINT4 u4DiffServRandomDropId,
                                        UINT4
                                        *pu4RetValDiffServRandomDropMinThreshBytes)
{
    if (gQoSGlobalInfo.eSysControl != QOS_SYS_CNTL_START)
    {
        QOS_TRC (MGMT_TRC, QOS_MODULE_SHUTDOW_ERROR_MSG);
        return (SNMP_FAILURE);
    }
    if (nmhGetFsQoSRandomDetectCfgMinAvgThresh (u4DiffServRandomDropId,
                                                QOS_START_INITIAL_INDEX,
                                                pu4RetValDiffServRandomDropMinThreshBytes)
        == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDiffServRandomDropMinThreshPkts
 Input       :  The Indices
                DiffServRandomDropId

                The Object 
                retValDiffServRandomDropMinThreshPkts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDiffServRandomDropMinThreshPkts (UINT4 u4DiffServRandomDropId,
                                       UINT4
                                       *pu4RetValDiffServRandomDropMinThreshPkts)
{
    if (gQoSGlobalInfo.eSysControl != QOS_SYS_CNTL_START)
    {
        QOS_TRC (MGMT_TRC, QOS_MODULE_SHUTDOW_ERROR_MSG);
        return (SNMP_FAILURE);
    }
    UNUSED_PARAM (u4DiffServRandomDropId);
    UNUSED_PARAM (pu4RetValDiffServRandomDropMinThreshPkts);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDiffServRandomDropMaxThreshBytes
 Input       :  The Indices
                DiffServRandomDropId

                The Object 
                retValDiffServRandomDropMaxThreshBytes
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDiffServRandomDropMaxThreshBytes (UINT4 u4DiffServRandomDropId,
                                        UINT4
                                        *pu4RetValDiffServRandomDropMaxThreshBytes)
{
    UINT4               u4MaxThreshBytes = QOS_START_INITIAL_INDEX;
    if (gQoSGlobalInfo.eSysControl != QOS_SYS_CNTL_START)
    {
        QOS_TRC (MGMT_TRC, QOS_MODULE_SHUTDOW_ERROR_MSG);
        return (SNMP_FAILURE);
    }
    if (nmhGetFsQoSRandomDetectCfgMaxAvgThresh (u4DiffServRandomDropId,
                                                QOS_START_INITIAL_INDEX,
                                                pu4RetValDiffServRandomDropMaxThreshBytes)
        == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    /*FsQoSRandomDetectCfgMaxAvgThresh returns the value in bits 
     * so multipying by 8*/
    u4MaxThreshBytes =
        *pu4RetValDiffServRandomDropMaxThreshBytes * BITS_PER_BYTE;
    *pu4RetValDiffServRandomDropMaxThreshBytes = u4MaxThreshBytes;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDiffServRandomDropMaxThreshPkts
 Input       :  The Indices
                DiffServRandomDropId

                The Object 
                retValDiffServRandomDropMaxThreshPkts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDiffServRandomDropMaxThreshPkts (UINT4 u4DiffServRandomDropId,
                                       UINT4
                                       *pu4RetValDiffServRandomDropMaxThreshPkts)
{
    if (gQoSGlobalInfo.eSysControl != QOS_SYS_CNTL_START)
    {
        QOS_TRC (MGMT_TRC, QOS_MODULE_SHUTDOW_ERROR_MSG);
        return (SNMP_FAILURE);
    }
    UNUSED_PARAM (u4DiffServRandomDropId);
    UNUSED_PARAM (pu4RetValDiffServRandomDropMaxThreshPkts);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDiffServRandomDropProbMax
 Input       :  The Indices
                DiffServRandomDropId

                The Object 
                retValDiffServRandomDropProbMax
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDiffServRandomDropProbMax (UINT4 u4DiffServRandomDropId,
                                 UINT4 *pu4RetValDiffServRandomDropProbMax)
{
    if (gQoSGlobalInfo.eSysControl != QOS_SYS_CNTL_START)
    {
        QOS_TRC (MGMT_TRC, QOS_MODULE_SHUTDOW_ERROR_MSG);
        return (SNMP_FAILURE);
    }
    if (nmhGetFsQoSRandomDetectCfgMaxProb (u4DiffServRandomDropId,
                                           QOS_START_INITIAL_INDEX,
                                           pu4RetValDiffServRandomDropProbMax)
        == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDiffServRandomDropWeight
 Input       :  The Indices
                DiffServRandomDropId

                The Object 
                retValDiffServRandomDropWeight
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDiffServRandomDropWeight (UINT4 u4DiffServRandomDropId,
                                UINT4 *pu4RetValDiffServRandomDropWeight)
{
    if (gQoSGlobalInfo.eSysControl != QOS_SYS_CNTL_START)
    {
        QOS_TRC (MGMT_TRC, QOS_MODULE_SHUTDOW_ERROR_MSG);
        return (SNMP_FAILURE);
    }
    if (nmhGetFsQoSRandomDetectCfgExpWeight (u4DiffServRandomDropId,
                                             QOS_START_INITIAL_INDEX,
                                             pu4RetValDiffServRandomDropWeight)
        == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDiffServRandomDropSamplingRate
 Input       :  The Indices
                DiffServRandomDropId

                The Object 
                retValDiffServRandomDropSamplingRate
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDiffServRandomDropSamplingRate (UINT4 u4DiffServRandomDropId,
                                      UINT4
                                      *pu4RetValDiffServRandomDropSamplingRate)
{
    tQoSRDCfgNode      *pRDCfgNode = NULL;
    if (gQoSGlobalInfo.eSysControl != QOS_SYS_CNTL_START)
    {
        QOS_TRC (MGMT_TRC, QOS_MODULE_SHUTDOW_ERROR_MSG);
        return (SNMP_FAILURE);
    }
    pRDCfgNode = QoSUtlGetRDCfgNode (u4DiffServRandomDropId,
                                     QOS_START_INITIAL_INDEX);
    if (pRDCfgNode == NULL)
    {
        return SNMP_FAILURE;
    }
    *pu4RetValDiffServRandomDropSamplingRate =
        pRDCfgNode->u4DSRandomDropSamplingRate;
    /*Currently no proprietary object to get the sampling rate seems
       h/w support exists bcm_port_sampling_rate_get. So hardware call need to be
       coded when NPAPI coding is done. */
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDiffServRandomDropStorage
 Input       :  The Indices
                DiffServRandomDropId

                The Object 
                retValDiffServRandomDropStorage
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDiffServRandomDropStorage (UINT4 u4DiffServRandomDropId,
                                 INT4 *pi4RetValDiffServRandomDropStorage)
{
    tQoSRDCfgNode      *pRDCfgNode = NULL;

    if (gQoSGlobalInfo.eSysControl != QOS_SYS_CNTL_START)
    {
        QOS_TRC (MGMT_TRC, QOS_MODULE_SHUTDOW_ERROR_MSG);
        return (SNMP_FAILURE);
    }
    pRDCfgNode = QoSUtlGetRDCfgNode (u4DiffServRandomDropId,
                                     QOS_START_INITIAL_INDEX);
    if (pRDCfgNode == NULL)
    {
        return SNMP_FAILURE;
    }
    *pi4RetValDiffServRandomDropStorage = pRDCfgNode->i4StorageType;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDiffServRandomDropStatus
 Input       :  The Indices
                DiffServRandomDropId

                The Object 
                retValDiffServRandomDropStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDiffServRandomDropStatus (UINT4 u4DiffServRandomDropId,
                                INT4 *pi4RetValDiffServRandomDropStatus)
{
    tQoSRDCfgNode      *pRDCfgNode = NULL;
    if (gQoSGlobalInfo.eSysControl != QOS_SYS_CNTL_START)
    {
        QOS_TRC (MGMT_TRC, QOS_MODULE_SHUTDOW_ERROR_MSG);
        return (SNMP_FAILURE);
    }
    pRDCfgNode = QoSUtlGetRDCfgNode (u4DiffServRandomDropId,
                                     QOS_START_INITIAL_INDEX);
    if (pRDCfgNode == NULL)
    {
        return SNMP_FAILURE;
    }
    *pi4RetValDiffServRandomDropStatus = pRDCfgNode->u1Status;
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetDiffServRandomDropMinThreshBytes
 Input       :  The Indices
                DiffServRandomDropId

                The Object 
                setValDiffServRandomDropMinThreshBytes
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDiffServRandomDropMinThreshBytes (UINT4 u4DiffServRandomDropId,
                                        UINT4
                                        u4SetValDiffServRandomDropMinThreshBytes)
{
    tQoSRDCfgNode      *pRDCfgNode = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    pRDCfgNode = QoSUtlGetRDCfgNode (u4DiffServRandomDropId,
                                     QOS_START_INITIAL_INDEX);
    if (pRDCfgNode == NULL)
    {
        return SNMP_FAILURE;
    }
    pRDCfgNode->u4MinAvgThresh = u4SetValDiffServRandomDropMinThreshBytes;
    SnmpNotifyInfo.pu4ObjectId = DiffServRandomDropMinThreshBytes;
    SnmpNotifyInfo.u4OidLen =
        sizeof (DiffServRandomDropMinThreshBytes) / sizeof (UINT4);
    SnmpNotifyInfo.u4SeqNum = 0;
    SnmpNotifyInfo.u1RowStatus = FALSE;
    SnmpNotifyInfo.pLockPointer = QoSLock;
    SnmpNotifyInfo.pUnLockPointer = QoSUnLock;
    SnmpNotifyInfo.u4Indices = 1;
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%u %u",
                      u4DiffServRandomDropId,
                      u4SetValDiffServRandomDropMinThreshBytes));
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetDiffServRandomDropMinThreshPkts
 Input       :  The Indices
                DiffServRandomDropId

                The Object 
                setValDiffServRandomDropMinThreshPkts
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDiffServRandomDropMinThreshPkts (UINT4 u4DiffServRandomDropId,
                                       UINT4
                                       u4SetValDiffServRandomDropMinThreshPkts)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;

    UNUSED_PARAM (u4DiffServRandomDropId);
    UNUSED_PARAM (u4SetValDiffServRandomDropMinThreshPkts);

    SnmpNotifyInfo.pu4ObjectId = DiffServRandomDropMinThreshPkts;
    SnmpNotifyInfo.u4OidLen =
        sizeof (DiffServRandomDropMinThreshPkts) / sizeof (UINT4);
    SnmpNotifyInfo.u4SeqNum = 0;
    SnmpNotifyInfo.u1RowStatus = FALSE;
    SnmpNotifyInfo.pLockPointer = QoSLock;
    SnmpNotifyInfo.pUnLockPointer = QoSUnLock;
    SnmpNotifyInfo.u4Indices = 1;
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%u %u",
                      u4DiffServRandomDropId,
                      u4SetValDiffServRandomDropMinThreshPkts));

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetDiffServRandomDropMaxThreshBytes
 Input       :  The Indices
                DiffServRandomDropId

                The Object 
                setValDiffServRandomDropMaxThreshBytes
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDiffServRandomDropMaxThreshBytes (UINT4 u4DiffServRandomDropId,
                                        UINT4
                                        u4SetValDiffServRandomDropMaxThreshBytes)
{
    tQoSRDCfgNode      *pRDCfgNode = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;

    pRDCfgNode = QoSUtlGetRDCfgNode (u4DiffServRandomDropId,
                                     QOS_START_INITIAL_INDEX);
    if (pRDCfgNode == NULL)
    {
        return SNMP_FAILURE;
    }
    pRDCfgNode->u4MaxAvgThresh = u4SetValDiffServRandomDropMaxThreshBytes;
    SnmpNotifyInfo.pu4ObjectId = DiffServRandomDropMaxThreshBytes;
    SnmpNotifyInfo.u4OidLen =
        sizeof (DiffServRandomDropMaxThreshBytes) / sizeof (UINT4);
    SnmpNotifyInfo.u4SeqNum = 0;
    SnmpNotifyInfo.u1RowStatus = FALSE;
    SnmpNotifyInfo.pLockPointer = QoSLock;
    SnmpNotifyInfo.pUnLockPointer = QoSUnLock;
    SnmpNotifyInfo.u4Indices = 1;
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%u %u",
                      u4DiffServRandomDropId,
                      u4SetValDiffServRandomDropMaxThreshBytes));
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetDiffServRandomDropMaxThreshPkts
 Input       :  The Indices
                DiffServRandomDropId

                The Object 
                setValDiffServRandomDropMaxThreshPkts
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDiffServRandomDropMaxThreshPkts (UINT4 u4DiffServRandomDropId,
                                       UINT4
                                       u4SetValDiffServRandomDropMaxThreshPkts)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;

    UNUSED_PARAM (u4DiffServRandomDropId);
    UNUSED_PARAM (u4SetValDiffServRandomDropMaxThreshPkts);

    SnmpNotifyInfo.pu4ObjectId = DiffServRandomDropMaxThreshPkts;
    SnmpNotifyInfo.u4OidLen =
        sizeof (DiffServRandomDropMaxThreshPkts) / sizeof (UINT4);
    SnmpNotifyInfo.u4SeqNum = 0;
    SnmpNotifyInfo.u1RowStatus = FALSE;
    SnmpNotifyInfo.pLockPointer = QoSLock;
    SnmpNotifyInfo.pUnLockPointer = QoSUnLock;
    SnmpNotifyInfo.u4Indices = 1;
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%u %u",
                      u4DiffServRandomDropId,
                      u4SetValDiffServRandomDropMaxThreshPkts));
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetDiffServRandomDropProbMax
 Input       :  The Indices
                DiffServRandomDropId

                The Object 
                setValDiffServRandomDropProbMax
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDiffServRandomDropProbMax (UINT4 u4DiffServRandomDropId,
                                 UINT4 u4SetValDiffServRandomDropProbMax)
{
    tQoSRDCfgNode      *pRDCfgNode = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;

    pRDCfgNode = QoSUtlGetRDCfgNode (u4DiffServRandomDropId,
                                     QOS_START_INITIAL_INDEX);
    if (pRDCfgNode == NULL)
    {
        return SNMP_FAILURE;
    }
    /*As per the standard the range is (0-1000).
     *But proprietary mib supports (1-100) so the variable is UINT1*/
    pRDCfgNode->u1MaxProb = (UINT1) u4SetValDiffServRandomDropProbMax;

    SnmpNotifyInfo.pu4ObjectId = DiffServRandomDropProbMax;
    SnmpNotifyInfo.u4OidLen =
        sizeof (DiffServRandomDropProbMax) / sizeof (UINT4);
    SnmpNotifyInfo.u4SeqNum = 0;
    SnmpNotifyInfo.u1RowStatus = FALSE;
    SnmpNotifyInfo.pLockPointer = QoSLock;
    SnmpNotifyInfo.pUnLockPointer = QoSUnLock;
    SnmpNotifyInfo.u4Indices = 1;
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%u %u",
                      u4DiffServRandomDropId,
                      u4SetValDiffServRandomDropProbMax));
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetDiffServRandomDropWeight
 Input       :  The Indices
                DiffServRandomDropId

                The Object 
                setValDiffServRandomDropWeight
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDiffServRandomDropWeight (UINT4 u4DiffServRandomDropId,
                                UINT4 u4SetValDiffServRandomDropWeight)
{
    tQoSRDCfgNode      *pRDCfgNode = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;

    pRDCfgNode = QoSUtlGetRDCfgNode (u4DiffServRandomDropId,
                                     QOS_START_INITIAL_INDEX);
    if (pRDCfgNode == NULL)
    {
        return SNMP_FAILURE;
    }
    pRDCfgNode->u1ExpWeight = (UINT1) u4SetValDiffServRandomDropWeight;
    SnmpNotifyInfo.pu4ObjectId = DiffServRandomDropWeight;
    SnmpNotifyInfo.u4OidLen =
        sizeof (DiffServRandomDropWeight) / sizeof (UINT4);
    SnmpNotifyInfo.u4SeqNum = 0;
    SnmpNotifyInfo.u1RowStatus = FALSE;
    SnmpNotifyInfo.pLockPointer = QoSLock;
    SnmpNotifyInfo.pUnLockPointer = QoSUnLock;
    SnmpNotifyInfo.u4Indices = 1;
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%u %u",
                      u4DiffServRandomDropId,
                      u4SetValDiffServRandomDropWeight));
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetDiffServRandomDropSamplingRate
 Input       :  The Indices
                DiffServRandomDropId

                The Object 
                setValDiffServRandomDropSamplingRate
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDiffServRandomDropSamplingRate (UINT4 u4DiffServRandomDropId,
                                      UINT4
                                      u4SetValDiffServRandomDropSamplingRate)
{
    tQoSRDCfgNode      *pRDCfgNode = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    pRDCfgNode = QoSUtlGetRDCfgNode (u4DiffServRandomDropId,
                                     QOS_START_INITIAL_INDEX);
    if (pRDCfgNode == NULL)
    {
        return SNMP_FAILURE;
    }
    pRDCfgNode->u4DSRandomDropSamplingRate =
        u4SetValDiffServRandomDropSamplingRate;
    SnmpNotifyInfo.pu4ObjectId = DiffServRandomDropSamplingRate;
    SnmpNotifyInfo.u4OidLen =
        sizeof (DiffServRandomDropSamplingRate) / sizeof (UINT4);
    SnmpNotifyInfo.u4SeqNum = 0;
    SnmpNotifyInfo.u1RowStatus = FALSE;
    SnmpNotifyInfo.pLockPointer = QoSLock;
    SnmpNotifyInfo.pUnLockPointer = QoSUnLock;
    SnmpNotifyInfo.u4Indices = 1;
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%u %u",
                      u4DiffServRandomDropId,
                      u4SetValDiffServRandomDropSamplingRate));
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetDiffServRandomDropStorage
 Input       :  The Indices
                DiffServRandomDropId

                The Object 
                setValDiffServRandomDropStorage
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDiffServRandomDropStorage (UINT4 u4DiffServRandomDropId,
                                 INT4 i4SetValDiffServRandomDropStorage)
{
    tQoSRDCfgNode      *pRDCfgNode = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    pRDCfgNode = QoSUtlGetRDCfgNode (u4DiffServRandomDropId,
                                     QOS_START_INITIAL_INDEX);
    if (pRDCfgNode == NULL)
    {
        return SNMP_FAILURE;
    }
    pRDCfgNode->i4StorageType = (UINT1) i4SetValDiffServRandomDropStorage;
    SnmpNotifyInfo.pu4ObjectId = DiffServRandomDropStorage;
    SnmpNotifyInfo.u4OidLen =
        sizeof (DiffServRandomDropStorage) / sizeof (UINT4);
    SnmpNotifyInfo.u4SeqNum = 0;
    SnmpNotifyInfo.u1RowStatus = FALSE;
    SnmpNotifyInfo.pLockPointer = QoSLock;
    SnmpNotifyInfo.pUnLockPointer = QoSUnLock;
    SnmpNotifyInfo.u4Indices = 1;
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%u %i",
                      u4DiffServRandomDropId,
                      i4SetValDiffServRandomDropStorage));
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetDiffServRandomDropStatus
 Input       :  The Indices
                DiffServRandomDropId
                The Object 
                setValDiffServRandomDropStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDiffServRandomDropStatus (UINT4 u4DiffServRandomDropId,
                                INT4 i4SetValDiffServRandomDropStatus)
{
    INT4                i4RetStatus = SNMP_FAILURE;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    i4RetStatus = nmhSetFsQoSRandomDetectCfgStatus (u4DiffServRandomDropId,
                                                    QOS_START_INITIAL_INDEX,
                                                    i4SetValDiffServRandomDropStatus);
    if (i4RetStatus == SNMP_SUCCESS)
    {
        SnmpNotifyInfo.pu4ObjectId = DiffServRandomDropStatus;
        SnmpNotifyInfo.u4OidLen =
            sizeof (DiffServRandomDropStatus) / sizeof (UINT4);
        SnmpNotifyInfo.u4SeqNum = 0;
        SnmpNotifyInfo.u1RowStatus = TRUE;
        SnmpNotifyInfo.pLockPointer = QoSLock;
        SnmpNotifyInfo.pUnLockPointer = QoSUnLock;
        SnmpNotifyInfo.u4Indices = 1;
        SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;

        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%u %i",
                          u4DiffServRandomDropId,
                          i4SetValDiffServRandomDropStatus));
        return SNMP_SUCCESS;
    }
    else
    {
        return SNMP_FAILURE;
    }
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2DiffServRandomDropMinThreshBytes
 Input       :  The Indices
                DiffServRandomDropId

                The Object 
                testValDiffServRandomDropMinThreshBytes
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2DiffServRandomDropMinThreshBytes (UINT4 *pu4ErrorCode,
                                           UINT4 u4DiffServRandomDropId,
                                           UINT4
                                           u4TestValDiffServRandomDropMinThreshBytes)
{
    return (nmhTestv2FsQoSRandomDetectCfgMinAvgThresh (pu4ErrorCode,
                                                       u4DiffServRandomDropId,
                                                       QOS_START_INITIAL_INDEX,
                                                       u4TestValDiffServRandomDropMinThreshBytes));
}

/****************************************************************************
 Function    :  nmhTestv2DiffServRandomDropMinThreshPkts
 Input       :  The Indices
                DiffServRandomDropId

                The Object 
                testValDiffServRandomDropMinThreshPkts
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2DiffServRandomDropMinThreshPkts (UINT4 *pu4ErrorCode,
                                          UINT4 u4DiffServRandomDropId,
                                          UINT4
                                          u4TestValDiffServRandomDropMinThreshPkts)
{
    if (gQoSGlobalInfo.eSysControl != QOS_SYS_CNTL_START)
    {
        QOS_TRC (MGMT_TRC, QOS_MODULE_SHUTDOW_ERROR_MSG);
        return (SNMP_FAILURE);
    }
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (u4DiffServRandomDropId);
    UNUSED_PARAM (u4TestValDiffServRandomDropMinThreshPkts);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2DiffServRandomDropMaxThreshBytes
 Input       :  The Indices
                DiffServRandomDropId

                The Object 
                testValDiffServRandomDropMaxThreshBytes
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2DiffServRandomDropMaxThreshBytes (UINT4 *pu4ErrorCode,
                                           UINT4 u4DiffServRandomDropId,
                                           UINT4
                                           u4TestValDiffServRandomDropMaxThreshBytes)
{
    return (nmhTestv2FsQoSRandomDetectCfgMaxAvgThresh (pu4ErrorCode,
                                                       u4DiffServRandomDropId,
                                                       QOS_START_INITIAL_INDEX,
                                                       u4TestValDiffServRandomDropMaxThreshBytes));
}

/****************************************************************************
 Function    :  nmhTestv2DiffServRandomDropMaxThreshPkts
 Input       :  The Indices
                DiffServRandomDropId

                The Object 
                testValDiffServRandomDropMaxThreshPkts
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2DiffServRandomDropMaxThreshPkts (UINT4 *pu4ErrorCode,
                                          UINT4 u4DiffServRandomDropId,
                                          UINT4
                                          u4TestValDiffServRandomDropMaxThreshPkts)
{
    if (gQoSGlobalInfo.eSysControl != QOS_SYS_CNTL_START)
    {
        QOS_TRC (MGMT_TRC, QOS_MODULE_SHUTDOW_ERROR_MSG);
        return (SNMP_FAILURE);
    }
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (u4DiffServRandomDropId);
    UNUSED_PARAM (u4TestValDiffServRandomDropMaxThreshPkts);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2DiffServRandomDropProbMax
 Input       :  The Indices
                DiffServRandomDropId

                The Object 
                testValDiffServRandomDropProbMax
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2DiffServRandomDropProbMax (UINT4 *pu4ErrorCode,
                                    UINT4 u4DiffServRandomDropId,
                                    UINT4 u4TestValDiffServRandomDropProbMax)
{
    return (nmhTestv2FsQoSRandomDetectCfgMaxProb (pu4ErrorCode,
                                                  u4DiffServRandomDropId,
                                                  QOS_START_INITIAL_INDEX,
                                                  u4TestValDiffServRandomDropProbMax));
}

/****************************************************************************
 Function    :  nmhTestv2DiffServRandomDropWeight
 Input       :  The Indices
                DiffServRandomDropId

                The Object 
                testValDiffServRandomDropWeight
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2DiffServRandomDropWeight (UINT4 *pu4ErrorCode,
                                   UINT4 u4DiffServRandomDropId,
                                   UINT4 u4TestValDiffServRandomDropWeight)
{
    return (nmhTestv2FsQoSRandomDetectCfgExpWeight (pu4ErrorCode,
                                                    u4DiffServRandomDropId,
                                                    QOS_START_INITIAL_INDEX,
                                                    u4TestValDiffServRandomDropWeight));
}

/****************************************************************************
 Function    :  nmhTestv2DiffServRandomDropSamplingRate
 Input       :  The Indices
                DiffServRandomDropId

                The Object 
                testValDiffServRandomDropSamplingRate
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2DiffServRandomDropSamplingRate (UINT4 *pu4ErrorCode,
                                         UINT4 u4DiffServRandomDropId,
                                         UINT4
                                         u4TestValDiffServRandomDropSamplingRate)
{
    tQoSRDCfgNode      *pRDCfgNode = NULL;

    if (gQoSGlobalInfo.eSysControl != QOS_SYS_CNTL_START)
    {
        QOS_TRC (MGMT_TRC, QOS_MODULE_SHUTDOW_ERROR_MSG);
        return (SNMP_FAILURE);
    }
    pRDCfgNode = QoSUtlGetRDCfgNode (u4DiffServRandomDropId,
                                     QOS_START_INITIAL_INDEX);
    if (pRDCfgNode == NULL)
    {
        return SNMP_FAILURE;
    }
    if (pRDCfgNode->u1Status == ACTIVE)
    {
        QOS_TRC (MGMT_TRC, "Can not change the value in ACTIVE State.\r\n");

        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    if ((u4TestValDiffServRandomDropSamplingRate != QOS_START_INITIAL_INDEX) &&
        (u4TestValDiffServRandomDropSamplingRate > QOS_MAX_SAMPLING_RATE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2DiffServRandomDropStorage
 Input       :  The Indices
                DiffServRandomDropId

                The Object 
                testValDiffServRandomDropStorage
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2DiffServRandomDropStorage (UINT4 *pu4ErrorCode,
                                    UINT4 u4DiffServRandomDropId,
                                    INT4 i4TestValDiffServRandomDropStorage)
{
    tQoSRDCfgNode      *pRDCfgNode = NULL;

    if (gQoSGlobalInfo.eSysControl != QOS_SYS_CNTL_START)
    {
        QOS_TRC (MGMT_TRC, QOS_MODULE_SHUTDOW_ERROR_MSG);
        return (SNMP_FAILURE);
    }
    pRDCfgNode = QoSUtlGetRDCfgNode (u4DiffServRandomDropId,
                                     QOS_START_INITIAL_INDEX);
    if (pRDCfgNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetQTempNode () Returns "
                      "FAILURE. \r\n", __FUNCTION__);

        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    if (pRDCfgNode->u1Status == QOS_ACTIVE)
    {
        QOS_TRC (MGMT_TRC, "Can not change the value in ACTIVE State.\r\n");

        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    if ((i4TestValDiffServRandomDropStorage != QOS_STORAGE_VOLATILE) &&
        (i4TestValDiffServRandomDropStorage != QOS_STORAGE_NONVOLATILE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhTestv2DiffServRandomDropStatus
 Input       :  The Indices
                DiffServRandomDropId

                The Object 
                testValDiffServRandomDropStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2DiffServRandomDropStatus (UINT4 *pu4ErrorCode,
                                   UINT4 u4DiffServRandomDropId,
                                   INT4 i4TestValDiffServRandomDropStatus)
{
    UINT4               u4CurrentType = DS_RANDOM_DROP;
    UINT4               u4DummyId = 0;

    if ((i4TestValDiffServRandomDropStatus == QOS_DESTROY) &&
        (QosUtlPointedToStatus
         (&u4CurrentType, &u4DiffServRandomDropId, &u4DummyId,
          DS_RANDOM_DROP, u4DiffServRandomDropId, 0, QOS_CHECK) != QOS_SUCCESS))
    {
        return SNMP_FAILURE;
    }

    if ((i4TestValDiffServRandomDropStatus == QOS_CREATE_AND_WAIT) &&
        (QOS_RD_COUNT >= QOS_RD_TBL_MAX_ENTRIES))
    {
        return (SNMP_FAILURE);
    }

    return (nmhTestv2FsQoSRandomDetectCfgStatus (pu4ErrorCode,
                                                 u4DiffServRandomDropId,
                                                 QOS_START_INITIAL_INDEX,
                                                 i4TestValDiffServRandomDropStatus));
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2DiffServRandomDropTable
 Input       :  The Indices
                DiffServRandomDropId
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2DiffServRandomDropTable (UINT4 *pu4ErrorCode,
                                 tSnmpIndexList * pSnmpIndexList,
                                 tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */
/****************************************************************************
 Function    :  nmhGetDiffServQNextFree
 Input       :  The Indices

                The Object 
                retValDiffServQNextFree
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDiffServQNextFree (UINT4 *pu4RetValDiffServQNextFree)
{
    if (gQoSGlobalInfo.eSysControl != QOS_SYS_CNTL_START)
    {
        QOS_TRC (MGMT_TRC, QOS_MODULE_SHUTDOW_ERROR_MSG);
        *pu4RetValDiffServQNextFree = 0;
        return SNMP_SUCCESS;
    }
    *pu4RetValDiffServQNextFree = QOS_CLFR_ELEM_NEXT_FREE ();
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : DiffServQTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceDiffServQTable
 Input       :  The Indices
                DiffServQId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceDiffServQTable (UINT4 u4DiffServQId)
{
    return (nmhValidateIndexInstanceFsQoSQTable
            (QOS_IF_IDX_FROM_GLOBAL_QID (u4DiffServQId),
             QOS_IF_QID_FROM_GLOBAL_QID (u4DiffServQId)));
}

/****************************************************************************
 Function    :  nmhGetFirstIndexDiffServQTable
 Input       :  The Indices
                DiffServQId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexDiffServQTable (UINT4 *pu4DiffServQId)
{
    INT4                i4IfIndex;
    UINT4               u4IfQid;

    if (nmhGetFirstIndexFsQoSQTable (&i4IfIndex, &u4IfQid) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    *pu4DiffServQId = QOS_GLOBAL_QID_FROM_IF_QID (i4IfIndex, u4IfQid);

    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhGetNextIndexDiffServQTable
Input       :  The Indices
DiffServQId
                nextDiffServQId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexDiffServQTable (UINT4 u4DiffServQId, UINT4 *pu4NextDiffServQId)
{
    INT4                i4IfIndex;
    UINT4               u4IfQid;

    if (nmhGetNextIndexFsQoSQTable (QOS_IF_IDX_FROM_GLOBAL_QID (u4DiffServQId),
                                    &i4IfIndex,
                                    QOS_IF_QID_FROM_GLOBAL_QID (u4DiffServQId),
                                    &u4IfQid) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }

    *pu4NextDiffServQId = QOS_GLOBAL_QID_FROM_IF_QID (i4IfIndex, u4IfQid);

    return (SNMP_SUCCESS);

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetDiffServQNext
 Input       :  The Indices
                DiffServQId

                The Object 
                retValDiffServQNext
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDiffServQNext (UINT4 u4DiffServQId, tSNMP_OID_TYPE * pRetValDiffServQNext)
{
    UINT4               u4SchedulerId = 0;

    if (nmhGetFsQoSQSchedulerId (QOS_IF_IDX_FROM_GLOBAL_QID (u4DiffServQId),
                                 QOS_IF_QID_FROM_GLOBAL_QID (u4DiffServQId),
                                 &u4SchedulerId) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }

    QosGetOidFromTypeId (u4SchedulerId, DS_SCHEDULER, pRetValDiffServQNext);
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetDiffServQMinRate
 Input       :  The Indices
                DiffServQId

                The Object 
                retValDiffServQMinRate
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDiffServQMinRate (UINT4 u4DiffServQId,
                        tSNMP_OID_TYPE * pRetValDiffServQMinRate)
{
    UINT4               u4MinRateId = 0;

    if (nmhGetFsQoSQShapeId (QOS_IF_IDX_FROM_GLOBAL_QID (u4DiffServQId),
                             QOS_IF_QID_FROM_GLOBAL_QID (u4DiffServQId),
                             &u4MinRateId) == SNMP_FAILURE)
    {

    }

    QosGetOidFromTypeId (u4MinRateId, DS_MINRATE, pRetValDiffServQMinRate);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDiffServQMaxRate
 Input       :  The Indices
                DiffServQId

                The Object 
                retValDiffServQMaxRate
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDiffServQMaxRate (UINT4 u4DiffServQId,
                        tSNMP_OID_TYPE * pRetValDiffServQMaxRate)
{
    UINT4               u4MaxRateId = 0;

    /* MaxRate is nothing but the shaper */

    if (nmhGetFsQoSQShapeId (QOS_IF_IDX_FROM_GLOBAL_QID (u4DiffServQId),
                             QOS_IF_QID_FROM_GLOBAL_QID (u4DiffServQId),
                             &u4MaxRateId) == SNMP_FAILURE)
    {

    }

    QosGetOidFromTypeId (u4MaxRateId, DS_MAXRATE, pRetValDiffServQMaxRate);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDiffServQStorage
 Input       :  The Indices
                DiffServQId

                The Object 
                retValDiffServQStorage
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDiffServQStorage (UINT4 u4DiffServQId, INT4 *pi4RetValDiffServQStorage)
{
    UNUSED_PARAM (u4DiffServQId);
    UNUSED_PARAM (pi4RetValDiffServQStorage);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDiffServQStatus
 Input       :  The Indices
                DiffServQId

                The Object 
                retValDiffServQStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDiffServQStatus (UINT4 u4DiffServQId, INT4 *pi4RetValDiffServQStatus)
{

    if (nmhGetFsQoSQStatus (QOS_IF_IDX_FROM_GLOBAL_QID (u4DiffServQId),
                            QOS_IF_QID_FROM_GLOBAL_QID (u4DiffServQId),
                            pi4RetValDiffServQStatus) == SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;

    }
    else
    {
        return SNMP_FAILURE;
    }

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetDiffServQNext
 Input       :  The Indices
                DiffServQId

                The Object 
                setValDiffServQNext
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDiffServQNext (UINT4 u4DiffServQId, tSNMP_OID_TYPE * pSetValDiffServQNext)
{
    UINT4               u4Type = 0;
    UINT4               u4Id = 0;
    UINT4               u4LocalSchedId;
    UINT4               u4LocalSchedInterface;
    INT4                i4Return = SNMP_FAILURE;
    QosGetTypeIdFromOid (pSetValDiffServQNext, &u4Type, &u4Id);

    if ((u4Type != DS_SCHEDULER) && (u4Type != QOS_ROWPOINTER_DEF))
    {
        return SNMP_FAILURE;
    }

    i4Return = QosSchedulerGetIfIdFromGlobalId (u4Id, &u4LocalSchedInterface,
                                                &u4LocalSchedId);
    UNUSED_PARAM (i4Return);
    if ((u4LocalSchedInterface != QOS_IF_IDX_FROM_GLOBAL_QID (u4DiffServQId)) &&
        (u4LocalSchedInterface != 0))
    {
        return SNMP_FAILURE;
    }

    return nmhSetFsQoSQSchedulerId (QOS_IF_IDX_FROM_GLOBAL_QID (u4DiffServQId),
                                    QOS_IF_QID_FROM_GLOBAL_QID (u4DiffServQId),
                                    u4LocalSchedId);
}

/****************************************************************************
 Function    :  nmhSetDiffServQMinRate
 Input       :  The Indices
                DiffServQId

                The Object 
                setValDiffServQMinRate
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDiffServQMinRate (UINT4 u4DiffServQId,
                        tSNMP_OID_TYPE * pSetValDiffServQMinRate)
{
    UINT4               u4Type = 0;
    UINT4               u4Id = 0;

    QosGetTypeIdFromOid (pSetValDiffServQMinRate, &u4Type, &u4Id);

    if ((u4Type != DS_MINRATE) && (u4Type != QOS_ROWPOINTER_DEF))
    {
        return SNMP_FAILURE;
    }

    return (nmhSetFsQoSQShapeId (QOS_IF_IDX_FROM_GLOBAL_QID (u4DiffServQId),
                                 QOS_IF_QID_FROM_GLOBAL_QID (u4DiffServQId),
                                 u4Id));

}

/****************************************************************************
 Function    :  nmhSetDiffServQMaxRate
 Input       :  The Indices
                DiffServQId

                The Object 
                setValDiffServQMaxRate
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDiffServQMaxRate (UINT4 u4DiffServQId,
                        tSNMP_OID_TYPE * pSetValDiffServQMaxRate)
{
    UINT4               u4Type = 0;
    UINT4               u4Id = 0;

    QosGetTypeIdFromOid (pSetValDiffServQMaxRate, &u4Type, &u4Id);

    if ((u4Type != DS_MAXRATE) && (u4Type != QOS_ROWPOINTER_DEF))
    {
        return SNMP_FAILURE;
    }

    return (nmhSetFsQoSQShapeId (QOS_IF_IDX_FROM_GLOBAL_QID (u4DiffServQId),
                                 QOS_IF_QID_FROM_GLOBAL_QID (u4DiffServQId),
                                 u4Id));

}

/****************************************************************************
 Function    :  nmhSetDiffServQStorage
 Input       :  The Indices
                DiffServQId

                The Object 
                setValDiffServQStorage
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDiffServQStorage (UINT4 u4DiffServQId, INT4 i4SetValDiffServQStorage)
{
    UNUSED_PARAM (u4DiffServQId);
    UNUSED_PARAM (i4SetValDiffServQStorage);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetDiffServQStatus
 Input       :  The Indices
                DiffServQId

                The Object 
                setValDiffServQStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDiffServQStatus (UINT4 u4DiffServQId, INT4 i4SetValDiffServQStatus)
{

    return (nmhSetFsQoSQStatus (QOS_IF_IDX_FROM_GLOBAL_QID (u4DiffServQId),
                                QOS_IF_QID_FROM_GLOBAL_QID (u4DiffServQId),
                                i4SetValDiffServQStatus));

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2DiffServQNext
 Input       :  The Indices
                DiffServQId

                The Object 
                testValDiffServQNext
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2DiffServQNext (UINT4 *pu4ErrorCode, UINT4 u4DiffServQId,
                        tSNMP_OID_TYPE * pTestValDiffServQNext)
{
    UINT4               u4Id = QOS_ROWPOINTER_DEF;
    UINT4               u4Type = QOS_ROWPOINTER_DEF;
    UINT4               u4LocalSchedId;
    UINT4               u4LocalSchedInterface;
    INT4                i4Return = SNMP_FAILURE;
    QosGetTypeIdFromOid (pTestValDiffServQNext, &u4Type, &u4Id);

    if ((u4Type != DS_SCHEDULER) && (u4Type != QOS_ROWPOINTER_DEF))
    {
        return SNMP_FAILURE;
    }
    i4Return = QosSchedulerGetIfIdFromGlobalId (u4Id, &u4LocalSchedInterface,
                                                &u4LocalSchedId);
    UNUSED_PARAM (i4Return);
    if ((u4LocalSchedInterface != QOS_IF_IDX_FROM_GLOBAL_QID (u4DiffServQId)) &&
        (u4LocalSchedInterface != 0))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    return nmhTestv2FsQoSQSchedulerId (pu4ErrorCode,
                                       QOS_IF_IDX_FROM_GLOBAL_QID
                                       (u4DiffServQId),
                                       QOS_IF_QID_FROM_GLOBAL_QID
                                       (u4DiffServQId), u4LocalSchedId);

}

/****************************************************************************
 Function    :  nmhTestv2DiffServQMinRate
 Input       :  The Indices
                DiffServQId

                The Object 
                testValDiffServQMinRate
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2DiffServQMinRate (UINT4 *pu4ErrorCode, UINT4 u4DiffServQId,
                           tSNMP_OID_TYPE * pTestValDiffServQMinRate)
{
    UINT4               u4Id = QOS_ROWPOINTER_DEF;
    UINT4               u4Type = QOS_ROWPOINTER_DEF;

    QosGetTypeIdFromOid (pTestValDiffServQMinRate, &u4Type, &u4Id);

    if ((u4Type != DS_MINRATE) && (u4Type != QOS_ROWPOINTER_DEF))
    {
        return SNMP_FAILURE;
    }

    return (nmhTestv2FsQoSQShapeId (pu4ErrorCode,
                                    QOS_IF_IDX_FROM_GLOBAL_QID (u4DiffServQId),
                                    QOS_IF_QID_FROM_GLOBAL_QID (u4DiffServQId),
                                    u4Id));

}

/****************************************************************************
 Function    :  nmhTestv2DiffServQMaxRate
 Input       :  The Indices
                DiffServQId

                The Object 
                testValDiffServQMaxRate
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2DiffServQMaxRate (UINT4 *pu4ErrorCode, UINT4 u4DiffServQId,
                           tSNMP_OID_TYPE * pTestValDiffServQMaxRate)
{
    UINT4               u4Id = QOS_ROWPOINTER_DEF;
    UINT4               u4Type = QOS_ROWPOINTER_DEF;

    QosGetTypeIdFromOid (pTestValDiffServQMaxRate, &u4Type, &u4Id);

    if ((u4Type != DS_MAXRATE) && (u4Type != QOS_ROWPOINTER_DEF))
    {
        return SNMP_FAILURE;
    }

    return (nmhTestv2FsQoSQShapeId (pu4ErrorCode,
                                    QOS_IF_IDX_FROM_GLOBAL_QID (u4DiffServQId),
                                    QOS_IF_QID_FROM_GLOBAL_QID (u4DiffServQId),
                                    u4Id));

}

/****************************************************************************
 Function    :  nmhTestv2DiffServQStorage
 Input       :  The Indices
                DiffServQId

                The Object 
                testValDiffServQStorage
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2DiffServQStorage (UINT4 *pu4ErrorCode, UINT4 u4DiffServQId,
                           INT4 i4TestValDiffServQStorage)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (u4DiffServQId);
    UNUSED_PARAM (i4TestValDiffServQStorage);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2DiffServQStatus
 Input       :  The Indices
                DiffServQId

                The Object 
                testValDiffServQStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2DiffServQStatus (UINT4 *pu4ErrorCode, UINT4 u4DiffServQId,
                          INT4 i4TestValDiffServQStatus)
{
    if ((i4TestValDiffServQStatus == QOS_CREATE_AND_WAIT) &&
        (QOS_Q_COUNT >= QOS_Q_TBL_MAX_ENTRIES))
    {
        return (SNMP_FAILURE);
    }

    return (nmhTestv2FsQoSQStatus (pu4ErrorCode,
                                   QOS_IF_IDX_FROM_GLOBAL_QID (u4DiffServQId),
                                   QOS_IF_QID_FROM_GLOBAL_QID (u4DiffServQId),
                                   i4TestValDiffServQStatus));
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2DiffServQTable
 Input       :  The Indices
                DiffServQId
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2DiffServQTable (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                        tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetDiffServSchedulerNextFree
 Input       :  The Indices

                The Object 
                retValDiffServSchedulerNextFree
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDiffServSchedulerNextFree (UINT4 *pu4RetValDiffServSchedulerNextFree)
{
    if (gQoSGlobalInfo.eSysControl != QOS_SYS_CNTL_START)
    {
        QOS_TRC (MGMT_TRC, QOS_MODULE_SHUTDOW_ERROR_MSG);
        *pu4RetValDiffServSchedulerNextFree = 0;
        return SNMP_SUCCESS;
    }
    *pu4RetValDiffServSchedulerNextFree = QOS_SCHED_NEXT_FREE ();
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : DiffServSchedulerTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceDiffServSchedulerTable
 Input       :  The Indices
                DiffServSchedulerId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceDiffServSchedulerTable (UINT4 u4DiffServSchedulerId)
{
    UNUSED_PARAM (u4DiffServSchedulerId);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexDiffServSchedulerTable
 Input       :  The Indices
                DiffServSchedulerId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexDiffServSchedulerTable (UINT4 *pu4DiffServSchedulerId)
{

    tQosSchedulerMapNode *pQosSchedulerMapNode;

    pQosSchedulerMapNode = (tQosSchedulerMapNode *)
        RBTreeGetFirst (gQoSGlobalInfo.pRbSchedGlobTbl);

    if (pQosSchedulerMapNode == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4DiffServSchedulerId = pQosSchedulerMapNode->u4GlobalId;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexDiffServSchedulerTable
 Input       :  The Indices
                DiffServSchedulerId
                nextDiffServSchedulerId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexDiffServSchedulerTable (UINT4 u4DiffServSchedulerId,
                                       UINT4 *pu4NextDiffServSchedulerId)
{
    tQosSchedulerMapNode *pQosSchedulerMapNode;
    tQosSchedulerMapNode QosSchedulerMapNode;

    QosSchedulerMapNode.u4GlobalId = u4DiffServSchedulerId;

    pQosSchedulerMapNode = (tQosSchedulerMapNode *)
        RBTreeGetNext (gQoSGlobalInfo.pRbSchedGlobTbl,
                       &QosSchedulerMapNode, QoSSchedMapCmpFun);

    if (pQosSchedulerMapNode == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4NextDiffServSchedulerId = pQosSchedulerMapNode->u4GlobalId;

    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetDiffServSchedulerNext
 Input       :  The Indices
                DiffServSchedulerId

                The Object 
                retValDiffServSchedulerNext
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDiffServSchedulerNext (UINT4 u4DiffServSchedulerId,
                             tSNMP_OID_TYPE * pRetValDiffServSchedulerNext)
{

    tQoSSchedNode      *pSchedNode = NULL;

    UINT4               u4Id = QOS_ROWPOINTER_DEF;
    UINT4               u4Type = QOS_ROWPOINTER_DEF;
    UINT4               u4IfSchedId = 0;
    UINT4               u4Interface = 0;
    INT1                i1ReturnValue = SNMP_FAILURE;

    if (gQoSGlobalInfo.eSysControl != QOS_SYS_CNTL_START)
    {
        QOS_TRC (MGMT_TRC, QOS_MODULE_SHUTDOW_ERROR_MSG);
        return (SNMP_FAILURE);
    }

    if (QosSchedulerGetIfIdFromGlobalId
        (u4DiffServSchedulerId, &u4Interface, &u4IfSchedId) == QOS_FAILURE)
    {
        return SNMP_FAILURE;
    }
    pSchedNode = QoSUtlGetSchedNode (u4Interface, u4IfSchedId);

    if (pSchedNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QosGetSchedulerEntry () "
                      " Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    u4Id = pSchedNode->u4DSSchedNextId;
    u4Type = pSchedNode->u4DSSchedNextType;

    if (QosGetOidFromTypeId (u4Id, u4Type, pRetValDiffServSchedulerNext)
        == QOS_SUCCESS)
    {
        i1ReturnValue = SNMP_SUCCESS;
    }
    return i1ReturnValue;

}

/****************************************************************************
 Function    :  nmhGetDiffServSchedulerMethod
 Input       :  The Indices
                DiffServSchedulerId

                The Object 
                retValDiffServSchedulerMethod
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDiffServSchedulerMethod (UINT4 u4DiffServSchedulerId,
                               tSNMP_OID_TYPE * pRetValDiffServSchedulerMethod)
{
    UINT4               u4Interface;
    UINT4               u4IfSchedId;
    INT4                i4SchedulerAlgo;
    UINT4               u4SchedMethod = DS_SCHED_SP;

    if (QosSchedulerGetIfIdFromGlobalId
        (u4DiffServSchedulerId, &u4Interface, &u4IfSchedId) == QOS_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (nmhGetFsQoSSchedulerSchedAlgorithm
        (u4Interface, u4IfSchedId, &i4SchedulerAlgo) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }

    switch (i4SchedulerAlgo)
    {
        case QOS_CLI_SCHED_ALGO_SP:
            u4SchedMethod = DS_SCHED_SP;
            break;

        case QOS_CLI_SCHED_ALGO_WRR:
            u4SchedMethod = DS_SCHED_WRR;
            break;

        case QOS_CLI_SCHED_ALGO_WFQ:
            u4SchedMethod = DS_SCHED_WFQ;
            break;
        default:
            break;
    }

    QosSchedOidFromType (u4SchedMethod, pRetValDiffServSchedulerMethod);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDiffServSchedulerMinRate
 Input       :  The Indices
                DiffServSchedulerId

                The Object 
                retValDiffServSchedulerMinRate
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDiffServSchedulerMinRate (UINT4 u4DiffServSchedulerId,
                                tSNMP_OID_TYPE *
                                pRetValDiffServSchedulerMinRate)
{
    tQoSSchedNode      *pSchedNode = NULL;

    UINT4               u4Id = QOS_ROWPOINTER_DEF;
    UINT4               u4Type = QOS_ROWPOINTER_DEF;
    UINT4               u4IfSchedId = 0;

    UINT4               u4Interface = 0;
    INT1                i1ReturnValue = SNMP_FAILURE;

    if (gQoSGlobalInfo.eSysControl != QOS_SYS_CNTL_START)
    {
        QOS_TRC (MGMT_TRC, QOS_MODULE_SHUTDOW_ERROR_MSG);
        return (SNMP_FAILURE);
    }

    if (QosSchedulerGetIfIdFromGlobalId
        (u4DiffServSchedulerId, &u4Interface, &u4IfSchedId) == QOS_FAILURE)
    {
        return SNMP_FAILURE;
    }
    pSchedNode = QoSUtlGetSchedNode (u4Interface, u4IfSchedId);

    if (pSchedNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QosGetSchedulerEntry () "
                      " Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    u4Id = pSchedNode->u4DSSchedulerMinRateId;
    u4Type = pSchedNode->u4DSSchedulerMinRateType;

    if (QosGetOidFromTypeId (u4Id, u4Type, pRetValDiffServSchedulerMinRate) ==
        QOS_SUCCESS)
    {
        i1ReturnValue = SNMP_SUCCESS;
    }
    return i1ReturnValue;
}

/****************************************************************************
 Function    :  nmhGetDiffServSchedulerMaxRate
 Input       :  The Indices
                DiffServSchedulerId

                The Object 
                retValDiffServSchedulerMaxRate
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDiffServSchedulerMaxRate (UINT4 u4DiffServSchedulerId,
                                tSNMP_OID_TYPE *
                                pRetValDiffServSchedulerMaxRate)
{
    tQoSSchedNode      *pSchedNode = NULL;

    UINT4               u4Id = QOS_ROWPOINTER_DEF;
    UINT4               u4Type = QOS_ROWPOINTER_DEF;
    UINT4               u4IfSchedId = 0;

    UINT4               u4Interface = 0;
    INT1                i1ReturnValue = SNMP_FAILURE;

    if (gQoSGlobalInfo.eSysControl != QOS_SYS_CNTL_START)
    {
        QOS_TRC (MGMT_TRC, QOS_MODULE_SHUTDOW_ERROR_MSG);
        return (SNMP_FAILURE);
    }

    if (QosSchedulerGetIfIdFromGlobalId
        (u4DiffServSchedulerId, &u4Interface, &u4IfSchedId) == QOS_FAILURE)
    {
        return SNMP_FAILURE;
    }
    pSchedNode = QoSUtlGetSchedNode (u4Interface, u4IfSchedId);

    if (pSchedNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QosGetSchedulerEntry () "
                      " Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    u4Id = pSchedNode->u4DSSchedulerMaxRateId;
    u4Type = pSchedNode->u4DSSchedulerMaxRateType;

    if (QosGetOidFromTypeId (u4Id, u4Type, pRetValDiffServSchedulerMaxRate) ==
        QOS_SUCCESS)
    {
        i1ReturnValue = SNMP_SUCCESS;
    }
    return i1ReturnValue;
}

/****************************************************************************
 Function    :  nmhGetDiffServSchedulerStorage
 Input       :  The Indices
                DiffServSchedulerId

                The Object 
                retValDiffServSchedulerStorage
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDiffServSchedulerStorage (UINT4 u4DiffServSchedulerId,
                                INT4 *pi4RetValDiffServSchedulerStorage)
{
    UNUSED_PARAM (u4DiffServSchedulerId);
    UNUSED_PARAM (pi4RetValDiffServSchedulerStorage);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDiffServSchedulerStatus
 Input       :  The Indices
                DiffServSchedulerId

                The Object 
                retValDiffServSchedulerStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDiffServSchedulerStatus (UINT4 u4DiffServSchedulerId,
                               INT4 *pi4RetValDiffServSchedulerStatus)
{

    UINT4               u4Interface;
    UINT4               u4IfSchedId;

    if (QosSchedulerGetIfIdFromGlobalId
        (u4DiffServSchedulerId, &u4Interface, &u4IfSchedId) == QOS_FAILURE)
    {
        return SNMP_FAILURE;
    }

    return (nmhGetFsQoSSchedulerStatus
            (u4Interface, u4IfSchedId, pi4RetValDiffServSchedulerStatus));
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetDiffServSchedulerNext
 Input       :  The Indices
                DiffServSchedulerId

                The Object 
                setValDiffServSchedulerNext
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDiffServSchedulerNext (UINT4 u4DiffServSchedulerId,
                             tSNMP_OID_TYPE * pSetValDiffServSchedulerNext)
{
    tQoSSchedNode      *pSchedNode = NULL;

    UINT4               u4DSSchedNextId = QOS_ROWPOINTER_DEF;
    UINT4               u4DSSchedNextType = QOS_ROWPOINTER_DEF;
    UINT4               u4Interface = 0;
    UINT4               u4IfSchedId = 0;
    tSnmpNotifyInfo     SnmpNotifyInfo;

    if (QosSchedulerGetIfIdFromGlobalId
        (u4DiffServSchedulerId, &u4Interface, &u4IfSchedId) == QOS_FAILURE)
    {
        return SNMP_FAILURE;
    }

    pSchedNode = QoSUtlGetSchedNode (u4Interface, u4IfSchedId);
    if (pSchedNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QosGetSchedulerEntry () "
                      " Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }
    if (QosGetTypeIdFromOid (pSetValDiffServSchedulerNext, &u4DSSchedNextType,
                             &u4DSSchedNextId) == QOS_SUCCESS)
    {
        pSchedNode->u4DSSchedNextType = u4DSSchedNextType;
        pSchedNode->u4DSSchedNextId = u4DSSchedNextId;
        SnmpNotifyInfo.pu4ObjectId = DiffServSchedulerNext;
        SnmpNotifyInfo.u4OidLen =
            sizeof (DiffServSchedulerNext) / sizeof (UINT4);
        SnmpNotifyInfo.u4SeqNum = 0;
        SnmpNotifyInfo.u1RowStatus = FALSE;
        SnmpNotifyInfo.pLockPointer = QoSLock;
        SnmpNotifyInfo.pUnLockPointer = QoSUnLock;
        SnmpNotifyInfo.u4Indices = 1;
        SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;

        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%u %o",
                          u4DiffServSchedulerId, pSetValDiffServSchedulerNext));
        return SNMP_SUCCESS;

    }

    return SNMP_FAILURE;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetDiffServSchedulerMethod
 Input       :  The Indices
                DiffServSchedulerId

                The Object 
                setValDiffServSchedulerMethod
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDiffServSchedulerMethod (UINT4 u4DiffServSchedulerId,
                               tSNMP_OID_TYPE * pSetValDiffServSchedulerMethod)
{
    UINT4               u4Interface;
    UINT4               u4IfSchedId;
    INT4                i4SchedulerAlgo = 0;
    UINT4               u4SchedMethod;

    if (QosGetSchedTypeFromOid (pSetValDiffServSchedulerMethod, &u4SchedMethod)
        == QOS_FAILURE)
    {
        return SNMP_FAILURE;
    }

    switch (u4SchedMethod)
    {
        case DS_SCHED_SP:
            i4SchedulerAlgo = QOS_CLI_SCHED_ALGO_SP;
            break;

        case DS_SCHED_WRR:
            i4SchedulerAlgo = QOS_CLI_SCHED_ALGO_WRR;
            break;

        case DS_SCHED_WFQ:
            i4SchedulerAlgo = QOS_CLI_SCHED_ALGO_WFQ;
            break;
        default:
            return SNMP_FAILURE;
    }

    if (QosSchedulerGetIfIdFromGlobalId
        (u4DiffServSchedulerId, &u4Interface, &u4IfSchedId) == QOS_FAILURE)
    {
        return SNMP_FAILURE;
    }

    return (nmhSetFsQoSSchedulerSchedAlgorithm
            (u4Interface, u4IfSchedId, i4SchedulerAlgo));

}

/****************************************************************************
 Function    :  nmhSetDiffServSchedulerMinRate
 Input       :  The Indices
                DiffServSchedulerId

                The Object 
                setValDiffServSchedulerMinRate
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDiffServSchedulerMinRate (UINT4 u4DiffServSchedulerId,
                                tSNMP_OID_TYPE *
                                pSetValDiffServSchedulerMinRate)
{
    tQoSSchedNode      *pSchedNode = NULL;

    UINT4               u4DSSchedulerMinRateId = QOS_ROWPOINTER_DEF;
    UINT4               u4DSSchedularMinRateType = QOS_ROWPOINTER_DEF;
    UINT4               u4Interface = 0;
    UINT4               u4IfSchedId = 0;
    tSnmpNotifyInfo     SnmpNotifyInfo;

    if (QosSchedulerGetIfIdFromGlobalId
        (u4DiffServSchedulerId, &u4Interface, &u4IfSchedId) == QOS_FAILURE)
    {
        return SNMP_FAILURE;
    }

    pSchedNode = QoSUtlGetSchedNode (u4Interface, u4IfSchedId);
    if (pSchedNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QosGetSchedulerEntry () "
                      " Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }
    if (QosGetTypeIdFromOid
        (pSetValDiffServSchedulerMinRate, &u4DSSchedularMinRateType,
         &u4DSSchedulerMinRateId) == QOS_SUCCESS)
    {
        pSchedNode->u4DSSchedulerMinRateType = u4DSSchedularMinRateType;
        pSchedNode->u4DSSchedulerMinRateId = u4DSSchedulerMinRateId;
        SnmpNotifyInfo.pu4ObjectId = DiffServSchedulerMinRate;
        SnmpNotifyInfo.u4OidLen =
            sizeof (DiffServSchedulerMinRate) / sizeof (UINT4);
        SnmpNotifyInfo.u4SeqNum = 0;
        SnmpNotifyInfo.u1RowStatus = FALSE;
        SnmpNotifyInfo.pLockPointer = QoSLock;
        SnmpNotifyInfo.pUnLockPointer = QoSUnLock;
        SnmpNotifyInfo.u4Indices = 1;
        SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;

        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%u %o",
                          u4DiffServSchedulerId,
                          pSetValDiffServSchedulerMinRate));
        return SNMP_SUCCESS;

    }

    /* This can only be 0.0 and we know it. So no need to save */
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetDiffServSchedulerMaxRate
 Input       :  The Indices
                DiffServSchedulerId

                The Object 
                setValDiffServSchedulerMaxRate
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDiffServSchedulerMaxRate (UINT4 u4DiffServSchedulerId,
                                tSNMP_OID_TYPE *
                                pSetValDiffServSchedulerMaxRate)
{
    tQoSSchedNode      *pSchedNode = NULL;

    UINT4               u4DSSchedulerMaxRateId = QOS_ROWPOINTER_DEF;
    UINT4               u4DSSchedularMaxRateType = QOS_ROWPOINTER_DEF;
    UINT4               u4Interface = 0;
    UINT4               u4IfSchedId = 0;
    tSnmpNotifyInfo     SnmpNotifyInfo;

    if (QosSchedulerGetIfIdFromGlobalId
        (u4DiffServSchedulerId, &u4Interface, &u4IfSchedId) == QOS_FAILURE)
    {
        return SNMP_FAILURE;
    }

    pSchedNode = QoSUtlGetSchedNode (u4Interface, u4IfSchedId);
    if (pSchedNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QosGetSchedulerEntry () "
                      " Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }
    if (QosGetTypeIdFromOid
        (pSetValDiffServSchedulerMaxRate, &u4DSSchedularMaxRateType,
         &u4DSSchedulerMaxRateId) == QOS_SUCCESS)
    {
        pSchedNode->u4DSSchedulerMaxRateType = u4DSSchedularMaxRateType;
        pSchedNode->u4DSSchedulerMaxRateId = u4DSSchedulerMaxRateId;
        SnmpNotifyInfo.pu4ObjectId = DiffServSchedulerMaxRate;
        SnmpNotifyInfo.u4OidLen =
            sizeof (DiffServSchedulerMaxRate) / sizeof (UINT4);
        SnmpNotifyInfo.u4SeqNum = 0;
        SnmpNotifyInfo.u1RowStatus = FALSE;
        SnmpNotifyInfo.pLockPointer = QoSLock;
        SnmpNotifyInfo.pUnLockPointer = QoSUnLock;
        SnmpNotifyInfo.u4Indices = 1;
        SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;

        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%u %o",
                          u4DiffServSchedulerId,
                          pSetValDiffServSchedulerMaxRate));
        return SNMP_SUCCESS;

    }

    /* This can only be 0.0 and we know it. So no need to save */
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetDiffServSchedulerStorage
 Input       :  The Indices
                DiffServSchedulerId

                The Object 
                setValDiffServSchedulerStorage
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDiffServSchedulerStorage (UINT4 u4DiffServSchedulerId,
                                INT4 i4SetValDiffServSchedulerStorage)
{
    UNUSED_PARAM (i4SetValDiffServSchedulerStorage);
    UNUSED_PARAM (u4DiffServSchedulerId);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetDiffServSchedulerStatus
 Input       :  The Indices
                DiffServSchedulerId

                The Object 
                setValDiffServSchedulerStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDiffServSchedulerStatus (UINT4 u4DiffServSchedulerId,
                               INT4 i4SetValDiffServSchedulerStatus)
{
    UINT4               u4Interface;
    UINT4               u4IfSchedId;

    if (QosSchedulerGetIfIdFromGlobalId
        (u4DiffServSchedulerId, &u4Interface, &u4IfSchedId) == QOS_FAILURE)
    {
        if (i4SetValDiffServSchedulerStatus == QOS_CREATE_AND_WAIT)
        {
            /* TODO ... We are passing the InterfaceShcdId also as u4DiffServSchedulerId. This has to be implemented 
               with a perport NextFreeSchedulerid 
               But since we are creating this always for 0, we can implement it with our own Schddler Id itself?? 
               Sice no per port would come upto this value hopefuly? */

            /* Update the values for INterface and Schedid since the Get has failed */
            u4Interface = 0;    /* We always create with 0 */
            u4IfSchedId = u4DiffServSchedulerId;

            if (QosAddSchedulerMapEntry (u4DiffServSchedulerId,
                                         u4Interface,
                                         u4IfSchedId) == QOS_FAILURE)
            {
                return SNMP_FAILURE;
            }

        }
        else
        {
            return SNMP_FAILURE;
        }
    }

    return (nmhSetFsQoSSchedulerStatus
            (u4Interface, u4IfSchedId, i4SetValDiffServSchedulerStatus));

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2DiffServSchedulerNext
 Input       :  The Indices
                DiffServSchedulerId

                The Object 
                testValDiffServSchedulerNext
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2DiffServSchedulerNext (UINT4 *pu4ErrorCode,
                                UINT4 u4DiffServSchedulerId,
                                tSNMP_OID_TYPE * pTestValDiffServSchedulerNext)
{
    UINT4               u4Id = QOS_ROWPOINTER_DEF;
    UINT4               u4Type = QOS_ROWPOINTER_DEF;
    UINT4               u4Interface;
    UINT4               u4IfSchedId;

    if (QosSchedulerGetIfIdFromGlobalId
        (u4DiffServSchedulerId, &u4Interface, &u4IfSchedId) == QOS_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (QosGetTypeIdFromOid (pTestValDiffServSchedulerNext, &u4Type, &u4Id) !=
        QOS_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    if ((u4Type == QOS_ROWPOINTER_DEF) || (u4Id == QOS_ROWPOINTER_DEF))
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2DiffServSchedulerMethod
 Input       :  The Indices
                DiffServSchedulerId

                The Object 
                testValDiffServSchedulerMethod
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2DiffServSchedulerMethod (UINT4 *pu4ErrorCode,
                                  UINT4 u4DiffServSchedulerId,
                                  tSNMP_OID_TYPE *
                                  pTestValDiffServSchedulerMethod)
{
    UINT4               u4SchedMethod = QOS_START_INITIAL_INDEX;
    UINT4               u4Interface;
    UINT4               u4IfSchedId;

    if (QosSchedulerGetIfIdFromGlobalId
        (u4DiffServSchedulerId, &u4Interface, &u4IfSchedId) == QOS_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (QosGetSchedTypeFromOid (pTestValDiffServSchedulerMethod, &u4SchedMethod)
        == QOS_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    if ((u4SchedMethod != DS_SCHED_SP) &&
        (u4SchedMethod != DS_SCHED_WRR) && (u4SchedMethod != DS_SCHED_WFQ))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2DiffServSchedulerMinRate
 Input       :  The Indices
                DiffServSchedulerId

                The Object 
                testValDiffServSchedulerMinRate
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2DiffServSchedulerMinRate (UINT4 *pu4ErrorCode,
                                   UINT4 u4DiffServSchedulerId,
                                   tSNMP_OID_TYPE *
                                   pTestValDiffServSchedulerMinRate)
{
    UINT4               u4Id = QOS_ROWPOINTER_DEF;
    UINT4               u4Type = QOS_ROWPOINTER_DEF;
    UINT4               u4Interface;
    UINT4               u4IfSchedId;

    if (QosSchedulerGetIfIdFromGlobalId
        (u4DiffServSchedulerId, &u4Interface, &u4IfSchedId) == QOS_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (QosGetTypeIdFromOid (pTestValDiffServSchedulerMinRate, &u4Type, &u4Id)
        != QOS_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if ((u4Type == QOS_ROWPOINTER_DEF) || (u4Id == QOS_ROWPOINTER_DEF))
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2DiffServSchedulerMaxRate
 Input       :  The Indices
                DiffServSchedulerId

                The Object 
                testValDiffServSchedulerMaxRate
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2DiffServSchedulerMaxRate (UINT4 *pu4ErrorCode,
                                   UINT4 u4DiffServSchedulerId,
                                   tSNMP_OID_TYPE *
                                   pTestValDiffServSchedulerMaxRate)
{
    UINT4               u4Id = QOS_ROWPOINTER_DEF;
    UINT4               u4Type = QOS_ROWPOINTER_DEF;
    UINT4               u4Interface;
    UINT4               u4IfSchedId;

    if (QosSchedulerGetIfIdFromGlobalId
        (u4DiffServSchedulerId, &u4Interface, &u4IfSchedId) == QOS_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (QosGetTypeIdFromOid (pTestValDiffServSchedulerMaxRate, &u4Type, &u4Id)
        != QOS_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if ((u4Type == QOS_ROWPOINTER_DEF) || (u4Id == QOS_ROWPOINTER_DEF))
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2DiffServSchedulerStorage
 Input       :  The Indices
                DiffServSchedulerId

                The Object 
                testValDiffServSchedulerStorage
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2DiffServSchedulerStorage (UINT4 *pu4ErrorCode,
                                   UINT4 u4DiffServSchedulerId,
                                   INT4 i4TestValDiffServSchedulerStorage)
{
    UINT4               u4Interface;
    UINT4               u4IfSchedId;

    UNUSED_PARAM (i4TestValDiffServSchedulerStorage);

    if (QosSchedulerGetIfIdFromGlobalId
        (u4DiffServSchedulerId, &u4Interface, &u4IfSchedId) == QOS_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2DiffServSchedulerStatus
 Input       :  The Indices
                DiffServSchedulerId

                The Object 
                testValDiffServSchedulerStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2DiffServSchedulerStatus (UINT4 *pu4ErrorCode,
                                  UINT4 u4DiffServSchedulerId,
                                  INT4 i4TestValDiffServSchedulerStatus)
{
    UNUSED_PARAM (u4DiffServSchedulerId);
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4TestValDiffServSchedulerStatus);

    if ((i4TestValDiffServSchedulerStatus == QOS_CREATE_AND_WAIT) &&
        (QOS_SCHED_COUNT >= QOS_SCHED_TBL_MAX_ENTRIES))
    {
        return (SNMP_FAILURE);
    }

    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2DiffServSchedulerTable
 Input       :  The Indices
                DiffServSchedulerId
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2DiffServSchedulerTable (UINT4 *pu4ErrorCode,
                                tSnmpIndexList * pSnmpIndexList,
                                tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetDiffServMinRateNextFree
 Input       :  The Indices

                The Object 
                retValDiffServMinRateNextFree
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDiffServMinRateNextFree (UINT4 *pu4RetValDiffServMinRateNextFree)
{
    if (gQoSGlobalInfo.eSysControl != QOS_SYS_CNTL_START)
    {
        QOS_TRC (MGMT_TRC, QOS_MODULE_SHUTDOW_ERROR_MSG);
        *pu4RetValDiffServMinRateNextFree = 0;
        return SNMP_SUCCESS;
    }
    *pu4RetValDiffServMinRateNextFree = QOS_MINRATE_NEXT_FREE ();
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : DiffServMinRateTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceDiffServMinRateTable
 Input       :  The Indices
                DiffServMinRateId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceDiffServMinRateTable (UINT4 u4DiffServMinRateId)
{
    /* Shape template table does not have any validate routine. TODO */
    return
        nmhValidateIndexInstanceFsQoSShapeTemplateTable (u4DiffServMinRateId);
}

/****************************************************************************
 Function    :  nmhGetFirstIndexDiffServMinRateTable
 Input       :  The Indices
                DiffServMinRateId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexDiffServMinRateTable (UINT4 *pu4DiffServMinRateId)
{
    return (nmhGetFirstIndexFsQoSShapeTemplateTable (pu4DiffServMinRateId));
}

/****************************************************************************
 Function    :  nmhGetNextIndexDiffServMinRateTable
 Input       :  The Indices
                DiffServMinRateId
                nextDiffServMinRateId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexDiffServMinRateTable (UINT4 u4DiffServMinRateId,
                                     UINT4 *pu4NextDiffServMinRateId)
{
    return (nmhGetNextIndexFsQoSShapeTemplateTable
            (u4DiffServMinRateId, pu4NextDiffServMinRateId));
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetDiffServMinRatePriority
 Input       :  The Indices
                DiffServMinRateId

                The Object 
                retValDiffServMinRatePriority
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDiffServMinRatePriority (UINT4 u4DiffServMinRateId,
                               UINT4 *pu4RetValDiffServMinRatePriority)
{
    /*TODO. We have to get the priority of the Q which is poiting to me. 
     * Dont worry. even if many ques are poiting all will have the same priority
     * since all of them are pointing to the same Minrate and hence wiil have same priority */
    UNUSED_PARAM (u4DiffServMinRateId);
    UNUSED_PARAM (pu4RetValDiffServMinRatePriority);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDiffServMinRateAbsolute
 Input       :  The Indices
                DiffServMinRateId

                The Object 
                retValDiffServMinRateAbsolute
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDiffServMinRateAbsolute (UINT4 u4DiffServMinRateId,
                               UINT4 *pu4RetValDiffServMinRateAbsolute)
{
    return (nmhGetFsQoSShapeTemplateCIR
            (u4DiffServMinRateId, pu4RetValDiffServMinRateAbsolute));
}

/****************************************************************************
 Function    :  nmhGetDiffServMinRateRelative
 Input       :  The Indices
                DiffServMinRateId

                The Object 
                retValDiffServMinRateRelative
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDiffServMinRateRelative (UINT4 u4DiffServMinRateId,
                               UINT4 *pu4RetValDiffServMinRateRelative)
{

    UINT4               u4MinRateAbsolute = 0;
    UINT4               u4Speed = 0;

    nmhGetFsQoSShapeTemplateCIR (u4DiffServMinRateId, &u4MinRateAbsolute);
    CfaGetIfSpeed (1, &u4Speed);

    if (u4Speed != 0)
    {
        *pu4RetValDiffServMinRateRelative = u4MinRateAbsolute / u4Speed;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDiffServMinRateStorage
 Input       :  The Indices
                DiffServMinRateId

                The Object 
                retValDiffServMinRateStorage
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDiffServMinRateStorage (UINT4 u4DiffServMinRateId,
                              INT4 *pi4RetValDiffServMinRateStorage)
{
    UNUSED_PARAM (u4DiffServMinRateId);
    UNUSED_PARAM (pi4RetValDiffServMinRateStorage);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDiffServMinRateStatus
 Input       :  The Indices
                DiffServMinRateId

                The Object 
                retValDiffServMinRateStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDiffServMinRateStatus (UINT4 u4DiffServMinRateId,
                             INT4 *pi4RetValDiffServMinRateStatus)
{

    return (nmhGetFsQoSShapeTemplateStatus
            (u4DiffServMinRateId, pi4RetValDiffServMinRateStatus));
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetDiffServMinRatePriority
 Input       :  The Indices
                DiffServMinRateId

                The Object 
                setValDiffServMinRatePriority
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDiffServMinRatePriority (UINT4 u4DiffServMinRateId,
                               UINT4 u4SetValDiffServMinRatePriority)
{
    UNUSED_PARAM (u4DiffServMinRateId);
    UNUSED_PARAM (u4SetValDiffServMinRatePriority);
    /*TODO. Set the priority of the corresponding QUEUS */
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetDiffServMinRateAbsolute
 Input       :  The Indices
                DiffServMinRateId

                The Object 
                setValDiffServMinRateAbsolute
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDiffServMinRateAbsolute (UINT4 u4DiffServMinRateId,
                               UINT4 u4SetValDiffServMinRateAbsolute)
{
    return (nmhSetFsQoSShapeTemplateCIR
            (u4DiffServMinRateId, u4SetValDiffServMinRateAbsolute));
}

/****************************************************************************
 Function    :  nmhSetDiffServMinRateRelative
 Input       :  The Indices
                DiffServMinRateId

                The Object 
                setValDiffServMinRateRelative
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDiffServMinRateRelative (UINT4 u4DiffServMinRateId,
                               UINT4 u4SetValDiffServMinRateRelative)
{
    UINT4               u4Speed = 0;

    /* We are converting this into CIR. So user can change either relative or absolute 
     * and find the results on both the oobjects */
    CfaGetIfSpeed (1, &u4Speed);
    return (nmhSetFsQoSShapeTemplateCIR
            (u4DiffServMinRateId, (u4SetValDiffServMinRateRelative * u4Speed)));

}

/****************************************************************************
 Function    :  nmhSetDiffServMinRateStorage
 Input       :  The Indices
                DiffServMinRateId

                The Object 
                setValDiffServMinRateStorage
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDiffServMinRateStorage (UINT4 u4DiffServMinRateId,
                              INT4 i4SetValDiffServMinRateStorage)
{
    UNUSED_PARAM (u4DiffServMinRateId);
    UNUSED_PARAM (i4SetValDiffServMinRateStorage);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetDiffServMinRateStatus
 Input       :  The Indices
                DiffServMinRateId

                The Object 
                setValDiffServMinRateStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDiffServMinRateStatus (UINT4 u4DiffServMinRateId,
                             INT4 i4SetValDiffServMinRateStatus)
{
    /* MIN Rate and maxreate both map to the same Shape Template Table
     * so even if one of them is creates, the create and wait will return 
     * failure when the other one is attempted to be created*/

    return (nmhSetFsQoSShapeTemplateStatus
            (u4DiffServMinRateId, i4SetValDiffServMinRateStatus));

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2DiffServMinRatePriority
 Input       :  The Indices
                DiffServMinRateId

                The Object 
                testValDiffServMinRatePriority
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2DiffServMinRatePriority (UINT4 *pu4ErrorCode,
                                  UINT4 u4DiffServMinRateId,
                                  UINT4 u4TestValDiffServMinRatePriority)
{
    UNUSED_PARAM (u4DiffServMinRateId);
    UNUSED_PARAM (u4TestValDiffServMinRatePriority);
    UNUSED_PARAM (pu4ErrorCode);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2DiffServMinRateAbsolute
 Input       :  The Indices
                DiffServMinRateId

                The Object 
                testValDiffServMinRateAbsolute
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2DiffServMinRateAbsolute (UINT4 *pu4ErrorCode,
                                  UINT4 u4DiffServMinRateId,
                                  UINT4 u4TestValDiffServMinRateAbsolute)
{
    return (nmhTestv2FsQoSShapeTemplateCIR (pu4ErrorCode, u4DiffServMinRateId,
                                            u4TestValDiffServMinRateAbsolute));

}

/****************************************************************************
 Function    :  nmhTestv2DiffServMinRateRelative
 Input       :  The Indices
                DiffServMinRateId

                The Object 
                testValDiffServMinRateRelative
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2DiffServMinRateRelative (UINT4 *pu4ErrorCode,
                                  UINT4 u4DiffServMinRateId,
                                  UINT4 u4TestValDiffServMinRateRelative)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (u4DiffServMinRateId);
    UNUSED_PARAM (u4TestValDiffServMinRateRelative);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2DiffServMinRateStorage
 Input       :  The Indices
                DiffServMinRateId

                The Object 
                testValDiffServMinRateStorage
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2DiffServMinRateStorage (UINT4 *pu4ErrorCode, UINT4 u4DiffServMinRateId,
                                 INT4 i4TestValDiffServMinRateStorage)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (u4DiffServMinRateId);
    UNUSED_PARAM (i4TestValDiffServMinRateStorage);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2DiffServMinRateStatus
 Input       :  The Indices
                DiffServMinRateId

                The Object 
                testValDiffServMinRateStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2DiffServMinRateStatus (UINT4 *pu4ErrorCode, UINT4 u4DiffServMinRateId,
                                INT4 i4TestValDiffServMinRateStatus)
{
    if ((i4TestValDiffServMinRateStatus == QOS_CREATE_AND_WAIT) &&
        (QOS_MINRATE_COUNT >= QOS_SHAPE_TEMP_TBL_MAX_ENTRIES))
    {
        return (SNMP_FAILURE);
    }

    return (nmhTestv2FsQoSShapeTemplateStatus
            (pu4ErrorCode, u4DiffServMinRateId,
             i4TestValDiffServMinRateStatus));
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2DiffServMinRateTable
 Input       :  The Indices
                DiffServMinRateId
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2DiffServMinRateTable (UINT4 *pu4ErrorCode,
                              tSnmpIndexList * pSnmpIndexList,
                              tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetDiffServMaxRateNextFree
 Input       :  The Indices

                The Object 
                retValDiffServMaxRateNextFree
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDiffServMaxRateNextFree (UINT4 *pu4RetValDiffServMaxRateNextFree)
{
    if (gQoSGlobalInfo.eSysControl != QOS_SYS_CNTL_START)
    {
        QOS_TRC (MGMT_TRC, QOS_MODULE_SHUTDOW_ERROR_MSG);
        *pu4RetValDiffServMaxRateNextFree = 0;
        return SNMP_SUCCESS;
    }
    *pu4RetValDiffServMaxRateNextFree = QOS_MAXRATE_NEXT_FREE ();
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : DiffServMaxRateTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceDiffServMaxRateTable
 Input       :  The Indices
                DiffServMaxRateId
                DiffServMaxRateLevel
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceDiffServMaxRateTable (UINT4 u4DiffServMaxRateId,
                                              UINT4 u4DiffServMaxRateLevel)
{
    UNUSED_PARAM (u4DiffServMaxRateLevel);
    return
        nmhValidateIndexInstanceFsQoSShapeTemplateTable (u4DiffServMaxRateId);
}

/****************************************************************************
 Function    :  nmhGetFirstIndexDiffServMaxRateTable
 Input       :  The Indices
                DiffServMaxRateId
                DiffServMaxRateLevel
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexDiffServMaxRateTable (UINT4 *pu4DiffServMaxRateId,
                                      UINT4 *pu4DiffServMaxRateLevel)
{
    if (nmhGetFirstIndexFsQoSShapeTemplateTable (pu4DiffServMaxRateId) ==
        SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }

    *pu4DiffServMaxRateLevel = 2;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexDiffServMaxRateTable
 Input       :  The Indices
                DiffServMaxRateId
                nextDiffServMaxRateId
                DiffServMaxRateLevel
                nextDiffServMaxRateLevel
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexDiffServMaxRateTable (UINT4 u4DiffServMaxRateId,
                                     UINT4 *pu4NextDiffServMaxRateId,
                                     UINT4 u4DiffServMaxRateLevel,
                                     UINT4 *pu4NextDiffServMaxRateLevel)
{
    UNUSED_PARAM (u4DiffServMaxRateLevel);
    *pu4NextDiffServMaxRateLevel = 2;

    return (nmhGetNextIndexFsQoSShapeTemplateTable
            (u4DiffServMaxRateId, pu4NextDiffServMaxRateId));
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetDiffServMaxRateAbsolute
 Input       :  The Indices
                DiffServMaxRateId
                DiffServMaxRateLevel

                The Object 
                retValDiffServMaxRateAbsolute
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDiffServMaxRateAbsolute (UINT4 u4DiffServMaxRateId,
                               UINT4 u4DiffServMaxRateLevel,
                               UINT4 *pu4RetValDiffServMaxRateAbsolute)
{
    if (u4DiffServMaxRateLevel != 2)
    {
        return SNMP_FAILURE;
    }
    return nmhGetFsQoSShapeTemplateEIR (u4DiffServMaxRateId,
                                        pu4RetValDiffServMaxRateAbsolute);
}

/****************************************************************************
 Function    :  nmhGetDiffServMaxRateRelative
 Input       :  The Indices
                DiffServMaxRateId
                DiffServMaxRateLevel

                The Object 
                retValDiffServMaxRateRelative
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDiffServMaxRateRelative (UINT4 u4DiffServMaxRateId,
                               UINT4 u4DiffServMaxRateLevel,
                               UINT4 *pu4RetValDiffServMaxRateRelative)
{
    UINT4               u4Speed = 0;
    UINT4               u4MaxRateAbsolute = 0;

    if ((nmhGetFsQoSShapeTemplateEIR (u4DiffServMaxRateId, &u4MaxRateAbsolute)
         == SNMP_FAILURE) || (u4DiffServMaxRateLevel != 2))
    {
        return SNMP_FAILURE;
    }
    CfaGetIfSpeed (1, &u4Speed);

    if (u4Speed != 0)
    {
        *pu4RetValDiffServMaxRateRelative = u4MaxRateAbsolute / u4Speed;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDiffServMaxRateThreshold
 Input       :  The Indices
                DiffServMaxRateId
                DiffServMaxRateLevel

                The Object 
                retValDiffServMaxRateThreshold
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDiffServMaxRateThreshold (UINT4 u4DiffServMaxRateId,
                                UINT4 u4DiffServMaxRateLevel,
                                INT4 *pi4RetValDiffServMaxRateThreshold)
{
    if (u4DiffServMaxRateLevel != 2)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsQoSShapeTemplateEBS
            (u4DiffServMaxRateId, (UINT4 *) pi4RetValDiffServMaxRateThreshold));

}

/****************************************************************************
 Function    :  nmhGetDiffServMaxRateStorage
 Input       :  The Indices
                DiffServMaxRateId
                DiffServMaxRateLevel

                The Object 
                retValDiffServMaxRateStorage
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDiffServMaxRateStorage (UINT4 u4DiffServMaxRateId,
                              UINT4 u4DiffServMaxRateLevel,
                              INT4 *pi4RetValDiffServMaxRateStorage)
{
    UNUSED_PARAM (u4DiffServMaxRateId);
    UNUSED_PARAM (u4DiffServMaxRateLevel);
    UNUSED_PARAM (pi4RetValDiffServMaxRateStorage);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDiffServMaxRateStatus
 Input       :  The Indices
                DiffServMaxRateId
                DiffServMaxRateLevel

                The Object 
                retValDiffServMaxRateStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDiffServMaxRateStatus (UINT4 u4DiffServMaxRateId,
                             UINT4 u4DiffServMaxRateLevel,
                             INT4 *pi4RetValDiffServMaxRateStatus)
{
    if (u4DiffServMaxRateLevel != 2)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsQoSShapeTemplateStatus
            (u4DiffServMaxRateId, pi4RetValDiffServMaxRateStatus));
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetDiffServMaxRateAbsolute
 Input       :  The Indices
                DiffServMaxRateId
                DiffServMaxRateLevel

                The Object 
                setValDiffServMaxRateAbsolute
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDiffServMaxRateAbsolute (UINT4 u4DiffServMaxRateId,
                               UINT4 u4DiffServMaxRateLevel,
                               UINT4 u4SetValDiffServMaxRateAbsolute)
{

    if (u4DiffServMaxRateLevel != 2)
    {
        return SNMP_FAILURE;
    }

    return (nmhSetFsQoSShapeTemplateEIR
            (u4DiffServMaxRateId, u4SetValDiffServMaxRateAbsolute));

}

/****************************************************************************
 Function    :  nmhSetDiffServMaxRateRelative
 Input       :  The Indices
                DiffServMaxRateId
                DiffServMaxRateLevel

                The Object 
                setValDiffServMaxRateRelative
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDiffServMaxRateRelative (UINT4 u4DiffServMaxRateId,
                               UINT4 u4DiffServMaxRateLevel,
                               UINT4 u4SetValDiffServMaxRateRelative)
{
    UINT4               u4Speed;

    if (u4DiffServMaxRateLevel != 2)
    {
        return SNMP_FAILURE;
    }

    /* We are converting this into EIR. So user can change either relative or absolute 
     * and find the results on both the oobjects */
    CfaGetIfSpeed (1, &u4Speed);
    return (nmhSetFsQoSShapeTemplateCIR
            (u4DiffServMaxRateId, (u4SetValDiffServMaxRateRelative * u4Speed)));

}

/****************************************************************************
 Function    :  nmhSetDiffServMaxRateThreshold
 Input       :  The Indices
                DiffServMaxRateId
                DiffServMaxRateLevel

                The Object 
                setValDiffServMaxRateThreshold
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDiffServMaxRateThreshold (UINT4 u4DiffServMaxRateId,
                                UINT4 u4DiffServMaxRateLevel,
                                INT4 i4SetValDiffServMaxRateThreshold)
{
    if (u4DiffServMaxRateLevel != 2)
    {
        return SNMP_FAILURE;
    }
    return (nmhSetFsQoSShapeTemplateEBS
            (u4DiffServMaxRateId, i4SetValDiffServMaxRateThreshold));
}

/****************************************************************************
 Function    :  nmhSetDiffServMaxRateStorage
 Input       :  The Indices
                DiffServMaxRateId
                DiffServMaxRateLevel

                The Object 
                setValDiffServMaxRateStorage
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDiffServMaxRateStorage (UINT4 u4DiffServMaxRateId,
                              UINT4 u4DiffServMaxRateLevel,
                              INT4 i4SetValDiffServMaxRateStorage)
{
    UNUSED_PARAM (u4DiffServMaxRateId);
    UNUSED_PARAM (u4DiffServMaxRateLevel);
    UNUSED_PARAM (i4SetValDiffServMaxRateStorage);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetDiffServMaxRateStatus
 Input       :  The Indices
                DiffServMaxRateId
                DiffServMaxRateLevel

                The Object 
                setValDiffServMaxRateStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDiffServMaxRateStatus (UINT4 u4DiffServMaxRateId,
                             UINT4 u4DiffServMaxRateLevel,
                             INT4 i4SetValDiffServMaxRateStatus)
{
    if (u4DiffServMaxRateLevel != 2)
    {
        return SNMP_FAILURE;
    }

    return (nmhSetFsQoSShapeTemplateStatus
            (u4DiffServMaxRateId, i4SetValDiffServMaxRateStatus));
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2DiffServMaxRateAbsolute
 Input       :  The Indices
                DiffServMaxRateId
                DiffServMaxRateLevel

                The Object 
                testValDiffServMaxRateAbsolute
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2DiffServMaxRateAbsolute (UINT4 *pu4ErrorCode,
                                  UINT4 u4DiffServMaxRateId,
                                  UINT4 u4DiffServMaxRateLevel,
                                  UINT4 u4TestValDiffServMaxRateAbsolute)
{
    UNUSED_PARAM (u4DiffServMaxRateLevel);
    return (nmhTestv2FsQoSShapeTemplateEIR (pu4ErrorCode, u4DiffServMaxRateId,
                                            u4TestValDiffServMaxRateAbsolute));
}

/****************************************************************************
 Function    :  nmhTestv2DiffServMaxRateRelative
 Input       :  The Indices
                DiffServMaxRateId
                DiffServMaxRateLevel

                The Object 
                testValDiffServMaxRateRelative
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2DiffServMaxRateRelative (UINT4 *pu4ErrorCode,
                                  UINT4 u4DiffServMaxRateId,
                                  UINT4 u4DiffServMaxRateLevel,
                                  UINT4 u4TestValDiffServMaxRateRelative)
{
    UNUSED_PARAM (u4DiffServMaxRateLevel);
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (u4DiffServMaxRateId);
    UNUSED_PARAM (u4TestValDiffServMaxRateRelative);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2DiffServMaxRateThreshold
 Input       :  The Indices
                DiffServMaxRateId
                DiffServMaxRateLevel

                The Object 
                testValDiffServMaxRateThreshold
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2DiffServMaxRateThreshold (UINT4 *pu4ErrorCode,
                                   UINT4 u4DiffServMaxRateId,
                                   UINT4 u4DiffServMaxRateLevel,
                                   INT4 i4TestValDiffServMaxRateThreshold)
{
    UNUSED_PARAM (u4DiffServMaxRateLevel);
    return (nmhTestv2FsQoSShapeTemplateEBS (pu4ErrorCode, u4DiffServMaxRateId,
                                            i4TestValDiffServMaxRateThreshold));

}

/****************************************************************************
 Function    :  nmhTestv2DiffServMaxRateStorage
 Input       :  The Indices
                DiffServMaxRateId
                DiffServMaxRateLevel

                The Object 
                testValDiffServMaxRateStorage
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2DiffServMaxRateStorage (UINT4 *pu4ErrorCode, UINT4 u4DiffServMaxRateId,
                                 UINT4 u4DiffServMaxRateLevel,
                                 INT4 i4TestValDiffServMaxRateStorage)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (u4DiffServMaxRateLevel);
    UNUSED_PARAM (u4DiffServMaxRateId);
    UNUSED_PARAM (i4TestValDiffServMaxRateStorage);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2DiffServMaxRateStatus
 Input       :  The Indices
                DiffServMaxRateId
                DiffServMaxRateLevel

                The Object 
                testValDiffServMaxRateStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2DiffServMaxRateStatus (UINT4 *pu4ErrorCode, UINT4 u4DiffServMaxRateId,
                                UINT4 u4DiffServMaxRateLevel,
                                INT4 i4TestValDiffServMaxRateStatus)
{
    if (u4DiffServMaxRateLevel != 2)
    {
        return SNMP_FAILURE;
    }

    if ((i4TestValDiffServMaxRateStatus == QOS_CREATE_AND_WAIT) &&
        (QOS_MAXRATE_COUNT >= QOS_SHAPE_TEMP_TBL_MAX_ENTRIES))
    {
        return (SNMP_FAILURE);
    }

    return (nmhTestv2FsQoSShapeTemplateStatus
            (pu4ErrorCode, u4DiffServMaxRateId,
             i4TestValDiffServMaxRateStatus));
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2DiffServMaxRateTable
 Input       :  The Indices
                DiffServMaxRateId
                DiffServMaxRateLevel
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2DiffServMaxRateTable (UINT4 *pu4ErrorCode,
                              tSnmpIndexList * pSnmpIndexList,
                              tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}
