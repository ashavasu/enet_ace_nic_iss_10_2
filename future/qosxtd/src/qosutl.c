/* $Id: qosutl.c,v 1.91 2018/01/04 09:55:15 siva Exp $ */
/****************************************************************************/
/* Copyright (C) 2007 Aricent Inc . All Rights Reserved                     */
/*                                                                          */
/*  FILE NAME             : qosutl.c                                        */
/*  PRINCIPAL AUTHOR      : Aricent                                         */
/*  SUBSYSTEM NAME        : QoS                                             */
/*  MODULE NAME           : QoS-UTL                                         */
/*  LANGUAGE              : C                                               */
/*  TARGET ENVIRONMENT    : Linux                                           */
/*  DATE OF FIRST RELEASE :                                                 */
/*  AUTHOR                : Aricent                                         */
/*  FUNCTIONS DEFINED(Applicable for Source files) :                        */
/*  DESCRIPTION           : This File contains the utility functions        */
/*                          for the Aricent QoS Module.                     */
/****************************************************************************/
/*                                                                          */
/*  Change History                                                          */
/*  Version               :                                                 */
/*  Date(DD/MM/YYYY)      :                                                 */
/*  Modified by           :                                                 */
/*  Description of change :                                                 */
/****************************************************************************/
#ifndef _QOS_UTL_C_
#define _QOS_UTL_C_

#include "qosinc.h"
#include "qosoid.h"

/*****************************************************************************/
/*                          COMMON FUNCTIONS                                 */
/*****************************************************************************/

/*****************************************************************************/
/* Function Name      : QoSUtlValidateTableName                              */
/* Description        : This function is used validate the Given Table Name  */
/*                      String. Set the ErrorCode accordingly.               */
/*                      1.It check the Length of the String, it should be    */
/*                        1 to 31 Characters. ERR = WRONG_LENGTH             */
/*                      2.It check the Chars of  the String, it can be       */
/*                        ALPAH, DIGIT , "_" ,"-". ERR = WRONG_VALUE         */
/* Input(s)           : pSNMPOctSrt - Pointer to SNMP OCT STR.               */
/*                      pu4ErrorCode - Pointer to ErrorCode.                 */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : QOS_SUCCESS /QOS_FAILURE                             */
/* Called By          : SNMP                                                 */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QoSUtlValidateTableName (tSNMP_OCTET_STRING_TYPE * pSNMPOctSrt,
                         UINT4 *pu4ErrorCode)
{
    UINT1              *pu1String = NULL;
    INT4                i4StrLen = 0;

    if ((pSNMPOctSrt == NULL) || (pSNMPOctSrt->pu1_OctetList == NULL))
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : ERROR: Name String is NULL.\r\n",
                      __FUNCTION__);

        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;

        return (QOS_FAILURE);
    }

    pu1String = pSNMPOctSrt->pu1_OctetList;
    i4StrLen = pSNMPOctSrt->i4_Length;

    /* Check Length */
    if ((i4StrLen < 1) || (i4StrLen >= QOS_MAX_TABLE_NAME))
    {
        QOS_TRC_ARG3 (MGMT_TRC, "%s : Table name length %d is out of Range."
                      " The Range Should be (1-%d). \r\n", __FUNCTION__,
                      i4StrLen, (QOS_MAX_TABLE_NAME - 1));

        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;

        return (QOS_FAILURE);
    }

    /* Check Char. */
    while (i4StrLen > 0)
    {
        if ((isalpha (*pu1String) || (ISDIGIT (*pu1String)) ||
             ((*pu1String) == '-') || ((*pu1String) == '_')) == 0)

        {
            QOS_TRC_ARG2 (MGMT_TRC, "%s : Table name  Invalid %c char."
                          "Valid Char = (A-Z, a-z, 0-9, _, - ).\r\n",
                          __FUNCTION__, *pu1String);

            CLI_SET_ERR (QOS_CLI_ERR_TBL_NAME_INVALID);

            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;

            return (QOS_FAILURE);
        }

        pu1String++;
        i4StrLen--;
    }

    return (QOS_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSUtlValidateClass                                  */
/* Description        : This function is used validate the Given Class       */
/*                      is Exists or Not in the Class Map Table.             */
/*                      Set the ErrorCode accordingly.                       */
/* Input(s)           : u4ClassId   - Class Map Id.                          */
/*                      pu4ErrorCode - Pointer to ErrorCode.                 */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : QOS_SUCCESS /QOS_FAILURE                             */
/* Called By          : SNMP                                                 */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QoSUtlValidateClass (UINT4 u4ClassId, UINT4 *pu4ErrorCode)
{
    tQoSClassInfoNode  *pClsInfoNode = NULL;

    pClsInfoNode = QoSUtlGetClassInfoNode (u4ClassId);

    if (pClsInfoNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetClassInfoNode () "
                      "Returns FAILURE. \r\n", __FUNCTION__);

        CLI_SET_ERR (QOS_CLI_ERR_CLS_NOT_CREATED);

        *pu4ErrorCode = SNMP_ERR_NO_CREATION;

        return (QOS_FAILURE);
    }

    if (TMO_SLL_Count (&(pClsInfoNode->SllFltInfo)) < 1)
    {
        QOS_TRC_ARG2 (MGMT_TRC, "%s : Filters Not yet Configured for "
                      " this CLASS  %d. \r\n", __FUNCTION__, u4ClassId);

        CLI_SET_ERR (QOS_CLI_ERR_NO_FILTER);

        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;

        return (QOS_FAILURE);
    }

    return (QOS_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSUtlValidateIfIndex                                */
/* Description        : This function is used validate the Given Interface   */
/*                      with the interface module.                           */
/*                      Set the ErrorCode accordingly.                       */
/* Input(s)           : u4IfIndex   - Interface Number.                      */
/*                      pu4ErrorCode - Pointer to ErrorCode.                 */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : QOS_SUCCESS /QOS_FAILURE                             */
/* Called By          : SNMP                                                 */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QoSUtlValidateIfIndex (UINT4 u4IfIndex, UINT4 *pu4ErrorCode)
{
    /* Check the Interface index */
    if ((u4IfIndex == 0) || (u4IfIndex > SYS_DEF_MAX_PHYSICAL_INTERFACES))
    {
        QOS_TRC_ARG2 (MGMT_TRC, "In %s : Invalid IfIndex %d.\r\n",
                      __FUNCTION__, u4IfIndex);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (QOS_FAILURE);
    }

    return (QOS_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSUtlValidateVlanId                                 */
/* Description        : This function is used validate the Given Vlan Id     */
/*                      with the interface module.                           */
/*                      Set the ErrorCode accordingly.                       */
/* Input(s)           : u4VlanId    - Vland Id.                              */
/*                      pu4ErrorCode - Pointer to ErrorCode.                 */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : QOS_SUCCESS /QOS_FAILURE                             */
/* Called By          : SNMP                                                 */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QoSUtlValidateVlanId (UINT4 u4VlanId, UINT4 *pu4ErrorCode)
{
    /* Check the VlaId is Valid */
    if ((u4VlanId < VLAN_DEV_MIN_VLAN_ID) || (u4VlanId > VLAN_MAX_VLAN_ID_EXT))
    {
        QOS_TRC_ARG2 (MGMT_TRC, "In %s : Invalid Vlan Id %d.\r\n",
                      __FUNCTION__, u4VlanId);

        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;

        CLI_SET_ERR (QOS_CLI_ERR_INVALID_VLAN_INDEX);

        return (QOS_FAILURE);
    }

    return (QOS_SUCCESS);
}

/*****************************************************************************/
/*                     PRIORITY MAP TABLE FUNCTIONS                          */
/*****************************************************************************/

/*****************************************************************************/
/* Function Name      : QoSInPriMapCmpFun                                    */
/* Description        : This function is used as a Compare function in the   */
/*                      RBTree.                                              */
/*                      1.It Compares the PriMapTableId and return the values*/
/* Input(s)           : e1 , e2 -  Elements of RBTree                        */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    :  -1/0/1                                              */
/* Called By          : RBTreeUtility                                        */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QoSInPriMapCmpFun (tRBElem * e1, tRBElem * e2)
{
    tQoSInPriorityMapNode *pInPriMapNode1 = NULL;
    tQoSInPriorityMapNode *pInPriMapNode2 = NULL;

    pInPriMapNode1 = (tQoSInPriorityMapNode *) e1;
    pInPriMapNode2 = (tQoSInPriorityMapNode *) e2;

    /* Compare the InPriMap Id */

    if (pInPriMapNode1->u4Id < pInPriMapNode2->u4Id)
    {
        return (-1);
    }
    else if (pInPriMapNode1->u4Id > pInPriMapNode2->u4Id)
    {
        return (1);
    }

    return (0);
}

INT4
QoSInPriMapUniCmpFun (tRBElem * e1, tRBElem * e2)
{
    tQoSInPriorityMapNode *pInPriMapNode1 = NULL;
    tQoSInPriorityMapNode *pInPriMapNode2 = NULL;

    pInPriMapNode1 = (tQoSInPriorityMapNode *) e1;
    pInPriMapNode2 = (tQoSInPriorityMapNode *) e2;

    /* Compare the Incoming Interface */
    if (pInPriMapNode1->u4IfIndex < pInPriMapNode2->u4IfIndex)
    {
        return (-1);
    }
    else if (pInPriMapNode1->u4IfIndex > pInPriMapNode2->u4IfIndex)
    {
        return (1);
    }
    else
    {
        /* Compare the Incoming VLAN ID   */
        if (pInPriMapNode1->u2VlanId < pInPriMapNode2->u2VlanId)
        {
            return (-1);
        }
        else if (pInPriMapNode1->u2VlanId > pInPriMapNode2->u2VlanId)
        {
            return (1);
        }
        else
        {
            /* Compare the Incoming priority type */
            if (pInPriMapNode1->u1InPriType < pInPriMapNode2->u1InPriType)
            {
                return (-1);
            }
            else if (pInPriMapNode1->u1InPriType > pInPriMapNode2->u1InPriType)
            {
                return (1);
            }
            else
            {
                /* Compare the Incoming priority Value */
                if (pInPriMapNode1->u1InPriority < pInPriMapNode2->u1InPriority)
                {
                    return (-1);
                }
                else if (pInPriMapNode1->u1InPriority >
                         pInPriMapNode2->u1InPriority)
                {
                    return (1);
                }
                else
                {
                    return (0);
                }
            }
        }
    }
}

/*****************************************************************************/
/* Function Name      : QoSInPriTblRBTFreeNodeFn                             */
/* Description        : This function is used to Delete the Priority Map     */
/*                      Table Entries and Release the Memory to the          */
/*                      respective Mem Pool .                                */
/* Input(s)           : tRBElem     - Pointer to Priority Map Table Entry    */
/*                      u4Arg       - Unused Param                           */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : QOS_SUCCESS /QOS_FAILURE                             */
/* Called By          : RBTreeUtility                                        */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QoSInPriTblRBTFreeNodeFn (tRBElem * pRBElem, UINT4 u4Arg)
{
    UINT4               u4RetStatus = MEM_FAILURE;

    UNUSED_PARAM (u4Arg);

    if (pRBElem == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : InPriTbl Entry is NULL.\r\n",
                      __FUNCTION__);

        return (QOS_FAILURE);
    }

    MEMSET (pRBElem, 0, sizeof (tQoSInPriorityMapNode));

    /* Release the Mem Block */
    u4RetStatus = MemReleaseMemBlock (gQoSGlobalInfo.QoSInPriMapTblMemPoolId,
                                      (UINT1 *) pRBElem);

    if (u4RetStatus != MEM_SUCCESS)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : InPriTbl- "
                      "MemReleaseMemBlock () Failed.\r\n", __FUNCTION__);
    }

    return (QOS_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSUtlGetPriorityMapNode                             */
/* Description        : This function is used to Get the Entry  form  the    */
/*                      Priority Map Table ,it will do the following Action  */
/*                      1. Call RBTreeGet Utility.                           */
/* Input(s)           : u4PriMapId      - Index to InPriMapNode              */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : gQoSGlobalInfo                                       */
/* Global Variables                                                          */
/* Modified           :                                                      */
/* Return Value(s)    : NULL/ Pointer to 'tQoSInPriorityMapNode'             */
/* Called By          : SNMP Low Level                                       */
/* Calling Function   :                                                      */
/*****************************************************************************/
tQoSInPriorityMapNode *
QoSUtlGetPriorityMapNode (UINT4 u4PriMapId)
{
    tQoSInPriorityMapNode TempPriMapNode;
    tQoSInPriorityMapNode *pPriMapNode = NULL;

    MEMSET (&TempPriMapNode, 0, sizeof (tQoSInPriorityMapNode));
    TempPriMapNode.u4Id = u4PriMapId;

    pPriMapNode = (tQoSInPriorityMapNode *)
        RBTreeGet (gQoSGlobalInfo.pRbInPriMapTbl, (tRBElem *) & TempPriMapNode);

    if (pPriMapNode == NULL)
    {
        QOS_TRC_ARG2 (MGMT_TRC, "%s : Priority Map Id %d is not Found in the "
                      "Priority Map Table. \r\n", __FUNCTION__, u4PriMapId);
    }

    return (pPriMapNode);
}

tQoSInPriorityMapNode *
QoSUtlGetPriorityMapUniNode (UINT4 u4IfIndex, UINT2 u2VlanId, UINT1 u1InPri,
                             UINT1 u1InPriType)
{
    tQoSInPriorityMapNode TempPriMapNode;
    tQoSInPriorityMapNode *pPriMapNode = NULL;

    MEMSET (&TempPriMapNode, 0, sizeof (tQoSInPriorityMapNode));

    TempPriMapNode.u4IfIndex = u4IfIndex;
    TempPriMapNode.u2VlanId = u2VlanId;
    TempPriMapNode.u1InPriority = u1InPri;
    TempPriMapNode.u1InPriType = u1InPriType;

    pPriMapNode = (tQoSInPriorityMapNode *)
        RBTreeGet (gQoSGlobalInfo.pRbInPriMapUniTbl,
                   (tRBElem *) & TempPriMapNode);

    if (pPriMapNode == NULL)
    {
        QOS_TRC_ARG5 (MGMT_TRC, "%s :UniquePriMapNode (Inedx %d.%d.%d.%d) "
                      " is not Found in the Priority Map Table. \r\n",
                      __FUNCTION__, u4IfIndex, u2VlanId, u1InPri, u1InPriType);
    }

    return (pPriMapNode);
}

/*****************************************************************************/
/* Function Name      : QoSCreatePriMapTblEntry                              */
/* Description        : This function is used to Create an Entry in   the    */
/*                      Priority Map Table ,it will do the following Action  */
/*                      1. Allocate a memory block for the New Entry         */
/*                      2. Initialize the New Entry                          */
/*                      3. Add this New Entry into the Priority Map Table    */
/* Input(s)           : u4PriMapId      - Index to InPriMapNode              */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : gQoSGlobalInfo                                       */
/* Global Variables                                                          */
/* Modified           : gQoSGlobalInfo                                       */
/* Return Value(s)    : NULL / Pointer to  InPriMapNode                      */
/* Called By          : SNMP Low Level                                       */
/* Calling Function   :                                                      */
/*****************************************************************************/
tQoSInPriorityMapNode *
QoSCreatePriMapTblEntry (UINT4 u4PriMapId)
{
    UINT4               u4RetStatus = QOS_FAILURE;
    tQoSInPriorityMapNode *pNewInPriMapNode = NULL;

    /* 1. Allocate a memory block for the New Entry */
    pNewInPriMapNode = (tQoSInPriorityMapNode *)
        MemAllocMemBlk (gQoSGlobalInfo.QoSInPriMapTblMemPoolId);
    if (pNewInPriMapNode == NULL)
    {
        QOS_TRC_ARG2 (MGMT_TRC, "In %s : MemAllocMemBlk () Failed ,"
                      "for Priority Map Id %d.\r\n", __FUNCTION__, u4PriMapId);
        return (NULL);
    }

    /* 2. Initialize the New Entry */
    MEMSET (pNewInPriMapNode, 0, sizeof (tQoSInPriorityMapNode));
    SPRINTF ((CHR1 *) pNewInPriMapNode->au1Name, "PRI-%u", u4PriMapId);

    pNewInPriMapNode->u4Id = u4PriMapId;
    pNewInPriMapNode->u1ConfStatus = QOS_PRI_MAP_TBL_CONF_STATUS_SYS;
    pNewInPriMapNode->u1RegenInnerPriority = QOS_PRI_MAP_TBL_INVLD_INREGPRI;
    pNewInPriMapNode->u1Status = NOT_READY;

    /* 3. Add this New Entry into the Priority Map Table */
    u4RetStatus = RBTreeAdd (gQoSGlobalInfo.pRbInPriMapTbl,
                             (tRBElem *) pNewInPriMapNode);
    if (u4RetStatus != RB_SUCCESS)
    {
        u4RetStatus =
            MemReleaseMemBlock (gQoSGlobalInfo.QoSInPriMapTblMemPoolId,
                                (UINT1 *) pNewInPriMapNode);

        if (u4RetStatus == MEM_FAILURE)
        {
            QOS_TRC_ARG2 (MGMT_TRC, "In %s : MemReleaseMemBlock () Failed ,"
                          "for PriMapId %d. \r\n", __FUNCTION__, u4PriMapId);

            return (NULL);
        }

        QOS_TRC_ARG2 (MGMT_TRC, "In %s : RBTreeAdd () Failed ,"
                      "for PriMapId %d. \r\n", __FUNCTION__, u4PriMapId);
        return (NULL);
    }

    return (pNewInPriMapNode);
}

INT4
QoSAddPriMapUniTblEntry (tQoSInPriorityMapNode * pPriMapNode)
{
    tQoSInPriorityMapNode *pTmpPriMapNode = NULL;
    UINT4               u4RetStatus = QOS_FAILURE;

    /* 1. Get this Entry from the Priority Map Unique Table
     * if it fails add it.*/
    pTmpPriMapNode = RBTreeGet (gQoSGlobalInfo.pRbInPriMapUniTbl,
                                (tRBElem *) pPriMapNode);
    if (pTmpPriMapNode == NULL)
    {
        /* 2. Add this New Entry into the Priority Map Unique Table */
        u4RetStatus = RBTreeAdd (gQoSGlobalInfo.pRbInPriMapUniTbl,
                                 (tRBElem *) pPriMapNode);
        if (u4RetStatus != RB_SUCCESS)
        {
            return (QOS_FAILURE);
        }
    }
    return (QOS_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSDeletePriMapTblEntry                              */
/* Description        : This function is used to Delete an Entry from the    */
/*                      Priority Map Table ,it will do the following Action  */
/*                      1. Remove the Entry from the Priority Map Table      */
/*                      2. Clear the Removed Entry                           */
/*                      3. Release the Removed Entry's memory                */
/* Input(s)           : pInPriMapNode   - Pointer to InPriMapNode            */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : gQoSGlobalInfo                                       */
/* Global Variables                                                          */
/* Modified           : gQoSGlobalInfo                                       */
/* Return Value(s)    : QOS_SUCCESS or QOS_FAILURE                           */
/* Called By          : SNMP Low Level                                       */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QoSDeletePriMapUniTblEntry (tQoSInPriorityMapNode * pPriMapNode)
{
    tQoSInPriorityMapNode *pTmpPriMapNode = NULL;

    pTmpPriMapNode = RBTreeGet (gQoSGlobalInfo.pRbInPriMapUniTbl,
                                (tRBElem *) pPriMapNode);
    if (pTmpPriMapNode != NULL)
    {
        pTmpPriMapNode = RBTreeRem (gQoSGlobalInfo.pRbInPriMapUniTbl,
                                    (tRBElem *) pPriMapNode);
        if (pTmpPriMapNode == NULL)
        {
            QOS_TRC_ARG1 (MGMT_TRC, "In %s : RBTreeRem () Failed for "
                          "UniqueNode. \r\n", __FUNCTION__);
            return (QOS_FAILURE);
        }
    }
    return (QOS_SUCCESS);
}

INT4
QoSDeletePriMapTblEntry (tQoSInPriorityMapNode * pInPriMapNode)
{
    UINT4               u4RetStatus = QOS_FAILURE;
    tQoSInPriorityMapNode *pDelInPriMapNode = NULL;

    if (pInPriMapNode == NULL)
    {
        return (QOS_FAILURE);
    }

    /* 1. Remove Entry from the Priority Map Table */
    pDelInPriMapNode = RBTreeRem (gQoSGlobalInfo.pRbInPriMapTbl,
                                  (tRBElem *) pInPriMapNode);
    if (pDelInPriMapNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : RBTreeRem () Failed. \r\n",
                      __FUNCTION__);

        return (QOS_FAILURE);
    }

    /* 1a. Remove Entry from the Priority Map Unique Table */
    u4RetStatus = QoSDeletePriMapUniTblEntry (pInPriMapNode);
    if (u4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSDeletePriMapUniTblEntry () "
                      "Failed for UniqueNode. \r\n", __FUNCTION__);
        return (QOS_FAILURE);
    }

    /* 2. Clear the Removed Entry */
    MEMSET (pDelInPriMapNode, 0, sizeof (tQoSInPriorityMapNode));

    /* 3. Release the Removed Entry's memory */
    u4RetStatus = MemReleaseMemBlock (gQoSGlobalInfo.QoSInPriMapTblMemPoolId,
                                      (UINT1 *) pDelInPriMapNode);
    if (u4RetStatus == MEM_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : MemReleaseMemBlock () Failed. \r\n",
                      __FUNCTION__);

        return (QOS_FAILURE);
    }

    return (QOS_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSGetNextInPriMapTblEntryIndex                      */
/* Description        : This function is used to Get the Next Entry from the */
/*                      Priority Map Table ,it will do the following Action  */
/*                      1. If u4CurrentIndex is 0 then  GetFirxtIndex.       */
/*                      2. If u4CurrentIndex is not 0 then  GetNextIndex.    */
/* Input(s)           : u4CurrentIndex  - Current index id                   */
/*                      pu4NextIndex    - Pointer to NextIndex Id            */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : gQoSGlobalInfo                                       */
/* Global Variables                                                          */
/* Modified           : gQoSGlobalInfo                                       */
/* Return Value(s)    : QOS_SUCCESS or QOS_FAILURE                           */
/* Called By          : SNMP Low Level                                       */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QoSGetNextInPriMapTblEntryIndex (UINT4 u4CurrentIndex, UINT4 *pu4NextIndex)
{
    tQoSInPriorityMapNode *pInPriMapNode = NULL;
    tQoSInPriorityMapNode InPriMapNode;

    if (u4CurrentIndex == 0)
    {
        pInPriMapNode = (tQoSInPriorityMapNode *)
            RBTreeGetFirst (gQoSGlobalInfo.pRbInPriMapTbl);
    }
    else
    {
        InPriMapNode.u4Id = u4CurrentIndex;
        pInPriMapNode = (tQoSInPriorityMapNode *)
            RBTreeGetNext (gQoSGlobalInfo.pRbInPriMapTbl,
                           &InPriMapNode, QoSInPriMapCmpFun);
    }

    if (pInPriMapNode == NULL)
    {
        QOS_TRC_ARG2 (MGMT_TRC, "In %s : RBTreeGetNext () Failed, "
                      " for PriMapId %d.\r\n", __FUNCTION__, u4CurrentIndex);

        return (QOS_FAILURE);
    }

    *pu4NextIndex = pInPriMapNode->u4Id;

    return (QOS_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSUtlValidatePriMapTblIdxInst                       */
/* Description        : This function is used to Validate the Given Index    */
/*                      instance in the Priority Map Table, if it is ACTIVE  */
/*                      return Failure Success otherwise.                    */
/* Input(s)           : u4FsQoSPriorityMapID - PriMapID                      */
/*                      pu4ErrorCode     - Pointer to Error Code             */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : gQoSGlobalInfo                                       */
/* Global Variables                                                          */
/* Modified           : gQoSGlobalInfo                                       */
/* Return Value(s)    : QOS_SUCCESS or QOS_FAILURE                           */
/* Called By          : SNMP Low Level                                       */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QoSUtlValidatePriMapTblIdxInst (UINT4 u4FsQoSPriorityMapID, UINT4 *pu4ErrorCode)
{
    INT4                i4RetStatus = 0;
    tQoSInPriorityMapNode *pPriMapNode = NULL;

    if (gQoSGlobalInfo.eSysControl != QOS_SYS_CNTL_START)
    {
        QOS_TRC (MGMT_TRC, QOS_MODULE_SHUTDOW_ERROR_MSG);

        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;

        CLI_SET_ERR (QOS_CLI_ERR_MODULE_SHUTDOW);

        return (QOS_FAILURE);
    }

    /* Check the Range of Index Values (Min-Max) */
    i4RetStatus = QOS_CHECK_PRI_MAP_TBL_INDEX_RANGE (u4FsQoSPriorityMapID);

    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG3 (MGMT_TRC, "%s : Priority Map Id %d is out of Range."
                      " The Range Should be (1-%d). \r\n", __FUNCTION__,
                      u4FsQoSPriorityMapID, QOS_PRI_MAP_TBL_MAX_INDEX_RANGE);

        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;

        CLI_SET_ERR (QOS_CLI_ERR_PRI_MAP_RANGE);

        return (QOS_FAILURE);
    }

    pPriMapNode = QoSUtlGetPriorityMapNode (u4FsQoSPriorityMapID);

    if (pPriMapNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetPriorityMapNode () Returns "
                      "FAILURE. \r\n", __FUNCTION__);

        *pu4ErrorCode = SNMP_ERR_NO_CREATION;

        CLI_SET_ERR (QOS_CLI_ERR_PRI_MAP_NO_ENTRY);

        return (QOS_FAILURE);
    }

    if (pPriMapNode->u1Status == ACTIVE)
    {
        QOS_TRC (MGMT_TRC, "Can not change the value in ACTIVE State.\r\n");

        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;

        CLI_SET_ERR (QOS_CLI_ERR_STATUS_ACTIVE);

        return (QOS_FAILURE);
    }

    return (QOS_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSIsDefPriorityMapTblEntry                          */
/* Description        : This function is used to Validate the Given Index    */
/*                      instance is the default  Priority Map Id.If not      */
/*                      present  return Failure Success otherwise.           */
/* Input(s)           : tQoSInPriorityMapNode- pPriMapNode                   */
/*                                                                           */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/* Return Value(s)    : QOS_SUCCESS or QOS_FAILURE                           */
/* Called By          : SNMP Low Level                                       */
/* Calling Function   :                                                      */
/*****************************************************************************/

INT4
QoSIsDefPriorityMapTblEntry (tQoSInPriorityMapNode * pPriMapNode)
{
    if (pPriMapNode != NULL)
    {
        if ((pPriMapNode->u4Id > 0) &&
            (pPriMapNode->u4Id <= QOS_PM_TBL_DEF_ENTRY_MAX + 1))
        {
            return (QOS_SUCCESS);
        }
    }

    return (QOS_FAILURE);
}

/*****************************************************************************
*                      VLAN QUEUEING TABLE FUNCTIONS                         */
/******************************************************************************/

/*****************************************************************************/
/* Function Name      : QoSVlanQMapCmpFun                                    */
/* Description        : This function is used as a Compare function in the   */
/*                      RBTree.                                              */
/*                      1.It Compares the VlanMapTableId and return the values*/
/* Input(s)           : e1 , e2 -  Elements of RBTree                        */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    :  -1/0/1                                              */
/* Called By          : RBTreeUtility                                        */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QoSVlanQMapCmpFun (tRBElem * e1, tRBElem * e2)
{
    tQoSInVlanMapNode  *pInVlanQMapNode1 = NULL;
    tQoSInVlanMapNode  *pInVlanQMapNode2 = NULL;

    pInVlanQMapNode1 = (tQoSInVlanMapNode *) e1;
    pInVlanQMapNode2 = (tQoSInVlanMapNode *) e2;

    /* Compare the InVlanMap Id */
    if (pInVlanQMapNode1->u4Id < pInVlanQMapNode2->u4Id)
    {
        return (-1);
    }
    else if (pInVlanQMapNode1->u4Id > pInVlanQMapNode2->u4Id)
    {
        return (1);
    }
    return (0);
}

/*****************************************************************************/
/* Function Name      : QoSCreateVlanQMapTblEntry                            */
/* Description        : This function is used to Create an Entry in   the    */
/*                      Vlan Map Table ,it will do the following Action      */
/*                      1. Allocate a memory block for the New Entry         */
/*                      2. Initialize the New Entry                          */
/*                      3. Add this New Entry into the Priority Map Table    */
/* Input(s)           : u4VlanQMapId      - Index to InVlanQMapNode          */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : gQoSGlobalInfo                                       */
/* Global Variables                                                          */
/* Modified           : gQoSGlobalInfo                                       */
/* Return Value(s)    : NULL / Pointer to  InPriMapNode                      */
/* Called By          : SNMP Low Level                                       */
/* Calling Function   :                                                      */
/*****************************************************************************/
tQoSInVlanMapNode  *
QoSCreateVlanQMapTblEntry (UINT4 u4VlanQMapId)
{
    UINT4               u4RetStatus = QOS_FAILURE;
    tQoSInVlanMapNode  *pNewVlanQMapNode = NULL;

    /* 1. Allocate a memory block for the New Entry */
    pNewVlanQMapNode = (tQoSInVlanMapNode *)
        MemAllocMemBlk (gQoSGlobalInfo.QoSVlanQueueingMapTblMemPoolId);
    if (pNewVlanQMapNode == NULL)
    {
        QOS_TRC_ARG2 (MGMT_TRC, "In %s : MemAllocMemBlk () Failed ,"
                      "for Vlan Map Id %d.\r\n", __FUNCTION__, u4VlanQMapId);
        return (NULL);
    }

    /* 2. Initialize the New Entry */
    MEMSET (pNewVlanQMapNode, 0, sizeof (tQoSInVlanMapNode));

    pNewVlanQMapNode->u4Id = u4VlanQMapId;
    pNewVlanQMapNode->u1Status = NOT_READY;

    /* 3. Add this New Entry into the Vlan Map Table */
    u4RetStatus = RBTreeAdd (gQoSGlobalInfo.pRbVlanQueueingMapTbl,
                             (tRBElem *) pNewVlanQMapNode);
    if (u4RetStatus != RB_SUCCESS)
    {
        u4RetStatus =
            MemReleaseMemBlock (gQoSGlobalInfo.QoSVlanQueueingMapTblMemPoolId,
                                (UINT1 *) pNewVlanQMapNode);

        if (u4RetStatus == MEM_FAILURE)
        {
            QOS_TRC_ARG2 (MGMT_TRC, "In %s : MemReleaseMemBlock () Failed ,"
                          "for VlanMapId %d. \r\n", __FUNCTION__, u4VlanQMapId);

            return (NULL);
        }

        QOS_TRC_ARG2 (MGMT_TRC, "In %s : RBTreeAdd () Failed ,"
                      "for VlanMapId %d. \r\n", __FUNCTION__, u4VlanQMapId);
        return (NULL);
    }
    return (pNewVlanQMapNode);
}

/*****************************************************************************/
/* Function Name      : QoSDeleteVlanQMapTblEntry                            */
/* Description        : This function is used to Delete pVlanQMapNode from   */
/*                      RBTree                                               */
/* Input(s)           : pVlanQMapNode     - pointer to tQoSInVlanMapNode     */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : gQoSGlobalInfo                                       */
/* Global Variables                                                          */
/* Modified           : gQoSGlobalInfo                                       */
/* Return Value(s)    : NULL / Pointer to  InPriMapNode                      */
/* Called By          : SNMP Low Level                                       */
/* Calling Function   :                                                      */
/*****************************************************************************/

INT4
QoSDeleteVlanQMapTblEntry (tQoSInVlanMapNode * pVlanQMapNode)
{
    UINT4               u4RetStatus = QOS_FAILURE;
    tQoSInVlanMapNode  *pDelVlanQMapNode = NULL;

    if (pVlanQMapNode == NULL)
    {
        return (QOS_FAILURE);
    }

    /* 1. Remove Entry from the vlan Map Table */
    pDelVlanQMapNode = RBTreeRem (gQoSGlobalInfo.pRbVlanQueueingMapTbl,
                                  (tRBElem *) pVlanQMapNode);
    if (pDelVlanQMapNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : RBTreeRem () Failed. \r\n",
                      __FUNCTION__);
        return (QOS_FAILURE);
    }

    /* 2. Clear the Removed Entry */
    MEMSET (pDelVlanQMapNode, 0, sizeof (tQoSInVlanMapNode));

    /* 3. Release the Removed Entry's memory */
    u4RetStatus =
        MemReleaseMemBlock (gQoSGlobalInfo.QoSVlanQueueingMapTblMemPoolId,
                            (UINT1 *) pDelVlanQMapNode);
    if (u4RetStatus == MEM_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : MemReleaseMemBlock () Failed. \r\n",
                      __FUNCTION__);
        return (QOS_FAILURE);
    }
    return (QOS_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSUtlValidateVlanQMapTblIdxInst                     */
/* Description        : This function is used to Validate the Given Index    */
/*                      instance in the Vlan Map Table, if it is ACTIVE      */
/*                      return Failure Success otherwise.                    */
/* Input(s)           : u4FsQoSVlanQMapID - VlanQMapID                       */
/*                      pu4ErrorCode     - Pointer to Error Code             */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : gQoSGlobalInfo                                       */
/* Global Variables                                                          */
/* Modified           : gQoSGlobalInfo                                       */
/* Return Value(s)    : QOS_SUCCESS or QOS_FAILURE                           */
/* Called By          : SNMP Low Level                                       */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QoSUtlValidateVlanQMapTblIdxInst (UINT4 u4FsQoSVlanQMapID, UINT4 *pu4ErrorCode)
{
    INT4                i4RetStatus = 0;
    tQoSInVlanMapNode  *pVlanQMapNode = NULL;

    if (gQoSGlobalInfo.eSysControl != QOS_SYS_CNTL_START)
    {
        QOS_TRC (MGMT_TRC, QOS_MODULE_SHUTDOW_ERROR_MSG);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (QOS_CLI_ERR_MODULE_SHUTDOW);
        return (QOS_FAILURE);
    }

    /* Check the Range of Index Values (Min-Max) */
    i4RetStatus = QOS_CHECK_VLAN_MAP_TBL_INDEX_RANGE (u4FsQoSVlanQMapID);

    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG3 (MGMT_TRC, "%s : Vlan Map Id %d is out of Range."
                      " The Range Should be (1-%d). \r\n", __FUNCTION__,
                      u4FsQoSVlanQMapID, QOS_VLAN_MAP_TBL_MAX_INDEX_RANGE);

        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (QOS_CLI_ERR_VLAN_MAP_RANGE);
        return (QOS_FAILURE);
    }

    pVlanQMapNode = QoSUtlGetVlanQMapNode (u4FsQoSVlanQMapID);

    if (pVlanQMapNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetVlanQMapNode () Returns "
                      "FAILURE. \r\n", __FUNCTION__);

        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (QOS_CLI_ERR_VLAN_MAP_NO_ENTRY);
        return (QOS_FAILURE);
    }

    if (pVlanQMapNode->u1Status == ACTIVE)
    {
        QOS_TRC (MGMT_TRC, "Can not change the value in ACTIVE State.\r\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (QOS_CLI_ERR_STATUS_ACTIVE);
        return (QOS_FAILURE);
    }
    return (QOS_SUCCESS);
}

/*****************************************************************************/
/*                     END OF VLAN QUEUEING MAP TABLE FUNCTIONS              */
/*****************************************************************************/

/*****************************************************************************/
/*                     CLASS MAP TABLE FUNCTIONS                             */
/*****************************************************************************/

/*****************************************************************************/
/* Function Name      : QoSClsMapCmpFun                                      */
/* Description        : This function is used as a Compare function in the   */
/*                      RBTree.                                              */
/*                      1.It Compares the ClsMapTableId and return the values*/
/* Input(s)           : e1 , e2 -  Elements of RBTree                        */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    :  -1/0/1                                              */
/* Called By          : RBTreeUtility                                        */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QoSClsMapCmpFun (tRBElem * e1, tRBElem * e2)
{
    tQoSClassMapNode   *pClassMapNode1 = NULL;
    tQoSClassMapNode   *pClassMapNode2 = NULL;

    pClassMapNode1 = (tQoSClassMapNode *) e1;
    pClassMapNode2 = (tQoSClassMapNode *) e2;

    /* Compare the Class Map Id */

    if (pClassMapNode1->u4Id < pClassMapNode2->u4Id)
    {
        return (-1);
    }
    else if (pClassMapNode1->u4Id > pClassMapNode2->u4Id)
    {
        return (1);
    }

    return (0);
}

/*****************************************************************************/
/* Function Name      : QoSClsMapTblRBTFreeNodeFn                            */
/* Description        : This function is used to Delete the  Class Map Table */
/*                      Entries and Release the Memory to the respective     */
/*                      Mem Pool .                                           */
/* Input(s)           : tRBElem     - Pointer to Class Map Table Entry       */
/*                      u4Arg       - Unused Param                           */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : QOS_SUCCESS /QOS_FAILURE                             */
/* Called By          : RBTreeUtility                                        */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QoSClsMapTblRBTFreeNodeFn (tRBElem * pRBElem, UINT4 u4Arg)
{
    tQoSClassMapNode   *pClsMapNode = NULL;
    UINT4               u4RetStatus = MEM_FAILURE;

    UNUSED_PARAM (u4Arg);

    if (pRBElem == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : ClsMapTbl Entry is NULL.\r\n",
                      __FUNCTION__);

        return (QOS_FAILURE);
    }

    pClsMapNode = (tQoSClassMapNode *) pRBElem;
    /* 2. Decrement the RefCount of L2andL3 Filter Entry */
    if ((pClsMapNode->u4L2FilterId) != 0)
    {
        u4RetStatus = QoSUtlUpdateFilterRefCount (pClsMapNode->u4L2FilterId,
                                                  QOS_L2FILTER, QOS_DECR);
        if (u4RetStatus == QOS_FAILURE)
        {
            QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlUpdateFilterRefCount () "
                          ": L2 - DECR Returns FAILURE. \r\n", __FUNCTION__);
            return (QOS_FAILURE);
        }
    }

    if ((pClsMapNode->u4L3FilterId) != 0)
    {
        u4RetStatus = QoSUtlUpdateFilterRefCount (pClsMapNode->u4L3FilterId,
                                                  QOS_L3FILTER, QOS_DECR);
        if (u4RetStatus == QOS_FAILURE)
        {
            QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlUpdateFilterRefCount () "
                          ": L3 - DECR Returns FAILURE. \r\n", __FUNCTION__);
            return (QOS_FAILURE);
        }
    }

    MEMSET (pRBElem, 0, sizeof (tQoSClassMapNode));

    /* Release the Mem Block */
    u4RetStatus = MemReleaseMemBlock (gQoSGlobalInfo.QoSClsMapTblMemPoolId,
                                      (UINT1 *) pRBElem);

    if (u4RetStatus != MEM_SUCCESS)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : ClassTbl- "
                      "MemReleaseMemBlock () Failed.\r\n", __FUNCTION__);
    }

    return (QOS_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSUtlGetClassMapNode                                */
/* Description        : This function is used to Get the Entry  form  the    */
/*                      Class Map Table ,it will do the following Action     */
/*                      1. Call RBTreeGet Utility.                           */
/* Input(s)           : u4ClsMapId      - Index to ClsMapNode                */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : gQoSGlobalInfo                                       */
/* Global Variables                                                          */
/* Modified           :                                                      */
/* Return Value(s)    : NULL/ Pointer to 'tQoSClassMapNode'                  */
/* Called By          : SNMP Low Level                                       */
/* Calling Function   :                                                      */
/*****************************************************************************/
tQoSClassMapNode   *
QoSUtlGetClassMapNode (UINT4 u4ClsMapId)
{
    tQoSClassMapNode    TempClsMapNode;
    tQoSClassMapNode   *pClsMapNode = NULL;

    MEMSET (&TempClsMapNode, 0, sizeof (tQoSClassMapNode));
    TempClsMapNode.u4Id = u4ClsMapId;

    pClsMapNode = (tQoSClassMapNode *)
        RBTreeGet (gQoSGlobalInfo.pRbClsMapTbl, (tRBElem *) & TempClsMapNode);

    if (pClsMapNode == NULL)
    {
        QOS_TRC_ARG2 (MGMT_TRC, "%s : Class Map Id %d is not Found in the "
                      "Class Map Table. \r\n", __FUNCTION__, u4ClsMapId);
    }

    return (pClsMapNode);

}

/*****************************************************************************/
/* Function Name      : QoSCreateClsMapTblEntry                              */
/* Description        : This function is used to Create an Entry in the      */
/*                      Class Map Table ,it will do the following Action     */
/*                      1. Allocate a memory block for the New Entry         */
/*                      2. Initialize the New Entry                          */
/*                      3. Add this New Entry into the Class Map Table       */
/* Input(s)           : u4ClsMapId      - Index to ClsMapNode                */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : gQoSGlobalInfo                                       */
/* Global Variables                                                          */
/* Modified           : gQoSGlobalInfo                                       */
/* Return Value(s)    : NULL / Pointer to  ClsMapNode                        */
/* Called By          : SNMP Low Level                                       */
/* Calling Function   :                                                      */
/*****************************************************************************/
tQoSClassMapNode   *
QoSCreateClsMapTblEntry (UINT4 u4ClsMapId)
{
    UINT4               u4RetStatus = QOS_FAILURE;
    tQoSClassMapNode   *pNewClsMapNode = NULL;

    /* 1. Allocate a memory block for the New Entry */
    pNewClsMapNode = (tQoSClassMapNode *)
        MemAllocMemBlk (gQoSGlobalInfo.QoSClsMapTblMemPoolId);
    if (pNewClsMapNode == NULL)
    {
        QOS_TRC_ARG2 (MGMT_TRC, "In %s : MemAllocMemBlk () Failed, "
                      "for ClsMapId %d. \r\n", __FUNCTION__, u4ClsMapId);

        return (NULL);
    }

    /* 2. Initialize the New Entry */
    MEMSET (pNewClsMapNode, 0, sizeof (tQoSClassMapNode));

    SPRINTF ((CHR1 *) pNewClsMapNode->au1Name, "CLASSMAP-%u", u4ClsMapId);
    pNewClsMapNode->u4Id = u4ClsMapId;
    pNewClsMapNode->u1PreColor = QOS_CLS_DEFAULT_PRE_COLOR;
    pNewClsMapNode->u1Status = NOT_READY;
    pNewClsMapNode->u4ClassId = QOS_MIN_RATE_ABSOLUTE_DEFAULT;
    pNewClsMapNode->u4VlanMapId = QOS_CLASSTBL_VLAN_MAP_DEFAULT_VAL;
    pNewClsMapNode->b1IsClsMapConfFromExt = FALSE;    /* Flag will be set to true by other modules */

    /* 3. Add this New Entry into the Class Map Table */
    u4RetStatus = RBTreeAdd (gQoSGlobalInfo.pRbClsMapTbl,
                             (tRBElem *) pNewClsMapNode);
    if (u4RetStatus != RB_SUCCESS)
    {
        u4RetStatus = MemReleaseMemBlock (gQoSGlobalInfo.QoSClsMapTblMemPoolId,
                                          (UINT1 *) pNewClsMapNode);
        if (u4RetStatus == MEM_FAILURE)
        {
            QOS_TRC_ARG2 (MGMT_TRC, "In %s : MemReleaseMemBlock () Failed ,"
                          "for ClsMapId %d. \r\n", __FUNCTION__, u4ClsMapId);

            return (NULL);
        }

        QOS_TRC_ARG2 (MGMT_TRC, "In %s : RBTreeAdd () Failed ,"
                      "for ClsMapId %d. \r\n", __FUNCTION__, u4ClsMapId);

        return (NULL);
    }

    QosUpdDSClfrElementNextFree (u4ClsMapId, QOS_CREATE_AND_WAIT);

    return (pNewClsMapNode);
}

/*****************************************************************************/
/* Function Name      : QoSValidateClsMapTblEntry                            */
/* Description        : This function is used to Check the Mandatory Fields  */
/*                      of Class Map Table's Entry, it will do the           */
/*                      following Action                                     */
/*                      1. ClassId should not be NULL (0).                   */
/*                      2. At least any one of the following entry needs to  */
/*                         be configured - L2Filter , L3Filter , PriMapId    */
/*                      3. A Entry can Either L2 and/or L3 Filters or        */
/*                         PriMapId Should be Configured .                   */
/*                      4. All Mandatory Fields are set,set the RowStatus as */
/*                         NOT_IN_SERVICE State.                             */
/* Input(s)           : pClsMapNode   - Pointer to ClsMapNode                */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : gQoSGlobalInfo                                       */
/* Global Variables                                                          */
/* Modified           : gQoSGlobalInfo                                       */
/* Return Value(s)    : QOS_SUCCESS or QOS_FAILURE                           */
/* Called By          : SNMP Low Level                                       */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QoSValidateClsMapTblEntry (tQoSClassMapNode * pClsMapNode)
{
    /*Check Mandatory Fields */
    if (pClsMapNode == NULL)
    {
        return (QOS_FAILURE);
    }

    if (pClsMapNode->u4ClassId == 0)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "%s : CLASS Should not be 0.\r\n",
                      __FUNCTION__);
        pClsMapNode->u1Status = NOT_READY;
        return (QOS_FAILURE);
    }

    /* L2 and L3 == 0     ---> PMID != 0 */
    /* L2 and/or L3  != 0 ---> PMID == 0 */
    if ((pClsMapNode->u4L2FilterId == 0) && (pClsMapNode->u4L3FilterId == 0))
    {
        if ((pClsMapNode->u4PriorityMapId == 0)
            && (pClsMapNode->u4VlanMapId == 0))
        {
            QOS_TRC_ARG1 (MGMT_TRC, "%s :L2 Filter and/or L3 Filter or Priority"
                          "Map Id or VlanMap Id Should be Configured .\r\n",
                          __FUNCTION__);
            pClsMapNode->u1Status = NOT_READY;
            return (QOS_FAILURE);
        }
    }
    else
    {
        if (pClsMapNode->u4PriorityMapId != 0)
        {
            QOS_TRC_ARG1 (MGMT_TRC, "%s :Either L2 and/or L3 Filters or "
                          "PriMapId is  Configured with this class-map.\r\n",
                          __FUNCTION__);

            pClsMapNode->u1Status = NOT_READY;
            return (QOS_FAILURE);
        }
        if (pClsMapNode->u4VlanMapId != 0)
        {
            QOS_TRC_ARG1 (MGMT_TRC, "%s :Either L2 and/or L3 Filters or "
                          "PriMapId or Vlan-map is  Configured with this class-map.\r\n",
                          __FUNCTION__);

            pClsMapNode->u1Status = NOT_READY;
            return (QOS_FAILURE);
        }
    }

    pClsMapNode->u1Status = NOT_IN_SERVICE;

    return (QOS_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSDeleteClsMapTblEntry                              */
/* Description        : This function is used to Delete an Entry from the    */
/*                      Classs Map Table ,it will do the following Action    */
/*                      1. Remove the Entry from the Class Map Table         */
/*                      2. Decrement the RefCount of Priority Map Table Entry*/
/*                      3. Clear the Removed Entry                           */
/*                      4. Release the Removed Entry's memory                */
/* Input(s)           : pClsMapNode   - Pointer to ClsMapNode                */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : gQoSGlobalInfo                                       */
/* Global Variables                                                          */
/* Modified           : gQoSGlobalInfo                                       */
/* Return Value(s)    : QOS_SUCCESS or QOS_FAILURE                           */
/* Called By          : SNMP Low Level                                       */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QoSDeleteClsMapTblEntry (tQoSClassMapNode * pClsMapNode)
{
    UINT4               u4RetStatus = QOS_FAILURE;
    tQoSClassMapNode   *pDelClsMapNode = NULL;
    tQoSInPriorityMapNode *pPriMapNode = NULL;
    UINT4               u4ClsMapId = 0;

    u4ClsMapId = pClsMapNode->u4Id;

    /* 1. Remove Entry from the Class Map Table */
    pDelClsMapNode = RBTreeRem (gQoSGlobalInfo.pRbClsMapTbl,
                                (tRBElem *) pClsMapNode);
    if (pDelClsMapNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : RBTreeRem () Failed.\r\n",
                      __FUNCTION__);

        return (QOS_FAILURE);
    }

    /* 2. Decrement the RefCount of Priority Map Table Entry */
    if ((pDelClsMapNode->u4PriorityMapId) != 0)
    {
        pPriMapNode =
            QoSUtlGetPriorityMapNode (pDelClsMapNode->u4PriorityMapId);

        if (pPriMapNode == NULL)
        {
            QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetPriorityMapNode () "
                          "Returns FAILURE. \r\n", __FUNCTION__);

            return (QOS_FAILURE);
        }

        pPriMapNode->u4RefCount = (pPriMapNode->u4RefCount) - 1;
    }

    if ((pDelClsMapNode->u4L2FilterId) != 0)
    {
        u4RetStatus = QoSUtlUpdateFilterRefCount (pDelClsMapNode->u4L2FilterId,
                                                  QOS_L2FILTER, QOS_DECR);
        if (u4RetStatus == QOS_FAILURE)
        {
            QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlUpdateFilterRefCount () "
                          ": L2 - DECR Returns FAILURE. \r\n", __FUNCTION__);
            return (QOS_FAILURE);
        }
    }

    if ((pDelClsMapNode->u4L3FilterId) != 0)
    {
        u4RetStatus = QoSUtlUpdateFilterRefCount (pDelClsMapNode->u4L3FilterId,
                                                  QOS_L3FILTER, QOS_DECR);
        if (u4RetStatus == QOS_FAILURE)
        {
            QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlUpdateFilterRefCount () "
                          ": L3 - DECR Returns FAILURE. \r\n", __FUNCTION__);
            return (QOS_FAILURE);
        }
    }

    /* 3. Clear the Removed Entry */
    MEMSET (pDelClsMapNode, 0, sizeof (tQoSClassMapNode));

    /* 4. Release the Removed Entry's memory */
    u4RetStatus = MemReleaseMemBlock (gQoSGlobalInfo.QoSClsMapTblMemPoolId,
                                      (UINT1 *) pDelClsMapNode);
    if (u4RetStatus == MEM_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : MemReleaseMemBlock () Failed.\r\n",
                      __FUNCTION__);

        return (QOS_FAILURE);
    }

    QosUpdDSClfrElementNextFree (u4ClsMapId, QOS_DESTROY);

    return (QOS_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSGetNextClsMapTblEntryIndex                        */
/* Description        : This function is used to Get the Next Entry from the */
/*                      Class Map Table ,it will do the following Action     */
/*                      1. If u4CurrentIndex is 0 then  GetFirxtIndex.       */
/*                      2. If u4CurrentIndex is not 0 then  GetNextIndex.    */
/* Input(s)           : u4CurrentIndex  - Current index id                   */
/*                      pu4NextIndex    - Pointer to NextIndex Id            */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : gQoSGlobalInfo                                       */
/* Global Variables                                                          */
/* Modified           : gQoSGlobalInfo                                       */
/* Return Value(s)    : QOS_SUCCESS or QOS_FAILURE                           */
/* Called By          : SNMP Low Level                                       */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QoSGetNextClsMapTblEntryIndex (UINT4 u4CurrentIndex, UINT4 *pu4NextIndex)
{
    tQoSClassMapNode   *pClsMapNode = NULL;
    tQoSClassMapNode    ClsMapNode;

    if (u4CurrentIndex == 0)
    {
        pClsMapNode = (tQoSClassMapNode *)
            RBTreeGetFirst (gQoSGlobalInfo.pRbClsMapTbl);
    }
    else
    {
        ClsMapNode.u4Id = u4CurrentIndex;
        pClsMapNode = (tQoSClassMapNode *)
            RBTreeGetNext (gQoSGlobalInfo.pRbClsMapTbl,
                           &ClsMapNode, QoSClsMapCmpFun);
    }

    if (pClsMapNode == NULL)
    {
        QOS_TRC_ARG2 (MGMT_TRC, "In %s : RBTreeGetNext () Failed, "
                      " for ClsMapId %d.\r\n", __FUNCTION__, u4CurrentIndex);

        return (QOS_FAILURE);
    }

    *pu4NextIndex = pClsMapNode->u4Id;

    return (QOS_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSUtlIsFilterMapToClsMapEntry                       */
/* Description        : This function is used to verify whether the given    */
/*                      L2 or L3 Filter is associated to any class map entry.*/
/*                                                                           */
/* Input(s)           : u1FilterType - QOS_L2FILTER / QOS_L3FILTER           */
/*                      u4FilterId   - L2 or L3 Filter ID to verify          */
/*                                                                           */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : gQoSGlobalInfo                                       */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : TRUE  - If filter is mapped to Class Map             */
/*                      FALSE - If filter is not mapped to Class Map         */
/*****************************************************************************/
INT4
QoSUtlIsFilterMapToClsMapEntry (UINT1 u1FilterType, UINT4 u4FilterId)
{
    tQoSClassMapNode   *pClsMapNode = NULL;

    pClsMapNode =
        (tQoSClassMapNode *) RBTreeGetFirst (gQoSGlobalInfo.pRbClsMapTbl);

    while (pClsMapNode != NULL)
    {
        /* Verify FilterId is associated to this Class Map node. */
        if (u1FilterType == QOS_L2FILTER)
        {
            if (pClsMapNode->u4L2FilterId == u4FilterId)
            {
                return TRUE;
            }
        }
        else if (u1FilterType == QOS_L3FILTER)
        {
            if (pClsMapNode->u4L3FilterId == u4FilterId)
            {
                return TRUE;
            }
        }

        pClsMapNode = (tQoSClassMapNode *)
            RBTreeGetNext (gQoSGlobalInfo.pRbClsMapTbl,
                           pClsMapNode, QoSClsMapCmpFun);
    }

    return FALSE;
}

/*****************************************************************************/
/* Function Name      : QoSUtlValidateL2Filter                               */
/* Description        : This function is used to validate the Given L2 Filter*/
/*                      Id is present in ACL Table.It Should be Active.      */
/* Input(s)           : u4L2FilterId    - L2 Filter Id                       */
/*                      pu4ErrorCode    - Pointer to Error Code              */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : gQoSGlobalInfo                                       */
/* Global Variables                                                          */
/* Modified           : gQoSGlobalInfo                                       */
/* Return Value(s)    : QOS_SUCCESS or QOS_FAILURE                           */
/* Called By          : SNMP Low Level                                       */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QoSUtlValidateL2Filter (UINT4 u4L2FilterId, UINT4 *pu4ErrorCode)
{
    tIssL2FilterEntry  *pL2FilterEntry = NULL;

    /* Check the L2 Filter Id */
    pL2FilterEntry = IssGetL2FilterTableEntry (u4L2FilterId);

    if (pL2FilterEntry == NULL)
    {
        QOS_TRC_ARG2 (MGMT_TRC, "In %s : ERROR IssGetL2FilterTableEntry ()"
                      " Failed, for L2Filter Id %d. \r\n", __FUNCTION__,
                      u4L2FilterId);

        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;

        CLI_SET_ERR (QOS_CLI_ERR_L2_FILTER_ID_INVALID);

        return (QOS_FAILURE);
    }

    if (pL2FilterEntry->IssL2FilterAction == ISS_DROP)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QOS cannot be applied for "
                      "a deny filter. \r\n", __FUNCTION__);

        CLI_SET_ERR (QOS_CLI_ERR_CLS_MAP_INVALID_FILTER_ACTION);

        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;

        return (SNMP_FAILURE);
    }

    if (pL2FilterEntry->u1IssL2FilterStatus != ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;

        CLI_SET_ERR (QOS_CLI_ERR_L2_FILTER_ID_NOT_ACTIVE);

        QOS_TRC_ARG2 (MGMT_TRC, "%s : The L2 Filter Id %d. not Active."
                      "\r\n", __FUNCTION__, u4L2FilterId);

        return (QOS_FAILURE);
    }
    return (QOS_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSUtlValidateL3Filter                               */
/* Description        : This function is used to validate the Given L3 Filter*/
/*                      Id is present in ACL Table.It Should be Active.      */
/* Input(s)           : u4L3FilterId    - L3 Filter Id                       */
/*                      pu4ErrorCode    - Pointer to Error Code              */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : gQoSGlobalInfo                                       */
/* Global Variables                                                          */
/* Modified           : gQoSGlobalInfo                                       */
/* Return Value(s)    : QOS_SUCCESS or QOS_FAILURE                           */
/* Called By          : SNMP Low Level                                       */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QoSUtlValidateL3Filter (UINT4 u4L3FilterId, UINT4 *pu4ErrorCode)
{
    tIssL3FilterEntry  *pL3FilterEntry = NULL;

    /* Check the L3 Filter Id */
    pL3FilterEntry = IssGetL3FilterTableEntry (u4L3FilterId);

    if (pL3FilterEntry == NULL)
    {
        QOS_TRC_ARG2 (MGMT_TRC, "In %s : ERROR IssGetL3FilterTableEntry ()"
                      " Failed, for L3Filter Id %d. \r\n", __FUNCTION__,
                      u4L3FilterId);

        CLI_SET_ERR (QOS_CLI_ERR_L3_FILTER_ID_INVALID);

        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;

        return (QOS_FAILURE);
    }

    if (pL3FilterEntry->IssL3FilterAction == ISS_DROP)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QOS cannot be applied for "
                      "a deny filter. \r\n", __FUNCTION__);

        CLI_SET_ERR (QOS_CLI_ERR_CLS_MAP_INVALID_FILTER_ACTION);

        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;

        return (SNMP_FAILURE);
    }

    if (pL3FilterEntry->u1IssL3FilterStatus != ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;

        QOS_TRC_ARG2 (MGMT_TRC, "%s : The L3 Filter Id %d. not Active."
                      "\r\n", __FUNCTION__, u4L3FilterId);

        CLI_SET_ERR (QOS_CLI_ERR_L3_FILTER_ID_NOT_ACTIVE);

        return (QOS_FAILURE);
    }
    return (QOS_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSUtlValidatePriorityMapId                          */
/* Description        : This function is used to validate the Given Priority */
/*                      Map Id in the Priority Map Table, and check the      */
/*                      status of the Entry is ACTIVE or Not.                */
/* Input(s)           : u4PriMapId      - Priority Map Id Value              */
/*                      pu4ErrorCode - Pointer to ErrorCode.                 */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : gQoSGlobalInfo                                       */
/* Global Variables                                                          */
/* Modified           : gQoSGlobalInfo                                       */
/* Return Value(s)    : QOS_SUCCESS or QOS_FAILURE                           */
/* Called By          : SNMP Low Level                                       */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QoSUtlValidatePriorityMapId (UINT4 u4PriMapId, UINT4 *pu4ErrorCode)
{
    tQoSInPriorityMapNode *pPriMapNode = NULL;

    pPriMapNode = QoSUtlGetPriorityMapNode (u4PriMapId);

    if (pPriMapNode == NULL)
    {
        QOS_TRC_ARG2 (MGMT_TRC, "%s : The PriMapId %d. Not yet Configured."
                      "\r\n", __FUNCTION__, u4PriMapId);

        CLI_SET_ERR (QOS_CLI_ERR_PRI_MAP_NO_ENTRY);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (QOS_FAILURE);
    }

    if (pPriMapNode->u1Status != ACTIVE)
    {
        QOS_TRC_ARG2 (MGMT_TRC, "%s : The PriMapId %d. not Active."
                      "\r\n", __FUNCTION__, u4PriMapId);

        CLI_SET_ERR (QOS_CLI_ERR_PRI_MAP_ID_NOT_ACTIVE);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;

        return (QOS_FAILURE);
    }

    return (QOS_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSUtlValidateVlanQMapId                             */
/* Description        : This function is used to validate the Given Vlan     */
/*                      Map Id in the Vlan Map Table, and check the          */
/*                      status of the Entry is ACTIVE or Not.                */
/* Input(s)           : u4VlanQMapId      - Vlan Map Id Value                */
/*                      pu4ErrorCode - Pointer to ErrorCode.                 */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : gQoSGlobalInfo                                       */
/* Global Variables                                                          */
/* Modified           : gQoSGlobalInfo                                       */
/* Return Value(s)    : QOS_SUCCESS or QOS_FAILURE                           */
/* Called By          : SNMP Low Level                                       */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QoSUtlValidateVlanQMapId (UINT4 u4VlanQMapId, UINT4 *pu4ErrorCode)
{
    tQoSInVlanMapNode  *pVlanQMapNode = NULL;

    pVlanQMapNode = QoSUtlGetVlanQMapNode (u4VlanQMapId);

    if (pVlanQMapNode == NULL)
    {
        QOS_TRC_ARG2 (MGMT_TRC, "%s : The u4VlanQMapId %d. Not yet Configured."
                      "\r\n", __FUNCTION__, u4VlanQMapId);
        CLI_SET_ERR (QOS_CLI_ERR_VLAN_MAP_NO_ENTRY);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (QOS_FAILURE);
    }

    if (pVlanQMapNode->u1Status != ACTIVE)
    {
        QOS_TRC_ARG2 (MGMT_TRC, "%s : The VlanQMapId %d. not Active."
                      "\r\n", __FUNCTION__, u4VlanQMapId);
        CLI_SET_ERR (QOS_CLI_ERR_VLAN_MAP_ID_NOT_ACTIVE);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return (QOS_FAILURE);
    }
    return (QOS_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSUtlValidateClsMapTblIdxInst                       */
/* Description        : This function is used to Validate the Given Index    */
/*                      instance in the Class Map Table, if it is ACTIVE     */
/*                      return Failure Success otherwise.                    */
/* Input(s)           : u4FsQoSClassMapId - Class Map ID                     */
/*                      pu4ErrorCode      - Pointer to Error Code            */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : gQoSGlobalInfo                                       */
/* Global Variables                                                          */
/* Modified           : gQoSGlobalInfo                                       */
/* Return Value(s)    : QOS_SUCCESS or QOS_FAILURE                           */
/* Called By          : SNMP Low Level                                       */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QoSUtlValidateClsMapTblIdxInst (UINT4 u4FsQoSClassMapId, UINT4 *pu4ErrorCode)
{
    tQoSClassMapNode   *pClsMapNode = NULL;
    INT4                i4RetStatus = 0;

    if (gQoSGlobalInfo.eSysControl != QOS_SYS_CNTL_START)
    {
        QOS_TRC (MGMT_TRC, QOS_MODULE_SHUTDOW_ERROR_MSG);

        CLI_SET_ERR (QOS_CLI_ERR_MODULE_SHUTDOW);

        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;

        return (QOS_FAILURE);
    }

    /* Check the Range of Index Values (Min-Max) */
    i4RetStatus = QOS_CHECK_CLS_MAP_TBL_INDEX_RANGE (u4FsQoSClassMapId);

    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG3 (MGMT_TRC, "%s : Class Map Id %d is out of Range."
                      " The Range Should be (1-%d). \r\n", __FUNCTION__,
                      u4FsQoSClassMapId, QOS_CLS_MAP_TBL_MAX_INDEX_RANGE);

        CLI_SET_ERR (QOS_CLI_ERR_CLS_MAP_RANGE);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (QOS_FAILURE);
    }

    pClsMapNode = QoSUtlGetClassMapNode (u4FsQoSClassMapId);

    if (pClsMapNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetClassMapNode () "
                      "Returns FAILURE. \r\n", __FUNCTION__);

        CLI_SET_ERR (QOS_CLI_ERR_CLS_MAP_NO_ENTRY);

        *pu4ErrorCode = SNMP_ERR_NO_CREATION;

        return (QOS_FAILURE);
    }

    if (pClsMapNode->u1Status == ACTIVE)
    {
        QOS_TRC (MGMT_TRC, "Can not change the value in ACTIVE State.\r\n");

        CLI_SET_ERR (QOS_CLI_ERR_STATUS_ACTIVE);

        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;

        return (QOS_FAILURE);
    }

    return (QOS_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSIsDefClassMapTblEntry                             */
/* Description        : This function is used to Validate the Given Index    */
/*                      instance is the default  CLass Map Id.If not         */
/*                      present  return Failure Success otherwise.           */
/* Input(s)           : tQoSClassMapNode- pClsMapNode                        */
/*                                                                           */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/* Return Value(s)    : QOS_SUCCESS or QOS_FAILURE                           */
/* Called By          : SNMP Low Level                                       */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QoSIsDefClassMapTblEntry (tQoSClassMapNode * pClsMapNode)
{
    if (pClsMapNode != NULL)
    {
        if ((pClsMapNode->u4Id > 0) &&
            (pClsMapNode->u4Id <= QOS_CM_TBL_DEF_ENTRY_MAX))
        {
            return (QOS_SUCCESS);
        }
    }

    return (QOS_FAILURE);
}

/*****************************************************************************/
/*               CLASS 2 PRIORITY MAP TABLE FUNCTIONS                        */
/*****************************************************************************/

/*****************************************************************************/
/* Function Name      : QoSCls2PriMapCmpFun                                  */
/* Description        : This function is used as a Compare function in the   */
/*                      RBTree.                                              */
/*                      1.It Compares the Cls2PriTableId and return the values*/
/* Input(s)           : e1 , e2 -  Elements of RBTree                        */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    :  -1/0/1                                              */
/* Called By          : RBTreeUtility                                        */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QoSCls2PriMapCmpFun (tRBElem * e1, tRBElem * e2)
{
    tQoSClass2PriorityNode *pCls2PriMapNode1 = NULL;
    tQoSClass2PriorityNode *pCls2PriMapNode2 = NULL;

    pCls2PriMapNode1 = (tQoSClass2PriorityNode *) e1;
    pCls2PriMapNode2 = (tQoSClass2PriorityNode *) e2;

    /* Compare the CLASS */

    if (pCls2PriMapNode1->u4ClassId < pCls2PriMapNode2->u4ClassId)
    {
        return (-1);
    }
    else if (pCls2PriMapNode1->u4ClassId > pCls2PriMapNode2->u4ClassId)
    {
        return (1);
    }

    return (0);
}

/*****************************************************************************/
/* Function Name      : QoSCls2PriTblRBTFreeNodeFn                           */
/* Description        : This function is used to Delete the Cls2Pri Map Table*/
/*                      Entries and Release the Memory to the respective     */
/*                      Mem Pool .                                           */
/* Input(s)           : tRBElem     - Pointer to Cls2PriMap Table Entry      */
/*                      u4Arg       - Unused Param                           */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : QOS_SUCCESS /QOS_FAILURE                             */
/* Called By          : RBTreeUtility                                        */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QoSCls2PriTblRBTFreeNodeFn (tRBElem * pRBElem, UINT4 u4Arg)
{
    UINT4               u4RetStatus = MEM_FAILURE;

    UNUSED_PARAM (u4Arg);

    if (pRBElem == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : Cls2PriMapTbl Entry is NULL.\r\n",
                      __FUNCTION__);

        return (QOS_FAILURE);
    }

    MEMSET (pRBElem, 0, sizeof (tQoSClass2PriorityNode));

    /* Release the Mem Block */
    u4RetStatus = MemReleaseMemBlock (gQoSGlobalInfo.QoSCls2PriMapTblMemPoolId,
                                      (UINT1 *) pRBElem);

    if (u4RetStatus != MEM_SUCCESS)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : Cls2PriTbl- "
                      "MemReleaseMemBlock () Failed.\r\n", __FUNCTION__);
    }

    return (QOS_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSUtlGetClass2PriorityNode                          */
/* Description        : This function is used to Get the Entry  form  the    */
/*                      PriorityToClass Map Table ,                          */
/*                      it will do the following Action                      */
/*                      1. Call RBTreeGet Utility.                           */
/* Input(s)           : u4Cls2PriId      - Index to Cls2PriNode              */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : gQoSGlobalInfo                                       */
/* Global Variables                                                          */
/* Modified           :                                                      */
/* Return Value(s)    : NULL/ Pointer to 'tQoSClass2PriorityNode'            */
/* Called By          : SNMP Low Level                                       */
/* Calling Function   :                                                      */
/*****************************************************************************/
tQoSClass2PriorityNode *
QoSUtlGetClass2PriorityNode (UINT4 u4Cls2PriId)
{
    tQoSClass2PriorityNode TempCls2PriMapNode;
    tQoSClass2PriorityNode *pCls2PriMapNode = NULL;

    MEMSET (&TempCls2PriMapNode, 0, sizeof (tQoSClass2PriorityNode));
    TempCls2PriMapNode.u4ClassId = u4Cls2PriId;

    pCls2PriMapNode = (tQoSClass2PriorityNode *)
        RBTreeGet (gQoSGlobalInfo.pRbCls2PriMapTbl,
                   (tRBElem *) & TempCls2PriMapNode);

    if (pCls2PriMapNode == NULL)
    {
        QOS_TRC_ARG2 (MGMT_TRC, "%s : Cls2PriMapId %d is not Found in the "
                      "Class To Priority Map Table. \r\n", __FUNCTION__,
                      u4Cls2PriId);
    }

    return (pCls2PriMapNode);
}

/*****************************************************************************/
/* Function Name      : QoSCreateCls2PriTblEntry                             */
/* Description        : This function is used to Create an Entry in the Class*/
/*                      ToPriority Map Table ,it will do the following Action*/
/*                      1. Allocate a memory block for the New Entry         */
/*                      2. Initialize the New Entry                          */
/*                      3. Add this New Entry into the ClassToPriorityMap    */
/*                        Table                                              */
/* Input(s)           : u4Cls2PriId      - Index to Cls2PriNode              */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : gQoSGlobalInfo                                       */
/* Global Variables                                                          */
/* Modified           : gQoSGlobalInfo                                       */
/* Return Value(s)    : NULL / Pointer to  Cls2PriNode                       */
/* Called By          : SNMP Low Level                                       */
/* Calling Function   :                                                      */
/*****************************************************************************/
tQoSClass2PriorityNode *
QoSCreateCls2PriTblEntry (UINT4 u4Cls2PriId)
{
    UINT4               u4RetStatus = QOS_FAILURE;
    tQoSClass2PriorityNode *pNewCls2PriNode = NULL;

    /* 1. Allocate a memory block for the New Entry */
    pNewCls2PriNode = (tQoSClass2PriorityNode *)
        MemAllocMemBlk (gQoSGlobalInfo.QoSCls2PriMapTblMemPoolId);
    if (pNewCls2PriNode == NULL)
    {
        QOS_TRC_ARG2 (MGMT_TRC, "In %s : MemAllocMemBlk () Failed, "
                      "for Cls2PriMapId %d. \r\n", __FUNCTION__, u4Cls2PriId);

        return (NULL);
    }

    /* 2. Initialize the New Entry */
    MEMSET (pNewCls2PriNode, 0, sizeof (tQoSClass2PriorityNode));

    SPRINTF ((CHR1 *) pNewCls2PriNode->au1GroupName, "CLASSTOPRI-%u",
             u4Cls2PriId);
    pNewCls2PriNode->u4ClassId = u4Cls2PriId;
    pNewCls2PriNode->u1Status = NOT_READY;

    /* 3. Add this New Entry into the Class Map Table */
    u4RetStatus = RBTreeAdd (gQoSGlobalInfo.pRbCls2PriMapTbl,
                             (tRBElem *) pNewCls2PriNode);
    if (u4RetStatus != RB_SUCCESS)
    {
        u4RetStatus =
            MemReleaseMemBlock (gQoSGlobalInfo.QoSCls2PriMapTblMemPoolId,
                                (UINT1 *) pNewCls2PriNode);

        if (u4RetStatus == MEM_FAILURE)
        {
            QOS_TRC_ARG2 (MGMT_TRC, "In %s : MemReleaseMemBlock () Failed for"
                          " Cls2PriMapId %d. \r\n", __FUNCTION__, u4Cls2PriId);

            return (NULL);
        }

        QOS_TRC_ARG2 (MGMT_TRC, "In %s : RBTreeAdd () Failed ,"
                      "for Cls2PriMapId %d. \r\n", __FUNCTION__, u4Cls2PriId);

        return (NULL);
    }

    return (pNewCls2PriNode);
}

/*****************************************************************************/
/* Function Name      : QoSValidateCls2PriTblEntry                           */
/* Description        : This function is used to Check the Mandatory Fields  */
/*                      of CladssToPriority Map Table's Entry, it will do the*/
/*                      following Action                                     */
/*                      1. NULL Check for ClassId and GroupName.             */
/*                      4. All Mandatory Fields are set,set the RowStatus as */
/*                        NOT_IN_SERVICE                                     */
/* Input(s)           : pCls2PriNode   - Pointer to Cls2PriNode              */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : gQoSGlobalInfo                                       */
/* Global Variables                                                          */
/* Modified           : gQoSGlobalInfo                                       */
/* Return Value(s)    : QOS_SUCCESS or QOS_FAILURE                           */
/* Called By          : SNMP Low Level                                       */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QoSValidateCls2PriTblEntry (tQoSClass2PriorityNode * pCls2PriNode)
{
    if (pCls2PriNode == NULL)
    {
        return (QOS_FAILURE);
    }

    /*Check Mandatory Fields */
    if ((pCls2PriNode->u4ClassId == 0) ||
        (pCls2PriNode->au1GroupName[0] == '\0'))
    {
        QOS_TRC_ARG1 (MGMT_TRC, "%s : Class Id Should not be 0 Or Name Should"
                      " not be NULL .\r\n", __FUNCTION__);
        pCls2PriNode->u1Status = NOT_READY;
        return (QOS_FAILURE);
    }

    if (pCls2PriNode->u1Status != ACTIVE)
    {
        pCls2PriNode->u1Status = NOT_IN_SERVICE;
    }

    return (QOS_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSDeleteCls2PriTblEntry                             */
/* Description        : This function is used to Delete an Entry from the    */
/*                      Class To Priority Map Table ,it will do the following*/
/*                      Action                                               */
/*                      1. Remove the Entry from the Cls2Pri Map Table       */
/*                      2. Clear the Removed Entry                           */
/*                      3. Release the Removed Entry's memory                */
/* Input(s)           : pCls2PriNode   - Pointer to Cls2PriNode              */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : gQoSGlobalInfo                                       */
/* Global Variables                                                          */
/* Modified           : gQoSGlobalInfo                                       */
/* Return Value(s)    : QOS_SUCCESS or QOS_FAILURE                           */
/* Called By          : SNMP Low Level                                       */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QoSDeleteCls2PriTblEntry (tQoSClass2PriorityNode * pCls2PriNode)
{
    tQoSClass2PriorityNode *pDelCls2PriNode = NULL;
    UINT4               u4RetStatus = QOS_FAILURE;

    /* 1. Remove Entry from the Class Map Table */
    pDelCls2PriNode = RBTreeRem (gQoSGlobalInfo.pRbCls2PriMapTbl,
                                 (tRBElem *) pCls2PriNode);
    if (pDelCls2PriNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : RBTreeRem () Failed.\r\n",
                      __FUNCTION__);

        return (QOS_FAILURE);
    }

    /* 2. Clear the Removed Entry */
    MEMSET (pDelCls2PriNode, 0, sizeof (tQoSClass2PriorityNode));

    /* 3. Release the Removed Entry's memory */
    u4RetStatus = MemReleaseMemBlock (gQoSGlobalInfo.QoSCls2PriMapTblMemPoolId,
                                      (UINT1 *) pDelCls2PriNode);
    if (u4RetStatus == MEM_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : MemReleaseMemBlock () Failed.\r\n",
                      __FUNCTION__);

        return (QOS_FAILURE);
    }

    return (QOS_SUCCESS);

}

/*****************************************************************************/
/* Function Name      : QoSGetNextCls2PriTblEntryIndex                       */
/* Description        : This function is used to Get the Next Entry from the */
/*                      ClassToPriority Map Table ,it will do the following  */
/*                      Action                                               */
/*                      1. If u4CurrentIndex is 0 then  GetFirxtIndex.       */
/*                      2. If u4CurrentIndex is not 0 then  GetNextIndex.    */
/* Input(s)           : u4CurrentIndex  - Current index id                   */
/*                      pu4NextIndex    - Pointer to NextIndex Id            */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : gQoSGlobalInfo                                       */
/* Global Variables                                                          */
/* Modified           : gQoSGlobalInfo                                       */
/* Return Value(s)    : QOS_SUCCESS or QOS_FAILURE                           */
/* Called By          : SNMP Low Level                                       */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QoSGetNextCls2PriTblEntryIndex (UINT4 u4CurrentIndex, UINT4 *pu4NextIndex)
{
    tQoSClass2PriorityNode *pCls2PriNode = NULL;
    tQoSClass2PriorityNode Cls2PriNode;

    if (u4CurrentIndex == 0)
    {
        pCls2PriNode = (tQoSClass2PriorityNode *)
            RBTreeGetFirst (gQoSGlobalInfo.pRbCls2PriMapTbl);
    }
    else
    {
        Cls2PriNode.u4ClassId = u4CurrentIndex;

        pCls2PriNode = (tQoSClass2PriorityNode *)
            RBTreeGetNext (gQoSGlobalInfo.pRbCls2PriMapTbl,
                           &Cls2PriNode, QoSCls2PriMapCmpFun);
    }

    if (pCls2PriNode == NULL)
    {
        QOS_TRC_ARG2 (MGMT_TRC, "In %s : RBTreeGetNext () Failed, for "
                      "Cls2PriMapId %d.\r\n", __FUNCTION__, u4CurrentIndex);

        return (QOS_FAILURE);
    }

    *pu4NextIndex = pCls2PriNode->u4ClassId;

    return (QOS_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSUtlValidateCls2PriMapTblIdxInst                   */
/* Description        : This function is used to Validate the Given Index    */
/*                      instance in the ClassToPriority Map Table,           */
/*                      if it is ACTIVE return Failure Success otherwise.    */
/* Input(s)           : u4FsQoSCls2PriCLASS - Class Map ID                   */
/*                      pu4ErrorCode      - Pointer to Error Code            */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : gQoSGlobalInfo                                       */
/* Global Variables                                                          */
/* Modified           : gQoSGlobalInfo                                       */
/* Return Value(s)    : QOS_SUCCESS or QOS_FAILURE                           */
/* Called By          : SNMP Low Level                                       */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QoSUtlValidateCls2PriMapTblIdxInst (UINT4 u4FsQoSClassToPriorityCLASS,
                                    UINT4 *pu4ErrorCode)
{
    tQoSClass2PriorityNode *pCls2PriNode = NULL;

    if (gQoSGlobalInfo.eSysControl != QOS_SYS_CNTL_START)
    {
        QOS_TRC (MGMT_TRC, QOS_MODULE_SHUTDOW_ERROR_MSG);
        CLI_SET_ERR (QOS_CLI_ERR_MODULE_SHUTDOW);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;

        return (QOS_FAILURE);
    }

    pCls2PriNode = QoSUtlGetClass2PriorityNode (u4FsQoSClassToPriorityCLASS);
    if (pCls2PriNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetClass2PriorityNode () "
                      " Returns FAILURE. \r\n", __FUNCTION__);

        CLI_SET_ERR (QOS_CLI_ERR_CLS2PRI_MAP_NO_ENTRY);

        *pu4ErrorCode = SNMP_ERR_NO_CREATION;

        return (QOS_FAILURE);
    }

    return (QOS_SUCCESS);

}

/*****************************************************************************/
/*              STANDARD METER TABLE FUNCTIONS                               */
/*****************************************************************************/

/*****************************************************************************/
/* Function Name      : QoSStdMeterCmpFun                                    */
/* Description        : This function is used as a Compare function in the   */
/*                      RBTree.                                              */
/*                      1.It Compares the MeterTableId and return the values */
/* Input(s)           : e1 , e2 -  Elements of RBTree                        */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    :  -1/0/1                                              */
/* Called By          : RBTreeUtility                                        */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QoSStdMeterCmpFun (tRBElem * e1, tRBElem * e2)
{
    tQosStdMeterEntry  *pMeterNode1 = NULL;
    tQosStdMeterEntry  *pMeterNode2 = NULL;

    pMeterNode1 = (tQosStdMeterEntry *) e1;
    pMeterNode2 = (tQosStdMeterEntry *) e2;

    /* Compare the Meter Table Ids */

    if (pMeterNode1->u4DSMeterId < pMeterNode2->u4DSMeterId)
    {
        return (-1);
    }
    else if (pMeterNode1->u4DSMeterId > pMeterNode2->u4DSMeterId)
    {
        return (1);
    }

    return (0);
}

/*****************************************************************************/
/*                        METER TABLE FUNCTIONS                              */
/*****************************************************************************/

/*****************************************************************************/
/* Function Name      : QoSMeterCmpFun                                       */
/* Description        : This function is used as a Compare function in the   */
/*                      RBTree.                                              */
/*                      1.It Compares the MeterTableId and return the values */
/* Input(s)           : e1 , e2 -  Elements of RBTree                        */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    :  -1/0/1                                              */
/* Called By          : RBTreeUtility                                        */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QoSMeterCmpFun (tRBElem * e1, tRBElem * e2)
{
    tQoSMeterNode      *pMeterNode1 = NULL;
    tQoSMeterNode      *pMeterNode2 = NULL;

    pMeterNode1 = (tQoSMeterNode *) e1;
    pMeterNode2 = (tQoSMeterNode *) e2;

    /* Compare the Meter Table Ids */

    if (pMeterNode1->u4Id < pMeterNode2->u4Id)
    {
        return (-1);
    }
    else if (pMeterNode1->u4Id > pMeterNode2->u4Id)
    {
        return (1);
    }

    return (0);
}

/*****************************************************************************/
/* Function Name      : QoSStdMeterTblRBTFreeNodeFn                          */
/* Description        : This function is used to Delete the Meter Table      */
/*                      Entries and Release the Memory to the respective     */
/*                      Mem Pool .                                           */
/* Input(s)           : tRBElem     - Pointer to Meter Table Entry           */
/*                      u4Arg       - Unused Param                           */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : QOS_SUCCESS /QOS_FAILURE                             */
/* Called By          : RBTreeUtility                                        */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QoSStdMeterTblRBTFreeNodeFn (tRBElem * pRBElem, UINT4 u4Arg)
{
    UINT4               u4RetStatus = MEM_FAILURE;

    UNUSED_PARAM (u4Arg);

    if (pRBElem == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : MeterTbl Entry is NULL.\r\n",
                      __FUNCTION__);

        return (QOS_FAILURE);
    }

    MEMSET (pRBElem, 0, sizeof (tQosStdMeterEntry));

    /* Release the Mem Block */
    u4RetStatus = MemReleaseMemBlock (gQoSGlobalInfo.DsMeterPoolId,
                                      (UINT1 *) pRBElem);

    if (u4RetStatus != MEM_SUCCESS)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : StdMeterTbl- "
                      "MemReleaseMemBlock () Failed.\r\n", __FUNCTION__);
    }

    return (QOS_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSMeterTblRBTFreeNodeFn                             */
/* Description        : This function is used to Delete the Meter Table      */
/*                      Entries and Release the Memory to the respective     */
/*                      Mem Pool .                                           */
/* Input(s)           : tRBElem     - Pointer to Meter Table Entry           */
/*                      u4Arg       - Unused Param                           */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : QOS_SUCCESS /QOS_FAILURE                             */
/* Called By          : RBTreeUtility                                        */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QoSMeterTblRBTFreeNodeFn (tRBElem * pRBElem, UINT4 u4Arg)
{
    UINT4               u4RetStatus = MEM_FAILURE;

    UNUSED_PARAM (u4Arg);

    if (pRBElem == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : MeterTbl Entry is NULL.\r\n",
                      __FUNCTION__);

        return (QOS_FAILURE);
    }

    MEMSET (pRBElem, 0, sizeof (tQoSMeterNode));

    /* Release the Mem Block */
    u4RetStatus = MemReleaseMemBlock (gQoSGlobalInfo.QoSMeterTblMemPoolId,
                                      (UINT1 *) pRBElem);

    if (u4RetStatus != MEM_SUCCESS)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : MeterTbl- "
                      "MemReleaseMemBlock () Failed.\r\n", __FUNCTION__);
    }

    return (QOS_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSUtlGetMeterNode                                   */
/* Description        : This function is used to Get the Entry  form  the    */
/*                      Meter Table ,it will do the following Action         */
/*                      1. Call RBTreeGet Utility.                           */
/* Input(s)           : u4MeterId      - Index to MeterNode                  */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : gQoSGlobalInfo                                       */
/* Global Variables                                                          */
/* Modified           :                                                      */
/* Return Value(s)    : NULL/ Pointer to 'tQoSMeterNode'                     */
/* Called By          : SNMP Low Level                                       */
/* Calling Function   :                                                      */
/*****************************************************************************/
tQoSMeterNode      *
QoSUtlGetMeterNode (UINT4 u4MeterId)
{
    tQoSMeterNode       TempMeterNode;
    tQoSMeterNode      *pMeterNode = NULL;

    MEMSET (&TempMeterNode, 0, sizeof (tQoSMeterNode));
    TempMeterNode.u4Id = u4MeterId;

    pMeterNode = (tQoSMeterNode *) RBTreeGet (gQoSGlobalInfo.pRbMeterTbl,
                                              (tRBElem *) & TempMeterNode);
    if (pMeterNode == NULL)
    {
        QOS_TRC_ARG2 (MGMT_TRC, "%s : Meter Id %d is not Found in the "
                      "Meter Table. \r\n", __FUNCTION__, u4MeterId);
    }

    return (pMeterNode);
}

/*****************************************************************************/
/* Function Name      : QoSUtlGetStdMeterNode                                */
/* Description        : This function is used to Get the Entry  form  the    */
/*                      Meter Table ,it will do the following Action         */
/*                      1. Call RBTreeGet Utility.                           */
/* Input(s)           : u4MeterId      - Index to MeterNode                  */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : gQoSGlobalInfo                                       */
/* Global Variables                                                          */
/* Modified           :                                                      */
/* Return Value(s)    : NULL/ Pointer to 'tQoSMeterNode'                     */
/* Called By          : SNMP Low Level                                       */
/* Calling Function   :                                                      */
/*****************************************************************************/
tQosStdMeterEntry  *
QoSUtlGetStdMeterNode (UINT4 u4MeterId)
{
    tQosStdMeterEntry   TempMeterNode;
    tQosStdMeterEntry  *pMeterNode = NULL;

    MEMSET (&TempMeterNode, 0, sizeof (tQosStdMeterEntry));
    TempMeterNode.u4DSMeterId = u4MeterId;

    pMeterNode = (tQosStdMeterEntry *) RBTreeGet (gQoSGlobalInfo.DsMeterTbl,
                                                  (tRBElem *) & TempMeterNode);
    if (pMeterNode == NULL)
    {
        QOS_TRC_ARG2 (MGMT_TRC, "%s : Meter Id %d is not Found in the "
                      "Meter Table. \r\n", __FUNCTION__, u4MeterId);
    }

    return (pMeterNode);
}

/*****************************************************************************/
/* Function Name      : QoSCreateMeterTblEntry                               */
/* Description        : This function is used to Create an Entry in   the    */
/*                      Meter Table ,it will do the following Action         */
/*                      1. Allocate a memory block for the New Entry         */
/*                      2. Initialize the New Entry                          */
/*                      3. Add this New Entry into the Meter Table           */
/* Input(s)           : u4MeterId      - Index to MeterNode                  */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : gQoSGlobalInfo                                       */
/* Global Variables                                                          */
/* Modified           : gQoSGlobalInfo                                       */
/* Return Value(s)    : NULL / Pointer to  MeterNode                         */
/* Called By          : SNMP Low Level                                       */
/* Calling Function   :                                                      */
/*****************************************************************************/
tQoSMeterNode      *
QoSCreateMeterTblEntry (UINT4 u4MeterId)
{
    UINT4               u4RetStatus = QOS_FAILURE;
    tQoSMeterNode      *pNewMeterNode = NULL;

    /* 1. Allocate a memory block for the New Entry */
    pNewMeterNode = (tQoSMeterNode *)
        MemAllocMemBlk (gQoSGlobalInfo.QoSMeterTblMemPoolId);
    if (pNewMeterNode == NULL)
    {
        QOS_TRC_ARG2 (MGMT_TRC, "In %s : MemAllocMemBlk () Failed, "
                      "for MeterId %d. \r\n", __FUNCTION__, u4MeterId);

        return (NULL);
    }

    /* 2. Initialize the New Entry */
    MEMSET (pNewMeterNode, 0, sizeof (tQoSMeterNode));

    SPRINTF ((CHR1 *) pNewMeterNode->au1Name, "METER-%u", u4MeterId);
    pNewMeterNode->u4Id = u4MeterId;
    pNewMeterNode->u1ColorMode = QOS_METER_COLOR_BLIND;
    pNewMeterNode->u1Type = QOS_METER_TYPE_SIMPLE_TB;
    pNewMeterNode->i4StorageType = QOS_STORAGE_NONVOLATILE;
    pNewMeterNode->u1Status = NOT_READY;

    /* 3. Add this New Entry into the Meter Table */
    u4RetStatus = RBTreeAdd (gQoSGlobalInfo.pRbMeterTbl,
                             (tRBElem *) pNewMeterNode);
    if (u4RetStatus != RB_SUCCESS)
    {
        u4RetStatus = MemReleaseMemBlock (gQoSGlobalInfo.QoSMeterTblMemPoolId,
                                          (UINT1 *) pNewMeterNode);
        if (u4RetStatus == MEM_FAILURE)
        {
            QOS_TRC_ARG1 (MGMT_TRC, "In %s : MemReleaseMemBlock () "
                          "Failed.\r\n", __FUNCTION__);

            return (NULL);
        }

        QOS_TRC_ARG2 (MGMT_TRC, "In %s : RBTreeAdd () Failed ,"
                      "for MeterId %d. \r\n", __FUNCTION__, u4MeterId);
        return (NULL);
    }

    return (pNewMeterNode);
}

/*****************************************************************************/
/* Function Name      : QoSCreateStdMeterTblEntry                            */
/* Description        : This function is used to Create an Entry in   the    */
/*                      Meter Table ,it will do the following Action         */
/*                      1. Allocate a memory block for the New Entry         */
/*                      2. Initialize the New Entry                          */
/*                      3. Add this New Entry into the Meter Table           */
/* Input(s)           : u4MeterId      - Index to MeterNode                  */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : gQoSGlobalInfo                                       */
/* Global Variables                                                          */
/* Modified           : gQoSGlobalInfo                                       */
/* Return Value(s)    : NULL / Pointer to  MeterNode                         */
/* Called By          : SNMP Low Level                                       */
/* Calling Function   :                                                      */
/*****************************************************************************/
tQosStdMeterEntry  *
QoSCreateStdMeterTblEntry (UINT4 u4MeterId)
{
    UINT4               u4RetStatus = QOS_FAILURE;
    tQosStdMeterEntry  *pNewMeterNode = NULL;

    /* 1. Allocate a memory block for the New Entry */
    pNewMeterNode = (tQosStdMeterEntry *)
        MemAllocMemBlk (gQoSGlobalInfo.DsMeterPoolId);
    if (pNewMeterNode == NULL)
    {
        QOS_TRC_ARG2 (MGMT_TRC, "In %s : MemAllocMemBlk () Failed, "
                      "for MeterId %d. \r\n", __FUNCTION__, u4MeterId);

        return (NULL);
    }

    /* 2. Initialize the New Entry */
    MEMSET (pNewMeterNode, 0, sizeof (tQosStdMeterEntry));

    pNewMeterNode->u4DSMeterId = u4MeterId;
    pNewMeterNode->i4StorageType = QOS_STORAGE_NONVOLATILE;
    pNewMeterNode->u1DSMeterStatus = NOT_READY;

    /* 3. Add this New Entry into the Meter Table */
    u4RetStatus = RBTreeAdd (gQoSGlobalInfo.DsMeterTbl,
                             (tRBElem *) pNewMeterNode);
    if (u4RetStatus != RB_SUCCESS)
    {
        u4RetStatus = MemReleaseMemBlock (gQoSGlobalInfo.DsMeterPoolId,
                                          (UINT1 *) pNewMeterNode);
        if (u4RetStatus == MEM_FAILURE)
        {
            QOS_TRC_ARG1 (MGMT_TRC, "In %s : MemReleaseMemBlock () "
                          "Failed.\r\n", __FUNCTION__);

            return (NULL);
        }

        QOS_TRC_ARG2 (MGMT_TRC, "In %s : RBTreeAdd () Failed ,"
                      "for MeterId %d. \r\n", __FUNCTION__, u4MeterId);
        return (NULL);
    }

    return (pNewMeterNode);
}

/*****************************************************************************/
/* Function Name      : QoSValidateMeterTblEntry                             */
/* Description        : This function is used to Check the Mandatory Fields  */
/*                      of Meter Table's Entry, it will do the               */
/*                      following Action                                     */
/*                      1. Check the Range for Mandatory fileds              */
/*                      4. All Mandatory Fields are set,set the RowStatus as */
/*                        NOT_IN_SERVICE                                     */
/* Input(s)           : pMeterNode   - Pointer to MeterNode                  */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : gQoSGlobalInfo                                       */
/* Global Variables                                                          */
/* Modified           : gQoSGlobalInfo                                       */
/* Return Value(s)    : QOS_SUCCESS or QOS_FAILURE                           */
/* Called By          : SNMP Low Level                                       */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QoSValidateMeterTblEntry (tQoSMeterNode * pMeterNode)
{
    INT4                i4ReturnValue = QOS_SUCCESS;
    if (pMeterNode == NULL)
    {
        return (QOS_FAILURE);
    }

    /*Check Mandatory Fields */
    switch (pMeterNode->u1Type)
    {
        case QOS_METER_TYPE_SIMPLE_TB:
            /*  As per rfc3290, the Simple Token Bucket Meter has two valid parameters such as Commited rate and BurstSize */
            if ((pMeterNode->u4CIR == 0) ||
                ((pMeterNode->u4Interval == 0) && (pMeterNode->u4CBS == 0))
                || (pMeterNode->u4EIR != 0) || (pMeterNode->u4EBS != 0))
            {
                QOS_TRC_ARG1 (MGMT_TRC, "%s : CIR and CBS Should not"
                              " be NULL for Simple Token Bucket Meter .\r\n",
                              __FUNCTION__);

                pMeterNode->u1Status = NOT_READY;
                i4ReturnValue = QOS_FAILURE;

            }

            break;

        case QOS_METER_TYPE_AVG_RATE:

            if ((pMeterNode->u4Interval == 0) || (pMeterNode->u4CIR == 0) ||
                (pMeterNode->u4CBS != 0) || (pMeterNode->u4EIR != 0) ||
                (pMeterNode->u4EBS != 0))
            {
                QOS_TRC_ARG1 (MGMT_TRC, "%s : Interval and CIR Should not"
                              " be NULL for Avg Rate Meter .\r\n",
                              __FUNCTION__);

                pMeterNode->u1Status = NOT_READY;
                i4ReturnValue = QOS_FAILURE;

            }

            break;

        case QOS_METER_TYPE_SRTCM:

            if ((pMeterNode->u4CBS == 0) &&
                ((pMeterNode->u4EBS == 0) || (pMeterNode->u4Interval != 0) ||
                 (pMeterNode->u4EIR != 0)))
            {
                QOS_TRC_ARG1 (MGMT_TRC, "%s : CBS and EBS Should not"
                              " be NULL for Single Rate Three Color Meter"
                              " (SRTCM).\r\n", __FUNCTION__);

                pMeterNode->u1Status = NOT_READY;
                i4ReturnValue = QOS_FAILURE;

            }

            break;

        case QOS_METER_TYPE_TRTCM:

            if ((pMeterNode->u4Interval != 0))
            {
                QOS_TRC_ARG1 (MGMT_TRC, "%s : CBS and EBS Should "
                              "not be NULL for Two Rate Three Color Meter"
                              " (TRTCM).\r\n", __FUNCTION__);

                pMeterNode->u1Status = NOT_READY;
                i4ReturnValue = QOS_FAILURE;

            }

            break;

        case QOS_METER_TYPE_TSWTCM:

            if ((pMeterNode->u4CIR == 0) || (pMeterNode->u4EIR == 0) ||
                (pMeterNode->u4Interval == 0) || (pMeterNode->u4CBS != 0) ||
                (pMeterNode->u4EBS != 0))
            {
                QOS_TRC_ARG1 (MGMT_TRC, "%s : CIR, EIR and Interval Should "
                              "not be NULL for Time Sliding Window Three "
                              "Color Marker (TSWTCM).\r\n", __FUNCTION__);

                pMeterNode->u1Status = NOT_READY;
                i4ReturnValue = QOS_FAILURE;

            }

            break;

        case QOS_METER_TYPE_MEF_DECOUPLED:
        case QOS_METER_TYPE_MEF_COUPLED:

            i4ReturnValue = QOS_SUCCESS;
            break;

        default:
            QOS_TRC_ARG1 (MGMT_TRC, "%s : Invalid Meter Type.\r\n",
                          __FUNCTION__);

            i4ReturnValue = QOS_FAILURE;

            break;
    }
    if (i4ReturnValue == QOS_SUCCESS)
    {
        pMeterNode->u1Status = NOT_IN_SERVICE;
    }

    return i4ReturnValue;
}

/*****************************************************************************/
/* Function Name      : QoSDeleteMeterTblEntry                               */
/* Description        : This function is used to Delete an Entry from the    */
/*                      Meter Map Table ,it will do the following Action     */
/*                      1. Remove the Entry from the meter Table             */
/*                      2. Decrement the RefCount of Meter Table Entry       */
/*                      3. Clear the Removed Entry                           */
/*                      4. Release the Removed Entry's memory                */
/* Input(s)           : pMeterNode   - Pointer to MeterNode                  */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : gQoSGlobalInfo                                       */
/* Global Variables                                                          */
/* Modified           : gQoSGlobalInfo                                       */
/* Return Value(s)    : QOS_SUCCESS or QOS_FAILURE                           */
/* Called By          : SNMP Low Level                                       */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QoSDeleteMeterTblEntry (tQoSMeterNode * pMeterNode)
{
    UINT4               u4RetStatus = QOS_FAILURE;
    tQoSMeterNode      *pDelMeterNode = NULL;
    tQoSMeterNode      *pNextMeterNode = NULL;

    if (pMeterNode == NULL)
    {
        return (QOS_FAILURE);
    }

    /* 1. Remove Entry from the Priority Map Table */
    pDelMeterNode = RBTreeRem (gQoSGlobalInfo.pRbMeterTbl,
                               (tRBElem *) pMeterNode);
    if (pDelMeterNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : RBTreeRem () Failed. \r\n",
                      __FUNCTION__);

        return (QOS_FAILURE);
    }

    /* 2. Decrement the RefCount of Meter Table Entry */
    if ((pDelMeterNode->u4Next) != 0)
    {
        pNextMeterNode = QoSUtlGetMeterNode (pDelMeterNode->u4Next);

        if (pNextMeterNode == NULL)
        {
            QOS_TRC_ARG1 (MGMT_TRC, "In %s :Get Next Meter QoSUtlGetMeterNode"
                          " () Returns FAILURE. \r\n", __FUNCTION__);

            return (QOS_FAILURE);
        }

        pNextMeterNode->u4RefCount = (pNextMeterNode->u4RefCount) - 1;
    }

    /* 3. Clear the Removed Entry */
    MEMSET (pDelMeterNode, 0, sizeof (tQoSMeterNode));

    /* 4. Release the Removed Entry's memory */
    u4RetStatus = MemReleaseMemBlock (gQoSGlobalInfo.QoSMeterTblMemPoolId,
                                      (UINT1 *) pDelMeterNode);
    if (u4RetStatus == MEM_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : MemReleaseMemBlock () Failed.\r\n",
                      __FUNCTION__);

        return (QOS_FAILURE);
    }

    return (QOS_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSDeleteStdMeterTblEntry                            */
/* Description        : This function is used to Delete an Entry from the    */
/*                      Meter Map Table ,it will do the following Action     */
/*                      1. Remove the Entry from the meter Table             */
/*                      2. Decrement the RefCount of Meter Table Entry       */
/*                      3. Clear the Removed Entry                           */
/*                      4. Release the Removed Entry's memory                */
/* Input(s)           : pMeterNode   - Pointer to MeterNode                  */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : gQoSGlobalInfo                                       */
/* Global Variables                                                          */
/* Modified           : gQoSGlobalInfo                                       */
/* Return Value(s)    : QOS_SUCCESS or QOS_FAILURE                           */
/* Called By          : SNMP Low Level                                       */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QoSDeleteStdMeterTblEntry (tQosStdMeterEntry * pMeterNode)
{
    UINT4               u4RetStatus = QOS_FAILURE;
    tQosStdMeterEntry  *pDelMeterNode = NULL;

    if (pMeterNode == NULL)
    {
        return (QOS_FAILURE);
    }

    /* 1. Remove Entry from the Priority Map Table */
    pDelMeterNode = RBTreeRem (gQoSGlobalInfo.DsMeterTbl,
                               (tRBElem *) pMeterNode);
    if (pDelMeterNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : RBTreeRem () Failed. \r\n",
                      __FUNCTION__);

        return (QOS_FAILURE);
    }

    /* 2. Clear the Removed Entry */
    MEMSET (pDelMeterNode, 0, sizeof (tQosStdMeterEntry));

    /* 3. Release the Removed Entry's memory */
    u4RetStatus = MemReleaseMemBlock (gQoSGlobalInfo.DsMeterPoolId,
                                      (UINT1 *) pDelMeterNode);
    if (u4RetStatus == MEM_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : MemReleaseMemBlock () Failed.\r\n",
                      __FUNCTION__);

        return (QOS_FAILURE);
    }

    return (QOS_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSGetNextMeterTblEntryIndex                         */
/* Description        : This function is used to Get the Next Entry from the */
/*                      Meter Table ,it will do the following Action         */
/*                      1. If u4CurrentIndex is 0 then  GetFirxtIndex.       */
/*                      2. If u4CurrentIndex is not 0 then  GetNextIndex.    */
/* Input(s)           : u4CurrentIndex  - Current index id                   */
/*                      pu4NextIndex    - Pointer to NextIndex Id            */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : gQoSGlobalInfo                                       */
/* Global Variables                                                          */
/* Modified           : gQoSGlobalInfo                                       */
/* Return Value(s)    : QOS_SUCCESS or QOS_FAILURE                           */
/* Called By          : SNMP Low Level                                       */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QoSGetNextMeterTblEntryIndex (UINT4 u4CurrentIndex, UINT4 *pu4NextIndex)
{
    tQoSMeterNode       MeterNode;
    tQoSMeterNode      *pMeterNode = NULL;

    if (u4CurrentIndex == 0)
    {
        pMeterNode = (tQoSMeterNode *)
            RBTreeGetFirst (gQoSGlobalInfo.pRbMeterTbl);
    }
    else
    {
        MeterNode.u4Id = u4CurrentIndex;

        pMeterNode =
            (tQoSMeterNode *) RBTreeGetNext (gQoSGlobalInfo.pRbMeterTbl,
                                             &MeterNode, QoSMeterCmpFun);
    }

    if (pMeterNode == NULL)
    {
        QOS_TRC_ARG2 (MGMT_TRC, "In %s : RBTreeGetNext () Failed, "
                      " for MeterId %d.\r\n", __FUNCTION__, u4CurrentIndex);

        return (QOS_FAILURE);
    }

    *pu4NextIndex = pMeterNode->u4Id;

    return (QOS_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSUtlValidateMeter                                  */
/* Description        : This function is used validate the Given Meter Id    */
/*                      in the Meter table.                                  */
/*                      if it is ACTIVE return Failure Success otherwise.    */
/*                      Set the ErrorCode accordingly.                       */
/* Input(s)           : u4MeterId   - Class Map Id.                          */
/*                      pu4ErrorCode - Pointer to ErrorCode.                 */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : QOS_SUCCESS /QOS_FAILURE                             */
/* Called By          : SNMP                                                 */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QoSUtlValidateMeter (UINT4 u4MeterId, UINT4 *pu4ErrorCode)
{
    tQoSMeterNode      *pMeterNode = NULL;

    pMeterNode = QoSUtlGetMeterNode (u4MeterId);

    if (pMeterNode == NULL)
    {
        QOS_TRC_ARG2 (MGMT_TRC, "%s : The Meter Id %d. Not yet Configured."
                      "\r\n", __FUNCTION__, u4MeterId);

        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (QOS_CLI_ERR_METER_NO_ENTRY);

        return (QOS_FAILURE);
    }

    if (pMeterNode->u1Status != ACTIVE)
    {
        QOS_TRC_ARG2 (MGMT_TRC, "%s : The Meter Id %d. not Active."
                      "\r\n", __FUNCTION__, u4MeterId);

        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (QOS_CLI_ERR_METER_NOT_ACTIVE);

        return (QOS_FAILURE);
    }

    return (QOS_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSUtlValidateMeterTblIdxInst                        */
/* Description        : This function is used to Validate the Given Index    */
/*                      instance in the Meter Table, if it is ACTIVE         */
/*                      return Failure Success otherwise.                    */
/* Input(s)           : u4FsQoSMeterId   - MeterID                           */
/*                      pu4ErrorCode     - Pointer to Error Code             */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : gQoSGlobalInfo                                       */
/* Global Variables                                                          */
/* Modified           : gQoSGlobalInfo                                       */
/* Return Value(s)    : QOS_SUCCESS or QOS_FAILURE                           */
/* Called By          : SNMP Low Level                                       */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QoSUtlValidateMeterTblIdxInst (UINT4 u4FsQoSMeterId, UINT4 *pu4ErrorCode)
{
    tQoSMeterNode      *pMeterNode = NULL;
    INT4                i4RetStatus = 0;

    if (gQoSGlobalInfo.eSysControl != QOS_SYS_CNTL_START)
    {
        QOS_TRC (MGMT_TRC, QOS_MODULE_SHUTDOW_ERROR_MSG);
        CLI_SET_ERR (QOS_CLI_ERR_MODULE_SHUTDOW);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;

        return (QOS_FAILURE);
    }

    /* Check the Range of Index Values (Min-Max) */
    i4RetStatus = QOS_CHECK_METER_TBL_INDEX_RANGE (u4FsQoSMeterId);

    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG3 (MGMT_TRC, "%s : Meter Id %d is out of Range."
                      " The Range Should be (1-%d). \r\n", __FUNCTION__,
                      u4FsQoSMeterId, QOS_METER_TBL_MAX_INDEX_RANGE);

        CLI_SET_ERR (QOS_CLI_ERR_METER_RANGE);

        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;

        return (QOS_FAILURE);
    }

    pMeterNode = QoSUtlGetMeterNode (u4FsQoSMeterId);

    if (pMeterNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetMeterNode () "
                      " Returns FAILURE. \r\n", __FUNCTION__);

        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (QOS_CLI_ERR_METER_NO_ENTRY);
        return (QOS_FAILURE);
    }

    if (pMeterNode->u1Status == ACTIVE)
    {
        QOS_TRC (MGMT_TRC, "Can not change the value in ACTIVE State.\r\n");

        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;

        CLI_SET_ERR (QOS_CLI_ERR_STATUS_ACTIVE);

        return (QOS_FAILURE);
    }

    return (QOS_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSValidateMeterTypeParams                           */
/* Description        : This function is used to Validate the Given Meter    */
/*                      Type and it Mandatory Fields                         */
/* Input(s)           : u4MeterId        - MeterID                           */
/*                      u4ParamType      - Meter Type                        */
/*                      pu4ErrorCode     - Pointer to Error Code             */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : gQoSGlobalInfo                                       */
/* Global Variables                                                          */
/* Modified           : gQoSGlobalInfo                                       */
/* Return Value(s)    : QOS_SUCCESS or QOS_FAILURE                           */
/* Called By          : SNMP Low Level                                       */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QoSValidateMeterTypeParams (UINT4 u4MeterId, UINT4 u4ParamType,
                            UINT4 *pu4ErrorCode)
{
    tQoSMeterNode      *pMeterNode = NULL;

    pMeterNode = QoSUtlGetMeterNode (u4MeterId);
    if (pMeterNode == NULL)
    {
        CLI_SET_ERR (QOS_CLI_ERR_METER_NO_ENTRY);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (QOS_FAILURE);
    }

    /*Check Mandatory Fields */
    switch (pMeterNode->u1Type)
    {
        case QOS_METER_TYPE_SIMPLE_TB:

            if ((QOS_METER_PARAM_TYPE_CIR == u4ParamType) ||
                (QOS_METER_PARAM_TYPE_CBS == u4ParamType) ||
                (QOS_METER_PARAM_TYPE_INTERVAL == u4ParamType))
            {
                return (QOS_SUCCESS);
            }
            break;

        case QOS_METER_TYPE_AVG_RATE:

            if ((QOS_METER_PARAM_TYPE_INTERVAL == u4ParamType) ||
                (QOS_METER_PARAM_TYPE_CIR == u4ParamType))
            {
                return (QOS_SUCCESS);
            }
            break;

        case QOS_METER_TYPE_SRTCM:

            if ((QOS_METER_PARAM_TYPE_CIR == u4ParamType) ||
                (QOS_METER_PARAM_TYPE_CBS == u4ParamType) ||
                (QOS_METER_PARAM_TYPE_EBS == u4ParamType))
            {
                return (QOS_SUCCESS);
            }
            break;

        case QOS_METER_TYPE_TRTCM:
        case QOS_METER_TYPE_MEF_COUPLED:
        case QOS_METER_TYPE_MEF_DECOUPLED:

            if ((QOS_METER_PARAM_TYPE_CIR == u4ParamType) ||
                (QOS_METER_PARAM_TYPE_CBS == u4ParamType) ||
                (QOS_METER_PARAM_TYPE_EIR == u4ParamType) ||
                (QOS_METER_PARAM_TYPE_EBS == u4ParamType))
            {
                return (QOS_SUCCESS);
            }
            break;

        case QOS_METER_TYPE_TSWTCM:

            if ((QOS_METER_PARAM_TYPE_CIR == u4ParamType) ||
                (QOS_METER_PARAM_TYPE_EIR == u4ParamType) ||
                (QOS_METER_PARAM_TYPE_INTERVAL == u4ParamType))
            {
                return (QOS_SUCCESS);
            }
            break;

        default:
            break;
    }

    CLI_SET_ERR (QOS_CLI_ERR_METER_INVALID_PARAM);
    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
    return (QOS_FAILURE);
}

/*****************************************************************************/
/*                     POLICY MAP TABLE FUNCTIONS                            */
/*****************************************************************************/

/*****************************************************************************/
/* Function Name      : QoSPlyMapCmpFun                                      */
/* Description        : This function is used as a Compare function in the   */
/*                      RBTree.                                              */
/*                      1.It Compares the PlyMapTableId and return the values*/
/* Input(s)           : e1 , e2 -  Elements of RBTree                        */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    :  -1/0/1                                              */
/* Called By          : RBTreeUtility                                        */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QoSPlyMapCmpFun (tRBElem * e1, tRBElem * e2)
{
    tQoSPolicyMapNode  *pPlyMapNode1 = NULL;
    tQoSPolicyMapNode  *pPlyMapNode2 = NULL;

    pPlyMapNode1 = (tQoSPolicyMapNode *) e1;
    pPlyMapNode2 = (tQoSPolicyMapNode *) e2;

    /* Compare the Policy Map Table Ids */

    if (pPlyMapNode1->u4Id < pPlyMapNode2->u4Id)
    {
        return (-1);
    }
    else if (pPlyMapNode1->u4Id > pPlyMapNode2->u4Id)
    {
        return (1);
    }

    return (0);
}

/*****************************************************************************/
/* Function Name      : QoSPlyMapTblRBTFreeNodeFn                            */
/* Description        : This function is used to Delete the Policy Table     */
/*                      Entries and Release the Memory to the respective     */
/*                      Mem Pool .                                           */
/* Input(s)           : tRBElem     - Pointer to Policy Table Entry          */
/*                      u4Arg       - Unused Param                           */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : QOS_SUCCESS /QOS_FAILURE                             */
/* Called By          : RBTreeUtility                                        */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QoSPlyMapTblRBTFreeNodeFn (tRBElem * pRBElem, UINT4 u4Arg)
{
    UINT4               u4RetStatus = MEM_FAILURE;

    UNUSED_PARAM (u4Arg);

    if (pRBElem == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : Policy Tbl Entry is NULL.\r\n",
                      __FUNCTION__);

        return (QOS_FAILURE);
    }

    MEMSET (pRBElem, 0, sizeof (tQoSPolicyMapNode));

    /* Release the Mem Block */
    u4RetStatus = MemReleaseMemBlock (gQoSGlobalInfo.QoSPlyMapTblMemPoolId,
                                      (UINT1 *) pRBElem);

    if (u4RetStatus != MEM_SUCCESS)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : PolicyTbl- "
                      "MemReleaseMemBlock () Failed.\r\n", __FUNCTION__);
    }
    return (QOS_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSUtlGetPolicyMapNode                               */
/* Description        : This function is used to Get the Entry  form  the    */
/*                      Policy Map Table ,it will do the following Action    */
/*                      1. Call RBTreeGet Utility.                           */
/* Input(s)           : u4PlyMapId      - Index to PlyMapNode                */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : gQoSGlobalInfo                                       */
/* Global Variables                                                          */
/* Modified           :                                                      */
/* Return Value(s)    : NULL/ Pointer to 'tQoSPolicyMapNode'                 */
/* Called By          : SNMP Low Level                                       */
/* Calling Function   :                                                      */
/*****************************************************************************/
tQoSPolicyMapNode  *
QoSUtlGetPolicyMapNode (UINT4 u4PlyMapId)
{
    tQoSPolicyMapNode   TempPlyMapNode;
    tQoSPolicyMapNode  *pPlyMapNode = NULL;

    MEMSET (&TempPlyMapNode, 0, sizeof (tQoSPolicyMapNode));
    TempPlyMapNode.u4Id = u4PlyMapId;

    pPlyMapNode = (tQoSPolicyMapNode *)
        RBTreeGet (gQoSGlobalInfo.pRbPlyMapTbl, (tRBElem *) & TempPlyMapNode);

    if (pPlyMapNode == NULL)
    {
        QOS_TRC_ARG2 (MGMT_TRC, "%s : Policy Map Id %d is not Found in the "
                      "Policy Map Table. \r\n", __FUNCTION__, u4PlyMapId);

    }

    return (pPlyMapNode);

}

/*****************************************************************************/
/* Function Name      : QoSUtlGetVlanQMapNode                                */
/* Description        : This function is used to Get the Entry  form  the    */
/*                      Vlan Map Table ,it will do the following Action      */
/*                      1. Call RBTreeGet Utility.                           */
/* Input(s)           : u4VlanQMapId      - Index to VlanQMapNode            */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : gQoSGlobalInfo                                       */
/* Global Variables                                                          */
/* Modified           :                                                      */
/* Return Value(s)    : NULL/ Pointer to 'tQoSVlanQMapNode'                  */
/* Called By          : SNMP Low Level                                       */
/* Calling Function   :                                                      */
/*****************************************************************************/
tQoSInVlanMapNode  *
QoSUtlGetVlanQMapNode (UINT4 u4VlanQMapId)
{
    tQoSInVlanMapNode   TempVlanQMapNode;
    tQoSInVlanMapNode  *pVlanQMapNode = NULL;

    MEMSET (&TempVlanQMapNode, 0, sizeof (tQoSInVlanMapNode));
    TempVlanQMapNode.u4Id = u4VlanQMapId;

    pVlanQMapNode = (tQoSInVlanMapNode *)
        RBTreeGet (gQoSGlobalInfo.pRbVlanQueueingMapTbl,
                   (tRBElem *) & TempVlanQMapNode);

    if (pVlanQMapNode == NULL)
    {
        QOS_TRC_ARG2 (MGMT_TRC, "%s : Vlan Map Id %d is not Found in the "
                      "Vlan Queueing Map Table. \r\n", __FUNCTION__,
                      u4VlanQMapId);
    }
    return (pVlanQMapNode);
}

/*****************************************************************************/
/* Function Name      : QoSCreatePlyMapTblEntry                              */
/* Description        : This function is used to Create an Entry in   the    */
/*                      Policy Map Table ,it will do the following Action    */
/*                      1. Allocate a memory block for the New Entry         */
/*                      2. Initialize the New Entry                          */
/*                      3. Add this New Entry into the Policy Map Table      */
/* Input(s)           : u4PlyMapId      - Index to PlyMapNode                */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : gQoSGlobalInfo                                       */
/* Global Variables                                                          */
/* Modified           : gQoSGlobalInfo                                       */
/* Return Value(s)    : NULL / Pointer to  PlyMapNode                        */
/* Called By          : SNMP Low Level                                       */
/* Calling Function   :                                                      */
/*****************************************************************************/
tQoSPolicyMapNode  *
QoSCreatePlyMapTblEntry (UINT4 u4PlyMapId)
{
    UINT4               u4RetStatus = QOS_FAILURE;
    tQoSPolicyMapNode  *pNewPlyMapNode = NULL;

    /* 1. Allocate a memory block for the New Entry */
    pNewPlyMapNode = (tQoSPolicyMapNode *)
        MemAllocMemBlk (gQoSGlobalInfo.QoSPlyMapTblMemPoolId);
    if (pNewPlyMapNode == NULL)
    {
        QOS_TRC_ARG2 (MGMT_TRC, "In %s : MemAllocMemBlk () Failed, "
                      "for PolicyMapId %d. \r\n", __FUNCTION__, u4PlyMapId);

        return (NULL);
    }

    /* 2. Initialize the New Entry */
    MEMSET (pNewPlyMapNode, 0, sizeof (tQoSPolicyMapNode));

    SPRINTF ((CHR1 *) pNewPlyMapNode->au1Name, "POL-%u", u4PlyMapId);
    pNewPlyMapNode->u4Id = u4PlyMapId;
    pNewPlyMapNode->u1PHBType = QOS_PLY_DEFAULT_PHB_TYPE;
    pNewPlyMapNode->u2DefaultPHB = QOS_PLY_DEFAULT_PHB_VAL;
    pNewPlyMapNode->i4VlanDE = QOS_PLY_DEFAULT_DEI_VAL;
    pNewPlyMapNode->u1Status = NOT_READY;
    pNewPlyMapNode->b1IsPlyMapConfFromExt = FALSE;    /* Flag will be set to true by other modules */

    /* 3. Add this New Entry into the Policy Map Table */
    u4RetStatus = RBTreeAdd (gQoSGlobalInfo.pRbPlyMapTbl,
                             (tRBElem *) pNewPlyMapNode);
    if (u4RetStatus != RB_SUCCESS)
    {
        u4RetStatus = MemReleaseMemBlock (gQoSGlobalInfo.QoSPlyMapTblMemPoolId,
                                          (UINT1 *) pNewPlyMapNode);
        if (u4RetStatus == MEM_FAILURE)
        {
            QOS_TRC_ARG1 (MGMT_TRC, "In %s : MemReleaseMemBlock () "
                          "Failed.\r\n", __FUNCTION__);

            return (NULL);
        }

        QOS_TRC_ARG2 (MGMT_TRC, "In %s : RBTreeAdd () Failed ,"
                      "for PlyMapId %d. \r\n", __FUNCTION__, u4PlyMapId);

        return (NULL);
    }

    return (pNewPlyMapNode);
}

/*****************************************************************************/
/* Function Name      : QoSValidatePlyMapTblEntry                            */
/* Description        : This function is used to Check the Mandatory Fields  */
/*                      of Policy Map Table's Entry,                         */
/*                      if All Mandatory Fields are set,set the RowStatus as */
/*                         NOT_IN_SERVICE State.                             */
/* Input(s)           : pPlyMapNode   - Pointer to PlyMapNode                */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : gQoSGlobalInfo                                       */
/* Global Variables                                                          */
/* Modified           : gQoSGlobalInfo                                       */
/* Return Value(s)    : QOS_SUCCESS or QOS_FAILURE                           */
/* Called By          : SNMP Low Level                                       */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QoSValidatePlyMapTblEntry (tQoSPolicyMapNode * pPlyMapNode)
{

    /* we can Update in ACTIVE state also */
    if (pPlyMapNode->u1Status != ACTIVE)
    {
        pPlyMapNode->u1Status = NOT_IN_SERVICE;
    }

    return (QOS_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSDeletePlyMapTblEntry                              */
/* Description        : This function is used to Delete an Entry from the    */
/*                      Policy Map Table ,it will do the following Action    */
/*                      1. Remove the Entry from the Policy Map Table        */
/*                      2. Clear the Removed Entry                           */
/*                      3. Release the Removed Entry's memory                */
/* Input(s)           : pPlyMapNode   - Pointer to PlyMapNode                */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : gQoSGlobalInfo                                       */
/* Global Variables                                                          */
/* Modified           : gQoSGlobalInfo                                       */
/* Return Value(s)    : QOS_SUCCESS or QOS_FAILURE                           */
/* Called By          : SNMP Low Level                                       */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QoSDeletePlyMapTblEntry (tQoSPolicyMapNode * pPlyMapNode)
{
    tQoSPolicyMapNode  *pDelPlyMapNode = NULL;
    tQoSMeterNode      *pTmpMeterNode = NULL;
    UINT4               u4RetStatus = QOS_FAILURE;

    /* 1. Remove Entry from the  Policy Map Table */
    pDelPlyMapNode = RBTreeRem (gQoSGlobalInfo.pRbPlyMapTbl,
                                (tRBElem *) pPlyMapNode);
    if (pDelPlyMapNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : RBTreeRem () Failed.\r\n",
                      __FUNCTION__);

        return (QOS_FAILURE);
    }

    /* Reset corresponding  Meter ref count if the id not '0' */
    if (pDelPlyMapNode->u4MeterTableId != 0)
    {
        pTmpMeterNode = QoSUtlGetMeterNode (pDelPlyMapNode->u4MeterTableId);
        if (pTmpMeterNode == NULL)
        {
            QOS_TRC_ARG1 (MGMT_TRC, "In %s :Reset: QoSUtlGetMeterNode () "
                          "Returns FAILURE. \r\n", __FUNCTION__);
            return (QOS_FAILURE);
        }
        pTmpMeterNode->u4RefCount = (pTmpMeterNode->u4RefCount) - 1;
    }

    /* 2. Clear the Removed Entry */
    MEMSET (pDelPlyMapNode, 0, sizeof (tQoSPolicyMapNode));

    /* 3. Release the Removed Entry's memory */
    u4RetStatus = MemReleaseMemBlock (gQoSGlobalInfo.QoSPlyMapTblMemPoolId,
                                      (UINT1 *) pDelPlyMapNode);
    if (u4RetStatus == MEM_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : MemReleaseMemBlock () Failed. "
                      "\r\n", __FUNCTION__);

        return (QOS_FAILURE);
    }

    return (QOS_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSGetNextPlyMapTblEntryIndex                        */
/* Description        : This function is used to Get the Next Entry from the */
/*                      Policy Map Table ,it will do the following Action    */
/*                      1. If u4CurrentIndex is 0 then  GetFirxtIndex.       */
/*                      2. If u4CurrentIndex is not 0 then  GetNextIndex.    */
/* Input(s)           : u4CurrentIndex  - Current index id                   */
/*                      pu4NextIndex    - Pointer to NextIndex Id            */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : gQoSGlobalInfo                                       */
/* Global Variables                                                          */
/* Modified           : gQoSGlobalInfo                                       */
/* Return Value(s)    : QOS_SUCCESS or QOS_FAILURE                           */
/* Called By          : SNMP Low Level                                       */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QoSGetNextPlyMapTblEntryIndex (UINT4 u4CurrentIndex, UINT4 *pu4NextIndex)
{
    tQoSPolicyMapNode  *pPlyMapNode = NULL;
    tQoSPolicyMapNode   PlyMapNode;

    if (u4CurrentIndex == 0)
    {
        pPlyMapNode = (tQoSPolicyMapNode *)
            RBTreeGetFirst (gQoSGlobalInfo.pRbPlyMapTbl);
    }
    else
    {
        PlyMapNode.u4Id = u4CurrentIndex;
        pPlyMapNode = (tQoSPolicyMapNode *)
            RBTreeGetNext (gQoSGlobalInfo.pRbPlyMapTbl, &PlyMapNode,
                           QoSPlyMapCmpFun);
    }

    if (pPlyMapNode == NULL)
    {
        QOS_TRC_ARG2 (MGMT_TRC, "In %s : RBTreeGetNext () Failed, "
                      " for PlyMapId %d.\r\n", __FUNCTION__, u4CurrentIndex);

        return (QOS_FAILURE);
    }

    *pu4NextIndex = pPlyMapNode->u4Id;

    return (QOS_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSGetNextVlanQMapTblEntryIndex                      */
/* Description        : This function is used to Get the Next Entry from the */
/*                      Vlan Map Table ,it will do the following Action      */
/*                      1. If u4CurrentIndex is 0 then  GetFirxtIndex.       */
/*                      2. If u4CurrentIndex is not 0 then  GetNextIndex.    */
/* Input(s)           : u4CurrentIndex  - Current index id                   */
/*                      pu4NextIndex    - Pointer to NextIndex Id            */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : gQoSGlobalInfo                                       */
/* Global Variables                                                          */
/* Modified           : gQoSGlobalInfo                                       */
/* Return Value(s)    : QOS_SUCCESS or QOS_FAILURE                           */
/* Called By          : SNMP Low Level                                       */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QoSGetNextVlanQMapTblEntryIndex (UINT4 u4CurrentIndex, UINT4 *pu4NextIndex)
{
    tQoSInVlanMapNode  *pVlanQMapNode = NULL;
    tQoSInVlanMapNode   VlanQMapNode;

    if (u4CurrentIndex == 0)
    {
        pVlanQMapNode = (tQoSInVlanMapNode *)
            RBTreeGetFirst (gQoSGlobalInfo.pRbVlanQueueingMapTbl);
    }
    else
    {
        VlanQMapNode.u4Id = u4CurrentIndex;
        pVlanQMapNode = (tQoSInVlanMapNode *)
            RBTreeGetNext (gQoSGlobalInfo.pRbVlanQueueingMapTbl, &VlanQMapNode,
                           QoSVlanQMapCmpFun);
    }

    if (pVlanQMapNode == NULL)
    {
        QOS_TRC_ARG2 (MGMT_TRC, "In %s : RBTreeGetNext () Failed, "
                      " for VlanQMapId %d.\r\n", __FUNCTION__, u4CurrentIndex);
        return (QOS_FAILURE);
    }
    *pu4NextIndex = pVlanQMapNode->u4Id;
    return (QOS_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSValidateInProConfActFlag                          */
/* Description        : This function is used to validate the In profile Conf*/
/*                      action Flag combination. It will do the following    */
/*                      Action                                               */
/*                      1. Check the Action flag Length, it should be = 1    */
/*                      2. Flag can not be '0', Should be within limit       */
/*                      3. IP TOS and IP DSCP can not co-exists.             */
/*                      4. VLAN and MPLS can not co-exits.                   */
/* Input(s)           : u4PlyMapId            - Policy Map Id                */
/*                      pSNMPOctSrtInActFlag  - Action Flag Mask Value.      */
/*                      pu4ErrorCode          - Pointer to ErrorCode.        */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : gQoSGlobalInfo                                       */
/* Global Variables                                                          */
/* Modified           : none                                                 */
/* Return Value(s)    : QOS_SUCCESS or QOS_FAILURE                           */
/* Called By          : SNMP Low Level                                       */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QoSValidateInProConfActFlag (UINT4 u4PlyMapId,
                             tSNMP_OCTET_STRING_TYPE * pSNMPOctSrtInActFlag,
                             UINT4 *pu4ErrorCode)
{
    tQoSPolicyMapNode  *pPlyMapNode = NULL;
    UINT1               u1InProConfActFlag = 0;
    UINT1               u1VlanFlags = 0;

    pPlyMapNode = QoSUtlGetPolicyMapNode (u4PlyMapId);

    if (pPlyMapNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetPolicyMapNode Returns "
                      "FAILURE. \r\n", __FUNCTION__);

        CLI_SET_ERR (QOS_CLI_ERR_PLY_MAP_NO_ENTRY);

        *pu4ErrorCode = SNMP_ERR_NO_CREATION;

        return (QOS_FAILURE);
    }

    if (pSNMPOctSrtInActFlag->i4_Length != QOS_POLICY_ACTION_FLAG_LENGTH)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;

        QOS_TRC_ARG2 (MGMT_TRC, "%s : Length of the Action Flag is %d. "
                      "This is Incorrect Value. The Value is = 1. \r\n",
                      __FUNCTION__, pSNMPOctSrtInActFlag->i4_Length);

        CLI_SET_ERR (QOS_CLI_ERR_ACT_FLG_LEN_INVALID);

        return (QOS_FAILURE);
    }

    MEMCPY (&(u1InProConfActFlag), pSNMPOctSrtInActFlag->pu1_OctetList,
            pSNMPOctSrtInActFlag->i4_Length);

    /* 1. Can not be '0', Should be within limit. */
    if (u1InProConfActFlag < QOS_PLY_IN_PROF_CONF_ACTION_MIN_VAL)
    {
        QOS_TRC_ARG4 (MGMT_TRC, "%s : In profile Conform action flag value %d,"
                      " is out of range. The range is Min %d to Max %d. \r\n",
                      __FUNCTION__, u1InProConfActFlag,
                      QOS_PLY_IN_PROF_CONF_ACTION_MIN_VAL,
                      QOS_PLY_IN_PROF_CONF_ACTION_MAX_VAL);

        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;

        return (QOS_FAILURE);
    }

    /* Check for Conformed Actions */
    /* 2. IP TOS and IP DSCP can not co-exists. */
    if (((u1InProConfActFlag & QOS_PLY_IN_PROF_CONF_IP_TOS) ==
         QOS_PLY_IN_PROF_CONF_IP_TOS) &&
        (u1InProConfActFlag & QOS_PLY_IN_PROF_CONF_IP_DSCP) ==
        QOS_PLY_IN_PROF_CONF_IP_DSCP)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "%s : In profile Conform action flag value "
                      "'IP_TOS and IP_DSCP' mutually exclusive.", __FUNCTION__);

        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;

        return (QOS_FAILURE);
    }

    u1VlanFlags = QOS_PLY_IN_PROF_CONF_VLAN_PRI |
        QOS_PLY_IN_PROF_CONF_VLAN_DE | QOS_PLY_IN_PROF_CONF_INNER_VLAN_PRI;

    /* 3. VLAN and MPLS can not co-exits. */
    if ((u1InProConfActFlag & u1VlanFlags) &&
        (u1InProConfActFlag & QOS_PLY_IN_PROF_CONF_MPLS_EXP))
    {
        QOS_TRC_ARG1 (MGMT_TRC, "%s : In profile Conform action flag value "
                      "'VLAN and MPLS Flags' mutually exclusive. \r\n",
                      __FUNCTION__);

        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;

        return (QOS_FAILURE);
    }

    return (QOS_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSValidateInProExcActFlag                           */
/* Description        : This function is used to validate the In profile Exc */
/*                      action Flag combination. It will do the following    */
/*                      Action                                               */
/*                      1. Check the Action flag Length, it should be = 1    */
/*                      2. Flag can not be '0', Should be within limit       */
/*                      3. IP TOS and IP DSCP can not co-exists.             */
/*                      4. VLAN and MPLS can not co-exits.                   */
/* Input(s)           : u4PlyMapId            - Policy Map Id                */
/*                      pSNMPOctSrtInActFlag  - Action Flag Mask Value.      */
/*                      pu4ErrorCode          - Pointer to ErrorCode.        */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : gQoSGlobalInfo                                       */
/* Global Variables                                                          */
/* Modified           : none                                                 */
/* Return Value(s)    : QOS_SUCCESS or QOS_FAILURE                           */
/* Called By          : SNMP Low Level                                       */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QoSValidateInProExcActFlag (UINT4 u4PlyMapId,
                            tSNMP_OCTET_STRING_TYPE * pSNMPOctSrtInActFlag,
                            UINT4 *pu4ErrorCode)
{
    tQoSPolicyMapNode  *pPlyMapNode = NULL;
    tQoSMeterNode      *pMeterNode = NULL;
    UINT1               u1InProExcActFlag = 0;
    UINT1               u1VlanFlags = 0;

    pPlyMapNode = QoSUtlGetPolicyMapNode (u4PlyMapId);

    if (pPlyMapNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetPolicyMapNode Returns "
                      "FAILURE. \r\n", __FUNCTION__);

        CLI_SET_ERR (QOS_CLI_ERR_PLY_MAP_NO_ENTRY);

        *pu4ErrorCode = SNMP_ERR_NO_CREATION;

        return (QOS_FAILURE);
    }

    pMeterNode = QoSUtlGetMeterNode (pPlyMapNode->u4MeterTableId);
    if (pMeterNode != NULL)
    {
        if (pMeterNode->u1Type == QOS_METER_TYPE_SIMPLE_TB)
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            CLI_SET_ERR (QOS_CLI_ERR_PLY_INVALID_METER_PARAM);
            return (QOS_FAILURE);
        }
    }

    if (pSNMPOctSrtInActFlag->i4_Length != QOS_POLICY_ACTION_FLAG_LENGTH)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;

        QOS_TRC_ARG2 (MGMT_TRC, "%s : Length of the Action Flag is %d. "
                      "This is Incorrect Value. The Value is = 1. \r\n",
                      __FUNCTION__, pSNMPOctSrtInActFlag->i4_Length);

        CLI_SET_ERR (QOS_CLI_ERR_ACT_FLG_LEN_INVALID);

        return (QOS_FAILURE);
    }

    MEMCPY (&(u1InProExcActFlag), pSNMPOctSrtInActFlag->pu1_OctetList,
            pSNMPOctSrtInActFlag->i4_Length);

    /* 1. Can not be '0', Should be within limit. */
    if ((u1InProExcActFlag < QOS_PLY_IN_PROF_EXC_ACTION_MIN_VAL) ||
        (u1InProExcActFlag > QOS_PLY_IN_PROF_EXC_ACTION_MAX_VAL))
    {
        QOS_TRC_ARG4 (MGMT_TRC, "%s : In profile Exceed action flag value %d,"
                      " is out of range. The range is Min %d to Max %d .",
                      __FUNCTION__, u1InProExcActFlag,
                      QOS_PLY_IN_PROF_EXC_ACTION_MIN_VAL,
                      QOS_PLY_IN_PROF_EXC_ACTION_MAX_VAL);

        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;

        return (QOS_FAILURE);
    }

    /* Check for Exceed Actions */

    /* 2. Drop Alone can exists. */
    if ((((u1InProExcActFlag) & QOS_PLY_IN_PROF_EXC_DROP) ==
         QOS_PLY_IN_PROF_EXC_DROP) &&
        (u1InProExcActFlag != QOS_PLY_IN_PROF_EXC_DROP))
    {
        QOS_TRC_ARG1 (MGMT_TRC, "%s : In profile Exceed action flag value "
                      "'DROP' can not club with other flag. \r\n",
                      __FUNCTION__);
        CLI_SET_ERR (QOS_CLI_ERR_DROP_CLUB);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (QOS_FAILURE);
    }

    /* 3. IP TOS and IP DSCP can not co-exists. */
    if (((u1InProExcActFlag & QOS_PLY_IN_PROF_EXC_IP_TOS) ==
         QOS_PLY_IN_PROF_EXC_IP_TOS) &&
        (u1InProExcActFlag & QOS_PLY_IN_PROF_EXC_IP_DSCP) ==
        QOS_PLY_IN_PROF_EXC_IP_DSCP)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "%s : In profile Exceed action flag value "
                      "'IP_TOS and IP_DSCP' mutually exclusive.\r\n",
                      __FUNCTION__);

        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;

        return (QOS_FAILURE);
    }

    u1VlanFlags = QOS_PLY_IN_PROF_EXC_VLAN_PRI |
        QOS_PLY_IN_PROF_EXC_VLAN_DE | QOS_PLY_IN_PROF_EXC_INNER_VLAN_PRI |
        QOS_PLY_IN_PROF_EXC_INNER_VLAN_DE;

    /* 3. VLAN and MPLS can not co-exits. */
    if ((u1InProExcActFlag & u1VlanFlags) &&
        (u1InProExcActFlag & QOS_PLY_IN_PROF_EXC_MPLS_EXP))
    {
        QOS_TRC_ARG1 (MGMT_TRC, "%s : In profile Exceed action flag value "
                      " 'VLAN and MPLS Flags' mutually exclusive.\r\n",
                      __FUNCTION__);

        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;

        return (QOS_FAILURE);
    }

    return (QOS_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSValidateInProConfAction                           */
/* Description        : This function is used to validate the In profile     */
/*                      action with Action Flag combination.                 */
/*                      It will do the following Action                      */
/*                      1. Get the Policy Map Entry.                         */
/*                      2. Check the Action flag with Give Action, return    */
/*                         Success if true , false otherwise.                */
/* Input(s)           : u4PlyMapId            - Policy Map Id                */
/*                      i4Value               - Action Value.                */
/*                      u2Type                - Action Type.                 */
/*                      pu4ErrorCode          - Pointer to ErrorCode.        */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : gQoSGlobalInfo                                       */
/* Global Variables                                                          */
/* Modified           : none                                                 */
/* Return Value(s)    : QOS_SUCCESS or QOS_FAILURE                           */
/* Called By          : SNMP Low Level                                       */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QoSValidateInProConfAction (UINT4 u4PlyMapId, INT4 i4Value, UINT1 u1Type,
                            UINT4 *pu4ErrorCode)
{
    tQoSPolicyMapNode  *pPlyMapNode = NULL;
    UINT1               u1InActFlag = 0;
    INT4                i4ReturnValue = QOS_SUCCESS;

    pPlyMapNode = QoSUtlGetPolicyMapNode (u4PlyMapId);

    if (pPlyMapNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetPolicyMapNode Returns "
                      "FAILURE. \r\n", __FUNCTION__);

        CLI_SET_ERR (QOS_CLI_ERR_PLY_MAP_NO_ENTRY);

        *pu4ErrorCode = SNMP_ERR_NO_CREATION;

        return (QOS_FAILURE);
    }

    u1InActFlag = pPlyMapNode->u1InProfConfActFlag;

    if ((u1InActFlag & u1Type) != u1Type)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "%s : InValid Action Value. Action Flag"
                      " not set for this Action. \r\n", __FUNCTION__);

        CLI_SET_ERR (QOS_CLI_ERR_ACT_FLG_INVALID);

        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;

        return (QOS_FAILURE);
    }

    /* Check Range */
    switch ((u1InActFlag & u1Type))
    {
        case QOS_PLY_IN_PROF_CONF_IP_DSCP:

            if ((i4Value < QOS_DSCP_MIN_VAL) || (i4Value > QOS_DSCP_MAX_VAL))
            {
                QOS_TRC_ARG4 (MGMT_TRC, "%s : DSCP value %d, is out of range."
                              " The range is Min %d to Max %d.\r\n",
                              __FUNCTION__, i4Value, QOS_DSCP_MIN_VAL,
                              QOS_DSCP_MAX_VAL);

                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;

                i4ReturnValue = QOS_FAILURE;
            }

            break;

        case QOS_PLY_IN_PROF_CONF_IP_TOS:
        case QOS_PLY_IN_PROF_CONF_VLAN_PRI:
        case QOS_PLY_IN_PROF_CONF_INNER_VLAN_PRI:
        case QOS_PLY_IN_PROF_CONF_MPLS_EXP:

            if ((i4Value < QOS_PRI_MIN_VAL) || (i4Value > QOS_PRI_MAX_VAL))
            {
                QOS_TRC_ARG4 (MGMT_TRC, "%s : Priority value %d, is out of "
                              " range. The range is Min %d to Max %d.\r\n",
                              __FUNCTION__, i4Value, QOS_PRI_MIN_VAL,
                              QOS_PRI_MAX_VAL);

                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;

                i4ReturnValue = QOS_FAILURE;
            }

            break;

        case QOS_PLY_IN_PROF_CONF_VLAN_DE:
        case QOS_PLY_IN_PROF_CONF_INNER_VLAN_DE:

            if ((i4Value < QOS_VLAN_DE_MIN_VAL) ||
                (i4Value > QOS_VLAN_DE_MAX_VAL))
            {
                QOS_TRC_ARG4 (MGMT_TRC, "%s : DE value %d, is out of "
                              " range. The range is Min %d to Max %d.\r\n",
                              __FUNCTION__, i4Value, QOS_VLAN_DE_MIN_VAL,
                              QOS_VLAN_DE_MAX_VAL);

                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;

                i4ReturnValue = QOS_FAILURE;
            }

            break;

        default:
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;

            QOS_TRC_ARG1 (MGMT_TRC, "%s : InValid Action Value. Action Flag"
                          " not set for this Action. \r\n", __FUNCTION__);
            i4ReturnValue = QOS_FAILURE;

            break;
    }

    return i4ReturnValue;
}

/*****************************************************************************/
/* Function Name      : QoSValidateInProExcAction                            */
/* Description        : This function is used to validate the In profile Exc */
/*                      action with Action Flag combination.                 */
/*                      It will do the following Action                      */
/*                      1. Get the Policy Map Entry.                         */
/*                      2. Check the Action flag with Give Action, return    */
/*                         Success if true , false otherwise.                */
/* Input(s)           : u4PlyMapId            - Policy Map Id                */
/*                      i4Value               - Action Value.                */
/*                      u2Type                - Action Type.                 */
/*                      pu4ErrorCode          - Pointer to ErrorCode.        */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : gQoSGlobalInfo                                       */
/* Global Variables                                                          */
/* Modified           : none                                                 */
/* Return Value(s)    : QOS_SUCCESS or QOS_FAILURE                           */
/* Called By          : SNMP Low Level                                       */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QoSValidateInProExcAction (UINT4 u4PlyMapId, INT4 i4Value, UINT1 u1Type,
                           UINT4 *pu4ErrorCode)
{
    tQoSPolicyMapNode  *pPlyMapNode = NULL;
    UINT1               u1InProExcActFlag = 0;
    INT4                i4ReturnValue = QOS_SUCCESS;

    pPlyMapNode = QoSUtlGetPolicyMapNode (u4PlyMapId);

    if (pPlyMapNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetPolicyMapNode Returns "
                      "FAILURE. \r\n", __FUNCTION__);

        CLI_SET_ERR (QOS_CLI_ERR_PLY_MAP_NO_ENTRY);

        *pu4ErrorCode = SNMP_ERR_NO_CREATION;

        return (QOS_FAILURE);
    }

    u1InProExcActFlag = pPlyMapNode->u1InProfExcActFlag;

    if ((u1InProExcActFlag & u1Type) != u1Type)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "%s : InValid Action Value. Action Flag"
                      " not set for this Action. \r\n", __FUNCTION__);

        CLI_SET_ERR (QOS_CLI_ERR_ACT_FLG_INVALID);

        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;

        return (QOS_FAILURE);
    }

    /* Check Range */
    switch ((u1InProExcActFlag & u1Type))
    {
        case QOS_PLY_IN_PROF_EXC_IP_DSCP:

            if ((i4Value < QOS_DSCP_MIN_VAL) || (i4Value > QOS_DSCP_MAX_VAL))
            {
                QOS_TRC_ARG4 (MGMT_TRC, "%s : DSCP value %d, is out of range."
                              " The range is Min %d to Max %d.\r\n",
                              __FUNCTION__, i4Value, QOS_DSCP_MIN_VAL,
                              QOS_DSCP_MAX_VAL);

                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;

                i4ReturnValue = QOS_FAILURE;
            }

            break;

        case QOS_PLY_IN_PROF_EXC_IP_TOS:
        case QOS_PLY_IN_PROF_EXC_VLAN_PRI:
        case QOS_PLY_IN_PROF_EXC_INNER_VLAN_PRI:
        case QOS_PLY_IN_PROF_EXC_MPLS_EXP:

            if ((i4Value < QOS_PRI_MIN_VAL) || (i4Value > QOS_PRI_MAX_VAL))
            {
                QOS_TRC_ARG4 (MGMT_TRC, "%s : Priority value %d, is out of "
                              " range. The range is Min %d to Max %d.\r\n",
                              __FUNCTION__, i4Value, QOS_PRI_MIN_VAL,
                              QOS_PRI_MAX_VAL);

                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;

                i4ReturnValue = QOS_FAILURE;
            }

            break;

        case QOS_PLY_IN_PROF_EXC_VLAN_DE:
        case QOS_PLY_IN_PROF_EXC_INNER_VLAN_DE:

            if ((i4Value < QOS_VLAN_DE_MIN_VAL) ||
                (i4Value > QOS_VLAN_DE_MAX_VAL))
            {
                QOS_TRC_ARG4 (MGMT_TRC, "%s : DE value %d, is out of "
                              " range. The range is Min %d to Max %d.\r\n",
                              __FUNCTION__, i4Value, QOS_VLAN_DE_MIN_VAL,
                              QOS_VLAN_DE_MAX_VAL);

                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;

                i4ReturnValue = QOS_FAILURE;
            }

            break;

        default:
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;

            QOS_TRC_ARG1 (MGMT_TRC, "%s : InValid Action Value. Action Flag"
                          " not set for this Action. \r\n", __FUNCTION__);
            i4ReturnValue = QOS_FAILURE;

            break;
    }

    return i4ReturnValue;
}

/*****************************************************************************/
/* Function Name      : QoSValidateOutProfileActionFlag                      */
/* Description        : This function is used to validate the Out profile    */
/*                      action Flag combination. It will do the following    */
/*                      Action                                               */
/*                      1. Check the Action flag Length, it should be = 1   */
/*                      2. Flag can not be '0', Should be within limit       */
/*                      3. Drop Alone can exists.                            */
/*                      4. IP TOS and IP DSCP can not co-exists.             */
/*                      5. VLAN and MPLS can not co-exits.                   */
/* Input(s)           : u4PlyMapId            - Policy Map Id                */
/*                      pSNMPOctSrtOutActFlag - Action Flag Mask Value.      */
/*                      pu4ErrorCode          - Pointer to ErrorCode.        */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : gQoSGlobalInfo                                       */
/* Global Variables                                                          */
/* Modified           : none                                                 */
/* Return Value(s)    : QOS_SUCCESS or QOS_FAILURE                           */
/* Called By          : SNMP Low Level                                       */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QoSValidateOutProfileActionFlag (UINT4 u4PlyMapId,
                                 tSNMP_OCTET_STRING_TYPE *
                                 pSNMPOctSrtOutActFlag, UINT4 *pu4ErrorCode)
{
    tQoSPolicyMapNode  *pPlyMapNode = NULL;
    UINT1               u1OutProActFlag = 0;
    UINT1               u1VlanFlags = 0;

    pPlyMapNode = QoSUtlGetPolicyMapNode (u4PlyMapId);

    if (pPlyMapNode == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (QOS_CLI_ERR_PLY_MAP_NO_ENTRY);
        return (QOS_FAILURE);
    }

    if (pSNMPOctSrtOutActFlag->i4_Length != QOS_POLICY_ACTION_FLAG_LENGTH)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;

        QOS_TRC_ARG2 (MGMT_TRC, "%s : Length of the Action Flag is %d. "
                      "This is Incorrect Value. The Value is = 1. \r\n",
                      __FUNCTION__, pSNMPOctSrtOutActFlag->i4_Length);
        CLI_SET_ERR (QOS_CLI_ERR_ACT_FLG_LEN_INVALID);

        return (QOS_FAILURE);
    }

    MEMCPY (&(u1OutProActFlag), pSNMPOctSrtOutActFlag->pu1_OctetList,
            pSNMPOctSrtOutActFlag->i4_Length);

    /* 1. Can not be '0', Should be within limit. */
    if ((u1OutProActFlag < QOS_PLY_OUT_PROF_ACTION_MIN_VAL) ||
        (u1OutProActFlag > QOS_PLY_OUT_PROF_ACTION_MAX_VAL))
    {
        QOS_TRC_ARG4 (MGMT_TRC, "%s : Out profile action flag value %d, is"
                      " out of range. The range is Min %d to Max %d. \r\n",
                      __FUNCTION__, u1OutProActFlag,
                      QOS_PLY_OUT_PROF_ACTION_MIN_VAL,
                      QOS_PLY_OUT_PROF_ACTION_MAX_VAL);

        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;

        return (QOS_FAILURE);
    }

    /* 2. Drop Alone can exists. */
    if ((((u1OutProActFlag) & QOS_PLY_OUT_PROF_VIO_DROP) ==
         QOS_PLY_OUT_PROF_VIO_DROP) &&
        (u1OutProActFlag != QOS_PLY_OUT_PROF_VIO_DROP))
    {
        QOS_TRC_ARG1 (MGMT_TRC, "%s : Out profile action flag value 'DROP' can"
                      " not club with other flag. \r\n", __FUNCTION__);
        CLI_SET_ERR (QOS_CLI_ERR_DROP_CLUB);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;

        return (QOS_FAILURE);
    }

    /* 3. IP TOS and IP DSCP can not co-exists. */
    if (((u1OutProActFlag & QOS_PLY_OUT_PROF_VIO_IP_TOS) ==
         QOS_PLY_OUT_PROF_VIO_IP_TOS) &&
        (u1OutProActFlag & QOS_PLY_OUT_PROF_VIO_IP_DSCP) ==
        QOS_PLY_OUT_PROF_VIO_IP_DSCP)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "%s : Out profile action flag value 'IP_TOS"
                      " and IP_DSCP' mutually exclusive.\r\n", __FUNCTION__);

        CLI_SET_ERR (QOS_CLI_ERR_TOS_DSCP);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;

        return (QOS_FAILURE);
    }

    u1VlanFlags = QOS_PLY_OUT_PROF_VIO_VLAN_PRI |
        QOS_PLY_OUT_PROF_VIO_VLAN_DE | QOS_PLY_OUT_PROF_VIO_INNER_VLAN_PRI |
        QOS_PLY_OUT_PROF_VIO_INNER_VLAN_DE;

    /* 4. VLAN and MPLS can not co-exits. */
    if ((u1OutProActFlag & u1VlanFlags) &&
        (u1OutProActFlag & QOS_PLY_OUT_PROF_VIO_MPLS_EXP))
    {
        QOS_TRC_ARG1 (MGMT_TRC, "%s : Out profile action flag value 'VLAN and"
                      " MPLS Flags' mutually exclusive.\r\n", __FUNCTION__);

        CLI_SET_ERR (QOS_CLI_ERR_VLAN_MPLS);

        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (QOS_FAILURE);
    }

    return (QOS_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSValidateOutProfileAction                          */
/* Description        : This function is used to validate the Out profile    */
/*                      action with Action Flag combination.                 */
/*                      It will do the following Action                      */
/*                      1. Get the Policy Map Entry.                         */
/*                      2. Check the Action flag with Give Action, return    */
/*                         Success if true , false otherwise.                */
/* Input(s)           : u4PlyMapId            - Policy Map Id                */
/*                      i4Value               - Action Value.                */
/*                      u2Type                - Action Type.                 */
/*                      pu4ErrorCode          - Pointer to ErrorCode.        */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : gQoSGlobalInfo                                       */
/* Global Variables                                                          */
/* Modified           : none                                                 */
/* Return Value(s)    : QOS_SUCCESS or QOS_FAILURE                           */
/* Called By          : SNMP Low Level                                       */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QoSValidateOutProfileAction (UINT4 u4PlyMapId, INT4 i4Value, UINT1 u1Type,
                             UINT4 *pu4ErrorCode)
{
    tQoSPolicyMapNode  *pPlyMapNode = NULL;
    UINT1               u1InActFlag = 0;
    INT4                i4ReturnValue = QOS_SUCCESS;

    pPlyMapNode = QoSUtlGetPolicyMapNode (u4PlyMapId);

    if (pPlyMapNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetPolicyMapNode Returns "
                      "FAILURE. \r\n", __FUNCTION__);

        *pu4ErrorCode = SNMP_ERR_NO_CREATION;

        return (QOS_FAILURE);
    }

    u1InActFlag = pPlyMapNode->u1OutProActionFlag;

    if ((u1InActFlag & u1Type) != u1Type)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "%s : InValid Action Value. Action Flag"
                      " not set for this Action. \r\n", __FUNCTION__);

        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;

        return (QOS_FAILURE);
    }

    /* Check Range */
    switch ((u1InActFlag & u1Type))
    {
        case QOS_PLY_OUT_PROF_VIO_IP_DSCP:

            if ((i4Value < QOS_DSCP_MIN_VAL) || (i4Value > QOS_DSCP_MAX_VAL))
            {
                QOS_TRC_ARG4 (MGMT_TRC, "%s : DSCP value %d, is out of range."
                              " The range is Min %d to Max %d.\r\n",
                              __FUNCTION__, i4Value, QOS_DSCP_MIN_VAL,
                              QOS_DSCP_MAX_VAL);

                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;

                i4ReturnValue = QOS_FAILURE;
            }

            break;

        case QOS_PLY_OUT_PROF_VIO_IP_TOS:
        case QOS_PLY_OUT_PROF_VIO_VLAN_PRI:
        case QOS_PLY_OUT_PROF_VIO_INNER_VLAN_PRI:
        case QOS_PLY_OUT_PROF_VIO_MPLS_EXP:

            if ((i4Value < QOS_PRI_MIN_VAL) || (i4Value > QOS_PRI_MAX_VAL))
            {
                QOS_TRC_ARG4 (MGMT_TRC, "%s : Priority value %d, is out of "
                              " range. The range is Min %d to Max %d.\r\n",
                              __FUNCTION__, i4Value, QOS_PRI_MIN_VAL,
                              QOS_PRI_MAX_VAL);

                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;

                i4ReturnValue = QOS_FAILURE;
            }

            break;

        case QOS_PLY_OUT_PROF_VIO_VLAN_DE:
        case QOS_PLY_OUT_PROF_VIO_INNER_VLAN_DE:

            if ((i4Value < QOS_VLAN_DE_MIN_VAL) ||
                (i4Value > QOS_VLAN_DE_MAX_VAL))
            {
                QOS_TRC_ARG4 (MGMT_TRC, "%s : DE value %d, is out of "
                              " range. The range is Min %d to Max %d.\r\n",
                              __FUNCTION__, i4Value, QOS_VLAN_DE_MIN_VAL,
                              QOS_VLAN_DE_MAX_VAL);

                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;

                i4ReturnValue = QOS_FAILURE;
            }

            break;

        default:
            QOS_TRC_ARG1 (MGMT_TRC, "%s : InValid Action Value. Action Flag"
                          " not set for this Action. \r\n", __FUNCTION__);

            i4ReturnValue = QOS_FAILURE;

            break;
    }

    return i4ReturnValue;
}

/*****************************************************************************/
/* Function Name      : QoSUtlValidatePolicyTblIdxInst                       */
/* Description        : This function is used to Validate the Given Index    */
/*                      instance in the Policy Map Table.                    */
/* Input(s)           : u4FsQoSPolicyMapId - Ply Map ID                      */
/*                      pu4ErrorCode     - Pointer to Error Code             */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : gQoSGlobalInfo                                       */
/* Global Variables                                                          */
/* Modified           : gQoSGlobalInfo                                       */
/* Return Value(s)    : QOS_SUCCESS or QOS_FAILURE                           */
/* Called By          : SNMP Low Level                                       */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QoSUtlValidatePolicyTblIdxInst (UINT4 u4FsQoSPolicyMapId, UINT4 *pu4ErrorCode)
{
    tQoSPolicyMapNode  *pPlyMapNode = NULL;
    INT4                i4RetStatus = QOS_FAILURE;

    if (gQoSGlobalInfo.eSysControl != QOS_SYS_CNTL_START)
    {
        QOS_TRC (MGMT_TRC, QOS_MODULE_SHUTDOW_ERROR_MSG);
        CLI_SET_ERR (QOS_CLI_ERR_MODULE_SHUTDOW);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return (QOS_FAILURE);
    }

    /* Check the Range of Index Values (Min-Max) */
    i4RetStatus = QOS_CHECK_PLY_MAP_TBL_INDEX_RANGE (u4FsQoSPolicyMapId);

    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG3 (MGMT_TRC, "%s : Policy Map Id %d is out of Range."
                      " The Range Should be (1-%d). \r\n", __FUNCTION__,
                      u4FsQoSPolicyMapId, QOS_PLY_MAP_TBL_MAX_INDEX_RANGE);

        CLI_SET_ERR (QOS_CLI_ERR_PLY_MAP_RANGE);

        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;

        return (QOS_FAILURE);
    }

    pPlyMapNode = QoSUtlGetPolicyMapNode (u4FsQoSPolicyMapId);

    if (pPlyMapNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetPolicyMapNode Returns "
                      "FAILURE. \r\n", __FUNCTION__);

        CLI_SET_ERR (QOS_CLI_ERR_PLY_MAP_NO_ENTRY);

        *pu4ErrorCode = SNMP_ERR_NO_CREATION;

        return (QOS_FAILURE);
    }

    return (QOS_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSIsDefPolicyMapTblEntry                            */
/* Description        : This function is used to Validate the Given Index    */
/*                      instance is the default  Policy Map Id.If not        */
/*                      present  return Failure Success otherwise.           */
/* Input(s)           : tQoSPolicyMapNode- pPlyMapNode                       */
/*                                                                           */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/* Return Value(s)    : QOS_SUCCESS or QOS_FAILURE                           */
/* Called By          : SNMP Low Level                                       */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QoSIsDefPolicyMapTblEntry (tQoSPolicyMapNode * pPlyMapNode)
{
    if (pPlyMapNode != NULL)
    {
        if (((pPlyMapNode->u4Id > 0) &&
             (pPlyMapNode->u4Id <= QOS_POL_TBL_DEF_ENTRY_MAX)) &&
            ((pPlyMapNode->u4ClassId > 0) &&
             (pPlyMapNode->u4ClassId <= QOS_POL_TBL_DEF_ENTRY_MAX)) &&
            ((pPlyMapNode->u4MeterTableId == 0)) &&
            ((pPlyMapNode->u4InProActConfNewClassId == 0)) &&
            ((pPlyMapNode->u4InProActExccedNewClassId == 0)) &&
            ((pPlyMapNode->u4InProActExceedActionId == 0)) &&
            ((pPlyMapNode->u4OutProActNewClassId == 0)) &&
            ((pPlyMapNode->u4OutProActionId == 0)) &&
            ((pPlyMapNode->u4InProfileActionId == 0)))
        {
            return (QOS_SUCCESS);
        }
    }

    return (QOS_FAILURE);
}

/*****************************************************************************/
/*                     CLASS INFO TABLE FUNCTIONS                            */
/*****************************************************************************/

/*****************************************************************************/
/* Function Name      : QoSClsInfoCmpFun                                     */
/* Description        : This function is used as a Compare function in the   */
/*                      RBTree.                                              */
/*                      1.It Compares the ClsInfoTableId and return the values*/
/* Input(s)           : e1 , e2 -  Elements of RBTree                        */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    :  -1/0/1                                              */
/* Called By          : SNMP                                                */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QoSClsInfoCmpFun (tRBElem * e1, tRBElem * e2)
{
    tQoSClassInfoNode  *pClsInfoNode1 = NULL;
    tQoSClassInfoNode  *pClsInfoNode2 = NULL;

    pClsInfoNode1 = (tQoSClassInfoNode *) e1;
    pClsInfoNode2 = (tQoSClassInfoNode *) e2;

    if (pClsInfoNode1->u4ClassId < pClsInfoNode2->u4ClassId)
    {
        return (-1);
    }
    else if (pClsInfoNode1->u4ClassId > pClsInfoNode2->u4ClassId)
    {
        return (1);
    }

    return (0);
}

/*****************************************************************************/
/* Function Name      : QoSClsInfoTblRBTFreeNodeFn                           */
/* Description        : This function is used to Delete the ClassInfo Table  */
/*                      Entries and Release the Memory to the respective     */
/*                      Mem Pool .                                           */
/* Input(s)           : tRBElem     - Pointer to ClassInfo Table Entry       */
/*                      u4Arg       - Unused Param                           */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : QOS_SUCCESS /QOS_FAILURE                             */
/* Called By          : SNMP                                                 */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QoSClsInfoTblRBTFreeNodeFn (tRBElem * pRBElem, UINT4 u4Arg)
{
    tQoSClassInfoNode  *pClsInfoNode = NULL;
    tTMO_SLL_NODE      *pCurrNode = NULL;
    tTMO_SLL_NODE      *pNextNode = NULL;
    UINT4               u4RetStatus = MEM_FAILURE;

    UNUSED_PARAM (u4Arg);

    if (pRBElem == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : Class Info Tbl Entry is NULL.\r\n",
                      __FUNCTION__);

        return (QOS_FAILURE);
    }

    pClsInfoNode = (tQoSClassInfoNode *) pRBElem;

    /* Delete All FilterInfo Entry in the ClassInfo Node */
    if ((TMO_SLL_Count (&(pClsInfoNode->SllFltInfo))) > 0)
    {
        UTL_SLL_OFFSET_SCAN (&(pClsInfoNode->SllFltInfo),
                             pCurrNode, pNextNode, tTMO_SLL_NODE *)
        {
            TMO_SLL_Delete (&(pClsInfoNode->SllFltInfo), pCurrNode);
            u4RetStatus =
                MemReleaseMemBlock (gQoSGlobalInfo.QoSFltInfoTblMemPoolId,
                                    (UINT1 *) pCurrNode);
            if (u4RetStatus != MEM_SUCCESS)
            {
                QOS_TRC_ARG1 (MGMT_TRC, "In %s : ERROR: ClsInfoFilterListTbl- "
                              "MemReleaseMemBlock () Failed.\r\n",
                              __FUNCTION__);
            }

        }                        /* End of Scan */

    }                            /* End of if */

    MEMSET (pRBElem, 0, sizeof (tQoSClassInfoNode));

    /* Release the Mem Block */
    u4RetStatus = MemReleaseMemBlock (gQoSGlobalInfo.QoSClsInfoTblMemPoolId,
                                      (UINT1 *) pRBElem);
    if (u4RetStatus != MEM_SUCCESS)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : ClsInfoTbl- "
                      "MemReleaseMemBlock () Failed.\r\n", __FUNCTION__);
    }

    return (QOS_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSCheckAndUpdateVlanMapIfIndex                      */
/* Description        : This function is used to Get the Entry  form  the    */
/*                      ClassInfo Table ,it will do the following Action     */
/*                      1. Call RBTreeGet Utility.                           */
/* Input(s)           : u4ClassId      - Index to ClsInfoNode                */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : gQoSGlobalInfo                                       */
/* Global Variables                                                          */
/* Modified           :                                                      */
/* Return Value(s)    : NULL/ Pointer to 'tQoSClassInfoNode'                 */
/* Called By          : SNMP Low Level                                       */
/* Calling Function   :                                                      */
/*****************************************************************************/
VOID
QoSCheckAndUpdateVlanMapIfIndex (UINT4 u4CLASS, UINT2 *u2VlanId)
{

    tQoSClassInfoNode  *pClsInfoNode = NULL;
    tQoSFilterInfoNode *pFltInfoNode = NULL;
    tQoSClassMapEntry  *pNewClassMapEntry = NULL;

    pClsInfoNode = QoSUtlGetClassInfoNode (u4CLASS);

    if (pClsInfoNode != NULL)
    {
        /* Scan the Filter list in the ClassInfo */
        TMO_SLL_Scan (&(pClsInfoNode->SllFltInfo), pFltInfoNode,
                      tQoSFilterInfoNode *)
        {
            /* Fill the Values for that Entry */
#ifdef NPAPI_WANTED
            pNewClassMapEntry =
                QoSFiltersForClassMapEntry (pFltInfoNode->pClsMapNode);
            if (pNewClassMapEntry->pInVlanMapPtr != NULL)
            {
                *(u2VlanId) = pNewClassMapEntry->pInVlanMapPtr->u2VlanId;
                return;
            }
#else
            UNUSED_PARAM (pNewClassMapEntry);
            UNUSED_PARAM (u2VlanId);
            return;
#endif
        }
    }
}

/*****************************************************************************/
/* Function Name      : QoSUtlGetClassInfoNode                               */
/* Description        : This function is used to Get the Entry  form  the    */
/*                      ClassInfo Table ,it will do the following Action     */
/*                      1. Call RBTreeGet Utility.                           */
/* Input(s)           : u4ClassId      - Index to ClsInfoNode                */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : gQoSGlobalInfo                                       */
/* Global Variables                                                          */
/* Modified           :                                                      */
/* Return Value(s)    : NULL/ Pointer to 'tQoSClassInfoNode'                 */
/* Called By          : SNMP Low Level                                       */
/* Calling Function   :                                                      */
/*****************************************************************************/
tQoSClassInfoNode  *
QoSUtlGetClassInfoNode (UINT4 u4ClassId)
{
    tQoSClassInfoNode   TempClsInfoNode;
    tQoSClassInfoNode  *pClsInfoNode = NULL;

    if (u4ClassId == 0)
    {
        return NULL;
    }

    MEMSET (&TempClsInfoNode, 0, sizeof (tQoSClassInfoNode));
    TempClsInfoNode.u4ClassId = u4ClassId;

    pClsInfoNode = (tQoSClassInfoNode *)
        RBTreeGet (gQoSGlobalInfo.pRbClsInfoTbl, (tRBElem *) & TempClsInfoNode);

    if (pClsInfoNode == NULL)
    {
        QOS_TRC_ARG2 (MGMT_TRC, "%s : Class Id %d is not Yet Create In "
                      "ClassInfoList . \r\n", __FUNCTION__, u4ClassId);

    }

    return (pClsInfoNode);
}

/*****************************************************************************/
/* Function Name      : QoSCreateClsInfoTblEntry                             */
/* Description        : This function is used to Create an Entry in   the    */
/*                      Class Info Table ,it will do the following Action    */
/*                      1. Allocate a memory block for the New Entry         */
/*                      2. Initialize the New Entry                          */
/*                      3. Add this New Entry into the ClassInf Table        */
/* Input(s)           : u4ClassId      - Index to ClsInfoNode                */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : gQoSGlobalInfo                                       */
/* Global Variables                                                          */
/* Modified           : gQoSGlobalInfo                                       */
/* Return Value(s)    : NULL / Pointer to  ClsInfoNode                       */
/* Called By          : SNMP Low Level                                       */
/* Calling Function   :                                                      */
/*****************************************************************************/
tQoSClassInfoNode  *
QoSCreateClsInfoTblEntry (UINT4 u4ClassId)
{
    tQoSClassInfoNode  *pNewClsInfoNode = NULL;
    UINT4               u4RetStatus = QOS_FAILURE;

    /* 1. Allocate a memory block for the New Entry */
    pNewClsInfoNode = (tQoSClassInfoNode *)
        MemAllocMemBlk (gQoSGlobalInfo.QoSClsInfoTblMemPoolId);
    if (pNewClsInfoNode == NULL)
    {
        QOS_TRC_ARG2 (MGMT_TRC, "In %s : MemAllocMemBlk () Failed, "
                      "for ClassId %d. \r\n", __FUNCTION__, u4ClassId);

        return (NULL);
    }

    /* 2. Initialize the New Entry */
    MEMSET (pNewClsInfoNode, 0, sizeof (tQoSClassInfoNode));

    pNewClsInfoNode->u4ClassId = u4ClassId;

    /*Init the SLLs in the Class Info */
    TMO_SLL_Init (&(pNewClsInfoNode->SllFltInfo));

    /* 3. Add this New Entry into the Policy Map Table */
    u4RetStatus = RBTreeAdd (gQoSGlobalInfo.pRbClsInfoTbl,
                             (tRBElem *) pNewClsInfoNode);

    if (u4RetStatus != RB_SUCCESS)
    {
        u4RetStatus =
            MemReleaseMemBlock (gQoSGlobalInfo.QoSClsInfoTblMemPoolId,
                                (UINT1 *) pNewClsInfoNode);
        if (u4RetStatus == MEM_FAILURE)
        {
            /* do nothing - this check is for avoiding coverity warning
             * u4Getval parameter will be used for our requirement.
             */
        }

        if (u4RetStatus == MEM_FAILURE)
        {
            QOS_TRC_ARG1 (MGMT_TRC, "In %s : MemReleaseMemBlock () "
                          "Failed.\r\n", __FUNCTION__);

            return (NULL);
        }

        QOS_TRC_ARG2 (MGMT_TRC, "In %s : RBTreeAdd () Failed ,"
                      "for ClassInfoId %d. \r\n", __FUNCTION__, u4ClassId);

        return (NULL);
    }

    QosUpdateDiffServCLASSNextFree (u4ClassId, QOS_CREATE_AND_WAIT);

    return (pNewClsInfoNode);
}

/*****************************************************************************/
/* Function Name      : QoSDeleteClsInfoTblEntry                              */
/* Description        : This function is used to Delete an Entry from the    */
/*                      Class Info Table ,it will do the following Action    */
/*                      1. Remove the Entry from the Class Info Table        */
/*                      2. Clear the Removed Entry                           */
/*                      3. Release the Removed Entry's memory                */
/* Input(s)           : pClsInfoNode   - Pointer to ClsInfoNode              */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : gQoSGlobalInfo                                       */
/* Global Variables                                                          */
/* Modified           : gQoSGlobalInfo                                       */
/* Return Value(s)    : QOS_SUCCESS or QOS_FAILURE                           */
/* Called By          : SNMP Low Level                                       */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QoSDeleteClsInfoTblEntry (tQoSClassInfoNode * pClsInfoNode)
{
    tQoSClassInfoNode  *pDelClsInfoNode = NULL;
    UINT4               u4RetStatus = QOS_FAILURE;
    UINT4               u4ClassId;

    /* 1. Remove Entry from the  Policy Map Table */
    pDelClsInfoNode = RBTreeRem (gQoSGlobalInfo.pRbClsInfoTbl,
                                 (tRBElem *) pClsInfoNode);
    if (pDelClsInfoNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : RBTreeRem () Failed.\r\n",
                      __FUNCTION__);

        return (QOS_FAILURE);
    }
    u4ClassId = pDelClsInfoNode->u4ClassId;

    /* 2. Clear the Removed Entry */
    MEMSET (pDelClsInfoNode, 0, sizeof (tQoSClassInfoNode));

    /* 3. Release the Removed Entry's memory */
    u4RetStatus = MemReleaseMemBlock (gQoSGlobalInfo.QoSClsInfoTblMemPoolId,
                                      (UINT1 *) pDelClsInfoNode);
    if (u4RetStatus == MEM_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : MemReleaseMemBlock () Failed. "
                      "\r\n", __FUNCTION__);

        return (QOS_FAILURE);
    }

    QosUpdateDiffServCLASSNextFree (u4ClassId, DESTROY);

    return (QOS_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSGetNextClsInfoTblEntryIndex                       */
/* Description        : This function is used to Get the Next Entry from the */
/*                      Class Info Table ,it will do the following Action    */
/*                      1. If u4CurrentIndex is 0 then  GetFirxtIndex.       */
/*                      2. If u4CurrentIndex is not 0 then  GetNextIndex.    */
/* Input(s)           : u4CurrentIndex  - Current index id                   */
/*                      pu4NextIndex    - Pointer to NextIndex Id            */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : gQoSGlobalInfo                                       */
/* Global Variables                                                          */
/* Modified           : gQoSGlobalInfo                                       */
/* Return Value(s)    : QOS_SUCCESS or QOS_FAILURE                           */
/* Called By          : SNMP Low Level                                       */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QoSGetNextClsInfoTblEntryIndex (UINT4 u4CurrentIndex, UINT4 *pu4NextIndex)
{
    tQoSClassInfoNode  *pClsInfoNode = NULL;
    tQoSClassInfoNode   ClsInfoNode;

    if (u4CurrentIndex == 0)
    {
        pClsInfoNode = (tQoSClassInfoNode *)
            RBTreeGetFirst (gQoSGlobalInfo.pRbClsInfoTbl);
    }
    else
    {
        ClsInfoNode.u4ClassId = u4CurrentIndex;
        pClsInfoNode = (tQoSClassInfoNode *)
            RBTreeGetNext (gQoSGlobalInfo.pRbClsInfoTbl,
                           &ClsInfoNode, QoSClsInfoCmpFun);
    }

    if (pClsInfoNode == NULL)
    {
        QOS_TRC_ARG2 (MGMT_TRC, "In %s : RBTreeGetNext () Failed, "
                      " for ClsInfoId %d.\r\n", __FUNCTION__, u4CurrentIndex);

        return (QOS_FAILURE);
    }

    *pu4NextIndex = pClsInfoNode->u4ClassId;

    return (QOS_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSClsInfoAddFilters                                 */
/* Description        : This function is used to Add  the Class Map Entry    */
/*                      Address in the Class Info Node                       */
/*                      1. If the Given Class does not exits Create.         */
/*                      2. Add the Filters into the CLASS                    */
/* Input(s)           : u4CLASS         - CLASSS                             */
/*                      pClsMapNode     - Pointer to ClassMap Tabel Entry    */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : gQoSGlobalInfo                                       */
/* Global Variables                                                          */
/* Modified           : gQoSGlobalInfo                                       */
/* Return Value(s)    : QOS_SUCCESS or QOS_FAILURE                           */
/* Called By          : SNMP Low Level                                       */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QoSClsInfoAddFilters (UINT4 u4CLASS, tQoSClassMapNode * pClsMapNode)
{
    tQoSClassInfoNode  *pClsInfoNode = NULL;
    tQoSFilterInfoNode *pFltInfoNode = NULL;
    tQoSFilterInfoNode *pTmpFltInfoNode = NULL;
    tQoSFilterInfoNode *pPrevFltInfoNode = NULL;

    /* Check the Given Class is Exists, if not Create new Class info. */
    pClsInfoNode = QoSUtlGetClassInfoNode (u4CLASS);

    if (pClsInfoNode == NULL)
    {
        /* Create new Class Info node */
        pClsInfoNode = QoSCreateClsInfoTblEntry (u4CLASS);

        if (pClsInfoNode == NULL)
        {
            QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSCreateClsInfoTblEntry ()"
                          " Failed.\r\n", __FUNCTION__);

            return (QOS_FAILURE);
        }
    }

    /* Allocate Memory for Entry */
    pFltInfoNode = (tQoSFilterInfoNode *)
        MemAllocMemBlk (gQoSGlobalInfo.QoSFltInfoTblMemPoolId);
    if (pFltInfoNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : MemAllocMemBlk () Failed \r\n",
                      __FUNCTION__);

        return (QOS_FAILURE);
    }

    /* 2. Initialize the New Entry */
    MEMSET (pFltInfoNode, 0, sizeof (tQoSFilterInfoNode));

    TMO_SLL_Init_Node (&(pFltInfoNode->NextNode));

    pFltInfoNode->pClsMapNode = pClsMapNode;

    /*Add the Given Filters into the Class */
    TMO_SLL_Scan (&(pClsInfoNode->SllFltInfo), pTmpFltInfoNode,
                  tQoSFilterInfoNode *)
    {
        if (pTmpFltInfoNode->pClsMapNode->u4Id == pClsMapNode->u4Id)
        {
            /* same node comes for addition. silently ignore */
            MemReleaseMemBlock (gQoSGlobalInfo.QoSFltInfoTblMemPoolId,
                                (UINT1 *) pFltInfoNode);
            return (QOS_SUCCESS);
        }

        /* Check for Previous Node */
        if (pTmpFltInfoNode->pClsMapNode->u4Id > pClsMapNode->u4Id)
        {
            break;
        }
        pPrevFltInfoNode = pTmpFltInfoNode;

    }                            /* End of Scan */

    TMO_SLL_Insert (&(pClsInfoNode->SllFltInfo),
                    &(pPrevFltInfoNode->NextNode), &(pFltInfoNode->NextNode));

    return (QOS_SUCCESS);

}

/*****************************************************************************/
/* Function Name      : QoSClsInfoDelFilters                                 */
/* Description        : This function is used to Delete Given Class Map Entry*/
/*                      Address From the Class Info Node.                    */
/*                      1. Delete the Filters from the CLASS Info Node       */
/* Input(s)           : pClsMapNode     - Pointer to ClassMap Tabel Entry    */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : gQoSGlobalInfo                                       */
/* Global Variables                                                          */
/* Modified           : gQoSGlobalInfo                                       */
/* Return Value(s)    : QOS_SUCCESS or QOS_FAILURE                           */
/* Called By          : SNMP Low Level                                       */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QoSClsInfoDelFilters (tQoSClassMapNode * pClsMapNode)
{
    tQoSClassInfoNode  *pClsInfoNode = NULL;
    tQoSFilterInfoNode *pFltInfoNode = NULL;
    tQoSFilterInfoNode *pTempFltInfoNode = NULL;
    UINT4               u4RetStatus = QOS_FAILURE;

    if (pClsMapNode == NULL)
    {
        return (QOS_FAILURE);
    }

    if (pClsMapNode->u4ClassId == 0)
    {
        return (QOS_SUCCESS);
    }

    /* Check the Given Class is Exists or not */
    pClsInfoNode = QoSUtlGetClassInfoNode (pClsMapNode->u4ClassId);

    if (pClsInfoNode == NULL)
    {
        QOS_TRC_ARG2 (MGMT_TRC, "In %s : QoSUtlGetClassInfoNode () Failed."
                      " CLASS %d not foumd.\r\n", __FUNCTION__,
                      pClsMapNode->u4ClassId);

        return (QOS_FAILURE);
    }

    UTL_SLL_OFFSET_SCAN (&(pClsInfoNode->SllFltInfo), pFltInfoNode,
                         pTempFltInfoNode, tQoSFilterInfoNode *)
    {
        /*Delete the Given Filters from the ClassInfo Node */
        if (pFltInfoNode->pClsMapNode == pClsMapNode)
        {
            TMO_SLL_Delete (&(pClsInfoNode->SllFltInfo),
                            &(pFltInfoNode->NextNode));

            pFltInfoNode->pClsMapNode = NULL;

            break;
        }
    }

    /* Release the Memory */
    u4RetStatus = MemReleaseMemBlock (gQoSGlobalInfo.QoSFltInfoTblMemPoolId,
                                      (UINT1 *) pFltInfoNode);
    if (u4RetStatus == MEM_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : MemReleaseMemBlock () Failed.\r\n",
                      __FUNCTION__);

        return (QOS_FAILURE);
    }

    return (QOS_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSTestClsInfoDelFilters                             */
/* Description        : This function is used to Test  Given Class Map Entry */
/*                      Can be delete or not.                                */
/*                        If the Class having Entry In the Policy Map Table  */
/*                      and  No of ClassMap Entry is 1 then return Failure   */
/*                      otherwise Return Success                             */
/* Input(s)           : pClsMapNode     - Pointer to ClassMap Tabel Entry;
 *                      i4RowStatus     - Variable to store the value of 
 *                      Rowstatus    */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : gQoSGlobalInfo                                       */
/* Global Variables                                                          */
/* Modified           : gQoSGlobalInfo                                       */
/* Return Value(s)    : QOS_SUCCESS or QOS_FAILURE                           */
/* Called By          : SNMP Low Level                                       */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QoSTestClsInfoDelFilters (tQoSClassMapNode * pClsMapNode, INT4 i4RowStatus)
{
    tQoSClassInfoNode  *pClsInfoNode = NULL;

    if (pClsMapNode == NULL)
    {
        return (QOS_FAILURE);
    }

    if (pClsMapNode->u4ClassId == 0)
    {
        /* Do Nothing */
        return (QOS_SUCCESS);
    }

    /* Check the Given Class is Exists or not */
    pClsInfoNode = QoSUtlGetClassInfoNode (pClsMapNode->u4ClassId);

    if (pClsInfoNode == NULL)
    {
        QOS_TRC_ARG2 (MGMT_TRC, "In %s : QoSUtlGetClassInfoNode () Failed."
                      " ClassId %d not foumd.\r\n", __FUNCTION__,
                      pClsMapNode->u4ClassId);

        return (QOS_FAILURE);
    }
    /*If the class is mapped with any policy map,then return failure */
    if ((i4RowStatus == DESTROY) && (pClsInfoNode->pPlyMapNode != NULL)
        && (pClsMapNode->u4ClassId != QOS_CM_TBL_DEF_CLASS))
    {
        return (QOS_FAILURE);
    }
    return (QOS_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSClsInfoAddPolicy                                  */
/* Description        : This function is used to Add a Given Policy Map Entry*/
/*                      into the ClassInfo Node.                             */
/* Input(s)           : u4ClassId       - CLASS Id for the Given Policy      */
/*                    : pPlyMapNode     - Pointer to Policy Map Tabel Entry  */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : gQoSGlobalInfo                                       */
/* Global Variables                                                          */
/* Modified           : gQoSGlobalInfo                                       */
/* Return Value(s)    : QOS_SUCCESS or QOS_FAILURE                           */
/* Called By          : SNMP Low Level                                       */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QoSClsInfoAddPolicy (UINT4 u4ClassId, tQoSPolicyMapNode * pPlyMapNode)
{
    tQoSClassInfoNode  *pClsInfoNode = NULL;

    pClsInfoNode = QoSUtlGetClassInfoNode (u4ClassId);
    if (pClsInfoNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetClassInfoNode () "
                      " Failed.\r\n", __FUNCTION__);

        return (QOS_FAILURE);
    }

    pClsInfoNode->pPlyMapNode = pPlyMapNode;

    return (QOS_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSClsInfoDelPolicy                                  */
/* Description        : This function is used to Delete a Given Policy Map   */
/*                      Entry from the ClassInfo Node.                       */
/*                    : pPlyMapNode     - Pointer to Policy Map Tabel Entry  */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : gQoSGlobalInfo                                       */
/* Global Variables                                                          */
/* Modified           : gQoSGlobalInfo                                       */
/* Return Value(s)    : QOS_SUCCESS or QOS_FAILURE                           */
/* Called By          : SNMP Low Level                                       */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QoSClsInfoDelPolicy (tQoSPolicyMapNode * pPlyMapNode)
{
    tQoSClassInfoNode  *pClsInfoNode = NULL;

    if (pPlyMapNode == NULL)
    {
        return (QOS_FAILURE);
    }

    if (pPlyMapNode->u4ClassId == 0)
    {
        /* Policy is interface specific and not for CLASS */
        /* Do nothing */
        return QOS_SUCCESS;
    }

    /* Check the Given Class is Exists or not */
    pClsInfoNode = QoSUtlGetClassInfoNode (pPlyMapNode->u4ClassId);

    if (pClsInfoNode == NULL)
    {
        QOS_TRC_ARG2 (MGMT_TRC, "In %s : QoSUtlGetClassInfoNode () Failed."
                      " ClassId %d not foumd.\r\n", __FUNCTION__,
                      pPlyMapNode->u4ClassId);

        return (QOS_FAILURE);
    }

    pClsInfoNode->pPlyMapNode = NULL;

    return (QOS_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSTestClsInfoAddPolicy                              */
/* Description        : This function is used to Test a Given Policy Map     */
/*                      Entry is already mapped with the another Policy.     */
/* Input(s)           : u4ClassId       - CLASS Id for the Given Policy      */
/*                    : u4PlyMapId      - Policy Map Id                      */
/*                      pu4ErrorCode     - Pointer to Error Code             */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : gQoSGlobalInfo                                       */
/* Global Variables                                                          */
/* Modified           : gQoSGlobalInfo                                       */
/* Return Value(s)    : QOS_SUCCESS or QOS_FAILURE                           */
/* Called By          : SNMP Low Level                                       */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QoSTestClsInfoAddPolicy (UINT4 u4ClassId, UINT4 u4PlyMapId, UINT4 *pu4ErrorCode)
{
    tQoSClassInfoNode  *pClsInfoNode = NULL;
    tQoSPolicyMapNode  *pPlyMapNode = NULL;

    pClsInfoNode = QoSUtlGetClassInfoNode (u4ClassId);
    if (pClsInfoNode == NULL)
    {
        return (QOS_SUCCESS);
    }

    pPlyMapNode = QoSUtlGetPolicyMapNode (u4PlyMapId);
    if (pPlyMapNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetPolicyMapNode () "
                      " Failed.\r\n", __FUNCTION__);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (QOS_FAILURE);
    }
    if ((u4PlyMapId == 1) && (pPlyMapNode->u4ClassId == QOS_CM_TBL_DEF_CLASS))
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : ERROR : Default Policy Map  "
                      " cannot be modified.\r\n", __FUNCTION__);
        CLI_SET_ERR (QOS_CLI_ERR_DEF_PLY_MAP);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return (QOS_FAILURE);
    }

    if ((pClsInfoNode->pPlyMapNode != NULL) &&
        (pClsInfoNode->pPlyMapNode != pPlyMapNode))
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : ERROR : CLASS is already Mapped with"
                      " another PolicyMapEntry.\r\n", __FUNCTION__);
        CLI_SET_ERR (QOS_CLI_ERR_PLY_MULTIMAP);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return (QOS_FAILURE);
    }

    return (QOS_SUCCESS);
}

/*****************************************************************************/
/*                   Q TEMPLATE TABLE FUNCTIONS                              */
/*****************************************************************************/
/*****************************************************************************/
/* Function Name      : QoSUtlGetQTempNode                                   */
/* Description        : This function is used to Get the Entry  form  the    */
/*                      Q Template Table ,it will do the following Action    */
/*                      1. It will Scan the SLL.                             */
/* Input(s)           : u4QTempId      - Index to QTempNode                  */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : gQoSGlobalInfo                                       */
/* Global Variables                                                          */
/* Modified           :                                                      */
/* Return Value(s)    : NULL/ Pointer to 'tQoSQTemplateNode'                 */
/* Called By          : SNMP Low Level                                       */
/* Calling Function   :                                                      */
/*****************************************************************************/
tQoSQTemplateNode  *
QoSUtlGetQTempNode (UINT4 u4QTempId)
{
    tQoSQTemplateNode  *pQTempNode = NULL;

    TMO_SLL_Scan (&(gQoSGlobalInfo.SllQTempTbl), pQTempNode,
                  tQoSQTemplateNode *)
    {
        if (pQTempNode->u4Id == u4QTempId)
        {
            return (pQTempNode);
        }

    }

    QOS_TRC_ARG2 (MGMT_TRC, "%s : Q Template Id %d is not Found in the "
                  "Q Template Table. \r\n", __FUNCTION__, u4QTempId);

    return (NULL);

}

/*****************************************************************************/
/* Function Name      : QoSGetNextQTempTblEntryIndex                         */
/* Description        : This function is used to Get the Next Entry from the */
/*                      Q Template Table ,it will do the following Action    */
/*                      1. If u4CurrentIndex is 0 then  GetFirxtIndex.       */
/*                      2. If u4CurrentIndex is not 0 then  GetNextIndex.    */
/* Input(s)           : u4CurrentIndex  - Current index id                   */
/*                      pu4NextIndex    - Pointer to NextIndex Id            */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : gQoSGlobalInfo                                       */
/* Global Variables                                                          */
/* Modified           : gQoSGlobalInfo                                       */
/* Return Value(s)    : QOS_SUCCESS or QOS_FAILURE                           */
/* Called By          : SNMP Low Level                                       */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QoSGetNextQTempTblEntryIndex (UINT4 u4CurrentIndex, UINT4 *pu4NextIndex)
{
    tQoSQTemplateNode  *pQTempNode = NULL;
    UINT4               u4NodeFound = QOS_FAILURE;

    if (u4CurrentIndex == 0)
    {
        pQTempNode = (tQoSQTemplateNode *)
            TMO_SLL_First (&(gQoSGlobalInfo.SllQTempTbl));
    }
    else
    {
        TMO_SLL_Scan (&(gQoSGlobalInfo.SllQTempTbl), pQTempNode,
                      tQoSQTemplateNode *)
        {
            /* If the u4CurrentIndex is not found give the Next greater */
            if ((u4NodeFound == QOS_SUCCESS) &&
                (pQTempNode->u4Id > u4CurrentIndex))
            {
                *pu4NextIndex = pQTempNode->u4Id;
                return (QOS_SUCCESS);
            }

            if (pQTempNode->u4Id == u4CurrentIndex)
            {
                u4NodeFound = QOS_SUCCESS;
            }

        }                        /* End of Scan */

    }                            /* End of Else */

    if (pQTempNode == NULL)
    {
        QOS_TRC_ARG2 (MGMT_TRC, "In %s : GetNext () Failed, "
                      " for QTempId %d.\r\n", __FUNCTION__, u4CurrentIndex);
        return (QOS_FAILURE);
    }

    *pu4NextIndex = pQTempNode->u4Id;

    return (QOS_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSCreateQTempTblEntry                               */
/* Description        : This function is used to Create an Entry in the      */
/*                      Q Template Table ,it will do the following Action    */
/*                      1. Allocate a memory block for the New Entry         */
/*                      2. Initialize the New Entry                          */
/*                      3. Add this New Entry into the Q Template Table      */
/* Input(s)           : u4QTemplateId   - Index to Q Template Table Entry    */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : gQoSGlobalInfo                                       */
/* Global Variables                                                          */
/* Modified           : gQoSGlobalInfo                                       */
/* Return Value(s)    : NULL / Pointer to  tQoSQTemplateNode                 */
/* Called By          : SNMP Low Level                                       */
/* Calling Function   :                                                      */
/*****************************************************************************/
tQoSQTemplateNode  *
QoSCreateQTempTblEntry (UINT4 u4QTemplateId)
{
    tQoSQTemplateNode  *pQTempNode = NULL;
    tQoSQTemplateNode  *pNewQTempNode = NULL;
    tQoSQTemplateNode  *pPrevQTempNode = NULL;

    /* 1. Allocate a memory block for the New Entry */
    pNewQTempNode = (tQoSQTemplateNode *)
        MemAllocMemBlk (gQoSGlobalInfo.QoSQTempTblMemPoolId);
    if (pNewQTempNode == NULL)
    {
        QOS_TRC_ARG2 (MGMT_TRC, "In %s : MemAllocMemBlk () Failed, "
                      "for QTempId %d. \r\n", __FUNCTION__, u4QTemplateId);

        return (NULL);
    }

    /* 2. Initialize the New Entry */
    MEMSET (pNewQTempNode, 0, sizeof (tQoSQTemplateNode));

    SPRINTF ((CHR1 *) & (pNewQTempNode->au1Name[0]), "QT-%u", u4QTemplateId);

    pNewQTempNode->u4Id = u4QTemplateId;
    pNewQTempNode->u4Size = QOS_Q_TEMP_SIZE_DEFAULT;
    pNewQTempNode->u2DropType = QOS_Q_TEMP_DROP_TYPE_DEFAULT;
    pNewQTempNode->u1DropAlgoEnableFlag = QOS_Q_TEMP_DROP_ALGO_DEFAULT;
    pNewQTempNode->i4StorageType = QOS_STORAGE_NONVOLATILE;
    pNewQTempNode->u1Status = NOT_IN_SERVICE;

    /* 3. Add this New Entry into the Q Template Table */
    TMO_SLL_Scan (&(gQoSGlobalInfo.SllQTempTbl), pQTempNode,
                  tQoSQTemplateNode *)
    {
        /* Check for Previous Node */
        if (pQTempNode->u4Id > u4QTemplateId)
        {
            break;
        }

        pPrevQTempNode = pQTempNode;

    }                            /* End of Scan */

    /* 3. Add this New Entry into the Q Template Table */
    TMO_SLL_Insert (&(gQoSGlobalInfo.SllQTempTbl),
                    &(pPrevQTempNode->NextNode), &(pNewQTempNode->NextNode));

    return (pNewQTempNode);
}

/*****************************************************************************/
/* Function Name      : QoSValidateDeleteQTempTblEntry                       */
/* Description        : This function is used to validate the Deletion       */
/*                       an Entry from the Q Template Table                  */
/* Input(s)           : pQTempNode   - Pointer to tQoSQTemplateNode          */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : gQoSGlobalInfo                                       */
/* Global Variables                                                          */
/* Modified           : gQoSGlobalInfo                                       */
/* Return Value(s)    : QOS_SUCCESS or QOS_FAILURE                           */
/* Called By          : SNMP Low Level                                       */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QoSValidateDeleteQTempTblEntry (tQoSQTemplateNode * pQTempNode)
{
    UINT4               u4Idx = 0;
    INT4                i4DP = 0;
    INT4                i4RowStatus = 0;

    if (pQTempNode == NULL)
    {
        return (QOS_FAILURE);
    }

    u4Idx = pQTempNode->u4Id;

    /* Check if any Random detect entries are present */
    do
    {
        if (nmhGetFsQoSRandomDetectCfgStatus (u4Idx, i4DP, &i4RowStatus) !=
            SNMP_FAILURE)
        {
            /* If there is an ACTIVE RD entry with the QTemplate ID do not allow
             * the QTemplate to be deleted */
            if ((i4RowStatus == ACTIVE) && (u4Idx == pQTempNode->u4Id))
            {
                QOS_TRC_ARG1 (MGMT_TRC,
                              "In %s : QoSUtlValidateQTempTblIdxInst () "
                              "Returns FAILURE. \r\n", __FUNCTION__);
                return (QOS_FAILURE);
            }
        }
    }
    while (nmhGetNextIndexFsQoSRandomDetectCfgTable
           (u4Idx, &u4Idx, i4DP, &i4DP) == SNMP_SUCCESS);

    return (QOS_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSDeleteQTempTblEntry                               */
/* Description        : This function is used to Delete an Entry from the    */
/*                      Q Template Table ,it will do the following Action    */
/*                      1. Remove the Entry from the Q Template Table        */
/*                      2. Clear the Removed Entry                           */
/*                      3. Release the Removed Entry's memory                */
/* Input(s)           : pQTempNode   - Pointer to tQoSQTemplateNode          */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : gQoSGlobalInfo                                       */
/* Global Variables                                                          */
/* Modified           : gQoSGlobalInfo                                       */
/* Return Value(s)    : QOS_SUCCESS or QOS_FAILURE                           */
/* Called By          : SNMP Low Level                                       */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QoSDeleteQTempTblEntry (tQoSQTemplateNode * pQTempNode)
{
    UINT4               u4RetStatus = QOS_FAILURE;

    if (pQTempNode == NULL)
    {
        return (QOS_FAILURE);
    }

    /* Delete the Given Node */
    TMO_SLL_Delete (&(gQoSGlobalInfo.SllQTempTbl), &(pQTempNode->NextNode));

    /* 2. Clear the Removed Entry */
    MEMSET (pQTempNode, 0, sizeof (tQoSQTemplateNode));

    /* 3. Release the Removed Entry's memory */
    u4RetStatus = MemReleaseMemBlock (gQoSGlobalInfo.QoSQTempTblMemPoolId,
                                      (UINT1 *) pQTempNode);
    if (u4RetStatus == MEM_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : MemReleaseMemBlock () Failed.\r\n",
                      __FUNCTION__);

        return (QOS_FAILURE);
    }

    return (QOS_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSUtlValidateQTempTblIdxInst                        */
/* Description        : This function is used to Validate the Given Index    */
/*                      instance in the Q Template Table, if it is ACTIVE    */
/*                      return Failure Success otherwise.                    */
/* Input(s)           : u4FsQoSQTemplateId- Q Template ID                    */
/*                      pu4ErrorCode      - Pointer to Error Code            */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : gQoSGlobalInfo                                       */
/* Global Variables                                                          */
/* Modified           : gQoSGlobalInfo                                       */
/* Return Value(s)    : QOS_SUCCESS or QOS_FAILURE                           */
/* Called By          : SNMP Low Level                                       */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QoSUtlValidateQTempTblIdxInst (UINT4 u4FsQoSQTemplateId, UINT4 *pu4ErrorCode)
{
    tQoSQTemplateNode  *pQTempNode = NULL;
    INT4                i4RetStatus = 0;

    if (gQoSGlobalInfo.eSysControl != QOS_SYS_CNTL_START)
    {
        QOS_TRC (MGMT_TRC, QOS_MODULE_SHUTDOW_ERROR_MSG);
        CLI_SET_ERR (QOS_CLI_ERR_MODULE_SHUTDOW);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return (QOS_FAILURE);
    }

    /* Check the Range of Index Values (Min-Max) */
    i4RetStatus = QOS_CHECK_Q_TEMP_TBL_INDEX_RANGE (u4FsQoSQTemplateId);
    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG3 (MGMT_TRC, "%s : Q Template Id %d is out of Range."
                      " The Range Should be (1-%d). \r\n", __FUNCTION__,
                      u4FsQoSQTemplateId, QOS_Q_TEMP_TBL_MAX_INDEX_RANGE);
        CLI_SET_ERR (QOS_CLI_ERR_QTEMP_RANGE);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (QOS_FAILURE);
    }

    pQTempNode = QoSUtlGetQTempNode (u4FsQoSQTemplateId);

    if (pQTempNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetQTempNode () "
                      "Returns FAILURE. \r\n", __FUNCTION__);
        CLI_SET_ERR (QOS_CLI_ERR_QTEMP_NOT_CREATION);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (QOS_FAILURE);
    }

    if (pQTempNode->u1Status == ACTIVE)
    {
        QOS_TRC (MGMT_TRC, "Can not change the value in ACTIVE State.\r\n");

        CLI_SET_ERR (QOS_CLI_ERR_STATUS_ACTIVE);

        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return (QOS_FAILURE);
    }

    return (QOS_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSIsDefQTempTblEntry                                */
/* Description        : This function is used to Validate the Given Index    */
/*                      instance is the default  QueueTemplate Id.If not     */
/*                      present  return Failure Success otherwise.           */
/* Input(s)           : tQoSQTemplateNode- pQTempNode                        */
/*                                                                           */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/* Return Value(s)    : QOS_SUCCESS or QOS_FAILURE                           */
/* Called By          : SNMP Low Level                                       */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QoSIsDefQTempTblEntry (tQoSQTemplateNode * pQTempNode)
{
    if (pQTempNode != NULL)
    {
        if ((pQTempNode->u4Id > 0) &&
            (pQTempNode->u4Id <= QOS_QT_TBL_DEF_ENTRY_MAX))
        {
            return (QOS_SUCCESS);
        }
    }

    return (QOS_FAILURE);
}

/*****************************************************************************/
/*                  RD CFG TABLE FUNCTIONS                                   */
/*****************************************************************************/

/*****************************************************************************/
/* Function Name      : QoSUtlGetRDCfgNode                                   */
/* Description        : This function is used to Get the Entry  form  the    */
/*                      RD Cfg Table ,it will do the following Action        */
/*                      1. It will Scan the SLL.                             */
/* Input(s)           : u4FsQoSQTemplateId - Index to QTempId                */
/*                    : i4FsQoSRandomDetectCfgDP - DP Value                  */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : gQoSGlobalInfo                                       */
/* Global Variables                                                          */
/* Modified           :                                                      */
/* Return Value(s)    : NULL/ Pointer to 'tQoSRDCfgNode'                     */
/* Called By          : SNMP Low Level                                       */
/* Calling Function   :                                                      */
/*****************************************************************************/
tQoSRDCfgNode      *
QoSUtlGetRDCfgNode (UINT4 u4FsQoSQTemplateId, INT4 i4FsQoSRandomDetectCfgDP)
{
    tQoSRDCfgNode      *pRDCfgNode = NULL;

    TMO_SLL_Scan (&(gQoSGlobalInfo.SllRDTbl), pRDCfgNode, tQoSRDCfgNode *)
    {
        if ((pRDCfgNode->u4QTemplateId == u4FsQoSQTemplateId) &&
            (pRDCfgNode->u1DP == i4FsQoSRandomDetectCfgDP))
        {
            return (pRDCfgNode);
        }
    }

    QOS_TRC_ARG3 (MGMT_TRC, "%s : RD QTemp Id %d, DP %d  is not Found in the "
                  "RD Cfg  Table. \r\n", __FUNCTION__,
                  u4FsQoSQTemplateId, i4FsQoSRandomDetectCfgDP);

    return (NULL);

}

/*****************************************************************************/
/* Function Name      : QoSGetNextRDCfgTblEntryIndex                         */
/* Description        : This function is used to Get the Next Entry from the */
/*                      RD Cfg Table ,it will do the following Action        */
/*                      1. If u4CurrentIndex is 0 then  GetFirxtIndex.       */
/*                      2. If u4CurrentIndex is not 0 then  GetNextIndex.    */
/* Input(s)           : u4CurrentIndex1 - Current index id (QTempId)         */
/*                    : i4CurrentIndex2 - Current index id (DP Valude)       */
/*                      pu4NextIndex1   - Pointer to NextIndex1 Id           */
/*                      pi4NextIndex2   - Pointer to NextIndex2 Id           */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : gQoSGlobalInfo                                       */
/* Global Variables                                                          */
/* Modified           : gQoSGlobalInfo                                       */
/* Return Value(s)    : QOS_SUCCESS or QOS_FAILURE                           */
/* Called By          : SNMP Low Level                                       */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QoSGetNextRDCfgTblEntryIndex (UINT4 u4CurrentIndex1, UINT4 *pu4NextIndex1,
                              INT4 i4CurrentIndex2, INT4 *pi4NextIndex2)
{
    tQoSRDCfgNode      *pRDCfgNode = NULL;
    UINT4               u4NodeFound = QOS_FAILURE;

    if ((u4CurrentIndex1 == 0) && (i4CurrentIndex2 == 0))
    {
        pRDCfgNode = (tQoSRDCfgNode *)
            TMO_SLL_First (&(gQoSGlobalInfo.SllRDTbl));
    }
    else
    {
        TMO_SLL_Scan (&(gQoSGlobalInfo.SllRDTbl), pRDCfgNode, tQoSRDCfgNode *)
        {
            /* If the u4CurrentIndex is not found give the Next greater */
            if ((u4NodeFound == QOS_SUCCESS) ||
                ((pRDCfgNode->u4QTemplateId == u4CurrentIndex1) &&
                 (pRDCfgNode->u1DP > i4CurrentIndex2)) ||
                (pRDCfgNode->u4QTemplateId > u4CurrentIndex1))
            {
                *pu4NextIndex1 = pRDCfgNode->u4QTemplateId;
                *pi4NextIndex2 = pRDCfgNode->u1DP;
                return (QOS_SUCCESS);
            }

            if ((pRDCfgNode->u4QTemplateId == u4CurrentIndex1) &&
                (pRDCfgNode->u1DP == (UINT1) i4CurrentIndex2))
            {
                u4NodeFound = QOS_SUCCESS;
            }

        }                        /* End of Scan */

    }                            /* End of Else */

    if (pRDCfgNode == NULL)
    {
        QOS_TRC_ARG3 (MGMT_TRC, "In %s : GetNext () Failed, "
                      " for RDCfgId QTemp %d DP %d .\r\n", __FUNCTION__,
                      u4CurrentIndex1, i4CurrentIndex2);
        return (QOS_FAILURE);
    }

    *pu4NextIndex1 = pRDCfgNode->u4QTemplateId;
    *pi4NextIndex2 = pRDCfgNode->u1DP;

    return (QOS_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSCreateRDCfgTblEntry                               */
/* Description        : This function is used to Create an Entry in the      */
/*                      RD Cfg Table ,it will do the following Action        */
/*                      1. Allocate a memory block for the New Entry         */
/*                      2. Initialize the New Entry                          */
/*                      3. Add this New Entry into the RD Cfg Table          */
/* Input(s)           : u4FsQoSQTemplateId - Q Template Id                   */
/*                    : i4FsQoSRandomDetectCfgDP - DP Value for a QTempID    */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : gQoSGlobalInfo                                       */
/* Global Variables                                                          */
/* Modified           : gQoSGlobalInfo                                       */
/* Return Value(s)    : NULL / Pointer to  tQoSRDCfgNode                     */
/* Called By          : SNMP Low Level                                       */
/* Calling Function   :                                                      */
/*****************************************************************************/
tQoSRDCfgNode      *
QoSCreateRDCfgTblEntry (UINT4 u4FsQoSQTemplateId, INT4 i4FsQoSRandomDetectCfgDP)
{
    tQoSRDCfgNode      *pRDCfgNode = NULL;
    tQoSRDCfgNode      *pNewRDCfgNode = NULL;
    tQoSRDCfgNode      *pPrevRDCfgNode = NULL;

    /* 1. Allocate a memory block for the New Entry */
    pNewRDCfgNode = (tQoSRDCfgNode *)
        MemAllocMemBlk (gQoSGlobalInfo.QoSRDTblMemPoolId);
    if (pNewRDCfgNode == NULL)
    {
        QOS_TRC_ARG3 (MGMT_TRC, "In %s : MemAllocMemBlk () Failed, for "
                      "RD QTemp  %d , DP %d. \r\n", __FUNCTION__,
                      u4FsQoSQTemplateId, i4FsQoSRandomDetectCfgDP);

        return (NULL);
    }

    /* 2. Initialize the New Entry */
    MEMSET (pNewRDCfgNode, 0, sizeof (tQoSRDCfgNode));

    pNewRDCfgNode->u4QTemplateId = u4FsQoSQTemplateId;
    pNewRDCfgNode->u1DP = (UINT1) i4FsQoSRandomDetectCfgDP;
    pNewRDCfgNode->u4MinAvgThresh = QOS_RD_CFG_MIN_AVG_TH_DEFAULT;
    pNewRDCfgNode->u4MaxAvgThresh = QOS_RD_CFG_MAX_AVG_TH_DEFAULT;
    pNewRDCfgNode->u4MaxPktSize = QOS_RD_CFG_MAX_PKT_SIZE_DEFAULT;
    pNewRDCfgNode->u1MaxProb = QOS_RD_CFG_MAX_PROB_DEFAULT;
    pNewRDCfgNode->u1ExpWeight = QOS_RD_CFG_EXP_WEIGHT_DEFAULT;
    pNewRDCfgNode->u4Gain = QOS_RD_CFG_GAIN_DEFAULT;
    pNewRDCfgNode->u1DropThreshType = QOS_RD_CFG_DROP_THRESH_TYPE_DEFAULT;
    pNewRDCfgNode->u4ECNThresh = QOS_RD_CFG_ECN_THRESH_DEFAULT;
    pNewRDCfgNode->u1RDActionFlag = QOS_RD_CFG_ACTION_FLAG_DEFAULT;
    pNewRDCfgNode->u1Status = QOS_STORAGE_NONVOLATILE;
    pNewRDCfgNode->u1Status = NOT_IN_SERVICE;

    /* 3. Add this New Entry into the RD Cfg Table */
    TMO_SLL_Scan (&(gQoSGlobalInfo.SllRDTbl), pRDCfgNode, tQoSRDCfgNode *)
    {
        /* Check for Previous Node */
        if (((pRDCfgNode->u4QTemplateId == u4FsQoSQTemplateId) &&
             (pRDCfgNode->u1DP > (UINT1) i4FsQoSRandomDetectCfgDP)) ||
            (pRDCfgNode->u4QTemplateId > u4FsQoSQTemplateId))
        {
            break;
        }

        pPrevRDCfgNode = pRDCfgNode;

    }                            /* End of Scan */

    /* 3. Add this New Entry into the RD Cfg Table */
    TMO_SLL_Insert (&(gQoSGlobalInfo.SllRDTbl),
                    &(pPrevRDCfgNode->NextNode), &(pNewRDCfgNode->NextNode));

    return (pNewRDCfgNode);
}

/*****************************************************************************/
/* Function Name      : QoSDeleteRDCfgTblEntry                               */
/* Description        : This function is used to Delete an Entry from the    */
/*                      RD Cfg Table ,it will do the following Action        */
/*                      1. Remove the Entry from the RD Cfg Table            */
/*                      2. Clear the Removed Entry                           */
/*                      3. Release the Removed Entry's memory                */
/* Input(s)           : pRDCfgNode   - Pointer to tQoSRDCfgNode              */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : gQoSGlobalInfo                                       */
/* Global Variables                                                          */
/* Modified           : gQoSGlobalInfo                                       */
/* Return Value(s)    : QOS_SUCCESS or QOS_FAILURE                           */
/* Called By          : SNMP Low Level                                       */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QoSDeleteRDCfgTblEntry (tQoSRDCfgNode * pRDCfgNode)
{
    UINT4               u4RetStatus = QOS_FAILURE;

    if (pRDCfgNode == NULL)
    {
        return (QOS_FAILURE);
    }

    /* Delete the Given Node */
    TMO_SLL_Delete (&(gQoSGlobalInfo.SllRDTbl), &(pRDCfgNode->NextNode));

    /* 2. Clear the Removed Entry */
    MEMSET (pRDCfgNode, 0, sizeof (tQoSRDCfgNode));

    /* 3. Release the Removed Entry's memory */
    u4RetStatus = MemReleaseMemBlock (gQoSGlobalInfo.QoSRDTblMemPoolId,
                                      (UINT1 *) pRDCfgNode);
    if (u4RetStatus == MEM_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : MemReleaseMemBlock () Failed.\r\n",
                      __FUNCTION__);

        return (QOS_FAILURE);
    }

    return (QOS_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSUtlValidateRDCfgTblIdxInst                        */
/* Description        : This function is used to Validate the Given Index    */
/*                      instance in the RD Cfg Table, if it is ACTIVE        */
/*                      return Failure Success otherwise.                    */
/* Input(s)           : u4FsQoSQTemplateId - Q Template Id                   */
/*                    : i4FsQoSRandomDetectCfgDP - DP Value for a QTempID    */
/*                      pu4ErrorCode      - Pointer to Error Code            */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : gQoSGlobalInfo                                       */
/* Global Variables                                                          */
/* Modified           : gQoSGlobalInfo                                       */
/* Return Value(s)    : QOS_SUCCESS or QOS_FAILURE                           */
/* Called By          : SNMP Low Level                                       */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QoSUtlValidateRDCfgTblIdxInst (UINT4 u4FsQoSQTemplateId,
                               INT4 i4FsQoSRandomDetectCfgDP,
                               UINT4 *pu4ErrorCode)
{
    tQoSRDCfgNode      *pRDCfgNode = NULL;
    INT4                i4RetStatus = 0;

    if (gQoSGlobalInfo.eSysControl != QOS_SYS_CNTL_START)
    {
        QOS_TRC (MGMT_TRC, QOS_MODULE_SHUTDOW_ERROR_MSG);
        CLI_SET_ERR (QOS_CLI_ERR_MODULE_SHUTDOW);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return (QOS_FAILURE);
    }

    /* Check the Range of Index (QTEMP) Values (Min-Max) */
    i4RetStatus = QOS_CHECK_RD_CFG_TBL_QT_INDEX_RANGE (u4FsQoSQTemplateId);
    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG3 (MGMT_TRC, "%s : RD Cfg Q Template Id %d is out of Range."
                      " The Range Should be (1-%d). \r\n", __FUNCTION__,
                      u4FsQoSQTemplateId, QOS_Q_TEMP_TBL_MAX_INDEX_RANGE);

        CLI_SET_ERR (QOS_CLI_ERR_QTEMP_RANGE);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (QOS_FAILURE);
    }

    /* Check the Range of Index (DP) Values (Min-Max) */
    if ((i4FsQoSRandomDetectCfgDP < QOS_RD_CONFG_DP_MIN_VAL) ||
        (i4FsQoSRandomDetectCfgDP > QOS_RD_CONFG_DP_MAX_VAL))
    {
        QOS_TRC_ARG3 (MGMT_TRC, "%s : RD Cfg DP %d is out of Range."
                      " The Range Should be (1-%d). \r\n", __FUNCTION__,
                      i4FsQoSRandomDetectCfgDP, QOS_RD_CONFG_DP_MAX_VAL);

        CLI_SET_ERR (QOS_CLI_ERR_RDCFG_DP_RANGE);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (QOS_FAILURE);
    }

    pRDCfgNode = QoSUtlGetRDCfgNode (u4FsQoSQTemplateId,
                                     i4FsQoSRandomDetectCfgDP);
    if (pRDCfgNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetQTempNode () Returns "
                      "FAILURE. \r\n", __FUNCTION__);

        CLI_SET_ERR (QOS_CLI_ERR_RDCFG_NO_ENTRY);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (QOS_FAILURE);
    }

    if (pRDCfgNode->u1Status == ACTIVE)
    {
        QOS_TRC (MGMT_TRC, "Can not change the value in ACTIVE State.\r\n");

        CLI_SET_ERR (QOS_CLI_ERR_STATUS_ACTIVE);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return (QOS_FAILURE);
    }

    return (QOS_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSUtlValidateRDCfgActionFlag                        */
/* Description        : This function is used to validate the additional     */
/*                      action Flag combination. The following are validated */
/*                      Action                                               */
/*                      1. Check the Action flag Length, it should be = 1    */
/*                      2. Flag can not be '0', Should be within limit       */
/* Input(s)           : u4PlyMapId            - Policy Map Id                */
/*                      pSNMPOctSrtOutActFlag - Action Flag Mask Value.      */
/*                      pu4ErrorCode          - Pointer to ErrorCode.        */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : gQoSGlobalInfo                                       */
/* Global Variables                                                          */
/* Modified           : none                                                 */
/* Return Value(s)    : QOS_SUCCESS or QOS_FAILURE                           */
/* Called By          : SNMP Low Level                                       */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QoSUtlValidateRDCfgActionFlag (UINT4 u4FsQoSQTemplateId,
                               INT4 i4FsQoSRandomDetectCfgDP,
                               tSNMP_OCTET_STRING_TYPE *
                               pSNMPOctSrtOutActFlag, UINT4 *pu4ErrorCode)
{

    INT4                i4RetStatus = QOS_FAILURE;
    UINT1               u1RDCfgActFlag = 0;
    /* Check Index */
    i4RetStatus = QoSUtlValidateRDCfgTblIdxInst (u4FsQoSQTemplateId,
                                                 i4FsQoSRandomDetectCfgDP,
                                                 pu4ErrorCode);
    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlValidateRDCfgTblIdxInst () "
                      "Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    if (pSNMPOctSrtOutActFlag->i4_Length != QOS_RD_CONFIG_ACTION_FLAG_LENGTH)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;

        QOS_TRC_ARG2 (MGMT_TRC, "%s : Length of the Action Flag is %d. "
                      "This is Incorrect Value. The Value is = 1. \r\n",
                      __FUNCTION__, pSNMPOctSrtOutActFlag->i4_Length);
        CLI_SET_ERR (QOS_CLI_ERR_RDCFG_ACT_FLG_LEN_INVALID);

        return (QOS_FAILURE);
    }

    MEMCPY (&(u1RDCfgActFlag), pSNMPOctSrtOutActFlag->pu1_OctetList,
            pSNMPOctSrtOutActFlag->i4_Length);

    /* Check Range */
    if (u1RDCfgActFlag > QOS_RD_CONFIG_ACTION_MAX_VAL)
    {
        QOS_TRC_ARG4 (MGMT_TRC, "%s : RD Config action flag value %d, is"
                      " out of range. The range is Min %d to Max %d. \r\n",
                      __FUNCTION__, u1RDCfgActFlag,
                      QOS_RD_CONFIG_ACTION_MIN_VAL,
                      QOS_RD_CONFIG_ACTION_MAX_VAL);

        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;

        return (QOS_FAILURE);
    }

    return (QOS_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSUtlValidateQTemplateSize                          */
/* Description        : This function is used to validate the Q Template Size*/
/*                      The following are the vaidations made                */
/*                      1. Q Template size must be must be greater than or   */
/*                         equal to the Minimum threshold of all the         */
/*                         drop profiles of the QTemplateId                  */
/*                      2. Q Template size must be must be lesser than or    */
/*                         equal to the Maximum threshold of all the         */
/*                         drop profiles of the QTemplateId                  */
/* Input(s)           : u4FsQoSQTemplateId          - Q Template Id          */
/*                      u4TestValFsQoSQTemplateSize - Q Tempalte Size        */
/*                      pu4ErrorCode                - Pointer to ErrorCode.  */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Modified           : none                                                 */
/* Return Value(s)    : QOS_SUCCESS or QOS_FAILURE                           */
/* Called By          : SNMP Low Level                                       */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QoSUtlValidateQTemplateSize (UINT4 u4FsQoSQTemplateId,
                             UINT4 u4TestValFsQoSQTemplateSize,
                             UINT4 *pu4ErrorCode)
{
    INT4                i4RowStatus = QOS_FAILURE;
    INT4                i4DP = QOS_RD_CONFG_DP_MIN_VAL;
    INT4                i4RetStatus = QOS_FAILURE;
    UINT4               u4Idx = u4FsQoSQTemplateId;
    UINT4               u4MinTH = 0;
    UINT4               u4MaxTH = 0;

    i4RetStatus = QoSUtlValidateQTempTblIdxInst (u4FsQoSQTemplateId,
                                                 pu4ErrorCode);
    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlValidateQTempTblIdxInst () "
                      "Returns FAILURE. \r\n", __FUNCTION__);
        return (QOS_FAILURE);
    }

    do
    {
        if (nmhGetFsQoSRandomDetectCfgStatus (u4Idx, i4DP, &i4RowStatus) !=
            SNMP_FAILURE)
        {
            /* Check the Min and Max Threshold values only for the given
             * u4FsQoSQTemplateId */
            if ((i4RowStatus == ACTIVE) && (u4Idx == u4FsQoSQTemplateId))
            {
                if (nmhGetFsQoSRandomDetectCfgMinAvgThresh (u4Idx, i4DP,
                                                            &u4MinTH) !=
                    SNMP_FAILURE)
                {
                    if (u4TestValFsQoSQTemplateSize < u4MinTH)
                    {
                        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                        QOS_TRC_ARG1 (MGMT_TRC,
                                      "%s : Q Template Size must greater than "
                                      "or equal to Minimum Threshold value \r\n",
                                      __FUNCTION__);
                        return (QOS_FAILURE);
                    }
                }

                if (nmhGetFsQoSRandomDetectCfgMaxAvgThresh (u4Idx, i4DP,
                                                            &u4MaxTH) !=
                    SNMP_FAILURE)
                {
                    if (u4TestValFsQoSQTemplateSize < u4MaxTH)
                    {
                        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                        QOS_TRC_ARG1 (MGMT_TRC,
                                      "%s : Q Template Size must greater than "
                                      "or equal to Maximum Threshold value \r\n",
                                      __FUNCTION__);
                        return (QOS_FAILURE);
                    }

                }
            }
        }
    }
    while (nmhGetNextIndexFsQoSRandomDetectCfgTable
           (u4Idx, &u4Idx, i4DP, &i4DP) == SNMP_SUCCESS);

    return (QOS_SUCCESS);

}

/*****************************************************************************/
/*                  SHAPE TEMP TABLE FUNCTIONS                               */
/*****************************************************************************/

/*****************************************************************************/
/* Function Name      : QoSUtlGetShapeTempNode                               */
/* Description        : This function is used to Get the Entry  form  the    */
/*                      ShapeTemplate Table ,it will do the following Action */
/*                      1. It will Scan the SLL.                             */
/* Input(s)           : u4ShapeTempId      - Index to ShapeTemplate Node     */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : gQoSGlobalInfo                                       */
/* Global Variables                                                          */
/* Modified           :                                                      */
/* Return Value(s)    : NULL/ Pointer to 'tQoSShapeTemplateNode'             */
/* Called By          : SNMP Low Level                                       */
/* Calling Function   :                                                      */
/*****************************************************************************/
tQoSShapeTemplateNode *
QoSUtlGetShapeTempNode (UINT4 u4ShapeTempId)
{
    tQoSShapeTemplateNode *pShapeTempNode = NULL;

    TMO_SLL_Scan (&(gQoSGlobalInfo.SllShapeTempTbl), pShapeTempNode,
                  tQoSShapeTemplateNode *)
    {
        if (pShapeTempNode->u4Id == u4ShapeTempId)
        {
            return (pShapeTempNode);
        }
    }

    QOS_TRC_ARG2 (MGMT_TRC, "%s : Q Shape Template Id %d is not Found in the "
                  "Shape Template Table. \r\n", __FUNCTION__, u4ShapeTempId);

    return (NULL);

}

/*****************************************************************************/
/* Function Name      : QoSGetNextShapeTempTblEntryIndex                     */
/* Description        : This function is used to Get the Next Entry from the */
/*                      Shape Temp Table ,it will do the following Action    */
/*                      1. If u4CurrentIndex is 0 then  GetFirxtIndex.       */
/*                      2. If u4CurrentIndex is not 0 then  GetNextIndex.    */
/* Input(s)           : u4CurrentIndex  - Current index id                   */
/*                      pu4NextIndex    - Pointer to NextIndex Id            */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : gQoSGlobalInfo                                       */
/* Global Variables                                                          */
/* Modified           : gQoSGlobalInfo                                       */
/* Return Value(s)    : QOS_SUCCESS or QOS_FAILURE                           */
/* Called By          : SNMP Low Level                                       */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QoSGetNextShapeTempTblEntryIndex (UINT4 u4CurrentIndex, UINT4 *pu4NextIndex)
{
    tQoSShapeTemplateNode *pShapeTempNode = NULL;
    UINT4               u4NodeFound = QOS_FAILURE;

    if (u4CurrentIndex == 0)
    {
        pShapeTempNode = (tQoSShapeTemplateNode *)
            TMO_SLL_First (&(gQoSGlobalInfo.SllShapeTempTbl));
    }
    else
    {
        TMO_SLL_Scan (&(gQoSGlobalInfo.SllShapeTempTbl), pShapeTempNode,
                      tQoSShapeTemplateNode *)
        {
            /* If the u4CurrentIndex is not found give the Next greater */
            if ((u4NodeFound == QOS_SUCCESS) ||
                (pShapeTempNode->u4Id > u4CurrentIndex))
            {
                *pu4NextIndex = pShapeTempNode->u4Id;
                return (QOS_SUCCESS);
            }

            if (pShapeTempNode->u4Id == u4CurrentIndex)
            {
                u4NodeFound = QOS_SUCCESS;
            }

        }                        /* End of Scan */

    }                            /* End of Else */

    if (pShapeTempNode == NULL)
    {
        QOS_TRC_ARG2 (MGMT_TRC, "In %s : GetNext () Failed, "
                      " for ShapeTempId %d.\r\n", __FUNCTION__, u4CurrentIndex);
        return (QOS_FAILURE);
    }

    *pu4NextIndex = pShapeTempNode->u4Id;

    return (QOS_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSCreateShapeTempTblEntry                           */
/* Description        : This function is used to Create an Entry in the      */
/*                      ShapeTemp Table ,it will do the following Action     */
/*                      1. Allocate a memory block for the New Entry         */
/*                      2. Initialize the New Entry                          */
/*                      3. Add this New Entry into the ShapeTemp Table       */
/* Input(s)           : u4ShapeTemplateId  - Index to ShapeTemp  Table Entry */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : gQoSGlobalInfo                                       */
/* Global Variables                                                          */
/* Modified           : gQoSGlobalInfo                                       */
/* Return Value(s)    : NULL / Pointer to  tQoSShapeTemplateNode             */
/* Called By          : SNMP Low Level                                       */
/* Calling Function   :                                                      */
/*****************************************************************************/
tQoSShapeTemplateNode *
QoSCreateShapeTempTblEntry (UINT4 u4ShapeTemplateId)
{
    tQoSShapeTemplateNode *pShapeTempNode = NULL;
    tQoSShapeTemplateNode *pNewShapeTempNode = NULL;
    tQoSShapeTemplateNode *pPrevShapeTempNode = NULL;

    /* 1. Allocate a memory block for the New Entry */
    pNewShapeTempNode = (tQoSShapeTemplateNode *)
        MemAllocMemBlk (gQoSGlobalInfo.QoSShapeTempTblMemPoolId);
    if (pNewShapeTempNode == NULL)
    {
        QOS_TRC_ARG2 (MGMT_TRC, "In %s : MemAllocMemBlk () Failed, for "
                      "ShapeTempId %d. \r\n", __FUNCTION__, u4ShapeTemplateId);

        return (NULL);
    }

    /* 2. Initialize the New Entry */
    MEMSET (pNewShapeTempNode, 0, sizeof (tQoSShapeTemplateNode));

    SPRINTF ((CHR1 *) & (pNewShapeTempNode->au1Name[0]), "ST-%u",
             u4ShapeTemplateId);

    pNewShapeTempNode->u4Id = u4ShapeTemplateId;
    pNewShapeTempNode->u4CIR = QOS_SHAPE_TEMP_CIR_DEFAULT;
    pNewShapeTempNode->u4CBS = QOS_SHAPE_TEMP_CBS_DEFAULT;
    pNewShapeTempNode->u4EIR = QOS_SHAPE_TEMP_EIR_DEFAULT;
    pNewShapeTempNode->u4EBS = QOS_SHAPE_TEMP_EBS_DEFAULT;
    pNewShapeTempNode->u1Status = NOT_IN_SERVICE;

    /* 3. Add this New Entry into the Shape Temp Table */
    TMO_SLL_Scan (&(gQoSGlobalInfo.SllShapeTempTbl), pShapeTempNode,
                  tQoSShapeTemplateNode *)
    {
        /* Check for Previous Node */
        if (pShapeTempNode->u4Id > u4ShapeTemplateId)
        {
            break;
        }

        pPrevShapeTempNode = pShapeTempNode;

    }                            /* End of Scan */

    /* 3. Add this New Entry into the Shape Temp Table */
    TMO_SLL_Insert (&(gQoSGlobalInfo.SllShapeTempTbl),
                    &(pPrevShapeTempNode->NextNode),
                    &(pNewShapeTempNode->NextNode));

    return (pNewShapeTempNode);
}

/*****************************************************************************/
/* Function Name      : QoSDeleteShapeTempTblEntry                           */
/* Description        : This function is used to Delete an Entry from the    */
/*                      Shape Temp Table ,it will do the following Action    */
/*                      1. Remove the Entry from the Shap Temp Table         */
/*                      2. Clear the Removed Entry                           */
/*                      3. Release the Removed Entry's memory                */
/* Input(s)           : pShapeTempNode   - Pointer to tQoSShapeTemplateNode  */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : gQoSGlobalInfo                                       */
/* Global Variables                                                          */
/* Modified           : gQoSGlobalInfo                                       */
/* Return Value(s)    : QOS_SUCCESS or QOS_FAILURE                           */
/* Called By          : SNMP Low Level                                       */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QoSDeleteShapeTempTblEntry (tQoSShapeTemplateNode * pShapeTempNode)
{
    UINT4               u4RetStatus = QOS_FAILURE;

    if (pShapeTempNode == NULL)
    {
        return (QOS_FAILURE);
    }

    /* Delete the Given Node */
    TMO_SLL_Delete (&(gQoSGlobalInfo.SllShapeTempTbl),
                    &(pShapeTempNode->NextNode));

    /* 2. Clear the Removed Entry */
    MEMSET (pShapeTempNode, 0, sizeof (tQoSShapeTemplateNode));

    /* 3. Release the Removed Entry's memory */
    u4RetStatus = MemReleaseMemBlock (gQoSGlobalInfo.QoSShapeTempTblMemPoolId,
                                      (UINT1 *) pShapeTempNode);
    if (u4RetStatus == MEM_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : MemReleaseMemBlock () Failed.\r\n",
                      __FUNCTION__);

        return (QOS_FAILURE);
    }

    return (QOS_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSUtlValidateShapeTempTblIdxInst                    */
/* Description        : This function is used to Validate the Given Index    */
/*                      instance in the Shape Temp Table, if it is ACTIVE    */
/*                      return Failure Success otherwise.                    */
/* Input(s)           : u4FsQoSShapeTemplateId -Shape Temp Id                */
/*                      pu4ErrorCode      - Pointer to Error Code            */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : gQoSGlobalInfo                                       */
/* Global Variables                                                          */
/* Modified           : gQoSGlobalInfo                                       */
/* Return Value(s)    : QOS_SUCCESS or QOS_FAILURE                           */
/* Called By          : SNMP Low Level                                       */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QoSUtlValidateShapeTempTblIdxInst (UINT4 u4FsQoSShapeTemplateId,
                                   UINT4 *pu4ErrorCode)
{
    tQoSShapeTemplateNode *pShapeTempNode = NULL;
    INT4                i4RetStatus = 0;

    if (gQoSGlobalInfo.eSysControl != QOS_SYS_CNTL_START)
    {
        QOS_TRC (MGMT_TRC, QOS_MODULE_SHUTDOW_ERROR_MSG);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return (QOS_FAILURE);
    }

    /* Check the Range of Index Values (Min-Max) */
    i4RetStatus = QOS_CHECK_SHAPE_TEMP_TBL_INDEX_RANGE (u4FsQoSShapeTemplateId);
    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG3 (MGMT_TRC, "%s : Shape Template Id %d is out of Range."
                      " The Range Should be (1-%d). \r\n", __FUNCTION__,
                      u4FsQoSShapeTemplateId,
                      QOS_SHAPE_TEMP_TBL_MAX_INDEX_RANGE);

        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;

        return (QOS_FAILURE);
    }

    pShapeTempNode = QoSUtlGetShapeTempNode (u4FsQoSShapeTemplateId);
    if (pShapeTempNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetShapeTempNode () "
                      "Returns FAILURE. \r\n", __FUNCTION__);

        *pu4ErrorCode = SNMP_ERR_NO_CREATION;

        return (QOS_FAILURE);
    }

    if (pShapeTempNode->u1Status == ACTIVE)
    {
        QOS_TRC (MGMT_TRC, "Can not change the value in ACTIVE State.\r\n");

        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;

        return (QOS_FAILURE);
    }

    return (QOS_SUCCESS);
}

/*****************************************************************************/
/*                     Q MAP TABLE FUNCTIONS                                 */
/*****************************************************************************/

/*****************************************************************************/
/* Function Name      : QoSQMapCmpFun                                        */
/* Description        : This function is used as a Compare function in the   */
/*                      RBTree.                                              */
/*                      1.It Compares the tQoSQMapNode and return the values */
/* Input(s)           : e1 , e2 -  Elements of RBTree                        */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    :  -1/0/1                                              */
/* Called By          : RBTreeUtility                                        */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QoSQMapCmpFun (tRBElem * e1, tRBElem * e2)
{
    tQoSQMapNode       *pQMapNode1 = NULL;
    tQoSQMapNode       *pQMapNode2 = NULL;

    pQMapNode1 = (tQoSQMapNode *) e1;
    pQMapNode2 = (tQoSQMapNode *) e2;

    /* Compare the QMap IfIndex */
    if (pQMapNode1->i4IfIndex < pQMapNode2->i4IfIndex)
    {
        return (-1);
    }
    else if (pQMapNode1->i4IfIndex > pQMapNode2->i4IfIndex)
    {
        return (1);
    }
    else
    {
        /* Compare the QMap CLASS */
        if (pQMapNode1->u4CLASS < pQMapNode2->u4CLASS)
        {
            return (-1);
        }
        else if (pQMapNode1->u4CLASS > pQMapNode2->u4CLASS)
        {
            return (1);
        }
        else
        {
            /* Compare the QMap RegenPriType */
            if (pQMapNode1->u1RegenPriType < pQMapNode2->u1RegenPriType)
            {
                return (-1);
            }
            else if (pQMapNode1->u1RegenPriType > pQMapNode2->u1RegenPriType)
            {
                return (1);
            }
            else
            {
                /* Compare the QMap RegenPri Value */
                if (pQMapNode1->u1RegenPri < pQMapNode2->u1RegenPri)
                {
                    return (-1);
                }
                else if (pQMapNode1->u1RegenPri > pQMapNode2->u1RegenPri)
                {
                    return (1);
                }
                else
                {
                    return (0);
                }
            }
        }
    }
}

/*****************************************************************************/
/* Function Name      : QoSQMapTblRBTFreeNodeFn                              */
/* Description        : This function is used to Delete the Q Map  Table     */
/*                      Entries and Release the Memory to the respective     */
/*                      Mem Pool .                                           */
/* Input(s)           : tRBElem     - Pointer to Q Map  Table Entry          */
/*                      u4Arg       - Unused Param                           */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : QOS_SUCCESS /QOS_FAILURE                             */
/* Called By          : SNMP                                                 */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QoSQMapTblRBTFreeNodeFn (tRBElem * pRBElem, UINT4 u4Arg)
{
    UINT4               u4RetStatus = MEM_FAILURE;

    UNUSED_PARAM (u4Arg);

    if (pRBElem == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QMap Tbl Entry is NULL.\r\n",
                      __FUNCTION__);

        return (QOS_FAILURE);
    }

    MEMSET (pRBElem, 0, sizeof (tQoSQMapNode));

    /* Release the Mem Block */
    u4RetStatus = MemReleaseMemBlock (gQoSGlobalInfo.QoSQMapTblMemPoolId,
                                      (UINT1 *) pRBElem);

    if (u4RetStatus != MEM_SUCCESS)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QMapTbl- "
                      "MemReleaseMemBlock () Failed.\r\n", __FUNCTION__);
    }

    return (QOS_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSUtlGetQMapNode                                    */
/* Description        : This function is used to Get the Entry  form  the    */
/*                      QMap Table ,it will do the following Action          */
/*                      1. Call RBTreeGet Utility.                           */
/* Input(s)           : u4QMapId    - Index to tQoSQMapNode                  */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : gQoSGlobalInfo                                       */
/* Global Variables                                                          */
/* Modified           :                                                      */
/* Return Value(s)    : NULL/ Pointer to 'tQoSQMapNode'                      */
/* Called By          : SNMP Low Level                                       */
/* Calling Function   :                                                      */
/*****************************************************************************/
tQoSQMapNode       *
QoSUtlGetQMapNode (INT4 i4IfIndex, UINT4 u4CLASS, INT4 i4RegenPriType,
                   UINT4 u4RegenPri)
{
    tQoSQMapNode        TempQMapNode;
    tQoSQMapNode       *pQMapNode = NULL;

    MEMSET (&TempQMapNode, 0, sizeof (tQoSQMapNode));

    TempQMapNode.i4IfIndex = i4IfIndex;
    TempQMapNode.u4CLASS = u4CLASS;
    TempQMapNode.u1RegenPri = (UINT1) u4RegenPri;
    TempQMapNode.u1RegenPriType = (UINT1) i4RegenPriType;

    pQMapNode = (tQoSQMapNode *) RBTreeGet (gQoSGlobalInfo.pRbQMapTbl,
                                            (tRBElem *) & TempQMapNode);
    if (pQMapNode == NULL)
    {
        QOS_TRC_ARG5 (MGMT_TRC, "%s : QMap Index %d.%d.%d.%d is not Found "
                      "in the QMap Table.\r\n", __FUNCTION__, i4IfIndex,
                      u4CLASS, i4RegenPriType, u4RegenPri);
    }

    return (pQMapNode);
}

/*****************************************************************************/
/* Function Name      : QoSUtlGetQMapEntry                                   */
/* Description        : This function is used to Get the Entry  form  the    */
/*                      QMap Table ,it will do the following Action          */
/*                      1. Call RBTreeGetFirst Utility.                      */
/*                      2. Call RBTreeGetNext Utility                        */
/* Input(s)           : u4FsQoSQMapCLASS    - CLASSId                        */
/* Output(s)          : pQMapNode           - QMap Node                      */
/* Global Variables                                                          */
/* Referred           : gQoSGlobalInfo                                       */
/* Global Variables                                                          */
/* Modified           :                                                      */
/* Return Value(s)    : NULL/ Pointer to 'tQoSQMapNode'                      */
/* Called By          : SNMP Low Level                                       */
/* Calling Function   :                                                      */
/*****************************************************************************/
tQoSQMapNode       *
QoSUtlGetQMapEntry (UINT4 u4FsQoSQMapCLASS)
{
    tQoSQMapNode       *pQMapNode = NULL;

    pQMapNode = (tQoSQMapNode *) RBTreeGetFirst (gQoSGlobalInfo.pRbQMapTbl);

    while (pQMapNode != NULL)
    {
        if (pQMapNode->u4CLASS == u4FsQoSQMapCLASS)
        {
            return pQMapNode;
        }

        pQMapNode = (tQoSQMapNode *) RBTreeGetNext (gQoSGlobalInfo.pRbQMapTbl,
                                                    pQMapNode, QoSQMapCmpFun);
    }

    return pQMapNode;

}

/*****************************************************************************/
/* Function Name      : QoSCreateQMapTblEntry                                */
/* Description        : This function is used to Create an Entry in   the    */
/*                      QMap Table ,it will do the following Action          */
/*                      1. Allocate a memory block for the New Entry         */
/*                      2. Initialize the New Entry                          */
/*                      3. Add this New Entry into the QMap Table            */
/* Input(s)           : u4QMapId      - Index to QMapNode                    */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : gQoSGlobalInfo                                       */
/* Global Variables                                                          */
/* Modified           : gQoSGlobalInfo                                       */
/* Return Value(s)    : NULL / Pointer to  QMapNode                          */
/* Called By          : SNMP Low Level                                       */
/* Calling Function   :                                                      */
/*****************************************************************************/
tQoSQMapNode       *
QoSCreateQMapTblEntry (INT4 i4IfIndex, UINT4 u4CLASS, INT4 i4RegenPriType,
                       UINT4 u4RegenPri)
{
    UINT4               u4RetStatus = QOS_FAILURE;
    tQoSQMapNode       *pNewQMapNode = NULL;

    /* 1. Allocate a memory block for the New Entry */
    pNewQMapNode = (tQoSQMapNode *)
        MemAllocMemBlk (gQoSGlobalInfo.QoSQMapTblMemPoolId);
    if (pNewQMapNode == NULL)
    {
        QOS_TRC_ARG5 (MGMT_TRC, "In %s : MemAllocMemBlk () Failed ,"
                      "for QMapIndex %d.%d.%d.%d \r\n", __FUNCTION__,
                      i4IfIndex, u4CLASS, i4RegenPriType, u4RegenPri);
        return (NULL);
    }

    /* 2. Initialize the New Entry */
    MEMSET (pNewQMapNode, 0, sizeof (tQoSQMapNode));

    pNewQMapNode->i4IfIndex = i4IfIndex;
    pNewQMapNode->u4CLASS = u4CLASS;
    pNewQMapNode->u1RegenPriType = (UINT1) i4RegenPriType;
    pNewQMapNode->u1RegenPri = (UINT1) u4RegenPri;
    pNewQMapNode->u1Status = NOT_READY;

    /* 3. Add this New Entry into the QMap Table */
    u4RetStatus = RBTreeAdd (gQoSGlobalInfo.pRbQMapTbl,
                             (tRBElem *) pNewQMapNode);
    if (u4RetStatus != RB_SUCCESS)
    {
        u4RetStatus =
            MemReleaseMemBlock (gQoSGlobalInfo.QoSQMapTblMemPoolId,
                                (UINT1 *) pNewQMapNode);

        if (u4RetStatus == MEM_FAILURE)
        {
            QOS_TRC_ARG5 (MGMT_TRC, "In %s : MemReleaseMemBlock () Failed ,"
                          "for QMapIndex %d.%d.%d.%d \r\n", __FUNCTION__,
                          i4IfIndex, u4CLASS, i4RegenPriType, u4RegenPri);

            return (NULL);
        }

        QOS_TRC_ARG5 (MGMT_TRC, "In %s : RBTreeAdd () Failed ,"
                      "for QMapIndex %d.%d.%d.%d \r\n", __FUNCTION__,
                      i4IfIndex, u4CLASS, i4RegenPriType, u4RegenPri);
        return (NULL);
    }

    return (pNewQMapNode);
}

/*****************************************************************************/
/* Function Name      : QoSGetNextQMapTblEntryIndex                          */
/* Description        : This function is used to Get the Next Entry from the */
/*                      QMap Table ,it will do the following Action          */
/*                      1. If u4CurrentIndex is 0 then  GetFirxtIndex.       */
/*                      2. If u4CurrentIndex is not 0 then  GetNextIndex.    */
/* Input(s)           : u4CurrentIndex  - Current index id                   */
/*                      pu4NextIndex    - Pointer to NextIndex Id            */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : gQoSGlobalInfo                                       */
/* Global Variables                                                          */
/* Modified           : gQoSGlobalInfo                                       */
/* Return Value(s)    : QOS_SUCCESS or QOS_FAILURE                           */
/* Called By          : SNMP Low Level                                       */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QoSGetNextQMapTblEntryIndex (INT4 i4IfIndex, INT4 *pi4NextIfIndex,
                             UINT4 u4CLASS, UINT4 *pu4NextCLASS,
                             INT4 i4RegenPriType, INT4 *pi4NextRegenPriType,
                             UINT4 u4RegenPri, UINT4 *pu4NextRegenPri)
{
    tQoSQMapNode       *pQMapNode = NULL;
    tQoSQMapNode        QMapNode;

    QMapNode.i4IfIndex = i4IfIndex;
    QMapNode.u4CLASS = u4CLASS;
    QMapNode.u1RegenPriType = (UINT1) i4RegenPriType;
    QMapNode.u1RegenPri = (UINT1) u4RegenPri;

    pQMapNode = (tQoSQMapNode *) RBTreeGetNext (gQoSGlobalInfo.pRbQMapTbl,
                                                &QMapNode, QoSQMapCmpFun);
    if (pQMapNode == NULL)
    {
        QOS_TRC_ARG5 (MGMT_TRC, "In %s : RBTreeGetNext () Failed, for "
                      "for QMapIndex %d.%d.%d.%d \r\n", __FUNCTION__,
                      i4IfIndex, u4CLASS, i4RegenPriType, u4RegenPri);
        return (QOS_FAILURE);
    }

    *pi4NextIfIndex = pQMapNode->i4IfIndex;
    *pu4NextCLASS = pQMapNode->u4CLASS;
    *pi4NextRegenPriType = pQMapNode->u1RegenPriType;
    *pu4NextRegenPri = pQMapNode->u1RegenPri;
    return (QOS_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSUtlIsClassMaptoQMapEntry                          */
/* Description        : This function is used to verify the given Class is   */
/*                      mapped to any QMap entry.                            */
/*                                                                           */
/* Input(s)           : u4CLASS - CLASS to verify in QMAP table.             */
/*                                                                           */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : gQoSGlobalInfo                                       */
/* Global Variables                                                          */
/* Modified           : gQoSGlobalInfo                                       */
/* Return Value(s)    : TRUE  - If CLASS entry exist in QMAP Table.          */
/*                      FALSE - If CLASS entry does not exist in QMAP Table. */
/* Called By          : SNMP Low Level                                       */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QoSUtlIsClassMaptoQMapEntry (UINT4 u4CLASS)
{
    tQoSQMapNode       *pQMapNode = NULL;

    pQMapNode = (tQoSQMapNode *) RBTreeGetFirst (gQoSGlobalInfo.pRbQMapTbl);

    while (pQMapNode != NULL)
    {
        if (pQMapNode->u4CLASS == u4CLASS)
        {
            return TRUE;
        }

        pQMapNode = (tQoSQMapNode *) RBTreeGetNext (gQoSGlobalInfo.pRbQMapTbl,
                                                    pQMapNode, QoSQMapCmpFun);
    }

    return FALSE;
}

/*****************************************************************************/
/* Function Name      : QoSDeleteQMapTblEntry                                */
/* Description        : This function is used to Delete an Entry from the    */
/*                      QMap Table ,it will do the following Action          */
/*                      1. Remove the Entry from the QMap Table              */
/*                      2. Clear the Removed Entry                           */
/*                      3. Release the Removed Entry's memory                */
/* Input(s)           : pQMapNode   - Pointer to QMapNode                    */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : gQoSGlobalInfo                                       */
/* Global Variables                                                          */
/* Modified           : gQoSGlobalInfo                                       */
/* Return Value(s)    : QOS_SUCCESS or QOS_FAILURE                           */
/* Called By          : SNMP Low Level                                       */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QoSDeleteQMapTblEntry (tQoSQMapNode * pQMapNode)
{
    tQoSQMapNode       *pDelQMapNode = NULL;
    UINT4               u4RetStatus = QOS_FAILURE;

    if (pQMapNode == NULL)
    {
        return (QOS_FAILURE);
    }

    /* 1. Remove Entry from the QMap Table */
    pDelQMapNode = RBTreeRem (gQoSGlobalInfo.pRbQMapTbl, (tRBElem *) pQMapNode);
    if (pDelQMapNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : RBTreeRem () Failed.\r\n",
                      __FUNCTION__);
        return (QOS_FAILURE);
    }

    /* 2. Clear the Removed Entry */
    MEMSET (pDelQMapNode, 0, sizeof (tQoSQMapNode));

    /* 3. Release the Removed Entry's memory */
    u4RetStatus = MemReleaseMemBlock (gQoSGlobalInfo.QoSQMapTblMemPoolId,
                                      (UINT1 *) pDelQMapNode);
    if (u4RetStatus == MEM_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : MemReleaseMemBlock () Failed. \r\n",
                      __FUNCTION__);

        return (QOS_FAILURE);
    }

    return (QOS_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSUtlValidateQMapTblIdxInst                         */
/* Description        : This function is used to Validate the Given Index    */
/*                      instance in the QMap Table, if it is ACTIVE          */
/*                      return Failure Success otherwise.                    */
/* Input(s)           : i4IfIndex         - Interface Index                  */
/*                    : u4CLASS           - CLASS Value                      */
/*                    : u4RegenPri        - RegnPriority Value               */
/*                      pu4ErrorCode     - Pointer to Error Code             */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : gQoSGlobalInfo                                       */
/* Global Variables                                                          */
/* Modified           : gQoSGlobalInfo                                       */
/* Return Value(s)    : QOS_SUCCESS or QOS_FAILURE                           */
/* Called By          : SNMP Low Level                                       */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QoSUtlValidateQMapTblIdxInst (INT4 i4IfIndex, UINT4 u4CLASS,
                              INT4 i4RegenPriType, UINT4 u4RegenPri,
                              UINT4 *pu4ErrorCode)
{
    INT4                i4RetStatus = 0;
    tQoSQMapNode       *pQMapNode = NULL;

    if (gQoSGlobalInfo.eSysControl != QOS_SYS_CNTL_START)
    {
        QOS_TRC (MGMT_TRC, QOS_MODULE_SHUTDOW_ERROR_MSG);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (QOS_CLI_ERR_MODULE_SHUTDOW);

        return (QOS_FAILURE);
    }

    if (i4IfIndex != 0)
    {
        i4RetStatus = QoSUtlValidateIfIndex (i4IfIndex, pu4ErrorCode);
        if (i4RetStatus == QOS_FAILURE)
        {
            return (QOS_FAILURE);
        }
    }

    /* Check the Range of Index Values (Min-Max) */
    if (u4CLASS > QOS_CLS2PRI_MAP_TBL_MAX_INDEX_RANGE)
    {
        QOS_TRC_ARG3 (MGMT_TRC, "%s : CLASS %d is out of Range."
                      " The Range Should be (0-%d). \r\n", __FUNCTION__,
                      u4CLASS, QOS_MAX_NUM_OF_CLASSES);

        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (QOS_CLI_ERR_CLS_MAP_RANGE);
        return (QOS_FAILURE);
    }

    if (i4RegenPriType != QOS_QMAP_PRI_TYPE_NONE)
    {
        if (u4CLASS != 0)
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return (QOS_FAILURE);
        }
    }

    switch (i4RegenPriType)
    {
        case QOS_QMAP_PRI_TYPE_NONE:
            /* CLASS specific Q */
            if (u4CLASS == 0)
            {
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                return (QOS_FAILURE);
            }

            break;
        case QOS_QMAP_PRI_TYPE_INT_PRI:
        case QOS_QMAP_PRI_TYPE_VLAN_PRI:
        case QOS_QMAP_PRI_TYPE_DOT1P:
            if (u4RegenPri > QOS_QMAP_PRI_VLAN_PRI_MAX)
            {
                CLI_SET_ERR (QOS_CLI_ERR_QMAP_PRI_RANGE);
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                return (QOS_FAILURE);
            }
            break;

        case QOS_QMAP_PRI_TYPE_IP_TOS:
            /* IP TOS-PRI BIT(3) */
            if (u4RegenPri > QOS_QMAP_PRI_IP_TOS_MAX)
            {
                CLI_SET_ERR (QOS_CLI_ERR_QMAP_PRI_RANGE);
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                return (QOS_FAILURE);
            }
            break;

        case QOS_QMAP_PRI_TYPE_IP_DSCP:

            /* IP DSCP BIT(6) */
            if (u4RegenPri > QOS_QMAP_PRI_IP_DSCP_MAX)
            {
                CLI_SET_ERR (QOS_CLI_ERR_QMAP_PRI_RANGE);
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                return (QOS_FAILURE);
            }
            break;

        case QOS_QMAP_PRI_TYPE_MPLS_EXP:
            /* MPLS EXP  BIT(3) */
            if (u4RegenPri > QOS_QMAP_PRI_MPLS_EXP_MAX)
            {
                CLI_SET_ERR (QOS_CLI_ERR_QMAP_PRI_RANGE);
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                return (QOS_FAILURE);
            }
            break;

        case QOS_QMAP_PRI_TYPE_VLAN_DEI:
            /* VLAN DEI BIT(1) */
            if ((u4RegenPri != QOS_QMAP_PRI_VLAN_DEI_MIN) &&
                (u4RegenPri != QOS_QMAP_PRI_VLAN_DEI_MAX))
            {
                CLI_SET_ERR (QOS_CLI_ERR_QMAP_PRI_RANGE);
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                return (QOS_FAILURE);
            }
            break;

        default:
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            CLI_SET_ERR (QOS_CLI_ERR_INVALID_PRI_TYPE);
            return (QOS_FAILURE);
    }

    pQMapNode = QoSUtlGetQMapNode (i4IfIndex, u4CLASS, i4RegenPriType,
                                   u4RegenPri);
    if (pQMapNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetQMapNode () Returns "
                      "FAILURE. \r\n", __FUNCTION__);

        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (QOS_CLI_ERR_QMAP_NO_ENTRY);
        return (QOS_FAILURE);
    }

    if (pQMapNode->u1Status == ACTIVE)
    {
        QOS_TRC (MGMT_TRC, "Can not change the value in ACTIVE State.\r\n");

        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (QOS_CLI_ERR_STATUS_ACTIVE);
        return (QOS_FAILURE);
    }

    return (QOS_SUCCESS);
}

/*****************************************************************************/
/*                     HIERARCHY TABLE FUNCTIONS                             */
/*****************************************************************************/

/*****************************************************************************/
/* Function Name      : QoSHierarchyCmpFun                                   */
/* Description        : This function is used as a Compare function in the   */
/*                      RBTree.                                              */
/*                      1.It Compares the tQoSHierarchyNode and return the   */
/*                      values                                               */
/* Input(s)           : e1 , e2 -  Elements of RBTree                        */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    :  -1/0/1                                              */
/* Called By          : SNMP                                                */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QoSHierarchyCmpFun (tRBElem * e1, tRBElem * e2)
{
    tQoSHierarchyNode  *pHierarchyNode1 = NULL;
    tQoSHierarchyNode  *pHierarchyNode2 = NULL;

    pHierarchyNode1 = (tQoSHierarchyNode *) e1;
    pHierarchyNode2 = (tQoSHierarchyNode *) e2;

    /* Check the IfIndex */
    if (pHierarchyNode1->i4IfIndex < pHierarchyNode2->i4IfIndex)
    {
        return (-1);
    }
    else if (pHierarchyNode1->i4IfIndex > pHierarchyNode2->i4IfIndex)
    {
        return (1);
    }
    else
    {
        /* Check the Level then SchedId */
        if (pHierarchyNode1->u1Level < pHierarchyNode2->u1Level)
        {
            return (-1);
        }
        else if (pHierarchyNode1->u1Level > pHierarchyNode2->u1Level)
        {
            return (1);
        }
        else
        {
            /* Both Level are Same Check the SchedId */
            if (pHierarchyNode1->u4SchedId < pHierarchyNode2->u4SchedId)
            {
                return (-1);
            }
            else if (pHierarchyNode1->u4SchedId > pHierarchyNode2->u4SchedId)
            {
                return (1);
            }
        }
    }
    return (0);

}

/*****************************************************************************/
/* Function Name      : QoSHierarchyTblRBTFreeNodeFn                         */
/* Description        : This function is used to Delete the Hierarchy Table  */
/*                      Entries and Release the Memory to the respective     */
/*                      Mem Pool .                                           */
/* Input(s)           : tRBElem     - Pointer to Hierarchy Table Entry       */
/*                      u4Arg       - Unused Param                           */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : QOS_SUCCESS /QOS_FAILURE                             */
/* Called By          : SNMP                                                 */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QoSHierarchyTblRBTFreeNodeFn (tRBElem * pRBElem, UINT4 u4Arg)
{
    UINT4               u4RetStatus = MEM_FAILURE;

    UNUSED_PARAM (u4Arg);

    if (pRBElem == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : Hierarchy Tbl Entry is NULL.\r\n",
                      __FUNCTION__);

        return (QOS_FAILURE);
    }

    MEMSET (pRBElem, 0, sizeof (tQoSHierarchyNode));

    /* Release the Mem Block */
    u4RetStatus = MemReleaseMemBlock (gQoSGlobalInfo.QoSHierarchyTblMemPoolId,
                                      (UINT1 *) pRBElem);

    if (u4RetStatus != MEM_SUCCESS)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : HierarchyTbl- "
                      "MemReleaseMemBlock () Failed.\r\n", __FUNCTION__);
    }

    return (QOS_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSUtlGetHierarchyNode                               */
/* Description        : This function is used to Get the Entry  form  the    */
/*                      hierarchy Table ,it will do the following Action     */
/*                      1. Call RBTreeGet Utility.                           */
/* Input(s)           : i4IfIndex  - Interface Index                         */
/*                    : u4HL       - Hierarchy Level                         */
/*                    : u4SchedId  - Scheduler Id                            */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : gQoSGlobalInfo                                       */
/* Global Variables                                                          */
/* Modified           :                                                      */
/* Return Value(s)    : NULL/ Pointer to 'tQoSHierarchyNode'                 */
/* Called By          : SNMP Low Level                                       */
/* Calling Function   :                                                      */
/*****************************************************************************/
tQoSHierarchyNode  *
QoSUtlGetHierarchyNode (INT4 i4IfIndex, UINT4 u4HL, UINT4 u4SchedId)
{
    tQoSHierarchyNode   TempHierarchyNode;
    tQoSHierarchyNode  *pHierarchyNode = NULL;

    MEMSET (&TempHierarchyNode, 0, sizeof (tQoSHierarchyNode));

    TempHierarchyNode.i4IfIndex = i4IfIndex;
    TempHierarchyNode.u1Level = (UINT1) u4HL;
    TempHierarchyNode.u4SchedId = u4SchedId;

    pHierarchyNode = (tQoSHierarchyNode *)
        RBTreeGet (gQoSGlobalInfo.pRbHierarchyTbl,
                   (tRBElem *) & TempHierarchyNode);
    if (pHierarchyNode == NULL)
    {
        QOS_TRC_ARG4 (MGMT_TRC, "%s : HierarchyNode (Index : %d.%d.%d) is not "
                      "Found in the Hierarchy Table.\r\n", __FUNCTION__,
                      i4IfIndex, u4HL, u4SchedId);
    }

    return (pHierarchyNode);
}

/*****************************************************************************/
/* Function Name      : QoSGetNextHierarchyTblEntryIndex                     */
/* Description        : This function is used to Get the Next Entry from the */
/*                      Hierarchy Table ,it will do the following Action     */
/*                      1. If u1HL and u4SchedId is 0 then GetFirxtIndex.    */
/*                      2. If u1HL and u4SchedId is not 0 then GetNextIndex. */
/* Input(s)           : i4IfIndex       - Current ifIndex                    */
/*                      pi4NextIfIndex  - Pointer to Next ifIndex            */
/*                      u4HL            - Current Hierarchy Level            */
/*                      pu4NextHL       - Pointer to Next Hierarchy Level    */
/*                      u4SchedId       - Current Scheduler Id               */
/*                      pu4NextSchedId  - Pointer to NextSchedId             */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : gQoSGlobalInfo                                       */
/* Global Variables                                                          */
/* Modified           : gQoSGlobalInfo                                       */
/* Return Value(s)    : QOS_SUCCESS or QOS_FAILURE                           */
/* Called By          : SNMP Low Level                                       */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QoSGetNextHierarchyTblEntryIndex (INT4 i4IfIndex, INT4 *pi4NextIfIndex,
                                  UINT4 u4HL, UINT4 *pu4NextHL,
                                  UINT4 u4SchedId, UINT4 *pu4NextSchedId)
{
    tQoSHierarchyNode   HierarchyNode;
    tQoSHierarchyNode  *pHierarchyNode = NULL;

    HierarchyNode.i4IfIndex = i4IfIndex;
    HierarchyNode.u1Level = (UINT1) u4HL;
    HierarchyNode.u4SchedId = u4SchedId;

    pHierarchyNode = (tQoSHierarchyNode *)
        RBTreeGetNext (gQoSGlobalInfo.pRbHierarchyTbl,
                       &HierarchyNode, QoSHierarchyCmpFun);

    if (pHierarchyNode == NULL)
    {
        QOS_TRC_ARG4 (MGMT_TRC, "In %s : RBTreeGetNext () Failed, for "
                      "HierarchyNode (Index : %d.%d.%d). \r\n",
                      __FUNCTION__, i4IfIndex, u4HL, u4SchedId);

        return (QOS_FAILURE);
    }

    *pi4NextIfIndex = pHierarchyNode->i4IfIndex;
    *pu4NextHL = pHierarchyNode->u1Level;
    *pu4NextSchedId = pHierarchyNode->u4SchedId;

    return (QOS_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSCreateHierarchyTblEntry                           */
/* Description        : This function is used to Create an Entry in   the    */
/*                      Hierarchy Table ,it will do the following Action     */
/*                      1. Allocate a memory block for the New Entry         */
/*                      2. Initialize the New Entry                          */
/*                      3. Add this New Entry into the Hierarchy Table       */
/* Input(s)           : i4IfIndex  - Interface Index                         */
/*                    : u4HL       - Hierarchy Level                         */
/*                    : u4SchedId  - Scheduler Id                            */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : gQoSGlobalInfo                                       */
/* Global Variables                                                          */
/* Modified           : gQoSGlobalInfo                                       */
/* Return Value(s)    : NULL / Pointer to  HierarchyNode                     */
/* Called By          : SNMP Low Level                                       */
/* Calling Function   :                                                      */
/*****************************************************************************/
tQoSHierarchyNode  *
QoSCreateHierarchyTblEntry (INT4 i4IfIndex, UINT4 u4HL, UINT4 u4SchedId)
{
    tQoSHierarchyNode  *pNewHierarchyNode = NULL;
    UINT4               u4RetStatus = QOS_FAILURE;

    /* 1. Allocate a memory block for the New Entry */
    pNewHierarchyNode = (tQoSHierarchyNode *)
        MemAllocMemBlk (gQoSGlobalInfo.QoSHierarchyTblMemPoolId);
    if (pNewHierarchyNode == NULL)
    {
        QOS_TRC_ARG4 (MGMT_TRC, "In %s : MemAllocateMemBlock () Failed ,"
                      "for HierarchyNode (Index : %d.%d.%d). \r\n",
                      __FUNCTION__, i4IfIndex, u4HL, u4SchedId);
        return (NULL);
    }

    /* 2. Initialize the New Entry */
    MEMSET (pNewHierarchyNode, 0, sizeof (tQoSHierarchyNode));

    pNewHierarchyNode->i4IfIndex = i4IfIndex;
    pNewHierarchyNode->u1Level = (UINT1) u4HL;
    pNewHierarchyNode->u4SchedId = u4SchedId;
    pNewHierarchyNode->u1Status = NOT_READY;

    /* 3. Add this New Entry into the Hierarchy Table */
    u4RetStatus = RBTreeAdd (gQoSGlobalInfo.pRbHierarchyTbl,
                             (tRBElem *) pNewHierarchyNode);
    if (u4RetStatus != RB_SUCCESS)
    {
        u4RetStatus =
            MemReleaseMemBlock (gQoSGlobalInfo.QoSHierarchyTblMemPoolId,
                                (UINT1 *) pNewHierarchyNode);
        if (u4RetStatus == MEM_FAILURE)
        {
            QOS_TRC_ARG4 (MGMT_TRC, "In %s : MemReleaseMemBlock () Failed ,"
                          "for HierarchyNode (Index : %d.%d.%d). \r\n",
                          __FUNCTION__, i4IfIndex, u4HL, u4SchedId);

            return (NULL);
        }

        QOS_TRC_ARG4 (MGMT_TRC, "In %s : RBTreeAdd () Failed ,"
                      "for HierarchyNode (Index : %d.%d.%d). \r\n",
                      __FUNCTION__, i4IfIndex, u4HL, u4SchedId);
        return (NULL);
    }

    return (pNewHierarchyNode);
}

/*****************************************************************************/
/* Function Name      : QoSDeleteHierarchyTblEntry                           */
/* Description        : This function is used to Delete an Entry from the    */
/*                      Hierarchy Table ,it will do the following Action     */
/*                      1. Remove the Entry from the Hierarchy Table         */
/*                      2. Clear the Removed Entry                           */
/*                      3. Release the Removed Entry's memory                */
/* Input(s)           : pHierarchyNode  - Pointer to HierarchyNode           */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : gQoSGlobalInfo                                       */
/* Global Variables                                                          */
/* Modified           : gQoSGlobalInfo                                       */
/* Return Value(s)    : QOS_SUCCESS or QOS_FAILURE                           */
/* Called By          : SNMP Low Level                                       */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QoSDeleteHierarchyTblEntry (tQoSHierarchyNode * pHierarchyNode)
{
    tQoSHierarchyNode  *pDelHierarchyNode = NULL;
    tQoSQNode          *pTmpQNode = NULL;
    tQoSSchedNode      *pTmpSchedNode = NULL;
    UINT4               u4RetStatus = QOS_FAILURE;

    if (pHierarchyNode == NULL)
    {
        return (QOS_FAILURE);
    }

    /* 1. Remove Entry from the Q Table */
    pDelHierarchyNode = RBTreeRem (gQoSGlobalInfo.pRbHierarchyTbl,
                                   (tRBElem *) pHierarchyNode);
    if (pDelHierarchyNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : RBTreeRem () Failed.\r\n",
                      __FUNCTION__);
        return (QOS_FAILURE);
    }
    /* Remove Scheduler Reference */
    if (pDelHierarchyNode->u4SchedNext != 0)
    {
        pTmpSchedNode = QoSUtlGetSchedNode (pDelHierarchyNode->i4IfIndex,
                                            pDelHierarchyNode->u4SchedNext);
        if (pTmpSchedNode == NULL)
        {
            QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetSchedNode () "
                          "Returns FAILURE. \r\n", __FUNCTION__);
            return (QOS_FAILURE);
        }

        pTmpSchedNode->u4RefCount = (pTmpSchedNode->u4RefCount) - 1;
    }

    /* Remove Q Reference */
    if (pHierarchyNode->u4QNext != 0)
    {
        pTmpQNode = QoSUtlGetQNode (pHierarchyNode->i4IfIndex,
                                    pHierarchyNode->u4QNext);
        if (pTmpQNode == NULL)
        {
            QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetQNode () "
                          "Returns FAILURE. \r\n", __FUNCTION__);
            return (QOS_FAILURE);
        }
        pTmpQNode->u4RefCount = (pTmpQNode->u4RefCount) - 1;
    }

    /* 2. Clear the Removed Entry */
    MEMSET (pDelHierarchyNode, 0, sizeof (tQoSHierarchyNode));

    /* 3. Release the Removed Entry's memory */
    u4RetStatus = MemReleaseMemBlock (gQoSGlobalInfo.QoSHierarchyTblMemPoolId,
                                      (UINT1 *) pDelHierarchyNode);
    if (u4RetStatus == MEM_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : MemReleaseMemBlock () Failed. \r\n",
                      __FUNCTION__);

        return (QOS_FAILURE);
    }

    return (QOS_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSUtlValidateHierarchyTblIdxInst                    */
/* Description        : This function is used to Validate the Given Index    */
/*                      instance in the hierarchy Table, if it is ACTIVE     */
/*                      return Failure Success otherwise.                    */
/* Input(s)           : i4IfIndex     - Interface Index                      */
/*                    : u1HL          - Hierarchy Level                      */
/*                    : u4SchedId     - Scheduler Id                         */
/*                      pu4ErrorCode  - Pointer to Error Code                */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : gQoSGlobalInfo                                       */
/* Global Variables                                                          */
/* Modified           : gQoSGlobalInfo                                       */
/* Return Value(s)    : QOS_SUCCESS or QOS_FAILURE                           */
/* Called By          : SNMP Low Level                                       */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QoSUtlValidateHierarchyTblIdxInst (INT4 i4IfIndex, UINT4 u4HL, UINT4 u4SchedId,
                                   UINT4 *pu4ErrorCode)
{
    tQoSHierarchyNode  *pHierarchyNode = NULL;
    INT4                i4RetStatus = 0;

    if (gQoSGlobalInfo.eSysControl != QOS_SYS_CNTL_START)
    {
        QOS_TRC (MGMT_TRC, QOS_MODULE_SHUTDOW_ERROR_MSG);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (QOS_CLI_ERR_MODULE_SHUTDOW);
        return (QOS_FAILURE);
    }

    /* Check the Range of Index Values (Min-Max) */
    i4RetStatus = QoSUtlValidateIfIndex (i4IfIndex, pu4ErrorCode);
    if (i4RetStatus == QOS_FAILURE)
    {
        return (QOS_FAILURE);
    }

    /* Check the Range of Index(SchedId) Values (Min-Max) */
    i4RetStatus = QOS_CHECK_SCHED_TBL_INDEX_RANGE (u4SchedId);
    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG3 (MGMT_TRC, "%s : HierarchySched  Id %d is out of Range."
                      " The Range Should be (1-%d). \r\n", __FUNCTION__,
                      u4SchedId, QOS_SCHED_TBL_MAX_INDEX_RANGE);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (QOS_CLI_ERR_SCHED_RANGE);
        return (QOS_FAILURE);
    }

    /* Check the Range of Index(HL) Values (Min-Max) */
    if ((u4HL <= QOS_SCHED_HL_MIN_VAL) || (u4HL > QOS_SCHED_HL_MAX_VAL))
    {
        QOS_TRC_ARG4 (MGMT_TRC, "%s : Hierarchy Level %d is out of Range."
                      " The Range Should be (%d-%d). \r\n", __FUNCTION__,
                      u4HL, (QOS_SCHED_HL_MIN_VAL + 1), QOS_SCHED_HL_MAX_VAL);

        CLI_SET_ERR (QOS_CLI_ERR_HL_INVALID);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (QOS_FAILURE);
    }

    pHierarchyNode = QoSUtlGetHierarchyNode (i4IfIndex, u4HL, u4SchedId);
    if (pHierarchyNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetHierarchyNode () Returns "
                      "FAILURE. \r\n", __FUNCTION__);

        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (QOS_CLI_ERR_HIERARCHY_NO_ENTRY);
        return (QOS_FAILURE);
    }

    if (pHierarchyNode->u1Status == ACTIVE)
    {
        QOS_TRC (MGMT_TRC, "Can not change the value in ACTIVE State.\r\n");

        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (QOS_CLI_ERR_STATUS_ACTIVE);
        return (QOS_FAILURE);
    }

    return (QOS_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSUtlValidateNextQ                                  */
/* Description        : This function is used to Validate the Given Q Entry  */
/*                      is exists or not and also it should be ACTIVE        */
/* Input(s)           : i4IfIndex        - Interface Index                   */
/*                    : u4HQNext         - Next Q Id in the Hierarchy        */
/*                      pu4ErrorCode     - Pointer to Error Code             */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : gQoSGlobalInfo                                       */
/* Global Variables                                                          */
/* Modified           : gQoSGlobalInfo                                       */
/* Return Value(s)    : QOS_SUCCESS or QOS_FAILURE                           */
/* Called By          : SNMP Low Level                                       */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QoSUtlValidateNextQ (INT4 i4IfIndex, UINT4 u4HQNext, UINT4 *pu4ErrorCode)
{
    tQoSQNode          *pQNode = NULL;

    pQNode = QoSUtlGetQNode (i4IfIndex, u4HQNext);
    if (pQNode == NULL)
    {
        QOS_TRC_ARG3 (MGMT_TRC, "%s : The QNode (Index : %d.%d), Not yet "
                      "Configured.\r\n", __FUNCTION__, i4IfIndex, u4HQNext);
        CLI_SET_ERR (QOS_CLI_ERR_Q_NO_ENTRY);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;

        return (QOS_FAILURE);
    }

    if (pQNode->u1Status != ACTIVE)
    {
        QOS_TRC_ARG3 (MGMT_TRC, "%s : The QNode (Index : %d.%d), not Active."
                      "\r\n", __FUNCTION__, i4IfIndex, u4HQNext);
        CLI_SET_ERR (QOS_CLI_ERR_Q_NOT_ACTIVE);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;

        return (QOS_FAILURE);
    }

    return (QOS_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSValidateHierarchyTblEntry                         */
/* Description        : This function is used to Validate the Given          */
/*                      Hierarchy Entry's Mandatory Params                   */
/* Input(s)           : pHierarchyNode   - hierarchy Entry Ptr               */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : gQoSGlobalInfo                                       */
/* Global Variables                                                          */
/* Modified           : gQoSGlobalInfo                                       */
/* Return Value(s)    : QOS_SUCCESS or QOS_FAILURE                           */
/* Called By          : SNMP Low Level                                       */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QoSValidateHierarchyTblEntry (tQoSHierarchyNode * pHierarchyNode)
{
    /*Check Mandatory Fields */
    if (pHierarchyNode == NULL)
    {
        return (QOS_FAILURE);
    }

    if ((pHierarchyNode->u4QNext == 0) && (pHierarchyNode->u4SchedNext == 0))
    {
        pHierarchyNode->u1Status = NOT_READY;
        return (QOS_FAILURE);
    }

    pHierarchyNode->u1Status = NOT_IN_SERVICE;
    return (QOS_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSUtlValidateHTIndex                                */
/* Description        : This function is used to Validate the Given          */
/*                      Hierarchy Entry's Indices                            */
/* Input(s)           : i4IfIndex        - Interface Index                   */
/*                    : u4SchedId        - Scheduler Id                      */
/*                      u4HL             - Hierarchy Level                   */
/*                      pu4ErrorCode     - Pointer to Error Code             */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : gQoSGlobalInfo                                       */
/* Global Variables                                                          */
/* Modified           : gQoSGlobalInfo                                       */
/* Return Value(s)    : QOS_SUCCESS or QOS_FAILURE                           */
/* Called By          : SNMP Low Level                                       */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QoSUtlValidateHTIndex (INT4 i4IfIndex, UINT4 u4SchedId, UINT4 u4HL,
                       UINT4 *pu4ErrorCode)
{
    tQoSSchedNode      *pSchedNode = NULL;

    pSchedNode = QoSUtlGetSchedNode (i4IfIndex, u4SchedId);
    if (pSchedNode == NULL)
    {
        QOS_TRC_ARG3 (MGMT_TRC, "%s : The Scheduler %d.%d . Not yet Configured."
                      "\r\n", __FUNCTION__, i4IfIndex, u4SchedId);
        CLI_SET_ERR (QOS_CLI_ERR_SCHED_NO_ENTRY);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (QOS_FAILURE);
    }

    if (pSchedNode->u1HierarchyLevel != (UINT1) u4HL)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (QOS_CLI_ERR_HL_NOT_MATCH);
        return (QOS_FAILURE);
    }

    return (QOS_SUCCESS);
}

/*****************************************************************************/
/*                     Q TABLE FUNCTIONS                                     */
/*****************************************************************************/
/*****************************************************************************/
/* Function Name      : DiffServQCmpFun                                      */
/* Description        : This function is used as a Compare function in the   */
/*                      RBTree.                                              */
/*                      1.It Compares the Q Ids and return the values        */
/* Input(s)           : e1 , e2 -  Elements of RBTree                        */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    :  -1/0/1                                              */
/* Called By          : RBTreeUtility                                        */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
DiffServQCmpFun (tRBElem * e1, tRBElem * e2)
{
    tQosStdQEntry      *pQNode1 = NULL;
    tQosStdQEntry      *pQNode2 = NULL;

    pQNode1 = (tQosStdQEntry *) e1;
    pQNode2 = (tQosStdQEntry *) e2;

    /* Check the IfIndex */
    if (pQNode1->u4DServQId < pQNode2->u4DServQId)
    {
        return (-1);
    }
    else if (pQNode1->u4DServQId > pQNode2->u4DServQId)
    {
        return (1);
    }

    return (0);
}

/*****************************************************************************/
/* Function Name      : QoSQCmpFun                                           */
/* Description        : This function is used as a Compare function in the   */
/*                      RBTree.                                              */
/*                      1.It Compares the Q Ids and return the values        */
/* Input(s)           : e1 , e2 -  Elements of RBTree                        */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    :  -1/0/1                                              */
/* Called By          : RBTreeUtility                                        */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QoSQCmpFun (tRBElem * e1, tRBElem * e2)
{
    tQoSQNode          *pQNode1 = NULL;
    tQoSQNode          *pQNode2 = NULL;

    pQNode1 = (tQoSQNode *) e1;
    pQNode2 = (tQoSQNode *) e2;

    /* Check the IfIndex */
    if (pQNode1->i4IfIndex < pQNode2->i4IfIndex)
    {
        return (-1);
    }
    else if (pQNode1->i4IfIndex > pQNode2->i4IfIndex)
    {
        return (1);
    }
    else
    {
        /* Compare the Q Id */
        if (pQNode1->u4Id < pQNode2->u4Id)
        {
            return (-1);
        }
        else if (pQNode1->u4Id > pQNode2->u4Id)
        {
            return (1);
        }
    }

    return (0);
}

/*****************************************************************************/
/* Function Name      : QoSQTblRBTFreeNodeFn                                 */
/* Description        : This function is used to Delete the Q Table          */
/*                      Entries and Release the Memory to the respective     */
/*                      Mem Pool .                                           */
/* Input(s)           : tRBElem     - Pointer to Q Table Entry               */
/*                      u4Arg       - Unused Param                           */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : QOS_SUCCESS /QOS_FAILURE                             */
/* Called By          : SNMP                                                 */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QoSQTblRBTFreeNodeFn (tRBElem * pRBElem, UINT4 u4Arg)
{
    UINT4               u4RetStatus = MEM_FAILURE;

    UNUSED_PARAM (u4Arg);

    /* Release the Mem Block */
    u4RetStatus =
        MemReleaseMemBlock (gQoSGlobalInfo.QoSQTblMemPoolId, (UINT1 *) pRBElem);

    if (u4RetStatus != MEM_SUCCESS)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QTbl- "
                      "MemReleaseMemBlock () Failed.\r\n", __FUNCTION__);
    }

    return (QOS_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSUtlGetQNode                                       */
/* Description        : This function is used to Get the Entry  form  the    */
/*                      Q Table ,it will do the following Action             */
/*                      1. Call RBTreeGet Utility.                           */
/* Input(s)           : ifIndex     - Interface Index                        */
/*                    : u4QId       - Q Id within the Interface              */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : gQoSGlobalInfo                                       */
/* Global Variables                                                          */
/* Modified           :                                                      */
/* Return Value(s)    : NULL/ Pointer to 'tQoSQNode'                         */
/* Called By          : SNMP Low Level                                       */
/* Calling Function   :                                                      */
/*****************************************************************************/
tQoSQNode          *
QoSUtlGetQNode (INT4 i4IfIndex, UINT4 u4QId)
{
    tQoSQNode           TempQNode;
    tQoSQNode          *pQNode = NULL;

    MEMSET (&TempQNode, 0, sizeof (tQoSQNode));

    TempQNode.i4IfIndex = i4IfIndex;
    TempQNode.u4Id = u4QId;

    pQNode = (tQoSQNode *) RBTreeGet (gQoSGlobalInfo.pRbQTbl,
                                      (tRBElem *) & TempQNode);
    if (pQNode == NULL)
    {
        QOS_TRC_ARG3 (MGMT_TRC, "%s : QNode (Index : %d.%d) is not Found."
                      "\r\n", __FUNCTION__, i4IfIndex, u4QId);
    }

    return (pQNode);
}

/*****************************************************************************/
/* Function Name      : QoSCreateQTblEntry                                   */
/* Description        : This function is used to Create an Entry in   the    */
/*                      Q Table ,it will do the following Action             */
/*                      1. Allocate a memory block for the New Entry         */
/*                      2. Initialize the New Entry                          */
/*                      3. Add this New Entry into the Q Table               */
/* Input(s)           : i4IfIndex  - Index to Interface                      */
/*                    : u4QId      - Index to QNode                          */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : gQoSGlobalInfo                                       */
/* Global Variables                                                          */
/* Modified           : gQoSGlobalInfo                                       */
/* Return Value(s)    : NULL / Pointer to  QNode                             */
/* Called By          : SNMP Low Level                                       */
/* Calling Function   :                                                      */
/*****************************************************************************/
tQoSQNode          *
QoSCreateQTblEntry (INT4 i4IfIndex, UINT4 u4QId)
{
    UINT4               u4RetStatus = QOS_FAILURE;
    tQoSQNode          *pNewQNode = NULL;

    /* 1. Allocate a memory block for the New Entry */
    pNewQNode = (tQoSQNode *) MemAllocMemBlk (gQoSGlobalInfo.QoSQTblMemPoolId);
    if (pNewQNode == NULL)
    {
        QOS_TRC_ARG3 (MGMT_TRC, "In %s : MemAllocMemBlk() Failed , for QNode "
                      "(Index : %d.%d) .\r\n", __FUNCTION__, i4IfIndex, u4QId);
        return (NULL);
    }

    /* 2. Initialize the New Entry */
    MEMSET (pNewQNode, 0, sizeof (tQoSQNode));

    pNewQNode->i4IfIndex = i4IfIndex;
    pNewQNode->u4Id = u4QId;
    pNewQNode->u4QueueHwId = 0;
    pNewQNode->u1Status = NOT_READY;
    pNewQNode->u1Priority = 1;
    pNewQNode->u2Weight = 1;
    if (u4QId <= QOS_QUEUE_MAX_NUM_UCOSQ)
    {
        pNewQNode->u4QueueType = QOS_Q_UNICAST_TYPE;
    }
    else if ((u4QId <= QOS_QUEUE_ENTRY_MAX) && (QOS_QUEUE_MAX_NUM_MCOSQ != 0))
    {
        pNewQNode->u4QueueType = QOS_Q_MULTICAST_TYPE;
    }
    else
    {
        pNewQNode->u4QueueType = QOS_Q_SUBSCRIBER_TYPE;
    }
    /* 3. Add this New Entry into the Q Table */
    u4RetStatus = RBTreeAdd (gQoSGlobalInfo.pRbQTbl, (tRBElem *) pNewQNode);
    if (u4RetStatus != RB_SUCCESS)
    {
        u4RetStatus = MemReleaseMemBlock (gQoSGlobalInfo.QoSQTblMemPoolId,
                                          (UINT1 *) pNewQNode);
        if (u4RetStatus == MEM_FAILURE)
        {
            QOS_TRC_ARG3 (MGMT_TRC, "In %s : MemReleaseMemBlock () Failed for "
                          "QNode (Index : %d.%d) .\r\n", __FUNCTION__,
                          i4IfIndex, u4QId);
            return (NULL);
        }

        QOS_TRC_ARG3 (MGMT_TRC, "In %s : RBTreeAdd () Failed ,for QNode "
                      "(Index : %d.%d) .\r\n", __FUNCTION__, i4IfIndex, u4QId);
        return (NULL);
    }

    return (pNewQNode);
}

/*****************************************************************************/
/* Function Name      : DiffServCreateQTblEntry                             */
/* Description        : This function is used to Create an Entry in   the    */
/*                      Q Table ,it will do the following Action             */
/*                      1. Allocate a memory block for the New Entry         */
/*                      2. Initialize the New Entry                          */
/*                      3. Add this New Entry into the Q Table               */
/* Input(s)           : i4IfIndex  - Index to Interface                      */
/*                    : u4QId      - Index to QNode                          */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : gQoSGlobalInfo                                       */
/* Global Variables                                                          */
/* Modified           : gQoSGlobalInfo                                       */
/* Return Value(s)    : NULL / Pointer to  QNode                             */
/* Called By          : SNMP Low Level                                       */
/* Calling Function   :                                                      */
/*****************************************************************************/
tQosStdQEntry      *
DiffServCreateQTblEntry (UINT4 u4QId)
{
    UINT4               u4RetStatus = QOS_FAILURE;
    tQosStdQEntry      *pNewQNode = NULL;

    /* 1. Allocate a memory block for the New Entry */
    pNewQNode = (tQosStdQEntry *) MemAllocMemBlk (gQoSGlobalInfo.DsQPoolId);
    if (pNewQNode == NULL)
    {
        QOS_TRC_ARG3 (MGMT_TRC, "In %s : MemAllocMemBlk() Failed , for QNode "
                      "(Index : %d.%d) .\r\n", __FUNCTION__, 0, u4QId);
        return (NULL);
    }

    /* 2. Initialize the New Entry */
    MEMSET (pNewQNode, 0, sizeof (tQoSQNode));

    pNewQNode->u4DServQId = u4QId;
    pNewQNode->i4StorageType = QOS_STORAGE_NONVOLATILE;
    pNewQNode->u1DSQStatus = NOT_READY;

    /* 3. Add this New Entry into the Q Table */
    u4RetStatus = RBTreeAdd (gQoSGlobalInfo.DsQHead, (tRBElem *) pNewQNode);
    if (u4RetStatus != RB_SUCCESS)
    {
        u4RetStatus = MemReleaseMemBlock (gQoSGlobalInfo.DsQPoolId,
                                          (UINT1 *) pNewQNode);
        if (u4RetStatus == MEM_FAILURE)
        {
            QOS_TRC_ARG3 (MGMT_TRC, "In %s : MemReleaseMemBlock () Failed for "
                          "QNode (Index : %d.%d) .\r\n", __FUNCTION__,
                          0, u4QId);
            return (NULL);
        }
        return (NULL);
    }

    return (pNewQNode);
}

/*****************************************************************************/
/* Function Name      : QoSGetNextQTblEntryIndex                             */
/* Description        : This function is used to Get the Next Entry from the */
/*                      Q Table ,it will do the following Action             */
/* Input(s)           : i4IfIndex       - Current If index                   */
/*                      pi4NextIfIndex  - Pointer to Next If Index           */
/*                      u4QId           - Current  Q Id                      */
/*                      pu4NextQId      - Pointer to Next Q Id               */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : gQoSGlobalInfo                                       */
/* Global Variables                                                          */
/* Modified           : gQoSGlobalInfo                                       */
/* Return Value(s)    : QOS_SUCCESS or QOS_FAILURE                           */
/* Called By          : SNMP Low Level                                       */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QoSGetNextQTblEntryIndex (INT4 i4IfIndex, INT4 *pi4NextIfIndex,
                          UINT4 u4QId, UINT4 *pu4NextQId)
{
    tQoSQNode           QNode;
    tQoSQNode          *pQNode = NULL;

    QNode.i4IfIndex = i4IfIndex;
    QNode.u4Id = u4QId;
    pQNode = (tQoSQNode *) RBTreeGetNext (gQoSGlobalInfo.pRbQTbl,
                                          &QNode, QoSQCmpFun);
    if (pQNode == NULL)
    {
        QOS_TRC_ARG3 (MGMT_TRC, "In %s : RBTreeGetNext () Failed, for QNode "
                      "(Index : %d.%d) .\r\n", __FUNCTION__, i4IfIndex, u4QId);
        return (QOS_FAILURE);
    }

    *pi4NextIfIndex = pQNode->i4IfIndex;
    *pu4NextQId = pQNode->u4Id;

    return (QOS_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSDeleteQTblEntry                                   */
/* Description        : This function is used to Delete an Entry from the    */
/*                      Q Table ,it will do the following Action             */
/*                      1. Remove the Entry from the Q Table                 */
/*                      2. Clear the Removed Entry                           */
/*                      3. Release the Removed Entry's memory                */
/* Input(s)           : pQNode   - Pointer to QNode                          */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : gQoSGlobalInfo                                       */
/* Global Variables                                                          */
/* Modified           : gQoSGlobalInfo                                       */
/* Return Value(s)    : QOS_SUCCESS or QOS_FAILURE                           */
/* Called By          : SNMP Low Level                                       */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QoSDeleteQTblEntry (tQoSQNode * pQNode)
{
    tQoSQNode          *pDelQNode = NULL;
    tQoSQTemplateNode  *pQTempNode = NULL;
    tQoSSchedNode      *pSchedNode = NULL;
    tQoSShapeTemplateNode *pShapeNode = NULL;
    UINT4               u4RetStatus = QOS_FAILURE;

    if (pQNode == NULL)
    {
        return (QOS_FAILURE);
    }

    /* 1. Remove Entry from the Q Table */
    pDelQNode = RBTreeRem (gQoSGlobalInfo.pRbQTbl, (tRBElem *) pQNode);
    if (pDelQNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : RBTreeRem () Failed.\r\n",
                      __FUNCTION__);
        return (QOS_FAILURE);
    }

    /* 2. Decrement the RefCount for QTemp, Sched,Shape Tables. */
    if (pQNode->u4CfgTemplateId != 0)
    {
        pQTempNode = QoSUtlGetQTempNode (pQNode->u4CfgTemplateId);
        if (pQTempNode == NULL)
        {
            QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetQTempNode () "
                          "Returns FAILURE. \r\n", __FUNCTION__);

            return (QOS_FAILURE);
        }
        pQTempNode->u4RefCount = (pQTempNode->u4RefCount) - 1;
    }

    if (pQNode->u4SchedulerId != 0)
    {
        pSchedNode = QoSUtlGetSchedNode (pQNode->i4IfIndex,
                                         pQNode->u4SchedulerId);
        if (pSchedNode == NULL)
        {
            QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetSchedNode () "
                          "Returns FAILURE. \r\n", __FUNCTION__);

            return (QOS_FAILURE);
        }
        pSchedNode->u4RefCount = (pSchedNode->u4RefCount) - 1;
    }

    if (pQNode->u4ShapeId != 0)
    {
        pShapeNode = QoSUtlGetShapeTempNode (pQNode->u4ShapeId);
        if (pShapeNode == NULL)
        {
            QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetShapeTempNode () "
                          "Returns FAILURE. \r\n", __FUNCTION__);

            return (QOS_FAILURE);
        }
        pShapeNode->u4RefCount = (pShapeNode->u4RefCount) - 1;
    }

    /* 3. Clear the Removed Entry */
    MEMSET (pDelQNode, 0, sizeof (tQoSQNode));

    /* 4. Release the Removed Entry's memory */
    u4RetStatus = MemReleaseMemBlock (gQoSGlobalInfo.QoSQTblMemPoolId,
                                      (UINT1 *) pDelQNode);
    if (u4RetStatus == MEM_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : MemReleaseMemBlock () Failed. \r\n",
                      __FUNCTION__);

        return (QOS_FAILURE);
    }

    return (QOS_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : DiffServDeleteQTblEntry                              */
/* Description        : This function is used to Delete an Entry from the    */
/*                      Q Table ,it will do the following Action             */
/*                      1. Remove the Entry from the Q Table                 */
/*                      2. Clear the Removed Entry                           */
/*                      3. Release the Removed Entry's memory                */
/* Input(s)           : pQNode   - Pointer to QNode                          */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : gQoSGlobalInfo                                       */
/* Global Variables                                                          */
/* Modified           : gQoSGlobalInfo                                       */
/* Return Value(s)    : QOS_SUCCESS or QOS_FAILURE                           */
/* Called By          : SNMP Low Level                                       */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4                            /*..... Tyyhis function is totally incomplete...do uiot */
DiffServDeleteQTblEntry (tQosStdQEntry * pQNode)
{
    tQosStdQEntry      *pDelQNode = NULL;

    pDelQNode = RBTreeGet (gQoSGlobalInfo.DsQHead, (tRBElem *) pQNode);
    if (pDelQNode != NULL)
    {
        pDelQNode = RBTreeRem (gQoSGlobalInfo.DsQHead, (tRBElem *) pQNode);
        if (pDelQNode == NULL)
        {
            QOS_TRC_ARG1 (MGMT_TRC, "In %s : RBTreeRem () Failed for "
                          "Queue. \r\n", __FUNCTION__);
            return (QOS_FAILURE);
        }
    }
    return (QOS_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSUtlValidateQTblIdxInst                            */
/* Description        : This function is used to Validate the Given Index    */
/*                      instance in the Q Table, if it is ACTIVE             */
/*                      return Failure Success otherwise.                    */
/* Input(s)           : i4IfIndex        - Interface Index                   */
/*                      u4FsQoSQId       - QID                               */
/*                      pu4ErrorCode     - Pointer to Error Code             */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : gQoSGlobalInfo                                       */
/* Global Variables                                                          */
/* Modified           : gQoSGlobalInfo                                       */
/* Return Value(s)    : QOS_SUCCESS or QOS_FAILURE                           */
/* Called By          : SNMP Low Level                                       */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QoSUtlValidateQTblIdxInst (INT4 i4IfIndex, UINT4 u4FsQoSQId,
                           UINT4 *pu4ErrorCode)
{
    tQoSQNode          *pQNode = NULL;

    if (gQoSGlobalInfo.eSysControl != QOS_SYS_CNTL_START)
    {
        QOS_TRC (MGMT_TRC, QOS_MODULE_SHUTDOW_ERROR_MSG);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (QOS_CLI_ERR_MODULE_SHUTDOW);

        return (QOS_FAILURE);
    }

    pQNode = QoSUtlGetQNode (i4IfIndex, u4FsQoSQId);
    if (pQNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetQNode () Returns "
                      "FAILURE. \r\n", __FUNCTION__);

        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (QOS_CLI_ERR_Q_NO_ENTRY);
        return (QOS_FAILURE);
    }

    if (pQNode->u1Status == ACTIVE)
    {
        QOS_TRC (MGMT_TRC, "Can not change the value in ACTIVE State.\r\n");

        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (QOS_CLI_ERR_STATUS_ACTIVE);
        return (QOS_FAILURE);
    }

    return (QOS_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSValidateQTblEntry                                 */
/* Description        : This function is used to Validate the Given Q Table  */
/*                      Entry's Mandatory Fields                             */
/* Input(s)           : pQNode    -  Ptr to Q Node                           */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : gQoSGlobalInfo                                       */
/* Global Variables                                                          */
/* Modified           : gQoSGlobalInfo                                       */
/* Return Value(s)    : QOS_SUCCESS or QOS_FAILURE                           */
/* Called By          : SNMP Low Level                                       */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QoSValidateQTblEntry (tQoSQNode * pQNode)
{
    /*Check Mandatory Fields */
    if (pQNode == NULL)
    {
        return (QOS_FAILURE);
    }

    if (pQNode->u4CfgTemplateId == 0)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "%s :Q TempId Should not be 0.\r\n",
                      __FUNCTION__);
        pQNode->u1Status = NOT_READY;
        return (QOS_FAILURE);
    }

    if (pQNode->u4SchedulerId == 0)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "%s :SchedId Should not be 0.\r\n",
                      __FUNCTION__);
        pQNode->u1Status = NOT_READY;
        return (QOS_FAILURE);
    }

    pQNode->u1Status = NOT_IN_SERVICE;

    return (QOS_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSIsDefQTblEntry                                    */
/* Description        : This function is used to Validate the Given Index    */
/*                      instance is the default  QueueTable Id.If not        */
/*                      present  return Failure Success otherwise.           */
/* Input(s)           : tQoSQNode- pQNode                                    */
/*                                                                           */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/* Return Value(s)    : QOS_SUCCESS or QOS_FAILURE                           */
/* Called By          : SNMP Low Level                                       */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QoSIsDefQTblEntry (tQoSQNode * pQNode)
{
    if (pQNode != NULL)
    {
        if ((pQNode->i4IfIndex > 0) &&
            (pQNode->i4IfIndex <= SYS_DEF_MAX_PHYSICAL_INTERFACES))
        {
            if ((pQNode->u4Id > 0) && (pQNode->u4Id <= QOS_QUEUE_ENTRY_MAX))
            {
                return (QOS_SUCCESS);
            }
        }
    }

    return (QOS_FAILURE);
}

/*****************************************************************************/
/* Function Name      : QoSUtlValidateShapeId                                */
/* Description        : This function is used to Validate the Given Shape Id */
/*                      Entry's is Validate or Not                           */
/* Input(s)           : pQNode    -  Ptr to Q Node                           */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : gQoSGlobalInfo                                       */
/* Global Variables                                                          */
/* Modified           : gQoSGlobalInfo                                       */
/* Return Value(s)    : QOS_SUCCESS or QOS_FAILURE                           */
/* Called By          : SNMP Low Level                                       */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QoSUtlValidateShapeId (UINT4 u4ShapeId, UINT4 *pu4ErrorCode)
{
    tQoSShapeTemplateNode *pShapeNode = NULL;

    pShapeNode = QoSUtlGetShapeTempNode (u4ShapeId);
    if (pShapeNode == NULL)
    {
        QOS_TRC_ARG2 (MGMT_TRC, "%s : The ShapeId %d. Not yet Configured."
                      "\r\n", __FUNCTION__, u4ShapeId);

        CLI_SET_ERR (QOS_CLI_ERR_SHAPE_NOT_CREATED);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (QOS_FAILURE);
    }

    if (pShapeNode->u1Status != ACTIVE)
    {
        QOS_TRC_ARG2 (MGMT_TRC, "%s : The ShapeId %d. not Active."
                      "\r\n", __FUNCTION__, u4ShapeId);

        CLI_SET_ERR (QOS_CLI_ERR_SHAPE_NOT_ACTIVE);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;

        return (QOS_FAILURE);
    }

    return (QOS_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSUtlValidateQTemp                                  */
/* Description        : This function is used to Validate the Given QTenmpId */
/*                      Entry's is Validate or Not                           */
/* Input(s)           : u4QTempId -  Q Template Id                           */
/*                      pu4ErrorCode     - Pointer to Error Code             */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : gQoSGlobalInfo                                       */
/* Global Variables                                                          */
/* Modified           : gQoSGlobalInfo                                       */
/* Return Value(s)    : QOS_SUCCESS or QOS_FAILURE                           */
/* Called By          : SNMP Low Level                                       */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QoSUtlValidateQTemp (UINT4 u4QTempId, UINT4 *pu4ErrorCode)
{
    tQoSQTemplateNode  *pQTempNode = NULL;

    pQTempNode = QoSUtlGetQTempNode (u4QTempId);
    if (pQTempNode == NULL)
    {
        QOS_TRC_ARG2 (MGMT_TRC, "%s : The QTempId %d. Not yet Configured."
                      "\r\n", __FUNCTION__, u4QTempId);
        CLI_SET_ERR (QOS_CLI_ERR_QTEMP_NOT_CREATION);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;

        return (QOS_FAILURE);
    }

    if (pQTempNode->u1Status != ACTIVE)
    {
        QOS_TRC_ARG2 (MGMT_TRC, "%s : The QTempId %d. not Active."
                      "\r\n", __FUNCTION__, u4QTempId);
        CLI_SET_ERR (QOS_CLI_ERR_QTEMP_NOT_ACTIVE);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;

        return (QOS_FAILURE);
    }

    return (QOS_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSUtlValidateSched                                  */
/* Description        : This function is used to Validate the Given SchedId  */
/*                      Entry's is Validate or Not                           */
/* Input(s)           : i4IfIndex -  Interface Index                         */
/*                    : u4SchedId -  Scheduler  Id                           */
/*                      pu4ErrorCode     - Pointer to Error Code             */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : gQoSGlobalInfo                                       */
/* Global Variables                                                          */
/* Modified           : gQoSGlobalInfo                                       */
/* Return Value(s)    : QOS_SUCCESS or QOS_FAILURE                           */
/* Called By          : SNMP Low Level                                       */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QoSUtlValidateSched (INT4 i4IfIndex, UINT4 u4SchedId, UINT4 *pu4ErrorCode)
{
    tQoSSchedNode      *pSchedNode = NULL;
    UINT4               u4IfIndex, u4SchedulerId;

    pSchedNode = QoSUtlGetSchedNode (i4IfIndex, u4SchedId);
    if (pSchedNode == NULL)
    {
        /* This could be a General Scheduler that can be on any port */
        if (QosSchedulerGetIfIdFromGlobalId (u4SchedId,
                                             &u4IfIndex, &u4SchedulerId)
            == QOS_SUCCESS)
        {
            if (u4IfIndex == 0)
            {
                if ((pSchedNode =
                     QoSUtlGetSchedNode (u4IfIndex, u4SchedulerId)) == NULL)
                {
                    CLI_SET_ERR (QOS_CLI_ERR_SCHED_NO_ENTRY);
                    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                    return (QOS_FAILURE);
                }
            }
            else
            {
                CLI_SET_ERR (QOS_CLI_ERR_SCHED_NO_ENTRY);
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                return (QOS_FAILURE);
            }
        }
        else
        {
            if (u4SchedId == QOS_ROWPOINTER_DEF)
            {
                return QOS_SUCCESS;
            }

            CLI_SET_ERR (QOS_CLI_ERR_SCHED_NO_ENTRY);
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return (QOS_FAILURE);
        }
    }

    if (pSchedNode->u1Status != ACTIVE)
    {
        CLI_SET_ERR (QOS_CLI_ERR_SCHED_NOT_ACTIVE);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return (QOS_FAILURE);
    }

    return (QOS_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSUtlValidateEgressPort                             */
/* Description        : This function is used validate the Given Interface   */
/*                      with the interface module.                           */
/*                      Set the ErrorCode accordingly.                       */
/* Input(s)           : u4EgrPort   - Interface Number.                      */
/*                      pu4ErrorCode - Pointer to ErrorCode.                 */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : QOS_SUCCESS /QOS_FAILURE                             */
/* Called By          : SNMP                                                 */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QoSUtlValidateEgressPort (UINT4 u4EgrPort, UINT4 *pu4ErrorCode)
{
    /* Check the Interface index */
    if (u4EgrPort > SYS_DEF_MAX_PHYSICAL_INTERFACES)
    {
        QOS_TRC_ARG2 (MGMT_TRC, "In %s : Invalid IfIndex %d.\r\n",
                      __FUNCTION__, u4EgrPort);

        CLI_SET_ERR (QOS_CLI_ERR_INVALID_IF_INDEX);

        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;

        return (QOS_FAILURE);
    }
    return (QOS_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSUtlValidateQMapClass                              */
/* Description        : This function is used validate the Given Class is    */
/*                      with in the Range or not .                           */
/* Input(s)           : u4Class     - CLASS ID                               */
/*                      pu4ErrorCode - Pointer to ErrorCode.                 */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : QOS_SUCCESS /QOS_FAILURE                             */
/* Called By          : SNMP                                                 */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QoSUtlValidateQMapClass (UINT4 u4Class, UINT4 *pu4ErrorCode)
{
    /* Check the Range of Index Values (Min-Max) */
    if ((u4Class < 1) || (u4Class > QOS_CLS2PRI_MAP_TBL_MAX_INDEX_RANGE))
    {
        QOS_TRC_ARG3 (MGMT_TRC, "%s : CLASS %d is out of Range. "
                      "The Range Should be (%1-%d). \r\n", __FUNCTION__,
                      u4Class, QOS_CLS2PRI_MAP_TBL_MAX_INDEX_RANGE);

        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;

        return (QOS_FAILURE);
    }

    return (QOS_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSUtlValidateQMapQId                                */
/* Description        : This function is used validate the Given Q Id is     */
/*                      validate or Not.                                     */
/*                      It will check the Given QMapId  EgressPort and       */
/*                      Given Q Id's Scheduler's IfIndex should be same      */
/*                      or not . if  Scheduler's IfIndex is 0 than it is the */
/*                      First Q add in the Q Map Table                       */
/* Input(s)           : i4IfIndex   - Interface Index                        */
/*                      u4QId       - Q Id                                   */
/*                      pu4ErrorCode - Pointer to ErrorCode.                 */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : QOS_SUCCESS /QOS_FAILURE                             */
/* Called By          : SNMP                                                 */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QoSUtlValidateQMapQId (INT4 i4IfIndex, UINT4 u4QId, UINT4 *pu4ErrorCode)
{
    tQoSQNode          *pQNode = NULL;

    pQNode = QoSUtlGetQNode (i4IfIndex, u4QId);
    if (pQNode == NULL)
    {
        QOS_TRC_ARG3 (MGMT_TRC, "%s : The QId %d in the Interface %d Not yet"
                      " Configured." "\r\n", __FUNCTION__, u4QId, i4IfIndex);
        CLI_SET_ERR (QOS_CLI_ERR_Q_NO_ENTRY);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (QOS_FAILURE);
    }

    if (pQNode->u1Status != ACTIVE)
    {
        QOS_TRC_ARG3 (MGMT_TRC, "%s : The QNode (Index : %d.%d) not Active."
                      "\r\n", __FUNCTION__, i4IfIndex, u4QId);
        CLI_SET_ERR (QOS_CLI_ERR_Q_NOT_ACTIVE);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;

        return (QOS_FAILURE);
    }

    return (QOS_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSValidateQMapTblEntry                              */
/* Description        : This function is used validate the Mandatory Params  */
/*                      for a Give Q Map Node.                               */
/* Input(s)           : pQMapNode   - Ptr tp tQoSQMapNode                    */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : QOS_SUCCESS /QOS_FAILURE                             */
/* Called By          : SNMP                                                 */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QoSValidateQMapTblEntry (tQoSQMapNode * pQMapNode)
{
    if (pQMapNode == NULL)
    {
        return (QOS_FAILURE);
    }

    /*Check Mandatory Fields */
    if (pQMapNode->u4QId == 0)
    {
        pQMapNode->u1Status = NOT_READY;
        return (QOS_FAILURE);
    }

    /* All Mandatory Fields are set, change the State to NOT_IN_SERVICE */
    pQMapNode->u1Status = NOT_IN_SERVICE;

    return (QOS_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSIsDefQMapTblEntry                                 */
/* Description        : This function is used to Validate the Given Index    */
/*                      instance is the default  Queue Map Id.If not         */
/*                      present  return Failure Success otherwise.           */
/* Input(s)           : tQoSQMapNode- pQMapNode                              */
/*                                                                           */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : gQoSGlobalInfo                                       */
/* Global Variables                                                          */
/* Modified           : gQoSGlobalInfo                                       */
/* Return Value(s)    : QOS_SUCCESS or QOS_FAILURE                           */
/* Called By          : SNMP Low Level                                       */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QoSIsDefQMapTblEntry (tQoSQMapNode * pQMapNode)
{
    if (pQMapNode != NULL)
    {

        if ((pQMapNode->i4IfIndex == 0) &&
            (pQMapNode->u4CLASS == 0) &&
            (pQMapNode->u1RegenPriType == QOS_QMAP_PRI_TYPE_VLAN_PRI))
        {
            /* QOS_DEFAULT_REGENPRI(pQMapNode->u4QId) == (pQMapNode->u1RegenPri) is  *
             * added to check whether u4QId and u1RegenPri are default values        *
             * Default value of u1RegenPri is u4QId +1                              */
            if ((pQMapNode->u1RegenPri <= QOS_PM_TBL_DEF_ENTRY_MAX) &&
                (QOS_DEFAULT_REGENPRI (pQMapNode->u4QId) ==
                 (pQMapNode->u1RegenPri)))
            {
                return (QOS_SUCCESS);
            }
        }
    }

    return (QOS_FAILURE);
}

/*****************************************************************************/
/*                     SCHEDULER TABLE FUNCTIONS                             */
/*****************************************************************************/

/*****************************************************************************/
/* Function Name      : QoSSchedCmpFun                                       */
/* Description        : This function is used as a Compare function in the   */
/*                      RBTree.                                              */
/*                      1.It Compares the Scheduler Ids and return the value */
/* Input(s)           : e1 , e2 -  Elements of RBTree                        */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    :  -1/0/1                                              */
/* Called By          : RBTreeUtility                                        */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QoSSchedCmpFun (tRBElem * e1, tRBElem * e2)
{
    tQoSSchedNode      *pSchedNode1 = NULL;
    tQoSSchedNode      *pSchedNode2 = NULL;

    pSchedNode1 = (tQoSSchedNode *) e1;
    pSchedNode2 = (tQoSSchedNode *) e2;

    /* Check the IfIndex */
    if (pSchedNode1->i4IfIndex < pSchedNode2->i4IfIndex)
    {
        return (-1);
    }
    else if (pSchedNode1->i4IfIndex > pSchedNode2->i4IfIndex)
    {
        return (1);
    }
    else
    {
        /* Compare the Sched Id */
        if (pSchedNode1->u4Id < pSchedNode2->u4Id)
        {
            return (-1);
        }
        else if (pSchedNode1->u4Id > pSchedNode2->u4Id)
        {
            return (1);
        }
    }
    return (0);
}

/*****************************************************************************/
/* Function Name      : QoSSchedTblRBTFreeNodeFn                             */
/* Description        : This function is used to Delete the Scheduler Table  */
/*                      Entries and Release the Memory to the respective     */
/*                      Mem Pool .                                           */
/* Input(s)           : tRBElem     - Pointer to Scheduler Table Entry       */
/*                      u4Arg       - Unused Param                           */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : QOS_SUCCESS /QOS_FAILURE                             */
/* Called By          : SNMP                                                 */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QoSSchedTblRBTFreeNodeFn (tRBElem * pRBElem, UINT4 u4Arg)
{
    UINT4               u4RetStatus = MEM_FAILURE;

    UNUSED_PARAM (u4Arg);

    /* Release the Mem Block */
    u4RetStatus = MemReleaseMemBlock (gQoSGlobalInfo.QoSSchedTblMemPoolId,
                                      (UINT1 *) pRBElem);
    if (u4RetStatus != MEM_SUCCESS)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : SchedTbl- "
                      "MemReleaseMemBlock () Failed.\r\n", __FUNCTION__);
    }
    return (QOS_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSUtlGetSchedNode                                   */
/* Description        : This function is used to Get the Entry  form  the    */
/*                      Scheduler Table ,it will do the following Action     */
/*                      1. Call RBTreeGet Utility.                           */
/* Input(s)           : i4IfIndex       - Interface Index                    */
/*                    : u4SchedId       - Index to tQoSSchedNode             */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : gQoSGlobalInfo                                       */
/* Global Variables                                                          */
/* Modified           :                                                      */
/* Return Value(s)    : NULL/ Pointer to 'tQoSSchedNode'                     */
/* Called By          : SNMP Low Level                                       */
/* Calling Function   :                                                      */
/*****************************************************************************/
tQoSSchedNode      *
QoSUtlGetSchedNode (INT4 i4IfIndex, UINT4 u4SchedId)
{
    tQoSSchedNode       TempSchedNode;
    tQoSSchedNode      *pSchedNode = NULL;

    MEMSET (&TempSchedNode, 0, sizeof (tQoSSchedNode));

    TempSchedNode.i4IfIndex = i4IfIndex;
    TempSchedNode.u4Id = u4SchedId;

    pSchedNode = (tQoSSchedNode *)
        RBTreeGet (gQoSGlobalInfo.pRbSchedTbl, (tRBElem *) & TempSchedNode);

    if (pSchedNode == NULL)
    {
        QOS_TRC_ARG3 (MGMT_TRC, "%s : Scheduler (Index : %d.%d) is not Found."
                      "\r\n", __FUNCTION__, i4IfIndex, u4SchedId);
    }

    return (pSchedNode);
}

/*****************************************************************************/
/* Function Name      : QoSCreateSchedTblEntry                               */
/* Description        : This function is used to Create an Entry in   the    */
/*                      Scheduler Table ,it will do the following Action     */
/*                      1. Allocate a memory block for the New Entry         */
/*                      2. Initialize the New Entry                          */
/*                      3. Add this New Entry into the Scheduler Table       */
/* Input(s)           : i4IfIndex      - Interface Index                     */
/*                    : u4SchedId      - Index to SchedNode                  */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : gQoSGlobalInfo                                       */
/* Global Variables                                                          */
/* Modified           : gQoSGlobalInfo                                       */
/* Return Value(s)    : NULL / Pointer to  SchedNode                         */
/* Called By          : SNMP Low Level                                       */
/* Calling Function   :                                                      */
/*****************************************************************************/
tQoSSchedNode      *
QoSCreateSchedTblEntry (INT4 i4IfIndex, UINT4 u4SchedId)
{
    UINT4               u4RetStatus = QOS_FAILURE;
    UINT4               u4ShedulerGlobalId;
    tQoSSchedNode      *pNewSchedNode = NULL;
    tQoSSchedNode      *pNewTempSchedNode = NULL;

    pNewTempSchedNode = QoSUtlGetSchedNode (i4IfIndex, u4SchedId);
    if (pNewTempSchedNode != NULL)
    {
        return pNewTempSchedNode;
    }

    /* 1. Allocate a memory block for the New Entry */
    pNewSchedNode = (tQoSSchedNode *)
        MemAllocMemBlk (gQoSGlobalInfo.QoSSchedTblMemPoolId);
    if (pNewSchedNode == NULL)
    {
        QOS_TRC_ARG3 (MGMT_TRC, "In %s : MemAllocMemBlk () Failed ,"
                      "for Scheduler (Index : %d.%d) .\r\n", __FUNCTION__,
                      i4IfIndex, u4SchedId);
        return (NULL);
    }

    /* 2. Initialize the New Entry */
    MEMSET (pNewSchedNode, 0, sizeof (tQoSSchedNode));

    pNewSchedNode->i4IfIndex = i4IfIndex;
    pNewSchedNode->u4Id = u4SchedId;
    pNewSchedNode->u4SchedulerHwId = 0;
    pNewSchedNode->u1HierarchyLevel = QOS_SCHED_HL_DEFAULT;
    pNewSchedNode->u4SchedChildren = 0;
    pNewSchedNode->u1SchedAlgorithm = QOS_SCHED_ALGO_DEFAULT;
    pNewSchedNode->u4ShapeId = 0;
    pNewSchedNode->u1Status = NOT_READY;

    /* 3. Add this New Entry into the Scheduler Table */
    u4RetStatus = RBTreeAdd (gQoSGlobalInfo.pRbSchedTbl,
                             (tRBElem *) pNewSchedNode);
    if (u4RetStatus != RB_SUCCESS)
    {
        u4RetStatus =
            MemReleaseMemBlock (gQoSGlobalInfo.QoSSchedTblMemPoolId,
                                (UINT1 *) pNewSchedNode);

        if (u4RetStatus == MEM_FAILURE)
        {
            QOS_TRC_ARG3 (MGMT_TRC, "In %s : MemReleaseMemBlock () Failed ,"
                          "for Scheduler (Index : %d.%d) .\r\n", __FUNCTION__,
                          i4IfIndex, u4SchedId);

            return (NULL);
        }

        QOS_TRC_ARG3 (MGMT_TRC, "In %s : RBTreeAdd () Failed ,"
                      "for Scheduler (Index : %d.%d) .\r\n", __FUNCTION__,
                      i4IfIndex, u4SchedId);
        return (NULL);
    }
    if (QosSchedulerGetGlobalIdFromIfId
        (i4IfIndex, u4SchedId, &u4ShedulerGlobalId) == QOS_FAILURE)
    {
        /* GLobal Id has not ben created for this pair. create it */
        u4ShedulerGlobalId = QOS_SCHED_NEXT_FREE ();
        if (u4ShedulerGlobalId == 0)
        {
            u4RetStatus =
                MemReleaseMemBlock (gQoSGlobalInfo.QoSSchedTblMemPoolId,
                                    (UINT1 *) pNewSchedNode);

            if (u4RetStatus == MEM_FAILURE)
            {
                QOS_TRC_ARG3 (MGMT_TRC, "In %s : MemReleaseMemBlock () Failed ,"
                              "for Scheduler (Index : %d.%d) .\r\n",
                              __FUNCTION__, i4IfIndex, u4SchedId);

            }
            return NULL;
        }
        if (QosAddSchedulerMapEntry (u4ShedulerGlobalId, i4IfIndex, u4SchedId)
            == QOS_FAILURE)
        {
            u4RetStatus =
                MemReleaseMemBlock (gQoSGlobalInfo.QoSSchedTblMemPoolId,
                                    (UINT1 *) pNewSchedNode);

            if (u4RetStatus == MEM_FAILURE)
            {
                QOS_TRC_ARG3 (MGMT_TRC, "In %s : MemReleaseMemBlock () Failed ,"
                              "for Scheduler (Index : %d.%d) .\r\n",
                              __FUNCTION__, i4IfIndex, u4SchedId);

            }
            return NULL;
        }
        QosUpdateDiffServSchedulerNextFree (u4ShedulerGlobalId,
                                            QOS_CREATE_AND_WAIT);

    }

    pNewSchedNode->u4GlobalId = u4ShedulerGlobalId;

    return (pNewSchedNode);
}

/*****************************************************************************/
/* Function Name      : QoSGetNextSchedTblEntryIndex                         */
/* Description        : This function is used to Get the Next Entry from the */
/*                      Scheduler Table ,it will do the following Action     */
/*                      1. If u4CurrentIndex is 0 then  GetFirxtIndex.       */
/*                      2. If u4CurrentIndex is not 0 then  GetNextIndex.    */
/* Input(s)           : i4IfIndex       - Current Interface Index            */
/*                      pi4NextIfIndex  - Pointer to next Interface Index    */
/*                      u4SchedId       - Current Scheduler Id               */
/*                      pu4NextSchedId  - Pointer to next Scheduler Id       */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : gQoSGlobalInfo                                       */
/* Global Variables                                                          */
/* Modified           : gQoSGlobalInfo                                       */
/* Return Value(s)    : QOS_SUCCESS or QOS_FAILURE                           */
/* Called By          : SNMP Low Level                                       */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QoSGetNextSchedTblEntryIndex (INT4 i4IfIndex, INT4 *pi4NextIfIndex,
                              UINT4 u4SchedId, UINT4 *pu4NextSchedId)
{
    tQoSSchedNode      *pSchedNode = NULL;
    tQoSSchedNode       SchedNode;

    SchedNode.i4IfIndex = i4IfIndex;
    SchedNode.u4Id = u4SchedId;

    pSchedNode = (tQoSSchedNode *) RBTreeGetNext (gQoSGlobalInfo.pRbSchedTbl,
                                                  &SchedNode, QoSSchedCmpFun);
    if (pSchedNode == NULL)
    {
        QOS_TRC_ARG2 (MGMT_TRC, "In %s : RBTreeGetNext () Failed, for "
                      " SchedId %d.\r\n", __FUNCTION__, u4SchedId);
        return (QOS_FAILURE);
    }

    *pi4NextIfIndex = pSchedNode->i4IfIndex;
    *pu4NextSchedId = pSchedNode->u4Id;

    return (QOS_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSDeleteSchedTblEntry                               */
/* Description        : This function is used to Delete an Entry from the    */
/*                      Scheduler Table ,it will do the following Action     */
/*                      1. Remove the Entry from the Scheduler Table         */
/*                      2. Clear the Removed Entry                           */
/*                      3. Release the Removed Entry's memory                */
/* Input(s)           : pSchedNode   - Pointer to SchedNode                  */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : gQoSGlobalInfo                                       */
/* Global Variables                                                          */
/* Modified           : gQoSGlobalInfo                                       */
/* Return Value(s)    : QOS_SUCCESS or QOS_FAILURE                           */
/* Called By          : SNMP Low Level                                       */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QoSDeleteSchedTblEntry (tQoSSchedNode * pSchedNode)
{
    tQoSSchedNode      *pDelSchedNode = NULL;
    tQoSShapeTemplateNode *pShapeNode = NULL;
    UINT4               u4RetStatus = QOS_FAILURE;

    if (pSchedNode == NULL)
    {
        return (QOS_FAILURE);
    }

    if (QosDeleteSchedMapEntry (pSchedNode->u4GlobalId) != QOS_SUCCESS)
    {
        return QOS_FAILURE;
    }

    QosUpdateDiffServSchedulerNextFree (pSchedNode->u4GlobalId, DESTROY);
    /* 1. Remove Entry from the Scheduler Table */
    pDelSchedNode = RBTreeRem (gQoSGlobalInfo.pRbSchedTbl,
                               (tRBElem *) pSchedNode);
    if (pDelSchedNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : RBTreeRem () Failed.\r\n",
                      __FUNCTION__);

        return (QOS_FAILURE);
    }

    /* 2. Decrement the RefCount for Shape Tables. */
    if (pDelSchedNode->u4ShapeId != 0)
    {
        pShapeNode = QoSUtlGetShapeTempNode (pDelSchedNode->u4ShapeId);
        if (pShapeNode == NULL)
        {
            QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetShapeTempNode () "
                          "Returns FAILURE. \r\n", __FUNCTION__);

            return (QOS_FAILURE);
        }
        pShapeNode->u4RefCount = (pShapeNode->u4RefCount) - 1;
    }

    /* 3. Clear the Removed Entry */
    MEMSET (pDelSchedNode, 0, sizeof (tQoSSchedNode));

    /* 4. Release the Removed Entry's memory */
    u4RetStatus = MemReleaseMemBlock (gQoSGlobalInfo.QoSSchedTblMemPoolId,
                                      (UINT1 *) pDelSchedNode);
    if (u4RetStatus == MEM_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : MemReleaseMemBlock () Failed. \r\n",
                      __FUNCTION__);
        return (QOS_FAILURE);
    }

    return (QOS_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSUtlValidateSchedTblIdxInst                        */
/* Description        : This function is used to Validate the Given Index    */
/*                      instance in the Scheduler Table.                     */
/* Input(s)           : i4IfIndex        - Interface Index                   */
/*                    : u4FsQoSSchedulerId- SchedID                          */
/*                      pu4ErrorCode     - Pointer to Error Code             */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : gQoSGlobalInfo                                       */
/* Global Variables                                                          */
/* Modified           : gQoSGlobalInfo                                       */
/* Return Value(s)    : QOS_SUCCESS or QOS_FAILURE                           */
/* Called By          : SNMP Low Level                                       */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QoSUtlValidateSchedTblIdxInst (INT4 i4IfIndex, UINT4 u4FsQoSSchedulerId,
                               UINT4 *pu4ErrorCode)
{
    INT4                i4RetStatus = 0;
    tQoSSchedNode      *pSchedNode = NULL;

    if (gQoSGlobalInfo.eSysControl != QOS_SYS_CNTL_START)
    {
        QOS_TRC (MGMT_TRC, QOS_MODULE_SHUTDOW_ERROR_MSG);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (QOS_CLI_ERR_MODULE_SHUTDOW);

        return (QOS_FAILURE);
    }

    /* Check the Range of Index Values (Min-Max) */
    i4RetStatus = QoSUtlValidateIfIndex (i4IfIndex, pu4ErrorCode);
    if (i4RetStatus == QOS_FAILURE)
    {
        return (QOS_FAILURE);
    }

    /* Check the Range of Index Values (Min-Max) */
    i4RetStatus = QOS_CHECK_SCHED_TBL_INDEX_RANGE (u4FsQoSSchedulerId);
    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG3 (MGMT_TRC, "%s : Scheduler Id %d is out of Range."
                      " The Range Should be (1-%d). \r\n", __FUNCTION__,
                      u4FsQoSSchedulerId, QOS_SCHED_TBL_MAX_INDEX_RANGE);

        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (QOS_CLI_ERR_SCHED_RANGE);
        return (QOS_FAILURE);
    }

    pSchedNode = QoSUtlGetSchedNode (i4IfIndex, u4FsQoSSchedulerId);
    if (pSchedNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetSchedNode () Returns "
                      "FAILURE. \r\n", __FUNCTION__);

        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (QOS_CLI_ERR_SCHED_NO_ENTRY);
        return (QOS_FAILURE);
    }

    return (QOS_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSIsDefSchedTblEntry                                */
/* Description        : This function is used to Validate the Given Index    */
/*                      instance is the default  Scheduler Id.If not         */
/*                      present  return Failure Success otherwise.           */
/* Input(s)           : tQoSSchedNode- pSchedNode                            */
/*                                                                           */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : QOS_SUCCESS or QOS_FAILURE                           */
/* Called By          : SNMP Low Level                                       */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QoSIsDefSchedTblEntry (tQoSSchedNode * pSchedNode)
{
    if (pSchedNode != NULL)
    {
        if (pSchedNode->u4Id == 1)
        {
            if ((pSchedNode->i4IfIndex > 0) &&
                (pSchedNode->i4IfIndex <= SYS_DEF_MAX_PHYSICAL_INTERFACES))
            {
                return (QOS_SUCCESS);
            }
        }
    }

    return (QOS_FAILURE);
}

/*****************************************************************************/
/*                    METER STATS TABLE FUNCTIONS                            */
/*****************************************************************************/

/*****************************************************************************/
/* Function Name      : QoSGetNextMeterStatsTblEntryIndex                    */
/* Description        : This function is used to Give next meter Index       */
/*                      from the Meter Table                                 */
/* Input(s)           : u4FsQoSMeterId  - Current Meter Id                   */
/*                      pu4NextFsQoSMeterId - Pointer to NextMeter Id        */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : gQoSGlobalInfo                                       */
/* Global Variables                                                          */
/* Modified           : gQoSGlobalInfo                                       */
/* Return Value(s)    : QOS_SUCCESS or QOS_FAILURE                           */
/* Called By          : SNMP Low Level                                       */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QoSGetNextMeterStatsTblEntryIndex (UINT4 u4FsQoSMeterId,
                                   UINT4 *pu4NextFsQoSMeterId)
{
    tQoSMeterNode       MeterNode;
    tQoSMeterNode      *pMeterNode = NULL;

    MeterNode.u4Id = u4FsQoSMeterId;
    pMeterNode = (tQoSMeterNode *) RBTreeGetNext (gQoSGlobalInfo.pRbMeterTbl,
                                                  &MeterNode, QoSMeterCmpFun);
    if (pMeterNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : RBTreeGetNext () Failed \r\n",
                      __FUNCTION__);
        return (QOS_FAILURE);
    }

    do
    {
        /* it Should be ACTIVE */
        if (pMeterNode->u1Status == ACTIVE)
        {
            *pu4NextFsQoSMeterId = pMeterNode->u4Id;
            return (QOS_SUCCESS);
        }

        MeterNode.u4Id = pMeterNode->u4Id;
        pMeterNode = (tQoSMeterNode *)
            RBTreeGetNext (gQoSGlobalInfo.pRbMeterTbl, &MeterNode,
                           QoSMeterCmpFun);
    }
    while (pMeterNode != NULL);

    return (QOS_FAILURE);
}

/*****************************************************************************/
/*                    COSQ STATS TABLE FUNCTIONS                             */
/*****************************************************************************/
/*****************************************************************************/
/* Function Name      : QoSUtlValidateCoSQStatsTblIdxInst                    */
/* Description        : This function is used to Validate the Given Index    */
/*                      instance in the Q Table.                             */
/* Input(s)           : i4IfIndex        - interface Index                   */
/*                      u4FsQoSCoSQId    - CoS Q within the Interface        */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : gQoSGlobalInfo                                       */
/* Global Variables                                                          */
/* Modified           : gQoSGlobalInfo                                       */
/* Return Value(s)    : QOS_SUCCESS or QOS_FAILURE                           */
/* Called By          : SNMP Low Level                                       */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QoSUtlValidateCoSQStatsTblIdxInst (INT4 i4IfIndex, UINT4 u4FsQoSCoSQId)
{
    tQoSQNode          *pQNode = NULL;

    pQNode = QoSUtlGetQNode (i4IfIndex, u4FsQoSCoSQId);
    if (pQNode == NULL)
    {
        QOS_TRC_ARG3 (MGMT_TRC, "%s : The Q Id %d In the IfIndex %d. Not yet "
                      "Configured.\r\n", __FUNCTION__, u4FsQoSCoSQId,
                      i4IfIndex);
        return (QOS_FAILURE);
    }

    if (pQNode->u1Status != ACTIVE)
    {
        return (QOS_FAILURE);
    }

    return (QOS_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSGetPrefNode                                       */
/* Description        : This function is used to Get the Entry  form  the    */
/*                      DUP Table                                            */
/* Input(s)           : i4IfIndex  - Interface Index                         */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : gQoSGlobalInfo                                       */
/* Global Variables                                                          */
/* Modified           :                                                      */
/* Return Value(s)    : NULL/ Pointer to 'UINT1'                             */
/* Called By          : SNMP Low Level                                       */
/* Calling Function   :                                                      */
/*****************************************************************************/
UINT1              *
QoSGetPrefNode (INT4 i4IfIndex)
{
    UINT1              *pu1PrefVal = NULL;

    pu1PrefVal = &(gQoSGlobalInfo.au1PrefTbl[i4IfIndex - 1]);

    return (pu1PrefVal);
}

/*****************************************************************************/
/*                    DUP TABLE FUNCTIONS                                    */
/*****************************************************************************/
/*****************************************************************************/
/* Function Name      : QoSUtlGetDUPNode                                     */
/* Description        : This function is used to Get the Entry  form  the    */
/*                      DUP Table                                            */
/* Input(s)           : i4IfIndex  - Interface Index                         */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : gQoSGlobalInfo                                       */
/* Global Variables                                                          */
/* Modified           :                                                      */
/* Return Value(s)    : NULL/ Pointer to 'UINT1'                             */
/* Called By          : SNMP Low Level                                       */
/* Calling Function   :                                                      */
/*****************************************************************************/
UINT1              *
QoSUtlGetDUPNode (INT4 i4IfIndex)
{
    UINT1              *pu1PriVal = NULL;

    pu1PriVal = &(gQoSGlobalInfo.au1PDUPTbl[i4IfIndex - 1]);

    return (pu1PriVal);
}

/*****************************************************************************/
/* Function Name      : QoSGetNextUDPTblEntryIndex                           */
/* Description        : This function is used to Get the Next Entry from the */
/*                      DUP Table                                            */
/* Input(s)           : i4IfIndex       - Current ifIndex                    */
/*                      pi4NextIfIndex  - Pointer to Next ifIndex            */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : gQoSGlobalInfo                                       */
/* Global Variables                                                          */
/* Modified           : gQoSGlobalInfo                                       */
/* Return Value(s)    : QOS_SUCCESS or QOS_FAILURE                           */
/* Called By          : SNMP Low Level                                       */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QoSGetNextDUPTblEntryIndex (INT4 i4IfIndex, INT4 *pi4NextIfIndex)
{
    UINT4               u4ErrorCode = 0;
    INT4                i4NextIfIdx = 0;
    INT4                i4RetStatus = QOS_FAILURE;

    i4NextIfIdx = i4IfIndex + 1;

    i4RetStatus = QoSUtlValidateIfIndex (i4NextIfIdx, &u4ErrorCode);
    if (i4RetStatus == QOS_FAILURE)
    {
        /* Reset the error code,because this failure case will not be handled
           by the calling function because this failure is taken as sucsess
           by the calling function. */

        CLI_SET_ERR (0);

        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlValidateIfIndex () "
                      "Returns FAILURE. \r\n", __FUNCTION__);
        return (QOS_FAILURE);
    }

    *pi4NextIfIndex = i4NextIfIdx;

    return (QOS_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSGetNextCoSQStatsTblEntryIndex                     */
/* Description        : This function is used to Give next Scheduler and CoSQ*/
/*                      from the Scheduler Table                             */
/* Input(s)           : i4IfIndex         - Current ifIndex                  */
/*                      pi4NextIfIndex    - Ptr to Next ifIndex              */
/*                    : u4QId             - Current CoSQ Id                  */
/*                      pu4NextQId        - Ptr to Next CoSQ Id              */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : gQoSGlobalInfo                                       */
/* Global Variables                                                          */
/* Modified           : gQoSGlobalInfo                                       */
/* Return Value(s)    : QOS_SUCCESS or QOS_FAILURE                           */
/* Called By          : SNMP Low Level                                       */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QoSGetNextCoSQStatsTblEntryIndex (INT4 i4IfIndex, INT4 *pi4NextIfIndex,
                                  UINT4 u4QId, UINT4 *pu4NextQId)
{
    INT4                i4RetStatus = QOS_FAILURE;

    i4RetStatus = QoSGetNextQTblEntryIndex (i4IfIndex, pi4NextIfIndex,
                                            u4QId, pu4NextQId);
    if (i4RetStatus == QOS_FAILURE)
    {
        return (QOS_FAILURE);
    }

    return (QOS_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSUtlCheckQTempReference                            */
/* Description        : This function is used to check the Q Template is     */
/*                    : enabled or Not. It Return QOS_SUCCESS on             */
/*                      QTemp.u1DropAlgoEnableFlag is Enabled, otherwise     */
/*                      QOS_FAILURE                                          */
/* Input(s)           : u4QTempId   - Q Template Id                          */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : QOS_SUCCESS or QOS_FAILURE                           */
/* Called By          : SNMP                                                 */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QoSUtlCheckQTempReference (UINT4 u4QTempId)
{
    tQoSQTemplateNode  *pQTemp = NULL;

    pQTemp = QoSUtlGetQTempNode (u4QTempId);
    if (pQTemp != NULL)
    {
        if (pQTemp->u1DropAlgoEnableFlag == QOS_Q_TEMP_DROP_ALGO_ENABLE)
        {
            return (QOS_SUCCESS);
        }
    }

    return (QOS_FAILURE);
}

/*****************************************************************************/
/* Function Name      : QoSUtlCheckRDTblEntry                                */
/* Description        : This function is used to check any RED Cfg is exists */
/*                    : for a QTempId                                        */
/* Input(s)           : u4QTempId   - Q Template Id                          */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : QOS_SUCCESS or QOS_FAILURE                           */
/* Called By          : SNMP                                                 */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QoSUtlCheckRDTblEntry (UINT4 u4QTempId)
{
    tQoSRDCfgNode      *pRDCfg = NULL;
    UINT4               u4Counter = 0;

    for (u4Counter = QOS_RD_CONFG_DP_MIN_VAL;
         u4Counter <= QOS_RD_CONFG_DP_MAX_VAL; u4Counter++)
    {
        pRDCfg = QoSUtlGetRDCfgNode (u4QTempId, u4Counter);

        if (pRDCfg != NULL)
        {
            if (pRDCfg->u1Status == ACTIVE)
            {
                return (QOS_SUCCESS);
            }
        }
    }

    return (QOS_FAILURE);
}

/*****************************************************************************/
/* Function Name      : QoSUtlUpdateFilterRefCount                           */
/* Description        : This function is used to update the Ref Count in the */
/*                    : L2/L3 Filter Table Entry                             */
/* Input(s)           : u4FilterId   - L2/L3 Filter Id                       */
/*                      u4FilterType - QOS_L2FILTER / QOS_L3FILTER           */
/*                      u4Action     - QOS_INCR / QOS_DECR                   */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : QOS_SUCCESS or QOS_FAILURE                           */
/* Called By          : SNMP                                                 */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QoSUtlUpdateFilterRefCount (UINT4 u4FilterId, UINT4 u4FilterType,
                            UINT4 u4Action)
{
    UINT4               u4RetStatus = ISS_FAILURE;
    UINT4               u4IssFilterType = ISS_L3FILTER;
    UINT4               u4IssAction = ISS_DECR;

    /* Check the L2 Filter Id */
    if (u4FilterType == QOS_L2FILTER)
    {
        u4IssFilterType = ISS_L2FILTER;

    }

    if (u4FilterType == QOS_L3FILTER)
    {
        u4IssFilterType = ISS_L3FILTER;

    }

    if (u4Action == QOS_INCR)
    {
        u4IssAction = ISS_INCR;
    }

    u4RetStatus = IssUpdateFilterRefCount (u4FilterId, u4IssFilterType,
                                           u4IssAction);
    if (u4RetStatus == ISS_FAILURE)
    {
        QOS_TRC_ARG4 (MGMT_TRC, "In %s : ERROR IssUpdateFilterRefCount"
                      " () Failed, for Filter Id %d TYPE %d Action %d. \r\n",
                      __FUNCTION__, u4FilterId, u4IssFilterType, u4IssAction);
        return (QOS_FAILURE);
    }

    return (QOS_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : RbWalkAction                                         */
/* Description        : This function is used to check Gvie Name in the      */
/*                    : respective table is unique or not                    */
/* Input(s)           : e           -  tRBElem      ptr                      */
/*                    : visit       - Type of the eRBVisit                   */
/*                    : level       - RBTree Search level                    */
/*                    : arg         - Input Arg                              */
/*                    : out         - Output Arg - Return value              */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : QOS_SUCCESS or QOS_FAILURE                           */
/* Called By          : SNMP                                                 */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
RbWalkAction (tRBElem * e, eRBVisit visit, UINT4 level, void *arg, void *out)
{
    tQoSRBWalkInput    *pRbWalkInput = NULL;
    UINT1              *pu1TblEntryName = NULL;

    *(UINT4 *) out = QOS_SUCCESS;
    pRbWalkInput = (tQoSRBWalkInput *) arg;

    UNUSED_PARAM (level);

    if ((visit == leaf) || (visit == postorder))
    {
        switch (pRbWalkInput->u4TblType)
        {
            case QOS_TBL_TYPE_PRI_MAP:
                pu1TblEntryName = ((tQoSInPriorityMapNode *) e)->au1Name;
                break;

            case QOS_TBL_TYPE_CLS_MAP:
                pu1TblEntryName = ((tQoSClassMapNode *) e)->au1Name;
                break;

            case QOS_TBL_TYPE_METER:
                pu1TblEntryName = ((tQoSMeterNode *) e)->au1Name;
                break;

            case QOS_TBL_TYPE_PLY_MAP:
                pu1TblEntryName = ((tQoSPolicyMapNode *) e)->au1Name;
                break;

            default:
                return (QOS_FAILURE);
                break;
        }

        /* Check the Name  is Unique or Not */
        if ((STRLEN (pu1TblEntryName)) == (UINT4)
            pRbWalkInput->pTblEntryName->i4_Length)
        {
            if ((STRNCMP (pu1TblEntryName,
                          pRbWalkInput->pTblEntryName->pu1_OctetList,
                          pRbWalkInput->pTblEntryName->i4_Length)) == 0)
            {
                *(UINT4 *) out = QOS_FAILURE;
                return (RB_WALK_BREAK);
            }
        }

    }

    return (RB_WALK_CONT);
}

/*****************************************************************************/
/* Function Name      : QoSUtlQTempIsUniqueName                              */
/* Description        : This function is used to check Gvie Name in the      */
/*                    : QTemplate  table is unique or not                    */
/* Input(s)           : pSnmpStr    - QTemplate Entry Name                   */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : QOS_SUCCESS or QOS_FAILURE                           */
/* Called By          : SNMP                                                 */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QoSUtlQTempIsUniqueName (tSNMP_OCTET_STRING_TYPE * pSnmpStr)
{
    tQoSQTemplateNode  *pQTempNode = NULL;

    TMO_SLL_Scan (&(gQoSGlobalInfo.SllQTempTbl), pQTempNode,
                  tQoSQTemplateNode *)
    {
        /* Check the Name  is Unique or Not */
        if ((STRLEN (pQTempNode->au1Name)) == (UINT4) pSnmpStr->i4_Length)
        {
            if ((STRNCMP (pQTempNode->au1Name, pSnmpStr->pu1_OctetList,
                          pSnmpStr->i4_Length)) == 0)
            {
                return (QOS_FAILURE);
            }
        }
    }

    return (QOS_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSUtlShapeIsUniqueName                              */
/* Description        : This function is used to check Gvie Name in the      */
/*                    : Shape Temp table is unique or not                    */
/* Input(s)           : pSnmpStr    - Shape Temp Entry Name                  */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : QOS_SUCCESS or QOS_FAILURE                           */
/* Called By          : SNMP                                                 */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QoSUtlShapeIsUniqueName (tSNMP_OCTET_STRING_TYPE * pSnmpStr)
{
    tQoSShapeTemplateNode *pShapeTempNode = NULL;

    TMO_SLL_Scan (&(gQoSGlobalInfo.SllShapeTempTbl), pShapeTempNode,
                  tQoSShapeTemplateNode *)
    {
        /* Check the Name  is Unique or Not */
        if ((STRLEN (pShapeTempNode->au1Name)) == (UINT4) pSnmpStr->i4_Length)
        {
            if ((STRNCMP (pShapeTempNode->au1Name, pSnmpStr->pu1_OctetList,
                          pSnmpStr->i4_Length)) == 0)
            {
                return (QOS_FAILURE);
            }
        }
    }

    return (QOS_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSUtlIsUniqueName                                   */
/* Description        : This function is used to check Unique name in the    */
/*                    : Give Table                                           */
/* Input(s)           : pFsQoSTblEntryName  - Tbl Entry Name                 */
/*                    : u4TblType           - Tbl Type                       */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : QOS_SUCCESS or QOS_FAILURE                           */
/* Called By          : SNMP                                                 */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QoSUtlIsUniqueName (tSNMP_OCTET_STRING_TYPE * pFsQoSTblEntryName,
                    UINT4 u4TblType)
{
    tQoSRBWalkInput     RbWalkInput;
    tRBTree             pRbTree = NULL;
    UINT4               u4Out = QOS_FAILURE;
    INT4                i4ReturnValue = QOS_SUCCESS;

    RbWalkInput.pTblEntryName = pFsQoSTblEntryName;

    switch (u4TblType)
    {
        case QOS_TBL_TYPE_PRI_MAP:
            RbWalkInput.u4TblType = QOS_TBL_TYPE_PRI_MAP;
            pRbTree = gQoSGlobalInfo.pRbInPriMapTbl;
            break;

        case QOS_TBL_TYPE_CLS_MAP:
            RbWalkInput.u4TblType = QOS_TBL_TYPE_CLS_MAP;
            pRbTree = gQoSGlobalInfo.pRbClsMapTbl;
            break;

        case QOS_TBL_TYPE_METER:
            RbWalkInput.u4TblType = QOS_TBL_TYPE_METER;
            pRbTree = gQoSGlobalInfo.pRbMeterTbl;
            break;

        case QOS_TBL_TYPE_PLY_MAP:
            RbWalkInput.u4TblType = QOS_TBL_TYPE_PLY_MAP;
            pRbTree = gQoSGlobalInfo.pRbPlyMapTbl;
            break;

        default:
            i4ReturnValue = QOS_FAILURE;
            break;
    }

    RBTreeWalk (pRbTree, RbWalkAction, (void *) &RbWalkInput, (void *) &u4Out);
    if (u4Out == QOS_FAILURE)
    {
        i4ReturnValue = QOS_FAILURE;
    }

    return i4ReturnValue;
}

/*****************************************************************************/
/* Function Name      : QoSUtilTestFsQoSMeterCIR                             */
/* Description        : This function is used to CIR parameter validation    */
/*                    : Give Table                                           */
/* Input(s)           : u4FsQoSMeterId  - Meter Table Id                     */
/*                    : u4TestValFsQoSMeterCIR,u4TestValFsQoSMeterEIR        */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : QOS_SUCCESS or QOS_FAILURE                           */
/* Called By          : CLI                                                 */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QoSUtilTestFsQoSMeterCIR (UINT4 *pu4ErrorCode, UINT4 u4FsQoSMeterId,
                          UINT4 u4TestValFsQoSMeterCIR,
                          UINT4 u4TestValFsQoSMeterEIR)
{
    INT4                i4RetStatus = QOS_FAILURE;

    tQoSMeterNode      *pMeterNode = NULL;

    *pu4ErrorCode = SNMP_ERR_NO_ERROR;

    /* Check Index */
    i4RetStatus = QoSUtlValidateMeterTblIdxInst (u4FsQoSMeterId, pu4ErrorCode);
    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlValidateMeterTblIdxInst () "
                      " Returns FAILURE. \r\n", __FUNCTION__);

        return (QOS_FAILURE);
    }

    /* Reset CIR Value to '0' */
    if (u4TestValFsQoSMeterCIR == 0)
    {
        return (QOS_SUCCESS);
    }

    /* Check  Object's Value */
    if (((INT4) u4TestValFsQoSMeterCIR < QOS_METER_CIR_MIN_VAL) ||
        (u4TestValFsQoSMeterCIR > QOS_METER_CIR_MAX_VAL))
    {
        QOS_TRC_ARG3 (MGMT_TRC, "%s : Meter CIR value is out of range. The"
                      " range should be Min %d - Max %d. \r\n", __FUNCTION__,
                      QOS_METER_CIR_MIN_VAL, QOS_METER_CIR_MAX_VAL);

        CLI_SET_ERR (QOS_CLI_ERR_INVALID_CIR);

        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (QOS_FAILURE);
    }

    i4RetStatus = QoSValidateMeterTypeParams (u4FsQoSMeterId,
                                              QOS_METER_PARAM_TYPE_CIR,
                                              pu4ErrorCode);
    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSValidateMeterTypeParams () "
                      " Returns FAILURE. \r\n", __FUNCTION__);
        return (QOS_FAILURE);
    }

    pMeterNode = QoSUtlGetMeterNode (u4FsQoSMeterId);
    if (pMeterNode == NULL)
    {
        CLI_SET_ERR (QOS_CLI_ERR_METER_NO_ENTRY);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (QOS_FAILURE);
    }

    if (u4TestValFsQoSMeterEIR > 0)
    {
        if (u4TestValFsQoSMeterCIR > u4TestValFsQoSMeterEIR)
        {
            /*CIR should be less than or equal to EIR */
            CLI_SET_ERR (QOS_CLI_ERR_METER_CIR_LESS_THAN_EIR);
            return (QOS_FAILURE);
        }
    }

    return (QOS_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSUtilTestFsQoSMeterEIR                             */
/* Description        : This function is used to EIR parameter validation    */
/*                    : Give Table                                           */
/* Input(s)           : u4FsQoSMeterId  - Meter Table Id                     */
/*                    : u4TestValFsQoSMeterCIR,u4TestValFsQoSMeterEIR        */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : QOS_SUCCESS or QOS_FAILURE                           */
/* Called By          : CLI                                                 */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QoSUtilTestFsQoSMeterEIR (UINT4 *pu4ErrorCode, UINT4 u4FsQoSMeterId,
                          UINT4 u4TestValFsQoSMeterEIR,
                          UINT4 u4TestValFsQoSMeterCIR)
{
    INT4                i4RetStatus = QOS_FAILURE;
    tQoSMeterNode      *pMeterNode = NULL;

    *pu4ErrorCode = SNMP_ERR_NO_ERROR;

    /* Check Index */
    i4RetStatus = QoSUtlValidateMeterTblIdxInst (u4FsQoSMeterId, pu4ErrorCode);
    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlValidateMeterTblIdxInst () "
                      " Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    /* Reset EIR Value to '0' */
    if (u4TestValFsQoSMeterEIR == 0)
    {
        return (SNMP_SUCCESS);
    }

    /* Check  Object's Value */
    if (((INT4) u4TestValFsQoSMeterEIR < QOS_METER_EIR_MIN_VAL) ||
        (u4TestValFsQoSMeterEIR > QOS_METER_EIR_MAX_VAL))
    {
        QOS_TRC_ARG3 (MGMT_TRC, "%s : Meter EIR value is out of range. The"
                      " range should be Min %d - Max %d. \r\n", __FUNCTION__,
                      QOS_METER_EIR_MIN_VAL, QOS_METER_EIR_MAX_VAL);

        CLI_SET_ERR (QOS_CLI_ERR_INVALID_EIR);

        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;

        return (SNMP_FAILURE);
    }

    i4RetStatus = QoSValidateMeterTypeParams (u4FsQoSMeterId,
                                              QOS_METER_PARAM_TYPE_EIR,
                                              pu4ErrorCode);
    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSValidateMeterTypeParams () "
                      " Returns FAILURE. \r\n", __FUNCTION__);
        return (SNMP_FAILURE);
    }

    pMeterNode = QoSUtlGetMeterNode (u4FsQoSMeterId);

    if (pMeterNode == NULL)
    {
        CLI_SET_ERR (QOS_CLI_ERR_SHAPE_NOT_CREATED);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (SNMP_FAILURE);
    }

    if (u4TestValFsQoSMeterEIR > 0)
    {
        if (u4TestValFsQoSMeterCIR > u4TestValFsQoSMeterEIR)
        {
            CLI_SET_ERR (QOS_CLI_ERR_METER_CIR_LESS_THAN_EIR);

            /*CIR should be less than or equal to EIR */
            return (SNMP_FAILURE);
        }
    }

    return (SNMP_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSUtilTestFsQoSShapeTemplateCIR                     */
/* Description        : This function is used to CIR parameter validation    */
/*                    : Give Table                                           */
/* Input(s)           : u4FsQoSShapeTemplateId   - Shape Table Id            */
/*                    : u4TestValFsQoSShapeTemplateCIR                       */
/*                      u4TestValFsQoSShapeTemplateEIR                       */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : QOS_SUCCESS or QOS_FAILURE                           */
/* Called By          : CLI                                                 */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QoSUtilTestFsQoSShapeTemplateCIR (UINT4 *pu4ErrorCode,
                                  UINT4 u4FsQoSShapeTemplateId,
                                  UINT4 u4TestValFsQoSShapeTemplateCIR,
                                  UINT4 u4TestValFsQoSShapeTemplateEIR)
{
    INT4                i4RetStatus = QOS_FAILURE;
    tQoSShapeTemplateNode *pShapeTempNode = NULL;

    *pu4ErrorCode = SNMP_ERR_NO_ERROR;

    /* Check Index */
    i4RetStatus = QoSUtlValidateShapeTempTblIdxInst (u4FsQoSShapeTemplateId,
                                                     pu4ErrorCode);
    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlValidateShapeTempTblIdxInst ()"
                      " Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    /* Check  Object's Value */
    if ((u4TestValFsQoSShapeTemplateCIR < QOS_SHAPE_CIR_MIN_VAL) ||
        (u4TestValFsQoSShapeTemplateCIR > QOS_SHAPE_CIR_MAX_VAL))
    {
        QOS_TRC_ARG3 (MGMT_TRC, "%s : Shape CIR value is out of range. The"
                      " range should be Min %d - Max %d. \r\n", __FUNCTION__,
                      QOS_SHAPE_CIR_MIN_VAL, QOS_SHAPE_CIR_MAX_VAL);
        CLI_SET_ERR (QOS_CLI_ERR_INVALID_CIR);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;

        return (SNMP_FAILURE);
    }
    pShapeTempNode = QoSUtlGetShapeTempNode (u4FsQoSShapeTemplateId);
    if (pShapeTempNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetShapeTempNode () "
                      "Returns FAILURE. \r\n", __FUNCTION__);

        *pu4ErrorCode = SNMP_ERR_NO_CREATION;

        return (QOS_FAILURE);
    }
#ifdef NPAPI_WANTED
    /*check hardware capability */
    if ((ISS_HW_SUPPORTED ==
         IssGetHwCapabilities (ISS_HW_SHAPE_PARAM_EIR_SUPPORT)))
    {
#endif

        if ((INT4) u4TestValFsQoSShapeTemplateEIR >= 0)
        {
            if (u4TestValFsQoSShapeTemplateCIR > u4TestValFsQoSShapeTemplateEIR)
            {
                CLI_SET_ERR (QOS_CLI_ERR_METER_CIR_LESS_THAN_EIR);

                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                /*CIR should be less than or equal to EIR */
                return (SNMP_FAILURE);
            }
        }
        else
        {
            if (u4TestValFsQoSShapeTemplateCIR > pShapeTempNode->u4EIR)

            {
                CLI_SET_ERR (QOS_CLI_ERR_METER_CIR_LESS_THAN_EIR);

                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                /*CIR should be less than or equal to EIR */
                return (SNMP_FAILURE);
            }
        }

#ifdef NPAPI_WANTED
    }
#endif

    return (SNMP_SUCCESS);

}

/*****************************************************************************/
/* Function Name      : QoSUtilTestFsQoSShapeTemplateEIR                     */
/* Description        : This function is used to EIR parameter validation    */
/*                    : Give Table                                           */
/* Input(s)           : u4FsQoSShapeTemplateId   - Shape Table Id            */
/*                    : u4TestValFsQoSShapeTemplateCIR                       */
/*                      u4TestValFsQoSShapeTemplateEIR                       */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : QOS_SUCCESS or QOS_FAILURE                           */
/* Called By          : CLI                                                 */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QoSUtilTestFsQoSShapeTemplateEIR (UINT4 *pu4ErrorCode,
                                  UINT4 u4FsQoSShapeTemplateId,
                                  UINT4 u4TestValFsQoSShapeTemplateEIR,
                                  UINT4 u4TestValFsQoSShapeTemplateCIR)
{
    INT4                i4RetStatus = QOS_FAILURE;
    tQoSShapeTemplateNode *pShapeTempNode = NULL;

    *pu4ErrorCode = SNMP_ERR_NO_ERROR;

#ifdef NPAPI_WANTED
    /*check hardware capability */
    if (ISS_HW_SUPPORTED !=
        IssGetHwCapabilities (ISS_HW_SHAPE_PARAM_EIR_SUPPORT))
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : IssGetHwCapabilities ()"
                      "Returns SUCCESS. EIR is not supported in platform. \r\n",
                      __FUNCTION__);
        CLI_SET_ERR (QOS_CLI_ERR_SHAPE_TEMP_EIR_SUPPORT);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;

        return SNMP_FAILURE;
    }
#endif

    /* Check Index */
    i4RetStatus = QoSUtlValidateShapeTempTblIdxInst (u4FsQoSShapeTemplateId,
                                                     pu4ErrorCode);
    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlValidateShapeTempTblIdxInst ()"
                      " Returns FAILURE. \r\n", __FUNCTION__);

        return (SNMP_FAILURE);
    }

    /* Check  Object's Value */
    if (u4TestValFsQoSShapeTemplateEIR > QOS_SHAPE_EIR_MAX_VAL)
    {
        QOS_TRC_ARG3 (MGMT_TRC, "%s : Shape EIR value is out of range. The"
                      " range should be Min %d - Max %d. \r\n", __FUNCTION__,
                      QOS_SHAPE_EIR_MIN_VAL, QOS_SHAPE_EIR_MAX_VAL);

        CLI_SET_ERR (QOS_CLI_ERR_INVALID_EIR);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }
    pShapeTempNode = QoSUtlGetShapeTempNode (u4FsQoSShapeTemplateId);
    if (pShapeTempNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetShapeTempNode () "
                      "Returns FAILURE. \r\n", __FUNCTION__);
        CLI_SET_ERR (QOS_CLI_ERR_SHAPE_NOT_CREATED);

        *pu4ErrorCode = SNMP_ERR_NO_CREATION;

        return (QOS_FAILURE);
    }
    if (u4TestValFsQoSShapeTemplateEIR > 0)
    {
        if (u4TestValFsQoSShapeTemplateCIR > u4TestValFsQoSShapeTemplateEIR)
        {
            CLI_SET_ERR (QOS_CLI_ERR_METER_CIR_LESS_THAN_EIR);

            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            /*CIR should be less than or equal to EIR */
            return (SNMP_FAILURE);
        }
    }

    return (SNMP_SUCCESS);
}

/*******************************/

/*****************************************************************************/
/*                     DATA PATH ENTRY TABLE FUNCTIONS                       */
/*****************************************************************************/

/*****************************************************************************/
/* Function Name      : QoSDataPathEntryCmpFun                               */
/* Description        : This function is used as a Compare function in the   */
/*                      RBTree.                                              */
/* Input(s)           : e1 , e2 -  Elements of RBTree                        */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    :  -1/0/1                                              */
/* Called By          : RBTreeUtility                                        */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QoSDataPathEntryCmpFun (tRBElem * e1, tRBElem * e2)
{
    tQosStdDataPathEntry *pDataPathEntry1 = NULL;
    tQosStdDataPathEntry *pDataPathEntry2 = NULL;

    pDataPathEntry1 = (tQosStdDataPathEntry *) e1;
    pDataPathEntry2 = (tQosStdDataPathEntry *) e2;

    /* Compare the Id */

    if (pDataPathEntry1->i4IfIndex < pDataPathEntry2->i4IfIndex)
    {
        return (-1);
    }
    else if (pDataPathEntry1->i4IfIndex > pDataPathEntry2->i4IfIndex)
    {
        return (1);
    }
    if (pDataPathEntry1->i4DSDataPathIfDir < pDataPathEntry2->i4DSDataPathIfDir)
    {
        return (-1);
    }
    else if ((pDataPathEntry1->i4DSDataPathIfDir) >
             (pDataPathEntry2->i4DSDataPathIfDir))
    {
        return (1);
    }
    return (0);
}

/*****************************************************************************/
/* Function Name      : QoSUtlGetDataPathEntry                               */
/* Description        : This function is used to Get the Entry  form  the    */
/*                      Clfr Element Table ,it will do the following Action  */
/*                      1. Call RBTreeGet Utility.                           */
/* Input(s)           : i4IfIndex,i4DSDataPathIfDir                          */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : gQoSGlobalInfo                                       */
/* Global Variables                                                          */
/* Modified           :                                                      */
/* Return Value(s)    : NULL/ Pointer to 'tQosStdDataPathEntry               */
/* Called By          : SNMP Low Level                                       */
/* Calling Function   :                                                      */
/*****************************************************************************/
tQosStdDataPathEntry *
QoSUtlGetDataPathEntry (UINT4 i4IfIndex, UINT4 i4DSDataPathIfDir)
{
    tQosStdDataPathEntry TempDataPathEntryNode;
    tQosStdDataPathEntry *pDataPathEntryNode = NULL;

    MEMSET (&TempDataPathEntryNode, 0, sizeof (tQosStdDataPathEntry));
    TempDataPathEntryNode.i4IfIndex = i4IfIndex;
    TempDataPathEntryNode.i4DSDataPathIfDir = i4DSDataPathIfDir;

    pDataPathEntryNode = (tQosStdDataPathEntry *)
        RBTreeGet (gQoSGlobalInfo.DsDataPathTbl,
                   (tRBElem *) & TempDataPathEntryNode);

    if (pDataPathEntryNode == NULL)
    {
        QOS_TRC_ARG3 (MGMT_TRC,
                      "%s : Data Path Entry (Port) Id %d  and Direction"
                      "Id (Ingress/Egress) %d is not Found in the "
                      "Classifier Element Table. \r\n", __FUNCTION__, i4IfIndex,
                      i4DSDataPathIfDir);
    }

    return (pDataPathEntryNode);

}

/*****************************************************************************/
/* Function Name      : QoSCreateDataPathEntry                               */
/* Description        : This function is used to Create an Entry in the      */
/*                      Data Path Entry Table ,it will do the following      */
/*                      Action                                               */
/*                      1. Allocate a memory block for the New Entry         */
/*                      2. Initialize the New Entry                          */
/* Input(s)           : i4IfIndex,i4DSDataPathIfDir                          */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : gQoSGlobalInfo                                       */
/* Global Variables                                                          */
/* Modified           : gQoSGlobalInfo                                       */
/* Return Value(s)    :                                                      */
/* Called By          : SNMP Low Level                                       */
/* Calling Function   :                                                      */
/*****************************************************************************/
tQosStdDataPathEntry *
QoSCreateDataPathEntry (UINT4 i4Index, UINT4 i4IfDir)
{
    UINT4               u4RetStatus = QOS_FAILURE;
    tQosStdDataPathEntry *pNewDataPathEntryNode = NULL;

    /* 1. Allocate a memory block for the New Entry */
    pNewDataPathEntryNode = (tQosStdDataPathEntry *)
        MemAllocMemBlk (gQoSGlobalInfo.DsDataPathPoolId);
    if (pNewDataPathEntryNode == NULL)
    {
        QOS_TRC_ARG3 (MGMT_TRC, "In %s : MemAllocMemBlk () Failed, "
                      "for Data Path Entry (Port) Id %d and for Direction Id"
                      "%d\r\n", __FUNCTION__, i4Index, i4IfDir);

        return (NULL);
    }
    MEMSET (pNewDataPathEntryNode, 0, sizeof (tQosStdDataPathEntry));

    pNewDataPathEntryNode->i4IfIndex = i4Index;
    pNewDataPathEntryNode->i4DSDataPathIfDir = i4IfDir;
    pNewDataPathEntryNode->i4StorageType = QOS_STORAGE_NONVOLATILE;
    pNewDataPathEntryNode->u1DataPathStatus = QOS_NOT_READY;

    /* 3. Add this New Entry into the Classfier  Ma  Table */
    u4RetStatus = RBTreeAdd (gQoSGlobalInfo.DsDataPathTbl,
                             (tRBElem *) pNewDataPathEntryNode);
    if (u4RetStatus != RB_SUCCESS)
    {
        u4RetStatus = MemReleaseMemBlock (gQoSGlobalInfo.DsDataPathPoolId,
                                          (UINT1 *) pNewDataPathEntryNode);
        if (u4RetStatus == MEM_FAILURE)
        {
            QOS_TRC_ARG3 (MGMT_TRC, "In %s : MemReleaseMemBlock () Failed ,"
                          "for Data Path (Port) Id  %d and for Direction Id %d. \r\n",
                          __FUNCTION__, i4Index, i4IfDir);
            return (NULL);
        }
        QOS_TRC_ARG3 (MGMT_TRC, "In %s : MemReleaseMemBlock () Failed ,"
                      "for Data Path (Port) Id  %d and for Direction Id %d. \r\n",
                      __FUNCTION__, i4Index, i4IfDir);

        return (NULL);
    }

    return (pNewDataPathEntryNode);
}

/*****************************************************************************/
/* Function Name      : QoSValidateDataPathEntry                             */
/* Description        : This function is used to Check the Mandatory Fields  */
/*                      of Data Path Table's Entry, it will do the           */
/*                      following Action                                     */
/*                      1. IfIndex Id should not be NULL (0).                */
/*                      2. Direction ID not ot be NULL  (0).                 */
/* Input(s)           : pDataPathEntryNode                                   */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : gQoSGlobalInfo                                       */
/* Global Variables                                                          */
/* Modified           : gQoSGlobalInfo                                       */
/* Return Value(s)    : QOS_SUCCESS or QOS_FAILURE                           */
/* Called By          : SNMP Low Level                                       */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QoSValidateDataPathEntry (tQosStdDataPathEntry * pDataPathEntryNode)
{
    /*Check Mandatory Fields */
    if (pDataPathEntryNode == NULL)
    {
        return (QOS_FAILURE);
    }

    if ((pDataPathEntryNode->i4IfIndex == QOS_START_INITIAL_INDEX) &&
        (pDataPathEntryNode->i4DSDataPathIfDir) == QOS_START_INITIAL_INDEX)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "%s : Id Should not be 0.\r\n", __FUNCTION__);
        pDataPathEntryNode->u1DataPathStatus = QOS_NOT_READY;
        return (QOS_FAILURE);
    }

    pDataPathEntryNode->u1DataPathStatus = QOS_NOT_IN_SERVICE;

    return (QOS_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSDeleteDataPathEntry                               */
/* Description        : This function is used to Delete an Entry from the    */
/*                      Data Path Entry Table ,it will do the following      */
/*                      Action                                               */
/*                      1. Remove the Entry from the Data Path Entry Table   */
/*                      3. Clear the Removed Entry                           */
/*                      4. Release the Removed Entry's memory                */
/* Input(s)           : pDataPathEntryNode                                   */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : gQoSGlobalInfo                                       */
/* Global Variables                                                          */
/* Modified           : gQoSGlobalInfo                                       */
/* Return Value(s)    : QOS_SUCCESS or QOS_FAILURE                           */
/* Called By          : SNMP Low Level                                       */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QoSDeleteDataPathEntry (tQosStdDataPathEntry * pDataPathEntryNode)
{
    UINT4               u4RetStatus = QOS_FAILURE;
    tQosStdDataPathEntry *pDelDataPathEntryNode = NULL;

    /* 1. Remove Entry from the Class Map Table */
    pDelDataPathEntryNode = RBTreeRem (gQoSGlobalInfo.DsDataPathTbl,
                                       (tRBElem *) pDataPathEntryNode);
    if (pDelDataPathEntryNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : RBTreeRem () Failed.\r\n",
                      __FUNCTION__);

        return (QOS_FAILURE);
    }

    /* 2. Clear the Removed Entry */
    MEMSET (pDelDataPathEntryNode, 0, sizeof (tQosStdDataPathEntry));

    /* 3. Release the Removed Entry's memory */
    u4RetStatus = MemReleaseMemBlock (gQoSGlobalInfo.DsDataPathPoolId,
                                      (UINT1 *) pDelDataPathEntryNode);
    if (u4RetStatus == MEM_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : MemReleaseMemBlock () Failed.\r\n",
                      __FUNCTION__);

        return (QOS_FAILURE);
    }

    return (QOS_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSGetNextValidDataPathIndex                         */
/* Description        : This function is used to Get the Next Entry from the */
/*                      Classfier Element Table                              */
/* Input(s)           : u4CurrentIndex  - Current index id                   */
/*                      pu4NextIndex    - Pointer to NextIndex Id            */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : gQoSGlobalInfo                                       */
/* Global Variables                                                          */
/* Modified           : gQoSGlobalInfo                                       */
/* Return Value(s)    : QOS_SUCCESS or QOS_FAILURE                           */
/* Called By          : SNMP Low Level                                       */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QoSGetNextValidDataPathIndex (INT4 i4IfIndex, INT4 *pi4NextIfIndex,
                              INT4 i4DataPathIfDir, INT4 *pi4NextDataPathIfDir)
{
    tQosStdDataPathEntry *pDataPathEntry = NULL;
    tQosStdDataPathEntry DataPathEntry;

    if ((i4IfIndex == QOS_START_INITIAL_INDEX)
        && (i4DataPathIfDir == QOS_START_INITIAL_INDEX))
    {
        pDataPathEntry = (tQosStdDataPathEntry *)
            RBTreeGetFirst (gQoSGlobalInfo.DsDataPathTbl);
    }
    else
    {
        DataPathEntry.i4IfIndex = i4IfIndex;
        DataPathEntry.i4DSDataPathIfDir = i4DataPathIfDir;
        pDataPathEntry = (tQosStdDataPathEntry *)
            RBTreeGetNext (gQoSGlobalInfo.DsDataPathTbl,
                           &DataPathEntry, QoSDataPathEntryCmpFun);
    }

    if (pDataPathEntry == NULL)
    {
        QOS_TRC_ARG3 (MGMT_TRC, "In %s : RBTreeGetNext () Failed, "
                      " for Data Path (Port) Id %d and Direction Id %d.\r\n",
                      __FUNCTION__, i4IfIndex, i4DataPathIfDir);

        return QOS_FAILURE;
    }

    *pi4NextIfIndex = pDataPathEntry->i4IfIndex;
    *pi4NextDataPathIfDir = pDataPathEntry->i4DSDataPathIfDir;
    return QOS_SUCCESS;

}

/*****************************************************************************/
/*                     CLASSIFIER TABLE FUNCTIONS                            */
/*****************************************************************************/

/*****************************************************************************/
/* Function Name      : QoSClfrCmpFun                                        */
/* Description        : This function is used as a Compare function in the   */
/*                      RBTree.                                              */
/* Input(s)           : e1 , e2 -  Elements of RBTree                        */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    :  -1/0/1                                              */
/* Called By          : RBTreeUtility                                        */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QoSClfrCmpFun (tRBElem * e1, tRBElem * e2)
{
    tQosStdClfrEntry   *pClfrEntry1 = NULL;
    tQosStdClfrEntry   *pClfrEntry2 = NULL;

    pClfrEntry1 = (tQosStdClfrEntry *) e1;
    pClfrEntry2 = (tQosStdClfrEntry *) e2;

    /* Compare the Id */

    if (pClfrEntry1->u4DsClfrId < pClfrEntry2->u4DsClfrId)
    {
        return (-1);
    }
    else if (pClfrEntry1->u4DsClfrId > pClfrEntry2->u4DsClfrId)
    {
        return (1);
    }
    return (0);
}

/*****************************************************************************/
/* Function Name      : QoSClfrRBTFreeNodeFn                                 */
/* Description        : This function is used to Delete the  Classifier      */
/*                      Entry                                                */
/*                      Entries and Release the Memory to the respective     */
/*                      Mem Pool .                                           */
/* Input(s)           : tRBElem     - Pointer to Clfr Element Table Entry    */
/*                      u4Arg       - Unused Param                           */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : QOS_SUCCESS /QOS_FAILURE                             */
/* Called By          : RBTreeUtility                                        */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QoSClfrRBTFreeNodeFn (tRBElem * pRBElem, UINT4 u4Arg)
{
    UINT4               u4RetStatus = MEM_FAILURE;

    UNUSED_PARAM (u4Arg);

    if (pRBElem == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : Clfr Entry is NULL.\r\n",
                      __FUNCTION__);

        return (QOS_FAILURE);
    }

    MEMSET (pRBElem, 0, sizeof (tQosStdClfrEntry));

    /* Release the Mem Block */
    u4RetStatus = MemReleaseMemBlock (gQoSGlobalInfo.DsClfrPoolId,
                                      (UINT1 *) pRBElem);

    if (u4RetStatus != MEM_SUCCESS)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : Clfr - "
                      "MemReleaseMemBlock () Failed.\r\n", __FUNCTION__);
    }

    return (QOS_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSUtlGetClfrEntry                                   */
/* Description        : This function is used to Get the Entry  form  the    */
/*                      Clfr Table ,it will do the following Action          */
/*                      1. Call RBTreeGet Utility.                           */
/* Input(s)           : u4DServClfrId                                        */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : gQoSGlobalInfo                                       */
/* Global Variables                                                          */
/* Modified           :                                                      */
/* Return Value(s)    : NULL/ Pointer to 'tQosStdClfrEntry                   */
/* Called By          : SNMP Low Level                                       */
/* Calling Function   :                                                      */
/*****************************************************************************/
tQosStdClfrEntry   *
QoSUtlGetClfrEntry (UINT4 u4DServClfrId)
{
    tQosStdClfrEntry    TempClfrNode;
    tQosStdClfrEntry   *pClfrNode = NULL;

    MEMSET (&TempClfrNode, 0, sizeof (tQosStdClfrEntry));
    TempClfrNode.u4DsClfrId = u4DServClfrId;

    pClfrNode = (tQosStdClfrEntry *)
        RBTreeGet (gQoSGlobalInfo.DsClfrTbl, (tRBElem *) & TempClfrNode);

    if (pClfrNode == NULL)
    {
        QOS_TRC_ARG2 (MGMT_TRC, "%s : Classifier Id %d "
                      " is not Found in the "
                      "Classifier Table. \r\n", __FUNCTION__, u4DServClfrId);

    }

    return (pClfrNode);

}

/*****************************************************************************/
/* Function Name      : QoSCreateClfrEntry                                   */
/* Description        : This function is used to Create an Entry in the      */
/*                      Classifier Table ,it will do the following           */
/*                      Action                                               */
/*                      1. Allocate a memory block for the New Entry         */
/*                      2. Initialize the New Entry                          */
/*                      3. Add this New Entry into the Class Map Table       */
/* Input(s)           : u4ClsMapId      - Index to ClsMapNode                */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : gQoSGlobalInfo                                       */
/* Global Variables                                                          */
/* Modified           : gQoSGlobalInfo                                       */
/* Return Value(s)    : NULL / Pointer to  ClsMapNode                        */
/* Called By          : SNMP Low Level                                       */
/* Calling Function   :                                                      */
/*****************************************************************************/
tQosStdClfrEntry   *
QoSCreateClfrEntry (UINT4 u4DServClfrId)
{
    UINT4               u4RetStatus = QOS_FAILURE;
    tQosStdClfrEntry   *pNewClfrNode = NULL;

    /* 1. Allocate a memory block for the New Entry */
    pNewClfrNode = (tQosStdClfrEntry *)
        MemAllocMemBlk (gQoSGlobalInfo.DsClfrPoolId);
    if (pNewClfrNode == NULL)
    {
        QOS_TRC_ARG2 (MGMT_TRC, "In %s : MemAllocMemBlk () Failed, "
                      "for Clfr Id  %d . \r\n", __FUNCTION__, u4DServClfrId);

        return (NULL);
    }

    pNewClfrNode->u4DsClfrId = u4DServClfrId;
    pNewClfrNode->i4StorageType = QOS_STORAGE_NONVOLATILE;

    /* 3. Add this New Entry into the Classfier  Ma  Table */
    u4RetStatus = RBTreeAdd (gQoSGlobalInfo.DsClfrTbl,
                             (tRBElem *) pNewClfrNode);
    if (u4RetStatus == RB_FAILURE)
    {
        /* do nothing - this check is for avoiding coverity warning
         * u4Getval parameter will be used for our requirement.
         */
    }
    if (u4RetStatus != RB_SUCCESS)
    {
        u4RetStatus = MemReleaseMemBlock (gQoSGlobalInfo.DsClfrPoolId,
                                          (UINT1 *) pNewClfrNode);
        if (u4RetStatus == MEM_FAILURE)
        {
            QOS_TRC_ARG2 (MGMT_TRC, "In %s : MemReleaseMemBlock () Failed ,"
                          "for Clfr Id  %d  \r\n", __FUNCTION__, u4DServClfrId);
            return (NULL);
        }

        QOS_TRC_ARG2 (MGMT_TRC, "In %s : RBTreeAdd () Failed ,"
                      "for Clfr Id  %d \r\n", __FUNCTION__, u4DServClfrId);
        return (NULL);
    }
    pNewClfrNode->u1DsClfrStatus = QOS_NOT_IN_SERVICE;

    return (pNewClfrNode);
}

/*****************************************************************************/
/* Function Name      : QoSValidateClfrEntry                                 */
/* Description        : This function is used to Check the Mandatory Fields  */
/*                      of Classfier Table's Entry, it will do the           */
/*                      following Action                                     */
/*                      1. Classfier Id should not be NULL (0).              */
/* Input(s)           : pClfrNode                                            */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : gQoSGlobalInfo                                       */
/* Global Variables                                                          */
/* Modified           : gQoSGlobalInfo                                       */
/* Return Value(s)    : QOS_SUCCESS or QOS_FAILURE                           */
/* Called By          : SNMP Low Level                                       */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QoSValidateClfrEntry (tQosStdClfrEntry * pClfrNode)
{
    /*Check Mandatory Fields */
    if (pClfrNode == NULL)
    {
        return (QOS_FAILURE);
    }

    if ((pClfrNode->u4DsClfrId == QOS_START_INITIAL_INDEX))
    {
        QOS_TRC_ARG1 (MGMT_TRC, "%s : CLASSFIER  Should not be 0.\r\n",
                      __FUNCTION__);
        pClfrNode->u1DsClfrStatus = QOS_NOT_READY;
        return (QOS_FAILURE);
    }

    pClfrNode->u4DsClfrId = QOS_NOT_IN_SERVICE;

    return (QOS_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSDeleteClfrEntry                                   */
/* Description        : This function is used to Delete an Entry from the    */
/*                      Classsfier Table ,it will do the following           */
/*                      Action                                               */
/*                      1. Remove the Entry from the Classfier      Table    */
/*                      3. Clear the Removed Entry                           */
/*                      4. Release the Removed Entry's memory                */
/* Input(s)           : pClfrNode                                            */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : gQoSGlobalInfo                                       */
/* Global Variables                                                          */
/* Modified           : gQoSGlobalInfo                                       */
/* Return Value(s)    : QOS_SUCCESS or QOS_FAILURE                           */
/* Called By          : SNMP Low Level                                       */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QoSDeleteClfrEntry (tQosStdClfrEntry * pClfrNode)
{
    UINT4               u4RetStatus = QOS_FAILURE;
    tQosStdClfrEntry   *pDelClfrNode = NULL;

    /* 1. Remove Entry from the Class Map Table */
    pDelClfrNode = RBTreeRem (gQoSGlobalInfo.DsClfrTbl, (tRBElem *) pClfrNode);
    if (pDelClfrNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : RBTreeRem () Failed.\r\n",
                      __FUNCTION__);

        return (QOS_FAILURE);
    }

    /* 2. Clear the Removed Entry */
    MEMSET (pDelClfrNode, 0, sizeof (tQosStdClfrEntry));

    /* 3. Release the Removed Entry's memory */
    u4RetStatus = MemReleaseMemBlock (gQoSGlobalInfo.DsClfrPoolId,
                                      (UINT1 *) pDelClfrNode);
    if (u4RetStatus == MEM_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : MemReleaseMemBlock () Failed.\r\n",
                      __FUNCTION__);

        return (QOS_FAILURE);
    }

    return (QOS_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSGetNextClfrEntryValidateIndex                    */
/* Description        : This function is used to Get the Next Entry from the */
/*                      Classfier Table                                      */
/* Input(s)           : u4CurrentIndex  - Current index id                   */
/*                      pu4NextIndex    - Pointer to NextIndex Id            */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : gQoSGlobalInfo                                       */
/* Global Variables                                                          */
/* Modified           : gQoSGlobalInfo                                       */
/* Return Value(s)    : QOS_SUCCESS or QOS_FAILURE                           */
/* Called By          : SNMP Low Level                                       */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QoSGetNextClfrEntryValidateIndex (UINT4 u4DiffServClfrId,
                                  UINT4 *pu4NextDiffServClfrId)
{
    tQosStdClfrEntry   *pClfrEntry = NULL;
    tQosStdClfrEntry    ClfrEntry;

    if (u4DiffServClfrId == QOS_START_INITIAL_INDEX)
    {
        pClfrEntry = (tQosStdClfrEntry *)
            RBTreeGetFirst (gQoSGlobalInfo.DsClfrTbl);
    }
    else
    {
        ClfrEntry.u4DsClfrId = u4DiffServClfrId;
        pClfrEntry = (tQosStdClfrEntry *)
            RBTreeGetNext (gQoSGlobalInfo.DsClfrTbl, &ClfrEntry, QoSClfrCmpFun);
    }

    if (pClfrEntry == NULL)
    {
        QOS_TRC_ARG2 (MGMT_TRC, "In %s : RBTreeGetNext () Failed, "
                      " for Classifier Id %d.\r\n", __FUNCTION__,
                      u4DiffServClfrId);

        return QOS_FAILURE;
    }

    *pu4NextDiffServClfrId = pClfrEntry->u4DsClfrId;
    return QOS_SUCCESS;

}

/*****************************************************************************/
/*                     CLASSIFIER ELEMENT TABLE FUNCTIONS                    */
/*****************************************************************************/

/*****************************************************************************/
/* Function Name      : QoSClfrElementCmpFun                                 */
/* Description        : This function is used as a Compare function in the   */
/*                      RBTree.                                              */
/* Input(s)           : e1 , e2 -  Elements of RBTree                        */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    :  -1/0/1                                              */
/* Called By          : RBTreeUtility                                        */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QoSClfrElementCmpFun (tRBElem * e1, tRBElem * e2)
{
    tQosStdClfrElementEntry *pClfrElementEntry1 = NULL;
    tQosStdClfrElementEntry *pClfrElementEntry2 = NULL;

    pClfrElementEntry1 = (tQosStdClfrElementEntry *) e1;
    pClfrElementEntry2 = (tQosStdClfrElementEntry *) e2;

    /* Compare the Id */

    if (pClfrElementEntry1->u4DServClfrId < pClfrElementEntry2->u4DServClfrId)
    {
        return (-1);
    }
    else if (pClfrElementEntry1->u4DServClfrId >
             pClfrElementEntry2->u4DServClfrId)
    {
        return (1);
    }
    if (pClfrElementEntry1->u4DServClfrElementId <
        pClfrElementEntry2->u4DServClfrElementId)
    {
        return (-1);
    }
    else if (pClfrElementEntry1->u4DServClfrElementId >
             pClfrElementEntry2->u4DServClfrElementId)
    {
        return (1);
    }
    return (0);
}

/*****************************************************************************/
/* Function Name      : QoSUtlGetClfrElementEntry                            */
/* Description        : This function is used to Get the Entry  form  the    */
/*                      Clfr Element Table ,it will do the following Action  */
/*                      1. Call RBTreeGet Utility.                           */
/* Input(s)           : u4DServClfrId,u4DServClfrElementId                  */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : gQoSGlobalInfo                                       */
/* Global Variables                                                          */
/* Modified           :                                                      */
/* Return Value(s)    : NULL/ Pointer to 'tQosStdClfrElementEntry            */
/* Called By          : SNMP Low Level                                       */
/* Calling Function   :                                                      */
/*****************************************************************************/
tQosStdClfrElementEntry *
QoSUtlGetClfrElementEntry (UINT4 u4DServClfrId, UINT4 u4DServClfrElementId)
{
    tQosStdClfrElementEntry TempClfrElementNode;
    tQosStdClfrElementEntry *pClfrElementNode = NULL;

    MEMSET (&TempClfrElementNode, 0, sizeof (tQosStdClfrElementEntry));
    TempClfrElementNode.u4DServClfrId = u4DServClfrId;
    TempClfrElementNode.u4DServClfrElementId = u4DServClfrElementId;

    pClfrElementNode = (tQosStdClfrElementEntry *)
        RBTreeGet (gQoSGlobalInfo.DsClfrElementTbl,
                   (tRBElem *) & TempClfrElementNode);

    if (pClfrElementNode == NULL)
    {
        QOS_TRC_ARG3 (MGMT_TRC, "%s : Classifier Id %d  and Classfier"
                      "Element Id %d is not Found in the "
                      "Classifier Element Table. \r\n", __FUNCTION__,
                      u4DServClfrId, u4DServClfrElementId);
    }

    return (pClfrElementNode);

}

/*****************************************************************************/
/* Function Name      : QoSCreateClfrElementEntry                            */
/* Description        : This function is used to Create an Entry in the      */
/*                      Classifier Element Table ,it will do the following   */
/*                      Action                                               */
/*                      1. Allocate a memory block for the New Entry         */
/*                      2. Initialize the New Entry                          */
/* Input(s)           : u4DServClfrId,u4DServClfrElementId                   */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : gQoSGlobalInfo                                       */
/* Global Variables                                                          */
/* Modified           : gQoSGlobalInfo                                       */
/* Return Value(s)    : NULL / Pointer to  ClsMapNode                        */
/* Called By          : SNMP Low Level                                       */
/* Calling Function   :                                                      */
/*****************************************************************************/
tQosStdClfrElementEntry *
QoSCreateClfrElementEntry (UINT4 u4DServClfrId, UINT4 u4DServClfrElementId)
{
    UINT4               u4RetStatus = QOS_FAILURE;
    tQosStdClfrElementEntry *pNewClfrElementNode = NULL;

    /* 1. Allocate a memory block for the New Entry */
    pNewClfrElementNode = (tQosStdClfrElementEntry *)
        MemAllocMemBlk (gQoSGlobalInfo.DsClfrElementPoolId);

    if (pNewClfrElementNode == NULL)
    {
        QOS_TRC_ARG3 (MGMT_TRC, "In %s : MemAllocMemBlk () Failed, "
                      "for Clfr Id  %d and for Clfr Element Id %d. \r\n",
                      __FUNCTION__, u4DServClfrId, u4DServClfrElementId);

        return (NULL);
    }
    /* 2. Initialize the New Entry */
    MEMSET (pNewClfrElementNode, 0, sizeof (tQosStdClfrElementEntry));

    pNewClfrElementNode->u4DServClfrId = u4DServClfrId;
    pNewClfrElementNode->u4DServClfrElementId = u4DServClfrElementId;
    pNewClfrElementNode->i4StorageType = QOS_STORAGE_NONVOLATILE;
    pNewClfrElementNode->u1DSClfrElementStatus = QOS_NOT_READY;

    /* 3. Add this New Entry into the Classfier  Ma  Table */
    u4RetStatus = RBTreeAdd (gQoSGlobalInfo.DsClfrElementTbl,
                             (tRBElem *) pNewClfrElementNode);
    if (u4RetStatus != RB_SUCCESS)
    {
        u4RetStatus = MemReleaseMemBlock (gQoSGlobalInfo.DsClfrElementPoolId,
                                          (UINT1 *) pNewClfrElementNode);
        if (u4RetStatus == MEM_FAILURE)
        {
            QOS_TRC_ARG3 (MGMT_TRC, "In %s : MemReleaseMemBlock () Failed ,"
                          "for Clfr Id  %d and for Clfr Element Id %d. \r\n",
                          __FUNCTION__, u4DServClfrId, u4DServClfrElementId);
            return (NULL);
        }

        QOS_TRC_ARG3 (MGMT_TRC, "In %s : RBTreeAdd () Failed ,"
                      "for Clfr Id  %d and for Clfr Element Id %d. \r\n",
                      __FUNCTION__, u4DServClfrId, u4DServClfrElementId);
        return (NULL);
    }
    return (pNewClfrElementNode);
}

/*****************************************************************************/
/* Function Name      : QoSValidateClfrElementEntry                          */
/* Description        : This function is used to Check the Mandatory Fields  */
/*                      of Classfier Element Table's Entry, it will do the   */
/*                      following Action                                     */
/*                      1. Classfier Id should not be NULL (0).              */
/*                      2.Classifier Element ID not ot be NULL  (0).         */
/* Input(s)           : pClfrElementNode                                     */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : gQoSGlobalInfo                                       */
/* Global Variables                                                          */
/* Modified           : gQoSGlobalInfo                                       */
/* Return Value(s)    : QOS_SUCCESS or QOS_FAILURE                           */
/* Called By          : SNMP Low Level                                       */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QoSValidateClfrElementEntry (tQosStdClfrElementEntry * pClfrElementNode)
{
    /*Check Mandatory Fields */
    if (pClfrElementNode == NULL)
    {
        return (QOS_FAILURE);
    }

    if ((pClfrElementNode->u4DServClfrId == QOS_START_INITIAL_INDEX) &&
        (pClfrElementNode->u4DServClfrElementId) == QOS_START_INITIAL_INDEX)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "%s : CLASSFIER  Should not be 0.\r\n",
                      __FUNCTION__);
        pClfrElementNode->u1DSClfrElementStatus = QOS_NOT_READY;
        return (QOS_FAILURE);
    }

    pClfrElementNode->u1DSClfrElementStatus = QOS_NOT_IN_SERVICE;

    return (QOS_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSDeleteClfrElementEntry                            */
/* Description        : This function is used to Delete an Entry from the    */
/*                      Classsfier Element Table ,it will do the following   */
/*                      Action                                               */
/*                      1. Remove the Entry from the Classfier Elem Table    */
/*                      3. Clear the Removed Entry                           */
/*                      4. Release the Removed Entry's memory                */
/* Input(s)           : pClfrElementNode                                     */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : gQoSGlobalInfo                                       */
/* Global Variables                                                          */
/* Modified           : gQoSGlobalInfo                                       */
/* Return Value(s)    : QOS_SUCCESS or QOS_FAILURE                           */
/* Called By          : SNMP Low Level                                       */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QoSDeleteClfrElementEntry (tQosStdClfrElementEntry * pClfrElementNode)
{
    UINT4               u4RetStatus = QOS_FAILURE;
    tQosStdClfrElementEntry *pDelClfrNode = NULL;

    /* 1. Remove Entry from the Class Map Table */
    pDelClfrNode = RBTreeRem (gQoSGlobalInfo.DsClfrElementTbl,
                              (tRBElem *) pClfrElementNode);
    if (pDelClfrNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : RBTreeRem () Failed.\r\n",
                      __FUNCTION__);

        return (QOS_FAILURE);
    }

    /* 2. Clear the Removed Entry */
    MEMSET (pDelClfrNode, 0, sizeof (tQosStdClfrElementEntry));

    /* 3. Release the Removed Entry's memory */
    u4RetStatus = MemReleaseMemBlock (gQoSGlobalInfo.DsClfrElementPoolId,
                                      (UINT1 *) pDelClfrNode);
    if (u4RetStatus == MEM_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : MemReleaseMemBlock () Failed.\r\n",
                      __FUNCTION__);

        return (QOS_FAILURE);
    }

    return (QOS_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSGetNextValidClfrElemIndex                         */
/* Description        : This function is used to Get the Next Entry from the */
/*                      Classfier Element Table                              */
/* Input(s)           : u4CurrentIndex  - Current index id                   */
/*                      pu4NextIndex    - Pointer to NextIndex Id            */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : gQoSGlobalInfo                                       */
/* Global Variables                                                          */
/* Modified           : gQoSGlobalInfo                                       */
/* Return Value(s)    : QOS_SUCCESS or QOS_FAILURE                           */
/* Called By          : SNMP Low Level                                       */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QoSGetNextValidClfrElemIndex (UINT4 u4DiffServClfrId,
                              UINT4 *pu4NextDiffServClfrId,
                              UINT4 u4DiffServClfrElementId,
                              UINT4 *pu4NextDiffServClfrElementId)
{
    tQosStdClfrElementEntry *pClfrElementEntry = NULL;
    tQosStdClfrElementEntry ClfrElementEntry;

    if ((u4DiffServClfrId == QOS_START_INITIAL_INDEX) &&
        (u4DiffServClfrElementId == QOS_START_INITIAL_INDEX))
    {
        pClfrElementEntry = (tQosStdClfrElementEntry *)
            RBTreeGetFirst (gQoSGlobalInfo.DsClfrElementTbl);
    }
    else
    {
        ClfrElementEntry.u4DServClfrId = u4DiffServClfrId;
        ClfrElementEntry.u4DServClfrElementId = u4DiffServClfrElementId;
        pClfrElementEntry = (tQosStdClfrElementEntry *)
            RBTreeGetNext (gQoSGlobalInfo.DsClfrElementTbl,
                           &ClfrElementEntry, QoSClfrElementCmpFun);
    }

    if (pClfrElementEntry == NULL)
    {
        QOS_TRC_ARG3 (MGMT_TRC, "In %s : RBTreeGetNext () Failed, "
                      " for Classifier Id %d and classifier Element Id %d.\r\n",
                      __FUNCTION__, u4DiffServClfrId, u4DiffServClfrElementId);

        return QOS_FAILURE;
    }

    *pu4NextDiffServClfrId = pClfrElementEntry->u4DServClfrId;
    *pu4NextDiffServClfrElementId = pClfrElementEntry->u4DServClfrElementId;
    return QOS_SUCCESS;

}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : QosGetTBparamTypeFromOid                           */
/*                                                                           */
/*     DESCRIPTION      : This function will return whether the given        */
/*                        Index exist or not.                                */
/*                                                                           */
/*     INPUT            :                                                    */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : QOS_SUCCESS / QOS_FAILURE                          */
/*                                                                           */
/*****************************************************************************/
INT4
QosGetTBparamTypeFromOid (tSNMP_OID_TYPE * Oid, UINT4 *u4Type)
{
    INT4                i4Match = QOS_SUCCESS;
    UINT4               u4Tmp = QOS_START_INITIAL_INDEX;
    UINT4               u4Count = QOS_START_INITIAL_INDEX;

    for (u4Tmp = 0; u4Tmp <= DS_TB_TSQ_TCM; u4Tmp++)
    {

        i4Match = QOS_TRUE;
        for (u4Count = 0; u4Count < QosTBParamTbl[u4Tmp].ObjectID.u4_Length;
             u4Count++)
        {
            if (QosTBParamTbl[u4Tmp].ObjectID.pu4_OidList[u4Count] !=
                Oid->pu4_OidList[u4Count])
            {
                i4Match = QOS_FAILURE;
                break;
            }
        }
        if (i4Match == QOS_SUCCESS)
        {
            *u4Type = QosTBParamTbl[u4Tmp].u4OidType;
            break;
        }
    }

    return i4Match;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : QosGetSchedTypeFromOid                             */
/*                                                                           */
/*     DESCRIPTION      : This function will return whether the given        */
/*                        Index exist or not.                                */
/*                                                                           */
/*     INPUT            :                                                    */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : QOS_SUCCESS / QOS_FAILURE                          */
/*                                                                           */
/*****************************************************************************/
INT4
QosGetSchedTypeFromOid (tSNMP_OID_TYPE * Oid, UINT4 *u4Type)
{
    UINT1               u1Match = QOS_SUCCESS;
    UINT4               u4Tmp = QOS_START_INITIAL_INDEX;
    UINT4               u4Count = QOS_START_INITIAL_INDEX;

    for (u4Tmp = 0; u4Tmp <= DS_SCHED_WFQ; u4Tmp++)
    {

        u1Match = QOS_TRUE;
        for (u4Count = 0;
             u4Count < QosSchedMethodTable[u4Tmp].ObjectID.u4_Length; u4Count++)
        {
            if (QosSchedMethodTable[u4Tmp].ObjectID.pu4_OidList[u4Count] !=
                Oid->pu4_OidList[u4Count])
            {
                u1Match = QOS_FAILURE;
                break;
            }
        }
        if (u1Match == QOS_SUCCESS)
        {
            *u4Type = QosSchedMethodTable[u4Tmp].u4OidType;
            break;
        }
    }

    return u1Match;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : QosGetTypeIdFromOid                                */
/*     DESCRIPTION      : This function returns type and Id for a given OID  */
/*                        Index exist or not.                                */
/*     INPUT            : Oid                                                */
/*     OUTPUT           : u4Type                                             */
/*                      : u4Id                                               */
/*     RETURNS          : QOS_SUCCESS / QOS_FAILURE                          */
/*****************************************************************************/
INT4
QosGetTypeIdFromOid (tSNMP_OID_TYPE * Oid, UINT4 *u4Type, UINT4 *u4Id)
{
    UINT1               u1Match = QOS_SUCCESS;
    UINT4               u4Tmp = QOS_START_INITIAL_INDEX;
    UINT4               u4Count = QOS_START_INITIAL_INDEX;

    for (u4Tmp = 0; u4Tmp <= DS_MAXRATE; u4Tmp++)
    {
        u1Match = QOS_SUCCESS;
        for (u4Count = 0; u4Count < QosFuncBlockTbl[u4Tmp].ObjectID.u4_Length;
             u4Count++)
        {
            if (QosFuncBlockTbl[u4Tmp].ObjectID.pu4_OidList[u4Count] !=
                Oid->pu4_OidList[u4Count])
            {
                u1Match = QOS_FAILURE;
                break;
            }
        }
        if (u1Match == QOS_SUCCESS)
        {
            *u4Id = Oid->pu4_OidList[QosFuncBlockTbl[u4Tmp].ObjectID.u4_Length];
            *u4Type = QosFuncBlockTbl[u4Tmp].u4OidType;
            break;
        }
    }
    return u1Match;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : QosGetOidFromTypeId                                */
/*     DESCRIPTION      : This function returns OID for a given type and Id  */
/*     INPUT            : u4Type                                             */
/*                      : u4Id                                               */
/*     OUTPUT           : None                                               */
/*     RETURNS          : QOS_SUCCESS / QOS_FAILURE                          */
/*****************************************************************************/
INT4
QosGetOidFromTypeId (UINT4 u4Id, UINT4 u4Type, tSNMP_OID_TYPE * Oid)
{
    UINT4               u4Tmp = 0;

    if (u4Type > DS_MAXRATE)
    {
        return QOS_FAILURE;
    }

    for (u4Tmp = 0; u4Tmp < QosFuncBlockTbl[u4Type].ObjectID.u4_Length; u4Tmp++)
    {
        Oid->pu4_OidList[u4Tmp] =
            QosFuncBlockTbl[u4Type].ObjectID.pu4_OidList[u4Tmp];
    }

    Oid->pu4_OidList[QosFuncBlockTbl[u4Type].ObjectID.u4_Length] = u4Id;
    Oid->u4_Length = QosFuncBlockTbl[u4Type].ObjectID.u4_Length + 1;
    if (u4Type == DS_MAXRATE)
    {
        Oid->pu4_OidList[QosFuncBlockTbl[u4Type].ObjectID.u4_Length + 1] = 2;
        Oid->u4_Length = QosFuncBlockTbl[u4Type].ObjectID.u4_Length + 2;
    }

    return QOS_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : QosGetOidFromTBParamType                                */
/*                                                                           */
/*     DESCRIPTION      : This function will return whether the given        */
/*                        Index exist or not.                                */
/*                                                                           */
/*     INPUT            :                                                   */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : QOS_SUCCESS / QOS_FAILURE                          */
/*                                                                           */
/*****************************************************************************/
INT4
QosGetOidFromTBParamType (UINT4 u4Type, tSNMP_OID_TYPE * Oid)
{
    UINT4               u4Tmp = 0;

    if (u4Type > DS_TB_TSQ_TCM)
    {
        return QOS_FAILURE;
    }

    for (u4Tmp = 0; u4Tmp < QosTBParamTbl[u4Type].ObjectID.u4_Length; u4Tmp++)
    {
        Oid->pu4_OidList[u4Tmp] =
            QosTBParamTbl[u4Type].ObjectID.pu4_OidList[u4Tmp];
    }

    Oid->u4_Length = QosTBParamTbl[u4Type].ObjectID.u4_Length;

    return QOS_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : QosGetTBParamTypeFromOid                           */
/*                                                                           */
/*     DESCRIPTION      : This function will return whether the given        */
/*                        Index exist or not.                                */
/*                                                                           */
/*     INPUT            :                                                   */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : QOS_SUCCESS / QOS_FAILURE                          */
/*                                                                           */
/*****************************************************************************/
INT4
QosGetTBParamTypeFromOid (UINT4 *u4Type, tSNMP_OID_TYPE * Oid)
{
    UINT4               u4Tmp = 0;
    UINT4               u4Count = 0;
    INT4                i4Match = QOS_FAILURE;
    for (u4Tmp = 0; u4Tmp <= DS_TB_TSQ_TCM; u4Tmp++)
    {
        i4Match = QOS_SUCCESS;
        for (u4Count = 0; u4Count < QosTBParamTbl[u4Tmp].ObjectID.u4_Length;
             u4Count++)
        {
            if (QosTBParamTbl[u4Tmp].ObjectID.pu4_OidList[u4Count] !=
                Oid->pu4_OidList[u4Count])
            {
                i4Match = QOS_FAILURE;
                break;
            }
        }
        if (i4Match == QOS_SUCCESS)
        {
            *u4Type = QosTBParamTbl[u4Tmp].u4OidType;
            break;
        }
    }

    return i4Match;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : QosSchedOidFromType                                */
/*                                                                           */
/*     DESCRIPTION      : This function will return whether the given        */
/*                        Index exist or not.                                */
/*                                                                           */
/*     INPUT            :                                                   */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : QOS_SUCCESS / QOS_FAILURE                          */
/*                                                                           */
/*****************************************************************************/
INT4
QosSchedOidFromType (UINT4 u4Type, tSNMP_OID_TYPE * Oid)
{
    UINT4               u4Tmp = 0;

    if (u4Type > DS_SCHED_WFQ)
    {
        return QOS_FAILURE;
    }

    for (u4Tmp = 0; u4Tmp < QosSchedMethodTable[u4Type].ObjectID.u4_Length;
         u4Tmp++)
    {
        Oid->pu4_OidList[u4Tmp] =
            QosSchedMethodTable[u4Type].ObjectID.pu4_OidList[u4Tmp];
    }

    Oid->u4_Length = QosSchedMethodTable[u4Type].ObjectID.u4_Length;

    return QOS_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    :QosValidateIndex                                 */
/*                                                                           */
/*     DESCRIPTION      : This function will return whether the given        */
/*                        Index exist or not.                                */
/*     INPUT            : u4DiffServId : Index Identifier                    */
/*                      : u4EntryType :  Entry Type                          */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : QOS_SUCCESS / QOS_FAILURE                          */
/*                                                                           */
/*****************************************************************************/
INT4
QosValidateIndex (UINT4 u4DiffServId, UINT4 u4EntryType)
{
    INT4                i4RetStatus = QOS_FAILURE;
    tQosStdClfrEntry   *pClfrEntry = NULL;
    tIssL3FilterEntry  *pMultiField = NULL;
    tQoSRDCfgNode      *pRDCfgNode = NULL;
    tQoSPolicyMapNode  *pMeterNode = NULL;
    tQoSMeterNode      *pTBMeterNode = NULL;
    tQosStdActionEntry *pActionNode = NULL;
    tQosStdCountActEntry *pCountActEntry = NULL;
    tQoSQNode          *pTmpQNode = NULL;

    if (gQoSGlobalInfo.eSysControl != QOS_SYS_CNTL_START)
    {
        QOS_TRC (MGMT_TRC, QOS_MODULE_SHUTDOW_ERROR_MSG);
        return (QOS_FAILURE);
    }
    switch (u4EntryType)
    {
        case DS_CLFR:
            /* Check the Range of Index Values (Min-Max) */
            i4RetStatus = QOS_CLFR_ID_VALID (u4DiffServId);
            pClfrEntry = QosGetClfrEntry (u4DiffServId);
            if (pClfrEntry == NULL)
            {
                i4RetStatus = QOS_FAILURE;
            }
            break;

        case DS_CLFR_ELEM:
            /* Check the Range of Index Values (Min-Max) */
            /*i4RetStatus = QOS_CLFR_ELEMENT_ID_VALID(u4DiffServId); */
            i4RetStatus = QOS_SUCCESS;
            break;
        case DS_MF_CLFR:
            /* Check the Range of Index Values (Min-Max) */
            i4RetStatus = QOS_L3FILTER_ID_VALID (u4DiffServId);
            pMultiField = QosGetMultiFieldClfrEntry (u4DiffServId);
            if (pMultiField == NULL)
            {
                i4RetStatus = QOS_FAILURE;
            }
            break;
        case DS_METER:
            /* Check the Range of Index Values (Min-Max) */
            i4RetStatus = QOS_CHECK_METER_TBL_INDEX_RANGE (u4DiffServId);
            pMeterNode = QoSUtlGetPolicyMapNode (u4DiffServId);
            if (pMeterNode == NULL)
            {
                QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetPolicyMapNode () "
                              " Returns FAILURE. \r\n", __FUNCTION__);
                return (QOS_FAILURE);
            }
            break;
        case DS_TBPARM:
            i4RetStatus = QOS_CHECK_METER_TBL_INDEX_RANGE (u4DiffServId);

            pTBMeterNode = QoSUtlGetMeterNode (u4DiffServId);
            if (pTBMeterNode == NULL)
            {
                QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetMeterNode () "
                              " Returns FAILURE. \r\n", __FUNCTION__);
                return (QOS_FAILURE);
            }
            /* Check the Range of Index Values (Min-Max) */
            break;
        case DS_ACTION:
            i4RetStatus = QOS_CHECK_ACTION_TBL_INDEX_RANGE (u4DiffServId);

            pActionNode = QoSUtlGetActionNode (u4DiffServId);
            if (pActionNode == NULL)
            {
                QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetActionNode () "
                              " Returns FAILURE. \r\n", __FUNCTION__);
                return (QOS_FAILURE);
            }
            /* Check the Range of Index Values (Min-Max) */
            break;
        case DS_STDDSCP:
            /* Check the Range of Index Values (Min-Max) */
            if (u4DiffServId > QOS_DSCP_MAX_VAL)
            {
                return SNMP_FAILURE;
            }
            i4RetStatus = QOS_SUCCESS;
            break;
        case DS_COUNT_ACT:
            i4RetStatus = QOS_CHECK_COUNT_ACT_TBL_INDEX_RANGE (u4DiffServId);

            pCountActEntry = QoSUtlGetCountActionNode (u4DiffServId);
            if (pCountActEntry == NULL)
            {
                return QOS_FAILURE;
            }
            /* Check the Range of Index Values (Min-Max) */
            break;
        case DS_ALG_DROP:
            /* Check the Range of Index Values (Min-Max) */
            if (QoSUtlGetQTempNode (u4DiffServId) == NULL)
            {
                return QOS_FAILURE;
            }
            i4RetStatus = QOS_SUCCESS;
            break;
        case DS_RANDOM_DROP:
            i4RetStatus = QOS_CHECK_RANDOM_TBL_INDEX_RANGE (u4DiffServId);
            if (i4RetStatus == QOS_SUCCESS)
            {
                pRDCfgNode = QoSUtlGetRDCfgNode (u4DiffServId, 0);
                if (pRDCfgNode == NULL)
                {
                    i4RetStatus = QOS_FAILURE;
                }
            }
            /* Check the Range of Index Values (Min-Max) */
            break;
        case DS_QUEUE:
            /* Check the Range of Index Values (Min-Max) */
            i4RetStatus = QOS_CHECK_Q_TEMP_TBL_INDEX_RANGE (u4DiffServId);
            pTmpQNode =
                QoSUtlGetQNode (QOS_IF_IDX_FROM_GLOBAL_QID (u4DiffServId),
                                u4DiffServId);
            if (pTmpQNode == NULL)
            {
                QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetQNode () "
                              "Returns FAILURE. \r\n", __FUNCTION__);
                return (QOS_FAILURE);
            }

            break;
        case DS_SCHEDULER:
            /* Check the Range of Index Values (Min-Max) */
            break;
        case DS_MINRATE:
            /* Check the Range of Index Values (Min-Max) */
            break;
        case DS_MAXRATE:
            /* Check the Range of Index Values (Min-Max) */
            break;
        default:
            i4RetStatus = QOS_SUCCESS;
            /* Check the Range of Index Values (Min-Max) */
            break;
    }

    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "%s :Entry not valid .", __FUNCTION__);

        return (QOS_FAILURE);
    }

    return (QOS_SUCCESS);

}

/*****************************************************************************/
/* Function Name      : QosGetDataPathEntry                                  */
/* Description        : Returns pointer to data path entry                   */
/* Input(s)           : i4IfIndex                                            */
/*                    : i4DSDataPathIfDir                                    */
/* Output(s)          : None.                                                */
/* Return Value(s)    : Pointer to Data path entry                           */
/*****************************************************************************/
tQosStdDataPathEntry *
QosGetDataPathEntry (INT4 i4IfIndex, INT4 i4DSDataPathIfDir)
{
    tQosStdDataPathEntry DataPathEntry;
    tQosStdDataPathEntry *pDataPathEntry = NULL;

    MEMSET (&DataPathEntry, 0, sizeof (tQosStdDataPathEntry));
    DataPathEntry.i4IfIndex = i4IfIndex;
    DataPathEntry.i4DSDataPathIfDir = i4DSDataPathIfDir;

    pDataPathEntry = (tQosStdDataPathEntry *)
        RBTreeGet (gQoSGlobalInfo.DsDataPathTbl, (tRBElem *) & DataPathEntry);
    if (pDataPathEntry == NULL)
    {
        QOS_TRC_ARG3 (MGMT_TRC, "%s :Data Path Entry (Port) Id %d "
                      "and Direction Id is not found in the  "
                      "Data Path Entry Table. \r\n", __FUNCTION__, i4IfIndex,
                      i4DSDataPathIfDir);
    }
    return (pDataPathEntry);
}

/*****************************************************************************/
/* Function Name      : QosGetClfrEntry                                      */
/* Description        : Returns pointer to classifier entry                  */
/* Input(s)           : u4Index                                              */
/* Output(s)          : None.                                                */
/* Return Value(s)    : Pointer to classifier entry                          */
/*****************************************************************************/

tQosStdClfrEntry   *
QosGetClfrEntry (UINT4 u4Index)
{
    tQosStdClfrEntry    ClfrEntry;
    tQosStdClfrEntry   *pClfrEntry = NULL;

    MEMSET (&ClfrEntry, 0, sizeof (tQosStdClfrEntry));
    ClfrEntry.u4DsClfrId = u4Index;

    pClfrEntry = (tQosStdClfrEntry *) RBTreeGet (gQoSGlobalInfo.DsClfrTbl,
                                                 (tRBElem *) & ClfrEntry);
    if (pClfrEntry == NULL)
    {
        QOS_TRC_ARG2 (MGMT_TRC, "%s :Classifier Id %d is not Found in the "
                      "Classifier Table. \r\n", __FUNCTION__, u4Index);
    }

    return (pClfrEntry);
}

/*****************************************************************************/
/* Function Name      : QosGetClfrElementEntry                               */
/* Description        : Returns pointer to classifier element entry          */
/* Input(s)           : u4DiffServClfrId                                     */
/*                    : u4DiffServClfrElementId                              */
/* Output(s)          : None.                                                */
/* Return Value(s)    : Pointer to classifier element entry                  */
/*****************************************************************************/
tQosStdClfrElementEntry *
QosGetClfrElementEntry (UINT4 u4DiffServClfrId, UINT4 u4DiffServClfrElementId)
{
    tQosStdClfrElementEntry ClfrElementEntry;
    tQosStdClfrElementEntry *pClfrElementEntry = NULL;

    MEMSET (&ClfrElementEntry, 0, sizeof (tQosStdClfrElementEntry));
    ClfrElementEntry.u4DServClfrId = u4DiffServClfrId;
    ClfrElementEntry.u4DServClfrElementId = u4DiffServClfrElementId;
    pClfrElementEntry = (tQosStdClfrElementEntry *)
        RBTreeGet (gQoSGlobalInfo.DsClfrElementTbl,
                   (tRBElem *) & ClfrElementEntry);
    if (pClfrElementEntry == NULL)
    {
        QOS_TRC_ARG2 (MGMT_TRC,
                      "%s :Classifier Element Id %d is not Found in the "
                      "Classifier Element Table. \r\n", __FUNCTION__,
                      u4DiffServClfrId);
    }
    return (pClfrElementEntry);
}

/*****************************************************************************/
/* Function Name      : QosGetMultiFieldClfrEntry                            */
/* Description        : This get the pointer to L3 ACL filter entry          */
/* Input(s)           : u4FilterNo                                           */
/* Output(s)          : None.                                                */
/* Return Value(s)    : Pointer to L3 ACL entry                              */
/*****************************************************************************/
tIssL3FilterEntry  *
QosGetMultiFieldClfrEntry (UINT4 u4FilterNo)
{
    tIssL3FilterEntry  *pIssL3FilterEntry = NULL;

    pIssL3FilterEntry = IssGetL3FilterEntry (u4FilterNo);

    if (pIssL3FilterEntry == NULL)
    {
        QOS_TRC_ARG2 (MGMT_TRC, "%s : Multi Field Classifier Id %d"
                      "is not Found in the "
                      "Multi Field Classifier Table. \r\n", __FUNCTION__,
                      u4FilterNo);
    }
    return (pIssL3FilterEntry);

}

/*****************************************************************************/
/* Function Name      : QosGetQEntry                                         */
/* Description        : This funtion gets the pointer to the Q Entry         */
/* Input(s)           : u4DServQId                                           */
/* Output(s)          : None.                                                */
/* Return Value(s)    : Pointer to the Q Entry                               */
/*****************************************************************************/
tQosStdQEntry      *
QosGetQEntry (UINT4 u4DServQId)
{
    tQosStdQEntry       QosStdQNode;
    tQosStdQEntry      *pQosStdQNode = NULL;

    MEMSET (&QosStdQNode, 0, sizeof (tQosStdQEntry));
    QosStdQNode.u4DServQId = u4DServQId;

    pQosStdQNode = (tQosStdQEntry *) RBTreeGet (gQoSGlobalInfo.DsQHead,
                                                (tRBElem *) & QosStdQNode);
    if (pQosStdQNode == NULL)
    {
        QOS_TRC_ARG2 (MGMT_TRC, "%s : Queue Id %d is not Found in the "
                      "Queue Table. \r\n", __FUNCTION__, u4DServQId);
    }

    return (pQosStdQNode);
}

/*****************************************************************************/
/* Function Name      : QosGetSchedulerEntry                             */
/*                                                                           */
/* Description        :                                                      */
/*                                                                           */
/* Input(s)           :  u4SchedulerId                                         */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Return Value(s)    : SUCCESS / FAILURE                                    */
/*****************************************************************************/
tQosStdSchedulerEntry *
QosGetSchedulerEntry (UINT4 u4SchedulerId)
{
    tQosStdSchedulerEntry TempSchedulerNode;
    tQosStdSchedulerEntry *pSchedulerNode = NULL;

    MEMSET (&TempSchedulerNode, 0, sizeof (tQosStdSchedulerEntry));
    TempSchedulerNode.u4DSSchedulerId = u4SchedulerId;

    pSchedulerNode =
        (tQosStdSchedulerEntry *) RBTreeGet (gQoSGlobalInfo.DsSchedulerTbl,
                                             (tRBElem *) & TempSchedulerNode);
    if (pSchedulerNode == NULL)
    {
        QOS_TRC_ARG2 (MGMT_TRC, "%s : Scheduler Id %d is not Found in the "
                      "Scheduler Table. \r\n", __FUNCTION__, u4SchedulerId);
    }

    return (pSchedulerNode);
}

/*****************************************************************************/
/* Function Name      : QosGetMinRateEntry                             */
/*                                                                           */
/* Description        :                                                      */
/*                                                                           */
/* Input(s)           :  u4MinRateId                                         */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Return Value(s)    : SUCCESS / FAILURE                                    */
/*****************************************************************************/
tQosStdMinRateEntry *
QosGetMinRateEntry (UINT4 u4MinRateId)
{
    tQosStdMinRateEntry TempMinRateNode;
    tQosStdMinRateEntry *pMinRateNode = NULL;

    MEMSET (&TempMinRateNode, 0, sizeof (tQosStdMinRateEntry));
    TempMinRateNode.u4DSMinRateId = u4MinRateId;

    pMinRateNode =
        (tQosStdMinRateEntry *) RBTreeGet (gQoSGlobalInfo.DsMinRateTbl,
                                           (tRBElem *) & TempMinRateNode);
    if (pMinRateNode == NULL)
    {
        QOS_TRC_ARG2 (MGMT_TRC, "%s : MinRate Id %d is not Found in the "
                      "MinRate Table. \r\n", __FUNCTION__, u4MinRateId);
    }

    return (pMinRateNode);
}

/*****************************************************************************/
/* Function Name      : QosGetMaxRateEntry                        */
/*                                                                           */
/* Description        :                                                      */
/*                                                                           */
/* Input(s)           :  u4MaxRateId, u4MaxRateLevel                         */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Return Value(s)    : SUCCESS / FAILURE                                    */
/*****************************************************************************/
tQosStdMaxRateEntry *
QosGetMaxRateEntry (UINT4 u4MaxRateId, UINT4 u4MaxRateLevel)
{
    tQosStdMaxRateEntry TempMaxRateNode;
    tQosStdMaxRateEntry *pMaxRateNode = NULL;

    MEMSET (&TempMaxRateNode, 0, sizeof (tQosStdMaxRateEntry));
    TempMaxRateNode.u4DSMaxRateId = u4MaxRateId;
    TempMaxRateNode.u4DSMaxRateLevel = u4MaxRateLevel;

    pMaxRateNode =
        (tQosStdMaxRateEntry *) RBTreeGet (gQoSGlobalInfo.DsMaxRateTbl,
                                           (tRBElem *) & TempMaxRateNode);
    if (pMaxRateNode == NULL)
    {
        QOS_TRC_ARG2 (MGMT_TRC, "%s : MaxRate Id %d is not Found in the "
                      "MaxRate Table. \r\n", __FUNCTION__, u4MaxRateId);
    }

    return (pMaxRateNode);

}

/*****************************************************************************/
/* Function Name      : QoSActionCmpFun                                      */
/* Description        : This function is used as a Compare function in the   */
/*                      RBTree.                                              */
/*                      1.It Compares the ActionId and return the values*/
/* Input(s)           : e1 , e2 -  Elements of RBTree                        */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    :  -1/0/1                                              */
/* Called By          : RBTreeUtility                                        */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QoSActionCmpFun (tRBElem * e1, tRBElem * e2)
{
    tQosStdActionEntry *pActionNode1 = NULL;
    tQosStdActionEntry *pActionNode2 = NULL;

    pActionNode1 = (tQosStdActionEntry *) e1;
    pActionNode2 = (tQosStdActionEntry *) e2;

    /* Compare the InPriMap Id */

    if (pActionNode1->u4DSActionId < pActionNode2->u4DSActionId)
    {
        return (-1);
    }
    else if (pActionNode1->u4DSActionId > pActionNode2->u4DSActionId)
    {
        return (1);
    }

    return (0);
}

/*****************************************************************************/
/* Function Name      : QoSCountActCmpFun                                    */
/* Description        : This function is used as a Compare function in the   */
/*                      RBTree.                                              */
/*                      1.It Compares the ActionId and return the values*/
/* Input(s)           : e1 , e2 -  Elements of RBTree                        */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    :  -1/0/1                                              */
/* Called By          : RBTreeUtility                                        */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QoSCountActCmpFun (tRBElem * e1, tRBElem * e2)
{
    tQosStdCountActEntry *pActionNode1 = NULL;
    tQosStdCountActEntry *pActionNode2 = NULL;

    pActionNode1 = (tQosStdCountActEntry *) e1;
    pActionNode2 = (tQosStdCountActEntry *) e2;

    /* Compare the InPriMap Id */

    if (pActionNode1->u4DSCountActId < pActionNode2->u4DSCountActId)
    {
        return (-1);
    }
    else if (pActionNode1->u4DSCountActId > pActionNode2->u4DSCountActId)
    {
        return (1);
    }

    return (0);
}

/*****************************************************************************/
/* Function Name      : QoSUtlGetActionNode                                  */
/* Description        : This function is used to Get the Entry  form  the    */
/*                      action Table ,it will do the following Action        */
/*                      1. Call RBTreeGet Utility.                           */
/* Input(s)           : u4ActionId      - Index to ActionNode                */
/* Return Value(s)    : NULL/ Pointer to 'tQoSStdActionTable'                */
/*****************************************************************************/
tQosStdActionEntry *
QoSUtlGetActionNode (UINT4 u4ActionId)
{
    tQosStdActionEntry  TempActionNode;
    tQosStdActionEntry *pActionNode = NULL;

    MEMSET (&TempActionNode, 0, sizeof (tQosStdActionEntry));
    TempActionNode.u4DSActionId = u4ActionId;

    pActionNode = (tQosStdActionEntry *)
        RBTreeGet (gQoSGlobalInfo.DsActionTbl, (tRBElem *) & TempActionNode);

    if (pActionNode == NULL)
    {
        QOS_TRC_ARG2 (MGMT_TRC, "%s : Action Id %d is not Found in the "
                      "Action Table. \r\n", __FUNCTION__, u4ActionId);
    }

    return (pActionNode);
}

/*****************************************************************************/
/* Function Name      : QoSUtlGetCountActionNode                             */
/* Description        : This function is used to Get the Entry  form  the    */
/*                      countact Table ,it will do the following Action      */
/*                      1. Call RBTreeGet Utility.                           */
/* Input(s)           : u4CntActId      - Index to CntActNode                */
/*****************************************************************************/
tQosStdCountActEntry *
QoSUtlGetCountActionNode (UINT4 u4CntActId)
{
    tQosStdCountActEntry TempActionNode;
    tQosStdCountActEntry *pCntActNode = NULL;

    MEMSET (&TempActionNode, 0, sizeof (tQosStdCountActEntry));
    TempActionNode.u4DSCountActId = u4CntActId;

    pCntActNode = (tQosStdCountActEntry *)
        RBTreeGet (gQoSGlobalInfo.DsCountActTbl, (tRBElem *) & TempActionNode);

    if (pCntActNode == NULL)
    {
        QOS_TRC_ARG2 (MGMT_TRC, "%s : CountAct Id %d is not Found in the "
                      "CountAct Table. \r\n", __FUNCTION__, u4CntActId);
    }

    return (pCntActNode);
}

/*****************************************************************************/
/* Function Name      : QoSGetNextActionTblEntryIndex                        */
/* Description        : This function is used to Get the Next Entry from the */
/*                      Action Table ,it will do the following Action        */
/*                      1. If u4CurrentIndex is 0 then  GetFirxtIndex.       */
/*                      2. If u4CurrentIndex is not 0 then  GetNextIndex.    */
/* Input(s)           : u4CurrentIndex  - Current index id                   */
/* Output(s)          : pu4NextIndex    - Pointer to NextIndex Id one.       */
/* Return Value(s)    : QOS_SUCCESS or QOS_FAILURE                           */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QoSGetNextActionTblEntryIndex (UINT4 u4CurrentIndex, UINT4 *pu4NextIndex)
{
    tQosStdActionEntry *pActionNode = NULL;
    tQosStdActionEntry  ActionNode;

    *pu4NextIndex = 0;

    MEMSET (&ActionNode, 0, sizeof (tQosStdActionEntry));
    if (u4CurrentIndex == QOS_START_INITIAL_INDEX)
    {
        pActionNode = (tQosStdActionEntry *)
            RBTreeGetFirst (gQoSGlobalInfo.DsActionTbl);
    }
    else
    {
        ActionNode.u4DSActionId = u4CurrentIndex;
        pActionNode = (tQosStdActionEntry *)
            RBTreeGetNext (gQoSGlobalInfo.DsActionTbl,
                           &ActionNode, QoSActionCmpFun);
    }

    if (pActionNode == NULL)
    {
        QOS_TRC_ARG2 (MGMT_TRC, "In %s : RBTreeGetNext () Failed, "
                      " for Action %d.\r\n", __FUNCTION__, u4CurrentIndex);

        return (QOS_FAILURE);
    }
    if (QosValidateIndex (pActionNode->u4DSActionId, DS_ACTION) == QOS_FAILURE)
    {
        return (QOS_FAILURE);
    }
    else
    {

        *pu4NextIndex = pActionNode->u4DSActionId;
    }

    return (QOS_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSGetNextCountActEntryIndex                         */
/* Description        : This function is used to Get the Next Entry from the */
/*                      Dscp Table ,it will do the following Action        */
/*                      1. If u4CurrentIndex is 0 then  GetFirxtIndex.       */
/*                      2. If u4CurrentIndex is not 0 then  GetNextIndex.    */
/* Input(s)           : u4CurrentIndex  - Current index id                   */
/* Output(s)          : pu4NextIndex    - Pointer to NextIndex Id one.       */
/* Return Value(s)    : QOS_SUCCESS or QOS_FAILURE                           */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QoSGetNextCountActEntryIndex (UINT4 u4CurrentIndex, UINT4 *pu4NextIndex)
{
    tQosStdCountActEntry *pCountActNode = NULL;
    tQosStdCountActEntry CountActNode;

    MEMSET (&CountActNode, 0, sizeof (tQosStdCountActEntry));
    if (u4CurrentIndex == QOS_START_INITIAL_INDEX)
    {
        pCountActNode = (tQosStdCountActEntry *)
            RBTreeGetFirst (gQoSGlobalInfo.DsCountActTbl);
    }
    else
    {
        CountActNode.u4DSCountActId = u4CurrentIndex;
        pCountActNode = (tQosStdCountActEntry *)
            RBTreeGetNext (gQoSGlobalInfo.DsCountActTbl,
                           &CountActNode, QoSCountActCmpFun);
    }

    if (pCountActNode == NULL)
    {
        QOS_TRC_ARG2 (MGMT_TRC, "In %s : RBTreeGetNext () Failed, "
                      " for CountAct %d.\r\n", __FUNCTION__, u4CurrentIndex);

        return (QOS_FAILURE);
    }

    *pu4NextIndex = pCountActNode->u4DSCountActId;

    return (QOS_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QosValidateType                                      */
/* Description        : This function is used to validate algo drop type     */
/* Input(s)           : u4DropType                                           */
/*                    : u4DropSpecificType                                   */
/* Output(s)          : None                                                 */
/* Returns            : QOS_SUCCESS/QOS_FAILURE                              */
/*****************************************************************************/
INT4
QosValidateType (UINT4 u4DropType, UINT4 u4DropSpecificType)
{
    INT4                i4RetStatus = QOS_FAILURE;

    if (gQoSGlobalInfo.eSysControl != QOS_SYS_CNTL_START)
    {
        QOS_TRC (MGMT_TRC, QOS_MODULE_SHUTDOW_ERROR_MSG);
        return (QOS_FAILURE);
    }
    switch (u4DropType)
    {
        case QOS_Q_TEMP_DROP_TYPE_OTHER:
            /*Ideally for this case u4DropSpecificType should
             * point to an entry in  another MIB.
             * This is not supported now.*/
            i4RetStatus = QOS_SUCCESS;
            UNUSED_PARAM (u4DropSpecificType);
            break;
        case QOS_Q_TEMP_DROP_TYPE_TAIL:
            i4RetStatus = QOS_SUCCESS;
            break;
        case QOS_Q_TEMP_DROP_TYPE_HEAD:
            i4RetStatus = QOS_SUCCESS;
            break;
        case QOS_Q_TEMP_DROP_TYPE_RED:
        case QOS_Q_TEMP_DROP_TYPE_WRED:
            if (u4DropSpecificType != DS_RANDOM_DROP)
            {
                i4RetStatus = QOS_FAILURE;
            }
            break;
        case QOS_Q_TEMP_DROP_TYPE_ALWAYS:
            i4RetStatus = QOS_SUCCESS;
            break;
        default:
            break;
    }

    if (i4RetStatus == QOS_FAILURE)
    {

        return (QOS_FAILURE);
    }

    return (QOS_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSUtlGetStdQEntry                                   */
/* Description        : This function is used to Get the Entry  form  the    */
/*                      Clfr Table ,it will do the following Action          */
/*                      1. Call RBTreeGet Utility.                           */
/* Input(s)           : u4DServClfrId                                        */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : gQoSGlobalInfo                                       */
/* Global Variables                                                          */
/* Modified           :                                                      */
/* Return Value(s)    : NULL/ Pointer to 'tQosStdClfrElementEntry            */
/* Called By          : SNMP Low Level                                       */
/* Calling Function   :                                                      */
/*****************************************************************************/
tQosStdQEntry      *
QoSUtlGetStdQEntry (UINT4 u4DServStdQId)
{
    tQosStdQEntry       TempStdQNode;
    tQosStdQEntry      *pStdQNode = NULL;

    MEMSET (&TempStdQNode, 0, sizeof (tQosStdQEntry));
    TempStdQNode.u4DServQId = u4DServStdQId;

    pStdQNode = (tQosStdQEntry *)
        RBTreeGet (gQoSGlobalInfo.DsQHead, (tRBElem *) & TempStdQNode);

    if (pStdQNode == NULL)
    {
        QOS_TRC_ARG2 (MGMT_TRC, "%s : Classifier Id %d "
                      " is not Found in the "
                      "Classifier Table. \r\n", __FUNCTION__, u4DServStdQId);

    }

    return (pStdQNode);

}

/*****************************************************************************/
/* Function Name      : DiffServStdQCmpFun                                   */
/* Description        : This function is used as a Compare function in the   */
/*                      RBTree.                                              */
/*                      1.It Compares the StdQEntries and return the values  */
/* Input(s)           : e1 , e2 -  Elements of RBTree                        */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    :  -1/0/1                                              */
/* Called By          : RBTreeUtility                                        */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
DiffServStdQCmpFun (tRBElem * e1, tRBElem * e2)
{
    tQosStdQEntry      *pStdQeNode1 = NULL;
    tQosStdQEntry      *pStdQeNode2 = NULL;

    pStdQeNode1 = (tQosStdQEntry *) e1;
    pStdQeNode2 = (tQosStdQEntry *) e2;

    /* Compare the InPriMap Id */

    if (pStdQeNode1->u4DServQId < pStdQeNode2->u4DServQId)
    {
        return (-1);
    }
    else if (pStdQeNode1->u4DServQId > pStdQeNode2->u4DServQId)
    {
        return (1);
    }

    return (0);
}

/*****************************************************************************/
/* Function Name      : QoSSchedulerCmpFun                                     */
/* Description        : This function is used as a Compare function in the   */
/*                      RBTree.                                              */
/*                      1.It Compares the PriMapTableId and return the values*/
/* Input(s)           : e1 , e2 -  Elements of RBTree                        */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    :  -1/0/1                                              */
/* Called By          : RBTreeUtility                                        */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QoSSchedulerCmpFun (tRBElem * e1, tRBElem * e2)
{
    tQosStdSchedulerEntry *pSchedulerNode1 = NULL;
    tQosStdSchedulerEntry *pSchedulerNode2 = NULL;

    pSchedulerNode1 = (tQosStdSchedulerEntry *) e1;
    pSchedulerNode2 = (tQosStdSchedulerEntry *) e2;

    /* Compare the InPriMap Id */

    if (pSchedulerNode1->u4DSSchedulerId < pSchedulerNode2->u4DSSchedulerId)
    {
        return (-1);
    }
    else if (pSchedulerNode1->u4DSSchedulerId >
             pSchedulerNode2->u4DSSchedulerId)
    {
        return (1);
    }

    return (0);
}

/*****************************************************************************/
/* Function Name      : QoSMinRateCmpFun                                     */
/* Description        : This function is used as a Compare function in the   */
/*                      RBTree.                                              */
/*                      1.It Compares the PriMapTableId and return the values*/
/* Input(s)           : e1 , e2 -  Elements of RBTree                        */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    :  -1/0/1                                              */
/* Called By          : RBTreeUtility                                        */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QoSMinRateCmpFun (tRBElem * e1, tRBElem * e2)
{
    tQosStdMinRateEntry *pMinRateNode1 = NULL;
    tQosStdMinRateEntry *pMinRateNode2 = NULL;

    pMinRateNode1 = (tQosStdMinRateEntry *) e1;
    pMinRateNode2 = (tQosStdMinRateEntry *) e2;

    /* Compare the InPriMap Id */

    if (pMinRateNode1->u4DSMinRateId < pMinRateNode2->u4DSMinRateId)
    {
        return (-1);
    }
    else if (pMinRateNode1->u4DSMinRateId > pMinRateNode2->u4DSMinRateId)
    {
        return (1);
    }

    return (0);
}

/*****************************************************************************/
/* Function Name      : QoSMaxRateCmpFun                                     */
/* Description        : This function is used as a Compare function in the   */
/*                      RBTree.                                              */
/*                      1.It Compares the PriMapTableId and return the values*/
/* Input(s)           : e1 , e2 -  Elements of RBTree                        */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    :  -1/0/1                                              */
/* Called By          : RBTreeUtility                                        */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QoSMaxRateCmpFun (tRBElem * e1, tRBElem * e2)
{
    tQosStdMaxRateEntry *pMaxRateNode1 = NULL;
    tQosStdMaxRateEntry *pMaxRateNode2 = NULL;

    pMaxRateNode1 = (tQosStdMaxRateEntry *) e1;
    pMaxRateNode2 = (tQosStdMaxRateEntry *) e2;

    /* Compare the InPriMap Id */

    if (pMaxRateNode1->u4DSMaxRateId < pMaxRateNode2->u4DSMaxRateId)
    {
        return (-1);
    }
    else if (pMaxRateNode1->u4DSMaxRateId > pMaxRateNode2->u4DSMaxRateId)
    {
        return (1);
    }

    if (pMaxRateNode1->u4DSMaxRateLevel < pMaxRateNode2->u4DSMaxRateLevel)
    {
        return (-1);
    }
    else if (pMaxRateNode1->u4DSMaxRateLevel > pMaxRateNode2->u4DSMaxRateLevel)
    {
        return (1);
    }

    return (0);
}

/*****************************************************************************/
/* Function Name      : QoSCreateStdSchedulerTblEntry                          */
/* Description        : This function is used to Create an Entry in   the    */
/*                      Scheduler Table ,it will do the following Action       */
/*                      1. Allocate a memory block for the New Entry         */
/*                      2. Initialize the New Entry                          */
/*                      3. Add this New Entry into the Priority Map Table    */
/* Input(s)           : u4PriMapId      - Index to InPriMapNode              */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : gQoSGlobalInfo                                       */
/* Global Variables                                                          */
/* Modified           : gQoSGlobalInfo                                       */
/* Return Value(s)    : NULL / Pointer to  InPriMapNode                      */
/* Called By          : SNMP Low Level                                       */
/* Calling Function   :                                                      */
/*****************************************************************************/
tQosStdSchedulerEntry *
QoSCreateStdSchedulerTblEntry (UINT4 u4SchedulerId)
{
    UINT4               u4RetStatus = QOS_FAILURE;
    tQosStdSchedulerEntry *pNewSchedulerNode = NULL;

    /* 1. Allocate a memory block for the New Entry */
    pNewSchedulerNode = (tQosStdSchedulerEntry *)
        MemAllocMemBlk (gQoSGlobalInfo.DsQosSchedulerPoolId);
    if (pNewSchedulerNode == NULL)
    {
        QOS_TRC_ARG2 (MGMT_TRC, "In %s : MemAllocMemBlk () Failed ,"
                      "for Scheduler Id %d.\r\n", __FUNCTION__, u4SchedulerId);
        return (NULL);
    }

    /* 2. Initialize the New Entry */
    MEMSET (pNewSchedulerNode, 0, sizeof (tQosStdSchedulerEntry));

    pNewSchedulerNode->u4DSSchedulerId = u4SchedulerId;
    pNewSchedulerNode->u4DSSchedNextId = 0;
    pNewSchedulerNode->u4DSSchedNextType = 0;
    pNewSchedulerNode->u4DSSchedMethodType = DS_SCHED_SP;
    pNewSchedulerNode->i4DSSchedStorage = QOS_STORAGE_NONVOLATILE;
    pNewSchedulerNode->u1DSchedStatus = NOT_IN_SERVICE;

    /* 3. Add this New Entry into the Priority Map Table */
    u4RetStatus = RBTreeAdd (gQoSGlobalInfo.DsSchedulerTbl,
                             (tRBElem *) pNewSchedulerNode);
    if (u4RetStatus != RB_SUCCESS)
    {
        u4RetStatus =
            MemReleaseMemBlock (gQoSGlobalInfo.DsQosSchedulerPoolId,
                                (UINT1 *) pNewSchedulerNode);

        if (u4RetStatus == MEM_FAILURE)
        {
            QOS_TRC_ARG2 (MGMT_TRC, "In %s : MemReleaseMemBlock () Failed ,"
                          "for Min Rate Id %d. \r\n", __FUNCTION__,
                          u4SchedulerId);

            return (NULL);
        }

        QOS_TRC_ARG2 (MGMT_TRC, "In %s : RBTreeAdd () Failed ,"
                      "for Scheduler Id %d. \r\n", __FUNCTION__, u4SchedulerId);
        return (NULL);
    }

    return (pNewSchedulerNode);
}

/*****************************************************************************/
/* Function Name      : QoSCreateStdMinRateTblEntry                          */
/* Description        : This function is used to Create an Entry in   the    */
/*                      MinRate Table ,it will do the following Action       */
/*                      1. Allocate a memory block for the New Entry         */
/*                      2. Initialize the New Entry                          */
/*                      3. Add this New Entry into the Priority Map Table    */
/* Input(s)           : u4PriMapId      - Index to InPriMapNode              */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : gQoSGlobalInfo                                       */
/* Global Variables                                                          */
/* Modified           : gQoSGlobalInfo                                       */
/* Return Value(s)    : NULL / Pointer to  InPriMapNode                      */
/* Called By          : SNMP Low Level                                       */
/* Calling Function   :                                                      */
/*****************************************************************************/
tQosStdMinRateEntry *
QoSCreateStdMinRateTblEntry (UINT4 u4MinRateId)
{
    UINT4               u4RetStatus = QOS_FAILURE;
    tQosStdMinRateEntry *pNewMinRateNode = NULL;

    /* 1. Allocate a memory block for the New Entry */
    pNewMinRateNode = (tQosStdMinRateEntry *)
        MemAllocMemBlk (gQoSGlobalInfo.DsMinRatePoolId);
    if (pNewMinRateNode == NULL)
    {
        QOS_TRC_ARG2 (MGMT_TRC, "In %s : MemAllocMemBlk () Failed ,"
                      "for MinRate Id %d.\r\n", __FUNCTION__, u4MinRateId);
        return (NULL);
    }

    /* 2. Initialize the New Entry */
    MEMSET (pNewMinRateNode, 0, sizeof (tQosStdMinRateEntry));

    pNewMinRateNode->u4DSMinRateId = u4MinRateId;
    pNewMinRateNode->u4DSMinRatePriority = QOS_MIN_RATE_PRIORITY_DEFAULT;
    pNewMinRateNode->u4DSMinRateAbsolute = QOS_MIN_RATE_ABSOLUTE_DEFAULT;
    pNewMinRateNode->u4DSMinRateRelative = QOS_MIN_RATE_RELATIVE_DEFAULT;
    pNewMinRateNode->i4StorageType = QOS_STORAGE_NONVOLATILE;
    pNewMinRateNode->u1DSMinRateStatus = NOT_IN_SERVICE;

    /* 3. Add this New Entry into the Priority Map Table */
    u4RetStatus = RBTreeAdd (gQoSGlobalInfo.DsMinRateTbl,
                             (tRBElem *) pNewMinRateNode);
    if (u4RetStatus != RB_SUCCESS)
    {
        u4RetStatus =
            MemReleaseMemBlock (gQoSGlobalInfo.DsMinRatePoolId,
                                (UINT1 *) pNewMinRateNode);

        if (u4RetStatus == MEM_FAILURE)
        {
            QOS_TRC_ARG2 (MGMT_TRC, "In %s : MemReleaseMemBlock () Failed ,"
                          "for Min Rate Id %d. \r\n", __FUNCTION__,
                          u4MinRateId);

            return (NULL);
        }

        QOS_TRC_ARG2 (MGMT_TRC, "In %s : RBTreeAdd () Failed ,"
                      "for MinRate Id %d. \r\n", __FUNCTION__, u4MinRateId);
        return (NULL);
    }

    return (pNewMinRateNode);
}

/*****************************************************************************/
/* Function Name      : QoSCreateStdMaxRateTblEntry                          */
/* Description        : This function is used to Create an Entry in   the    */
/*                      MaxRate Table ,it will do the following Action       */
/*                      1. Allocate a memory block for the New Entry         */
/*                      2. Initialize the New Entry                          */
/*                      3. Add this New Entry into the Priority Map Table    */
/* Input(s)           : u4PriMapId      - Index to InPriMapNode              */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : gQoSGlobalInfo                                       */
/* Global Variables                                                          */
/* Modified           : gQoSGlobalInfo                                       */
/* Return Value(s)    : NULL / Pointer to  InPriMapNode                      */
/* Called By          : SNMP Low Level                                       */
/* Calling Function   :                                                      */
/*****************************************************************************/
tQosStdMaxRateEntry *
QoSCreateStdMaxRateTblEntry (UINT4 u4DSMaxRateId, UINT4 u4DSMaxRateLevel)
{
    UINT4               u4RetStatus = QOS_FAILURE;
    tQosStdMaxRateEntry *pNewMaxRateNode = NULL;

    /* 1. Allocate a memory block for the New Entry */
    pNewMaxRateNode = (tQosStdMaxRateEntry *)
        MemAllocMemBlk (gQoSGlobalInfo.DsMaxRatePoolId);
    if (pNewMaxRateNode == NULL)
    {
        QOS_TRC_ARG2 (MGMT_TRC, "In %s : MemAllocMemBlk () Failed ,"
                      "for MaxRate Id %d.\r\n", __FUNCTION__, u4DSMaxRateId);
        return (NULL);
    }

    /* 2. Initialize the New Entry */
    MEMSET (pNewMaxRateNode, 0, sizeof (tQosStdMaxRateEntry));

    pNewMaxRateNode->u4DSMaxRateId = u4DSMaxRateId;
    pNewMaxRateNode->u4DSMaxRateLevel = u4DSMaxRateLevel;
    pNewMaxRateNode->u4DSMaxRateAbsolute = QOS_MAX_RATE_ABSOLUTE_DEFAULT;
    pNewMaxRateNode->u4DSMaxRateRelative = QOS_MAX_RATE_ABSOLUTE_DEFAULT;
    pNewMaxRateNode->u4DSMaxRateThreshold = QOS_MAX_RATE_THRESHOLD_DEFAULT;
    pNewMaxRateNode->i4StorageType = QOS_STORAGE_NONVOLATILE;
    pNewMaxRateNode->u1DSMaxRateStatus = NOT_IN_SERVICE;

    /* 3. Add this New Entry into the Priority Map Table */
    u4RetStatus = RBTreeAdd (gQoSGlobalInfo.DsMaxRateTbl,
                             (tRBElem *) pNewMaxRateNode);
    if (u4RetStatus != RB_SUCCESS)
    {
        u4RetStatus =
            MemReleaseMemBlock (gQoSGlobalInfo.DsMaxRatePoolId,
                                (UINT1 *) pNewMaxRateNode);

        if (u4RetStatus == MEM_FAILURE)
        {
            QOS_TRC_ARG2 (MGMT_TRC, "In %s : MemReleaseMemBlock () Failed ,"
                          "for Max Rate Id %d. \r\n", __FUNCTION__,
                          u4DSMaxRateId);

            return (NULL);
        }

        QOS_TRC_ARG2 (MGMT_TRC, "In %s : RBTreeAdd () Failed ,"
                      "for MaxRate Id %d. \r\n", __FUNCTION__, u4DSMaxRateId);
        return (NULL);
    }

    return (pNewMaxRateNode);
}

/*****************************************************************************/
/* Function Name      : QoSDeleteStdSchedulerTblEntry                          */
/* Description        : This function is used to Delete an Entry from the    */
/*                      Scheduler Table ,it will do the following Action       */
/*                      1. Remove the Entry from the Scheduler Table           */
/*                      2. Clear the Removed Entry                           */
/*                      3. Release the Removed Entry's memory                */
/* Input(s)           : pSchedulerEntry - Pointer to the SchedulerEntry          */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : gQoSGlobalInfo                                       */
/* Global Variables                                                          */
/* Modified           : gQoSGlobalInfo                                       */
/* Return Value(s)    : QOS_SUCCESS or QOS_FAILURE                           */
/* Called By          : SNMP Low Level                                       */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QoSDeleteStdSchedulerTblEntry (tQosStdSchedulerEntry * pSchedulerEntry)
{
    tQosStdSchedulerEntry *pTmpSchedulerNode = NULL;

    pTmpSchedulerNode = RBTreeGet (gQoSGlobalInfo.DsSchedulerTbl,
                                   (tRBElem *) pSchedulerEntry);
    if (pTmpSchedulerNode != NULL)
    {
        pTmpSchedulerNode = RBTreeRem (gQoSGlobalInfo.DsSchedulerTbl,
                                       (tRBElem *) pSchedulerEntry);
        if (pTmpSchedulerNode == NULL)
        {
            QOS_TRC_ARG1 (MGMT_TRC, "In %s : RBTreeRem () Failed for "
                          "Scheduler. \r\n", __FUNCTION__);
            return (QOS_FAILURE);
        }
    }
    return (QOS_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSDeleteStdMinRateTblEntry                          */
/* Description        : This function is used to Delete an Entry from the    */
/*                      MinRate Table ,it will do the following Action       */
/*                      1. Remove the Entry from the MinRate Table           */
/*                      2. Clear the Removed Entry                           */
/*                      3. Release the Removed Entry's memory                */
/* Input(s)           : pMinRateEntry - Pointer to the MinRateEntry          */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : gQoSGlobalInfo                                       */
/* Global Variables                                                          */
/* Modified           : gQoSGlobalInfo                                       */
/* Return Value(s)    : QOS_SUCCESS or QOS_FAILURE                           */
/* Called By          : SNMP Low Level                                       */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QoSDeleteStdMinRateTblEntry (tQosStdMinRateEntry * pMinRateEntry)
{
    tQosStdMinRateEntry *pTmpMinRateNode = NULL;

    pTmpMinRateNode = RBTreeGet (gQoSGlobalInfo.DsMinRateTbl,
                                 (tRBElem *) pMinRateEntry);
    if (pTmpMinRateNode != NULL)
    {
        pTmpMinRateNode = RBTreeRem (gQoSGlobalInfo.DsMinRateTbl,
                                     (tRBElem *) pMinRateEntry);
        if (pTmpMinRateNode == NULL)
        {
            QOS_TRC_ARG1 (MGMT_TRC, "In %s : RBTreeRem () Failed for "
                          "MinRate. \r\n", __FUNCTION__);
            return (QOS_FAILURE);
        }
    }
    return (QOS_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSDeleteStdMaxRateTblEntry                          */
/* Description        : This function is used to Delete an Entry from the    */
/*                      MaxRate Table ,it will do the following Action       */
/*                      1. Remove the Entry from the MaxRate Table           */
/*                      2. Clear the Removed Entry                           */
/*                      3. Release the Removed Entry's memory                */
/* Input(s)           : pMaxRateEntry - Pointer to the MaxRateEntry          */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : gQoSGlobalInfo                                       */
/* Global Variables                                                          */
/* Modified           : gQoSGlobalInfo                                       */
/* Return Value(s)    : QOS_SUCCESS or QOS_FAILURE                           */
/* Called By          : SNMP Low Level                                       */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QoSDeleteStdMaxRateTblEntry (tQosStdMaxRateEntry * pMaxRateEntry)
{
    tQosStdMaxRateEntry *pTmpMaxRateNode = NULL;

    pTmpMaxRateNode = RBTreeGet (gQoSGlobalInfo.DsMaxRateTbl,
                                 (tRBElem *) pMaxRateEntry);
    if (pTmpMaxRateNode != NULL)
    {
        pTmpMaxRateNode = RBTreeRem (gQoSGlobalInfo.DsMaxRateTbl,
                                     (tRBElem *) pMaxRateEntry);
        if (pTmpMaxRateNode == NULL)
        {
            QOS_TRC_ARG1 (MGMT_TRC, "In %s : RBTreeRem () Failed for "
                          "MaxRate. \r\n", __FUNCTION__);
            return (QOS_FAILURE);
        }
    }
    return (QOS_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSCreateActionTblEntry                              */
/* Description        : This function is used to Create an Entry in   the    */
/*                      Action Table ,it will do the following Action        */
/*                      1. Allocate a memory block for the New Entry         */
/*                      2. Initialize the New Entry                          */
/*                      3. Add this New Entry into the Action Table          */
/* Input(s)           : u4ActionId      - Index to ActionNode                */
/* Output(s)          : None.                                                */
/* Returns            : Pointer to Action entry                              */
/*****************************************************************************/
tQosStdActionEntry *
QoSCreateActionTblEntry (UINT4 u4ActionId)
{
    UINT4               u4RetStatus = QOS_FAILURE;
    tQosStdActionEntry *pActionNode = NULL;

    /* 1. Allocate a memory block for the New Entry */
    pActionNode = (tQosStdActionEntry *)
        MemAllocMemBlk (gQoSGlobalInfo.DsActionPoolId);
    if (pActionNode == NULL)
    {
        QOS_TRC_ARG2 (MGMT_TRC, "In %s : MemAllocMemBlk () Failed, "
                      "for ActionId %d. \r\n", __FUNCTION__, u4ActionId);

        return (NULL);
    }

    /* 2. Initialize the New Entry */
    MEMSET (pActionNode, 0, sizeof (tQosStdActionEntry));

    pActionNode->u4DSActionId = u4ActionId;
    pActionNode->i4StorageType = QOS_STORAGE_NONVOLATILE;
    pActionNode->u1DSActionStatus = NOT_READY;

    /* 3. Add this New Entry into the Meter Table */
    u4RetStatus = RBTreeAdd (gQoSGlobalInfo.DsActionTbl,
                             (tRBElem *) pActionNode);
    if (u4RetStatus != RB_SUCCESS)
    {
        u4RetStatus = MemReleaseMemBlock (gQoSGlobalInfo.DsActionPoolId,
                                          (UINT1 *) pActionNode);
        if (u4RetStatus == MEM_FAILURE)
        {
            QOS_TRC_ARG1 (MGMT_TRC, "In %s : MemReleaseMemBlock () "
                          "Failed.\r\n", __FUNCTION__);

            return (NULL);
        }

        QOS_TRC_ARG2 (MGMT_TRC, "In %s : RBTreeAdd () Failed ,"
                      "for MeterId %d. \r\n", __FUNCTION__, u4ActionId);
        return (NULL);
    }

    return (pActionNode);
}

/*****************************************************************************/
/* Function Name      : QoSDeleteActionTblEntry                               */
/* Description        : This function is used to Delete an Entry from the    */
/*                      Action Table ,it will do the following Action     */
/*                      1. Remove the Entry from the action Table             */
/*                      2. Decrement the RefCount of action Table Entry       */
/*                      3. Clear the Removed Entry                           */
/*                      4. Release the Removed Entry's memory                */
/* Input(s)           : pActionNode   - Pointer to MeterNode                  */
/* Output(s)          : None.                                                */
/* Return Value(s)    : QOS_SUCCESS or QOS_FAILURE                           */
/*****************************************************************************/
INT4
QoSDeleteActionTblEntry (tQosStdActionEntry * pActionNode)
{
    UINT4               u4RetStatus = QOS_FAILURE;
    tQosStdActionEntry *pDelActionNode = NULL;

    if (pActionNode == NULL)
    {
        return (QOS_FAILURE);
    }
    pDelActionNode = RBTreeRem (gQoSGlobalInfo.DsActionTbl,
                                (tRBElem *) pActionNode);
    if (pDelActionNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : RBTreeRem () Failed. \r\n",
                      __FUNCTION__);

        return (QOS_FAILURE);
    }

    MEMSET (pDelActionNode, 0, sizeof (tQosStdActionEntry));

    u4RetStatus = MemReleaseMemBlock (gQoSGlobalInfo.DsActionPoolId,
                                      (UINT1 *) pDelActionNode);
    if (u4RetStatus == MEM_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : MemReleaseMemBlock () Failed.\r\n",
                      __FUNCTION__);

        return (QOS_FAILURE);
    }

    return (QOS_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSCreateCountActTblEntry                            */
/* Description        : This function is used to Create an Entry in   the    */
/*                      CountAction Table ,it will do the following Action   */
/*                      1. Allocate a memory block for the New Entry         */
/*                      2. Initialize the New Entry                          */
/*                      3. Add this New Entry into the countact Table        */
/* Input(s)           : u4ActionId      - Index to ActionNode                */
/* Output(s)          : None.                                                */
/* Returns            : Pointer to Action entry                              */
/*****************************************************************************/
tQosStdCountActEntry *
QoSCreateCountActTblEntry (UINT4 u4CountActId)
{
    UINT4               u4RetStatus = QOS_FAILURE;
    tQosStdCountActEntry *pCountActNode = NULL;

    /* 1. Allocate a memory block for the New Entry */
    pCountActNode = (tQosStdCountActEntry *)
        MemAllocMemBlk (gQoSGlobalInfo.DsCountActPoolId);
    if (pCountActNode == NULL)
    {
        QOS_TRC_ARG2 (MGMT_TRC, "In %s : MemAllocMemBlk () Failed, "
                      "for CountActId %d. \r\n", __FUNCTION__, u4CountActId);

        return (NULL);
    }

    /* 2. Initialize the New Entry */
    MEMSET (pCountActNode, 0, sizeof (tQosStdCountActEntry));

    pCountActNode->u4DSCountActId = u4CountActId;
    pCountActNode->i4StorageType = QOS_STORAGE_NONVOLATILE;
    pCountActNode->u1DSCountActStatus = NOT_IN_SERVICE;

    /* 3. Add this New Entry into the Meter Table */
    u4RetStatus = RBTreeAdd (gQoSGlobalInfo.DsCountActTbl,
                             (tRBElem *) pCountActNode);
    if (u4RetStatus != RB_SUCCESS)
    {
        u4RetStatus = MemReleaseMemBlock (gQoSGlobalInfo.DsCountActPoolId,
                                          (UINT1 *) pCountActNode);
        if (u4RetStatus == MEM_FAILURE)
        {
            QOS_TRC_ARG1 (MGMT_TRC, "In %s : MemReleaseMemBlock () "
                          "Failed.\r\n", __FUNCTION__);

            return (NULL);
        }

        QOS_TRC_ARG2 (MGMT_TRC, "In %s : RBTreeAdd () Failed ,"
                      "for u4CountActId %d. \r\n", __FUNCTION__, u4CountActId);
        return (NULL);
    }

    return (pCountActNode);
}

/*****************************************************************************/
/* Function Name      : QoSDeleteCountActTblEntry                            */
/* Description        : This function is used to Delete an Entry from the    */
/*                      CountAct Table ,it will do the following Action      */
/*                      1. Remove the Entry from the countact Table          */
/*                      2. Decrement the RefCount of countact Table Entry    */
/*                      3. Clear the Removed Entry                           */
/*                      4. Release the Removed Entry's memory                */
/* Input(s)           : pCountActNode   - Pointer to countactnode            */
/* Output(s)          : None.                                                */
/* Return Value(s)    : QOS_SUCCESS or QOS_FAILURE                           */
/*****************************************************************************/
INT4
QoSDeleteCountActTblEntry (tQosStdCountActEntry * pCountActNode)
{
    UINT4               u4RetStatus = QOS_FAILURE;
    tQosStdCountActEntry *pDelCountActNode = NULL;

    if (pCountActNode == NULL)
    {
        return (QOS_FAILURE);
    }

    pDelCountActNode = RBTreeRem (gQoSGlobalInfo.DsCountActTbl,
                                  (tRBElem *) pCountActNode);
    if (pDelCountActNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : RBTreeRem () Failed. \r\n",
                      __FUNCTION__);

        return (QOS_FAILURE);
    }

    MEMSET (pDelCountActNode, 0, sizeof (tQosStdCountActEntry));

    u4RetStatus = MemReleaseMemBlock (gQoSGlobalInfo.DsCountActPoolId,
                                      (UINT1 *) pDelCountActNode);
    if (u4RetStatus == MEM_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : MemReleaseMemBlock () Failed.\r\n",
                      __FUNCTION__);

        return (QOS_FAILURE);
    }

    return (QOS_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSDataPathEntryNodeFn                             */
/* Description        : This function is used to Delete the Scheduler Table  */
/*                      Entries and Release the Memory to the respective     */
/*                      Mem Pool .                                           */
/* Input(s)           : tRBElem     - Pointer to Scheduler Table Entry       */
/*                      u4Arg       - Unused Param                           */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : QOS_SUCCESS /QOS_FAILURE                             */
/* Called By          : SNMP                                                 */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QoSDataPathEntryNodeFn (tRBElem * pRBElem, UINT4 u4Arg)
{
    UINT4               u4RetStatus = MEM_FAILURE;

    UNUSED_PARAM (u4Arg);

    /* Release the Mem Block */
    u4RetStatus = MemReleaseMemBlock (gQoSGlobalInfo.DsDataPathPoolId,
                                      (UINT1 *) pRBElem);
    if (u4RetStatus != MEM_SUCCESS)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : DataPathTbl- "
                      "MemReleaseMemBlock () Failed.\r\n", __FUNCTION__);
    }
    return (QOS_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSClfrNodeFn                             */
/* Description        : This function is used to Delete the Scheduler Table  */
/*                      Entries and Release the Memory to the respective     */
/*                      Mem Pool .                                           */
/* Input(s)           : tRBElem     - Pointer to Scheduler Table Entry       */
/*                      u4Arg       - Unused Param                           */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : QOS_SUCCESS /QOS_FAILURE                             */
/* Called By          : SNMP                                                 */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QoSClfrNodeFn (tRBElem * pRBElem, UINT4 u4Arg)
{
    UINT4               u4RetStatus = MEM_FAILURE;

    UNUSED_PARAM (u4Arg);

    /* Release the Mem Block */
    u4RetStatus = MemReleaseMemBlock (gQoSGlobalInfo.DsClfrPoolId,
                                      (UINT1 *) pRBElem);
    if (u4RetStatus != MEM_SUCCESS)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : ClfrTbl- "
                      "MemReleaseMemBlock () Failed.\r\n", __FUNCTION__);
    }
    return (QOS_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSClfrElementNodeFn                             */
/* Description        : This function is used to Delete the Scheduler Table  */
/*                      Entries and Release the Memory to the respective     */
/*                      Mem Pool .                                           */
/* Input(s)           : tRBElem     - Pointer to Scheduler Table Entry       */
/*                      u4Arg       - Unused Param                           */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : QOS_SUCCESS /QOS_FAILURE                             */
/* Called By          : SNMP                                                 */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QoSClfrElementNodeFn (tRBElem * pRBElem, UINT4 u4Arg)
{
    UINT4               u4RetStatus = MEM_FAILURE;

    UNUSED_PARAM (u4Arg);

    /* Release the Mem Block */
    u4RetStatus = MemReleaseMemBlock (gQoSGlobalInfo.DsClfrElementPoolId,
                                      (UINT1 *) pRBElem);
    if (u4RetStatus != MEM_SUCCESS)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : ClfrElementTbl- "
                      "MemReleaseMemBlock () Failed.\r\n", __FUNCTION__);
    }
    return (QOS_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSStdMeterNodeFn                             */
/* Description        : This function is used to Delete the Scheduler Table  */
/*                      Entries and Release the Memory to the respective     */
/*                      Mem Pool .                                           */
/* Input(s)           : tRBElem     - Pointer to Scheduler Table Entry       */
/*                      u4Arg       - Unused Param                           */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : QOS_SUCCESS /QOS_FAILURE                             */
/* Called By          : SNMP                                                 */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QoSStdMeterNodeFn (tRBElem * pRBElem, UINT4 u4Arg)
{
    UINT4               u4RetStatus = MEM_FAILURE;

    UNUSED_PARAM (u4Arg);

    /* Release the Mem Block */
    u4RetStatus = MemReleaseMemBlock (gQoSGlobalInfo.DsMeterPoolId,
                                      (UINT1 *) pRBElem);
    if (u4RetStatus != MEM_SUCCESS)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : MeterTbl- "
                      "MemReleaseMemBlock () Failed.\r\n", __FUNCTION__);
    }
    return (QOS_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSActionNodeFn                             */
/* Description        : This function is used to Delete the Scheduler Table  */
/*                      Entries and Release the Memory to the respective     */
/*                      Mem Pool .                                           */
/* Input(s)           : tRBElem     - Pointer to Scheduler Table Entry       */
/*                      u4Arg       - Unused Param                           */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : QOS_SUCCESS /QOS_FAILURE                             */
/* Called By          : SNMP                                                 */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QoSActionNodeFn (tRBElem * pRBElem, UINT4 u4Arg)
{
    UINT4               u4RetStatus = MEM_FAILURE;

    UNUSED_PARAM (u4Arg);

    /* Release the Mem Block */
    u4RetStatus = MemReleaseMemBlock (gQoSGlobalInfo.DsActionPoolId,
                                      (UINT1 *) pRBElem);
    if (u4RetStatus != MEM_SUCCESS)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : ActionTbl- "
                      "MemReleaseMemBlock () Failed.\r\n", __FUNCTION__);
    }
    return (QOS_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSCountActNodeFn                             */
/* Description        : This function is used to Delete the Scheduler Table  */
/*                      Entries and Release the Memory to the respective     */
/*                      Mem Pool .                                           */
/* Input(s)           : tRBElem     - Pointer to Scheduler Table Entry       */
/*                      u4Arg       - Unused Param                           */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : QOS_SUCCESS /QOS_FAILURE                             */
/* Called By          : SNMP                                                 */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QoSCountActNodeFn (tRBElem * pRBElem, UINT4 u4Arg)
{
    UINT4               u4RetStatus = MEM_FAILURE;

    UNUSED_PARAM (u4Arg);

    /* Release the Mem Block */
    u4RetStatus = MemReleaseMemBlock (gQoSGlobalInfo.DsCountActPoolId,
                                      (UINT1 *) pRBElem);
    if (u4RetStatus != MEM_SUCCESS)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : CountActTbl- "
                      "MemReleaseMemBlock () Failed.\r\n", __FUNCTION__);
    }
    return (QOS_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSQueueNodeFn                             */
/* Description        : This function is used to Delete the Scheduler Table  */
/*                      Entries and Release the Memory to the respective     */
/*                      Mem Pool .                                           */
/* Input(s)           : tRBElem     - Pointer to Scheduler Table Entry       */
/*                      u4Arg       - Unused Param                           */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : QOS_SUCCESS /QOS_FAILURE                             */
/* Called By          : SNMP                                                 */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QoSQueueNodeFn (tRBElem * pRBElem, UINT4 u4Arg)
{
    UINT4               u4RetStatus = MEM_FAILURE;

    UNUSED_PARAM (u4Arg);

    /* Release the Mem Block */
    u4RetStatus = MemReleaseMemBlock (gQoSGlobalInfo.DsQPoolId,
                                      (UINT1 *) pRBElem);
    if (u4RetStatus != MEM_SUCCESS)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QTbl- "
                      "MemReleaseMemBlock () Failed.\r\n", __FUNCTION__);
    }
    return (QOS_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSSchedulerNodeFn                             */
/* Description        : This function is used to Delete the Scheduler Table  */
/*                      Entries and Release the Memory to the respective     */
/*                      Mem Pool .                                           */
/* Input(s)           : tRBElem     - Pointer to Scheduler Table Entry       */
/*                      u4Arg       - Unused Param                           */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : QOS_SUCCESS /QOS_FAILURE                             */
/* Called By          : SNMP                                                 */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QoSSchedulerNodeFn (tRBElem * pRBElem, UINT4 u4Arg)
{
    UINT4               u4RetStatus = MEM_FAILURE;

    UNUSED_PARAM (u4Arg);

    /* Release the Mem Block */
    u4RetStatus = MemReleaseMemBlock (gQoSGlobalInfo.DsQosSchedulerPoolId,
                                      (UINT1 *) pRBElem);
    if (u4RetStatus != MEM_SUCCESS)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : SchedulerTbl- "
                      "MemReleaseMemBlock () Failed.\r\n", __FUNCTION__);
    }
    return (QOS_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSMinRateNodeFn                             */
/* Description        : This function is used to Delete the Scheduler Table  */
/*                      Entries and Release the Memory to the respective     */
/*                      Mem Pool .                                           */
/* Input(s)           : tRBElem     - Pointer to Scheduler Table Entry       */
/*                      u4Arg       - Unused Param                           */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : QOS_SUCCESS /QOS_FAILURE                             */
/* Called By          : SNMP                                                 */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QoSMinRateNodeFn (tRBElem * pRBElem, UINT4 u4Arg)
{
    UINT4               u4RetStatus = MEM_FAILURE;

    UNUSED_PARAM (u4Arg);

    /* Release the Mem Block */
    u4RetStatus = MemReleaseMemBlock (gQoSGlobalInfo.DsMinRatePoolId,
                                      (UINT1 *) pRBElem);
    if (u4RetStatus != MEM_SUCCESS)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : MinRateTbl- "
                      "MemReleaseMemBlock () Failed.\r\n", __FUNCTION__);
    }
    return (QOS_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSMaxRateNodeFn                             */
/* Description        : This function is used to Delete the Scheduler Table  */
/*                      Entries and Release the Memory to the respective     */
/*                      Mem Pool .                                           */
/* Input(s)           : tRBElem     - Pointer to Scheduler Table Entry       */
/*                      u4Arg       - Unused Param                           */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : QOS_SUCCESS /QOS_FAILURE                             */
/* Called By          : SNMP                                                 */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QoSMaxRateNodeFn (tRBElem * pRBElem, UINT4 u4Arg)
{
    UINT4               u4RetStatus = MEM_FAILURE;

    UNUSED_PARAM (u4Arg);

    /* Release the Mem Block */
    u4RetStatus = MemReleaseMemBlock (gQoSGlobalInfo.DsMaxRatePoolId,
                                      (UINT1 *) pRBElem);
    if (u4RetStatus != MEM_SUCCESS)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : MaxRateTbl- "
                      "MemReleaseMemBlock () Failed.\r\n", __FUNCTION__);
    }
    return (QOS_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSSchedMapNodeFn                             */
/* Description        : This function is used to Delete the Scheduler Table  */
/*                      Entries and Release the Memory to the respective     */
/*                      Mem Pool .                                           */
/* Input(s)           : tRBElem     - Pointer to Scheduler Map Table Entry       */
/*                      u4Arg       - Unused Param                           */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : QOS_SUCCESS /QOS_FAILURE                             */
/* Called By          : SNMP                                                 */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QoSSchedMapNodeFn (tRBElem * pRBElem, UINT4 u4Arg)
{
    UINT4               u4RetStatus = MEM_FAILURE;

    UNUSED_PARAM (u4Arg);

    /* Release the Mem Block */
    u4RetStatus = MemReleaseMemBlock (gQoSGlobalInfo.QoSSchedulerMapPoolId,
                                      (UINT1 *) pRBElem);
    if (u4RetStatus != MEM_SUCCESS)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : ShedMapeTbl- "
                      "MemReleaseMemBlock () Failed.\r\n", __FUNCTION__);
    }
    return (QOS_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QosUtlValidateDataPathEntry                          */
/* Description        : This function is used to Validate the Given Index    */
/*                      instance in the Data Path Table, if it is ACTIVE     */
/*                      return Failure Success otherwise.                    */
/* Input(s)           : i4IfIndex - Interface Index                          */
/*                    : i4DiffServDataPathIfDirection - Direction            */
/*                      pu4ErrorCode     - Pointer to Error Code             */
/* Output(s)          : None.                                                */
/* Return Value(s)    : QOS_SUCCESS or QOS_FAILURE                           */
/*****************************************************************************/
INT4
QosUtlValidateDataPathEntry (INT4 i4IfIndex, INT4 i4DiffServDataPathIfDirection,
                             UINT4 *pu4ErrorCode)
{
    tQosStdDataPathEntry *pDataPathEntry = NULL;

    if (gQoSGlobalInfo.eSysControl != QOS_SYS_CNTL_START)
    {
        QOS_TRC (MGMT_TRC, QOS_MODULE_SHUTDOW_ERROR_MSG);

        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return (QOS_FAILURE);
    }
    if (CfaValidateIfIndex (i4IfIndex) == CFA_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC,
                      "In %s : QosUtlValidateDataPathEntry () Returns "
                      "FAILURE. \r\n", __FUNCTION__);

        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return QOS_FAILURE;
    }
    if ((i4DiffServDataPathIfDirection != QOS_DATA_IN) &&
        (i4DiffServDataPathIfDirection != QOS_DATA_OUT))
    {
        QOS_TRC (MGMT_TRC, "Direction value is not correct\r\n");

        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return QOS_FAILURE;
    }
    if (QOS_DATA_PATH_ID_VALID (i4IfIndex) != QOS_SUCCESS)
    {
        QOS_TRC_ARG3 (MGMT_TRC,
                      "%s : DataPath Interface  Id %d is out of Range."
                      " The Range Should be (1-%d). \r\n", __FUNCTION__,
                      i4IfIndex, QOS_DATA_PATH_MAX);

        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return QOS_FAILURE;
    }
    pDataPathEntry = QosGetDataPathEntry (i4IfIndex,
                                          i4DiffServDataPathIfDirection);
    if (pDataPathEntry == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetPriorityMapNode () Returns "
                      "FAILURE. \r\n", __FUNCTION__);

        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return QOS_FAILURE;
    }
    if (pDataPathEntry->u1DataPathStatus == ACTIVE)
    {
        QOS_TRC (MGMT_TRC, "Can not change the value in ACTIVE State.\r\n");

        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return QOS_FAILURE;
    }
    return (QOS_SUCCESS);

}

/*****************************************************************************/
/* Function Name      : QosUtlValidateClfrEntry                              */
/* Description        : This function is used to Validate the Given Index    */
/*                      instance in the Classfier Table, if it is ACTIVE     */
/*                      return Failure Success otherwise.                    */
/* Input(s)           : u4DiffServClfrId - Classifier Index                  */
/*                      pu4ErrorCode     - Pointer to Error Code             */
/* Output(s)          : None.                                                */
/* Return Value(s)    : QOS_SUCCESS or QOS_FAILURE                           */
/*****************************************************************************/
INT4
QosUtlValidateClfrEntry (UINT4 u4DiffServClfrId, UINT4 *pu4ErrorCode)
{
    tQosStdClfrEntry   *pClfrEntry = NULL;

    if (gQoSGlobalInfo.eSysControl != QOS_SYS_CNTL_START)
    {
        QOS_TRC (MGMT_TRC, QOS_MODULE_SHUTDOW_ERROR_MSG);

        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return QOS_FAILURE;
    }
    if (QOS_CLFR_ID_VALID (u4DiffServClfrId) == QOS_FAILURE)
    {
        QOS_TRC_ARG3 (MGMT_TRC, "%s : Classifier  Id %d is out of Range."
                      " The Range Should be (1-%d). \r\n", __FUNCTION__,
                      u4DiffServClfrId, QOS_CLFR_MAX);

        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return QOS_FAILURE;
    }
    pClfrEntry = QosGetClfrEntry (u4DiffServClfrId);

    if (pClfrEntry == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QosGetClfrEntry () Returns "
                      "FAILURE. \r\n", __FUNCTION__);

        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return QOS_FAILURE;
    }
    if (pClfrEntry->u1DsClfrStatus == QOS_ACTIVE)
    {
        QOS_TRC (MGMT_TRC, "Can not change the value in ACTIVE State.\r\n");

        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return QOS_FAILURE;
    }
    return QOS_SUCCESS;

}

/*****************************************************************************/
/* Function Name      : QosUtlValidateClfrElementEntry                       */
/* Description        : This function is used to Validate the Given Index    */
/*                      instance in the Classfier Elem Table, if it is ACTIVE*/
/*                      return Failure Success otherwise.                    */
/* Input(s)           : u4DiffServClfrElemId: Classifier Element Id          */
/*                    : u4DiffServClfrId:     Classifier Id                  */
/*                      pu4ErrorCode     - Pointer to Error Code             */
/* Output(s)          : None.                                                */
/* Return Value(s)    : QOS_SUCCESS or QOS_FAILURE                           */
/*****************************************************************************/
INT4
QosUtlValidateClfrElementEntry (UINT4 u4DiffServClfrId,
                                UINT4 u4DiffServClfrElemId, UINT4 *pu4ErrorCode)
{
    tQosStdClfrElementEntry *pClfrElementEntry = NULL;

    tQosStdClfrEntry   *pClfrEntry = NULL;

    if (QOS_CLFR_ID_VALID (u4DiffServClfrId) == QOS_FAILURE)
    {
        QOS_TRC_ARG3 (MGMT_TRC, "%s : Classifier  Id %d is out of Range."
                      " The Range Should be (1-%d). \r\n", __FUNCTION__,
                      u4DiffServClfrId, QOS_CLFR_MAX);

        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return QOS_FAILURE;
    }
    pClfrEntry = QosGetClfrEntry (u4DiffServClfrId);

    if (pClfrEntry == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QosGetClfrEntry () Returns "
                      "FAILURE. \r\n", __FUNCTION__);

        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return QOS_FAILURE;
    }

    if (QOS_CLFR_ELEMENT_ID_VALID (u4DiffServClfrId, u4DiffServClfrElemId)
        != QOS_SUCCESS)
    {
        /*If u4DiffServClfrId is out of range trace would have got
         * printed in the function called before.So trace is printed
         * only for u4DiffServClfrElementId*/
        QOS_TRC_ARG3 (MGMT_TRC, "%s : Classifier Element Id %d is out of Range."
                      " The Range Should be (1-%d). \r\n", __FUNCTION__,
                      u4DiffServClfrElemId, QOS_CLFR_ELEMENT_PREC_MAX);

        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return QOS_FAILURE;
    }
    pClfrElementEntry = QosGetClfrElementEntry (u4DiffServClfrId,
                                                u4DiffServClfrElemId);
    if (pClfrElementEntry == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QosGetClfrElementEntry () Returns "
                      "FAILURE. \r\n", __FUNCTION__);

        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return QOS_FAILURE;
    }
    if (pClfrElementEntry->u1DSClfrElementStatus == QOS_ACTIVE)
    {
        QOS_TRC (MGMT_TRC, "Can not change the value in ACTIVE State.\r\n");

        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return QOS_FAILURE;
    }
    return QOS_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : QosUtlValidateMultiFieldClfrEntry                    */
/* Description        : This function is used to Validate the Given Index    */
/*                      instance in the MultiField Classfier Table,          */
/*                      if it is ACTIVE                                      */
/*                      return Failure Success otherwise.                    */
/* Input(s)           : u4DiffServMultiFieldClfrId: Classifier Element Id    */
/*                      pu4ErrorCode     - Pointer to Error Code             */
/* Output(s)          : None.                                                */
/* Return Value(s)    : QOS_SUCCESS or QOS_FAILURE                           */
/*****************************************************************************/
INT4
QosUtlValidateMultiFieldClfrEntry (UINT4 u4DiffServMultiFieldClfrId,
                                   tIssL3FilterEntry * pIssL3FilterEntry,
                                   UINT4 *pu4ErrorCode)
{

    if (gQoSGlobalInfo.eSysControl != QOS_SYS_CNTL_START)
    {
        QOS_TRC (MGMT_TRC, QOS_MODULE_SHUTDOW_ERROR_MSG);
        return (QOS_FAILURE);
    }
    if (QOS_L3FILTER_ID_VALID (u4DiffServMultiFieldClfrId) == QOS_FAILURE)
    {
        QOS_TRC_ARG3 (MGMT_TRC,
                      "%s : MultiField Classifier Id %d is out of Range."
                      " The Range Should be (1-%d). \r\n", __FUNCTION__,
                      u4DiffServMultiFieldClfrId, QOS_MAX_FILTER_ID);

        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return QOS_FAILURE;
    }
    if (pIssL3FilterEntry->u1IssL3FilterStatus == QOS_ACTIVE)
    {
        QOS_TRC (MGMT_TRC, "Can not change the value in ACTIVE State.\r\n");

        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return QOS_FAILURE;
    }
    return QOS_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : QosUtlValidateMeterEntry                             */
/* Description        : This function is used to Validate the Given Index    */
/*                      instance in the Meter Table,                         */
/*                      if it is ACTIVE                                      */
/*                      return Failure Success otherwise.                    */
/* Input(s)           : u4DsMeterId: Meter Id                                */
/*                      pu4ErrorCode     - Pointer to Error Code             */
/* Output(s)          : None.                                                */
/* Return Value(s)    : QOS_SUCCESS or QOS_FAILURE                           */
/*****************************************************************************/
INT4
QosUtlValidateMeterEntry (UINT4 u4DsMeterId, UINT4 *pu4ErrorCode)
{
    tQosStdMeterEntry  *pMeterNode = NULL;

    pMeterNode = QoSUtlGetStdMeterNode (u4DsMeterId);
    if (pMeterNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetMeterNode () "
                      " Returns FAILURE. \r\n", __FUNCTION__);
        return (QOS_FAILURE);
    }
    if (gQoSGlobalInfo.eSysControl != QOS_SYS_CNTL_START)
    {
        QOS_TRC (MGMT_TRC, QOS_MODULE_SHUTDOW_ERROR_MSG);
        return (QOS_FAILURE);
    }
    if ((u4DsMeterId > 0) && (u4DsMeterId <= QOS_METER_TBL_MAX_INDEX_RANGE))
    {
        /*nothing to do */
    }
    else
    {
        QOS_TRC_ARG3 (MGMT_TRC, "%s : Meter Id %d is out of Range."
                      " The Range Should be (1-%d). \r\n", __FUNCTION__,
                      u4DsMeterId, QOS_METER_TBL_MAX_INDEX_RANGE);

        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return QOS_FAILURE;
    }
    if (pMeterNode->u1DSMeterStatus == QOS_ACTIVE)
    {
        QOS_TRC (MGMT_TRC, "Can not change the value in ACTIVE State.\r\n");

        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return QOS_FAILURE;
    }
    return QOS_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : QosUtlValidateTBParamEntry                           */
/* Description        : This function is used to Validate the Given Index    */
/*                      instance in the TB Param Table,                      */
/*                      if it is ACTIVE                                      */
/*                      return Failure Success otherwise.                    */
/* Input(s)           : u4DsTBParamId: TBParam  Id                           */
/*                      pu4ErrorCode     - Pointer to Error Code             */
/* Output(s)          : None.                                                */
/* Return Value(s)    : QOS_SUCCESS or QOS_FAILURE                           */
/*****************************************************************************/
INT4
QosUtlValidateTBParamEntry (UINT4 u4DsTBParamId, UINT4 *pu4ErrorCode)
{
    tQoSMeterNode      *pMeterNode = NULL;

    if (gQoSGlobalInfo.eSysControl != QOS_SYS_CNTL_START)
    {
        QOS_TRC (MGMT_TRC, QOS_MODULE_SHUTDOW_ERROR_MSG);
        return (QOS_FAILURE);
    }
    pMeterNode = QoSUtlGetMeterNode (u4DsTBParamId);
    if (pMeterNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetMeterNode () "
                      " Returns FAILURE. \r\n", __FUNCTION__);
        return (QOS_FAILURE);
    }
    if (QOS_CHECK_METER_TBL_INDEX_RANGE (u4DsTBParamId) == QOS_FAILURE)
    {
        QOS_TRC_ARG3 (MGMT_TRC, "%s : TBParam Id %d is out of Range."
                      " The Range Should be (1-%d). \r\n", __FUNCTION__,
                      u4DsTBParamId, QOS_METER_TBL_MAX_INDEX_RANGE);

        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return QOS_FAILURE;
    }
    if (pMeterNode->u1Status == QOS_ACTIVE)
    {
        QOS_TRC (MGMT_TRC, "Can not change the value in ACTIVE State.\r\n");

        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return QOS_FAILURE;
    }
    return QOS_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : QosUtlValidateActionEntry                            */
/* Description        : This function is used to Validate the Given Index    */
/*                      instance in the Action Table,                        */
/*                      if it is ACTIVE                                      */
/*                      return Failure Success otherwise.                    */
/* Input(s)           : u4DsMeterId: Meter Id                                */
/*                      pu4ErrorCode     - Pointer to Error Code             */
/* Output(s)          : None.                                                */
/* Return Value(s)    : QOS_SUCCESS or QOS_FAILURE                           */
/*****************************************************************************/
INT4
QosUtlValidateActionEntry (UINT4 u4DiffServActionId, UINT4 *pu4ErrorCode)
{
    tQosStdActionEntry *pActionNode = NULL;

    if (gQoSGlobalInfo.eSysControl != QOS_SYS_CNTL_START)
    {
        QOS_TRC (MGMT_TRC, QOS_MODULE_SHUTDOW_ERROR_MSG);
        return (QOS_FAILURE);
    }
    pActionNode = QoSUtlGetActionNode (u4DiffServActionId);
    if (pActionNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetActionNode () "
                      " Returns FAILURE. \r\n", __FUNCTION__);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (QOS_FAILURE);
    }
    if (QOS_CHECK_ACTION_TBL_INDEX_RANGE (u4DiffServActionId) == QOS_FAILURE)
    {
        QOS_TRC_ARG3 (MGMT_TRC, "%s : Action Id %d is out of Range."
                      " The Range Should be (1-%d). \r\n", __FUNCTION__,
                      u4DiffServActionId, QOS_ACTION_TBL_MAX_INDEX_RANGE);

        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return QOS_FAILURE;
    }
    if (pActionNode->u1DSActionStatus == QOS_ACTIVE)
    {
        QOS_TRC (MGMT_TRC, "Can not change the value in ACTIVE State.\r\n");

        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return QOS_FAILURE;
    }
    return QOS_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : QosUtlValidateCountActEntry                          */
/* Description        : This function is used to Validate the Given Index    */
/*                      instance in the Action Table,                        */
/*                      if it is ACTIVE                                      */
/*                      return Failure Success otherwise.                    */
/* Input(s)           : u4DsMeterId: Meter Id                                */
/*                      pu4ErrorCode     - Pointer to Error Code             */
/* Output(s)          : None.                                                */
/* Return Value(s)    : QOS_SUCCESS or QOS_FAILURE                           */
/*****************************************************************************/
INT4
QosUtlValidateCountActEntry (UINT4 u4DiffServCountActId, UINT4 *pu4ErrorCode)
{
    tQosStdCountActEntry *pCountActEntry = NULL;

    if (gQoSGlobalInfo.eSysControl != QOS_SYS_CNTL_START)
    {
        QOS_TRC (MGMT_TRC, QOS_MODULE_SHUTDOW_ERROR_MSG);
        return (QOS_FAILURE);
    }
    pCountActEntry = QoSUtlGetCountActionNode (u4DiffServCountActId);
    if (pCountActEntry == NULL)
    {
        return QOS_FAILURE;
    }
    if (QOS_CHECK_COUNT_ACT_TBL_INDEX_RANGE (u4DiffServCountActId)
        == QOS_FAILURE)
    {
        QOS_TRC_ARG3 (MGMT_TRC, "%s : Count Act Id %d is out of Range."
                      " The Range Should be (1-%d). \r\n", __FUNCTION__,
                      u4DiffServCountActId, QOS_COUNT_ACT_TBL_MAX_INDEX_RANGE);

        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return QOS_FAILURE;
    }
    if (pCountActEntry->u1DSCountActStatus == QOS_ACTIVE)
    {
        QOS_TRC (MGMT_TRC, "Can not change the value in ACTIVE State.\r\n");

        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return QOS_FAILURE;
    }
    return QOS_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : QosUtlValidateAlgDropEntry                           */
/* Description        : This function is used to Validate the Given Index    */
/*                      instance in the AlgoDrop Table,                      */
/*                      if it is ACTIVE                                      */
/*                      return Failure Success otherwise.                    */
/* Input(s)           : u4DsAlgDropId:  Algo Drop Id                         */
/*                      pu4ErrorCode     - Pointer to Error Code             */
/* Output(s)          : None.                                                */
/* Return Value(s)    : QOS_SUCCESS or QOS_FAILURE                           */
/*****************************************************************************/
INT4
QosUtlValidateAlgDropEntry (UINT4 u4DsAlgoDropId, UINT4 *pu4ErrorCode)
{
    tQoSQTemplateNode  *pQTempNode = NULL;

    if (gQoSGlobalInfo.eSysControl != QOS_SYS_CNTL_START)
    {
        QOS_TRC (MGMT_TRC, QOS_MODULE_SHUTDOW_ERROR_MSG);
        return (QOS_FAILURE);
    }
    pQTempNode = QoSUtlGetQTempNode (u4DsAlgoDropId);
    if (pQTempNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetQTempNode () "
                      " Returns FAILURE. \r\n", __FUNCTION__);

        return (QOS_FAILURE);
    }
    if (QOS_CHECK_Q_TEMP_TBL_INDEX_RANGE (u4DsAlgoDropId) == QOS_FAILURE)
    {
        QOS_TRC_ARG3 (MGMT_TRC, "%s : AlgDrop Id %d is out of Range."
                      " The Range Should be (1-%d). \r\n", __FUNCTION__,
                      u4DsAlgoDropId, QOS_Q_TEMP_TBL_MAX_INDEX_RANGE);

        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return QOS_FAILURE;
    }
    if (pQTempNode->u1Status == ACTIVE)
    {
        QOS_TRC (MGMT_TRC, "Can not change the value in ACTIVE State.\r\n");

        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return QOS_FAILURE;
    }
    return QOS_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : QosUtlValidateQEntry                                 */
/* Description        : This function is used to Validate the Given Index    */
/*                      instance in the Q Table,                             */
/*                      if it is ACTIVE                                      */
/*                      return Failure Success otherwise.                    */
/* Input(s)           : u4DsQId:  Algo Drop Id                         */
/*                      pu4ErrorCode     - Pointer to Error Code             */
/* Output(s)          : None.                                                */
/* Return Value(s)    : QOS_SUCCESS or QOS_FAILURE                           */
/*****************************************************************************/
INT4
QosUtlValidateQEntry (UINT4 u4DsQId, UINT4 *pu4ErrorCode)
{
    tQosStdQEntry      *pQNode = NULL;

    if (gQoSGlobalInfo.eSysControl != QOS_SYS_CNTL_START)
    {
        QOS_TRC (MGMT_TRC, QOS_MODULE_SHUTDOW_ERROR_MSG);
        return (QOS_FAILURE);
    }
    pQNode = QoSUtlGetStdQEntry (u4DsQId);
    if (pQNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetStdQEntry () "
                      " Returns FAILURE. \r\n", __FUNCTION__);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return QOS_FAILURE;
    }
    if (QOS_CHECK_Q_TBL_INDEX_RANGE (u4DsQId) == QOS_FAILURE)
    {
        QOS_TRC_ARG3 (MGMT_TRC, "%s : Queue Id %d is out of Range."
                      " The Range Should be (1-%d). \r\n", __FUNCTION__,
                      u4DsQId, QOS_Q_TBL_MAX_INDEX_RANGE);

        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return QOS_FAILURE;
    }
    if (pQNode->u1DSQStatus == ACTIVE)
    {
        QOS_TRC (MGMT_TRC, "Can not change the value in ACTIVE State.\r\n");

        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return QOS_FAILURE;
    }
    return QOS_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : QoSValidateDataPathTableStatus                       */
/* Description        : This function is used to Check the Mandatory Fields  */
/*                      of DataPath Table's Entry.                           */
/* Input(s)           : pClsMapNode   - Pointer to ClsMapNode                */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : gQoSGlobalInfo                                       */
/* Global Variables                                                          */
/* Modified           : gQoSGlobalInfo                                       */
/* Return Value(s)    : QOS_SUCCESS or QOS_FAILURE                           */
/* Called By          : SNMP Low Level                                       */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QoSValidateDataPathTableStatus (tQosStdDataPathEntry * pDataPathEntry)
{
    /*Check Mandatory Fields */
    if (pDataPathEntry == NULL)
    {
        return (QOS_FAILURE);
    }
    if ((pDataPathEntry->u4RowPointerType != DS_CLFR) &&
        (pDataPathEntry->u4RowPointerType != DS_METER) &&
        (pDataPathEntry->u4RowPointerType != DS_ACTION) &&
        (pDataPathEntry->u4RowPointerType != DS_ALG_DROP) &&
        (pDataPathEntry->u4RowPointerType != QOS_ROWPOINTER_DEF) &&
        (pDataPathEntry->u4RowPointerType != DS_QUEUE))
    {
        QOS_TRC_ARG1 (MGMT_TRC, "%s :DataPathStart should point to a valid"
                      "entry in any of these tables .\r\n", __FUNCTION__);
        pDataPathEntry->u1DataPathStatus = QOS_NOT_READY;
        return (QOS_FAILURE);
    }

    pDataPathEntry->u1DataPathStatus = QOS_NOT_IN_SERVICE;

    return (QOS_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSValidateClassifierElementTableStatus              */
/* Description        : This function is used to Check the Mandatory Fields  */
/*                      of classifier element Entry.                         */
/* Input(s)           : pClfrEntry   - Pointer to classfier entry            */
/*                    : pClfrElementEntry   - Pointer to clfrElement entry   */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : gQoSGlobalInfo                                       */
/* Global Variables                                                          */
/* Modified           : gQoSGlobalInfo                                       */
/* Return Value(s)    : QOS_SUCCESS or QOS_FAILURE                           */
/* Called By          : SNMP Low Level                                       */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QoSValidateClassifierElementTableStatus (tQosStdClfrElementEntry *
                                         pClfrElementEntry)
{
    /*Check Mandatory Fields */
    if (pClfrElementEntry == NULL)
    {
        return (QOS_FAILURE);
    }
    if ((pClfrElementEntry->u4DServClfrId == QOS_ROWPOINTER_DEF) ||
        (pClfrElementEntry->u4DServClfrElementId == QOS_ROWPOINTER_DEF))
    {
        QOS_TRC_ARG1 (MGMT_TRC, "%s :Classifier ID or Classifier Element Id"
                      "should not be zero .\r\n", __FUNCTION__);
        pClfrElementEntry->u1DSClfrElementStatus = QOS_NOT_READY;
        return (QOS_FAILURE);
    }

    if ((pClfrElementEntry->u4DSClfrElementSpecificType != DS_MF_CLFR) &&
        (pClfrElementEntry->u4DSClfrElementSpecificType != QOS_ROWPOINTER_DEF))
    {
        QOS_TRC_ARG1 (MGMT_TRC, "%s :Classifier Specific should point to a"
                      "MFC entry .\r\n", __FUNCTION__);
        pClfrElementEntry->u1DSClfrElementStatus = QOS_NOT_READY;
        return (QOS_FAILURE);
    }

    pClfrElementEntry->u1DSClfrElementStatus = QOS_NOT_IN_SERVICE;
    return (QOS_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSValidateMeterTableStatus                          */
/* Description        : This function is used to Check the Mandatory Fields  */
/*                      of meter Entry.                                      */
/* Input(s)           : pMeterNode   - Pointer to meter node                 */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : gQoSGlobalInfo                                       */
/* Global Variables                                                          */
/* Modified           : gQoSGlobalInfo                                       */
/* Return Value(s)    : QOS_SUCCESS or QOS_FAILURE                           */
/* Called By          : SNMP Low Level                                       */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QoSValidateMeterTableStatus (tQosStdMeterEntry * pMeterNode)
{
    /*Check Mandatory Fields */
    if (pMeterNode == NULL)
    {
        return (QOS_FAILURE);
    }
    if ((pMeterNode->u4DSMeterSpecificType != DS_TBPARM) &&
        (pMeterNode->u4DSMeterSpecificType != QOS_ROWPOINTER_DEF))
    {
        QOS_TRC_ARG1 (MGMT_TRC,
                      "%s :Should point to TBParam entry at present\r\n",
                      __FUNCTION__);
        pMeterNode->u1DSMeterStatus = QOS_NOT_READY;
        return (QOS_FAILURE);
    }

    pMeterNode->u1DSMeterStatus = QOS_NOT_IN_SERVICE;
    return (QOS_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSValidateActionTableStatus                         */
/* Description        : This function is used to Check the Mandatory Fields  */
/*                      of action Entry.                                      */
/* Input(s)           : pActionNode   - Pointer to action node               */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : gQoSGlobalInfo                                       */
/* Global Variables                                                          */
/* Modified           : gQoSGlobalInfo                                       */
/* Return Value(s)    : QOS_SUCCESS or QOS_FAILURE                           */
/* Called By          : SNMP Low Level                                       */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QoSValidateActionTableStatus (tQosStdActionEntry * pActionNode)
{
    /*Check Mandatory Fields */
    if (pActionNode == NULL)
    {
        return (QOS_FAILURE);
    }
    if ((pActionNode->u4DSActionSpecificType != DS_STDDSCP) &&
        (pActionNode->u4DSActionSpecificType != DS_COUNT_ACT) &&
        (pActionNode->u4DSActionSpecificType != DS_ALG_DROP) &&
        (pActionNode->u4DSActionSpecificType != QOS_ROWPOINTER_DEF))
    {
        QOS_TRC_ARG1 (MGMT_TRC, "%s :Action specific Should point to valid"
                      "index\r\n", __FUNCTION__);
        pActionNode->u1DSActionStatus = QOS_NOT_READY;
        return (QOS_FAILURE);
    }
    pActionNode->u1DSActionStatus = QOS_NOT_IN_SERVICE;
    return (QOS_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSValidateQueueTableStatus                          */
/* Description        : This function is used to Check the Mandatory Fields  */
/*                      of Queue Entry.                                      */
/* Input(s)           : pQNode   - Pointer to Queue node                     */
/* Output(s)          : None.                                                */
/* Return Value(s)    : QOS_SUCCESS or QOS_FAILURE                           */
/*****************************************************************************/
INT4
QoSValidateQueueTableStatus (tQosStdQEntry * pQNode)
{
    /*Check Mandatory Fields */
    if (pQNode == NULL)
    {
        return (QOS_FAILURE);
    }
    pQNode->u1DSQStatus = QOS_NOT_IN_SERVICE;
    return (QOS_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QosUtlPointedToStatus                                */
/* Description        : This function is used to validate whether an entry   */
/*                      can be added or deleted.                             */
/* Input(s)           : u4Type:  Rowpointer type                             */
/* Input(s)           : u4Id:    Index                                       */
/*                      u4Status     - QOS_ADD/QOS_DEL/QOS_CHECK             */
/* Return Value(s)    : QOS_SUCCESS or QOS_FAILURE                           */
/*****************************************************************************/
INT4
QosUtlPointedToStatus (UINT4 *pu4PointerType, UINT4 *pu4PointerId,
                       UINT4 *pu4PointerId2, UINT4 u4Type, UINT4 u4Id,
                       UINT4 u4Id2, UINT4 u4Status)
{
    tQosPointNode      *pQosPointNode = NULL;
    tQosPointerListNode *pQosPointerList = NULL;

    if ((u4Type == QOS_START_INITIAL_INDEX) &&
        (u4Id == QOS_START_INITIAL_INDEX) && (u4Id2 == QOS_START_INITIAL_INDEX))
    {
        return QOS_SUCCESS;
    }

    if (u4Status == QOS_ADD)
    {
        TMO_SLL_Scan (&(gQoSGlobalInfo.SllQosPointedTo), pQosPointNode,
                      tQosPointNode *)
        {
            if ((pQosPointNode->u4Type == u4Type) &&
                (pQosPointNode->u4Id == u4Id) &&
                (pQosPointNode->u4Id2 == u4Id2))
            {
                /* Add the pointers list into the PointerSLL of the entry */
                if ((pQosPointerList = (tQosPointerListNode *) MemAllocMemBlk
                     (gQoSGlobalInfo.QoSPointerQid)) == NULL)
                {
                    return QOS_FAILURE;
                }

                MEMSET (pQosPointerList, 0, sizeof (tQosPointerListNode));

                pQosPointerList->u4PointerType = *pu4PointerType;
                pQosPointerList->u4PointerId = *pu4PointerId;
                pQosPointerList->u4PointerId2 = *pu4PointerId2;

                TMO_SLL_Add (&(pQosPointNode->SllQosPointerList),
                             &(pQosPointerList->QosPointerListNode));

                pQosPointNode->u4Count++;
                return QOS_SUCCESS;
            }
        }

        /* Entry is not present. Add the Entry in the Pointed To List */
        if ((pQosPointNode = (tQosPointNode *) MemAllocMemBlk
             (gQoSGlobalInfo.QoSPonitedToQid)) == NULL)
        {
            return QOS_FAILURE;
        }

        MEMSET (pQosPointNode, 0, sizeof (tQosPointNode));

        pQosPointNode->u4Type = u4Type;
        pQosPointNode->u4Id = u4Id;
        pQosPointNode->u4Id2 = u4Id2;

        /* Add the pointers list into the PointerSLL of the entry */
        if ((pQosPointerList = (tQosPointerListNode *) MemAllocMemBlk
             (gQoSGlobalInfo.QoSPointerQid)) == NULL)
        {
            MEM_FREE (pQosPointNode);
            return QOS_FAILURE;
        }

        MEMSET (pQosPointerList, 0, sizeof (tQosPointerListNode));

        pQosPointerList->u4PointerType = *pu4PointerType;
        pQosPointerList->u4PointerId = *pu4PointerId;
        pQosPointerList->u4PointerId2 = *pu4PointerId2;

        TMO_SLL_Init (&(pQosPointNode->SllQosPointerList));

        TMO_SLL_Add (&(pQosPointNode->SllQosPointerList),
                     &(pQosPointerList->QosPointerListNode));

        pQosPointNode->u4Count++;

        TMO_SLL_Add (&(gQoSGlobalInfo.SllQosPointedTo),
                     &(pQosPointNode->QosPointListNode));
        return QOS_SUCCESS;
    }
    else if (u4Status == QOS_DEL)
    {
        TMO_SLL_Scan (&(gQoSGlobalInfo.SllQosPointedTo), pQosPointNode,
                      tQosPointNode *)
        {
            if ((pQosPointNode->u4Type == u4Type) &&
                (pQosPointNode->u4Id == u4Id) &&
                (pQosPointNode->u4Id2 == u4Id2))
            {
                /* Remove the pointers from the list of pointers to the Entry */
                TMO_SLL_Scan (&(pQosPointNode->SllQosPointerList),
                              pQosPointerList, tQosPointerListNode *)
                {
                    if ((pQosPointerList->u4PointerType == *pu4PointerType) &&
                        (pQosPointerList->u4PointerId == *pu4PointerId) &&
                        (pQosPointerList->u4PointerId2 == *pu4PointerId2))
                    {
                        TMO_SLL_Delete (&(pQosPointNode->SllQosPointerList),
                                        &(pQosPointerList->QosPointerListNode));
                        MemReleaseMemBlock (gQoSGlobalInfo.QoSPointerQid,
                                            (UINT1 *) pQosPointerList);
                        pQosPointNode->u4Count--;
                        if (pQosPointNode->u4Count == 0)
                        {
                            break;
                        }
                    }
                }

                /* If Count is 0, remove the entry. Nobody is pointing to it */
                if (pQosPointNode->u4Count == 0)
                {
                    TMO_SLL_Delete (&(gQoSGlobalInfo.SllQosPointedTo),
                                    &(pQosPointNode->QosPointListNode));
                    MemReleaseMemBlock (gQoSGlobalInfo.QoSPonitedToQid,
                                        (UINT1 *) pQosPointNode);
                }
                return QOS_SUCCESS;
            }
        }
        return QOS_FAILURE;
    }
    else if (u4Status == QOS_CHECK)
    {
        TMO_SLL_Scan (&gQoSGlobalInfo.SllQosPointedTo, pQosPointNode,
                      tQosPointNode *)
        {
            if ((pQosPointNode->u4Type == u4Type) &&
                (pQosPointNode->u4Id == u4Id) &&
                (pQosPointNode->u4Id2 == u4Id2))
            {
                return QOS_FAILURE;
            }
        }
        return QOS_SUCCESS;
    }

    return QOS_FAILURE;
}

/*****************************************************************************/
/* Function Name      : QosUtlGetPointerListForEntry                         */
/* Description        : This function is used to get the list of functional  */
/*                      blocks pointing to this functional block             */
/* Input(s)           : u4Type:  Rowpointer type base pointer for the        */
/*                    : u4Id:    Index                                       */
/*                    : pQosPointerList: Pointer to the list                 */
/*                      u4Status     - QOS_ADD/QOS_DEL/QOS_CHECK             */
/* Return Value(s)    : QOS_SUCCESS or QOS_FAILURE                           */
/*****************************************************************************/
INT4
QosUtlGetPointerListForEntry (UINT4 u4Type, UINT4 u4Id, UINT4 u4Id2,
                              tTMO_SLL ** pQosPointerList)
{

    tQosPointNode      *pQosPointNode;

    TMO_SLL_Scan (&gQoSGlobalInfo.SllQosPointedTo, pQosPointNode,
                  tQosPointNode *)
    {
        if ((pQosPointNode->u4Type == u4Type) &&
            (pQosPointNode->u4Id == u4Id) && (pQosPointNode->u4Id2 == u4Id2))
        {
            *pQosPointerList = &(pQosPointNode->SllQosPointerList);
            return QOS_SUCCESS;
        }
    }
    return QOS_FAILURE;
}

/*****************************************************************************/
/* Function Name      : QosAddSchedulerMapEntry                              */
/* Description        : This function Adds a scheduler map to the sch */
/*                      blocks pointing to this functional block             */
/* Input(s)           : u4Type:  Rowpointer type base pointer for the        */
/*                    : u4Id:    Index                                       */
/*                    : pQosPointerList: Pointer to the list                 */
/*                      u4Status     - QOS_ADD/QOS_DEL/QOS_CHECK             */
/* Return Value(s)    : QOS_SUCCESS or QOS_FAILURE                           */
/*****************************************************************************/
INT4
QosAddSchedulerMapEntry (UINT4 u4GlobalId, UINT4 u4Interface, UINT4 u4IfSchedId)
{

    tQosSchedulerMapNode *pQosSchedulerMapNode = NULL;
    UINT4               u4RetStatus;

    if ((pQosSchedulerMapNode = (tQosSchedulerMapNode *) MemAllocMemBlk
         (gQoSGlobalInfo.QoSSchedulerMapPoolId)) == NULL)
    {
        return QOS_FAILURE;
    }

    pQosSchedulerMapNode->u4GlobalId = u4GlobalId;
    pQosSchedulerMapNode->u4Interface = u4Interface;
    pQosSchedulerMapNode->u4IfSchedId = u4IfSchedId;

    u4RetStatus = RBTreeAdd (gQoSGlobalInfo.pRbSchedGlobTbl,
                             (tRBElem *) pQosSchedulerMapNode);

    if (u4RetStatus != RB_SUCCESS)
    {
        u4RetStatus = MemReleaseMemBlock (gQoSGlobalInfo.QoSSchedulerMapPoolId,
                                          (UINT1 *) pQosSchedulerMapNode);
        if (u4RetStatus == MEM_FAILURE)
        {
            QOS_TRC_ARG1 (MGMT_TRC, "In %s : MemReleaseMemBlock () "
                          "Failed.\r\n", __FUNCTION__);

            return QOS_FAILURE;
        }

        QOS_TRC_ARG2 (MGMT_TRC, "In %s : RBTreeAdd () Failed ,"
                      "for u4IfSchedId %d. \r\n", __FUNCTION__, u4IfSchedId);

        return QOS_FAILURE;
    }

    return QOS_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : QosDeleteSchedMapEntry                               */
/* Description        : This function Adds a scheduler map to the sch */
/*                      blocks pointing to this functional block             */
/* Input(s)           : u4Type:  Rowpointer type base pointer for the        */
/*                    : u4Id:    Index                                       */
/*                    : pQosPointerList: Pointer to the list                 */
/*                      u4Status     - QOS_ADD/QOS_DEL/QOS_CHECK             */
/* Return Value(s)    : QOS_SUCCESS or QOS_FAILURE                           */
/*****************************************************************************/
INT4
QosDeleteSchedMapEntry (UINT4 u4GlobalId)
{

    tQosSchedulerMapNode *pQosSchedulerMapNode = NULL;
    tQosSchedulerMapNode TempSchedMapNode;
    tQosSchedulerMapNode *pDelSchedMapNode = NULL;
    UINT4               u4RetStatus = QOS_FAILURE;

    TempSchedMapNode.u4GlobalId = u4GlobalId;

    pQosSchedulerMapNode = (tQosSchedulerMapNode *)
        RBTreeGet (gQoSGlobalInfo.pRbSchedGlobTbl,
                   (tRBElem *) & TempSchedMapNode);
    if (pQosSchedulerMapNode == NULL)
    {
        /* Already the Node is deleted */
        return QOS_SUCCESS;
    }

    /* 1. Remove Entry from the Scheduler Table */

    pDelSchedMapNode = RBTreeRem (gQoSGlobalInfo.pRbSchedGlobTbl,
                                  (tRBElem *) pQosSchedulerMapNode);

    if (pDelSchedMapNode == NULL)
    {
        return QOS_FAILURE;
    }

    /* 2. Clear the Removed Entry */
    MEMSET (pDelSchedMapNode, 0, sizeof (tQosSchedulerMapNode));

    /* 3. Release the Removed Entry's memory */
    u4RetStatus = MemReleaseMemBlock (gQoSGlobalInfo.QoSSchedulerMapPoolId,
                                      (UINT1 *) pDelSchedMapNode);
    if (u4RetStatus == MEM_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : MemReleaseMemBlock () Failed. \r\n",
                      __FUNCTION__);
        return (QOS_FAILURE);
    }

    return QOS_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : QosSchedulerGetGlobalIdFromIfId                      */
/* Description        : This function Adds a scheduler map to the sch */
/*                      blocks pointing to this functional block             */
/* Input(s)           : u4Type:  Rowpointer type base pointer for the        */
/*                    : u4Id:    Index                                       */
/*                    : pQosPointerList: Pointer to the list                 */
/*                      u4Status     - QOS_ADD/QOS_DEL/QOS_CHECK             */
/* Return Value(s)    : QOS_SUCCESS or QOS_FAILURE                           */
/*****************************************************************************/
INT4
QosSchedulerGetGlobalIdFromIfId (UINT4 u4Interface, UINT4 u4IfSchedId,
                                 UINT4 *pu4GlobalId)
{

    tQosSchedulerMapNode *pQosSchedulerMapNode = NULL;
    tQosSchedulerMapNode QosSchedulerMapNode;
    UINT4               u4CurrentIndex = 0;

    do
    {
        if (u4CurrentIndex == 0)
        {
            pQosSchedulerMapNode = (tQosSchedulerMapNode *)
                RBTreeGetFirst (gQoSGlobalInfo.pRbSchedGlobTbl);
        }
        else
        {
            QosSchedulerMapNode.u4GlobalId = u4CurrentIndex;
            pQosSchedulerMapNode = (tQosSchedulerMapNode *)
                RBTreeGetNext (gQoSGlobalInfo.pRbSchedGlobTbl,
                               &QosSchedulerMapNode, QoSSchedMapCmpFun);
        }

        if (pQosSchedulerMapNode == NULL)
        {
            break;
        }

        u4CurrentIndex = pQosSchedulerMapNode->u4GlobalId;

        if ((pQosSchedulerMapNode->u4Interface == u4Interface) &&
            (pQosSchedulerMapNode->u4IfSchedId == u4IfSchedId))
        {
            *pu4GlobalId = pQosSchedulerMapNode->u4GlobalId;
            return QOS_SUCCESS;
        }

    }
    while (pQosSchedulerMapNode != NULL);

    return QOS_FAILURE;
}

/*****************************************************************************/
/* Function Name      : QosSchedulerGetIfIdFromGlobalId                      */
/* Description        : This function Adds a scheduler map to the sch */
/*                      blocks pointing to this functional block             */
/* Input(s)           : u4Type:  Rowpointer type base pointer for the        */
/*                    : u4Id:    Index                                       */
/*                    : pQosPointerList: Pointer to the list                 */
/*                      u4Status     - QOS_ADD/QOS_DEL/QOS_CHECK             */
/* Return Value(s)    : QOS_SUCCESS or QOS_FAILURE                           */
/*****************************************************************************/
INT4
QosSchedulerGetIfIdFromGlobalId (UINT4 u4GlobalId, UINT4 *pu4Interface,
                                 UINT4 *pu4IfSchedId)
{

    tQosSchedulerMapNode *pQosSchedulerMapNode;
    tQosSchedulerMapNode TempSchedMapNode;

    TempSchedMapNode.u4GlobalId = u4GlobalId;

    pQosSchedulerMapNode = (tQosSchedulerMapNode *)
        RBTreeGet (gQoSGlobalInfo.pRbSchedGlobTbl,
                   (tRBElem *) & TempSchedMapNode);

    if (pQosSchedulerMapNode == NULL)
    {
        return QOS_FAILURE;
    }

    *pu4Interface = pQosSchedulerMapNode->u4Interface;
    *pu4IfSchedId = pQosSchedulerMapNode->u4IfSchedId;

    return QOS_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : QosUtlUpdateClfrElemStatus                            */
/* Description        : This function is used to  update Clfr Element status  */
/* Input(s)           : u4Type:  Rowpointer type base pointer for the        */
/*                    : u4Id:    Index                                       */
/*                    : u4Id2 : Pointer to the list                          */
/*                      u4Status     - QOS_ADD/QOS_DEL/QOS_CHECK             */
/* Return Value(s)    : QOS_SUCCESS or QOS_FAILURE                           */
/*****************************************************************************/
INT4
QosUtlUpdateClfrElemStatus (UINT4 u4Type, UINT4 u4Id, UINT4 u4Id2,
                            UINT4 u4Status)
{
    tQosPointerListNode *pQosPointerNode = NULL;
    tQosPointerListNode *pQosPointerNode2 = NULL;
    tQosPointerListNode *pQosPointerNode3 = NULL;
    tQosPointerListNode *pQosPointerNode4 = NULL;
    tTMO_SLL           *pQosPointerList;
    tTMO_SLL           *pQosPointerList2;
    tTMO_SLL           *pQosPointerList3;
    tTMO_SLL           *pQosPointerList4;

    if (QosUtlGetPointerListForEntry (u4Type, u4Id, u4Id2, &pQosPointerList) ==
        QOS_FAILURE)
    {
        return QOS_FAILURE;
    }

    TMO_SLL_Scan (pQosPointerList, pQosPointerNode, tQosPointerListNode *)
    {
        if (pQosPointerNode->u4PointerType == DS_CLFR_ELEM)
        {
            nmhSetDiffServClfrElementStatus (pQosPointerNode->u4PointerId,
                                             pQosPointerNode->u4PointerId2,
                                             u4Status);
            continue;
        }

        if (QosUtlGetPointerListForEntry (pQosPointerNode->u4PointerType,
                                          pQosPointerNode->u4PointerId,
                                          pQosPointerNode->u4PointerId2,
                                          &pQosPointerList2) == QOS_FAILURE)
        {
            continue;

        }
        TMO_SLL_Scan (pQosPointerList2, pQosPointerNode2, tQosPointerListNode *)
        {
            if (pQosPointerNode2->u4PointerType == DS_CLFR_ELEM)
            {
                nmhSetDiffServClfrElementStatus (pQosPointerNode2->u4PointerId,
                                                 pQosPointerNode2->u4PointerId2,
                                                 u4Status);
                continue;
            }
            if (QosUtlGetPointerListForEntry (pQosPointerNode2->u4PointerType,
                                              pQosPointerNode2->u4PointerId,
                                              pQosPointerNode2->u4PointerId2,
                                              &pQosPointerList3) == QOS_FAILURE)
            {
                continue;
            }

            TMO_SLL_Scan (pQosPointerList3, pQosPointerNode3,
                          tQosPointerListNode *)
            {
                if (pQosPointerNode3->u4PointerType == DS_CLFR_ELEM)
                {
                    nmhSetDiffServClfrElementStatus (pQosPointerNode3->
                                                     u4PointerId,
                                                     pQosPointerNode3->
                                                     u4PointerId2, u4Status);
                    continue;
                }

                if (QosUtlGetPointerListForEntry
                    (pQosPointerNode3->u4PointerType,
                     pQosPointerNode3->u4PointerId,
                     pQosPointerNode3->u4PointerId2,
                     &pQosPointerList4) == QOS_FAILURE)
                {
                    continue;
                }

                TMO_SLL_Scan (pQosPointerList4, pQosPointerNode4,
                              tQosPointerListNode *)
                {

                    if (pQosPointerNode3->u4PointerType == DS_CLFR_ELEM)
                    {
                        nmhSetDiffServClfrElementStatus (pQosPointerNode4->
                                                         u4PointerId,
                                                         pQosPointerNode4->
                                                         u4PointerId2,
                                                         u4Status);
                        continue;
                    }
                }
            }
        }
    }
    return QOS_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : QoSAddDefDataPathTblEntries                          */
/* Description        : This function creates datapath entries for al ports  */
/*                       during Module initialization                        */
/* Input(s)           : i4IfIndex:  Interface for which datapath entry  is   */
/*                                    to be created                          */
/*                    : i4Direction : Ingress/ Egress                        */
/* Return Value(s)    : QOS_SUCCESS or QOS_FAILURE                           */
/*****************************************************************************/
INT4
QoSAddDefDataPathTblEntries (INT4 i4IfIndex, INT4 i4Direction)
{
    INT4                i4RetStatus = QOS_FAILURE;

    /* If the port is part of port channel Skip the entry    */
    /* The Datapath entries creation will not be applicable for the */
    /* port part of port-channel.                            */
    if (QosL2IwfIsPortInPortChannel ((UINT4) i4IfIndex) == L2IWF_SUCCESS)
    {
        return (QOS_SUCCESS);
    }

    i4RetStatus = nmhSetDiffServDataPathStatus (i4IfIndex, i4Direction,
                                                QOS_CREATE_AND_WAIT);
    if (i4RetStatus == SNMP_FAILURE)
    {
        return QOS_FAILURE;
    }
    return QOS_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : QoSDelDefDataPathTblEntries                          */
/* Description        : This function deletes datapath entries for all ports  */
/*                                                                           */
/* Input(s)           : i4IfIndex:  Interface for which datapath entry  is   */
/*                                    to be created                          */
/*                    : i4Direction : Ingress/ Egress                        */
/* Return Value(s)    : QOS_SUCCESS or QOS_FAILURE                           */
/*****************************************************************************/
INT4
QoSDelDefDataPathTblEntries (INT4 i4IfIndex, INT4 i4Direction)
{
    INT4                i4RetStatus = QOS_FAILURE;

    i4RetStatus = nmhSetDiffServDataPathStatus (i4IfIndex, i4Direction,
                                                QOS_DESTROY);
    if (i4RetStatus == SNMP_FAILURE)
    {
        return QOS_FAILURE;
    }
    return QOS_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : QoSSchedMapCmpFun                                    */
/* Description        : This function is used as a Compare function in the   */
/*                      RBTree.                                              */
/*                      1.It Compares the Scheduler Global Ids and return the*/
/*                        value                                              */
/* Input(s)           : e1 , e2 -  Elements of RBTree                        */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    :  -1/0/1                                              */
/* Called By          : RBTreeUtility                                        */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QoSSchedMapCmpFun (tRBElem * e1, tRBElem * e2)
{
    tQosSchedulerMapNode *pSchedNode1 = NULL;
    tQosSchedulerMapNode *pSchedNode2 = NULL;

    pSchedNode1 = (tQosSchedulerMapNode *) e1;
    pSchedNode2 = (tQosSchedulerMapNode *) e2;

    /* Check the IfIndex */
    if (pSchedNode1->u4GlobalId < pSchedNode2->u4GlobalId)
    {
        return (-1);
    }
    else if (pSchedNode1->u4GlobalId > pSchedNode2->u4GlobalId)
    {
        return (1);
    }

    return (0);
}

/*****************************************************************************/
/* Function Name      : QosGetQFromPri                                       */
/* Description        : This function is used to Get the Queue               */
/*                         from the Priority                                 */
/*                      1. Get the Q map Node from Priority                  */
/*                      2. Get the Queue Id from the Q Map Node              */
/* Input(s)           : i4IfIndex  -Interface Index                          */
/*                      u4PriIdx   -Priroity                                 */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/* Return Value(s)    : QNode - Queue Node                                   */
/* Called By          :                                                      */
/* Calling Function   :                                                      */
/*****************************************************************************/

tQoSQNode          *
QosGetQFromPri (INT4 i4IfIndex, INT4 u4PriIdx)
{

    tQoSQMapNode       *pQMapNode = NULL;
    tQoSQNode          *pQNode = NULL;

    /*Get the Queue Id from from Qmap Node using the Priority */
    pQMapNode = QoSUtlGetQMapNode (i4IfIndex, QOS_INIT_VAL,
                                   QOS_MIN_VALUE, u4PriIdx);

    if (pQMapNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetQMapNode () "
                      " Returns NULL. \r\n", __FUNCTION__);
        return NULL;
    }

    /*Get the Q Node */
    pQNode = QoSUtlGetQNode (i4IfIndex, pQMapNode->u4QId);
    if (pQNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetQNode () "
                      " Returns NULL. \r\n", __FUNCTION__);
        return NULL;
    }

    /*Return the Queue Id */
    return pQNode;
}

/*****************************************************************************/
/* Function Name      : QosUtlHwCfgEtsParams                                 */
/* Description        : This function is used to program the HW              */
/*                                       with the configured ETS values      */
/*                         from the Priority                                 */
/*                      1. Remove the Stored Configuration from the Hardware */
/*                      2. Program the  Hardware with the configured         */
/*                                  ETS Value                                */
/* Input(s)           : pQNode -Q Node                                       */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/* Return Value(s)    : QOS_SUCCESS or QOS_FAILURE                           */
/* Called By          :                                                      */
/* Calling Function   :                                                      */
/*****************************************************************************/

INT4
QosUtlHwCfgEtsParams (tQoSQNode * pQNode)
{

    /* This function triggers the ETS configured values(like Queue bandwidth) 
     * setting in the HW. The Queue Creation and Deletion is done by 
     * the QoS control plane. Since there exist no NPAPIs for updating the 
     * Queues parameters. QueueCreate & delete NPAPI takes care of updating 
     * them. 
     */
    /* 
     * Due to the limitation in the Queue delete NPAPI specially for the 
     * BCM Trident-2 HW & complex to do skip in the NPAPI. 
     * Hence, skipping the Queue deletion function by commenting out the
     * below function call. 

     if ((pQNode->u1Status == ACTIVE) &&
     (gQoSGlobalInfo.eSysStatus == QOS_SYS_STATUS_ENABLE))
     {
     if (QoSHwWrQueueDelete (pQNode->i4IfIndex, pQNode->u4Id) == QOS_FAILURE)
     {
     QOS_TRC_ARG1 (MGMT_TRC, "In %s : "
     "QoSHwWrQueueDelete () Returns FAILURE.\r\n",
     __FUNCTION__);
     return (QOS_FAILURE);
     }
     } 
     */
    /*Program the  Hardware with the configured ETS Value */
    if (QoSHwWrQueueCreate (pQNode->i4IfIndex, pQNode->u4Id) == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSHwWrQueueCreate ()"
                      " Returns FAILURE.\r\n", __FUNCTION__);
        return (QOS_FAILURE);
    }
    return QOS_SUCCESS;

}

/*****************************************************************************/
/* Function Name      : QosxUtlFillPriToQidMap                               */
/* Description        : This function is used to fill the Priority to        */
/*                             Queue Mapping                                 */
/* Input(s)           : u4IfIndex - Interface Index                          */
/*                       : *pu4QueueId - Pointer to Queue Ids                */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/* Return Value(s)    : QOS_SUCCESS or QOS_FAILURE                           */
/* Called By          : Qos Utility Function                                 */
/* Calling Function   :                                                      */
/*****************************************************************************/

INT4
QosxUtlFillPriToQidMap (UINT4 u4IfIndex, UINT4 *pu4QueueId)
{

    tQoSQMapNode       *pQMapNode = NULL;
    UINT4               u4PriIdx = QOS_INIT_VAL;
    /*Get the Queue Id from from Qmap Node using the Priority */

    for (u4PriIdx = QOS_INIT_VAL; u4PriIdx < QOS_MAX_VLAN_PRI; u4PriIdx++)
    {
        pQMapNode = QoSUtlGetQMapNode (u4IfIndex, QOS_INIT_VAL,
                                       QOS_QMAP_PRI_TYPE_VLAN_PRI, u4PriIdx);
        if (pQMapNode != NULL)
        {
            pu4QueueId[u4PriIdx] = pQMapNode->u4QId;
        }
        else
        {
            QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetQMapNode () "
                          " Returns NULL. \r\n", __FUNCTION__);
            pu4QueueId[u4PriIdx] = QOS_INIT_VAL;
        }
    }

    return QOS_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : QosUtlCpuQCmpFun                                     */
/* Description        : This function is used as a Compare function in the   */
/*                      RBTree.                                              */
/*                      1.It Compares the CpuQEntries and return the values  */
/* Input(s)           : e1 , e2 -  Elements of RBTree                        */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    :  -1/0/1                                              */
/* Called By          : RBTreeUtility                                        */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QosUtlCpuQCmpFun (tRBElem * e1, tRBElem * e2)
{
    tQosCpuQEntry      *pCpuQeNode1 = NULL;
    tQosCpuQEntry      *pCpuQeNode2 = NULL;

    pCpuQeNode1 = (tQosCpuQEntry *) e1;
    pCpuQeNode2 = (tQosCpuQEntry *) e2;

    /* Compare the Queue Id */

    if (pCpuQeNode1->u4CpuQId < pCpuQeNode2->u4CpuQId)
    {
        return (-1);
    }
    else if (pCpuQeNode1->u4CpuQId > pCpuQeNode2->u4CpuQId)
    {
        return (1);
    }

    return (0);
}

/*****************************************************************************/
/* Function Name      : QoSUtlGetCpuQNode                                    */
/* Description        : This function is used to Get the Entry  form  the    */
/*                      CpuQ Table ,it will do the following Action          */
/*                      1. Call RBTreeGet Utility.                           */
/* Input(s)           : u4QId       - Q Id within the Interface              */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : gQoSGlobalInfo                                       */
/* Global Variables                                                          */
/* Modified           :                                                      */
/* Return Value(s)    : NULL/ Pointer to 'tQoSQNode'                         */
/* Called By          : SNMP Low Level                                       */
/* Calling Function   :                                                      */
/*****************************************************************************/
tQosCpuQEntry      *
QoSUtlGetCpuQNode (UINT4 u4QId)
{
    tQosCpuQEntry       TempCpuQNode;
    tQosCpuQEntry      *pCpuQNode = NULL;

    MEMSET (&TempCpuQNode, 0, sizeof (tQosCpuQEntry));

    TempCpuQNode.u4CpuQId = u4QId;

    pCpuQNode = (tQosCpuQEntry *) RBTreeGet (gQoSGlobalInfo.QosCpuQTbl,
                                             (tRBElem *) & TempCpuQNode);
    if (pCpuQNode == NULL)
    {
        QOS_TRC_ARG2 (MGMT_TRC, "%s : QNode (Index : %d) is not Found."
                      "\r\n", __FUNCTION__, u4QId);
    }

    return (pCpuQNode);
}

/*****************************************************************************/
/* Function Name      : QoSUtlCreateCpuQTblEntry                             */
/* Description        : This function is used to Create an Entry in   the    */
/*                      CpuQ Table ,it will do the following Action          */
/*                      1. Allocate a memory block for the New Entry         */
/*                      2. Initialize the New Entry                          */
/*                      3. Add this New Entry into the Cpu Q Table           */
/* Input(s)           : u4QId      - Index to QNode                          */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : gQoSGlobalInfo                                       */
/* Global Variables                                                          */
/* Modified           : gQoSGlobalInfo                                       */
/* Return Value(s)    : NULL / Pointer to  QNode                             */
/* Called By          : SNMP Low Level                                       */
/* Calling Function   :                                                      */
/*****************************************************************************/
tQosCpuQEntry      *
QoSUtlCreateCpuQTblEntry (UINT4 u4QId)
{
    UINT4               u4RetStatus = QOS_FAILURE;
    tQosCpuQEntry      *pNewCpuQNode = NULL;

    /* 1. Allocate a memory block for the New Entry */
    pNewCpuQNode =
        (tQosCpuQEntry *) MemAllocMemBlk (gQoSGlobalInfo.QosCpuQMemPoolId);
    if (pNewCpuQNode == NULL)
    {
        QOS_TRC_ARG2 (MGMT_TRC,
                      "In %s : MemAllocMemBlk() Failed , for CpuQNode "
                      "(Index : %d) .\r\n", __FUNCTION__, u4QId);
        return (NULL);
    }

    /* 2. Initialize the New Entry */
    MEMSET (pNewCpuQNode, 0, sizeof (tQosCpuQEntry));

    pNewCpuQNode->u4CpuQId = u4QId;
    pNewCpuQNode->u4CpuQMinRate = QOS_DEF_CPU_MIN_RATE;
    pNewCpuQNode->u4CpuQMaxRate = QOS_DEF_CPU_MAX_RATE;

    /* 3. Add this New Entry into the CpuQ Table */
    u4RetStatus =
        RBTreeAdd (gQoSGlobalInfo.QosCpuQTbl, (tRBElem *) pNewCpuQNode);
    if (u4RetStatus != RB_SUCCESS)
    {
        u4RetStatus = MemReleaseMemBlock (gQoSGlobalInfo.QosCpuQMemPoolId,
                                          (UINT1 *) pNewCpuQNode);
        if (u4RetStatus == MEM_FAILURE)
        {
            QOS_TRC_ARG2 (MGMT_TRC, "In %s : MemReleaseMemBlock () Failed for "
                          "CpuQNode (Index :%d) .\r\n", __FUNCTION__, u4QId);
            return (NULL);
        }

        QOS_TRC_ARG2 (MGMT_TRC, "In %s : RBTreeAdd () Failed ,for CpuQNode "
                      "(Index : %d) .\r\n", __FUNCTION__, u4QId);
        return (NULL);
    }

    return (pNewCpuQNode);
}

/*****************************************************************************/
/* Function Name      : QoSUtlGetNextCpuQTblEntryIndex                       */
/* Description        : This function is used to Get the Next Entry from the */
/*                      CpuQ Table.                                          */
/* Input(s)           : u4QId           - Current  Q Id                      */
/*                      pu4NextQId      - Pointer to Next Q Id               */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : gQoSGlobalInfo                                       */
/* Global Variables                                                          */
/* Modified           : gQoSGlobalInfo                                       */
/* Return Value(s)    : QOS_SUCCESS or QOS_FAILURE                           */
/* Called By          : SNMP Low Level                                       */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QoSUtlGetNextCpuQTblEntryIndex (UINT4 u4QId, UINT4 *pu4NextQId)
{
    tQosCpuQEntry       CpuQNode;
    tQosCpuQEntry      *pCpuQNode = NULL;

    CpuQNode.u4CpuQId = u4QId;
    pCpuQNode = (tQosCpuQEntry *) RBTreeGetNext (gQoSGlobalInfo.QosCpuQTbl,
                                                 &CpuQNode, QosUtlCpuQCmpFun);
    if (pCpuQNode == NULL)
    {
        QOS_TRC_ARG2 (MGMT_TRC, "In %s : RBTreeGetNext () Failed, for CpuQNode "
                      "(Index : %d) .\r\n", __FUNCTION__, u4QId);
        return (QOS_FAILURE);
    }

    *pu4NextQId = pCpuQNode->u4CpuQId;

    return (QOS_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSUtlDeleteCpuQTblEntry                             */
/* Description        : This function is used to Delete an Entry from the    */
/*                      CpuQ Table.                                          */
/*                      1. Remove the Entry from the CpuQ Table              */
/*                      2. Clear the Removed Entry                           */
/*                      3. Release the Removed Entry's memory                */
/* Input(s)           : pCpuQNode   - Pointer to QNode                       */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : gQoSGlobalInfo                                       */
/* Global Variables                                                          */
/* Modified           : gQoSGlobalInfo                                       */
/* Return Value(s)    : QOS_SUCCESS or QOS_FAILURE                           */
/* Called By          : SNMP Low Level                                       */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QoSUtlDeleteCpuQTblEntry (tQosCpuQEntry * pCpuQNode)
{
    tQosCpuQEntry      *pDelCpuQNode = NULL;
    UINT4               u4RetStatus = QOS_FAILURE;

    if (pCpuQNode == NULL)
    {
        return (QOS_FAILURE);
    }

    /* 1. Remove Entry from the CpuQ Table */
    pDelCpuQNode = RBTreeRem (gQoSGlobalInfo.QosCpuQTbl, (tRBElem *) pCpuQNode);
    if (pDelCpuQNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : RBTreeRem () Failed.\r\n",
                      __FUNCTION__);
        return (QOS_FAILURE);
    }

    /* 2. Clear the Removed Entry */
    MEMSET (pDelCpuQNode, 0, sizeof (tQosCpuQEntry));

    /* 3. Release the Removed Entry's memory */
    u4RetStatus = MemReleaseMemBlock (gQoSGlobalInfo.QosCpuQMemPoolId,
                                      (UINT1 *) pDelCpuQNode);
    if (u4RetStatus == MEM_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : MemReleaseMemBlock () Failed. \r\n",
                      __FUNCTION__);

        return (QOS_FAILURE);
    }

    return (QOS_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSUtlCpuQTblRBTFreeNodeFn                           */
/* Description        : This function is used to Delete the CpuQ Table       */
/*                      Entries and Release the Memory to the respective     */
/*                      Mem Pool .                                           */
/* Input(s)           : tRBElem     - Pointer to Q Table Entry               */
/*                      u4Arg       - Unused Param                           */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : QOS_SUCCESS /QOS_FAILURE                             */
/* Called By          : SNMP                                                 */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QoSUtlCpuQTblRBTFreeNodeFn (tRBElem * pRBElem, UINT4 u4Arg)
{
    UINT4               u4RetStatus = MEM_FAILURE;

    UNUSED_PARAM (u4Arg);

    /* Release the Mem Block */
    u4RetStatus =
        MemReleaseMemBlock (gQoSGlobalInfo.QosCpuQMemPoolId, (UINT1 *) pRBElem);

    if (u4RetStatus != MEM_SUCCESS)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : CpuQTbl- "
                      "MemReleaseMemBlock () Failed.\r\n", __FUNCTION__);
        return (QOS_FAILURE);
    }

    return (QOS_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSCliRSQMapEntry                                    */
/* Description        : This function is used to Reset QueueMap table Entry  */
/* Input(s)           : i4IfIdx      - Interface Idx                         */
/*                    : u4CLASS      - Traffic Class                         */
/*                    : i4MapType    - MapTye   CLASS or PRI_TYPE            */
/*                    : u4PriVal     - Value for the Type                    */
/*                    : u4CurQIdx    - Q Id for the Map                      */
/*                    : i4RowStatus  - RowStatus                             */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : CLI_SUCCESS/CLI_FAILURE                              */
/* Called By          : cli_process_qos_cmd                                  */
/* Calling Function   : SNMP nmh Routines                                    */
/*****************************************************************************/
VOID
QoSCliRSQMapEntry (INT4 i4IfIdx, UINT4 u4CLASS, INT4 i4MapType,
                   UINT4 u4PriVal, UINT4 u4CurQIdx, INT4 i4RowStatus)
{
    INT4                i4RetStatus = SNMP_FAILURE;

    if (i4RowStatus == 0)
    {
        i4RetStatus = nmhSetFsQoSQMapStatus (i4IfIdx, u4CLASS, i4MapType,
                                             u4PriVal, DESTROY);
        return;
    }

    if (u4CurQIdx != (UINT4) CLI_RS_INVALID_MASK)
    {
        i4RetStatus = nmhSetFsQoSQMapQId (i4IfIdx, u4CLASS, i4MapType,
                                          u4PriVal, u4CurQIdx);
    }

    i4RetStatus = nmhSetFsQoSQMapStatus (i4IfIdx, u4CLASS, i4MapType,
                                         u4PriVal, i4RowStatus);

    UNUSED_PARAM (i4RetStatus);

}

/*****************************************************************************/
/* Function Name      : QoSUtlValidatePriority                               */
/* Description        : This function is used check the range for QOS        */
/*                    : priority based on the priority type.                 */
/* Input(s)           : u1PriorityType  - Priority Type                      */
/*                    : u4PriorityVal   - Priority Value                     */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : QOS_SUCCESS /QOS_FAILURE                             */
/* Called By          : SNMP nmh routines                                    */
/* Calling Function   : None                                                 */
/*****************************************************************************/
INT4
QoSUtlValidatePriority (UINT1 u1PriorityType, UINT4 u4PriorityVal)
{
    INT4                i4ReturnValue = QOS_SUCCESS;

    switch (u1PriorityType)
    {
        case QOS_PLY_PHB_TYPE_VLAN_PRI:
        case QOS_PLY_PHB_TYPE_DOT1P:
            if (u4PriorityVal > QOS_IN_PRIORITY_VLAN_PRI_MAX)
            {
                QOS_TRC_ARG3 (MGMT_TRC, "%s :InPriority value is out of range."
                              " for Configured InPriType. Range should be Min"
                              " %d - Max %d. \r\n", __FUNCTION__,
                              QOS_IN_PRIORITY_VLAN_PRI_MIN,
                              QOS_IN_PRIORITY_VLAN_PRI_MAX);

                CLI_SET_ERR (QOS_CLI_ERR_INVALID_VLAN_PRI);

                i4ReturnValue = QOS_FAILURE;
            }
            break;

        case QOS_PLY_PHB_TYPE_IP_TOS:
            /* IP TOS-PRI BIT(3) */
            if (u4PriorityVal > QOS_IN_PRIORITY_IP_TOS_MAX)
            {
                QOS_TRC_ARG3 (MGMT_TRC, "%s :InPriority value is out of range."
                              " for Configured InPriType. Range should be Min"
                              " %d - Max %d. \r\n", __FUNCTION__,
                              QOS_IN_PRIORITY_IP_TOS_MIN,
                              QOS_IN_PRIORITY_IP_TOS_MAX);

                CLI_SET_ERR (QOS_CLI_ERR_INVALID_IP_TOS);

                i4ReturnValue = QOS_FAILURE;
            }
            break;

        case QOS_PLY_PHB_TYPE_IP_DSCP:

            /* IP DSCP BIT(6) */
            if (u4PriorityVal > QOS_IN_PRIORITY_IP_DSCP_MAX)
            {
                QOS_TRC_ARG3 (MGMT_TRC, "%s :InPriority value is out of range."
                              " for Configured InPriType. Range should be Min"
                              " %d - Max %d. \r\n", __FUNCTION__,
                              QOS_IN_PRIORITY_IP_DSCP_MIN,
                              QOS_IN_PRIORITY_IP_DSCP_MAX);

                CLI_SET_ERR (QOS_CLI_ERR_INVALID_IP_DSCP);

                i4ReturnValue = QOS_FAILURE;
            }
            break;

        case QOS_PLY_PHB_TYPE_MPLS_EXP:
            /* MPLS EXP  BIT(3) */
            if (u4PriorityVal > QOS_IN_PRIORITY_MPLS_EXP_MAX)
            {
                QOS_TRC_ARG3 (MGMT_TRC, "%s :InPriority value is out of range."
                              " for Configured InPriType. Range should be Min"
                              " %d - Max %d. \r\n", __FUNCTION__,
                              QOS_IN_PRIORITY_MPLS_EXP_MIN,
                              QOS_IN_PRIORITY_MPLS_EXP_MAX);

                CLI_SET_ERR (QOS_CLI_ERR_INVALID_MPLS_EXP);

                i4ReturnValue = QOS_FAILURE;
            }
            break;

            /* For QOS_PLY_PHB_TYPE_NONE Should not allow  Default PHB Value 
             * it comes under default */
        case QOS_PLY_PHB_TYPE_NONE:
            break;
        default:
            CLI_SET_ERR (QOS_CLI_ERR_INVALID_PRI_TYPE);
            i4ReturnValue = QOS_FAILURE;
            break;
    }
    return (i4ReturnValue);
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : QosUtilRBTreeStub                                */
/*                                                                           */
/*    Description         : This stub function is called when QOS_ARRAY_TO_  */
/*                          RBTREE_WANTED is disabled. To avoid compilation  */
/*                          break , condition covers always true/false, this */
/*                          function is added. Check its usage at calling    */
/*                          function.                                        */
/*                                                                           */
/*    Input(s)            : None                                             */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : QOS_SUCCESS                                      */
/*****************************************************************************/
INT4
QosUtilRBTreeStub ()
{
    return QOS_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : QoSUtilOsixBitlistIsBitSet                       */
/*                                                                           */
/*    Description         : This function is called to check whether the     */
/*                          bit list bit is set or not. For coverity fix the */
/*                          FSAP macro OSIX_BITLIST_IS_BIT_SET is converted  */
/*                          as util function. Whenever a change is made in   */
/*                          the macro this function also needs to be changed */
/*                                                                           */
/*    Input(s)            : au1BitArray - BitArray to be checked             */
/*                          u2BitNumber - Bit Index value                    */
/*                          i4ArraySize - Maximum bit array possible         */
/*                          bResult     - boolean value to check TRUE/FALSE  */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : None                                             */
/*****************************************************************************/

VOID
QoSUtilOsixBitlistIsBitSet (UINT1 *au1BitArray,
                            UINT2 u2BitNumber, INT4 i4ArraySize,
                            BOOL1 * bResult)
{
    UINT2               u2BitNumberBytePos;
    UINT2               u2BitNumberBitPos;
    u2BitNumberBytePos = (UINT2) (u2BitNumber / BITS_PER_BYTE);
    u2BitNumberBitPos = (UINT2) (u2BitNumber % BITS_PER_BYTE);
    *bResult = OSIX_FALSE;
    if (u2BitNumberBitPos == (UINT2) 0)
    {
        u2BitNumberBytePos = (UINT2) (u2BitNumberBytePos - 1);
    }

    if (u2BitNumberBytePos < ((UINT2) i4ArraySize))
    {
        if ((au1BitArray[u2BitNumberBytePos]
             & gau1BitMaskMap[u2BitNumberBitPos]) != 0)
        {

            *bResult = OSIX_TRUE;
        }
    }

}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : QoSUtilOsixBitlistIsBitSet                       */
/*                                                                           */
/*    Description         : This function is called to set the bitlist. For  */
/*                          coverity fix the FSAP macro OSIX_BITLIST_SET_BIT */
/*                          is converted as util funtion. Whenever a change  */
/*                          is made in the macro, this function also needs   */
/*                          to be changed.                                   */
/*                                                                           */
/*    Input(s)            : au1BitArray - BitArray to be checked             */
/*                          u2BitNumber - Bit Index value                    */
/*                          i4ArraySize - Maximum bit array possible         */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*                                                                           */
/*    Returns             : None                                             */
/*****************************************************************************/

VOID
QoSUtilOsixBitlistSetBit (UINT1 *au1BitArray,
                          UINT2 u2BitNumber, INT4 i4ArraySize)
{
    UINT2               u2BitNumberBytePos;
    UINT2               u2BitNumberBitPos;
    u2BitNumberBytePos = (UINT2) (u2BitNumber / BITS_PER_BYTE);
    u2BitNumberBitPos = (UINT2) (u2BitNumber % BITS_PER_BYTE);
    if (u2BitNumberBitPos == 0)
    {
        u2BitNumberBytePos = (UINT2) (u2BitNumberBytePos - 1);
    }

    if (u2BitNumberBytePos < ((UINT2) i4ArraySize))
    {
        au1BitArray[u2BitNumberBytePos] =
            (UINT1) (au1BitArray[u2BitNumberBytePos] |
                     gau1BitMaskMap[u2BitNumberBitPos]);
    }
}

/*****************************************************************************/
/* Function Name      : QoSUtlValidateL2FilterIdForClassMapId                */
/* Description        : This function is used to validate the Given L2 Filter*/
/*                      Id is mapped only to one ClassMap Id.                */
/* Input(s)           : u4L2FilterId    - L2 Filter Id                       */
/*                      pu4ErrorCode    - Pointer to Error Code              */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           :                                                      */
/* Global Variables                                                          */
/* Modified           :                                                      */
/* Return Value(s)    : QOS_SUCCESS or QOS_FAILURE                           */
/* Called By          : SNMP Low Level                                       */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QoSUtlValidateL2FilterIdForClassMapId (UINT4 u4ClassMapId,
                                       UINT4 u4L2FilterId, UINT4 *pu4ErrorCode)
{
    tQoSClassMapNode   *pClsMapNode = NULL;
    INT4                i4RetStatus = 0;
    UINT4               u4ClassId = 0;
    UINT4               u4NextClassMapId = 0;

    i4RetStatus = nmhGetFirstIndexFsQoSClassMapTable (&u4ClassId);

    do
    {
        /* Check to avoid mapping more than one L2FilterId to a ClassMapId 
           Entry */
        pClsMapNode = QoSUtlGetClassMapNode (u4ClassId);

        if (pClsMapNode != NULL)
        {
            if ((pClsMapNode->u4L2FilterId == u4L2FilterId) &&
                (pClsMapNode->u4Id != u4ClassMapId))
            {
                QOS_TRC_ARG2 (MGMT_TRC, "In %s : ERROR Mapping Failed"
                              " for L2Filter Id %d. \r\n", __FUNCTION__,
                              u4L2FilterId);

                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;

                CLI_SET_ERR (QOS_CLI_ERR_CLS_MAP_L2_L3_FILTER_ID);

                return (QOS_FAILURE);
            }
        }

        i4RetStatus =
            nmhGetNextIndexFsQoSClassMapTable (u4ClassId, &u4NextClassMapId);

        u4ClassId = u4NextClassMapId;
    }
    while (i4RetStatus == SNMP_SUCCESS);

    return (QOS_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSUtlValidateL3FilterIdForClassMapId                */
/* Description        : This function is used to validate the Given L2 Filter*/
/*                      Id is mapped only to one ClassMap Id.                */
/* Input(s)           : u4L3FilterId    - L3 Filter Id                       */
/*                      pu4ErrorCode    - Pointer to Error Code              */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           :                                                      */
/* Global Variables                                                          */
/* Modified           :                                                      */
/* Return Value(s)    : QOS_SUCCESS or QOS_FAILURE                           */
/* Called By          : SNMP Low Level                                       */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QoSUtlValidateL3FilterIdForClassMapId (UINT4 u4ClassMapId,
                                       UINT4 u4L3FilterId, UINT4 *pu4ErrorCode)
{
    tQoSClassMapNode   *pClsMapNode = NULL;
    INT4                i4RetStatus = 0;
    UINT4               u4ClassId = 0;
    UINT4               u4NextClassMapId = 0;

    i4RetStatus = nmhGetFirstIndexFsQoSClassMapTable (&u4ClassId);

    do
    {
        /* Check to avoid mapping more than one L2FilterId to a ClassMapId
           Entry */
        pClsMapNode = QoSUtlGetClassMapNode (u4ClassId);

        if (pClsMapNode != NULL)
        {
            if ((pClsMapNode->u4L3FilterId == u4L3FilterId) &&
                (pClsMapNode->u4Id != u4ClassMapId))
            {
                QOS_TRC_ARG2 (MGMT_TRC, "In %s : ERROR Mapping Failed"
                              " for L3Filter Id %d. \r\n", __FUNCTION__,
                              u4L3FilterId);

                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;

                CLI_SET_ERR (QOS_CLI_ERR_CLS_MAP_L2_L3_FILTER_ID);

                return (QOS_FAILURE);
            }
        }

        i4RetStatus =
            nmhGetNextIndexFsQoSClassMapTable (u4ClassId, &u4NextClassMapId);

        u4ClassId = u4NextClassMapId;
    }
    while (i4RetStatus == SNMP_SUCCESS);

    return (QOS_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSUtlGetPolicyMapId                                 */
/* Description        : This function is used to Get the Entry  from  the    */
/*                      Policy Map Table for specific Meter id.              */
/*                                                                           */
/* Input(s)           : u4PrevPlyId     - Previous Policy Id                 */
/*                                                                           */
/* Output(s)          : u4CurPlyId      - Current Policy Id                  */
/* Global Variables                                                          */
/* Referred           : gQoSGlobalInfo                                       */
/* Global Variables                                                          */
/* Modified           :                                                      */
/* Return Value(s)    : NULL/ Pointer to 'tQoSPolicyMapNode'                 */
/* Called By          : SNMP Low Level                                       */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QoSUtlGetPolicyMapId (UINT4 u4PrevPlyId, UINT4 *u4CurPlyId)
{
    INT4                i4RetStatus = 0;

    i4RetStatus = nmhGetNextIndexFsQoSPolicyMapTable (u4PrevPlyId, u4CurPlyId);
    if (i4RetStatus != SNMP_SUCCESS)
    {
        return QOS_FAILURE;
    }
    return QOS_SUCCESS;

}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : QosUtlDeleteExistMeterStats                      */
/*                                                                           */
/*    Description         : This function is used to deleting the existing   */
/*                         statistics entries in the hardware                */
/*                                                                           */
/*    Input(s)            : None                                             */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : None                                             */
/*                                                                           */
/*****************************************************************************/
VOID
QosUtlDeleteExistMeterStats ()
{
    UINT4               u4CurrMeterId = 0;
    UINT4               u4NextMeterId = 0;

    while (nmhGetNextIndexFsQoSPolicerStatsTable
           (u4CurrMeterId, &u4NextMeterId) == QOS_SUCCESS)
    {

        /*deleting the existing statistics entry in the hardware */
        if (nmhSetFsQoSPolicerStatsStatus
            (u4NextMeterId, QOS_STATS_DISABLE) != QOS_SUCCESS)
        {
            return;
        }

        u4CurrMeterId = u4NextMeterId;
    }
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : QoSUtlValidateClassTypeParams                    */
/*                                                                           */
/*    Description         : This api will validate CLASS type against the    */
/*                          QMap configured parameters.                      */
/*                                                                           */
/*    Input(s)            : u4QMapCLASS                                      */
/*                          i4IfIndex                                        */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : QOS_SUCCESS/QOS_FAILURE                          */
/*                                                                           */
/*****************************************************************************/

INT4
QoSUtlValidateClassTypeParams (UINT4 u4QMapCLASS, INT4 i4IfIndex)
{
#ifdef NPAPI_WANTED

    tQoSClassInfoNode  *pClsInfoNode = NULL;
    tQoSFilterInfoNode *pFltInfoNode = NULL;
    tQoSClassMapEntry  *pNewClassMapEntry = NULL;

    if (u4QMapCLASS != 0)
    {
        pClsInfoNode = QoSUtlGetClassInfoNode (u4QMapCLASS);
        if (pClsInfoNode == NULL)
        {
            CLI_SET_ERR (QOS_CLI_ERR_CLS_NOT_CREATED);
            return (QOS_FAILURE);
        }

        TMO_SLL_Scan (&(pClsInfoNode->SllFltInfo), pFltInfoNode,
                      tQoSFilterInfoNode *)
        {

            /* Fill the Values for that Entry */
            pNewClassMapEntry =
                QoSFiltersForClassMapEntry (pFltInfoNode->pClsMapNode);

            if (pNewClassMapEntry != NULL)
            {
                if ((pNewClassMapEntry->pL2FilterPtr != NULL)
                    && (i4IfIndex != 0))
                {
                    QOS_TRC_ARG1 (MGMT_TRC, "In %s : ERROR: "
                                  "Invalid parameters." " \r\n", __FUNCTION__);
                    CLI_SET_ERR (QOS_CLI_ERR_QMAP_INVALID_ACL_PARAMS);
                    return (QOS_FAILURE);
                }
                else if ((pNewClassMapEntry->pL3FilterPtr != NULL)
                         && (i4IfIndex != 0))
                {
                    QOS_TRC_ARG1 (MGMT_TRC, "In %s : ERROR: "
                                  "Invalid parameters." " \r\n", __FUNCTION__);
                    CLI_SET_ERR (QOS_CLI_ERR_QMAP_INVALID_ACL_PARAMS);
                    return (QOS_FAILURE);
                }
                else if ((pNewClassMapEntry->pInVlanMapPtr != NULL)
                         && (i4IfIndex == 0))
                {
                    QOS_TRC_ARG1 (MGMT_TRC, "In %s : ERROR: "
                                  "Invalid parameters." " \r\n", __FUNCTION__);
                    CLI_SET_ERR (QOS_CLI_ERR_QMAP_INVALID_VLAN_MAP_PARAMS);
                    return (QOS_FAILURE);
                }
            }
        }
    }

    return (QOS_SUCCESS);
#else
    UNUSED_PARAM (u4QMapCLASS);
    UNUSED_PARAM (i4IfIndex);

    return (QOS_SUCCESS);
#endif
}

/*****************************************************************************/
/* Function Name      : QoSAddDefPcpTblEntries                               */
/* Description        : This function adds default PCP entries for all ports */
/*                                                                           */
/* Input(s)           : i4IfIndex:  Interface for which PCP entry  is        */
/*                                    to be created                          */
/*                                                                           */
/* Return Value(s)    : QOS_SUCCESS or QOS_FAILURE                           */
/*****************************************************************************/
INT4
QoSAddDefPcpTblEntries (INT4 i4IfIndex)
{
    tQoSPortTblEntry   *pQoSPortTblNode = NULL;
    INT4                i4RetStatus = QOS_FAILURE;
    INT4                i4PktType;
    INT4                i4PcpModel;
    INT4                i4Action;

    /* During System Init, Default PCP Model chosen will 8P0D and the packet */
    /* type is Ethernet Type and the action specified will be CREATE */
    i4PktType = QOS_PCP_PKT_TYPE_DOT1P;
    i4PcpModel = QOS_PCP_SEL_ROW_8P0D;
    i4Action = QOS_PCP_TBL_ACTION_CREATE;
    /* Creation of Port Table Node in global Port Tbl RB tree */
    pQoSPortTblNode = QoSCreatePortTblEntry (i4IfIndex, i4PktType);
    if (pQoSPortTblNode == NULL)
    {
        return (QOS_FAILURE);
    }
    /* Populating the values of PCP Table */
    pQoSPortTblNode->i4PcpSelRow = i4PcpModel;

    i4RetStatus = QoSConfigPcpTblEntries (pQoSPortTblNode, i4Action);
    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC,
                      "In %s : QoSConfigPcpTblEntries "
                      " () Returns FAILURE. \r\n", __FUNCTION__);
        return (QOS_FAILURE);
    }

    return QOS_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : QoSConfigPcpTblEntries                               */
/* Description        : This function adds or modifies a PCP Table entry     */
/*                      depends on the action specified for a specfic port   */
/*                      and Packet Type                                      */
/*                                                                           */
/* Input(s)           : i4IfIndex:  Interface for which PCP entry  is        */
/*                                    to be created                          */
/*                      i4PktType:  Packet type                              */
/*                      i4PcpModel: PCP Selection Model                      */
/*                      i4Action: Action to be done                          */
/*                                                                           */
/* Return Value(s)    : QOS_SUCCESS or QOS_FAILURE                           */
/*****************************************************************************/
INT4
QoSConfigPcpTblEntries (tQoSPortTblEntry * pQoSPortTblNode, INT4 i4Action)
{
    INT4                i4RetStatus = QOS_FAILURE;

    /*Depends on the action specified we will Add/Modify an Entry in PCP tbl */
    /*Action: CREATION */
    if (i4Action == QOS_PCP_TBL_ACTION_CREATE)
    {
        i4RetStatus = QoSCreatePcpTblEntries (pQoSPortTblNode);
        if (i4RetStatus == QOS_FAILURE)
        {
            QOS_TRC_ARG1 (MGMT_TRC,
                          "In %s : QoSCreatePcpTblEntries "
                          " () Returns FAILURE. \r\n", __FUNCTION__);
            return (QOS_FAILURE);
        }

    }

    /*Action: MODIFICATION */
    if (i4Action == QOS_PCP_TBL_ACTION_MODIFY)
    {
        i4RetStatus = QoSModifyPcpTblEntries (pQoSPortTblNode);
        if (i4RetStatus == QOS_FAILURE)
        {
            QOS_TRC_ARG1 (MGMT_TRC,
                          "In %s : QoSModifyPcpTblEntries "
                          " () Returns FAILURE. \r\n", __FUNCTION__);
            return (QOS_FAILURE);
        }
    }

    /*Action: DELETION */
    if (i4Action == QOS_PCP_TBL_ACTION_DELETE)
    {
        /*Put the Enries in NOT_IN_SERVICE State and do the Cleanup of Entries */
        /*in Hardware */
        i4RetStatus = QoSDeletePortTblEntry (pQoSPortTblNode);
        if (i4RetStatus == QOS_FAILURE)
        {
            QOS_TRC_ARG1 (MGMT_TRC,
                          "In %s : QoSDeletePortTblEntry "
                          " () Returns FAILURE. \r\n", __FUNCTION__);

            return (QOS_FAILURE);
        }
    }

    return (QOS_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSCreatePcpTblEntries                               */
/* Description        : This function adds  a PCP Table entry                */
/*                      depends on the action specified for a specfic port   */
/*                      and Packet Type                                      */
/*                                                                           */
/* Input(s)           : i4IfIndex:  Interface for which PCP entry  is        */
/*                                    to be created                          */
/*                      i4PktType:  Packet type                              */
/*                      i4PcpModel: PCP Selection Model                      */
/*                      i4Action: Action to be done                          */
/*                                                                           */
/* Return Value(s)    : QOS_SUCCESS or QOS_FAILURE                           */
/*****************************************************************************/
INT4
QoSCreatePcpTblEntries (tQoSPortTblEntry * pQoSPortTblNode)
{
    INT4                i4RetStatus = QOS_FAILURE;
    INT4                i4PcpTblIndex = 0;
    INT4                i4IfIndex = pQoSPortTblNode->i4IfIndex;
    INT4                i4PktType = pQoSPortTblNode->i4PktType;

    QOS_GET_PCP_TBL_INDEX (i4IfIndex, i4PktType, i4PcpTblIndex);
    pQoSPortTblNode->i4PcpTblIndex = i4PcpTblIndex;

    pQoSPortTblNode->u1Type = QOS_PCP_ENTRY_TYPE_IMPLICIT;
    pQoSPortTblNode->u1Status = ACTIVE;
    /* 1. Creation of PriorityMap Table entries for PCP Encoding */
    /* 1P + DEI -> PCP (Internal Priority) */
    i4RetStatus = QoSAddPcpPriMapTblEntries (pQoSPortTblNode);
    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC,
                      "In %s : QoSAddPcpPriMapTblEntries "
                      " () Returns FAILURE. \r\n", __FUNCTION__);

        return (QOS_FAILURE);
    }
    /*2.Creation of PolicyMap table Entries for Packet marking */
    /* PCP Decoding PCP-> 1P + DEI  */
    /* Since Marking is not supported for DSCP */

    if (i4PktType != QOS_PCP_PKT_TYPE_IP)
    {
        i4RetStatus = QoSAddPcpPlyMapTblEntries (pQoSPortTblNode);
        if (i4RetStatus == QOS_FAILURE)
        {
            QOS_TRC_ARG1 (MGMT_TRC,
                          "In %s : QoSAddPcpPlyMapTblEntries "
                          " () Returns FAILURE. \r\n", __FUNCTION__);

            return (QOS_FAILURE);
        }
    }

    /*3.Creation of Queuemap table Entries for default queue */
    /* mapping */
    i4RetStatus = QoSAddPcpQMapTblEntries (pQoSPortTblNode);
    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC,
                      "In %s : QoSAddDefPcpQMapTblEntries "
                      " () Returns FAILURE. \r\n", __FUNCTION__);

        return (QOS_FAILURE);
    }
    return QOS_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : QoSAddPcpPriMapTblEntries                            */
/* Description        : This function adds the Priority Map entries required */
/*                      for implementing the PCP Model                       */
/*                                                                           */
/* Input(s)           : pPortTblNode:  PCP Port Table Entry for which the    */
/*                      Priority Map Table entries to be created             */
/*                                                                           */
/* Return Value(s)    : QOS_SUCCESS or QOS_FAILURE                           */
/*****************************************************************************/
INT4
QoSAddPcpPriMapTblEntries (tQoSPortTblEntry * pPortTblNode)
{
    tQoSInPriorityMapNode *pPMNode = NULL;
    INT4                i4RetStatus = QOS_FAILURE;
    UINT4               u4PriMapId = 0;
    UINT4               u4StartIndex = (UINT4) pPortTblNode->i4PcpTblIndex;
    UINT1               u1Priority = 0;
    UINT1               u1DEI = 0;
    UINT1               u1OutLoopIndex = 0;
    UINT1               u1InLoopIndex = 0;
    UINT1               u1TempVal = 0;
    /*Add PriorityMap Table Entries for 802.1P Packet for */
    /*all the Interface Index (1-24)                     */
    /*24 Ports x 8 1P Priority values = 192 map Id's     */
    /*30001-30192 Priority map Id's will be used for DO1P */

    /*------------------------------------------------------------*/
    /*| Id     | IfIndex | VlanId | IPT | IPV | RPV | RIPV | CS | */
    /*------------------------------------------------------------*/
    /*| 30001  |    1    |   0    | VP  |  0  |  0  |  8   | SD | */
    /*| 30002  |    1    |   0    | VP  |  0DE|  1  |  8   | SD | */
    /*| 30003  |    1    |   0    | VP  |  1  |  2  |  8   | SD | */
    /*| 30004  |    1    |   0    | VP  |  1DE|  3  |  8   | SD | */
    /*| 30005  |    1    |   0    | VP  |  2  |  4  |  8   | SD | */
    /*| 30006  |    1    |   0    | VP  |  2DE|  5  |  8   | SD | */
    /*| 30007  |    1    |   0    | VP  |  3  |  6  |  8   | SD | */
    /*| 30008  |    1    |   0    | VP  |  3DE|  7  |  8   | SD | */

    /*Fetching the value of Inner and Outer loop value depending up on
     * the Packet Type*/

    QOS_GET_PCP_OL_INDEX (pPortTblNode->i4PktType, u1OutLoopIndex);
    QOS_GET_PCP_IL_INDEX (pPortTblNode->i4PktType, u1InLoopIndex);

    /*Model chosen is applied to each Priority values */

    for (u1Priority = 0; u1Priority <= u1OutLoopIndex; u1Priority++)
    {
        for (u1DEI = 0; u1DEI <= u1InLoopIndex; u1DEI++)
        {
            u4PriMapId = u4StartIndex;
            pPMNode = QoSCreatePriMapTblEntry (u4PriMapId);
            if (pPMNode == NULL)
            {
                return (QOS_FAILURE);
            }

            /* Update the Entry  and dependent Tbls */
            pPMNode->u4Id = u4PriMapId;
            pPMNode->u1ConfStatus = QOS_PRI_MAP_TBL_CONF_STATUS_MGNT;
            pPMNode->u4IfIndex = (UINT4) pPortTblNode->i4IfIndex;
            pPMNode->u2VlanId = 0;
            pPMNode->u1InPriority = u1Priority;
            pPMNode->u1Type = QOS_PCP_ENTRY_TYPE_IMPLICIT;

            /*Populate the parameters of Priority Map for DOT1P Packet Type */
            if (pPortTblNode->i4PktType == QOS_PCP_PKT_TYPE_DOT1P)
            {
                pPMNode->u1InPriType = QOS_IN_PRI_TYPE_DOT1P;
                pPMNode->i4InDEI = (INT4) u1DEI;
                pPMNode->u1RegenPriority = gau1PcpVal[pPortTblNode->i4PcpSelRow]
                    [(u1Priority << 1) + u1DEI];
                pPMNode->i4RegenColor = gau1ColorVal[pPortTblNode->i4PcpSelRow]
                    [u1Priority];
                /*Updation of Global Oper Array */
                QoSUtlStoreOperEncodingInfo (pPMNode->u4IfIndex, u1Priority,
                                             u1DEI, pPMNode->u1RegenPriority,
                                             pPortTblNode->i4PcpSelRow,
                                             pPortTblNode->i4PktType,
                                             QOS_PCP_TBL_ACTION_CREATE);
            }
            /*Populate the parameters of Priority Map for IP Packet Type */
            if (pPortTblNode->i4PktType == QOS_PCP_PKT_TYPE_IP)
            {
                pPMNode->u1InPriType = QOS_IN_PRI_TYPE_IP_DSCP;
                u1TempVal = u1Priority / 8;
                pPMNode->u1RegenPriority = u1TempVal;
                pPMNode->i4RegenColor = gau1ColorVal[pPortTblNode->i4PcpSelRow]
                    [u1TempVal];
                QoSUtlStoreOperEncodingInfo (pPMNode->u4IfIndex, u1Priority,
                                             0, pPMNode->u1RegenPriority,
                                             pPortTblNode->i4PcpSelRow,
                                             pPortTblNode->i4PktType,
                                             QOS_PCP_TBL_ACTION_CREATE);
            }

            /*Populate the parameters of Priority Map for MPLS Packet Type */
            if (pPortTblNode->i4PktType == QOS_PCP_PKT_TYPE_MPLS)
            {
                pPMNode->u1InPriType = QOS_IN_PRI_TYPE_MPLS_EXP;
                pPMNode->u1RegenPriority = u1Priority;
                pPMNode->i4RegenColor = gau1ColorVal[pPortTblNode->i4PcpSelRow]
                    [u1Priority];
                QoSUtlStoreOperEncodingInfo (pPMNode->u4IfIndex, u1Priority,
                                             0, pPMNode->u1RegenPriority,
                                             pPortTblNode->i4PcpSelRow,
                                             pPortTblNode->i4PktType,
                                             QOS_PCP_TBL_ACTION_CREATE);

            }
            pPMNode->u1Status = ACTIVE;

            /*Giving Trigger for NPAPI programming */
            i4RetStatus = QoSHwWrMapClasstoPriMap (pPMNode, QOS_NP_ADD);
            if ((i4RetStatus == QOS_FAILURE)
                || (i4RetStatus == QOS_NP_NOT_SUPPORTED))
            {
                QOS_TRC_ARG1 (MGMT_TRC,
                              "In %s : QoSHwWrMapClasstoPriMap "
                              " () Returns FAILURE. \r\n", __FUNCTION__);

                return (QOS_FAILURE);
            }

            u4StartIndex++;
        }
    }
    return QOS_SUCCESS;

}

/*****************************************************************************/
/* Function Name      : QoSAddConfPcpPlyMapTblEntries                        */
/* Description        : This function adds default Policy Map entries        */
/*                        for all ports                                      */
/*                                                                           */
/* Input(s)           : i4IfIndex:  Interface for which PCP entry  is        */
/*                                    to be created                          */
/*                                                                           */
/* Return Value(s)    : QOS_SUCCESS or QOS_FAILURE                           */
/*****************************************************************************/
INT4
QoSAddPcpPlyMapTblEntries (tQoSPortTblEntry * pPortTblNode)
{
    tQoSInPriorityMapNode *pPriMapNode = NULL;
    tQoSPolicyMapNode  *pPOLMNode = NULL;
    tQoSClassMapNode   *pCMNode = NULL;
    tQoSClassInfoNode  *pClsInfoNode = NULL;
    INT4                i4RetStatus = QOS_FAILURE;
    UINT4               u4StartIndex = (UINT4) pPortTblNode->i4PcpTblIndex;
    UINT1               u1IPVal = 0;
    UINT1               u1DEI = 0;
    UINT1               u1OutLoopIndex = 0;
    UINT1               u1InLoopIndex = 0;

    QOS_GET_PCP_OL_INDEX (pPortTblNode->i4PktType, u1OutLoopIndex);
    QOS_GET_PCP_IL_INDEX (pPortTblNode->i4PktType, u1InLoopIndex);

    for (u1IPVal = 0; u1IPVal <= u1OutLoopIndex; u1IPVal++)
    {
        for (u1DEI = 0; u1DEI <= u1InLoopIndex; u1DEI++)
        {
            if (QoSCreateClsInfoTblEntry (u4StartIndex) == NULL)
            {
                return (QOS_FAILURE);
            }

            /*3. Create a class map with this Priority map as classifier */
            /*Add PriorityMap Table Entries */
            /*-----------------------------------------------------------*/
            /*| Id     | L2filterId | L3FilterId | Priority MapId | Precolor */
            /*-----------------------------------------------------------*/
            /*| 40001  |    0       |   0        | 40001          |  GREEN  | */
            /*| 40002  |    0       |   0        | 40002          |  GREEN  | */
            /*| 40003  |    0       |   0        | 40003          |  GREEN  | */
            /*| 40004  |    0       |   0        | 40004          |  GREEN  | */
            /*| 40005  |    0       |   0        | 40005          |  GREEN  | */
            /*| 40006  |    0       |   0        | 40006          |  GREEN  | */
            /*| 40007  |    0       |   0        | 40007          |  GREEN  | */
            /*| 40008  |    0       |   0        | 40008          |  GREEN  | */

            pCMNode = QoSCreateClsMapTblEntry (u4StartIndex);
            if (pCMNode == NULL)
            {
                return (QOS_FAILURE);
            }
            /* Update the Entry  and dependent Tbls */
            pCMNode->u4Id = u4StartIndex;
            pCMNode->u4L2FilterId = 0;
            pCMNode->u4L3FilterId = 0;
            pCMNode->u4PriorityMapId = u4StartIndex;
            pCMNode->u4ClassId = u4StartIndex;
            pCMNode->u1PreColor = QOS_CLS_DEFAULT_PRE_COLOR;
            pCMNode->u1Type = QOS_PCP_ENTRY_TYPE_IMPLICIT;
            pCMNode->u1Status = ACTIVE;

            /*  increment the ref count */
            pPriMapNode = QoSUtlGetPriorityMapNode (u4StartIndex);
            if (pPriMapNode == NULL)
            {
                QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetPriorityMapNode () "
                              "Returns FAILURE. \r\n", __FUNCTION__);

                return (SNMP_FAILURE);
            }

            /* Incremnet the RefCount of PriMapId  Entry in the PriorityMap Table */
            pPriMapNode->u4RefCount = (pPriMapNode->u4RefCount) + 1;

            /* This Function is used to Add ClassMapIds to the respective 
             * CLASS using the internal DataSrtuct 'tQoSClassInfoNode'
             */
            i4RetStatus = QoSClsInfoAddFilters (u4StartIndex, pCMNode);
            if (i4RetStatus == QOS_FAILURE)
            {
                QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSClsInfoAddFilters () "
                              "Returns FAILURE. \r\n", __FUNCTION__);

                return (SNMP_FAILURE);
            }
            /* Configure the HW  if Needed */

            if (gQoSGlobalInfo.eSysStatus == QOS_SYS_STATUS_ENABLE)
            {
                pClsInfoNode = QoSUtlGetClassInfoNode (u4StartIndex);
                if ((pClsInfoNode != NULL)
                    && (pClsInfoNode->pPlyMapNode != NULL))
                {
                    /* if a Policy is already create for this Class 
                     * Get the Policy Map Add the Class (Filters) to it */
                    i4RetStatus =
                        QoSHwWrMapClassToPolicy (pClsInfoNode->pPlyMapNode,
                                                 pCMNode, QOS_PLY_MAP);
                    if (i4RetStatus == QOS_FAILURE)
                    {
                        QOS_TRC_ARG1 (MGMT_TRC,
                                      "In %s : QoSHwWrMapClassToPolicy "
                                      " () Returns FAILURE. \r\n",
                                      __FUNCTION__);

                        return (SNMP_FAILURE);
                    }

                }                /* End of If */
            }

            /*4. Create a Policy map and set class to Policymap */

            pPOLMNode = QoSCreatePlyMapTblEntry (u4StartIndex);
            if (pPOLMNode == NULL)
            {
                return (QOS_FAILURE);
            }

            /* Update the Entry  and dependent Tbls */
            pPOLMNode->u4Id = u4StartIndex;
            pPOLMNode->u4IfIndex = (UINT4) pPortTblNode->i4IfIndex;
            pPOLMNode->u4ClassId = u4StartIndex;
            pPOLMNode->u4MeterTableId = 0;
            if (pPortTblNode->i4PktType == QOS_PCP_PKT_TYPE_DOT1P)
            {
                pPOLMNode->u1PHBType = QOS_PLY_PHB_TYPE_DOT1P;
                pPOLMNode->i4VlanDE = (gau1DOT1PVal[pPortTblNode->i4PcpSelRow]
                                       [pPriMapNode->u1RegenPriority]) & 1;
                gau1DEArray[pPortTblNode->i4IfIndex]
                    [pPriMapNode->u1RegenPriority] =
                    (UINT1) pPOLMNode->i4VlanDE;
                pPOLMNode->u2DefaultPHB =
                    (gau1DOT1PVal[pPortTblNode->i4PcpSelRow]
                     [pPriMapNode->u1RegenPriority]) >> 1;
                QoSUtlStoreOperDecodingInfo (pPOLMNode->u4IfIndex,
                                             pPriMapNode->u1RegenPriority,
                                             (UINT1) pPOLMNode->u2DefaultPHB,
                                             (UINT1) pPOLMNode->i4VlanDE,
                                             pPortTblNode->i4PcpSelRow,
                                             pPortTblNode->i4PktType,
                                             QOS_PCP_TBL_ACTION_CREATE);

            }
            /*Updating the Policy Map Parameters for MPLS Packet Type */
            if (pPortTblNode->i4PktType == QOS_PCP_PKT_TYPE_MPLS)
            {
                pPOLMNode->u1PHBType = QOS_PLY_PHB_TYPE_MPLS_EXP;
                pPOLMNode->u2DefaultPHB = pPriMapNode->u1RegenPriority;
                QoSUtlStoreOperDecodingInfo (pPOLMNode->u4IfIndex,
                                             pPriMapNode->u1RegenPriority,
                                             (UINT1) pPOLMNode->u2DefaultPHB, 0,
                                             pPortTblNode->i4PcpSelRow,
                                             pPortTblNode->i4PktType,
                                             QOS_PCP_TBL_ACTION_CREATE);
            }
            pPOLMNode->i4RegenColor = pPriMapNode->i4RegenColor;
            pPOLMNode->u1Type = QOS_PCP_ENTRY_TYPE_IMPLICIT;
            pPOLMNode->u1Status = ACTIVE;

            /* Add the entry from the Internal Reference. */
            if (QOS_CM_TBL_DEF_CLASS != 0)
            {
                i4RetStatus = QoSClsInfoAddPolicy (u4StartIndex, pPOLMNode);
                if (i4RetStatus == QOS_FAILURE)
                {
                    QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSClsInfoAddPolicy () "
                                  "Returns FAILURE. \r\n", __FUNCTION__);

                    return (SNMP_FAILURE);
                }
            }
            /* Configure the HW  if Needed */
            if (gQoSGlobalInfo.eSysStatus == QOS_SYS_STATUS_ENABLE)
            {
                /* 1. Add the entry into:the HARDWARE. */
                i4RetStatus = QoSHwWrMapClassToPolicy (pPOLMNode, NULL,
                                                       QOS_PLY_ADD);
                if (i4RetStatus == QOS_FAILURE)
                {
                    QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSHwWrMapClassToPolicy "
                                  " () Returns FAILURE. \r\n", __FUNCTION__);

                    return (SNMP_FAILURE);
                }
            }
            u4StartIndex++;
        }
    }
    return QOS_SUCCESS;

}

/*****************************************************************************/
/* Function Name      : QoSAddPcpQMapTblEntries                              */
/* Description        : This function is used to Create default tables for   */
/*                      QueueMap Table Entry.                                */
/* Input(s)           : i4IfIdx - IfIndex .                                  */
/*                    : u1Flag  - Flag indicating if the default entry       */
/*                                has to be created in SoftwareAndHardware   */
/*                                or ONLY in hardware.                       */
/*                                (At QOS module disable, default Q map      */
/*                                 entry needs to be created in hardware     */
/*                                 even if software has non-default          */
/*                                 Qmap entries.                             */
/*                              - QOS_QMAP_SW_HW / QOS_QMAP_HW_ONLY          */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           :                                                      */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : QOS_SUCCESS or QOS_FAILURE                           */
/* Called By          : QoSAddDefTblEntries                                  */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QoSAddPcpQMapTblEntries (tQoSPortTblEntry * pPortTblNode)
{
    tQoSQMapNode       *pQMapNode = NULL;
    UINT4               u4CLASS = 0;
    INT4                i4IfIdx = pPortTblNode->i4IfIndex;
#ifdef NPAPI_WANTED
    INT4                i4RetStatus = QOS_FAILURE;
#endif
    UINT1               u1IPVal = 0;

    for (u1IPVal = 0; u1IPVal <= QOS_PM_TBL_DEF_ENTRY_MAX; u1IPVal++)
    {
        /* Create the entry in the Software alone */
        /* In NPAPI, the default mapping is handled in Porting layer */
        /* during NP Init */

        /* If the QueueMap Entry is already created Just update the */
        /* entry. No Need to create again */
        pQMapNode = QoSUtlGetQMapNode (i4IfIdx, u4CLASS,
                                       QOS_QMAP_PRI_TYPE_INT_PRI, u1IPVal);
        if (pQMapNode == NULL)
        {
            pQMapNode = QoSCreateQMapTblEntry (i4IfIdx, u4CLASS,
                                               QOS_QMAP_PRI_TYPE_INT_PRI,
                                               u1IPVal);
            if (pQMapNode == NULL)
            {
                return (QOS_FAILURE);
            }
        }

        /* Update the Entry  and dependent Tbls */
        pQMapNode->i4IfIndex = (UINT4) i4IfIdx;
        pQMapNode->u4CLASS = u4CLASS;
        pQMapNode->u1RegenPriType = QOS_QMAP_PRI_TYPE_INT_PRI;
        pQMapNode->u1RegenPri = u1IPVal;
        pQMapNode->u4QId =
            (UINT4) gau1QVal[pPortTblNode->i4PcpSelRow][u1IPVal] + 1;
        pQMapNode->u1Status = ACTIVE;
        pQMapNode->u1Type = QOS_PCP_ENTRY_TYPE_IMPLICIT;

        /* Programming the Queue mapping in hardware */
#ifdef NPAPI_WANTED
        i4RetStatus = QosxQoSHwMapClassToQueueId (NULL,
                                                  (INT4) pQMapNode->i4IfIndex,
                                                  pQMapNode->
                                                  u1RegenPriType,
                                                  pQMapNode->u1RegenPri,
                                                  pQMapNode->u4QId,
                                                  QOS_QMAP_ADD);

        if (i4RetStatus == QOS_FAILURE)
        {
            QOS_TRC_ARG1 (MGMT_TRC, "In %s : "
                          "QoSHwWrMapClassToQueue() : Add "
                          "Returns FAILURE. \r\n", __FUNCTION__);
            return (QOS_FAILURE);

        }
#endif
    }                            /*End of -for */

    return (QOS_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSModifyPcpTblEntries                               */
/* Description        : This function is used to modify an Entry in the      */
/*                      Port Table ,it will do the following Action          */
/*                      1. Modify the Entry in the Priority Map Table        */
/*                      2. Modify the Entry in the Policy Map Table          */
/*                      3. Modify the Entry in the Class Map Table           */
/* Input(s)           : pPortTblNode   - Pointer to PortTblNode              */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : gQoSGlobalInfo                                       */
/* Global Variables                                                          */
/* Modified           : gQoSGlobalInfo                                       */
/* Return Value(s)    : QOS_SUCCESS or QOS_FAILURE                           */
/* Called By          : SNMP Low Level                                       */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QoSModifyPcpTblEntries (tQoSPortTblEntry * pPortTblNode)
{
    /*1.Fetch the PRIMAP, PLYMAP, CLSMAP, QMAP Entries stored in the PortTable
     * Node */
    /*2.Modify the Entries as per the PCP MOdel chosen by the Administrator */
    /*3.Program the modified entries in Hardware */

    tQoSInPriorityMapNode *pPriMapNode = NULL;
    tQoSPolicyMapNode  *pPlyMapNode = NULL;
    tQoSClassMapNode   *pCMNode = NULL;
    tQoSClassInfoNode  *pClsInfoNode = NULL;
    tQoSQMapNode       *pQMapNode = NULL;
    INT4                i4RetStatus = QOS_FAILURE;
    UINT1               u1Priority = 0;
    UINT1               u1DEI = 0;
    UINT1               u1OutLoopIndex = 0;
    UINT1               u1InLoopIndex = 0;
    UINT1               u1TempVal = 0;
    UINT4               u4StartIndex = 0;

    u4StartIndex = (UINT4) pPortTblNode->i4PcpTblIndex;

    /*Getting the inner and outer loop index depending on the packet type */
    QOS_GET_PCP_OL_INDEX (pPortTblNode->i4PktType, u1OutLoopIndex);
    QOS_GET_PCP_IL_INDEX (pPortTblNode->i4PktType, u1InLoopIndex);

    for (u1Priority = 0; u1Priority <= u1OutLoopIndex; u1Priority++)
    {
        for (u1DEI = 0; u1DEI <= u1InLoopIndex; u1DEI++)
        {
            /*Get the PriorityMapNode and modify as per the Model */
            /*selected */
            pPriMapNode = QoSUtlGetPriorityMapNode (u4StartIndex);
            if (pPriMapNode != NULL)
            {
                /*Populating the Priority Map properties depending up on the
                 * Packet Type*/
                if (pPortTblNode->i4PktType == QOS_PCP_PKT_TYPE_DOT1P)
                {
                    pPriMapNode->u1InPriority = u1Priority;
                    pPriMapNode->i4InDEI = (INT4) u1DEI;
                    pPriMapNode->u1RegenPriority =
                        gau1PcpVal[pPortTblNode->i4PcpSelRow]
                        [(u1Priority << 1) + u1DEI];
                    pPriMapNode->i4RegenColor =
                        gau1ColorVal[pPortTblNode->i4PcpSelRow][pPriMapNode->
                                                                u1RegenPriority];
                    QoSUtlStoreOperEncodingInfo (pPriMapNode->u4IfIndex,
                                                 u1Priority, u1DEI,
                                                 pPriMapNode->u1RegenPriority,
                                                 pPortTblNode->i4PcpSelRow,
                                                 pPortTblNode->i4PktType,
                                                 QOS_PCP_TBL_ACTION_MODIFY);

                }
                /*Populating the Priority Map properties for IP Packet Type */
                if (pPortTblNode->i4PktType == QOS_PCP_PKT_TYPE_IP)
                {
                    u1TempVal = u1Priority / 8;
                    pPriMapNode->u1RegenPriority = u1TempVal;
                    pPriMapNode->i4RegenColor =
                        gau1ColorVal[pPortTblNode->i4PcpSelRow][u1TempVal];
                    QoSUtlStoreOperEncodingInfo (pPriMapNode->u4IfIndex,
                                                 u1Priority, 0,
                                                 pPriMapNode->u1RegenPriority,
                                                 pPortTblNode->i4PcpSelRow,
                                                 pPortTblNode->i4PktType,
                                                 QOS_PCP_TBL_ACTION_CREATE);

                }
                /*Populating the Priority Map properties for MPLS Packet Type */
                if (pPortTblNode->i4PktType == QOS_PCP_PKT_TYPE_MPLS)
                {
                    pPriMapNode->u1RegenPriority = u1Priority;
                    pPriMapNode->i4RegenColor =
                        gau1ColorVal[pPortTblNode->i4PcpSelRow][u1Priority];
                    QoSUtlStoreOperEncodingInfo (pPriMapNode->u4IfIndex,
                                                 u1Priority, 0,
                                                 pPriMapNode->u1RegenPriority,
                                                 pPortTblNode->i4PcpSelRow,
                                                 pPortTblNode->i4PktType,
                                                 QOS_PCP_TBL_ACTION_CREATE);

                }

                pPriMapNode->u1Status = ACTIVE;
                /*Programming the Priority Map Entries in Hardware */
                i4RetStatus = QoSHwWrMapClasstoPriMap (pPriMapNode, QOS_NP_ADD);
                if ((i4RetStatus == QOS_FAILURE)
                    || (i4RetStatus == QOS_NP_NOT_SUPPORTED))
                {
                    QOS_TRC_ARG1 (MGMT_TRC,
                                  "In %s : QoSHwWrMapClasstoPriMap "
                                  " () Returns FAILURE. \r\n", __FUNCTION__);

                    return (QOS_FAILURE);
                }
                /*Currently DSCP->DSCP marking is not supported in PCP Model selection */
                if (pPortTblNode->i4PktType != QOS_PCP_PKT_TYPE_IP)
                {
                    pCMNode = QoSUtlGetClassMapNode (u4StartIndex);
                    if (pCMNode == NULL)
                    {
                        QOS_TRC_ARG1 (MGMT_TRC,
                                      "In %s : QoSUtlGetPriorityMapNode () "
                                      "Returns FAILURE. \r\n", __FUNCTION__);

                        return (SNMP_FAILURE);
                    }

                    /* This Function is used to Add ClassMapIds to the respective 
                     * CLASS using the internal DataSrtuct 'tQoSClassInfoNode'
                     */
                    i4RetStatus = QoSClsInfoAddFilters (u4StartIndex, pCMNode);
                    if (i4RetStatus == QOS_FAILURE)
                    {
                        QOS_TRC_ARG1 (MGMT_TRC,
                                      "In %s : QoSClsInfoAddFilters () "
                                      "Returns FAILURE. \r\n", __FUNCTION__);

                        return (SNMP_FAILURE);
                    }
                    /*Fetching the Properties of Policy Map with the Index stored in 
                     * the Port Table*/

                    pPlyMapNode = QoSUtlGetPolicyMapNode (u4StartIndex);

                    /* Modifying the Policy Map Parameters as per the Model modified
                     * and packet type chosen*/
                    if (pPlyMapNode != NULL)
                    {
                        if (pPortTblNode->i4PktType == QOS_PCP_PKT_TYPE_DOT1P)
                        {
                            pPlyMapNode->i4VlanDE =
                                ((gau1DOT1PVal[pPortTblNode->i4PcpSelRow]
                                  [pPriMapNode->u1RegenPriority]) & 1);
                            gau1DEArray[pPortTblNode->i4IfIndex][pPriMapNode->
                                                                 u1RegenPriority]
                                = (UINT1) pPlyMapNode->i4VlanDE;
                            pPlyMapNode->u2DefaultPHB =
                                ((gau1DOT1PVal[pPortTblNode->i4PcpSelRow]
                                  [pPriMapNode->u1RegenPriority]) >> 1);
                            QoSUtlStoreOperDecodingInfo (pPlyMapNode->u4IfIndex,
                                                         pPriMapNode->
                                                         u1RegenPriority,
                                                         (UINT1) pPlyMapNode->
                                                         u2DefaultPHB,
                                                         (UINT1) pPlyMapNode->
                                                         i4VlanDE,
                                                         pPortTblNode->
                                                         i4PcpSelRow,
                                                         pPortTblNode->
                                                         i4PktType,
                                                         QOS_PCP_TBL_ACTION_MODIFY);

                        }
                        if (pPortTblNode->i4PktType == QOS_PCP_PKT_TYPE_MPLS)
                        {
                            pPlyMapNode->u1PHBType = QOS_PLY_PHB_TYPE_MPLS_EXP;
                            pPlyMapNode->u2DefaultPHB =
                                pPriMapNode->u1RegenPriority;
                            QoSUtlStoreOperDecodingInfo (pPlyMapNode->u4IfIndex,
                                                         pPriMapNode->
                                                         u1RegenPriority,
                                                         (UINT1) pPlyMapNode->
                                                         u2DefaultPHB, 0,
                                                         pPortTblNode->
                                                         i4PcpSelRow,
                                                         pPortTblNode->
                                                         i4PktType,
                                                         QOS_PCP_TBL_ACTION_MODIFY);

                        }
                        pPlyMapNode->i4RegenColor = pPriMapNode->i4RegenColor;
                        pPlyMapNode->u1Status = ACTIVE;
                    }
                    /*Giving NP trigger for programming the PolicyMap with modified
                     * parameters in HARDWARE*/
                    if (gQoSGlobalInfo.eSysStatus == QOS_SYS_STATUS_ENABLE)
                    {
                        pClsInfoNode = QoSUtlGetClassInfoNode (u4StartIndex);
                        if ((pClsInfoNode != NULL)
                            && (pClsInfoNode->pPlyMapNode != NULL))
                        {
                            /* if a Policy is already create for this Class 
                             * Get the Policy Map Add the Class (Filters) to it */
                            i4RetStatus =
                                QoSHwWrMapClassToPolicy (pClsInfoNode->
                                                         pPlyMapNode, pCMNode,
                                                         QOS_PLY_MAP);
                            if (i4RetStatus == QOS_FAILURE)
                            {
                                QOS_TRC_ARG1 (MGMT_TRC,
                                              "In %s : QoSHwWrMapClassToPolicy "
                                              " () Returns FAILURE. \r\n",
                                              __FUNCTION__);

                                return (SNMP_FAILURE);
                            }

                        }        /* End of If */
                    }

                    /* Add the entry from the Internal Reference. */
                    if (QOS_CM_TBL_DEF_CLASS != 0)
                    {
                        i4RetStatus =
                            QoSClsInfoAddPolicy (u4StartIndex, pPlyMapNode);
                        if (i4RetStatus == QOS_FAILURE)
                        {
                            QOS_TRC_ARG1 (MGMT_TRC,
                                          "In %s : QoSClsInfoAddPolicy () "
                                          "Returns FAILURE. \r\n",
                                          __FUNCTION__);

                            return (SNMP_FAILURE);
                        }
                    }
                }
                /*Fetching the QMAP Node with the Index stored in Port
                 * Table Node*/
                pQMapNode = QoSUtlGetQMapNode (pPortTblNode->i4IfIndex, 0,
                                               QOS_QMAP_PRI_TYPE_INT_PRI,
                                               pPriMapNode->u1RegenPriority);
                if (pQMapNode == NULL)
                {
                    return (QOS_FAILURE);
                }

                /*Updating the Queue Value depending on the Model chosen */
                pQMapNode->u4QId = (UINT4) gau1QVal[pPortTblNode->i4PcpSelRow]
                    [pQMapNode->u1RegenPri] + 1;
                pQMapNode->u1Status = ACTIVE;

                /*Programming the Queue Map entry in Hardware */
#ifdef NPAPI_WANTED
                i4RetStatus = QosxQoSHwMapClassToQueueId (NULL,
                                                          (INT4) pQMapNode->
                                                          i4IfIndex,
                                                          pQMapNode->
                                                          u1RegenPriType,
                                                          pQMapNode->u1RegenPri,
                                                          pQMapNode->u4QId,
                                                          QOS_QMAP_ADD);

                if (i4RetStatus == QOS_FAILURE)
                {
                    QOS_TRC_ARG1 (MGMT_TRC, "In %s : "
                                  "QoSHwWrMapClassToQueue() : Add "
                                  "Returns FAILURE. \r\n", __FUNCTION__);
                    return (QOS_FAILURE);

                }
#endif
            }
        }
        u4StartIndex++;
    }

    return QOS_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : QoSDeletePortTblEntry                                */
/* Description        : This function is used to delete an Entry in the      */
/*                      Port Table ,it will do the following Action          */
/*                      1. Add the Entry in the Priority Map Table           */
/* Input(s)           : pPortTblNode   - Pointer to PortTblNode              */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : gQoSGlobalInfo                                       */
/* Global Variables                                                          */
/* Modified           : gQoSGlobalInfo                                       */
/* Return Value(s)    : QOS_SUCCESS or QOS_FAILURE                           */
/* Called By          : SNMP Low Level                                       */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QoSDeletePortTblEntry (tQoSPortTblEntry * pPortTblNode)
{
    /*1.Fetch the PRIMAP, PLYMAP, CLSMAP, QMAP Entries stored in the PortTable
     * Node and send DELETE notification to Hardware, so that the entries will
     * be cleaned in Hardware*/
    /*2.Modify the Entries as per the PCP MOdel chosen by the Administrator */
    /*3.Program the modified entries in Hardware */
    tQoSInPriorityMapNode *pPriMapNode = NULL;
    tQoSPolicyMapNode  *pPlyMapNode = NULL;
    tQoSQMapNode       *pQMapNode = NULL;
    INT4                i4RetStatus = QOS_FAILURE;
    UINT4               u4StartIndex = (UINT4) pPortTblNode->i4PcpTblIndex;
    UINT1               u1Priority = 0;
    UINT1               u1DEI = 0;
    UINT1               u1OutLoopIndex = 0;
    UINT1               u1InLoopIndex = 0;

    QOS_GET_PCP_OL_INDEX (pPortTblNode->i4PktType, u1OutLoopIndex);
    QOS_GET_PCP_IL_INDEX (pPortTblNode->i4PktType, u1InLoopIndex);

    for (u1Priority = 0; u1Priority <= u1OutLoopIndex; u1Priority++)
    {
        for (u1DEI = 0; u1DEI <= u1InLoopIndex; u1DEI++)
        {
            /*Get the PriorityMapNode and modify as per the Model */
            /*selected */
            pPriMapNode = QoSUtlGetPriorityMapNode (u4StartIndex);
            if (pPriMapNode == NULL)
            {
                return (SNMP_FAILURE);
            }
            if (pPriMapNode != NULL)
            {
                i4RetStatus = QoSHwWrMapClasstoPriMap (pPriMapNode, QOS_NP_DEL);
                if ((i4RetStatus == QOS_NP_NOT_SUPPORTED)
                    || (i4RetStatus == QOS_FAILURE))
                {
                    if (i4RetStatus == QOS_NP_NOT_SUPPORTED)
                    {
                        CLI_SET_ERR (QOS_CLI_ERR_INVALID_PRIORITY_VALUE);
                    }
                    QOS_TRC_ARG1 (MGMT_TRC,
                                  "In %s : ERROR: QoSHwWrMapClasstoPriMap () "
                                  " Returns FAILURE. \r\n", __FUNCTION__);
                    return (SNMP_FAILURE);
                }
            }
            if (pPortTblNode->i4PktType != QOS_PCP_PKT_TYPE_IP)
            {
                pPlyMapNode = QoSUtlGetPolicyMapNode (u4StartIndex);
                if (pPlyMapNode != NULL)
                {
                    if ((pPlyMapNode->u1Status == ACTIVE) &&
                        (gQoSGlobalInfo.eSysStatus == QOS_SYS_STATUS_ENABLE))
                    {
                        /* 1. Remove the entry from the HARDWARE. */
                        i4RetStatus =
                            QoSHwWrUnmapClassFromPolicy (pPlyMapNode, NULL,
                                                         QOS_PLY_DEL);
                        if (i4RetStatus == QOS_FAILURE)
                        {
                            QOS_TRC_ARG1 (MGMT_TRC, "In %s : "
                                          "QoSHwWrUnmapClassFromPolicy () Returns"
                                          " FAILURE. \r\n", __FUNCTION__);

                            return (SNMP_FAILURE);
                        }
                        /*Delete All Class Map Entries in the HW */
                        if (pPlyMapNode->u4ClassId != 0)
                        {
                            i4RetStatus =
                                QoSHwWrUtilDeleteAllClsMapEntry (pPlyMapNode->
                                                                 u4ClassId);
                            if (i4RetStatus == QOS_FAILURE)
                            {
                                QOS_TRC_ARG1 (MGMT_TRC, "In %s : "
                                              "QoSHwWrUtilDeleteAllClsMapEntry () "
                                              "Returns FAILURE. \r\n",
                                              __FUNCTION__);
                                return (SNMP_FAILURE);
                            }
                        }
                    }
                    pPlyMapNode->u1Status = NOT_IN_SERVICE;
                }
            }

            pQMapNode = QoSUtlGetQMapNode (pPortTblNode->i4IfIndex, 0,
                                           QOS_QMAP_PRI_TYPE_INT_PRI,
                                           pPriMapNode->u1RegenPriority);
            if (pQMapNode != NULL)
            {
                if ((pQMapNode->u1Status == ACTIVE) &&
                    (gQoSGlobalInfo.eSysStatus == QOS_SYS_STATUS_ENABLE))
                {
#ifdef NPAPI_WANTED
                    i4RetStatus = QosxQoSHwMapClassToQueueId (NULL,
                                                              (INT4) pQMapNode->
                                                              i4IfIndex,
                                                              pQMapNode->
                                                              u1RegenPriType,
                                                              pQMapNode->
                                                              u1RegenPri,
                                                              pQMapNode->u4QId,
                                                              QOS_QMAP_DEL);
                    if (i4RetStatus == QOS_FAILURE)
                    {
                        QOS_TRC_ARG1 (MGMT_TRC, "In %s : "
                                      "QoSHwWrMapClassToQueue() : Del "
                                      "Returns FAILURE. \r\n", __FUNCTION__);
                        return (SNMP_FAILURE);

                    }
#endif
                }
                pQMapNode->u1Status = NOT_IN_SERVICE;
            }
            u4StartIndex++;
        }
    }

    return QOS_SUCCESS;
}

/*****************************************************************************/
/*                     PORT TABLE FUNCTIONS                                  */
/*****************************************************************************/
/*****************************************************************************/
/* Function Name      : QoSPortTblCmpFun                                    */
/* Description        : This function is used as a Compare function in the   */
/*                      RBTree.                                              */
/*                      1.It Compares the u4IfIndex and return the values*/
/* Input(s)           : e1 , e2 -  Elements of RBTree                        */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    :  -1/0/1                                              */
/* Called By          : RBTreeUtility                                        */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QoSPortTblCmpFun (tRBElem * e1, tRBElem * e2)
{
    tQoSPortTblEntry   *pPortTblNode1 = NULL;
    tQoSPortTblEntry   *pPortTblNode2 = NULL;

    pPortTblNode1 = (tQoSPortTblEntry *) e1;
    pPortTblNode2 = (tQoSPortTblEntry *) e2;

    /* Compare the Interface Index */

    if (pPortTblNode1->i4IfIndex < pPortTblNode2->i4IfIndex)
    {
        return (-1);
    }
    else if (pPortTblNode1->i4IfIndex > pPortTblNode2->i4IfIndex)
    {
        return (1);
    }
    else
    {
        if (pPortTblNode1->i4PktType < pPortTblNode2->i4PktType)
        {
            return (-1);
        }
        else if (pPortTblNode1->i4PktType > pPortTblNode2->i4PktType)
        {
            return (1);
        }
        else
        {
            return (0);
        }
    }

}

/*****************************************************************************/
/* Function Name      : QoSUtlGetPortTblNode                                 */
/* Description        : This function is used to Get the Entry  form  the    */
/*                      Port Table ,it will do the following Action          */
/*                      1. Call RBTreeGet Utility.                           */
/* Input(s)           : u4IfIndex      - Index to PortTblNode                */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : gQoSGlobalInfo                                       */
/* Global Variables                                                          */
/* Modified           :                                                      */
/* Return Value(s)    : NULL/ Pointer to 'tQoSPortTblEntry'                  */
/* Called By          : SNMP Low Level                                       */
/* Calling Function   :                                                      */
/*****************************************************************************/
tQoSPortTblEntry   *
QoSUtlGetPortTblNode (INT4 i4IfIndex, INT4 i4PktType)
{
    tQoSPortTblEntry    TempPortTblNode;
    tQoSPortTblEntry   *pPortTblNode = NULL;

    MEMSET (&TempPortTblNode, 0, sizeof (tQoSPortTblEntry));
    TempPortTblNode.i4IfIndex = i4IfIndex;
    TempPortTblNode.i4PktType = i4PktType;

    pPortTblNode = (tQoSPortTblEntry *)
        RBTreeGet (gQoSGlobalInfo.pRbPortTbl, (tRBElem *) & TempPortTblNode);

    if (pPortTblNode == NULL)
    {
        QOS_TRC_ARG2 (MGMT_TRC, "%s : Port Number %d is not Found in the "
                      "Port Table. \r\n", __FUNCTION__, i4IfIndex);
    }

    return (pPortTblNode);
}

/*****************************************************************************/
/* Function Name      : QoSCreatePortTblEntry                                */
/* Description        : This function is used to Create an Entry in   the    */
/*                      Port Table ,it will do the following Action          */
/*                      1. Allocate a memory block for the New Entry         */
/*                      2. Initialize the New Entry                          */
/*                      3. Add this New Entry into the Port Table            */
/* Input(s)           : i4IfIndex      - Index to PortTblNode                */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : gQoSGlobalInfo                                       */
/* Global Variables                                                          */
/* Modified           : gQoSGlobalInfo                                       */
/* Return Value(s)    : NULL / Pointer to  QMapNode                          */
/* Called By          : SNMP Low Level                                       */
/* Calling Function   :                                                      */
/*****************************************************************************/
tQoSPortTblEntry   *
QoSCreatePortTblEntry (INT4 i4IfIndex, INT4 i4PktType)
{
    UINT4               u4RetStatus = QOS_FAILURE;
    tQoSPortTblEntry   *pNewPortTblNode = NULL;

    /* 1. Allocate a memory block for the New Entry */
    pNewPortTblNode = (tQoSPortTblEntry *)
        MemAllocMemBlk (gQoSGlobalInfo.QoSPortTblMemPoolId);
    if (pNewPortTblNode == NULL)
    {
        QOS_TRC_ARG3 (MGMT_TRC, "In %s : MemAllocMemBlk () Failed ,"
                      "for IfIndex %d.%d \r\n", __FUNCTION__,
                      i4IfIndex, i4PktType);
        return (NULL);
    }

    /* 2. Initialize the New Entry */
    MEMSET (pNewPortTblNode, 0, sizeof (tQoSPortTblEntry));

    pNewPortTblNode->i4IfIndex = i4IfIndex;
    pNewPortTblNode->i4PktType = i4PktType;
    pNewPortTblNode->u1Status = CREATE_AND_WAIT;

    /* 3. Add this New Entry into the Port Table */
    u4RetStatus = RBTreeAdd (gQoSGlobalInfo.pRbPortTbl,
                             (tRBElem *) pNewPortTblNode);
    if (u4RetStatus != RB_SUCCESS)
    {
        u4RetStatus =
            MemReleaseMemBlock (gQoSGlobalInfo.QoSPortTblMemPoolId,
                                (UINT1 *) pNewPortTblNode);

        if (u4RetStatus == MEM_FAILURE)
        {
            QOS_TRC_ARG3 (MGMT_TRC, "In %s : MemReleaseMemBlock () Failed ,"
                          "for IfIndex %d.%d \r\n", __FUNCTION__,
                          i4IfIndex, i4PktType);

            return (NULL);
        }

        QOS_TRC_ARG3 (MGMT_TRC, "In %s : RBTreeAdd () Failed ,"
                      "for IfIndex %d.%d \r\n", __FUNCTION__,
                      i4IfIndex, i4PktType);
        return (NULL);
    }

    return (pNewPortTblNode);
}

/*****************************************************************************/
/* Function Name      : QoSAddPortTblEntry                                   */
/* Description        : This function is used to add an Entry in the         */
/*                      Port Table ,it will do the following Action          */
/*                      1. Add the Entry in the Priority Map Table           */
/* Input(s)           : pPortTblNode   - Pointer to PortTblNode              */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : gQoSGlobalInfo                                       */
/* Global Variables                                                          */
/* Modified           : gQoSGlobalInfo                                       */
/* Return Value(s)    : QOS_SUCCESS or QOS_FAILURE                           */
/* Called By          : SNMP Low Level                                       */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QoSAddPortTblEntry (tQoSPortTblEntry * pPortTblNode)
{
    tQoSPortTblEntry   *pTmpPortTblNode = NULL;
    UINT4               u4RetStatus = QOS_FAILURE;

    /* 1. Get this Entry from the Priority Map Unique Table
     * if it fails add it.*/
    pTmpPortTblNode = RBTreeGet (gQoSGlobalInfo.pRbPortTbl,
                                 (tRBElem *) pPortTblNode);
    if (pTmpPortTblNode == NULL)
    {
        /* 2. Add this New Entry into the Priority Map Unique Table */
        u4RetStatus = RBTreeAdd (gQoSGlobalInfo.pRbPortTbl,
                                 (tRBElem *) pPortTblNode);
        if (u4RetStatus != RB_SUCCESS)
        {
            return (QOS_FAILURE);
        }
    }
    return (QOS_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSGetNextPortTblEntryIndex                          */
/* Description        : This function is used to Get the Next Entry from the */
/*                      Port Table ,it will do the following Action  */
/*                      1. If u4CurrentIndex is 0 then  GetFirxtIndex.       */
/*                      2. If u4CurrentIndex is not 0 then  GetNextIndex.    */
/* Input(s)           : u4CurrentIndex  - Current index id                   */
/*                      pu4NextIndex    - Pointer to NextIndex Id            */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : gQoSGlobalInfo                                       */
/* Global Variables                                                          */
/* Modified           : gQoSGlobalInfo                                       */
/* Return Value(s)    : QOS_SUCCESS or QOS_FAILURE                           */
/* Called By          : SNMP Low Level                                       */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QoSGetNextPortTblEntryIndex (INT4 i4CurrentIndex1, INT4 *pi4NextIndex1,
                             INT4 i4CurrentIndex2, INT4 *pi4NextIndex2)
{
    tQoSPortTblEntry   *pPortTblNode = NULL;
    tQoSPortTblEntry    PortTblNode;

    if ((i4CurrentIndex1 == 0) && (i4CurrentIndex2 == 0))
    {
        pPortTblNode = (tQoSPortTblEntry *)
            RBTreeGetFirst (gQoSGlobalInfo.pRbPortTbl);
    }
    else
    {
        PortTblNode.i4IfIndex = i4CurrentIndex1;
        PortTblNode.i4PktType = i4CurrentIndex2;
        pPortTblNode = (tQoSPortTblEntry *)
            RBTreeGetNext (gQoSGlobalInfo.pRbPortTbl,
                           &PortTblNode, QoSPortTblCmpFun);
    }

    if (pPortTblNode == NULL)
    {
        QOS_TRC_ARG2 (MGMT_TRC, "In %s : RBTreeGetNext () Failed, "
                      " for IfIndex %d.\r\n", __FUNCTION__, i4CurrentIndex1);

        return (QOS_FAILURE);
    }

    *pi4NextIndex1 = pPortTblNode->i4IfIndex;
    *pi4NextIndex2 = pPortTblNode->i4PktType;

    return (QOS_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSUtlValidatePortTblIdxInst                         */
/* Description        : This function is used to Validate the Given Index    */
/*                      instance in the Port Table, if it is ACTIVE          */
/*                      return Failure Success otherwise.                    */
/* Input(s)           : u4FsQoSPortNum - Port Number                         */
/*                      pu4ErrorCode     - Pointer to Error Code             */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : gQoSGlobalInfo                                       */
/* Global Variables                                                          */
/* Modified           : gQoSGlobalInfo                                       */
/* Return Value(s)    : QOS_SUCCESS or QOS_FAILURE                           */
/* Called By          : SNMP Low Level                                       */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QoSUtlValidatePortTblIdxInst (INT4 i4IfIndex, INT4 i4PktType,
                              UINT4 *pu4ErrorCode)
{
    INT4                i4RetStatus = 0;
    tQoSPortTblEntry   *pPortTblNode = NULL;

    if (gQoSGlobalInfo.eSysControl != QOS_SYS_CNTL_START)
    {
        QOS_TRC (MGMT_TRC, QOS_MODULE_SHUTDOW_ERROR_MSG);

        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;

        CLI_SET_ERR (QOS_CLI_ERR_MODULE_SHUTDOW);

        return (QOS_FAILURE);
    }

    /* Check the Range of Index Values (Min-Max) */
    i4RetStatus = QOS_CHECK_PORT_TBL_INDEX_RANGE (i4IfIndex, i4PktType);

    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG3 (MGMT_TRC, "%s : Interface Index %d is out of Range."
                      " The Range Should be (1-%d). \r\n", __FUNCTION__,
                      i4IfIndex, QOS_PORT_TBL_MAX_INDEX_RANGE);

        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;

        CLI_SET_ERR (QOS_CLI_ERR_PRI_MAP_RANGE);

        return (QOS_FAILURE);
    }

    pPortTblNode = QoSUtlGetPortTblNode (i4IfIndex, i4PktType);

    if (pPortTblNode == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetPortTblNode () Returns "
                      "FAILURE. \r\n", __FUNCTION__);

        *pu4ErrorCode = SNMP_ERR_NO_CREATION;

        CLI_SET_ERR (QOS_CLI_ERR_PORT_TBL_NO_ENTRY);

        return (QOS_FAILURE);
    }

    return (QOS_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSPortTblRBTFreeNodeFn                              */
/* Description        : This function is used to Delete the Port             */
/*                      Table Entries and Release the Memory to the          */
/*                      respective Mem Pool .                                */
/* Input(s)           : tRBElem     - Pointer to Port Table Entry            */
/*                      u4Arg       - Unused Param                           */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : QOS_SUCCESS /QOS_FAILURE                             */
/* Called By          : RBTreeUtility                                        */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QoSPortTblRBTFreeNodeFn (tRBElem * pRBElem, UINT4 u4Arg)
{
    UINT4               u4RetStatus = MEM_FAILURE;

    UNUSED_PARAM (u4Arg);

    if (pRBElem == NULL)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : InPriTbl Entry is NULL.\r\n",
                      __FUNCTION__);

        return (QOS_FAILURE);
    }

    MEMSET (pRBElem, 0, sizeof (tQoSPortTblEntry));

    /* Release the Mem Block */
    u4RetStatus = MemReleaseMemBlock (gQoSGlobalInfo.QoSPortTblMemPoolId,
                                      (UINT1 *) pRBElem);

    if (u4RetStatus != MEM_SUCCESS)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : PortTbl- "
                      "MemReleaseMemBlock () Failed.\r\n", __FUNCTION__);
    }

    return (QOS_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QosUtlGetClassMapEntryType                           */
/* Description        : This function is used to get the Entry Type          */
/*                      of ClassMap table                                    */
/* Input(s)           : ClassMapId    - ClassMap Index                       */
/* Output(s)          : Entry type                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : QOS_SUCCESS /QOS_FAILURE                             */
/* Called By          : RBTreeUtility                                        */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QosUtlGetClassMapEntryType (UINT4 u4ClassMapId, INT4 *pi4EntryType)
{
    tQoSClassMapNode   *pClsMapNode = NULL;
    pClsMapNode = QoSUtlGetClassMapNode (u4ClassMapId);
    if (pClsMapNode != NULL)
    {
        *pi4EntryType = (INT4) pClsMapNode->u1Type;
    }
    return QOS_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : QosUtlGetPriMapEntryType                             */
/* Description        : This function is used to get the Entry Type          */
/*                      of PriorityMap table                                 */
/* Input(s)           : PriMapId    - PriMap Index                           */
/* Output(s)          : Entry type                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : QOS_SUCCESS /QOS_FAILURE                             */
/* Called By          : RBTreeUtility                                        */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QosUtlGetPriMapEntryType (UINT4 u4PriMapId, INT4 *pi4EntryType)
{
    tQoSInPriorityMapNode *pPriMapNode = NULL;

    pPriMapNode = QoSUtlGetPriorityMapNode (u4PriMapId);
    if (pPriMapNode != NULL)
    {
        *pi4EntryType = (INT4) pPriMapNode->u1Type;
    }
    return QOS_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : QosUtlGetPlyMapEntryType                             */
/* Description        : This function is used to get the Entry Type          */
/*                      of PlyMap table                                      */
/* Input(s)           : PlyMapId    - PlyMap Index                           */
/* Output(s)          : Entry type                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : QOS_SUCCESS /QOS_FAILURE                             */
/* Called By          : RBTreeUtility                                        */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QosUtlGetPlyMapEntryType (UINT4 u4PlyMapId, INT4 *pi4EntryType)
{
    tQoSPolicyMapNode  *pPlyMapNode = NULL;

    pPlyMapNode = QoSUtlGetPolicyMapNode (u4PlyMapId);
    if (pPlyMapNode != NULL)
    {
        *pi4EntryType = (INT4) pPlyMapNode->u1Type;
    }
    return QOS_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : QosUtlGetQMapEntryType                               */
/* Description        : This function is used to get the Entry Type          */
/*                      of QMap table                                        */
/* Input(s)           : IfIndex    - Interface Index                         */
/*                    : u4CLASS    - Class                                   */
/*                    : i4PriType  - Priority Type                           */
/*                    : u4priVal   - Priority Value                          */
/* Output(s)          : Entry type                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : QOS_SUCCESS /QOS_FAILURE                             */
/* Called By          : RBTreeUtility                                        */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QosUtlGetQMapEntryType (INT4 i4IfIdx, UINT4 u4CLASS, INT4 i4PriType,
                        UINT4 u4PriVal, INT4 *pi4EntryType)
{
    tQoSQMapNode       *pQMapNode = NULL;

    pQMapNode = QoSUtlGetQMapNode (i4IfIdx, u4CLASS, i4PriType, u4PriVal);
    if (pQMapNode != NULL)
    {
        *pi4EntryType = (INT4) pQMapNode->u1Type;
    }
    return QOS_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : QosUtlValidateDEValue                                */
/* Description        : This function is used to validate the DE value       */
/*                      configured                                           */
/* Input(s)           : PlyMapNode    - PlyMap Node                          */
/* Output(s)          : Entry type                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : QOS_SUCCESS /QOS_FAILURE                             */
/* Called By          : RBTreeUtility                                        */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QosUtlValidateDEValue (tQoSPolicyMapNode * pPlyMapNode)
{
    tQoSPortTblEntry   *pPortTblNode = NULL;
    UINT1               u1IPVal = 0;
    UINT1               u1NoOfDEConfig = 0;
    UINT1               u1NoOfDEAllowed = 0;

    if ((pPlyMapNode != NULL) && (pPlyMapNode->u4IfIndex != 0))
    {
        for (u1IPVal = 0; u1IPVal <= QOS_PM_TBL_DEF_ENTRY_MAX; u1IPVal++)
        {
            if (gau1DEArray[pPlyMapNode->u4IfIndex][u1IPVal] != 0)
            {
                u1NoOfDEConfig++;
            }
        }
    }

    if (pPlyMapNode != NULL)
    {
        pPortTblNode = QoSUtlGetPortTblNode ((INT4) pPlyMapNode->u4IfIndex,
                                             QOS_PCP_PKT_TYPE_DOT1P);
        if (pPortTblNode != NULL)
        {
            u1NoOfDEAllowed = (UINT1) pPortTblNode->i4PcpSelRow;
        }
        if (u1NoOfDEConfig == u1NoOfDEAllowed)
        {
            return QOS_FAILURE;
        }
    }
    return QOS_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : QoSUtlStoreOperEncodingInfo                          */
/* Description        : This function is used to store the PCP Encoding      */
/*                      Information for the particular port                  */
/* Input(s)           : u4IfIndex - Interface Index                          */
/*                      u1Priority - Incoming Priority                       */
/*                      u1DEI - Incoming DEI value                           */
/*                      u1PcpValue - PCP value                               */
/* Output(s)          : Entry type                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : QOS_SUCCESS /QOS_FAILURE                             */
/* Called By          : RBTreeUtility                                        */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QoSUtlStoreOperEncodingInfo (UINT4 u4IfIndex, UINT1 u1Priority,
                             UINT1 u1DEI, UINT1 u1PcpValue,
                             INT4 i4PcpSelRow, INT4 i4PktType, INT4 i4Action)
{
    INT4                i4Index = 0;
    INT1                i1FoundFlag = QOS_FAILURE;

    if (i4Action == QOS_PCP_TBL_ACTION_CREATE)
    {
        for (i4Index = 0; i4Index < QOS_PCP_MAX_ENTRY; i4Index++)
        {
            if (gaQoSPcpEncodingInfo[i4Index].u4IfIndex == 0)
            {
                /*Free Index Found */
                i1FoundFlag = QOS_SUCCESS;
                break;
            }
        }
        if (i1FoundFlag == QOS_FAILURE)
        {
            /*Maximum Filter Entry saved */
            return QOS_FAILURE;
        }

        gaQoSPcpEncodingInfo[i4Index].u4IfIndex = u4IfIndex;
        gaQoSPcpEncodingInfo[i4Index].i4PktType = i4PktType;
        gaQoSPcpEncodingInfo[i4Index].i4PcpSelRow = i4PcpSelRow;
        gaQoSPcpEncodingInfo[i4Index].u1InPriority = u1Priority;
        gaQoSPcpEncodingInfo[i4Index].u1InDEI = u1DEI;
        gaQoSPcpEncodingInfo[i4Index].u1PcpValue = u1PcpValue;
    }
    else
    {
        for (i4Index = 0; i4Index < QOS_PCP_MAX_ENTRY; i4Index++)
        {
            if ((gaQoSPcpEncodingInfo[i4Index].u4IfIndex == u4IfIndex) &&
                (gaQoSPcpEncodingInfo[i4Index].u1InPriority == u1Priority) &&
                (gaQoSPcpEncodingInfo[i4Index].u1InDEI == u1DEI) &&
                (gaQoSPcpEncodingInfo[i4Index].i4PktType == i4PktType))

            {
                gaQoSPcpEncodingInfo[i4Index].u1PcpValue = u1PcpValue;
                /*Free Index Found */
                i1FoundFlag = QOS_SUCCESS;
                break;
            }
        }

    }
    return QOS_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : QoSUtlStoreOperDecodingInfo                          */
/* Description        : This function is used to store the PCP Decoding      */
/*                      Information for the particular port                  */
/* Input(s)           : u4IfIndex - Interface Index                          */
/*                      u1Priority - Outgoing Priority                       */
/*                      u1DEI - Outgoing DEI value                           */
/*                      u1PcpValue - PCP value                               */
/* Output(s)          : Entry type                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : QOS_SUCCESS /QOS_FAILURE                             */
/* Called By          : RBTreeUtility                                        */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QoSUtlStoreOperDecodingInfo (UINT4 u4IfIndex, UINT1 u1PcpValue,
                             UINT1 u1Priority, UINT1 u1DEI,
                             INT4 i4PcpSelRow, INT4 i4PktType, INT4 i4Action)
{
    INT4                i4Index = 0;
    INT1                i1FoundFlag = QOS_FAILURE;

    if (i4Action == QOS_PCP_TBL_ACTION_CREATE)
    {
        for (i4Index = 0; i4Index < QOS_PCP_MAX_ENTRY; i4Index++)
        {
            if (gaQoSPcpDecodingInfo[i4Index].u4IfIndex == 0)
            {
                /*Free Index Found */
                i1FoundFlag = QOS_SUCCESS;
                break;
            }
        }
        if (i1FoundFlag == QOS_FAILURE)
        {
            /*Maximum Filter Entry saved */
            return QOS_FAILURE;
        }

        gaQoSPcpDecodingInfo[i4Index].u4IfIndex = u4IfIndex;
        gaQoSPcpEncodingInfo[i4Index].i4PktType = i4PktType;
        gaQoSPcpEncodingInfo[i4Index].i4PcpSelRow = i4PcpSelRow;
        gaQoSPcpDecodingInfo[i4Index].u1OutPriority = u1Priority;
        gaQoSPcpDecodingInfo[i4Index].u1OutDEI = u1DEI;
        gaQoSPcpDecodingInfo[i4Index].u1PcpValue = u1PcpValue;
    }
    else
    {
        for (i4Index = 0; i4Index < QOS_PCP_MAX_ENTRY; i4Index++)
        {
            if ((gaQoSPcpDecodingInfo[i4Index].u4IfIndex == u4IfIndex) &&
                (gaQoSPcpDecodingInfo[i4Index].u1PcpValue == u1PcpValue) &&
                (gaQoSPcpEncodingInfo[i4Index].i4PktType == i4PktType))

            {
                gaQoSPcpDecodingInfo[i4Index].u1OutPriority = u1Priority;
                gaQoSPcpDecodingInfo[i4Index].u1OutDEI = u1DEI;
                /*Free Index Found */
                i1FoundFlag = QOS_SUCCESS;
                break;
            }
        }

    }
    return QOS_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : QosUtlUpdInProExcActionDrop                      */
/*                                                                           */
/*    Description         : This function is updates the                     */
/*                          InProfileExceedActionFlag to DROP                */
/*                                                                           */
/*    Input(s)            : pPlyMapNode - Policy Map Node                    */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : None                                             */
/*                                                                           */
/*****************************************************************************/
VOID
QosUtlUpdInProExcActionDrop (tQoSPolicyMapNode * pPlyMapNode)
{
    pPlyMapNode->u2InProActExceedVlanPrio = 0;
    pPlyMapNode->u2InProActExceedVlanDE = 0;
    pPlyMapNode->u2InProActExceedInnerVlanPrio = 0;
    pPlyMapNode->u2InProActExceedInnerVlanDE = 0;
    pPlyMapNode->u1InProActExceedDscpOrToS = 0;
}

VOID
QoSResetPlyMap (tQoSPolicyMapNode * pPlyMapNode)
{
    /* Reset Conform, Exceed, Violate action flags */
    pPlyMapNode->u1InProfConfActFlag = 0;
    pPlyMapNode->u1InProfExcActFlag = 0;
    pPlyMapNode->u1OutProActionFlag = 0;
    /* Reset conform action attributes */
    pPlyMapNode->u4InProActConfNewClassId = 0;
    pPlyMapNode->u4InProfileActionPort = 0;
    pPlyMapNode->u4InProfileActionId = 0;
    pPlyMapNode->u4InProfileTrafficClass = 0;
    pPlyMapNode->u1InProActConfDscpOrToS = 0;
    pPlyMapNode->u1InProActConfMplsExp = 0;
    pPlyMapNode->u2InProActConfVlanPrio = 0;
    pPlyMapNode->u2InProActConfVlanDE = 0;
    pPlyMapNode->u2InProActConfInnerVlanPrio = 0;
    pPlyMapNode->u2InProActConfInnerVlanDE = 0;
    /* Reset exceed action attributes */
    pPlyMapNode->u4InProActExccedNewClassId = 0;
    pPlyMapNode->u4InProActExceedActionId = 0;
    pPlyMapNode->u4InProActExceedTrafficClass = 0;
    pPlyMapNode->u1InProActExceedDscpOrToS = 0;
    pPlyMapNode->u1InProActExceedMplsExp = 0;
    pPlyMapNode->u2InProActExceedVlanPrio = 0;
    pPlyMapNode->u2InProActExceedVlanDE = 0;
    pPlyMapNode->u2InProActExceedInnerVlanPrio = 0;
    pPlyMapNode->u2InProActExceedInnerVlanDE = 0;
    /* Reset violate action attributes */
    pPlyMapNode->u4OutProActNewClassId = 0;
    pPlyMapNode->u4OutProActionId = 0;
    pPlyMapNode->u4OutProActTrafficClass = 0;
    pPlyMapNode->u1OutProActDscpOrTos = 0;
    pPlyMapNode->u1OutProActVlanPrio = 0;
    pPlyMapNode->u2OutProActVlanDE = 0;
    pPlyMapNode->u1OutProActMplsExp = 0;
    pPlyMapNode->u2OutProActInnerVlanPrio = 0;
    pPlyMapNode->u2OutProActInnerVlanDE = 0;
}

#endif /* _QOS_UTL_C_ */
