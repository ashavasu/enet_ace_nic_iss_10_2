/********************************************************************
 * * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * *
 * * $Id: fsqosxwr.c,v 1.17 2016/07/05 07:38:44 siva Exp $
 * *
 * * Description: Protocol Low Level Routines
 * *********************************************************************/

# include  "lr.h"
# include  "fssnmp.h"
# include  "fsqosxwr.h"
# include  "fsqosxdb.h"
# include  "qosinc.h"

VOID
RegisterFSQOSX ()
{
    SNMPRegisterMibWithLock (&fsqosxOID, &fsqosxEntry, QoSLock, QoSUnLock,
                             SNMP_MSR_TGR_TRUE);
    SNMPAddSysorEntry (&fsqosxOID, (const UINT1 *) "fsqosxtd");
}

VOID
UnRegisterFSQOSX ()
{
    SNMPUnRegisterMib (&fsqosxOID, &fsqosxEntry);
    SNMPDelSysorEntry (&fsqosxOID, (const UINT1 *) "fsqosxtd");
}

INT4
FsQoSSystemControlGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsQoSSystemControl (&(pMultiData->i4_SLongValue)));
}

INT4
FsQoSStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsQoSStatus (&(pMultiData->i4_SLongValue)));
}

INT4
FsQoSTrcFlagGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsQoSTrcFlag (&(pMultiData->u4_ULongValue)));
}

INT4
FsQoSRateUnitGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsQoSRateUnit (&(pMultiData->i4_SLongValue)));
}

INT4
FsQoSRateGranularityGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsQoSRateGranularity (&(pMultiData->u4_ULongValue)));
}

INT4
FsQoSSystemControlSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsQoSSystemControl (pMultiData->i4_SLongValue));
}

INT4
FsQoSStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsQoSStatus (pMultiData->i4_SLongValue));
}

INT4
FsQoSTrcFlagSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsQoSTrcFlag (pMultiData->u4_ULongValue));
}

INT4
FsQoSSystemControlTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                        tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsQoSSystemControl (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FsQoSStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                 tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsQoSStatus (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FsQoSTrcFlagTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                  tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsQoSTrcFlag (pu4Error, pMultiData->u4_ULongValue));
}

INT4
FsQoSSystemControlDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                       tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsQoSSystemControl
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsQoSStatusDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsQoSStatus (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsQoSTrcFlagDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                 tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsQoSTrcFlag (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFsQoSPriorityMapTable (tSnmpIndex * pFirstMultiIndex,
                                   tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsQoSPriorityMapTable
            (&(pNextMultiIndex->pIndex[0].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsQoSPriorityMapTable
            (pFirstMultiIndex->pIndex[0].u4_ULongValue,
             &(pNextMultiIndex->pIndex[0].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
FsQoSPriorityMapNameGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsQoSPriorityMapTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsQoSPriorityMapName (pMultiIndex->pIndex[0].u4_ULongValue,
                                        pMultiData->pOctetStrValue));

}

INT4
FsQoSPriorityMapIfIndexGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsQoSPriorityMapTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsQoSPriorityMapIfIndex (pMultiIndex->pIndex[0].u4_ULongValue,
                                           &(pMultiData->u4_ULongValue)));

}

INT4
FsQoSPriorityMapVlanIdGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsQoSPriorityMapTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsQoSPriorityMapVlanId (pMultiIndex->pIndex[0].u4_ULongValue,
                                          &(pMultiData->u4_ULongValue)));

}

INT4
FsQoSPriorityMapInPriorityGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsQoSPriorityMapTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsQoSPriorityMapInPriority
            (pMultiIndex->pIndex[0].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsQoSPriorityMapInPriTypeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsQoSPriorityMapTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsQoSPriorityMapInPriType
            (pMultiIndex->pIndex[0].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsQoSPriorityMapRegenPriorityGet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsQoSPriorityMapTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsQoSPriorityMapRegenPriority
            (pMultiIndex->pIndex[0].u4_ULongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsQoSPriorityMapRegenInnerPriorityGet (tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsQoSPriorityMapTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsQoSPriorityMapRegenInnerPriority
            (pMultiIndex->pIndex[0].u4_ULongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsQoSPriorityMapConfigStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsQoSPriorityMapTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsQoSPriorityMapConfigStatus
            (pMultiIndex->pIndex[0].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsQoSPriorityMapStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsQoSPriorityMapTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsQoSPriorityMapStatus (pMultiIndex->pIndex[0].u4_ULongValue,
                                          &(pMultiData->i4_SLongValue)));

}
INT4 FsQoSPriorityMapInDEIGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsQoSPriorityMapTable(
        pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return(nmhGetFsQoSPriorityMapInDEI(
        pMultiIndex->pIndex[0].u4_ULongValue,
        &(pMultiData->i4_SLongValue)));
 
}
 
INT4 FsQoSPriorityMapRegenColorGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsQoSPriorityMapTable(
        pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return(nmhGetFsQoSPriorityMapRegenColor(
        pMultiIndex->pIndex[0].u4_ULongValue,
        &(pMultiData->i4_SLongValue)));
 
}

INT4
FsQoSPriorityMapNameSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsQoSPriorityMapName (pMultiIndex->pIndex[0].u4_ULongValue,
                                        pMultiData->pOctetStrValue));

}

INT4
FsQoSPriorityMapIfIndexSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsQoSPriorityMapIfIndex (pMultiIndex->pIndex[0].u4_ULongValue,
                                           pMultiData->u4_ULongValue));

}

INT4
FsQoSPriorityMapVlanIdSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsQoSPriorityMapVlanId (pMultiIndex->pIndex[0].u4_ULongValue,
                                          pMultiData->u4_ULongValue));

}

INT4
FsQoSPriorityMapInPrioritySet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsQoSPriorityMapInPriority
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->i4_SLongValue));

}

INT4
FsQoSPriorityMapInPriTypeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsQoSPriorityMapInPriType
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->i4_SLongValue));

}

INT4
FsQoSPriorityMapRegenPrioritySet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    return (nmhSetFsQoSPriorityMapRegenPriority
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->u4_ULongValue));

}

INT4
FsQoSPriorityMapRegenInnerPrioritySet (tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    return (nmhSetFsQoSPriorityMapRegenInnerPriority
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->u4_ULongValue));

}

INT4
FsQoSPriorityMapStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsQoSPriorityMapStatus (pMultiIndex->pIndex[0].u4_ULongValue,
                                          pMultiData->i4_SLongValue));
}

INT4 FsQoSPriorityMapInDEISet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsQoSPriorityMapInDEI(
        pMultiIndex->pIndex[0].u4_ULongValue,
        pMultiData->i4_SLongValue));
 
}
 
INT4 FsQoSPriorityMapRegenColorSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsQoSPriorityMapRegenColor(
        pMultiIndex->pIndex[0].u4_ULongValue,
        pMultiData->i4_SLongValue));
}

INT4
FsQoSPriorityMapNameTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                          tRetVal * pMultiData)
{
    return (nmhTestv2FsQoSPriorityMapName (pu4Error,
                                           pMultiIndex->pIndex[0].u4_ULongValue,
                                           pMultiData->pOctetStrValue));

}

INT4
FsQoSPriorityMapIfIndexTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                             tRetVal * pMultiData)
{
    return (nmhTestv2FsQoSPriorityMapIfIndex (pu4Error,
                                              pMultiIndex->pIndex[0].
                                              u4_ULongValue,
                                              pMultiData->u4_ULongValue));

}

INT4
FsQoSPriorityMapVlanIdTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                            tRetVal * pMultiData)
{
    return (nmhTestv2FsQoSPriorityMapVlanId (pu4Error,
                                             pMultiIndex->pIndex[0].
                                             u4_ULongValue,
                                             pMultiData->u4_ULongValue));

}

INT4
FsQoSPriorityMapInPriorityTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                tRetVal * pMultiData)
{
    return (nmhTestv2FsQoSPriorityMapInPriority (pu4Error,
                                                 pMultiIndex->pIndex[0].
                                                 u4_ULongValue,
                                                 pMultiData->i4_SLongValue));
}
 
INT4 FsQoSPriorityMapInDEITest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhTestv2FsQoSPriorityMapInDEI(pu4Error,
        pMultiIndex->pIndex[0].u4_ULongValue,
        pMultiData->i4_SLongValue));
 
}
 
INT4 FsQoSPriorityMapRegenColorTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhTestv2FsQoSPriorityMapRegenColor(pu4Error,
        pMultiIndex->pIndex[0].u4_ULongValue,
        pMultiData->i4_SLongValue));

}

INT4
FsQoSPriorityMapInPriTypeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                               tRetVal * pMultiData)
{
    return (nmhTestv2FsQoSPriorityMapInPriType (pu4Error,
                                                pMultiIndex->pIndex[0].
                                                u4_ULongValue,
                                                pMultiData->i4_SLongValue));

}

INT4
FsQoSPriorityMapRegenPriorityTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    return (nmhTestv2FsQoSPriorityMapRegenPriority (pu4Error,
                                                    pMultiIndex->pIndex[0].
                                                    u4_ULongValue,
                                                    pMultiData->u4_ULongValue));

}

INT4
FsQoSPriorityMapRegenInnerPriorityTest (UINT4 *pu4Error,
                                        tSnmpIndex * pMultiIndex,
                                        tRetVal * pMultiData)
{
    return (nmhTestv2FsQoSPriorityMapRegenInnerPriority (pu4Error,
                                                         pMultiIndex->pIndex[0].
                                                         u4_ULongValue,
                                                         pMultiData->
                                                         u4_ULongValue));

}

INT4
FsQoSPriorityMapStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                            tRetVal * pMultiData)
{
    return (nmhTestv2FsQoSPriorityMapStatus (pu4Error,
                                             pMultiIndex->pIndex[0].
                                             u4_ULongValue,
                                             pMultiData->i4_SLongValue));

}

INT4
FsQoSPriorityMapTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                          tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsQoSPriorityMapTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFsQoSClassMapTable (tSnmpIndex * pFirstMultiIndex,
                                tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsQoSClassMapTable
            (&(pNextMultiIndex->pIndex[0].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsQoSClassMapTable
            (pFirstMultiIndex->pIndex[0].u4_ULongValue,
             &(pNextMultiIndex->pIndex[0].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
FsQoSClassMapNameGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsQoSClassMapTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsQoSClassMapName (pMultiIndex->pIndex[0].u4_ULongValue,
                                     pMultiData->pOctetStrValue));

}

INT4
FsQoSClassMapL2FilterIdGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsQoSClassMapTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsQoSClassMapL2FilterId (pMultiIndex->pIndex[0].u4_ULongValue,
                                           &(pMultiData->u4_ULongValue)));

}

INT4
FsQoSClassMapL3FilterIdGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsQoSClassMapTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsQoSClassMapL3FilterId (pMultiIndex->pIndex[0].u4_ULongValue,
                                           &(pMultiData->u4_ULongValue)));

}

INT4
FsQoSClassMapPriorityMapIdGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsQoSClassMapTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsQoSClassMapPriorityMapId
            (pMultiIndex->pIndex[0].u4_ULongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsQoSClassMapCLASSGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsQoSClassMapTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsQoSClassMapCLASS (pMultiIndex->pIndex[0].u4_ULongValue,
                                      &(pMultiData->u4_ULongValue)));

}

INT4
FsQoSClassMapClfrIdGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsQoSClassMapTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsQoSClassMapClfrId (pMultiIndex->pIndex[0].u4_ULongValue,
                                       &(pMultiData->u4_ULongValue)));

}

INT4
FsQoSClassMapPreColorGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsQoSClassMapTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsQoSClassMapPreColor (pMultiIndex->pIndex[0].u4_ULongValue,
                                         &(pMultiData->i4_SLongValue)));

}

INT4
FsQoSClassMapStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsQoSClassMapTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsQoSClassMapStatus (pMultiIndex->pIndex[0].u4_ULongValue,
                                       &(pMultiData->i4_SLongValue)));

}


INT4 FsQoSClassMapVlanMapIdGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsQoSClassMapTable(
        pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return(nmhGetFsQoSClassMapVlanMapId(
        pMultiIndex->pIndex[0].u4_ULongValue,
        &(pMultiData->u4_ULongValue)));

}

INT4
FsQoSClassMapNameSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsQoSClassMapName (pMultiIndex->pIndex[0].u4_ULongValue,
                                     pMultiData->pOctetStrValue));

}

INT4
FsQoSClassMapL2FilterIdSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsQoSClassMapL2FilterId (pMultiIndex->pIndex[0].u4_ULongValue,
                                           pMultiData->u4_ULongValue));

}

INT4
FsQoSClassMapL3FilterIdSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsQoSClassMapL3FilterId (pMultiIndex->pIndex[0].u4_ULongValue,
                                           pMultiData->u4_ULongValue));

}

INT4
FsQoSClassMapPriorityMapIdSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsQoSClassMapPriorityMapId
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->u4_ULongValue));

}

INT4
FsQoSClassMapCLASSSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsQoSClassMapCLASS (pMultiIndex->pIndex[0].u4_ULongValue,
                                      pMultiData->u4_ULongValue));

}

INT4
FsQoSClassMapClfrIdSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsQoSClassMapClfrId (pMultiIndex->pIndex[0].u4_ULongValue,
                                       pMultiData->u4_ULongValue));

}

INT4
FsQoSClassMapPreColorSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsQoSClassMapPreColor (pMultiIndex->pIndex[0].u4_ULongValue,
                                         pMultiData->i4_SLongValue));

}

INT4
FsQoSClassMapStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsQoSClassMapStatus (pMultiIndex->pIndex[0].u4_ULongValue,
                                       pMultiData->i4_SLongValue));

}

INT4 FsQoSClassMapVlanMapIdSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsQoSClassMapVlanMapId(
        pMultiIndex->pIndex[0].u4_ULongValue,
        pMultiData->u4_ULongValue));

}

INT4
FsQoSClassMapNameTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                       tRetVal * pMultiData)
{
    return (nmhTestv2FsQoSClassMapName (pu4Error,
                                        pMultiIndex->pIndex[0].u4_ULongValue,
                                        pMultiData->pOctetStrValue));

}

INT4
FsQoSClassMapL2FilterIdTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                             tRetVal * pMultiData)
{
    return (nmhTestv2FsQoSClassMapL2FilterId (pu4Error,
                                              pMultiIndex->pIndex[0].
                                              u4_ULongValue,
                                              pMultiData->u4_ULongValue));

}

INT4
FsQoSClassMapL3FilterIdTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                             tRetVal * pMultiData)
{
    return (nmhTestv2FsQoSClassMapL3FilterId (pu4Error,
                                              pMultiIndex->pIndex[0].
                                              u4_ULongValue,
                                              pMultiData->u4_ULongValue));

}

INT4
FsQoSClassMapPriorityMapIdTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                tRetVal * pMultiData)
{
    return (nmhTestv2FsQoSClassMapPriorityMapId (pu4Error,
                                                 pMultiIndex->pIndex[0].
                                                 u4_ULongValue,
                                                 pMultiData->u4_ULongValue));

}

INT4
FsQoSClassMapCLASSTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                        tRetVal * pMultiData)
{
    return (nmhTestv2FsQoSClassMapCLASS (pu4Error,
                                         pMultiIndex->pIndex[0].u4_ULongValue,
                                         pMultiData->u4_ULongValue));

}

INT4
FsQoSClassMapClfrIdTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                         tRetVal * pMultiData)
{
    return (nmhTestv2FsQoSClassMapClfrId (pu4Error,
                                          pMultiIndex->pIndex[0].u4_ULongValue,
                                          pMultiData->u4_ULongValue));

}

INT4
FsQoSClassMapPreColorTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                           tRetVal * pMultiData)
{
    return (nmhTestv2FsQoSClassMapPreColor (pu4Error,
                                            pMultiIndex->pIndex[0].
                                            u4_ULongValue,
                                            pMultiData->i4_SLongValue));

}

INT4
FsQoSClassMapStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                         tRetVal * pMultiData)
{
    return (nmhTestv2FsQoSClassMapStatus (pu4Error,
                                          pMultiIndex->pIndex[0].u4_ULongValue,
                                          pMultiData->i4_SLongValue));

}

INT4 FsQoSClassMapVlanMapIdTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhTestv2FsQoSClassMapVlanMapId(pu4Error,
        pMultiIndex->pIndex[0].u4_ULongValue,
        pMultiData->u4_ULongValue));

}


INT4
FsQoSClassMapTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                       tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsQoSClassMapTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFsQoSClassToPriorityTable (tSnmpIndex * pFirstMultiIndex,
                                       tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsQoSClassToPriorityTable
            (&(pNextMultiIndex->pIndex[0].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsQoSClassToPriorityTable
            (pFirstMultiIndex->pIndex[0].u4_ULongValue,
             &(pNextMultiIndex->pIndex[0].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
FsQoSClassToPriorityRegenPriGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsQoSClassToPriorityTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsQoSClassToPriorityRegenPri
            (pMultiIndex->pIndex[0].u4_ULongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsQoSClassToPriorityGroupNameGet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsQoSClassToPriorityTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsQoSClassToPriorityGroupName
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->pOctetStrValue));

}

INT4
FsQoSClassToPriorityStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsQoSClassToPriorityTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsQoSClassToPriorityStatus
            (pMultiIndex->pIndex[0].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsQoSClassToPriorityRegenPriSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsQoSClassToPriorityRegenPri
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->u4_ULongValue));

}

INT4
FsQoSClassToPriorityGroupNameSet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    return (nmhSetFsQoSClassToPriorityGroupName
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->pOctetStrValue));

}

INT4
FsQoSClassToPriorityStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsQoSClassToPriorityStatus
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->i4_SLongValue));

}

INT4
FsQoSClassToPriorityRegenPriTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    return (nmhTestv2FsQoSClassToPriorityRegenPri (pu4Error,
                                                   pMultiIndex->pIndex[0].
                                                   u4_ULongValue,
                                                   pMultiData->u4_ULongValue));

}

INT4
FsQoSClassToPriorityGroupNameTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    return (nmhTestv2FsQoSClassToPriorityGroupName (pu4Error,
                                                    pMultiIndex->pIndex[0].
                                                    u4_ULongValue,
                                                    pMultiData->
                                                    pOctetStrValue));

}

INT4
FsQoSClassToPriorityStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                tRetVal * pMultiData)
{
    return (nmhTestv2FsQoSClassToPriorityStatus (pu4Error,
                                                 pMultiIndex->pIndex[0].
                                                 u4_ULongValue,
                                                 pMultiData->i4_SLongValue));

}

INT4
FsQoSClassToPriorityTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                              tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsQoSClassToPriorityTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFsQoSMeterTable (tSnmpIndex * pFirstMultiIndex,
                             tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsQoSMeterTable
            (&(pNextMultiIndex->pIndex[0].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsQoSMeterTable
            (pFirstMultiIndex->pIndex[0].u4_ULongValue,
             &(pNextMultiIndex->pIndex[0].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
FsQoSMeterNameGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsQoSMeterTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsQoSMeterName (pMultiIndex->pIndex[0].u4_ULongValue,
                                  pMultiData->pOctetStrValue));

}

INT4
FsQoSMeterTypeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsQoSMeterTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsQoSMeterType (pMultiIndex->pIndex[0].u4_ULongValue,
                                  &(pMultiData->i4_SLongValue)));

}

INT4
FsQoSMeterIntervalGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsQoSMeterTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsQoSMeterInterval (pMultiIndex->pIndex[0].u4_ULongValue,
                                      &(pMultiData->u4_ULongValue)));

}

INT4
FsQoSMeterColorModeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsQoSMeterTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsQoSMeterColorMode (pMultiIndex->pIndex[0].u4_ULongValue,
                                       &(pMultiData->i4_SLongValue)));

}

INT4
FsQoSMeterCIRGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsQoSMeterTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsQoSMeterCIR (pMultiIndex->pIndex[0].u4_ULongValue,
                                 &(pMultiData->u4_ULongValue)));

}

INT4
FsQoSMeterCBSGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsQoSMeterTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsQoSMeterCBS (pMultiIndex->pIndex[0].u4_ULongValue,
                                 &(pMultiData->u4_ULongValue)));

}

INT4
FsQoSMeterEIRGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsQoSMeterTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsQoSMeterEIR (pMultiIndex->pIndex[0].u4_ULongValue,
                                 &(pMultiData->u4_ULongValue)));

}

INT4
FsQoSMeterEBSGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsQoSMeterTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsQoSMeterEBS (pMultiIndex->pIndex[0].u4_ULongValue,
                                 &(pMultiData->u4_ULongValue)));

}

INT4
FsQoSMeterNextGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsQoSMeterTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsQoSMeterNext (pMultiIndex->pIndex[0].u4_ULongValue,
                                  &(pMultiData->u4_ULongValue)));

}

INT4
FsQoSMeterStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsQoSMeterTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsQoSMeterStatus (pMultiIndex->pIndex[0].u4_ULongValue,
                                    &(pMultiData->i4_SLongValue)));

}

INT4
FsQoSMeterNameSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsQoSMeterName (pMultiIndex->pIndex[0].u4_ULongValue,
                                  pMultiData->pOctetStrValue));

}

INT4
FsQoSMeterTypeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsQoSMeterType (pMultiIndex->pIndex[0].u4_ULongValue,
                                  pMultiData->i4_SLongValue));

}

INT4
FsQoSMeterIntervalSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsQoSMeterInterval (pMultiIndex->pIndex[0].u4_ULongValue,
                                      pMultiData->u4_ULongValue));

}

INT4
FsQoSMeterColorModeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsQoSMeterColorMode (pMultiIndex->pIndex[0].u4_ULongValue,
                                       pMultiData->i4_SLongValue));

}

INT4
FsQoSMeterCIRSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsQoSMeterCIR (pMultiIndex->pIndex[0].u4_ULongValue,
                                 pMultiData->u4_ULongValue));

}

INT4
FsQoSMeterCBSSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsQoSMeterCBS (pMultiIndex->pIndex[0].u4_ULongValue,
                                 pMultiData->u4_ULongValue));

}

INT4
FsQoSMeterEIRSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsQoSMeterEIR (pMultiIndex->pIndex[0].u4_ULongValue,
                                 pMultiData->u4_ULongValue));

}

INT4
FsQoSMeterEBSSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsQoSMeterEBS (pMultiIndex->pIndex[0].u4_ULongValue,
                                 pMultiData->u4_ULongValue));

}

INT4
FsQoSMeterNextSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsQoSMeterNext (pMultiIndex->pIndex[0].u4_ULongValue,
                                  pMultiData->u4_ULongValue));

}

INT4
FsQoSMeterStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsQoSMeterStatus (pMultiIndex->pIndex[0].u4_ULongValue,
                                    pMultiData->i4_SLongValue));

}

INT4
FsQoSMeterNameTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                    tRetVal * pMultiData)
{
    return (nmhTestv2FsQoSMeterName (pu4Error,
                                     pMultiIndex->pIndex[0].u4_ULongValue,
                                     pMultiData->pOctetStrValue));

}

INT4
FsQoSMeterTypeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                    tRetVal * pMultiData)
{
    return (nmhTestv2FsQoSMeterType (pu4Error,
                                     pMultiIndex->pIndex[0].u4_ULongValue,
                                     pMultiData->i4_SLongValue));

}

INT4
FsQoSMeterIntervalTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                        tRetVal * pMultiData)
{
    return (nmhTestv2FsQoSMeterInterval (pu4Error,
                                         pMultiIndex->pIndex[0].u4_ULongValue,
                                         pMultiData->u4_ULongValue));

}

INT4
FsQoSMeterColorModeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                         tRetVal * pMultiData)
{
    return (nmhTestv2FsQoSMeterColorMode (pu4Error,
                                          pMultiIndex->pIndex[0].u4_ULongValue,
                                          pMultiData->i4_SLongValue));

}

INT4
FsQoSMeterCIRTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                   tRetVal * pMultiData)
{
    return (nmhTestv2FsQoSMeterCIR (pu4Error,
                                    pMultiIndex->pIndex[0].u4_ULongValue,
                                    pMultiData->u4_ULongValue));

}

INT4
FsQoSMeterCBSTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                   tRetVal * pMultiData)
{
    return (nmhTestv2FsQoSMeterCBS (pu4Error,
                                    pMultiIndex->pIndex[0].u4_ULongValue,
                                    pMultiData->u4_ULongValue));

}

INT4
FsQoSMeterEIRTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                   tRetVal * pMultiData)
{
    return (nmhTestv2FsQoSMeterEIR (pu4Error,
                                    pMultiIndex->pIndex[0].u4_ULongValue,
                                    pMultiData->u4_ULongValue));

}

INT4
FsQoSMeterEBSTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                   tRetVal * pMultiData)
{
    return (nmhTestv2FsQoSMeterEBS (pu4Error,
                                    pMultiIndex->pIndex[0].u4_ULongValue,
                                    pMultiData->u4_ULongValue));

}

INT4
FsQoSMeterNextTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                    tRetVal * pMultiData)
{
    return (nmhTestv2FsQoSMeterNext (pu4Error,
                                     pMultiIndex->pIndex[0].u4_ULongValue,
                                     pMultiData->u4_ULongValue));

}

INT4
FsQoSMeterStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                      tRetVal * pMultiData)
{
    return (nmhTestv2FsQoSMeterStatus (pu4Error,
                                       pMultiIndex->pIndex[0].u4_ULongValue,
                                       pMultiData->i4_SLongValue));

}

INT4
FsQoSMeterTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                    tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsQoSMeterTable (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFsQoSPolicyMapTable (tSnmpIndex * pFirstMultiIndex,
                                 tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsQoSPolicyMapTable
            (&(pNextMultiIndex->pIndex[0].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsQoSPolicyMapTable
            (pFirstMultiIndex->pIndex[0].u4_ULongValue,
             &(pNextMultiIndex->pIndex[0].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
FsQoSPolicyMapNameGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsQoSPolicyMapTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsQoSPolicyMapName (pMultiIndex->pIndex[0].u4_ULongValue,
                                      pMultiData->pOctetStrValue));

}

INT4
FsQoSPolicyMapIfIndexGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsQoSPolicyMapTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsQoSPolicyMapIfIndex (pMultiIndex->pIndex[0].u4_ULongValue,
                                         &(pMultiData->u4_ULongValue)));

}

INT4
FsQoSPolicyMapCLASSGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsQoSPolicyMapTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsQoSPolicyMapCLASS (pMultiIndex->pIndex[0].u4_ULongValue,
                                       &(pMultiData->u4_ULongValue)));

}

INT4
FsQoSPolicyMapPHBTypeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsQoSPolicyMapTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsQoSPolicyMapPHBType (pMultiIndex->pIndex[0].u4_ULongValue,
                                         &(pMultiData->i4_SLongValue)));

}

INT4
FsQoSPolicyMapDefaultPHBGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsQoSPolicyMapTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsQoSPolicyMapDefaultPHB
            (pMultiIndex->pIndex[0].u4_ULongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsQoSPolicyMapMeterTableIdGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsQoSPolicyMapTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsQoSPolicyMapMeterTableId
            (pMultiIndex->pIndex[0].u4_ULongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsQoSPolicyMapFailMeterTableIdGet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsQoSPolicyMapTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsQoSPolicyMapFailMeterTableId
            (pMultiIndex->pIndex[0].u4_ULongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsQoSPolicyMapInProfileConformActionFlagGet (tSnmpIndex * pMultiIndex,
                                             tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsQoSPolicyMapTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsQoSPolicyMapInProfileConformActionFlag
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->pOctetStrValue));

}

INT4
FsQoSPolicyMapInProfileConformActionIdGet (tSnmpIndex * pMultiIndex,
                                           tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsQoSPolicyMapTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsQoSPolicyMapInProfileConformActionId
            (pMultiIndex->pIndex[0].u4_ULongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsQoSPolicyMapInProfileActionSetPortGet (tSnmpIndex * pMultiIndex,
                                         tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsQoSPolicyMapTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsQoSPolicyMapInProfileActionSetPort
            (pMultiIndex->pIndex[0].u4_ULongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsQoSPolicyMapConformActionSetIpTOSGet (tSnmpIndex * pMultiIndex,
                                        tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsQoSPolicyMapTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsQoSPolicyMapConformActionSetIpTOS
            (pMultiIndex->pIndex[0].u4_ULongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsQoSPolicyMapConformActionSetDscpGet (tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsQoSPolicyMapTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsQoSPolicyMapConformActionSetDscp
            (pMultiIndex->pIndex[0].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsQoSPolicyMapConformActionSetVlanPrioGet (tSnmpIndex * pMultiIndex,
                                           tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsQoSPolicyMapTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsQoSPolicyMapConformActionSetVlanPrio
            (pMultiIndex->pIndex[0].u4_ULongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsQoSPolicyMapConformActionSetVlanDEGet (tSnmpIndex * pMultiIndex,
                                         tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsQoSPolicyMapTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsQoSPolicyMapConformActionSetVlanDE
            (pMultiIndex->pIndex[0].u4_ULongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsQoSPolicyMapConformActionSetInnerVlanPrioGet (tSnmpIndex * pMultiIndex,
                                                tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsQoSPolicyMapTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsQoSPolicyMapConformActionSetInnerVlanPrio
            (pMultiIndex->pIndex[0].u4_ULongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsQoSPolicyMapConformActionSetMplsExpGet (tSnmpIndex * pMultiIndex,
                                          tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsQoSPolicyMapTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsQoSPolicyMapConformActionSetMplsExp
            (pMultiIndex->pIndex[0].u4_ULongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsQoSPolicyMapConformActionSetNewCLASSGet (tSnmpIndex * pMultiIndex,
                                           tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsQoSPolicyMapTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsQoSPolicyMapConformActionSetNewCLASS
            (pMultiIndex->pIndex[0].u4_ULongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsQoSPolicyMapInProfileExceedActionFlagGet (tSnmpIndex * pMultiIndex,
                                            tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsQoSPolicyMapTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsQoSPolicyMapInProfileExceedActionFlag
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->pOctetStrValue));

}

INT4
FsQoSPolicyMapInProfileExceedActionIdGet (tSnmpIndex * pMultiIndex,
                                          tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsQoSPolicyMapTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsQoSPolicyMapInProfileExceedActionId
            (pMultiIndex->pIndex[0].u4_ULongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsQoSPolicyMapExceedActionSetIpTOSGet (tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsQoSPolicyMapTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsQoSPolicyMapExceedActionSetIpTOS
            (pMultiIndex->pIndex[0].u4_ULongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsQoSPolicyMapExceedActionSetDscpGet (tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsQoSPolicyMapTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsQoSPolicyMapExceedActionSetDscp
            (pMultiIndex->pIndex[0].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsQoSPolicyMapExceedActionSetInnerVlanPrioGet (tSnmpIndex * pMultiIndex,
                                               tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsQoSPolicyMapTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsQoSPolicyMapExceedActionSetInnerVlanPrio
            (pMultiIndex->pIndex[0].u4_ULongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsQoSPolicyMapExceedActionSetVlanPrioGet (tSnmpIndex * pMultiIndex,
                                          tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsQoSPolicyMapTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsQoSPolicyMapExceedActionSetVlanPrio
            (pMultiIndex->pIndex[0].u4_ULongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsQoSPolicyMapExceedActionSetVlanDEGet (tSnmpIndex * pMultiIndex,
                                        tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsQoSPolicyMapTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsQoSPolicyMapExceedActionSetVlanDE
            (pMultiIndex->pIndex[0].u4_ULongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsQoSPolicyMapExceedActionSetMplsExpGet (tSnmpIndex * pMultiIndex,
                                         tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsQoSPolicyMapTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsQoSPolicyMapExceedActionSetMplsExp
            (pMultiIndex->pIndex[0].u4_ULongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsQoSPolicyMapExceedActionSetNewCLASSGet (tSnmpIndex * pMultiIndex,
                                          tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsQoSPolicyMapTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsQoSPolicyMapExceedActionSetNewCLASS
            (pMultiIndex->pIndex[0].u4_ULongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsQoSPolicyMapOutProfileActionFlagGet (tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsQoSPolicyMapTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsQoSPolicyMapOutProfileActionFlag
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->pOctetStrValue));

}

INT4
FsQoSPolicyMapOutProfileActionIdGet (tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsQoSPolicyMapTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsQoSPolicyMapOutProfileActionId
            (pMultiIndex->pIndex[0].u4_ULongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsQoSPolicyMapOutProfileActionSetIPTOSGet (tSnmpIndex * pMultiIndex,
                                           tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsQoSPolicyMapTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsQoSPolicyMapOutProfileActionSetIPTOS
            (pMultiIndex->pIndex[0].u4_ULongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsQoSPolicyMapOutProfileActionSetDscpGet (tSnmpIndex * pMultiIndex,
                                          tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsQoSPolicyMapTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsQoSPolicyMapOutProfileActionSetDscp
            (pMultiIndex->pIndex[0].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsQoSPolicyMapOutProfileActionSetInnerVlanPrioGet (tSnmpIndex * pMultiIndex,
                                                   tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsQoSPolicyMapTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsQoSPolicyMapOutProfileActionSetInnerVlanPrio
            (pMultiIndex->pIndex[0].u4_ULongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsQoSPolicyMapOutProfileActionSetVlanPrioGet (tSnmpIndex * pMultiIndex,
                                              tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsQoSPolicyMapTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsQoSPolicyMapOutProfileActionSetVlanPrio
            (pMultiIndex->pIndex[0].u4_ULongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsQoSPolicyMapOutProfileActionSetVlanDEGet (tSnmpIndex * pMultiIndex,
                                            tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsQoSPolicyMapTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsQoSPolicyMapOutProfileActionSetVlanDE
            (pMultiIndex->pIndex[0].u4_ULongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsQoSPolicyMapOutProfileActionSetMplsExpGet (tSnmpIndex * pMultiIndex,
                                             tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsQoSPolicyMapTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsQoSPolicyMapOutProfileActionSetMplsExp
            (pMultiIndex->pIndex[0].u4_ULongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsQoSPolicyMapOutProfileActionSetNewCLASSGet (tSnmpIndex * pMultiIndex,
                                              tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsQoSPolicyMapTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsQoSPolicyMapOutProfileActionSetNewCLASS
            (pMultiIndex->pIndex[0].u4_ULongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsQoSPolicyMapStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsQoSPolicyMapTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsQoSPolicyMapStatus (pMultiIndex->pIndex[0].u4_ULongValue,
                                        &(pMultiData->i4_SLongValue)));

}

INT4
FsQoSPolicyMapConformActionSetInnerVlanDEGet (tSnmpIndex * pMultiIndex,
                                              tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsQoSPolicyMapTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsQoSPolicyMapConformActionSetInnerVlanDE
            (pMultiIndex->pIndex[0].u4_ULongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsQoSPolicyMapExceedActionSetInnerVlanDEGet (tSnmpIndex * pMultiIndex,
                                             tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsQoSPolicyMapTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsQoSPolicyMapExceedActionSetInnerVlanDE
            (pMultiIndex->pIndex[0].u4_ULongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsQoSPolicyMapOutProfileActionSetInnerVlanDEGet (tSnmpIndex * pMultiIndex,
                                                 tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsQoSPolicyMapTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsQoSPolicyMapOutProfileActionSetInnerVlanDE
            (pMultiIndex->pIndex[0].u4_ULongValue,
             &(pMultiData->u4_ULongValue)));

}
	INT4 FsQoSPolicyDefaultVlanDEGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsQoSPolicyMapTable(
        pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return(nmhGetFsQoSPolicyDefaultVlanDE(
        pMultiIndex->pIndex[0].u4_ULongValue,
        &(pMultiData->i4_SLongValue)));

}
INT4
FsQoSPolicyMapNameSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsQoSPolicyMapName (pMultiIndex->pIndex[0].u4_ULongValue,
                                      pMultiData->pOctetStrValue));

}

INT4
FsQoSPolicyMapIfIndexSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsQoSPolicyMapIfIndex (pMultiIndex->pIndex[0].u4_ULongValue,
                                         pMultiData->u4_ULongValue));

}

INT4
FsQoSPolicyMapCLASSSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsQoSPolicyMapCLASS (pMultiIndex->pIndex[0].u4_ULongValue,
                                       pMultiData->u4_ULongValue));

}

INT4
FsQoSPolicyMapPHBTypeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsQoSPolicyMapPHBType (pMultiIndex->pIndex[0].u4_ULongValue,
                                         pMultiData->i4_SLongValue));

}

INT4
FsQoSPolicyMapDefaultPHBSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsQoSPolicyMapDefaultPHB
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->u4_ULongValue));

}

INT4
FsQoSPolicyMapMeterTableIdSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsQoSPolicyMapMeterTableId
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->u4_ULongValue));

}

INT4
FsQoSPolicyMapFailMeterTableIdSet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    return (nmhSetFsQoSPolicyMapFailMeterTableId
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->u4_ULongValue));

}

INT4
FsQoSPolicyMapInProfileConformActionFlagSet (tSnmpIndex * pMultiIndex,
                                             tRetVal * pMultiData)
{
    return (nmhSetFsQoSPolicyMapInProfileConformActionFlag
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->pOctetStrValue));

}

INT4
FsQoSPolicyMapInProfileConformActionIdSet (tSnmpIndex * pMultiIndex,
                                           tRetVal * pMultiData)
{
    return (nmhSetFsQoSPolicyMapInProfileConformActionId
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->u4_ULongValue));

}

INT4
FsQoSPolicyMapInProfileActionSetPortSet (tSnmpIndex * pMultiIndex,
                                         tRetVal * pMultiData)
{
    return (nmhSetFsQoSPolicyMapInProfileActionSetPort
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->u4_ULongValue));

}

INT4
FsQoSPolicyMapConformActionSetIpTOSSet (tSnmpIndex * pMultiIndex,
                                        tRetVal * pMultiData)
{
    return (nmhSetFsQoSPolicyMapConformActionSetIpTOS
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->u4_ULongValue));

}

INT4
FsQoSPolicyMapConformActionSetDscpSet (tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    return (nmhSetFsQoSPolicyMapConformActionSetDscp
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->i4_SLongValue));

}

INT4
FsQoSPolicyMapConformActionSetVlanPrioSet (tSnmpIndex * pMultiIndex,
                                           tRetVal * pMultiData)
{
    return (nmhSetFsQoSPolicyMapConformActionSetVlanPrio
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->u4_ULongValue));

}

INT4
FsQoSPolicyMapConformActionSetVlanDESet (tSnmpIndex * pMultiIndex,
                                         tRetVal * pMultiData)
{
    return (nmhSetFsQoSPolicyMapConformActionSetVlanDE
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->u4_ULongValue));

}

INT4
FsQoSPolicyMapConformActionSetInnerVlanPrioSet (tSnmpIndex * pMultiIndex,
                                                tRetVal * pMultiData)
{
    return (nmhSetFsQoSPolicyMapConformActionSetInnerVlanPrio
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->u4_ULongValue));

}

INT4
FsQoSPolicyMapConformActionSetMplsExpSet (tSnmpIndex * pMultiIndex,
                                          tRetVal * pMultiData)
{
    return (nmhSetFsQoSPolicyMapConformActionSetMplsExp
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->u4_ULongValue));

}

INT4
FsQoSPolicyMapConformActionSetNewCLASSSet (tSnmpIndex * pMultiIndex,
                                           tRetVal * pMultiData)
{
    return (nmhSetFsQoSPolicyMapConformActionSetNewCLASS
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->u4_ULongValue));

}

INT4
FsQoSPolicyMapInProfileExceedActionFlagSet (tSnmpIndex * pMultiIndex,
                                            tRetVal * pMultiData)
{
    return (nmhSetFsQoSPolicyMapInProfileExceedActionFlag
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->pOctetStrValue));

}

INT4
FsQoSPolicyMapInProfileExceedActionIdSet (tSnmpIndex * pMultiIndex,
                                          tRetVal * pMultiData)
{
    return (nmhSetFsQoSPolicyMapInProfileExceedActionId
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->u4_ULongValue));

}

INT4
FsQoSPolicyMapExceedActionSetIpTOSSet (tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    return (nmhSetFsQoSPolicyMapExceedActionSetIpTOS
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->u4_ULongValue));

}

INT4
FsQoSPolicyMapExceedActionSetDscpSet (tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    return (nmhSetFsQoSPolicyMapExceedActionSetDscp
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->i4_SLongValue));

}

INT4
FsQoSPolicyMapExceedActionSetInnerVlanPrioSet (tSnmpIndex * pMultiIndex,
                                               tRetVal * pMultiData)
{
    return (nmhSetFsQoSPolicyMapExceedActionSetInnerVlanPrio
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->u4_ULongValue));

}

INT4
FsQoSPolicyMapExceedActionSetVlanPrioSet (tSnmpIndex * pMultiIndex,
                                          tRetVal * pMultiData)
{
    return (nmhSetFsQoSPolicyMapExceedActionSetVlanPrio
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->u4_ULongValue));

}

INT4
FsQoSPolicyMapExceedActionSetVlanDESet (tSnmpIndex * pMultiIndex,
                                        tRetVal * pMultiData)
{
    return (nmhSetFsQoSPolicyMapExceedActionSetVlanDE
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->u4_ULongValue));

}

INT4
FsQoSPolicyMapExceedActionSetMplsExpSet (tSnmpIndex * pMultiIndex,
                                         tRetVal * pMultiData)
{
    return (nmhSetFsQoSPolicyMapExceedActionSetMplsExp
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->u4_ULongValue));

}

INT4
FsQoSPolicyMapExceedActionSetNewCLASSSet (tSnmpIndex * pMultiIndex,
                                          tRetVal * pMultiData)
{
    return (nmhSetFsQoSPolicyMapExceedActionSetNewCLASS
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->u4_ULongValue));

}

INT4
FsQoSPolicyMapOutProfileActionFlagSet (tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    return (nmhSetFsQoSPolicyMapOutProfileActionFlag
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->pOctetStrValue));

}

INT4
FsQoSPolicyMapOutProfileActionIdSet (tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    return (nmhSetFsQoSPolicyMapOutProfileActionId
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->u4_ULongValue));

}

INT4
FsQoSPolicyMapOutProfileActionSetIPTOSSet (tSnmpIndex * pMultiIndex,
                                           tRetVal * pMultiData)
{
    return (nmhSetFsQoSPolicyMapOutProfileActionSetIPTOS
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->u4_ULongValue));

}

INT4
FsQoSPolicyMapOutProfileActionSetDscpSet (tSnmpIndex * pMultiIndex,
                                          tRetVal * pMultiData)
{
    return (nmhSetFsQoSPolicyMapOutProfileActionSetDscp
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->i4_SLongValue));

}

INT4
FsQoSPolicyMapOutProfileActionSetInnerVlanPrioSet (tSnmpIndex * pMultiIndex,
                                                   tRetVal * pMultiData)
{
    return (nmhSetFsQoSPolicyMapOutProfileActionSetInnerVlanPrio
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->u4_ULongValue));

}

INT4
FsQoSPolicyMapOutProfileActionSetVlanPrioSet (tSnmpIndex * pMultiIndex,
                                              tRetVal * pMultiData)
{
    return (nmhSetFsQoSPolicyMapOutProfileActionSetVlanPrio
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->u4_ULongValue));

}

INT4
FsQoSPolicyMapOutProfileActionSetVlanDESet (tSnmpIndex * pMultiIndex,
                                            tRetVal * pMultiData)
{
    return (nmhSetFsQoSPolicyMapOutProfileActionSetVlanDE
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->u4_ULongValue));

}

INT4
FsQoSPolicyMapOutProfileActionSetMplsExpSet (tSnmpIndex * pMultiIndex,
                                             tRetVal * pMultiData)
{
    return (nmhSetFsQoSPolicyMapOutProfileActionSetMplsExp
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->u4_ULongValue));

}

INT4
FsQoSPolicyMapOutProfileActionSetNewCLASSSet (tSnmpIndex * pMultiIndex,
                                              tRetVal * pMultiData)
{
    return (nmhSetFsQoSPolicyMapOutProfileActionSetNewCLASS
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->u4_ULongValue));

}

INT4
FsQoSPolicyMapStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsQoSPolicyMapStatus (pMultiIndex->pIndex[0].u4_ULongValue,
                                        pMultiData->i4_SLongValue));

}

INT4
FsQoSPolicyMapConformActionSetInnerVlanDESet (tSnmpIndex * pMultiIndex,
                                              tRetVal * pMultiData)
{
    return (nmhSetFsQoSPolicyMapConformActionSetInnerVlanDE
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->u4_ULongValue));

}

INT4
FsQoSPolicyMapExceedActionSetInnerVlanDESet (tSnmpIndex * pMultiIndex,
                                             tRetVal * pMultiData)
{
    return (nmhSetFsQoSPolicyMapExceedActionSetInnerVlanDE
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->u4_ULongValue));

}

INT4
FsQoSPolicyMapOutProfileActionSetInnerVlanDESet (tSnmpIndex * pMultiIndex,
                                                 tRetVal * pMultiData)
{
    return (nmhSetFsQoSPolicyMapOutProfileActionSetInnerVlanDE
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->u4_ULongValue));
}
 
INT4 FsQoSPolicyDefaultVlanDESet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsQoSPolicyDefaultVlanDE(
        pMultiIndex->pIndex[0].u4_ULongValue,
        pMultiData->i4_SLongValue));

}

INT4
FsQoSPolicyMapNameTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                        tRetVal * pMultiData)
{
    return (nmhTestv2FsQoSPolicyMapName (pu4Error,
                                         pMultiIndex->pIndex[0].u4_ULongValue,
                                         pMultiData->pOctetStrValue));

}

INT4
FsQoSPolicyMapIfIndexTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                           tRetVal * pMultiData)
{
    return (nmhTestv2FsQoSPolicyMapIfIndex (pu4Error,
                                            pMultiIndex->pIndex[0].
                                            u4_ULongValue,
                                            pMultiData->u4_ULongValue));

}

INT4
FsQoSPolicyMapCLASSTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                         tRetVal * pMultiData)
{
    return (nmhTestv2FsQoSPolicyMapCLASS (pu4Error,
                                          pMultiIndex->pIndex[0].u4_ULongValue,
                                          pMultiData->u4_ULongValue));

}

INT4
FsQoSPolicyMapPHBTypeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                           tRetVal * pMultiData)
{
    return (nmhTestv2FsQoSPolicyMapPHBType (pu4Error,
                                            pMultiIndex->pIndex[0].
                                            u4_ULongValue,
                                            pMultiData->i4_SLongValue));

}

INT4
FsQoSPolicyMapDefaultPHBTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                              tRetVal * pMultiData)
{
    return (nmhTestv2FsQoSPolicyMapDefaultPHB (pu4Error,
                                               pMultiIndex->pIndex[0].
                                               u4_ULongValue,
                                               pMultiData->u4_ULongValue));

}

INT4
FsQoSPolicyMapMeterTableIdTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                tRetVal * pMultiData)
{
    return (nmhTestv2FsQoSPolicyMapMeterTableId (pu4Error,
                                                 pMultiIndex->pIndex[0].
                                                 u4_ULongValue,
                                                 pMultiData->u4_ULongValue));

}

INT4
FsQoSPolicyMapFailMeterTableIdTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    return (nmhTestv2FsQoSPolicyMapFailMeterTableId (pu4Error,
                                                     pMultiIndex->pIndex[0].
                                                     u4_ULongValue,
                                                     pMultiData->
                                                     u4_ULongValue));

}

INT4
FsQoSPolicyMapInProfileConformActionFlagTest (UINT4 *pu4Error,
                                              tSnmpIndex * pMultiIndex,
                                              tRetVal * pMultiData)
{
    return (nmhTestv2FsQoSPolicyMapInProfileConformActionFlag (pu4Error,
                                                               pMultiIndex->
                                                               pIndex[0].
                                                               u4_ULongValue,
                                                               pMultiData->
                                                               pOctetStrValue));

}

INT4
FsQoSPolicyMapInProfileConformActionIdTest (UINT4 *pu4Error,
                                            tSnmpIndex * pMultiIndex,
                                            tRetVal * pMultiData)
{
    return (nmhTestv2FsQoSPolicyMapInProfileConformActionId (pu4Error,
                                                             pMultiIndex->
                                                             pIndex[0].
                                                             u4_ULongValue,
                                                             pMultiData->
                                                             u4_ULongValue));

}

INT4
FsQoSPolicyMapInProfileActionSetPortTest (UINT4 *pu4Error,
                                          tSnmpIndex * pMultiIndex,
                                          tRetVal * pMultiData)
{
    return (nmhTestv2FsQoSPolicyMapInProfileActionSetPort (pu4Error,
                                                           pMultiIndex->
                                                           pIndex[0].
                                                           u4_ULongValue,
                                                           pMultiData->
                                                           u4_ULongValue));

}

INT4
FsQoSPolicyMapConformActionSetIpTOSTest (UINT4 *pu4Error,
                                         tSnmpIndex * pMultiIndex,
                                         tRetVal * pMultiData)
{
    return (nmhTestv2FsQoSPolicyMapConformActionSetIpTOS (pu4Error,
                                                          pMultiIndex->
                                                          pIndex[0].
                                                          u4_ULongValue,
                                                          pMultiData->
                                                          u4_ULongValue));

}

INT4
FsQoSPolicyMapConformActionSetDscpTest (UINT4 *pu4Error,
                                        tSnmpIndex * pMultiIndex,
                                        tRetVal * pMultiData)
{
    return (nmhTestv2FsQoSPolicyMapConformActionSetDscp (pu4Error,
                                                         pMultiIndex->pIndex[0].
                                                         u4_ULongValue,
                                                         pMultiData->
                                                         i4_SLongValue));

}

INT4
FsQoSPolicyMapConformActionSetVlanPrioTest (UINT4 *pu4Error,
                                            tSnmpIndex * pMultiIndex,
                                            tRetVal * pMultiData)
{
    return (nmhTestv2FsQoSPolicyMapConformActionSetVlanPrio (pu4Error,
                                                             pMultiIndex->
                                                             pIndex[0].
                                                             u4_ULongValue,
                                                             pMultiData->
                                                             u4_ULongValue));

}

INT4
FsQoSPolicyMapConformActionSetVlanDETest (UINT4 *pu4Error,
                                          tSnmpIndex * pMultiIndex,
                                          tRetVal * pMultiData)
{
    return (nmhTestv2FsQoSPolicyMapConformActionSetVlanDE (pu4Error,
                                                           pMultiIndex->
                                                           pIndex[0].
                                                           u4_ULongValue,
                                                           pMultiData->
                                                           u4_ULongValue));

}

INT4
FsQoSPolicyMapConformActionSetInnerVlanPrioTest (UINT4 *pu4Error,
                                                 tSnmpIndex * pMultiIndex,
                                                 tRetVal * pMultiData)
{
    return (nmhTestv2FsQoSPolicyMapConformActionSetInnerVlanPrio (pu4Error,
                                                                  pMultiIndex->
                                                                  pIndex[0].
                                                                  u4_ULongValue,
                                                                  pMultiData->
                                                                  u4_ULongValue));

}

INT4
FsQoSPolicyMapConformActionSetMplsExpTest (UINT4 *pu4Error,
                                           tSnmpIndex * pMultiIndex,
                                           tRetVal * pMultiData)
{
    return (nmhTestv2FsQoSPolicyMapConformActionSetMplsExp (pu4Error,
                                                            pMultiIndex->
                                                            pIndex[0].
                                                            u4_ULongValue,
                                                            pMultiData->
                                                            u4_ULongValue));

}

INT4
FsQoSPolicyMapConformActionSetNewCLASSTest (UINT4 *pu4Error,
                                            tSnmpIndex * pMultiIndex,
                                            tRetVal * pMultiData)
{
    return (nmhTestv2FsQoSPolicyMapConformActionSetNewCLASS (pu4Error,
                                                             pMultiIndex->
                                                             pIndex[0].
                                                             u4_ULongValue,
                                                             pMultiData->
                                                             u4_ULongValue));

}

INT4
FsQoSPolicyMapInProfileExceedActionFlagTest (UINT4 *pu4Error,
                                             tSnmpIndex * pMultiIndex,
                                             tRetVal * pMultiData)
{
    return (nmhTestv2FsQoSPolicyMapInProfileExceedActionFlag (pu4Error,
                                                              pMultiIndex->
                                                              pIndex[0].
                                                              u4_ULongValue,
                                                              pMultiData->
                                                              pOctetStrValue));

}

INT4
FsQoSPolicyMapInProfileExceedActionIdTest (UINT4 *pu4Error,
                                           tSnmpIndex * pMultiIndex,
                                           tRetVal * pMultiData)
{
    return (nmhTestv2FsQoSPolicyMapInProfileExceedActionId (pu4Error,
                                                            pMultiIndex->
                                                            pIndex[0].
                                                            u4_ULongValue,
                                                            pMultiData->
                                                            u4_ULongValue));

}

INT4
FsQoSPolicyMapExceedActionSetIpTOSTest (UINT4 *pu4Error,
                                        tSnmpIndex * pMultiIndex,
                                        tRetVal * pMultiData)
{
    return (nmhTestv2FsQoSPolicyMapExceedActionSetIpTOS (pu4Error,
                                                         pMultiIndex->pIndex[0].
                                                         u4_ULongValue,
                                                         pMultiData->
                                                         u4_ULongValue));

}

INT4
FsQoSPolicyMapExceedActionSetDscpTest (UINT4 *pu4Error,
                                       tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    return (nmhTestv2FsQoSPolicyMapExceedActionSetDscp (pu4Error,
                                                        pMultiIndex->pIndex[0].
                                                        u4_ULongValue,
                                                        pMultiData->
                                                        i4_SLongValue));

}

INT4
FsQoSPolicyMapExceedActionSetInnerVlanPrioTest (UINT4 *pu4Error,
                                                tSnmpIndex * pMultiIndex,
                                                tRetVal * pMultiData)
{
    return (nmhTestv2FsQoSPolicyMapExceedActionSetInnerVlanPrio (pu4Error,
                                                                 pMultiIndex->
                                                                 pIndex[0].
                                                                 u4_ULongValue,
                                                                 pMultiData->
                                                                 u4_ULongValue));

}

INT4
FsQoSPolicyMapExceedActionSetVlanPrioTest (UINT4 *pu4Error,
                                           tSnmpIndex * pMultiIndex,
                                           tRetVal * pMultiData)
{
    return (nmhTestv2FsQoSPolicyMapExceedActionSetVlanPrio (pu4Error,
                                                            pMultiIndex->
                                                            pIndex[0].
                                                            u4_ULongValue,
                                                            pMultiData->
                                                            u4_ULongValue));

}

INT4
FsQoSPolicyMapExceedActionSetVlanDETest (UINT4 *pu4Error,
                                         tSnmpIndex * pMultiIndex,
                                         tRetVal * pMultiData)
{
    return (nmhTestv2FsQoSPolicyMapExceedActionSetVlanDE (pu4Error,
                                                          pMultiIndex->
                                                          pIndex[0].
                                                          u4_ULongValue,
                                                          pMultiData->
                                                          u4_ULongValue));

}

INT4
FsQoSPolicyMapExceedActionSetMplsExpTest (UINT4 *pu4Error,
                                          tSnmpIndex * pMultiIndex,
                                          tRetVal * pMultiData)
{
    return (nmhTestv2FsQoSPolicyMapExceedActionSetMplsExp (pu4Error,
                                                           pMultiIndex->
                                                           pIndex[0].
                                                           u4_ULongValue,
                                                           pMultiData->
                                                           u4_ULongValue));

}

INT4
FsQoSPolicyMapExceedActionSetNewCLASSTest (UINT4 *pu4Error,
                                           tSnmpIndex * pMultiIndex,
                                           tRetVal * pMultiData)
{
    return (nmhTestv2FsQoSPolicyMapExceedActionSetNewCLASS (pu4Error,
                                                            pMultiIndex->
                                                            pIndex[0].
                                                            u4_ULongValue,
                                                            pMultiData->
                                                            u4_ULongValue));

}

INT4
FsQoSPolicyMapOutProfileActionFlagTest (UINT4 *pu4Error,
                                        tSnmpIndex * pMultiIndex,
                                        tRetVal * pMultiData)
{
    return (nmhTestv2FsQoSPolicyMapOutProfileActionFlag (pu4Error,
                                                         pMultiIndex->pIndex[0].
                                                         u4_ULongValue,
                                                         pMultiData->
                                                         pOctetStrValue));

}

INT4
FsQoSPolicyMapOutProfileActionIdTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    return (nmhTestv2FsQoSPolicyMapOutProfileActionId (pu4Error,
                                                       pMultiIndex->pIndex[0].
                                                       u4_ULongValue,
                                                       pMultiData->
                                                       u4_ULongValue));

}

INT4
FsQoSPolicyMapOutProfileActionSetIPTOSTest (UINT4 *pu4Error,
                                            tSnmpIndex * pMultiIndex,
                                            tRetVal * pMultiData)
{
    return (nmhTestv2FsQoSPolicyMapOutProfileActionSetIPTOS (pu4Error,
                                                             pMultiIndex->
                                                             pIndex[0].
                                                             u4_ULongValue,
                                                             pMultiData->
                                                             u4_ULongValue));

}

INT4
FsQoSPolicyMapOutProfileActionSetDscpTest (UINT4 *pu4Error,
                                           tSnmpIndex * pMultiIndex,
                                           tRetVal * pMultiData)
{
    return (nmhTestv2FsQoSPolicyMapOutProfileActionSetDscp (pu4Error,
                                                            pMultiIndex->
                                                            pIndex[0].
                                                            u4_ULongValue,
                                                            pMultiData->
                                                            i4_SLongValue));

}

INT4
FsQoSPolicyMapOutProfileActionSetInnerVlanPrioTest (UINT4 *pu4Error,
                                                    tSnmpIndex * pMultiIndex,
                                                    tRetVal * pMultiData)
{
    return (nmhTestv2FsQoSPolicyMapOutProfileActionSetInnerVlanPrio (pu4Error,
                                                                     pMultiIndex->
                                                                     pIndex[0].
                                                                     u4_ULongValue,
                                                                     pMultiData->
                                                                     u4_ULongValue));

}

INT4
FsQoSPolicyMapOutProfileActionSetVlanPrioTest (UINT4 *pu4Error,
                                               tSnmpIndex * pMultiIndex,
                                               tRetVal * pMultiData)
{
    return (nmhTestv2FsQoSPolicyMapOutProfileActionSetVlanPrio (pu4Error,
                                                                pMultiIndex->
                                                                pIndex[0].
                                                                u4_ULongValue,
                                                                pMultiData->
                                                                u4_ULongValue));

}

INT4
FsQoSPolicyMapOutProfileActionSetVlanDETest (UINT4 *pu4Error,
                                             tSnmpIndex * pMultiIndex,
                                             tRetVal * pMultiData)
{
    return (nmhTestv2FsQoSPolicyMapOutProfileActionSetVlanDE (pu4Error,
                                                              pMultiIndex->
                                                              pIndex[0].
                                                              u4_ULongValue,
                                                              pMultiData->
                                                              u4_ULongValue));

}

INT4
FsQoSPolicyMapOutProfileActionSetMplsExpTest (UINT4 *pu4Error,
                                              tSnmpIndex * pMultiIndex,
                                              tRetVal * pMultiData)
{
    return (nmhTestv2FsQoSPolicyMapOutProfileActionSetMplsExp (pu4Error,
                                                               pMultiIndex->
                                                               pIndex[0].
                                                               u4_ULongValue,
                                                               pMultiData->
                                                               u4_ULongValue));

}

INT4
FsQoSPolicyMapOutProfileActionSetNewCLASSTest (UINT4 *pu4Error,
                                               tSnmpIndex * pMultiIndex,
                                               tRetVal * pMultiData)
{
    return (nmhTestv2FsQoSPolicyMapOutProfileActionSetNewCLASS (pu4Error,
                                                                pMultiIndex->
                                                                pIndex[0].
                                                                u4_ULongValue,
                                                                pMultiData->
                                                                u4_ULongValue));

}

INT4
FsQoSPolicyMapStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                          tRetVal * pMultiData)
{
    return (nmhTestv2FsQoSPolicyMapStatus (pu4Error,
                                           pMultiIndex->pIndex[0].u4_ULongValue,
                                           pMultiData->i4_SLongValue));

}

INT4
FsQoSPolicyMapConformActionSetInnerVlanDETest (UINT4 *pu4Error,
                                               tSnmpIndex * pMultiIndex,
                                               tRetVal * pMultiData)
{
    return (nmhTestv2FsQoSPolicyMapConformActionSetInnerVlanDE (pu4Error,
                                                                pMultiIndex->
                                                                pIndex[0].
                                                                u4_ULongValue,
                                                                pMultiData->
                                                                u4_ULongValue));

}

INT4
FsQoSPolicyMapExceedActionSetInnerVlanDETest (UINT4 *pu4Error,
                                              tSnmpIndex * pMultiIndex,
                                              tRetVal * pMultiData)
{
    return (nmhTestv2FsQoSPolicyMapExceedActionSetInnerVlanDE (pu4Error,
                                                               pMultiIndex->
                                                               pIndex[0].
                                                               u4_ULongValue,
                                                               pMultiData->
                                                               u4_ULongValue));

}

INT4
FsQoSPolicyMapOutProfileActionSetInnerVlanDETest (UINT4 *pu4Error,
                                                  tSnmpIndex * pMultiIndex,
                                                  tRetVal * pMultiData)
{
    return (nmhTestv2FsQoSPolicyMapOutProfileActionSetInnerVlanDE (pu4Error,
                                                                   pMultiIndex->
                                                                   pIndex[0].
                                                                   u4_ULongValue,
                                                                   pMultiData->
                                                                   u4_ULongValue));

}

INT4 FsQoSPolicyDefaultVlanDETest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhTestv2FsQoSPolicyDefaultVlanDE(pu4Error,
        pMultiIndex->pIndex[0].u4_ULongValue,
        pMultiData->i4_SLongValue));
 
}
INT4
FsQoSPolicyMapTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                        tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsQoSPolicyMapTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFsQoSQTemplateTable (tSnmpIndex * pFirstMultiIndex,
                                 tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsQoSQTemplateTable
            (&(pNextMultiIndex->pIndex[0].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsQoSQTemplateTable
            (pFirstMultiIndex->pIndex[0].u4_ULongValue,
             &(pNextMultiIndex->pIndex[0].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
FsQoSQTemplateNameGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsQoSQTemplateTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsQoSQTemplateName (pMultiIndex->pIndex[0].u4_ULongValue,
                                      pMultiData->pOctetStrValue));

}

INT4
FsQoSQTemplateDropTypeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsQoSQTemplateTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsQoSQTemplateDropType (pMultiIndex->pIndex[0].u4_ULongValue,
                                          &(pMultiData->i4_SLongValue)));

}

INT4
FsQoSQTemplateDropAlgoEnableFlagGet (tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsQoSQTemplateTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsQoSQTemplateDropAlgoEnableFlag
            (pMultiIndex->pIndex[0].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsQoSQTemplateSizeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsQoSQTemplateTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsQoSQTemplateSize (pMultiIndex->pIndex[0].u4_ULongValue,
                                      &(pMultiData->u4_ULongValue)));

}

INT4
FsQoSQTemplateStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsQoSQTemplateTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsQoSQTemplateStatus (pMultiIndex->pIndex[0].u4_ULongValue,
                                        &(pMultiData->i4_SLongValue)));

}

INT4
FsQoSQTemplateNameSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsQoSQTemplateName (pMultiIndex->pIndex[0].u4_ULongValue,
                                      pMultiData->pOctetStrValue));

}

INT4
FsQoSQTemplateDropTypeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsQoSQTemplateDropType (pMultiIndex->pIndex[0].u4_ULongValue,
                                          pMultiData->i4_SLongValue));

}

INT4
FsQoSQTemplateDropAlgoEnableFlagSet (tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    return (nmhSetFsQoSQTemplateDropAlgoEnableFlag
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->i4_SLongValue));

}

INT4
FsQoSQTemplateSizeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsQoSQTemplateSize (pMultiIndex->pIndex[0].u4_ULongValue,
                                      pMultiData->u4_ULongValue));

}

INT4
FsQoSQTemplateStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsQoSQTemplateStatus (pMultiIndex->pIndex[0].u4_ULongValue,
                                        pMultiData->i4_SLongValue));

}

INT4
FsQoSQTemplateNameTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                        tRetVal * pMultiData)
{
    return (nmhTestv2FsQoSQTemplateName (pu4Error,
                                         pMultiIndex->pIndex[0].u4_ULongValue,
                                         pMultiData->pOctetStrValue));

}

INT4
FsQoSQTemplateDropTypeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                            tRetVal * pMultiData)
{
    return (nmhTestv2FsQoSQTemplateDropType (pu4Error,
                                             pMultiIndex->pIndex[0].
                                             u4_ULongValue,
                                             pMultiData->i4_SLongValue));

}

INT4
FsQoSQTemplateDropAlgoEnableFlagTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    return (nmhTestv2FsQoSQTemplateDropAlgoEnableFlag (pu4Error,
                                                       pMultiIndex->pIndex[0].
                                                       u4_ULongValue,
                                                       pMultiData->
                                                       i4_SLongValue));

}

INT4
FsQoSQTemplateSizeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                        tRetVal * pMultiData)
{
    return (nmhTestv2FsQoSQTemplateSize (pu4Error,
                                         pMultiIndex->pIndex[0].u4_ULongValue,
                                         pMultiData->u4_ULongValue));

}

INT4
FsQoSQTemplateStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                          tRetVal * pMultiData)
{
    return (nmhTestv2FsQoSQTemplateStatus (pu4Error,
                                           pMultiIndex->pIndex[0].u4_ULongValue,
                                           pMultiData->i4_SLongValue));

}

INT4
FsQoSQTemplateTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                        tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsQoSQTemplateTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFsQoSRandomDetectCfgTable (tSnmpIndex * pFirstMultiIndex,
                                       tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsQoSRandomDetectCfgTable
            (&(pNextMultiIndex->pIndex[0].u4_ULongValue),
             &(pNextMultiIndex->pIndex[1].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsQoSRandomDetectCfgTable
            (pFirstMultiIndex->pIndex[0].u4_ULongValue,
             &(pNextMultiIndex->pIndex[0].u4_ULongValue),
             pFirstMultiIndex->pIndex[1].i4_SLongValue,
             &(pNextMultiIndex->pIndex[1].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
FsQoSRandomDetectCfgMinAvgThreshGet (tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsQoSRandomDetectCfgTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsQoSRandomDetectCfgMinAvgThresh
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsQoSRandomDetectCfgMaxAvgThreshGet (tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsQoSRandomDetectCfgTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsQoSRandomDetectCfgMaxAvgThresh
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsQoSRandomDetectCfgMaxPktSizeGet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsQoSRandomDetectCfgTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsQoSRandomDetectCfgMaxPktSize
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsQoSRandomDetectCfgMaxProbGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsQoSRandomDetectCfgTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsQoSRandomDetectCfgMaxProb
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsQoSRandomDetectCfgExpWeightGet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsQoSRandomDetectCfgTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsQoSRandomDetectCfgExpWeight
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsQoSRandomDetectCfgStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsQoSRandomDetectCfgTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsQoSRandomDetectCfgStatus
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsQoSRandomDetectCfgGainGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsQoSRandomDetectCfgTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsQoSRandomDetectCfgGain
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsQoSRandomDetectCfgDropThreshTypeGet (tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsQoSRandomDetectCfgTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsQoSRandomDetectCfgDropThreshType
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsQoSRandomDetectCfgECNThreshGet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsQoSRandomDetectCfgTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsQoSRandomDetectCfgECNThresh
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsQoSRandomDetectCfgActionFlagGet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsQoSRandomDetectCfgTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsQoSRandomDetectCfgActionFlag
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].i4_SLongValue, pMultiData->pOctetStrValue));

}

INT4
FsQoSRandomDetectCfgMinAvgThreshSet (tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    return (nmhSetFsQoSRandomDetectCfgMinAvgThresh
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].i4_SLongValue, pMultiData->u4_ULongValue));

}

INT4
FsQoSRandomDetectCfgMaxAvgThreshSet (tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    return (nmhSetFsQoSRandomDetectCfgMaxAvgThresh
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].i4_SLongValue, pMultiData->u4_ULongValue));

}

INT4
FsQoSRandomDetectCfgMaxPktSizeSet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    return (nmhSetFsQoSRandomDetectCfgMaxPktSize
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].i4_SLongValue, pMultiData->u4_ULongValue));

}

INT4
FsQoSRandomDetectCfgMaxProbSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsQoSRandomDetectCfgMaxProb
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].i4_SLongValue, pMultiData->u4_ULongValue));

}

INT4
FsQoSRandomDetectCfgExpWeightSet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    return (nmhSetFsQoSRandomDetectCfgExpWeight
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].i4_SLongValue, pMultiData->u4_ULongValue));

}

INT4
FsQoSRandomDetectCfgStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsQoSRandomDetectCfgStatus
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
FsQoSRandomDetectCfgGainSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsQoSRandomDetectCfgGain
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].i4_SLongValue, pMultiData->u4_ULongValue));

}

INT4
FsQoSRandomDetectCfgDropThreshTypeSet (tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    return (nmhSetFsQoSRandomDetectCfgDropThreshType
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
FsQoSRandomDetectCfgECNThreshSet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    return (nmhSetFsQoSRandomDetectCfgECNThresh
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].i4_SLongValue, pMultiData->u4_ULongValue));

}

INT4
FsQoSRandomDetectCfgActionFlagSet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    return (nmhSetFsQoSRandomDetectCfgActionFlag
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].i4_SLongValue, pMultiData->pOctetStrValue));

}

INT4
FsQoSRandomDetectCfgMinAvgThreshTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    return (nmhTestv2FsQoSRandomDetectCfgMinAvgThresh (pu4Error,
                                                       pMultiIndex->pIndex[0].
                                                       u4_ULongValue,
                                                       pMultiIndex->pIndex[1].
                                                       i4_SLongValue,
                                                       pMultiData->
                                                       u4_ULongValue));

}

INT4
FsQoSRandomDetectCfgMaxAvgThreshTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    return (nmhTestv2FsQoSRandomDetectCfgMaxAvgThresh (pu4Error,
                                                       pMultiIndex->pIndex[0].
                                                       u4_ULongValue,
                                                       pMultiIndex->pIndex[1].
                                                       i4_SLongValue,
                                                       pMultiData->
                                                       u4_ULongValue));

}

INT4
FsQoSRandomDetectCfgMaxPktSizeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    return (nmhTestv2FsQoSRandomDetectCfgMaxPktSize (pu4Error,
                                                     pMultiIndex->pIndex[0].
                                                     u4_ULongValue,
                                                     pMultiIndex->pIndex[1].
                                                     i4_SLongValue,
                                                     pMultiData->
                                                     u4_ULongValue));

}

INT4
FsQoSRandomDetectCfgMaxProbTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                 tRetVal * pMultiData)
{
    return (nmhTestv2FsQoSRandomDetectCfgMaxProb (pu4Error,
                                                  pMultiIndex->pIndex[0].
                                                  u4_ULongValue,
                                                  pMultiIndex->pIndex[1].
                                                  i4_SLongValue,
                                                  pMultiData->u4_ULongValue));

}

INT4
FsQoSRandomDetectCfgExpWeightTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    return (nmhTestv2FsQoSRandomDetectCfgExpWeight (pu4Error,
                                                    pMultiIndex->pIndex[0].
                                                    u4_ULongValue,
                                                    pMultiIndex->pIndex[1].
                                                    i4_SLongValue,
                                                    pMultiData->u4_ULongValue));

}

INT4
FsQoSRandomDetectCfgStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                tRetVal * pMultiData)
{
    return (nmhTestv2FsQoSRandomDetectCfgStatus (pu4Error,
                                                 pMultiIndex->pIndex[0].
                                                 u4_ULongValue,
                                                 pMultiIndex->pIndex[1].
                                                 i4_SLongValue,
                                                 pMultiData->i4_SLongValue));

}

INT4
FsQoSRandomDetectCfgGainTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                              tRetVal * pMultiData)
{
    return (nmhTestv2FsQoSRandomDetectCfgGain (pu4Error,
                                               pMultiIndex->pIndex[0].
                                               u4_ULongValue,
                                               pMultiIndex->pIndex[1].
                                               i4_SLongValue,
                                               pMultiData->u4_ULongValue));

}

INT4
FsQoSRandomDetectCfgDropThreshTypeTest (UINT4 *pu4Error,
                                        tSnmpIndex * pMultiIndex,
                                        tRetVal * pMultiData)
{
    return (nmhTestv2FsQoSRandomDetectCfgDropThreshType (pu4Error,
                                                         pMultiIndex->pIndex[0].
                                                         u4_ULongValue,
                                                         pMultiIndex->pIndex[1].
                                                         i4_SLongValue,
                                                         pMultiData->
                                                         i4_SLongValue));

}

INT4
FsQoSRandomDetectCfgECNThreshTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    return (nmhTestv2FsQoSRandomDetectCfgECNThresh (pu4Error,
                                                    pMultiIndex->pIndex[0].
                                                    u4_ULongValue,
                                                    pMultiIndex->pIndex[1].
                                                    i4_SLongValue,
                                                    pMultiData->u4_ULongValue));

}

INT4
FsQoSRandomDetectCfgActionFlagTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    return (nmhTestv2FsQoSRandomDetectCfgActionFlag (pu4Error,
                                                     pMultiIndex->pIndex[0].
                                                     u4_ULongValue,
                                                     pMultiIndex->pIndex[1].
                                                     i4_SLongValue,
                                                     pMultiData->
                                                     pOctetStrValue));

}

INT4
FsQoSRandomDetectCfgTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                              tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsQoSRandomDetectCfgTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFsQoSShapeTemplateTable (tSnmpIndex * pFirstMultiIndex,
                                     tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsQoSShapeTemplateTable
            (&(pNextMultiIndex->pIndex[0].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsQoSShapeTemplateTable
            (pFirstMultiIndex->pIndex[0].u4_ULongValue,
             &(pNextMultiIndex->pIndex[0].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
FsQoSShapeTemplateNameGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsQoSShapeTemplateTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsQoSShapeTemplateName (pMultiIndex->pIndex[0].u4_ULongValue,
                                          pMultiData->pOctetStrValue));

}

INT4
FsQoSShapeTemplateCIRGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsQoSShapeTemplateTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsQoSShapeTemplateCIR (pMultiIndex->pIndex[0].u4_ULongValue,
                                         &(pMultiData->u4_ULongValue)));

}

INT4
FsQoSShapeTemplateCBSGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsQoSShapeTemplateTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsQoSShapeTemplateCBS (pMultiIndex->pIndex[0].u4_ULongValue,
                                         &(pMultiData->u4_ULongValue)));

}

INT4
FsQoSShapeTemplateEIRGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsQoSShapeTemplateTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsQoSShapeTemplateEIR (pMultiIndex->pIndex[0].u4_ULongValue,
                                         &(pMultiData->u4_ULongValue)));

}

INT4
FsQoSShapeTemplateEBSGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsQoSShapeTemplateTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsQoSShapeTemplateEBS (pMultiIndex->pIndex[0].u4_ULongValue,
                                         &(pMultiData->u4_ULongValue)));

}

INT4
FsQoSShapeTemplateStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsQoSShapeTemplateTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsQoSShapeTemplateStatus
            (pMultiIndex->pIndex[0].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsQoSShapeTemplateNameSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsQoSShapeTemplateName (pMultiIndex->pIndex[0].u4_ULongValue,
                                          pMultiData->pOctetStrValue));

}

INT4
FsQoSShapeTemplateCIRSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsQoSShapeTemplateCIR (pMultiIndex->pIndex[0].u4_ULongValue,
                                         pMultiData->u4_ULongValue));

}

INT4
FsQoSShapeTemplateCBSSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsQoSShapeTemplateCBS (pMultiIndex->pIndex[0].u4_ULongValue,
                                         pMultiData->u4_ULongValue));

}

INT4
FsQoSShapeTemplateEIRSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsQoSShapeTemplateEIR (pMultiIndex->pIndex[0].u4_ULongValue,
                                         pMultiData->u4_ULongValue));

}

INT4
FsQoSShapeTemplateEBSSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsQoSShapeTemplateEBS (pMultiIndex->pIndex[0].u4_ULongValue,
                                         pMultiData->u4_ULongValue));

}

INT4
FsQoSShapeTemplateStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsQoSShapeTemplateStatus
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->i4_SLongValue));

}

INT4
FsQoSShapeTemplateNameTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                            tRetVal * pMultiData)
{
    return (nmhTestv2FsQoSShapeTemplateName (pu4Error,
                                             pMultiIndex->pIndex[0].
                                             u4_ULongValue,
                                             pMultiData->pOctetStrValue));

}

INT4
FsQoSShapeTemplateCIRTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                           tRetVal * pMultiData)
{
    return (nmhTestv2FsQoSShapeTemplateCIR (pu4Error,
                                            pMultiIndex->pIndex[0].
                                            u4_ULongValue,
                                            pMultiData->u4_ULongValue));

}

INT4
FsQoSShapeTemplateCBSTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                           tRetVal * pMultiData)
{
    return (nmhTestv2FsQoSShapeTemplateCBS (pu4Error,
                                            pMultiIndex->pIndex[0].
                                            u4_ULongValue,
                                            pMultiData->u4_ULongValue));

}

INT4
FsQoSShapeTemplateEIRTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                           tRetVal * pMultiData)
{
    return (nmhTestv2FsQoSShapeTemplateEIR (pu4Error,
                                            pMultiIndex->pIndex[0].
                                            u4_ULongValue,
                                            pMultiData->u4_ULongValue));

}

INT4
FsQoSShapeTemplateEBSTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                           tRetVal * pMultiData)
{
    return (nmhTestv2FsQoSShapeTemplateEBS (pu4Error,
                                            pMultiIndex->pIndex[0].
                                            u4_ULongValue,
                                            pMultiData->u4_ULongValue));

}

INT4
FsQoSShapeTemplateStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                              tRetVal * pMultiData)
{
    return (nmhTestv2FsQoSShapeTemplateStatus (pu4Error,
                                               pMultiIndex->pIndex[0].
                                               u4_ULongValue,
                                               pMultiData->i4_SLongValue));

}

INT4
FsQoSShapeTemplateTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                            tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsQoSShapeTemplateTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFsQoSQMapTable (tSnmpIndex * pFirstMultiIndex,
                            tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsQoSQMapTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue),
             &(pNextMultiIndex->pIndex[1].u4_ULongValue),
             &(pNextMultiIndex->pIndex[2].i4_SLongValue),
             &(pNextMultiIndex->pIndex[3].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsQoSQMapTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue),
             pFirstMultiIndex->pIndex[1].u4_ULongValue,
             &(pNextMultiIndex->pIndex[1].u4_ULongValue),
             pFirstMultiIndex->pIndex[2].i4_SLongValue,
             &(pNextMultiIndex->pIndex[2].i4_SLongValue),
             pFirstMultiIndex->pIndex[3].u4_ULongValue,
             &(pNextMultiIndex->pIndex[3].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
FsQoSQMapQIdGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsQoSQMapTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].i4_SLongValue,
         pMultiIndex->pIndex[3].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsQoSQMapQId (pMultiIndex->pIndex[0].i4_SLongValue,
                                pMultiIndex->pIndex[1].u4_ULongValue,
                                pMultiIndex->pIndex[2].i4_SLongValue,
                                pMultiIndex->pIndex[3].u4_ULongValue,
                                &(pMultiData->u4_ULongValue)));

}

INT4
FsQoSQMapStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsQoSQMapTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].i4_SLongValue,
         pMultiIndex->pIndex[3].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsQoSQMapStatus (pMultiIndex->pIndex[0].i4_SLongValue,
                                   pMultiIndex->pIndex[1].u4_ULongValue,
                                   pMultiIndex->pIndex[2].i4_SLongValue,
                                   pMultiIndex->pIndex[3].u4_ULongValue,
                                   &(pMultiData->i4_SLongValue)));

}
INT4 FsQoSQMapRegenDEIGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsQoSQMapTable(
        pMultiIndex->pIndex[0].i4_SLongValue,
        pMultiIndex->pIndex[1].u4_ULongValue,
        pMultiIndex->pIndex[2].i4_SLongValue,
        pMultiIndex->pIndex[3].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return(nmhGetFsQoSQMapRegenDEI(
        pMultiIndex->pIndex[0].i4_SLongValue,
        pMultiIndex->pIndex[1].u4_ULongValue,
        pMultiIndex->pIndex[2].i4_SLongValue,
        pMultiIndex->pIndex[3].u4_ULongValue,
        &(pMultiData->i4_SLongValue)));
 
}
 
INT4 FsQoSQMapRegenColorGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsQoSQMapTable(
        pMultiIndex->pIndex[0].i4_SLongValue,
        pMultiIndex->pIndex[1].u4_ULongValue,
        pMultiIndex->pIndex[2].i4_SLongValue,
        pMultiIndex->pIndex[3].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return(nmhGetFsQoSQMapRegenColor(
        pMultiIndex->pIndex[0].i4_SLongValue,
        pMultiIndex->pIndex[1].u4_ULongValue,
        pMultiIndex->pIndex[2].i4_SLongValue,
        pMultiIndex->pIndex[3].u4_ULongValue,
        &(pMultiData->i4_SLongValue)));

}
INT4
FsQoSQMapQIdSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsQoSQMapQId (pMultiIndex->pIndex[0].i4_SLongValue,
                                pMultiIndex->pIndex[1].u4_ULongValue,
                                pMultiIndex->pIndex[2].i4_SLongValue,
                                pMultiIndex->pIndex[3].u4_ULongValue,
                                pMultiData->u4_ULongValue));

}

INT4
FsQoSQMapStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsQoSQMapStatus (pMultiIndex->pIndex[0].i4_SLongValue,
                                   pMultiIndex->pIndex[1].u4_ULongValue,
                                   pMultiIndex->pIndex[2].i4_SLongValue,
                                   pMultiIndex->pIndex[3].u4_ULongValue,
                                   pMultiData->i4_SLongValue));
}
 
INT4 FsQoSQMapRegenDEISet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsQoSQMapRegenDEI(
        pMultiIndex->pIndex[0].i4_SLongValue,
        pMultiIndex->pIndex[1].u4_ULongValue,
        pMultiIndex->pIndex[2].i4_SLongValue,
        pMultiIndex->pIndex[3].u4_ULongValue,
        pMultiData->i4_SLongValue));

}

INT4
FsQoSQMapQIdTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                  tRetVal * pMultiData)
{
    return (nmhTestv2FsQoSQMapQId (pu4Error,
                                   pMultiIndex->pIndex[0].i4_SLongValue,
                                   pMultiIndex->pIndex[1].u4_ULongValue,
                                   pMultiIndex->pIndex[2].i4_SLongValue,
                                   pMultiIndex->pIndex[3].u4_ULongValue,
                                   pMultiData->u4_ULongValue));

}

INT4
FsQoSQMapStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                     tRetVal * pMultiData)
{
    return (nmhTestv2FsQoSQMapStatus (pu4Error,
                                      pMultiIndex->pIndex[0].i4_SLongValue,
                                      pMultiIndex->pIndex[1].u4_ULongValue,
                                      pMultiIndex->pIndex[2].i4_SLongValue,
                                      pMultiIndex->pIndex[3].u4_ULongValue,
                                      pMultiData->i4_SLongValue));
	}

INT4 FsQoSQMapRegenDEITest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhTestv2FsQoSQMapRegenDEI(pu4Error,
        pMultiIndex->pIndex[0].i4_SLongValue,
        pMultiIndex->pIndex[1].u4_ULongValue,
        pMultiIndex->pIndex[2].i4_SLongValue,
        pMultiIndex->pIndex[3].u4_ULongValue,
        pMultiData->i4_SLongValue));
}

INT4
FsQoSQMapTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                   tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsQoSQMapTable (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFsQoSQTable (tSnmpIndex * pFirstMultiIndex,
                         tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsQoSQTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue),
             &(pNextMultiIndex->pIndex[1].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsQoSQTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue),
             pFirstMultiIndex->pIndex[1].u4_ULongValue,
             &(pNextMultiIndex->pIndex[1].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
FsQoSQCfgTemplateIdGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsQoSQTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsQoSQCfgTemplateId (pMultiIndex->pIndex[0].i4_SLongValue,
                                       pMultiIndex->pIndex[1].u4_ULongValue,
                                       &(pMultiData->u4_ULongValue)));

}

INT4
FsQoSQSchedulerIdGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsQoSQTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsQoSQSchedulerId (pMultiIndex->pIndex[0].i4_SLongValue,
                                     pMultiIndex->pIndex[1].u4_ULongValue,
                                     &(pMultiData->u4_ULongValue)));

}

INT4
FsQoSQWeightGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsQoSQTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsQoSQWeight (pMultiIndex->pIndex[0].i4_SLongValue,
                                pMultiIndex->pIndex[1].u4_ULongValue,
                                &(pMultiData->u4_ULongValue)));

}

INT4
FsQoSQPriorityGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsQoSQTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsQoSQPriority (pMultiIndex->pIndex[0].i4_SLongValue,
                                  pMultiIndex->pIndex[1].u4_ULongValue,
                                  &(pMultiData->u4_ULongValue)));

}

INT4
FsQoSQShapeIdGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsQoSQTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsQoSQShapeId (pMultiIndex->pIndex[0].i4_SLongValue,
                                 pMultiIndex->pIndex[1].u4_ULongValue,
                                 &(pMultiData->u4_ULongValue)));

}

INT4
FsQoSQStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsQoSQTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsQoSQStatus (pMultiIndex->pIndex[0].i4_SLongValue,
                                pMultiIndex->pIndex[1].u4_ULongValue,
                                &(pMultiData->i4_SLongValue)));

}

INT4
FsQoSQTypeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsQoSQTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsQoSQType (pMultiIndex->pIndex[0].i4_SLongValue,
                              pMultiIndex->pIndex[1].u4_ULongValue,
                              &(pMultiData->u4_ULongValue)));

}

INT4
FsQoSQCfgTemplateIdSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsQoSQCfgTemplateId (pMultiIndex->pIndex[0].i4_SLongValue,
                                       pMultiIndex->pIndex[1].u4_ULongValue,
                                       pMultiData->u4_ULongValue));

}

INT4
FsQoSQSchedulerIdSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsQoSQSchedulerId (pMultiIndex->pIndex[0].i4_SLongValue,
                                     pMultiIndex->pIndex[1].u4_ULongValue,
                                     pMultiData->u4_ULongValue));

}

INT4
FsQoSQWeightSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsQoSQWeight (pMultiIndex->pIndex[0].i4_SLongValue,
                                pMultiIndex->pIndex[1].u4_ULongValue,
                                pMultiData->u4_ULongValue));

}

INT4
FsQoSQPrioritySet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsQoSQPriority (pMultiIndex->pIndex[0].i4_SLongValue,
                                  pMultiIndex->pIndex[1].u4_ULongValue,
                                  pMultiData->u4_ULongValue));

}

INT4
FsQoSQShapeIdSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsQoSQShapeId (pMultiIndex->pIndex[0].i4_SLongValue,
                                 pMultiIndex->pIndex[1].u4_ULongValue,
                                 pMultiData->u4_ULongValue));

}

INT4
FsQoSQStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsQoSQStatus (pMultiIndex->pIndex[0].i4_SLongValue,
                                pMultiIndex->pIndex[1].u4_ULongValue,
                                pMultiData->i4_SLongValue));

}

INT4
FsQoSQTypeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsQoSQType (pMultiIndex->pIndex[0].i4_SLongValue,
                              pMultiIndex->pIndex[1].u4_ULongValue,
                              pMultiData->u4_ULongValue));

}

INT4
FsQoSQCfgTemplateIdTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                         tRetVal * pMultiData)
{
    return (nmhTestv2FsQoSQCfgTemplateId (pu4Error,
                                          pMultiIndex->pIndex[0].i4_SLongValue,
                                          pMultiIndex->pIndex[1].u4_ULongValue,
                                          pMultiData->u4_ULongValue));

}

INT4
FsQoSQSchedulerIdTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                       tRetVal * pMultiData)
{
    return (nmhTestv2FsQoSQSchedulerId (pu4Error,
                                        pMultiIndex->pIndex[0].i4_SLongValue,
                                        pMultiIndex->pIndex[1].u4_ULongValue,
                                        pMultiData->u4_ULongValue));

}

INT4
FsQoSQWeightTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                  tRetVal * pMultiData)
{
    return (nmhTestv2FsQoSQWeight (pu4Error,
                                   pMultiIndex->pIndex[0].i4_SLongValue,
                                   pMultiIndex->pIndex[1].u4_ULongValue,
                                   pMultiData->u4_ULongValue));

}

INT4
FsQoSQPriorityTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                    tRetVal * pMultiData)
{
    return (nmhTestv2FsQoSQPriority (pu4Error,
                                     pMultiIndex->pIndex[0].i4_SLongValue,
                                     pMultiIndex->pIndex[1].u4_ULongValue,
                                     pMultiData->u4_ULongValue));

}

INT4
FsQoSQShapeIdTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                   tRetVal * pMultiData)
{
    return (nmhTestv2FsQoSQShapeId (pu4Error,
                                    pMultiIndex->pIndex[0].i4_SLongValue,
                                    pMultiIndex->pIndex[1].u4_ULongValue,
                                    pMultiData->u4_ULongValue));

}

INT4
FsQoSQStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                  tRetVal * pMultiData)
{
    return (nmhTestv2FsQoSQStatus (pu4Error,
                                   pMultiIndex->pIndex[0].i4_SLongValue,
                                   pMultiIndex->pIndex[1].u4_ULongValue,
                                   pMultiData->i4_SLongValue));

}

INT4
FsQoSQTypeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhTestv2FsQoSQType (pu4Error,
                                 pMultiIndex->pIndex[0].i4_SLongValue,
                                 pMultiIndex->pIndex[1].u4_ULongValue,
                                 pMultiData->u4_ULongValue));

}

INT4
FsQoSQTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsQoSQTable (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFsQoSSchedulerTable (tSnmpIndex * pFirstMultiIndex,
                                 tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsQoSSchedulerTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue),
             &(pNextMultiIndex->pIndex[1].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsQoSSchedulerTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue),
             pFirstMultiIndex->pIndex[1].u4_ULongValue,
             &(pNextMultiIndex->pIndex[1].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
FsQoSSchedulerSchedAlgorithmGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsQoSSchedulerTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsQoSSchedulerSchedAlgorithm
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsQoSSchedulerShapeIdGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsQoSSchedulerTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsQoSSchedulerShapeId (pMultiIndex->pIndex[0].i4_SLongValue,
                                         pMultiIndex->pIndex[1].u4_ULongValue,
                                         &(pMultiData->u4_ULongValue)));

}

INT4
FsQoSSchedulerHierarchyLevelGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsQoSSchedulerTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsQoSSchedulerHierarchyLevel
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsQoSSchedulerGlobalIdGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsQoSSchedulerTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsQoSSchedulerGlobalId (pMultiIndex->pIndex[0].i4_SLongValue,
                                          pMultiIndex->pIndex[1].u4_ULongValue,
                                          &(pMultiData->u4_ULongValue)));

}

INT4
FsQoSSchedulerStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsQoSSchedulerTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsQoSSchedulerStatus (pMultiIndex->pIndex[0].i4_SLongValue,
                                        pMultiIndex->pIndex[1].u4_ULongValue,
                                        &(pMultiData->i4_SLongValue)));

}

INT4
FsQoSSchedulerSchedAlgorithmSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsQoSSchedulerSchedAlgorithm
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].u4_ULongValue, pMultiData->i4_SLongValue));

}

INT4
FsQoSSchedulerShapeIdSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsQoSSchedulerShapeId (pMultiIndex->pIndex[0].i4_SLongValue,
                                         pMultiIndex->pIndex[1].u4_ULongValue,
                                         pMultiData->u4_ULongValue));

}

INT4
FsQoSSchedulerHierarchyLevelSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsQoSSchedulerHierarchyLevel
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].u4_ULongValue, pMultiData->u4_ULongValue));

}

INT4
FsQoSSchedulerGlobalIdSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsQoSSchedulerGlobalId (pMultiIndex->pIndex[0].i4_SLongValue,
                                          pMultiIndex->pIndex[1].u4_ULongValue,
                                          pMultiData->u4_ULongValue));

}

INT4
FsQoSSchedulerStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsQoSSchedulerStatus (pMultiIndex->pIndex[0].i4_SLongValue,
                                        pMultiIndex->pIndex[1].u4_ULongValue,
                                        pMultiData->i4_SLongValue));

}

INT4
FsQoSSchedulerSchedAlgorithmTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    return (nmhTestv2FsQoSSchedulerSchedAlgorithm (pu4Error,
                                                   pMultiIndex->pIndex[0].
                                                   i4_SLongValue,
                                                   pMultiIndex->pIndex[1].
                                                   u4_ULongValue,
                                                   pMultiData->i4_SLongValue));

}

INT4
FsQoSSchedulerShapeIdTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                           tRetVal * pMultiData)
{
    return (nmhTestv2FsQoSSchedulerShapeId (pu4Error,
                                            pMultiIndex->pIndex[0].
                                            i4_SLongValue,
                                            pMultiIndex->pIndex[1].
                                            u4_ULongValue,
                                            pMultiData->u4_ULongValue));

}

INT4
FsQoSSchedulerHierarchyLevelTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    return (nmhTestv2FsQoSSchedulerHierarchyLevel (pu4Error,
                                                   pMultiIndex->pIndex[0].
                                                   i4_SLongValue,
                                                   pMultiIndex->pIndex[1].
                                                   u4_ULongValue,
                                                   pMultiData->u4_ULongValue));

}

INT4
FsQoSSchedulerGlobalIdTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                            tRetVal * pMultiData)
{
    return (nmhTestv2FsQoSSchedulerGlobalId (pu4Error,
                                             pMultiIndex->pIndex[0].
                                             i4_SLongValue,
                                             pMultiIndex->pIndex[1].
                                             u4_ULongValue,
                                             pMultiData->u4_ULongValue));

}

INT4
FsQoSSchedulerStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                          tRetVal * pMultiData)
{
    return (nmhTestv2FsQoSSchedulerStatus (pu4Error,
                                           pMultiIndex->pIndex[0].i4_SLongValue,
                                           pMultiIndex->pIndex[1].u4_ULongValue,
                                           pMultiData->i4_SLongValue));

}

INT4
FsQoSSchedulerTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                        tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsQoSSchedulerTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFsQoSHierarchyTable (tSnmpIndex * pFirstMultiIndex,
                                 tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsQoSHierarchyTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue),
             &(pNextMultiIndex->pIndex[1].u4_ULongValue),
             &(pNextMultiIndex->pIndex[2].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsQoSHierarchyTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue),
             pFirstMultiIndex->pIndex[1].u4_ULongValue,
             &(pNextMultiIndex->pIndex[1].u4_ULongValue),
             pFirstMultiIndex->pIndex[2].u4_ULongValue,
             &(pNextMultiIndex->pIndex[2].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
FsQoSHierarchyQNextGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsQoSHierarchyTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsQoSHierarchyQNext (pMultiIndex->pIndex[0].i4_SLongValue,
                                       pMultiIndex->pIndex[1].u4_ULongValue,
                                       pMultiIndex->pIndex[2].u4_ULongValue,
                                       &(pMultiData->u4_ULongValue)));

}

INT4
FsQoSHierarchySchedNextGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsQoSHierarchyTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsQoSHierarchySchedNext (pMultiIndex->pIndex[0].i4_SLongValue,
                                           pMultiIndex->pIndex[1].u4_ULongValue,
                                           pMultiIndex->pIndex[2].u4_ULongValue,
                                           &(pMultiData->u4_ULongValue)));

}

INT4
FsQoSHierarchyWeightGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsQoSHierarchyTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsQoSHierarchyWeight (pMultiIndex->pIndex[0].i4_SLongValue,
                                        pMultiIndex->pIndex[1].u4_ULongValue,
                                        pMultiIndex->pIndex[2].u4_ULongValue,
                                        &(pMultiData->u4_ULongValue)));

}

INT4
FsQoSHierarchyPriorityGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsQoSHierarchyTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsQoSHierarchyPriority (pMultiIndex->pIndex[0].i4_SLongValue,
                                          pMultiIndex->pIndex[1].u4_ULongValue,
                                          pMultiIndex->pIndex[2].u4_ULongValue,
                                          &(pMultiData->i4_SLongValue)));

}

INT4
FsQoSHierarchyStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsQoSHierarchyTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsQoSHierarchyStatus (pMultiIndex->pIndex[0].i4_SLongValue,
                                        pMultiIndex->pIndex[1].u4_ULongValue,
                                        pMultiIndex->pIndex[2].u4_ULongValue,
                                        &(pMultiData->i4_SLongValue)));

}

INT4
FsQoSHierarchyQNextSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsQoSHierarchyQNext (pMultiIndex->pIndex[0].i4_SLongValue,
                                       pMultiIndex->pIndex[1].u4_ULongValue,
                                       pMultiIndex->pIndex[2].u4_ULongValue,
                                       pMultiData->u4_ULongValue));

}

INT4
FsQoSHierarchySchedNextSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsQoSHierarchySchedNext (pMultiIndex->pIndex[0].i4_SLongValue,
                                           pMultiIndex->pIndex[1].u4_ULongValue,
                                           pMultiIndex->pIndex[2].u4_ULongValue,
                                           pMultiData->u4_ULongValue));

}

INT4
FsQoSHierarchyWeightSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsQoSHierarchyWeight (pMultiIndex->pIndex[0].i4_SLongValue,
                                        pMultiIndex->pIndex[1].u4_ULongValue,
                                        pMultiIndex->pIndex[2].u4_ULongValue,
                                        pMultiData->u4_ULongValue));

}

INT4
FsQoSHierarchyPrioritySet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsQoSHierarchyPriority (pMultiIndex->pIndex[0].i4_SLongValue,
                                          pMultiIndex->pIndex[1].u4_ULongValue,
                                          pMultiIndex->pIndex[2].u4_ULongValue,
                                          pMultiData->i4_SLongValue));

}

INT4
FsQoSHierarchyStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsQoSHierarchyStatus (pMultiIndex->pIndex[0].i4_SLongValue,
                                        pMultiIndex->pIndex[1].u4_ULongValue,
                                        pMultiIndex->pIndex[2].u4_ULongValue,
                                        pMultiData->i4_SLongValue));

}

INT4
FsQoSHierarchyQNextTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                         tRetVal * pMultiData)
{
    return (nmhTestv2FsQoSHierarchyQNext (pu4Error,
                                          pMultiIndex->pIndex[0].i4_SLongValue,
                                          pMultiIndex->pIndex[1].u4_ULongValue,
                                          pMultiIndex->pIndex[2].u4_ULongValue,
                                          pMultiData->u4_ULongValue));

}

INT4
FsQoSHierarchySchedNextTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                             tRetVal * pMultiData)
{
    return (nmhTestv2FsQoSHierarchySchedNext (pu4Error,
                                              pMultiIndex->pIndex[0].
                                              i4_SLongValue,
                                              pMultiIndex->pIndex[1].
                                              u4_ULongValue,
                                              pMultiIndex->pIndex[2].
                                              u4_ULongValue,
                                              pMultiData->u4_ULongValue));

}

INT4
FsQoSHierarchyWeightTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                          tRetVal * pMultiData)
{
    return (nmhTestv2FsQoSHierarchyWeight (pu4Error,
                                           pMultiIndex->pIndex[0].i4_SLongValue,
                                           pMultiIndex->pIndex[1].u4_ULongValue,
                                           pMultiIndex->pIndex[2].u4_ULongValue,
                                           pMultiData->u4_ULongValue));

}

INT4
FsQoSHierarchyPriorityTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                            tRetVal * pMultiData)
{
    return (nmhTestv2FsQoSHierarchyPriority (pu4Error,
                                             pMultiIndex->pIndex[0].
                                             i4_SLongValue,
                                             pMultiIndex->pIndex[1].
                                             u4_ULongValue,
                                             pMultiIndex->pIndex[2].
                                             u4_ULongValue,
                                             pMultiData->i4_SLongValue));

}

INT4
FsQoSHierarchyStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                          tRetVal * pMultiData)
{
    return (nmhTestv2FsQoSHierarchyStatus (pu4Error,
                                           pMultiIndex->pIndex[0].i4_SLongValue,
                                           pMultiIndex->pIndex[1].u4_ULongValue,
                                           pMultiIndex->pIndex[2].u4_ULongValue,
                                           pMultiData->i4_SLongValue));

}

INT4
FsQoSHierarchyTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                        tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsQoSHierarchyTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFsQoSDefUserPriorityTable (tSnmpIndex * pFirstMultiIndex,
                                       tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsQoSDefUserPriorityTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsQoSDefUserPriorityTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
FsQoSPortDefaultUserPriorityGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsQoSDefUserPriorityTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsQoSPortDefaultUserPriority
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsQoSPortPbitPrefOverDscpGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsQoSDefUserPriorityTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsQoSPortPbitPrefOverDscp
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsQoSPortDefaultUserPrioritySet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsQoSPortDefaultUserPriority
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
FsQoSPortPbitPrefOverDscpSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsQoSPortPbitPrefOverDscp
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
FsQoSPortPbitPrefOverDscpTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                               tRetVal * pMultiData)
{
    return (nmhTestv2FsQoSPortPbitPrefOverDscp (pu4Error,
                                                pMultiIndex->pIndex[0].
                                                i4_SLongValue,
                                                pMultiData->i4_SLongValue));

}

INT4
FsQoSPortDefaultUserPriorityTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    return (nmhTestv2FsQoSPortDefaultUserPriority (pu4Error,
                                                   pMultiIndex->pIndex[0].
                                                   i4_SLongValue,
                                                   pMultiData->i4_SLongValue));

}

INT4
FsQoSDefUserPriorityTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                              tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsQoSDefUserPriorityTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFsQoSPolicerStatsTable (tSnmpIndex * pFirstMultiIndex,
                                    tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsQoSPolicerStatsTable
            (&(pNextMultiIndex->pIndex[0].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsQoSPolicerStatsTable
            (pFirstMultiIndex->pIndex[0].u4_ULongValue,
             &(pNextMultiIndex->pIndex[0].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
FsQoSPolicerStatsConformPktsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsQoSPolicerStatsTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsQoSPolicerStatsConformPkts
            (pMultiIndex->pIndex[0].u4_ULongValue,
             &(pMultiData->u8_Counter64Value)));

}

INT4
FsQoSPolicerStatsConformOctetsGet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsQoSPolicerStatsTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsQoSPolicerStatsConformOctets
            (pMultiIndex->pIndex[0].u4_ULongValue,
             &(pMultiData->u8_Counter64Value)));

}

INT4
FsQoSPolicerStatsExceedPktsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsQoSPolicerStatsTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsQoSPolicerStatsExceedPkts
            (pMultiIndex->pIndex[0].u4_ULongValue,
             &(pMultiData->u8_Counter64Value)));

}

INT4
FsQoSPolicerStatsExceedOctetsGet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsQoSPolicerStatsTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsQoSPolicerStatsExceedOctets
            (pMultiIndex->pIndex[0].u4_ULongValue,
             &(pMultiData->u8_Counter64Value)));

}

INT4
FsQoSPolicerStatsViolatePktsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsQoSPolicerStatsTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsQoSPolicerStatsViolatePkts
            (pMultiIndex->pIndex[0].u4_ULongValue,
             &(pMultiData->u8_Counter64Value)));

}

INT4
FsQoSPolicerStatsViolateOctetsGet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsQoSPolicerStatsTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsQoSPolicerStatsViolateOctets
            (pMultiIndex->pIndex[0].u4_ULongValue,
             &(pMultiData->u8_Counter64Value)));

}

INT4 FsQoSPolicerStatsStatusGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsQoSPolicerStatsTable(
        pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return(nmhGetFsQoSPolicerStatsStatus(
        pMultiIndex->pIndex[0].u4_ULongValue,
        &(pMultiData->i4_SLongValue)));

}
INT4 FsQoSPolicerStatsClearCounterGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsQoSPolicerStatsTable(
        pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return(nmhGetFsQoSPolicerStatsClearCounter(
        pMultiIndex->pIndex[0].u4_ULongValue,
        &(pMultiData->i4_SLongValue)));

}
INT4 FsQoSPolicerStatsStatusSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsQoSPolicerStatsStatus(
        pMultiIndex->pIndex[0].u4_ULongValue,
        pMultiData->i4_SLongValue));

}

INT4 FsQoSPolicerStatsClearCounterSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsQoSPolicerStatsClearCounter(
        pMultiIndex->pIndex[0].u4_ULongValue,
        pMultiData->i4_SLongValue));

}

INT4 FsQoSPolicerStatsStatusTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhTestv2FsQoSPolicerStatsStatus(pu4Error,
        pMultiIndex->pIndex[0].u4_ULongValue,
        pMultiData->i4_SLongValue));

}

INT4 FsQoSPolicerStatsClearCounterTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhTestv2FsQoSPolicerStatsClearCounter(pu4Error,
        pMultiIndex->pIndex[0].u4_ULongValue,
        pMultiData->i4_SLongValue));

}

INT4 FsQoSPolicerStatsTableDep(UINT4 *pu4Error, tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpvarbinds)
{
    return(nmhDepv2FsQoSPolicerStatsTable(pu4Error, pSnmpIndexList, pSnmpvarbinds));
}


INT4
GetNextIndexFsQoSCoSQStatsTable (tSnmpIndex * pFirstMultiIndex,
                                 tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsQoSCoSQStatsTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue),
             &(pNextMultiIndex->pIndex[1].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsQoSCoSQStatsTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue),
             pFirstMultiIndex->pIndex[1].u4_ULongValue,
             &(pNextMultiIndex->pIndex[1].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
FsQoSCoSQStatsEnQPktsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsQoSCoSQStatsTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsQoSCoSQStatsEnQPkts (pMultiIndex->pIndex[0].i4_SLongValue,
                                         pMultiIndex->pIndex[1].u4_ULongValue,
                                         &(pMultiData->u8_Counter64Value)));

}

INT4
FsQoSCoSQStatsEnQBytesGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsQoSCoSQStatsTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsQoSCoSQStatsEnQBytes (pMultiIndex->pIndex[0].i4_SLongValue,
                                          pMultiIndex->pIndex[1].u4_ULongValue,
                                          &(pMultiData->u8_Counter64Value)));

}

INT4
FsQoSCoSQStatsDeQPktsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsQoSCoSQStatsTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsQoSCoSQStatsDeQPkts (pMultiIndex->pIndex[0].i4_SLongValue,
                                         pMultiIndex->pIndex[1].u4_ULongValue,
                                         &(pMultiData->u8_Counter64Value)));

}

INT4
FsQoSCoSQStatsDeQBytesGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsQoSCoSQStatsTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsQoSCoSQStatsDeQBytes (pMultiIndex->pIndex[0].i4_SLongValue,
                                          pMultiIndex->pIndex[1].u4_ULongValue,
                                          &(pMultiData->u8_Counter64Value)));

}

INT4
FsQoSCoSQStatsDiscardPktsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsQoSCoSQStatsTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsQoSCoSQStatsDiscardPkts
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             &(pMultiData->u8_Counter64Value)));

}

INT4
FsQoSCoSQStatsDiscardBytesGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsQoSCoSQStatsTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsQoSCoSQStatsDiscardBytes
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             &(pMultiData->u8_Counter64Value)));

}

INT4
FsQoSCoSQStatsOccupancyGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsQoSCoSQStatsTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsQoSCoSQStatsOccupancy (pMultiIndex->pIndex[0].i4_SLongValue,
                                           pMultiIndex->pIndex[1].u4_ULongValue,
                                           &(pMultiData->u8_Counter64Value)));

}

INT4
FsQoSCoSQStatsCongMgntAlgoDropGet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsQoSCoSQStatsTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsQoSCoSQStatsCongMgntAlgoDrop
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             &(pMultiData->u8_Counter64Value)));

}

INT4
GetNextIndexFsQosHwCpuRateLimitTable (tSnmpIndex * pFirstMultiIndex,
                                      tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsQosHwCpuRateLimitTable
            (&(pNextMultiIndex->pIndex[0].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsQosHwCpuRateLimitTable
            (pFirstMultiIndex->pIndex[0].u4_ULongValue,
             &(pNextMultiIndex->pIndex[0].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
FsQosHwCpuMinRateGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsQosHwCpuRateLimitTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsQosHwCpuMinRate (pMultiIndex->pIndex[0].u4_ULongValue,
                                     &(pMultiData->u4_ULongValue)));
}

INT4
FsQosHwCpuMaxRateGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsQosHwCpuRateLimitTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsQosHwCpuMaxRate (pMultiIndex->pIndex[0].u4_ULongValue,
                                     &(pMultiData->u4_ULongValue)));
}

INT4
FsQosHwCpuRowStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsQosHwCpuRateLimitTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsQosHwCpuRowStatus (pMultiIndex->pIndex[0].u4_ULongValue,
                                       &(pMultiData->i4_SLongValue)));
}

INT4
FsQosHwCpuMinRateSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsQosHwCpuMinRate (pMultiIndex->pIndex[0].u4_ULongValue,
                                     pMultiData->u4_ULongValue));
}

INT4
FsQosHwCpuMaxRateSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsQosHwCpuMaxRate (pMultiIndex->pIndex[0].u4_ULongValue,
                                     pMultiData->u4_ULongValue));
}

INT4
FsQosHwCpuRowStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsQosHwCpuRowStatus (pMultiIndex->pIndex[0].u4_ULongValue,
                                       pMultiData->i4_SLongValue));
}

INT4
FsQosHwCpuMinRateTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                       tRetVal * pMultiData)
{
    return (nmhTestv2FsQosHwCpuMinRate (pu4Error,
                                        pMultiIndex->pIndex[0].u4_ULongValue,
                                        pMultiData->u4_ULongValue));
}

INT4
FsQosHwCpuMaxRateTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                       tRetVal * pMultiData)
{
    return (nmhTestv2FsQosHwCpuMaxRate (pu4Error,
                                        pMultiIndex->pIndex[0].u4_ULongValue,
                                        pMultiData->u4_ULongValue));
}

INT4
FsQosHwCpuRowStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                         tRetVal * pMultiData)
{
    return (nmhTestv2FsQosHwCpuRowStatus (pu4Error,
                                          pMultiIndex->pIndex[0].u4_ULongValue,
                                          pMultiData->i4_SLongValue));
}

INT4
FsQosHwCpuRateLimitTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                             tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsQosHwCpuRateLimitTable (pu4Error,
                                              pSnmpIndexList, pSnmpvarbinds));
}
INT4 GetNextIndexFsQoSPortTable(tSnmpIndex *pFirstMultiIndex,tSnmpIndex * pNextMultiIndex)
{
        if (pFirstMultiIndex == NULL) 
        {
                if (nmhGetFirstIndexFsQoSPortTable(
                        &(pNextMultiIndex->pIndex[0].i4_SLongValue),
                        &(pNextMultiIndex->pIndex[1].i4_SLongValue)) == SNMP_FAILURE)
                {
                        return SNMP_FAILURE;
                }
        }
        else
        {
                if (nmhGetNextIndexFsQoSPortTable(
                        pFirstMultiIndex->pIndex[0].i4_SLongValue,
                        &(pNextMultiIndex->pIndex[0].i4_SLongValue),
                        pFirstMultiIndex->pIndex[1].i4_SLongValue,
                        &(pNextMultiIndex->pIndex[1].i4_SLongValue)) == SNMP_FAILURE)
                {
                        return SNMP_FAILURE;
                }
        }
        
        return SNMP_SUCCESS;
}
INT4 FsQoSPcpSelRowGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
        if (nmhValidateIndexInstanceFsQoSPortTable(
                pMultiIndex->pIndex[0].i4_SLongValue,
                pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
        {
                return SNMP_FAILURE;
        }
        return(nmhGetFsQoSPcpSelRow(
                pMultiIndex->pIndex[0].i4_SLongValue,
                pMultiIndex->pIndex[1].i4_SLongValue,
                &(pMultiData->i4_SLongValue)));
 
}
INT4 FsQoSPcpRowStatusGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
        if (nmhValidateIndexInstanceFsQoSPortTable(
                pMultiIndex->pIndex[0].i4_SLongValue,
                pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
        {
                return SNMP_FAILURE;
        }
        return(nmhGetFsQoSPcpRowStatus(
                pMultiIndex->pIndex[0].i4_SLongValue,
                pMultiIndex->pIndex[1].i4_SLongValue,
                &(pMultiData->i4_SLongValue)));
 
}
INT4 FsQoSPcpSelRowSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
        return (nmhSetFsQoSPcpSelRow(
                pMultiIndex->pIndex[0].i4_SLongValue,
                pMultiIndex->pIndex[1].i4_SLongValue,
                pMultiData->i4_SLongValue));
 
}
 
INT4 FsQoSPcpRowStatusSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
        return (nmhSetFsQoSPcpRowStatus(
                pMultiIndex->pIndex[0].i4_SLongValue,
                pMultiIndex->pIndex[1].i4_SLongValue,
                pMultiData->i4_SLongValue));
 
}
 
INT4 FsQoSPcpSelRowTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
        return (nmhTestv2FsQoSPcpSelRow(pu4Error,
                pMultiIndex->pIndex[0].i4_SLongValue,
                pMultiIndex->pIndex[1].i4_SLongValue,
                pMultiData->i4_SLongValue));
 
}
 
INT4 FsQoSPcpRowStatusTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
        return (nmhTestv2FsQoSPcpRowStatus(pu4Error,
                pMultiIndex->pIndex[0].i4_SLongValue,
                pMultiIndex->pIndex[1].i4_SLongValue,
                pMultiData->i4_SLongValue));
 
}
 
INT4 FsQoSPortTableDep(UINT4 *pu4Error, tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpvarbinds)
{
        return(nmhDepv2FsQoSPortTable(pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

/*******************************************************************************
 *                     VLAN QUEUEING TABLE
*******************************************************************************/                     

INT4 FsQoSVlanQueueingStatusGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	UNUSED_PARAM(pMultiIndex);
	return(nmhGetFsQoSVlanQueueingStatus(&(pMultiData->i4_SLongValue)));
}
INT4 FsQoSVlanQueueingStatusSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	UNUSED_PARAM(pMultiIndex);
	return(nmhSetFsQoSVlanQueueingStatus(pMultiData->i4_SLongValue));
}


INT4 FsQoSVlanQueueingStatusTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	UNUSED_PARAM(pMultiIndex);
	return(nmhTestv2FsQoSVlanQueueingStatus(pu4Error, pMultiData->i4_SLongValue));
}


INT4 FsQoSVlanQueueingStatusDep(UINT4 *pu4Error,tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpvarbinds)
{
	return(nmhDepv2FsQoSVlanQueueingStatus(pu4Error, pSnmpIndexList, pSnmpvarbinds));
}


INT4 GetNextIndexFsQoSVlanMapTable(tSnmpIndex *pFirstMultiIndex,tSnmpIndex * pNextMultiIndex)
{
	if (pFirstMultiIndex == NULL) 
	{
		if (nmhGetFirstIndexFsQoSVlanMapTable(
			&(pNextMultiIndex->pIndex[0].u4_ULongValue)) == SNMP_FAILURE)
		{
 			return SNMP_FAILURE;
		}
	}
	else
	{
		if (nmhGetNextIndexFsQoSVlanMapTable(
			pFirstMultiIndex->pIndex[0].u4_ULongValue,
			&(pNextMultiIndex->pIndex[0].u4_ULongValue)) == SNMP_FAILURE)
		{
			return SNMP_FAILURE;
		}
	}
	
	return SNMP_SUCCESS;
}

INT4 FsQoSVlanMapIfIndexGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsQoSVlanMapTable(
		pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsQoSVlanMapIfIndex(
		pMultiIndex->pIndex[0].u4_ULongValue,
		&(pMultiData->u4_ULongValue)));

}
INT4 FsQoSVlanMapVlanIdGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsQoSVlanMapTable(
		pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsQoSVlanMapVlanId(
		pMultiIndex->pIndex[0].u4_ULongValue,
		&(pMultiData->u4_ULongValue)));

}
INT4 FsQoSVlanMapStatusGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsQoSVlanMapTable(
		pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsQoSVlanMapStatus(
		pMultiIndex->pIndex[0].u4_ULongValue,
		&(pMultiData->i4_SLongValue)));

}
INT4 FsQoSVlanMapIfIndexSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhSetFsQoSVlanMapIfIndex(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiData->u4_ULongValue));

}

INT4 FsQoSVlanMapVlanIdSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhSetFsQoSVlanMapVlanId(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiData->u4_ULongValue));

}

INT4 FsQoSVlanMapStatusSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhSetFsQoSVlanMapStatus(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiData->i4_SLongValue));

}

INT4 FsQoSVlanMapIfIndexTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhTestv2FsQoSVlanMapIfIndex(pu4Error,
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiData->u4_ULongValue));

}

INT4 FsQoSVlanMapVlanIdTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhTestv2FsQoSVlanMapVlanId(pu4Error,
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiData->u4_ULongValue));

}

INT4 FsQoSVlanMapStatusTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhTestv2FsQoSVlanMapStatus(pu4Error,
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiData->i4_SLongValue));

}

INT4 FsQoSVlanMapTableDep(UINT4 *pu4Error, tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpvarbinds)
{
	return(nmhDepv2FsQoSVlanMapTable(pu4Error, pSnmpIndexList, pSnmpvarbinds));
}
