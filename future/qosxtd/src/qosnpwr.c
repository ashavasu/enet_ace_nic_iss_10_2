/* $Id: qosnpwr.c,v 1.45 2017/09/20 11:39:18 siva Exp $ */
/****************************************************************************/
/* Copyright (C) 2007 Aricent Inc . All Rights Reserved                     */
/*                                                                          */
/*  FILE NAME             : qosnpwr.c                                       */
/*  PRINCIPAL AUTHOR      : Aricent                                         */
/*  SUBSYSTEM NAME        : QoS                                             */
/*  MODULE NAME           : QoS-NP-WR                                       */
/*  LANGUAGE              : C                                               */
/*  TARGET ENVIRONMENT    : Linux                                           */
/*  DATE OF FIRST RELEASE :                                                 */
/*  AUTHOR                : Aricent                                         */
/*  FUNCTIONS DEFINED(Applicable for Source files) :                        */
/*  DESCRIPTION           : This File contains the NP Wraper Functions      */
/*                          for the Aricent QoS Module.                     */
/****************************************************************************/
/*                                                                          */
/*  Change History                                                          */
/*  Version               :                                                 */
/*  Date(DD/MM/YYYY)      :                                                 */
/*  Modified by           :                                                 */
/*  Description of change :                                                 */
/****************************************************************************/
#ifndef __QOS_NP_WR_C__
#define __QOS_NP_WR_C__

#include "qosinc.h"

/*****************************************************************************/
/* Function Name      : QoSHwWrInit                                          */
/* Description        : This function is used to init the QoS in the         */
/*                       Hardware                                            */
/*                      if NPAPI Defined it will do the following Actions,   */
/*                       otherwise return SUCCESS.                           */
/*                      1. Fill the Required values form the software.       */
/*                      2. Call the NPAPI Function , retuen the Status.      */
/* Input(s)           : None.                                                */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : QOS_SUCCESS / QOS_FAILURE                            */
/* Called By          : Protocol Main and Lowlevel Routine                   */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QoSHwWrInit (VOID)
{
#ifdef NPAPI_WANTED
    INT4                i4RetStatus = QOS_FAILURE;

    /* Call the NP Function */
    i4RetStatus = QosxQoSHwInit ();

    if (i4RetStatus == FNP_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : ERROR: QoSHwInit () "
                      " Returns FAILURE. \r\n", __FUNCTION__);

        return (QOS_FAILURE);
    }

#else

#endif /* NPAPI_WANTED */

    return (QOS_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSHwWrMapClassToPolicy                              */
/* Description        : This function is used to Map the Policy prameters    */
/*                       for a CLASS in the Hardware                         */
/*                      if NPAPI Defined it will do the following Actions,   */
/*                       otherwise return SUCCESS.                           */
/*                      1. Fill the Required values form the software.       */
/*                      2. Call the NPAPI Function , retuen the Status.      */
/* Input(s)           : pPlyMapNode- Pointer to the tQoSPolicyMapNode        */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : QOS_SUCCESS / QOS_FAILURE                            */
/* Called By          : Protocol Main and Lowlevel Routine                   */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QoSHwWrMapClassToPolicy (tQoSPolicyMapNode * pPlyMapNode,
                         tQoSClassMapNode * pClsMapNode, UINT1 u1Flag)
{
#ifdef NPAPI_WANTED

    tQoSPolicyMapEntry  NewPolicyMapEntry;
    tQoSInProfileActionEntry NewInProActEntry;
    tQoSOutProfileActionEntry NewOutProActEntry;
    tQoSMeterEntry     *pNewMeterEntry = NULL;
    tQoSMeterNode      *pMeterNode = NULL;
    tQoSClassInfoNode  *pClsInfoNode = NULL;
    tQoSFilterInfoNode *pFltInfoNode = NULL;
    tQoSClassMapEntry  *pNewClassMapEntry = NULL;

    INT4                i4RetStatus = FNP_FAILURE;
    UINT4               u4First = QOS_SUCCESS;
    UINT1               u1TmpFlag = 0;

    MEMSET (&(NewInProActEntry), 0, (sizeof (tQoSInProfileActionEntry)));
    MEMSET (&(NewOutProActEntry), 0, (sizeof (tQoSOutProfileActionEntry)));
    MEMSET (&(NewPolicyMapEntry), 0, (sizeof (tQoSPolicyMapEntry)));

    /* Fill the Values for that Entry */
    /* Meter Table */
    if (pPlyMapNode->u4MeterTableId != 0)
    {
        pMeterNode = QoSUtlGetMeterNode (pPlyMapNode->u4MeterTableId);

        if (pMeterNode == NULL)
        {
            return (QOS_FAILURE);
        }

        pNewMeterEntry = MEM_MALLOC ((sizeof (tQoSMeterEntry)), tQoSMeterEntry);
        if (pNewMeterEntry == NULL)
        {
            return (QOS_FAILURE);
        }

        QoSHwWrUtilFillMeterParams (pNewMeterEntry, pMeterNode);

        /* Fill Out profile Action */
        QoSHwWrUtilFillOutProParams (&(NewOutProActEntry), pPlyMapNode);

        /* Fill In profile Action */
        QoSHwWrUtilFillInProParams (&(NewInProActEntry), pPlyMapNode);
    }

    /* Fill Policy Map Table */
    QoSHwWrUtilFillPolicyParams (&(NewPolicyMapEntry), pPlyMapNode);

    /* This is Interface Specific Policy CLASS is NULL */
    if (pPlyMapNode->u4ClassId == 0)
    {
        /* Call the NP Function */
        i4RetStatus = QosxQoSHwMapClassToPolicy (pNewClassMapEntry,
                                                 &(NewPolicyMapEntry),
                                                 &(NewInProActEntry),
                                                 &(NewOutProActEntry),
                                                 pNewMeterEntry, u1Flag);
        pPlyMapNode->i4HwExpMapId = NewPolicyMapEntry.i4HwExpMapId;
        if (pNewMeterEntry != NULL)
        {
            MEM_FREE (pNewMeterEntry);
        }
        if (i4RetStatus == FNP_FAILURE || i4RetStatus == FNP_NOT_SUPPORTED)
        {
            if (i4RetStatus == FNP_NOT_SUPPORTED)
            {
                CLI_SET_ERR (QOS_CLI_ERR_PLY_ACL_STAGE_INVALID);
            }
            QOS_TRC_ARG1 (MGMT_TRC, "In %s : ERROR: QoSHwMapClassToPolicy () "
                          " Returns FAILURE. \r\n", __FUNCTION__);

            return (QOS_FAILURE);
        }
        return (QOS_SUCCESS);
    }                            /* End of NULL CLASS */

    if (pClsMapNode != NULL)
    {
        /* Fill the Values for that Entry */
        pNewClassMapEntry = QoSFiltersForClassMapEntry (pClsMapNode);
        if (pNewClassMapEntry == NULL)
        {
            QOS_TRC_ARG1 (MGMT_TRC, "In %s : ERROR: "
                          "QoSFiltersForClassMapEntry() Returns FAILURE."
                          " \r\n", __FUNCTION__);

            if (pNewMeterEntry != NULL)
            {
                MEM_FREE (pNewMeterEntry);
            }
            return (QOS_FAILURE);
        }

        /* Call the NP Function */
        if (pPlyMapNode != NULL)
        {
            if (pNewClassMapEntry->pPriorityMapPtr != NULL)
            {
                if (pPlyMapNode->u1PHBType == QOS_PLY_PHB_TYPE_DOT1P)
                {
                    gau1DEArray[pPlyMapNode->u4IfIndex]
                        [pNewClassMapEntry->pPriorityMapPtr->u1RegenPri]
                        = (UINT1) pPlyMapNode->i4VlanDE;
                    QoSUtlStoreOperDecodingInfo (pPlyMapNode->u4IfIndex,
                                                 pNewClassMapEntry->
                                                 pPriorityMapPtr->u1RegenPri,
                                                 (UINT1) pPlyMapNode->
                                                 u2DefaultPHB,
                                                 (UINT1) pPlyMapNode->i4VlanDE,
                                                 0, QOS_PCP_PKT_TYPE_DOT1P,
                                                 QOS_PCP_TBL_ACTION_MODIFY);
                }

                if (pPlyMapNode->u1PHBType == QOS_PLY_PHB_TYPE_IP_DSCP)
                {
                    QoSUtlStoreOperDecodingInfo (pPlyMapNode->u4IfIndex,
                                                 pNewClassMapEntry->
                                                 pPriorityMapPtr->u1RegenPri,
                                                 (UINT1) pPlyMapNode->
                                                 u2DefaultPHB, 0, 0,
                                                 QOS_PCP_PKT_TYPE_IP,
                                                 QOS_PCP_TBL_ACTION_MODIFY);
                }
                if (pPlyMapNode->u1PHBType == QOS_PLY_PHB_TYPE_MPLS_EXP)
                {
                    QoSUtlStoreOperDecodingInfo (pPlyMapNode->u4IfIndex,
                                                 pNewClassMapEntry->
                                                 pPriorityMapPtr->u1RegenPri,
                                                 (UINT1) pPlyMapNode->
                                                 u2DefaultPHB, 0, 0,
                                                 QOS_PCP_PKT_TYPE_MPLS,
                                                 QOS_PCP_TBL_ACTION_MODIFY);
                }
            }
        }
        i4RetStatus = QosxQoSHwMapClassToPolicy (pNewClassMapEntry,
                                                 &(NewPolicyMapEntry),
                                                 &(NewInProActEntry),
                                                 &(NewOutProActEntry),
                                                 pNewMeterEntry, u1Flag);

        pPlyMapNode->i4HwExpMapId = NewPolicyMapEntry.i4HwExpMapId;
        QoSUtlClassMapEntryMemFree (pNewClassMapEntry);

        if (i4RetStatus == FNP_FAILURE || i4RetStatus == FNP_NOT_SUPPORTED)
        {
            if (i4RetStatus == FNP_NOT_SUPPORTED)
            {
                CLI_SET_ERR (QOS_CLI_ERR_PLY_ACL_STAGE_INVALID);
            }
            QOS_TRC_ARG1 (MGMT_TRC, "In %s : ERROR: QoSHwMapClassToPolicy "
                          "() Returns FAILURE. \r\n", __FUNCTION__);
            if (pNewMeterEntry != NULL)
            {
                MEM_FREE (pNewMeterEntry);
            }
            return (QOS_FAILURE);
        }
    }
    else
    {
        /* Get all The Class Map for a given Policy */
        pClsInfoNode = QoSUtlGetClassInfoNode (pPlyMapNode->u4ClassId);

        if (pClsInfoNode == NULL)
        {
            if (pNewMeterEntry != NULL)
            {
                MEM_FREE (pNewMeterEntry);
            }
            return (QOS_FAILURE);
        }

        /* Scan the Filter list in the ClassInfo */
        TMO_SLL_Scan (&(pClsInfoNode->SllFltInfo), pFltInfoNode,
                      tQoSFilterInfoNode *)
        {
            /* Add the Policy while maping the First Class Filter Node */
            if ((u1Flag == QOS_PLY_ADD) && (u4First == QOS_SUCCESS))
            {
                u1TmpFlag = QOS_PLY_ADD;
                u4First = QOS_FAILURE;
            }
            else
            {
                u1TmpFlag = QOS_PLY_MAP;
            }

            /* Fill the Values for that Entry */
            pNewClassMapEntry =
                QoSFiltersForClassMapEntry (pFltInfoNode->pClsMapNode);

            if (pNewClassMapEntry == NULL)
            {
                QOS_TRC_ARG1 (MGMT_TRC, "In %s : ERROR: "
                              "QoSFiltersForClassMapEntry() Returns FAILURE."
                              " \r\n", __FUNCTION__);

                if (pNewMeterEntry != NULL)
                {
                    MEM_FREE (pNewMeterEntry);
                }
                return (QOS_FAILURE);
            }

            if (pPlyMapNode != NULL)
            {
                if (pNewClassMapEntry->pPriorityMapPtr != NULL)
                {
                    if (pPlyMapNode->u1PHBType == QOS_PLY_PHB_TYPE_DOT1P)
                    {
                        gau1DEArray[pPlyMapNode->u4IfIndex]
                            [pNewClassMapEntry->pPriorityMapPtr->u1RegenPri]
                            = (UINT1) pPlyMapNode->i4VlanDE;
                        QoSUtlStoreOperDecodingInfo (pPlyMapNode->u4IfIndex,
                                                     pNewClassMapEntry->
                                                     pPriorityMapPtr->
                                                     u1RegenPri,
                                                     pPlyMapNode->u2DefaultPHB,
                                                     (UINT1) pPlyMapNode->
                                                     i4VlanDE, 0,
                                                     QOS_PCP_PKT_TYPE_DOT1P,
                                                     QOS_PCP_TBL_ACTION_MODIFY);
                    }

                    if (pPlyMapNode->u1PHBType == QOS_PLY_PHB_TYPE_IP_DSCP)
                    {
                        QoSUtlStoreOperDecodingInfo (pPlyMapNode->u4IfIndex,
                                                     pNewClassMapEntry->
                                                     pPriorityMapPtr->
                                                     u1RegenPri,
                                                     (UINT1) pPlyMapNode->
                                                     u2DefaultPHB, 0, 0,
                                                     QOS_PCP_PKT_TYPE_IP,
                                                     QOS_PCP_TBL_ACTION_MODIFY);
                    }
                    if (pPlyMapNode->u1PHBType == QOS_PLY_PHB_TYPE_MPLS_EXP)
                    {
                        QoSUtlStoreOperDecodingInfo (pPlyMapNode->u4IfIndex,
                                                     pNewClassMapEntry->
                                                     pPriorityMapPtr->
                                                     u1RegenPri,
                                                     (UINT1) pPlyMapNode->
                                                     u2DefaultPHB, 0, 0,
                                                     QOS_PCP_PKT_TYPE_MPLS,
                                                     QOS_PCP_TBL_ACTION_MODIFY);
                    }

                }
            }
            /* Call the NP Function */
            i4RetStatus = QosxQoSHwMapClassToPolicy (pNewClassMapEntry,
                                                     &(NewPolicyMapEntry),
                                                     &(NewInProActEntry),
                                                     &(NewOutProActEntry),
                                                     pNewMeterEntry, u1TmpFlag);

            pPlyMapNode->i4HwExpMapId = NewPolicyMapEntry.i4HwExpMapId;
            QoSUtlClassMapEntryMemFree (pNewClassMapEntry);
            if (i4RetStatus == FNP_NOT_SUPPORTED)
            {
                CLI_SET_ERR (QOS_CLI_ERR_PLY_ACL_STAGE_INVALID);
                QOS_TRC_ARG1 (MGMT_TRC, "In %s : ERROR: QoSHwMapClassToPolicy "
                              "() Returns FAILURE. \r\n", __FUNCTION__);

                if (pNewMeterEntry != NULL)
                {
                    MEM_FREE (pNewMeterEntry);
                }
                return (QOS_FAILURE);
            }
            else if (i4RetStatus == FNP_FAILURE)
            {
                QOS_TRC_ARG1 (MGMT_TRC, "In %s : ERROR: QoSHwMapClassToPolicy "
                              "() Returns FAILURE. \r\n", __FUNCTION__);

                if (pNewMeterEntry != NULL)
                {
                    MEM_FREE (pNewMeterEntry);
                }
                return (QOS_FAILURE);

            }
        }                        /* End of Scan */
    }
    if (pNewMeterEntry != NULL)
    {
        MEM_FREE (pNewMeterEntry);
    }

#else

    UNUSED_PARAM (pPlyMapNode);
    UNUSED_PARAM (pClsMapNode);
    UNUSED_PARAM (u1Flag);

#endif /* NPAPI_WANTED */

    return (QOS_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSHwWrUpdatePolicyMapForClass                       */
/* Description        : This function is used to update the Policy prameters */
/*                       for a CLASS in the Hardware                         */
/*                      if NPAPI Defined it will do the following Actions,   */
/*                       otherwise return SUCCESS.                           */
/*                      1. Fill the Required values form the software.       */
/*                      2. Call the NPAPI Function , retuen the Status.      */
/* Input(s)           : pPlyMapNode- Pointer to the tQoSPolicyMapNode        */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : QOS_SUCCESS / QOS_FAILURE                            */
/* Called By          : Protocol Main and Lowlevel Routine                   */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QoSHwWrUpdatePolicyMapForClass (tQoSPolicyMapNode * pPlyMapNode, UINT1 u1Flag)
{
#ifdef NPAPI_WANTED

    tQoSPolicyMapEntry  NewPolicyMapEntry;
    tQoSInProfileActionEntry NewInProActEntry;
    tQoSOutProfileActionEntry NewOutProActEntry;
    tQoSMeterEntry     *pNewMeterEntry = NULL;
    tQoSClassMapEntry  *pNewClassMapEntry = NULL;
    tQoSMeterNode      *pMeterNode = NULL;
    tQoSClassInfoNode  *pClsInfoNode = NULL;
    tQoSFilterInfoNode *pFltInfoNode = NULL;
    INT4                i4RetStatus = FNP_FAILURE;

    MEMSET (&(NewInProActEntry), 0, (sizeof (tQoSInProfileActionEntry)));
    MEMSET (&(NewOutProActEntry), 0, (sizeof (tQoSOutProfileActionEntry)));
    MEMSET (&(NewPolicyMapEntry), 0, (sizeof (tQoSPolicyMapEntry)));

    /* Fill the Values for that Entry */
    /* Meter Table */
    if (pPlyMapNode->u4MeterTableId != 0)
    {
        pMeterNode = QoSUtlGetMeterNode (pPlyMapNode->u4MeterTableId);
        if (pMeterNode == NULL)
        {
            return (QOS_FAILURE);
        }

        pNewMeterEntry = MEM_MALLOC ((sizeof (tQoSMeterEntry)), tQoSMeterEntry);
        if (pNewMeterEntry == NULL)
        {
            return (QOS_FAILURE);
        }

        QoSHwWrUtilFillMeterParams (pNewMeterEntry, pMeterNode);

        /* Fill Out profile Action */
        QoSHwWrUtilFillOutProParams (&(NewOutProActEntry), pPlyMapNode);

        /* Fill In profile Action */
        QoSHwWrUtilFillInProParams (&(NewInProActEntry), pPlyMapNode);

    }
    else
    {
        /* Reset the Meter action once meter is removed  from policy map */
        pPlyMapNode->u1OutProActionFlag = 0;
        pPlyMapNode->u1OutProActVlanPrio = 0;
        pPlyMapNode->u2OutProActVlanDE = 0;
        pPlyMapNode->u2OutProActInnerVlanPrio = 0;
        pPlyMapNode->u2OutProActInnerVlanDE = 0;
        pPlyMapNode->u1OutProActDscpOrTos = 0;
        pPlyMapNode->u1OutProActDscpOrTos = 0;
        pPlyMapNode->u1OutProActMplsExp = 0;
        pPlyMapNode->u4OutProActNewClassId = 0;
        pPlyMapNode->u4OutProActTrafficClass = 0;
        pPlyMapNode->u1InProfConfActFlag = 0;
        pPlyMapNode->u1InProfExcActFlag = 0;
        pPlyMapNode->u2InProActConfVlanPrio = 0;
        pPlyMapNode->u2InProActConfVlanDE = 0;
        pPlyMapNode->u2InProActConfInnerVlanPrio = 0;
        pPlyMapNode->u2InProActConfInnerVlanDE = 0;
        pPlyMapNode->u1InProActConfDscpOrToS = 0;
        pPlyMapNode->u1InProActConfDscpOrToS = 0;
        pPlyMapNode->u1InProActConfMplsExp = 0;
        pPlyMapNode->u4InProActConfNewClassId = 0;
        pPlyMapNode->u4InProfileTrafficClass = 0;
        pPlyMapNode->u2InProActExceedVlanPrio = 0;
        pPlyMapNode->u2InProActExceedVlanDE = 0;
        pPlyMapNode->u2InProActExceedInnerVlanPrio = 0;
        pPlyMapNode->u2InProActExceedInnerVlanDE = 0;
        pPlyMapNode->u1InProActExceedDscpOrToS = 0;
        pPlyMapNode->u1InProActExceedDscpOrToS = 0;
        pPlyMapNode->u1InProActConfMplsExp = 0;
        pPlyMapNode->u4InProActExccedNewClassId = 0;
        pPlyMapNode->u4InProActExceedTrafficClass = 0;
    }

    /* Fill Policy Map Table */
    QoSHwWrUtilFillPolicyParams (&(NewPolicyMapEntry), pPlyMapNode);

    /* This is Interface Specific Policy CLASS is NULL */
    if (pPlyMapNode->u4ClassId == 0)
    {
        /* Call the NP Function */
        i4RetStatus = QosxQoSHwUpdatePolicyMapForClass (pNewClassMapEntry,
                                                        &(NewPolicyMapEntry),
                                                        &(NewInProActEntry),
                                                        &(NewOutProActEntry),
                                                        pNewMeterEntry, u1Flag);
        if (pNewMeterEntry != NULL)
        {
            MEM_FREE (pNewMeterEntry);
        }
        if (i4RetStatus == FNP_FAILURE || i4RetStatus == FNP_NOT_SUPPORTED)
        {
            QOS_TRC_ARG1 (MGMT_TRC, "In %s : ERROR: "
                          " QoSHwUpdatePolicyMapForClass ()  Returns FAILURE."
                          " \r\n", __FUNCTION__);

            return (QOS_FAILURE);
        }
        return (QOS_SUCCESS);
    }                            /* End of NULL CLASS */

    /* Get all The Class Map for a given Policy */
    pClsInfoNode = QoSUtlGetClassInfoNode (pPlyMapNode->u4ClassId);

    if (pClsInfoNode == NULL)
    {
        if (pNewMeterEntry != NULL)
        {
            MEM_FREE (pNewMeterEntry);
        }
        return (QOS_FAILURE);
    }

    /* Scan the Filter list in the ClassInfo */
    TMO_SLL_Scan (&(pClsInfoNode->SllFltInfo), pFltInfoNode,
                  tQoSFilterInfoNode *)
    {
        /* Fill the Values for that Entry */
        pNewClassMapEntry =
            QoSFiltersForClassMapEntry (pFltInfoNode->pClsMapNode);

        if (pNewClassMapEntry == NULL)
        {
            QOS_TRC_ARG1 (MGMT_TRC, "In %s : ERROR: "
                          "QoSFiltersForClassMapEntry () Returns FAILURE."
                          " \r\n", __FUNCTION__);
            if (pNewMeterEntry != NULL)
            {
                MEM_FREE (pNewMeterEntry);
            }
            return (QOS_FAILURE);
        }

        /* Call the NP Function */
        i4RetStatus = QosxQoSHwUpdatePolicyMapForClass (pNewClassMapEntry,
                                                        &(NewPolicyMapEntry),
                                                        &(NewInProActEntry),
                                                        &(NewOutProActEntry),
                                                        pNewMeterEntry, u1Flag);

        QoSUtlClassMapEntryMemFree (pNewClassMapEntry);

        if (i4RetStatus == FNP_FAILURE || i4RetStatus == FNP_NOT_SUPPORTED)
        {
            QOS_TRC_ARG1 (MGMT_TRC, "In %s : ERROR: "
                          "QoSHwUpdatePolicyMapForClass() Returns FAILURE. "
                          "\r\n", __FUNCTION__);

            if (pNewMeterEntry != NULL)
            {
                MEM_FREE (pNewMeterEntry);
            }
            return (QOS_FAILURE);
        }

    }

    if (pNewMeterEntry != NULL)
    {
        MEM_FREE (pNewMeterEntry);
    }

#else

    UNUSED_PARAM (pPlyMapNode);
    UNUSED_PARAM (u1Flag);

#endif /* NPAPI_WANTED */

    return (QOS_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSHwWrUnmapClassFromPolicy                          */
/* Description        : This function is used to unmap the Class from the    */
/*                       policy in the Hardware                              */
/*                      if NPAPI Defined it will do the following Actions,   */
/*                       otherwise return SUCCESS.                           */
/*                      1. Fill the Required values form the software.       */
/*                      2. Call the NPAPI Function , retuen the Status.      */
/* Input(s)           : pPlyMapNode- Pointer to the tQoSPolicyMapNode        */
/*                    : u1Flag     -  QOS_PLY_UNMAP / QOS_PLY_DEL            */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : QOS_SUCCESS / QOS_FAILURE                            */
/* Called By          : Protocol Main and Lowlevel Routine                   */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QoSHwWrUnmapClassFromPolicy (tQoSPolicyMapNode * pPlyMapNode,
                             tQoSClassMapNode * pClsMapNode, UINT1 u1Flag)
{
#ifdef NPAPI_WANTED
    tQoSPolicyMapEntry  UnmapPolicyMapEntry;
    tQoSMeterNode      *pMeterNode = NULL;
    tQoSClassInfoNode  *pClsInfoNode = NULL;
    tQoSFilterInfoNode *pFltInfoNode = NULL;
    tQoSClassMapEntry  *pUnmapClassMapEntry = NULL;
    tQoSMeterEntry     *pUnmapMeterEntry = NULL;
    INT4                i4RetStatus = FNP_FAILURE;
    UINT4               u4Count = 0;
    UINT1               u1TmpFlag = 0;

    /* Init Entry */
    MEMSET (&(UnmapPolicyMapEntry), 0, (sizeof (tQoSPolicyMapEntry)));

    /* Fill Meter Table Entry */
    /* Meter Table */
    if (pPlyMapNode->u4MeterTableId != 0)
    {
        pMeterNode = QoSUtlGetMeterNode (pPlyMapNode->u4MeterTableId);

        if (pMeterNode == NULL)
        {
            return (QOS_FAILURE);
        }

        pUnmapMeterEntry =
            MEM_MALLOC ((sizeof (tQoSMeterEntry)), tQoSMeterEntry);
        if (pUnmapMeterEntry == NULL)
        {
            return (QOS_FAILURE);
        }

        QoSHwWrUtilFillMeterParams (pUnmapMeterEntry, pMeterNode);
    }

    /* Policy Table Entry */
    QoSHwWrUtilFillPolicyParams (&(UnmapPolicyMapEntry), pPlyMapNode);

    /* This is Interface Specific Policy CLASS is NULL */
    if (pPlyMapNode->u4ClassId == 0)
    {
        /* Call the NP Function */
        i4RetStatus = QosxQoSHwUnmapClassFromPolicy (pUnmapClassMapEntry,
                                                     &(UnmapPolicyMapEntry),
                                                     pUnmapMeterEntry, u1Flag);
        if (pUnmapMeterEntry != NULL)
        {
            MEM_FREE (pUnmapMeterEntry);
        }

        if (i4RetStatus == FNP_FAILURE)
        {
            QOS_TRC_ARG1 (MGMT_TRC, "In %s : ERROR: QoSHwUnmapClassFromPolicy "
                          "() Returns FAILURE. \r\n", __FUNCTION__);

            return (QOS_FAILURE);
        }

        return (QOS_SUCCESS);
    }                            /* End of NULL CLASS */

    /* Remove the Specific Entry */
    if (pClsMapNode != NULL)
    {
        /* Fill the Values for that Entry */
        pUnmapClassMapEntry = QoSFiltersForClassMapEntry (pClsMapNode);
        if (pUnmapClassMapEntry == NULL)
        {
            QOS_TRC_ARG1 (MGMT_TRC, "In %s : ERROR: QoSFiltersForClassMapEntry"
                          " () Returns FAILURE. \r\n", __FUNCTION__);
            if (pUnmapMeterEntry != NULL)
            {
                MEM_FREE (pUnmapMeterEntry);
            }
            return (QOS_FAILURE);
        }

        /* Call the NP Function */
        i4RetStatus = QosxQoSHwUnmapClassFromPolicy (pUnmapClassMapEntry,
                                                     &(UnmapPolicyMapEntry),
                                                     pUnmapMeterEntry, u1Flag);

        QoSUtlClassMapEntryMemFree (pUnmapClassMapEntry);

        if (i4RetStatus == FNP_FAILURE)
        {
            QOS_TRC_ARG1 (MGMT_TRC, "In %s : ERROR: QoSHwUnmapClassFromPolicy "
                          "() Returns FAILURE. \r\n", __FUNCTION__);

            if (pUnmapMeterEntry != NULL)
            {
                MEM_FREE (pUnmapMeterEntry);
            }
            return (QOS_FAILURE);
        }

    }
    else
    {
        /* Get all The Class Map for a given Policy */
        pClsInfoNode = QoSUtlGetClassInfoNode (pPlyMapNode->u4ClassId);

        if (pClsInfoNode == NULL)
        {
            QOS_TRC_ARG1 (MGMT_TRC, "In %s : ERROR: QoSUtlGetClassInfoNode "
                          "() Returns FAILURE. \r\n", __FUNCTION__);
            if (pUnmapMeterEntry != NULL)
            {
                MEM_FREE (pUnmapMeterEntry);
            }
            return (QOS_FAILURE);
        }

        /* Scan the Filter list in the ClassInfo */
        /*  get the Count of Node in the SllFltInfo. */
        u4Count = TMO_SLL_Count (&(pClsInfoNode->SllFltInfo));

        TMO_SLL_Scan (&(pClsInfoNode->SllFltInfo), pFltInfoNode,
                      tQoSFilterInfoNode *)
        {
            /* Dlelet the Policy while unmaping the Last Class Filter Node */
            if ((u1Flag == QOS_PLY_DEL) && (u4Count == 1))
            {
                u1TmpFlag = QOS_PLY_DEL;
            }
            else
            {
                u1TmpFlag = QOS_PLY_UNMAP;
            }

            /* Fill the Values for that Entry */
            pUnmapClassMapEntry =
                QoSFiltersForClassMapEntry (pFltInfoNode->pClsMapNode);

            if (pUnmapClassMapEntry == NULL)
            {
                QOS_TRC_ARG1 (MGMT_TRC, "In %s : ERROR: "
                              "QoSFiltersForClassMapEntry () Returns FAILURE."
                              " \r\n", __FUNCTION__);
                if (pUnmapMeterEntry != NULL)
                {
                    MEM_FREE (pUnmapMeterEntry);
                }
                return (QOS_FAILURE);
            }

            /* Call the NP Function */
            i4RetStatus = QosxQoSHwUnmapClassFromPolicy (pUnmapClassMapEntry,
                                                         &(UnmapPolicyMapEntry),
                                                         pUnmapMeterEntry,
                                                         u1TmpFlag);

            QoSUtlClassMapEntryMemFree (pUnmapClassMapEntry);

            if (i4RetStatus == FNP_FAILURE)
            {
                QOS_TRC_ARG1 (MGMT_TRC, "In %s : ERROR: "
                              "QoSHwUnmapClassFromPolicy "
                              "() Returns FAILURE. \r\n", __FUNCTION__);

                if (pUnmapMeterEntry != NULL)
                {
                    MEM_FREE (pUnmapMeterEntry);
                }
                return (QOS_FAILURE);
            }

            u4Count = u4Count - 1;
        }                        /* End of Scan */

    }                            /* End of else */
    if (pUnmapMeterEntry != NULL)
    {
        MEM_FREE (pUnmapMeterEntry);
    }
#else

    UNUSED_PARAM (pPlyMapNode);
    UNUSED_PARAM (pClsMapNode);
    UNUSED_PARAM (u1Flag);

#endif /* NPAPI_WANTED */

    return (QOS_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSHwWrDeleteClassMapEntry                           */
/* Description        : This function is used to Delete a Class Map Entry's  */
/*                       Filters form the HardWare.                          */
/*                      if NPAPI Defined it will do the following Actions,   */
/*                       otherwise return SUCCESS.                           */
/*                      1. Fill the Required values form the software.       */
/*                      2. Call the NPAPI Function , retuen the Status.      */
/* Input(s)           : pClsMapNode- Pointer to the tQoSClassMapNode         */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : QOS_SUCCESS / QOS_FAILURE                            */
/* Called By          : Protocol Main and Lowlevel Routine                   */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QoSHwWrDeleteClassMapEntry (tQoSClassMapNode * pClsMapNode)
{
#ifdef NPAPI_WANTED
    tQoSClassMapEntry  *pDelQoSClassMapEntry = NULL;
    INT4                i4RetStatus = FNP_FAILURE;

    /* Fill the Values for that Entry */
    pDelQoSClassMapEntry = QoSFiltersForClassMapEntry (pClsMapNode);
    if (pDelQoSClassMapEntry == NULL)
    {
        return (QOS_FAILURE);
    }

    /* Call the NP Function */
    i4RetStatus = QosxQoSHwDeleteClassMapEntry (pDelQoSClassMapEntry);

    QoSUtlClassMapEntryMemFree (pDelQoSClassMapEntry);

    if (i4RetStatus == FNP_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : ERROR: QoSHwDeleteClassMapEntry () "
                      " Returns FAILURE. \r\n", __FUNCTION__);
        return (QOS_FAILURE);
    }

#else

    UNUSED_PARAM (pClsMapNode);

#endif /* NPAPI_WANTED */

    return (QOS_SUCCESS);
}

INT4
QoSHwWrUtilDeleteAllClsMapEntry (UINT4 u4CLASS)
{
#ifdef NPAPI_WANTED
    tQoSClassMapEntry  *pDelClsMapEntry = NULL;
    tQoSFilterInfoNode *pFltInfoNode = NULL;
    tQoSClassInfoNode  *pClsInfoNode = NULL;
    tQoSClass2PriorityNode *pCls2PriMapNode = NULL;
    INT4                i4RetStatus = FNP_FAILURE;

    /* Get all The Class Map for a given CLASS */
    pClsInfoNode = QoSUtlGetClassInfoNode (u4CLASS);
    if (pClsInfoNode == NULL)
    {
        return (QOS_FAILURE);
    }

    TMO_SLL_Scan (&(pClsInfoNode->SllFltInfo), pFltInfoNode,
                  tQoSFilterInfoNode *)
    {
        /* Fill the Values for that Entry */
        pDelClsMapEntry =
            QoSFiltersForClassMapEntry (pFltInfoNode->pClsMapNode);

        if (pDelClsMapEntry == NULL)
        {
            QOS_TRC_ARG1 (MGMT_TRC, "In %s : ERROR: "
                          "QoSFiltersForClassMapEntry () Returns FAILURE."
                          " \r\n", __FUNCTION__);
            return (QOS_FAILURE);
        }

        /* Call the NP Function */
        i4RetStatus = QosxQoSHwDeleteClassMapEntry (pDelClsMapEntry);

        QoSUtlClassMapEntryMemFree (pDelClsMapEntry);

        if (i4RetStatus == FNP_FAILURE)
        {
            QOS_TRC_ARG1 (MGMT_TRC, "In %s : ERROR: QoSHwDeleteClassMapEntry "
                          "()  Returns FAILURE. \r\n", __FUNCTION__);
            return (QOS_FAILURE);
        }
        pCls2PriMapNode = QoSUtlGetClass2PriorityNode (u4CLASS);
        if ((pCls2PriMapNode != NULL) && (pCls2PriMapNode->u1Status == ACTIVE))
        {
            i4RetStatus =
                QoSHwWrMapClassToIntPriority (pFltInfoNode->pClsMapNode,
                                              pCls2PriMapNode->u1RegenPri,
                                              QOS_NP_ADD);
            if (i4RetStatus == QOS_FAILURE)
            {
                QOS_TRC_ARG1 (MGMT_TRC,
                              "In %s :  QoSHwWrMapClassToIntPriority "
                              " () Returns FAILURE. \r\n", __FUNCTION__);

                return (QOS_FAILURE);
            }
            else if (i4RetStatus == QOS_NP_NOT_SUPPORTED)
            {
                QOS_TRC_ARG1 (MGMT_TRC,
                              "In %s :  QoSHwWrMapClassToIntPriority "
                              " () Returns NOT SUPPORTED. \r\n", __FUNCTION__);

                return (QOS_NP_NOT_SUPPORTED);
            }
        }

    }                            /* End of Scan */
#else

    UNUSED_PARAM (u4CLASS);
#endif
    return (QOS_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSHwWrMeterCreate                                   */
/* Description        : This function is used to Create Meter in the         */
/*                       Hardware                                            */
/*                      if NPAPI Defined it will do the following Actions,   */
/*                       otherwise return SUCCESS.                           */
/*                      1. Fill the Required values form the software.       */
/*                      2. Call the NPAPI Function , retuen the Status.      */
/* Input(s)           : pMeterNode - Pointer to the tQoSMeterNode            */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : QOS_SUCCESS / QOS_FAILURE                            */
/* Called By          : Protocol Main and Lowlevel Routine                   */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QoSHwWrMeterCreate (tQoSMeterNode * pMeterNode)
{
#ifdef NPAPI_WANTED
    INT4                i4RetStatus = FNP_FAILURE;
    tQoSMeterEntry      NewQoSMeterEntry;

    if (pMeterNode == NULL)
    {
        return (QOS_FAILURE);
    }

    /* Init Entry */
    MEMSET (&(NewQoSMeterEntry), 0, (sizeof (tQoSMeterEntry)));

    /* Fill Params */
    QoSHwWrUtilFillMeterParams (&(NewQoSMeterEntry), pMeterNode);

    /* Call the NP Function */
    i4RetStatus = QosxQoSHwMeterCreate (&(NewQoSMeterEntry));

    if (i4RetStatus == FNP_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : ERROR: QoSHwMeterCreate () "
                      " Returns FAILURE. \r\n", __FUNCTION__);

        return (QOS_FAILURE);
    }

#else

    UNUSED_PARAM (pMeterNode);

#endif /* NPAPI_WANTED */

    return (QOS_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSHwWrMeterDelete                                   */
/* Description        : This function is used to Delete Meter in the         */
/*                       Hardware                                            */
/*                      if NPAPI Defined it will do the following Actions,   */
/*                       otherwise return SUCCESS.                           */
/*                      1. Fill the Required values form the software.       */
/*                      2. Call the NPAPI Function , retuen the Status.      */
/* Input(s)           : pMeterNode - Pointer to the tQoSMeterNode            */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : QOS_SUCCESS / QOS_FAILURE                            */
/* Called By          : Protocol Main and Lowlevel Routine                   */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QoSHwWrMeterDelete (tQoSMeterNode * pMeterNode)
{
#ifdef NPAPI_WANTED
    INT4                i4MeterId = 0;
    INT4                i4RetStatus = FNP_FAILURE;

    if (pMeterNode == NULL)
    {
        return (QOS_FAILURE);
    }

    /* Fill the Values for that Entry */
    i4MeterId = (INT4) pMeterNode->u4Id;

    /* Call the NP Function */
    i4RetStatus = QosxQoSHwMeterDelete (i4MeterId);

    if (i4RetStatus == FNP_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : ERROR: QoSHwMeterDelete () "
                      " Returns FAILURE. \r\n", __FUNCTION__);

        return (QOS_FAILURE);
    }

#else

    UNUSED_PARAM (pMeterNode);

#endif /* NPAPI_WANTED */

    return (QOS_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSHwWrMeterStatsUpdate                              */
/* Description        : This function is used to Create Meter Stats in the   */
/*                       Hardware                                            */
/*                      if NPAPI Defined it will do the following Actions,   */
/*                       otherwise return SUCCESS.                           */
/*                      1. Fill the Required values form the software.       */
/*                      2. Call the NPAPI Function , retuen the Status.      */
/* Input(s)           : pMeterNode - Pointer to the tQoSMeterNode            */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : QOS_SUCCESS / QOS_FAILURE                            */
/* Called By          : Protocol Main and Lowlevel Routine                   */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QoSHwWrMeterStatsUpdate (UINT4 u4HwFilterId, tQoSMeterNode * pMeterNode)
{
#ifdef NPAPI_WANTED
    INT4                i4RetStatus = FNP_FAILURE;
    tQoSMeterEntry      NewQoSMeterEntry;

    if (pMeterNode == NULL)
    {
        return (QOS_FAILURE);
    }

    /* Init Entry */
    MEMSET (&(NewQoSMeterEntry), 0, (sizeof (tQoSMeterEntry)));

    /* Fill Params */
    QoSHwWrUtilFillMeterStatsParams (&(NewQoSMeterEntry), pMeterNode);

    /* Call the NP Function */
    i4RetStatus = QosxQoSHwMeterStatsUpdate (u4HwFilterId, &(NewQoSMeterEntry));

    if (i4RetStatus == FNP_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : ERROR: QoSHwMeterCreate () "
                      " Returns FAILURE. \r\n", __FUNCTION__);

        return (QOS_FAILURE);
    }

#else
    UNUSED_PARAM (u4HwFilterId);
    UNUSED_PARAM (pMeterNode);
#endif /* NPAPI_WANTED */

    return (QOS_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSHwWrMeterStatsDelete                              */
/* Description        : This function is used to Delete Meter Stats in the   */
/*                       Hardware                                            */
/*                      if NPAPI Defined it will do the following Actions,   */
/*                       otherwise return SUCCESS.                           */
/*                      1. Fill the Required values form the software.       */
/*                      2. Call the NPAPI Function , return the Status.      */
/* Input(s)           : pMeterNode - Pointer to the tQoSMeterNode            */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : QOS_SUCCESS / QOS_FAILURE                            */
/* Called By          : Protocol Main and Lowlevel Routine                   */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QoSHwWrMeterStatsClear (tQoSMeterNode * pMeterNode)
{
#ifdef NPAPI_WANTED
    INT4                i4MeterId = 0;
    INT4                i4RetStatus = FNP_FAILURE;

    if (pMeterNode == NULL)
    {
        return (QOS_FAILURE);
    }

    /* Fill the Values for that Entry */
    i4MeterId = (INT4) pMeterNode->u4Id;

    /* Call the NP Function */
    i4RetStatus = QosxQoSHwMeterStatsClear (i4MeterId);

    if (i4RetStatus == FNP_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : ERROR: QoSHwMeterDelete () "
                      " Returns FAILURE. \r\n", __FUNCTION__);

        return (QOS_FAILURE);
    }

#else

    UNUSED_PARAM (pMeterNode);

#endif /* NPAPI_WANTED */

    return (QOS_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSHwWrSchedulerAdd                                  */
/* Description        : This function is used to Add Scheduler in the        */
/*                       Hardware                                            */
/*                      if NPAPI Defined it will do the following Actions,   */
/*                       otherwise return SUCCESS.                           */
/*                      1. Fill the Required values form the software.       */
/*                      2. Call the NPAPI Function , retuen the Status.      */
/* Input(s)           : i4IfIndex  - Interface Index                         */
/*                    : u4SchedId  - Scheduler Id                            */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : QOS_SUCCESS / QOS_FAILURE                            */
/* Called By          : Protocol Main and Lowlevel Routine                   */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QoSHwWrSchedulerAdd (INT4 i4IfIndex, UINT4 u4SchedId)
{
#ifdef NPAPI_WANTED
    INT4                i4RetStatus = FNP_FAILURE;
    tQoSSchedulerEntry *pSchedEntry = NULL;
    tQoSSchedNode      *pSchedNode = NULL;

    /* Fill the tQoSSchedulerEntry values */
    pSchedEntry = QoSHwWrUtilFillSchedEntry (i4IfIndex, u4SchedId);
    if (pSchedEntry == NULL)
    {
        return (QOS_FAILURE);
    }

    /* Call the NP Function */
    i4RetStatus = (INT4) QosxQoSHwSchedulerAdd (pSchedEntry);

    if (pSchedEntry->u4QosSchedHwId != 0)
    {
        pSchedNode = QoSUtlGetSchedNode (i4IfIndex, u4SchedId);
        if (pSchedNode != NULL)
        {
            /* Fill in the Scheduler HW ID */
            pSchedNode->u4SchedulerHwId = pSchedEntry->u4QosSchedHwId;
#ifdef L2RED_WANTED
            QosRedSyncSchedHwId (i4IfIndex, u4SchedId,
                                 pSchedNode->u4SchedulerHwId);
#endif
        }
    }
    QoSHwWrUtilFreeSchedEntry (pSchedEntry);

    if (i4RetStatus == FNP_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : ERROR: QoSHwSchedulerAdd () "
                      " Returns FAILURE. \r\n", __FUNCTION__);

        return (QOS_FAILURE);
    }
    else if (i4RetStatus == FNP_NOT_SUPPORTED)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : ERROR: QoSHwSchedulerAdd () "
                      " Returns NOT_SUPPORTED. \r\n", __FUNCTION__);
        return QOS_NP_NOT_SUPPORTED;
    }
#else

    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (u4SchedId);

#endif /* NPAPI_WANTED */

    return (QOS_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSHwWrSchedulerUpdateParams                         */
/* Description        : This function is used to Update Scheduler in the     */
/*                       Hardware                                            */
/*                      if NPAPI Defined it will do the following Actions,   */
/*                       otherwise return SUCCESS.                           */
/*                      1. Fill the Required values form the software.       */
/*                      2. Call the NPAPI Function , retuen the Status.      */
/* Input(s)           : i4IfIndex  - Interface Index                         */
/*                    : u4SchedId  - Scheduler Id                            */
/*                    : u1Flag     - Indicate the Updated Param.             */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : QOS_SUCCESS / QOS_FAILURE                            */
/* Called By          : Protocol Main and Lowlevel Routine                   */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QoSHwWrSchedulerUpdateParams (INT4 i4IfIndex, UINT4 u4SchedId, UINT1 u1Flag)
{
#ifdef NPAPI_WANTED
    tQoSSchedulerEntry *pSchedEntry = NULL;
    INT4                i4RetStatus = FNP_FAILURE;

    /* Fill the tQoSSchedulerEntry values */
    pSchedEntry = QoSHwWrUtilFillSchedEntry (i4IfIndex, u4SchedId);
    if (pSchedEntry == NULL)
    {
        return (QOS_FAILURE);
    }

    pSchedEntry->u1Flag = u1Flag;

    /* Call the NP Function */
    i4RetStatus = QosxQoSHwSchedulerUpdateParams (pSchedEntry);

    QoSHwWrUtilFreeSchedEntry (pSchedEntry);

    if (i4RetStatus == FNP_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : ERROR: QoSHwSchedulerUpdateParams "
                      "() Returns FAILURE. \r\n", __FUNCTION__);

        return (QOS_FAILURE);
    }

#else

    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (u4SchedId);
    UNUSED_PARAM (u1Flag);

#endif /* NPAPI_WANTED */

    return (QOS_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSHwWrSchedulerDelete                               */
/* Description        : This function is used to Delete Scheduler in the     */
/*                       Hardware                                            */
/*                      if NPAPI Defined it will do the following Actions,   */
/*                       otherwise return SUCCESS.                           */
/*                      1. Fill the Required values form the software.       */
/*                      2. Call the NPAPI Function , retuen the Status.      */
/* Input(s)           : i4IfIndex  - Interface Index                         */
/*                    : u4SchedId  - Scheduler Id                            */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : QOS_SUCCESS / QOS_FAILURE                            */
/* Called By          : Protocol Main and Lowlevel Routine                   */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QoSHwWrSchedulerDelete (INT4 i4IfIndex, UINT4 u4SchedId)
{
#ifdef NPAPI_WANTED
    INT4                i4RetStatus = FNP_FAILURE;
    tQoSSchedNode      *pSchedNode = NULL;

    pSchedNode = QoSUtlGetSchedNode (i4IfIndex, u4SchedId);
    if (pSchedNode == NULL)
    {
        return QOS_FAILURE;
    }
    i4RetStatus =
        QosxQoSHwSchedulerDelete (i4IfIndex, pSchedNode->u4SchedulerHwId);
    if (i4RetStatus == FNP_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : ERROR: QoSHwSchedulerDelete () "
                      " Returns FAILURE. \r\n", __FUNCTION__);
        return (QOS_FAILURE);
    }
    else if (i4RetStatus == FNP_NOT_SUPPORTED)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : ERROR: QoSHwSchedulerDelete () "
                      " Returns NOT SUPPORTED. \r\n", __FUNCTION__);
        return (QOS_NP_NOT_SUPPORTED);
    }
    pSchedNode->u4SchedulerHwId = 0;

#ifdef L2RED_WANTED
    QosRedSyncSchedHwId (i4IfIndex, u4SchedId, pSchedNode->u4SchedulerHwId);
#endif
#else

    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (u4SchedId);

#endif /* NPAPI_WANTED */
    return (QOS_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSHwWrSubscriberQueueCreate                         */
/* Description        : This function is used to create sucscriber queues    */
/*                      in hardware                                          */
/* Input(s)           : i4IfIndex  - Interface Index                         */
/*                    : u4QId      - Scheduler Id                            */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : QOS_SUCCESS / QOS_FAILURE                            */
/* Called By          : Protocol Main and Lowlevel Routine                   */
/* Calling Function   :                                                      */
/*****************************************************************************/

INT4
QoSHwWrSubscriberQueueCreate (INT4 i4IfIndex, UINT4 u4QId)
{
#ifdef NPAPI_WANTED
    tQoSQEntry         *pQEntry = NULL;
    tQoSQtypeEntry     *pQTypeEntry = NULL;
    tQoSREDCfgEntry    *apRDCfgEntry[6] =
        { NULL, NULL, NULL, NULL, NULL, NULL };
    INT4                i4RetStatus = FNP_FAILURE;
    tQoSQNode          *pQNode = NULL;
    tQoSSchedNode      *pSchedNode = NULL;

    /* Fill the tQoSSchedulerEntry values */
    i4RetStatus = QoSHwWrUtilFillQParams (i4IfIndex, u4QId, &pQEntry,
                                          &pQTypeEntry, (&(apRDCfgEntry[0])));
    if (i4RetStatus == QOS_FAILURE)
    {
        return (QOS_FAILURE);
    }

    i4RetStatus = QosxQoSHwSetVlanQueuingStatus (i4IfIndex, pQEntry);
    if (pQEntry->i4QosQueueHwId != 0)
    {
        pQNode = QoSUtlGetQNode (i4IfIndex, u4QId);
        if (pQNode != NULL)
        {
            /* Fill the Hw ID for the new Queue */
            pQNode->u4QueueHwId = pQEntry->i4QosQueueHwId;
            pQNode->u4SchedulerId = pQEntry->i4QosSchedulerId;

            pSchedNode =
                QoSUtlGetSchedNode (i4IfIndex, pQEntry->i4QosSchedulerId);
            if (pSchedNode != NULL)
            {
                /* Fill in the Scheduler HW ID and the number of
                 * children associated to the scheduler in the
                 * scheduler structure */
                pSchedNode->u4SchedulerHwId =
                    pQEntry->pSchedPtr->u4QosSchedHwId;
                pSchedNode->u4SchedChildren =
                    pQEntry->pSchedPtr->u4QosSchedChildren;
            }
        }
    }

    QoSHwWrUtilFreeQParams (pQEntry, pQTypeEntry, apRDCfgEntry);

    if (i4RetStatus == FNP_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : ERROR: QoSHwSetVlanQueuingStatus () "
                      " Returns FAILURE. \r\n", __FUNCTION__);
        return (QOS_FAILURE);
    }

    else if (i4RetStatus == FNP_NOT_SUPPORTED)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : ERROR: QoSHwSetVlanQueuingStatus () "
                      " Returns NOT_SUPPORTED. \r\n", __FUNCTION__);
        return QOS_NP_NOT_SUPPORTED;
    }

#else
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (u4QId);

#endif /* NPAPI_WANTED */
    return (QOS_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSHwWrQueueCreate                                   */
/* Description        : This function is used to Create Queue in the hardware*/
/*                      if NPAPI Defined it will do the following Actions,   */
/*                       otherwise return SUCCESS.                           */
/*                      1. Fill the Required values form the software.       */
/*                      2. Call the NPAPI Function , retuen the Status.      */
/* Input(s)           : i4IfIndex  - Interface Index                         */
/*                    : u4SchedId  - Scheduler Id                            */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : QOS_SUCCESS / QOS_FAILURE                            */
/* Called By          : Protocol Main and Lowlevel Routine                   */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QoSHwWrQueueCreate (INT4 i4IfIndex, UINT4 u4QId)
{
#ifdef NPAPI_WANTED
    tQoSQEntry         *pQEntry = NULL;
    tQoSQtypeEntry     *pQTypeEntry = NULL;
    tQoSREDCfgEntry    *apRDCfgEntry[6] =
        { NULL, NULL, NULL, NULL, NULL, NULL };
    INT4                i4RetStatus = FNP_FAILURE;
    INT2                i2HL = 0;
    tQoSQNode          *pQNode = NULL;
    tQoSSchedNode      *pSchedNode = NULL;

    /* Fill the tQoSSchedulerEntry values */
    i4RetStatus = QoSHwWrUtilFillQParams (i4IfIndex, u4QId, &pQEntry,
                                          &pQTypeEntry, (&(apRDCfgEntry[0])));
    if (i4RetStatus == QOS_FAILURE)
    {
        return (QOS_FAILURE);
    }

    i2HL = pQEntry->pSchedPtr->u1HL;

    i4RetStatus = QosxQoSHwQueueCreate (i4IfIndex, u4QId, pQEntry, pQTypeEntry,
                                        apRDCfgEntry, i2HL);
    if (pQEntry->i4QosQueueHwId != 0)
    {
        pQNode = QoSUtlGetQNode (i4IfIndex, u4QId);
        if (pQNode != NULL)
        {
            /* Fill the Hw ID for the new Queue */
            pQNode->u4QueueHwId = pQEntry->i4QosQueueHwId;
            pQNode->u4SchedulerId = pQEntry->i4QosSchedulerId;

#ifdef L2RED_WANTED
            QosRedSyncQueueHwId (i4IfIndex, u4QId, pQNode->u4QueueHwId);
#endif
            pSchedNode =
                QoSUtlGetSchedNode (i4IfIndex, pQEntry->i4QosSchedulerId);
            if (pSchedNode != NULL)
            {
                /* Fill in the Scheduler HW ID and the number of
                 * children associated to the scheduler in the
                 * scheduler structure */
                pSchedNode->u4SchedulerHwId =
                    pQEntry->pSchedPtr->u4QosSchedHwId;
                pSchedNode->u4SchedChildren =
                    pQEntry->pSchedPtr->u4QosSchedChildren;
            }
        }
    }
    QoSHwWrUtilFreeQParams (pQEntry, pQTypeEntry, apRDCfgEntry);

    if (i4RetStatus == FNP_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : ERROR: QoSHwQueueCreate () "
                      " Returns FAILURE. \r\n", __FUNCTION__);
        return (QOS_FAILURE);
    }

    else if (i4RetStatus == FNP_NOT_SUPPORTED)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : ERROR: QoSHwQueueCreate () "
                      " Returns NOT_SUPPORTED. \r\n", __FUNCTION__);
        return QOS_NP_NOT_SUPPORTED;
    }

#else
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (u4QId);

#endif /* NPAPI_WANTED */

    return (QOS_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSHwWrQueueDelete                                   */
/* Description        : This function is used to Delete Queue in the hardware*/
/*                      if NPAPI Defined it will do the following Actions,   */
/*                       otherwise return SUCCESS.                           */
/*                      1. Fill the Required values form the software.       */
/*                      2. Call the NPAPI Function , retuen the Status.      */
/* Input(s)           : i4IfIndex  - Interface Index                         */
/*                    : u4SchedId  - Scheduler Id                            */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : QOS_SUCCESS / QOS_FAILURE                            */
/* Called By          : Protocol Main and Lowlevel Routine                   */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QoSHwWrQueueDelete (INT4 i4IfIndex, UINT4 u4QId)
{
#ifdef NPAPI_WANTED
    INT4                i4RetStatus = FNP_FAILURE;
    tQoSQNode          *pQNode = NULL;
    tQoSSchedNode      *pSchedNode = NULL;

    pQNode = QoSUtlGetQNode (i4IfIndex, u4QId);
    if (pQNode == NULL)
    {
        return (QOS_FAILURE);
    }
    pSchedNode = QoSUtlGetSchedNode (i4IfIndex, pQNode->u4SchedulerId);
    if (pSchedNode == NULL)
    {
        return QOS_FAILURE;
    }
#ifdef BCM56840_WANTED
    /* First detach the COSQ from its parent scheduler then
     * delete the COSQ itself */
    if (pSchedNode->u1HierarchyLevel > QOS_S1_SCHEDULER)
    {
        i4RetStatus = QosxQoSHwSchedulerHierarchyMap (i4IfIndex, pSchedNode->
                                                      u4SchedulerHwId, 0, 0, 0,
                                                      pQNode->u4QueueHwId,
                                                      (INT2) (pSchedNode->
                                                              u1HierarchyLevel),
                                                      QOS_HIERARCHY_DEL);

        if (i4RetStatus == FNP_SUCCESS)
        {
            /* If the detach succeeds, the COSQ was indeed associated with 
             * the scheduler and is now successfully removed. The return
             * failure is intentionally not handled.
             */
            pSchedNode->u4SchedChildren -= 1;
        }
    }
#endif
    i4RetStatus = QosxQoSHwQueueDelete (i4IfIndex, pQNode->u4QueueHwId);
    if (i4RetStatus == FNP_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : ERROR: QoSHwQueueDelete () "
                      " Returns FAILURE. \r\n", __FUNCTION__);
        return (QOS_FAILURE);
    }
    pQNode->u4QueueHwId = 0;
#ifdef L2RED_WANTED
    QosRedSyncQueueHwId (i4IfIndex, u4QId, pQNode->u4QueueHwId);
#endif

#else
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (u4QId);

#endif /* NPAPI_WANTED */

    return (QOS_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSHwWrMapClassToQueue                               */
/* Description        : This function is used to map Class or Priority to Q  */
/*                      if NPAPI Defined it will do the following Actions,   */
/*                       otherwise return SUCCESS.                           */
/*                      1. Fill the Required values form the software.       */
/*                      2. Call the NPAPI Function , retuen the Status.      */
/* Input(s)           : i4IfIndex     - Interface Index                      */
/*                    : u4CLASS       - CLASS to QId                         */
/*                    : i4RegenPriType- VLAN/TOS/DSCp/MPLS/NONE              */
/*                    : u4Priority    - Priority Value of above Type         */
/*                    : u4QId         - Q Id to the CLASS or  RegenPri       */
/*                    : u1Flag        - Map(1) or UnMap(2)                   */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : QOS_SUCCESS / QOS_FAILURE                            */
/* Called By          : Protocol Main and Lowlevel Routine                   */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QoSHwWrMapClassToQueue (INT4 i4IfIndex, UINT4 u4CLASS, INT4 i4RegenPriType,
                        UINT4 u4Priority, UINT4 u4QId, UINT1 u1Flag)
{
#ifdef NPAPI_WANTED
    INT4                i4RetStatus = QOS_FAILURE;
    UINT4               u4ClsOrPri = 0;
    tQoSClassInfoNode  *pClsInfoNode = NULL;
    tQoSFilterInfoNode *pFltInfoNode = NULL;
    tQoSClassMapEntry  *pNewClassMapEntry = NULL;
    tQoSInVlanMapNode  *pInVlanMapEntry = NULL;

    if (u4CLASS != 0)
    {
        u4ClsOrPri = u4CLASS;

        pClsInfoNode = QoSUtlGetClassInfoNode (u4CLASS);

        if (pClsInfoNode == NULL)
        {
            return (QOS_FAILURE);
        }

        /* Scan the Filter list in the ClassInfo */
        TMO_SLL_Scan (&(pClsInfoNode->SllFltInfo), pFltInfoNode,
                      tQoSFilterInfoNode *)
        {

            /* Fill the Values for that Entry */
            pNewClassMapEntry =
                QoSFiltersForClassMapEntry (pFltInfoNode->pClsMapNode);

            if (pNewClassMapEntry == NULL)
            {
                QOS_TRC_ARG1 (MGMT_TRC, "In %s : ERROR: "
                              "QoSFiltersForClassMapEntry() Returns FAILURE."
                              " \r\n", __FUNCTION__);
                return (QOS_FAILURE);
            }
            if (pNewClassMapEntry->pL2FilterPtr != NULL)
            {
                i4RegenPriType = QOS_QMAP_PRI_TYPE_L2_FIL;
                u4ClsOrPri =
                    (UINT4) pNewClassMapEntry->pL2FilterPtr->i4IssL2FilterNo;
            }
            else if (pNewClassMapEntry->pL3FilterPtr != NULL)
            {
                i4RegenPriType = QOS_QMAP_PRI_TYPE_L3_FIL;
                u4ClsOrPri =
                    (UINT4) pNewClassMapEntry->pL3FilterPtr->i4IssL3FilterNo;
            }
            else if (pNewClassMapEntry->pInVlanMapPtr != NULL)
            {
                i4RegenPriType = QOS_QMAP_PRI_TYPE_VLAN_MAP;
                u4ClsOrPri = pNewClassMapEntry->pInVlanMapPtr->u4Id;
            }
            i4RetStatus = QosxQoSHwMapClassToQueue (i4IfIndex, i4RegenPriType,
                                                    u4ClsOrPri, u4QId, u1Flag);
            /* In case the L2/L3 filter Hw Handle is needed then this NPAPI will
               be called to pass the information */
            i4RetStatus = QosxQoSHwMapClassToQueueId (pNewClassMapEntry,
                                                      i4IfIndex, i4RegenPriType,
                                                      u4ClsOrPri, u4QId,
                                                      u1Flag);

            if (pNewClassMapEntry->pInVlanMapPtr != NULL)
            {
                pInVlanMapEntry =
                    QoSUtlGetVlanQMapNode (pNewClassMapEntry->pInVlanMapPtr->
                                           u4Id);

                if (pInVlanMapEntry != NULL)
                {
                    pInVlanMapEntry->u4VlanMapHwId =
                        pNewClassMapEntry->pInVlanMapPtr->u4VlanMapHwId;
                }
            }

            QoSUtlClassMapEntryMemFree (pNewClassMapEntry);

            if (i4RetStatus == FNP_FAILURE)
            {
                QOS_TRC_ARG1 (MGMT_TRC, "In %s : ERROR: QoSHwMapClassToPolicy "
                              "() Returns FAILURE. \r\n", __FUNCTION__);

                return (QOS_FAILURE);
            }
        }
    }
    else
    {
        u4ClsOrPri = u4Priority;
        i4RetStatus =
            QosxQoSHwMapClassToQueue (i4IfIndex, i4RegenPriType, u4ClsOrPri,
                                      u4QId, u1Flag);
        /* In case the L2/L3 filter Hw Handle is needed then this NPAPI will
           be called to pass the information. Need to pass CalssMap Entry as
           NULL  */
        i4RetStatus =
            QosxQoSHwMapClassToQueueId (NULL, i4IfIndex, i4RegenPriType,
                                        u4ClsOrPri, u4QId, u1Flag);
        if (i4RetStatus == FNP_FAILURE)
        {
            QOS_TRC_ARG1 (MGMT_TRC, "In %s : ERROR: QoSHwMapClassToQueue () "
                          " Returns FAILURE. \r\n", __FUNCTION__);
            return (QOS_FAILURE);
        }
    }

#else

    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (u4CLASS);
    UNUSED_PARAM (i4RegenPriType);
    UNUSED_PARAM (u4Priority);
    UNUSED_PARAM (u4QId);
    UNUSED_PARAM (u1Flag);

#endif /* NPAPI_WANTED */
    return (QOS_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSHwWrSchedulerHierarchyMap                         */
/* Description        : This function is used to Add Scheduler Hierarchy     */
/*                       level in the Hardware                               */
/*                      if NPAPI Defined it will do the following Actions,   */
/*                       otherwise return SUCCESS.                           */
/*                      1. Fill the Required values form the software.       */
/*                      2. Call the NPAPI Function , retuen the Status.      */
/* Input(s)           : i4IfIndex     - Interface Index                      */
/*                    : u4SchedId     - Scheduler Id                         */
/*                    : u2Sweight     - Scheduler Weight for NextLevel       */
/*                    : u1Spriority   - Scheduler Priority for NextLevel     */
/*                    : i4NextSchedId - NextLevel Scheduler Id               */
/*                    : i4NextQId     - NextLevel Q Id                       */
/*                    : i2HL          - Hierarchy Level                      */
/*                    : u1Flag        - Map(1) or UnMap(2)                   */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : QOS_SUCCESS / QOS_FAILURE                            */
/* Called By          : Protocol Main and Lowlevel Routine                   */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QoSHwWrSchedulerHierarchyMap (INT4 i4IfIndex, INT4 i4SchedulerId,
                              UINT2 u2Sweight, UINT1 u1Spriority,
                              INT4 i4NextSchedId, INT4 i4NextQId,
                              UINT1 u1HL, UINT1 u1Flag)
{
#ifdef NPAPI_WANTED
    INT4                i4RetStatus = FNP_FAILURE;

    i4RetStatus = QosxQoSHwSchedulerHierarchyMap (i4IfIndex, i4SchedulerId,
                                                  u2Sweight, u1Spriority,
                                                  i4NextSchedId, i4NextQId,
                                                  (INT2) u1HL, u1Flag);
    if (i4RetStatus == FNP_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : ERROR: QoSHwSchedulerHierarchyMap () "
                      " Returns FAILURE. \r\n", __FUNCTION__);
        return (QOS_FAILURE);
    }
#else

    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (i4SchedulerId);
    UNUSED_PARAM (u2Sweight);
    UNUSED_PARAM (u1Spriority);
    UNUSED_PARAM (i4NextSchedId);
    UNUSED_PARAM (i4NextQId);
    UNUSED_PARAM (u1HL);
    UNUSED_PARAM (u1Flag);

#endif /* NPAPI_WANTED */

    return (QOS_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSHwWrSetDefUserPriority                            */
/* Description        : This function is used to Set the Default User        */
/*                      Priority for a Port in the Hardware                  */
/*                      if NPAPI Defined it will do the following Actions,   */
/*                       otherwise return SUCCESS.                           */
/*                      1. Fill the Required values form the software.       */
/*                      2. Call the NPAPI Function , retuen the Status.      */
/* Input(s)           : i4Port      - Port Number                            */
/*                    : i4DefPriority - Default Priority                     */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : QOS_SUCCESS / QOS_FAILURE                            */
/* Called By          : Protocol Main and Lowlevel Routine                   */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QoSHwWrSetDefUserPriority (INT4 i4Port, INT4 i4DefPriority)
{
#ifdef NPAPI_WANTED
    INT4                i4RetStatus = FNP_FAILURE;

    i4RetStatus = QosxQoSHwSetDefUserPriority (i4Port, i4DefPriority);
    if (i4RetStatus == FNP_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : ERROR: QoSHwSetDefUserPriority () "
                      " Returns FAILURE. \r\n", __FUNCTION__);
        return (QOS_FAILURE);
    }
#else

    UNUSED_PARAM (i4Port);
    UNUSED_PARAM (i4DefPriority);

#endif /* NPAPI_WANTED */

    return (QOS_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSHwWrSetPbitPreferenceOverDscp                     */
/* Description        : This function is used to Set the Pbit preferenc      */
/*                      over DSCP                                            */
/* Input(s)           : i4Port      - Port Number                            */
/*                    : i4Pref      - Preference                             */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : QOS_SUCCESS / QOS_FAILURE                            */
/* Called By          : Protocol Main and Lowlevel Routine                   */
/* Calling Function   :                                                      */
/*****************************************************************************/

INT4
QoSHwWrSetPbitPreferenceOverDscp (INT4 i4Port, INT4 i4Pref)
{
#ifdef NPAPI_WANTED
    INT4                i4RetStatus = FNP_FAILURE;

    i4RetStatus = QosxQoSHwSetPbitPreferenceOverDscp (i4Port, i4Pref);
    if (i4RetStatus == FNP_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC,
                      "In %s : ERROR: QoSHwSetPbitPreferenceOverDscp () "
                      " Returns FAILURE. \r\n", __FUNCTION__);
        return (QOS_FAILURE);
    }
#else

    UNUSED_PARAM (i4Port);
    UNUSED_PARAM (i4Pref);

#endif /* NPAPI_WANTED */

    return (QOS_SUCCESS);

}

/*****************************************************************************/
/* Function Name      : QoSHwWrGetMeterStats                                 */
/* Description        : This function is used to Get the Meter Stats from the*/
/*                       Hardware                                            */
/*                      if NPAPI Defined it will do the following Actions,   */
/*                       otherwise return SUCCESS.                           */
/*                      1. Fill the Required values form the software.       */
/*                      2. Call the NPAPI Function , retuen the Status.      */
/* Input(s)           : u4MeterId   - Meter Id                               */
/*                    : u4StatsType - Type of the Stats Counter              */
/*                    : pu8MeterStatsCounter - Pointer to 64BitCounter       */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : QOS_SUCCESS / QOS_FAILURE                            */
/* Called By          : Protocol Main and Lowlevel Routine                   */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QoSHwWrGetMeterStats (UINT4 u4FsQoSMeterId, UINT4 u4StatsType,
                      tSNMP_COUNTER64_TYPE * pu8MeterStatsCounter)
{
#ifdef NPAPI_WANTED
    INT4                i4RetStatus = FNP_FAILURE;

    /* Call the NP Function */
    i4RetStatus = QosxQoSHwGetMeterStats (u4FsQoSMeterId, u4StatsType,
                                          pu8MeterStatsCounter);
    if (i4RetStatus == FNP_FAILURE)
    {
        QOS_TRC_ARG3 (MGMT_TRC, "In %s : ERROR: QoSHwGetMeterStats () "
                      " Returns FAILURE for a Meter Id %ld stats type of %ld"
                      ".\r\n", u4FsQoSMeterId, u4StatsType, __FUNCTION__);
        return (QOS_FAILURE);
    }
#else

    UNUSED_PARAM (u4FsQoSMeterId);
    UNUSED_PARAM (u4StatsType);
    UNUSED_PARAM (pu8MeterStatsCounter);

#endif /* NPAPI_WANTED */

    return (QOS_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSHwWrGetCoSQStats                                  */
/* Description        : This function is used to Get the Meter Stats from the*/
/*                       Hardware                                            */
/*                      if NPAPI Defined it will do the following Actions,   */
/*                       otherwise return SUCCESS.                           */
/*                      1. Fill the Required values form the software.       */
/*                      2. Call the NPAPI Function , retuen the Status.      */
/* Input(s)           : i4IfIndex   - interface Index                        */
/*                    : u4FsQoSCoSQId - Q Id in the Interface                */
/*                    : u4StatsType - Type of the Stats Counter              */
/*                    : pu8MeterStatsCounter - Pointer to 64BitCounter       */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : QOS_SUCCESS / QOS_FAILURE                            */
/* Called By          : Protocol Main and Lowlevel Routine                   */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QoSHwWrGetCoSQStats (INT4 i4IfIndex, UINT4 u4FsQoSCoSQId, UINT4 u4StatsType,
                     tSNMP_COUNTER64_TYPE * pu8CoSQStatsCounter)
{
#ifdef NPAPI_WANTED
    INT4                i4RetStatus = FNP_FAILURE;

    /* Call the NP Function */
    i4RetStatus = QosxQoSHwGetCoSQStats (i4IfIndex, u4FsQoSCoSQId, u4StatsType,
                                         pu8CoSQStatsCounter);
    if (i4RetStatus == FNP_FAILURE)
    {
        QOS_TRC_ARG4 (MGMT_TRC, "In %s : ERROR: QoSHwGetCoSQStats () "
                      " Returns FAILURE for a Q Id %ld ,in the IfIndex Id %ld "
                      "stats type of %ld" ".\r\n", u4FsQoSCoSQId,
                      i4IfIndex, u4StatsType, __FUNCTION__);
        return (QOS_FAILURE);
    }
#else

    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (u4FsQoSCoSQId);
    UNUSED_PARAM (u4StatsType);
    UNUSED_PARAM (pu8CoSQStatsCounter);

#endif /* NPAPI_WANTED */

    return (QOS_SUCCESS);
}

#ifdef NPAPI_WANTED

/*****************************************************************************/
/* Function Name      : QoSHwWrUtilFillInProParams                           */
/* Description        : This function is used to Get the filter entries form */
/*                       the ACL L2 and L3 Filter Table and InPriorityMap    */
/*                       Table. it will do the Following Actions             */
/*                       1. Allocate Memory for tQoSClassMapNode using       */
/*                         QoSUtlClassMapEntryMemAlloc so the Calling        */
/*                         function should FREE the Memory In case of SUCCESS*/
/*                       2. if L2 Filter Id is not NULL then Get the L2      */
/*                          filter entry, if not release the Memory for L2   */
/*                          allocated in the QoSUtlClassMapEntryMemAlloc     */
/*                          function and Set it as NULL.                     */
/*                       3. if L3 Filter Id is not NULL then Get the L3      */
/*                          filter entry, if not release the Memory for L3   */
/*                          allocated in the QoSUtlClassMapEntryMemAlloc     */
/*                          function and Set it as NULL.                     */
/*                       4. if PriMap Id is not NULL then Get the PriMap     */
/*                          entry, if not release the Memory for PriMap entry*/
/*                          allocated in the QoSUtlClassMapEntryMemAlloc     */
/*                          function and Set it as NULL.                     */
/* Input(s)           : pClsMapNode  - Pointer to                            */
/*                                     tQoSClassMapNode                      */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : NULL or Pointer to tQoSClassMapEntry                 */
/* Called By          : qosnpwr Routine                                      */
/* Calling Function   :                                                      */
/*****************************************************************************/
tQoSClassMapEntry  *
QoSFiltersForClassMapEntry (tQoSClassMapNode * pClsMapNode)
{
    tQoSClassMapEntry  *pClsMapEnt = NULL;
    tIssL2FilterEntry  *pL2FilterEntry = NULL;
    tIssL3FilterEntry  *pL3FilterEntry = NULL;
    tQoSInVlanMapNode  *pInVlanMapNode = NULL;
    tQoSInPriorityMapNode *pQoSInPriMapNode = NULL;

    pClsMapEnt = QoSUtlClassMapEntryMemAlloc ();
    if (pClsMapEnt == NULL)
    {
        return (NULL);
    }

    /* Fill the Common Params */
    pClsMapEnt->u4QoSMFClass = pClsMapNode->u4ClassId;
    pClsMapEnt->u1QoSMFCStatus = pClsMapNode->u1Status;
    pClsMapEnt->u1PreColor = pClsMapNode->u1PreColor;

    /* Copy the L2 Filter Table Entry */
    if (pClsMapNode->u4L2FilterId != 0)
    {
        pL2FilterEntry = IssGetL2FilterTableEntry (pClsMapNode->u4L2FilterId);

        if (pL2FilterEntry == NULL)
        {
            QoSUtlClassMapEntryMemFree (pClsMapEnt);
            return (NULL);
        }
        MEMCPY (pClsMapEnt->pL2FilterPtr, pL2FilterEntry,
                (sizeof (tIssL2FilterEntry)));
    }
    else
    {
        MEM_FREE (pClsMapEnt->pL2FilterPtr);
        pClsMapEnt->pL2FilterPtr = NULL;
    }

    /* Copy the L3 Filter Table Entry */
    if (pClsMapNode->u4L3FilterId != 0)
    {
        pL3FilterEntry = IssGetL3FilterTableEntry (pClsMapNode->u4L3FilterId);

        if (pL3FilterEntry == NULL)
        {
            QoSUtlClassMapEntryMemFree (pClsMapEnt);
            return (NULL);
        }
        MEMCPY (pClsMapEnt->pL3FilterPtr, pL3FilterEntry,
                (sizeof (tIssL3FilterEntry)));
    }
    else
    {
        MEM_FREE (pClsMapEnt->pL3FilterPtr);
        pClsMapEnt->pL3FilterPtr = NULL;
    }

    /* Copy Vlan Map Table Entry */
    if (pClsMapNode->u4VlanMapId != 0)
    {
        pInVlanMapNode = QoSUtlGetVlanQMapNode (pClsMapNode->u4VlanMapId);

        if (pInVlanMapNode == NULL)
        {
            return (NULL);
        }

        pClsMapEnt->pInVlanMapPtr->u4Id = pInVlanMapNode->u4Id;
        pClsMapEnt->pInVlanMapPtr->u4IfIndex = pInVlanMapNode->u4IfIndex;
        pClsMapEnt->pInVlanMapPtr->u4VlanMapHwId =
            pInVlanMapNode->u4VlanMapHwId;
        pClsMapEnt->pInVlanMapPtr->u2VlanId = pInVlanMapNode->u2VlanId;
    }
    else
    {
        if (pClsMapEnt->pInVlanMapPtr != NULL)
        {
            MEM_FREE (pClsMapEnt->pInVlanMapPtr);
            pClsMapEnt->pInVlanMapPtr = NULL;
        }
    }

    /* Copy the Priority Map Table Entry */
    if (pClsMapNode->u4PriorityMapId != 0)
    {
        pQoSInPriMapNode =
            QoSUtlGetPriorityMapNode (pClsMapNode->u4PriorityMapId);

        if (pQoSInPriMapNode == NULL)
        {
            QoSUtlClassMapEntryMemFree (pClsMapEnt);
            return (NULL);
        }
        /* Copy the Priority Map Table Entry */
        pClsMapEnt->pPriorityMapPtr->u4IfIndex = pQoSInPriMapNode->u4IfIndex;
        pClsMapEnt->pPriorityMapPtr->u4QoSPriorityMapId =
            pQoSInPriMapNode->u4Id;
        pClsMapEnt->pPriorityMapPtr->u2VlanId = pQoSInPriMapNode->u2VlanId;
        pClsMapEnt->pPriorityMapPtr->u1InPriority = (UINT1)
            pQoSInPriMapNode->u1InPriority;
        pClsMapEnt->pPriorityMapPtr->u1InPriType =
            pQoSInPriMapNode->u1InPriType;
        pClsMapEnt->pPriorityMapPtr->u1RegenPri = (UINT1)
            pQoSInPriMapNode->u1RegenPriority;
        pClsMapEnt->pPriorityMapPtr->u1RegenInnerPri = (UINT1)
            pQoSInPriMapNode->u1RegenInnerPriority;
        pClsMapEnt->pPriorityMapPtr->u1PriorityMapStatus =
            pQoSInPriMapNode->u1Status;
        pClsMapEnt->pPriorityMapPtr->u4HwFilterId =
            pQoSInPriMapNode->u4HwFilterId;
    }
    else
    {
        MEM_FREE (pClsMapEnt->pPriorityMapPtr);
        pClsMapEnt->pPriorityMapPtr = NULL;
    }

    return (pClsMapEnt);
}

/*****************************************************************************/
/* Function Name      : QoSHwWrUtilFillInProParams                           */
/* Description        : This function is used to fill the data from the      */
/*                       struct 'tQoSMeterNode' to 'tQoSMeterEntry'          */
/* Input(s)           : pMeterEntry  - Pointer to                            */
/*                                     tQoSMeterEntry                        */
/*                    : pMeterNode   - Pointer to                            */
/*                                     tQoSMeterNode                         */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : None.                                                */
/* Called By          : qosnpwr Routine                                      */
/* Calling Function   :                                                      */
/*****************************************************************************/
VOID
QoSHwWrUtilFillMeterParams (tQoSMeterEntry * pMeterEntry,
                            tQoSMeterNode * pMeterNode)
{
    /* Fill Meter Table Entry using Meter Table Node */

    pMeterEntry->i4QoSMeterId = (INT4) pMeterNode->u4Id;
    pMeterEntry->u1QoSInterval = (UINT1) pMeterNode->u4Interval;
    pMeterEntry->u4QoSCIR = pMeterNode->u4CIR;
    pMeterEntry->u4QoSCBS = pMeterNode->u4CBS;
    pMeterEntry->u4QoSEIR = pMeterNode->u4EIR;
    pMeterEntry->u4QoSEBS = pMeterNode->u4EBS;
    pMeterEntry->u4QoSMeterFlag = 0;
    pMeterEntry->u4NextMeter = pMeterNode->u4Next;
    pMeterEntry->u1QoSMeterType = pMeterNode->u1Type;
    pMeterEntry->u1QoSMeterColorMode = pMeterNode->u1ColorMode;
    pMeterEntry->u1MeterStatStatus = pMeterNode->u1StatsStatus;
    pMeterEntry->u1QoSMeterStatus = pMeterNode->u1Status;

}

/*****************************************************************************/
/* Function Name      : QoSHwWrUtilFillMeterStatsParams                      */
/* Description        : This function is used to fill the data from the      */
/*                       struct 'tQoSMeterNode' to 'tQoSMeterEntry'          */
/* Input(s)           : pMeterEntry  - Pointer to                            */
/*                                     tQoSMeterEntry                        */
/*                    : pMeterNode   - Pointer to                            */
/*                                     tQoSMeterNode                         */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : None.                                                */
/* Called By          : qosnpwr Routine                                      */
/* Calling Function   :                                                      */
/*****************************************************************************/
VOID
QoSHwWrUtilFillMeterStatsParams (tQoSMeterEntry * pMeterEntry,
                                 tQoSMeterNode * pMeterNode)
{
    /* Fill Meter Table Entry using Meter Table Node */

    pMeterEntry->i4QoSMeterId = (INT4) pMeterNode->u4Id;
    pMeterEntry->u1MeterStatStatus = pMeterNode->u1StatsStatus;
    pMeterEntry->u1QoSMeterStatus = pMeterNode->u1Status;

}

/*****************************************************************************/
/* Function Name      : QoSHwWrUtilFillInProParams                           */
/* Description        : This function is used to fill the data from the      */
/*                       struct 'tQoSPolicyMapNode' to                       */
/*                       tQoSPolicyMapEntry.                                 */
/* Input(s)           : pPlyMapEntry - Pointer to                            */
/*                                     tQoSPolicyMapEntry                    */
/*                    : pPlyMapNode  - Pointer to                            */
/*                                     tQoSPolicyMapNode                     */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : None.                                                */
/* Called By          : qosnpwr Routine                                      */
/* Calling Function   :                                                      */
/*****************************************************************************/
VOID
QoSHwWrUtilFillPolicyParams (tQoSPolicyMapEntry * pPlyMapEntry,
                             tQoSPolicyMapNode * pPlyMapNode)
{
    /* Fill Policy Table Entry using Policy Table Node */
    pPlyMapEntry->u4QoSClass = pPlyMapNode->u4ClassId;
    pPlyMapEntry->i4IfIndex = (INT4) pPlyMapNode->u4IfIndex;
    pPlyMapEntry->i4QoSMeterId = (INT4) pPlyMapNode->u4MeterTableId;
    pPlyMapEntry->i4QoSPolicyMapId = (INT4) pPlyMapNode->u4Id;
    pPlyMapEntry->u1PHBType = pPlyMapNode->u1PHBType;
    pPlyMapEntry->u1DefaultPHB = (UINT1) pPlyMapNode->u2DefaultPHB;
    pPlyMapEntry->i4OutDEI = pPlyMapNode->i4VlanDE;
    pPlyMapEntry->i4Color = pPlyMapNode->i4RegenColor;
    pPlyMapEntry->u1PolicyMapStatus = pPlyMapNode->u1Status;

}

/*****************************************************************************/
/* Function Name      : QoSHwWrUtilFillInProParams                           */
/* Description        : This function is used to fill the data from the      */
/*                       struct 'tQoSPolicyMapNode' to                       */
/*                       tQoSInProfileActionEntry.                           */
/* Input(s)           : pInProEntry  - Pointer to                            */
/*                                     tQoSInProfileActionEntry              */
/*                    : pPlyMapNode  - Pointer to                            */
/*                                     tQoSPolicyMapNode                     */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : None.                                                */
/* Called By          : qosnpwr Routine                                      */
/* Calling Function   :                                                      */
/*****************************************************************************/
VOID
QoSHwWrUtilFillInProParams (tQoSInProfileActionEntry * pInProEntry,
                            tQoSPolicyMapNode * pPlyMapNode)
{
    /* Fill InProfile Table Entry using Policy Table (in profile action) Node */
    pInProEntry->u4QoSInProfConfActFlag = (UINT4)
        pPlyMapNode->u1InProfConfActFlag;

    pInProEntry->u4QoSInProfExcActFlag = (UINT4)
        pPlyMapNode->u1InProfExcActFlag;

    pInProEntry->u4QoSInProfileActionVlanPrio = (UINT4)
        pPlyMapNode->u2InProActConfVlanPrio;

    pInProEntry->u4QoSInProfileActionVlanDE = (UINT4)
        pPlyMapNode->u2InProActConfVlanDE;

    pInProEntry->u4QoSInProfileActionInnerVlanPrio = (UINT4)
        pPlyMapNode->u2InProActConfInnerVlanPrio;

    pInProEntry->u4QoSInProfileActionInnerVlanDE = (UINT4)
        pPlyMapNode->u2InProActConfInnerVlanDE;

    pInProEntry->u4QoSInProfileActionIpTOS = (UINT4)
        pPlyMapNode->u1InProActConfDscpOrToS;

    pInProEntry->u4QoSInProfileActionDscp = (UINT4)
        pPlyMapNode->u1InProActConfDscpOrToS;

    pInProEntry->u4QoSInProfileActionMplsExp = (UINT4)
        pPlyMapNode->u1InProActConfMplsExp;

    pInProEntry->u4QoSInProfConfNewClass =
        pPlyMapNode->u4InProActConfNewClassId;

    pInProEntry->u4QoSInProfConfTrafficClass =
        pPlyMapNode->u4InProfileTrafficClass;

    pInProEntry->u4QoSInProfileExceedActionVlanPrio = (UINT4)
        pPlyMapNode->u2InProActExceedVlanPrio;

    pInProEntry->u4QoSInProfileExceedActionVlanDE = (UINT4)
        pPlyMapNode->u2InProActExceedVlanDE;

    pInProEntry->u4QoSInProfileExceedActionInnerVlanPrio = (UINT4)
        pPlyMapNode->u2InProActExceedInnerVlanPrio;

    pInProEntry->u4QoSInProfileExceedActionInnerVlanDE = (UINT4)
        pPlyMapNode->u2InProActExceedInnerVlanDE;

    pInProEntry->u4QoSInProfileExceedActionIpTOS = (UINT4)
        pPlyMapNode->u1InProActExceedDscpOrToS;

    pInProEntry->u4QoSInProfileExceedActionDscp = (UINT4)
        pPlyMapNode->u1InProActExceedDscpOrToS;

    pInProEntry->u4QoSInProfileExceedActionMplsExp = (UINT4)
        pPlyMapNode->u1InProActExceedMplsExp;

    pInProEntry->u4QoSInProfExcNewClass =
        pPlyMapNode->u4InProActExccedNewClassId;

    pInProEntry->u4QoSInProfExcTrafficClass =
        pPlyMapNode->u4InProActExceedTrafficClass;

    pInProEntry->u4QoSInProfileActionPort = pPlyMapNode->u4InProfileActionPort;

}

/*****************************************************************************/
/* Function Name      : QoSHwWrUtilFillOutProParams                          */
/* Description        : This function is used to fill the data from the      */
/*                       struct 'tQoSPolicyMapNode' to                       */
/*                       tQoSOutProfileActionEntry.                          */
/* Input(s)           : pOutProEntry - Pointer to                            */
/*                                     tQoSOutProfileActionEntry             */
/*                    : pPlyMapNode  - Pointer to                            */
/*                                     tQoSPolicyMapNode                     */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : None.                                                */
/* Called By          : qosnpwr Routine                                      */
/* Calling Function   :                                                      */
/*****************************************************************************/
VOID
QoSHwWrUtilFillOutProParams (tQoSOutProfileActionEntry * pOutProEntry,
                             tQoSPolicyMapNode * pPlyMapNode)
{
    /* Fill OutProfile Table Entry using Policy Table
     * (Out profile action) Node */
    pOutProEntry->u4QoSOutProfileActionFlag = (UINT4)
        pPlyMapNode->u1OutProActionFlag;

    pOutProEntry->u4QoSOutProfileActionVlanPrio = (UINT4)
        pPlyMapNode->u1OutProActVlanPrio;

    pOutProEntry->u4QoSOutProfileActionVlanDE = (UINT4)
        pPlyMapNode->u2OutProActVlanDE;

    pOutProEntry->u4QoSOutProfileActionInnerVlanPrio = (UINT4)
        pPlyMapNode->u2OutProActInnerVlanPrio;

    pOutProEntry->u4QoSOutProfileActionInnerVlanDE = (UINT4)
        pPlyMapNode->u2OutProActInnerVlanDE;

    pOutProEntry->u4QoSOutProfileActionIpTOS = (UINT4)
        pPlyMapNode->u1OutProActDscpOrTos;

    pOutProEntry->u4QoSOutProfileActionDscp = (UINT4)
        pPlyMapNode->u1OutProActDscpOrTos;

    pOutProEntry->u4QoSOutProfileActionMplsExp = (UINT4)
        pPlyMapNode->u1OutProActMplsExp;

    pOutProEntry->u4QoSOutProfileNewClass = pPlyMapNode->u4OutProActNewClassId;

    pOutProEntry->u4QoSOutProfileTrafficClass =
        pPlyMapNode->u4OutProActTrafficClass;

}

/*****************************************************************************/
/* Function Name      : QoSUtlClassMapEntryMemAlloc                          */
/* Description        : This function is used to Allocate memory for the     */
/*                       struct 'tQoSClassMapEntry' and it's members also.   */
/*                       The Calling Function Need to release the allocated  */
/*                       Memory incase of Return none NULL.                  */
/* Input(s)           : None.                                                */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : NULL or Pointer to tQoSClassMapEntry                 */
/* Called By          : qosnpwr Routine                                      */
/* Calling Function   :                                                      */
/*****************************************************************************/
tQoSClassMapEntry  *
QoSUtlClassMapEntryMemAlloc (VOID)
{
    tIssL2FilterEntry  *pL2FilterEntry = NULL;
    tIssL3FilterEntry  *pL3FilterEntry = NULL;
    tQoSPriorityMapEntry *pPriorityMapEntry = NULL;
    tQoSClassMapEntry  *pClassMapEntry = NULL;
    tQoSInVlanMapEntry *pInVlanMapEntry = NULL;

    /* 1. Memory Allocation for tIssL2FilterEntry */
    pL2FilterEntry =
        MEM_CALLOC ((sizeof (tIssL2FilterEntry)), 1, tIssL2FilterEntry);
    if (pL2FilterEntry == NULL)
    {
        return (NULL);
    }
    /* 2. Memory Allocation for tIssL3FilterEntry */
    pL3FilterEntry =
        MEM_CALLOC ((sizeof (tIssL3FilterEntry)), 1, tIssL3FilterEntry);
    if (pL3FilterEntry == NULL)
    {
        MEM_FREE (pL2FilterEntry);
        return (NULL);
    }
    /* 3. Memory Allocation for tQoSPriorityMapEntry */
    pPriorityMapEntry =
        MEM_CALLOC ((sizeof (tQoSPriorityMapEntry)), 1, tQoSPriorityMapEntry);
    if (pPriorityMapEntry == NULL)
    {
        MEM_FREE (pL2FilterEntry);
        MEM_FREE (pL3FilterEntry);
        return (NULL);
    }

    /* 4. Memory Allocation for tQoSInVlanMapEntry */
    pInVlanMapEntry =
        MEM_CALLOC ((sizeof (tQoSInVlanMapEntry)), 1, tQoSInVlanMapEntry);
    if (pPriorityMapEntry == NULL)
    {
        MEM_FREE (pL2FilterEntry);
        MEM_FREE (pL3FilterEntry);
        MEM_FREE (pPriorityMapEntry);
        return (NULL);
    }

    /* 5. Memory Allocation for tQoSClassMapEntry */
    pClassMapEntry =
        MEM_MALLOC ((sizeof (tQoSClassMapEntry)), tQoSClassMapEntry);
    if (pClassMapEntry == NULL)
    {
        MEM_FREE (pL2FilterEntry);
        MEM_FREE (pL3FilterEntry);
        MEM_FREE (pPriorityMapEntry);
        MEM_FREE (pInVlanMapEntry);
        return (NULL);
    }

    pClassMapEntry->pL2FilterPtr = pL2FilterEntry;
    pClassMapEntry->pL3FilterPtr = pL3FilterEntry;
    pClassMapEntry->pPriorityMapPtr = pPriorityMapEntry;
    pClassMapEntry->pInVlanMapPtr = pInVlanMapEntry;

    return (pClassMapEntry);

}

/*****************************************************************************/
/* Function Name      : QoSUtlClassMapEntryMemFree                           */
/* Description        : This function is used to release the memory allocated*/
/*                       in QoSUtlClassMapEntryMemAlloc function.            */
/* Input(s)           : pClassMapEntry - Pointer to release                  */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : None.                                                */
/* Called By          : qosnpwr Routine                                      */
/* Calling Function   :                                                      */
/*****************************************************************************/
VOID
QoSUtlClassMapEntryMemFree (tQoSClassMapEntry * pClassMapEntry)
{
    if (pClassMapEntry != NULL)
    {
        if (pClassMapEntry->pL2FilterPtr != NULL)
        {
            MEM_FREE (pClassMapEntry->pL2FilterPtr);
        }
        if (pClassMapEntry->pL3FilterPtr != NULL)
        {
            MEM_FREE (pClassMapEntry->pL3FilterPtr);
        }

        if (pClassMapEntry->pPriorityMapPtr != NULL)
        {
            MEM_FREE (pClassMapEntry->pPriorityMapPtr);
        }
        if (pClassMapEntry->pInVlanMapPtr != NULL)
        {
            MEM_FREE (pClassMapEntry->pInVlanMapPtr);
        }

        MEM_FREE (pClassMapEntry);
    }
}

/*****************************************************************************/
/* Function Name      : QoSHwWrUtilFillSchedEntry                            */
/* Description        : This function is used to Allocate memory for the     */
/*                       struct 'tQoSSchedulerEntry' and it's members also.  */
/*                       The Calling Function Need to release the allocated  */
/*                       Memory incase of Return none NULL.                  */
/* Input(s)           : i4IfIndex -Interface Index                           */
/*                    : u4SchedId -Scheduler Id                              */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : NULL or Pointer to tQoSSchedulerEntry                */
/* Called By          : qosnpwr Routine                                      */
/* Calling Function   :                                                      */
/*****************************************************************************/
tQoSSchedulerEntry *
QoSHwWrUtilFillSchedEntry (INT4 i4IfIndex, UINT4 u4SchedId)
{
    tQoSSchedNode      *pSchedNode = NULL;
    tQoSShapeTemplateNode *pShapeNode = NULL;
    tQoSSchedulerEntry *pSchedEntry = NULL;
    tQoSShapeCfgEntry  *pShapeEntry = NULL;

    pSchedEntry = MEM_CALLOC ((sizeof (tQoSSchedulerEntry)), 1,
                              tQoSSchedulerEntry);
    if (pSchedEntry == NULL)
    {
        return (NULL);
    }

    pShapeEntry = MEM_CALLOC ((sizeof (tQoSShapeCfgEntry)), 1,
                              tQoSShapeCfgEntry);
    if (pShapeEntry == NULL)
    {
        QoSHwWrUtilFreeSchedEntry (pSchedEntry);
        return (NULL);
    }

    pSchedEntry->pShapePtr = pShapeEntry;

    pSchedNode = QoSUtlGetSchedNode (i4IfIndex, u4SchedId);
    if (pSchedNode == NULL)
    {
        QoSHwWrUtilFreeSchedEntry (pSchedEntry);
        return (NULL);
    }
    pSchedEntry->i4QosIfIndex = pSchedNode->i4IfIndex;
    pSchedEntry->i4QosSchedulerId = pSchedNode->u4Id;
    pSchedEntry->u4QosSchedHwId = pSchedNode->u4SchedulerHwId;
    pSchedEntry->u4QosSchedChildren = pSchedNode->u4SchedChildren;
    pSchedEntry->u1QosSchedAlgo = pSchedNode->u1SchedAlgorithm;
    pSchedEntry->u1QosSchedulerStatus = pSchedNode->u1Status;
    pSchedEntry->u1HL = pSchedNode->u1HierarchyLevel;

    if (pSchedNode->u4ShapeId != 0)
    {
        pShapeNode = QoSUtlGetShapeTempNode (pSchedNode->u4ShapeId);
        if (pShapeNode == NULL)
        {
            QoSHwWrUtilFreeSchedEntry (pSchedEntry);
            return (NULL);
        }
        pSchedEntry->pShapePtr->u2ShaperId = (UINT2) pSchedNode->u4ShapeId;
        pSchedEntry->pShapePtr->u4QosCIR = pShapeNode->u4CIR;
        pSchedEntry->pShapePtr->u4QosCBS = pShapeNode->u4CBS;
        pSchedEntry->pShapePtr->u4QosEIR = pShapeNode->u4EIR;
        pSchedEntry->pShapePtr->u4QosEBS = pShapeNode->u4EBS;
    }

    return (pSchedEntry);
}

/*****************************************************************************/
/* Function Name      : QoSHwWrUtilFreeSchedEntry                            */
/* Description        : This function is used to release the memory allocated*/
/*                       in QoSHwWrUtilFillSchedEntry  function.             */
/* Input(s)           : pSchedEntry    - Pointer to release                  */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : None.                                                */
/* Called By          : qosnpwr Routine                                      */
/* Calling Function   :                                                      */
/*****************************************************************************/
VOID
QoSHwWrUtilFreeSchedEntry (tQoSSchedulerEntry * pSchedEntry)
{
    if (pSchedEntry != NULL)
    {
        if (pSchedEntry->pShapePtr != NULL)
        {
            MEM_FREE (pSchedEntry->pShapePtr);
        }
        MEM_FREE (pSchedEntry);
    }
}

/*****************************************************************************/
/* Function Name      : QoSHwWrUtilFillQParams                               */
/* Description        : This function is used to Allocate memory for the     */
/*                       struct 'tQoSQEntry,tQoSQtypeEntry, tQoSREDCfgEntry' */
/*                       and it's members also.                              */
/*                       The Calling Function Need to release the allocated  */
/*                       Memory incase of Return none NULL.                  */
/* Input(s)           : u4QId     - Q Id                                     */
/*                    : pQEntry   - Ptr ro allocated tQoSQEntry struct.      */
/*                    : pQTypeEntry -Ptr ro allocated tQoSQtypeEntry struct  */
/*                    : apRDCfgEntry-Ptr ro allocated tQoSREDCfgEntry struct */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : QOS_SUCCESS / QOS_FAILURE                            */
/* Called By          : qosnpwr Routine                                      */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QoSHwWrUtilFillQParams (INT4 i4IfIndex, UINT4 u4QId, tQoSQEntry ** ppQEntry,
                        tQoSQtypeEntry ** ppQTypeEntry,
                        tQoSREDCfgEntry * papRDCfgEntry[])
{
    tQoSQNode          *pQNode = NULL;
    tQoSQTemplateNode  *pQTempNode = NULL;
    tQoSRDCfgNode      *pRDCfgNode = NULL;
    tQoSShapeTemplateNode *pShapeNode = NULL;
    tQoSSchedNode      *pSchedNode = NULL;
    tQoSQEntry         *pQEntry = NULL;
    tQoSQtypeEntry     *pQTypeEntry = NULL;
    tQoSREDCfgEntry    *apRDCfgEntry[6] =
        { NULL, NULL, NULL, NULL, NULL, NULL };

    tQoSShapeCfgEntry  *pShapeEntry = NULL;
    tQoSSchedulerEntry *pSchedEntry = NULL;
    UINT4               u4Cnt = 0;

    pQEntry = MEM_CALLOC ((sizeof (tQoSQEntry)), 1, tQoSQEntry);
    if (pQEntry == NULL)
    {
        return (QOS_FAILURE);
    }

    pShapeEntry =
        MEM_CALLOC ((sizeof (tQoSShapeCfgEntry)), 1, tQoSShapeCfgEntry);
    if (pShapeEntry == NULL)
    {
        QoSHwWrUtilFreeQParams (pQEntry, pQTypeEntry, apRDCfgEntry);
        return (QOS_FAILURE);
    }

    pQEntry->pShapePtr = pShapeEntry;

    pSchedEntry = MEM_CALLOC ((sizeof (tQoSSchedulerEntry)), 1,
                              tQoSSchedulerEntry);
    if (pSchedEntry == NULL)
    {
        QoSHwWrUtilFreeQParams (pQEntry, pQTypeEntry, apRDCfgEntry);
        return (QOS_FAILURE);
    }

    pQEntry->pSchedPtr = pSchedEntry;

    pSchedEntry->pShapePtr = MEM_CALLOC ((sizeof (tQoSShapeCfgEntry)), 1,
                                         tQoSShapeCfgEntry);
    if (pSchedEntry->pShapePtr == NULL)
    {
        QoSHwWrUtilFreeQParams (pQEntry, pQTypeEntry, apRDCfgEntry);
        return (QOS_FAILURE);
    }

    pQTypeEntry = MEM_CALLOC ((sizeof (tQoSQtypeEntry)), 1, tQoSQtypeEntry);
    if (pQTypeEntry == NULL)
    {
        QoSHwWrUtilFreeQParams (pQEntry, pQTypeEntry, apRDCfgEntry);
        return (QOS_FAILURE);
    }

    /* Q Table Params */
    pQNode = QoSUtlGetQNode (i4IfIndex, u4QId);
    if (pQNode == NULL)
    {
        QoSHwWrUtilFreeQParams (pQEntry, pQTypeEntry, apRDCfgEntry);
        return (QOS_FAILURE);
    }
    pQEntry->i4QosQId = pQNode->u4Id;
    pQEntry->i4QosQueueHwId = pQNode->u4QueueHwId;
    pQEntry->i4QosSchedulerId = (INT4) pQNode->u4SchedulerId;
    pQEntry->u2QosQWeight = pQNode->u2Weight;
    pQEntry->u1QosQPriority = pQNode->u1Priority;
    pQEntry->u1QosQStatus = pQNode->u1Status;
    pQEntry->u4QueueType = pQNode->u4QueueType;
    /* Shape Template Params */
    if (pQNode->u4ShapeId != 0)
    {
        pShapeNode = QoSUtlGetShapeTempNode (pQNode->u4ShapeId);
        if (pShapeNode == NULL)
        {
            QoSHwWrUtilFreeQParams (pQEntry, pQTypeEntry, apRDCfgEntry);
            return (QOS_FAILURE);
        }
        pQEntry->pShapePtr->u4QosCIR = pShapeNode->u4CIR;
        pQEntry->pShapePtr->u4QosCBS = pShapeNode->u4CBS;
        pQEntry->pShapePtr->u4QosEIR = pShapeNode->u4EIR;
        pQEntry->pShapePtr->u4QosEBS = pShapeNode->u4EBS;
    }
    pQEntry->pShapePtr->u2ShaperId = (UINT2) pQNode->u4ShapeId;
    /* Scheduler Params */
    if (pQNode->u4SchedulerId != 0)
    {
        pSchedNode = QoSUtlGetSchedNode (i4IfIndex, pQNode->u4SchedulerId);

        if (pSchedNode == NULL)
        {
            if ((pSchedNode =
                 QoSUtlGetSchedNode (0, pQNode->u4SchedulerId)) == NULL)
            {
                QoSHwWrUtilFreeQParams (pQEntry, pQTypeEntry, apRDCfgEntry);
                return (QOS_FAILURE);
            }
        }

        pSchedEntry->i4QosIfIndex = pSchedNode->i4IfIndex;
        pSchedEntry->i4QosSchedulerId = pSchedNode->u4Id;
        pSchedEntry->u4QosSchedHwId = pSchedNode->u4SchedulerHwId;
        pSchedEntry->u4QosSchedChildren = pSchedNode->u4SchedChildren;
        pSchedEntry->u1QosSchedAlgo = pSchedNode->u1SchedAlgorithm;
        pSchedEntry->u1QosSchedulerStatus = pSchedNode->u1Status;
        pSchedEntry->u1HL = pSchedNode->u1HierarchyLevel;

        if (pSchedNode->u4ShapeId != 0)
        {
            pShapeNode = QoSUtlGetShapeTempNode (pSchedNode->u4ShapeId);

            if (pShapeNode == NULL)
            {
                QoSHwWrUtilFreeQParams (pQEntry, pQTypeEntry, apRDCfgEntry);
                return (QOS_FAILURE);
            }

            pSchedEntry->pShapePtr->u4QosCIR = pShapeNode->u4CIR;
            pSchedEntry->pShapePtr->u4QosCBS = pShapeNode->u4CBS;
            pSchedEntry->pShapePtr->u4QosEIR = pShapeNode->u4EIR;
            pSchedEntry->pShapePtr->u4QosEBS = pShapeNode->u4EBS;
        }
    }

    /* Q Template Params */
    pQTempNode = QoSUtlGetQTempNode (pQNode->u4CfgTemplateId);
    if (pQTempNode == NULL)
    {
        QoSHwWrUtilFreeQParams (pQEntry, pQTypeEntry, apRDCfgEntry);
        return (QOS_FAILURE);
    }

    pQTypeEntry->u4QueueTypeId = pQTempNode->u4Id;
    pQTypeEntry->u4QueueSize = pQTempNode->u4Size;
    pQTypeEntry->u1DropAlgo = (UINT1) pQTempNode->u2DropType;
    pQTypeEntry->u1DropAlgoEnableFlag = pQTempNode->u1DropAlgoEnableFlag;

    /* RED Params */
    if (pQTempNode->u1DropAlgoEnableFlag == QOS_Q_TEMP_DROP_ALGO_ENABLE)
    {
        for (u4Cnt = QOS_RD_CONFG_DP_MIN_VAL;
             u4Cnt <= QOS_RD_CONFG_DP_MAX_VAL; u4Cnt++)
        {
            pRDCfgNode = QoSUtlGetRDCfgNode (pQNode->u4CfgTemplateId, u4Cnt);
            if (pRDCfgNode == NULL)
            {
                continue;
            }

            if (pRDCfgNode->u1Status != ACTIVE)
            {
                continue;
            }

            apRDCfgEntry[u4Cnt] = MEM_CALLOC ((sizeof (tQoSREDCfgEntry)), 1,
                                              tQoSREDCfgEntry);
            if (apRDCfgEntry[u4Cnt] == NULL)
            {
                QoSHwWrUtilFreeQParams (pQEntry, pQTypeEntry, apRDCfgEntry);
                return (QOS_FAILURE);
            }
            /* Assign the Ptr */

            apRDCfgEntry[u4Cnt]->u4MinAvgThresh = pRDCfgNode->u4MinAvgThresh;
            apRDCfgEntry[u4Cnt]->u4MaxAvgThresh = pRDCfgNode->u4MaxAvgThresh;
            apRDCfgEntry[u4Cnt]->u4MaxPktSize = pRDCfgNode->u4MaxPktSize;
            apRDCfgEntry[u4Cnt]->u1MaxProbability = pRDCfgNode->u1MaxProb;
            apRDCfgEntry[u4Cnt]->u1ExpWeight = pRDCfgNode->u1ExpWeight;
            apRDCfgEntry[u4Cnt]->u1DropPrecedence = pRDCfgNode->u1DP;
            apRDCfgEntry[u4Cnt]->u4Gain = pRDCfgNode->u4Gain;
            apRDCfgEntry[u4Cnt]->u4ECNThresh = pRDCfgNode->u4ECNThresh;
            apRDCfgEntry[u4Cnt]->u1DropThreshType =
                pRDCfgNode->u1DropThreshType;
            apRDCfgEntry[u4Cnt]->u1RDActionFlag = pRDCfgNode->u1RDActionFlag;

        }                        /* End of for */

    }

    /* Assign the Ptrs */
    *ppQEntry = pQEntry;
    *ppQTypeEntry = pQTypeEntry;
    papRDCfgEntry[0] = apRDCfgEntry[0];
    papRDCfgEntry[1] = apRDCfgEntry[1];
    papRDCfgEntry[2] = apRDCfgEntry[2];
    papRDCfgEntry[3] = apRDCfgEntry[3];
    papRDCfgEntry[4] = apRDCfgEntry[4];
    papRDCfgEntry[5] = apRDCfgEntry[5];

    return (QOS_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSHwWrUtilFreeQParams                               */
/* Description        : This function is used to release the memory allocated*/
/*                       in QoSHwWrUtilFillQParams     function.             */
/* Input(s)           : pSchedEntry    - Pointer to release                  */
/*                    : pQTypeEntry    - Pointer to release                  */
/*                    : papRDCfgEntry  - array of Pointer to release         */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : None.                                                */
/* Called By          : qosnpwr Routine                                      */
/* Calling Function   :                                                      */
/*****************************************************************************/
VOID
QoSHwWrUtilFreeQParams (tQoSQEntry * pQEntry, tQoSQtypeEntry * pQTypeEntry,
                        tQoSREDCfgEntry * papRDCfgEntry[])
{
    UINT4               u4Counter = 0;

    if (pQEntry != NULL)
    {
        if (pQEntry->pShapePtr != NULL)
        {
            MEM_FREE (pQEntry->pShapePtr);
        }

        if (pQEntry->pSchedPtr != NULL)
        {
            if (pQEntry->pSchedPtr->pShapePtr != NULL)
            {
                MEM_FREE (pQEntry->pSchedPtr->pShapePtr);
            }
            MEM_FREE (pQEntry->pSchedPtr);
        }

        MEM_FREE (pQEntry);
    }

    if (pQTypeEntry != NULL)
    {
        MEM_FREE (pQTypeEntry);
    }

    /*RED Cfg  array of Ptr */
    for (u4Counter = QOS_RD_CONFG_DP_MIN_VAL;
         u4Counter <= QOS_RD_CONFG_DP_MAX_VAL; u4Counter++)
    {
        if (papRDCfgEntry[u4Counter] != NULL)
        {
            MEM_FREE (papRDCfgEntry[u4Counter]);
        }
    }
}
#endif /* NPAPI_WANTED */
/*****************************************************************************/
/* Function Name      : QoSHwWrGetCountActPkts                               */
/* Description        : This function is used to Get the CountAct Stats from the*/
/*                       Hardware                                            */
/*                      if NPAPI Defined it will do the following Actions,   */
/*                       otherwise return SUCCESS.                           */
/*                      1. Fill the Required values form the software.       */
/*                      2. Call the NPAPI Function , retuen the Status.      */
/* Input(s)           : u4DiffServCountActId   -Id                               */
/*                    : u4StatsType - Type of the Stats Counter              */
/*                    : pu8RetValDiffServCountActPkts - Pointer to 64BitCounter*/
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : QOS_SUCCESS / QOS_FAILURE                            */
/* Called By          : Protocol Main and Lowlevel Routine                   */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QoSHwWrGetCountActPkts (UINT4 u4DiffServCountActId, UINT4 u4StatsType,
                        tSNMP_COUNTER64_TYPE * pu8RetValDiffServCountActPkts)
{
#ifdef NPAPI_WANTED
    INT4                i4RetStatus = FNP_FAILURE;

    /* Call the NP Function */
    i4RetStatus = QosxQoSHwGetCountActStats (u4DiffServCountActId, u4StatsType,
                                             pu8RetValDiffServCountActPkts);
    if (i4RetStatus == FNP_FAILURE)
    {
        QOS_TRC_ARG3 (MGMT_TRC, "In %s : ERROR: QoSHwGetMeterStats () "
                      " Returns FAILURE for a Meter Id %ld stats type of %ld"
                      ".\r\n", u4DiffServCountActId, u4StatsType, __FUNCTION__);
        return (QOS_FAILURE);
    }
#else

    UNUSED_PARAM (u4DiffServCountActId);
    UNUSED_PARAM (u4StatsType);
    UNUSED_PARAM (pu8RetValDiffServCountActPkts);

#endif /* NPAPI_WANTED */

    return (QOS_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSHwWrGetCountActOctets                               */
/* Description        : This function is used to Get the CountAct Stats from the*/
/*                       Hardware                                            */
/*                      if NPAPI Defined it will do the following Actions,   */
/*                       otherwise return SUCCESS.                           */
/*                      1. Fill the Required values form the software.       */
/*                      2. Call the NPAPI Function , retuen the Status.      */
/* Input(s)           : u4DiffServCountActId   -Id                               */
/*                    : u4StatsType - Type of the Stats Counter              */
/*                    : pu8RetValDiffServCountActPkts - Pointer to 64BitCounter*/
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : QOS_SUCCESS / QOS_FAILURE                            */
/* Called By          : Protocol Main and Lowlevel Routine                   */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QoSHwWrGetCountActOctets (UINT4 u4DiffServCountActId, UINT4 u4StatsType,
                          tSNMP_COUNTER64_TYPE *
                          pu8RetValDiffServCountActOctets)
{
#ifdef NPAPI_WANTED
    INT4                i4RetStatus = FNP_FAILURE;

    /* Call the NP Function */
    i4RetStatus = QosxQoSHwGetCountActStats (u4DiffServCountActId, u4StatsType,
                                             pu8RetValDiffServCountActOctets);
    if (i4RetStatus == FNP_FAILURE)
    {
        QOS_TRC_ARG3 (MGMT_TRC, "In %s : ERROR: QoSHwGetCountActStats () "
                      " Returns FAILURE for a CountAct Id %ld stats type of %ld"
                      ".\r\n", u4DiffServCountActId, u4StatsType, __FUNCTION__);
        return (QOS_FAILURE);
    }
#else

    UNUSED_PARAM (u4DiffServCountActId);
    UNUSED_PARAM (u4StatsType);
    UNUSED_PARAM (pu8RetValDiffServCountActOctets);

#endif /* NPAPI_WANTED */

    return (QOS_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSHwWrGetAlgDropOctets                              */
/* Description        : This function is used to Get the AlgDrop Stats from the*/
/*                       Hardware                                            */
/*                      if NPAPI Defined it will do the following Actions,   */
/*                       otherwise return SUCCESS.                           */
/*                      1. Fill the Required values form the software.       */
/*                      2. Call the NPAPI Function , retuen the Status.      */
/* Input(s)           : u4DiffServAlgDropId   -Id                               */
/*                    : u4StatsType - Type of the Stats Counter              */
/*                    : pu8RetValDiffServAlgDropOctets - Pointer to 64BitCounter*/
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : QOS_SUCCESS / QOS_FAILURE                            */
/* Called By          : Protocol Main and Lowlevel Routine                   */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QoSHwWrGetAlgDropOctets (UINT4 u4DiffServAlgDropId, UINT4 u4StatsType,
                         tSNMP_COUNTER64_TYPE * pu8RetValDiffServAlgDropOctets)
{
#ifdef NPAPI_WANTED
    INT4                i4RetStatus = FNP_FAILURE;

    /* Call the NP Function */
    i4RetStatus = QosxQoSHwGetAlgDropStats (u4DiffServAlgDropId, u4StatsType,
                                            pu8RetValDiffServAlgDropOctets);
    if (i4RetStatus == FNP_FAILURE)
    {
        QOS_TRC_ARG3 (MGMT_TRC, "In %s : ERROR: QoSHwGetAlgDropStats () "
                      " Returns FAILURE for a AlgDrop Id %ld stats type of %ld"
                      ".\r\n", u4DiffServAlgDropId, u4StatsType, __FUNCTION__);
        return (QOS_FAILURE);
    }
#else

    UNUSED_PARAM (u4DiffServAlgDropId);
    UNUSED_PARAM (u4StatsType);
    UNUSED_PARAM (pu8RetValDiffServAlgDropOctets);

#endif /* NPAPI_WANTED */

    return (QOS_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSHwWrGetAlgDropPkts                                */
/* Description        : This function is used to Get the AlgDrop Stats from the*/
/*                       Hardware                                            */
/*                      if NPAPI Defined it will do the following Actions,   */
/*                       otherwise return SUCCESS.                           */
/*                      1. Fill the Required values form the software.       */
/*                      2. Call the NPAPI Function , retuen the Status.      */
/* Input(s)           : u4DiffServCountActId   -Id                               */
/*                    : u4StatsType - Type of the Stats Counter              */
/*                    : pu8RetValDiffServCountActPkts - Pointer to 64BitCounter*/
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : QOS_SUCCESS / QOS_FAILURE                            */
/* Called By          : Protocol Main and Lowlevel Routine                   */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QoSHwWrGetAlgDropPkts (UINT4 u4DiffServAlgDropId, UINT4 u4StatsType,
                       tSNMP_COUNTER64_TYPE * pu8RetValDiffServAlgDropPkts)
{
#ifdef NPAPI_WANTED
    INT4                i4RetStatus = FNP_FAILURE;

    /* Call the NP Function */
    i4RetStatus = QosxQoSHwGetAlgDropStats (u4DiffServAlgDropId, u4StatsType,
                                            pu8RetValDiffServAlgDropPkts);
    if (i4RetStatus == FNP_FAILURE)
    {
        QOS_TRC_ARG3 (MGMT_TRC, "In %s : ERROR: QoSHwGetAlgDropStats () "
                      " Returns FAILURE for a AlgDrop %ld stats type of %ld"
                      ".\r\n", u4DiffServAlgDropId, u4StatsType, __FUNCTION__);
        return (QOS_FAILURE);
    }
#else

    UNUSED_PARAM (u4DiffServAlgDropId);
    UNUSED_PARAM (u4StatsType);
    UNUSED_PARAM (pu8RetValDiffServAlgDropPkts);

#endif /* NPAPI_WANTED */

    return (QOS_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSHwWrGetRandomDropOctets                           */
/* Description        : This function is used to Get the Random Drop Stats   */
/*                      the  Hardware                                        */
/*                      if NPAPI Defined it will do the following Actions,   */
/*                       otherwise return SUCCESS.                           */
/*                      1. Fill the Required values form the software.       */
/*                      2. Call the NPAPI Function , retuen the Status.      */
/* Input(s)           : u4DiffServAlgDropId   -Id                               */
/*                    : u4StatsType - Type of the Stats Counter              */
/*                    : pu8RetValDiffServRandomDropOctets - Pointer to 64BitCounter*/
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : QOS_SUCCESS / QOS_FAILURE                            */
/* Called By          : Protocol Main and Lowlevel Routine                   */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QoSHwWrGetRandomDropOctets (UINT4 u4DiffServAlgDropId, UINT4 u4StatsType,
                            tSNMP_COUNTER64_TYPE *
                            pu8RetValDiffServRandomDropOctets)
{
#ifdef NPAPI_WANTED
    INT4                i4RetStatus = FNP_FAILURE;

    /* Call the NP Function */
    i4RetStatus = QosxQoSHwGetRandomDropStats (u4DiffServAlgDropId, u4StatsType,
                                               pu8RetValDiffServRandomDropOctets);
    if (i4RetStatus == FNP_FAILURE)
    {
        QOS_TRC_ARG3 (MGMT_TRC, "In %s : ERROR: QoSHwGetAlgDropStats () "
                      " Returns FAILURE for a AlgDrop Id %ld stats type of %ld"
                      ".\r\n", u4DiffServAlgDropId, u4StatsType, __FUNCTION__);
        return (QOS_FAILURE);
    }
#else

    UNUSED_PARAM (u4DiffServAlgDropId);
    UNUSED_PARAM (u4StatsType);
    UNUSED_PARAM (pu8RetValDiffServRandomDropOctets);

#endif /* NPAPI_WANTED */

    return (QOS_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSHwWrGetRandomDropPkts                                */
/* Description        : This function is used to Get the RandomDrop Stats from the*/
/*                       Hardware                                            */
/*                      if NPAPI Defined it will do the following Actions,   */
/*                       otherwise return SUCCESS.                           */
/*                      1. Fill the Required values form the software.       */
/*                      2. Call the NPAPI Function , retuen the Status.      */
/* Input(s)           : u4DiffServCountActId   -Id                               */
/*                    : u4StatsType - Type of the Stats Counter              */
/*                    : pu8RetValDiffServCountActPkts - Pointer to 64BitCounter*/
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : QOS_SUCCESS / QOS_FAILURE                            */
/* Called By          : Protocol Main and Lowlevel Routine                   */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QoSHwWrGetRandomDropPkts (UINT4 u4DiffServAlgDropId, UINT4 u4StatsType,
                          tSNMP_COUNTER64_TYPE *
                          pu8RetValDiffServRandomDropPkts)
{
#ifdef NPAPI_WANTED
    INT4                i4RetStatus = FNP_FAILURE;

    /* Call the NP Function */
    i4RetStatus = QosxQoSHwGetRandomDropStats (u4DiffServAlgDropId, u4StatsType,
                                               pu8RetValDiffServRandomDropPkts);
    if (i4RetStatus == FNP_FAILURE)
    {
        QOS_TRC_ARG3 (MGMT_TRC, "In %s : ERROR: QoSHwGetRandomDropStats () "
                      " Returns FAILURE for a AlgDrop %ld stats type of %ld"
                      ".\r\n", u4DiffServAlgDropId, u4StatsType, __FUNCTION__);
        return (QOS_FAILURE);
    }
#else

    UNUSED_PARAM (u4DiffServAlgDropId);
    UNUSED_PARAM (u4StatsType);
    UNUSED_PARAM (pu8RetValDiffServRandomDropPkts);

#endif /* NPAPI_WANTED */

    return (QOS_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSHwWrSetCpuRateLimit                               */
/* Description        : This function is used to Set the Min and Max rates   */
/*                      of CPU in the Hardware.                              */
/*                      if NPAPI Defined it will do the following Actions,   */
/*                       otherwise return SUCCESS.                           */
/*                      1. Fill the Required values form the software.       */
/*                      2. Call the NPAPI Function , retuen the Status.      */
/* Input(s)           : i4CpuQueueId     - CPU Queue ID                      */
/*                    : u4MinRate        - Minimum rate                      */
/*                    : u4MaxRate        - Maximum rate                      */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : QOS_SUCCESS / QOS_FAILURE                            */
/* Called By          : Protocol Main and Lowlevel Routine                   */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QoSHwWrSetCpuRateLimit (INT4 i4CpuQueueId, UINT4 u4MinRate, UINT4 u4MaxRate)
{
#ifdef NPAPI_WANTED
    INT4                i4RetStatus = FNP_FAILURE;

    i4RetStatus = QosxQoSHwSetCpuRateLimit (i4CpuQueueId, u4MinRate, u4MaxRate);
    if (i4RetStatus == FNP_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : ERROR: QoSHwSetCpuRateLimit () "
                      " Returns FAILURE. \r\n", __FUNCTION__);
        return (QOS_FAILURE);
    }
#else

    UNUSED_PARAM (i4CpuQueueId);
    UNUSED_PARAM (u4MinRate);
    UNUSED_PARAM (u4MaxRate);

#endif /* NPAPI_WANTED */

    return (QOS_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSHwWrMapClassToIntPriority                         */
/* Description        : This function is used to  map                        */
/*                      traffic classifiled by filter  to internal priority. */
/*                      if NPAPI Defined it will do the following Actions,   */
/*                       otherwise return SUCCESS.                           */
/*                      1. Fill the Required values form the software.       */
/*                      2. Call the NPAPI Function , retuen the Status.      */
/* Input(s)           : u4HwFilterId     - Filter Id                         */
/*                    : u1RegenPri       - Regen Pri                         */
/*                    : u1Flag           - Add /Remove                       */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : QOS_SUCCESS / QOS_FAILURE                            */
/* Called By          : Protocol Main and Lowlevel Routine                   */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QoSHwWrMapClassToIntPriority (tQoSClassMapNode * pClsMapNode, UINT1 u1RegenPri,
                              UINT1 u1Flag)
{
#ifdef NPAPI_WANTED
    INT4                i4RetStatus = FNP_FAILURE;
    tQosClassToIntPriEntry QosClassToIntPriEntry;
    tIssL2FilterEntry  *pL2FilterEntry = NULL;
    tIssL3FilterEntry  *pL3FilterEntry = NULL;

    if (pClsMapNode == NULL)
    {
        return QOS_FAILURE;
    }
    MEMSET (&(QosClassToIntPriEntry), 0, (sizeof (tQosClassToIntPriEntry)));

    if (pClsMapNode->u4L2FilterId != 0)
    {
        pL2FilterEntry =
            IssGetL2FilterTableEntry ((INT4) pClsMapNode->u4L2FilterId);
        if (pL2FilterEntry == NULL)
        {
            return QOS_FAILURE;
        }
        QosClassToIntPriEntry.pL2FilterPtr =
            MEM_CALLOC ((sizeof (tIssL2FilterEntry)), 1, tIssL2FilterEntry);

        if (QosClassToIntPriEntry.pL2FilterPtr == NULL)
        {
            return QOS_FAILURE;
        }

        MEMCPY (QosClassToIntPriEntry.pL2FilterPtr, pL2FilterEntry,
                (sizeof (tIssL2FilterEntry)));
    }
    else if (pClsMapNode->u4L3FilterId != 0)
    {
        pL3FilterEntry =
            IssGetL3FilterTableEntry ((INT4) pClsMapNode->u4L3FilterId);

        if (pL3FilterEntry == NULL)
        {
            return QOS_FAILURE;
        }
        QosClassToIntPriEntry.pL3FilterPtr =
            MEM_CALLOC ((sizeof (tIssL3FilterEntry)), 1, tIssL3FilterEntry);
        if (QosClassToIntPriEntry.pL3FilterPtr == NULL)
        {
            return QOS_FAILURE;
        }
        MEMCPY (QosClassToIntPriEntry.pL3FilterPtr, pL3FilterEntry,
                (sizeof (tIssL3FilterEntry)));
    }

    QosClassToIntPriEntry.u1IntPriority = u1RegenPri;
    QosClassToIntPriEntry.u1Flag = u1Flag;

    if ((pClsMapNode->u4L2FilterId != 0) || (pClsMapNode->u4L3FilterId != 0))
    {
        i4RetStatus =
            (INT4) QosxQosHwMapClassToIntPriority (&QosClassToIntPriEntry);
    }
    else if (u1Flag == QOS_NP_DEL)
    {                            /* Returning success to proceed with module cleanup, since
                                 * CLASS is not mapped to either L2 or L3 filters */
        i4RetStatus = FNP_SUCCESS;
    }
    if (QosClassToIntPriEntry.pL2FilterPtr != NULL)
    {
        MEM_FREE (QosClassToIntPriEntry.pL2FilterPtr);
    }
    if (QosClassToIntPriEntry.pL3FilterPtr != NULL)
    {
        MEM_FREE (QosClassToIntPriEntry.pL3FilterPtr);
    }
    if (i4RetStatus == FNP_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : ERROR: QosHwMapClassToIntPriority () "
                      " Returns FAILURE. \r\n", __FUNCTION__);
        return (QOS_FAILURE);
    }
    if (i4RetStatus == FNP_NOT_SUPPORTED)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : ERROR: QosHwMapClassToIntPriority () "
                      " Returns NOT_SUPPORTED. \r\n", __FUNCTION__);
        return (QOS_NP_NOT_SUPPORTED);
    }
#else
    UNUSED_PARAM (pClsMapNode);
    UNUSED_PARAM (u1RegenPri);
    UNUSED_PARAM (u1Flag);

#endif
    return QOS_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : QoSHwWrMapClasstoPriMap                               */
/* Description        : This function is used to  map traffic classifiled    */
/*                         by VlanPri/Exp  to internal priority.                */
/*                      if NPAPI Defined it will do the following Actions,   */
/*                       otherwise return SUCCESS.                           */
/*                      1. Fill the Required values form the software.       */
/*                      2. Call the NPAPI Function , retuen the Status.      */
/* Input(s)           : pPriMapNode - Pointer to priority map parameters     */
/*                    : u1Flag      - Add/Remove                             */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : QOS_SUCCESS / QOS_FAILURE                            */
/* Called By          : Protocol Main and Lowlevel Routine                   */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QoSHwWrMapClasstoPriMap (tQoSInPriorityMapNode * pPriMapNode, UINT1 u1Flag)
{
#ifdef NPAPI_WANTED
    INT4                i4RetStatus = FNP_FAILURE;
    tQosClassToPriMapEntry QosClassToPriMapEntry;

    MEMSET (&(QosClassToPriMapEntry), 0, (sizeof (tQosClassToPriMapEntry)));

    QosClassToPriMapEntry.i4IfIndex = (INT4) pPriMapNode->u4IfIndex;

    QosClassToPriMapEntry.u4HwFilterId = pPriMapNode->u4HwFilterId;
    QosClassToPriMapEntry.u2VlanId = pPriMapNode->u2VlanId;
    QosClassToPriMapEntry.u1PriType = pPriMapNode->u1InPriType;
    QosClassToPriMapEntry.u1Priority = pPriMapNode->u1InPriority;
    QosClassToPriMapEntry.u1RegenPriority = pPriMapNode->u1RegenPriority;
    QosClassToPriMapEntry.i1DEI = (INT1) pPriMapNode->i4InDEI;
    QosClassToPriMapEntry.i1Color = (INT1) pPriMapNode->i4RegenColor;
    QosClassToPriMapEntry.u1Flag = u1Flag;    /* QOS_NP_ADD/QOS_NP_DEL */

    i4RetStatus = QosxQoSHwMapClasstoPriMap (&QosClassToPriMapEntry);
    if (i4RetStatus == FNP_NOT_SUPPORTED)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : ERROR: QoSHwWrMapClasstoPriMap () "
                      " Returns NOT_SUPPORTED. \r\n", __FUNCTION__);
        return (QOS_NP_NOT_SUPPORTED);
    }

    if (i4RetStatus == FNP_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : ERROR: QoSHwWrMapClasstoPriMap () "
                      " Returns FAILURE. \r\n", __FUNCTION__);
        return (QOS_FAILURE);
    }
    pPriMapNode->u4HwFilterId = QosClassToPriMapEntry.u4HwFilterId;
    pPriMapNode->i4HwExpMapId = QosClassToPriMapEntry.i4HwExpMapId;
#else
    UNUSED_PARAM (pPriMapNode);
    UNUSED_PARAM (u1Flag);
#endif
    return QOS_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : QoSHwWrGetDscpQueueMap                               */
/* Description        : This function is used to  get the queue ID set in    */
/*                         the hardware for the given DSCP value             */
/*                      if NPAPI Defined it will do the following Actions,   */
/*                       otherwise return SUCCESS.                           */
/*                      1. Fill the Required values form the software.       */
/*                      2. Call the NPAPI Function , retuen the Status.      */
/* Input(s)           : i4QueueID   - Pointer to Queue ID                    */
/*                    : i4DscpVal   - DscpValue                              */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : QOS_SUCCESS / QOS_FAILURE                            */
/* Called By          : Protocol Main and Lowlevel Routine                   */
/*****************************************************************************/
INT4
QoSHwWrGetDscpQueueMap (INT4 i4DscpVal, INT4 *i4QueueID)
{
#ifdef NPAPI_WANTED

    INT4                i4RetStatus = FNP_FAILURE;
    /* Call the NP Function */
    i4RetStatus = QosxQoSHwGetDscpQueueMap (i4DscpVal, i4QueueID);
    if (i4RetStatus == FNP_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : ERROR: QoSHwWrGetDscpQueueMap () "
                      " Returns FAILURE. \r\n", __FUNCTION__);

        return (QOS_FAILURE);
    }

#else
    UNUSED_PARAM (i4DscpVal);
    UNUSED_PARAM (i4QueueID);
#endif
    return QOS_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : QosHwWrSetTrafficClassToPcp                          */
/* Description        : This function is used to Set traffic class to PCP    */
/* Input(s)           : i4IfIndex   - Port Number                            */
/*                    : i4UserPriority - Priority                            */
/*                    : i4TrafficClass - TrafficClass                        */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : QOS_SUCCESS / QOS_FAILURE                            */
/* Called By          : Protocol Main and Lowlevel Routine                   */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QosHwWrSetTrafficClassToPcp (INT4 i4IfIndex, INT4 i4UserPriority,
                             INT4 i4TrafficClass)
{
#ifdef NPAPI_WANTED
    if (QosxQoSHwSetTrafficClassToPcp (i4IfIndex, i4UserPriority,
                                       i4TrafficClass) == FNP_FAILURE)
    {
        return (QOS_FAILURE);
    }
#else
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (i4UserPriority);
    UNUSED_PARAM (i4TrafficClass);
#endif
    return (QOS_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QosHwWrGetPfcStats                                   */
/* Description        : This function is used to Get PFC Statistics          */
/* Input(s)           : pQosPfcStats - pointer to the Structure              */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : QOS_SUCCESS / QOS_FAILURE                            */
/* Called By          : Protocol Main and Lowlevel Routine                   */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QosHwWrGetPfcStats (tQosPfcStats * pQosPfcStats)
{
#ifdef NPAPI_WANTED
    tQosPfcHwStats      PfcHwStats;
    MEMSET (&PfcHwStats, DCBX_ZERO, sizeof (tQosPfcHwStats));

    PfcHwStats.u4IfIndex = pQosPfcStats->u4IfIndex;
    PfcHwStats.u1PfcStatsType = pQosPfcStats->u1PfcStatsType;
    PfcHwStats.u4PfcPauseCount = pQosPfcStats->u4PfcPauseCount;
    if (QosxFsQosHwGetPfcStats (&PfcHwStats) == FNP_FAILURE)
    {
        return (QOS_FAILURE);
    }
    else
    {
        pQosPfcStats->u4PfcPauseCount = PfcHwStats.u4PfcPauseCount;
    }
#else
    UNUSED_PARAM (pQosPfcStats);
#endif
    return (QOS_SUCCESS);
}
#endif /* __QOS_NP_WR_C__ */
