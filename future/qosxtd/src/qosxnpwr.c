/* $Id: qosxnpwr.c,v 1.7 2016/05/25 10:04:42 siva Exp $ */
/****************************************************************************/
/* Copyright (C) 2012 Aricent Inc . All Rights Reserved                     */
/*                                                                          */
/*  FILE NAME             : qosxnpwr.c                                      */
/*  PRINCIPAL AUTHOR      : Aricent                                         */
/*  SUBSYSTEM NAME        : QoSx                                            */
/*  MODULE NAME           : QoSX-NP-WR                                      */
/*  LANGUAGE              : C                                               */
/*  TARGET ENVIRONMENT    : Linux                                           */
/*  DATE OF FIRST RELEASE :                                                 */
/*  AUTHOR                : Aricent                                         */
/*  FUNCTIONS DEFINED(Applicable for Source files) :                        */
/*  DESCRIPTION           : This File contains the NPAPI related Wraper     */
/*                          routines for the Aricent QoSx Module.           */
/****************************************************************************/
/*                                                                          */
/*  Change History                                                          */
/*  Version               :                                                 */
/*  Date(DD/MM/YYYY)      :                                                 */
/*  Modified by           :                                                 */
/*  Description of change :                                                 */
/****************************************************************************/
#ifndef __QOSX_NP_WR_C__
#define __QOSX_NP_WR_C__

#include "nputil.h"

/***************************************************************************
 *                                                                          
 *    Function Name       : QosxNpWrHwProgram                                         
 *                                                                          
 *    Description         : This function takes care of calling appropriate 
 *                          Np call using the tQosxNpModInfo
 *                                                                          
 *    Input(s)            : FsHwNpParam of type tfsHwNp                     
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

PUBLIC UINT1
QosxNpWrHwProgram (tFsHwNp * pFsHwNp)
{
    UINT1               u1RetVal = FNP_SUCCESS;
    UINT4               u4Opcode = 0;
    tQosxNpModInfo     *pQosxNpModInfo = NULL;

    if (NULL == pFsHwNp)
    {
        return FNP_FAILURE;
    }

    u4Opcode = pFsHwNp->u4Opcode;
    pQosxNpModInfo = &(pFsHwNp->QosxNpModInfo);

    if (NULL == pQosxNpModInfo)
    {
        return FNP_FAILURE;
    }

    switch (u4Opcode)
    {
        case QO_S_HW_INIT:
        {
            u1RetVal = QoSHwInit ();
            break;
        }
        case QO_S_HW_MAP_CLASS_TO_POLICY:
        {
            tQosxNpWrQoSHwMapClassToPolicy *pEntry = NULL;
            pEntry = &pQosxNpModInfo->QosxNpQoSHwMapClassToPolicy;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                QoSHwMapClassToPolicy (pEntry->pClsMapEntry,
                                       pEntry->pPlyMapEntry,
                                       pEntry->pInProActEntry,
                                       pEntry->pOutProActEntry,
                                       pEntry->pMeterEntry, pEntry->u1Flag);
            break;
        }
#ifdef MBSM_WANTED
        case QO_S_MBSM_HW_SCHEDULER_ADD:
        {
            tQosxNpWrQoSMbsmHwSchedulerAdd *pEntry = NULL;
            pEntry = &pQosxNpModInfo->QosxNpQoSMbsmHwSchedulerAdd;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                QoSMbsmHwSchedulerAdd (pEntry->pSchedEntry, pEntry->pSlotInfo);
            break;
        }
        case QO_S_MBSM_HW_METER_CREATE:
        {
            tQosxNpWrQoSMbsmHwMeterCreate *pEntry = NULL;
            pEntry = &pQosxNpModInfo->QosxNpQoSMbsmHwMeterCreate;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                QoSMbsmHwMeterCreate (pEntry->pMeterEntry, pEntry->pSlotInfo);
            break;
        }

#endif
        case QO_S_HW_UPDATE_POLICY_MAP_FOR_CLASS:
        {
            tQosxNpWrQoSHwUpdatePolicyMapForClass *pEntry = NULL;
            pEntry = &pQosxNpModInfo->QosxNpQoSHwUpdatePolicyMapForClass;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                QoSHwUpdatePolicyMapForClass (pEntry->pClsMapEntry,
                                              pEntry->pPlyMapEntry,
                                              pEntry->pInProActEntry,
                                              pEntry->pOutProActEntry,
                                              pEntry->pMeterEntry,
                                              pEntry->u1Flag);
            break;
        }
        case QO_S_HW_UNMAP_CLASS_FROM_POLICY:
        {
            tQosxNpWrQoSHwUnmapClassFromPolicy *pEntry = NULL;
            pEntry = &pQosxNpModInfo->QosxNpQoSHwUnmapClassFromPolicy;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                QoSHwUnmapClassFromPolicy (pEntry->pClsMapEntry,
                                           pEntry->pPlyMapEntry,
                                           pEntry->pMeterEntry, pEntry->u1Flag);
            break;
        }
        case QO_S_HW_DELETE_CLASS_MAP_ENTRY:
        {
            tQosxNpWrQoSHwDeleteClassMapEntry *pEntry = NULL;
            pEntry = &pQosxNpModInfo->QosxNpQoSHwDeleteClassMapEntry;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal = QoSHwDeleteClassMapEntry (pEntry->pClsMapEntry);
            break;
        }
        case QO_S_HW_METER_CREATE:
        {
            tQosxNpWrQoSHwMeterCreate *pEntry = NULL;
            pEntry = &pQosxNpModInfo->QosxNpQoSHwMeterCreate;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal = QoSHwMeterCreate (pEntry->pMeterEntry);
            break;
        }
        case QO_S_HW_METER_DELETE:
        {
            tQosxNpWrQoSHwMeterDelete *pEntry = NULL;
            pEntry = &pQosxNpModInfo->QosxNpQoSHwMeterDelete;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal = QoSHwMeterDelete (pEntry->i4MeterId);
            break;
        }

        case QO_S_HW_METER_STAT_UPDATE:
        {
            tQosxNpWrQoSHwMeterStatsUpdate *pEntry = NULL;
            pEntry = &pQosxNpModInfo->QosxNpQoSHwMeterStatsUpdate;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal = QoSHwMeterStatsUpdate (pEntry->u4HwFilterId,pEntry->pMeterEntry);
            break;
        }

        case QO_S_HW_METER_STAT_CLEAR:
        {
            tQosxNpWrQoSHwMeterClear *pEntry = NULL;
            pEntry = &pQosxNpModInfo->QosxNpQoSHwMeterStatsClear;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal = QoSHwMeterStatsClear (pEntry->i4MeterId);
            break;
        }

        case QO_S_HW_SCHEDULER_ADD:
        {
            tQosxNpWrQoSHwSchedulerAdd *pEntry = NULL;
            pEntry = &pQosxNpModInfo->QosxNpQoSHwSchedulerAdd;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal = QoSHwSchedulerAdd (pEntry->pSchedEntry);
            break;
        }
        case QO_S_HW_SCHEDULER_UPDATE_PARAMS:
        {
            tQosxNpWrQoSHwSchedulerUpdateParams *pEntry = NULL;
            pEntry = &pQosxNpModInfo->QosxNpQoSHwSchedulerUpdateParams;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal = QoSHwSchedulerUpdateParams (pEntry->pSchedEntry);
            break;
        }
        case QO_S_HW_SCHEDULER_DELETE:
        {
            tQosxNpWrQoSHwSchedulerDelete *pEntry = NULL;
            pEntry = &pQosxNpModInfo->QosxNpQoSHwSchedulerDelete;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                QoSHwSchedulerDelete (pEntry->i4IfIndex, pEntry->u4SchedId);
            break;
        }
        case QO_S_HW_QUEUE_CREATE:
        {
            tQosxNpWrQoSHwQueueCreate *pEntry = NULL;
            pEntry = &pQosxNpModInfo->QosxNpQoSHwQueueCreate;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                QoSHwQueueCreate (pEntry->i4IfIndex, pEntry->u4QId,
                                  pEntry->pQEntry, pEntry->pQTypeEntry,
                                  pEntry->papRDCfgEntry, pEntry->i2HL);
            break;
        }
        case QO_S_HW_QUEUE_DELETE:
        {
            tQosxNpWrQoSHwQueueDelete *pEntry = NULL;
            pEntry = &pQosxNpModInfo->QosxNpQoSHwQueueDelete;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal = QoSHwQueueDelete (pEntry->i4IfIndex, pEntry->u4Id);
            break;
        }
        case QO_S_HW_MAP_CLASS_TO_QUEUE:
        {
            tQosxNpWrQoSHwMapClassToQueue *pEntry = NULL;
            pEntry = &pQosxNpModInfo->QosxNpQoSHwMapClassToQueue;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                QoSHwMapClassToQueue (pEntry->i4IfIndex, pEntry->i4ClsOrPriType,
                                      pEntry->u4ClsOrPri, pEntry->u4QId,
                                      pEntry->u1Flag);
            break;
        }
        case QO_S_HW_MAP_CLASS_TO_QUEUE_ID:
        {
            tQosxNpWrQoSHwMapClassToQueueId *pEntry = NULL;
            pEntry = &pQosxNpModInfo->QosxNpQoSHwMapClassToQueueId;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                QoSHwMapClassToQueueId (pEntry->pClsMapEntry, pEntry->i4IfIndex,
                                        pEntry->i4ClsOrPriType,
                                        pEntry->u4ClsOrPri, pEntry->u4QId,
                                        pEntry->u1Flag);
            break;
        }
        case QO_S_HW_SCHEDULER_HIERARCHY_MAP:
        {
            tQosxNpWrQoSHwSchedulerHierarchyMap *pEntry = NULL;
            pEntry = &pQosxNpModInfo->QosxNpQoSHwSchedulerHierarchyMap;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                QoSHwSchedulerHierarchyMap (pEntry->i4IfIndex,
                                            pEntry->u4SchedId,
                                            pEntry->u2Sweight,
                                            pEntry->u1Spriority,
                                            pEntry->u4NextSchedId,
                                            pEntry->u4NextQId, pEntry->i2HL,
                                            pEntry->u1Flag);
            break;
        }
        case QO_S_HW_SET_DEF_USER_PRIORITY:
        {
            tQosxNpWrQoSHwSetDefUserPriority *pEntry = NULL;
            pEntry = &pQosxNpModInfo->QosxNpQoSHwSetDefUserPriority;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                QoSHwSetDefUserPriority (pEntry->i4Port, pEntry->i4DefPriority);
            break;
        }
        case QO_S_HW_GET_METER_STATS:
        {
            tQosxNpWrQoSHwGetMeterStats *pEntry = NULL;
            pEntry = &pQosxNpModInfo->QosxNpQoSHwGetMeterStats;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                QoSHwGetMeterStats (pEntry->u4MeterId, pEntry->u4StatsType,
                                    pEntry->pu8MeterStatsCounter);
            break;
        }
        case QO_S_HW_GET_CO_S_Q_STATS:
        {
            tQosxNpWrQoSHwGetCoSQStats *pEntry = NULL;
            pEntry = &pQosxNpModInfo->QosxNpQoSHwGetCoSQStats;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                QoSHwGetCoSQStats (pEntry->i4IfIndex, pEntry->u4QId,
                                   pEntry->u4StatsType,
                                   pEntry->pu8CoSQStatsCounter);
            break;
        }
        case FS_QOS_HW_CONFIG_PFC:
        {
            tQosxNpWrFsQosHwConfigPfc *pEntry = NULL;
            pEntry = &pQosxNpModInfo->QosxNpFsQosHwConfigPfc;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal = FsQosHwConfigPfc (pEntry->pQosPfcHwEntry);
            break;
        }
        case QO_S_HW_GET_COUNT_ACT_STATS:
        {
            tQosxNpWrQoSHwGetCountActStats *pEntry = NULL;
            pEntry = &pQosxNpModInfo->QosxNpQoSHwGetCountActStats;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                QoSHwGetCountActStats (pEntry->u4DiffServId,
                                       pEntry->u4StatsType,
                                       pEntry->pu8RetValDiffServCounter);
            break;
        }
        case QO_S_HW_GET_ALG_DROP_STATS:
        {
            tQosxNpWrQoSHwGetAlgDropStats *pEntry = NULL;
            pEntry = &pQosxNpModInfo->QosxNpQoSHwGetAlgDropStats;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                QoSHwGetAlgDropStats (pEntry->u4DiffServId, pEntry->u4StatsType,
                                      pEntry->pu8RetValDiffServCounter);
            break;
        }
        case QO_S_HW_GET_RANDOM_DROP_STATS:
        {
            tQosxNpWrQoSHwGetRandomDropStats *pEntry = NULL;
            pEntry = &pQosxNpModInfo->QosxNpQoSHwGetRandomDropStats;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                QoSHwGetRandomDropStats (pEntry->u4DiffServId,
                                         pEntry->u4StatsType,
                                         pEntry->pu8RetValDiffServCounter);
            break;
        }
        case QO_S_HW_SET_PBIT_PREFERENCE_OVER_DSCP:
        {
            tQosxNpWrQoSHwSetPbitPreferenceOverDscp *pEntry = NULL;
            pEntry = &pQosxNpModInfo->QosxNpQoSHwSetPbitPreferenceOverDscp;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                QoSHwSetPbitPreferenceOverDscp (pEntry->i4Port,
                                                pEntry->i4PbitPref);
            break;
        }
        case QO_S_HW_SET_CPU_RATE_LIMIT:
        {
            tQosxNpWrQoSHwSetCpuRateLimit *pEntry = NULL;
            pEntry = &pQosxNpModInfo->QosxNpQoSHwSetCpuRateLimit;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                QoSHwSetCpuRateLimit (pEntry->i4CpuQueueId, pEntry->u4MinRate,
                                      pEntry->u4MaxRate);
            break;
        }
        case QO_S_HW_MAP_CLASSTO_PRI_MAP:
        {
            tQosxNpWrQoSHwMapClasstoPriMap *pEntry = NULL;
            pEntry = &pQosxNpModInfo->QosxNpQoSHwMapClasstoPriMap;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal = QoSHwMapClasstoPriMap (pEntry->pQosClassToPriMapEntry);
            break;
        }
        case QOS_HW_MAP_CLASS_TO_INT_PRIORITY:
        {
            tQosxNpWrQosHwMapClassToIntPriority *pEntry = NULL;
            pEntry = &pQosxNpModInfo->QosxNpQosHwMapClassToIntPriority;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                QosHwMapClassToIntPriority (pEntry->pQosClassToIntPriEntry);
            break;
        }

        case QO_S_HW_GET_DSCP_Q_MAP:
        {
            tQosxNpWrQosHwGetDscpQueueMap *pEntry = NULL;
            pEntry = &pQosxNpModInfo->QosxNpQosHwGetDscpQueueMap;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal = QosHwGetDscpQueueMap (pEntry->i4QosDscpVal ,pEntry->i4QosQueID);
            break;
        }
        case QO_S_HW_SET_TRAF_CLASS_TO_PCP:
        {
            tQosxNpWrQosHwSetTrafficClassToPcp *pEntry = NULL;
            pEntry = &pQosxNpModInfo->QosxNpQosHwSetTrafficClassToPcp;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal = QoSHwSetTrafficClassToPcp (pEntry->i4IfIndex, pEntry->i4UserPriority, pEntry->i4TrafficClass);
            break;
        }

        case QO_S_HW_SET_VLAN_QUEUING_STATUS:
        {
            tQosxNpWrQoSHwSetVlanQueuingStatus *pEntry = NULL;
            pEntry = &pQosxNpModInfo->QosxNpQoSHwSetVlanQueuingStatus;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                QoSHwSetVlanQueuingStatus (pEntry->i4IfIndex,
                                           pEntry->pQEntrySubscriber);
            break;
        }
        case FS_QOS_HW_GET_PFC_STATS:
        {
            tQosxNpWrFsQosHwGetPfcStats *pEntry = NULL;
            pEntry = &pQosxNpModInfo->QosxNpFsQosHwGetPfcStats;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal = FsQosHwGetPfcStats (pEntry->pQosPfcHwStats);
            break;
        }
#ifdef MBSM_WANTED
        case QO_S_MBSM_HW_UPDATE_POLICY_MAP_FOR_CLASS:
        {
            tQosxNpWrQoSMbsmHwUpdatePolicyMapForClass *pEntry = NULL;
            pEntry = &pQosxNpModInfo->QosxNpQoSMbsmHwUpdatePolicyMapForClass;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                QoSMbsmHwUpdatePolicyMapForClass (pEntry->pClsMapEntry,
                                                  pEntry->pPlyMapEntry,
                                                  pEntry->pInProActEntry,
                                                  pEntry->pOutProActEntry,
                                                  pEntry->pMeterEntry,
                                                  pEntry->pSlotInfo);
            break;
        }
        case QO_S_MBSM_HW_QUEUE_CREATE:
        {
            tQosxNpWrQoSMbsmHwQueueCreate *pEntry = NULL;
            pEntry = &pQosxNpModInfo->QosxNpQoSMbsmHwQueueCreate;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                QoSMbsmHwQueueCreate (pEntry->i4IfIndex, pEntry->u4QId,
                                      pEntry->pQEntry, pEntry->pQTypeEntry,
                                      pEntry->papRDCfgEntry, pEntry->i2HL,
                                      pEntry->pSlotInfo);
            break;
        }
        case QO_S_MBSM_HW_MAP_CLASS_TO_QUEUE:
        {
            tQosxNpWrQoSMbsmHwMapClassToQueue *pEntry = NULL;
            pEntry = &pQosxNpModInfo->QosxNpQoSMbsmHwMapClassToQueue;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                QoSMbsmHwMapClassToQueue (pEntry->i4IfIndex,
                                          pEntry->i4ClsOrPriType,
                                          pEntry->u4ClsOrPri, pEntry->u4QId,
                                          pEntry->u1Flag, pEntry->pSlotInfo);
            break;
        }
        case QO_S_MBSM_HW_MAP_CLASS_TO_QUEUE_ID:
        {
            tQosxNpWrQoSMbsmHwMapClassToQueueId *pEntry = NULL;
            pEntry = &pQosxNpModInfo->QosxNpQoSMbsmHwMapClassToQueueId;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                QoSMbsmHwMapClassToQueueId (pEntry->pClsMapEntry,
                                            pEntry->i4IfIndex,
                                            pEntry->i4ClsOrPriType,
                                            pEntry->u4ClsOrPri, pEntry->u4QId,
                                            pEntry->u1Flag, pEntry->pSlotInfo);
            break;
        }
        case FS_QOS_MBSM_HW_CONFIG_PFC:
        {
            tQosxNpWrFsQosMbsmHwConfigPfc *pEntry = NULL;
            pEntry = &pQosxNpModInfo->QosxNpFsQosMbsmHwConfigPfc;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                FsQosMbsmHwConfigPfc (pEntry->pQosPfcHwEntry,
                                      pEntry->pSlotInfo);
            break;
        }
        case QO_S_MBSM_HW_INIT:
        {
            tQosxNpWrQoSMbsmHwInit *pEntry = NULL;
            pEntry = &pQosxNpModInfo->QosxNpQoSMbsmHwInit;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal = QoSMbsmHwInit (pEntry->pSlotInfo);
            break;
        }
        case QO_S_MBSM_HW_SET_CPU_RATE_LIMIT:
        {
            tQosxNpWrQoSMbsmHwSetCpuRateLimit *pEntry = NULL;
            pEntry = &pQosxNpModInfo->QosxNpQoSMbsmHwSetCpuRateLimit;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                QoSMbsmHwSetCpuRateLimit (pEntry->i4CpuQueueId,
                                          pEntry->u4MinRate, pEntry->u4MaxRate,
                                          pEntry->pSlotInfo);
            break;
        }
        case QO_S_MBSM_HW_MAP_CLASS_TO_POLICY:
        {
            tQosxNpWrQoSMbsmHwMapClassToPolicy *pEntry = NULL;
            pEntry = &pQosxNpModInfo->QosxNpQoSMbsmHwMapClassToPolicy;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                QoSMbsmHwMapClassToPolicy (pEntry->pClsMapEntry,
                                           pEntry->pPlyMapEntry,
                                           pEntry->pInProActEntry,
                                           pEntry->pOutProActEntry,
                                           pEntry->pMeterEntry, pEntry->u1Flag,
                                           pEntry->pSlotInfo);
            break;
        }
        case QO_S_MBSM_HW_SCHEDULER_HIERARCHY_MAP:
        {
            tQosxNpWrQoSMbsmHwSchedulerHierarchyMap *pEntry = NULL;
            pEntry = &pQosxNpModInfo->QosxNpQoSMbsmHwSchedulerHierarchyMap;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                QoSMbsmHwSchedulerHierarchyMap (pEntry->i4IfIndex,
                                                pEntry->u4SchedId,
                                                pEntry->u2Sweight,
                                                pEntry->u1Spriority,
                                                pEntry->u4NextSchedId,
                                                pEntry->u4NextQId, pEntry->i2HL,
                                                pEntry->u1Flag,
                                                pEntry->pSlotInfo);
            break;
        }
        case QO_S_MBSM_HW_SET_DEF_USER_PRIORITY:
        {
            tQosxNpWrQoSMbsmHwSetDefUserPriority *pEntry = NULL;
            pEntry = &pQosxNpModInfo->QosxNpQoSMbsmHwSetDefUserPriority;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                QoSMbsmHwSetDefUserPriority (pEntry->i4Port,
                                             pEntry->i4DefPriority,
                                             pEntry->pSlotInfo);
            break;
        }
        case QO_S_MBSM_HW_SET_PBIT_PREFERENCE_OVER_DSCP:
        {
            tQosxNpWrQoSMbsmHwSetPbitPreferenceOverDscp *pEntry = NULL;
            pEntry = &pQosxNpModInfo->QosxNpQoSMbsmHwSetPbitPreferenceOverDscp;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                QoSMbsmHwSetPbitPreferenceOverDscp (pEntry->i4Port,
                                                    pEntry->i4PbitPref,
                                                    pEntry->pSlotInfo);
            break;
        }
        case QO_S_MBSM_HW_MAP_CLASSTO_PRI_MAP:
        {
            tQosxNpWrQoSMbsmHwMapClasstoPriMap *pEntry = NULL;
            pEntry = &pQosxNpModInfo->QosxNpQoSMbsmHwMapClasstoPriMap;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                QoSMbsmHwMapClasstoPriMap (pEntry->pQosClassToPriMapEntry,
                                           pEntry->pSlotInfo);
            break;
        }
        case QOS_MBSM_HW_MAP_CLASS_TO_INT_PRIORITY:
        {
            tQosxNpWrQosMbsmHwMapClassToIntPriority *pEntry = NULL;
            pEntry = &pQosxNpModInfo->QosxNpQosMbsmHwMapClassToIntPriority;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                QosMbsmHwMapClassToIntPriority (pEntry->pQosClassToIntPriEntry,
                                                pEntry->pSlotInfo);
            break;
        }
        case QO_S_MBSM_HW_SCHEDULER_UPDATE_PARAMS:
        {
            tQosxNpWrQoSMbsmHwSchedulerUpdateParams *pEntry = NULL;
            pEntry = &pQosxNpModInfo->QosxNpQoSMbsmHwSchedulerUpdateParams;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                QoSMbsmHwSchedulerUpdateParams (pEntry->pSchedEntry,
                                                pEntry->pSlotInfo);
            break;
        }

#endif /* MBSM_WANTED */
        default:
            u1RetVal = FNP_FAILURE;
            break;

    }                            /* switch */

    return (u1RetVal);
}

#endif /* __QOSX_NP_WR_C__ */
