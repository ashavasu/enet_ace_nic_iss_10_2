/* $Id: qosxmbsm.c,v 1.13 2014/11/19 11:04:00 siva Exp $ */

#include "qosinc.h"

/*****************************************************************************/
/* Function Name      : QosxProcessMbsmMessage                               */
/*                                                                           */
/* Description        : This function constructs the QOSX Q Mesg and calls   */
/*                      the  Q Message event to process this Mesg.           */
/*                                                                           */
/*                      This function will be called from the MBSM module    */
/*                      when an indication for the Card Insertion is         */
/*                      received.                                            */
/*                                                                           */
/* Input(s)           :  tMbsmProtoMsg - Structure containing the SlotInfo,  */
/*                                       PortInfo and the Protocol Id.       */
/*                    :  i4Event       - Event to be processed . Currently it*/
/*                                       is MBSM_MSG_CARD_INSERT event       */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : MBSM_FAILURE / MBSM_SUCCESS                          */
/*****************************************************************************/
INT4
QosxProcessMbsmMessage (tMbsmProtoMsg * pProtoMsg, INT4 i4Event)
{
    tQosQueueMsg       *pQosMsg = NULL;
    tMbsmProtoAckMsg    MbsmProtoAckMsg;

    if (pProtoMsg == NULL)
    {
        return MBSM_FAILURE;
    }
    if (gQoSGlobalInfo.eSysControl != QOS_SYS_CNTL_START)
    {
        MEMSET (&MbsmProtoAckMsg, 0, sizeof (tMbsmProtoAckMsg));
        MbsmProtoAckMsg.i4ProtoCookie = pProtoMsg->i4ProtoCookie;
        MbsmProtoAckMsg.i4SlotId = pProtoMsg->MbsmSlotInfo.i4SlotId;
        MbsmProtoAckMsg.i4RetStatus = MBSM_SUCCESS;
        MbsmSendAckFromProto (&MbsmProtoAckMsg);
        return MBSM_SUCCESS;
    }
    if ((pQosMsg = (tQosQueueMsg *) QOS_QMSG_ALLOC_MEMBLK) == NULL)
    {
        QOS_TRC (OS_RESOURCE_TRC | ALL_FAILURE_TRC,
                 "Memory allocation failed for queue message\r\n");
        return MBSM_FAILURE;
    }

    MEMSET (pQosMsg, 0, sizeof (tQosQueueMsg));
    if (!(pQosMsg->pMbsmProtoMsg =
          MEM_MALLOC (sizeof (tMbsmProtoMsg), tMbsmProtoMsg)))
    {
        QOS_RELEASE_QMSG_MEM_BLOCK (pQosMsg);
        return MBSM_FAILURE;
    }
    pQosMsg->u2MsgType = (UINT4) i4Event;
    MEMCPY (pQosMsg->pMbsmProtoMsg, pProtoMsg, sizeof (tMbsmProtoMsg));
    if (QOS_SEND_TO_QUEUE (QOS_MSG_QUEUE_ID, (UINT1 *) &pQosMsg,
                           QOS_DEF_MSG_LEN) == OSIX_FAILURE)
    {
        QOS_TRC_ARG1 (OS_RESOURCE_TRC, "%s : Send To QOS Queue failed,"
                      "releasing the queue message for port create indication.\r\n",
                      __FUNCTION__);
        if (pQosMsg->pMbsmProtoMsg != NULL)
        {
            MEM_FREE (pQosMsg->pMbsmProtoMsg);
        }
        QOS_RELEASE_QMSG_MEM_BLOCK (pQosMsg);
        return MBSM_FAILURE;
    }
    if (QOS_SEND_EVENT (QOS_TASK_ID, QOS_MSG_EVENT) != OSIX_SUCCESS)
    {
        QOS_TRC (OS_RESOURCE_TRC | ALL_FAILURE_TRC,
                 "Sending QOS_MSG_EVENT to QOS Task failed\r\n");
        if (pQosMsg->pMbsmProtoMsg != NULL)
        {
            MEM_FREE (pQosMsg->pMbsmProtoMsg);
        }
        QOS_RELEASE_QMSG_MEM_BLOCK (pQosMsg);
        return MBSM_FAILURE;
    }
    return MBSM_SUCCESS;
}

/****************************************************************************
 *                                                                           
 *     FUNCTION NAME    : QosMbsmProgramHw               
 *                                                                           
 *     DESCRIPTION      : This function scans required tables in QOS  
 *                        and programs hardware.    
 *                                                                           
 *     INPUT            :                              
 *                          
 *                                                                           
 *     OUTPUT           : None                                               
 *                                                                           
 *     RETURNS          : None                            
 *                                                                           
 *****************************************************************************/
VOID
QosMbsmProgramHw (tMbsmProtoMsg * pProtoMsg)
{
    tMbsmProtoAckMsg    MbsmProtoAckMsg;
    tMbsmSlotInfo      *pSlotInfo;

    pSlotInfo = &(pProtoMsg->MbsmSlotInfo);
    if (pProtoMsg != NULL)
    {
        if (OSIX_FALSE == MBSM_SLOT_INFO_ISPORTMSG (pSlotInfo))
        {
            QosxQoSMbsmHwInit (&pProtoMsg->MbsmSlotInfo);
            if (QOS_FAILURE ==
                QosMbsmScanAndProgramSchedTblInHw (&pProtoMsg->MbsmPortInfo,
                                                   &pProtoMsg->MbsmSlotInfo))
            {
                QOS_TRC_ARG1 (MGMT_TRC, "In %s Remote NP update failed",
                              "QosMbsmScanAndProgramSchedTblInHw");
            }

            if (QOS_SUCCESS !=
                QoSMbsmSchedulerHierarchyMap ((&pProtoMsg->MbsmSlotInfo),
                                              (&pProtoMsg->MbsmPortInfo)))
            {
                QOS_TRC_ARG1 (MGMT_TRC, "In %s Remote NP update failed",
                              "QoSMbsmSchedulerHierarchyMap");
            }

            QosMbsmScanAndProgramIngressTblInHw (&pProtoMsg->MbsmSlotInfo);
            QosMbsmScanAndProgramEgressTblInHw (&pProtoMsg->MbsmSlotInfo);
            if (QOS_SUCCESS !=
                QoSMbsmSchedulerUpdateParams ((&pProtoMsg->MbsmSlotInfo),
                                              (&pProtoMsg->MbsmPortInfo)))
            {
                QOS_TRC_ARG1 (MGMT_TRC, "In %s Remote NP update failed",
                              "QoSMbsmSchedulerUpdateParams");
            }

            QosMbsmConfigPfc (&pProtoMsg->MbsmPortInfo,
                              &pProtoMsg->MbsmSlotInfo);
            if (QOS_FAILURE ==
                QosMbsmProgramCpuEntries (&pProtoMsg->MbsmSlotInfo))
            {
                QOS_TRC_ARG1 (MGMT_TRC, "In %s Remote NP update failed",
                              "QosMbsmProgramCpuEntries");

            }

            if (QOS_FAILURE ==
                QoSMbsmProgramMeterEntries (&pProtoMsg->MbsmSlotInfo))
            {
                QOS_TRC_ARG1 (MGMT_TRC, "In %s Remote NP update failed",
                              "QoSMbsmProgramMeterEntries");
            }
            if (QOS_FAILURE !=
                QosMbsmMapClasstoPriMap (&pProtoMsg->MbsmSlotInfo,
                                         &pProtoMsg->MbsmPortInfo))

            {
                QOS_TRC_ARG1 (MGMT_TRC, "In %s Remote NP update failed",
                              "QosMbsmMapClasstoPriMap");
            }
            if (QOS_SUCCESS !=
                QosMbsmProgramDefUserPriority ((&pProtoMsg->MbsmSlotInfo),
                                               (&pProtoMsg->MbsmPortInfo)))
            {
                QOS_TRC_ARG1 (MGMT_TRC, "In %s Remote NP update failed",
                              "wQosMbsmProgramDefUserPriority");
            }
            if (QOS_SUCCESS !=
                QoSMbsmMapClassToIntPriority (&pProtoMsg->MbsmSlotInfo))
            {
                QOS_TRC_ARG1 (MGMT_TRC, "In %s Remote NP update failed",
                              "QoSMbsmMapClassToIntPriority");
            }

            if (QOS_SUCCESS !=
                QosMbsmHwSetPbitPreferenceOverDscp ((&pProtoMsg->MbsmSlotInfo),
                                                    (&pProtoMsg->MbsmPortInfo)))
            {
                QOS_TRC_ARG1 (MGMT_TRC, "In %s Remote NP update failed",
                              "QosMbsmHwSetPbitPreferenceOverDscp");
            }
            if (QOS_SUCCESS !=
                QosMbsmMapClassToPolicy ((&pProtoMsg->MbsmSlotInfo),
                                         (&pProtoMsg->MbsmPortInfo)))
            {
                QOS_TRC_ARG1 (MGMT_TRC, "In %s Remote NP update failed",
                              "QosMbsmMapClassToPolicy");
            }

        }
    }
    MEMSET (&MbsmProtoAckMsg, 0, sizeof (tMbsmProtoAckMsg));
    MbsmProtoAckMsg.i4ProtoCookie = pProtoMsg->i4ProtoCookie;
    MbsmProtoAckMsg.i4SlotId = pProtoMsg->MbsmSlotInfo.i4SlotId;
    MbsmProtoAckMsg.i4RetStatus = MBSM_SUCCESS;
    MbsmSendAckFromProto (&MbsmProtoAckMsg);
    return;
}

/****************************************************************************
 *                                                                           
 *     FUNCTION NAME    : QosMbsmScanAndProgramIngressTblInHw               
 *                                                                           
 *     DESCRIPTION      : This function scans required Ingress tables in QOS  
 *                        and programs hardware.    
 *                                                                           
 *     Input(s)        :  pSlotInfo  - pointer to slot information  
 *                          
 *                                                                           
 *     OUTPUT           : None                                               
 *                                                                           
 *     RETURNS          : None                            
 *                                                                           
 *****************************************************************************/
INT4
QosMbsmScanAndProgramIngressTblInHw (tMbsmSlotInfo * pSlotInfo)
{
    tQoSPolicyMapEntry  NewPolicyMapEntry;
    tQoSInProfileActionEntry NewInProActEntry;
    tQoSOutProfileActionEntry NewOutProActEntry;
    tQoSMeterEntry     *pNewMeterEntry = NULL;
    tQoSMeterNode      *pMeterNode = NULL;
    tQoSClassInfoNode  *pClsInfoNode = NULL;
    tQoSFilterInfoNode *pFltInfoNode = NULL;
    tQoSClassMapEntry  *pNewClassMapEntry = NULL;

    INT4                i4RetStatus = FNP_FAILURE;
    UINT4               u4Idx = 0;
    tQoSPolicyMapNode  *pPlyMapNode = NULL;

    MEMSET (&(NewInProActEntry), 0, (sizeof (tQoSInProfileActionEntry)));
    MEMSET (&(NewOutProActEntry), 0, (sizeof (tQoSOutProfileActionEntry)));
    MEMSET (&(NewPolicyMapEntry), 0, (sizeof (tQoSPolicyMapEntry)));
    if (nmhGetFirstIndexFsQoSPolicyMapTable (&u4Idx) != SNMP_FAILURE)
    {
        do
        {
            pPlyMapNode = QoSUtlGetPolicyMapNode (u4Idx);
            if (pPlyMapNode != NULL)
            {
                /* Meter Table */
                if (pPlyMapNode->u4MeterTableId != 0)
                {
                    pMeterNode =
                        QoSUtlGetMeterNode (pPlyMapNode->u4MeterTableId);

                    if (pMeterNode == NULL)
                    {
                        continue;
                    }

                    pNewMeterEntry =
                        MEM_MALLOC ((sizeof (tQoSMeterEntry)), tQoSMeterEntry);
                    if (pNewMeterEntry == NULL)
                    {
                        continue;
                    }

                    QoSHwWrUtilFillMeterParams (pNewMeterEntry, pMeterNode);

                    /* Fill Out profile Action */
                    QoSHwWrUtilFillOutProParams (&(NewOutProActEntry),
                                                 pPlyMapNode);

                    /* Fill In profile Action */
                    QoSHwWrUtilFillInProParams (&(NewInProActEntry),
                                                pPlyMapNode);
                }

                /* Fill Policy Map Table */
                QoSHwWrUtilFillPolicyParams (&(NewPolicyMapEntry), pPlyMapNode);

                /* This is Interface Specific Policy CLASS is NULL */
                if (pPlyMapNode->u4ClassId == 0)
                {
                    QosxQoSMbsmHwUpdatePolicyMapForClass (pNewClassMapEntry,
                                                          &NewPolicyMapEntry,
                                                          &NewInProActEntry,
                                                          &NewOutProActEntry,
                                                          pNewMeterEntry,
                                                          pSlotInfo);
                    if (pNewMeterEntry != NULL)
                    {
                        MEM_FREE (pNewMeterEntry);
                    }
                    return (QOS_SUCCESS);
                }                /* End of NULL CLASS */
                else
                {
                    /* Get all The Class Map for a given Policy */
                    pClsInfoNode =
                        QoSUtlGetClassInfoNode (pPlyMapNode->u4ClassId);

                    if (pClsInfoNode == NULL)
                    {
                        if (pNewMeterEntry != NULL)
                        {
                            MEM_FREE (pNewMeterEntry);
                        }
                        continue;
                    }

                    /* Scan the Filter list in the ClassInfo */
                    TMO_SLL_Scan (&(pClsInfoNode->SllFltInfo), pFltInfoNode,
                                  tQoSFilterInfoNode *)
                    {

                        /* Fill the Values for that Entry */
                        pNewClassMapEntry =
                            QoSFiltersForClassMapEntry (pFltInfoNode->
                                                        pClsMapNode);

                        if (pNewClassMapEntry == NULL)
                        {
                            QOS_TRC_ARG1 (MGMT_TRC, "In %s : ERROR: "
                                          "QoSFiltersForClassMapEntry() Returns FAILURE."
                                          " \r\n", __FUNCTION__);

                            if (pNewMeterEntry != NULL)
                            {
                                MEM_FREE (pNewMeterEntry);
                            }
                            continue;
                        }

                        /* Call the NP Function */
                        i4RetStatus =
                            QosxQoSMbsmHwUpdatePolicyMapForClass
                            (pNewClassMapEntry, &(NewPolicyMapEntry),
                             &(NewInProActEntry), &(NewOutProActEntry),
                             pNewMeterEntry, pSlotInfo);

                        QoSUtlClassMapEntryMemFree (pNewClassMapEntry);

                        if (i4RetStatus == FNP_FAILURE)
                        {
                            QOS_TRC_ARG1 (MGMT_TRC,
                                          "In %s : ERROR: QoSHwMapClassToPolicy "
                                          "() Returns FAILURE. \r\n",
                                          __FUNCTION__);

                            if (pNewMeterEntry != NULL)
                            {
                                MEM_FREE (pNewMeterEntry);
                            }
                            return (QOS_FAILURE);
                        }

                    }            /* End of Scan */
                }
                if (pNewMeterEntry != NULL)
                {
                    MEM_FREE (pNewMeterEntry);
                }
            }
        }
        while ((nmhGetNextIndexFsQoSPolicyMapTable (u4Idx, &u4Idx)
                == SNMP_SUCCESS));

    }

    return (QOS_SUCCESS);

}

/****************************************************************************
 *                                                                           
 *     FUNCTION NAME    : QosMbsmScanAndProgramEgressTblInHw               
 *                                                                           
 *     DESCRIPTION      : This function scans required Egress tables in QOS  
 *                        and programs hardware.    
 *                                                                           
 *     INPUT            : CliHandle - CliContext ID                             
 *                          
 *                                                                           
 *     OUTPUT           : None                                               
 *                                                                           
 *     RETURNS          : None                            
 *                                                                           
 *****************************************************************************/
INT4
QosMbsmScanAndProgramEgressTblInHw (tMbsmSlotInfo * pSlotInfo)
{
    INT4                i4IfIdx = 0;
    INT4                i4RetStatus = FNP_FAILURE;
    INT4                i4ExitOnFailure = QOS_FALSE;
    INT4                i4RegenPriType = 0;
    INT2                i2HL = 0;
    UINT4               u4QIdx = 0;
    UINT4               u4CLASS = 0;
    UINT4               u4ClsOrPri = 0;
    UINT4               u4QId = 0;

    tQoSQEntry         *pQEntry = NULL;
    tQoSQtypeEntry     *pQTypeEntry = NULL;
    tQoSREDCfgEntry    *apRDCfgEntry[6] =
        { NULL, NULL, NULL, NULL, NULL, NULL };
    tQoSClassInfoNode  *pClsInfoNode = NULL;
    tQoSFilterInfoNode *pFltInfoNode = NULL;
    tQoSClassMapEntry  *pNewClassMapEntry = NULL;
    tQoSQNode          *pQNode = NULL;
    tQoSSchedNode      *pSchedNode = NULL;

    if (nmhGetFirstIndexFsQoSQMapTable (&i4IfIdx, &u4CLASS, &i4RegenPriType,
                                        &u4ClsOrPri) != SNMP_FAILURE)
    {
        do
        {
            if (u4CLASS != 0)
            {
                i4RegenPriType = u4CLASS;

                pClsInfoNode = QoSUtlGetClassInfoNode (u4CLASS);

                if (pClsInfoNode == NULL)
                {
                    continue;
                }

                /* Scan the Filter list in the ClassInfo */
                TMO_SLL_Scan (&(pClsInfoNode->SllFltInfo), pFltInfoNode,
                              tQoSFilterInfoNode *)
                {

                    /* Fill the Values for that Entry */
                    pNewClassMapEntry =
                        QoSFiltersForClassMapEntry (pFltInfoNode->pClsMapNode);

                    if (pNewClassMapEntry == NULL)
                    {
                        QOS_TRC_ARG1 (MGMT_TRC, "In %s : ERROR: "
                                      "QoSFiltersForClassMapEntry() Returns FAILURE."
                                      " \r\n", __FUNCTION__);
                        continue;
                    }
                    if (nmhGetFsQoSQMapQId (i4IfIdx, u4CLASS, i4RegenPriType,
                                            u4ClsOrPri, &u4QId) != SNMP_FAILURE)
                    {
                        QoSUtlClassMapEntryMemFree (pNewClassMapEntry);
                        continue;
                    }
                    i4RetStatus =
                        QosxQoSMbsmHwMapClassToQueue (i4IfIdx, i4RegenPriType,
                                                      u4ClsOrPri, u4QId,
                                                      QOS_QMAP_ADD, pSlotInfo);
                    /* In case the L2/L3 filter Hw Handle is needed then this NPAPI will
                       be called to pass the information */

                    i4RetStatus =
                        QosxQoSMbsmHwMapClassToQueueId (pNewClassMapEntry,
                                                        i4IfIdx, i4RegenPriType,
                                                        u4ClsOrPri, u4QId,
                                                        QOS_QMAP_ADD,
                                                        pSlotInfo);

                    QoSUtlClassMapEntryMemFree (pNewClassMapEntry);

                    if (i4RetStatus == FNP_FAILURE)
                    {
                        QOS_TRC_ARG1 (MGMT_TRC,
                                      "In %s : ERROR: QoSHwMapClassToQueue "
                                      "() Returns FAILURE. \r\n", __FUNCTION__);

                        i4ExitOnFailure = QOS_TRUE;
                        break;
                    }
                }
            }
            else
            {
                /*i4RegenPriType = u4ClsOrPri; */
                nmhGetFsQoSQMapQId (i4IfIdx, u4CLASS, i4RegenPriType,
                                    u4ClsOrPri, &u4QId);
                i4RetStatus =
                    QosxQoSMbsmHwMapClassToQueue (i4IfIdx, i4RegenPriType,
                                                  u4ClsOrPri, u4QId,
                                                  QOS_QMAP_ADD, pSlotInfo);
                /* In case the L2/L3 filter Hw Handle is needed then this NPAPI will
                   be called to pass the information */

                i4RetStatus =
                    QosxQoSMbsmHwMapClassToQueueId (NULL, i4IfIdx,
                                                    i4RegenPriType, u4ClsOrPri,
                                                    u4QId, QOS_QMAP_ADD,
                                                    pSlotInfo);
                if (i4RetStatus == FNP_FAILURE)
                {
                    QOS_TRC_ARG1 (MGMT_TRC,
                                  "In %s : ERROR: QoSHwMapClassToQueue () "
                                  " Returns FAILURE. \r\n", __FUNCTION__);
                    i4ExitOnFailure = QOS_TRUE;
                    break;
                }
            }

        }
        while (nmhGetNextIndexFsQoSQMapTable (i4IfIdx, &i4IfIdx,
                                              u4CLASS, &u4CLASS,
                                              i4RegenPriType, &i4RegenPriType,
                                              u4ClsOrPri,
                                              &u4ClsOrPri) == SNMP_SUCCESS);
    }
    i4IfIdx = 0;
    if (nmhGetFirstIndexFsQoSQTable (&i4IfIdx, &u4QIdx) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    do
    {
        /* Process for the IfIndexes mapped to the particular Slot */
        if ((i4IfIdx < (INT4) pSlotInfo->u4StartIfIndex) ||
            (i4IfIdx >
             (INT4) (pSlotInfo->u4StartIfIndex + pSlotInfo->u4NumPorts - 1)))
        {
            continue;
        }

        /* Fill the tQoSSchedulerEntry values */
        i4RetStatus = QoSHwWrUtilFillQParams (i4IfIdx, u4QIdx, &pQEntry,
                                              &pQTypeEntry,
                                              (&(apRDCfgEntry[0])));
        if (i4RetStatus == QOS_FAILURE)
        {
            continue;
        }

        i2HL = pQEntry->pSchedPtr->u1HL;

        pQEntry->i4QosQueueHwId = 0;
        i4RetStatus = QosxQoSMbsmHwQueueCreate (i4IfIdx, u4QIdx,
                                                pQEntry, pQTypeEntry,
                                                apRDCfgEntry, i2HL, pSlotInfo);
        if (pQEntry->i4QosQueueHwId != 0)
        {
            pQNode = QoSUtlGetQNode (i4IfIdx, u4QIdx);
            if (pQNode != NULL)
            {
                /* Fill the Hw ID for the new Queue */
                pQNode->u4QueueHwId = pQEntry->i4QosQueueHwId;
                pQNode->u4SchedulerId = pQEntry->i4QosSchedulerId;

                pSchedNode =
                    QoSUtlGetSchedNode (i4IfIdx, pQEntry->i4QosSchedulerId);
                if (pSchedNode != NULL)
                {
                    /* Fill in the Scheduler HW ID and the number of
                     * children associated to the scheduler in the
                     * scheduler structure */
                    pSchedNode->u4SchedulerHwId =
                        pQEntry->pSchedPtr->u4QosSchedHwId;
                    pSchedNode->u4SchedChildren =
                        pQEntry->pSchedPtr->u4QosSchedChildren;
                }
            }
        }

        QoSHwWrUtilFreeQParams (pQEntry, pQTypeEntry, apRDCfgEntry);

        if (i4RetStatus == FNP_FAILURE)
        {
            QOS_TRC_ARG1 (MGMT_TRC, "In %s : ERROR: QoSHwQueueCreate () "
                          " Returns FAILURE. \r\n", __FUNCTION__);
            return (QOS_FAILURE);
        }

    }
    while (nmhGetNextIndexFsQoSQTable (i4IfIdx, &i4IfIdx, u4QIdx, &u4QIdx)
           == SNMP_SUCCESS);
    if (i4ExitOnFailure == QOS_TRUE)
    {
        return QOS_FAILURE;
    }
    return (QOS_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QosMbsmConfigPfc                                     */
/* Description        : This function is used to program MBSM Pfc NPAPI      */
/* Input(s)           : MbsmPortInfo and MbsmSlotInfo                        */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/* Return Value(s)    : QOS_SUCCESS or QOS_FAILURE                           */
/* Called By          : Qos Utility Function                                 */
/* Calling Function   :                                                      */
/*****************************************************************************/

INT4
QosMbsmConfigPfc (tMbsmPortInfo * pMbsmPortInfo, tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (pMbsmPortInfo);
    UNUSED_PARAM (pSlotInfo);
    return QOS_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : QosMbsmScanAndProgramSchedTblInHw                    */
/* Description        : This function is used to program MBSM scheduler      */
/* Input(s)           : MbsmPortInfo and MbsmSlotInfo                        */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/* Return Value(s)    : QOS_SUCCESS or QOS_FAILURE                           */
/* Called By          : Qos Utility Function                                 */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QosMbsmScanAndProgramSchedTblInHw (tMbsmPortInfo * pPortInfo,
                                   tMbsmSlotInfo * pSlotInfo)
{
    tQoSSchedNode      *pQosSchedNode = NULL;
    tQoSSchedulerEntry *pQosSchedulerEntry = NULL;
    tQoSSchedNode       QosSchedNode;

    MEMSET (&QosSchedNode, 0, sizeof (tQoSSchedNode));

    while (NULL != (pQosSchedNode =
                    RBTreeGetNext (gQoSGlobalInfo.pRbSchedTbl,
                                   &QosSchedNode, NULL)))
    {
        /* Program only for the remote card entries */
        if (OSIX_TRUE == QosMbsmCheckPortRange (pPortInfo,
                                                pQosSchedNode->i4IfIndex))
        {
            /* Fill the tQoSSchedulerEntry values */
            pQosSchedulerEntry =
                QoSHwWrUtilFillSchedEntry (pQosSchedNode->i4IfIndex,
                                           pQosSchedNode->u4Id);

            if (pQosSchedulerEntry == NULL)
            {
                return (QOS_FAILURE);
            }

            pQosSchedulerEntry->u4QosSchedHwId = 0;
            if (FNP_FAILURE == QosxQoSMbsmHwSchedulerAdd (pQosSchedulerEntry,
                                                          pSlotInfo))
            {
                QoSHwWrUtilFreeSchedEntry (pQosSchedulerEntry);
                return QOS_FAILURE;
            }
            QosSchedNode.u4SchedulerHwId = pQosSchedulerEntry->u4QosSchedHwId;
            QoSHwWrUtilFreeSchedEntry (pQosSchedulerEntry);
            pQosSchedulerEntry = NULL;
        }

        QosSchedNode.u4Id = pQosSchedNode->u4Id;
        QosSchedNode.i4IfIndex = pQosSchedNode->i4IfIndex;
    }                            /* End of while */
    return QOS_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : QoSMbsmProgramMeterEntries                           */
/* Description        : This function is used to program MBSM Meter entries  */
/* Input(s)           : MbsmSlotInfo                                         */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/* Return Value(s)    : QOS_SUCCESS or QOS_FAILURE                           */
/* Called By          : Qos Utility Function                                 */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QoSMbsmProgramMeterEntries (tMbsmSlotInfo * pSlotInfo)
{
    tQoSMeterNode      *pMeterNode = NULL;
    tQoSMeterEntry      NewQoSMeterEntry;
    UINT4               u4MeterId = 0;
    UINT4               u4NextMeterId = 0;
    INT4                i4RetStatus = QOS_FAILURE;

    MEMSET (&NewQoSMeterEntry, 0, sizeof (tQoSMeterEntry));

    i4RetStatus = QoSGetNextMeterTblEntryIndex (u4MeterId, &u4NextMeterId);
    if (i4RetStatus == QOS_FAILURE)
    {
        /* The Table is Empty */
        return (QOS_SUCCESS);
    }

    /* Table is Populated */
    do
    {
        pMeterNode = QoSUtlGetMeterNode (u4NextMeterId);
        if (pMeterNode == NULL)
        {
            QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetMeterNode () "
                          " Returns NULL. \r\n", __FUNCTION__);
            return (QOS_FAILURE);
        }

        /* Only Active Node in the Meter Table need to Configure */
        if (pMeterNode->u1Status == ACTIVE)
        {
            /* 1. Add the entry into the HARDWARE. */

            /* Init Entry */
            MEMSET (&(NewQoSMeterEntry), 0, (sizeof (tQoSMeterEntry)));

            /* Fill Params */
            QoSHwWrUtilFillMeterParams (&(NewQoSMeterEntry), pMeterNode);

            /* Call the NP Function */
            i4RetStatus =
                QosxQoSMbsmHwMeterCreate (&(NewQoSMeterEntry), pSlotInfo);

            if (i4RetStatus == FNP_FAILURE)
            {
                QOS_TRC_ARG1 (MGMT_TRC, "In %s : ERROR: QoSHwMeterCreate () "
                              " Returns FAILURE. \r\n", __FUNCTION__);

                return (QOS_FAILURE);
            }

            if (i4RetStatus == QOS_FAILURE)
            {
                QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSHwWrMeterCreate () "
                              " Returns FAILURE. \r\n", __FUNCTION__);
                return (QOS_FAILURE);
            }
        }                        /* End of if */

        u4MeterId = u4NextMeterId;

        i4RetStatus = QoSGetNextMeterTblEntryIndex (u4MeterId, &u4NextMeterId);
    }
    while (i4RetStatus == SNMP_SUCCESS);

    return (QOS_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QosMbsmCheckPortRange                                */
/* Description        : This function is used to check if the input ifIndex  */
/*                      falls within the range given in pPortInfo.           */
/* Input(s)           : MbsmPortInfo and u4IfIndex                           */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/* Return Value(s)    : QOS_TRUE or QOS_FALSE                                */
/* Called By          : Qos Utility Function                                 */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QosMbsmCheckPortRange (tMbsmPortInfo * pPortInfo, UINT4 u4IfIndex)
{
    UINT4               u4StartPortIndex = 0;
    UINT4               u4EndPortIndex = 0;

    u4StartPortIndex = MBSM_PORT_INFO_STARTINDEX (pPortInfo);
    u4EndPortIndex = MBSM_PORT_INFO_STARTINDEX (pPortInfo) +
        MBSM_PORT_INFO_PORTCOUNT (pPortInfo);

    if ((u4IfIndex >= u4StartPortIndex) && (u4IfIndex <= u4EndPortIndex))
    {
        return OSIX_TRUE;
    }

    return OSIX_FALSE;
}

/*****************************************************************************/
/* Function Name      : QosMbsmMapClasstoPriMap                              */
/* Description        : This function is used to Program the CPU entries QOSX*/
/* Input(s)           : MbsmSlotInfo                                         */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/* Return Value(s)    : QOS_SUCCESS or QOS_FAILURE                           */
/*****************************************************************************/
INT4
QosMbsmMapClasstoPriMap (tMbsmSlotInfo * pSlotInfo, tMbsmPortInfo * pPortInfo)
{
    tQoSInPriorityMapNode *pInPriMapNode = NULL;
    tQoSInPriorityMapNode InPriMapNode;
    tQosClassToPriMapEntry QosClassToPriMapEntry;
    INT4                i4RetStatus = 0;

    MEMSET (&(QosClassToPriMapEntry), 0, (sizeof (tQosClassToPriMapEntry)));
    MEMSET (&InPriMapNode, 0, sizeof (tQoSInPriorityMapNode));

    while (NULL != (pInPriMapNode = (tQoSInPriorityMapNode *)
                    RBTreeGetNext (gQoSGlobalInfo.pRbInPriMapTbl,
                                   &InPriMapNode, QoSInPriMapCmpFun)))
    {
        InPriMapNode.u4Id = pInPriMapNode->u4Id;
        InPriMapNode.u4IfIndex = pInPriMapNode->u4IfIndex;
        InPriMapNode.u2VlanId = pInPriMapNode->u2VlanId;
        InPriMapNode.u1InPriority = pInPriMapNode->u1InPriority;
        InPriMapNode.u1InPriType = pInPriMapNode->u1InPriType;

        if (OSIX_TRUE == QosMbsmCheckPortRange (pPortInfo,
                                                pInPriMapNode->u4IfIndex))

        {
            QosClassToPriMapEntry.i4IfIndex = pInPriMapNode->u4IfIndex;
            QosClassToPriMapEntry.u4HwFilterId = pInPriMapNode->u4HwFilterId;
            QosClassToPriMapEntry.u2VlanId = pInPriMapNode->u2VlanId;
            QosClassToPriMapEntry.u1PriType = pInPriMapNode->u1InPriType;
            QosClassToPriMapEntry.u1Priority = pInPriMapNode->u1InPriority;
            QosClassToPriMapEntry.u1RegenPriority =
                pInPriMapNode->u1RegenPriority;
            QosClassToPriMapEntry.u1Flag = QOS_NP_ADD;
            i4RetStatus = QosxQoSMbsmHwMapClasstoPriMap (&QosClassToPriMapEntry,
                                                         pSlotInfo);
            if (i4RetStatus == FNP_NOT_SUPPORTED)
            {
                QOS_TRC_ARG1 (MGMT_TRC,
                              "In %s : ERROR: QoSHwWrMapClasstoPriMap () "
                              " Returns NOT_SUPPORTED. \r\n", __FUNCTION__);
                return (QOS_NP_NOT_SUPPORTED);
            }

            if (i4RetStatus == FNP_FAILURE)
            {
                QOS_TRC_ARG1 (MGMT_TRC,
                              "In %s : ERROR: QoSHwWrMapClasstoPriMap () "
                              " Returns FAILURE. \r\n", __FUNCTION__);

                return (QOS_FAILURE);
            }
            pInPriMapNode->u4HwFilterId = QosClassToPriMapEntry.u4HwFilterId;
        }
        /* Update the hardware filter id */

    }                            /* End of while */
    return QOS_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : QosMbsmProgramCpuEntries                             */
/* Description        : This function is used to Program the CPU entries QOSX*/
/* Input(s)           : MbsmSlotInfo                                         */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/* Return Value(s)    : QOS_SUCCESS or QOS_FAILURE                           */
/*****************************************************************************/
INT4
QosMbsmProgramCpuEntries (tMbsmSlotInfo * pSlotInfo)
{
    tQosCpuQEntry      *pCpuQNode = NULL;
    tQosCpuQEntry       NewCpuQNode;
    UINT4               u4CpuQId = 0;

    /*  Add QueueTemplate Table Entries    */
    /*------------------------------------|*/
    /*| QId | MinRate | MaxRate | Status  | */
    /*------------------------------------|*/
    /*| 1   | 1       | 65535   | Active  | */

    MEMSET (&NewCpuQNode, 0, sizeof (tQosCpuQEntry));

    NewCpuQNode.u4CpuQId = u4CpuQId;

    while (NULL != (pCpuQNode =
                    RBTreeGetNext (gQoSGlobalInfo.QosCpuQTbl,
                                   (tRBElem *) & NewCpuQNode, NULL)))
    {
        NewCpuQNode.u4CpuQId = pCpuQNode->u4CpuQId;
        /* Configure the HW */
        if (QosxQoSMbsmHwSetCpuRateLimit ((INT4) pCpuQNode->u4CpuQId,
                                          pCpuQNode->u4CpuQMinRate,
                                          pCpuQNode->u4CpuQMaxRate,
                                          pSlotInfo) == FNP_FAILURE)
        {
            QOS_TRC_ARG1 (MGMT_TRC, "In %s : QosxQoSMbsmHwSetCpuRateLimit "
                          " () Returns FAILURE. \r\n", __FUNCTION__);
            return (QOS_FAILURE);
        }
    }                            /* End of while */

    return (QOS_SUCCESS);

}

/*****************************************************************************/
/* Function Name      : QosMbsmProgramDefUserPriority                        */
/* Description        : This function is used to Program the Default User pri*/
/* Input(s)           : MbsmSlotInfo                                         */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/* Return Value(s)    : QOS_SUCCESS or QOS_FAILURE                           */
/*****************************************************************************/
INT4
QosMbsmProgramDefUserPriority (tMbsmSlotInfo * pSlotInfo,
                               tMbsmPortInfo * pPortInfo)
{
    UINT1              *pu1DUPNode = NULL;
    INT4                i4RetStatus = QOS_FAILURE;
    INT4                i4IfIndex = 0;

    /* Table is Populated */
    while (SNMP_SUCCESS ==
           (i4RetStatus = QoSGetNextDUPTblEntryIndex (i4IfIndex, &i4IfIndex)))
    {
        pu1DUPNode = QoSUtlGetDUPNode (i4IfIndex);

        if (OSIX_TRUE == QosMbsmCheckPortRange (pPortInfo, i4IfIndex))

        {
            if (pu1DUPNode != NULL)
            {
                i4RetStatus = QosxQoSMbsmHwSetDefUserPriority (i4IfIndex,
                                                               *pu1DUPNode,
                                                               pSlotInfo);
                if (i4RetStatus == QOS_FAILURE)
                {
                    QOS_TRC_ARG1 (MGMT_TRC,
                                  "In %s : QosxQoSMbsmHwSetDefUserPriority ()"
                                  "Returns FAILURE. \r\n", __FUNCTION__);
                    return (QOS_FAILURE);
                }
            }
        }

    }                            /* End of while */

    return (QOS_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QosMbsmProgramDefUserPriority                        */
/* Description        : This function is used to Program the Default User pri*/
/* Input(s)           : MbsmSlotInfo                                         */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/* Return Value(s)    : QOS_SUCCESS or QOS_FAILURE                           */
/*****************************************************************************/
INT4
QoSMbsmMapClassToIntPriority (tMbsmSlotInfo * pSlotInfo)
{
    tQosClassToIntPriEntry QosClassToIntPriEntry;
    tQoSClassMapNode    ClsMapNode;
    tQoSClassMapNode   *pClsMapNode = NULL;
    tQoSClass2PriorityNode *pCls2PriNode = NULL;
    tIssL2FilterEntry  *pL2FilterEntry = NULL;
    tIssL3FilterEntry  *pL3FilterEntry = NULL;
    INT4                i4RetStatus = FNP_FAILURE;

    MEMSET (&ClsMapNode, 0, sizeof (tQoSClassMapNode));

    while (NULL !=
           (pClsMapNode =
            (tQoSClassMapNode *) RBTreeGetNext (gQoSGlobalInfo.pRbClsMapTbl,
                                                (tRBElem *) & ClsMapNode,
                                                NULL)))
    {
        ClsMapNode.u4Id = pClsMapNode->u4Id;
        pCls2PriNode = QoSUtlGetClass2PriorityNode (pClsMapNode->u4ClassId);
        if ((pCls2PriNode != NULL) && (pCls2PriNode->u1Status == ACTIVE))
        {
            if (pClsMapNode->u4L2FilterId != 0)
            {
                pL2FilterEntry =
                    IssGetL2FilterTableEntry (pClsMapNode->u4L2FilterId);
                if (pL2FilterEntry == NULL)
                {
                    continue;
                }
                QosClassToIntPriEntry.pL2FilterPtr =
                    MEM_CALLOC ((sizeof (tIssL2FilterEntry)), 1,
                                tIssL2FilterEntry);

                if (QosClassToIntPriEntry.pL2FilterPtr == NULL)
                {
                    continue;
                }

                MEMCPY (QosClassToIntPriEntry.pL2FilterPtr, pL2FilterEntry,
                        (sizeof (tIssL2FilterEntry)));
            }
            else if (pClsMapNode->u4L3FilterId != 0)
            {
                pL3FilterEntry =
                    IssGetL3FilterTableEntry (pClsMapNode->u4L3FilterId);

                if (pL3FilterEntry == NULL)
                {
                    continue;
                }
                QosClassToIntPriEntry.pL3FilterPtr =
                    MEM_CALLOC ((sizeof (tIssL3FilterEntry)), 1,
                                tIssL3FilterEntry);
                if (QosClassToIntPriEntry.pL3FilterPtr == NULL)
                {
                    continue;
                }
                MEMCPY (QosClassToIntPriEntry.pL3FilterPtr, pL3FilterEntry,
                        (sizeof (tIssL3FilterEntry)));
            }

            QosClassToIntPriEntry.u1IntPriority = pCls2PriNode->u1RegenPri;
            QosClassToIntPriEntry.u1Flag = QOS_NP_ADD;

            if ((pClsMapNode->u4L2FilterId != 0)
                || (pClsMapNode->u4L3FilterId != 0))
            {
                i4RetStatus =
                    (INT4)
                    QosxQosMbsmHwMapClassToIntPriority (&QosClassToIntPriEntry,
                                                        pSlotInfo);
            }
            if (QosClassToIntPriEntry.pL2FilterPtr != NULL)
            {
                MEM_FREE (QosClassToIntPriEntry.pL2FilterPtr);
            }
            if (QosClassToIntPriEntry.pL3FilterPtr != NULL)
            {
                MEM_FREE (QosClassToIntPriEntry.pL3FilterPtr);
            }
            if (i4RetStatus == FNP_FAILURE)
            {
                QOS_TRC_ARG1 (MGMT_TRC,
                              "In %s : ERROR: QoSMbsmMapClassToIntPriority () "
                              " Returns FAILURE. \r\n", __FUNCTION__);
                return (QOS_FAILURE);
            }
            if (i4RetStatus == FNP_NOT_SUPPORTED)
            {
                QOS_TRC_ARG1 (MGMT_TRC,
                              "In %s : ERROR: QoSMbsmMapClassToIntPriority () "
                              " Returns NOT_SUPPORTED. \r\n", __FUNCTION__);
                return (QOS_FAILURE);

            }

        }
    }                            /* End of while */
    return QOS_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : QoSHwWrSchedulerUpdateParams                         */
/* Description        : This function is used to Program the schedular       */
/*                      params.                                              */
/* Input(s)           : MbsmSlotInfo                                         */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/* Return Value(s)    : QOS_SUCCESS or QOS_FAILURE                           */
/*****************************************************************************/
INT4
QoSMbsmSchedulerUpdateParams (tMbsmSlotInfo * pSlotInfo,
                              tMbsmPortInfo * pPortInfo)
{
    tQoSSchedulerEntry *pSchedEntry = NULL;
    tQoSSchedulerEntry  SchedEntry;
    INT4                i4RetStatus = FNP_FAILURE;
    tQoSSchedNode      *pSchedNode = NULL;
    tQoSSchedNode       SchedNode;

    MEMSET (&SchedNode, 0, sizeof (tQoSSchedNode));
    MEMSET (&SchedEntry, 0, sizeof (tQoSSchedulerEntry));

    while (NULL != (pSchedNode = RBTreeGetNext (gQoSGlobalInfo.pRbSchedTbl,
                                                (tRBElem *) & SchedNode, NULL)))
    {

        SchedNode.i4IfIndex = pSchedNode->i4IfIndex;
        SchedNode.u4Id = pSchedNode->u4Id;

        if (pSchedNode->u1Status == ACTIVE)
        {

            if (OSIX_TRUE == QosMbsmCheckPortRange (pPortInfo,
                                                    pSchedNode->i4IfIndex))
            {

                /* Fill the tQoSSchedulerEntry values */
                pSchedEntry = QoSHwWrUtilFillSchedEntry (pSchedNode->i4IfIndex,
                                                         pSchedNode->u4Id);
                if (pSchedEntry == NULL)
                {
                    return (QOS_FAILURE);
                }

                /* Call the NP Function */
                MEMCPY (&SchedEntry, pSchedEntry, sizeof (tQoSSchedulerEntry));
                SchedEntry.u1Flag = QOS_SCHED_UPDATE_ALGO;
                i4RetStatus =
                    QosxQoSMbsmHwSchedulerUpdateParams (&SchedEntry, pSlotInfo);
                if (i4RetStatus == FNP_FAILURE)
                {
                    QOS_TRC_ARG1 (MGMT_TRC,
                                  "In %s : ERROR: QosxQoSMbsmHwSchedulerUpdateParams "
                                  "() Returns FAILURE. \r\n", __FUNCTION__);

                    QoSHwWrUtilFreeSchedEntry (pSchedEntry);
                    return (QOS_FAILURE);
                }

                /* Call the NP Function */
                MEMCPY (&SchedEntry, pSchedEntry, sizeof (tQoSSchedulerEntry));
                SchedEntry.u1Flag = QOS_SCHED_UPDATE_HL;
                i4RetStatus = QosxQoSMbsmHwSchedulerUpdateParams (&SchedEntry,
                                                                  pSlotInfo);
                if (i4RetStatus == FNP_FAILURE)
                {
                    QOS_TRC_ARG1 (MGMT_TRC,
                                  "In %s : ERROR: QosxQoSMbsmHwSchedulerUpdateParams "
                                  "() Returns FAILURE. \r\n", __FUNCTION__);

                    QoSHwWrUtilFreeSchedEntry (pSchedEntry);
                    return (QOS_FAILURE);
                }

                MEMCPY (&SchedEntry, pSchedEntry, sizeof (tQoSSchedulerEntry));
                SchedEntry.u1Flag = QOS_SCHED_UPDATE_SHAPE;

                /* Call the NP Function */
                i4RetStatus =
                    QosxQoSMbsmHwSchedulerUpdateParams (&SchedEntry, pSlotInfo);
                if (i4RetStatus == FNP_FAILURE)
                {
                    QOS_TRC_ARG1 (MGMT_TRC,
                                  "In %s : ERROR: QosxQoSMbsmHwSchedulerUpdateParams "
                                  "() Returns FAILURE. \r\n", __FUNCTION__);

                    QoSHwWrUtilFreeSchedEntry (pSchedEntry);
                    return (QOS_FAILURE);
                }

                QoSHwWrUtilFreeSchedEntry (pSchedEntry);

            }
        }

    }                            /* End of while */
    return QOS_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : QosMbsmMapClassToPolicy                              */
/* Description        : This function is used to Program the policy          */
/*                      map table.                                           */
/* Input(s)           : MbsmSlotInfo                                         */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/* Return Value(s)    : QOS_SUCCESS or QOS_FAILURE                           */
/*****************************************************************************/
INT4
QosMbsmMapClassToPolicy (tMbsmSlotInfo * pSlotInfo, tMbsmPortInfo * pPortInfo)
{
    tQoSPolicyMapNode  *pQosPolicyMapNode = NULL;
    tQoSPolicyMapEntry  NewPolicyMapEntry;
    tQoSInProfileActionEntry NewInProActEntry;
    tQoSOutProfileActionEntry NewOutProActEntry;
    tQoSMeterEntry     *pNewMeterEntry = NULL;
    tQoSMeterNode      *pMeterNode = NULL;
    tQoSClassInfoNode  *pClsInfoNode = NULL;
    tQoSFilterInfoNode *pFltInfoNode = NULL;
    tQoSClassMapEntry  *pNewClassMapEntry = NULL;
    INT4                i4RetStatus = FNP_FAILURE;
    UINT4               u4First = QOS_SUCCESS;
    UINT1               u1TmpFlag = 0;
    UINT4               u4PolMapId = 0;
    UINT1               u1IPVal = 0;

    MEMSET (&(NewInProActEntry), 0, (sizeof (tQoSInProfileActionEntry)));
    MEMSET (&(NewOutProActEntry), 0, (sizeof (tQoSOutProfileActionEntry)));
    MEMSET (&(NewPolicyMapEntry), 0, (sizeof (tQoSPolicyMapEntry)));

    for (u1IPVal = 0; u1IPVal < QOS_POL_TBL_DEF_ENTRY_MAX; u1IPVal++)
    {
        u4PolMapId = u1IPVal + 1;

        pQosPolicyMapNode = QoSUtlGetPolicyMapNode (u4PolMapId);

        if (pQosPolicyMapNode == NULL)
        {
            continue;
        }
        /* Fill the Values for that Entry */
        /* Meter Table */
        if (pQosPolicyMapNode->u4MeterTableId != 0)
        {
            pMeterNode = QoSUtlGetMeterNode (pQosPolicyMapNode->u4MeterTableId);

            if (pMeterNode == NULL)
            {
                return (QOS_FAILURE);
            }

            pNewMeterEntry =
                MEM_MALLOC ((sizeof (tQoSMeterEntry)), tQoSMeterEntry);
            if (pNewMeterEntry == NULL)
            {
                return (QOS_FAILURE);
            }

            QoSHwWrUtilFillMeterParams (pNewMeterEntry, pMeterNode);

            /* Fill Out profile Action */
            QoSHwWrUtilFillOutProParams (&(NewOutProActEntry),
                                         pQosPolicyMapNode);

            /* Fill In profile Action */
            QoSHwWrUtilFillInProParams (&(NewInProActEntry), pQosPolicyMapNode);
        }

        /* Fill Policy Map Table */
        QoSHwWrUtilFillPolicyParams (&(NewPolicyMapEntry), pQosPolicyMapNode);

        /* This is Interface Specific Policy CLASS is NULL */
        if (OSIX_FALSE == QosMbsmCheckPortRange (pPortInfo,
                                                 pQosPolicyMapNode->u4IfIndex))
        {
            continue;
        }
        if (pQosPolicyMapNode->u4ClassId == 0)
        {
            /* Call the NP Function */
            i4RetStatus = QosxQoSMbsmHwMapClassToPolicy (pNewClassMapEntry,
                                                         &(NewPolicyMapEntry),
                                                         &(NewInProActEntry),
                                                         &(NewOutProActEntry),
                                                         pNewMeterEntry,
                                                         QOS_PLY_ADD,
                                                         pSlotInfo);
            if (pNewMeterEntry != NULL)
            {
                MEM_FREE (pNewMeterEntry);
            }
            if (i4RetStatus == FNP_FAILURE || i4RetStatus == FNP_NOT_SUPPORTED)
            {
                QOS_TRC_ARG1 (MGMT_TRC,
                              "In %s : ERROR: QosMbsmMapClassToPolicy () "
                              " Returns FAILURE. \r\n", __FUNCTION__);

                return (QOS_FAILURE);
            }
            return (QOS_SUCCESS);
        }                        /* End of NULL CLASS */
        /* Get all The Class Map for a given Policy */
        pClsInfoNode = QoSUtlGetClassInfoNode (pQosPolicyMapNode->u4ClassId);

        if (pClsInfoNode == NULL)
        {
            if (pNewMeterEntry != NULL)
            {
                MEM_FREE (pNewMeterEntry);
            }
            return (QOS_FAILURE);
        }

        /* Scan the Filter list in the ClassInfo */
        TMO_SLL_Scan (&(pClsInfoNode->SllFltInfo), pFltInfoNode,
                      tQoSFilterInfoNode *)
        {
            /* Add the Policy while maping the First Class Filter Node */
            if (u4First == QOS_SUCCESS)
            {
                u1TmpFlag = QOS_PLY_ADD;
                u4First = QOS_FAILURE;
            }
            else
            {
                u1TmpFlag = QOS_PLY_MAP;
            }

            /* Fill the Values for that Entry */
            pNewClassMapEntry =
                QoSFiltersForClassMapEntry (pFltInfoNode->pClsMapNode);

            if (pNewClassMapEntry == NULL)
            {
                QOS_TRC_ARG1 (MGMT_TRC,
                              "In QosMbsmMapClassToPolicy  %s : ERROR: "
                              "() Returns FAILURE." " \r\n", __FUNCTION__);

                if (pNewMeterEntry != NULL)
                {
                    MEM_FREE (pNewMeterEntry);
                }
                return (QOS_FAILURE);
            }

            /* Call the NP Function */
            i4RetStatus = QosxQoSMbsmHwMapClassToPolicy (pNewClassMapEntry,
                                                         &(NewPolicyMapEntry),
                                                         &(NewInProActEntry),
                                                         &(NewOutProActEntry),
                                                         pNewMeterEntry,
                                                         u1TmpFlag, pSlotInfo);

            QoSUtlClassMapEntryMemFree (pNewClassMapEntry);

            if (i4RetStatus == FNP_FAILURE || i4RetStatus == FNP_NOT_SUPPORTED)
            {
                QOS_TRC_ARG1 (MGMT_TRC, "In %s : ERROR: QoSHwMapClassToPolicy "
                              "() Returns FAILURE. \r\n", __FUNCTION__);

                if (pNewMeterEntry != NULL)
                {
                    MEM_FREE (pNewMeterEntry);
                }
                return (QOS_FAILURE);
            }
        }                        /* End of Scan */
        if (pNewMeterEntry != NULL)
        {
            MEM_FREE (pNewMeterEntry);
        }
    }                            /* End of for */
    return QOS_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : QosxQoSMbsmHwSetPbitPreferenceOverDscp               */
/* Description        : This function is used to Program the schedular       */
/*                      hierarchy map.                                       */
/* Input(s)           : MbsmSlotInfo                                         */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/* Return Value(s)    : QOS_SUCCESS or QOS_FAILURE                           */
/*****************************************************************************/
INT4
QosMbsmHwSetPbitPreferenceOverDscp (tMbsmSlotInfo * pSlotInfo,
                                    tMbsmPortInfo * pPortInfo)
{
    UINT4               u4IfIndex = 0;
    UINT4               u4EndIndex = 0;
    UINT1              *pu1Pref = NULL;

    u4EndIndex = pPortInfo->u4StartIfIndex + pPortInfo->u4PortCount;

    for (u4IfIndex = pPortInfo->u4StartIfIndex;
         u4IfIndex <= u4EndIndex; u4IfIndex++)
    {
        pu1Pref = QoSGetPrefNode (u4IfIndex);
        if (pu1Pref == NULL)
        {
            continue;
        }

        if (FNP_FAILURE ==
            QosxQoSMbsmHwSetPbitPreferenceOverDscp (u4IfIndex,
                                                    *pu1Pref, pSlotInfo))
        {
            QOS_TRC_ARG1 (MGMT_TRC,
                          "In %s : ERROR: QosxQoSMbsmHwSetPbitPreferenceOverDscp () "
                          " Returns FAILURE. \r\n", __FUNCTION__);
            return (QOS_FAILURE);
        }
    }
    return QOS_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : QoSMbsmSchedulerHierarchyMap                         */
/* Description        : This function is used to Program the schedular       */
/*                      hierarchy map.                                       */
/* Input(s)           : MbsmSlotInfo                                         */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/* Return Value(s)    : QOS_SUCCESS or QOS_FAILURE                           */
/*****************************************************************************/
INT4
QoSMbsmSchedulerHierarchyMap (tMbsmSlotInfo * pMbsmSlotInfo,
                              tMbsmPortInfo * pPortInfo)
{
    tQoSHierarchyNode  *pHierarchyNode = NULL;
    tQoSHierarchyNode   HierarchyNode;
    tQoSSchedNode      *pSchedNode = NULL;
    UINT4               u4SchedHwId = 0;
    UINT4               u4NxtSchedHwId = 0;

    MEMSET (&HierarchyNode, 0, sizeof (tQoSHierarchyNode));

    while (NULL !=
           (pHierarchyNode =
            RBTreeGetNext (gQoSGlobalInfo.pRbHierarchyTbl,
                           (tRBElem *) & HierarchyNode, NULL)))
    {
        HierarchyNode.i4IfIndex = pHierarchyNode->i4IfIndex;
        HierarchyNode.u1Level = pHierarchyNode->u1Level;
        HierarchyNode.u4SchedId = pHierarchyNode->u4SchedId;
        pSchedNode =
            QoSUtlGetSchedNode (HierarchyNode.i4IfIndex,
                                HierarchyNode.u4SchedId);
        if (pSchedNode == NULL)
        {
            QOS_TRC_ARG1 (MGMT_TRC, "In %s : NULL check failed \r\n",
                          __FUNCTION__);
            return QOS_FAILURE;
        }
        u4SchedHwId = pSchedNode->u4SchedulerHwId;
        pSchedNode = NULL;
        if (pHierarchyNode->u4SchedNext != 0)
        {
            pSchedNode =
                QoSUtlGetSchedNode (HierarchyNode.i4IfIndex,
                                    HierarchyNode.u4SchedId);
            if (pSchedNode == NULL)
            {
                QOS_TRC_ARG1 (MGMT_TRC, "In %s : NULL check failed \r\n",
                              __FUNCTION__);
                return QOS_FAILURE;
            }
            u4NxtSchedHwId = pSchedNode->u4SchedulerHwId;
            pSchedNode = NULL;
        }

        if (pHierarchyNode->u1Status == ACTIVE)
        {

            if (OSIX_TRUE == QosMbsmCheckPortRange (pPortInfo,
                                                    pHierarchyNode->i4IfIndex))
            {
                if (FNP_FAILURE ==
                    QosxQoSMbsmHwSchedulerHierarchyMap (pHierarchyNode->
                                                        i4IfIndex,
                                                        (INT4) u4SchedHwId,
                                                        pHierarchyNode->
                                                        u2Weight,
                                                        pHierarchyNode->
                                                        u1Priority,
                                                        u4NxtSchedHwId,
                                                        pHierarchyNode->u4QNext,
                                                        pHierarchyNode->u1Level,
                                                        QOS_HIERARCHY_ADD,
                                                        pMbsmSlotInfo))
                {
                    return QOS_FAILURE;
                }
            }
        }

    }                            /* End of while */
    return QOS_SUCCESS;
}
