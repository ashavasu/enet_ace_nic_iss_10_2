
/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: qosrbutl.c,v 1.1 2014/01/24 12:17:14 siva Exp $
 *
 * Description: This file contains RB tree utilities regarding            
 *              QOS_ARRAY_TO_RBTREE_WANTED flag. 
 *
 *******************************************************************/

#include "qosinc.h"

/*--------  RBTREE UTILITIES FOR gQosStdClfrPortTable -----------*/

/*****************************************************************************/
/* Function Name      : QosCreateQosStdClfrPortTbl                           */
/*                                                                           */
/* Description        : This routine creates gQosStdClfrPortTable.           */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : QOS_SUCCESS/QOS_FAILURE                              */
/*****************************************************************************/
INT4
QosCreateQosStdClfrPortTbl ()
{
    /* RBTree creation for QosStdClfrPortTbl. */
    gQosStdClfrPortTable = RBTreeCreateEmbedded (0, QosStdClfrPortTblCmp);

    if (gQosStdClfrPortTable == NULL)
    {
        QOS_TRC (INIT_SHUT_TRC | ALL_FAILURE_TRC, "QosCreateQosStdClfrPortTbl:"
                   "RB Tree creation for QosStdClfrPortTbl is failed\r\n");
        return QOS_FAILURE;
    }
    return QOS_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : QosDeleteQosStdClfrPortTbl                           */
/*                                                                           */
/* Description        : This routine deletes gQosStdClfrPortTable.           */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
QosDeleteQosStdClfrPortTbl ()
{
    /* RBTree deletion for QosStdClfrPortTbl. */
    QosDelAllStdClfrPortTblEntries ();
    if (gQosStdClfrPortTable != NULL)
    {
        RBTreeDestroy (gQosStdClfrPortTable, NULL, 0);
        gQosStdClfrPortTable = NULL;
    }
    return;
}

/*****************************************************************************/
/* Function Name      : QosStdClfrPortTblCmp                                 */
/*                                                                           */
/* Description        : This routine is used in gQosStdClfrPortTable for     */
/*                      comparing two keys used in RBTree functionality.     */
/*                                                                           */
/* Input(s)           : pNode     - Key 1                                    */
/*                      pNodeIn   - Key 2                                    */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : -1/1 -> when any key element is Less/Greater         */
/*                      0    -> when all key elements are Equal              */
/*****************************************************************************/
INT4
QosStdClfrPortTblCmp (tRBElem * Node,
                      tRBElem * NodeIn)
{
    tQosStdClfrPortNode     *pNode = NULL;
    tQosStdClfrPortNode     *pNodeIn = NULL;
  
    pNode = (tQosStdClfrPortNode *)Node;
    pNodeIn = (tQosStdClfrPortNode *) NodeIn;

    /* key 1 --> Diffserv Classifier id.
     * key 2 --> Interface index.
     */
    if (pNode->u4DsClfrId < pNodeIn->u4DsClfrId)
    {
        return (QOS_RBTREE_KEY_LESSER);
    }
    else if (pNode->u4DsClfrId > pNodeIn->u4DsClfrId)
    {
        return (QOS_RBTREE_KEY_GREATER);
    }
    else if (pNode->u4IfIndex < pNodeIn->u4IfIndex)
    {
        return (QOS_RBTREE_KEY_LESSER);
    }
    else if (pNode->u4IfIndex > pNodeIn->u4IfIndex)
    {
        return (QOS_RBTREE_KEY_GREATER);
   }
    return (QOS_RBTREE_KEY_EQUAL);
}
/*****************************************************************************/
/* Function Name      : QosAddStdClfrPortTblEntry                            */
/*                                                                           */
/* Description        : This routine add a tQosStdClfrPortNode  in           */
/*                      gQosStdClfrPortTable.                                */
/*                                                                           */
/* Input(s)           : u4DsClfrId    - Diff Serv Classifier Id.             */
/*                      u4IfIndex    - Interface index.                      */
/*                                                                           */
/* Output(s)          : ppStdClfrPortNode  - pointer to the pStdClfrPortNode.*/
/*                                                                           */
/* Return Value(s)    : QOS_SUCCESS / QOS_FAILURE                            */
/*****************************************************************************/
INT4
QosAddStdClfrPortTblEntry (UINT4 u4DsClfrId, UINT2 u4IfIndex, 
                           tQosStdClfrPortNode **ppStdClfrPortNode)
{
    (*ppStdClfrPortNode) = NULL;

    if (((*ppStdClfrPortNode) = QosGetStdClfrPortTblEntry (u4DsClfrId, 
                u4IfIndex)) != NULL)
    {
        return QOS_SUCCESS;
    }

    (*ppStdClfrPortNode) = (tQosStdClfrPortNode *)(VOID *) 
               MemAllocMemBlk (gQoSGlobalInfo.QosStdClfrPortPoolId);
    if ((*ppStdClfrPortNode) == NULL)
    {
        QOS_TRC (CONTROL_PLANE_TRC | ALL_FAILURE_TRC, "QosAddStdClfrPortTblEntry:"
                   "Memory allocation for tQosStdClfrPortNode failed\r\n");
        return QOS_FAILURE;
    }

    MEMSET ((*ppStdClfrPortNode), 0, sizeof (tQosStdClfrPortNode));

    (*ppStdClfrPortNode)->u4DsClfrId = u4DsClfrId;
    (*ppStdClfrPortNode)->u4IfIndex = u4IfIndex;

    if (RBTreeAdd (gQosStdClfrPortTable, (tRBElem *)
                       (*ppStdClfrPortNode)) == RB_FAILURE)
    {
        MemReleaseMemBlock (gQoSGlobalInfo.QosStdClfrPortPoolId,
                            (UINT1 *) (*ppStdClfrPortNode));
        (*ppStdClfrPortNode) = NULL;
        QOS_TRC (CONTROL_PLANE_TRC | ALL_FAILURE_TRC, 
                 "QosAddStdClfrPortTblEntry:Global RBTree Addition Failed for "
                 "gQosStdClfrPortTable Entry \r\n");
        return QOS_FAILURE;
    }
    return QOS_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : QosGetStdClfrPortTblEntry                            */
/*                                                                           */
/* Description        : This routine gets a tQosStdClfrPortNode  for the     */
/*                      given u4DsClfrId + u4IfIndex.                        */
/*                                                                           */
/* Input(s)           : u4DsClfrId    - Diff Serv Classifier Id.             */
/*                      u4IfIndex    - Interface index.                      */
/*                                                                           */
/* Output(s)          : pStdClfrPortNode  - pointer to the pStdClfrPortNode  */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
tQosStdClfrPortNode *
QosGetStdClfrPortTblEntry (UINT4 u4DsClfrId, UINT2 u4IfIndex)
{
    tQosStdClfrPortNode  *pStdClfrPortNode = NULL;
    tQosStdClfrPortNode   StdClfrPortNode;

    MEMSET (&StdClfrPortNode, 0, sizeof (tQosStdClfrPortNode));

    StdClfrPortNode.u4DsClfrId = u4DsClfrId;
    StdClfrPortNode.u4IfIndex = u4IfIndex;

    pStdClfrPortNode  = (tQosStdClfrPortNode *) RBTreeGet 
        ((gQosStdClfrPortTable), (tRBElem *) &StdClfrPortNode);

    return pStdClfrPortNode;
}

/*****************************************************************************/
/* Function Name      : QosGetFirstStdClfrPortTblEntry                       */
/*                                                                           */
/* Description        : This routine is used to get first tQosStdClfrPortNode*/
/*                      from gQosStdClfrPortTable.                           */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : pStdClfrPortNode - pointer to the tQosStdClfrPortNode*/
/*                                                                           */
/* Return Value(s)    : NONE                                                 */
/*****************************************************************************/
tQosStdClfrPortNode  *
QosGetFirstStdClfrPortTblEntry ()
{
    tQosStdClfrPortNode    *pStdClfrPortNode = NULL;

    pStdClfrPortNode = (tQosStdClfrPortNode *)
                    RBTreeGetFirst (gQosStdClfrPortTable);

    return pStdClfrPortNode;
}

/*****************************************************************************/
/* Function Name      : QosGetNextStdClfrPortTblEntry                        */
/*                                                                           */
/* Description        : This routine is used to get the next                 */
/*                      tQosStdClfrPortNode from gQosStdClfrPortTable.       */
/*                                                                           */
/* Input(s)           : u4DsClfrId    - Diff Serv Classifier Id.             */
/*                      u4IfIndex    - Interface index.                      */
/*                                                                           */
/* Output(s)          : pStdClfrPortNode - pointer to the tQosStdClfrPortNode*/
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
tQosStdClfrPortNode *
QosGetNextStdClfrPortTblEntry (UINT4 u4DsClfrId, UINT2 u4IfIndex)
{
    tQosStdClfrPortNode  *pStdClfrPortNode = NULL;
    tQosStdClfrPortNode   StdClfrPortNode;

    MEMSET (&StdClfrPortNode, 0, sizeof (tQosStdClfrPortNode));
    
    StdClfrPortNode.u4DsClfrId = u4DsClfrId;
    StdClfrPortNode.u4IfIndex = u4IfIndex;

    pStdClfrPortNode  = (tQosStdClfrPortNode *) RBTreeGetNext 
      ((gQosStdClfrPortTable), (tRBElem *) &StdClfrPortNode, NULL);

    return pStdClfrPortNode;
}

/*****************************************************************************/
/* Function Name      : QosDelStdClfrPortTblEntry                            */
/*                                                                           */
/* Description        : This routine deletes a tQosStdClfrPortNode from the  */
/*                      gQosStdClfrPortTable.                                */
/*                                                                           */
/* Input(s)           : pStdClfrPortNode - node to be deleted.               */
/*                                                                           */
/* Return Value(s)    : QOS_SUCCESS/QOS_FAILURE                              */
/*****************************************************************************/
INT4
QosDelStdClfrPortTblEntry (tQosStdClfrPortNode *pStdClfrPortNode)
{
    if (pStdClfrPortNode == NULL)
    {
        return QOS_FAILURE;
    }
    if (RBTreeRemove ((gQosStdClfrPortTable), 
            (tRBElem *) pStdClfrPortNode) == RB_FAILURE)
    {
        QOS_TRC (CONTROL_PLANE_TRC | ALL_FAILURE_TRC, 
                 "QosDelStdClfrPortTblEntry: RBTree remove failed \r\n");
        return QOS_FAILURE;
    }
    MemReleaseMemBlock (gQoSGlobalInfo.QosStdClfrPortPoolId,
                        (UINT1 *) (pStdClfrPortNode));
    pStdClfrPortNode = NULL;
    return QOS_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : QosDelAllStdClfrPortTblEntries                       */
/*                                                                           */
/* Description        : This function deletes all the tQosStdClfrPortNode    */
/*                      entries of gQosStdClfrPortTable.                     */
/*                                                                           */
/* Input(s)           : NONE                                                 */
/*                                                                           */
/* Output(s)          : NONE                                                 */
/*                                                                           */
/* Return Value(s)    : NONE                                                 */
/*****************************************************************************/
VOID
QosDelAllStdClfrPortTblEntries ()
{
    tQosStdClfrPortNode   *pStdClfrPortNode = NULL;
    tQosStdClfrPortNode   *pNextQosStdClfrPortNode = NULL;

    pStdClfrPortNode = QosGetFirstStdClfrPortTblEntry ();

    while (pStdClfrPortNode != NULL)
    {
        /* Delete all the nodes */
        pNextQosStdClfrPortNode = QosGetNextStdClfrPortTblEntry 
            (pStdClfrPortNode->u4DsClfrId, pStdClfrPortNode->u4IfIndex);

        QosDelStdClfrPortTblEntry (pStdClfrPortNode);

        pStdClfrPortNode = pNextQosStdClfrPortNode;
    }
    return;
}

 /*****************************************************************************/
 /*                                                                           */
 /* Function Name       : QosAddPortNodeInStdClfrPortTbl                      */
 /*                                                                           */
 /* Description         : This function adds tQosStdClfrPortNode in           */
 /*                       gQosStdClfrPortTable for the given                  */
 /*                       u4DsClfrId + u4IfIndex and sets u2Flag.             */
 /*                                                                           */
 /* Input(s)            : u4DsClfrId   - Diff serv classifier id              */
 /*                       u4IfIndex    - Interface index                      */
 /*                       u2Flag       - Flag to be set in tQosStdClfrPortNode*/
 /*                                                                           */
 /* Output(s)           : NONE                                                */
 /*                                                                           */
 /* Returns             : NONE                                                */
 /*****************************************************************************/
VOID
QosAddPortNodeInStdClfrPortTbl (UINT4 u4DsClfrId, UINT2 u4IfIndex,
                                UINT2 u2Flag)
{
    tQosStdClfrPortNode    *pStdClfrPortNode = NULL;

    if (QosAddStdClfrPortTblEntry (u4DsClfrId, u4IfIndex, &pStdClfrPortNode)
        == OSIX_FAILURE)
    {
        return;
    }
    pStdClfrPortNode->u2BitMask |= u2Flag;
    return;
}

 /*****************************************************************************/
 /*                                                                           */
 /* Function Name       : QosDelPortNodeInStdClfrPortTbl                      */
 /*                                                                           */
 /* Description         : This function resets in pQosStdClfrPortNode for the */
 /*                       given u4DsClfrId + u4IfIndex. And deletes the       */
 /*                       pQosStdClfrPortNode if none other u2BitMask is set. */
 /*                                                                           */
 /* Input(s)            : u4DsClfrId   - Diff serv classifier id              */
 /*                       u4IfIndex   - Interface index                       */
 /*                       u2Flag       - Flag to be reset in tQosStdClfrPortNode*/
 /*                                                                           */
 /* Output(s)           : NONE                                                */
 /*                                                                           */
 /* Returns             : NONE                                                */
 /*****************************************************************************/
VOID
QosDelPortNodeInStdClfrPortTbl (UINT4 u4DsClfrId, UINT2 u4IfIndex, 
                                UINT2 u2Flag)
{
    tQosStdClfrPortNode    *pStdClfrPortNode = NULL;

    pStdClfrPortNode = QosGetStdClfrPortTblEntry (u4DsClfrId, u4IfIndex);

    if (pStdClfrPortNode == NULL)
    {
        return;
    }
    pStdClfrPortNode->u2BitMask &= (~u2Flag);

    if (pStdClfrPortNode->u2BitMask == 0)
    {
        QosDelStdClfrPortTblEntry (pStdClfrPortNode);
    }
    return;
}

