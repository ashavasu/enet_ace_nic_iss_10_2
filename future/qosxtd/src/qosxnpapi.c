/* $Id: qosxnpapi.c,v 1.15 2016/05/25 10:04:42 siva Exp $ */
/****************************************************************************/
/* Copyright (C) 2012 Aricent Inc . All Rights Reserved                     */
/*                                                                          */
/*  FILE NAME             : qosxnpapi.c                                     */
/*  PRINCIPAL AUTHOR      : Aricent                                         */
/*  SUBSYSTEM NAME        : QoSx                                            */
/*  MODULE NAME           : QoSX-NP-API                                     */
/*  LANGUAGE              : C                                               */
/*  TARGET ENVIRONMENT    : Linux                                           */
/*  DATE OF FIRST RELEASE :                                                 */
/*  AUTHOR                : Aricent                                         */
/*  FUNCTIONS DEFINED(Applicable for Source files) :                        */
/*  DESCRIPTION           : All  Network Processor API Function             */
/*                          calls are done here                             */
/****************************************************************************/
/*                                                                          */
/*  Change History                                                          */
/*  Version               :                                                 */
/*  Date(DD/MM/YYYY)      :                                                 */
/*  Modified by           :                                                 */
/*  Description of change :                                                 */
/****************************************************************************/

#ifndef __QOSX_NPAPI_C__
#define __QOSX_NPAPI_C__

#include "nputil.h"
#include "qosinc.h"

/***************************************************************************
 *                                                                          
 *    Function Name       : QosxQoSHwInit                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes QoSHwInit
 *                                                                          
 *    Input(s)            : Arguments of QoSHwInit
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

#undef QosxQoSHwInit
UINT1
QosxQoSHwInit ()
{
    tFsHwNp             FsHwNp;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_QOSX_MOD,    /* Module ID */
                         QO_S_HW_INIT,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : QosxQoSHwMapClassToPolicy                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes QoSHwMapClassToPolicy
 *                                                                          
 *    Input(s)            : Arguments of QoSHwMapClassToPolicy
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/
#undef QosxQoSHwMapClassToPolicy
UINT1
QosxQoSHwMapClassToPolicy (tQoSClassMapEntry * pClsMapEntry,
                           tQoSPolicyMapEntry * pPlyMapEntry,
                           tQoSInProfileActionEntry * pInProActEntry,
                           tQoSOutProfileActionEntry * pOutProActEntry,
                           tQoSMeterEntry * pMeterEntry, UINT1 u1Flag)
{
    tFsHwNp             FsHwNp;
    tQosxNpModInfo     *pQosxNpModInfo = NULL;
    tQosxNpWrQoSHwMapClassToPolicy *pEntry = NULL;
    tQoSQMapNode       *pQMapNode = NULL;
    INT4                i4RetVal = 0;

    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_QOSX_MOD,    /* Module ID */
                         QO_S_HW_MAP_CLASS_TO_POLICY,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pQosxNpModInfo = &(FsHwNp.QosxNpModInfo);
    pEntry = &pQosxNpModInfo->QosxNpQoSHwMapClassToPolicy;

    /* Pass the IfIndex, Regenpriority and RegenPriVaue as 0 */

    /* Get the QMap Node corresponding to the CLASS value, and
     * update the TrafficClass with Q-Map's Queue-Id Corresponding to the CLASS
     * DO this for the CLASS in class-map entry, New-Class in InProfConform,
     * New-Class in InProfExceed and New-Class in OuProfAction entries */
    if (pClsMapEntry != NULL)
    {
        /* Set default traffic Class as 1 */
        pClsMapEntry->u4TrafficClass = 1;
        if (pClsMapEntry->u4QoSMFClass != 0)
        {
            pQMapNode = QoSUtlGetQMapEntry (pClsMapEntry->u4QoSMFClass);
            if (pQMapNode != NULL)
            {
                pClsMapEntry->u4TrafficClass = pQMapNode->u4QId;
            }
        }
    }

    if (pInProActEntry != NULL)
    {
        /* Set default traffic Class as 1 */
        pInProActEntry->u4QoSInProfConfTrafficClass = 1;
        pInProActEntry->u4QoSInProfExcTrafficClass = 1;

        if (pInProActEntry->u4QoSInProfConfNewClass != 0)
        {
            pQMapNode =
                QoSUtlGetQMapEntry (pInProActEntry->u4QoSInProfConfNewClass);
            if (pQMapNode != NULL)
            {
                pInProActEntry->u4QoSInProfConfTrafficClass = pQMapNode->u4QId;
            }
        }

        if (pInProActEntry->u4QoSInProfExcNewClass != 0)
        {
            pQMapNode =
                QoSUtlGetQMapEntry (pInProActEntry->u4QoSInProfExcNewClass);
            if (pQMapNode != NULL)
            {
                pInProActEntry->u4QoSInProfExcTrafficClass = pQMapNode->u4QId;
            }
        }
    }
    if (pOutProActEntry != NULL)
    {
        /* Set default traffic Class as 1 */
        pOutProActEntry->u4QoSOutProfileTrafficClass = 1;
        if (pOutProActEntry->u4QoSOutProfileNewClass != 0)
        {
            pQMapNode =
                QoSUtlGetQMapEntry (pOutProActEntry->u4QoSOutProfileNewClass);
            if (pQMapNode != NULL)
            {
                pOutProActEntry->u4QoSOutProfileTrafficClass = pQMapNode->u4QId;
            }
        }
    }
    pEntry->pClsMapEntry = pClsMapEntry;
    pEntry->pPlyMapEntry = pPlyMapEntry;
    pEntry->pInProActEntry = pInProActEntry;
    pEntry->pOutProActEntry = pOutProActEntry;
    pEntry->pMeterEntry = pMeterEntry;
    pEntry->u1Flag = u1Flag;

    i4RetVal = NpUtilHwProgram (&FsHwNp);
    if (i4RetVal == FNP_FAILURE || i4RetVal == FNP_NOT_SUPPORTED)
    {
        return (i4RetVal);
    }
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : QosxQoSHwUpdatePolicyMapForClass                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes QoSHwUpdatePolicyMapForClass
 *                                                                          
 *    Input(s)            : Arguments of QoSHwUpdatePolicyMapForClass
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/
#undef QosxQoSHwUpdatePolicyMapForClass
UINT1
QosxQoSHwUpdatePolicyMapForClass (tQoSClassMapEntry * pClsMapEntry,
                                  tQoSPolicyMapEntry * pPlyMapEntry,
                                  tQoSInProfileActionEntry * pInProActEntry,
                                  tQoSOutProfileActionEntry * pOutProActEntry,
                                  tQoSMeterEntry * pMeterEntry, UINT1 u1Flag)
{
    tFsHwNp             FsHwNp;
    tQosxNpModInfo     *pQosxNpModInfo = NULL;
    tQosxNpWrQoSHwUpdatePolicyMapForClass *pEntry = NULL;
    tQoSQMapNode       *pQMapNode = NULL;
    INT4                i4RetVal = 0;

    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_QOSX_MOD,    /* Module ID */
                         QO_S_HW_UPDATE_POLICY_MAP_FOR_CLASS,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pQosxNpModInfo = &(FsHwNp.QosxNpModInfo);
    pEntry = &pQosxNpModInfo->QosxNpQoSHwUpdatePolicyMapForClass;

    /* Get the QMap Node corresponding to the CLASS value, and
     * update the TrafficClass with Q-Map's Queue-Id Corresponding to the CLASS
     * DO this for the CLASS in class-map entry, New-Class in InProfConform, 
     * New-Class in InProfExceed and New-Class in OuProfAction entries */

    /* Pass the IfIndex, Regenpriority and RegenPriVaue as 0 */
    if (pClsMapEntry != NULL)
    {
        pClsMapEntry->u4TrafficClass = 1;    /* Set default traffic Class as 1 */
        if (pClsMapEntry->u4QoSMFClass != 0)
        {
            pQMapNode = QoSUtlGetQMapEntry (pClsMapEntry->u4QoSMFClass);
            if (pQMapNode != NULL)
            {
                pClsMapEntry->u4TrafficClass = pQMapNode->u4QId;
            }
        }
    }

    if (pInProActEntry != NULL)
    {
        /* Set default traffic Class as 1 */
        pInProActEntry->u4QoSInProfConfTrafficClass = 1;
        pInProActEntry->u4QoSInProfExcTrafficClass = 1;

        if (pInProActEntry->u4QoSInProfConfNewClass != 0)
        {
            pQMapNode =
                QoSUtlGetQMapEntry (pInProActEntry->u4QoSInProfConfNewClass);
            if (pQMapNode != NULL)
            {
                pInProActEntry->u4QoSInProfConfTrafficClass = pQMapNode->u4QId;
            }
        }

        if (pInProActEntry->u4QoSInProfExcNewClass != 0)
        {
            pQMapNode =
                QoSUtlGetQMapEntry (pInProActEntry->u4QoSInProfExcNewClass);
            if (pQMapNode != NULL)
            {
                pInProActEntry->u4QoSInProfExcTrafficClass = pQMapNode->u4QId;
            }
        }
    }

    if (pOutProActEntry != NULL)
    {
        /* Set default traffic Class as 1 */
        pOutProActEntry->u4QoSOutProfileTrafficClass = 1;
        if (pOutProActEntry->u4QoSOutProfileNewClass != 0)
        {
            pQMapNode =
                QoSUtlGetQMapEntry (pOutProActEntry->u4QoSOutProfileNewClass);
            if (pQMapNode != NULL)
            {
                pOutProActEntry->u4QoSOutProfileTrafficClass = pQMapNode->u4QId;
            }
        }
    }
    pEntry->pClsMapEntry = pClsMapEntry;
    pEntry->pPlyMapEntry = pPlyMapEntry;
    pEntry->pInProActEntry = pInProActEntry;
    pEntry->pOutProActEntry = pOutProActEntry;
    pEntry->pMeterEntry = pMeterEntry;
    pEntry->u1Flag = u1Flag;

    i4RetVal = NpUtilHwProgram (&FsHwNp);
    if (i4RetVal == FNP_FAILURE || i4RetVal == FNP_NOT_SUPPORTED)
    {
        return (i4RetVal);
    }
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : QosxQoSHwUnmapClassFromPolicy                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes QoSHwUnmapClassFromPolicy
 *                                                                          
 *    Input(s)            : Arguments of QoSHwUnmapClassFromPolicy
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/
#undef QosxQoSHwUnmapClassFromPolicy
UINT1
QosxQoSHwUnmapClassFromPolicy (tQoSClassMapEntry * pClsMapEntry,
                               tQoSPolicyMapEntry * pPlyMapEntry,
                               tQoSMeterEntry * pMeterEntry, UINT1 u1Flag)
{
    tFsHwNp             FsHwNp;
    tQosxNpModInfo     *pQosxNpModInfo = NULL;
    tQosxNpWrQoSHwUnmapClassFromPolicy *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_QOSX_MOD,    /* Module ID */
                         QO_S_HW_UNMAP_CLASS_FROM_POLICY,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pQosxNpModInfo = &(FsHwNp.QosxNpModInfo);
    pEntry = &pQosxNpModInfo->QosxNpQoSHwUnmapClassFromPolicy;

    pEntry->pClsMapEntry = pClsMapEntry;
    pEntry->pPlyMapEntry = pPlyMapEntry;
    pEntry->pMeterEntry = pMeterEntry;
    pEntry->u1Flag = u1Flag;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : QosxQoSHwDeleteClassMapEntry                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes QoSHwDeleteClassMapEntry
 *                                                                          
 *    Input(s)            : Arguments of QoSHwDeleteClassMapEntry
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/
#undef QosxQoSHwDeleteClassMapEntry
UINT1
QosxQoSHwDeleteClassMapEntry (tQoSClassMapEntry * pClsMapEntry)
{
    tFsHwNp             FsHwNp;
    tQosxNpModInfo     *pQosxNpModInfo = NULL;
    tQosxNpWrQoSHwDeleteClassMapEntry *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_QOSX_MOD,    /* Module ID */
                         QO_S_HW_DELETE_CLASS_MAP_ENTRY,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pQosxNpModInfo = &(FsHwNp.QosxNpModInfo);
    pEntry = &pQosxNpModInfo->QosxNpQoSHwDeleteClassMapEntry;

    pEntry->pClsMapEntry = pClsMapEntry;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : QosxQoSHwMeterCreate                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes QoSHwMeterCreate
 *                                                                          
 *    Input(s)            : Arguments of QoSHwMeterCreate
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/
#undef QosxQoSHwMeterCreate
UINT1
QosxQoSHwMeterCreate (tQoSMeterEntry * pMeterEntry)
{
    tFsHwNp             FsHwNp;
    tQosxNpModInfo     *pQosxNpModInfo = NULL;
    tQosxNpWrQoSHwMeterCreate *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_QOSX_MOD,    /* Module ID */
                         QO_S_HW_METER_CREATE,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pQosxNpModInfo = &(FsHwNp.QosxNpModInfo);
    pEntry = &pQosxNpModInfo->QosxNpQoSHwMeterCreate;

    pEntry->pMeterEntry = pMeterEntry;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : QosxQoSHwMeterDelete                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes QoSHwMeterDelete
 *                                                                          
 *    Input(s)            : Arguments of QoSHwMeterDelete
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/
#undef QosxQoSHwMeterDelete
UINT1
QosxQoSHwMeterDelete (INT4 i4MeterId)
{
    tFsHwNp             FsHwNp;
    tQosxNpModInfo     *pQosxNpModInfo = NULL;
    tQosxNpWrQoSHwMeterDelete *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_QOSX_MOD,    /* Module ID */
                         QO_S_HW_METER_DELETE,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pQosxNpModInfo = &(FsHwNp.QosxNpModInfo);
    pEntry = &pQosxNpModInfo->QosxNpQoSHwMeterDelete;

    pEntry->i4MeterId = i4MeterId;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}

/***************************************************************************
 *
 *    Function Name       : QosxQoSHwMeterStatsUpdate
 *
 *    Description         : This function using its arguments populates the
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes QoSHwMeterStatCreate
 *
 *    Input(s)            : Arguments of QoSHwMeterStatCreate
 *
 *    Output(s)           : None
 *
 *    Global Var Referred : None
 *
 *    Global Var Modified : None.
 *
 *    Use of Recursion    : None.
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE
 *
 *****************************************************************************/
#undef QosxQoSHwMeterStatsUpdate
UINT1
QosxQoSHwMeterStatsUpdate (UINT4 u4HwFilterId,tQoSMeterEntry * pMeterEntry)
{
    tFsHwNp             FsHwNp;
    tQosxNpModInfo     *pQosxNpModInfo = NULL;
    tQosxNpWrQoSHwMeterStatsUpdate *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_QOSX_MOD,    /* Module ID */
                         QO_S_HW_METER_STAT_UPDATE,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pQosxNpModInfo = &(FsHwNp.QosxNpModInfo);
    pEntry = &pQosxNpModInfo->QosxNpQoSHwMeterStatsUpdate;

    pEntry->pMeterEntry = pMeterEntry;
    pEntry->u4HwFilterId = u4HwFilterId;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}

/***************************************************************************
 *
 *    Function Name       : QosxQoSHwMeterStatsClear
 *
 *    Description         : This function using its arguments populates the
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes QoSHwMeterDelete
 *
 *    Input(s)            : Arguments of QoSHwMeterStatsDelete
 *
 *    Output(s)           : None
 *
 *    Global Var Referred : None
 *
 *    Global Var Modified : None.
 *
 *    Use of Recursion    : None.
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE
 *
 *****************************************************************************/
#undef QosxQoSHwMeterStatsClear
UINT1
QosxQoSHwMeterStatsClear (INT4 i4MeterId)
{
    tFsHwNp             FsHwNp;
    tQosxNpModInfo     *pQosxNpModInfo = NULL;
    tQosxNpWrQoSHwMeterClear *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_QOSX_MOD,    /* Module ID */
                         QO_S_HW_METER_STAT_CLEAR,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pQosxNpModInfo = &(FsHwNp.QosxNpModInfo);
    pEntry = &pQosxNpModInfo->QosxNpQoSHwMeterStatsClear;

    pEntry->i4MeterId = i4MeterId;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : QosxQoSHwSchedulerAdd                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes QoSHwSchedulerAdd
 *                                                                          
 *    Input(s)            : Arguments of QoSHwSchedulerAdd
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/
#undef QosxQoSHwSchedulerAdd
UINT1
QosxQoSHwSchedulerAdd (tQoSSchedulerEntry * pSchedEntry)
{
    tFsHwNp             FsHwNp;
    INT4                i4RetVal = FNP_ZERO;
    tQosxNpModInfo     *pQosxNpModInfo = NULL;
    tQosxNpWrQoSHwSchedulerAdd *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_QOSX_MOD,    /* Module ID */
                         QO_S_HW_SCHEDULER_ADD,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pQosxNpModInfo = &(FsHwNp.QosxNpModInfo);
    pEntry = &pQosxNpModInfo->QosxNpQoSHwSchedulerAdd;

    pEntry->pSchedEntry = pSchedEntry;

    i4RetVal = NpUtilHwProgram (&FsHwNp);
    return (i4RetVal);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : QosxQoSHwSchedulerUpdateParams                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes QoSHwSchedulerUpdateParams
 *                                                                          
 *    Input(s)            : Arguments of QoSHwSchedulerUpdateParams
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/
#undef QosxQoSHwSchedulerUpdateParams
UINT1
QosxQoSHwSchedulerUpdateParams (tQoSSchedulerEntry * pSchedEntry)
{
    tFsHwNp             FsHwNp;
    tQosxNpModInfo     *pQosxNpModInfo = NULL;
    tQosxNpWrQoSHwSchedulerUpdateParams *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_QOSX_MOD,    /* Module ID */
                         QO_S_HW_SCHEDULER_UPDATE_PARAMS,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pQosxNpModInfo = &(FsHwNp.QosxNpModInfo);
    pEntry = &pQosxNpModInfo->QosxNpQoSHwSchedulerUpdateParams;

    pEntry->pSchedEntry = pSchedEntry;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : QosxQoSHwSchedulerDelete                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes QoSHwSchedulerDelete
 *                                                                          
 *    Input(s)            : Arguments of QoSHwSchedulerDelete
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/
#undef QosxQoSHwSchedulerDelete
UINT1
QosxQoSHwSchedulerDelete (INT4 i4IfIndex, UINT4 u4SchedId)
{
    tFsHwNp             FsHwNp;
    tQosxNpModInfo     *pQosxNpModInfo = NULL;
    tQosxNpWrQoSHwSchedulerDelete *pEntry = NULL;
    INT4                i4RetVal = 0;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_QOSX_MOD,    /* Module ID */
                         QO_S_HW_SCHEDULER_DELETE,    /* Function/OpCode */
                         i4IfIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pQosxNpModInfo = &(FsHwNp.QosxNpModInfo);
    pEntry = &pQosxNpModInfo->QosxNpQoSHwSchedulerDelete;

    pEntry->i4IfIndex = i4IfIndex;
    pEntry->u4SchedId = u4SchedId;
    
    i4RetVal = NpUtilHwProgram (&FsHwNp);
    if (FNP_FAILURE == i4RetVal || FNP_NOT_SUPPORTED == i4RetVal )
    {
        return (i4RetVal);
    }
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : QosxQoSHwQueueCreate                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes QoSHwQueueCreate
 *                                                                          
 *    Input(s)            : Arguments of QoSHwQueueCreate
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/
#undef QosxQoSHwQueueCreate
UINT1
QosxQoSHwQueueCreate (INT4 i4IfIndex, UINT4 u4QId, tQoSQEntry * pQEntry,
                      tQoSQtypeEntry * pQTypeEntry,
                      tQoSREDCfgEntry * *papRDCfgEntry, INT2 i2HL)
{
    tFsHwNp             FsHwNp;
    tQosxNpModInfo     *pQosxNpModInfo = NULL;
    tQosxNpWrQoSHwQueueCreate *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_QOSX_MOD,    /* Module ID */
                         QO_S_HW_QUEUE_CREATE,    /* Function/OpCode */
                         i4IfIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pQosxNpModInfo = &(FsHwNp.QosxNpModInfo);
    pEntry = &pQosxNpModInfo->QosxNpQoSHwQueueCreate;

    pEntry->i4IfIndex = i4IfIndex;
    pEntry->u4QId = u4QId;
    pEntry->pQEntry = pQEntry;
    pEntry->pQTypeEntry = pQTypeEntry;
    pEntry->papRDCfgEntry = papRDCfgEntry;
    pEntry->i2HL = i2HL;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : QosxQoSHwQueueDelete                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes QoSHwQueueDelete
 *                                                                          
 *    Input(s)            : Arguments of QoSHwQueueDelete
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/
#undef QosxQoSHwQueueDelete
UINT1
QosxQoSHwQueueDelete (INT4 i4IfIndex, UINT4 u4Id)
{
    tFsHwNp             FsHwNp;
    tQosxNpModInfo     *pQosxNpModInfo = NULL;
    tQosxNpWrQoSHwQueueDelete *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_QOSX_MOD,    /* Module ID */
                         QO_S_HW_QUEUE_DELETE,    /* Function/OpCode */
                         i4IfIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pQosxNpModInfo = &(FsHwNp.QosxNpModInfo);
    pEntry = &pQosxNpModInfo->QosxNpQoSHwQueueDelete;

    pEntry->i4IfIndex = i4IfIndex;
    pEntry->u4Id = u4Id;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : QosxQoSHwMapClassToQueue                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes QoSHwMapClassToQueue
 *                                                                          
 *    Input(s)            : Arguments of QoSHwMapClassToQueue
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/
#undef QosxQoSHwMapClassToQueue
UINT1
QosxQoSHwMapClassToQueue (INT4 i4IfIndex, INT4 i4ClsOrPriType, UINT4 u4ClsOrPri,
                          UINT4 u4QId, UINT1 u1Flag)
{
    tFsHwNp             FsHwNp;
    tQosxNpModInfo     *pQosxNpModInfo = NULL;
    tQosxNpWrQoSHwMapClassToQueue *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_QOSX_MOD,    /* Module ID */
                         QO_S_HW_MAP_CLASS_TO_QUEUE,    /* Function/OpCode */
                         i4IfIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pQosxNpModInfo = &(FsHwNp.QosxNpModInfo);
    pEntry = &pQosxNpModInfo->QosxNpQoSHwMapClassToQueue;

    pEntry->i4IfIndex = i4IfIndex;
    pEntry->i4ClsOrPriType = i4ClsOrPriType;
    pEntry->u4ClsOrPri = u4ClsOrPri;
    pEntry->u4QId = u4QId;
    pEntry->u1Flag = u1Flag;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : QosxQoSHwMapClassToQueueId                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes QoSHwMapClassToQueueId
 *                                                                          
 *    Input(s)            : Arguments of QoSHwMapClassToQueueId
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/
#undef QosxQoSHwMapClassToQueueId
UINT1
QosxQoSHwMapClassToQueueId (tQoSClassMapEntry * pClsMapEntry, INT4 i4IfIndex,
                            INT4 i4ClsOrPriType, UINT4 u4ClsOrPri, UINT4 u4QId,
                            UINT1 u1Flag)
{
    tFsHwNp             FsHwNp;
    tQosxNpModInfo     *pQosxNpModInfo = NULL;
    tQosxNpWrQoSHwMapClassToQueueId *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_QOSX_MOD,    /* Module ID */
                         QO_S_HW_MAP_CLASS_TO_QUEUE_ID,    /* Function/OpCode */
                         i4IfIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pQosxNpModInfo = &(FsHwNp.QosxNpModInfo);
    pEntry = &pQosxNpModInfo->QosxNpQoSHwMapClassToQueueId;

    pEntry->pClsMapEntry = pClsMapEntry;
    pEntry->i4IfIndex = i4IfIndex;
    pEntry->i4ClsOrPriType = i4ClsOrPriType;
    pEntry->u4ClsOrPri = u4ClsOrPri;
    pEntry->u4QId = u4QId;
    pEntry->u1Flag = u1Flag;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : QosxQoSHwSchedulerHierarchyMap                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes QoSHwSchedulerHierarchyMap
 *                                                                          
 *    Input(s)            : Arguments of QoSHwSchedulerHierarchyMap
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/
#undef QosxQoSHwSchedulerHierarchyMap
UINT1
QosxQoSHwSchedulerHierarchyMap (INT4 i4IfIndex, UINT4 u4SchedId,
                                UINT2 u2Sweight, UINT1 u1Spriority,
                                UINT4 u4NextSchedId, UINT4 u4NextQId, INT2 i2HL,
                                UINT1 u1Flag)
{
    tFsHwNp             FsHwNp;
    tQosxNpModInfo     *pQosxNpModInfo = NULL;
    tQosxNpWrQoSHwSchedulerHierarchyMap *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_QOSX_MOD,    /* Module ID */
                         QO_S_HW_SCHEDULER_HIERARCHY_MAP,    /* Function/OpCode */
                         i4IfIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pQosxNpModInfo = &(FsHwNp.QosxNpModInfo);
    pEntry = &pQosxNpModInfo->QosxNpQoSHwSchedulerHierarchyMap;

    pEntry->i4IfIndex = i4IfIndex;
    pEntry->u4SchedId = u4SchedId;
    pEntry->u2Sweight = u2Sweight;
    pEntry->u1Spriority = u1Spriority;
    pEntry->u4NextSchedId = u4NextSchedId;
    pEntry->u4NextQId = u4NextQId;
    pEntry->i2HL = i2HL;
    pEntry->u1Flag = u1Flag;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : QosxQoSHwSetDefUserPriority                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes QoSHwSetDefUserPriority
 *                                                                          
 *    Input(s)            : Arguments of QoSHwSetDefUserPriority
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/
#undef QosxQoSHwSetDefUserPriority
UINT1
QosxQoSHwSetDefUserPriority (INT4 i4Port, INT4 i4DefPriority)
{
    tFsHwNp             FsHwNp;
    tQosxNpModInfo     *pQosxNpModInfo = NULL;
    tQosxNpWrQoSHwSetDefUserPriority *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_QOSX_MOD,    /* Module ID */
                         QO_S_HW_SET_DEF_USER_PRIORITY,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         1,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pQosxNpModInfo = &(FsHwNp.QosxNpModInfo);
    pEntry = &pQosxNpModInfo->QosxNpQoSHwSetDefUserPriority;

    pEntry->i4Port = i4Port;
    pEntry->i4DefPriority = i4DefPriority;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : QosxQoSHwGetMeterStats                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes QoSHwGetMeterStats
 *                                                                          
 *    Input(s)            : Arguments of QoSHwGetMeterStats
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
QosxQoSHwGetMeterStats (UINT4 u4MeterId, UINT4 u4StatsType,
                        tSNMP_COUNTER64_TYPE * pu8MeterStatsCounter)
{
    tFsHwNp             FsHwNp;
    tQosxNpModInfo     *pQosxNpModInfo = NULL;
    tQosxNpWrQoSHwGetMeterStats *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_QOSX_MOD,    /* Module ID */
                         QO_S_HW_GET_METER_STATS,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pQosxNpModInfo = &(FsHwNp.QosxNpModInfo);
    pEntry = &pQosxNpModInfo->QosxNpQoSHwGetMeterStats;

    pEntry->u4MeterId = u4MeterId;
    pEntry->u4StatsType = u4StatsType;
    pEntry->pu8MeterStatsCounter = pu8MeterStatsCounter;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : QosxQoSHwGetCoSQStats                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes QoSHwGetCoSQStats
 *                                                                          
 *    Input(s)            : Arguments of QoSHwGetCoSQStats
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
QosxQoSHwGetCoSQStats (INT4 i4IfIndex, UINT4 u4QId, UINT4 u4StatsType,
                       tSNMP_COUNTER64_TYPE * pu8CoSQStatsCounter)
{
    tFsHwNp             FsHwNp;
    tQosxNpModInfo     *pQosxNpModInfo = NULL;
    tQosxNpWrQoSHwGetCoSQStats *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_QOSX_MOD,    /* Module ID */
                         QO_S_HW_GET_CO_S_Q_STATS,    /* Function/OpCode */
                         i4IfIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pQosxNpModInfo = &(FsHwNp.QosxNpModInfo);
    pEntry = &pQosxNpModInfo->QosxNpQoSHwGetCoSQStats;

    pEntry->i4IfIndex = i4IfIndex;
    pEntry->u4QId = u4QId;
    pEntry->u4StatsType = u4StatsType;
    pEntry->pu8CoSQStatsCounter = pu8CoSQStatsCounter;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : QosxFsQosHwConfigPfc                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsQosHwConfigPfc
 *                                                                          
 *    Input(s)            : Arguments of FsQosHwConfigPfc
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/
#undef QosxFsQosHwConfigPfc
UINT1
QosxFsQosHwConfigPfc (tQosPfcHwEntry * pQosPfcHwEntry)
{
    tFsHwNp             FsHwNp;
    tQosxNpModInfo     *pQosxNpModInfo = NULL;
    tQosxNpWrFsQosHwConfigPfc *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_QOSX_MOD,    /* Module ID */
                         FS_QOS_HW_CONFIG_PFC,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pQosxNpModInfo = &(FsHwNp.QosxNpModInfo);
    pEntry = &pQosxNpModInfo->QosxNpFsQosHwConfigPfc;

    pEntry->pQosPfcHwEntry = pQosPfcHwEntry;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : QosxQoSHwGetCountActStats                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes QoSHwGetCountActStats
 *                                                                          
 *    Input(s)            : Arguments of QoSHwGetCountActStats
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
QosxQoSHwGetCountActStats (UINT4 u4DiffServId, UINT4 u4StatsType,
                           tSNMP_COUNTER64_TYPE * pu8RetValDiffServCounter)
{
    tFsHwNp             FsHwNp;
    tQosxNpModInfo     *pQosxNpModInfo = NULL;
    tQosxNpWrQoSHwGetCountActStats *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_QOSX_MOD,    /* Module ID */
                         QO_S_HW_GET_COUNT_ACT_STATS,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pQosxNpModInfo = &(FsHwNp.QosxNpModInfo);
    pEntry = &pQosxNpModInfo->QosxNpQoSHwGetCountActStats;

    pEntry->u4DiffServId = u4DiffServId;
    pEntry->u4StatsType = u4StatsType;
    pEntry->pu8RetValDiffServCounter = pu8RetValDiffServCounter;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : QosxQoSHwGetAlgDropStats                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes QoSHwGetAlgDropStats
 *                                                                          
 *    Input(s)            : Arguments of QoSHwGetAlgDropStats
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
QosxQoSHwGetAlgDropStats (UINT4 u4DiffServId, UINT4 u4StatsType,
                          tSNMP_COUNTER64_TYPE * pu8RetValDiffServCounter)
{
    tFsHwNp             FsHwNp;
    tQosxNpModInfo     *pQosxNpModInfo = NULL;
    tQosxNpWrQoSHwGetAlgDropStats *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_QOSX_MOD,    /* Module ID */
                         QO_S_HW_GET_ALG_DROP_STATS,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pQosxNpModInfo = &(FsHwNp.QosxNpModInfo);
    pEntry = &pQosxNpModInfo->QosxNpQoSHwGetAlgDropStats;

    pEntry->u4DiffServId = u4DiffServId;
    pEntry->u4StatsType = u4StatsType;
    pEntry->pu8RetValDiffServCounter = pu8RetValDiffServCounter;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : QosxQoSHwGetRandomDropStats                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes QoSHwGetRandomDropStats
 *                                                                          
 *    Input(s)            : Arguments of QoSHwGetRandomDropStats
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
QosxQoSHwGetRandomDropStats (UINT4 u4DiffServId, UINT4 u4StatsType,
                             tSNMP_COUNTER64_TYPE * pu8RetValDiffServCounter)
{
    tFsHwNp             FsHwNp;
    tQosxNpModInfo     *pQosxNpModInfo = NULL;
    tQosxNpWrQoSHwGetRandomDropStats *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_QOSX_MOD,    /* Module ID */
                         QO_S_HW_GET_RANDOM_DROP_STATS,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pQosxNpModInfo = &(FsHwNp.QosxNpModInfo);
    pEntry = &pQosxNpModInfo->QosxNpQoSHwGetRandomDropStats;

    pEntry->u4DiffServId = u4DiffServId;
    pEntry->u4StatsType = u4StatsType;
    pEntry->pu8RetValDiffServCounter = pu8RetValDiffServCounter;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : QosxQoSHwSetPbitPreferenceOverDscp                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes QoSHwSetPbitPreferenceOverDscp
 *                                                                          
 *    Input(s)            : Arguments of QoSHwSetPbitPreferenceOverDscp
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
QosxQoSHwSetPbitPreferenceOverDscp (INT4 i4Port, INT4 i4PbitPref)
{
    tFsHwNp             FsHwNp;
    tQosxNpModInfo     *pQosxNpModInfo = NULL;
    tQosxNpWrQoSHwSetPbitPreferenceOverDscp *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_QOSX_MOD,    /* Module ID */
                         QO_S_HW_SET_PBIT_PREFERENCE_OVER_DSCP,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         1,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pQosxNpModInfo = &(FsHwNp.QosxNpModInfo);
    pEntry = &pQosxNpModInfo->QosxNpQoSHwSetPbitPreferenceOverDscp;

    pEntry->i4Port = i4Port;
    pEntry->i4PbitPref = i4PbitPref;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : QosxQoSHwSetCpuRateLimit                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes QoSHwSetCpuRateLimit
 *                                                                          
 *    Input(s)            : Arguments of QoSHwSetCpuRateLimit
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/
#undef QosxQoSHwSetCpuRateLimit
UINT1
QosxQoSHwSetCpuRateLimit (INT4 i4CpuQueueId, UINT4 u4MinRate, UINT4 u4MaxRate)
{
    tFsHwNp             FsHwNp;
    tQosxNpModInfo     *pQosxNpModInfo = NULL;
    tQosxNpWrQoSHwSetCpuRateLimit *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_QOSX_MOD,    /* Module ID */
                         QO_S_HW_SET_CPU_RATE_LIMIT,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pQosxNpModInfo = &(FsHwNp.QosxNpModInfo);
    pEntry = &pQosxNpModInfo->QosxNpQoSHwSetCpuRateLimit;

    pEntry->i4CpuQueueId = i4CpuQueueId;
    pEntry->u4MinRate = u4MinRate;
    pEntry->u4MaxRate = u4MaxRate;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : QosxQoSHwMapClasstoPriMap                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes QoSHwMapClasstoPriMap
 *                                                                          
 *    Input(s)            : Arguments of QoSHwMapClasstoPriMap
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
QosxQoSHwMapClasstoPriMap (tQosClassToPriMapEntry * pQosClassToPriMapEntry)
{
    tFsHwNp             FsHwNp;
    tQosxNpModInfo     *pQosxNpModInfo = NULL;
    tQosxNpWrQoSHwMapClasstoPriMap *pEntry = NULL;
    INT4                i4RetVal = 0;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_QOSX_MOD,    /* Module ID */
                         QO_S_HW_MAP_CLASSTO_PRI_MAP,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pQosxNpModInfo = &(FsHwNp.QosxNpModInfo);
    pEntry = &pQosxNpModInfo->QosxNpQoSHwMapClasstoPriMap;

    pEntry->pQosClassToPriMapEntry = pQosClassToPriMapEntry;

    i4RetVal = NpUtilHwProgram (&FsHwNp);
    if (i4RetVal == FNP_FAILURE || i4RetVal == FNP_NOT_SUPPORTED)
    {
        return (i4RetVal);
    }
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : QosxQosHwMapClassToIntPriority                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes QosHwMapClassToIntPriority
 *                                                                          
 *    Input(s)            : Arguments of QosHwMapClassToIntPriority
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
QosxQosHwMapClassToIntPriority (tQosClassToIntPriEntry * pQosClassToIntPriEntry)
{
    INT4                i4RetVal = FNP_ZERO;
    tFsHwNp             FsHwNp;
    tQosxNpModInfo     *pQosxNpModInfo = NULL;
    tQosxNpWrQosHwMapClassToIntPriority *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_QOSX_MOD,    /* Module ID */
                         QOS_HW_MAP_CLASS_TO_INT_PRIORITY,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pQosxNpModInfo = &(FsHwNp.QosxNpModInfo);
    pEntry = &pQosxNpModInfo->QosxNpQosHwMapClassToIntPriority;

    pEntry->pQosClassToIntPriEntry = pQosClassToIntPriEntry;

    i4RetVal = NpUtilHwProgram (&FsHwNp);
    return (i4RetVal);
}

#ifdef MBSM_WANTED

/***************************************************************************
 *                                                                          
 *    Function Name       : QosxQoSMbsmHwUpdatePolicyMapForClass                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes QoSMbsmHwUpdatePolicyMapForClass
 *                                                                          
 *    Input(s)            : Arguments of QoSMbsmHwUpdatePolicyMapForClass
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
QosxQoSMbsmHwUpdatePolicyMapForClass (tQoSClassMapEntry * pClsMapEntry,
                                      tQoSPolicyMapEntry * pPlyMapEntry,
                                      tQoSInProfileActionEntry * pInProActEntry,
                                      tQoSOutProfileActionEntry *
                                      pOutProActEntry,
                                      tQoSMeterEntry * pMeterEntry,
                                      tMbsmSlotInfo * pSlotInfo)
{
    tFsHwNp             FsHwNp;
    tQosxNpModInfo     *pQosxNpModInfo = NULL;
    tQosxNpWrQoSMbsmHwUpdatePolicyMapForClass *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_QOSX_MOD,    /* Module ID */
                         QO_S_MBSM_HW_UPDATE_POLICY_MAP_FOR_CLASS,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pQosxNpModInfo = &(FsHwNp.QosxNpModInfo);
    pEntry = &pQosxNpModInfo->QosxNpQoSMbsmHwUpdatePolicyMapForClass;

    pEntry->pClsMapEntry = pClsMapEntry;
    pEntry->pPlyMapEntry = pPlyMapEntry;
    pEntry->pInProActEntry = pInProActEntry;
    pEntry->pOutProActEntry = pOutProActEntry;
    pEntry->pMeterEntry = pMeterEntry;
    pEntry->pSlotInfo = pSlotInfo;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : QosxQoSMbsmHwQueueCreate                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes QoSMbsmHwQueueCreate
 *                                                                          
 *    Input(s)            : Arguments of QoSMbsmHwQueueCreate
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
QosxQoSMbsmHwQueueCreate (INT4 i4IfIndex, UINT4 u4QId, tQoSQEntry * pQEntry,
                          tQoSQtypeEntry * pQTypeEntry,
                          tQoSREDCfgEntry * *papRDCfgEntry, INT2 i2HL,
                          tMbsmSlotInfo * pSlotInfo)
{
    tFsHwNp             FsHwNp;
    tQosxNpModInfo     *pQosxNpModInfo = NULL;
    tQosxNpWrQoSMbsmHwQueueCreate *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_QOSX_MOD,    /* Module ID */
                         QO_S_MBSM_HW_QUEUE_CREATE,    /* Function/OpCode */
                         i4IfIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pQosxNpModInfo = &(FsHwNp.QosxNpModInfo);
    pEntry = &pQosxNpModInfo->QosxNpQoSMbsmHwQueueCreate;

    pEntry->i4IfIndex = i4IfIndex;
    pEntry->u4QId = u4QId;
    pEntry->pQEntry = pQEntry;
    pEntry->pQTypeEntry = pQTypeEntry;
    pEntry->papRDCfgEntry = papRDCfgEntry;
    pEntry->i2HL = i2HL;
    pEntry->pSlotInfo = pSlotInfo;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : QosxQoSMbsmHwMapClassToQueue                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes QoSMbsmHwMapClassToQueue
 *                                                                          
 *    Input(s)            : Arguments of QoSMbsmHwMapClassToQueue
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
QosxQoSMbsmHwMapClassToQueue (INT4 i4IfIndex, INT4 i4ClsOrPriType,
                              UINT4 u4ClsOrPri, UINT4 u4QId, UINT1 u1Flag,
                              tMbsmSlotInfo * pSlotInfo)
{
    tFsHwNp             FsHwNp;
    tQosxNpModInfo     *pQosxNpModInfo = NULL;
    tQosxNpWrQoSMbsmHwMapClassToQueue *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_QOSX_MOD,    /* Module ID */
                         QO_S_MBSM_HW_MAP_CLASS_TO_QUEUE,    /* Function/OpCode */
                         i4IfIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pQosxNpModInfo = &(FsHwNp.QosxNpModInfo);
    pEntry = &pQosxNpModInfo->QosxNpQoSMbsmHwMapClassToQueue;

    pEntry->i4IfIndex = i4IfIndex;
    pEntry->i4ClsOrPriType = i4ClsOrPriType;
    pEntry->u4ClsOrPri = u4ClsOrPri;
    pEntry->u4QId = u4QId;
    pEntry->u1Flag = u1Flag;
    pEntry->pSlotInfo = pSlotInfo;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : QosxQoSMbsmHwMapClassToQueueId                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes QoSMbsmHwMapClassToQueueId
 *                                                                          
 *    Input(s)            : Arguments of QoSMbsmHwMapClassToQueueId
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
QosxQoSMbsmHwMapClassToQueueId (tQoSClassMapEntry * pClsMapEntry,
                                INT4 i4IfIndex, INT4 i4ClsOrPriType,
                                UINT4 u4ClsOrPri, UINT4 u4QId, UINT1 u1Flag,
                                tMbsmSlotInfo * pSlotInfo)
{
    tFsHwNp             FsHwNp;
    tQosxNpModInfo     *pQosxNpModInfo = NULL;
    tQosxNpWrQoSMbsmHwMapClassToQueueId *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_QOSX_MOD,    /* Module ID */
                         QO_S_MBSM_HW_MAP_CLASS_TO_QUEUE_ID,    /* Function/OpCode */
                         i4IfIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pQosxNpModInfo = &(FsHwNp.QosxNpModInfo);
    pEntry = &pQosxNpModInfo->QosxNpQoSMbsmHwMapClassToQueueId;

    pEntry->pClsMapEntry = pClsMapEntry;
    pEntry->i4IfIndex = i4IfIndex;
    pEntry->i4ClsOrPriType = i4ClsOrPriType;
    pEntry->u4ClsOrPri = u4ClsOrPri;
    pEntry->u4QId = u4QId;
    pEntry->u1Flag = u1Flag;
    pEntry->pSlotInfo = pSlotInfo;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : QosxFsQosMbsmHwConfigPfc                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsQosMbsmHwConfigPfc
 *                                                                          
 *    Input(s)            : Arguments of FsQosMbsmHwConfigPfc
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
QosxFsQosMbsmHwConfigPfc (tQosPfcHwEntry * pQosPfcHwEntry,
                          tMbsmSlotInfo * pSlotInfo)
{
    tFsHwNp             FsHwNp;
    tQosxNpModInfo     *pQosxNpModInfo = NULL;
    tQosxNpWrFsQosMbsmHwConfigPfc *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_QOSX_MOD,    /* Module ID */
                         FS_QOS_MBSM_HW_CONFIG_PFC,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pQosxNpModInfo = &(FsHwNp.QosxNpModInfo);
    pEntry = &pQosxNpModInfo->QosxNpFsQosMbsmHwConfigPfc;

    pEntry->pQosPfcHwEntry = pQosPfcHwEntry;
    pEntry->pSlotInfo = pSlotInfo;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : QosxQoSMbsmHwSchedulerAdd                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes QoSMbsmHwSchedulerAdd
 *                                                                          
 *    Input(s)            : Arguments of QoSMbsmHwSchedulerAdd
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
QosxQoSMbsmHwSchedulerAdd (tQoSSchedulerEntry * pSchedEntry,
                           tMbsmSlotInfo * pSlotInfo)
{
    tFsHwNp             FsHwNp;
    tQosxNpModInfo     *pQosxNpModInfo = NULL;
    tQosxNpWrQoSMbsmHwSchedulerAdd *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_QOSX_MOD,    /* Module ID */
                         QO_S_MBSM_HW_SCHEDULER_ADD,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pQosxNpModInfo = &(FsHwNp.QosxNpModInfo);
    pEntry = &pQosxNpModInfo->QosxNpQoSMbsmHwSchedulerAdd;

    pEntry->pSchedEntry = pSchedEntry;
    pEntry->pSlotInfo = pSlotInfo;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}

/***************************************************************************
 *
 *    Function Name       : QosxQoSMbsmHwSchedulerAdd
 *
 *    Description         : This function using its arguments populates the
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes QoSMbsmHwSchedulerAdd
 *
 *    Input(s)            : Arguments of QoSMbsmHwSchedulerAdd
 *
 *    Output(s)           : None
 *
 *    Global Var Referred : None
 *
 *    Global Var Modified : None.
 *
 *    Use of Recursion    : None.
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE
 *
 *****************************************************************************/
UINT1
QosxQoSMbsmHwMeterCreate (tQoSMeterEntry * pMeterEntry,
                          tMbsmSlotInfo * pSlotInfo)
{
    tFsHwNp             FsHwNp;
    tQosxNpModInfo     *pQosxNpModInfo = NULL;
    tQosxNpWrQoSMbsmHwMeterCreate *pEntry = NULL;

    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_QOSX_MOD,    /* Module ID */
                         QO_S_MBSM_HW_METER_CREATE,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pQosxNpModInfo = &(FsHwNp.QosxNpModInfo);
    pEntry = &pQosxNpModInfo->QosxNpQoSMbsmHwMeterCreate;

    pEntry->pMeterEntry = pMeterEntry;
    pEntry->pSlotInfo = pSlotInfo;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : QosxQoSMbsmHwInit                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes QoSMbsmHwInit
 *                                                                          
 *    Input(s)            : Arguments of QoSMbsmHwInit
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
QosxQoSMbsmHwInit (tMbsmSlotInfo * pSlotInfo)
{
    tFsHwNp             FsHwNp;
    tQosxNpModInfo     *pQosxNpModInfo = NULL;
    tQosxNpWrQoSMbsmHwInit *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_QOSX_MOD,    /* Module ID */
                         QO_S_MBSM_HW_INIT,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pQosxNpModInfo = &(FsHwNp.QosxNpModInfo);
    pEntry = &pQosxNpModInfo->QosxNpQoSMbsmHwInit;

    pEntry->pSlotInfo = pSlotInfo;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : QosxQoSMbsmHwSetCpuRateLimit                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes QoSMbsmHwSetCpuRateLimit
 *                                                                          
 *    Input(s)            : Arguments of QoSMbsmHwSetCpuRateLimit
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
QosxQoSMbsmHwSetCpuRateLimit (INT4 i4CpuQueueId, UINT4 u4MinRate,
                              UINT4 u4MaxRate, tMbsmSlotInfo * pSlotInfo)
{
    tFsHwNp             FsHwNp;
    tQosxNpModInfo     *pQosxNpModInfo = NULL;
    tQosxNpWrQoSMbsmHwSetCpuRateLimit *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_QOSX_MOD,    /* Module ID */
                         QO_S_MBSM_HW_SET_CPU_RATE_LIMIT,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pQosxNpModInfo = &(FsHwNp.QosxNpModInfo);
    pEntry = &pQosxNpModInfo->QosxNpQoSMbsmHwSetCpuRateLimit;

    pEntry->i4CpuQueueId = i4CpuQueueId;
    pEntry->u4MinRate = u4MinRate;
    pEntry->u4MaxRate = u4MaxRate;
    pEntry->pSlotInfo = pSlotInfo;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : QosxQoSMbsmHwMapClassToPolicy                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes QoSMbsmHwMapClassToPolicy
 *                                                                          
 *    Input(s)            : Arguments of QoSMbsmHwMapClassToPolicy
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
QosxQoSMbsmHwMapClassToPolicy (tQoSClassMapEntry * pClsMapEntry,
                               tQoSPolicyMapEntry * pPlyMapEntry,
                               tQoSInProfileActionEntry * pInProActEntry,
                               tQoSOutProfileActionEntry * pOutProActEntry,
                               tQoSMeterEntry * pMeterEntry, UINT1 u1Flag,
                               tMbsmSlotInfo * pSlotInfo)
{
    tFsHwNp             FsHwNp;
    tQosxNpModInfo     *pQosxNpModInfo = NULL;
    tQosxNpWrQoSMbsmHwMapClassToPolicy *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_QOSX_MOD,    /* Module ID */
                         QO_S_MBSM_HW_MAP_CLASS_TO_POLICY,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pQosxNpModInfo = &(FsHwNp.QosxNpModInfo);
    pEntry = &pQosxNpModInfo->QosxNpQoSMbsmHwMapClassToPolicy;

    pEntry->pClsMapEntry = pClsMapEntry;
    pEntry->pPlyMapEntry = pPlyMapEntry;
    pEntry->pInProActEntry = pInProActEntry;
    pEntry->pOutProActEntry = pOutProActEntry;
    pEntry->pMeterEntry = pMeterEntry;
    pEntry->u1Flag = u1Flag;
    pEntry->pSlotInfo = pSlotInfo;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : QosxQoSMbsmHwSchedulerHierarchyMap                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes QoSMbsmHwSchedulerHierarchyMap
 *                                                                          
 *    Input(s)            : Arguments of QoSMbsmHwSchedulerHierarchyMap
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
QosxQoSMbsmHwSchedulerHierarchyMap (INT4 i4IfIndex, UINT4 u4SchedId,
                                    UINT2 u2Sweight, UINT1 u1Spriority,
                                    UINT4 u4NextSchedId, UINT4 u4NextQId,
                                    INT2 i2HL, UINT1 u1Flag,
                                    tMbsmSlotInfo * pSlotInfo)
{
    tFsHwNp             FsHwNp;
    tQosxNpModInfo     *pQosxNpModInfo = NULL;
    tQosxNpWrQoSMbsmHwSchedulerHierarchyMap *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_QOSX_MOD,    /* Module ID */
                         QO_S_MBSM_HW_SCHEDULER_HIERARCHY_MAP,    /* Function/OpCode */
                         i4IfIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pQosxNpModInfo = &(FsHwNp.QosxNpModInfo);
    pEntry = &pQosxNpModInfo->QosxNpQoSMbsmHwSchedulerHierarchyMap;

    pEntry->i4IfIndex = i4IfIndex;
    pEntry->u4SchedId = u4SchedId;
    pEntry->u2Sweight = u2Sweight;
    pEntry->u1Spriority = u1Spriority;
    pEntry->u4NextSchedId = u4NextSchedId;
    pEntry->u4NextQId = u4NextQId;
    pEntry->i2HL = i2HL;
    pEntry->u1Flag = u1Flag;
    pEntry->pSlotInfo = pSlotInfo;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : QosxQoSMbsmHwSchedulerUpdateParams                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes QoSMbsmHwSchedulerUpdateParams
 *                                                                          
 *    Input(s)            : Arguments of QoSMbsmHwSchedulerUpdateParams
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
QosxQoSMbsmHwSchedulerUpdateParams (tQoSSchedulerEntry * pSchedEntry,
                                    tMbsmSlotInfo * pSlotInfo)
{
    tFsHwNp             FsHwNp;
    tQosxNpModInfo     *pQosxNpModInfo = NULL;
    tQosxNpWrQoSMbsmHwSchedulerUpdateParams *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_QOSX_MOD,    /* Module ID */
                         QO_S_MBSM_HW_SCHEDULER_UPDATE_PARAMS,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pQosxNpModInfo = &(FsHwNp.QosxNpModInfo);
    pEntry = &pQosxNpModInfo->QosxNpQoSMbsmHwSchedulerUpdateParams;

    pEntry->pSchedEntry = pSchedEntry;
    pEntry->pSlotInfo = pSlotInfo;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : QosxQoSMbsmHwSetPbitPreferenceOverDscp                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes QoSMbsmHwSetPbitPreferenceOverDscp
 *                                                                          
 *    Input(s)            : Arguments of QoSMbsmHwSetPbitPreferenceOverDscp
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
QosxQoSMbsmHwSetPbitPreferenceOverDscp (INT4 i4Port, INT4 i4PbitPref,
                                        tMbsmSlotInfo * pSlotInfo)
{
    tFsHwNp             FsHwNp;
    tQosxNpModInfo     *pQosxNpModInfo = NULL;
    tQosxNpWrQoSMbsmHwSetPbitPreferenceOverDscp *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_QOSX_MOD,    /* Module ID */
                         QO_S_MBSM_HW_SET_PBIT_PREFERENCE_OVER_DSCP,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         1,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pQosxNpModInfo = &(FsHwNp.QosxNpModInfo);
    pEntry = &pQosxNpModInfo->QosxNpQoSMbsmHwSetPbitPreferenceOverDscp;

    pEntry->i4Port = i4Port;
    pEntry->i4PbitPref = i4PbitPref;
    pEntry->pSlotInfo = pSlotInfo;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : QosxQoSMbsmHwMapClasstoPriMap                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes QoSMbsmHwMapClasstoPriMap
 *                                                                          
 *    Input(s)            : Arguments of QoSMbsmHwMapClasstoPriMap
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
QosxQoSMbsmHwMapClasstoPriMap (tQosClassToPriMapEntry * pQosClassToPriMapEntry,
                               tMbsmSlotInfo * pSlotInfo)
{
    tFsHwNp             FsHwNp;
    tQosxNpModInfo     *pQosxNpModInfo = NULL;
    tQosxNpWrQoSMbsmHwMapClasstoPriMap *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_QOSX_MOD,    /* Module ID */
                         QO_S_MBSM_HW_MAP_CLASSTO_PRI_MAP,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pQosxNpModInfo = &(FsHwNp.QosxNpModInfo);
    pEntry = &pQosxNpModInfo->QosxNpQoSMbsmHwMapClasstoPriMap;

    pEntry->pQosClassToPriMapEntry = pQosClassToPriMapEntry;
    pEntry->pSlotInfo = pSlotInfo;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : QosxQosMbsmHwMapClassToIntPriority                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes QosMbsmHwMapClassToIntPriority
 *                                                                          
 *    Input(s)            : Arguments of QosMbsmHwMapClassToIntPriority
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
QosxQosMbsmHwMapClassToIntPriority (tQosClassToIntPriEntry *
                                    pQosClassToIntPriEntry,
                                    tMbsmSlotInfo * pSlotInfo)
{
    tFsHwNp             FsHwNp;
    tQosxNpModInfo     *pQosxNpModInfo = NULL;
    tQosxNpWrQosMbsmHwMapClassToIntPriority *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_QOSX_MOD,    /* Module ID */
                         QOS_MBSM_HW_MAP_CLASS_TO_INT_PRIORITY,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pQosxNpModInfo = &(FsHwNp.QosxNpModInfo);
    pEntry = &pQosxNpModInfo->QosxNpQosMbsmHwMapClassToIntPriority;

    pEntry->pQosClassToIntPriEntry = pQosClassToIntPriEntry;
    pEntry->pSlotInfo = pSlotInfo;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : QosxQoSMbsmHwSetDefUserPriority                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes QoSMbsmHwSetDefUserPriority
 *                                                                          
 *    Input(s)            : Arguments of QoSMbsmHwSetDefUserPriority
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
QosxQoSMbsmHwSetDefUserPriority (INT4 i4Port, INT4 i4DefPriority,
                                 tMbsmSlotInfo * pSlotInfo)
{
    tFsHwNp             FsHwNp;
    tQosxNpModInfo     *pQosxNpModInfo = NULL;
    tQosxNpWrQoSMbsmHwSetDefUserPriority *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_QOSX_MOD,    /* Module ID */
                         QO_S_MBSM_HW_SET_DEF_USER_PRIORITY,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         1,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pQosxNpModInfo = &(FsHwNp.QosxNpModInfo);
    pEntry = &pQosxNpModInfo->QosxNpQoSMbsmHwSetDefUserPriority;

    pEntry->i4Port = i4Port;
    pEntry->i4DefPriority = i4DefPriority;
    pEntry->pSlotInfo = pSlotInfo;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}
#endif /* MBSM_WANTED */
/***************************************************************************
 *
 *    Function Name       : QosxQoSHwGetDscpQueueMap
 *
 *    Description         : This function using its arguments populates the
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsQoSHwGetDscpQueueMap
 *
 *    Input(s)            : i4DscpVal - Dscp Value
 *                          i4QueueID - Pointer to QueueID
 *
 *    Output(s)           : None
 *
****************************************************************************/
UINT1
QosxQoSHwGetDscpQueueMap (INT4 i4DscpVal,INT4 *i4QueueID)

{
    tFsHwNp             FsHwNp;
    tQosxNpModInfo     *pQosxNpModInfo = NULL;
    tQosxNpWrQosHwGetDscpQueueMap  *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_QOSX_MOD,    /* Module ID */
                         QO_S_HW_GET_DSCP_Q_MAP,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pQosxNpModInfo = &(FsHwNp.QosxNpModInfo);
    pEntry = &pQosxNpModInfo->QosxNpQosHwGetDscpQueueMap;
    pEntry->i4QosDscpVal = i4DscpVal;
    pEntry->i4QosQueID   = i4QueueID;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}
/***************************************************************************
 *
 *    Function Name       : QosxQoSHwSetTrafficClassToPcp
 *
 *    Description         : This function using its arguments populates the
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsQoSHwGetDscpQueueMap
 *
 *    Input(s)            : i4IfIndex - Interface Index
 *                          i4UserPriority - User priority
 *                          i4TrafficClass - TrafficClass
 *
 *    Output(s)           : None
 *
****************************************************************************/
UINT1 QosxQoSHwSetTrafficClassToPcp (INT4 i4IfIndex, 
                                     INT4 i4UserPriority, 
                                     INT4 i4TrafficClass)
{
    tFsHwNp             FsHwNp;
    tQosxNpModInfo     *pQosxNpModInfo = NULL;
    tQosxNpWrQosHwSetTrafficClassToPcp *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_QOSX_MOD,    /* Module ID */
                         QO_S_HW_SET_TRAF_CLASS_TO_PCP,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         1,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pQosxNpModInfo = &(FsHwNp.QosxNpModInfo);
    pEntry = &pQosxNpModInfo->QosxNpQosHwSetTrafficClassToPcp;
    pEntry->i4IfIndex = i4IfIndex;
    pEntry->i4UserPriority = i4UserPriority;
    pEntry->i4TrafficClass = i4TrafficClass;
    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}

/***************************************************************************
 *
 *    Function Name       : QosxQoSHwSetVlanQueuingStatus
 *
 *    Description         : This function using its arguments populates the
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes QoSHwSetVlanQueuingStatus
 *
 *    Input(s)            : Arguments of QoSHwSetVlanQueuingStatus
 *
 *    Output(s)           : None
 *
 *    Global Var Referred : None
 *
 *    Global Var Modified : None.
 *
 *    Use of Recursion    : None.
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE
 *
 *****************************************************************************/
UINT1
QosxQoSHwSetVlanQueuingStatus (INT4 i4IfIndex, tQoSQEntry * pQEntrySubscriber)
{
    tFsHwNp             FsHwNp;
    tQosxNpModInfo     *pQosxNpModInfo = NULL;
    tQosxNpWrQoSHwSetVlanQueuingStatus *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_QOSX_MOD,    /* Module ID */
                         QO_S_HW_SET_VLAN_QUEUING_STATUS,    /* Function/OpCode */
                         i4IfIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pQosxNpModInfo = &(FsHwNp.QosxNpModInfo);
    pEntry = &pQosxNpModInfo->QosxNpQoSHwSetVlanQueuingStatus;

    pEntry->i4IfIndex = i4IfIndex;
    pEntry->pQEntrySubscriber = pQEntrySubscriber;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : QosxFsQosHwGetPfcStats                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsQosHwGetPfcStats
 *                                                                          
 *    Input(s)            : Arguments of FsQosHwGetPfcStats
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
QosxFsQosHwGetPfcStats (tQosPfcHwStats * pQosPfcHwStats)
{
    tFsHwNp             FsHwNp;
    tQosxNpModInfo     *pQosxNpModInfo = NULL;
    tQosxNpWrFsQosHwGetPfcStats *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_QOSX_MOD,    /* Module ID */
                         FS_QOS_HW_GET_PFC_STATS,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pQosxNpModInfo = &(FsHwNp.QosxNpModInfo);
    pEntry = &pQosxNpModInfo->QosxNpFsQosHwGetPfcStats;

    pEntry->pQosPfcHwStats = pQosPfcHwStats;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}
#endif /* __QOSX_NPAPI_C__ */
