/* $Id: qossys.c,v 1.66 2018/01/04 09:55:01 siva Exp $ */
/****************************************************************************/
/* Copyright (C) 2007 Aricent Inc . All Rights Reserved                     */
/*                                                                          */
/*  FILE NAME             : qossys.c                                        */
/*  PRINCIPAL AUTHOR      : Aricent                                         */
/*  SUBSYSTEM NAME        : QoS                                             */
/*  MODULE NAME           : QoS-SYS                                         */
/*  LANGUAGE              : C                                               */
/*  TARGET ENVIRONMENT    : Linux                                           */
/*  DATE OF FIRST RELEASE :                                                 */
/*  AUTHOR                : Aricent                                         */
/*  FUNCTIONS DEFINED(Applicable for Source files) :                        */
/*  DESCRIPTION           : This File contains the main functions; this     */
/*                          file contains routines for the global memory    */
/*                          initialisation and module initialisation        */
/*                          1. Memory Allocation  / Deletion                */
/*                          2. Semaphore Creation                           */
/*                          3. Table Initializations.                       */
/*                          for the Aricent QoS Module.                     */
/****************************************************************************/
/*                                                                          */
/*  Change History                                                          */
/*  Version               :                                                 */
/*  Date(DD/MM/YYYY)      :                                                 */
/*  Modified by           :                                                 */
/*  Description of change :                                                 */
/****************************************************************************/
#ifndef __QOS_SYS_C__
#define __QOS_SYS_C__

#include "qosinc.h"

/*****************************************************************************/
/* Function Name      : QoSInit                                              */
/* Description        : This function is used to Start the QoS Sub-Systme    */
/*                      during the System startup                            */
/*                      1. It Create Sema4 for QoS.                          */
/*                      2. It Call QoSStart                                  */
/*                      3. It Register QOS MIB with SNMP Agent               */
/*                      4. It Registers QOS with RM                          */
/* Input(s)           : i1Params  - UnUsed Param                             */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : gQoSGlobalInfo                                       */
/* Global Variables                                                          */
/* Modified           : gQoSGlobalInfo                                       */
/* Return Value(s)    : None                                                 */
/* Called By          : System Main Thread.                                  */
/* Calling Function   :                                                      */
/*****************************************************************************/
VOID
QoSInit (INT1 *i1Params)
{
    INT4                i4RetStatus = QOS_FAILURE;
    UINT4               u4EventMask = QOS_INIT_VAL;

#ifdef NPAPI_WANTED
    INT4                i4CosQ = 0;
    INT4                i4IfIndex = 0;
#endif

    UNUSED_PARAM (i1Params);
    MEMSET (&(gQoSGlobalInfo), 0, (sizeof (tQoSGlobalInfo)));

    /* Create Mutual exclusion semaphore for QoS */
    i4RetStatus =
        OsixSemCrt ((UINT1 *) QOS_SEM_NAME, &(gQoSGlobalInfo.QoSSemId));

    if (i4RetStatus != OSIX_SUCCESS)
    {
        QOS_TRC_ARG1 (OS_RESOURCE_TRC, "%s : Sema4 Creation Failed!.\r\n",
                      __FUNCTION__);

        QOS_INIT_COMPLETE (OSIX_FAILURE);

        return;
    }

    /* Semaphore is by default created with initial value 0. So GiveSem
     * is called before using the semaphore. */
    OsixSemGive (gQoSGlobalInfo.QoSSemId);

    OsixTskIdSelf (&(gQoSGlobalInfo.QoSTaskId));

    i4RetStatus = QoSStart ();
    if (i4RetStatus != QOS_SUCCESS)
    {
        QOS_TRC_ARG1 (OS_RESOURCE_TRC, "%s : QoSStart () Failed!.\r\n",
                      __FUNCTION__);

        QOS_INIT_COMPLETE (OSIX_FAILURE);

        return;
    }

    /* Register QOS MIB OID with SNMP Agent */
    RegisterFSQOSX ();
    RegisterSTDQOS ();

    /* Create Mempool for QOS Queue Message */
    if (MEM_FAILURE == MemCreateMemPool (sizeof (tQosQueueMsg),
                                         (QOS_QUEUE_SIZE),
                                         MEM_HEAP_MEMORY_TYPE,
                                         &(gQoSGlobalInfo.QosQPktMemPoolId)))
    {
        QOS_TRC (OS_RESOURCE_TRC, "Pool Creation Error!\n");
        QOS_INIT_COMPLETE (OSIX_FAILURE);
        return;
    }

    if (OsixQueCrt ((UINT1 *) "QXTQ", OSIX_MAX_Q_MSG_LEN,
                    QOS_QUEUE_SIZE, &(QOS_MSG_QUEUE_ID)) != OSIX_SUCCESS)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "%s : Creation of QOS Queue Failed!.\r\n",
                      __FUNCTION__);

        i4RetStatus = QoSShutdown ();
        if (i4RetStatus != QOS_SUCCESS)
        {
            QOS_TRC_ARG1 (MGMT_TRC, "%s : QoSShutdown () Failed!.\r\n",
                          __FUNCTION__);
        }
        QOS_INIT_COMPLETE (OSIX_FAILURE);

        return;

    }
    /*Register with SYSLOG Module */
    gQoSGlobalInfo.gi4QoSxSysLogId = SYS_LOG_REGISTER ((CONST UINT1 *) "QOSX",
                                                       SYSLOG_CRITICAL_LEVEL);

    /* Init the HARDWARE */
#ifndef RM_WANTED
#ifndef MBSM_WANTED
    i4RetStatus = QoSHwWrInit ();
    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (OS_RESOURCE_TRC, "%s : ERROR: QoSHwWrInit () Failed!. "
                      "\r\n", __FUNCTION__);
        QOS_INIT_COMPLETE (OSIX_FAILURE);
        return;
    }
#endif
#endif
#ifdef NPAPI_WANTED
    /* Initialize the default HW ID's for Queues */
    for (i4IfIndex = 0; i4IfIndex < SYS_DEF_MAX_PHYSICAL_INTERFACES;
         i4IfIndex++)
    {
        for (i4CosQ = 0; i4CosQ < QOS_QUEUE_ENTRY_MAX; i4CosQ++)
        {
            gu4QueueHwId[i4IfIndex][i4CosQ] = 0;
        }
    }
#endif

    if (gQoSGlobalInfo.eSysControl != QOS_SYS_CNTL_SHUTDOWN)
    {
        /* Default entries will be added only if the MSR database is not
         * restored. Or else default entries will be restored from the
         * MSR database. */
        if ((IssGetRestoreOptionFromNvRam () != ISS_CONFIG_RESTORE) ||
            (IssGetAutoSaveStatus () == ISS_TRUE))
        {
            /* >> When RM and MBSM are NOT defined, the QoSAddDefTblEntries() is called at QOSInit,
             * >> When RM wanted and MBSM=NO, QoSAddDefTblEntries () called from RM GO_ACTIVE
             * >> When RM = NO & MBSM is wanted . QoSAddDefTblEntries() should be called 
             *    from self attach*/

#ifndef RM_WANTED
#ifndef MBSM_WANTED
            /* Add Default Table Entries */
            i4RetStatus = QoSAddDefTblEntries ();
            if (i4RetStatus != QOS_SUCCESS)
            {
                QOS_TRC_ARG1 (MGMT_TRC,
                              "%s : QoSAddDefTblEntries () Failed!.\r\n",
                              __FUNCTION__);

                i4RetStatus = QoSShutdown ();
                if (i4RetStatus != QOS_SUCCESS)
                {
                    QOS_TRC_ARG1 (MGMT_TRC, "%s : QoSShutdown () Failed!.\r\n",
                                  __FUNCTION__);
                }
                QOS_INIT_COMPLETE (OSIX_FAILURE);

                return;

            }
#endif
#endif
            gQoSGlobalInfo.eSysStatus = QOS_SYS_STATUS_ENABLE;
            QoSEnable ();
        }
        /* Add CPU Rate limit Table entries */
        i4RetStatus = QoSAddDefCpuQTblEntries ();
        if (i4RetStatus == QOS_FAILURE)
        {
            QOS_INIT_COMPLETE (OSIX_FAILURE);
            return;
        }
    }

    /* Registration of QOS with RM */
    if (QosRedInitGlobalInfo () == OSIX_FAILURE)
    {
        QOS_TRC (INIT_SHUT_TRC | ALL_FAILURE_TRC, "QoSInit:"
                 " Redundancy Global Info Initialization FAILED !!!\r\n");
        QosRedDeInitGlobalInfo ();
    }

    QOS_INIT_COMPLETE (OSIX_SUCCESS);

    QOS_FOREVER ()
    {
        /* wait for the MSR thread to restore the stored configuration
         * and then set the qos enable*/
        QOS_RECV_EVENT (QOS_TASK_ID, MSR_RESTORE_COMPLETE_EVENT | QOS_MSG_EVENT,
                        OSIX_WAIT, &u4EventMask);
        QoSLock ();
        if (u4EventMask & MSR_RESTORE_COMPLETE_EVENT)
        {
        }
        if (u4EventMask & QOS_MSG_EVENT)
        {
            QosMainProcessQMsgEvent ();
        }
        QoSUnLock ();
    }
}

/*****************************************************************************/
/* Function Name      : QoSLock                                              */
/* Description        : This function is used to take the QoS  mutual        */
/*                      exclusion SEMa4 to avoid simultaneous access to      */
/*                      protocol data structures by the protocol task and    */
/*                      configuration task/thread.                           */
/* Input(s)           : None.                                                */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : gQoSGlobalInfo.QoSSemId                              */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : QOS_SUCCESS or QOS_FAILURE                           */
/* Called By          : Protocol Main and SNMP/CLI                           */
/* Calling Function   : SNMP,QoSMain                                         */
/*****************************************************************************/
INT4
QoSLock ()
{
    if (OsixSemTake (gQoSGlobalInfo.QoSSemId) != OSIX_SUCCESS)
    {
        QOS_TRC (OS_RESOURCE_TRC | ALL_FAILURE_TRC,
                 "Failed to Take QoS Mutual Exclusion Sema4.\r\n");
        return (QOS_FAILURE);
    }
    return (QOS_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSUnLock                                            */
/* Description        : This function is used to give the QoS mutual         */
/*                      exclusion SEMa4 to avoid simultaneous access to      */
/*                      protocol data structures by the protocol task and    */
/*                      configuration task/thread.                           */
/* Input(s)           : None.                                                */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : gQoSGlobalInfo.QoSSemId                              */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : QOS_SUCCESS or QOS_FAILURE                           */
/* Called By          : Protocol Main and SNMP/CLI                           */
/* Calling Function   : SNMP,QoSMain                                         */
/*****************************************************************************/
INT4
QoSUnLock ()
{
    if (OsixSemGive (gQoSGlobalInfo.QoSSemId) != OSIX_SUCCESS)
    {
        QOS_TRC (OS_RESOURCE_TRC | ALL_FAILURE_TRC,
                 "Failed to Give QoS Mutual Exclusion Sema4.\r\n");
        return (QOS_FAILURE);
    }
    return (QOS_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSInitGlobal                                        */
/* Description        : This function is used to Set Default Values for      */
/*                      Global variables                                     */
/* Input(s)           : None.                                                */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : gQoSGlobalInfo                                       */
/* Global Variables                                                          */
/* Modified           : gQoSGlobalInfo                                       */
/* Return Value(s)    : None                                                 */
/* Called By          : QoSStart,QoSShutdown                                 */
/* Calling Function   :                                                      */
/*****************************************************************************/
VOID
QoSInitGlobal (VOID)
{
    tOsixSemId          QoSSemId;
    tOsixTaskId         QoSTaskId;
    tMemPoolId          QosQPktMemPoolId;
    tQosRedGlobalInfo   TempRedGlobalInfo;

    /* Backup the Sem4 Id Clear all golbal values finally assign the Sem4Id */
    MEMSET (&(QoSSemId), 0, (sizeof (tOsixSemId)));
    MEMCPY (&(QoSSemId), &(gQoSGlobalInfo.QoSSemId), (sizeof (tOsixSemId)));

    /* Backup the Sem4 Id Clear all golbal values finally assign the Sem4Id */
    MEMSET (&(QoSTaskId), 0, (sizeof (tOsixTaskId)));
    MEMCPY (&(QoSTaskId), &(gQoSGlobalInfo.QoSTaskId), (sizeof (tOsixTaskId)));

    MEMSET (&(QosQPktMemPoolId), 0, (sizeof (tMemPoolId)));
    MEMCPY (&(QosQPktMemPoolId), &(gQoSGlobalInfo.QosQPktMemPoolId),
            (sizeof (tMemPoolId)));

    /* Backup the RedInitGlobal Info and finally assign back the global info */
    MEMSET (&(TempRedGlobalInfo), 0, (sizeof (tQosRedGlobalInfo)));
    MEMCPY (&(TempRedGlobalInfo), &(gQoSGlobalInfo.RedGlobalInfo),
            (sizeof (tQosRedGlobalInfo)));

    MEMSET (&(gQoSGlobalInfo), 0, (sizeof (tQoSGlobalInfo)));
    MEMSET (&(gQoSGlobalInfo.au1PrefTbl), QOS_PBIT_PREF_ENABLE,
            (sizeof (gQoSGlobalInfo.au1PrefTbl)));

    MEMCPY (&(gQoSGlobalInfo.QoSSemId), &(QoSSemId), (sizeof (tOsixSemId)));
    MEMCPY (&(gQoSGlobalInfo.QoSTaskId), &(QoSTaskId), (sizeof (tOsixTaskId)));
    MEMCPY (&(gQoSGlobalInfo.QosQPktMemPoolId), &(QosQPktMemPoolId),
            (sizeof (tMemPoolId)));
    MEMCPY (&(gQoSGlobalInfo.RedGlobalInfo), &(TempRedGlobalInfo),
            (sizeof (tQosRedGlobalInfo)));

    gQoSGlobalInfo.eSysControl = QOS_SYS_CNTL_START;
    gQoSGlobalInfo.eSysStatus = QOS_SYS_STATUS_ENABLE;
    gQoSGlobalInfo.eVlanQStatus = VLAN_QUEUEING_SYS_STATUS_DISABLE;
    gQoSGlobalInfo.u4RateUnit = QOS_RATE_UNIT;
    gQoSGlobalInfo.u4TrcFlag = QOS_TRC_MIN_LEVEL;
    gQoSGlobalInfo.u4RateGranularity = QOS_RATE_GRANULARITY;

    gQoSGlobalInfo.DSNextFree.u4DataPathNextFree = QOS_NEXT_INDEX;
    gQoSGlobalInfo.DSNextFree.u4ClfrNextFree = QOS_NEXT_INDEX;
    gQoSGlobalInfo.DSNextFree.u4ClfrElementNextFree = QOS_NEXT_INDEX;
    gQoSGlobalInfo.DSNextFree.u4MultiFieldNextFree =
        QOS_MIN_FILTER_ID + QOS_MIN_VALUE;
    gQoSGlobalInfo.DSNextFree.u4MeterNextFree = QOS_NEXT_INDEX;
    gQoSGlobalInfo.DSNextFree.u4TBParamNextFree = QOS_NEXT_INDEX;
    gQoSGlobalInfo.DSNextFree.u4ActionNextFree = QOS_NEXT_INDEX;
    gQoSGlobalInfo.DSNextFree.u4CountActNextFree = QOS_NEXT_INDEX;
    gQoSGlobalInfo.DSNextFree.u4AlgoDropNextFree =
        QOS_NEXT_INDEX + QOS_NEXT_INDEX;
    gQoSGlobalInfo.DSNextFree.u4RandomDropNextFree = QOS_NEXT_INDEX;
    gQoSGlobalInfo.DSNextFree.u4QNextFree = QOS_NEXT_INDEX;
    gQoSGlobalInfo.DSNextFree.u4SchedulerNextFree = QOS_NEXT_INDEX;
    gQoSGlobalInfo.DSNextFree.u4MinRateNextFree = QOS_NEXT_INDEX;
    gQoSGlobalInfo.DSNextFree.u4MaxRateNextFree = QOS_NEXT_INDEX;
    gQoSGlobalInfo.DSNextFree.u4TemplateNextFree = QOS_NEXT_INDEX;
}

/*****************************************************************************/
/* Function Name      : QoSStart                                             */
/* Description        : This function is used to Start the QoS Module.It     */
/*                      will do the following Actions                        */
/*                      1. Allocate Memory from the Resource Management      */
/*                         Module.                                           */
/*                      2. All Tables fro QoS Module will be Created         */
/* Input(s)           : None.                                                */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : gQoSGlobalInfo                                       */
/* Global Variables                                                          */
/* Modified           : gQoSGlobalInfo                                       */
/* Return Value(s)    : QOS_SUCCESS or QOS_FAILURE                           */
/* Called By          : SNMP                                                 */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QoSStart ()
{
    UINT4               u4RetStatus = QOS_FAILURE;

    QoSInitGlobal ();
    u4RetStatus = QoSCreateMemPools ();

    if (u4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (OS_RESOURCE_TRC, "%s : QoSCreateMemPools () Failed!.\r\n",
                      __FUNCTION__);

        return (QOS_FAILURE);
    }

    u4RetStatus = QoSCreateTables ();

    if (u4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (OS_RESOURCE_TRC, "%s : QoSCreateTables () Failed!.\r\n",
                      __FUNCTION__);

        QoSDeleteMemPools ();

        return (QOS_FAILURE);
    }

    return (QOS_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSShutdown                                          */
/* Description        : This function is used to Shutdown the QoS Module.It  */
/*                      will do the following Actions                        */
/*                      1. Deleting the Hardware Configurations.             */
/*                      2. Relasing the allocated Memory to the Resource     */
/*                         Management Module.                                */
/*                      3. System Control and System Status will set it as   */
/*                          SHUTDOWN and DISABEL Respectively.               */
/* Input(s)           : None.                                                */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : gQoSGlobalInfo                                       */
/* Global Variables                                                          */
/* Modified           : gQoSGlobalInfo                                       */
/* Return Value(s)    : QOS_SUCCESS or QOS_FAILURE                           */
/* Called By          : Protocol Main and SNMP/CLI                           */
/* Calling Function   : SNMP, QoSMain                                        */
/*****************************************************************************/
INT4
QoSShutdown ()
{
    INT4                i4RetStatus = QOS_FAILURE;

    /*Remove Hardware entries */
    if (gQoSGlobalInfo.eSysStatus == QOS_SYS_STATUS_ENABLE)
    {
        i4RetStatus = QoSDisable ();
        if (i4RetStatus == QOS_FAILURE)
        {
            return (QOS_FAILURE);
        }
    }

    /* This Function used to Remove Node from the Table or Tree and Relase 
     * the memory to the respective MemoryPool*/
    i4RetStatus = QoSDeleteTables ();
    if (i4RetStatus == QOS_FAILURE)
    {
        return (QOS_FAILURE);
    }

    /* Delete all MemoryPool */
    QoSDeleteMemPools ();

    /*Init Global structure */
    QoSInitGlobal ();

    /* Change the  System Control as SHUTDOWN */
    gQoSGlobalInfo.eSysControl = QOS_SYS_CNTL_SHUTDOWN;
    gQoSGlobalInfo.eSysStatus = QOS_SYS_STATUS_DISABLE;

    return (QOS_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSConfigMeter                                       */
/* Description        : This function is used to Add or Delete the Meter     */
/*                      Table ACTIVE Entries in the HARDWARE                 */
/*                      QOS_CFG_TYPE_CREATE - Add the Entries in HARDWARE    */
/*                      QOS_CFG_TYPE_DELETE - Del the Entries from HARDWARE  */
/* Input(s)           : u4CfgType - QOS_CFG_TYPE_CREATE / QOS_CFG_TYPE_DELETE*/
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : gQoSGlobalInfo                                       */
/* Global Variables                                                          */
/* Modified           : gQoSGlobalInfo                                       */
/* Return Value(s)    : QOS_SUCCESS or QOS_FAILURE                           */
/* Called By          : QoSShutdown                                          */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QoSConfigMeter (UINT4 u4CfgType)
{
    tQoSMeterNode      *pMeterNode = NULL;
    UINT4               u4MeterId = 0;
    UINT4               u4NextMeterId = 0;
    INT4                i4RetStatus = QOS_FAILURE;

    i4RetStatus = QoSGetNextMeterTblEntryIndex (u4MeterId, &u4NextMeterId);
    if (i4RetStatus == QOS_FAILURE)
    {
        /* The Table is Empty */
        return (QOS_SUCCESS);
    }

    /* Table is Populated */
    do
    {
        pMeterNode = QoSUtlGetMeterNode (u4NextMeterId);
        if (pMeterNode == NULL)
        {
            QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetMeterNode () "
                          " Returns NULL. \r\n", __FUNCTION__);
            return (QOS_FAILURE);
        }

        /* Only Active Node in the Meter Table need to Configure */
        if (pMeterNode->u1Status == ACTIVE)
        {
            if (u4CfgType == QOS_CFG_TYPE_CREATE)
            {
                /* 1. Add the entry into the HARDWARE. */
                i4RetStatus = QoSHwWrMeterCreate (pMeterNode);
                if (i4RetStatus == QOS_FAILURE)
                {
                    QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSHwWrMeterCreate () "
                                  " Returns FAILURE. \r\n", __FUNCTION__);
                    return (QOS_FAILURE);
                }
            }
            else
            {
                /* 1. Remove the entry from the HARDWARE. */
                i4RetStatus = QoSHwWrMeterDelete (pMeterNode);
                if (i4RetStatus == QOS_FAILURE)
                {
                    QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSHwWrMeterDelete () "
                                  " Returns FAILURE. \r\n", __FUNCTION__);
                    return (QOS_FAILURE);
                }
            }
        }                        /* End of if */

        u4MeterId = u4NextMeterId;

        i4RetStatus = QoSGetNextMeterTblEntryIndex (u4MeterId, &u4NextMeterId);
    }
    while (i4RetStatus == SNMP_SUCCESS);

    return (QOS_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSConfigPolicy                                      */
/* Description        : This function is used to Add or Delete the PolicyMap */
/*                      Table ACTIVE Entries in the HARDWARE                 */
/*                      QOS_CFG_TYPE_CREATE - Add the Entries in HARDWARE    */
/*                      QOS_CFG_TYPE_DELETE - Del the Entries from HARDWARE  */
/* Input(s)           : u4CfgType - QOS_CFG_TYPE_CREATE / QOS_CFG_TYPE_DELETE*/
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : gQoSGlobalInfo                                       */
/* Global Variables                                                          */
/* Modified           : gQoSGlobalInfo                                       */
/* Return Value(s)    : QOS_SUCCESS or QOS_FAILURE                           */
/* Called By          : QoSShutdown                                          */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QoSConfigPolicy (UINT4 u4CfgType)
{
    tQoSPolicyMapNode  *pPlyMapNode = NULL;
    UINT4               u4PlyId = 0;
    UINT4               u4NextPlyId = 0;
    INT4                i4RetStatus = QOS_FAILURE;

    i4RetStatus = QoSGetNextPlyMapTblEntryIndex (u4PlyId, &u4NextPlyId);
    if (i4RetStatus == QOS_FAILURE)
    {
        /* The Table is Empty */
        return (QOS_SUCCESS);
    }

    /* Table is Populated */
    do
    {
        pPlyMapNode = QoSUtlGetPolicyMapNode (u4NextPlyId);
        if (pPlyMapNode == NULL)
        {
            QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetPolicyMapNode () "
                          " Returns NULL. \r\n", __FUNCTION__);
            return (QOS_FAILURE);
        }

        /* Only Active Node in the Policy Map Table need to Configure */
        if (pPlyMapNode->u1Status == ACTIVE)
        {
            if (u4CfgType == QOS_CFG_TYPE_CREATE)
            {
                /* 1. Add the entry into the HARDWARE. */
                i4RetStatus = QoSHwWrMapClassToPolicy (pPlyMapNode, NULL,
                                                       QOS_PLY_ADD);
                if (i4RetStatus == QOS_FAILURE)
                {
                    QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSHwWrMapClassToPolicy "
                                  "() Returns FAILURE. \r\n", __FUNCTION__);
                    return (QOS_FAILURE);
                }
            }
            else
            {
                /* 1. Remove the entry from the HARDWARE. */
                i4RetStatus = QoSHwWrUnmapClassFromPolicy (pPlyMapNode, NULL,
                                                           QOS_PLY_DEL);
                if (i4RetStatus == QOS_FAILURE)
                {
                    QOS_TRC_ARG1 (MGMT_TRC, "In %s : "
                                  "QoSHwWrUnmapClassFromPolicy () Returns"
                                  " FAILURE. \r\n", __FUNCTION__);
                    return (QOS_FAILURE);
                }

            }
        }                        /* End of if */

        u4PlyId = u4NextPlyId;

        i4RetStatus = QoSGetNextPlyMapTblEntryIndex (u4PlyId, &u4NextPlyId);
    }
    while (i4RetStatus == SNMP_SUCCESS);

    return (QOS_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSConfigClsMap                                      */
/* Description        : This function is used to Add or Delete the ClassMap  */
/*                      Table ACTIVE Entries in the HARDWARE                 */
/*                      QOS_CFG_TYPE_CREATE - Add the Entries in HARDWARE    */
/*                      QOS_CFG_TYPE_DELETE - Del the Entries from HARDWARE  */
/* Input(s)           : u4CfgType - QOS_CFG_TYPE_CREATE / QOS_CFG_TYPE_DELETE*/
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : gQoSGlobalInfo                                       */
/* Global Variables                                                          */
/* Modified           : gQoSGlobalInfo                                       */
/* Return Value(s)    : QOS_SUCCESS or QOS_FAILURE                           */
/* Called By          : QoSShutdown                                          */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QoSConfigClsMap (UINT4 u4CfgType)
{
    tQoSClassMapNode   *pClsMapNode = NULL;
    INT4                i4RetStatus = QOS_FAILURE;
    UINT4               u4NextClsMapId = 0;
    UINT4               u4ClsMapId = 0;

    i4RetStatus = QoSGetNextClsMapTblEntryIndex (u4ClsMapId, &u4NextClsMapId);
    if (i4RetStatus == QOS_FAILURE)
    {
        /* The Table is Empty */
        return (QOS_SUCCESS);
    }

    /* Table is Populated */
    do
    {
        pClsMapNode = QoSUtlGetClassMapNode (u4NextClsMapId);
        if (pClsMapNode == NULL)
        {
            QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetClassMapNode () "
                          " Returns NULL. \r\n", __FUNCTION__);
            return (QOS_FAILURE);
        }

        /* Only Active Node in the ClsMap Table need to Delete */
        if (pClsMapNode->u1Status == ACTIVE)
        {
            if (u4CfgType == QOS_CFG_TYPE_DELETE)
            {
                /* 1. Remove the entry from the HARDWARE. */
                i4RetStatus = QoSHwWrDeleteClassMapEntry (pClsMapNode);
                if (i4RetStatus == QOS_FAILURE)
                {
                    QOS_TRC_ARG1 (MGMT_TRC, "In %s : "
                                  "QoSHwWrDeleteClassMapEntry () Returns "
                                  "FAILURE. \r\n", __FUNCTION__);
                    return (QOS_FAILURE);
                }
            }
        }                        /* End of if */

        u4ClsMapId = u4NextClsMapId;

        i4RetStatus = QoSGetNextClsMapTblEntryIndex (u4ClsMapId,
                                                     &u4NextClsMapId);
    }
    while (i4RetStatus == SNMP_SUCCESS);

    return (QOS_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSConfigSched                                       */
/* Description        : This function is used to Add or Delete the Scheduler */
/*                      Table ACTIVE Entries in the HARDWARE                 */
/*                      QOS_CFG_TYPE_CREATE - Add the Entries in HARDWARE    */
/*                      QOS_CFG_TYPE_DELETE - Del the Entries from HARDWARE  */
/* Input(s)           : u4CfgType - QOS_CFG_TYPE_CREATE / QOS_CFG_TYPE_DELETE*/
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : gQoSGlobalInfo                                       */
/* Global Variables                                                          */
/* Modified           : gQoSGlobalInfo                                       */
/* Return Value(s)    : QOS_SUCCESS or QOS_FAILURE                           */
/* Called By          : QoSShutdown                                          */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QoSConfigSched (UINT4 u4CfgType)
{
    tQoSSchedNode      *pSchedNode = NULL;
    INT4                i4RetStatus = QOS_FAILURE;
    INT4                i4IfIndex = 0;
    INT4                i4NextIfIndex = 0;
    UINT4               u4Sched = 0;
    UINT4               u4NextSched = 0;

    i4RetStatus = QoSGetNextSchedTblEntryIndex (i4IfIndex, &i4NextIfIndex,
                                                u4Sched, &u4NextSched);
    if (i4RetStatus == QOS_FAILURE)
    {
        /* The Table is Empty */
        return (QOS_SUCCESS);
    }

    /* Table is Populated */
    do
    {
        pSchedNode = QoSUtlGetSchedNode (i4NextIfIndex, u4NextSched);
        if (pSchedNode == NULL)
        {
            QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetSchedNode () "
                          " Returns NULL. \r\n", __FUNCTION__);
            return (QOS_FAILURE);
        }

        /* Only Active Node in the Scheduler Table need to Add */
        if (pSchedNode->u1Status == ACTIVE)
        {
            if (u4CfgType == QOS_CFG_TYPE_CREATE)
            {
                i4RetStatus = QoSHwWrSchedulerAdd (pSchedNode->i4IfIndex,
                                                   pSchedNode->u4Id);
                if (i4RetStatus == QOS_FAILURE)
                {
                    QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSHwWrSchedulerAdd ()"
                                  " Returns FAILURE.\r\n", __FUNCTION__);
                    return (QOS_FAILURE);
                }
            }
            else
            {
                i4RetStatus = QoSHwWrSchedulerDelete (pSchedNode->i4IfIndex,
                                                      pSchedNode->u4Id);
                if (i4RetStatus == QOS_FAILURE)
                {
                    QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSHwWrSchedulerDelete"
                                  " () Returns FAILURE. \r\n", __FUNCTION__);
                    return (QOS_FAILURE);
                }
            }
        }                        /* End of if */

        i4IfIndex = i4NextIfIndex;
        u4Sched = u4NextSched;

        i4RetStatus = QoSGetNextSchedTblEntryIndex (i4IfIndex, &i4NextIfIndex,
                                                    u4Sched, &u4NextSched);
    }
    while (i4RetStatus == SNMP_SUCCESS);

    return (QOS_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSConfigPbitPreference                              */
/* Description        : This func is used to Set or Reset the Pbit PReference*/
/*                        over DSCP                                            */
/*                      QOS_CFG_TYPE_CREATE - Set the PBITPREF in H/W        */
/*                      QOS_CFG_TYPE_DELETE - Reset the PBITPREF in H/W to default  */
/* Input(s)           : u4CfgType - QOS_CFG_TYPE_CREATE / QOS_CFG_TYPE_DELETE*/
/* Output(s)          : None.                                                */
/* Return Value(s)    : QOS_SUCCESS or QOS_FAILURE                           */
/* Called By          : QoSShutdown                                          */
/* Calling Function   :                                                      */
/*****************************************************************************/

INT4
QoSConfigPbitPreference (UINT4 u4CfgType)
{
    INT4                i4IfIndex = 0;
    INT4                i4RetStatus = 0;
    UINT1              *pu1Pref = NULL;

    for (i4IfIndex = 1; i4IfIndex <= SYS_DEF_MAX_PHYSICAL_INTERFACES;
         i4IfIndex++)
    {
        if (u4CfgType == QOS_CFG_TYPE_CREATE)
        {
            pu1Pref = QoSGetPrefNode (i4IfIndex);
            if (pu1Pref == NULL)
            {
                continue;
            }
            /* Configure PbitPreference from CtrlPlane to H/w */
            i4RetStatus =
                QoSHwWrSetPbitPreferenceOverDscp (i4IfIndex, *pu1Pref);
            if (i4RetStatus == QOS_FAILURE)
            {
                QOS_TRC_ARG1 (MGMT_TRC,
                              "In %s : QoSHwWrSetPbitPreferenceOverDscp "
                              " () Returns FAILURE. \r\n", __FUNCTION__);
                return (QOS_FAILURE);
            }
        }
        else
        {
            /* PbitPreference is set to default-ENABLE in H/w */
            i4RetStatus =
                QoSHwWrSetPbitPreferenceOverDscp (i4IfIndex,
                                                  QOS_PBIT_PREF_ENABLE);
            if (i4RetStatus == QOS_FAILURE)
            {
                QOS_TRC_ARG1 (MGMT_TRC,
                              "In %s : QoSHwWrSetPbitPreferenceOverDscp "
                              " () Returns FAILURE. \r\n", __FUNCTION__);
                return (QOS_FAILURE);
            }
        }

    }
    return (QOS_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSConfigQueueStat                                   */
/* Description        : This function is used to Clear the queue statistics  */
/*                      QOS_CFG_TYPE_CREATE - Return SUCCESS                 */
/*                      QOS_CFG_TYPE_DELETE - Clear the queue statistics     */
/* Input(s)           : u4CfgType - QOS_CFG_TYPE_CREATE / QOS_CFG_TYPE_DELETE*/
/* Output(s)          : None.                                                */
/* Return Value(s)    : QOS_SUCCESS or QOS_FAILURE                           */
/* Called By          : QoSShutdown                                          */
/* Calling Function   :                                                      */
/*****************************************************************************/

INT4
QoSConfigQueueStat (UINT4 u4CfgType)
{

    INT4                i4IfIndex = 0;
    INT4                i4RetStatus = 0;
    UINT4               u4FsQoSCoSQId = 0;
    tSNMP_COUNTER64_TYPE u8EnQOct;

    UNUSED_PARAM (u4CfgType);

    MEMSET (&u8EnQOct, 0, sizeof (tSNMP_COUNTER64_TYPE));

    /* Queue Stats are cleared when QOS is enabled or disabled */
    i4RetStatus =
        QoSGetNextCoSQStatsTblEntryIndex (0, &i4IfIndex, 0, &u4FsQoSCoSQId);
    if (i4RetStatus == QOS_FAILURE)
    {
        /*Table is empty */
        return (QOS_SUCCESS);
    }

    do
    {
        /* Clears Queue statistics in H/w for i4IfIndex, 
         * Return value is not checked as it does not affect the functionality in case of failure */
        QoSHwWrGetCoSQStats (i4IfIndex, u4FsQoSCoSQId, QOS_COSQ_STATS_CLEAR,
                             &u8EnQOct);

        i4RetStatus = QoSGetNextCoSQStatsTblEntryIndex (i4IfIndex, &i4IfIndex,
                                                        u4FsQoSCoSQId,
                                                        &u4FsQoSCoSQId);
    }
    while (i4RetStatus == SNMP_SUCCESS);

    return (QOS_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSConfigPriMap                                       */
/* Description        : This function is used to Add or Delete the Priority Map */
/*                      Table ACTIVE Entries in the HARDWARE                 */
/*                      QOS_CFG_TYPE_CREATE - Add the Entries in HARDWARE    */
/*                      QOS_CFG_TYPE_DELETE - Del the Entries from HARDWARE  */
/* Input(s)           : u4CfgType - QOS_CFG_TYPE_CREATE / QOS_CFG_TYPE_DELETE*/
/* Output(s)          : None.                                                */
/* Return Value(s)    : QOS_SUCCESS or QOS_FAILURE                           */
/* Called By          : QoSShutdown                                          */
/* Calling Function   :                                                      */
/*****************************************************************************/

INT4
QoSConfigPriMap (UINT4 u4CfgType)
{
    tQoSInPriorityMapNode *pPriMapNode = NULL;
    UINT4               u4FsQoSPriorityMapID = 0;
    UINT4               u4NextFsQoSPriorityMapID = 0;
    INT4                i4RetStatus = QOS_FAILURE;

    i4RetStatus = nmhGetFirstIndexFsQoSPriorityMapTable (&u4FsQoSPriorityMapID);
    if (i4RetStatus == QOS_FAILURE)
    {
        /* The Table is Empty */
        return (QOS_SUCCESS);
    }
    u4NextFsQoSPriorityMapID = u4FsQoSPriorityMapID;
    /* Table is Populated */
    do
    {
        pPriMapNode = QoSUtlGetPriorityMapNode (u4NextFsQoSPriorityMapID);
        if (pPriMapNode == NULL)
        {
            QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetPriorityMapNode () "
                          " Returns NULL. \r\n", __FUNCTION__);
            return (QOS_FAILURE);
        }

        /* Only Active Node in the Priority Map Table need to Added/Deleted */
        if (pPriMapNode->u1Status == ACTIVE)
        {
            if (u4CfgType == QOS_CFG_TYPE_CREATE)
            {
                i4RetStatus = QoSHwWrMapClasstoPriMap (pPriMapNode, QOS_NP_ADD);
                if ((i4RetStatus == QOS_FAILURE)
                    || (i4RetStatus == QOS_NP_NOT_SUPPORTED))
                {
                    QOS_TRC_ARG1 (MGMT_TRC,
                                  "In %s : QoSHwWrMapClasstoPriMap () "
                                  "Returns FAILURE. \r\n", __FUNCTION__);
                    return (QOS_FAILURE);
                }
            }
            else
            {
                i4RetStatus = QoSHwWrMapClasstoPriMap (pPriMapNode, QOS_NP_DEL);
                if ((i4RetStatus == QOS_FAILURE)
                    || (i4RetStatus == QOS_NP_NOT_SUPPORTED))
                {
                    QOS_TRC_ARG1 (MGMT_TRC,
                                  "In %s : QoSHwWrMapClasstoPriMap () "
                                  "Returns FAILURE. \r\n", __FUNCTION__);
                    return (QOS_FAILURE);
                }

            }
        }                        /* End of if */

        i4RetStatus =
            nmhGetNextIndexFsQoSPriorityMapTable (u4NextFsQoSPriorityMapID,
                                                  &u4NextFsQoSPriorityMapID);
    }
    while (i4RetStatus == SNMP_SUCCESS);

    return (QOS_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSConfigQueue                                       */
/* Description        : This function is used to Add or Delete the Queue     */
/*                      Table ACTIVE Entries in the HARDWARE                 */
/*                      QOS_CFG_TYPE_CREATE - Add the Entries in HARDWARE    */
/*                      QOS_CFG_TYPE_DELETE - Del the Entries from HARDWARE  */
/* Input(s)           : u4CfgType - QOS_CFG_TYPE_CREATE / QOS_CFG_TYPE_DELETE*/
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : gQoSGlobalInfo                                       */
/* Global Variables                                                          */
/* Modified           : gQoSGlobalInfo                                       */
/* Return Value(s)    : QOS_SUCCESS or QOS_FAILURE                           */
/* Called By          : QoSShutdown                                          */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QoSConfigQueue (UINT4 u4CfgType)
{
    tQoSQNode          *pQNode = NULL;
    INT4                i4RetStatus = QOS_FAILURE;
    INT4                i4IfIndex = 0;
    INT4                i4NextIfIndex = 0;
    UINT4               u4QId = 0;
    UINT4               u4NextQId = 0;

    i4RetStatus = QoSGetNextQTblEntryIndex (i4IfIndex, &i4NextIfIndex,
                                            u4QId, &u4NextQId);
    if (i4RetStatus == QOS_FAILURE)
    {
        /* The Table is Empty */
        return (QOS_SUCCESS);
    }

    /* Table is Populated */
    do
    {
        pQNode = QoSUtlGetQNode (i4NextIfIndex, u4NextQId);
        if (pQNode == NULL)
        {
            QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetQNode () "
                          " Returns NULL. \r\n", __FUNCTION__);
            return (QOS_FAILURE);
        }

        /* Only Active Node in the Queue Table need to Add */
        if (pQNode->u1Status == ACTIVE)
        {
            if (u4CfgType == QOS_CFG_TYPE_CREATE)
            {
                i4RetStatus = QoSHwWrQueueCreate (pQNode->i4IfIndex,
                                                  pQNode->u4Id);
                if (i4RetStatus == QOS_FAILURE)
                {
                    QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSHwWrQueueCreate ()"
                                  " Returns FAILURE.\r\n", __FUNCTION__);
                    return (QOS_FAILURE);
                }
            }
            else
            {
                i4RetStatus = QoSHwWrQueueDelete (pQNode->i4IfIndex,
                                                  pQNode->u4Id);
                if (i4RetStatus == QOS_FAILURE)
                {
                    QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSHwWrQueueDelete "
                                  " () Returns FAILURE. \r\n", __FUNCTION__);
                    return (QOS_FAILURE);
                }
            }

        }                        /* End of if */

        i4IfIndex = i4NextIfIndex;
        u4QId = u4NextQId;

        i4RetStatus = QoSGetNextQTblEntryIndex (i4IfIndex, &i4NextIfIndex,
                                                u4QId, &u4NextQId);
    }
    while (i4RetStatus == SNMP_SUCCESS);

    return (QOS_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSConfigQMap                                        */
/* Description        : This function is used to Add or Delete the QMap      */
/*                      Table ACTIVE Entries in the HARDWARE                 */
/*                      QOS_CFG_TYPE_CREATE - Add the Entries in HARDWARE    */
/*                      QOS_CFG_TYPE_DELETE - Del the Entries from HARDWARE  */
/* Input(s)           : u4CfgType - QOS_CFG_TYPE_CREATE / QOS_CFG_TYPE_DELETE*/
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : gQoSGlobalInfo                                       */
/* Global Variables                                                          */
/* Modified           : gQoSGlobalInfo                                       */
/* Return Value(s)    : QOS_SUCCESS or QOS_FAILURE                           */
/* Called By          : QoSShutdown                                          */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QoSConfigQMap (UINT4 u4CfgType)
{
    tQoSQMapNode       *pQMapNode = NULL;
    INT4                i4RetStatus = QOS_FAILURE;
    INT4                i4IfIndex = 0;
    INT4                i4DefIfIndex = 0;
    INT4                i4NextIfIndex = 0;
    UINT4               u4CLASS = 0;
    UINT4               u4NextCLASS = 0;
    INT4                i4RegenPriType = 0;
    INT4                i4NextRGPType = 0;
    UINT4               u4RegenPri = 0;
    UINT4               u4NextRegenPri = 0;

    i4RetStatus = QoSGetNextQMapTblEntryIndex (i4IfIndex, &i4NextIfIndex,
                                               u4CLASS, &u4NextCLASS,
                                               i4RegenPriType, &i4NextRGPType,
                                               u4RegenPri, &u4NextRegenPri);
    if (i4RetStatus == QOS_FAILURE)
    {
        /* The Table is Empty */
        return (QOS_SUCCESS);
    }

    /* Table is Populated */
    do
    {
        pQMapNode = QoSUtlGetQMapNode (i4NextIfIndex, u4NextCLASS,
                                       i4NextRGPType, u4NextRegenPri);
        if (pQMapNode == NULL)
        {
            QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetQMapNode () "
                          " Returns NULL. \r\n", __FUNCTION__);
            return (QOS_FAILURE);
        }

        /* Only Active Node in the QMap Table need to Add */
        if (pQMapNode->u1Status == ACTIVE)
        {
            if (u4CfgType == QOS_CFG_TYPE_CREATE)
            {
                i4RetStatus = QoSHwWrMapClassToQueue (pQMapNode->i4IfIndex,
                                                      pQMapNode->u4CLASS,
                                                      (INT4) pQMapNode->
                                                      u1RegenPriType,
                                                      pQMapNode->u1RegenPri,
                                                      pQMapNode->u4QId,
                                                      QOS_QMAP_ADD);
                if (i4RetStatus == QOS_FAILURE)
                {
                    QOS_TRC_ARG1 (MGMT_TRC, "In %s : "
                                  "QoSHwWrMapClassToQueue() : Add "
                                  "Returns FAILURE. \r\n", __FUNCTION__);
                    return (QOS_FAILURE);
                }
            }
            else
            {
                /* Delete the currently active entries from the hardware */
                i4RetStatus = QoSHwWrMapClassToQueue (pQMapNode->i4IfIndex,
                                                      pQMapNode->u4CLASS,
                                                      (INT4) pQMapNode->
                                                      u1RegenPriType,
                                                      pQMapNode->u1RegenPri,
                                                      pQMapNode->u4QId,
                                                      QOS_QMAP_DEL);
                if (i4RetStatus == QOS_FAILURE)
                {
                    QOS_TRC_ARG1 (MGMT_TRC, "In %s : "
                                  "QoSHwWrMapClassToQueue() : Del "
                                  "Returns FAILURE. \r\n", __FUNCTION__);
                    return (QOS_FAILURE);
                }
            }
        }                        /* End of if */

        i4IfIndex = i4NextIfIndex;
        u4CLASS = u4NextCLASS;
        i4RegenPriType = i4NextRGPType;
        u4RegenPri = u4NextRegenPri;

        i4RetStatus = QoSGetNextQMapTblEntryIndex (i4IfIndex, &i4NextIfIndex,
                                                   u4CLASS, &u4NextCLASS,
                                                   i4RegenPriType,
                                                   &i4NextRGPType, u4RegenPri,
                                                   &u4NextRegenPri);
    }
    while (i4RetStatus == SNMP_SUCCESS);

    /* For delete request, add the default QMap entries in the hardware (ONLY)
     * for all the interfaces
     */
    if (u4CfgType == QOS_CFG_TYPE_DELETE)
    {
        QoSAddDefQMapTblEntries (i4DefIfIndex, QOS_QMAP_HW_ONLY);
    }

    return (QOS_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSConfigHierarchy                                   */
/* Description        : This function is used to Add or Delete the Hierarchy */
/*                      Table ACTIVE Entries in the HARDWARE                 */
/*                      QOS_CFG_TYPE_CREATE - Add the Entries in HARDWARE    */
/*                      QOS_CFG_TYPE_DELETE - Del the Entries from HARDWARE  */
/* Input(s)           : u4CfgType - QOS_CFG_TYPE_CREATE / QOS_CFG_TYPE_DELETE*/
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : gQoSGlobalInfo                                       */
/* Global Variables                                                          */
/* Modified           : gQoSGlobalInfo                                       */
/* Return Value(s)    : QOS_SUCCESS or QOS_FAILURE                           */
/* Called By          : QoSShutdown                                          */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QoSConfigHierarchy (UINT4 u4CfgType)
{
    tQoSHierarchyNode  *pHNode = NULL;
    tQoSSchedNode      *pSchedNextNode = NULL;
    INT4                i4RetStatus = QOS_FAILURE;
    INT4                i4IfIndex = 0;
    INT4                i4NextIfIndex = 0;
    UINT4               u4HL = 0;
    UINT4               u4NextHL = 0;
    UINT4               u4SchedId = 0;
    UINT4               u4NextSchedId = 0;

    i4RetStatus = QoSGetNextHierarchyTblEntryIndex (i4IfIndex, &i4NextIfIndex,
                                                    u4HL, &u4NextHL,
                                                    u4SchedId, &u4NextSchedId);
    if (i4RetStatus == QOS_FAILURE)
    {
        /* The Table is Empty */
        return (QOS_SUCCESS);
    }
    /* Table is Populated */
    do
    {
        pHNode = QoSUtlGetHierarchyNode (i4NextIfIndex, u4NextHL,
                                         u4NextSchedId);
        if (pHNode == NULL)
        {
            QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetHierarchyNode () "
                          " Returns NULL. \r\n", __FUNCTION__);
            return (QOS_FAILURE);
        }

        pSchedNextNode = QoSUtlGetSchedNode (i4NextIfIndex, u4NextSchedId);
        if (pSchedNextNode == NULL)
        {
            QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetSchedNode () "
                          "Returns FAILURE. \r\n", __FUNCTION__);
            return (QOS_FAILURE);
        }

        /* Only Active Node in the Hierarchy Table need to Add */
        if (pHNode->u1Status == ACTIVE)
        {
            /* 1. Add the entry into the HARDWARE. */
            if (u4CfgType == QOS_CFG_TYPE_CREATE)
            {
                i4RetStatus =
                    QoSHwWrSchedulerHierarchyMap (pHNode->i4IfIndex,
                                                  pHNode->u4SchedId,
                                                  pHNode->u2Weight,
                                                  pHNode->u1Priority,
                                                  pHNode->u4SchedNext,
                                                  pHNode->u4QNext,
                                                  pHNode->u1Level,
                                                  QOS_HIERARCHY_ADD);
                if (i4RetStatus == QOS_FAILURE)
                {
                    QOS_TRC_ARG1 (MGMT_TRC, "In %s : "
                                  "QoSHwWrSchedulerHierarchyMap (): Add "
                                  "Returns FAILURE. \r\n", __FUNCTION__);
                    return (QOS_FAILURE);
                }
                else
                {
                    /*Incrementing the children count on success */
                    if (pHNode->u4SchedNext != 0)
                        pSchedNextNode->u4SchedChildren++;
                }
            }
            else
            {
                /* 1. Remove the entry from the HARDWARE. */
                i4RetStatus =
                    QoSHwWrSchedulerHierarchyMap (pHNode->i4IfIndex,
                                                  pHNode->u4SchedId,
                                                  pHNode->u2Weight,
                                                  pHNode->u1Priority,
                                                  pHNode->u4SchedNext,
                                                  pHNode->u4QNext,
                                                  pHNode->u1Level,
                                                  QOS_HIERARCHY_DEL);
                if (i4RetStatus == QOS_FAILURE)
                {
                    QOS_TRC_ARG1 (MGMT_TRC, "In %s : "
                                  "QoSHwWrSchedulerHierarchyMap () DEL: "
                                  "Returns FAILURE. \r\n", __FUNCTION__);
                    return (QOS_FAILURE);
                }
                else
                {
                    /*Decrement the children count on successful deletion */
                    if (pHNode->u4SchedNext != 0)
                        pSchedNextNode->u4SchedChildren--;
                }
            }
        }                        /* End of if */

        i4IfIndex = i4NextIfIndex;
        u4HL = u4NextHL;
        u4SchedId = u4NextSchedId;

        i4RetStatus = QoSGetNextHierarchyTblEntryIndex (i4IfIndex,
                                                        &i4NextIfIndex,
                                                        u4HL, &u4NextHL,
                                                        u4SchedId,
                                                        &u4NextSchedId);
    }
    while (i4RetStatus == SNMP_SUCCESS);

    return (QOS_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSConfigPDUP                                        */
/* Description        : This function is used to Add or Delete the PDUP      */
/*                      Table ACTIVE Entries in the HARDWARE                 */
/*                      QOS_CFG_TYPE_CREATE - Add the Entries in HARDWARE    */
/*                      QOS_CFG_TYPE_DELETE - Del the Entries from HARDWARE  */
/* Input(s)           : u4CfgType - QOS_CFG_TYPE_CREATE / QOS_CFG_TYPE_DELETE*/
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : gQoSGlobalInfo                                       */
/* Global Variables                                                          */
/* Modified           : gQoSGlobalInfo                                       */
/* Return Value(s)    : QOS_SUCCESS or QOS_FAILURE                           */
/* Called By          : QoSShutdown                                          */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QoSConfigPDUP (UINT4 u4CfgType)
{
    UINT1              *pu1DUPNode = NULL;
    INT4                i4RetStatus = QOS_FAILURE;
    INT4                i4IfIndex = 0;
    INT4                i4NextIfIndex = 0;

    i4RetStatus = QoSGetNextDUPTblEntryIndex (i4IfIndex, &i4NextIfIndex);
    if (i4RetStatus == QOS_FAILURE)
    {
        /* The Table is Empty */
        return (QOS_SUCCESS);
    }

    /* Table is Populated */
    do
    {
        pu1DUPNode = QoSUtlGetDUPNode (i4NextIfIndex);
        if (pu1DUPNode == NULL)
        {
            QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetDUPNode () "
                          " Returns NULL. \r\n", __FUNCTION__);
            return (QOS_FAILURE);
        }

        /* 1. Add the entry into the HARDWARE. */
        if (u4CfgType == QOS_CFG_TYPE_CREATE)
        {
            i4RetStatus = QoSHwWrSetDefUserPriority (i4IfIndex, *pu1DUPNode);
            if (i4RetStatus == QOS_FAILURE)
            {
                QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSHwWrSetDefUserPriority ()"
                              "Returns FAILURE. \r\n", __FUNCTION__);
                return (QOS_FAILURE);
            }
        }

        i4IfIndex = i4NextIfIndex;

        i4RetStatus = QoSGetNextDUPTblEntryIndex (i4IfIndex, &i4NextIfIndex);
    }
    while (i4RetStatus == SNMP_SUCCESS);

    return (QOS_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSEnable                                            */
/* Description        : This function is used to Enable the QoS Module.It    */
/*                      will do the following Actions                        */
/*                       It will configure the hardware with Current Data    */
/*                         in the Software.                                  */
/*                      1. Init the Hardware                                 */
/*                      2. It Add the Class(Filters) into the hardware.      */
/*                      3. It Add the Meters into the hardware.              */
/*                      4. It Add the Policies into the hardware.            */
/* Input(s)           : None.                                                */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : gQoSGlobalInfo                                       */
/* Global Variables                                                          */
/* Modified           : gQoSGlobalInfo                                       */
/* Return Value(s)    : QOS_SUCCESS or QOS_FAILURE                           */
/* Called By          : Protocol Main and SNMP/CLI                           */
/* Calling Function   : SNMP, QoSMain                                        */
/*****************************************************************************/
INT4
QoSEnable ()
{
    INT4                i4RetStatus = QOS_FAILURE;
/*in active node Npsync messages should be blocked*/
#ifdef L2RED_WANTED
    QosRedHwAuditIncBlkCounter ();
#endif

    /* Configure the HARDWARE if Configuration present in the Software */
    i4RetStatus = QoSConfigMeter (QOS_CFG_TYPE_CREATE);
    if (i4RetStatus == QOS_FAILURE)
    {
        return (QOS_FAILURE);
    }

    i4RetStatus = QoSConfigPolicy (QOS_CFG_TYPE_CREATE);
    if (i4RetStatus == QOS_FAILURE)
    {
        return (QOS_FAILURE);

    }

    /* Add only if hierarchial scheduling is not supported */
    if (QOS_HL_DEF_SCHED_CFG_SUPPORTED != gu1HLSupportFlag)
    {
        /* Add Scheduler */
        i4RetStatus = QoSConfigSched (QOS_CFG_TYPE_CREATE);
        if (i4RetStatus == QOS_FAILURE)
        {
            return (QOS_FAILURE);
        }

        /* Add Queue     */
        i4RetStatus = QoSConfigQueue (QOS_CFG_TYPE_CREATE);
        if (i4RetStatus == QOS_FAILURE)
        {
            return (QOS_FAILURE);
        }
    }

    /* Add QueueMap   */
    i4RetStatus = QoSConfigQMap (QOS_CFG_TYPE_CREATE);
    if (i4RetStatus == QOS_FAILURE)
    {
        return (QOS_FAILURE);
    }

    /* Add DefaultUserPriority for a Port */
    i4RetStatus = QoSConfigPDUP (QOS_CFG_TYPE_CREATE);
    if (i4RetStatus == QOS_FAILURE)
    {
        return (QOS_FAILURE);
    }

    /* Add Priority Map */
    i4RetStatus = QoSConfigPriMap (QOS_CFG_TYPE_CREATE);
    if (i4RetStatus == QOS_FAILURE)
    {
        return (QOS_FAILURE);
    }
    /* Pbit-Preference */
    i4RetStatus = QoSConfigPbitPreference (QOS_CFG_TYPE_CREATE);
    if (i4RetStatus == QOS_FAILURE)
    {
        return (QOS_FAILURE);
    }
    /* Queue Stat */
    i4RetStatus = QoSConfigQueueStat (QOS_CFG_TYPE_CREATE);
    if (i4RetStatus == QOS_FAILURE)
    {
        return (QOS_FAILURE);
    }
#ifdef L2RED_WANTED
    QosRedHwAuditDecBlkCounter ();
#endif

    return (QOS_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSDisable                                           */
/* Description        : This function is used to Disable the QoS Module.It   */
/*                      will do the following Actions                        */
/*                      1. It Delete the Policies from the hardware.         */
/*                      2. It Delete the Meters from the hardware.           */
/*                      3. It Delete the Class(Filters) from the hardware.   */
/* Input(s)           : None.                                                */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : gQoSGlobalInfo                                       */
/* Global Variables                                                          */
/* Modified           : gQoSGlobalInfo                                       */
/* Return Value(s)    : QOS_SUCCESS or QOS_FAILURE                           */
/* Called By          : QoSShutdown                                          */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QoSDisable ()
{
    INT4                i4RetStatus = QOS_FAILURE;

/*in active node Npsync messages should be blocked*/
#ifdef L2RED_WANTED
    QosRedHwAuditIncBlkCounter ();
#endif

    /* Delete the Hardware Entries */
    /* 1. Configured Policy          (PolicyForClass) */
    i4RetStatus = QoSConfigPolicy (QOS_CFG_TYPE_DELETE);
    if (i4RetStatus == QOS_FAILURE)
    {
        return (QOS_FAILURE);
    }
    /* 2. Configure the  Meters          (Meter) */
    i4RetStatus = QoSConfigMeter (QOS_CFG_TYPE_DELETE);
    if (i4RetStatus == QOS_FAILURE)
    {
        return (QOS_FAILURE);
    }

    /* 3. Del QueueMap   */
    i4RetStatus = QoSConfigQMap (QOS_CFG_TYPE_DELETE);
    if (i4RetStatus == QOS_FAILURE)
    {
        return (QOS_FAILURE);
    }

    /* 4. Configure the ClassMapEntries  (Filters) */
    i4RetStatus = QoSConfigClsMap (QOS_CFG_TYPE_DELETE);
    if (i4RetStatus == QOS_FAILURE)
    {
        return (QOS_FAILURE);
    }

    /* Add only if hierarchial scheduling is not supported */
    if (QOS_HL_DEF_SCHED_CFG_SUPPORTED != gu1HLSupportFlag)
    {
        /* Del Queue     */
        i4RetStatus = QoSConfigQueue (QOS_CFG_TYPE_DELETE);
        if (i4RetStatus == QOS_FAILURE)
        {
            return (QOS_FAILURE);
        }

        /* Del Scheduler */
        i4RetStatus = QoSConfigSched (QOS_CFG_TYPE_DELETE);
        if (i4RetStatus == QOS_FAILURE)
        {
            return (QOS_FAILURE);
        }
    }
    /* Del Priority Map */
    i4RetStatus = QoSConfigPriMap (QOS_CFG_TYPE_DELETE);
    if (i4RetStatus == QOS_FAILURE)
    {
        return (QOS_FAILURE);
    }
    /* Pbit-Preference */
    i4RetStatus = QoSConfigPbitPreference (QOS_CFG_TYPE_DELETE);
    if (i4RetStatus == QOS_FAILURE)
    {
        return (QOS_FAILURE);
    }
    /* Queue Stat */
    i4RetStatus = QoSConfigQueueStat (QOS_CFG_TYPE_DELETE);
    if (i4RetStatus == QOS_FAILURE)
    {
        return (QOS_FAILURE);
    }
#ifdef L2RED_WANTED
    QosRedHwAuditDecBlkCounter ();
#endif

    return (QOS_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSDeleteMemPools                                    */
/* Description        : This function is used to delete Memory Pool for QoS  */
/*                      Tables Using Memory Management Module                */
/* Input(s)           : None.                                                */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : gQoSGlobalInfo                                       */
/* Global Variables                                                          */
/* Modified           : gQoSGlobalInfo                                       */
/* Return Value(s)    : None.                                                */
/* Called By          : QoSStart                                             */
/* Calling Function   :                                                      */
/*****************************************************************************/
VOID
QoSDeleteMemPools (VOID)
{
    QosxSizingMemDeleteMemPools ();
    /* Delete All Memory Pools */
    if (gQoSGlobalInfo.QoSInPriMapTblMemPoolId > 0)
    {
        MemDeleteMemPool (gQoSGlobalInfo.QoSInPriMapTblMemPoolId);
        gQoSGlobalInfo.QoSInPriMapTblMemPoolId = 0;
    }

    if (gQoSGlobalInfo.QoSClsMapTblMemPoolId > 0)
    {
        MemDeleteMemPool (gQoSGlobalInfo.QoSClsMapTblMemPoolId);
        gQoSGlobalInfo.QoSClsMapTblMemPoolId = 0;
    }

    if (gQoSGlobalInfo.QoSCls2PriMapTblMemPoolId > 0)
    {
        MemDeleteMemPool (gQoSGlobalInfo.QoSCls2PriMapTblMemPoolId);
        gQoSGlobalInfo.QoSCls2PriMapTblMemPoolId = 0;
    }

    if (gQoSGlobalInfo.QoSMeterTblMemPoolId > 0)
    {
        MemDeleteMemPool (gQoSGlobalInfo.QoSMeterTblMemPoolId);
        gQoSGlobalInfo.QoSMeterTblMemPoolId = 0;
    }

    if (gQoSGlobalInfo.DsMeterPoolId > 0)
    {
        MemDeleteMemPool (gQoSGlobalInfo.DsMeterPoolId);
        gQoSGlobalInfo.DsMeterPoolId = 0;
    }

    if (gQoSGlobalInfo.QoSPlyMapTblMemPoolId > 0)
    {
        MemDeleteMemPool (gQoSGlobalInfo.QoSPlyMapTblMemPoolId);
        gQoSGlobalInfo.QoSPlyMapTblMemPoolId = 0;
    }

    if (gQoSGlobalInfo.QoSQTempTblMemPoolId > 0)
    {
        MemDeleteMemPool (gQoSGlobalInfo.QoSQTempTblMemPoolId);
        gQoSGlobalInfo.QoSQTempTblMemPoolId = 0;
    }

    if (gQoSGlobalInfo.QoSRDTblMemPoolId > 0)
    {
        MemDeleteMemPool (gQoSGlobalInfo.QoSRDTblMemPoolId);
        gQoSGlobalInfo.QoSRDTblMemPoolId = 0;
    }

    if (gQoSGlobalInfo.QoSShapeTempTblMemPoolId > 0)
    {
        MemDeleteMemPool (gQoSGlobalInfo.QoSShapeTempTblMemPoolId);
        gQoSGlobalInfo.QoSShapeTempTblMemPoolId = 0;
    }

    if (gQoSGlobalInfo.QoSQMapTblMemPoolId > 0)
    {
        MemDeleteMemPool (gQoSGlobalInfo.QoSQMapTblMemPoolId);
        gQoSGlobalInfo.QoSQMapTblMemPoolId = 0;
    }

    if (gQoSGlobalInfo.QoSQTblMemPoolId > 0)
    {
        MemDeleteMemPool (gQoSGlobalInfo.QoSQTblMemPoolId);
        gQoSGlobalInfo.QoSQTblMemPoolId = 0;
    }

    if (gQoSGlobalInfo.DsQPoolId > 0)
    {
        MemDeleteMemPool (gQoSGlobalInfo.DsQPoolId);
        gQoSGlobalInfo.DsQPoolId = 0;
    }

    if (gQoSGlobalInfo.QoSSchedTblMemPoolId > 0)
    {
        MemDeleteMemPool (gQoSGlobalInfo.QoSSchedTblMemPoolId);
        gQoSGlobalInfo.QoSSchedTblMemPoolId = 0;
    }

    if (gQoSGlobalInfo.QoSHierarchyTblMemPoolId > 0)
    {
        MemDeleteMemPool (gQoSGlobalInfo.QoSHierarchyTblMemPoolId);
        gQoSGlobalInfo.QoSHierarchyTblMemPoolId = 0;
    }

    if (gQoSGlobalInfo.QoSClsInfoTblMemPoolId > 0)
    {
        MemDeleteMemPool (gQoSGlobalInfo.QoSClsInfoTblMemPoolId);
        gQoSGlobalInfo.QoSClsInfoTblMemPoolId = 0;
    }

    if (gQoSGlobalInfo.QoSFltInfoTblMemPoolId > 0)
    {
        MemDeleteMemPool (gQoSGlobalInfo.QoSFltInfoTblMemPoolId);
        gQoSGlobalInfo.QoSFltInfoTblMemPoolId = 0;
    }

    /* QOS STD DELETION */
    if (gQoSGlobalInfo.DsDataPathPoolId > 0)
    {
        MemDeleteMemPool (gQoSGlobalInfo.DsDataPathPoolId);
        gQoSGlobalInfo.DsDataPathPoolId = 0;
    }

    if (gQoSGlobalInfo.DsClfrPoolId > 0)
    {
        MemDeleteMemPool (gQoSGlobalInfo.DsClfrPoolId);
        gQoSGlobalInfo.DsClfrPoolId = 0;
    }
    if (gQoSGlobalInfo.DsClfrElementPoolId > 0)
    {
        MemDeleteMemPool (gQoSGlobalInfo.DsClfrElementPoolId);
        gQoSGlobalInfo.DsClfrElementPoolId = 0;
    }
    if (gQoSGlobalInfo.DsActionPoolId > 0)
    {
        MemDeleteMemPool (gQoSGlobalInfo.DsActionPoolId);
        gQoSGlobalInfo.DsActionPoolId = 0;
    }
    if (gQoSGlobalInfo.DsCountActPoolId > 0)
    {
        MemDeleteMemPool (gQoSGlobalInfo.DsCountActPoolId);
        gQoSGlobalInfo.DsCountActPoolId = 0;
    }
    if (gQoSGlobalInfo.DsQosSchedulerPoolId > 0)
    {
        MemDeleteMemPool (gQoSGlobalInfo.DsQosSchedulerPoolId);
        gQoSGlobalInfo.DsQosSchedulerPoolId = 0;
    }

    if (gQoSGlobalInfo.DsMinRatePoolId > 0)
    {
        MemDeleteMemPool (gQoSGlobalInfo.DsMinRatePoolId);
        gQoSGlobalInfo.DsMinRatePoolId = 0;
    }

    if (gQoSGlobalInfo.DsMaxRatePoolId > 0)
    {
        MemDeleteMemPool (gQoSGlobalInfo.DsMaxRatePoolId);
        gQoSGlobalInfo.DsMaxRatePoolId = 0;
    }

    if (gQoSGlobalInfo.QoSPonitedToQid > 0)
    {
        MemDeleteMemPool (gQoSGlobalInfo.QoSPonitedToQid);
        gQoSGlobalInfo.QoSPonitedToQid = 0;
    }

    if (gQoSGlobalInfo.QoSSchedulerMapPoolId > 0)
    {
        MemDeleteMemPool (gQoSGlobalInfo.QoSSchedulerMapPoolId);
        gQoSGlobalInfo.QoSSchedulerMapPoolId = 0;
    }

    if (gQoSGlobalInfo.QoSPointerQid > 0)
    {
        MemDeleteMemPool (gQoSGlobalInfo.QoSPointerQid);
        gQoSGlobalInfo.QoSPointerQid = 0;
    }

    if (gQoSGlobalInfo.QosCpuQMemPoolId > 0)
    {
        MemDeleteMemPool (gQoSGlobalInfo.QosCpuQMemPoolId);
        gQoSGlobalInfo.QosCpuQMemPoolId = 0;
    }
    if (gQoSGlobalInfo.QoSPortTblMemPoolId > 0)
    {
        MemDeleteMemPool (gQoSGlobalInfo.QoSPortTblMemPoolId);
        gQoSGlobalInfo.QoSPortTblMemPoolId = 0;
    }

    /* deleting QoS VLAN Queueing mempool */
    if (gQoSGlobalInfo.QoSVlanQueueingMapTblMemPoolId > 0)
    {
        MemDeleteMemPool (gQoSGlobalInfo.QoSVlanQueueingMapTblMemPoolId);
        gQoSGlobalInfo.QoSVlanQueueingMapTblMemPoolId = 0;
    }
}

/*****************************************************************************/
/* Function Name      : QoSCreateMemPools                                    */
/* Description        : This function is used to Create Memory Pool for QoS  */
/*                      Tables Using Memory Management Module                */
/* Input(s)           : None.                                                */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : gQoSGlobalInfo                                       */
/* Global Variables                                                          */
/* Modified           : gQoSGlobalInfo                                       */
/* Return Value(s)    : QOS_SUCCESS or QOS_FAILURE                           */
/* Called By          : QoSStart                                             */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QoSCreateMemPools (VOID)
{
    UINT4               u4RetStatus = MEM_FAILURE;

    if (QosxSizingMemCreateMemPools () == OSIX_FAILURE)
    {
        QOS_TRC (OS_RESOURCE_TRC, "Pool Creation Error!\n");
        return (QOS_FAILURE);
    }
    gQoSGlobalInfo.QosStdClfrPortPoolId =
        QOSXMemPoolIds[MAX_QOS_STD_CLFR_PORT_ENTRIES_SIZING_ID];

    /* Create Memory for QoS Vlan Queueing */
    gQoSGlobalInfo.QoSVlanQueueingMapTblMemPoolId =
        QOSXMemPoolIds[MAX_QOS_VLAN_MAP_ENTRIES_SIZING_ID];

    /* Create Memory Pools for QoS Tables */
    /* 1. Incoming Priority Map Table  */
    u4RetStatus = MemCreateMemPool (sizeof (tQoSInPriorityMapNode),
                                    (QOS_PRI_MAP_TBL_MAX_ENTRIES /
                                     QOS_MEM_POOL_SIZE_FACTOR),
                                    MEM_HEAP_MEMORY_TYPE,
                                    &(gQoSGlobalInfo.QoSInPriMapTblMemPoolId));
    if (u4RetStatus == MEM_FAILURE)
    {
        QOS_TRC (OS_RESOURCE_TRC, "Pool Creation Error!\n");

        QoSDeleteMemPools ();

        return (QOS_FAILURE);
    }

    /* 2. Class Map Table  */
    u4RetStatus = MemCreateMemPool (sizeof (tQoSClassMapNode),
                                    (QOS_CLS_MAP_TBL_MAX_ENTRIES /
                                     QOS_MEM_POOL_SIZE_FACTOR),
                                    MEM_HEAP_MEMORY_TYPE,
                                    &(gQoSGlobalInfo.QoSClsMapTblMemPoolId));
    if (u4RetStatus == MEM_FAILURE)
    {
        QOS_TRC (OS_RESOURCE_TRC, "Pool Creation Error!\n");

        QoSDeleteMemPools ();

        return (QOS_FAILURE);
    }

    /* 3. Class to Priority Map Table  */
    u4RetStatus = MemCreateMemPool (sizeof (tQoSClass2PriorityNode),
                                    (QOS_CLS2PRI_MAP_TBL_MAX_ENTRIES /
                                     QOS_MEM_POOL_SIZE_FACTOR),
                                    MEM_HEAP_MEMORY_TYPE,
                                    &(gQoSGlobalInfo.
                                      QoSCls2PriMapTblMemPoolId));
    if (u4RetStatus == MEM_FAILURE)
    {
        QOS_TRC (OS_RESOURCE_TRC, "Pool Creation Error!\n");

        QoSDeleteMemPools ();

        return (QOS_FAILURE);
    }

    /* 4. Meter Table  */
    u4RetStatus = MemCreateMemPool (sizeof (tQoSMeterNode),
                                    (QOS_METER_TBL_MAX_ENTRIES /
                                     QOS_MEM_POOL_SIZE_FACTOR),
                                    MEM_HEAP_MEMORY_TYPE,
                                    &(gQoSGlobalInfo.QoSMeterTblMemPoolId));
    if (u4RetStatus == MEM_FAILURE)
    {
        QOS_TRC (OS_RESOURCE_TRC, "Pool Creation Error!\n");

        QoSDeleteMemPools ();

        return (QOS_FAILURE);
    }

    /*Std Meter */
    u4RetStatus = MemCreateMemPool (sizeof (tQosStdMeterEntry),
                                    (QOS_METER_TBL_MAX_ENTRIES /
                                     QOS_MEM_POOL_SIZE_FACTOR),
                                    MEM_HEAP_MEMORY_TYPE,
                                    &(gQoSGlobalInfo.DsMeterPoolId));
    if (u4RetStatus == MEM_FAILURE)
    {
        QOS_TRC (OS_RESOURCE_TRC, "Pool Creation Error!\n");

        QoSDeleteMemPools ();

        return (QOS_FAILURE);
    }

    /* 5. Policy Map Table */
    u4RetStatus = MemCreateMemPool (sizeof (tQoSPolicyMapNode),
                                    (QOS_PLY_MAP_TBL_MAX_ENTRIES /
                                     QOS_MEM_POOL_SIZE_FACTOR),
                                    MEM_HEAP_MEMORY_TYPE,
                                    &(gQoSGlobalInfo.QoSPlyMapTblMemPoolId));
    if (u4RetStatus == MEM_FAILURE)
    {
        QOS_TRC (OS_RESOURCE_TRC, "Pool Creation Error!\n");

        QoSDeleteMemPools ();

        return (QOS_FAILURE);
    }

    /* 6. Q Template Table */
    u4RetStatus = MemCreateMemPool (sizeof (tQoSQTemplateNode),
                                    (QOS_Q_TEMP_TBL_MAX_ENTRIES /
                                     QOS_MEM_POOL_SIZE_FACTOR),
                                    MEM_HEAP_MEMORY_TYPE,
                                    &(gQoSGlobalInfo.QoSQTempTblMemPoolId));
    if (u4RetStatus == MEM_FAILURE)
    {
        QOS_TRC (OS_RESOURCE_TRC, "Pool Creation Error!\n");

        QoSDeleteMemPools ();

        return (QOS_FAILURE);
    }

    /* 7. Random Detect Table */
    u4RetStatus = MemCreateMemPool (sizeof (tQoSRDCfgNode),
                                    (QOS_RD_TBL_MAX_ENTRIES /
                                     QOS_MEM_POOL_SIZE_FACTOR),
                                    MEM_HEAP_MEMORY_TYPE,
                                    &(gQoSGlobalInfo.QoSRDTblMemPoolId));
    if (u4RetStatus == MEM_FAILURE)
    {
        QOS_TRC (OS_RESOURCE_TRC, "Pool Creation Error!\n");

        QoSDeleteMemPools ();

        return (QOS_FAILURE);
    }

    /* 8. Shape Template Table */
    u4RetStatus = MemCreateMemPool (sizeof (tQoSShapeTemplateNode),
                                    (QOS_SHAPE_TEMP_TBL_MAX_ENTRIES /
                                     QOS_MEM_POOL_SIZE_FACTOR),
                                    MEM_HEAP_MEMORY_TYPE,
                                    &(gQoSGlobalInfo.QoSShapeTempTblMemPoolId));

    if (u4RetStatus == MEM_FAILURE)
    {
        QOS_TRC (OS_RESOURCE_TRC, "Pool Creation Error!\n");

        QoSDeleteMemPools ();

        return (QOS_FAILURE);
    }

    /* 9. Q Map Table */
    u4RetStatus = MemCreateMemPool (sizeof (tQoSQMapNode),
                                    (QOS_Q_MAP_TBL_MAX_ENTRIES /
                                     QOS_MEM_POOL_SIZE_FACTOR),
                                    MEM_HEAP_MEMORY_TYPE,
                                    &(gQoSGlobalInfo.QoSQMapTblMemPoolId));
    if (u4RetStatus == MEM_FAILURE)
    {
        QOS_TRC (OS_RESOURCE_TRC, "Pool Creation Error!\n");

        QoSDeleteMemPools ();

        return (QOS_FAILURE);
    }

    /* 10. Q Table */
    u4RetStatus = MemCreateMemPool (sizeof (tQoSQNode),
                                    (QOS_Q_TBL_MAX_ENTRIES /
                                     QOS_MEM_POOL_SIZE_FACTOR),
                                    MEM_HEAP_MEMORY_TYPE,
                                    &(gQoSGlobalInfo.QoSQTblMemPoolId));
    if (u4RetStatus == MEM_FAILURE)
    {
        QOS_TRC (OS_RESOURCE_TRC, "Pool Creation Error!\n");

        QoSDeleteMemPools ();

        return (QOS_FAILURE);
    }

    /* 10.1 Std Q Table */
    u4RetStatus = MemCreateMemPool (sizeof (tQosStdQEntry),
                                    (QOS_Q_TBL_MAX_ENTRIES /
                                     QOS_MEM_POOL_SIZE_FACTOR),
                                    MEM_HEAP_MEMORY_TYPE,
                                    &(gQoSGlobalInfo.DsQPoolId));
    if (u4RetStatus == MEM_FAILURE)
    {
        QOS_TRC (OS_RESOURCE_TRC, "Pool Creation Error!\n");

        QoSDeleteMemPools ();

        return (QOS_FAILURE);
    }

    /* 11. Scheduler Table */
    u4RetStatus = MemCreateMemPool (sizeof (tQoSSchedNode),
                                    (QOS_SCHED_TBL_MAX_ENTRIES /
                                     QOS_MEM_POOL_SIZE_FACTOR),
                                    MEM_HEAP_MEMORY_TYPE,
                                    &(gQoSGlobalInfo.QoSSchedTblMemPoolId));
    if (u4RetStatus == MEM_FAILURE)
    {
        QOS_TRC (OS_RESOURCE_TRC, "Pool Creation Error!\n");

        QoSDeleteMemPools ();

        return (QOS_FAILURE);
    }

    /* 12. Hierarchy Table */
    u4RetStatus = MemCreateMemPool (sizeof (tQoSHierarchyNode),
                                    (QOS_HIERARCHY_TBL_MAX_ENTRIES /
                                     QOS_MEM_POOL_SIZE_FACTOR),
                                    MEM_HEAP_MEMORY_TYPE,
                                    &(gQoSGlobalInfo.QoSHierarchyTblMemPoolId));
    if (u4RetStatus == MEM_FAILURE)
    {
        QOS_TRC (OS_RESOURCE_TRC, "Pool Creation Error!\n");

        QoSDeleteMemPools ();

        return (QOS_FAILURE);
    }

    /* 13. Class Info Table */
    u4RetStatus = MemCreateMemPool (sizeof (tQoSClassInfoNode),
                                    (QOS_MAX_NUM_OF_CLASSES /
                                     QOS_MEM_POOL_SIZE_FACTOR),
                                    MEM_HEAP_MEMORY_TYPE,
                                    &(gQoSGlobalInfo.QoSClsInfoTblMemPoolId));
    if (u4RetStatus == MEM_FAILURE)
    {
        QOS_TRC (OS_RESOURCE_TRC, "Pool Creation Error!\n");

        QoSDeleteMemPools ();

        return (QOS_FAILURE);
    }

    /* 14. Filter Info Node Table */
    u4RetStatus = MemCreateMemPool (sizeof (tQoSFilterInfoNode),
                                    (QOS_FILTER_INFO_MAX_ENTRIES /
                                     QOS_MEM_POOL_SIZE_FACTOR),
                                    MEM_HEAP_MEMORY_TYPE,
                                    &(gQoSGlobalInfo.QoSFltInfoTblMemPoolId));
    if (u4RetStatus == MEM_FAILURE)
    {
        QOS_TRC (OS_RESOURCE_TRC, "Pool Creation Error!\n");

        QoSDeleteMemPools ();

        return (QOS_FAILURE);
    }

    /* 16. MemPool for Qos Std Data Path start */
    u4RetStatus = MemCreateMemPool (sizeof (tQosStdDataPathEntry),
                                    (QOS_DATA_PATH_SIZE),
                                    MEM_HEAP_MEMORY_TYPE,
                                    &(gQoSGlobalInfo.DsDataPathPoolId));
    if (u4RetStatus == MEM_FAILURE)
    {
        QOS_TRC (OS_RESOURCE_TRC, "Pool Creation Error!\n");

        QoSDeleteMemPools ();

        return (QOS_FAILURE);
    }

    /* 17. Mempool for Clfr Table */
    u4RetStatus = MemCreateMemPool (sizeof (tQosStdClfrEntry),
                                    (QOS_CLFR_SIZE),
                                    MEM_HEAP_MEMORY_TYPE,
                                    &(gQoSGlobalInfo.DsClfrPoolId));
    if (u4RetStatus == MEM_FAILURE)
    {
        QOS_TRC (OS_RESOURCE_TRC, "Pool Creation Error!\n");

        QoSDeleteMemPools ();

        return (QOS_FAILURE);
    }
    /* 18. Mempool for Clfr Element Table */
    u4RetStatus = MemCreateMemPool (sizeof (tQosStdClfrElementEntry),
                                    (QOS_CLFR_ELEMENT_SIZE),
                                    MEM_HEAP_MEMORY_TYPE,
                                    &(gQoSGlobalInfo.DsClfrElementPoolId));
    if (u4RetStatus == MEM_FAILURE)
    {
        QOS_TRC (OS_RESOURCE_TRC, "Pool Creation Error!\n");

        QoSDeleteMemPools ();

        return (QOS_FAILURE);
    }
    /* Mempool for Action Table */
    u4RetStatus = MemCreateMemPool (sizeof (tQosStdActionEntry),
                                    (QOS_ACTION_TBL_MAX_ENTRIES),
                                    MEM_HEAP_MEMORY_TYPE,
                                    &(gQoSGlobalInfo.DsActionPoolId));
    if (u4RetStatus == MEM_FAILURE)
    {
        QOS_TRC (OS_RESOURCE_TRC, "Pool Creation Error!\n");

        QoSDeleteMemPools ();

        return (QOS_FAILURE);
    }
    /* Mempool for CountAct Table */
    u4RetStatus = MemCreateMemPool (sizeof (tQosStdCountActEntry),
                                    (QOS_COUNTACT_TBL_MAX_ENTRIES),
                                    MEM_HEAP_MEMORY_TYPE,
                                    &(gQoSGlobalInfo.DsCountActPoolId));
    if (u4RetStatus == MEM_FAILURE)
    {
        QOS_TRC (OS_RESOURCE_TRC, "Pool Creation Error!\n");

        QoSDeleteMemPools ();

        return (QOS_FAILURE);
    }

    /* Scheduler Table */
    u4RetStatus = MemCreateMemPool (sizeof (tQosStdSchedulerEntry),
                                    (QOS_Q_TBL_MAX_ENTRIES /
                                     QOS_MEM_POOL_SIZE_FACTOR),
                                    MEM_HEAP_MEMORY_TYPE,
                                    &(gQoSGlobalInfo.DsQosSchedulerPoolId));
    if (u4RetStatus == MEM_FAILURE)
    {
        QOS_TRC (OS_RESOURCE_TRC, "Pool Creation Error!\n");

        QoSDeleteMemPools ();

        return (QOS_FAILURE);
    }

    /* 19. MinRate Table */
    u4RetStatus = MemCreateMemPool (sizeof (tQosStdMinRateEntry),
                                    (QOS_Q_TBL_MAX_ENTRIES /
                                     QOS_MEM_POOL_SIZE_FACTOR),
                                    MEM_HEAP_MEMORY_TYPE,
                                    &(gQoSGlobalInfo.DsMinRatePoolId));
    if (u4RetStatus == MEM_FAILURE)
    {
        QOS_TRC (OS_RESOURCE_TRC, "Pool Creation Error!\n");

        QoSDeleteMemPools ();

        return (QOS_FAILURE);
    }

    /* 20. MaxRate Table */
    u4RetStatus = MemCreateMemPool (sizeof (tQosStdMaxRateEntry),
                                    (QOS_Q_TBL_MAX_ENTRIES /
                                     QOS_MEM_POOL_SIZE_FACTOR),
                                    MEM_HEAP_MEMORY_TYPE,
                                    &(gQoSGlobalInfo.DsMaxRatePoolId));
    if (u4RetStatus == MEM_FAILURE)
    {
        QOS_TRC (OS_RESOURCE_TRC, "Pool Creation Error!\n");

        QoSDeleteMemPools ();

        return (QOS_FAILURE);
    }

    u4RetStatus = MemCreateMemPool (sizeof (tQosPointNode),
                                    (QOS_MAX_POINTED_TO_ENTRIES /
                                     QOS_MEM_POOL_SIZE_FACTOR),
                                    MEM_HEAP_MEMORY_TYPE,
                                    &(gQoSGlobalInfo.QoSPonitedToQid));
    if (u4RetStatus == MEM_FAILURE)
    {
        QOS_TRC (OS_RESOURCE_TRC, "Pool Creation Error!\n");

        QoSDeleteMemPools ();

        return (QOS_FAILURE);
    }

    u4RetStatus = MemCreateMemPool (sizeof (tQosPointNode),
                                    (QOS_SCHED_TBL_MAX_ENTRIES /
                                     QOS_MEM_POOL_SIZE_FACTOR),
                                    MEM_HEAP_MEMORY_TYPE,
                                    &(gQoSGlobalInfo.QoSSchedulerMapPoolId));
    if (u4RetStatus == MEM_FAILURE)
    {
        QOS_TRC (OS_RESOURCE_TRC, "Pool Creation Error!\n");

        QoSDeleteMemPools ();

        return (QOS_FAILURE);
    }

    u4RetStatus = MemCreateMemPool (sizeof (tQosPointerListNode),
                                    (QOS_MAX_POINTED_TO_ENTRIES /
                                     QOS_MEM_POOL_SIZE_FACTOR),
                                    MEM_HEAP_MEMORY_TYPE,
                                    &(gQoSGlobalInfo.QoSPointerQid));
    if (u4RetStatus == MEM_FAILURE)
    {
        QOS_TRC (OS_RESOURCE_TRC, "Pool Creation Error!\n");

        QoSDeleteMemPools ();

        return (QOS_FAILURE);
    }

    /* CPU Rate Limit  Table */
    u4RetStatus = MemCreateMemPool (sizeof (tQosCpuQEntry),
                                    (QOS_MAX_CPU_Q_ENTRIES /
                                     QOS_MEM_POOL_SIZE_FACTOR),
                                    MEM_HEAP_MEMORY_TYPE,
                                    &(gQoSGlobalInfo.QosCpuQMemPoolId));
    if (u4RetStatus == MEM_FAILURE)
    {
        QOS_TRC (OS_RESOURCE_TRC, "Pool Creation Error!\n");

        QoSDeleteMemPools ();

        return (QOS_FAILURE);
    }
    /* QOS PORT  Table */
    u4RetStatus = MemCreateMemPool (sizeof (tQoSPortTblEntry),
                                    (QOS_MAX_PORT_TBL_ENTRIES /
                                     QOS_MEM_POOL_SIZE_FACTOR),
                                    MEM_HEAP_MEMORY_TYPE,
                                    &(gQoSGlobalInfo.QoSPortTblMemPoolId));
    if (u4RetStatus == MEM_FAILURE)
    {
        QOS_TRC (OS_RESOURCE_TRC, "Pool Creation Error!\n");

        QoSDeleteMemPools ();

        return (QOS_FAILURE);
    }

    return (QOS_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSDeleteTables                                      */
/* Description        : This function is used to delete the QoS Table and    */
/*                       it Entries if it Exists                             */
/* Input(s)           : None.                                                */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : gQoSGlobalInfo                                       */
/* Global Variables                                                          */
/* Modified           : gQoSGlobalInfo                                       */
/* Return Value(s)    : QOS_SUCCESS or QOS_FAILURE                           */
/* Called By          : QoSShutdown                                          */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QoSDeleteTables (VOID)
{
    tTMO_SLL_NODE      *pCurrNode = NULL;
    tTMO_SLL_NODE      *pNextNode = NULL;
    INT4                i4RetStatus = QOS_FAILURE;

    /*  RBTree added in case of DPPortList present in tQosStdClfrEntry is to
     *  be tuned dynamically. */
    QOS_DELETE_STD_CLFR_PORT_RBTREE ();

    /* Delete the Table (Sll,Hash,RBTree) */
    if (gQoSGlobalInfo.pRbInPriMapUniTbl != NULL)
    {
        RBTreeDestroy (gQoSGlobalInfo.pRbInPriMapUniTbl, NULL, 0);
        gQoSGlobalInfo.pRbInPriMapUniTbl = NULL;
    }

    if (gQoSGlobalInfo.pRbInPriMapTbl != NULL)
    {
        RBTreeDestroy (gQoSGlobalInfo.pRbInPriMapTbl,
                       QoSInPriTblRBTFreeNodeFn, 0);
        gQoSGlobalInfo.pRbInPriMapTbl = NULL;
    }

    /* Delete Vlan queueing table */
    if (gQoSGlobalInfo.pRbVlanQueueingMapTbl != NULL)
    {
        RBTreeDestroy (gQoSGlobalInfo.pRbVlanQueueingMapTbl, NULL, 0);
        gQoSGlobalInfo.pRbVlanQueueingMapTbl = NULL;
    }

    if (gQoSGlobalInfo.pRbClsMapTbl != NULL)
    {
        RBTreeDestroy (gQoSGlobalInfo.pRbClsMapTbl,
                       QoSClsMapTblRBTFreeNodeFn, 0);
        gQoSGlobalInfo.pRbClsMapTbl = NULL;
    }

    if (gQoSGlobalInfo.pRbCls2PriMapTbl != NULL)
    {
        RBTreeDestroy (gQoSGlobalInfo.pRbCls2PriMapTbl,
                       QoSCls2PriTblRBTFreeNodeFn, 0);
        gQoSGlobalInfo.pRbCls2PriMapTbl = NULL;
    }

    if (gQoSGlobalInfo.pRbMeterTbl != NULL)
    {
        RBTreeDestroy (gQoSGlobalInfo.pRbMeterTbl, QoSMeterTblRBTFreeNodeFn, 0);
        gQoSGlobalInfo.pRbMeterTbl = NULL;
    }

    if (gQoSGlobalInfo.pRbPlyMapTbl != NULL)
    {
        RBTreeDestroy (gQoSGlobalInfo.pRbPlyMapTbl,
                       QoSPlyMapTblRBTFreeNodeFn, 0);
        gQoSGlobalInfo.pRbPlyMapTbl = NULL;
    }

    if (gQoSGlobalInfo.pRbQMapTbl != NULL)
    {
        RBTreeDestroy (gQoSGlobalInfo.pRbQMapTbl, QoSQMapTblRBTFreeNodeFn, 0);
        gQoSGlobalInfo.pRbQMapTbl = NULL;
    }

    if (gQoSGlobalInfo.pRbQTbl != NULL)
    {
        RBTreeDestroy (gQoSGlobalInfo.pRbQTbl, QoSQTblRBTFreeNodeFn, 0);
        gQoSGlobalInfo.pRbQTbl = NULL;
    }

    if (gQoSGlobalInfo.pRbSchedTbl != NULL)
    {
        RBTreeDestroy (gQoSGlobalInfo.pRbSchedTbl, QoSSchedTblRBTFreeNodeFn, 0);
        gQoSGlobalInfo.pRbSchedTbl = NULL;
    }

    if (gQoSGlobalInfo.pRbHierarchyTbl != NULL)
    {
        RBTreeDestroy (gQoSGlobalInfo.pRbHierarchyTbl,
                       QoSHierarchyTblRBTFreeNodeFn, 0);
        gQoSGlobalInfo.pRbHierarchyTbl = NULL;
    }

    if (gQoSGlobalInfo.pRbClsInfoTbl != NULL)
    {
        RBTreeDestroy (gQoSGlobalInfo.pRbClsInfoTbl,
                       QoSClsInfoTblRBTFreeNodeFn, 0);
        gQoSGlobalInfo.pRbClsInfoTbl = NULL;
    }
    if (gQoSGlobalInfo.DsDataPathTbl != NULL)
    {
        RBTreeDestroy (gQoSGlobalInfo.DsDataPathTbl, QoSDataPathEntryNodeFn, 0);
        gQoSGlobalInfo.DsDataPathTbl = NULL;
    }
    if (gQoSGlobalInfo.DsClfrTbl != NULL)
    {
        RBTreeDestroy (gQoSGlobalInfo.DsClfrTbl, QoSClfrNodeFn, 0);
        gQoSGlobalInfo.DsClfrTbl = NULL;
    }
    if (gQoSGlobalInfo.DsClfrElementTbl != NULL)
    {
        RBTreeDestroy (gQoSGlobalInfo.DsClfrElementTbl, QoSClfrElementNodeFn,
                       0);
        gQoSGlobalInfo.DsClfrElementTbl = NULL;
    }
    if (gQoSGlobalInfo.DsMeterTbl != NULL)
    {
        RBTreeDestroy (gQoSGlobalInfo.DsMeterTbl, QoSStdMeterNodeFn, 0);
        gQoSGlobalInfo.DsMeterTbl = NULL;
    }
    if (gQoSGlobalInfo.DsActionTbl != NULL)
    {
        RBTreeDestroy (gQoSGlobalInfo.DsActionTbl, QoSActionNodeFn, 0);
        gQoSGlobalInfo.DsActionTbl = NULL;
    }
    if (gQoSGlobalInfo.DsCountActTbl != NULL)
    {
        RBTreeDestroy (gQoSGlobalInfo.DsCountActTbl, QoSCountActNodeFn, 0);
        gQoSGlobalInfo.DsCountActTbl = NULL;
    }
    if (gQoSGlobalInfo.DsQHead != NULL)
    {
        RBTreeDestroy (gQoSGlobalInfo.DsQHead, QoSQueueNodeFn, 0);
        gQoSGlobalInfo.DsQHead = NULL;
    }
    if (gQoSGlobalInfo.DsSchedulerTbl != NULL)
    {
        RBTreeDestroy (gQoSGlobalInfo.DsSchedulerTbl, QoSSchedulerNodeFn, 0);
        gQoSGlobalInfo.DsSchedulerTbl = NULL;
    }

    if (gQoSGlobalInfo.DsMinRateTbl != NULL)
    {
        RBTreeDestroy (gQoSGlobalInfo.DsMinRateTbl, QoSMinRateNodeFn, 0);
        gQoSGlobalInfo.DsMinRateTbl = NULL;
    }
    if (gQoSGlobalInfo.DsMaxRateTbl != NULL)
    {
        RBTreeDestroy (gQoSGlobalInfo.DsMaxRateTbl, QoSMaxRateNodeFn, 0);
        gQoSGlobalInfo.DsMaxRateTbl = NULL;
    }
    if (gQoSGlobalInfo.pRbSchedGlobTbl != NULL)
    {
        RBTreeDestroy (gQoSGlobalInfo.pRbSchedGlobTbl, QoSSchedMapNodeFn, 0);
        gQoSGlobalInfo.pRbSchedGlobTbl = NULL;
    }
    if (gQoSGlobalInfo.QosCpuQTbl != NULL)
    {
        RBTreeDestroy (gQoSGlobalInfo.QosCpuQTbl, QoSUtlCpuQTblRBTFreeNodeFn,
                       0);
        gQoSGlobalInfo.QosCpuQTbl = NULL;
    }
    if (gQoSGlobalInfo.pRbPortTbl != NULL)
    {
        RBTreeDestroy (gQoSGlobalInfo.pRbPortTbl, QoSPortTblRBTFreeNodeFn, 0);
        gQoSGlobalInfo.pRbPortTbl = NULL;
    }

    /*  Delete SLL Entries */
    /* Q Template Table */
    if ((TMO_SLL_Count (&(gQoSGlobalInfo.SllQTempTbl))) > 0)
    {
        UTL_SLL_OFFSET_SCAN (&(gQoSGlobalInfo.SllQTempTbl),
                             pCurrNode, pNextNode, tTMO_SLL_NODE *)
        {
            TMO_SLL_Delete (&(gQoSGlobalInfo.SllQTempTbl), pCurrNode);

            i4RetStatus =
                MemReleaseMemBlock (gQoSGlobalInfo.QoSQTempTblMemPoolId,
                                    (UINT1 *) pCurrNode);

            if (i4RetStatus != MEM_SUCCESS)
            {
                QOS_TRC_ARG1 (MGMT_TRC, "In %s :QTempTbl-MemReleaseMemBlock"
                              " () Failed.\r\n", __FUNCTION__);

                return (QOS_FAILURE);
            }

        }                        /* End of Scan */

    }                            /* End of if */

    /* Random Discard  Table */
    if ((TMO_SLL_Count (&(gQoSGlobalInfo.SllRDTbl))) > 0)
    {
        UTL_SLL_OFFSET_SCAN (&(gQoSGlobalInfo.SllRDTbl),
                             pCurrNode, pNextNode, tTMO_SLL_NODE *)
        {
            TMO_SLL_Delete (&(gQoSGlobalInfo.SllRDTbl), pCurrNode);

            i4RetStatus =
                MemReleaseMemBlock (gQoSGlobalInfo.QoSRDTblMemPoolId,
                                    (UINT1 *) pCurrNode);

            if (i4RetStatus != MEM_SUCCESS)
            {
                QOS_TRC_ARG1 (MGMT_TRC, "In %s : RDTbl-MemReleaseMemBlock"
                              " () Failed.\r\n", __FUNCTION__);

                return (QOS_FAILURE);
            }

        }                        /* End of Scan */

    }                            /* End of if */

    /* Shape Template Table */
    if ((TMO_SLL_Count (&(gQoSGlobalInfo.SllShapeTempTbl))) > 0)
    {

        UTL_SLL_OFFSET_SCAN (&(gQoSGlobalInfo.SllShapeTempTbl),
                             pCurrNode, pNextNode, tTMO_SLL_NODE *)
        {
            TMO_SLL_Delete (&(gQoSGlobalInfo.SllShapeTempTbl), pCurrNode);
            i4RetStatus =
                MemReleaseMemBlock (gQoSGlobalInfo.QoSShapeTempTblMemPoolId,
                                    (UINT1 *) pCurrNode);

            if (i4RetStatus != MEM_SUCCESS)
            {
                QOS_TRC_ARG1 (MGMT_TRC, "In %s : ShapeTempTbl- "
                              "MemReleaseMemBlock () Failed.\r\n",
                              __FUNCTION__);

                return (QOS_FAILURE);
            }

        }                        /* End of Scan */

    }                            /* End of if */

    return (QOS_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSCreateTables                                      */
/* Description        : This function is used to Create RbTrees and Init the */
/*                      SLL_Node for QoS Tables.                             */
/* Input(s)           : None.                                                */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : gQoSGlobalInfo                                       */
/* Global Variables                                                          */
/* Modified           : gQoSGlobalInfo                                       */
/* Return Value(s)    : QOS_SUCCESS or QOS_FAILURE                           */
/* Called By          : QoSStart                                             */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QoSCreateTables (VOID)
{
    /* 1. Incoming Priority Map Table  */
    gQoSGlobalInfo.pRbInPriMapTbl =
        RBTreeCreateEmbedded (FSAP_OFFSETOF (tQoSInPriorityMapNode, RbNode),
                              QoSInPriMapCmpFun);

    if (gQoSGlobalInfo.pRbInPriMapTbl == NULL)
    {
        QOS_TRC_ARG1 (OS_RESOURCE_TRC, "%s :PriMapTbl-RBTreeCreateEmbedded ()"
                      " Failed!.\r\n", __FUNCTION__);

        QoSDeleteTables ();

        return (QOS_FAILURE);
    }

    /* 1a. Incoming Priority Map Unique Table  */
    gQoSGlobalInfo.pRbInPriMapUniTbl =
        RBTreeCreateEmbedded (FSAP_OFFSETOF (tQoSInPriorityMapNode,
                                             RbNodeUnique),
                              QoSInPriMapUniCmpFun);

    if (gQoSGlobalInfo.pRbInPriMapUniTbl == NULL)
    {
        QOS_TRC_ARG1 (OS_RESOURCE_TRC, "%s :PriMapUniqueTbl- "
                      "RBTreeCreateEmbedded () Failed!.\r\n", __FUNCTION__);

        QoSDeleteTables ();

        return (QOS_FAILURE);
    }

    /* a. Incoming Vlan queueing Map Table  */
    gQoSGlobalInfo.pRbVlanQueueingMapTbl =
        RBTreeCreateEmbedded (FSAP_OFFSETOF (tQoSInVlanMapNode, RbNode),
                              QoSVlanQMapCmpFun);

    if (gQoSGlobalInfo.pRbVlanQueueingMapTbl == NULL)
    {
        QOS_TRC_ARG1 (OS_RESOURCE_TRC, "%s :VlanQMap-RBTreeCreateEmbedded ()"
                      " Failed!.\r\n", __FUNCTION__);
        QoSDeleteTables ();
        return (QOS_FAILURE);
    }

    /* 2. Class Map Table  */
    gQoSGlobalInfo.pRbClsMapTbl =
        RBTreeCreateEmbedded (FSAP_OFFSETOF (tQoSClassMapNode, RbNode),
                              QoSClsMapCmpFun);
    if (gQoSGlobalInfo.pRbClsMapTbl == NULL)
    {
        QOS_TRC_ARG1 (OS_RESOURCE_TRC, "%s : ClsMapTbl-RBTreeCreateEmbedded ()"
                      " Failed!.\r\n", __FUNCTION__);

        QoSDeleteTables ();

        return (QOS_FAILURE);
    }

    /* 3. Class to Priority Map Table  */
    gQoSGlobalInfo.pRbCls2PriMapTbl =
        RBTreeCreateEmbedded (FSAP_OFFSETOF (tQoSClass2PriorityNode, RbNode),
                              QoSCls2PriMapCmpFun);

    if (gQoSGlobalInfo.pRbCls2PriMapTbl == NULL)
    {
        QOS_TRC_ARG1 (OS_RESOURCE_TRC, "%s : Cls2PriMapTbl- "
                      "RBTreeCreateEmbedded () Failed!.\r\n", __FUNCTION__);

        QoSDeleteTables ();

        return (QOS_FAILURE);
    }

    /* 4. Meter Table  */
    gQoSGlobalInfo.pRbMeterTbl =
        RBTreeCreateEmbedded (FSAP_OFFSETOF (tQoSMeterNode, RbNode),
                              QoSMeterCmpFun);

    if (gQoSGlobalInfo.pRbMeterTbl == NULL)
    {
        QOS_TRC_ARG1 (OS_RESOURCE_TRC, "%s : MeterTbl- "
                      "RBTreeCreateEmbedded () Failed!.\r\n", __FUNCTION__);

        QoSDeleteTables ();

        return (QOS_FAILURE);
    }

    /* 4.1 StdMeter Table  */
    gQoSGlobalInfo.DsMeterTbl =
        RBTreeCreateEmbedded (FSAP_OFFSETOF (tQosStdMeterEntry, RbNode),
                              QoSStdMeterCmpFun);

    if (gQoSGlobalInfo.pRbMeterTbl == NULL)
    {
        QOS_TRC_ARG1 (OS_RESOURCE_TRC, "%s : StdMeterTbl- "
                      "RBTreeCreateEmbedded () Failed!.\r\n", __FUNCTION__);

        QoSDeleteTables ();

        return (QOS_FAILURE);
    }

    /* 5. Policy Map Table */
    gQoSGlobalInfo.pRbPlyMapTbl =
        RBTreeCreateEmbedded (FSAP_OFFSETOF (tQoSPolicyMapNode, RbNode),
                              QoSPlyMapCmpFun);
    if (gQoSGlobalInfo.pRbPlyMapTbl == NULL)
    {
        QOS_TRC_ARG1 (OS_RESOURCE_TRC, "%s : PolicyMapTbl- "
                      "RBTreeCreateEmbedded () Failed!.\r\n", __FUNCTION__);

        QoSDeleteTables ();

        return (QOS_FAILURE);
    }

    /* 6. Q Template Table */
    TMO_SLL_Init (&(gQoSGlobalInfo.SllQTempTbl));
    /* 7. Random Detect Table */
    TMO_SLL_Init (&(gQoSGlobalInfo.SllRDTbl));
    /* 8. Shape Template Table */
    TMO_SLL_Init (&(gQoSGlobalInfo.SllShapeTempTbl));
    TMO_SLL_Init (&(gQoSGlobalInfo.SllQosPointedTo));

    /* 9. Q Map Table */
    gQoSGlobalInfo.pRbQMapTbl =
        RBTreeCreateEmbedded (FSAP_OFFSETOF (tQoSQMapNode, RbNode),
                              QoSQMapCmpFun);
    if (gQoSGlobalInfo.pRbQMapTbl == NULL)
    {
        QOS_TRC_ARG1 (OS_RESOURCE_TRC, "%s : QMapTbl- "
                      "RBTreeCreateEmbedded () Failed!.\r\n", __FUNCTION__);

        QoSDeleteTables ();

        return (QOS_FAILURE);
    }
    /* 10. Q Table */
    gQoSGlobalInfo.pRbQTbl =
        RBTreeCreateEmbedded (FSAP_OFFSETOF (tQoSQNode, RbNode), QoSQCmpFun);
    if (gQoSGlobalInfo.pRbQTbl == NULL)
    {
        QOS_TRC_ARG1 (OS_RESOURCE_TRC, "%s : QTbl- "
                      "RBTreeCreateEmbedded () Failed!.\r\n", __FUNCTION__);

        QoSDeleteTables ();

        return (QOS_FAILURE);
    }

    /* 10.1 StdQ Table */
    gQoSGlobalInfo.DsQHead =
        RBTreeCreateEmbedded (FSAP_OFFSETOF (tQosStdQEntry, RbNode),
                              DiffServStdQCmpFun);
    if (gQoSGlobalInfo.DsQHead == NULL)
    {
        QOS_TRC_ARG1 (OS_RESOURCE_TRC, "%s : QTbl- "
                      "RBTreeCreateEmbedded () Failed!.\r\n", __FUNCTION__);

        QoSDeleteTables ();

        return (QOS_FAILURE);
    }

    /* 11. Scheduler Table */
    gQoSGlobalInfo.pRbSchedTbl =
        RBTreeCreateEmbedded (FSAP_OFFSETOF (tQoSSchedNode, RbNode),
                              QoSSchedCmpFun);
    if (gQoSGlobalInfo.pRbSchedTbl == NULL)
    {
        QOS_TRC_ARG1 (OS_RESOURCE_TRC, "%s : Scheduler Tbl- "
                      "RBTreeCreateEmbedded () Failed!.\r\n", __FUNCTION__);

        QoSDeleteTables ();

        return (QOS_FAILURE);
    }
    /* 12. Hierarchy Table */
    gQoSGlobalInfo.pRbHierarchyTbl =
        RBTreeCreateEmbedded (FSAP_OFFSETOF (tQoSQNode, RbNode),
                              QoSHierarchyCmpFun);
    if (gQoSGlobalInfo.pRbHierarchyTbl == NULL)
    {
        QOS_TRC_ARG1 (OS_RESOURCE_TRC, "%s : Hierarchy Tbl- "
                      "RBTreeCreateEmbedded () Failed!.\r\n", __FUNCTION__);

        QoSDeleteTables ();

        return (QOS_FAILURE);
    }

    /* 13. Class Info Table */
    gQoSGlobalInfo.pRbClsInfoTbl =
        RBTreeCreateEmbedded (FSAP_OFFSETOF (tQoSClassInfoNode, RbNode),
                              QoSClsInfoCmpFun);
    if (gQoSGlobalInfo.pRbClsInfoTbl == NULL)
    {
        QOS_TRC_ARG1 (OS_RESOURCE_TRC, "%s : ClsInfoTbl- "
                      "RBTreeCreateEmbedded () Failed!.\r\n", __FUNCTION__);

        QoSDeleteTables ();

        return (QOS_FAILURE);
    }
    /* 14. Data Path Table  */
    gQoSGlobalInfo.DsDataPathTbl =
        RBTreeCreateEmbedded (FSAP_OFFSETOF (tQosStdDataPathEntry, RbNode),
                              QoSDataPathEntryCmpFun);
    if (gQoSGlobalInfo.DsDataPathTbl == NULL)
    {
        QOS_TRC_ARG1 (OS_RESOURCE_TRC,
                      "%s : DataPathTbl-RBTreeCreateEmbedded ()"
                      " Failed!.\r\n", __FUNCTION__);

        QoSDeleteTables ();

        return (QOS_FAILURE);
    }
    /* 15. Classifier  Table  */
    gQoSGlobalInfo.DsClfrTbl =
        RBTreeCreateEmbedded (FSAP_OFFSETOF (tQosStdClfrEntry, RbNode),
                              QoSClfrCmpFun);
    if (gQoSGlobalInfo.DsClfrTbl == NULL)
    {
        QOS_TRC_ARG1 (OS_RESOURCE_TRC,
                      "%s : ClassifierTbl-RBTreeCreateEmbedded ()"
                      " Failed!.\r\n", __FUNCTION__);

        QoSDeleteTables ();

        return (QOS_FAILURE);
    }
    /* 16. Classifier Element Table  */
    gQoSGlobalInfo.DsClfrElementTbl =
        RBTreeCreateEmbedded (FSAP_OFFSETOF (tQosStdClfrElementEntry, RbNode),
                              QoSClfrElementCmpFun);
    if (gQoSGlobalInfo.DsClfrElementTbl == NULL)
    {
        QOS_TRC_ARG1 (OS_RESOURCE_TRC,
                      "%s : ClfrElementTbl-RBTreeCreateEmbedded ()"
                      " Failed!.\r\n", __FUNCTION__);

        QoSDeleteTables ();

        return (QOS_FAILURE);
    }
    /* Action Table */
    gQoSGlobalInfo.DsActionTbl =
        RBTreeCreateEmbedded (FSAP_OFFSETOF (tQosStdActionEntry, RbNode),
                              QoSActionCmpFun);
    if (gQoSGlobalInfo.DsActionTbl == NULL)
    {
        QOS_TRC_ARG1 (OS_RESOURCE_TRC, "%s : Action- "
                      "RBTreeCreateEmbedded () Failed!.\r\n", __FUNCTION__);

        QoSDeleteTables ();

        return (QOS_FAILURE);
    }
    /* Count Action Table */
    gQoSGlobalInfo.DsCountActTbl =
        RBTreeCreateEmbedded (FSAP_OFFSETOF (tQosStdCountActEntry, RbNode),
                              QoSCountActCmpFun);
    if (gQoSGlobalInfo.DsCountActTbl == NULL)
    {
        QOS_TRC_ARG1 (OS_RESOURCE_TRC, "%s :Count Action- "
                      "RBTreeCreateEmbedded () Failed!.\r\n", __FUNCTION__);

        QoSDeleteTables ();

        return (QOS_FAILURE);
    }

    /* Scheduler Table */
    gQoSGlobalInfo.DsSchedulerTbl =
        RBTreeCreateEmbedded (FSAP_OFFSETOF (tQosStdSchedulerEntry, RbNode),
                              QoSSchedulerCmpFun);
    if (gQoSGlobalInfo.DsSchedulerTbl == NULL)
    {
        QOS_TRC_ARG1 (OS_RESOURCE_TRC, "%s : Scheduler- "
                      "RBTreeCreateEmbedded () Failed!.\r\n", __FUNCTION__);

        QoSDeleteTables ();

        return (QOS_FAILURE);
    }

    /* MinRate Table */
    gQoSGlobalInfo.DsMinRateTbl =
        RBTreeCreateEmbedded (FSAP_OFFSETOF (tQosStdMinRateEntry, RbNode),
                              QoSMinRateCmpFun);
    if (gQoSGlobalInfo.DsMinRateTbl == NULL)
    {
        QOS_TRC_ARG1 (OS_RESOURCE_TRC, "%s : MinRate- "
                      "RBTreeCreateEmbedded () Failed!.\r\n", __FUNCTION__);

        QoSDeleteTables ();

        return (QOS_FAILURE);
    }

    /* 18 MaxRate Table */
    gQoSGlobalInfo.DsMaxRateTbl =
        RBTreeCreateEmbedded (FSAP_OFFSETOF (tQosStdMaxRateEntry, RbNode),
                              QoSMaxRateCmpFun);
    if (gQoSGlobalInfo.DsMaxRateTbl == NULL)
    {
        QOS_TRC_ARG1 (OS_RESOURCE_TRC, "%s : MaxRate- "
                      "RBTreeCreateEmbedded () Failed!.\r\n", __FUNCTION__);

        QoSDeleteTables ();

        return (QOS_FAILURE);
    }

    /* 19. Scheduler Mapping Table to maintain Global Id's */

    gQoSGlobalInfo.pRbSchedGlobTbl =
        RBTreeCreateEmbedded (FSAP_OFFSETOF (tQosSchedulerMapNode, RbNode),
                              QoSSchedMapCmpFun);
    if (gQoSGlobalInfo.pRbSchedGlobTbl == NULL)
    {
        QOS_TRC_ARG1 (OS_RESOURCE_TRC, "%s : Scheduler Tbl- "
                      "RBTreeCreateEmbedded () Failed!.\r\n", __FUNCTION__);

        QoSDeleteTables ();

        return (QOS_FAILURE);
    }

    /* Cpu Q Rate Limit Table */
    gQoSGlobalInfo.QosCpuQTbl =
        RBTreeCreateEmbedded (FSAP_OFFSETOF (tQosCpuQEntry, RbNode),
                              QosUtlCpuQCmpFun);
    if (gQoSGlobalInfo.QosCpuQTbl == NULL)
    {
        QOS_TRC_ARG1 (OS_RESOURCE_TRC, "%s : CpuQTbl- "
                      "RBTreeCreateEmbedded () Failed!.\r\n", __FUNCTION__);

        QoSDeleteTables ();

        return (QOS_FAILURE);
    }
    /* Port Table */
    gQoSGlobalInfo.pRbPortTbl =
        RBTreeCreateEmbedded (FSAP_OFFSETOF (tQoSPortTblEntry, RbNode),
                              QoSPortTblCmpFun);
    if (gQoSGlobalInfo.pRbPortTbl == NULL)
    {
        QOS_TRC_ARG1 (OS_RESOURCE_TRC, "%s : PortTbl- "
                      "RBTreeCreateEmbedded () Failed!.\r\n", __FUNCTION__);

        QoSDeleteTables ();

        return (QOS_FAILURE);
    }

    /*  RBTree added in case of DPPortList present in tQosStdClfrEntry is to
     *  be tuned dynamically. */
    if (QOS_CREATE_STD_CLFR_PORT_RBTREE () == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (OS_RESOURCE_TRC, "%s : - gQosStdClfrPortTable "
                      "RBTreeCreateEmbedded () Failed!.\r\n", __FUNCTION__);

        QoSDeleteTables ();

        return (QOS_FAILURE);
    }
    return (QOS_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSAddDefPriorityMapTblEntries                       */
/* Description        : This function is used to Create default tables for   */
/*                      PriorityMap Table Entry.                             */
/* Input(s)           : None.                                                */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           :                                                      */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : QOS_SUCCESS or QOS_FAILURE                           */
/* Called By          : QoSAddDefTblEntries                                  */
/* Calling Function   :                                                      */
/*****************************************************************************/

INT4
QoSAddDefPriorityMapTblEntries ()
{

    tQoSInPriorityMapNode *pPMNode = NULL;
    INT4                i4RetStatus = QOS_FAILURE;
    UINT1               u1IPVal = 0;
    UINT4               u4PriMapId = 0;

    /*Add PriorityMap Table Entries */
    /*-------------------------------------------------------*/
    /*| Id | IfIndex | VlanId | IPT | IPV | RPV | RIPV | CS | */
    /*-------------------------------------------------------*/
    /*| 1  |    0    |   0    | VP  |  0  |  0  |  8   | SD | */
    /*| 2  |    0    |   0    | VP  |  1  |  1  |  8   | SD | */
    /*| 3  |    0    |   0    | VP  |  2  |  2  |  8   | SD | */
    /*| 4  |    0    |   0    | VP  |  3  |  3  |  8   | SD | */
    /*| 5  |    0    |   0    | VP  |  4  |  4  |  8   | SD | */
    /*| 6  |    0    |   0    | VP  |  5  |  5  |  8   | SD | */
    /*| 7  |    0    |   0    | VP  |  6  |  6  |  8   | SD | */
    /*| 8  |    0    |   0    | VP  |  7  |  7  |  8   | SD | */

    for (u1IPVal = 0; u1IPVal <= QOS_PM_TBL_DEF_ENTRY_MAX; u1IPVal++)
    {
        u4PriMapId = u1IPVal + 1;

        pPMNode = QoSCreatePriMapTblEntry (u4PriMapId);
        if (pPMNode == NULL)
        {
            return (QOS_FAILURE);
        }

        /* Update the Entry  and dependent Tbls */
        pPMNode->u4Id = u4PriMapId;
        pPMNode->u1ConfStatus = QOS_PRI_MAP_TBL_CONF_STATUS_SYS;
        pPMNode->u1RegenInnerPriority = QOS_PRI_MAP_TBL_INVLD_INREGPRI;
        pPMNode->u4IfIndex = 0;
        pPMNode->u2VlanId = 0;
        pPMNode->u1InPriType = QOS_PM_TBL_DEF_ENTRY_TYPE;
        pPMNode->u1InPriority = u1IPVal;
        pPMNode->u1RegenPriority = u1IPVal;
        pPMNode->u1Status = ACTIVE;

        /* Add into the UniPriMapTble */
        i4RetStatus = QoSAddPriMapUniTblEntry (pPMNode);
        if (i4RetStatus == QOS_FAILURE)
        {
            return (QOS_FAILURE);
        }
        i4RetStatus = QoSHwWrMapClasstoPriMap (pPMNode, QOS_NP_ADD);
        if ((i4RetStatus == QOS_FAILURE)
            || (i4RetStatus == QOS_NP_NOT_SUPPORTED))
        {
            QOS_TRC_ARG1 (MGMT_TRC,
                          "In %s : QoSHwWrMapClasstoPriMap "
                          " () Returns FAILURE. \r\n", __FUNCTION__);

            return (QOS_FAILURE);
        }
    }                            /*End of -for */

    return (QOS_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSAddDefClassMapTblEntries                          */
/* Description        : This function is used to Create default tables for   */
/*                      ClassMap Table Entry.                                */
/* Input(s)           : None.                                                */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           :                                                      */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : QOS_SUCCESS or QOS_FAILURE                           */
/* Called By          : QoSAddDefTblEntries                                  */
/* Calling Function   :                                                      */
/*****************************************************************************/
/* Default table for Class Map*/
INT4
QoSAddDefClassMapTblEntries ()
{
    tQoSInPriorityMapNode *pPriMapNode = NULL;
    tQoSClassMapNode   *pCMNode = NULL;
    tQoSClassInfoNode  *pClsInfoNode = NULL;
    INT4                i4RetStatus = QOS_FAILURE;
#ifdef DEFAULT_PCP_WANTED
    INT4                i4IfIndex;
#endif
    UINT1               u1IPVal = 0;
    UINT4               u4ClassMapId = 0;
    UINT4               u4PrMaId = 0;
    /* Add Priority Code Point table entries for 8P0D Model for Ethernet
     * Packet Type and for all the Physical Interfaces*/
#ifdef DEFAULT_PCP_WANTED
    for (i4IfIndex = 1; i4IfIndex <= SYS_DEF_MAX_PHYSICAL_INTERFACES;
         i4IfIndex++)
    {
        i4RetStatus = QoSAddDefPcpTblEntries (i4IfIndex);
        if (i4RetStatus == QOS_FAILURE)
        {
            return (QOS_FAILURE);
        }
    }
#endif
    /*Add PriorityMap Table Entries */
    /*-----------------------------------------------------------*/
    /*| Id | L2filterId | L3FilterId | Priority MapId | Precolor */
    /*-----------------------------------------------------------*/
    /*| 1  |    0       |   0        | 1               |  NONE  | */
    /*| 2  |    0       |   0        | 2               |  NONE  | */
    /*| 3  |    0       |   0        | 3               |  NONE  | */
    /*| 4  |    0       |   0        | 4               |  NONE  | */
    /*| 5  |    0       |   0        | 5               |  NONE  | */
    /*| 6  |    0       |   0        | 6               |  NONE  | */
    /*| 7  |    0       |   0        | 7               |  NONE  | */
    /*| 8  |    0       |   0        | 8               |  NONE  | */

    for (u1IPVal = 0; u1IPVal < QOS_CM_TBL_DEF_ENTRY_MAX; u1IPVal++)
    {
        u4ClassMapId = u1IPVal + 1;
        u4PrMaId = u4ClassMapId;
        pCMNode = QoSCreateClsMapTblEntry (u4ClassMapId);
        if (pCMNode == NULL)
        {
            return (QOS_FAILURE);
        }

        /* Update the Entry  and dependent Tbls */
        pCMNode->u4Id = u4ClassMapId;
        pCMNode->u4L2FilterId = 0;
        pCMNode->u4L3FilterId = 0;
        pCMNode->u4PriorityMapId = u4PrMaId;
        pCMNode->u4ClassId = QOS_CM_TBL_DEF_CLASS;
        pCMNode->u1PreColor = QOS_CLS_DEFAULT_PRE_COLOR;
        pCMNode->u1Status = ACTIVE;

        /*  increment the ref count */
        if (u4PrMaId != 0)
        {
            pPriMapNode = QoSUtlGetPriorityMapNode (u4PrMaId);
            if (pPriMapNode == NULL)
            {
                QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetPriorityMapNode () "
                              "Returns FAILURE. \r\n", __FUNCTION__);

                return (SNMP_FAILURE);
            }

            /* Incremnet the RefCount of PriMapId  Entry in the PriorityMap Table */
            pPriMapNode->u4RefCount = (pPriMapNode->u4RefCount) + 1;
        }

        /* This Function is used to Add ClassMapIds to the respective 
         * CLASS using the internal DataSrtuct 'tQoSClassInfoNode'
         */
        i4RetStatus = QoSClsInfoAddFilters (QOS_CM_TBL_DEF_CLASS, pCMNode);
        if (i4RetStatus == QOS_FAILURE)
        {
            QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSClsInfoAddFilters () "
                          "Returns FAILURE. \r\n", __FUNCTION__);

            return (SNMP_FAILURE);
        }
        /* Configure the HW  if Needed */

        if (gQoSGlobalInfo.eSysStatus == QOS_SYS_STATUS_ENABLE)
        {
            pClsInfoNode = QoSUtlGetClassInfoNode (QOS_CM_TBL_DEF_CLASS);
            if ((pClsInfoNode != NULL) && (pClsInfoNode->pPlyMapNode != NULL))
            {
                /* if a Policy is already create for this Class 
                 * Get the Policy Map Add the Class (Filters) to it */
                i4RetStatus =
                    QoSHwWrMapClassToPolicy (pClsInfoNode->pPlyMapNode,
                                             pCMNode, QOS_PLY_MAP);
                if (i4RetStatus == QOS_FAILURE)
                {
                    QOS_TRC_ARG1 (MGMT_TRC,
                                  "In %s : QoSHwWrMapClassToPolicy "
                                  " () Returns FAILURE. \r\n", __FUNCTION__);

                    return (SNMP_FAILURE);
                }

            }                    /* End of If */
        }

    }                            /*End of -for */
    return (QOS_SUCCESS);
}

/* End of default table for class map*/

/*****************************************************************************/
/* Function Name      : QoSAddDefPolicyMapTblEntries                         */
/* Description        : This function is used to Create default tables for   */
/*                      PolicyMap Table Entry.                               */
/* Input(s)           : None.                                                */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           :                                                      */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : QOS_SUCCESS or QOS_FAILURE                           */
/* Called By          : QoSAddDefTblEntries                                  */
/* Calling Function   :                                                      */
/*****************************************************************************/
/*  default table for policy  map*/

INT4
QoSAddDefPolicyMapTblEntries ()
{

    tQoSPolicyMapNode  *pPOLMNode = NULL;
    INT4                i4RetStatus = QOS_FAILURE;
    UINT1               u1IPVal = 0;
    UINT4               u4PolMapId = 0;

    for (u1IPVal = 0; u1IPVal < QOS_POL_TBL_DEF_ENTRY_MAX; u1IPVal++)
    {
        u4PolMapId = u1IPVal + 1;

        pPOLMNode = QoSCreatePlyMapTblEntry (u4PolMapId);
        if (pPOLMNode == NULL)
        {
            return (QOS_FAILURE);
        }

        /* Update the Entry  and dependent Tbls */
        pPOLMNode->u4Id = u4PolMapId;
        pPOLMNode->u4IfIndex = 0;
        pPOLMNode->u4ClassId = QOS_CM_TBL_DEF_CLASS;
        pPOLMNode->u4MeterTableId = 0;
        pPOLMNode->u1PHBType = QOS_PLY_DEFAULT_PHB_TYPE;
        pPOLMNode->u2DefaultPHB = QOS_PLY_DEFAULT_PHB_VAL;
        pPOLMNode->u1Status = ACTIVE;

        /* Add the entry from the Internal Reference. */
        if (QOS_CM_TBL_DEF_CLASS != 0)
        {
            i4RetStatus = QoSClsInfoAddPolicy (QOS_CM_TBL_DEF_CLASS, pPOLMNode);
            if (i4RetStatus == QOS_FAILURE)
            {
                QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSClsInfoAddPolicy () "
                              "Returns FAILURE. \r\n", __FUNCTION__);

                return (SNMP_FAILURE);
            }
        }
        /* Configure the HW  if Needed */
        if (gQoSGlobalInfo.eSysStatus == QOS_SYS_STATUS_ENABLE)
        {
            /* 1. Add the entry into:the HARDWARE. */
            i4RetStatus = QoSHwWrMapClassToPolicy (pPOLMNode, NULL,
                                                   QOS_PLY_ADD);
            if (i4RetStatus == QOS_FAILURE)
            {
                QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSHwWrMapClassToPolicy "
                              " () Returns FAILURE. \r\n", __FUNCTION__);

                return (SNMP_FAILURE);
            }
        }

    }                            /*End of -for */

    return (QOS_SUCCESS);

    /* End of default table for Policy map */
}

/*****************************************************************************/
/* Function Name      : QoSAddDefQTempTblEntries                             */
/* Description        : This function is used to Create default tables for   */
/*                      QueueTemplate Table Entry.                           */
/* Input(s)           : None.                                                */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           :                                                      */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : QOS_SUCCESS or QOS_FAILURE                           */
/* Called By          : QoSAddDefTblEntries                                  */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QoSAddDefQTempTblEntries ()
{

    tQoSQTemplateNode  *pQTNode = NULL;
    UINT1               u1IPVal = 0;
    UINT4               u4QTempId = 0;

    /*Add QueueTemplate Table Entries */
    /*-----------------------------------------------|*/
    /*| Id | DropType | DropEnableFlag | TemplateSize| */
    /*-----------------------------------------------|*/
    /*| 1  |TailDrop  |   Disable      |   10000     | */
    for (u1IPVal = 0; u1IPVal < QOS_QT_TBL_DEF_ENTRY_MAX; u1IPVal++)
    {
        u4QTempId = u1IPVal + 1;

        pQTNode = QoSCreateQTempTblEntry (u4QTempId);
        if (pQTNode == NULL)
        {
            return (QOS_FAILURE);
        }

        /* Update the Entry  and dependent Tbls */
        pQTNode->u4Id = u4QTempId;
        pQTNode->u4Size = QOS_Q_TEMP_SIZE_DEFAULT;
        pQTNode->u2DropType = QOS_Q_TEMP_DROP_TYPE_TAIL;
        pQTNode->u1DropAlgoEnableFlag = QOS_Q_TEMP_DROP_ALGO_DISABLE;
        pQTNode->u1Status = ACTIVE;

        /* Configure the HW  if Needed */

    }                            /*End of -for */

    return (QOS_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSAddDefSchedTblEntries                             */
/* Description        : This function is used to Create default tables for   */
/*                      Scheduler Table Entry for the given port number.     */
/* Input(s)           : u4Port - port number for which schedule Tbl entry is */
/*                               created                                     */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           :                                                      */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : QOS_SUCCESS or QOS_FAILURE                           */
/* Called By          : QosMainProcessQMsgEvent                              */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QoSAddDefSchedTblEntries (UINT4 u4Port)
{

    tQoSSchedNode      *pSchNode = NULL;
    INT4                i4RetStatus = QOS_FAILURE;
    UINT4               u4SchId = 1;

    /* If the port is part of port channel Skip the entry    */
    /* The scheduler creation will not be applicable for the */
    /* port part of port-channel.                            */
    if (QosL2IwfIsPortInPortChannel (u4Port) == L2IWF_SUCCESS)
    {
        return (QOS_SUCCESS);
    }

    pSchNode = QoSCreateSchedTblEntry ((INT4) u4Port, u4SchId);
    if (pSchNode == NULL)
    {
        return (QOS_FAILURE);
    }

    /* Update the Entry  and dependent Tbls */
    pSchNode->u4Id = u4SchId;
    pSchNode->u4ShapeId = 0;
    pSchNode->i4IfIndex = (INT4) u4Port;
    pSchNode->u1SchedAlgorithm = QOS_SCHED_ALGO_DEFAULT;
    pSchNode->u1Status = ACTIVE;

    /* Configure the HW  if Needed */
    /* 1. Add the entry into the HARDWARE. */
    if (gQoSGlobalInfo.eSysStatus == QOS_SYS_STATUS_ENABLE)
    {
        i4RetStatus = QoSHwWrSchedulerAdd (pSchNode->i4IfIndex, pSchNode->u4Id);
        if (i4RetStatus == QOS_FAILURE)
        {
            QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSHwWrSchedulerAdd ()"
                          " Returns FAILURE.\r\n", __FUNCTION__);
            return (SNMP_FAILURE);
        }
    }

    return (QOS_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSAddDefHierarchyTblEntries                         */
/* Description        : This function is used to Create default tables for   */
/*                      Scheduler,Hierarchical and Queue Table Entries       */
/*                      for the given port number.                           */
/* Input(s)           : u4Port - port number for which schedule Tbl entry is */
/*                               created                                     */
/*                      i1Levels - Number of Hierarchical Levels             */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           :                                                      */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : QOS_SUCCESS or QOS_FAILURE                           */
/* Called By          : QoSAddDefTblEntries                                  */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QoSAddDefHierarchyTblEntries (UINT4 u4Port, INT1 i1Levels)
{
    INT4                i4RetStatus = 0;
    UINT4               u4SchedCount = 1;
    UINT1               u1NumSched = 0;
    UINT1               u1HLIdx = 0;
    UINT4               u4S1SchedId = 0;
    UINT4               u4S2SchedId = 0;
    UINT4               u4S1SchedHwId = 0;
    UINT4               u4S2SchedHwId = 0;
    INT1                i1NumScheds = QOS_HL_DEF_NUM_S1_SCHEDULERS;

    tQoSHierarchyNode  *pHierarchyNode = NULL;
    tQoSSchedNode      *pSchedNode = NULL;

    /* Number of SchedulerNodes and Hierarchical Nodes for each level
     * will be created in the following call*/
    for (u1HLIdx = QOS_S1_SCHEDULER; u1HLIdx <= i1Levels; u1HLIdx++)
    {
        if (QOS_S2_SCHEDULER == u1HLIdx)
        {
            i1NumScheds = QOS_HL_DEF_NUM_S2_SCHEDULERS;
        }
        else if (QOS_S3_SCHEDULER == u1HLIdx)
        {
            i1NumScheds = QOS_HL_DEF_NUM_S3_SCHEDULERS;
        }
        i4RetStatus =
            QoSCreateHLSchedulers (u4Port, i1NumScheds, &u4SchedCount, u1HLIdx);
        if (i4RetStatus == QOS_FAILURE)
        {
            QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSCreateHLSchedulers ()"
                          " Returns FAILURE.\r\n", __FUNCTION__);
            return (QOS_FAILURE);
        }
    }

    u1NumSched = (UINT1) (u4SchedCount - 1);
    u4SchedCount = 0;

    /*Attaching of Schedulers */
    /*Default HL Scheduling Schema is shown below,
     *
     * 8UC Queues -> (S3)L1 UC Scheduler \ 
     *                                    > (S2)L0 Scheduler -> (S1)PortScheduler 
     * 8MC Queues -> (S3)L1 MC Scheduler /    
     * 
     * */

    /*Attaching (Port)S1 scheduler with Port */
    if (u1NumSched != 0)
    {
        pHierarchyNode =
            QoSUtlGetHierarchyNode ((INT4) u4Port, QOS_S1_SCHEDULER,
                                    QOS_HL_DEF_S1_SCHEDULER_ID);
        if (pHierarchyNode == NULL)
        {
            QOS_TRC_ARG1 (MGMT_TRC, "In %s : NULL check failed \r\n",
                          __FUNCTION__);
            return (QOS_FAILURE);
        }
        pSchedNode =
            QoSUtlGetSchedNode (pHierarchyNode->i4IfIndex,
                                pHierarchyNode->u4SchedId);
        if (pSchedNode == NULL)
        {
            QOS_TRC_ARG1 (MGMT_TRC, "In %s : NULL check failed \r\n",
                          __FUNCTION__);
            return QOS_FAILURE;
        }

        /* Next Scheduler For PortScheduler(S1) Will be 0,
         * As it is already attached to the port
         * */

        u4S1SchedHwId = pSchedNode->u4SchedulerHwId;
        pHierarchyNode->u4SchedNext = 0;
        i4RetStatus =
            QoSHwWrSchedulerHierarchyMap (pHierarchyNode->i4IfIndex,
                                          (INT4) u4S1SchedHwId,
                                          pHierarchyNode->u2Weight,
                                          pHierarchyNode->u1Priority,
                                          (INT4) pHierarchyNode->u4SchedNext,
                                          (INT4) pHierarchyNode->u4QNext,
                                          pHierarchyNode->u1Level,
                                          QOS_HIERARCHY_ADD);
        if (i4RetStatus == QOS_FAILURE)
        {
            QOS_TRC_ARG1 (MGMT_TRC, "In %s : "
                          "QoSHwWrSchedulerHierarchyMap : Add "
                          " () Returns FAILURE. \r\n", __FUNCTION__);
            return (QOS_FAILURE);
        }
        pHierarchyNode->u1Status = ACTIVE;
        u4S1SchedId = pHierarchyNode->u4SchedId;
        u4SchedCount++;
        u1NumSched--;
    }

    /*Attaching S2 scheduler(s) with S1 Schedulers */
    if (u4S1SchedId != 0)
    {
        pHierarchyNode = NULL;
        pSchedNode = NULL;
        pHierarchyNode =
            QoSUtlGetHierarchyNode ((INT4) u4Port, QOS_S2_SCHEDULER,
                                    QOS_HL_DEF_S2_SCHEDULER_ID);
        if (pHierarchyNode == NULL)
        {
            QOS_TRC_ARG1 (MGMT_TRC, "In %s : NULL check failed \r\n",
                          __FUNCTION__);
            return QOS_FAILURE;
        }
        pSchedNode =
            QoSUtlGetSchedNode (pHierarchyNode->i4IfIndex,
                                pHierarchyNode->u4SchedId);
        if (pSchedNode == NULL)
        {
            QOS_TRC_ARG1 (MGMT_TRC, "In %s : NULL check failed \r\n",
                          __FUNCTION__);
            return QOS_FAILURE;
        }

        /* Next Scheduler For S2 Scheduler Will be S1 Scheduler,
         * So the Hardware handle of the S1 and S2 schedulers will be sent 
         * to make the attachment. (S2)L0 Scheduler -> (S1)PortScheduler
         * */
        pHierarchyNode->u4SchedNext = u4S1SchedId;
        u4S2SchedHwId = pSchedNode->u4SchedulerHwId;
        i4RetStatus =
            QoSHwWrSchedulerHierarchyMap (pHierarchyNode->i4IfIndex,
                                          (INT4) u4S2SchedHwId,
                                          pHierarchyNode->u2Weight,
                                          pHierarchyNode->u1Priority,
                                          (INT4) u4S1SchedHwId,
                                          (INT4) pHierarchyNode->u4QNext,
                                          pHierarchyNode->u1Level,
                                          QOS_HIERARCHY_ADD);
        if (i4RetStatus == QOS_FAILURE)
        {
            QOS_TRC_ARG1 (MGMT_TRC, "In %s : "
                          "QoSHwWrSchedulerHierarchyMap : Add "
                          " () Returns FAILURE. \r\n", __FUNCTION__);
            return (QOS_FAILURE);
        }
        /* Once the Shedulers are attached the Hierarchical Node Status 
         * can be made active */
        pHierarchyNode->u1Status = ACTIVE;
        u4S2SchedId = pHierarchyNode->u4SchedId;
        u4SchedCount++;
        u1NumSched--;
    }

    /*Attaching S3 scheduler(s) with S2 Schedulers */
    if (u4S2SchedId != 0)
    {
        pHierarchyNode = NULL;
        pSchedNode = NULL;
        pHierarchyNode =
            QoSUtlGetHierarchyNode ((INT4) u4Port, QOS_S3_SCHEDULER,
                                    QOS_HL_DEF_S3_SCHEDULER1_ID);
        if (pHierarchyNode == NULL)
        {
            QOS_TRC_ARG1 (MGMT_TRC, "In %s : NULL check failed \r\n",
                          __FUNCTION__);
            return QOS_FAILURE;
        }
        pSchedNode =
            QoSUtlGetSchedNode (pHierarchyNode->i4IfIndex,
                                pHierarchyNode->u4SchedId);
        if (pSchedNode == NULL)
        {
            QOS_TRC_ARG1 (MGMT_TRC, "In %s : NULL check failed \r\n",
                          __FUNCTION__);
            return QOS_FAILURE;
        }

        /* Next Scheduler For S2 Scheduler Will be S1 Scheduler,
         * So the Hardware handle of the S3 and S2 schedulers will be sent 
         * to make the attachment. (S3)L1 UC Scheduler -> (S2)L0 Scheduler
         * */
        pHierarchyNode->u4SchedNext = u4S2SchedId;
        i4RetStatus =
            QoSHwWrSchedulerHierarchyMap (pHierarchyNode->i4IfIndex,
                                          (INT4) pSchedNode->u4SchedulerHwId,
                                          pHierarchyNode->u2Weight,
                                          pHierarchyNode->u1Priority,
                                          (INT4) u4S2SchedHwId,
                                          (INT4) pHierarchyNode->u4QNext,
                                          pHierarchyNode->u1Level,
                                          QOS_HIERARCHY_ADD);
        if (i4RetStatus == QOS_FAILURE)
        {
            QOS_TRC_ARG1 (MGMT_TRC, "In %s : "
                          "QoSHwWrSchedulerHierarchyMap : Add "
                          " () Returns FAILURE. \r\n", __FUNCTION__);
            return (QOS_FAILURE);
        }

        /* Once the Shedulers are attached the Hierarchical Node Status 
         * can be made active */
        pHierarchyNode->u1Status = ACTIVE;
        u4SchedCount++;
        u1NumSched--;

        pHierarchyNode = NULL;
        pSchedNode = NULL;
        pHierarchyNode =
            QoSUtlGetHierarchyNode ((INT4) u4Port, QOS_S3_SCHEDULER,
                                    QOS_HL_DEF_S3_SCHEDULER2_ID);
        if (pHierarchyNode == NULL)
        {
            QOS_TRC_ARG1 (MGMT_TRC, "In %s : NULL check failed \r\n",
                          __FUNCTION__);
            return QOS_FAILURE;
        }
        pSchedNode =
            QoSUtlGetSchedNode (pHierarchyNode->i4IfIndex,
                                pHierarchyNode->u4SchedId);
        if (pSchedNode == NULL)
        {
            QOS_TRC_ARG1 (MGMT_TRC, "In %s : NULL check failed \r\n",
                          __FUNCTION__);
            return QOS_FAILURE;
        }
        /* Next Scheduler For S2 Scheduler Will be S1 Scheduler,
         * So the Hardware handle of the S3 and S2 schedulers will be sent 
         * to make the attachment. (S3)L1 UC Scheduler -> (S2)L0 Scheduler
         * */
        pHierarchyNode->u4SchedNext = u4S2SchedId;
        i4RetStatus =
            QoSHwWrSchedulerHierarchyMap (pHierarchyNode->i4IfIndex,
                                          (INT4) pSchedNode->u4SchedulerHwId,
                                          pHierarchyNode->u2Weight,
                                          pHierarchyNode->u1Priority,
                                          (INT4) u4S2SchedHwId,
                                          (INT4) pHierarchyNode->u4QNext,
                                          pHierarchyNode->u1Level,
                                          QOS_HIERARCHY_ADD);
        if (i4RetStatus == QOS_FAILURE)
        {
            QOS_TRC_ARG1 (MGMT_TRC, "In %s : "
                          "QoSHwWrSchedulerHierarchyMap : Add "
                          " () Returns FAILURE. \r\n", __FUNCTION__);
            return (QOS_FAILURE);
        }
        /* Once the Shedulers are attached the Hierarchical Node Status 
         * can be made active */
        pHierarchyNode->u1Status = ACTIVE;
        u4SchedCount++;
        u1NumSched--;

        while (u1NumSched != 0)
        {
            pHierarchyNode = NULL;
            pSchedNode = NULL;
            pHierarchyNode =
                QoSUtlGetHierarchyNode ((INT4) u4Port, QOS_S3_SCHEDULER,
                                        (u4SchedCount + 1));
            if (pHierarchyNode == NULL)
            {
                QOS_TRC_ARG1 (MGMT_TRC, "In %s : NULL check failed \r\n",
                              __FUNCTION__);
                return QOS_FAILURE;
            }
            pSchedNode =
                QoSUtlGetSchedNode (pHierarchyNode->i4IfIndex,
                                    pHierarchyNode->u4SchedId);
            if (pSchedNode == NULL)
            {
                QOS_TRC_ARG1 (MGMT_TRC, "In %s : NULL check failed \r\n",
                              __FUNCTION__);
                return QOS_FAILURE;
            }
            /* Next Scheduler For S2 Scheduler Will be S1 Scheduler,
             * So the Hardware handle of the S3 and S2 schedulers will be sent
             * to make the attachment. (S3)L1 UC Scheduler -> (S2)L0 Scheduler
             * */
            pHierarchyNode->u4SchedNext = u4S2SchedId;
            i4RetStatus =
                QoSHwWrSchedulerHierarchyMap (pHierarchyNode->i4IfIndex,
                                              (INT4) pSchedNode->
                                              u4SchedulerHwId,
                                              pHierarchyNode->u2Weight,
                                              pHierarchyNode->u1Priority,
                                              (INT4) u4S2SchedHwId,
                                              (INT4) pHierarchyNode->u4QNext,
                                              pHierarchyNode->u1Level,
                                              QOS_HIERARCHY_ADD);

            if (i4RetStatus == QOS_FAILURE)
            {
                QOS_TRC_ARG1 (MGMT_TRC, "In %s : "
                              "QoSHwWrSchedulerHierarchyMap : Add "
                              " () Returns FAILURE. \r\n", __FUNCTION__);
                return (QOS_FAILURE);
            }
            /* Once the Shedulers are attached the Hierarchical Node Status
             * can be made active */
            pHierarchyNode->u1Status = ACTIVE;
            u4SchedCount++;
            u1NumSched--;
        }

    }

    /*Add QueueTable  Table Entries */
    if (QoSAddDefQTblEntries (u4Port) == QOS_FAILURE)
    {
        QOS_TRC_ARG2 (MGMT_TRC,
                      "%s : adding Queue Table entries for port %d, failed\r\n",
                      __FUNCTION__, (INT4) u4Port);
        return (QOS_FAILURE);
    }

    /* Set Scheduling Algorithm to all the Schedulers in the Hierarchical Schema */
    i4RetStatus = QoSHwWrSchedulerUpdateParams ((INT4) u4Port,
                                                QOS_HL_DEF_S1_SCHEDULER_ID,
                                                QOS_SCHED_UPDATE_ALGO);
    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSHwWrSchedulerUpdateParams () "
                      "Returns FAILURE. \r\n", __FUNCTION__);
        return (QOS_FAILURE);
    }
    i4RetStatus = QoSHwWrSchedulerUpdateParams ((INT4) u4Port,
                                                QOS_HL_DEF_S2_SCHEDULER_ID,
                                                QOS_SCHED_UPDATE_ALGO);
    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSHwWrSchedulerUpdateParams () "
                      "Returns FAILURE. \r\n", __FUNCTION__);
        return (QOS_FAILURE);
    }
    i4RetStatus = QoSHwWrSchedulerUpdateParams ((INT4) u4Port,
                                                QOS_HL_DEF_S3_SCHEDULER1_ID,
                                                QOS_SCHED_UPDATE_ALGO);
    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSHwWrSchedulerUpdateParams () "
                      "Returns FAILURE. \r\n", __FUNCTION__);
        return (QOS_FAILURE);
    }

    i4RetStatus = QoSHwWrSchedulerUpdateParams ((INT4) u4Port,
                                                QOS_HL_DEF_S3_SCHEDULER2_ID,
                                                QOS_SCHED_UPDATE_ALGO);
    if (i4RetStatus == QOS_FAILURE)
    {
        QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSHwWrSchedulerUpdateParams () "
                      "Returns FAILURE. \r\n", __FUNCTION__);
    }

    return (QOS_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSCreateHLSchedulers                                */
/* Description        : This function is used to Create default Scheduler    */
/*                      and Hierarchical Nodes for Hierarchical Schedulers   */
/* Input(s)           : u4Port -Interface Index                              */
/*                      i1NumSchedulers - Number of Schedulers to be created */
/*                                        for the Level                      */
/*                      pu4SchedCount - Number of schedulers already created */
/*                                      On the Same Port+1,gives the Id for  */
/*                                      Next Scheduler to be created         */
/*                      u1HLLevel    - Hierarchical Level                    */
/* Output(s)          : pu4SchedCount                                        */
/* Global Variables                                                          */
/* Referred           : gQoSGlobalInfo                                       */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : QOS_SUCCESS or QOS_FAILURE                           */
/* Called By          : QoSAddDefHierarchyTblEntries                         */
/*****************************************************************************/
INT4
QoSCreateHLSchedulers (UINT4 u4Port, INT1 i1NumSchedulers, UINT4 *pu4SchedCount,
                       UINT1 u1HLLevel)
{
    INT1                i1SchedIdx = 0;
    INT4                i4RetStatus = 0;
    UINT4               u4SchedId = 0;
    tQoSHierarchyNode  *pHLNode = NULL;
    tQoSSchedNode      *pSchNode = NULL;

    for (i1SchedIdx = 0; i1SchedIdx < i1NumSchedulers; i1SchedIdx++)
    {
        u4SchedId = (*pu4SchedCount);
        /*Creation of Scheduler Node and Updation of it's Parameters */
        pSchNode = QoSCreateSchedTblEntry ((INT4) u4Port, u4SchedId);
        if (pSchNode == NULL)
        {
            return (QOS_FAILURE);
        }
        pSchNode->u1HierarchyLevel = u1HLLevel;

        /*Creation of Hierarchy Node and Updation of it's Parameters */
        pHLNode =
            QoSCreateHierarchyTblEntry (pSchNode->i4IfIndex, u1HLLevel,
                                        u4SchedId);
        if (pHLNode == NULL)
        {
            return (QOS_FAILURE);
        }
        /* Program the Hardware with the Scheduler Parameters */
        if (gQoSGlobalInfo.eSysStatus == QOS_SYS_STATUS_ENABLE)
        {
            i4RetStatus =
                QoSHwWrSchedulerAdd (pSchNode->i4IfIndex, pSchNode->u4Id);
            if (i4RetStatus == QOS_FAILURE)
            {
                QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSHwWrSchedulerAdd ()"
                              " Returns FAILURE.\r\n", __FUNCTION__);
                return (QOS_FAILURE);
            }
        }
        pSchNode->u1Status = ACTIVE;
        (*pu4SchedCount)++;
        pHLNode = NULL;
        pSchNode = NULL;
    }
    return (QOS_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSAddDefCpuQTblEntries                              */
/* Description        : This function is used to Create default tables for   */
/*                      CPU rate limit Table Entry.                          */
/* Input(s)           : None.                                                */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           :                                                      */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : QOS_SUCCESS or QOS_FAILURE                           */
/* Called By          : QoSAddDefTblEntries                                  */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QoSAddDefCpuQTblEntries ()
{
    tQosCpuQEntry      *pCpuQNode = NULL;
    UINT4               u4CpuQId = 0;
    UINT4               u4TmpCpuQId = 0;

    /*  Add QueueTemplate Table Entries    */
    /*------------------------------------|*/
    /*| QId | MinRate | MaxRate | Status  | */
    /*------------------------------------|*/
    /*| 1   | 1       | 65535   | Active  | */

    for (u4CpuQId = 0; u4CpuQId < QOS_QUEUE_ENTRY_MAX; u4CpuQId++)
    {
        u4TmpCpuQId = (u4CpuQId + 1);

        pCpuQNode = QoSUtlCreateCpuQTblEntry (u4TmpCpuQId);
        if (pCpuQNode == NULL)
        {
            QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlCreateCpuQTblEntry "
                          " () Returns FAILURE. \r\n", __FUNCTION__);
            return (QOS_FAILURE);
        }

        /* Update the Tbl Entry */
        pCpuQNode->u4CpuQId = u4TmpCpuQId;
        pCpuQNode->u4CpuQMinRate = QOS_DEF_CPU_MIN_RATE;
        pCpuQNode->u4CpuQMaxRate = QOS_DEF_CPU_MAX_RATE;
        pCpuQNode->u1CpuQStatus = ACTIVE;

        /* Configure the HW */
        if (QoSHwWrSetCpuRateLimit ((INT4) pCpuQNode->u4CpuQId,
                                    pCpuQNode->u4CpuQMinRate,
                                    pCpuQNode->u4CpuQMaxRate) == QOS_FAILURE)
        {
            QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSHwWrSetCpuRateLimit "
                          " () Returns FAILURE. \r\n", __FUNCTION__);
            return (QOS_FAILURE);
        }
    }                            /*End of -for */

    return (QOS_SUCCESS);
}

INT4
QoSSetDefCpuQTblEntries ()
{
    tQosCpuQEntry      *pCpuQNode = NULL;
    UINT4               u4CpuQId = 0;
    UINT4               u4TmpCpuQId = 0;

    for (u4CpuQId = 0; u4CpuQId < QOS_QUEUE_ENTRY_MAX; u4CpuQId++)
    {
        u4TmpCpuQId = (u4CpuQId + 1);
        pCpuQNode = QoSUtlGetCpuQNode (u4TmpCpuQId);
        if (pCpuQNode == NULL)
        {
            QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetCpuQNode () "
                          "Returns FAILURE. \r\n", __FUNCTION__);
            return (QOS_FAILURE);
        }

        /* Set the Tbl Entry */
        pCpuQNode->u4CpuQId = u4TmpCpuQId;
        pCpuQNode->u4CpuQMinRate = QOS_DEF_CPU_MIN_RATE;
        pCpuQNode->u4CpuQMaxRate = QOS_DEF_CPU_MAX_RATE;
        pCpuQNode->u1CpuQStatus = ACTIVE;

        /* Configure the HW */
        if (QoSHwWrSetCpuRateLimit ((INT4) pCpuQNode->u4CpuQId,
                                    pCpuQNode->u4CpuQMinRate,
                                    pCpuQNode->u4CpuQMaxRate) == QOS_FAILURE)
        {
            QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSHwWrSetCpuRateLimit "
                          " () Returns FAILURE. \r\n", __FUNCTION__);
            return (QOS_FAILURE);
        }
    }
    return (QOS_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSDelDefSchedTblEntries                             */
/* Description        : This function is used to delete default tables for   */
/*                      Scheduler Table Entry for the given port number.     */
/* Input(s)           : u4Port - port number for which schedule Tbl entry is */
/*                               deleted                                     */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           :                                                      */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : QOS_SUCCESS or QOS_FAILURE                           */
/* Called By          : QosMainProcessQMsgEvent                              */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QoSDelDefSchedTblEntries (UINT4 u4Port)
{

    tQoSSchedNode      *pSchedNode = NULL;
    INT4                i4RetStatus = QOS_FAILURE;
    tQoSSchedNode       SchedNode;

    MEMSET (&SchedNode, 0, sizeof (tQoSSchedNode));

    SchedNode.i4IfIndex = (INT4) u4Port;
    SchedNode.u4Id = 0;

    while ((pSchedNode =
            (tQoSSchedNode *) RBTreeGetNext (gQoSGlobalInfo.pRbSchedTbl,
                                             &SchedNode,
                                             QoSSchedCmpFun)) != NULL)
    {
        if (pSchedNode->i4IfIndex != SchedNode.i4IfIndex)
        {
            return (QOS_SUCCESS);
        }

        if ((pSchedNode->u1Status == ACTIVE) &&
            (gQoSGlobalInfo.eSysStatus == QOS_SYS_STATUS_ENABLE))
        {
            i4RetStatus = QoSHwWrSchedulerDelete (pSchedNode->i4IfIndex,
                                                  pSchedNode->u4Id);
            if (i4RetStatus == QOS_FAILURE)
            {
                QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSHwWrSchedulerDelete"
                              " () Returns FAILURE. \r\n", __FUNCTION__);
                return (QOS_FAILURE);
            }
        }

        /* Delete the scheduler entry */
        i4RetStatus = QoSDeleteSchedTblEntry (pSchedNode);
        if (i4RetStatus == QOS_FAILURE)
        {
            QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSDeleteSchedTblEntry ()"
                          " Returns FAILURE. \r\n", __FUNCTION__);

            return (QOS_FAILURE);
        }

    }

    return (QOS_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSDelDefQTblEntries                                 */
/* Description        : This function is used to delete default tables for   */
/*                      Queue Table Entry for the given port number.         */
/* Input(s)           : u4Port - port number for which schedule Tbl entry is */
/*                               deleted                                     */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           :                                                      */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : QOS_SUCCESS or QOS_FAILURE                           */
/* Called By          : QosMainProcessQMsgEvent                              */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QoSDelDefQTblEntries (UINT4 u4Port)
{

    INT4                i4RetStatus = QOS_FAILURE;
    tQoSQNode          *pQNode = NULL;
    tQoSQNode           TempQNode;

    MEMSET (&TempQNode, 0, sizeof (tQoSQNode));

    TempQNode.i4IfIndex = u4Port;
    TempQNode.u4Id = 0;

    while ((pQNode = (tQoSQNode *) RBTreeGetNext (gQoSGlobalInfo.pRbQTbl,
                                                  &TempQNode,
                                                  QoSQCmpFun)) != NULL)
    {
        if (pQNode->i4IfIndex != TempQNode.i4IfIndex)
        {
            return (QOS_SUCCESS);
        }

        if ((pQNode->u1Status == ACTIVE) &&
            (gQoSGlobalInfo.eSysStatus == QOS_SYS_STATUS_ENABLE))
        {
            i4RetStatus =
                QoSHwWrQueueDelete ((INT4) pQNode->i4IfIndex, pQNode->u4Id);
            if (i4RetStatus == QOS_FAILURE)
            {
                QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSHwWrQueueDelete"
                              " () Returns FAILURE. \r\n", __FUNCTION__);
                return (QOS_FAILURE);
            }
        }

        /* Delete the scheduler entry */
        i4RetStatus = QoSDeleteQTblEntry (pQNode);
        if (i4RetStatus == QOS_FAILURE)
        {
            QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSDeleteQTblEntry ()"
                          " Returns FAILURE. \r\n", __FUNCTION__);

            return (QOS_FAILURE);
        }

    }

    return (QOS_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSDelHwSchedTblEntries                              */
/* Description        : This function is used to delete  scheduler table     */
/*                      entries from the hardware for the given port number. */
/* Input(s)           : u4Port - port number for which schedule Tbl entry is */
/*                               deleted                                     */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           :                                                      */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : QOS_SUCCESS or QOS_FAILURE                           */
/* Called By          : QosMainProcessQMsgEvent                              */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QoSDelHwSchedTblEntries (UINT4 u4Port)
{

    tQoSSchedNode      *pSchedNode = NULL;
    INT4                i4RetStatus = QOS_FAILURE;
    tQoSSchedNode       SchedNode;

    SchedNode.i4IfIndex = (INT4) u4Port;
    SchedNode.u4Id = 0;

    while ((pSchedNode =
            (tQoSSchedNode *) RBTreeGetNext (gQoSGlobalInfo.pRbSchedTbl,
                                             &SchedNode,
                                             QoSSchedCmpFun)) != NULL)
    {
        if (pSchedNode->i4IfIndex != SchedNode.i4IfIndex)
        {
            return (QOS_SUCCESS);
        }

        if ((pSchedNode->u1Status == ACTIVE) &&
            (gQoSGlobalInfo.eSysStatus == QOS_SYS_STATUS_ENABLE))
        {
            i4RetStatus = QoSHwWrSchedulerDelete (pSchedNode->i4IfIndex,
                                                  pSchedNode->u4Id);
            if (i4RetStatus == QOS_FAILURE)
            {
                QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSHwWrSchedulerDelete"
                              " () Returns FAILURE. \r\n", __FUNCTION__);
                return (QOS_FAILURE);
            }
        }
        SchedNode.u4Id = pSchedNode->u4Id;
    }

    return (QOS_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSDelHwQTblEntries                                  */
/* Description        : This function is used to delete  scheduler table     */
/*                      entries from the hardware for the given port number. */
/* Input(s)           : u4Port - port number for which Queue  Tbl entry is   */
/*                               deleted                                     */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           :                                                      */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : QOS_SUCCESS or QOS_FAILURE                           */
/* Called By          : QosMainProcessQMsgEvent                              */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QoSDelHwQTblEntries (UINT4 u4Port)
{
    INT4                i4RetStatus = QOS_FAILURE;
    tQoSQNode          *pQNode = NULL;
    tQoSQNode           TempQNode;

    MEMSET (&TempQNode, 0, sizeof (tQoSQNode));

    TempQNode.i4IfIndex = u4Port;
    TempQNode.u4Id = 0;

    while ((pQNode = (tQoSQNode *) RBTreeGetNext (gQoSGlobalInfo.pRbQTbl,
                                                  &TempQNode,
                                                  QoSQCmpFun)) != NULL)
    {
        if (pQNode->i4IfIndex != TempQNode.i4IfIndex)
        {
            return (QOS_SUCCESS);
        }

        if ((pQNode->u1Status == ACTIVE) &&
            (gQoSGlobalInfo.eSysStatus == QOS_SYS_STATUS_ENABLE))
        {
            i4RetStatus = QoSHwWrQueueDelete (pQNode->i4IfIndex, pQNode->u4Id);
            if (i4RetStatus == QOS_FAILURE)
            {
                QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSHwWrQueueDelete"
                              " () Returns FAILURE. \r\n", __FUNCTION__);
                return (QOS_FAILURE);
            }
        }
        TempQNode.u4Id = pQNode->u4Id;

    }

    return (QOS_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSAddHwSchedTblEntries                              */
/* Description        : This function is used to add  scheduler table        */
/*                      entries to the hardware for the given port number.   */
/* Input(s)           : u4Port - port number for which schedule Tbl entry is */
/*                               added in the hardware                       */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           :                                                      */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : QOS_SUCCESS or QOS_FAILURE                           */
/* Called By          : QosMainProcessQMsgEvent                              */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QoSAddHwSchedTblEntries (UINT4 u4Port)
{

    tQoSSchedNode      *pSchedNode = NULL;
    INT4                i4RetStatus = QOS_FAILURE;
    tQoSSchedNode       SchedNode;

    SchedNode.i4IfIndex = (INT4) u4Port;
    SchedNode.u4Id = 0;

    while ((pSchedNode =
            (tQoSSchedNode *) RBTreeGetNext (gQoSGlobalInfo.pRbSchedTbl,
                                             &SchedNode,
                                             QoSSchedCmpFun)) != NULL)
    {
        if (pSchedNode->i4IfIndex != SchedNode.i4IfIndex)
        {
            return (QOS_SUCCESS);
        }

        if ((pSchedNode->u1Status == ACTIVE) &&
            (gQoSGlobalInfo.eSysStatus == QOS_SYS_STATUS_ENABLE))
        {
            i4RetStatus = QoSHwWrSchedulerAdd (pSchedNode->i4IfIndex,
                                               pSchedNode->u4Id);
            if (i4RetStatus == QOS_FAILURE)
            {
                QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSHwWrSchedulerAdd"
                              " () Returns FAILURE. \r\n", __FUNCTION__);
                return (QOS_FAILURE);
            }
        }
        SchedNode.u4Id = pSchedNode->u4Id;
    }

    return (QOS_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSAddHwQTblEntries                                  */
/* Description        : This function is used to add  Queue table            */
/*                      entries to the hardware for the given port number.   */
/* Input(s)           : u4Port - port number for which Queue    Tbl entry is */
/*                               added in the hardware                       */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           :                                                      */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : QOS_SUCCESS or QOS_FAILURE                           */
/* Called By          : QosMainProcessQMsgEvent                              */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QoSAddHwQTblEntries (UINT4 u4Port)
{
    INT4                i4RetStatus = QOS_FAILURE;
    tQoSQNode          *pQNode = NULL;
    tQoSQNode           TempQNode;

    MEMSET (&TempQNode, 0, sizeof (tQoSQNode));

    TempQNode.i4IfIndex = u4Port;
    TempQNode.u4Id = 0;

    while ((pQNode = (tQoSQNode *) RBTreeGetNext (gQoSGlobalInfo.pRbQTbl,
                                                  &TempQNode,
                                                  QoSQCmpFun)) != NULL)
    {
        if (pQNode->i4IfIndex != TempQNode.i4IfIndex)
        {
            return (QOS_SUCCESS);
        }

        if ((pQNode->u1Status == ACTIVE) &&
            (gQoSGlobalInfo.eSysStatus == QOS_SYS_STATUS_ENABLE))
        {
            i4RetStatus = QoSHwWrQueueCreate (pQNode->i4IfIndex, pQNode->u4Id);
            if (i4RetStatus == QOS_FAILURE)
            {
                QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSHwWrQueueCreate"
                              " () Returns FAILURE. \r\n", __FUNCTION__);
                return (QOS_FAILURE);
            }
        }
        TempQNode.u4Id = pQNode->u4Id;

    }

    return (QOS_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSAddDefQTblEntries                               */
/* Description        : This function is used to Create default tables for   */
/*                      Queue Table Entry for the given port                 */
/* Input(s)           : u4Port - port number for which schedule Tbl entry is */
/*                               created                                     */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           :                                                      */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : QOS_SUCCESS or QOS_FAILURE                           */
/* Called By          : QosMainProcessQMsgEvent                              */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QoSAddDefQTblEntries (UINT4 u4Port)
{

    tQoSQNode          *pQTbleNode = NULL;
    tQoSSchedNode      *pSchedNode = NULL;
    tQoSQTemplateNode  *pQTempNode = NULL;
    INT4                i4RetStatus = QOS_FAILURE;
    UINT1               u1IPVal = 0;
    UINT4               u4QTbleId = 0;
    UINT4               u4SchedId = 1;
    UINT4               u4CfgTempId = 1;

    /* If the port is part of port channel Skip the entry    */
    /* The Que creation will not be applicable for the */
    /* port part of port-channel.                            */
    if (QosL2IwfIsPortInPortChannel (u4Port) == L2IWF_SUCCESS)
    {
        return QOS_SUCCESS;
    }

    for (u1IPVal = 0; u1IPVal < QOS_QUEUE_ENTRY_MAX; u1IPVal++)
    {
        u4QTbleId = u1IPVal + 1;
        /* Refcount for QueueTemplate */

        pQTempNode = QoSUtlGetQTempNode (u4CfgTempId);
        if (pQTempNode == NULL)
        {
            QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetQTempNode () "
                          "Returns FAILURE. \r\n", __FUNCTION__);
            return (QOS_FAILURE);
        }
        /* Incremnet the RefCount of QTempId  Entry in the QTempTable */
        pQTempNode->u4RefCount = (pQTempNode->u4RefCount) + 1;
        pQTbleNode = QoSCreateQTblEntry (u4Port, u4QTbleId);
        if (pQTbleNode == NULL)
        {
            return (QOS_FAILURE);
        }

        /* Update the Entry  and dependent Tbls */
        pQTbleNode->u4Id = u4QTbleId;
        pQTbleNode->i4IfIndex = u4Port;
        pQTbleNode->u4CfgTemplateId = u4CfgTempId;
        pQTbleNode->u4SchedulerId = u4SchedId;
        pQTbleNode->u4ShapeId = 0;
        pQTbleNode->u2Weight = QOS_Q_TABLE_WEIGHT_DEFAULT;
        pQTbleNode->u1Priority = u1IPVal;
        pQTbleNode->u1Status = ACTIVE;

        /* In Hierarchical Scheduling Unicast Queues will be attached to UC Scheduler
         * And Multicast Queues will be attached to MC Scheduler */

        if (gu1HLSupportFlag == QOS_HL_DEF_SCHED_CFG_SUPPORTED)
        {
            if (pQTbleNode->u4Id <= QOS_QUEUE_MAX_NUM_UCOSQ)
            {
                pQTbleNode->u4SchedulerId = QOS_HL_DEF_S3_SCHEDULER1_ID;
            }
            else if (pQTbleNode->u4Id <= (QOS_QUEUE_MAX_NUM_UCOSQ +
                                          QOS_QUEUE_MAX_NUM_MCOSQ))

            {
                pQTbleNode->u4SchedulerId = QOS_HL_DEF_S3_SCHEDULER2_ID;
            }
            else
            {
                return (QOS_FAILURE);
            }
        }

        /* Refcount for SchedulerNode */
        pSchedNode = QoSUtlGetSchedNode (u4Port, u4SchedId);
        if (pSchedNode == NULL)
        {
            QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetSchedNode () "
                          "Returns FAILURE. \r\n", __FUNCTION__);
            return (QOS_FAILURE);
            /* Incremnet the RefCount of SchedId  Entry in the Sched Table */
        }
        pSchedNode->u4RefCount = (pSchedNode->u4RefCount) + 1;

        /* Configure the HW  if Needed */
        /* 1. Add the entry into the HARDWARE. */
        if (gQoSGlobalInfo.eSysStatus == QOS_SYS_STATUS_ENABLE)
        {
            i4RetStatus = QoSHwWrQueueCreate (pQTbleNode->i4IfIndex,
                                              pQTbleNode->u4Id);
            if (i4RetStatus == QOS_FAILURE)
            {
                QOS_TRC_ARG1 (MGMT_TRC, "In %s : "
                              "QoSHwWrQueueCreate () Returns FAILURE.\r\n",
                              __FUNCTION__);
                return (QOS_FAILURE);
            }
        }

    }                            /*End of -for */

    return (QOS_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSAddDefQMapTblEntries                              */
/* Description        : This function is used to Create default tables for   */
/*                      QueueMap Table Entry.                                */
/* Input(s)           : i4IfIdx - IfIndex .                                  */
/*                    : u1Flag  - Flag indicating if the default entry       */
/*                                has to be created in SoftwareAndHardware   */
/*                                or ONLY in hardware.                       */
/*                                (At QOS module disable, default Q map      */
/*                                 entry needs to be created in hardware     */
/*                                 even if software has non-default          */
/*                                 Qmap entries.                             */
/*                              - QOS_QMAP_SW_HW / QOS_QMAP_HW_ONLY          */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           :                                                      */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : QOS_SUCCESS or QOS_FAILURE                           */
/* Called By          : QoSAddDefTblEntries                                  */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QoSAddDefQMapTblEntries (INT4 i4IfIdx, UINT1 u1Flag)
{

    tQoSQMapNode       *pQMapNode = NULL;
    tQoSQMapNode        LocalQMapNode;
    INT4                i4RetStatus = QOS_FAILURE;
    UINT1               u1IPVal = 0;
    UINT4               u4CLASS = 0;
    UINT1               u1RegenPri = 0;
    UINT1               u1QMax = QOS_QUEUE_MAX_NUM_UCOSQ - 1;
    UINT1               au1QId[DEFAULT_MAX_ARRAYSIZE_PRI]
        [DEFAULT_MAX_ARRAYSIZE_AVIALABLEQUEUE] = { {1, 1, 1, 1, 1, 1, 1, 1},
    {1, 1, 1, 1, 2, 2, 2, 2},
    {1, 1, 2, 2, 3, 3, 3, 3},
    {1, 1, 2, 2, 3, 4, 4, 4},
    {1, 2, 2, 3, 4, 5, 5, 5},
    {1, 2, 3, 3, 4, 5, 6, 6},
    {1, 2, 3, 4, 5, 6, 7, 7},
    {1, 2, 3, 4, 6, 6, 7, 8}
    };

    /* If the port is part of port channel Skip the entry    */
    /* The QueMap creation will not be applicable for the */
    /* port part of port-channel.                            */
    if (L2IwfIsPortInPortChannel ((UINT4) i4IfIdx) == L2IWF_SUCCESS)
    {
        return (QOS_SUCCESS);
    }

    MEMSET (&(LocalQMapNode), 0, (sizeof (tQoSQMapNode)));

    /* QuemeMapping with regenprioty */
    /*----------------------------------------
      |      |         No avialbale queue       |
      |------|----------------------------------|
      |      |  1   2   3   4   5   6   7   8   |
      |------|----------------------------------|
      |      |                                  |
      |  r  0|  Q1 Q1  Q1   Q1  Q1  Q1  Q1  Q1  |
      |  e   |                                  |
      |  g  1|  Q1 Q1  Q1   Q1  Q2  Q2  Q2  Q2  |
      |  e   |                                  |
      |  n  2|  Q1 Q1  Q1   Q2  Q3  Q3  Q3  Q3  |
      |  p   |                                  |
      |  r  3|  Q1 Q1  Q2   Q2  Q3  Q4  Q4  Q4  |
      |  i   |                                  |
      |  o  4|  Q1 Q2  Q2   Q3  Q4  Q5  Q5  Q5  |
      |  r   |                                  |
      |  i  5|  Q1 Q2  Q2   Q3  Q4  Q5  Q6  Q6  |
      |  t   |                                  |
      |  y  6|  Q1 Q2  Q3   Q4  Q5  Q6  Q7  Q7  |
      |      |                                  |
      |     7|  Q1 Q2  Q3   Q4  Q5  Q6  Q7  Q8  |
      -----------------------------------------*/

    for (u1IPVal = 0; u1IPVal <= QOS_PM_TBL_DEF_ENTRY_MAX; u1IPVal++)
    {
        if (u1Flag == QOS_QMAP_SW_HW)
        {
            /* Create the entry in the Software as well as in Hardware */
            pQMapNode = QoSCreateQMapTblEntry
                (i4IfIdx, u4CLASS, QOS_QMAP_PRI_TYPE_VLAN_PRI, u1IPVal);
            if (pQMapNode == NULL)
            {
                return (QOS_FAILURE);
            }
        }
        else
        {
            /* Create the entry only in the Hardware */

            MEMSET (&(LocalQMapNode), 0, (sizeof (tQoSQMapNode)));
            pQMapNode = &LocalQMapNode;

        }

        u1RegenPri = u1IPVal;

        /* Update the Entry  and dependent Tbls */
        pQMapNode->i4IfIndex = i4IfIdx;
        pQMapNode->u4CLASS = u4CLASS;
        pQMapNode->u1RegenPriType = QOS_QMAP_PRI_TYPE_VLAN_PRI;
        pQMapNode->u1RegenPri = u1RegenPri;
        pQMapNode->u4QId = au1QId[u1RegenPri][u1QMax];
        pQMapNode->u1Status = ACTIVE;

        /* Configure the HW  if Needed */
        /* 1. Add the entry into the HARDWARE. */
        if (gQoSGlobalInfo.eSysStatus == QOS_SYS_STATUS_ENABLE)
        {
            i4RetStatus = QoSHwWrMapClassToQueue (pQMapNode->i4IfIndex,
                                                  pQMapNode->u4CLASS,
                                                  pQMapNode->u1RegenPriType,
                                                  pQMapNode->u1RegenPri,
                                                  pQMapNode->u4QId,
                                                  QOS_QMAP_ADD);
            if (i4RetStatus == QOS_FAILURE)
            {
                QOS_TRC_ARG1 (MGMT_TRC, "In %s : "
                              "QoSHwWrMapClassToQueue() : Add "
                              "Returns FAILURE. \r\n", __FUNCTION__);
                return (QOS_FAILURE);

            }
        }

    }                            /*End of -for */

    return (QOS_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSAddDefTblEntries                                  */
/* Description        : This function is used to Create default tables for   */
/*                      QOS Tables.                                          */
/* Input(s)           : None.                                                */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           :                                                      */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : QOS_SUCCESS or QOS_FAILURE                           */
/* Called By          : QoSInit                                              */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QoSAddDefTblEntries ()
{
    INT4                i4RetStatus = QOS_FAILURE;
    INT4                i4IfIndex = 0;
    INT4                i4NextIfIndex = 0;
    INT4                i4DefIndex = 0;
    INT4                i4Direction = 0;
    UINT1               u1QTblCreated = OSIX_FALSE;
    /*Add PriorityMap Table Entries */

    i4RetStatus = QoSAddDefPriorityMapTblEntries ();
    if (i4RetStatus == QOS_FAILURE)
    {
        return (QOS_FAILURE);
    }
    /* Add default class */
    if (QoSCreateClsInfoTblEntry (QOS_CM_TBL_DEF_CLASS) == NULL)
    {
        return (QOS_FAILURE);
    }

    /*Add ClassMap Table Entries */

    i4RetStatus = QoSAddDefClassMapTblEntries ();
    if (i4RetStatus == QOS_FAILURE)
    {
        return (QOS_FAILURE);
    }

    /*Add PolicyMap Table Entries */
    i4RetStatus = QoSAddDefPolicyMapTblEntries ();
    if (i4RetStatus == QOS_FAILURE)
    {
        return (QOS_FAILURE);
    }
    i4RetStatus = QoSAddDefQTempTblEntries ();
    if (i4RetStatus == QOS_FAILURE)
    {
        return (QOS_FAILURE);
    }
    i4IfIndex = 0;
    while (QosGetNextValidPort ((UINT4) i4IfIndex, (UINT4 *) &i4NextIfIndex) ==
           QOS_SUCCESS)
    {
        if (QOS_HL_DEF_SCHED_CFG_SUPPORTED == gu1HLSupportFlag)
        {
            /* If the Hierarchical Scheduling is supported by default in the Target, then 
             * the QoSAddDefHierarchyTblEntries will be called, which will create/Attach
             * the Hierachical Scheduler Nodes and Queues as shown below,
             * 8UC Queues -> (S3)L1 UC Scheduler \ 
             *                                    > (S2)L0 Scheduler -> (S1)PortScheduler 
             * 8MC Queues -> (S3)L1 MC Scheduler /    
             * */
            if (QoSAddDefHierarchyTblEntries
                (i4NextIfIndex, QOS_HL_DEF_NUM_LEVELS) == QOS_FAILURE)
            {
                QOS_TRC_ARG2 (MGMT_TRC,
                              "%s : adding HL entries for port %d, failed\r\n",
                              __FUNCTION__, i4NextIfIndex);
                return (QOS_FAILURE);
            }
        }
        else
        {
            if (QoSAddDefSchedTblEntries (i4NextIfIndex) == QOS_FAILURE)
            {
                QOS_TRC_ARG2 (MGMT_TRC,
                              "%s : adding scheduler entries for port %d, failed\r\n",
                              __FUNCTION__, i4NextIfIndex);
                return (QOS_FAILURE);
            }

            /*Add QueueTable  Table Entries */
            if (QoSAddDefQTblEntries (i4NextIfIndex) == QOS_FAILURE)
            {
                QOS_TRC_ARG2 (MGMT_TRC,
                              "%s : adding Queue Table entries for port %d, failed\r\n",
                              __FUNCTION__, i4NextIfIndex);
                return (QOS_FAILURE);
            }
        }
        for (i4Direction = QOS_DATA_IN; i4Direction <= QOS_DATA_OUT;
             i4Direction++)
        {
            if (QoSAddDefDataPathTblEntries (i4NextIfIndex, i4Direction)
                == QOS_FAILURE)
            {
                return (QOS_FAILURE);
            }
        }
        /* Population of default entries for all Packet types for 8P0D Model */
        i4IfIndex = i4NextIfIndex;
        u1QTblCreated = OSIX_TRUE;
    }
    /*Add QueueMapTable  Table Entries */
    if (u1QTblCreated == OSIX_TRUE)
    {
        i4RetStatus = QoSAddDefQMapTblEntries (i4DefIndex, QOS_QMAP_SW_HW);
        if (i4RetStatus == QOS_FAILURE)
        {
            return (QOS_FAILURE);
        }
    }
    return (QOS_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSDelQMapTblEntries                              */
/* Description        : This function is used to Create default tables for   */
/*                      QueueMap Table Entry.                                */
/* Input(s)           : None.                                                */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           :                                                      */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : QOS_SUCCESS or QOS_FAILURE                           */
/* Called By          : QoSAddDefTblEntries                                  */
/* Calling Function   :                                                      */
/*****************************************************************************/

INT4
QoSDelQMapTblEntries (UINT4 u4Port)
{

    INT4                i4IfIndex = 0;
    INT4                i4RetStatus = QOS_FAILURE;
    tQoSQMapNode       *pQMapNode = NULL;
    tQoSQMapNode        QMapNode;
    QMapNode.i4IfIndex = i4IfIndex;
    QMapNode.u4CLASS = 0;
    QMapNode.u1RegenPriType = 0;
    QMapNode.u1RegenPri = 0;

    while ((pQMapNode =
            (tQoSQMapNode *) RBTreeGetNext (gQoSGlobalInfo.pRbQMapTbl,
                                            &QMapNode, QoSQMapCmpFun)) != NULL)
    {
        if (pQMapNode->i4IfIndex != u4Port)
        {
            QMapNode.i4IfIndex = pQMapNode->i4IfIndex;
            QMapNode.u4CLASS = pQMapNode->u4CLASS;
            QMapNode.u1RegenPriType = pQMapNode->u1RegenPriType;
            QMapNode.u1RegenPri = pQMapNode->u1RegenPri;
            continue;
        }
        if ((pQMapNode->u1Status == ACTIVE) &&
            (gQoSGlobalInfo.eSysStatus == QOS_SYS_STATUS_ENABLE))
        {
            i4RetStatus = QoSHwWrMapClassToQueue ((INT4) pQMapNode->i4IfIndex,
                                                  pQMapNode->u4CLASS,
                                                  pQMapNode->u1RegenPriType,
                                                  pQMapNode->u1RegenPri,
                                                  pQMapNode->u4QId,
                                                  QOS_QMAP_DEL);
            if (i4RetStatus == QOS_FAILURE)
            {
                QOS_TRC_ARG1 (MGMT_TRC,
                              "In %s : " "QoSHwWrMapClassToQueue() : Del "
                              "Returns FAILURE. \r\n", __FUNCTION__);
                return (QOS_FAILURE);

            }
        }

        /* 2. Remove the entry from the SOFTWARE. */

        i4RetStatus = QoSDeleteQMapTblEntry (pQMapNode);
        if (i4RetStatus == QOS_FAILURE)
        {
            QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSDeleteQMapTblEntry ()"
                          " Returns FAILURE. \r\n", __FUNCTION__);
            return (QOS_FAILURE);
        }
        QMapNode.i4IfIndex = pQMapNode->i4IfIndex;
        QMapNode.u4CLASS = pQMapNode->u4CLASS;
        QMapNode.u1RegenPriType = pQMapNode->u1RegenPriType;
        QMapNode.u1RegenPri = pQMapNode->u1RegenPri;

    }
    return (QOS_SUCCESS);

}

/*****************************************************************************/
/* Function Name      : QoSRegenerateVlanPriority                            */
/* Description        : This function is Called From VlanModule.It will the  */
/*                      u1RegenPri Value to Vlan Module                      */
/* Input(s)           : u4IfIndex,u2VlanId,u1InPriority.                     */
/* Output(s)          : u1RegenPri.                                          */
/* Global Variables                                                          */
/* Referred           :                                                      */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : u1RegenPri.                                          */
/* Called By          : Vlan Module                                          */
/* Calling Function   :                                                      */
/*****************************************************************************/
UINT1
QoSRegenerateVlanPriority (UINT4 u4IfIndex, UINT2 u2VlanId, UINT1 u1InPriority)
{
    tQoSInPriorityMapNode *pPriMapNode = NULL;
    UINT1               u1RegenPri = 0;
    UINT1               u1InPriType = 0;
    UINT4               u4TempIfIndex = 0;

    u4TempIfIndex = u4IfIndex;

    /*For the given the ifindex,vlanid,inpriority-Find the corresponding
       RegenPri using the function QoSUtlGetPriorityMapUniNode */

    /*Append InPriType to VlanPri */

    u1InPriType = QOS_IN_PRI_TYPE_VLAN_PRI;

    pPriMapNode =
        QoSUtlGetPriorityMapUniNode (u4IfIndex,
                                     u2VlanId, u1InPriority, u1InPriType);
    if (pPriMapNode != NULL)
    {
        u1RegenPri = pPriMapNode->u1RegenPriority;
        return u1RegenPri;
    }

    /*Since there is No Prioritymapnode with given u4IfIndex,u2VlanId,
     * u1InPriority.Give the Default Value*/
    else
    {

        /*To give more specific Value For the given u4IfIndex,u2VlanId,
         * u1InPriority.Make the u4IfIndex=0 and check whether the 
         * corresponding  PriorityMap Node is Available*/

        u4IfIndex = 0;
        pPriMapNode =
            QoSUtlGetPriorityMapUniNode (u4IfIndex,
                                         u2VlanId, u1InPriority, u1InPriType);

        if (pPriMapNode != NULL)
        {
            u1RegenPri = pPriMapNode->u1RegenPriority;
            return u1RegenPri;
        }

        /*To give Next specific Value For the given u4IfIndex,u2VlanId,
         * u1InPriority.Make the  u2VlanId=0 and check whether the 
         * corresponding  PriorityMap Node is Available*/

        else
        {
            u2VlanId = 0;
            u4IfIndex = u4TempIfIndex;
            pPriMapNode =
                QoSUtlGetPriorityMapUniNode (u4IfIndex,
                                             u2VlanId,
                                             u1InPriority, u1InPriType);

            if (pPriMapNode != NULL)
            {
                u1RegenPri = pPriMapNode->u1RegenPriority;
                return u1RegenPri;
            }
        }
    }
/* If No Match is present, give the Default Value with u4IfIndex=0
   u2VlanId=0*/

    u4IfIndex = 0;
    u2VlanId = 0;
    pPriMapNode =
        QoSUtlGetPriorityMapUniNode (u4IfIndex,
                                     u2VlanId, u1InPriority, u1InPriType);

    if (pPriMapNode != NULL)
    {
        return (pPriMapNode->u1RegenPriority);
    }
    else
    {
        return u1RegenPri;
    }

}

/******************************************************************************
 * Function           : QOSSendEventToQOSTask
 * Input(s)           : u4Event - Event
 * Output(s)          : None
 * Returns            : OSIX_SUCCESS/OSIX_FAILURE
 * Action             : MSR modules should call this routine to
 *                      post any event to QOS task.
 ******************************************************************************/

UINT4
QOSSendEventToQOSTask (UINT4 u4Event)
{
    if (QOS_TASK_ID == 0)
    {
        if (OsixGetTaskId (SELF, (const UINT1 *) QOS_TASK_NAME,
                           &QOS_TASK_ID) != OSIX_SUCCESS)
        {
            return OSIX_FAILURE;
        }
    }

    if (QOS_SEND_EVENT (QOS_TASK_ID, u4Event) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

VOID
QosMainProcessQMsgEvent (VOID)
{
    tQosQueueMsg       *pQosMsg = NULL;
    INT4                i4RetStatus = QOS_SUCCESS;
    INT4                i4Direction = QOS_DATA_IN;
#ifdef MBSM_WANTED
    tMbsmProtoAckMsg    MbsmProtoAckMsg;
    MEMSET (&MbsmProtoAckMsg, 0, sizeof (tMbsmProtoAckMsg));
#endif

    while (OsixQueRecv (QOS_MSG_QUEUE_ID, (UINT1 *) &pQosMsg,
                        OSIX_DEF_MSG_LEN, 0) == OSIX_SUCCESS)
    {
        switch (pQosMsg->u2MsgType)
        {
            case QOS_CREATE_PORT_MSG:
                /*Add Scheduler  Table Entries */
                if (QOS_HL_DEF_SCHED_CFG_SUPPORTED == gu1HLSupportFlag)
                {
                    break;
                }
                if (gQoSGlobalInfo.eSysControl != QOS_SYS_CNTL_START)
                {
                    QOS_TRC (MGMT_TRC, QOS_MODULE_SHUTDOW_ERROR_MSG);
                    break;
                }
                i4RetStatus = QoSAddDefSchedTblEntries (pQosMsg->u4Port);
                if (i4RetStatus == QOS_FAILURE)
                {
                    QOS_TRC_ARG2 (MGMT_TRC,
                                  "%s : adding scheduler entries for port %d, failed\r\n",
                                  __FUNCTION__, pQosMsg->u4Port);
                    break;
                }

                /*Add QueueTable  Table Entries */
                i4RetStatus = QoSAddDefQTblEntries (pQosMsg->u4Port);
                if (i4RetStatus == QOS_FAILURE)
                {
                    QOS_TRC_ARG2 (MGMT_TRC,
                                  "%s : adding Queue Table entries for port %d, failed\r\n",
                                  __FUNCTION__, pQosMsg->u4Port);
                    break;
                }
                for (i4Direction = QOS_DATA_IN; i4Direction <= QOS_DATA_OUT;
                     i4Direction++)
                {
                    if (QoSAddDefDataPathTblEntries
                        ((INT4) (pQosMsg->u4Port), i4Direction) == QOS_FAILURE)
                    {
                        break;
                    }
                }

                /* Add QueueMap entries */
                i4RetStatus =
                    QoSAddDefQMapTblEntries ((INT4) pQosMsg->u4Port,
                                             QOS_QMAP_SW_HW);
                if (i4RetStatus == QOS_FAILURE)
                {
                    QOS_TRC_ARG2 (MGMT_TRC,
                                  "%s : adding Queue map entries for port %d, failed\r\n",
                                  __FUNCTION__, pQosMsg->u4Port);

                }
                break;

            case QOS_DELETE_PORT_MSG:
                if (QOS_HL_DEF_SCHED_CFG_SUPPORTED == gu1HLSupportFlag)
                {
                    break;
                }
                if (gQoSGlobalInfo.eSysControl != QOS_SYS_CNTL_START)
                {
                    QOS_TRC (MGMT_TRC, QOS_MODULE_SHUTDOW_ERROR_MSG);
                    break;
                }
                i4RetStatus = QoSDelDefQTblEntries (pQosMsg->u4Port);
                if (i4RetStatus == QOS_FAILURE)
                {
                    QOS_TRC_ARG2 (MGMT_TRC,
                                  "%s : deleting Queue Table entries for port %d, failed\r\n",
                                  __FUNCTION__, pQosMsg->u4Port);
                    break;
                }

                i4RetStatus = QoSDelDefSchedTblEntries (pQosMsg->u4Port);
                if (i4RetStatus == QOS_FAILURE)
                {
                    QOS_TRC_ARG2 (MGMT_TRC,
                                  "%s : deleting scheduler entries for port %d, failed\r\n",
                                  __FUNCTION__, pQosMsg->u4Port);
                    break;
                }
                for (i4Direction = QOS_DATA_IN; i4Direction <= QOS_DATA_OUT;
                     i4Direction++)
                {
                    if (QoSDelDefDataPathTblEntries
                        ((INT4) pQosMsg->u4Port, i4Direction) == QOS_FAILURE)
                    {
                        break;
                    }
                }
                i4RetStatus = QoSDelQMapTblEntries (pQosMsg->u4Port);
                if (i4RetStatus == QOS_FAILURE)
                {
                    QOS_TRC_ARG2 (MGMT_TRC,
                                  "%s : deleting queue map entries for port %d, failed\r\n",
                                  __FUNCTION__, pQosMsg->u4Port);

                }
                break;

            case QOS_ENABLE_PORT_MSG:
                /* When the port is enabled/disabled, It is not required to add/remove the 
                 * scheduler/Queue entries. 
                 * */
                break;
            case QOS_DISABLE_PORT_MSG:
                /* When the port is enabled/disabled, It is not required to add/remove the 
                 * scheduler/Queue entries. 
                 * */
                break;
#ifdef MBSM_WANTED
            case MBSM_MSG_CARD_INSERT:
                QosMbsmProgramHw (pQosMsg->pMbsmProtoMsg);
                if (pQosMsg->pMbsmProtoMsg != NULL)
                {
                    MEM_FREE (pQosMsg->pMbsmProtoMsg);
                }
                break;
            case MBSM_MSG_CARD_REMOVE:
                MbsmProtoAckMsg.i4ProtoCookie =
                    pQosMsg->pMbsmProtoMsg->i4ProtoCookie;
                MbsmProtoAckMsg.i4SlotId =
                    pQosMsg->pMbsmProtoMsg->MbsmSlotInfo.i4SlotId;
                MbsmProtoAckMsg.i4RetStatus = MBSM_SUCCESS;
                MbsmSendAckFromProto (&MbsmProtoAckMsg);
                if (pQosMsg->pMbsmProtoMsg != NULL)
                {
                    MEM_FREE (pQosMsg->pMbsmProtoMsg);
                }
                break;
            case MBSM_MSG_SELF_CARD_INSERT:
                i4RetStatus = QoSInitWithMBSMandRM ();
                if (i4RetStatus == QOS_FAILURE)
                {
                    QOS_TRC_ARG1 (OS_RESOURCE_TRC,
                                  "%s : ERROR: QoSHwWrInit () Failed!. " "\r\n",
                                  __FUNCTION__);
                }
                if (pQosMsg->pMbsmProtoMsg != NULL)
                {
                    MEM_FREE (pQosMsg->pMbsmProtoMsg);
                }
                break;
#endif
            case QOS_RM_MSG:
                QosRedHandleRmEvents (pQosMsg);
                break;

            default:
                break;
        }
        QOS_RELEASE_QMSG_MEM_BLOCK (pQosMsg);
    }
    return;
}

/******************************************************************************
 * Function           : QoSInitWithMBSMandRM
 * Input(s)           : None
 * Output(s)          : None
 * Returns            : QOS_SUCCESS/QOS_FAILURE
 * Action             : This function will Program the QoSHwInit and Default Table
 *                      Entries to the Hardware only if MBSM SelfAttach is over and
 *                      the RM mode is ACTIVE.
 *******************************************************************************/
UINT4
QoSInitWithMBSMandRM (VOID)
{
    INT4                i4RetStatus = 0;
#ifdef MBSM_WANTED
    if (MbsmGetSelfAttachStatus () == MBSM_SUCCESS)
#endif
    {
#ifdef RM_WANTED
        if (gQoSGlobalInfo.RedGlobalInfo.u1NodeStatus == RM_ACTIVE)
#endif
        {
            i4RetStatus = QoSHwWrInit ();
            if (i4RetStatus == QOS_FAILURE)
            {
                QOS_TRC_ARG1 (OS_RESOURCE_TRC,
                              "%s : ERROR: QoSHwWrInit () Failed!. " "\r\n",
                              __FUNCTION__);
                return QOS_FAILURE;
            }
            /* Default entries will be added only if the MSR database is not
             * restored. Or else default entries will be restored from the
             * MSR database. 
             * In auto-save case, forcefully create def-table entries, because
             * defaults wont be saved in auto save case and init should take care */
            if ((IssGetRestoreOptionFromNvRam () != ISS_CONFIG_RESTORE) ||
                (IssGetAutoSaveStatus () == ISS_TRUE))
            {
                /* Add Default Table Entries */
                i4RetStatus = QoSAddDefTblEntries ();
                if (i4RetStatus != QOS_SUCCESS)
                {
                    QOS_TRC_ARG1 (MGMT_TRC,
                                  "%s : QoSAddDefTblEntries () Failed!.\r\n",
                                  __FUNCTION__);
                    return QOS_FAILURE;
                }
            }
        }
    }
    return QOS_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : QoSConfigVlanMap                                     */
/* Description        : This function is used to Add or Delete the VlanMap   */
/*                      Table ACTIVE Entries in the HARDWARE                 */
/*                      QOS_CFG_TYPE_CREATE - Add the Entries in HARDWARE    */
/*                      QOS_CFG_TYPE_DELETE - Del the Entries from HARDWARE  */
/* Input(s)           : u4CfgType - QOS_CFG_TYPE_CREATE / QOS_CFG_TYPE_DELETE*/
/* Output(s)          : None.                                                */
/* Return Value(s)    : QOS_SUCCESS or QOS_FAILURE                           */
/* Called By          : QoSShutdown                                          */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QoSConfigVlanMap (UINT4 u4CfgType)
{
    INT4                i4RetStatus = QOS_FAILURE;

    /* This function will iterate through all active QMap entries
     * and removes hardware programming for which VlanMap is
     * associated */
    if (u4CfgType == QOS_CFG_TYPE_CREATE)
    {
        i4RetStatus = QoSConfigVlanMapTables (QOS_CFG_TYPE_CREATE);
        if (i4RetStatus != QOS_SUCCESS)
        {
            QOS_TRC_ARG1 (MGMT_TRC, "In %s : "
                          "QoSConfigVlanMapTables () Returns"
                          " FAILURE. \r\n", __FUNCTION__);
            return (QOS_FAILURE);
        }
    }
    else
    {
        i4RetStatus = QoSConfigVlanMapTables (QOS_CFG_TYPE_DELETE);
        if (i4RetStatus != QOS_SUCCESS)
        {
            QOS_TRC_ARG1 (MGMT_TRC, "In %s : "
                          "QoSConfigVlanMapTables () Returns"
                          " FAILURE. \r\n", __FUNCTION__);
            return (QOS_FAILURE);
        }
    }
    return (QOS_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSConfigVlanMapTables                               */
/* Description        : This function is used to Add or Delete the Vlan Map  */
/*                      Table ACTIVE Entries in the HARDWARE                 */
/*                      QOS_CFG_TYPE_CREATE - Add the Entries in HARDWARE    */
/*                      QOS_CFG_TYPE_DELETE - Del the Entries from HARDWARE  */
/* Input(s)           : u4CfgType - QOS_CFG_TYPE_CREATE / QOS_CFG_TYPE_DELETE*/
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : gQoSGlobalInfo                                       */
/* Global Variables                                                          */
/* Modified           : gQoSGlobalInfo                                       */
/* Return Value(s)    : QOS_SUCCESS or QOS_FAILURE                           */
/* Called By          : QoSShutdown                                          */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QoSConfigVlanMapTables (UINT4 u4CfgType)
{
    tQoSQMapNode       *pQMapNode = NULL;
    tQoSQNode          *pQNode = NULL;
    INT4                i4RetStatus = QOS_FAILURE;
    INT4                i4IfIndex = 0;
    INT4                i4NextIfIndex = 0;
    UINT4               u4CLASS = 0;
    UINT4               u4NextCLASS = 0;
    INT4                i4RegenPriType = 0;
    INT4                i4NextRGPType = 0;
    UINT4               u4RegenPri = 0;
    UINT4               u4NextRegenPri = 0;

    i4RetStatus = QoSGetNextQMapTblEntryIndex (i4IfIndex, &i4NextIfIndex,
                                               u4CLASS, &u4NextCLASS,
                                               i4RegenPriType, &i4NextRGPType,
                                               u4RegenPri, &u4NextRegenPri);
    if (i4RetStatus == QOS_FAILURE)
    {
        /* The Table is Empty */
        return (QOS_SUCCESS);
    }

    /* Table is Populated */
    do
    {
        pQMapNode = QoSUtlGetQMapNode (i4NextIfIndex, u4NextCLASS,
                                       i4NextRGPType, u4NextRegenPri);
        if (pQMapNode == NULL)
        {
            QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetQMapNode () "
                          " Returns NULL. \r\n", __FUNCTION__);
            return (QOS_FAILURE);
        }

        /* Only Active Node in the QMap Table need to Add */
        if (pQMapNode->u1Status == ACTIVE)
        {
            pQNode = QoSUtlGetQNode (i4NextIfIndex, pQMapNode->u4QId);
            if (pQNode != NULL)
            {
                if (pQNode->u4QueueType == QOS_Q_SUBSCRIBER_TYPE)
                {
                    if (u4CfgType == QOS_CFG_TYPE_CREATE)
                    {
                        i4RetStatus =
                            QoSHwWrMapClassToQueue ((INT4) pQMapNode->i4IfIndex,
                                                    pQMapNode->u4CLASS,
                                                    (INT4) pQMapNode->
                                                    u1RegenPriType,
                                                    pQMapNode->u1RegenPri,
                                                    pQMapNode->u4QId,
                                                    QOS_QMAP_ADD);
                        if (i4RetStatus == QOS_FAILURE)
                        {
                            QOS_TRC_ARG1 (MGMT_TRC, "In %s : "
                                          "QoSHwWrMapClassToQueue() : Add "
                                          "Returns FAILURE. \r\n",
                                          __FUNCTION__);
                            return (QOS_FAILURE);
                        }
                    }
                    else
                    {
                        /* Delete the currently active entries from the hardware */
                        i4RetStatus =
                            QoSHwWrMapClassToQueue ((INT4) pQMapNode->i4IfIndex,
                                                    pQMapNode->u4CLASS,
                                                    (INT4) pQMapNode->
                                                    u1RegenPriType,
                                                    pQMapNode->u1RegenPri,
                                                    pQMapNode->u4QId,
                                                    QOS_QMAP_DEL);
                        if (i4RetStatus == QOS_FAILURE)
                        {
                            QOS_TRC_ARG1 (MGMT_TRC, "In %s : "
                                          "QoSHwWrMapClassToQueue() : Del "
                                          "Returns FAILURE. \r\n",
                                          __FUNCTION__);
                            return (QOS_FAILURE);
                        }
                    }
                }
            }
            else
            {
                QOS_TRC_ARG1 (MGMT_TRC, "In %s : "
                              "QoSUtlGetQNode () : Get "
                              "Returns FAILURE. \r\n", __FUNCTION__);
            }

        }

        i4IfIndex = i4NextIfIndex;
        u4CLASS = u4NextCLASS;
        i4RegenPriType = i4NextRGPType;
        u4RegenPri = u4NextRegenPri;

        i4RetStatus = QoSGetNextQMapTblEntryIndex (i4IfIndex, &i4NextIfIndex,
                                                   u4CLASS, &u4NextCLASS,
                                                   i4RegenPriType,
                                                   &i4NextRGPType, u4RegenPri,
                                                   &u4NextRegenPri);
    }
    while (i4RetStatus == SNMP_SUCCESS);

    return (QOS_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSVlanQueueingEnable                                */
/* Description        : This function is used to Enable the Vlan Queueing. It*/
/*                      will do the following Actions                        */
/*                       It will configure the hardware with Current Data    */
/*                         in the Software.                                  */
/*                      1. Init the Hardware with the HL schema & subscriber */
/*                         queues required for Vlan Queueing (this is happen */
/*                         only once after a boot up - disabling will not    */
/*                         remove the HL and queues from hardware)           */
/*                      2. It Add the Vlan-Map entries present in SW to the  */
/*                         hardware.                                         */
/* Input(s)           : None.                                                */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : gu4QoSVlanQueueingGlobalInfo                         */
/* Return Value(s)    : QOS_SUCCESS or QOS_FAILURE                           */
/* Called By          : Protocol Main and SNMP/CLI                           */
/* Calling Function   : SNMP, QoSMain                                        */
/*****************************************************************************/
INT4
QoSVlanQueueingEnable ()
{
    INT4                i4RetStatus = QOS_FAILURE;

    /* Configure the HARDWARE if Configuration present in the Software */
    i4RetStatus = QoSConfigVlanMap (QOS_CFG_TYPE_CREATE);
    if (i4RetStatus == QOS_FAILURE)
    {
        return (QOS_FAILURE);
    }
    return (QOS_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QoSVlanQueueingDisable                               */
/* Description        : This function is used to Disale the Vlan Queueing. It*/
/*                      will do the following Actions                        */
/*                       It will configure the hardware with Current Data    */
/*                         in the Software.                                  */
/*                      1. It removes the Vlan-Map entries present in        */
/*                         the hardware.                                     */
/* Input(s)           : None.                                                */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : gu4QoSVlanQueueingGlobalInfo                         */
/* Return Value(s)    : QOS_SUCCESS or QOS_FAILURE                           */
/* Called By          : Protocol Main and SNMP/CLI                           */
/* Calling Function   : SNMP, QoSMain                                        */
/*****************************************************************************/
INT4
QoSVlanQueueingDisable ()
{
    INT4                i4RetStatus = QOS_FAILURE;

    /* Configure the HARDWARE if Configuration present in the Software */
    i4RetStatus = QoSConfigVlanMap (QOS_CFG_TYPE_DELETE);
    if (i4RetStatus == QOS_FAILURE)
    {
        return (QOS_FAILURE);
    }
    return (QOS_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QosUtilCreateSubscriberQueues                        */
/* Description        : This function is used to create subscriber queues    */
/* Input(s)           : None.                                                */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : gu4QoSVlanQueueingGlobalInfo                         */
/* Return Value(s)    : QOS_SUCCESS or QOS_FAILURE                           */
/* Called By          : Protocol Main and SNMP/CLI                           */
/* Calling Function   : SNMP, QoSMain                                        */
/*****************************************************************************/
INT4
QosUtilCreateSubscriberQueues ()
{
    INT4                i4RetStatus = QOS_FAILURE;
    INT4                i4IfIndex = 0;
    /* Iterate for all the interfaces to create and attach subscriber
     * queues. */
    for (i4IfIndex = 1; i4IfIndex <= SYS_DEF_MAX_PHYSICAL_INTERFACES;
         i4IfIndex++)
    {
        i4RetStatus = QosUtilCreateSubscriberQueuesForPort ((UINT4) i4IfIndex);
        if (i4RetStatus != QOS_SUCCESS)
        {
            QOS_TRC_ARG1 (MGMT_TRC,
                          "In %s : QosUtilCreateSubscriberQueuesForPort () - FAILED. \r\n",
                          __FUNCTION__);
            return (QOS_FAILURE);
        }
    }
    return QOS_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : QosUtilCreateSubscriberQueuesForPort                 */
/* Description        : This function is used to create subscriber queues    */
/* Input(s)           : None.                                                */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : gu4QoSVlanQueueingGlobalInfo                         */
/* Return Value(s)    : QOS_SUCCESS or QOS_FAILURE                           */
/* Called By          : QosUtilCreateSubscriberQueuesForPort                 */
/* Calling Function   : SNMP, QoSMain                                        */
/*****************************************************************************/
INT4
QosUtilCreateSubscriberQueuesForPort (UINT4 u4Port)
{
    tQoSQNode          *pQTbleNode = NULL;
    tQoSSchedNode      *pSchedNode = NULL;
    tQoSQTemplateNode  *pQTempNode = NULL;
    INT4                i4RetStatus = QOS_FAILURE;
    UINT1               u1IPVal = 0;
    UINT1               u1IPriVal = 0;
    UINT4               u4QTbleId = 0;
    UINT4               u4CfgTempId = 1;

    /* If the port is part of port channel Skip the entry    */
    /* The Que creation will not be applicable for the       */
    /* port part of port-channel.                            */
    if (QosL2IwfIsPortInPortChannel (u4Port) == L2IWF_SUCCESS)
    {
        return QOS_SUCCESS;
    }

    for (u1IPVal = QOS_QUEUE_ENTRY_MAX; u1IPVal < (QOS_QUEUE_ENTRY_MAX + 8);
         u1IPVal++)
    {
        u4QTbleId = (UINT4) u1IPVal + 1;
        /* Refcount for QueueTemplate */
        pQTempNode = QoSUtlGetQTempNode (u4CfgTempId);
        if (pQTempNode == NULL)
        {
            QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetQTempNode () "
                          "Returns FAILURE. \r\n", __FUNCTION__);
            return (QOS_FAILURE);
        }
        /* Incremnet the RefCount of QTempId  Entry in the QTempTable */
        pQTempNode->u4RefCount = (pQTempNode->u4RefCount) + 1;
        if ((pQTbleNode = QoSUtlGetQNode ((INT4) u4Port, u4QTbleId)) == NULL)
        {

            pQTbleNode = QoSCreateQTblEntry ((INT4) u4Port, u4QTbleId);
            if (pQTbleNode == NULL)
            {
                return (QOS_FAILURE);
            }

            /* Update the Entry  and dependent Tbls */
            pQTbleNode->u4Id = u4QTbleId;
            pQTbleNode->i4IfIndex = u4Port;
            pQTbleNode->u4CfgTemplateId = u4CfgTempId;
            pQTbleNode->u4ShapeId = 0;
            pQTbleNode->u2Weight = QOS_Q_TABLE_WEIGHT_DEFAULT;
            pQTbleNode->u1Priority = (u1IPriVal++);
            pQTbleNode->u4SchedulerId = (QOS_HL_DEF_NUM_S3_SCHEDULERS +
                                         QOS_HL_DEF_NUM_S2_SCHEDULERS +
                                         QOS_HL_DEF_NUM_S1_SCHEDULERS);
            /* Overriding Queue Type for Subscriber type Queues */
            pQTbleNode->u4QueueType = QOS_Q_SUBSCRIBER_TYPE;
            pQTbleNode->u1Status = ACTIVE;

            /* Refcount for SchedulerNode */
            pSchedNode =
                QoSUtlGetSchedNode ((INT4) u4Port, pQTbleNode->u4SchedulerId);
            if (pSchedNode == NULL)
            {
                QOS_TRC_ARG1 (MGMT_TRC, "In %s : QoSUtlGetSchedNode () "
                              "Returns FAILURE. \r\n", __FUNCTION__);
                return (QOS_FAILURE);
                /* Incremnet the RefCount of SchedId  Entry in the Sched Table */
            }
            pSchedNode->u4RefCount = (pSchedNode->u4RefCount) + 1;
        }

        /* Configure the HW  if Needed */
        /* 1. Add the entry into the HARDWARE. */
        i4RetStatus =
            QoSHwWrSubscriberQueueCreate ((INT4) pQTbleNode->i4IfIndex,
                                          pQTbleNode->u4Id);
        if (i4RetStatus == QOS_FAILURE)
        {
            QOS_TRC_ARG1 (MGMT_TRC, "In %s : "
                          "QoSHwWrSubscriberQueueCreate () Returns FAILURE.\r\n",
                          __FUNCTION__);
            return (QOS_FAILURE);
        }
    }                            /*End of -for */
    return (QOS_SUCCESS);
}

#endif /* __QOS_SYS_C__ */
