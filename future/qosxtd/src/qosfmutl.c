/*****************************************************************************/
/*    FILE  NAME            : qosfmutl.c                                     */
/*    PRINCIPAL AUTHOR      : Aricent Inc.                                   */
/*    MODULE NAME           : QOSX                                           */
/*    LANGUAGE              : C                                              */
/*    TARGET ENVIRONMENT    : Any                                            */
/*    DATE OF FIRST RELEASE :                                                */
/*    AUTHOR                : Aricent Inc.                                   */
/*    DESCRIPTION           : This file contains the exported APIs of        */
/*                            qos module                                     */
/*---------------------------------------------------------------------------*/
#include "qosinc.h"
/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : QosIsTypeValid                                     */
/*                                                                           */
/*     DESCRIPTION      : This function Checks if the rowpointer type        */
/*                        is valid.                                          */
/*                                                                           */
/*     INPUT            : u4CurrentType : Current functional block           */
/*                      : u4RPType    :  Next functional block               */
/*                      : u4SpecficType :  Specific functional block         */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : QOS_SUCCESS / QOS_FAILURE                          */
/*                                                                           */
/*****************************************************************************/
INT4
QosIsTypeValid (UINT4 u4CurrentType, UINT4 u4RPType1,
                UINT4 u4RPType2, UINT4 u4RPType3, UINT4 u4SpecficType)
{
    UNUSED_PARAM (u4CurrentType);
    UNUSED_PARAM (u4RPType1);
    UNUSED_PARAM (u4RPType2);
    UNUSED_PARAM (u4RPType3);
    UNUSED_PARAM (u4SpecficType);
    return (QOS_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : QosIsTypeValid                                     */
/*                                                                           */
/*     DESCRIPTION      : This function Checks whether the mentioned         */
/*                        AlgoDrop Type is valid for configuring as an       */
/*                        action.                                            */
/*     INPUT            : i4AlgoDropType : The Algo drop type to be checked  */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : QOS_SUCCESS / QOS_FAILURE                          */
/*                                                                           */
/*****************************************************************************/

INT4
QosIsDropTypeValid (INT4 i4AlgoDropType)
{
    UNUSED_PARAM (i4AlgoDropType);
    return QOS_SUCCESS;
}
