/*****************************************************************************/
/*    FILE  NAME            : qosbcmutl.c                                     */
/*    PRINCIPAL AUTHOR      : Aricent Inc.                                   */
/*    MODULE NAME           : QOSX                                           */
/*    LANGUAGE              : C                                              */
/*    TARGET ENVIRONMENT    : Any                                            */
/*    DATE OF FIRST RELEASE :                                                */
/*    AUTHOR                : Aricent Inc.                                   */
/*    DESCRIPTION           : This file contains the exported APIs of        */
/*                            qos module                                     */
/*---------------------------------------------------------------------------*/
#include "qosinc.h"
/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : QosIsTypeValid                                     */
/*                                                                           */
/*     DESCRIPTION      : This function Checks if the rowpointer type        */
/*                        is valid. For Ex                                   */
/*                        in BCM After Classifier only Meter is possible     */
/*                        ie  Classfier->Meter is correct                    */
/*                            Classfier->Action is not correct               */
/*     INPUT            : u4CurrentType : Current functional block           */
/*                      : u4RPType    :  Next functional block             */
/*                      : u4SpecficType :  Specific functional block         */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : QOS_SUCCESS / QOS_FAILURE                          */
/*                                                                           */
/*****************************************************************************/
INT4
QosIsTypeValid (UINT4 u4CurrentType, UINT4 u4RPType1,
                UINT4 u4RPType2, UINT4 u4RPType3, UINT4 u4SpecficType)
{

    switch (u4CurrentType)
    {
        case DS_DATAPATH:
            if ((u4RPType1 != DS_CLFR) && (u4RPType1 != DS_QUEUE)
                && (u4RPType1 != QOS_ROWPOINTER_DEF))
            {
                return QOS_FAILURE;
            }
            break;
        case DS_CLFR:
            if (u4RPType1 != QOS_ROWPOINTER_DEF)
            {
                return QOS_FAILURE;
            }
            if (u4SpecficType != QOS_ROWPOINTER_DEF)
            {
                return QOS_FAILURE;
            }
            break;

        case DS_CLFR_ELEM:
            if (u4RPType1 == DS_CLFR)
            {
                return QOS_FAILURE;
            }
            if ((u4SpecficType != DS_MF_CLFR) &&
                (u4SpecficType != QOS_ROWPOINTER_DEF))
            {
                return QOS_FAILURE;
            }
            break;
        case DS_MF_CLFR:
            break;
        case DS_METER:
            if ((u4RPType1 != DS_ACTION) && (u4RPType1 != QOS_ROWPOINTER_DEF))
            {
                return QOS_FAILURE;
            }
            if ((u4RPType2 != DS_METER) && (u4RPType2 != DS_ACTION)
                && (u4RPType2 != QOS_ROWPOINTER_DEF))
            {
                return QOS_FAILURE;
            }
            if (u4SpecficType != DS_TBPARM)
            {
                return QOS_FAILURE;
            }
            break;
        case DS_TBPARM:
            break;
        case DS_ACTION:
            if (u4RPType1 != QOS_ROWPOINTER_DEF)
            {
                return QOS_FAILURE;
            }
            if (u4RPType2 != QOS_ROWPOINTER_DEF)
            {
                return QOS_FAILURE;
            }
            if ((u4SpecficType != DS_STDDSCP) &&
                (u4SpecficType != DS_COUNT_ACT) &&
                (u4SpecficType != DS_ALG_DROP) &&
                (u4SpecficType != QOS_ROWPOINTER_DEF))
            {
                return QOS_FAILURE;
            }
            break;
        case DS_STDDSCP:
            break;
        case DS_COUNT_ACT:
            break;
        case DS_ALG_DROP:
            if ((u4RPType1 != DS_QUEUE) && (u4RPType1 != QOS_ROWPOINTER_DEF))
            {
                return QOS_FAILURE;
            }
            break;
        case DS_RANDOM_DROP:
            break;
        case DS_QUEUE:
            if ((u4RPType1 != DS_SCHEDULER) &&
                (u4RPType1 != QOS_ROWPOINTER_DEF))
            {
                return QOS_FAILURE;
            }
            if ((u4RPType2 != DS_MINRATE) && (u4RPType2 != QOS_ROWPOINTER_DEF))
            {
                return QOS_FAILURE;
            }
            if ((u4RPType3 != DS_MAXRATE) && (u4RPType3 != QOS_ROWPOINTER_DEF))
            {
                return QOS_FAILURE;
            }
            break;
        case DS_SCHEDULER:
            if (u4RPType1 != QOS_ROWPOINTER_DEF)
            {
                return QOS_FAILURE;
            }
            if (u4RPType2 != QOS_ROWPOINTER_DEF)
            {
                return QOS_FAILURE;
            }
            if (u4RPType3 != QOS_ROWPOINTER_DEF)
            {
                return QOS_FAILURE;
            }
            break;
        case DS_MINRATE:
            break;
        case DS_MAXRATE:
            break;
        default:
            break;
    }
    return (QOS_SUCCESS);

}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : QosIsDropTypeValid                                 */
/*                                                                           */
/*     DESCRIPTION      : This function Checks whether the mentioned         */
/*                        AlgoDrop Type is valid for configuring as an       */
/*                        action.                                            */
/*     INPUT            : i4AlgoDropType : The Algo drop type to be checked  */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : QOS_SUCCESS / QOS_FAILURE                          */
/*                                                                           */
/*****************************************************************************/

INT4
QosIsDropTypeValid (INT4 i4AlgoDropType)
{
    /* Action Spwcifica can be Drop Always */
    if ((i4AlgoDropType == QOS_Q_TEMP_DROP_TYPE_ALWAYS) ||
        (i4AlgoDropType == QOS_Q_TEMP_DROP_TYPE_TAIL))
    {
        return QOS_SUCCESS;
    }

    return QOS_FAILURE;
}
