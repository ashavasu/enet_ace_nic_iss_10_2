/********************************************************************
* Copyright (C) 2010 Aricent Inc . All Rights Reserved
*
* $Id: cntrc.h,v 1.3 2010/12/14 10:43:26 siva Exp $
*
* Description: This file contains procedures and definitions
*             used for debugging.
*******************************************************************/
#ifndef _CN_TRC_H_
#define _CN_TRC_H_

#define CN_TRC_MAX_CHRS_PER_LINE      16
#ifdef TRACE_WANTED


#define  CN_TRC_FLAG(x) (gCnMasterTblInfo.ppCompTblInfo[x]->u4TraceLevel)

#define CN_TLV_DUMP(Comp, TraceType, pBuf, len) \
            if ((CN_TRC_FLAG(Comp) & TraceType) != CN_ZERO)    \
            {\
             CnTlvDump (Comp, TraceType, pBuf, len); \
            }
          
#define CN_GBL_TRC(Comp, TraceType, Str)                      \
        CnTrace(Comp, TraceType, Str); 
#define CN_GBL_TRC_ARG1(Comp, TraceType, Str, Arg1)            \
        CnTrace(Comp, TraceType, Str, Arg1); 
#define CN_GBL_TRC_ARG2(Comp, TraceType, Str, Arg1, Arg2)      \
       CnTrace(Comp, TraceType, Str, Arg1, Arg2); 

#define CN_GBL_TRC_ARG3(Comp, TraceType, Str, Arg1, Arg2, Arg3)      \
        CnTrace(Comp, TraceType, Str, Arg1, Arg2, Arg3); 

#define CN_GBL_TRC_ARG4(Comp, TraceType, Str, Arg1, Arg2, Arg3, Arg4)  \
        CnTrace(Comp, TraceType, Str, Arg1, Arg2, Arg3, Arg4); 

#define CN_GBL_TRC_ARG5(Comp, TraceType, Str, Arg1, Arg2, Arg3, Arg4, Arg5)    \
        CnTrace(Comp, TraceType, Str, Arg1, Arg2, Arg3, Arg4, Arg5); 

#define CN_GBL_TRC_ARG6(Comp, TraceType, Str, Arg1, Arg2, Arg3, Arg4, Arg5, Arg6)  \
        CnTrace(Comp, TraceType, Str, Arg1, Arg2, Arg3, Arg4, Arg5, Arg6); 

#define CN_TRC(Comp, TraceType, Str)                      \
       if ((CN_TRC_FLAG(Comp) & TraceType) != CN_ZERO)    \
        {\
            CnTrace(Comp, TraceType, Str); \
        }
 
#define CN_TRC_ARG1(Comp, TraceType, Str, Arg1)            \
       if ((CN_TRC_FLAG(Comp) & TraceType) != CN_ZERO)    \
        {\
            CnTrace(Comp, TraceType, Str, Arg1); \
        }

#define CN_TRC_ARG2(Comp, TraceType, Str, Arg1, Arg2)      \
       if ((CN_TRC_FLAG(Comp) & TraceType) != CN_ZERO)    \
        {\
            CnTrace(Comp, TraceType, Str, Arg1, Arg2);  \
        }

#define CN_TRC_ARG3(Comp, TraceType, Str, Arg1, Arg2, Arg3)      \
       if ((CN_TRC_FLAG(Comp) & TraceType) != CN_ZERO)    \
        {\
            CnTrace(Comp, TraceType, Str, Arg1, Arg2, Arg3);     \
        }
    

#define CN_TRC_ARG4(Comp, TraceType, Str, Arg1, Arg2, Arg3, Arg4)  \
       if ((CN_TRC_FLAG(Comp) & TraceType) != CN_ZERO)    \
        {\
          CnTrace(Comp, TraceType, Str, Arg1, Arg2, Arg3, Arg4);  \
        }
    

#define CN_TRC_ARG5(Comp, TraceType, Str, Arg1, Arg2, Arg3, Arg4, Arg5)    \
       if ((CN_TRC_FLAG(Comp) & TraceType) != CN_ZERO)    \
        {\
            CnTrace(Comp, TraceType, Str, Arg1, Arg2, Arg3, Arg4, Arg5);      \
        }

#define CN_TRC_ARG6(Comp, TraceType, Str, Arg1, Arg2, Arg3, Arg4, Arg5, Arg6)  \
       if ((CN_TRC_FLAG(Comp) & TraceType) != CN_ZERO)    \
        {\
         CnTrace(Comp, TraceType, Str, Arg1, Arg2, Arg3, Arg4, Arg5, Arg6);  \
        }

#else  /* TRACE_WANTED */
#define CN_TLV_DUMP(Comp, TraceType, pBuf, Len) 
#define CN_GBL_TRC(Comp, TraceType, Str)
#define CN_GBL_TRC_ARG1(Comp, TraceType, Str, Arg1)
#define CN_GBL_TRC_ARG2(Comp, TraceType, Str, Arg1, Arg2)
#define CN_GBL_TRC_ARG3(Comp, TraceType, Str, Arg1, Arg2, Arg3)
#define CN_GBL_TRC_ARG4(Comp, TraceType, Str, Arg1, Arg2, Arg3, Arg4)
#define CN_GBL_TRC_ARG5(Comp, TraceType, Str, Arg1, Arg2, Arg3, Arg4, Arg5) 
#define CN_GBL_TRC_ARG6(Comp, TraceType, Str, Arg1, Arg2, Arg3, Arg4, Arg5, Arg6) 

#define CN_TRC(Comp, TraceType, Str)
#define CN_TRC_ARG1(Comp, TraceType, Str, Arg1)
#define CN_TRC_ARG2(Comp, TraceType, Str, Arg1, Arg2)
#define CN_TRC_ARG3(Comp, TraceType, Str, Arg1, Arg2, Arg3)
#define CN_TRC_ARG4(Comp, TraceType, Str, Arg1, Arg2, Arg3, Arg4)
#define CN_TRC_ARG5(Comp, TraceType, Str, Arg1, Arg2, Arg3, Arg4, Arg5) 
#define CN_TRC_ARG6(Comp, TraceType, Str, Arg1, Arg2, Arg3, Arg4, Arg5, Arg6) 
#endif /* TRACE_WANTED */




#endif /* _CN_TRC_H_ */
/*---------------------------------------------------------------------------*/
/*                       End of the file  cntrc.h                            */
/*---------------------------------------------------------------------------*/
