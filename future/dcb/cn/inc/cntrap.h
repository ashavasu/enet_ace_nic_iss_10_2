/********************************************************************
* Copyright (C) 2010 Aricent Inc . All Rights Reserved
*
* $Id: cntrap.h,v 1.4 2011/04/28 11:13:13 siva Exp $
*
* Description: This file contains trap data structures defined for
*              CN  module.
*********************************************************************/

#ifndef _CN_TRAP_H
#define _CN_TRAP_H

#define CN_OBJECT_NAME_LEN              256
#define CN_SNMPV2_TRAP_OID_LEN          11

#define CN_CNM_TRAP_ENABLE               2
#define CN_ERROR_PORT_ENTRY_TRAP_ENABLE  1

#define  CN_ERROR_PORT_ENTRY_TRAP        1
#define  CN_CNM_TRAP                     2 
#define  CN_ALL_TRAP                     \
                                        ((CN_ERROR_PORT_ENTRY_TRAP)| \
                                        (CN_CNM_TRAP))


enum
{
    CN_ADDED_ERROR_PORT_ENTRY = 1,
    CN_DELETED_ERROR_PORT_ENTRY,
    CN_SEND_CN_MESSAGE
};

/*CN Message Parameters */
typedef struct CnmParams
{
    UINT1 au1CpIdentifier[CN_CPID_LEN]; /*Congestion point identifier*/
    INT4  i4CnmQOffset; /*CNM Offset */
    INT4  i4CnmQDelta; /* CN Message Delta*/
}tCnmParams;

/*Used for sending Trap*/
typedef struct CnTrapMsg
{
    UINT4 u4ContextId; /*Context Id*/
    UINT4 u4PortId;    /*Port Number*/
    UINT1 u1CNPV;      /*CN Priority Value*/ 
    UINT1 au1Pad[3];  /* Pad */
    union
    {
        /*Congestion Message Params*/
        tCnmParams CnmParams;
    }unTrapMsg;
}tCnTrapMsg;  

tSNMP_OID_TYPE* CnMakeObjIdFrmString  PROTO ((INT1 *pi1TextStr, 
                                      UINT1 *pu1TableName));
VOID CnSendNotification PROTO ((tCnTrapMsg *, UINT1 u1Type));

#endif /*_CN_TRAP_H*/
/*--------------------------------------------------------------------------*/
/*                       End of the file  cntrap.h                          */
/*--------------------------------------------------------------------------*/
