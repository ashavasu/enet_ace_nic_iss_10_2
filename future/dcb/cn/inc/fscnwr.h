#ifndef _FSCNWR_H
#define _FSCNWR_H

VOID RegisterFSCN(VOID);

VOID UnRegisterFSCN(VOID);
INT4 FsCnSystemControlGet(tSnmpIndex *, tRetVal *);
INT4 FsCnGlobalEnableTrapGet(tSnmpIndex *, tRetVal *);
INT4 FsCnSystemControlSet(tSnmpIndex *, tRetVal *);
INT4 FsCnGlobalEnableTrapSet(tSnmpIndex *, tRetVal *);
INT4 FsCnSystemControlTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsCnGlobalEnableTrapTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsCnSystemControlDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsCnGlobalEnableTrapDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 GetNextIndexFsCnXGlobalTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsCnXGlobalTraceLevelGet(tSnmpIndex *, tRetVal *);
INT4 FsCnXGlobalClearCountersGet(tSnmpIndex *, tRetVal *);
INT4 FsCnXGlobalTLVErrorsGet(tSnmpIndex *, tRetVal *);
INT4 FsCnXGlobalTraceLevelSet(tSnmpIndex *, tRetVal *);
INT4 FsCnXGlobalClearCountersSet(tSnmpIndex *, tRetVal *);
INT4 FsCnXGlobalTraceLevelTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsCnXGlobalClearCountersTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsCnXGlobalTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 GetNextIndexFsCnXPortPriTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsCnXPortPriClearCpCountersGet(tSnmpIndex *, tRetVal *);
INT4 FsCnXPortPriErrorEntryGet(tSnmpIndex *, tRetVal *);
INT4 FsCnXPortPriOperDefModeGet(tSnmpIndex *, tRetVal *);
INT4 FsCnXPortPriOperAltPriGet(tSnmpIndex *, tRetVal *);
INT4 FsCnXPortPriLastRcvdEventGet(tSnmpIndex *, tRetVal *);
INT4 FsCnXPortPriLastRcvdEventTimeGet(tSnmpIndex *, tRetVal *);
INT4 FsCnXPortPriLastSentEventGet(tSnmpIndex *, tRetVal *);
INT4 FsCnXPortPriLastSentEventTimeGet(tSnmpIndex *, tRetVal *);
INT4 FsCnXPortPriClearCpCountersSet(tSnmpIndex *, tRetVal *);
INT4 FsCnXPortPriClearCpCountersTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsCnXPortPriTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
#endif /* _FSCNWR_H */
