/********************************************************************
* Copyright (C) 2010 Aricent Inc . All Rights Reserved
*
* $Id: cnsem.h,v 1.3 2010/07/23 13:33:20 prabuc Exp $
*
* Description: Data structure used by CN module State
*              Event machine.
*********************************************************************/
#ifndef _CN_SEM_H_
#define _CN_SEM_H_

/* Index of the Sem Function pointers */
enum
{
  A0 = 0,
  A1 = 1,
  A2 = 2,
  A3 = 3,
  A4 = 4,
  CN_MAX_SEM_FN_PTRS = 5
};

/* CN SEM function pointers */
void (*gaCnActionProc[CN_MAX_SEM_FN_PTRS]) (tCnCompTblInfo *pCompEntry,
                                            tCnPortTblInfo *pPortEntry,
                                            tCnPortPriTblInfo *pPortPriEntry) =
{              /*states*/
 /* A0 */ CnSmEventIgnore,  		/* Do Nothing */
 /* A1 */ CnSmStateDisabled,   		/* CN_DISABLE */
 /* A2 */ CnSmStateEdge,		/* CN_EDGE */
 /* A3 */ CnSmStateInterior,		/* CN_INTERIOR */
 /* A4 */ CnSmStateInteriorReady   /* CN_INTERIOR_READY */
};

/* CN State Event Table */

const UINT1 gau1CnSem[CN_MAX_EVENTS][CN_MAX_STATES] =
{
/*                               STATES:
 * _______________________________________________________
 *                |      CN   | Interior |Interior | Edge |
 *   EVENTS       |  Disabled |          | Ready   |      |
 * _______________________________________________________*/
/* NOT_RECEIVE_TLV|*/ {   A0,    A2,      A2,        A0},
/* RECEIVE_TLV    |*/ {   A0,    A0,      A3,        A3},
/* RECEIVE_READY  |*/ {   A0,    A4,      A0,        A4},
/*--------------------------------------------------------*/ 
};

#endif 
/*-----------------------------------------------------------------------*/
/*                       End of the file  cnsem.h                        */
/*-----------------------------------------------------------------------*/

