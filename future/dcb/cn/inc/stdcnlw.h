/********************************************************************
* Copyright (C) 2009 Aricent Inc . All Rights Reserved
*
* $Id: stdcnlw.h,v 1.1.1.1 2010/07/05 06:43:40 prabuc Exp $
*
* Description: Proto types for Low Level  Routines
*********************************************************************/

/* Proto Validate Index Instance for Ieee8021CnGlobalTable. */
INT1
nmhValidateIndexInstanceIeee8021CnGlobalTable ARG_LIST((UINT4 ));

/* Proto Type for Low Level GET FIRST fn for Ieee8021CnGlobalTable  */

INT1
nmhGetFirstIndexIeee8021CnGlobalTable ARG_LIST((UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexIeee8021CnGlobalTable ARG_LIST((UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetIeee8021CnGlobalMasterEnable ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetIeee8021CnGlobalCnmTransmitPriority ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetIeee8021CnGlobalDiscardedFrames ARG_LIST((UINT4 ,tSNMP_COUNTER64_TYPE *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetIeee8021CnGlobalMasterEnable ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetIeee8021CnGlobalCnmTransmitPriority ARG_LIST((UINT4  ,UINT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Ieee8021CnGlobalMasterEnable ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2Ieee8021CnGlobalCnmTransmitPriority ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2Ieee8021CnGlobalTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for Ieee8021CnErroredPortTable. */
INT1
nmhValidateIndexInstanceIeee8021CnErroredPortTable ARG_LIST((UINT4  , UINT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for Ieee8021CnErroredPortTable  */

INT1
nmhGetFirstIndexIeee8021CnErroredPortTable ARG_LIST((UINT4 * , UINT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexIeee8021CnErroredPortTable ARG_LIST((UINT4 , UINT4 * , UINT4 , UINT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

/* Proto Validate Index Instance for Ieee8021CnCompntPriTable. */
INT1
nmhValidateIndexInstanceIeee8021CnCompntPriTable ARG_LIST((UINT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for Ieee8021CnCompntPriTable  */

INT1
nmhGetFirstIndexIeee8021CnCompntPriTable ARG_LIST((UINT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexIeee8021CnCompntPriTable ARG_LIST((UINT4 , UINT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetIeee8021CnComPriDefModeChoice ARG_LIST((UINT4  , UINT4 ,INT4 *));

INT1
nmhGetIeee8021CnComPriAlternatePriority ARG_LIST((UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetIeee8021CnComPriAutoAltPri ARG_LIST((UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetIeee8021CnComPriAdminDefenseMode ARG_LIST((UINT4  , UINT4 ,INT4 *));

INT1
nmhGetIeee8021CnComPriCreation ARG_LIST((UINT4  , UINT4 ,INT4 *));

INT1
nmhGetIeee8021CnComPriLldpInstanceChoice ARG_LIST((UINT4  , UINT4 ,INT4 *));

INT1
nmhGetIeee8021CnComPriLldpInstanceSelector ARG_LIST((UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetIeee8021CnComPriRowStatus ARG_LIST((UINT4  , UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetIeee8021CnComPriDefModeChoice ARG_LIST((UINT4  , UINT4  ,INT4 ));

INT1
nmhSetIeee8021CnComPriAlternatePriority ARG_LIST((UINT4  , UINT4  ,UINT4 ));

INT1
nmhSetIeee8021CnComPriAdminDefenseMode ARG_LIST((UINT4  , UINT4  ,INT4 ));

INT1
nmhSetIeee8021CnComPriCreation ARG_LIST((UINT4  , UINT4  ,INT4 ));

INT1
nmhSetIeee8021CnComPriLldpInstanceChoice ARG_LIST((UINT4  , UINT4  ,INT4 ));

INT1
nmhSetIeee8021CnComPriLldpInstanceSelector ARG_LIST((UINT4  , UINT4  ,UINT4 ));

INT1
nmhSetIeee8021CnComPriRowStatus ARG_LIST((UINT4  , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Ieee8021CnComPriDefModeChoice ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2Ieee8021CnComPriAlternatePriority ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2Ieee8021CnComPriAdminDefenseMode ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2Ieee8021CnComPriCreation ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2Ieee8021CnComPriLldpInstanceChoice ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2Ieee8021CnComPriLldpInstanceSelector ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2Ieee8021CnComPriRowStatus ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2Ieee8021CnCompntPriTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for Ieee8021CnPortPriTable. */
INT1
nmhValidateIndexInstanceIeee8021CnPortPriTable ARG_LIST((UINT4  , UINT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for Ieee8021CnPortPriTable  */

INT1
nmhGetFirstIndexIeee8021CnPortPriTable ARG_LIST((UINT4 * , UINT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexIeee8021CnPortPriTable ARG_LIST((UINT4 , UINT4 * , UINT4 , UINT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetIeee8021CnPortPriDefModeChoice ARG_LIST((UINT4  , UINT4  , INT4 ,INT4 *));

INT1
nmhGetIeee8021CnPortPriAdminDefenseMode ARG_LIST((UINT4  , UINT4  , INT4 ,INT4 *));

INT1
nmhGetIeee8021CnPortPriAutoDefenseMode ARG_LIST((UINT4  , UINT4  , INT4 ,INT4 *));

INT1
nmhGetIeee8021CnPortPriLldpInstanceChoice ARG_LIST((UINT4  , UINT4  , INT4 ,INT4 *));

INT1
nmhGetIeee8021CnPortPriLldpInstanceSelector ARG_LIST((UINT4  , UINT4  , INT4 ,UINT4 *));

INT1
nmhGetIeee8021CnPortPriAlternatePriority ARG_LIST((UINT4  , UINT4  , INT4 ,UINT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetIeee8021CnPortPriDefModeChoice ARG_LIST((UINT4  , UINT4  , INT4  ,INT4 ));

INT1
nmhSetIeee8021CnPortPriAdminDefenseMode ARG_LIST((UINT4  , UINT4  , INT4  ,INT4 ));

INT1
nmhSetIeee8021CnPortPriLldpInstanceChoice ARG_LIST((UINT4  , UINT4  , INT4  ,INT4 ));

INT1
nmhSetIeee8021CnPortPriLldpInstanceSelector ARG_LIST((UINT4  , UINT4  , INT4  ,UINT4 ));

INT1
nmhSetIeee8021CnPortPriAlternatePriority ARG_LIST((UINT4  , UINT4  , INT4  ,UINT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Ieee8021CnPortPriDefModeChoice ARG_LIST((UINT4 *  ,UINT4  , UINT4  , INT4  ,INT4 ));

INT1
nmhTestv2Ieee8021CnPortPriAdminDefenseMode ARG_LIST((UINT4 *  ,UINT4  , UINT4  , INT4  ,INT4 ));

INT1
nmhTestv2Ieee8021CnPortPriLldpInstanceChoice ARG_LIST((UINT4 *  ,UINT4  , UINT4  , INT4  ,INT4 ));

INT1
nmhTestv2Ieee8021CnPortPriLldpInstanceSelector ARG_LIST((UINT4 *  ,UINT4  , UINT4  , INT4  ,UINT4 ));

INT1
nmhTestv2Ieee8021CnPortPriAlternatePriority ARG_LIST((UINT4 *  ,UINT4  , UINT4  , INT4  ,UINT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2Ieee8021CnPortPriTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for Ieee8021CnCpTable. */
INT1
nmhValidateIndexInstanceIeee8021CnCpTable ARG_LIST((UINT4  , INT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for Ieee8021CnCpTable  */

INT1
nmhGetFirstIndexIeee8021CnCpTable ARG_LIST((UINT4 * , INT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexIeee8021CnCpTable ARG_LIST((UINT4 , UINT4 * , INT4 , INT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetIeee8021CnCpPriority ARG_LIST((UINT4  , INT4  , UINT4 ,UINT4 *));

INT1
nmhGetIeee8021CnCpMacAddress ARG_LIST((UINT4  , INT4  , UINT4 ,tMacAddr * ));

INT1
nmhGetIeee8021CnCpIdentifier ARG_LIST((UINT4  , INT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetIeee8021CnCpQueueSizeSetPoint ARG_LIST((UINT4  , INT4  , UINT4 ,UINT4 *));

INT1
nmhGetIeee8021CnCpFeedbackWeight ARG_LIST((UINT4  , INT4  , UINT4 ,INT4 *));

INT1
nmhGetIeee8021CnCpMinSampleBase ARG_LIST((UINT4  , INT4  , UINT4 ,UINT4 *));

INT1
nmhGetIeee8021CnCpDiscardedFrames ARG_LIST((UINT4  , INT4  , UINT4 ,tSNMP_COUNTER64_TYPE *));

INT1
nmhGetIeee8021CnCpTransmittedFrames ARG_LIST((UINT4  , INT4  , UINT4 ,tSNMP_COUNTER64_TYPE *));

INT1
nmhGetIeee8021CnCpTransmittedCnms ARG_LIST((UINT4  , INT4  , UINT4 ,tSNMP_COUNTER64_TYPE *));

INT1
nmhGetIeee8021CnCpMinHeaderOctets ARG_LIST((UINT4  , INT4  , UINT4 ,UINT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetIeee8021CnCpQueueSizeSetPoint ARG_LIST((UINT4  , INT4  , UINT4  ,UINT4 ));

INT1
nmhSetIeee8021CnCpFeedbackWeight ARG_LIST((UINT4  , INT4  , UINT4  ,INT4 ));

INT1
nmhSetIeee8021CnCpMinSampleBase ARG_LIST((UINT4  , INT4  , UINT4  ,UINT4 ));

INT1
nmhSetIeee8021CnCpMinHeaderOctets ARG_LIST((UINT4  , INT4  , UINT4  ,UINT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Ieee8021CnCpQueueSizeSetPoint ARG_LIST((UINT4 *  ,UINT4  , INT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2Ieee8021CnCpFeedbackWeight ARG_LIST((UINT4 *  ,UINT4  , INT4  , UINT4  ,INT4 ));

INT1
nmhTestv2Ieee8021CnCpMinSampleBase ARG_LIST((UINT4 *  ,UINT4  , INT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2Ieee8021CnCpMinHeaderOctets ARG_LIST((UINT4 *  ,UINT4  , INT4  , UINT4  ,UINT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2Ieee8021CnCpTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for Ieee8021CnCpidToInterfaceTable. */
INT1
nmhValidateIndexInstanceIeee8021CnCpidToInterfaceTable ARG_LIST((tSNMP_OCTET_STRING_TYPE *));

/* Proto Type for Low Level GET FIRST fn for Ieee8021CnCpidToInterfaceTable  */

INT1
nmhGetFirstIndexIeee8021CnCpidToInterfaceTable ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexIeee8021CnCpidToInterfaceTable ARG_LIST((tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetIeee8021CnCpidToIfComponentId ARG_LIST((tSNMP_OCTET_STRING_TYPE *,UINT4 *));

INT1
nmhGetIeee8021CnCpidToIfIfIndex ARG_LIST((tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetIeee8021CnCpidToIfCpIndex ARG_LIST((tSNMP_OCTET_STRING_TYPE *,UINT4 *));

/* Proto Validate Index Instance for Ieee8021CnRpPortPriTable. */
INT1
nmhValidateIndexInstanceIeee8021CnRpPortPriTable ARG_LIST((UINT4  , UINT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for Ieee8021CnRpPortPriTable  */

INT1
nmhGetFirstIndexIeee8021CnRpPortPriTable ARG_LIST((UINT4 * , UINT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexIeee8021CnRpPortPriTable ARG_LIST((UINT4 , UINT4 * , UINT4 , UINT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetIeee8021CnRpPortPriMaxRps ARG_LIST((UINT4  , UINT4  , INT4 ,UINT4 *));

INT1
nmhGetIeee8021CnRpPortPriCreatedRps ARG_LIST((UINT4  , UINT4  , INT4 ,UINT4 *));

INT1
nmhGetIeee8021CnRpPortPriCentiseconds ARG_LIST((UINT4  , UINT4  , INT4 ,tSNMP_COUNTER64_TYPE *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetIeee8021CnRpPortPriMaxRps ARG_LIST((UINT4  , UINT4  , INT4  ,UINT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Ieee8021CnRpPortPriMaxRps ARG_LIST((UINT4 *  ,UINT4  , UINT4  , INT4  ,UINT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2Ieee8021CnRpPortPriTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for Ieee8021CnRpGroupTable. */
INT1
nmhValidateIndexInstanceIeee8021CnRpGroupTable ARG_LIST((UINT4  , UINT4  , INT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for Ieee8021CnRpGroupTable  */

INT1
nmhGetFirstIndexIeee8021CnRpGroupTable ARG_LIST((UINT4 * , UINT4 * , INT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexIeee8021CnRpGroupTable ARG_LIST((UINT4 , UINT4 * , UINT4 , UINT4 * , INT4 , INT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetIeee8021CnRpgEnable ARG_LIST((UINT4  , UINT4  , INT4  , UINT4 ,INT4 *));

INT1
nmhGetIeee8021CnRpgTimeReset ARG_LIST((UINT4  , UINT4  , INT4  , UINT4 ,INT4 *));

INT1
nmhGetIeee8021CnRpgByteReset ARG_LIST((UINT4  , UINT4  , INT4  , UINT4 ,UINT4 *));

INT1
nmhGetIeee8021CnRpgThreshold ARG_LIST((UINT4  , UINT4  , INT4  , UINT4 ,UINT4 *));

INT1
nmhGetIeee8021CnRpgMaxRate ARG_LIST((UINT4  , UINT4  , INT4  , UINT4 ,UINT4 *));

INT1
nmhGetIeee8021CnRpgAiRate ARG_LIST((UINT4  , UINT4  , INT4  , UINT4 ,UINT4 *));

INT1
nmhGetIeee8021CnRpgHaiRate ARG_LIST((UINT4  , UINT4  , INT4  , UINT4 ,UINT4 *));

INT1
nmhGetIeee8021CnRpgGd ARG_LIST((UINT4  , UINT4  , INT4  , UINT4 ,INT4 *));

INT1
nmhGetIeee8021CnRpgMinDecFac ARG_LIST((UINT4  , UINT4  , INT4  , UINT4 ,UINT4 *));

INT1
nmhGetIeee8021CnRpgMinRate ARG_LIST((UINT4  , UINT4  , INT4  , UINT4 ,UINT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetIeee8021CnRpgEnable ARG_LIST((UINT4  , UINT4  , INT4  , UINT4  ,INT4 ));

INT1
nmhSetIeee8021CnRpgTimeReset ARG_LIST((UINT4  , UINT4  , INT4  , UINT4  ,INT4 ));

INT1
nmhSetIeee8021CnRpgByteReset ARG_LIST((UINT4  , UINT4  , INT4  , UINT4  ,UINT4 ));

INT1
nmhSetIeee8021CnRpgThreshold ARG_LIST((UINT4  , UINT4  , INT4  , UINT4  ,UINT4 ));

INT1
nmhSetIeee8021CnRpgMaxRate ARG_LIST((UINT4  , UINT4  , INT4  , UINT4  ,UINT4 ));

INT1
nmhSetIeee8021CnRpgAiRate ARG_LIST((UINT4  , UINT4  , INT4  , UINT4  ,UINT4 ));

INT1
nmhSetIeee8021CnRpgHaiRate ARG_LIST((UINT4  , UINT4  , INT4  , UINT4  ,UINT4 ));

INT1
nmhSetIeee8021CnRpgGd ARG_LIST((UINT4  , UINT4  , INT4  , UINT4  ,INT4 ));

INT1
nmhSetIeee8021CnRpgMinDecFac ARG_LIST((UINT4  , UINT4  , INT4  , UINT4  ,UINT4 ));

INT1
nmhSetIeee8021CnRpgMinRate ARG_LIST((UINT4  , UINT4  , INT4  , UINT4  ,UINT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Ieee8021CnRpgEnable ARG_LIST((UINT4 *  ,UINT4  , UINT4  , INT4  , UINT4  ,INT4 ));

INT1
nmhTestv2Ieee8021CnRpgTimeReset ARG_LIST((UINT4 *  ,UINT4  , UINT4  , INT4  , UINT4  ,INT4 ));

INT1
nmhTestv2Ieee8021CnRpgByteReset ARG_LIST((UINT4 *  ,UINT4  , UINT4  , INT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2Ieee8021CnRpgThreshold ARG_LIST((UINT4 *  ,UINT4  , UINT4  , INT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2Ieee8021CnRpgMaxRate ARG_LIST((UINT4 *  ,UINT4  , UINT4  , INT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2Ieee8021CnRpgAiRate ARG_LIST((UINT4 *  ,UINT4  , UINT4  , INT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2Ieee8021CnRpgHaiRate ARG_LIST((UINT4 *  ,UINT4  , UINT4  , INT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2Ieee8021CnRpgGd ARG_LIST((UINT4 *  ,UINT4  , UINT4  , INT4  , UINT4  ,INT4 ));

INT1
nmhTestv2Ieee8021CnRpgMinDecFac ARG_LIST((UINT4 *  ,UINT4  , UINT4  , INT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2Ieee8021CnRpgMinRate ARG_LIST((UINT4 *  ,UINT4  , UINT4  , INT4  , UINT4  ,UINT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2Ieee8021CnRpGroupTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));
