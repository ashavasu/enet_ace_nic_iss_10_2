/********************************************************************
* Copyright (C) 2010 Aricent Inc . All Rights Reserved
*
* $Id: cnmacs.h,v 1.3 2011/04/18 06:43:48 siva Exp $
*
* Description: This file contains macro definitions used in
*              CN module
**********************************************************************/

#ifndef _CN_MACS_H_
#define _CN_MACS_H_

#define CN_IS_SHUTDOWN()\
    ((gCnMasterTblInfo.u1CnSysControl == CN_SHUTDOWN)?OSIX_TRUE:OSIX_FALSE)

#define CN_GET_APPL_ID(tLldpAppId)\
{\
        tLldpAppId.u2TlvType = CN_TLV_TYPE;\
        tLldpAppId.u1SubType = CN_TLV_SUB_TYPE;\
        MEMCPY(tLldpAppId.au1OUI, gau1CnOUI ,CN_TLV_OUI_LEN);\
}  

#define SENT_EVENT(u1LastSentEvent) gau1CnSentEvent[u1LastSentEvent]

#define RCVD_EVENT(u1LastRcvdEvent) gau1CnRcvdEvent[u1LastRcvdEvent]

#define  CN_OFFSET(x,y)   FSAP_OFFSETOF(x,y)

/* This macro is used to convert the CN compoennt Id
 * to context ID for external modules*/
#define CN_CONVERT_COMP_ID_TO_CXT_ID(u4CompId) (u4CompId - 1)

/* This macro is used to convert the contextId from
 * external modules to CN component Id*/
#define CN_CONVERT_CXT_ID_TO_COMP_ID(u4CompId) (u4CompId + 1)

#define CN_IS_VALID_COMP_ID(u4CompId)\
     (((u4CompId == CN_ZERO) || (u4CompId >= CN_MAX_CONTEXTS))?\
        OSIX_FALSE:OSIX_TRUE)

#define CN_COMP_TBL_ENTRY(u4CompId)\
    ((gCnMasterTblInfo.ppCompTblInfo)?\
     (gCnMasterTblInfo.ppCompTblInfo[u4CompId]):NULL)

#define CN_COMP_PRI_ENTRY(u4CompId, u4Priority, pCnCompPriEntry)\
{\
    if(CN_COMP_TBL_ENTRY(u4CompId) != NULL)\
    {\
        pCnCompPriEntry = CN_COMP_TBL_ENTRY(u4CompId)->pCompPriTbl[u4Priority];\
    }\
    else\
    {\
        pCnCompPriEntry = NULL;\
    }\
}

#define CN_PORT_TBL_ENTRY(u4CompId, u4PortIndex, pCnPortTblEntry)\
{\
    tCnPortTblInfo tempPortTblEntry;\
    MEMSET(&tempPortTblEntry, CN_ZERO, sizeof(tCnPortTblInfo));\
    if(CN_COMP_TBL_ENTRY(u4CompId) != NULL)\
    {\
        if((CN_COMP_TBL_ENTRY(u4CompId)) -> PortTbl != NULL)\
        {\
            tempPortTblEntry.u4ComponentId = u4CompId;\
            tempPortTblEntry.u4PortId = u4PortIndex;\
            pCnPortTblEntry = \
           (tCnPortTblInfo *) RBTreeGet(CN_COMP_TBL_ENTRY(u4CompId) -> PortTbl,\
                                          (tCnPortTblInfo *)&tempPortTblEntry);\
        }\
     }\
    else\
    {\
        pCnPortTblEntry = NULL;\
    }\
}

#define CN_PORT_PRI_ENTRY(u4CompId, u4PortId, u4Priority, pCnPortPriEntry)\
{\
    tCnPortTblInfo *pCnPortTbl = NULL;\
    CN_PORT_TBL_ENTRY(u4CompId, u4PortId, pCnPortTbl);\
    if(pCnPortTbl != NULL)\
    {\
        pCnPortPriEntry = pCnPortTbl -> paPortPriTbl[u4Priority];\
    }\
    else\
    {\
        pCnPortPriEntry = NULL;\
    }\
}

#define CN_PRIO_FROM_CPINDEX(u4CpIndex) (u4CpIndex - 1)
#define CN_CPINDEX_FROM_PRIO(u4Priority) (u4Priority + 1)

#define CN_IS_PORT_CHANNEL(u4Port) \
    (((u4Port > BRG_MAX_PHY_PORTS) && (u4Port <= BRG_MAX_PHY_PLUS_LOG_PORTS)) \
     ? OSIX_TRUE : OSIX_FALSE)

#define CN_FORM_CPID(pau1Portmac, u1CNPV, pCnCpId)\
{\
    MEMCPY(pCnCpId, pau1PortMac, MAC_ADDR_LEN) ;\
    pCnCpId += MAC_ADDR_LEN;\
    *pCnCpId = CN_ZERO;\
    pCnCpId +=1;\
    *pCnCpId = u1CNPV;\
    pCnCpId += 1;\
}
#define CN_PUT_1BYTE(pu1PktBuf, u1Val)    \
    *pu1PktBuf = u1Val;                     \
    pu1PktBuf += 1;

#define CN_PUT_2BYTE(pu1PktBuf, u2Val)    \
    u2Val = OSIX_HTONS(u2Val);       \
    MEMCPY (pu1PktBuf, &u2Val, 2);          \
    pu1PktBuf += 2;

#define CN_PUT_OUI(pu1PktBuf, au1OUI)    \
    MEMCPY (pu1PktBuf, au1OUI, 3);          \
    pu1PktBuf += 3;


#define CN_FORM_TLV_HEADER(u2Type, u2Len, pu2Header)                      \
    *pu2Header = u2Type << (UINT2)CN_TLV_LEN_MAX_BITS;                    \
    *pu2Header = *pu2Header | u2Len;


#endif /* _CN_MACS_H_ */
/*-----------------------------------------------------------------------*/
/*                       End of the file  cnmacs.h                       */
/*-----------------------------------------------------------------------*/

