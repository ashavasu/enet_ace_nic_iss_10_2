/*****************************************************************************
* Copyright (C) 2010 Aricent Inc . All Rights Reserved
*
* $Id: cnglob.h,v 1.2 2010/07/23 13:33:20 prabuc Exp $
*
* Description: It contains Global variables used by CN
*             
******************************************************************************/
#ifndef _CN_GLOB_H_
#define _CN_GLOB_H_ 

tCnMasterTblInfo gCnMasterTblInfo;

UINT1 gau1CnSentEvent[CN_DBG_MAX_TX_EVENTS][CN_MAX_EVENT_NAME_LEN] = {
     "No TLV should be sent",
     "Sent CNPV Indicator",
     "Sent Ready Indicator"};

UINT1 gau1CnRcvdEvent[CN_DBG_MAX_RX_EVENTS][CN_MAX_EVENT_NAME_LEN] = {
      "Aged Out Event Received",
      "CNPV Received from TLV",
      "Ready Received from TLV",
      "TLV Droped due to half duplexity"};

const CHR1 *gau1CnStateStr[CN_MAX_STATES] = {
  "INVALID_STATE",
  "CN_DISABLE",
  "CN_INTERIOR",
  "CN_INTERIOR_READY",
  "CN_EDGE"
};

const CHR1 *gau1CnEvntStr[CN_MAX_EVENTS] = {
  "NOT_RECEIVE_TLV",
  "RECEIVE_TLV",
  "RECEIVE_READY"
};

UINT1    gau1CnOUI[CN_TLV_OUI_LEN]= {0x00,0x80,0xc2};

UINT1    gau1CnPreFormedTlv[CN_PREFORMED_TLV_LEN];


#endif /* _CN_GLOB_H_ */
/*--------------------------------------------------------------------------*/
/*                       End of the file  cnglob.h                          */
/*--------------------------------------------------------------------------*/
