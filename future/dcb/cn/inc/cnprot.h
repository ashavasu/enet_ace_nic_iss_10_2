/********************************************************************
* Copyright (C) 2010 Aricent Inc . All Rights Reserved
*
* $Id: cnprot.h,v 1.7 2010/10/28 11:49:48 prabuc Exp $
*
* Description: This file contains prototypes for functions
*              defined in CN.
**********************************************************************/
#ifndef _CN_PROTO_H
#define _CN_PROTO_H

/*--------------------------------------------------------------------------*/
/*                      cnmain.c                                          */
/*--------------------------------------------------------------------------*/
PUBLIC VOID CnMainAssignMempoolIds       PROTO ((VOID));
PUBLIC INT4 CnMainInitMasterTblInfo      PROTO ((VOID));
/*--------------------------------------------------------------------------*/
/*                       cnmod.c                                           */
/*--------------------------------------------------------------------------*/
PUBLIC VOID CnModuleEnable                 PROTO ((VOID));
PUBLIC VOID CnModuleDisable                PROTO ((VOID));
PUBLIC INT4 CnModuleStart                  PROTO ((VOID));
PUBLIC VOID CnModuleShutdown               PROTO ((VOID));

/*--------------------------------------------------------------------------*/
/*                       cntmr.c                                            */
/*--------------------------------------------------------------------------*/
PUBLIC INT4 CnTmrInit                  PROTO ((VOID));
PUBLIC INT4 CnTmrDeInit                PROTO ((VOID));
PUBLIC VOID CnTmrExpHandler            PROTO ((VOID));

/*--------------------------------------------------------------------------*/
/*                       cnport.c                                           */
/*--------------------------------------------------------------------------*/

PUBLIC INT4 CnPortVcmGetAliasName PROTO ((UINT4 u4ContextId, UINT1 *pu1Alias));
PUBLIC INT4 CnPortLldpApplRegWithL2iwf PROTO ((VOID));
PUBLIC INT4 CnPortLldpApplDeRegWithL2iwf PROTO ((VOID));
PUBLIC INT4 CnPortVcmIsSwitchExist PROTO ((UINT1 *pu1Alias,
                                           UINT4 *pu4ContextId));
PUBLIC INT4 CnPortGetPortDuplexity PROTO ((UINT4 u4PortId, 
                                           INT4 *pi4PortDuplexity));
PUBLIC INT4 CnPortVcmGetCxtInfoFromIfIndex PROTO ((UINT4 u4IfIndex,
                                                   UINT4 *pu4ContextId,
                                                   UINT2 *pu2LocalPortId));
PUBLIC INT4 CnPortPostCnMsgToLldp PROTO ((UINT1 u1MsgType, UINT4 u4IfIndex,
                                          tLldpAppPortMsg *pLldpAppPortMsg));
PUBLIC INT4 CnPortGetTctoPriority  PROTO ((UINT1 u1CNPV, UINT4 u4IfIndex ,
                                            UINT4 * u4TrafficClass));
PUBLIC INT4
CnPortNotifyFaults PROTO ((tSNMP_VAR_BIND * pTrapMsg, UINT1 *pu1Msg
                           , UINT4 u4ModuleId));
PUBLIC VOID
CnPortVcmGetFirstVcId PROTO ((UINT4 *pu4ContextId));
PUBLIC INT4
CnPortVcmGetNextVcId PROTO ((UINT4 u4ContextId, UINT4 *pu4NextContextId));

PUBLIC INT4
CnPortQosPriorityMapReq PROTO ((UINT4 u4CompId, UINT4 u4Port, UINT1 u1InPri,
                                UINT1 *pu1OutPri, UINT1 u1Request));
PUBLIC INT4 
CnCfaCliGetIfName PROTO ((UINT4 u4IfIndex, INT1 *pi1IfName));
PUBLIC INT4
CnPortCfaGetIfInfo PROTO ((UINT4 u4IfIndex, tCfaIfInfo * pIfInfo));
PUBLIC INT4
CnPortIsPortInPortChannel PROTO ((UINT4 u4IfIndex));
/*--------------------------------------------------------------------------*/
/*                       cnif.c                                             */
/*--------------------------------------------------------------------------*/
PUBLIC INT4 CnCxtCreateContext  PROTO ((UINT4 u4ContextId));
PUBLIC INT4 CnCxtDeleteContext  PROTO ((UINT4 u4ContextId));
PUBLIC INT4 CnIfCreate          PROTO ((UINT4 u4ContextId, UINT4 u4PortId));
PUBLIC INT4 CnIfDelete          PROTO ((UINT4 u4PortId));
PUBLIC INT4 CnIfDeletetoLAG     PROTO ((UINT4 u4ContextId, UINT4 u4PortId,
                                         UINT4 u4LagMemberPortId));
PUBLIC VOID CnIfCreateAllPorts  PROTO ((UINT4 u4CompId));
PUBLIC INT4 CnIfAddPortToLAG    PROTO ((UINT4 u4CompId, UINT4 u4PortIndex, 
                                        UINT4 u4PoInedex));
PUBLIC INT4 CnIfDelPortFromLAG  PROTO ((UINT4 u4PortIndex, UINT4 u4PoInedex));
PUBLIC INT4 CnIfPortChannelReadyinHw PROTO ((UINT4 u4ContextId, UINT4 u4PoInedex));
PUBLIC INT4 CnPortVcmGetContextPortList PROTO ((UINT4 u4ContextId, 
                                                tPortList PortList));
/*--------------------------------------------------------------------------*/
/*                       cnutil.c                                           */
/*--------------------------------------------------------------------------*/
PUBLIC INT4 CnLock                     PROTO ((VOID));
PUBLIC INT4 CnUnLock                   PROTO ((VOID));
PUBLIC VOID CnUtilCnpdAdminDefenseMode 
                                PROTO ((tCnPortPriTblInfo *pPortPriTable));

PUBLIC VOID CnUtilMasterEnable          PROTO ((tCnCompTblInfo *pComptbl));
PUBLIC VOID CnUtilMasterDisable         PROTO ((tCnCompTblInfo *pComptbl));
PUBLIC VOID CnUtilCalculateErroredPortEntry  PROTO ((UINT4 u4ContextId, 
                                               UINT4 u4NewCNPV, 
                                               UINT4 u4CompAltPri,
                                               UINT1 u1Status));
PUBLIC INT4 CnUtilAddErroredPortEntry PROTO ((tCnPortPriTblInfo
                                              *pPortPriTable));
PUBLIC INT4 CnUtilDeleteErroredPortEntry PROTO ((tCnPortPriTblInfo
                                                 *pPortPriTable));
PUBLIC VOID CnUtilCalculateOperAltPri PROTO ((tCnPortPriTblInfo 
                                              *pPortPriTable));
PUBLIC VOID CnUtilCalculateAutoAltPri PROTO ((tCnCompTblInfo *pComptbl));
PUBLIC INT4
CnUtilCalculateTlvIndicators PROTO ((tCnCompTblInfo *pCompTbl,
                              tCnPortTblInfo *pCnPortTblEntry,
                              tCnPortPriTblInfo *pPortPriTbl));

PUBLIC VOID CnUtilDeleteRBTrees        PROTO ((VOID));
PUBLIC VOID CnUtilCreatePortTblRBTree PROTO ((UINT4 u4CompId));
PUBLIC INT4 CnUtilRBCmpPortTbl        PROTO ((tRBElem * pRBElem1,
                                             tRBElem * pRBElem2));
PUBLIC INT4 CnUtilRBCmpCpIdTbl        PROTO ((tRBElem * pRBElem1,
                                              tRBElem * pRBElem2));
PUBLIC VOID CnUtilDeleteQMsg          PROTO ((tOsixQId QId,
                                               tMemPoolId PoolId));
PUBLIC VOID CnUtilLldpInstanceChoice PROTO ((tCnPortPriTblInfo 
                                         *pCnPortPriTable,UINT4 *pu4RetVal));
PUBLIC INT4 CnUtilCreateCompPriEntry PROTO ((tCnCompTblInfo *pCnCompTblEntry,
                                             UINT4 u4Priority));
PUBLIC INT4 CnUtilActivateCNPV PROTO ((tCnCompTblInfo *pCompEntry,
                                                   UINT4 u4Priority));
PUBLIC VOID CnUtilDeActivateCNPV PROTO ((tCnCompTblInfo *pCnCompTblEntry, 
                                         UINT4 u4Priority));
PUBLIC INT4 CnUtilDeleteCNPV PROTO ((tCnCompTblInfo *pCnCompTblEntry,
                                     UINT4 u4CompPriority));
PUBLIC VOID CnUtilSetFsCnXPortPriRowStatus PROTO ((UINT4 u4CompId,
                                    UINT4 u4Priority, INT4 i4PortId,
                                             INT4 i4RowStatus));
PUBLIC VOID CnUtilSetFsCnXCpRowStatus PROTO ((UINT4 u4CompId,
                      INT4 i4PortId, UINT4 u4CpIndex,
                                        INT4 i4RowStatus));
PUBLIC INT4 CnUtilTlvConstruct PROTO ((tCnPortTblInfo *pPortTbl, 
                                       UINT1 u1Msg));
PUBLIC VOID CnUtilFillLldpPortMsg PROTO ((tLldpAppPortMsg *pLldpAppPortMsg));
PUBLIC INT4 CnUtilFindTrcOpt PROTO ((UINT1 *pu1TrcStr, INT4 *pi4TrcOpt));
PUBLIC VOID CnUtilConstructPreFormTlv PROTO ((VOID));
PUBLIC INT4 CnUtilCreatePortPriEntry PROTO ((tCnPortTblInfo *pCnPortTblEntry, 
                                   UINT4 u4CNPV, UINT1 u1ComPriDefModeCh, 
                                  UINT4 u4CompAltPri, UINT1 u1ErrorPortFlag));
PUBLIC VOID
CnUtilReCalculateCpProp PROTO ((UINT4 u4CompId, UINT4 u4PortId, UINT4 u4CNPV,
                                UINT4 u4TrafficClass));
PUBLIC VOID
CnUtilActivatePortInCNPV PROTO ((tCnCompTblInfo *pCnCompTblEntry,
                                 UINT4 u4PortId, UINT4 u4Priority));
PUBLIC INT4
CnGetFirstPortInContext PROTO ((UINT4 u4CompId, INT4 *pi4IfIndex));
PUBLIC INT4
CnGetNextPortInContext PROTO ((UINT4 u4CompId, INT4 u4IfIndex,
                               INT4 *pu4NextIfIndex));
PUBLIC VOID
CnUtilEnableCnpv PROTO ((tCnCompTblInfo *pComp,
                         tCnPortTblInfo *pPort,
                         tCnPortPriTblInfo  *pCnPortPriEntry));
PUBLIC VOID
CnUtilUpdateDefInPortPriTable PROTO ((tCnCompPriTbl *pCnCompPriEntry, 
                                                          UINT4 u4PortI));
PUBLIC INT4 
CnUtilCheckQoSPriorityMap PROTO ((tCnCompTblInfo *pComp, UINT4 u4Priority));
PUBLIC INT4 
CnUtilCheckMaxCNPV PROTO ((UINT4 u4CompId));
/*--------------------------------------------------------------------------*/
/*                       cnque.c                                            */
/*--------------------------------------------------------------------------*/
PUBLIC INT4 CnQueEnqMsg             PROTO ((tCnQMsg *pMsg));
PUBLIC VOID CnQueMsgHandler         PROTO ((VOID));

/*--------------------------------------------------------------------------*/
/*                       cnsem.c                                            */
/*--------------------------------------------------------------------------*/
PUBLIC VOID CnSemRunStateMachine    PROTO ((tCnCompTblInfo *pCompEntry,
                                            tCnPortTblInfo *pPortEntry,
                                            tCnPortPriTblInfo *pPortPriTable,
                                                            UINT1 U1Event));

PUBLIC VOID CnSmEventIgnore PROTO ((tCnCompTblInfo *pCompEntry,
                                    tCnPortTblInfo *pPortEntry,
                                    tCnPortPriTblInfo *pPortPriTable));
PUBLIC VOID CnSmStateDisabled PROTO ((tCnCompTblInfo *pCompEntry,
                                      tCnPortTblInfo *pPortEntry,
                                      tCnPortPriTblInfo *pPortPriTable));
PUBLIC VOID CnSmStateEdge PROTO ((tCnCompTblInfo *pCompEntry,
                                  tCnPortTblInfo *pPortEntry,
                                  tCnPortPriTblInfo *pPortPriTable));
PUBLIC VOID CnSmStateInterior PROTO ((tCnCompTblInfo *pCompEntry,
                                      tCnPortTblInfo *pPortEntry,
                                      tCnPortPriTblInfo *pPortPriTable));
PUBLIC VOID CnSmStateInteriorReady PROTO ((tCnCompTblInfo *pCompEntry,
                                           tCnPortTblInfo *pPortEntry,
                                           tCnPortPriTblInfo *pPortPriTable));

/*--------------------------------------------------------------------------*/
/*                       cntrc.c                                            */
/*--------------------------------------------------------------------------*/

PUBLIC VOID CnTrace PROTO ((UINT4 Comp, UINT4 TraceType, const char *fmt, ...)); 
PUBLIC VOID CnTlvDump PROTO ((UINT4 Comp, UINT4 TraceType, UINT1 *pBuf, 
                                    UINT1 u1len));


/*--------------------------------------------------------------------------*/
/*                       cnhwwr.c                                           */
/*--------------------------------------------------------------------------*/
PUBLIC INT4
CnHwSetDefenseMode PROTO ((UINT4 u4CompId, UINT4 u4PortId, UINT1 u1CNPV,
                           UINT1 u1DefenseMode));

PUBLIC INT4 
CnHwSetCpParams PROTO ((tCnPortPriTblInfo  *pCnPortPriEntry));

PUBLIC INT4 
CnHwSetCnmPri PROTO ((UINT4 u4CompId, UINT1 u1CnmPri));

PUBLIC INT4 
CnHwReSetCounters PROTO ((UINT4 u4CompId, UINT4 u4PortId, UINT1 u1Priority));

PUBLIC INT4 
CnHwGetCounters PROTO ((UINT4 u4CompId, UINT4 u4PortId, UINT1 u1CNPV,
                        tSNMP_COUNTER64_TYPE *pDisFrames,
                        tSNMP_COUNTER64_TYPE *pTransFrames,
                        tSNMP_COUNTER64_TYPE *pTransCnms));
#endif /* _CN_PROTO_H */
/*--------------------------------------------------------------------------*/
/*                       End of the file  cnprot.h                          */
/*--------------------------------------------------------------------------*/


