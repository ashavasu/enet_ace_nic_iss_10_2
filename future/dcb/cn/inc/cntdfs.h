/*****************************************************************************
* Copyright (C) 2010 Aricent Inc . All Rights Reserved
*
* $Id: cntdfs.h,v 1.2 2010/07/27 10:50:37 prabuc Exp $
*
* Description: This file contains the data structure definition for CN module
******************************************************************************/
#ifndef _CN_TDFS_H
#define _CN_TDFS_H
/* Message type enqueued to CN */
enum 
{  
    /*CN queue will receive application event from lldp*/
    CN_CREATE_CONTEXT_MSG = L2IWF_LLDP_APPL_MAX_MSG,
    CN_DELETE_CONTEXT_MSG,
    CN_CREATE_PORT_MSG,
    CN_DELETE_PORT_MSG,
    CN_MAP_PORT_MSG,
    CN_UNMAP_PORT_MSG,
    CN_ADD_LAG_MEMBER_MSG,
    CN_REM_LAG_MEMBER_MSG,
    CN_CNM_GENERATE_MSG,
    CN_PO_READY_IN_HW_MSG,
    CN_RM_MSG
};

/*CN Timer Types*/
enum
{
    CN_TLV_TMR = 0,
    CN_MAX_TIMER_TYPES = 1
};

/* CN timer structure */
typedef struct _CnTmr {
    tTmrBlk    CnTmrBlk;
    UINT1      u1Status;
    UINT1      au1Padding[3];
}tCnTmr;

/* CN Queue Messages - Size of this struct needs to verify */
typedef struct _cnQMsg
{
    UINT4          u4MsgType;
    UINT4          u4PortId; /* Physical or LAG Id */
    UINT4           u4ContextId;
    union {
        UINT4           u4LagMemberPortId; /* LAG Member port No */
        UINT1           au1CNTlv[CN_TLV_LEN_PADDED]; /* CN TLV from LLDP */
        tCnCnmGen       CnCnmGen; /*NPAPI CNM generation information*/
    }unMsgParam;
}tCnQMsg;

/* Component Priority Table */
typedef struct _cnCompPriTbl
{
    UINT4 u4CompId;              /* Component or Context Id */
    UINT4 u4ComPriLldpInstSelector;
                                 /* implies a default value
                                    for which lldp instance
                                    is to be used */
    UINT1 u1CNPV;                /* CN Priority Value */
    UINT1 u1ComPriDefModeChoice; /* DefenseModeChoice */
    UINT1 u1ComPriAltPri;        /* Alternate priority */
    UINT1 u1ComPriAutoAltPri;    /*alternate priority to be used 
                                   in autodefense mode*/
    UINT1 u1ComPriAdminDefMode;  /* Admin Defense Mode*/
    UINT1 u1ComPriLldpInstChoice;/*LldpInstanceChoice*/
    UINT1 u1DefaultPortDefModeChoice; 
                                 /* Default port default
                                    defense mode choice.
                                    MIB object: CnComPriCreation */

    UINT1 u1ComPriRowStatus;     /*RowStatus*/

}tCnCompPriTbl;

/* Port Priority Table - Contains Port-CNPV and Congestion Point Properties */
typedef struct _cnPortPriTbl
{
    tRBNodeEmbd   NextCpIdNode;          /* Node in CpIdTbl with CPID 
                                            as Index */
    UINT4         u4CnComPriComponentId; /* context id*/
    UINT4         u4PortId;              /* Port Id - Physical and LAG ID */
    UINT4         u4LastEvtRcvdTime;     /* Last event received time */
    UINT4         u4LastEvtSentTime;     /* Last event sent time */
    UINT4         u4CpQSizeSetPoint;     /* CP Property */
    UINT4         u4CpMinSampleBase;     /* CP Property */
    UINT4         u4TrafficClass;        /*Traffic class corresponding to the 
                                           priority*/
    UINT4         u4PortPriLldpInstSelector; 
                                         /* Port-Pri LLDP Inst. Selector */
    UINT1         au1CnCpIdentifier[CN_CPID_LEN];   
                                         /*CP Identifier */
    UINT1         u1LastRcvdEvent;       /* Event received by CN module */
    UINT1         u1LastSentEvent;       /* Event sent from CN module */
    UINT1         u1CNPV;                /* cnpv */
    UINT1         u1PortPriDefModeChoice; /* Port-Pri Defense mode choice*/
    UINT1         u1PortPriAdminDefMode;  /* Port-Pri ADM defense mode */
    UINT1         u1PortPriOperDefMode;   /* Operation defense mode */
    UINT1         u1PortPriLldpInstChoice;/* Port-Pri Inst. choice */
    UINT1         u1PortPriAdminAltPri;   /* Admin Alternate Priority */
    UINT1         u1PortPriOperAltPri;    /* Operational alt. priority */
    BOOL1         bIsErrorPortEntry;      /* Is this entry an errored entry*/
    BOOL1         bXmitCnpvCapable;       /*Flag to set the CNPV indicator */
    BOOL1         bXmitReady;             /*Flag to set the ready indicator*/
    BOOL1         bXmitDoesEdge;          /*Does bridge support EDGE - flag*/
    BOOL1         bIsAdminDefMode;        /* Is defense mode controlled by 
                                                admin values */
    UINT1         u1OperAdminDefenseMode; /* Defense mode to be used when 
                                                controlled by admin values*/
    UINT1         u1CnCpPriority;         /* Lowest CNPV that this i
                                                CP serves*/
    UINT1         u1CpMinHeaderOctets;    /* CP Property */
    INT1          i1CnCpFeedbackWeight;   /* CP property */
    UINT1         au1Padding[2];
}tCnPortPriTblInfo;

/* Component table - Global Information for each components  */
typedef struct _cnCompTblInfo
{
    tCnCompPriTbl      *pCompPriTbl[CN_MAX_VLAN_PRIORITY]; 
                                      /* Pointers to each
                                         priority in the
                                         system */
    tRBTree            PortTbl;       /* No. of Ports in the component.
                                         Embedded RB Tree Head for
                                         tCnPortTblInfo structure with
                                         index as Port Id */
    UINT4              u4CompId;      /*Component or context Id*/
    UINT4              u4TraceLevel;  /*Trace level*/
    UINT4              u4TlvErrors;   /* No. of CN TLVs dropped */
    UINT1              u1CnmTransmitPriority;
                                        /* VLAN priority of CNMs */
    BOOL1              bCnCompMasterEnable; 
                                      /* Context level functioanlity 
                                         enable or diable*/
    UINT1              au1Padding[2];
}tCnCompTblInfo;

/* Port table - Contains details of each port of a component */
typedef struct _cnPortTblInfo
{
    tRBNodeEmbd        NextPortNode;
    tCnTmr             TlvTxDelayTmrNode; 
                               /* Tlv TX timer */
    tCnPortPriTblInfo  *paPortPriTbl[CN_MAX_VLAN_PRIORITY]; 
                               /* Port Priority Tbl. This table will be 
                                * scanned for Errored port table also */ 
    UINT4 u4PortId;
    UINT4 u4ComponentId;       /* Virtual Context Id */
    UINT1 u1CnpvreadyIndicator;/* Bitmap for each cnpv with ready indicator*/
    UINT1 u1CnpvIndicator;     /* Bitmap for each cnpv present in the port*/
    UINT1 au1Padding[2];
}tCnPortTblInfo;


/* CN Module Master table Information */
typedef struct _cnMasterTblInfo
{
    tOsixTaskId        CnTaskId;
    tOsixQId           cnTaskQId;
    tOsixSemId         SemId;
    tCnCompTblInfo     **ppCompTblInfo; /* List of components in the system */
    tRBTree            CpIdTbl;         /* CPID to Component, Port, CpIndex 
                                          mapping table. Head of Embedded 
                                          RBTree contains tCnPortPriTblInfo */
    tMemPoolId         CompTblPoolId;   /* Mem. pool of tCnCompTblInfo */
    tMemPoolId         CompPriTblPoolId;/* Mem. pool of tCnCompPriTbl */
    tMemPoolId         PortTblPoolId;   /* Mem pool of tCnPortTblInfo */
    tMemPoolId         PortPriTblPoolId;/* Mem pool of tCnPortPriTblInfo */
    tMemPoolId         QMsgPoolId;      /* Mem pool of tCnQMsg */
    tMemPoolId         pCompPriTblPoolId; /*Memory Pool for component table pointer*/
    tTmrDesc           aTmrDesc[CN_MAX_TIMER_TYPES];
                                        /*timer array contains
                                         * function pointers */
    tTimerListId       TmrListId;       /*Timer List ID*/
    INT4               i4TlvTxDelay;    /*Timer value*/
    UINT4              u4GlobalTrapStatus; 
                                        /* CN TRAP status */
    UINT4              u4SysLogId;
    UINT1              u1CnSysControl;  /* CN system control */
    UINT1              au1Padding[3];
}tCnMasterTblInfo;

#endif
/* End of file */
