/********************************************************************
* Copyright (C) 2010 Aricent Inc . All Rights Reserved
*
* $Id: cndefn.h,v 1.7 2015/09/04 05:36:56 siva Exp $
*
* Description: This file contains macros definitions for constants
*              used in CN.
*********************************************************************/
#ifndef _CN_DEFN_H
#define _CN_DEFN_H

#define CN_TASK_NAME                ((const UINT1 *)"CNTK")
#define CN_MSG_QUEUE_NAME           ((UINT1 *)"CNTQ")
#define CN_PROTO_SEM                ((UINT1 *)"CNSM")

/* Events that will be received by CN Task */
#define CN_QMSG_EVENT               0x01 /* Bit 0 */
#define CN_TMR_EXPIRY_EVENT         0x02 /* Bit 1*/
#define CN_ALL_EVENTS  (CN_TMR_EXPIRY_EVENT | CN_QMSG_EVENT)    
#define CN_ZERO                     0 /*To use zero in the code*/
#define CN_ONE                      1 /*To use one in the code*/
#define CN_MAX_SYSLOG_MSG           4 /*Max range for gac1CnSysLogMsg array*/

/* Clear Counters Set and Reset */
#define CN_SET_CLEAR_COUNTER       1
#define CN_RESET_CLEAR_COUNTER     2

/* Set and Reset ErrorPortEntry */
#define CN_ERROR_PORT_ENTRY_SET     1
#define CN_ERROR_PORT_ENTRY_RESET   2

/* CN Module default parameters */
#define CN_DEFAULT_MODULE_STATUS    CN_MASTER_DISABLE
#define CN_DEFAULT_TRACE_LEVEL      CN_ALL_FAILURE_TRC
#define CN_DEFAULT_CNM_PRI            6
#define CN_DEFAULT_TRAP             3 /*Default trap value*/
#define CN_DEFAULT_CONTEXT          CN_MIN_CONTEXT

/*SNMP TRAP OID*/
#define CN_SNMP_TRAP_OID { 1, 3, 6, 1, 6, 3, 1, 1, 4, 1, 0 }

/*Default CP Params */
#define CN_DEFAULT_QUEUE_SET_PT         26000
#define CN_DEFAULT_MIN_SAMPLE_BASE      150000
#define CN_DEFAULT_FEEDBACK_WEIGHT      1
#define CN_DEFAULT_HEADER_OCTETS        0
/*Default values for Lldp Instance Choice */
#define CN_DEF_COMP_LLDP_INST_SELECTOR     1
#define CN_DEF_PORT_LLDP_INST_SELECTOR     3

/* Boundary values for Lldp Instance Choice */
#define CN_MAX_LLDP_INST_SELECTOR 4096
#define CN_MIN_LLDP_INST_SELECTOR 1

/* Maximum number of interfaces maintained in CN */
#define CN_MIN_PORTS                1
#define CN_MAX_PORTS                BRG_MAX_PHY_PLUS_LOG_PORTS
#define CN_MIN_CNPV                 0 
#define CN_MAX_VLAN_PRIORITY        8
#define CN_MAX_CNPV                 7  /* Maximum no. of CNPVs that can
                                          be created in the system. */

#define CN_MIN_CONTEXT              1
#define CN_MAX_CONTEXTS             (FsCNSizingParams[MAX_CN_COMP_TABLE_PTRS_SIZING_ID].u4PreAllocatedUnits)
#define CN_MAX_MSGQ_DEPTH           (CN_MAX_PORTS/4) /* Nominal value */
#define CN_MAX_CNCPINDEX            4096 /*maximum possible value*/
#define CN_INVALID_VALUE            -1 
/*maximum valuse of queue set point*/
#define CN_MIN_QUEUE_SET_PT         100
#define CN_MAX_QUEUE_SET_PT         4294967295

 /*maximum and minimum value of the minimum sample base*/
#define CN_MAX_MIN_SAMPLE_BASE      4294967295 
#define CN_MIN_MIN_SAMPLE_BASE      10000

/*MI port related macros*/
#define CN_IFPORT_LIST_SIZE         BRG_PORT_LIST_SIZE

#define CN_PORTS_PER_BYTE           8
#define CN_BIT8                     0x80

/*Cn Timer Info*/
#define CN_TMR_RUNNING              1
#define CN_TMR_NOT_RUNNING          2
#define CN_TMR_EXPIRED              3
#define CN_TLV_TX_DELAY             2

/*CN TLV INFO*/
#define CN_TLV_TYPE                 127
#define CN_PREFORMED_TLV_LEN        6
#define CN_TLV_SUB_TYPE             13 /*Changing the CN_TLV sub_type to 13 as per IEEE std */
#define CN_TLV_OUI_LEN              3
#define CN_TLV_BIT_ZERO             0x00
#define CN_TLV_BIT_ONE              0x01
#define CN_TLV_LEN                  8
#define CN_TLV_LEN_MAX_BITS         9

/*Maximum value for header octets */
#define CN_MAX_HEADER_OCTETS        64

/*MAximum and Minimum values for Feedback weight */
#define CN_MIN_FEEDBACK_WEIGHT      (-10)
#define CN_MAX_FEEDBACK_WEIGHT      10

/*date length*/
#define MAX_DATE_LEN             100

/*CN Duplexity*/
#define CN_DUP_HALF                 ISS_HALFDUP 
#define CN_DUP_FULL                 ISS_FULLDUP            

/*CN Traces*/
#define CN_MAX_TRC_LEN             1024
#define CN_INVALID_CONTEXT         0xFFFFFFFF

#define CN_DISABLE_REMAPPING       1 /*disable remapping*/
#define CN_TURN_ON                 2 /*Turn on defense mode*/
#define CN_TURN_OFF                3 /*Turn off defense mdoe*/

#define CN_TLV_LEN_PADDED          (((CN_TLV_LEN + 3) / 4) * 4) 
#define CN_MAX_EVENT_NAME_LEN      33     

/*This macro is used to set cnpv and ready indicator*/
/*since this is a bridge is always true*/
#define CNPD_ACCEPTS_CNTAG         OSIX_TRUE 

/* Queue memory pool*/
#define CN_QMSG_MEMBLK_SIZE               sizeof(tCnQMsg)
#define CN_QMSG_MEMBLK_COUNT              CN_MAX_MSGQ_DEPTH

/*Component Table memory pool*/
#define CN_COMP_INFO_MEMBLK_SIZE          sizeof(tCnCompTblInfo)
#define CN_COMP_INFO_MEMBLK_COUNT         CN_MAX_CONTEXTS

/*Component priority table  memory pool*/
#define CN_COMP_PRI_INFO_MEMBLK_SIZE      sizeof(tCnCompPriTbl)
#define CN_COMP_PRI_INFO_MEMBLK_COUNT    (CN_MAX_CONTEXTS * CN_MAX_CNPV)

/*Port table  memory pool*/
#define CN_PORT_INFO_MEMBLK_SIZE          sizeof(tCnPortTblInfo)
#define CN_PORT_INFO_MEMBLK_COUNT         CN_MAX_PORTS

/*Port priority memory pool*/
#define CN_PORT_PRI_INFO_MEMBLK_SIZE      sizeof (tCnPortPriTblInfo)
#define CN_PORT_PRI_INFO_MEMBLK_COUNT     (CN_MAX_PORTS * CN_MAX_CNPV)

/*Port component table pointer memory pool*/
#define CN_COMP_TBL_PTR_MEMBLK_SIZE    sizeof(tCnCompTblInfo *)
#define CN_COMP_TBL_PTR_MEMBLK_COUNT   CN_MAX_CONTEXTS


/*CN RB Tree */
#define CN_RB_GREATER                     1
#define CN_RB_EQUAL                       0
#define CN_RB_LESS                        -1

/*CN ErrorPort Related Macros*/
#define CN_CNPV_CREATE                   1
#define CN_CNPV_DELETE                   2 

/*CN-LLDP Mseeage Types*/
#define CN_MSG_PORT_REG                  1
#define CN_MSG_PORT_UPDATE               2
#define CN_MSG_PORT_DEREG                3

/*CN ROW STATUS VALUES */
#define   CN_ACTIVE                      ACTIVE
#define   CN_NOT_IN_SERVICE              NOT_IN_SERVICE
#define   CN_NOT_READY                   NOT_READY
#define   CN_CREATE_AND_GO               CREATE_AND_GO
#define   CN_CREATE_AND_WAIT             CREATE_AND_WAIT
#define   CN_DESTROY                     DESTROY

/*CN State Machine Events*/
enum
{  CN_NOT_RECEIVE_TLV = 0,
   CN_RECEIVE_TLV = 1,
   CN_RECEIVE_READY = 2,
   CN_MAX_EVENTS = 3
};

/* CN Last Event Received - These enums are used for capturing the
 * TX and RX events for debuggin purpose. */
enum
{
    CN_DBG_NOT_RECEIVE_TLV = 0,
    CN_DBG_RECEIVE_TLV = 1,
    CN_DBG_RECEIVE_READY = 2,
    CN_DBG_TLV_DROP_FOR_HALFDUPLEX = 3,
    CN_DBG_MAX_RX_EVENTS =4
};


/*CN State Machine Sent Events*/
enum
{
    CN_DBG_NO_TLV_SENT = 0,
    CN_DBG_SENT_TLV = 1,
    CN_DBG_SENT_READY = 2,
    CN_DBG_MAX_TX_EVENTS = 3
};
#endif 
/*-----------------------------------------------------------------------*/
/*                       End of the file  cndefn.h                       */
/*-----------------------------------------------------------------------*/











