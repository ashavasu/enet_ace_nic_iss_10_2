/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: stdcndb.h,v 1.2 2013/06/15 13:49:33 siva Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/
#ifndef _STDCNDB_H
#define _STDCNDB_H

UINT1 Ieee8021CnGlobalTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32};
UINT1 Ieee8021CnErroredPortTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_INTEGER};
UINT1 Ieee8021CnCompntPriTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 Ieee8021CnPortPriTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_INTEGER};
UINT1 Ieee8021CnCpTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 Ieee8021CnCpidToInterfaceTableINDEX [] = {SNMP_FIXED_LENGTH_OCTET_STRING ,8};
UINT1 Ieee8021CnRpPortPriTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_INTEGER};
UINT1 Ieee8021CnRpGroupTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_UNSIGNED32};

UINT4 stdcn [] ={1,3,111,2,802,1,1,18};
tSNMP_OID_TYPE stdcnOID = {8, stdcn};


UINT4 Ieee8021CnGlobalComponentId [ ] ={1,3,111,2,802,1,1,18,1,1,1,1};
UINT4 Ieee8021CnGlobalMasterEnable [ ] ={1,3,111,2,802,1,1,18,1,1,1,2};
UINT4 Ieee8021CnGlobalCnmTransmitPriority [ ] ={1,3,111,2,802,1,1,18,1,1,1,3};
UINT4 Ieee8021CnGlobalDiscardedFrames [ ] ={1,3,111,2,802,1,1,18,1,1,1,4};
UINT4 Ieee8021CnEpComponentId [ ] ={1,3,111,2,802,1,1,18,1,2,1,1};
UINT4 Ieee8021CnEpPriority [ ] ={1,3,111,2,802,1,1,18,1,2,1,2};
UINT4 Ieee8021CnEpIfIndex [ ] ={1,3,111,2,802,1,1,18,1,2,1,3};
UINT4 Ieee8021CnComPriComponentId [ ] ={1,3,111,2,802,1,1,18,1,3,1,1};
UINT4 Ieee8021CnComPriPriority [ ] ={1,3,111,2,802,1,1,18,1,3,1,2};
UINT4 Ieee8021CnComPriDefModeChoice [ ] ={1,3,111,2,802,1,1,18,1,3,1,3};
UINT4 Ieee8021CnComPriAlternatePriority [ ] ={1,3,111,2,802,1,1,18,1,3,1,4};
UINT4 Ieee8021CnComPriAutoAltPri [ ] ={1,3,111,2,802,1,1,18,1,3,1,5};
UINT4 Ieee8021CnComPriAdminDefenseMode [ ] ={1,3,111,2,802,1,1,18,1,3,1,6};
UINT4 Ieee8021CnComPriCreation [ ] ={1,3,111,2,802,1,1,18,1,3,1,7};
UINT4 Ieee8021CnComPriLldpInstanceChoice [ ] ={1,3,111,2,802,1,1,18,1,3,1,8};
UINT4 Ieee8021CnComPriLldpInstanceSelector [ ] ={1,3,111,2,802,1,1,18,1,3,1,9};
UINT4 Ieee8021CnComPriRowStatus [ ] ={1,3,111,2,802,1,1,18,1,3,1,10};
UINT4 Ieee8021CnPortPriComponentId [ ] ={1,3,111,2,802,1,1,18,1,4,1,1};
UINT4 Ieee8021CnPortPriority [ ] ={1,3,111,2,802,1,1,18,1,4,1,2};
UINT4 Ieee8021CnPortPriIfIndex [ ] ={1,3,111,2,802,1,1,18,1,4,1,3};
UINT4 Ieee8021CnPortPriDefModeChoice [ ] ={1,3,111,2,802,1,1,18,1,4,1,4};
UINT4 Ieee8021CnPortPriAdminDefenseMode [ ] ={1,3,111,2,802,1,1,18,1,4,1,5};
UINT4 Ieee8021CnPortPriAutoDefenseMode [ ] ={1,3,111,2,802,1,1,18,1,4,1,6};
UINT4 Ieee8021CnPortPriLldpInstanceChoice [ ] ={1,3,111,2,802,1,1,18,1,4,1,7};
UINT4 Ieee8021CnPortPriLldpInstanceSelector [ ] ={1,3,111,2,802,1,1,18,1,4,1,8};
UINT4 Ieee8021CnPortPriAlternatePriority [ ] ={1,3,111,2,802,1,1,18,1,4,1,9};
UINT4 Ieee8021CnCpComponentId [ ] ={1,3,111,2,802,1,1,18,1,5,1,1};
UINT4 Ieee8021CnCpIfIndex [ ] ={1,3,111,2,802,1,1,18,1,5,1,2};
UINT4 Ieee8021CnCpIndex [ ] ={1,3,111,2,802,1,1,18,1,5,1,3};
UINT4 Ieee8021CnCpPriority [ ] ={1,3,111,2,802,1,1,18,1,5,1,4};
UINT4 Ieee8021CnCpMacAddress [ ] ={1,3,111,2,802,1,1,18,1,5,1,5};
UINT4 Ieee8021CnCpIdentifier [ ] ={1,3,111,2,802,1,1,18,1,5,1,6};
UINT4 Ieee8021CnCpQueueSizeSetPoint [ ] ={1,3,111,2,802,1,1,18,1,5,1,7};
UINT4 Ieee8021CnCpFeedbackWeight [ ] ={1,3,111,2,802,1,1,18,1,5,1,8};
UINT4 Ieee8021CnCpMinSampleBase [ ] ={1,3,111,2,802,1,1,18,1,5,1,9};
UINT4 Ieee8021CnCpDiscardedFrames [ ] ={1,3,111,2,802,1,1,18,1,5,1,10};
UINT4 Ieee8021CnCpTransmittedFrames [ ] ={1,3,111,2,802,1,1,18,1,5,1,11};
UINT4 Ieee8021CnCpTransmittedCnms [ ] ={1,3,111,2,802,1,1,18,1,5,1,12};
UINT4 Ieee8021CnCpMinHeaderOctets [ ] ={1,3,111,2,802,1,1,18,1,5,1,13};
UINT4 Ieee8021CnCpidToIfCpid [ ] ={1,3,111,2,802,1,1,18,1,6,1,1};
UINT4 Ieee8021CnCpidToIfComponentId [ ] ={1,3,111,2,802,1,1,18,1,6,1,2};
UINT4 Ieee8021CnCpidToIfIfIndex [ ] ={1,3,111,2,802,1,1,18,1,6,1,3};
UINT4 Ieee8021CnCpidToIfCpIndex [ ] ={1,3,111,2,802,1,1,18,1,6,1,4};
UINT4 Ieee8021CnRpPortPriComponentId [ ] ={1,3,111,2,802,1,1,18,1,7,1,1};
UINT4 Ieee8021CnRpPortPriPriority [ ] ={1,3,111,2,802,1,1,18,1,7,1,2};
UINT4 Ieee8021CnRpPortPriIfIndex [ ] ={1,3,111,2,802,1,1,18,1,7,1,3};
UINT4 Ieee8021CnRpPortPriMaxRps [ ] ={1,3,111,2,802,1,1,18,1,7,1,4};
UINT4 Ieee8021CnRpPortPriCreatedRps [ ] ={1,3,111,2,802,1,1,18,1,7,1,5};
UINT4 Ieee8021CnRpPortPriCentiseconds [ ] ={1,3,111,2,802,1,1,18,1,7,1,6};
UINT4 Ieee8021CnRpgComponentId [ ] ={1,3,111,2,802,1,1,18,1,8,1,1};
UINT4 Ieee8021CnRpgPriority [ ] ={1,3,111,2,802,1,1,18,1,8,1,2};
UINT4 Ieee8021CnRpgIfIndex [ ] ={1,3,111,2,802,1,1,18,1,8,1,3};
UINT4 Ieee8021CnRpgIdentifier [ ] ={1,3,111,2,802,1,1,18,1,8,1,4};
UINT4 Ieee8021CnRpgEnable [ ] ={1,3,111,2,802,1,1,18,1,8,1,5};
UINT4 Ieee8021CnRpgTimeReset [ ] ={1,3,111,2,802,1,1,18,1,8,1,6};
UINT4 Ieee8021CnRpgByteReset [ ] ={1,3,111,2,802,1,1,18,1,8,1,7};
UINT4 Ieee8021CnRpgThreshold [ ] ={1,3,111,2,802,1,1,18,1,8,1,8};
UINT4 Ieee8021CnRpgMaxRate [ ] ={1,3,111,2,802,1,1,18,1,8,1,9};
UINT4 Ieee8021CnRpgAiRate [ ] ={1,3,111,2,802,1,1,18,1,8,1,10};
UINT4 Ieee8021CnRpgHaiRate [ ] ={1,3,111,2,802,1,1,18,1,8,1,11};
UINT4 Ieee8021CnRpgGd [ ] ={1,3,111,2,802,1,1,18,1,8,1,12};
UINT4 Ieee8021CnRpgMinDecFac [ ] ={1,3,111,2,802,1,1,18,1,8,1,13};
UINT4 Ieee8021CnRpgMinRate [ ] ={1,3,111,2,802,1,1,18,1,8,1,14};




tMbDbEntry stdcnMibEntry[]= {

{{12,Ieee8021CnGlobalComponentId}, GetNextIndexIeee8021CnGlobalTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, Ieee8021CnGlobalTableINDEX, 1, 0, 0, NULL},

{{12,Ieee8021CnGlobalMasterEnable}, GetNextIndexIeee8021CnGlobalTable, Ieee8021CnGlobalMasterEnableGet, Ieee8021CnGlobalMasterEnableSet, Ieee8021CnGlobalMasterEnableTest, Ieee8021CnGlobalTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Ieee8021CnGlobalTableINDEX, 1, 0, 0, NULL},

{{12,Ieee8021CnGlobalCnmTransmitPriority}, GetNextIndexIeee8021CnGlobalTable, Ieee8021CnGlobalCnmTransmitPriorityGet, Ieee8021CnGlobalCnmTransmitPrioritySet, Ieee8021CnGlobalCnmTransmitPriorityTest, Ieee8021CnGlobalTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Ieee8021CnGlobalTableINDEX, 1, 0, 0, NULL},

{{12,Ieee8021CnGlobalDiscardedFrames}, GetNextIndexIeee8021CnGlobalTable, Ieee8021CnGlobalDiscardedFramesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER64, SNMP_READONLY, Ieee8021CnGlobalTableINDEX, 1, 0, 0, NULL},

{{12,Ieee8021CnEpComponentId}, GetNextIndexIeee8021CnErroredPortTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, Ieee8021CnErroredPortTableINDEX, 3, 0, 0, NULL},

{{12,Ieee8021CnEpPriority}, GetNextIndexIeee8021CnErroredPortTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, Ieee8021CnErroredPortTableINDEX, 3, 0, 0, NULL},

{{12,Ieee8021CnEpIfIndex}, GetNextIndexIeee8021CnErroredPortTable, Ieee8021CnEpIfIndexGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Ieee8021CnErroredPortTableINDEX, 3, 0, 0, NULL},

{{12,Ieee8021CnComPriComponentId}, GetNextIndexIeee8021CnCompntPriTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, Ieee8021CnCompntPriTableINDEX, 2, 0, 0, NULL},

{{12,Ieee8021CnComPriPriority}, GetNextIndexIeee8021CnCompntPriTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, Ieee8021CnCompntPriTableINDEX, 2, 0, 0, NULL},

{{12,Ieee8021CnComPriDefModeChoice}, GetNextIndexIeee8021CnCompntPriTable, Ieee8021CnComPriDefModeChoiceGet, Ieee8021CnComPriDefModeChoiceSet, Ieee8021CnComPriDefModeChoiceTest, Ieee8021CnCompntPriTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Ieee8021CnCompntPriTableINDEX, 2, 0, 0, "2"},

{{12,Ieee8021CnComPriAlternatePriority}, GetNextIndexIeee8021CnCompntPriTable, Ieee8021CnComPriAlternatePriorityGet, Ieee8021CnComPriAlternatePrioritySet, Ieee8021CnComPriAlternatePriorityTest, Ieee8021CnCompntPriTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Ieee8021CnCompntPriTableINDEX, 2, 0, 0, "0"},

{{12,Ieee8021CnComPriAutoAltPri}, GetNextIndexIeee8021CnCompntPriTable, Ieee8021CnComPriAutoAltPriGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Ieee8021CnCompntPriTableINDEX, 2, 0, 0, NULL},

{{12,Ieee8021CnComPriAdminDefenseMode}, GetNextIndexIeee8021CnCompntPriTable, Ieee8021CnComPriAdminDefenseModeGet, Ieee8021CnComPriAdminDefenseModeSet, Ieee8021CnComPriAdminDefenseModeTest, Ieee8021CnCompntPriTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Ieee8021CnCompntPriTableINDEX, 2, 0, 0, "2"},

{{12,Ieee8021CnComPriCreation}, GetNextIndexIeee8021CnCompntPriTable, Ieee8021CnComPriCreationGet, Ieee8021CnComPriCreationSet, Ieee8021CnComPriCreationTest, Ieee8021CnCompntPriTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Ieee8021CnCompntPriTableINDEX, 2, 0, 0, "1"},

{{12,Ieee8021CnComPriLldpInstanceChoice}, GetNextIndexIeee8021CnCompntPriTable, Ieee8021CnComPriLldpInstanceChoiceGet, Ieee8021CnComPriLldpInstanceChoiceSet, Ieee8021CnComPriLldpInstanceChoiceTest, Ieee8021CnCompntPriTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Ieee8021CnCompntPriTableINDEX, 2, 0, 0, "2"},

{{12,Ieee8021CnComPriLldpInstanceSelector}, GetNextIndexIeee8021CnCompntPriTable, Ieee8021CnComPriLldpInstanceSelectorGet, Ieee8021CnComPriLldpInstanceSelectorSet, Ieee8021CnComPriLldpInstanceSelectorTest, Ieee8021CnCompntPriTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Ieee8021CnCompntPriTableINDEX, 2, 0, 0, "1"},

{{12,Ieee8021CnComPriRowStatus}, GetNextIndexIeee8021CnCompntPriTable, Ieee8021CnComPriRowStatusGet, Ieee8021CnComPriRowStatusSet, Ieee8021CnComPriRowStatusTest, Ieee8021CnCompntPriTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Ieee8021CnCompntPriTableINDEX, 2, 0, 1, NULL},

{{12,Ieee8021CnPortPriComponentId}, GetNextIndexIeee8021CnPortPriTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, Ieee8021CnPortPriTableINDEX, 3, 0, 0, NULL},

{{12,Ieee8021CnPortPriority}, GetNextIndexIeee8021CnPortPriTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, Ieee8021CnPortPriTableINDEX, 3, 0, 0, NULL},

{{12,Ieee8021CnPortPriIfIndex}, GetNextIndexIeee8021CnPortPriTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, Ieee8021CnPortPriTableINDEX, 3, 0, 0, NULL},

{{12,Ieee8021CnPortPriDefModeChoice}, GetNextIndexIeee8021CnPortPriTable, Ieee8021CnPortPriDefModeChoiceGet, Ieee8021CnPortPriDefModeChoiceSet, Ieee8021CnPortPriDefModeChoiceTest, Ieee8021CnPortPriTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Ieee8021CnPortPriTableINDEX, 3, 0, 0, "3"},

{{12,Ieee8021CnPortPriAdminDefenseMode}, GetNextIndexIeee8021CnPortPriTable, Ieee8021CnPortPriAdminDefenseModeGet, Ieee8021CnPortPriAdminDefenseModeSet, Ieee8021CnPortPriAdminDefenseModeTest, Ieee8021CnPortPriTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Ieee8021CnPortPriTableINDEX, 3, 0, 0, "1"},

{{12,Ieee8021CnPortPriAutoDefenseMode}, GetNextIndexIeee8021CnPortPriTable, Ieee8021CnPortPriAutoDefenseModeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Ieee8021CnPortPriTableINDEX, 3, 0, 0, NULL},

{{12,Ieee8021CnPortPriLldpInstanceChoice}, GetNextIndexIeee8021CnPortPriTable, Ieee8021CnPortPriLldpInstanceChoiceGet, Ieee8021CnPortPriLldpInstanceChoiceSet, Ieee8021CnPortPriLldpInstanceChoiceTest, Ieee8021CnPortPriTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Ieee8021CnPortPriTableINDEX, 3, 0, 0, "3"},

{{12,Ieee8021CnPortPriLldpInstanceSelector}, GetNextIndexIeee8021CnPortPriTable, Ieee8021CnPortPriLldpInstanceSelectorGet, Ieee8021CnPortPriLldpInstanceSelectorSet, Ieee8021CnPortPriLldpInstanceSelectorTest, Ieee8021CnPortPriTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Ieee8021CnPortPriTableINDEX, 3, 0, 0, "3"},

{{12,Ieee8021CnPortPriAlternatePriority}, GetNextIndexIeee8021CnPortPriTable, Ieee8021CnPortPriAlternatePriorityGet, Ieee8021CnPortPriAlternatePrioritySet, Ieee8021CnPortPriAlternatePriorityTest, Ieee8021CnPortPriTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Ieee8021CnPortPriTableINDEX, 3, 0, 0, "0"},

{{12,Ieee8021CnCpComponentId}, GetNextIndexIeee8021CnCpTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, Ieee8021CnCpTableINDEX, 3, 0, 0, NULL},

{{12,Ieee8021CnCpIfIndex}, GetNextIndexIeee8021CnCpTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, Ieee8021CnCpTableINDEX, 3, 0, 0, NULL},

{{12,Ieee8021CnCpIndex}, GetNextIndexIeee8021CnCpTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, Ieee8021CnCpTableINDEX, 3, 0, 0, NULL},

{{12,Ieee8021CnCpPriority}, GetNextIndexIeee8021CnCpTable, Ieee8021CnCpPriorityGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Ieee8021CnCpTableINDEX, 3, 0, 0, NULL},

{{12,Ieee8021CnCpMacAddress}, GetNextIndexIeee8021CnCpTable, Ieee8021CnCpMacAddressGet, NULL, NULL, NULL, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_READONLY, Ieee8021CnCpTableINDEX, 3, 0, 0, NULL},

{{12,Ieee8021CnCpIdentifier}, GetNextIndexIeee8021CnCpTable, Ieee8021CnCpIdentifierGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, Ieee8021CnCpTableINDEX, 3, 0, 0, NULL},

{{12,Ieee8021CnCpQueueSizeSetPoint}, GetNextIndexIeee8021CnCpTable, Ieee8021CnCpQueueSizeSetPointGet, Ieee8021CnCpQueueSizeSetPointSet, Ieee8021CnCpQueueSizeSetPointTest, Ieee8021CnCpTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Ieee8021CnCpTableINDEX, 3, 0, 0, "26000"},

{{12,Ieee8021CnCpFeedbackWeight}, GetNextIndexIeee8021CnCpTable, Ieee8021CnCpFeedbackWeightGet, Ieee8021CnCpFeedbackWeightSet, Ieee8021CnCpFeedbackWeightTest, Ieee8021CnCpTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, Ieee8021CnCpTableINDEX, 3, 0, 0, "1"},

{{12,Ieee8021CnCpMinSampleBase}, GetNextIndexIeee8021CnCpTable, Ieee8021CnCpMinSampleBaseGet, Ieee8021CnCpMinSampleBaseSet, Ieee8021CnCpMinSampleBaseTest, Ieee8021CnCpTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Ieee8021CnCpTableINDEX, 3, 0, 0, "150000"},

{{12,Ieee8021CnCpDiscardedFrames}, GetNextIndexIeee8021CnCpTable, Ieee8021CnCpDiscardedFramesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER64, SNMP_READONLY, Ieee8021CnCpTableINDEX, 3, 0, 0, NULL},

{{12,Ieee8021CnCpTransmittedFrames}, GetNextIndexIeee8021CnCpTable, Ieee8021CnCpTransmittedFramesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER64, SNMP_READONLY, Ieee8021CnCpTableINDEX, 3, 0, 0, NULL},

{{12,Ieee8021CnCpTransmittedCnms}, GetNextIndexIeee8021CnCpTable, Ieee8021CnCpTransmittedCnmsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER64, SNMP_READONLY, Ieee8021CnCpTableINDEX, 3, 0, 0, NULL},

{{12,Ieee8021CnCpMinHeaderOctets}, GetNextIndexIeee8021CnCpTable, Ieee8021CnCpMinHeaderOctetsGet, Ieee8021CnCpMinHeaderOctetsSet, Ieee8021CnCpMinHeaderOctetsTest, Ieee8021CnCpTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Ieee8021CnCpTableINDEX, 3, 0, 0, "0"},

{{12,Ieee8021CnCpidToIfCpid}, GetNextIndexIeee8021CnCpidToInterfaceTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, Ieee8021CnCpidToInterfaceTableINDEX, 1, 0, 0, NULL},

{{12,Ieee8021CnCpidToIfComponentId}, GetNextIndexIeee8021CnCpidToInterfaceTable, Ieee8021CnCpidToIfComponentIdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Ieee8021CnCpidToInterfaceTableINDEX, 1, 0, 0, NULL},

{{12,Ieee8021CnCpidToIfIfIndex}, GetNextIndexIeee8021CnCpidToInterfaceTable, Ieee8021CnCpidToIfIfIndexGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Ieee8021CnCpidToInterfaceTableINDEX, 1, 0, 0, NULL},

{{12,Ieee8021CnCpidToIfCpIndex}, GetNextIndexIeee8021CnCpidToInterfaceTable, Ieee8021CnCpidToIfCpIndexGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Ieee8021CnCpidToInterfaceTableINDEX, 1, 0, 0, NULL},

{{12,Ieee8021CnRpPortPriComponentId}, GetNextIndexIeee8021CnRpPortPriTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, Ieee8021CnRpPortPriTableINDEX, 3, 0, 0, NULL},

{{12,Ieee8021CnRpPortPriPriority}, GetNextIndexIeee8021CnRpPortPriTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, Ieee8021CnRpPortPriTableINDEX, 3, 0, 0, NULL},

{{12,Ieee8021CnRpPortPriIfIndex}, GetNextIndexIeee8021CnRpPortPriTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, Ieee8021CnRpPortPriTableINDEX, 3, 0, 0, NULL},

{{12,Ieee8021CnRpPortPriMaxRps}, GetNextIndexIeee8021CnRpPortPriTable, Ieee8021CnRpPortPriMaxRpsGet, Ieee8021CnRpPortPriMaxRpsSet, Ieee8021CnRpPortPriMaxRpsTest, Ieee8021CnRpPortPriTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Ieee8021CnRpPortPriTableINDEX, 3, 0, 0, "1"},

{{12,Ieee8021CnRpPortPriCreatedRps}, GetNextIndexIeee8021CnRpPortPriTable, Ieee8021CnRpPortPriCreatedRpsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Ieee8021CnRpPortPriTableINDEX, 3, 0, 0, NULL},

{{12,Ieee8021CnRpPortPriCentiseconds}, GetNextIndexIeee8021CnRpPortPriTable, Ieee8021CnRpPortPriCentisecondsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER64, SNMP_READONLY, Ieee8021CnRpPortPriTableINDEX, 3, 0, 0, NULL},

{{12,Ieee8021CnRpgComponentId}, GetNextIndexIeee8021CnRpGroupTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, Ieee8021CnRpGroupTableINDEX, 4, 0, 0, NULL},

{{12,Ieee8021CnRpgPriority}, GetNextIndexIeee8021CnRpGroupTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, Ieee8021CnRpGroupTableINDEX, 4, 0, 0, NULL},

{{12,Ieee8021CnRpgIfIndex}, GetNextIndexIeee8021CnRpGroupTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, Ieee8021CnRpGroupTableINDEX, 4, 0, 0, NULL},

{{12,Ieee8021CnRpgIdentifier}, GetNextIndexIeee8021CnRpGroupTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, Ieee8021CnRpGroupTableINDEX, 4, 0, 0, NULL},

{{12,Ieee8021CnRpgEnable}, GetNextIndexIeee8021CnRpGroupTable, Ieee8021CnRpgEnableGet, Ieee8021CnRpgEnableSet, Ieee8021CnRpgEnableTest, Ieee8021CnRpGroupTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Ieee8021CnRpGroupTableINDEX, 4, 0, 0, "1"},

{{12,Ieee8021CnRpgTimeReset}, GetNextIndexIeee8021CnRpGroupTable, Ieee8021CnRpgTimeResetGet, Ieee8021CnRpgTimeResetSet, Ieee8021CnRpgTimeResetTest, Ieee8021CnRpGroupTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Ieee8021CnRpGroupTableINDEX, 4, 0, 0, "15"},

{{12,Ieee8021CnRpgByteReset}, GetNextIndexIeee8021CnRpGroupTable, Ieee8021CnRpgByteResetGet, Ieee8021CnRpgByteResetSet, Ieee8021CnRpgByteResetTest, Ieee8021CnRpGroupTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Ieee8021CnRpGroupTableINDEX, 4, 0, 0, "150000"},

{{12,Ieee8021CnRpgThreshold}, GetNextIndexIeee8021CnRpGroupTable, Ieee8021CnRpgThresholdGet, Ieee8021CnRpgThresholdSet, Ieee8021CnRpgThresholdTest, Ieee8021CnRpGroupTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Ieee8021CnRpGroupTableINDEX, 4, 0, 0, "5"},

{{12,Ieee8021CnRpgMaxRate}, GetNextIndexIeee8021CnRpGroupTable, Ieee8021CnRpgMaxRateGet, Ieee8021CnRpgMaxRateSet, Ieee8021CnRpgMaxRateTest, Ieee8021CnRpGroupTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Ieee8021CnRpGroupTableINDEX, 4, 0, 0, NULL},

{{12,Ieee8021CnRpgAiRate}, GetNextIndexIeee8021CnRpGroupTable, Ieee8021CnRpgAiRateGet, Ieee8021CnRpgAiRateSet, Ieee8021CnRpgAiRateTest, Ieee8021CnRpGroupTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Ieee8021CnRpGroupTableINDEX, 4, 0, 0, "5"},

{{12,Ieee8021CnRpgHaiRate}, GetNextIndexIeee8021CnRpGroupTable, Ieee8021CnRpgHaiRateGet, Ieee8021CnRpgHaiRateSet, Ieee8021CnRpgHaiRateTest, Ieee8021CnRpGroupTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Ieee8021CnRpGroupTableINDEX, 4, 0, 0, "50"},

{{12,Ieee8021CnRpgGd}, GetNextIndexIeee8021CnRpGroupTable, Ieee8021CnRpgGdGet, Ieee8021CnRpgGdSet, Ieee8021CnRpgGdTest, Ieee8021CnRpGroupTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, Ieee8021CnRpGroupTableINDEX, 4, 0, 0, "7"},

{{12,Ieee8021CnRpgMinDecFac}, GetNextIndexIeee8021CnRpGroupTable, Ieee8021CnRpgMinDecFacGet, Ieee8021CnRpgMinDecFacSet, Ieee8021CnRpgMinDecFacTest, Ieee8021CnRpGroupTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Ieee8021CnRpGroupTableINDEX, 4, 0, 0, "50"},

{{12,Ieee8021CnRpgMinRate}, GetNextIndexIeee8021CnRpGroupTable, Ieee8021CnRpgMinRateGet, Ieee8021CnRpgMinRateSet, Ieee8021CnRpgMinRateTest, Ieee8021CnRpGroupTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Ieee8021CnRpGroupTableINDEX, 4, 0, 0, "5"},
};
tMibData stdcnEntry = { 63, stdcnMibEntry };

#endif /* _STDCNDB_H */

