enum {
    MAX_CN_COMP_PRIORITY_TABLE_SIZING_ID,
    MAX_CN_COMP_TABLE_INFO_SIZING_ID,
    MAX_CN_COMP_TABLE_PTRS_SIZING_ID,
    MAX_CN_PORT_PRIORITY_TABLE_INFO_SIZING_ID,
    MAX_CN_PORT_TABLE_INFO_SIZING_ID,
    MAX_CN_Q_MESG_SIZING_ID,
    CN_MAX_SIZING_ID
};


#ifdef  _CNSZ_C
tMemPoolId CNMemPoolIds[ CN_MAX_SIZING_ID];
INT4  CnSizingMemCreateMemPools(VOID);
VOID  CnSizingMemDeleteMemPools(VOID);
INT4  CnSzRegisterModuleSizingParams( CHR1 *pu1ModName);
#else  /*  _CNSZ_C  */
extern tMemPoolId CNMemPoolIds[ ];
extern INT4  CnSizingMemCreateMemPools(VOID);
extern VOID  CnSizingMemDeleteMemPools(VOID);
extern INT4  CnSzRegisterModuleSizingParams( CHR1 *pu1ModName); 
#endif /*  _CNSZ_C  */


#ifdef  _CNSZ_C
tFsModSizingParams FsCNSizingParams [] = {
{ "tCnCompPriTbl", "MAX_CN_COMP_PRIORITY_TABLE", sizeof(tCnCompPriTbl),MAX_CN_COMP_PRIORITY_TABLE, MAX_CN_COMP_PRIORITY_TABLE,0 },
{ "tCnCompTblInfo", "MAX_CN_COMP_TABLE_INFO", sizeof(tCnCompTblInfo),MAX_CN_COMP_TABLE_INFO, MAX_CN_COMP_TABLE_INFO,0 },
{ "tCnCompTblInfo*", "MAX_CN_COMP_TABLE_PTRS", sizeof(tCnCompTblInfo*),MAX_CN_COMP_TABLE_PTRS, MAX_CN_COMP_TABLE_PTRS,0 },
{ "tCnPortPriTblInfo", "MAX_CN_PORT_PRIORITY_TABLE_INFO", sizeof(tCnPortPriTblInfo),MAX_CN_PORT_PRIORITY_TABLE_INFO, MAX_CN_PORT_PRIORITY_TABLE_INFO,0 },
{ "tCnPortTblInfo", "MAX_CN_PORT_TABLE_INFO", sizeof(tCnPortTblInfo),MAX_CN_PORT_TABLE_INFO, MAX_CN_PORT_TABLE_INFO,0 },
{ "tCnQMsg", "MAX_CN_Q_MESG", sizeof(tCnQMsg),MAX_CN_Q_MESG, MAX_CN_Q_MESG,0 },
{"\0","\0",0,0,0,0}
};
#else  /*  _CNSZ_C  */
extern tFsModSizingParams FsCNSizingParams [];
#endif /*  _CNSZ_C  */


