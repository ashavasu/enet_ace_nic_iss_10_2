/********************************************************************
* Copyright (C) 2010 Aricent Inc . All Rights Reserved
*
* $Id: cnextn.h,v 1.2 2010/07/23 13:33:20 prabuc Exp $
*
* Description: This file contains extern variables declaration
*              used in CN.
*********************************************************************/
#ifndef _CN_EXTN_H
#define _CN_EXTN_H

PUBLIC tCnMasterTblInfo gCnMasterTblInfo;

PUBLIC  UINT1 gau1CnOUI[CN_TLV_OUI_LEN];

PUBLIC  UINT1 gau1CnSentEvent[CN_DBG_MAX_TX_EVENTS][CN_MAX_EVENT_NAME_LEN];

PUBLIC  UINT1 gau1CnRcvdEvent[CN_DBG_MAX_RX_EVENTS][CN_MAX_EVENT_NAME_LEN];

PUBLIC  UINT1 gau1CnPreFormedTlv[CN_PREFORMED_TLV_LEN];

PUBLIC  const CHR1 *gau1CnStateStr[CN_MAX_STATES];

PUBLIC  const CHR1 *gau1CnEvntStr[CN_MAX_EVENTS];

#endif /* _CN_EXTN_H*/
/*-----------------------------------------------------------------------*/
/*                       End of the file  cnextn.h                       */
/*-----------------------------------------------------------------------*/

