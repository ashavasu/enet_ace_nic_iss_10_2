/********************************************************************
* Copyright (C) 2010 Aricent Inc . All Rights Reserved
*
* $Id: fscnlw.h,v 1.1.1.1 2010/07/05 06:43:40 prabuc Exp $
*
* Description: Proto types for Low Level  Routines
*********************************************************************/

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsCnSystemControl ARG_LIST((INT4 *));

INT1
nmhGetFsCnGlobalEnableTrap ARG_LIST((INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsCnSystemControl ARG_LIST((INT4 ));

INT1
nmhSetFsCnGlobalEnableTrap ARG_LIST((INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsCnSystemControl ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsCnGlobalEnableTrap ARG_LIST((UINT4 *  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsCnSystemControl ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsCnGlobalEnableTrap ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsCnXGlobalTable. */
INT1
nmhValidateIndexInstanceFsCnXGlobalTable ARG_LIST((UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsCnXGlobalTable  */

INT1
nmhGetFirstIndexFsCnXGlobalTable ARG_LIST((UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsCnXGlobalTable ARG_LIST((UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsCnXGlobalTraceLevel ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetFsCnXGlobalClearCounters ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetFsCnXGlobalTLVErrors ARG_LIST((UINT4 ,UINT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsCnXGlobalTraceLevel ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetFsCnXGlobalClearCounters ARG_LIST((UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsCnXGlobalTraceLevel ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2FsCnXGlobalClearCounters ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsCnXGlobalTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsCnXPortPriTable. */
INT1
nmhValidateIndexInstanceFsCnXPortPriTable ARG_LIST((UINT4  , UINT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsCnXPortPriTable  */

INT1
nmhGetFirstIndexFsCnXPortPriTable ARG_LIST((UINT4 * , UINT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsCnXPortPriTable ARG_LIST((UINT4 , UINT4 * , UINT4 , UINT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsCnXPortPriClearCpCounters ARG_LIST((UINT4  , UINT4  , INT4 ,INT4 *));

INT1
nmhGetFsCnXPortPriErrorEntry ARG_LIST((UINT4  , UINT4  , INT4 ,INT4 *));

INT1
nmhGetFsCnXPortPriOperDefMode ARG_LIST((UINT4  , UINT4  , INT4 ,INT4 *));

INT1
nmhGetFsCnXPortPriOperAltPri ARG_LIST((UINT4  , UINT4  , INT4 ,UINT4 *));

INT1
nmhGetFsCnXPortPriLastRcvdEvent ARG_LIST((UINT4  , UINT4  , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsCnXPortPriLastRcvdEventTime ARG_LIST((UINT4  , UINT4  , INT4 ,UINT4 *));

INT1
nmhGetFsCnXPortPriLastSentEvent ARG_LIST((UINT4  , UINT4  , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsCnXPortPriLastSentEventTime ARG_LIST((UINT4  , UINT4  , INT4 ,UINT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsCnXPortPriClearCpCounters ARG_LIST((UINT4  , UINT4  , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsCnXPortPriClearCpCounters ARG_LIST((UINT4 *  ,UINT4  , UINT4  , INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsCnXPortPriTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto type for Low Level GET Routine All Objects.  */
