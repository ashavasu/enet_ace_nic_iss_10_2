/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fscndb.h,v 1.3 2011/08/24 06:19:14 siva Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/
#ifndef _FSCNDB_H
#define _FSCNDB_H

UINT1 FsCnXGlobalTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32};
UINT1 FsCnXPortPriTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_INTEGER};

UINT4 fscn [] ={1,3,6,1,4,1,29601,2,47};
tSNMP_OID_TYPE fscnOID = {9, fscn};


UINT4 FsCnSystemControl [ ] ={1,3,6,1,4,1,29601,2,47,1,1};
UINT4 FsCnGlobalEnableTrap [ ] ={1,3,6,1,4,1,29601,2,47,1,2};
UINT4 FsCnXGlobalTraceLevel [ ] ={1,3,6,1,4,1,29601,2,47,2,1,1,1};
UINT4 FsCnXGlobalClearCounters [ ] ={1,3,6,1,4,1,29601,2,47,2,1,1,2};
UINT4 FsCnXGlobalTLVErrors [ ] ={1,3,6,1,4,1,29601,2,47,2,1,1,3};
UINT4 FsCnXPortPriClearCpCounters [ ] ={1,3,6,1,4,1,29601,2,47,3,1,1,1};
UINT4 FsCnXPortPriErrorEntry [ ] ={1,3,6,1,4,1,29601,2,47,3,1,1,2};
UINT4 FsCnXPortPriOperDefMode [ ] ={1,3,6,1,4,1,29601,2,47,3,1,1,3};
UINT4 FsCnXPortPriOperAltPri [ ] ={1,3,6,1,4,1,29601,2,47,3,1,1,4};
UINT4 FsCnXPortPriLastRcvdEvent [ ] ={1,3,6,1,4,1,29601,2,47,3,1,1,5};
UINT4 FsCnXPortPriLastRcvdEventTime [ ] ={1,3,6,1,4,1,29601,2,47,3,1,1,6};
UINT4 FsCnXPortPriLastSentEvent [ ] ={1,3,6,1,4,1,29601,2,47,3,1,1,7};
UINT4 FsCnXPortPriLastSentEventTime [ ] ={1,3,6,1,4,1,29601,2,47,3,1,1,8};
UINT4 FsCnCnmQOffset [ ] ={1,3,6,1,4,1,29601,2,47,4,1};
UINT4 FsCnCnmQDelta [ ] ={1,3,6,1,4,1,29601,2,47,4,2};




tMbDbEntry fscnMibEntry[]= {

{{11,FsCnSystemControl}, NULL, FsCnSystemControlGet, FsCnSystemControlSet, FsCnSystemControlTest, FsCnSystemControlDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "1"},

{{11,FsCnGlobalEnableTrap}, NULL, FsCnGlobalEnableTrapGet, FsCnGlobalEnableTrapSet, FsCnGlobalEnableTrapTest, FsCnGlobalEnableTrapDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "3"},

{{13,FsCnXGlobalTraceLevel}, GetNextIndexFsCnXGlobalTable, FsCnXGlobalTraceLevelGet, FsCnXGlobalTraceLevelSet, FsCnXGlobalTraceLevelTest, FsCnXGlobalTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsCnXGlobalTableINDEX, 1, 0, 0, "32"},

{{13,FsCnXGlobalClearCounters}, GetNextIndexFsCnXGlobalTable, FsCnXGlobalClearCountersGet, FsCnXGlobalClearCountersSet, FsCnXGlobalClearCountersTest, FsCnXGlobalTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsCnXGlobalTableINDEX, 1, 0, 0, "2"},

{{13,FsCnXGlobalTLVErrors}, GetNextIndexFsCnXGlobalTable, FsCnXGlobalTLVErrorsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsCnXGlobalTableINDEX, 1, 0, 0, NULL},

{{13,FsCnXPortPriClearCpCounters}, GetNextIndexFsCnXPortPriTable, FsCnXPortPriClearCpCountersGet, FsCnXPortPriClearCpCountersSet, FsCnXPortPriClearCpCountersTest, FsCnXPortPriTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsCnXPortPriTableINDEX, 3, 0, 0, "2"},

{{13,FsCnXPortPriErrorEntry}, GetNextIndexFsCnXPortPriTable, FsCnXPortPriErrorEntryGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsCnXPortPriTableINDEX, 3, 0, 0, "2"},

{{13,FsCnXPortPriOperDefMode}, GetNextIndexFsCnXPortPriTable, FsCnXPortPriOperDefModeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsCnXPortPriTableINDEX, 3, 0, 0, NULL},

{{13,FsCnXPortPriOperAltPri}, GetNextIndexFsCnXPortPriTable, FsCnXPortPriOperAltPriGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsCnXPortPriTableINDEX, 3, 0, 0, NULL},

{{13,FsCnXPortPriLastRcvdEvent}, GetNextIndexFsCnXPortPriTable, FsCnXPortPriLastRcvdEventGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsCnXPortPriTableINDEX, 3, 0, 0, NULL},

{{13,FsCnXPortPriLastRcvdEventTime}, GetNextIndexFsCnXPortPriTable, FsCnXPortPriLastRcvdEventTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, FsCnXPortPriTableINDEX, 3, 0, 0, NULL},

{{13,FsCnXPortPriLastSentEvent}, GetNextIndexFsCnXPortPriTable, FsCnXPortPriLastSentEventGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsCnXPortPriTableINDEX, 3, 0, 0, NULL},

{{13,FsCnXPortPriLastSentEventTime}, GetNextIndexFsCnXPortPriTable, FsCnXPortPriLastSentEventTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, FsCnXPortPriTableINDEX, 3, 0, 0, NULL},

{{11,FsCnCnmQOffset}, NULL, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_ACCESSIBLEFORNOTIFY, NULL, 0, 0, 0, NULL},

{{11,FsCnCnmQDelta}, NULL, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_ACCESSIBLEFORNOTIFY, NULL, 0, 0, 0, NULL},
};
tMibData fscnEntry = { 15, fscnMibEntry };

#endif /* _FSCNDB_H */

