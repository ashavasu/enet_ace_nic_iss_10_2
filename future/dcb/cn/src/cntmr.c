/********************************************************************
 * Copyright (C) 2010 Aricent Inc . All Rights Reserved
 *
 * $Id: cntmr.c,v 1.2 2010/08/17 11:06:21 prabuc Exp $
 *
 * Description: This file contains CN timer related functions.  
 *********************************************************************/
#include "cninc.h"

PRIVATE VOID CnTmrInitTmrDesc PROTO ((VOID));

PRIVATE VOID CnTmrTlvTxDelayTmrExp PROTO ((VOID *pArg));

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : CnTmrInit 
 *                                                                          
 *    DESCRIPTION      : This function creates a timer list for all the timers
 *                       in CN module.  
 *
 *    INPUT            : None 
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE 
 *                                                                          
 ****************************************************************************/
PUBLIC INT4
CnTmrInit (VOID)
{
    if (TmrCreateTimerList (CN_TASK_NAME, CN_TMR_EXPIRY_EVENT, NULL,
                            &(gCnMasterTblInfo.TmrListId)) == TMR_FAILURE)
    {
        CN_GBL_TRC (CN_INVALID_CONTEXT, (CN_ALL_FAILURE_TRC | CN_CRITICAL_TRC),
                    "CnTmrInit: Timer list creation FAILED !!!\r\n");
        return OSIX_FAILURE;
    }

    CnTmrInitTmrDesc ();

    return OSIX_SUCCESS;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : CnTmrInitTmrDesc 
 *                                                                          
 *    DESCRIPTION      : This function intializes the timer desc for all 
 *                       the timers in CN module.  
 *
 *    INPUT            : None 
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : None 
 *                                                                          
 ****************************************************************************/
PRIVATE VOID
CnTmrInitTmrDesc (VOID)
{
    /*intializes the timer desc for Delay timer */
    gCnMasterTblInfo.aTmrDesc[CN_TLV_TMR].i2Offset =
        (INT2) CN_OFFSET (tCnPortTblInfo, TlvTxDelayTmrNode);
    gCnMasterTblInfo.aTmrDesc[CN_TLV_TMR].TmrExpFn = CnTmrTlvTxDelayTmrExp;
    return;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : CnTmrDeInit 
 *                                                                          
 *    DESCRIPTION      : This function de-initializes the timer list.
 *
 *    INPUT            : None
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE 
 *                                                                          
 ****************************************************************************/
PUBLIC INT4
CnTmrDeInit (VOID)
{
    if (TmrDeleteTimerList (gCnMasterTblInfo.TmrListId) == TMR_FAILURE)
    {
        CN_GBL_TRC (CN_INVALID_CONTEXT, (CN_ALL_FAILURE_TRC | CN_CRITICAL_TRC),
                    "CnTmrDeInit: Timer list deletion FAILED !!!\r\n");
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : CnTmrExpHandler 
 *                                                                          
 *    DESCRIPTION      : This function is called whenever a timer expiry 
 *                       message is received by CN task. 
 *
 *    INPUT            : None 
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : None 
 *                                                                          
 ****************************************************************************/
PUBLIC VOID
CnTmrExpHandler (VOID)
{
    tTmrAppTimer       *pExpiredTimers = NULL;
    UINT1               u1TimerId = CN_ZERO;
    INT2                i2Offset = CN_ZERO;

    while ((pExpiredTimers =
            TmrGetNextExpiredTimer (gCnMasterTblInfo.TmrListId)) != NULL)
    {

        u1TimerId = ((tTmrBlk *) pExpiredTimers)->u1TimerId;
        if (u1TimerId < CN_MAX_TIMER_TYPES)
        {
            i2Offset = gCnMasterTblInfo.aTmrDesc[u1TimerId].i2Offset;

            (*(gCnMasterTblInfo.aTmrDesc[u1TimerId].TmrExpFn))
                ((UINT1 *) pExpiredTimers - i2Offset);

        }
    }                            /* End of while */

    return;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : CnTmrTlvTxDelayTmrExp
 *                                                                          
 *    DESCRIPTION      : Function that handles Tlv timer of CN Module.
 *                       This timer indicates the delay(in seconds) that
 *                       CN  module introduces When Tlv construction 
 *
 *    INPUT            : pArg - pointer to the port specific struct whose 
 *                       timer has expired. 
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : None
 *                                                                          
 ****************************************************************************/
PUBLIC VOID
CnTmrTlvTxDelayTmrExp (VOID *pArg)
{
    tCnPortTblInfo     *pPortInfo = NULL;

    pPortInfo = (tCnPortTblInfo *) pArg;
    if (pPortInfo == NULL)
    {
        return;
    }
    if (TmrStopTimer (gCnMasterTblInfo.TmrListId,
                      &(pPortInfo->TlvTxDelayTmrNode.CnTmrBlk.TimerNode))
        != TMR_SUCCESS)
    {
        return;
    }
    pPortInfo->TlvTxDelayTmrNode.u1Status = CN_TMR_NOT_RUNNING;
    /*construct and post TLV to LLDP on this post */
    if (CnUtilTlvConstruct (pPortInfo, (UINT4) CN_MSG_PORT_UPDATE)
        == OSIX_FAILURE)
    {
        return;
    }
    return;
}

/* End of file */
