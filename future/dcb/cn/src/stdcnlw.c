/********************************************************************
* Copyright (C) 2009 Aricent Inc . All Rights Reserved
*
* $Id: stdcnlw.c,v 1.9 2014/01/30 12:20:42 siva Exp $
*
* Description: Protocol Low Level Routines
*********************************************************************/
#include  "fssnmp.h"
#include  "cninc.h"

/* LOW LEVEL Routines for Table : Ieee8021CnGlobalTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceIeee8021CnGlobalTable
 Input       :  The Indices
                Ieee8021CnGlobalComponentId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceIeee8021CnGlobalTable (UINT4
                                               u4Ieee8021CnGlobalComponentId)
{
    if (CN_IS_SHUTDOWN ())
    {
        return SNMP_FAILURE;
    }
    if (CN_IS_VALID_COMP_ID (u4Ieee8021CnGlobalComponentId) == OSIX_FALSE)
    {
        CLI_SET_ERR (CN_CLI_INVALID_COMP_ID);
        return SNMP_FAILURE;
    }
    if (CN_COMP_TBL_ENTRY (u4Ieee8021CnGlobalComponentId) == NULL)
    {
        CLI_SET_ERR (CN_CLI_COMP_ENTRY_NOT_FOUND);
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexIeee8021CnGlobalTable
 Input       :  The Indices
                Ieee8021CnGlobalComponentId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexIeee8021CnGlobalTable (UINT4 *pu4Ieee8021CnGlobalComponentId)
{
    *pu4Ieee8021CnGlobalComponentId = CN_MIN_CONTEXT;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexIeee8021CnGlobalTable
 Input       :  The Indices
                Ieee8021CnGlobalComponentId
                nextIeee8021CnGlobalComponentId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexIeee8021CnGlobalTable (UINT4 u4Ieee8021CnGlobalComponentId,
                                      UINT4 *pu4NextIeee8021CnGlobalComponentId)
{
    UINT4               u4CompId = CN_MIN_CONTEXT;
    tCnCompTblInfo     *pCnCompTblEntry = NULL;

    for (u4CompId = u4Ieee8021CnGlobalComponentId + CN_ONE;
         u4CompId < CN_MAX_CONTEXTS; u4CompId++)
    {
        pCnCompTblEntry = CN_COMP_TBL_ENTRY (u4CompId);
        if (pCnCompTblEntry != NULL)
        {
            *pu4NextIeee8021CnGlobalComponentId = u4CompId;
            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetIeee8021CnGlobalMasterEnable
 Input       :  The Indices
                Ieee8021CnGlobalComponentId

                The Object 
                retValIeee8021CnGlobalMasterEnable
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021CnGlobalMasterEnable (UINT4 u4Ieee8021CnGlobalComponentId,
                                    INT4 *pi4RetValIeee8021CnGlobalMasterEnable)
{
    if (CN_COMP_TBL_ENTRY (u4Ieee8021CnGlobalComponentId) != NULL)
    {
        *pi4RetValIeee8021CnGlobalMasterEnable = (INT4)
            ((CN_COMP_TBL_ENTRY (u4Ieee8021CnGlobalComponentId))->
             bCnCompMasterEnable);
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetIeee8021CnGlobalCnmTransmitPriority
 Input       :  The Indices
                Ieee8021CnGlobalComponentId

                The Object 
                retValIeee8021CnGlobalCnmTransmitPriority
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
     
     
     
     
     
     
     
    nmhGetIeee8021CnGlobalCnmTransmitPriority
    (UINT4 u4Ieee8021CnGlobalComponentId,
     UINT4 *pi4RetValIeee8021CnGlobalCnmTransmitPriority)
{
    if (CN_COMP_TBL_ENTRY (u4Ieee8021CnGlobalComponentId) != NULL)
    {
        *pi4RetValIeee8021CnGlobalCnmTransmitPriority =
            (CN_COMP_TBL_ENTRY (u4Ieee8021CnGlobalComponentId)->
             u1CnmTransmitPriority);
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetIeee8021CnGlobalDiscardedFrames
 Input       :  The Indices
                Ieee8021CnGlobalComponentId

                The Object 
                retValIeee8021CnGlobalDiscardedFrames
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
     
     
     
     
     
     
     
    nmhGetIeee8021CnGlobalDiscardedFrames
    (UINT4 u4Ieee8021CnGlobalComponentId,
     tSNMP_COUNTER64_TYPE * pu8RetValIeee8021CnGlobalDiscardedFrames)
{
    tCnCompTblInfo     *pCnCompTblEntry = NULL;
    tCnPortTblInfo     *pCnPortTblEntry = NULL;
    tCnPortTblInfo     *pCnTempPortTblEntry = NULL;
    tCnPortPriTblInfo  *pCnPortPriEntry = NULL;
    UINT4               u4PortId = CN_ZERO;
    UINT4               u4Priority = CN_ZERO;
    FS_UINT8            u8DiscardedFrames;
    FS_UINT8            u8CompDisFrames;
    tSNMP_COUNTER64_TYPE DisFrames;
    tSNMP_COUNTER64_TYPE TransFrames;
    tSNMP_COUNTER64_TYPE TransCnms;

    MEMSET (&DisFrames, CN_ZERO, sizeof (tSNMP_COUNTER64_TYPE));
    MEMSET (&TransFrames, CN_ZERO, sizeof (tSNMP_COUNTER64_TYPE));
    MEMSET (&TransCnms, CN_ZERO, sizeof (tSNMP_COUNTER64_TYPE));
    FSAP_U8_CLR (&u8DiscardedFrames);
    FSAP_U8_CLR (&u8CompDisFrames);

    pCnCompTblEntry = CN_COMP_TBL_ENTRY (u4Ieee8021CnGlobalComponentId);
    if (pCnCompTblEntry == NULL)
    {
        CN_GBL_TRC (CN_INVALID_CONTEXT,
                    (CN_MGMT_TRC | CN_ALL_FAILURE_TRC),
                    "nmhGetIeee8021CnGlobalDiscardedFrames: FAILED \r\n"
                    "Context is not present in CN !!!\r\n");

        return SNMP_FAILURE;
    }

    pCnPortTblEntry = (tCnPortTblInfo *) RBTreeGetFirst
        (pCnCompTblEntry->PortTbl);
    while (pCnPortTblEntry != NULL)
    {
        u4PortId = pCnPortTblEntry->u4PortId;
        for (u4Priority = CN_ZERO; u4Priority < CN_MAX_VLAN_PRIORITY;
             u4Priority++)
        {
            CN_PORT_PRI_ENTRY (u4Ieee8021CnGlobalComponentId, u4PortId,
                               u4Priority, pCnPortPriEntry);
            if (pCnPortPriEntry != NULL)
            {
                if (CnHwGetCounters (u4Ieee8021CnGlobalComponentId, u4PortId,
                                     ((UINT1) u4Priority), &DisFrames,
                                     &TransFrames, &TransCnms) == OSIX_FAILURE)
                {
                    return SNMP_FAILURE;
                }

                FSAP_U8_ASSIGN_HI (&u8DiscardedFrames, DisFrames.msn);
                FSAP_U8_ASSIGN_LO (&u8DiscardedFrames, DisFrames.lsn);
                FSAP_U8_ADD (&u8CompDisFrames, &u8CompDisFrames,
                             &u8DiscardedFrames);
            }
        }
        pCnTempPortTblEntry = pCnPortTblEntry;
        pCnPortTblEntry = (tCnPortTblInfo *) RBTreeGetNext
            (pCnCompTblEntry->PortTbl, pCnTempPortTblEntry, NULL);
    }
    MEMCPY (pu8RetValIeee8021CnGlobalDiscardedFrames, &u8CompDisFrames,
            sizeof (FS_UINT8));
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetIeee8021CnGlobalMasterEnable
 Input       :  The Indices
                Ieee8021CnGlobalComponentId

                The Object 
                setValIeee8021CnGlobalMasterEnable
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIeee8021CnGlobalMasterEnable (UINT4 u4Ieee8021CnGlobalComponentId,
                                    INT4 i4SetValIeee8021CnGlobalMasterEnable)
{
    tCnCompTblInfo     *pCnCompTblEntry = NULL;

    pCnCompTblEntry = CN_COMP_TBL_ENTRY (u4Ieee8021CnGlobalComponentId);
    if (pCnCompTblEntry == NULL)
    {

        CN_GBL_TRC (CN_INVALID_CONTEXT,
                    (CN_ALL_FAILURE_TRC | CN_MGMT_TRC),
                    "nmhSetIeee8021CnGlobalMasterEnable: FAILED\r\n"
                    "Context is not present in CN !!!\r\n");

        return SNMP_FAILURE;
    }

    if (pCnCompTblEntry->bCnCompMasterEnable ==
        (UINT1) i4SetValIeee8021CnGlobalMasterEnable)
    {
        return SNMP_SUCCESS;
    }

    if (i4SetValIeee8021CnGlobalMasterEnable == CN_MASTER_ENABLE)
    {
        CnUtilMasterEnable (pCnCompTblEntry);
    }
    else
    {
        CnUtilMasterDisable (pCnCompTblEntry);
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetIeee8021CnGlobalCnmTransmitPriority
 Input       :  The Indices
                Ieee8021CnGlobalComponentId

                The Object 
                setValIeee8021CnGlobalCnmTransmitPriority
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetIeee8021CnGlobalCnmTransmitPriority (UINT4 u4Ieee8021CnGlobalComponentId,
                                           UINT4
                                           u4SetValIeee8021CnGlobalCnmTransmitPriority)
{
    tCnCompTblInfo     *pCnCompTblEntry = NULL;

    pCnCompTblEntry = CN_COMP_TBL_ENTRY (u4Ieee8021CnGlobalComponentId);
    if (pCnCompTblEntry == NULL)
    {
        CN_GBL_TRC (CN_INVALID_CONTEXT,
                    (CN_ALL_FAILURE_TRC | CN_MGMT_TRC),
                    "nmhSetIeee8021CnGlobalCnmTransmitPriority: FAILED\r\n"
                    "Context is not present in CN !!!\r\n");

        return SNMP_FAILURE;
    }
    if (pCnCompTblEntry->u1CnmTransmitPriority ==
        u4SetValIeee8021CnGlobalCnmTransmitPriority)
    {
        return SNMP_SUCCESS;
    }

    pCnCompTblEntry->u1CnmTransmitPriority = ((UINT1)
                                              u4SetValIeee8021CnGlobalCnmTransmitPriority);

    if (pCnCompTblEntry->bCnCompMasterEnable == CN_MASTER_ENABLE)
    {
        if (CnHwSetCnmPri (u4Ieee8021CnGlobalComponentId,
                           ((UINT1)
                            u4SetValIeee8021CnGlobalCnmTransmitPriority)) ==
            OSIX_FAILURE)
        {
            return SNMP_FAILURE;
        }

    }
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2Ieee8021CnGlobalMasterEnable
 Input       :  The Indices
                Ieee8021CnGlobalComponentId

                The Object 
                testValIeee8021CnGlobalMasterEnable
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ieee8021CnGlobalMasterEnable (UINT4 *pu4ErrorCode,
                                       UINT4 u4Ieee8021CnGlobalComponentId,
                                       INT4
                                       i4TestValIeee8021CnGlobalMasterEnable)
{
    if (CN_IS_VALID_COMP_ID (u4Ieee8021CnGlobalComponentId) == OSIX_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CN_CLI_INVALID_COMP_ID);
        return SNMP_FAILURE;
    }
    if ((i4TestValIeee8021CnGlobalMasterEnable != CN_MASTER_ENABLE) &&
        (i4TestValIeee8021CnGlobalMasterEnable != CN_MASTER_DISABLE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    if (CN_COMP_TBL_ENTRY (u4Ieee8021CnGlobalComponentId) == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CN_CLI_COMP_ENTRY_NOT_FOUND);
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Ieee8021CnGlobalCnmTransmitPriority
 Input       :  The Indices
                Ieee8021CnGlobalComponentId

                The Object 
                testValIeee8021CnGlobalCnmTransmitPriority
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ieee8021CnGlobalCnmTransmitPriority (UINT4 *pu4ErrorCode,
                                              UINT4
                                              u4Ieee8021CnGlobalComponentId,
                                              UINT4
                                              u4TestValIeee8021CnGlobalCnmTransmitPriority)
{
    if (CN_IS_VALID_COMP_ID (u4Ieee8021CnGlobalComponentId) == OSIX_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CN_CLI_INVALID_COMP_ID);
        return SNMP_FAILURE;
    }
    if (u4TestValIeee8021CnGlobalCnmTransmitPriority >= CN_MAX_VLAN_PRIORITY)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CN_CLI_INVALID_VLAN_PRIORITY);
        return SNMP_FAILURE;
    }
    if (CN_COMP_TBL_ENTRY (u4Ieee8021CnGlobalComponentId) == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CN_CLI_COMP_ENTRY_NOT_FOUND);
        return SNMP_FAILURE;

    }
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2Ieee8021CnGlobalTable
 Input       :  The Indices
                Ieee8021CnGlobalComponentId
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Ieee8021CnGlobalTable (UINT4 *pu4ErrorCode,
                               tSnmpIndexList * pSnmpIndexList,
                               tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : Ieee8021CnErroredPortTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceIeee8021CnErroredPortTable
 Input       :  The Indices
                Ieee8021CnEpComponentId
                Ieee8021CnEpPriority
                Ieee8021CnEpIfIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1 
     
     
     
     
     
     
     
    nmhValidateIndexInstanceIeee8021CnErroredPortTable
    (UINT4 u4Ieee8021CnEpComponentId,
     UINT4 u4Ieee8021CnEpPriority, INT4 i4Ieee8021CnEpIfIndex)
{
    tCnPortPriTblInfo  *pCnPortPriEntry = NULL;
    tCnCompTblInfo     *pCnCompTblEntry = NULL;
    tCnPortTblInfo     *pCnPortTblEntry = NULL;
    UINT4               u4PortId = i4Ieee8021CnEpIfIndex;

    if (CN_IS_SHUTDOWN ())
    {
        return SNMP_FAILURE;
    }

    if (CN_IS_VALID_COMP_ID (u4Ieee8021CnEpComponentId) == OSIX_FALSE)
    {
        CLI_SET_ERR (CN_CLI_INVALID_COMP_ID);
        return SNMP_FAILURE;
    }

    if (u4Ieee8021CnEpPriority >= CN_MAX_VLAN_PRIORITY)
    {
        CLI_SET_ERR (CN_CLI_INVALID_CNPV);
        return SNMP_FAILURE;
    }

    if ((i4Ieee8021CnEpIfIndex < CN_MIN_PORTS) ||
        (i4Ieee8021CnEpIfIndex > CN_MAX_PORTS))
    {
        CLI_SET_ERR (CN_CLI_INVALID_PORT_ID);
        return SNMP_FAILURE;
    }

    pCnCompTblEntry = CN_COMP_TBL_ENTRY (u4Ieee8021CnEpComponentId);
    if (pCnCompTblEntry == NULL)
    {
        CLI_SET_ERR (CN_CLI_COMP_ENTRY_NOT_FOUND);

        CN_GBL_TRC (CN_INVALID_CONTEXT,
                    (CN_ALL_FAILURE_TRC | CN_MGMT_TRC),
                    "nmhValidateIndexInstanceIeee8021CnErroredPortTable: FAILED\r\n"
                    "Context is not present in CN !!!\r\n");

        return SNMP_FAILURE;
    }

    CN_PORT_TBL_ENTRY (u4Ieee8021CnEpComponentId, u4PortId, pCnPortTblEntry);
    if (pCnPortTblEntry == NULL)
    {
        CLI_SET_ERR (CN_CLI_PORT_ENTRY_NOT_FOUND);
        CN_TRC_ARG1 (u4Ieee8021CnEpComponentId,
                     (CN_ALL_FAILURE_TRC | CN_MGMT_TRC),
                     "nmhValidateIndexInstanceIeee8021CnErroredPortTable: FAILED\r\n"
                     "Port Table Entry does not exist for port %d\r\n",
                     u4PortId);
        return SNMP_FAILURE;
    }

    CN_PORT_PRI_ENTRY (u4Ieee8021CnEpComponentId, u4PortId,
                       u4Ieee8021CnEpPriority, pCnPortPriEntry);
    if (pCnPortPriEntry == NULL)
    {
        CLI_SET_ERR (CN_CLI_PORT_PRI_ENTRY_NOT_FOUND);
        CN_TRC_ARG2 (u4Ieee8021CnEpComponentId,
                     (CN_ALL_FAILURE_TRC | CN_MGMT_TRC),
                     "nmhValidateIndexInstanceIeee8021CnErroredPortTable: FAILED\r\n"
                     "Port Priority Entry not found for Port %d CNPV %d\r\n",
                     u4PortId, u4Ieee8021CnEpPriority);
        return SNMP_FAILURE;
    }

    if (pCnPortPriEntry->bIsErrorPortEntry != CN_ERROR_PORT_ENTRY_SET)
    {
        CLI_SET_ERR (CN_CLI_ERROR_PORT_ENTRY_NOT_FOUND);
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexIeee8021CnErroredPortTable
 Input       :  The Indices
                Ieee8021CnEpComponentId
                Ieee8021CnEpPriority
                Ieee8021CnEpIfIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexIeee8021CnErroredPortTable (UINT4 *pu4Ieee8021CnEpComponentId,
                                            UINT4 *pu4Ieee8021CnEpPriority,
                                            INT4 *pi4Ieee8021CnEpIfIndex)
{
    UINT4               u4CompId = CN_MIN_CONTEXT;
    UINT4               u4PortId = CN_MIN_PORTS;
    UINT4               u4Priority = CN_ZERO;
    tCnPortPriTblInfo  *pCnPortPriEntry = NULL;

    CN_PORT_PRI_ENTRY (u4CompId, u4PortId, u4Priority, pCnPortPriEntry);
    if ((pCnPortPriEntry != NULL) &&
        (pCnPortPriEntry->bIsErrorPortEntry == CN_ERROR_PORT_ENTRY_SET))
    {
        *pu4Ieee8021CnEpComponentId = u4CompId;
        *pi4Ieee8021CnEpIfIndex = u4PortId;
        *pu4Ieee8021CnEpPriority = u4Priority;
        return SNMP_SUCCESS;
    }
    else
    {
        return (nmhGetNextIndexIeee8021CnErroredPortTable
                (u4CompId, pu4Ieee8021CnEpComponentId, u4Priority,
                 pu4Ieee8021CnEpPriority, u4PortId, pi4Ieee8021CnEpIfIndex));
    }
}

/****************************************************************************
 Function    :  nmhGetNextIndexIeee8021CnErroredPortTable
 Input       :  The Indices
                Ieee8021CnEpComponentId
                nextIeee8021CnEpComponentId
                Ieee8021CnEpPriority
                nextIeee8021CnEpPriority
                Ieee8021CnEpIfIndex
                nextIeee8021CnEpIfIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexIeee8021CnErroredPortTable (UINT4 u4Ieee8021CnEpComponentId,
                                           UINT4
                                           *pu4NextIeee8021CnEpComponentId,
                                           UINT4 u4Ieee8021CnEpPriority,
                                           UINT4 *pu4NextIeee8021CnEpPriority,
                                           INT4 i4Ieee8021CnEpIfIndex,
                                           INT4 *pi4NextIeee8021CnEpIfIndex)
{
    UINT4               u4PortId = CN_ZERO;
    UINT4               u4CompId = CN_MIN_CONTEXT;
    UINT4               u4Priority = CN_ZERO;
    tCnPortTblInfo     *pCnPortTblEntry = NULL;
    tCnPortTblInfo     *pTempPortTblEntry = NULL;
    tCnPortTblInfo      TempPortTblEntry;
    tCnPortPriTblInfo  *pCnPortPriEntry = NULL;
    tCnCompTblInfo     *pCnCompTblEntry = NULL;

    MEMSET (&TempPortTblEntry, CN_ZERO, sizeof (tCnPortTblInfo));

    for (u4CompId = u4Ieee8021CnEpComponentId; u4CompId < CN_MAX_CONTEXTS;
         u4CompId++)
    {
        pCnCompTblEntry = CN_COMP_TBL_ENTRY (u4CompId);
        if (pCnCompTblEntry != NULL)
        {
            if (u4CompId == u4Ieee8021CnEpComponentId)
            {
                u4Priority = u4Ieee8021CnEpPriority;
            }
            else
            {
                u4Priority = CN_ZERO;
            }
            for (; u4Priority < CN_MAX_VLAN_PRIORITY; u4Priority++)
            {
                if (pCnCompTblEntry->pCompPriTbl[u4Priority] != NULL)
                {
                    if ((u4CompId == u4Ieee8021CnEpComponentId) &&
                        (u4Priority == u4Ieee8021CnEpPriority))
                    {
                        TempPortTblEntry.u4PortId =
                            (UINT4) i4Ieee8021CnEpIfIndex;
                        pTempPortTblEntry = &TempPortTblEntry;
                        pCnPortTblEntry =
                            (tCnPortTblInfo *) RBTreeGetNext (pCnCompTblEntry->
                                                              PortTbl,
                                                              pTempPortTblEntry,
                                                              NULL);
                    }
                    else
                    {
                        pCnPortTblEntry = (tCnPortTblInfo *) RBTreeGetFirst
                            (pCnCompTblEntry->PortTbl);
                    }
                    if (pCnPortTblEntry != NULL)
                    {
                        u4PortId = pCnPortTblEntry->u4PortId;
                        CN_PORT_PRI_ENTRY (u4CompId, u4PortId,
                                           u4Priority, pCnPortPriEntry);
                        if ((pCnPortPriEntry != NULL) &&
                            (pCnPortPriEntry->bIsErrorPortEntry ==
                             CN_ERROR_PORT_ENTRY_SET))
                        {
                            *pu4NextIeee8021CnEpComponentId = u4CompId;
                            *pu4NextIeee8021CnEpPriority = u4Priority;
                            *pi4NextIeee8021CnEpIfIndex = u4PortId;
                            return SNMP_SUCCESS;
                        }
                    }
                }
            }
        }
    }
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/* LOW LEVEL Routines for Table : Ieee8021CnCompntPriTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceIeee8021CnCompntPriTable
 Input       :  The Indices
                Ieee8021CnComPriComponentId
                Ieee8021CnComPriPriority
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1 
     
     
     
     
     
     
     
    nmhValidateIndexInstanceIeee8021CnCompntPriTable
    (UINT4 u4Ieee8021CnComPriComponentId, UINT4 u4Ieee8021CnComPriPriority)
{
    tCnCompTblInfo     *pCnCompTblEntry = NULL;
    tCnCompPriTbl      *pCnComPriEntry = NULL;

    if (CN_IS_SHUTDOWN ())
    {
        return SNMP_FAILURE;
    }

    if (CN_IS_VALID_COMP_ID (u4Ieee8021CnComPriComponentId) == OSIX_FALSE)
    {
        return SNMP_FAILURE;
    }

    if (u4Ieee8021CnComPriPriority >= CN_MAX_VLAN_PRIORITY)
    {
        return SNMP_FAILURE;
    }

    pCnCompTblEntry = CN_COMP_TBL_ENTRY (u4Ieee8021CnComPriComponentId);
    if (pCnCompTblEntry == NULL)
    {
        CLI_SET_ERR (CN_CLI_COMP_PRI_ENTRY_NOT_FOUND);
        CN_GBL_TRC (CN_INVALID_CONTEXT,
                    (CN_ALL_FAILURE_TRC | CN_MGMT_TRC),
                    "nmhValidateIndexInstanceIeee8021CnCompntPriTable: FAILED\r\n"
                    "Context is not present in CN !!!\r\n");

        return SNMP_FAILURE;
    }

    CN_COMP_PRI_ENTRY (u4Ieee8021CnComPriComponentId,
                       u4Ieee8021CnComPriPriority, pCnComPriEntry);
    if (pCnComPriEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexIeee8021CnCompntPriTable
 Input       :  The Indices
                Ieee8021CnComPriComponentId
                Ieee8021CnComPriPriority
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexIeee8021CnCompntPriTable (UINT4 *pu4Ieee8021CnComPriComponentId,
                                          UINT4 *pu4Ieee8021CnComPriPriority)
{
    tCnCompPriTbl      *pCnCompPriEntry = NULL;

    CN_COMP_PRI_ENTRY (CN_MIN_CONTEXT, CN_ZERO, pCnCompPriEntry);
    if (pCnCompPriEntry != NULL)
    {
        *pu4Ieee8021CnComPriComponentId = CN_MIN_CONTEXT;
        *pu4Ieee8021CnComPriPriority = CN_ZERO;
        return SNMP_SUCCESS;
    }
    return (nmhGetNextIndexIeee8021CnCompntPriTable
            (CN_MIN_CONTEXT, pu4Ieee8021CnComPriComponentId, CN_ZERO,
             pu4Ieee8021CnComPriPriority));
}

/****************************************************************************
 Function    :  nmhGetNextIndexIeee8021CnCompntPriTable
 Input       :  The Indices
                Ieee8021CnComPriComponentId
                nextIeee8021CnComPriComponentId
                Ieee8021CnComPriPriority
                nextIeee8021CnComPriPriority
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexIeee8021CnCompntPriTable (UINT4 u4Ieee8021CnComPriComponentId,
                                         UINT4
                                         *pu4NextIeee8021CnComPriComponentId,
                                         UINT4 u4Ieee8021CnComPriPriority,
                                         UINT4 *pu4NextIeee8021CnComPriPriority)
{

    UINT4               u4CompId = u4Ieee8021CnComPriComponentId;
    UINT4               u4Priority = u4Ieee8021CnComPriPriority;
    tCnCompPriTbl      *pCnCompPriEntry = NULL;

    for (; u4CompId < CN_MAX_CONTEXTS; u4CompId++)
    {

        if (CN_COMP_TBL_ENTRY (u4CompId) != NULL)
        {
            if (u4CompId == u4Ieee8021CnComPriComponentId)
            {
                u4Priority = u4Ieee8021CnComPriPriority + CN_ONE;
            }
            else
            {
                u4Priority = CN_ZERO;
            }
            for (; u4Priority < CN_MAX_VLAN_PRIORITY; u4Priority++)
            {
                CN_COMP_PRI_ENTRY (u4CompId, u4Priority, pCnCompPriEntry);
                if (pCnCompPriEntry != NULL)
                {
                    *pu4NextIeee8021CnComPriComponentId = u4CompId;
                    *pu4NextIeee8021CnComPriPriority = u4Priority;
                    return SNMP_SUCCESS;
                }
            }
        }
    }
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetIeee8021CnComPriDefModeChoice
 Input       :  The Indices
                Ieee8021CnComPriComponentId
                Ieee8021CnComPriPriority

                The Object 
                retValIeee8021CnComPriDefModeChoice
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021CnComPriDefModeChoice (UINT4 u4Ieee8021CnComPriComponentId,
                                     UINT4 u4Ieee8021CnComPriPriority,
                                     INT4
                                     *pi4RetValIeee8021CnComPriDefModeChoice)
{
    tCnCompPriTbl      *pCnCompPriEntry = NULL;

    CN_COMP_PRI_ENTRY (u4Ieee8021CnComPriComponentId,
                       u4Ieee8021CnComPriPriority, pCnCompPriEntry);
    if (pCnCompPriEntry != NULL)
    {
        *pi4RetValIeee8021CnComPriDefModeChoice =
            pCnCompPriEntry->u1ComPriDefModeChoice;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetIeee8021CnComPriAlternatePriority
 Input       :  The Indices
                Ieee8021CnComPriComponentId
                Ieee8021CnComPriPriority

                The Object 
                retValIeee8021CnComPriAlternatePriority
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
     
     
     
     
     
     
     
    nmhGetIeee8021CnComPriAlternatePriority
    (UINT4 u4Ieee8021CnComPriComponentId,
     UINT4 u4Ieee8021CnComPriPriority,
     UINT4 *pi4RetValIeee8021CnComPriAlternatePriority)
{
    tCnCompPriTbl      *pCnCompPriEntry = NULL;

    CN_COMP_PRI_ENTRY (u4Ieee8021CnComPriComponentId,
                       u4Ieee8021CnComPriPriority, pCnCompPriEntry);
    if (pCnCompPriEntry != NULL)
    {
        *pi4RetValIeee8021CnComPriAlternatePriority =
            pCnCompPriEntry->u1ComPriAltPri;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetIeee8021CnComPriAutoAltPri
 Input       :  The Indices
                Ieee8021CnComPriComponentId
                Ieee8021CnComPriPriority

                The Object 
                retValIeee8021CnComPriAutoAltPri
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021CnComPriAutoAltPri (UINT4 u4Ieee8021CnComPriComponentId,
                                  UINT4 u4Ieee8021CnComPriPriority,
                                  UINT4 *pi4RetValIeee8021CnComPriAutoAltPri)
{
    tCnCompPriTbl      *pCnCompPriEntry = NULL;

    CN_COMP_PRI_ENTRY (u4Ieee8021CnComPriComponentId,
                       u4Ieee8021CnComPriPriority, pCnCompPriEntry);
    if (pCnCompPriEntry != NULL)
    {
        *pi4RetValIeee8021CnComPriAutoAltPri =
            pCnCompPriEntry->u1ComPriAutoAltPri;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetIeee8021CnComPriAdminDefenseMode
 Input       :  The Indices
                Ieee8021CnComPriComponentId
                Ieee8021CnComPriPriority

                The Object 
                retValIeee8021CnComPriAdminDefenseMode
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
     
     
     
     
     
     
     
    nmhGetIeee8021CnComPriAdminDefenseMode
    (UINT4 u4Ieee8021CnComPriComponentId,
     UINT4 u4Ieee8021CnComPriPriority,
     INT4 *pi4RetValIeee8021CnComPriAdminDefenseMode)
{
    tCnCompPriTbl      *pCnCompPriEntry = NULL;

    CN_COMP_PRI_ENTRY (u4Ieee8021CnComPriComponentId,
                       u4Ieee8021CnComPriPriority, pCnCompPriEntry);
    if (pCnCompPriEntry != NULL)
    {
        *pi4RetValIeee8021CnComPriAdminDefenseMode =
            pCnCompPriEntry->u1ComPriAdminDefMode;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetIeee8021CnComPriCreation
 Input       :  The Indices
                Ieee8021CnComPriComponentId
                Ieee8021CnComPriPriority

                The Object 
                retValIeee8021CnComPriCreation
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021CnComPriCreation (UINT4 u4Ieee8021CnComPriComponentId,
                                UINT4 u4Ieee8021CnComPriPriority,
                                INT4 *pi4RetValIeee8021CnComPriCreation)
{
    tCnCompPriTbl      *pCnCompPriEntry = NULL;

    CN_COMP_PRI_ENTRY (u4Ieee8021CnComPriComponentId,
                       u4Ieee8021CnComPriPriority, pCnCompPriEntry);
    if (pCnCompPriEntry != NULL)
    {
        *pi4RetValIeee8021CnComPriCreation =
            pCnCompPriEntry->u1DefaultPortDefModeChoice;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetIeee8021CnComPriLldpInstanceChoice
 Input       :  The Indices
                Ieee8021CnComPriComponentId
                Ieee8021CnComPriPriority

                The Object 
                retValIeee8021CnComPriLldpInstanceChoice
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
     
     
     
     
     
     
     
    nmhGetIeee8021CnComPriLldpInstanceChoice
    (UINT4 u4Ieee8021CnComPriComponentId,
     UINT4 u4Ieee8021CnComPriPriority,
     INT4 *pi4RetValIeee8021CnComPriLldpInstanceChoice)
{
    tCnCompPriTbl      *pCnCompPriEntry = NULL;

    CN_COMP_PRI_ENTRY (u4Ieee8021CnComPriComponentId,
                       u4Ieee8021CnComPriPriority, pCnCompPriEntry);
    if (pCnCompPriEntry != NULL)
    {
        *pi4RetValIeee8021CnComPriLldpInstanceChoice =
            pCnCompPriEntry->u1ComPriLldpInstChoice;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetIeee8021CnComPriLldpInstanceSelector
 Input       :  The Indices
                Ieee8021CnComPriComponentId
                Ieee8021CnComPriPriority

                The Object 
                retValIeee8021CnComPriLldpInstanceSelector
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
     
     
     
     
     
     
     
    nmhGetIeee8021CnComPriLldpInstanceSelector
    (UINT4 u4Ieee8021CnComPriComponentId,
     UINT4 u4Ieee8021CnComPriPriority,
     UINT4 *pi4RetValIeee8021CnComPriLldpInstanceSelector)
{
    tCnCompPriTbl      *pCnCompPriEntry = NULL;

    CN_COMP_PRI_ENTRY (u4Ieee8021CnComPriComponentId,
                       u4Ieee8021CnComPriPriority, pCnCompPriEntry);
    if (pCnCompPriEntry != NULL)
    {
        *pi4RetValIeee8021CnComPriLldpInstanceSelector =
            (INT4) pCnCompPriEntry->u4ComPriLldpInstSelector;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetIeee8021CnComPriRowStatus
 Input       :  The Indices
                Ieee8021CnComPriComponentId
                Ieee8021CnComPriPriority

                The Object 
                retValIeee8021CnComPriRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021CnComPriRowStatus (UINT4 u4Ieee8021CnComPriComponentId,
                                 UINT4 u4Ieee8021CnComPriPriority,
                                 INT4 *pi4RetValIeee8021CnComPriRowStatus)
{
    tCnCompPriTbl      *pCnCompPriEntry = NULL;

    CN_COMP_PRI_ENTRY (u4Ieee8021CnComPriComponentId,
                       u4Ieee8021CnComPriPriority, pCnCompPriEntry);
    if (pCnCompPriEntry != NULL)
    {
        *pi4RetValIeee8021CnComPriRowStatus =
            pCnCompPriEntry->u1ComPriRowStatus;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetIeee8021CnComPriDefModeChoice
 Input       :  The Indices
                Ieee8021CnComPriComponentId
                Ieee8021CnComPriPriority

                The Object 
                setValIeee8021CnComPriDefModeChoice
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIeee8021CnComPriDefModeChoice (UINT4 u4Ieee8021CnComPriComponentId,
                                     UINT4 u4Ieee8021CnComPriPriority,
                                     INT4 i4SetValIeee8021CnComPriDefModeChoice)
{
    tCnCompTblInfo     *pCnCompTblEntry = NULL;
    tCnCompPriTbl      *pCnCompPriEntry = NULL;
    tCnPortPriTblInfo  *pCnPortPriEntry = NULL;
    tCnPortTblInfo     *pCnPortTblEntry = NULL;
    tCnPortTblInfo     *pCnTempPortTblEntry = NULL;
    UINT4               u4PortId = CN_ZERO;

    pCnCompTblEntry = CN_COMP_TBL_ENTRY (u4Ieee8021CnComPriComponentId);
    if (pCnCompTblEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    CN_COMP_PRI_ENTRY (u4Ieee8021CnComPriComponentId,
                       u4Ieee8021CnComPriPriority, pCnCompPriEntry);
    if (pCnCompPriEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pCnCompPriEntry->u1ComPriDefModeChoice ==
        i4SetValIeee8021CnComPriDefModeChoice)
    {
        /*Same value configured */
        return SNMP_SUCCESS;
    }

    pCnCompPriEntry->u1ComPriDefModeChoice = ((UINT1)
                                              i4SetValIeee8021CnComPriDefModeChoice);

    pCnPortTblEntry = (tCnPortTblInfo *) RBTreeGetFirst
        (pCnCompTblEntry->PortTbl);

    while (pCnPortTblEntry != NULL)
    {
        u4PortId = pCnPortTblEntry->u4PortId;
        CN_PORT_PRI_ENTRY (u4Ieee8021CnComPriComponentId, u4PortId,
                           u4Ieee8021CnComPriPriority, pCnPortPriEntry);
        if (pCnPortPriEntry != NULL)
        {
            if (pCnPortPriEntry->u1PortPriDefModeChoice == CN_COMP)
            {
                /*Setting IsAdmin Flag */
                if (i4SetValIeee8021CnComPriDefModeChoice == CN_ADMIN)
                {
                    pCnPortPriEntry->bIsAdminDefMode = OSIX_TRUE;
                }
                else
                {
                    pCnPortPriEntry->bIsAdminDefMode = OSIX_FALSE;
                }

                /*Invoke corresponding state machine function */
                if (pCnCompPriEntry->u1ComPriRowStatus == CN_ACTIVE)
                {
                    CnUtilEnableCnpv (pCnCompTblEntry, pCnPortTblEntry,
                                      pCnPortPriEntry);
                }
            }
        }
        pCnTempPortTblEntry = pCnPortTblEntry;
        pCnPortTblEntry = (tCnPortTblInfo *) RBTreeGetNext
            (pCnCompTblEntry->PortTbl, pCnTempPortTblEntry, NULL);

    }
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhSetIeee8021CnComPriAlternatePriority
Input       :  The Indices
               Ieee8021CnComPriComponentId
               Ieee8021CnComPriPriority

               The Object 
               setValIeee8021CnComPriAlternatePriority
Output      :  The Set Low Lev Routine Take the Indices &
               Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1 
     
     
     
     
     
     
     
    nmhSetIeee8021CnComPriAlternatePriority
    (UINT4 u4Ieee8021CnComPriComponentId,
     UINT4 u4Ieee8021CnComPriPriority,
     UINT4 u4SetValIeee8021CnComPriAlternatePriority)
{
    tCnCompTblInfo     *pCnCompTblEntry = NULL;
    tCnCompPriTbl      *pCnCompPriEntry = NULL;
    tCnPortTblInfo     *pCnPortTblEntry = NULL;
    tCnPortTblInfo     *pTempPortTblEntry = NULL;
    tCnPortPriTblInfo  *pCnPortPriEntry = NULL;
    UINT4               u4PortId = CN_ZERO;
    UINT4               u4Priority = CN_ZERO;
    UINT1               u1ErroredPortFlag = OSIX_FALSE;
    UINT1               u1OutPri = CN_ZERO;

    pCnCompTblEntry = CN_COMP_TBL_ENTRY (u4Ieee8021CnComPriComponentId);
    if (pCnCompTblEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    CN_COMP_PRI_ENTRY (u4Ieee8021CnComPriComponentId,
                       u4Ieee8021CnComPriPriority, pCnCompPriEntry);
    if (pCnCompPriEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    /* Already same value for Alt Priority  */
    if (pCnCompPriEntry->u1ComPriAltPri ==
        u4SetValIeee8021CnComPriAlternatePriority)
    {
        return SNMP_SUCCESS;
    }

    pCnCompPriEntry->u1ComPriAltPri = ((UINT1)
                                       u4SetValIeee8021CnComPriAlternatePriority);

    /* Check for errored port entry */
    while ((u4Priority < CN_MAX_VLAN_PRIORITY)
           && (u1ErroredPortFlag != OSIX_TRUE))
    {
        CN_COMP_PRI_ENTRY (u4Ieee8021CnComPriComponentId, u4Priority,
                           pCnCompPriEntry);
        if (pCnCompPriEntry != NULL)
        {
            if (pCnCompPriEntry->u1CNPV ==
                u4SetValIeee8021CnComPriAlternatePriority)
            {
                u1ErroredPortFlag = OSIX_TRUE;
            }
        }
        u4Priority++;
    }

    CN_COMP_PRI_ENTRY (u4Ieee8021CnComPriComponentId,
                       u4Ieee8021CnComPriPriority, pCnCompPriEntry);
    if (pCnCompPriEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    /*Setting the errored port entry flag */
    pCnPortTblEntry = RBTreeGetFirst (pCnCompTblEntry->PortTbl);
    while (pCnPortTblEntry != NULL)
    {
        u4PortId = pCnPortTblEntry->u4PortId;
        CN_PORT_PRI_ENTRY (u4Ieee8021CnComPriComponentId, u4PortId,
                           u4Ieee8021CnComPriPriority, pCnPortPriEntry);
        if (pCnPortPriEntry != NULL)
        {
            /*Adding an errored port entry */
            if (u1ErroredPortFlag == OSIX_TRUE)
            {
                CnUtilAddErroredPortEntry (pCnPortPriEntry);
            }
            else
            {
                /*Delete Errored Port entry if it exists already */
                if (pCnPortPriEntry->bIsErrorPortEntry ==
                    CN_ERROR_PORT_ENTRY_SET)
                {
                    CnUtilDeleteErroredPortEntry (pCnPortPriEntry);
                }
            }

            if (pCnPortPriEntry->u1PortPriDefModeChoice == CN_COMP)
            {
                CnUtilCalculateOperAltPri (pCnPortPriEntry);

                /*Set Alternate Priority in hardware */
                if (pCnCompPriEntry->u1ComPriRowStatus == CN_ACTIVE)
                {
                    if ((pCnCompTblEntry->bCnCompMasterEnable ==
                         CN_MASTER_ENABLE) &&
                        (pCnPortPriEntry->bIsAdminDefMode == OSIX_TRUE) &&
                        (pCnPortPriEntry->u1PortPriOperDefMode == CN_EDGE))
                    {
                        u1OutPri = pCnPortPriEntry->u1PortPriOperAltPri;
                        if (CnPortQosPriorityMapReq
                            (pCnPortPriEntry->u4CnComPriComponentId,
                             pCnPortPriEntry->u4PortId,
                             pCnPortPriEntry->u1CNPV,
                             &u1OutPri, QOS_ADD_PRIORITY_MAP) == OSIX_FAILURE)
                        {
                            return SNMP_FAILURE;
                        }
                    }
                }
            }
        }

        pTempPortTblEntry = pCnPortTblEntry;
        pCnPortTblEntry = (tCnPortTblInfo *) RBTreeGetNext
            (pCnCompTblEntry->PortTbl, pTempPortTblEntry, NULL);
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhSetIeee8021CnComPriAdminDefenseMode
Input       :  The Indices
               Ieee8021CnComPriComponentId
               Ieee8021CnComPriPriority

               The Object 
               setValIeee8021CnComPriAdminDefenseMode
Output      :  The Set Low Lev Routine Take the Indices &
               Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1 
     
     
     
     
     
     
     
    nmhSetIeee8021CnComPriAdminDefenseMode
    (UINT4 u4Ieee8021CnComPriComponentId,
     UINT4 u4Ieee8021CnComPriPriority,
     INT4 i4SetValIeee8021CnComPriAdminDefenseMode)
{
    tCnCompPriTbl      *pCnCompPriEntry = NULL;
    tCnPortTblInfo     *pCnPortTblEntry = NULL;
    tCnPortTblInfo     *pTempPortTblEntry = NULL;
    tCnPortPriTblInfo  *pCnPortPriEntry = NULL;
    tCnCompTblInfo     *pCnCompTblEntry = NULL;
    UINT4               u4PortId = CN_ZERO;

    pCnCompTblEntry = CN_COMP_TBL_ENTRY (u4Ieee8021CnComPriComponentId);
    if (pCnCompTblEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    CN_COMP_PRI_ENTRY (u4Ieee8021CnComPriComponentId,
                       u4Ieee8021CnComPriPriority, pCnCompPriEntry);
    if (pCnCompPriEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    /*Already same value for Admin Defense mode */
    if (pCnCompPriEntry->u1ComPriAdminDefMode ==
        i4SetValIeee8021CnComPriAdminDefenseMode)
    {
        return SNMP_SUCCESS;
    }

    pCnCompPriEntry->u1ComPriAdminDefMode = ((UINT1)
                                             i4SetValIeee8021CnComPriAdminDefenseMode);

    /* If component defence mode choice is auto, Component defence mode
     *  value is not going to be refered by port priority table .*/
    if (pCnCompPriEntry->u1ComPriDefModeChoice == CN_AUTO)
    {
        return SNMP_SUCCESS;
    }

    /* Defense mode choice of the component is admin */

    pCnPortTblEntry = (tCnPortTblInfo *) RBTreeGetFirst
        (pCnCompTblEntry->PortTbl);

    while (pCnPortTblEntry != NULL)
    {
        u4PortId = pCnPortTblEntry->u4PortId;

        CN_PORT_PRI_ENTRY (u4Ieee8021CnComPriComponentId, u4PortId,
                           u4Ieee8021CnComPriPriority, pCnPortPriEntry);
        if (pCnPortPriEntry != NULL)
        {
            if ((pCnPortPriEntry->u1PortPriDefModeChoice == CN_COMP) &&
                (pCnCompPriEntry->u1ComPriRowStatus == CN_ACTIVE))
            {
                /*Invoke corresponding state machine function */
                CnUtilEnableCnpv (pCnCompTblEntry, pCnPortTblEntry,
                                  pCnPortPriEntry);
            }
        }
        pTempPortTblEntry = pCnPortTblEntry;
        pCnPortTblEntry = (tCnPortTblInfo *) RBTreeGetNext
            (pCnCompTblEntry->PortTbl, pTempPortTblEntry, NULL);
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhSetIeee8021CnComPriCreation
Input       :  The Indices
               Ieee8021CnComPriComponentId
               Ieee8021CnComPriPriority

               The Object 
               setValIeee8021CnComPriCreation
Output      :  The Set Low Lev Routine Take the Indices &
               Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetIeee8021CnComPriCreation (UINT4 u4Ieee8021CnComPriComponentId,
                                UINT4 u4Ieee8021CnComPriPriority,
                                INT4 i4SetValIeee8021CnComPriCreation)
{
    tCnCompPriTbl      *pCnCompPriEntry = NULL;
    tCnPortTblInfo     *pCnPortTblEntry = NULL;
    tCnPortTblInfo     *pCnTempPortTblEntry = NULL;
    tCnPortPriTblInfo  *pCnPortPriEntry = NULL;
    tCnCompTblInfo     *pCnCompTblEntry = NULL;
    UINT4               u4PortId = CN_ZERO;

    pCnCompTblEntry = CN_COMP_TBL_ENTRY (u4Ieee8021CnComPriComponentId);
    if (pCnCompTblEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    CN_COMP_PRI_ENTRY (u4Ieee8021CnComPriComponentId,
                       u4Ieee8021CnComPriPriority, pCnCompPriEntry);

    if (pCnCompPriEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    /*Already same value for PortDefaultDefModeChoice */
    if (pCnCompPriEntry->u1DefaultPortDefModeChoice ==
        i4SetValIeee8021CnComPriCreation)
    {
        return SNMP_SUCCESS;
    }

    pCnCompPriEntry->u1DefaultPortDefModeChoice = ((UINT1)
                                                   i4SetValIeee8021CnComPriCreation);

    /* RowStatus NOT_READY ensures port priority table entries are created
     * But not yet activated */
    if (pCnCompPriEntry->u1ComPriRowStatus == CN_NOT_READY)
    {
        if (pCnCompPriEntry->u1DefaultPortDefModeChoice == CN_AUTO_DISABLE)
        {
            pCnPortTblEntry = (tCnPortTblInfo *) RBTreeGetFirst
                (pCnCompTblEntry->PortTbl);
            /*Filling DefenseModeChoice of Newly created port 
             * entries with CN_ADMIN */
            while (pCnPortTblEntry != NULL)
            {
                u4PortId = pCnPortTblEntry->u4PortId;
                CN_PORT_PRI_ENTRY (u4Ieee8021CnComPriComponentId, u4PortId,
                                   u4Ieee8021CnComPriPriority, pCnPortPriEntry);
                if (pCnPortPriEntry != NULL)
                {
                    pCnPortPriEntry->u1PortPriDefModeChoice = CN_ADMIN;
                    pCnPortPriEntry->u1PortPriAdminDefMode = CN_DISABLED;
                    pCnPortPriEntry->bIsAdminDefMode = OSIX_TRUE;
                    CnUtilCnpdAdminDefenseMode (pCnPortPriEntry);
                }
                pCnTempPortTblEntry = pCnPortTblEntry;
                pCnPortTblEntry = (tCnPortTblInfo *) RBTreeGetNext
                    (pCnCompTblEntry->PortTbl, pCnTempPortTblEntry, NULL);
            }
        }
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhSetIeee8021CnComPriLldpInstanceChoice
Input       :  The Indices
               Ieee8021CnComPriComponentId
               Ieee8021CnComPriPriority

               The Object 
               setValIeee8021CnComPriLldpInstanceChoice
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetIeee8021CnComPriLldpInstanceChoice (UINT4 u4Ieee8021CnComPriComponentId,
                                          UINT4 u4Ieee8021CnComPriPriority,
                                          INT4
                                          i4SetValIeee8021CnComPriLldpInstanceChoice)
{
    tCnCompPriTbl      *pCnCompPriEntry = NULL;
    tCnPortPriTblInfo  *pCnPortPriEntry = NULL;
    tCnPortTblInfo     *pCnPortTblEntry = NULL;
    tCnPortTblInfo     *pTempPortTblEntry = NULL;
    tCnCompTblInfo     *pCncompTblEntry = NULL;
    UINT4               u4PortId = CN_ZERO;

    pCncompTblEntry = CN_COMP_TBL_ENTRY (u4Ieee8021CnComPriComponentId);
    if (pCncompTblEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    CN_COMP_PRI_ENTRY (u4Ieee8021CnComPriComponentId,
                       u4Ieee8021CnComPriPriority, pCnCompPriEntry);
    if (pCnCompPriEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    /*Already same value for LldpInstChoice */
    if (pCnCompPriEntry->u1ComPriLldpInstChoice ==
        i4SetValIeee8021CnComPriLldpInstanceChoice)
    {
        return SNMP_SUCCESS;
    }

    pCnCompPriEntry->u1ComPriLldpInstChoice = ((UINT1)
                                               i4SetValIeee8021CnComPriLldpInstanceChoice);

    pCnPortTblEntry = (tCnPortTblInfo *) RBTreeGetFirst
        (pCncompTblEntry->PortTbl);

    while (pCnPortTblEntry != NULL)
    {
        u4PortId = pCnPortTblEntry->u4PortId;
        CN_PORT_PRI_ENTRY (u4Ieee8021CnComPriComponentId,
                           u4PortId,
                           u4Ieee8021CnComPriPriority, pCnPortPriEntry);
        if (pCnPortPriEntry != NULL)
        {
            if ((pCnPortPriEntry->u1PortPriLldpInstChoice == CN_LLDP_COMP) &&
                (pCnCompPriEntry->u1ComPriRowStatus == CN_ACTIVE))
            {
                /* Resetting Defense mode and Updating LLDP module since there
                 * is change in LLDP instance selector in Component table */
                CnUtilEnableCnpv (pCncompTblEntry, pCnPortTblEntry,
                                  pCnPortPriEntry);
            }
        }
        pTempPortTblEntry = pCnPortTblEntry;
        pCnPortTblEntry = (tCnPortTblInfo *) RBTreeGetNext
            (pCncompTblEntry->PortTbl, pTempPortTblEntry, NULL);
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhSetIeee8021CnComPriLldpInstanceSelector
Input       :  The Indices
               Ieee8021CnComPriComponentId
               Ieee8021CnComPriPriority

               The Object 
               setValIeee8021CnComPriLldpInstanceSelector
Output      :  The Set Low Lev Routine Take the Indices &
               Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1 
     
     
     
     
     
     
     
    nmhSetIeee8021CnComPriLldpInstanceSelector
    (UINT4 u4Ieee8021CnComPriComponentId,
     UINT4 u4Ieee8021CnComPriPriority,
     UINT4 u4SetValIeee8021CnComPriLldpInstanceSelector)
{
    tCnCompPriTbl      *pCnCompPriEntry = NULL;

    CN_COMP_PRI_ENTRY (u4Ieee8021CnComPriComponentId,
                       u4Ieee8021CnComPriPriority, pCnCompPriEntry);
    if (pCnCompPriEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pCnCompPriEntry->u4ComPriLldpInstSelector =
        u4SetValIeee8021CnComPriLldpInstanceSelector;
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhSetIeee8021CnComPriRowStatus
Input       :  The Indices
               Ieee8021CnComPriComponentId
               Ieee8021CnComPriPriority

               The Object 
               setValIeee8021CnComPriRowStatus
Output      :  The Set Low Lev Routine Take the Indices &
               Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetIeee8021CnComPriRowStatus (UINT4 u4Ieee8021CnComPriComponentId,
                                 UINT4 u4Ieee8021CnComPriPriority,
                                 INT4 i4SetValIeee8021CnComPriRowStatus)
{
    tCnCompPriTbl      *pCnCompPriEntry = NULL;
    tCnCompTblInfo     *pCnCompTblEntry = NULL;

    pCnCompTblEntry = CN_COMP_TBL_ENTRY (u4Ieee8021CnComPriComponentId);
    if (pCnCompTblEntry == NULL)
    {
        CN_GBL_TRC (CN_INVALID_CONTEXT,
                    (CN_MGMT_TRC | CN_ALL_FAILURE_TRC),
                    "nmhSetIeee8021CnComPriRowStatus: FAILED \r\n"
                    "Context is not present in CN !!!\r\n");

        return SNMP_FAILURE;
    }
    switch (i4SetValIeee8021CnComPriRowStatus)
    {
        case CN_CREATE_AND_GO:

            pCnCompPriEntry =
                pCnCompTblEntry->pCompPriTbl[u4Ieee8021CnComPriPriority];
            if (pCnCompPriEntry != NULL)
            {
                /* Entry has already been created */
                CN_TRC_ARG1 (u4Ieee8021CnComPriComponentId,
                             (CN_MGMT_TRC | CN_ALL_FAILURE_TRC),
                             "nmhSetIeee8021CnComPriRowStatus: FAILED \r\n"
                             "CNPV %d Crated Already !!!\r\n",
                             u4Ieee8021CnComPriPriority);
                return SNMP_FAILURE;
            }

            /*Getting and checking for master enabled status in component */
            /*Allocate Memory and Set Default Values */
            if (CnUtilCreateCompPriEntry (pCnCompTblEntry,
                                          u4Ieee8021CnComPriPriority) ==
                OSIX_FAILURE)
            {
                CN_TRC_ARG1 (u4Ieee8021CnComPriComponentId,
                             (CN_MGMT_TRC | CN_ALL_FAILURE_TRC),
                             "nmhSetIeee8021CnComPriRowStatus: FAILED \r\n"
                             "Create CompPriEntry Failed for "
                             "CNPV %d\r\n", u4Ieee8021CnComPriPriority);

                return SNMP_FAILURE;
            }

            /*Set NPAPI and Start State Machine */
            if (pCnCompTblEntry->bCnCompMasterEnable != CN_MASTER_DISABLE)
            {
                CnUtilActivateCNPV (pCnCompTblEntry,
                                    u4Ieee8021CnComPriPriority);
            }

            /*Set row status as active */
            pCnCompPriEntry =
                pCnCompTblEntry->pCompPriTbl[u4Ieee8021CnComPriPriority];
            if (pCnCompPriEntry == NULL)
            {
                return SNMP_FAILURE;
            }
            pCnCompPriEntry->u1ComPriRowStatus = CN_ACTIVE;
            return SNMP_SUCCESS;

        case CN_ACTIVE:

            pCnCompPriEntry =
                pCnCompTblEntry->pCompPriTbl[u4Ieee8021CnComPriPriority];
            if (pCnCompPriEntry != NULL)
            {
                /*Getting and checking for master enabled status in component */
                if (pCnCompTblEntry->bCnCompMasterEnable != CN_MASTER_DISABLE)
                {
                    /*Set NPAPI and Start State Machine */
                    CnUtilActivateCNPV (pCnCompTblEntry,
                                        u4Ieee8021CnComPriPriority);
                }
                pCnCompPriEntry->u1ComPriRowStatus = CN_ACTIVE;
                return SNMP_SUCCESS;
            }
            CN_TRC_ARG1 (u4Ieee8021CnComPriComponentId,
                         (CN_MGMT_TRC | CN_ALL_FAILURE_TRC),
                         "nmhSetIeee8021CnComPriRowStatus: FAILED \r\n"
                         "Entry not found for CNPV %d \r\n",
                         u4Ieee8021CnComPriPriority);
            return SNMP_FAILURE;

        case CN_CREATE_AND_WAIT:
            pCnCompPriEntry =
                pCnCompTblEntry->pCompPriTbl[u4Ieee8021CnComPriPriority];
            if (pCnCompPriEntry != NULL)
            {
                /* Entry has already been created */
                CN_TRC_ARG1 (u4Ieee8021CnComPriComponentId,
                             (CN_MGMT_TRC | CN_ALL_FAILURE_TRC),
                             "nmhSetIeee8021CnComPriRowStatus: FAILED \r\n"
                             "CNPV %d Created Already !!!\r\n",
                             u4Ieee8021CnComPriPriority);

                return SNMP_FAILURE;
            }

            if (CnUtilCreateCompPriEntry (pCnCompTblEntry,
                                          u4Ieee8021CnComPriPriority) ==
                OSIX_FAILURE)
            {
                CN_TRC_ARG1 (u4Ieee8021CnComPriComponentId,
                             (CN_MGMT_TRC | CN_ALL_FAILURE_TRC),
                             "nmhSetIeee8021CnComPriRowStatus: FAILED\r\n"
                             "CreateCompPriEntry Failed for CNPV %d\r\n",
                             u4Ieee8021CnComPriPriority);
                return SNMP_FAILURE;
            }
            pCnCompPriEntry =
                pCnCompTblEntry->pCompPriTbl[u4Ieee8021CnComPriPriority];
            if (pCnCompPriEntry == NULL)
            {
                return SNMP_FAILURE;
            }
            pCnCompPriEntry->u1ComPriRowStatus = CN_NOT_READY;
            return SNMP_SUCCESS;

        case CN_NOT_IN_SERVICE:
            pCnCompPriEntry =
                pCnCompTblEntry->pCompPriTbl[u4Ieee8021CnComPriPriority];
            if (pCnCompPriEntry != NULL)
            {
                pCnCompPriEntry->u1ComPriRowStatus = CN_NOT_IN_SERVICE;

                /* Stop the state Machine */
                CnUtilDeActivateCNPV (pCnCompTblEntry,
                                      u4Ieee8021CnComPriPriority);
                return SNMP_SUCCESS;
            }
            /*Entry not created */
            CN_TRC_ARG1 (u4Ieee8021CnComPriComponentId,
                         (CN_MGMT_TRC | CN_ALL_FAILURE_TRC),
                         "nmhSetIeee8021CnComPriRowStatus: FAILED\r\n"
                         "CNPV %d not yet Created !!!\r\n",
                         u4Ieee8021CnComPriPriority);

            return SNMP_FAILURE;

        case DESTROY:
            pCnCompPriEntry =
                pCnCompTblEntry->pCompPriTbl[u4Ieee8021CnComPriPriority];
            if (pCnCompPriEntry == NULL)
            {
                /*Entry not created */
                CN_TRC_ARG1 (u4Ieee8021CnComPriComponentId,
                             (CN_MGMT_TRC | CN_ALL_FAILURE_TRC),
                             "nmhSetIeee8021CnComPriRowStatus: "
                             "CNPV %d not yet created !!!\r\n",
                             u4Ieee8021CnComPriPriority);

                return SNMP_FAILURE;
            }

            /* Stop State Machine if it is already active */
            if (pCnCompPriEntry->u1ComPriRowStatus == CN_ACTIVE)
            {
                CnUtilDeActivateCNPV (pCnCompTblEntry,
                                      u4Ieee8021CnComPriPriority);
            }

            /*Release Allocated Memory */
            if (CnUtilDeleteCNPV (pCnCompTblEntry,
                                  u4Ieee8021CnComPriPriority) == OSIX_SUCCESS)
            {
                CN_TRC_ARG1 (u4Ieee8021CnComPriComponentId, CN_MGMT_TRC,
                             "nmhSetIeee8021CnComPriRowStatus: SUCCESS "
                             "CNPV %d Deleted \r\n",
                             u4Ieee8021CnComPriPriority);
                return SNMP_SUCCESS;

            }
            CN_TRC_ARG1 (u4Ieee8021CnComPriComponentId,
                         (CN_MGMT_TRC | CN_ALL_FAILURE_TRC),
                         "nmhSetIeee8021CnComPriRowStatus: FAILED in "
                         "CNPV %d Deletion\r\n", u4Ieee8021CnComPriPriority);

            return SNMP_FAILURE;

        default:
            break;
    }
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
Function    :  nmhTestv2Ieee8021CnComPriDefModeChoice
Input       :  The Indices
               Ieee8021CnComPriComponentId
               Ieee8021CnComPriPriority

               The Object 
               testValIeee8021CnComPriDefModeChoice
Output      :  The Test Low Lev Routine Take the Indices &
               Test whether that Value is Valid Input for Set.
               Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
               SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
               SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
               SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
               SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
               SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2Ieee8021CnComPriDefModeChoice (UINT4 *pu4ErrorCode,
                                        UINT4 u4Ieee8021CnComPriComponentId,
                                        UINT4 u4Ieee8021CnComPriPriority,
                                        INT4
                                        i4TestValIeee8021CnComPriDefModeChoice)
{
    tCnCompPriTbl      *pCnComPriEntry = NULL;
    tCnCompTblInfo     *pCnCompTblEntry = NULL;

    if (CN_IS_SHUTDOWN ())
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (CN_IS_VALID_COMP_ID (u4Ieee8021CnComPriComponentId) == OSIX_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CN_CLI_INVALID_COMP_ID);
        return SNMP_FAILURE;
    }

    if (u4Ieee8021CnComPriPriority >= CN_MAX_VLAN_PRIORITY)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CN_CLI_INVALID_CNPV);
        return SNMP_FAILURE;
    }

    if ((i4TestValIeee8021CnComPriDefModeChoice != CN_ADMIN) &&
        (i4TestValIeee8021CnComPriDefModeChoice != CN_AUTO))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    pCnCompTblEntry = CN_COMP_TBL_ENTRY (u4Ieee8021CnComPriComponentId);
    if (pCnCompTblEntry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CN_CLI_COMP_ENTRY_NOT_FOUND);
        return SNMP_FAILURE;
    }

    CN_COMP_PRI_ENTRY (u4Ieee8021CnComPriComponentId,
                       u4Ieee8021CnComPriPriority, pCnComPriEntry);
    if (pCnComPriEntry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CN_CLI_COMP_PRI_ENTRY_NOT_FOUND);
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhTestv2Ieee8021CnComPriAlternatePriority
Input       :  The Indices
               Ieee8021CnComPriComponentId
               Ieee8021CnComPriPriority

               The Object 
               testValIeee8021CnComPriAlternatePriority
Output      :  The Test Low Lev Routine Take the Indices &
               Test whether that Value is Valid Input for Set.
               Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
               SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
               SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
               SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
               SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
               SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2Ieee8021CnComPriAlternatePriority (UINT4 *pu4ErrorCode,
                                            UINT4 u4Ieee8021CnComPriComponentId,
                                            UINT4 u4Ieee8021CnComPriPriority,
                                            UINT4
                                            u4TestValIeee8021CnComPriAlternatePriority)
{
    tCnCompPriTbl      *pCnComPriEntry = NULL;
    tCnCompTblInfo     *pCnCompTblEntry = NULL;

    if (CN_IS_SHUTDOWN ())
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (CN_IS_VALID_COMP_ID (u4Ieee8021CnComPriComponentId) == OSIX_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CN_CLI_INVALID_COMP_ID);
        return SNMP_FAILURE;
    }

    if (u4Ieee8021CnComPriPriority >= CN_MAX_VLAN_PRIORITY)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CN_CLI_INVALID_CNPV);
        return SNMP_FAILURE;
    }

    if (u4TestValIeee8021CnComPriAlternatePriority >= CN_MAX_VLAN_PRIORITY)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CN_CLI_INVALID_VLAN_PRIORITY);
        return SNMP_FAILURE;
    }

    pCnCompTblEntry = CN_COMP_TBL_ENTRY (u4Ieee8021CnComPriComponentId);
    if (pCnCompTblEntry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CN_CLI_COMP_ENTRY_NOT_FOUND);
        return SNMP_FAILURE;
    }

    CN_COMP_PRI_ENTRY (u4Ieee8021CnComPriComponentId,
                       u4Ieee8021CnComPriPriority, pCnComPriEntry);
    if (pCnComPriEntry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CN_CLI_COMP_PRI_ENTRY_NOT_FOUND);
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhTestv2Ieee8021CnComPriAdminDefenseMode
Input       :  The Indices
               Ieee8021CnComPriComponentId
               Ieee8021CnComPriPriority

               The Object 
               testValIeee8021CnComPriAdminDefenseMode
Output      :  The Test Low Lev Routine Take the Indices &
               Test whether that Value is Valid Input for Set.
               Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
               SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
               SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
               SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
               SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
               SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1 
     
     
     
     
     
     
     
    nmhTestv2Ieee8021CnComPriAdminDefenseMode
    (UINT4 *pu4ErrorCode, UINT4 u4Ieee8021CnComPriComponentId,
     UINT4 u4Ieee8021CnComPriPriority,
     INT4 i4TestValIeee8021CnComPriAdminDefenseMode)
{
    tCnCompPriTbl      *pCnComPriEntry = NULL;
    tCnCompTblInfo     *pCnCompTblEntry = NULL;

    if (CN_IS_SHUTDOWN ())
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (CN_IS_VALID_COMP_ID (u4Ieee8021CnComPriComponentId) == OSIX_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CN_CLI_INVALID_COMP_ID);
        return SNMP_FAILURE;
    }

    if (u4Ieee8021CnComPriPriority >= CN_MAX_VLAN_PRIORITY)
    {
        CLI_SET_ERR (CN_CLI_INVALID_CNPV);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if ((i4TestValIeee8021CnComPriAdminDefenseMode != CN_DISABLED) &&
        (i4TestValIeee8021CnComPriAdminDefenseMode != CN_INTERIOR) &&
        (i4TestValIeee8021CnComPriAdminDefenseMode != CN_INTERIOR_READY) &&
        (i4TestValIeee8021CnComPriAdminDefenseMode != CN_EDGE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    pCnCompTblEntry = CN_COMP_TBL_ENTRY (u4Ieee8021CnComPriComponentId);
    if (pCnCompTblEntry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CN_CLI_COMP_ENTRY_NOT_FOUND);
        return SNMP_FAILURE;
    }

    CN_COMP_PRI_ENTRY (u4Ieee8021CnComPriComponentId,
                       u4Ieee8021CnComPriPriority, pCnComPriEntry);
    if (pCnComPriEntry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CN_CLI_COMP_PRI_ENTRY_NOT_FOUND);
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhTestv2Ieee8021CnComPriCreation
Input       :  The Indices
               Ieee8021CnComPriComponentId
               Ieee8021CnComPriPriority

               The Object 
               testValIeee8021CnComPriCreation
Output      :  The Test Low Lev Routine Take the Indices &
               Test whether that Value is Valid Input for Set.
               Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
               SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
               SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
               SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
               SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
               SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2Ieee8021CnComPriCreation (UINT4 *pu4ErrorCode,
                                   UINT4 u4Ieee8021CnComPriComponentId,
                                   UINT4 u4Ieee8021CnComPriPriority,
                                   INT4 i4TestValIeee8021CnComPriCreation)
{
    tCnCompPriTbl      *pCnComPriEntry = NULL;
    tCnCompTblInfo     *pCnCompTblEntry = NULL;

    if (CN_IS_SHUTDOWN ())
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (CN_IS_VALID_COMP_ID (u4Ieee8021CnComPriComponentId) == OSIX_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CN_CLI_INVALID_COMP_ID);
        return SNMP_FAILURE;
    }

    if (u4Ieee8021CnComPriPriority >= CN_MAX_VLAN_PRIORITY)
    {
        CLI_SET_ERR (CN_CLI_INVALID_CNPV);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if ((i4TestValIeee8021CnComPriCreation != CN_AUTO_DISABLE) &&
        (i4TestValIeee8021CnComPriCreation != CN_AUTO_ENABLE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    pCnCompTblEntry = CN_COMP_TBL_ENTRY (u4Ieee8021CnComPriComponentId);
    if (pCnCompTblEntry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CN_CLI_COMP_ENTRY_NOT_FOUND);
        return SNMP_FAILURE;
    }

    CN_COMP_PRI_ENTRY (u4Ieee8021CnComPriComponentId,
                       u4Ieee8021CnComPriPriority, pCnComPriEntry);
    if (pCnComPriEntry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CN_CLI_ENTRY_NOT_FOUND);
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhTestv2Ieee8021CnComPriLldpInstanceChoice
Input       :  The Indices
               Ieee8021CnComPriComponentId
               Ieee8021CnComPriPriority
               
               The Object 
               testValIeee8021CnComPriLldpInstanceChoice
Output      :  The Test Low Lev Routine Take the Indices &
               Test whether that Value is Valid Input for Set.
               Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
               SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
               SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
               SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
               SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
               SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1 
     
     
     
     
     
     
     
    nmhTestv2Ieee8021CnComPriLldpInstanceChoice
    (UINT4 *pu4ErrorCode,
     UINT4 u4Ieee8021CnComPriComponentId,
     UINT4 u4Ieee8021CnComPriPriority,
     INT4 i4TestValIeee8021CnComPriLldpInstanceChoice)
{
    tCnCompPriTbl      *pCnComPriEntry = NULL;
    tCnCompTblInfo     *pCnCompTblEntry = NULL;

    if (CN_IS_SHUTDOWN ())
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (CN_IS_VALID_COMP_ID (u4Ieee8021CnComPriComponentId) == OSIX_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CN_CLI_INVALID_COMP_ID);
        return SNMP_FAILURE;
    }

    if (u4Ieee8021CnComPriPriority >= CN_MAX_VLAN_PRIORITY)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CN_CLI_INVALID_CNPV);
        return SNMP_FAILURE;
    }

    if ((i4TestValIeee8021CnComPriLldpInstanceChoice != CN_LLDP_NONE) &&
        (i4TestValIeee8021CnComPriLldpInstanceChoice != CN_LLDP_ADMIN))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    pCnCompTblEntry = CN_COMP_TBL_ENTRY (u4Ieee8021CnComPriComponentId);
    if (pCnCompTblEntry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CN_CLI_COMP_ENTRY_NOT_FOUND);
        return SNMP_FAILURE;
    }

    CN_COMP_PRI_ENTRY (u4Ieee8021CnComPriComponentId,
                       u4Ieee8021CnComPriPriority, pCnComPriEntry);
    if (pCnComPriEntry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CN_CLI_COMP_PRI_ENTRY_NOT_FOUND);
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhTestv2Ieee8021CnComPriLldpInstanceSelector
Input       :  The Indices
               Ieee8021CnComPriComponentId
               Ieee8021CnComPriPriority

               The Object 
               testValIeee8021CnComPriLldpInstanceSelector
Output      :  The Test Low Lev Routine Take the Indices &
               Test whether that Value is Valid Input for Set.
               Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
               SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
               SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
               SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
               SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
               SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1 
     
     
     
     
     
     
     
    nmhTestv2Ieee8021CnComPriLldpInstanceSelector
    (UINT4 *pu4ErrorCode,
     UINT4 u4Ieee8021CnComPriComponentId,
     UINT4 u4Ieee8021CnComPriPriority,
     UINT4 u4TestValIeee8021CnComPriLldpInstanceSelector)
{
    tCnCompPriTbl      *pCnComPriEntry = NULL;
    tCnCompTblInfo     *pCnCompTblEntry = NULL;

    if (CN_IS_SHUTDOWN ())
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (CN_IS_VALID_COMP_ID (u4Ieee8021CnComPriComponentId) == OSIX_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CN_CLI_INVALID_COMP_ID);
        return SNMP_FAILURE;
    }

    if (u4Ieee8021CnComPriPriority >= CN_MAX_VLAN_PRIORITY)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CN_CLI_INVALID_CNPV);
        return SNMP_FAILURE;
    }

    if ((u4TestValIeee8021CnComPriLldpInstanceSelector <
         CN_MIN_LLDP_INST_SELECTOR) ||
        (u4TestValIeee8021CnComPriLldpInstanceSelector >
         CN_MAX_LLDP_INST_SELECTOR))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CN_CLI_INVALID_LLDP_INST_SELECTOR);
        return SNMP_FAILURE;
    }

    pCnCompTblEntry = CN_COMP_TBL_ENTRY (u4Ieee8021CnComPriComponentId);
    if (pCnCompTblEntry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CN_CLI_COMP_ENTRY_NOT_FOUND);
        return SNMP_FAILURE;
    }

    CN_COMP_PRI_ENTRY (u4Ieee8021CnComPriComponentId,
                       u4Ieee8021CnComPriPriority, pCnComPriEntry);
    if (pCnComPriEntry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CN_CLI_COMP_PRI_ENTRY_NOT_FOUND);
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhTestv2Ieee8021CnComPriRowStatus
Input       :  The Indices
               Ieee8021CnComPriComponentId
               Ieee8021CnComPriPriority

               The Object 
               testValIeee8021CnComPriRowStatus
Output      :  The Test Low Lev Routine Take the Indices &
               Test whether that Value is Valid Input for Set.
               Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
               SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
               SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
               SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
               SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
               SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2Ieee8021CnComPriRowStatus (UINT4 *pu4ErrorCode,
                                    UINT4 u4Ieee8021CnComPriComponentId,
                                    UINT4 u4Ieee8021CnComPriPriority,
                                    INT4 i4TestValIeee8021CnComPriRowStatus)
{
    tCnCompPriTbl      *pCnCompPriEntry = NULL;
    tCnCompTblInfo     *pCnCompTblEntry = NULL;

    if (CN_IS_SHUTDOWN ())
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (CN_IS_VALID_COMP_ID (u4Ieee8021CnComPriComponentId) == OSIX_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CN_CLI_INVALID_COMP_ID);
        return SNMP_FAILURE;
    }

    if (u4Ieee8021CnComPriPriority >= CN_MAX_VLAN_PRIORITY)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CN_CLI_INVALID_CNPV);
        return SNMP_FAILURE;
    }

    if ((i4TestValIeee8021CnComPriRowStatus != CN_ACTIVE) &&
        (i4TestValIeee8021CnComPriRowStatus != CN_NOT_IN_SERVICE) &&
        (i4TestValIeee8021CnComPriRowStatus != CN_CREATE_AND_GO) &&
        (i4TestValIeee8021CnComPriRowStatus != CN_CREATE_AND_WAIT) &&
        (i4TestValIeee8021CnComPriRowStatus != CN_DESTROY))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    pCnCompTblEntry = CN_COMP_TBL_ENTRY (u4Ieee8021CnComPriComponentId);
    if (pCnCompTblEntry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        CLI_SET_ERR (CN_CLI_COMP_ENTRY_NOT_FOUND);
        return SNMP_FAILURE;
    }

    CN_COMP_PRI_ENTRY (u4Ieee8021CnComPriComponentId,
                       u4Ieee8021CnComPriPriority, pCnCompPriEntry);
    switch (i4TestValIeee8021CnComPriRowStatus)
    {
        case CN_ACTIVE:
            if (pCnCompPriEntry == NULL)
            {
                *pu4ErrorCode = SNMP_ERR_NO_CREATION;
                CLI_SET_ERR (CN_CLI_COMP_PRI_ENTRY_NOT_FOUND);
                return SNMP_FAILURE;
            }
            /*Check for number of CNPVs created in the component */
            if (CnUtilCheckMaxCNPV (u4Ieee8021CnComPriComponentId) ==
                OSIX_FAILURE)
            {
                /*Already 7 CNPVs are active in the component */
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                CLI_SET_ERR (CN_CLI_CNPV_CREATION_VIOLATED);
                CN_TRC (u4Ieee8021CnComPriComponentId, CN_ALL_FAILURE_TRC,
                        "CnUtilCreateCompPriEntry: FAILED\r\n"
                        "Maximum possible CNPVs reached "
                        "for this context\r\n");
                return SNMP_FAILURE;
            }
            break;

        case CN_NOT_IN_SERVICE:
            if (pCnCompPriEntry == NULL)
            {
                *pu4ErrorCode = SNMP_ERR_NO_CREATION;
                CLI_SET_ERR (CN_CLI_COMP_PRI_ENTRY_NOT_FOUND);
                return SNMP_FAILURE;
            }
            if (pCnCompPriEntry->u1ComPriRowStatus == CN_NOT_READY)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
                CLI_SET_ERR (CN_CLI_ENTRY_NOT_ACTIVE);
                return SNMP_FAILURE;
            }
            break;

        case CN_CREATE_AND_GO:
            if (pCnCompPriEntry != NULL)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
                CLI_SET_ERR (CN_CLI_CNPV_ALREADY_EXIST);
                CN_TRC_ARG1 (u4Ieee8021CnComPriComponentId,
                             (CN_MGMT_TRC),
                             "nmhTestv2Ieee8021CnComPriRowStatus: FAILED\r\n"
                             "Entry already exists for CNPV %d\r\n",
                             u4Ieee8021CnComPriPriority);
                return SNMP_FAILURE;
            }

            /*Check for number of CNPVs created in the component */
            if (CnUtilCheckMaxCNPV (u4Ieee8021CnComPriComponentId) ==
                OSIX_FAILURE)
            {
                /*Already 7 CNPVs are active in the component */
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                CLI_SET_ERR (CN_CLI_CNPV_CREATION_VIOLATED);
                CN_TRC (u4Ieee8021CnComPriComponentId, CN_MGMT_TRC,
                        "CnUtilCreateCompPriEntry: FAILED\r\n"
                        "Maximum possible active CNPVs reached "
                        "for this Context\r\n");
                return SNMP_FAILURE;
            }

            /*Invoke the below function to check if an alternate priority for
             *the CNPV to be created is already configured in QOS.
             *if so, CNPV can't be created till
             *that configuration is removed.*/

            if (CnUtilCheckQoSPriorityMap (pCnCompTblEntry,
                                           u4Ieee8021CnComPriPriority) ==
                OSIX_FAILURE)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                CN_TRC_ARG1 (u4Ieee8021CnComPriComponentId,
                             (CN_MGMT_TRC | CN_ALL_FAILURE_TRC),
                             "nmhSetIeee8021CnComPriRowStatus: FAILED\r\n"
                             "QoS Priority Map Entry exists - CNPV %d can not "
                             "be created\r\n", u4Ieee8021CnComPriPriority);
                CLI_SET_ERR (CN_CLI_PRIORITY_MAP_ALREADY_EXIST);
                return SNMP_FAILURE;
            }
            break;

        case CN_CREATE_AND_WAIT:
            if (pCnCompPriEntry != NULL)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
                CLI_SET_ERR (CN_CLI_CNPV_ALREADY_EXIST);
                CN_TRC_ARG1 (u4Ieee8021CnComPriComponentId,
                             (CN_MGMT_TRC),
                             "nmhTestv2Ieee8021CnComPriRowStatus: FAILED\r\n"
                             "Entry already exists for CNPV %d\r\n",
                             u4Ieee8021CnComPriPriority);
                return SNMP_FAILURE;
            }

            /*Invoke the below function to check if an alternate priority for
             * the CNPV to be created is already configured in QOS.
             * if so, CNPV can't be created till
             * that configuration is removed.*/

            if (CnUtilCheckQoSPriorityMap (pCnCompTblEntry,
                                           u4Ieee8021CnComPriPriority) ==
                OSIX_FAILURE)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                CLI_SET_ERR (CN_CLI_PRIORITY_MAP_ALREADY_EXIST);
                CN_TRC_ARG1 (u4Ieee8021CnComPriComponentId,
                             (CN_MGMT_TRC | CN_ALL_FAILURE_TRC),
                             "nmhSetIeee8021CnComPriRowStatus: FAILED \r\n"
                             "QoS Priority Map Entry exists - CNPV %d can not "
                             "be created\r\n", u4Ieee8021CnComPriPriority);
                return SNMP_FAILURE;
            }
            break;

        default:                /* case CN_DESTROY: */
            if (pCnCompPriEntry == NULL)
            {
                *pu4ErrorCode = SNMP_ERR_NO_CREATION;
                CLI_SET_ERR (CN_CLI_COMP_PRI_ENTRY_NOT_FOUND);
                return SNMP_FAILURE;
            }
            break;
    }
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
Function    :  nmhDepv2Ieee8021CnCompntPriTable
Input       :  The Indices
               Ieee8021CnComPriComponentId
               Ieee8021CnComPriPriority
Output      :  The Dependency Low Lev Routine Take the Indices &
               check whether dependency is met or not.
               Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
               SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
               SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
               SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
               SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
               SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhDepv2Ieee8021CnCompntPriTable (UINT4 *pu4ErrorCode,
                                  tSnmpIndexList * pSnmpIndexList,
                                  tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : Ieee8021CnPortPriTable. */

/****************************************************************************
Function    :  nmhValidateIndexInstanceIeee8021CnPortPriTable
Input       :  The Indices
               Ieee8021CnPortPriComponentId
               Ieee8021CnPortPriority
               Ieee8021CnPortPriIfIndex
Output      :  The Routines Validates the Given Indices.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceIeee8021CnPortPriTable (UINT4
                                                u4Ieee8021CnPortPriComponentId,
                                                UINT4 u4Ieee8021CnPortPriority,
                                                INT4 i4Ieee8021CnPortPriIfIndex)
{
    UINT4               u4PortId = (UINT4) i4Ieee8021CnPortPriIfIndex;
    tCnPortPriTblInfo  *pCnPortPriEntry = NULL;
    tCnCompTblInfo     *pCnCompTblEntry = NULL;
    tCnPortTblInfo     *pCnPortTblEntry = NULL;

    if (CN_IS_SHUTDOWN ())
    {
        return SNMP_FAILURE;
    }

    if (CN_IS_VALID_COMP_ID (u4Ieee8021CnPortPriComponentId) == OSIX_FALSE)
    {
        CLI_SET_ERR (CN_CLI_INVALID_COMP_ID);
        return SNMP_FAILURE;
    }
    if (u4Ieee8021CnPortPriority >= CN_MAX_VLAN_PRIORITY)
    {
        CLI_SET_ERR (CN_CLI_INVALID_CNPV);
        return SNMP_FAILURE;
    }
    if ((i4Ieee8021CnPortPriIfIndex < CN_MIN_PORTS) ||
        (i4Ieee8021CnPortPriIfIndex > CN_MAX_PORTS))
    {
        CLI_SET_ERR (CN_CLI_INVALID_PORT_ID);
        return SNMP_FAILURE;
    }

    pCnCompTblEntry = CN_COMP_TBL_ENTRY (u4Ieee8021CnPortPriComponentId);
    if (pCnCompTblEntry == NULL)
    {
        CLI_SET_ERR (CN_CLI_COMP_ENTRY_NOT_FOUND);
        CN_GBL_TRC (CN_INVALID_CONTEXT,
                    (CN_MGMT_TRC | CN_ALL_FAILURE_TRC),
                    "nmhValidateIndexInstanceIeee8021CnPortPriTable: FAILED \r\n"
                    "Context is not present in CN \r\n");

        return SNMP_FAILURE;
    }

    CN_PORT_TBL_ENTRY (u4Ieee8021CnPortPriComponentId, u4PortId,
                       pCnPortTblEntry);
    if (pCnPortTblEntry == NULL)
    {
        CLI_SET_ERR (CN_CLI_PORT_ENTRY_NOT_FOUND);
        CN_TRC_ARG1 (u4Ieee8021CnPortPriComponentId,
                     (CN_MGMT_TRC | CN_ALL_FAILURE_TRC),
                     "nmhValidateIndexInstanceIeee8021CnPortPriTable: FAILED\r\n"
                     "Port Entry does not exists for port %d !!!\r\n",
                     u4PortId);
        return SNMP_FAILURE;
    }

    CN_PORT_PRI_ENTRY (u4Ieee8021CnPortPriComponentId, u4PortId,
                       u4Ieee8021CnPortPriority, pCnPortPriEntry);
    if (pCnPortPriEntry == NULL)
    {
        CLI_SET_ERR (CN_CLI_PORT_PRI_ENTRY_NOT_FOUND);
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhGetFirstIndexIeee8021CnPortPriTable
Input       :  The Indices
               Ieee8021CnPortPriComponentId
               Ieee8021CnPortPriority
               Ieee8021CnPortPriIfIndex
Output      :  The Get First Routines gets the Lexicographicaly
               First Entry from the Table.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexIeee8021CnPortPriTable (UINT4 *pu4Ieee8021CnPortPriComponentId,
                                        UINT4 *pu4Ieee8021CnPortPriority,
                                        INT4 *pi4Ieee8021CnPortPriIfIndex)
{
    tCnPortPriTblInfo  *pCnPortPriEntry = NULL;
    UINT4               u4CompId = CN_MIN_CONTEXT;
    UINT4               u4PortId = CN_MIN_PORTS;
    UINT4               u4Priority = CN_ZERO;

    CN_PORT_PRI_ENTRY (u4CompId, u4PortId, u4Priority, pCnPortPriEntry);
    if (pCnPortPriEntry != NULL)
    {
        *pu4Ieee8021CnPortPriComponentId = u4CompId;
        *pu4Ieee8021CnPortPriority = u4Priority;
        *pi4Ieee8021CnPortPriIfIndex = (INT4) u4PortId;
        return SNMP_SUCCESS;
    }
    return (nmhGetNextIndexIeee8021CnPortPriTable
            (u4CompId, pu4Ieee8021CnPortPriComponentId,
             u4Priority, pu4Ieee8021CnPortPriority, u4PortId,
             pi4Ieee8021CnPortPriIfIndex));
}

/****************************************************************************
Function    :  nmhGetNextIndexIeee8021CnPortPriTable
Input       :  The Indices
               Ieee8021CnPortPriComponentId
               Ieee8021CnPortPriority
               nextIeee8021CnPortPriIfIndex
Output      :  The Get Next function gets the Next Index for
               the Index Value given in the Index Values. The
               Indices are stored in the next_varname variables.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexIeee8021CnPortPriTable (UINT4 u4Ieee8021CnPortPriComponentId,
                                       UINT4
                                       *pu4NextIeee8021CnPortPriComponentId,
                                       UINT4 u4Ieee8021CnPortPriority,
                                       UINT4 *pu4NextIeee8021CnPortPriority,
                                       INT4 i4Ieee8021CnPortPriIfIndex,
                                       INT4 *pi4NextIeee8021CnPortPriIfIndex)
{
    tCnCompTblInfo     *pCnCompTblEntry = NULL;
    tCnPortTblInfo     *pCnPortTblEntry = NULL;
    tCnPortTblInfo     *pTempPortTblEntry = NULL;
    tCnPortTblInfo      TempPortTblEntry;
    tCnPortPriTblInfo  *pCnPortPriEntry = NULL;
    UINT4               u4CompId = CN_MIN_CONTEXT;
    UINT4               u4PortId = CN_ZERO;
    UINT4               u4Priority = CN_ZERO;

    MEMSET (&TempPortTblEntry, CN_ZERO, sizeof (tCnPortTblInfo));
    for (u4CompId = u4Ieee8021CnPortPriComponentId; u4CompId < CN_MAX_CONTEXTS;
         u4CompId++)
    {
        pCnCompTblEntry = CN_COMP_TBL_ENTRY (u4CompId);
        if (pCnCompTblEntry != NULL)
        {
            if (u4CompId == u4Ieee8021CnPortPriComponentId)
            {
                u4Priority = u4Ieee8021CnPortPriority;
            }
            else
            {
                u4Priority = CN_ZERO;
            }
            for (; u4Priority < CN_MAX_VLAN_PRIORITY; u4Priority++)
            {
                if (pCnCompTblEntry->pCompPriTbl[u4Priority] != NULL)
                {
                    if ((u4CompId == u4Ieee8021CnPortPriComponentId) &&
                        (u4Priority == u4Ieee8021CnPortPriority))
                    {
                        TempPortTblEntry.u4PortId =
                            (UINT4) i4Ieee8021CnPortPriIfIndex;
                        pTempPortTblEntry = &TempPortTblEntry;
                        pCnPortTblEntry =
                            (tCnPortTblInfo *) RBTreeGetNext (pCnCompTblEntry->
                                                              PortTbl,
                                                              pTempPortTblEntry,
                                                              NULL);
                    }
                    else
                    {
                        pCnPortTblEntry = (tCnPortTblInfo *) RBTreeGetFirst
                            (pCnCompTblEntry->PortTbl);
                    }
                    if (pCnPortTblEntry != NULL)
                    {
                        u4PortId = pCnPortTblEntry->u4PortId;
                        CN_PORT_PRI_ENTRY (u4CompId, u4PortId,
                                           u4Priority, pCnPortPriEntry);
                        if (pCnPortPriEntry != NULL)
                        {
                            *pu4NextIeee8021CnPortPriComponentId = u4CompId;
                            *pi4NextIeee8021CnPortPriIfIndex = u4PortId;
                            *pu4NextIeee8021CnPortPriority = u4Priority;
                            return SNMP_SUCCESS;
                        }
                    }
                }
            }
        }
    }

    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
Function    :  nmhGetIeee8021CnPortPriDefModeChoice
Input       :  The Indices
               Ieee8021CnPortPriComponentId
               Ieee8021CnPortPriority
               Ieee8021CnPortPriIfIndex

               The Object 
               retValIeee8021CnPortPriDefModeChoice
Output      :  The Get Low Lev Routine Take the Indices &
               store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetIeee8021CnPortPriDefModeChoice (UINT4 u4Ieee8021CnPortPriComponentId,
                                      UINT4 u4Ieee8021CnPortPriority,
                                      INT4 i4Ieee8021CnPortPriIfIndex,
                                      INT4
                                      *pi4RetValIeee8021CnPortPriDefModeChoice)
{
    tCnPortPriTblInfo  *pCnPortPriEntry = NULL;
    UINT4               u4PortId = i4Ieee8021CnPortPriIfIndex;

    CN_PORT_PRI_ENTRY (u4Ieee8021CnPortPriComponentId, u4PortId,
                       u4Ieee8021CnPortPriority, pCnPortPriEntry);
    if (pCnPortPriEntry != NULL)
    {
        *pi4RetValIeee8021CnPortPriDefModeChoice =
            (INT4) pCnPortPriEntry->u1PortPriDefModeChoice;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
Function    :  nmhGetIeee8021CnPortPriAdminDefenseMode
Input       :  The Indices
               Ieee8021CnPortPriComponentId
               Ieee8021CnPortPriority
               Ieee8021CnPortPriIfIndex

               The Object 
               retValIeee8021CnPortPriAdminDefenseMode
Output      :  The Get Low Lev Routine Take the Indices &
               store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1 
     
     
     
     
     
     
     
    nmhGetIeee8021CnPortPriAdminDefenseMode
    (UINT4 u4Ieee8021CnPortPriComponentId,
     UINT4 u4Ieee8021CnPortPriority,
     INT4 i4Ieee8021CnPortPriIfIndex,
     INT4 *pi4RetValIeee8021CnPortPriAdminDefenseMode)
{
    tCnPortPriTblInfo  *pCnPortPriTblEntry = NULL;
    UINT4               u4PortId = i4Ieee8021CnPortPriIfIndex;

    CN_PORT_PRI_ENTRY (u4Ieee8021CnPortPriComponentId, u4PortId,
                       u4Ieee8021CnPortPriority, pCnPortPriTblEntry);
    if (pCnPortPriTblEntry != NULL)
    {
        *pi4RetValIeee8021CnPortPriAdminDefenseMode =
            (INT4) pCnPortPriTblEntry->u1PortPriAdminDefMode;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
Function    :  nmhGetIeee8021CnPortPriAutoDefenseMode
Input       :  The Indices
               Ieee8021CnPortPriComponentId
               Ieee8021CnPortPriority
               Ieee8021CnPortPriIfIndex

               The Object 
               retValIeee8021CnPortPriAutoDefenseMode
Output      :  The Get Low Lev Routine Take the Indices &
               store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1 
     
     
     
     
     
     
     
    nmhGetIeee8021CnPortPriAutoDefenseMode
    (UINT4 u4Ieee8021CnPortPriComponentId,
     UINT4 u4Ieee8021CnPortPriority,
     INT4 i4Ieee8021CnPortPriIfIndex,
     INT4 *pi4RetValIeee8021CnPortPriAutoDefenseMode)
{
    tCnPortPriTblInfo  *pCnPortPriTblEntry = NULL;
    UINT4               u4PortId = i4Ieee8021CnPortPriIfIndex;

    CN_PORT_PRI_ENTRY (u4Ieee8021CnPortPriComponentId, u4PortId,
                       u4Ieee8021CnPortPriority, pCnPortPriTblEntry);
    if (pCnPortPriTblEntry != NULL)
    {
        *pi4RetValIeee8021CnPortPriAutoDefenseMode =
            (INT4) pCnPortPriTblEntry->u1PortPriOperDefMode;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
Function    :  nmhGetIeee8021CnPortPriLldpInstanceChoice
Input       :  The Indices
               Ieee8021CnPortPriComponentId
               Ieee8021CnPortPriority
               Ieee8021CnPortPriIfIndex

               The Object 
               retValIeee8021CnPortPriLldpInstanceChoice
Output      :  The Get Low Lev Routine Take the Indices &
               store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1 
     
     
     
     
     
     
     
    nmhGetIeee8021CnPortPriLldpInstanceChoice
    (UINT4 u4Ieee8021CnPortPriComponentId,
     UINT4 u4Ieee8021CnPortPriority,
     INT4 i4Ieee8021CnPortPriIfIndex,
     INT4 *pi4RetValIeee8021CnPortPriLldpInstanceChoice)
{
    tCnPortPriTblInfo  *pCnPortPriTblEntry = NULL;
    UINT4               u4PortId = i4Ieee8021CnPortPriIfIndex;

    CN_PORT_PRI_ENTRY (u4Ieee8021CnPortPriComponentId, u4PortId,
                       u4Ieee8021CnPortPriority, pCnPortPriTblEntry);
    if (pCnPortPriTblEntry != NULL)
    {
        *pi4RetValIeee8021CnPortPriLldpInstanceChoice =
            (INT4) pCnPortPriTblEntry->u1PortPriLldpInstChoice;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
Function    :  nmhGetIeee8021CnPortPriLldpInstanceSelector
Input       :  The Indices
               Ieee8021CnPortPriComponentId
               Ieee8021CnPortPriority
               Ieee8021CnPortPriIfIndex

               The Object 
               retValIeee8021CnPortPriLldpInstanceSelector
Output      :  The Get Low Lev Routine Take the Indices &
               store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetIeee8021CnPortPriLldpInstanceSelector (UINT4
                                             u4Ieee8021CnPortPriComponentId,
                                             UINT4 u4Ieee8021CnPortPriority,
                                             INT4 i4Ieee8021CnPortPriIfIndex,
                                             UINT4
                                             *pi4RetValIeee8021CnPortPriLldpInstanceSelector)
{
    tCnPortPriTblInfo  *pCnPortPriTblEntry = NULL;
    UINT4               u4PortId = i4Ieee8021CnPortPriIfIndex;

    CN_PORT_PRI_ENTRY (u4Ieee8021CnPortPriComponentId, u4PortId,
                       u4Ieee8021CnPortPriority, pCnPortPriTblEntry);
    if (pCnPortPriTblEntry != NULL)
    {
        *pi4RetValIeee8021CnPortPriLldpInstanceSelector =
            pCnPortPriTblEntry->u4PortPriLldpInstSelector;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
Function    :  nmhGetIeee8021CnPortPriAlternatePriority
Input       :  The Indices
               Ieee8021CnPortPriComponentId
               Ieee8021CnPortPriority
               Ieee8021CnPortPriIfIndex

               The Object 
               retValIeee8021CnPortPriAlternatePriority
Output      :  The Get Low Lev Routine Take the Indices &
               store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1 
     
     
     
     
     
     
     
    nmhGetIeee8021CnPortPriAlternatePriority
    (UINT4 u4Ieee8021CnPortPriComponentId,
     UINT4 u4Ieee8021CnPortPriority,
     INT4 i4Ieee8021CnPortPriIfIndex,
     UINT4 *pi4RetValIeee8021CnPortPriAlternatePriority)
{
    tCnPortPriTblInfo  *pCnPortPriTblEntry = NULL;
    UINT4               u4PortId = i4Ieee8021CnPortPriIfIndex;

    CN_PORT_PRI_ENTRY (u4Ieee8021CnPortPriComponentId, u4PortId,
                       u4Ieee8021CnPortPriority, pCnPortPriTblEntry);
    if (pCnPortPriTblEntry != NULL)
    {
        *pi4RetValIeee8021CnPortPriAlternatePriority =
            pCnPortPriTblEntry->u1PortPriAdminAltPri;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
Function    :  nmhSetIeee8021CnPortPriDefModeChoice
 Input       :  The Indices
                Ieee8021CnPortPriComponentId
                Ieee8021CnPortPriority
                Ieee8021CnPortPriIfIndex

                The Object 
                setValIeee8021CnPortPriDefModeChoice
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetIeee8021CnPortPriDefModeChoice (UINT4 u4Ieee8021CnPortPriComponentId,
                                      UINT4 u4Ieee8021CnPortPriority,
                                      INT4 i4Ieee8021CnPortPriIfIndex,
                                      INT4
                                      i4SetValIeee8021CnPortPriDefModeChoice)
{
    tCnPortPriTblInfo  *pCnPortPriTblEntry = NULL;
    tCnCompTblInfo     *pCnCompTblEntry = NULL;
    tCnPortTblInfo      TempPortTbl;
    tCnPortTblInfo     *pCnPortTblEntry = NULL;
    tCnCompPriTbl      *pCnCompPriEntry = NULL;
    UINT4               u4PortId = i4Ieee8021CnPortPriIfIndex;

    MEMSET (&TempPortTbl, CN_ZERO, sizeof (tCnPortTblInfo));
    TempPortTbl.u4PortId = u4PortId;
    pCnCompTblEntry = CN_COMP_TBL_ENTRY (u4Ieee8021CnPortPriComponentId);
    if (pCnCompTblEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pCnCompPriEntry = pCnCompTblEntry->pCompPriTbl[u4Ieee8021CnPortPriority];
    if (pCnCompPriEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pCnPortTblEntry = (tCnPortTblInfo *) RBTreeGet
        (pCnCompTblEntry->PortTbl, &TempPortTbl);
    if (pCnPortTblEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pCnPortPriTblEntry =
        pCnPortTblEntry->paPortPriTbl[u4Ieee8021CnPortPriority];
    if (pCnPortPriTblEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    /*Already same value for PortPriDefModeChoice */
    if (pCnPortPriTblEntry->u1PortPriDefModeChoice ==
        (UINT1) i4SetValIeee8021CnPortPriDefModeChoice)
    {
        return SNMP_SUCCESS;
    }
    /*Avoiding state machine changes 
       if current defense mode choice and the defense mode choice 
       to be configured is auto */
    if ((pCnPortPriTblEntry->bIsAdminDefMode == OSIX_FALSE)
        && (i4SetValIeee8021CnPortPriDefModeChoice == CN_AUTO))
    {
        pCnPortPriTblEntry->u1PortPriDefModeChoice =
            (UINT1) i4SetValIeee8021CnPortPriDefModeChoice;
        return SNMP_SUCCESS;
    }
    pCnPortPriTblEntry->u1PortPriDefModeChoice =
        (UINT1) i4SetValIeee8021CnPortPriDefModeChoice;

    if ((pCnPortPriTblEntry->u1PortPriDefModeChoice == CN_ADMIN) ||
        ((pCnPortPriTblEntry->u1PortPriDefModeChoice == CN_COMP) &&
         (pCnCompPriEntry->u1ComPriDefModeChoice == CN_ADMIN)))
    {
        pCnPortPriTblEntry->bIsAdminDefMode = OSIX_TRUE;
    }
    else
    {
        pCnPortPriTblEntry->bIsAdminDefMode = OSIX_FALSE;
    }
    /*Call corresponding state machine function */
    if (pCnCompPriEntry->u1ComPriRowStatus == CN_ACTIVE)
    {
        CnUtilEnableCnpv (pCnCompTblEntry, pCnPortTblEntry, pCnPortPriTblEntry);
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetIeee8021CnPortPriAdminDefenseMode
 Input       :  The Indices
                Ieee8021CnPortPriComponentId
                Ieee8021CnPortPriority
                Ieee8021CnPortPriIfIndex

                The Object 
                setValIeee8021CnPortPriAdminDefenseMode
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetIeee8021CnPortPriAdminDefenseMode (UINT4 u4Ieee8021CnPortPriComponentId,
                                         UINT4 u4Ieee8021CnPortPriority,
                                         INT4 i4Ieee8021CnPortPriIfIndex,
                                         INT4
                                         i4SetValIeee8021CnPortPriAdminDefenseMode)
{
    tCnPortPriTblInfo  *pCnPortPriTblEntry = NULL;
    tCnCompTblInfo     *pCnCompTblEntry = NULL;
    tCnCompPriTbl      *pCnCompPriEntry = NULL;
    tCnPortTblInfo     *pCnPortTblEntry = NULL;
    tCnPortTblInfo      TempPortTbl;
    UINT4               u4PortId = i4Ieee8021CnPortPriIfIndex;

    MEMSET (&TempPortTbl, CN_ZERO, sizeof (tCnPortTblInfo));
    TempPortTbl.u4PortId = u4PortId;
    pCnCompTblEntry = CN_COMP_TBL_ENTRY (u4Ieee8021CnPortPriComponentId);
    if (pCnCompTblEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pCnCompPriEntry = pCnCompTblEntry->pCompPriTbl[u4Ieee8021CnPortPriority];
    if (pCnCompPriEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pCnPortTblEntry = (tCnPortTblInfo *) RBTreeGet
        (pCnCompTblEntry->PortTbl, &TempPortTbl);
    if (pCnPortTblEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pCnPortPriTblEntry =
        pCnPortTblEntry->paPortPriTbl[u4Ieee8021CnPortPriority];
    if (pCnPortPriTblEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pCnPortPriTblEntry->u1PortPriAdminDefMode ==
        (UINT1) i4SetValIeee8021CnPortPriAdminDefenseMode)
    {
        return SNMP_SUCCESS;
    }

    pCnPortPriTblEntry->u1PortPriAdminDefMode =
        (UINT1) i4SetValIeee8021CnPortPriAdminDefenseMode;

    /*Call corresponding state machine function */
    if ((pCnCompPriEntry->u1ComPriRowStatus == CN_ACTIVE) &&
        (pCnPortPriTblEntry->bIsAdminDefMode == OSIX_TRUE))
    {
        CnUtilEnableCnpv (pCnCompTblEntry, pCnPortTblEntry, pCnPortPriTblEntry);
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetIeee8021CnPortPriLldpInstanceChoice
 Input       :  The Indices
                Ieee8021CnPortPriComponentId
                Ieee8021CnPortPriority
                Ieee8021CnPortPriIfIndex

                The Object 
                setValIeee8021CnPortPriLldpInstanceChoice
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
     
     
     
     
     
     
     
    nmhSetIeee8021CnPortPriLldpInstanceChoice
    (UINT4 u4Ieee8021CnPortPriComponentId,
     UINT4 u4Ieee8021CnPortPriority,
     INT4 i4Ieee8021CnPortPriIfIndex,
     INT4 i4SetValIeee8021CnPortPriLldpInstanceChoice)
{
    tCnPortPriTblInfo  *pCnPortPriTblEntry = NULL;
    tCnPortTblInfo     *pCnPortTblEntry = NULL;
    tCnCompTblInfo     *pCnCompTblEntry = NULL;
    tCnPortTblInfo      TempPortTbl;
    UINT4               u4PortId = (UINT4) i4Ieee8021CnPortPriIfIndex;

    MEMSET (&TempPortTbl, CN_ZERO, sizeof (tCnPortTblInfo));
    TempPortTbl.u4PortId = u4PortId;
    pCnCompTblEntry = CN_COMP_TBL_ENTRY (u4Ieee8021CnPortPriComponentId);
    if (pCnCompTblEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    pCnPortTblEntry = (tCnPortTblInfo *) RBTreeGet
        (pCnCompTblEntry->PortTbl, &TempPortTbl);
    if (pCnPortTblEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    pCnPortPriTblEntry =
        pCnPortTblEntry->paPortPriTbl[u4Ieee8021CnPortPriority];
    if (pCnPortPriTblEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    /*Already same value for PortPriLldpInstanceChoice */
    if (pCnPortPriTblEntry->u1PortPriLldpInstChoice ==
        (UINT1) i4SetValIeee8021CnPortPriLldpInstanceChoice)
    {
        return SNMP_SUCCESS;
    }

    pCnPortPriTblEntry->u1PortPriLldpInstChoice =
        (UINT1) i4SetValIeee8021CnPortPriLldpInstanceChoice;

    /*Reset defense mode and set CNPV and Ready Indicators */
    CnUtilEnableCnpv (pCnCompTblEntry, pCnPortTblEntry, pCnPortPriTblEntry);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetIeee8021CnPortPriLldpInstanceSelector
 Input       :  The Indices
                Ieee8021CnPortPriComponentId
                Ieee8021CnPortPriority
                Ieee8021CnPortPriIfIndex

                The Object 
                setValIeee8021CnPortPriLldpInstanceSelector
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIeee8021CnPortPriLldpInstanceSelector (UINT4
                                             u4Ieee8021CnPortPriComponentId,
                                             UINT4 u4Ieee8021CnPortPriority,
                                             INT4 i4Ieee8021CnPortPriIfIndex,
                                             UINT4
                                             u4SetValIeee8021CnPortPriLldpInstanceSelector)
{
    tCnPortPriTblInfo  *pCnPortPriTblEntry = NULL;
    UINT4               u4PortId = i4Ieee8021CnPortPriIfIndex;

    CN_PORT_PRI_ENTRY (u4Ieee8021CnPortPriComponentId, u4PortId,
                       u4Ieee8021CnPortPriority, pCnPortPriTblEntry);
    if (pCnPortPriTblEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    pCnPortPriTblEntry->u4PortPriLldpInstSelector =
        (UINT1) u4SetValIeee8021CnPortPriLldpInstanceSelector;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetIeee8021CnPortPriAlternatePriority
 Input       :  The Indices
                Ieee8021CnPortPriComponentId
                Ieee8021CnPortPriority
                Ieee8021CnPortPriIfIndex

                The Object 
                setValIeee8021CnPortPriAlternatePriority
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
     
     
     
     
     
     
     
    nmhSetIeee8021CnPortPriAlternatePriority
    (UINT4 u4Ieee8021CnPortPriComponentId,
     UINT4 u4Ieee8021CnPortPriority,
     INT4 i4Ieee8021CnPortPriIfIndex,
     UINT4 u4SetValIeee8021CnPortPriAlternatePriority)
{
    tCnCompPriTbl      *pCnCompPriEntry = NULL;
    tCnCompTblInfo     *pCnCompTblEntry = NULL;
    tCnPortPriTblInfo  *pCnPortPriEntry = NULL;
    UINT4               u4Priority = CN_ZERO;
    UINT4               u4PortId = i4Ieee8021CnPortPriIfIndex;
    UINT1               u1ErroredPortFlag = CN_ZERO;
    UINT1               u1OutPri = CN_ZERO;

    CN_PORT_PRI_ENTRY (u4Ieee8021CnPortPriComponentId, u4PortId,
                       u4Ieee8021CnPortPriority, pCnPortPriEntry);

    if (pCnPortPriEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    /*Already same value for PortPriAdminAltPri */
    if (pCnPortPriEntry->u1PortPriAdminAltPri ==
        u4SetValIeee8021CnPortPriAlternatePriority)
    {
        return SNMP_SUCCESS;
    }

    pCnPortPriEntry->u1PortPriAdminAltPri = ((UINT1)
                                             u4SetValIeee8021CnPortPriAlternatePriority);

    /*Calculate the Oper Alternate Priority */
    CnUtilCalculateOperAltPri (pCnPortPriEntry);

    pCnCompTblEntry = CN_COMP_TBL_ENTRY (u4Ieee8021CnPortPriComponentId);
    if (pCnCompTblEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    if ((pCnCompTblEntry->bCnCompMasterEnable == CN_MASTER_ENABLE) &&
        (pCnPortPriEntry->u1PortPriOperDefMode != CN_DISABLED) &&
        (pCnPortPriEntry->bIsAdminDefMode == OSIX_TRUE))
    {
        u1OutPri = pCnPortPriEntry->u1PortPriOperAltPri;
        CnPortQosPriorityMapReq (pCnPortPriEntry->u4CnComPriComponentId,
                                 pCnPortPriEntry->u4PortId,
                                 pCnPortPriEntry->u1CNPV,
                                 &u1OutPri, QOS_ADD_PRIORITY_MAP);
    }

    /*checking for errored port entry */
    while ((u4Priority < CN_MAX_VLAN_PRIORITY) &&
           (u1ErroredPortFlag == CN_ZERO))
    {
        CN_COMP_PRI_ENTRY (u4Ieee8021CnPortPriComponentId, u4Priority,
                           pCnCompPriEntry);

        if (pCnCompPriEntry != NULL)
        {
            if (pCnCompPriEntry->u1CNPV ==
                (UINT1) u4SetValIeee8021CnPortPriAlternatePriority)
            {
                /* Addind an errored port entry */
                u1ErroredPortFlag++;
                CnUtilAddErroredPortEntry (pCnPortPriEntry);
            }
        }
        u4Priority++;
    }

    /*Delete errored port entry if there is no conflict */
    if (u1ErroredPortFlag == CN_ZERO)
    {
        if (pCnPortPriEntry->bIsErrorPortEntry == CN_ERROR_PORT_ENTRY_SET)
        {

            CnUtilDeleteErroredPortEntry (pCnPortPriEntry);
        }
    }
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2Ieee8021CnPortPriDefModeChoice
 Input       :  The Indices
                Ieee8021CnPortPriComponentId
                Ieee8021CnPortPriority
                Ieee8021CnPortPriIfIndex

                The Object 
                testValIeee8021CnPortPriDefModeChoice
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
     
     
     
     
     
     
     
    nmhTestv2Ieee8021CnPortPriDefModeChoice
    (UINT4 *pu4ErrorCode,
     UINT4 u4Ieee8021CnPortPriComponentId,
     UINT4 u4Ieee8021CnPortPriority,
     INT4 i4Ieee8021CnPortPriIfIndex,
     INT4 i4TestValIeee8021CnPortPriDefModeChoice)
{
    tCnPortPriTblInfo  *pCnPortPriEntry = NULL;
    tCnCompTblInfo     *pCnCompTblEntry = NULL;
    tCnPortTblInfo     *pCnPortTblEntry = NULL;
    UINT4               u4PortId = CN_ZERO;

    if (CN_IS_SHUTDOWN ())
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (CN_IS_VALID_COMP_ID (u4Ieee8021CnPortPriComponentId) == OSIX_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CN_CLI_INVALID_COMP_ID);
        return SNMP_FAILURE;
    }
    if (u4Ieee8021CnPortPriority >= CN_MAX_VLAN_PRIORITY)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CN_CLI_INVALID_CNPV);
        return SNMP_FAILURE;
    }
    if ((i4Ieee8021CnPortPriIfIndex < CN_MIN_PORTS) ||
        (i4Ieee8021CnPortPriIfIndex > CN_MAX_PORTS))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CN_CLI_INVALID_PORT_ID);
        return SNMP_FAILURE;
    }
    if ((i4TestValIeee8021CnPortPriDefModeChoice != CN_ADMIN) &&
        (i4TestValIeee8021CnPortPriDefModeChoice != CN_AUTO) &&
        (i4TestValIeee8021CnPortPriDefModeChoice != CN_COMP))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    pCnCompTblEntry = CN_COMP_TBL_ENTRY (u4Ieee8021CnPortPriComponentId);
    if (pCnCompTblEntry == NULL)
    {
        CLI_SET_ERR (CN_CLI_COMP_ENTRY_NOT_FOUND);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    u4PortId = (UINT4) i4Ieee8021CnPortPriIfIndex;
    CN_PORT_TBL_ENTRY (u4Ieee8021CnPortPriComponentId, u4PortId,
                       pCnPortTblEntry);
    if (pCnPortTblEntry == NULL)
    {
        CLI_SET_ERR (CN_CLI_PORT_ENTRY_NOT_FOUND);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CN_TRC_ARG1 (u4Ieee8021CnPortPriComponentId,
                     (CN_MGMT_TRC | CN_ALL_FAILURE_TRC),
                     "nmhTestv2Ieee8021CnPortPriDefModeChoice: FAILED\r\n"
                     "Port Entry does not exists for port %d !!!\r\n",
                     u4PortId);
        return SNMP_FAILURE;
    }

    CN_PORT_PRI_ENTRY (u4Ieee8021CnPortPriComponentId, u4PortId,
                       u4Ieee8021CnPortPriority, pCnPortPriEntry);
    if (pCnPortPriEntry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CN_CLI_PORT_PRI_ENTRY_NOT_FOUND);
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Ieee8021CnPortPriAdminDefenseMode
 Input       :  The Indices
                Ieee8021CnPortPriComponentId
                Ieee8021CnPortPriority
                Ieee8021CnPortPriIfIndex

                The Object 
                testValIeee8021CnPortPriAdminDefenseMode
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
     
     
     
     
     
     
     
    nmhTestv2Ieee8021CnPortPriAdminDefenseMode
    (UINT4 *pu4ErrorCode,
     UINT4 u4Ieee8021CnPortPriComponentId,
     UINT4 u4Ieee8021CnPortPriority,
     INT4 i4Ieee8021CnPortPriIfIndex,
     INT4 i4TestValIeee8021CnPortPriAdminDefenseMode)
{
    tCnPortPriTblInfo  *pCnPortPriEntry = NULL;
    tCnCompTblInfo     *pCnCompTblEntry = NULL;
    tCnPortTblInfo     *pCnPortTblEntry = NULL;
    UINT4               u4PortId = CN_ZERO;

    if (CN_IS_SHUTDOWN ())
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (CN_IS_VALID_COMP_ID (u4Ieee8021CnPortPriComponentId) == OSIX_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CN_CLI_INVALID_COMP_ID);
        return SNMP_FAILURE;
    }

    if (u4Ieee8021CnPortPriority >= CN_MAX_VLAN_PRIORITY)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CN_CLI_INVALID_CNPV);
        return SNMP_FAILURE;
    }

    if ((i4Ieee8021CnPortPriIfIndex < CN_MIN_PORTS) ||
        (i4Ieee8021CnPortPriIfIndex > CN_MAX_PORTS))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CN_CLI_INVALID_PORT_ID);
        return SNMP_FAILURE;
    }

    if ((i4TestValIeee8021CnPortPriAdminDefenseMode != CN_DISABLED) &&
        (i4TestValIeee8021CnPortPriAdminDefenseMode != CN_INTERIOR) &&
        (i4TestValIeee8021CnPortPriAdminDefenseMode != CN_INTERIOR_READY) &&
        (i4TestValIeee8021CnPortPriAdminDefenseMode != CN_EDGE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    pCnCompTblEntry = CN_COMP_TBL_ENTRY (u4Ieee8021CnPortPriComponentId);
    if (pCnCompTblEntry == NULL)
    {
        CLI_SET_ERR (CN_CLI_COMP_ENTRY_NOT_FOUND);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    u4PortId = (UINT4) i4Ieee8021CnPortPriIfIndex;
    CN_PORT_TBL_ENTRY (u4Ieee8021CnPortPriComponentId, u4PortId,
                       pCnPortTblEntry);
    if (pCnPortTblEntry == NULL)
    {
        CLI_SET_ERR (CN_CLI_PORT_ENTRY_NOT_FOUND);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CN_TRC_ARG1 (u4Ieee8021CnPortPriComponentId,
                     (CN_MGMT_TRC | CN_ALL_FAILURE_TRC),
                     "nmhTestv2Ieee8021CnPortPriAdminDefenseMode: FAILED\r\n"
                     "Port Entry does not exists for port %d !!!\r\n",
                     u4PortId);
        return SNMP_FAILURE;
    }

    CN_PORT_PRI_ENTRY (u4Ieee8021CnPortPriComponentId, u4PortId,
                       u4Ieee8021CnPortPriority, pCnPortPriEntry);
    if (pCnPortPriEntry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CN_CLI_PORT_PRI_ENTRY_NOT_FOUND);
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Ieee8021CnPortPriLldpInstanceChoice
 Input       :  The Indices
                Ieee8021CnPortPriComponentId
                Ieee8021CnPortPriority
                Ieee8021CnPortPriIfIndex

                The Object 
                testValIeee8021CnPortPriLldpInstanceChoice
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
     
     
     
     
     
     
     
    nmhTestv2Ieee8021CnPortPriLldpInstanceChoice
    (UINT4 *pu4ErrorCode,
     UINT4 u4Ieee8021CnPortPriComponentId,
     UINT4 u4Ieee8021CnPortPriority,
     INT4 i4Ieee8021CnPortPriIfIndex,
     INT4 i4TestValIeee8021CnPortPriLldpInstanceChoice)
{
    tCnPortPriTblInfo  *pCnPortPriEntry = NULL;
    tCnCompTblInfo     *pCnCompTblEntry = NULL;
    tCnPortTblInfo     *pCnPortTblEntry = NULL;
    UINT4               u4PortId = CN_ZERO;

    if (CN_IS_SHUTDOWN ())
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (CN_IS_VALID_COMP_ID (u4Ieee8021CnPortPriComponentId) == OSIX_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CN_CLI_INVALID_COMP_ID);
        return SNMP_FAILURE;
    }

    if (u4Ieee8021CnPortPriority >= CN_MAX_VLAN_PRIORITY)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CN_CLI_INVALID_CNPV);
        return SNMP_FAILURE;
    }

    if ((i4Ieee8021CnPortPriIfIndex < CN_MIN_PORTS) ||
        (i4Ieee8021CnPortPriIfIndex > CN_MAX_PORTS))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CN_CLI_INVALID_PORT_ID);
        return SNMP_FAILURE;
    }

    if ((i4TestValIeee8021CnPortPriLldpInstanceChoice != CN_LLDP_NONE) &&
        (i4TestValIeee8021CnPortPriLldpInstanceChoice != CN_LLDP_ADMIN) &&
        (i4TestValIeee8021CnPortPriLldpInstanceChoice != CN_LLDP_COMP))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    pCnCompTblEntry = CN_COMP_TBL_ENTRY (u4Ieee8021CnPortPriComponentId);
    if (pCnCompTblEntry == NULL)
    {
        CLI_SET_ERR (CN_CLI_COMP_ENTRY_NOT_FOUND);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    u4PortId = (UINT4) i4Ieee8021CnPortPriIfIndex;
    CN_PORT_TBL_ENTRY (u4Ieee8021CnPortPriComponentId, u4PortId,
                       pCnPortTblEntry);
    if (pCnPortTblEntry == NULL)
    {
        CLI_SET_ERR (CN_CLI_PORT_ENTRY_NOT_FOUND);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CN_TRC_ARG1 (u4Ieee8021CnPortPriComponentId,
                     (CN_MGMT_TRC | CN_ALL_FAILURE_TRC),
                     "nmhTestv2Ieee8021CnPortPriLldpInstanceChoice: FAILED\r\n"
                     "Port Entry does not exists for port %d !!!\r\n",
                     u4PortId);
        return SNMP_FAILURE;
    }

    CN_PORT_PRI_ENTRY (u4Ieee8021CnPortPriComponentId, u4PortId,
                       u4Ieee8021CnPortPriority, pCnPortPriEntry);
    if (pCnPortPriEntry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CN_CLI_PORT_PRI_ENTRY_NOT_FOUND);
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Ieee8021CnPortPriLldpInstanceSelector
 Input       :  The Indices
                Ieee8021CnPortPriComponentId
                Ieee8021CnPortPriority
                Ieee8021CnPortPriIfIndex

                The Object 
                testValIeee8021CnPortPriLldpInstanceSelector
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ieee8021CnPortPriLldpInstanceSelector (UINT4 *pu4ErrorCode,
                                                UINT4
                                                u4Ieee8021CnPortPriComponentId,
                                                UINT4 u4Ieee8021CnPortPriority,
                                                INT4 i4Ieee8021CnPortPriIfIndex,
                                                UINT4
                                                u4TestValIeee8021CnPortPriLldpInstanceSelector)
{
    tCnPortPriTblInfo  *pCnPortPriEntry = NULL;
    tCnCompTblInfo     *pCnCompTblEntry = NULL;
    tCnPortTblInfo     *pCnPortTblEntry = NULL;
    UINT4               u4PortId = CN_ZERO;

    if (CN_IS_SHUTDOWN ())
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (CN_IS_VALID_COMP_ID (u4Ieee8021CnPortPriComponentId) == OSIX_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CN_CLI_INVALID_COMP_ID);
        return SNMP_FAILURE;
    }

    if (u4Ieee8021CnPortPriority >= CN_MAX_VLAN_PRIORITY)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CN_CLI_INVALID_CNPV);
        return SNMP_FAILURE;
    }

    if ((i4Ieee8021CnPortPriIfIndex < CN_MIN_PORTS) ||
        (i4Ieee8021CnPortPriIfIndex > CN_MAX_PORTS))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CN_CLI_INVALID_PORT_ID);
        return SNMP_FAILURE;
    }

    if ((u4TestValIeee8021CnPortPriLldpInstanceSelector <
         CN_MIN_LLDP_INST_SELECTOR) ||
        (u4TestValIeee8021CnPortPriLldpInstanceSelector >
         CN_MAX_LLDP_INST_SELECTOR))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CN_CLI_INVALID_LLDP_INST_SELECTOR);
        return SNMP_FAILURE;
    }

    pCnCompTblEntry = CN_COMP_TBL_ENTRY (u4Ieee8021CnPortPriComponentId);
    if (pCnCompTblEntry == NULL)
    {
        CLI_SET_ERR (CN_CLI_COMP_ENTRY_NOT_FOUND);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    u4PortId = (UINT4) i4Ieee8021CnPortPriIfIndex;
    CN_PORT_TBL_ENTRY (u4Ieee8021CnPortPriComponentId, u4PortId,
                       pCnPortTblEntry);
    if (pCnPortTblEntry == NULL)
    {
        CLI_SET_ERR (CN_CLI_PORT_ENTRY_NOT_FOUND);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CN_TRC_ARG1 (u4Ieee8021CnPortPriComponentId,
                     (CN_MGMT_TRC | CN_ALL_FAILURE_TRC),
                     "nmhTestv2Ieee8021CnPortPriLldpInstanceSelector: FAILED\r\n"
                     "Port Entry does not exists for port %d !!!\r\n",
                     u4PortId);
        return SNMP_FAILURE;
    }

    CN_PORT_PRI_ENTRY (u4Ieee8021CnPortPriComponentId, u4PortId,
                       u4Ieee8021CnPortPriority, pCnPortPriEntry);
    if (pCnPortPriEntry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CN_CLI_PORT_PRI_ENTRY_NOT_FOUND);
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Ieee8021CnPortPriAlternatePriority
 Input       :  The Indices
                Ieee8021CnPortPriComponentId
                Ieee8021CnPortPriority
                Ieee8021CnPortPriIfIndex

                The Object 
                testValIeee8021CnPortPriAlternatePriority
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
     
     
     
     
     
     
     
    nmhTestv2Ieee8021CnPortPriAlternatePriority
    (UINT4 *pu4ErrorCode,
     UINT4 u4Ieee8021CnPortPriComponentId,
     UINT4 u4Ieee8021CnPortPriority,
     INT4 i4Ieee8021CnPortPriIfIndex,
     UINT4 u4TestValIeee8021CnPortPriAlternatePriority)
{
    tCnPortPriTblInfo  *pCnPortPriEntry = NULL;
    tCnCompTblInfo     *pCnCompTblEntry = NULL;
    tCnPortTblInfo     *pCnPortTblEntry = NULL;
    UINT4               u4PortId = CN_ZERO;

    if (CN_IS_SHUTDOWN ())
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (CN_IS_VALID_COMP_ID (u4Ieee8021CnPortPriComponentId) == OSIX_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CN_CLI_INVALID_COMP_ID);
        return SNMP_FAILURE;
    }

    if (u4Ieee8021CnPortPriority >= CN_MAX_VLAN_PRIORITY)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CN_CLI_INVALID_CNPV);
        return SNMP_FAILURE;
    }

    if ((i4Ieee8021CnPortPriIfIndex < CN_MIN_PORTS) ||
        (i4Ieee8021CnPortPriIfIndex > CN_MAX_PORTS))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CN_CLI_INVALID_PORT_ID);
        return SNMP_FAILURE;
    }

    if (u4TestValIeee8021CnPortPriAlternatePriority >= CN_MAX_VLAN_PRIORITY)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CN_CLI_INVALID_VLAN_PRIORITY);
        return SNMP_FAILURE;
    }

    pCnCompTblEntry = CN_COMP_TBL_ENTRY (u4Ieee8021CnPortPriComponentId);
    if (pCnCompTblEntry == NULL)
    {
        CLI_SET_ERR (CN_CLI_COMP_ENTRY_NOT_FOUND);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    u4PortId = (UINT4) i4Ieee8021CnPortPriIfIndex;
    CN_PORT_TBL_ENTRY (u4Ieee8021CnPortPriComponentId, u4PortId,
                       pCnPortTblEntry);
    if (pCnPortTblEntry == NULL)
    {
        CLI_SET_ERR (CN_CLI_PORT_ENTRY_NOT_FOUND);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CN_TRC_ARG1 (u4Ieee8021CnPortPriComponentId,
                     (CN_MGMT_TRC | CN_ALL_FAILURE_TRC),
                     "nmhTestv2Ieee8021CnPortPriAlternatePriority: FAILED\r\n"
                     "Port Entry does not exists for port %d !!!\r\n",
                     u4PortId);
        return SNMP_FAILURE;
    }

    CN_PORT_PRI_ENTRY (u4Ieee8021CnPortPriComponentId, u4PortId,
                       u4Ieee8021CnPortPriority, pCnPortPriEntry);
    if (pCnPortPriEntry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CN_CLI_PORT_PRI_ENTRY_NOT_FOUND);
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2Ieee8021CnPortPriTable
 Input       :  The Indices
                Ieee8021CnPortPriComponentId
                Ieee8021CnPortPriority
                Ieee8021CnPortPriIfIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Ieee8021CnPortPriTable (UINT4 *pu4ErrorCode,
                                tSnmpIndexList * pSnmpIndexList,
                                tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : Ieee8021CnCpTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceIeee8021CnCpTable
 Input       :  The Indices
                Ieee8021CnCpComponentId
                Ieee8021CnCpIfIndex
                Ieee8021CnCpIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceIeee8021CnCpTable (UINT4 u4Ieee8021CnCpComponentId,
                                           INT4 i4Ieee8021CnCpIfIndex,
                                           UINT4 u4Ieee8021CnCpIndex)
{
    UINT4               u4Priority = CN_ZERO;
    UINT4               u4PortId = i4Ieee8021CnCpIfIndex;
    tCnPortPriTblInfo  *pCnPortPriEntry = NULL;
    tCnCompTblInfo     *pCnCompTblEntry = NULL;
    tCnPortTblInfo     *pCnPortTblEntry = NULL;

    if (CN_IS_SHUTDOWN ())
    {
        return SNMP_FAILURE;
    }

    if (CN_IS_VALID_COMP_ID (u4Ieee8021CnCpComponentId) == OSIX_FALSE)
    {
        CLI_SET_ERR (CN_CLI_INVALID_COMP_ID);
        return SNMP_FAILURE;
    }

    if ((i4Ieee8021CnCpIfIndex < CN_MIN_PORTS) ||
        (i4Ieee8021CnCpIfIndex > CN_MAX_PORTS))
    {
        CLI_SET_ERR (CN_CLI_INVALID_PORT_ID);
        return SNMP_FAILURE;
    }

    if ((u4Ieee8021CnCpIndex < CN_CPINDEX_FROM_PRIO (CN_MIN_CNPV)) ||
        (u4Ieee8021CnCpIndex > CN_CPINDEX_FROM_PRIO
         (CN_MAX_VLAN_PRIORITY - CN_ONE)))
    {
        CLI_SET_ERR (CN_CLI_INVALID_CPINDEX);
        return SNMP_FAILURE;
    }
    pCnCompTblEntry = CN_COMP_TBL_ENTRY (u4Ieee8021CnCpComponentId);
    if (pCnCompTblEntry == NULL)
    {
        CLI_SET_ERR (CN_CLI_COMP_ENTRY_NOT_FOUND);
        CN_GBL_TRC (CN_INVALID_CONTEXT,
                    (CN_MGMT_TRC | CN_ALL_FAILURE_TRC),
                    "nmhValidateIndexInstanceIeee8021CnCpTable: FAILED\r\n"
                    "Context is not present in CN !!!\r\n");

        return SNMP_FAILURE;
    }

    CN_PORT_TBL_ENTRY (u4Ieee8021CnCpComponentId, u4PortId, pCnPortTblEntry);
    if (pCnPortTblEntry == NULL)
    {
        CLI_SET_ERR (CN_CLI_PORT_ENTRY_NOT_FOUND);
        CN_TRC_ARG1 (u4Ieee8021CnCpComponentId,
                     (CN_MGMT_TRC | CN_ALL_FAILURE_TRC),
                     "nmhValidateIndexInstanceIeee8021CnCpTable: FAILED\r\n"
                     "Port Entry does not exists for port %d !!!\r\n",
                     u4PortId);
        return SNMP_FAILURE;
    }

    u4Priority = CN_PRIO_FROM_CPINDEX (u4Ieee8021CnCpIndex);
    CN_PORT_PRI_ENTRY (u4Ieee8021CnCpComponentId, u4PortId,
                       u4Priority, pCnPortPriEntry);
    if (pCnPortPriEntry == NULL)
    {
        CLI_SET_ERR (CN_CLI_PORT_PRI_ENTRY_NOT_FOUND);
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexIeee8021CnCpTable
 Input       :  The Indices
                Ieee8021CnCpComponentId
                Ieee8021CnCpIfIndex
                Ieee8021CnCpIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexIeee8021CnCpTable (UINT4 *pu4Ieee8021CnCpComponentId,
                                   INT4 *pi4Ieee8021CnCpIfIndex,
                                   UINT4 *pu4Ieee8021CnCpIndex)
{
    tCnPortPriTblInfo  *pCnPortPriEntry = NULL;
    UINT4               u4CompId = CN_MIN_CONTEXT;
    UINT4               u4PortId = CN_MIN_PORTS;
    UINT4               u4Priority = CN_ZERO;

    CN_PORT_PRI_ENTRY (u4CompId, u4PortId, u4Priority, pCnPortPriEntry);
    if (pCnPortPriEntry != NULL)
    {
        *pu4Ieee8021CnCpComponentId = u4CompId;
        *pi4Ieee8021CnCpIfIndex = pCnPortPriEntry->u4PortId;
        *pu4Ieee8021CnCpIndex = CN_CPINDEX_FROM_PRIO (pCnPortPriEntry->u1CNPV);
        return SNMP_SUCCESS;
    }
    return (nmhGetNextIndexIeee8021CnCpTable (u4CompId,
                                              pu4Ieee8021CnCpComponentId,
                                              u4PortId, pi4Ieee8021CnCpIfIndex,
                                              CN_CPINDEX_FROM_PRIO (u4Priority),
                                              pu4Ieee8021CnCpIndex));
}

/****************************************************************************
 Function    :  nmhGetNextIndexIeee8021CnCpTable
 Input       :  The Indices
                Ieee8021CnCpComponentId
                nextIeee8021CnCpComponentId
                Ieee8021CnCpIfIndex
                nextIeee8021CnCpIfIndex
                Ieee8021CnCpIndex
                nextIeee8021CnCpIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexIeee8021CnCpTable (UINT4 u4Ieee8021CnCpComponentId,
                                  UINT4 *pu4NextIeee8021CnCpComponentId,
                                  INT4 i4Ieee8021CnCpIfIndex,
                                  INT4 *pi4NextIeee8021CnCpIfIndex,
                                  UINT4 u4Ieee8021CnCpIndex,
                                  UINT4 *pu4NextIeee8021CnCpIndex)
{
    UINT4               u4PortId = CN_ZERO;
    UINT4               u4CompId = CN_MIN_CONTEXT;
    UINT4               u4Priority = CN_ZERO;
    tCnCompTblInfo     *pCnCompTblEntry = NULL;
    tCnPortTblInfo     *pCnPortTblEntry = NULL;
    tCnPortTblInfo      TempPortTblEntry;
    tCnPortTblInfo     *pCnTempPortTblEntry = NULL;
    tCnPortPriTblInfo  *pCnPortPriEntry = NULL;

    MEMSET (&TempPortTblEntry, CN_ZERO, sizeof (tCnPortTblInfo));
    for (u4CompId = u4Ieee8021CnCpComponentId; u4CompId < CN_MAX_CONTEXTS;
         u4CompId++)
    {
        pCnCompTblEntry = CN_COMP_TBL_ENTRY (u4CompId);
        if (pCnCompTblEntry != NULL)
        {
            if (u4CompId == u4Ieee8021CnCpComponentId)
            {
                TempPortTblEntry.u4PortId = (UINT4) i4Ieee8021CnCpIfIndex;
                pCnTempPortTblEntry = &TempPortTblEntry;
                pCnPortTblEntry =
                    (tCnPortTblInfo *) RBTreeGet (pCnCompTblEntry->PortTbl,
                                                  pCnTempPortTblEntry);
            }
            else
            {
                pCnPortTblEntry = (tCnPortTblInfo *) RBTreeGetFirst
                    (pCnCompTblEntry->PortTbl);
            }
            while (pCnPortTblEntry != NULL)
            {
                u4PortId = pCnPortTblEntry->u4PortId;
                if ((u4CompId == u4Ieee8021CnCpComponentId) &&
                    (u4PortId == (UINT4) i4Ieee8021CnCpIfIndex))
                {
                    u4Priority = CN_PRIO_FROM_CPINDEX (u4Ieee8021CnCpIndex)
                        + CN_ONE;
                }
                else
                {
                    u4Priority = CN_ZERO;
                }
                for (; u4Priority < CN_MAX_VLAN_PRIORITY; u4Priority++)
                {
                    CN_PORT_PRI_ENTRY (u4CompId, u4PortId, u4Priority,
                                       pCnPortPriEntry);
                    if (pCnPortPriEntry != NULL)
                    {
                        *pu4NextIeee8021CnCpComponentId = u4CompId;
                        *pi4NextIeee8021CnCpIfIndex =
                            (INT4) pCnPortPriEntry->u4PortId;
                        *pu4NextIeee8021CnCpIndex =
                            CN_CPINDEX_FROM_PRIO (pCnPortPriEntry->u1CNPV);
                        return SNMP_SUCCESS;
                    }
                }
                pCnTempPortTblEntry = pCnPortTblEntry;
                pCnPortTblEntry = (tCnPortTblInfo *) RBTreeGetNext
                    (pCnCompTblEntry->PortTbl, pCnTempPortTblEntry, NULL);
            }
        }
    }
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetIeee8021CnCpPriority
 Input       :  The Indices
                Ieee8021CnCpComponentId
                Ieee8021CnCpIfIndex
                Ieee8021CnCpIndex

                The Object 
                retValIeee8021CnCpPriority
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021CnCpPriority (UINT4 u4Ieee8021CnCpComponentId,
                            INT4 i4Ieee8021CnCpIfIndex,
                            UINT4 u4Ieee8021CnCpIndex,
                            UINT4 *pi4RetValIeee8021CnCpPriority)
{
    tCnPortPriTblInfo  *pCnPortPriEntry = NULL;
    UINT4               u4PortId = CN_ZERO;
    UINT4               u4Priority = CN_ZERO;

    u4PortId = (UINT4) i4Ieee8021CnCpIfIndex;
    u4Priority = CN_PRIO_FROM_CPINDEX (u4Ieee8021CnCpIndex);
    CN_PORT_PRI_ENTRY (u4Ieee8021CnCpComponentId, u4PortId, u4Priority,
                       pCnPortPriEntry);
    if (pCnPortPriEntry != NULL)
    {
        *pi4RetValIeee8021CnCpPriority = pCnPortPriEntry->u1CnCpPriority;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetIeee8021CnCpMacAddress
 Input       :  The Indices
                Ieee8021CnCpComponentId
                Ieee8021CnCpIfIndex
                Ieee8021CnCpIndex

                The Object 
                retValIeee8021CnCpMacAddress
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021CnCpMacAddress (UINT4 u4Ieee8021CnCpComponentId,
                              INT4 i4Ieee8021CnCpIfIndex,
                              UINT4 u4Ieee8021CnCpIndex,
                              tMacAddr * pRetValIeee8021CnCpMacAddress)
{
    tCnPortPriTblInfo  *pCnPortPriEntry = NULL;
    UINT4               u4PortId = CN_ZERO;
    UINT4               u4Priority = CN_ZERO;
    tCfaIfInfo          IfInfo;

    MEMSET (&IfInfo, CN_ZERO, sizeof (tCfaIfInfo));

    u4PortId = (UINT4) i4Ieee8021CnCpIfIndex;
    u4Priority = CN_PRIO_FROM_CPINDEX (u4Ieee8021CnCpIndex);

    CN_PORT_PRI_ENTRY (u4Ieee8021CnCpComponentId, u4PortId, u4Priority,
                       pCnPortPriEntry);
    if (pCnPortPriEntry != NULL)
    {
        if (CnPortCfaGetIfInfo (u4PortId, &IfInfo) == OSIX_FAILURE)
        {
            CN_TRC (u4Ieee8021CnCpComponentId,
                    (CN_MGMT_TRC | CN_ALL_FAILURE_TRC),
                    "nmhGetIeee8021CnCpMacAddress: FAILED\r\n"
                    " to Get MAC Address from CFA !!!\r\n");
            return SNMP_FAILURE;
        }
        MEMCPY (pRetValIeee8021CnCpMacAddress, IfInfo.au1MacAddr,
                sizeof (IfInfo.au1MacAddr));
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetIeee8021CnCpIdentifier
 Input       :  The Indices
                Ieee8021CnCpComponentId
                Ieee8021CnCpIfIndex
                Ieee8021CnCpIndex

                The Object 
                retValIeee8021CnCpIdentifier
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021CnCpIdentifier (UINT4 u4Ieee8021CnCpComponentId,
                              INT4 i4Ieee8021CnCpIfIndex,
                              UINT4 u4Ieee8021CnCpIndex,
                              tSNMP_OCTET_STRING_TYPE *
                              pRetValIeee8021CnCpIdentifier)
{
    tCnPortPriTblInfo  *pCnPortPriEntry = NULL;
    UINT4               u4PortId = CN_ZERO;
    UINT4               u4Priority = CN_ZERO;

    u4PortId = (UINT4) i4Ieee8021CnCpIfIndex;
    u4Priority = CN_PRIO_FROM_CPINDEX (u4Ieee8021CnCpIndex);
    CN_PORT_PRI_ENTRY (u4Ieee8021CnCpComponentId, u4PortId, u4Priority,
                       pCnPortPriEntry);

    if (pCnPortPriEntry != NULL)
    {
        MEMCPY (pRetValIeee8021CnCpIdentifier->pu1_OctetList,
                pCnPortPriEntry->au1CnCpIdentifier, CN_CPID_LEN);
        pRetValIeee8021CnCpIdentifier->i4_Length = CN_CPID_LEN;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetIeee8021CnCpQueueSizeSetPoint
 Input       :  The Indices
                Ieee8021CnCpComponentId
                Ieee8021CnCpIfIndex
                Ieee8021CnCpIndex

                The Object 
                retValIeee8021CnCpQueueSizeSetPoint
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021CnCpQueueSizeSetPoint (UINT4 u4Ieee8021CnCpComponentId,
                                     INT4 i4Ieee8021CnCpIfIndex,
                                     UINT4 u4Ieee8021CnCpIndex,
                                     UINT4
                                     *pi4RetValIeee8021CnCpQueueSizeSetPoint)
{
    tCnPortPriTblInfo  *pCnPortPriEntry = NULL;
    UINT4               u4PortId = CN_ZERO;
    UINT4               u4Priority = CN_ZERO;

    u4PortId = (UINT4) i4Ieee8021CnCpIfIndex;
    u4Priority = CN_PRIO_FROM_CPINDEX (u4Ieee8021CnCpIndex);

    CN_PORT_PRI_ENTRY (u4Ieee8021CnCpComponentId, u4PortId, u4Priority,
                       pCnPortPriEntry);

    if (pCnPortPriEntry != NULL)
    {
        *pi4RetValIeee8021CnCpQueueSizeSetPoint =
            pCnPortPriEntry->u4CpQSizeSetPoint;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetIeee8021CnCpFeedbackWeight
 Input       :  The Indices
                Ieee8021CnCpComponentId
                Ieee8021CnCpIfIndex
                Ieee8021CnCpIndex

                The Object 
                retValIeee8021CnCpFeedbackWeight
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021CnCpFeedbackWeight (UINT4 u4Ieee8021CnCpComponentId,
                                  INT4 i4Ieee8021CnCpIfIndex,
                                  UINT4 u4Ieee8021CnCpIndex,
                                  INT4 *pi4RetValIeee8021CnCpFeedbackWeight)
{
    tCnPortPriTblInfo  *pCnPortPriEntry = NULL;
    UINT4               u4PortId = CN_ZERO;
    UINT4               u4Priority = CN_ZERO;

    u4PortId = (UINT4) i4Ieee8021CnCpIfIndex;
    u4Priority = CN_PRIO_FROM_CPINDEX (u4Ieee8021CnCpIndex);

    CN_PORT_PRI_ENTRY (u4Ieee8021CnCpComponentId, u4PortId, u4Priority,
                       pCnPortPriEntry);

    if (pCnPortPriEntry != NULL)
    {
        *pi4RetValIeee8021CnCpFeedbackWeight =
            pCnPortPriEntry->i1CnCpFeedbackWeight;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetIeee8021CnCpMinSampleBase
 Input       :  The Indices
                Ieee8021CnCpComponentId
                Ieee8021CnCpIfIndex
                Ieee8021CnCpIndex

                The Object 
                retValIeee8021CnCpMinSampleBase
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021CnCpMinSampleBase (UINT4 u4Ieee8021CnCpComponentId,
                                 INT4 i4Ieee8021CnCpIfIndex,
                                 UINT4 u4Ieee8021CnCpIndex,
                                 UINT4 *pi4RetValIeee8021CnCpMinSampleBase)
{
    tCnPortPriTblInfo  *pCnPortPriEntry = NULL;
    UINT4               u4PortId = CN_ZERO;
    UINT4               u4Priority = CN_ZERO;

    u4PortId = (UINT4) i4Ieee8021CnCpIfIndex;
    u4Priority = CN_PRIO_FROM_CPINDEX (u4Ieee8021CnCpIndex);

    CN_PORT_PRI_ENTRY (u4Ieee8021CnCpComponentId, u4PortId, u4Priority,
                       pCnPortPriEntry);
    if (pCnPortPriEntry != NULL)
    {
        *pi4RetValIeee8021CnCpMinSampleBase =
            pCnPortPriEntry->u4CpMinSampleBase;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetIeee8021CnCpDiscardedFrames
 Input       :  The Indices
                Ieee8021CnCpComponentId
                Ieee8021CnCpIfIndex
                Ieee8021CnCpIndex

                The Object 
                retValIeee8021CnCpDiscardedFrames
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021CnCpDiscardedFrames (UINT4 u4Ieee8021CnCpComponentId,
                                   INT4 i4Ieee8021CnCpIfIndex,
                                   UINT4 u4Ieee8021CnCpIndex,
                                   tSNMP_COUNTER64_TYPE *
                                   pu8RetValIeee8021CnCpDiscardedFrames)
{
    tSNMP_COUNTER64_TYPE u8TransFrames;
    tSNMP_COUNTER64_TYPE u8TransCnms;

    MEMSET (&u8TransFrames, CN_ZERO, sizeof (tSNMP_COUNTER64_TYPE));
    MEMSET (&u8TransCnms, CN_ZERO, sizeof (tSNMP_COUNTER64_TYPE));

    if (CnHwGetCounters (u4Ieee8021CnCpComponentId, i4Ieee8021CnCpIfIndex,
                         ((UINT1) CN_PRIO_FROM_CPINDEX (u4Ieee8021CnCpIndex)),
                         pu8RetValIeee8021CnCpDiscardedFrames,
                         &u8TransFrames, &u8TransCnms) == OSIX_FAILURE)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetIeee8021CnCpTransmittedFrames
 Input       :  The Indices
                Ieee8021CnCpComponentId
                Ieee8021CnCpIfIndex
                Ieee8021CnCpIndex

                The Object 
                retValIeee8021CnCpTransmittedFrames
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021CnCpTransmittedFrames (UINT4 u4Ieee8021CnCpComponentId,
                                     INT4 i4Ieee8021CnCpIfIndex,
                                     UINT4 u4Ieee8021CnCpIndex,
                                     tSNMP_COUNTER64_TYPE *
                                     pu8RetValIeee8021CnCpTransmittedFrames)
{
    tSNMP_COUNTER64_TYPE u8DisFrames;
    tSNMP_COUNTER64_TYPE u8TransCnms;

    MEMSET (&u8DisFrames, CN_ZERO, sizeof (tSNMP_COUNTER64_TYPE));
    MEMSET (&u8TransCnms, CN_ZERO, sizeof (tSNMP_COUNTER64_TYPE));

    if (CnHwGetCounters (u4Ieee8021CnCpComponentId, i4Ieee8021CnCpIfIndex,
                         ((UINT1) CN_PRIO_FROM_CPINDEX (u4Ieee8021CnCpIndex)),
                         &u8DisFrames, pu8RetValIeee8021CnCpTransmittedFrames,
                         &u8TransCnms) == OSIX_FAILURE)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetIeee8021CnCpTransmittedCnms
 Input       :  The Indices
                Ieee8021CnCpComponentId
                Ieee8021CnCpIfIndex
                Ieee8021CnCpIndex

                The Object 
                retValIeee8021CnCpTransmittedCnms
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021CnCpTransmittedCnms (UINT4 u4Ieee8021CnCpComponentId,
                                   INT4 i4Ieee8021CnCpIfIndex,
                                   UINT4 u4Ieee8021CnCpIndex,
                                   tSNMP_COUNTER64_TYPE *
                                   pu8RetValIeee8021CnCpTransmittedCnms)
{
    tSNMP_COUNTER64_TYPE u8DisFrames;
    tSNMP_COUNTER64_TYPE u8TransFrames;

    MEMSET (&u8TransFrames, CN_ZERO, sizeof (tSNMP_COUNTER64_TYPE));
    MEMSET (&u8TransFrames, CN_ZERO, sizeof (tSNMP_COUNTER64_TYPE));

    if (CnHwGetCounters (u4Ieee8021CnCpComponentId, i4Ieee8021CnCpIfIndex,
                         ((UINT1) CN_PRIO_FROM_CPINDEX (u4Ieee8021CnCpIndex)),
                         &u8DisFrames, &u8TransFrames,
                         pu8RetValIeee8021CnCpTransmittedCnms) == OSIX_FAILURE)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetIeee8021CnCpMinHeaderOctets
 Input       :  The Indices
                Ieee8021CnCpComponentId
                Ieee8021CnCpIfIndex
                Ieee8021CnCpIndex

                The Object 
                retValIeee8021CnCpMinHeaderOctets
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021CnCpMinHeaderOctets (UINT4 u4Ieee8021CnCpComponentId,
                                   INT4 i4Ieee8021CnCpIfIndex,
                                   UINT4 u4Ieee8021CnCpIndex,
                                   UINT4 *pi4RetValIeee8021CnCpMinHeaderOctets)
{
    tCnPortPriTblInfo  *pCnPortPriEntry = NULL;
    UINT4               u4PortId = CN_ZERO;
    UINT4               u4Priority = CN_ZERO;

    u4PortId = (UINT4) i4Ieee8021CnCpIfIndex;
    u4Priority = CN_PRIO_FROM_CPINDEX (u4Ieee8021CnCpIndex);
    CN_PORT_PRI_ENTRY (u4Ieee8021CnCpComponentId, u4PortId, u4Priority,
                       pCnPortPriEntry);
    if (pCnPortPriEntry != NULL)
    {
        *pi4RetValIeee8021CnCpMinHeaderOctets =
            (INT4) pCnPortPriEntry->u1CpMinHeaderOctets;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetIeee8021CnCpQueueSizeSetPoint
 Input       :  The Indices
                Ieee8021CnCpComponentId
                Ieee8021CnCpIfIndex
                Ieee8021CnCpIndex

                The Object 
                setValIeee8021CnCpQueueSizeSetPoint
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIeee8021CnCpQueueSizeSetPoint (UINT4 u4Ieee8021CnCpComponentId,
                                     INT4 i4Ieee8021CnCpIfIndex,
                                     UINT4 u4Ieee8021CnCpIndex,
                                     UINT4
                                     u4SetValIeee8021CnCpQueueSizeSetPoint)
{
    tCnPortPriTblInfo  *pCnPortPriEntry = NULL;
    tCnCompTblInfo     *pCnCompTblEntry = NULL;
    UINT4               u4PortId = CN_ZERO;
    UINT4               u4Priority = CN_ZERO;

    u4PortId = (UINT4) i4Ieee8021CnCpIfIndex;
    u4Priority = CN_PRIO_FROM_CPINDEX (u4Ieee8021CnCpIndex);

    CN_PORT_PRI_ENTRY (u4Ieee8021CnCpComponentId, u4PortId, u4Priority,
                       pCnPortPriEntry);
    if (pCnPortPriEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    /*Already same value for QSizeSetPoint */
    if (pCnPortPriEntry->u4CpQSizeSetPoint ==
        u4SetValIeee8021CnCpQueueSizeSetPoint)
    {
        return SNMP_SUCCESS;
    }

    pCnPortPriEntry->u4CpQSizeSetPoint = u4SetValIeee8021CnCpQueueSizeSetPoint;

    /*Configure in Hardware */
    pCnCompTblEntry = CN_COMP_TBL_ENTRY (u4Ieee8021CnCpComponentId);
    if (pCnCompTblEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    if ((pCnCompTblEntry->bCnCompMasterEnable == CN_MASTER_ENABLE) &&
        (pCnPortPriEntry->u1PortPriOperDefMode != CN_DISABLED))
    {
        CnHwSetCpParams (pCnPortPriEntry);
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetIeee8021CnCpFeedbackWeight
 Input       :  The Indices
                Ieee8021CnCpComponentId
                Ieee8021CnCpIfIndex
                Ieee8021CnCpIndex

                The Object 
                setValIeee8021CnCpFeedbackWeight
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIeee8021CnCpFeedbackWeight (UINT4 u4Ieee8021CnCpComponentId,
                                  INT4 i4Ieee8021CnCpIfIndex,
                                  UINT4 u4Ieee8021CnCpIndex,
                                  INT4 i4SetValIeee8021CnCpFeedbackWeight)
{
    tCnPortPriTblInfo  *pCnPortPriEntry = NULL;
    UINT4               u4PortId = CN_ZERO;
    UINT4               u4Priority = CN_ZERO;
    tCnCompTblInfo     *pCnCompTblEntry = NULL;

    u4PortId = (UINT4) i4Ieee8021CnCpIfIndex;
    u4Priority = CN_PRIO_FROM_CPINDEX (u4Ieee8021CnCpIndex);
    CN_PORT_PRI_ENTRY (u4Ieee8021CnCpComponentId, u4PortId, u4Priority,
                       pCnPortPriEntry);
    if (pCnPortPriEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    /*Already same value for FeedbackWeight */
    if (pCnPortPriEntry->i1CnCpFeedbackWeight ==
        i4SetValIeee8021CnCpFeedbackWeight)
    {
        return SNMP_SUCCESS;
    }
    pCnPortPriEntry->i1CnCpFeedbackWeight = ((INT1)
                                             i4SetValIeee8021CnCpFeedbackWeight);

    /*Configure in Hardware */
    pCnCompTblEntry = CN_COMP_TBL_ENTRY (u4Ieee8021CnCpComponentId);
    if ((pCnCompTblEntry != NULL) &&
        (pCnCompTblEntry->bCnCompMasterEnable == CN_MASTER_ENABLE) &&
        (pCnPortPriEntry->u1PortPriOperDefMode != CN_DISABLED))
    {
        CnHwSetCpParams (pCnPortPriEntry);
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetIeee8021CnCpMinSampleBase
 Input       :  The Indices
                Ieee8021CnCpComponentId
                Ieee8021CnCpIfIndex
                Ieee8021CnCpIndex

                The Object 
                setValIeee8021CnCpMinSampleBase
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIeee8021CnCpMinSampleBase (UINT4 u4Ieee8021CnCpComponentId,
                                 INT4 i4Ieee8021CnCpIfIndex,
                                 UINT4 u4Ieee8021CnCpIndex,
                                 UINT4 u4SetValIeee8021CnCpMinSampleBase)
{
    tCnPortPriTblInfo  *pCnPortPriEntry = NULL;
    UINT4               u4PortId = CN_ZERO;
    UINT4               u4Priority = CN_ZERO;
    tCnCompTblInfo     *pCnCompTblEntry = NULL;

    u4PortId = (UINT4) i4Ieee8021CnCpIfIndex;
    u4Priority = CN_PRIO_FROM_CPINDEX (u4Ieee8021CnCpIndex);
    CN_PORT_PRI_ENTRY (u4Ieee8021CnCpComponentId, u4PortId, u4Priority,
                       pCnPortPriEntry);
    if (pCnPortPriEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pCnPortPriEntry->u4CpMinSampleBase == u4SetValIeee8021CnCpMinSampleBase)
    {
        return SNMP_SUCCESS;
    }

    pCnPortPriEntry->u4CpMinSampleBase = u4SetValIeee8021CnCpMinSampleBase;

    pCnCompTblEntry = CN_COMP_TBL_ENTRY (u4Ieee8021CnCpComponentId);
    if (pCnCompTblEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    if ((pCnCompTblEntry->bCnCompMasterEnable == CN_MASTER_ENABLE) &&
        (pCnPortPriEntry->u1PortPriOperDefMode != CN_DISABLED))
    {
        CnHwSetCpParams (pCnPortPriEntry);
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetIeee8021CnCpMinHeaderOctets
 Input       :  The Indices
                Ieee8021CnCpComponentId
                Ieee8021CnCpIfIndex
                Ieee8021CnCpIndex

                The Object 
                setValIeee8021CnCpMinHeaderOctets
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetIeee8021CnCpMinHeaderOctets (UINT4 u4Ieee8021CnCpComponentId,
                                   INT4 i4Ieee8021CnCpIfIndex,
                                   UINT4 u4Ieee8021CnCpIndex,
                                   UINT4 u4SetValIeee8021CnCpMinHeaderOctets)
{
    tCnPortPriTblInfo  *pCnPortPriEntry = NULL;
    tCnCompTblInfo     *pCnCompTblEntry = NULL;
    UINT4               u4PortId = CN_ZERO;
    UINT4               u4Priority = CN_ZERO;

    pCnCompTblEntry = CN_COMP_TBL_ENTRY (u4Ieee8021CnCpComponentId);
    u4PortId = (UINT4) i4Ieee8021CnCpIfIndex;
    u4Priority = CN_PRIO_FROM_CPINDEX (u4Ieee8021CnCpIndex);
    CN_PORT_PRI_ENTRY (u4Ieee8021CnCpComponentId, u4PortId, u4Priority,
                       pCnPortPriEntry);
    if (pCnPortPriEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    /*Already same value for MinHeaderOctets */
    if (pCnPortPriEntry->u1CpMinHeaderOctets ==
        u4SetValIeee8021CnCpMinHeaderOctets)
    {
        return SNMP_SUCCESS;
    }
    pCnPortPriEntry->u1CpMinHeaderOctets = ((UINT1)
                                            u4SetValIeee8021CnCpMinHeaderOctets);

    /*Configure in Hardware */
    if ((pCnCompTblEntry->bCnCompMasterEnable == CN_MASTER_ENABLE) &&
        (pCnPortPriEntry->u1PortPriOperDefMode != CN_DISABLED))
    {
        CnHwSetCpParams (pCnPortPriEntry);
    }
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2Ieee8021CnCpQueueSizeSetPoint
 Input       :  The Indices
                Ieee8021CnCpComponentId
                Ieee8021CnCpIfIndex
                Ieee8021CnCpIndex

                The Object 
                testValIeee8021CnCpQueueSizeSetPoint
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ieee8021CnCpQueueSizeSetPoint (UINT4 *pu4ErrorCode,
                                        UINT4 u4Ieee8021CnCpComponentId,
                                        INT4 i4Ieee8021CnCpIfIndex,
                                        UINT4 u4Ieee8021CnCpIndex,
                                        UINT4
                                        u4TestValIeee8021CnCpQueueSizeSetPoint)
{
    tCnPortPriTblInfo  *pCnPortPriEntry = NULL;
    tCnCompTblInfo     *pCnCompTblEntry = NULL;
    tCnPortTblInfo     *pCnPortTblEntry = NULL;
    UINT4               u4PortId = CN_ZERO;

    if (CN_IS_SHUTDOWN ())
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (CN_IS_VALID_COMP_ID (u4Ieee8021CnCpComponentId) == OSIX_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CN_CLI_INVALID_COMP_ID);
        return SNMP_FAILURE;
    }

    if ((i4Ieee8021CnCpIfIndex < CN_MIN_PORTS) ||
        (i4Ieee8021CnCpIfIndex > CN_MAX_PORTS))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CN_CLI_INVALID_PORT_ID);
        return SNMP_FAILURE;
    }

    if ((u4Ieee8021CnCpIndex < CN_CPINDEX_FROM_PRIO (CN_MIN_CNPV)) ||
        (u4Ieee8021CnCpIndex > CN_CPINDEX_FROM_PRIO
         (CN_MAX_VLAN_PRIORITY - CN_ONE)))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CN_CLI_INVALID_CPINDEX);
        return SNMP_FAILURE;
    }

    if (u4TestValIeee8021CnCpQueueSizeSetPoint < CN_MIN_QUEUE_SET_PT)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CN_CLI_INVALID_QSETPT);
        return SNMP_FAILURE;
    }

    pCnCompTblEntry = CN_COMP_TBL_ENTRY (u4Ieee8021CnCpComponentId);
    if (pCnCompTblEntry == NULL)
    {
        CLI_SET_ERR (CN_CLI_COMP_ENTRY_NOT_FOUND);
        return SNMP_FAILURE;
    }

    u4PortId = (UINT4) i4Ieee8021CnCpIfIndex;
    CN_PORT_TBL_ENTRY (u4Ieee8021CnCpComponentId, u4PortId, pCnPortTblEntry);
    if (pCnPortTblEntry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CN_CLI_PORT_ENTRY_NOT_FOUND);
        CN_TRC_ARG1 (u4Ieee8021CnCpComponentId,
                     (CN_MGMT_TRC | CN_ALL_FAILURE_TRC),
                     "nmhTestv2Ieee8021CnCpQueueSizeSetPoint: FAILED"
                     "Port Entry does not exists for port %d !!!\r\n",
                     u4PortId);
        return SNMP_FAILURE;
    }

    CN_PORT_PRI_ENTRY (u4Ieee8021CnCpComponentId, u4PortId,
                       CN_PRIO_FROM_CPINDEX (u4Ieee8021CnCpIndex),
                       pCnPortPriEntry);
    if (pCnPortPriEntry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CN_CLI_PORT_PRI_ENTRY_NOT_FOUND);
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Ieee8021CnCpFeedbackWeight
 Input       :  The Indices
                Ieee8021CnCpComponentId
                Ieee8021CnCpIfIndex
                Ieee8021CnCpIndex

                The Object 
                testValIeee8021CnCpFeedbackWeight
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ieee8021CnCpFeedbackWeight (UINT4 *pu4ErrorCode,
                                     UINT4 u4Ieee8021CnCpComponentId,
                                     INT4 i4Ieee8021CnCpIfIndex,
                                     UINT4 u4Ieee8021CnCpIndex,
                                     INT4 i4TestValIeee8021CnCpFeedbackWeight)
{
    tCnPortPriTblInfo  *pCnPortPriEntry = NULL;
    UINT4               u4PortId = CN_ZERO;
    tCnCompTblInfo     *pCnCompTblEntry = NULL;
    tCnPortTblInfo     *pCnPortTblEntry = NULL;

    if (CN_IS_SHUTDOWN ())
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (CN_IS_VALID_COMP_ID (u4Ieee8021CnCpComponentId) == OSIX_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CN_CLI_INVALID_COMP_ID);
        return SNMP_FAILURE;
    }

    if ((i4Ieee8021CnCpIfIndex < CN_MIN_PORTS) ||
        (i4Ieee8021CnCpIfIndex > CN_MAX_PORTS))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CN_CLI_INVALID_PORT_ID);
        return SNMP_FAILURE;
    }

    if ((u4Ieee8021CnCpIndex < CN_CPINDEX_FROM_PRIO (CN_MIN_CNPV)) ||
        (u4Ieee8021CnCpIndex > CN_CPINDEX_FROM_PRIO
         (CN_MAX_VLAN_PRIORITY - CN_ONE)))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CN_CLI_INVALID_CPINDEX);
        return SNMP_FAILURE;
    }

    if ((i4TestValIeee8021CnCpFeedbackWeight < CN_MIN_FEEDBACK_WEIGHT) ||
        (i4TestValIeee8021CnCpFeedbackWeight > CN_MAX_FEEDBACK_WEIGHT))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CN_CLI_INVALID_FB_WGT);
        return SNMP_FAILURE;
    }

    pCnCompTblEntry = CN_COMP_TBL_ENTRY (u4Ieee8021CnCpComponentId);
    if (pCnCompTblEntry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CN_CLI_COMP_ENTRY_NOT_FOUND);
        return SNMP_FAILURE;
    }

    u4PortId = (UINT4) i4Ieee8021CnCpIfIndex;
    CN_PORT_TBL_ENTRY (u4Ieee8021CnCpComponentId, u4PortId, pCnPortTblEntry);
    if (pCnPortTblEntry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CN_CLI_PORT_ENTRY_NOT_FOUND);
        CN_TRC_ARG1 (u4Ieee8021CnCpComponentId,
                     (CN_MGMT_TRC | CN_ALL_FAILURE_TRC),
                     "nmhTestv2Ieee8021CnCpFeedbackWeight: FAILED\r\n"
                     "Port Entry does not exists for port %d !!!\r\n",
                     u4PortId);
        return SNMP_FAILURE;
    }

    CN_PORT_PRI_ENTRY (u4Ieee8021CnCpComponentId, u4PortId,
                       CN_PRIO_FROM_CPINDEX (u4Ieee8021CnCpIndex),
                       pCnPortPriEntry);
    if (pCnPortPriEntry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CN_CLI_ENTRY_NOT_FOUND);
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Ieee8021CnCpMinSampleBase
 Input       :  The Indices
                Ieee8021CnCpComponentId
                Ieee8021CnCpIfIndex
                Ieee8021CnCpIndex

                The Object 
                testValIeee8021CnCpMinSampleBase
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ieee8021CnCpMinSampleBase (UINT4 *pu4ErrorCode,
                                    UINT4 u4Ieee8021CnCpComponentId,
                                    INT4 i4Ieee8021CnCpIfIndex,
                                    UINT4 u4Ieee8021CnCpIndex,
                                    UINT4 u4TestValIeee8021CnCpMinSampleBase)
{
    tCnPortPriTblInfo  *pCnPortPriEntry = NULL;
    tCnCompTblInfo     *pCnCompTblEntry = NULL;
    tCnPortTblInfo     *pCnPortTblEntry = NULL;
    UINT4               u4PortId = CN_ZERO;

    if (CN_IS_SHUTDOWN ())
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (CN_IS_VALID_COMP_ID (u4Ieee8021CnCpComponentId) == OSIX_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CN_CLI_INVALID_COMP_ID);
        return SNMP_FAILURE;
    }

    if ((i4Ieee8021CnCpIfIndex < CN_MIN_PORTS) ||
        (i4Ieee8021CnCpIfIndex > CN_MAX_PORTS))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CN_CLI_INVALID_PORT_ID);
        return SNMP_FAILURE;
    }

    if ((u4Ieee8021CnCpIndex < CN_CPINDEX_FROM_PRIO (CN_MIN_CNPV)) ||
        (u4Ieee8021CnCpIndex > CN_CPINDEX_FROM_PRIO
         (CN_MAX_VLAN_PRIORITY - CN_ONE)))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CN_CLI_INVALID_CPINDEX);
        return SNMP_FAILURE;
    }

    if (u4TestValIeee8021CnCpMinSampleBase < CN_MIN_MIN_SAMPLE_BASE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CN_CLI_INVALID_SAMPLE_BASE);
        return SNMP_FAILURE;
    }

    pCnCompTblEntry = CN_COMP_TBL_ENTRY (u4Ieee8021CnCpComponentId);
    if (pCnCompTblEntry == NULL)
    {
        CLI_SET_ERR (CN_CLI_COMP_ENTRY_NOT_FOUND);
        return SNMP_FAILURE;
    }

    u4PortId = (UINT4) i4Ieee8021CnCpIfIndex;
    CN_PORT_TBL_ENTRY (u4Ieee8021CnCpComponentId, u4PortId, pCnPortTblEntry);
    if (pCnPortTblEntry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CN_CLI_PORT_ENTRY_NOT_FOUND);
        CN_TRC_ARG1 (u4Ieee8021CnCpComponentId,
                     (CN_MGMT_TRC | CN_ALL_FAILURE_TRC),
                     "nmhTestv2Ieee8021CnCpMinSampleBase: FAILED\r\n"
                     "Port Entry does not exists for port %d !!!\r\n",
                     u4PortId);
        return SNMP_FAILURE;
    }

    CN_PORT_PRI_ENTRY (u4Ieee8021CnCpComponentId, u4PortId,
                       CN_PRIO_FROM_CPINDEX (u4Ieee8021CnCpIndex),
                       pCnPortPriEntry);
    if (pCnPortPriEntry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CN_CLI_PORT_PRI_ENTRY_NOT_FOUND);
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2Ieee8021CnCpMinHeaderOctets
 Input       :  The Indices
                Ieee8021CnCpComponentId
                Ieee8021CnCpIfIndex
                Ieee8021CnCpIndex

                The Object 
                testValIeee8021CnCpMinHeaderOctets
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ieee8021CnCpMinHeaderOctets (UINT4 *pu4ErrorCode,
                                      UINT4 u4Ieee8021CnCpComponentId,
                                      INT4 i4Ieee8021CnCpIfIndex,
                                      UINT4 u4Ieee8021CnCpIndex,
                                      UINT4
                                      u4TestValIeee8021CnCpMinHeaderOctets)
{
    tCnPortPriTblInfo  *pCnPortPriEntry = NULL;
    tCnCompTblInfo     *pCnCompTblEntry = NULL;
    tCnPortTblInfo     *pCnPortTblEntry = NULL;
    UINT4               u4PortId = CN_ZERO;

    if (CN_IS_SHUTDOWN ())
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (CN_IS_VALID_COMP_ID (u4Ieee8021CnCpComponentId) == OSIX_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CN_CLI_INVALID_COMP_ID);
        return SNMP_FAILURE;
    }

    if ((i4Ieee8021CnCpIfIndex < CN_MIN_PORTS) ||
        (i4Ieee8021CnCpIfIndex > CN_MAX_PORTS))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CN_CLI_INVALID_PORT_ID);
        return SNMP_FAILURE;
    }

    if ((u4Ieee8021CnCpIndex < (UINT4) CN_CPINDEX_FROM_PRIO (CN_MIN_CNPV)) ||
        (u4Ieee8021CnCpIndex >
         (UINT4) CN_CPINDEX_FROM_PRIO (CN_MAX_VLAN_PRIORITY - CN_ONE)))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CN_CLI_INVALID_CPINDEX);
        return SNMP_FAILURE;
    }

    if (u4TestValIeee8021CnCpMinHeaderOctets > CN_MAX_HEADER_OCTETS)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CN_CLI_INVALID_HDR_OCTS);
        return SNMP_FAILURE;
    }

    pCnCompTblEntry = CN_COMP_TBL_ENTRY (u4Ieee8021CnCpComponentId);
    if (pCnCompTblEntry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CN_CLI_COMP_ENTRY_NOT_FOUND);
        return SNMP_FAILURE;
    }

    u4PortId = (UINT4) i4Ieee8021CnCpIfIndex;
    CN_PORT_TBL_ENTRY (u4Ieee8021CnCpComponentId, u4PortId, pCnPortTblEntry);
    if (pCnPortTblEntry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CN_CLI_PORT_ENTRY_NOT_FOUND);
        CN_TRC_ARG1 (u4Ieee8021CnCpComponentId,
                     (CN_MGMT_TRC | CN_ALL_FAILURE_TRC),
                     "nmhTestv2Ieee8021CnCpMinHeaderOctets: FAILED\r\n"
                     "Port Entry does not exists for port %d !!!\r\n",
                     u4PortId);
        return SNMP_FAILURE;
    }

    CN_PORT_PRI_ENTRY (u4Ieee8021CnCpComponentId, u4PortId,
                       CN_PRIO_FROM_CPINDEX (u4Ieee8021CnCpIndex),
                       pCnPortPriEntry);
    if (pCnPortPriEntry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CN_CLI_PORT_PRI_ENTRY_NOT_FOUND);
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2Ieee8021CnCpTable
 Input       :  The Indices
                Ieee8021CnCpComponentId
                Ieee8021CnCpIfIndex
                Ieee8021CnCpIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Ieee8021CnCpTable (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                           tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : Ieee8021CnCpidToInterfaceTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceIeee8021CnCpidToInterfaceTable
 Input       :  The Indices
                Ieee8021CnCpidToIfCpid
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1 
     
     
     
     
     
     
     
    nmhValidateIndexInstanceIeee8021CnCpidToInterfaceTable
    (tSNMP_OCTET_STRING_TYPE * pIeee8021CnCpidToIfCpid)
{
    tCnPortPriTblInfo  *pCnPortPriEntry = NULL;
    tCnPortPriTblInfo   TempPortPriEntry;

    MEMSET (&TempPortPriEntry, CN_ZERO, sizeof (tCnPortPriTblInfo));

    if (CN_IS_SHUTDOWN ())
    {
        return SNMP_FAILURE;
    }

    if (pIeee8021CnCpidToIfCpid->i4_Length != CN_CPID_LEN)
    {
        CLI_SET_ERR (CN_CLI_INVALID_CPID_LEN);
        return SNMP_FAILURE;
    }

    MEMCPY (TempPortPriEntry.au1CnCpIdentifier,
            pIeee8021CnCpidToIfCpid->pu1_OctetList, CN_CPID_LEN);
    pCnPortPriEntry = (tCnPortPriTblInfo *) RBTreeGet
        (gCnMasterTblInfo.CpIdTbl, &TempPortPriEntry);
    if (pCnPortPriEntry == NULL)
    {
        CLI_SET_ERR (CN_CLI_PORT_PRI_ENTRY_NOT_FOUND);
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexIeee8021CnCpidToInterfaceTable
 Input       :  The Indices
                Ieee8021CnCpidToIfCpid
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexIeee8021CnCpidToInterfaceTable (tSNMP_OCTET_STRING_TYPE
                                                * pIeee8021CnCpidToIfCpid)
{
    tCnPortPriTblInfo  *pCnPortPriEntry = NULL;

    pCnPortPriEntry = (tCnPortPriTblInfo *) RBTreeGetFirst
        (gCnMasterTblInfo.CpIdTbl);
    if (pCnPortPriEntry == NULL)
    {
        /*Entry does not exist */
        return SNMP_FAILURE;
    }
    MEMCPY (pIeee8021CnCpidToIfCpid->pu1_OctetList,
            pCnPortPriEntry->au1CnCpIdentifier, CN_CPID_LEN);
    pIeee8021CnCpidToIfCpid->i4_Length = CN_CPID_LEN;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexIeee8021CnCpidToInterfaceTable
 Input       :  The Indices
                Ieee8021CnCpidToIfCpid
                nextIeee8021CnCpidToIfCpid
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1                nmhGetNextIndexIeee8021CnCpidToInterfaceTable
    (tSNMP_OCTET_STRING_TYPE * pIeee8021CnCpidToIfCpid,
     tSNMP_OCTET_STRING_TYPE * pNextIeee8021CnCpidToIfCpid)
{
    tCnPortPriTblInfo  *pCnPortPriEntry = NULL;
    tCnPortPriTblInfo   TempPortPriEntry;

    MEMCPY (TempPortPriEntry.au1CnCpIdentifier,
            pIeee8021CnCpidToIfCpid->pu1_OctetList, CN_CPID_LEN);
    pCnPortPriEntry = (tCnPortPriTblInfo *) RBTreeGetNext
        (gCnMasterTblInfo.CpIdTbl, &TempPortPriEntry, NULL);
    if (pCnPortPriEntry == NULL)
    {
        /*Entry does not exist */
        return SNMP_FAILURE;
    }
    MEMCPY (pNextIeee8021CnCpidToIfCpid->pu1_OctetList,
            pCnPortPriEntry->au1CnCpIdentifier, CN_CPID_LEN);
    pNextIeee8021CnCpidToIfCpid->i4_Length = CN_CPID_LEN;
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetIeee8021CnCpidToIfComponentId
 Input       :  The Indices
                Ieee8021CnCpidToIfCpid

                The Object 
                retValIeee8021CnCpidToIfComponentId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021CnCpidToIfComponentId (tSNMP_OCTET_STRING_TYPE *
                                     pIeee8021CnCpidToIfCpid,
                                     UINT4
                                     *pi4RetValIeee8021CnCpidToIfComponentId)
{
    tCnPortPriTblInfo  *pCnPortPriEntry = NULL;
    tCnPortPriTblInfo   TempPortPriEntry;

    MEMCPY (TempPortPriEntry.au1CnCpIdentifier,
            pIeee8021CnCpidToIfCpid->pu1_OctetList, CN_CPID_LEN);
    pCnPortPriEntry = (tCnPortPriTblInfo *) RBTreeGet
        (gCnMasterTblInfo.CpIdTbl, &TempPortPriEntry);
    if (pCnPortPriEntry == NULL)
    {
        /*Entry does not exist */
        return SNMP_FAILURE;
    }
    *pi4RetValIeee8021CnCpidToIfComponentId =
        pCnPortPriEntry->u4CnComPriComponentId;
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetIeee8021CnCpidToIfIfIndex
 Input       :  The Indices
                Ieee8021CnCpidToIfCpid

                The Object 
                retValIeee8021CnCpidToIfIfIndex
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021CnCpidToIfIfIndex (tSNMP_OCTET_STRING_TYPE *
                                 pIeee8021CnCpidToIfCpid,
                                 INT4 *pi4RetValIeee8021CnCpidToIfIfIndex)
{
    tCnPortPriTblInfo  *pCnPortPriEntry = NULL;
    tCnPortPriTblInfo   TempPortPriEntry;

    MEMCPY (TempPortPriEntry.au1CnCpIdentifier,
            pIeee8021CnCpidToIfCpid->pu1_OctetList, CN_CPID_LEN);
    pCnPortPriEntry = (tCnPortPriTblInfo *) RBTreeGet
        (gCnMasterTblInfo.CpIdTbl, &TempPortPriEntry);
    if (pCnPortPriEntry == NULL)
    {
        /*Entry does not exist */
        return SNMP_FAILURE;
    }
    *pi4RetValIeee8021CnCpidToIfIfIndex = (INT4) pCnPortPriEntry->u4PortId;
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetIeee8021CnCpidToIfCpIndex
 Input       :  The Indices
                Ieee8021CnCpidToIfCpid

                The Object 
                retValIeee8021CnCpidToIfCpIndex
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021CnCpidToIfCpIndex (tSNMP_OCTET_STRING_TYPE *
                                 pIeee8021CnCpidToIfCpid,
                                 UINT4 *pi4RetValIeee8021CnCpidToIfCpIndex)
{
    tCnPortPriTblInfo  *pCnPortPriEntry = NULL;
    tCnPortPriTblInfo   TempPortPriEntry;

    MEMCPY (TempPortPriEntry.au1CnCpIdentifier,
            pIeee8021CnCpidToIfCpid->pu1_OctetList, CN_CPID_LEN);

    pCnPortPriEntry = (tCnPortPriTblInfo *) RBTreeGet
        (gCnMasterTblInfo.CpIdTbl, &TempPortPriEntry);
    if (pCnPortPriEntry == NULL)
    {
        /*Entry does not exist */
        return SNMP_FAILURE;
    }
    *pi4RetValIeee8021CnCpidToIfCpIndex =
        CN_CPINDEX_FROM_PRIO (pCnPortPriEntry->u1CNPV);
    return SNMP_SUCCESS;

}

/* LOW LEVEL Routines for Table : Ieee8021CnRpPortPriTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceIeee8021CnRpPortPriTable
 Input       :  The Indices
                Ieee8021CnRpPortPriComponentId
                Ieee8021CnRpPortPriPriority
                Ieee8021CnRpPortPriIfIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceIeee8021CnRpPortPriTable (UINT4
                                                  u4Ieee8021CnRpPortPriComponentId,
                                                  UINT4
                                                  u4Ieee8021CnRpPortPriPriority,
                                                  INT4
                                                  i4Ieee8021CnRpPortPriIfIndex)
{
    UNUSED_PARAM (u4Ieee8021CnRpPortPriComponentId);
    UNUSED_PARAM (u4Ieee8021CnRpPortPriPriority);
    UNUSED_PARAM (i4Ieee8021CnRpPortPriIfIndex);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexIeee8021CnRpPortPriTable
 Input       :  The Indices
                Ieee8021CnRpPortPriComponentId
                Ieee8021CnRpPortPriPriority
                Ieee8021CnRpPortPriIfIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexIeee8021CnRpPortPriTable (UINT4
                                          *pu4Ieee8021CnRpPortPriComponentId,
                                          UINT4 *pu4Ieee8021CnRpPortPriPriority,
                                          INT4 *pi4Ieee8021CnRpPortPriIfIndex)
{
    UNUSED_PARAM (pu4Ieee8021CnRpPortPriComponentId);
    UNUSED_PARAM (pu4Ieee8021CnRpPortPriPriority);
    UNUSED_PARAM (pi4Ieee8021CnRpPortPriIfIndex);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexIeee8021CnRpPortPriTable
 Input       :  The Indices
                Ieee8021CnRpPortPriComponentId
                nextIeee8021CnRpPortPriComponentId
                Ieee8021CnRpPortPriPriority
                nextIeee8021CnRpPortPriPriority
                Ieee8021CnRpPortPriIfIndex
                nextIeee8021CnRpPortPriIfIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexIeee8021CnRpPortPriTable (UINT4 u4Ieee8021CnRpPortPriComponentId,
                                         UINT4
                                         *pu4NextIeee8021CnRpPortPriComponentId,
                                         UINT4 u4Ieee8021CnRpPortPriPriority,
                                         UINT4
                                         *pu4NextIeee8021CnRpPortPriPriority,
                                         INT4 i4Ieee8021CnRpPortPriIfIndex,
                                         INT4
                                         *pi4NextIeee8021CnRpPortPriIfIndex)
{
    UNUSED_PARAM (u4Ieee8021CnRpPortPriComponentId);
    UNUSED_PARAM (pu4NextIeee8021CnRpPortPriComponentId);
    UNUSED_PARAM (u4Ieee8021CnRpPortPriPriority);
    UNUSED_PARAM (pu4NextIeee8021CnRpPortPriPriority);
    UNUSED_PARAM (i4Ieee8021CnRpPortPriIfIndex);
    UNUSED_PARAM (pi4NextIeee8021CnRpPortPriIfIndex);
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetIeee8021CnRpPortPriMaxRps
 Input       :  The Indices
                Ieee8021CnRpPortPriComponentId
                Ieee8021CnRpPortPriPriority
                Ieee8021CnRpPortPriIfIndex

                The Object 
                retValIeee8021CnRpPortPriMaxRps
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021CnRpPortPriMaxRps (UINT4 u4Ieee8021CnRpPortPriComponentId,
                                 UINT4 u4Ieee8021CnRpPortPriPriority,
                                 INT4 i4Ieee8021CnRpPortPriIfIndex,
                                 UINT4 *pi4RetValIeee8021CnRpPortPriMaxRps)
{
    UNUSED_PARAM (u4Ieee8021CnRpPortPriComponentId);
    UNUSED_PARAM (u4Ieee8021CnRpPortPriPriority);
    UNUSED_PARAM (i4Ieee8021CnRpPortPriIfIndex);
    UNUSED_PARAM (pi4RetValIeee8021CnRpPortPriMaxRps);

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetIeee8021CnRpPortPriCreatedRps
 Input       :  The Indices
                Ieee8021CnRpPortPriComponentId
                Ieee8021CnRpPortPriPriority
                Ieee8021CnRpPortPriIfIndex

                The Object 
                retValIeee8021CnRpPortPriCreatedRps
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021CnRpPortPriCreatedRps (UINT4 u4Ieee8021CnRpPortPriComponentId,
                                     UINT4 u4Ieee8021CnRpPortPriPriority,
                                     INT4 i4Ieee8021CnRpPortPriIfIndex,
                                     UINT4
                                     *pi4RetValIeee8021CnRpPortPriCreatedRps)
{

    UNUSED_PARAM (u4Ieee8021CnRpPortPriComponentId);
    UNUSED_PARAM (u4Ieee8021CnRpPortPriPriority);
    UNUSED_PARAM (i4Ieee8021CnRpPortPriIfIndex);
    UNUSED_PARAM (pi4RetValIeee8021CnRpPortPriCreatedRps);
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetIeee8021CnRpPortPriCentiseconds
 Input       :  The Indices
                Ieee8021CnRpPortPriComponentId
                Ieee8021CnRpPortPriPriority
                Ieee8021CnRpPortPriIfIndex

                The Object 
                retValIeee8021CnRpPortPriCentiseconds
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021CnRpPortPriCentiseconds (UINT4 u4Ieee8021CnRpPortPriComponentId,
                                       UINT4 u4Ieee8021CnRpPortPriPriority,
                                       INT4 i4Ieee8021CnRpPortPriIfIndex,
                                       tSNMP_COUNTER64_TYPE *
                                       pu8RetValIeee8021CnRpPortPriCentiseconds)
{
    UNUSED_PARAM (u4Ieee8021CnRpPortPriComponentId);
    UNUSED_PARAM (u4Ieee8021CnRpPortPriPriority);
    UNUSED_PARAM (i4Ieee8021CnRpPortPriIfIndex);
    UNUSED_PARAM (pu8RetValIeee8021CnRpPortPriCentiseconds);
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetIeee8021CnRpPortPriMaxRps
 Input       :  The Indices
                Ieee8021CnRpPortPriComponentId
                Ieee8021CnRpPortPriPriority
                Ieee8021CnRpPortPriIfIndex

                The Object 
                setValIeee8021CnRpPortPriMaxRps
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIeee8021CnRpPortPriMaxRps (UINT4 u4Ieee8021CnRpPortPriComponentId,
                                 UINT4 u4Ieee8021CnRpPortPriPriority,
                                 INT4 i4Ieee8021CnRpPortPriIfIndex,
                                 UINT4 u4SetValIeee8021CnRpPortPriMaxRps)
{

    UNUSED_PARAM (u4Ieee8021CnRpPortPriComponentId);
    UNUSED_PARAM (u4Ieee8021CnRpPortPriPriority);
    UNUSED_PARAM (i4Ieee8021CnRpPortPriIfIndex);
    UNUSED_PARAM (u4SetValIeee8021CnRpPortPriMaxRps);
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2Ieee8021CnRpPortPriMaxRps
 Input       :  The Indices
                Ieee8021CnRpPortPriComponentId
                Ieee8021CnRpPortPriPriority
                Ieee8021CnRpPortPriIfIndex

                The Object 
                testValIeee8021CnRpPortPriMaxRps
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ieee8021CnRpPortPriMaxRps (UINT4 *pu4ErrorCode,
                                    UINT4 u4Ieee8021CnRpPortPriComponentId,
                                    UINT4 u4Ieee8021CnRpPortPriPriority,
                                    INT4 i4Ieee8021CnRpPortPriIfIndex,
                                    UINT4 u4TestValIeee8021CnRpPortPriMaxRps)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (u4Ieee8021CnRpPortPriComponentId);
    UNUSED_PARAM (u4Ieee8021CnRpPortPriPriority);
    UNUSED_PARAM (i4Ieee8021CnRpPortPriIfIndex);
    UNUSED_PARAM (u4TestValIeee8021CnRpPortPriMaxRps);
    return SNMP_SUCCESS;

}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2Ieee8021CnRpPortPriTable
 Input       :  The Indices
                Ieee8021CnRpPortPriComponentId
                Ieee8021CnRpPortPriPriority
                Ieee8021CnRpPortPriIfIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Ieee8021CnRpPortPriTable (UINT4 *pu4ErrorCode,
                                  tSnmpIndexList * pSnmpIndexList,
                                  tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : Ieee8021CnRpGroupTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceIeee8021CnRpGroupTable
 Input       :  The Indices
                Ieee8021CnRpgComponentId
                Ieee8021CnRpgPriority
                Ieee8021CnRpgIfIndex
                Ieee8021CnRpgIdentifier
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1 
     
     
     
     
     
     
     
    nmhValidateIndexInstanceIeee8021CnRpGroupTable
    (UINT4 u4Ieee8021CnRpgComponentId,
     UINT4 u4Ieee8021CnRpgPriority,
     INT4 i4Ieee8021CnRpgIfIndex, UINT4 u4Ieee8021CnRpgIdentifier)
{

    UNUSED_PARAM (u4Ieee8021CnRpgComponentId);
    UNUSED_PARAM (u4Ieee8021CnRpgPriority);
    UNUSED_PARAM (i4Ieee8021CnRpgIfIndex);
    UNUSED_PARAM (u4Ieee8021CnRpgIdentifier);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexIeee8021CnRpGroupTable
 Input       :  The Indices
                Ieee8021CnRpgComponentId
                Ieee8021CnRpgPriority
                Ieee8021CnRpgIfIndex
                Ieee8021CnRpgIdentifier
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexIeee8021CnRpGroupTable (UINT4 *pu4Ieee8021CnRpgComponentId,
                                        UINT4 *pu4Ieee8021CnRpgPriority,
                                        INT4 *pi4Ieee8021CnRpgIfIndex,
                                        UINT4 *pu4Ieee8021CnRpgIdentifier)
{
    UNUSED_PARAM (pu4Ieee8021CnRpgComponentId);
    UNUSED_PARAM (pu4Ieee8021CnRpgPriority);
    UNUSED_PARAM (pi4Ieee8021CnRpgIfIndex);
    UNUSED_PARAM (pu4Ieee8021CnRpgIdentifier);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexIeee8021CnRpGroupTable
 Input       :  The Indices
                Ieee8021CnRpgComponentId
                nextIeee8021CnRpgComponentId
                Ieee8021CnRpgPriority
                nextIeee8021CnRpgPriority
                Ieee8021CnRpgIfIndex
                nextIeee8021CnRpgIfIndex
                Ieee8021CnRpgIdentifier
                nextIeee8021CnRpgIdentifier
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexIeee8021CnRpGroupTable (UINT4 u4Ieee8021CnRpgComponentId,
                                       UINT4 *pu4NextIeee8021CnRpgComponentId,
                                       UINT4 u4Ieee8021CnRpgPriority,
                                       UINT4 *pu4NextIeee8021CnRpgPriority,
                                       INT4 i4Ieee8021CnRpgIfIndex,
                                       INT4 *pi4NextIeee8021CnRpgIfIndex,
                                       UINT4 u4Ieee8021CnRpgIdentifier,
                                       UINT4 *pu4NextIeee8021CnRpgIdentifier)
{

    UNUSED_PARAM (u4Ieee8021CnRpgComponentId);
    UNUSED_PARAM (pu4NextIeee8021CnRpgComponentId);
    UNUSED_PARAM (u4Ieee8021CnRpgPriority);
    UNUSED_PARAM (pu4NextIeee8021CnRpgPriority);
    UNUSED_PARAM (i4Ieee8021CnRpgIfIndex);
    UNUSED_PARAM (pi4NextIeee8021CnRpgIfIndex);
    UNUSED_PARAM (u4Ieee8021CnRpgIdentifier);
    UNUSED_PARAM (pu4NextIeee8021CnRpgIdentifier);
    return SNMP_FAILURE;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetIeee8021CnRpgEnable
 Input       :  The Indices
                Ieee8021CnRpgComponentId
                Ieee8021CnRpgPriority
                Ieee8021CnRpgIfIndex
                Ieee8021CnRpgIdentifier

                The Object 
                retValIeee8021CnRpgEnable
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021CnRpgEnable (UINT4 u4Ieee8021CnRpgComponentId,
                           UINT4 u4Ieee8021CnRpgPriority,
                           INT4 i4Ieee8021CnRpgIfIndex,
                           UINT4 u4Ieee8021CnRpgIdentifier,
                           INT4 *pi4RetValIeee8021CnRpgEnable)
{
    UNUSED_PARAM (u4Ieee8021CnRpgComponentId);
    UNUSED_PARAM (u4Ieee8021CnRpgPriority);
    UNUSED_PARAM (i4Ieee8021CnRpgIfIndex);
    UNUSED_PARAM (u4Ieee8021CnRpgIdentifier);
    UNUSED_PARAM (pi4RetValIeee8021CnRpgEnable);
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetIeee8021CnRpgTimeReset
 Input       :  The Indices
                Ieee8021CnRpgComponentId
                Ieee8021CnRpgPriority
                Ieee8021CnRpgIfIndex
                Ieee8021CnRpgIdentifier

                The Object 
                retValIeee8021CnRpgTimeReset
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021CnRpgTimeReset (UINT4 u4Ieee8021CnRpgComponentId,
                              UINT4 u4Ieee8021CnRpgPriority,
                              INT4 i4Ieee8021CnRpgIfIndex,
                              UINT4 u4Ieee8021CnRpgIdentifier,
                              INT4 *pi4RetValIeee8021CnRpgTimeReset)
{
    UNUSED_PARAM (u4Ieee8021CnRpgComponentId);
    UNUSED_PARAM (u4Ieee8021CnRpgPriority);
    UNUSED_PARAM (i4Ieee8021CnRpgIfIndex);
    UNUSED_PARAM (u4Ieee8021CnRpgIdentifier);
    UNUSED_PARAM (pi4RetValIeee8021CnRpgTimeReset);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetIeee8021CnRpgByteReset
 Input       :  The Indices
                Ieee8021CnRpgComponentId
                Ieee8021CnRpgPriority
                Ieee8021CnRpgIfIndex
                Ieee8021CnRpgIdentifier

                The Object 
                retValIeee8021CnRpgByteReset
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021CnRpgByteReset (UINT4 u4Ieee8021CnRpgComponentId,
                              UINT4 u4Ieee8021CnRpgPriority,
                              INT4 i4Ieee8021CnRpgIfIndex,
                              UINT4 u4Ieee8021CnRpgIdentifier,
                              UINT4 *pi4RetValIeee8021CnRpgByteReset)
{

    UNUSED_PARAM (u4Ieee8021CnRpgComponentId);
    UNUSED_PARAM (u4Ieee8021CnRpgPriority);
    UNUSED_PARAM (i4Ieee8021CnRpgIfIndex);
    UNUSED_PARAM (u4Ieee8021CnRpgIdentifier);
    UNUSED_PARAM (pi4RetValIeee8021CnRpgByteReset);
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetIeee8021CnRpgThreshold
 Input       :  The Indices
                Ieee8021CnRpgComponentId
                Ieee8021CnRpgPriority
                Ieee8021CnRpgIfIndex
                Ieee8021CnRpgIdentifier

                The Object 
                retValIeee8021CnRpgThreshold
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021CnRpgThreshold (UINT4 u4Ieee8021CnRpgComponentId,
                              UINT4 u4Ieee8021CnRpgPriority,
                              INT4 i4Ieee8021CnRpgIfIndex,
                              UINT4 u4Ieee8021CnRpgIdentifier,
                              UINT4 *pi4RetValIeee8021CnRpgThreshold)
{

    UNUSED_PARAM (u4Ieee8021CnRpgComponentId);
    UNUSED_PARAM (u4Ieee8021CnRpgPriority);
    UNUSED_PARAM (i4Ieee8021CnRpgIfIndex);
    UNUSED_PARAM (u4Ieee8021CnRpgIdentifier);
    UNUSED_PARAM (pi4RetValIeee8021CnRpgThreshold);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetIeee8021CnRpgMaxRate
 Input       :  The Indices
                Ieee8021CnRpgComponentId
                Ieee8021CnRpgPriority
                Ieee8021CnRpgIfIndex
                Ieee8021CnRpgIdentifier

                The Object 
                retValIeee8021CnRpgMaxRate
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021CnRpgMaxRate (UINT4 u4Ieee8021CnRpgComponentId,
                            UINT4 u4Ieee8021CnRpgPriority,
                            INT4 i4Ieee8021CnRpgIfIndex,
                            UINT4 u4Ieee8021CnRpgIdentifier,
                            UINT4 *pi4RetValIeee8021CnRpgMaxRate)
{

    UNUSED_PARAM (u4Ieee8021CnRpgComponentId);
    UNUSED_PARAM (u4Ieee8021CnRpgPriority);
    UNUSED_PARAM (i4Ieee8021CnRpgIfIndex);
    UNUSED_PARAM (u4Ieee8021CnRpgIdentifier);
    UNUSED_PARAM (pi4RetValIeee8021CnRpgMaxRate);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetIeee8021CnRpgAiRate
 Input       :  The Indices
                Ieee8021CnRpgComponentId
                Ieee8021CnRpgPriority
                Ieee8021CnRpgIfIndex
                Ieee8021CnRpgIdentifier

                The Object 
                retValIeee8021CnRpgAiRate
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021CnRpgAiRate (UINT4 u4Ieee8021CnRpgComponentId,
                           UINT4 u4Ieee8021CnRpgPriority,
                           INT4 i4Ieee8021CnRpgIfIndex,
                           UINT4 u4Ieee8021CnRpgIdentifier,
                           UINT4 *pi4RetValIeee8021CnRpgAiRate)
{

    UNUSED_PARAM (u4Ieee8021CnRpgComponentId);
    UNUSED_PARAM (u4Ieee8021CnRpgPriority);
    UNUSED_PARAM (i4Ieee8021CnRpgIfIndex);
    UNUSED_PARAM (u4Ieee8021CnRpgIdentifier);
    UNUSED_PARAM (pi4RetValIeee8021CnRpgAiRate);

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetIeee8021CnRpgHaiRate
 Input       :  The Indices
                Ieee8021CnRpgComponentId
                Ieee8021CnRpgPriority
                Ieee8021CnRpgIfIndex
                Ieee8021CnRpgIdentifier

                The Object 
                retValIeee8021CnRpgHaiRate
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021CnRpgHaiRate (UINT4 u4Ieee8021CnRpgComponentId,
                            UINT4 u4Ieee8021CnRpgPriority,
                            INT4 i4Ieee8021CnRpgIfIndex,
                            UINT4 u4Ieee8021CnRpgIdentifier,
                            UINT4 *pi4RetValIeee8021CnRpgHaiRate)
{
    UNUSED_PARAM (u4Ieee8021CnRpgComponentId);
    UNUSED_PARAM (u4Ieee8021CnRpgPriority);
    UNUSED_PARAM (i4Ieee8021CnRpgIfIndex);
    UNUSED_PARAM (u4Ieee8021CnRpgIdentifier);
    UNUSED_PARAM (pi4RetValIeee8021CnRpgHaiRate);

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetIeee8021CnRpgGd
 Input       :  The Indices
                Ieee8021CnRpgComponentId
                Ieee8021CnRpgPriority
                Ieee8021CnRpgIfIndex
                Ieee8021CnRpgIdentifier

                The Object 
                retValIeee8021CnRpgGd
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021CnRpgGd (UINT4 u4Ieee8021CnRpgComponentId,
                       UINT4 u4Ieee8021CnRpgPriority,
                       INT4 i4Ieee8021CnRpgIfIndex,
                       UINT4 u4Ieee8021CnRpgIdentifier,
                       INT4 *pi4RetValIeee8021CnRpgGd)
{
    UNUSED_PARAM (u4Ieee8021CnRpgComponentId);
    UNUSED_PARAM (u4Ieee8021CnRpgPriority);
    UNUSED_PARAM (i4Ieee8021CnRpgIfIndex);
    UNUSED_PARAM (u4Ieee8021CnRpgIdentifier);
    UNUSED_PARAM (pi4RetValIeee8021CnRpgGd);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetIeee8021CnRpgMinDecFac
 Input       :  The Indices
                Ieee8021CnRpgComponentId
                Ieee8021CnRpgPriority
                Ieee8021CnRpgIfIndex
                Ieee8021CnRpgIdentifier

                The Object 
                retValIeee8021CnRpgMinDecFac
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021CnRpgMinDecFac (UINT4 u4Ieee8021CnRpgComponentId,
                              UINT4 u4Ieee8021CnRpgPriority,
                              INT4 i4Ieee8021CnRpgIfIndex,
                              UINT4 u4Ieee8021CnRpgIdentifier,
                              UINT4 *pi4RetValIeee8021CnRpgMinDecFac)
{
    UNUSED_PARAM (u4Ieee8021CnRpgComponentId);
    UNUSED_PARAM (u4Ieee8021CnRpgPriority);
    UNUSED_PARAM (i4Ieee8021CnRpgIfIndex);
    UNUSED_PARAM (u4Ieee8021CnRpgIdentifier);
    UNUSED_PARAM (pi4RetValIeee8021CnRpgMinDecFac);
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetIeee8021CnRpgMinRate
 Input       :  The Indices
                Ieee8021CnRpgComponentId
                Ieee8021CnRpgPriority
                Ieee8021CnRpgIfIndex
                Ieee8021CnRpgIdentifier

                The Object 
                retValIeee8021CnRpgMinRate
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021CnRpgMinRate (UINT4 u4Ieee8021CnRpgComponentId,
                            UINT4 u4Ieee8021CnRpgPriority,
                            INT4 i4Ieee8021CnRpgIfIndex,
                            UINT4 u4Ieee8021CnRpgIdentifier,
                            UINT4 *pi4RetValIeee8021CnRpgMinRate)
{
    UNUSED_PARAM (u4Ieee8021CnRpgComponentId);
    UNUSED_PARAM (u4Ieee8021CnRpgPriority);
    UNUSED_PARAM (i4Ieee8021CnRpgIfIndex);
    UNUSED_PARAM (u4Ieee8021CnRpgIdentifier);
    UNUSED_PARAM (pi4RetValIeee8021CnRpgMinRate);
    return SNMP_FAILURE;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetIeee8021CnRpgEnable
 Input       :  The Indices
                Ieee8021CnRpgComponentId
                Ieee8021CnRpgPriority
                Ieee8021CnRpgIfIndex
                Ieee8021CnRpgIdentifier

                The Object 
                setValIeee8021CnRpgEnable
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIeee8021CnRpgEnable (UINT4 u4Ieee8021CnRpgComponentId,
                           UINT4 u4Ieee8021CnRpgPriority,
                           INT4 i4Ieee8021CnRpgIfIndex,
                           UINT4 u4Ieee8021CnRpgIdentifier,
                           INT4 i4SetValIeee8021CnRpgEnable)
{

    UNUSED_PARAM (u4Ieee8021CnRpgComponentId);
    UNUSED_PARAM (u4Ieee8021CnRpgPriority);
    UNUSED_PARAM (i4Ieee8021CnRpgIfIndex);
    UNUSED_PARAM (u4Ieee8021CnRpgIdentifier);
    UNUSED_PARAM (i4SetValIeee8021CnRpgEnable);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetIeee8021CnRpgTimeReset
 Input       :  The Indices
                Ieee8021CnRpgComponentId
                Ieee8021CnRpgPriority
                Ieee8021CnRpgIfIndex
                Ieee8021CnRpgIdentifier

                The Object 
                setValIeee8021CnRpgTimeReset
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIeee8021CnRpgTimeReset (UINT4 u4Ieee8021CnRpgComponentId,
                              UINT4 u4Ieee8021CnRpgPriority,
                              INT4 i4Ieee8021CnRpgIfIndex,
                              UINT4 u4Ieee8021CnRpgIdentifier,
                              INT4 i4SetValIeee8021CnRpgTimeReset)
{
    UNUSED_PARAM (u4Ieee8021CnRpgComponentId);
    UNUSED_PARAM (u4Ieee8021CnRpgPriority);
    UNUSED_PARAM (i4Ieee8021CnRpgIfIndex);
    UNUSED_PARAM (u4Ieee8021CnRpgIdentifier);
    UNUSED_PARAM (i4SetValIeee8021CnRpgTimeReset);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetIeee8021CnRpgByteReset
 Input       :  The Indices
                Ieee8021CnRpgComponentId
                Ieee8021CnRpgPriority
                Ieee8021CnRpgIfIndex
                Ieee8021CnRpgIdentifier

                The Object 
                setValIeee8021CnRpgByteReset
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIeee8021CnRpgByteReset (UINT4 u4Ieee8021CnRpgComponentId,
                              UINT4 u4Ieee8021CnRpgPriority,
                              INT4 i4Ieee8021CnRpgIfIndex,
                              UINT4 u4Ieee8021CnRpgIdentifier,
                              UINT4 u4SetValIeee8021CnRpgByteReset)
{

    UNUSED_PARAM (u4Ieee8021CnRpgComponentId);
    UNUSED_PARAM (u4Ieee8021CnRpgPriority);
    UNUSED_PARAM (i4Ieee8021CnRpgIfIndex);
    UNUSED_PARAM (u4Ieee8021CnRpgIdentifier);
    UNUSED_PARAM (u4SetValIeee8021CnRpgByteReset);
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhSetIeee8021CnRpgThreshold
 Input       :  The Indices
                Ieee8021CnRpgComponentId
                Ieee8021CnRpgPriority
                Ieee8021CnRpgIfIndex
                Ieee8021CnRpgIdentifier

                The Object 
                setValIeee8021CnRpgThreshold
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIeee8021CnRpgThreshold (UINT4 u4Ieee8021CnRpgComponentId,
                              UINT4 u4Ieee8021CnRpgPriority,
                              INT4 i4Ieee8021CnRpgIfIndex,
                              UINT4 u4Ieee8021CnRpgIdentifier,
                              UINT4 u4SetValIeee8021CnRpgThreshold)
{

    UNUSED_PARAM (u4Ieee8021CnRpgComponentId);
    UNUSED_PARAM (u4Ieee8021CnRpgPriority);
    UNUSED_PARAM (i4Ieee8021CnRpgIfIndex);
    UNUSED_PARAM (u4Ieee8021CnRpgIdentifier);
    UNUSED_PARAM (u4SetValIeee8021CnRpgThreshold);
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhSetIeee8021CnRpgMaxRate
 Input       :  The Indices
                Ieee8021CnRpgComponentId
                Ieee8021CnRpgPriority
                Ieee8021CnRpgIfIndex
                Ieee8021CnRpgIdentifier

                The Object 
                setValIeee8021CnRpgMaxRate
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIeee8021CnRpgMaxRate (UINT4 u4Ieee8021CnRpgComponentId,
                            UINT4 u4Ieee8021CnRpgPriority,
                            INT4 i4Ieee8021CnRpgIfIndex,
                            UINT4 u4Ieee8021CnRpgIdentifier,
                            UINT4 u4SetValIeee8021CnRpgMaxRate)
{
    UNUSED_PARAM (u4Ieee8021CnRpgComponentId);
    UNUSED_PARAM (u4Ieee8021CnRpgPriority);
    UNUSED_PARAM (i4Ieee8021CnRpgIfIndex);
    UNUSED_PARAM (u4Ieee8021CnRpgIdentifier);
    UNUSED_PARAM (u4SetValIeee8021CnRpgMaxRate);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetIeee8021CnRpgAiRate
 Input       :  The Indices
                Ieee8021CnRpgComponentId
                Ieee8021CnRpgPriority
                Ieee8021CnRpgIfIndex
                Ieee8021CnRpgIdentifier

                The Object 
                setValIeee8021CnRpgAiRate
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIeee8021CnRpgAiRate (UINT4 u4Ieee8021CnRpgComponentId,
                           UINT4 u4Ieee8021CnRpgPriority,
                           INT4 i4Ieee8021CnRpgIfIndex,
                           UINT4 u4Ieee8021CnRpgIdentifier,
                           UINT4 u4SetValIeee8021CnRpgAiRate)
{
    UNUSED_PARAM (u4Ieee8021CnRpgComponentId);
    UNUSED_PARAM (u4Ieee8021CnRpgPriority);
    UNUSED_PARAM (i4Ieee8021CnRpgIfIndex);
    UNUSED_PARAM (u4Ieee8021CnRpgIdentifier);
    UNUSED_PARAM (u4SetValIeee8021CnRpgAiRate);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetIeee8021CnRpgHaiRate
 Input       :  The Indices
                Ieee8021CnRpgComponentId
                Ieee8021CnRpgPriority
                Ieee8021CnRpgIfIndex
                Ieee8021CnRpgIdentifier

                The Object 
                setValIeee8021CnRpgHaiRate
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIeee8021CnRpgHaiRate (UINT4 u4Ieee8021CnRpgComponentId,
                            UINT4 u4Ieee8021CnRpgPriority,
                            INT4 i4Ieee8021CnRpgIfIndex,
                            UINT4 u4Ieee8021CnRpgIdentifier,
                            UINT4 u4SetValIeee8021CnRpgHaiRate)
{

    UNUSED_PARAM (u4Ieee8021CnRpgComponentId);
    UNUSED_PARAM (u4Ieee8021CnRpgPriority);
    UNUSED_PARAM (i4Ieee8021CnRpgIfIndex);
    UNUSED_PARAM (u4Ieee8021CnRpgIdentifier);
    UNUSED_PARAM (u4SetValIeee8021CnRpgHaiRate);
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhSetIeee8021CnRpgGd
 Input       :  The Indices
                Ieee8021CnRpgComponentId
                Ieee8021CnRpgPriority
                Ieee8021CnRpgIfIndex
                Ieee8021CnRpgIdentifier

                The Object 
                setValIeee8021CnRpgGd
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIeee8021CnRpgGd (UINT4 u4Ieee8021CnRpgComponentId,
                       UINT4 u4Ieee8021CnRpgPriority,
                       INT4 i4Ieee8021CnRpgIfIndex,
                       UINT4 u4Ieee8021CnRpgIdentifier,
                       INT4 i4SetValIeee8021CnRpgGd)
{

    UNUSED_PARAM (u4Ieee8021CnRpgComponentId);
    UNUSED_PARAM (u4Ieee8021CnRpgPriority);
    UNUSED_PARAM (i4Ieee8021CnRpgIfIndex);
    UNUSED_PARAM (u4Ieee8021CnRpgIdentifier);
    UNUSED_PARAM (i4SetValIeee8021CnRpgGd);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetIeee8021CnRpgMinDecFac
 Input       :  The Indices
                Ieee8021CnRpgComponentId
                Ieee8021CnRpgPriority
                Ieee8021CnRpgIfIndex
                Ieee8021CnRpgIdentifier

                The Object 
                setValIeee8021CnRpgMinDecFac
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIeee8021CnRpgMinDecFac (UINT4 u4Ieee8021CnRpgComponentId,
                              UINT4 u4Ieee8021CnRpgPriority,
                              INT4 i4Ieee8021CnRpgIfIndex,
                              UINT4 u4Ieee8021CnRpgIdentifier,
                              UINT4 u4SetValIeee8021CnRpgMinDecFac)
{
    UNUSED_PARAM (u4Ieee8021CnRpgComponentId);
    UNUSED_PARAM (u4Ieee8021CnRpgPriority);
    UNUSED_PARAM (i4Ieee8021CnRpgIfIndex);
    UNUSED_PARAM (u4Ieee8021CnRpgIdentifier);
    UNUSED_PARAM (u4SetValIeee8021CnRpgMinDecFac);

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetIeee8021CnRpgMinRate
 Input       :  The Indices
                Ieee8021CnRpgComponentId
                Ieee8021CnRpgPriority
                Ieee8021CnRpgIfIndex
                Ieee8021CnRpgIdentifier

                The Object 
                setValIeee8021CnRpgMinRate
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIeee8021CnRpgMinRate (UINT4 u4Ieee8021CnRpgComponentId,
                            UINT4 u4Ieee8021CnRpgPriority,
                            INT4 i4Ieee8021CnRpgIfIndex,
                            UINT4 u4Ieee8021CnRpgIdentifier,
                            UINT4 u4SetValIeee8021CnRpgMinRate)
{

    UNUSED_PARAM (u4Ieee8021CnRpgComponentId);
    UNUSED_PARAM (u4Ieee8021CnRpgPriority);
    UNUSED_PARAM (i4Ieee8021CnRpgIfIndex);
    UNUSED_PARAM (u4Ieee8021CnRpgIdentifier);
    UNUSED_PARAM (u4SetValIeee8021CnRpgMinRate);
    return SNMP_FAILURE;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2Ieee8021CnRpgEnable
 Input       :  The Indices
                Ieee8021CnRpgComponentId
                Ieee8021CnRpgPriority
                Ieee8021CnRpgIfIndex
                Ieee8021CnRpgIdentifier

                The Object 
                testValIeee8021CnRpgEnable
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ieee8021CnRpgEnable (UINT4 *pu4ErrorCode,
                              UINT4 u4Ieee8021CnRpgComponentId,
                              UINT4 u4Ieee8021CnRpgPriority,
                              INT4 i4Ieee8021CnRpgIfIndex,
                              UINT4 u4Ieee8021CnRpgIdentifier,
                              INT4 i4TestValIeee8021CnRpgEnable)
{

    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (u4Ieee8021CnRpgComponentId);
    UNUSED_PARAM (u4Ieee8021CnRpgPriority);
    UNUSED_PARAM (i4Ieee8021CnRpgIfIndex);
    UNUSED_PARAM (u4Ieee8021CnRpgIdentifier);
    UNUSED_PARAM (i4TestValIeee8021CnRpgEnable);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2Ieee8021CnRpgTimeReset
 Input       :  The Indices
                Ieee8021CnRpgComponentId
                Ieee8021CnRpgPriority
                Ieee8021CnRpgIfIndex
                Ieee8021CnRpgIdentifier

                The Object 
                testValIeee8021CnRpgTimeReset
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ieee8021CnRpgTimeReset (UINT4 *pu4ErrorCode,
                                 UINT4 u4Ieee8021CnRpgComponentId,
                                 UINT4 u4Ieee8021CnRpgPriority,
                                 INT4 i4Ieee8021CnRpgIfIndex,
                                 UINT4 u4Ieee8021CnRpgIdentifier,
                                 INT4 i4TestValIeee8021CnRpgTimeReset)
{

    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (u4Ieee8021CnRpgComponentId);
    UNUSED_PARAM (u4Ieee8021CnRpgPriority);
    UNUSED_PARAM (i4Ieee8021CnRpgIfIndex);
    UNUSED_PARAM (u4Ieee8021CnRpgIdentifier);
    UNUSED_PARAM (i4TestValIeee8021CnRpgTimeReset);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2Ieee8021CnRpgByteReset
 Input       :  The Indices
                Ieee8021CnRpgComponentId
                Ieee8021CnRpgPriority
                Ieee8021CnRpgIfIndex
                Ieee8021CnRpgIdentifier

                The Object 
                testValIeee8021CnRpgByteReset
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ieee8021CnRpgByteReset (UINT4 *pu4ErrorCode,
                                 UINT4 u4Ieee8021CnRpgComponentId,
                                 UINT4 u4Ieee8021CnRpgPriority,
                                 INT4 i4Ieee8021CnRpgIfIndex,
                                 UINT4 u4Ieee8021CnRpgIdentifier,
                                 UINT4 u4TestValIeee8021CnRpgByteReset)
{

    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (u4Ieee8021CnRpgComponentId);
    UNUSED_PARAM (u4Ieee8021CnRpgPriority);
    UNUSED_PARAM (i4Ieee8021CnRpgIfIndex);
    UNUSED_PARAM (u4Ieee8021CnRpgIdentifier);
    UNUSED_PARAM (u4TestValIeee8021CnRpgByteReset);
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhTestv2Ieee8021CnRpgThreshold
 Input       :  The Indices
                Ieee8021CnRpgComponentId
                Ieee8021CnRpgPriority
                Ieee8021CnRpgIfIndex
                Ieee8021CnRpgIdentifier

                The Object 
                testValIeee8021CnRpgThreshold
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ieee8021CnRpgThreshold (UINT4 *pu4ErrorCode,
                                 UINT4 u4Ieee8021CnRpgComponentId,
                                 UINT4 u4Ieee8021CnRpgPriority,
                                 INT4 i4Ieee8021CnRpgIfIndex,
                                 UINT4 u4Ieee8021CnRpgIdentifier,
                                 UINT4 u4TestValIeee8021CnRpgThreshold)
{

    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (u4Ieee8021CnRpgComponentId);
    UNUSED_PARAM (u4Ieee8021CnRpgPriority);
    UNUSED_PARAM (i4Ieee8021CnRpgIfIndex);
    UNUSED_PARAM (u4Ieee8021CnRpgIdentifier);
    UNUSED_PARAM (u4TestValIeee8021CnRpgThreshold);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2Ieee8021CnRpgMaxRate
 Input       :  The Indices
                Ieee8021CnRpgComponentId
                Ieee8021CnRpgPriority
                Ieee8021CnRpgIfIndex
                Ieee8021CnRpgIdentifier

                The Object 
                testValIeee8021CnRpgMaxRate
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ieee8021CnRpgMaxRate (UINT4 *pu4ErrorCode,
                               UINT4 u4Ieee8021CnRpgComponentId,
                               UINT4 u4Ieee8021CnRpgPriority,
                               INT4 i4Ieee8021CnRpgIfIndex,
                               UINT4 u4Ieee8021CnRpgIdentifier,
                               UINT4 u4TestValIeee8021CnRpgMaxRate)
{

    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (u4Ieee8021CnRpgComponentId);
    UNUSED_PARAM (u4Ieee8021CnRpgPriority);
    UNUSED_PARAM (i4Ieee8021CnRpgIfIndex);
    UNUSED_PARAM (u4Ieee8021CnRpgIdentifier);
    UNUSED_PARAM (u4TestValIeee8021CnRpgMaxRate);
    return SNMP_FAILURE;
}

/****************************************************************************
Function    :  nmhTestv2Ieee8021CnRpgAiRate
 Input       :  The Indices
                Ieee8021CnRpgComponentId
                Ieee8021CnRpgPriority
                Ieee8021CnRpgIfIndex
                Ieee8021CnRpgIdentifier

                The Object 
                testValIeee8021CnRpgAiRate
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ieee8021CnRpgAiRate (UINT4 *pu4ErrorCode,
                              UINT4 u4Ieee8021CnRpgComponentId,
                              UINT4 u4Ieee8021CnRpgPriority,
                              INT4 i4Ieee8021CnRpgIfIndex,
                              UINT4 u4Ieee8021CnRpgIdentifier,
                              UINT4 u4TestValIeee8021CnRpgAiRate)
{

    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (u4Ieee8021CnRpgComponentId);
    UNUSED_PARAM (u4Ieee8021CnRpgPriority);
    UNUSED_PARAM (i4Ieee8021CnRpgIfIndex);
    UNUSED_PARAM (u4Ieee8021CnRpgIdentifier);
    UNUSED_PARAM (u4TestValIeee8021CnRpgAiRate);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2Ieee8021CnRpgHaiRate
 Input       :  The Indices
                Ieee8021CnRpgComponentId
                Ieee8021CnRpgPriority
                Ieee8021CnRpgIfIndex
                Ieee8021CnRpgIdentifier

                The Object 
                testValIeee8021CnRpgHaiRate
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ieee8021CnRpgHaiRate (UINT4 *pu4ErrorCode,
                               UINT4 u4Ieee8021CnRpgComponentId,
                               UINT4 u4Ieee8021CnRpgPriority,
                               INT4 i4Ieee8021CnRpgIfIndex,
                               UINT4 u4Ieee8021CnRpgIdentifier,
                               UINT4 u4TestValIeee8021CnRpgHaiRate)
{

    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (u4Ieee8021CnRpgComponentId);
    UNUSED_PARAM (u4Ieee8021CnRpgPriority);
    UNUSED_PARAM (i4Ieee8021CnRpgIfIndex);
    UNUSED_PARAM (u4Ieee8021CnRpgIdentifier);
    UNUSED_PARAM (u4TestValIeee8021CnRpgHaiRate);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2Ieee8021CnRpgGd
 Input       :  The Indices
                Ieee8021CnRpgComponentId
                Ieee8021CnRpgPriority
                Ieee8021CnRpgIfIndex
                Ieee8021CnRpgIdentifier

                The Object 
                testValIeee8021CnRpgGd
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ieee8021CnRpgGd (UINT4 *pu4ErrorCode, UINT4 u4Ieee8021CnRpgComponentId,
                          UINT4 u4Ieee8021CnRpgPriority,
                          INT4 i4Ieee8021CnRpgIfIndex,
                          UINT4 u4Ieee8021CnRpgIdentifier,
                          INT4 i4TestValIeee8021CnRpgGd)
{

    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (u4Ieee8021CnRpgComponentId);
    UNUSED_PARAM (u4Ieee8021CnRpgPriority);
    UNUSED_PARAM (i4Ieee8021CnRpgIfIndex);
    UNUSED_PARAM (u4Ieee8021CnRpgIdentifier);
    UNUSED_PARAM (i4TestValIeee8021CnRpgGd);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2Ieee8021CnRpgMinDecFac
 Input       :  The Indices
                Ieee8021CnRpgComponentId
                Ieee8021CnRpgPriority
                Ieee8021CnRpgIfIndex
                Ieee8021CnRpgIdentifier

                The Object 
                testValIeee8021CnRpgMinDecFac
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ieee8021CnRpgMinDecFac (UINT4 *pu4ErrorCode,
                                 UINT4 u4Ieee8021CnRpgComponentId,
                                 UINT4 u4Ieee8021CnRpgPriority,
                                 INT4 i4Ieee8021CnRpgIfIndex,
                                 UINT4 u4Ieee8021CnRpgIdentifier,
                                 UINT4 u4TestValIeee8021CnRpgMinDecFac)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (u4Ieee8021CnRpgComponentId);
    UNUSED_PARAM (u4Ieee8021CnRpgPriority);
    UNUSED_PARAM (i4Ieee8021CnRpgIfIndex);
    UNUSED_PARAM (u4Ieee8021CnRpgIdentifier);
    UNUSED_PARAM (u4TestValIeee8021CnRpgMinDecFac);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2Ieee8021CnRpgMinRate
 Input       :  The Indices
                Ieee8021CnRpgComponentId
                Ieee8021CnRpgPriority
                Ieee8021CnRpgIfIndex
                Ieee8021CnRpgIdentifier

                The Object 
                testValIeee8021CnRpgMinRate
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ieee8021CnRpgMinRate (UINT4 *pu4ErrorCode,
                               UINT4 u4Ieee8021CnRpgComponentId,
                               UINT4 u4Ieee8021CnRpgPriority,
                               INT4 i4Ieee8021CnRpgIfIndex,
                               UINT4 u4Ieee8021CnRpgIdentifier,
                               UINT4 u4TestValIeee8021CnRpgMinRate)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (u4Ieee8021CnRpgComponentId);
    UNUSED_PARAM (u4Ieee8021CnRpgPriority);
    UNUSED_PARAM (i4Ieee8021CnRpgIfIndex);
    UNUSED_PARAM (u4Ieee8021CnRpgIdentifier);
    UNUSED_PARAM (u4TestValIeee8021CnRpgMinRate);
    return SNMP_FAILURE;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2Ieee8021CnRpGroupTable
 Input       :  The Indices
                Ieee8021CnRpgComponentId
                Ieee8021CnRpgPriority
                Ieee8021CnRpgIfIndex
                Ieee8021CnRpgIdentifier
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Ieee8021CnRpGroupTable (UINT4 *pu4ErrorCode,
                                tSnmpIndexList * pSnmpIndexList,
                                tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* End of file */
