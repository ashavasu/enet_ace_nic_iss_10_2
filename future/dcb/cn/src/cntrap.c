/*****************************************************************************
 * Copyright (C) 2010 Aricent Inc . All Rights Reserved
 *
 * $Id: cntrap.c,v 1.8 2015/01/30 10:35:14 siva Exp $
 *
 * Description: This file contains the functions to send the trap message.
 ******************************************************************************/
#include "cninc.h"
#include "snmputil.h"

#ifdef SNMP_3_WANTED
#include "fscn.h"
#include "stdcn.h"
#endif

CHR1               *gac1CnSysLogMsg[CN_MAX_SYSLOG_MSG] = {
    NULL,
    "Added Errored Port Entry",
    "Deleted Errored Port Entry",
    "Sent Congestion Notification Message"
};

#ifdef SNMP_3_WANTED
/******************************************************************************
* Function :   CnMakeObjIdFrmString
*
* Description: This Function retuns the OID  of the given string for the 
*              proprietary MIB.
*
* Input    :   pi1TextStr - pointer to the string.
*              pTableName - TableName has to be fetched.
*
* Output   :   None.
*
* Returns  :   pOidPtr or NULL
*******************************************************************************/

tSNMP_OID_TYPE     *
CnMakeObjIdFrmString (INT1 *pi1TextStr, UINT1 *pu1TableName)
{
    tSNMP_OID_TYPE     *pOidPtr = NULL;
    INT1               *pi1DotPtr = NULL;
    UINT2               u2Index = CN_ZERO;
    INT1                ai1TempBuffer[CN_OBJECT_NAME_LEN + CN_ONE] =
        { CN_ZERO };
    UINT1              *pu1TempPtr = NULL;
    struct MIB_OID     *pTableName = NULL;
    pTableName = (struct MIB_OID *) (VOID *) pu1TableName;

    pi1DotPtr = pi1TextStr + STRLEN ((INT1 *) pi1TextStr);
    pu1TempPtr = (UINT1 *) pi1TextStr;
    UINT2               u2Len = 0;

    for (u2Index = CN_ZERO;
         ((pu1TempPtr < (UINT1 *) pi1DotPtr) && (u2Index < CN_OBJECT_NAME_LEN));
         u2Index++)
    {
        ai1TempBuffer[u2Index] = *pu1TempPtr++;
    }
    ai1TempBuffer[u2Index] = '\0';
    for (u2Index = CN_ZERO; pTableName[u2Index].pName != NULL; u2Index++)
    {
        if ((STRCMP
             (pTableName[u2Index].pName,
              (INT1 *) ai1TempBuffer) == CN_ZERO)
            && (STRLEN ((INT1 *) ai1TempBuffer) ==
                STRLEN (pTableName[u2Index].pName)))
        {
            u2Len =
                (UINT2) (STRLEN (pTableName[u2Index].pNumber) <
                        (sizeof(ai1TempBuffer)-1) ?
                        (STRLEN (pTableName[u2Index].pNumber)) : (sizeof(ai1TempBuffer)-1));

            STRNCPY ((INT1 *) ai1TempBuffer, pTableName[u2Index].pNumber,
                     u2Len);
            ai1TempBuffer[u2Len] = '\0';
            break;
        }
    }
    if (pTableName[u2Index].pName == NULL)
    {
        return (NULL);
    }

    /* Now we've got something with numbers instead of an alpha header */

    /* count the dots.  num +1 is the number of SID's */
    pOidPtr = SNMP_AGT_GetOidFromString ((INT1 *) ai1TempBuffer);
    return (pOidPtr);
}
#endif /* SNMP_3_WANTED */
/******************************************************************************
* Function Name      : CnSendNotification 
*
* Description        : This routine will send the snmp traps 
*
* Input(s)           : u1TrapId - Trap ID
*                      pNotifyInfo - Void Pointer to tCnMisConfigTrap
*
* Output(s)          : None
*
* Return Value(s)    : None
*****************************************************************************/
VOID
CnSendNotification (tCnTrapMsg * pNotifyInfo, UINT1 u1TrapId)
{

#ifdef SNMP_3_WANTED
    tSNMP_VAR_BIND     *pVbList = NULL;
    tSNMP_VAR_BIND     *pStartVb = NULL;
    tSNMP_OID_TYPE     *pEnterpriseOid = NULL;
    tSNMP_OID_TYPE     *pSnmpTrapOid = NULL;
    tSNMP_OCTET_STRING_TYPE *pValueStr = NULL;
    tSNMP_COUNTER64_TYPE u8CounterVal = { CN_ZERO, CN_ZERO };
    UINT4               au4SnmpTrapOid[] = CN_SNMP_TRAP_OID;
    UINT1               au1Buf[CN_OBJECT_NAME_LEN] = { CN_ZERO };
    INT4                i4ErrorEntry = CN_ZERO;
    tSNMP_OID_TYPE     *pOid = NULL;
    tSNMP_COUNTER64_TYPE SnmpCnt64Type;

    SnmpCnt64Type.msn = CN_ZERO;
    SnmpCnt64Type.lsn = CN_ZERO;

    /* Checking Trap is enabled or not */

    if ((gCnMasterTblInfo.u4GlobalTrapStatus == CN_ZERO) ||
        ((gCnMasterTblInfo.u4GlobalTrapStatus == CN_CNM_TRAP_ENABLE) &&
         (u1TrapId != CN_SEND_CN_MESSAGE)) ||
        ((u1TrapId == CN_SEND_CN_MESSAGE) &&
         (gCnMasterTblInfo.u4GlobalTrapStatus ==
          CN_ERROR_PORT_ENTRY_TRAP_ENABLE)))
    {
        return;
    }

    pSnmpTrapOid = alloc_oid (CN_SNMPV2_TRAP_OID_LEN);
    if (pSnmpTrapOid == NULL)
    {
        CN_TRC (pNotifyInfo->u4ContextId, CN_ALL_FAILURE_TRC,
                "CnSendNotification: OID Memory Allocation Failed\r\n");
        return;
    }
    MEMCPY (pSnmpTrapOid->pu4_OidList, au4SnmpTrapOid,
            CN_SNMPV2_TRAP_OID_LEN * sizeof (UINT4));

    SPRINTF ((char *) au1Buf, "fsCnTraps");
    pEnterpriseOid =
        CnMakeObjIdFrmString ((INT1 *) au1Buf,
                              (UINT1 *) fs_cn_orig_mib_oid_table);
    if (pEnterpriseOid == NULL)
    {
        SNMP_FreeOid (pSnmpTrapOid);
        CN_TRC (pNotifyInfo->u4ContextId, CN_ALL_FAILURE_TRC,
                "CnSendNotification: OID generation from String Failed\r\n");
        return;
    }
    if (u1TrapId == CN_SEND_CN_MESSAGE)
    {
        pEnterpriseOid->u4_Length = pEnterpriseOid->u4_Length - CN_ONE;
        pEnterpriseOid->pu4_OidList[pEnterpriseOid->u4_Length++] = CN_CNM_TRAP;
        pVbList = (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind
            (pSnmpTrapOid, SNMP_DATA_TYPE_OBJECT_ID, CN_ZERO, CN_ZERO, NULL,
             pEnterpriseOid, u8CounterVal);
        if (pVbList == NULL)
        {
            SNMP_FreeOid (pEnterpriseOid);
            SNMP_FreeOid (pSnmpTrapOid);
            CN_TRC (pNotifyInfo->u4ContextId, CN_ALL_FAILURE_TRC,
                    "CnSendNotification: Variable Binding Failed\r\n");
            return;
        }

        pStartVb = pVbList;

        MEMSET (au1Buf, CN_ZERO, CN_OBJECT_NAME_LEN);
        SPRINTF ((char *) au1Buf, "ieee8021CnCpIdentifier");

        pOid =
            CnMakeObjIdFrmString ((INT1 *) au1Buf,
                                  (UINT1 *) std_cn_orig_mib_oid_table);
        if (pOid == NULL)
        {
            SNMP_free_snmp_vb_list (pStartVb);
            CN_TRC (pNotifyInfo->u4ContextId, CN_ALL_FAILURE_TRC,
                    "CnSendNotification: OID Not Found\r\n");
            return;
        }

        pOid->pu4_OidList[pOid->u4_Length] = pNotifyInfo->u4ContextId;
        pOid->u4_Length++;
        pOid->pu4_OidList[pOid->u4_Length] = pNotifyInfo->u4PortId;
        pOid->u4_Length++;
        pOid->pu4_OidList[pOid->u4_Length] = pNotifyInfo->u1CNPV;
        pOid->u4_Length++;

        /* Form the octet string */
        if ((pValueStr =
             SNMP_AGT_FormOctetString
             (pNotifyInfo->unTrapMsg.CnmParams.au1CpIdentifier, CN_CPID_LEN))
            == NULL)
        {
            SNMP_FreeOid (pOid);
            SNMP_free_snmp_vb_list (pStartVb);
            CN_TRC (pNotifyInfo->u4ContextId, CN_ALL_FAILURE_TRC,
                    "CnSendNotification: " "Form Octet String Failed\r\n");
            return;
        }

        pVbList->pNextVarBind = SNMP_AGT_FormVarBind (pOid,
                                                      SNMP_DATA_TYPE_OCTET_PRIM,
                                                      CN_ZERO, CN_ZERO,
                                                      pValueStr, NULL,
                                                      SnmpCnt64Type);
        if (pVbList->pNextVarBind == NULL)
        {
            SNMP_FreeOid (pOid);
            SNMP_AGT_FreeOctetString (pValueStr);
            SNMP_free_snmp_vb_list (pStartVb);
            return;
        }

        pVbList = pVbList->pNextVarBind;
        MEMSET (au1Buf, CN_ZERO, CN_OBJECT_NAME_LEN);
        SPRINTF ((char *) au1Buf, "fsCnCnmQOffset");

        pOid =
            CnMakeObjIdFrmString ((INT1 *) au1Buf,
                                  (UINT1 *) fs_cn_orig_mib_oid_table);
        if (pOid == NULL)
        {
            SNMP_free_snmp_vb_list (pStartVb);
            CN_TRC (pNotifyInfo->u4ContextId, CN_ALL_FAILURE_TRC,
                    "CnSendNotification: OID Not Found\r\n");
            return;
        }
        pVbList->pNextVarBind = SNMP_AGT_FormVarBind (pOid,
                                                      SNMP_DATA_TYPE_INTEGER32,
                                                      CN_ZERO,
                                                      pNotifyInfo->unTrapMsg.
                                                      CnmParams.i4CnmQOffset,
                                                      NULL, NULL,
                                                      SnmpCnt64Type);
        if (pVbList->pNextVarBind == NULL)
        {
            SNMP_free_snmp_vb_list (pStartVb);
            SNMP_FreeOid (pOid);
            CN_TRC (pNotifyInfo->u4ContextId, CN_ALL_FAILURE_TRC,
                    "CnSendNotification: Variable Binding Failed\r\n");
            return;
        }
        pVbList = pVbList->pNextVarBind;
        MEMSET (au1Buf, CN_ZERO, CN_OBJECT_NAME_LEN);
        SPRINTF ((char *) au1Buf, "fsCnCnmQDelta");
        pOid =
            CnMakeObjIdFrmString ((INT1 *) au1Buf,
                                  (UINT1 *) fs_cn_orig_mib_oid_table);
        if (pOid == NULL)
        {
            SNMP_free_snmp_vb_list (pStartVb);
            CN_TRC (pNotifyInfo->u4ContextId, CN_ALL_FAILURE_TRC,
                    "CnSendNotification: OID Not Found\r\n");
            return;
        }
        pVbList->pNextVarBind = SNMP_AGT_FormVarBind (pOid,
                                                      SNMP_DATA_TYPE_INTEGER32,
                                                      CN_ZERO,
                                                      pNotifyInfo->unTrapMsg.
                                                      CnmParams.i4CnmQDelta,
                                                      NULL, NULL,
                                                      SnmpCnt64Type);
        if (pVbList->pNextVarBind == NULL)
        {
            SNMP_free_snmp_vb_list (pStartVb);
            SNMP_FreeOid (pOid);
            CN_TRC (pNotifyInfo->u4ContextId, CN_ALL_FAILURE_TRC,
                    "CnSendNotification: Variable Binding Failed\r\n");
            return;
        }

        pVbList = pVbList->pNextVarBind;
        pVbList->pNextVarBind = NULL;
    }
    else
    {
        pEnterpriseOid->u4_Length = pEnterpriseOid->u4_Length - CN_ONE;
        pEnterpriseOid->pu4_OidList[pEnterpriseOid->u4_Length++]
            = CN_ERROR_PORT_ENTRY_TRAP;
        pVbList = (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind
            (pSnmpTrapOid, SNMP_DATA_TYPE_OBJECT_ID, CN_ZERO, CN_ZERO, NULL,
             pEnterpriseOid, u8CounterVal);
        if (pVbList == NULL)
        {
            SNMP_FreeOid (pEnterpriseOid);
            SNMP_FreeOid (pSnmpTrapOid);
            CN_TRC (pNotifyInfo->u4ContextId, CN_ALL_FAILURE_TRC,
                    "CnSendNotification: Variable Binding Failed\r\n");
            return;
        }

        pStartVb = pVbList;
        MEMSET (au1Buf, CN_ZERO, CN_OBJECT_NAME_LEN);
        SPRINTF ((char *) au1Buf, "fsCnXPortPriErrorEntry");
        pOid =
            CnMakeObjIdFrmString ((INT1 *) au1Buf,
                                  (UINT1 *) fs_cn_orig_mib_oid_table);
        if (pOid == NULL)
        {
            SNMP_free_snmp_vb_list (pStartVb);
            CN_TRC (pNotifyInfo->u4ContextId, CN_ALL_FAILURE_TRC,
                    "CnSendNotification: OID Not Found\r\n");
            return;
        }
        pOid->pu4_OidList[pOid->u4_Length] = pNotifyInfo->u4ContextId;
        pOid->u4_Length++;
        pOid->pu4_OidList[pOid->u4_Length] = pNotifyInfo->u4PortId;
        pOid->u4_Length++;
        pOid->pu4_OidList[pOid->u4_Length] = pNotifyInfo->u1CNPV;
        pOid->u4_Length++;

        if (CN_ADDED_ERROR_PORT_ENTRY == u1TrapId)
        {
            i4ErrorEntry = CN_ONE;
        }
        else
        {
            i4ErrorEntry = CN_ZERO;
        }
        pVbList->pNextVarBind = SNMP_AGT_FormVarBind (pOid,
                                                      SNMP_DATA_TYPE_INTEGER32,
                                                      CN_ZERO, i4ErrorEntry,
                                                      NULL, NULL,
                                                      SnmpCnt64Type);
        if (pVbList->pNextVarBind == NULL)
        {
            SNMP_free_snmp_vb_list (pStartVb);
            SNMP_FreeOid (pOid);
            CN_TRC (pNotifyInfo->u4ContextId, CN_ALL_FAILURE_TRC,
                    "CnSendNotification: Variable Binding Failed\r\n");
            return;
        }
        pVbList = pVbList->pNextVarBind;
        pVbList->pNextVarBind = NULL;

    }
    if ((u1TrapId <= CN_ZERO) || (u1TrapId >= CN_MAX_SYSLOG_MSG))
    {
        SNMP_free_snmp_vb_list (pStartVb);
        return;
    }
    CnPortNotifyFaults (pStartVb, (UINT1 *) gac1CnSysLogMsg[u1TrapId],
                        FM_NOTIFY_MOD_ID_CN);
#else
    UNUSED_PARAM (u1TrapId);
    UNUSED_PARAM (pNotifyInfo);

#endif
}

/* End of file */
