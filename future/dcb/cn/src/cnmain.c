/********************************************************************
 * Copyright (C) 2010 Aricent Inc . All Rights Reserved
 *
 * $Id: cnmain.c,v 1.5 2010/10/29 18:02:03 prabuc Exp $
 *
 * Description: This file contains CN task main loop and
 *              initialization routines.
 *********************************************************************/

#include "cninc.h"
#include "cnglob.h"
/* Proto types of the functions private to this file only */

PRIVATE INT4 CnMainTaskInit PROTO ((VOID));
PRIVATE VOID CnMainHandleInitFailure PROTO ((VOID));

/****************************************************************************
*
*    FUNCTION NAME    : CnMainTask
*
*    DESCRIPTION      : Entry point function of CN task.
*
*    INPUT            : pArg - Pointer to the arguments
*
*    OUTPUT           : None
*
*    RETURNS          : None
*
****************************************************************************/
PUBLIC VOID
CnMainTask (INT1 *pArg)
{
    UINT4               u4Events = CN_ZERO;

    UNUSED_PARAM (pArg);

    if (CnMainTaskInit () == OSIX_FAILURE)
    {
        CnMainHandleInitFailure ();

        /* Indicate the status of initialization to main routine */
        lrInitComplete (OSIX_FAILURE);
        return;
    }
    /* Register the protocol MIBS with SNMP */
    RegisterFSCN ();
    RegisterSTDCN ();
    /* Indicate the status of initialization to main routine */
    lrInitComplete (OSIX_SUCCESS);

    while (CN_ONE)
    {
        if (OsixEvtRecv (gCnMasterTblInfo.CnTaskId, CN_ALL_EVENTS,
                         OSIX_WAIT, &u4Events) == OSIX_SUCCESS)
        {
            /* Mutual exclusion flag ON */
            CnLock ();

            if (u4Events & CN_TMR_EXPIRY_EVENT)
            {
                CnTmrExpHandler ();
            }
            if (u4Events & CN_QMSG_EVENT)
            {
                CnQueMsgHandler ();
            }
            CnUnLock ();
        }
    }
}

/****************************************************************************
*
*    FUNCTION NAME    : CnMainTaskInit
*
*    DESCRIPTION      : This function creates the task queue, allocates
*                       MemPools for task queue messages and creates protocol
*                       semaphore.
*
*    INPUT            : None
*
*    OUTPUT           : None
*
*    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
*
****************************************************************************/
PUBLIC INT4
CnMainTaskInit (VOID)
{
    MEMSET (&gCnMasterTblInfo, CN_ZERO, sizeof (tCnMasterTblInfo));

    OsixTskIdSelf (&(gCnMasterTblInfo.CnTaskId));

    /* CN Protocol semaphore - created with initial value 0. */
    if (OsixSemCrt (CN_PROTO_SEM, &(gCnMasterTblInfo.SemId)) == OSIX_FAILURE)
    {
        CN_GBL_TRC (CN_INVALID_CONTEXT, CN_CRITICAL_TRC,
                    "CnMainTaskInit: CN protocol semaphore "
                    "creation FAILED !!!\r\n");
        return (OSIX_FAILURE);
    }

    /* Semaphore is by default created with initial value 0. So GiveSem
     * is called before using the semaphore. */
    OsixSemGive (gCnMasterTblInfo.SemId);

    /* CN MsgQ */
    if (OsixQueCrt ((UINT1 *) CN_MSG_QUEUE_NAME, OSIX_MAX_Q_MSG_LEN,
                    CN_MAX_MSGQ_DEPTH, &(gCnMasterTblInfo.cnTaskQId))
        == OSIX_FAILURE)
    {
        CN_GBL_TRC (CN_INVALID_CONTEXT, CN_CRITICAL_TRC,
                    "CnMainTaskInit: CN Msg Queue creation FAILED !!!\r\n");
        return (OSIX_FAILURE);
    }

    /* start CN module */
    if (CnModuleStart () == OSIX_FAILURE)
    {
        CN_GBL_TRC (CN_INVALID_CONTEXT, CN_CRITICAL_TRC,
                    "CnMainTaskInit: CN Module Start FAILED !!!\r\n");
        return (OSIX_FAILURE);
    }

    return (OSIX_SUCCESS);
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : CnMainInitMasterTblInfo 
 *                                                                          
 *    DESCRIPTION      : This function initalizes global structure with 
 *                       default values.
 *
 *    INPUT            : None 
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE 
 *                                                                          
 ****************************************************************************/
PUBLIC INT4
CnMainInitMasterTblInfo (VOID)
{

    /*allocating memory for component table */
    gCnMasterTblInfo.ppCompTblInfo = (tCnCompTblInfo **)
        MemAllocMemBlk (gCnMasterTblInfo.pCompPriTblPoolId);

    if (gCnMasterTblInfo.ppCompTblInfo == NULL)
    {
        CN_GBL_TRC (CN_INVALID_CONTEXT, CN_CRITICAL_TRC,
                    "CnMainInitMasterTblInfo: Memory Allocation FAILURE for "
                    "Context table present in CN Master Table\r\n");
        return OSIX_FAILURE;
    }
    MEMSET (gCnMasterTblInfo.ppCompTblInfo, CN_ZERO,
            ((sizeof (tCnCompTblInfo *)) * CN_MAX_CONTEXTS));

    /*Assigning default trap value */
    gCnMasterTblInfo.u4GlobalTrapStatus = CN_DEFAULT_TRAP;

    /*Enabling the master */
    gCnMasterTblInfo.u1CnSysControl = CN_STARTED;

#ifndef FM_WANTED
    gCnMasterTblInfo.u4SysLogId = CN_ZERO;
#endif
    /*setting the delay timer interval */
    gCnMasterTblInfo.i4TlvTxDelay = CN_TLV_TX_DELAY;

    return (OSIX_SUCCESS);
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : CnMainHandleInitFailure 
 *                                                                          
 *    DESCRIPTION      : Function called when the initialization of CN
 *                       fails at any point during initialization.  
 *
 *    INPUT            : None 
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : None 
 *                                                                          
 ****************************************************************************/
PUBLIC VOID
CnMainHandleInitFailure (VOID)
{
    /*Deleting the RB trees */
    CnUtilDeleteRBTrees ();

    CnUtilDeleteQMsg (gCnMasterTblInfo.cnTaskQId, gCnMasterTblInfo.QMsgPoolId);

    CnSizingMemDeleteMemPools ();

    if (gCnMasterTblInfo.cnTaskQId != CN_ZERO)
    {
        /* Delete Q */
        OsixQueDel (gCnMasterTblInfo.cnTaskQId);
    }
    /* Delete Semaphore */
    if (gCnMasterTblInfo.SemId != CN_ZERO)
    {
        OsixSemDel (gCnMasterTblInfo.SemId);
    }
    /*DeInit the Timer */
    if (gCnMasterTblInfo.TmrListId != CN_ZERO)
    {
        CnTmrDeInit ();
    }
    return;
}

/*****************************************************************************/
/* Function Name      : CnMainAssignMempoolIds                               */
/*                                                                           */
/* Description        : This function is used to assign respective mempool   */
/*                       ID's                                                */
/*                                                                           */
/* Input (s)          : NONE.                                                */
/*                                                                           */
/* Output (s)         : NONE                                                 */
/*                                                                           */
/* Return Value(s)    : NONE                                                 */
/*                                                                           */
/*****************************************************************************/

VOID
CnMainAssignMempoolIds (VOID)
{
    /*tCnQMsg */
    gCnMasterTblInfo.QMsgPoolId = CNMemPoolIds[MAX_CN_Q_MESG_SIZING_ID];

    /*tCnCompTblInfo */
    gCnMasterTblInfo.CompTblPoolId =
        CNMemPoolIds[MAX_CN_COMP_TABLE_INFO_SIZING_ID];

    /*tCnCompPriTbl */
    gCnMasterTblInfo.CompPriTblPoolId =
        CNMemPoolIds[MAX_CN_COMP_PRIORITY_TABLE_SIZING_ID];

    /*tCnPortTblInfo */
    gCnMasterTblInfo.PortTblPoolId =
        CNMemPoolIds[MAX_CN_PORT_TABLE_INFO_SIZING_ID];

    /*tCnPortPriTblInfo */
    gCnMasterTblInfo.PortPriTblPoolId =
        CNMemPoolIds[MAX_CN_PORT_PRIORITY_TABLE_INFO_SIZING_ID];

    /*tCnCompTblInfo* */
    gCnMasterTblInfo.pCompPriTblPoolId =
        CNMemPoolIds[MAX_CN_COMP_TABLE_PTRS_SIZING_ID];
}

/*--------------------------------------------------------------------------*/
/*                       End of the file  cnmain.c                          */
/*--------------------------------------------------------------------------*/
