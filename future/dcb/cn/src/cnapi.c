/*****************************************************************************
 * Copyright (C) 2010 Aricent Inc . All Rights Reserved
 *
 * $Id: cnapi.c,v 1.4 2010/08/17 11:06:21 prabuc Exp $
 *
 * Description: This file contains the APIs exported by CN module.
 ******************************************************************************/
#include "cninc.h"

/****************************************************************************
*
*    FUNCTION NAME    : CnApiContextRequest 
*
*    DESCRIPTION      : This function is invoked by L2IWF to posts messages 
*                       to CN task for the following events,
*                       1. Creation of context
*                       2. Deletion of context 
*
*    INPUT            : u4ContextId - ComponentId
*                       u1Request - event from l2iwf
*                       1. For context creation - CN_CREATE_CXT_MSG
*                       2. For context deletion - CN_DELETE_CXT_MSG
*
*    OUTPUT           : NONE
*
*    RETURNS          : OSIX_SUCCESS / OSIX_FAILURE 
*
****************************************************************************/
PUBLIC INT4
CnApiContextRequest (UINT4 u4ContextId, UINT1 u1Request)
{
    tCnQMsg            *pMsg = NULL;

    u4ContextId = CN_CONVERT_CXT_ID_TO_COMP_ID (u4ContextId);
    /*check for the CN module start or shutdown */
    if (gCnMasterTblInfo.u1CnSysControl != CN_STARTED)
    {
        return OSIX_SUCCESS;
    }

    /* Allocate Interface message buffer from the pool */
    if ((pMsg = (tCnQMsg *) MemAllocMemBlk (gCnMasterTblInfo.QMsgPoolId))
        == NULL)
    {
        CN_GBL_TRC_ARG1 (CN_INVALID_CONTEXT,
                         (CN_RESOURCE_TRC | CN_ALL_FAILURE_TRC),
                         "CnApiContextRequest: Context creation Msg ALLOC_MEM_BLOCK"
                         " FAILED for ContextId %d \r\n", u4ContextId);
        return OSIX_FAILURE;
    }

    MEMSET (pMsg, CN_ZERO, sizeof (tCnQMsg));

    pMsg->u4ContextId = u4ContextId;

    switch (u1Request)
    {
        case CN_CREATE_CXT_MSG:
            pMsg->u4MsgType = CN_CREATE_CONTEXT_MSG;
            break;
        case CN_DELETE_CXT_MSG:
            pMsg->u4MsgType = CN_DELETE_CONTEXT_MSG;
            break;
        default:
            MemReleaseMemBlock (gCnMasterTblInfo.QMsgPoolId, (UINT1 *) pMsg);
            CN_GBL_TRC_ARG1 (CN_INVALID_CONTEXT,
                             (CN_RESOURCE_TRC | CN_ALL_FAILURE_TRC),
                             "CnApiContextRequest: "
                             "Invalid ContextId %d request received\r\n",
                             u4ContextId);
            return OSIX_FAILURE;
    }

    return (CnQueEnqMsg (pMsg));
}

/****************************************************************************
*
*    FUNCTION NAME    : CnApiPortRequest 
*
*    DESCRIPTION      : This function is called by L2IWF module to post message
*                       to CN module to indicate the following events, 
*                       1. Creation of a port
*                       2. Deletion of a port
*                       3. Mapping a port to a context 
*                       4. UnMapping a port from a context
*
*    INPUT            : u4ContextId - ComponentId
*                       u4PortId    - Port Id
*                       u1Request - event from l2iwf
*                          For port creation - CN_CREATE_IF_MSG
*                          For port deletion - CN_DELETE_IF_MSG
*                          For port mapping  - CN_MAP_PORT_MSG
*                          For port unmapping- CN_UNMAP_IF_MSG
*
*    OUTPUT           : NONE
*
*    RETURNS          : OSIX_SUCCESS / OSIX_FAILURE
*
****************************************************************************/
PUBLIC INT4
CnApiPortRequest (UINT4 u4ContextId, UINT4 u4PortId, UINT1 u1Request)
{
    tCnQMsg            *pMsg = NULL;

    u4ContextId = CN_CONVERT_CXT_ID_TO_COMP_ID (u4ContextId);

    /*check for the CN module start or shutdown */
    if (gCnMasterTblInfo.u1CnSysControl != CN_STARTED)
    {
        return OSIX_SUCCESS;
    }

    if (u4PortId > CN_MAX_PORTS)
    {
        CN_TRC_ARG1 (u4ContextId, CN_ALL_FAILURE_TRC,
                     "CnApiPortRequest invoked with invalid port"
                     " %d \r\n", u4PortId);
        return OSIX_FAILURE;
    }

    /* Allocate Interface message buffer from the pool */
    if ((pMsg = (tCnQMsg *) MemAllocMemBlk (gCnMasterTblInfo.QMsgPoolId))
        == NULL)
    {
        CN_TRC_ARG2 (u4ContextId, (CN_RESOURCE_TRC | CN_ALL_FAILURE_TRC),
                     "CnApiPortRequest: ContextId %d Port %d"
                     " Msg ALLOC_MEM_BLOCK FAILED\r\n", u4ContextId, u4PortId);
        return OSIX_FAILURE;
    }

    MEMSET (pMsg, CN_ZERO, sizeof (tCnQMsg));

    pMsg->u4ContextId = u4ContextId;
    pMsg->u4PortId = u4PortId;

    switch (u1Request)
    {
        case CN_CREATE_IF_MSG:
            pMsg->u4MsgType = CN_CREATE_PORT_MSG;
            break;
        case CN_DELETE_IF_MSG:
            pMsg->u4MsgType = CN_DELETE_PORT_MSG;
            break;
        case CN_MAP_IF_MSG:
            pMsg->u4MsgType = CN_MAP_PORT_MSG;
            break;
        case CN_UNMAP_IF_MSG:
            pMsg->u4MsgType = CN_UNMAP_PORT_MSG;
            break;
        default:
            MemReleaseMemBlock (gCnMasterTblInfo.QMsgPoolId, (UINT1 *) pMsg);
            CN_TRC_ARG3 (u4ContextId,
                         (CN_RESOURCE_TRC | CN_ALL_FAILURE_TRC),
                         "CnApiPortRequest: ContextId %d  Port %d "
                         "Invalid request %d received\r\n",
                         u4ContextId, u4PortId, u1Request);
            return OSIX_FAILURE;
    }

    return (CnQueEnqMsg (pMsg));
}

/****************************************************************************
*
*    FUNCTION NAME    : CnApiPortToPortChannelRequest
*
*    DESCRIPTION      : This function is called by L2IWF module to post message
*                       to CN module to indicate the following events.
*                       1. Port becoming member of LAG
*                       2. Port removed from a LAG
*
*    INPUT            : u4ContextId - ComponentId
*                       u4PortId - Port Id
*                       u4PortChId - LAG Id
*                       u1Request - event from l2iwf  
*                          1. Member port addition - CN_ADD_LA_MEM_MSG
*                          2. Member port deletion - CN_REM_LA_MEM_MSG
*
*    OUTPUT           : NONE
*
*    RETURNS          : OSIX_SUCCESS / OSIX_FAILURE
*
****************************************************************************/
PUBLIC INT4
CnApiPortToPortChannelRequest (UINT4 u4ContextId, UINT4 u4PortId,
                               UINT4 u4PortChId, UINT1 u1Request)
{
    tCnQMsg            *pMsg = NULL;

    u4ContextId = CN_CONVERT_CXT_ID_TO_COMP_ID (u4ContextId);

    /*check for the CN module start or shutdown */
    if (gCnMasterTblInfo.u1CnSysControl != CN_STARTED)
    {
        return OSIX_SUCCESS;
    }
    if (u4PortId > CN_MAX_PORTS)
    {
        CN_TRC_ARG1 (u4ContextId, CN_ALL_FAILURE_TRC,
                     "CnApiPortToPortChannelRequest invoked for "
                     "invalid port %d \r\n", u4PortId);
        return OSIX_FAILURE;
    }

    /* Allocate Interface message buffer from the pool */
    if ((pMsg = (tCnQMsg *) MemAllocMemBlk (gCnMasterTblInfo.QMsgPoolId))
        == NULL)
    {
        CN_TRC_ARG2 (u4ContextId, (CN_RESOURCE_TRC | CN_ALL_FAILURE_TRC),
                     "CnApiPortToPortChannelRequest: ContextId %d Port %d"
                     " ALLOC_MEM_BLOCK FAILED\r\n", u4ContextId, u4PortId);
        return OSIX_FAILURE;
    }

    MEMSET (pMsg, CN_ZERO, sizeof (tCnQMsg));

    pMsg->u4PortId = u4PortChId;
    pMsg->u4ContextId = u4ContextId;
    pMsg->unMsgParam.u4LagMemberPortId = u4PortId;

    switch (u1Request)
    {
        case CN_ADD_LA_MEM_MSG:
            pMsg->u4MsgType = CN_ADD_LAG_MEMBER_MSG;
            break;
        case CN_REM_LA_MEM_MSG:
            pMsg->u4MsgType = CN_REM_LAG_MEMBER_MSG;
            break;
        default:
            MemReleaseMemBlock (gCnMasterTblInfo.QMsgPoolId, (UINT1 *) pMsg);
            CN_TRC_ARG3 (u4ContextId, CN_ALL_FAILURE_TRC,
                         "CnApiPortToPortChannelRequest : ContextId %d Port %d"
                         " Invalid request %d received\r\n",
                         u4ContextId, u4PortId, u1Request);
            return OSIX_FAILURE;
    }

    return (CnQueEnqMsg (pMsg));
}

/****************************************************************************
*
*    FUNCTION NAME    : CnApiNpapiNotifyCnm
*
*    DESCRIPTION      : This function posts a message to CN task's 
*                       queue to indicate that CNM generated in Hardware.
*                       This function is invoked by hardware whenever 
*                       CNM is generated at a CP.
*
*    INPUT            : pu1CpId - Pointer to CPID
*                       i4CnmQOffset - CNM offset 
*                       i4CnmQDelta - CNM delta
*
*    OUTPUT           : NONE
*
*    RETURNS          : OSIX_SUCCESS / OSIX_FAILURE
*
****************************************************************************/
PUBLIC INT4
CnApiNpapiNotifyCnm (UINT1 *pu1CpId, INT4 i4CnmQOffset, INT4 i4CnmQDelta)
{
    tCnQMsg            *pMsg = NULL;

    /* Allocate Interface message buffer from the pool */
    if ((pMsg = (tCnQMsg *) MemAllocMemBlk (gCnMasterTblInfo.QMsgPoolId))
        == NULL)
    {
        CN_GBL_TRC (CN_INVALID_CONTEXT, (CN_RESOURCE_TRC | CN_ALL_FAILURE_TRC),
                    "CnApiNpapiNotifyCnm: "
                    "Memory allocation failed for CN message queue\r\n");
        return OSIX_FAILURE;
    }

    MEMSET (pMsg, CN_ZERO, sizeof (tCnQMsg));

    pMsg->u4MsgType = CN_CNM_GENERATE_MSG;
    pMsg->unMsgParam.CnCnmGen.i4CnmQOffset = i4CnmQOffset;
    pMsg->unMsgParam.CnCnmGen.i4CnmQDelta = i4CnmQDelta;
    MEMCPY (pMsg->unMsgParam.CnCnmGen.au1CpIdentifier, pu1CpId, CN_CPID_LEN);

    return (CnQueEnqMsg (pMsg));
}

/****************************************************************************
*
*    FUNCTION NAME    : CnApiApplCallbkFunc 
*
*    DESCRIPTION      : This is the application call back function. This 
*                       function will be registered with LLDP and LLDP will
*                       post events to CN module through this function.
*
*    INPUT            : pLldpAppTlv - LLDP Message to CN.
*
*    OUTPUT           : NONE
*
*    RETURNS          : NONE
*
****************************************************************************/
PUBLIC VOID
CnApiApplCallbkFunc (tLldpAppTlv * pLldpAppTlv)
{
    tCnQMsg            *pMsg = NULL;

    /*check for the CN module start or shutdown */
    if (gCnMasterTblInfo.u1CnSysControl != CN_STARTED)
    {
        return;
    }
    if (pLldpAppTlv->u4MsgType > L2IWF_LLDP_APPL_MAX_MSG)
    {
        CN_GBL_TRC_ARG1 (CN_INVALID_CONTEXT, CN_ALL_FAILURE_TRC,
                         "CnApiApplCallbkFunc:  "
                         "Invoked with invalid Event %d \r\n",
                         pLldpAppTlv->u4MsgType);

        return;
    }

    if ((pLldpAppTlv->u4MsgType == L2IWF_LLDP_APPL_TLV_RECV) ||
        (pLldpAppTlv->u4MsgType == L2IWF_LLDP_APPL_TLV_AGED))
    {
        if (pLldpAppTlv->u4RxAppTlvPortId > CN_MAX_PORTS)
        {
            CN_GBL_TRC_ARG1 (CN_INVALID_CONTEXT, CN_ALL_FAILURE_TRC,
                             "CnApiApplCallbkFunc: "
                             "Invoked with invalid port %d\r\n",
                             pLldpAppTlv->u4RxAppTlvPortId);
            return;
        }

        if (pLldpAppTlv->u4MsgType == L2IWF_LLDP_APPL_TLV_RECV)
        {                        /*checking the TLV length */
            if (pLldpAppTlv->u2RxAppTlvLen != CN_TLV_LEN)
            {
                return;
            }
        }
    }

    /* Allocate Interface message buffer from the pool */
    if ((pMsg = (tCnQMsg *) MemAllocMemBlk (gCnMasterTblInfo.QMsgPoolId))
        == NULL)
    {
        CN_GBL_TRC_ARG1 (CN_INVALID_CONTEXT,
                         (CN_RESOURCE_TRC | CN_ALL_FAILURE_TRC),
                         "CnApiApplCallbkFunc: Port %d,"
                         " Msg ALLOC_MEM_BLOCK FAILED\r\n",
                         pLldpAppTlv->u4RxAppTlvPortId);
        return;
    }

    MEMSET (pMsg, CN_ZERO, sizeof (tCnQMsg));

    pMsg->u4MsgType = pLldpAppTlv->u4MsgType;
    pMsg->u4PortId = pLldpAppTlv->u4RxAppTlvPortId;

    /* Application TLV pointer is valid only when the received
     * message type is LLDP_APPL_TLV_RECV */
    if (pLldpAppTlv->u4MsgType == L2IWF_LLDP_APPL_TLV_RECV)
    {
        MEMCPY (pMsg->unMsgParam.au1CNTlv, pLldpAppTlv->pu1RxAppTlv,
                pLldpAppTlv->u2RxAppTlvLen);
    }

    CnQueEnqMsg (pMsg);

    return;
}

/****************************************************************************
*
*    FUNCTION NAME    : CnApiCopyPortPropertiesToHw
*
*    DESCRIPTION      : This function posts a message to CN task's message
*                       queue to indicate the port channel status created 
*                       in hardware
*
*    INPUT            : u4PoIndex - PortChannel Index
*
*    OUTPUT           : NONE
*
*    RETURNS          : OSIX_SUCCESS / OSIX_FAILURE
*
****************************************************************************/
PUBLIC INT4
CnApiCopyPortPropertiesToHw (UINT4 u4PoIndex)
{
    tCnQMsg            *pMsg = NULL;
    UINT4               u4ContextId = CN_ZERO;
    UINT2               u2LocalPortId = CN_ZERO;

    if (gCnMasterTblInfo.u1CnSysControl != CN_STARTED)
    {
        return OSIX_SUCCESS;
    }

    if (CnPortVcmGetCxtInfoFromIfIndex (u4PoIndex, &u4ContextId,
                                        &u2LocalPortId) == OSIX_FAILURE)
    {
        CN_GBL_TRC_ARG1 (CN_INVALID_CONTEXT, CN_ALL_FAILURE_TRC,
                         "CnApiCopyPortPropertiesToHw: Port %d is not "
                         "mapped to any Context", u4PoIndex);
        return OSIX_FAILURE;
    }

    /* Allocate Interface message buffer from the pool */
    if ((pMsg = (tCnQMsg *) MemAllocMemBlk (gCnMasterTblInfo.QMsgPoolId))
        == NULL)
    {
        CN_TRC_ARG2 (u4ContextId, (CN_RESOURCE_TRC | CN_ALL_FAILURE_TRC),
                     "CnApiCopyPortPropertiesToHw: ContextId %d  Port %d,"
                     " ALLOC_MEM_BLOCK FAILED\r\n", u4ContextId, u4PoIndex);
        return OSIX_FAILURE;
    }
    MEMSET (pMsg, CN_ZERO, sizeof (tCnQMsg));

    pMsg->u4ContextId = u4ContextId;
    pMsg->u4PortId = u4PoIndex;
    pMsg->u4MsgType = CN_PO_READY_IN_HW_MSG;
    return (CnQueEnqMsg (pMsg));
}

/*---------------------------------------------------------------------------*/
/*                       End of the file  cnapi.c                            */
/*---------------------------------------------------------------------------*/
