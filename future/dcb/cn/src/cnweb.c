/********************************************************************
 * Copyright (C) 2010 Aricent Inc . All Rights Reserved
 *
 * $Id: cnweb.c,v 1.7 2014/01/25 13:55:08 siva Exp $
 *
 * Description: Routines for CN web Module
 *******************************************************************/

#ifdef WEBNM_WANTED

#include "cninc.h"

PRIVATE VOID CnUtilWebGetPortPriEntry PROTO ((tHttp * pHttp,
                                              UINT4 u4CompId,
                                              UINT4 u4Pri, INT4 i4Port));
PRIVATE VOID CnUtilWebGetCpTblEntry PROTO ((tHttp * pHttp,
                                            UINT4 u4CompId,
                                            UINT4 u4CPIndex, INT4 i4Port));

PRIVATE VOID CnUtilWebGetStatistics PROTO ((tHttp * pHttp,
                                            UINT4 u4CompId,
                                            UINT4 u4Pri, INT4 i4Port));

PRIVATE VOID CnUtilWebGetDebug PROTO ((tHttp * pHttp, UINT4 u4CompId));

PRIVATE VOID CnUtilWebGetCxtandPort PROTO ((tHttp * pHttp));

PRIVATE VOID CnUtilWebGetCxt PROTO ((tHttp * pHttp));

PRIVATE VOID CnProcessCnPortSettingsPageGet PROTO ((tHttp * pHttp));
PRIVATE VOID CnProcessCnPortSettingsPageSet PROTO ((tHttp * pHttp));
PRIVATE VOID CnProcessCnCpSettingsPageGet PROTO ((tHttp * pHttp));
PRIVATE VOID CnProcessCnCpSettingsPageSet PROTO ((tHttp * pHttp));
PRIVATE VOID CnProcessCnStatisticsPageGet PROTO ((tHttp * pHttp));
PRIVATE VOID CnProcessCnStatisticsPageSet PROTO ((tHttp * pHttp));
PRIVATE VOID CnProcessCnDebugPageGet PROTO ((tHttp * pHttp));
PRIVATE VOID CnProcessCnDebugPageSet PROTO ((tHttp * pHttp));

/*********************************************************************
 *  Function Name : CnProcessCnPortSettingsPage    
 *  Description   : This function processes the request coming for CN 
 *                  Port Settings page.
 *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 *  Output(s)     : None.
 *  Return Values : None
 *********************************************************************/
VOID
CnProcessCnPortSettingsPage (tHttp * pHttp)
{
    WebnmRegisterLock (pHttp, CnLock, CnUnLock);
    CnLock ();
    if (pHttp->i4Method == ISS_GET)
    {
        CnProcessCnPortSettingsPageGet (pHttp);
    }
    else if (pHttp->i4Method == ISS_SET)
    {
        CnProcessCnPortSettingsPageSet (pHttp);
    }
    CnUnLock ();
    WebnmUnRegisterLock (pHttp);
    return;
}

/*********************************************************************
 *  Function Name : CnProcessCnPortSettingsPageGet
 *  Description   : This function processes the request coming for CN
 *                  Port Settings page.
 *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 *  Output(s)     : None.
 *  Return Values : None
 *********************************************************************/
PRIVATE VOID
CnProcessCnPortSettingsPageGet (tHttp * pHttp)
{
    CnUtilWebGetCxtandPort (pHttp);
    WebnmSendString (pHttp, ISS_TABLE_FLAG);
    IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
    WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                    (pHttp->i4HtmlSize - pHttp->i4Write));
    return;
}

/*********************************************************************
 *  Function Name : CnProcessCnPortSettingsPageSet
 *  Description   : This function processes the request coming for CN
 *                  Port Settings page.
 *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 *  Output(s)     : None.
 *  Return Values : None
 *********************************************************************/
PRIVATE VOID
CnProcessCnPortSettingsPageSet (tHttp * pHttp)
{
    UINT4               u4ContextId = CN_ZERO;
    UINT4               u4TempContextId = CN_ZERO;
    UINT4               u4CurrPri = CN_ZERO;
    UINT4               u4AltPri = CN_ZERO;
    UINT4               u4Temp = CN_ZERO;
    INT4                i4CurrIf = CN_ZERO;
    INT4                i4SetVal = CN_ZERO;

    STRCPY (pHttp->au1Name, "ACTION");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    if (STRCMP (pHttp->au1Value, "Display") == CN_ZERO)
    {
        STRCPY (pHttp->au1Name, "DCB_CN_CONTEXT");
        HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery);
        u4TempContextId = ATOI (pHttp->au1Value);

        STRCPY (pHttp->au1Name, "DCB_CN_PORT");
        HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery);
        i4CurrIf = ATOI (pHttp->au1Value);
        STRCPY (pHttp->au1Name, "CNPV_GET");
        HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery);
        u4CurrPri = ATOI (pHttp->au1Value);

        CnUtilWebGetCxtandPort (pHttp);
        WebnmSendString (pHttp, ISS_TABLE_FLAG);

        if (u4CurrPri == CN_MAX_VLAN_PRIORITY)
        {
            u4Temp = pHttp->i4Write;
            for (u4CurrPri = CN_ZERO; u4CurrPri < CN_MAX_VLAN_PRIORITY;
                 u4CurrPri++)
            {
                if (nmhValidateIndexInstanceIeee8021CnPortPriTable
                    (u4TempContextId, u4CurrPri, i4CurrIf) == SNMP_SUCCESS)
                {
                    pHttp->i4Write = u4Temp;
                    CnUtilWebGetPortPriEntry (pHttp, u4TempContextId, u4CurrPri,
                                              i4CurrIf);
                    WebnmSockWrite (pHttp, (UINT1 *)
                                    (pHttp->pi1Html + pHttp->i4Write),
                                    (pHttp->i4HtmlSize - pHttp->i4Write));

                }
            }
        }
        else
        {
            if (nmhValidateIndexInstanceIeee8021CnPortPriTable
                (u4TempContextId, u4CurrPri, i4CurrIf) == SNMP_SUCCESS)
            {
                CnUtilWebGetPortPriEntry (pHttp, u4TempContextId,
                                          u4CurrPri, i4CurrIf);
                WebnmSockWrite (pHttp, (UINT1 *)
                                (pHttp->pi1Html + pHttp->i4Write),
                                (pHttp->i4HtmlSize - pHttp->i4Write));

            }
        }
    }
    else
    {
        STRCPY (pHttp->au1Name, "COMPID");
        if (HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                                pHttp->au1PostQuery) == ENM_SUCCESS)
        {
            u4ContextId = (UINT4) ATOI (pHttp->au1Value);
        }
        STRCPY (pHttp->au1Name, "PORTID");
        if (HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                                pHttp->au1PostQuery) == ENM_SUCCESS)
        {
            i4CurrIf = (UINT4) ATOI (pHttp->au1Value);
        }
        STRCPY (pHttp->au1Name, "CNPV");
        if (HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                                pHttp->au1PostQuery) == ENM_SUCCESS)
        {
            u4CurrPri = (UINT4) ATOI (pHttp->au1Value);
        }

        STRCPY (pHttp->au1Name, "DEFENSE_MOD_CH");
        if (HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                                pHttp->au1PostQuery) == ENM_SUCCESS)
        {
            i4SetVal = (UINT4) ATOI (pHttp->au1Value);
        }

        nmhSetIeee8021CnPortPriDefModeChoice (u4ContextId, u4CurrPri,
                                              i4CurrIf, i4SetVal);

        STRCPY (pHttp->au1Name, "ALTPRI");
        if (HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                                pHttp->au1PostQuery) == ENM_SUCCESS)
        {
            u4AltPri = (UINT4) ATOI (pHttp->au1Value);
        }
        nmhSetIeee8021CnPortPriAlternatePriority (u4ContextId, u4CurrPri,
                                                  i4CurrIf, u4AltPri);

        STRCPY (pHttp->au1Name, "DEFENSE_MOD");
        if (HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                                pHttp->au1PostQuery) == ENM_SUCCESS)
        {
            i4SetVal = (UINT4) ATOI (pHttp->au1Value);
        }
        nmhSetIeee8021CnPortPriAdminDefenseMode (u4ContextId, u4CurrPri,
                                                 i4CurrIf, i4SetVal);

        STRCPY (pHttp->au1Name, "LLDP_INS_CH");
        if (HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                                pHttp->au1PostQuery) == ENM_SUCCESS)
        {
            i4SetVal = (UINT4) ATOI (pHttp->au1Value);
        }
        nmhSetIeee8021CnPortPriLldpInstanceChoice (u4ContextId, u4CurrPri,
                                                   i4CurrIf, i4SetVal);

        CnUtilWebGetCxtandPort (pHttp);
        WebnmSendString (pHttp, ISS_TABLE_FLAG);
        CnUtilWebGetPortPriEntry (pHttp, u4ContextId, u4CurrPri, i4CurrIf);
        WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                        (pHttp->i4HtmlSize - pHttp->i4Write));
    }
    return;
}

/*********************************************************************
 *  Function Name : CnProcessCnCpSettingsPage
 *  Description   : This function processes the request coming for CN
 *                  CP Settings page.
 *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 *  Output(s)     : None.
 *  Return Values : None
 *********************************************************************/
VOID
CnProcessCnCpSettingsPage (tHttp * pHttp)
{
    WebnmRegisterLock (pHttp, CnLock, CnUnLock);
    CnLock ();
    if (pHttp->i4Method == ISS_GET)
    {
        CnProcessCnCpSettingsPageGet (pHttp);
    }
    else if (pHttp->i4Method == ISS_SET)
    {
        CnProcessCnCpSettingsPageSet (pHttp);
    }
    CnUnLock ();
    WebnmUnRegisterLock (pHttp);
    return;
}

/*********************************************************************
 *  Function Name : CnProcessCnCpSettingsPageGet
 *  Description   : This function processes the request coming for CN
 *                  CP Settings page.
 *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 *  Output(s)     : None.
 *  Return Values : None
 *********************************************************************/
PRIVATE VOID
CnProcessCnCpSettingsPageGet (tHttp * pHttp)
{
    CnUtilWebGetCxtandPort (pHttp);
    WebnmSendString (pHttp, ISS_TABLE_FLAG);
    IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
    WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                    (pHttp->i4HtmlSize - pHttp->i4Write));
    return;
}

/*********************************************************************
 *  Function Name : CnProcessCnCpSettingsPageSet
 *  Description   : This function processes the request coming for CN
 *                  CP Settings page.
 *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 *  Output(s)     : None.
 *  Return Values : None
 *********************************************************************/
PRIVATE VOID
CnProcessCnCpSettingsPageSet (tHttp * pHttp)
{
    UINT4               u4ContextId = CN_ZERO;
    UINT4               u4TempContextId = CN_ZERO;
    UINT4               u4CPIndex = CN_ZERO;
    UINT4               u4ErrorCode = CN_ZERO;
    UINT4               u4SetVal = CN_ZERO;
    UINT4               u4Temp = CN_ZERO;
    INT4                i4CurrIf = CN_ZERO;
    INT4                i4SetVal = CN_ZERO;

    STRCPY (pHttp->au1Name, "ACTION");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    if (STRCMP (pHttp->au1Value, "Display") == CN_ZERO)
    {
        STRCPY (pHttp->au1Name, "DCB_CN_CONTEXT");
        HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery);
        u4TempContextId = ATOI (pHttp->au1Value);

        STRCPY (pHttp->au1Name, "DCB_CN_PORT");
        HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery);
        i4CurrIf = ATOI (pHttp->au1Value);
        STRCPY (pHttp->au1Name, "CPINDEX_GET");
        HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery);
        u4CPIndex = ATOI (pHttp->au1Value);

        CnUtilWebGetCxtandPort (pHttp);
        WebnmSendString (pHttp, ISS_TABLE_FLAG);

        if (u4CPIndex == CN_MAX_VLAN_PRIORITY)
        {
            u4Temp = pHttp->i4Write;
            for (u4CPIndex = CN_ONE; u4CPIndex <= CN_MAX_VLAN_PRIORITY;
                 u4CPIndex++)
            {
                if (nmhValidateIndexInstanceIeee8021CnCpTable
                    (u4TempContextId, i4CurrIf, u4CPIndex) == SNMP_SUCCESS)
                {
                    pHttp->i4Write = u4Temp;
                    CnUtilWebGetCpTblEntry (pHttp, u4TempContextId, u4CPIndex,
                                            i4CurrIf);
                    WebnmSockWrite (pHttp, (UINT1 *)
                                    (pHttp->pi1Html + pHttp->i4Write),
                                    (pHttp->i4HtmlSize - pHttp->i4Write));

                }
            }
        }
        else
        {
            u4CPIndex = u4CPIndex + CN_ONE;
            if (nmhValidateIndexInstanceIeee8021CnCpTable
                (u4TempContextId, i4CurrIf, u4CPIndex) == SNMP_SUCCESS)
            {
                CnUtilWebGetCpTblEntry (pHttp, u4TempContextId, u4CPIndex,
                                        i4CurrIf);
                WebnmSockWrite (pHttp, (UINT1 *)
                                (pHttp->pi1Html + pHttp->i4Write),
                                (pHttp->i4HtmlSize - pHttp->i4Write));

            }
        }
    }
    else
    {
        STRCPY (pHttp->au1Name, "COMPID");
        if (HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                                pHttp->au1PostQuery) == ENM_SUCCESS)
        {
            u4ContextId = (UINT4) ATOI (pHttp->au1Value);
        }
        STRCPY (pHttp->au1Name, "PORTID");
        if (HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                                pHttp->au1PostQuery) == ENM_SUCCESS)
        {
            i4CurrIf = (UINT4) ATOI (pHttp->au1Value);
        }
        STRCPY (pHttp->au1Name, "CPINDEX");
        if (HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                                pHttp->au1PostQuery) == ENM_SUCCESS)
        {
            u4CPIndex = (UINT4) ATOI (pHttp->au1Value);
        }

        STRCPY (pHttp->au1Name, "QUEUE_SET_PT");
        if (HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                                pHttp->au1PostQuery) == ENM_SUCCESS)
        {
            u4SetVal = (UINT4) ATOI (pHttp->au1Value);
        }

        if (nmhTestv2Ieee8021CnCpQueueSizeSetPoint (&u4ErrorCode, u4ContextId,
                                                    i4CurrIf, u4CPIndex,
                                                    u4SetVal) != SNMP_FAILURE)
        {
            nmhSetIeee8021CnCpQueueSizeSetPoint (u4ContextId, i4CurrIf,
                                                 u4CPIndex, u4SetVal);
        }
        else
        {
            IssSendError (pHttp, (CONST INT1 *)
                          "Queue Size Set Point is not within valid range");
        }
        STRCPY (pHttp->au1Name, "FEEDBK_WEIGHT");
        if (HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                                pHttp->au1PostQuery) == ENM_SUCCESS)
        {
            i4SetVal = (UINT4) ATOI (pHttp->au1Value);
        }
        if (nmhTestv2Ieee8021CnCpFeedbackWeight (&u4ErrorCode, u4ContextId,
                                                 i4CurrIf, u4CPIndex,
                                                 i4SetVal) != SNMP_FAILURE)
        {
            nmhSetIeee8021CnCpFeedbackWeight (u4ContextId, i4CurrIf,
                                              u4CPIndex, i4SetVal);
        }
        else
        {
            IssSendError (pHttp, (CONST INT1 *)
                          "FeedBack Weight is not within valid range");
        }
        STRCPY (pHttp->au1Name, "MIN_SAM_BASE");
        if (HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                                pHttp->au1PostQuery) == ENM_SUCCESS)
        {
            u4SetVal = (UINT4) ATOI (pHttp->au1Value);
        }
        if (nmhTestv2Ieee8021CnCpMinSampleBase (&u4ErrorCode, u4ContextId,
                                                i4CurrIf, u4CPIndex,
                                                u4SetVal) != SNMP_FAILURE)
        {
            nmhSetIeee8021CnCpMinSampleBase (u4ContextId, i4CurrIf,
                                             u4CPIndex, u4SetVal);
        }
        else
        {
            IssSendError (pHttp, (CONST INT1 *)
                          "Minimum Sample Base is not within valid range");
        }
        STRCPY (pHttp->au1Name, "HEADER_OCTETS");
        if (HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                                pHttp->au1PostQuery) == ENM_SUCCESS)
        {
            u4SetVal = (UINT4) ATOI (pHttp->au1Value);
        }
        if (nmhTestv2Ieee8021CnCpMinHeaderOctets (&u4ErrorCode, u4ContextId,
                                                  i4CurrIf, u4CPIndex,
                                                  u4SetVal) != SNMP_FAILURE)
        {
            nmhSetIeee8021CnCpMinHeaderOctets (u4ContextId, i4CurrIf,
                                               u4CPIndex, u4SetVal);
        }
        else
        {
            IssSendError (pHttp, (CONST INT1 *)
                          "Header Octets is not within valid range");
        }
        CnUtilWebGetCxtandPort (pHttp);
        WebnmSendString (pHttp, ISS_TABLE_FLAG);
        CnUtilWebGetCpTblEntry (pHttp, u4ContextId, u4CPIndex, i4CurrIf);
        WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                        (pHttp->i4HtmlSize - pHttp->i4Write));
    }
    return;
}

/*********************************************************************
 *  Function Name : CnProcessCnStatisticsPage
 *  Description   : This function processes the request coming for CN
 *                  Statistics page.
 *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 *  Output(s)     : None.
 *  Return Values : None
 *********************************************************************/
VOID
CnProcessCnStatisticsPage (tHttp * pHttp)
{
    WebnmRegisterLock (pHttp, CnLock, CnUnLock);
    CnLock ();
    if (pHttp->i4Method == ISS_GET)
    {
        CnProcessCnStatisticsPageGet (pHttp);
    }
    else if (pHttp->i4Method == ISS_SET)
    {
        CnProcessCnStatisticsPageSet (pHttp);
    }
    CnUnLock ();
    WebnmUnRegisterLock (pHttp);
    return;
}

/*********************************************************************
 *  Function Name : CnProcessCnStatisticsPageGet
 *  Description   : This function processes the request coming for CN
 *                  Statistics page.
 *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 *  Output(s)     : None.
 *  Return Values : None
 *********************************************************************/
PRIVATE VOID
CnProcessCnStatisticsPageGet (tHttp * pHttp)
{
    CnUtilWebGetCxtandPort (pHttp);
    WebnmSendString (pHttp, ISS_TABLE_FLAG);
    IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
    WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                    (pHttp->i4HtmlSize - pHttp->i4Write));
    return;
}

/*********************************************************************
 *  Function Name : CnProcessCnStatisticsPageSet
 *  Description   : This function processes the request coming for CN
 *                  Statistics page.
 *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 *  Output(s)     : None.
 *  Return Values : None
 *********************************************************************/
PRIVATE VOID
CnProcessCnStatisticsPageSet (tHttp * pHttp)
{
    UINT4               u4ContextId = CN_ZERO;
    UINT4               u4TempContextId = CN_ZERO;
    UINT4               u4CurrPri = CN_ZERO;
    UINT4               u4ErrorCode = CN_ZERO;
    UINT4               u4Temp = CN_ZERO;
    INT4                i4CurrIf = CN_ZERO;

    STRCPY (pHttp->au1Name, "ACTION");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    if (STRCMP (pHttp->au1Value, "Display") == CN_ZERO)
    {
        STRCPY (pHttp->au1Name, "DCB_CN_CONTEXT");
        HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery);
        u4TempContextId = ATOI (pHttp->au1Value);

        STRCPY (pHttp->au1Name, "DCB_CN_PORT");
        HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery);
        i4CurrIf = ATOI (pHttp->au1Value);
        STRCPY (pHttp->au1Name, "CNPV_GET");
        HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery);
        u4CurrPri = ATOI (pHttp->au1Value);

        CnUtilWebGetCxtandPort (pHttp);
        WebnmSendString (pHttp, ISS_TABLE_FLAG);

        if (u4CurrPri == CN_MAX_VLAN_PRIORITY)
        {
            u4Temp = pHttp->i4Write;
            for (u4CurrPri = CN_ZERO; u4CurrPri < CN_MAX_VLAN_PRIORITY;
                 u4CurrPri++)
            {
                if (nmhValidateIndexInstanceIeee8021CnPortPriTable
                    (u4TempContextId, u4CurrPri, i4CurrIf) == SNMP_SUCCESS)
                {
                    pHttp->i4Write = u4Temp;
                    CnUtilWebGetStatistics (pHttp, u4TempContextId, u4CurrPri,
                                            i4CurrIf);
                    WebnmSockWrite (pHttp, (UINT1 *)
                                    (pHttp->pi1Html + pHttp->i4Write),
                                    (pHttp->i4HtmlSize - pHttp->i4Write));

                }
            }
        }

        else
        {
            if (nmhValidateIndexInstanceIeee8021CnPortPriTable
                (u4TempContextId, u4CurrPri, i4CurrIf) == SNMP_SUCCESS)
            {
                CnUtilWebGetStatistics (pHttp, u4TempContextId,
                                        u4CurrPri, i4CurrIf);
                WebnmSockWrite (pHttp, (UINT1 *)
                                (pHttp->pi1Html + pHttp->i4Write),
                                (pHttp->i4HtmlSize - pHttp->i4Write));

            }
        }
    }
    else
    {
        STRCPY (pHttp->au1Name, "COMPID");
        if (HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                                pHttp->au1PostQuery) == ENM_SUCCESS)
        {
            u4ContextId = (UINT4) ATOI (pHttp->au1Value);
        }
        STRCPY (pHttp->au1Name, "PORTID");
        if (HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                                pHttp->au1PostQuery) == ENM_SUCCESS)
        {
            i4CurrIf = (UINT4) ATOI (pHttp->au1Value);
        }
        STRCPY (pHttp->au1Name, "CNPV");
        if (HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                                pHttp->au1PostQuery) == ENM_SUCCESS)
        {
            u4CurrPri = (UINT4) ATOI (pHttp->au1Value);
        }
        if (nmhTestv2FsCnXPortPriClearCpCounters (&u4ErrorCode, u4ContextId,
                                                  u4CurrPri, i4CurrIf,
                                                  OSIX_TRUE) != SNMP_FAILURE)
        {
            nmhSetFsCnXPortPriClearCpCounters (u4ContextId, u4CurrPri,
                                               i4CurrIf, OSIX_TRUE);
        }
        CnUtilWebGetCxtandPort (pHttp);
        WebnmSendString (pHttp, ISS_TABLE_FLAG);
        CnUtilWebGetStatistics (pHttp, u4ContextId, u4CurrPri, i4CurrIf);
        WebnmSockWrite (pHttp, (UINT1 *)
                        (pHttp->pi1Html + pHttp->i4Write),
                        (pHttp->i4HtmlSize - pHttp->i4Write));
    }
    return;
}

/*********************************************************************
 *  Function Name : CnProcessCnDebugPage
 *  Description   : This function processes the request coming for CN
 *                  Debug Settings page.
 *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 *  Output(s)     : None.
 *  Return Values : None
 *********************************************************************/
VOID
CnProcessCnDebugPage (tHttp * pHttp)
{
    WebnmRegisterLock (pHttp, CnLock, CnUnLock);
    CnLock ();
    if (pHttp->i4Method == ISS_GET)
    {
        CnProcessCnDebugPageGet (pHttp);
    }
    else if (pHttp->i4Method == ISS_SET)
    {
        CnProcessCnDebugPageSet (pHttp);
    }
    CnUnLock ();
    WebnmUnRegisterLock (pHttp);
    return;
}

/*********************************************************************
 *  Function Name : CnProcessCnDebugPageGet
 *  Description   : This function processes the request coming for CN
 *                  Debug Settings page.
 *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 *  Output(s)     : None.
 *  Return Values : None
 *********************************************************************/
PRIVATE VOID
CnProcessCnDebugPageGet (tHttp * pHttp)
{
    CnUtilWebGetCxt (pHttp);
    CnUtilWebGetDebug (pHttp, CN_CONVERT_CXT_ID_TO_COMP_ID
                       (CN_DEFAULT_CONTEXT));
    WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                    (pHttp->i4HtmlSize - pHttp->i4Write));
    return;
}

/*********************************************************************
 *  Function Name : CnProcessCnDebugPageSet
 *  Description   : This function processes the request coming for CN
 *                  Debug Settings page.
 *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 *  Output(s)     : None.
 *  Return Values : None
 *********************************************************************/
PRIVATE VOID
CnProcessCnDebugPageSet (tHttp * pHttp)
{
    UINT4               u4TempContextId = CN_ZERO;
    UINT4               u4ErrorCode = CN_ZERO;
    INT4                i4Trc = CN_ZERO;
    INT4                i4Trap = CN_ZERO;

    STRCPY (pHttp->au1Name, "DCB_CN_CONTEXT");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);

    u4TempContextId = ATOI (pHttp->au1Value);

    STRCPY (pHttp->au1Name, "ACTION");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);

    if (STRCMP (pHttp->au1Value, "Display") == CN_ZERO)
    {
        CnUtilWebGetCxt (pHttp);
        if (nmhValidateIndexInstanceIeee8021CnGlobalTable
            (u4TempContextId) == SNMP_SUCCESS)
        {
            CnUtilWebGetDebug (pHttp, u4TempContextId);
            WebnmSockWrite (pHttp, (UINT1 *)
                            (pHttp->pi1Html + pHttp->i4Write),
                            (pHttp->i4HtmlSize - pHttp->i4Write));
        }
    }
    else
    {
        STRCPY (pHttp->au1Name, "TRC");
        if (HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                                pHttp->au1PostQuery) == ENM_SUCCESS)
        {
            i4Trc = (UINT4) ATOI (pHttp->au1Value);
        }
        STRCPY (pHttp->au1Name, "TRAP");
        if (HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                                pHttp->au1PostQuery) == ENM_SUCCESS)
        {
            i4Trap = (UINT4) ATOI (pHttp->au1Value);
        }
        if (nmhTestv2FsCnXGlobalTraceLevel (&u4ErrorCode,
                                            u4TempContextId,
                                            i4Trc) != SNMP_FAILURE)
        {
            nmhSetFsCnXGlobalTraceLevel (u4TempContextId, i4Trc);
        }
        if (nmhTestv2FsCnGlobalEnableTrap (&u4ErrorCode, i4Trap)
            != SNMP_FAILURE)
        {
            nmhSetFsCnGlobalEnableTrap (i4Trap);
        }
        CnUtilWebGetCxt (pHttp);
        CnUtilWebGetDebug (pHttp, u4TempContextId);
        WebnmSockWrite (pHttp, (UINT1 *)
                        (pHttp->pi1Html + pHttp->i4Write),
                        (pHttp->i4HtmlSize - pHttp->i4Write));
    }

    return;
}

/*********************************************************************
 *  Function Name : CnProcessCnDebugPageSet
 *  Description   : This function processes the request coming for CN
 *                  Debug Settings page.
 *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 *  Output(s)     : None.
 *  Return Values : None
 *********************************************************************/
PRIVATE VOID
CnUtilWebGetDebug (tHttp * pHttp, UINT4 u4CompId)
{
    INT4                i4TraceLevel = CN_ZERO;
    INT4                i4GlobalEnableTrap = CN_ZERO;

    nmhGetFsCnXGlobalTraceLevel (u4CompId, &i4TraceLevel);
    STRCPY (pHttp->au1KeyString, "TRC_KEY");
    WebnmSendString (pHttp, pHttp->au1KeyString);
    SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4TraceLevel);
    WebnmSockWrite (pHttp, pHttp->au1DataString, STRLEN (pHttp->au1DataString));
    nmhGetFsCnGlobalEnableTrap (&i4GlobalEnableTrap);
    STRCPY (pHttp->au1KeyString, "TRAP_KEY");
    WebnmSendString (pHttp, pHttp->au1KeyString);
    SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4GlobalEnableTrap);
    WebnmSockWrite (pHttp, pHttp->au1DataString, STRLEN (pHttp->au1DataString));
    return;
}

/*********************************************************************
 *  Function Name : CnUtilWebGetCxt
 *  Description   : This function prints all component  entries 
 *  Input(s)      : pHttp - pointer to tHttp
 *  Output(s)     : None.
 *  Return Values : None
 *********************************************************************/
PRIVATE VOID
CnUtilWebGetCxt (tHttp * pHttp)
{
    UINT4               u4ContextId = CN_ZERO;
    UINT4               u4NextContextId = CN_ZERO;
    CnPortVcmGetFirstVcId (&u4ContextId);

    STRCPY (pHttp->au1KeyString, "<! DCB_CN_COMP>");
    WebnmSendString (pHttp, pHttp->au1KeyString);

    SPRINTF ((CHR1 *) pHttp->au1DataString,
             "<option value = \"%d\">%d \n", u4ContextId, u4ContextId);
    WebnmSockWrite (pHttp, pHttp->au1DataString, STRLEN (pHttp->au1DataString));
    while (CnPortVcmGetNextVcId (u4ContextId, &u4NextContextId) == OSIX_SUCCESS)
    {
        STRCPY (pHttp->au1KeyString, "<! DCB_CN_COMP>");
        WebnmSendString (pHttp, pHttp->au1KeyString);

        SPRINTF ((CHR1 *) pHttp->au1DataString,
                 "<option value = \"%d\">%d \n", u4NextContextId,
                 u4NextContextId);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        STRLEN (pHttp->au1DataString));
        u4ContextId = u4NextContextId;
    }

}

/*********************************************************************
 *  Function Name : CnUtilWebGetCxtandPort
 *  Description   : This function prints all component  entries and
 *                  port entries
 *  Input(s)      : pHttp - pointer to tHttp 
 *  Output(s)     : None.
 *  Return Values : None
 *********************************************************************/
PRIVATE VOID
CnUtilWebGetCxtandPort (tHttp * pHttp)
{
    UINT4               u4ContextId = CN_ZERO;
    UINT4               u4NextContextId = CN_ZERO;
    UINT2               u2NextPort = CN_ZERO;
    UINT2               u2CurrPort = CN_ZERO;
    UINT1               au1IfName[CFA_MAX_PORT_NAME_LENGTH] = { CN_ZERO };
    INT1               *piIfName = NULL;

    MEMSET (au1IfName, CN_ZERO, CFA_MAX_PORT_NAME_LENGTH);
    piIfName = (INT1 *) &au1IfName[CN_ZERO];

    CnPortVcmGetFirstVcId (&u4ContextId);

    STRCPY (pHttp->au1KeyString, "<! DCB_CN_COMP>");
    WebnmSendString (pHttp, pHttp->au1KeyString);

    SPRINTF ((CHR1 *) pHttp->au1DataString,
             "<option value = \"%d\">%d \n", u4ContextId, u4ContextId);
    WebnmSockWrite (pHttp, pHttp->au1DataString, STRLEN (pHttp->au1DataString));
    while (CnPortVcmGetNextVcId (u4ContextId, &u4NextContextId) == OSIX_SUCCESS)
    {
        STRCPY (pHttp->au1KeyString, "<! DCB_CN_COMP>");
        WebnmSendString (pHttp, pHttp->au1KeyString);

        SPRINTF ((CHR1 *) pHttp->au1DataString,
                 "<option value = \"%d\">%d \n", u4NextContextId,
                 u4NextContextId);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        STRLEN (pHttp->au1DataString));
        u4ContextId = u4NextContextId;
    }
    while (L2IwfGetNextValidPort (u2CurrPort, &u2NextPort) != L2IWF_FAILURE)
    {

        STRCPY (pHttp->au1KeyString, "<! DCB_CN_PORT>");
        WebnmSendString (pHttp, pHttp->au1KeyString);

        CfaCliGetIfName ((UINT4) u2NextPort, piIfName);

        SPRINTF ((CHR1 *) pHttp->au1DataString,
                 "<option value =\"%d\">%s\n", u2NextPort, (CHR1 *) piIfName);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        STRLEN (pHttp->au1DataString));
        u2CurrPort = u2NextPort;
    }
    return;
}

/*********************************************************************
 *  Function Name : CnUtilWebGetCpTblEntry
 *  Description   : This function prints all entries in the CP
 *                  table
 *  Input(s)      : u4ContextId - context ID
 *                  u4CPIndex - CpIndex
 *                  i4CurrIf - Interface
 *  Output(s)     : None.
 *  Return Values : None
 *********************************************************************/
PRIVATE VOID
CnUtilWebGetCpTblEntry (tHttp * pHttp, UINT4 u4ContextId,
                        UINT4 u4CPIndex, INT4 i4CurrIf)
{
    UINT4               u4HeadOct = CN_ZERO;
    UINT4               u4CpQueSetPt = CN_ZERO;
    UINT4               u4MinSampBase = CN_ZERO;
    UINT4               u4CpPri = CN_ZERO;
    INT4                i4FeedBkWeight = CN_ZERO;
    INT4                i4Count = CN_ZERO;
    UINT1               au1CpId[CN_CPID_LEN + CN_ONE] = { CN_ZERO };
    UINT1               au1IfMac[CFA_MAX_MEDIA_ADDR_LEN] = { CN_ZERO };
    tMacAddr            CnCpMacAdd = { CN_ZERO };
    tSNMP_OCTET_STRING_TYPE CnCpIdentifier;

    MEMSET (&CnCpIdentifier, CN_ZERO, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1CpId, '\0', CN_CPID_LEN + 1);

    CnCpIdentifier.pu1_OctetList = au1CpId;
    CnCpIdentifier.i4_Length = CN_ZERO;

    nmhGetIeee8021CnCpPriority (u4ContextId, i4CurrIf, u4CPIndex, &u4CpPri);
    STRCPY (pHttp->au1KeyString, "COMPID_KEY");
    WebnmSendString (pHttp, pHttp->au1KeyString);
    SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", u4ContextId);
    WebnmSockWrite (pHttp, pHttp->au1DataString, STRLEN (pHttp->au1DataString));
    WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

    STRCPY (pHttp->au1KeyString, "PORTID_KEY");
    WebnmSendString (pHttp, pHttp->au1KeyString);
    SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4CurrIf);
    WebnmSockWrite (pHttp, pHttp->au1DataString, STRLEN (pHttp->au1DataString));
    WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

    STRCPY (pHttp->au1KeyString, "CPINDEX_KEY");
    WebnmSendString (pHttp, pHttp->au1KeyString);
    SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", u4CPIndex);
    WebnmSockWrite (pHttp, pHttp->au1DataString, STRLEN (pHttp->au1DataString));
    WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

    STRCPY (pHttp->au1KeyString, "CNCPPRI_KEY");
    WebnmSendString (pHttp, pHttp->au1KeyString);
    SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", u4CpPri);
    WebnmSockWrite (pHttp, pHttp->au1DataString, STRLEN (pHttp->au1DataString));
    WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);
    nmhGetIeee8021CnCpMacAddress (u4ContextId, i4CurrIf, u4CPIndex,
                                  &CnCpMacAdd);
    STRCPY (pHttp->au1KeyString, "MAC_ADDR_KEY");
    WebnmSendString (pHttp, pHttp->au1KeyString);
    PrintMacAddress (CnCpMacAdd, au1IfMac);
    SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", au1IfMac);
    WebnmSockWrite (pHttp, pHttp->au1DataString, STRLEN (pHttp->au1DataString));
    WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

    nmhGetIeee8021CnCpIdentifier (u4ContextId, i4CurrIf, u4CPIndex,
                                  &CnCpIdentifier);
    STRCPY (pHttp->au1KeyString, "CPID_KEY");
    WebnmSendString (pHttp, pHttp->au1KeyString);
    for (i4Count = CN_ZERO; i4Count < CN_CPID_LEN; i4Count++)
    {
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%02x", au1CpId[i4Count]);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        STRLEN (pHttp->au1DataString));

        if (i4Count != (CN_CPID_LEN - CN_ONE))
        {
            SPRINTF ((CHR1 *) pHttp->au1DataString, ":");
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            STRLEN (pHttp->au1DataString));

        }
    }
    WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

    nmhGetIeee8021CnCpQueueSizeSetPoint (u4ContextId, i4CurrIf,
                                         u4CPIndex, &u4CpQueSetPt);

    STRCPY (pHttp->au1KeyString, "QUEUE_SET_PT_KEY");
    WebnmSendString (pHttp, pHttp->au1KeyString);
    SPRINTF ((CHR1 *) pHttp->au1DataString, "%u", u4CpQueSetPt);
    WebnmSockWrite (pHttp, pHttp->au1DataString, STRLEN (pHttp->au1DataString));
    WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

    nmhGetIeee8021CnCpFeedbackWeight (u4ContextId, i4CurrIf,
                                      u4CPIndex, &i4FeedBkWeight);
    STRCPY (pHttp->au1KeyString, "FEEDBK_WEIGHT_KEY");
    WebnmSendString (pHttp, pHttp->au1KeyString);

    SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4FeedBkWeight);
    WebnmSockWrite (pHttp, pHttp->au1DataString, STRLEN (pHttp->au1DataString));
    WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

    nmhGetIeee8021CnCpMinSampleBase (u4ContextId, i4CurrIf,
                                     u4CPIndex, &u4MinSampBase);
    STRCPY (pHttp->au1KeyString, "MIN_SAM_BASE_KEY");
    WebnmSendString (pHttp, pHttp->au1KeyString);
    SPRINTF ((CHR1 *) pHttp->au1DataString, "%u", u4MinSampBase);
    WebnmSockWrite (pHttp, pHttp->au1DataString, STRLEN (pHttp->au1DataString));
    WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);
    nmhGetIeee8021CnCpMinHeaderOctets (u4ContextId, i4CurrIf,
                                       u4CPIndex, &u4HeadOct);
    STRCPY (pHttp->au1KeyString, "HEADER_OCTETS_KEY");
    WebnmSendString (pHttp, pHttp->au1KeyString);
    SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", u4HeadOct);
    WebnmSockWrite (pHttp, pHttp->au1DataString, STRLEN (pHttp->au1DataString));
    WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);
    WebnmSendString (pHttp, ISS_TABLE_STOP_FLAG);
    return;
}

/*********************************************************************
 *  Function Name : CnUtilWebGetStatistics
 *  Description   : This function prints all entries in the statistics 
 *                  page
 *  Input(s)      : u4ContextId - context ID
 *                  u4CurrPri - Priority
 *                  i4CurrIf - Interface
 *  Output(s)     : None.
 *  Return Values : None
 *********************************************************************/
PRIVATE VOID
CnUtilWebGetStatistics (tHttp * pHttp, UINT4 u4ContextId,
                        UINT4 u4CurrPri, INT4 i4CurrIf)
{
    FS_UINT8            u8DisFrames;
    FS_UINT8            u8TransFrames;
    FS_UINT8            u8TransCnms;
    UINT4               u4CpIndex = CN_ZERO;
    tSNMP_COUNTER64_TYPE DisFrames;
    tSNMP_COUNTER64_TYPE TransFrames;
    tSNMP_COUNTER64_TYPE TransCnms;
    UINT1               au1DisFrames[CFA_CLI_U8_STR_LENGTH] = { CN_ZERO };
    UINT1               au1TransFrames[CFA_CLI_U8_STR_LENGTH] = { CN_ZERO };
    UINT1               au1TransCnms[CFA_CLI_U8_STR_LENGTH] = { CN_ZERO };

    MEMSET (&DisFrames, CN_ZERO, sizeof (tSNMP_COUNTER64_TYPE));
    MEMSET (&TransFrames, CN_ZERO, sizeof (tSNMP_COUNTER64_TYPE));
    MEMSET (&TransCnms, CN_ZERO, sizeof (tSNMP_COUNTER64_TYPE));

    u4CpIndex = CN_CPINDEX_FROM_PRIO (u4CurrPri);

    nmhGetIeee8021CnCpDiscardedFrames (u4ContextId, i4CurrIf, u4CpIndex,
                                       &DisFrames);
    nmhGetIeee8021CnCpTransmittedFrames (u4ContextId, i4CurrIf, u4CpIndex,
                                         &TransFrames);
    nmhGetIeee8021CnCpTransmittedCnms (u4ContextId, i4CurrIf, u4CpIndex,
                                       &TransCnms);
    FSAP_U8_ASSIGN_HI (&u8DisFrames, DisFrames.msn);
    FSAP_U8_ASSIGN_LO (&u8DisFrames, DisFrames.lsn);
    FSAP_U8_ASSIGN_HI (&u8TransFrames, TransFrames.msn);
    FSAP_U8_ASSIGN_LO (&u8TransFrames, TransFrames.lsn);
    FSAP_U8_ASSIGN_HI (&u8TransCnms, TransCnms.msn);
    FSAP_U8_ASSIGN_LO (&u8TransCnms, TransCnms.lsn);
    FSAP_U8_2STR (&u8DisFrames, (CHR1 *) au1DisFrames);
    FSAP_U8_2STR (&u8TransFrames, (CHR1 *) au1TransFrames);
    FSAP_U8_2STR (&u8TransCnms, (CHR1 *) au1TransCnms);

    STRCPY (pHttp->au1KeyString, "COMPID_KEY");
    WebnmSendString (pHttp, pHttp->au1KeyString);
    SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", u4ContextId);
    WebnmSockWrite (pHttp, pHttp->au1DataString, STRLEN (pHttp->au1DataString));
    WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

    STRCPY (pHttp->au1KeyString, "PORTID_KEY");
    WebnmSendString (pHttp, pHttp->au1KeyString);
    SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4CurrIf);
    WebnmSockWrite (pHttp, pHttp->au1DataString, STRLEN (pHttp->au1DataString));
    WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

    STRCPY (pHttp->au1KeyString, "CNPV_KEY");
    WebnmSendString (pHttp, pHttp->au1KeyString);
    SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", u4CurrPri);
    WebnmSockWrite (pHttp, pHttp->au1DataString, STRLEN (pHttp->au1DataString));
    WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

    STRCPY (pHttp->au1KeyString, "TRANS_CNM_KEY");
    WebnmSendString (pHttp, pHttp->au1KeyString);
    SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", au1TransCnms);
    WebnmSockWrite (pHttp, pHttp->au1DataString, STRLEN (pHttp->au1DataString));
    WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

    STRCPY (pHttp->au1KeyString, "TRANS_FRAME_KEY");
    WebnmSendString (pHttp, pHttp->au1KeyString);
    SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", au1TransFrames);
    WebnmSockWrite (pHttp, pHttp->au1DataString, STRLEN (pHttp->au1DataString));
    WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

    STRCPY (pHttp->au1KeyString, "DISCARD_FRAME_KEY");
    WebnmSendString (pHttp, pHttp->au1KeyString);
    SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", au1DisFrames);
    WebnmSockWrite (pHttp, pHttp->au1DataString, STRLEN (pHttp->au1DataString));
    WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);
    WebnmSendString (pHttp, ISS_TABLE_STOP_FLAG);
    return;
}

/*********************************************************************
 *  Function Name : CnUtilWebGetPortPriEntry
 *  Description   : This function prints all entries in the port priority 
 *                  table
 *  Input(s)      : u4ContextId - context ID
 *                  u4CurrPri - Priority
 *                  i4CurrIf - Interface
 *  Output(s)     : None.
 *  Return Values : None
 *********************************************************************/
PRIVATE VOID
CnUtilWebGetPortPriEntry (tHttp * pHttp, UINT4 u4ContextId,
                          UINT4 u4CurrPri, INT4 i4CurrIf)
{
    UINT4               u4AltPri = CN_ZERO;
    UINT4               u4RecTime = CN_ZERO;
    UINT4               u4SentTime = CN_ZERO;
    UINT4               u4LldpInsSel = CN_ZERO;
    INT4                i4DefModeCh = CN_ZERO;
    INT4                i4DefMode = CN_ZERO;
    INT4                i4LldpInsCh = CN_ZERO;
    INT4                i4ErroedPort = CN_ZERO;
    UINT1               u1RecEvent = CN_ZERO;
    UINT1               u1SentEvent = CN_ZERO;
    UINT1               au1TimeStr[MAX_DATE_LEN] = { CN_ZERO };
    tCnPortPriTblInfo  *pCnPortPriTbl = NULL;

    nmhGetIeee8021CnPortPriDefModeChoice (u4ContextId, u4CurrPri, i4CurrIf,
                                          &i4DefModeCh);

    STRCPY (pHttp->au1KeyString, "COMPID_KEY");
    WebnmSendString (pHttp, pHttp->au1KeyString);
    SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", u4ContextId);
    WebnmSockWrite (pHttp, pHttp->au1DataString, STRLEN (pHttp->au1DataString));
    WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

    STRCPY (pHttp->au1KeyString, "PORTID_KEY");
    WebnmSendString (pHttp, pHttp->au1KeyString);
    SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4CurrIf);
    WebnmSockWrite (pHttp, pHttp->au1DataString, STRLEN (pHttp->au1DataString));
    WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

    STRCPY (pHttp->au1KeyString, "CNPV_KEY");
    WebnmSendString (pHttp, pHttp->au1KeyString);
    SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", u4CurrPri);
    WebnmSockWrite (pHttp, pHttp->au1DataString, STRLEN (pHttp->au1DataString));
    WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

    STRCPY (pHttp->au1KeyString, "DEFENSE_MOD_CH_KEY");
    WebnmSendString (pHttp, pHttp->au1KeyString);
    SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4DefModeCh);
    WebnmSockWrite (pHttp, pHttp->au1DataString, STRLEN (pHttp->au1DataString));
    WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

    nmhGetIeee8021CnPortPriAutoDefenseMode (u4ContextId, u4CurrPri,
                                            i4CurrIf, &i4DefMode);
    STRCPY (pHttp->au1KeyString, "OPER_DEFENSE_MOD_KEY");
    WebnmSendString (pHttp, pHttp->au1KeyString);
    if (i4DefMode == CN_DISABLED)
    {
        SPRINTF ((CHR1 *) pHttp->au1DataString, "DISABLED");
    }
    else if (i4DefMode == CN_EDGE)
    {
        SPRINTF ((CHR1 *) pHttp->au1DataString, "EDGE");
    }
    else if (i4DefMode == CN_INTERIOR)
    {
        SPRINTF ((CHR1 *) pHttp->au1DataString, "INTERIOR");
    }
    else
    {
        SPRINTF ((CHR1 *) pHttp->au1DataString, "INTERIOR READY");
    }
    WebnmSockWrite (pHttp, pHttp->au1DataString, STRLEN (pHttp->au1DataString));
    WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

    nmhGetFsCnXPortPriOperAltPri (u4ContextId, u4CurrPri, i4CurrIf, &u4AltPri);
    STRCPY (pHttp->au1KeyString, "OPER_ALTPRI_KEY");
    WebnmSendString (pHttp, pHttp->au1KeyString);
    SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", u4AltPri);
    WebnmSockWrite (pHttp, pHttp->au1DataString, STRLEN (pHttp->au1DataString));
    WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

    nmhGetIeee8021CnPortPriAlternatePriority (u4ContextId, u4CurrPri,
                                              i4CurrIf, &u4AltPri);
    STRCPY (pHttp->au1KeyString, "ALTPRI_KEY");
    WebnmSendString (pHttp, pHttp->au1KeyString);
    SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", u4AltPri);
    WebnmSockWrite (pHttp, pHttp->au1DataString, STRLEN (pHttp->au1DataString));
    WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);
    nmhGetFsCnXPortPriErrorEntry (u4ContextId, u4CurrPri,
                                  i4CurrIf, &i4ErroedPort);

    STRCPY (pHttp->au1KeyString, "CONFLICT_STATUS_KEY");
    WebnmSendString (pHttp, pHttp->au1KeyString);

    if (i4ErroedPort == OSIX_TRUE)
    {
        SPRINTF ((CHR1 *) pHttp->au1DataString, "TRUE");
    }
    else
    {
        SPRINTF ((CHR1 *) pHttp->au1DataString, "FALSE");
    }
    WebnmSockWrite (pHttp, pHttp->au1DataString, STRLEN (pHttp->au1DataString));
    WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);
    nmhGetIeee8021CnPortPriLldpInstanceChoice (u4ContextId,
                                               u4CurrPri, i4CurrIf,
                                               &i4LldpInsCh);
    STRCPY (pHttp->au1KeyString, "LLDP_INS_CH_KEY");
    WebnmSendString (pHttp, pHttp->au1KeyString);
    SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4LldpInsCh);
    WebnmSockWrite (pHttp, pHttp->au1DataString, STRLEN (pHttp->au1DataString));
    WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);
    nmhGetIeee8021CnPortPriLldpInstanceSelector (u4ContextId,
                                                 u4CurrPri, i4CurrIf,
                                                 &u4LldpInsSel);
    STRCPY (pHttp->au1KeyString, "LLDP_INS_SELECTOR_KEY");
    WebnmSendString (pHttp, pHttp->au1KeyString);
    SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", u4LldpInsSel);
    WebnmSockWrite (pHttp, pHttp->au1DataString, STRLEN (pHttp->au1DataString));
    WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

    /*directly accessing the port priority structure since
     *nmh get routines will return a string*/

    CN_PORT_PRI_ENTRY (u4ContextId, i4CurrIf, u4CurrPri, pCnPortPriTbl);
    if (pCnPortPriTbl != NULL)
    {
        u1RecEvent = pCnPortPriTbl->u1LastRcvdEvent;
        STRCPY (pHttp->au1KeyString, "LAST_REC_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", u1RecEvent);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

    }
    nmhGetFsCnXPortPriLastRcvdEventTime (u4ContextId,
                                         u4CurrPri, i4CurrIf, &u4RecTime);

    STRCPY (pHttp->au1KeyString, "LAST_REC_EVE_TIME_KEY");
    WebnmSendString (pHttp, pHttp->au1KeyString);
    UtlGetTimeStrForTicks (u4RecTime, (CHR1 *) au1TimeStr);
    SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", au1TimeStr);
    WebnmSockWrite (pHttp, pHttp->au1DataString, STRLEN (pHttp->au1DataString));
    WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

    /*directly accessing the port priority structure since
     *nmh get routines will return a string*/

    CN_PORT_PRI_ENTRY (u4ContextId, i4CurrIf, u4CurrPri, pCnPortPriTbl);
    if (pCnPortPriTbl != NULL)
    {
        u1SentEvent = pCnPortPriTbl->u1LastSentEvent;
        STRCPY (pHttp->au1KeyString, "LAST_SENT_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", u1SentEvent);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);
    }
    nmhGetFsCnXPortPriLastSentEventTime (u4ContextId,
                                         u4CurrPri, i4CurrIf, &u4SentTime);

    STRCPY (pHttp->au1KeyString, "LAST_SENT_EVE_TIME_KEY");
    WebnmSendString (pHttp, pHttp->au1KeyString);
    UtlGetTimeStrForTicks (u4SentTime, (CHR1 *) au1TimeStr);
    SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", au1TimeStr);
    WebnmSockWrite (pHttp, pHttp->au1DataString, STRLEN (pHttp->au1DataString));
    WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

    WebnmSendString (pHttp, ISS_TABLE_STOP_FLAG);
    return;
}
#endif /* WEBNM_WANTED */
