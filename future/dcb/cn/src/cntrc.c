/*****************************************************************************
 * Copyright (C) 2010 Aricent Inc . All Rights Reserved
 *
 * $Id: cntrc.c,v 1.4 2010/08/17 11:06:21 prabuc Exp $
 *
 * Description: This file contains debugging related functions
 *****************************************************************************/
#include "cninc.h"

/****************************************************************************
 *                                                                           
 * Function     : CnTrace                                                 
 *                                                                           
 * Description  : converts variable argument in to string send to print
 *                function  
 *                                                                           
 * Input        : u4CompId - Context Identifier 
 *                u4TraceType  - Trace flag 
 *                fmt  - format strong, variable argument
 *                                                                           
 * Output       : None                                                       
 *                                                                           
 * Returns      : NONE                                             
 *                                                                           
 *****************************************************************************/
PUBLIC VOID
CnTrace (UINT4 u4CompId, UINT4 TraceType, const char *fmt, ...)
{
    static CHR1         ac1ModuleString[CN_MAX_TRC_LEN] = { CN_ZERO };
    va_list             ap;
    UINT4               u4OffSet = CN_ZERO;
    UNUSED_PARAM (TraceType);

    if (CN_IS_VALID_COMP_ID (u4CompId) == OSIX_FALSE)
    {
        SNPRINTF (ac1ModuleString, sizeof (ac1ModuleString), "CN: ");

    }
    else
    {
        SNPRINTF (ac1ModuleString, sizeof (ac1ModuleString),
                  "CN[CXT-ID: %u] : ", CN_CONVERT_COMP_ID_TO_CXT_ID (u4CompId));
    }

    u4OffSet = STRLEN (ac1ModuleString);
    va_start (ap, fmt);
    vsprintf (&ac1ModuleString[u4OffSet], fmt, ap);
    va_end (ap);

    UtlTrcPrint ((const char *) ac1ModuleString);

    return;
}

/****************************************************************************
 *
 * Function     : CnTlvDump
 *
 * Description  : converts variable argument in to string send to print
 *                function
 *
 * Input        : u4Comp - Context Identifier
 *                u4TraceType  - Trace flag
 *                pBuf - Packet Buffer
 *                u1len - Length of TLV
 *                
 *
 * Output       : None
 *
 * Returns      : NONE
 *
 *****************************************************************************/
PUBLIC VOID
CnTlvDump (UINT4 u4Comp, UINT4 u4TraceType, UINT1 *pBuf, UINT1 u1Len)
{
    static CHR1         ac1ModuleString[CN_MAX_TRC_LEN] = { CN_ZERO };
    UINT1               u1Count = CN_ZERO;

    UNUSED_PARAM (u4TraceType);
    UNUSED_PARAM (u4Comp);

    while (u1Count < u1Len)
    {
        SPRINTF ((CHR1 *) ac1ModuleString, "%02x ", pBuf[u1Count]);
        UtlTrcPrint (ac1ModuleString);
        u1Count++;
        if ((u1Count % CN_TRC_MAX_CHRS_PER_LINE) == CN_ZERO)
        {
            UtlTrcPrint ("\r\n");
        }
    }

    UtlTrcPrint ("\r\n");
    return;
}

/*-----------------------------------------------------------------------*/
/*                       End of the file  cntrc.c                        */
/*-----------------------------------------------------------------------*/
