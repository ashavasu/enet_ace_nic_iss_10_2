/*************************************************************************/
/*  Copyright (C) 2010 Aricent Inc. All Rights Reserved.                 */
/*                                                                       */
/*  $Id: cncli.c,v 1.17 2014/08/25 12:37:54 siva Exp $                                                               */
/*                                                                       */
/*  Description : This file contains functions to process CLI commands   */
/*                in CN module.                                          */
/*****************************i*******************************************/
#ifndef CN_CLI_C
#define CN_CLI_C

#include "cninc.h"
#include "cncli.h"
#include "fscncli.h"
#include "stdcncli.h"

PRIVATE INT4        CnCliSetSysCtrl
PROTO ((tCliHandle CliHandle, INT4 i4ModuleStatus));
PRIVATE INT4        CnCliSetCn
PROTO ((tCliHandle CliHandle, UINT4 u4CompId, INT4 i4Status));
PRIVATE INT4        CnCliSetCnm
PROTO ((tCliHandle CliHandle, UINT4 u4CompId, UINT4 u4cnmpri));
PRIVATE INT4        CnCliSetCnpv
PROTO ((tCliHandle CliHandle, UINT4 u4CompId, UINT4 u4priority));
PRIVATE INT4        CnCliDeleteCnpv
PROTO ((tCliHandle CliHandle, UINT4 u4CompId, UINT4 u4priority));
PRIVATE INT4        CnCliSetDefChoice
PROTO ((tCliHandle CliHandle, UINT4 u4CompId, UINT4 u4Pri, UINT1 u1DefCh));
PRIVATE INT4        CnCliSetAltPri
PROTO ((tCliHandle CliHandle, UINT4 u4CompId, UINT4 u4Pri, UINT4 u4AltPri));
PRIVATE INT4        CnCliSetDefMode
PROTO ((tCliHandle CliHandle, UINT4 u4CompId, UINT4 u4Pri, UINT1 u1DefMode));
PRIVATE INT4        CnCliSetPortDefChoice
PROTO ((tCliHandle CliHandle, UINT4 u4CompId, UINT4 u4Pri,
        UINT1 u1PortDefaultDefModeCh));
PRIVATE INT4        CnCliSetLldpChoice
PROTO ((tCliHandle CliHandle, UINT4 u4CompId, UINT4 u4Pri, INT4 i4InsChoice));
PRIVATE INT4        CnCliSetlldpSelector
PROTO ((tCliHandle CliHandle, UINT4 u4CompId, UINT4 u4Pri, UINT4 u4Selector));
PRIVATE INT4        CncliResetCounter
PROTO ((tCliHandle CliHandle, UINT4 u4PortId, UINT1 *pu1ContextName));
PRIVATE INT4        CnCliDebug
PROTO ((tCliHandle CliHandle, INT4 i4TrcOpt, UINT1 *pu1Context, UINT1 u1Flag));
PRIVATE INT4        CnCliSetNotify
PROTO ((tCliHandle CliHandle, INT4 i4Type, UINT1 u1Flag));
PRIVATE INT4        CnCliPortDefModeCh
PROTO ((tCliHandle CliHandle, UINT4 u4CompId, INT4 i4IfIndex, UINT4 u4CNPV,
        INT4 i4DefCh));
PRIVATE INT4        CnCliPortDefMode
PROTO ((tCliHandle CliHandle, UINT4 u4CompId, INT4 i4IfIndex, UINT4 u4CNPV,
        INT4 i4DefMode));
PRIVATE INT4        CnCliSetPortLldpChoice
PROTO ((tCliHandle CliHandle, UINT4 u4CompId, INT4 i4IfIndex, UINT4 u4Pri,
        INT4 i4InsChoice));
PRIVATE INT4        CnCliSetPortLldpSelector
PROTO ((tCliHandle CliHandle, UINT4 u4CompId, INT4 i4IfIndex, UINT4 u4Pri,
        UINT4 u4Selector));
PRIVATE INT4        CnCliSetPortAltPri
PROTO ((tCliHandle CliHandle, UINT4 u4CompId, INT4 i4IfIndex, UINT4 u4Pri,
        UINT4 u4AltPri));
PRIVATE INT4        CnCliSetCpParam
PROTO ((tCliHandle CliHandle, INT4 i4IfIndex, UINT4 u4Pri,
        UINT4 *pu4QueSetPt, INT4 *pi4FeedBkWeight, UINT4 *pu4MinSamBase,
        UINT4 *pu4HeadOct));
PRIVATE INT4        CnCliShowCnpv
PROTO ((tCliHandle CliHandle, UINT4 u4Ifnum, UINT1 *pu1CxtName,
        UINT4 *pu4Cnpv));

PRIVATE INT4
    CnCliShowCompGlobInfo PROTO ((tCliHandle CliHandle, UINT4 u4CompId));
PRIVATE INT4 CnCliShowAllGlobInfo PROTO ((tCliHandle CliHandle));
PRIVATE INT4
    CnCliShowGlobInfo PROTO ((tCliHandle CliHandle, UINT1 *pu1ContextName));
PRIVATE INT4
     
     
     
     CnCliShowIfaceCounters
PROTO ((tCliHandle CliHandle, UINT4 u4IfIndex, UINT4 *pu4CNPV));
PRIVATE INT4        CnCliShowCnpvCounters
PROTO ((tCliHandle CliHandle, UINT4 u4CompId, UINT4 u4PortId));
PRIVATE INT4        CnCliShowIfCounters
PROTO ((tCliHandle CliHandle, UINT4 u4Priority));
PRIVATE INT4 CnCliShowAllIfCnpvCounters PROTO ((tCliHandle CliHandle));
PRIVATE INT4
    CnCliShowIfCnpvCounters PROTO ((tCliHandle CliHandle,
                                    UINT4 u4CompId,
                                    UINT4 u4PortId, UINT4 u4CNPV));
PRIVATE INT4 CnCliShowCpParam PROTO ((tCliHandle CliHandle, UINT4 u4PortId,
                                      UINT1 *pu1CxtName, UINT4 *pu4Priority));
PRIVATE VOID        CnCliShowCnCxtCnpvCpProp
PROTO ((tCliHandle CliHandle, UINT4 u4CompId, UINT4 u4Priority));
PRIVATE VOID        CnCliShowCnCxtCpProp
PROTO ((tCliHandle CliHandle, UINT4 u4CompId));
PRIVATE VOID        CnCliShowCnIfCpProp
PROTO ((tCliHandle CliHandle, UINT4 u4CompId, UINT4 u4PortId));
PRIVATE VOID        CnCliShowCnCnpvCpProp
PROTO ((tCliHandle CliHandle, UINT4 u4Priority));
PRIVATE VOID CnCliShowCnAllCpProp PROTO ((tCliHandle CliHandle));
PRIVATE VOID CnCliShowCnCpProp PROTO ((tCliHandle CliHandle, UINT4 u4CompId,
                                       UINT4 u4PortId, UINT4 u4Priority));
PRIVATE VOID CnCliShowCnCxtCnpv PROTO ((tCliHandle CliHandle, UINT4 u4CompId,
                                        UINT4 u4Priority));
PRIVATE VOID CnCliShowCxtCnpv PROTO ((tCliHandle CliHandle, UINT4 u4CompId));
PRIVATE INT4 CnCliShowCnIfCnpv PROTO ((tCliHandle CliHandle, UINT4 u4PortId,
                                       UINT4 u4Priority));
PRIVATE VOID CnCliShowPriCnpv PROTO ((tCliHandle CliHandle, UINT4 u4Priority));
PRIVATE INT4 CnCliShowIfCnpv PROTO ((tCliHandle CliHandle, UINT4 u4PortId));
PRIVATE VOID CnCliShowAllCnpv PROTO ((tCliHandle CliHandle));
PRIVATE VOID CnCliShowCnCnpv PROTO ((tCliHandle CliHandle, UINT4 u4PortId,
                                     UINT4 u4CompId, UINT4 u4Priority));
PRIVATE VOID
     
     
     
     CnCliShowRunningConfigInterface
PROTO ((tCliHandle CliHandle, UINT4 u4CompId));
INT4 CnCliShowTrace PROTO ((tCliHandle CliHandle, UINT4 u4CompId));
PRIVATE VOID
     
     
     
     CnCliShowRunningConfigScalars
PROTO ((tCliHandle CliHandle, UINT4 u4CompId));

PRIVATE INT4
     
     
     
     CnCliShowCompCnpv
PROTO ((tCliHandle CliHandle, UINT1 *pu1CxtName, UINT4 *pu4Priority));
PRIVATE VOID        CnCliShowCnCompCnpv
PROTO ((tCliHandle CliHandle, UINT4 u4CompId, UINT4 u4Priority));
/*****************************************************************************/
/*                                                                           */
/*  FUNCTION NAME   : cli_process_cn_cmd                                     */
/*                                                                           */
/* Description      : This function is Classify the Given Command and        */
/*                    configure the CN module through CLI using set of       */
/*                    nmh routines.                                          */
/*                                                                           */
/*  INPUT           : CliHandle - CliContext ID                              */
/*                    u4Command - Command identifier                         */
/*                     ...       - Variable command argument list            */
/*                                                                           */
/*  OUTPUT          : None                                                   */
/*                                                                           */
/*  RETURNS         : CLI_SUCCESS/CLI_FAILURE                                */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
cli_process_cn_cmd (tCliHandle CliHandle, UINT4 u4Command, ...)
{
    va_list             ap;

    UINT4              *apu4Args[CN_CLI_MAX_ARGS] = { CN_ZERO };
    UINT4               u4IfIndex = CN_ZERO;
    UINT4               u4ErrorCode = SNMP_ERR_NO_ERROR;
    INT1                i1ArgNo = CN_ZERO;
    INT4                i4RetStatus = CN_ZERO;
    INT4                i4IfIndex = CN_ZERO;
    UINT4               u4CompId = CN_DEFAULT_CONTEXT;
    UINT4               u4TempCompId = CN_ZERO;
    INT4                i4Status = CN_ZERO;
    UINT4               u4TempIfIndex = CN_ZERO;
    UINT2               u2LocalPortId = CN_ZERO;
    UINT1              *pu1FeedBkWgt = NULL;
    INT4               *pi4FeedBkWgt = NULL;
    INT4                i4FeedBkWgt = CN_ZERO;

    MEMSET (apu4Args, CN_ZERO, CN_CLI_MAX_ARGS);
    nmhGetFsCnSystemControl (&i4Status);

    /* If CN module is shutdown, only shut/no-shut command
     * is allowed */
    if ((u4Command != CLI_CN_SYS_CTRL) && (i4Status == CN_SHUTDOWN))
    {
        CliPrintf (CliHandle, " CN is Shutdown \r\n");
        return CLI_FAILURE;
    }

    va_start (ap, u4Command);

    /* Third argument is always interface name/index */
    u4IfIndex = va_arg (ap, UINT4);

    /* Walk through the rest of the arguments and store in apu4Args array. */
    while (CN_ONE)
    {
        apu4Args[i1ArgNo++] = va_arg (ap, UINT4 *);

        if (i1ArgNo == CN_CLI_MAX_ARGS)
        {
            break;
        }

    }
    va_end (ap);

    /* Register Lock with CLI */
    /*Check the Lock and Unlock function names */
    CliRegisterLock (CliHandle, CnLock, CnUnLock);
    CnLock ();

    u4TempCompId = CLI_GET_CXT_ID ();
    if (u4TempCompId != CN_INVALID_CONTEXT)
    {
        u4CompId = CN_CONVERT_CXT_ID_TO_COMP_ID (u4TempCompId);
    }
    else
    {
        i4IfIndex = CLI_GET_IFINDEX ();
        if (i4IfIndex >= CN_MIN_PORTS)
        {
            u4TempIfIndex = i4IfIndex;
            if (CnPortVcmGetCxtInfoFromIfIndex (u4TempIfIndex, &u4CompId,
                                                &u2LocalPortId) == OSIX_FAILURE)
            {
                CnUnLock ();
                CliUnRegisterLock (CliHandle);
                return CLI_FAILURE;
            }
        }
    }
    switch (u4Command)
    {
        case CLI_CN_SYS_CTRL:
            /*apu4Args[CN_ZERO] = Start/Shutdown */
            i4RetStatus = CnCliSetSysCtrl (CliHandle,
                                           CLI_PTR_TO_I4 ((apu4Args[CN_ZERO])));
            break;

        case CLI_CN_SET_CN:
            /* apu4Args[CN_ZERO] = Enable/Disable */
            i4RetStatus = CnCliSetCn (CliHandle, u4CompId,
                                      CLI_PTR_TO_I4 ((apu4Args[CN_ZERO])));
            break;

        case CLI_CN_SET_CNM_PRIO:
            /* apu4Args[CN_ZERO] = CnmTransmitPriority */
            i4RetStatus = CnCliSetCnm (CliHandle, u4CompId,
                                       (*(apu4Args[CN_ZERO])));
            break;

        case CLI_CN_SET_CNPV:
            /*apu4Args[CN_ZERO] = CNPV */
            i4RetStatus = CnCliSetCnpv (CliHandle, u4CompId,
                                        (*(apu4Args[CN_ZERO])));
            break;

        case CLI_CN_NO_CNPV:
            /*apu4Args[CN_ZERO] = CNPV */
            i4RetStatus =
                CnCliDeleteCnpv (CliHandle, u4CompId, (*(apu4Args[CN_ZERO])));
            break;

        case CLI_CN_SET_DEF_CHOICE:
            /*apu4Args[CN_ZERO] = CNPV
             * apu4Args[CN_ONE] = Defense mode choice */
            i4RetStatus = CnCliSetDefChoice (CliHandle, u4CompId,
                                             (*(apu4Args[CN_ZERO])),
                                             (UINT1)
                                             CLI_PTR_TO_U4 (apu4Args[CN_ONE]));
            break;

        case CLI_CN_SET_ALT_PRI:
            /*apu4Args[CN_ZERO] = CNPV
             *apu4Args[CN_ONE] = Alternate Priority */
            i4RetStatus =
                CnCliSetAltPri (CliHandle, u4CompId, (*(apu4Args[CN_ZERO])),
                                (*(apu4Args[CN_ONE])));
            break;

        case CLI_CN_SET_DEF_MODE:
            /*apu4Args[CN_ZERO] = CNPV
             * apu4Args[CN_ONE] = Defense Mode */
            i4RetStatus = CnCliSetDefMode (CliHandle, u4CompId,
                                           (*(apu4Args[CN_ZERO])),
                                           (UINT1)
                                           CLI_PTR_TO_U4 (apu4Args[CN_ONE]));
            break;

        case CLI_CN_SET_PORT_DEF_CH:
            /* apu4Args[CN_ZERO] = CNPV
             * apu4Args[CN_ONE] = Port Default Defense Mode Choice */
            i4RetStatus = CnCliSetPortDefChoice (CliHandle, u4CompId,
                                                 (*(apu4Args[CN_ZERO])),
                                                 (UINT1) CLI_PTR_TO_U4
                                                 (apu4Args[CN_ONE]));
            break;

        case CLI_CN_SET_LLDP_CHOICE:
            /* apu4Args[CN_ZERO] = CNPV
             * apu4Args[CN_ONE] = LLDP_INST_CHOICE */
            i4RetStatus = CnCliSetLldpChoice (CliHandle, u4CompId,
                                              (*(apu4Args[CN_ZERO])),
                                              CLI_PTR_TO_I4 (apu4Args[CN_ONE]));
            break;

        case CLI_CN_SET_LLDP_SELECTOR:
            /* apu4Args[CN_ZERO] = CNPV
             * apu4Args[CN_ONE] = LLDP_INST_SELECTOR */
            i4RetStatus = CnCliSetlldpSelector (CliHandle, u4CompId,
                                                (*(apu4Args[CN_ZERO])),
                                                (*(apu4Args[CN_ONE])));
            break;

        case CLI_CN_SET_NOTIFY:
            /* apu4Args[CN_ZERO] = Value for TrapFlag */
            i4RetStatus = CnCliSetNotify (CliHandle,
                                          CLI_PTR_TO_I4 (apu4Args[CN_ZERO]),
                                          CLI_CN_SET_NOTIFY);
            break;

        case CLI_CN_NO_NOTIFY:
            /* apu4Args[CN_ZERO] = Value for TrapFlag */
            i4RetStatus = CnCliSetNotify (CliHandle,
                                          CLI_PTR_TO_I4 (apu4Args[CN_ZERO]),
                                          CLI_CN_NO_NOTIFY);
            break;

        case CLI_CN_PORT_SET_DEF_CH:
            /* apu4Args[CN_ZERO] = CNPV
             * apu4Args[CN_ONE] = PortPriDefModeChoice */
            i4RetStatus = CnCliPortDefModeCh (CliHandle, u4CompId,
                                              i4IfIndex,
                                              (*(apu4Args[CN_ZERO])),
                                              CLI_PTR_TO_I4 (apu4Args[CN_ONE]));
            break;

        case CLI_CN_PORT_DEF_MODE:
            /* apu4Args[CN_ZERO] = CNPV
             * apu4Args[CN_ONE] = PortPriDefMode */
            i4RetStatus = CnCliPortDefMode (CliHandle, u4CompId, i4IfIndex,
                                            (*(apu4Args[CN_ZERO])),
                                            CLI_PTR_TO_I4 (apu4Args[CN_ONE]));
            break;

        case CLI_CN_PORT_LLDP_CH:
            /* apu4Args[CN_ZERO] = CNPV
             * apu4Args[CN_ONE] = PortPriLldpInstanceChoice*/
            i4RetStatus = CnCliSetPortLldpChoice (CliHandle, u4CompId,
                                                  i4IfIndex,
                                                  (*(apu4Args[CN_ZERO])),
                                                  CLI_PTR_TO_I4 (apu4Args
                                                                 [CN_ONE]));
            break;

        case CLI_CN_PORT_LLDP_SELECTOR:
            /* apu4Args[CN_ZERO] = CNPV
             * apu4Args[CN_ONE] = PortPriLldpInstanceSelector */
            i4RetStatus = CnCliSetPortLldpSelector (CliHandle, u4CompId,
                                                    i4IfIndex,
                                                    (*(apu4Args[CN_ZERO])),
                                                    (*(apu4Args[CN_ONE])));
            break;

        case CLI_CN_PORT_ALT_PRI:
            /* apu4Args[CN_ZERO] = CNPV
             * apu4Args[CN_ONE] = PortPriAltPri */
            i4RetStatus = CnCliSetPortAltPri (CliHandle, u4CompId,
                                              i4IfIndex,
                                              (*(apu4Args[CN_ZERO])),
                                              (*(apu4Args[CN_ONE])));
            break;

        case CLI_CN_PORT_SET_CP_PARAM:
            /* apu4Args[CN_ZERO] = CNPV
             * apu4Args[CN_ONE] = QSizeSetPt
             * apu4Args[2] = FeedBkWgt
             * apu4Args[3] = MinSamples
             * apu4Args[4] = HeaderOctets */

            /* AT least one parameter need to be set */
            if (!(apu4Args[1] || apu4Args[2] || apu4Args[3] || apu4Args[4]))
            {
                CLI_SET_ERR (CN_CLI_WRONG_CONG_PROP);
                i4RetStatus = CLI_FAILURE;
                break;
            }

            if (apu4Args[2] != NULL)
            {
                pu1FeedBkWgt = (UINT1 *) apu4Args[2];
                if ((isdigit (*pu1FeedBkWgt) == CN_ZERO) &&
                    (*pu1FeedBkWgt != '-'))
                {
                    /*Invalid Input for FeedBackWgt */
                    CLI_SET_ERR (CN_CLI_WRONG_FEED_BACK_WGT);
                    i4RetStatus = CLI_FAILURE;
                    break;
                }
                else
                {
                    i4FeedBkWgt = CLI_ATOI (pu1FeedBkWgt);
                    pi4FeedBkWgt = &i4FeedBkWgt;
                }
            }
            i4RetStatus = CnCliSetCpParam (CliHandle,
                                           i4IfIndex,
                                           (*(apu4Args[CN_ZERO])),
                                           apu4Args[CN_ONE],
                                           pi4FeedBkWgt,
                                           apu4Args[3], apu4Args[4]);
            break;

        case CLI_CN_DEBUG:
            /* apu4Args[CN_ZERO] = TraceOption
             * apu4Args[CN_ONE] = ContextName */
            i4RetStatus = CnCliDebug (CliHandle,
                                      CLI_PTR_TO_I4 (apu4Args[CN_ZERO]),
                                      ((UINT1 *) apu4Args[CN_ONE]),
                                      CLI_CN_DEBUG);
            break;

        case CLI_CN_NO_DEBUG:
            /* apu4Args[CN_ZERO] = TraceOption
             * apu4Args[1] = ContextName */
            i4RetStatus = CnCliDebug (CliHandle,
                                      CLI_PTR_TO_I4 (apu4Args[CN_ZERO]),
                                      ((UINT1 *) apu4Args[CN_ONE]),
                                      CLI_CN_NO_DEBUG);
            break;

        case CLI_CN_CLEAR_COUNTER:
            /* apu4Args[CN_ZERO] = ContextName */
            i4RetStatus = CncliResetCounter (CliHandle, u4IfIndex,
                                             (UINT1 *) apu4Args[CN_ZERO]);
            break;

        case CLI_CN_SHOW_GLOB_INFO:
            /* apu4Args[CN_ZERO] = ContextName */
            i4RetStatus = CnCliShowGlobInfo (CliHandle, (UINT1 *)
                                             apu4Args[CN_ZERO]);
            break;

        case CLI_CN_SHOW_INT_COUNTERS:
            /* apu4Args[CN_ZERO] = CNPV */
            i4RetStatus = CnCliShowIfaceCounters (CliHandle, u4IfIndex,
                                                  (apu4Args[CN_ZERO]));
            break;

        case CLI_CN_SHOW_CNPV:
            /* apu4Args[CN_ZERO] = ContextName
             * apu4Args[1] = CNPV */
            if (CLI_PTR_TO_U4 (apu4Args[2]) == CN_PORT_PRI_SHOW)
            {
                i4RetStatus = CnCliShowCnpv (CliHandle, u4IfIndex,
                                             (UINT1 *) apu4Args[CN_ZERO],
                                             apu4Args[CN_ONE]);
            }
            else
            {
                i4RetStatus =
                    CnCliShowCompCnpv (CliHandle, (UINT1 *) apu4Args[CN_ZERO],
                                       apu4Args[CN_ONE]);
            }
            break;

        case CLI_CN_SHOW_CP_PARAM:
            /* apu4Args[CN_ZERO] = ContextName
             * apu4Args[1] = CNPV */
            i4RetStatus = CnCliShowCpParam (CliHandle, u4IfIndex,
                                            (UINT1 *) apu4Args[CN_ZERO],
                                            apu4Args[CN_ONE]);
            break;

        default:
            CliPrintf (CliHandle, "Unknown Command\r\n");
            CnUnLock ();
            CliUnRegisterLock (CliHandle);
            return CLI_FAILURE;
    }

    if ((i4RetStatus == CLI_FAILURE) &&
        (CLI_GET_ERR (&u4ErrorCode) == CLI_SUCCESS))
    {
        if (u4ErrorCode > CN_ZERO)
        {
            CliPrintf (CliHandle, "\r%% %s\r\n",
                       gapc1CnCliErrString[u4ErrorCode]);
        }

        CLI_SET_ERR (CN_ZERO);

    }

    /*Check UnLock Function Name */
    CnUnLock ();
    CliUnRegisterLock (CliHandle);
    return i4RetStatus;
}                                /*cli_process_cn_cmd */

/******************************************************************************
 * Function Name : CnCliSetSysCtrl
 * Description   : This function is used to set system control for the module
 *                 as start or shutdown
 * Input(s)      : tCliHandle - CliHandle
 *                 i4Status   - Module status to be set
 * Output(s)     : System Control is set
 * Return(s)     : CLI_SUCCESS / CLI_FAILURE
 *****************************************************************************/
INT4
CnCliSetSysCtrl (tCliHandle CliHandle, INT4 i4Status)
{
    UINT4               u4ErrorCode = CN_ZERO;

    if (nmhTestv2FsCnSystemControl (&u4ErrorCode, i4Status) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }
    if (nmhSetFsCnSystemControl (i4Status) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/******************************************************************************
 * Function Name : CnCliSetCn
 * Description   : This function is used to set module status as enabled 
 *                 or disabled
 * Input(s)      : tCliHandle - CliHandle
 *                 u4CompId   - Component Id
 *                 u1Status   - Module status to be set
 * Output(s)     : Module status is set
 * Return(s)     : CLI_SUCCESS / CLI_FAILURE
 * ****************************************************************************/
INT4
CnCliSetCn (tCliHandle CliHandle, UINT4 u4CompId, INT4 i4Status)
{
    UINT4               u4ErrorCode = CN_ZERO;

    if (nmhTestv2Ieee8021CnGlobalMasterEnable
        (&u4ErrorCode, u4CompId, i4Status) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }
    if (nmhSetIeee8021CnGlobalMasterEnable (u4CompId, i4Status) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/******************************************************************************
 *  Function Name : CnCliSetCnm
 *  Description   : This function is used to set the transmission priority of 
 *                  CNMs generated in the given component
 *  Input(s)      : tCliHandle - CliHandle
 *                  u4CompId   - Component Id
 *                  u4CompPriority   - CNM Transmit priority
 *  Output(s)     : CNM Transmit priority for the particular component is set
 *  Return(s)     : CLI_SUCCESS / CLI_FAILURE
 *****************************************************************************/
INT4
CnCliSetCnm (tCliHandle CliHandle, UINT4 u4CompId, UINT4 u4CnmPriority)
{
    UINT4               u4ErrorCode = SNMP_ERR_NO_ERROR;

    if (nmhTestv2Ieee8021CnGlobalCnmTransmitPriority (&u4ErrorCode, u4CompId,
                                                      u4CnmPriority) ==
        SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }
    if (nmhSetIeee8021CnGlobalCnmTransmitPriority (u4CompId, u4CnmPriority) ==
        SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;

    }
    return CLI_SUCCESS;
}

/******************************************************************************
 * Function Name : CnCliSetCnpv
 * Description   : This function is used to create a CNPV in the 
 *                 current component
 * Input(s)      : tCliHandle - CliHandle
 *                 u4CompId   - Component Id
 *                 u4CNPV   - CNPV to be created
 * Output(s)     : Given Priority is configured as CNPV in the component
 * Return(s)     : CLI_SUCCESS / CLI_FAILURE
 ******************************************************************************/
INT4
CnCliSetCnpv (tCliHandle CliHandle, UINT4 u4CompId, UINT4 u4CNPV)
{
    UINT4               u4ErrorCode = SNMP_ERR_NO_ERROR;

    if (nmhTestv2Ieee8021CnComPriRowStatus (&u4ErrorCode, u4CompId, u4CNPV,
                                            CN_CREATE_AND_WAIT) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }
    if (nmhSetIeee8021CnComPriRowStatus (u4CompId, u4CNPV, CN_CREATE_AND_WAIT)
        == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;

    }
    if (nmhTestv2Ieee8021CnComPriRowStatus (&u4ErrorCode, u4CompId, u4CNPV,
                                            CN_ACTIVE) == SNMP_FAILURE)
    {
        /* Delete the created CNPV */
        nmhSetIeee8021CnComPriRowStatus (u4CompId, u4CNPV, CN_DESTROY);
        return CLI_FAILURE;
    }
    if (nmhSetIeee8021CnComPriRowStatus (u4CompId, u4CNPV, CN_ACTIVE) ==
        SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;

    }
    return CLI_SUCCESS;
}

/******************************************************************************
 * Function Name : CnCliDeleteCnpv
 * Description   : This function is used to delete  a CNPV in the
 *                 current component
 * Input(s)      : tCliHandle - CliHandle
 *                 u4CompId   - Component Id
 *                 u4CNPV   - CNPV to be deleted
 * Output(s)     : CNPV is deleted in the component
 * Return(s)     : CLI_SUCCESS / CLI_FAILURE
 ******************************************************************************/
INT4
CnCliDeleteCnpv (tCliHandle CliHandle, UINT4 u4CompId, UINT4 u4CNPV)
{
    UINT4               u4ErrorCode = SNMP_ERR_NO_ERROR;

    if (nmhTestv2Ieee8021CnComPriRowStatus
        (&u4ErrorCode, u4CompId, u4CNPV, DESTROY) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }
    if (nmhSetIeee8021CnComPriRowStatus (u4CompId, u4CNPV, DESTROY) ==
        SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;

    }
    return CLI_SUCCESS;
}

/******************************************************************************
 * Function Name : CnCliSetDefChoice
 * Description   : This function is used to set the defense mode choice for
 *                 the given  CNPV in the current component
 * Input(s)      : tCliHandle - CliHandle
 *                 u4CompId   - ComponentId
 *                 u4CNPV      - CNPV 
 *                 u1DefChoice - Defense Mode Choice for the CNPV 
 * Output(s)     : DefenseModeChoice of the CNPV in the current component is set
 * Return(s)     : CLI_SUCCESS / CLI_FAILURE
 ******************************************************************************/
INT4
CnCliSetDefChoice (tCliHandle CliHandle, UINT4 u4CompId, UINT4 u4CNPV,
                   UINT1 u1DefChoice)
{
    UINT4               u4ErrorCode = SNMP_ERR_NO_ERROR;
    UINT1               u1Flag = OSIX_FALSE;

    if (nmhValidateIndexInstanceIeee8021CnCompntPriTable (u4CompId, u4CNPV) ==
        SNMP_FAILURE)
    {
        u1Flag = OSIX_TRUE;
        if (nmhTestv2Ieee8021CnComPriRowStatus (&u4ErrorCode, u4CompId, u4CNPV,
                                                CN_CREATE_AND_WAIT) ==
            SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }
        if (nmhSetIeee8021CnComPriRowStatus (u4CompId, u4CNPV,
                                             CN_CREATE_AND_WAIT) ==
            SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }
    nmhTestv2Ieee8021CnComPriDefModeChoice (&u4ErrorCode, u4CompId, u4CNPV,
                                            u1DefChoice);
    nmhSetIeee8021CnComPriDefModeChoice (u4CompId, u4CNPV, u1DefChoice);
    if (u1Flag == OSIX_TRUE)
    {
        if (nmhTestv2Ieee8021CnComPriRowStatus (&u4ErrorCode, u4CompId, u4CNPV,
                                                CN_ACTIVE) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }
        nmhSetIeee8021CnComPriRowStatus (u4CompId, u4CNPV, CN_ACTIVE);

    }
    return CLI_SUCCESS;
}

/******************************************************************************
 * Function Name : CnCliSetAltPri
 * Description   : This function is used to set the alternate priority for
 *                  the given  CNPV in the current component
 * Input(s)      : tCliHandle - CliHandle
 *                 u4CNPV      - CNPV
 *                 u4AltPri   - Alternate Priority
 * Output(s)     : Alternate Prioirty of the CNPV in the current component
 *                 is set
 * Return(s)     : CLI_SUCCESS / CLI_FAILURE
 ******************************************************************************/
INT4
CnCliSetAltPri (tCliHandle CliHandle, UINT4 u4CompId, UINT4 u4CNPV,
                UINT4 u4AltPri)
{
    UINT4               u4ErrorCode = SNMP_ERR_NO_ERROR;
    UINT1               u1Flag = OSIX_FALSE;

    if (nmhValidateIndexInstanceIeee8021CnCompntPriTable (u4CompId, u4CNPV) ==
        SNMP_FAILURE)
    {
        u1Flag = OSIX_TRUE;
        if (nmhTestv2Ieee8021CnComPriRowStatus (&u4ErrorCode, u4CompId, u4CNPV,
                                                CN_CREATE_AND_WAIT) ==
            SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }
        if (nmhSetIeee8021CnComPriRowStatus (u4CompId, u4CNPV,
                                             CN_CREATE_AND_WAIT) ==
            SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }
    if (nmhTestv2Ieee8021CnComPriAlternatePriority
        (&u4ErrorCode, u4CompId, u4CNPV, u4AltPri) == SNMP_FAILURE)
    {
        if (u1Flag == OSIX_TRUE)
        {
            nmhSetIeee8021CnComPriRowStatus (u4CompId, u4CNPV, CN_DESTROY);
        }

        return CLI_FAILURE;
    }
    if (nmhSetIeee8021CnComPriAlternatePriority (u4CompId, u4CNPV, u4AltPri) ==
        SNMP_FAILURE)
    {
        if (u1Flag == OSIX_TRUE)
        {
            nmhSetIeee8021CnComPriRowStatus (u4CompId, u4CNPV, CN_DESTROY);
        }

        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;

    }

    if (u1Flag == OSIX_TRUE)
    {
        if (nmhTestv2Ieee8021CnComPriRowStatus (&u4ErrorCode, u4CompId, u4CNPV,
                                                CN_ACTIVE) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }
        nmhSetIeee8021CnComPriRowStatus (u4CompId, u4CNPV, CN_ACTIVE);

    }

    return CLI_SUCCESS;
}

/******************************************************************************
 * Function Name : CnCliSetDefMode
 * Description   : This function is used to set the defense for
 *                 the given  CNPV in the current component
 * Input(s)      : tCliHandle  - CliHandle
 *                 u4CNPV      - CNPV
 *                 u1DefMode   -  Defense Mode
 * Output(s)     : Defense Mode of the CNPV in the current component is set
 * Return(s)     : CLI_SUCCESS / CLI_FAILURE
 ******************************************************************************/
INT4
CnCliSetDefMode (tCliHandle CliHandle, UINT4 u4CompId, UINT4 u4CNPV,
                 UINT1 u1DefMode)
{
    UINT4               u4ErrorCode = SNMP_ERR_NO_ERROR;
    UINT1               u1Flag = OSIX_FALSE;

    if (nmhValidateIndexInstanceIeee8021CnCompntPriTable (u4CompId, u4CNPV) ==
        SNMP_FAILURE)
    {
        u1Flag = OSIX_TRUE;
        if (nmhTestv2Ieee8021CnComPriRowStatus (&u4ErrorCode, u4CompId, u4CNPV,
                                                CN_CREATE_AND_WAIT) ==
            SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }
        if (nmhSetIeee8021CnComPriRowStatus (u4CompId, u4CNPV,
                                             CN_CREATE_AND_WAIT) ==
            SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }

    nmhTestv2Ieee8021CnComPriAdminDefenseMode
        (&u4ErrorCode, u4CompId, u4CNPV, u1DefMode);
    nmhSetIeee8021CnComPriAdminDefenseMode (u4CompId, u4CNPV, u1DefMode);
    if (u1Flag == OSIX_TRUE)
    {
        if (nmhTestv2Ieee8021CnComPriRowStatus (&u4ErrorCode, u4CompId, u4CNPV,
                                                CN_ACTIVE) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }
        nmhSetIeee8021CnComPriRowStatus (u4CompId, u4CNPV, CN_ACTIVE);

    }

    return CLI_SUCCESS;
}

/******************************************************************************
 * Function Name : CnCliSetPortDefChoice
 * Description   : This function is used to set the default defense mode choice
 *                 for the given  CNPV in the ports that are newly created 
 *                 in the current component
 * Input(s)      : tCliHandle  - CliHandle
 *                 u4CNPV      - CNPV
 *                 u1PortDefaultDefModeCh   -  Default DefMosdeChoice for the
 *                 newly created ports in the current component
 * Output(s)     : PortDefaultDefenseModeChoice of the CNPV in the 
 *                 current componet is set
 * Return(s)     : CLI_SUCCESS / CLI_FAILURE
 ******************************************************************************/
INT4
CnCliSetPortDefChoice (tCliHandle CliHandle, UINT4 u4CompId, UINT4 u4CNPV,
                       UINT1 u1PortDefaultDefModeCh)
{
    UINT4               u4ErrorCode = SNMP_ERR_NO_ERROR;
    UINT1               u1Flag = OSIX_FALSE;

    if (nmhValidateIndexInstanceIeee8021CnCompntPriTable (u4CompId, u4CNPV) ==
        SNMP_FAILURE)
    {
        u1Flag = OSIX_TRUE;
        if (nmhTestv2Ieee8021CnComPriRowStatus (&u4ErrorCode, u4CompId, u4CNPV,
                                                CN_CREATE_AND_WAIT) ==
            SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }
        if (nmhSetIeee8021CnComPriRowStatus (u4CompId, u4CNPV,
                                             CN_CREATE_AND_WAIT) ==
            SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }

    nmhTestv2Ieee8021CnComPriCreation (&u4ErrorCode, u4CompId, u4CNPV,
                                       u1PortDefaultDefModeCh);
    nmhSetIeee8021CnComPriCreation (u4CompId, u4CNPV, u1PortDefaultDefModeCh);
    if (u1Flag == OSIX_TRUE)
    {
        if (nmhTestv2Ieee8021CnComPriRowStatus (&u4ErrorCode, u4CompId, u4CNPV,
                                                CN_ACTIVE) == SNMP_FAILURE)
        {
            return CLI_FAILURE;

        }
        nmhSetIeee8021CnComPriRowStatus (u4CompId, u4CNPV, CN_ACTIVE);

    }
    return CLI_SUCCESS;
}

/******************************************************************************
 * Function Name : CnCliSetLldpChoice
 * Description   : This function is used to set the LLDP Instance choice
 *                 for the given  CNPV in the current component
 * Input(s)      : tCliHandle  - CliHandle
 *                 u4CompId    - Component Id
 *                 u4CNPV      - CNPV
 *                 i4InstChoice- LLDP Instance Choice 
 * Output(s)     :  LLDP Instance Choice of the CNPV in the
 *                 current componet is set
 * Return(s)     : CLI_SUCCESS / CLI_FAILURE
 ******************************************************************************/
INT4
CnCliSetLldpChoice (tCliHandle CliHandle, UINT4 u4CompId, UINT4 u4CNPV,
                    INT4 i4InstChoice)
{
    UINT4               u4ErrorCode = SNMP_ERR_NO_ERROR;
    UINT1               u1Flag = OSIX_FALSE;

    if (nmhValidateIndexInstanceIeee8021CnCompntPriTable (u4CompId,
                                                          u4CNPV) ==
        SNMP_FAILURE)
    {
        u1Flag = OSIX_TRUE;
        if (nmhTestv2Ieee8021CnComPriRowStatus (&u4ErrorCode, u4CompId, u4CNPV,
                                                CN_CREATE_AND_WAIT) ==
            SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }
        if (nmhSetIeee8021CnComPriRowStatus (u4CompId, u4CNPV,
                                             CN_CREATE_AND_WAIT) ==
            SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }

    nmhTestv2Ieee8021CnComPriLldpInstanceChoice (&u4ErrorCode, u4CompId,
                                                 u4CNPV, i4InstChoice);
    if (nmhSetIeee8021CnComPriLldpInstanceChoice
        (u4CompId, u4CNPV, i4InstChoice) == SNMP_FAILURE)
    {
        if (u1Flag == OSIX_TRUE)
        {
            nmhSetIeee8021CnComPriRowStatus (u4CompId, u4CNPV, CN_DESTROY);
        }

        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;

    }
    if (u1Flag == OSIX_TRUE)
    {
        if (nmhTestv2Ieee8021CnComPriRowStatus (&u4ErrorCode, u4CompId, u4CNPV,
                                                CN_ACTIVE) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }
        nmhSetIeee8021CnComPriRowStatus (u4CompId, u4CNPV, CN_ACTIVE);

    }
    return CLI_SUCCESS;
}

/******************************************************************************
 * Function Name : CnCliSetlldpSelector 
 * Description   : This function is used to set the LLDP Instance Selector
 *                 for the given  CNPV in the current component
 * Input(s)      : tCliHandle  - CliHandle
 *                 u4CompId    - Component Id
 *                 u4CNPV      - CNPV
 *                 u4InstChoice- LLDP Instance Selector
 * Output(s)     :  LLDP Instance Selector of the CNPV in the
 *                 current componet is set
 * Return(s)     : CLI_SUCCESS / CLI_FAILURE
 *****************************************************************************/
INT4
CnCliSetlldpSelector (tCliHandle CliHandle, UINT4 u4CompId, UINT4 u4CNPV,
                      UINT4 u4InstSelector)
{
    UINT4               u4ErrorCode = SNMP_ERR_NO_ERROR;
    UINT1               u1Flag = OSIX_FALSE;

    if (nmhValidateIndexInstanceIeee8021CnCompntPriTable (u4CompId,
                                                          u4CNPV) ==
        SNMP_FAILURE)
    {
        u1Flag = OSIX_TRUE;
        if (nmhTestv2Ieee8021CnComPriRowStatus (&u4ErrorCode, u4CompId, u4CNPV,
                                                CN_CREATE_AND_WAIT) ==
            SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }
        if (nmhSetIeee8021CnComPriRowStatus (u4CompId, u4CNPV,
                                             CN_CREATE_AND_WAIT) ==
            SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }

    nmhTestv2Ieee8021CnComPriLldpInstanceSelector (&u4ErrorCode, u4CompId,
                                                   u4CNPV, u4InstSelector);
    nmhSetIeee8021CnComPriLldpInstanceSelector (u4CompId, u4CNPV,
                                                u4InstSelector);
    if (u1Flag == OSIX_TRUE)
    {
        if (nmhTestv2Ieee8021CnComPriRowStatus (&u4ErrorCode, u4CompId, u4CNPV,
                                                CN_ACTIVE) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }
        nmhSetIeee8021CnComPriRowStatus (u4CompId, u4CNPV, CN_ACTIVE);

    }

    return CLI_SUCCESS;
}

/******************************************************************************
 * Function Name : CnCliSetNotify
 * Description   : This function is used to set/Reset the Module Level Trap                 
 * Input(s)      : tCliHandle  - CliHandle
 *                  i4TrapFlag - Trap Flag
 * Output(s)     : Module level trap is enabled
 * Return(s)     : CLI_SUCCESS / CLI_FAILURE
 *****************************************************************************/
INT4
CnCliSetNotify (tCliHandle CliHandle, INT4 i4TrapFlag, UINT1 u1Flag)
{
    UINT4               u4ErrorCode = SNMP_ERR_NO_ERROR;
    INT4                i4TrapOpt = CN_ZERO;

    nmhGetFsCnGlobalEnableTrap (&i4TrapOpt);
    if (u1Flag == CLI_CN_SET_NOTIFY)
    {
        i4TrapOpt |= i4TrapFlag;
    }
    else
    {
        i4TrapOpt &= (~(i4TrapFlag));
    }
    nmhTestv2FsCnGlobalEnableTrap (&u4ErrorCode, i4TrapOpt);
    nmhSetFsCnGlobalEnableTrap (i4TrapOpt);
    UNUSED_PARAM (CliHandle);
    return CLI_SUCCESS;
}

/*****************************************************************************
 * Function Name : CnCliPortDefModeCh
 * Description   : This function is used to set the defense mode choice for a
 *                 CNPV at the given port 
 * Input(s)      : tCliHandle  - CliHandle
 *                 u4CompId    - Component Id
 *                 i4IfIndex   - Interface Index
 *                 u4CNPV      - CNPV
 *                 u1DefModeCh - DefenseModeChoice
 * Output(s)     : Defense Mode Choice for the CNPV at a port is set
 * Return(s)     : CLI_SUCCESS / CLI_FAILURE
 *****************************************************************************/
INT4
CnCliPortDefModeCh (tCliHandle CliHandle, UINT4 u4CompId, INT4 i4IfIndex,
                    UINT4 u4CNPV, INT4 i4DefModeCh)
{
    UINT4               u4ErrorCode = SNMP_ERR_NO_ERROR;

    if (nmhTestv2Ieee8021CnPortPriDefModeChoice
        (&u4ErrorCode, u4CompId, u4CNPV, i4IfIndex,
         i4DefModeCh) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }
    if (nmhSetIeee8021CnPortPriDefModeChoice (u4CompId, u4CNPV, i4IfIndex,
                                              i4DefModeCh) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/****************************************************************************
 * Function Name : CnCliPortDefMode
 * Description   : This function is used to set the defense mode for a
 *                 CNPV at the given port
 * Input(s)      : tCliHandle  - CliHandle
 *                 u4CompId    - Component Id
 *                 i4IfIndex   - IfIndex
 *                 u4CNPV      - CNPV
 *                 i4DefMode - DefenseMode
 * Output(s)     : Defense Mode for the CNPV at a port is set
 * Return(s)     : CLI_SUCCESS / CLI_FAILURE
 *****************************************************************************/
INT4
CnCliPortDefMode (tCliHandle CliHandle, UINT4 u4CompId, INT4 i4IfIndex,
                  UINT4 u4CNPV, INT4 i4DefMode)
{
    UINT4               u4ErrorCode = SNMP_ERR_NO_ERROR;

    if (nmhTestv2Ieee8021CnPortPriAdminDefenseMode
        (&u4ErrorCode, u4CompId, u4CNPV, i4IfIndex, i4DefMode) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }
    if (nmhSetIeee8021CnPortPriAdminDefenseMode (u4CompId, u4CNPV, i4IfIndex,
                                                 i4DefMode) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/******************************************************************************
 * Function Name : CnCliSetPortLldpChoice
 * Description   : This function is used to set the LLDP Instance Choice for a
 *                 CNPV at the given port
 * Input(s)      : tCliHandle  - CliHandle
 *                 u4CompId    - Component Id
 *                 i4IfIndex   - IfIndex
 *                  u4CNPV      - CNPV
 *                 i4InstChoice - LLDP Instance Choice
 * Output(s)     : LLDP Instance Choice for the CNPV at a port is set
 * Return(s)     : CLI_SUCCESS / CLI_FAILURE
 ******************************************************************************/
INT4
CnCliSetPortLldpChoice (tCliHandle CliHandle, UINT4 u4CompId, INT4 i4IfIndex,
                        UINT4 u4CNPV, INT4 i4InstChoice)
{
    UINT4               u4ErrorCode = SNMP_ERR_NO_ERROR;

    if (nmhTestv2Ieee8021CnPortPriLldpInstanceChoice (&u4ErrorCode, u4CompId,
                                                      u4CNPV, i4IfIndex,
                                                      i4InstChoice) ==
        SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }
    if (nmhSetIeee8021CnPortPriLldpInstanceChoice (u4CompId, u4CNPV, i4IfIndex,
                                                   i4InstChoice) ==
        SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/******************************************************************************
 *  Function Name : CnCliSetPortlldpSelector
 *  Description   : This function is used to set the LLDP Instance Selector for
 *                  given  CNPV at a particular port
 *  Input(s)      : tCliHandle  - CliHandle
 *                 u4CompId    - Component Id
 *                 u4CNPV      - CNPV
 *                 i4IfIndex   - IfIndex
 *                 u4InstChoice - LLDP Instance Selector
 * Output(s)     : LLDP Instance Selector for the CNPV at a port is set
 * Return(s)     : CLI_SUCCESS / CLI_FAILURE
 ******************************************************************************/
INT4
CnCliSetPortLldpSelector (tCliHandle CliHandle, UINT4 u4CompId, INT4 i4IfIndex,
                          UINT4 u4CNPV, UINT4 u4InstSelector)
{
    UINT4               u4ErrorCode = SNMP_ERR_NO_ERROR;

    if (nmhTestv2Ieee8021CnPortPriLldpInstanceSelector (&u4ErrorCode, u4CompId,
                                                        u4CNPV, i4IfIndex,
                                                        u4InstSelector) ==
        SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }
    if (nmhSetIeee8021CnPortPriLldpInstanceSelector (u4CompId, u4CNPV,
                                                     i4IfIndex,
                                                     u4InstSelector) ==
        SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/******************************************************************************
 * Function Name : CnCliSetPortAltPri
 * Description   : This function is used to set the Alternate Priority for
 *                  given  CNPV at a particular port
 * Input(s)      : tCliHandle  - CliHandle
 *                 u4CompId    - Component Id
 *                 i4IfIndex   - IfIndex
 *                 u4CNPV      - CNPV
 *                 u4AltPri    - Alternate Priority
 * Output(s)     : Alternate Priority for the CNPV at a port is set
 * Return(s)     : CLI_SUCCESS / CLI_FAILURE
 ******************************************************************************/
INT4
CnCliSetPortAltPri (tCliHandle CliHandle, UINT4 u4CompId, INT4 i4IfIndex,
                    UINT4 u4CNPV, UINT4 u4AltPri)
{
    UINT4               u4ErrorCode = SNMP_ERR_NO_ERROR;

    if (nmhTestv2Ieee8021CnPortPriAlternatePriority (&u4ErrorCode, u4CompId,
                                                     u4CNPV, i4IfIndex,
                                                     u4AltPri) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }
    if (nmhSetIeee8021CnPortPriAlternatePriority (u4CompId, u4CNPV, i4IfIndex,
                                                  u4AltPri) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/******************************************************************************
 * Function Name : CnCliSetCpParam
 * Description   : This function is used to set the CP Properties for a CP
 *                 corresponding to the given  CNPV at a particular port
 * Input(s)      : tCliHandle  - CliHandle
 *                 i4IfIndex   -IfIndex
 *                 u4CNPV      - CNPV
 *                 pu4QSizeSetPt- QueueSizeSetPoint
 *                 pi4FeedBkWgt - FeedBackWeight
 *                 pu4SampleBase - Minimum Sample Base
 *                 pu4HeaderOcts -Header Octets
 * Output(s)     : CP Properties for the CNPV at a port is set
 * Return(s)     : CLI_SUCCESS / CLI_FAILURE
 ******************************************************************************/
INT4
CnCliSetCpParam (tCliHandle CliHandle, INT4 i4IfIndex,
                 UINT4 u4CNPV, UINT4 *pu4QSizeSetPt, INT4 *pi4FeedBkWgt,
                 UINT4 *pu4SampleBase, UINT4 *pu4HeaderOcts)
{
    UINT4               u4ErrorCode = SNMP_ERR_NO_ERROR;
    UINT4               u4CpIndex = CN_CPINDEX_FROM_PRIO (u4CNPV);
    UINT4               u4TempCompId = CN_ZERO;
    UINT4               u4CompId = CN_ZERO;
    UINT4               u4TempIfIndex = CN_ZERO;
    UINT2               u2LocalPortId = CN_ZERO;

    u4TempCompId = CLI_GET_CXT_ID ();
    if (u4TempCompId != CN_INVALID_CONTEXT)
    {
        u4CompId = CN_CONVERT_CXT_ID_TO_COMP_ID (u4TempCompId);
    }
    else
    {
        u4TempIfIndex = i4IfIndex;
        if (CnPortVcmGetCxtInfoFromIfIndex (u4TempIfIndex, &u4CompId,
                                            &u2LocalPortId) == OSIX_FAILURE)
        {
            return CLI_FAILURE;
        }
    }

    if (pu4QSizeSetPt != NULL)
    {
        if (nmhTestv2Ieee8021CnCpQueueSizeSetPoint
            (&u4ErrorCode, u4CompId, i4IfIndex, u4CpIndex,
             *pu4QSizeSetPt) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }
        if (nmhSetIeee8021CnCpQueueSizeSetPoint (u4CompId, i4IfIndex, u4CpIndex,
                                                 *pu4QSizeSetPt) ==
            SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }
    if (pi4FeedBkWgt != NULL)
    {
        if (nmhTestv2Ieee8021CnCpFeedbackWeight (&u4ErrorCode, u4CompId,
                                                 i4IfIndex, u4CpIndex,
                                                 *pi4FeedBkWgt) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }
        if (nmhSetIeee8021CnCpFeedbackWeight (u4CompId, i4IfIndex, u4CpIndex,
                                              *pi4FeedBkWgt) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }
    if (pu4SampleBase != NULL)
    {
        if (nmhTestv2Ieee8021CnCpMinSampleBase (&u4ErrorCode, u4CompId,
                                                i4IfIndex, u4CpIndex,
                                                *pu4SampleBase) == SNMP_FAILURE)

        {
            return CLI_FAILURE;
        }
        if (nmhSetIeee8021CnCpMinSampleBase (u4CompId, i4IfIndex, u4CpIndex,
                                             *pu4SampleBase) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }
    if (pu4HeaderOcts != NULL)
    {
        if (nmhTestv2Ieee8021CnCpMinHeaderOctets (&u4ErrorCode, u4CompId,
                                                  i4IfIndex, u4CpIndex,
                                                  *pu4HeaderOcts) ==
            SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }
        if (nmhSetIeee8021CnCpMinHeaderOctets (u4CompId, i4IfIndex, u4CpIndex,
                                               *pu4HeaderOcts) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }
    return CLI_SUCCESS;
}

/******************************************************************************
 * Function Name : CncliResetCounter
 * Description   : This function is used to reset the counters in a 
 *                 port or component
 * Input(s)      : tCliHandle  - CliHandle
 *                u4IfIndex    - PortIndex
 *                pu1ContextName - ContextName
 * Output(s)     : Counters are reset
 * Return(s)     : CLI_SUCCESS / CLI_FAILURE
 ******************************************************************************/
INT4
CncliResetCounter (tCliHandle CliHandle, UINT4 u4IfIndex, UINT1 *pu1ContxtName)
{
    UINT4               u4ErrorCode = SNMP_ERR_NO_ERROR;
    INT4                i4IfIndex = u4IfIndex;
    UINT4               u4CompId = CN_ZERO;
    UINT4               u4Priority = CN_ZERO;
    UINT2               u2LocalPortId = CN_ZERO;
    UINT4               u4NextCompId = CN_ZERO;

    if ((u4IfIndex == CN_ZERO) && (pu1ContxtName == NULL))
    {
        nmhGetFirstIndexIeee8021CnGlobalTable (&u4NextCompId);
        do
        {
            u4CompId = u4NextCompId;
            if (nmhTestv2FsCnXGlobalClearCounters (&u4ErrorCode, u4CompId,
                                                   CN_SET_CLEAR_COUNTER) ==
                SNMP_FAILURE)
            {
                return CLI_FAILURE;
            }
            if (nmhSetFsCnXGlobalClearCounters
                (u4CompId, CN_SET_CLEAR_COUNTER) == SNMP_FAILURE)
            {
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }

        }
        while (nmhGetNextIndexIeee8021CnGlobalTable (u4CompId,
                                                     &u4NextCompId) !=
               SNMP_FAILURE);

    }
    else if (u4IfIndex == CN_ZERO)
    {
        /*PortId is not given */
        if (CnPortVcmIsSwitchExist (pu1ContxtName, &u4CompId) != OSIX_SUCCESS)
        {
            return CLI_FAILURE;
        }

        if (nmhTestv2FsCnXGlobalClearCounters (&u4ErrorCode, u4CompId,
                                               CN_SET_CLEAR_COUNTER) ==
            SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }
        if (nmhSetFsCnXGlobalClearCounters (u4CompId, CN_SET_CLEAR_COUNTER)
            == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }
    else
    {
        if (CnPortVcmGetCxtInfoFromIfIndex (u4IfIndex, &u4CompId,
                                            &u2LocalPortId) == VCM_FAILURE)
        {
            return CLI_FAILURE;
        }

        for (u4Priority = CN_ZERO; u4Priority < CN_MAX_VLAN_PRIORITY;
             u4Priority++)
        {
            if (nmhValidateIndexInstanceIeee8021CnCompntPriTable (u4CompId,
                                                                  u4Priority) !=
                SNMP_FAILURE)
            {
                if (nmhTestv2FsCnXPortPriClearCpCounters (&u4ErrorCode,
                                                          u4CompId, u4Priority,
                                                          i4IfIndex,
                                                          CN_SET_CLEAR_COUNTER)
                    == SNMP_FAILURE)
                {
                    return CLI_FAILURE;
                }
                if (nmhSetFsCnXPortPriClearCpCounters (u4CompId, u4Priority,
                                                       i4IfIndex,
                                                       CN_SET_CLEAR_COUNTER)
                    == SNMP_FAILURE)
                {
                    CLI_FATAL_ERROR (CliHandle);
                    return CLI_FAILURE;
                }
            }
        }
    }
    return CLI_SUCCESS;
}

/******************************************************************************
 * Function Name : CnCliDebug
 * Description   : This function is used to set the Debug Flags in the 
 *                 current component or all components
 * Input(s)      : tCliHandle  - CliHandle
 *                 i4TraceLevel - TraceLevel to be set
 *                 pu1ContextName - Context Name
 * Output(s)     : Debug flags are set
 * Return(s)     : CLI_SUCCESS / CLI_FAILURE
 ******************************************************************************/
INT4
CnCliDebug (tCliHandle CliHandle, INT4 i4TraceOpt, UINT1 *pu1ContextName,
            UINT1 u1SetFlag)
{
    UINT4               u4ErrorCode = SNMP_ERR_NO_ERROR;
    UINT4               u4CompId = CN_ZERO;
    INT4                i4TraceLevel = CN_ZERO;
    UINT4               u4NextCompId = CN_ZERO;

    if (pu1ContextName != NULL)
    {
        if (CnPortVcmIsSwitchExist (pu1ContextName, &u4CompId) == OSIX_FAILURE)
        {
            return CLI_FAILURE;
        }

        if (nmhValidateIndexInstanceFsCnXGlobalTable (u4CompId) != SNMP_FAILURE)
        {
            nmhGetFsCnXGlobalTraceLevel (u4CompId, &i4TraceLevel);
            if (u1SetFlag == CLI_CN_DEBUG)
            {
                i4TraceLevel |= i4TraceOpt;
            }
            else
            {
                i4TraceLevel &= (~(i4TraceOpt));
            }
            if (nmhTestv2FsCnXGlobalTraceLevel
                (&u4ErrorCode, u4CompId, i4TraceLevel) == SNMP_FAILURE)
            {
                return CLI_FAILURE;
            }
            if (nmhSetFsCnXGlobalTraceLevel (u4CompId, i4TraceLevel) ==
                SNMP_FAILURE)
            {
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }
        }
    }
    else
    {
        nmhGetFirstIndexFsCnXGlobalTable (&u4NextCompId);
        do
        {
            u4CompId = u4NextCompId;
            if (nmhValidateIndexInstanceFsCnXGlobalTable (u4CompId) !=
                SNMP_FAILURE)
            {
                nmhGetFsCnXGlobalTraceLevel (u4CompId, &i4TraceLevel);

                if (u1SetFlag == CLI_CN_DEBUG)
                {
                    i4TraceLevel |= i4TraceOpt;
                }
                else
                {
                    i4TraceLevel &= (~(i4TraceOpt));
                }
                if (nmhTestv2FsCnXGlobalTraceLevel
                    (&u4ErrorCode, u4CompId, i4TraceLevel) == SNMP_FAILURE)
                {
                    return CLI_FAILURE;
                }
                if (nmhSetFsCnXGlobalTraceLevel (u4CompId, i4TraceLevel) ==
                    SNMP_FAILURE)
                {
                    CLI_FATAL_ERROR (CliHandle);
                    return CLI_FAILURE;
                }
            }
        }
        while (nmhGetNextIndexFsCnXGlobalTable (u4CompId, &u4NextCompId) !=
               SNMP_FAILURE);
    }
    return CLI_SUCCESS;
}

/******************************************************************************
 * Function Name : CnCliShowAllGlobInfo
 * Description   : This function is used to display the global information
 *                 of all components
 * Input(s)      : tCliHandle  - CliHandle
 *                 u4ompId     - Component Id
 * Output(s)     : Global information per component is displayed
 * Return(s)     : CLI_SUCCESS / CLI_FAILURE
 ******************************************************************************/
INT4
CnCliShowAllGlobInfo (tCliHandle CliHandle)
{
    UINT4               u4CompId = CN_ZERO;
    UINT4               u4NextCompId = CN_ZERO;
    INT4                i4RetStatus = CN_ZERO;

    nmhGetFirstIndexIeee8021CnGlobalTable (&u4NextCompId);
    do
    {
        u4CompId = u4NextCompId;
        CnCliShowCompGlobInfo (CliHandle, u4CompId);
        i4RetStatus = nmhGetNextIndexIeee8021CnGlobalTable (u4CompId,
                                                            &u4NextCompId);
    }
    while (i4RetStatus != SNMP_FAILURE);

    return CLI_SUCCESS;
}

/******************************************************************************
 * Function Name : CnCliShowCompGlobInfo
 * Description   : This function is used to display the global information
 *                 of given component
 * Input(s)      : tCliHandle  - CliHandle
 *                  u4ompId     - Component Id
 * Output(s)     : Global information per component is displayed
 * Return(s)     : CLI_SUCCESS / CLI_FAILURE
 ******************************************************************************/
INT4
CnCliShowCompGlobInfo (tCliHandle CliHandle, UINT4 u4CompId)
{
    INT4                i4MasterEnable = CN_ZERO;
    UINT4               u4CnmPri = CN_ZERO;
    tSNMP_COUNTER64_TYPE DisFrames;
    FS_UINT8            u8DisFrames;
    UINT1               au1DisFrames[CFA_CLI_U8_STR_LENGTH] = { CN_ZERO };
    UINT4               u4TLVErrors = CN_ZERO;
    INT4                i4Priority = CN_ZERO;
    UINT4               u4IsFirstCnpv = CN_ZERO;
    UINT1               au1Alias[VCM_ALIAS_MAX_LEN] = { CN_ZERO };

    MEMSET (&DisFrames, CN_ZERO, sizeof (tSNMP_COUNTER64_TYPE));

    CnPortVcmGetAliasName (u4CompId, au1Alias);
    CliPrintf (CliHandle, "\r\nSwitch  %s \r\n", au1Alias);
    CliPrintf (CliHandle, "------------------\r\n");

    nmhGetIeee8021CnGlobalMasterEnable (u4CompId, &i4MasterEnable);

    if (i4MasterEnable == CN_MASTER_ENABLE)
    {
        CliPrintf (CliHandle, "Module Status         : Enabled \r\n");
    }
    else
    {
        CliPrintf (CliHandle, "Module Status         : Disabled\r\n");
    }
    CliPrintf (CliHandle, "CNPVs                 :");
    for (i4Priority = CN_ZERO; i4Priority < CN_MAX_VLAN_PRIORITY; i4Priority++)
    {
        if (nmhValidateIndexInstanceIeee8021CnCompntPriTable (u4CompId,
                                                              i4Priority) !=
            SNMP_FAILURE)
        {
            if (u4IsFirstCnpv == CN_ZERO)
            {
                CliPrintf (CliHandle, " %d", i4Priority);
                u4IsFirstCnpv = CN_ONE;
            }
            else
            {
                CliPrintf (CliHandle, ",%d", i4Priority);
            }
        }
    }
    if (u4IsFirstCnpv == CN_ZERO)
    {
        CliPrintf (CliHandle, " -");
    }
    CliPrintf (CliHandle, "\r\n");

    nmhGetIeee8021CnGlobalCnmTransmitPriority (u4CompId, &u4CnmPri);
    CliPrintf (CliHandle, "CNM Transmit Priority : %d \r\n", u4CnmPri);

    nmhGetIeee8021CnGlobalDiscardedFrames (u4CompId, &DisFrames);
    u8DisFrames.u4Hi = DisFrames.msn;
    u8DisFrames.u4Lo = DisFrames.lsn;
    FSAP_U8_2STR (&u8DisFrames, (CHR1 *) & au1DisFrames);
    CliPrintf (CliHandle, "GlobalDiscardedFrames : %s \r\n", au1DisFrames);

    nmhGetFsCnXGlobalTLVErrors (u4CompId, &u4TLVErrors);
    CliPrintf (CliHandle, "GlobalTLVErrors       : %d \r\n", u4TLVErrors);

    return CLI_SUCCESS;
}

/******************************************************************************
 * Function Name : CnCliShowGlobInfo
 * Description   : This function is used to display the global information
 *                  per component
 * Input(s)      : tCliHandle  - CliHandle
 *                 pu1ContextName - Context Name
 * Output(s)     : Global information per component is displayed
 * Return(s)     : CLI_SUCCESS / CLI_FAILURE
 ******************************************************************************/
INT4
CnCliShowGlobInfo (tCliHandle CliHandle, UINT1 *pu1ContextName)
{
    UINT4               u4CompId = CN_ZERO;

    INT4                i4TrapType = CN_ZERO;

    if (pu1ContextName != NULL)
    {
        if (CnPortVcmIsSwitchExist (pu1ContextName, &u4CompId) == OSIX_FAILURE)
        {
            CLI_SET_ERR (CN_CLI_INVALID_COMP_ID);
            /*Switch not found */
            return CLI_FAILURE;
        }
    }

    nmhGetFsCnGlobalEnableTrap (&i4TrapType);
    switch (i4TrapType)
    {
        case CN_DEFAULT_TRAP:
            CliPrintf (CliHandle, "Enabled Traps: Errored Port and CNM\r\n");
            break;
        case CN_ERROR_PORT_TRAP:
            CliPrintf (CliHandle, "Enabled Traps: Errored Port \r\n");
            break;
        case CN_CNM_TRAP:
            CliPrintf (CliHandle, "Enabled Traps: CNM\r\n");
            break;
        default:
            CliPrintf (CliHandle, "Enabled Traps: None\r\n");
            break;
    }
    if (pu1ContextName == NULL)
    {
        CnCliShowAllGlobInfo (CliHandle);
    }
    else
    {
        if (CnCliShowCompGlobInfo (CliHandle, u4CompId) == CLI_FAILURE)
        {
            return CLI_FAILURE;
        }
    }
    return CLI_SUCCESS;
}

/******************************************************************************
 * Function Name : CnCliShowIfaceCounters
 * Description   : This function is used to display the interface specific
 *                 counters                  
 * Input(s)      : tCliHandle  - CliHandle
 *                 u4IfIndex   - Interface Index
 *                 u4CNPV      - CNPV
 * Output(s)     : Interface specific counters are displayed
 * Return(s)     : CLI_SUCCESS / CLI_FAILURE
 ******************************************************************************/
INT4
CnCliShowIfaceCounters (tCliHandle CliHandle, UINT4 u4IfIndex, UINT4 *pu4CNPV)
{

    UINT4               u4CompId = CN_ZERO;
    UINT2               u2LocalPortId = CN_ZERO;

    CliPrintf (CliHandle,
               "Interface CNPV  Transmitted Frames    Discarded Frames"
               "     Transmitted Cnms\r\n");
    CliPrintf (CliHandle,
               "--------- ---- -------------------- --------------------"
               " -------------------- \r\n");

    if ((u4IfIndex == CN_ZERO) && (pu4CNPV == NULL))
    {
        if (CnCliShowAllIfCnpvCounters (CliHandle) == CLI_FAILURE)
        {
            return CLI_FAILURE;
        }
    }
    else if (u4IfIndex == CN_ZERO)
    {
        if (CnCliShowIfCounters (CliHandle, *pu4CNPV) == CLI_FAILURE)
        {
            return CLI_FAILURE;
        }
    }
    else if (pu4CNPV == NULL)
    {
        if (CnPortVcmGetCxtInfoFromIfIndex (u4IfIndex, &u4CompId,
                                            &u2LocalPortId) == VCM_FAILURE)
        {
            return CLI_FAILURE;
        }

        if (CnCliShowCnpvCounters (CliHandle, u4CompId, u4IfIndex) ==
            CLI_FAILURE)
        {
            return CLI_FAILURE;
        }
    }
    else
    {
        if (CnPortVcmGetCxtInfoFromIfIndex (u4IfIndex, &u4CompId,
                                            &u2LocalPortId) == VCM_FAILURE)
        {
            return CLI_FAILURE;
        }

        if (CnCliShowIfCnpvCounters (CliHandle, u4CompId, u4IfIndex, *pu4CNPV)
            == CLI_FAILURE)
        {
            return CLI_FAILURE;
        }
    }
    return CLI_SUCCESS;
}

/******************************************************************************
 * Function Name : CnCliShowIfCnpvCounters
 * Description   : This function is used to display the interface and 
 *                 priority specific counters
 * Input(s)      : tCliHandle  - CliHandle
 *                 u4compId    - ComponentId
 *                 u4IfIndex   - Interface Index
 *                 u4CNPV      - CNPV
 * Output(s)     : Interface and priority specific counters are displayed
 * Return(s)     : CLI_SUCCESS / CLI_FAILURE
 ******************************************************************************/
INT4
CnCliShowIfCnpvCounters (tCliHandle CliHandle, UINT4 u4CompId, UINT4 u4PortId,
                         UINT4 u4CNPV)
{
    UINT4               u4CpIndex = CN_ZERO;
    FS_UINT8            u8DisFrames;
    FS_UINT8            u8TransFrames;
    FS_UINT8            u8TransCnms;
    tSNMP_COUNTER64_TYPE DisFrames;
    tSNMP_COUNTER64_TYPE TransFrames;
    tSNMP_COUNTER64_TYPE TransCnms;
    UINT1               au1DisFrames[CFA_CLI_U8_STR_LENGTH] = { CN_ZERO };
    UINT1               au1TransFrames[CFA_CLI_U8_STR_LENGTH] = { CN_ZERO };
    UINT1               au1TransCnms[CFA_CLI_U8_STR_LENGTH] = { CN_ZERO };
    UINT1               au1IfName[CFA_CLI_MAX_IF_NAME_LEN] = { CN_ZERO };
    INT1               *piIfName = NULL;

    MEMSET (&DisFrames, CN_ZERO, sizeof (tSNMP_COUNTER64_TYPE));
    MEMSET (&TransFrames, CN_ZERO, sizeof (tSNMP_COUNTER64_TYPE));
    MEMSET (&TransCnms, CN_ZERO, sizeof (tSNMP_COUNTER64_TYPE));
    FSAP_U8_CLR (&u8DisFrames);
    FSAP_U8_CLR (&u8TransFrames);
    FSAP_U8_CLR (&u8TransCnms);

    piIfName = (INT1 *) &au1IfName[CN_ZERO];

    CnCfaCliGetIfName (u4PortId, piIfName);
    u4CpIndex = CN_CPINDEX_FROM_PRIO (u4CNPV);

    if (nmhValidateIndexInstanceIeee8021CnCpTable (u4CompId, u4PortId,
                                                   u4CpIndex) != SNMP_FAILURE)
    {
        nmhGetIeee8021CnCpDiscardedFrames (u4CompId, u4PortId, u4CpIndex,
                                           &DisFrames);
        nmhGetIeee8021CnCpTransmittedFrames (u4CompId, u4PortId, u4CpIndex,
                                             &TransFrames);
        nmhGetIeee8021CnCpTransmittedCnms (u4CompId, u4PortId, u4CpIndex,
                                           &TransCnms);
        FSAP_U8_ASSIGN_HI (&u8DisFrames, DisFrames.msn);
        FSAP_U8_ASSIGN_LO (&u8DisFrames, DisFrames.lsn);
        FSAP_U8_ASSIGN_HI (&u8TransFrames, TransFrames.msn);
        FSAP_U8_ASSIGN_LO (&u8TransFrames, TransFrames.lsn);
        FSAP_U8_ASSIGN_HI (&u8TransCnms, TransCnms.msn);
        FSAP_U8_ASSIGN_LO (&u8TransCnms, TransCnms.lsn);
        FSAP_U8_2STR (&u8DisFrames, (CHR1 *) au1DisFrames);
        FSAP_U8_2STR (&u8TransFrames, (CHR1 *) au1TransFrames);
        FSAP_U8_2STR (&u8TransCnms, (CHR1 *) au1TransCnms);

        CliPrintf (CliHandle, "%-9s %-4d %-20s %-20s %-20s\r\n", piIfName,
                   u4CNPV, au1TransFrames, au1DisFrames, au1TransCnms);
    }
    return CLI_SUCCESS;
}

/******************************************************************************
 * Function Name : CnCliShowCnpvCounters
 * Description   : This function is used to display the interface and
 *                 priority specific counters per interface
 * Input(s)      : tCliHandle  - CliHandle
 *                 u4PortId   - Interface Index
 * Output(s)     : Interface and priority specific counters are displayed
 * Return(s)     : CLI_SUCCESS / CLI_FAILURE
 ******************************************************************************/
INT4
CnCliShowCnpvCounters (tCliHandle CliHandle, UINT4 u4CompId, UINT4 u4PortId)
{
    UINT4               u4Priority = CN_ZERO;

    for (u4Priority = CN_ZERO; u4Priority < CN_MAX_VLAN_PRIORITY; u4Priority++)
    {
        if (nmhValidateIndexInstanceIeee8021CnPortPriTable (u4CompId,
                                                            u4Priority,
                                                            u4PortId) ==
            SNMP_SUCCESS)
        {
            if (CnCliShowIfCnpvCounters (CliHandle, u4CompId, u4PortId,
                                         u4Priority) == CLI_FAILURE)
            {
                return CLI_FAILURE;
            }
        }
    }
    return CLI_SUCCESS;
}

/******************************************************************************
 * Function Name : CnCliShowIfCounters
 * Description   : This function is used to display the interface and
 *                 priority specific counters per CNPV
 * Input(s)      : tCliHandle  - CliHandle
 *                 u4Priority  - CNPV
 * Output(s)     : Interface and priority specific counters are displayed
 * Return(s)     : CLI_SUCCESS / CLI_FAILURE
 ******************************************************************************/
INT4
CnCliShowIfCounters (tCliHandle CliHandle, UINT4 u4Priority)
{
    UINT4               u4CompId = CN_ZERO;
    INT4                i4IfIndex = CN_ZERO;
    INT4                i4Result = CN_ZERO;
    UINT4               u4CurrentIndex = CN_ZERO;

    for (u4CompId = CN_MIN_CONTEXT; u4CompId < CN_MAX_CONTEXTS; u4CompId++)
    {
        i4Result = CnGetFirstPortInContext (u4CompId, &i4IfIndex);
        while (i4Result != OSIX_FAILURE)
        {
            u4CurrentIndex = (UINT4) i4IfIndex;
            if (nmhValidateIndexInstanceIeee8021CnPortPriTable (u4CompId,
                                                                u4Priority,
                                                                i4IfIndex) ==
                SNMP_SUCCESS)
            {
                if (CnCliShowIfCnpvCounters (CliHandle, u4CompId,
                                             (UINT4) i4IfIndex,
                                             u4Priority) == CLI_FAILURE)
                {
                    return CLI_FAILURE;
                }
            }
            i4Result = CnGetNextPortInContext (u4CompId, u4CurrentIndex,
                                               &i4IfIndex);
        }
    }
    return CLI_SUCCESS;
}

/******************************************************************************
 * Function Name : CnCliShowAllIfCnpvCounters
 * Description   : This function is used to display the interface and
 *                 priority specific counters
 * Input(s)      : tCliHandle  - CliHandle
 * Output(s)     : Interface and priority specific counters are displayed
 * Return(s)     : CLI_SUCCESS / CLI_FAILURE
 ******************************************************************************/
INT4
CnCliShowAllIfCnpvCounters (tCliHandle CliHandle)
{
    UINT4               u4CompId = CN_ZERO;
    INT4                i4IfIndex = CN_ZERO;
    UINT4               u4CurrentIndex = CN_ZERO;
    UINT4               u4Priority = CN_ZERO;
    INT4                i4Result = CN_ZERO;

    for (u4CompId = CN_MIN_CONTEXT; u4CompId < CN_MAX_CONTEXTS; u4CompId++)
    {
        i4Result = CnGetFirstPortInContext (u4CompId, &i4IfIndex);
        while (i4Result != OSIX_FAILURE)
        {
            u4CurrentIndex = (UINT4) i4IfIndex;
            for (u4Priority = CN_ZERO; u4Priority < CN_MAX_VLAN_PRIORITY;
                 u4Priority++)
            {
                if (nmhValidateIndexInstanceIeee8021CnPortPriTable (u4CompId,
                                                                    u4Priority,
                                                                    i4IfIndex)
                    == SNMP_SUCCESS)
                {
                    if (CnCliShowIfCnpvCounters (CliHandle, u4CompId,
                                                 u4CurrentIndex,
                                                 u4Priority) == CLI_FAILURE)
                    {
                        return CLI_FAILURE;
                    }
                }
            }
            i4Result = CnGetNextPortInContext (u4CompId, u4CurrentIndex,
                                               &i4IfIndex);
        }
    }
    return CLI_SUCCESS;
}

/******************************************************************************
 * Function Name : CnCliShowCnCompCnpv
 * Description   : This function is used to display the CNPV properties
                    in a component priority Table
 * Input(s)      : tCliHandle  - CliHandle
 *                 u4CompId    - ComponentId
 *                 u4Priority  - CNPV
 * Output(s)     : CNPV Properties are displayed
 * Return(s)     : None
 ******************************************************************************/
VOID
CnCliShowCnCompCnpv (tCliHandle CliHandle, UINT4 u4CompId, UINT4 u4Priority)
{

    INT4                i4CompPriDefModeChoice = CN_ZERO;
    INT4                i4CompPriAdminDefMode = CN_ZERO;
    INT4                i4CompPriLldpInstChoice = CN_ZERO;
    UINT4               u4CompPriLldpInstSelector = CN_ZERO;
    UINT4               u4CompPriAdminAltPri = CN_ZERO;
    UINT4               u4CompPriAutoAltPri = CN_ZERO;
    INT4                i4CompPriCreation = CN_ZERO;
    INT4                i4RowStatus = CN_ZERO;

    if (nmhValidateIndexInstanceIeee8021CnCompntPriTable (u4CompId, u4Priority)
        != SNMP_FAILURE)
    {
        nmhGetIeee8021CnComPriRowStatus (u4CompId, u4Priority, &i4RowStatus);
        if (i4RowStatus == CN_ACTIVE)
        {
            nmhGetIeee8021CnComPriDefModeChoice (u4CompId, u4Priority,
                                                 &i4CompPriDefModeChoice);
            nmhGetIeee8021CnComPriAdminDefenseMode (u4CompId, u4Priority,
                                                    &i4CompPriAdminDefMode);

            nmhGetIeee8021CnComPriAlternatePriority (u4CompId, u4Priority,
                                                     &u4CompPriAdminAltPri);
            nmhGetIeee8021CnComPriAutoAltPri (u4CompId, u4Priority,
                                              &u4CompPriAutoAltPri);

            nmhGetIeee8021CnComPriCreation (u4CompId, u4Priority,
                                            &i4CompPriCreation);
            nmhGetIeee8021CnComPriLldpInstanceChoice (u4CompId, u4Priority,
                                                      &i4CompPriLldpInstChoice);

            nmhGetIeee8021CnComPriLldpInstanceSelector (u4CompId, u4Priority,
                                                        &u4CompPriLldpInstSelector);

            CliPrintf (CliHandle, "CNPV %d \r\n\r\n", u4Priority);

            switch (i4CompPriDefModeChoice)
            {
                case CN_ADMIN:
                    CliPrintf (CliHandle, "DefModeChoice        : Admin\r\n");
                    break;
                case CN_AUTO:
                    CliPrintf (CliHandle, "DefModeChoice        : Auto \r\n");
                    break;
                default:
                    break;
            }
            switch (i4CompPriAdminDefMode)
            {
                case CN_DISABLED:
                    CliPrintf (CliHandle,
                               "AdminDefenseMode     : Disabled \r\n");
                    break;
                case CN_INTERIOR:
                    CliPrintf (CliHandle,
                               "AdminDefenseMode     : Interior \r\n");
                    break;
                case CN_INTERIOR_READY:
                    CliPrintf (CliHandle,
                               "AdminDefenseMode     : Interior Ready \r\n");
                    break;
                case CN_EDGE:
                    CliPrintf (CliHandle, "AdminDefenseMode     : Edge \r\n");
                    break;
                default:
                    break;
            }
            CliPrintf (CliHandle, "AlternatePriority    : %d \r\n",
                       u4CompPriAdminAltPri);
            CliPrintf (CliHandle, "AutoAltPri           : %d \r\n",
                       u4CompPriAutoAltPri);
            if (i4CompPriCreation == CN_AUTO_ENABLE)
            {

                CliPrintf (CliHandle, "Creation             : Auto Enable\r\n");
            }
            else
            {
                CliPrintf (CliHandle,
                           "Creation             : Auto Disable\r\n");
            }
            switch (i4CompPriLldpInstChoice)
            {
                case CN_LLDP_ADMIN:
                    CliPrintf (CliHandle, "LldpInstanceChoice   : Admin \r\n");
                    break;
                case CN_LLDP_NONE:
                    CliPrintf (CliHandle, "LldpInstanceChoice   : None \r\n");
                    break;
                default:
                    break;
            }
            CliPrintf (CliHandle, "LldpInstanceSelector : %d \r\n",
                       u4CompPriLldpInstSelector);

            CliPrintf (CliHandle, "----------------------------------\r\n");

            return;
        }
    }
}

/******************************************************************************
 * Function Name : CnCliShowCnCnpv
 * Description   : This function is used to display the CNPV properties
 * Input(s)      : tCliHandle  - CliHandle
 *                 u4CompId    - ComponentId
 *                 u4PortId    - IfIndex
 *                 u4Priority  - CNPV
 * Output(s)     : CNPV Properties are displayed
 * Return(s)     : None
 ******************************************************************************/
VOID
CnCliShowCnCnpv (tCliHandle CliHandle, UINT4 u4CompId, UINT4 u4PortId,
                 UINT4 u4Priority)
{
    INT4                i4PortPriDefModeChoice = CN_ZERO;
    INT4                i4PortPriAdminDefMode = CN_ZERO;
    INT4                i4PortPriLldpInstChoice = CN_ZERO;
    UINT4               u4PortPriLldpInstSelector = CN_ZERO;
    INT4                i4Index = u4PortId;
    INT4                i4OperDefenseMode = CN_ZERO;
    UINT4               u4PortPriAdminAltPri = CN_ZERO;
    INT4                i4IsErrorPortEntry = CN_ZERO;
    UINT4               u4LastEvtSentTime = CN_ZERO;
    UINT4               u4LastEvtRcvdTime = CN_ZERO;
    UINT1               au1RcvdEvnt[CN_MAX_EVENT_NAME_LEN] = { CN_ZERO };
    UINT1               au1SentEvnt[CN_MAX_EVENT_NAME_LEN] = { CN_ZERO };
    UINT4               u4OperAltPri = CN_ZERO;
    tSNMP_OCTET_STRING_TYPE RcvdEvnt;
    tSNMP_OCTET_STRING_TYPE SentEvnt;
    UINT1               au1IfName[CFA_CLI_MAX_IF_NAME_LEN] = { CN_ZERO };
    INT1               *piIfName = NULL;
    UINT1               au1TimeStr[MAX_DATE_LEN] = { CN_ZERO };
    MEMSET (&RcvdEvnt, CN_ZERO, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&SentEvnt, CN_ZERO, sizeof (tSNMP_OCTET_STRING_TYPE));

    RcvdEvnt.pu1_OctetList = au1RcvdEvnt;
    SentEvnt.pu1_OctetList = au1SentEvnt;
    piIfName = (INT1 *) &au1IfName[CN_ZERO];
    if (nmhValidateIndexInstanceIeee8021CnPortPriTable
        (u4CompId, u4Priority, i4Index) != SNMP_FAILURE)
    {

        CnCfaCliGetIfName (u4PortId, piIfName);
        CliPrintf (CliHandle, "Port %s , CNPV %d \r\n\r\n", piIfName,
                   u4Priority);

        nmhGetIeee8021CnPortPriDefModeChoice (u4CompId, u4Priority, i4Index,
                                              &i4PortPriDefModeChoice);
        nmhGetIeee8021CnPortPriAdminDefenseMode (u4CompId, u4Priority, i4Index,
                                                 &i4PortPriAdminDefMode);
        nmhGetIeee8021CnPortPriLldpInstanceChoice (u4CompId, u4Priority,
                                                   i4Index,
                                                   &i4PortPriLldpInstChoice);
        nmhGetIeee8021CnPortPriLldpInstanceSelector (u4CompId, u4Priority,
                                                     i4Index,
                                                     &u4PortPriLldpInstSelector);
        nmhGetIeee8021CnPortPriAlternatePriority (u4CompId, u4Priority, i4Index,
                                                  &u4PortPriAdminAltPri);
        nmhGetFsCnXPortPriErrorEntry (u4CompId, u4Priority, i4Index,
                                      &i4IsErrorPortEntry);
        nmhGetFsCnXPortPriLastSentEventTime (u4CompId, u4Priority, i4Index,
                                             &u4LastEvtSentTime);
        nmhGetFsCnXPortPriLastRcvdEventTime (u4CompId, u4Priority, i4Index,
                                             &u4LastEvtRcvdTime);
        nmhGetFsCnXPortPriOperDefMode (u4CompId, u4Priority, i4Index,
                                       &i4OperDefenseMode);
        nmhGetFsCnXPortPriLastRcvdEvent (u4CompId, u4Priority, i4Index,
                                         &RcvdEvnt);
        nmhGetFsCnXPortPriLastSentEvent (u4CompId, u4Priority, i4Index,
                                         &SentEvnt);
        nmhGetFsCnXPortPriOperAltPri (u4CompId, u4Priority, i4Index,
                                      &u4OperAltPri);

        switch (i4PortPriDefModeChoice)
        {
            case CN_ADMIN:
                CliPrintf (CliHandle, "DefModeChoice        : Admin\r\n");
                break;
            case CN_AUTO:
                CliPrintf (CliHandle, "DefModeChoice        : Auto \r\n");
                break;
            default:            /* CN_COMP: */
                CliPrintf (CliHandle, "DefModeChoice        : Comp \r\n");
                break;
        }
        switch (i4OperDefenseMode)
        {
            case CN_DISABLED:
                CliPrintf (CliHandle, "OperDefMode          : Disabled \r\n");
                break;
            case CN_INTERIOR:
                CliPrintf (CliHandle, "OperDefMode          : Interior \r\n");
                break;
            case CN_INTERIOR_READY:
                CliPrintf (CliHandle, "OperDefMode          : Interior Ready "
                           "\r\n");
                break;
            default:            /* CN_EDGE: */
                CliPrintf (CliHandle, "OperDefMode          : Edge \r\n");
                break;
        }
        switch (i4PortPriAdminDefMode)
        {
            case CN_DISABLED:
                CliPrintf (CliHandle, "AdminDefenseMode     : Disabled \r\n");
                break;
            case CN_INTERIOR:
                CliPrintf (CliHandle, "AdminDefenseMode     : Interior \r\n");
                break;
            case CN_INTERIOR_READY:
                CliPrintf (CliHandle, "AdminDefenseMode     : Interior Ready"
                           " \r\n");
                break;
            default:            /* CN_EDGE: */
                CliPrintf (CliHandle, "AdminDefenseMode     : Edge \r\n");
                break;
        }
        CliPrintf (CliHandle, "OperAltPri           : %d \r\n", u4OperAltPri);
        CliPrintf (CliHandle, "AlternatePriority    : %d \r\n",
                   u4PortPriAdminAltPri);
        switch (i4PortPriLldpInstChoice)
        {
            case CN_LLDP_ADMIN:
                CliPrintf (CliHandle, "LldpInstanceChoice   : Admin \r\n");
                break;
            case CN_LLDP_NONE:
                CliPrintf (CliHandle, "LldpInstanceChoice   : None \r\n");
                break;
            default:            /* CN_LLDP_COMP: */
                CliPrintf (CliHandle, "LldpInstanceChoice   : Comp \r\n");
                break;
        }
        CliPrintf (CliHandle, "LldpInstanceSelector : %d \r\n",
                   u4PortPriLldpInstSelector);

        if (i4IsErrorPortEntry == CN_ERROR_PORT_ENTRY_SET)
        {
            CliPrintf (CliHandle, "IsErroredPort        : Yes\r\n");
        }
        else
        {
            CliPrintf (CliHandle, "IsErroredPort        : No \r\n");
        }

        CliPrintf (CliHandle, "LastRcvdEvent        : %s \r\n",
                   RcvdEvnt.pu1_OctetList);
        UtlGetTimeStrForTicks (u4LastEvtRcvdTime, (CHR1 *) au1TimeStr);
        CliPrintf (CliHandle, "LastRcvdEventTime    : %s \r\n", au1TimeStr);
        CliPrintf (CliHandle, "LastSentEvent        : %s \r\n",
                   SentEvnt.pu1_OctetList);
        MEMSET (au1TimeStr, CN_ZERO, MAX_DATE_LEN);
        UtlGetTimeStrForTicks (u4LastEvtSentTime, (CHR1 *) au1TimeStr);
        CliPrintf (CliHandle, "LastSentEventTime    : %s \r\n", au1TimeStr);
        CliPrintf (CliHandle, "----------------------------------\r\n");
    }
    return;
}

/******************************************************************************
 * Function Name : CnCliShowAllCnpv
 * Description   : This function is used to display the CNPV properties
 * Input(s)      : tCliHandle  - CliHandle
 * Output(s)     : CNPV Properties are displayed
 * Return(s)     : None
 ******************************************************************************/
VOID
CnCliShowAllCnpv (tCliHandle CliHandle)
{
    UINT4               u4CompId = CN_ZERO;
    UINT4               u4CurrentIndex = CN_ZERO;
    INT4                i4IfIndex = CN_ZERO;
    INT4                i4Result = CN_ZERO;
    UINT4               u4Priority = CN_ZERO;
    UINT1               au1Alias[VCM_ALIAS_MAX_LEN] = { CN_ZERO };

    for (u4CompId = CN_MIN_CONTEXT; u4CompId < CN_MAX_CONTEXTS; u4CompId++)
    {
        if (CnPortVcmGetAliasName (u4CompId, au1Alias) != OSIX_FAILURE)
        {
            CliPrintf (CliHandle, "\r\nSwitch  %s \r\n", au1Alias);
            CliPrintf (CliHandle, "----------------------------------\r\n");

            i4Result = CnGetFirstPortInContext (u4CompId, &i4IfIndex);
            while (i4Result != OSIX_FAILURE)
            {

                u4CurrentIndex = (UINT4) i4IfIndex;
                for (u4Priority = CN_ZERO; u4Priority < CN_MAX_VLAN_PRIORITY;
                     u4Priority++)
                {
                    CnCliShowCnCnpv (CliHandle, u4CompId, u4CurrentIndex,
                                     u4Priority);
                }
                i4Result = CnGetNextPortInContext (u4CompId, u4CurrentIndex,
                                                   &i4IfIndex);
            }                    /* while */
        }
    }                            /* for u4CompId */
    return;
}

/******************************************************************************
 * Function Name : CnCliShowPriCnpv
 * Description   : This function is used to display the CNPV properties
 * Input(s)      : tCliHandle  - CliHandle
 *                 u4Priority  - CNPV
 * Output(s)     : CNPV Properties are displayed
 * Return(s)     : None
 *****************************************************************************/
VOID
CnCliShowPriCnpv (tCliHandle CliHandle, UINT4 u4Priority)
{
    UINT4               u4CompId = CN_ZERO;
    UINT4               u4CurrentIndex = CN_ZERO;
    INT4                i4IfIndex = CN_ZERO;
    INT4                i4Result = CN_ZERO;
    UINT1               au1Alias[VCM_ALIAS_MAX_LEN] = { CN_ZERO };

    for (u4CompId = CN_MIN_CONTEXT; u4CompId < CN_MAX_CONTEXTS; u4CompId++)
    {
        if (CnPortVcmGetAliasName (u4CompId, au1Alias) != OSIX_FAILURE)
        {
            CliPrintf (CliHandle, "\r\nSwitch  %s \r\n\r\n", au1Alias);

            i4Result = CnGetFirstPortInContext (u4CompId, &i4IfIndex);
            while (i4Result != OSIX_FAILURE)
            {
                u4CurrentIndex = i4IfIndex;
                CnCliShowCnCnpv (CliHandle, u4CompId, u4CurrentIndex,
                                 u4Priority);
                i4Result = CnGetNextPortInContext (u4CompId, u4CurrentIndex,
                                                   &i4IfIndex);
            }                    /* while */
        }
    }                            /* for u4CompId */
    return;
}

/******************************************************************************
 * Function Name : CnCliShowIfCnpv
 * Description   : This function is used to display the CNPV properties
 *                 per port
 * Input(s)      : tCliHandle  - CliHandle
 *                 u4PortId    - IfIndex
 * Output(s)     : CNPV Properties are displayed
 * Return(s)     : CLI_SUCCESS/CLI_FAILURE
 *****************************************************************************/
INT4
CnCliShowIfCnpv (tCliHandle CliHandle, UINT4 u4PortId)
{
    UINT4               u4CompId = CN_ZERO;
    UINT4               u4Priority = CN_ZERO;
    UINT2               u2LocalPortId = CN_ZERO;

    if (CnPortVcmGetCxtInfoFromIfIndex (u4PortId, &u4CompId,
                                        &u2LocalPortId) == OSIX_FAILURE)
    {
        return CLI_FAILURE;
    }

    for (u4Priority = CN_ZERO; u4Priority < CN_MAX_VLAN_PRIORITY; u4Priority++)
    {
        CnCliShowCnCnpv (CliHandle, u4CompId, u4PortId, u4Priority);
    }
    return CLI_SUCCESS;
}

/****************************************************************************
 * Function Name : CnCliShowCnIfCnpv
 * Description   : This function is used to display the CNPV properties
 * Input(s)      : tCliHandle  - CliHandle
 *                 u4PortId    - IfIndex
 *                 u4Priority  - CNPV
 * Output(s)     : CNPV Properties are displayed
 * Return(s)     : CLI_SUCCESS / CLI_FAILURE
 *****************************************************************************/
INT4
CnCliShowCnIfCnpv (tCliHandle CliHandle, UINT4 u4PortId, UINT4 u4Priority)
{
    UINT4               u4CompId = CN_ZERO;
    UINT2               u2LocalPortId = CN_ZERO;

    if (CnPortVcmGetCxtInfoFromIfIndex (u4PortId, &u4CompId,
                                        &u2LocalPortId) == VCM_FAILURE)
    {
        return CLI_FAILURE;
    }

    CnCliShowCnCnpv (CliHandle, u4CompId, u4PortId, u4Priority);
    return CLI_SUCCESS;
}

/******************************************************************************
 * Function Name : CnCliShowCxtCnpv
 * Description   : This function is used to display the CNPV properties
 * Input(s)      : tCliHandle  - CliHandle
 *                 u4CompId    - ComponentId
 * Output(s)     : CNPV Properties are displayed
 * Return(s)     : None
 ******************************************************************************/
VOID
CnCliShowCxtCnpv (tCliHandle CliHandle, UINT4 u4CompId)
{
    UINT4               u4CurrentIndex = CN_ZERO;
    INT4                i4IfIndex = CN_ZERO;
    INT4                i4Result = CN_ZERO;
    UINT4               u4Priority = CN_ZERO;

    i4Result = CnGetFirstPortInContext (u4CompId, &i4IfIndex);
    while (i4Result != OSIX_FAILURE)
    {
        u4CurrentIndex = i4IfIndex;
        for (u4Priority = CN_ZERO; u4Priority < CN_MAX_VLAN_PRIORITY;
             u4Priority++)
        {
            CnCliShowCnCnpv (CliHandle, u4CompId, u4CurrentIndex, u4Priority);
        }
        i4Result = CnGetNextPortInContext (u4CompId, u4CurrentIndex,
                                           &i4IfIndex);
    }                            /* while */
    return;
}

/******************************************************************************
 * Function Name : CnCliShowCnCxtCnpv
 * Description   : This function is used to display the CNPV properties
 * Input(s)      : tCliHandle  - CliHandle
 *                 u4CompId    - ComponentId
 *                 u4Priority  - CNPV   
 * Output(s)     : CNPV Properties are displayed
 * Return(s)     : None
 ******************************************************************************/
VOID
CnCliShowCnCxtCnpv (tCliHandle CliHandle, UINT4 u4CompId, UINT4 u4Priority)
{
    UINT4               u4CurrentIndex = CN_ZERO;
    INT4                i4IfIndex = CN_ZERO;
    INT4                i4Result = CN_ZERO;

    i4Result = CnGetFirstPortInContext (u4CompId, &i4IfIndex);
    while (i4Result != OSIX_FAILURE)
    {
        u4CurrentIndex = i4IfIndex;
        CnCliShowCnCnpv (CliHandle, u4CompId, u4CurrentIndex, u4Priority);
        i4Result = CnGetNextPortInContext (u4CompId, u4CurrentIndex,
                                           &i4IfIndex);
    }                            /* while */
    return;
}

/******************************************************************************
 * Function Name : CnCliShowCompCnpv
 * Description   : This function is used to display Coponent Priority Table
 * Input(s)      : tCliHandle  - CliHandle
 *                 u4CompId    - ComponentId
 *                 u4Priority  - CNPV   
 * Output(s)     : CNPV Properties are displayed
 * Return(s)     : None
 ******************************************************************************/
INT4
CnCliShowCompCnpv (tCliHandle CliHandle, UINT1 *pu1CxtName, UINT4 *pu4Priority)
{
    UINT1               au1Alias[VCM_ALIAS_MAX_LEN] = { CN_ZERO };
    UINT4               u4Priority = CN_ZERO;
    UINT4               u4CompId = CN_ZERO;
    UINT1               u1CxtFlag = OSIX_FALSE;
    UINT1               u1CNPVFlag = OSIX_FALSE;

    if (pu1CxtName != NULL)
    {
        if (CnPortVcmIsSwitchExist (pu1CxtName, &u4CompId) == OSIX_FAILURE)
        {
            CLI_SET_ERR (CN_CLI_INVALID_COMP_ID);
            return CLI_FAILURE;
        }
        u1CxtFlag = OSIX_TRUE;
    }

    if (pu4Priority != NULL)
    {
        if (*pu4Priority >= CN_MAX_VLAN_PRIORITY)
        {
            CLI_SET_ERR (CN_CLI_INVALID_CNPV);
            return CLI_FAILURE;
        }
        u1CNPVFlag = OSIX_TRUE;
    }
    if ((u1CxtFlag == OSIX_TRUE) && (u1CNPVFlag == OSIX_TRUE))
    {
        CnCliShowCnCompCnpv (CliHandle, u4CompId, *pu4Priority);
    }
    else if (u1CxtFlag == OSIX_TRUE)
    {
        for (u4Priority = CN_ZERO; u4Priority < CN_MAX_VLAN_PRIORITY;
             u4Priority++)
        {
            CnCliShowCnCompCnpv (CliHandle, u4CompId, u4Priority);
        }

    }
    else if (u1CNPVFlag == OSIX_TRUE)
    {
        for (u4CompId = CN_MIN_CONTEXT; u4CompId < CN_MAX_CONTEXTS; u4CompId++)
        {
            if (CnPortVcmGetAliasName (u4CompId, au1Alias) != OSIX_FAILURE)
            {
                CliPrintf (CliHandle, "\r\nSwitch  %s \r\n", au1Alias);
                CliPrintf (CliHandle, "----------------------------------\r\n");
                CnCliShowCnCompCnpv (CliHandle, u4CompId, *pu4Priority);
            }
        }
    }
    else
    {
        for (u4CompId = CN_MIN_CONTEXT; u4CompId < CN_MAX_CONTEXTS; u4CompId++)
        {
            if (CnPortVcmGetAliasName (u4CompId, au1Alias) != OSIX_FAILURE)
            {
                CliPrintf (CliHandle, "\r\nSwitch  %s \r\n", au1Alias);
                CliPrintf (CliHandle, "----------------------------------\r\n");
                for (u4Priority = CN_ZERO; u4Priority < CN_MAX_VLAN_PRIORITY;
                     u4Priority++)
                {
                    CnCliShowCnCompCnpv (CliHandle, u4CompId, u4Priority);
                }
            }
        }
    }
    return CLI_SUCCESS;
}

/******************************************************************************
 * Function Name : CnCliShowCnpv
 * Description   : This function is used to display the CNPV properties
 * Input(s)      : tCliHandle  - CliHandle
 *                     - ComponentId
 *                 u4Priority  - CNPV
 * Output(s)     : CNPV Properties are displayed
 * Return(s)     : CLI_SUCCESS / CLI_FAILURE
 ******************************************************************************/
INT4
CnCliShowCnpv (tCliHandle CliHandle, UINT4 u4PortId, UINT1 *pu1CxtName,
               UINT4 *pu4Priority)
{
    UINT4               u4CompId = CN_ZERO;
    UINT1               u1CxtFlag = OSIX_FALSE;
    UINT1               u1PortFlag = OSIX_FALSE;
    UINT1               u1CNPVFlag = OSIX_FALSE;

    if (pu1CxtName != NULL)
    {
        if (CnPortVcmIsSwitchExist (pu1CxtName, &u4CompId) != OSIX_SUCCESS)
        {
            CLI_SET_ERR (CN_CLI_INVALID_COMP_ID);
            return CLI_FAILURE;
        }
        u1CxtFlag = OSIX_TRUE;
    }
    if (u4PortId != CN_ZERO)
    {
        u1PortFlag = OSIX_TRUE;
    }
    if (pu4Priority != NULL)
    {
        if (*pu4Priority >= CN_MAX_VLAN_PRIORITY)
        {
            CLI_SET_ERR (CN_CLI_INVALID_CNPV);
            return CLI_FAILURE;
        }
        u1CNPVFlag = OSIX_TRUE;
    }

    if ((u1CxtFlag != OSIX_TRUE) && (u1PortFlag != OSIX_TRUE) &&
        (u1CNPVFlag != OSIX_TRUE))
    {
        CnCliShowAllCnpv (CliHandle);
    }
    else if ((u1CxtFlag == OSIX_TRUE) && (u1CNPVFlag == OSIX_TRUE))
    {
        CnCliShowCnCxtCnpv (CliHandle, u4CompId, *pu4Priority);
    }
    else if ((u1PortFlag == OSIX_TRUE) && (u1CNPVFlag == OSIX_TRUE))
    {
        if (CnCliShowCnIfCnpv (CliHandle, u4PortId, *pu4Priority) ==
            CLI_FAILURE)
        {
            return CLI_FAILURE;
        }
    }
    else if (u1CxtFlag == OSIX_TRUE)
    {
        CnCliShowCxtCnpv (CliHandle, u4CompId);
    }
    else if (u1PortFlag == OSIX_TRUE)
    {
        if (CnCliShowIfCnpv (CliHandle, u4PortId) == CLI_FAILURE)
        {
            return CLI_FAILURE;
        }
    }
    else                        /*CNPV alone is given */
    {
        CnCliShowPriCnpv (CliHandle, *pu4Priority);
    }
    return CLI_SUCCESS;
}

/******************************************************************************
 * Function Name : CnCliShowCnCpProp
 * Description   : This function is used to display the CP Properties
 * Input(s)      : tCliHandle  - CliHandle
 *                 u4CompId    - ComponentId
 *                 u4PortId    - IfIndex
 *                 u4Priority  - CNPV
 * Output(s)     : CP Properties are displayed
 * Return(s)     : CLI_SUCCESS / CLI_FAILURE
 ******************************************************************************/
VOID
CnCliShowCnCpProp (tCliHandle CliHandle, UINT4 u4CompId,
                   UINT4 u4PortId, UINT4 u4Priority)
{
    UINT4               u4CpQSizeSetPoint = CN_ZERO;
    UINT4               u4CpIndex = CN_CPINDEX_FROM_PRIO (u4Priority);
    UINT4               u4CpMinSampleBase = CN_ZERO;
    INT4                i4CnCpFeedbackWeight = CN_ZERO;
    UINT4               u4CpMinHeaderOctets = CN_ZERO;
    UINT1               au1IfName[CFA_CLI_MAX_IF_NAME_LEN] = { CN_ZERO };
    INT1               *piIfName = NULL;
    tSNMP_OCTET_STRING_TYPE CnCpIdentifier;
    UINT1               au1CpId[CN_CPID_LEN] = { CN_ZERO };
    INT4                i4Count = CN_ZERO;

    MEMSET (&CnCpIdentifier, CN_ZERO, sizeof (tSNMP_OCTET_STRING_TYPE));
    CnCpIdentifier.pu1_OctetList = au1CpId;

    piIfName = (INT1 *) &au1IfName[CN_ZERO];
    if (nmhValidateIndexInstanceIeee8021CnCpTable (u4CompId, u4PortId,
                                                   u4CpIndex) != SNMP_FAILURE)
    {
        nmhGetIeee8021CnCpQueueSizeSetPoint (u4CompId, u4PortId, u4CpIndex,
                                             &u4CpQSizeSetPoint);
        nmhGetIeee8021CnCpFeedbackWeight (u4CompId, u4PortId, u4CpIndex,
                                          &i4CnCpFeedbackWeight);
        nmhGetIeee8021CnCpMinSampleBase (u4CompId, u4PortId, u4CpIndex,
                                         &u4CpMinSampleBase);
        nmhGetIeee8021CnCpMinHeaderOctets (u4CompId, u4PortId, u4CpIndex,
                                           &u4CpMinHeaderOctets);
        nmhGetIeee8021CnCpIdentifier (u4CompId, u4PortId, u4CpIndex,
                                      &CnCpIdentifier);
        CnCfaCliGetIfName (u4PortId, piIfName);
        CliPrintf (CliHandle, "%-9s ", piIfName);
        CliPrintf (CliHandle, " %-3d ", u4Priority);

        for (i4Count = CN_ZERO; i4Count < CN_CPID_LEN; i4Count++)
        {
            CliPrintf (CliHandle, "%02x", au1CpId[i4Count]);
            if (i4Count != (CN_CPID_LEN - CN_ONE))
            {
                CliPrintf (CliHandle, ":", au1CpId[i4Count]);
            }
        }
        CliPrintf (CliHandle, " %-10u ", u4CpQSizeSetPoint);
        CliPrintf (CliHandle, "%-10d ", i4CnCpFeedbackWeight);
        CliPrintf (CliHandle, "%-10u ", u4CpMinSampleBase);
        CliPrintf (CliHandle, "%-3d\r\n", u4CpMinHeaderOctets);
    }
    return;
}

/*******************************************************************************
 * Function Name : CnCliShowCnAllCpProp
 * Description   : This function is used to display the CP Properties 
 * Input(s)      : tCliHandle  - CliHandle
 * Output(s)     : CP Properties are displayed
 * Return(s)     : CLI_SUCCESS / CLI_FAILURE
 ******************************************************************************/
VOID
CnCliShowCnAllCpProp (tCliHandle CliHandle)
{
    UINT4               u4CompId = CN_ZERO;

    for (u4CompId = CN_MIN_CONTEXT; u4CompId < CN_MAX_CONTEXTS; u4CompId++)
    {
        if (nmhValidateIndexInstanceIeee8021CnGlobalTable (u4CompId) ==
            SNMP_SUCCESS)
        {
            CnCliShowCnCxtCpProp (CliHandle, u4CompId);
        }
    }
    return;
}

/*******************************************************************************
 * Function Name : CnCliShowCnCnpvCpProp
 * Description   : This function is used to display the CP Properties per CNPV
 * Input(s)      : tCliHandle  - CliHandle
 *                 u4Priotiy   - CNPV
 * Output(s)     : CP Properties are displayed
 * Return(s)     : CLI_SUCCESS / CLI_FAILURE
 ******************************************************************************/
VOID
CnCliShowCnCnpvCpProp (tCliHandle CliHandle, UINT4 u4Priority)
{
    UINT4               u4CompId = CN_ZERO;
    UINT4               u4CurrentIndex = CN_ZERO;
    INT4                i4IfIndex = CN_ZERO;
    INT4                i4Result = CN_ZERO;

    for (u4CompId = CN_MIN_CONTEXT; u4CompId < CN_MAX_CONTEXTS; u4CompId++)
    {
        i4Result = CnGetFirstPortInContext (u4CompId, &i4IfIndex);
        while (i4Result == OSIX_SUCCESS)
        {
            u4CurrentIndex = i4IfIndex;
            CnCliShowCnCpProp (CliHandle, u4CompId, u4CurrentIndex, u4Priority);
            i4Result = CnGetNextPortInContext (u4CompId, u4CurrentIndex,
                                               &i4IfIndex);
        }
    }
    return;
}

/*******************************************************************************
 * Function Name : CnCliShowCnIfCpProp
 * Description   : This function is used to display the CP Properties per
 *                 interface
 * Input(s)      : tCliHandle  - CliHandle
 *                 u4CompId    - ComponentId
 *                 u4PortId    - IfIndex
 * Output(s)     : CP Properties are displayed
 * Return(s)     : None
 ******************************************************************************/
VOID
CnCliShowCnIfCpProp (tCliHandle CliHandle, UINT4 u4CompId, UINT4 u4PortId)
{
    UINT4               u4Priority = CN_ZERO;

    for (u4Priority = CN_ZERO; u4Priority < CN_MAX_VLAN_PRIORITY; u4Priority++)
    {
        CnCliShowCnCpProp (CliHandle, u4CompId, u4PortId, u4Priority);
    }
    return;
}

/*******************************************************************************
 * Function Name : CnCliShowCnCxtCpProp
 * Description   : This function is used to display the CP Properties per
 *                 Context
 * Input(s)      : tCliHandle  - CliHandle
 *                 u4CompId    - ComponentId
 * Output(s)     : CP Properties are displayed
 * Return(s)     : CLI_SUCCESS / CLI_FAILURE
 ******************************************************************************/
VOID
CnCliShowCnCxtCpProp (tCliHandle CliHandle, UINT4 u4CompId)
{
    UINT4               u4CurrentIndex = CN_ZERO;
    INT4                i4IfIndex = CN_ZERO;
    INT4                i4Result = CN_ZERO;

    i4Result = CnGetFirstPortInContext (u4CompId, &i4IfIndex);
    while (i4Result != OSIX_FAILURE)
    {
        u4CurrentIndex = i4IfIndex;
        CnCliShowCnIfCpProp (CliHandle, u4CompId, u4CurrentIndex);
        i4Result = CnGetNextPortInContext (u4CompId, u4CurrentIndex,
                                           &i4IfIndex);
    }
    return;
}

/*******************************************************************************
 * Function Name : CnCliShowCnCxtCnpvCpProp
 * Description   : This function is used to display the CP Properties per
 *                 Context per CNPV
 * Input(s)      : tCliHandle  - CliHandle
 *                 u4CompId    - ComponentId
 *                 u4Priority  - CNPV
 * Output(s)     : CP Properties are displayed
 * Return(s)     : CLI_SUCCESS / CLI_FAILURE
 ******************************************************************************/
VOID
CnCliShowCnCxtCnpvCpProp (tCliHandle CliHandle, UINT4 u4CompId,
                          UINT4 u4Priority)
{
    UINT4               u4CurrentIndex = CN_ZERO;
    INT4                i4IfIndex = CN_ZERO;
    INT4                i4Result = CN_ZERO;

    i4Result = CnGetFirstPortInContext (u4CompId, &i4IfIndex);
    while (i4Result != OSIX_FAILURE)
    {
        u4CurrentIndex = i4IfIndex;
        CnCliShowCnCpProp (CliHandle, u4CompId, u4CurrentIndex, u4Priority);
        i4Result = CnGetNextPortInContext (u4CompId, u4CurrentIndex,
                                           &i4IfIndex);
    }
    return;
}

/*******************************************************************************
 * Function Name : CnCliShowCpParam
 * Description   : This function is used to display the CP Properties                 
 * Input(s)      : tCliHandle  - CliHandle
 *                 u4PortId    - IfIndex
 *                 pu1CxtName  - ContextName
 *                 pu4Priority  - CNPV
 * Output(s)     : CP Properties are displayed
 * Return(s)     : CLI_SUCCESS / CLI_FAILURE
 ******************************************************************************/
INT4
CnCliShowCpParam (tCliHandle CliHandle, UINT4 u4PortId,
                  UINT1 *pu1CxtName, UINT4 *pu4Priority)
{
    UINT4               u4CompId = CN_ZERO;
    UINT2               u2LocalPortId = CN_ZERO;
    UINT1               u1CNPVFlag = OSIX_FALSE;
    UINT1               u1PortFlag = OSIX_FALSE;
    UINT1               u1CxtFlag = OSIX_FALSE;

    if (pu1CxtName != NULL)
    {
        if (CnPortVcmIsSwitchExist (pu1CxtName, &u4CompId) == OSIX_FAILURE)
        {
            CLI_SET_ERR (CN_CLI_INVALID_COMP_ID);
            return CLI_FAILURE;
        }
        u1CxtFlag = OSIX_TRUE;
    }
    if (u4PortId != CN_ZERO)
    {
        u1PortFlag = OSIX_TRUE;
        if (u1CxtFlag != OSIX_TRUE)
        {
            if (CnPortVcmGetCxtInfoFromIfIndex (u4PortId, &u4CompId,
                                                &u2LocalPortId) == VCM_FAILURE)
            {
                return CLI_FAILURE;
            }
        }
    }
    if (pu4Priority != NULL)
    {
        u1CNPVFlag = OSIX_TRUE;
    }

    CliPrintf (CliHandle, "Interface CNPV     Cp Identifier       QSizeSetPt"
               " FeedBackWt MinSamBase HdrOcts\r\n");
    CliPrintf (CliHandle, "--------- ---- ----------------------- ----------"
               " ---------- ---------- -------\r\n");

    if ((u1CxtFlag != OSIX_TRUE) && (u1PortFlag != OSIX_TRUE) &&
        (u1CNPVFlag != OSIX_TRUE))
    {
        CnCliShowCnAllCpProp (CliHandle);
    }
    else if ((u1CxtFlag == OSIX_TRUE) && (u1CNPVFlag == OSIX_TRUE))
    {
        CnCliShowCnCxtCnpvCpProp (CliHandle, u4CompId, *pu4Priority);
    }
    else if ((u1PortFlag == OSIX_TRUE) && (u1CNPVFlag == OSIX_TRUE))
    {
        CnCliShowCnCpProp (CliHandle, u4CompId, u4PortId, *pu4Priority);
    }
    else if (u1CxtFlag == OSIX_TRUE)
    {
        CnCliShowCnCxtCpProp (CliHandle, u4CompId);
    }
    else if (u1PortFlag == OSIX_TRUE)
    {
        CnCliShowCnIfCpProp (CliHandle, u4CompId, u4PortId);
    }
    else                        /*CNPV alone is given */
    {
        CnCliShowCnCnpvCpProp (CliHandle, *pu4Priority);
    }
    return CLI_SUCCESS;
}

/******************************************************************************
* Function Name : CnCliShowGlobalRunningConfig
* 
* Description   : This function prints the global configurations made in CN
*                 module
* 
* Input(s)      : tCliHandle  - CliHandle
* 
* Output(s)     : Configurations of CN module are printed
* 
* Return(s)     : CLI_SUCCESS / CLI_FAILURE
*******************************************************************************/
INT4
CnCliShowGlobalRunningConfig (tCliHandle CliHandle)
{
    INT4                i4TrapType = CN_ZERO;
    INT4                i4SysCtrl = CN_ZERO;

    CliRegisterLock (CliHandle, CnLock, CnUnLock);
    CnLock ();

    nmhGetFsCnSystemControl (&i4SysCtrl);
    if (i4SysCtrl == CN_SHUTDOWN)
    {
        CliPrintf (CliHandle, "shutdown cn\r\n");
        CnUnLock ();
        CliUnRegisterLock (CliHandle);
        return CLI_SUCCESS;
    }

    nmhGetFsCnGlobalEnableTrap (&i4TrapType);
    if (i4TrapType != CN_DEFAULT_TRAP)
    {
        if (i4TrapType == CN_CNM_TRAP)
        {
            CliPrintf (CliHandle, "no cn trap error-port-table\r\n");
        }
        else if (i4TrapType == CN_ERROR_PORT_TRAP)
        {
            CliPrintf (CliHandle, "no cn trap cnm\r\n");
        }
        else
        {
            CliPrintf (CliHandle, "no cn trap\r\n");
        }
    }
    CnUnLock ();
    CliUnRegisterLock (CliHandle);
    return CLI_SUCCESS;
}

/******************************************************************************
* Function Name : CnCliShowRunningConfig
* 
* Description   : This function prints the configurations made in CN module
* 
* Input(s)      : tCliHandle  - CliHandle
*                 u4CompId    - Component Id
*                 u4Module    - Specify Module for SRC (CN / all)
* 
* Output(s)     : Configurations of CN module are printed
* 
* Return(s)     : CLI_SUCCESS / CLI_FAILURE
*******************************************************************************/
INT4
CnCliShowRunningConfig (tCliHandle CliHandle, UINT4 u4CompId, UINT4 u4Module)
{
    UINT4               u4SysMode = CN_ZERO;
    UINT1               au1ContextName[VCM_ALIAS_MAX_LEN] = { CN_ZERO };

    UNUSED_PARAM (u4Module);

    CliRegisterLock (CliHandle, CnLock, CnUnLock);
    CnLock ();

    u4CompId = CN_CONVERT_CXT_ID_TO_COMP_ID (u4CompId);
    u4SysMode = VcmGetSystemModeExt (CN_PROTOCOL_ID);
    if (u4SysMode == VCM_MI_MODE)
    {
        MEMSET (au1ContextName, CN_ZERO, VCM_ALIAS_MAX_LEN);
        CnPortVcmGetAliasName (u4CompId, au1ContextName);
        if (STRLEN (au1ContextName) != 0)
        {

        CliPrintf (CliHandle, "\rswitch  %s \r\n\r\n", au1ContextName);
    }
    }

    CnCliShowRunningConfigScalars (CliHandle, u4CompId);
    if (u4SysMode == VCM_MI_MODE)
    {
        CliPrintf (CliHandle, "! \r\n");
    }
    CnCliShowRunningConfigInterface (CliHandle, u4CompId);

    CnUnLock ();
    CliUnRegisterLock (CliHandle);

    return CLI_SUCCESS;
}

/*****************************************************************************
*     FUNCTION NAME    : CnCliShowRunningConfigScalars                       
*                                                                           
*     DESCRIPTION      : This function displays scalar objects in CN for  
*                        show running configuration.                        
*                                                                           
*     INPUT            : CliHandle - Handle to the cli context              
*                                                                           
*     OUTPUT           : Displays the configurations made for scalar objects                                             
*                                                                           
*     RETURNS          : CLI_SUCCESS/SLI_FAILURE                                               
*                                                                           
*****************************************************************************/
VOID
CnCliShowRunningConfigScalars (tCliHandle CliHandle, UINT4 u4CompId)
{
    INT4                i4ModuleStatus = CN_ZERO;
    UINT4               u4TransPriority = CN_ZERO;
    INT4                i4Priority = CN_ZERO;
    INT4                i4Status = CN_ZERO;
    INT4                i4PriDefModeChoice = CN_ZERO;
    UINT4               u4PriAlternatePriority = CN_ZERO;
    INT4                i4PriAdminDefenseMode = CN_ZERO;
    INT4                i4PriCreation = CN_ZERO;
    INT4                i4PriLldpInstanceChoice = CN_ZERO;
    UINT4               u4PriLldpInstanceSelector = CN_ZERO;

    if (nmhValidateIndexInstanceIeee8021CnGlobalTable (u4CompId) !=
        SNMP_FAILURE)
    {
        nmhGetIeee8021CnGlobalMasterEnable (u4CompId, &i4ModuleStatus);
        /* Transmit Priority */
        nmhGetIeee8021CnGlobalCnmTransmitPriority (u4CompId, &u4TransPriority);
        if (i4ModuleStatus == OSIX_TRUE)
        {
            CliPrintf (CliHandle, "set cn enable \r\n");
        }

        if (u4TransPriority != CN_DEFAULT_CNM_PRI)
        {
            CliPrintf (CliHandle, "set cn cnm-transmit-priority %d\r\n",
                       u4TransPriority);
        }

        for (i4Priority = CN_ZERO; i4Priority < CN_MAX_VLAN_PRIORITY;
             i4Priority++)
        {
            if (nmhValidateIndexInstanceIeee8021CnCompntPriTable (u4CompId,
                                                                  i4Priority) !=
                SNMP_FAILURE)
            {
                nmhGetIeee8021CnComPriRowStatus (u4CompId, i4Priority,
                                                 &i4Status);
                if (i4Status == CN_ACTIVE)
                {
                    nmhGetIeee8021CnComPriDefModeChoice
                        (u4CompId, i4Priority, &i4PriDefModeChoice);
                    nmhGetIeee8021CnComPriAlternatePriority
                        (u4CompId, i4Priority, &u4PriAlternatePriority);
                    nmhGetIeee8021CnComPriAdminDefenseMode
                        (u4CompId, i4Priority, &i4PriAdminDefenseMode);
                    nmhGetIeee8021CnComPriCreation
                        (u4CompId, i4Priority, &i4PriCreation);
                    nmhGetIeee8021CnComPriLldpInstanceChoice
                        (u4CompId, i4Priority, &i4PriLldpInstanceChoice);
                    nmhGetIeee8021CnComPriLldpInstanceSelector
                        (u4CompId, i4Priority, &u4PriLldpInstanceSelector);

                    if (i4PriCreation != CN_AUTO_ENABLE)
                    {
                        CliPrintf (CliHandle, "cn cnpv %d port-default-defense"
                                   " auto-disable\r\n", i4Priority);
                    }
                    else
                    {
                        CliPrintf (CliHandle, "cn cnpv %d\r\n", i4Priority);
                    }

                    if (i4PriDefModeChoice != CN_AUTO)
                    {
                        CliPrintf (CliHandle, "cn cnpv %d defense on\r\n",
                                   i4Priority);

                    }
                    if (u4PriAlternatePriority != CN_ZERO)
                    {
                        CliPrintf (CliHandle, "cn cnpv %d alternate-priority"
                                   " %d\r\n", i4Priority,
                                   u4PriAlternatePriority);

                    }
                    if (i4PriAdminDefenseMode != CN_INTERIOR)
                    {
                        switch (i4PriAdminDefenseMode)
                        {
                            case CN_DISABLED:
                                CliPrintf (CliHandle, "cn cnpv %d defense-mode"
                                           " disabled\r\n", i4Priority);
                                break;
                            case CN_INTERIOR_READY:
                                CliPrintf (CliHandle, "cn cnpv %d defense-mode"
                                           " interiorReady \r\n", i4Priority);
                                break;
                            default:    /* CN_EDGE: */
                                CliPrintf (CliHandle, "cn cnpv %d defense-mode"
                                           " edge\r\n", i4Priority);
                                break;
                        }
                    }
                    if (i4PriLldpInstanceChoice != CN_LLDP_ADMIN)
                    {
                        CliPrintf (CliHandle, "cn cnpv %d lldp-instance-choice"
                                   " none \r\n", i4Priority);
                    }

                    if (u4PriLldpInstanceSelector !=
                        CN_DEF_COMP_LLDP_INST_SELECTOR)
                    {
                        CliPrintf (CliHandle,
                                   "cn cnpv %d lldp-instance-selector"
                                   " %d \r\n", i4Priority,
                                   u4PriLldpInstanceSelector);
                    }
                }
            }
            i4Status = CN_ZERO;
        }
    }
    return;
}

/*****************************************************************************/
/*     FUNCTION NAME    : CnCliShowRunningConfigInterface                    */
/*                                                                           */
/*     DESCRIPTION      : This function displays current CN   configuration  */
/*                        for interfaces                                     */
/*                                                                           */
/*     INPUT            : CliHandle - Handle to the cli context              */
/*                        u4CompId - Context Identifier                      */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/

VOID
CnCliShowRunningConfigInterface (tCliHandle CliHandle, UINT4 u4CompId)
{

#define      CN_TEMP_STR_LEN            35
#define      CN_CP_PROP_CMD_LEN         175
    INT4                i4IfIndex = CN_ZERO;
    UINT1               au1DisplayString[CN_CP_PROP_CMD_LEN] = { CN_ZERO };
    UINT1               au1TempDisplayString[CN_TEMP_STR_LEN] = { CN_ZERO };
    INT4                i4PortPriDefModeChoice = CN_ZERO;
    INT4                i4PortPriAdminDefenseMode = CN_ZERO;
    INT4                i4PortPriLldpInstanceChoice = CN_ZERO;
    UINT4               u4PortPriLldpInstanceSelector = CN_ZERO;
    UINT4               u4PortPriAlternatePriority = CN_ZERO;
    INT4                i4Priority = CN_ZERO;
    UINT4               u4CpQueueSizeSetPoint = CN_ZERO;
    INT4                i4CpFeedbackWeight = CN_ZERO;
    UINT4               u4CpMinSampleBase = CN_ZERO;
    UINT4               u4CpMinHeaderOctets = CN_ZERO;
    UINT4               u4CurrentIndex = CN_ZERO;
    INT4                i4CpIndex = CN_ZERO;
    INT4                i4Len = CN_ZERO;
    INT4                i4Result = CN_ZERO;
    INT4                i4Status = CN_ZERO;
    INT4                i4SysCtrl = CN_ZERO;
    UINT1               u1ShowFlag = TRUE;

    nmhGetFsCnSystemControl (&i4SysCtrl);

    if (i4SysCtrl == CN_SHUTDOWN)
    {
        return;
    }

    i4Result = CnGetFirstPortInContext (u4CompId, &i4IfIndex);

    while (i4Result != OSIX_FAILURE)
    {
        u1ShowFlag = TRUE;
        for (i4Priority = CN_ZERO; i4Priority < CN_MAX_VLAN_PRIORITY;
             i4Priority++)
        {
            if (nmhValidateIndexInstanceIeee8021CnCompntPriTable (u4CompId,
                                                                  i4Priority) !=
                SNMP_FAILURE)
            {

                nmhGetIeee8021CnComPriRowStatus (u4CompId, i4Priority,
                                                 &i4Status);
                if (i4Status == CN_ACTIVE)
                {
                    if (nmhValidateIndexInstanceIeee8021CnPortPriTable
                        (u4CompId, i4Priority, i4IfIndex) != SNMP_FAILURE)
                    {
                        /*Port priority Table */
                        nmhGetIeee8021CnPortPriDefModeChoice
                            (u4CompId, i4Priority, i4IfIndex,
                             &i4PortPriDefModeChoice);
                        nmhGetIeee8021CnPortPriAdminDefenseMode
                            (u4CompId, i4Priority, i4IfIndex,
                             &i4PortPriAdminDefenseMode);
                        nmhGetIeee8021CnPortPriLldpInstanceChoice
                            (u4CompId, i4Priority, i4IfIndex,
                             &i4PortPriLldpInstanceChoice);
                        nmhGetIeee8021CnPortPriLldpInstanceSelector
                            (u4CompId, i4Priority, i4IfIndex,
                             &u4PortPriLldpInstanceSelector);
                        nmhGetIeee8021CnPortPriAlternatePriority
                            (u4CompId, i4Priority, i4IfIndex,
                             &u4PortPriAlternatePriority);
                        /*CP Properties */
                        i4CpIndex = CN_CPINDEX_FROM_PRIO (i4Priority);
                        nmhGetIeee8021CnCpQueueSizeSetPoint
                            (u4CompId, i4IfIndex, i4CpIndex,
                             &u4CpQueueSizeSetPoint);
                        nmhGetIeee8021CnCpFeedbackWeight
                            (u4CompId, i4IfIndex, i4CpIndex,
                             &i4CpFeedbackWeight);
                        nmhGetIeee8021CnCpMinSampleBase
                            (u4CompId, i4IfIndex, i4CpIndex,
                             &u4CpMinSampleBase);
                        nmhGetIeee8021CnCpMinHeaderOctets
                            (u4CompId, i4IfIndex, i4CpIndex,
                             &u4CpMinHeaderOctets);
                        if (((i4PortPriDefModeChoice != CN_COMP) ||
                             (u4PortPriAlternatePriority != CN_ZERO) ||
                             (i4PortPriAdminDefenseMode != CN_DISABLED) ||
                             (i4PortPriLldpInstanceChoice != CN_LLDP_COMP) ||
                             (u4PortPriLldpInstanceSelector !=
                              CN_DEF_PORT_LLDP_INST_SELECTOR)
                             ||
                             ((u4CpQueueSizeSetPoint != CN_DEFAULT_QUEUE_SET_PT)
                              || (i4CpFeedbackWeight !=
                                  CN_DEFAULT_FEEDBACK_WEIGHT)
                              || (u4CpMinSampleBase !=
                                  CN_DEFAULT_MIN_SAMPLE_BASE)
                              || (u4CpMinHeaderOctets !=
                                  CN_DEFAULT_HEADER_OCTETS)))
                            && (u1ShowFlag == TRUE))
                        {
                            u4CurrentIndex = i4IfIndex;
                            CfaCliConfGetIfName (i4IfIndex,
                                                 (INT1 *) au1TempDisplayString);
                            CliPrintf (CliHandle, "interface %s\r\n",
                                       au1TempDisplayString);
                            MEMSET (au1TempDisplayString, CN_ZERO,
                                    CN_TEMP_STR_LEN);
                            u1ShowFlag = FALSE;

                        }
                        if (i4PortPriDefModeChoice != CN_COMP)
                        {
                            if (i4PortPriDefModeChoice == CN_ADMIN)
                            {
                                CliPrintf (CliHandle, "cn cnpv %d defense on"
                                           "\r\n", i4Priority);
                            }
                            else
                            {
                                CliPrintf (CliHandle, "cn cnpv %d defense"
                                           " auto\r\n", i4Priority);
                            }

                        }
                        if (u4PortPriAlternatePriority != CN_ZERO)
                        {
                            CliPrintf (CliHandle,
                                       "cn cnpv %d alternate-priority"
                                       " %d\r\n", i4Priority,
                                       u4PortPriAlternatePriority);

                        }
                        if (i4PortPriAdminDefenseMode != CN_DISABLED)
                        {
                            switch (i4PortPriAdminDefenseMode)
                            {
                                case CN_INTERIOR:
                                    CliPrintf (CliHandle, "cn cnpv %d"
                                               " defense-mode interior\r\n",
                                               i4Priority);
                                    break;
                                case CN_INTERIOR_READY:
                                    CliPrintf (CliHandle, "cn cnpv %d"
                                               " defense-mode interiorReady\r\n",
                                               i4Priority);
                                    break;
                                case CN_EDGE:
                                    CliPrintf (CliHandle, "cn cnpv %d"
                                               " defense-mode edge\r\n",
                                               i4Priority);
                                    break;
                                default:
                                    break;
                            }
                        }
                        if (i4PortPriLldpInstanceChoice != CN_LLDP_COMP)
                        {
                            if (i4PortPriLldpInstanceChoice == CN_LLDP_NONE)
                            {
                                CliPrintf (CliHandle, "cn cnpv %d"
                                           " lldp-instance-choice "
                                           "none\r\n", i4Priority);
                            }
                            else
                            {
                                CliPrintf (CliHandle, "cn cnpv %d"
                                           " lldp-instance-choice "
                                           "on\r\n", i4Priority);
                            }
                        }

                        if (u4PortPriLldpInstanceSelector !=
                            CN_DEF_PORT_LLDP_INST_SELECTOR)
                        {
                            CliPrintf (CliHandle, "cn cnpv %d"
                                       " lldp-instance-selector %d \r\n",
                                       i4Priority,
                                       u4PortPriLldpInstanceSelector);
                        }

                        if ((u4CpQueueSizeSetPoint != CN_DEFAULT_QUEUE_SET_PT)
                            ||
                            (i4CpFeedbackWeight != CN_DEFAULT_FEEDBACK_WEIGHT)
                            ||
                            (u4CpMinSampleBase != CN_DEFAULT_MIN_SAMPLE_BASE)
                            ||
                            (u4CpMinHeaderOctets != CN_DEFAULT_HEADER_OCTETS))
                        {

                            MEMSET (au1DisplayString, CN_ZERO,
                                    CN_CP_PROP_CMD_LEN);
                            SPRINTF ((CHR1 *) au1TempDisplayString,
                                     "cn cp-properties %d ", i4Priority);

                            MEMCPY (au1DisplayString, au1TempDisplayString,
                                    STRLEN (au1TempDisplayString));
                            i4Len = STRLEN (au1TempDisplayString);
                            if (u4CpQueueSizeSetPoint !=
                                CN_DEFAULT_QUEUE_SET_PT)
                            {

                                MEMSET (au1TempDisplayString, CN_ZERO,
                                        CN_TEMP_STR_LEN);
                                SPRINTF ((CHR1 *) au1TempDisplayString,
                                         "qsize-set-point %u ",
                                         u4CpQueueSizeSetPoint);
                                MEMCPY (au1DisplayString + i4Len,
                                        au1TempDisplayString,
                                        STRLEN (au1TempDisplayString));

                                i4Len += STRLEN (au1TempDisplayString);

                            }
                            if (i4CpFeedbackWeight !=
                                CN_DEFAULT_FEEDBACK_WEIGHT)
                            {
                                MEMSET (au1TempDisplayString, CN_ZERO,
                                        CN_TEMP_STR_LEN);
                                SPRINTF ((CHR1 *) au1TempDisplayString,
                                         "feedback-weight %d ",
                                         i4CpFeedbackWeight);
                                MEMCPY (au1DisplayString + i4Len,
                                        au1TempDisplayString,
                                        STRLEN (au1TempDisplayString));
                                i4Len += STRLEN (au1TempDisplayString);

                            }
                            if (u4CpMinSampleBase != CN_DEFAULT_MIN_SAMPLE_BASE)
                            {
                                MEMSET (au1TempDisplayString, CN_ZERO,
                                        CN_TEMP_STR_LEN);
                                SPRINTF ((CHR1 *) au1TempDisplayString,
                                         "samples %u ", u4CpMinSampleBase);
                                MEMCPY (au1DisplayString + i4Len,
                                        au1TempDisplayString,
                                        STRLEN (au1TempDisplayString));
                                i4Len += STRLEN (au1TempDisplayString);
                            }
                            if (u4CpMinHeaderOctets != CN_DEFAULT_HEADER_OCTETS)
                            {
                                MEMSET (au1TempDisplayString, CN_ZERO,
                                        CN_TEMP_STR_LEN);
                                SPRINTF ((CHR1 *) au1TempDisplayString,
                                         "header-octets %u ",
                                         u4CpMinHeaderOctets);
                                MEMCPY (au1DisplayString + i4Len,
                                        au1TempDisplayString,
                                        STRLEN (au1TempDisplayString));
                                i4Len += STRLEN (au1TempDisplayString);
                            }
                            CliPrintf (CliHandle, "%s \r\n", au1DisplayString);
                        }
                    }
                }
            }
            i4Status = CN_ZERO;
        }
        CliPrintf (CliHandle, "!");
        CliPrintf (CliHandle, "\r\n");
        u4CurrentIndex = i4IfIndex;
        i4Result = CnGetNextPortInContext (u4CompId, u4CurrentIndex,
                                           &i4IfIndex);
    }
    return;
}

/*****************************************************************************/
/*     FUNCTION NAME    : IssCnShowDebugging                                 */
/*                                                                           */
/*     DESCRIPTION      : This function prints CN Debug level                */
/*                        for interfaces                                     */
/*                                                                           */
/*     INPUT            : None                                               */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : None                                               */
/*                                                                           */
/*****************************************************************************/
VOID
IssCnShowDebugging (tCliHandle CliHandle)
{
    INT4                i4TraceLevel = CN_ZERO;
    INT4                i4Status = CN_ZERO;

    nmhGetFsCnSystemControl (&i4Status);
    if (i4Status != CN_SHUTDOWN)
    {
        if (nmhGetFsCnXGlobalTraceLevel (CN_DEFAULT_CONTEXT,
                                         &i4TraceLevel) == SNMP_FAILURE)
        {
            return;
        }

        CliPrintf (CliHandle, "\rCN :");
        if (i4TraceLevel == CN_ALL_TRC)
        {
            CliPrintf (CliHandle, "\r\n  CN all debugging is on");
            return;
        }
        if (i4TraceLevel & CN_MGMT_TRC)
        {
            CliPrintf (CliHandle, "\r\n  CN management debugging is on");
        }
        if (i4TraceLevel & CN_RESOURCE_TRC)
        {
            CliPrintf (CliHandle, "\r\n  CN resource debugging is on");
        }
        if (i4TraceLevel & CN_ALL_FAILURE_TRC)
        {
            CliPrintf (CliHandle, "\r\n  CN failure debugging is on");
        }
        if (i4TraceLevel & CN_CONTROL_PLANE_TRC)
        {
            CliPrintf (CliHandle, "\r\n  CN control plane debugging is on");
        }
        if (i4TraceLevel & CN_SEM_TRC)
        {
            CliPrintf (CliHandle, "\r\n  CN SEM debugging is on");
        }
        if (i4TraceLevel & CN_TLV_TRC)
        {
            CliPrintf (CliHandle, "\r\n  CN TLV debugging is on");
        }
        if (i4TraceLevel & CN_RED_TRC)
        {
            CliPrintf (CliHandle, "\r\n  CN redundancy debugging is on");
        }
        CliPrintf (CliHandle, "\r\n");
    }
    return;
}
#endif
/* End of File */
