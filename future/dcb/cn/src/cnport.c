/*****************************************************************************
 * Copyright (C) 2010 Aricent Inc . All Rights Reserved
 *
 * $Id: cnport.c,v 1.12 2014/07/21 11:31:46 siva Exp $
 *
 * Description: This file contains the portable routines which calls APIs
 *              given by other modules.
 ******************************************************************************/

#include "cninc.h"
#include "snmputil.h"

/***************************************************************************
 * FUNCTION NAME    : CnPortVcmGetCxtInfoFromIfIndex 
 *
 * DESCRIPTION      : Routine used to get the Context Information from
 *                    ifIndex.
 *
 * INPUT            : u4IfIndex - Port Identifier
 *
 * OUTPUT           : pu4ContextId - Context Identifier
 *                    pu2LocalPortId - Local Port Identifier
 * 
 * RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 * 
 **************************************************************************/
PUBLIC INT4
CnPortVcmGetCxtInfoFromIfIndex (UINT4 u4IfIndex, UINT4 *pu4ContextId,
                                UINT2 *pu2LocalPortId)
{
    INT4                i4RetVal = OSIX_FAILURE;

    i4RetVal = VcmGetContextInfoFromIfIndex (u4IfIndex, pu4ContextId,
                                             pu2LocalPortId);
    if (i4RetVal != VCM_SUCCESS)
    {
        return (OSIX_FAILURE);
    }
    *pu4ContextId = CN_CONVERT_CXT_ID_TO_COMP_ID (*pu4ContextId);
    return (OSIX_SUCCESS);
}

/***************************************************************************
 * FUNCTION NAME    : CnPortVcmGetContextPortList
 *
 * DESCRIPTION      : Routine used to get the Context Information from
 *                    ifIndex.
 *
 * INPUT            : u4ContextId - context Identifier
 *
 * OUTPUT           : tPortList - port list array
 *
 * RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *
 **************************************************************************/
PUBLIC INT4
CnPortVcmGetContextPortList (UINT4 u4ContextId, tPortList PortList)
{
    u4ContextId = CN_CONVERT_COMP_ID_TO_CXT_ID (u4ContextId);
    if (VcmGetContextPortList (u4ContextId, PortList) == VCM_FAILURE)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : CnPortVcmGetAliasName
 *
 * DESCRIPTION      : Routine used to get the Alias Name for the context
 *
 * INPUT            : u4ContextId - Context Identifier
 *
 * OUTPUT           : pu1Alias - Context Name
 *
 * RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *
 **************************************************************************/
PUBLIC INT4
CnPortVcmGetAliasName (UINT4 u4ContextId, UINT1 *pu1Alias)
{
    INT4                i4RetVal = VCM_FAILURE;

    u4ContextId = CN_CONVERT_COMP_ID_TO_CXT_ID (u4ContextId);
    i4RetVal = VcmGetAliasName (u4ContextId, pu1Alias);

    return (((i4RetVal == VCM_FAILURE) ? OSIX_FAILURE : OSIX_SUCCESS));
}

/***************************************************************************
 * FUNCTION NAME    : CnPortVcmIsSwitchExist
 *
 * DESCRIPTION      : Routine used to get the context Id for the Alias Name
 *
 * INPUT            : pu1Alias - Context Name
 *
 * OUTPUT           : u4ContextId - Context Identifier
 *
 * RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *
 * ***************************************************************************/
PUBLIC INT4
CnPortVcmIsSwitchExist (UINT1 *pu1Alias, UINT4 *pu4ContextId)
{
    INT4                i4RetVal = OSIX_FAILURE;

    if (VcmIsSwitchExist (pu1Alias, pu4ContextId) == VCM_TRUE)
    {
        *pu4ContextId = CN_CONVERT_CXT_ID_TO_COMP_ID (*pu4ContextId);
        i4RetVal = OSIX_SUCCESS;
    }

    return i4RetVal;

}

/***************************************************************************
 * FUNCTION NAME    : CnPortVcmGetCxtInfoFromIfIndex
 *
 * DESCRIPTION      : Routine used to get the first Context ID.
 *
 * INPUT            : pu4ContextId - pointer to context ID
 *
 * OUTPUT           : u4ContextId - Context Identifier
 *
 * RETURNS          : None
 *
 **************************************************************************/
PUBLIC VOID
CnPortVcmGetFirstVcId (UINT4 *pu4ContextId)
{
    INT4                i4RetValue = 0;
    UNUSED_PARAM (i4RetValue);

    i4RetValue = VcmGetFirstActiveContext (pu4ContextId);
    *pu4ContextId = CN_CONVERT_CXT_ID_TO_COMP_ID (*pu4ContextId);
    return;

}

/***************************************************************************
 * FUNCTION NAME    : CnPortVcmGetNextVcId
 *
 * DESCRIPTION      : Routine used to get the next Context ID
 *
 * INPUT            : u4ContextId - Context Identifier
 *
 * OUTPUT           : pu4NextContextId - pointer to next context Identifier
 *
 * RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *
 **************************************************************************/
PUBLIC INT4
CnPortVcmGetNextVcId (UINT4 u4ContextId, UINT4 *pu4NextContextId)
{
    INT4                i4RetVal = OSIX_FAILURE;

    u4ContextId = CN_CONVERT_COMP_ID_TO_CXT_ID (u4ContextId);

    i4RetVal = VcmGetNextActiveContext (u4ContextId, pu4NextContextId);

    if (i4RetVal == VCM_FAILURE)
    {
        return OSIX_FAILURE;
    }

    *pu4NextContextId = CN_CONVERT_CXT_ID_TO_COMP_ID (*pu4NextContextId);

    return OSIX_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : CnPortCfaGetIfInfo 
 *
 * DESCRIPTION      : This function returns the interface related params
 *                    (Duplexity) to the external modules.
 *
 * INPUT            : u4IfIndex - Port Identifier
 *
 * OUTPUT           : pIfInfo   - Pointer to the tCfaIfInfo
 * 
 * RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 * 
 **************************************************************************/
PUBLIC INT4
CnPortCfaGetIfInfo (UINT4 u4IfIndex, tCfaIfInfo * pIfInfo)
{
    INT4                i4RetVal = OSIX_FAILURE;

    i4RetVal = CfaGetIfInfo (u4IfIndex, pIfInfo);

    if (i4RetVal != CFA_SUCCESS)
    {
        return (OSIX_FAILURE);
    }
    return (OSIX_SUCCESS);
}

/***************************************************************************
 * FUNCTION NAME    : CnPortGetPortDuplexity
 *
 * DESCRIPTION      : This function returns the interface's Duplexity 
 *
 * INPUT            : u4IfIndex - Port Identifier
 *
 * OUTPUT           : pi4PortDuplexity  - Duplexity of the port
 * 
 * RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 * 
 **************************************************************************/
PUBLIC INT4
CnPortGetPortDuplexity (UINT4 u4PortId, INT4 *pi4PortDuplexity)
{
    INT4                i4RetVal = OSIX_FAILURE;

#ifdef NPAPI_WANTED
    i4RetVal = IssGetPortCtrlDuplex (u4PortId, pi4PortDuplexity);

    if (i4RetVal != ISS_SUCCESS)
    {
        return (OSIX_FAILURE);
    }
#else
    *pi4PortDuplexity = CN_DUP_FULL;
    UNUSED_PARAM (u4PortId);
    UNUSED_PARAM (i4RetVal);
#endif
    return (OSIX_SUCCESS);
}

/***************************************************************************
 * FUNCTION NAME    : CnPortLldpApplRegWithL2iwf
 *
 * DESCRIPTION      : This function will register with L2IWF as a LLDP 
 *                     application.
 *
 * INPUT            : NONE
 *
 * OUTPUT           : NONE
 *
 * RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *
 **************************************************************************/
PUBLIC INT4
CnPortLldpApplRegWithL2iwf (VOID)
{
    tL2LldpAppInfo      L2LldpAppInfo;

    MEMSET (&L2LldpAppInfo, CN_ZERO, sizeof (tL2LldpAppInfo));

    L2LldpAppInfo.LldpAppId.u2TlvType = CN_TLV_TYPE;
    L2LldpAppInfo.LldpAppId.u1SubType = CN_TLV_SUB_TYPE;
    MEMCPY (L2LldpAppInfo.LldpAppId.au1OUI, gau1CnOUI, CN_TLV_OUI_LEN);
    L2LldpAppInfo.pAppCallBackFn = CnApiApplCallbkFunc;

    if (L2IwfLldpHandleApplRequest (&L2LldpAppInfo, L2IWF_LLDP_APPL_REGISTER)
        == OSIX_FAILURE)
    {
        return (OSIX_FAILURE);
    }
    return (OSIX_SUCCESS);
}

/***************************************************************************
 * FUNCTION NAME    : CnPortLldpApplDeRegWithL2iwf
 *
 * DESCRIPTION      : This function will register with L2IWF as a LLDP
 *                    application..
 *
 * INPUT            : NONE
 *
 * OUTPUT           : NONE
 *
 * RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *
 **************************************************************************/
PUBLIC INT4
CnPortLldpApplDeRegWithL2iwf (VOID)
{
    tL2LldpAppInfo      L2LldpAppInfo;

    MEMSET (&L2LldpAppInfo, CN_ZERO, sizeof (tL2LldpAppInfo));

    L2LldpAppInfo.LldpAppId.u2TlvType = CN_TLV_TYPE;
    L2LldpAppInfo.LldpAppId.u1SubType = CN_TLV_SUB_TYPE;
    MEMCPY (L2LldpAppInfo.LldpAppId.au1OUI, gau1CnOUI, CN_TLV_OUI_LEN);
    L2LldpAppInfo.pAppCallBackFn = CnApiApplCallbkFunc;

    if (L2IwfLldpHandleApplRequest (&L2LldpAppInfo, L2IWF_LLDP_APPL_DEREGISTER)
        == OSIX_FAILURE)
    {
        return (OSIX_FAILURE);
    }
    return (OSIX_SUCCESS);
}

/***************************************************************************
 * FUNCTION NAME    : CnPortPostCnMsgToLldp
 *
 * DESCRIPTION      : This function will post the CN Message to LLDP
 *
 * INPUT            : u1MsgType - should have value UPDATE/REG/DEREG
 *                    u4IfIndex - Port Id
 *                    pLldpAppPortMsg - Pointer to LLDP port Message
 *                     structure 
 *
 * OUTPUT           : NONE
 *
 * RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *
 **************************************************************************/
PUBLIC INT4
CnPortPostCnMsgToLldp (UINT1 u1MsgType, UINT4 u4IfIndex, tLldpAppPortMsg
                       * pLldpAppPortMsg)
{
    UINT4               u4ContextId = CN_INVALID_CONTEXT;
    UINT2               u2LocalPortId = 0;

    CnPortVcmGetCxtInfoFromIfIndex (u4IfIndex, &u4ContextId, &u2LocalPortId);

    switch (u1MsgType)
    {
        case CN_MSG_PORT_UPDATE:

            if (L2IwfLldpHandleApplPortRequest (u4IfIndex,
                                                pLldpAppPortMsg,
                                                L2IWF_LLDP_APPL_PORT_UPDATE)
                != OSIX_SUCCESS)
            {
                if (u4ContextId != CN_INVALID_CONTEXT)
                {
                    CN_TRC_ARG1 (u4ContextId, (ALL_FAILURE_TRC | CN_TLV_TRC),
                                 "CnPortPostCnMsgToLldp: "
                                 "L2IwfLldpHandleApplPortRequest "
                                 "FAILED for update request on port %u \r\n",
                                 u4IfIndex);
                }
                return (OSIX_FAILURE);
            }
            break;

        case CN_MSG_PORT_REG:

            if (L2IwfLldpHandleApplPortRequest (u4IfIndex,
                                                pLldpAppPortMsg,
                                                L2IWF_LLDP_APPL_PORT_REGISTER)
                != OSIX_SUCCESS)
            {
                if (u4ContextId != CN_INVALID_CONTEXT)
                {
                    CN_TRC_ARG1 (u4ContextId, (ALL_FAILURE_TRC | CN_TLV_TRC),
                                 "CnPortPostCnMsgToLldp: "
                                 "L2IwfLldpHandleApplPortRequest FAILED"
                                 "for register request on port %u\r\n",
                                 u4IfIndex);
                }
                return (OSIX_FAILURE);

            }
            break;

        case CN_MSG_PORT_DEREG:

            if (L2IwfLldpHandleApplPortRequest (u4IfIndex,
                                                pLldpAppPortMsg,
                                                L2IWF_LLDP_APPL_PORT_DEREGISTER)
                != OSIX_SUCCESS)
            {
                if (u4ContextId != CN_INVALID_CONTEXT)
                {
                    CN_TRC_ARG1 (u4ContextId, (ALL_FAILURE_TRC | CN_TLV_TRC),
                                 "CnPortPostCnMsgToLldp: "
                                 "L2IwfLldpHandleApplPortRequest FAILED"
                                 "for deregister request on port %u\r\n",
                                 u4IfIndex);
                }
                return (OSIX_FAILURE);

            }
            break;
        default:
            CN_GBL_TRC_ARG1 (CN_INVALID_CONTEXT,
                             (CN_ALL_FAILURE_TRC | CN_TLV_TRC),
                             "CnPortPostCnMsgToLldp: Unknown message type "
                             "received on Port %u\r\n", u4IfIndex);
            break;
    }
    return (OSIX_SUCCESS);
}

/***************************************************************************
 * FUNCTION NAME    : CnPortGetTctoPriority
 *
 * DESCRIPTION      : This function will get the Traffic class from priority
 *
 * INPUT            :  u1CNPV - CNPV 
 *                     u4IfIndex - Port ID
 *
 * OUTPUT           : u4TrafficClass - Traffic class
 *
 * RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *
 **************************************************************************/
PUBLIC INT4
CnPortGetTctoPriority (UINT1 u1CNPV, UINT4 u4IfIndex, UINT4 *u4TrafficClass)
{
    if (QosApiGetPortPriToTCMapping (u4IfIndex, u1CNPV, u4TrafficClass)
        != OSIX_SUCCESS)
    {
        return (OSIX_FAILURE);
    }
    return (OSIX_SUCCESS);
}

/****************************************************************************
 *
 *    FUNCTION NAME    : CnPortNotifyFaults
 *
 *    DESCRIPTION      : This function Sends the trap message to the Fault
 *                       Manager.
 *
 *    INPUT            : tSNMP_VAR_BIND * - pointer the trapmessage.
 *                       pu1Msg  *        - Pointer to the log message.
 *                       u4ModuleId       - ModuleId
 *
 *    OUTPUT           : Calls the FmApiNotifyFaults to send the
 *                       Trap message to FaultManager.
 *
 *    RETURNS          : OSIX_SUCCESS/ OSIX_FAILURE
 *
 ****************************************************************************/

INT4
CnPortNotifyFaults (tSNMP_VAR_BIND * pTrapMsg, UINT1 *pu1Msg, UINT4 u4ModuleId)
{
#ifdef FM_WANTED
    tFmFaultMsg         FmFaultMsg;

    MEMSET (&FmFaultMsg, CN_ZERO, sizeof (tFmFaultMsg));

    FmFaultMsg.pTrapMsg = pTrapMsg;
    FmFaultMsg.pSyslogMsg = pu1Msg;
    FmFaultMsg.u4ModuleId = u4ModuleId;
    if (FmApiNotifyFaults (&FmFaultMsg) == OSIX_FAILURE)
    {
        CN_GBL_TRC (CN_INVALID_CONTEXT, CN_ALL_FAILURE_TRC,
                    "CnPortNotifyFaults: Sending "
                    "Notfication to FM Failed \r\n");
        SNMP_free_snmp_vb_list (pTrapMsg);
        return OSIX_FAILURE;
    }
#else
    SNMP_free_snmp_vb_list (pTrapMsg);
    UNUSED_PARAM (pu1Msg);
    UNUSED_PARAM (u4ModuleId);
#endif
    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : CnPortQosPriorityMapReq
 *
 *    DESCRIPTION      : This function invokes the QOS API to process the 
 *                       priority map request from CN module.
 *
 *    INPUT            : 
 *
 *    OUTPUT           : Calls the FmApiNotifyFaults to send the
 *                       Trap message to FaultManager.
 *
 *    RETURNS          : OSIX_SUCCESS/ OSIX_FAILURE
 *
 ****************************************************************************/
PUBLIC INT4
CnPortQosPriorityMapReq (UINT4 u4CompId, UINT4 u4Port, UINT1 u1InPri,
                         UINT1 *pu1OutPri, UINT1 u1Request)
{
    INT4                i4RetVal = OSIX_FAILURE;

    /* To handle warnings in case of Trace is not defined */
    UNUSED_PARAM (u4CompId);

    if ((u1Request == QOS_ADD_PRIORITY_MAP)
        || (u1Request == QOS_DEL_PRIORITY_MAP))
    {
        CN_TRC_ARG5 (u4CompId, CN_CONTROL_PLANE_TRC,
                     "CnPortQosPriorityMapReq: called with "
                     "ContextId: %u, Port: %u, InPri: %d, OutPri: %d, Type: %d\r\n",
                     u4CompId, u4Port, u1InPri, *pu1OutPri, u1Request);
    }

    i4RetVal = QosApiHandlePriorityMapRequest (u4Port, u1InPri, pu1OutPri,
                                               u1Request);

    return i4RetVal;
}

/*****************************************************************************/
/* Function Name      : CnCfaCliGetIfName                                    */
/*                                                                           */
/* Description        : This function calls the CFA Module to get the        */
/*                      interface name from the given interface index.       */
/*                                                                           */
/* Input(s)           : u4IfIndex - Interface index                          */
/*                                                                           */
/* Output(s)          : pi1IfName - Interface name of the given IfIndex.     */
/*                                                                           */
/* Return Value(s)    : -                                                 */
/*                                                                           */
/*****************************************************************************/
INT4
CnCfaCliGetIfName (UINT4 u4IfIndex, INT1 *pi1IfName)
{
    return (CfaCliGetIfName (u4IfIndex, pi1IfName));
}

/*****************************************************************************/
/* Function Name      : CnPortIsPortInPortChannel                            */
/*                                                                           */
/* Description        : This function calls L2IWF Module and returns         */
/*                      whether the given port is part of any port channel   */
/*                                                                           */
/* Input(s)           : u4IfIndex - Interface index                          */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : OSIX_SUCCESS / OSIX_FAILURE                          */
/*                                                                           */
/*****************************************************************************/
INT4
CnPortIsPortInPortChannel (UINT4 u4IfIndex)
{
    if (L2IwfIsPortInPortChannel (u4IfIndex) == L2IWF_SUCCESS)
    {
        return OSIX_SUCCESS;
    }

    return OSIX_FAILURE;
}

/*--------------------------------------------------------------------------*/
/*                       End of the file  cnport.c                          */
/*--------------------------------------------------------------------------*/
