/********************************************************************
* Copyright (C) 2009Aricent Inc . All Rights Reserved
*
* $Id: fscnlw.c,v 1.7 2011/09/24 06:38:24 siva Exp $
*
* Description: Protocol Low Level Routines
*********************************************************************/

#include  "lr.h"
#include  "fssnmp.h"
#include  "cninc.h"
#include  "cncli.h"

/****************************************************************************
 Function    :  nmhGetFsCnSystemControl
 Input       :  The Indices

                The Object 
                retValFsCnSystemControl
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsCnSystemControl (INT4 *pi4RetValFsCnSystemControl)
{
    *pi4RetValFsCnSystemControl = (INT4) gCnMasterTblInfo.u1CnSysControl;
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsCnGlobalEnableTrap
 Input       :  The Indices

                The Object 
                retValFsCnGlobalEnableTrap
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsCnGlobalEnableTrap (INT4 *pi4RetValFsCnGlobalEnableTrap)
{
    if (CN_IS_SHUTDOWN ())
    {
        /*CN is shutdown. So return Default value */
        *pi4RetValFsCnGlobalEnableTrap = (INT4) CN_DEFAULT_TRAP;
    }
    *pi4RetValFsCnGlobalEnableTrap = (INT4) gCnMasterTblInfo.u4GlobalTrapStatus;
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsCnSystemControl
 Input       :  The Indices

                The Object 
                setValFsCnSystemControl
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsCnSystemControl (INT4 i4SetValFsCnSystemControl)
{

    if (gCnMasterTblInfo.u1CnSysControl == i4SetValFsCnSystemControl)
    {
        return SNMP_SUCCESS;

    }
    if (i4SetValFsCnSystemControl == CN_STARTED)
    {
        if (CnModuleStart () == OSIX_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        CnModuleShutdown ();
    }
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetFsCnGlobalEnableTrap
 Input       :  The Indices

                The Object 
                setValFsCnGlobalEnableTrap
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsCnGlobalEnableTrap (INT4 i4SetValFsCnGlobalEnableTrap)
{

    if (gCnMasterTblInfo.u4GlobalTrapStatus ==
        (UINT4) i4SetValFsCnGlobalEnableTrap)
    {
        return SNMP_SUCCESS;
    }
    gCnMasterTblInfo.u4GlobalTrapStatus = (UINT4) i4SetValFsCnGlobalEnableTrap;
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsCnSystemControl
 Input       :  The Indices

                The Object 
                testValFsCnSystemControl
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsCnSystemControl (UINT4 *pu4ErrorCode,
                            INT4 i4TestValFsCnSystemControl)
{
    if ((i4TestValFsCnSystemControl != CN_SHUTDOWN) &&
        (i4TestValFsCnSystemControl != CN_STARTED))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsCnGlobalEnableTrap
 Input       :  The Indices

                The Object 
                testValFsCnGlobalEnableTrap
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsCnGlobalEnableTrap (UINT4 *pu4ErrorCode,
                               INT4 i4TestValFsCnGlobalEnableTrap)
{
    if (CN_IS_SHUTDOWN ())
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    if ((i4TestValFsCnGlobalEnableTrap < CN_ZERO) ||
        (i4TestValFsCnGlobalEnableTrap > CN_ALL_TRAP))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsCnSystemControl
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsCnSystemControl (UINT4 *pu4ErrorCode,
                           tSnmpIndexList * pSnmpIndexList,
                           tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsCnGlobalEnableTrap
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsCnGlobalEnableTrap (UINT4 *pu4ErrorCode,
                              tSnmpIndexList * pSnmpIndexList,
                              tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsCnXGlobalTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsCnXGlobalTable
 Input       :  The Indices
                Ieee8021CnGlobalComponentId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsCnXGlobalTable (UINT4 u4Ieee8021CnGlobalComponentId)
{
    if (CN_IS_SHUTDOWN ())
    {
        return SNMP_FAILURE;
    }
    if (CN_IS_VALID_COMP_ID (u4Ieee8021CnGlobalComponentId) == OSIX_FALSE)
    {
        return SNMP_FAILURE;
    }
    if (CN_COMP_TBL_ENTRY (u4Ieee8021CnGlobalComponentId) == NULL)
    {
        CLI_SET_ERR (CN_CLI_COMP_ENTRY_NOT_FOUND);
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsCnXGlobalTable
 Input       :  The Indices
                Ieee8021CnGlobalComponentId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsCnXGlobalTable (UINT4 *pu4Ieee8021CnGlobalComponentId)
{
    *pu4Ieee8021CnGlobalComponentId = CN_MIN_CONTEXT;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsCnXGlobalTable
 Input       :  The Indices
                Ieee8021CnGlobalComponentId
                nextIeee8021CnGlobalComponentId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsCnXGlobalTable (UINT4 u4Ieee8021CnGlobalComponentId,
                                 UINT4 *pu4NextIeee8021CnGlobalComponentId)
{
    UINT4               u4CompId = CN_MIN_CONTEXT;

    for (u4CompId = u4Ieee8021CnGlobalComponentId + CN_ONE;
         u4CompId < CN_MAX_CONTEXTS; u4CompId++)
    {
        if (CN_COMP_TBL_ENTRY (u4CompId) != NULL)
        {
            *pu4NextIeee8021CnGlobalComponentId = u4CompId;
            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsCnXGlobalTraceLevel
 Input       :  The Indices
                Ieee8021CnGlobalComponentId

                The Object 
                retValFsCnXGlobalTraceLevel
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsCnXGlobalTraceLevel (UINT4 u4Ieee8021CnGlobalComponentId,
                             INT4 *pi4RetValFsCnXGlobalTraceLevel)
{
    if (CN_COMP_TBL_ENTRY (u4Ieee8021CnGlobalComponentId) != NULL)
    {
        *pi4RetValFsCnXGlobalTraceLevel =
            (INT4) (CN_COMP_TBL_ENTRY (u4Ieee8021CnGlobalComponentId)->
                    u4TraceLevel);
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsCnXGlobalClearCounters
 Input       :  The Indices
                Ieee8021CnGlobalComponentId

                The Object 
                retValFsCnXGlobalClearCounters
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsCnXGlobalClearCounters (UINT4 u4Ieee8021CnGlobalComponentId,
                                INT4 *pi4RetValFsCnXGlobalClearCounters)
{
    UNUSED_PARAM (u4Ieee8021CnGlobalComponentId);
    *pi4RetValFsCnXGlobalClearCounters = CN_RESET_CLEAR_COUNTER;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsCnXGlobalTLVErrors
 Input       :  The Indices
                Ieee8021CnGlobalComponentId

                The Object 
                retValFsCnXGlobalTLVErrors
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsCnXGlobalTLVErrors (UINT4 u4Ieee8021CnGlobalComponentId,
                            UINT4 *pu4RetValFsCnXGlobalTLVErrors)
{
    if (CN_COMP_TBL_ENTRY (u4Ieee8021CnGlobalComponentId) != NULL)
    {
        *pu4RetValFsCnXGlobalTLVErrors =
            CN_COMP_TBL_ENTRY (u4Ieee8021CnGlobalComponentId)->u4TlvErrors;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsCnXGlobalTraceLevel
 Input       :  The Indices
                Ieee8021CnGlobalComponentId

                The Object 
                setValFsCnXGlobalTraceLevel
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsCnXGlobalTraceLevel (UINT4 u4Ieee8021CnGlobalComponentId,
                             INT4 i4SetValFsCnXGlobalTraceLevel)
{
    tCnCompTblInfo     *pCnCompTblEntry = NULL;

    pCnCompTblEntry = CN_COMP_TBL_ENTRY (u4Ieee8021CnGlobalComponentId);

    if (pCnCompTblEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pCnCompTblEntry->u4TraceLevel = (UINT4) i4SetValFsCnXGlobalTraceLevel;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsCnXGlobalClearCounters
 Input       :  The Indices
                Ieee8021CnGlobalComponentId

                The Object 
                setValFsCnXGlobalClearCounters
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsCnXGlobalClearCounters (UINT4 u4Ieee8021CnGlobalComponentId,
                                INT4 i4SetValFsCnXGlobalClearCounters)
{
    tCnPortTblInfo     *pCnPortTblEntry = NULL;
    tCnPortTblInfo     *pCnTempPortTblEntry = NULL;
    tCnPortPriTblInfo  *pCnPortPriEntry = NULL;
    tCnCompTblInfo     *pCnCompTblEntry = NULL;
    UINT4               u4Priority = CN_ZERO;
    UINT4               u4PortId = CN_ZERO;

    pCnCompTblEntry = CN_COMP_TBL_ENTRY (u4Ieee8021CnGlobalComponentId);

    if (pCnCompTblEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    if (i4SetValFsCnXGlobalClearCounters == CN_SET_CLEAR_COUNTER)
    {
        pCnCompTblEntry->u4TlvErrors = CN_ZERO;

        pCnPortTblEntry = (tCnPortTblInfo *) RBTreeGetFirst
            (pCnCompTblEntry->PortTbl);
        while (pCnPortTblEntry != NULL)
        {
            u4PortId = pCnPortTblEntry->u4PortId;
            for (u4Priority = CN_ZERO; u4Priority < CN_MAX_VLAN_PRIORITY;
                 u4Priority++)
            {
                CN_PORT_PRI_ENTRY (u4Ieee8021CnGlobalComponentId, u4PortId,
                                   u4Priority, pCnPortPriEntry);
                if (pCnPortPriEntry != NULL)
                {

                    /*Configure NPAPI */
                    CnHwReSetCounters (u4Ieee8021CnGlobalComponentId,
                                       u4PortId, ((UINT1) u4Priority));
                }
            }
            pCnTempPortTblEntry = pCnPortTblEntry;
            pCnPortTblEntry = (tCnPortTblInfo *) RBTreeGetNext
                (pCnCompTblEntry->PortTbl, pCnTempPortTblEntry, NULL);
        }
    }
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsCnXGlobalTraceLevel
 Input       :  The Indices
                Ieee8021CnGlobalComponentId

                The Object 
                testValFsCnXGlobalTraceLevel
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsCnXGlobalTraceLevel (UINT4 *pu4ErrorCode,
                                UINT4 u4Ieee8021CnGlobalComponentId,
                                INT4 i4TestValFsCnXGlobalTraceLevel)
{
    if (CN_IS_SHUTDOWN ())
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;

        return SNMP_FAILURE;
    }

    if (CN_IS_VALID_COMP_ID (u4Ieee8021CnGlobalComponentId) == OSIX_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CN_CLI_INVALID_COMP_ID);
        return SNMP_FAILURE;
    }

    if (CN_COMP_TBL_ENTRY (u4Ieee8021CnGlobalComponentId) == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CN_CLI_COMP_ENTRY_NOT_FOUND);
        return SNMP_FAILURE;
    }
    if ((i4TestValFsCnXGlobalTraceLevel < CN_ZERO) ||
        (i4TestValFsCnXGlobalTraceLevel > CN_ALL_TRC))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsCnXGlobalClearCounters
 Input       :  The Indices
                Ieee8021CnGlobalComponentId

                The Object 
                testValFsCnXGlobalClearCounters
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsCnXGlobalClearCounters (UINT4 *pu4ErrorCode,
                                   UINT4 u4Ieee8021CnGlobalComponentId,
                                   INT4 i4TestValFsCnXGlobalClearCounters)
{
    if (CN_IS_SHUTDOWN ())
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (CN_IS_VALID_COMP_ID (u4Ieee8021CnGlobalComponentId) == OSIX_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CN_CLI_INVALID_COMP_ID);
        return SNMP_FAILURE;
    }
    if (CN_COMP_TBL_ENTRY (u4Ieee8021CnGlobalComponentId) == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CN_CLI_COMP_ENTRY_NOT_FOUND);
        return SNMP_FAILURE;
    }
    if ((i4TestValFsCnXGlobalClearCounters != CN_SET_CLEAR_COUNTER) &&
        (i4TestValFsCnXGlobalClearCounters != CN_RESET_CLEAR_COUNTER))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;

    }
    return SNMP_SUCCESS;

}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsCnXGlobalTable
 Input       :  The Indices
                Ieee8021CnGlobalComponentId
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsCnXGlobalTable (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                          tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsCnXPortPriTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsCnXPortPriTable
 Input       :  The Indices
                Ieee8021CnPortPriComponentId
                Ieee8021CnPortPriority
                Ieee8021CnPortPriIfIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsCnXPortPriTable (UINT4 u4Ieee8021CnPortPriComponentId,
                                           UINT4 u4Ieee8021CnPortPriority,
                                           INT4 i4Ieee8021CnPortPriIfIndex)
{
    UINT4               u4PortId = i4Ieee8021CnPortPriIfIndex;
    tCnPortPriTblInfo  *pCnPortPriEntry = NULL;
    tCnCompTblInfo     *pCnCompTblEntry = NULL;
    tCnPortTblInfo     *pCnPortTblEntry = NULL;

    if (CN_IS_SHUTDOWN ())
    {
        return SNMP_FAILURE;
    }

    if (CN_IS_VALID_COMP_ID (u4Ieee8021CnPortPriComponentId) == OSIX_FALSE)
    {
        return SNMP_FAILURE;
    }

    if (u4Ieee8021CnPortPriority >= CN_MAX_VLAN_PRIORITY)
    {
        return SNMP_FAILURE;
    }

    if ((i4Ieee8021CnPortPriIfIndex < CN_MIN_PORTS) ||
        (i4Ieee8021CnPortPriIfIndex > CN_MAX_PORTS))
    {
        return SNMP_FAILURE;
    }

    pCnCompTblEntry = CN_COMP_TBL_ENTRY (u4Ieee8021CnPortPriComponentId);
    if (pCnCompTblEntry == NULL)
    {
        CLI_SET_ERR (CN_CLI_COMP_ENTRY_NOT_FOUND);
        CN_TRC (u4Ieee8021CnPortPriComponentId,
                (CN_MGMT_TRC | CN_ALL_FAILURE_TRC),
                "nmhValidateIndexInstanceFsCnXPortPriTable: FAILED\r\n"
                "Context is not present in CN !!!\r\n");
        return SNMP_FAILURE;
    }

    CN_PORT_TBL_ENTRY (u4Ieee8021CnPortPriComponentId, u4PortId,
                       pCnPortTblEntry);
    if (pCnPortTblEntry == NULL)
    {
        CLI_SET_ERR (CN_CLI_PORT_ENTRY_NOT_FOUND);
        CN_TRC_ARG1 (u4Ieee8021CnPortPriComponentId,
                     (CN_MGMT_TRC | CN_ALL_FAILURE_TRC),
                     "nmhValidateIndexInstanceFsCnXPortPriTable: "
                     "Port Entry does not exists for port %d !!!\r\n",
                     u4PortId);
        return SNMP_FAILURE;
    }

    CN_PORT_PRI_ENTRY (u4Ieee8021CnPortPriComponentId, u4PortId,
                       u4Ieee8021CnPortPriority, pCnPortPriEntry);
    if (pCnPortPriEntry == NULL)
    {
        CLI_SET_ERR (CN_CLI_PORT_PRI_ENTRY_NOT_FOUND);
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsCnXPortPriTable
 Input       :  The Indices
                Ieee8021CnPortPriComponentId
                Ieee8021CnPortPriority
                Ieee8021CnPortPriIfIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsCnXPortPriTable (UINT4 *pu4Ieee8021CnPortPriComponentId,
                                   UINT4 *pu4Ieee8021CnPortPriority,
                                   INT4 *pi4Ieee8021CnPortPriIfIndex)
{
    tCnPortPriTblInfo  *pCnPortPriEntry = NULL;
    UINT4               u4CompId = CN_MIN_CONTEXT;
    UINT4               u4Priority = CN_ZERO;
    UINT4               u4PortId = CN_MIN_PORTS;

    CN_PORT_PRI_ENTRY (u4CompId, u4PortId, u4Priority, pCnPortPriEntry);
    if (pCnPortPriEntry != NULL)
    {
        *pu4Ieee8021CnPortPriComponentId = u4CompId;
        *pi4Ieee8021CnPortPriIfIndex = (INT4) u4PortId;
        *pu4Ieee8021CnPortPriority = u4Priority;
        return SNMP_SUCCESS;
    }
    return (nmhGetNextIndexIeee8021CnPortPriTable
            (u4CompId, pu4Ieee8021CnPortPriComponentId,
             u4Priority, pu4Ieee8021CnPortPriority, u4PortId,
             pi4Ieee8021CnPortPriIfIndex));

}

/****************************************************************************
 Function    :  nmhGetNextIndexFsCnXPortPriTable
 Input       :  The Indices
                Ieee8021CnPortPriComponentId
                nextIeee8021CnPortPriComponentId
                Ieee8021CnPortPriority
                nextIeee8021CnPortPriority
                Ieee8021CnPortPriIfIndex
                nextIeee8021CnPortPriIfIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsCnXPortPriTable (UINT4 u4Ieee8021CnPortPriComponentId,
                                  UINT4 *pu4NextIeee8021CnPortPriComponentId,
                                  UINT4 u4Ieee8021CnPortPriority,
                                  UINT4 *pu4NextIeee8021CnPortPriority,
                                  INT4 i4Ieee8021CnPortPriIfIndex,
                                  INT4 *pi4NextIeee8021CnPortPriIfIndex)
{
    tCnCompTblInfo     *pCnCompTblEntry = NULL;
    tCnPortTblInfo     *pCnPortTblEntry = NULL;
    tCnPortTblInfo     *pTempPortTblEntry = NULL;
    tCnPortTblInfo      TempPortTblEntry;
    tCnPortPriTblInfo  *pCnPortPriEntry = NULL;
    UINT4               u4PortId = CN_ZERO;
    UINT4               u4Priority = CN_ZERO;
    UINT4               u4CompId = CN_MIN_CONTEXT;

    MEMSET (&TempPortTblEntry, CN_ZERO, sizeof (tCnPortTblInfo));

    for (u4CompId = u4Ieee8021CnPortPriComponentId; u4CompId < CN_MAX_CONTEXTS;
         u4CompId++)
    {
        pCnCompTblEntry = CN_COMP_TBL_ENTRY (u4CompId);
        if (pCnCompTblEntry != NULL)
        {
            if (u4CompId == u4Ieee8021CnPortPriComponentId)
            {
                u4Priority = u4Ieee8021CnPortPriority;
            }
            else
            {
                u4Priority = CN_ZERO;
            }
            for (; u4Priority < CN_MAX_VLAN_PRIORITY; u4Priority++)
            {
                if (pCnCompTblEntry->pCompPriTbl[u4Priority] != NULL)
                {
                    if ((u4CompId == u4Ieee8021CnPortPriComponentId) &&
                        (u4Priority == u4Ieee8021CnPortPriority))
                    {
                        TempPortTblEntry.u4PortId =
                            (UINT4) i4Ieee8021CnPortPriIfIndex;
                        pTempPortTblEntry = &TempPortTblEntry;
                        pCnPortTblEntry =
                            (tCnPortTblInfo *) RBTreeGetNext (pCnCompTblEntry->
                                                              PortTbl,
                                                              pTempPortTblEntry,
                                                              NULL);
                    }
                    else
                    {
                        pCnPortTblEntry = (tCnPortTblInfo *) RBTreeGetFirst
                            (pCnCompTblEntry->PortTbl);
                    }
                    if (pCnPortTblEntry != NULL)
                    {
                        u4PortId = pCnPortTblEntry->u4PortId;
                        CN_PORT_PRI_ENTRY (u4CompId, u4PortId,
                                           u4Priority, pCnPortPriEntry);
                        if (pCnPortPriEntry != NULL)
                        {
                            *pu4NextIeee8021CnPortPriComponentId = u4CompId;
                            *pi4NextIeee8021CnPortPriIfIndex = u4PortId;
                            *pu4NextIeee8021CnPortPriority = u4Priority;
                            return SNMP_SUCCESS;
                        }
                    }

                }
            }
        }
    }
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsCnXPortPriClearCpCounters
 Input       :  The Indices
                Ieee8021CnPortPriComponentId
                Ieee8021CnPortPriority
                Ieee8021CnPortPriIfIndex

                The Object 
                retValFsCnXPortPriClearCpCounters
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsCnXPortPriClearCpCounters (UINT4 u4Ieee8021CnPortPriComponentId,
                                   UINT4 u4Ieee8021CnPortPriority,
                                   INT4 i4Ieee8021CnPortPriIfIndex,
                                   INT4 *pi4RetValFsCnXPortPriClearCpCounters)
{
    *pi4RetValFsCnXPortPriClearCpCounters = CN_RESET_CLEAR_COUNTER;
    UNUSED_PARAM (u4Ieee8021CnPortPriComponentId);
    UNUSED_PARAM (u4Ieee8021CnPortPriority);
    UNUSED_PARAM (i4Ieee8021CnPortPriIfIndex);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsCnXPortPriErrorEntry
 Input       :  The Indices
                Ieee8021CnPortPriComponentId
                Ieee8021CnPortPriority
                Ieee8021CnPortPriIfIndex

                The Object 
                retValFsCnXPortPriErrorEntry
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsCnXPortPriErrorEntry (UINT4 u4Ieee8021CnPortPriComponentId,
                              UINT4 u4Ieee8021CnPortPriority,
                              INT4 i4Ieee8021CnPortPriIfIndex,
                              INT4 *pi4RetValFsCnXPortPriErrorEntry)
{
    tCnPortPriTblInfo  *pCnPortPriEntry = NULL;
    UINT4               u4PortId = i4Ieee8021CnPortPriIfIndex;

    CN_PORT_PRI_ENTRY (u4Ieee8021CnPortPriComponentId, u4PortId,
                       u4Ieee8021CnPortPriority, pCnPortPriEntry);
    if (pCnPortPriEntry != NULL)
    {
        *pi4RetValFsCnXPortPriErrorEntry = pCnPortPriEntry->bIsErrorPortEntry;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 * Function    :  nmhGetFsCnXPortPriOperDefMode
 * Input       :  The Indices
 *                Ieee8021CnPortPriComponentId
 *                Ieee8021CnPortPriority
 *                Ieee8021CnPortPriIfIndex
 *                The Object
 *                retValFsCnXPortPriOperDefMode
 * Output      :  The Get Low Lev Routine Take the Indices &
 *                store the Value requested in the Return val.
 * Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsCnXPortPriOperDefMode (UINT4 u4Ieee8021CnPortPriComponentId,
                               UINT4 u4Ieee8021CnPortPriority,
                               INT4 i4Ieee8021CnPortPriIfIndex,
                               INT4 *pi4RetValFsCnXPortPriOperDefMode)
{
    tCnPortPriTblInfo  *pCnPortPriEntry = NULL;
    UINT4               u4PortId = i4Ieee8021CnPortPriIfIndex;

    CN_PORT_PRI_ENTRY (u4Ieee8021CnPortPriComponentId, u4PortId,
                       u4Ieee8021CnPortPriority, pCnPortPriEntry);
    if (pCnPortPriEntry != NULL)
    {
        *pi4RetValFsCnXPortPriOperDefMode =
            pCnPortPriEntry->u1PortPriOperDefMode;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/**************************************************************************
 *  Function    :  nmhGetFsCnXPortPriOperAltPri
 *  Input       :  The Indices
 *                 Ieee8021CnPortPriComponentId
 *                 Ieee8021CnPortPriority
 *                 Ieee8021CnPortPriIfIndex
 *                 The Object
 *                 retValFsCnXPortPriOperAltPri
 *  Output      :  The Get Low Lev Routine Take the Indices &
 *                 store the Value requested in the Return val.
 *  Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 *************************************************************************/
INT1
nmhGetFsCnXPortPriOperAltPri (UINT4 u4Ieee8021CnPortPriComponentId,
                              UINT4 u4Ieee8021CnPortPriority,
                              INT4 i4Ieee8021CnPortPriIfIndex,
                              UINT4 *pu4RetValFsCnXPortPriOperAltPri)
{
    tCnPortPriTblInfo  *pCnPortPriEntry = NULL;
    UINT4               u4PortId = i4Ieee8021CnPortPriIfIndex;

    CN_PORT_PRI_ENTRY (u4Ieee8021CnPortPriComponentId, u4PortId,
                       u4Ieee8021CnPortPriority, pCnPortPriEntry);
    if (pCnPortPriEntry != NULL)
    {
        *pu4RetValFsCnXPortPriOperAltPri = pCnPortPriEntry->u1PortPriOperAltPri;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetFsCnXPortPriLastRcvdEvent
 Input       :  The Indices
                Ieee8021CnPortPriComponentId
                Ieee8021CnPortPriority
                Ieee8021CnPortPriIfIndex

                The Object 
                retValFsCnXPortPriLastRcvdEvent
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsCnXPortPriLastRcvdEvent (UINT4 u4Ieee8021CnPortPriComponentId,
                                 UINT4 u4Ieee8021CnPortPriority,
                                 INT4 i4Ieee8021CnPortPriIfIndex,
                                 tSNMP_OCTET_STRING_TYPE
                                 * pRetValFsCnXPortPriLastRcvdEvent)
{
    tCnPortPriTblInfo  *pCnPortPriEntry = NULL;
    UINT4               u4PortId = i4Ieee8021CnPortPriIfIndex;

    CN_PORT_PRI_ENTRY (u4Ieee8021CnPortPriComponentId, u4PortId,
                       u4Ieee8021CnPortPriority, pCnPortPriEntry);
    if (pCnPortPriEntry != NULL)
    {
        MEMCPY (pRetValFsCnXPortPriLastRcvdEvent->pu1_OctetList,
                RCVD_EVENT (pCnPortPriEntry->u1LastRcvdEvent),
                sizeof (RCVD_EVENT (pCnPortPriEntry->u1LastRcvdEvent)));
        pRetValFsCnXPortPriLastRcvdEvent->i4_Length =
            STRLEN (RCVD_EVENT (pCnPortPriEntry->u1LastRcvdEvent));
        return SNMP_SUCCESS;

    }
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetFsCnXPortPriLastRcvdEventTime
 Input       :  The Indices
                Ieee8021CnPortPriComponentId
                Ieee8021CnPortPriority
                Ieee8021CnPortPriIfIndex

                The Object 
                retValFsCnXPortPriLastRcvdEventTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsCnXPortPriLastRcvdEventTime (UINT4 u4Ieee8021CnPortPriComponentId,
                                     UINT4 u4Ieee8021CnPortPriority,
                                     INT4 i4Ieee8021CnPortPriIfIndex,
                                     UINT4
                                     *pu4RetValFsCnXPortPriLastRcvdEventTime)
{
    tCnPortPriTblInfo  *pCnPortPriEntry = NULL;
    UINT4               u4PortId = i4Ieee8021CnPortPriIfIndex;

    CN_PORT_PRI_ENTRY (u4Ieee8021CnPortPriComponentId, u4PortId,
                       u4Ieee8021CnPortPriority, pCnPortPriEntry);
    if (pCnPortPriEntry != NULL)
    {
        *pu4RetValFsCnXPortPriLastRcvdEventTime =
            pCnPortPriEntry->u4LastEvtRcvdTime;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsCnXPortPriLastSentEvent
 Input       :  The Indices
                Ieee8021CnPortPriComponentId
                Ieee8021CnPortPriority
                Ieee8021CnPortPriIfIndex

                The Object 
                retValFsCnXPortPriLastSentEvent
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsCnXPortPriLastSentEvent (UINT4 u4Ieee8021CnPortPriComponentId,
                                 UINT4 u4Ieee8021CnPortPriority,
                                 INT4 i4Ieee8021CnPortPriIfIndex,
                                 tSNMP_OCTET_STRING_TYPE
                                 * pRetValFsCnXPortPriLastSentEvent)
{
    tCnPortPriTblInfo  *pCnPortPriEntry = NULL;
    UINT4               u4PortId = i4Ieee8021CnPortPriIfIndex;

    CN_PORT_PRI_ENTRY (u4Ieee8021CnPortPriComponentId, u4PortId,
                       u4Ieee8021CnPortPriority, pCnPortPriEntry);
    if (pCnPortPriEntry != NULL)
    {
        MEMCPY (pRetValFsCnXPortPriLastSentEvent->pu1_OctetList,
                SENT_EVENT (pCnPortPriEntry->u1LastSentEvent),
                STRLEN (SENT_EVENT (pCnPortPriEntry->u1LastSentEvent)));
        pRetValFsCnXPortPriLastSentEvent->i4_Length =
            STRLEN (SENT_EVENT (pCnPortPriEntry->u1LastSentEvent));
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsCnXPortPriLastSentEventTime
 Input       :  The Indices
                Ieee8021CnPortPriComponentId
                Ieee8021CnPortPriority
                Ieee8021CnPortPriIfIndex

                The Object 
                retValFsCnXPortPriLastSentEventTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsCnXPortPriLastSentEventTime (UINT4 u4Ieee8021CnPortPriComponentId,
                                     UINT4 u4Ieee8021CnPortPriority,
                                     INT4 i4Ieee8021CnPortPriIfIndex,
                                     UINT4
                                     *pu4RetValFsCnXPortPriLastSentEventTime)
{
    tCnPortPriTblInfo  *pCnPortPriEntry = NULL;
    UINT4               u4PortId = i4Ieee8021CnPortPriIfIndex;

    CN_PORT_PRI_ENTRY (u4Ieee8021CnPortPriComponentId, u4PortId,
                       u4Ieee8021CnPortPriority, pCnPortPriEntry);
    if (pCnPortPriEntry != NULL)
    {
        *pu4RetValFsCnXPortPriLastSentEventTime =
            pCnPortPriEntry->u4LastEvtSentTime;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsCnXPortPriClearCpCounters
 Input       :  The Indices
                Ieee8021CnPortPriComponentId
                Ieee8021CnPortPriority
                Ieee8021CnPortPriIfIndex

                The Object 
                setValFsCnXPortPriClearCpCounters
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsCnXPortPriClearCpCounters (UINT4 u4Ieee8021CnPortPriComponentId,
                                   UINT4 u4Ieee8021CnPortPriority,
                                   INT4 i4Ieee8021CnPortPriIfIndex,
                                   INT4 i4SetValFsCnXPortPriClearCpCounters)
{
    UINT4               u4PortId = i4Ieee8021CnPortPriIfIndex;

    if (i4SetValFsCnXPortPriClearCpCounters == CN_SET_CLEAR_COUNTER)
    {
        if (CnHwReSetCounters (u4Ieee8021CnPortPriComponentId, u4PortId,
                               ((UINT1) u4Ieee8021CnPortPriority)) ==
            OSIX_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsCnXPortPriClearCpCounters
 Input       :  The Indices
                Ieee8021CnPortPriComponentId
                Ieee8021CnPortPriority
                Ieee8021CnPortPriIfIndex

                The Object 
                testValFsCnXPortPriClearCpCounters
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsCnXPortPriClearCpCounters (UINT4 *pu4ErrorCode,
                                      UINT4 u4Ieee8021CnPortPriComponentId,
                                      UINT4 u4Ieee8021CnPortPriority,
                                      INT4 i4Ieee8021CnPortPriIfIndex,
                                      INT4 i4TestValFsCnXPortPriClearCpCounters)
{
    tCnPortPriTblInfo  *pCnPortPriEntry = NULL;
    tCnCompTblInfo     *pCnCompTblEntry = NULL;
    tCnPortTblInfo     *pCnPortTblEntry = NULL;
    UINT4               u4PortId = i4Ieee8021CnPortPriIfIndex;

    if (CN_IS_SHUTDOWN ())
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (CN_IS_VALID_COMP_ID (u4Ieee8021CnPortPriComponentId) == OSIX_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CN_CLI_INVALID_COMP_ID);
        return SNMP_FAILURE;
    }

    if (u4Ieee8021CnPortPriority >= CN_MAX_VLAN_PRIORITY)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if ((i4Ieee8021CnPortPriIfIndex < CN_MIN_PORTS) ||
        (i4Ieee8021CnPortPriIfIndex > CN_MAX_PORTS))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CN_CLI_INVALID_PORT_ID);
        return SNMP_FAILURE;
    }

    if ((i4TestValFsCnXPortPriClearCpCounters != CN_SET_CLEAR_COUNTER) &&
        (i4TestValFsCnXPortPriClearCpCounters != CN_RESET_CLEAR_COUNTER))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    pCnCompTblEntry = CN_COMP_TBL_ENTRY (u4Ieee8021CnPortPriComponentId);
    if (pCnCompTblEntry == NULL)
    {
        CLI_SET_ERR (CN_CLI_COMP_ENTRY_NOT_FOUND);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    u4PortId = (UINT4) i4Ieee8021CnPortPriIfIndex;
    CN_PORT_TBL_ENTRY (u4Ieee8021CnPortPriComponentId, u4PortId,
                       pCnPortTblEntry);
    if (pCnPortTblEntry == NULL)
    {
        CLI_SET_ERR (CN_CLI_PORT_ENTRY_NOT_FOUND);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CN_TRC_ARG1 (u4Ieee8021CnPortPriComponentId,
                     (CN_MGMT_TRC | CN_ALL_FAILURE_TRC),
                     "nmhTestv2FsCnXPortPriClearCpCounters: FAILED\r\n"
                     "Port Entry does not exists for port %d !!!\r\n",
                     u4PortId);
        return SNMP_FAILURE;
    }

    CN_PORT_PRI_ENTRY (u4Ieee8021CnPortPriComponentId, u4PortId,
                       u4Ieee8021CnPortPriority, pCnPortPriEntry);
    if (pCnPortPriEntry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CN_CLI_PORT_PRI_ENTRY_NOT_FOUND);
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsCnXPortPriTable
 Input       :  The Indices
                Ieee8021CnPortPriComponentId
                Ieee8021CnPortPriority
                Ieee8021CnPortPriIfIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsCnXPortPriTable (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                           tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */
