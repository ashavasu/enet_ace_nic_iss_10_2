/* $Id: fscnwr.c,v 1.3 2011/09/24 06:38:24 siva Exp $ */
# include  "lr.h"
# include  "fssnmp.h"
# include  "fscnwr.h"
# include  "fscndb.h"
# include  "cninc.h"
VOID
RegisterFSCN ()
{
    SNMPRegisterMibWithLock (&fscnOID, &fscnEntry, CnLock, CnUnLock,
                             SNMP_MSR_TGR_TRUE);
    SNMPAddSysorEntry (&fscnOID, (const UINT1 *) "fscn");
}

VOID
UnRegisterFSCN ()
{
    SNMPUnRegisterMib (&fscnOID, &fscnEntry);
    SNMPDelSysorEntry (&fscnOID, (const UINT1 *) "fscn");
}

INT4
FsCnSystemControlGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsCnSystemControl (&(pMultiData->i4_SLongValue)));
}

INT4
FsCnGlobalEnableTrapGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsCnGlobalEnableTrap (&(pMultiData->i4_SLongValue)));
}

INT4
FsCnSystemControlSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsCnSystemControl (pMultiData->i4_SLongValue));
}

INT4
FsCnGlobalEnableTrapSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsCnGlobalEnableTrap (pMultiData->i4_SLongValue));
}

INT4
FsCnSystemControlTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                       tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsCnSystemControl (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FsCnGlobalEnableTrapTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                          tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsCnGlobalEnableTrap
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FsCnSystemControlDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                      tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsCnSystemControl
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsCnGlobalEnableTrapDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                         tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsCnGlobalEnableTrap
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFsCnXGlobalTable (tSnmpIndex * pFirstMultiIndex,
                              tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsCnXGlobalTable
            (&(pNextMultiIndex->pIndex[0].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsCnXGlobalTable
            (pFirstMultiIndex->pIndex[0].u4_ULongValue,
             &(pNextMultiIndex->pIndex[0].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
FsCnXGlobalTraceLevelGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsCnXGlobalTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsCnXGlobalTraceLevel (pMultiIndex->pIndex[0].u4_ULongValue,
                                         &(pMultiData->i4_SLongValue)));

}

INT4
FsCnXGlobalClearCountersGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsCnXGlobalTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsCnXGlobalClearCounters
            (pMultiIndex->pIndex[0].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsCnXGlobalTLVErrorsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsCnXGlobalTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsCnXGlobalTLVErrors (pMultiIndex->pIndex[0].u4_ULongValue,
                                        &(pMultiData->u4_ULongValue)));

}

INT4
FsCnXGlobalTraceLevelSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsCnXGlobalTraceLevel (pMultiIndex->pIndex[0].u4_ULongValue,
                                         pMultiData->i4_SLongValue));

}

INT4
FsCnXGlobalClearCountersSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsCnXGlobalClearCounters
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->i4_SLongValue));

}

INT4
FsCnXGlobalTraceLevelTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                           tRetVal * pMultiData)
{
    return (nmhTestv2FsCnXGlobalTraceLevel (pu4Error,
                                            pMultiIndex->pIndex[0].
                                            u4_ULongValue,
                                            pMultiData->i4_SLongValue));

}

INT4
FsCnXGlobalClearCountersTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                              tRetVal * pMultiData)
{
    return (nmhTestv2FsCnXGlobalClearCounters (pu4Error,
                                               pMultiIndex->pIndex[0].
                                               u4_ULongValue,
                                               pMultiData->i4_SLongValue));

}

INT4
FsCnXGlobalTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                     tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsCnXGlobalTable (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFsCnXPortPriTable (tSnmpIndex * pFirstMultiIndex,
                               tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsCnXPortPriTable
            (&(pNextMultiIndex->pIndex[0].u4_ULongValue),
             &(pNextMultiIndex->pIndex[1].u4_ULongValue),
             &(pNextMultiIndex->pIndex[2].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsCnXPortPriTable
            (pFirstMultiIndex->pIndex[0].u4_ULongValue,
             &(pNextMultiIndex->pIndex[0].u4_ULongValue),
             pFirstMultiIndex->pIndex[1].u4_ULongValue,
             &(pNextMultiIndex->pIndex[1].u4_ULongValue),
             pFirstMultiIndex->pIndex[2].i4_SLongValue,
             &(pNextMultiIndex->pIndex[2].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
FsCnXPortPriClearCpCountersGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsCnXPortPriTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsCnXPortPriClearCpCounters
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             pMultiIndex->pIndex[2].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsCnXPortPriErrorEntryGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsCnXPortPriTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsCnXPortPriErrorEntry (pMultiIndex->pIndex[0].u4_ULongValue,
                                          pMultiIndex->pIndex[1].u4_ULongValue,
                                          pMultiIndex->pIndex[2].i4_SLongValue,
                                          &(pMultiData->i4_SLongValue)));

}

INT4
FsCnXPortPriOperDefModeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsCnXPortPriTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsCnXPortPriOperDefMode (pMultiIndex->pIndex[0].u4_ULongValue,
                                           pMultiIndex->pIndex[1].u4_ULongValue,
                                           pMultiIndex->pIndex[2].i4_SLongValue,
                                           &(pMultiData->i4_SLongValue)));

}

INT4
FsCnXPortPriOperAltPriGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsCnXPortPriTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsCnXPortPriOperAltPri (pMultiIndex->pIndex[0].u4_ULongValue,
                                          pMultiIndex->pIndex[1].u4_ULongValue,
                                          pMultiIndex->pIndex[2].i4_SLongValue,
                                          &(pMultiData->u4_ULongValue)));

}

INT4
FsCnXPortPriLastRcvdEventGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsCnXPortPriTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsCnXPortPriLastRcvdEvent
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             pMultiIndex->pIndex[2].i4_SLongValue, pMultiData->pOctetStrValue));

}

INT4
FsCnXPortPriLastRcvdEventTimeGet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsCnXPortPriTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsCnXPortPriLastRcvdEventTime
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             pMultiIndex->pIndex[2].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsCnXPortPriLastSentEventGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsCnXPortPriTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsCnXPortPriLastSentEvent
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             pMultiIndex->pIndex[2].i4_SLongValue, pMultiData->pOctetStrValue));

}

INT4
FsCnXPortPriLastSentEventTimeGet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsCnXPortPriTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsCnXPortPriLastSentEventTime
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             pMultiIndex->pIndex[2].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsCnXPortPriClearCpCountersSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsCnXPortPriClearCpCounters
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             pMultiIndex->pIndex[2].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
FsCnXPortPriClearCpCountersTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                 tRetVal * pMultiData)
{
    return (nmhTestv2FsCnXPortPriClearCpCounters (pu4Error,
                                                  pMultiIndex->pIndex[0].
                                                  u4_ULongValue,
                                                  pMultiIndex->pIndex[1].
                                                  u4_ULongValue,
                                                  pMultiIndex->pIndex[2].
                                                  i4_SLongValue,
                                                  pMultiData->i4_SLongValue));

}

INT4
FsCnXPortPriTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                      tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsCnXPortPriTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}
