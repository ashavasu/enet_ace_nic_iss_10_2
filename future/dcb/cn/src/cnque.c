/********************************************************************
 * Copyright (C) 2010 Aricent Inc . All Rights Reserved
 *
 * $Id: cnque.c,v 1.4 2016/08/04 08:14:00 siva Exp $
 *
 * Description: This file contains procedures related to
 *              - Processing QMsgs
 *              - Enqing QMsg from external modules to CN task
 *********************************************************************/
#include "cninc.h"

/* Proto types of the functions private to this file only */

PRIVATE INT4 CnQueHandleCnTlv PROTO ((UINT4 u4PortId, UINT1 *pu1CnTLV));
PRIVATE INT4 CnQueHandleAgedOut PROTO ((UINT4 u4PortId));
PRIVATE VOID CnQueHandleReReg PROTO ((VOID));
PRIVATE VOID CnQueHandleCnmGeneration PROTO ((INT4 i4CnmQOffset,
                                              INT4 i4CnmQDelta,
                                              UINT1 *pu1CpIdentifier));
/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : CnQueMsgHandler  
 *                                                                          
 *    DESCRIPTION      : This function process the queue messages received 
 *                       by CN task. 
 *
 *    INPUT            : None 
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : None 
 *                                                                          
 ****************************************************************************/
PUBLIC VOID
CnQueMsgHandler (VOID)
{
    tCnQMsg            *pQMsg = NULL;

    /* Event received, dequeue messages for processing */
    while (OsixQueRecv (gCnMasterTblInfo.cnTaskQId, (UINT1 *) &pQMsg,
                        OSIX_DEF_MSG_LEN, OSIX_NO_WAIT) == OSIX_SUCCESS)
    {
        switch (pQMsg->u4MsgType)
        {
            case CN_CREATE_CONTEXT_MSG:

                CnCxtCreateContext (pQMsg->u4ContextId);
                L2MI_SYNC_GIVE_SEM ();
                break;
            case CN_DELETE_CONTEXT_MSG:

                CnCxtDeleteContext (pQMsg->u4ContextId);
                L2MI_SYNC_GIVE_SEM ();
                break;
            case CN_CREATE_PORT_MSG:    /*this case hits when SI mode */

                CnIfCreate (pQMsg->u4ContextId, pQMsg->u4PortId);
                L2_SYNC_GIVE_SEM ();
                break;
            case CN_DELETE_PORT_MSG:    /*this case hits when SI mode */

                CnIfDelete (pQMsg->u4PortId);
                break;

            case CN_MAP_PORT_MSG:    /*this case hits when MI mode */

                CnIfCreate (pQMsg->u4ContextId, pQMsg->u4PortId);
                L2MI_SYNC_GIVE_SEM ();
                break;
            case CN_UNMAP_PORT_MSG:    /*this case hits when MI mode */

                CnIfDelete (pQMsg->u4PortId);
                L2MI_SYNC_GIVE_SEM ();
                break;

            case CN_ADD_LAG_MEMBER_MSG:

                CnIfAddPortToLAG (pQMsg->u4ContextId,
                                  pQMsg->unMsgParam.u4LagMemberPortId,
                                  pQMsg->u4PortId);
                break;

            case CN_REM_LAG_MEMBER_MSG:

                CnIfDelPortFromLAG (pQMsg->unMsgParam.u4LagMemberPortId,
                                    pQMsg->u4PortId);
                break;

            case CN_PO_READY_IN_HW_MSG:
                if ( gCnMasterTblInfo.ppCompTblInfo[pQMsg->u4ContextId] != NULL)
                {
                CnIfPortChannelReadyinHw (pQMsg->u4ContextId, pQMsg->u4PortId);
                }
                break;
            case L2IWF_LLDP_APPL_TLV_RECV:

                CnQueHandleCnTlv (pQMsg->u4PortId, pQMsg->unMsgParam.au1CNTlv);
                break;
            case L2IWF_LLDP_APPL_TLV_AGED:

                CnQueHandleAgedOut (pQMsg->u4PortId);
                break;

            case L2IWF_LLDP_APPL_RE_REG:

                CnQueHandleReReg ();
                break;
            case CN_CNM_GENERATE_MSG:

                CnQueHandleCnmGeneration (pQMsg->unMsgParam.CnCnmGen.
                                          i4CnmQOffset,
                                          pQMsg->unMsgParam.CnCnmGen.
                                          i4CnmQDelta,
                                          pQMsg->unMsgParam.CnCnmGen.
                                          au1CpIdentifier);
                break;
            default:
                CN_GBL_TRC_ARG1 (CN_INVALID_CONTEXT,
                                 (CN_CRITICAL_TRC | CN_ALL_FAILURE_TRC),
                                 "CnQueMsgHandler: Unknown message type %d "
                                 "received\r\n", pQMsg->u4MsgType);
                break;
        }
        /* Release the buffer to pool */
        MemReleaseMemBlock (gCnMasterTblInfo.QMsgPoolId, (UINT1 *) pQMsg);
    }
    return;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : CnQueEnqMsg
 *                                                                          
 *    DESCRIPTION      : Function to post  message to CN task. 
 *
 *    INPUT            : pMsg - Pointer to msg to be posted 
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE 
 *                                                                          
 ****************************************************************************/
PUBLIC INT4
CnQueEnqMsg (tCnQMsg * pMsg)
{
    UINT4               u4Event = pMsg->u4MsgType;

    if (OsixQueSend (gCnMasterTblInfo.cnTaskQId, (UINT1 *) &pMsg,
                     OSIX_DEF_MSG_LEN) == OSIX_FAILURE)
    {
        MemReleaseMemBlock (gCnMasterTblInfo.QMsgPoolId, (UINT1 *) pMsg);

        CN_GBL_TRC_ARG1 (CN_INVALID_CONTEXT,
                         (CN_RESOURCE_TRC | CN_ALL_FAILURE_TRC),
                         "CnQueEnqMsg: Osix Queue send FAILED for Msg type %d \r\n",
                         u4Event);
        return OSIX_FAILURE;
    }

    OsixEvtSend (gCnMasterTblInfo.CnTaskId, CN_QMSG_EVENT);

    if (u4Event == CN_CREATE_PORT_MSG)
    {
        L2_SYNC_TAKE_SEM ();
    }
    else if ((u4Event == CN_CREATE_CONTEXT_MSG) ||
             (u4Event == CN_MAP_PORT_MSG) ||
             (u4Event == CN_UNMAP_PORT_MSG) ||
             (u4Event == CN_DELETE_CONTEXT_MSG))
    {
        L2MI_SYNC_TAKE_SEM ();
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : CnQueHandleCnmGeneration
 *
 *    DESCRIPTION      : Function to send trapfor generated CNM in hardware.
 *
 *    INPUT            : i4CnmQOffset - CNM offset
 *                       i4CnmQDelta - CNM delta
 *                       pu1CpIdentifier - pointer to CPID
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : NONE
 *
 ****************************************************************************/
PRIVATE VOID
CnQueHandleCnmGeneration (INT4 i4CnmQOffset, INT4 i4CnmQDelta,
                          UINT1 *pu1CpIdentifier)
{

    tCnTrapMsg          CnTrapMsg;
    tCnPortPriTblInfo  *pCnPortPriEntry = NULL;
    tCnPortPriTblInfo   TempPortPriEntry;

    MEMSET (&CnTrapMsg, CN_ZERO, sizeof (tCnTrapMsg));
    MEMSET (&TempPortPriEntry, CN_ZERO, sizeof (tCnPortPriTblInfo));

    MEMCPY (TempPortPriEntry.au1CnCpIdentifier, pu1CpIdentifier, CN_CPID_LEN);
    pCnPortPriEntry = (tCnPortPriTblInfo *) RBTreeGet
        (gCnMasterTblInfo.CpIdTbl, &TempPortPriEntry);

    if (pCnPortPriEntry == NULL)
    {
        /*Entry does not exist */
        CN_GBL_TRC (CN_INVALID_CONTEXT, CN_ALL_FAILURE_TRC,
                    "CnQueHandleCnmGeneration: "
                    "Port Priority Table Entry does not exist \r\n");
        return;
    }

    CnTrapMsg.u4ContextId = pCnPortPriEntry->u4CnComPriComponentId;
    CnTrapMsg.u4PortId = pCnPortPriEntry->u4PortId;
    CnTrapMsg.u1CNPV = pCnPortPriEntry->u1CNPV;
    CnTrapMsg.unTrapMsg.CnmParams.i4CnmQOffset = i4CnmQOffset;
    CnTrapMsg.unTrapMsg.CnmParams.i4CnmQDelta = i4CnmQDelta;
    MEMCPY (CnTrapMsg.unTrapMsg.CnmParams.au1CpIdentifier,
            pu1CpIdentifier, CN_CPID_LEN);

#ifndef FM_WANTED
    SYS_LOG_MSG ((SYSLOG_CRITICAL_LEVEL, gCnMasterTblInfo.u4SysLogId,
                  "CNM Message sent for port %d CNPV %d "
                  pPortPriTable->u4PortId, pPortPriTable->u1CNPV));
#endif
    CnSendNotification (&CnTrapMsg, CN_SEND_CN_MESSAGE);

    return;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : CnQueHandleCnTlv
 *
 *    DESCRIPTION      : Function to handle the tlv received from queue.
 *
 *    INPUT            : pu1CnTLV - Pointer to tlv array
 *                       U4PortId - Port ID
 *
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *
 ****************************************************************************/
PRIVATE INT4
CnQueHandleCnTlv (UINT4 u4PortId, UINT1 *pu1CnTLV)
{
    UINT4               u4CompId = CN_ZERO;
    UINT4               u4Priority = CN_ZERO;
    UINT4               u4LldpInsCh = CN_ZERO;
    INT4                i4PortDuplexity = ISS_HALFDUP;
    UINT2               u2LocalPort = CN_ZERO;
    UINT1               u1CnpvIndicator = CN_ZERO;
    UINT1               u1ReadyIndicator = CN_ZERO;
    UINT1              *pu1TempTlv = NULL;
    tCnPortPriTblInfo  *pCnPortPriTblInfo = NULL;
    tCnCompTblInfo     *pCnCompTblEntry = NULL;
    tCnPortTblInfo     *pCnPortTblEntry = NULL;
    tCnPortTblInfo      TempPortTbl;

    MEMSET (&TempPortTbl, CN_ZERO, sizeof (tCnPortTblInfo));
    TempPortTbl.u4PortId = u4PortId;

    CnPortVcmGetCxtInfoFromIfIndex (u4PortId, &u4CompId, &u2LocalPort);

    pCnCompTblEntry = CN_COMP_TBL_ENTRY (u4CompId);
    if (pCnCompTblEntry == NULL)
    {
        CN_TRC_ARG1 (u4CompId, (CN_ALL_FAILURE_TRC | CN_TLV_TRC),
                     "CnQueHandleCnTlv  : Context %u "
                     "is not present in CN database \r\n", u4CompId);

        return (OSIX_FAILURE);
    }

    /* Process TLV only if the CN is enabled in the context */
    if (pCnCompTblEntry->bCnCompMasterEnable != OSIX_TRUE)
    {
        CN_TRC_ARG1 (u4CompId, (CN_ALL_FAILURE_TRC | CN_TLV_TRC),
                     "CnQueHandleCnTlv: CN disabled in the context %u, "
                     "so not processing the received TLV\r\n", u4CompId);
        return OSIX_FAILURE;
    }

    /*checking for tlv type and subtype */
    if (MEMCMP (pu1CnTLV, gau1CnPreFormedTlv, CN_PREFORMED_TLV_LEN) != CN_ZERO)
    {
        CN_TRC_ARG1 (u4CompId, (CN_ALL_FAILURE_TRC | CN_TLV_TRC),
                     "CnQueHandleCnTlv :Received Invalid TLV on "
                     " port: %d !!!\r\n" "DUMPING RECEIVED CN TLV\n", u4PortId);
        CN_TLV_DUMP (u4CompId, (CN_TLV_TRC | CN_ALL_FAILURE_TRC),
                     pu1CnTLV, CN_TLV_LEN);
        /*Increment the value of the TLV error variable */
        pCnCompTblEntry->u4TlvErrors++;
        return OSIX_FAILURE;
    }

    pCnPortTblEntry = (tCnPortTblInfo *) RBTreeGet
        (pCnCompTblEntry->PortTbl, &TempPortTbl);
    if (pCnPortTblEntry == NULL)
    {
        CN_TRC_ARG1 (u4CompId, (CN_ALL_FAILURE_TRC | CN_TLV_TRC),
                     "CnQueHandleCnTlv  : Port %d "
                     "is not present in CN database \r\n", u4PortId);
        return (OSIX_FAILURE);
    }

    /* Need not process the incoming TLV if the received port
     * is not FULL duplex. If the port is not FULL duplex it
     * is possible that more than one peer is present for the 
     * LLDP. So all the SEM should move to EDGE.
     */

    /* For physical ports get the duplexity */
    if (CN_IS_PORT_CHANNEL (u4PortId) != OSIX_TRUE)
    {
        CnPortGetPortDuplexity (u4PortId, &i4PortDuplexity);
        /* SEM changed into EDGE when a port has HALF DUPLEX */
        if (i4PortDuplexity != CN_DUP_FULL)
        {
            CN_TRC_ARG1 (u4CompId, (CN_ALL_FAILURE_TRC | CN_TLV_TRC),
                         "CnQueHandleCnTlv: Port %d is not full duplex, "
                         "so setting the CN SEM states to EDGE\r\n", u4PortId);
            for (u4Priority = CN_ZERO; u4Priority < CN_MAX_VLAN_PRIORITY;
                 u4Priority++)
            {
                pCnPortPriTblInfo = pCnPortTblEntry->paPortPriTbl[u4Priority];
                if ((pCnPortPriTblInfo != NULL) &&
                    (pCnPortPriTblInfo->bIsAdminDefMode == OSIX_FALSE))
                {                /*checking for LLDP instance choice NONE */
                    CnUtilLldpInstanceChoice (pCnPortPriTblInfo, &u4LldpInsCh);
                    if (u4LldpInsCh != CN_LLDP_NONE)
                    {
                        /*calling sem for all the CNPV present in the port */
                        pCnPortPriTblInfo->u1LastRcvdEvent =
                            (UINT1) CN_DBG_TLV_DROP_FOR_HALFDUPLEX;
                        OsixGetSysTime (&
                                        (pCnPortPriTblInfo->u4LastEvtRcvdTime));
                        CnSemRunStateMachine (pCnCompTblEntry, pCnPortTblEntry,
                                              pCnPortPriTblInfo,
                                              (UINT1) CN_NOT_RECEIVE_TLV);
                    }
                }
            }
            return (OSIX_SUCCESS);
        }
    }

    pu1TempTlv = pu1CnTLV;
    pu1CnTLV = pu1CnTLV + CN_PREFORMED_TLV_LEN;
    u1CnpvIndicator = *pu1CnTLV;
    pu1CnTLV++;
    u1ReadyIndicator = *pu1CnTLV;
    /*This loop check the TLV for errors
     * When ready indicator set and CNPV indicator is not set then that
     * TLV is treated as invalid TLV*/
    for (u4Priority = CN_ZERO; u4Priority < CN_MAX_VLAN_PRIORITY; u4Priority++)
    {
        if ((u1ReadyIndicator & (CN_TLV_BIT_ONE << u4Priority)) != CN_ZERO)
        {
            if ((u1CnpvIndicator & (CN_TLV_BIT_ONE << u4Priority)) == CN_ZERO)
            {

                CN_TRC_ARG1 (u4CompId, (CN_ALL_FAILURE_TRC | CN_TLV_TRC),
                             "CnQueHandleCnTlv: Received Invalid TLV on "
                             " port: %d !!!\r\n"
                             "DUMPING RECEIVED CN TLV\n", u4PortId);
                CN_TLV_DUMP (u4CompId, (CN_TLV_TRC | CN_ALL_FAILURE_TRC),
                             pu1TempTlv, CN_TLV_LEN);
                /*Increment the value of the TLV error variable */
                pCnCompTblEntry->u4TlvErrors++;
                return (OSIX_FAILURE);
            }
        }
    }                            /*end of for loop */

    CN_TRC_ARG1 (u4CompId, CN_TLV_TRC,
                 "CnQueHandleCnTlv: Received Valid TLV on "
                 " port: %d !!!\r\n" "DUMPING RECEIVED CN TLV\n", u4PortId);

    CN_TLV_DUMP (u4CompId, CN_TLV_TRC, pu1TempTlv, CN_TLV_LEN);

    /*This will parse and set the SEM according to inputs */
    for (u4Priority = CN_ZERO; u4Priority < CN_MAX_VLAN_PRIORITY; u4Priority++)
    {
        pCnPortPriTblInfo = pCnPortTblEntry->paPortPriTbl[u4Priority];
        if ((pCnPortPriTblInfo != NULL) &&
            (pCnPortPriTblInfo->bIsAdminDefMode == OSIX_FALSE))
        {                        /*checking for LLDP instance choice NONE */
            CnUtilLldpInstanceChoice (pCnPortPriTblInfo, &u4LldpInsCh);
            if (u4LldpInsCh != CN_LLDP_NONE)
            {

                if (((u1ReadyIndicator & (CN_TLV_BIT_ONE << u4Priority))
                     != CN_ZERO) &&
                    ((u1CnpvIndicator & (CN_TLV_BIT_ONE << u4Priority))
                     != CN_ZERO))
                {
                    pCnPortPriTblInfo->u1LastRcvdEvent =
                        (UINT1) CN_DBG_RECEIVE_READY;
                    OsixGetSysTime (&(pCnPortPriTblInfo->u4LastEvtRcvdTime));
                    /*calling sem for all the CNPV present in the port */
                    CnSemRunStateMachine (pCnCompTblEntry, pCnPortTblEntry,
                                          pCnPortPriTblInfo,
                                          (UINT1) CN_RECEIVE_READY);

                }
                else if ((u1CnpvIndicator & (CN_TLV_BIT_ONE << u4Priority))
                         != CN_ZERO)
                {
                    pCnPortPriTblInfo->u1LastRcvdEvent = (UINT1) CN_RECEIVE_TLV;
                    OsixGetSysTime (&(pCnPortPriTblInfo->u4LastEvtRcvdTime));
                    /*calling sem for all the CNPV present in the port */
                    CnSemRunStateMachine (pCnCompTblEntry, pCnPortTblEntry,
                                          pCnPortPriTblInfo,
                                          (UINT1) CN_DBG_RECEIVE_TLV);

                }
                else
                {
                    pCnPortPriTblInfo->u1LastRcvdEvent =
                        (UINT1) CN_DBG_NOT_RECEIVE_TLV;
                    OsixGetSysTime (&(pCnPortPriTblInfo->u4LastEvtRcvdTime));
                    /*calling sem for all the CNPV present in the port */
                    CnSemRunStateMachine (pCnCompTblEntry, pCnPortTblEntry,
                                          pCnPortPriTblInfo,
                                          (UINT1) CN_NOT_RECEIVE_TLV);

                }
            }
        }
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : QueHandleAgedOut
 *
 *    DESCRIPTION      : Function to handle the tlv agedout message  from queue.
 *
 *    INPUT            : u4PortId - Port ID
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *
 ****************************************************************************/
PRIVATE INT4
CnQueHandleAgedOut (UINT4 u4PortId)
{
    UINT4               u4CompId = CN_ZERO;
    UINT4               u4Priority = CN_ZERO;
    UINT4               u4LldpInsCh = CN_ZERO;
    UINT2               u2LocalPortId = CN_ZERO;
    tCnCompTblInfo     *pCnCompTblEntry = NULL;

    tCnPortTblInfo     *pCnPortTblEntry = NULL;
    tCnPortPriTblInfo  *pCnPortPriEntry = NULL;

    CnPortVcmGetCxtInfoFromIfIndex (u4PortId, &u4CompId, &u2LocalPortId);
    pCnCompTblEntry = CN_COMP_TBL_ENTRY (u4CompId);

    if (pCnCompTblEntry != NULL)
    {
        CN_PORT_TBL_ENTRY (u4CompId, u4PortId, pCnPortTblEntry);

        if (pCnPortTblEntry != NULL)
        {
            for (u4Priority = CN_ZERO; u4Priority < CN_MAX_VLAN_PRIORITY;
                 u4Priority++)
            {
                CN_PORT_PRI_ENTRY (u4CompId, u4PortId, u4Priority,
                                   pCnPortPriEntry);
                if ((pCnPortPriEntry != NULL) &&
                    (pCnPortPriEntry->bIsAdminDefMode != OSIX_TRUE))
                {
                    CnUtilLldpInstanceChoice (pCnPortPriEntry, &u4LldpInsCh);
                    if (u4LldpInsCh != CN_LLDP_NONE)
                    {
                        pCnPortPriEntry->u1LastRcvdEvent =
                            (UINT1) CN_DBG_NOT_RECEIVE_TLV;
                        OsixGetSysTime (&(pCnPortPriEntry->u4LastEvtRcvdTime));

                        /*calling sem for all the CNPV present in the port */
                        CnSemRunStateMachine (pCnCompTblEntry,
                                              pCnPortTblEntry,
                                              pCnPortPriEntry,
                                              (UINT1) CN_NOT_RECEIVE_TLV);

                    }
                }
            }
        }

    }

    CN_TRC_ARG1 (u4CompId, (CN_CONTROL_PLANE_TRC | CN_TLV_TRC),
                 "CnQueHandleAgedOut: Processed age-out on"
                 " Port: %d\r\n", u4PortId);

    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : CnQueHandleReReg
 *
 *    DESCRIPTION      : Function to handle the LLDP re register message.
 *
 *    INPUT            : NONE
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : MNONE
 *
 ****************************************************************************/
PRIVATE VOID
CnQueHandleReReg (VOID)
{
    UINT4               u4CompId = CN_ZERO;
    tCnPortTblInfo     *pCnPortTblEntry = NULL;
    tCnPortTblInfo     *pCnTempPortTbl = NULL;
    tCnCompTblInfo     *pCnCompTblEntry = NULL;

    for (u4CompId = CN_MIN_CONTEXT; u4CompId < CN_MAX_CONTEXTS; u4CompId++)
    {
        pCnCompTblEntry = CN_COMP_TBL_ENTRY (u4CompId);
        if (pCnCompTblEntry != NULL)
        {
            pCnPortTblEntry = RBTreeGetFirst (pCnCompTblEntry->PortTbl);
            while (pCnPortTblEntry != NULL)
            {
                if (CnUtilTlvConstruct
                    (pCnPortTblEntry, (UINT1) CN_MSG_PORT_REG) != OSIX_SUCCESS)
                {
                    CN_TRC_ARG1 (pCnPortTblEntry->u4ComponentId,
                                 (CN_ALL_FAILURE_TRC),
                                 "CnQueHandleReReg: LLDP Registration FAILED for "
                                 "Port: %d\n", pCnPortTblEntry->u4PortId);
                }

                pCnTempPortTbl = pCnPortTblEntry;
                pCnPortTblEntry = (tCnPortTblInfo *) RBTreeGetNext
                    (pCnCompTblEntry->PortTbl, pCnTempPortTbl, NULL);
            }
        }
    }

    return;
}

/*-----------------------------------------------------------------------*/
/*                       End of the file  cnque.c                        */
/*-----------------------------------------------------------------------*/
