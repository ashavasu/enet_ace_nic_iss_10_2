/*****************************************************************************
 * Copyright (C) 2010 Aricent Inc . All Rights Reserved
 *
 * $Id: cnhwwr.c,v 1.5 2010/12/14 10:43:29 siva Exp $ 
 *
 * Description: This file contains wrapper functions for the CN NPAPIs
 *****************************************************************************/

#include "cninc.h"
/****************************************************************************
 *
 *    FUNCTION NAME    : CnHwSetDefenseMode
 *
 *    DESCRIPTION      : This function calls the NPAPI to set the 
 *                       defense mode in hardware
 *
 *    INPUT            : u4CompId - Component ID
 *                       u4PortId - Port number
 *                       u1CNPV - CN priority value
 *                       u1DefenseMode - CND defense mode
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_SUCCESS / OSIX_FAILURE
 *
 ****************************************************************************/
PUBLIC INT4
CnHwSetDefenseMode (UINT4 u4CompId, UINT4 u4PortId, UINT1 u1CNPV,
                    UINT1 u1DefenseMode)
{

    /* To handle warnings in case of Trace is not defined */
    UNUSED_PARAM (u4CompId);
    UNUSED_PARAM (u4PortId);
    UNUSED_PARAM (u1CNPV);
    UNUSED_PARAM (u1DefenseMode);

    CN_TRC_ARG4 (u4CompId, CN_CONTROL_PLANE_TRC,
                 "CnHwSetDefenseMode: FsMiCnHwSetDefenseMode called with "
                 "ContextId: %u, Port: %u, CNPV: %u, DefenseMode: %s \r\n",
                 u4CompId, u4PortId, u1CNPV, gau1CnStateStr[u1DefenseMode]);
#ifdef NPAPI_WANTED
    if (FsMiCnHwSetDefenseMode (CN_CONVERT_COMP_ID_TO_CXT_ID (u4CompId),
                                u4PortId, u1CNPV, u1DefenseMode) != FNP_SUCCESS)
    {
        CN_TRC_ARG4 (u4CompId, (CN_CONTROL_PLANE_TRC | CN_ALL_FAILURE_TRC),
                     "CnHwSetDefenseMode: FsMiCnHwSetDefenseMode FAILED for "
                     "ContextId: %u, Port: %u, CNPV: %u DefenseMode %s\r\n",
                     u4CompId, u4PortId, u1CNPV, gau1CnStateStr[u1DefenseMode]);
        return OSIX_FAILURE;
    }
#endif
    CN_TRC_ARG4 (u4CompId, CN_CONTROL_PLANE_TRC,
                 "CnHwSetDefenseMode: FsMiCnHwSetDefenseMode SUCCESS for"
                 " ContextId: %u, Port: %u, CNPV: %u DefenseMode %s\r\n",
                 u4CompId, u4PortId, u1CNPV, gau1CnStateStr[u1DefenseMode]);
    return OSIX_SUCCESS;
}

/******************************************************************************
* Function Name : CnHwSetCpParams
* Description   : This function calls NPAPI to set the CP properties in h/w
* 
* Input(s)      : pCnPortPriEntry - Port priority Entry
*
* Output(s)     : None
*
* Return(s)     : OSIX_SUCCESS / OSIX_FAILURE
* **************************************************************************/
PUBLIC INT4
CnHwSetCpParams (tCnPortPriTblInfo * pCnPortPriEntry)
{
    tCnHwCpParams       CnCpParams;
    UINT4               u4CnCpComponentId = CN_ZERO;
    UINT4               u4PortId = CN_ZERO;

    MEMSET (&CnCpParams, CN_ZERO, sizeof (tCnHwCpParams));

    u4CnCpComponentId = pCnPortPriEntry->u4CnComPriComponentId;
    u4PortId = pCnPortPriEntry->u4PortId;

    CnCpParams.u4CpQSizeSetPoint = pCnPortPriEntry->u4CpQSizeSetPoint;
    CnCpParams.u4CpMinSampleBase = pCnPortPriEntry->u4CpMinSampleBase;
    MEMCPY (&CnCpParams.au1CnCpIdentifier, pCnPortPriEntry->au1CnCpIdentifier,
            CN_CPID_LEN);
    CnCpParams.u1CnCpPriority = pCnPortPriEntry->u1CNPV;
    CnCpParams.u1CpMinHeaderOctets = pCnPortPriEntry->u1CpMinHeaderOctets;
    CnCpParams.i1CnCpFeedbackWeight = pCnPortPriEntry->i1CnCpFeedbackWeight;

    CN_TRC_ARG3 (u4CnCpComponentId, CN_CONTROL_PLANE_TRC,
                 "CnHwSetCpParams: "
                 "FsMiCnHwSetCpParams called with ContextId: %u, "
                 "Port: %u, Queue: %u\r\n", u4CnCpComponentId,
                 u4PortId, pCnPortPriEntry->u4TrafficClass);

#ifdef NPAPI_WANTED
    if (FsMiCnHwSetCpParams (CN_CONVERT_COMP_ID_TO_CXT_ID (u4CnCpComponentId),
                             u4PortId, pCnPortPriEntry->u4TrafficClass,
                             &CnCpParams) != FNP_SUCCESS)
    {
        CN_TRC_ARG3 (u4CnCpComponentId,
                     (CN_CONTROL_PLANE_TRC | CN_ALL_FAILURE_TRC),
                     "CnHwSetCpParams: "
                     "FsMiCnHwSetCpParams FAILED for ContextId: %u, "
                     "Port: %u, Queue: %u\r\n", u4CnCpComponentId,
                     u4PortId, pCnPortPriEntry->u4TrafficClass);
        return OSIX_FAILURE;
    }
#endif
    CN_TRC_ARG3 (u4CnCpComponentId, CN_CONTROL_PLANE_TRC,
                 "CnHwSetCpParams:"
                 "FsMiCnHwSetCpParams SUCCESS for ContextId: %u, "
                 "Port: %u, Queue: %u\r\n", u4CnCpComponentId,
                 u4PortId, pCnPortPriEntry->u4TrafficClass);
    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : CnHwSetCnmPri
 *
 *    DESCRIPTION      : This function calls the NPAPI to set the CNM priority
 *                       in h/w
 *
 *    INPUT            : u4CompId - Virtual component Id
 *                       u1CnmPri - VLAN priority of CNM generated
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_SUCCESS / OSIX_FAILURE
 *
 ****************************************************************************/
PUBLIC INT4
CnHwSetCnmPri (UINT4 u4CompId, UINT1 u1CnmPri)
{

    /* To handle warnings in case of Trace is not defined */
    UNUSED_PARAM (u4CompId);
    UNUSED_PARAM (u1CnmPri);

    CN_TRC_ARG2 (u4CompId, CN_CONTROL_PLANE_TRC,
                 "CnHwSetCnmPri: "
                 "FsMiCnHwSetCnmPriority called with ContextId: %u, "
                 "CNM Priority: %u\r\n", u4CompId, u1CnmPri);

#ifdef NPAPI_WANTED
    if (FsMiCnHwSetCnmPriority
        (CN_CONVERT_COMP_ID_TO_CXT_ID (u4CompId), u1CnmPri) != FNP_SUCCESS)
    {
        CN_TRC_ARG2 (u4CompId,
                     (CN_CONTROL_PLANE_TRC | CN_ALL_FAILURE_TRC),
                     "CnHwSetCnmPri: "
                     "FsMiCnHwSetCnmPriority FAILED for ContextId: %u, "
                     "CNM Priority: %u\r\n", u4CompId, u1CnmPri);
        return OSIX_FAILURE;
    }
#endif
    CN_TRC_ARG2 (u4CompId, CN_CONTROL_PLANE_TRC,
                 "CnHwSetCnmPri: "
                 "FsMiCnHwSetCnmPriority SUCCESS for ContextId: %u, "
                 "CNM Priority: %u\r\n", u4CompId, u1CnmPri);

    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : CnHwReSetCounters
 *
 *    DESCRIPTION      : This function invokes NPAPI to reset the counters
 *                       in h/w.
 *
 *    INPUT            : u4CompId - Virutal component Id
 *                       u4PortId - Port Number
 *                       u1Priority - CN priority value
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_SUCCESS / OSIX_FAILURE
 *
 ****************************************************************************/
PUBLIC INT4
CnHwReSetCounters (UINT4 u4CompId, UINT4 u4PortId, UINT1 u1Priority)
{

    /* To handle warnings in case of Trace is not defined */
    UNUSED_PARAM (u4CompId);
    UNUSED_PARAM (u4PortId);
    UNUSED_PARAM (u1Priority);

    CN_TRC_ARG3 (u4CompId,
                 (CN_CONTROL_PLANE_TRC),
                 "CnHwReSetCounters: "
                 "FsMiCnHwReSetCounters called with ContextId: %u, "
                 "Port: %u, CNPV: %u\r\n", u4CompId, u4PortId, u1Priority);
#ifdef NPAPI_WANTED
    if (FsMiCnHwReSetCounters (CN_CONVERT_COMP_ID_TO_CXT_ID (u4CompId),
                               u4PortId, u1Priority) != FNP_SUCCESS)
    {
        CN_TRC_ARG3 (u4CompId,
                     (CN_CONTROL_PLANE_TRC | CN_ALL_FAILURE_TRC),
                     "CnHwReSetCounters: "
                     "FsMiCnHwReSetCounters FAILED for ContextId: %u, "
                     "Port: %u, CNPV: %u\r\n", u4CompId, u4PortId, u1Priority);
        return OSIX_FAILURE;
    }
#endif
    CN_TRC_ARG3 (u4CompId, CN_CONTROL_PLANE_TRC,
                 "CnHwReSetCounters: "
                 "FsMiCnHwReSetCounters SUCCESS for ContextId: %u, "
                 "Port: %u, CNPV: %u\r\n", u4CompId, u4PortId, u1Priority);
    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : CnHwGetCounters
 *
 *    DESCRIPTION      : This function willbe called from lldp events to the
 *
 *    INPUT            : u4CompId   - Component Id
 *                       u4PortId   - Interface Index
 *                       u1Priority - CNPV
 *
 *    OUTPUT           : pDisFrames   - Discarded Frames
 *                       pTransFrames - Transmitted Frames
 *                       pTransCnms   - Transmitted CNMs
 *
 *    RETURNS          : OSIX_SUCCESS / OSIX_FAILURE
 *
 ****************************************************************************/
PUBLIC INT4
CnHwGetCounters (UINT4 u4CompId, UINT4 u4PortId, UINT1 u1Priority,
                 tSNMP_COUNTER64_TYPE * pDisFrames,
                 tSNMP_COUNTER64_TYPE * pTransFrames,
                 tSNMP_COUNTER64_TYPE * pTransCnms)
{
    FS_UINT8            u8DisFrames;
    FS_UINT8            u8TransFrames;
    FS_UINT8            u8TransCnms;

    FSAP_U8_CLR (&u8DisFrames);
    FSAP_U8_CLR (&u8TransFrames);
    FSAP_U8_CLR (&u8TransCnms);

    /* To handle warnings in case of Trace is not defined */
    UNUSED_PARAM (u4CompId);
    UNUSED_PARAM (u4PortId);
    UNUSED_PARAM (u1Priority);

    CN_TRC_ARG3 (u4CompId, CN_CONTROL_PLANE_TRC,
                 "CnHwGetCounters: "
                 "FsMiCnHwGetCounters called with ContextId: %u, "
                 "Port: %u, CNPV: %u\r\n", u4CompId, u4PortId, u1Priority);

#ifdef NPAPI_WANTED
    if (FsMiCnHwGetCounters (CN_CONVERT_COMP_ID_TO_CXT_ID (u4CompId),
                             u4PortId, u1Priority, &u8DisFrames,
                             &u8TransFrames, &u8TransCnms) != FNP_SUCCESS)
    {
        CN_TRC_ARG3 (u4CompId,
                     (CN_CONTROL_PLANE_TRC | CN_ALL_FAILURE_TRC),
                     "CnHwGetCounters: "
                     "FsMiCnHwGetCounters FAILED for ContextId: %u, "
                     "Port: %u, CNPV: %u\r\n", u4CompId, u4PortId, u1Priority);
        return OSIX_FAILURE;
    }
#endif
    pDisFrames->msn = u8DisFrames.u4Hi;
    pDisFrames->lsn = u8DisFrames.u4Lo;

    pTransFrames->msn = u8TransFrames.u4Hi;
    pTransFrames->lsn = u8TransFrames.u4Lo;

    pTransCnms->msn = u8TransCnms.u4Hi;
    pTransCnms->lsn = u8TransCnms.u4Lo;

    CN_TRC_ARG3 (u4CompId, CN_CONTROL_PLANE_TRC,
                 "CnHwGetCounters: "
                 "FsMiCnHwGetCounters SUCCESS for ContextId: %u, "
                 "Port: %u, CNPV: %u\r\n", u4CompId, u4PortId, u1Priority);
    return OSIX_SUCCESS;
}

/* End of File */
