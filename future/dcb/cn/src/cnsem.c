/*****************************************************************************
 * Copyright (C) 2010 Aricent Inc . All Rights Reserved
 *
 * $Id: cnsem.c,v 1.8 2014/01/25 13:55:08 siva Exp $
 *
 * Description: This file contains CN SEM related functions
 *****************************************************************************/
#include "cninc.h"
#include "cnsem.h"

/****************************************************************************
 *
 *    FUNCTION NAME    : CnSemRunStateMachine 
 *
 *    DESCRIPTION      : This function willbe called from lldp events to the
 *                       CN State Event Machine.
 *                       This function calls the appropriate  state routine
 *                       based on the current state and event received.
 *                       State-Event Machine is running on per port per priority
 *
 *    INPUT            : pCnCompTblEntry - pointer to component table entry
 *                       pCnPortTblEntry - pointer to port table entry
 *                       pPortPriTable - pointer to port priority structure
 *                       u1Event - Event
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : NONE
 *
 ****************************************************************************/

PUBLIC VOID
CnSemRunStateMachine (tCnCompTblInfo * pCnCompTblEntry,
                      tCnPortTblInfo * pCnPortTblEntry,
                      tCnPortPriTblInfo * pPortPriTable, UINT1 u1Event)
{
    UINT1               u1ActionProcIdx = CN_ZERO;
    UINT1               u1CurrentState = CN_ZERO;

    /* Get the current state */
    u1CurrentState = (UINT1) pPortPriTable->u1PortPriOperDefMode;

    /* Get the index of the action procedure */
    if ((u1Event < CN_MAX_EVENTS) && ((u1CurrentState < CN_MAX_STATES) &&
                                      (u1CurrentState > CN_ZERO)))
    {
        u1ActionProcIdx = gau1CnSem[u1Event][u1CurrentState - CN_ONE];

        CN_TRC_ARG5 (pPortPriTable->u4CnComPriComponentId,
                     (CN_SEM_TRC | CN_CONTROL_PLANE_TRC),
                     "CnSemRunStateMachine: ContextId: %d, Port: %d,"
                     " CNPV: %d\n, STATE: %s, EVENT: %s\r\n",
                     pPortPriTable->u4CnComPriComponentId,
                     pPortPriTable->u4PortId,
                     pPortPriTable->u1CNPV,
                     gau1CnStateStr[u1CurrentState], gau1CnEvntStr[u1Event]);

        /* Call corresponding states */
        (*gaCnActionProc[u1ActionProcIdx]) (pCnCompTblEntry,
                                            pCnPortTblEntry, pPortPriTable);

    }

    return;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : CnSmEventIgnore
 *
 *    DESCRIPTION      : Handle Invalid event received
 *
 *    INPUT            : pCompEntry   -Pointer to Comp Table Entry
 *                       pPortEntry   - Pointer to Port Table Entry
 *                       pPortPriEntry - Port priority  on which this event
 *                                       has occured
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : NONE
 *
 ****************************************************************************/
PUBLIC VOID
CnSmEventIgnore (tCnCompTblInfo * pCompEntry, tCnPortTblInfo * pPortEntry,
                 tCnPortPriTblInfo * pPortPriEntry)
{
    /* To handle warnings in case of Trace is not defined */

    UNUSED_PARAM (pCompEntry);
    UNUSED_PARAM (pPortEntry);
    UNUSED_PARAM (pPortPriEntry);
    CN_TRC_ARG4 (pPortPriEntry->u4CnComPriComponentId,
                 (CN_SEM_TRC | CN_CONTROL_PLANE_TRC),
                 "CnSmEventIgnore: ContextId: %d, Port: %d, CNPV: %d"
                 " Event ignored, Current state %d\r\n",
                 pPortPriEntry->u4CnComPriComponentId,
                 pPortPriEntry->u4PortId,
                 pPortPriEntry->u1CNPV, pPortPriEntry->u1PortPriOperDefMode);
    return;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : CnSemStateDisabled 
 *
 *    DESCRIPTION      : CN SEM Disabled state 
 *
 *    INPUT            : pCompEntry   -Pointer to Comp Table Entry
 *                       pPortEntry   - Pointer to Port Table Entry
 *                       pPortPriTable - Port priority  on which this event
 *                                       has occured
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : NONE
 *
 ****************************************************************************/
PUBLIC VOID
CnSmStateDisabled (tCnCompTblInfo * pCompEntry, tCnPortTblInfo * pPortEntry,
                   tCnPortPriTblInfo * pPortPriTable)
{
    UINT1               u1OutPri = CN_ZERO;

    /* CN module's master status should be TRUE here, if it were false
     * then this state machine function should not have been called */

    /*set current defense mode */
    pPortPriTable->u1PortPriOperDefMode = CN_DISABLED;

    CN_TRC_ARG3 (pPortPriTable->u4CnComPriComponentId,
                 (CN_SEM_TRC | CN_CONTROL_PLANE_TRC),
                 "CnSmStateDisabled: ContextId: %d, Port: %d, CNPV: %d,"
                 " State: Disabled\r\n",
                 pPortPriTable->u4CnComPriComponentId,
                 pPortPriTable->u4PortId, pPortPriTable->u1CNPV);

    pPortPriTable->bXmitCnpvCapable = OSIX_FALSE;    /*set the cnpv indicator */
    pPortPriTable->bXmitReady = OSIX_FALSE;    /*set the ready indicator */

    CnUtilCalculateTlvIndicators (pCompEntry, pPortEntry, pPortPriTable);

    /*DISABLE REMAPPING  should have same priority for
       incoming and outgoing */

    /*Qos Delete Priority should be called here */

    u1OutPri = pPortPriTable->u1CNPV;

    CnPortQosPriorityMapReq (pPortPriTable->u4CnComPriComponentId,
                             pPortPriTable->u4PortId,
                             pPortPriTable->u1CNPV,
                             &u1OutPri, QOS_DEL_PRIORITY_MAP);

    CnHwSetDefenseMode (pPortPriTable->u4CnComPriComponentId,
                        pPortPriTable->u4PortId,
                        pPortPriTable->u1CNPV,
                        pPortPriTable->u1PortPriOperDefMode);
    return;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : CnSmStateEdge
 *
 *    DESCRIPTION      : CN SEM Edge state
 *
 *    INPUT            : pCompEntry   -Pointer to Comp Table Entry
 *                       pPortEntry   - Pointer to Port Table Entry
 *                       pPortPriTable - Port priority  on which this event
 *                                       has occured
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : NONE
 *
 ****************************************************************************/
PUBLIC VOID
CnSmStateEdge (tCnCompTblInfo * pCompEntry, tCnPortTblInfo * pPortEntry,
               tCnPortPriTblInfo * pPortPriTable)
{

    INT4                i4RetVal = OSIX_SUCCESS;
    UINT1               u1OutPri = CN_ZERO;

    /* CN module's master status should be TRUE here, if it were false
     * then this state machine function should not have been called */

    /*set current defense mode */
    pPortPriTable->u1PortPriOperDefMode = CN_EDGE;

    CN_TRC_ARG3 (pPortPriTable->u4CnComPriComponentId,
                 (CN_SEM_TRC | CN_CONTROL_PLANE_TRC),
                 "CnSmStateEdge: ContextId: %d, Port: %d, CNPV: %d,"
                 " State: Edge\r\n",
                 pPortPriTable->u4CnComPriComponentId,
                 pPortPriTable->u4PortId, pPortPriTable->u1CNPV);

    pPortPriTable->bXmitCnpvCapable = OSIX_TRUE;    /*set the cnpv indicator */
    pPortPriTable->bXmitReady = OSIX_FALSE;    /*set the ready indicator */
    i4RetVal = CnUtilCalculateTlvIndicators (pCompEntry, pPortEntry,
                                             pPortPriTable);

    /*TURN_ON DEFENSE
       should remap the incoming packet with  alternate priority */

    u1OutPri = pPortPriTable->u1PortPriOperAltPri;

    CnPortQosPriorityMapReq (pPortPriTable->u4CnComPriComponentId,
                             pPortPriTable->u4PortId,
                             pPortPriTable->u1CNPV,
                             &u1OutPri, QOS_ADD_PRIORITY_MAP);

    CnHwSetDefenseMode (pPortPriTable->u4CnComPriComponentId,
                        pPortPriTable->u4PortId,
                        pPortPriTable->u1CNPV,
                        pPortPriTable->u1PortPriOperDefMode);

    CnHwSetCpParams (pPortPriTable);

    UNUSED_PARAM (i4RetVal);
    return;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : CnSmStateInterior
 *
 *    DESCRIPTION      : CN SEM Interior state
 *
 *    INPUT            : pCompEntry   -Pointer to Comp Table Entry
 *                       pPortEntry   - Pointer to Port Table Entry
 *                       pPortPriTable - Port priority  on which this event
 *                                       has occured
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : NONE
 *
 ****************************************************************************/
PUBLIC VOID
CnSmStateInterior (tCnCompTblInfo * pCompEntry, tCnPortTblInfo * pPortEntry,
                   tCnPortPriTblInfo * pPortPriTable)
{
    INT4                i4RetVal = OSIX_SUCCESS;
    UINT1               u1OutPri = CN_ZERO;

    /* CN module's master status should be TRUE here, if it were false
     * then this state machine function should not have been called */

    /*set current defense mode */
    pPortPriTable->u1PortPriOperDefMode = CN_INTERIOR;

    CN_TRC_ARG3 (pPortPriTable->u4CnComPriComponentId,
                 (CN_SEM_TRC | CN_CONTROL_PLANE_TRC),
                 "CnSmStateInterior: ContextId: %d, Port: %d, CNPV: %d,"
                 " State: Interior\r\n",
                 pPortPriTable->u4CnComPriComponentId,
                 pPortPriTable->u4PortId, pPortPriTable->u1CNPV);

    pPortPriTable->bXmitCnpvCapable = OSIX_TRUE;    /*set the cnpv indicator */
    pPortPriTable->bXmitReady = CNPD_ACCEPTS_CNTAG;    /*set the ready indicator */
    i4RetVal = CnUtilCalculateTlvIndicators (pCompEntry, pPortEntry,
                                             pPortPriTable);

    /*TRUN OFF DEFENSE  should have same priority for
       incoming and outgoing */

    /*QoS add Priority should be called here */
    u1OutPri = pPortPriTable->u1CNPV;

    CnPortQosPriorityMapReq (pPortPriTable->u4CnComPriComponentId,
                             pPortPriTable->u4PortId,
                             pPortPriTable->u1CNPV,
                             &u1OutPri, QOS_ADD_PRIORITY_MAP);

    CnHwSetDefenseMode (pPortPriTable->u4CnComPriComponentId,
                        pPortPriTable->u4PortId,
                        pPortPriTable->u1CNPV,
                        pPortPriTable->u1PortPriOperDefMode);

    CnHwSetCpParams (pPortPriTable);

    UNUSED_PARAM (i4RetVal);
    return;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : CnSmStateInteriorReady
 *
 *    DESCRIPTION      : CN SEM Interior Ready state
 *
 *    INPUT            : pCompEntry   -Pointer to Comp Table Entry
 *                       pPortEntry   - Pointer to Port Table Entry
 *                       pPortPriTable - Port priority  on which this event
 *                                       has occured
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : NONE
 *
 ****************************************************************************/
PUBLIC VOID
CnSmStateInteriorReady (tCnCompTblInfo * pCompEntry,
                        tCnPortTblInfo * pPortEntry,
                        tCnPortPriTblInfo * pPortPriTable)
{

    INT4                i4RetVal = OSIX_SUCCESS;
    UINT1               u1OutPri = CN_ZERO;

    /* CN module's master status should be TRUE here, if it were false
     * then this state machine function should not have been called */

    /*set current defense mode */
    pPortPriTable->u1PortPriOperDefMode = CN_INTERIOR_READY;

    CN_TRC_ARG3 (pPortPriTable->u4CnComPriComponentId,
                 (CN_SEM_TRC | CN_CONTROL_PLANE_TRC),
                 "CnSmStateInteriorReady: ContextId: %d, Port: %d, CNPV: %d,"
                 " State: Interior Ready\r\n",
                 pPortPriTable->u4CnComPriComponentId,
                 pPortPriTable->u4PortId, pPortPriTable->u1CNPV);

    pPortPriTable->bXmitCnpvCapable = OSIX_TRUE;    /*set the cnpv indicator */
    pPortPriTable->bXmitReady = CNPD_ACCEPTS_CNTAG;    /*set the ready indicator */
    i4RetVal = CnUtilCalculateTlvIndicators (pCompEntry, pPortEntry,
                                             pPortPriTable);

    /*TRUN OFF DEFENSE  should have same priority for
       incoming and outgoing */

    /*QoS add Priority should be called here */

    u1OutPri = pPortPriTable->u1CNPV;
    CnPortQosPriorityMapReq (pPortPriTable->u4CnComPriComponentId,
                             pPortPriTable->u4PortId,
                             pPortPriTable->u1CNPV,
                             &u1OutPri, QOS_ADD_PRIORITY_MAP);

    CnHwSetDefenseMode (pPortPriTable->u4CnComPriComponentId,
                        pPortPriTable->u4PortId,
                        pPortPriTable->u1CNPV,
                        pPortPriTable->u1PortPriOperDefMode);

    CnHwSetCpParams (pPortPriTable);

    UNUSED_PARAM (i4RetVal);
    return;
}

/*--------------------------------------------------------------------------*/
/*                       End of the file  cnsem.c                           */
/*--------------------------------------------------------------------------*/
