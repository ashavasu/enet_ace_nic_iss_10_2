/********************************************************************
 * Copyright (C) 2010 Aricent Inc . All Rights Reserved
 *
 * $Id: cnmod.c,v 1.8 2010/10/28 11:49:50 prabuc Exp $
 *
 * Description: This file contains module start and  module shutdown
 *              routines.
 *********************************************************************/
#include "cninc.h"

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : CnModuleStart
 *                                                                          
 *    DESCRIPTION      : This function allocates memory pools for all tables 
 *                       in CN module. It also initalizes the global 
 *                       structure.
 *
 *    INPUT            : None
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *                                                                          
 ****************************************************************************/
PUBLIC INT4
CnModuleStart (VOID)
{
    UINT4               u4RBNodeOffset = CN_ZERO;
    UINT4               u4ContextId = CN_ZERO;
    UINT4               u4NextContextId = CN_ZERO;
    UINT4               u4CompId = CN_ZERO;
    tCnCompTblInfo     *pCnCompTblEntry = NULL;
    tLldpAppPortMsg     LldpAppPortMsg;
    tCnPortTblInfo     *pCnPortTblEntry = NULL;
    tCnPortTblInfo     *pCnTempPortTbl = NULL;
    UINT1               au1TlvBuf[CN_TLV_LEN] = { CN_ZERO };

    MEMSET (&LldpAppPortMsg, CN_ZERO, sizeof (tLldpAppPortMsg));
    MEMSET (&au1TlvBuf, CN_ZERO, CN_TLV_LEN);

    if (CnSizingMemCreateMemPools () == OSIX_FAILURE)
    {
        CN_GBL_TRC (CN_INVALID_CONTEXT, CN_CRITICAL_TRC,
                    "CnModuleStart: Memory Initialization FAILED !!!\r\n");
        CnSizingMemDeleteMemPools ();
        return (OSIX_FAILURE);
    }

    /*Assigning respective mempool Id's */
    CnMainAssignMempoolIds ();

    /* CN - CPID Table RBTree creation */
    u4RBNodeOffset = FSAP_OFFSETOF (tCnPortPriTblInfo, NextCpIdNode);
    gCnMasterTblInfo.CpIdTbl =
        RBTreeCreateEmbedded (u4RBNodeOffset, CnUtilRBCmpCpIdTbl);
    /*CN Timer Init */
    if (CnTmrInit () == OSIX_FAILURE)
    {
        CN_GBL_TRC (CN_INVALID_CONTEXT, CN_CRITICAL_TRC,
                    "CnModuleStart: Timer Initialization FAILED !!!\r\n");
        return (OSIX_FAILURE);
    }
    /* Initialize global information */
    if (CnMainInitMasterTblInfo () == OSIX_FAILURE)
    {
        CN_GBL_TRC (CN_INVALID_CONTEXT, CN_CRITICAL_TRC,
                    "CnModuleStart: CnMainInitMasterTblInfo FAILED !!!\r\n");
        return (OSIX_FAILURE);
    }
    /*Registration with LLDP as an anpplication */
    if (CnPortLldpApplRegWithL2iwf () == OSIX_FAILURE)
    {
        CN_GBL_TRC (CN_INVALID_CONTEXT, CN_CRITICAL_TRC,
                    "CnModuleStart: Registration FAILED in L2IWF"
                    "as LLDP application\r\n");
        return (OSIX_FAILURE);
    }
    /*to Form first six bytes of tlv */
    CnUtilConstructPreFormTlv ();

    /* Create first context */
    CnPortVcmGetFirstVcId (&u4ContextId);
    if (CnCxtCreateContext (u4ContextId) == OSIX_FAILURE)
    {
        CN_GBL_TRC_ARG1 (CN_INVALID_CONTEXT, CN_CRITICAL_TRC,
                         "CnModuleStart: ContextId %u creation FAILED\r\n",
                         u4ContextId);
        return (OSIX_FAILURE);
    }

    /* Get all the ports under that context and add in CN database */
    CnIfCreateAllPorts (u4ContextId);

    /*create all other existing contexts */
    while (CnPortVcmGetNextVcId (u4ContextId, &u4NextContextId) == OSIX_SUCCESS)
    {
        if (CnCxtCreateContext (u4NextContextId) == OSIX_FAILURE)
        {
            CN_GBL_TRC_ARG1 (CN_INVALID_CONTEXT, CN_CRITICAL_TRC,
                             "CnModuleStart: ContextId %u creation FAILED\r\n",
                             u4NextContextId);
            return (OSIX_FAILURE);
        }

        CnIfCreateAllPorts (u4NextContextId);
        u4ContextId = u4NextContextId;
    }

    for (u4CompId = CN_MIN_CONTEXT; u4CompId < CN_MAX_CONTEXTS; u4CompId++)
    {
        pCnCompTblEntry = CN_COMP_TBL_ENTRY (u4CompId);
        if (pCnCompTblEntry != NULL)
        {
            pCnPortTblEntry = RBTreeGetFirst (pCnCompTblEntry->PortTbl);
            while (pCnPortTblEntry != NULL)
            {
                LldpAppPortMsg.pu1TxAppTlv = au1TlvBuf;
                CnUtilFillLldpPortMsg (&LldpAppPortMsg);
                if (CnPortPostCnMsgToLldp
                    (CN_MSG_PORT_REG, pCnPortTblEntry->u4PortId,
                     &LldpAppPortMsg) == OSIX_FAILURE)
                {
                    CN_TRC_ARG1 (u4CompId, CN_ALL_FAILURE_TRC,
                                 "CnModuleStart: LLDP port Reg FAILED for "
                                 "Port %d\r\n", pCnPortTblEntry->u4PortId);
                }
                pCnTempPortTbl = pCnPortTblEntry;
                pCnPortTblEntry = (tCnPortTblInfo *) RBTreeGetNext
                    (pCnCompTblEntry->PortTbl, pCnTempPortTbl, NULL);
            }
        }
    }
    CnModuleEnable ();
    return (OSIX_SUCCESS);

}

/****************************************************************************
 *
 *    FUNCTION NAME    : CnModuleShutdown
 *
 *    DESCRIPTION      : This function deallocates memory pools for all tables
 *                       in CN module. 
 *
 *    INPUT            : None
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *
 ****************************************************************************/
PUBLIC VOID
CnModuleShutdown (VOID)
{

    UINT4               u4CompId = CN_ZERO;
    tCnCompTblInfo     *pCnCompTblEntry = NULL;
    tLldpAppPortMsg     LldpAppPortMsg;
    tCnPortTblInfo     *pCnPortTblEntry = NULL;
    tCnPortTblInfo     *pCnTempPortTbl = NULL;
    UINT1               au1TlvBuf[CN_TLV_LEN] = { CN_ZERO };

    MEMSET (&LldpAppPortMsg, CN_ZERO, sizeof (tLldpAppPortMsg));
    MEMSET (&au1TlvBuf, CN_ZERO, CN_TLV_LEN);

    CnModuleDisable ();

    for (u4CompId = CN_MIN_CONTEXT; u4CompId < CN_MAX_CONTEXTS; u4CompId++)
    {
        pCnCompTblEntry = CN_COMP_TBL_ENTRY (u4CompId);
        if (pCnCompTblEntry != NULL)
        {
            pCnPortTblEntry = RBTreeGetFirst (pCnCompTblEntry->PortTbl);
            while (pCnPortTblEntry != NULL)
            {
                if (pCnPortTblEntry->TlvTxDelayTmrNode.u1Status ==
                    CN_TMR_RUNNING)
                {
                    TmrStopTimer (gCnMasterTblInfo.TmrListId,
                                  &(pCnPortTblEntry->TlvTxDelayTmrNode.CnTmrBlk.
                                    TimerNode));
                }
                LldpAppPortMsg.pu1TxAppTlv = au1TlvBuf;
                CnUtilFillLldpPortMsg (&LldpAppPortMsg);
                if (CnPortPostCnMsgToLldp
                    (CN_MSG_PORT_DEREG, pCnPortTblEntry->u4PortId,
                     &LldpAppPortMsg) == OSIX_FAILURE)
                {
                    CN_TRC_ARG1 (u4CompId,
                                 (CN_ALL_FAILURE_TRC),
                                 "CnModuleShutdown: LLDP port De-Reg FAILED for "
                                 "Port %d\r\n", pCnPortTblEntry->u4PortId);
                }
                pCnTempPortTbl = pCnPortTblEntry;
                pCnPortTblEntry = (tCnPortTblInfo *) RBTreeGetNext
                    (pCnCompTblEntry->PortTbl, pCnTempPortTbl, NULL);
            }
        }
    }

    /*Disalbing the master */
    gCnMasterTblInfo.u1CnSysControl = CN_SHUTDOWN;

    CnUtilDeleteRBTrees ();

    CnUtilDeleteQMsg (gCnMasterTblInfo.cnTaskQId, gCnMasterTblInfo.QMsgPoolId);

    CnSizingMemDeleteMemPools ();
    /*Timer deinit called for delay timer */
    if (gCnMasterTblInfo.TmrListId != CN_ZERO)
    {
        if (CnTmrDeInit () == OSIX_FAILURE)
        {
            CN_GBL_TRC (CN_INVALID_CONTEXT, CN_CRITICAL_TRC,
                        "CnModuleShutDown: Timer Deinit FAILED\r\n");
            CN_GBL_TRC (CN_INVALID_CONTEXT, CN_CRITICAL_TRC,
                        "CnModuleShutDown: " "CN shutdown is unsuccessful\r\n");
        }
    }

    gCnMasterTblInfo.ppCompTblInfo = NULL;

    if (CnPortLldpApplDeRegWithL2iwf () == OSIX_FAILURE)
    {
        CN_GBL_TRC (CN_INVALID_CONTEXT, CN_CRITICAL_TRC,
                    "CnModuleShutDown: "
                    "L2IWF application Deregister FAILED \r\n");
    }

    return;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : CnModuleEnable
 *                                                                          
 *    DESCRIPTION      : This function is called to enable CN module
 *
 *    INPUT            : None 
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : None
 *                                                                          
 ****************************************************************************/
PUBLIC VOID
CnModuleEnable (VOID)
{
#ifndef FM_WANTED
    INT4                i4SysLogId = -1;
#endif
    /*since there is no CNPV present in the system start, this function will 
       register with SYSLOG alone */
#ifndef FM_WANTED
    /* Register the module with Syslog server */
    i4SysLogId = SYS_LOG_REGISTER (CN_TASK_NAME, SYSLOG_ALERT_LEVEL);
    if (i4SysLogId < CN_ZERO)
    {
        CN_GBL_TRC (CN_INVALID_CONTEXT, CN_CRITICAL_TRC, "CnModuleEnable: "
                    "FAILED to register with Syslog\r\n");
        return;
    }
    gCnMasterTblInfo.u4SysLogId = (UINT4) i4SysLogId;
#endif
    return;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : CnModuleDisable 
 *                                                                          
 *    DESCRIPTION      : This function is called to disable CN module. 
 *                       
 *    INPUT            : None
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : None
 *                                                                          
 ****************************************************************************/
PUBLIC VOID
CnModuleDisable (VOID)
{
    UINT4               u4CompId = CN_ZERO;

#ifndef FM_WANTED
    SYS_LOG_DEREGISTER (gCnMasterTblInfo.u4SysLogId);
    gCnMasterTblInfo.u4SysLogId = CN_ZERO;
#endif
    /*disabling master for all the components */
    for (u4CompId = CN_MIN_CONTEXT; u4CompId < CN_MAX_CONTEXTS; u4CompId++)
    {
        if (CN_COMP_TBL_ENTRY (u4CompId) != NULL)
        {
            CnUtilMasterDisable (CN_COMP_TBL_ENTRY (u4CompId));
        }
    }
    return;
}

/*-----------------------------------------------------------------------*/
/*                       End of the file  cnmod.c                        */
/*-----------------------------------------------------------------------*/
