/* $Id: stdcnwr.c,v 1.3 2011/09/24 06:38:24 siva Exp $ */
# include  "lr.h"
# include  "fssnmp.h"
# include  "stdcnwr.h"
# include  "stdcndb.h"
# include  "cninc.h"
INT4
GetNextIndexIeee8021CnGlobalTable (tSnmpIndex * pFirstMultiIndex,
                                   tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexIeee8021CnGlobalTable
            (&(pNextMultiIndex->pIndex[0].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexIeee8021CnGlobalTable
            (pFirstMultiIndex->pIndex[0].u4_ULongValue,
             &(pNextMultiIndex->pIndex[0].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

VOID
RegisterSTDCN ()
{
    SNMPRegisterMibWithLock (&stdcnOID, &stdcnEntry, CnLock, CnUnLock,
                             SNMP_MSR_TGR_TRUE);
    SNMPAddSysorEntry (&stdcnOID, (const UINT1 *) "stdcn");
}

VOID
UnRegisterSTDCN ()
{
    SNMPUnRegisterMib (&stdcnOID, &stdcnEntry);
    SNMPDelSysorEntry (&stdcnOID, (const UINT1 *) "stdcn");
}

INT4
Ieee8021CnGlobalMasterEnableGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIeee8021CnGlobalTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIeee8021CnGlobalMasterEnable
            (pMultiIndex->pIndex[0].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Ieee8021CnGlobalCnmTransmitPriorityGet (tSnmpIndex * pMultiIndex,
                                        tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIeee8021CnGlobalTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIeee8021CnGlobalCnmTransmitPriority
            (pMultiIndex->pIndex[0].u4_ULongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
Ieee8021CnGlobalDiscardedFramesGet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIeee8021CnGlobalTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIeee8021CnGlobalDiscardedFrames
            (pMultiIndex->pIndex[0].u4_ULongValue,
             &(pMultiData->u8_Counter64Value)));

}

INT4
Ieee8021CnGlobalMasterEnableSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetIeee8021CnGlobalMasterEnable
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->i4_SLongValue));

}

INT4
Ieee8021CnGlobalCnmTransmitPrioritySet (tSnmpIndex * pMultiIndex,
                                        tRetVal * pMultiData)
{
    return (nmhSetIeee8021CnGlobalCnmTransmitPriority
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->u4_ULongValue));

}

INT4
Ieee8021CnGlobalMasterEnableTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    return (nmhTestv2Ieee8021CnGlobalMasterEnable (pu4Error,
                                                   pMultiIndex->pIndex[0].
                                                   u4_ULongValue,
                                                   pMultiData->i4_SLongValue));

}

INT4
Ieee8021CnGlobalCnmTransmitPriorityTest (UINT4 *pu4Error,
                                         tSnmpIndex * pMultiIndex,
                                         tRetVal * pMultiData)
{
    return (nmhTestv2Ieee8021CnGlobalCnmTransmitPriority (pu4Error,
                                                          pMultiIndex->
                                                          pIndex[0].
                                                          u4_ULongValue,
                                                          pMultiData->
                                                          u4_ULongValue));

}

INT4
Ieee8021CnGlobalTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                          tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2Ieee8021CnGlobalTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexIeee8021CnErroredPortTable (tSnmpIndex * pFirstMultiIndex,
                                        tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexIeee8021CnErroredPortTable
            (&(pNextMultiIndex->pIndex[0].u4_ULongValue),
             &(pNextMultiIndex->pIndex[1].u4_ULongValue),
             &(pNextMultiIndex->pIndex[2].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexIeee8021CnErroredPortTable
            (pFirstMultiIndex->pIndex[0].u4_ULongValue,
             &(pNextMultiIndex->pIndex[0].u4_ULongValue),
             pFirstMultiIndex->pIndex[1].u4_ULongValue,
             &(pNextMultiIndex->pIndex[1].u4_ULongValue),
             pFirstMultiIndex->pIndex[2].i4_SLongValue,
             &(pNextMultiIndex->pIndex[2].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
Ieee8021CnEpIfIndexGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIeee8021CnErroredPortTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    pMultiData->i4_SLongValue = pMultiIndex->pIndex[2].i4_SLongValue;

    return SNMP_SUCCESS;

}

INT4
GetNextIndexIeee8021CnCompntPriTable (tSnmpIndex * pFirstMultiIndex,
                                      tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexIeee8021CnCompntPriTable
            (&(pNextMultiIndex->pIndex[0].u4_ULongValue),
             &(pNextMultiIndex->pIndex[1].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexIeee8021CnCompntPriTable
            (pFirstMultiIndex->pIndex[0].u4_ULongValue,
             &(pNextMultiIndex->pIndex[0].u4_ULongValue),
             pFirstMultiIndex->pIndex[1].u4_ULongValue,
             &(pNextMultiIndex->pIndex[1].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
Ieee8021CnComPriDefModeChoiceGet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIeee8021CnCompntPriTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIeee8021CnComPriDefModeChoice
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Ieee8021CnComPriAlternatePriorityGet (tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIeee8021CnCompntPriTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIeee8021CnComPriAlternatePriority
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
Ieee8021CnComPriAutoAltPriGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIeee8021CnCompntPriTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIeee8021CnComPriAutoAltPri
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
Ieee8021CnComPriAdminDefenseModeGet (tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIeee8021CnCompntPriTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIeee8021CnComPriAdminDefenseMode
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Ieee8021CnComPriCreationGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIeee8021CnCompntPriTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIeee8021CnComPriCreation
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Ieee8021CnComPriLldpInstanceChoiceGet (tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIeee8021CnCompntPriTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIeee8021CnComPriLldpInstanceChoice
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Ieee8021CnComPriLldpInstanceSelectorGet (tSnmpIndex * pMultiIndex,
                                         tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIeee8021CnCompntPriTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIeee8021CnComPriLldpInstanceSelector
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
Ieee8021CnComPriRowStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIeee8021CnCompntPriTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIeee8021CnComPriRowStatus
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Ieee8021CnComPriDefModeChoiceSet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    return (nmhSetIeee8021CnComPriDefModeChoice
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue, pMultiData->i4_SLongValue));

}

INT4
Ieee8021CnComPriAlternatePrioritySet (tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    return (nmhSetIeee8021CnComPriAlternatePriority
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue, pMultiData->u4_ULongValue));

}

INT4
Ieee8021CnComPriAdminDefenseModeSet (tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    return (nmhSetIeee8021CnComPriAdminDefenseMode
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue, pMultiData->i4_SLongValue));

}

INT4
Ieee8021CnComPriCreationSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetIeee8021CnComPriCreation
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue, pMultiData->i4_SLongValue));

}

INT4
Ieee8021CnComPriLldpInstanceChoiceSet (tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    return (nmhSetIeee8021CnComPriLldpInstanceChoice
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue, pMultiData->i4_SLongValue));

}

INT4
Ieee8021CnComPriLldpInstanceSelectorSet (tSnmpIndex * pMultiIndex,
                                         tRetVal * pMultiData)
{
    return (nmhSetIeee8021CnComPriLldpInstanceSelector
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue, pMultiData->u4_ULongValue));

}

INT4
Ieee8021CnComPriRowStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetIeee8021CnComPriRowStatus
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue, pMultiData->i4_SLongValue));

}

INT4
Ieee8021CnComPriDefModeChoiceTest (UINT4 *pu4Error,
                                   tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    return (nmhTestv2Ieee8021CnComPriDefModeChoice (pu4Error,
                                                    pMultiIndex->pIndex[0].
                                                    u4_ULongValue,
                                                    pMultiIndex->pIndex[1].
                                                    u4_ULongValue,
                                                    pMultiData->i4_SLongValue));

}

INT4
Ieee8021CnComPriAlternatePriorityTest (UINT4 *pu4Error,
                                       tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    return (nmhTestv2Ieee8021CnComPriAlternatePriority (pu4Error,
                                                        pMultiIndex->pIndex[0].
                                                        u4_ULongValue,
                                                        pMultiIndex->pIndex[1].
                                                        u4_ULongValue,
                                                        pMultiData->
                                                        u4_ULongValue));

}

INT4
Ieee8021CnComPriAdminDefenseModeTest (UINT4 *pu4Error,
                                      tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    return (nmhTestv2Ieee8021CnComPriAdminDefenseMode (pu4Error,
                                                       pMultiIndex->pIndex[0].
                                                       u4_ULongValue,
                                                       pMultiIndex->pIndex[1].
                                                       u4_ULongValue,
                                                       pMultiData->
                                                       i4_SLongValue));

}

INT4
Ieee8021CnComPriCreationTest (UINT4 *pu4Error,
                              tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhTestv2Ieee8021CnComPriCreation (pu4Error,
                                               pMultiIndex->pIndex[0].
                                               u4_ULongValue,
                                               pMultiIndex->pIndex[1].
                                               u4_ULongValue,
                                               pMultiData->i4_SLongValue));

}

INT4
Ieee8021CnComPriLldpInstanceChoiceTest (UINT4 *pu4Error,
                                        tSnmpIndex * pMultiIndex,
                                        tRetVal * pMultiData)
{
    return (nmhTestv2Ieee8021CnComPriLldpInstanceChoice (pu4Error,
                                                         pMultiIndex->pIndex[0].
                                                         u4_ULongValue,
                                                         pMultiIndex->pIndex[1].
                                                         u4_ULongValue,
                                                         pMultiData->
                                                         i4_SLongValue));

}

INT4
Ieee8021CnComPriLldpInstanceSelectorTest (UINT4 *pu4Error,
                                          tSnmpIndex * pMultiIndex,
                                          tRetVal * pMultiData)
{
    return (nmhTestv2Ieee8021CnComPriLldpInstanceSelector (pu4Error,
                                                           pMultiIndex->
                                                           pIndex[0].
                                                           u4_ULongValue,
                                                           pMultiIndex->
                                                           pIndex[1].
                                                           u4_ULongValue,
                                                           pMultiData->
                                                           u4_ULongValue));

}

INT4
Ieee8021CnComPriRowStatusTest (UINT4 *pu4Error,
                               tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhTestv2Ieee8021CnComPriRowStatus (pu4Error,
                                                pMultiIndex->pIndex[0].
                                                u4_ULongValue,
                                                pMultiIndex->pIndex[1].
                                                u4_ULongValue,
                                                pMultiData->i4_SLongValue));

}

INT4
Ieee8021CnCompntPriTableDep (UINT4 *pu4Error,
                             tSnmpIndexList * pSnmpIndexList,
                             tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2Ieee8021CnCompntPriTable (pu4Error, pSnmpIndexList,
                                              pSnmpvarbinds));
}

INT4
GetNextIndexIeee8021CnPortPriTable (tSnmpIndex * pFirstMultiIndex,
                                    tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexIeee8021CnPortPriTable
            (&(pNextMultiIndex->pIndex[0].u4_ULongValue),
             &(pNextMultiIndex->pIndex[1].u4_ULongValue),
             &(pNextMultiIndex->pIndex[2].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexIeee8021CnPortPriTable
            (pFirstMultiIndex->pIndex[0].u4_ULongValue,
             &(pNextMultiIndex->pIndex[0].u4_ULongValue),
             pFirstMultiIndex->pIndex[1].u4_ULongValue,
             &(pNextMultiIndex->pIndex[1].u4_ULongValue),
             pFirstMultiIndex->pIndex[2].i4_SLongValue,
             &(pNextMultiIndex->pIndex[2].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
Ieee8021CnPortPriDefModeChoiceGet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIeee8021CnPortPriTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIeee8021CnPortPriDefModeChoice
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             pMultiIndex->pIndex[2].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Ieee8021CnPortPriAdminDefenseModeGet (tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIeee8021CnPortPriTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIeee8021CnPortPriAdminDefenseMode
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             pMultiIndex->pIndex[2].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Ieee8021CnPortPriAutoDefenseModeGet (tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIeee8021CnPortPriTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIeee8021CnPortPriAutoDefenseMode
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             pMultiIndex->pIndex[2].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Ieee8021CnPortPriLldpInstanceChoiceGet (tSnmpIndex * pMultiIndex,
                                        tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIeee8021CnPortPriTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIeee8021CnPortPriLldpInstanceChoice
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             pMultiIndex->pIndex[2].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Ieee8021CnPortPriLldpInstanceSelectorGet (tSnmpIndex * pMultiIndex,
                                          tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIeee8021CnPortPriTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIeee8021CnPortPriLldpInstanceSelector
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             pMultiIndex->pIndex[2].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
Ieee8021CnPortPriAlternatePriorityGet (tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIeee8021CnPortPriTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIeee8021CnPortPriAlternatePriority
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             pMultiIndex->pIndex[2].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
Ieee8021CnPortPriDefModeChoiceSet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    return (nmhSetIeee8021CnPortPriDefModeChoice
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             pMultiIndex->pIndex[2].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
Ieee8021CnPortPriAdminDefenseModeSet (tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    return (nmhSetIeee8021CnPortPriAdminDefenseMode
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             pMultiIndex->pIndex[2].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
Ieee8021CnPortPriLldpInstanceChoiceSet (tSnmpIndex * pMultiIndex,
                                        tRetVal * pMultiData)
{
    return (nmhSetIeee8021CnPortPriLldpInstanceChoice
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             pMultiIndex->pIndex[2].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
Ieee8021CnPortPriLldpInstanceSelectorSet (tSnmpIndex * pMultiIndex,
                                          tRetVal * pMultiData)
{
    return (nmhSetIeee8021CnPortPriLldpInstanceSelector
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             pMultiIndex->pIndex[2].i4_SLongValue, pMultiData->u4_ULongValue));

}

INT4
Ieee8021CnPortPriAlternatePrioritySet (tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    return (nmhSetIeee8021CnPortPriAlternatePriority
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             pMultiIndex->pIndex[2].i4_SLongValue, pMultiData->u4_ULongValue));

}

INT4
Ieee8021CnPortPriDefModeChoiceTest (UINT4 *pu4Error,
                                    tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    return (nmhTestv2Ieee8021CnPortPriDefModeChoice (pu4Error,
                                                     pMultiIndex->pIndex[0].
                                                     u4_ULongValue,
                                                     pMultiIndex->pIndex[1].
                                                     u4_ULongValue,
                                                     pMultiIndex->pIndex[2].
                                                     i4_SLongValue,
                                                     pMultiData->
                                                     i4_SLongValue));

}

INT4
Ieee8021CnPortPriAdminDefenseModeTest (UINT4 *pu4Error,
                                       tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    return (nmhTestv2Ieee8021CnPortPriAdminDefenseMode (pu4Error,
                                                        pMultiIndex->pIndex[0].
                                                        u4_ULongValue,
                                                        pMultiIndex->pIndex[1].
                                                        u4_ULongValue,
                                                        pMultiIndex->pIndex[2].
                                                        i4_SLongValue,
                                                        pMultiData->
                                                        i4_SLongValue));

}

INT4
Ieee8021CnPortPriLldpInstanceChoiceTest (UINT4 *pu4Error,
                                         tSnmpIndex * pMultiIndex,
                                         tRetVal * pMultiData)
{
    return (nmhTestv2Ieee8021CnPortPriLldpInstanceChoice (pu4Error,
                                                          pMultiIndex->
                                                          pIndex[0].
                                                          u4_ULongValue,
                                                          pMultiIndex->
                                                          pIndex[1].
                                                          u4_ULongValue,
                                                          pMultiIndex->
                                                          pIndex[2].
                                                          i4_SLongValue,
                                                          pMultiData->
                                                          i4_SLongValue));

}

INT4
Ieee8021CnPortPriLldpInstanceSelectorTest (UINT4 *pu4Error,
                                           tSnmpIndex * pMultiIndex,
                                           tRetVal * pMultiData)
{
    return (nmhTestv2Ieee8021CnPortPriLldpInstanceSelector (pu4Error,
                                                            pMultiIndex->
                                                            pIndex[0].
                                                            u4_ULongValue,
                                                            pMultiIndex->
                                                            pIndex[1].
                                                            u4_ULongValue,
                                                            pMultiIndex->
                                                            pIndex[2].
                                                            i4_SLongValue,
                                                            pMultiData->
                                                            u4_ULongValue));

}

INT4
Ieee8021CnPortPriAlternatePriorityTest (UINT4 *pu4Error,
                                        tSnmpIndex * pMultiIndex,
                                        tRetVal * pMultiData)
{
    return (nmhTestv2Ieee8021CnPortPriAlternatePriority (pu4Error,
                                                         pMultiIndex->pIndex[0].
                                                         u4_ULongValue,
                                                         pMultiIndex->pIndex[1].
                                                         u4_ULongValue,
                                                         pMultiIndex->pIndex[2].
                                                         i4_SLongValue,
                                                         pMultiData->
                                                         u4_ULongValue));

}

INT4
Ieee8021CnPortPriTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                           tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2Ieee8021CnPortPriTable (pu4Error, pSnmpIndexList,
                                            pSnmpvarbinds));
}

INT4
GetNextIndexIeee8021CnCpTable (tSnmpIndex * pFirstMultiIndex,
                               tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexIeee8021CnCpTable
            (&(pNextMultiIndex->pIndex[0].u4_ULongValue),
             &(pNextMultiIndex->pIndex[1].i4_SLongValue),
             &(pNextMultiIndex->pIndex[2].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexIeee8021CnCpTable
            (pFirstMultiIndex->pIndex[0].u4_ULongValue,
             &(pNextMultiIndex->pIndex[0].u4_ULongValue),
             pFirstMultiIndex->pIndex[1].i4_SLongValue,
             &(pNextMultiIndex->pIndex[1].i4_SLongValue),
             pFirstMultiIndex->pIndex[2].u4_ULongValue,
             &(pNextMultiIndex->pIndex[2].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
Ieee8021CnCpPriorityGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIeee8021CnCpTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIeee8021CnCpPriority (pMultiIndex->pIndex[0].u4_ULongValue,
                                        pMultiIndex->pIndex[1].i4_SLongValue,
                                        pMultiIndex->pIndex[2].u4_ULongValue,
                                        &(pMultiData->u4_ULongValue)));

}

INT4
Ieee8021CnCpMacAddressGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIeee8021CnCpTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    pMultiData->pOctetStrValue->i4_Length = 6;
    return (nmhGetIeee8021CnCpMacAddress (pMultiIndex->pIndex[0].u4_ULongValue,
                                          pMultiIndex->pIndex[1].i4_SLongValue,
                                          pMultiIndex->pIndex[2].u4_ULongValue,
                                          (tMacAddr *) pMultiData->
                                          pOctetStrValue->pu1_OctetList));

}

INT4
Ieee8021CnCpIdentifierGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIeee8021CnCpTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIeee8021CnCpIdentifier (pMultiIndex->pIndex[0].u4_ULongValue,
                                          pMultiIndex->pIndex[1].i4_SLongValue,
                                          pMultiIndex->pIndex[2].u4_ULongValue,
                                          pMultiData->pOctetStrValue));

}

INT4
Ieee8021CnCpQueueSizeSetPointGet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIeee8021CnCpTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIeee8021CnCpQueueSizeSetPoint
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             pMultiIndex->pIndex[2].u4_ULongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
Ieee8021CnCpFeedbackWeightGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIeee8021CnCpTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIeee8021CnCpFeedbackWeight
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             pMultiIndex->pIndex[2].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Ieee8021CnCpMinSampleBaseGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIeee8021CnCpTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIeee8021CnCpMinSampleBase
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             pMultiIndex->pIndex[2].u4_ULongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
Ieee8021CnCpDiscardedFramesGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIeee8021CnCpTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIeee8021CnCpDiscardedFrames
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             pMultiIndex->pIndex[2].u4_ULongValue,
             &(pMultiData->u8_Counter64Value)));

}

INT4
Ieee8021CnCpTransmittedFramesGet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIeee8021CnCpTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIeee8021CnCpTransmittedFrames
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             pMultiIndex->pIndex[2].u4_ULongValue,
             &(pMultiData->u8_Counter64Value)));

}

INT4
Ieee8021CnCpTransmittedCnmsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIeee8021CnCpTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIeee8021CnCpTransmittedCnms
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             pMultiIndex->pIndex[2].u4_ULongValue,
             &(pMultiData->u8_Counter64Value)));

}

INT4
Ieee8021CnCpMinHeaderOctetsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIeee8021CnCpTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIeee8021CnCpMinHeaderOctets
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             pMultiIndex->pIndex[2].u4_ULongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
Ieee8021CnCpQueueSizeSetPointSet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    return (nmhSetIeee8021CnCpQueueSizeSetPoint
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             pMultiIndex->pIndex[2].u4_ULongValue, pMultiData->u4_ULongValue));

}

INT4
Ieee8021CnCpFeedbackWeightSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetIeee8021CnCpFeedbackWeight
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             pMultiIndex->pIndex[2].u4_ULongValue, pMultiData->i4_SLongValue));

}

INT4
Ieee8021CnCpMinSampleBaseSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetIeee8021CnCpMinSampleBase
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             pMultiIndex->pIndex[2].u4_ULongValue, pMultiData->u4_ULongValue));

}

INT4
Ieee8021CnCpMinHeaderOctetsSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetIeee8021CnCpMinHeaderOctets
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             pMultiIndex->pIndex[2].u4_ULongValue, pMultiData->u4_ULongValue));

}

INT4
Ieee8021CnCpQueueSizeSetPointTest (UINT4 *pu4Error,
                                   tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    return (nmhTestv2Ieee8021CnCpQueueSizeSetPoint (pu4Error,
                                                    pMultiIndex->pIndex[0].
                                                    u4_ULongValue,
                                                    pMultiIndex->pIndex[1].
                                                    i4_SLongValue,
                                                    pMultiIndex->pIndex[2].
                                                    u4_ULongValue,
                                                    pMultiData->u4_ULongValue));

}

INT4
Ieee8021CnCpFeedbackWeightTest (UINT4 *pu4Error,
                                tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhTestv2Ieee8021CnCpFeedbackWeight (pu4Error,
                                                 pMultiIndex->pIndex[0].
                                                 u4_ULongValue,
                                                 pMultiIndex->pIndex[1].
                                                 i4_SLongValue,
                                                 pMultiIndex->pIndex[2].
                                                 u4_ULongValue,
                                                 pMultiData->i4_SLongValue));

}

INT4
Ieee8021CnCpMinSampleBaseTest (UINT4 *pu4Error,
                               tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhTestv2Ieee8021CnCpMinSampleBase (pu4Error,
                                                pMultiIndex->pIndex[0].
                                                u4_ULongValue,
                                                pMultiIndex->pIndex[1].
                                                i4_SLongValue,
                                                pMultiIndex->pIndex[2].
                                                u4_ULongValue,
                                                pMultiData->u4_ULongValue));

}

INT4
Ieee8021CnCpMinHeaderOctetsTest (UINT4 *pu4Error,
                                 tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhTestv2Ieee8021CnCpMinHeaderOctets (pu4Error,
                                                  pMultiIndex->pIndex[0].
                                                  u4_ULongValue,
                                                  pMultiIndex->pIndex[1].
                                                  i4_SLongValue,
                                                  pMultiIndex->pIndex[2].
                                                  u4_ULongValue,
                                                  pMultiData->u4_ULongValue));

}

INT4
Ieee8021CnCpTableDep (UINT4 *pu4Error,
                      tSnmpIndexList * pSnmpIndexList,
                      tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2Ieee8021CnCpTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexIeee8021CnCpidToInterfaceTable (tSnmpIndex * pFirstMultiIndex,
                                            tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexIeee8021CnCpidToInterfaceTable
            (pNextMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexIeee8021CnCpidToInterfaceTable
            (pFirstMultiIndex->pIndex[0].pOctetStrValue,
             pNextMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
Ieee8021CnCpidToIfComponentIdGet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIeee8021CnCpidToInterfaceTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIeee8021CnCpidToIfComponentId
            (pMultiIndex->pIndex[0].pOctetStrValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
Ieee8021CnCpidToIfIfIndexGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIeee8021CnCpidToInterfaceTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIeee8021CnCpidToIfIfIndex
            (pMultiIndex->pIndex[0].pOctetStrValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Ieee8021CnCpidToIfCpIndexGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIeee8021CnCpidToInterfaceTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIeee8021CnCpidToIfCpIndex
            (pMultiIndex->pIndex[0].pOctetStrValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
GetNextIndexIeee8021CnRpPortPriTable (tSnmpIndex * pFirstMultiIndex,
                                      tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexIeee8021CnRpPortPriTable
            (&(pNextMultiIndex->pIndex[0].u4_ULongValue),
             &(pNextMultiIndex->pIndex[1].u4_ULongValue),
             &(pNextMultiIndex->pIndex[2].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexIeee8021CnRpPortPriTable
            (pFirstMultiIndex->pIndex[0].u4_ULongValue,
             &(pNextMultiIndex->pIndex[0].u4_ULongValue),
             pFirstMultiIndex->pIndex[1].u4_ULongValue,
             &(pNextMultiIndex->pIndex[1].u4_ULongValue),
             pFirstMultiIndex->pIndex[2].i4_SLongValue,
             &(pNextMultiIndex->pIndex[2].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
Ieee8021CnRpPortPriMaxRpsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIeee8021CnRpPortPriTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIeee8021CnRpPortPriMaxRps
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             pMultiIndex->pIndex[2].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
Ieee8021CnRpPortPriCreatedRpsGet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIeee8021CnRpPortPriTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIeee8021CnRpPortPriCreatedRps
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             pMultiIndex->pIndex[2].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
Ieee8021CnRpPortPriCentisecondsGet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIeee8021CnRpPortPriTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIeee8021CnRpPortPriCentiseconds
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             pMultiIndex->pIndex[2].i4_SLongValue,
             &(pMultiData->u8_Counter64Value)));

}

INT4
Ieee8021CnRpPortPriMaxRpsSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetIeee8021CnRpPortPriMaxRps
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             pMultiIndex->pIndex[2].i4_SLongValue, pMultiData->u4_ULongValue));

}

INT4
Ieee8021CnRpPortPriMaxRpsTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                               tRetVal * pMultiData)
{
    return (nmhTestv2Ieee8021CnRpPortPriMaxRps (pu4Error,
                                                pMultiIndex->pIndex[0].
                                                u4_ULongValue,
                                                pMultiIndex->pIndex[1].
                                                u4_ULongValue,
                                                pMultiIndex->pIndex[2].
                                                i4_SLongValue,
                                                pMultiData->u4_ULongValue));

}

INT4
Ieee8021CnRpPortPriTableDep (UINT4 *pu4Error,
                             tSnmpIndexList * pSnmpIndexList,
                             tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2Ieee8021CnRpPortPriTable (pu4Error, pSnmpIndexList,
                                              pSnmpvarbinds));
}

INT4
GetNextIndexIeee8021CnRpGroupTable (tSnmpIndex * pFirstMultiIndex,
                                    tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexIeee8021CnRpGroupTable
            (&(pNextMultiIndex->pIndex[0].u4_ULongValue),
             &(pNextMultiIndex->pIndex[1].u4_ULongValue),
             &(pNextMultiIndex->pIndex[2].i4_SLongValue),
             &(pNextMultiIndex->pIndex[3].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexIeee8021CnRpGroupTable
            (pFirstMultiIndex->pIndex[0].u4_ULongValue,
             &(pNextMultiIndex->pIndex[0].u4_ULongValue),
             pFirstMultiIndex->pIndex[1].u4_ULongValue,
             &(pNextMultiIndex->pIndex[1].u4_ULongValue),
             pFirstMultiIndex->pIndex[2].i4_SLongValue,
             &(pNextMultiIndex->pIndex[2].i4_SLongValue),
             pFirstMultiIndex->pIndex[3].u4_ULongValue,
             &(pNextMultiIndex->pIndex[3].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
Ieee8021CnRpgEnableGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIeee8021CnRpGroupTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].i4_SLongValue,
         pMultiIndex->pIndex[3].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIeee8021CnRpgEnable (pMultiIndex->pIndex[0].u4_ULongValue,
                                       pMultiIndex->pIndex[1].u4_ULongValue,
                                       pMultiIndex->pIndex[2].i4_SLongValue,
                                       pMultiIndex->pIndex[3].u4_ULongValue,
                                       &(pMultiData->i4_SLongValue)));

}

INT4
Ieee8021CnRpgTimeResetGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIeee8021CnRpGroupTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].i4_SLongValue,
         pMultiIndex->pIndex[3].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIeee8021CnRpgTimeReset (pMultiIndex->pIndex[0].u4_ULongValue,
                                          pMultiIndex->pIndex[1].u4_ULongValue,
                                          pMultiIndex->pIndex[2].i4_SLongValue,
                                          pMultiIndex->pIndex[3].u4_ULongValue,
                                          &(pMultiData->i4_SLongValue)));

}

INT4
Ieee8021CnRpgByteResetGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIeee8021CnRpGroupTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].i4_SLongValue,
         pMultiIndex->pIndex[3].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIeee8021CnRpgByteReset (pMultiIndex->pIndex[0].u4_ULongValue,
                                          pMultiIndex->pIndex[1].u4_ULongValue,
                                          pMultiIndex->pIndex[2].i4_SLongValue,
                                          pMultiIndex->pIndex[3].u4_ULongValue,
                                          &(pMultiData->u4_ULongValue)));

}

INT4
Ieee8021CnRpgThresholdGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIeee8021CnRpGroupTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].i4_SLongValue,
         pMultiIndex->pIndex[3].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIeee8021CnRpgThreshold (pMultiIndex->pIndex[0].u4_ULongValue,
                                          pMultiIndex->pIndex[1].u4_ULongValue,
                                          pMultiIndex->pIndex[2].i4_SLongValue,
                                          pMultiIndex->pIndex[3].u4_ULongValue,
                                          &(pMultiData->u4_ULongValue)));

}

INT4
Ieee8021CnRpgMaxRateGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIeee8021CnRpGroupTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].i4_SLongValue,
         pMultiIndex->pIndex[3].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIeee8021CnRpgMaxRate (pMultiIndex->pIndex[0].u4_ULongValue,
                                        pMultiIndex->pIndex[1].u4_ULongValue,
                                        pMultiIndex->pIndex[2].i4_SLongValue,
                                        pMultiIndex->pIndex[3].u4_ULongValue,
                                        &(pMultiData->u4_ULongValue)));

}

INT4
Ieee8021CnRpgAiRateGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIeee8021CnRpGroupTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].i4_SLongValue,
         pMultiIndex->pIndex[3].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIeee8021CnRpgAiRate (pMultiIndex->pIndex[0].u4_ULongValue,
                                       pMultiIndex->pIndex[1].u4_ULongValue,
                                       pMultiIndex->pIndex[2].i4_SLongValue,
                                       pMultiIndex->pIndex[3].u4_ULongValue,
                                       &(pMultiData->u4_ULongValue)));

}

INT4
Ieee8021CnRpgHaiRateGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIeee8021CnRpGroupTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].i4_SLongValue,
         pMultiIndex->pIndex[3].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIeee8021CnRpgHaiRate (pMultiIndex->pIndex[0].u4_ULongValue,
                                        pMultiIndex->pIndex[1].u4_ULongValue,
                                        pMultiIndex->pIndex[2].i4_SLongValue,
                                        pMultiIndex->pIndex[3].u4_ULongValue,
                                        &(pMultiData->u4_ULongValue)));

}

INT4
Ieee8021CnRpgGdGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIeee8021CnRpGroupTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].i4_SLongValue,
         pMultiIndex->pIndex[3].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIeee8021CnRpgGd (pMultiIndex->pIndex[0].u4_ULongValue,
                                   pMultiIndex->pIndex[1].u4_ULongValue,
                                   pMultiIndex->pIndex[2].i4_SLongValue,
                                   pMultiIndex->pIndex[3].u4_ULongValue,
                                   &(pMultiData->i4_SLongValue)));

}

INT4
Ieee8021CnRpgMinDecFacGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIeee8021CnRpGroupTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].i4_SLongValue,
         pMultiIndex->pIndex[3].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIeee8021CnRpgMinDecFac (pMultiIndex->pIndex[0].u4_ULongValue,
                                          pMultiIndex->pIndex[1].u4_ULongValue,
                                          pMultiIndex->pIndex[2].i4_SLongValue,
                                          pMultiIndex->pIndex[3].u4_ULongValue,
                                          &(pMultiData->u4_ULongValue)));

}

INT4
Ieee8021CnRpgMinRateGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIeee8021CnRpGroupTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].i4_SLongValue,
         pMultiIndex->pIndex[3].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIeee8021CnRpgMinRate (pMultiIndex->pIndex[0].u4_ULongValue,
                                        pMultiIndex->pIndex[1].u4_ULongValue,
                                        pMultiIndex->pIndex[2].i4_SLongValue,
                                        pMultiIndex->pIndex[3].u4_ULongValue,
                                        &(pMultiData->u4_ULongValue)));

}

INT4
Ieee8021CnRpgEnableSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetIeee8021CnRpgEnable (pMultiIndex->pIndex[0].u4_ULongValue,
                                       pMultiIndex->pIndex[1].u4_ULongValue,
                                       pMultiIndex->pIndex[2].i4_SLongValue,
                                       pMultiIndex->pIndex[3].u4_ULongValue,
                                       pMultiData->i4_SLongValue));

}

INT4
Ieee8021CnRpgTimeResetSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetIeee8021CnRpgTimeReset (pMultiIndex->pIndex[0].u4_ULongValue,
                                          pMultiIndex->pIndex[1].u4_ULongValue,
                                          pMultiIndex->pIndex[2].i4_SLongValue,
                                          pMultiIndex->pIndex[3].u4_ULongValue,
                                          pMultiData->i4_SLongValue));

}

INT4
Ieee8021CnRpgByteResetSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetIeee8021CnRpgByteReset (pMultiIndex->pIndex[0].u4_ULongValue,
                                          pMultiIndex->pIndex[1].u4_ULongValue,
                                          pMultiIndex->pIndex[2].i4_SLongValue,
                                          pMultiIndex->pIndex[3].u4_ULongValue,
                                          pMultiData->u4_ULongValue));

}

INT4
Ieee8021CnRpgThresholdSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetIeee8021CnRpgThreshold (pMultiIndex->pIndex[0].u4_ULongValue,
                                          pMultiIndex->pIndex[1].u4_ULongValue,
                                          pMultiIndex->pIndex[2].i4_SLongValue,
                                          pMultiIndex->pIndex[3].u4_ULongValue,
                                          pMultiData->u4_ULongValue));

}

INT4
Ieee8021CnRpgMaxRateSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetIeee8021CnRpgMaxRate (pMultiIndex->pIndex[0].u4_ULongValue,
                                        pMultiIndex->pIndex[1].u4_ULongValue,
                                        pMultiIndex->pIndex[2].i4_SLongValue,
                                        pMultiIndex->pIndex[3].u4_ULongValue,
                                        pMultiData->u4_ULongValue));

}

INT4
Ieee8021CnRpgAiRateSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetIeee8021CnRpgAiRate (pMultiIndex->pIndex[0].u4_ULongValue,
                                       pMultiIndex->pIndex[1].u4_ULongValue,
                                       pMultiIndex->pIndex[2].i4_SLongValue,
                                       pMultiIndex->pIndex[3].u4_ULongValue,
                                       pMultiData->u4_ULongValue));

}

INT4
Ieee8021CnRpgHaiRateSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetIeee8021CnRpgHaiRate (pMultiIndex->pIndex[0].u4_ULongValue,
                                        pMultiIndex->pIndex[1].u4_ULongValue,
                                        pMultiIndex->pIndex[2].i4_SLongValue,
                                        pMultiIndex->pIndex[3].u4_ULongValue,
                                        pMultiData->u4_ULongValue));

}

INT4
Ieee8021CnRpgGdSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetIeee8021CnRpgGd (pMultiIndex->pIndex[0].u4_ULongValue,
                                   pMultiIndex->pIndex[1].u4_ULongValue,
                                   pMultiIndex->pIndex[2].i4_SLongValue,
                                   pMultiIndex->pIndex[3].u4_ULongValue,
                                   pMultiData->i4_SLongValue));

}

INT4
Ieee8021CnRpgMinDecFacSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetIeee8021CnRpgMinDecFac (pMultiIndex->pIndex[0].u4_ULongValue,
                                          pMultiIndex->pIndex[1].u4_ULongValue,
                                          pMultiIndex->pIndex[2].i4_SLongValue,
                                          pMultiIndex->pIndex[3].u4_ULongValue,
                                          pMultiData->u4_ULongValue));

}

INT4
Ieee8021CnRpgMinRateSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetIeee8021CnRpgMinRate (pMultiIndex->pIndex[0].u4_ULongValue,
                                        pMultiIndex->pIndex[1].u4_ULongValue,
                                        pMultiIndex->pIndex[2].i4_SLongValue,
                                        pMultiIndex->pIndex[3].u4_ULongValue,
                                        pMultiData->u4_ULongValue));

}

INT4
Ieee8021CnRpgEnableTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                         tRetVal * pMultiData)
{
    return (nmhTestv2Ieee8021CnRpgEnable (pu4Error,
                                          pMultiIndex->pIndex[0].u4_ULongValue,
                                          pMultiIndex->pIndex[1].u4_ULongValue,
                                          pMultiIndex->pIndex[2].i4_SLongValue,
                                          pMultiIndex->pIndex[3].u4_ULongValue,
                                          pMultiData->i4_SLongValue));

}

INT4
Ieee8021CnRpgTimeResetTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                            tRetVal * pMultiData)
{
    return (nmhTestv2Ieee8021CnRpgTimeReset (pu4Error,
                                             pMultiIndex->pIndex[0].
                                             u4_ULongValue,
                                             pMultiIndex->pIndex[1].
                                             u4_ULongValue,
                                             pMultiIndex->pIndex[2].
                                             i4_SLongValue,
                                             pMultiIndex->pIndex[3].
                                             u4_ULongValue,
                                             pMultiData->i4_SLongValue));

}

INT4
Ieee8021CnRpgByteResetTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                            tRetVal * pMultiData)
{
    return (nmhTestv2Ieee8021CnRpgByteReset (pu4Error,
                                             pMultiIndex->pIndex[0].
                                             u4_ULongValue,
                                             pMultiIndex->pIndex[1].
                                             u4_ULongValue,
                                             pMultiIndex->pIndex[2].
                                             i4_SLongValue,
                                             pMultiIndex->pIndex[3].
                                             u4_ULongValue,
                                             pMultiData->u4_ULongValue));

}

INT4
Ieee8021CnRpgThresholdTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                            tRetVal * pMultiData)
{
    return (nmhTestv2Ieee8021CnRpgThreshold (pu4Error,
                                             pMultiIndex->pIndex[0].
                                             u4_ULongValue,
                                             pMultiIndex->pIndex[1].
                                             u4_ULongValue,
                                             pMultiIndex->pIndex[2].
                                             i4_SLongValue,
                                             pMultiIndex->pIndex[3].
                                             u4_ULongValue,
                                             pMultiData->u4_ULongValue));

}

INT4
Ieee8021CnRpgMaxRateTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                          tRetVal * pMultiData)
{
    return (nmhTestv2Ieee8021CnRpgMaxRate (pu4Error,
                                           pMultiIndex->pIndex[0].u4_ULongValue,
                                           pMultiIndex->pIndex[1].u4_ULongValue,
                                           pMultiIndex->pIndex[2].i4_SLongValue,
                                           pMultiIndex->pIndex[3].u4_ULongValue,
                                           pMultiData->u4_ULongValue));

}

INT4
Ieee8021CnRpgAiRateTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                         tRetVal * pMultiData)
{
    return (nmhTestv2Ieee8021CnRpgAiRate (pu4Error,
                                          pMultiIndex->pIndex[0].u4_ULongValue,
                                          pMultiIndex->pIndex[1].u4_ULongValue,
                                          pMultiIndex->pIndex[2].i4_SLongValue,
                                          pMultiIndex->pIndex[3].u4_ULongValue,
                                          pMultiData->u4_ULongValue));

}

INT4
Ieee8021CnRpgHaiRateTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                          tRetVal * pMultiData)
{
    return (nmhTestv2Ieee8021CnRpgHaiRate (pu4Error,
                                           pMultiIndex->pIndex[0].u4_ULongValue,
                                           pMultiIndex->pIndex[1].u4_ULongValue,
                                           pMultiIndex->pIndex[2].i4_SLongValue,
                                           pMultiIndex->pIndex[3].u4_ULongValue,
                                           pMultiData->u4_ULongValue));

}

INT4
Ieee8021CnRpgGdTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                     tRetVal * pMultiData)
{
    return (nmhTestv2Ieee8021CnRpgGd (pu4Error,
                                      pMultiIndex->pIndex[0].u4_ULongValue,
                                      pMultiIndex->pIndex[1].u4_ULongValue,
                                      pMultiIndex->pIndex[2].i4_SLongValue,
                                      pMultiIndex->pIndex[3].u4_ULongValue,
                                      pMultiData->i4_SLongValue));

}

INT4
Ieee8021CnRpgMinDecFacTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                            tRetVal * pMultiData)
{
    return (nmhTestv2Ieee8021CnRpgMinDecFac (pu4Error,
                                             pMultiIndex->pIndex[0].
                                             u4_ULongValue,
                                             pMultiIndex->pIndex[1].
                                             u4_ULongValue,
                                             pMultiIndex->pIndex[2].
                                             i4_SLongValue,
                                             pMultiIndex->pIndex[3].
                                             u4_ULongValue,
                                             pMultiData->u4_ULongValue));

}

INT4
Ieee8021CnRpgMinRateTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                          tRetVal * pMultiData)
{
    return (nmhTestv2Ieee8021CnRpgMinRate (pu4Error,
                                           pMultiIndex->pIndex[0].u4_ULongValue,
                                           pMultiIndex->pIndex[1].u4_ULongValue,
                                           pMultiIndex->pIndex[2].i4_SLongValue,
                                           pMultiIndex->pIndex[3].u4_ULongValue,
                                           pMultiData->u4_ULongValue));

}

INT4
Ieee8021CnRpGroupTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                           tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2Ieee8021CnRpGroupTable (pu4Error, pSnmpIndexList,
                                            pSnmpvarbinds));
}
