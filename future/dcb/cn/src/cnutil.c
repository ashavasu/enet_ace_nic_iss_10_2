/********************************************************************
 * Copyright (C) 2010 Aricent Inc . All Rights Reserved
 *
 * $Id: cnutil.c,v 1.10 2014/02/25 12:43:58 siva Exp $
 *
 * Description: This file contains the utility procedures used by CN
 *              module.
 *********************************************************************/
#include "cninc.h"

/****************************************************************************
*
*    FUNCTION NAME    : CnLock
*
*    DESCRIPTION      : Function to take the mutual exclusion protocol
*                       semaphore.
*
*    INPUT            : None
*
*    OUTPUT           : None
*
*    RETURNS          : SNMP_SUCCESS/SNMP_FAILURE
*
****************************************************************************/
PUBLIC INT4
CnLock (VOID)
{
    if (OsixSemTake (gCnMasterTblInfo.SemId) == OSIX_FAILURE)
    {
        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);
}

/****************************************************************************
*
*    FUNCTION NAME    : CnUnLock
*
*    DESCRIPTION      : Function to release the mutual exclusion protocol
*                       semaphore.
*
*    INPUT            : None
*
*    OUTPUT           : None
*
*    RETURNS          : None
****************************************************************************/
PUBLIC INT4
CnUnLock (VOID)
{
    OsixSemGive (gCnMasterTblInfo.SemId);
    return SNMP_SUCCESS;
}

/****************************************************************************
*
*    FUNCTION NAME    : CnUtilConstructPreFormTlv
*
*    DESCRIPTION      : Function to construct the first six bytes of Cn TLV
*
*    INPUT            : None
*
*    OUTPUT           : None
*
*    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
*
****************************************************************************/
PUBLIC VOID
CnUtilConstructPreFormTlv (VOID)
{
    UINT2               u2TlvHeader = CN_ZERO;
    UINT1              *pu1Temp = NULL;

    MEMSET (gau1CnPreFormedTlv, CN_ZERO, sizeof (gau1CnPreFormedTlv));

    /*Form TLV Header */
    CN_FORM_TLV_HEADER ((UINT2) CN_TLV_TYPE, (UINT2) CN_PREFORMED_TLV_LEN,
                        &u2TlvHeader);
    /* put tlv header in linear buffer */
    pu1Temp = gau1CnPreFormedTlv;
    CN_PUT_2BYTE (pu1Temp, u2TlvHeader)
        CN_PUT_OUI (pu1Temp, gau1CnOUI)
        CN_PUT_1BYTE (pu1Temp, (UINT1) CN_TLV_SUB_TYPE) return;
}

/****************************************************************************
*
*    FUNCTION NAME    : CnUtilMasterEnable
*
*    DESCRIPTION      : Function to enable master status in the component
*
*    INPUT            : pComptbl - pointer to component table
*
*    OUTPUT           : None
*
*    RETURNS          : None
*
****************************************************************************/

PUBLIC VOID
CnUtilMasterEnable (tCnCompTblInfo * pComptbl)
{
    UINT4               u4Priority = CN_ZERO;
    tCnPortTblInfo     *pCnPortTblEntry = NULL;
    tCnPortTblInfo     *pCnTempPortTbl = NULL;
    tCnPortPriTblInfo  *pCnPortPriEntry = NULL;

    /*Setting the CNM priority for the component in NPAPI */
    CnHwSetCnmPri (pComptbl->u4CompId, pComptbl->u1CnmTransmitPriority);

    /*set the flag master enable */
    pComptbl->bCnCompMasterEnable = CN_MASTER_ENABLE;

    pCnPortTblEntry = RBTreeGetFirst (pComptbl->PortTbl);
    while (pCnPortTblEntry != NULL)
    {
        for (u4Priority = CN_ZERO; u4Priority < CN_MAX_VLAN_PRIORITY;
             u4Priority++)
        {
            CN_PORT_PRI_ENTRY (pComptbl->u4CompId, pCnPortTblEntry->u4PortId,
                               u4Priority, pCnPortPriEntry);
            if (pCnPortPriEntry != NULL)
            {
                CnUtilEnableCnpv (pComptbl, pCnPortTblEntry, pCnPortPriEntry);
            }
        }
        pCnTempPortTbl = pCnPortTblEntry;
        pCnPortTblEntry = (tCnPortTblInfo *) RBTreeGetNext
            (pComptbl->PortTbl, pCnTempPortTbl, NULL);
    }
    return;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : CnUtilMasterDisable
 *
 *    DESCRIPTION      : Function to disable master status in the component
 *
 *    INPUT            : pComptbl - pointer to component table
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : NONE
 *
 ****************************************************************************/
PUBLIC VOID
CnUtilMasterDisable (tCnCompTblInfo * pComptbl)
{
    UINT4               u4Priority = CN_ZERO;
    tCnPortTblInfo     *pCnPortTblEntry = NULL;
    tCnPortTblInfo     *pCnTempPortTbl = NULL;
    tCnPortPriTblInfo  *pCnPortPriEntry = NULL;

    pCnPortTblEntry = RBTreeGetFirst (pComptbl->PortTbl);
    while (pCnPortTblEntry != NULL)
        /*loop for all the ports present in the component */
    {
        for (u4Priority = CN_ZERO; u4Priority < CN_MAX_VLAN_PRIORITY;
             u4Priority++)
        {
            CN_PORT_PRI_ENTRY (pComptbl->u4CompId, pCnPortTblEntry->u4PortId,
                               u4Priority, pCnPortPriEntry);
            if (pCnPortPriEntry != NULL)
            {                    /*Setting the operational defense mode as disabled */
                CnSmStateDisabled (pComptbl, pCnPortTblEntry, pCnPortPriEntry);
            }
        }
        pCnTempPortTbl = pCnPortTblEntry;
        pCnPortTblEntry = (tCnPortTblInfo *) RBTreeGetNext
            (pComptbl->PortTbl, pCnTempPortTbl, NULL);
    }
    pComptbl->bCnCompMasterEnable = CN_MASTER_DISABLE;

    return;

}

/****************************************************************************
 *
 *    FUNCTION NAME    : cnUtilAddErroredPortEntry
 *
 *    DESCRIPTION      : Function to delete the entry port entry
 *
 *    INPUT            : pPortPriTable  - pointer to port priority table
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *
 ****************************************************************************/
PUBLIC INT4
CnUtilAddErroredPortEntry (tCnPortPriTblInfo * pPortPriTable)
{
    tCnTrapMsg          CnTrapMsg;

    MEMSET (&CnTrapMsg, CN_ZERO, sizeof (tCnTrapMsg));

    pPortPriTable->bIsErrorPortEntry = CN_ERROR_PORT_ENTRY_SET;

    CnTrapMsg.u4ContextId = pPortPriTable->u4CnComPriComponentId;
    CnTrapMsg.u4PortId = pPortPriTable->u4PortId;
    CnTrapMsg.u1CNPV = pPortPriTable->u1CNPV;

#ifndef FM_WANTED
    SYS_LOG_MSG ((SYSLOG_CRITICAL_LEVEL, gCnMasterTblInfo.u4SysLogId,
                  "CnUtilAddErroredPortEntry Port %d, priority %d : "
                  "added error port entry", pPortPriTable->u4PortId,
                  pPortPriTable->u1CNPV,));
#endif

    CN_TRC_ARG2 (pPortPriTable->u4CnComPriComponentId,
                 CN_CONTROL_PLANE_TRC,
                 "CnUtilAddErroredPortEntry: Error port created "
                 "for port %d CNPV %d \r\n",
                 pPortPriTable->u4PortId, pPortPriTable->u1CNPV);

    CnSendNotification ((VOID *) &CnTrapMsg, CN_ADDED_ERROR_PORT_ENTRY);

    return (OSIX_SUCCESS);
}

/****************************************************************************
 *
 *    FUNCTION NAME    : cnUtilDeleteErroredPortEntry
 *
 *    DESCRIPTION      : Function to delete the entry port entry
 *
 *    INPUT            : pPortPriTable  - pointer to port priority table
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *
 ****************************************************************************/
PUBLIC INT4
CnUtilDeleteErroredPortEntry (tCnPortPriTblInfo * pPortPriTable)
{
    tCnTrapMsg          CnTrapMsg;

    MEMSET (&CnTrapMsg, CN_ZERO, sizeof (tCnTrapMsg));

    pPortPriTable->bIsErrorPortEntry = CN_ERROR_PORT_ENTRY_RESET;

    CnTrapMsg.u4ContextId = pPortPriTable->u4CnComPriComponentId;
    CnTrapMsg.u4PortId = pPortPriTable->u4PortId;
    CnTrapMsg.u1CNPV = pPortPriTable->u1CNPV;

#ifndef FM_WANTED
    SYS_LOG_MSG ((SYSLOG_CRITICAL_LEVEL, gCnMasterTblInfo.u4SysLogId,
                  "CnUtilDeleteErroredPortEntry Port %d, priority %d : "
                  "deleted error port entry", pPortPriTable->u4PortId,
                  pPortPriTable->u1CNPV,));
#endif

    CN_TRC_ARG2 (pPortPriTable->u4CnComPriComponentId,
                 CN_CONTROL_PLANE_TRC,
                 "CnUtilDeleteErroredPortEntry: Error port deleted "
                 "for port %d CNPV %d \r\n",
                 pPortPriTable->u4PortId, pPortPriTable->u1CNPV);

    CnSendNotification ((VOID *) &CnTrapMsg, CN_DELETED_ERROR_PORT_ENTRY);

    return (OSIX_SUCCESS);
}

/****************************************************************************
 *    FUNCTION NAME    : CnUtilCreatePortTblRBTree
 *
 *    DESCRIPTION      : Function Create the RBtrees in CN module
 *                       PortTbl                                             
 *
 *    INPUT            : None
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : None
 ****************************************************************************/
PUBLIC VOID
CnUtilCreatePortTblRBTree (UINT4 U4CompId)
{
    UINT4               u4RBNodeOffset = CN_ZERO;
    tCnCompTblInfo     *pCnCompTblEntry = NULL;

    pCnCompTblEntry = CN_COMP_TBL_ENTRY (U4CompId);
    if (pCnCompTblEntry != NULL)
    {
        /* Create PortTbl RBTree */
        u4RBNodeOffset = FSAP_OFFSETOF (tCnPortTblInfo, NextPortNode);
        (gCnMasterTblInfo.ppCompTblInfo[U4CompId])->PortTbl =
            RBTreeCreateEmbedded (u4RBNodeOffset, CnUtilRBCmpPortTbl);
    }
    return;
}

/****************************************************************************
*    FUNCTION NAME    : CnUtilDeleteRBTrees
*
*    DESCRIPTION      : Function Deletes all RBtrees in CN module
*                       following RBTrees are Created -
*                       1. PortTbl
*                       2. CpIdTbl
*
*    INPUT            : None
*
*    OUTPUT           : None
*
*    RETURNS          : None
****************************************************************************/
PUBLIC VOID
CnUtilDeleteRBTrees (VOID)
{
    UINT4               u4CompId = CN_ZERO;
    /* 1. Delete PortTbl  RBTree */
    for (u4CompId = CN_MIN_CONTEXT; u4CompId < CN_MAX_CONTEXTS; u4CompId++)
    {
        if (CN_COMP_TBL_ENTRY (u4CompId) != NULL)
        {
            RBTreeDelete (CN_COMP_TBL_ENTRY (u4CompId)->PortTbl);
            CN_COMP_TBL_ENTRY (u4CompId)->PortTbl = NULL;
        }
    }
    /* 2. Delete CpIdTbl  RBTree */
    if (gCnMasterTblInfo.CpIdTbl != NULL)
    {
        RBTreeDelete (gCnMasterTblInfo.CpIdTbl);
        gCnMasterTblInfo.CpIdTbl = NULL;
    }

    return;
}

/****************************************************************************
 *    FUNCTION NAME    : CnUtilRBCmpPortTbl
 *
 *    DESCRIPTION      : This function compare the index of the RB tree for
 *                       Port Table
 *
 *    INPUT            : pRBElem1 - pointer to first RBElement of the tree
 *                       pRBElem2 -  pointer to second RBElement of the tree
 *                       
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 ****************************************************************************/
PUBLIC INT4
CnUtilRBCmpPortTbl (tRBElem * pRBElem1, tRBElem * pRBElem2)
{
    tCnPortTblInfo     *pPortTblEntry1 = (tCnPortTblInfo *) pRBElem1;
    tCnPortTblInfo     *pPortTblEntry2 = (tCnPortTblInfo *) pRBElem2;
    /* Compare the Index - Port Id */
    if (pPortTblEntry1->u4PortId > pPortTblEntry2->u4PortId)
    {
        return CN_RB_GREATER;
    }
    else if (pPortTblEntry1->u4PortId < pPortTblEntry2->u4PortId)
    {
        return CN_RB_LESS;
    }
    else
    {
        return CN_RB_EQUAL;
    }
}

/****************************************************************************
 *    FUNCTION NAME    : CnUtilRBCmpCpIdTbl
 *
 *    DESCRIPTION      : This function compare the index of the RB tree for
 *                       CPID Table
 *
 *    INPUT            : pRBElem1 - pointer to first RBElement of the tree
 *                       pRBElem2 -  pointer to second RBElement of the tree
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 ****************************************************************************/
PUBLIC INT4
CnUtilRBCmpCpIdTbl (tRBElem * pRBElem1, tRBElem * pRBElem2)
{
    INT4                i4MemcmpRetVal = CN_ZERO;

    tCnPortPriTblInfo  *pPortPriTblEntry1 = (tCnPortPriTblInfo *) pRBElem1;
    tCnPortPriTblInfo  *pPortPriTblEntry2 = (tCnPortPriTblInfo *) pRBElem2;

    /* Compare the Index - CP Id */
    i4MemcmpRetVal = MEMCMP (pPortPriTblEntry1->au1CnCpIdentifier,
                             pPortPriTblEntry2->au1CnCpIdentifier, CN_CPID_LEN);
    if (i4MemcmpRetVal > CN_ZERO)
    {
        return CN_RB_GREATER;
    }
    else if (i4MemcmpRetVal < CN_ZERO)
    {
        return CN_RB_LESS;
    }
    else
    {
        return CN_RB_EQUAL;
    }
}

/****************************************************************************
 *
 *    FUNCTION NAME    : CnUtilDeleteQMsg 
 *
 *    DESCRIPTION      : Function that deletes message in Queue
 *
 *    INPUT            : QId -    Queue Id to be deleted
 *                       PoolId - Mem Pool Id associated with the queue
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : None
 *
 ****************************************************************************/
PUBLIC VOID
CnUtilDeleteQMsg (tOsixQId QId, tMemPoolId PoolId)
{
    tCnQMsg            *pQueue = NULL;

    if (QId == CN_ZERO)
    {
        return;
    }
    if (PoolId == CN_ZERO)
    {
        return;
    }
    /* Dequeue all messages */
    while (OsixQueRecv (QId, (UINT1 *) &pQueue,
                        OSIX_DEF_MSG_LEN, CN_ZERO) == OSIX_SUCCESS)
    {
        MemReleaseMemBlock (PoolId, (UINT1 *) pQueue);
    }

    return;
}

/****************************************************************************
*
*    FUNCTION NAME    : CnUtilCnpdAdminDefenseMode
*
*    DESCRIPTION      : Function that calculates the oper admin defense mode
*
*    INPUT            : pPortPriTable - pointer to port priority table
*
*    OUTPUT           : None
*
*    RETURNS          : None
*
****************************************************************************/
PUBLIC VOID
CnUtilCnpdAdminDefenseMode (tCnPortPriTblInfo * pPortPriTable)
{

    tCnCompPriTbl      *pCnCompPriEntry = NULL;

    if (pPortPriTable->u1PortPriDefModeChoice == CN_ADMIN)
    {
        pPortPriTable->u1OperAdminDefenseMode =
            pPortPriTable->u1PortPriAdminDefMode;
    }
    else if (pPortPriTable->u1PortPriDefModeChoice == CN_COMP)
    {
        CN_COMP_PRI_ENTRY (pPortPriTable->u4CnComPriComponentId,
                           pPortPriTable->u1CNPV, pCnCompPriEntry);
        /*port pirority defense mode choice is comp 
         *checking the defense mode choice is component priority table */
        if ((pCnCompPriEntry != NULL) &&
            (pCnCompPriEntry->u1ComPriDefModeChoice == CN_ADMIN))
        {
            pPortPriTable->u1OperAdminDefenseMode =
                pCnCompPriEntry->u1ComPriAdminDefMode;
        }
    }
    return;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : CnUtilCalculateErroredPortEntry
 *
 *    DESCRIPTION      : Function that calculates the error port entry when 
 *                       new  CNPV creates or the alternate priority changes
 *
 *    INPUT            : u4ContextId - context ID
 *                       u4NewCNPV - New CNPV created
 *                       u1Status - alterpriority / CNPV
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : None 
 *
 ****************************************************************************/
PUBLIC VOID
CnUtilCalculateErroredPortEntry (UINT4 u4ContextId, UINT4 u4NewCNPV,
                                 UINT4 u4CompAltPriority, UINT1 u1Status)
{
    UINT4               u4Priority = CN_ZERO;
    tCnPortTblInfo     *pCnPortTblEntry = NULL;
    tCnPortTblInfo     *pCnTempPortTbl = NULL;
    tCnPortPriTblInfo  *pCnPortPriEntry = NULL;
    tCnCompTblInfo     *pCnCompTblEntry = NULL;
    tCnCompPriTbl      *pCnCompPriEntry = NULL;

    pCnCompTblEntry = CN_COMP_TBL_ENTRY (u4ContextId);
    if (pCnCompTblEntry != NULL)
    {
        pCnPortTblEntry = RBTreeGetFirst (pCnCompTblEntry->PortTbl);
        while (pCnPortTblEntry != NULL)
            /*loop for all the ports present in the component */
        {
            for (u4Priority = CN_ZERO; u4Priority < CN_MAX_VLAN_PRIORITY;
                 u4Priority++)
            {
                CN_COMP_PRI_ENTRY (u4ContextId, u4Priority, pCnCompPriEntry);

                if (pCnCompPriEntry == NULL)
                {
                    continue;
                }

                CN_PORT_PRI_ENTRY (pCnCompTblEntry->u4CompId,
                                   pCnPortTblEntry->u4PortId,
                                   u4Priority, pCnPortPriEntry);
                if (pCnPortPriEntry != NULL)
                {
                    if (u1Status == CN_CNPV_CREATE)
                    {
                        /* Error port entry - creation logic
                         * 1. Newly created CNPV is already configured
                         *    as alternate priority for either component
                         *    priority or port priority table
                         * 2. Created CNPV itself has the same value
                         *    as its alternate priority
                         **/

                        if ((pCnPortPriEntry->u1PortPriAdminAltPri ==
                             (UINT1) u4NewCNPV)
                            || (u4CompAltPriority == u4NewCNPV)
                            || (pCnCompPriEntry->u1ComPriAltPri ==
                                (UINT1) u4NewCNPV))
                        {
                            CnUtilAddErroredPortEntry (pCnPortPriEntry);
                        }
                    }
                    else if (u1Status == CN_CNPV_DELETE)
                    {
                        /* Error port entry - deletion logic
                         * 1. Deleted CNPV is already configured
                         *    as alt. priority for either component
                         *    or port priority table.
                         */
                        if ((pCnPortPriEntry->u1PortPriAdminAltPri ==
                             (UINT1) u4NewCNPV) ||
                            (pCnCompPriEntry->u1ComPriAltPri ==
                             (UINT1) u4NewCNPV))
                        {

                            CnUtilDeleteErroredPortEntry (pCnPortPriEntry);
                        }
                    }
                }
            }
            pCnTempPortTbl = pCnPortTblEntry;
            pCnPortTblEntry = (tCnPortTblInfo *) RBTreeGetNext
                (pCnCompTblEntry->PortTbl, pCnTempPortTbl, NULL);
        }
    }

    return;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : CnUtilCalculateAutoAltPri
 *
 *    DESCRIPTION      : Function that calculates the auto altrnate priority
 *
 *    INPUT            : pComptbl - Pointer to component table
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : None 
 *
 ****************************************************************************/

PUBLIC VOID
CnUtilCalculateAutoAltPri (tCnCompTblInfo * pComptbl)
{

    UINT4               u4AltPri = CN_ZERO;
    tCnCompPriTbl      *pCnCompPriTbl = NULL;
    tCnPortTblInfo     *pCnPortTblEntry = NULL;
    tCnPortTblInfo     *pCnTempPortTblEntry = NULL;
    tCnPortPriTblInfo  *pCnPortPriEntry = NULL;
    UINT4               u4LoopFlag = OSIX_TRUE;
    UINT4               u4PortId = CN_ZERO;
    UINT1               u1PrevOperAltPri = CN_ZERO;
    UINT1               u1Priority = CN_ZERO;

    /*Finding the lowest value */
    while ((u1Priority < CN_MAX_VLAN_PRIORITY) && (u4LoopFlag == OSIX_TRUE))
    {
        CN_COMP_PRI_ENTRY (pComptbl->u4CompId, u1Priority, pCnCompPriTbl)
            if (pCnCompPriTbl == NULL)
        {
            u4AltPri = u1Priority;
            u4LoopFlag = OSIX_FALSE;
        }

        u1Priority++;
    }
    /*set the auto alternate priority */
    for (u1Priority = CN_MIN_CNPV; u1Priority < CN_MAX_VLAN_PRIORITY;
         u1Priority++)
    {
        CN_COMP_PRI_ENTRY (pComptbl->u4CompId, u1Priority, pCnCompPriTbl)
            if (pCnCompPriTbl != NULL)
        {
            pCnCompPriTbl->u1ComPriAutoAltPri = (UINT1) u4AltPri;
        }
    }

    /*setting in Hardware */
    pCnPortTblEntry = (tCnPortTblInfo *) RBTreeGetFirst (pComptbl->PortTbl);

    while (pCnPortTblEntry != NULL)
    {
        u4PortId = pCnPortTblEntry->u4PortId;
        for (u1Priority = CN_MIN_CNPV; u1Priority < CN_MAX_VLAN_PRIORITY;
             u1Priority++)
        {
            CN_COMP_PRI_ENTRY (pComptbl->u4CompId, u1Priority, pCnCompPriTbl);
            if (pCnCompPriTbl != NULL)
            {
                CN_PORT_PRI_ENTRY (pComptbl->u4CompId, u4PortId, u1Priority,
                                   pCnPortPriEntry);
                if (pCnPortPriEntry != NULL)
                {
                    u1PrevOperAltPri = pCnPortPriEntry->u1PortPriOperAltPri;
                    CnUtilCalculateOperAltPri (pCnPortPriEntry);
                    if ((pComptbl->bCnCompMasterEnable == CN_MASTER_ENABLE) &&
                        (pCnCompPriTbl->u1ComPriRowStatus == CN_ACTIVE) &&
                        (pCnPortPriEntry->u1PortPriOperDefMode == CN_EDGE) &&
                        (u1PrevOperAltPri !=
                         pCnPortPriEntry->u1PortPriOperAltPri))
                    {
                        CnPortQosPriorityMapReq (pComptbl->u4CompId, u4PortId,
                                                 u1Priority,
                                                 &(pCnPortPriEntry->
                                                   u1PortPriOperAltPri),
                                                 QOS_ADD_PRIORITY_MAP);
                    }
                }
            }
        }
        pCnTempPortTblEntry = pCnPortTblEntry;
        pCnPortTblEntry = (tCnPortTblInfo *) RBTreeGetNext
            (pComptbl->PortTbl, pCnTempPortTblEntry, NULL);
    }

    return;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : CnUtilCalculateOperAltPri
 *
 *    DESCRIPTION      : Function that calculates the operational alternate 
 *                       priority for each port and priority
 *
 *    INPUT            : pPortPriTable  - Pointer to Port Priority 
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : None
 *
 ****************************************************************************/
PUBLIC VOID
CnUtilCalculateOperAltPri (tCnPortPriTblInfo * pPortPriTable)
{

    tCnCompPriTbl      *pCnCompPriEntry = NULL;

    CN_COMP_PRI_ENTRY (pPortPriTable->u4CnComPriComponentId,
                       pPortPriTable->u1CNPV, pCnCompPriEntry);
    if (pCnCompPriEntry == NULL)
    {
        return;
    }

    if (pPortPriTable->u1PortPriDefModeChoice == CN_ADMIN)
    {
        pPortPriTable->u1PortPriOperAltPri =
            pPortPriTable->u1PortPriAdminAltPri;
    }

    else if (pPortPriTable->u1PortPriDefModeChoice == CN_COMP)
    {
        /*port pirority defense mode choice is comp
         *checking the defense mode choice is component priority table */
        if (pCnCompPriEntry->u1ComPriDefModeChoice == CN_ADMIN)
        {
            pPortPriTable->u1PortPriOperAltPri =
                pCnCompPriEntry->u1ComPriAltPri;
        }
        else
        {
            pPortPriTable->u1PortPriOperAltPri =
                pCnCompPriEntry->u1ComPriAutoAltPri;
        }
    }

    else
    {
        pPortPriTable->u1PortPriOperAltPri =
            pCnCompPriEntry->u1ComPriAutoAltPri;
    }

    return;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : CnUtilCalculateTlvIndicators
 *
 *    DESCRIPTION      : This routine calculate the TLV Indicators
 *    
 *    INPUT            : pPortPriTbl  - Pointer to Port priority table
 *                       pCompTbl     - Pointer to Component Table
 *                       pCnPortTblEntry - Pointer to PortTable
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *
 ****************************************************************************/
PUBLIC INT4
CnUtilCalculateTlvIndicators (tCnCompTblInfo * pCompTbl,
                              tCnPortTblInfo * pCnPortTblEntry,
                              tCnPortPriTblInfo * pPortPriTbl)
{
    UINT4               u4LldpInsChoice = CN_ZERO;
    UINT1               u1cnpvIndicator = CN_ZERO;
    UINT1               u1ReadyIndicator = CN_ZERO;

    /*form one byte present in the Port table */

    /*TLV BIT set logic :
       To set a bit as one:
       1. shift that one with n times to that bit
       2. do OR operation with that byte present in the port table
       ***********************************************************
       To Set a bit as zero
       1. shift that one with n times to that bit
       2. take one's compliment to that local byte
       3. do AND OR operation with that byte present in the port table */

    if (pCompTbl->bCnCompMasterEnable == CN_MASTER_ENABLE)
    {
        CnUtilLldpInstanceChoice (pPortPriTbl, &u4LldpInsChoice);
        if (u4LldpInsChoice != CN_LLDP_NONE)
        {
            if ((pPortPriTbl->bXmitReady == OSIX_TRUE) &&
                (pPortPriTbl->bXmitCnpvCapable == OSIX_TRUE))
            {
                u1cnpvIndicator =
                    ((UINT1) (CN_TLV_BIT_ONE << pPortPriTbl->u1CNPV));
                u1ReadyIndicator =
                    ((UINT1) (CN_TLV_BIT_ONE << pPortPriTbl->u1CNPV));
                pPortPriTbl->u1LastSentEvent = (UINT1) CN_DBG_SENT_READY;
                OsixGetSysTime (&(pPortPriTbl->u4LastEvtSentTime));

                pCnPortTblEntry->u1CnpvIndicator =
                    pCnPortTblEntry->u1CnpvIndicator | u1cnpvIndicator;

                pCnPortTblEntry->u1CnpvreadyIndicator =
                    pCnPortTblEntry->u1CnpvreadyIndicator | u1ReadyIndicator;

            }
            else if (pPortPriTbl->bXmitCnpvCapable == OSIX_TRUE)
            {
                u1cnpvIndicator =
                    ((UINT1) (CN_TLV_BIT_ONE << pPortPriTbl->u1CNPV));
                u1ReadyIndicator =
                    ((UINT1) (CN_TLV_BIT_ONE << pPortPriTbl->u1CNPV));
                pPortPriTbl->u1LastSentEvent = (UINT1) CN_DBG_SENT_TLV;
                OsixGetSysTime (&(pPortPriTbl->u4LastEvtSentTime));

                pCnPortTblEntry->u1CnpvIndicator =
                    pCnPortTblEntry->u1CnpvIndicator | u1cnpvIndicator;

                u1ReadyIndicator = ((UINT1) (~u1ReadyIndicator));
                pCnPortTblEntry->u1CnpvreadyIndicator =
                    pCnPortTblEntry->u1CnpvreadyIndicator & u1ReadyIndicator;
            }
            else
            {
                u1cnpvIndicator =
                    ((UINT1) (CN_TLV_BIT_ONE << pPortPriTbl->u1CNPV));
                u1ReadyIndicator =
                    ((UINT1) (CN_TLV_BIT_ONE << pPortPriTbl->u1CNPV));
                pPortPriTbl->u1LastSentEvent = (UINT1) CN_DBG_NO_TLV_SENT;
                OsixGetSysTime (&(pPortPriTbl->u4LastEvtSentTime));

                u1cnpvIndicator = ((UINT1) (~u1cnpvIndicator));
                pCnPortTblEntry->u1CnpvIndicator =
                    pCnPortTblEntry->u1CnpvIndicator & u1cnpvIndicator;

                u1ReadyIndicator = ((UINT1) (~u1ReadyIndicator));
                pCnPortTblEntry->u1CnpvreadyIndicator =
                    pCnPortTblEntry->u1CnpvreadyIndicator & u1ReadyIndicator;

            }
        }
        else
        {
            u1cnpvIndicator = ((UINT1) (CN_TLV_BIT_ONE << pPortPriTbl->u1CNPV));
            u1ReadyIndicator =
                ((UINT1) (CN_TLV_BIT_ONE << pPortPriTbl->u1CNPV));
            pPortPriTbl->u1LastSentEvent = (UINT1) CN_DBG_NO_TLV_SENT;
            OsixGetSysTime (&(pPortPriTbl->u4LastEvtSentTime));

            u1cnpvIndicator = ((UINT1) (~u1cnpvIndicator));
            pCnPortTblEntry->u1CnpvIndicator = pCnPortTblEntry->u1CnpvIndicator
                & u1cnpvIndicator;

            u1ReadyIndicator = ((UINT1) (~u1ReadyIndicator));
            pCnPortTblEntry->u1CnpvreadyIndicator =
                pCnPortTblEntry->u1CnpvreadyIndicator & u1ReadyIndicator;

        }
    }
    else
    {
        u1cnpvIndicator = ((UINT1) (CN_TLV_BIT_ONE << pPortPriTbl->u1CNPV));
        u1ReadyIndicator = ((UINT1) (CN_TLV_BIT_ONE << pPortPriTbl->u1CNPV));
        pPortPriTbl->u1LastSentEvent = (UINT1) CN_DBG_NO_TLV_SENT;
        OsixGetSysTime (&(pPortPriTbl->u4LastEvtSentTime));

        u1cnpvIndicator = ((UINT1) ~u1cnpvIndicator);
        pCnPortTblEntry->u1CnpvIndicator = pCnPortTblEntry->u1CnpvIndicator
            & u1cnpvIndicator;

        u1ReadyIndicator = ((UINT1) ~u1ReadyIndicator);
        pCnPortTblEntry->u1CnpvreadyIndicator =
            pCnPortTblEntry->u1CnpvreadyIndicator & u1ReadyIndicator;

    }

    /*TLV Timer will start if it is nor running already */
    if (pCnPortTblEntry->TlvTxDelayTmrNode.u1Status == CN_TMR_NOT_RUNNING)
    {
        if (TmrStart (gCnMasterTblInfo.TmrListId,
                      &(pCnPortTblEntry->TlvTxDelayTmrNode.CnTmrBlk),
                      CN_TLV_TMR, gCnMasterTblInfo.i4TlvTxDelay,
                      (UINT4) CN_ZERO) != TMR_SUCCESS)
        {
            CN_TRC_ARG1 (pCnPortTblEntry->u4ComponentId, (CN_ALL_FAILURE_TRC
                                                          |
                                                          CN_CONTROL_PLANE_TRC),
                         "Port: %d TLV Timer Start FAILED\r\n",
                         pCnPortTblEntry->u4PortId);
            return OSIX_FAILURE;
        }
        pCnPortTblEntry->TlvTxDelayTmrNode.u1Status = CN_TMR_RUNNING;
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : CnUtilFillLldpPortMsg
 *
 *    DESCRIPTION      : This routine fill the LLDP Port message structure
 *
 *    INPUT            : pLldpAppPortMsg  - Pointer to LLDP port Message table
 *
 *    OUTPUT           : pLldpAppPortMsg - Pointer to LLDP port Message table
 *
 *    RETURNS          : NONE
 *
 ****************************************************************************/
PUBLIC VOID
CnUtilFillLldpPortMsg (tLldpAppPortMsg * pLldpAppPortMsg)
{

    CN_GET_APPL_ID (pLldpAppPortMsg->LldpAppId);

    pLldpAppPortMsg->bAppTlvTxStatus = OSIX_FALSE;
    pLldpAppPortMsg->u1AppTlvRxStatus = OSIX_TRUE;
    pLldpAppPortMsg->u2TxAppTlvLen = CN_TLV_LEN;
    pLldpAppPortMsg->u4LldpInstSelector = CN_ZERO;
    pLldpAppPortMsg->pAppCallBackFn = CnApiApplCallbkFunc;

    MEMCPY (pLldpAppPortMsg->pu1TxAppTlv, gau1CnPreFormedTlv,
            CN_PREFORMED_TLV_LEN);
    pLldpAppPortMsg->pu1TxAppTlv = pLldpAppPortMsg->pu1TxAppTlv
        + CN_PREFORMED_TLV_LEN;
    CN_PUT_1BYTE (pLldpAppPortMsg->pu1TxAppTlv, (UINT1) CN_ZERO)
        CN_PUT_1BYTE (pLldpAppPortMsg->pu1TxAppTlv, (UINT1) CN_ZERO) return;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : CnUtilTlvConstruct
 *
 *    DESCRIPTION      : Function that construct the CN TLV  
 *
 *    INPUT            : pPortTbl - pointer to Port tablei
 *                       u1Msg - Message Type
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *
 ****************************************************************************/
PUBLIC INT4
CnUtilTlvConstruct (tCnPortTblInfo * pPortTbl, UINT1 u1Msg)
{
    UINT1               au1TlvBuf[CN_TLV_LEN] = { CN_ZERO };
    UINT1              *pu1Temp = NULL;
    tLldpAppPortMsg     LldpAppPortMsg;
    MEMSET (&LldpAppPortMsg, CN_ZERO, sizeof (tLldpAppPortMsg));

    CN_GET_APPL_ID (LldpAppPortMsg.LldpAppId);

    LldpAppPortMsg.u2TxAppTlvLen = CN_TLV_LEN;
    LldpAppPortMsg.u4LldpInstSelector = CN_ZERO;
    LldpAppPortMsg.pAppCallBackFn = CnApiApplCallbkFunc;
    pu1Temp = au1TlvBuf;
    MEMCPY (pu1Temp, gau1CnPreFormedTlv, CN_PREFORMED_TLV_LEN);
    pu1Temp = pu1Temp + CN_PREFORMED_TLV_LEN;

    CN_PUT_1BYTE (pu1Temp, pPortTbl->u1CnpvIndicator)
        CN_PUT_1BYTE (pu1Temp, pPortTbl->u1CnpvreadyIndicator)
        LldpAppPortMsg.pu1TxAppTlv = au1TlvBuf;

    if (pPortTbl->u1CnpvIndicator != CN_ZERO)
    {
        LldpAppPortMsg.bAppTlvTxStatus = OSIX_TRUE;
        LldpAppPortMsg.u1AppTlvRxStatus = OSIX_TRUE;

        CN_TRC_ARG1 (pPortTbl->u4ComponentId, CN_TLV_TRC,
                     "CnUtilTlvConstruct : Transmitting TLV on "
                     "Port: %d \r\n DUMPING TRANSMIT CN TLV \r\n",
                     pPortTbl->u4PortId);

        CN_TLV_DUMP (pPortTbl->u4ComponentId, CN_TLV_TRC,
                     LldpAppPortMsg.pu1TxAppTlv, CN_TLV_LEN);
    }
    else
    {
        LldpAppPortMsg.bAppTlvTxStatus = OSIX_FALSE;
        LldpAppPortMsg.u1AppTlvRxStatus = OSIX_TRUE;
        CN_TRC_ARG1 (pPortTbl->u4ComponentId, CN_TLV_TRC,
                     "CnUtilTlvConstruct : No CN TLV to be transmitted on "
                     "Port: %d \r\n", pPortTbl->u4PortId);
    }

    if (CnPortPostCnMsgToLldp (u1Msg, pPortTbl->u4PortId,
                               &LldpAppPortMsg) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : CnUtilLldpInstanceChoice
 *
 *    DESCRIPTION      : Function that calculates the LLDP INSTANCE CHOICE
 *
 *    INPUT            : pPortPriTable- pointer to Port priority table
 *
 *    OUTPUT           : pu4RetVal - CN_LLDP_ADMIN/CN_LLDP_NONE
 *
 *    RETURNS          : 
 *
 ****************************************************************************/
PUBLIC VOID
CnUtilLldpInstanceChoice (tCnPortPriTblInfo * pPortPriTable, UINT4 *pu4RetVal)
{
    tCnCompPriTbl      *pCnCompPriEntry = NULL;

    if (pPortPriTable->u1PortPriLldpInstChoice == CN_LLDP_ADMIN)
    {
        *pu4RetVal = CN_LLDP_ADMIN;
    }
    else if (pPortPriTable->u1PortPriLldpInstChoice == CN_LLDP_COMP)
    {
        CN_COMP_PRI_ENTRY (pPortPriTable->u4CnComPriComponentId,
                           pPortPriTable->u1CNPV, pCnCompPriEntry);
        /*port pirority defense mode choice is comp
         *checking the defense mode choice is component priority table */
        if ((pCnCompPriEntry != NULL) &&
            (pCnCompPriEntry->u1ComPriLldpInstChoice == CN_LLDP_ADMIN))
        {
            *pu4RetVal = CN_LLDP_ADMIN;
        }
        else
        {
            *pu4RetVal = CN_LLDP_NONE;
        }
    }
    else
    {
        *pu4RetVal = CN_LLDP_NONE;
    }
    return;
}

/****************************************************************************
*
*    FUNCTION NAME    : CnUtilActivatePortInCNPV
*
*    DESCRIPTION      : Function that activate CNPV for given  port where 
*                       port is created after the CNPV creation.
*
*    INPUT            : pCnCompTblEntry - Component Table Entry
*                       u4PortIndex - Port Index
*                       u4Priority - Priority
*
*    OUTPUT           : None
*
*    RETURNS          : None
*
*****************************************************************************/
PUBLIC VOID
CnUtilActivatePortInCNPV (tCnCompTblInfo * pCnCompTblEntry, UINT4 u4PortId,
                          UINT4 u4Priority)
{
    tCnPortPriTblInfo  *pCnPortPriEntry = NULL;
    tCnPortTblInfo     *pCnPortTblEntry = NULL;
    tCnPortTblInfo      TempPortTbl;

    MEMSET (&TempPortTbl, CN_ZERO, sizeof (tCnPortTblInfo));

    TempPortTbl.u4PortId = u4PortId;

    pCnPortTblEntry = (tCnPortTblInfo *) RBTreeGet
        (pCnCompTblEntry->PortTbl, &TempPortTbl);
    if (pCnPortTblEntry != NULL)
    {
        pCnPortPriEntry = pCnPortTblEntry->paPortPriTbl[u4Priority];
    }

    if (pCnPortPriEntry != NULL)
    {
        CnUtilEnableCnpv (pCnCompTblEntry, pCnPortTblEntry, pCnPortPriEntry);
    }

    return;
}

/****************************************************************************
*
*    FUNCTION NAME    : CnGetFirstPortInContext
*
*    DESCRIPTION      : This function is used to get the first port
*                       mapped to this context
*
*    INPUT            : u4ContextId  -Context Id
*
*    OUTPUT           : pi4IfIndex - Interface Index
*
*    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
*
****************************************************************************/
PUBLIC INT4
CnGetFirstPortInContext (UINT4 u4CompId, INT4 *pu4IfIndex)
{

    tCnPortTblInfo     *pCnPortTblEntry = NULL;

    if (CN_COMP_TBL_ENTRY (u4CompId) == NULL)
    {
        return OSIX_FAILURE;
    }

    pCnPortTblEntry = (tCnPortTblInfo *) RBTreeGetFirst
        (CN_COMP_TBL_ENTRY (u4CompId)->PortTbl);

    if (pCnPortTblEntry != NULL)
    {
        *pu4IfIndex = pCnPortTblEntry->u4PortId;
        return OSIX_SUCCESS;
    }
    return OSIX_FAILURE;
}

/****************************************************************************
*
*    FUNCTION NAME    : CnGetNextPortInContext
*
*    DESCRIPTION      : This function is used to get the next port
*                       mapped to this context.
*
*    INPUT            : i4IfIndex - Current IfIndex.
*                       u4ContextId  -Context Id
*
*    OUTPUT           : pi4NextIfIndex
*
*    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
*
****************************************************************************/
PUBLIC INT4
CnGetNextPortInContext (UINT4 u4CompId, INT4 u4IfIndex, INT4 *pu4NextIfIndex)
{
    tCnPortTblInfo     *pCnPortTblEntry = NULL;
    tCnPortTblInfo      CnPortTblEntry;

    *pu4NextIfIndex = CN_ZERO;
    MEMSET (&CnPortTblEntry, CN_ZERO, sizeof (tCnPortTblInfo));

    if (CN_COMP_TBL_ENTRY (u4CompId) == NULL)
    {
        return OSIX_FAILURE;
    }

    CnPortTblEntry.u4PortId = u4IfIndex;

    pCnPortTblEntry = (tCnPortTblInfo *) RBTreeGetNext
        (CN_COMP_TBL_ENTRY (u4CompId)->PortTbl, &CnPortTblEntry, NULL);

    if (pCnPortTblEntry != NULL)
    {
        *pu4NextIfIndex = pCnPortTblEntry->u4PortId;
        return OSIX_SUCCESS;
    }
    return OSIX_FAILURE;
}

/*****************************************************************************
*  Function Name   : CnUtilCreateComPriEntry              
*  Description     : This function allocates memory for the  
*                    component priority table and assigns default values 
*                    This function calls the CnUtilCreatePortPriEntry for all 
*                    ports present in the component
*  Input(s)        :  pCnCompTblEntry - Component Table Entry 
*                     u4CompPriority - CNPV
*  Output(s)       : Component Priority Entry and corresponding Port Priority
*                    Entries are created and default values are set
*  Returns         : OSIX_SUCCESS / OSIX_FAILURE          
*******************************************************************************/
INT4
CnUtilCreateCompPriEntry (tCnCompTblInfo * pCnCompTblEntry,
                          UINT4 u4CompPriority)
{
    tCnCompPriTbl      *pCnCompPriEntry = NULL;
    tCnPortTblInfo     *pCnPortTblEntry = NULL;
    tCnPortTblInfo     *pCnTempPortTblEntry = NULL;
    tCnPortTblInfo     *pCnFailedPortEntry = NULL;
    tCnPortTblInfo     *pCnTempFailedPortEntry = NULL;
    tCnPortPriTblInfo  *pCnPortPriEntry = NULL;
    UINT4               u4PortId = CN_ZERO;
    UINT4               u4CompId = pCnCompTblEntry->u4CompId;
    UINT1               u1ErroredPortFlag = OSIX_TRUE;

    pCnCompPriEntry = (tCnCompPriTbl *) MemAllocMemBlk
        (gCnMasterTblInfo.CompPriTblPoolId);

    if (pCnCompPriEntry == NULL)
    {
        /*Memory allocation failed */
        CN_TRC_ARG2 (u4CompId, (CN_ALL_FAILURE_TRC | CN_RESOURCE_TRC),
                     "CnUtilCreateCompPriEntry: Memory Allocation FAILED for"
                     " Context %u, CNPV %u\r\n", pCnCompTblEntry->u4CompId,
                     u4CompPriority);
        return OSIX_FAILURE;
    }
    MEMSET (pCnCompPriEntry, CN_ZERO, sizeof (tCnCompPriTbl));

    /*Assign Default Values for component priority entries */
    pCnCompPriEntry->u4CompId = pCnCompTblEntry->u4CompId;
    pCnCompPriEntry->u1CNPV = ((UINT1) u4CompPriority);
    pCnCompPriEntry->u1ComPriDefModeChoice = CN_AUTO;
    pCnCompPriEntry->u1ComPriAltPri = CN_ZERO;
    pCnCompPriEntry->u1ComPriAdminDefMode = CN_INTERIOR;
    pCnCompPriEntry->u1ComPriLldpInstChoice = CN_LLDP_ADMIN;
    pCnCompPriEntry->u4ComPriLldpInstSelector = CN_DEF_COMP_LLDP_INST_SELECTOR;
    pCnCompPriEntry->u1DefaultPortDefModeChoice = CN_AUTO_ENABLE;

    /* Update address of CompPriEntry in Component Table */
    pCnCompTblEntry->pCompPriTbl[u4CompPriority] = pCnCompPriEntry;

    /*Calculate auto alternate priority */
    CnUtilCalculateAutoAltPri (pCnCompTblEntry);

    /*Create in all member ports of this component */
    pCnPortTblEntry = (tCnPortTblInfo *) RBTreeGetFirst
        (pCnCompTblEntry->PortTbl);
    while (pCnPortTblEntry != NULL)
    {
        /*Create Port Priority Entries for CNPV created */
        if (CnUtilCreatePortPriEntry (pCnPortTblEntry, u4CompPriority,
                                      pCnCompPriEntry->u1ComPriDefModeChoice,
                                      pCnCompPriEntry->u1ComPriAltPri,
                                      u1ErroredPortFlag) == OSIX_FAILURE)
        {
            /*Port Pri Entry Creation Failed */
            /*Remove PortPriEntries created so far */
            pCnFailedPortEntry = (tCnPortTblInfo *) RBTreeGetFirst
                (pCnCompTblEntry->PortTbl);

            while ((pCnFailedPortEntry != NULL)
                   && (pCnFailedPortEntry->u4PortId !=
                       pCnPortTblEntry->u4PortId))
            {
                u4PortId = pCnFailedPortEntry->u4PortId;
                CN_PORT_PRI_ENTRY (u4CompId, u4PortId, u4CompPriority,
                                   pCnPortPriEntry);
                if (pCnPortPriEntry != NULL)
                {
                    RBTreeRem (gCnMasterTblInfo.CpIdTbl,
                               (tCnPortPriTblInfo *) pCnPortPriEntry);
                    MemReleaseMemBlock (gCnMasterTblInfo.PortPriTblPoolId,
                                        (UINT1 *) pCnPortPriEntry);
                    pCnFailedPortEntry->paPortPriTbl[u4CompPriority] = NULL;
                }
                pCnTempFailedPortEntry = pCnFailedPortEntry;
                pCnFailedPortEntry = (tCnPortTblInfo *) RBTreeGetNext
                    (pCnCompTblEntry->PortTbl, pCnTempFailedPortEntry, NULL);
            }

            CN_TRC_ARG2 (u4CompId, (CN_ALL_FAILURE_TRC | CN_RESOURCE_TRC),
                         "CnUtilCreateCompPriEntry: CnUtilCreatePortPriEntry "
                         "FAILED for Port %u, CNPV %u\r\n",
                         pCnPortTblEntry->u4PortId, u4CompPriority);

            /*Remove CompPriEntry */
            MemReleaseMemBlock (gCnMasterTblInfo.CompPriTblPoolId,
                                (UINT1 *) pCnCompPriEntry);
            pCnCompTblEntry->pCompPriTbl[u4CompPriority] = NULL;
            return OSIX_FAILURE;
        }
        pCnTempPortTblEntry = pCnPortTblEntry;
        pCnPortTblEntry = (tCnPortTblInfo *) RBTreeGetNext
            (pCnCompTblEntry->PortTbl, pCnTempPortTblEntry, NULL);
        u1ErroredPortFlag = OSIX_FALSE;
    }
    return OSIX_SUCCESS;
}

/*******************************************************************************
 * *  Function Name   : CnUtilCreatePortPriEntry
 * *  Description     : This function allocates memory for the
 * *                    Port priority table and sets the properties of port
 * *                     priority table
 * *  Input(s)        :  pCnPortTblEntry         - PortTblEntry
 *                       u4CompPriority          - CNPV
 *                       u4CompAltPri            - Component Priority's 
 *                                                 Alternate Priority
 * *                     u1ComPriDefModeChoice   - Component Priority
 *                                                 Defense Mode Choice
 *                       u1ErroredPortFlag         - Flag to specify whether or 
 *                                                 not to calculate erroredport
 *                                                 entry
 * *  Output(s)       : Port Priority Entries are created
 * *  Returns         : OSIX_SUCCESS / OSIX_FAILURE
 * ****************************************************************************/
INT4
CnUtilCreatePortPriEntry (tCnPortTblInfo * pCnPortTblEntry,
                          UINT4 u4CompPriority, UINT1 u1CompPriDefModeChoice,
                          UINT4 u4CompAltPri, UINT1 u1ErroredPortFlag)
{
    tCnPortPriTblInfo  *pCnPortPriEntry = NULL;
    UINT1              *pu1CPID = NULL;
    UINT1              *pau1PortMac = NULL;
    tCfaIfInfo          IfInfo;
    UINT4               u4TrafficClass = CN_ZERO;
    UINT4               u4RetValue = 0;

    UNUSED_PARAM (u4RetValue);

    pCnPortPriEntry = (tCnPortPriTblInfo *) MemAllocMemBlk
        (gCnMasterTblInfo.PortPriTblPoolId);

    if (pCnPortPriEntry == NULL)
    {
        CN_TRC_ARG2 (pCnPortTblEntry->u4ComponentId,
                     (CN_ALL_FAILURE_TRC | CN_RESOURCE_TRC),
                     "CnUtilCreatePortPriEntry: Memory Allocation FAILED for port %d"
                     " CNPV %d\r\n", pCnPortTblEntry->u4PortId, u4CompPriority);
        return OSIX_FAILURE;
    }

    /*Assign address of PortPriEntry in PortTbl */
    pCnPortTblEntry->paPortPriTbl[u4CompPriority] = pCnPortPriEntry;
    pCnPortTblEntry->TlvTxDelayTmrNode.u1Status = CN_TMR_NOT_RUNNING;

    pCnPortPriEntry->u4CnComPriComponentId = pCnPortTblEntry->u4ComponentId;
    pCnPortPriEntry->u4PortId = pCnPortTblEntry->u4PortId;
    pCnPortPriEntry->u1CNPV = (UINT1) u4CompPriority;

    /*Get Traffic class from QoS and fill it */
    if (CnPortGetTctoPriority (pCnPortPriEntry->u1CNPV,
                               pCnPortPriEntry->u4PortId,
                               &u4TrafficClass) == OSIX_FAILURE)
    {
        CN_TRC_ARG2 (pCnPortTblEntry->u4ComponentId, CN_ALL_FAILURE_TRC,
                     "CnUtilCreatePortPriEntry: FAILED, "
                     "CnPortGetTctoPriority FAILED for Port %d, "
                     "CNPV %u\r\n", pCnPortPriEntry->u4PortId,
                     pCnPortPriEntry->u1CNPV);
        CLI_SET_ERR (CN_CLI_ERROR_GET_TC_FAILED);

        MemReleaseMemBlock (gCnMasterTblInfo.PortPriTblPoolId,
                            (UINT1 *) pCnPortPriEntry);
        pCnPortTblEntry->paPortPriTbl[u4CompPriority] = NULL;

        return OSIX_FAILURE;
    }
    pCnPortPriEntry->u4TrafficClass = u4TrafficClass;

    /*Form CPID for the entry */
    if (CnPortCfaGetIfInfo (pCnPortTblEntry->u4PortId, &IfInfo) == OSIX_FAILURE)
    {
        CN_TRC_ARG1 (pCnPortTblEntry->u4ComponentId,
                     (CN_MGMT_TRC | CN_ALL_FAILURE_TRC),
                     "CnUtilActivateCNPV: MAC Address not retrieved"
                     "from CFA for port %d \r\n", pCnPortTblEntry->u4PortId);
        MemReleaseMemBlock (gCnMasterTblInfo.PortPriTblPoolId,
                            (UINT1 *) pCnPortPriEntry);
        pCnPortTblEntry->paPortPriTbl[u4CompPriority] = NULL;

        return OSIX_FAILURE;
    }

    pu1CPID = pCnPortPriEntry->au1CnCpIdentifier;
    pau1PortMac = IfInfo.au1MacAddr;
    CN_FORM_CPID (pau1PortMac, pCnPortPriEntry->u1CNPV, pu1CPID);

    pCnPortPriEntry->u1PortPriDefModeChoice = CN_COMP;
    if (u1CompPriDefModeChoice == CN_ADMIN)
    {
        pCnPortPriEntry->bIsAdminDefMode = OSIX_TRUE;
    }
    else
    {
        pCnPortPriEntry->bIsAdminDefMode = OSIX_FALSE;
    }
    pCnPortPriEntry->u1PortPriAdminDefMode = CN_DISABLED;

    /*Calculate the Oper Admin Defense Mode */
    CnUtilCnpdAdminDefenseMode (pCnPortPriEntry);

    /*Assign default values to port Priority entries */
    pCnPortPriEntry->u1PortPriLldpInstChoice = CN_LLDP_COMP;
    pCnPortPriEntry->u4PortPriLldpInstSelector = CN_DEF_PORT_LLDP_INST_SELECTOR;
    pCnPortPriEntry->u1PortPriAdminAltPri = CN_ZERO;
    pCnPortPriEntry->u1CnCpPriority = ((UINT1) u4CompPriority);
    pCnPortPriEntry->i1CnCpFeedbackWeight = CN_DEFAULT_FEEDBACK_WEIGHT;
    pCnPortPriEntry->u1CpMinHeaderOctets = CN_DEFAULT_HEADER_OCTETS;
    pCnPortPriEntry->u4CpQSizeSetPoint = CN_DEFAULT_QUEUE_SET_PT;
    pCnPortPriEntry->u4CpMinSampleBase = CN_DEFAULT_MIN_SAMPLE_BASE;
    pCnPortPriEntry->bIsErrorPortEntry = CN_ERROR_PORT_ENTRY_RESET;

    /* Setting  Oper defence mode to Disabled.
     * This will be used in state machine to 
     * Understand its getting called first time */

    pCnPortPriEntry->u1PortPriOperDefMode = CN_DISABLED;
    /*Calculate the Oper Alternate Priority */
    CnUtilCalculateOperAltPri (pCnPortPriEntry);

    if (u1ErroredPortFlag == OSIX_TRUE)
    {
        /* Check for errored port entry */
        CnUtilCalculateErroredPortEntry (pCnPortTblEntry->u4ComponentId,
                                         u4CompPriority,
                                         u4CompAltPri, CN_CNPV_CREATE);
    }
    /* add an entry in the CnCpidToIfIfIndex Table */
    u4RetValue =
        RBTreeAdd (gCnMasterTblInfo.CpIdTbl,
                   (tCnPortPriTblInfo *) pCnPortPriEntry);

    return OSIX_SUCCESS;
}

/******************************************************************************
 *  Function Name   : CnUtilActivateCNPV
 *  Description     : This function calls NPAPI and state machine function for 
 *                    all ports in the component
 *  Input(s)        :  pCnCompTblEntry - Component Table Entry
 *                     u4Priority      - CNPV
 *  Output(s)       : NPAPI and State Machine functions are activated for the
 *                    CNPV for all ports in the component
 *  Returns         : OSIX_SUCCESS / OSIX_FAILURE
 * ***************************************************************************/
INT4
CnUtilActivateCNPV (tCnCompTblInfo * pCnCompTblEntry, UINT4 u4Priority)
{
    tCnPortTblInfo     *pCnPortTblEntry = NULL;
    tCnPortTblInfo     *pCnTempPortTblEntry = NULL;
    tCnPortPriTblInfo  *pCnPortPriEntry = NULL;
    UINT4               u4PortId = CN_ZERO;

    pCnPortTblEntry =
        (tCnPortTblInfo *) RBTreeGetFirst (pCnCompTblEntry->PortTbl);

    while (pCnPortTblEntry != NULL)
    {
        u4PortId = pCnPortTblEntry->u4PortId;
        CN_PORT_PRI_ENTRY (pCnCompTblEntry->u4CompId, u4PortId, u4Priority,
                           pCnPortPriEntry);

        if (pCnPortPriEntry != NULL)
        {
            CnUtilEnableCnpv (pCnCompTblEntry, pCnPortTblEntry,
                              pCnPortPriEntry);
        }
        pCnTempPortTblEntry = pCnPortTblEntry;
        pCnPortTblEntry = (tCnPortTblInfo *) RBTreeGetNext
            (pCnCompTblEntry->PortTbl, pCnTempPortTblEntry, NULL);
    }
    return OSIX_SUCCESS;
}

/******************************************************************************
*  Function Name   : CnUtilDeActivateCNPV
*  Description     : This function disables the state machine for all
*                    ports in the component
*  Input(s)        :  pCnCompTblEntry - Component Table Entry
*                     u4Priority      - CNPV
*  Output(s)       : State Machine functions are Deactivated for the CNPV for
*                    all ports in the component
*  Returns         : OSIX_SUCCESS / OSIX_FAILURE
*******************************************************************************/
VOID
CnUtilDeActivateCNPV (tCnCompTblInfo * pCnCompTblEntry, UINT4 u4Priority)
{
    tCnPortTblInfo     *pCnPortTblEntry = NULL;
    tCnPortTblInfo     *pCnTempPortTblEntry = NULL;
    tCnPortPriTblInfo  *pCnPortPriEntry = NULL;
    UINT4               u4PortId = CN_ZERO;
    UINT4               u4CompId = pCnCompTblEntry->u4CompId;

    if (pCnCompTblEntry->bCnCompMasterEnable == CN_MASTER_DISABLE)
    {
        /*State Machines are already in Disabled state */
        return;
    }

    /*Disable the state machine for all ports with given
     * priority in the given component */
    pCnPortTblEntry =
        (tCnPortTblInfo *) RBTreeGetFirst (pCnCompTblEntry->PortTbl);
    while (pCnPortTblEntry != NULL)
    {
        u4PortId = pCnPortTblEntry->u4PortId;
        CN_PORT_PRI_ENTRY (u4CompId, u4PortId, u4Priority, pCnPortPriEntry);
        if (pCnPortPriEntry != NULL)
        {
            CnSmStateDisabled (pCnCompTblEntry, pCnPortTblEntry,
                               pCnPortPriEntry);
        }
        pCnTempPortTblEntry = pCnPortTblEntry;
        pCnPortTblEntry =
            (tCnPortTblInfo *) RBTreeGetNext (pCnCompTblEntry->PortTbl,
                                              pCnTempPortTblEntry, NULL);
    }
    return;
}

/*****************************************************************************
 * *  Function Name   : CnUtilDeleteCNPV
 * *  Description     : This function deallocates memory allocated for the
 * *                    component priority table and corresponding Port 
 *                      Priority table entries. This function calls the
 *                      CnUtilDeletePortPriEntry for all ports present
 *                      in the component
 * *  Input(s)        :  pCnCompTblEntry - Component Priority Entry
 *                       u4CompPriority      - CNPV
 * *  Output(s)       : Memory allocated for component Priority Entry and 
 *                      corresponding Port Priority Entries are deallocated
 * *  Returns         : OSIX_SUCCESS / OSIX_FAILURE
 * ***************************************************************************/
INT4
CnUtilDeleteCNPV (tCnCompTblInfo * pCnCompTblEntry, UINT4 u4CompPriority)
{
    tCnPortTblInfo     *pCnPortTblEntry = NULL;
    tCnPortTblInfo     *pCnTempPortTblEntry = NULL;
    tCnPortPriTblInfo  *pCnPortPriEntry = NULL;
    tCnCompPriTbl      *pCnCompPriEntry = NULL;
    UINT4               u4PortId = CN_ZERO;
    UINT4               u4CompId = pCnCompTblEntry->u4CompId;

    pCnCompPriEntry = pCnCompTblEntry->pCompPriTbl[u4CompPriority];
    if (pCnCompPriEntry != NULL)
    {

        /*Deallocate memory occupied by all ports in the component with u4CompId
         * and u4CompPriority */
        pCnPortTblEntry = (tCnPortTblInfo *) RBTreeGetFirst
            (pCnCompTblEntry->PortTbl);

        while (pCnPortTblEntry != NULL)
        {
            u4PortId = pCnPortTblEntry->u4PortId;

            CN_PORT_PRI_ENTRY (u4CompId, u4PortId, u4CompPriority,
                               pCnPortPriEntry);

            if (pCnPortPriEntry != NULL)
            {
                /* Check if there is any conflict with CP properties configured
                 * for the Traffic Class associated with this CNPV */
                CnUtilReCalculateCpProp (u4CompId, u4PortId, u4CompPriority,
                                         pCnPortPriEntry->u4TrafficClass);
                RBTreeRem (gCnMasterTblInfo.CpIdTbl,
                           (tCnPortPriTblInfo *) pCnPortPriEntry);
                MemReleaseMemBlock (gCnMasterTblInfo.PortPriTblPoolId,
                                    (UINT1 *) pCnPortPriEntry);
                pCnPortPriEntry = NULL;
                pCnPortTblEntry->paPortPriTbl[u4CompPriority] = NULL;

            }
            pCnTempPortTblEntry = pCnPortTblEntry;
            pCnPortTblEntry = (tCnPortTblInfo *) RBTreeGetNext
                (pCnCompTblEntry->PortTbl, pCnTempPortTblEntry, NULL);
        }
        /*Deallocate memory allocated to CompPriEntry */
        MemReleaseMemBlock (gCnMasterTblInfo.CompPriTblPoolId,
                            (UINT1 *) pCnCompPriEntry);
        pCnCompPriEntry = NULL;
        pCnCompTblEntry->pCompPriTbl[u4CompPriority] = NULL;

        /*Calculate Errored Port Entry */
        CnUtilCalculateErroredPortEntry
            (u4CompId, u4CompPriority, CN_ZERO, CN_CNPV_DELETE);

        /*Recalculate the Auto Alternate priority */
        CnUtilCalculateAutoAltPri (pCnCompTblEntry);
        return OSIX_SUCCESS;
    }
    return OSIX_FAILURE;
}

/******************************************************************************
 * Function Name : CnUtilReCalculateCpProp
 * Description   : This function is used to recalculate the CP properties for
 *                 a traffic class.
 * Input(s)      : u4CompId - ComponentId
 *                 u4PortId - Port Index
 *                 u4CNPV - CNPV
 * Output(s)     : The CP properties of a traffic class are recalculated if 
 *                 the CNPV to be deleted has a traffic class that is 
 *                 configured for another CNPV
 * Return(s)     : None
 * **************************************************************************/
VOID
CnUtilReCalculateCpProp (UINT4 u4CompId, UINT4 u4PortId, UINT4 u4CNPV,
                         UINT4 u4TrafficClass)
{
    UINT4               u4Priority = CN_ZERO;
    tCnPortPriTblInfo  *pCnPortPriEntry = NULL;
    tCnCompTblInfo     *pCnCompTblEntry = NULL;

    pCnCompTblEntry = CN_COMP_TBL_ENTRY (u4CompId);

    for (u4Priority = CN_ZERO; u4Priority < CN_MAX_VLAN_PRIORITY; u4Priority++)
    {
        if (u4Priority != u4CNPV)
        {
            CN_PORT_PRI_ENTRY (u4CompId, u4PortId, u4Priority, pCnPortPriEntry);
            if ((pCnPortPriEntry != NULL) &&
                (pCnPortPriEntry->u4TrafficClass == u4TrafficClass))
            {
                /*Revert the current CP properties */
                if ((pCnCompTblEntry->bCnCompMasterEnable == OSIX_TRUE) &&
                    (pCnPortPriEntry->u1PortPriOperDefMode != CN_DISABLED))
                {
                    CnHwSetCpParams (pCnPortPriEntry);
                }
            }
        }
    }
    return;
}

/******************************************************************************
* Function Name : CnUtilEnableCnpv
*
* Description   : This function is called to invoke state machine to 
*                 set oper defence mode and call corresponding NPAPI calls
*
* Input(s)      : pCnPortPriEntry - Port priority Entry
*
* Output(s)     : None
*
* Return(s)     : None
***************************************************************************/
VOID
CnUtilEnableCnpv (tCnCompTblInfo * pCnCompTblEntry,
                  tCnPortTblInfo * pCnPortTblEntry,
                  tCnPortPriTblInfo * pCnPortPriEntry)
{

    if (pCnCompTblEntry->bCnCompMasterEnable == CN_MASTER_ENABLE)
    {
        CnUtilCalculateOperAltPri (pCnPortPriEntry);
        if (pCnPortPriEntry->bIsAdminDefMode == OSIX_TRUE)
        {
            CnUtilCnpdAdminDefenseMode (pCnPortPriEntry);
            /*Call State Machine for appropriate Defense Mode */
            switch (pCnPortPriEntry->u1OperAdminDefenseMode)
            {
                case CN_DISABLED:
                    CnSmStateDisabled (pCnCompTblEntry, pCnPortTblEntry,
                                       pCnPortPriEntry);
                    break;
                case CN_EDGE:
                    CnSmStateEdge (pCnCompTblEntry, pCnPortTblEntry,
                                   pCnPortPriEntry);
                    break;
                case CN_INTERIOR:
                    CnSmStateInterior (pCnCompTblEntry, pCnPortTblEntry,
                                       pCnPortPriEntry);
                    break;
                default:        /* CN_INTERIOR_READY - Default case cannot happen,
                                   this is done to satisfy the static compilation 
                                   tools. */
                    CnSmStateInteriorReady (pCnCompTblEntry, pCnPortTblEntry,
                                            pCnPortPriEntry);
                    break;
            }
        }

        else
        {
            CnSmStateEdge (pCnCompTblEntry, pCnPortTblEntry, pCnPortPriEntry);
        }
    }

    return;
}

/******************************************************************************
* Function Name : CnUtilUpdateDefInPortPriTable
*
* Description   : This function is called to set default values of
*                 port priority table to Admin if creation object is disabled in 
*                 component table                 
*
* Input(s)      : pCnCompPriEntry - Component Priority Entry
*                 u4PortId - Port Index 
*                 u4CNPV   - CNPV
*
* Output(s)     : None
*
* Return(s)     : None
***************************************************************************/

VOID
CnUtilUpdateDefInPortPriTable (tCnCompPriTbl * pCnCompPriEntry, UINT4 u4PortId)
{

    tCnPortPriTblInfo  *pCnPortPriEntry = NULL;
    UINT4               u4CompId = pCnCompPriEntry->u4CompId;
    UINT4               u4CNPV = pCnCompPriEntry->u1CNPV;

    /* If Creation object is disabled in Component table, setting         */
    /* default defence mode choice as admin and defence mode as disabled  */

    if (pCnCompPriEntry->u1DefaultPortDefModeChoice == CN_AUTO_DISABLE)
    {
        CN_PORT_PRI_ENTRY (u4CompId, u4PortId, u4CNPV, pCnPortPriEntry);
        if (pCnPortPriEntry != NULL)
        {
            pCnPortPriEntry->u1PortPriDefModeChoice = CN_ADMIN;
            pCnPortPriEntry->u1PortPriAdminDefMode = CN_DISABLED;
            pCnPortPriEntry->bIsAdminDefMode = OSIX_TRUE;
        }
    }
    return;
}

/******************************************************************************
 * Function Name : CnUtilCheckQoSPriorityMap
 * Description   : This function is called to check if an QosMapPriorityEntry 
 *                 is found for the given priority in all ports of the given 
 *                 component
 *
 * Input(s)      : pCnCompTblEntry - pointer to component table entry
 *                 u4Priority      - CNPV
 *
 * Output(s)     : None
 *
 * Return(s)     : OSIX_SUCCESS/OSIX_FAILURE
 * ***************************************************************************/
INT4
CnUtilCheckQoSPriorityMap (tCnCompTblInfo * pCnCompTblEntry, UINT4 u4Priority)
{
    tCnPortTblInfo     *pCnPortTblEntry = NULL;
    tCnPortTblInfo     *pTempPortTblEntry = NULL;
    UINT1               u1OutPri = CN_ZERO;

    pCnPortTblEntry = (tCnPortTblInfo *) RBTreeGetFirst
        (pCnCompTblEntry->PortTbl);
    while (pCnPortTblEntry != NULL)
    {
        if (CnPortQosPriorityMapReq (pCnCompTblEntry->u4CompId,
                                     pCnPortTblEntry->u4PortId, ((UINT1)
                                                                 u4Priority),
                                     &u1OutPri,
                                     QOS_GET_PRIORITY_MAP) == OSIX_SUCCESS)
        {
            return OSIX_FAILURE;
        }
        pTempPortTblEntry = pCnPortTblEntry;
        pCnPortTblEntry = (tCnPortTblInfo *) RBTreeGetNext
            (pCnCompTblEntry->PortTbl, pTempPortTblEntry, NULL);
    }
    return OSIX_SUCCESS;
}

/******************************************************************************
 * Function Name : CnUtilCheckMaxCNPV
 * Description   : This function is called to check if already maximum 
 *                 number of CNPVs are created in the given component
 *                
 * Input(s)      : u4CompId     - Component Id
 *                
 * Output(s)     : None
 *
 * Return(s)     : OSIX_SUCCESS/OSIX_FAILURE
 ****************************************************************************/
INT4
CnUtilCheckMaxCNPV (UINT4 u4CompId)
{

    tCnCompPriTbl      *pCnCompPriEntry = NULL;
    UINT1               u1Count = CN_ZERO;
    UINT4               u4Priority = CN_ZERO;

    for (u4Priority = CN_ZERO; u4Priority < CN_MAX_VLAN_PRIORITY; u4Priority++)
    {
        CN_COMP_PRI_ENTRY (u4CompId, u4Priority, pCnCompPriEntry);
        if ((pCnCompPriEntry != NULL) &&
            (pCnCompPriEntry->u1ComPriRowStatus == CN_ACTIVE))
        {
            u1Count++;
        }
    }

    if (u1Count == CN_MAX_CNPV)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/* End of file */
