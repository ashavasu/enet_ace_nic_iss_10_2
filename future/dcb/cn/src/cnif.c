/********************************************************************
* Copyright (C) 2010 Aricent Inc . All Rights Reserved
*
* $Id: cnif.c,v 1.13 2014/03/14 12:36:59 siva Exp $
*
* Description:   This File contains procedures for
*              - Creating/ Deleting interface structures
*              - Handling interface up/down, interface parameter change
*                notifications from lower layer.
*
*********************************************************************/

#include "cninc.h"
/****************************************************************************
*
*    FUNCTION NAME    : CnIfCreate 
*
*    DESCRIPTION      : This function allocates memory for the new port entry,
*                       initializes the entry and adds it to the port table.
*
*    INPUT            : u4CompId - Component number 
*                       u4PortIndex - Port number to be created
*
*    OUTPUT           : None.
*
*    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
*
****************************************************************************/
PUBLIC INT4
CnIfCreate (UINT4 u4CompId, UINT4 u4PortIndex)
{
    tCnPortTblInfo     *pPortTblEntry = NULL;
    tCnPortTblInfo      TempPortTblEntry;
    tCnCompPriTbl      *pCompPriEntry = NULL;
    tCnCompTblInfo     *pCompTblEntry = NULL;
    tLldpAppPortMsg     LldpAppPortMsg;
    UINT4               u4Priority = CN_ZERO;
    UINT4               u4RetValue = 0;
    UINT1               au1TlvBuf[CN_TLV_LEN] = { CN_ZERO };

    UNUSED_PARAM (u4RetValue);
    MEMSET (&LldpAppPortMsg, CN_ZERO, sizeof (tLldpAppPortMsg));
    MEMSET (&TempPortTblEntry, CN_ZERO, sizeof (tCnPortTblInfo));

    TempPortTblEntry.u4PortId = u4PortIndex;

    pCompTblEntry = CN_COMP_TBL_ENTRY (u4CompId);

    if (pCompTblEntry != NULL)
    {
        pPortTblEntry = RBTreeGet (pCompTblEntry->PortTbl, &TempPortTblEntry);
        if (pPortTblEntry != NULL)
        {
            return OSIX_SUCCESS;

        }
    }
    else
    {
        CN_TRC_ARG1 (u4CompId, (CN_ALL_FAILURE_TRC | CN_MGMT_TRC),
                     "CnIfCreate: Context %d is Invalid \r\n", u4CompId);
        return OSIX_FAILURE;
    }

    if (u4PortIndex <= SYS_DEF_MAX_PHYSICAL_INTERFACES)
    {
        LldpAppPortMsg.pu1TxAppTlv = au1TlvBuf;
        CnUtilFillLldpPortMsg (&LldpAppPortMsg);
        if (CnPortPostCnMsgToLldp (CN_MSG_PORT_REG, u4PortIndex,
                                   &LldpAppPortMsg) == OSIX_FAILURE)
        {
            CN_TRC_ARG1 (u4CompId, (CN_MGMT_TRC),
                         "CnIfCreate: LLDP port Registration FAILED for "
                         "Port %d from CN\r\n", u4PortIndex);
            return OSIX_FAILURE;
        }
    }

    pPortTblEntry = (tCnPortTblInfo *)
        MemAllocMemBlk (gCnMasterTblInfo.PortTblPoolId);

    if (pPortTblEntry == NULL)
    {
        CN_TRC_ARG1 (u4CompId, (CN_RESOURCE_TRC | CN_ALL_FAILURE_TRC),
                     "CnIfCreate: Memory Allocation for Port Table FAILED for port %d\r\n",
                     u4PortIndex);
        return OSIX_FAILURE;
    }

    MEMSET (pPortTblEntry, CN_ZERO, sizeof (tCnPortTblInfo));
    MEMSET (pPortTblEntry->paPortPriTbl, CN_ZERO,
            (sizeof (tCnPortPriTblInfo *) * CN_MAX_VLAN_PRIORITY));

    pPortTblEntry->u4PortId = u4PortIndex;
    pPortTblEntry->u4ComponentId = u4CompId;

    u4RetValue = RBTreeAdd (pCompTblEntry->PortTbl, (tRBElem *) pPortTblEntry);
    /*  If CNPV is already enabled in this context 
     *  1. create port priority table entry for all cnpvs for this port
     *  2. Add port priority table enty to cpid table*/

    for (u4Priority = CN_ZERO; u4Priority < CN_MAX_VLAN_PRIORITY; u4Priority++)
    {
        CN_COMP_PRI_ENTRY (u4CompId, u4Priority, pCompPriEntry);
        if (pCompPriEntry != NULL)
        {
            if (CnUtilCreatePortPriEntry (pPortTblEntry, u4Priority,
                                          pCompPriEntry->u1ComPriDefModeChoice,
                                          pCompPriEntry->u1ComPriAltPri,
                                          OSIX_TRUE) == OSIX_FAILURE)
            {
                CN_TRC_ARG2 (u4CompId, (CN_ALL_FAILURE_TRC | CN_MGMT_TRC),
                             "CnIfCreate: Port Priority Table Entry creation Failed: "
                             "Port %d CNPV %d\r\n", u4PortIndex, u4Priority);
                return OSIX_FAILURE;
            }

            /*If creation object (u1DefaultPortDefModeChoice) is disabled,
             * default defence mode choice in port priority entry should be 
             * Admin. Calling below function to do the same*/
            CnUtilUpdateDefInPortPriTable (pCompPriEntry, u4PortIndex);

            CnUtilActivatePortInCNPV (pCompTblEntry, u4PortIndex, u4Priority);
        }
    }
    CN_TRC_ARG1 (u4CompId, CN_MGMT_TRC,
                 "CnIfCreate: Port %d Created in CN \r\n", u4PortIndex);
    return OSIX_SUCCESS;
}

/****************************************************************************
*
*    FUNCTION NAME    : CnIfDelete
*
*    DESCRIPTION      : This function Deallocates memory for the new port entry,
                        and 
                         1) Call State Machine Function to set disable in HW
                         2) Remove Port Entry from CPID Tree
                         3) Delete port priority table entry
                         4) Delete port table entry 
 
*    INPUT            : u4PortIndex - Port number to be created
*
*    OUTPUT           : None.
*
*    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
*
****************************************************************************/
PUBLIC INT4
CnIfDelete (UINT4 u4PortIndex)
{

    tCnPortTblInfo      TempPortTblEntry;
    tLldpAppPortMsg     LldpAppPortMsg;
    tCnCompTblInfo     *pCnCompTblEntry = NULL;
    tCnPortTblInfo     *pCnPortTblEntry = NULL;
    tCnPortPriTblInfo  *pCnPortPriEntry = NULL;
    UINT4               u4CompId = CN_ZERO;
    UINT4               u4Priority = CN_ZERO;
    UINT4               u4ContextId = CN_INVALID_CONTEXT;
    UINT2               u2LocalPortId = 0;
    UINT1               au1TlvBuf[CN_TLV_LEN] = { CN_ZERO };

    CnPortVcmGetCxtInfoFromIfIndex (u4PortIndex, &u4ContextId, &u2LocalPortId);

    MEMSET (&LldpAppPortMsg, CN_ZERO, sizeof (tLldpAppPortMsg));
    MEMSET (&TempPortTblEntry, CN_ZERO, sizeof (tCnPortTblInfo));
    TempPortTblEntry.u4PortId = u4PortIndex;

    for (u4CompId = CN_MIN_CONTEXT; u4CompId < CN_MAX_CONTEXTS; u4CompId++)
    {
        pCnCompTblEntry = CN_COMP_TBL_ENTRY (u4CompId);
        if (pCnCompTblEntry != NULL)
        {
            pCnPortTblEntry = RBTreeGet (pCnCompTblEntry->PortTbl,
                                         &TempPortTblEntry);
            if (pCnPortTblEntry != NULL)
            {
                break;
            }
        }
    }
    if (pCnPortTblEntry == NULL)
    {
        if (u4ContextId != CN_INVALID_CONTEXT)
        {
            CN_TRC_ARG1 (u4ContextId, (ALL_FAILURE_TRC | CN_TLV_TRC),
                         "CnIfDelete: FAILED to find Port table entry for port "
                         "%d\r\n", u4PortIndex);
        }
        return OSIX_FAILURE;
    }
    if (u4PortIndex <= SYS_DEF_MAX_PHYSICAL_INTERFACES)
    {
        LldpAppPortMsg.pu1TxAppTlv = au1TlvBuf;
        CnUtilFillLldpPortMsg (&LldpAppPortMsg);
        if (CnPortPostCnMsgToLldp (CN_MSG_PORT_DEREG, u4PortIndex,
                                   &LldpAppPortMsg) == OSIX_FAILURE)
        {
            CN_TRC_ARG1 (u4CompId, (ALL_FAILURE_TRC | CN_TLV_TRC),
                         "CnIfDelete: LLDP port UnRegistration FAILED for "
                         "Port %d\r\n", u4PortIndex);
            return OSIX_FAILURE;
        }
    }
    for (u4Priority = CN_ZERO; u4Priority < CN_MAX_VLAN_PRIORITY; u4Priority++)
    {
        pCnPortPriEntry = pCnPortTblEntry->paPortPriTbl[u4Priority];

        if (pCnPortPriEntry != NULL)
        {
            CnSmStateDisabled (pCnCompTblEntry, pCnPortTblEntry,
                               pCnPortPriEntry);
            RBTreeRem (gCnMasterTblInfo.CpIdTbl,
                       (tCnPortPriTblInfo *) pCnPortPriEntry);
            MemReleaseMemBlock (gCnMasterTblInfo.PortPriTblPoolId,
                                (UINT1 *) pCnPortPriEntry);
        }
    }

    /*Delete port table entry */
    if (pCnPortTblEntry->TlvTxDelayTmrNode.u1Status == CN_TMR_RUNNING)
    {
        TmrStopTimer (gCnMasterTblInfo.TmrListId,
                      &(pCnPortTblEntry->TlvTxDelayTmrNode.CnTmrBlk.TimerNode));
    }

    RBTreeRem (pCnCompTblEntry->PortTbl, pCnPortTblEntry);

    MemReleaseMemBlock (gCnMasterTblInfo.PortTblPoolId,
                        (UINT1 *) pCnPortTblEntry);

    CN_TRC_ARG1 (u4CompId, CN_MGMT_TRC,
                 "CnIfDelete: Port %d Deleted from CN\r\n", u4PortIndex);
    return OSIX_SUCCESS;
}

/****************************************************************************
*
*    FUNCTION NAME    : CnCxtCreateContext
*
*    DESCRIPTION      : This function allocates memory for the new context
                         entry and initializes the entry. 
*
*    INPUT            : u4ComponentId - Component number
*                       
*    OUTPUT           : None.
*
*    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
*
****************************************************************************/
PUBLIC INT4
CnCxtCreateContext (UINT4 u4ComponentId)
{

    tCnCompTblInfo     *pCompEntry = NULL;

    if (CN_IS_VALID_COMP_ID (u4ComponentId) == OSIX_FALSE)
    {
        CN_TRC_ARG1 (u4ComponentId, (CN_ALL_FAILURE_TRC | CN_CONTROL_PLANE_TRC),
                     "CnCxtCreateContext: Context %d is invalid",
                     u4ComponentId);
        return OSIX_FAILURE;
    }

    pCompEntry = CN_COMP_TBL_ENTRY (u4ComponentId);

    /* pCompEntry is not NULL, context with same index already exists */
    if (pCompEntry != NULL)
    {
        CN_TRC (u4ComponentId, (CN_ALL_FAILURE_TRC | CN_MGMT_TRC),
                "CnCxtCreateContext: Context Creation in CN FAILED."
                " Entry Exists !!! \r\n");
        return OSIX_SUCCESS;
    }

    /* Allocate memory for the new component Entry */
    if ((pCompEntry =
         (tCnCompTblInfo *) MemAllocMemBlk (gCnMasterTblInfo.
                                            CompTblPoolId)) == NULL)
    {
        CN_TRC (u4ComponentId, (CN_RESOURCE_TRC | CN_ALL_FAILURE_TRC),
                "CnCxtCreateContext: Memory Allocation FAILED for"
                " Context Table in CN\r\n");
        return OSIX_FAILURE;
    }
    MEMSET (pCompEntry, CN_ZERO, sizeof (tCnCompTblInfo));
    MEMSET (pCompEntry->pCompPriTbl, CN_ZERO,
            (sizeof (tCnCompPriTbl *) * CN_MAX_VLAN_PRIORITY));
    gCnMasterTblInfo.ppCompTblInfo[u4ComponentId] = pCompEntry;

    /*Create RB tree for Port Table */
    CnUtilCreatePortTblRBTree (u4ComponentId);

    pCompEntry->u4CompId = u4ComponentId;

    pCompEntry->u1CnmTransmitPriority = CN_DEFAULT_CNM_PRI;

    pCompEntry->u4TraceLevel = CN_DEFAULT_TRACE_LEVEL;

    pCompEntry->u4TlvErrors = CN_ZERO;

    pCompEntry->bCnCompMasterEnable = CN_DEFAULT_MODULE_STATUS;

    /* Configure CNM transmit priority in hardware. */

    CnHwSetCnmPri (u4ComponentId, CN_DEFAULT_CNM_PRI);

    CN_TRC_ARG1 (u4ComponentId, CN_MGMT_TRC,
                 "CnCxtCreateContext: Context %d created in CN", u4ComponentId);
    return OSIX_SUCCESS;
}

/****************************************************************************
*
*    FUNCTION NAME    : CnCxtDeleteContext
*
*    DESCRIPTION      : This function Deallocates memory for the context
*
*    INPUT            : u4CompId - Component number
*
*    OUTPUT           : None.
*
*    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
*
****************************************************************************/
PUBLIC INT4
CnCxtDeleteContext (UINT4 u4CompId)
{
    tCnCompTblInfo     *pCompEntry = NULL;

    pCompEntry = CN_COMP_TBL_ENTRY (u4CompId);

    if (pCompEntry != NULL)
    {
        MemReleaseMemBlock (gCnMasterTblInfo.CompTblPoolId,
                            (UINT1 *) pCompEntry);
        gCnMasterTblInfo.ppCompTblInfo[u4CompId] = NULL;
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
*
*    FUNCTION NAME    : CnIfPortChannelReadyinHw
*
*    DESCRIPTION      : This function activate Configured cnpvs on a port channel
*                       when port channel created in hw  
*
*    INPUT            :  u4ContextId - Context Id
*                        u4PoIndex   - Port channel Index
*
*    OUTPUT           : None.
*
*    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
*
****************************************************************************/
PUBLIC INT4
CnIfPortChannelReadyinHw (UINT4 u4ContextId, UINT4 u4PoIndex)
{
    UINT4               u4Priority = CN_ZERO;
    tCnCompTblInfo     *pCnCompTblEntry = NULL;

    pCnCompTblEntry = CN_COMP_TBL_ENTRY (u4ContextId);
    if (pCnCompTblEntry == NULL)
    {
        CN_TRC_ARG1 (u4ContextId, (CN_ALL_FAILURE_TRC | CN_MGMT_TRC),
                     "CnIfPortChannelReadyinHw: Context %d is Invalid in CN\r\n",
                     u4ContextId);
        return OSIX_FAILURE;
    }
    for (u4Priority = CN_ZERO; u4Priority < CN_MAX_VLAN_PRIORITY; u4Priority++)
    {
        if (pCnCompTblEntry->pCompPriTbl[u4Priority] != NULL)
        {
            CnUtilActivatePortInCNPV (pCnCompTblEntry, u4PoIndex, u4Priority);
        }
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
*
*    FUNCTION NAME    : CnIfAddPortToLAG
*
*    DESCRIPTION      : This function update CN database when a port added to 
*                       a port channel.
*
*    INPUT            : u4CompId - Component number
*                       u4PortIndex - Port number
*                       u4PoIndex - Port Channel Index
*
*    OUTPUT           : None.
*
*    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
*
****************************************************************************/
PUBLIC INT4
CnIfAddPortToLAG (UINT4 u4CompId, UINT4 u4PortIndex, UINT4 u4PoIndex)
{
    tCnPortTblInfo      TempPortTblEntry;
    tCnCompTblInfo     *pCnCompTblEntry = NULL;
    tLldpAppPortMsg     LldpAppPortMsg;
    tCnPortTblInfo     *pCnPortTblEntry = NULL;
    UINT1               au1TlvBuf[CN_TLV_LEN] = { CN_ZERO };

    MEMSET (&LldpAppPortMsg, CN_ZERO, sizeof (tLldpAppPortMsg));
    MEMSET (&TempPortTblEntry, CN_ZERO, sizeof (tCnPortTblInfo));

    /*  Since port is getting mapped to LA ,          */
    /*  1. Deleting port and port priority entry      */
    /*  2. Calling LLDP with LA Inedex to update TLV  */

    pCnCompTblEntry = CN_COMP_TBL_ENTRY (u4CompId);
    if (pCnCompTblEntry == NULL)
    {
        CN_TRC_ARG1 (u4CompId, (CN_ALL_FAILURE_TRC | CN_MGMT_TRC),
                     "CnIfAddPortToLAG:  Context %d is Invalid in CN",
                     u4CompId);
        return OSIX_FAILURE;
    }
    TempPortTblEntry.u4PortId = u4PoIndex;
    pCnPortTblEntry = RBTreeGet (pCnCompTblEntry->PortTbl, &TempPortTblEntry);
    if (pCnPortTblEntry == NULL)
    {
        CN_TRC_ARG1 (u4CompId, (CN_ALL_FAILURE_TRC),
                     "CnIfAddPortToLAG: FAILED to get Port Table Entry in CN for port "
                     "%d \r\n", u4PortIndex);
        return OSIX_FAILURE;

    }

    LldpAppPortMsg.pu1TxAppTlv = au1TlvBuf;
    CnUtilFillLldpPortMsg (&LldpAppPortMsg);
    if (CnPortPostCnMsgToLldp (CN_MSG_PORT_REG, u4PortIndex, &LldpAppPortMsg)
        == OSIX_FAILURE)
    {
        CN_TRC_ARG1 (u4CompId, (CN_ALL_FAILURE_TRC | CN_CONTROL_PLANE_TRC),
                     "CnIfAddPortToLAG: LLDP Reg FAILED for port %d in CN\r\n",
                     u4PortIndex);
        return OSIX_FAILURE;
    }

    /*Update LLDP with updated TLV with port-channel index */
    CnUtilTlvConstruct (pCnPortTblEntry, CN_MSG_PORT_UPDATE);

    return OSIX_SUCCESS;
}

/****************************************************************************
*
*    FUNCTION NAME    : CnIfDelPortFromLAG
*
*    DESCRIPTION      : This function update CN database when
*                        port deleted from a port channel
*
*    INPUT            : u4PortIndex - Port Index
*                       u4PoIndex - Port Channel Index
*
*    OUTPUT           : None.
*
*    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
*
****************************************************************************/
PUBLIC INT4
CnIfDelPortFromLAG (UINT4 u4PortIndex, UINT4 u4PoIndex)
{
    UINT4               u4ContextId = CN_ZERO;
    UINT2               u2LocalPortId = CN_ZERO;
    tLldpAppPortMsg     LldpAppPortMsg;
    UINT1               au1TlvBuf[CN_TLV_LEN] = { CN_ZERO };

    UNUSED_PARAM (u4PoIndex);
    MEMSET (&LldpAppPortMsg, CN_ZERO, sizeof (tLldpAppPortMsg));

    /*UnRegister from LLDP and create port entry in CN */

    LldpAppPortMsg.pu1TxAppTlv = au1TlvBuf;
    CnUtilFillLldpPortMsg (&LldpAppPortMsg);

    /* Getting Context for given Port */
    /* If port is not mapped to context,  do not create port entry in CN */

    if (CnPortVcmGetCxtInfoFromIfIndex (u4PortIndex, &u4ContextId,
                                        &u2LocalPortId) == OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    if (CnPortPostCnMsgToLldp (CN_MSG_PORT_DEREG, u4PortIndex, &LldpAppPortMsg)
        == OSIX_FAILURE)
    {
        CN_TRC_ARG1 (u4ContextId, (CN_ALL_FAILURE_TRC | CN_MGMT_TRC),
                     "CnIfDelPortFromLAG: LLDP UnRegistation for port %d "
                     "FAILED in CN\r\n", u4PortIndex);
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
*
*    FUNCTION NAME    : CnIfCreateAllPorts
*
*    DESCRIPTION      : This function creates all valid ports in CN module.
*
*    INPUT            : u4CompId - Component ID
*
*    OUTPUT           : None
*
*    RETURNS          : None
*
****************************************************************************/
PUBLIC VOID
CnIfCreateAllPorts (UINT4 u4CompId)
{
    /* Scaling no of ports in the system will increase the size of tPortList,
     * which affects the process's stack size. so declaring this variable 
     * as static. */
    static tPortList    IfPortList;
    UINT4               u4Port = CN_ZERO;
    UINT2               u2PortIndex = CN_ZERO;
    UINT2               u2BitIndex = CN_ZERO;
    UINT1               u1PortFlag = CN_ZERO;

    MEMSET (&IfPortList, CN_ZERO, sizeof (tPortList));
    /* CnPortGetNextValidPort retrun success only for SI case */
    if (CnPortVcmGetContextPortList (u4CompId, IfPortList) == OSIX_SUCCESS)
    {

        for (u2PortIndex = CN_ZERO; u2PortIndex < CN_IFPORT_LIST_SIZE;
             u2PortIndex++)
        {
            u1PortFlag = IfPortList[u2PortIndex];
            for (u2BitIndex = CN_ZERO; ((u2BitIndex < CN_PORTS_PER_BYTE)
                                        && (u1PortFlag != CN_ZERO));
                 u2BitIndex++)
            {
                if ((u1PortFlag & CN_BIT8) != CN_ZERO)
                {
                    u4Port = (UINT4) ((u2PortIndex * CN_PORTS_PER_BYTE) +
                                      u2BitIndex + CN_ONE);

                    /* Invoke interface creation only if the physical 
                     * port is not part of port channel. */

                    if (CnIfCreate (u4CompId, u4Port) == OSIX_FAILURE)
                    {
                        CN_TRC_ARG1 (u4CompId,
                                     (CN_ALL_FAILURE_TRC),
                                     "CnIfCreateAllPorts: Port %lu"
                                     "Creation Faild in CN\r\n", u4Port);
                    }
                    else
                    {
                        CN_TRC_ARG1 (u4CompId, CN_CONTROL_PLANE_TRC,
                                     "CnIfCreateAllPorts: Port %lu "
                                     "Creation SUCCESS in CN\r\n", u4Port);
                    }
                }
                u1PortFlag = (UINT1) (u1PortFlag << CN_ONE);
            }                    /* FOR LOOP 2 END */

        }                        /* FOR LOOP 1 END */
    }
    return;
}

/*--------------------------------------------------------------------------*/
/*                       End of the file  cnif.c                            */
/*--------------------------------------------------------------------------*/
