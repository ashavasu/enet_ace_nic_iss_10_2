##########################################################################
# Copyright (C) 2010 Aricent Inc . All Rights Reserved                   #
# ------------------------------------------                             #
# $Id: make.h,v 1.5 2016/03/05 12:17:46 siva Exp $ #
# DESCRIPTION  : make.h file for DCBX                                    #
##########################################################################

# Set the PROJ_BASE_DIR as the directory where you untar the project files
PROJECT_NAME  = AricentDCBX
PROJECT_BASE_DIR = ${BASE_DIR}/dcb/dcbx
PROJECT_SOURCE_DIR = ${PROJECT_BASE_DIR}/src
PROJECT_INCLUDE_DIR = ${PROJECT_BASE_DIR}/inc
PROJECT_OBJECT_DIR = ${PROJECT_BASE_DIR}/obj
FUTURE_INC_DIR  = $(BASE_DIR)/inc

# Specify the project include directories and dependencies
PROJECT_INCLUDE_FILES  = \
    $(PROJECT_INCLUDE_DIR)/fsdcbxdb.h   \
    $(PROJECT_INCLUDE_DIR)/fsdcbxlw.h   \
    $(PROJECT_INCLUDE_DIR)/fsdcbxwr.h   \
    $(PROJECT_INCLUDE_DIR)/stddcbdb.h   \
    $(PROJECT_INCLUDE_DIR)/stddcblw.h   \
    $(PROJECT_INCLUDE_DIR)/stddcbwr.h   \
    $(PROJECT_INCLUDE_DIR)/dcbxdefn.h   \
    $(PROJECT_INCLUDE_DIR)/dcbxglob.h   \
    $(PROJECT_INCLUDE_DIR)/dcbxinc.h    \
    $(PROJECT_INCLUDE_DIR)/dcbxtrap.h   \
    $(PROJECT_INCLUDE_DIR)/dcbxprot.h   \
    $(PROJECT_INCLUDE_DIR)/dcbxtdfs.h   \
    $(PROJECT_INCLUDE_DIR)/dcbxsz.h     \
    $(PROJECT_INCLUDE_DIR)/etssz.h      \
    $(PROJECT_INCLUDE_DIR)/pfcsz.h      \
    $(PROJECT_INCLUDE_DIR)/appsz.h      \
    $(PROJECT_INCLUDE_DIR)/ceesz.h      \
    $(PROJECT_INCLUDE_DIR)/dcbxtrc.h    \
    $(PROJECT_INCLUDE_DIR)/ceedefn.h 


PROJECT_FINAL_INCLUDES_DIRS =  -I$(PROJECT_INCLUDE_DIR) \
                   -I$(COMMON_INCLUDE_DIRS) \
                   -I$(FUTURE_INC_DIR)

PROJECT_FINAL_INCLUDE_FILES    +=  $(PROJECT_INCLUDE_FILES)

PROJECT_DEPENDENCIES = $(COMMON_DEPENDENCIES)         \
                       $(PROJECT_FINAL_INCLUDE_FILES) \
                       $(PROJECT_BASE_DIR)/Makefile   \
                       $(PROJECT_BASE_DIR)/make.h

