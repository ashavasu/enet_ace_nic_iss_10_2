/*************************************************************************
 * Copyright (C) 2010 Aricent Inc . All Rights Reserved
 * $Id: dcbxprot.h,v 1.17 2017/01/25 13:19:41 siva Exp $
 * Description: This header file contains all the prototypes 
 *              declaration for DCBX Module.
****************************************************************************/
#ifndef __DCBX_PROTO_H__
#define __DCBX_PROTO_H__

/* DCBX Main Function Prototype */

PUBLIC INT4 DcbxMainTaskInit PROTO ((VOID));
PUBLIC VOID DcbxMainTaskDeInit PROTO ((VOID));
PUBLIC VOID DcbxMainProcessEvents PROTO ((UINT4 u4Events));
PUBLIC INT4 DcbxMainModuleStart PROTO ((VOID));
PUBLIC VOID DcbxMainModuleShutDown PROTO ((VOID));
PUBLIC INT4 ETSMainModuleStart PROTO ((VOID));
PUBLIC VOID ETSMainModuleShutDown PROTO ((VOID));
PUBLIC INT4 PFCMainModuleStart PROTO ((VOID));
PUBLIC VOID PFCMainModuleShutDown PROTO ((VOID));
PUBLIC VOID DcbxMainAssignMempoolIds PROTO ((VOID));
PUBLIC VOID ETSMainAssignMempoolIds PROTO ((VOID));
PUBLIC VOID PFCMainAssignMempoolIds PROTO ((VOID));
PUBLIC INT4 AppPriMainModuleStart PROTO ((VOID));
PUBLIC VOID AppPriMainModuleShutDown PROTO ((VOID));
PUBLIC VOID AppPriMainAssignMempoolIds PROTO ((VOID));

/* DCBX Queue Function Prototype */
PUBLIC INT4 DcbxQueEnqMsg PROTO ((tDcbxQueueMsg *pMsg));
PUBLIC VOID DcbxQueMsgHandler PROTO ((VOID));
PUBLIC VOID DcbxQueProcessApplTlvMsg PROTO ((tDcbxQueueMsg *pMsg));
PUBLIC VOID DcbxQueProcessReRegMsg PROTO ((tDcbxQueueMsg *pMsg));

/* DCBX Sem Function Prototype */
PUBLIC INT4 DcbxSemUpdate PROTO ((UINT4 u4IfIndex,
                          tDcbxAppRegInfo *pDcbxAppPortMsg ));

/* DCBX API Function Prototype */
PUBLIC INT4 DcbxApiApplPortUpdate PROTO ((UINT4 u4IfIndex,
                                   tDcbxAppRegInfo *pDcbxAppPortMsg));
PUBLIC INT4 DcbxApiApplPortReg PROTO ((UINT4 u4IfIndex,tDcbxAppRegInfo *pDcbxAppPortMsg));
PUBLIC INT4 DcbxApiApplPortDeReg PROTO ((UINT4 u4IfIndex,
                                  tDcbxAppRegInfo *pDcbxAppPortMsg));

/* DCBX PORT Function Prototype */
PUBLIC INT4 DcbxPortGetNextValidPort PROTO ((UINT2 u2PrevPort, UINT2 *pu2PortIndex));
PUBLIC INT4 DcbxPortNotifyFaults 
            PROTO ((tSNMP_VAR_BIND * pTrapMsg, UINT1 *pu1Msg, UINT4 u4ModuleId));
PUBLIC INT4 DcbxPortL2IwfApplPortRequest PROTO ((UINT4 u4IfIndex,
                        tLldpAppPortMsg *pLldpAppPortMsg,
                        UINT1 u1MsgType));
PUBLIC INT4 DcbxPortL2IwfApplRequest PROTO ((tL2LldpAppInfo *pL2LldpAppInfo,
                        UINT1 u1MsgType));
PUBLIC INT4 DcbxPortCfaGetIfMacAddr PROTO ((UINT4 u4IfIndex,UINT1 *pu1MacAddr));

/*DCBX RM PORTING FUNCTIONS*/
PUBLIC UINT4 DcbxPortRmRegisterProtocols PROTO ((tRmRegParams * pRmReg));

PUBLIC UINT4 DcbxPortRmDeRegisterProtocols PROTO ((VOID));

PUBLIC UINT4 DcbxPortRmReleaseMemoryForMsg PROTO ((UINT1 *pu1Block));

PUBLIC UINT4 DcbxPortRmSetBulkUpdatesStatus PROTO ((VOID));

PUBLIC UINT1 DcbxPortRmGetStandbyNodeCount PROTO ((VOID));

PUBLIC UINT4 DcbxPortRmApiHandleProtocolEvent PROTO ((tRmProtoEvt * pEvt));

PUBLIC UINT4 DcbxPortRmGetNodeState PROTO ((VOID));

PUBLIC INT4 DcbxPortRmApiSendProtoAckToRM PROTO ((tRmProtoAck * pProtoAck));
PUBLIC UINT4 DcbxPortRmEnqMsgToRm PROTO ((tRmMsg * pRmMsg, UINT2 u2DataLen,
                                          UINT4 u4SrcEntId, UINT4 u4DestEntId));

/* CEE related function prototypes */
PUBLIC VOID DcbxUtlGetPFCTxStatus PROTO ((UINT4 u4PortNumber, UINT1 *pu1TxStatus));
PUBLIC VOID DcbxUtlGetETSTxStatus PROTO ((UINT4 u4PortNumber, UINT1 *pu1ETSTxStatus));
PUBLIC VOID DcbxUtlGetAppPriTxStatus PROTO ((UINT4 u4PortNumber, UINT1 *pu1TxStatus));

PUBLIC VOID DcbxUtlGetPFCAdminMode PROTO ((UINT4 u4IfIndex, UINT1 *pu1PFCAdminMode));
PUBLIC VOID DcbxUtlGetETSAdminMode PROTO ((UINT4 u4IfIndex, UINT1 *pu1ETSAdminMode));
PUBLIC VOID DcbxUtlGetAppPriAdminMode PROTO ((UINT4 u4IfIndex, UINT1 *pu1AppPriAdminMode));

PUBLIC VOID CEEFormAppPriTLV PROTO ((UINT1 *pPu1Buf, UINT4 u4IfIndex, UINT1 u1OperVer, UINT1 u1MaxVer, UINT1 *pApTblLen));
PUBLIC VOID CEEFormPFCTLV PROTO ((UINT1 *pPu1Buf, UINT4 u4IfIndex, UINT1 u1OperVer, UINT1 u1MaxVer));
PUBLIC VOID CEEFormPriGrpTlv PROTO ((UINT1 *pPu1Buf, UINT4 u4IfIndex, UINT1 u1OperVer, UINT1 u1MaxVer));
PUBLIC VOID CEEFormAndSendTLV PROTO ((UINT4 u4IfIndex, UINT1 u1MsgType, UINT1 u1DeRegFeatType));
PUBLIC VOID CEEPostMsgToDCBX PROTO ((UINT1 u1MsgType, UINT4 u4IfIndex,
                         tDcbxAppRegInfo * pDcbxAppPortMsg));
PUBLIC VOID CEEConstructTLVHeader PROTO ((UINT1  *pPu1Buf));
PUBLIC VOID CEERegisterApplForPort PROTO ((UINT4 u4IfIndex, UINT1 u1FeatType));
PUBLIC VOID CEEDeRegisterApplForPort PROTO ((UINT4 u4IfIndex, UINT1 u1FeatType));
PUBLIC VOID CEEGetDCBxMode PROTO ((UINT4 u4IfIndex, UINT1 *pu1DcbxMode));
PUBLIC VOID DcbxUtlGetOperVersion PROTO ((UINT4 u4IfIndex, UINT1 *pu1DcbxVersion));
PUBLIC VOID CEEHandleReRegReqFromDCBX PROTO ((UINT4 u4IfIndex));
PUBLIC VOID DcbxUtlGetDCBXMode PROTO ((UINT4 u4IfIndex, UINT1 *pu1DcbxMode));

PUBLIC UINT1 CEEFormControlTLV PROTO ((UINT1 *pPu1Buf, UINT4 u4IfIndex, UINT1 u1OperVer, UINT1 u1MaxVer));
PUBLIC UINT1 DcbxGetOperVersion PROTO ((UINT4 u4IfIndex));

PUBLIC INT4 DcbxUtlHandleVerResolution PROTO ((tDcbxQueueMsg * pMsg));
PUBLIC INT4 DcbxUtlSwitchVersion PROTO ((UINT1 u1SwitchToVersion, UINT4 u4IfIndex));    
PUBLIC VOID DcbxUtlSwitchMode PROTO ((UINT1 u1NewMode, UINT1 u1PrevMode, UINT4 u4IfIndex));    
PUBLIC VOID CEEUtlHandlePktConfigErrors PROTO ((UINT1 u1TlvType, tCeeTLVInfo CeeTLVInfo, INT4 i4Event));
PUBLIC INT4 CEEUtlValidateAndDropTLV PROTO ((UINT4 u4IfIndex, tApplTlvParam * pApplTlvParam));
PUBLIC tDcbxAppEntry *DcbxUtlGetAppEntryBasedOnMode PROTO ((UINT1 u1Mode, UINT4 u4IfIndex, UINT1 u1TlvType));
PUBLIC VOID DcbxUtlHandleSwitchModeChg PROTO ((UINT1 u1PrevVer, UINT4 u4IfIndex));
PUBLIC VOID DcbxUtlCallCEEAgeOut PROTO ((UINT4 u4IfIndex));
PUBLIC VOID DcbxPktDump (UINT1 *pu1Buf, UINT2 u2Length, UINT1 u1PktType);
PUBLIC INT4 DcbxUtlCallIEEEAgeOut PROTO ((UINT4 u4IfIndex));

/* DCBX Utility Function Prototype*/

PUBLIC INT4 DcbxUtlCompareMacaddr PROTO ((tMacAddr addr1, tMacAddr addr2));
PUBLIC INT4 DcbxUtlPortCreate PROTO ((UINT4 u4IfIndex));
PUBLIC VOID DcbxUtlPortDelete PROTO ((UINT4 u4IfIndex));
PUBLIC VOID DcbxUtlDeletePorts PROTO ((VOID));
PUBLIC VOID DcbxUtlAddPortToLAG PROTO ((UINT4 u4PoIndex));
PUBLIC VOID DcbxUtlDelPortFromLAG PROTO ((UINT4 u4IfIndex, UINT4 u4PoIndex));
PUBLIC VOID DcbxUtlPortAdminDisable PROTO ((UINT4 u4IfIndex));
PUBLIC VOID DcbxUtlPortAdminEnable PROTO ((UINT4 u4IfIndex));
PUBLIC VOID DcbxUtlCreateAllPorts PROTO ((VOID));
PUBLIC INT4 DcbxUtlApplCmpFun PROTO ((tRBElem * pElem1, tRBElem * pElem2));
PUBLIC VOID DcbxUtlFillLldpInfo PROTO ((tDcbxAppRegInfo *pDcbxAppPortMsg,
                                tLldpAppPortMsg *pLldpAppPortMsg));
PUBLIC VOID DcbxUtlFillL2LldpInfo PROTO ((tDcbxAppRegInfo *pDcbxAppPortMsg,
                    tL2LldpAppInfo *pL2LldpAppInfo));

PUBLIC tDcbxPortEntry * DcbxUtlGetPortEntry PROTO ((UINT4 u4IfIndex ));
PUBLIC INT4 DcbxUtlValidatePort PROTO ((UINT4 u4IfIndex));
PUBLIC INT4 DcbxUtlPortTblCmpFn PROTO ((tRBElem * pRBElem1, tRBElem * pRBElem2));
PUBLIC UINT1 ETSUtlCountBitSet PROTO ((UINT1 *pu1Byte));
PUBLIC UINT4 ETSUtlValidateTCG PROTO ((UINT1 u1LocalMaxTCG,
                                UINT1 u1RemMaxTCG,
                                UINT1 *pau1EtsTCGID, UINT1 *pau1ETSBW, 
                                UINT1 *pu1EtsTsaTable));
PUBLIC UINT4 ETSUtlCompareTCG PROTO ((UINT1 *pau1AdmEtsTCGID, UINT1 *pau1RemEtsTCGID));
PUBLIC VOID DcbxUtlOsixBitlistIsBitSet PROTO ((UINT1 *au1BitArray, UINT2 u2BitNumber, 
     INT4 i4ArraySize, BOOL1 *bResult));

/* ETS Utility Function Prototype */

PUBLIC VOID ETSUtlConstructConfPreFormTlv PROTO ((VOID));
PUBLIC VOID ETSUtlConstructRecoPreFormTlv PROTO ((VOID));
PUBLIC VOID ETSUtlConstructTcSuppPreFormTlv PROTO ((VOID));
PUBLIC VOID ETSPortPostMsgToDCBX PROTO ((UINT1 u1MsgType,UINT4 u4IfIndex, 
                                  tDcbxAppRegInfo  *pDcbxAppPortMsg));
PUBLIC VOID ETSUtlHandleAgedOutFromDCBX PROTO ((tETSPortEntry *pPortEntry,
                                         UINT1 u1TlvSubType));
PUBLIC VOID ETSUtlClearRemoteParam PROTO ((tETSPortEntry *pPortEntry, 
                                   UINT1 u1TlvType));
PUBLIC VOID ETSUtlHandleReRegReqFromDCBX PROTO ((tETSPortEntry *pPortEntry,
                                          UINT1 u1TlvSubType));
PUBLIC VOID ETSUtlDeleteAllPorts PROTO ((VOID));
PUBLIC VOID ETSUtlDeletePort PROTO ((UINT4 u4IfIndex));
PUBLIC VOID ETSUtlRegisterApplForPort PROTO ((tETSPortEntry *pPortEntry, UINT1 u1TlvType));
PUBLIC VOID ETSUtlDeRegisterApplForPort PROTO ((tETSPortEntry *pPortEntry, UINT1 u1TlvType));
PUBLIC VOID ETSUtlModuleStatusChange PROTO ((UINT1 u1Status));
PUBLIC VOID ETSUtlAdminModeChange PROTO ((tETSPortEntry *pPortEntry, UINT1 u1NewAdminMode));
PUBLIC VOID ETSUtlOperStateChange PROTO ((tETSPortEntry *pPortEntry, UINT1 u1ETSState));
PUBLIC VOID ETSUtlUpdateLocalParam PROTO ((tETSPortEntry *pPortEntry, UINT1 u1ETSState));
PUBLIC VOID ETSUtlTlvStatusChange PROTO ((tETSPortEntry *pPortEntry, 
                                  UINT1 u1NewTlvStatus, UINT1 u1TlvType));
PUBLIC VOID ETSUtlAdminParamChange PROTO ((tETSPortEntry *pPortEntry, UINT1 u1TlvType));
PUBLIC VOID ETSUtlWillingChange PROTO ((tETSPortEntry *pPortEntry,UINT1 u1IsLoca1Willing));
PUBLIC VOID ETSUtlFormAndSendConfigTLV PROTO ((tETSPortEntry *pPortEntry, UINT1 u1MsgType));
PUBLIC VOID ETSUtlFormAndSendRecoTLV PROTO ((tETSPortEntry *pPortEntry, UINT1 u1MsgType));
PUBLIC VOID ETSUtlFormAndSendTCSupportTLV PROTO ((tETSPortEntry *pPortEntry, UINT1 u1MsgType));
PUBLIC VOID ETSUtlHandleConfigTLV PROTO ((tETSPortEntry *pPortEntry,
                                 tApplTlvParam * pApplTlvParam));
PUBLIC VOID ETSUtlHandleRecoTLV PROTO ((tETSPortEntry *pPortEntry,
                               tApplTlvParam * pApplTlvParam));
PUBLIC VOID ETSUtlHandleTcSupportTLV PROTO ((tETSPortEntry *pPortEntry,
                                    tApplTlvParam * pApplTlvParam));
PUBLIC INT4 ETSUtlHwConfig PROTO ((tETSPortEntry *pPortEntry, UINT1 u1Status));
PUBLIC INT4 EtsUtlMcQueueDettachAttach PROTO ((UINT4 u4IfIndex, UINT4 u4ScheId, UINT1 u1ScheAlgo));
PUBLIC INT4 EtsUtlEtsDeleteScheduler PROTO ((tETSPortEntry *pPortEntry));

PUBLIC INT4 ETSUtlPortTblCmpFn PROTO ((tRBElem * pRBElem1, tRBElem * pRBElem2));
PUBLIC tETSPortEntry * ETSUtlGetPortEntry PROTO ((UINT4 u4IfIndex));
PUBLIC tETSPortEntry * ETSUtlCreatePortTblEntry PROTO ((UINT4 u4IfIndex));
PUBLIC INT4 ETSUtlDeletePortTblEntry PROTO ((tETSPortEntry * pETSPortEntry));

/* ETS API Function Prototype */
PUBLIC VOID ETSApiConfTlvCallBackFn PROTO ((tDcbxAppInfo *pDcbxAppInfo));
PUBLIC VOID ETSApiRecoTlvCallBackFn PROTO ((tDcbxAppInfo *pDcbxAppInfo));
PUBLIC VOID ETSApiTcSuppTlvCallBackFn PROTO ((tDcbxAppInfo *pDcbxAppInfo));

/* PFC Utility Function Prototype */

PUBLIC VOID PFCUtlConstructPreFormTlv PROTO ((VOID));
PUBLIC VOID PFCUtlHandleReRegReqFromDCBX PROTO ((tPFCPortEntry *pPortEntry));
PUBLIC VOID PFCUtlHandleAgedOutFromDCBX PROTO ((tPFCPortEntry *pPortEntry));
PUBLIC VOID PFCUtlDeleteAllPorts PROTO ((VOID));
PUBLIC VOID PFCUtlDeletePort PROTO ((UINT4 u4IfIndex));
PUBLIC VOID PFCUtlRegisterApplForPort PROTO ((tPFCPortEntry *pPortEntry));
PUBLIC VOID PFCUtlDeRegisterApplForPort PROTO ((tPFCPortEntry *pPortEntry));
PUBLIC VOID PFCUtlModuleStatusChange PROTO ((UINT1 u1Status));
PUBLIC VOID PFCUtlAdminModeChange PROTO ((tPFCPortEntry *pPortEntry, UINT1 u1NewAdminMode));
PUBLIC VOID PFCUtlOperStateChange PROTO ((tPFCPortEntry *pPortEntry, UINT1 u1PFCState));
PUBLIC INT4 PFCUtlUpdateLocalParam PROTO ((tPFCPortEntry *pPortEntry, UINT1 u1PFCState));
PUBLIC VOID PFCUtlTlvStatusChange PROTO ((tPFCPortEntry *pPortEntry, UINT1 u1NewTlvStatus));
PUBLIC INT4 PFCUtlAdminParamChange PROTO ((tPFCPortEntry *pPortEntry));
PUBLIC VOID PFCUtlWillingChange PROTO ((tPFCPortEntry *pPortEntry,UINT1 u1IsLoca1Willing));
PUBLIC VOID PFCUtlFormAndSendPFCTLV PROTO ((tPFCPortEntry *pPortEntry, UINT1 u1MsgType));
PUBLIC VOID PFCUtlHandleTLV PROTO ((tPFCPortEntry *pPortEntry,
                           tApplTlvParam * pApplTlvParam));

PUBLIC INT4 PFCUtlConfigProfile PROTO ((tPFCPortEntry *pPortEntry,UINT1 u1NewPfcProfile,
                           UINT1 u1Status));
PUBLIC VOID PFCUtlInitPFCProfiles PROTO ((VOID));
PUBLIC INT4 PFCUtlHwConfigPfc PROTO ((UINT4 u4IfIndex, UINT1 u1PfcPriority,
                               UINT1 u1Profile, UINT1 u1Flag));
PUBLIC INT4 PFCUtlDeleteProfile PROTO ((UINT1 u1OldProfile));
PUBLIC INT4 PFCUtlCreateProfile PROTO ((UINT1 u1OldProfile, 
                                 UINT1 u1NewPfcProfile, UINT1 u1Flag));
PUBLIC INT4 PFCUtlUpdateProfile PROTO ((tPFCPortEntry *pPortEntry,
                         UINT1 u1PfcPriority,
                         UINT1 u1NewPfcProfile, UINT1 u1Flag));


PUBLIC INT4 PFCUtlPortTblCmpFn PROTO ((tRBElem * pRBElem1, tRBElem * pRBElem2));
PUBLIC tPFCPortEntry * PFCUtlGetPortEntry PROTO ((UINT4 u4IfIndex ));
PUBLIC tPFCPortEntry * PFCUtlCreatePortTblEntry PROTO ((UINT4 u4IfIndex));
PUBLIC INT4 PFCUtlDeletePortTblEntry PROTO ((tPFCPortEntry * pPFCPortEntry));
PUBLIC VOID PFCPortPostMsgToDCBX PROTO ((UINT1 u1MsgType,UINT4 u4IfIndex, 
                                  tDcbxAppRegInfo  *pDcbxAppPortMsg));

/* PFC API Function Prototype */
PUBLIC VOID PFCApiTlvCallBackFn PROTO ((tDcbxAppInfo *pDcbxAppInfo));

/*DCBX RED Functions*/
PUBLIC VOID
DcbxCeeRedProcessDynSyncAgeOutInfo PROTO ((tRmMsg * pMsg, UINT4 *pu4OffSet,
                                    UINT2 u2RemMsgLen));
PUBLIC VOID
DcbxCeeRedSendDynSyncAgeOutInfo PROTO ((UINT4   u4IfIndex));
PUBLIC VOID
DcbxCeeRedBulkSyncCtrlCounterInfo PROTO ((VOID));

PUBLIC VOID DcbxCEERedSendDynamicSyncInfo PROTO ((tDcbxQueueMsg *pMsg));
PUBLIC VOID DcbxCEERedProcDynamicSyncInfo PROTO ((tRmMsg *pRmMsg, UINT4 *pu4OffSet,
                                           UINT2 u2RemMsgLen));
PUBLIC VOID
DcbxCeeRedFormCtrlInfoSyncMsg PROTO ((tCEECtrlEntry* pCtrlInfo,
                            tRmMsg * pMsg, UINT4 *pu4OffSet));
PUBLIC VOID
DcbxCeeRedBulkSyncCtrlInfo PROTO ((VOID));

PUBLIC VOID DcbxCEERedSendDynamicRemInfo PROTO ((UINT4 u4IfIndex, tApplTlvParam *pApplTlvParam));

PUBLIC VOID DcbxCEERedProcDynamicRemInfo PROTO ((tRmMsg *pMsg, UINT4 *pu4OffSet,
                                           UINT2 u2RemMsgLen));
PUBLIC VOID DcbxCeeRedBulkSyncPfcRemInfo PROTO ((VOID));
PUBLIC VOID DcbxCeeRedProcessDynamicPfcRemInfo PROTO ((tRmMsg * pMsg, 
                                                     UINT4 *pu4OffSet,UINT2 u2RemMsgLen));
PUBLIC VOID
DcbxCeeRedFormPfcRemInfoSyncMsg PROTO ((tPFCPortEntry *pPfcPortInfo,
            tRmMsg *pMsg, UINT4 *pu4OffSet));

PUBLIC VOID
DcbxCeeRedBulkSyncEtsRemInfo PROTO ((VOID));

PUBLIC VOID
DcbxCeeRedFormEtsRemInfoSyncMsg PROTO ((tETSPortEntry * pEtsPortInfo,
                              tRmMsg * pMsg, UINT4 *pu4OffSet));
PUBLIC VOID
DcbxCeeRedBulkSyncAppPriRemInfo PROTO ((VOID));
PUBLIC VOID
DcbxCeeRedFormAppPriRemInfoSyncMsg PROTO ((tAppPriPortEntry *pAppPriPortInfo,
                                             tRmMsg *pMsg, UINT4 *pu4OffSet));
PUBLIC VOID DcbxCeeRedProcessDynamicEtsRemInfo PROTO ((tRmMsg * pMsg, 
            UINT4 *pu4OffSet,UINT2 u2RemMsgLen));

PUBLIC VOID
DcbxCeeRedProcessDynamicAppPriRemInfo PROTO ((tRmMsg *pMsg, UINT4 *pu4OffSet,
                                                UINT2 u2RemMsgLen));
PUBLIC VOID DcbxRedBulkSyncDcbxPortInfo PROTO ((VOID));
PUBLIC VOID
DcbxRedProcessDynamicDcbxPortInfo PROTO ((tRmMsg *pMsg, UINT4 *pu4OffSet,
                        UINT2 u2RemMsgLen));
PUBLIC VOID
DcbxCeeRedSendDynamicCtrlInfo PROTO ((tCEECtrlEntry *pCtrlEntry));
PUBLIC VOID
DcbxCeeRedProcessBulkCtrlInfoSyncMsg PROTO ((tRmMsg * pMsg,
                      UINT4 *pu4OffSet, UINT2 u2MsgLen));
PUBLIC VOID
DcbxCeeRedSendDynamicCtrlCounterInfo PROTO ((tCEECtrlEntry *pCtrlInfo, UINT1 u1CounterType));
PUBLIC VOID
DcbxCeeRedProcessDynCtrlCounterInfo PROTO ((tRmMsg * pMsg, UINT4 *pu4OffSet,
                                     UINT2 u2RemMsgLen));

PUBLIC INT4 DcbxRedInitGlobalInfo PROTO ((VOID));
PUBLIC VOID DcbxRedDeInitGlobalInfo PROTO ((VOID));
PUBLIC INT4 DcbxRedRmCallBack PROTO ((UINT1 u1Event, tRmMsg * pData, UINT2 u2DataLen));
PUBLIC VOID DcbxRedHandleRmEvents PROTO ((tDcbxQueueMsg * pMsg));
PUBLIC VOID DcbxRedHandleGoActive PROTO ((VOID));
PUBLIC VOID DcbxRedHandleIdleToActive PROTO ((VOID));
PUBLIC VOID DcbxRedHandleStandByToActive PROTO ((VOID));
PUBLIC VOID DcbxRedHandleGoStandby PROTO ((VOID));
PUBLIC VOID DcbxRedHandleIdleToStandby PROTO ((VOID));
PUBLIC VOID DcbxRedHandleActiveToStandby PROTO ((VOID));
PUBLIC VOID DcbxRedProcessPeerMsgAtActive PROTO ((tRmMsg * pMsg, UINT2 u2DataLen));
PUBLIC VOID DcbxRedProcessPeerMsgAtStandby PROTO ((tRmMsg * pMsg, UINT2 u2DataLen));
PUBLIC VOID DcbxRedProcessBulkTailMsg PROTO ((tRmMsg * pMsg, UINT4 *pu4OffSet));
PUBLIC VOID DcbxRedSendBulkRequest PROTO ((VOID));
PUBLIC VOID DcbxRedHandleBulkRequest PROTO ((VOID));
PUBLIC VOID DcbxRedSendBulkUpdTailMsg PROTO ((VOID));
PUBLIC VOID DcbxRedBulkSyncEtsRemInfo PROTO ((VOID));
PUBLIC VOID DcbxRedBulkSyncEtsSemInfo PROTO ((VOID));
PUBLIC VOID DcbxRedBulkSyncEtsCounterInfo PROTO ((VOID));
PUBLIC VOID DcbxRedSendDynamicEtsRemInfo PROTO ((tETSPortEntry *pEtsPortInfo));
PUBLIC VOID DcbxRedSendDynamicEtsSemInfo PROTO ((tETSPortEntry *pEtsPortInfo));
PUBLIC VOID DcbxRedSendDynamicEtsCounterInfo PROTO ((tETSPortEntry *pEtsPortInfo,
                                                     UINT1 u1CounterType));
PUBLIC VOID DcbxRedFormEtsRemInfoSyncMsg PROTO ((tETSPortEntry *pEtsPortInfo, 
                                              tRmMsg * pMsg,UINT4 *pu4OffSet));
PUBLIC VOID DcbxRedFormEtsSemInfoSyncMsg PROTO ((tETSPortEntry *pEtsPortInfo, 
                                              tRmMsg * pMsg,UINT4 *pu4OffSet));
PUBLIC VOID DcbxRedFormEtsCounterInfoSyncMsg PROTO ((tETSPortEntry *pEtsPortInfo, 
                                              tRmMsg * pMsg,UINT4 *pu4OffSet,
                                              UINT1 u1CounterType));
PUBLIC VOID DcbxRedFormEtsAllCounterInfoSyncMsg PROTO ((tETSPortEntry *pEtsPortInfo, 
                                                    tRmMsg * pMsg,UINT4 *pu4OffSet));
PUBLIC VOID DcbxRedBulkSyncPfcRemInfo PROTO ((VOID));
PUBLIC VOID DcbxRedBulkSyncPfcSemInfo PROTO ((VOID));
PUBLIC VOID DcbxRedBulkSyncPfcCounterInfo PROTO ((VOID));
PUBLIC VOID DcbxRedSendDynamicPfcRemInfo PROTO ((tPFCPortEntry *pPfcPortInfo));
PUBLIC VOID DcbxRedSendDynamicPfcSemInfo PROTO ((tPFCPortEntry *pPfcPortInfo));
PUBLIC VOID DcbxRedSendDynamicPfcCounterInfo PROTO ((tPFCPortEntry *pPfcPortInfo,
                                                     UINT1 u1CounterType));
PUBLIC VOID DcbxRedFormPfcRemInfoSyncMsg PROTO ((tPFCPortEntry *pPfcPortInfo, 
                                              tRmMsg * pMsg,UINT4 *pu4OffSet));
PUBLIC VOID DcbxRedFormPfcSemInfoSyncMsg PROTO ((tPFCPortEntry *pPfcPortInfo, 
                                              tRmMsg * pMsg,UINT4 *pu4OffSet));
PUBLIC VOID DcbxRedFormPfcCounterInfoSyncMsg PROTO ((tPFCPortEntry *pPfcPortInfo, 
                                              tRmMsg * pMsg,UINT4 *pu4OffSet,
                                              UINT1 u1CounterType));
PUBLIC VOID DcbxRedFormPfcAllCounterInfoSyncMsg PROTO ((tPFCPortEntry *pPfcPortInfo, 
                                              tRmMsg * pMsg,UINT4 *pu4OffSet));
PUBLIC VOID DcbxRedProcessDynamicEtsRemInfo PROTO ((tRmMsg * pMsg, 
                                         UINT4 *pu4OffSet,UINT2 u2RemMsgLen));
PUBLIC VOID DcbxRedProcessDynamicEtsSemInfo PROTO ((tRmMsg * pMsg, 
                                         UINT4 *pu4OffSet,UINT2 u2RemMsgLen));
PUBLIC VOID DcbxRedProcessDynamicEtsCounterInfo PROTO ((tRmMsg * pMsg, 
                                         UINT4 *pu4OffSet,UINT2 u2RemMsgLen));
PUBLIC VOID DcbxRedProcessDynamicEtsAllCounterInfo PROTO ((tRmMsg * pMsg, 
                                         UINT4 *pu4OffSet,UINT2 u2RemMsgLen));
PUBLIC VOID DcbxRedProcessDynamicPfcRemInfo PROTO ((tRmMsg * pMsg, 
                                         UINT4 *pu4OffSet,UINT2 u2RemMsgLen));
PUBLIC VOID DcbxRedProcessDynamicPfcSemInfo PROTO ((tRmMsg * pMsg, 
                                         UINT4 *pu4OffSet,UINT2 u2RemMsgLen));
PUBLIC VOID DcbxRedProcessDynamicPfcCounterInfo PROTO ((tRmMsg * pMsg, 
                                         UINT4 *pu4OffSet,UINT2 u2RemMsgLen));
PUBLIC VOID DcbxRedProcessDynamicPfcAllCounterInfo PROTO ((tRmMsg * pMsg, 
                                         UINT4 *pu4OffSet,UINT2 u2RemMsgLen));

PUBLIC VOID
DcbxRedProcessDynamicAppPriRemInfo PROTO ((tRmMsg * pMsg, UINT4 *pu4OffSet,
                                           UINT2 u2RemMsgLen));
PUBLIC VOID
DcbxRedProcessDynamicAppPriSemInfo PROTO ((tRmMsg * pMsg, UINT4 *pu4OffSet,
                                           UINT2 u2RemMsgLen));
PUBLIC VOID
DcbxRedProcessDynamicAppPriAllCounterInfo PROTO ((tRmMsg * pMsg, UINT4 *pu4OffSet,
                                                  UINT2 u2RemMsgLen));
PUBLIC VOID
DcbxRedProcessDynamicAppPriCounterInfo PROTO ((tRmMsg * pMsg, UINT4 *pu4OffSet,
                                               UINT2 u2RemMsgLen));
PUBLIC VOID DcbxRedBulkSyncAppPriRemInfo PROTO ((VOID));
PUBLIC VOID DcbxRedBulkSyncAppPriSemInfo PROTO ((VOID));
PUBLIC VOID DcbxRedBulkSyncAppPriCounterInfo PROTO ((VOID));
PUBLIC VOID DcbxRedSendDynamicAppPriRemInfo PROTO ((tAppPriPortEntry
                                                    *pAppPriPortInfo));
PUBLIC VOID DcbxRedSendDynamicAppPriCounterInfo
                                   PROTO ((tAppPriPortEntry *pAppPriPortInfo,
                                           UINT1 u1CounterType));

PUBLIC VOID DcbxRedSendDynamicAppPriSemInfo PROTO ((tAppPriPortEntry
                                                    *pAppPriPortInfo));
PUBLIC VOID DcbxRedFormAppPriRemInfoSyncMsg
                PROTO ((tAppPriPortEntry *pAppPriPortInfo, 
                        tRmMsg * pMsg,UINT4 *pu4OffSet));
PUBLIC VOID DcbxRedFormAppPriSemInfoSyncMsg PROTO ((tAppPriPortEntry *pAppPriPortInfo, 
                                                    tRmMsg * pMsg,UINT4 *pu4OffSet));
PUBLIC VOID DcbxRedFormAppPriCounterInfoSyncMsg
                               PROTO ((tAppPriPortEntry *pAppPriPortInfo,
                                tRmMsg * pMsg,UINT4 *pu4OffSet,
                                UINT1 u1CounterType));
PUBLIC VOID DcbxRedFormAppPriAllCounterInfoSyncMsg
                                    PROTO ((tAppPriPortEntry *pAppPriPortInfo, 
                                            tRmMsg * pMsg,UINT4 *pu4OffSet));
PUBLIC VOID
DcbxRedSendDynamicDcbxStatusInfo PROTO ((UINT1 u1MsgType,
                                         UINT4 u4IfIndex,
                                         UINT1 u1DcbxStatus));
PUBLIC VOID
DcbxRedProcessDynamicDcbxStatusInfo PROTO ((tRmMsg * pMsg,
                                              UINT4 *pu4OffSet,
                                              UINT2 u2RemMsgLen));
#ifdef MBSM_WANTED
PUBLIC VOID DcbxMbsmProcessMsg PROTO ((tMbsmProtoMsg * pProtoMsg));
PUBLIC VOID DcbxMbsmConfigPfc PROTO ((tMbsmPortInfo * pMbsmPortInfo, 
                          tMbsmSlotInfo * pSlotInfo));
PUBLIC VOID DcbxMbsmConfigAppPri PROTO ((tMbsmPortInfo * pMbsmPortInfo, 
                          tMbsmSlotInfo * pSlotInfo));
PUBLIC VOID DcbxMbsmConfigEts PROTO ((tMbsmPortInfo * pMbsmPortInfo, 
                          tMbsmSlotInfo * pSlotInfo));
#endif  /* MBSM_WANTED */

/* Application Priority utility functions */
PUBLIC tAppPriPortEntry *
AppPriUtlGetPortEntry PROTO ((UINT4 u4IfIndex));

PUBLIC VOID
AppPriUtlTlvStatusChange PROTO ((tAppPriPortEntry * pPortEntry, UINT1 u1NewTlvStatus));

PUBLIC VOID
AppPriUtlRegisterApplForPort PROTO ((tAppPriPortEntry * pPortEntry));
PUBLIC VOID
AppPriUtlDeRegisterApplForPort PROTO ((tAppPriPortEntry * pPortEntry));
PUBLIC VOID
AppPriPPortPostMsgToDCBX PROTO ((UINT1 u1MsgType, UINT4 u4IfIndex,
                             tDcbxAppRegInfo * pDcbxAppPortMsg));
PUBLIC VOID
AppPriUtlOperStateChange PROTO ((tAppPriPortEntry * pPortEntry, UINT1 u1APPState));
PUBLIC VOID
AppPriUtlUpdateLocalParam PROTO ((tAppPriPortEntry * pPortEntry, UINT1 u1APPState));
PUBLIC VOID
AppPriUtlFormAndSendAppPriTLV PROTO ((tAppPriPortEntry * pPortEntry, UINT1 u1MsgType));
PUBLIC VOID
AppPriUtlWillingChange PROTO ((tAppPriPortEntry * pPortEntry, UINT1 u1IsLocalWilling));
PUBLIC VOID
AppPriUtlDeleteAllPorts PROTO ((VOID));
INT4
AppPriUtlDeletePortTblEntry PROTO ((tAppPriPortEntry * pAppPortEntry));
PUBLIC VOID
AppPriUtlModuleStatusChange PROTO ((UINT1 u1Status));
PUBLIC VOID
AppPriUtlAdminModeChange PROTO ((tAppPriPortEntry * pPortEntry, UINT1 u1NewAdminMode));
tAppPriPortEntry      *
AppPriUtlCreatePortTblEntry PROTO ((UINT4 u4IfIndex));
PUBLIC VOID
AppPriPortPostMsgToDCBX PROTO ((UINT1 u1MsgType, UINT4 u4IfIndex,
                            tDcbxAppRegInfo * pDcbxAppPortMsg));
INT4
AppPriUtlPortTblCmpFn PROTO ((tRBElem * pRBElem1, tRBElem * pRBElem2));
INT4
AppPriUtlAppTblCmpFn PROTO ((tRBElem * pRBElem1, tRBElem * pRBElem2));
tAppPriMappingEntry      *
AppPriUtlGetAppPriMappingEntry PROTO ((UINT4 u4IfIndex, INT4 i4Selector,
                            INT4 i4Protocol, UINT1 u1Type));
tAppPriMappingEntry      *
AppPriUtlCreateAppPriMappingTblEntry PROTO ((UINT4 u4IfIndex, INT4 i4Selector,
                                         INT4 i4Protocol, INT1 i1Type));
PUBLIC VOID
AppPriUtlHandleTLV PROTO ((tAppPriPortEntry * pPortEntry,
                       tApplTlvParam * pApplTlvParam));
INT4
AppPriUtlDeleteAppPriMappingEntry PROTO ((UINT4 u4IfIndex, INT4 i4Selector, INT4 i4Protocol, UINT1 u1Type));
PUBLIC VOID
AppPriUtlDeleteAllAppPriMappingEntries PROTO ((UINT4 u4IfIndex));
PUBLIC VOID
AppPriUtlAdminParamChange PROTO ((tAppPriPortEntry *pPortEntry));
PUBLIC VOID
AppPriUtlHandleReRegReqFromDCBX PROTO ((tAppPriPortEntry * pPortEntry));
PUBLIC VOID
AppPriUtlHandleAgedOutFromDCBX PROTO ((tAppPriPortEntry * pPortEntry));
PUBLIC VOID
AppPriUtlDeleteRemoteMappingEntries PROTO ((UINT4 u4IfIndex));
PUBLIC VOID
AppPriUtlDeleteRemotePortEntries PROTO ((UINT4 u4IfIndex));
PUBLIC VOID
AppPriUtlDelFn PROTO ((tTMO_SLL_NODE *pAppPriMappingEntry));
PUBLIC INT4
AppPriUtlHwConfig PROTO ((tAppPriPortEntry * pPortEntry, UINT1 u1Status));
PUBLIC INT4
AppPriUtlHwConfigAppPriMappingEntry PROTO (( UINT4 u4IfIndex, INT4 i4Selector,
                                     tAppPriMappingEntry * pAppPriEntry,
                                     UINT1 u1Status));
PUBLIC INT4
AppPriUtlConfigHwMapping PROTO ((UINT1 u1FilterType, 
            tAppPriMappingEntry *pAppPriEntry, 
            tIssAclHwFilterInfo *pAclFilterInfo, UINT1 u1Status));
PUBLIC VOID
AppPriUtlDumpPkt PROTO ((UINT1 *pu1Buf, UINT4 u4Length));

/*Application Priority API functions */
PUBLIC VOID
AppPriUtlDeletePort PROTO ((UINT4 u4IfIndex));
PUBLIC VOID
AppPriApiTlvCallBackFn PROTO ((tDcbxAppInfo * pDcbxAppInfo));

/*Ut Functions*/
PUBLIC VOID UtMain PROTO ((VOID));
PUBLIC VOID DcbxCliSetUtVar PROTO((tCliHandle CliHandle, UINT4 *pu4Glob));

INT4 DcbxL2IwfIsPortInPortChannel (UINT4 u4IfIndex);

/* CEE Functions */

PUBLIC INT4 CEEMainModuleStart PROTO ((VOID));
PUBLIC INT4 CEEUtlPortTblCmpFn PROTO ((tRBElem * pRBElem1, tRBElem * pRBElem2));
PUBLIC VOID CEEMainAssignMempoolIds PROTO ((VOID));
PUBLIC tCEECtrlEntry  * CEEUtlGetCtrlEntry PROTO ((UINT4 u4IfIndex));
PUBLIC INT4 CEEUtlDeleteCtrlTblEntry PROTO ((tCEECtrlEntry * pCEECtrlEntry));
PUBLIC tCEECtrlEntry * CEEUtlCreateCtrlTblEntry PROTO ((UINT4 u4IfIndex));
PUBLIC VOID CEEUtlHandleAgedOutFromDCBX PROTO ((UINT4 u4IfIndex));

/*CEE Feat State Machine Related Functions */
PUBLIC VOID CEEUtlHandleTLV PROTO ((UINT4 , tApplTlvParam *));

PUBLIC INT4 CEEHandlePFCTlv PROTO ((tPFCPortEntry *, tCeeTLVInfo *, UINT1 *));
PUBLIC BOOL1 PFCApiIsCfgCompatible PROTO ((tPFCPortEntry *pPFCPortEntry));

PUBLIC INT4 CEEHandleETSTlv PROTO ((tETSPortEntry *, tCeeTLVInfo *, UINT1 *));

PUBLIC INT4
CEEHandleAppPriTlv PROTO((tAppPriPortEntry *, tCeeTLVInfo *, UINT1 *));
PUBLIC UINT1
CEEUtlCalculateOperState PROTO ((UINT1 u1RemWilling, UINT1 u1AdmWilling, BOOL1 bIsCompatible));
PUBLIC BOOL1
AppPriApiIsCfgCompatible PROTO((tAppPriPortEntry *));
BOOL1
AppPriCompareAllMappingEntry PROTO ((tTMO_SLL *pAppPriAdmTbl, tTMO_SLL *pAppPriRemTbl));
BOOL1
AppPriCompareFcoeMappings PROTO ((tTMO_SLL *pAppPriAdmTbl, tTMO_SLL *pAppPriRemTbl, BOOL1 b1FcoeFipExist));
BOOL1
AppPriCompareNonFcoeMappings PROTO ((tTMO_SLL *pAppPriAdmTbl, tTMO_SLL *pAppPriRemTbl));

PUBLIC VOID
CEEUtlSetSemType PROTO ((INT4 i4FsDCBXPortNumber, UINT1 u1Mode));

PUBLIC VOID CEEUtlHandleCtrlTLV PROTO ((tCEECtrlEntry *, tCeeTLVInfo *));
PUBLIC VOID CeeUtlHandleCtrlTlvAgedOut PROTO ((UINT4 ));
PUBLIC VOID CEEApiTlvCallBackFn PROTO ((tDcbxAppInfo * ));
PUBLIC BOOL1 ETSApiIsCfgCompatible PROTO ((tETSPortEntry *));

PUBLIC UINT1 CEEtoIEEEBitManipulation PROTO ((UINT1 u1CEEValue));
PUBLIC VOID  CEESemRun (UINT1 u1TlvType, tCeeTLVInfo * pCeeAppTlvParam, INT4 i4Event);
PUBLIC VOID  CEESmStateUseLocalCfg PROTO ((UINT1 , tCeeTLVInfo * ));
PUBLIC VOID  CEESmStateUsePeerCfg PROTO ((UINT1 , tCeeTLVInfo * ));
PUBLIC VOID  CEESmStateCfgNotCompatible PROTO ((UINT1 , tCeeTLVInfo * ));

PUBLIC VOID CEEUtlClearETSRemoteParam PROTO ((tETSPortEntry * ));
PUBLIC VOID CEEUtlUpdatETSLocalParam PROTO ((tETSPortEntry * , UINT1 ));
PUBLIC INT4 CeeUtlETSHwConfig PROTO ((tETSPortEntry * , UINT1 ));
PUBLIC INT4 CEEUtlValidateTLV PROTO ((UINT4 , tApplTlvParam *));
PUBLIC VOID CEEUtlDeleteAllPorts PROTO ((VOID));
PUBLIC VOID CEEMainModuleShutDown PROTO ((VOID));

PUBLIC VOID 
PFCUtlRegOrDeRegBasedOnMode PROTO ((tPFCPortEntry *pPortEntry, UINT1 u1Status));
PUBLIC VOID 
ETSUtlRegOrDeRegBasedOnMode PROTO ((tETSPortEntry *pPortEntry, 
                             UINT1 u1Status, UINT1 u1TlvType));
PUBLIC VOID 
AppPriUtlRegOrDeRegBasedOnMode PROTO ((tAppPriPortEntry * pPortEntry, 
                                        UINT1 u1Status));
PUBLIC VOID CEEUtlHandleLLdpTxRxNotify PROTO ((UINT4, UINT4));
PUBLIC VOID CEEUtlHandleLLdpTxRxDisableState PROTO ((UINT4));

/* CEE SEM function pointers */
PUBLIC VOID CEESemEventIgnore              PROTO ((UINT1, tCeeTLVInfo *));
PUBLIC VOID CEESmStateDelAgedInfo          PROTO ((UINT1, tCeeTLVInfo *));
PUBLIC VOID CEESmStateRxFeatTlv            PROTO ((UINT1, tCeeTLVInfo *));
PUBLIC VOID CEESmStateNoFeatTlv            PROTO ((UINT1, tCeeTLVInfo *));
PUBLIC VOID CEESmStateWaitForFeatTlv       PROTO ((UINT1, tCeeTLVInfo *));
PUBLIC VOID CEESmStateRxDupTlv             PROTO ((UINT1, tCeeTLVInfo *));
PUBLIC VOID CeeSmStateUseLocalCfg          PROTO ((UINT1, tCeeTLVInfo *));
PUBLIC VOID CeeSmStateUsePeerCfg           PROTO ((UINT1, tCeeTLVInfo *));
PUBLIC VOID CeeSmStateCfgNotComaptible     PROTO ((UINT1, tCeeTLVInfo *));
PUBLIC VOID CEESmStateFeatInit             PROTO ((UINT1, tCeeTLVInfo *));
PUBLIC VOID CEESmStateInvalidPktRcvd          PROTO ((UINT1, tCeeTLVInfo *));

PUBLIC INT4
CEEUtlCheckForAckMiss PROTO ((tCEECtrlEntry *pCEECtrlEntry, tCeeTLVInfo *pCeeTLVInfo));
VOID CEEUtlUpdateSmForPktErrors PROTO ((tCeeTLVInfo CeeTLVInfo, INT4 i4Event));
PUBLIC VOID CEEUtlClearCeeCtrlParams PROTO ((UINT4 u4IfIndex));

/* CEE AgeOut funtions*/

PUBLIC VOID CEEUtlHandleAppPriAgeOut PROTO ((tAppPriPortEntry *));
PUBLIC VOID CEEUtlHandlePFCAgeOut    PROTO ((tPFCPortEntry *));
PUBLIC VOID CEEUtlHandleETSAgeOut    PROTO ((tETSPortEntry *));
#endif /* __DCBX_PROTO_H__ */

