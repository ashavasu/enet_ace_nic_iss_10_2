/*************************************************************************
 * Copyright (C) 2010 Aricent Inc . All Rights Reserved
 * $Id: pfcsz.h,v 1.1 2010/10/28 11:54:27 prabuc Exp $
 * Description: This header file contains all mempool prototypes 
 *              declaration for DCBX PFC Module.
****************************************************************************/
enum {
    MAX_DCBX_PFC_PORT_ENTRIES_SIZING_ID,
    PFC_MAX_SIZING_ID
};


#ifdef  _PFCSZ_C
tMemPoolId PFCMemPoolIds[ PFC_MAX_SIZING_ID];
INT4  PfcSizingMemCreateMemPools(VOID);
VOID  PfcSizingMemDeleteMemPools(VOID);
INT4  PfcSzRegisterModuleSizingParams( CHR1 *pu1ModName);
#else  /*  _PFCSZ_C  */
extern tMemPoolId PFCMemPoolIds[ ];
extern INT4  PfcSizingMemCreateMemPools(VOID);
extern VOID  PfcSizingMemDeleteMemPools(VOID);
extern INT4  PfcSzRegisterModuleSizingParams( CHR1 *pu1ModName); 
#endif /*  _PFCSZ_C  */


#ifdef  _PFCSZ_C
tFsModSizingParams FsPFCSizingParams [] = {
{ "tPFCPortEntry", "MAX_DCBX_PFC_PORT_ENTRIES", sizeof(tPFCPortEntry),MAX_DCBX_PFC_PORT_ENTRIES, MAX_DCBX_PFC_PORT_ENTRIES,0 },
{"\0","\0",0,0,0,0}
};
#else  /*  _PFCSZ_C  */
extern tFsModSizingParams FsPFCSizingParams [];
#endif /*  _PFCSZ_C  */


