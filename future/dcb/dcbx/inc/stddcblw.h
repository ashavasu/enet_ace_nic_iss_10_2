/********************************************************************
* Copyright (C) 2010 Aricent Inc . All Rights Reserved
*
* $Id: stddcblw.h,v 1.4 2016/07/09 09:41:20 siva Exp $
*
* Description: Proto types for Low Level  Routines
*********************************************************************/

/* Proto Validate Index Instance for LldpXdot1dcbxConfigETSConfigurationTable. */
INT1
nmhValidateIndexInstanceLldpXdot1dcbxConfigETSConfigurationTable ARG_LIST((INT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for LldpXdot1dcbxConfigETSConfigurationTable  */

INT1
nmhGetFirstIndexLldpXdot1dcbxConfigETSConfigurationTable ARG_LIST((INT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexLldpXdot1dcbxConfigETSConfigurationTable ARG_LIST((INT4 , INT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetLldpXdot1dcbxConfigETSConfigurationTxEnable ARG_LIST((INT4  , UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetLldpXdot1dcbxConfigETSConfigurationTxEnable ARG_LIST((INT4  , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2LldpXdot1dcbxConfigETSConfigurationTxEnable ARG_LIST((UINT4 *  ,INT4  , UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2LldpXdot1dcbxConfigETSConfigurationTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for LldpXdot1dcbxConfigETSRecommendationTable. */
INT1
nmhValidateIndexInstanceLldpXdot1dcbxConfigETSRecommendationTable ARG_LIST((INT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for LldpXdot1dcbxConfigETSRecommendationTable  */

INT1
nmhGetFirstIndexLldpXdot1dcbxConfigETSRecommendationTable ARG_LIST((INT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexLldpXdot1dcbxConfigETSRecommendationTable ARG_LIST((INT4 , INT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetLldpXdot1dcbxConfigETSRecommendationTxEnable ARG_LIST((INT4  , UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetLldpXdot1dcbxConfigETSRecommendationTxEnable ARG_LIST((INT4  , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2LldpXdot1dcbxConfigETSRecommendationTxEnable ARG_LIST((UINT4 *  ,INT4  , UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2LldpXdot1dcbxConfigETSRecommendationTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for LldpXdot1dcbxConfigPFCTable. */
INT1
nmhValidateIndexInstanceLldpXdot1dcbxConfigPFCTable ARG_LIST((INT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for LldpXdot1dcbxConfigPFCTable  */

INT1
nmhGetFirstIndexLldpXdot1dcbxConfigPFCTable ARG_LIST((INT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexLldpXdot1dcbxConfigPFCTable ARG_LIST((INT4 , INT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetLldpXdot1dcbxConfigPFCTxEnable ARG_LIST((INT4  , UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetLldpXdot1dcbxConfigPFCTxEnable ARG_LIST((INT4  , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2LldpXdot1dcbxConfigPFCTxEnable ARG_LIST((UINT4 *  ,INT4  , UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2LldpXdot1dcbxConfigPFCTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for LldpXdot1dcbxConfigApplicationPriorityTable. */
INT1
nmhValidateIndexInstanceLldpXdot1dcbxConfigApplicationPriorityTable ARG_LIST((INT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for LldpXdot1dcbxConfigApplicationPriorityTable  */

INT1
nmhGetFirstIndexLldpXdot1dcbxConfigApplicationPriorityTable ARG_LIST((INT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexLldpXdot1dcbxConfigApplicationPriorityTable ARG_LIST((INT4 , INT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetLldpXdot1dcbxConfigApplicationPriorityTxEnable ARG_LIST((INT4  , UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetLldpXdot1dcbxConfigApplicationPriorityTxEnable ARG_LIST((INT4  , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2LldpXdot1dcbxConfigApplicationPriorityTxEnable ARG_LIST((UINT4 *  ,INT4  , UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2LldpXdot1dcbxConfigApplicationPriorityTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for LldpXdot1dcbxLocETSBasicConfigurationTable. */
INT1
nmhValidateIndexInstanceLldpXdot1dcbxLocETSBasicConfigurationTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for LldpXdot1dcbxLocETSBasicConfigurationTable  */

INT1
nmhGetFirstIndexLldpXdot1dcbxLocETSBasicConfigurationTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexLldpXdot1dcbxLocETSBasicConfigurationTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetLldpXdot1dcbxLocETSConCreditBasedShaperSupport ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetLldpXdot1dcbxLocETSConTrafficClassesSupported ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetLldpXdot1dcbxLocETSConWilling ARG_LIST((INT4 ,INT4 *));

/* Proto Validate Index Instance for LldpXdot1dcbxLocETSConPriorityAssignmentTable. */
INT1
nmhValidateIndexInstanceLldpXdot1dcbxLocETSConPriorityAssignmentTable ARG_LIST((INT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for LldpXdot1dcbxLocETSConPriorityAssignmentTable  */

INT1
nmhGetFirstIndexLldpXdot1dcbxLocETSConPriorityAssignmentTable ARG_LIST((INT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexLldpXdot1dcbxLocETSConPriorityAssignmentTable ARG_LIST((INT4 , INT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetLldpXdot1dcbxLocETSConPriTrafficClass ARG_LIST((INT4  , UINT4 ,UINT4 *));

/* Proto Validate Index Instance for LldpXdot1dcbxLocETSConTrafficClassBandwidthTable. */
INT1
nmhValidateIndexInstanceLldpXdot1dcbxLocETSConTrafficClassBandwidthTable ARG_LIST((INT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for LldpXdot1dcbxLocETSConTrafficClassBandwidthTable  */

INT1
nmhGetFirstIndexLldpXdot1dcbxLocETSConTrafficClassBandwidthTable ARG_LIST((INT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexLldpXdot1dcbxLocETSConTrafficClassBandwidthTable ARG_LIST((INT4 , INT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetLldpXdot1dcbxLocETSConTrafficClassBandwidth ARG_LIST((INT4  , UINT4 ,UINT4 *));

/* Proto Validate Index Instance for LldpXdot1dcbxLocETSConTrafficSelectionAlgorithmTable. */
INT1
nmhValidateIndexInstanceLldpXdot1dcbxLocETSConTrafficSelectionAlgorithmTable ARG_LIST((INT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for LldpXdot1dcbxLocETSConTrafficSelectionAlgorithmTable  */

INT1
nmhGetFirstIndexLldpXdot1dcbxLocETSConTrafficSelectionAlgorithmTable ARG_LIST((INT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexLldpXdot1dcbxLocETSConTrafficSelectionAlgorithmTable ARG_LIST((INT4 , INT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetLldpXdot1dcbxLocETSConTrafficSelectionAlgorithm ARG_LIST((INT4  , UINT4 ,INT4 *));

/* Proto Validate Index Instance for LldpXdot1dcbxLocETSRecoTrafficClassBandwidthTable. */
INT1
nmhValidateIndexInstanceLldpXdot1dcbxLocETSRecoTrafficClassBandwidthTable ARG_LIST((INT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for LldpXdot1dcbxLocETSRecoTrafficClassBandwidthTable  */

INT1
nmhGetFirstIndexLldpXdot1dcbxLocETSRecoTrafficClassBandwidthTable ARG_LIST((INT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexLldpXdot1dcbxLocETSRecoTrafficClassBandwidthTable ARG_LIST((INT4 , INT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetLldpXdot1dcbxLocETSRecoTrafficClassBandwidth ARG_LIST((INT4  , UINT4 ,UINT4 *));

/* Proto Validate Index Instance for LldpXdot1dcbxLocETSRecoTrafficSelectionAlgorithmTable. */
INT1
nmhValidateIndexInstanceLldpXdot1dcbxLocETSRecoTrafficSelectionAlgorithmTable ARG_LIST((INT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for LldpXdot1dcbxLocETSRecoTrafficSelectionAlgorithmTable  */

INT1
nmhGetFirstIndexLldpXdot1dcbxLocETSRecoTrafficSelectionAlgorithmTable ARG_LIST((INT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexLldpXdot1dcbxLocETSRecoTrafficSelectionAlgorithmTable ARG_LIST((INT4 , INT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetLldpXdot1dcbxLocETSRecoTrafficSelectionAlgorithm ARG_LIST((INT4  , UINT4 ,INT4 *));

/* Proto Validate Index Instance for LldpXdot1dcbxLocPFCBasicTable. */
INT1
nmhValidateIndexInstanceLldpXdot1dcbxLocPFCBasicTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for LldpXdot1dcbxLocPFCBasicTable  */

INT1
nmhGetFirstIndexLldpXdot1dcbxLocPFCBasicTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexLldpXdot1dcbxLocPFCBasicTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetLldpXdot1dcbxLocPFCWilling ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetLldpXdot1dcbxLocPFCMBC ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetLldpXdot1dcbxLocPFCCap ARG_LIST((INT4 ,UINT4 *));

/* Proto Validate Index Instance for LldpXdot1dcbxLocPFCEnableTable. */
INT1
nmhValidateIndexInstanceLldpXdot1dcbxLocPFCEnableTable ARG_LIST((INT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for LldpXdot1dcbxLocPFCEnableTable  */

INT1
nmhGetFirstIndexLldpXdot1dcbxLocPFCEnableTable ARG_LIST((INT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexLldpXdot1dcbxLocPFCEnableTable ARG_LIST((INT4 , INT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetLldpXdot1dcbxLocPFCEnableEnabled ARG_LIST((INT4  , UINT4 ,INT4 *));

/* Proto Validate Index Instance for LldpXdot1dcbxLocApplicationPriorityAppTable. */
INT1
nmhValidateIndexInstanceLldpXdot1dcbxLocApplicationPriorityAppTable ARG_LIST((INT4  , INT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for LldpXdot1dcbxLocApplicationPriorityAppTable  */

INT1
nmhGetFirstIndexLldpXdot1dcbxLocApplicationPriorityAppTable ARG_LIST((INT4 * , INT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexLldpXdot1dcbxLocApplicationPriorityAppTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetLldpXdot1dcbxLocApplicationPriorityAEPriority ARG_LIST((INT4  , INT4  , UINT4 ,UINT4 *));

/* Proto Validate Index Instance for LldpXdot1dcbxRemETSBasicConfigurationTable. */
INT1
nmhValidateIndexInstanceLldpXdot1dcbxRemETSBasicConfigurationTable ARG_LIST((UINT4  , INT4  , UINT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for LldpXdot1dcbxRemETSBasicConfigurationTable  */

INT1
nmhGetFirstIndexLldpXdot1dcbxRemETSBasicConfigurationTable ARG_LIST((UINT4 * , INT4 * , UINT4 *  , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexLldpXdot1dcbxRemETSBasicConfigurationTable ARG_LIST((UINT4 , UINT4 * , INT4 , INT4 * , UINT4 , UINT4 *  , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetLldpXdot1dcbxRemETSConCreditBasedShaperSupport ARG_LIST((UINT4  , INT4  , UINT4  , INT4 ,INT4 *));

INT1
nmhGetLldpXdot1dcbxRemETSConTrafficClassesSupported ARG_LIST((UINT4  , INT4  , UINT4  , INT4 ,UINT4 *));

INT1
nmhGetLldpXdot1dcbxRemETSConWilling ARG_LIST((UINT4  , INT4  , UINT4  , INT4 ,INT4 *));

/* Proto Validate Index Instance for LldpXdot1dcbxRemETSConPriorityAssignmentTable. */
INT1
nmhValidateIndexInstanceLldpXdot1dcbxRemETSConPriorityAssignmentTable ARG_LIST((UINT4  , INT4  , UINT4  , INT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for LldpXdot1dcbxRemETSConPriorityAssignmentTable  */

INT1
nmhGetFirstIndexLldpXdot1dcbxRemETSConPriorityAssignmentTable ARG_LIST((UINT4 * , INT4 * , UINT4 *  , INT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexLldpXdot1dcbxRemETSConPriorityAssignmentTable ARG_LIST((UINT4 , UINT4 * , INT4 , INT4 * , UINT4 , UINT4 *  , INT4 , INT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetLldpXdot1dcbxRemETSConPriTrafficClass ARG_LIST((UINT4  , INT4  , UINT4  , INT4  , UINT4 ,UINT4 *));

/* Proto Validate Index Instance for LldpXdot1dcbxRemETSConTrafficClassBandwidthTable. */
INT1
nmhValidateIndexInstanceLldpXdot1dcbxRemETSConTrafficClassBandwidthTable ARG_LIST((UINT4  , INT4  , UINT4  , INT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for LldpXdot1dcbxRemETSConTrafficClassBandwidthTable  */

INT1
nmhGetFirstIndexLldpXdot1dcbxRemETSConTrafficClassBandwidthTable ARG_LIST((UINT4 * , INT4 * , UINT4 *  , INT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexLldpXdot1dcbxRemETSConTrafficClassBandwidthTable ARG_LIST((UINT4 , UINT4 * , INT4 , INT4 * , UINT4 , UINT4 *  , INT4 , INT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetLldpXdot1dcbxRemETSConTrafficClassBandwidth ARG_LIST((UINT4  , INT4  , UINT4  , INT4  , UINT4 ,UINT4 *));

/* Proto Validate Index Instance for LldpXdot1dcbxRemETSConTrafficSelectionAlgorithmTable. */
INT1
nmhValidateIndexInstanceLldpXdot1dcbxRemETSConTrafficSelectionAlgorithmTable ARG_LIST((UINT4  , INT4  , UINT4  , INT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for LldpXdot1dcbxRemETSConTrafficSelectionAlgorithmTable  */

INT1
nmhGetFirstIndexLldpXdot1dcbxRemETSConTrafficSelectionAlgorithmTable ARG_LIST((UINT4 * , INT4 * , UINT4 *  , INT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexLldpXdot1dcbxRemETSConTrafficSelectionAlgorithmTable ARG_LIST((UINT4 , UINT4 * , INT4 , INT4 * , UINT4 , UINT4 *  , INT4 , INT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetLldpXdot1dcbxRemETSConTrafficSelectionAlgorithm ARG_LIST((UINT4  , INT4  , UINT4  , INT4  , UINT4 ,INT4 *));

/* Proto Validate Index Instance for LldpXdot1dcbxRemETSRecoTrafficClassBandwidthTable. */
INT1
nmhValidateIndexInstanceLldpXdot1dcbxRemETSRecoTrafficClassBandwidthTable ARG_LIST((UINT4  , INT4  , UINT4  , INT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for LldpXdot1dcbxRemETSRecoTrafficClassBandwidthTable  */

INT1
nmhGetFirstIndexLldpXdot1dcbxRemETSRecoTrafficClassBandwidthTable ARG_LIST((UINT4 * , INT4 * , UINT4 *  , INT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexLldpXdot1dcbxRemETSRecoTrafficClassBandwidthTable ARG_LIST((UINT4 , UINT4 * , INT4 , INT4 * , UINT4 , UINT4 *  , INT4 , INT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetLldpXdot1dcbxRemETSRecoTrafficClassBandwidth ARG_LIST((UINT4  , INT4  , UINT4  , INT4  , UINT4 ,UINT4 *));

/* Proto Validate Index Instance for LldpXdot1dcbxRemETSRecoTrafficSelectionAlgorithmTable. */
INT1
nmhValidateIndexInstanceLldpXdot1dcbxRemETSRecoTrafficSelectionAlgorithmTable ARG_LIST((UINT4  , INT4  , UINT4  , INT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for LldpXdot1dcbxRemETSRecoTrafficSelectionAlgorithmTable  */

INT1
nmhGetFirstIndexLldpXdot1dcbxRemETSRecoTrafficSelectionAlgorithmTable ARG_LIST((UINT4 * , INT4 * , UINT4 *  , INT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexLldpXdot1dcbxRemETSRecoTrafficSelectionAlgorithmTable ARG_LIST((UINT4 , UINT4 * , INT4 , INT4 * , UINT4 , UINT4 *  , INT4 , INT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetLldpXdot1dcbxRemETSRecoTrafficSelectionAlgorithm ARG_LIST((UINT4  , INT4  , UINT4  , INT4  , UINT4 ,INT4 *));

/* Proto Validate Index Instance for LldpXdot1dcbxRemPFCBasicTable. */
INT1
nmhValidateIndexInstanceLldpXdot1dcbxRemPFCBasicTable ARG_LIST((UINT4  , INT4  , UINT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for LldpXdot1dcbxRemPFCBasicTable  */

INT1
nmhGetFirstIndexLldpXdot1dcbxRemPFCBasicTable ARG_LIST((UINT4 * , INT4 * , UINT4 *  , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexLldpXdot1dcbxRemPFCBasicTable ARG_LIST((UINT4 , UINT4 * , INT4 , INT4 * , UINT4 , UINT4 *  , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetLldpXdot1dcbxRemPFCWilling ARG_LIST((UINT4  , INT4  , UINT4  , INT4 ,INT4 *));

INT1
nmhGetLldpXdot1dcbxRemPFCMBC ARG_LIST((UINT4  , INT4  , UINT4  , INT4 ,INT4 *));

INT1
nmhGetLldpXdot1dcbxRemPFCCap ARG_LIST((UINT4  , INT4  , UINT4  , INT4 ,UINT4 *));

/* Proto Validate Index Instance for LldpXdot1dcbxRemPFCEnableTable. */
INT1
nmhValidateIndexInstanceLldpXdot1dcbxRemPFCEnableTable ARG_LIST((UINT4  , INT4  , UINT4  , INT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for LldpXdot1dcbxRemPFCEnableTable  */

INT1
nmhGetFirstIndexLldpXdot1dcbxRemPFCEnableTable ARG_LIST((UINT4 * , INT4 * , UINT4 *  , INT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexLldpXdot1dcbxRemPFCEnableTable ARG_LIST((UINT4 , UINT4 * , INT4 , INT4 * , UINT4 , UINT4 *  , INT4 , INT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetLldpXdot1dcbxRemPFCEnableEnabled ARG_LIST((UINT4  , INT4  , UINT4  , INT4  , UINT4 ,INT4 *));

/* Proto Validate Index Instance for LldpXdot1dcbxRemApplicationPriorityAppTable. */
INT1
nmhValidateIndexInstanceLldpXdot1dcbxRemApplicationPriorityAppTable ARG_LIST((UINT4  , INT4  , UINT4  , INT4  , INT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for LldpXdot1dcbxRemApplicationPriorityAppTable  */

INT1
nmhGetFirstIndexLldpXdot1dcbxRemApplicationPriorityAppTable ARG_LIST((UINT4 * , INT4 * , UINT4 *  , INT4 * , INT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexLldpXdot1dcbxRemApplicationPriorityAppTable ARG_LIST((UINT4 , UINT4 * , INT4 , INT4 * , UINT4 , UINT4 *  , INT4 , INT4 * , INT4 , INT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetLldpXdot1dcbxRemApplicationPriorityAEPriority ARG_LIST((UINT4  , INT4  , UINT4  , INT4  , INT4  , UINT4 ,UINT4 *));

/* Proto Validate Index Instance for LldpXdot1dcbxAdminETSBasicConfigurationTable. */
INT1
nmhValidateIndexInstanceLldpXdot1dcbxAdminETSBasicConfigurationTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for LldpXdot1dcbxAdminETSBasicConfigurationTable  */

INT1
nmhGetFirstIndexLldpXdot1dcbxAdminETSBasicConfigurationTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexLldpXdot1dcbxAdminETSBasicConfigurationTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetLldpXdot1dcbxAdminETSConCreditBasedShaperSupport ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetLldpXdot1dcbxAdminETSConTrafficClassesSupported ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetLldpXdot1dcbxAdminETSConWilling ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetLldpXdot1dcbxAdminETSConWilling ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2LldpXdot1dcbxAdminETSConWilling ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2LldpXdot1dcbxAdminETSBasicConfigurationTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for LldpXdot1dcbxAdminETSConPriorityAssignmentTable. */
INT1
nmhValidateIndexInstanceLldpXdot1dcbxAdminETSConPriorityAssignmentTable ARG_LIST((INT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for LldpXdot1dcbxAdminETSConPriorityAssignmentTable  */

INT1
nmhGetFirstIndexLldpXdot1dcbxAdminETSConPriorityAssignmentTable ARG_LIST((INT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexLldpXdot1dcbxAdminETSConPriorityAssignmentTable ARG_LIST((INT4 , INT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetLldpXdot1dcbxAdminETSConPriTrafficClass ARG_LIST((INT4  , UINT4 ,UINT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetLldpXdot1dcbxAdminETSConPriTrafficClass ARG_LIST((INT4  , UINT4  ,UINT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2LldpXdot1dcbxAdminETSConPriTrafficClass ARG_LIST((UINT4 *  ,INT4  , UINT4  ,UINT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2LldpXdot1dcbxAdminETSConPriorityAssignmentTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for LldpXdot1dcbxAdminETSConTrafficClassBandwidthTable. */
INT1
nmhValidateIndexInstanceLldpXdot1dcbxAdminETSConTrafficClassBandwidthTable ARG_LIST((INT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for LldpXdot1dcbxAdminETSConTrafficClassBandwidthTable  */

INT1
nmhGetFirstIndexLldpXdot1dcbxAdminETSConTrafficClassBandwidthTable ARG_LIST((INT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexLldpXdot1dcbxAdminETSConTrafficClassBandwidthTable ARG_LIST((INT4 , INT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetLldpXdot1dcbxAdminETSConTrafficClassBandwidth ARG_LIST((INT4  , UINT4 ,UINT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetLldpXdot1dcbxAdminETSConTrafficClassBandwidth ARG_LIST((INT4  , UINT4  ,UINT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2LldpXdot1dcbxAdminETSConTrafficClassBandwidth ARG_LIST((UINT4 *  ,INT4  , UINT4  ,UINT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2LldpXdot1dcbxAdminETSConTrafficClassBandwidthTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for LldpXdot1dcbxAdminETSConTrafficSelectionAlgorithmTable. */
INT1
nmhValidateIndexInstanceLldpXdot1dcbxAdminETSConTrafficSelectionAlgorithmTable ARG_LIST((INT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for LldpXdot1dcbxAdminETSConTrafficSelectionAlgorithmTable  */

INT1
nmhGetFirstIndexLldpXdot1dcbxAdminETSConTrafficSelectionAlgorithmTable ARG_LIST((INT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexLldpXdot1dcbxAdminETSConTrafficSelectionAlgorithmTable ARG_LIST((INT4 , INT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetLldpXdot1dcbxAdminETSConTrafficSelectionAlgorithm ARG_LIST((INT4  , UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetLldpXdot1dcbxAdminETSConTrafficSelectionAlgorithm ARG_LIST((INT4  , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2LldpXdot1dcbxAdminETSConTrafficSelectionAlgorithm ARG_LIST((UINT4 *  ,INT4  , UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2LldpXdot1dcbxAdminETSConTrafficSelectionAlgorithmTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for LldpXdot1dcbxAdminETSRecoTrafficClassBandwidthTable. */
INT1
nmhValidateIndexInstanceLldpXdot1dcbxAdminETSRecoTrafficClassBandwidthTable ARG_LIST((INT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for LldpXdot1dcbxAdminETSRecoTrafficClassBandwidthTable  */

INT1
nmhGetFirstIndexLldpXdot1dcbxAdminETSRecoTrafficClassBandwidthTable ARG_LIST((INT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexLldpXdot1dcbxAdminETSRecoTrafficClassBandwidthTable ARG_LIST((INT4 , INT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetLldpXdot1dcbxAdminETSRecoTrafficClassBandwidth ARG_LIST((INT4  , UINT4 ,UINT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetLldpXdot1dcbxAdminETSRecoTrafficClassBandwidth ARG_LIST((INT4  , UINT4  ,UINT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2LldpXdot1dcbxAdminETSRecoTrafficClassBandwidth ARG_LIST((UINT4 *  ,INT4  , UINT4  ,UINT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2LldpXdot1dcbxAdminETSRecoTrafficClassBandwidthTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for LldpXdot1dcbxAdminETSRecoTrafficSelectionAlgorithmTable. */
INT1
nmhValidateIndexInstanceLldpXdot1dcbxAdminETSRecoTrafficSelectionAlgorithmTable ARG_LIST((INT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for LldpXdot1dcbxAdminETSRecoTrafficSelectionAlgorithmTable  */

INT1
nmhGetFirstIndexLldpXdot1dcbxAdminETSRecoTrafficSelectionAlgorithmTable ARG_LIST((INT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexLldpXdot1dcbxAdminETSRecoTrafficSelectionAlgorithmTable ARG_LIST((INT4 , INT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetLldpXdot1dcbxAdminETSRecoTrafficSelectionAlgorithm ARG_LIST((INT4  , UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetLldpXdot1dcbxAdminETSRecoTrafficSelectionAlgorithm ARG_LIST((INT4  , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2LldpXdot1dcbxAdminETSRecoTrafficSelectionAlgorithm ARG_LIST((UINT4 *  ,INT4  , UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2LldpXdot1dcbxAdminETSRecoTrafficSelectionAlgorithmTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for LldpXdot1dcbxAdminPFCBasicTable. */
INT1
nmhValidateIndexInstanceLldpXdot1dcbxAdminPFCBasicTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for LldpXdot1dcbxAdminPFCBasicTable  */

INT1
nmhGetFirstIndexLldpXdot1dcbxAdminPFCBasicTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexLldpXdot1dcbxAdminPFCBasicTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetLldpXdot1dcbxAdminPFCWilling ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetLldpXdot1dcbxAdminPFCMBC ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetLldpXdot1dcbxAdminPFCCap ARG_LIST((INT4 ,UINT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetLldpXdot1dcbxAdminPFCWilling ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2LldpXdot1dcbxAdminPFCWilling ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2LldpXdot1dcbxAdminPFCBasicTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for LldpXdot1dcbxAdminPFCEnableTable. */
INT1
nmhValidateIndexInstanceLldpXdot1dcbxAdminPFCEnableTable ARG_LIST((INT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for LldpXdot1dcbxAdminPFCEnableTable  */

INT1
nmhGetFirstIndexLldpXdot1dcbxAdminPFCEnableTable ARG_LIST((INT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexLldpXdot1dcbxAdminPFCEnableTable ARG_LIST((INT4 , INT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetLldpXdot1dcbxAdminPFCEnableEnabled ARG_LIST((INT4  , UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetLldpXdot1dcbxAdminPFCEnableEnabled ARG_LIST((INT4  , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2LldpXdot1dcbxAdminPFCEnableEnabled ARG_LIST((UINT4 *  ,INT4  , UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2LldpXdot1dcbxAdminPFCEnableTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for LldpXdot1dcbxAdminApplicationPriorityAppTable. */
INT1
nmhValidateIndexInstanceLldpXdot1dcbxAdminApplicationPriorityAppTable ARG_LIST((INT4  , INT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for LldpXdot1dcbxAdminApplicationPriorityAppTable  */

INT1
nmhGetFirstIndexLldpXdot1dcbxAdminApplicationPriorityAppTable ARG_LIST((INT4 * , INT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexLldpXdot1dcbxAdminApplicationPriorityAppTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetLldpXdot1dcbxAdminApplicationPriorityAEPriority ARG_LIST((INT4  , INT4  , UINT4 ,UINT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetLldpXdot1dcbxAdminApplicationPriorityAEPriority ARG_LIST((INT4  , INT4  , UINT4  ,UINT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2LldpXdot1dcbxAdminApplicationPriorityAEPriority ARG_LIST((UINT4 *  ,INT4  , INT4  , UINT4  ,UINT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2LldpXdot1dcbxAdminApplicationPriorityAppTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));
