/*************************************************************************
 * Copyright (C) 2010 Aricent Inc . All Rights Reserved
 * $Id: dcbxtrap.h,v 1.9 2016/05/25 10:06:09 siva Exp $
 * Description: This header file contains all the trap related
 *              macros and declaration for DCBX Module.
****************************************************************************/
#ifndef _DCBX_TRAP_H
#define _DCBX_TRAP_H

#define DCBX_OBJECT_NAME_LEN      256
#define DCBX_SNMPV2_TRAP_OID_LEN  11

#define DCBX_MAX_SYSLOG_MSG       5
#define PFC_TRAP_START            4
#define APP_PRI_TRAP_START        8
#define CEE_TRAP_START            12
#define DCBX_SNMP_TRAP_OID { 1, 3, 6, 1, 6, 3, 1, 1, 4, 1, 0 }

#define GET_SET_TRAP_POSITION(u4TempTrapId, u1TrapPos)\
{\
    while (u4TempTrapId)\
    {\
        if (u4TempTrapId&1)\
        {\
          break;\
        }\
        u1TrapPos++;\
        u4TempTrapId = u4TempTrapId>>1;\
    }\
}\

enum
{
    ETS_TRAP = 1,
    PFC_TRAP,
    APP_PRI_TRAP
};

enum
{
    MODULE_TRAP = 1,
    ADMIN_STATUS_TRAP,
    PEER_STATE_TRAP,
    OPER_STATE_TRAP
};

/*Used for sending Trap*/
typedef struct DcbxTrapMsg
{
    UINT4 u4IfIndex;   /* Port Number */
    UINT1 u1Module;    /* ETS/PFC */
    union
    {
        UINT1 u1ModuleStatus;
        UINT1 u1AdminMode;
        UINT1 u1PeerStatus;
        UINT1 u1OperState;
    }unTrapMsg;
    UINT1 au1Pad[2];  /* Pad */
}tDcbxTrapMsg;

typedef struct DcbxCEETrapMsg
{
    UINT4 u4IfIndex;    /*Port Number*/
    union
    {
        UINT1 u1OperVersion;
        UINT1 u1FeatType;
    }unTrapMsg;
    UINT1 au1Pad[3];
}tDcbxCEETrapMsg;

tSNMP_OID_TYPE* DcbxMakeObjIdFrmString  PROTO ((INT1 *pi1TextStr,
                                              UINT1 *pu1TableName));
VOID DcbxSendNotification PROTO ((tDcbxTrapMsg *pNotifyMsg, UINT1 u1TrapId));

VOID DcbxCEESendPortNotification (tDcbxCEETrapMsg * pNotifyInfo, UINT4 u4TrapId);

#endif

