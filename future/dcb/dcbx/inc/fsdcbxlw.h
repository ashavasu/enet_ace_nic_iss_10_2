/********************************************************************
* Copyright (C) 2010 Aricent Inc . All Rights Reserved
*
* $Id: fsdcbxlw.h,v 1.8 2016/07/09 09:41:20 siva Exp $
*
* Description: Proto types for Low Level  Routines
*********************************************************************/

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsDcbPfcMinThreshold ARG_LIST((UINT4 *));

INT1
nmhGetFsDcbPfcMaxThreshold ARG_LIST((UINT4 *));

INT1
nmhGetFsDcbMaxPfcProfiles ARG_LIST((UINT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsDcbPfcMinThreshold ARG_LIST((UINT4 ));

INT1
nmhSetFsDcbPfcMaxThreshold ARG_LIST((UINT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsDcbPfcMinThreshold ARG_LIST((UINT4 *  ,UINT4 ));

INT1
nmhTestv2FsDcbPfcMaxThreshold ARG_LIST((UINT4 *  ,UINT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsDcbPfcMinThreshold ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsDcbPfcMaxThreshold ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsDcbPortTable. */
INT1
nmhValidateIndexInstanceFsDcbPortTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsDcbPortTable  */

INT1
nmhGetFirstIndexFsDcbPortTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsDcbPortTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsDcbETSAdminStatus ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsDcbPFCAdminStatus ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsDcbRowStatus ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsDcbAppPriAdminStatus ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsDcbOperVersion ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsDcbMaxVersion ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsDcbPeerOperVersion ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsDcbPeerMaxVersion ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsDcbRowStatus ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsDcbRowStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));


/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsDcbPortTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsDcbxGlobalTraceLevel ARG_LIST((INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsDcbxGlobalTraceLevel ARG_LIST((INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsDcbxGlobalTraceLevel ARG_LIST((UINT4 *  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsDcbxGlobalTraceLevel ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsDCBXPortTable. */
INT1
nmhValidateIndexInstanceFsDCBXPortTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsDCBXPortTable  */

INT1
nmhGetFirstIndexFsDCBXPortTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsDCBXPortTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsDCBXAdminStatus ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsDCBXMode ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsDCBXAdminStatus ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsDCBXMode ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsDCBXAdminStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsDCBXMode ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsDCBXPortTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsETSSystemControl ARG_LIST((INT4 *));

INT1
nmhGetFsETSModuleStatus ARG_LIST((INT4 *));

INT1
nmhGetFsETSClearCounters ARG_LIST((INT4 *));

INT1
nmhGetFsETSGlobalEnableTrap ARG_LIST((INT4 *));

INT1
nmhGetFsETSGeneratedTrapCount ARG_LIST((UINT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsETSSystemControl ARG_LIST((INT4 ));

INT1
nmhSetFsETSModuleStatus ARG_LIST((INT4 ));

INT1
nmhSetFsETSClearCounters ARG_LIST((INT4 ));

INT1
nmhSetFsETSGlobalEnableTrap ARG_LIST((INT4 ));

INT1
nmhSetFsETSGeneratedTrapCount ARG_LIST((UINT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsETSSystemControl ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsETSModuleStatus ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsETSClearCounters ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsETSGlobalEnableTrap ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsETSGeneratedTrapCount ARG_LIST((UINT4 *  ,UINT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsETSSystemControl ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsETSModuleStatus ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsETSClearCounters ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsETSGlobalEnableTrap ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsETSGeneratedTrapCount ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsETSPortTable. */
INT1
nmhValidateIndexInstanceFsETSPortTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsETSPortTable  */

INT1
nmhGetFirstIndexFsETSPortTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsETSPortTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsETSAdminMode ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsETSDcbxOperState ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsETSDcbxStateMachine ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsETSClearTLVCounters ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsETSConfTxTLVCounter ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsETSConfRxTLVCounter ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsETSConfRxTLVErrors ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsETSRecoTxTLVCounter ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsETSRecoRxTLVCounter ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsETSRecoRxTLVErrors ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsETSTcSuppTxTLVCounter ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsETSTcSuppRxTLVCounter ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsETSTcSuppRxTLVErrors ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsETSRowStatus ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsETSSyncd ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsETSError ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsETSDcbxStatus ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsETSAdminMode ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsETSClearTLVCounters ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsETSRowStatus ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsETSAdminMode ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsETSClearTLVCounters ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsETSRowStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsETSPortTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsPFCSystemControl ARG_LIST((INT4 *));

INT1
nmhGetFsPFCModuleStatus ARG_LIST((INT4 *));

INT1
nmhGetFsPFCClearCounters ARG_LIST((INT4 *));

INT1
nmhGetFsPFCGlobalEnableTrap ARG_LIST((INT4 *));

INT1
nmhGetFsPFCGeneratedTrapCount ARG_LIST((UINT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsPFCSystemControl ARG_LIST((INT4 ));

INT1
nmhSetFsPFCModuleStatus ARG_LIST((INT4 ));

INT1
nmhSetFsPFCClearCounters ARG_LIST((INT4 ));

INT1
nmhSetFsPFCGlobalEnableTrap ARG_LIST((INT4 ));

INT1
nmhSetFsPFCGeneratedTrapCount ARG_LIST((UINT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsPFCSystemControl ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsPFCModuleStatus ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsPFCClearCounters ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsPFCGlobalEnableTrap ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsPFCGeneratedTrapCount ARG_LIST((UINT4 *  ,UINT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsPFCSystemControl ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsPFCModuleStatus ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsPFCClearCounters ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsPFCGlobalEnableTrap ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsPFCGeneratedTrapCount ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsPFCPortTable. */
INT1
nmhValidateIndexInstanceFsPFCPortTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsPFCPortTable  */

INT1
nmhGetFirstIndexFsPFCPortTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsPFCPortTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsPFCAdminMode ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsPFCDcbxOperState ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsPFCDcbxStateMachine ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsPFCClearTLVCounters ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsPFCTxTLVCounter ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsPFCRxTLVCounter ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsPFCRxTLVErrors ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsPFCRowStatus ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsPFCSyncd ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsPFCError ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsPFCDcbxStatus ARG_LIST ((INT4 , INT4 *));

INT1
nmhGetFsPFCRxPauseFrameCounter ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsPFCTxPauseFrameCounter ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsPFCRxPauseFrameP0Counter ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsPFCRxPauseFrameP1Counter ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsPFCRxPauseFrameP2Counter ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsPFCRxPauseFrameP3Counter ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsPFCRxPauseFrameP4Counter ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsPFCRxPauseFrameP5Counter ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsPFCRxPauseFrameP6Counter ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsPFCRxPauseFrameP7Counter ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsPFCTxPauseFrameP0Counter ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsPFCTxPauseFrameP1Counter ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsPFCTxPauseFrameP2Counter ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsPFCTxPauseFrameP3Counter ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsPFCTxPauseFrameP4Counter ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsPFCTxPauseFrameP5Counter ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsPFCTxPauseFrameP6Counter ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsPFCTxPauseFrameP7Counter ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsPFCDataFrameDiscardCounter ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsPFCClearPauseFrameCounters ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsPFCAdminMode ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsPFCClearTLVCounters ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsPFCRowStatus ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsPFCClearPauseFrameCounters ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsPFCAdminMode ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsPFCClearTLVCounters ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsPFCRowStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsPFCClearPauseFrameCounters ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsPFCPortTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsAppPriSystemControl ARG_LIST((INT4 *));

INT1
nmhGetFsAppPriModuleStatus ARG_LIST((INT4 *));

INT1
nmhGetFsAppPriClearCounters ARG_LIST((INT4 *));

INT1
nmhGetFsAppPriGlobalEnableTrap ARG_LIST((INT4 *));

INT1
nmhGetFsAppPriGeneratedTrapCount ARG_LIST((UINT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsAppPriSystemControl ARG_LIST((INT4 ));

INT1
nmhSetFsAppPriModuleStatus ARG_LIST((INT4 ));

INT1
nmhSetFsAppPriClearCounters ARG_LIST((INT4 ));

INT1
nmhSetFsAppPriGlobalEnableTrap ARG_LIST((INT4 ));

INT1
nmhSetFsAppPriGeneratedTrapCount ARG_LIST((UINT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsAppPriSystemControl ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsAppPriModuleStatus ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsAppPriClearCounters ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsAppPriGlobalEnableTrap ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsAppPriGeneratedTrapCount ARG_LIST((UINT4 *  ,UINT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsAppPriSystemControl ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsAppPriModuleStatus ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsAppPriClearCounters ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsAppPriGlobalEnableTrap ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsAppPriGeneratedTrapCount ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsAppPriPortTable. */
INT1
nmhValidateIndexInstanceFsAppPriPortTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsAppPriPortTable  */

INT1
nmhGetFirstIndexFsAppPriPortTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsAppPriPortTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsAppPriAdminMode ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsAppPriDcbxOperState ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsAppPriDcbxStateMachine ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsAppPriClearTLVCounters ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsAppPriTxTLVCounter ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsAppPriRxTLVCounter ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsAppPriRxTLVErrors ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsAppPriAppProtocols ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsAppPriRowStatus ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsAppPriSyncd ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsAppPriError ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsAppPriDcbxStatus ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsAppPriAdminMode ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsAppPriClearTLVCounters ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsAppPriRowStatus ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsAppPriAdminMode ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsAppPriClearTLVCounters ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsAppPriRowStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsAppPriPortTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsAppPriXAppTable. */
INT1
nmhValidateIndexInstanceFsAppPriXAppTable ARG_LIST((INT4  , INT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsAppPriXAppTable  */

INT1
nmhGetFirstIndexFsAppPriXAppTable ARG_LIST((INT4 * , INT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsAppPriXAppTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsAppPriXAppRowStatus ARG_LIST((INT4  , INT4  , UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsAppPriXAppRowStatus ARG_LIST((INT4  , INT4  , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsAppPriXAppRowStatus ARG_LIST((UINT4 *  ,INT4  , INT4  , UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsAppPriXAppTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FslldpXdot1dcbxLocApplicationPriorityTable. */
INT1
nmhValidateIndexInstanceFslldpXdot1dcbxLocApplicationPriorityTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for FslldpXdot1dcbxLocApplicationPriorityTable  */

INT1
nmhGetFirstIndexFslldpXdot1dcbxLocApplicationPriorityTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFslldpXdot1dcbxLocApplicationPriorityTable ARG_LIST((INT4 , INT4 *));

/* Proto Validate Index Instance for FslldpXdot1dcbxLocApplicationPriorityBasicTable. */
INT1
nmhValidateIndexInstanceFslldpXdot1dcbxLocApplicationPriorityBasicTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for FslldpXdot1dcbxLocApplicationPriorityBasicTable  */

INT1
nmhGetFirstIndexFslldpXdot1dcbxLocApplicationPriorityBasicTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFslldpXdot1dcbxLocApplicationPriorityBasicTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFslldpXdot1dcbxLocApplicationPriorityWilling ARG_LIST((INT4 ,INT4 *));

/* Proto Validate Index Instance for FslldpXdot1dcbxAdminApplicationPriorityBasicTable. */
INT1
nmhValidateIndexInstanceFslldpXdot1dcbxAdminApplicationPriorityBasicTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for FslldpXdot1dcbxAdminApplicationPriorityBasicTable  */

INT1
nmhGetFirstIndexFslldpXdot1dcbxAdminApplicationPriorityBasicTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFslldpXdot1dcbxAdminApplicationPriorityBasicTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFslldpXdot1dcbxAdminApplicationPriorityWilling ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFslldpXdot1dcbxAdminApplicationPriorityWilling ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FslldpXdot1dcbxAdminApplicationPriorityWilling ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FslldpXdot1dcbxAdminApplicationPriorityBasicTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FslldpXdot1dcbxRemApplicationPriorityBasicTable. */
INT1
nmhValidateIndexInstanceFslldpXdot1dcbxRemApplicationPriorityBasicTable ARG_LIST((UINT4  , INT4  , UINT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for FslldpXdot1dcbxRemApplicationPriorityBasicTable  */

INT1
nmhGetFirstIndexFslldpXdot1dcbxRemApplicationPriorityBasicTable ARG_LIST((UINT4 * , INT4 * , UINT4 *  , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFslldpXdot1dcbxRemApplicationPriorityBasicTable ARG_LIST((UINT4 , UINT4 * , INT4 , INT4 * , UINT4 , UINT4 *  , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFslldpXdot1dcbxRemApplicationPriorityWilling ARG_LIST((UINT4  , INT4  , UINT4  , INT4 ,INT4 *));

/* Proto Validate Index Instance for FslldpXdot1dcbxConfigTCSupportedTable. */
INT1
nmhValidateIndexInstanceFslldpXdot1dcbxConfigTCSupportedTable ARG_LIST((INT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FslldpXdot1dcbxConfigTCSupportedTable  */

INT1
nmhGetFirstIndexFslldpXdot1dcbxConfigTCSupportedTable ARG_LIST((INT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFslldpXdot1dcbxConfigTCSupportedTable ARG_LIST((INT4 , INT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFslldpXdot1dcbxConfigTCSupportedTxEnable ARG_LIST((INT4  , UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFslldpXdot1dcbxConfigTCSupportedTxEnable ARG_LIST((INT4  , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FslldpXdot1dcbxConfigTCSupportedTxEnable ARG_LIST((UINT4 *  ,INT4  , UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FslldpXdot1dcbxConfigTCSupportedTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FslldpXdot1dcbxLocTCSupportedTable. */
INT1
nmhValidateIndexInstanceFslldpXdot1dcbxLocTCSupportedTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for FslldpXdot1dcbxLocTCSupportedTable  */

INT1
nmhGetFirstIndexFslldpXdot1dcbxLocTCSupportedTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFslldpXdot1dcbxLocTCSupportedTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFslldpXdot1dcbxLocTCSupported ARG_LIST((INT4 ,INT4 *));

/* Proto Validate Index Instance for FslldpXdot1dcbxRemTCSupportedTable. */
INT1
nmhValidateIndexInstanceFslldpXdot1dcbxRemTCSupportedTable ARG_LIST((UINT4  , INT4  , UINT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for FslldpXdot1dcbxRemTCSupportedTable  */

INT1
nmhGetFirstIndexFslldpXdot1dcbxRemTCSupportedTable ARG_LIST((UINT4 * , INT4 * , UINT4 *  , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFslldpXdot1dcbxRemTCSupportedTable ARG_LIST((UINT4 , UINT4 * , INT4 , INT4 * , UINT4 , UINT4 *  , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFslldpXdot1dcbxRemTCSupported ARG_LIST((UINT4  , INT4  , UINT4  , INT4 ,INT4 *));

/* Proto Validate Index Instance for FslldpXdot1dcbxAdminTCSupportedTable. */
INT1
nmhValidateIndexInstanceFslldpXdot1dcbxAdminTCSupportedTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for FslldpXdot1dcbxAdminTCSupportedTable  */

INT1
nmhGetFirstIndexFslldpXdot1dcbxAdminTCSupportedTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFslldpXdot1dcbxAdminTCSupportedTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFslldpXdot1dcbxAdminTCSupported ARG_LIST((INT4 ,INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsDcbxCEEGlobalEnableTrap ARG_LIST((INT4 *));

INT1
nmhGetFsDcbxCEEGeneratedTrapCount ARG_LIST((UINT4 *));

INT1
nmhGetFsDcbxCEEClearCounters ARG_LIST((INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsDcbxCEEGlobalEnableTrap ARG_LIST((INT4 ));

INT1
nmhSetFsDcbxCEEClearCounters ARG_LIST((INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsDcbxCEEGlobalEnableTrap ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsDcbxCEEClearCounters ARG_LIST((UINT4 *  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsDcbxCEEGlobalEnableTrap ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsDcbxCEEClearCounters ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsDcbxCEECtrlTable. */
INT1
nmhValidateIndexInstanceFsDcbxCEECtrlTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsDcbxCEECtrlTable  */

INT1
nmhGetFirstIndexFsDcbxCEECtrlTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsDcbxCEECtrlTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsDcbxCEECtrlSeqNo ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsDcbxCEECtrlAckNo ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsDcbxCEECtrlRcvdAckNo ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsDcbxCEECtrlTxTLVCounter ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsDcbxCEECtrlRxTLVCounter ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsDcbxCEECtrlRxTLVErrorCounter ARG_LIST((INT4 ,UINT4 *));
INT1
nmhSetFsDcbxCEETlvClearCounters ARG_LIST ((INT4 , INT4 ));

INT1 
nmhTestv2FsDcbxCEETlvClearCounters ARG_LIST ((UINT4 *, INT4 , INT4 ));

INT1 
nmhGetFsDcbPeerNotAdvDcbx ARG_LIST((INT4 , INT4 *));

