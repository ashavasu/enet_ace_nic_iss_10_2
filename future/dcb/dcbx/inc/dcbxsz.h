/*************************************************************************
 * Copyright (C) 2010 Aricent Inc . All Rights Reserved
 * $Id: dcbxsz.h,v 1.2 2010/10/28 11:49:52 prabuc Exp $
 * Description: This header file contains all mempool prototypes 
 *              declaration for DCBX Module.
****************************************************************************/
enum {
    MAX_DCBX_APP_TABLE_ENTRIES_SIZING_ID,
    MAX_DCBX_MBSM_MAX_LC_SLOTS_SIZING_ID,
    MAX_DCBX_PORT_TABLE_ENTRIES_SIZING_ID,
    MAX_DCBX_QUEUE_MESG_SIZING_ID,
    DCBX_MAX_SIZING_ID
};


#ifdef  _DCBXSZ_C
tMemPoolId DCBXMemPoolIds[ DCBX_MAX_SIZING_ID];
INT4  DcbxSizingMemCreateMemPools(VOID);
VOID  DcbxSizingMemDeleteMemPools(VOID);
INT4  DcbxSzRegisterModuleSizingParams( CHR1 *pu1ModName);
#else  /*  _DCBXSZ_C  */
extern tMemPoolId DCBXMemPoolIds[ ];
extern INT4  DcbxSizingMemCreateMemPools(VOID);
extern VOID  DcbxSizingMemDeleteMemPools(VOID);
extern INT4  DcbxSzRegisterModuleSizingParams( CHR1 *pu1ModName); 
#endif /*  _DCBXSZ_C  */


#ifdef  _DCBXSZ_C
tFsModSizingParams FsDCBXSizingParams [] = {
{ "tDcbxAppEntry", "MAX_DCBX_APP_TABLE_ENTRIES", sizeof(tDcbxAppEntry),MAX_DCBX_APP_TABLE_ENTRIES, MAX_DCBX_APP_TABLE_ENTRIES,0 },
{ "tMbsmProtoMsg", "MAX_DCBX_MBSM_MAX_LC_SLOTS", sizeof(tMbsmProtoMsg),MAX_DCBX_MBSM_MAX_LC_SLOTS, MAX_DCBX_MBSM_MAX_LC_SLOTS,0 },
{ "tDcbxPortEntry", "MAX_DCBX_PORT_TABLE_ENTRIES", sizeof(tDcbxPortEntry),MAX_DCBX_PORT_TABLE_ENTRIES, MAX_DCBX_PORT_TABLE_ENTRIES,0 },
{ "tDcbxQueueMsg", "MAX_DCBX_QUEUE_MESG", sizeof(tDcbxQueueMsg),MAX_DCBX_QUEUE_MESG, MAX_DCBX_QUEUE_MESG,0 },
{"\0","\0",0,0,0,0}
};
#else  /*  _DCBXSZ_C  */
extern tFsModSizingParams FsDCBXSizingParams [];
#endif /*  _DCBXSZ_C  */


