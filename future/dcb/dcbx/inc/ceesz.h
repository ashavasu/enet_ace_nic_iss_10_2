/*************************************************************************
 * Copyright (C) 2010 Aricent Inc . All Rights Reserved
 * $Id: ceesz.h,v 1.1 2016/03/05 12:19:43 siva Exp $
 * Description: This header file contains all mempool prototypes 
 *              declaration for DCBX CEE Module.
****************************************************************************/
enum {
    MAX_DCBX_CEE_PORT_ENTRIES_SIZING_ID,
    CEE_MAX_SIZING_ID
};


#ifdef  _CEESZ_C
tMemPoolId CEEMemPoolIds[ CEE_MAX_SIZING_ID];
INT4  CeeSizingMemCreateMemPools(VOID);
VOID  CeeSizingMemDeleteMemPools(VOID);
INT4  CeeSzRegisterModuleSizingParams( CHR1 *pu1ModName);
tFsModSizingParams FsCEESizingParams [] = {
{ "tCEECtrlEntry", "MAX_DCBX_PORT_TABLE_ENTRIES", sizeof(tCEECtrlEntry),MAX_DCBX_PORT_TABLE_ENTRIES, MAX_DCBX_PORT_TABLE_ENTRIES,0 },
{"\0","\0",0,0,0,0}
};
#else  /*  _CEESZ_C  */
extern tMemPoolId CEEMemPoolIds[ ];
extern INT4  CeeSizingMemCreateMemPools(VOID);
extern VOID  CeeSizingMemDeleteMemPools(VOID);
extern INT4  CeeSzRegisterModuleSizingParams( CHR1 *pu1ModName); 
extern tFsModSizingParams FsCEESizingParams [];
#endif /*  _CEESZ_C  */

