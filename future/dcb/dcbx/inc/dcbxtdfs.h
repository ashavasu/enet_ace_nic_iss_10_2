/*************************************************************************
 * Copyright (C) 2010 Aricent Inc . All Rights Reserved
 * $Id: dcbxtdfs.h,v 1.18 2017/12/06 09:29:07 siva Exp $
 * Description: This header file contains all the data structure
 *              declaration for DCBX Module.
****************************************************************************/
#ifndef __DCBX_TDFS_H__
#define __DCBX_TDFS_H__

#define QOS_HL_DEF_S1_SCHEDULER_ID          1 /*Default Scheduler ID for Level1 Scheduler*/
#define QOS_HL_DEF_S2_SCHEDULER_ID          2 /*Default Scheduler ID for Level2 Scheduler*/
#define QOS_HL_DEF_S3_SCHEDULER1_ID         3 /*Default Scheduler ID for Level3 Scheduler*/
#define QOS_HL_DEF_S3_SCHEDULER2_ID         4 /*Default Scheduler ID for Level3 Scheduler*/
#define QOS_HL_DEF_S3_SCHEDULER3_ID         5 /*Default Scheduler ID for Level3 Scheduler*/
#define QOS_HL_DEF_S3_SCHEDULER4_ID         6 /*Default Scheduler ID for Level3 Scheduler*/
#define QOS_HL_DEF_S3_SCHEDULER5_ID         7 /*Default Scheduler ID for Level3 Scheduler*/
#define QOS_HL_DEF_S3_SCHEDULER6_ID         8 /*Default Scheduler ID for Level3 Scheduler*/
#define QOS_HL_DEF_S3_SCHEDULER7_ID         9 /*Default Scheduler ID for Level3 Scheduler*/
#define QOS_HL_DEF_S3_SCHEDULER8_ID         10 /*Default Scheduler ID for Level3 Scheduler*/
#define QOS_HL_DEF_S3_SCHEDULER9_ID         11 /*Default Scheduler ID for Level3 Scheduler*/
#define QOS_HL_DEF_S3_SCHEDULER10_ID        12 /*Default Scheduler ID for Level3 Scheduler*/
#define QOS_HL_DEF_S3_SCHEDULER11_ID        13 /*Default Scheduler ID for Level3 Scheduler*/

/*update the number of TCGID is 15. update corresponding data structure also*/

/*DCBX Module Data Structures*/

/* DCBX HA related structure */
typedef struct _tDcbxRedGlobalIndo 
{
    UINT1        u1NodeStatus;          /* Node Status - Idle/Standby/Active */
    UINT1        u1NumOfStandbyNodesUp; /* Number of Standby nodes which are
                                           booted up.If this value is 0, then dynamic
                                           messages are not sent by active node*/
    BOOL1        bBulkReqRcvd;         /* Bulk Request Received Flag 
                                          OSIX_TRUE/OSIX_FALSE.This flag is set 
                                          when the dynamic bulk request message 
                                          reaches the active node before the 
                                          STANDBY_UP event. Bulk updates are 
                                          sent only after STANDBY_UP event is
                                          reached.*/  
    UINT1        au1Padding[1];
}tDcbxRedGlobalInfo;

/* DCBX Global Structure */
typedef struct _tDcbxGlobalInfo
{
    tOsixTaskId DcbxTaskId;        /*DCBX Task ID*/
    tOsixQId    DcbxPktQId;        /*DCBX Queue ID*/ 
    tOsixSemId  DcbxSemId;         /*Semaphore ID*/
    tMemPoolId  DcbxPortPoolId;    /*Pool Id for DCBX Port Table*/
    tMemPoolId  DcbxQPktPoolId;    /*Pool ID for DCBX Queue Msg*/
    tMemPoolId  DcbxApplPoolId;    /*Pool ID for DCBX Application Table*/
    tMemPoolId  DcbxMbsmPoolId;    /*Pool ID for DCBX MBSM Message*/
    tRBTree     pRbDcbxPortTbl;    /*DCBX Port Table Header*/   
    tRBTree     pRbDcbxAppTbl;     /*DCBX Port Table Header*/  
    tDcbxRedGlobalInfo DcbxRedGlobalInfo; /* HA Global structure */
    UINT4       u4DCBXTrcFlag;     /*DCBX Trace Flag */ 
    UINT1       u1DCBXSystemCtrl;  /*DCBX System Control Status */ 
    UINT1       au1Padding[3];  
}tDcbxGlobalInfo;

/* DCBX - Application ID Strucuture */
typedef struct _tDcbxAppId
{
    UINT1    au1OUI[DCBX_MAX_OUI_LEN]; /*TLV OUI */
    UINT1    u1SubType;                /*TLV Sub Type */
    UINT2    u2TlvType;                /*TLV Type */
    UINT1    au1Padding[2];            
}tDcbxAppId;

/* DCBX - Application TLV Parameter strucutre which gives 
 * the infomration of TLV from LLDP to Application */
typedef struct _tApplTlvParam
{
    UINT1    au1DcbxTlv[DCBX_TLV_LEN]; /*DCBX TLV from LLDP.*/
    INT4     i4RemIndex;               /*Remote Index for the remote entry*/
    UINT4    u4RemLastUpdateTime;      /*Remote Last Update time*/
    UINT4    u4RemLocalDestMACIndex;   /*Remote Destination MAC Index for this TLV*/
    tMacAddr RemLocalDestMacAddr;      /*Remote Destination MAC for this TLV*/
    tMacAddr RemNodeMacAddr;      /* Source MAC of the remote Node */
    UINT2    u2RxTlvLen;               /*TLV Receive length. */
    UINT1    au1Padding[2];
}tApplTlvParam;

typedef struct _tDcbxRmMsg
{
   tRmMsg       *pData;        /* Pointer to received message info from RM */
   UINT2        u2DataLen;     /* Length of the message received from RM */
   UINT1        u1Event;       /* Event received from RM */
   UINT1        au1Padding[1]; /* Padding bytes */
}tDcbxRmMsg;

/* DCBX - Queue Structure */
typedef struct _tDcbxQueueMsg 
{
    UINT4  u4IfIndex;                /*Port Number*/
    UINT4  u4MsgType;                /*Message Type*/
    tDcbxAppId DcbxAppId;            /*Application Id*/
    union
    {
        UINT4         u4PortChannelId;/* PortChannel ID in which the 
                                        u4IfIndex is a member port. */
        tApplTlvParam ApplTlvParam;   /* Application TLV Paramter */
        tDcbxRmMsg    DcbxRmMsg;      /* RM Message Strucuture */
#ifdef MBSM_WANTED
        tMbsmProtoMsg *pMbsmProtoMsg;
#endif
    }unMsgParam;
} tDcbxQueueMsg;

/* DCBX - Msg to applications - Argument for appln call back functions */
typedef struct _tDcbxAppInfo
{
    UINT4       u4IfIndex;        /*Interface Index Number */
    UINT4       u4MsgType;        /*Event to application Whether TLV update 
                                        *or oper state update*/
    tApplTlvParam    ApplTlvParam;     /*TLV Related information */
    UINT1       u1ApplOperState;  /*If Msg type is oper state update
                                        * then Init or Recommended*/
    UINT1       au1Padding[3];
}tDcbxAppInfo;

/* Application(ETS/PFC/Application Priority) specific info stored in DCBX */
typedef struct _tDcbxAppRegInfo
{
    tDcbxAppId  DcbxAppId;            /*Application Id */
    VOID        (*pApplCallBackFn) (tDcbxAppInfo *); 
                                      /*Application callback fn*/
                                      /*DCBX delivers the TLV rcvd to 
                                       *application through this FnPtr.*/
                                      /*DCBX delivers the Port oper state to 
                                       *application thru this FnPtr.*/
    UINT1       *pu1ApplTlv;          /*TLV information */
    UINT4       u4RemLocalDestMACIndex;
                                      /*Remote Destination MAC Index */ 
    UINT2       u2TlvLen;             /*Transmit TLV Length */
    UINT1       u1ApplLocalWilling;   /*Local Willing Status */
    UINT1       u1ApplRemoteWilling;  /*Remote Willing Status */
    UINT1      au1RemDestMacAddr[MAC_ADDR_LEN]; 
                                      /*Remote Destination MAC Address */
    UINT1       u1ApplStateMachineType;/*State Macine Type */
    UINT1       au1Padding[1];
}tDcbxAppRegInfo;

/*Dcbx Port Table*/
typedef struct _tDcbxPortEntry
{
    tRBNodeEmbd RbNode;             /*Link to Traverse the Table    */                   
    UINT4       u4IfIndex;          /*Interface Index of the entry  */
    UINT1       u1DcbxAdminStatus;  /*Enabled or Disabled status of 
                                     *DCBX on a Port */
    UINT1       u1DcbxMode;         /*Dcbx Mode */  
    UINT1       u1ETSAdminStatus;   /*ETS Admin Status */
    UINT1       u1PFCAdminStatus;   /*PFC Admin Status */
    UINT1       u1AppPriAdminStatus;/* Applicaion Priority Admin Status */
    UINT1       u1OperVersion;      /* Local Operating Version */
    UINT1       u1MaxVersion;       /* Local Maximum Operating Version */
    UINT1       u1PeerOperVersion;  /* Remote Operating Version*/
    UINT1       u1PeerMaxVersion;   /* Remote Maximum Operating Version*/
    UINT1       u1LldpTxStatus;
    UINT1       u1LldpRxStatus;
    UINT1       au1Padding[1];

}tDcbxPortEntry;

/* DCBX Application Port list used in Application Table */
typedef UINT1 tApplMappedPortList[DCBX_MAX_PORT_LIST_SIZE];

/*DCBX Application Table*/
typedef struct _tDcbxAppEntry
{
    tRBNodeEmbd          RbNode;          /*Link to traverse the table */
    tDcbxAppId           DcbxAppId;       /*Application Id */
    VOID                (*pAppCallBackFn)(tDcbxAppInfo *); 
                                          /*Application Callback fn*/
    tApplMappedPortList ApplMappedIfPortList;
                                         /*Port list array to store the 
                                           *registration status of the port*/
}tDcbxAppEntry;

/* ETS related Data Strucuture */

/* ETS Global Strucuture */
typedef struct _tETSGlobalInfo
{
    tMemPoolId  ETSPortPoolId;    /*Pool Id for ETS Port*/
    tRBTree     pRbETSPortTbl;    /*ETS Port Table Header*/   
    UINT4       u4ETSTrap;       /*Enabled or Disabled Status of Trap*/
    UINT4       u4ETSGeneratedTrapCount; /*ETS Trap Count*/
    UINT1       u1ETSSystemCtrl;  /*Start or Shutdown status of ETS Module*/
    UINT1       u1ETSModStatus;   /*Enabled or Disabled status of ETS Module*/
    UINT1       au1Padding[2];
}tETSGlobalInfo;

/* Ets Qos Hw Global Structure */
typedef struct _tETSQosPortInfo
{
    UINT4      u4EtsUsedUcScheId[ETS_MAX_S3_SCHID_CONF];
    UINT4      u4EtsUsedUcScheCnt;
    UINT4      u4EtsUsedMcScheId;
    UINT4      u4EtsUsedMcScheCnt;
    UINT4      u4EtsUsedUcQueueId[ETS_MAX_TCGID_CONF];
    UINT4      u4EtsUsedUcQueueCnt;
    UINT4      u4EtsQToSchIndex[ETS_MAX_TCGID_CONF+1];
}tETSQosPortInfo;

/*ETS Port Local information Structure*/
typedef struct _tETSLocPortInfo
{
    UINT1 au1ETSLocBW [ETS_MAX_NUM_TCGID];
                                  /*Bandwidth Allocated to the TCGID
                                   of the LocalSystem*/
    UINT1 au1ETSLocTCGID [DCBX_MAX_PRIORITIES];
                                 /*TCGID associated to the Priority
                                   of the LocalSystem*/
    UINT1 au1ETSLocTsaTable [ETS_MAX_TCGID_CONF];
                                  /* traffic selection algorithm associated to TC of the LocalSystem */ 
    UINT1 au1ETSLocRecoBW [ETS_MAX_NUM_TCGID];
                                 /*Recommended bandwidth*/
    UINT1 au1ETSLocRecoTsaTable [ETS_MAX_TCGID_CONF];
                                 /*Recommended traffic selection algorithm table entry*/
    UINT1 u1ETSLocNumTcSup;      /*Number of TC supported in the
                                  *Localsystem*/
    UINT1 u1ETSLocNumTCGSup;     /*Number of TCG Supported in the
                                  *LocalSystem*/
    UINT1 u1ETSLocWilling;       /*ETS Willing status of the LocalSystem*/
    UINT1 u1ETSLocCBS;           /*Credit based shaper status of the
                                   Local System*/
}tETSLocPortInfo;

/* ETS - Admin Information strucutre */
typedef struct _tETSAdmPortInfo
{
    UINT1 au1ETSAdmBW [ETS_MAX_NUM_TCGID];
                                 /*Configured bandwidth of the TCGID*/
    UINT1 au1ETSAdmTCGID [DCBX_MAX_PRIORITIES];
                                 /*Configured TCGID mapping of priority*/
    UINT1 au1ETSAdmTsaTable [ETS_MAX_TCGID_CONF];
                                  /* Configured traffic selection algorithm table associated to each TC */
    UINT1 au1ETSAdmRecoBW [ETS_MAX_NUM_TCGID];
                                 /*Configured Reco bandwidth of TCGID*/
    UINT1 au1ETSAdmRecoTsaTable [ETS_MAX_TCGID_CONF];
                                  /* Configured Reco traffic selection algorithm table associated to each TC */
    UINT1 u1ETSAdmNumTcSup;      /*Number of configured TC supported*/
    UINT1 u1ETSAdmNumTCGSup;     /*Number of configured TCG Supported*/
    UINT1 u1ETSAdmWilling;       /*Configured ETS Willing Status*/
    UINT1 u1ETSAdmCBS;           /*Configures ETS Credit based shaper Status*/
}tETSAdmPortInfo;

/* ETS - Remote Information strucutre */
typedef struct _tETSRemPortInfo
{
    UINT4 u4ETSRemTimeMark;                    /* Time Filter */
    INT4  i4ETSRemIndex;                       /*Remote Index */
    UINT4 u4ETSDestRemMacIndex;                /*Remote Destination MAC Index*/
    UINT1 au1ETSRemBW [ETS_MAX_NUM_TCGID];    /*Bandwidth Allocated to the
                                                *TCGID of the RemoteSystem*/
    UINT1 au1ETSRemTCGID [DCBX_MAX_PRIORITIES];/*TCGID associated to the
                                                *Priority of the Remote System*/
    UINT1 au1ETSRemRecoTCGID [DCBX_MAX_PRIORITIES];
    UINT1 au1ETSRemTsaTable [ETS_MAX_TCGID_CONF];
                                  /* traffic selection algorithm table of Remote System */
    UINT1 au1ETSRemRecoBW [ETS_MAX_NUM_TCGID];/*Reco TCGID associated to the
                                                *Priority of the Remote system*/
    UINT1 au1ETSRemRecoTsaTable [ETS_MAX_TCGID_CONF];
                                                /* Reco traffic selection algorithm table of Remote System */
    UINT1 u1ETSRemNumTcSup;   
    UINT1 u1ETSRemNumTCGSup;                   /*Number of TCG Supported in
                                                *the RemoteSystem*/
    UINT1 u1ETSRemWilling;                     /*ETS Willing status of the
                                               *RemoteSystem*/

    UINT1 u1ETSRemCBS;                         /*Credit based shaper status of the
                                                Remote System*/
    UINT1 au1ETSDestRemMacAddr[MAC_ADDR_LEN];  /*Remote Destination MAC Addr*/
    UINT1 au1Padding[2];
}tETSRemPortInfo;

/*ETS Port Table*/
typedef struct _tETSPortEntry
{
    tRBNodeEmbd RbNode;              /*Link to Traverse the Table*/                   
    UINT4 u4IfIndex;                 /*Port Number */
    tETSLocPortInfo ETSLocPortInfo;  /*Structure for Local Port Info*/
    tETSAdmPortInfo ETSAdmPortInfo;  /*Strucutre for Admin Port Info*/
    tETSRemPortInfo ETSRemPortInfo;  /*Structure for Remote Port Info*/
    tETSQosPortInfo ETSQosPortInfo;
    UINT4 u4ETSConfTxTLVCount;       /*Conf TLV Tx Count*/
    UINT4 u4ETSConfRxTLVCount;       /*Conf TLV Rx Count*/ 
    UINT4 u4ETSConfRxTLVError;       /*Conf TLV Rx Error Count*/ 
    UINT4 u4ETSRecoTxTLVCount;       /*Reco TLV Tx Count*/
    UINT4 u4ETSRecoRxTLVCount;       /*Reco TLV Rx Count*/
    UINT4 u4ETSRecoRxTLVError;       /*Reco TLV Rx Error Count*/
    UINT4 u4ETSTcSuppTxTLVCount;     /*TcSupp TLV Tx Count*/
    UINT4 u4ETSTcSuppRxTLVCount;     /*TcSupp TLV Rx Count*/
    UINT4 u4ETSTcSupRxTLVError;      /*TcSupp TLV Rx Error Count*/
    INT4  i4EtsFilterId;             /*ETS Filter Id */
    UINT1 u1ETSAdminMode;            /*ETS enabled Mode(static/dynamic)/
                                      *Disable Mode for the port */
    UINT1 u1ETSDcbxOperState;        /*ETS oper state (Init/Recommended) 
                                      *for the port */
    UINT1 u1ETSDcbxSemType;          /*DCBX State Event Machine Used*/
    UINT1 u1ETSConfigTxStatus;       /*ETS Config TLV transfer status on port*/
    UINT1 u1ETSRecoTxStatus;         /*ETS Reco TLV transfer status on port*/
    UINT1 u1TcSupportTxStatus;       /*TC supported TLV transfer 
                                       *status on port*/ 
    UINT1 u1ETSRowStatus;            /*ETS Node Creation/Deletion */
    UINT1 u1RemTlvUpdStatus;         /*Remote TLV update Status. used to check
                                      *whether Conf/Reco/TcSupp TLV updation
                                      *happened or not */
    UINT1 u1CurrentState;            /*Current State */
    UINT1 u1DcbxStatus;              /*DCBx Status */
    BOOL1 bETSSyncd;                 /* Flag denotes whether local and peer are in synch. */
    BOOL1 bETSError;                 /* Error Flag to denote local Errors */
    BOOL1 bETSPeerWilling;           /* Peer Willingness Flag */ 
    UINT1 au1Padding[3];
}tETSPortEntry;

/*CEE Related Data Structures */

typedef struct _tCEEGlobalInfo {
   tMemPoolId  CEECtrlPoolId;      /* Pool Id for CEE Port */
   tRBTree     pRbCeeCtrlTbl;      /* CEE Port Table Header */
   UINT4       u4CEETrap;          /* Enabled or Disabled Status of Trap */
   UINT4       u4CEETrapGeneratedCount; /* Generated Trap Count */
}tCEEGlobalInfo;

typedef struct _tCeeApplTlvParam{
    UINT4    u4IfIndex;                /*Port Number*/
    UINT4    u4RemLastUpdateTime;      /*Remote Last Update time*/
    INT4     i4RemIndex;               /*Remote Index for the remote entry*/
    UINT4    u4RemLocalDestMACIndex;   /*Remote Destination MAC Index for this TLV*/
    UINT1    au1CeeApplTlv[CEE_APP_PRI_MAX_TLV_LEN]; /*Feature TLV Paramter*/
    tMacAddr RemDestMacAddr;           /*Remote Destination MAC for this TLV*/
    UINT2    u2TlvLen;                 /* Length of Recieved TLV*/
    UINT1    u1TlvType;                /*Type of Rx TLV - Control/ETS/PFC/App Pri */
    UINT1    au1Reserved[3];
}tCeeTLVInfo;

/*CEE Control Entry */
typedef struct _tCEECtrlEntry
{
    tRBNodeEmbd RbNode;               /*Link to Traverse the Table */
    UINT4       u4IfIndex;            /*Port Number */
    UINT4       u4CtrlTxTLVCount;     /*Ctrl TLV Tx Count*/
    UINT4       u4CtrlRxTLVCount;     /*Ctrl TLV Rx Count*/
    UINT4       u4CtrlRxTLVErrorCount;/*Ctrl TLV Rx Error Count*/
    UINT4       u4CtrlSeqNo;          /*Sequence Number of Transmitted Pkt*/
    UINT4       u4CtrlRcvdSeqNo;      /*Sequence Number of Recieved Pkt*/
    UINT4       u4CtrlAckNo;          /*Acknowledgement Number of Transmitted Pkt*/
    UINT4       u4CtrlRcvdAckNo;      /*Acknowledgement Number of Recieved Pkt*/
    UINT4       u4FeatEnabledCount;   /*Number of Enabled Features */
    UINT4       u4CtrlSyncNo;         /*If SyncNo greater than u4CtrlSeqNo represents
                                        there are pending updates */
    UINT4       u4CtrlAckMissCount;   /*Maintains the Acknowledgement Miss count */
}tCEECtrlEntry;

/* PFC Related Data Structures */

/* PFC Profile Entry */
typedef struct _tPFCProfileEntry
{
    UINT4 u4PfcNumOfPorts;         /* Number of ports associated
                                      to this profile */
    INT4  i4PfcHwProfileId;        /* Hardware Profile Id */
    UINT1 u1IsStatusValid;         /* Status of this profile */
    UINT1 au1Padding[3];
}tPFCProfileEntry;

/* PFC Global Strucutre */
typedef struct _tPFCGlobalInfo
{
    tMemPoolId  PFCPortPoolId;     /*Pool Id for PFC Port*/
    tRBTree     pRbPFCPortTbl;     /*PFC Port Table Header*/   
    tPFCProfileEntry gaPFCProfileEntry[PFC_MAX_PROFILES];
                                   /*Array for Maximum number of profiles
                                    *to store the profile information */
    UINT4       u4PFCMinThresh;    /*Minimum Threshold for the PFC */
    UINT4       u4PFCMaxThresh;    /*Maximum Threshold for the PFC */
    UINT4       u4PFCTrap;      /*Enabled or Disabled Status of Trap*/
    UINT4       u4PFCGeneratedTrapCount; /*PFC Trap Count*/
    UINT2       u2PFCMaxNumOfProfiles; /*Number of PFC  Profiles Created */
    UINT1       u1PFCSystemCtrl;   /*Start or Shutdown status of PFC Module*/
    UINT1       u1PFCModStatus;    /*Enabled or Disabled status of PFC Module*/   
}tPFCGlobalInfo;

/* PFC - Local Information Structure */
typedef struct _tPFCLocPortInfo
{
    UINT1 u1PFCLocWilling;    /*PFC Willing status of the LocalSystem*/
    UINT1 u1PFCLocCap;        /*PFC Capability of the LocalSystem*/  
    UINT1 u1PFCLocMBC;        /*MBC Status of the LocalSystem*/
    UINT1 u1PFCLocStatus;     /*PFC enable status per priority.
                                Bit 0 - Priority 0 Enable status
                                Bit 1 - Priority 1 Enable status */
}tPFCLocPortInfo;

/* PFC - Admin Information Structure */
typedef struct _tPFCAdmPortInfo
{
    UINT1 u1PFCAdmWilling;    /*Configured PFC Willing Status*/
    UINT1 u1PFCAdmCap;        /*Configured PFC Capability*/
    UINT1 u1PFCAdmMBC;        /*Configured MBC status*/
    UINT1 u1PFCAdmStatus;     /*Configured PFC enable status*/
}tPFCAdmPortInfo;

/* PFC - Remote Information Structure */
typedef struct _tPFCRemPortInfo
{
    UINT4 u4PFCRemTimeMark;   /*Time Filter */
    INT4  i4PFCRemIndex;      /*Remote Index */
    UINT4 u4PFCDestRemMacIndex;/*Remote Destination MAC Index*/   
    UINT1 au1PFCDestRemMacAddr[MAC_ADDR_LEN];   
                              /*Remote Destination MAC Addr*/
    UINT1 u1PFCRemWilling;    /*PFC Willing status of the RemoteSystem*/
    UINT1 u1PFCRemCap;        /*PFC Capability of the RemoteSystem*/
    UINT1 u1PFCRemMBC;        /*MBC Status of the RemoteSystem*/
    UINT1 u1PFCRemStatus;     /*PFC enable status per priority*/
    UINT1 au1Padding[2];
}tPFCRemPortInfo;

/*PFC Port Table*/
typedef struct _tPFCPortEntry
{
    tRBNodeEmbd RbNode;               /*Link to Traverse the Table */                   
    UINT4 u4IfIndex;                  /*Port Number */
    tPFCLocPortInfo  PFCLocPortInfo;  /*Structure to Local Port Info*/
    tPFCAdmPortInfo  PFCAdmPortInfo;  /*Structure to Admin Port Info*/
    tPFCRemPortInfo  PFCRemPortInfo;  /*Strucutre to Remote Port Info*/
    UINT4 u4PFCTxTLVCount;            /*PFC TLV Tx Count*/
    UINT4 u4PFCRxTLVCount;            /*PFC TLV Rx Count*/
    UINT4 u4PFCRxTLVError;            /*PFC TLV Rx Error Count*/
    UINT1 u1PFCAdminMode;             /*PFC enabled Mode(static/dynamic)/
                                       *Disable Mode for the port */
    UINT1 u1PFCTxStatus;              /*PFC TLV transfer status on port*/
    UINT1 u1PFCDcbxOperState;         /*PFC oper state (Init/Recommended) 
                                       *for the port */
    UINT1 u1PFCDcbxSemType;           /*DCBX State Event Machine Used*/
    UINT1 u1PFCRowStatus;             /*PFC Node Creation/Deletion */
    UINT1 u1CurrentState;             /*Current State */
    BOOL1 bPFCSyncd;                  /* Flag denotes whether local and peer are in synch. */
    BOOL1 bPFCError;                  /* Error Flag to denote local Errors */
    BOOL1 bPFCPeerWilling;            /* Peer Willingness Flag */
    UINT1 u1DcbxStatus;               /* DCBx Status */
    UINT1 au1Padding[2];
}tPFCPortEntry;

/*Application priority related data structures */
/* Application Priority Global Structure */
typedef struct _tAppPriGlobalInfo
{
    tMemPoolId  AppPriPortPoolId;     /*Pool Id for ApplicationPriority Port*/
    tMemPoolId  AppPriMappingPoolId;  /*Pool Id for ApplicationPriority
                                           Mapping*/
    tRBTree     pRbAppPriPortTbl;     /*ApplicationPriority Port Table Header*/   
    UINT4       u4AppPriTrapStatus;   /*Enabled or Disabled Status of Trap*/
    UINT4       u4AppPriGeneratedTrapCount; /*Application Priority Trap Count*/
    UINT1       u1AppPriSystemCtrl;   /*Start or Shutdown status of 
                                        ApplicationPriority Module*/
    UINT1       u1AppPriModStatus;    /*Enabled or Disabled status of 
                                        ApplicationPriority Module*/   
    UINT1       au1Padding[2];        /*Padding */
}tAppPriGlobalInfo;

/* Application Priority Mapping Information */
typedef struct _tAppPriMappingInfo
{
    tTMO_SLL_NODE   AppPriMappingEntry;/* SLL Node */
    INT4            i4AppPriProtocol;  /* Application Priority Protocol */
    UINT1           u1AppPriPriority;  /* Application Priority assigned for 
                                           the protocol */
    UINT1           u1RowStatus;       /*Row Status for Application Priority 
                                         Mapping entry*/
    UINT1           au1Padding[2]; 

}tAppPriMappingEntry;


/* Application Priority - Local Information Structure */
typedef struct _tAppPriLocPortInfo
{
    tTMO_SLL   *pAppPriLocMappingTbl[APP_PRI_MAX_SELECTOR];
               /* Operational Application to Priority Mapping Table.
                  This will either point to the  AppPriAdmMappingTbl
                  or AppPriRemMappingTbl.*/
    UINT1      u1AppPriLocWilling; /*ApplicationPriority Willing status of
                                        the LocalSystem*/
    UINT1      au1Padding[3];
}tAppPriLocPortInfo;

/* ApplicationPriority - Admin Information Structure */
typedef struct _tAppPriAdmPortInfo
{
    tTMO_SLL  AppPriAdmMappingTbl[APP_PRI_MAX_SELECTOR];
              /* User configured Application to Priority Mapping Table*/
    UINT1     u1AppPriAdmWilling; /* Admin Configured Application Priority 
                                         Willing Status*/
    UINT1 au1Padding[3];
}tAppPriAdmPortInfo;

/* Application Priority - Remote Information Structure */
typedef struct _tAppPriRemPortInfo
{
    tTMO_SLL     AppPriRemMappingTbl[APP_PRI_MAX_SELECTOR]; 
                 /* Remote Application to Priority Mapping Table*/
    UINT4        u4AppPriRemTimeMark;     /*Time Filter */
    INT4         i4AppPriRemIndex;        /*Remote Index */
    UINT4        u4AppPriDestRemMacIndex; /*Remote Destination MAC Index*/ 
    UINT1        au1AppPriDestRemMacAddr[MAC_ADDR_LEN]; /*Remote Destination
                                                          MAC Address*/
    UINT1        u1AppPriRemWilling;     /*Application Priority Willing status
                                           of the RemoteSystem*/
    UINT1 au1Padding[1];
}tAppPriRemPortInfo;

/*Application Priority Port Table. This structure will be populated when
 * Application Prioirty feature is enabled on a port*/
typedef struct _tAppPriPortEntry
{
    tRBNodeEmbd         RbNode;               /*Link to Traverse the Table */                   
    tAppPriLocPortInfo  AppPriLocPortInfo;    /*Structure to Local Port Info*/
    tAppPriAdmPortInfo  AppPriAdmPortInfo;    /*Structure to Admin Port Info*/
    tAppPriRemPortInfo  AppPriRemPortInfo;    /*Strucutre to Remote Port Info*/
    
    UINT4               u4IfIndex;            /*Port Number */
    UINT4               u4AppPriTLVTxCount;   /*Application Priority TLV Tx Count*/
    UINT4               u4AppPriTLVRxCount;   /*Application Priority TLV Rx Count*/
    UINT4               u4AppPriTLVRxError;   /*Application Priority TLV Rx Error Count*/
    UINT4               u4AppPriAppProtocols; /*No.of Application Priority 
                                               entries in last received TLV*/
    UINT1               u1AppPriAdminMode;    /* Application Priority enabled Mode
                                                (static/dynamic)/Disable Mode for
                                                 the port */
    UINT1               u1AppPriTxStatus;      /*Application Priority TLV 
                                                 transfer status on port*/
    UINT1               u1AppPriDcbxOperState; /*Application Priority oper state
                                                 (Init/Recommended) for the port */
    UINT1               u1AppPriDcbxSemType;   /*DCBX State Event Machine Used*/
    UINT1               u1AppPriRowStatus;     /*Application Priority Node
                                                 Creation/Deletion */
    UINT1               u1CurrentState;        /*Current State */
    BOOL1               bAppPriSyncd;          /* Flag denotes whether
                                                * local and peer are in synch. */
    BOOL1               bAppPriError;          /* Error Flag to denote local Errors */
    BOOL1               bAppPriPeerWilling;    /* Peer Willingness Flag */
    UINT1               u1DcbxStatus;          /*DCBx Status */
    UINT1               au1Padding[2];
}tAppPriPortEntry;
#endif /* __DCBX_TDFS_H__ */




