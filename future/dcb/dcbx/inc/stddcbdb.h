/********************************************************************
* Copyright (C) 2010 Aricent Inc . All Rights Reserved
*
* $Id: stddcbdb.h,v 1.4 2016/07/09 09:41:20 siva Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/
#ifndef _STDDCBDB_H
#define _STDDCBDB_H

UINT1 LldpXdot1dcbxConfigETSConfigurationTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 LldpXdot1dcbxConfigETSRecommendationTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 LldpXdot1dcbxConfigPFCTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 LldpXdot1dcbxConfigApplicationPriorityTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 LldpXdot1dcbxLocETSBasicConfigurationTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};
UINT1 LldpXdot1dcbxLocETSConPriorityAssignmentTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 LldpXdot1dcbxLocETSConTrafficClassBandwidthTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 LldpXdot1dcbxLocETSConTrafficSelectionAlgorithmTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 LldpXdot1dcbxLocETSRecoTrafficClassBandwidthTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 LldpXdot1dcbxLocETSRecoTrafficSelectionAlgorithmTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 LldpXdot1dcbxLocPFCBasicTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};
UINT1 LldpXdot1dcbxLocPFCEnableTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 LldpXdot1dcbxLocApplicationPriorityAppTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 LldpXdot1dcbxRemETSBasicConfigurationTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_INTEGER32};
UINT1 LldpXdot1dcbxRemETSConPriorityAssignmentTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 LldpXdot1dcbxRemETSConTrafficClassBandwidthTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 LldpXdot1dcbxRemETSConTrafficSelectionAlgorithmTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 LldpXdot1dcbxRemETSRecoTrafficClassBandwidthTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 LldpXdot1dcbxRemETSRecoTrafficSelectionAlgorithmTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 LldpXdot1dcbxRemPFCBasicTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_INTEGER32};
UINT1 LldpXdot1dcbxRemPFCEnableTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 LldpXdot1dcbxRemApplicationPriorityAppTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 LldpXdot1dcbxAdminETSBasicConfigurationTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};
UINT1 LldpXdot1dcbxAdminETSConPriorityAssignmentTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 LldpXdot1dcbxAdminETSConTrafficClassBandwidthTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 LldpXdot1dcbxAdminETSConTrafficSelectionAlgorithmTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 LldpXdot1dcbxAdminETSRecoTrafficClassBandwidthTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 LldpXdot1dcbxAdminETSRecoTrafficSelectionAlgorithmTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 LldpXdot1dcbxAdminPFCBasicTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};
UINT1 LldpXdot1dcbxAdminPFCEnableTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 LldpXdot1dcbxAdminApplicationPriorityAppTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_UNSIGNED32};

UINT4 stddcb [] ={1,0,8802,1,1,2,1,5,32962,5};
tSNMP_OID_TYPE stddcbOID = {10, stddcb};


UINT4 LldpXdot1dcbxConfigETSConfigurationTxEnable [ ] ={1,0,8802,1,1,2,1,5,32962,5,1,1,1,1,1};
UINT4 LldpXdot1dcbxConfigETSRecommendationTxEnable [ ] ={1,0,8802,1,1,2,1,5,32962,5,1,1,2,1,1};
UINT4 LldpXdot1dcbxConfigPFCTxEnable [ ] ={1,0,8802,1,1,2,1,5,32962,5,1,1,3,1,1};
UINT4 LldpXdot1dcbxConfigApplicationPriorityTxEnable [ ] ={1,0,8802,1,1,2,1,5,32962,5,1,1,4,1,1};
UINT4 LldpXdot1dcbxLocETSConCreditBasedShaperSupport [ ] ={1,0,8802,1,1,2,1,5,32962,5,1,2,1,1,1,1};
UINT4 LldpXdot1dcbxLocETSConTrafficClassesSupported [ ] ={1,0,8802,1,1,2,1,5,32962,5,1,2,1,1,1,2};
UINT4 LldpXdot1dcbxLocETSConWilling [ ] ={1,0,8802,1,1,2,1,5,32962,5,1,2,1,1,1,3};
UINT4 LldpXdot1dcbxLocETSConPriority [ ] ={1,0,8802,1,1,2,1,5,32962,5,1,2,1,2,1,1};
UINT4 LldpXdot1dcbxLocETSConPriTrafficClass [ ] ={1,0,8802,1,1,2,1,5,32962,5,1,2,1,2,1,2};
UINT4 LldpXdot1dcbxLocETSConTrafficClass [ ] ={1,0,8802,1,1,2,1,5,32962,5,1,2,1,3,1,1};
UINT4 LldpXdot1dcbxLocETSConTrafficClassBandwidth [ ] ={1,0,8802,1,1,2,1,5,32962,5,1,2,1,3,1,2};
UINT4 LldpXdot1dcbxLocETSConTSATrafficClass [ ] ={1,0,8802,1,1,2,1,5,32962,5,1,2,1,4,1,1};
UINT4 LldpXdot1dcbxLocETSConTrafficSelectionAlgorithm [ ] ={1,0,8802,1,1,2,1,5,32962,5,1,2,1,4,1,2};
UINT4 LldpXdot1dcbxLocETSRecoTrafficClass [ ] ={1,0,8802,1,1,2,1,5,32962,5,1,2,2,1,1,1};
UINT4 LldpXdot1dcbxLocETSRecoTrafficClassBandwidth [ ] ={1,0,8802,1,1,2,1,5,32962,5,1,2,2,1,1,2};
UINT4 LldpXdot1dcbxLocETSRecoTSATrafficClass [ ] ={1,0,8802,1,1,2,1,5,32962,5,1,2,2,2,1,1};
UINT4 LldpXdot1dcbxLocETSRecoTrafficSelectionAlgorithm [ ] ={1,0,8802,1,1,2,1,5,32962,5,1,2,2,2,1,2};
UINT4 LldpXdot1dcbxLocPFCWilling [ ] ={1,0,8802,1,1,2,1,5,32962,5,1,2,3,1,1,1};
UINT4 LldpXdot1dcbxLocPFCMBC [ ] ={1,0,8802,1,1,2,1,5,32962,5,1,2,3,1,1,2};
UINT4 LldpXdot1dcbxLocPFCCap [ ] ={1,0,8802,1,1,2,1,5,32962,5,1,2,3,1,1,3};
UINT4 LldpXdot1dcbxLocPFCEnablePriority [ ] ={1,0,8802,1,1,2,1,5,32962,5,1,2,3,2,1,1};
UINT4 LldpXdot1dcbxLocPFCEnableEnabled [ ] ={1,0,8802,1,1,2,1,5,32962,5,1,2,3,2,1,2};
UINT4 LldpXdot1dcbxLocApplicationPriorityAESelector [ ] ={1,0,8802,1,1,2,1,5,32962,5,1,2,4,1,1};
UINT4 LldpXdot1dcbxLocApplicationPriorityAEProtocol [ ] ={1,0,8802,1,1,2,1,5,32962,5,1,2,4,1,2};
UINT4 LldpXdot1dcbxLocApplicationPriorityAEPriority [ ] ={1,0,8802,1,1,2,1,5,32962,5,1,2,4,1,3};
UINT4 LldpXdot1dcbxRemETSConCreditBasedShaperSupport [ ] ={1,0,8802,1,1,2,1,5,32962,5,1,3,1,1,1,1};
UINT4 LldpXdot1dcbxRemETSConTrafficClassesSupported [ ] ={1,0,8802,1,1,2,1,5,32962,5,1,3,1,1,1,2};
UINT4 LldpXdot1dcbxRemETSConWilling [ ] ={1,0,8802,1,1,2,1,5,32962,5,1,3,1,1,1,3};
UINT4 LldpXdot1dcbxRemETSConPriority [ ] ={1,0,8802,1,1,2,1,5,32962,5,1,3,1,2,1,1};
UINT4 LldpXdot1dcbxRemETSConPriTrafficClass [ ] ={1,0,8802,1,1,2,1,5,32962,5,1,3,1,2,1,2};
UINT4 LldpXdot1dcbxRemETSConTrafficClass [ ] ={1,0,8802,1,1,2,1,5,32962,5,1,3,1,3,1,1};
UINT4 LldpXdot1dcbxRemETSConTrafficClassBandwidth [ ] ={1,0,8802,1,1,2,1,5,32962,5,1,3,1,3,1,2};
UINT4 LldpXdot1dcbxRemETSConTSATrafficClass [ ] ={1,0,8802,1,1,2,1,5,32962,5,1,3,1,4,1,1};
UINT4 LldpXdot1dcbxRemETSConTrafficSelectionAlgorithm [ ] ={1,0,8802,1,1,2,1,5,32962,5,1,3,1,4,1,2};
UINT4 LldpXdot1dcbxRemETSRecoTrafficClass [ ] ={1,0,8802,1,1,2,1,5,32962,5,1,3,2,1,1,1};
UINT4 LldpXdot1dcbxRemETSRecoTrafficClassBandwidth [ ] ={1,0,8802,1,1,2,1,5,32962,5,1,3,2,1,1,2};
UINT4 LldpXdot1dcbxRemETSRecoTSATrafficClass [ ] ={1,0,8802,1,1,2,1,5,32962,5,1,3,2,2,1,1};
UINT4 LldpXdot1dcbxRemETSRecoTrafficSelectionAlgorithm [ ] ={1,0,8802,1,1,2,1,5,32962,5,1,3,2,2,1,2};
UINT4 LldpXdot1dcbxRemPFCWilling [ ] ={1,0,8802,1,1,2,1,5,32962,5,1,3,3,1,1,1};
UINT4 LldpXdot1dcbxRemPFCMBC [ ] ={1,0,8802,1,1,2,1,5,32962,5,1,3,3,1,1,2};
UINT4 LldpXdot1dcbxRemPFCCap [ ] ={1,0,8802,1,1,2,1,5,32962,5,1,3,3,1,1,3};
UINT4 LldpXdot1dcbxRemPFCEnablePriority [ ] ={1,0,8802,1,1,2,1,5,32962,5,1,3,3,2,1,1};
UINT4 LldpXdot1dcbxRemPFCEnableEnabled [ ] ={1,0,8802,1,1,2,1,5,32962,5,1,3,3,2,1,2};
UINT4 LldpXdot1dcbxRemApplicationPriorityAESelector [ ] ={1,0,8802,1,1,2,1,5,32962,5,1,3,4,1,1};
UINT4 LldpXdot1dcbxRemApplicationPriorityAEProtocol [ ] ={1,0,8802,1,1,2,1,5,32962,5,1,3,4,1,2};
UINT4 LldpXdot1dcbxRemApplicationPriorityAEPriority [ ] ={1,0,8802,1,1,2,1,5,32962,5,1,3,4,1,3};
UINT4 LldpXdot1dcbxAdminETSConCreditBasedShaperSupport [ ] ={1,0,8802,1,1,2,1,5,32962,5,1,4,1,1,1,1};
UINT4 LldpXdot1dcbxAdminETSConTrafficClassesSupported [ ] ={1,0,8802,1,1,2,1,5,32962,5,1,4,1,1,1,2};
UINT4 LldpXdot1dcbxAdminETSConWilling [ ] ={1,0,8802,1,1,2,1,5,32962,5,1,4,1,1,1,3};
UINT4 LldpXdot1dcbxAdminETSConPriority [ ] ={1,0,8802,1,1,2,1,5,32962,5,1,4,1,2,1,1};
UINT4 LldpXdot1dcbxAdminETSConPriTrafficClass [ ] ={1,0,8802,1,1,2,1,5,32962,5,1,4,1,2,1,2};
UINT4 LldpXdot1dcbxAdminETSConTrafficClass [ ] ={1,0,8802,1,1,2,1,5,32962,5,1,4,1,3,1,1};
UINT4 LldpXdot1dcbxAdminETSConTrafficClassBandwidth [ ] ={1,0,8802,1,1,2,1,5,32962,5,1,4,1,3,1,2};
UINT4 LldpXdot1dcbxAdminETSConTSATrafficClass [ ] ={1,0,8802,1,1,2,1,5,32962,5,1,4,1,4,1,1};
UINT4 LldpXdot1dcbxAdminETSConTrafficSelectionAlgorithm [ ] ={1,0,8802,1,1,2,1,5,32962,5,1,4,1,4,1,2};
UINT4 LldpXdot1dcbxAdminETSRecoTrafficClass [ ] ={1,0,8802,1,1,2,1,5,32962,5,1,4,2,1,1,1};
UINT4 LldpXdot1dcbxAdminETSRecoTrafficClassBandwidth [ ] ={1,0,8802,1,1,2,1,5,32962,5,1,4,2,1,1,2};
UINT4 LldpXdot1dcbxAdminETSRecoTSATrafficClass [ ] ={1,0,8802,1,1,2,1,5,32962,5,1,4,2,2,1,1};
UINT4 LldpXdot1dcbxAdminETSRecoTrafficSelectionAlgorithm [ ] ={1,0,8802,1,1,2,1,5,32962,5,1,4,2,2,1,2};
UINT4 LldpXdot1dcbxAdminPFCWilling [ ] ={1,0,8802,1,1,2,1,5,32962,5,1,4,3,1,1,1};
UINT4 LldpXdot1dcbxAdminPFCMBC [ ] ={1,0,8802,1,1,2,1,5,32962,5,1,4,3,1,1,2};
UINT4 LldpXdot1dcbxAdminPFCCap [ ] ={1,0,8802,1,1,2,1,5,32962,5,1,4,3,1,1,3};
UINT4 LldpXdot1dcbxAdminPFCEnablePriority [ ] ={1,0,8802,1,1,2,1,5,32962,5,1,4,3,2,1,1};
UINT4 LldpXdot1dcbxAdminPFCEnableEnabled [ ] ={1,0,8802,1,1,2,1,5,32962,5,1,4,3,2,1,2};
UINT4 LldpXdot1dcbxAdminApplicationPriorityAESelector [ ] ={1,0,8802,1,1,2,1,5,32962,5,1,4,4,1,1};
UINT4 LldpXdot1dcbxAdminApplicationPriorityAEProtocol [ ] ={1,0,8802,1,1,2,1,5,32962,5,1,4,4,1,2};
UINT4 LldpXdot1dcbxAdminApplicationPriorityAEPriority [ ] ={1,0,8802,1,1,2,1,5,32962,5,1,4,4,1,3};




tMbDbEntry stddcbMibEntry[]= {

{{15,LldpXdot1dcbxConfigETSConfigurationTxEnable}, GetNextIndexLldpXdot1dcbxConfigETSConfigurationTable, LldpXdot1dcbxConfigETSConfigurationTxEnableGet, LldpXdot1dcbxConfigETSConfigurationTxEnableSet, LldpXdot1dcbxConfigETSConfigurationTxEnableTest, LldpXdot1dcbxConfigETSConfigurationTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, LldpXdot1dcbxConfigETSConfigurationTableINDEX, 2, 0, 0, "2"},

{{15,LldpXdot1dcbxConfigETSRecommendationTxEnable}, GetNextIndexLldpXdot1dcbxConfigETSRecommendationTable, LldpXdot1dcbxConfigETSRecommendationTxEnableGet, LldpXdot1dcbxConfigETSRecommendationTxEnableSet, LldpXdot1dcbxConfigETSRecommendationTxEnableTest, LldpXdot1dcbxConfigETSRecommendationTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, LldpXdot1dcbxConfigETSRecommendationTableINDEX, 2, 0, 0, "2"},

{{15,LldpXdot1dcbxConfigPFCTxEnable}, GetNextIndexLldpXdot1dcbxConfigPFCTable, LldpXdot1dcbxConfigPFCTxEnableGet, LldpXdot1dcbxConfigPFCTxEnableSet, LldpXdot1dcbxConfigPFCTxEnableTest, LldpXdot1dcbxConfigPFCTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, LldpXdot1dcbxConfigPFCTableINDEX, 2, 0, 0, "2"},

{{15,LldpXdot1dcbxConfigApplicationPriorityTxEnable}, GetNextIndexLldpXdot1dcbxConfigApplicationPriorityTable, LldpXdot1dcbxConfigApplicationPriorityTxEnableGet, LldpXdot1dcbxConfigApplicationPriorityTxEnableSet, LldpXdot1dcbxConfigApplicationPriorityTxEnableTest, LldpXdot1dcbxConfigApplicationPriorityTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, LldpXdot1dcbxConfigApplicationPriorityTableINDEX, 2, 0, 0, "2"},

{{16,LldpXdot1dcbxLocETSConCreditBasedShaperSupport}, GetNextIndexLldpXdot1dcbxLocETSBasicConfigurationTable, LldpXdot1dcbxLocETSConCreditBasedShaperSupportGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, LldpXdot1dcbxLocETSBasicConfigurationTableINDEX, 1, 0, 0, NULL},

{{16,LldpXdot1dcbxLocETSConTrafficClassesSupported}, GetNextIndexLldpXdot1dcbxLocETSBasicConfigurationTable, LldpXdot1dcbxLocETSConTrafficClassesSupportedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, LldpXdot1dcbxLocETSBasicConfigurationTableINDEX, 1, 0, 0, NULL},

{{16,LldpXdot1dcbxLocETSConWilling}, GetNextIndexLldpXdot1dcbxLocETSBasicConfigurationTable, LldpXdot1dcbxLocETSConWillingGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, LldpXdot1dcbxLocETSBasicConfigurationTableINDEX, 1, 0, 0, NULL},

{{16,LldpXdot1dcbxLocETSConPriority}, GetNextIndexLldpXdot1dcbxLocETSConPriorityAssignmentTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, LldpXdot1dcbxLocETSConPriorityAssignmentTableINDEX, 2, 0, 0, NULL},

{{16,LldpXdot1dcbxLocETSConPriTrafficClass}, GetNextIndexLldpXdot1dcbxLocETSConPriorityAssignmentTable, LldpXdot1dcbxLocETSConPriTrafficClassGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, LldpXdot1dcbxLocETSConPriorityAssignmentTableINDEX, 2, 0, 0, NULL},

{{16,LldpXdot1dcbxLocETSConTrafficClass}, GetNextIndexLldpXdot1dcbxLocETSConTrafficClassBandwidthTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, LldpXdot1dcbxLocETSConTrafficClassBandwidthTableINDEX, 2, 0, 0, NULL},

{{16,LldpXdot1dcbxLocETSConTrafficClassBandwidth}, GetNextIndexLldpXdot1dcbxLocETSConTrafficClassBandwidthTable, LldpXdot1dcbxLocETSConTrafficClassBandwidthGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, LldpXdot1dcbxLocETSConTrafficClassBandwidthTableINDEX, 2, 0, 0, NULL},

{{16,LldpXdot1dcbxLocETSConTSATrafficClass}, GetNextIndexLldpXdot1dcbxLocETSConTrafficSelectionAlgorithmTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, LldpXdot1dcbxLocETSConTrafficSelectionAlgorithmTableINDEX, 2, 0, 0, NULL},

{{16,LldpXdot1dcbxLocETSConTrafficSelectionAlgorithm}, GetNextIndexLldpXdot1dcbxLocETSConTrafficSelectionAlgorithmTable, LldpXdot1dcbxLocETSConTrafficSelectionAlgorithmGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, LldpXdot1dcbxLocETSConTrafficSelectionAlgorithmTableINDEX, 2, 0, 0, NULL},

{{16,LldpXdot1dcbxLocETSRecoTrafficClass}, GetNextIndexLldpXdot1dcbxLocETSRecoTrafficClassBandwidthTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, LldpXdot1dcbxLocETSRecoTrafficClassBandwidthTableINDEX, 2, 0, 0, NULL},

{{16,LldpXdot1dcbxLocETSRecoTrafficClassBandwidth}, GetNextIndexLldpXdot1dcbxLocETSRecoTrafficClassBandwidthTable, LldpXdot1dcbxLocETSRecoTrafficClassBandwidthGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, LldpXdot1dcbxLocETSRecoTrafficClassBandwidthTableINDEX, 2, 0, 0, NULL},

{{16,LldpXdot1dcbxLocETSRecoTSATrafficClass}, GetNextIndexLldpXdot1dcbxLocETSRecoTrafficSelectionAlgorithmTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, LldpXdot1dcbxLocETSRecoTrafficSelectionAlgorithmTableINDEX, 2, 0, 0, NULL},

{{16,LldpXdot1dcbxLocETSRecoTrafficSelectionAlgorithm}, GetNextIndexLldpXdot1dcbxLocETSRecoTrafficSelectionAlgorithmTable, LldpXdot1dcbxLocETSRecoTrafficSelectionAlgorithmGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, LldpXdot1dcbxLocETSRecoTrafficSelectionAlgorithmTableINDEX, 2, 0, 0, NULL},

{{16,LldpXdot1dcbxLocPFCWilling}, GetNextIndexLldpXdot1dcbxLocPFCBasicTable, LldpXdot1dcbxLocPFCWillingGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, LldpXdot1dcbxLocPFCBasicTableINDEX, 1, 0, 0, NULL},

{{16,LldpXdot1dcbxLocPFCMBC}, GetNextIndexLldpXdot1dcbxLocPFCBasicTable, LldpXdot1dcbxLocPFCMBCGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, LldpXdot1dcbxLocPFCBasicTableINDEX, 1, 0, 0, NULL},

{{16,LldpXdot1dcbxLocPFCCap}, GetNextIndexLldpXdot1dcbxLocPFCBasicTable, LldpXdot1dcbxLocPFCCapGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, LldpXdot1dcbxLocPFCBasicTableINDEX, 1, 0, 0, NULL},

{{16,LldpXdot1dcbxLocPFCEnablePriority}, GetNextIndexLldpXdot1dcbxLocPFCEnableTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, LldpXdot1dcbxLocPFCEnableTableINDEX, 2, 0, 0, NULL},

{{16,LldpXdot1dcbxLocPFCEnableEnabled}, GetNextIndexLldpXdot1dcbxLocPFCEnableTable, LldpXdot1dcbxLocPFCEnableEnabledGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, LldpXdot1dcbxLocPFCEnableTableINDEX, 2, 0, 0, NULL},

{{15,LldpXdot1dcbxLocApplicationPriorityAESelector}, GetNextIndexLldpXdot1dcbxLocApplicationPriorityAppTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, LldpXdot1dcbxLocApplicationPriorityAppTableINDEX, 3, 0, 0, NULL},

{{15,LldpXdot1dcbxLocApplicationPriorityAEProtocol}, GetNextIndexLldpXdot1dcbxLocApplicationPriorityAppTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, LldpXdot1dcbxLocApplicationPriorityAppTableINDEX, 3, 0, 0, NULL},

{{15,LldpXdot1dcbxLocApplicationPriorityAEPriority}, GetNextIndexLldpXdot1dcbxLocApplicationPriorityAppTable, LldpXdot1dcbxLocApplicationPriorityAEPriorityGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, LldpXdot1dcbxLocApplicationPriorityAppTableINDEX, 3, 0, 0, NULL},

{{16,LldpXdot1dcbxRemETSConCreditBasedShaperSupport}, GetNextIndexLldpXdot1dcbxRemETSBasicConfigurationTable, LldpXdot1dcbxRemETSConCreditBasedShaperSupportGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, LldpXdot1dcbxRemETSBasicConfigurationTableINDEX, 4, 0, 0, NULL},

{{16,LldpXdot1dcbxRemETSConTrafficClassesSupported}, GetNextIndexLldpXdot1dcbxRemETSBasicConfigurationTable, LldpXdot1dcbxRemETSConTrafficClassesSupportedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, LldpXdot1dcbxRemETSBasicConfigurationTableINDEX, 4, 0, 0, NULL},

{{16,LldpXdot1dcbxRemETSConWilling}, GetNextIndexLldpXdot1dcbxRemETSBasicConfigurationTable, LldpXdot1dcbxRemETSConWillingGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, LldpXdot1dcbxRemETSBasicConfigurationTableINDEX, 4, 0, 0, NULL},

{{16,LldpXdot1dcbxRemETSConPriority}, GetNextIndexLldpXdot1dcbxRemETSConPriorityAssignmentTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, LldpXdot1dcbxRemETSConPriorityAssignmentTableINDEX, 5, 0, 0, NULL},

{{16,LldpXdot1dcbxRemETSConPriTrafficClass}, GetNextIndexLldpXdot1dcbxRemETSConPriorityAssignmentTable, LldpXdot1dcbxRemETSConPriTrafficClassGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, LldpXdot1dcbxRemETSConPriorityAssignmentTableINDEX, 5, 0, 0, NULL},

{{16,LldpXdot1dcbxRemETSConTrafficClass}, GetNextIndexLldpXdot1dcbxRemETSConTrafficClassBandwidthTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, LldpXdot1dcbxRemETSConTrafficClassBandwidthTableINDEX, 5, 0, 0, NULL},

{{16,LldpXdot1dcbxRemETSConTrafficClassBandwidth}, GetNextIndexLldpXdot1dcbxRemETSConTrafficClassBandwidthTable, LldpXdot1dcbxRemETSConTrafficClassBandwidthGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, LldpXdot1dcbxRemETSConTrafficClassBandwidthTableINDEX, 5, 0, 0, NULL},

{{16,LldpXdot1dcbxRemETSConTSATrafficClass}, GetNextIndexLldpXdot1dcbxRemETSConTrafficSelectionAlgorithmTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, LldpXdot1dcbxRemETSConTrafficSelectionAlgorithmTableINDEX, 5, 0, 0, NULL},

{{16,LldpXdot1dcbxRemETSConTrafficSelectionAlgorithm}, GetNextIndexLldpXdot1dcbxRemETSConTrafficSelectionAlgorithmTable, LldpXdot1dcbxRemETSConTrafficSelectionAlgorithmGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, LldpXdot1dcbxRemETSConTrafficSelectionAlgorithmTableINDEX, 5, 0, 0, NULL},

{{16,LldpXdot1dcbxRemETSRecoTrafficClass}, GetNextIndexLldpXdot1dcbxRemETSRecoTrafficClassBandwidthTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, LldpXdot1dcbxRemETSRecoTrafficClassBandwidthTableINDEX, 5, 0, 0, NULL},

{{16,LldpXdot1dcbxRemETSRecoTrafficClassBandwidth}, GetNextIndexLldpXdot1dcbxRemETSRecoTrafficClassBandwidthTable, LldpXdot1dcbxRemETSRecoTrafficClassBandwidthGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, LldpXdot1dcbxRemETSRecoTrafficClassBandwidthTableINDEX, 5, 0, 0, NULL},

{{16,LldpXdot1dcbxRemETSRecoTSATrafficClass}, GetNextIndexLldpXdot1dcbxRemETSRecoTrafficSelectionAlgorithmTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, LldpXdot1dcbxRemETSRecoTrafficSelectionAlgorithmTableINDEX, 5, 0, 0, NULL},

{{16,LldpXdot1dcbxRemETSRecoTrafficSelectionAlgorithm}, GetNextIndexLldpXdot1dcbxRemETSRecoTrafficSelectionAlgorithmTable, LldpXdot1dcbxRemETSRecoTrafficSelectionAlgorithmGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, LldpXdot1dcbxRemETSRecoTrafficSelectionAlgorithmTableINDEX, 5, 0, 0, NULL},

{{16,LldpXdot1dcbxRemPFCWilling}, GetNextIndexLldpXdot1dcbxRemPFCBasicTable, LldpXdot1dcbxRemPFCWillingGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, LldpXdot1dcbxRemPFCBasicTableINDEX, 4, 0, 0, NULL},

{{16,LldpXdot1dcbxRemPFCMBC}, GetNextIndexLldpXdot1dcbxRemPFCBasicTable, LldpXdot1dcbxRemPFCMBCGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, LldpXdot1dcbxRemPFCBasicTableINDEX, 4, 0, 0, NULL},

{{16,LldpXdot1dcbxRemPFCCap}, GetNextIndexLldpXdot1dcbxRemPFCBasicTable, LldpXdot1dcbxRemPFCCapGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, LldpXdot1dcbxRemPFCBasicTableINDEX, 4, 0, 0, NULL},

{{16,LldpXdot1dcbxRemPFCEnablePriority}, GetNextIndexLldpXdot1dcbxRemPFCEnableTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, LldpXdot1dcbxRemPFCEnableTableINDEX, 5, 0, 0, NULL},

{{16,LldpXdot1dcbxRemPFCEnableEnabled}, GetNextIndexLldpXdot1dcbxRemPFCEnableTable, LldpXdot1dcbxRemPFCEnableEnabledGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, LldpXdot1dcbxRemPFCEnableTableINDEX, 5, 0, 0, NULL},

{{15,LldpXdot1dcbxRemApplicationPriorityAESelector}, GetNextIndexLldpXdot1dcbxRemApplicationPriorityAppTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, LldpXdot1dcbxRemApplicationPriorityAppTableINDEX, 6, 0, 0, NULL},

{{15,LldpXdot1dcbxRemApplicationPriorityAEProtocol}, GetNextIndexLldpXdot1dcbxRemApplicationPriorityAppTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, LldpXdot1dcbxRemApplicationPriorityAppTableINDEX, 6, 0, 0, NULL},

{{15,LldpXdot1dcbxRemApplicationPriorityAEPriority}, GetNextIndexLldpXdot1dcbxRemApplicationPriorityAppTable, LldpXdot1dcbxRemApplicationPriorityAEPriorityGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, LldpXdot1dcbxRemApplicationPriorityAppTableINDEX, 6, 0, 0, NULL},

{{16,LldpXdot1dcbxAdminETSConCreditBasedShaperSupport}, GetNextIndexLldpXdot1dcbxAdminETSBasicConfigurationTable, LldpXdot1dcbxAdminETSConCreditBasedShaperSupportGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, LldpXdot1dcbxAdminETSBasicConfigurationTableINDEX, 1, 0, 0, NULL},

{{16,LldpXdot1dcbxAdminETSConTrafficClassesSupported}, GetNextIndexLldpXdot1dcbxAdminETSBasicConfigurationTable, LldpXdot1dcbxAdminETSConTrafficClassesSupportedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, LldpXdot1dcbxAdminETSBasicConfigurationTableINDEX, 1, 0, 0, NULL},

{{16,LldpXdot1dcbxAdminETSConWilling}, GetNextIndexLldpXdot1dcbxAdminETSBasicConfigurationTable, LldpXdot1dcbxAdminETSConWillingGet, LldpXdot1dcbxAdminETSConWillingSet, LldpXdot1dcbxAdminETSConWillingTest, LldpXdot1dcbxAdminETSBasicConfigurationTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, LldpXdot1dcbxAdminETSBasicConfigurationTableINDEX, 1, 0, 0, "2"},

{{16,LldpXdot1dcbxAdminETSConPriority}, GetNextIndexLldpXdot1dcbxAdminETSConPriorityAssignmentTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, LldpXdot1dcbxAdminETSConPriorityAssignmentTableINDEX, 2, 0, 0, NULL},

{{16,LldpXdot1dcbxAdminETSConPriTrafficClass}, GetNextIndexLldpXdot1dcbxAdminETSConPriorityAssignmentTable, LldpXdot1dcbxAdminETSConPriTrafficClassGet, LldpXdot1dcbxAdminETSConPriTrafficClassSet, LldpXdot1dcbxAdminETSConPriTrafficClassTest, LldpXdot1dcbxAdminETSConPriorityAssignmentTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, LldpXdot1dcbxAdminETSConPriorityAssignmentTableINDEX, 2, 0, 0, "0"},

{{16,LldpXdot1dcbxAdminETSConTrafficClass}, GetNextIndexLldpXdot1dcbxAdminETSConTrafficClassBandwidthTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, LldpXdot1dcbxAdminETSConTrafficClassBandwidthTableINDEX, 2, 0, 0, NULL},

{{16,LldpXdot1dcbxAdminETSConTrafficClassBandwidth}, GetNextIndexLldpXdot1dcbxAdminETSConTrafficClassBandwidthTable, LldpXdot1dcbxAdminETSConTrafficClassBandwidthGet, LldpXdot1dcbxAdminETSConTrafficClassBandwidthSet, LldpXdot1dcbxAdminETSConTrafficClassBandwidthTest, LldpXdot1dcbxAdminETSConTrafficClassBandwidthTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, LldpXdot1dcbxAdminETSConTrafficClassBandwidthTableINDEX, 2, 0, 0, NULL},

{{16,LldpXdot1dcbxAdminETSConTSATrafficClass}, GetNextIndexLldpXdot1dcbxAdminETSConTrafficSelectionAlgorithmTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, LldpXdot1dcbxAdminETSConTrafficSelectionAlgorithmTableINDEX, 2, 0, 0, NULL},

{{16,LldpXdot1dcbxAdminETSConTrafficSelectionAlgorithm}, GetNextIndexLldpXdot1dcbxAdminETSConTrafficSelectionAlgorithmTable, LldpXdot1dcbxAdminETSConTrafficSelectionAlgorithmGet, LldpXdot1dcbxAdminETSConTrafficSelectionAlgorithmSet, LldpXdot1dcbxAdminETSConTrafficSelectionAlgorithmTest, LldpXdot1dcbxAdminETSConTrafficSelectionAlgorithmTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, LldpXdot1dcbxAdminETSConTrafficSelectionAlgorithmTableINDEX, 2, 0, 0, NULL},

{{16,LldpXdot1dcbxAdminETSRecoTrafficClass}, GetNextIndexLldpXdot1dcbxAdminETSRecoTrafficClassBandwidthTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, LldpXdot1dcbxAdminETSRecoTrafficClassBandwidthTableINDEX, 2, 0, 0, NULL},

{{16,LldpXdot1dcbxAdminETSRecoTrafficClassBandwidth}, GetNextIndexLldpXdot1dcbxAdminETSRecoTrafficClassBandwidthTable, LldpXdot1dcbxAdminETSRecoTrafficClassBandwidthGet, LldpXdot1dcbxAdminETSRecoTrafficClassBandwidthSet, LldpXdot1dcbxAdminETSRecoTrafficClassBandwidthTest, LldpXdot1dcbxAdminETSRecoTrafficClassBandwidthTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, LldpXdot1dcbxAdminETSRecoTrafficClassBandwidthTableINDEX, 2, 0, 0, NULL},

{{16,LldpXdot1dcbxAdminETSRecoTSATrafficClass}, GetNextIndexLldpXdot1dcbxAdminETSRecoTrafficSelectionAlgorithmTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, LldpXdot1dcbxAdminETSRecoTrafficSelectionAlgorithmTableINDEX, 2, 0, 0, NULL},

{{16,LldpXdot1dcbxAdminETSRecoTrafficSelectionAlgorithm}, GetNextIndexLldpXdot1dcbxAdminETSRecoTrafficSelectionAlgorithmTable, LldpXdot1dcbxAdminETSRecoTrafficSelectionAlgorithmGet, LldpXdot1dcbxAdminETSRecoTrafficSelectionAlgorithmSet, LldpXdot1dcbxAdminETSRecoTrafficSelectionAlgorithmTest, LldpXdot1dcbxAdminETSRecoTrafficSelectionAlgorithmTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, LldpXdot1dcbxAdminETSRecoTrafficSelectionAlgorithmTableINDEX, 2, 0, 0, NULL},

{{16,LldpXdot1dcbxAdminPFCWilling}, GetNextIndexLldpXdot1dcbxAdminPFCBasicTable, LldpXdot1dcbxAdminPFCWillingGet, LldpXdot1dcbxAdminPFCWillingSet, LldpXdot1dcbxAdminPFCWillingTest, LldpXdot1dcbxAdminPFCBasicTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, LldpXdot1dcbxAdminPFCBasicTableINDEX, 1, 0, 0, "2"},

{{16,LldpXdot1dcbxAdminPFCMBC}, GetNextIndexLldpXdot1dcbxAdminPFCBasicTable, LldpXdot1dcbxAdminPFCMBCGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, LldpXdot1dcbxAdminPFCBasicTableINDEX, 1, 0, 0, NULL},

{{16,LldpXdot1dcbxAdminPFCCap}, GetNextIndexLldpXdot1dcbxAdminPFCBasicTable, LldpXdot1dcbxAdminPFCCapGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, LldpXdot1dcbxAdminPFCBasicTableINDEX, 1, 0, 0, NULL},

{{16,LldpXdot1dcbxAdminPFCEnablePriority}, GetNextIndexLldpXdot1dcbxAdminPFCEnableTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, LldpXdot1dcbxAdminPFCEnableTableINDEX, 2, 0, 0, NULL},

{{16,LldpXdot1dcbxAdminPFCEnableEnabled}, GetNextIndexLldpXdot1dcbxAdminPFCEnableTable, LldpXdot1dcbxAdminPFCEnableEnabledGet, LldpXdot1dcbxAdminPFCEnableEnabledSet, LldpXdot1dcbxAdminPFCEnableEnabledTest, LldpXdot1dcbxAdminPFCEnableTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, LldpXdot1dcbxAdminPFCEnableTableINDEX, 2, 0, 0, "2"},

{{15,LldpXdot1dcbxAdminApplicationPriorityAESelector}, GetNextIndexLldpXdot1dcbxAdminApplicationPriorityAppTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, LldpXdot1dcbxAdminApplicationPriorityAppTableINDEX, 3, 0, 0, NULL},

{{15,LldpXdot1dcbxAdminApplicationPriorityAEProtocol}, GetNextIndexLldpXdot1dcbxAdminApplicationPriorityAppTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, LldpXdot1dcbxAdminApplicationPriorityAppTableINDEX, 3, 0, 0, NULL},

{{15,LldpXdot1dcbxAdminApplicationPriorityAEPriority}, GetNextIndexLldpXdot1dcbxAdminApplicationPriorityAppTable, LldpXdot1dcbxAdminApplicationPriorityAEPriorityGet, LldpXdot1dcbxAdminApplicationPriorityAEPrioritySet, LldpXdot1dcbxAdminApplicationPriorityAEPriorityTest, LldpXdot1dcbxAdminApplicationPriorityAppTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, LldpXdot1dcbxAdminApplicationPriorityAppTableINDEX, 3, 0, 0, NULL},
};
tMibData stddcbEntry = { 67, stddcbMibEntry };

#endif /* _STDDCBDB_H */

