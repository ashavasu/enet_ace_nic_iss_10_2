/*************************************************************************
 * Copyright (C) 2010 Aricent Inc . All Rights Reserved
 * $Id: fsdcbx.h,v 1.4 2016/03/05 12:17:50 siva Exp $
 * Description: This header file contains all the fs dcb OID with
 *              name mapping used in DCBX Module.
****************************************************************************/
/* Automatically generated by FuturePostmosy
 * Do not Edit!!!  */

#ifndef _SNMP_MIB_H
#define _SNMP_MIB_H

/* SNMP-MIB translation table. */

static struct MIB_OID fs_dcbx_orig_mib_oid_table[] = {
{"ccitt",        "0"},
{"iso",        "1"},
{"lldpExtensions",        "1.0.8802.1.1.2.1.5"},
{"lldpV2Xdot1MIB",        "1.0.8802.1.1.2.1.5.40000"},
{"org",        "1.3"},
{"dod",        "1.3.6"},
{"internet",        "1.3.6.1"},
{"directory",        "1.3.6.1.1"},
{"mgmt",        "1.3.6.1.2"},
{"mib-2",        "1.3.6.1.2.1"},
{"ip",        "1.3.6.1.2.1.4"},
{"transmission",        "1.3.6.1.2.1.10"},
{"mplsStdMIB",        "1.3.6.1.2.1.10.166"},
{"rmon",        "1.3.6.1.2.1.16"},
{"statistics",        "1.3.6.1.2.1.16.1"},
{"etherStatsHighCapacityEntry",        "1.3.6.1.2.1.16.1.7.1"},
{"history",        "1.3.6.1.2.1.16.2"},
{"hosts",        "1.3.6.1.2.1.16.4"},
{"hostTopN",        "1.3.6.1.2.1.16.5"},
{"matrix",        "1.3.6.1.2.1.16.6"},
{"filter",        "1.3.6.1.2.1.16.7"},
{"capture",        "1.3.6.1.2.1.16.8"},
{"tokenRing",        "1.3.6.1.2.1.16.10"},
{"protocolDist",        "1.3.6.1.2.1.16.12"},
{"nlHost",        "1.3.6.1.2.1.16.14"},
{"nlMatrix",        "1.3.6.1.2.1.16.15"},
{"alHost",        "1.3.6.1.2.1.16.16"},
{"alMatrix",        "1.3.6.1.2.1.16.17"},
{"usrHistory",        "1.3.6.1.2.1.16.18"},
{"probeConfig",        "1.3.6.1.2.1.16.19"},
{"rmonConformance",        "1.3.6.1.2.1.16.20"},
{"dot1dBridge",        "1.3.6.1.2.1.17"},
{"dot1dStp",        "1.3.6.1.2.1.17.2"},
{"dot1dTp",        "1.3.6.1.2.1.17.4"},
{"vrrpOperEntry",        "1.3.6.1.2.1.18.1.3.1"},
{"dns",        "1.3.6.1.2.1.32"},
{"experimental",        "1.3.6.1.3"},
{"private",        "1.3.6.1.4"},
{"enterprises",        "1.3.6.1.4.1"},
{"ARICENT-MPLS-TP-MIB",        "1.3.6.1.4.1.2076.13.4"},
{"issExt",        "1.3.6.1.4.1.2076.81.8"},
{"fsDot1dBridge",        "1.3.6.1.4.1.2076.116"},
{"fsDot1dStp",        "1.3.6.1.4.1.2076.116.2"},
{"fsDot1dTp",        "1.3.6.1.4.1.2076.116.4"},
{"fsDcbMIB",        "1.3.6.1.4.1.29601.2.22"},
{"fsDcbSystem",        "1.3.6.1.4.1.29601.2.22.1"},
{"fsDcbPfcMinThreshold",        "1.3.6.1.4.1.29601.2.22.1.1"},
{"fsDcbPfcMaxThreshold",        "1.3.6.1.4.1.29601.2.22.1.2"},
{"fsDcbMaxPfcProfiles",        "1.3.6.1.4.1.29601.2.22.1.3"},
{"fsDcbObjects",        "1.3.6.1.4.1.29601.2.22.2"},
{"fsDcbPortTable",        "1.3.6.1.4.1.29601.2.22.2.1"},
{"fsDcbPortEntry",        "1.3.6.1.4.1.29601.2.22.2.1.1"},
{"fsDcbPortNumber",        "1.3.6.1.4.1.29601.2.22.2.1.1.1"},
{"fsDcbETSAdminStatus",        "1.3.6.1.4.1.29601.2.22.2.1.1.2"},
{"fsDcbPFCAdminStatus",        "1.3.6.1.4.1.29601.2.22.2.1.1.3"},
{"fsDcbRowStatus",        "1.3.6.1.4.1.29601.2.22.2.1.1.4"},
{"fsDcbAppPriAdminStatus",        "1.3.6.1.4.1.29601.2.22.2.1.1.5"},
{"fsDcbOperVersion",        "1.3.6.1.4.1.29601.2.22.2.1.1.6"},
{"fsDcbMaxVersion",        "1.3.6.1.4.1.29601.2.22.2.1.1.7"},
{"fsDcbPeerOperVersion",        "1.3.6.1.4.1.29601.2.22.2.1.1.8"},
{"fsDcbPeerMaxVersion",        "1.3.6.1.4.1.29601.2.22.2.1.1.9"},
{"fsDcbApplicationObjects",        "1.3.6.1.4.1.29601.2.22.3"},
{"fsDCBXObjects",        "1.3.6.1.4.1.29601.2.22.3.1"},
{"fsDCBXScalars",        "1.3.6.1.4.1.29601.2.22.3.1.1"},
{"fsDcbxGlobalTraceLevel",        "1.3.6.1.4.1.29601.2.22.3.1.1.1"},
{"fsDCBXPortTable",        "1.3.6.1.4.1.29601.2.22.3.1.2"},
{"fsDCBXPortEntry",        "1.3.6.1.4.1.29601.2.22.3.1.2.1"},
{"fsDCBXPortNumber",        "1.3.6.1.4.1.29601.2.22.3.1.2.1.1"},
{"fsDCBXAdminStatus",        "1.3.6.1.4.1.29601.2.22.3.1.2.1.2"},
{"fsDCBXMode",        "1.3.6.1.4.1.29601.2.22.3.1.2.1.3"},
{"fsETSObjects",        "1.3.6.1.4.1.29601.2.22.3.2"},
{"fsETSScalars",        "1.3.6.1.4.1.29601.2.22.3.2.1"},
{"fsETSSystemControl",        "1.3.6.1.4.1.29601.2.22.3.2.1.1"},
{"fsETSModuleStatus",        "1.3.6.1.4.1.29601.2.22.3.2.1.2"},
{"fsETSClearCounters",        "1.3.6.1.4.1.29601.2.22.3.2.1.3"},
{"fsETSGlobalEnableTrap",        "1.3.6.1.4.1.29601.2.22.3.2.1.4"},
{"fsETSGeneratedTrapCount",        "1.3.6.1.4.1.29601.2.22.3.2.1.5"},
{"fsETSPortTable",        "1.3.6.1.4.1.29601.2.22.3.2.2"},
{"fsETSPortEntry",        "1.3.6.1.4.1.29601.2.22.3.2.2.1"},
{"fsETSPortNumber",        "1.3.6.1.4.1.29601.2.22.3.2.2.1.1"},
{"fsETSAdminMode",        "1.3.6.1.4.1.29601.2.22.3.2.2.1.2"},
{"fsETSDcbxOperState",        "1.3.6.1.4.1.29601.2.22.3.2.2.1.3"},
{"fsETSDcbxStateMachine",        "1.3.6.1.4.1.29601.2.22.3.2.2.1.4"},
{"fsETSClearTLVCounters",        "1.3.6.1.4.1.29601.2.22.3.2.2.1.5"},
{"fsETSConfTxTLVCounter",        "1.3.6.1.4.1.29601.2.22.3.2.2.1.6"},
{"fsETSConfRxTLVCounter",        "1.3.6.1.4.1.29601.2.22.3.2.2.1.7"},
{"fsETSConfRxTLVErrors",        "1.3.6.1.4.1.29601.2.22.3.2.2.1.8"},
{"fsETSRecoTxTLVCounter",        "1.3.6.1.4.1.29601.2.22.3.2.2.1.9"},
{"fsETSRecoRxTLVCounter",        "1.3.6.1.4.1.29601.2.22.3.2.2.1.10"},
{"fsETSRecoRxTLVErrors",        "1.3.6.1.4.1.29601.2.22.3.2.2.1.11"},
{"fsETSTcSuppTxTLVCounter",        "1.3.6.1.4.1.29601.2.22.3.2.2.1.12"},
{"fsETSTcSuppRxTLVCounter",        "1.3.6.1.4.1.29601.2.22.3.2.2.1.13"},
{"fsETSTcSuppRxTLVErrors",        "1.3.6.1.4.1.29601.2.22.3.2.2.1.14"},
{"fsETSRowStatus",        "1.3.6.1.4.1.29601.2.22.3.2.2.1.15"},
{"fsETSSyncd",        "1.3.6.1.4.1.29601.2.22.3.2.2.1.16"},
{"fsETSError",        "1.3.6.1.4.1.29601.2.22.3.2.2.1.17"},
{"fsETSDcbxStatus",        "1.3.6.1.4.1.29601.2.22.3.2.2.1.18"},
{"fsPFCObjects",        "1.3.6.1.4.1.29601.2.22.3.3"},
{"fsPFCScalars",        "1.3.6.1.4.1.29601.2.22.3.3.1"},
{"fsPFCSystemControl",        "1.3.6.1.4.1.29601.2.22.3.3.1.1"},
{"fsPFCModuleStatus",        "1.3.6.1.4.1.29601.2.22.3.3.1.2"},
{"fsPFCClearCounters",        "1.3.6.1.4.1.29601.2.22.3.3.1.3"},
{"fsPFCGlobalEnableTrap",        "1.3.6.1.4.1.29601.2.22.3.3.1.4"},
{"fsPFCGeneratedTrapCount",        "1.3.6.1.4.1.29601.2.22.3.3.1.5"},
{"fsPFCPortTable",        "1.3.6.1.4.1.29601.2.22.3.3.2"},
{"fsPFCPortEntry",        "1.3.6.1.4.1.29601.2.22.3.3.2.1"},
{"fsPFCPortNumber",        "1.3.6.1.4.1.29601.2.22.3.3.2.1.1"},
{"fsPFCAdminMode",        "1.3.6.1.4.1.29601.2.22.3.3.2.1.2"},
{"fsPFCDcbxOperState",        "1.3.6.1.4.1.29601.2.22.3.3.2.1.3"},
{"fsPFCDcbxStateMachine",        "1.3.6.1.4.1.29601.2.22.3.3.2.1.4"},
{"fsPFCClearTLVCounters",        "1.3.6.1.4.1.29601.2.22.3.3.2.1.5"},
{"fsPFCTxTLVCounter",        "1.3.6.1.4.1.29601.2.22.3.3.2.1.6"},
{"fsPFCRxTLVCounter",        "1.3.6.1.4.1.29601.2.22.3.3.2.1.7"},
{"fsPFCRxTLVErrors",        "1.3.6.1.4.1.29601.2.22.3.3.2.1.8"},
{"fsPFCRowStatus",        "1.3.6.1.4.1.29601.2.22.3.3.2.1.9"},
{"fsPFCSyncd",        "1.3.6.1.4.1.29601.2.22.3.3.2.1.10"},
{"fsPFCError",        "1.3.6.1.4.1.29601.2.22.3.3.2.1.11"},
{"fsPFCDcbxStatus",        "1.3.6.1.4.1.29601.2.22.3.3.2.1.12"},
{"fsAppPriObjects",        "1.3.6.1.4.1.29601.2.22.3.4"},
{"fsAppPriScalars",        "1.3.6.1.4.1.29601.2.22.3.4.1"},
{"fsAppPriSystemControl",        "1.3.6.1.4.1.29601.2.22.3.4.1.1"},
{"fsAppPriModuleStatus",        "1.3.6.1.4.1.29601.2.22.3.4.1.2"},
{"fsAppPriClearCounters",        "1.3.6.1.4.1.29601.2.22.3.4.1.3"},
{"fsAppPriGlobalEnableTrap",        "1.3.6.1.4.1.29601.2.22.3.4.1.4"},
{"fsAppPriGeneratedTrapCount",        "1.3.6.1.4.1.29601.2.22.3.4.1.5"},
{"fsAppPriPortTable",        "1.3.6.1.4.1.29601.2.22.3.4.2"},
{"fsAppPriPortEntry",        "1.3.6.1.4.1.29601.2.22.3.4.2.1"},
{"fsAppPriPortNumber",        "1.3.6.1.4.1.29601.2.22.3.4.2.1.1"},
{"fsAppPriAdminMode",        "1.3.6.1.4.1.29601.2.22.3.4.2.1.2"},
{"fsAppPriDcbxOperState",        "1.3.6.1.4.1.29601.2.22.3.4.2.1.3"},
{"fsAppPriDcbxStateMachine",        "1.3.6.1.4.1.29601.2.22.3.4.2.1.4"},
{"fsAppPriClearTLVCounters",        "1.3.6.1.4.1.29601.2.22.3.4.2.1.5"},
{"fsAppPriTxTLVCounter",        "1.3.6.1.4.1.29601.2.22.3.4.2.1.6"},
{"fsAppPriRxTLVCounter",        "1.3.6.1.4.1.29601.2.22.3.4.2.1.7"},
{"fsAppPriRxTLVErrors",        "1.3.6.1.4.1.29601.2.22.3.4.2.1.8"},
{"fsAppPriAppProtocols",        "1.3.6.1.4.1.29601.2.22.3.4.2.1.9"},
{"fsAppPriRowStatus",        "1.3.6.1.4.1.29601.2.22.3.4.2.1.10"},
{"fsAppPriSyncd",        "1.3.6.1.4.1.29601.2.22.3.4.2.1.11"},
{"fsAppPriError",        "1.3.6.1.4.1.29601.2.22.3.4.2.1.12"},
{"fsAppPriDcbxStatus",        "1.3.6.1.4.1.29601.2.22.3.4.2.1.13"},
{"fsAppPriXAppTable",        "1.3.6.1.4.1.29601.2.22.3.4.3"},
{"fsAppPriXAppEntry",        "1.3.6.1.4.1.29601.2.22.3.4.3.1"},
{"fsAppPriXAppRowStatus",        "1.3.6.1.4.1.29601.2.22.3.4.3.1.1"},
{"fslldpXdot1dcbxLocApplicationPriorityBasicTable",        "1.3.6.1.4.1.29601.2.22.3.4.4"},
{"fslldpXdot1dcbxLocApplicationPriorityBasicEntry",        "1.3.6.1.4.1.29601.2.22.3.4.4.1"},
{"fslldpXdot1dcbxLocApplicationPriorityWilling",        "1.3.6.1.4.1.29601.2.22.3.4.4.1.1"},
{"fslldpXdot1dcbxAdminApplicationPriorityBasicTable",        "1.3.6.1.4.1.29601.2.22.3.4.5"},
{"fslldpXdot1dcbxAdminApplicationPriorityBasicEntry",        "1.3.6.1.4.1.29601.2.22.3.4.5.1"},
{"fslldpXdot1dcbxAdminApplicationPriorityWilling",        "1.3.6.1.4.1.29601.2.22.3.4.5.1.1"},
{"fslldpXdot1dcbxRemApplicationPriorityBasicTable",        "1.3.6.1.4.1.29601.2.22.3.4.6"},
{"fslldpXdot1dcbxRemApplicationPriorityBasicEntry",        "1.3.6.1.4.1.29601.2.22.3.4.6.1"},
{"fslldpXdot1dcbxRemApplicationPriorityWilling",        "1.3.6.1.4.1.29601.2.22.3.4.6.1.1"},
{"fsTCSupportedObjects",        "1.3.6.1.4.1.29601.2.22.3.5"},
{"fslldpXdot1dcbxConfigTCSupportedTable",        "1.3.6.1.4.1.29601.2.22.3.5.1"},
{"fslldpXdot1dcbxConfigTCSupportedEntry",        "1.3.6.1.4.1.29601.2.22.3.5.1.1"},
{"fslldpXdot1dcbxConfigTCSupportedTxEnable",        "1.3.6.1.4.1.29601.2.22.3.5.1.1.1"},
{"fslldpXdot1dcbxLocTCSupportedTable",        "1.3.6.1.4.1.29601.2.22.3.5.2"},
{"fslldpXdot1dcbxLocTCSupportedEntry",        "1.3.6.1.4.1.29601.2.22.3.5.2.1"},
{"fslldpXdot1dcbxLocTCSupported",        "1.3.6.1.4.1.29601.2.22.3.5.2.1.1"},
{"fslldpXdot1dcbxRemTCSupportedTable",        "1.3.6.1.4.1.29601.2.22.3.5.3"},
{"fslldpXdot1dcbxRemTCSupportedEntry",        "1.3.6.1.4.1.29601.2.22.3.5.3.1"},
{"fslldpXdot1dcbxRemTCSupported",        "1.3.6.1.4.1.29601.2.22.3.5.3.1.1"},
{"fslldpXdot1dcbxAdminTCSupportedTable",        "1.3.6.1.4.1.29601.2.22.3.5.4"},
{"fslldpXdot1dcbxAdminTCSupportedEntry",        "1.3.6.1.4.1.29601.2.22.3.5.4.1"},
{"fslldpXdot1dcbxAdminTCSupported",        "1.3.6.1.4.1.29601.2.22.3.5.4.1.1"},
{"fsDcbxCEEObjects",        "1.3.6.1.4.1.29601.2.22.3.6"},
{"fsDcbxCEEScalars",        "1.3.6.1.4.1.29601.2.22.3.6.1"},
{"fsDcbxCEEGlobalEnableTrap",        "1.3.6.1.4.1.29601.2.22.3.6.1.1"},
{"fsDcbxCEEGeneratedTrapCount",        "1.3.6.1.4.1.29601.2.22.3.6.1.2"},
{"fsDcbxCEEClearCounters",        "1.3.6.1.4.1.29601.2.22.3.6.1.3"},
{"fsDcbxCEECtrlTable",        "1.3.6.1.4.1.29601.2.22.3.6.2"},
{"fsDcbxCEECtrlEntry",        "1.3.6.1.4.1.29601.2.22.3.6.2.1"},
{"fsDcbxCEECtrlPortNumber",        "1.3.6.1.4.1.29601.2.22.3.6.2.1.1"},
{"fsDcbxCEECtrlSeqNo",        "1.3.6.1.4.1.29601.2.22.3.6.2.1.2"},
{"fsDcbxCEECtrlAckNo",        "1.3.6.1.4.1.29601.2.22.3.6.2.1.3"},
{"fsDcbxCEECtrlRcvdAckNo",        "1.3.6.1.4.1.29601.2.22.3.6.2.1.4"},
{"fsDcbxCEECtrlTxTLVCounter",        "1.3.6.1.4.1.29601.2.22.3.6.2.1.5"},
{"fsDcbxCEECtrlRxTLVCounter",        "1.3.6.1.4.1.29601.2.22.3.6.2.1.6"},
{"fsDcbxCEECtrlRxTLVErrorCounter",        "1.3.6.1.4.1.29601.2.22.3.6.2.1.7"},
{"fsDcbNotificationObjects",        "1.3.6.1.4.1.29601.2.22.4"},
{"fsDCBTraps",        "1.3.6.1.4.1.29601.2.22.4.0"},
{"fsDCBTrapObjects",        "1.3.6.1.4.1.29601.2.22.4.1"},
{"fsDcbTrapPortNumber",        "1.3.6.1.4.1.29601.2.22.4.1.1"},
{"fsDcbPeerUpStatus",        "1.3.6.1.4.1.29601.2.22.4.1.2"},
{"fsDcbFeatureType",        "1.3.6.1.4.1.29601.2.22.4.1.3"},
{"security",        "1.3.6.1.5"},
{"snmpV2",        "1.3.6.1.6"},
{"snmpDomains",        "1.3.6.1.6.1"},
{"snmpProxys",        "1.3.6.1.6.2"},
{"snmpModules",        "1.3.6.1.6.3"},
{"snmpTraps",        "1.3.6.1.6.3.1.1.5"},
{"snmpAuthProtocols",        "1.3.6.1.6.3.10.1.1"},
{"snmpPrivProtocols",        "1.3.6.1.6.3.10.1.2"},
{"ieee802dot1mibs",        "1.111.2.802.1"},
{"joint-iso-ccitt",        "2"},
{0,0}
};

#endif /* _SNMP_MIB_H */
