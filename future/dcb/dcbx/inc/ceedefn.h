/*************************************************************************
* Copyright (C) 2010 Aricent Inc . All Rights Reserved
* $Id: ceedefn.h,v 1.6 2017/08/24 11:59:42 siva Exp $
* Description: This header file contains all the Macros and definitions 
*              used for DCBX Modules.
****************************************************************************/
#ifndef __CEE_DEFS_H__
#define __CEE_DEFS_H__

/* TLV Hdr Len is Type(7bits), Length(9bits), OUI(3bytes) and Sub-Type (1bytes)*/
#define CEE_TLV_HDR_LEN                 6
/* Max length will be Ctrl TLV + PFC + PG + AP */
#define CEE_TLV_MAX_LEN                 CEE_TLV_HDR_LEN +     \
                                        CEE_CONTROL_TLV_LEN + \
                                        CEE_PG_TLV_LEN +      \
                                        CEE_PFC_TLV_LEN +     \
                                        CEE_APP_PRI_MAX_TLV_LEN

#define CEE_TLV_TYPE_MASK               (UINT2) 0xFE00 /* TLV Type = 7 Bits */
#define CEE_TLV_LEN_MASK                (UINT2) 0x01FF /* TLV Length = 9 Bits */
#define CEE_APP_PRI_SELECTOR_BIT_MASK   0x03

#define DCBX_GET_MODE(u1DcbxMode) \
    ((u1DcbxMode == DCBX_MODE_AUTO) ? "DCBX_MODE_AUTO" : \
                 ((u1DcbxMode == DCBX_MODE_IEEE) ? \
                  "DCBX_MODE_IEEE":"DCBX_MODE_CEE"))
#define DCBX_GET_ETS_MODE(u1Status) \
    ((u1Status == ETS_ENABLED)? "ETS_ENABLED" : "ETS_DISABLED")

#define DCBX_GET_APP_PRI_MODE(u1Status) \
    ((u1Status == APP_PRI_ENABLED)? "APP_PRI_ENABLED" : "APP_PRI_DISABLED")
 
#define DCBX_GET_PFC_MODE(u1Status) \
    ((u1Status == PFC_ENABLED)? "PFC_ENABLED" : "PFC_DISABLED")

#define DCBX_GET_FEATURE_TYPE_MODE(u1FeatType) \
    ((u1FeatType == CEE_ETS_TLV_TYPE)? "ETS" : \
      ((u1FeatType == CEE_PFC_TLV_TYPE)? "PFC" : "App Priority"))

/*
 <-----------------------6 octects------------------->
______________________________________________________
|          |            |                |           |
|  TLV     |   Length   | OUI = "001B21" | Subtype =2|
| Type =127|            |   3 octets     | 1 octect  |
|__________|____________|________________|___________|
                                      
*/
#define CEE_TLV_TYPE                        127
#define CEE_TLV_TYPE_LEN                    2 
#define CEE_TLV_SUBTYPE                     2 

/**** Feature SUB TLV Header Related Macros ****/
/*
 * Common Sub TLV header for PFC, ETS and Application Priority TLV 
_____________________________________________________________________________
|          |            |           |           |  |  |   |      |          |
|          |            | Operating | Max       |  |  |   | Re-  |          |
| Type = 2 | Length= 17 | Version   | Version   |En|W.|Err|served| SubType  |
|__________|____________|___________|___________|__|__|___|______|__________|
   7 bits       9 bits    1 octet      1 octet   1  1   1    5     1 octect 
                                                bit bit bit bits 
*/
#define CEE_FEATURE_SUB_TLV_SUBTYPE            0 /* Same SubType for all TLVs */
#define CEE_FEATURE_SUB_TLV_TYPE_LEN           2 /* Type     - 7 bits
                                                  * Length   - 9 bits
                                                  */ 
#define CEE_FEATURE_SUB_TLV_OPER_VER_LEN       1 /* 1 Octect */ 
#define CEE_FEATURE_SUB_TLV_MAX_VER_LEN        1 /* 1 Octect */
#define CEE_FEATURE_SUB_TLV_VECTOR_LEN         1 /* Enable    - 1 bit
                                                  * Willing   - 1 bit
                                                  * Err       - 1 bit
                                                  * Reserverd - 5 bits
                                                  */ 
#define CEE_FEATURE_SUB_TLV_SUBTYPE_LEN        1 /* 1 Octet */

#define CEE_ENABLE_BIT_MASK                    0x80
#define CEE_WILLING_BIT_MASK                   0x40
#define CEE_ERROR_BIT_MASK                     0x20
/**** End of Feature SUB TLV Header Related Macros ****/

/******************* DCBx CONTROL TLV RELATED MACROS  *******************/
   /*
    DCBx CONTROL TLV
    _________________________________________________________________________________________
    |          |            |           |           |                  |                    |
    |          |            | Operating | Max       |                  |                    |
    | Type = 1 | Length= 10 | Version   | Version   |    Seq No        |      Ack No        |
    |__________|____________|___________|___________|__________________|____________________|
       7 bits       9 bits    1 octet      1 octet       4 octet               4 octet       
                                                                 
    */
#define CEE_CONTROL_TLV_TYPE                  1
#define CEE_CONTROL_TLV_SEQ_NO_LEN            4
#define CEE_CONTROL_TLV_ACK_NO_LEN            4
#define CEE_CONTROL_TLV_PAYLOAD_LEN           10
/* Total TLV length = 12 Octect */
#define CEE_CONTROL_TLV_LEN                   12
/******************* End of DCBx CONTROL TLV *******************/

/******************* Priority Grouping TLV RELATED MACROS *******************/
/*
* PRIORITY-BASED FLOW CONTROL TLV
_______________________________________________________________________________________________________________________________________
|          |            |           |           |  |  |   |      |          |                      |                         |        |  
|          |            | Operating | Max       |  |  |   | Re-  |          | Priority grouping to |Priority grouping Percent|Max Num.|
| Type = 2 | Length= 17 | Version   | Version   |En|W.|Err|served| SubType  |        priority      |       Allocation        | of TCs |
|__________|____________|___________|___________|__|__|___|______|__________|______________________|_________________________|________|
   7 bits       9 bits    1 octet      1 octet   1  1   1    5     1 octect        4 octets                8 octects          1 octet
                                                bit bit bit bits 
*/
#define CEE_PG_TLV_TYPE                    2
#define CEE_PG_TO_PRIORITY_LEN             4
#define CEE_PG_PERCENT_ALLOC_LEN           8
#define CEE_PG_MAX_NO_OF_TCS_LEN           1
#define CEE_PG_TLV_PAYLOAD_LEN             17
/* Total PFC TLV Length = 19 octects*/
#define CEE_PG_TLV_LEN                     (CEE_FEATURE_SUB_TLV_TYPE_LEN + \
                                            CEE_FEATURE_SUB_TLV_OPER_VER_LEN + \
                                            CEE_FEATURE_SUB_TLV_MAX_VER_LEN + \
                                            CEE_FEATURE_SUB_TLV_VECTOR_LEN + \
                                            CEE_FEATURE_SUB_TLV_SUBTYPE_LEN + \
                                            CEE_PG_TO_PRIORITY_LEN + \
                                            CEE_PG_PERCENT_ALLOC_LEN + \
                                            CEE_PG_MAX_NO_OF_TCS_LEN)

/******************* Priority Grouping TLV MACROS END *******************/



/******************* PFC TLV RELATED MACROS *******************/
/*
* PRIORITY-BASED FLOW CONTROL TLV
___________________________________________________________________________________________________________
|          |            |           |           |  |  |   |        |          |             |             |   
|          |            | Operating | Max       |  |  |   |  Re-   |          | PFC Enabled | Max TCs W/  |
| Type = 3 | Length= 6  | Version   | Version   |En|W.|Err| served | SubType  |   Table     | PFC Support |
|__________|____________|___________|___________|__|__|___|________|__________|_____________|_____________|
   7 bits       9 bits    1 octet      1 octet   1  1   1   5 bits   1 octect    1 octet        1 octect
                                                bit bit bit
*/

#define CEE_PFC_ENABLED_TABLE_LEN          1
#define CEE_PFC_MAX_TCS_SUPPORT_LEN        1
#define CEE_PFC_TLV_PAYLOAD_LEN            6
/* Total PFC TLV Length = 8 octets*/
#define CEE_PFC_TLV_LEN                    (CEE_FEATURE_SUB_TLV_TYPE_LEN + \
                                            CEE_FEATURE_SUB_TLV_OPER_VER_LEN + \
                                            CEE_FEATURE_SUB_TLV_MAX_VER_LEN + \
                                            CEE_FEATURE_SUB_TLV_VECTOR_LEN + \
                                            CEE_FEATURE_SUB_TLV_SUBTYPE_LEN + \
                                            CEE_PFC_ENABLED_TABLE_LEN + \
                                            CEE_PFC_MAX_TCS_SUPPORT_LEN)

/******************* PFC TLV MACROS END *******************/


/******************* APPLICATION PRIORITY TLV RELATED MACROS *******************/
/*
* APPLICATION TLV
_________________________________________________________________________________________________________
|          |            |           |           |  |  |   |        |            |                       |    
|          |            | Operating | Max       |  |  |   |        |            |                       |    
| Type = 4 | Length= 16 | Version   | Version   |En|W.|Err|Reserved|Sub_Type = 0|   Application Table   |  
|__________|____________|___________|___________|__|__|___|________|____________|_______________________|
   7 bits       9 bits    1 octet      1 octet   1  1   1   5 bits   1 octect      Multiple of 6 octets
                                                bit bit bit
* Application table:
* -----------------
    ____________________________________________________________
    | Application | Upper | Sel. |              | User Priority |
    | Protocol ID | OUI   | Field|   Lower OUI  |    Map        |
    |_____________|_______|______|______________|_______________|
       2 octets     6 bits   2       2 octets       1 octect
                            bits
*/

#define CEE_APP_PRI_TABLE_LEN         6 /* Application Table:
                                         * AppId       - 2 Octet
                                         * UpperOUI    - 6 bits
                                         * Sel Field   - 2 bits
                                         * Lower OUI   - 2 Octet
                                         * User Pri Map- 1 Octet
                                         */

/* TLV Length excludeing TYPE and Length fields */
#define CEE_APP_PRI_TLV_HDR_LEN       (CEE_FEATURE_SUB_TLV_OPER_VER_LEN + \
                                       CEE_FEATURE_SUB_TLV_MAX_VER_LEN + \
                                       CEE_FEATURE_SUB_TLV_VECTOR_LEN + \
                                       CEE_FEATURE_SUB_TLV_SUBTYPE_LEN)

/* Total APPLICATION PRIORITY TLV Length */
#define CEE_APP_PRI_MAX_TLV_LEN      (CEE_FEATURE_SUB_TLV_TYPE_LEN +     \
                                       CEE_APP_PRI_TLV_HDR_LEN +      \
                                       (CEE_APP_PRI_TABLE_LEN *       \
                                       DCBX_MAX_APP_PRI_ENTRIES_PER_PORT))

/************* APPLICATION PRIORITY TLV MACROS END *******************/

/* Get the Application Id */
#define CEE_GET_APPL_ID(CEEAppId)\
{\
    CEEAppId.u2TlvType = CEE_TLV_TYPE; \
    CEEAppId.u1SubType = CEE_TLV_SUBTYPE; \
    MEMCPY(CEEAppId.au1OUI, gau1CEEOUI, DCBX_MAX_OUI_LEN);\
}  

#define CEE_USE_LOCAL_CFG  DCBX_OPER_INIT
#define CEE_USE_PEER_CFG  DCBX_OPER_RECO

#define CEE_MAX_ACK_MISS_CNT                3
#define CEE_OPER_DISABLED                   3
#define CEE_OPER_USE_LOCAL_CFG              4
#define CEE_OPER_USE_PEER_CFG               5

#define CEE_SET_CURRENT_STATE(pPortInfo, i4State) \
{\
    pPortInfo->u1CurrentState = i4State; \
}


#define CEE_APP_PRI_ETHERTYPE_SEL           0
#define CEE_APP_PRI_TCP_UDP_SEL             1

#define CEE_APP_PRI_IEEE_ETHERTYPE_SEL      1
#define CEE_APP_PRI_IEEE_TCP_UDP_SEL        4


#define CEE_APP_PRI_TLV_INFO_STR_LEN       (CEE_FEATURE_SUB_TLV_VECTOR_LEN + \
                                            CEE_FEATURE_SUB_TLV_SUBTYPE_LEN + \
                                            CEE_FEATURE_SUB_TLV_OPER_VER_LEN + \
                                            CEE_FEATURE_SUB_TLV_MAX_VER_LEN)
#define CEE_APP_PRI_TABLE_ENTRY_LEN         6

#define APP_PRI_GET_UPM_FRM_IEEE_TO_CEE(u1UserPriMap)\
{\
    UINT1 u1Cnt = 0;\
    UINT1 u1Loop = u1UserPriMap;\
    UINT1 u1RetUserPri = 1;\
    if (u1UserPriMap == 0)\
    {\
        u1UserPriMap = u1RetUserPri;\
    }\
    else\
    {\
        for (u1Cnt = 0; u1Cnt < u1Loop; u1Cnt++)\
        {\
            u1RetUserPri  = (UINT1)(u1RetUserPri << 1);\
        }\
        u1UserPriMap = u1RetUserPri;\
    }\
}

#define APP_PRI_GET_UPM_FRM_CEE_TO_IEEE(u1Priority)\
{\
    UINT1 u1Cnt = 0;\
    UINT1 u1BitMap = 1;\
    for (u1Cnt = 0; u1Cnt < DCBX_MAX_PRIORITIES; u1Cnt++)\
    {\
        if (u1Priority == u1BitMap)\
        {\
            u1Priority = u1Cnt;\
            break;\
        }\
        u1BitMap = (UINT1)(u1BitMap << 1);\
    }\
}

#define DCBX_GET_CEE_OPERST_FRM_IEEE(u1OperState, u1RetOperState)\
{\
    u1RetOperState = u1OperState + 3;\
}

#define CEE_INVALID_VALUE                   -1

/* CEE Feature state machine EVENTS */
enum
{
    CEE_EV_FEAT_TLV_RECEIVED   = 0, /* Received DCBX Feature TLV */
    CEE_EV_NO_DCBX             = 1, /* No DCBX frame received */
    CEE_EV_NO_FEAT_TLV_RECEIVED= 2, /* No Feature TLV received */
    CEE_EV_DUP_TLV_RECEIVED    = 3, /* Duplicate Feature TLV received */
    CEE_EV_INIT                = 4, /* Init Feature */
    CEE_EV_INVALID_PKT_RECEIVED= 5, /* Incorrect/mismatch oper/max version received*/
    CEE_MAX_EVENTS             = 6
};

/* CEE Feature state machine STATES */
enum
{
    CEE_ST_WAIT_FOR_FEAT_TLV  = 0,
    CEE_ST_DELETE_AGED_INFO   = 1,
    CEE_ST_USE_LOC_CFG        = 2,
    CEE_ST_USE_PEER_CFG       = 3,
    CEE_ST_CFG_NOT_CMP        = 4,
    CEE_ST_RCVD_DUP_TLV       = 5,
    CEE_ST_FEAT_INIT          = 6,
    CEE_MAX_STATES            = 7
};

enum
{
    CEE_CTRL_TLV_TYPE = 1,
    CEE_ETS_TLV_TYPE,
    CEE_PFC_TLV_TYPE,
    CEE_APP_PRI_TLV_TYPE,
    CEE_MAX_TLV_TYPE
};

/* Index of the Sem Function pointers */
enum
{
    A0  = 0,
    A1  = 1,
    A2  = 2,
    A3  = 3,
    A4  = 4,
    A5  = 5,
    A6  = 6,
    A7  = 7,
    CEE_MAX_SEM_FN_PTRS  = 8
};

#endif /* __CEE_DEFS_H__ */

