/*************************************************************************
* Copyright (C) 2010 Aricent Inc . All Rights Reserved
* $Id: dcbxdefn.h,v 1.14 2017/01/25 13:19:41 siva Exp $
* Description: This header file contains all the Macros and definitions 
*              used for DCBX Modules.
****************************************************************************/

#ifndef __DCB_DEFS_H__
#define __DCB_DEFS_H__

/*DCBX MACROS*/

/* DCBX Task & Q related Macros*/

#define DCBX_MAX_Q_DEPTH       (DCBX_MAX_PORT_LIST_SIZE * 14)
                                       /* Nominal Value */
#define DCBX_TASK_QUEUE_LIMIT   (DCBX_MAX_Q_DEPTH* 0.6)

#define DCBX_TASK_NAME         ((UINT1 *)"DCBT")
#define DCBX_TASK_QUEUE_NAME   ((UINT1 *)"DCBQ")
#define DCBX_PROTO_SEM         ((UINT1 *)"DCBX")


/* Event Related Macros */
#define DCBX_QMSG_EVENT        0x00000001
#define DCBX_ALL_EVENTS        0xFFFFFFFF

/* Packet Dump Tx or Rx Macros */
#define DCBX_TX                1
#define DCBX_RX                2

enum
{
    /* DCBX Queue Message Types */
    DCBX_CREATE_PORT_MSG =  L2IWF_LLDP_APPL_MAX_MSG,
    DCBX_DELETE_PORT_MSG,
    DCBX_ADD_LAG_MEMBER_MSG,
    DCBX_REM_LAG_MEMBER_MSG,
    DCBX_RM_MSG,
    DCBX_MBSM_MSG_CARD_INSERT,
};

/*DCBX MAX STATUS LIST*/
#define MAX_DCBX_STATUS                            11

/* List of Traffic Selection Algorithms */
#define DCBX_SPA_NAME                              "spa"  
#define DCBX_CBSA_NAME                             "cbsa"
#define DCBX_ETSA_NAME                             "etsa"
#define DCBX_VSA_NAME                              "vsa"
#define DCBX_STRICT_PRIORITY_ALGO                  0
#define DCBX_CREDIT_BASED_SHAPER_ALGO              1
#define DCBX_ENHANCED_TRANSMISSION_SELECTION_ALGO  2
#define DCBX_VENDOR_SPECIFIC_ALGO                  255

#define CLI_ETS_UPDATE_ADM_CON_TABLE               1
#define CLI_ETS_UPDATE_ADM_RECO_TABLE              0

/* Message Type used by ETS/PFC to post and 
 * receive message from/to  DCBX */
#define DCBX_LLDP_PORT_REG                         1
#define DCBX_LLDP_PORT_UPDATE                      2
#define DCBX_LLDP_PORT_DEREG                       3
#define DCBX_STATE_MACHINE                         4
#define DCBX_OPER_UPDATE                           5
#define DCBX_LLDP_CEE_ACK                          6

/* DCBX Constant value Macros */
#define DCBX_INIT_VAL                              0
#define DCBX_MIN_VAL                               1
#define DCBX_INVALID_ID                           -1

#define DCBX_ONE                                   1
#define DCBX_TWO                                   2
#define DCBX_THREE                                 3
#define DCBX_FOUR                                  4
#define DCBX_FIVE                                  5
#define DCBX_SEVEN                                 7
#define DCBX_EIGHT                                 8
#define DCBX_NINE                                  9

/* Enabled/Disabled Macros */
#define DCBX_ENABLED                               1
#define DCBX_DISABLED                              2

#define DCBX_MAX_PRIORITIES                        8
#define DCBX_MAX_VALID_PRI                         7

/* DCBX Status related macros */
#define DCBX_STATUS_PEER_NO_ADV_FEAT               1
#define DCBX_STATUS_PEER_NO_ADV_DCBX               2
#define DCBX_STATUS_NOT_ADVERTISE                  3
#define DCBX_STATUS_DISABLED                       4
#define DCBX_STATUS_PEER_DISABLED                  5
#define DCBX_STATUS_PEER_IN_ERROR                  6
#define DCBX_STATUS_PEER_NW_CFG_COMPAT             7
#define DCBX_STATUS_CFG_NOT_COMPT                  8
#define DCBX_STATUS_OK                             9
#define DCBX_STATUS_UNKNOWN                        10
#define DCBX_STATUS_CFG_NOT_COMP_BUT_SUPP          11

#define DCBX_RED_ETS_DCBX_STATUS_INFO              1
#define DCBX_RED_PFC_DCBX_STATUS_INFO              2
#define DCBX_RED_APP_PRI_DCBX_STATUS_INFO          3

#define SET_DCBX_STATUS(pPortInfo, u1DcbxSemType, u1ApplType, i4Status) \
{\
    if (u1DcbxSemType == DCBX_SYM_STATE_MACHINE)\
    {\
       DcbxRedSendDynamicDcbxStatusInfo (u1ApplType, pPortInfo->u4IfIndex, i4Status); \
    }\
    pPortInfo->u1DcbxStatus = i4Status; \
}

/* Macros used by RB Tree Compare Function */
#define DCBX_EQUAL             0
#define DCBX_GREATER           1
#define DCBX_LESSER           -1

/* Macros used by SNMP Tables */
#define DCBX_DEST_ADDR_INDEX   1
#define DCBX_REM_MAX_INDEX     2147463647

/* TLV Related Macros */
#define DCBX_TLV_LENGTH_IN_BITS 9

#define DCBX_MAX_OUI_LEN        3
#define DCBX_MAX_APP_TLV        4

#define DCBX_TLV_LEN            144  /* TLV Length. This should be 
                                        always multiples of 4 */

/* State Machine Related Macros */
#define DCBX_OPER_INIT          1
#define DCBX_OPER_RECO          2

#define DCBX_ASYM_STATE_MACHINE 1
#define DCBX_SYM_STATE_MACHINE  2
#define DCBX_FEAT_STATE_MACHINE 3

/* WEB related Macros */
#define DCBX_MAX_MAC_NAME_LENGTH  20
#define DCBX_OCTET_STRING         5
#define DCBX_WEB_COUNT_THREE      3
#define DCBX_WEB_COUNT_ELEVEN     11

/*ETS MACROS*/
/*Maximum number of Traffic Class Group ID*/
#define ETS_MAX_NUM_TCGID         16

/* ETS Bandwidth Macros  */
#define ETS_DEFAULT_BW1           12
#define ETS_DEFAULT_BW2           13
#define ETS_MAX_BW                100

/* ETS Port Mode Macros */
#define ETS_PORT_MODE_AUTO        0
#define ETS_PORT_MODE_ON          1
#define ETS_PORT_MODE_OFF         2

/* ETS Port State Macros */
#define ETS_OPER_OFF              0
#define ETS_OPER_INIT             DCBX_OPER_INIT
#define ETS_OPER_RECO             DCBX_OPER_RECO

/* ETS TLV Related Macros */
#define ETS_TLV_TYPE              127

#define ETS_CONF_TLV_SUB_TYPE     9
#define ETS_RECO_TLV_SUB_TYPE     10
#define ETS_TC_SUPP_TLV_SUB_TYPE  8 /* Changing the ETS TC Supported TLV SUB_TYPE as 8 as per IEEE standard */
#define ETS_CEE_TLV_SUB_TYPE      2 
#define ETS_TLV_ALL               255

#define ETS_CONF_TLV_REM_UPD      0x01
#define ETS_RECO_TLV_REM_UPD      0x02
#define ETS_TC_SUPP_TLV_REM_UPD   0x04
#define ETS_CEE_TLV_REM_UPD       0x08

#define ETS_PREFORMED_TLV_LEN     6

#define ETS_CONF_TLV_LEN          27
#define ETS_CONF_TLV_INFO_LEN     25

#define ETS_RECO_TLV_LEN          27
#define ETS_RECO_TLV_INFO_LEN     25

#define ETS_TC_SUPP_TLV_LEN        7
#define ETS_TC_SUPP_TLV_INFO_LEN  5

#define ETS_PRI_MAP_BIT_MASK      0x0000000F
#define ETS_PRI_MAPPING_BITS_LEN  4

#define ETS_WILLING_MASK          0x80
#define ETS_CBS_MASK              0x40
#define ETS_MAX_TCG_MASK          0x07
#define ETS_TC_SUPP_MASK          0x07

/* ETS Trap Related Macros */
#define ETS_MODULE_STATUS_TRAP    0x00000001
#define ETS_ADMIN_MODE_TRAP       0x00000002
#define ETS_PEER_STATUS_TRAP      0x00000004
#define ETS_SEM_TRAP              0x00000008
#define ETS_ALL_TRAP              (ETS_MODULE_STATUS_TRAP | \
                                   ETS_ADMIN_MODE_TRAP    | \
                                   ETS_PEER_STATUS_TRAP   | \
                                   ETS_SEM_TRAP)


/*PFC MACROS*/
/* PFC Constant Value Macros */
#define PFC_PORT_CFG              0x01
#define PFC_DEL_PROFILE           0x02
#define PFC_THR_CFG               0x04

#define PFC_MIN_THRES             1
#define PFC_MAX_THRES             65535 

#define PFC_INIT_VAL              0
#define PFC_INVALID_ID           -1
#define PFC_MIN_VALUE             1

/* PFC Port Mode Macros */
#define PFC_PORT_MODE_AUTO        0
#define PFC_PORT_MODE_ON          1
#define PFC_PORT_MODE_OFF         2

/* PFC Port State Macros */
#define PFC_OPER_OFF              0
#define PFC_OPER_INIT             DCBX_OPER_INIT
#define PFC_OPER_RECO             DCBX_OPER_RECO

/* PFC TLV Related Macros */
#define PFC_TLV_TYPE              127
#define PFC_TLV_SUBTYPE           11

#define PFC_TLV_LEN               8
#define PFC_TLV_INFO_LEN          6
#define PFC_PREFORMED_TLV_LEN     6

#define PFC_WILLING_MASK          0x80
#define PFC_MBC_MASK              0x40
#define PFC_CAP_MASK              0x0F

/* PFC Trap Related Macros */
#define PFC_MODULE_STATUS_TRAP    0x00000001
#define PFC_ADMIN_MODE_TRAP       0x00000002
#define PFC_PEER_STATUS_TRAP      0x00000004
#define PFC_SEM_TRAP              0x00000008
#define PFC_ALL_TRAP              (PFC_MODULE_STATUS_TRAP | \
                                   PFC_ADMIN_MODE_TRAP    | \
                                   PFC_PEER_STATUS_TRAP   | \
                                   PFC_SEM_TRAP)
/* Application Priority related macros */
#define APP_PRI_MAX_SELECTOR      DCBX_MAX_APP_PRI_SELECTORS 

#define APP_PRI_PRIORITY_POSITION 5
/* Application Priority Port Mode Macros */
#define APP_PRI_PORT_MODE_AUTO        0
#define APP_PRI_PORT_MODE_ON          1
#define APP_PRI_PORT_MODE_OFF         2
/* Application Priority TLV Related Macros */
#define APP_PRI_TLV_TYPE              127
#define APP_PRI_TLV_SUBTYPE           12
                                   /* 
                                   Refer: D.2.12: IEEE802.1Qaz/D2.5: 
                                   */

#define APP_PRI_TLV_TYPE_TLV_LEN_SIZE  2 
                                   /* 
                                   Refer: 38.5.5: IEEE802.1Qaz/D1.2: 
                                   * TLV Type = 7 bits
                                   * TLV information string length = 9 bits
                                   */

#define APP_PRI_TLV_SUBTYPE_LEN       1
#define APP_PRI_TLV_RESERVED_LEN      1

#define APP_PRI_TLV_INFO_STR_HDR_LEN   (DCBX_MAX_OUI_LEN + \
                                        APP_PRI_TLV_SUBTYPE_LEN + \
                                        APP_PRI_TLV_RESERVED_LEN)

#define APP_PRI_TABLE_ENTRY_LEN       3
/* Refer section 38.5.5 IEEE802.1Qaz/D1.2 */
#define APP_PRI_TLV_LEN              ((APP_PRI_TLV_TYPE_TLV_LEN_SIZE + \
                                       APP_PRI_TLV_INFO_STR_HDR_LEN + \
                                       (APP_PRI_TABLE_ENTRY_LEN * \
                                        DCBX_MAX_APP_PRI_ENTRIES_PER_PORT)))
#define APP_PRI_PREFORMED_TLV_LEN      6

#define APP_PRI_TLV_TYPE_MASK         (UINT2) 0xFE00 /* TLV Type = 7 Bits */
#define APP_PRI_TLV_LEN_MASK          (UINT2) 0x01FF /* TLV Length = 9 Bits */
#define APP_PRI_TLV_SUBTYPE_MASK      0x0C
#define APP_PRI_WILLING_MASK          0x80
#define APP_PRI_SELECTOR_MASK         0x07
#define APP_PRI_PRIORITY_MASK         0xE0
#define APP_PRI_MAX_PROTOCOL          DCBX_MAX_APP_PRI_ENTRIES_PER_PORT
#define APP_PRI_MAX_PROTOCOL_ID       65535
/* Application Priority Port State Macros */
enum CreateType
{
    APP_PRI_ADMIN,
    APP_PRI_LOCAL,
    APP_PRI_REMOTE
};
#define APP_PRI_OPER_OFF              0
#define APP_PRI_OPER_INIT             DCBX_OPER_INIT
#define APP_PRI_OPER_RECO             DCBX_OPER_RECO

/* AP Trap Related Macros */
#define APP_PRI_MODULE_STATUS_TRAP    0x00000001
#define APP_PRI_ADMIN_MODE_TRAP       0x00000002
#define APP_PRI_PEER_STATUS_TRAP      0x00000004
#define APP_PRI_SEM_TRAP              0x00000008
#define APP_PRI_ALL_TRAP              (APP_PRI_MODULE_STATUS_TRAP|\
                                      APP_PRI_ADMIN_MODE_TRAP    | \
                                      APP_PRI_PEER_STATUS_TRAP   | \
                                      APP_PRI_SEM_TRAP)
/*Macros for OIDs*/
#define PFC_CONFIG_TX_OID { 1, 0, 8802, 1, 1, 2, 1, 5, 32962, 3, 1, 1, 4 }
#define PFC_CONFIG_TABLE_OID  { 1, 0, 8802, 1, 1, 2, 1, 5, 32962, 3, 1, 4, 4 }
#define PFC_PROP_OBJECT_ID { 1, 3, 6, 1, 4, 1, 29601, 2, 22, 3, 3 }
#define PFC_SYS_CNTRL_OID { 1, 3, 6, 1, 4, 1, 29601, 2, 22, 3, 3, 1, 1 }
#define ETS_TC_SUP_TX_OID { 1, 0, 8802, 1, 1, 2, 1, 5, 32962, 3, 1, 1, 1 }
#define ETS_CONFIG_TX_OID { 1, 0, 8802, 1, 1, 2, 1, 5, 32962, 3, 1, 1, 2 }
#define ETS_RECO_TX_OID { 1, 0, 8802, 1, 1, 2, 1, 5, 32962, 3, 1, 1, 3 }
#define ETS_CONFIG_TABLE_OID { 1, 0, 8802, 1, 1, 2, 1, 5, 32962, 3, 1, 4, 2 }
#define ETS_RECO_TABLE_OID { 1, 0, 8802, 1, 1, 2, 1, 5, 32962, 3, 1, 4, 3 }
#define ETS_OBJECT_OID { 1, 3, 6, 1, 4, 1, 29601, 2, 22, 3, 2 }
#define ETS_SYS_CNTRL_OID { 1, 3, 6, 1, 4, 1, 29601, 2, 22, 3, 2, 1, 1 }
#define APP_PRI_CONFIG_TX_OID { 1,0, 8802, 1, 1, 2, 1, 5, 32962, 3, 1, 1, 5 }
#define APP_PRI_CONFIG_TABLE_OID { 1, 0, 8802, 1, 1, 2, 1, 5, 32962, 3, 1, 4, 5 }
#define APP_PRI_PROP_OBJECT_OID { 1, 3, 6, 1, 4, 1, 29601, 2, 22, 3, 4 }
#define APP_PRI_SYS_CNTRL_OID { 1, 3, 6, 1, 4, 1, 29601, 2, 22, 3, 4, 1, 1 }
/* Definitions */
/* To get Tc Supp Application Id */
#define ETS_GET_TC_SUPP_APPL_ID(DcbxAppId)\
{\
            DcbxAppId.u2TlvType = ETS_TLV_TYPE;\
            DcbxAppId.u1SubType = ETS_TC_SUPP_TLV_SUB_TYPE;\
            MEMCPY(DcbxAppId.au1OUI, gau1DcbxOUI ,DCBX_MAX_OUI_LEN);\
}  
/* To get Configuration Application Id */
#define ETS_GET_CONF_APPL_ID(DcbxAppId)\
{\
            DcbxAppId.u2TlvType = ETS_TLV_TYPE;\
            DcbxAppId.u1SubType = ETS_CONF_TLV_SUB_TYPE;\
            MEMCPY(DcbxAppId.au1OUI, gau1DcbxOUI ,DCBX_MAX_OUI_LEN);\
}  
/* To get Recommendation Application Id */
#define ETS_GET_RECO_APPL_ID(DcbxAppId)\
{\
            DcbxAppId.u2TlvType = ETS_TLV_TYPE;\
            DcbxAppId.u1SubType = ETS_RECO_TLV_SUB_TYPE;\
            MEMCPY(DcbxAppId.au1OUI, gau1DcbxOUI ,DCBX_MAX_OUI_LEN);\
}
/* Fill teh Priority Mapping in the TLV */
#define ETS_FILL_PRI_MAPPING(pu1PktBuf,au1EtsTCG) \
{ \
    UINT4 u4Temp = DCBX_ZERO;\
       UINT4 u4Coun = DCBX_ZERO;   \
       UINT4 u4ETSPriMapping = DCBX_ZERO;\
       u4ETSPriMapping = (UINT4) (au1EtsTCG[DCBX_ZERO]) ; \
       for(u4Coun = 1;u4Coun<DCBX_MAX_PRIORITIES;u4Coun++) \
    {\
        u4Temp = (UINT4) (au1EtsTCG[u4Coun]) ;    \
        u4ETSPriMapping = (u4ETSPriMapping << (UINT4) ETS_PRI_MAPPING_BITS_LEN ) | u4Temp;    \
        } \
    DCBX_PUT_4BYTE(pu1PktBuf,u4ETSPriMapping); \
} 
/*Get the Priority Mapping from the TLV */
#define ETS_FILL_PRI_MAPPING_FROM_TLV(u4PriMapping, au1ETSRemTCGID) \
{ \
    INT4 i4Coun = DCBX_ZERO; \
    for(i4Coun=(DCBX_MAX_PRIORITIES - DCBX_ONE);i4Coun>=DCBX_ZERO;i4Coun--) \
    { \
        au1ETSRemTCGID[i4Coun] = (UINT1) (u4PriMapping & ETS_PRI_MAP_BIT_MASK); \
        u4PriMapping = u4PriMapping >>ETS_PRI_MAPPING_BITS_LEN;   \
    }\
}
/* Fill the Bandwidth in the TLV */
#define ETS_FILL_TCG_BW(pu1PktBuf,au1EtsBW) \
{\
       UINT4 u4Coun = ETS_TCGID0; \
       for(u4Coun = ETS_TCGID0; u4Coun < ETS_MAX_TCGID_CONF; u4Coun++) \
    {\
        DCBX_PUT_1BYTE(pu1PktBuf, au1EtsBW[u4Coun]);   \
        }\
}
/* Fill the TSA Table in the TLV */
#define ETS_FILL_TSA_TABLE(pu1PktBuf,au1EtsTsaTable) \
{\
       UINT4 u4Coun = ETS_TCGID0; \
       for(u4Coun = ETS_TCGID0; u4Coun < ETS_MAX_TCGID_CONF; u4Coun++) \
    {\
        DCBX_PUT_1BYTE(pu1PktBuf, au1EtsTsaTable[u4Coun]);   \
        }\
}
/* Get the Bandwith from the TLV */
#define ETS_FILL_TCG_BW_FROM_TLV(pu1PktBuf,au1EtsBW) \
{\
       UINT4 u4Coun = ETS_TCGID0; \
       for(u4Coun = ETS_TCGID0; u4Coun < ETS_MAX_TCGID_CONF; u4Coun++) \
    {\
        DCBX_GET_1BYTE(pu1PktBuf, au1EtsBW[u4Coun]);   \
        }\
}
/* Get the Tsa Table from the TLV */
#define ETS_FILL_TCG_TSA_TABLE_FROM_TLV(pu1PktBuf,au1EtsTsaTable) \
{\
       UINT4 u4Coun = ETS_TCGID0; \
       for(u4Coun = ETS_TCGID0; u4Coun < ETS_MAX_TCGID_CONF; u4Coun++) \
    {\
        DCBX_GET_1BYTE(pu1PktBuf, au1EtsTsaTable[u4Coun]);   \
        }\
}
/* To Get the PFC Application TLV */
#define PFC_GET_APPL_ID(DcbxAppId)\
{\
            DcbxAppId.u2TlvType = PFC_TLV_TYPE;\
            DcbxAppId.u1SubType = PFC_TLV_SUBTYPE;\
            MEMCPY(DcbxAppId.au1OUI, gau1DcbxOUI ,DCBX_MAX_OUI_LEN);\
}  
/* To Get the Application Priority TLV's Application Id */
#define APP_PRI_GET_APPL_ID(DcbxAppId)\
{\
            DcbxAppId.u2TlvType = APP_PRI_TLV_TYPE;\
            DcbxAppId.u1SubType = APP_PRI_TLV_SUBTYPE;\
            MEMCPY(DcbxAppId.au1OUI, gau1DcbxOUI ,DCBX_MAX_OUI_LEN);\
}  
#define APP_PRI_LOC_TBL(pPortEntry, i4Selector) pPortEntry->AppPriLocPortInfo.pAppPriLocMappingTbl[i4Selector]
#define APP_PRI_ADM_TBL(pPortEntry, i4Selector) pPortEntry->AppPriAdmPortInfo.AppPriAdmMappingTbl[i4Selector]
#define APP_PRI_REM_TBL(pPortEntry, i4Selector) pPortEntry->AppPriRemPortInfo.AppPriRemMappingTbl[i4Selector]
/*DCBX Definition to put the bytes in the buffer */
#define DCBX_PUT_1BYTE(pu1PktBuf, u1Val)    \
        *pu1PktBuf = u1Val;                     \
    pu1PktBuf += DCBX_ONE;

#define DCBX_GET_1BYTE(pu1PktBuf, u1Val)    \
        u1Val = *pu1PktBuf;                     \
    pu1PktBuf += DCBX_ONE;

#define DCBX_PUT_2BYTE(pu1PktBuf, u2Val)    \
        u2Val = OSIX_HTONS(u2Val);       \
    MEMCPY (pu1PktBuf, &u2Val, DCBX_TWO);     \
    pu1PktBuf += DCBX_TWO;

#define DCBX_GET_2BYTE(pu1PktBuf, u2Val)    \
        MEMCPY (&u2Val,pu1PktBuf, DCBX_TWO);  \
    u2Val = OSIX_NTOHS(u2Val);       \
    pu1PktBuf += DCBX_TWO;

#define DCBX_PUT_4BYTE(pu1PktBuf, u4Val)    \
        u4Val = OSIX_HTONL(u4Val);       \
    MEMCPY (pu1PktBuf, &u4Val, DCBX_FOUR);  \
    pu1PktBuf += DCBX_FOUR;

#define DCBX_GET_4BYTE(pu1PktBuf, u4Val)    \
        MEMCPY (&u4Val,pu1PktBuf, DCBX_FOUR);  \
    u4Val = OSIX_NTOHL(u4Val);       \
    pu1PktBuf += DCBX_FOUR;

#define DCBX_PUT_OUI(pu1PktBuf, au1OUI)    \
        MEMCPY (pu1PktBuf, au1OUI, DCBX_MAX_OUI_LEN);     \
    pu1PktBuf += DCBX_MAX_OUI_LEN;

#define DCBX_PUT_HEADER_LENGTH(u2Type, u2Len, pu2Header) \
       *pu2Header = u2Type << (UINT2)DCBX_TLV_LENGTH_IN_BITS; \
       *pu2Header = *pu2Header | u2Len;

/* Macros for shutdown check used in low level routine */
#define ETS_SHUTDOWN_CHECK\
        if ( gETSGlobalInfo.u1ETSSystemCtrl == ETS_SHUTDOWN )\
        {\
            DCBX_TRC (MGMT_TRC, ETS_MODULE_SHUTDOW_ERROR_MSG);\
            return (SNMP_FAILURE);\
        }

#define PFC_SHUTDOWN_CHECK\
         if ( gPFCGlobalInfo.u1PFCSystemCtrl == PFC_SHUTDOWN )\
        {\
            DCBX_TRC (MGMT_TRC, PFC_MODULE_SHUTDOW_ERROR_MSG);\
            return (SNMP_FAILURE);\
        }

/* Priya: Check this macro */
#define APP_PRI_SHUTDOWN_CHECK\
         if ( gAppPriGlobalInfo.u1AppPriSystemCtrl == APP_PRI_SHUTDOWN )\
        {\
            DCBX_TRC (MGMT_TRC, APP_PRI_MODULE_SHUTDOW_ERROR_MSG);\
            return (SNMP_FAILURE);\
        }

#define ETS_SHUTDOWN_CHECK_TEST\
        if (gETSGlobalInfo.u1ETSSystemCtrl == ETS_SHUTDOWN)\
        {\
            DCBX_TRC (MGMT_TRC, ETS_MODULE_SHUTDOW_ERROR_MSG);\
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;\
            CLI_SET_ERR (CLI_ERR_ETS_MODULE_SHUTDOWN);\
            return (SNMP_FAILURE);\
        }

#define PFC_SHUTDOWN_CHECK_TEST\
        if (gPFCGlobalInfo.u1PFCSystemCtrl == PFC_SHUTDOWN)\
        {\
            DCBX_TRC (MGMT_TRC, PFC_MODULE_SHUTDOW_ERROR_MSG);\
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;\
            CLI_SET_ERR (CLI_ERR_PFC_MODULE_SHUTDOWN);\
            return (SNMP_FAILURE);\
        }

#define APP_PRI_SHUTDOWN_CHECK_TEST\
        if (gAppPriGlobalInfo.u1AppPriSystemCtrl == APP_PRI_SHUTDOWN)\
        {\
            DCBX_TRC (MGMT_TRC, APP_PRI_MODULE_SHUTDOW_ERROR_MSG);\
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;\
            CLI_SET_ERR (CLI_ERR_PFC_MODULE_SHUTDOWN);\
            return (SNMP_FAILURE);\
        }

/* Error Strings used for Traces in low level routines */
#define ETS_MODULE_SHUTDOW_ERROR_MSG\
             " ETS Module  Should Be Started Before Accessing It !.\r\n"

#define PFC_MODULE_SHUTDOW_ERROR_MSG\
             " PFC Module  Should Be Started Before Accessing It !.\r\n"

#define APP_PRI_MODULE_SHUTDOW_ERROR_MSG\
             " Application Priority Module Should Be Started Before Accessing It !.\r\n"

#define NIL_INFO\
             " No Related Information is there !.\r\n"

#define WRONG_VAL\
             " Erroraneous Data!.\r\n"

#define MISMATCH\
             " MISMATCH VALUE YET IT WILL PROCEED WITH WHAT THE STRUCTURE HAS!.\r\n"

#define INDEX_NOT_IN_RANGE\
             " Index out off Range!.\r\n"

#ifdef MBSM_WANTED
#define DCBX_MBSM_MAX_LC_SLOTS MBSM_MAX_LC_SLOTS
#else
#define DCBX_MBSM_MAX_LC_SLOTS 1
#endif


#define IS_OUI_DCBX_IEEE(au1OUI) \
    (MEMCMP (au1OUI, gau1DcbxOUI, DCBX_MAX_OUI_LEN))

#define DCBX_FCOE_PROTOCOL  0x8906
#define DCBX_FIP_PROTOCOL   0x8914

#define DCBX_IS_FCOE_OR_FIP_PROTOCOL(i4Protocol) \
    ((i4Protocol == (INT4) DCBX_FCOE_PROTOCOL) ? OSIX_TRUE : ((i4Protocol == (INT4)DCBX_FIP_PROTOCOL) ? OSIX_TRUE : OSIX_FALSE))

#define DCBX_IS_FCOE_PROTOCOL(i4Protocol) \
    ((i4Protocol == (INT4) DCBX_FCOE_PROTOCOL) ? OSIX_TRUE : OSIX_FALSE)

#define DCBX_IS_FIP_PROTOCOL(i4Protocol) \
    ((i4Protocol == (INT4) DCBX_FIP_PROTOCOL) ? OSIX_TRUE : OSIX_FALSE)
#endif

/***********  Redundancy related MACROs *************/
enum
{
    DCBX_RED_ETS_CONF_TX_COUNTER = 1,
    DCBX_RED_ETS_CONF_RX_COUNTER,
    DCBX_RED_ETS_CONF_ERR_COUNTER,
    DCBX_RED_ETS_RECO_TX_COUNTER,
    DCBX_RED_ETS_RECO_RX_COUNTER,
    DCBX_RED_ETS_RECO_ERR_COUNTER,
    DCBX_RED_ETS_TC_SUPP_TX_COUNTER,
    DCBX_RED_ETS_TC_SUPP_RX_COUNTER,
    DCBX_RED_ETS_TC_SUPP_ERR_COUNTER,
    DCBX_RED_PFC_TX_COUNTER,
    DCBX_RED_PFC_RX_COUNTER,
    DCBX_RED_PFC_ERR_COUNTER,
    DCBX_RED_APP_PRI_TX_COUNTER,
    DCBX_RED_APP_PRI_RX_COUNTER,
    DCBX_RED_APP_PRI_ERR_COUNTER,
    DCBX_RED_APP_PRI_PROTOCOL_COUNTER,
    DCBX_CEE_RED_CTRL_TX_COUNTER,
    DCBX_CEE_RED_CTRL_RX_COUNTER,
    DCBX_CEE_RED_CTRL_RX_ERR_COUNTER
};

#define DCBX_RED_NODE_STATUS() \
       gDcbxGlobalInfo.DcbxRedGlobalInfo.u1NodeStatus 

#define DCBX_RED_STBY_NODE_UP_COUNT() \
    gDcbxGlobalInfo.DcbxRedGlobalInfo.u1NumOfStandbyNodesUp

/* MACRO to allocate buffer for RM message */
#define DCBX_DB_MAX_BUF_SIZE                          1500

/* Macros to write in to RM buffer. */
#define DCBX_RM_PUT_1_BYTE(pMsg, pu4Offset, u1MesgType) \
do { \
    RM_DATA_ASSIGN_1_BYTE (pMsg, *(pu4Offset), u1MesgType); \
        *(pu4Offset) += 1;\
}while (0)

#define DCBX_RM_PUT_2_BYTE(pMsg, pu4Offset, u2MesgType) \
do { \
    RM_DATA_ASSIGN_2_BYTE (pMsg, *(pu4Offset), u2MesgType); \
        *(pu4Offset) += 2;\
}while (0)

#define DCBX_RM_PUT_4_BYTE(pMsg, pu4Offset, u4MesgType) \
do { \
    RM_DATA_ASSIGN_4_BYTE (pMsg, *(pu4Offset), u4MesgType); \
        *(pu4Offset) += 4;\
}while (0)

#define DCBX_RM_PUT_N_BYTE(pdest, psrc, pu4Offset, u4Size) \
do { \
    RM_COPY_TO_OFFSET(pdest, psrc, *(pu4Offset), u4Size); \
        *(pu4Offset) +=u4Size; \
}while (0)

#define DCBX_RM_GET_1_BYTE(pMsg, pu4Offset, u1MesgType) \
do { \
    RM_GET_DATA_1_BYTE (pMsg, *(pu4Offset), u1MesgType); \
        *(pu4Offset) += 1;\
}while (0)

#define DCBX_RM_GET_2_BYTE(pMsg, pu4Offset, u2MesgType) \
do { \
    RM_GET_DATA_2_BYTE (pMsg, *(pu4Offset), u2MesgType); \
        *(pu4Offset) += 2;\
}while (0)

#define DCBX_RM_GET_4_BYTE(pMsg, pu4Offset, u4MesgType) \
do { \
    RM_GET_DATA_4_BYTE (pMsg, *(pu4Offset), u4MesgType); \
        *(pu4Offset) += 4;\
}while (0)

#define DCBX_RM_GET_N_BYTE(psrc, pdest, pu4Offset, u4Size) \
do { \
    RM_GET_DATA_N_BYTE (psrc, pdest, *(pu4Offset), u4Size); \
        *(pu4Offset) += u4Size; \
}while (0)

#define DCBX_RED_BULK_REQUEST_MSG                      1
#define DCBX_RED_BULK_UPD_TAIL_MESSAGE                 2

#define DCBX_RED_TYPE_FIELD_SIZE                       1
#define DCBX_RED_LEN_FIELD_SIZE                        2
#define DCBX_RED_BULK_REQ_SIZE                         3
#define DCBX_RED_BULK_UPD_TAIL_MSG_SIZE                3

/* STBY Processing macros */
/* ETS Redundancy MSG TYPE Macros */
#define DCBX_RED_ETS_REM_INFO                          3
#define DCBX_RED_ETS_SEM_INFO                          4
#define DCBX_RED_ETS_COUNTER_INFO                      5
#define DCBX_RED_ETS_ALL_COUNTER_INFO                  6
/* PFC Redundancy Redundancy MSG TYPE Macros */
#define DCBX_RED_PFC_REM_INFO                          7
#define DCBX_RED_PFC_SEM_INFO                          8
#define DCBX_RED_PFC_COUNTER_INFO                      9
#define DCBX_RED_PFC_ALL_COUNTER_INFO                 10
/* Application Priority Redundancy MSG TYPE Macros */
#define DCBX_RED_APP_PRI_REM_INFO                     11
#define DCBX_RED_APP_PRI_SEM_INFO                     12
#define DCBX_RED_APP_PRI_COUNTER_INFO                 13
#define DCBX_RED_APP_PRI_ALL_COUNTER_INFO             14
/* CEE PROCESS RELATED MACROS */
#define DCBX_CEE_RED_DYN_SYNC_INFO                    15
#define DCBX_CEE_RED_PFC_REM_INFO                     16
#define DCBX_CEE_RED_ETS_REM_INFO                     17
#define DCBX_CEE_RED_APP_PRI_REM_INFO                 18
#define DCBX_CEE_RED_DCBX_PORT_INFO                   19
#define DCBX_CEE_RED_CTRL_INFO                        20
#define DCBX_CEE_RED_CTRL_COUNTER_INFO                21
#define DCBX_CEE_RED_ALL_FEAT_AGE_OUT_INFO            22
#define DCBX_RED_DCBX_STATUS_INFO                     23

/* ETS Macros */
#define DCBX_RED_ETS_REM_INFO_VALUE_SIZE              (12 + (1 * ETS_MAX_NUM_TCGID) + \
                                                       (1 * DCBX_MAX_PRIORITIES) +    \
                                                       (1 * ETS_MAX_NUM_TCGID) +      \
                                                       (1 * MAC_ADDR_LEN) +           \
                                                       4)

#define DCBX_RED_ETS_SEM_INFO_VALUE_SIZE               5
#define DCBX_RED_ETS_COUNTER_INFO_VALUE_SIZE           9
#define DCBX_RED_ETS_ALL_COUNTER_INFO_VALUE_SIZE       40

#define DCBX_RED_ETS_REM_INFO_SIZE                    (DCBX_RED_TYPE_FIELD_SIZE + \
                                                       DCBX_RED_LEN_FIELD_SIZE + \
                                                       DCBX_RED_ETS_REM_INFO_VALUE_SIZE)
#define DCBX_RED_ETS_SEM_INFO_SIZE                    (DCBX_RED_TYPE_FIELD_SIZE + \
                                                       DCBX_RED_LEN_FIELD_SIZE + \
                                                       DCBX_RED_ETS_SEM_INFO_VALUE_SIZE)
#define DCBX_RED_ETS_COUNTER_INFO_SIZE                (DCBX_RED_TYPE_FIELD_SIZE + \
                                                       DCBX_RED_LEN_FIELD_SIZE + \
                                                       DCBX_RED_ETS_COUNTER_INFO_VALUE_SIZE)
#define DCBX_RED_ETS_ALL_COUNTER_INFO_SIZE            (DCBX_RED_TYPE_FIELD_SIZE + \
                                                       DCBX_RED_LEN_FIELD_SIZE + \
                                                       DCBX_RED_ETS_ALL_COUNTER_INFO_VALUE_SIZE)

/* PFC Macros */
#define DCBX_RED_PFC_REM_INFO_VALUE_SIZE              (12 + (1* MAC_ADDR_LEN) + 4)
#define DCBX_RED_PFC_SEM_INFO_VALUE_SIZE               5
#define DCBX_RED_PFC_COUNTER_INFO_VALUE_SIZE           9
#define DCBX_RED_PFC_ALL_COUNTER_INFO_VALUE_SIZE       16

#define DCBX_RED_PFC_REM_INFO_SIZE                    (DCBX_RED_TYPE_FIELD_SIZE + \
                                                       DCBX_RED_LEN_FIELD_SIZE + \
                                                       DCBX_RED_PFC_REM_INFO_VALUE_SIZE)
#define DCBX_RED_PFC_SEM_INFO_SIZE                    (DCBX_RED_TYPE_FIELD_SIZE + \
                                                       DCBX_RED_LEN_FIELD_SIZE + \
                                                       DCBX_RED_PFC_SEM_INFO_VALUE_SIZE)
#define DCBX_RED_PFC_COUNTER_INFO_SIZE                (DCBX_RED_TYPE_FIELD_SIZE + \
                                                       DCBX_RED_LEN_FIELD_SIZE + \
                                                       DCBX_RED_PFC_COUNTER_INFO_VALUE_SIZE)
#define DCBX_RED_PFC_ALL_COUNTER_INFO_SIZE            (DCBX_RED_TYPE_FIELD_SIZE + \
                                                       DCBX_RED_LEN_FIELD_SIZE + \
                                                       DCBX_RED_PFC_ALL_COUNTER_INFO_VALUE_SIZE)

/* Application Priority Macros */
#define DCBX_RED_APP_PRI_REM_INFO_VALUE_SIZE         23
#define DCBX_RED_APP_PRI_SEM_INFO_VALUE_SIZE         5
#define DCBX_RED_APP_PRI_COUNTER_INFO_VALUE_SIZE     9
#define DCBX_RED_APP_PRI_ALL_COUNTER_INFO_VALUE_SIZE 20
#define DCBX_RED_APP_PRI_REM_INFO_SIZE              (DCBX_RED_TYPE_FIELD_SIZE + \
                                                     DCBX_RED_LEN_FIELD_SIZE + \
                                                     DCBX_RED_APP_PRI_REM_INFO_VALUE_SIZE)
#define DCBX_RED_APP_PRI_SEM_INFO_SIZE              (DCBX_RED_TYPE_FIELD_SIZE + \
                                                     DCBX_RED_LEN_FIELD_SIZE + \
                                                     DCBX_RED_APP_PRI_SEM_INFO_VALUE_SIZE)
#define DCBX_RED_APP_PRI_COUNTER_INFO_SIZE          (DCBX_RED_TYPE_FIELD_SIZE + \
                                                     DCBX_RED_LEN_FIELD_SIZE + \
                                                     DCBX_RED_APP_PRI_COUNTER_INFO_VALUE_SIZE)
#define DCBX_RED_APP_PRI_ALL_COUNTER_INFO_SIZE      (DCBX_RED_TYPE_FIELD_SIZE + \
                                                     DCBX_RED_LEN_FIELD_SIZE + \
                                                     DCBX_RED_APP_PRI_ALL_COUNTER_INFO_VALUE_SIZE)


#define DCBX_CEE_RED_INTF_LEN                         4
#define DCBX_CEE_RED_CTRL_INFO_VALUE_SIZE             24 /* (IfIndex + SeqNo + RcvdSeqNo + Ack + RcvdAck + AckMissCnt) */
#define DCBX_CEE_RED_CTRL_ALL_COUNTER_INFO_VALUE_SIZE 12 /* u4TxCount + u4RxCount + u4RxErrCount */
#define DCBX_CEE_RED_CTRL_COUNTER_INFO_VALUE_SIZE     9  /* i4Interface + u1CountType + u4Counter */
#define DCBX_CEE_RED_DCBX_PORT_INFO_VALUE_SIZE        11 /* (IfIndex +
                                                            u1DcbxMode +
                                                            u1OperVersion +
                                                            u1MaxVersion +
                                                            u1PeerOperVersion +
                                                            u1PeerMaxVersion +
                                                            u1LldpTxStatus +
                                                            u1LldpRxStatus*/

#define DCBX_RED_DCBX_STATUS_INFO_SIZE               (DCBX_RED_TYPE_FIELD_SIZE + \
                                                      DCBX_RED_LEN_FIELD_SIZE + \
                                                      DCBX_RED_DCBX_STATUS_INFO_VALUE_SIZE)
#define DCBX_RED_DCBX_STATUS_INFO_VALUE_SIZE          6 /* addition of (u1AppType + u4IfIndex + u1DcbxStatus) */

/** MACROS used to sync CEE related info to STBY **/
/* i4Intf + bPeerWilling + u1Status + u1CurrentState + bSynd + bErr */
#define DCBX_CEE_RED_ETS_INFO_VALUE_SIZE              9
#define DCBX_CEE_RED_PFC_INFO_VALUE_SIZE              9 
#define DCBX_CEE_RED_APP_PRI_INFO_VALUE_SIZE          9

/* Total Length of the RM message used to post/proceess the DCBX Port Info */
#define DCBX_CEE_RED_DCBX_PORT_INFO_SIZE             (DCBX_RED_TYPE_FIELD_SIZE + \
                                                      DCBX_RED_LEN_FIELD_SIZE + \
                                                      DCBX_CEE_RED_DCBX_PORT_INFO_VALUE_SIZE)

/* Total Length of the RM message used to post/proceess the Ctrl Info */
#define DCBX_CEE_RED_CTRL_INFO_SIZE                  (DCBX_RED_TYPE_FIELD_SIZE + \
                                                      DCBX_RED_LEN_FIELD_SIZE + \
                                                      DCBX_CEE_RED_CTRL_INFO_VALUE_SIZE + \
                                                      DCBX_CEE_RED_CTRL_ALL_COUNTER_INFO_VALUE_SIZE)

/* Total Length of the RM message used to post/proceess the ETS Info */
#define DCBX_CEE_RED_ETS_INFO_SIZE                   (DCBX_RED_TYPE_FIELD_SIZE + \
                                                      DCBX_RED_LEN_FIELD_SIZE + \
                                                      DCBX_CEE_RED_ETS_INFO_VALUE_SIZE)

/* Total Length of the RM message used to post/proceess the PFC Info */
#define DCBX_CEE_RED_PFC_INFO_SIZE                   (DCBX_RED_TYPE_FIELD_SIZE + \
                                                      DCBX_RED_LEN_FIELD_SIZE + \
                                                      DCBX_CEE_RED_PFC_INFO_VALUE_SIZE)

/* Total Length of the RM message used to post/proceess the APPPri Info */
#define DCBX_CEE_RED_APP_PRI_INFO_SIZE               (DCBX_RED_TYPE_FIELD_SIZE + \
                                                      DCBX_RED_LEN_FIELD_SIZE + \
                                                      DCBX_CEE_RED_APP_PRI_INFO_VALUE_SIZE)
/* Dynamic Ctrl entry */
#define DCBX_CEE_RED_CTRL_COUNTER_INFO_SIZE          (DCBX_RED_TYPE_FIELD_SIZE + \
                                                      DCBX_RED_LEN_FIELD_SIZE + \
                                                      DCBX_CEE_RED_CTRL_COUNTER_INFO_VALUE_SIZE)

#define DCBX_CEE_RED_ALL_FEAT_AGEOUT_INFO_SIZE       (DCBX_RED_TYPE_FIELD_SIZE + \
                                                      DCBX_RED_LEN_FIELD_SIZE + \
                                                      DCBX_CEE_RED_INTF_LEN)


/**************  End of Redundancy Related MACROS *************/

