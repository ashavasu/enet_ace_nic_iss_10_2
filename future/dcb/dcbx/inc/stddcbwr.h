/********************************************************************
* Copyright (C) 2010 Aricent Inc . All Rights Reserved
*
* $Id: stddcbwr.h,v 1.2 2011/08/01 05:10:53 siva Exp $
*
* Description: Proto types for Mid Level  Routines
*********************************************************************/
#ifndef _STDDCBWR_H
#define _STDDCBWR_H
INT4 GetNextIndexLldpV2Xdot1ConfigPortVlanTable(tSnmpIndex *, tSnmpIndex *);

VOID RegisterSTDDCB(VOID);

VOID UnRegisterSTDDCB(VOID);
INT4 GetNextIndexLldpXdot1dcbxConfigETSConfigurationTable(tSnmpIndex *, tSnmpIndex *);
INT4 LldpXdot1dcbxConfigETSConfigurationTxEnableGet(tSnmpIndex *, tRetVal *);
INT4 LldpXdot1dcbxConfigETSConfigurationTxEnableSet(tSnmpIndex *, tRetVal *);
INT4 LldpXdot1dcbxConfigETSConfigurationTxEnableTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 LldpXdot1dcbxConfigETSConfigurationTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 GetNextIndexLldpXdot1dcbxConfigETSRecommendationTable(tSnmpIndex *, tSnmpIndex *);
INT4 LldpXdot1dcbxConfigETSRecommendationTxEnableGet(tSnmpIndex *, tRetVal *);
INT4 LldpXdot1dcbxConfigETSRecommendationTxEnableSet(tSnmpIndex *, tRetVal *);
INT4 LldpXdot1dcbxConfigETSRecommendationTxEnableTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 LldpXdot1dcbxConfigETSRecommendationTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 GetNextIndexLldpXdot1dcbxConfigPFCTable(tSnmpIndex *, tSnmpIndex *);
INT4 LldpXdot1dcbxConfigPFCTxEnableGet(tSnmpIndex *, tRetVal *);
INT4 LldpXdot1dcbxConfigPFCTxEnableSet(tSnmpIndex *, tRetVal *);
INT4 LldpXdot1dcbxConfigPFCTxEnableTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 LldpXdot1dcbxConfigPFCTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 GetNextIndexLldpXdot1dcbxConfigApplicationPriorityTable(tSnmpIndex *, tSnmpIndex *);
INT4 LldpXdot1dcbxConfigApplicationPriorityTxEnableGet(tSnmpIndex *, tRetVal *);
INT4 LldpXdot1dcbxConfigApplicationPriorityTxEnableSet(tSnmpIndex *, tRetVal *);
INT4 LldpXdot1dcbxConfigApplicationPriorityTxEnableTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 LldpXdot1dcbxConfigApplicationPriorityTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 GetNextIndexLldpXdot1dcbxLocETSBasicConfigurationTable(tSnmpIndex *, tSnmpIndex *);
INT4 LldpXdot1dcbxLocETSConCreditBasedShaperSupportGet(tSnmpIndex *, tRetVal *);
INT4 LldpXdot1dcbxLocETSConTrafficClassesSupportedGet(tSnmpIndex *, tRetVal *);
INT4 LldpXdot1dcbxLocETSConWillingGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexLldpXdot1dcbxLocETSConPriorityAssignmentTable(tSnmpIndex *, tSnmpIndex *);
INT4 LldpXdot1dcbxLocETSConPriTrafficClassGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexLldpXdot1dcbxLocETSConTrafficClassBandwidthTable(tSnmpIndex *, tSnmpIndex *);
INT4 LldpXdot1dcbxLocETSConTrafficClassBandwidthGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexLldpXdot1dcbxLocETSConTrafficSelectionAlgorithmTable(tSnmpIndex *, tSnmpIndex *);
INT4 LldpXdot1dcbxLocETSConTrafficSelectionAlgorithmGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexLldpXdot1dcbxLocETSRecoTrafficClassBandwidthTable(tSnmpIndex *, tSnmpIndex *);
INT4 LldpXdot1dcbxLocETSRecoTrafficClassBandwidthGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexLldpXdot1dcbxLocETSRecoTrafficSelectionAlgorithmTable(tSnmpIndex *, tSnmpIndex *);
INT4 LldpXdot1dcbxLocETSRecoTrafficSelectionAlgorithmGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexLldpXdot1dcbxLocPFCBasicTable(tSnmpIndex *, tSnmpIndex *);
INT4 LldpXdot1dcbxLocPFCWillingGet(tSnmpIndex *, tRetVal *);
INT4 LldpXdot1dcbxLocPFCMBCGet(tSnmpIndex *, tRetVal *);
INT4 LldpXdot1dcbxLocPFCCapGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexLldpXdot1dcbxLocPFCEnableTable(tSnmpIndex *, tSnmpIndex *);
INT4 LldpXdot1dcbxLocPFCEnableEnabledGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexLldpXdot1dcbxLocApplicationPriorityAppTable(tSnmpIndex *, tSnmpIndex *);
INT4 LldpXdot1dcbxLocApplicationPriorityAEPriorityGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexLldpXdot1dcbxRemETSBasicConfigurationTable(tSnmpIndex *, tSnmpIndex *);
INT4 LldpXdot1dcbxRemETSConCreditBasedShaperSupportGet(tSnmpIndex *, tRetVal *);
INT4 LldpXdot1dcbxRemETSConTrafficClassesSupportedGet(tSnmpIndex *, tRetVal *);
INT4 LldpXdot1dcbxRemETSConWillingGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexLldpXdot1dcbxRemETSConPriorityAssignmentTable(tSnmpIndex *, tSnmpIndex *);
INT4 LldpXdot1dcbxRemETSConPriTrafficClassGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexLldpXdot1dcbxRemETSConTrafficClassBandwidthTable(tSnmpIndex *, tSnmpIndex *);
INT4 LldpXdot1dcbxRemETSConTrafficClassBandwidthGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexLldpXdot1dcbxRemETSConTrafficSelectionAlgorithmTable(tSnmpIndex *, tSnmpIndex *);
INT4 LldpXdot1dcbxRemETSConTrafficSelectionAlgorithmGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexLldpXdot1dcbxRemETSRecoTrafficClassBandwidthTable(tSnmpIndex *, tSnmpIndex *);
INT4 LldpXdot1dcbxRemETSRecoTrafficClassBandwidthGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexLldpXdot1dcbxRemETSRecoTrafficSelectionAlgorithmTable(tSnmpIndex *, tSnmpIndex *);
INT4 LldpXdot1dcbxRemETSRecoTrafficSelectionAlgorithmGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexLldpXdot1dcbxRemPFCBasicTable(tSnmpIndex *, tSnmpIndex *);
INT4 LldpXdot1dcbxRemPFCWillingGet(tSnmpIndex *, tRetVal *);
INT4 LldpXdot1dcbxRemPFCMBCGet(tSnmpIndex *, tRetVal *);
INT4 LldpXdot1dcbxRemPFCCapGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexLldpXdot1dcbxRemPFCEnableTable(tSnmpIndex *, tSnmpIndex *);
INT4 LldpXdot1dcbxRemPFCEnableEnabledGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexLldpXdot1dcbxRemApplicationPriorityAppTable(tSnmpIndex *, tSnmpIndex *);
INT4 LldpXdot1dcbxRemApplicationPriorityAEPriorityGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexLldpXdot1dcbxAdminETSBasicConfigurationTable(tSnmpIndex *, tSnmpIndex *);
INT4 LldpXdot1dcbxAdminETSConCreditBasedShaperSupportGet(tSnmpIndex *, tRetVal *);
INT4 LldpXdot1dcbxAdminETSConTrafficClassesSupportedGet(tSnmpIndex *, tRetVal *);
INT4 LldpXdot1dcbxAdminETSConWillingGet(tSnmpIndex *, tRetVal *);
INT4 LldpXdot1dcbxAdminETSConWillingSet(tSnmpIndex *, tRetVal *);
INT4 LldpXdot1dcbxAdminETSConWillingTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 LldpXdot1dcbxAdminETSBasicConfigurationTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 GetNextIndexLldpXdot1dcbxAdminETSConPriorityAssignmentTable(tSnmpIndex *, tSnmpIndex *);
INT4 LldpXdot1dcbxAdminETSConPriTrafficClassGet(tSnmpIndex *, tRetVal *);
INT4 LldpXdot1dcbxAdminETSConPriTrafficClassSet(tSnmpIndex *, tRetVal *);
INT4 LldpXdot1dcbxAdminETSConPriTrafficClassTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 LldpXdot1dcbxAdminETSConPriorityAssignmentTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 GetNextIndexLldpXdot1dcbxAdminETSConTrafficClassBandwidthTable(tSnmpIndex *, tSnmpIndex *);
INT4 LldpXdot1dcbxAdminETSConTrafficClassBandwidthGet(tSnmpIndex *, tRetVal *);
INT4 LldpXdot1dcbxAdminETSConTrafficClassBandwidthSet(tSnmpIndex *, tRetVal *);
INT4 LldpXdot1dcbxAdminETSConTrafficClassBandwidthTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 LldpXdot1dcbxAdminETSConTrafficClassBandwidthTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 GetNextIndexLldpXdot1dcbxAdminETSConTrafficSelectionAlgorithmTable(tSnmpIndex *, tSnmpIndex *);
INT4 LldpXdot1dcbxAdminETSConTrafficSelectionAlgorithmGet(tSnmpIndex *, tRetVal *);
INT4 LldpXdot1dcbxAdminETSConTrafficSelectionAlgorithmSet(tSnmpIndex *, tRetVal *);
INT4 LldpXdot1dcbxAdminETSConTrafficSelectionAlgorithmTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 LldpXdot1dcbxAdminETSConTrafficSelectionAlgorithmTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 GetNextIndexLldpXdot1dcbxAdminETSRecoTrafficClassBandwidthTable(tSnmpIndex *, tSnmpIndex *);
INT4 LldpXdot1dcbxAdminETSRecoTrafficClassBandwidthGet(tSnmpIndex *, tRetVal *);
INT4 LldpXdot1dcbxAdminETSRecoTrafficClassBandwidthSet(tSnmpIndex *, tRetVal *);
INT4 LldpXdot1dcbxAdminETSRecoTrafficClassBandwidthTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 LldpXdot1dcbxAdminETSRecoTrafficClassBandwidthTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 GetNextIndexLldpXdot1dcbxAdminETSRecoTrafficSelectionAlgorithmTable(tSnmpIndex *, tSnmpIndex *);
INT4 LldpXdot1dcbxAdminETSRecoTrafficSelectionAlgorithmGet(tSnmpIndex *, tRetVal *);
INT4 LldpXdot1dcbxAdminETSRecoTrafficSelectionAlgorithmSet(tSnmpIndex *, tRetVal *);
INT4 LldpXdot1dcbxAdminETSRecoTrafficSelectionAlgorithmTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 LldpXdot1dcbxAdminETSRecoTrafficSelectionAlgorithmTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 GetNextIndexLldpXdot1dcbxAdminPFCBasicTable(tSnmpIndex *, tSnmpIndex *);
INT4 LldpXdot1dcbxAdminPFCWillingGet(tSnmpIndex *, tRetVal *);
INT4 LldpXdot1dcbxAdminPFCMBCGet(tSnmpIndex *, tRetVal *);
INT4 LldpXdot1dcbxAdminPFCCapGet(tSnmpIndex *, tRetVal *);
INT4 LldpXdot1dcbxAdminPFCWillingSet(tSnmpIndex *, tRetVal *);
INT4 LldpXdot1dcbxAdminPFCWillingTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 LldpXdot1dcbxAdminPFCBasicTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 GetNextIndexLldpXdot1dcbxAdminPFCEnableTable(tSnmpIndex *, tSnmpIndex *);
INT4 LldpXdot1dcbxAdminPFCEnableEnabledGet(tSnmpIndex *, tRetVal *);
INT4 LldpXdot1dcbxAdminPFCEnableEnabledSet(tSnmpIndex *, tRetVal *);
INT4 LldpXdot1dcbxAdminPFCEnableEnabledTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 LldpXdot1dcbxAdminPFCEnableTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 GetNextIndexLldpXdot1dcbxAdminApplicationPriorityAppTable(tSnmpIndex *, tSnmpIndex *);
INT4 LldpXdot1dcbxAdminApplicationPriorityAEPriorityGet(tSnmpIndex *, tRetVal *);
INT4 LldpXdot1dcbxAdminApplicationPriorityAEPrioritySet(tSnmpIndex *, tRetVal *);
INT4 LldpXdot1dcbxAdminApplicationPriorityAEPriorityTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 LldpXdot1dcbxAdminApplicationPriorityAppTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
#endif /* _STDDCBWR_H */
