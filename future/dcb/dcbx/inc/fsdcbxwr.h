/********************************************************************
* Copyright (C) 2010 Aricent Inc . All Rights Reserved
*
* $Id: fsdcbxwr.h,v 1.7 2016/05/25 10:06:09 siva Exp $
*
* Description: Proto types for Mid Level  Routines
*********************************************************************/
#ifndef _FSDCBXWR_H
#define _FSDCBXWR_H

VOID RegisterFSDCBX(VOID);

VOID UnRegisterFSDCBX(VOID);
INT4 FsDcbPfcMinThresholdGet(tSnmpIndex *, tRetVal *);
INT4 FsDcbPfcMaxThresholdGet(tSnmpIndex *, tRetVal *);
INT4 FsDcbMaxPfcProfilesGet(tSnmpIndex *, tRetVal *);
INT4 FsDcbPfcMinThresholdSet(tSnmpIndex *, tRetVal *);
INT4 FsDcbPfcMaxThresholdSet(tSnmpIndex *, tRetVal *);
INT4 FsDcbPfcMinThresholdTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsDcbPfcMaxThresholdTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsDcbPfcMinThresholdDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsDcbPfcMaxThresholdDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 GetNextIndexFsDcbPortTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsDcbETSAdminStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsDcbPFCAdminStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsDcbRowStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsDcbAppPriAdminStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsDcbOperVersionGet(tSnmpIndex *, tRetVal *);
INT4 FsDcbMaxVersionGet(tSnmpIndex *, tRetVal *);
INT4 FsDcbPeerOperVersionGet(tSnmpIndex *, tRetVal *);
INT4 FsDcbPeerMaxVersionGet(tSnmpIndex *, tRetVal *);
INT4 FsDcbRowStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsDcbRowStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsDcbPortTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 FsDcbxGlobalTraceLevelGet(tSnmpIndex *, tRetVal *);
INT4 FsDcbxGlobalTraceLevelSet(tSnmpIndex *, tRetVal *);
INT4 FsDcbxGlobalTraceLevelTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsDcbxGlobalTraceLevelDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 GetNextIndexFsDCBXPortTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsDCBXAdminStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsDCBXModeGet(tSnmpIndex *, tRetVal *);
INT4 FsDCBXAdminStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsDCBXModeSet(tSnmpIndex *, tRetVal *);
INT4 FsDCBXAdminStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsDCBXModeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsDCBXPortTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 FsETSSystemControlGet(tSnmpIndex *, tRetVal *);
INT4 FsETSModuleStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsETSClearCountersGet(tSnmpIndex *, tRetVal *);
INT4 FsETSGlobalEnableTrapGet(tSnmpIndex *, tRetVal *);
INT4 FsETSGeneratedTrapCountGet(tSnmpIndex *, tRetVal *);
INT4 FsETSSystemControlSet(tSnmpIndex *, tRetVal *);
INT4 FsETSModuleStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsETSClearCountersSet(tSnmpIndex *, tRetVal *);
INT4 FsETSGlobalEnableTrapSet(tSnmpIndex *, tRetVal *);
INT4 FsETSGeneratedTrapCountSet(tSnmpIndex *, tRetVal *);
INT4 FsETSSystemControlTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsETSModuleStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsETSClearCountersTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsETSGlobalEnableTrapTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsETSGeneratedTrapCountTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsETSSystemControlDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsETSModuleStatusDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsETSClearCountersDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsETSGlobalEnableTrapDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsETSGeneratedTrapCountDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 GetNextIndexFsETSPortTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsETSAdminModeGet(tSnmpIndex *, tRetVal *);
INT4 FsETSDcbxOperStateGet(tSnmpIndex *, tRetVal *);
INT4 FsETSDcbxStateMachineGet(tSnmpIndex *, tRetVal *);
INT4 FsETSClearTLVCountersGet(tSnmpIndex *, tRetVal *);
INT4 FsETSConfTxTLVCounterGet(tSnmpIndex *, tRetVal *);
INT4 FsETSConfRxTLVCounterGet(tSnmpIndex *, tRetVal *);
INT4 FsETSConfRxTLVErrorsGet(tSnmpIndex *, tRetVal *);
INT4 FsETSRecoTxTLVCounterGet(tSnmpIndex *, tRetVal *);
INT4 FsETSRecoRxTLVCounterGet(tSnmpIndex *, tRetVal *);
INT4 FsETSRecoRxTLVErrorsGet(tSnmpIndex *, tRetVal *);
INT4 FsETSTcSuppTxTLVCounterGet(tSnmpIndex *, tRetVal *);
INT4 FsETSTcSuppRxTLVCounterGet(tSnmpIndex *, tRetVal *);
INT4 FsETSTcSuppRxTLVErrorsGet(tSnmpIndex *, tRetVal *);
INT4 FsETSRowStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsETSSyncdGet(tSnmpIndex *, tRetVal *);
INT4 FsETSErrorGet(tSnmpIndex *, tRetVal *);
INT4 FsETSDcbxStatusGet(tSnmpIndex * , tRetVal * );
INT4 FsETSAdminModeSet(tSnmpIndex *, tRetVal *);
INT4 FsETSClearTLVCountersSet(tSnmpIndex *, tRetVal *);
INT4 FsETSRowStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsETSAdminModeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsETSClearTLVCountersTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsETSRowStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsETSPortTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 FsPFCSystemControlGet(tSnmpIndex *, tRetVal *);
INT4 FsPFCModuleStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsPFCClearCountersGet(tSnmpIndex *, tRetVal *);
INT4 FsPFCGlobalEnableTrapGet(tSnmpIndex *, tRetVal *);
INT4 FsPFCGeneratedTrapCountGet(tSnmpIndex *, tRetVal *);
INT4 FsPFCSystemControlSet(tSnmpIndex *, tRetVal *);
INT4 FsPFCModuleStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsPFCClearCountersSet(tSnmpIndex *, tRetVal *);
INT4 FsPFCGlobalEnableTrapSet(tSnmpIndex *, tRetVal *);
INT4 FsPFCGeneratedTrapCountSet(tSnmpIndex *, tRetVal *);
INT4 FsPFCSystemControlTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsPFCModuleStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsPFCClearCountersTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsPFCGlobalEnableTrapTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsPFCGeneratedTrapCountTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsPFCSystemControlDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsPFCModuleStatusDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsPFCClearCountersDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsPFCGlobalEnableTrapDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsPFCGeneratedTrapCountDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 GetNextIndexFsPFCPortTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsPFCAdminModeGet(tSnmpIndex *, tRetVal *);
INT4 FsPFCDcbxOperStateGet(tSnmpIndex *, tRetVal *);
INT4 FsPFCDcbxStateMachineGet(tSnmpIndex *, tRetVal *);
INT4 FsPFCClearTLVCountersGet(tSnmpIndex *, tRetVal *);
INT4 FsPFCTxTLVCounterGet(tSnmpIndex *, tRetVal *);
INT4 FsPFCRxTLVCounterGet(tSnmpIndex *, tRetVal *);
INT4 FsPFCRxTLVErrorsGet(tSnmpIndex *, tRetVal *);
INT4 FsPFCRowStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsPFCSyncdGet(tSnmpIndex *, tRetVal *);
INT4 FsPFCErrorGet(tSnmpIndex *, tRetVal *);
INT4 FsPFCDcbxStatusGet(tSnmpIndex * , tRetVal * );
INT4 FsPFCRxPauseFrameCounterGet(tSnmpIndex *, tRetVal *);
INT4 FsPFCTxPauseFrameCounterGet(tSnmpIndex *, tRetVal *);
INT4 FsPFCRxPauseFrameP0CounterGet(tSnmpIndex *, tRetVal *);
INT4 FsPFCRxPauseFrameP1CounterGet(tSnmpIndex *, tRetVal *);
INT4 FsPFCRxPauseFrameP2CounterGet(tSnmpIndex *, tRetVal *);
INT4 FsPFCRxPauseFrameP3CounterGet(tSnmpIndex *, tRetVal *);
INT4 FsPFCRxPauseFrameP4CounterGet(tSnmpIndex *, tRetVal *);
INT4 FsPFCRxPauseFrameP5CounterGet(tSnmpIndex *, tRetVal *);
INT4 FsPFCRxPauseFrameP6CounterGet(tSnmpIndex *, tRetVal *);
INT4 FsPFCRxPauseFrameP7CounterGet(tSnmpIndex *, tRetVal *);
INT4 FsPFCTxPauseFrameP0CounterGet(tSnmpIndex *, tRetVal *);
INT4 FsPFCTxPauseFrameP1CounterGet(tSnmpIndex *, tRetVal *);
INT4 FsPFCTxPauseFrameP2CounterGet(tSnmpIndex *, tRetVal *);
INT4 FsPFCTxPauseFrameP3CounterGet(tSnmpIndex *, tRetVal *);
INT4 FsPFCTxPauseFrameP4CounterGet(tSnmpIndex *, tRetVal *);
INT4 FsPFCTxPauseFrameP5CounterGet(tSnmpIndex *, tRetVal *);
INT4 FsPFCTxPauseFrameP6CounterGet(tSnmpIndex *, tRetVal *);
INT4 FsPFCTxPauseFrameP7CounterGet(tSnmpIndex *, tRetVal *);
INT4 FsPFCDataFrameDiscardCounterGet(tSnmpIndex *, tRetVal *);
INT4 FsPFCClearPauseFrameCountersGet(tSnmpIndex *, tRetVal *);
INT4 FsPFCAdminModeSet(tSnmpIndex *, tRetVal *);
INT4 FsPFCClearTLVCountersSet(tSnmpIndex *, tRetVal *);
INT4 FsPFCRowStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsPFCClearPauseFrameCountersSet(tSnmpIndex *, tRetVal *);
INT4 FsPFCAdminModeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsPFCClearTLVCountersTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsPFCRowStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsPFCClearPauseFrameCountersTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsPFCPortTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 FsAppPriSystemControlGet(tSnmpIndex *, tRetVal *);
INT4 FsAppPriModuleStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsAppPriClearCountersGet(tSnmpIndex *, tRetVal *);
INT4 FsAppPriGlobalEnableTrapGet(tSnmpIndex *, tRetVal *);
INT4 FsAppPriGeneratedTrapCountGet(tSnmpIndex *, tRetVal *);
INT4 FsAppPriSystemControlSet(tSnmpIndex *, tRetVal *);
INT4 FsAppPriModuleStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsAppPriClearCountersSet(tSnmpIndex *, tRetVal *);
INT4 FsAppPriGlobalEnableTrapSet(tSnmpIndex *, tRetVal *);
INT4 FsAppPriGeneratedTrapCountSet(tSnmpIndex *, tRetVal *);
INT4 FsAppPriSystemControlTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsAppPriModuleStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsAppPriClearCountersTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsAppPriGlobalEnableTrapTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsAppPriGeneratedTrapCountTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsAppPriSystemControlDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsAppPriModuleStatusDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsAppPriClearCountersDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsAppPriGlobalEnableTrapDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsAppPriGeneratedTrapCountDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 GetNextIndexFsAppPriPortTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsAppPriAdminModeGet(tSnmpIndex *, tRetVal *);
INT4 FsAppPriDcbxOperStateGet(tSnmpIndex *, tRetVal *);
INT4 FsAppPriDcbxStateMachineGet(tSnmpIndex *, tRetVal *);
INT4 FsAppPriClearTLVCountersGet(tSnmpIndex *, tRetVal *);
INT4 FsAppPriTxTLVCounterGet(tSnmpIndex *, tRetVal *);
INT4 FsAppPriRxTLVCounterGet(tSnmpIndex *, tRetVal *);
INT4 FsAppPriRxTLVErrorsGet(tSnmpIndex *, tRetVal *);
INT4 FsAppPriAppProtocolsGet(tSnmpIndex *, tRetVal *);
INT4 FsAppPriRowStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsAppPriSyncdGet(tSnmpIndex *, tRetVal *);
INT4 FsAppPriErrorGet(tSnmpIndex *, tRetVal *);
INT4 FsAppPriDcbxStatusGet(tSnmpIndex * , tRetVal * );
INT4 FsAppPriAdminModeSet(tSnmpIndex *, tRetVal *);
INT4 FsAppPriClearTLVCountersSet(tSnmpIndex *, tRetVal *);
INT4 FsAppPriRowStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsAppPriAdminModeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsAppPriClearTLVCountersTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsAppPriRowStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsAppPriPortTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 GetNextIndexFsAppPriXAppTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsAppPriXAppRowStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsAppPriXAppRowStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsAppPriXAppRowStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsAppPriXAppTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 GetNextIndexFslldpXdot1dcbxLocApplicationPriorityBasicTable(tSnmpIndex *, tSnmpIndex *);
INT4 FslldpXdot1dcbxLocApplicationPriorityWillingGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexFslldpXdot1dcbxAdminApplicationPriorityBasicTable(tSnmpIndex *, tSnmpIndex *);
INT4 FslldpXdot1dcbxAdminApplicationPriorityWillingGet(tSnmpIndex *, tRetVal *);
INT4 FslldpXdot1dcbxAdminApplicationPriorityWillingSet(tSnmpIndex *, tRetVal *);
INT4 FslldpXdot1dcbxAdminApplicationPriorityWillingTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FslldpXdot1dcbxAdminApplicationPriorityBasicTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 GetNextIndexFslldpXdot1dcbxRemApplicationPriorityBasicTable(tSnmpIndex *, tSnmpIndex *);
INT4 FslldpXdot1dcbxRemApplicationPriorityWillingGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexFslldpXdot1dcbxConfigTCSupportedTable(tSnmpIndex *, tSnmpIndex *);
INT4 FslldpXdot1dcbxConfigTCSupportedTxEnableGet(tSnmpIndex *, tRetVal *);
INT4 FslldpXdot1dcbxConfigTCSupportedTxEnableSet(tSnmpIndex *, tRetVal *);
INT4 FslldpXdot1dcbxConfigTCSupportedTxEnableTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FslldpXdot1dcbxConfigTCSupportedTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 GetNextIndexFslldpXdot1dcbxLocTCSupportedTable(tSnmpIndex *, tSnmpIndex *);
INT4 FslldpXdot1dcbxLocTCSupportedGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexFslldpXdot1dcbxRemTCSupportedTable(tSnmpIndex *, tSnmpIndex *);
INT4 FslldpXdot1dcbxRemTCSupportedGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexFslldpXdot1dcbxAdminTCSupportedTable(tSnmpIndex *, tSnmpIndex *);
INT4 FslldpXdot1dcbxAdminTCSupportedGet(tSnmpIndex *, tRetVal *);
INT4 FsDcbxCEEGlobalEnableTrapGet(tSnmpIndex *, tRetVal *);
INT4 FsDcbxCEEGeneratedTrapCountGet(tSnmpIndex *, tRetVal *);
INT4 FsDcbxCEEClearCountersGet(tSnmpIndex *, tRetVal *);
INT4 FsDcbxCEEGlobalEnableTrapSet(tSnmpIndex *, tRetVal *);
INT4 FsDcbxCEEClearCountersSet(tSnmpIndex *, tRetVal *);
INT4 FsDcbxCEEGlobalEnableTrapTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsDcbxCEEClearCountersTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsDcbxCEEGlobalEnableTrapDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsDcbxCEEClearCountersDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 GetNextIndexFsDcbxCEECtrlTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsDcbxCEECtrlSeqNoGet(tSnmpIndex *, tRetVal *);
INT4 FsDcbxCEECtrlAckNoGet(tSnmpIndex *, tRetVal *);
INT4 FsDcbxCEECtrlRcvdAckNoGet(tSnmpIndex *, tRetVal *);
INT4 FsDcbxCEECtrlTxTLVCounterGet(tSnmpIndex *, tRetVal *);
INT4 FsDcbxCEECtrlRxTLVCounterGet(tSnmpIndex *, tRetVal *);
INT4 FsDcbxCEECtrlRxTLVErrorCounterGet(tSnmpIndex *, tRetVal *);
#endif /* _FSDCBXWR_H */
