/*************************************************************************
 * Copyright (C) 2010 Aricent Inc . All Rights Reserved
 * $Id: etssz.h,v 1.1 2010/10/28 11:54:25 prabuc Exp $
 * Description: This header file contains all mempool prototypes 
 *              declaration for DCBX ETS Module.
****************************************************************************/
enum {
    MAX_DCBX_ETS_PORT_ENTRIES_SIZING_ID,
    ETS_MAX_SIZING_ID
};


#ifdef  _ETSSZ_C
tMemPoolId ETSMemPoolIds[ ETS_MAX_SIZING_ID];
INT4  EtsSizingMemCreateMemPools(VOID);
VOID  EtsSizingMemDeleteMemPools(VOID);
INT4  EtsSzRegisterModuleSizingParams( CHR1 *pu1ModName);
#else  /*  _ETSSZ_C  */
extern tMemPoolId ETSMemPoolIds[ ];
extern INT4  EtsSizingMemCreateMemPools(VOID);
extern VOID  EtsSizingMemDeleteMemPools(VOID);
extern INT4  EtsSzRegisterModuleSizingParams( CHR1 *pu1ModName); 
#endif /*  _ETSSZ_C  */


#ifdef  _ETSSZ_C
tFsModSizingParams FsETSSizingParams [] = {
{ "tETSPortEntry", "MAX_DCBX_ETS_PORT_ENTRIES", sizeof(tETSPortEntry),MAX_DCBX_ETS_PORT_ENTRIES, MAX_DCBX_ETS_PORT_ENTRIES,0 },
{"\0","\0",0,0,0,0}
};
#else  /*  _ETSSZ_C  */
extern tFsModSizingParams FsETSSizingParams [];
#endif /*  _ETSSZ_C  */


