/*************************************************************************
 * Copyright (C) 2010 Aricent Inc . All Rights Reserved
 * $Id: dcbxglob.h,v 1.5 2016/05/25 10:06:09 siva Exp $
 * Description: This header file contains all the declaration for
 *              global variables. 
****************************************************************************/
#ifndef __DCB_GLOB_H__
#define __DCB_GLOB_H__

#ifdef DCBXMAIN_C
UINT1 gau1CEEOUI[DCBX_MAX_OUI_LEN] = {0x00,0x1b,0x21}; /* CEE OUI 0x001b21 */
UINT1 gau1DcbxOUI[DCBX_MAX_OUI_LEN] = {0x00,0x80,0xc2};

UINT1 gau1PFCPreFormedTlv[PFC_PREFORMED_TLV_LEN];
UINT1 gau1ETSConfPreFormedTlv[ETS_PREFORMED_TLV_LEN];
UINT1 gau1ETSRecoPreFormedTlv[ETS_PREFORMED_TLV_LEN];
UINT1 gau1ETSTcSuppPreFormedTlv[ETS_PREFORMED_TLV_LEN];

tDcbxGlobalInfo   gDcbxGlobalInfo;
tETSGlobalInfo    gETSGlobalInfo;
tPFCGlobalInfo    gPFCGlobalInfo;
tCEEGlobalInfo    gCEEGlobalInfo;
tAppPriGlobalInfo gAppPriGlobalInfo;

const CHR1 *gau1CeeStateStr[CEE_MAX_STATES] = 
{
    "CEE_ST_WAIT_FOR_FEAT_TLV",    
    "CEE_ST_DELETE_AGED_INFO",
    "CEE_ST_USE_LOC_CFG",     
    "CEE_ST_USE_PEER_CFG",     
    "CEE_ST_CFG_NOT_CMP",
    "CEE_ST_RCVD_DUP_TLV",
    "CEE_ST_FEAT_INIT"
};

const CHR1 *gau1CeeEvntStr[CEE_MAX_EVENTS] = 
{
    "FEAT_TLV_RECEIVED",
    "NO_DCBX_TLV",
    "NO_FEAT_TLV",
    "DUP_TLV_RECEIVED",
    "FEAT_INIT",
    "INVALID_PKT_RCVD"
};
void (*gaCeeActionProc[CEE_MAX_SEM_FN_PTRS]) (UINT1 u1TlvType, tCeeTLVInfo *) =
{                                    
   /* A0 */  &CEESemEventIgnore,      
   /* A1 */  &CEESmStateRxFeatTlv,      
   /* A2 */  &CEESmStateDelAgedInfo,     
   /* A3 */  &CEESmStateNoFeatTlv,     
   /* A4 */  &CEESmStateRxDupTlv,     
   /* A5 */  &CEESmStateFeatInit,   
   /* A6 */  &CEESmStateWaitForFeatTlv,   
   /* A7 */  &CEESmStateInvalidPktRcvd

   /* NOTE: CEESmStateUseLocalCfg() , CEESmStateUsePeerCfg (), CEESmStateCfgNotCompatible ()
    * functions describes the following states CEE_USE_LOC_CFG,
    * CEE_USE_PEER_CFG and CEE_CFG_NOT_CMP but there is no explicit event to
    * move into this state. Its a direct function call from CEESmStateRxFeatTlv */
};

/* CEE Event Table */
const UINT1 gau1CeeSem[CEE_MAX_EVENTS][CEE_MAX_STATES] =
{
/*                                            STATES:
  _____________________________________________________________________________
  |                 |      Wait    |Delete | Use  | Use   |Cfg        |Dup    |
  |                 |      For     |AgedOut| Peer | Local |Not        |Feat   |
  |    EVENTS       |      FeatTlv |Info   | Cfg  | Cfg   |Compatible |Tlv    |
  |_________________|______________|_______|______|_______|___________|_______|
*/

/* FEAT_TLV_RECEIVED|*/   { A1,      A0,     A0,    A0,     A0,         A0 },
/* NO_DCBX_TLV      |*/   { A2,      A0,     A0,    A0,     A0,         A0 },
/* NO_FEAT_TLV      |*/   { A3,      A0,     A0,    A0,     A0,         A0 },
/* DUP_FEAT_TLV     |*/   { A4,      A0,     A0,    A0,     A0,         A0 },
/* FEAT_INIT        |*/   { A5,      A0,     A0,    A0,     A0,         A0 },
/* INVALID_VER_RECEIVED|*/{ A7,      A0,     A0,    A0,     A0,         A0 }
/* ----------------------------------------------------------------------------*/
};
#else
extern tDcbxGlobalInfo   gDcbxGlobalInfo;
extern tETSGlobalInfo    gETSGlobalInfo;
extern tPFCGlobalInfo    gPFCGlobalInfo;
extern tCEEGlobalInfo    gCEEGlobalInfo;
extern tAppPriGlobalInfo gAppPriGlobalInfo;

extern UINT1 gau1DcbxOUI[DCBX_MAX_OUI_LEN];
extern UINT1 gau1CEEOUI[DCBX_MAX_OUI_LEN];
extern UINT1 gau1ETSConfPreFormedTlv[ETS_PREFORMED_TLV_LEN];
extern UINT1 gau1ETSRecoPreFormedTlv[ETS_PREFORMED_TLV_LEN];
extern UINT1 gau1ETSTcSuppPreFormedTlv[ETS_PREFORMED_TLV_LEN];

extern UINT1 gau1PFCPreFormedTlv[PFC_PREFORMED_TLV_LEN];
extern CHR1 *gau1CeeEvntStr[CEE_MAX_EVENTS];
extern CHR1 *gau1CeeStateStr[CEE_MAX_STATES];
extern VOID (*gaCeeActionProc[CEE_MAX_SEM_FN_PTRS]) (UINT1 u1TlvType, tCeeTLVInfo *);
extern UINT1 gau1CeeSem[CEE_MAX_EVENTS][CEE_MAX_STATES];

#endif
#endif /* __DCB_GLOB_H__ */


