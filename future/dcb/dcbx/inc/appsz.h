/*************************************************************************
 * Copyright (C) 2010 Aricent Inc . All Rights Reserved
 * $Id: appsz.h,v 1.2 2011/06/30 06:53:02 siva Exp $
 * Description: This header file contains all mempool prototypes 
 *              declaration for DCBX Application Priority Module.
****************************************************************************/
enum {
    MAX_DCBX_APP_PRI_ENTRIES_SIZING_ID,
    MAX_DCBX_APP_PRI_PORTS_SIZING_ID,
    APP_MAX_SIZING_ID
};


#ifdef  _APPSZ_C
tMemPoolId APPMemPoolIds[ APP_MAX_SIZING_ID];
INT4  AppSizingMemCreateMemPools(VOID);
VOID  AppSizingMemDeleteMemPools(VOID);
INT4  AppSzRegisterModuleSizingParams( CHR1 *pu1ModName);
#else  /*  _APPSZ_C  */
extern tMemPoolId APPMemPoolIds[ ];
extern INT4  AppSizingMemCreateMemPools(VOID);
extern VOID  AppSizingMemDeleteMemPools(VOID);
extern INT4  AppSzRegisterModuleSizingParams( CHR1 *pu1ModName); 
#endif /*  _APPSZ_C  */


#ifdef  _APPSZ_C
tFsModSizingParams FsAPPSizingParams [] = {
{ "tAppPriMappingEntry", "MAX_DCBX_APP_PRI_ENTRIES", sizeof(tAppPriMappingEntry),MAX_DCBX_APP_PRI_ENTRIES, MAX_DCBX_APP_PRI_ENTRIES,0 },
{ "tAppPriPortEntry", "MAX_DCBX_APP_PRI_PORTS", sizeof(tAppPriPortEntry),MAX_DCBX_APP_PRI_PORTS, MAX_DCBX_APP_PRI_PORTS,0 },
{"\0","\0",0,0,0,0}
};
#else  /*  _APPSZ_C  */
extern tFsModSizingParams FsAPPSizingParams [];
#endif /*  _APPSZ_C  */


