/********************************************************************
* Copyright (C) 2010 Aricent Inc . All Rights Reserved
*
* $Id: fsdcbxdb.h,v 1.9 2016/07/09 09:41:20 siva Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/
#ifndef _FSDCBXDB_H
#define _FSDCBXDB_H

UINT1 FsDcbPortTableINDEX [] = {SNMP_DATA_TYPE_INTEGER};
UINT1 FsDCBXPortTableINDEX [] = {SNMP_DATA_TYPE_INTEGER};
UINT1 FsETSPortTableINDEX [] = {SNMP_DATA_TYPE_INTEGER};
UINT1 FsPFCPortTableINDEX [] = {SNMP_DATA_TYPE_INTEGER};
UINT1 FsAppPriPortTableINDEX [] = {SNMP_DATA_TYPE_INTEGER};
UINT1 FsAppPriXAppTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 FslldpXdot1dcbxLocApplicationPriorityBasicTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};
UINT1 FslldpXdot1dcbxAdminApplicationPriorityBasicTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};
UINT1 FslldpXdot1dcbxRemApplicationPriorityBasicTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_INTEGER32};
UINT1 FslldpXdot1dcbxConfigTCSupportedTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 FslldpXdot1dcbxLocTCSupportedTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};
UINT1 FslldpXdot1dcbxRemTCSupportedTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_INTEGER32};
UINT1 FslldpXdot1dcbxAdminTCSupportedTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};
UINT1 FsDcbxCEECtrlTableINDEX [] = {SNMP_DATA_TYPE_INTEGER};

UINT4 fsdcbx [] ={1,3,6,1,4,1,29601,2,22};
tSNMP_OID_TYPE fsdcbxOID = {9, fsdcbx};
/* Generated OID's for tables */
UINT4 FslldpXdot1dcbxLocApplicationPriorityBasicTable [] ={1,3,6,1,4,1,29601,2,22,3,4,4};
tSNMP_OID_TYPE FslldpXdot1dcbxLocApplicationPriorityBasicTableOID = {12, FslldpXdot1dcbxLocApplicationPriorityBasicTable};


UINT4 FslldpXdot1dcbxAdminApplicationPriorityBasicTable [] ={1,3,6,1,4,1,29601,2,22,3,4,5};
tSNMP_OID_TYPE FslldpXdot1dcbxAdminApplicationPriorityBasicTableOID = {12, FslldpXdot1dcbxAdminApplicationPriorityBasicTable};


UINT4 FslldpXdot1dcbxRemApplicationPriorityBasicTable [] ={1,3,6,1,4,1,29601,2,22,3,4,6};
tSNMP_OID_TYPE FslldpXdot1dcbxRemApplicationPriorityBasicTableOID = {12, FslldpXdot1dcbxRemApplicationPriorityBasicTable};


UINT4 FslldpXdot1dcbxConfigTCSupportedTable [] ={1,3,6,1,4,1,29601,2,22,3,5,1};
tSNMP_OID_TYPE FslldpXdot1dcbxConfigTCSupportedTableOID = {12, FslldpXdot1dcbxConfigTCSupportedTable};


UINT4 FslldpXdot1dcbxLocTCSupportedTable [] ={1,3,6,1,4,1,29601,2,22,3,5,2};
tSNMP_OID_TYPE FslldpXdot1dcbxLocTCSupportedTableOID = {12, FslldpXdot1dcbxLocTCSupportedTable};


UINT4 FslldpXdot1dcbxRemTCSupportedTable [] ={1,3,6,1,4,1,29601,2,22,3,5,3};
tSNMP_OID_TYPE FslldpXdot1dcbxRemTCSupportedTableOID = {12, FslldpXdot1dcbxRemTCSupportedTable};


UINT4 FslldpXdot1dcbxAdminTCSupportedTable [] ={1,3,6,1,4,1,29601,2,22,3,5,4};
tSNMP_OID_TYPE FslldpXdot1dcbxAdminTCSupportedTableOID = {12, FslldpXdot1dcbxAdminTCSupportedTable};




UINT4 FsDcbPfcMinThreshold [ ] ={1,3,6,1,4,1,29601,2,22,1,1};
UINT4 FsDcbPfcMaxThreshold [ ] ={1,3,6,1,4,1,29601,2,22,1,2};
UINT4 FsDcbMaxPfcProfiles [ ] ={1,3,6,1,4,1,29601,2,22,1,3};
UINT4 FsDcbPortNumber [ ] ={1,3,6,1,4,1,29601,2,22,2,1,1,1};
UINT4 FsDcbETSAdminStatus [ ] ={1,3,6,1,4,1,29601,2,22,2,1,1,2};
UINT4 FsDcbPFCAdminStatus [ ] ={1,3,6,1,4,1,29601,2,22,2,1,1,3};
UINT4 FsDcbRowStatus [ ] ={1,3,6,1,4,1,29601,2,22,2,1,1,4};
UINT4 FsDcbAppPriAdminStatus [ ] ={1,3,6,1,4,1,29601,2,22,2,1,1,5};
UINT4 FsDcbOperVersion [ ] ={1,3,6,1,4,1,29601,2,22,2,1,1,6};
UINT4 FsDcbMaxVersion [ ] ={1,3,6,1,4,1,29601,2,22,2,1,1,7};
UINT4 FsDcbPeerOperVersion [ ] ={1,3,6,1,4,1,29601,2,22,2,1,1,8};
UINT4 FsDcbPeerMaxVersion [ ] ={1,3,6,1,4,1,29601,2,22,2,1,1,9};
UINT4 FsDcbxGlobalTraceLevel [ ] ={1,3,6,1,4,1,29601,2,22,3,1,1,1};
UINT4 FsDCBXPortNumber [ ] ={1,3,6,1,4,1,29601,2,22,3,1,2,1,1};
UINT4 FsDCBXAdminStatus [ ] ={1,3,6,1,4,1,29601,2,22,3,1,2,1,2};
UINT4 FsDCBXMode [ ] ={1,3,6,1,4,1,29601,2,22,3,1,2,1,3};
UINT4 FsETSSystemControl [ ] ={1,3,6,1,4,1,29601,2,22,3,2,1,1};
UINT4 FsETSModuleStatus [ ] ={1,3,6,1,4,1,29601,2,22,3,2,1,2};
UINT4 FsETSClearCounters [ ] ={1,3,6,1,4,1,29601,2,22,3,2,1,3};
UINT4 FsETSGlobalEnableTrap [ ] ={1,3,6,1,4,1,29601,2,22,3,2,1,4};
UINT4 FsETSGeneratedTrapCount [ ] ={1,3,6,1,4,1,29601,2,22,3,2,1,5};
UINT4 FsETSPortNumber [ ] ={1,3,6,1,4,1,29601,2,22,3,2,2,1,1};
UINT4 FsETSAdminMode [ ] ={1,3,6,1,4,1,29601,2,22,3,2,2,1,2};
UINT4 FsETSDcbxOperState [ ] ={1,3,6,1,4,1,29601,2,22,3,2,2,1,3};
UINT4 FsETSDcbxStateMachine [ ] ={1,3,6,1,4,1,29601,2,22,3,2,2,1,4};
UINT4 FsETSClearTLVCounters [ ] ={1,3,6,1,4,1,29601,2,22,3,2,2,1,5};
UINT4 FsETSConfTxTLVCounter [ ] ={1,3,6,1,4,1,29601,2,22,3,2,2,1,6};
UINT4 FsETSConfRxTLVCounter [ ] ={1,3,6,1,4,1,29601,2,22,3,2,2,1,7};
UINT4 FsETSConfRxTLVErrors [ ] ={1,3,6,1,4,1,29601,2,22,3,2,2,1,8};
UINT4 FsETSRecoTxTLVCounter [ ] ={1,3,6,1,4,1,29601,2,22,3,2,2,1,9};
UINT4 FsETSRecoRxTLVCounter [ ] ={1,3,6,1,4,1,29601,2,22,3,2,2,1,10};
UINT4 FsETSRecoRxTLVErrors [ ] ={1,3,6,1,4,1,29601,2,22,3,2,2,1,11};
UINT4 FsETSTcSuppTxTLVCounter [ ] ={1,3,6,1,4,1,29601,2,22,3,2,2,1,12};
UINT4 FsETSTcSuppRxTLVCounter [ ] ={1,3,6,1,4,1,29601,2,22,3,2,2,1,13};
UINT4 FsETSTcSuppRxTLVErrors [ ] ={1,3,6,1,4,1,29601,2,22,3,2,2,1,14};
UINT4 FsETSRowStatus [ ] ={1,3,6,1,4,1,29601,2,22,3,2,2,1,15};
UINT4 FsETSSyncd [ ] ={1,3,6,1,4,1,29601,2,22,3,2,2,1,16};
UINT4 FsETSError [ ] ={1,3,6,1,4,1,29601,2,22,3,2,2,1,17};
UINT4 FsETSDcbxStatus [ ] ={1,3,6,1,4,1,29601,2,22,3,2,2,1,18};
UINT4 FsPFCSystemControl [ ] ={1,3,6,1,4,1,29601,2,22,3,3,1,1};
UINT4 FsPFCModuleStatus [ ] ={1,3,6,1,4,1,29601,2,22,3,3,1,2};
UINT4 FsPFCClearCounters [ ] ={1,3,6,1,4,1,29601,2,22,3,3,1,3};
UINT4 FsPFCGlobalEnableTrap [ ] ={1,3,6,1,4,1,29601,2,22,3,3,1,4};
UINT4 FsPFCGeneratedTrapCount [ ] ={1,3,6,1,4,1,29601,2,22,3,3,1,5};
UINT4 FsPFCPortNumber [ ] ={1,3,6,1,4,1,29601,2,22,3,3,2,1,1};
UINT4 FsPFCAdminMode [ ] ={1,3,6,1,4,1,29601,2,22,3,3,2,1,2};
UINT4 FsPFCDcbxOperState [ ] ={1,3,6,1,4,1,29601,2,22,3,3,2,1,3};
UINT4 FsPFCDcbxStateMachine [ ] ={1,3,6,1,4,1,29601,2,22,3,3,2,1,4};
UINT4 FsPFCClearTLVCounters [ ] ={1,3,6,1,4,1,29601,2,22,3,3,2,1,5};
UINT4 FsPFCTxTLVCounter [ ] ={1,3,6,1,4,1,29601,2,22,3,3,2,1,6};
UINT4 FsPFCRxTLVCounter [ ] ={1,3,6,1,4,1,29601,2,22,3,3,2,1,7};
UINT4 FsPFCRxTLVErrors [ ] ={1,3,6,1,4,1,29601,2,22,3,3,2,1,8};
UINT4 FsPFCRowStatus [ ] ={1,3,6,1,4,1,29601,2,22,3,3,2,1,9};
UINT4 FsPFCSyncd [ ] ={1,3,6,1,4,1,29601,2,22,3,3,2,1,10};
UINT4 FsPFCError [ ] ={1,3,6,1,4,1,29601,2,22,3,3,2,1,11};
UINT4 FsPFCDcbxStatus [ ] ={1,3,6,1,4,1,29601,2,22,3,3,2,1,12};
UINT4 FsPFCRxPauseFrameCounter [ ] ={1,3,6,1,4,1,29601,2,22,3,3,2,1,13};
UINT4 FsPFCTxPauseFrameCounter [ ] ={1,3,6,1,4,1,29601,2,22,3,3,2,1,14};
UINT4 FsPFCRxPauseFrameP0Counter [ ] ={1,3,6,1,4,1,29601,2,22,3,3,2,1,15};
UINT4 FsPFCRxPauseFrameP1Counter [ ] ={1,3,6,1,4,1,29601,2,22,3,3,2,1,16};
UINT4 FsPFCRxPauseFrameP2Counter [ ] ={1,3,6,1,4,1,29601,2,22,3,3,2,1,17};
UINT4 FsPFCRxPauseFrameP3Counter [ ] ={1,3,6,1,4,1,29601,2,22,3,3,2,1,18};
UINT4 FsPFCRxPauseFrameP4Counter [ ] ={1,3,6,1,4,1,29601,2,22,3,3,2,1,19};
UINT4 FsPFCRxPauseFrameP5Counter [ ] ={1,3,6,1,4,1,29601,2,22,3,3,2,1,20};
UINT4 FsPFCRxPauseFrameP6Counter [ ] ={1,3,6,1,4,1,29601,2,22,3,3,2,1,21};
UINT4 FsPFCRxPauseFrameP7Counter [ ] ={1,3,6,1,4,1,29601,2,22,3,3,2,1,22};
UINT4 FsPFCTxPauseFrameP0Counter [ ] ={1,3,6,1,4,1,29601,2,22,3,3,2,1,23};
UINT4 FsPFCTxPauseFrameP1Counter [ ] ={1,3,6,1,4,1,29601,2,22,3,3,2,1,24};
UINT4 FsPFCTxPauseFrameP2Counter [ ] ={1,3,6,1,4,1,29601,2,22,3,3,2,1,25};
UINT4 FsPFCTxPauseFrameP3Counter [ ] ={1,3,6,1,4,1,29601,2,22,3,3,2,1,26};
UINT4 FsPFCTxPauseFrameP4Counter [ ] ={1,3,6,1,4,1,29601,2,22,3,3,2,1,27};
UINT4 FsPFCTxPauseFrameP5Counter [ ] ={1,3,6,1,4,1,29601,2,22,3,3,2,1,28};
UINT4 FsPFCTxPauseFrameP6Counter [ ] ={1,3,6,1,4,1,29601,2,22,3,3,2,1,29};
UINT4 FsPFCTxPauseFrameP7Counter [ ] ={1,3,6,1,4,1,29601,2,22,3,3,2,1,30};
UINT4 FsPFCDataFrameDiscardCounter [ ] ={1,3,6,1,4,1,29601,2,22,3,3,2,1,31};
UINT4 FsPFCClearPauseFrameCounters [ ] ={1,3,6,1,4,1,29601,2,22,3,3,2,1,32};
UINT4 FsAppPriSystemControl [ ] ={1,3,6,1,4,1,29601,2,22,3,4,1,1};
UINT4 FsAppPriModuleStatus [ ] ={1,3,6,1,4,1,29601,2,22,3,4,1,2};
UINT4 FsAppPriClearCounters [ ] ={1,3,6,1,4,1,29601,2,22,3,4,1,3};
UINT4 FsAppPriGlobalEnableTrap [ ] ={1,3,6,1,4,1,29601,2,22,3,4,1,4};
UINT4 FsAppPriGeneratedTrapCount [ ] ={1,3,6,1,4,1,29601,2,22,3,4,1,5};
UINT4 FsAppPriPortNumber [ ] ={1,3,6,1,4,1,29601,2,22,3,4,2,1,1};
UINT4 FsAppPriAdminMode [ ] ={1,3,6,1,4,1,29601,2,22,3,4,2,1,2};
UINT4 FsAppPriDcbxOperState [ ] ={1,3,6,1,4,1,29601,2,22,3,4,2,1,3};
UINT4 FsAppPriDcbxStateMachine [ ] ={1,3,6,1,4,1,29601,2,22,3,4,2,1,4};
UINT4 FsAppPriClearTLVCounters [ ] ={1,3,6,1,4,1,29601,2,22,3,4,2,1,5};
UINT4 FsAppPriTxTLVCounter [ ] ={1,3,6,1,4,1,29601,2,22,3,4,2,1,6};
UINT4 FsAppPriRxTLVCounter [ ] ={1,3,6,1,4,1,29601,2,22,3,4,2,1,7};
UINT4 FsAppPriRxTLVErrors [ ] ={1,3,6,1,4,1,29601,2,22,3,4,2,1,8};
UINT4 FsAppPriAppProtocols [ ] ={1,3,6,1,4,1,29601,2,22,3,4,2,1,9};
UINT4 FsAppPriRowStatus [ ] ={1,3,6,1,4,1,29601,2,22,3,4,2,1,10};
UINT4 FsAppPriSyncd [ ] ={1,3,6,1,4,1,29601,2,22,3,4,2,1,11};
UINT4 FsAppPriError [ ] ={1,3,6,1,4,1,29601,2,22,3,4,2,1,12};
UINT4 FsAppPriDcbxStatus [ ] ={1,3,6,1,4,1,29601,2,22,3,4,2,1,13};
UINT4 FsAppPriXAppRowStatus [ ] ={1,3,6,1,4,1,29601,2,22,3,4,3,1,1};
UINT4 FslldpXdot1dcbxLocApplicationPriorityWilling [ ] ={1,3,6,1,4,1,29601,2,22,3,4,4,1,1};
UINT4 FslldpXdot1dcbxAdminApplicationPriorityWilling [ ] ={1,3,6,1,4,1,29601,2,22,3,4,5,1,1};
UINT4 FslldpXdot1dcbxRemApplicationPriorityWilling [ ] ={1,3,6,1,4,1,29601,2,22,3,4,6,1,1};
UINT4 FslldpXdot1dcbxConfigTCSupportedTxEnable [ ] ={1,3,6,1,4,1,29601,2,22,3,5,1,1,1};
UINT4 FslldpXdot1dcbxLocTCSupported [ ] ={1,3,6,1,4,1,29601,2,22,3,5,2,1,1};
UINT4 FslldpXdot1dcbxRemTCSupported [ ] ={1,3,6,1,4,1,29601,2,22,3,5,3,1,1};
UINT4 FslldpXdot1dcbxAdminTCSupported [ ] ={1,3,6,1,4,1,29601,2,22,3,5,4,1,1};
UINT4 FsDcbxCEEGlobalEnableTrap [ ] ={1,3,6,1,4,1,29601,2,22,3,6,1,1};
UINT4 FsDcbxCEEGeneratedTrapCount [ ] ={1,3,6,1,4,1,29601,2,22,3,6,1,2};
UINT4 FsDcbxCEEClearCounters [ ] ={1,3,6,1,4,1,29601,2,22,3,6,1,3};
UINT4 FsDcbxCEECtrlPortNumber [ ] ={1,3,6,1,4,1,29601,2,22,3,6,2,1,1};
UINT4 FsDcbxCEECtrlSeqNo [ ] ={1,3,6,1,4,1,29601,2,22,3,6,2,1,2};
UINT4 FsDcbxCEECtrlAckNo [ ] ={1,3,6,1,4,1,29601,2,22,3,6,2,1,3};
UINT4 FsDcbxCEECtrlRcvdAckNo [ ] ={1,3,6,1,4,1,29601,2,22,3,6,2,1,4};
UINT4 FsDcbxCEECtrlTxTLVCounter [ ] ={1,3,6,1,4,1,29601,2,22,3,6,2,1,5};
UINT4 FsDcbxCEECtrlRxTLVCounter [ ] ={1,3,6,1,4,1,29601,2,22,3,6,2,1,6};
UINT4 FsDcbxCEECtrlRxTLVErrorCounter [ ] ={1,3,6,1,4,1,29601,2,22,3,6,2,1,7};
UINT4 FsDcbTrapPortNumber [ ] ={1,3,6,1,4,1,29601,2,22,4,1,1};
UINT4 FsDcbPeerUpStatus [ ] ={1,3,6,1,4,1,29601,2,22,4,1,2};
UINT4 FsDcbFeatureType [ ] ={1,3,6,1,4,1,29601,2,22,4,1,3};




tMbDbEntry fsdcbxMibEntry[]= {

{{11,FsDcbPfcMinThreshold}, NULL, FsDcbPfcMinThresholdGet, FsDcbPfcMinThresholdSet, FsDcbPfcMinThresholdTest, FsDcbPfcMinThresholdDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{11,FsDcbPfcMaxThreshold}, NULL, FsDcbPfcMaxThresholdGet, FsDcbPfcMaxThresholdSet, FsDcbPfcMaxThresholdTest, FsDcbPfcMaxThresholdDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{11,FsDcbMaxPfcProfiles}, NULL, FsDcbMaxPfcProfilesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, NULL, 0, 0, 0, "256"},

{{13,FsDcbPortNumber}, GetNextIndexFsDcbPortTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsDcbPortTableINDEX, 1, 0, 0, NULL},

{{13,FsDcbETSAdminStatus}, GetNextIndexFsDcbPortTable, FsDcbETSAdminStatusGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsDcbPortTableINDEX, 1, 0, 0, "2"},

{{13,FsDcbPFCAdminStatus}, GetNextIndexFsDcbPortTable, FsDcbPFCAdminStatusGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsDcbPortTableINDEX, 1, 0, 0, "2"},

{{13,FsDcbRowStatus}, GetNextIndexFsDcbPortTable, FsDcbRowStatusGet, FsDcbRowStatusSet, FsDcbRowStatusTest, FsDcbPortTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsDcbPortTableINDEX, 1, 0, 1, NULL},

{{13,FsDcbAppPriAdminStatus}, GetNextIndexFsDcbPortTable, FsDcbAppPriAdminStatusGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsDcbPortTableINDEX, 1, 0, 0, "2"},

{{13,FsDcbOperVersion}, GetNextIndexFsDcbPortTable, FsDcbOperVersionGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsDcbPortTableINDEX, 1, 0, 0, NULL},

{{13,FsDcbMaxVersion}, GetNextIndexFsDcbPortTable, FsDcbMaxVersionGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsDcbPortTableINDEX, 1, 0, 0, NULL},

{{13,FsDcbPeerOperVersion}, GetNextIndexFsDcbPortTable, FsDcbPeerOperVersionGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsDcbPortTableINDEX, 1, 0, 0, NULL},

{{13,FsDcbPeerMaxVersion}, GetNextIndexFsDcbPortTable, FsDcbPeerMaxVersionGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsDcbPortTableINDEX, 1, 0, 0, NULL},

{{13,FsDcbxGlobalTraceLevel}, NULL, FsDcbxGlobalTraceLevelGet, FsDcbxGlobalTraceLevelSet, FsDcbxGlobalTraceLevelTest, FsDcbxGlobalTraceLevelDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "256"},

{{14,FsDCBXPortNumber}, GetNextIndexFsDCBXPortTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsDCBXPortTableINDEX, 1, 0, 0, NULL},

{{14,FsDCBXAdminStatus}, GetNextIndexFsDCBXPortTable, FsDCBXAdminStatusGet, FsDCBXAdminStatusSet, FsDCBXAdminStatusTest, FsDCBXPortTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsDCBXPortTableINDEX, 1, 0, 0, "1"},

{{14,FsDCBXMode}, GetNextIndexFsDCBXPortTable, FsDCBXModeGet, FsDCBXModeSet, FsDCBXModeTest, FsDCBXPortTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsDCBXPortTableINDEX, 1, 0, 0, "1"},

{{13,FsETSSystemControl}, NULL, FsETSSystemControlGet, FsETSSystemControlSet, FsETSSystemControlTest, FsETSSystemControlDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "1"},

{{13,FsETSModuleStatus}, NULL, FsETSModuleStatusGet, FsETSModuleStatusSet, FsETSModuleStatusTest, FsETSModuleStatusDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "1"},

{{13,FsETSClearCounters}, NULL, FsETSClearCountersGet, FsETSClearCountersSet, FsETSClearCountersTest, FsETSClearCountersDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "2"},

{{13,FsETSGlobalEnableTrap}, NULL, FsETSGlobalEnableTrapGet, FsETSGlobalEnableTrapSet, FsETSGlobalEnableTrapTest, FsETSGlobalEnableTrapDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "3"},

{{13,FsETSGeneratedTrapCount}, NULL, FsETSGeneratedTrapCountGet, FsETSGeneratedTrapCountSet, FsETSGeneratedTrapCountTest, FsETSGeneratedTrapCountDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{14,FsETSPortNumber}, GetNextIndexFsETSPortTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsETSPortTableINDEX, 1, 0, 0, NULL},

{{14,FsETSAdminMode}, GetNextIndexFsETSPortTable, FsETSAdminModeGet, FsETSAdminModeSet, FsETSAdminModeTest, FsETSPortTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsETSPortTableINDEX, 1, 0, 0, "2"},

{{14,FsETSDcbxOperState}, GetNextIndexFsETSPortTable, FsETSDcbxOperStateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsETSPortTableINDEX, 1, 0, 0, NULL},

{{14,FsETSDcbxStateMachine}, GetNextIndexFsETSPortTable, FsETSDcbxStateMachineGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsETSPortTableINDEX, 1, 0, 0, NULL},

{{14,FsETSClearTLVCounters}, GetNextIndexFsETSPortTable, FsETSClearTLVCountersGet, FsETSClearTLVCountersSet, FsETSClearTLVCountersTest, FsETSPortTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsETSPortTableINDEX, 1, 0, 0, "2"},

{{14,FsETSConfTxTLVCounter}, GetNextIndexFsETSPortTable, FsETSConfTxTLVCounterGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsETSPortTableINDEX, 1, 0, 0, NULL},

{{14,FsETSConfRxTLVCounter}, GetNextIndexFsETSPortTable, FsETSConfRxTLVCounterGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsETSPortTableINDEX, 1, 0, 0, NULL},

{{14,FsETSConfRxTLVErrors}, GetNextIndexFsETSPortTable, FsETSConfRxTLVErrorsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsETSPortTableINDEX, 1, 0, 0, NULL},

{{14,FsETSRecoTxTLVCounter}, GetNextIndexFsETSPortTable, FsETSRecoTxTLVCounterGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsETSPortTableINDEX, 1, 0, 0, NULL},

{{14,FsETSRecoRxTLVCounter}, GetNextIndexFsETSPortTable, FsETSRecoRxTLVCounterGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsETSPortTableINDEX, 1, 0, 0, NULL},

{{14,FsETSRecoRxTLVErrors}, GetNextIndexFsETSPortTable, FsETSRecoRxTLVErrorsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsETSPortTableINDEX, 1, 0, 0, NULL},

{{14,FsETSTcSuppTxTLVCounter}, GetNextIndexFsETSPortTable, FsETSTcSuppTxTLVCounterGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsETSPortTableINDEX, 1, 0, 0, NULL},

{{14,FsETSTcSuppRxTLVCounter}, GetNextIndexFsETSPortTable, FsETSTcSuppRxTLVCounterGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsETSPortTableINDEX, 1, 0, 0, NULL},

{{14,FsETSTcSuppRxTLVErrors}, GetNextIndexFsETSPortTable, FsETSTcSuppRxTLVErrorsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsETSPortTableINDEX, 1, 0, 0, NULL},

{{14,FsETSRowStatus}, GetNextIndexFsETSPortTable, FsETSRowStatusGet, FsETSRowStatusSet, FsETSRowStatusTest, FsETSPortTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsETSPortTableINDEX, 1, 0, 1, NULL},

{{14,FsETSSyncd}, GetNextIndexFsETSPortTable, FsETSSyncdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsETSPortTableINDEX, 1, 0, 0, NULL},

{{14,FsETSError}, GetNextIndexFsETSPortTable, FsETSErrorGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsETSPortTableINDEX, 1, 0, 0, NULL},

{{14,FsETSDcbxStatus}, GetNextIndexFsETSPortTable, FsETSDcbxStatusGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsETSPortTableINDEX, 1, 0, 0, NULL},

{{13,FsPFCSystemControl}, NULL, FsPFCSystemControlGet, FsPFCSystemControlSet, FsPFCSystemControlTest, FsPFCSystemControlDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "1"},

{{13,FsPFCModuleStatus}, NULL, FsPFCModuleStatusGet, FsPFCModuleStatusSet, FsPFCModuleStatusTest, FsPFCModuleStatusDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "1"},

{{13,FsPFCClearCounters}, NULL, FsPFCClearCountersGet, FsPFCClearCountersSet, FsPFCClearCountersTest, FsPFCClearCountersDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "2"},

{{13,FsPFCGlobalEnableTrap}, NULL, FsPFCGlobalEnableTrapGet, FsPFCGlobalEnableTrapSet, FsPFCGlobalEnableTrapTest, FsPFCGlobalEnableTrapDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "3"},

{{13,FsPFCGeneratedTrapCount}, NULL, FsPFCGeneratedTrapCountGet, FsPFCGeneratedTrapCountSet, FsPFCGeneratedTrapCountTest, FsPFCGeneratedTrapCountDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{14,FsPFCPortNumber}, GetNextIndexFsPFCPortTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsPFCPortTableINDEX, 1, 0, 0, NULL},

{{14,FsPFCAdminMode}, GetNextIndexFsPFCPortTable, FsPFCAdminModeGet, FsPFCAdminModeSet, FsPFCAdminModeTest, FsPFCPortTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsPFCPortTableINDEX, 1, 0, 0, "2"},

{{14,FsPFCDcbxOperState}, GetNextIndexFsPFCPortTable, FsPFCDcbxOperStateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsPFCPortTableINDEX, 1, 0, 0, NULL},

{{14,FsPFCDcbxStateMachine}, GetNextIndexFsPFCPortTable, FsPFCDcbxStateMachineGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsPFCPortTableINDEX, 1, 0, 0, NULL},

{{14,FsPFCClearTLVCounters}, GetNextIndexFsPFCPortTable, FsPFCClearTLVCountersGet, FsPFCClearTLVCountersSet, FsPFCClearTLVCountersTest, FsPFCPortTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsPFCPortTableINDEX, 1, 0, 0, "2"},

{{14,FsPFCTxTLVCounter}, GetNextIndexFsPFCPortTable, FsPFCTxTLVCounterGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsPFCPortTableINDEX, 1, 0, 0, NULL},

{{14,FsPFCRxTLVCounter}, GetNextIndexFsPFCPortTable, FsPFCRxTLVCounterGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsPFCPortTableINDEX, 1, 0, 0, NULL},

{{14,FsPFCRxTLVErrors}, GetNextIndexFsPFCPortTable, FsPFCRxTLVErrorsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsPFCPortTableINDEX, 1, 0, 0, NULL},

{{14,FsPFCRowStatus}, GetNextIndexFsPFCPortTable, FsPFCRowStatusGet, FsPFCRowStatusSet, FsPFCRowStatusTest, FsPFCPortTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsPFCPortTableINDEX, 1, 0, 1, NULL},

{{14,FsPFCSyncd}, GetNextIndexFsPFCPortTable, FsPFCSyncdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsPFCPortTableINDEX, 1, 0, 0, NULL},

{{14,FsPFCError}, GetNextIndexFsPFCPortTable, FsPFCErrorGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsPFCPortTableINDEX, 1, 0, 0, NULL},

{{14,FsPFCDcbxStatus}, GetNextIndexFsPFCPortTable, FsPFCDcbxStatusGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsPFCPortTableINDEX, 1, 0, 0, NULL},

{{14,FsPFCRxPauseFrameCounter}, GetNextIndexFsPFCPortTable, FsPFCRxPauseFrameCounterGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsPFCPortTableINDEX, 1, 0, 0, NULL},

{{14,FsPFCTxPauseFrameCounter}, GetNextIndexFsPFCPortTable, FsPFCTxPauseFrameCounterGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsPFCPortTableINDEX, 1, 0, 0, NULL},

{{14,FsPFCRxPauseFrameP0Counter}, GetNextIndexFsPFCPortTable, FsPFCRxPauseFrameP0CounterGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsPFCPortTableINDEX, 1, 0, 0, NULL},

{{14,FsPFCRxPauseFrameP1Counter}, GetNextIndexFsPFCPortTable, FsPFCRxPauseFrameP1CounterGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsPFCPortTableINDEX, 1, 0, 0, NULL},

{{14,FsPFCRxPauseFrameP2Counter}, GetNextIndexFsPFCPortTable, FsPFCRxPauseFrameP2CounterGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsPFCPortTableINDEX, 1, 0, 0, NULL},

{{14,FsPFCRxPauseFrameP3Counter}, GetNextIndexFsPFCPortTable, FsPFCRxPauseFrameP3CounterGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsPFCPortTableINDEX, 1, 0, 0, NULL},

{{14,FsPFCRxPauseFrameP4Counter}, GetNextIndexFsPFCPortTable, FsPFCRxPauseFrameP4CounterGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsPFCPortTableINDEX, 1, 0, 0, NULL},

{{14,FsPFCRxPauseFrameP5Counter}, GetNextIndexFsPFCPortTable, FsPFCRxPauseFrameP5CounterGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsPFCPortTableINDEX, 1, 0, 0, NULL},

{{14,FsPFCRxPauseFrameP6Counter}, GetNextIndexFsPFCPortTable, FsPFCRxPauseFrameP6CounterGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsPFCPortTableINDEX, 1, 0, 0, NULL},

{{14,FsPFCRxPauseFrameP7Counter}, GetNextIndexFsPFCPortTable, FsPFCRxPauseFrameP7CounterGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsPFCPortTableINDEX, 1, 0, 0, NULL},

{{14,FsPFCTxPauseFrameP0Counter}, GetNextIndexFsPFCPortTable, FsPFCTxPauseFrameP0CounterGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsPFCPortTableINDEX, 1, 0, 0, NULL},

{{14,FsPFCTxPauseFrameP1Counter}, GetNextIndexFsPFCPortTable, FsPFCTxPauseFrameP1CounterGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsPFCPortTableINDEX, 1, 0, 0, NULL},

{{14,FsPFCTxPauseFrameP2Counter}, GetNextIndexFsPFCPortTable, FsPFCTxPauseFrameP2CounterGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsPFCPortTableINDEX, 1, 0, 0, NULL},

{{14,FsPFCTxPauseFrameP3Counter}, GetNextIndexFsPFCPortTable, FsPFCTxPauseFrameP3CounterGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsPFCPortTableINDEX, 1, 0, 0, NULL},

{{14,FsPFCTxPauseFrameP4Counter}, GetNextIndexFsPFCPortTable, FsPFCTxPauseFrameP4CounterGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsPFCPortTableINDEX, 1, 0, 0, NULL},

{{14,FsPFCTxPauseFrameP5Counter}, GetNextIndexFsPFCPortTable, FsPFCTxPauseFrameP5CounterGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsPFCPortTableINDEX, 1, 0, 0, NULL},

{{14,FsPFCTxPauseFrameP6Counter}, GetNextIndexFsPFCPortTable, FsPFCTxPauseFrameP6CounterGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsPFCPortTableINDEX, 1, 0, 0, NULL},

{{14,FsPFCTxPauseFrameP7Counter}, GetNextIndexFsPFCPortTable, FsPFCTxPauseFrameP7CounterGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsPFCPortTableINDEX, 1, 0, 0, NULL},

{{14,FsPFCDataFrameDiscardCounter}, GetNextIndexFsPFCPortTable, FsPFCDataFrameDiscardCounterGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsPFCPortTableINDEX, 1, 0, 0, NULL},

{{14,FsPFCClearPauseFrameCounters}, GetNextIndexFsPFCPortTable, FsPFCClearPauseFrameCountersGet, FsPFCClearPauseFrameCountersSet, FsPFCClearPauseFrameCountersTest, FsPFCPortTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsPFCPortTableINDEX, 1, 0, 0, "2"},

{{13,FsAppPriSystemControl}, NULL, FsAppPriSystemControlGet, FsAppPriSystemControlSet, FsAppPriSystemControlTest, FsAppPriSystemControlDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "1"},

{{13,FsAppPriModuleStatus}, NULL, FsAppPriModuleStatusGet, FsAppPriModuleStatusSet, FsAppPriModuleStatusTest, FsAppPriModuleStatusDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "1"},

{{13,FsAppPriClearCounters}, NULL, FsAppPriClearCountersGet, FsAppPriClearCountersSet, FsAppPriClearCountersTest, FsAppPriClearCountersDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "2"},

{{13,FsAppPriGlobalEnableTrap}, NULL, FsAppPriGlobalEnableTrapGet, FsAppPriGlobalEnableTrapSet, FsAppPriGlobalEnableTrapTest, FsAppPriGlobalEnableTrapDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "3"},

{{13,FsAppPriGeneratedTrapCount}, NULL, FsAppPriGeneratedTrapCountGet, FsAppPriGeneratedTrapCountSet, FsAppPriGeneratedTrapCountTest, FsAppPriGeneratedTrapCountDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{14,FsAppPriPortNumber}, GetNextIndexFsAppPriPortTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsAppPriPortTableINDEX, 1, 0, 0, NULL},

{{14,FsAppPriAdminMode}, GetNextIndexFsAppPriPortTable, FsAppPriAdminModeGet, FsAppPriAdminModeSet, FsAppPriAdminModeTest, FsAppPriPortTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsAppPriPortTableINDEX, 1, 0, 0, "2"},

{{14,FsAppPriDcbxOperState}, GetNextIndexFsAppPriPortTable, FsAppPriDcbxOperStateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsAppPriPortTableINDEX, 1, 0, 0, NULL},

{{14,FsAppPriDcbxStateMachine}, GetNextIndexFsAppPriPortTable, FsAppPriDcbxStateMachineGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsAppPriPortTableINDEX, 1, 0, 0, NULL},

{{14,FsAppPriClearTLVCounters}, GetNextIndexFsAppPriPortTable, FsAppPriClearTLVCountersGet, FsAppPriClearTLVCountersSet, FsAppPriClearTLVCountersTest, FsAppPriPortTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsAppPriPortTableINDEX, 1, 0, 0, "2"},

{{14,FsAppPriTxTLVCounter}, GetNextIndexFsAppPriPortTable, FsAppPriTxTLVCounterGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsAppPriPortTableINDEX, 1, 0, 0, NULL},

{{14,FsAppPriRxTLVCounter}, GetNextIndexFsAppPriPortTable, FsAppPriRxTLVCounterGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsAppPriPortTableINDEX, 1, 0, 0, NULL},

{{14,FsAppPriRxTLVErrors}, GetNextIndexFsAppPriPortTable, FsAppPriRxTLVErrorsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsAppPriPortTableINDEX, 1, 0, 0, NULL},

{{14,FsAppPriAppProtocols}, GetNextIndexFsAppPriPortTable, FsAppPriAppProtocolsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsAppPriPortTableINDEX, 1, 0, 0, NULL},

{{14,FsAppPriRowStatus}, GetNextIndexFsAppPriPortTable, FsAppPriRowStatusGet, FsAppPriRowStatusSet, FsAppPriRowStatusTest, FsAppPriPortTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsAppPriPortTableINDEX, 1, 0, 1, NULL},

{{14,FsAppPriSyncd}, GetNextIndexFsAppPriPortTable, FsAppPriSyncdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsAppPriPortTableINDEX, 1, 0, 0, NULL},

{{14,FsAppPriError}, GetNextIndexFsAppPriPortTable, FsAppPriErrorGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsAppPriPortTableINDEX, 1, 0, 0, NULL},

{{14,FsAppPriDcbxStatus}, GetNextIndexFsAppPriPortTable, FsAppPriDcbxStatusGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsAppPriPortTableINDEX, 1, 0, 0, NULL},

{{14,FsAppPriXAppRowStatus}, GetNextIndexFsAppPriXAppTable, FsAppPriXAppRowStatusGet, FsAppPriXAppRowStatusSet, FsAppPriXAppRowStatusTest, FsAppPriXAppTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsAppPriXAppTableINDEX, 3, 0, 1, NULL},

{{14,FslldpXdot1dcbxLocApplicationPriorityWilling}, GetNextIndexFslldpXdot1dcbxLocApplicationPriorityBasicTable, FslldpXdot1dcbxLocApplicationPriorityWillingGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FslldpXdot1dcbxLocApplicationPriorityBasicTableINDEX, 1, 0, 0, NULL},

{{14,FslldpXdot1dcbxAdminApplicationPriorityWilling}, GetNextIndexFslldpXdot1dcbxAdminApplicationPriorityBasicTable, FslldpXdot1dcbxAdminApplicationPriorityWillingGet, FslldpXdot1dcbxAdminApplicationPriorityWillingSet, FslldpXdot1dcbxAdminApplicationPriorityWillingTest, FslldpXdot1dcbxAdminApplicationPriorityBasicTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FslldpXdot1dcbxAdminApplicationPriorityBasicTableINDEX, 1, 0, 0, "2"},

{{14,FslldpXdot1dcbxRemApplicationPriorityWilling}, GetNextIndexFslldpXdot1dcbxRemApplicationPriorityBasicTable, FslldpXdot1dcbxRemApplicationPriorityWillingGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FslldpXdot1dcbxRemApplicationPriorityBasicTableINDEX, 4, 0, 0, NULL},

{{14,FslldpXdot1dcbxConfigTCSupportedTxEnable}, GetNextIndexFslldpXdot1dcbxConfigTCSupportedTable, FslldpXdot1dcbxConfigTCSupportedTxEnableGet, FslldpXdot1dcbxConfigTCSupportedTxEnableSet, FslldpXdot1dcbxConfigTCSupportedTxEnableTest, FslldpXdot1dcbxConfigTCSupportedTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FslldpXdot1dcbxConfigTCSupportedTableINDEX, 2, 0, 0, "2"},

{{14,FslldpXdot1dcbxLocTCSupported}, GetNextIndexFslldpXdot1dcbxLocTCSupportedTable, FslldpXdot1dcbxLocTCSupportedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FslldpXdot1dcbxLocTCSupportedTableINDEX, 1, 0, 0, NULL},

{{14,FslldpXdot1dcbxRemTCSupported}, GetNextIndexFslldpXdot1dcbxRemTCSupportedTable, FslldpXdot1dcbxRemTCSupportedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FslldpXdot1dcbxRemTCSupportedTableINDEX, 4, 0, 0, NULL},

{{14,FslldpXdot1dcbxAdminTCSupported}, GetNextIndexFslldpXdot1dcbxAdminTCSupportedTable, FslldpXdot1dcbxAdminTCSupportedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FslldpXdot1dcbxAdminTCSupportedTableINDEX, 1, 0, 0, NULL},

{{13,FsDcbxCEEGlobalEnableTrap}, NULL, FsDcbxCEEGlobalEnableTrapGet, FsDcbxCEEGlobalEnableTrapSet, FsDcbxCEEGlobalEnableTrapTest, FsDcbxCEEGlobalEnableTrapDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "0"},

{{13,FsDcbxCEEGeneratedTrapCount}, NULL, FsDcbxCEEGeneratedTrapCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{13,FsDcbxCEEClearCounters}, NULL, FsDcbxCEEClearCountersGet, FsDcbxCEEClearCountersSet, FsDcbxCEEClearCountersTest, FsDcbxCEEClearCountersDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "2"},

{{14,FsDcbxCEECtrlPortNumber}, GetNextIndexFsDcbxCEECtrlTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsDcbxCEECtrlTableINDEX, 1, 0, 0, NULL},

{{14,FsDcbxCEECtrlSeqNo}, GetNextIndexFsDcbxCEECtrlTable, FsDcbxCEECtrlSeqNoGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsDcbxCEECtrlTableINDEX, 1, 0, 0, NULL},

{{14,FsDcbxCEECtrlAckNo}, GetNextIndexFsDcbxCEECtrlTable, FsDcbxCEECtrlAckNoGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsDcbxCEECtrlTableINDEX, 1, 0, 0, NULL},

{{14,FsDcbxCEECtrlRcvdAckNo}, GetNextIndexFsDcbxCEECtrlTable, FsDcbxCEECtrlRcvdAckNoGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsDcbxCEECtrlTableINDEX, 1, 0, 0, NULL},

{{14,FsDcbxCEECtrlTxTLVCounter}, GetNextIndexFsDcbxCEECtrlTable, FsDcbxCEECtrlTxTLVCounterGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsDcbxCEECtrlTableINDEX, 1, 0, 0, NULL},

{{14,FsDcbxCEECtrlRxTLVCounter}, GetNextIndexFsDcbxCEECtrlTable, FsDcbxCEECtrlRxTLVCounterGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsDcbxCEECtrlTableINDEX, 1, 0, 0, NULL},

{{14,FsDcbxCEECtrlRxTLVErrorCounter}, GetNextIndexFsDcbxCEECtrlTable, FsDcbxCEECtrlRxTLVErrorCounterGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsDcbxCEECtrlTableINDEX, 1, 0, 0, NULL},

{{12,FsDcbTrapPortNumber}, NULL, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_ACCESSIBLEFORNOTIFY, NULL, 0, 0, 0, NULL},

{{12,FsDcbPeerUpStatus}, NULL, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_ACCESSIBLEFORNOTIFY, NULL, 0, 0, 0, NULL},

{{12,FsDcbFeatureType}, NULL, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_ACCESSIBLEFORNOTIFY, NULL, 0, 0, 0, NULL},
};
tMibData fsdcbxEntry = { 115, fsdcbxMibEntry };

#endif /* _FSDCBXDB_H */

