/*************************************************************************
 * Copyright (C) 2010 Aricent Inc . All Rights Reserved
 * $Id: dcbxtrc.h,v 1.13 2017/08/24 11:59:42 siva Exp $
 * Description: This header file contains all the trace related
 *              macros and definition for DCBX Module.
****************************************************************************/
#include "dcbxglob.h"
#ifndef __DCBM_TRC_H__
#define __DCBM_TRC_H__

/* Trace and debug flags */
#define   DCBX_TRC_FLAG            (gDcbxGlobalInfo.u4DCBXTrcFlag)

#ifdef TRACE_WANTED
#define DCBX_MGMT_TRC             0x00000001
#define DCBX_SEM_TRC              0x00000002
#define DCBX_TLV_TRC              0x00000004
#define DCBX_RESOURCE_TRC         0x00000008
#define DCBX_FAILURE_TRC          0x00000010
#define DCBX_RED_TRC              0x00000020
#define DCBX_MBSM_TRC             0x00000040
#define DCBX_CONTROL_PLANE_TRC    0x00000080
#define DCBX_CRITICAL_TRC         0x00000100
#define DCBX_ALL_FLAG 511

#define DCBX_ALL_TRC        (DCBX_MGMT_TRC | \
                             DCBX_SEM_TRC | \
                             DCBX_TLV_TRC | \
                             DCBX_RESOURCE_TRC | \
                             DCBX_FAILURE_TRC | \
                             DCBX_RED_TRC | \
                             DCBX_MBSM_TRC | \
                             DCBX_CONTROL_PLANE_TRC | \
                             DCBX_CRITICAL_TRC)

#define DCBX_MIN_TRC    0
#define DCBX_MAX_TRC    DCBX_ALL_TRC

#ifndef __FUNCTION__
#define DCBX_FUNCTION_NAME    __FUNCTION__
#endif

#define DCBX_GLOBAL_TRC(args) \
             UtlTrcLog (ALL_FAILURE_TRC, ALL_FAILURE_TRC, DCBX_MOD_NAME, args)
/* Module names */
#define   DCBX_MOD_NAME            ((const char *)"DCBX")

#define DCBX_TRC(TraceType, fmt)                             \
        MOD_TRC(DCBX_TRC_FLAG, TraceType, DCBX_MOD_NAME,      \
                (const char *)fmt)

#define DCBX_TRC_ARG1(TraceType, fmt, arg1)                      \
        MOD_TRC_ARG1(DCBX_TRC_FLAG, TraceType, DCBX_MOD_NAME, \
                     (const char *)fmt, arg1)

#define DCBX_TRC_ARG2(TraceType, fmt, arg1, arg2)            \
        MOD_TRC_ARG2(DCBX_TRC_FLAG, TraceType, DCBX_MOD_NAME, \
                     (const char *)fmt, arg1, arg2)

#define DCBX_TRC_ARG3(TraceType, fmt, arg1, arg2, arg3)      \
        MOD_TRC_ARG3(DCBX_TRC_FLAG, TraceType, DCBX_MOD_NAME, \
                     (const char *)fmt, arg1, arg2, arg3)

#define DCBX_TRC_ARG4(TraceType, fmt, arg1, arg2, arg3, arg4)   \
        MOD_TRC_ARG4(DCBX_TRC_FLAG, TraceType, DCBX_MOD_NAME,    \
                     (const char *)fmt, arg1, arg2, arg3, arg4)

#define DCBX_TRC_ARG5(TraceType, fmt, arg1, arg2, arg3, arg4,arg5)   \
        MOD_TRC_ARG5(DCBX_TRC_FLAG, TraceType, DCBX_MOD_NAME,    \
                     (const char *)fmt, arg1, arg2, arg3, arg4, arg5)

#define DCBX_TRC_ARG6(TraceType, fmt, arg1, arg2, arg3, arg4,arg5, arg6)   \
        MOD_TRC_ARG6(DCBX_TRC_FLAG, TraceType, DCBX_MOD_NAME,    \
                     (const char *)fmt, arg1, arg2, arg3, arg4, arg5, arg6)

#define DCBX_PRINT  UtlTrcLog
#else
#define DCBX_MGMT_TRC             0
#define DCBX_SEM_TRC              0 
#define DCBX_TLV_TRC              0
#define DCBX_RESOURCE_TRC         0
#define DCBX_FAILURE_TRC          0
#define DCBX_RED_TRC              0
#define DCBX_MBSM_TRC             0
#define DCBX_CONTROL_PLANE_TRC    0
#define DCBX_CRITICAL_TRC         0

#define DCBX_ALL_FLAG             0
#define DCBX_ALL_TRC              0

#define DCBX_MIN_TRC    0
#define DCBX_MAX_TRC    0

#ifndef __FUNCTION__
#define DCBX_FUNCTION_NAME    __FUNCTION__
#endif

/* Module names */
#define   DCBX_MOD_NAME            0

#define DCBX_TRC(TraceType, fmt)                             \
{ \
    UNUSED_PARAM (fmt); \
}

#define DCBX_TRC_ARG1(TraceType, fmt, arg1)                      \
{ \
    UNUSED_PARAM (fmt); \
    UNUSED_PARAM (arg1); \
}


#define DCBX_TRC_ARG2(TraceType, fmt, arg1, arg2)            \
{   \
    UNUSED_PARAM (fmt); \
    UNUSED_PARAM (arg1); \
    UNUSED_PARAM (arg2); \
}

#define DCBX_TRC_ARG3(TraceType, fmt, arg1, arg2, arg3)      \
{   \
    UNUSED_PARAM (fmt); \
    UNUSED_PARAM (arg1); \
    UNUSED_PARAM (arg2); \
    UNUSED_PARAM (arg3); \
}

#define DCBX_TRC_ARG4(TraceType, fmt, arg1, arg2, arg3, arg4)   \
{   \
    UNUSED_PARAM (fmt); \
    UNUSED_PARAM (arg1); \
    UNUSED_PARAM (arg2); \
    UNUSED_PARAM (arg3); \
    UNUSED_PARAM (arg4); \
}

#define DCBX_TRC_ARG5(TraceType, fmt, arg1, arg2, arg3, arg4,arg5)   \
{   \
    UNUSED_PARAM (fmt); \
    UNUSED_PARAM (arg1); \
    UNUSED_PARAM (arg2); \
    UNUSED_PARAM (arg3); \
    UNUSED_PARAM (arg4); \
    UNUSED_PARAM (arg5); \
}


#define DCBX_TRC_ARG6(TraceType, fmt, arg1, arg2, arg3, arg4,arg5, arg6)   \
{   \
    UNUSED_PARAM (fmt); \
    UNUSED_PARAM (arg1); \
    UNUSED_PARAM (arg2); \
    UNUSED_PARAM (arg3); \
    UNUSED_PARAM (arg4); \
    UNUSED_PARAM (arg5); \
    UNUSED_PARAM (arg6); \
}

#define DCBX_PRINT UtlTrcLog

#define DCBX_GLOBAL_TRC(args) \
    UNUSED_PARAM (args);

#endif  /* TRACE_WANTED */

#endif


