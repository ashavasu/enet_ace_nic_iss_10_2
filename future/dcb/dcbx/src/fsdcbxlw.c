/********************************************************************
* Copyright (C) 2010 Aricent Inc . All Rights Reserved
*
* $Id: fsdcbxlw.c,v 1.26 2016/07/09 09:41:21 siva Exp $
*
* Description: Protocol Low Level Routines
*********************************************************************/

# include "dcbxinc.h"
# include "msr.h"

/* Function to Notify MSR regd shutdown status */
PRIVATE VOID ETSNotifyShutdownStatus PROTO ((VOID));
PRIVATE VOID PFCNotifyShutdownStatus PROTO ((VOID));
PRIVATE VOID AppPriNotifyShutdownStatus PROTO ((VOID));

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsDcbPfcMinThreshold
 Input       :  The Indices

                The Object 
                retValFsDcbPfcMinThreshold
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDcbPfcMinThreshold (UINT4 *pu4RetValFsDcbPfcMinThreshold)
{
    *pu4RetValFsDcbPfcMinThreshold = gPFCGlobalInfo.u4PFCMinThresh;
    DCBX_TRC_ARG2 (DCBX_MGMT_TRC,
                   "%s : PFC Minimum Threshold = %d SUCCESS.\r\n", __FUNCTION__,
                   *pu4RetValFsDcbPfcMinThreshold);
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFsDcbPfcMaxThreshold
 Input       :  The Indices

                The Object 
                retValFsDcbPfcMaxThreshold
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDcbPfcMaxThreshold (UINT4 *pu4RetValFsDcbPfcMaxThreshold)
{
    *pu4RetValFsDcbPfcMaxThreshold = gPFCGlobalInfo.u4PFCMaxThresh;
    DCBX_TRC_ARG2 (DCBX_MGMT_TRC,
                   "%s : PFC Maximum Threshold = %d SUCCESS.\r\n", __FUNCTION__,
                   *pu4RetValFsDcbPfcMaxThreshold);
    return (SNMP_SUCCESS);
}

/****************************************************************************
Function    :  nmhGetFsDcbMaxPfcProfiles
 Input       :  The Indices

                The Object 
                retValFsDcbMaxPfcProfiles
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDcbMaxPfcProfiles (UINT4 *pu4RetValFsDcbMaxPfcProfiles)
{
    *pu4RetValFsDcbMaxPfcProfiles = PFC_MAX_PROFILES;
    DCBX_TRC_ARG2 (DCBX_MGMT_TRC, "%s : PFC Maximum Profiles = %d SUCCESS.\r\n",
                   __FUNCTION__, *pu4RetValFsDcbMaxPfcProfiles);
    return (SNMP_SUCCESS);
}

/* Low Level SET Routine for All Objects  */
/****************************************************************************
 Function    :  nmhSetFsDcbPfcMinThreshold
 Input       :  The Indices

                The Object 
                setValFsDcbPfcMinThreshold
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsDcbPfcMinThreshold (UINT4 u4SetValFsDcbPfcMinThreshold)
{
    INT4                i4RetVal = OSIX_SUCCESS;

    DCBX_TRC_ARG2 (DCBX_MGMT_TRC, "%s, Args: "
            "u4SetValFsDcbPfcMinThreshold: %d\r\n", 
            __func__, u4SetValFsDcbPfcMinThreshold);

    gPFCGlobalInfo.u4PFCMinThresh = u4SetValFsDcbPfcMinThreshold;
    i4RetVal = PFCUtlHwConfigPfc (PFC_INIT_VAL, PFC_INIT_VAL,
                                  PFC_INIT_VAL, PFC_THR_CFG);
    if (i4RetVal == OSIX_FAILURE)
    {
        DCBX_TRC_ARG2 (DCBX_MGMT_TRC,
                       "%s: PFC Minimum Threshold = %d FAILURE. \r\n",
                       __FUNCTION__, u4SetValFsDcbPfcMinThreshold);
        return SNMP_FAILURE;
    }
    return (SNMP_SUCCESS);
}

/****************************************************************************
Function    :  nmhSetFsDcbPfcMaxThreshold
Input       :  The Indices

                The Object 
                setValFsDcbPfcMaxThreshold
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsDcbPfcMaxThreshold (UINT4 u4SetValFsDcbPfcMaxThreshold)
{
    INT4                i4RetVal = OSIX_SUCCESS;

    DCBX_TRC_ARG2 (DCBX_MGMT_TRC, "%s, Args: "
            "u4SetValFsDcbPfcMaxThreshold: %d\r\n", 
            __func__, u4SetValFsDcbPfcMaxThreshold);

    gPFCGlobalInfo.u4PFCMaxThresh = u4SetValFsDcbPfcMaxThreshold;
    i4RetVal = PFCUtlHwConfigPfc (PFC_INIT_VAL, PFC_INIT_VAL,
                                  PFC_INIT_VAL, PFC_THR_CFG);
    if (i4RetVal == OSIX_FAILURE)
    {
        DCBX_TRC_ARG2 (DCBX_MGMT_TRC,
                       "%s: PFC Maximum Threshold = %d FAILURE. \r\n",
                       __FUNCTION__, u4SetValFsDcbPfcMaxThreshold);
        return SNMP_FAILURE;
    }
    return (SNMP_SUCCESS);
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsDcbPfcMinThreshold
 Input       :  The Indices

                The Object 
                testValFsDcbPfcMinThreshold
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsDcbPfcMinThreshold (UINT4 *pu4ErrorCode,
                               UINT4 u4TestValFsDcbPfcMinThreshold)
{
    PFC_SHUTDOWN_CHECK_TEST;

    if ((u4TestValFsDcbPfcMinThreshold < PFC_MIN_THRES) ||
        (u4TestValFsDcbPfcMinThreshold > PFC_MAX_THRES))
    {
        DCBX_TRC_ARG3 (DCBX_MGMT_TRC,
                       "%s : PFC Minimum Threshold value is out of range. The"
                       " range should be Min %d - Max %d. \r\n", __FUNCTION__,
                       PFC_MIN_THRES, PFC_MAX_THRES);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_ERR_PFC_INVALID_THRESHOLD);
        return (SNMP_FAILURE);
    }

    if (u4TestValFsDcbPfcMinThreshold > gPFCGlobalInfo.u4PFCMaxThresh)
    {
        DCBX_TRC_ARG3 (DCBX_MGMT_TRC,
                       "%s : PFC Minimum Threshold value %d is greater than "
                       " maximum threshold value %d \r\n", __FUNCTION__,
                       u4TestValFsDcbPfcMinThreshold,
                       gPFCGlobalInfo.u4PFCMaxThresh);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_ERR_PFC_THRESH_MISCONF);
        return (SNMP_FAILURE);
    }

    DCBX_TRC_ARG2 (DCBX_MGMT_TRC,
                   "%s : PFC Minimum Threshold = %d SUCCESS.\r\n", __FUNCTION__,
                   u4TestValFsDcbPfcMinThreshold);
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhTestv2FsDcbPfcMaxThreshold
 Input       :  The Indices

                The Object 
                testValFsDcbPfcMaxThreshold
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsDcbPfcMaxThreshold (UINT4 *pu4ErrorCode,
                               UINT4 u4TestValFsDcbPfcMaxThreshold)
{

    PFC_SHUTDOWN_CHECK_TEST;

    if ((u4TestValFsDcbPfcMaxThreshold < PFC_MIN_THRES) ||
        (u4TestValFsDcbPfcMaxThreshold > PFC_MAX_THRES))
    {
        DCBX_TRC_ARG3 (DCBX_MGMT_TRC,
                       "%s : PFC Maximum Threshold value is out of range. The"
                       " range should be Max %d - Max %d. \r\n", __FUNCTION__,
                       PFC_MIN_THRES, PFC_MAX_THRES);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_ERR_PFC_INVALID_THRESHOLD);
        return (SNMP_FAILURE);
    }

    if (u4TestValFsDcbPfcMaxThreshold < gPFCGlobalInfo.u4PFCMinThresh)
    {
        DCBX_TRC_ARG3 (DCBX_MGMT_TRC,
                       "%s : PFC Maximum Threshold value %d is lesser than "
                       " maximum threshold value %d \r\n", __FUNCTION__,
                       u4TestValFsDcbPfcMaxThreshold,
                       gPFCGlobalInfo.u4PFCMaxThresh);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_ERR_PFC_THRESH_MISCONF);
        return (SNMP_FAILURE);
    }

    DCBX_TRC_ARG2 (DCBX_MGMT_TRC,
                   "%s : PFC Maximum Threshold = %d SUCCESS.\r\n", __FUNCTION__,
                   u4TestValFsDcbPfcMaxThreshold);
    return (SNMP_SUCCESS);
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsDcbPfcMinThreshold
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsDcbPfcMinThreshold (UINT4 *pu4ErrorCode,
                              tSnmpIndexList * pSnmpIndexList,
                              tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsDcbPfcMaxThreshold
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsDcbPfcMaxThreshold (UINT4 *pu4ErrorCode,
                              tSnmpIndexList * pSnmpIndexList,
                              tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsDcbPortTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsDcbPortTable
 Input       :  The Indices
                FsDcbPortNumber
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsDcbPortTable (INT4 i4FsDcbPortNumber)
{
    /*Backward Compatibility Support for FsDcbPortTable */
    if (nmhValidateIndexInstanceFsDCBXPortTable (i4FsDcbPortNumber)
            != SNMP_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsDcbPortTable
 Input       :  The Indices
                FsDcbPortNumber
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsDcbPortTable (INT4 *pi4FsDcbPortNumber)
{

    /*Backward Compatibility Support for FsDcbPortTable */
    if ((nmhGetFirstIndexFsDCBXPortTable (pi4FsDcbPortNumber)
         == SNMP_SUCCESS)
        || (nmhGetFirstIndexFsPFCPortTable (pi4FsDcbPortNumber)
            == SNMP_SUCCESS)
        || (nmhGetFirstIndexFsETSPortTable (pi4FsDcbPortNumber)
            == SNMP_SUCCESS)
        || (nmhGetFirstIndexFsAppPriPortTable (pi4FsDcbPortNumber)
            == SNMP_SUCCESS))
    {
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsDcbPortTable
 Input       :  The Indices
                FsDcbPortNumber
                nextFsDcbPortNumber
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsDcbPortTable (INT4 i4FsDcbPortNumber,
                               INT4 *pi4NextFsDcbPortNumber)
{
    /*Backward Compatibility Support for FsDcbPortTable */
    if ((nmhGetNextIndexFsDCBXPortTable (i4FsDcbPortNumber,
                    pi4NextFsDcbPortNumber) == SNMP_SUCCESS)
        || (nmhGetNextIndexFsPFCPortTable (i4FsDcbPortNumber,
                pi4NextFsDcbPortNumber) == SNMP_SUCCESS) 
        || (nmhGetNextIndexFsETSPortTable (i4FsDcbPortNumber,
                pi4NextFsDcbPortNumber) == SNMP_SUCCESS) 
        || (nmhGetNextIndexFsAppPriPortTable (i4FsDcbPortNumber,
                pi4NextFsDcbPortNumber) == SNMP_SUCCESS))
    {
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsDcbETSAdminStatus
 Input       :  The Indices
                FsDcbPortNumber

                The Object 
                retValFsDcbETSAdminStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDcbETSAdminStatus (INT4 i4FsDcbPortNumber,
                           INT4 *pi4RetValFsDcbETSAdminStatus)
{
    tDcbxPortEntry     *pDcbxPortEntry = NULL;
    pDcbxPortEntry = DcbxUtlGetPortEntry ((UINT4) i4FsDcbPortNumber);
    if (pDcbxPortEntry == NULL)
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : DcbxUtlGetPortEntry () "
                       "Returns FAILURE. \r\n", __FUNCTION__);
        return SNMP_FAILURE;
    }
    *pi4RetValFsDcbETSAdminStatus = pDcbxPortEntry->u1ETSAdminStatus;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsDcbPFCAdminStatus
 Input       :  The Indices
                FsDcbPortNumber

                The Object 
                retValFsDcbPFCAdminStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDcbPFCAdminStatus (INT4 i4FsDcbPortNumber,
                           INT4 *pi4RetValFsDcbPFCAdminStatus)
{
    tDcbxPortEntry     *pDcbxPortEntry = NULL;
    pDcbxPortEntry = DcbxUtlGetPortEntry ((UINT4) i4FsDcbPortNumber);
    if (pDcbxPortEntry == NULL)
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : DcbxUtlGetPortEntry () "
                       "Returns FAILURE. \r\n", __FUNCTION__);
        return SNMP_FAILURE;
    }
    *pi4RetValFsDcbPFCAdminStatus = pDcbxPortEntry->u1PFCAdminStatus;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsDcbAppPriAdminStatus
 Input       :  The Indices
                FsDcbPortNumber

                The Object 
                retValFsDcbAppPriAdminStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsDcbAppPriAdminStatus(INT4 i4FsDcbPortNumber, INT4 *pi4RetValFsDcbAppPriAdminStatus)
{
    tDcbxPortEntry     *pDcbxPortEntry = NULL;
    pDcbxPortEntry = DcbxUtlGetPortEntry ((UINT4) i4FsDcbPortNumber);
    if (pDcbxPortEntry == NULL)
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : DcbxUtlGetPortEntry () "
                       "Returns FAILURE. \r\n", __FUNCTION__);
        return SNMP_FAILURE;
    }
    *pi4RetValFsDcbAppPriAdminStatus = pDcbxPortEntry->u1AppPriAdminStatus;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsDcbRowStatus
 Input       :  The Indices
                FsDcbPortNumber

                The Object 
                retValFsDcbRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDcbRowStatus (INT4 i4FsDcbPortNumber, INT4 *pi4RetValFsDcbRowStatus)
{
    INT4                i4RowStatus = DCBX_ZERO;

    if ((nmhGetFsETSRowStatus (i4FsDcbPortNumber, &i4RowStatus) != SNMP_SUCCESS)
        && (nmhGetFsPFCRowStatus (i4FsDcbPortNumber, &i4RowStatus) !=
            SNMP_SUCCESS)
        && (nmhGetFsAppPriRowStatus (i4FsDcbPortNumber, &i4RowStatus) !=
            SNMP_SUCCESS))
    {
        *pi4RetValFsDcbRowStatus = NOT_IN_SERVICE;
        return SNMP_SUCCESS;
    }
    *pi4RetValFsDcbRowStatus = ACTIVE;
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsDcbRowStatus
 Input       :  The Indices
                FsDcbPortNumber

                The Object 
                setValFsDcbRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsDcbRowStatus (INT4 i4FsDcbPortNumber, INT4 i4SetValFsDcbRowStatus)
{

    DCBX_TRC_ARG3 (DCBX_MGMT_TRC, "%s, Args: "
            "i4FsDcbPortNumber: %d i4SetValFsDcbRowStatus: %d\r\n", 
            __func__, i4FsDcbPortNumber, i4SetValFsDcbRowStatus);

    if (i4SetValFsDcbRowStatus == ACTIVE)
    {
        return SNMP_SUCCESS;
    }
    else if ((i4SetValFsDcbRowStatus == CREATE_AND_GO) ||
             (i4SetValFsDcbRowStatus == CREATE_AND_WAIT))
    {
        nmhSetFsETSRowStatus (i4FsDcbPortNumber, CREATE_AND_GO);
        nmhSetFsPFCRowStatus (i4FsDcbPortNumber, CREATE_AND_GO);
        nmhSetFsAppPriRowStatus (i4FsDcbPortNumber, CREATE_AND_GO);
    }
    else
    {
        nmhSetFsETSRowStatus (i4FsDcbPortNumber, DESTROY);
        nmhSetFsPFCRowStatus (i4FsDcbPortNumber, DESTROY);
        nmhSetFsAppPriRowStatus (i4FsDcbPortNumber, DESTROY);
    }
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsDcbRowStatus
 Input       :  The Indices
                FsDcbPortNumber

                The Object 
                testValFsDcbRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsDcbRowStatus (UINT4 *pu4ErrorCode, INT4 i4FsDcbPortNumber,
                         INT4 i4TestValFsDcbRowStatus)
{

    tDcbxPortEntry     *pDcbxPortEntry = NULL;
    UINT4               u4ErrorCode = DCBX_ZERO;

    if (DcbxUtlValidatePort ((UINT4) i4FsDcbPortNumber) == OSIX_FAILURE)
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : DcbxUtlValidatePort() "
                       "Returns Failure. \r\n", __FUNCTION__);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_ERR_DCBX_NO_PORT);
        return SNMP_FAILURE;
    }
    pDcbxPortEntry = DcbxUtlGetPortEntry ((UINT4) i4FsDcbPortNumber);
    switch (i4TestValFsDcbRowStatus)
    {
        case CREATE_AND_GO:
            if ((nmhTestv2FsETSRowStatus (&u4ErrorCode, i4FsDcbPortNumber,
                                          CREATE_AND_GO) != SNMP_SUCCESS) ||
                (nmhTestv2FsPFCRowStatus (&u4ErrorCode, i4FsDcbPortNumber,
                                          CREATE_AND_GO) != SNMP_SUCCESS) ||
                (nmhTestv2FsAppPriRowStatus (&u4ErrorCode, i4FsDcbPortNumber, 
                                             CREATE_AND_GO) != SNMP_SUCCESS))
            {
                return SNMP_FAILURE;
            }
            break;

        case DESTROY:
            if (pDcbxPortEntry == NULL)
            {
                DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : ETSUtlGetPortEntry () "
                               "Returns FAILURE. \r\n", __FUNCTION__);
                *pu4ErrorCode = SNMP_ERR_NO_CREATION;
                CLI_SET_ERR (CLI_ERR_ETS_NO_PORT_ENTRY);
                return SNMP_FAILURE;
            }
            break;

        case ACTIVE:
            break;

        default:
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsDcbPortTable
 Input       :  The Indices
                FsDcbPortNumber
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsDcbPortTable (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                        tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsDcbxGlobalTraceLevel
 Input       :  The Indices

                The Object 
                retValFsDcbxGlobalTraceLevel
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDcbxGlobalTraceLevel (INT4 *pi4RetValFsDcbxGlobalTraceLevel)
{
    *pi4RetValFsDcbxGlobalTraceLevel = (INT4) gDcbxGlobalInfo.u4DCBXTrcFlag;
    DCBX_TRC_ARG2 (DCBX_MGMT_TRC,
                   "%s : DCBX Global Trace Level = %d SUCCESS.\r\n",
                   __FUNCTION__, *pi4RetValFsDcbxGlobalTraceLevel);
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsDcbxGlobalTraceLevel
 Input       :  The Indices

                The Object 
                setValFsDcbxGlobalTraceLevel
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsDcbxGlobalTraceLevel (INT4 i4SetValFsDcbxGlobalTraceLevel)
{
    gDcbxGlobalInfo.u4DCBXTrcFlag = (UINT4) i4SetValFsDcbxGlobalTraceLevel;
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsDcbxGlobalTraceLevel
 Input       :  The Indices

                The Object 
                testValFsDcbxGlobalTraceLevel
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsDcbxGlobalTraceLevel (UINT4 *pu4ErrorCode,
                                 INT4 i4TestValFsDcbxGlobalTraceLevel)
{

    if ((i4TestValFsDcbxGlobalTraceLevel < DCBX_MIN_TRC) ||
        (i4TestValFsDcbxGlobalTraceLevel > DCBX_MAX_TRC))
    {
        DCBX_TRC_ARG3 (DCBX_MGMT_TRC,
                       "%s : DCBX TRACE out of range. The"
                       " range should be Max %d - Max %d. \r\n", __FUNCTION__,
                       PFC_MIN_THRES, PFC_MAX_THRES);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_ERR_DCBX_INVALID_TRACE);
        return (SNMP_FAILURE);
    }
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsDcbxGlobalTraceLevel
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsDcbxGlobalTraceLevel (UINT4 *pu4ErrorCode,
                                tSnmpIndexList * pSnmpIndexList,
                                tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsDCBXPortTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsDCBXPortTable
 Input       :  The Indices
                FsDCBXPortNumber
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsDCBXPortTable (INT4 i4FsDCBXPortNumber)
{
    tDcbxPortEntry     *pDcbxPortEntry = NULL;

    pDcbxPortEntry = DcbxUtlGetPortEntry ((UINT4) i4FsDCBXPortNumber);
    if (pDcbxPortEntry == NULL)
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : DcbxUtlGetPortEntry () "
                       "Returns FAILURE. \r\n", __FUNCTION__);
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsDCBXPortTable
 Input       :  The Indices
                FsDCBXPortNumber
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsDCBXPortTable (INT4 *pi4FsDCBXPortNumber)
{
    tDcbxPortEntry     *pDcbxPortEntry = NULL;

    pDcbxPortEntry = (tDcbxPortEntry *)
        RBTreeGetFirst (gDcbxGlobalInfo.pRbDcbxPortTbl);
    if (pDcbxPortEntry == NULL)
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : RbTreeGetFirst Node () "
                       "Returns NULL. \r\n", __FUNCTION__);
        return SNMP_FAILURE;
    }

    *pi4FsDCBXPortNumber = (INT4) pDcbxPortEntry->u4IfIndex;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsDCBXPortTable
 Input       :  The Indices
                FsDCBXPortNumber
                nextFsDCBXPortNumber
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsDCBXPortTable (INT4 i4FsDCBXPortNumber,
                                INT4 *pi4NextFsDCBXPortNumber)
{
    tDcbxPortEntry     *pDcbxPortEntry = NULL;
    tDcbxPortEntry      DCBXPortEntry;

    MEMSET (&DCBXPortEntry, DCBX_INIT_VAL, sizeof (tDcbxPortEntry));
    DCBXPortEntry.u4IfIndex = (UINT4) i4FsDCBXPortNumber;
    pDcbxPortEntry = (tDcbxPortEntry *)
        RBTreeGetNext (gDcbxGlobalInfo.pRbDcbxPortTbl,
                       &DCBXPortEntry, DcbxUtlPortTblCmpFn);
    if (pDcbxPortEntry == NULL)
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : RBTreeGetNext () "
                       "Returns FAILURE. \r\n", __FUNCTION__);
        return SNMP_FAILURE;
    }

    *pi4NextFsDCBXPortNumber = (INT4) pDcbxPortEntry->u4IfIndex;
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsDCBXAdminStatus
 Input       :  The Indices
                FsDCBXPortNumber

                The Object 
                retValFsDCBXAdminStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDCBXAdminStatus (INT4 i4FsDCBXPortNumber,
                         INT4 *pi4RetValFsDCBXAdminStatus)
{
    tDcbxPortEntry     *pDcbxPortEntry = NULL;

    pDcbxPortEntry = DcbxUtlGetPortEntry ((UINT4) i4FsDCBXPortNumber);
    if (pDcbxPortEntry == NULL)
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : DcbxUtlGetPortEntry () "
                       "Returns FAILURE. \r\n", __FUNCTION__);
        return SNMP_FAILURE;
    }

    *pi4RetValFsDCBXAdminStatus = pDcbxPortEntry->u1DcbxAdminStatus;
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsDCBXAdminStatus
 Input       :  The Indices
                FsDCBXPortNumber

                The Object 
                setValFsDCBXAdminStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsDCBXAdminStatus (INT4 i4FsDCBXPortNumber,
                         INT4 i4SetValFsDCBXAdminStatus)
{
    tDcbxPortEntry     *pDcbxPortEntry = NULL;

    DCBX_TRC_ARG3 (DCBX_MGMT_TRC, "%s, Args: "
            "i4FsDCBXPortNumber: %d i4SetValFsDCBXAdminStatus: %d\r\n", 
            __func__, i4FsDCBXPortNumber, i4SetValFsDCBXAdminStatus);

    pDcbxPortEntry = DcbxUtlGetPortEntry ((UINT4) i4FsDCBXPortNumber);
    if (pDcbxPortEntry == NULL)
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : DcbxUtlGetPortEntry () "
                       "Returns FAILURE. \r\n", __FUNCTION__);
        return SNMP_FAILURE;
    }
    pDcbxPortEntry->u1DcbxAdminStatus = (UINT1) i4SetValFsDCBXAdminStatus;
    if (i4SetValFsDCBXAdminStatus == DCBX_ENABLED)
    {
        DcbxUtlPortAdminEnable ((UINT4) i4FsDCBXPortNumber);
    }
    else
    {
        DcbxUtlPortAdminDisable ((UINT4) i4FsDCBXPortNumber);
    }
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsDCBXAdminStatus
 Input       :  The Indices
                FsDCBXPortNumber

                The Object 
                testValFsDCBXAdminStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsDCBXAdminStatus (UINT4 *pu4ErrorCode,
                            INT4 i4FsDCBXPortNumber,
                            INT4 i4TestValFsDCBXAdminStatus)
{
    tDcbxPortEntry     *pDcbxPortEntry = NULL;

    if (DcbxUtlValidatePort ((UINT4) i4FsDCBXPortNumber) == OSIX_FAILURE)
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : DcbxUtlValidatePort() "
                       "Returns Failure. \r\n", __FUNCTION__);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        /* Currently, DCBX shouldn't be allowed in 
           Port-Channel Interface, setting appropriate error code 
        */
        if (CfaIsLaggInterface ((UINT4)i4FsDCBXPortNumber) == CFA_TRUE) 
        {
          CLI_SET_ERR (CLI_ERR_DCBX_PORT_IN_PORT_CHANNEL);
        }
        else
        {
          CLI_SET_ERR (CLI_ERR_DCBX_NO_PORT);
        }
        return SNMP_FAILURE;
    }

    pDcbxPortEntry = DcbxUtlGetPortEntry ((UINT4) i4FsDCBXPortNumber);
    if (pDcbxPortEntry == NULL)
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : DcbxUtlGetPortEntry () "
                       "Returns FAILURE. \r\n", __FUNCTION__);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_ERR_DCBX_NO_PORT);
        return SNMP_FAILURE;
    }

    if ((i4TestValFsDCBXAdminStatus != DCBX_ENABLED) &&
        (i4TestValFsDCBXAdminStatus != DCBX_DISABLED))
    {
        DCBX_TRC_ARG2 (DCBX_MGMT_TRC, "%s : DCBX Admin Status %d FAILED.\r\n",
                       __FUNCTION__, i4TestValFsDCBXAdminStatus);

        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_ERR_DCBX_INVALID_ADMIN_STATUS);
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsDCBXPortTable
 Input       :  The Indices
                FsDCBXPortNumber
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsDCBXPortTable (UINT4 *pu4ErrorCode,
                         tSnmpIndexList * pSnmpIndexList,
                         tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsETSSystemControl
 Input       :  The Indices

                The Object 
                retValFsETSSystemControl
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsETSSystemControl (INT4 *pi4RetValFsETSSystemControl)
{
    *pi4RetValFsETSSystemControl = gETSGlobalInfo.u1ETSSystemCtrl;
    DCBX_TRC_ARG2 (DCBX_MGMT_TRC, "%s : ETS System Control = %d SUCCESS.\r\n",
                   __FUNCTION__, *pi4RetValFsETSSystemControl);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsETSModuleStatus
 Input       :  The Indices

                The Object 
                retValFsETSModuleStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsETSModuleStatus (INT4 *pi4RetValFsETSModuleStatus)
{
    *pi4RetValFsETSModuleStatus = gETSGlobalInfo.u1ETSModStatus;
    DCBX_TRC_ARG2 (DCBX_MGMT_TRC, "%s : ETS Module Status = %d SUCCESS.\r\n",
                   __FUNCTION__, *pi4RetValFsETSModuleStatus);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsETSClearCounters
 Input       :  The Indices

                The Object 
                retValFsETSClearCounters
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsETSClearCounters (INT4 *pi4RetValFsETSClearCounters)
{

    *pi4RetValFsETSClearCounters = ETS_DISABLED;
    DCBX_TRC_ARG2 (DCBX_MGMT_TRC,
                   "%s : ETS Clear Counters Status  = %d SUCCESS.\r\n",
                   __FUNCTION__, *pi4RetValFsETSClearCounters);
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsETSGlobalEnableTrap
 Input       :  The Indices

                The Object 
                retValFsETSGlobalEnableTrap
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsETSGlobalEnableTrap (INT4 *pi4RetValFsETSGlobalEnableTrap)
{
    *pi4RetValFsETSGlobalEnableTrap = (INT4) gETSGlobalInfo.u4ETSTrap;
    DCBX_TRC_ARG2 (DCBX_MGMT_TRC, "%s : ETS Global Trap = %d SUCCESS.\r\n",
                   __FUNCTION__, *pi4RetValFsETSGlobalEnableTrap);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsETSGeneratedTrapCount
 Input       :  The Indices
                The Object
                retValFsETSGeneratedTrapCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
                Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsETSGeneratedTrapCount (UINT4 *pu4RetValFsETSGeneratedTrapCount)
{
    *pu4RetValFsETSGeneratedTrapCount = gETSGlobalInfo.u4ETSGeneratedTrapCount;
    DCBX_TRC_ARG2 (DCBX_MGMT_TRC, "%s : ETS Trap Count= %d SUCCESS.\r\n",
                   __FUNCTION__, *pu4RetValFsETSGeneratedTrapCount);
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsETSSystemControl
 Input       :  The Indices

                The Object 
                setValFsETSSystemControl
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsETSSystemControl (INT4 i4SetValFsETSSystemControl)
{
    DCBX_TRC_ARG2 (DCBX_MGMT_TRC, "%s, Args: "
            "i4SetValFsETSSystemControl: %d\r\n", 
            __func__, i4SetValFsETSSystemControl);

    if (gETSGlobalInfo.u1ETSSystemCtrl != (UINT1) i4SetValFsETSSystemControl)
    {
        if (i4SetValFsETSSystemControl == ETS_START)
        {
            if (ETSMainModuleStart () == OSIX_FAILURE)
            {
                return SNMP_FAILURE;
            }
        }
        else
        {
            ETSMainModuleShutDown ();
            ETSNotifyShutdownStatus ();
        }
        gETSGlobalInfo.u1ETSSystemCtrl = (UINT1) i4SetValFsETSSystemControl;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsETSModuleStatus
 Input       :  The Indices

                The Object 
                setValFsETSModuleStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsETSModuleStatus (INT4 i4SetValFsETSModuleStatus)
{
    DCBX_TRC_ARG2 (DCBX_MGMT_TRC, "%s, Args: "
            "i4SetValFsETSModuleStatus: %d\r\n", 
            __func__, i4SetValFsETSModuleStatus);

    if (gETSGlobalInfo.u1ETSModStatus != (UINT1) i4SetValFsETSModuleStatus)
    {
        ETSUtlModuleStatusChange ((UINT1) i4SetValFsETSModuleStatus);
        /* Call the module status change function which will have 
         * the updation of Module status data strcuture */
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsETSClearCounters
 Input       :  The Indices

                The Object 
                setValFsETSClearCounters
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsETSClearCounters (INT4 i4SetValFsETSClearCounters)
{
    tETSPortEntry      *pETSPortEntry = NULL;
    INT4                i4ETSPortNumber = DCBX_ZERO;
    INT4                i4NextETSPortNumber = DCBX_ZERO;
    INT4                i4RetVal = SNMP_SUCCESS;

    DCBX_TRC_ARG2 (DCBX_MGMT_TRC, "%s, Args: "
            "i4SetValFsETSClearCounters: %d\r\n", 
            __func__, i4SetValFsETSClearCounters);

    if (i4SetValFsETSClearCounters == ETS_ENABLED)
    {
        i4RetVal = nmhGetFirstIndexFsETSPortTable (&i4NextETSPortNumber);
        if (i4RetVal == SNMP_FAILURE)
        {
            DCBX_TRC_ARG1 (DCBX_MGMT_TRC,
                           "In %s : nmhGetFirstIndexFsETSPortTable () "
                           "No ETS entries present to clear. \r\n",
                           __FUNCTION__);
            return SNMP_SUCCESS;
        }

        do
        {
            i4ETSPortNumber = i4NextETSPortNumber;
            pETSPortEntry = ETSUtlGetPortEntry ((UINT4) i4ETSPortNumber);
            if (pETSPortEntry == NULL)
            {
                DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : ETSUtlGetPortEntry () "
                               "Returns FAILURE. \r\n", __FUNCTION__);
                return SNMP_FAILURE;
            }
            pETSPortEntry->u4ETSTcSuppTxTLVCount = DCBX_ZERO;
            pETSPortEntry->u4ETSTcSuppRxTLVCount = DCBX_ZERO;
            pETSPortEntry->u4ETSTcSupRxTLVError = DCBX_ZERO;
            pETSPortEntry->u4ETSRecoTxTLVCount = DCBX_ZERO;
            pETSPortEntry->u4ETSRecoRxTLVCount = DCBX_ZERO;
            pETSPortEntry->u4ETSRecoRxTLVError = DCBX_ZERO;
            pETSPortEntry->u4ETSConfTxTLVCount = DCBX_ZERO;
            pETSPortEntry->u4ETSConfRxTLVCount = DCBX_ZERO;
            pETSPortEntry->u4ETSConfRxTLVError = DCBX_ZERO;
        }
        while (nmhGetNextIndexFsETSPortTable
               (i4ETSPortNumber, &i4NextETSPortNumber) != SNMP_FAILURE);
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsETSGlobalEnableTrap
 Input       :  The Indices

                The Object 
                setValFsETSGlobalEnableTrap
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsETSGlobalEnableTrap (INT4 i4SetValFsETSGlobalEnableTrap)
{
    DCBX_TRC_ARG2 (DCBX_MGMT_TRC, "%s, Args: "
            "i4SetValFsETSGlobalEnableTrap: %d\r\n", 
            __func__, i4SetValFsETSGlobalEnableTrap);

    gETSGlobalInfo.u4ETSTrap = (UINT4) i4SetValFsETSGlobalEnableTrap;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsETSGeneratedTrapCount
 Input       :  The Indices
                The Object
                setValFsETSGeneratedTrapCount
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsETSGeneratedTrapCount (UINT4 u4SetValFsETSGeneratedTrapCount)
{
    DCBX_TRC_ARG2 (DCBX_MGMT_TRC, "%s, Args: "
            "u4SetValFsETSGeneratedTrapCount: %d\r\n", 
            __func__, u4SetValFsETSGeneratedTrapCount);

    gETSGlobalInfo.u4ETSGeneratedTrapCount = u4SetValFsETSGeneratedTrapCount;
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsETSSystemControl
 Input       :  The Indices

                The Object 
                testValFsETSSystemControl
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsETSSystemControl (UINT4 *pu4ErrorCode,
                             INT4 i4TestValFsETSSystemControl)
{
    if ((i4TestValFsETSSystemControl < ETS_START) ||
        (i4TestValFsETSSystemControl > ETS_SHUTDOWN))
    {
        DCBX_TRC_ARG2 (DCBX_MGMT_TRC, "%s : ETS SYSTEM CONTROL%d FAILED.\r\n",
                       __FUNCTION__, i4TestValFsETSSystemControl);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_ERR_ETS_INVALID_SYS_CONTROL);
        return (SNMP_FAILURE);
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsETSModuleStatus
 Input       :  The Indices

                The Object 
                testValFsETSModuleStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsETSModuleStatus (UINT4 *pu4ErrorCode,
                            INT4 i4TestValFsETSModuleStatus)
{
    ETS_SHUTDOWN_CHECK_TEST;
    if ((i4TestValFsETSModuleStatus < ETS_ENABLED) ||
        (i4TestValFsETSModuleStatus > ETS_DISABLED))
    {
        DCBX_TRC_ARG2 (DCBX_MGMT_TRC, "%s : ETS MODULE STATUS%d FAILED.\r\n",
                       __FUNCTION__, i4TestValFsETSModuleStatus);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_ERR_ETS_INVALID_SYS_STATUS);
        return (SNMP_FAILURE);
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsETSClearCounters
 Input       :  The Indices

                The Object 
                testValFsETSClearCounters
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsETSClearCounters (UINT4 *pu4ErrorCode,
                             INT4 i4TestValFsETSClearCounters)
{
    ETS_SHUTDOWN_CHECK_TEST;

    if ((i4TestValFsETSClearCounters != ETS_ENABLED) &&
        (i4TestValFsETSClearCounters != ETS_DISABLED))
    {
        DCBX_TRC_ARG2 (DCBX_MGMT_TRC,
                       "%s : ETS Clear Counter Status%d FAILED.\r\n",
                       __FUNCTION__, i4TestValFsETSClearCounters);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_ERR_ETS_INVALID_CLEAR_STATUS);
        return (SNMP_FAILURE);
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsETSGlobalEnableTrap
 Input       :  The Indices

                The Object 
                testValFsETSGlobalEnableTrap
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsETSGlobalEnableTrap (UINT4 *pu4ErrorCode,
                                INT4 i4TestValFsETSGlobalEnableTrap)
{
    ETS_SHUTDOWN_CHECK_TEST;

    if (i4TestValFsETSGlobalEnableTrap & (~(ETS_ALL_TRAP)))
    {
        DCBX_TRC_ARG2 (DCBX_MGMT_TRC,
                       "%s : ETS Global Trap Level%d FAILED.\r\n", __FUNCTION__,
                       i4TestValFsETSGlobalEnableTrap);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_ERR_ETS_INVALID_NOTIFY);
        return (SNMP_FAILURE);
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsETSGeneratedTrapCount
 Input       :  The Indices
                The Object
                testValFsETSGeneratedTrapCount
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsETSGeneratedTrapCount (UINT4 *pu4ErrorCode,
                                  UINT4 u4TestValFsETSGeneratedTrapCount)
{
    ETS_SHUTDOWN_CHECK_TEST;

    if (u4TestValFsETSGeneratedTrapCount != 0)
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC,
                       "%s : Values other than zero are not allowed to set "
                       "for this object.\r\n", __FUNCTION__);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_ERR_ETS_INVALID_CLEAR_TRAP);
        return (SNMP_FAILURE);
    }

    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsETSSystemControl
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsETSSystemControl (UINT4 *pu4ErrorCode,
                            tSnmpIndexList * pSnmpIndexList,
                            tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsETSModuleStatus
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsETSModuleStatus (UINT4 *pu4ErrorCode,
                           tSnmpIndexList * pSnmpIndexList,
                           tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsETSClearCounters
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsETSClearCounters (UINT4 *pu4ErrorCode,
                            tSnmpIndexList * pSnmpIndexList,
                            tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsETSGlobalEnableTrap
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsETSGlobalEnableTrap (UINT4 *pu4ErrorCode,
                               tSnmpIndexList * pSnmpIndexList,
                               tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsETSGeneratedTrapCount
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsETSGeneratedTrapCount (UINT4 *pu4ErrorCode,
                                 tSnmpIndexList * pSnmpIndexList,
                                 tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsETSPortTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsETSPortTable
 Input       :  The Indices
                FsETSPortNumber
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsETSPortTable (INT4 i4FsETSPortNumber)
{
    tETSPortEntry      *pETSPortEntry = NULL;

    ETS_SHUTDOWN_CHECK;
    pETSPortEntry = ETSUtlGetPortEntry ((UINT4) i4FsETSPortNumber);
    if (pETSPortEntry == NULL)
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : ETSUtlGetPortEntry () "
                       "Returns FAILURE. \r\n", __FUNCTION__);
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsETSPortTable
 Input       :  The Indices
                FsETSPortNumber
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsETSPortTable (INT4 *pi4FsETSPortNumber)
{
    tETSPortEntry      *pETSPortEntry = NULL;

    ETS_SHUTDOWN_CHECK;
    pETSPortEntry = (tETSPortEntry *)
        RBTreeGetFirst (gETSGlobalInfo.pRbETSPortTbl);
    if (pETSPortEntry == NULL)
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : RbTreeGetFirst Node () "
                       "Returns NULL. \r\n", __FUNCTION__);
        return SNMP_FAILURE;
    }
    *pi4FsETSPortNumber = (INT4) pETSPortEntry->u4IfIndex;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsETSPortTable
 Input       :  The Indices
                FsETSPortNumber
                nextFsETSPortNumber
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsETSPortTable (INT4 i4FsETSPortNumber,
                               INT4 *pi4NextFsETSPortNumber)
{
    tETSPortEntry      *pETSPortEntry = NULL;
    tETSPortEntry       ETSPortEntry;

    ETS_SHUTDOWN_CHECK;

    MEMSET (&ETSPortEntry, DCBX_INIT_VAL, sizeof (tETSPortEntry));
    ETSPortEntry.u4IfIndex = (UINT4) i4FsETSPortNumber;
    pETSPortEntry = (tETSPortEntry *)
        RBTreeGetNext (gETSGlobalInfo.pRbETSPortTbl,
                       &ETSPortEntry, DcbxUtlPortTblCmpFn);
    if (pETSPortEntry == NULL)
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : RbTreeGetFirst Node () "
                       "Returns NULL. \r\n", __FUNCTION__);
        return SNMP_FAILURE;
    }

    *pi4NextFsETSPortNumber = (INT4) pETSPortEntry->u4IfIndex;
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsETSAdminMode
 Input       :  The Indices
                FsETSPortNumber

                The Object 
                retValFsETSAdminMode
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsETSAdminMode (INT4 i4FsETSPortNumber, INT4 *pi4RetValFsETSAdminMode)
{
    tETSPortEntry      *pETSPortEntry = NULL;

    pETSPortEntry = ETSUtlGetPortEntry ((UINT4) i4FsETSPortNumber);
    if (pETSPortEntry == NULL)
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : ETSUtlGetPortEntry () "
                       "Returns FAILURE. \r\n", __FUNCTION__);
        return SNMP_FAILURE;
    }

    *pi4RetValFsETSAdminMode = pETSPortEntry->u1ETSAdminMode;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsETSDcbxOperState
 Input       :  The Indices
                FsETSPortNumber

                The Object 
                retValFsETSDcbxOperState
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsETSDcbxOperState (INT4 i4FsETSPortNumber,
                          INT4 *pi4RetValFsETSDcbxOperState)
{
    tETSPortEntry      *pETSPortEntry = NULL;
    UINT1               u1DcbxVersion = DCBX_VER_UNKNOWN;

    pETSPortEntry = ETSUtlGetPortEntry ((UINT4) i4FsETSPortNumber);
    if (pETSPortEntry == NULL)
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : ETSUtlGetPortEntry () "
                       "Returned FAILURE.\r\n", __FUNCTION__);
        return SNMP_FAILURE;
    }

    DcbxUtlGetOperVersion ((UINT4) i4FsETSPortNumber, &u1DcbxVersion);
    if (u1DcbxVersion == DCBX_VER_CEE)
    {
        DCBX_GET_CEE_OPERST_FRM_IEEE (pETSPortEntry->u1ETSDcbxOperState,
                *pi4RetValFsETSDcbxOperState);
    }
    else
    {
        *pi4RetValFsETSDcbxOperState = pETSPortEntry->u1ETSDcbxOperState;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsETSDcbxStateMachine
 Input       :  The Indices
                FsETSPortNumber

                The Object 
                retValFsETSDcbxStateMachine
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsETSDcbxStateMachine (INT4 i4FsETSPortNumber,
                             INT4 *pi4RetValFsETSDcbxStateMachine)
{
    tETSPortEntry      *pETSPortEntry = NULL;

    pETSPortEntry = ETSUtlGetPortEntry ((UINT4) i4FsETSPortNumber);
    if (pETSPortEntry == NULL)
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : ETSUtlGetPortEntry () "
                       "Returns FAILURE. \r\n", __FUNCTION__);
        return SNMP_FAILURE;
    }

    *pi4RetValFsETSDcbxStateMachine = pETSPortEntry->u1ETSDcbxSemType;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsETSClearTLVCounters
 Input       :  The Indices
                FsETSPortNumber

                The Object 
                retValFsETSClearTLVCounters
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsETSClearTLVCounters (INT4 i4FsETSPortNumber,
                             INT4 *pi4RetValFsETSClearTLVCounters)
{
    tETSPortEntry      *pETSPortEntry = NULL;

    pETSPortEntry = ETSUtlGetPortEntry ((UINT4) i4FsETSPortNumber);
    if (pETSPortEntry == NULL)
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : ETSUtlGetPortEntry () "
                       "Returns FAILURE. \r\n", __FUNCTION__);
        return SNMP_FAILURE;
    }

    *pi4RetValFsETSClearTLVCounters = ETS_DISABLED;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsETSConfTxTLVCounter
 Input       :  The Indices
                FsETSPortNumber

                The Object 
                retValFsETSConfTxTLVCounter
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsETSConfTxTLVCounter (INT4 i4FsETSPortNumber,
                             UINT4 *pu4RetValFsETSConfTxTLVCounter)
{
    tETSPortEntry      *pETSPortEntry = NULL;

    pETSPortEntry = ETSUtlGetPortEntry ((UINT4) i4FsETSPortNumber);
    if (pETSPortEntry == NULL)
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : ETSUtlGetPortEntry () "
                       "Returns FAILURE. \r\n", __FUNCTION__);
        return SNMP_FAILURE;
    }

    *pu4RetValFsETSConfTxTLVCounter = pETSPortEntry->u4ETSConfTxTLVCount;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsETSConfRxTLVCounter
 Input       :  The Indices
                FsETSPortNumber

                The Object 
                retValFsETSConfRxTLVCounter
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsETSConfRxTLVCounter (INT4 i4FsETSPortNumber,
                             UINT4 *pu4RetValFsETSConfRxTLVCounter)
{
    tETSPortEntry      *pETSPortEntry = NULL;

    pETSPortEntry = ETSUtlGetPortEntry ((UINT4) i4FsETSPortNumber);
    if (pETSPortEntry == NULL)
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : ETSUtlGetPortEntry () "
                       "Returns FAILURE. \r\n", __FUNCTION__);
        return SNMP_FAILURE;
    }

    *pu4RetValFsETSConfRxTLVCounter = pETSPortEntry->u4ETSConfRxTLVCount;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsETSConfRxTLVErrors
 Input       :  The Indices
                FsETSPortNumber

                The Object 
                retValFsETSConfRxTLVErrors
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsETSConfRxTLVErrors (INT4 i4FsETSPortNumber,
                            UINT4 *pu4RetValFsETSConfRxTLVErrors)
{
    tETSPortEntry      *pETSPortEntry = NULL;

    pETSPortEntry = ETSUtlGetPortEntry ((UINT4) i4FsETSPortNumber);
    if (pETSPortEntry == NULL)
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : ETSUtlGetPortEntry () "
                       "Returns FAILURE. \r\n", __FUNCTION__);
        return SNMP_FAILURE;
    }

    *pu4RetValFsETSConfRxTLVErrors = pETSPortEntry->u4ETSConfRxTLVError;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsETSRecoTxTLVCounter
 Input       :  The Indices
                FsETSPortNumber

                The Object 
                retValFsETSRecoTxTLVCounter
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsETSRecoTxTLVCounter (INT4 i4FsETSPortNumber,
                             UINT4 *pu4RetValFsETSRecoTxTLVCounter)
{
    tETSPortEntry      *pETSPortEntry = NULL;

    pETSPortEntry = ETSUtlGetPortEntry ((UINT4) i4FsETSPortNumber);
    if (pETSPortEntry == NULL)
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : ETSUtlGetPortEntry () "
                       "Returns FAILURE. \r\n", __FUNCTION__);
        return SNMP_FAILURE;
    }

    *pu4RetValFsETSRecoTxTLVCounter = pETSPortEntry->u4ETSRecoTxTLVCount;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsETSRecoRxTLVCounter
 Input       :  The Indices
                FsETSPortNumber

                The Object 
                retValFsETSRecoRxTLVCounter
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsETSRecoRxTLVCounter (INT4 i4FsETSPortNumber,
                             UINT4 *pu4RetValFsETSRecoRxTLVCounter)
{
    tETSPortEntry      *pETSPortEntry = NULL;

    pETSPortEntry = ETSUtlGetPortEntry ((UINT4) i4FsETSPortNumber);
    if (pETSPortEntry == NULL)
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : ETSUtlGetPortEntry () "
                       "Returns FAILURE. \r\n", __FUNCTION__);
        return SNMP_FAILURE;
    }

    *pu4RetValFsETSRecoRxTLVCounter = pETSPortEntry->u4ETSRecoRxTLVCount;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsETSRecoRxTLVErrors
 Input       :  The Indices
                FsETSPortNumber

                The Object 
                retValFsETSRecoRxTLVErrors
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsETSRecoRxTLVErrors (INT4 i4FsETSPortNumber,
                            UINT4 *pu4RetValFsETSRecoRxTLVErrors)
{
    tETSPortEntry      *pETSPortEntry = NULL;

    pETSPortEntry = ETSUtlGetPortEntry ((UINT4) i4FsETSPortNumber);
    if (pETSPortEntry == NULL)
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : ETSUtlGetPortEntry () "
                       "Returns FAILURE. \r\n", __FUNCTION__);
        return SNMP_FAILURE;
    }

    *pu4RetValFsETSRecoRxTLVErrors = pETSPortEntry->u4ETSRecoRxTLVError;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsETSTcSuppTxTLVCounter
 Input       :  The Indices
                FsETSPortNumber

                The Object 
                retValFsETSTcSuppTxTLVCounter
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsETSTcSuppTxTLVCounter (INT4 i4FsETSPortNumber,
                               UINT4 *pu4RetValFsETSTcSuppTxTLVCounter)
{
    tETSPortEntry      *pETSPortEntry = NULL;

    pETSPortEntry = ETSUtlGetPortEntry ((UINT4) i4FsETSPortNumber);
    if (pETSPortEntry == NULL)
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : ETSUtlGetPortEntry () "
                       "Returns FAILURE. \r\n", __FUNCTION__);
        return SNMP_FAILURE;
    }

    *pu4RetValFsETSTcSuppTxTLVCounter = pETSPortEntry->u4ETSTcSuppTxTLVCount;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsETSTcSuppRxTLVCounter
 Input       :  The Indices
                FsETSPortNumber

                The Object 
                retValFsETSTcSuppRxTLVCounter
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsETSTcSuppRxTLVCounter (INT4 i4FsETSPortNumber,
                               UINT4 *pu4RetValFsETSTcSuppRxTLVCounter)
{
    tETSPortEntry      *pETSPortEntry = NULL;

    pETSPortEntry = ETSUtlGetPortEntry ((UINT4) i4FsETSPortNumber);
    if (pETSPortEntry == NULL)
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : ETSUtlGetPortEntry () "
                       "Returns FAILURE. \r\n", __FUNCTION__);
        return SNMP_FAILURE;
    }

    *pu4RetValFsETSTcSuppRxTLVCounter = pETSPortEntry->u4ETSTcSuppRxTLVCount;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsETSTcSuppRxTLVErrors
 Input       :  The Indices
                FsETSPortNumber

                The Object 
                retValFsETSTcSuppRxTLVErrors
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsETSTcSuppRxTLVErrors (INT4 i4FsETSPortNumber,
                              UINT4 *pu4RetValFsETSTcSuppRxTLVErrors)
{
    tETSPortEntry      *pETSPortEntry = NULL;

    pETSPortEntry = ETSUtlGetPortEntry ((UINT4) i4FsETSPortNumber);
    if (pETSPortEntry == NULL)
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : ETSUtlGetPortEntry () "
                       "Returns FAILURE. \r\n", __FUNCTION__);
        return SNMP_FAILURE;
    }

    *pu4RetValFsETSTcSuppRxTLVErrors = pETSPortEntry->u4ETSTcSupRxTLVError;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsETSRowStatus
 Input       :  The Indices
                FsETSPortNumber

                The Object 
                retValFsETSRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsETSRowStatus (INT4 i4FsETSPortNumber, INT4 *pi4RetValFsETSRowStatus)
{
    tETSPortEntry      *pETSPortEntry = NULL;

    pETSPortEntry = ETSUtlGetPortEntry ((UINT4) i4FsETSPortNumber);
    if (pETSPortEntry == NULL)
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : ETSUtlGetPortEntry () "
                       "Returns FAILURE. \r\n", __FUNCTION__);
        return SNMP_FAILURE;
    }

    *pi4RetValFsETSRowStatus = pETSPortEntry->u1ETSRowStatus;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsETSSyncd
 Input       :  The Indices
                FsETSPortNumber

                The Object 
                retValFsETSSyncd
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsETSSyncd(INT4 i4FsETSPortNumber , INT4 *pi4RetValFsETSSyncd)
{
    tETSPortEntry      *pETSPortEntry = NULL;

    pETSPortEntry = ETSUtlGetPortEntry ((UINT4) i4FsETSPortNumber);
    if (pETSPortEntry == NULL)
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : ETSUtlGetPortEntry () "
                       "Returns FAILURE. \r\n", __FUNCTION__);
        return SNMP_FAILURE;
    }

    *pi4RetValFsETSSyncd = pETSPortEntry->bETSSyncd;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsETSError
 Input       :  The Indices
                FsETSPortNumber

                The Object 
                retValFsETSError
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsETSError(INT4 i4FsETSPortNumber , INT4 *pi4RetValFsETSError)
{
    tETSPortEntry      *pETSPortEntry = NULL;

    pETSPortEntry = ETSUtlGetPortEntry ((UINT4) i4FsETSPortNumber);
    if (pETSPortEntry == NULL)
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : ETSUtlGetPortEntry () "
                       "Returns FAILURE. \r\n", __FUNCTION__);
        return SNMP_FAILURE;
    }

    *pi4RetValFsETSError = pETSPortEntry->bETSError;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsETSDcbxStatus
 Input       :  The Indices
                FsETSPortNumber

                The Object 
                retValFsETSDcbxStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsETSDcbxStatus(INT4 i4FsETSPortNumber , INT4 *pi4RetValFsETSDcbxStatus)
{
    tETSPortEntry      *pETSPortEntry = NULL;

    pETSPortEntry = ETSUtlGetPortEntry ((UINT4) i4FsETSPortNumber);
    if (pETSPortEntry == NULL)
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : ETSUtlGetPortEntry () "
                       "Returns FAILURE. \r\n", __FUNCTION__);
        return SNMP_FAILURE;
    }

    *pi4RetValFsETSDcbxStatus = pETSPortEntry->u1DcbxStatus;
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsETSAdminMode
 Input       :  The Indices
                FsETSPortNumber

                The Object 
                setValFsETSAdminMode
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsETSAdminMode (INT4 i4FsETSPortNumber, INT4 i4SetValFsETSAdminMode)
{
    tETSPortEntry      *pETSPortEntry = NULL;

    DCBX_TRC_ARG3 (DCBX_MGMT_TRC, "%s, Args: "
            "i4FsETSPortNumber: %d i4SetValFsETSAdminMode: %d\r\n", 
            __func__, i4FsETSPortNumber, i4SetValFsETSAdminMode);

    pETSPortEntry = ETSUtlGetPortEntry ((UINT4) i4FsETSPortNumber);
    if (pETSPortEntry == NULL)
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : ETSUtlGetPortEntry () "
                       "Returns FAILURE. \r\n", __FUNCTION__);
        return SNMP_FAILURE;
    }
    if (pETSPortEntry->u1ETSAdminMode != (UINT1) i4SetValFsETSAdminMode)
    {

        ETSUtlAdminModeChange (pETSPortEntry, (UINT1) i4SetValFsETSAdminMode);
        /* Updation of Admin mode data structure will be taken care
         * inside the function */
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsETSClearTLVCounters
 Input       :  The Indices
                FsETSPortNumber

                The Object 
                setValFsETSClearTLVCounters
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsETSClearTLVCounters (INT4 i4FsETSPortNumber,
                             INT4 i4SetValFsETSClearTLVCounters)
{
    tETSPortEntry      *pETSPortEntry = NULL;

    DCBX_TRC_ARG3 (DCBX_MGMT_TRC, "%s, Args: "
            "i4FsETSPortNumber: %d "
            "i4SetValFsETSClearTLVCounters: %d\r\n", 
            __func__, i4FsETSPortNumber, i4SetValFsETSClearTLVCounters);

    pETSPortEntry = ETSUtlGetPortEntry ((UINT4) i4FsETSPortNumber);
    if (pETSPortEntry == NULL)
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : ETSUtlGetPortEntry () "
                       "Returns FAILURE. \r\n", __FUNCTION__);
        return SNMP_FAILURE;
    }
    if (i4SetValFsETSClearTLVCounters == ETS_ENABLED)
    {
        pETSPortEntry->u4ETSTcSuppTxTLVCount = DCBX_ZERO;
        pETSPortEntry->u4ETSTcSuppRxTLVCount = DCBX_ZERO;
        pETSPortEntry->u4ETSTcSupRxTLVError = DCBX_ZERO;
        pETSPortEntry->u4ETSRecoTxTLVCount = DCBX_ZERO;
        pETSPortEntry->u4ETSRecoRxTLVCount = DCBX_ZERO;
        pETSPortEntry->u4ETSRecoRxTLVError = DCBX_ZERO;
        pETSPortEntry->u4ETSConfTxTLVCount = DCBX_ZERO;
        pETSPortEntry->u4ETSConfRxTLVCount = DCBX_ZERO;
        pETSPortEntry->u4ETSConfRxTLVError = DCBX_ZERO;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhSetFsETSRowStatus
Input       :  The Indices
FsETSPortNumber

The Object 
setValFsETSRowStatus
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsETSRowStatus (INT4 i4FsETSPortNumber, INT4 i4SetValFsETSRowStatus)
{
    tETSPortEntry      *pETSPortEntry = NULL;
    tCEECtrlEntry      *pCEECtrlEntry = NULL;
    tDcbxPortEntry     *pDcbxPortEntry = NULL;
    INT4                i4RetVal = OSIX_SUCCESS;

    DCBX_TRC_ARG3 (DCBX_MGMT_TRC, "%s, Args: "
            "i4FsETSPortNumber: %d "
            "i4SetValFsETSRowStatus: %d\r\n", 
            __func__, i4FsETSPortNumber, i4SetValFsETSRowStatus);

    pDcbxPortEntry = DcbxUtlGetPortEntry ((UINT4) i4FsETSPortNumber);
    if (pDcbxPortEntry == NULL)
    {
        DCBX_TRC_ARG2 (MGMT_TRC | ALL_FAILURE_TRC, "%s : "
                "pDcbxPortEntry (Port) Id %d is invalid \r\n",
                __func__, i4FsETSPortNumber);
        return SNMP_FAILURE;
    }

    switch (i4SetValFsETSRowStatus)
    {
        case CREATE_AND_WAIT:    /* Intentional fall through for MSR */
        case CREATE_AND_GO:

            /* Memblock creation and initializing the default values
             * to the ETS Node for this port  are handled inside the below
             * function and  rowstatus is initialized here */
            pETSPortEntry =
                ETSUtlCreatePortTblEntry ((UINT4) i4FsETSPortNumber);
            if (pETSPortEntry == NULL)
            {
                DCBX_TRC_ARG1 (DCBX_MGMT_TRC,
                               "In %s : ETSUtlCreatePortTblEntry () "
                               "Returns FAILURE. \r\n", __FUNCTION__);
                return SNMP_FAILURE;
            }
            pCEECtrlEntry =
                CEEUtlCreateCtrlTblEntry ((UINT4) i4FsETSPortNumber);
            if (pCEECtrlEntry == NULL)
            {
                DCBX_TRC_ARG1 (DCBX_MGMT_TRC | DCBX_FAILURE_TRC,
                               "In %s : CEEUtlCreateCtrlTblEntry () "
                               "Returns FAILURE. \r\n", __FUNCTION__);
                return SNMP_FAILURE;
            }
            pDcbxPortEntry->u1ETSAdminStatus = ETS_ENABLED;
            pETSPortEntry->u1ETSRowStatus = ACTIVE;
            break;

        case DESTROY:
            pETSPortEntry = ETSUtlGetPortEntry ((UINT4) i4FsETSPortNumber);
            if (pETSPortEntry == NULL)
            {
                return SNMP_SUCCESS;
            }
            i4RetVal = ETSUtlDeletePortTblEntry (pETSPortEntry);
            if (i4RetVal != OSIX_SUCCESS)
            {
                return SNMP_FAILURE;
            }
            pCEECtrlEntry =
                CEEUtlGetCtrlEntry ((UINT4) i4FsETSPortNumber);
            if (pCEECtrlEntry == NULL)
            {
                return SNMP_SUCCESS;
            }
            i4RetVal = CEEUtlDeleteCtrlTblEntry (pCEECtrlEntry);
            if (i4RetVal != OSIX_SUCCESS)
            {
                DCBX_TRC_ARG1 (DCBX_MGMT_TRC | DCBX_FAILURE_TRC,
                               "In %s : CEEUtlDeleteCtrlTblEntry () "
                               "Returns FAILURE. \r\n", __FUNCTION__);
                return SNMP_FAILURE;
            }
            pDcbxPortEntry->u1ETSAdminStatus = ETS_DISABLED;
            break;
        case ACTIVE:
            break;
        default:
            return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsETSAdminMode
 Input       :  The Indices
                FsETSPortNumber

                The Object 
                testValFsETSAdminMode
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsETSAdminMode (UINT4 *pu4ErrorCode,
                         INT4 i4FsETSPortNumber, INT4 i4TestValFsETSAdminMode)
{
    tETSPortEntry      *pETSPortEntry = NULL;

    ETS_SHUTDOWN_CHECK_TEST;

    if (DcbxUtlValidatePort ((UINT4) i4FsETSPortNumber) == OSIX_FAILURE)
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : DcbxUtlValidatePort() "
                       "Returns Failure. \r\n", __FUNCTION__);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_ERR_DCBX_NO_PORT);
        return SNMP_FAILURE;
    }

    pETSPortEntry = ETSUtlGetPortEntry ((UINT4) i4FsETSPortNumber);
    if (pETSPortEntry == NULL)
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : ETSUtlGetPortEntry () "
                       "Returns FAILURE. \r\n", __FUNCTION__);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_ERR_ETS_NO_PORT_ENTRY);
        return SNMP_FAILURE;
    }

    if ((i4TestValFsETSAdminMode < ETS_PORT_MODE_AUTO) ||
        (i4TestValFsETSAdminMode > ETS_PORT_MODE_OFF))
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : Wrong AdminMode() "
                       "Returns Failure. \r\n", __FUNCTION__);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_ERR_ETS_INVALID_PORT_MODE_STATUS);
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsETSClearTLVCounters
 Input       :  The Indices
                FsETSPortNumber

                The Object 
                testValFsETSClearTLVCounters
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsETSClearTLVCounters (UINT4 *pu4ErrorCode,
                                INT4 i4FsETSPortNumber,
                                INT4 i4TestValFsETSClearTLVCounters)
{
    tETSPortEntry      *pETSPortEntry = NULL;
    if (DcbxUtlValidatePort ((UINT4) i4FsETSPortNumber) == OSIX_FAILURE)
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : DcbxUtlValidatePort() "
                       "Returns Failure. \r\n", __FUNCTION__);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_ERR_DCBX_NO_PORT);
        return SNMP_FAILURE;
    }

    ETS_SHUTDOWN_CHECK_TEST;

    pETSPortEntry = ETSUtlGetPortEntry ((UINT4) i4FsETSPortNumber);
    if (pETSPortEntry == NULL)
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : ETSUtlGetPortEntry () "
                       "Returns FAILURE. \r\n", __FUNCTION__);

        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_ERR_ETS_NO_PORT_ENTRY);
        return SNMP_FAILURE;
    }

    if ((i4TestValFsETSClearTLVCounters != ETS_DISABLED) &&
        (i4TestValFsETSClearTLVCounters != ETS_ENABLED))
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : Wrong Setting() "
                       "Returns Failure. \r\n", __FUNCTION__);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_ERR_ETS_INVALID_CLEAR_STATUS);
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsETSRowStatus
 Input       :  The Indices
                FsETSPortNumber

                The Object 
                testValFsETSRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsETSRowStatus (UINT4 *pu4ErrorCode,
                         INT4 i4FsETSPortNumber, INT4 i4TestValFsETSRowStatus)
{
    tETSPortEntry      *pETSPortEntry = NULL;

    if (DcbxUtlValidatePort ((UINT4) i4FsETSPortNumber) == OSIX_FAILURE)
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : DcbxUtlValidatePort() "
                       "Returns Failure. \r\n", __FUNCTION__);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        /* Currently, DCBX shouldn't be allowed in 
           Port-Channel Interface, setting appropriate error code 
        */
        if (CfaIsLaggInterface ((UINT4)i4FsETSPortNumber) == CFA_TRUE) 
        {
          CLI_SET_ERR (CLI_ERR_DCBX_PORT_IN_PORT_CHANNEL);
        }
        else
        {
          CLI_SET_ERR (CLI_ERR_DCBX_NO_PORT);
        }
        return SNMP_FAILURE;
    }

    ETS_SHUTDOWN_CHECK_TEST;

    pETSPortEntry = ETSUtlGetPortEntry ((UINT4) i4FsETSPortNumber);
    switch (i4TestValFsETSRowStatus)
    {
        case CREATE_AND_GO:

            /* If ETS Node is present for this port and
             * trying to create the same entry then
             * return failure */
            if (pETSPortEntry != NULL)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                CLI_SET_ERR (CLI_ERR_ETS_PORT_ALREADY_PRESENT);
                return (SNMP_FAILURE);
            }
            if ((MemGetFreeUnits (gETSGlobalInfo.ETSPortPoolId)) ==
                DCBX_INIT_VAL)
            {
                *pu4ErrorCode = SNMP_ERR_NO_CREATION;
                CLI_SET_ERR (CLI_ERR_ETS_PORT_CREATION_FAILS);
                return SNMP_FAILURE;
            }
            break;

        case DESTROY:
            if (pETSPortEntry == NULL)
            {
                DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : ETSUtlGetPortEntry () "
                               "Returns FAILURE. \r\n", __FUNCTION__);
                *pu4ErrorCode = SNMP_ERR_NO_CREATION;
                CLI_SET_ERR (CLI_ERR_ETS_NO_PORT_ENTRY);
                return SNMP_FAILURE;
            }
            break;

        case ACTIVE:
            break;

        default:
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsETSPortTable
 Input       :  The Indices
                FsETSPortNumber
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsETSPortTable (UINT4 *pu4ErrorCode,
                        tSnmpIndexList * pSnmpIndexList,
                        tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsPFCSystemControl
 Input       :  The Indices

                The Object 
                retValFsPFCSystemControl
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPFCSystemControl (INT4 *pi4RetValFsPFCSystemControl)
{
    *pi4RetValFsPFCSystemControl = gPFCGlobalInfo.u1PFCSystemCtrl;
    DCBX_TRC_ARG2 (DCBX_MGMT_TRC, "%s : PFC System Control = %d SUCCESS.\r\n",
                   __FUNCTION__, *pi4RetValFsPFCSystemControl);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsPFCModuleStatus
 Input       :  The Indices

                The Object 
                retValFsPFCModuleStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPFCModuleStatus (INT4 *pi4RetValFsPFCModuleStatus)
{
    *pi4RetValFsPFCModuleStatus = gPFCGlobalInfo.u1PFCModStatus;
    DCBX_TRC_ARG2 (DCBX_MGMT_TRC, "%s : PFC Module Status = %d SUCCESS.\r\n",
                   __FUNCTION__, *pi4RetValFsPFCModuleStatus);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsPFCClearCounters
 Input       :  The Indices

                The Object 
                retValFsPFCClearCounters
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPFCClearCounters (INT4 *pi4RetValFsPFCClearCounters)
{
    *pi4RetValFsPFCClearCounters = PFC_DISABLED;
    DCBX_TRC_ARG2 (DCBX_MGMT_TRC, "%s : PFC Clear Counter = %d SUCCESS.\r\n",
                   __FUNCTION__, *pi4RetValFsPFCClearCounters);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsPFCGlobalEnableTrap
 Input       :  The Indices

                The Object 
                retValFsPFCGlobalEnableTrap
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPFCGlobalEnableTrap (INT4 *pi4RetValFsPFCGlobalEnableTrap)
{
    *pi4RetValFsPFCGlobalEnableTrap = (INT4) gPFCGlobalInfo.u4PFCTrap;
    DCBX_TRC_ARG2 (DCBX_MGMT_TRC, "%s : PFC Trap = %d SUCCESS.\r\n",
                   __FUNCTION__, *pi4RetValFsPFCGlobalEnableTrap);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsPFCGeneratedTrapCount
 Input       :  The Indices
                The Object
                retValFsPFCGeneratedTrapCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPFCGeneratedTrapCount (UINT4 *pu4RetValFsPFCGeneratedTrapCount)
{
    *pu4RetValFsPFCGeneratedTrapCount = gPFCGlobalInfo.u4PFCGeneratedTrapCount;
    DCBX_TRC_ARG2 (DCBX_MGMT_TRC, "%s : PFC Trap Count= %d SUCCESS.\r\n",
                   __FUNCTION__, *pu4RetValFsPFCGeneratedTrapCount);
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */
/****************************************************************************
 Function    :  nmhSetFsPFCSystemControl
 Input       :  The Indices

                The Object 
                setValFsPFCSystemControl
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsPFCSystemControl (INT4 i4SetValFsPFCSystemControl)
{
    DCBX_TRC_ARG2 (DCBX_MGMT_TRC, "%s, Args: "
            "i4SetValFsPFCSystemControl: %d\r\n", 
            __func__, i4SetValFsPFCSystemControl);

    if (gPFCGlobalInfo.u1PFCSystemCtrl != (UINT1) i4SetValFsPFCSystemControl)
    {

        if (i4SetValFsPFCSystemControl == PFC_START)
        {
            if (PFCMainModuleStart () == OSIX_FAILURE)
            {
                return SNMP_FAILURE;
            }
        }
        else
        {
            PFCMainModuleShutDown ();
            PFCNotifyShutdownStatus ();
        }
        gPFCGlobalInfo.u1PFCSystemCtrl = (UINT1) i4SetValFsPFCSystemControl;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsPFCModuleStatus
 Input       :  The Indices

                The Object 
                setValFsPFCModuleStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsPFCModuleStatus (INT4 i4SetValFsPFCModuleStatus)
{
    DCBX_TRC_ARG2 (DCBX_MGMT_TRC, "%s, Args: "
            "i4SetValFsPFCModuleStatus: %d\r\n", 
            __func__, i4SetValFsPFCModuleStatus);

    if (gPFCGlobalInfo.u1PFCModStatus != (UINT1) i4SetValFsPFCModuleStatus)
    {

        PFCUtlModuleStatusChange ((UINT1) i4SetValFsPFCModuleStatus);
        /* Updation of Module status data strucutre will be taken care
         * inside the Status change function */
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsPFCClearCounters
 Input       :  The Indices

                The Object 
                setValFsPFCClearCounters
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsPFCClearCounters (INT4 i4SetValFsPFCClearCounters)
{

    tPFCPortEntry      *pPFCPortEntry = NULL;
    INT4                i4PFCPortNumber = DCBX_ZERO;
    INT4                i4NextPFCPortNumber = DCBX_ZERO;
    INT4                i4RetVal = SNMP_SUCCESS;

    DCBX_TRC_ARG2 (DCBX_MGMT_TRC, "%s, Args: "
            "i4SetValFsPFCClearCounters: %d\r\n", 
            __func__, i4SetValFsPFCClearCounters);

    if (i4SetValFsPFCClearCounters == PFC_ENABLED)
    {
        i4RetVal = nmhGetFirstIndexFsPFCPortTable (&i4NextPFCPortNumber);
        if (i4RetVal == SNMP_FAILURE)
        {
            DCBX_TRC_ARG1 (DCBX_MGMT_TRC,
                           "In %s : nmhGetFirstIndexFsPFCPortTable () "
                           "No PFC Entries is present to clear . \r\n",
                           __FUNCTION__);
            return SNMP_SUCCESS;
        }

        do
        {
            i4PFCPortNumber = i4NextPFCPortNumber;
            pPFCPortEntry = PFCUtlGetPortEntry ((UINT4) i4PFCPortNumber);
            if (pPFCPortEntry == NULL)
            {
                DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : PFCUtlGetPortEntry () "
                               "Returns FAILURE. \r\n", __FUNCTION__);
                return SNMP_FAILURE;
            }
            pPFCPortEntry->u4PFCTxTLVCount = DCBX_ZERO;
            pPFCPortEntry->u4PFCRxTLVError = DCBX_ZERO;
            pPFCPortEntry->u4PFCRxTLVCount = DCBX_ZERO;
        }
        while (nmhGetNextIndexFsPFCPortTable
               (i4PFCPortNumber, &i4NextPFCPortNumber) != SNMP_FAILURE);
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsPFCGlobalEnableTrap
 Input       :  The Indices

                The Object 
                setValFsPFCGlobalEnableTrap
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsPFCGlobalEnableTrap (INT4 i4SetValFsPFCGlobalEnableTrap)
{
    DCBX_TRC_ARG2 (DCBX_MGMT_TRC, "%s, Args: "
            "i4SetValFsPFCGlobalEnableTrap: %d\r\n", 
            __func__, i4SetValFsPFCGlobalEnableTrap);

    gPFCGlobalInfo.u4PFCTrap = (UINT4) i4SetValFsPFCGlobalEnableTrap;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsPFCGeneratedTrapCount
 Input       :  The Indices
                The Object
                setValFsPFCGeneratedTrapCount
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsPFCGeneratedTrapCount (UINT4 u4SetValFsPFCGeneratedTrapCount)
{
    DCBX_TRC_ARG2 (DCBX_MGMT_TRC, "%s, Args: "
            "u4SetValFsPFCGeneratedTrapCount: %d\r\n", 
            __func__, u4SetValFsPFCGeneratedTrapCount);

    gPFCGlobalInfo.u4PFCGeneratedTrapCount = u4SetValFsPFCGeneratedTrapCount;
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsPFCSystemControl
 Input       :  The Indices

                The Object 
                testValFsPFCSystemControl
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsPFCSystemControl (UINT4 *pu4ErrorCode,
                             INT4 i4TestValFsPFCSystemControl)
{
    if ((i4TestValFsPFCSystemControl < PFC_START) ||
        (i4TestValFsPFCSystemControl > PFC_SHUTDOWN))
    {
        DCBX_TRC_ARG2 (DCBX_MGMT_TRC, "%s : PFC SYSTEM CONTROL%d FAILED.\r\n",
                       __FUNCTION__, i4TestValFsPFCSystemControl);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_ERR_PFC_INVALID_SYS_CONTROL);
        return (SNMP_FAILURE);
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsPFCModuleStatus
 Input       :  The Indices

                The Object 
                testValFsPFCModuleStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsPFCModuleStatus (UINT4 *pu4ErrorCode,
                            INT4 i4TestValFsPFCModuleStatus)
{
    PFC_SHUTDOWN_CHECK_TEST;

    if ((i4TestValFsPFCModuleStatus != PFC_ENABLED) &&
        (i4TestValFsPFCModuleStatus != PFC_DISABLED))
    {
        DCBX_TRC_ARG2 (DCBX_MGMT_TRC, "%s : PFC SYSTEM CONTROL%d FAILED.\r\n",
                       __FUNCTION__, i4TestValFsPFCModuleStatus);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_ERR_PFC_INVALID_SYS_STATUS);
        return (SNMP_FAILURE);
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsPFCClearCounters
 Input       :  The Indices

                The Object 
                testValFsPFCClearCounters
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsPFCClearCounters (UINT4 *pu4ErrorCode,
                             INT4 i4TestValFsPFCClearCounters)
{

    PFC_SHUTDOWN_CHECK_TEST;
    if ((i4TestValFsPFCClearCounters != PFC_DISABLED) &&
        (i4TestValFsPFCClearCounters != PFC_ENABLED))
    {
        DCBX_TRC_ARG2 (DCBX_MGMT_TRC,
                       "%s : PFC Clear Counter Status%d FAILED.\r\n",
                       __FUNCTION__, i4TestValFsPFCClearCounters);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_ERR_PFC_INVALID_CLEAR_STATUS);
        return (SNMP_FAILURE);
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsPFCGlobalEnableTrap
 Input       :  The Indices

                The Object 
                testValFsPFCGlobalEnableTrap
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsPFCGlobalEnableTrap (UINT4 *pu4ErrorCode,
                                INT4 i4TestValFsPFCGlobalEnableTrap)
{

    PFC_SHUTDOWN_CHECK_TEST;
    if (i4TestValFsPFCGlobalEnableTrap & (~(PFC_ALL_TRAP)))
    {
        DCBX_TRC_ARG2 (DCBX_MGMT_TRC,
                       "%s : PFC Clear Counter Status%d FAILED.\r\n",
                       __FUNCTION__, i4TestValFsPFCGlobalEnableTrap);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_ERR_PFC_INVALID_NOTIFY);
        return (SNMP_FAILURE);
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsPFCGeneratedTrapCount
 Input       :  The Indices
                The Object
                testValFsPFCGeneratedTrapCount
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsPFCGeneratedTrapCount (UINT4 *pu4ErrorCode,
                                  UINT4 u4TestValFsPFCGeneratedTrapCount)
{
    PFC_SHUTDOWN_CHECK_TEST;

    if (u4TestValFsPFCGeneratedTrapCount != 0)
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC,
                       "%s : Values other than zero are not allowed "
                       "for this object", __FUNCTION__);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_ERR_PFC_INVALID_CLEAR_TRAP);
        return (SNMP_FAILURE);
    }

    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsPFCSystemControl
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhDepv2FsPFCSystemControl (UINT4 *pu4ErrorCode,
                            tSnmpIndexList * pSnmpIndexList,
                            tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsPFCModuleStatus
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsPFCModuleStatus (UINT4 *pu4ErrorCode,
                           tSnmpIndexList * pSnmpIndexList,
                           tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsPFCClearCounters
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsPFCClearCounters (UINT4 *pu4ErrorCode,
                            tSnmpIndexList * pSnmpIndexList,
                            tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsPFCGlobalEnableTrap
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsPFCGlobalEnableTrap (UINT4 *pu4ErrorCode,
                               tSnmpIndexList * pSnmpIndexList,
                               tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsPFCGeneratedTrapCount
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsPFCGeneratedTrapCount (UINT4 *pu4ErrorCode,
                                 tSnmpIndexList * pSnmpIndexList,
                                 tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsPFCPortTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsPFCPortTable
 Input       :  The Indices
                FsPFCPortNumber
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsPFCPortTable (INT4 i4FsPFCPortNumber)
{
    tPFCPortEntry      *pPFCPortEntry = NULL;

    PFC_SHUTDOWN_CHECK;

    pPFCPortEntry = PFCUtlGetPortEntry ((UINT4) i4FsPFCPortNumber);
    if (pPFCPortEntry == NULL)
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : PFCUtlGetPortEntry () "
                       "Returned FAILURE. \r\n", __FUNCTION__);
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsPFCPortTable
 Input       :  The Indices
                FsPFCPortNumber
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsPFCPortTable (INT4 *pi4FsPFCPortNumber)
{
    tPFCPortEntry      *pPFCPortEntry = NULL;

    PFC_SHUTDOWN_CHECK;
    pPFCPortEntry = (tPFCPortEntry *)
        RBTreeGetFirst (gPFCGlobalInfo.pRbPFCPortTbl);
    if (pPFCPortEntry == NULL)
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : RbTreeGetFirst Node () "
                       "Returned NULL. \r\n", __FUNCTION__);
        return SNMP_FAILURE;
    }
    *pi4FsPFCPortNumber = (INT4) pPFCPortEntry->u4IfIndex;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsPFCPortTable
 Input       :  The Indices
                FsPFCPortNumber
                nextFsPFCPortNumber
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsPFCPortTable (INT4 i4FsPFCPortNumber,
                               INT4 *pi4NextFsPFCPortNumber)
{
    tPFCPortEntry      *pPFCPortEntry = NULL;
    tPFCPortEntry       PFCPortEntry;

    PFC_SHUTDOWN_CHECK;

    MEMSET (&PFCPortEntry, DCBX_INIT_VAL, sizeof (tPFCPortEntry));
    PFCPortEntry.u4IfIndex = (UINT4) i4FsPFCPortNumber;
    pPFCPortEntry = (tPFCPortEntry *)
        RBTreeGetNext (gPFCGlobalInfo.pRbPFCPortTbl,
                       &PFCPortEntry, DcbxUtlPortTblCmpFn);
    if (pPFCPortEntry == NULL)
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : RbTreeGetFirst Node () "
                       "Returned NULL. \r\n", __FUNCTION__);
        return SNMP_FAILURE;
    }

    *pi4NextFsPFCPortNumber = (INT4) pPFCPortEntry->u4IfIndex;
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsPFCAdminMode
 Input       :  The Indices
                FsPFCPortNumber

                The Object 
                retValFsPFCAdminMode
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPFCAdminMode (INT4 i4FsPFCPortNumber, INT4 *pi4RetValFsPFCAdminMode)
{
    tPFCPortEntry      *pPFCPortEntry = NULL;

    pPFCPortEntry = PFCUtlGetPortEntry ((UINT4) i4FsPFCPortNumber);
    if (pPFCPortEntry == NULL)
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : PFCUtlGetPortEntry () "
                       "Returned FAILURE. \r\n", __FUNCTION__);
        return SNMP_FAILURE;
    }

    *pi4RetValFsPFCAdminMode = pPFCPortEntry->u1PFCAdminMode;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsPFCDcbxOperState
 Input       :  The Indices
                FsPFCPortNumber

                The Object 
                retValFsPFCDcbxOperState
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPFCDcbxOperState (INT4 i4FsPFCPortNumber,
                          INT4 *pi4RetValFsPFCDcbxOperState)
{
    tPFCPortEntry      *pPFCPortEntry = NULL;
    UINT1               u1DcbxVersion = DCBX_VER_UNKNOWN;

    pPFCPortEntry = PFCUtlGetPortEntry ((UINT4) i4FsPFCPortNumber);
    if (pPFCPortEntry == NULL)
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : PFCUtlGetPortEntry () "
                       "Returned FAILURE. \r\n", __FUNCTION__);
        return SNMP_FAILURE;
    }

    DcbxUtlGetOperVersion ((UINT4) i4FsPFCPortNumber, &u1DcbxVersion);
    if (u1DcbxVersion == DCBX_VER_CEE)
    {
        DCBX_GET_CEE_OPERST_FRM_IEEE(pPFCPortEntry->u1PFCDcbxOperState,
                *pi4RetValFsPFCDcbxOperState);
    }
    else
    {
        *pi4RetValFsPFCDcbxOperState = pPFCPortEntry->u1PFCDcbxOperState;
    }
    
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsPFCDcbxStateMachine
 Input       :  The Indices
                FsPFCPortNumber

                The Object 
                retValFsPFCDcbxStateMachine
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPFCDcbxStateMachine (INT4 i4FsPFCPortNumber,
                             INT4 *pi4RetValFsPFCDcbxStateMachine)
{
    tPFCPortEntry      *pPFCPortEntry = NULL;

    pPFCPortEntry = PFCUtlGetPortEntry ((UINT4) i4FsPFCPortNumber);
    if (pPFCPortEntry == NULL)
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : PFCUtlGetPortEntry () "
                       "Returned FAILURE. \r\n", __FUNCTION__);
        return SNMP_FAILURE;
    }

    *pi4RetValFsPFCDcbxStateMachine = pPFCPortEntry->u1PFCDcbxSemType;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsPFCClearTLVCounters
 Input       :  The Indices
                FsPFCPortNumber

                The Object 
                retValFsPFCClearTLVCounters
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPFCClearTLVCounters (INT4 i4FsPFCPortNumber,
                             INT4 *pi4RetValFsPFCClearTLVCounters)
{
    tPFCPortEntry      *pPFCPortEntry = NULL;

    pPFCPortEntry = PFCUtlGetPortEntry ((UINT4) i4FsPFCPortNumber);
    if (pPFCPortEntry == NULL)
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : PFCUtlGetPortEntry () "
                       "Returned FAILURE. \r\n", __FUNCTION__);
        return SNMP_FAILURE;
    }

    *pi4RetValFsPFCClearTLVCounters = PFC_DISABLED;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsPFCTxTLVCounter
 Input       :  The Indices
                FsPFCPortNumber

                The Object 
                retValFsPFCTxTLVCounter
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPFCTxTLVCounter (INT4 i4FsPFCPortNumber,
                         UINT4 *pu4RetValFsPFCTxTLVCounter)
{
    tPFCPortEntry      *pPFCPortEntry = NULL;

    pPFCPortEntry = PFCUtlGetPortEntry ((UINT4) i4FsPFCPortNumber);
    if (pPFCPortEntry == NULL)
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : PFCUtlGetPortEntry () "
                       "Returned FAILURE. \r\n", __FUNCTION__);
        return SNMP_FAILURE;
    }

    *pu4RetValFsPFCTxTLVCounter = pPFCPortEntry->u4PFCTxTLVCount;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsPFCRxTLVCounter
 Input       :  The Indices
                FsPFCPortNumber

                The Object 
                retValFsPFCRxTLVCounter
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPFCRxTLVCounter (INT4 i4FsPFCPortNumber,
                         UINT4 *pu4RetValFsPFCRxTLVCounter)
{
    tPFCPortEntry      *pPFCPortEntry = NULL;

    pPFCPortEntry = PFCUtlGetPortEntry ((UINT4) i4FsPFCPortNumber);
    if (pPFCPortEntry == NULL)
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : PFCUtlGetPortEntry () "
                       "Returned FAILURE. \r\n", __FUNCTION__);
        return SNMP_FAILURE;
    }

    *pu4RetValFsPFCRxTLVCounter = pPFCPortEntry->u4PFCRxTLVCount;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsPFCRxTLVErrors
 Input       :  The Indices
                FsPFCPortNumber

                The Object 
                retValFsPFCRxTLVErrors
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPFCRxTLVErrors (INT4 i4FsPFCPortNumber,
                        UINT4 *pu4RetValFsPFCRxTLVErrors)
{
    tPFCPortEntry      *pPFCPortEntry = NULL;

    pPFCPortEntry = PFCUtlGetPortEntry ((UINT4) i4FsPFCPortNumber);
    if (pPFCPortEntry == NULL)
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : PFCUtlGetPortEntry () "
                       "Returned FAILURE. \r\n", __FUNCTION__);
        return SNMP_FAILURE;
    }

    *pu4RetValFsPFCRxTLVErrors = pPFCPortEntry->u4PFCRxTLVError;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsPFCRowStatus
 Input       :  The Indices
                FsPFCPortNumber

                The Object 
                retValFsPFCRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPFCRowStatus (INT4 i4FsPFCPortNumber, INT4 *pi4RetValFsPFCRowStatus)
{
    tPFCPortEntry      *pPFCPortEntry = NULL;

    pPFCPortEntry = PFCUtlGetPortEntry ((UINT4) i4FsPFCPortNumber);
    if (pPFCPortEntry == NULL)
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : PFCUtlGetPortEntry () "
                       "Returned FAILURE. \r\n", __FUNCTION__);
        return SNMP_FAILURE;
    }

    *pi4RetValFsPFCRowStatus = pPFCPortEntry->u1PFCRowStatus;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsPFCSyncd
 Input       :  The Indices
                FsPFCPortNumber

                The Object 
                retValFsPFCSyncd
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsPFCSyncd(INT4 i4FsPFCPortNumber , INT4 *pi4RetValFsPFCSyncd)
{
    tPFCPortEntry      *pPFCPortEntry = NULL;

    pPFCPortEntry = PFCUtlGetPortEntry ((UINT4) i4FsPFCPortNumber);
    if (pPFCPortEntry == NULL)
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : PFCUtlGetPortEntry () "
                       "Returned FAILURE. \r\n", __FUNCTION__);
        return SNMP_FAILURE;
    }

    *pi4RetValFsPFCSyncd = pPFCPortEntry->bPFCSyncd;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsPFCError
 Input       :  The Indices
                FsPFCPortNumber

                The Object 
                retValFsPFCError
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsPFCError(INT4 i4FsPFCPortNumber , INT4 *pi4RetValFsPFCError)
{
    tPFCPortEntry      *pPFCPortEntry = NULL;

    pPFCPortEntry = PFCUtlGetPortEntry ((UINT4) i4FsPFCPortNumber);
    if (pPFCPortEntry == NULL)
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : PFCUtlGetPortEntry () "
                       "Returned FAILURE. \r\n", __FUNCTION__);
        return SNMP_FAILURE;
    }

    *pi4RetValFsPFCError = pPFCPortEntry->bPFCError;
    return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetFsPFCDcbxStatus
 Input       :  The Indices
                FsPFCPortNumber

                The Object 
                retValFsPFCDcbxStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsPFCDcbxStatus(INT4 i4FsPFCPortNumber , INT4 *pi4RetValFsPFCDcbxStatus)
{
    tPFCPortEntry      *pPFCPortEntry = NULL;

    pPFCPortEntry = PFCUtlGetPortEntry ((UINT4) i4FsPFCPortNumber);
    if (pPFCPortEntry == NULL)
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : PFCUtlGetPortEntry () "
                       "Returned FAILURE. \r\n", __FUNCTION__);
        return SNMP_FAILURE;
    }

    *pi4RetValFsPFCDcbxStatus = pPFCPortEntry->u1DcbxStatus;
    return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetFsPFCRxPauseFrameCounter
 Input       :  The Indices
                FsPFCPortNumber

                The Object 
                retValFsPFCRxPauseFrameCounter
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsPFCRxPauseFrameCounter(INT4 i4FsPFCPortNumber , UINT4 *pu4RetValFsPFCRxPauseFrameCounter)
{
    UINT4    u4PFCRxPauseFrameCounter = 0;
    UINT4    u4PFCRxGetFrameCounter = 0;

    nmhGetFsPFCRxPauseFrameP0Counter (i4FsPFCPortNumber, &u4PFCRxGetFrameCounter);
    u4PFCRxPauseFrameCounter += u4PFCRxGetFrameCounter;
    nmhGetFsPFCRxPauseFrameP1Counter (i4FsPFCPortNumber, &u4PFCRxGetFrameCounter);
    u4PFCRxPauseFrameCounter += u4PFCRxGetFrameCounter;
    nmhGetFsPFCRxPauseFrameP2Counter (i4FsPFCPortNumber, &u4PFCRxGetFrameCounter);
    u4PFCRxPauseFrameCounter += u4PFCRxGetFrameCounter;
    nmhGetFsPFCRxPauseFrameP3Counter (i4FsPFCPortNumber, &u4PFCRxGetFrameCounter);
    u4PFCRxPauseFrameCounter += u4PFCRxGetFrameCounter;
    nmhGetFsPFCRxPauseFrameP4Counter (i4FsPFCPortNumber, &u4PFCRxGetFrameCounter);
    u4PFCRxPauseFrameCounter += u4PFCRxGetFrameCounter;
    nmhGetFsPFCRxPauseFrameP5Counter (i4FsPFCPortNumber, &u4PFCRxGetFrameCounter);
    u4PFCRxPauseFrameCounter += u4PFCRxGetFrameCounter;
    nmhGetFsPFCRxPauseFrameP6Counter (i4FsPFCPortNumber, &u4PFCRxGetFrameCounter);
    u4PFCRxPauseFrameCounter += u4PFCRxGetFrameCounter;
    nmhGetFsPFCRxPauseFrameP7Counter (i4FsPFCPortNumber, &u4PFCRxGetFrameCounter);
    u4PFCRxPauseFrameCounter += u4PFCRxGetFrameCounter;

	*pu4RetValFsPFCRxPauseFrameCounter = u4PFCRxPauseFrameCounter;
	return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetFsPFCTxPauseFrameCounter
 Input       :  The Indices
                FsPFCPortNumber

                The Object 
                retValFsPFCTxPauseFrameCounter
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsPFCTxPauseFrameCounter(INT4 i4FsPFCPortNumber , UINT4 *pu4RetValFsPFCTxPauseFrameCounter)
{
    UINT4    u4PFCTxPauseFrameCounter = 0;
    UINT4    u4PFCTxGetFrameCounter = 0;

    nmhGetFsPFCTxPauseFrameP0Counter (i4FsPFCPortNumber, &u4PFCTxGetFrameCounter);
    u4PFCTxPauseFrameCounter += u4PFCTxGetFrameCounter;
    nmhGetFsPFCTxPauseFrameP1Counter (i4FsPFCPortNumber, &u4PFCTxGetFrameCounter);
    u4PFCTxPauseFrameCounter += u4PFCTxGetFrameCounter;
    nmhGetFsPFCTxPauseFrameP2Counter (i4FsPFCPortNumber, &u4PFCTxGetFrameCounter);
    u4PFCTxPauseFrameCounter += u4PFCTxGetFrameCounter;
    nmhGetFsPFCTxPauseFrameP3Counter (i4FsPFCPortNumber, &u4PFCTxGetFrameCounter);
    u4PFCTxPauseFrameCounter += u4PFCTxGetFrameCounter;
    nmhGetFsPFCTxPauseFrameP4Counter (i4FsPFCPortNumber, &u4PFCTxGetFrameCounter);
    u4PFCTxPauseFrameCounter += u4PFCTxGetFrameCounter;
    nmhGetFsPFCTxPauseFrameP5Counter (i4FsPFCPortNumber, &u4PFCTxGetFrameCounter);
    u4PFCTxPauseFrameCounter += u4PFCTxGetFrameCounter;
    nmhGetFsPFCTxPauseFrameP6Counter (i4FsPFCPortNumber, &u4PFCTxGetFrameCounter);
    u4PFCTxPauseFrameCounter += u4PFCTxGetFrameCounter;
    nmhGetFsPFCTxPauseFrameP7Counter (i4FsPFCPortNumber, &u4PFCTxGetFrameCounter);
    u4PFCTxPauseFrameCounter += u4PFCTxGetFrameCounter;

	*pu4RetValFsPFCTxPauseFrameCounter = u4PFCTxPauseFrameCounter;
	return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetFsPFCRxPauseFrameP0Counter
 Input       :  The Indices
                FsPFCPortNumber

                The Object 
                retValFsPFCRxPauseFrameP0Counter
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsPFCRxPauseFrameP0Counter(INT4 i4FsPFCPortNumber , UINT4 *pu4RetValFsPFCRxPauseFrameP0Counter)
{
	tQosPfcStats QosPfcStats;

	MEMSET (&QosPfcStats, 0 , sizeof(tQosPfcStats));

	QosPfcStats.u4IfIndex = (UINT4) i4FsPFCPortNumber;
	QosPfcStats.u1PfcStatsType = PFC_STATS_RX_PFC_FRAME_PRI0;
	QosApiQosHwGetPfcStats (&QosPfcStats);
	*pu4RetValFsPFCRxPauseFrameP0Counter = QosPfcStats.u4PfcPauseCount;
	return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetFsPFCRxPauseFrameP1Counter
 Input       :  The Indices
                FsPFCPortNumber

                The Object 
                retValFsPFCRxPauseFrameP1Counter
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsPFCRxPauseFrameP1Counter(INT4 i4FsPFCPortNumber , UINT4 *pu4RetValFsPFCRxPauseFrameP1Counter)
{
	tQosPfcStats QosPfcStats;

	MEMSET (&QosPfcStats, 0 , sizeof(tQosPfcStats));

	QosPfcStats.u4IfIndex = (UINT4) i4FsPFCPortNumber;
	QosPfcStats.u1PfcStatsType = PFC_STATS_RX_PFC_FRAME_PRI1;
	QosApiQosHwGetPfcStats (&QosPfcStats);
	*pu4RetValFsPFCRxPauseFrameP1Counter = QosPfcStats.u4PfcPauseCount;
	return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetFsPFCRxPauseFrameP2Counter
 Input       :  The Indices
                FsPFCPortNumber

                The Object 
                retValFsPFCRxPauseFrameP2Counter
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsPFCRxPauseFrameP2Counter(INT4 i4FsPFCPortNumber , UINT4 *pu4RetValFsPFCRxPauseFrameP2Counter)
{
	tQosPfcStats QosPfcStats;

	MEMSET (&QosPfcStats, 0 , sizeof(tQosPfcStats));

	QosPfcStats.u4IfIndex = (UINT4) i4FsPFCPortNumber;
	QosPfcStats.u1PfcStatsType = PFC_STATS_RX_PFC_FRAME_PRI2;
	QosApiQosHwGetPfcStats (&QosPfcStats);
	*pu4RetValFsPFCRxPauseFrameP2Counter = QosPfcStats.u4PfcPauseCount;
	return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetFsPFCRxPauseFrameP3Counter
 Input       :  The Indices
                FsPFCPortNumber

                The Object 
                retValFsPFCRxPauseFrameP3Counter
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsPFCRxPauseFrameP3Counter(INT4 i4FsPFCPortNumber , UINT4 *pu4RetValFsPFCRxPauseFrameP3Counter)
{
	tQosPfcStats QosPfcStats;

	MEMSET (&QosPfcStats, 0 , sizeof(tQosPfcStats));

	QosPfcStats.u4IfIndex = (UINT4) i4FsPFCPortNumber;
	QosPfcStats.u1PfcStatsType = PFC_STATS_RX_PFC_FRAME_PRI3;
	QosApiQosHwGetPfcStats (&QosPfcStats);
	*pu4RetValFsPFCRxPauseFrameP3Counter = QosPfcStats.u4PfcPauseCount;
	return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetFsPFCRxPauseFrameP4Counter
 Input       :  The Indices
                FsPFCPortNumber

                The Object 
                retValFsPFCRxPauseFrameP4Counter
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsPFCRxPauseFrameP4Counter(INT4 i4FsPFCPortNumber , UINT4 *pu4RetValFsPFCRxPauseFrameP4Counter)
{
	tQosPfcStats QosPfcStats;

	MEMSET (&QosPfcStats, 0 , sizeof(tQosPfcStats));

	QosPfcStats.u4IfIndex = (UINT4) i4FsPFCPortNumber;
	QosPfcStats.u1PfcStatsType = PFC_STATS_RX_PFC_FRAME_PRI4;
	QosApiQosHwGetPfcStats (&QosPfcStats);
	*pu4RetValFsPFCRxPauseFrameP4Counter = QosPfcStats.u4PfcPauseCount;
	return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetFsPFCRxPauseFrameP5Counter
 Input       :  The Indices
                FsPFCPortNumber

                The Object 
                retValFsPFCRxPauseFrameP5Counter
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsPFCRxPauseFrameP5Counter(INT4 i4FsPFCPortNumber , UINT4 *pu4RetValFsPFCRxPauseFrameP5Counter)
{
	tQosPfcStats QosPfcStats;

	MEMSET (&QosPfcStats, 0 , sizeof(tQosPfcStats));

	QosPfcStats.u4IfIndex = (UINT4) i4FsPFCPortNumber;
	QosPfcStats.u1PfcStatsType = PFC_STATS_RX_PFC_FRAME_PRI5;
	QosApiQosHwGetPfcStats (&QosPfcStats);
	*pu4RetValFsPFCRxPauseFrameP5Counter = QosPfcStats.u4PfcPauseCount;
	return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetFsPFCRxPauseFrameP6Counter
 Input       :  The Indices
                FsPFCPortNumber

                The Object 
                retValFsPFCRxPauseFrameP6Counter
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsPFCRxPauseFrameP6Counter(INT4 i4FsPFCPortNumber , UINT4 *pu4RetValFsPFCRxPauseFrameP6Counter)
{
	tQosPfcStats QosPfcStats;

	MEMSET (&QosPfcStats, 0 , sizeof(tQosPfcStats));

	QosPfcStats.u4IfIndex = (UINT4) i4FsPFCPortNumber;
	QosPfcStats.u1PfcStatsType = PFC_STATS_RX_PFC_FRAME_PRI6;
	QosApiQosHwGetPfcStats (&QosPfcStats);
	*pu4RetValFsPFCRxPauseFrameP6Counter = QosPfcStats.u4PfcPauseCount;
	return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetFsPFCRxPauseFrameP7Counter
 Input       :  The Indices
                FsPFCPortNumber

                The Object 
                retValFsPFCRxPauseFrameP7Counter
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsPFCRxPauseFrameP7Counter(INT4 i4FsPFCPortNumber , UINT4 *pu4RetValFsPFCRxPauseFrameP7Counter)
{
	tQosPfcStats QosPfcStats;

	MEMSET (&QosPfcStats, 0 , sizeof(tQosPfcStats));

	QosPfcStats.u4IfIndex = (UINT4) i4FsPFCPortNumber;
	QosPfcStats.u1PfcStatsType = PFC_STATS_RX_PFC_FRAME_PRI7;
	QosApiQosHwGetPfcStats (&QosPfcStats);
	*pu4RetValFsPFCRxPauseFrameP7Counter = QosPfcStats.u4PfcPauseCount;
	return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetFsPFCTxPauseFrameP0Counter
 Input       :  The Indices
                FsPFCPortNumber

                The Object 
                retValFsPFCTxPauseFrameP0Counter
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsPFCTxPauseFrameP0Counter(INT4 i4FsPFCPortNumber , UINT4 *pu4RetValFsPFCTxPauseFrameP0Counter)
{
	tQosPfcStats QosPfcStats;

	MEMSET (&QosPfcStats, 0 , sizeof(tQosPfcStats));

	QosPfcStats.u4IfIndex = (UINT4) i4FsPFCPortNumber;
	QosPfcStats.u1PfcStatsType = PFC_STATS_TX_PFC_FRAME_PRI0;
	QosApiQosHwGetPfcStats (&QosPfcStats);
	*pu4RetValFsPFCTxPauseFrameP0Counter = QosPfcStats.u4PfcPauseCount;
	return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetFsPFCTxPauseFrameP1Counter
 Input       :  The Indices
                FsPFCPortNumber

                The Object 
                retValFsPFCTxPauseFrameP1Counter
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsPFCTxPauseFrameP1Counter(INT4 i4FsPFCPortNumber , UINT4 *pu4RetValFsPFCTxPauseFrameP1Counter)
{
	tQosPfcStats QosPfcStats;

	MEMSET (&QosPfcStats, 0 , sizeof(tQosPfcStats));

	QosPfcStats.u4IfIndex = (UINT4) i4FsPFCPortNumber;
	QosPfcStats.u1PfcStatsType = PFC_STATS_TX_PFC_FRAME_PRI1;
	QosApiQosHwGetPfcStats (&QosPfcStats);
	*pu4RetValFsPFCTxPauseFrameP1Counter = QosPfcStats.u4PfcPauseCount;
	return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetFsPFCTxPauseFrameP2Counter
 Input       :  The Indices
                FsPFCPortNumber

                The Object 
                retValFsPFCTxPauseFrameP2Counter
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsPFCTxPauseFrameP2Counter(INT4 i4FsPFCPortNumber , UINT4 *pu4RetValFsPFCTxPauseFrameP2Counter)
{
	tQosPfcStats QosPfcStats;

	MEMSET (&QosPfcStats, 0 , sizeof(tQosPfcStats));

	QosPfcStats.u4IfIndex = (UINT4) i4FsPFCPortNumber;
	QosPfcStats.u1PfcStatsType = PFC_STATS_TX_PFC_FRAME_PRI2;
	QosApiQosHwGetPfcStats (&QosPfcStats);
	*pu4RetValFsPFCTxPauseFrameP2Counter = QosPfcStats.u4PfcPauseCount;
	return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetFsPFCTxPauseFrameP3Counter
 Input       :  The Indices
                FsPFCPortNumber

                The Object 
                retValFsPFCTxPauseFrameP3Counter
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsPFCTxPauseFrameP3Counter(INT4 i4FsPFCPortNumber , UINT4 *pu4RetValFsPFCTxPauseFrameP3Counter)
{
	tQosPfcStats QosPfcStats;

	MEMSET (&QosPfcStats, 0 , sizeof(tQosPfcStats));

	QosPfcStats.u4IfIndex = (UINT4) i4FsPFCPortNumber;
	QosPfcStats.u1PfcStatsType = PFC_STATS_TX_PFC_FRAME_PRI3;
	QosApiQosHwGetPfcStats (&QosPfcStats);
	*pu4RetValFsPFCTxPauseFrameP3Counter = QosPfcStats.u4PfcPauseCount;
	return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetFsPFCTxPauseFrameP4Counter
 Input       :  The Indices
                FsPFCPortNumber

                The Object 
                retValFsPFCTxPauseFrameP4Counter
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsPFCTxPauseFrameP4Counter(INT4 i4FsPFCPortNumber , UINT4 *pu4RetValFsPFCTxPauseFrameP4Counter)
{
	tQosPfcStats QosPfcStats;

	MEMSET (&QosPfcStats, 0 , sizeof(tQosPfcStats));

	QosPfcStats.u4IfIndex = (UINT4) i4FsPFCPortNumber;
	QosPfcStats.u1PfcStatsType = PFC_STATS_TX_PFC_FRAME_PRI4;
	QosApiQosHwGetPfcStats (&QosPfcStats);
	*pu4RetValFsPFCTxPauseFrameP4Counter = QosPfcStats.u4PfcPauseCount;
	return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetFsPFCTxPauseFrameP5Counter
 Input       :  The Indices
                FsPFCPortNumber

                The Object 
                retValFsPFCTxPauseFrameP5Counter
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsPFCTxPauseFrameP5Counter(INT4 i4FsPFCPortNumber , UINT4 *pu4RetValFsPFCTxPauseFrameP5Counter)
{
	tQosPfcStats QosPfcStats;

	MEMSET (&QosPfcStats, 0 , sizeof(tQosPfcStats));

	QosPfcStats.u4IfIndex = (UINT4) i4FsPFCPortNumber;
	QosPfcStats.u1PfcStatsType = PFC_STATS_TX_PFC_FRAME_PRI5;
	QosApiQosHwGetPfcStats (&QosPfcStats);
	*pu4RetValFsPFCTxPauseFrameP5Counter = QosPfcStats.u4PfcPauseCount;
	return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetFsPFCTxPauseFrameP6Counter
 Input       :  The Indices
                FsPFCPortNumber

                The Object 
                retValFsPFCTxPauseFrameP6Counter
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsPFCTxPauseFrameP6Counter(INT4 i4FsPFCPortNumber , UINT4 *pu4RetValFsPFCTxPauseFrameP6Counter)
{
	tQosPfcStats QosPfcStats;

	MEMSET (&QosPfcStats, 0 , sizeof(tQosPfcStats));

	QosPfcStats.u4IfIndex = (UINT4) i4FsPFCPortNumber;
	QosPfcStats.u1PfcStatsType = PFC_STATS_TX_PFC_FRAME_PRI6;
	QosApiQosHwGetPfcStats (&QosPfcStats);
	*pu4RetValFsPFCTxPauseFrameP6Counter = QosPfcStats.u4PfcPauseCount;
	return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetFsPFCTxPauseFrameP7Counter
 Input       :  The Indices
                FsPFCPortNumber

                The Object 
                retValFsPFCTxPauseFrameP7Counter
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsPFCTxPauseFrameP7Counter(INT4 i4FsPFCPortNumber , UINT4 *pu4RetValFsPFCTxPauseFrameP7Counter)
{
	tQosPfcStats QosPfcStats;

	MEMSET (&QosPfcStats, 0 , sizeof(tQosPfcStats));

	QosPfcStats.u4IfIndex = (UINT4) i4FsPFCPortNumber;
	QosPfcStats.u1PfcStatsType = PFC_STATS_TX_PFC_FRAME_PRI7;
	QosApiQosHwGetPfcStats (&QosPfcStats);
	*pu4RetValFsPFCTxPauseFrameP7Counter = QosPfcStats.u4PfcPauseCount;
	return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetFsPFCDataFrameDiscardCounter
 Input       :  The Indices
                FsPFCPortNumber

                The Object 
                retValFsPFCDataFrameDiscardCounter
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsPFCDataFrameDiscardCounter(INT4 i4FsPFCPortNumber , UINT4 *pu4RetValFsPFCDataFrameDiscardCounter)
{
    UNUSED_PARAM (i4FsPFCPortNumber);
    UNUSED_PARAM (pu4RetValFsPFCDataFrameDiscardCounter);
    return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetFsPFCClearPauseFrameCounters
 Input       :  The Indices
                FsPFCPortNumber

                The Object 
                retValFsPFCClearPauseFrameCounters
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsPFCClearPauseFrameCounters(INT4 i4FsPFCPortNumber , INT4 *pi4RetValFsPFCClearPauseFrameCounters)
{
    UNUSED_PARAM (i4FsPFCPortNumber);
    UNUSED_PARAM (pi4RetValFsPFCClearPauseFrameCounters);
    return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhSetFsPFCClearPauseFrameCounters
 Input       :  The Indices
                FsPFCPortNumber

                The Object 
                setValFsPFCClearPauseFrameCounters
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhSetFsPFCClearPauseFrameCounters(INT4 i4FsPFCPortNumber , INT4 i4SetValFsPFCClearPauseFrameCounters)
{
    DCBX_TRC_ARG3 (DCBX_MGMT_TRC, "%s, Args: "
            "i4FsPFCPortNumber: %d i4SetValFsPFCClearPauseFrameCounters: %d\r\n", 
            __func__, i4FsPFCPortNumber, i4SetValFsPFCClearPauseFrameCounters);

    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsPFCAdminMode
 Input       :  The Indices
                FsPFCPortNumber

                The Object 
                setValFsPFCAdminMode
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsPFCAdminMode (INT4 i4FsPFCPortNumber, INT4 i4SetValFsPFCAdminMode)
{
    tPFCPortEntry      *pPFCPortEntry = NULL;

    DCBX_TRC_ARG3 (DCBX_MGMT_TRC, "%s, Args: "
            "i4FsPFCPortNumber: %d "
            "i4SetValFsPFCAdminMode: %d\r\n", 
            __func__, i4FsPFCPortNumber, i4SetValFsPFCAdminMode);

    pPFCPortEntry = PFCUtlGetPortEntry ((UINT4) i4FsPFCPortNumber);
    if (pPFCPortEntry == NULL)
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : PFCUtlGetPortEntry () "
                       "Returned FAILURE. \r\n", __FUNCTION__);
        return SNMP_FAILURE;
    }

    if (pPFCPortEntry->u1PFCAdminMode != (UINT1) i4SetValFsPFCAdminMode)
    {

        PFCUtlAdminModeChange (pPFCPortEntry, (UINT1) i4SetValFsPFCAdminMode);
        /* Updation of Admin mode chage data structure will be taken
         * care inside teh Admin Mode change function */
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsPFCClearTLVCounters
 Input       :  The Indices
                FsPFCPortNumber

                The Object 
                setValFsPFCClearTLVCounters
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsPFCClearTLVCounters (INT4 i4FsPFCPortNumber,
                             INT4 i4SetValFsPFCClearTLVCounters)
{
    tPFCPortEntry      *pPFCPortEntry = NULL;

    DCBX_TRC_ARG3 (DCBX_MGMT_TRC, "%s, Args: "
            "i4FsPFCPortNumber: %d "
            "i4SetValFsPFCClearTLVCounters: %d\r\n", 
            __func__, i4FsPFCPortNumber, i4SetValFsPFCClearTLVCounters);

    pPFCPortEntry = PFCUtlGetPortEntry ((UINT4) i4FsPFCPortNumber);
    if (pPFCPortEntry == NULL)
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : PFCUtlGetPortEntry () "
                       "Returned FAILURE. \r\n", __FUNCTION__);
        return SNMP_FAILURE;
    }
    if (i4SetValFsPFCClearTLVCounters == PFC_ENABLED)
    {
        pPFCPortEntry->u4PFCTxTLVCount = DCBX_ZERO;
        pPFCPortEntry->u4PFCRxTLVError = DCBX_ZERO;
        pPFCPortEntry->u4PFCRxTLVCount = DCBX_ZERO;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsPFCRowStatus
 Input       :  The Indices
                FsPFCPortNumber

                The Object 
                setValFsPFCRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsPFCRowStatus (INT4 i4FsPFCPortNumber, INT4 i4SetValFsPFCRowStatus)
{
    tPFCPortEntry      *pPFCPortEntry = NULL;
    tCEECtrlEntry      *pCEECtrlEntry = NULL;
    tDcbxPortEntry     *pDcbxPortEntry = NULL;
    INT4                i4RetVal = OSIX_SUCCESS;

    DCBX_TRC_ARG3 (DCBX_MGMT_TRC, "%s, Args: "
            "i4FsPFCPortNumber: %d "
            "i4SetValFsPFCRowStatus: %d\r\n", 
            __func__, i4FsPFCPortNumber, i4SetValFsPFCRowStatus);

    pDcbxPortEntry = DcbxUtlGetPortEntry ((UINT4) i4FsPFCPortNumber);
    if (pDcbxPortEntry == NULL)
    {
        DCBX_TRC_ARG2 (MGMT_TRC | ALL_FAILURE_TRC, "%s : "
                "pDcbxPortEntry (Port) Id %d is invalid \r\n",
                __func__, i4FsPFCPortNumber);
        return SNMP_FAILURE;
    }
    switch (i4SetValFsPFCRowStatus)
    {
        case CREATE_AND_WAIT:    /* Intentional fall through for MSR */
        case CREATE_AND_GO:

            /* Memblock creation and initializing the default values
             * to the PFC  Node for this port  are handled inside the below
             * function and  rowstatus is initialized here */
            pPFCPortEntry =
                PFCUtlCreatePortTblEntry ((UINT4) i4FsPFCPortNumber);
            if (pPFCPortEntry == NULL)
            {
                DCBX_TRC_ARG1 (DCBX_MGMT_TRC,
                               "In %s : PFCUtlCreatePortTblEntry () "
                               "Returns FAILURE. \r\n", __FUNCTION__);
                return SNMP_FAILURE;
            }
            pCEECtrlEntry =
                CEEUtlCreateCtrlTblEntry ((UINT4) i4FsPFCPortNumber);
            if (pCEECtrlEntry == NULL)
            {
                DCBX_TRC_ARG1 (DCBX_MGMT_TRC | DCBX_FAILURE_TRC,
                               "In %s : CEEUtlCreateCtrlTblEntry () "
                               "Returns FAILURE. \r\n", __FUNCTION__);
                return SNMP_FAILURE;
            }
            pDcbxPortEntry->u1PFCAdminStatus = PFC_ENABLED;
            pPFCPortEntry->u1PFCRowStatus = ACTIVE;
            break;

        case DESTROY:
            pPFCPortEntry = PFCUtlGetPortEntry ((UINT4) i4FsPFCPortNumber);
            if (pPFCPortEntry == NULL)
            {
                return SNMP_SUCCESS;
            }

            /* Delete the Dcb Entry */
            i4RetVal = PFCUtlDeletePortTblEntry (pPFCPortEntry);
            if (i4RetVal != OSIX_SUCCESS)
            {
                return SNMP_FAILURE;
            }
            pCEECtrlEntry =
                CEEUtlGetCtrlEntry ((UINT4) i4FsPFCPortNumber);
            if (pCEECtrlEntry == NULL)
            {
                return SNMP_SUCCESS;
            }
            i4RetVal = CEEUtlDeleteCtrlTblEntry (pCEECtrlEntry);
            if (i4RetVal != OSIX_SUCCESS)
            {
                DCBX_TRC_ARG1 (DCBX_MGMT_TRC | DCBX_FAILURE_TRC,
                               "In %s : CEEUtlDeleteCtrlTblEntry () "
                               "Returns FAILURE. \r\n", __FUNCTION__);
                return SNMP_FAILURE;
            }
            pDcbxPortEntry->u1PFCAdminStatus = PFC_DISABLED;
            break;
        case ACTIVE:
            break;
        default:
            return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsPFCAdminMode
 Input       :  The Indices
                FsPFCPortNumber

                The Object 
                testValFsPFCAdminMode
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsPFCAdminMode (UINT4 *pu4ErrorCode,
                         INT4 i4FsPFCPortNumber, INT4 i4TestValFsPFCAdminMode)
{
    tPFCPortEntry      *pPFCPortEntry = NULL;

    PFC_SHUTDOWN_CHECK_TEST;
    if (DcbxUtlValidatePort ((UINT4) i4FsPFCPortNumber) == OSIX_FAILURE)
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : DcbxUtlValidatePort() "
                       "Returns Failure. \r\n", __FUNCTION__);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_ERR_DCBX_NO_PORT);
        return SNMP_FAILURE;
    }

    pPFCPortEntry = PFCUtlGetPortEntry ((UINT4) i4FsPFCPortNumber);
    if (pPFCPortEntry == NULL)
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : PFCUtlGetPortEntry () "
                       "Returns FAILURE. \r\n", __FUNCTION__);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_ERR_PFC_NO_PORT_ENTRY);
        return SNMP_FAILURE;
    }

    if ((i4TestValFsPFCAdminMode < PFC_PORT_MODE_AUTO) ||
        (i4TestValFsPFCAdminMode > PFC_PORT_MODE_OFF))
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : Wrong AdminMode() "
                       "Returns Failure. \r\n", __FUNCTION__);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_ERR_PFC_INVALID_PORT_MODE_STATUS);
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsPFCClearTLVCounters
 Input       :  The Indices
                FsPFCPortNumber

                The Object 
                testValFsPFCClearTLVCounters
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsPFCClearTLVCounters (UINT4 *pu4ErrorCode,
                                INT4 i4FsPFCPortNumber,
                                INT4 i4TestValFsPFCClearTLVCounters)
{
    tPFCPortEntry      *pPFCPortEntry = NULL;

    if (DcbxUtlValidatePort ((UINT4) i4FsPFCPortNumber) == OSIX_FAILURE)
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : DcbxUtlValidatePort() "
                       "Returns Failure. \r\n", __FUNCTION__);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_ERR_DCBX_NO_PORT);
        return SNMP_FAILURE;
    }

    PFC_SHUTDOWN_CHECK_TEST;
    pPFCPortEntry = PFCUtlGetPortEntry ((UINT4) i4FsPFCPortNumber);
    if (pPFCPortEntry == NULL)
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : PFCUtlGetPortEntry () "
                       "Returns FAILURE. \r\n", __FUNCTION__);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_ERR_PFC_NO_PORT_ENTRY);
        return SNMP_FAILURE;
    }

    if ((i4TestValFsPFCClearTLVCounters != PFC_DISABLED) &&
        (i4TestValFsPFCClearTLVCounters != PFC_ENABLED))
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : Wrong Setting() "
                       "Returns Failure. \r\n", __FUNCTION__);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_ERR_PFC_INVALID_CLEAR_STATUS);
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsPFCRowStatus
 Input       :  The Indices
                FsPFCPortNumber

                The Object 
                testValFsPFCRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsPFCRowStatus (UINT4 *pu4ErrorCode,
                         INT4 i4FsPFCPortNumber, INT4 i4TestValFsPFCRowStatus)
{
    tPFCPortEntry      *pPFCPortEntry = NULL;

    if (DcbxUtlValidatePort ((UINT4) i4FsPFCPortNumber) == OSIX_FAILURE)
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : DcbxUtlValidatePort() "
                       "Returns Failure. \r\n", __FUNCTION__);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        /* Currently, DCBX shouldn't be allowed in 
           Port-Channel Interface, setting appropriate error code 
        */
        if (CfaIsLaggInterface ((UINT4)i4FsPFCPortNumber) == CFA_TRUE) 
        {
          CLI_SET_ERR (CLI_ERR_DCBX_PORT_IN_PORT_CHANNEL);
        }
        else
        {
          CLI_SET_ERR (CLI_ERR_DCBX_NO_PORT);
        }
        return SNMP_FAILURE;
    }

    PFC_SHUTDOWN_CHECK_TEST;
    pPFCPortEntry = PFCUtlGetPortEntry ((UINT4) i4FsPFCPortNumber);
    switch (i4TestValFsPFCRowStatus)
    {
        case CREATE_AND_GO:

            /* If PFC Node is present for this port and
             * trying to create the same entry then
             * return failure */
            if (pPFCPortEntry != NULL)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                CLI_SET_ERR (CLI_ERR_PFC_PORT_ALREADY_PRESENT);
                return (SNMP_FAILURE);
            }
            if ((MemGetFreeUnits (gPFCGlobalInfo.PFCPortPoolId)) ==
                DCBX_INIT_VAL)
            {
                *pu4ErrorCode = SNMP_ERR_NO_CREATION;
                CLI_SET_ERR (CLI_ERR_PFC_PORT_CREATION_FAILS);
                return SNMP_FAILURE;
            }
            break;

        case DESTROY:

            if (pPFCPortEntry == NULL)
            {
                DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : PFCUtlGetPortEntry () "
                               "Returns FAILURE. \r\n", __FUNCTION__);
                *pu4ErrorCode = SNMP_ERR_NO_CREATION;
                CLI_SET_ERR (CLI_ERR_PFC_NO_PORT_ENTRY);
                return SNMP_FAILURE;
            }
            break;

        case ACTIVE:
            break;

        default:
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsPFCClearPauseFrameCounters
 Input       :  The Indices
                FsPFCPortNumber

                The Object 
                testValFsPFCClearPauseFrameCounters
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhTestv2FsPFCClearPauseFrameCounters(UINT4 *pu4ErrorCode , INT4 i4FsPFCPortNumber , INT4 i4TestValFsPFCClearPauseFrameCounters)
{
	UNUSED_PARAM (pu4ErrorCode);
	UNUSED_PARAM (i4FsPFCPortNumber);
	UNUSED_PARAM (i4TestValFsPFCClearPauseFrameCounters);
	return SNMP_SUCCESS;
}
/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsPFCPortTable
 Input       :  The Indices
                FsPFCPortNumber
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsPFCPortTable (UINT4 *pu4ErrorCode,
                        tSnmpIndexList * pSnmpIndexList,
                        tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsAppPriSystemControl
 Input       :  The Indices

                The Object 
                retValFsAppPriSystemControl
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsAppPriSystemControl (INT4 *pi4RetValFsAppPriSystemControl)
{
    *pi4RetValFsAppPriSystemControl = gAppPriGlobalInfo.u1AppPriSystemCtrl;
    DCBX_TRC_ARG2 (DCBX_MGMT_TRC,
                   "%s : Application Priority System Control = %d"
                   "SUCCESS.\r\n", __FUNCTION__,
                   *pi4RetValFsAppPriSystemControl);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsAppPriModuleStatus
 Input       :  The Indices

                The Object 
                retValFsAppPriModuleStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsAppPriModuleStatus (INT4 *pi4RetValFsAppPriModuleStatus)
{
    *pi4RetValFsAppPriModuleStatus = gAppPriGlobalInfo.u1AppPriModStatus;
    DCBX_TRC_ARG2 (DCBX_MGMT_TRC, "%s : Application Priority Module Status = %d"
                   "SUCCESS.\r\n", __FUNCTION__,
                   *pi4RetValFsAppPriModuleStatus);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsAppPriClearCounters
 Input       :  The Indices

                The Object 
                retValFsAppPriClearCounters
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsAppPriClearCounters (INT4 *pi4RetValFsAppPriClearCounters)
{
    *pi4RetValFsAppPriClearCounters = APP_PRI_DISABLED;
    DCBX_TRC_ARG2 (DCBX_MGMT_TRC,
                   "%s : ApplicationPriority Clear Counter = %d SUCCESS.\r\n",
                   __FUNCTION__, *pi4RetValFsAppPriClearCounters);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsAppPriGlobalEnableTrap
 Input       :  The Indices

                The Object 
                retValFsAppPriGlobalEnableTrap
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsAppPriGlobalEnableTrap (INT4 *pi4RetValFsAppPriGlobalEnableTrap)
{
    *pi4RetValFsAppPriGlobalEnableTrap =
        (INT4) gAppPriGlobalInfo.u4AppPriTrapStatus;
    DCBX_TRC_ARG2 (DCBX_MGMT_TRC,
                   "%s : ApplicationPriority Trap Status = %d" "SUCCESS.\r\n",
                   __FUNCTION__, *pi4RetValFsAppPriGlobalEnableTrap);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsAppPriGeneratedTrapCount
 Input       :  The Indices
                The Object
                retValFsAppPriGeneratedTrapCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
***************************************************************************/
INT1
nmhGetFsAppPriGeneratedTrapCount (UINT4 *pu4RetValFsAppPriGeneratedTrapCount)
{
    *pu4RetValFsAppPriGeneratedTrapCount
        = gAppPriGlobalInfo.u4AppPriGeneratedTrapCount;
    DCBX_TRC_ARG2 (DCBX_MGMT_TRC,
                   "%s : Application Priority Trap Count= %d SUCCESS.\r\n",
                   __FUNCTION__, *pu4RetValFsAppPriGeneratedTrapCount);
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsAppPriSystemControl
 Input       :  The Indices

                The Object 
                setValFsAppPriSystemControl
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsAppPriSystemControl (INT4 i4SetValFsAppPriSystemControl)
{
    DCBX_TRC_ARG2 (DCBX_MGMT_TRC, "%s, Args: "
            "i4SetValFsAppPriSystemControl: %d\r\n", 
            __func__, i4SetValFsAppPriSystemControl);

    if (gAppPriGlobalInfo.u1AppPriSystemCtrl !=
        (UINT1) i4SetValFsAppPriSystemControl)
    {

        if (i4SetValFsAppPriSystemControl == APP_PRI_START)
        {
            if (AppPriMainModuleStart () == OSIX_FAILURE)
            {
                return SNMP_FAILURE;
            }
        }
        else
        {
            AppPriMainModuleShutDown ();
            AppPriNotifyShutdownStatus ();
        }
        gAppPriGlobalInfo.u1AppPriSystemCtrl =
            (UINT1) i4SetValFsAppPriSystemControl;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsAppPriModuleStatus
 Input       :  The Indices

                The Object 
                setValFsAppPriModuleStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsAppPriModuleStatus (INT4 i4SetValFsAppPriModuleStatus)
{
    DCBX_TRC_ARG2 (DCBX_MGMT_TRC, "%s, Args: "
            "i4SetValFsAppPriModuleStatus: %d\r\n", 
            __func__, i4SetValFsAppPriModuleStatus);

    if (gAppPriGlobalInfo.u1AppPriModStatus !=
        (UINT1) i4SetValFsAppPriModuleStatus)
    {

        AppPriUtlModuleStatusChange ((UINT1) i4SetValFsAppPriModuleStatus);
        /* Updation of Module status data strucutre will be taken care
         * inside the Status change function */
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsAppPriClearCounters
 Input       :  The Indices

                The Object 
                setValFsAppPriClearCounters
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsAppPriClearCounters (INT4 i4SetValFsAppPriClearCounters)
{
    tAppPriPortEntry   *pAPPortEntry = NULL;
    INT4                i4APPortNumber = DCBX_ZERO;
    INT4                i4NextAPPortNumber = DCBX_ZERO;
    INT4                i4RetVal = SNMP_SUCCESS;

    DCBX_TRC_ARG2 (DCBX_MGMT_TRC, "%s, Args: "
            "i4SetValFsAppPriClearCounters: %d\r\n", 
            __func__, i4SetValFsAppPriClearCounters);

    if (i4SetValFsAppPriClearCounters == APP_PRI_ENABLED)
    {
        i4RetVal = nmhGetFirstIndexFsAppPriPortTable (&i4NextAPPortNumber);
        if (i4RetVal == SNMP_FAILURE)
        {
            DCBX_TRC_ARG1 (DCBX_MGMT_TRC,
                           "In %s : nmhGetFirstIndexFsAppPriPortTable () "
                           "No ApplicationPriority Entries is present to clear . \r\n",
                           __FUNCTION__);
            return SNMP_SUCCESS;
        }

        do
        {
            i4APPortNumber = i4NextAPPortNumber;
            pAPPortEntry = AppPriUtlGetPortEntry ((UINT4) i4APPortNumber);
            if (pAPPortEntry == NULL)
            {
                DCBX_TRC_ARG1 (DCBX_MGMT_TRC,
                               "In %s : AppPriUtlGetPortEntry () "
                               "Returns FAILURE. \r\n", __FUNCTION__);
                return SNMP_FAILURE;
            }
            pAPPortEntry->u4AppPriTLVTxCount = DCBX_ZERO;
            pAPPortEntry->u4AppPriTLVRxError = DCBX_ZERO;
            pAPPortEntry->u4AppPriTLVRxCount = DCBX_ZERO;
            pAPPortEntry->u4AppPriAppProtocols = DCBX_ZERO;
        }
        while (nmhGetNextIndexFsAppPriPortTable
               (i4APPortNumber, &i4NextAPPortNumber) != SNMP_FAILURE);
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsAppPriGlobalEnableTrap
 Input       :  The Indices

                The Object 
                setValFsAppPriGlobalEnableTrap
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsAppPriGlobalEnableTrap (INT4 i4SetValFsAppPriGlobalEnableTrap)
{
    gAppPriGlobalInfo.u4AppPriTrapStatus =
        (UINT4) i4SetValFsAppPriGlobalEnableTrap;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsAppPriGeneratedTrapCount
 Input       :  The Indices
                The Object
                setValFsAppPriGeneratedTrapCount
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsAppPriGeneratedTrapCount (UINT4 u4SetValFsAppPriGeneratedTrapCount)
{
    DCBX_TRC_ARG2 (DCBX_MGMT_TRC, "%s, Args: "
            "u4SetValFsAppPriGeneratedTrapCount: %d\r\n", 
            __func__, u4SetValFsAppPriGeneratedTrapCount);

    gAppPriGlobalInfo.u4AppPriGeneratedTrapCount =
        u4SetValFsAppPriGeneratedTrapCount;
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsAppPriSystemControl
 Input       :  The Indices

                The Object 
                testValFsAppPriSystemControl
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsAppPriSystemControl (UINT4 *pu4ErrorCode,
                                INT4 i4TestValFsAppPriSystemControl)
{
    if ((i4TestValFsAppPriSystemControl < APP_PRI_START) ||
        (i4TestValFsAppPriSystemControl > APP_PRI_SHUTDOWN))
    {
        DCBX_TRC_ARG2 (DCBX_MGMT_TRC,
                       "%s : Failed while setting the value to %d\r\n",
                       __FUNCTION__, i4TestValFsAppPriSystemControl);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_ERR_APP_PRI_INVALID_SYS_CONTROL);
        return (SNMP_FAILURE);
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsAppPriModuleStatus
 Input       :  The Indices

                The Object 
                testValFsAppPriModuleStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsAppPriModuleStatus (UINT4 *pu4ErrorCode,
                               INT4 i4TestValFsAppPriModuleStatus)
{
    APP_PRI_SHUTDOWN_CHECK_TEST;

    if ((i4TestValFsAppPriModuleStatus != APP_PRI_ENABLED) &&
        (i4TestValFsAppPriModuleStatus != APP_PRI_DISABLED))
    {
        DCBX_TRC_ARG2 (DCBX_MGMT_TRC,
                       "%s :Failed while setting the value to %d\r\n",
                       __FUNCTION__, i4TestValFsAppPriModuleStatus);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_ERR_APP_PRI_INVALID_SYS_STATUS);
        return (SNMP_FAILURE);
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsAppPriClearCounters
 Input       :  The Indices

                The Object 
                testValFsAppPriClearCounters
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsAppPriClearCounters (UINT4 *pu4ErrorCode,
                                INT4 i4TestValFsAppPriClearCounters)
{
    APP_PRI_SHUTDOWN_CHECK_TEST;
    if (i4TestValFsAppPriClearCounters != APP_PRI_ENABLED)
    {
        DCBX_TRC_ARG2 (DCBX_MGMT_TRC,
                       "%s :Failed while setting the value to %d\r\n",
                       __FUNCTION__, i4TestValFsAppPriClearCounters);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_ERR_APP_PRI_INVALID_CLEAR_STATUS);
        return (SNMP_FAILURE);
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsAppPriGlobalEnableTrap
 Input       :  The Indices

                The Object 
                testValFsAppPriGlobalEnableTrap
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsAppPriGlobalEnableTrap (UINT4 *pu4ErrorCode,
                                   INT4 i4TestValFsAppPriGlobalEnableTrap)
{
    APP_PRI_SHUTDOWN_CHECK_TEST;
    if (i4TestValFsAppPriGlobalEnableTrap & (~(APP_PRI_ALL_TRAP)))
    {
        DCBX_TRC_ARG2 (DCBX_MGMT_TRC,
                       "%s : Failed while setting the value to %d\r\n",
                       __FUNCTION__, i4TestValFsAppPriGlobalEnableTrap);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_ERR_APP_PRI_INVALID_NOTIFY);
        return (SNMP_FAILURE);
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsAppPriGeneratedTrapCount
 Input       :  The Indices
                The Object
                testValFsAppPriGeneratedTrapCount
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
                Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsAppPriGeneratedTrapCount (UINT4 *pu4ErrorCode,
                                     UINT4 u4TestValFsAppPriGeneratedTrapCount)
{
    APP_PRI_SHUTDOWN_CHECK_TEST;

    if (u4TestValFsAppPriGeneratedTrapCount != 0)
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC,
                       "%s : Values other than zero are not allowed "
                       "for this object.\r\n", __FUNCTION__);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_ERR_APP_PRI_INVALID_CLEAR_TRAP);
        return (SNMP_FAILURE);
    }

    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsAppPriSystemControl
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsAppPriSystemControl (UINT4 *pu4ErrorCode,
                               tSnmpIndexList * pSnmpIndexList,
                               tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsAppPriModuleStatus
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsAppPriModuleStatus (UINT4 *pu4ErrorCode,
                              tSnmpIndexList * pSnmpIndexList,
                              tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsAppPriClearCounters
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsAppPriClearCounters (UINT4 *pu4ErrorCode,
                               tSnmpIndexList * pSnmpIndexList,
                               tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsAppPriGlobalEnableTrap
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsAppPriGlobalEnableTrap (UINT4 *pu4ErrorCode,
                                  tSnmpIndexList * pSnmpIndexList,
                                  tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsAppPriGeneratedTrapCount
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsAppPriGeneratedTrapCount (UINT4 *pu4ErrorCode,
                                    tSnmpIndexList * pSnmpIndexList,
                                    tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsAppPriPortTable. */

/* LOW LEVEL Routines for Table : FsAppPriPortTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsAppPriPortTable
 Input       :  The Indices
                FsAppPriPortNumber
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsAppPriPortTable (INT4 i4FsAppPriPortNumber)
{
    tAppPriPortEntry   *pAPPortEntry = NULL;

    APP_PRI_SHUTDOWN_CHECK;

    pAPPortEntry = AppPriUtlGetPortEntry ((UINT4) i4FsAppPriPortNumber);
    if (pAPPortEntry == NULL)
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : AppPriUtlGetPortEntry () "
                       "Returns FAILURE. \r\n", __FUNCTION__);
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsAppPriPortTable
 Input       :  The Indices
                FsAppPriPortNumber
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsAppPriPortTable (INT4 *pi4FsAppPriPortNumber)
{
    tAppPriPortEntry   *pAPPortEntry = NULL;

    APP_PRI_SHUTDOWN_CHECK;
    pAPPortEntry = (tAppPriPortEntry *)
        RBTreeGetFirst (gAppPriGlobalInfo.pRbAppPriPortTbl);
    if (pAPPortEntry == NULL)
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : RbTreeGetFirst Node () "
                       "Returns NULL. \r\n", __FUNCTION__);
        return SNMP_FAILURE;
    }
    *pi4FsAppPriPortNumber = (INT4) pAPPortEntry->u4IfIndex;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsAppPriPortTable
 Input       :  The Indices
                FsAppPriPortNumber
                nextFsAppPriPortNumber
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsAppPriPortTable (INT4 i4FsAppPriPortNumber,
                                  INT4 *pi4NextFsAppPriPortNumber)
{
    tAppPriPortEntry   *pAPPortEntry = NULL;
    tAppPriPortEntry    APPortEntry;

    APP_PRI_SHUTDOWN_CHECK;
    MEMSET (&APPortEntry, DCBX_ZERO, sizeof (tAppPriPortEntry));
    APPortEntry.u4IfIndex = (UINT4) i4FsAppPriPortNumber;
    pAPPortEntry = (tAppPriPortEntry *)
        RBTreeGetNext (gAppPriGlobalInfo.pRbAppPriPortTbl, &APPortEntry,
                       AppPriUtlPortTblCmpFn);
    if (pAPPortEntry == NULL)
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : RbTreeGetFirst Node () "
                       "Returns NULL. \r\n", __FUNCTION__);
        return SNMP_FAILURE;
    }
    *pi4NextFsAppPriPortNumber = (INT4) pAPPortEntry->u4IfIndex;
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsAppPriAdminMode
 Input       :  The Indices
                FsAppPriPortNumber

                The Object 
                retValFsAppPriAdminMode
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsAppPriAdminMode (INT4 i4FsAppPriPortNumber,
                         INT4 *pi4RetValFsAppPriAdminMode)
{
    tAppPriPortEntry   *pAPPortEntry = NULL;

    pAPPortEntry = AppPriUtlGetPortEntry ((UINT4) i4FsAppPriPortNumber);

    if (pAPPortEntry == NULL)
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : AppPriUtlGetPortEntry () "
                       "Returns NULL. \r\n", __FUNCTION__);
        return SNMP_FAILURE;
    }
    *pi4RetValFsAppPriAdminMode = (INT4) pAPPortEntry->u1AppPriAdminMode;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsAppPriDcbxOperState
 Input       :  The Indices
                FsAppPriPortNumber

                The Object 
                retValFsAppPriDcbxOperState
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsAppPriDcbxOperState (INT4 i4FsAppPriPortNumber,
                             INT4 *pi4RetValFsAppPriDcbxOperState)
{
    tAppPriPortEntry   *pAPPortEntry = NULL;
    UINT1               u1DcbxVersion = DCBX_VER_UNKNOWN;

    pAPPortEntry = AppPriUtlGetPortEntry ((UINT4) i4FsAppPriPortNumber);
    if (pAPPortEntry == NULL)
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : AppPriUtlGetPortEntry () "
                       "Returned NULL. \r\n", __FUNCTION__);
        return SNMP_FAILURE;
    }

    DcbxUtlGetOperVersion ((UINT4) i4FsAppPriPortNumber, &u1DcbxVersion);
    if (u1DcbxVersion == DCBX_VER_CEE)
    {
        DCBX_GET_CEE_OPERST_FRM_IEEE(pAPPortEntry->u1AppPriDcbxOperState,
                *pi4RetValFsAppPriDcbxOperState);
    }
    else
    {
        *pi4RetValFsAppPriDcbxOperState =
            (INT4) pAPPortEntry->u1AppPriDcbxOperState;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsAppPriDcbxStateMachine
 Input       :  The Indices
                FsAppPriPortNumber

                The Object 
                retValFsAppPriDcbxStateMachine
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsAppPriDcbxStateMachine (INT4 i4FsAppPriPortNumber,
                                INT4 *pi4RetValFsAppPriDcbxStateMachine)
{
    tAppPriPortEntry   *pAPPortEntry = NULL;

    pAPPortEntry = AppPriUtlGetPortEntry ((UINT4) i4FsAppPriPortNumber);

    if (pAPPortEntry == NULL)
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : AppPriUtlGetPortEntry () "
                       "Returns NULL. \r\n", __FUNCTION__);
        return SNMP_FAILURE;
    }
    *pi4RetValFsAppPriDcbxStateMachine =
        (INT4) pAPPortEntry->u1AppPriDcbxSemType;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsAppPriClearTLVCounters
 Input       :  The Indices
                FsAppPriPortNumber

                The Object 
                retValFsAppPriClearTLVCounters
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsAppPriClearTLVCounters (INT4 i4FsAppPriPortNumber,
                                INT4 *pi4RetValFsAppPriClearTLVCounters)
{
    tAppPriPortEntry   *pAPPortEntry = NULL;

    pAPPortEntry = AppPriUtlGetPortEntry ((UINT4) i4FsAppPriPortNumber);

    if (pAPPortEntry == NULL)
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : AppPriUtlGetPortEntry () "
                       "Returns NULL. \r\n", __FUNCTION__);
        return SNMP_FAILURE;
    }
    *pi4RetValFsAppPriClearTLVCounters = APP_PRI_DISABLED;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsAppPriTxTLVCounter
 Input       :  The Indices
                FsAppPriPortNumber

                The Object 
                retValFsAppPriTxTLVCounter
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsAppPriTxTLVCounter (INT4 i4FsAppPriPortNumber,
                            UINT4 *pu4RetValFsAppPriTxTLVCounter)
{
    tAppPriPortEntry   *pAPPortEntry = NULL;

    pAPPortEntry = AppPriUtlGetPortEntry ((UINT4) i4FsAppPriPortNumber);

    if (pAPPortEntry == NULL)
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : AppPriUtlGetPortEntry () "
                       "Returns NULL. \r\n", __FUNCTION__);
        return SNMP_FAILURE;
    }
    *pu4RetValFsAppPriTxTLVCounter = pAPPortEntry->u4AppPriTLVTxCount;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsAppPriRxTLVCounter
 Input       :  The Indices
                FsAppPriPortNumber

                The Object 
                retValFsAppPriRxTLVCounter
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsAppPriRxTLVCounter (INT4 i4FsAppPriPortNumber,
                            UINT4 *pu4RetValFsAppPriRxTLVCounter)
{
    tAppPriPortEntry   *pAPPortEntry = NULL;

    pAPPortEntry = AppPriUtlGetPortEntry ((UINT4) i4FsAppPriPortNumber);

    if (pAPPortEntry == NULL)
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : AppPriUtlGetPortEntry () "
                       "Returns NULL. \r\n", __FUNCTION__);
        return SNMP_FAILURE;
    }
    *pu4RetValFsAppPriRxTLVCounter = pAPPortEntry->u4AppPriTLVRxCount;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsAppPriRxTLVErrors
 Input       :  The Indices
                FsAppPriPortNumber

                The Object 
                retValFsAppPriRxTLVErrors
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsAppPriRxTLVErrors (INT4 i4FsAppPriPortNumber,
                           UINT4 *pu4RetValFsAppPriRxTLVErrors)
{
    tAppPriPortEntry   *pAPPortEntry = NULL;

    pAPPortEntry = AppPriUtlGetPortEntry ((UINT4) i4FsAppPriPortNumber);

    if (pAPPortEntry == NULL)
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : AppPriUtlGetPortEntry () "
                       "Returns NULL. \r\n", __FUNCTION__);
        return SNMP_FAILURE;
    }
    *pu4RetValFsAppPriRxTLVErrors = pAPPortEntry->u4AppPriTLVRxError;
    return SNMP_SUCCESS;
}

/****************************************************************************
 *  Function    :  nmhGetFsAppPriAppProtocols
 *  Input       :  The Indices
 *                 FsAppPriPortNumber
 *      
 *                 The Object
 *                 retValFsAppPriAppProtocols
 *  Output      :  The Get Low Lev Routine Take the Indices &
 *                 store the Value requested in the Return val.
 *  Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 *****************************************************************************/
INT1
nmhGetFsAppPriAppProtocols (INT4 i4FsAppPriPortNumber,
                            UINT4 *pu4RetValFsAppPriAppProtocols)
{
    tAppPriPortEntry   *pAPPortEntry = NULL;

    pAPPortEntry = AppPriUtlGetPortEntry ((UINT4) i4FsAppPriPortNumber);

    if (pAPPortEntry == NULL)
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : AppPriUtlGetPortEntry () "
                       "Returns NULL. \r\n", __FUNCTION__);
        return SNMP_FAILURE;
    }
    *pu4RetValFsAppPriAppProtocols = pAPPortEntry->u4AppPriAppProtocols;
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsAppPriRowStatus
 Input       :  The Indices
                FsAppPriPortNumber

                The Object 
                retValFsAppPriRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsAppPriRowStatus (INT4 i4FsAppPriPortNumber,
                         INT4 *pi4RetValFsAppPriRowStatus)
{
    tAppPriPortEntry   *pAPPortEntry = NULL;

    pAPPortEntry = AppPriUtlGetPortEntry ((UINT4) i4FsAppPriPortNumber);

    if (pAPPortEntry == NULL)
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : AppPriUtlGetPortEntry () "
                       "Returns NULL. \r\n", __FUNCTION__);
        return SNMP_FAILURE;
    }
    *pi4RetValFsAppPriRowStatus = pAPPortEntry->u1AppPriRowStatus;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsAppPriSyncd
 Input       :  The Indices
                FsAppPriPortNumber

                The Object 
                retValFsAppPriSyncd
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsAppPriSyncd(INT4 i4FsAppPriPortNumber , INT4 *pi4RetValFsAppPriSyncd)
{
    tAppPriPortEntry   *pAPPortEntry = NULL;

    pAPPortEntry = AppPriUtlGetPortEntry ((UINT4) i4FsAppPriPortNumber);

    if (pAPPortEntry == NULL)
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : AppPriUtlGetPortEntry () "
                       "Returns NULL. \r\n", __FUNCTION__);
        return SNMP_FAILURE;
    }
    *pi4RetValFsAppPriSyncd = pAPPortEntry->bAppPriSyncd;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsAppPriError
 Input       :  The Indices
                FsAppPriPortNumber

                The Object 
                retValFsAppPriError
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsAppPriError(INT4 i4FsAppPriPortNumber , INT4 *pi4RetValFsAppPriError)
{
    tAppPriPortEntry   *pAPPortEntry = NULL;

    pAPPortEntry = AppPriUtlGetPortEntry ((UINT4) i4FsAppPriPortNumber);

    if (pAPPortEntry == NULL)
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : AppPriUtlGetPortEntry () "
                       "Returns NULL. \r\n", __FUNCTION__);
        return SNMP_FAILURE;
    }
    *pi4RetValFsAppPriError = pAPPortEntry->bAppPriError;
    return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetFsAppPriDcbxStatus
 Input       :  The Indices
                FsAppPriPortNumber

                The Object 
                retValFsAppPriDcbxStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsAppPriDcbxStatus(INT4 i4FsAppPriPortNumber , INT4 *pi4RetValFsAppPriDcbxStatus)
{
    tAppPriPortEntry   *pAPPortEntry = NULL;

    pAPPortEntry = AppPriUtlGetPortEntry ((UINT4) i4FsAppPriPortNumber);

    if (pAPPortEntry == NULL)
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : AppPriUtlGetPortEntry () "
                       "Returns NULL. \r\n", __FUNCTION__);
        return SNMP_FAILURE;
    }
    *pi4RetValFsAppPriDcbxStatus = pAPPortEntry->u1DcbxStatus;
    return SNMP_SUCCESS;
}
/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsAppPriAdminMode
 Input       :  The Indices
                FsAppPriPortNumber

                The Object 
                setValFsAppPriAdminMode
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsAppPriAdminMode (INT4 i4FsAppPriPortNumber,
                         INT4 i4SetValFsAppPriAdminMode)
{
    tAppPriPortEntry   *pAPPortEntry = NULL;

    DCBX_TRC_ARG3 (DCBX_MGMT_TRC, "%s, Args: "
            "i4FsAppPriPortNumber: %d "
            "i4SetValFsAppPriAdminMode: %d\r\n", 
            __func__, i4FsAppPriPortNumber, i4SetValFsAppPriAdminMode);

    pAPPortEntry = AppPriUtlGetPortEntry ((UINT4) i4FsAppPriPortNumber);
    if (pAPPortEntry == NULL)
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : AppPriUtlGetPortEntry () "
                       "Returns FAILURE. \r\n", __FUNCTION__);
        return SNMP_FAILURE;
    }

    if (pAPPortEntry->u1AppPriAdminMode != (UINT1) i4SetValFsAppPriAdminMode)
    {

        AppPriUtlAdminModeChange (pAPPortEntry,
                                  (UINT1) i4SetValFsAppPriAdminMode);
        /* Updation of Admin mode chage data structure will be taken
         * care inside the Admin Mode change function */
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsAppPriClearTLVCounters
 Input       :  The Indices
                FsAppPriPortNumber

                The Object 
                setValFsAppPriClearTLVCounters
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsAppPriClearTLVCounters (INT4 i4FsAppPriPortNumber,
                                INT4 i4SetValFsAppPriClearTLVCounters)
{
    tAppPriPortEntry   *pAPPortEntry = NULL;

    DCBX_TRC_ARG3 (DCBX_MGMT_TRC, "%s, Args: "
            "i4FsAppPriPortNumber: %d "
            "i4SetValFsAppPriClearTLVCounters: %d\r\n", 
            __func__, i4FsAppPriPortNumber, i4SetValFsAppPriClearTLVCounters);

    pAPPortEntry = AppPriUtlGetPortEntry ((UINT4) i4FsAppPriPortNumber);
    if (pAPPortEntry == NULL)
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : AppPriUtlGetPortEntry () "
                       "Returns FAILURE. \r\n", __FUNCTION__);
        return SNMP_FAILURE;
    }
    if (i4SetValFsAppPriClearTLVCounters == APP_PRI_ENABLED)
    {
        pAPPortEntry->u4AppPriTLVTxCount = DCBX_ZERO;
        pAPPortEntry->u4AppPriTLVRxError = DCBX_ZERO;
        pAPPortEntry->u4AppPriTLVRxCount = DCBX_ZERO;
        pAPPortEntry->u4AppPriAppProtocols = DCBX_ZERO;

    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsAppPriRowStatus
 Input       :  The Indices
                FsAppPriPortNumber

                The Object 
                setValFsAppPriRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsAppPriRowStatus (INT4 i4FsAppPriPortNumber,
                         INT4 i4SetValFsAppPriRowStatus)
{
    tAppPriPortEntry   *pAPPortEntry = NULL;
    tCEECtrlEntry      *pCEECtrlEntry = NULL;
    tDcbxPortEntry     *pDcbxPortEntry = NULL;
    INT4                i4RetVal = OSIX_SUCCESS;

    DCBX_TRC_ARG3 (DCBX_MGMT_TRC, "%s, Args: "
            "i4FsAppPriPortNumber: %d "
            "i4SetValFsAppPriRowStatus: %d\r\n", 
            __func__, i4FsAppPriPortNumber, i4SetValFsAppPriRowStatus);

    pDcbxPortEntry = DcbxUtlGetPortEntry ((UINT4) i4FsAppPriPortNumber);
    if (pDcbxPortEntry == NULL)
    {
        DCBX_TRC_ARG2 (MGMT_TRC | ALL_FAILURE_TRC, "%s : "
                "pDcbxPortEntry (Port) Id %d is invalid \r\n",
                __func__, i4FsAppPriPortNumber);
        return SNMP_FAILURE;
    }

    switch (i4SetValFsAppPriRowStatus)
    {
        case CREATE_AND_WAIT:    /* Intentional fall through for MSR */
        case CREATE_AND_GO:

            /* Memblock creation and initializing the default values
             * to the ApplicationPriority  Node for this port  are handled inside the below
             * function and  rowstatus is initialized here */
            pAPPortEntry =
                AppPriUtlCreatePortTblEntry ((UINT4) i4FsAppPriPortNumber);
            if (pAPPortEntry == NULL)
            {
                DCBX_TRC_ARG1 (DCBX_MGMT_TRC,
                               "In %s : AppPriUtlCreatePortTblEntry () "
                               "Returns FAILURE. \r\n", __FUNCTION__);
                return SNMP_FAILURE;
            }
            pCEECtrlEntry =
                CEEUtlCreateCtrlTblEntry ((UINT4) i4FsAppPriPortNumber);
            if (pCEECtrlEntry == NULL)
            {
                DCBX_TRC_ARG1 (DCBX_MGMT_TRC | DCBX_FAILURE_TRC,
                               "In %s : CEEUtlCreateCtrlTblEntry () "
                               "Returns FAILURE. \r\n", __FUNCTION__);
                return SNMP_FAILURE;
            }
            pDcbxPortEntry->u1AppPriAdminStatus = APP_PRI_ENABLED;
            pAPPortEntry->u1AppPriRowStatus = ACTIVE;
            break;

        case ACTIVE:
            break;

        case DESTROY:
            pAPPortEntry = AppPriUtlGetPortEntry ((UINT4) i4FsAppPriPortNumber);
            if (pAPPortEntry == NULL)
            {
                return SNMP_SUCCESS;
            }

            /* Delete the Application Priority Entry */
            i4RetVal = AppPriUtlDeletePortTblEntry (pAPPortEntry);
            if (i4RetVal != OSIX_SUCCESS)
            {
                DCBX_TRC_ARG1 (DCBX_MGMT_TRC,
                               "In %s : AppPriUtlDeletePortTblEntry () "
                               "Returns FAILURE. \r\n", __FUNCTION__);
                return SNMP_FAILURE;
            }
            pCEECtrlEntry =
                CEEUtlGetCtrlEntry ((UINT4) i4FsAppPriPortNumber);
            if (pCEECtrlEntry == NULL)
            {
                return SNMP_SUCCESS;
            }
            i4RetVal = CEEUtlDeleteCtrlTblEntry (pCEECtrlEntry);
            if (i4RetVal != OSIX_SUCCESS)
            {
                DCBX_TRC_ARG1 (DCBX_MGMT_TRC | DCBX_FAILURE_TRC,
                               "In %s : CEEUtlDeleteCtrlTblEntry () "
                               "Returns FAILURE. \r\n", __FUNCTION__);
                return SNMP_FAILURE;
            }
            pDcbxPortEntry->u1AppPriAdminStatus = APP_PRI_DISABLED;
            break;
        default:
            return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsAppPriAdminMode
 Input       :  The Indices
                FsAppPriPortNumber

                The Object 
                testValFsAppPriAdminMode
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsAppPriAdminMode (UINT4 *pu4ErrorCode, INT4 i4FsAppPriPortNumber,
                            INT4 i4TestValFsAppPriAdminMode)
{
    tAppPriPortEntry   *pAPPortEntry = NULL;

    APP_PRI_SHUTDOWN_CHECK_TEST;

    if ((i4TestValFsAppPriAdminMode < APP_PRI_PORT_MODE_AUTO) ||
        (i4TestValFsAppPriAdminMode > APP_PRI_PORT_MODE_OFF))
    {
        DCBX_TRC_ARG2 (DCBX_MGMT_TRC, "In %s : "
                       "Failed while setting the value to %d \r\n",
                       __FUNCTION__, i4TestValFsAppPriAdminMode);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_ERR_APP_PRI_INVALID_PORT_MODE_STATUS);
        return SNMP_FAILURE;
    }

    if (DcbxUtlValidatePort ((UINT4) i4FsAppPriPortNumber) == OSIX_FAILURE)
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : DcbxUtlValidatePort() "
                       "Returns Failure. \r\n", __FUNCTION__);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_ERR_DCBX_NO_PORT);
        return SNMP_FAILURE;
    }

    pAPPortEntry = AppPriUtlGetPortEntry ((UINT4) i4FsAppPriPortNumber);
    if (pAPPortEntry == NULL)
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : AppPriUtlGetPortEntry () "
                       "Returns FAILURE. \r\n", __FUNCTION__);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_ERR_APP_PRI_NO_PORT_ENTRY);
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsAppPriClearTLVCounters
 Input       :  The Indices
                FsAppPriPortNumber

                The Object 
                testValFsAppPriClearTLVCounters
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsAppPriClearTLVCounters (UINT4 *pu4ErrorCode,
                                   INT4 i4FsAppPriPortNumber,
                                   INT4 i4TestValFsAppPriClearTLVCounters)
{
    tAppPriPortEntry   *pAPPortEntry = NULL;

    APP_PRI_SHUTDOWN_CHECK_TEST;

    if ((i4TestValFsAppPriClearTLVCounters != APP_PRI_DISABLED) &&
        (i4TestValFsAppPriClearTLVCounters != APP_PRI_ENABLED))
    {
        DCBX_TRC_ARG2 (DCBX_MGMT_TRC, "In %s : "
                       "Failed while setting value to %d\r\n",
                       __FUNCTION__, i4TestValFsAppPriClearTLVCounters);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_ERR_APP_PRI_INVALID_CLEAR_STATUS);
        return SNMP_FAILURE;
    }
    if (DcbxUtlValidatePort ((UINT4) i4FsAppPriPortNumber) == OSIX_FAILURE)
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : DcbxUtlValidatePort() "
                       "Returns Failure. \r\n", __FUNCTION__);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_ERR_DCBX_NO_PORT);
        return SNMP_FAILURE;
    }

    pAPPortEntry = AppPriUtlGetPortEntry ((UINT4) i4FsAppPriPortNumber);
    if (pAPPortEntry == NULL)
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : AppPriUtlGetPortEntry () "
                       "Returns FAILURE. \r\n", __FUNCTION__);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_ERR_APP_PRI_NO_PORT_ENTRY);
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsAppPriRowStatus
 Input       :  The Indices
                FsAppPriPortNumber

                The Object 
                testValFsAppPriRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsAppPriRowStatus (UINT4 *pu4ErrorCode,
                            INT4 i4FsAppPriPortNumber,
                            INT4 i4TestValFsAppPriRowStatus)
{
    tAppPriPortEntry   *pAPPortEntry = NULL;

    APP_PRI_SHUTDOWN_CHECK_TEST;

    if (DcbxUtlValidatePort ((UINT4) i4FsAppPriPortNumber) == OSIX_FAILURE)
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : DcbxUtlValidatePort() "
                       "Returns Failure. \r\n", __FUNCTION__);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        /* Currently, DCBX shouldn't be allowed in 
           Port-Channel Interface, setting appropriate error code 
        */
        if (CfaIsLaggInterface ((UINT4)i4FsAppPriPortNumber) == CFA_TRUE) 
        {
          CLI_SET_ERR (CLI_ERR_DCBX_PORT_IN_PORT_CHANNEL);
        }
        else
        {
          CLI_SET_ERR (CLI_ERR_DCBX_NO_PORT);
        }
        return SNMP_FAILURE;
    }

    pAPPortEntry = AppPriUtlGetPortEntry ((UINT4) i4FsAppPriPortNumber);
    switch (i4TestValFsAppPriRowStatus)
    {
        case CREATE_AND_GO:

            /* If ApplicationPriority Node is present for this port and
             * trying to create the same entry then
             * return failure */
            if (pAPPortEntry != NULL)
            {
                DCBX_TRC_ARG2 (DCBX_MGMT_TRC, "In %s : "
                               "Port Entry with IfIndex %d "
                               "already present\r\n",
                               __FUNCTION__, i4TestValFsAppPriRowStatus);
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                CLI_SET_ERR (CLI_ERR_APP_PRI_PORT_ALREADY_PRESENT);
                return (SNMP_FAILURE);
            }
            if ((MemGetFreeUnits (gAppPriGlobalInfo.AppPriPortPoolId)) ==
                DCBX_INIT_VAL)
            {
                DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : "
                               "Memory not available to create the"
                               " port entry \r\n", __FUNCTION__);
                *pu4ErrorCode = SNMP_ERR_NO_CREATION;
                CLI_SET_ERR (CLI_ERR_APP_PRI_PORT_CREATION_FAILS);
                return SNMP_FAILURE;
            }
            break;

        case DESTROY:

            if (pAPPortEntry == NULL)
            {
                DCBX_TRC_ARG1 (DCBX_MGMT_TRC,
                               "In %s : AppPriUtlGetPortEntry () "
                               "Returns FAILURE. \r\n", __FUNCTION__);
                *pu4ErrorCode = SNMP_ERR_NO_CREATION;
                CLI_SET_ERR (CLI_ERR_APP_PRI_NO_PORT_ENTRY);
                return SNMP_FAILURE;
            }
            break;

        case ACTIVE:
            break;

        default:
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsAppPriPortTable
 Input       :  The Indices
                FsAppPriPortNumber
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsAppPriPortTable (UINT4 *pu4ErrorCode,
                           tSnmpIndexList * pSnmpIndexList,
                           tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsAppPriXAppTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsAppPriXAppTable
 Input       :  The Indices
                LldpV2LocPortIfIndex
                LldpXdot1dcbxAdminApplicationPriorityAESelector
                LldpXdot1dcbxAdminApplicationPriorityAEProtocol
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsAppPriXAppTable (INT4 i4LldpV2LocPortIfIndex,
                                           INT4
                                           i4LldpXdot1dcbxAdminApplicationPriorityAESelector,
                                           UINT4
                                           u4LldpXdot1dcbxAdminApplicationPriorityAEProtocol)
{
    return nmhValidateIndexInstanceLldpXdot1dcbxAdminApplicationPriorityAppTable
        (i4LldpV2LocPortIfIndex,
         i4LldpXdot1dcbxAdminApplicationPriorityAESelector,
         u4LldpXdot1dcbxAdminApplicationPriorityAEProtocol);

}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsAppPriXAppTable
 Input       :  The Indices
                LldpV2LocPortIfIndex
                LldpXdot1dcbxAdminApplicationPriorityAESelector
                LldpXdot1dcbxAdminApplicationPriorityAEProtocol
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsAppPriXAppTable (INT4 *pi4LldpV2LocPortIfIndex,
                                   INT4
                                   *pi4LldpXdot1dcbxAdminApplicationPriorityAESelector,
                                   UINT4
                                   *pu4LldpXdot1dcbxAdminApplicationPriorityAEProtocol)
{
    return nmhGetFirstIndexLldpXdot1dcbxAdminApplicationPriorityAppTable
        (pi4LldpV2LocPortIfIndex,
         pi4LldpXdot1dcbxAdminApplicationPriorityAESelector,
         pu4LldpXdot1dcbxAdminApplicationPriorityAEProtocol);
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsAppPriXAppTable
 Input       :  The Indices
                LldpV2LocPortIfIndex
                nextLldpV2LocPortIfIndex
                LldpXdot1dcbxAdminApplicationPriorityAESelector
                nextLldpXdot1dcbxAdminApplicationPriorityAESelector
                LldpXdot1dcbxAdminApplicationPriorityAEProtocol
                nextLldpXdot1dcbxAdminApplicationPriorityAEProtocol
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsAppPriXAppTable (INT4 i4LldpV2LocPortIfIndex,
                                  INT4 *pi4NextLldpV2LocPortIfIndex,
                                  INT4
                                  i4LldpXdot1dcbxAdminApplicationPriorityAESelector,
                                  INT4
                                  *pi4NextLldpXdot1dcbxAdminApplicationPriorityAESelector,
                                  UINT4
                                  u4LldpXdot1dcbxAdminApplicationPriorityAEProtocol,
                                  UINT4
                                  *pu4NextLldpXdot1dcbxAdminApplicationPriorityAEProtocol)
{
    return nmhGetNextIndexLldpXdot1dcbxAdminApplicationPriorityAppTable
        (i4LldpV2LocPortIfIndex,
         pi4NextLldpV2LocPortIfIndex,
         i4LldpXdot1dcbxAdminApplicationPriorityAESelector,
         pi4NextLldpXdot1dcbxAdminApplicationPriorityAESelector,
         u4LldpXdot1dcbxAdminApplicationPriorityAEProtocol,
         pu4NextLldpXdot1dcbxAdminApplicationPriorityAEProtocol);
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsAppPriXAppRowStatus
 Input       :  The Indices
                LldpV2LocPortIfIndex
                LldpXdot1dcbxAdminApplicationPriorityAESelector
                LldpXdot1dcbxAdminApplicationPriorityAEProtocol

                The Object 
                retValFsAppPriXAppRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsAppPriXAppRowStatus (INT4 i4LldpV2LocPortIfIndex,
                             INT4
                             i4LldpXdot1dcbxAdminApplicationPriorityAESelector,
                             UINT4
                             u4LldpXdot1dcbxAdminApplicationPriorityAEProtocol,
                             INT4 *pi4RetValFsAppPriXAppRowStatus)
{
    tAppPriPortEntry   *pAppPriPortEntry = NULL;
    tAppPriMappingEntry *pAppPriMappingEntry = NULL;

    pAppPriPortEntry = AppPriUtlGetPortEntry ((UINT4) i4LldpV2LocPortIfIndex);
    if (pAppPriPortEntry == NULL)
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : AppPriUtlGetPortEntry () "
                       "Returns FAILURE. \r\n", __FUNCTION__);
        return SNMP_FAILURE;
    }
    pAppPriMappingEntry = AppPriUtlGetAppPriMappingEntry
        ((UINT4) i4LldpV2LocPortIfIndex,
         i4LldpXdot1dcbxAdminApplicationPriorityAESelector,
         (INT4)u4LldpXdot1dcbxAdminApplicationPriorityAEProtocol, APP_PRI_ADMIN);
    if (pAppPriMappingEntry == NULL)
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : "
                       "AppPriUtlGetAppPriMappingEntry () "
                       "Returns FAILURE. \r\n", __FUNCTION__);
        return SNMP_FAILURE;
    }
    *pi4RetValFsAppPriXAppRowStatus = pAppPriMappingEntry->u1RowStatus;
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsAppPriXAppRowStatus
 Input       :  The Indices
                LldpV2LocPortIfIndex
                LldpXdot1dcbxAdminApplicationPriorityAESelector
                LldpXdot1dcbxAdminApplicationPriorityAEProtocol

                The Object 
                setValFsAppPriXAppRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsAppPriXAppRowStatus (INT4 i4LldpV2LocPortIfIndex,
                             INT4
                             i4LldpXdot1dcbxAdminApplicationPriorityAESelector,
                             UINT4
                             u4LldpXdot1dcbxAdminApplicationPriorityAEProtocol,
                             INT4 i4SetValFsAppPriXAppRowStatus)
{
    tAppPriPortEntry   *pAppPriPortEntry = NULL;
    tAppPriMappingEntry *pAppPriMappingEntry = NULL;

    DCBX_TRC_ARG5 (DCBX_MGMT_TRC, "%s, Args: "
            "i4LldpV2LocPortIfIndex: %d "
            "i4LldpXdot1dcbxAdminApplicationPriorityAESelector: %d "
            "u4LldpXdot1dcbxAdminApplicationPriorityAEProtocol: %d"
            "i4SetValFsAppPriXAppRowStatus: %d\r\n", __func__,
            i4LldpV2LocPortIfIndex,
            i4LldpXdot1dcbxAdminApplicationPriorityAESelector,
            u4LldpXdot1dcbxAdminApplicationPriorityAEProtocol,
            i4SetValFsAppPriXAppRowStatus);

    pAppPriPortEntry = AppPriUtlGetPortEntry ((UINT4) i4LldpV2LocPortIfIndex);
    if (pAppPriPortEntry == NULL)
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : AppPriUtlGetPortEntry () "
                       "Returns FAILURE. \r\n", __FUNCTION__);
        return SNMP_SUCCESS;
    }

    switch (i4SetValFsAppPriXAppRowStatus)
    {
        case CREATE_AND_GO:
            pAppPriMappingEntry = AppPriUtlCreateAppPriMappingTblEntry
                ((UINT4) i4LldpV2LocPortIfIndex,
                 i4LldpXdot1dcbxAdminApplicationPriorityAESelector,
                 (INT4)u4LldpXdot1dcbxAdminApplicationPriorityAEProtocol,
                 APP_PRI_ADMIN);
            if (pAppPriMappingEntry == NULL)
            {
                DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : "
                               "AppPriUtlCreateAppPriMappingTblEntry () "
                               "Returns NULL. \r\n", __FUNCTION__);
                return SNMP_FAILURE;
            }
            if (pAppPriPortEntry->u1AppPriDcbxOperState == APP_PRI_OPER_INIT)
            {
                if (AppPriUtlHwConfigAppPriMappingEntry
                    ((UINT4) i4LldpV2LocPortIfIndex,
                     i4LldpXdot1dcbxAdminApplicationPriorityAESelector,
                     pAppPriMappingEntry, APP_PRI_HW_CREATE) == OSIX_FAILURE)
                {
                    DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : "
                                   "AppPriUtlHwConfigAppPriMappingEntry () "
                                   "Returns FAILURE. \r\n", __FUNCTION__);
                    return SNMP_FAILURE;
                }
            }
            pAppPriMappingEntry->u1RowStatus = ACTIVE;
            AppPriUtlAdminParamChange (pAppPriPortEntry);
            break;
        case CREATE_AND_WAIT:
            pAppPriMappingEntry = AppPriUtlCreateAppPriMappingTblEntry
                ((UINT4) i4LldpV2LocPortIfIndex,
                 i4LldpXdot1dcbxAdminApplicationPriorityAESelector,
                 (INT4)u4LldpXdot1dcbxAdminApplicationPriorityAEProtocol,
                 APP_PRI_ADMIN);
            if (pAppPriMappingEntry == NULL)
            {
                DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : "
                               "AppPriUtlCreateAppPriMappingTblEntry () "
                               "Returns NULL. \r\n", __FUNCTION__);
                return SNMP_FAILURE;
            }
            pAppPriMappingEntry->u1RowStatus = NOT_READY;
            break;
        case ACTIVE:
            pAppPriMappingEntry = AppPriUtlGetAppPriMappingEntry
                ((UINT4) i4LldpV2LocPortIfIndex,
                 i4LldpXdot1dcbxAdminApplicationPriorityAESelector,
                 (INT4)u4LldpXdot1dcbxAdminApplicationPriorityAEProtocol,
                 APP_PRI_ADMIN);
            if (pAppPriMappingEntry == NULL)
            {
                DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : "
                               "AppPriUtlGetAppPriMappingEntry () "
                               "Returns NULL. \r\n", __FUNCTION__);
                return SNMP_FAILURE;
            }
            if (pAppPriPortEntry->u1AppPriDcbxOperState == APP_PRI_OPER_INIT)
            {
                if (AppPriUtlHwConfigAppPriMappingEntry
                    ((UINT4) i4LldpV2LocPortIfIndex,
                     i4LldpXdot1dcbxAdminApplicationPriorityAESelector,
                     pAppPriMappingEntry, APP_PRI_HW_CREATE) == OSIX_FAILURE)
                {
                    DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : "
                                   "AppPriUtlHwConfigAppPriMappingEntry () "
                                   "Returns FAILURE. \r\n", __FUNCTION__);
                    return SNMP_FAILURE;
                }
            }
            pAppPriMappingEntry->u1RowStatus = ACTIVE;
            AppPriUtlAdminParamChange (pAppPriPortEntry);
            break;
        case NOT_IN_SERVICE:
            pAppPriMappingEntry = AppPriUtlGetAppPriMappingEntry
                ((UINT4) i4LldpV2LocPortIfIndex,
                 i4LldpXdot1dcbxAdminApplicationPriorityAESelector,
                 (INT4)u4LldpXdot1dcbxAdminApplicationPriorityAEProtocol,
                 APP_PRI_ADMIN);
            if (pAppPriMappingEntry == NULL)
            {
                DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : "
                               "AppPriUtlGetAppPriMappingEntry () "
                               "Returns NULL. \r\n", __FUNCTION__);
                return SNMP_FAILURE;
            }
            if (pAppPriPortEntry->u1AppPriDcbxOperState == APP_PRI_OPER_INIT)
            {
                if (AppPriUtlHwConfigAppPriMappingEntry
                    ((UINT4) i4LldpV2LocPortIfIndex,
                     i4LldpXdot1dcbxAdminApplicationPriorityAESelector,
                     pAppPriMappingEntry, APP_PRI_HW_DELETE) == OSIX_FAILURE)
                {
                    DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : "
                                   "AppPriUtlHwConfigAppPriMappingEntry () "
                                   "Returns FAILURE. \r\n", __FUNCTION__);
                    return SNMP_FAILURE;
                }
            }
            pAppPriMappingEntry->u1RowStatus = NOT_IN_SERVICE;
            break;
        case DESTROY:
            pAppPriMappingEntry = AppPriUtlGetAppPriMappingEntry
                ((UINT4) i4LldpV2LocPortIfIndex,
                 i4LldpXdot1dcbxAdminApplicationPriorityAESelector,
                 (INT4)u4LldpXdot1dcbxAdminApplicationPriorityAEProtocol,
                 APP_PRI_ADMIN);
            if (pAppPriMappingEntry == NULL)
            {
                DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : "
                               "AppPriUtlGetAppPriMappingEntry () "
                               "Returns NULL. \r\n", __FUNCTION__);
                return SNMP_FAILURE;
            }
            if (pAppPriPortEntry->u1AppPriDcbxOperState == APP_PRI_OPER_INIT)
            {
                if (AppPriUtlHwConfigAppPriMappingEntry
                    ((UINT4) i4LldpV2LocPortIfIndex,
                     i4LldpXdot1dcbxAdminApplicationPriorityAESelector,
                     pAppPriMappingEntry, APP_PRI_HW_DELETE) == OSIX_FAILURE)
                {
                    DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : "
                                   "AppPriUtlHwConfigAppPriMappingEntry () "
                                   "Returns FAILURE. \r\n", __FUNCTION__);
                    return SNMP_FAILURE;
                }
            }
            if (AppPriUtlDeleteAppPriMappingEntry
                ((UINT4) i4LldpV2LocPortIfIndex,
                 i4LldpXdot1dcbxAdminApplicationPriorityAESelector,
                 (INT4)u4LldpXdot1dcbxAdminApplicationPriorityAEProtocol,
                 APP_PRI_ADMIN) == OSIX_FAILURE)
            {
                DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : "
                               "AppPriUtlDeleteAppPriMappingTblEntry () "
                               "Returns FAILURE. \r\n", __FUNCTION__);
                return SNMP_FAILURE;
            }
            AppPriUtlAdminParamChange (pAppPriPortEntry);
            break;
        default:
            break;
    }
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsAppPriXAppRowStatus
 Input       :  The Indices
                LldpV2LocPortIfIndex
                LldpXdot1dcbxAdminApplicationPriorityAESelector
                LldpXdot1dcbxAdminApplicationPriorityAEProtocol

                The Object 
                testValFsAppPriXAppRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsAppPriXAppRowStatus (UINT4 *pu4ErrorCode,
                                INT4 i4LldpV2LocPortIfIndex,
                                INT4
                                i4LldpXdot1dcbxAdminApplicationPriorityAESelector,
                                UINT4
                                u4LldpXdot1dcbxAdminApplicationPriorityAEProtocol,
                                INT4 i4TestValFsAppPriXAppRowStatus)
{
    tAppPriPortEntry   *pAppPortEntry = NULL;
    tAppPriMappingEntry *pAppPriMappingEntry = NULL;
    UINT4               u4Count = DCBX_ZERO;

    APP_PRI_SHUTDOWN_CHECK_TEST;

    if (DcbxUtlValidatePort ((UINT4) i4LldpV2LocPortIfIndex) == OSIX_FAILURE)
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : DcbxUtlValidatePort() "
                       "Returns Failure. \r\n", __FUNCTION__);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_ERR_DCBX_NO_PORT);
        return SNMP_FAILURE;
    }

    pAppPortEntry = AppPriUtlGetPortEntry ((UINT4) i4LldpV2LocPortIfIndex);
    if (pAppPortEntry == NULL)
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : AppPriUtlGetPortEntry () "
                       " Returns FAILURE. \r\n", __FUNCTION__);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_ERR_APP_PRI_NO_PORT_ENTRY);
        return SNMP_FAILURE;
    }

    if ((i4LldpXdot1dcbxAdminApplicationPriorityAESelector < DCBX_ONE) ||
        (i4LldpXdot1dcbxAdminApplicationPriorityAESelector >
         APP_PRI_MAX_SELECTOR))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_ERR_APP_PRI_INVALID_SELECTOR);
        DCBX_TRC_ARG2 (DCBX_MGMT_TRC,
                       " In %s : "
                       "Failed while setting selector to value %d \r\n",
                       __FUNCTION__,
                       i4LldpXdot1dcbxAdminApplicationPriorityAESelector);
        return SNMP_FAILURE;
    }

    if (((INT4) 
         u4LldpXdot1dcbxAdminApplicationPriorityAEProtocol < DCBX_ZERO) ||
        (u4LldpXdot1dcbxAdminApplicationPriorityAEProtocol >
         APP_PRI_MAX_PROTOCOL_ID))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_ERR_APP_PRI_INVALID_PROTOCOL);
        DCBX_TRC_ARG2 (DCBX_MGMT_TRC,
                       "In %s : "
                       " Failed while setting the value %d for "
                       "Application Protocol ID. \r\n",
                       __FUNCTION__,
                       u4LldpXdot1dcbxAdminApplicationPriorityAEProtocol);
        return SNMP_FAILURE;
    }

    if ((i4TestValFsAppPriXAppRowStatus == CREATE_AND_WAIT) ||
        (i4TestValFsAppPriXAppRowStatus == CREATE_AND_GO))
    {
        /* Check if maximum number of entries is already created */
        pAppPriMappingEntry = AppPriUtlGetAppPriMappingEntry
            ((UINT4) i4LldpV2LocPortIfIndex,
             i4LldpXdot1dcbxAdminApplicationPriorityAESelector,
             (INT4)u4LldpXdot1dcbxAdminApplicationPriorityAEProtocol, APP_PRI_ADMIN);
        if (pAppPriMappingEntry == NULL)
        {
            u4Count = TMO_SLL_Count
                (&(APP_PRI_ADM_TBL (pAppPortEntry,
                                    i4LldpXdot1dcbxAdminApplicationPriorityAESelector - 1)));
            if (u4Count > APP_PRI_MAX_PROTOCOL)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                CLI_SET_ERR (CLI_ERR_APP_PRI_MAX_APP_PRI_MAPPING);
                DCBX_TRC_ARG1 (DCBX_MGMT_TRC,
                               "In %s : Maximum number of Application to Priority "
                               "Mappings already done \r\n", __FUNCTION__);
                return SNMP_FAILURE;
            }
        }
    }

    if (i4TestValFsAppPriXAppRowStatus == DESTROY)
    {
        pAppPriMappingEntry = AppPriUtlGetAppPriMappingEntry
            ((UINT4) i4LldpV2LocPortIfIndex,
             i4LldpXdot1dcbxAdminApplicationPriorityAESelector,
             (INT4)u4LldpXdot1dcbxAdminApplicationPriorityAEProtocol, APP_PRI_ADMIN);
        if (pAppPriMappingEntry == NULL)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            CLI_SET_ERR (CLI_ERR_NO_APP_PRI_MAPPING_ENTRY);
            DCBX_TRC_ARG1 (DCBX_MGMT_TRC,
                           "In %s : Application to Priority Mapping entry "
                           "does not exist \r\n", __FUNCTION__);
            return SNMP_FAILURE;
        }
    }
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsAppPriXAppTable
 Input       :  The Indices
                LldpV2LocPortIfIndex
                LldpXdot1dcbxAdminApplicationPriorityAESelector
                LldpXdot1dcbxAdminApplicationPriorityAEProtocol
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsAppPriXAppTable (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                           tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  ETSNotifyShutdownStatus
 Input       :  None
 Output      :  None
 Returns     :  None
****************************************************************************/
VOID
ETSNotifyShutdownStatus (VOID)
{
#ifdef ISS_WANTED
    static UINT1        au1ObjectOid[DCBX_SEVEN][SNMP_MAX_OID_LENGTH]
        = { {DCBX_ZERO}, {DCBX_ZERO} };
    INT1                i1count = DCBX_ZERO;
    /* To delete the entries from lldpXdot1dcbxConfigTCSupportedEntry */
    UINT4               au4etsTcSuppTx[] = ETS_TC_SUP_TX_OID;
    /* To delete the entries from lldpXdot1dcbxConfigETSConfigurationEntry */
    UINT4               au4etsConfTx[] = ETS_CONFIG_TX_OID;
    /* To delete the entries from lldpXdot1dcbxConfigETSRecommendationEntry */
    UINT4               au4etsRecoTx[] = ETS_RECO_TX_OID;
    /* To delete the entries from lldpXdot1dcbxAdminETSBasicConfigurationEntry
     * and lldpXdot1dcbxAdminETSConPriorityAssignmentEntry*/
    UINT4               au4etsConfTable[] = ETS_CONFIG_TABLE_OID;
    /* To delete the entries from lldpXdot1dcbxAdminETSRecommendationEntry */
    UINT4               au4etsRecoTable[] = ETS_RECO_TABLE_OID;
    /* To delete the ETS entries from from fsdcbx MIB */
    UINT4               au4fsEtsObject[] = ETS_OBJECT_OID;
    UINT4               au4fsEtsSysCtrl[] = ETS_SYS_CNTRL_OID;

    /* The oid list contains the list of Oids that has been registered
     * for the ETS + the oid of the ETSSystemControl object. */
    SNMPGetOidString (au4etsTcSuppTx, (sizeof (au4etsTcSuppTx) /
                                       sizeof (UINT4)), au1ObjectOid[i1count]);
    SNMPGetOidString (au4etsConfTx, (sizeof (au4etsConfTx) /
                                     sizeof (UINT4)), au1ObjectOid[++i1count]);
    SNMPGetOidString (au4etsRecoTx, (sizeof (au4etsRecoTx) / sizeof (UINT4)),
                      au1ObjectOid[++i1count]);
    SNMPGetOidString (au4etsConfTable, (sizeof (au4etsConfTable) /
                                        sizeof (UINT4)),
                      au1ObjectOid[++i1count]);
    SNMPGetOidString (au4etsRecoTable,
                      (sizeof (au4etsRecoTable) / sizeof (UINT4)),
                      au1ObjectOid[++i1count]);
    SNMPGetOidString (au4fsEtsObject,
                      (sizeof (au4fsEtsObject) / sizeof (UINT4)),
                      au1ObjectOid[++i1count]);
    SNMPGetOidString (au4fsEtsSysCtrl,
                      (sizeof (au4fsEtsSysCtrl) / sizeof (UINT4)),
                      au1ObjectOid[++i1count]);

    /* Send a notification to MSR to process the ETS shutdown, with
     * ETS oids and its system control object */
    MSR_NOTIFY_PROTOCOL_SHUT (au1ObjectOid, (UINT2) (++i1count),
                              MSR_INVALID_CNTXT);
#endif
    return;
}

/****************************************************************************
 Function    :  PFCNotifyShutdownStatus
 Input       :  None
 Output      :  None
 Returns     :  None
****************************************************************************/
VOID
PFCNotifyShutdownStatus (VOID)
{
#ifdef ISS_WANTED
    static UINT1        au1ObjectOid[DCBX_FOUR][SNMP_MAX_OID_LENGTH]
        = { {DCBX_ZERO}, {DCBX_ZERO} };
    INT1                i1count = DCBX_ZERO;

    /* To delete the entries from lldpXdot1dcbxConfigPFCEntry */
    UINT4               au4pfcConfigTx[] = PFC_CONFIG_TX_OID;
    /* To delete the entries from lldpXdot1dcbxAdminPFCEnableEntry
     * and lldpXdot1dcbxAdminPFCBasicEntry*/
    UINT4               au4pfcConfTable[] = PFC_CONFIG_TABLE_OID;
    /* To delete the PFC entries from fsdcbx MIB */
    UINT4               au4fsPfcObject[] = PFC_PROP_OBJECT_ID;
    UINT4               au4fsPfcSysCtrl[] = PFC_SYS_CNTRL_OID;

    /* The oid list contains the list of Oids that has been registered
     *  for the PFC + the oid of the PFCSystemControl object. */
    SNMPGetOidString (au4pfcConfigTx, (sizeof (au4pfcConfigTx) /
                                       sizeof (UINT4)), au1ObjectOid[i1count]);
    SNMPGetOidString (au4pfcConfTable, (sizeof (au4pfcConfTable) /
                                        sizeof (UINT4)),
                      au1ObjectOid[++i1count]);
    SNMPGetOidString (au4fsPfcObject, (sizeof (au4fsPfcObject) /
                                       sizeof (UINT4)),
                      au1ObjectOid[++i1count]);
    SNMPGetOidString (au4fsPfcSysCtrl, (sizeof (au4fsPfcSysCtrl) /
                                        sizeof (UINT4)),
                      au1ObjectOid[++i1count]);

    /* Send a notification to MSR to process the PFC shutdown, with
     * PFC oids and its system control object */
    MSR_NOTIFY_PROTOCOL_SHUT (au1ObjectOid, (UINT2) (++i1count),
                              MSR_INVALID_CNTXT);
#endif
    return;
}

/****************************************************************************
 Function    :  AppPriNotifyShutdownStatus
 Input       :  None
 Output      :  None
 Returns     :  None
****************************************************************************/
VOID
AppPriNotifyShutdownStatus (VOID)
{
#ifdef ISS_WANTED
    static UINT1        au1ObjectOid[DCBX_FOUR][SNMP_MAX_OID_LENGTH]
        = { {DCBX_ZERO}, {DCBX_ZERO} };
    INT1                i1count = DCBX_ZERO;

    /* To delete the entries from lldpXdot1dcbxConfigApplicationPriorityEntry */
    UINT4               au4AppPriConfigTx[] = APP_PRI_CONFIG_TX_OID;
    /* To delete the entries from lldpXdot1dcbxAdminApplicationPriorityAppEntry
     * and lldpXdot1dcbxAdminApplicationPriorityBasicEntry*/
    UINT4               au4AppPriConfTable[] = APP_PRI_CONFIG_TABLE_OID;
    /* To delete the Application Priority entries from fsdcbx MIB */
    UINT4               au4fsAppPriObject[] = APP_PRI_PROP_OBJECT_OID;
    UINT4               au4fsAppPriSysCtrl[] = APP_PRI_SYS_CNTRL_OID;

    /* The oid list contains the list of Oids that has been registered
     * for the Application Prioriy + the oid of the AppPriSystemControl object. */
    SNMPGetOidString (au4AppPriConfigTx, (sizeof (au4AppPriConfigTx) /
                                          sizeof (UINT4)),
                      au1ObjectOid[i1count]);
    SNMPGetOidString (au4AppPriConfTable,
                      (sizeof (au4AppPriConfTable) / sizeof (UINT4)),
                      au1ObjectOid[++i1count]);
    SNMPGetOidString (au4fsAppPriObject,
                      (sizeof (au4fsAppPriObject) / sizeof (UINT4)),
                      au1ObjectOid[++i1count]);
    SNMPGetOidString (au4fsAppPriSysCtrl,
                      (sizeof (au4fsAppPriSysCtrl) / sizeof (UINT4)),
                      au1ObjectOid[++i1count]);

    /* Send a notification to MSR to process the PFC shutdown, with
     * PFC oids and its system control object */
    MSR_NOTIFY_PROTOCOL_SHUT (au1ObjectOid, (UINT2) (++i1count),
                              MSR_INVALID_CNTXT);
#endif
    return;
}

/* LOW LEVEL Routines for Table : FslldpXdot1dcbxLocApplicationPriorityBasicTable */

/**************************************************************************************
 Function    :  nmhValidateIndexInstanceFslldpXdot1dcbxLocApplicationPriorityBasicTable
 Input       :  The Indices
                LldpV2LocPortIfIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
***************************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFslldpXdot1dcbxLocApplicationPriorityBasicTable (INT4
                                                                         i4LldpV2LocPortIfIndex)
{
    tAppPriPortEntry   *pAppPortEntry = NULL;

    APP_PRI_SHUTDOWN_CHECK;

    pAppPortEntry = AppPriUtlGetPortEntry ((UINT4) i4LldpV2LocPortIfIndex);
    if (pAppPortEntry == NULL)
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : AppPriUtlGetPortEntry() "
                       "Returns NULL. \r\n", __FUNCTION__);
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFslldpXdot1dcbxLocApplicationPriorityBasicTable
 Input       :  The Indices
                LldpV2LocPortIfIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFslldpXdot1dcbxLocApplicationPriorityBasicTable (INT4
                                                                 *pi4LldpV2LocPortIfIndex)
{
    tAppPriPortEntry   *pAppPortEntry = NULL;

    APP_PRI_SHUTDOWN_CHECK;

    pAppPortEntry = (tAppPriPortEntry *)
        RBTreeGetFirst (gAppPriGlobalInfo.pRbAppPriPortTbl);
    if (pAppPortEntry == NULL)
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : AppPriUtlGetPortEntry() "
                       "Returns NULL. \r\n", __FUNCTION__);
        return SNMP_FAILURE;
    }
    *pi4LldpV2LocPortIfIndex = (INT4) pAppPortEntry->u4IfIndex;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFslldpXdot1dcbxLocApplicationPriorityBasicTable
 Input       :  The Indices
                LldpV2LocPortIfIndex
                nextLldpV2LocPortIfIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFslldpXdot1dcbxLocApplicationPriorityBasicTable (INT4
                                                                i4LldpV2LocPortIfIndex,
                                                                INT4
                                                                *pi4NextLldpV2LocPortIfIndex)
{
    tAppPriPortEntry   *pAppPortEntry = NULL;
    tAppPriPortEntry    AppPortEntry;

    APP_PRI_SHUTDOWN_CHECK;

    MEMSET (&AppPortEntry, DCBX_ZERO, sizeof (tAppPriPortEntry));
    AppPortEntry.u4IfIndex = (UINT4) i4LldpV2LocPortIfIndex;
    pAppPortEntry = (tAppPriPortEntry *)
        RBTreeGetNext (gAppPriGlobalInfo.pRbAppPriPortTbl,
                       &AppPortEntry, AppPriUtlPortTblCmpFn);
    if (pAppPortEntry == NULL)
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : RBtreeGetNExt() "
                       "Returns NULL. \r\n", __FUNCTION__);
        return SNMP_FAILURE;
    }

    *pi4NextLldpV2LocPortIfIndex = (INT4) pAppPortEntry->u4IfIndex;
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFslldpXdot1dcbxLocApplicationPriorityWilling
 Input       :  The Indices
                LldpV2LocPortIfIndex

                The Object 
                retValFslldpXdot1dcbxLocApplicationPriorityWilling
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFslldpXdot1dcbxLocApplicationPriorityWilling (INT4 i4LldpV2LocPortIfIndex,
                                                    INT4
                                                    *pi4RetValFslldpXdot1dcbxLocApplicationPriorityWilling)
{
    tAppPriPortEntry   *pAppPortEntry = NULL;

    APP_PRI_SHUTDOWN_CHECK;
    pAppPortEntry = AppPriUtlGetPortEntry ((UINT4) i4LldpV2LocPortIfIndex);

    if (pAppPortEntry == NULL)
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : AppPriUtlGetPortEntry () "
                       "Returns NULL. \r\n", __FUNCTION__);
        return SNMP_FAILURE;
    }
    *pi4RetValFslldpXdot1dcbxLocApplicationPriorityWilling =
        pAppPortEntry->AppPriLocPortInfo.u1AppPriLocWilling;

    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FslldpXdot1dcbxAdminApplicationPriorityBasicTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFslldpXdot1dcbxAdminApplicationPriorityBasicTable
 Input       :  The Indices
                LldpV2LocPortIfIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFslldpXdot1dcbxAdminApplicationPriorityBasicTable (INT4
                                                                           i4LldpV2LocPortIfIndex)
{
    tAppPriPortEntry   *pAppPortEntry = NULL;

    APP_PRI_SHUTDOWN_CHECK;

    pAppPortEntry = AppPriUtlGetPortEntry ((UINT4) i4LldpV2LocPortIfIndex);
    if (pAppPortEntry == NULL)
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : AppPriUtlGetPortEntry() "
                       "Returns NULL. \r\n", __FUNCTION__);
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsAdminAppPriBasicTable
 Input       :  The Indices
                LldpV2LocPortIfIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFslldpXdot1dcbxAdminApplicationPriorityBasicTable (INT4
                                                                   *pi4LldpV2LocPortIfIndex)
{
    tAppPriPortEntry   *pAppPortEntry = NULL;

    APP_PRI_SHUTDOWN_CHECK;

    pAppPortEntry = (tAppPriPortEntry *)
        RBTreeGetFirst (gAppPriGlobalInfo.pRbAppPriPortTbl);
    if (pAppPortEntry == NULL)
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : RBtreeGetFirst() "
                       "Returns NULL. \r\n", __FUNCTION__);
        return SNMP_FAILURE;
    }

    *pi4LldpV2LocPortIfIndex = (INT4) pAppPortEntry->u4IfIndex;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFslldpXdot1dcbxAdminApplicationPriorityBasicTable
 Input       :  The Indices
                LldpV2LocPortIfIndex
                nextLldpV2LocPortIfIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFslldpXdot1dcbxAdminApplicationPriorityBasicTable (INT4
                                                                  i4LldpV2LocPortIfIndex,
                                                                  INT4
                                                                  *pi4NextLldpV2LocPortIfIndex)
{
    tAppPriPortEntry   *pAppPortEntry = NULL;
    tAppPriPortEntry    AppPortEntry;

    APP_PRI_SHUTDOWN_CHECK;

    MEMSET (&AppPortEntry, DCBX_ZERO, sizeof (tAppPriPortEntry));
    AppPortEntry.u4IfIndex = (UINT4) i4LldpV2LocPortIfIndex;
    pAppPortEntry = (tAppPriPortEntry *)
        RBTreeGetNext (gAppPriGlobalInfo.pRbAppPriPortTbl,
                       &AppPortEntry, AppPriUtlPortTblCmpFn);
    if (pAppPortEntry == NULL)
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : RBtreeGetFirst() "
                       "Returns NULL. \r\n", __FUNCTION__);
        return SNMP_FAILURE;
    }

    *pi4NextLldpV2LocPortIfIndex = (INT4) pAppPortEntry->u4IfIndex;
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFslldpXdot1dcbxAdminApplicationPriorityWilling
 Input       :  The Indices
                LldpV2LocPortIfIndex

                The Object 
                retValFslldpXdot1dcbxAdminApplicationPriorityWilling
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFslldpXdot1dcbxAdminApplicationPriorityWilling (INT4
                                                      i4LldpV2LocPortIfIndex,
                                                      INT4
                                                      *pi4RetValFslldpXdot1dcbxAdminApplicationPriorityWilling)
{
    tAppPriPortEntry   *pAppPortEntry = NULL;

    APP_PRI_SHUTDOWN_CHECK;
    pAppPortEntry = AppPriUtlGetPortEntry ((UINT4) i4LldpV2LocPortIfIndex);

    if (pAppPortEntry == NULL)
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : AppPriUtlGetPortEntry() "
                       "Returns FAILURE. \r\n", __FUNCTION__);
        return SNMP_FAILURE;
    }
    *pi4RetValFslldpXdot1dcbxAdminApplicationPriorityWilling =
        pAppPortEntry->AppPriAdmPortInfo.u1AppPriAdmWilling;
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFslldpXdot1dcbxAdminApplicationPriorityWilling
 Input       :  The Indices
                LldpV2LocPortIfIndex

                The Object 
                setValFslldpXdot1dcbxAdminApplicationPriorityWilling
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFslldpXdot1dcbxAdminApplicationPriorityWilling (INT4
                                                      i4LldpV2LocPortIfIndex,
                                                      INT4
                                                      i4SetValFslldpXdot1dcbxAdminApplicationPriorityWilling)
{
    tAppPriPortEntry   *pAppPortEntry = NULL;

    DCBX_TRC_ARG3 (DCBX_MGMT_TRC, "%s, Args: "
            "i4LldpV2LocPortIfIndex: %d "
            "i4SetValFslldpXdot1dcbxAdminApplicationPriorityWilling: %d\r\n", 
            __func__, i4LldpV2LocPortIfIndex,
            i4SetValFslldpXdot1dcbxAdminApplicationPriorityWilling);

    pAppPortEntry = AppPriUtlGetPortEntry ((UINT4) i4LldpV2LocPortIfIndex);

    if (pAppPortEntry == NULL)
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : AppPriUtlGetPortEntry() "
                       "Returns FAILURE. \r\n", __FUNCTION__);
        return SNMP_FAILURE;
    }
    if (pAppPortEntry->AppPriAdmPortInfo.u1AppPriAdmWilling !=
        (UINT1) i4SetValFslldpXdot1dcbxAdminApplicationPriorityWilling)
    {
        pAppPortEntry->AppPriAdmPortInfo.u1AppPriAdmWilling =
            (UINT1) i4SetValFslldpXdot1dcbxAdminApplicationPriorityWilling;
        pAppPortEntry->AppPriLocPortInfo.u1AppPriLocWilling =
            (UINT1) i4SetValFslldpXdot1dcbxAdminApplicationPriorityWilling;
        AppPriUtlWillingChange (pAppPortEntry, OSIX_TRUE);
    }
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FslldpXdot1dcbxAdminApplicationPriorityWilling
 Input       :  The Indices
                LldpV2LocPortIfIndex

                The Object 
                testValFslldpXdot1dcbxAdminApplicationPriorityWilling
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FslldpXdot1dcbxAdminApplicationPriorityWilling (UINT4 *pu4ErrorCode,
                                                         INT4
                                                         i4LldpV2LocPortIfIndex,
                                                         INT4
                                                         i4TestValFslldpXdot1dcbxAdminApplicationPriorityWilling)
{
    tAppPriPortEntry   *pAppPortEntry = NULL;

    APP_PRI_SHUTDOWN_CHECK_TEST;

    pAppPortEntry = AppPriUtlGetPortEntry ((UINT4) i4LldpV2LocPortIfIndex);
    if (pAppPortEntry == NULL)
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : PAppPriUtlGetPortEntry () "
                       " Returns FAILURE. \r\n", __FUNCTION__);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_ERR_APP_PRI_NO_PORT_ENTRY);
        return SNMP_FAILURE;
    }

    if ((i4TestValFslldpXdot1dcbxAdminApplicationPriorityWilling !=
         APP_PRI_ENABLED)
        && (i4TestValFslldpXdot1dcbxAdminApplicationPriorityWilling !=
            APP_PRI_DISABLED))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_ERR_APP_PRI_INVALID_WILLING_STATUS);
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FslldpXdot1dcbxAdminApplicationPriorityBasicTable
 Input       :  The Indices
                LldpV2LocPortIfIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FslldpXdot1dcbxAdminApplicationPriorityBasicTable (UINT4 *pu4ErrorCode,
                                                           tSnmpIndexList *
                                                           pSnmpIndexList,
                                                           tSNMP_VAR_BIND *
                                                           pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FslldpXdot1dcbxRemApplicationPriorityBasicTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFslldpXdot1dcbxRemApplicationPriorityBasicTable
 Input       :  The Indices
                LldpV2RemTimeMark
                LldpV2RemLocalIfIndex
                u4LldpV2RemLocalDestMACAddress
                LldpV2RemIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFslldpXdot1dcbxRemApplicationPriorityBasicTable (UINT4
                                                                         u4LldpV2RemTimeMark,
                                                                         INT4
                                                                         i4LldpV2RemLocalIfIndex,
                                                                         UINT4
                                                                         u4LldpV2RemLocalDestMACAddress,
                                                                         INT4
                                                                         i4LldpV2RemIndex)
{
    tAppPriPortEntry   *pAppPortEntry = NULL;
    tAppPriRemPortInfo *pAppRemPortInfo = NULL;

    APP_PRI_SHUTDOWN_CHECK;

    pAppPortEntry = AppPriUtlGetPortEntry ((UINT4) i4LldpV2RemLocalIfIndex);
    if (pAppPortEntry == NULL)
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : AppPriUtlGetPortEntry() "
                       "Returns NULL. \r\n", __FUNCTION__);
        return SNMP_FAILURE;
    }

    pAppRemPortInfo = &(pAppPortEntry->AppPriRemPortInfo);
    if (pAppPortEntry->AppPriRemPortInfo.i4AppPriRemIndex == DCBX_ZERO)
    {
        DCBX_TRC (DCBX_MGMT_TRC, NIL_INFO);
        return SNMP_FAILURE;
    }

    if ((pAppPortEntry->AppPriRemPortInfo.i4AppPriRemIndex < DCBX_ZERO) ||
        (pAppPortEntry->AppPriRemPortInfo.i4AppPriRemIndex >
         DCBX_REM_MAX_INDEX))
    {
        DCBX_TRC (DCBX_MGMT_TRC, INDEX_NOT_IN_RANGE);
        return SNMP_FAILURE;
    }
    if (pAppPortEntry->AppPriRemPortInfo.u4AppPriDestRemMacIndex == DCBX_ZERO)
    {
        DCBX_TRC (DCBX_MGMT_TRC, WRONG_VAL);
        return SNMP_FAILURE;
    }

    if ((pAppRemPortInfo->u4AppPriRemTimeMark != u4LldpV2RemTimeMark) ||
        (pAppRemPortInfo->i4AppPriRemIndex != i4LldpV2RemIndex) ||
        (pAppRemPortInfo->u4AppPriDestRemMacIndex != u4LldpV2RemLocalDestMACAddress))
    {
        DCBX_TRC (DCBX_MGMT_TRC, MISMATCH);
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsRemAppPriBasicTable
 Input       :  The Indices
                LldpV2RemTimeMark
                LldpV2RemLocalIfIndex
                u4LldpV2RemLocalDestMACAddress
                LldpV2RemIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFslldpXdot1dcbxRemApplicationPriorityBasicTable (UINT4
                                                                 *pu4LldpV2RemTimeMark,
                                                                 INT4
                                                                 *pi4LldpV2RemLocalIfIndex,
                                                                 UINT4 *
                                                                 pu4LldpV2RemLocalDestMACAddress,
                                                                 INT4
                                                                 *pi4LldpV2RemIndex)
{
    APP_PRI_SHUTDOWN_CHECK;
    return (nmhGetNextIndexFslldpXdot1dcbxRemApplicationPriorityBasicTable
            (DCBX_ZERO, pu4LldpV2RemTimeMark, DCBX_ZERO,
             pi4LldpV2RemLocalIfIndex, DCBX_ZERO,
             pu4LldpV2RemLocalDestMACAddress, DCBX_ZERO, pi4LldpV2RemIndex));
}

/****************************************************************************
 Function    :  nmhGetNextIndexFslldpXdot1dcbxRemApplicationPriorityBasicTable
 Input       :  The Indices
                LldpV2RemTimeMark
                nextLldpV2RemTimeMark
                LldpV2RemLocalIfIndex
                nextLldpV2RemLocalIfIndex
                u4LldpV2RemLocalDestMACAddress
                nextLldpV2RemLocalDestMACAddress
                LldpV2RemIndex
                nextLldpV2RemIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFslldpXdot1dcbxRemApplicationPriorityBasicTable (UINT4
                                                                u4LldpV2RemTimeMark,
                                                                UINT4
                                                                *pu4NextLldpV2RemTimeMark,
                                                                INT4
                                                                i4LldpV2RemLocalIfIndex,
                                                                INT4
                                                                *pi4NextLldpV2RemLocalIfIndex,
                                                                UINT4
                                                                u4LldpV2RemLocalDestMACAddress,
                                                                UINT4 *
                                                                pu4NextLldpV2RemLocalDestMACAddress,
                                                                INT4
                                                                i4LldpV2RemIndex,
                                                                INT4
                                                                *pi4NextLldpV2RemIndex)
{
    tAppPriPortEntry   *pAppPortEntry = NULL;
    tAppPriPortEntry    AppPortEntry;
    INT4                i4IfIndex = i4LldpV2RemLocalIfIndex;
    INT4                i4RemIndex = i4LldpV2RemIndex;

    UNUSED_PARAM (u4LldpV2RemTimeMark);
    UNUSED_PARAM (u4LldpV2RemLocalDestMACAddress);
    APP_PRI_SHUTDOWN_CHECK;

    pAppPortEntry = AppPriUtlGetPortEntry ((UINT4) i4IfIndex);

    if ((pAppPortEntry == NULL) ||
        (i4RemIndex >= pAppPortEntry->AppPriRemPortInfo.i4AppPriRemIndex))

    {
        MEMSET (&AppPortEntry, DCBX_ZERO, sizeof (tAppPriPortEntry));
        AppPortEntry.u4IfIndex = (UINT4) i4IfIndex;
        do
        {
            pAppPortEntry = (tAppPriPortEntry *)
                RBTreeGetNext (gAppPriGlobalInfo.pRbAppPriPortTbl,
                               &AppPortEntry, AppPriUtlPortTblCmpFn);
            if ((pAppPortEntry != NULL) &&
                (pAppPortEntry->AppPriRemPortInfo.i4AppPriRemIndex !=
                 DCBX_ZERO))
            {
                *pi4NextLldpV2RemLocalIfIndex = (INT4) pAppPortEntry->u4IfIndex;
                *pu4NextLldpV2RemTimeMark =
                    pAppPortEntry->AppPriRemPortInfo.u4AppPriRemTimeMark;

                *pu4NextLldpV2RemLocalDestMACAddress =
                    pAppPortEntry->AppPriRemPortInfo.u4AppPriDestRemMacIndex;

                *pi4NextLldpV2RemIndex =
                    pAppPortEntry->AppPriRemPortInfo.i4AppPriRemIndex;
                return SNMP_SUCCESS;
            }
            if (pAppPortEntry != NULL)
            {
                MEMSET (&AppPortEntry, DCBX_ZERO, sizeof (tAppPriPortEntry));
                AppPortEntry.u4IfIndex = pAppPortEntry->u4IfIndex;
            }
        }
        while (pAppPortEntry != NULL);
    }
    else
    {

        *pi4NextLldpV2RemLocalIfIndex = (INT4) pAppPortEntry->u4IfIndex;
        *pu4NextLldpV2RemTimeMark =
            pAppPortEntry->AppPriRemPortInfo.u4AppPriRemTimeMark;
                
        *pu4NextLldpV2RemLocalDestMACAddress =
                pAppPortEntry->AppPriRemPortInfo.u4AppPriDestRemMacIndex;
        *pi4NextLldpV2RemIndex =
            pAppPortEntry->AppPriRemPortInfo.i4AppPriRemIndex;
        return SNMP_SUCCESS;
    }
    DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : Rbtree GetNext Node () "
                   "Returns NULL. \r\n", __FUNCTION__);
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFslldpXdot1dcbxRemApplicationPriorityWilling
 Input       :  The Indices
                LldpV2RemTimeMark
                LldpV2RemLocalIfIndex
                u4LldpV2RemLocalDestMACAddress
                LldpV2RemIndex

                The Object 
                retValFslldpXdot1dcbxRemApplicationPriorityWilling
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFslldpXdot1dcbxRemApplicationPriorityWilling (UINT4 u4LldpV2RemTimeMark,
                                                    INT4
                                                    i4LldpV2RemLocalIfIndex,
                                                    UINT4
                                                    u4LldpV2RemLocalDestMACAddress,
                                                    INT4 i4LldpV2RemIndex,
                                                    INT4
                                                    *pi4RetValFslldpXdot1dcbxRemApplicationPriorityWilling)
{
    tAppPriPortEntry   *pAppPortEntry = NULL;

    UNUSED_PARAM (u4LldpV2RemTimeMark);
    UNUSED_PARAM (i4LldpV2RemIndex);
    UNUSED_PARAM (u4LldpV2RemLocalDestMACAddress);

    pAppPortEntry = AppPriUtlGetPortEntry ((UINT4) i4LldpV2RemLocalIfIndex);
    if (pAppPortEntry == NULL)
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : AppPriUtlGetPortEntry() "
                       "Returns NULL. \r\n", __FUNCTION__);
        return SNMP_FAILURE;
    }

    if (pAppPortEntry->AppPriRemPortInfo.i4AppPriRemIndex == DCBX_ZERO)
    {
        DCBX_TRC (DCBX_MGMT_TRC, NIL_INFO);
        return SNMP_FAILURE;
    }

    *pi4RetValFslldpXdot1dcbxRemApplicationPriorityWilling =
        pAppPortEntry->AppPriRemPortInfo.u1AppPriRemWilling;

    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FslldpXdot1dcbxConfigTCSupportedTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFslldpXdot1dcbxConfigTCSupportedTable
 Input       :  The Indices
                LldpV2PortConfigIfIndex
                LldpV2PortConfigDestAddressIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFslldpXdot1dcbxConfigTCSupportedTable (INT4
                                                               i4LldpV2PortConfigIfIndex,
                                                               UINT4
                                                               u4LldpV2PortConfigDestAddressIndex)
{
    tETSPortEntry      *pETSPortEntry = NULL;

    ETS_SHUTDOWN_CHECK;

    pETSPortEntry = ETSUtlGetPortEntry ((UINT4) i4LldpV2PortConfigIfIndex);
    if (pETSPortEntry == NULL)
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : ETSUtlGetPortEntry() "
                       "Returns NULL. \r\n", __FUNCTION__);
        return SNMP_FAILURE;
    }

    /* Destination Address Index will always be 1.
     * LLDP can not assign more than 1 to this Index.
     */
    if (u4LldpV2PortConfigDestAddressIndex != DCBX_DEST_ADDR_INDEX)
    {
        DCBX_TRC (DCBX_MGMT_TRC, WRONG_VAL);
        return (SNMP_FAILURE);
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsConfigTCSupportedTable
 Input       :  The Indices
                LldpV2PortConfigIfIndex
                LldpV2PortConfigDestAddressIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFslldpXdot1dcbxConfigTCSupportedTable (INT4
                                                       *pi4LldpV2PortConfigIfIndex,
                                                       UINT4
                                                       *pu4LldpV2PortConfigDestAddressIndex)
{
    tETSPortEntry      *pETSPortEntry = NULL;

    ETS_SHUTDOWN_CHECK;

    pETSPortEntry = (tETSPortEntry *)
        RBTreeGetFirst (gETSGlobalInfo.pRbETSPortTbl);
    if (pETSPortEntry == NULL)
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : RbTreeGetFirst Node () "
                       "Returns NULL. \r\n", __FUNCTION__);
        return SNMP_FAILURE;
    }

    *pi4LldpV2PortConfigIfIndex = (INT4) pETSPortEntry->u4IfIndex;
    *pu4LldpV2PortConfigDestAddressIndex = DCBX_DEST_ADDR_INDEX;
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetNextIndexFslldpXdot1dcbxConfigTCSupportedTable
 Input       :  The Indices
                LldpV2PortConfigIfIndex
                nextLldpV2PortConfigIfIndex
                LldpV2PortConfigDestAddressIndex
                nextLldpV2PortConfigDestAddressIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFslldpXdot1dcbxConfigTCSupportedTable (INT4
                                                      i4LldpV2PortConfigIfIndex,
                                                      INT4
                                                      *pi4NextLldpV2PortConfigIfIndex,
                                                      UINT4
                                                      u4LldpV2PortConfigDestAddressIndex,
                                                      UINT4
                                                      *pu4NextLldpV2PortConfigDestAddressIndex)
{
    tETSPortEntry      *pETSPortEntry = NULL;
    tETSPortEntry       ETSPortEntry;
    INT4                i4IfIndex = i4LldpV2PortConfigIfIndex;
    INT4                i4AddrIndex = (INT4)u4LldpV2PortConfigDestAddressIndex;

    pETSPortEntry = ETSUtlGetPortEntry ((UINT4) i4IfIndex);

    if ((pETSPortEntry == NULL) || (DCBX_ZERO < (UINT4) i4AddrIndex))
    {
        MEMSET (&ETSPortEntry, DCBX_ZERO, sizeof (tETSPortEntry));
        ETSPortEntry.u4IfIndex = (UINT4) i4LldpV2PortConfigIfIndex;
        pETSPortEntry = (tETSPortEntry *)
            RBTreeGetNext (gETSGlobalInfo.pRbETSPortTbl,
                           &ETSPortEntry, DcbxUtlPortTblCmpFn);
        if (pETSPortEntry == NULL)
        {
            DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : Rbtree GetNext Node () "
                           "Returns NULL. \r\n", __FUNCTION__);
            return SNMP_FAILURE;
        }
    }

    *pi4NextLldpV2PortConfigIfIndex = (INT4) pETSPortEntry->u4IfIndex;
    *pu4NextLldpV2PortConfigDestAddressIndex = DCBX_DEST_ADDR_INDEX;
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFslldpXdot1dcbxConfigTCSupportedTxEnable
 Input       :  The Indices
                LldpV2PortConfigIfIndex
                LldpV2PortConfigDestAddressIndex

                The Object 
                retValFslldpXdot1dcbxConfigTCSupportedTxEnable
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFslldpXdot1dcbxConfigTCSupportedTxEnable (INT4 i4LldpV2PortConfigIfIndex,
                                                UINT4
                                                u4LldpV2PortConfigDestAddressIndex,
                                                INT4
                                                *pi4RetValFslldpXdot1dcbxConfigTCSupportedTxEnable)
{
    tETSPortEntry      *pETSPortEntry = NULL;
    UNUSED_PARAM (u4LldpV2PortConfigDestAddressIndex);

    pETSPortEntry = ETSUtlGetPortEntry ((UINT4) i4LldpV2PortConfigIfIndex);
    if (pETSPortEntry == NULL)
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : ETSUtlGetPortEntry () "
                       " Returns FAILURE. \r\n", __FUNCTION__);
        return SNMP_FAILURE;
    }

    *pi4RetValFslldpXdot1dcbxConfigTCSupportedTxEnable =
        pETSPortEntry->u1TcSupportTxStatus;
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFslldpXdot1dcbxConfigTCSupportedTxEnable
 Input       :  The Indices
                LldpV2PortConfigIfIndex
                LldpV2PortConfigDestAddressIndex

                The Object 
                setValFslldpXdot1dcbxConfigTCSupportedTxEnable
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFslldpXdot1dcbxConfigTCSupportedTxEnable (INT4 i4LldpV2PortConfigIfIndex,
                                                UINT4
                                                u4LldpV2PortConfigDestAddressIndex,
                                                INT4
                                                i4SetValFslldpXdot1dcbxConfigTCSupportedTxEnable)
{
    tETSPortEntry      *pETSPortEntry = NULL;

    DCBX_TRC_ARG4 (DCBX_MGMT_TRC, "%s, Args: "
            "i4LldpV2PortConfigIfIndex: %d "
            "u4LldpV2PortConfigDestAddressIndex: %d "
            "i4SetValFslldpXdot1dcbxConfigTCSupportedTxEnable: %d\r\n", 
            __func__, i4LldpV2PortConfigIfIndex,
            u4LldpV2PortConfigDestAddressIndex,
            i4SetValFslldpXdot1dcbxConfigTCSupportedTxEnable);

    pETSPortEntry = ETSUtlGetPortEntry ((UINT4) i4LldpV2PortConfigIfIndex);
    if (pETSPortEntry == NULL)
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : ETSUtlGetPortEntry () "
                       " Returns FAILURE. \r\n", __FUNCTION__);
        return SNMP_FAILURE;
    }
    if (pETSPortEntry->u1TcSupportTxStatus !=
        (UINT1) i4SetValFslldpXdot1dcbxConfigTCSupportedTxEnable)
    {

        ETSUtlTlvStatusChange (pETSPortEntry,
                               (UINT1)
                               i4SetValFslldpXdot1dcbxConfigTCSupportedTxEnable,
                               ETS_TC_SUPP_TLV_SUB_TYPE);
        /* Updation of TLV Tx enable data strucure will
         * be done inside the TLV status change function */
    }
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FslldpXdot1dcbxConfigTCSupportedTxEnable
 Input       :  The Indices
                LldpV2PortConfigIfIndex
                LldpV2PortConfigDestAddressIndex

                The Object 
                testValFslldpXdot1dcbxConfigTCSupportedTxEnable
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FslldpXdot1dcbxConfigTCSupportedTxEnable (UINT4 *pu4ErrorCode,
                                                   INT4
                                                   i4LldpV2PortConfigIfIndex,
                                                   UINT4
                                                   u4LldpV2PortConfigDestAddressIndex,
                                                   INT4
                                                   i4TestValFslldpXdot1dcbxConfigTCSupportedTxEnable)
{
    tETSPortEntry      *pETSPortEntry = NULL;

    ETS_SHUTDOWN_CHECK_TEST;

    pETSPortEntry = ETSUtlGetPortEntry ((UINT4) i4LldpV2PortConfigIfIndex);
    if (pETSPortEntry == NULL)
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : ETSUtlGetPortEntry () "
                       " Returns FAILURE. \r\n", __FUNCTION__);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_ERR_ETS_NO_PORT_ENTRY);
        return SNMP_FAILURE;
    }

    if ((i4TestValFslldpXdot1dcbxConfigTCSupportedTxEnable != ETS_ENABLED)
        && (i4TestValFslldpXdot1dcbxConfigTCSupportedTxEnable != ETS_DISABLED))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_ERR_ETS_INVALID_TX_STATUS);
        return SNMP_FAILURE;
    }

    /* Destination Address Index will always be 1.
     * LLDP can not assign more than 1 to this Index.
     */
    if (u4LldpV2PortConfigDestAddressIndex != DCBX_DEST_ADDR_INDEX)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }

    return SNMP_SUCCESS;

}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FslldpXdot1dcbxConfigTCSupportedTable
 Input       :  The Indices
                LldpV2PortConfigIfIndex
                LldpV2PortConfigDestAddressIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FslldpXdot1dcbxConfigTCSupportedTable (UINT4 *pu4ErrorCode,
                                               tSnmpIndexList * pSnmpIndexList,
                                               tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FslldpXdot1dcbxLocTCSupportedTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFslldpXdot1dcbxLocTCSupportedTable
 Input       :  The Indices
                LldpV2LocPortIfIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFslldpXdot1dcbxLocTCSupportedTable (INT4
                                                            i4LldpV2LocPortIfIndex)
{
    tETSPortEntry      *pETSPortEntry = NULL;

    ETS_SHUTDOWN_CHECK;

    pETSPortEntry = ETSUtlGetPortEntry ((UINT4) i4LldpV2LocPortIfIndex);
    if (pETSPortEntry == NULL)
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : ETSUtlGetPortEntry() "
                       "Returns NULL. \r\n", __FUNCTION__);
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsLocTCSupportedTable
 Input       :  The Indices
                LldpV2LocPortIfIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFslldpXdot1dcbxLocTCSupportedTable (INT4
                                                    *pi4LldpV2LocPortIfIndex)
{
    tETSPortEntry      *pETSPortEntry = NULL;

    ETS_SHUTDOWN_CHECK;

    pETSPortEntry = (tETSPortEntry *)
        RBTreeGetFirst (gETSGlobalInfo.pRbETSPortTbl);
    if (pETSPortEntry == NULL)
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : RbTreeGetFirst Node () "
                       "Returns NULL. \r\n", __FUNCTION__);
        return SNMP_FAILURE;
    }

    *pi4LldpV2LocPortIfIndex = (INT4) pETSPortEntry->u4IfIndex;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFslldpXdot1dcbxLocTCSupportedTable
 Input       :  The Indices
                LldpV2LocPortIfIndex
                nextLldpV2LocPortIfIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFslldpXdot1dcbxLocTCSupportedTable (INT4 i4LldpV2LocPortIfIndex,
                                                   INT4
                                                   *pi4NextLldpV2LocPortIfIndex)
{
    tETSPortEntry      *pETSPortEntry = NULL;
    tETSPortEntry       ETSPortEntry;

    ETS_SHUTDOWN_CHECK;

    MEMSET (&ETSPortEntry, DCBX_ZERO, sizeof (tETSPortEntry));
    ETSPortEntry.u4IfIndex = (UINT4) i4LldpV2LocPortIfIndex;
    pETSPortEntry = (tETSPortEntry *)
        RBTreeGetNext (gETSGlobalInfo.pRbETSPortTbl,
                       &ETSPortEntry, DcbxUtlPortTblCmpFn);
    if (pETSPortEntry == NULL)
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : Rbtree GetNext Node () "
                       "Returns NULL. \r\n", __FUNCTION__);
        return SNMP_FAILURE;
    }

    *pi4NextLldpV2LocPortIfIndex = (INT4) pETSPortEntry->u4IfIndex;
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFslldpXdot1dcbxLocTCSupported
 Input       :  The Indices
                LldpV2LocPortIfIndex

                The Object 
                retValFslldpXdot1dcbxLocTCSupported
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFslldpXdot1dcbxLocTCSupported (INT4 i4LldpV2LocPortIfIndex,
                                     INT4
                                     *pi4RetValFslldpXdot1dcbxLocTCSupported)
{
    tETSPortEntry      *pETSPortEntry = NULL;

    pETSPortEntry = ETSUtlGetPortEntry ((UINT4) i4LldpV2LocPortIfIndex);
    if (pETSPortEntry == NULL)
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : ETSUtlGetPortEntry () "
                       " Returns FAILURE. \r\n", __FUNCTION__);
        return SNMP_FAILURE;
    }

    *pi4RetValFslldpXdot1dcbxLocTCSupported =
        (pETSPortEntry->ETSLocPortInfo).u1ETSLocNumTcSup;
    return SNMP_SUCCESS;

}

/* LOW LEVEL Routines for Table : FslldpXdot1dcbxRemTCSupportedTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFslldpXdot1dcbxRemTCSupportedTable
 Input       :  The Indices
                LldpV2RemTimeMark
                LldpV2RemLocalIfIndex
                u4LldpV2RemLocalDestMACAddress
                LldpV2RemIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFslldpXdot1dcbxRemTCSupportedTable (UINT4
                                                            u4LldpV2RemTimeMark,
                                                            INT4
                                                            i4LldpV2RemLocalIfIndex,
                                                            UINT4
                                                            u4LldpV2RemLocalDestMACAddress,
                                                            INT4
                                                            i4LldpV2RemIndex)
{
    tETSPortEntry      *pETSPortEntry = NULL;
    tETSRemPortInfo    *pETSRemPortInfo = NULL;

    ETS_SHUTDOWN_CHECK;

    pETSPortEntry = ETSUtlGetPortEntry ((UINT4) i4LldpV2RemLocalIfIndex);
    if (pETSPortEntry == NULL)
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : ETSUtlGetPortEntry() "
                       "Returns NULL. \r\n", __FUNCTION__);
        return SNMP_FAILURE;
    }

    pETSRemPortInfo = &(pETSPortEntry->ETSRemPortInfo);
    if (pETSPortEntry->ETSRemPortInfo.i4ETSRemIndex == DCBX_ZERO)
    {
        DCBX_TRC (DCBX_MGMT_TRC, NIL_INFO);
        return SNMP_FAILURE;
    }

    if ((pETSRemPortInfo->i4ETSRemIndex < DCBX_ZERO)
        || (pETSRemPortInfo->i4ETSRemIndex > DCBX_REM_MAX_INDEX))
    {
        DCBX_TRC (DCBX_MGMT_TRC, INDEX_NOT_IN_RANGE);
        return SNMP_FAILURE;
    }

    if (pETSPortEntry->ETSRemPortInfo.u4ETSDestRemMacIndex == DCBX_ZERO)
    {
        DCBX_TRC (DCBX_MGMT_TRC, WRONG_VAL);
        return SNMP_FAILURE;
    }

    if ((pETSRemPortInfo->u4ETSRemTimeMark != u4LldpV2RemTimeMark)
        || (pETSRemPortInfo->i4ETSRemIndex != i4LldpV2RemIndex)
        || (pETSRemPortInfo->u4ETSDestRemMacIndex != u4LldpV2RemLocalDestMACAddress))
    {
        DCBX_TRC (DCBX_MGMT_TRC, MISMATCH);
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFslldpXdot1dcbxRemTCSupportedTable
 Input       :  The Indices
                LldpV2RemTimeMark
                LldpV2RemLocalIfIndex
                u4LldpV2RemLocalDestMACAddress
                LldpV2RemIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFslldpXdot1dcbxRemTCSupportedTable (UINT4 *pu4LldpV2RemTimeMark,
                                                    INT4
                                                    *pi4LldpV2RemLocalIfIndex,
                                                    UINT4 *
                                                    pu4LldpV2RemLocalDestMACAddress,
                                                    INT4 *pi4LldpV2RemIndex)
{
    ETS_SHUTDOWN_CHECK;
    return (nmhGetNextIndexFslldpXdot1dcbxRemTCSupportedTable
            (DCBX_ZERO, pu4LldpV2RemTimeMark,
             DCBX_ZERO, pi4LldpV2RemLocalIfIndex,
             DCBX_ZERO, pu4LldpV2RemLocalDestMACAddress,
             DCBX_ZERO, pi4LldpV2RemIndex));

}

/****************************************************************************
 Function    :  nmhGetNextIndexFslldpXdot1dcbxRemTCSupportedTable
 Input       :  The Indices
                LldpV2RemTimeMark
                nextLldpV2RemTimeMark
                LldpV2RemLocalIfIndex
                nextLldpV2RemLocalIfIndex
                u4LldpV2RemLocalDestMACAddress
                nextLldpV2RemLocalDestMACAddress
                LldpV2RemIndex
                nextLldpV2RemIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFslldpXdot1dcbxRemTCSupportedTable (UINT4 u4LldpV2RemTimeMark,
                                                   UINT4
                                                   *pu4NextLldpV2RemTimeMark,
                                                   INT4 i4LldpV2RemLocalIfIndex,
                                                   INT4
                                                   *pi4NextLldpV2RemLocalIfIndex,
                                                   UINT4
                                                   u4LldpV2RemLocalDestMACAddress,
                                                   UINT4 *
                                                   pu4NextLldpV2RemLocalDestMACAddress,
                                                   INT4 i4LldpV2RemIndex,
                                                   INT4 *pi4NextLldpV2RemIndex)
{
    tETSPortEntry      *pETSPortEntry = NULL;
    tETSPortEntry       ETSPortEntry;
    INT4                i4IfIndex = i4LldpV2RemLocalIfIndex;
    INT4                i4RemIndex = i4LldpV2RemIndex;

    UNUSED_PARAM (u4LldpV2RemTimeMark);
    UNUSED_PARAM (u4LldpV2RemLocalDestMACAddress);
    ETS_SHUTDOWN_CHECK;


    pETSPortEntry = ETSUtlGetPortEntry ((UINT4) i4IfIndex);

    if ((pETSPortEntry == NULL) ||
        (i4RemIndex >= pETSPortEntry->ETSRemPortInfo.i4ETSRemIndex))

    {
        MEMSET (&ETSPortEntry, DCBX_ZERO, sizeof (tETSPortEntry));
        ETSPortEntry.u4IfIndex = (UINT4) i4IfIndex;
        do
        {
            pETSPortEntry = (tETSPortEntry *)
                RBTreeGetNext (gETSGlobalInfo.pRbETSPortTbl,
                               &ETSPortEntry, DcbxUtlPortTblCmpFn);
            if ((pETSPortEntry != NULL) &&
                (pETSPortEntry->ETSRemPortInfo.i4ETSRemIndex != DCBX_ZERO))
            {
                *pi4NextLldpV2RemLocalIfIndex = (INT4) pETSPortEntry->u4IfIndex;
                *pu4NextLldpV2RemTimeMark =
                    pETSPortEntry->ETSRemPortInfo.u4ETSRemTimeMark;
                *pu4NextLldpV2RemLocalDestMACAddress = 
                    pETSPortEntry->ETSRemPortInfo.u4ETSDestRemMacIndex;

                *pi4NextLldpV2RemIndex =
                    pETSPortEntry->ETSRemPortInfo.i4ETSRemIndex;
                return SNMP_SUCCESS;

            }
            if (pETSPortEntry != NULL)
            {
                MEMSET (&ETSPortEntry, DCBX_ZERO, sizeof (tETSPortEntry));
                ETSPortEntry.u4IfIndex = pETSPortEntry->u4IfIndex;
            }
        }
        while (pETSPortEntry != NULL);
    }
    else
    {
        *pi4NextLldpV2RemLocalIfIndex = (INT4) pETSPortEntry->u4IfIndex;
        *pu4NextLldpV2RemTimeMark =
            pETSPortEntry->ETSRemPortInfo.u4ETSRemTimeMark;
        *pu4NextLldpV2RemLocalDestMACAddress = 
            pETSPortEntry->ETSRemPortInfo.u4ETSDestRemMacIndex;
        *pi4NextLldpV2RemIndex = pETSPortEntry->ETSRemPortInfo.i4ETSRemIndex;
        return SNMP_SUCCESS;

    }
    DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : Rbtree GetNext Node () "
                   "Returns NULL. \r\n", __FUNCTION__);
    return SNMP_FAILURE;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFslldpXdot1dcbxRemTCSupported
 Input       :  The Indices
                LldpV2RemTimeMark
                LldpV2RemLocalIfIndex
                u4LldpV2RemLocalDestMACAddress
                LldpV2RemIndex

                The Object 
                retValFslldpXdot1dcbxRemTCSupported
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFslldpXdot1dcbxRemTCSupported (UINT4 u4LldpV2RemTimeMark,
                                     INT4 i4LldpV2RemLocalIfIndex,
                                     UINT4 u4LldpV2RemLocalDestMACAddress,
                                     INT4 i4LldpV2RemIndex,
                                     INT4
                                     *pi4RetValFslldpXdot1dcbxRemTCSupported)
{
    tETSPortEntry      *pETSPortEntry = NULL;

    UNUSED_PARAM (u4LldpV2RemTimeMark);
    UNUSED_PARAM (i4LldpV2RemIndex);
    UNUSED_PARAM (u4LldpV2RemLocalDestMACAddress);

    pETSPortEntry = ETSUtlGetPortEntry ((UINT4) i4LldpV2RemLocalIfIndex);
    if (pETSPortEntry == NULL)
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : ETSUtlGetPortEntry() "
                       "Returns NULL. \r\n", __FUNCTION__);
        return SNMP_FAILURE;
    }

    if (pETSPortEntry->ETSRemPortInfo.i4ETSRemIndex == DCBX_ZERO)
    {
        DCBX_TRC (DCBX_MGMT_TRC, NIL_INFO);
        return SNMP_FAILURE;
    }

    *pi4RetValFslldpXdot1dcbxRemTCSupported =
        pETSPortEntry->ETSRemPortInfo.u1ETSRemNumTcSup;

    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FslldpXdot1dcbxAdminTCSupportedTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFslldpXdot1dcbxAdminTCSupportedTable
 Input       :  The Indices
                LldpV2LocPortIfIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFslldpXdot1dcbxAdminTCSupportedTable (INT4
                                                              i4LldpV2LocPortIfIndex)
{
    tETSPortEntry      *pETSPortEntry = NULL;

    ETS_SHUTDOWN_CHECK;

    pETSPortEntry = ETSUtlGetPortEntry ((UINT4) i4LldpV2LocPortIfIndex);
    if (pETSPortEntry == NULL)
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : ETSUtlGetPortEntry() "
                       "Returns NULL. \r\n", __FUNCTION__);
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFslldpXdot1dcbxAdminTCSupportedTable
 Input       :  The Indices
                LldpV2LocPortIfIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFslldpXdot1dcbxAdminTCSupportedTable (INT4
                                                      *pi4LldpV2LocPortIfIndex)
{
    tETSPortEntry      *pETSPortEntry = NULL;

    ETS_SHUTDOWN_CHECK;

    pETSPortEntry = (tETSPortEntry *)
        RBTreeGetFirst (gETSGlobalInfo.pRbETSPortTbl);
    if (pETSPortEntry == NULL)
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : RbTreeGetFirst Node () "
                       "Returns NULL. \r\n", __FUNCTION__);
        return SNMP_FAILURE;
    }

    *pi4LldpV2LocPortIfIndex = (INT4) pETSPortEntry->u4IfIndex;
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetNextIndexFslldpXdot1dcbxAdminTCSupportedTable
 Input       :  The Indices
                LldpV2LocPortIfIndex
                nextLldpV2LocPortIfIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFslldpXdot1dcbxAdminTCSupportedTable (INT4
                                                     i4LldpV2LocPortIfIndex,
                                                     INT4
                                                     *pi4NextLldpV2LocPortIfIndex)
{
    tETSPortEntry      *pETSPortEntry = NULL;
    tETSPortEntry       ETSPortEntry;

    ETS_SHUTDOWN_CHECK;

    MEMSET (&ETSPortEntry, DCBX_ZERO, sizeof (tETSPortEntry));
    ETSPortEntry.u4IfIndex = (UINT4) i4LldpV2LocPortIfIndex;
    pETSPortEntry = (tETSPortEntry *)
        RBTreeGetNext (gETSGlobalInfo.pRbETSPortTbl,
                       &ETSPortEntry, DcbxUtlPortTblCmpFn);
    if (pETSPortEntry == NULL)
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : Rbtree GetNext Node () "
                       "Returns NULL. \r\n", __FUNCTION__);
        return SNMP_FAILURE;
    }

    *pi4NextLldpV2LocPortIfIndex = (INT4) pETSPortEntry->u4IfIndex;
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFslldpXdot1dcbxAdminTCSupported
 Input       :  The Indices
                LldpV2LocPortIfIndex

                The Object 
                retValFslldpXdot1dcbxAdminTCSupported
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFslldpXdot1dcbxAdminTCSupported (INT4 i4LldpV2LocPortIfIndex,
                                       INT4
                                       *pi4RetValFslldpXdot1dcbxAdminTCSupported)
{

    tETSPortEntry      *pETSPortEntry = NULL;

    pETSPortEntry = ETSUtlGetPortEntry ((UINT4) i4LldpV2LocPortIfIndex);
    if (pETSPortEntry == NULL)
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : ETSUtlGetPortEntry () "
                       " Returns FAILURE. \r\n", __FUNCTION__);
        return SNMP_FAILURE;
    }

    *pi4RetValFslldpXdot1dcbxAdminTCSupported =
        (pETSPortEntry->ETSAdmPortInfo).u1ETSAdmNumTcSup;
    return SNMP_SUCCESS;
}
/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsDcbxCEEGlobalEnableTrap
 Input       :  The Indices

                The Object 
                retValFsDcbxCEEGlobalEnableTrap
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsDcbxCEEGlobalEnableTrap(INT4 *pi4RetValFsDcbxCEEGlobalEnableTrap)
{
	*pi4RetValFsDcbxCEEGlobalEnableTrap = (INT4) gCEEGlobalInfo.u4CEETrap;
	return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetFsDcbxCEEGeneratedTrapCount
 Input       :  The Indices

                The Object 
                retValFsDcbxCEEGeneratedTrapCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsDcbxCEEGeneratedTrapCount(UINT4 *pu4RetValFsDcbxCEEGeneratedTrapCount)
{
	*pu4RetValFsDcbxCEEGeneratedTrapCount = gCEEGlobalInfo.u4CEETrapGeneratedCount;
	DCBX_TRC_ARG2 (DCBX_MGMT_TRC,
			"%s : CEE Global Generated Trap Count = %d SUCCESS.\r\n", __FUNCTION__,
			*pu4RetValFsDcbxCEEGeneratedTrapCount);
	return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetFsDcbxCEEClearCounters
 Input       :  The Indices

                The Object 
                retValFsDcbxCEEClearCounters
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsDcbxCEEClearCounters(INT4 *pi4RetValFsDcbxCEEClearCounters)
{
    *pi4RetValFsDcbxCEEClearCounters = PFC_DISABLED;
    DCBX_TRC_ARG2 (DCBX_MGMT_TRC, "%s : PFC Clear Counter = %d SUCCESS.\r\n",
                   __FUNCTION__, *pi4RetValFsDcbxCEEClearCounters);
    return SNMP_SUCCESS;
}
/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsDcbxCEEGlobalEnableTrap
 Input       :  The Indices

                The Object 
                setValFsDcbxCEEGlobalEnableTrap
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhSetFsDcbxCEEGlobalEnableTrap(INT4 i4SetValFsDcbxCEEGlobalEnableTrap)
{
    DCBX_TRC_ARG2 (DCBX_MGMT_TRC, "%s, Args: "
            "i4SetValFsDcbxCEEGlobalEnableTrap: %d\r\n", 
            __func__, i4SetValFsDcbxCEEGlobalEnableTrap);

	gCEEGlobalInfo.u4CEETrap = (UINT4) i4SetValFsDcbxCEEGlobalEnableTrap;
	return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhSetFsDcbxCEEClearCounters
 Input       :  The Indices

                The Object 
                setValFsDcbxCEEClearCounters
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhSetFsDcbxCEEClearCounters(INT4 i4SetValFsDcbxCEEClearCounters)
{
	tCEECtrlEntry     *pCEECtrlEntry = NULL;
    INT4                i4CEEPortNumber = DCBX_ZERO;
    INT4                i4NextCEEPortNumber = DCBX_ZERO;
    INT4                i4RetVal = SNMP_SUCCESS;

    DCBX_TRC_ARG2 (DCBX_MGMT_TRC, "%s, Args: "
            "i4SetValFsDcbxCEEClearCounters: %d\r\n", 
            __func__, i4SetValFsDcbxCEEClearCounters);

    if (i4SetValFsDcbxCEEClearCounters == APP_PRI_ENABLED)
    {
        i4RetVal = nmhGetFirstIndexFsDcbxCEECtrlTable (&i4NextCEEPortNumber);
        if (i4RetVal == SNMP_FAILURE)
        {
            DCBX_TRC_ARG1 (DCBX_MGMT_TRC,
                           "In %s : nmhGetFirstIndexFsAppPriPortTable () "
                           "No CEE Port Entries is present to clear . \r\n",
                           __FUNCTION__);
            return SNMP_SUCCESS;
        }

        do
        {
            i4CEEPortNumber = i4NextCEEPortNumber;
            pCEECtrlEntry = CEEUtlGetCtrlEntry ((UINT4) i4CEEPortNumber);
            if (pCEECtrlEntry != NULL)
            {
                pCEECtrlEntry->u4CtrlTxTLVCount = DCBX_ZERO;
                pCEECtrlEntry->u4CtrlRxTLVCount = DCBX_ZERO;
                pCEECtrlEntry->u4CtrlRxTLVErrorCount = DCBX_ZERO;
            }
        }
        while (nmhGetNextIndexFsDcbxCEECtrlTable
               (i4CEEPortNumber, &i4NextCEEPortNumber) != SNMP_FAILURE);
    }
    return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhSetFsDcbxCEETlvClearCounters
 Input       :  The Indices

                The Object 
                setValFsDcbxCEETlvClearCounters
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhSetFsDcbxCEETlvClearCounters(INT4 i4SetValFsDcbxCEETlvClearCounters, INT4 i4CEEPortNumber)
{
    tCEECtrlEntry     *pCEECtrlEntry = NULL;

    DCBX_TRC_ARG3 (DCBX_MGMT_TRC, "%s, Args: "
            "i4SetValFsDcbxCEETlvClearCounters: %d "
            "i4CEEPortNumber: %d\r\n", 
            __func__, i4SetValFsDcbxCEETlvClearCounters, i4CEEPortNumber);

    if (i4SetValFsDcbxCEETlvClearCounters == APP_PRI_ENABLED)
    {
            pCEECtrlEntry = CEEUtlGetCtrlEntry ((UINT4) i4CEEPortNumber);
            if (pCEECtrlEntry != NULL)
            {
                pCEECtrlEntry->u4CtrlTxTLVCount = DCBX_ZERO;
                pCEECtrlEntry->u4CtrlRxTLVCount = DCBX_ZERO;
                pCEECtrlEntry->u4CtrlRxTLVErrorCount = DCBX_ZERO;
            }
    }
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsDcbxCEEGlobalEnableTrap
 Input       :  The Indices

                The Object 
                testValFsDcbxCEEGlobalEnableTrap
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhTestv2FsDcbxCEEGlobalEnableTrap(UINT4 *pu4ErrorCode , INT4 i4TestValFsDcbxCEEGlobalEnableTrap)
{
    if (i4TestValFsDcbxCEEGlobalEnableTrap & (~(DCBX_CEE_ALL_TRAP)))
    {
        DCBX_TRC_ARG2 (DCBX_MGMT_TRC,
                       "%s : CEE Global Trap Level%d FAILED.\r\n", __FUNCTION__,
                       i4TestValFsDcbxCEEGlobalEnableTrap);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_ERR_CEE_INVALID_TRAP_VAL);
        return (SNMP_FAILURE);
    }
    return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhTestv2FsDcbxCEEClearCounters
 Input       :  The Indices

                The Object 
                testValFsDcbxCEEClearCounters
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhTestv2FsDcbxCEEClearCounters(UINT4 *pu4ErrorCode , INT4 i4TestValFsDcbxCEEClearCounters)
{
    if ((i4TestValFsDcbxCEEClearCounters != DCBX_ENABLED) &&
        (i4TestValFsDcbxCEEClearCounters != DCBX_DISABLED))
    {
        DCBX_TRC_ARG2 (DCBX_MGMT_TRC,
                       "%s : CEE Clear Counter Status%d FAILED.\r\n",
                       __FUNCTION__, i4TestValFsDcbxCEEClearCounters);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_ERR_CEE_INVALID_CLEAR_COUNTER_VAL);
        return (SNMP_FAILURE);
    }

    return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhTestv2FsDcbxCEETlvClearCounters
 Input       :  The Indices

                The Object 
                testValFsDcbxCEETlvClearCounters
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhTestv2FsDcbxCEETlvClearCounters(UINT4 *pu4ErrorCode ,
			INT4 i4FsDcbxCEECtrlPortNumber, INT4 i4TestValFsDcbxCEETlvClearCounters)
{
    /* Validate Port Entry */
    if (DcbxUtlValidatePort ((UINT4) i4FsDcbxCEECtrlPortNumber) == OSIX_FAILURE)
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : DcbxUtlValidatePort() "
                       "Returns Failure. \r\n", __FUNCTION__);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_ERR_DCBX_NO_PORT);
        return SNMP_FAILURE;
    }

    if ((i4TestValFsDcbxCEETlvClearCounters != DCBX_ENABLED) &&
        (i4TestValFsDcbxCEETlvClearCounters != DCBX_DISABLED))
    {
        DCBX_TRC_ARG2 (DCBX_MGMT_TRC,
                       "%s : CEE Clear Counter Status%d FAILED.\r\n",
                       __FUNCTION__, i4TestValFsDcbxCEETlvClearCounters);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_ERR_CEE_INVALID_CLEAR_COUNTER_VAL);
        return (SNMP_FAILURE);
    }
    return SNMP_SUCCESS;
}
/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsDcbxCEEGlobalEnableTrap
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhDepv2FsDcbxCEEGlobalEnableTrap(UINT4 *pu4ErrorCode, tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpVarBind)
{
	UNUSED_PARAM(pu4ErrorCode);
	UNUSED_PARAM(pSnmpIndexList);
	UNUSED_PARAM(pSnmpVarBind);
	return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhDepv2FsDcbxCEEClearCounters
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhDepv2FsDcbxCEEClearCounters(UINT4 *pu4ErrorCode, tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpVarBind)
{
	UNUSED_PARAM(pu4ErrorCode);
	UNUSED_PARAM(pSnmpIndexList);
	UNUSED_PARAM(pSnmpVarBind);
	return SNMP_SUCCESS;
}
/* LOW LEVEL Routines for Table : FsDcbxCEECtrlTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsDcbxCEECtrlTable
 Input       :  The Indices
                FsDcbxCEECtrlPortNumber
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1 nmhValidateIndexInstanceFsDcbxCEECtrlTable(INT4 i4FsDcbxCEECtrlPortNumber)
{
    tCEECtrlEntry      *pCEECtrlEntry = NULL;

    pCEECtrlEntry = CEEUtlGetCtrlEntry ((UINT4) i4FsDcbxCEECtrlPortNumber);
    if (pCEECtrlEntry == NULL)
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : CEEUtlGetCtrlEntry () "
                       "Returned FAILURE. \r\n", __FUNCTION__);
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}
/****************************************************************************
 Function    :  nmhGetFirstIndexFsDcbxCEECtrlTable
 Input       :  The Indices
                FsDcbxCEECtrlPortNumber
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1 nmhGetFirstIndexFsDcbxCEECtrlTable(INT4 *pi4FsDcbxCEECtrlPortNumber)
{
    tCEECtrlEntry     *pCEECtrlEntry = NULL;
    pCEECtrlEntry = (tCEECtrlEntry *)
	    RBTreeGetFirst(gCEEGlobalInfo.pRbCeeCtrlTbl);
    if (pCEECtrlEntry == NULL)
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : RBTreeGetFirst () "
                       "Returned FAILURE. \r\n", __FUNCTION__);
        return SNMP_FAILURE;
    }

    *pi4FsDcbxCEECtrlPortNumber = (INT4) pCEECtrlEntry->u4IfIndex;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsDcbxCEECtrlTable
 Input       :  The Indices
                FsDcbxCEECtrlPortNumber
                nextFsDcbxCEECtrlPortNumber
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1 nmhGetNextIndexFsDcbxCEECtrlTable(INT4 i4FsDcbxCEECtrlPortNumber ,INT4 *pi4NextFsDcbxCEECtrlPortNumber )
{
    tCEECtrlEntry     *pCEECtrlEntry = NULL;
    tCEECtrlEntry      CEECtrlEntry;

    MEMSET (&CEECtrlEntry, DCBX_INIT_VAL, sizeof (tCEECtrlEntry));
    CEECtrlEntry.u4IfIndex = (UINT4) i4FsDcbxCEECtrlPortNumber;
    pCEECtrlEntry = (tCEECtrlEntry *)
        RBTreeGetNext (gCEEGlobalInfo.pRbCeeCtrlTbl,
                       &CEECtrlEntry, CEEUtlPortTblCmpFn);
    if (pCEECtrlEntry == NULL)
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : RBTreeGetNext () "
                       "Returned FAILURE. \r\n", __FUNCTION__);
        return SNMP_FAILURE;
    }

    *pi4NextFsDcbxCEECtrlPortNumber = (INT4) pCEECtrlEntry->u4IfIndex;
    return SNMP_SUCCESS;
}
/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsDcbxCEECtrlSeqNo
 Input       :  The Indices
                FsDcbxCEECtrlPortNumber

                The Object 
                retValFsDcbxCEECtrlSeqNo
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsDcbxCEECtrlSeqNo(INT4 i4FsDcbxCEECtrlPortNumber, 
        UINT4 *pu4RetValFsDcbxCEECtrlSeqNo)
{
    tCEECtrlEntry      *pCEECtrlEntry = NULL;

    pCEECtrlEntry = CEEUtlGetCtrlEntry ((UINT4) i4FsDcbxCEECtrlPortNumber);
    if (pCEECtrlEntry == NULL)
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : CEEUtlGetCtrlEntry () "
                       "Returned FAILURE. \r\n", __FUNCTION__);
        return SNMP_FAILURE;
    }

    *pu4RetValFsDcbxCEECtrlSeqNo = pCEECtrlEntry->u4CtrlSeqNo;
    return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetFsDcbxCEECtrlAckNo
 Input       :  The Indices
                FsDcbxCEECtrlPortNumber

                The Object 
                retValFsDcbxCEECtrlAckNo
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsDcbxCEECtrlAckNo(INT4 i4FsDcbxCEECtrlPortNumber, 
        UINT4 *pu4RetValFsDcbxCEECtrlAckNo)
{
    tCEECtrlEntry      *pCEECtrlEntry = NULL;

    pCEECtrlEntry = CEEUtlGetCtrlEntry ((UINT4) i4FsDcbxCEECtrlPortNumber);
    if (pCEECtrlEntry == NULL)
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : CEEUtlGetCtrlEntry () "
                       "Returned FAILURE. \r\n", __FUNCTION__);
        return SNMP_FAILURE;
    }

    *pu4RetValFsDcbxCEECtrlAckNo = pCEECtrlEntry->u4CtrlAckNo;
    return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetFsDcbxCEECtrlRcvdAckNo
 Input       :  The Indices
                FsDcbxCEECtrlPortNumber

                The Object 
                retValFsDcbxCEECtrlRcvdAckNo
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsDcbxCEECtrlRcvdAckNo(INT4 i4FsDcbxCEECtrlPortNumber, 
        UINT4 *pu4RetValFsDcbxCEECtrlRcvdAckNo)
{
    tCEECtrlEntry      *pCEECtrlEntry = NULL;

    pCEECtrlEntry = CEEUtlGetCtrlEntry ((UINT4) i4FsDcbxCEECtrlPortNumber);
    if (pCEECtrlEntry == NULL)
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : CEEUtlGetCtrlEntry () "
                       "Returned FAILURE. \r\n", __FUNCTION__);
        return SNMP_FAILURE;
    }

    *pu4RetValFsDcbxCEECtrlRcvdAckNo = pCEECtrlEntry->u4CtrlRcvdAckNo;
    return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetFsDcbxCEECtrlTxTLVCounter
 Input       :  The Indices
                FsDcbxCEECtrlPortNumber

                The Object 
                retValFsDcbxCEECtrlTxTLVCounter
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsDcbxCEECtrlTxTLVCounter(INT4 i4FsDcbxCEECtrlPortNumber, 
        UINT4 *pu4RetValFsDcbxCEECtrlTxTLVCounter)
{
    tCEECtrlEntry      *pCEECtrlEntry = NULL;

    pCEECtrlEntry = CEEUtlGetCtrlEntry ((UINT4) i4FsDcbxCEECtrlPortNumber);
    if (pCEECtrlEntry == NULL)
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : CEEUtlGetCtrlEntry () "
                       "Returned FAILURE. \r\n", __FUNCTION__);
        return SNMP_FAILURE;
    }

    *pu4RetValFsDcbxCEECtrlTxTLVCounter = pCEECtrlEntry->u4CtrlTxTLVCount;
    return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetFsDcbxCEECtrlRxTLVCounter
 Input       :  The Indices
                FsDcbxCEECtrlPortNumber

                The Object 
                retValFsDcbxCEECtrlRxTLVCounter
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsDcbxCEECtrlRxTLVCounter(INT4 i4FsDcbxCEECtrlPortNumber , 
        UINT4 *pu4RetValFsDcbxCEECtrlRxTLVCounter)
{
    tCEECtrlEntry      *pCEECtrlEntry = NULL;

    pCEECtrlEntry = CEEUtlGetCtrlEntry ((UINT4) i4FsDcbxCEECtrlPortNumber);
    if (pCEECtrlEntry == NULL)
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : CEEUtlGetCtrlEntry () "
                       "Returned FAILURE. \r\n", __FUNCTION__);
        return SNMP_FAILURE;
    }

    *pu4RetValFsDcbxCEECtrlRxTLVCounter = pCEECtrlEntry->u4CtrlRxTLVCount;
    return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetFsDcbxCEECtrlRxTLVErrorCounter
 Input       :  The Indices
                FsDcbxCEECtrlPortNumber

                The Object 
                retValFsDcbxCEECtrlRxTLVErrorCounter
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsDcbxCEECtrlRxTLVErrorCounter(INT4 i4FsDcbxCEECtrlPortNumber,
        UINT4 *pu4RetValFsDcbxCEECtrlRxTLVErrorCounter)
{
    tCEECtrlEntry      *pCEECtrlEntry = NULL;

    pCEECtrlEntry = CEEUtlGetCtrlEntry ((UINT4) i4FsDcbxCEECtrlPortNumber);
    if (pCEECtrlEntry == NULL)
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : CEEUtlGetCtrlEntry () "
                       "Returned FAILURE. \r\n", __FUNCTION__);
        return SNMP_FAILURE;
    }

    *pu4RetValFsDcbxCEECtrlRxTLVErrorCounter = pCEECtrlEntry->u4CtrlRxTLVErrorCount;
    return SNMP_SUCCESS;
}


/****************************************************************************
 Function    :  nmhGetFsDcbOperVersion
 Input       :  The Indices
                FsDcbPortNumber

                The Object 
                retValFsDcbOperVersion
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDcbOperVersion (INT4 i4FsDcbPortNumber, INT4 *pi4RetValFsDcbOperVersion)
{
    tDcbxPortEntry      *pDcbxPortEntry = NULL;

    pDcbxPortEntry = DcbxUtlGetPortEntry ((UINT4) i4FsDcbPortNumber);
	if(pDcbxPortEntry == NULL)
	{
		DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : DcbxUtlGetPortEntry () "
				"Returns FAILURE. \r\n", __FUNCTION__);
		return SNMP_FAILURE;
	}
    *pi4RetValFsDcbOperVersion = pDcbxPortEntry->u1OperVersion; 
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsDcbMaxVersion
 Input       :  The Indices
                FsDcbPortNumber

                The Object 
                retValFsDcbOperVersion
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDcbMaxVersion (INT4 i4FsDcbPortNumber, INT4 *pi4RetValFsDcbMaxVersion)
{
    tDcbxPortEntry      *pDcbxPortEntry = NULL;

    pDcbxPortEntry = DcbxUtlGetPortEntry ((UINT4) i4FsDcbPortNumber);
	if(pDcbxPortEntry == NULL)
	{
		DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : DcbxUtlGetPortEntry () "
				"Returns FAILURE. \r\n", __FUNCTION__);
		return SNMP_FAILURE;
	}
    *pi4RetValFsDcbMaxVersion = pDcbxPortEntry->u1MaxVersion; 
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsDcbPeerOperVersion
 Input       :  The Indices
                FsDcbPortNumber

                The Object 
                retValFsDcbOperVersion
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDcbPeerOperVersion (INT4 i4FsDcbPortNumber, INT4 *pi4RetValFsDcbPeerOperVersion)
{
    tDcbxPortEntry      *pDcbxPortEntry = NULL;

    pDcbxPortEntry = DcbxUtlGetPortEntry ((UINT4) i4FsDcbPortNumber);
    if(pDcbxPortEntry == NULL)
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : DcbxUtlGetPortEntry () "
                "Returns FAILURE. \r\n", __FUNCTION__);
        return SNMP_FAILURE;
    }
    *pi4RetValFsDcbPeerOperVersion = pDcbxPortEntry->u1PeerOperVersion; 
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsDcbPeerMaxVersion
 Input       :  The Indices
                FsDcbPortNumber

                The Object 
                retValFsDcbPeerMaxVersion
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDcbPeerMaxVersion (INT4 i4FsDcbPortNumber, INT4 *pi4RetValFsDcbPeerMaxVersion)
{
    tDcbxPortEntry      *pDcbxPortEntry = NULL;

    pDcbxPortEntry = DcbxUtlGetPortEntry ((UINT4) i4FsDcbPortNumber);
    if(pDcbxPortEntry == NULL)
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : DcbxUtlGetPortEntry () "
                "Returns FAILURE. \r\n", __FUNCTION__);
        return SNMP_FAILURE;
    }
    *pi4RetValFsDcbPeerMaxVersion = pDcbxPortEntry->u1PeerMaxVersion; 
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsDCBXMode
 Input       :  The Indices
                FsDCBXPortNumber

                The Object 
                retValFsDCBXMode
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsDCBXMode(INT4 i4FsDCBXPortNumber , INT4 *pi4RetValFsDCBXMode)
{
    tDcbxPortEntry      *pDcbxPortEntry = NULL;

    pDcbxPortEntry = DcbxUtlGetPortEntry ((UINT4) i4FsDCBXPortNumber);
    if (pDcbxPortEntry == NULL)
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : DcbxUtlGetPortEntry () "
                "Returns FAILURE. \r\n", __FUNCTION__);
        return SNMP_FAILURE;
    }

    *pi4RetValFsDCBXMode = pDcbxPortEntry->u1DcbxMode;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsDCBXMode
 Input       :  The Indices
                FsDCBXPortNumber

                The Object 
                setValFsDCBXMode
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhSetFsDCBXMode(INT4 i4FsDCBXPortNumber , INT4 i4SetValFsDCBXMode)
{
    INT4 i4PrevMode = DCBX_VER_UNKNOWN;
    tDcbxPortEntry      *pDcbxPortEntry = NULL;

    DCBX_TRC_ARG3 (DCBX_MGMT_TRC, "%s, Args: "
            "i4FsDCBXPortNumber: %d "
            "i4SetValFsDCBXMode: %d\r\n", 
            __func__, i4FsDCBXPortNumber, i4SetValFsDCBXMode);

    pDcbxPortEntry = DcbxUtlGetPortEntry ((UINT4) i4FsDCBXPortNumber);
    if (pDcbxPortEntry == NULL)
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : DcbxUtlGetPortEntry () "
                       "Returns FAILURE. \r\n", __FUNCTION__);
         CLI_SET_ERR (CLI_ERR_DCBX_NO_PORT_ENTRY);
        return SNMP_FAILURE;
    }
    if (i4SetValFsDCBXMode != pDcbxPortEntry->u1DcbxMode)
    {
        i4PrevMode = pDcbxPortEntry->u1DcbxMode;
        pDcbxPortEntry->u1DcbxMode = (UINT1) i4SetValFsDCBXMode;
        DcbxUtlSwitchMode((UINT1) i4SetValFsDCBXMode, 
                (UINT1) i4PrevMode, (UINT4) i4FsDCBXPortNumber);

        if (i4SetValFsDCBXMode == DCBX_MODE_AUTO)
        {
            CEEUtlSetSemType (i4FsDCBXPortNumber, DCBX_DEFAULT_VER);
        }
        else
        {
            CEEUtlSetSemType (i4FsDCBXPortNumber, (UINT1) i4SetValFsDCBXMode);
        }
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsDCBXMode
 Input       :  The Indices
                FsDCBXPortNumber

                The Object 
                testValFsDCBXMode
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhTestv2FsDCBXMode(UINT4 *pu4ErrorCode , INT4 i4FsDCBXPortNumber , INT4 i4TestValFsDCBXMode)
{

    tDcbxPortEntry     *pDcbxPortEntry = NULL;
    if (DcbxUtlValidatePort ((UINT4) i4FsDCBXPortNumber) == OSIX_FAILURE)
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : DcbxUtlValidatePort() "
                       "Returns Failure. \r\n", __FUNCTION__);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_ERR_DCBX_NO_PORT);
        return SNMP_FAILURE;
    }

    pDcbxPortEntry = DcbxUtlGetPortEntry ((UINT4) i4FsDCBXPortNumber);
    if (pDcbxPortEntry == NULL)
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : DcbxUtlGetPortEntry () "
                       "Returns FAILURE. \r\n", __FUNCTION__);
        CLI_SET_ERR (CLI_ERR_DCBX_NO_PORT_ENTRY);
        return SNMP_FAILURE;
    }

    if((i4TestValFsDCBXMode != DCBX_MODE_AUTO) &&
       (i4TestValFsDCBXMode != DCBX_MODE_IEEE) &&
       (i4TestValFsDCBXMode != DCBX_MODE_CEE))
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : Invaild Mode!  "
                "Returning Failure. \r\n", __FUNCTION__);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_ERR_INVALID_DCBX_MODE);
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}
