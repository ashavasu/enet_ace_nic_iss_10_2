/*************************************************************************
 * Copyright (C) 2010 Aricent Inc . All Rights Reserved
 * $Id: dcbxcli.c,v 1.41 2017/12/06 09:29:08 siva Exp $
****************************************************************************/
/*************************************************************************/
/* Copyright (C) 2010 Aricent Inc . All Rights Reserved                  */
/*                                                                       */
/*  FILE NAME                     : dcbxcli.c                                */
/*  PRINCIPAL AUTHOR            : Aricent                                 */
/*  SUBSYSTEM NAME               : DCB                                     */
/*  MODULE NAME                  : DCBX-CLI                                */
/*  LANGUAGE                     : C                                       */
/*  TARGET ENVIRONMENT      : Linux                                      */
/*  AUTHOR                           : Aricent                             */
/*  FUNCTIONS DEFINED(Applicable for Source files) :                     */
/*  DESCRIPTION        : This module provide the CLI Mangement Interface */
/*                                    for the Aricent DCBX Module.       */
/*************************************************************************/
#ifndef DCBX_CLI_C
#define DCBX_CLI_C

#include "dcbxinc.h"
#include "fsdcbxcli.h"
#include "stddcbcli.h"

/*ETS Cli Funtion ProtoTypes */
PRIVATE INT4        ETSCliSetControlStatus
PROTO ((tCliHandle CliHandle, UINT1 u1Status));
PRIVATE INT4        ETSCliSetModuleStatus
PROTO ((tCliHandle CliHandle, UINT1 u1Status));
PRIVATE INT4        ETSCliSetPortStatus
PROTO ((tCliHandle CliHandle, INT4 i4IfIndex, UINT1 i1ETSRowStatus));
PRIVATE INT4        ETSCliSetPortMode
PROTO ((tCliHandle CliHandle, INT4 i4IfIndex, UINT1 u1AdminMode));
PRIVATE INT4        ETSCliSetTrapStatus
PROTO ((tCliHandle CliHandle, UINT4 u4TrapType, UINT1 u1Status));
PRIVATE INT4        ETSCliSetWillingStatus
PROTO ((tCliHandle CliHandle, INT4 i4IfIndex, UINT1 u1Status));
PRIVATE INT4        ETSCliSetPriToPGMapping
PROTO ((tCliHandle CliHandle, INT4 i4IfIndex, INT4 *pi4PriList, UINT4 u4PgId));
PRIVATE INT4        ETSCliSetBwToPg
PROTO ((tCliHandle CliHandle, INT4 i4IfIndex, UINT1 u1Value, UINT4 *au4EtsBW));
PRIVATE INT4        ETSCliSetTlvSelection
PROTO ((tCliHandle CliHandle, INT4 i4IfIndex, UINT1 u1ETSConfTlv,
        UINT1 u1ETSRecoTlv, UINT1 u1ETSTcSuppTlv, UINT1 u1Status));
PRIVATE INT4 ETSCliClearCounters PROTO ((tCliHandle CliHandle, INT4 i4IfIndex));
PRIVATE INT4        ETSCliShowPortCounters
PROTO ((tCliHandle CliHandle, INT4 i4IfIndex));
PRIVATE INT4        ETSCliShowPortInfo
PROTO ((tCliHandle CliHandle, INT4 i4IfIndex, UINT1 u1Detail));
PRIVATE INT4        ETSCliShowPortDetails
PROTO ((tCliHandle CliHandle, INT4 i4IfIndex, UINT1 u1Detail));
PRIVATE INT4        ETSCliShowLocalPortInfo
PROTO ((tCliHandle CliHandle, INT4 i4IfIndex));
PRIVATE INT4        ETSCliShowAdminPortInfo
PROTO ((tCliHandle CliHandle, INT4 i4IfIndex));
PRIVATE INT4        ETSCliShowRemPortInfo
PROTO ((tCliHandle CliHandle, INT4 i4IfIndex));
PRIVATE INT4 ETSCliShowGbl PROTO ((tCliHandle CliHandle));
PRIVATE INT4        ETSShowRunningInterfaceConfig
PROTO ((tCliHandle CliHandle, INT4 i4IntIndex, INT1 i1DcbxShowInt));
PRIVATE INT4        ETSCliShowPortCountersInfo
PROTO ((tCliHandle CliHandle, INT4 i4IfIndex));
PRIVATE INT4        ETSCliSetTSAPerTC (INT4 i4IfIndex, INT4 *pi4TcNoList,
                                       INT4 u4TsaAlgo,
                                       UINT1 u1UpdateAdmConOrAdmReco);

/* PFC CLI Function Prototypes */

PRIVATE INT4        PFCCliSetControlStatus
PROTO ((tCliHandle CliHandle, UINT1 u1Status));
PRIVATE INT4        PFCCliSetModuleStatus
PROTO ((tCliHandle CliHandle, UINT1 u1Status));
PRIVATE INT4        PFCCliSetPortMode
PROTO ((tCliHandle CliHandle, INT4 i4IfIndex, UINT1 u1AdminMode));
PRIVATE INT4        PFCCliSetPortStatus
PROTO ((tCliHandle CliHandle, INT4 i4IfIndex, UINT1 u1PFCRowStatus));
PRIVATE INT4        PFCCliSetTrapStatus
PROTO ((tCliHandle CliHandle, UINT4 u4TrapType, UINT1 u1Status));
PRIVATE INT4        PFCCliSetThreshold
PROTO ((tCliHandle CliHandle, UINT4 u4MinThersho1d, UINT4 u4MaxThreshold));
PRIVATE INT4        PFCCliSetWillingStatus
PROTO ((tCliHandle CliHandle, INT4 i4IfIndex, UINT1 u1Status));
PRIVATE INT4        PFCCliSetFCPerPriority
PROTO ((tCliHandle CliHandle, INT4 i4IfIndex, INT4 *pi4PriList, INT4 i4Status));
PRIVATE INT4        PFCCliSetTlvSelection
PROTO ((tCliHandle CliHandle, INT4 i4IfIndex, UINT1 u1Status));
PRIVATE INT4 PFCCliClearCounters PROTO ((tCliHandle CliHandle, INT4 i4IfIndex));
PRIVATE INT4        PFCCliShowPortCounters
PROTO ((tCliHandle CliHandle, INT4 i4IfIndex));
PRIVATE INT4        PFCCliShowPortInfo
PROTO ((tCliHandle CliHandle, INT4 i4IfIndex, UINT1 u1Detail));
PRIVATE INT4        PFCCliShowPortDetails
PROTO ((tCliHandle CliHandle, INT4 i4IfIndex, UINT1 u1Detail));
PRIVATE INT4        PFCCliShowLocalPortInfo
PROTO ((tCliHandle CliHandle, INT4 i4IfIndex));
PRIVATE INT4        PFCCliShowAdmPortInfo
PROTO ((tCliHandle CliHandle, INT4 i4IfIndex));
PRIVATE INT4        PFCCliShowRemPortInfo
PROTO ((tCliHandle CliHandle, INT4 i4IfIndex));
PRIVATE INT4 PFCCliShowGblInfo PROTO ((tCliHandle CliHandle));
PRIVATE INT4 PFCCliShowGbl PROTO ((tCliHandle CliHandle));
PRIVATE INT4        PFCShowRunningInterfaceConfig
PROTO ((tCliHandle CliHandle, INT4 i4IntIndex, INT1 i1DcbxShowInt));
PRIVATE INT4 PFCShowRunningGlobalConfig PROTO ((tCliHandle CliHandle));

/* DCBX Function Prototype */
PRIVATE INT4 DcbxCliSetStatus PROTO ((tCliHandle CliHandle, INT4 i4IfIndex,
                                      UINT1 u1AdminStatus));
PRIVATE INT4 DCBXCliSetDebugStatus PROTO ((tCliHandle CliHandle,
                                           INT4 i4TraceType, UINT1 u1Status));
PRIVATE INT4 DcbxConvertStrToPriArray PROTO ((tCliHandle CliHandle,
                                              UINT1 *pu1Str,
                                              INT4 *pi4PriArray));
PRIVATE INT4 DCBXCliShowPortInfo PROTO ((tCliHandle CliHandle));
PRIVATE INT4 DCBXCliClearCounters PROTO ((tCliHandle, INT4));
PRIVATE INT4 DcbxCeeCliSetTrapStatus PROTO ((tCliHandle, UINT4, UINT1));
PRIVATE INT4 DCBXCliShowPortsDetail PROTO ((tCliHandle, INT4));
PRIVATE INT4 DCBXCliShowCtrlTlvInfo PROTO ((tCliHandle, INT4));
PRIVATE INT4 DcbxCliSetMode PROTO ((tCliHandle, INT4, INT4));
PRIVATE INT4 CEEShowRunningInterfaceConfig PROTO ((tCliHandle, INT4, INT1));
PRIVATE INT4 ETSCliShowCEEPortDetails PROTO ((tCliHandle CliHandle,
                                              INT4 i4IfIndex, UINT1 u1Detail));
PRIVATE INT4 ETSCliShowCEELocalPortInfo PROTO ((tCliHandle CliHandle,
                                                INT4 i4IfIndex));
PRIVATE INT4 ETSCliShowCEEAdminPortInfo PROTO ((tCliHandle CliHandle,
                                                INT4 i4IfIndex));
PRIVATE INT4 ETSCliShowCEERemPortInfo PROTO ((tCliHandle CliHandle,
                                              INT4 i4IfIndex));

/*DCBX STATUS ARRAY*/
const CHR1         *au1DcbxStatus[MAX_DCBX_STATUS] = {
    "peerNotAdvFeat",
    "peerNotAdvDcbx",
    "notAdvertise",
    "disabled",
    "peerDisabled",
    "peerInError",
    "peerNWillingCompatibleCfg",
    "cfgNotCompatible",
    "ok",
    "statusUnknown",
    "CfgNotCompatibleButSupported"
};

/***************************************************************************/
/* Function Name      : cli_process_dcbx_cmd                               */
/* Description        : This function is Classify the Given Command and    */
/*                      configure the QoS module through CLI using setof   */
/*                      nmh routines.                                      */
/* Input(s)           : CliHandle - Handle to CLI session                  */
/*                    : u4Command - Command Type to Classify.              */
/*                    : ...       - Variable number of Arg for the above   */
/*                    :             Command type if possible.              */
/* Output(s)              : None.                                          */
/* Global Variables                                                        */
/* Referred           : gapi1DcbxCliErrString.                             */
/* Global Variables                                                        */
/* Modified           : None.                                              */
/* Return Value(s)    : CLI_SUCCESS/CLI_FAILURE                            */
/* Called By          : CLI Thread.                                        */
/* Calling Function   :                                                    */
/***************************************************************************/
PUBLIC INT4
cli_process_dcbx_cmd (tCliHandle CliHandle, UINT4 u4Command, ...)
{
    va_list             ap;
    UINT4              *apu4Args[DCBX_CLI_MAX_ARGS];
    INT1                i1ArgNo = DCBX_ZERO;
    UINT4               u4ErrCode = DCBX_ZERO;
    INT4                i4IfIndex = DCBX_ZERO;
    INT4                i4Inst = DCBX_ZERO;
    INT4                i4RetStatus = CLI_FAILURE;
    INT4                ai4PriList[DCBX_MAX_PRIORITIES] = { DCBX_ZERO };
    INT4                ai4TcNoList[ETS_MAX_TCGID_CONF + DCBX_ONE]
        = { DCBX_ZERO };
    INT4                i4SystemControl = DCBX_ZERO;
    UINT4               au4EtsBW[ETS_MAX_TCGID_CONF] = { DCBX_ZERO };
    UINT1               u1ConOrReco = DCBX_ZERO;

    MEMSET (au4EtsBW, DCBX_ZERO, ETS_MAX_TCGID_CONF * sizeof (UINT4));

    MEMSET (ai4PriList, DCBX_INVALID_ID,
            (DCBX_MAX_PRIORITIES * (sizeof (INT4))));

    MEMSET (ai4TcNoList, DCBX_INVALID_ID,
            (ETS_MAX_TCGID_CONF * (sizeof (INT4))));

    va_start (ap, u4Command);

    /* Third argument is always interface name/index */
    i4Inst = va_arg (ap, INT4);

    if (i4Inst != DCBX_ZERO)
    {
        i4IfIndex = (INT4) i4Inst;
    }

    /* Walk through  the arguments and store in apu4Args array. */

    while (DCBX_MIN_VAL)
    {
        apu4Args[i1ArgNo++] = va_arg (ap, UINT4 *);

        if (i1ArgNo == DCBX_CLI_MAX_ARGS)
        {
            break;
        }

    }
    va_end (ap);

    /* Register Lock with CLI */
    CliRegisterLock (CliHandle, DcbxLock, DcbxUnLock);
    DcbxLock ();

    if (u4Command <= CLI_ETS_COMMAND_END)
    {
        nmhGetFsETSSystemControl (&i4SystemControl);
        if ((u4Command != CLI_ETS_START) && (i4SystemControl == ETS_SHUTDOWN))
        {
            CliPrintf (CliHandle, "\r%% ETS is shutdown\r\n");
            DcbxUnLock ();
            CliUnRegisterLock (CliHandle);
            return CLI_FAILURE;
        }
    }
    else if (u4Command <= CLI_PFC_COMMAND_END)
    {
        nmhGetFsPFCSystemControl (&i4SystemControl);
        if ((u4Command != CLI_PFC_START) && (i4SystemControl == PFC_SHUTDOWN))
        {
            CliPrintf (CliHandle, "\r%% PFC is shutdown\r\n");
            DcbxUnLock ();
            CliUnRegisterLock (CliHandle);
            return CLI_FAILURE;
        }
    }
    else if (u4Command <= CLI_APP_PRI_COMMAND_END)
    {
        nmhGetFsAppPriSystemControl (&i4SystemControl);
        if ((u4Command != CLI_APP_PRI_START) &&
            (i4SystemControl == APP_PRI_SHUTDOWN))
        {
            CliPrintf (CliHandle,
                       "\r%% Application Prioirity Module is shutdown\r\n");
            DcbxUnLock ();
            CliUnRegisterLock (CliHandle);
            return CLI_FAILURE;
        }
    }
    switch (u4Command)
    {

            /*ETS Commands */
            /*ETS  Configuration Commands */

        case CLI_ETS_SHUTDOWN:

            i4RetStatus =
                ETSCliSetControlStatus (CliHandle, DCBX_SYS_CNTL_SHUTDOWN);
            break;

        case CLI_ETS_START:

            i4RetStatus =
                ETSCliSetControlStatus (CliHandle, DCBX_SYS_CNTL_START);
            break;

        case CLI_ETS_SYS_STATUS:

            /* apu4Args[0] = enable */
            /* apu4Args[0] = disable */
            if (apu4Args[DCBX_ZERO] != NULL)
            {
                i4RetStatus = ETSCliSetModuleStatus (CliHandle,
                                                     DCBX_SYS_STATUS_ENABLE);
            }
            else
            {
                i4RetStatus = ETSCliSetModuleStatus (CliHandle,
                                                     DCBX_SYS_STATUS_DISABLE);
            }
            break;

        case CLI_ETS_ENABLE_NOTIFY:

            i4RetStatus = ETSCliSetTrapStatus (CliHandle,
                                               CLI_PTR_TO_U4
                                               (apu4Args[DCBX_ZERO]),
                                               DCBX_TRAP_ENABLE);
            break;

        case CLI_ETS_DISABLE_NOTIFY:

            i4RetStatus = ETSCliSetTrapStatus (CliHandle,
                                               CLI_PTR_TO_U4
                                               (apu4Args[DCBX_ZERO]),
                                               DCBX_TRAP_DISABLE);
            break;
        case CLI_ETS_PORT_STATUS:
            /* apu4Args[DCBX_ZERO] = enable */
            /* apu4Args[1] = disable */

            i4IfIndex = CLI_GET_IFINDEX ();
            if (apu4Args[DCBX_ZERO] != NULL)
            {
                i4RetStatus = ETSCliSetPortStatus (CliHandle, i4IfIndex,
                                                   CREATE_AND_GO);
            }
            else
            {
                i4RetStatus =
                    ETSCliSetPortStatus (CliHandle, i4IfIndex, DESTROY);
            }
            break;
        case CLI_ETS_SET_PORT_MODE:

            i4IfIndex = CLI_GET_IFINDEX ();
            if (apu4Args[DCBX_ZERO] != NULL)
            {
                i4RetStatus = ETSCliSetPortMode (CliHandle, i4IfIndex,
                                                 DCBX_ADM_MODE_AUTO);
            }
            else
            {
                i4RetStatus = ETSCliSetPortMode (CliHandle, i4IfIndex,
                                                 DCBX_ADM_MODE_ON);
            }
            break;

        case CLI_ETS_PORT_MODE_OFF:

            i4IfIndex = CLI_GET_IFINDEX ();
            i4RetStatus = ETSCliSetPortMode (CliHandle, i4IfIndex,
                                             DCBX_ADM_MODE_OFF);
            break;

        case CLI_ETS_WILLING_STATUS:

            i4IfIndex = CLI_GET_IFINDEX ();
            if (apu4Args[DCBX_ZERO] != NULL)
            {
                i4RetStatus = ETSCliSetWillingStatus (CliHandle,
                                                      i4IfIndex, ETS_ENABLED);
            }
            else
            {
                i4RetStatus = ETSCliSetWillingStatus (CliHandle,
                                                      i4IfIndex, ETS_DISABLED);
            }
            break;

        case CLI_ETS_PRI_PG_MAP:
            i4IfIndex = CLI_GET_IFINDEX ();
            if ('-' == *((INT1 *) apu4Args[DCBX_ZERO]))
            {
                CLI_SET_ERR (CLI_ERR_ETS_INVALID_PRIORITY);
                i4RetStatus = CLI_FAILURE;
                break;
            }
            if (DcbxConvertStrToPriArray
                (CliHandle, (UINT1 *) apu4Args[DCBX_ZERO], ai4PriList)
                != CLI_SUCCESS)
            {
                i4RetStatus = CLI_FAILURE;
                break;
            }
            i4RetStatus = ETSCliSetPriToPGMapping (CliHandle, i4IfIndex,
                                                   ai4PriList,
                                                   (UINT4) CLI_PTR_TO_I4
                                                   (apu4Args[DCBX_ONE]));
            break;

        case CLI_ETS_PGID_BW_CONF:
        case CLI_ETS_RECO_PGID_BW_CONF:
            i4IfIndex = CLI_GET_IFINDEX ();
            MEMSET (au4EtsBW, DCBX_ZERO, sizeof (UINT4) * ETS_MAX_TCGID_CONF);
            au4EtsBW[ETS_TCGID0] = (UINT1) CLI_PTR_TO_I4 (apu4Args[ETS_TCGID0]);
            au4EtsBW[ETS_TCGID1] = (UINT1) CLI_PTR_TO_I4 (apu4Args[ETS_TCGID1]);
            au4EtsBW[ETS_TCGID2] = (UINT1) CLI_PTR_TO_I4 (apu4Args[ETS_TCGID2]);
            au4EtsBW[ETS_TCGID3] = (UINT1) CLI_PTR_TO_I4 (apu4Args[ETS_TCGID3]);
            au4EtsBW[ETS_TCGID4] = (UINT1) CLI_PTR_TO_I4 (apu4Args[ETS_TCGID4]);
            au4EtsBW[ETS_TCGID5] = (UINT1) CLI_PTR_TO_I4 (apu4Args[ETS_TCGID5]);
            au4EtsBW[ETS_TCGID6] = (UINT1) CLI_PTR_TO_I4 (apu4Args[ETS_TCGID6]);
            au4EtsBW[ETS_TCGID7] = (UINT1) CLI_PTR_TO_I4 (apu4Args[ETS_TCGID7]);

            u1ConOrReco = ((u4Command ==
                            CLI_ETS_PGID_BW_CONF) ? ETS_CONFIG_TABLE :
                           ETS_RECOMMEND_TABLE);
            i4RetStatus =
                ETSCliSetBwToPg (CliHandle, i4IfIndex, u1ConOrReco, au4EtsBW);
            break;

        case CLI_ETS_TLV_SEL:
            i4IfIndex = CLI_GET_IFINDEX ();
            i4RetStatus = ETSCliSetTlvSelection (CliHandle,
                                                 i4IfIndex, (UINT1)
                                                 CLI_PTR_TO_I4
                                                 (apu4Args[DCBX_ZERO]),
                                                 (UINT1) CLI_PTR_TO_I4
                                                 (apu4Args[DCBX_ONE]),
                                                 (UINT1) CLI_PTR_TO_I4
                                                 (apu4Args[DCBX_TWO]),
                                                 ETS_ENABLED);
            break;

        case CLI_ETS_NO_TLV_SEL:
            i4IfIndex = CLI_GET_IFINDEX ();
            i4RetStatus = ETSCliSetTlvSelection (CliHandle, i4IfIndex, (UINT1)
                                                 CLI_PTR_TO_I4 (apu4Args
                                                                [DCBX_ZERO]),
                                                 (UINT1) CLI_PTR_TO_I4
                                                 (apu4Args[DCBX_ONE]),
                                                 (UINT1) CLI_PTR_TO_I4
                                                 (apu4Args[DCBX_TWO]),
                                                 ETS_DISABLED);
            break;

        case CLI_ETS_CLR_COUNTERS:
            i4RetStatus = ETSCliClearCounters (CliHandle, i4IfIndex);
            break;

            /*ETS SHOW COMMANDS */
        case CLI_SHOW_ETS_GLOB_INFO:
            i4RetStatus = ETSCliShowGbl (CliHandle);
            break;
        case CLI_SHOW_ETS_PORT_INFO:

            i4RetStatus = ETSCliShowPortInfo (CliHandle,
                                              i4IfIndex,
                                              (UINT1)
                                              CLI_PTR_TO_U4 (apu4Args
                                                             [DCBX_ZERO]));
            break;

        case CLI_SHOW_ETS_PORT_COUNTERS:

            i4RetStatus = ETSCliShowPortCounters (CliHandle, i4IfIndex);
            break;

            /* PFC Commands */
            /* PFC Configuration Commands */

        case CLI_PFC_SHUTDOWN:

            i4RetStatus = PFCCliSetControlStatus (CliHandle,
                                                  DCBX_SYS_CNTL_SHUTDOWN);
            break;

        case CLI_PFC_START:

            i4RetStatus = PFCCliSetControlStatus (CliHandle,
                                                  DCBX_SYS_CNTL_START);
            break;

        case CLI_PFC_SYS_STATUS:
            /* apu4Args[0] = enable */
            /* apu4Args[0] = disable */
            if (apu4Args[DCBX_ZERO] != NULL)
            {
                i4RetStatus = PFCCliSetModuleStatus (CliHandle,
                                                     DCBX_SYS_STATUS_ENABLE);
            }
            else
            {
                i4RetStatus = PFCCliSetModuleStatus (CliHandle,
                                                     DCBX_SYS_STATUS_DISABLE);
            }
            break;

        case CLI_PFC_THRESHOLD:

            i4RetStatus = PFCCliSetThreshold (CliHandle,
                                              CLI_PTR_TO_U4
                                              (apu4Args[DCBX_ZERO]),
                                              CLI_PTR_TO_U4
                                              (apu4Args[DCBX_ONE]));
            break;

        case CLI_PFC_ENABLE_NOTIFY:

            i4RetStatus = PFCCliSetTrapStatus (CliHandle,
                                               CLI_PTR_TO_U4 (apu4Args
                                                              [DCBX_ZERO]),
                                               DCBX_TRAP_ENABLE);
            break;

        case CLI_PFC_DISABLE_NOTIFY:

            i4RetStatus = PFCCliSetTrapStatus (CliHandle,
                                               CLI_PTR_TO_U4
                                               (apu4Args[DCBX_ZERO]),
                                               DCBX_TRAP_DISABLE);
            break;

        case CLI_PFC_PORT_STATUS:
            /* apu4Args[DCBX_ZERO] = enable */
            /* apu4Args[1] = disable */

            i4IfIndex = CLI_GET_IFINDEX ();
            if (apu4Args[DCBX_ZERO] != NULL)
            {
                i4RetStatus = PFCCliSetPortStatus (CliHandle, i4IfIndex,
                                                   CREATE_AND_GO);
            }
            else
            {
                i4RetStatus =
                    PFCCliSetPortStatus (CliHandle, i4IfIndex, DESTROY);
            }
            break;

        case CLI_PFC_SET_PORT_MODE:

            i4IfIndex = CLI_GET_IFINDEX ();
            if (apu4Args[DCBX_ZERO] != NULL)
            {
                i4RetStatus = PFCCliSetPortMode (CliHandle, i4IfIndex,
                                                 DCBX_ADM_MODE_AUTO);
            }
            else
            {
                i4RetStatus = PFCCliSetPortMode (CliHandle, i4IfIndex,
                                                 DCBX_ADM_MODE_ON);
            }
            break;
        case CLI_PFC_PORT_MODE_OFF:

            i4IfIndex = CLI_GET_IFINDEX ();
            i4RetStatus = PFCCliSetPortMode (CliHandle, i4IfIndex,
                                             DCBX_ADM_MODE_OFF);
            break;

        case CLI_PFC_WILLING_STATUS:

            i4IfIndex = CLI_GET_IFINDEX ();
            if (apu4Args[DCBX_ZERO] != NULL)
            {
                i4RetStatus = PFCCliSetWillingStatus (CliHandle,
                                                      i4IfIndex, PFC_ENABLED);
            }
            else
            {
                i4RetStatus = PFCCliSetWillingStatus (CliHandle,
                                                      i4IfIndex, PFC_DISABLED);
            }
            break;

        case CLI_PFC_PER_PRI_STATUS:

            i4IfIndex = CLI_GET_IFINDEX ();
            if (DcbxConvertStrToPriArray
                (CliHandle, (UINT1 *) apu4Args[DCBX_ZERO], ai4PriList)
                != CLI_SUCCESS)
            {
                i4RetStatus = CLI_FAILURE;
                break;
            }
            i4RetStatus = PFCCliSetFCPerPriority (CliHandle, i4IfIndex,
                                                  ai4PriList,
                                                  CLI_PTR_TO_I4
                                                  (apu4Args[DCBX_ONE]));
            break;

        case CLI_PFC_TLV_SEL:

            i4IfIndex = CLI_GET_IFINDEX ();
            i4RetStatus = PFCCliSetTlvSelection (CliHandle, i4IfIndex,
                                                 PFC_ENABLED);
            break;

        case CLI_PFC_NO_TLV_SEL:

            i4IfIndex = CLI_GET_IFINDEX ();
            i4RetStatus = PFCCliSetTlvSelection (CliHandle, i4IfIndex,
                                                 PFC_DISABLED);
            break;

        case CLI_PFC_CLR_COUNTERS:
            i4RetStatus = PFCCliClearCounters (CliHandle, i4IfIndex);
            break;
            /* PFC SHOW COMMANDS */
        case CLI_SHOW_PFC_PORT_INFO:

            i4RetStatus = PFCCliShowPortInfo (CliHandle,
                                              i4IfIndex,
                                              (UINT1)
                                              CLI_PTR_TO_U4
                                              (apu4Args[DCBX_ZERO]));
            break;

        case CLI_SHOW_PFC_PORT_COUNTERS:

            i4RetStatus = PFCCliShowPortCounters (CliHandle, i4IfIndex);
            break;

        case CLI_SHOW_PFC_GLOB_CONF:

            i4RetStatus = PFCCliShowGblInfo (CliHandle);
            break;
        case CLI_SHOW_PFC_GLOB_INFO:
            i4RetStatus = PFCCliShowGbl (CliHandle);
            break;
            /*DCBX Commands */
            /*DCBX Config Commands */
        case CLI_SHOW_DCBX_PORT_INFO:
            i4RetStatus = DCBXCliShowPortInfo (CliHandle);
            break;

        case CLI_CTRL_CLR_COUNTERS:
            i4RetStatus = DCBXCliClearCounters (CliHandle, i4IfIndex);
            break;

        case CLI_CEE_ENABLE_NOTIFY:

            i4RetStatus = DcbxCeeCliSetTrapStatus (CliHandle,
                                                   CLI_PTR_TO_U4
                                                   (apu4Args[DCBX_ZERO]),
                                                   DCBX_TRAP_ENABLE);
            break;

        case CLI_CEE_DISABLE_NOTIFY:
            i4RetStatus = DcbxCeeCliSetTrapStatus (CliHandle,
                                                   CLI_PTR_TO_U4
                                                   (apu4Args[DCBX_ZERO]),
                                                   DCBX_TRAP_DISABLE);
            break;

        case CLI_SHOW_DCBX_DET:
            i4RetStatus = DCBXCliShowPortsDetail (CliHandle, i4IfIndex);
            break;

        case CLI_DCBX_MODE:
            i4IfIndex = CLI_GET_IFINDEX ();
            i4RetStatus = DcbxCliSetMode (CliHandle, i4IfIndex,
                                          CLI_PTR_TO_I4 (apu4Args[DCBX_ZERO]));

            break;

        case CLI_DCBX_ADMIN_STATUS:

            i4IfIndex = CLI_GET_IFINDEX ();
            /* apu4Args[DCBX_ZERO] = enable */
            /* apu4Args[DCBX_ZERO] = disable */
            if (apu4Args[DCBX_ZERO] != NULL)
            {
                i4RetStatus = DcbxCliSetStatus (CliHandle, i4IfIndex,
                                                DCBX_SYS_STATUS_ENABLE);
            }
            else
            {
                i4RetStatus = DcbxCliSetStatus (CliHandle, i4IfIndex,
                                                DCBX_SYS_STATUS_DISABLE);
            }
            break;

        case CLI_DCBX_DEBUG:
            i4RetStatus = DCBXCliSetDebugStatus (CliHandle,
                                                 CLI_PTR_TO_I4
                                                 (apu4Args[DCBX_ZERO]),
                                                 DCBX_ENABLED);
            break;
        case CLI_DCBX_NO_DEBUG:
            i4RetStatus = DCBXCliSetDebugStatus (CliHandle,
                                                 CLI_PTR_TO_I4
                                                 (apu4Args[DCBX_ZERO]),
                                                 DCBX_DISABLED);
            break;
            /*Application Prioirty Commands */
        case CLI_APP_PRI_SHUTDOWN:
            i4RetStatus =
                AppPriCliSetControlStatus (CliHandle, DCBX_SYS_CNTL_SHUTDOWN);
            break;
        case CLI_APP_PRI_START:
            i4RetStatus =
                AppPriCliSetControlStatus (CliHandle, DCBX_SYS_CNTL_START);
            break;
        case CLI_APP_PRI_SYS_STATUS:
            /* apu4Args[0] = enable */
            /* apu4Args[1] = disable */
            if (apu4Args[DCBX_ZERO] != NULL)
            {
                i4RetStatus = AppPriCliSetModuleStatus (CliHandle,
                                                        DCBX_SYS_STATUS_ENABLE);
            }
            else
            {
                i4RetStatus = AppPriCliSetModuleStatus (CliHandle,
                                                        DCBX_SYS_STATUS_DISABLE);
            }
            break;
        case CLI_APP_PRI_ENABLE_NOTIFY:

            i4RetStatus = AppPriCliSetTrapStatus (CliHandle,
                                                  CLI_PTR_TO_U4
                                                  (apu4Args[DCBX_ZERO]),
                                                  DCBX_TRAP_ENABLE);
            break;

        case CLI_APP_PRI_DISABLE_NOTIFY:

            i4RetStatus = AppPriCliSetTrapStatus (CliHandle,
                                                  CLI_PTR_TO_U4
                                                  (apu4Args[DCBX_ZERO]),
                                                  DCBX_TRAP_DISABLE);
            break;
        case CLI_APP_PRI_PORT_STATUS:
            /* apu4Args[DCBX_ZERO] = enable */
            /* apu4Args[1] = disable */

            i4IfIndex = CLI_GET_IFINDEX ();
            if (apu4Args[DCBX_ZERO] != NULL)
            {
                i4RetStatus = AppPriCliSetPortStatus (CliHandle, i4IfIndex,
                                                      CREATE_AND_GO);
            }
            else
            {
                i4RetStatus =
                    AppPriCliSetPortStatus (CliHandle, i4IfIndex, DESTROY);
            }
            break;
        case CLI_APP_PRI_SET_PORT_MODE:

            i4IfIndex = CLI_GET_IFINDEX ();
            if (apu4Args[DCBX_ZERO] != NULL)
            {
                i4RetStatus = AppPriCliSetAdminMode (CliHandle, i4IfIndex,
                                                     DCBX_ADM_MODE_AUTO);
            }
            else
            {
                i4RetStatus = AppPriCliSetAdminMode (CliHandle, i4IfIndex,
                                                     DCBX_ADM_MODE_ON);
            }
            break;

        case CLI_APP_PRI_PORT_MODE_OFF:

            i4IfIndex = CLI_GET_IFINDEX ();
            i4RetStatus = AppPriCliSetAdminMode (CliHandle, i4IfIndex,
                                                 DCBX_ADM_MODE_OFF);
            break;

        case CLI_APP_PRI_WILLING_STATUS:

            i4IfIndex = CLI_GET_IFINDEX ();
            if (apu4Args[DCBX_ZERO] != NULL)
            {
                i4RetStatus = AppPriCliSetWillingStatus (CliHandle,
                                                         i4IfIndex,
                                                         APP_PRI_ENABLED);
            }
            else
            {
                i4RetStatus = AppPriCliSetWillingStatus (CliHandle,
                                                         i4IfIndex,
                                                         APP_PRI_DISABLED);
            }
            break;

        case CLI_APP_PRI_MAP_PRI:
            /* apu4Args[0] - selector field
             * apu4Args[1] - protocol id
             * apu4Args[2] - priority */
            i4IfIndex = CLI_GET_IFINDEX ();
            i4RetStatus = AppPriCliSetAppToPriMapping (CliHandle, i4IfIndex,
                                                       (INT4) (*
                                                               (apu4Args
                                                                [DCBX_ZERO])),
                                                       (*
                                                        (apu4Args
                                                         [DCBX_ONE])),
                                                       (*(apu4Args[2])));
            break;
        case CLI_APP_PRI_UNMAP_PRI:
            /*apu4Args[0] - selector field
             * apu4Args[1] - protocol id */
            i4IfIndex = CLI_GET_IFINDEX ();
            i4RetStatus = AppPriCliUnSetAppToPriMapping (CliHandle, i4IfIndex,
                                                         (INT4) (*
                                                                 (apu4Args
                                                                  [DCBX_ZERO])),
                                                         (*
                                                          (apu4Args
                                                           [DCBX_ONE])));
            break;
        case CLI_APP_PRI_TLV_SEL:

            i4IfIndex = CLI_GET_IFINDEX ();
            i4RetStatus = AppPriCliSetTlvSelection (CliHandle, i4IfIndex,
                                                    APP_PRI_ENABLED);
            break;

        case CLI_APP_PRI_NO_TLV_SEL:

            i4IfIndex = CLI_GET_IFINDEX ();
            i4RetStatus = AppPriCliSetTlvSelection (CliHandle, i4IfIndex,
                                                    APP_PRI_DISABLED);
            break;

        case CLI_APP_PRI_CLR_COUNTERS:
            i4RetStatus = AppPriCliClearCounters (CliHandle, i4IfIndex);
            break;

            /*Application Priority SHOW COMMANDS */
        case CLI_SHOW_APP_PRI_GLOB_INFO:
            i4RetStatus = AppPriCliShowGbl (CliHandle);
            break;
        case CLI_SHOW_APP_PRI_PORT_INFO:

            i4RetStatus = AppPriCliShowPortInfo (CliHandle,
                                                 i4IfIndex,
                                                 (UINT1)
                                                 CLI_PTR_TO_U4 (apu4Args
                                                                [DCBX_ZERO]));
            break;

        case CLI_SHOW_APP_PRI_PORT_COUNTERS:

            i4RetStatus = AppPriCliShowPortCounters (CliHandle, i4IfIndex);
            break;
        case CLI_ETS_SET_TSA_TABLE:

            i4IfIndex = CLI_GET_IFINDEX ();
            if (DcbxConvertStrToPriArray
                (CliHandle, (UINT1 *) apu4Args[DCBX_ZERO], ai4TcNoList)
                != CLI_SUCCESS)
            {
                CLI_SET_ERR (CLI_ERR_ETS_INVALID_TCGID);
                i4RetStatus = CLI_FAILURE;
                break;
            }

            if (ETSCliSetTSAPerTC (i4IfIndex,
                                   ai4TcNoList,
                                   CLI_PTR_TO_I4 (apu4Args[DCBX_ONE]),
                                   CLI_ETS_UPDATE_ADM_CON_TABLE) == CLI_FAILURE)
            {
                i4RetStatus = CLI_FAILURE;
                break;
            }
            i4RetStatus = CLI_SUCCESS;
            break;
        case CLI_ETS_SET_RECO_TSA_TABLE:

            i4IfIndex = CLI_GET_IFINDEX ();
            if (DcbxConvertStrToPriArray
                (CliHandle, (UINT1 *) apu4Args[DCBX_ZERO], ai4TcNoList)
                != CLI_SUCCESS)
            {
                CLI_SET_ERR (CLI_ERR_ETS_INVALID_TCGID);
                i4RetStatus = CLI_FAILURE;
                break;
            }
            if (ETSCliSetTSAPerTC (i4IfIndex,
                                   ai4TcNoList,
                                   CLI_PTR_TO_I4 (apu4Args[DCBX_ONE]),
                                   CLI_ETS_UPDATE_ADM_RECO_TABLE) ==
                CLI_FAILURE)
            {
                i4RetStatus = CLI_FAILURE;
                break;
            }
            i4RetStatus = CLI_SUCCESS;
            break;

        default:
            CliPrintf (CliHandle, "\r%% Unknown DCBX command. \r\n");
            break;
    }
    if ((i4RetStatus == CLI_FAILURE)
        && (CLI_GET_ERR (&u4ErrCode) == CLI_SUCCESS))
    {
        if ((u4ErrCode > DCBX_ZERO) && (u4ErrCode < DCBX_CLI_MAX_ERR))
        {
            CliPrintf (CliHandle, "\r%% %s", gDcbxCliErrString[u4ErrCode]);
        }

        CLI_SET_ERR (DCBX_ZERO);
    }
    CLI_SET_CMD_STATUS ((UINT4) i4RetStatus);
    DcbxUnLock ();
    CliUnRegisterLock (CliHandle);
    return (i4RetStatus);
}

/*****************************************************************************
 Function Name      : ETSCliSetControlStatus
 Description        : This function will be used to start/shutdown 
                      the ETS Module on the subsystem 
 Input(s)           : CliHandle   - Handle to CLI session                                          
                    : u1Status - Start/Shutdown status for ETS module.
 Output(s)          : CLI_SET_ERR will be used in case any errors in the Test
                      Routine and 
                     CLI_FATAL_ERROR (CliHanlde) will be used in case of any 
                     errors in the Set Routine.                                               
 Return Value(s)    : CLI_SUCCESS/CLI_FAILURE           
 Called By          : cli_process_dcbx_cmd                         
 Calling Function    : SNMP nmh Routines                         
*****************************************************************************/
PRIVATE INT4
ETSCliSetControlStatus (tCliHandle CliHandle, UINT1 u1Status)
{
    UINT4               u4ErrorCode = SNMP_ERR_NO_ERROR;

    /* Test the Given Vlaue if it success then Set */
    if (nmhTestv2FsETSSystemControl (&u4ErrorCode, u1Status) != SNMP_SUCCESS)
    {
        return (CLI_FAILURE);
    }

    if (nmhSetFsETSSystemControl (u1Status) != SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }

    return (CLI_SUCCESS);

}

/*****************************************************************************
 Function Name      : ETSCliSetModuleStatus
 Description        : This function will be used to Enable/disable 
                      the ETS Module on the subsystem 
 Input(s)           : CliHandle   - Handle to CLI session                
                    : u1Status - enable/disable status for ETS module.
 Output(s)          : CLI_SET_ERR will be used in case any errors in the 
                      Test Routine and 
                      CLI_FATAL_ERROR (CliHanlde) will be used in case of any 
                       errors in the Set Routine.                                  
 Return Value(s)     : CLI_SUCCESS/CLI_FAILURE           
 Called By           : cli_process_dcbx_cmd                         
 Calling Function    : SNMP nmh Routines                         
*****************************************************************************/

PRIVATE INT4
ETSCliSetModuleStatus (tCliHandle CliHandle, UINT1 u1Status)
{
    UINT4               u4ErrorCode = SNMP_ERR_NO_ERROR;

    UNUSED_PARAM (CliHandle);

    /* Test the Given Vlaue if it success then Set */
    if (nmhTestv2FsETSModuleStatus (&u4ErrorCode, u1Status) != SNMP_SUCCESS)
    {
        return (CLI_FAILURE);
    }

    nmhSetFsETSModuleStatus (u1Status);
    return (CLI_SUCCESS);

}

/*****************************************************************************
 Function Name      : ETSCliSetPortStatus
 Description        : This function will be used to enable/disable 
                      the Priority grouping Admin status on the given port.  
 Input(s)           : CliHandle    - Handle to CLI session                
                    : i4IfIndex - Port number for which the Priority grouping
                      admin status to be set.
                    :i1ETSRowStatus -Enabled/Disabled.  
 Output(s)          : CLI_SET_ERR will be used in case any errors in the
                      Test Routine and
                      CLI_FATAL_ERROR (CliHanlde) will be used in case of 
                      any errors in the Set Routine.
 Return Value(s)    : CLI_SUCCESS/CLI_FAILURE           
 Called By          : cli_process_dcbx_cmd                         
 Calling Function   : SNMP nmh Routines                         
*****************************************************************************/

PRIVATE INT4
ETSCliSetPortStatus (tCliHandle CliHandle, INT4 i4IfIndex, UINT1 i1ETSRowStatus)
{
    UINT4               u4ErrorCode = SNMP_ERR_NO_ERROR;

    /* Test the Given Vlaue if it success then Set */
    if (nmhTestv2FsETSRowStatus (&u4ErrorCode, i4IfIndex,
                                 i1ETSRowStatus) != SNMP_SUCCESS)
    {
        return (CLI_FAILURE);
    }

    if (nmhSetFsETSRowStatus (i4IfIndex, i1ETSRowStatus) != SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }
    return (CLI_SUCCESS);

}

/*****************************************************************************
 Function Name      : ETSCliSetPortMode
 Description        : This function will be used to set Priority grouping
                     feature mode (Negotiable or Force-enable) on the
                     given port. 
 Input(s)           : CliHandle    - Handle to CLI session                
                    : i4IfIndex - Port number for which the Admin mode
                    to be set.
                    :u1AdminMode -Auto/On Mode.  
 Output(s)          : CLI_SET_ERR will be used in case any errors in the
                      Test Routine and 
                      CLI_FATAL_ERROR (CliHanlde) will be used in case of
                      any errors in the Set Routine.                            
 Return Value(s)    : CLI_SUCCESS/CLI_FAILURE           
 Called By          : cli_process_dcbx_cmd                         
 Calling Function   : SNMP nmh Routines                         
*****************************************************************************/

PRIVATE INT4
ETSCliSetPortMode (tCliHandle CliHandle, INT4 i4IfIndex, UINT1 u1AdminMode)
{
    UINT4               u4ErrorCode = SNMP_ERR_NO_ERROR;

    UNUSED_PARAM (CliHandle);
    /* Test the Given Vlaue if it success then Set */
    if (nmhTestv2FsETSAdminMode (&u4ErrorCode, i4IfIndex,
                                 u1AdminMode) != SNMP_SUCCESS)
    {
        return (CLI_FAILURE);
    }
    nmhSetFsETSAdminMode (i4IfIndex, u1AdminMode);
    return (CLI_SUCCESS);

}

/*****************************************************************************
 Function Name      : ETSCliSetTrapStatus
 Description        : This function will be used to Enable/disable the ETS
                      Trap based on the Trap type specified.
 Input(s)           : CliHandle    - Handle to CLI session                
                    : u4TrapType - ETS Trap types for which it should be
                      enable/disable.
                    :u1Status - enable/disable status for ETS module.
 Output(s)          : CLI_SET_ERR will be used in case any errors in the
                      Test Routine and
                      CLI_FATAL_ERROR (CliHanlde) will be used in case of 
                      any errors in the Set Routine.                            
 Return Value(s)     : CLI_SUCCESS/CLI_FAILURE
 Called By           : cli_process_dcbx_cmd                         
 Calling Function    : SNMP nmh Routines                         
*****************************************************************************/
PRIVATE INT4
ETSCliSetTrapStatus (tCliHandle CliHandle, UINT4 u4TrapType, UINT1 u1Status)
{
    UINT4               u4ErrorCode = SNMP_ERR_NO_ERROR;
    INT4                i4EtsTrapVal = DCBX_ZERO;

    UNUSED_PARAM (CliHandle);
    nmhGetFsETSGlobalEnableTrap (&i4EtsTrapVal);
    if (u1Status == DCBX_TRAP_ENABLE)
    {
        i4EtsTrapVal = i4EtsTrapVal | (INT4) u4TrapType;
    }
    else
    {
        i4EtsTrapVal = (i4EtsTrapVal) & (INT4) (~u4TrapType);
    }

    /* Test the Given Vlaue if it success then Set */

    if (nmhTestv2FsETSGlobalEnableTrap (&u4ErrorCode, i4EtsTrapVal)
        != SNMP_SUCCESS)
    {
        return (CLI_FAILURE);
    }

    nmhSetFsETSGlobalEnableTrap (i4EtsTrapVal);
    return (CLI_SUCCESS);

}

/****************************************************************************
 Function Name      : ETSCliSetWillingStatus                                
 Description        : This function will be used to set Priority grouping
                      willing status on the given port.     
 Input(s)           : CliHandle    - Handle to CLI session               
                    :i4IfIndex - Port number for which the Willing status
                     to be set.
                    :u1Status - ETS_ENABLED/ETS_DISABLED. 
 Output(s)          : CLI_SET_ERR will be used in case any errors 
                      in the Test Routine and 
                      CLI_FATAL_ERROR (CliHanlde) will be used in case of 
                      any errors in the Set Routine.                            
 Return Value(s)    : CLI_SUCCESS/CLI_FAILURE           
 Called By          : cli_process_dcbx_cmd                         
 Calling Function   : SNMP nmh Routines                         
*****************************************************************************/
PRIVATE INT4
ETSCliSetWillingStatus (tCliHandle CliHandle, INT4 i4IfIndex, UINT1 u1Status)
{
    UINT4               u4ErrorCode = SNMP_ERR_NO_ERROR;

    UNUSED_PARAM (CliHandle);
    /* Test the Given Vlaue if it success then Set */
    if (nmhTestv2LldpXdot1dcbxAdminETSConWilling
        (&u4ErrorCode, i4IfIndex, u1Status) != SNMP_SUCCESS)
    {
        return (CLI_FAILURE);
    }

    nmhSetLldpXdot1dcbxAdminETSConWilling (i4IfIndex, u1Status);
    return (CLI_SUCCESS);

}

/*****************************************************************************
 Function Name      : ETSCliSetPriToPGMapping                             
 Description        : This function will be used to map the priority list 
                      to the Priority group Id on the given port.  
 Input(s)              : CliHandle    - Handle to CLI session      
                    : i4IfIndex    - Interface Index                   
                    : pu4PriList  - Priroity List                          
                    : u4PgId      -Priority Group ID                  
 Output(s)          : CLI_SET_ERR will be used in case any errors in the Test
                      Routine and 
                      CLI_FATAL_ERROR (CliHanlde) will be used in case of any                         
                      errors in the Set Routine.                               
 Return Value(s)    : CLI_SUCCESS/CLI_FAILURE 
 Called By          : cli_process_dcbx_cmd                         
*****************************************************************************/
PRIVATE INT4
ETSCliSetPriToPGMapping (tCliHandle CliHandle, INT4 i4IfIndex,
                         INT4 *pi4PriList, UINT4 u4PgId)
{
    UINT4               u4ErrorCode = SNMP_ERR_NO_ERROR;
    INT4                i4Priority = DCBX_ZERO;
    INT4                i4PriIdx = DCBX_ZERO;

    UNUSED_PARAM (CliHandle);
    for (i4PriIdx = DCBX_ZERO; i4PriIdx < DCBX_MAX_PRIORITIES; i4PriIdx++)
    {
        if (pi4PriList[i4PriIdx] != DCBX_INVALID_ID)
        {
            /* Test the Given Vlaue if it success then Set */
            i4Priority = pi4PriList[i4PriIdx];
            if (nmhTestv2LldpXdot1dcbxAdminETSConPriTrafficClass
                (&u4ErrorCode, i4IfIndex, (UINT4) i4Priority,
                 u4PgId) != SNMP_SUCCESS)
            {
                return (CLI_FAILURE);
            }
        }
    }
    for (i4PriIdx = DCBX_ZERO; i4PriIdx < DCBX_MAX_PRIORITIES; i4PriIdx++)
    {
        if (pi4PriList[i4PriIdx] != DCBX_INVALID_ID)
        {
            i4Priority = pi4PriList[i4PriIdx];

            nmhSetLldpXdot1dcbxAdminETSConPriTrafficClass (i4IfIndex,
                                                           i4Priority, u4PgId);
        }
    }
    return (CLI_SUCCESS);
}

/*****************************************************************************
 Function Name   : ETSCliSetBwToPg   
 Description     : This function is used to configure bandwidth for TCGID
 Input(s)        : CliHandle    - Handle to CLI session                
                 : i4IfIndex    - Interface Index                       
                 :u1Value - Used to indicate the bandwidth configuration 
                  is for ETS configuration table or ETS Recommendation table.
                 : i4BandWidth1 - Bandwidth for Priority Group 1       
                 : i4BandWidth2 - Bandwidth for Priority Group 2       
                 : i4BandWidth3 - Bandwidth for Priority Group 3       
                 : i4BandWidth4 - Bandwidth for Priority Group 4       
                 : i4BandWidth5 - Bandwidth for Priority Group 5       
                 : i4BandWidth6 - Bandwidth for Priority Group 6       
                 : i4BandWidth7 - Bandwidth for Priority Group 7       
                 : i4BandWidth8 - Bandwidth for Priority Group 8       
 Output(s)       : CLI_SET_ERR will be used in case any errors in the Test
                   Routine and 
                   CLI_FATAL_ERROR (CliHanlde) will be used in case of any     
                   errors in the Set Routine. 
 Return Value(s) : CLI_SUCCESS/CLI_FAILURE           
 Called By       : cli_process_dcbx_cmd                       
*****************************************************************************/

PRIVATE INT4
ETSCliSetBwToPg (tCliHandle CliHandle, INT4 i4IfIndex,
                 UINT1 u1Value, UINT4 *pau4EtsBW)
{
    UINT4               u4ErrorCode = SNMP_ERR_NO_ERROR;
    UINT4               au4ConBw[ETS_MAX_TCGID_CONF + DCBX_ONE] = { DCBX_ZERO };
    UINT4               u4Index = DCBX_ZERO;
    UINT4               au4BwBackup[ETS_MAX_TCGID_CONF + DCBX_ONE] =
        { DCBX_ZERO };
    UINT4               u4Sum = DCBX_ZERO;

    if (pau4EtsBW == NULL)
    {
        CliPrintf (CliHandle, "\r%% %s\n\r",
                   "Error: In ETSCliSetBwToPg: NULL Argument Recieved");
        return CLI_FAILURE;
    }

    for (u4Index = DCBX_ZERO; u4Index < ETS_MAX_TCGID_CONF; u4Index++)
    {
        au4ConBw[u4Index] = pau4EtsBW[u4Index];
    }

    /*Setting Bandwidth in ETS Configuration Table */
    if (ETS_CONFIG_TABLE == u1Value)
    {
        for (u4Index = DCBX_ZERO; u4Index < ETS_MAX_TCGID_CONF; u4Index++)
        {
            u4Sum += au4ConBw[u4Index];
        }

        if (u4Sum != ETS_MAX_BW)
        {
            CliPrintf (CliHandle, "\r%% %s - [%d]\n\r",
                       "Error: Total Bandwidth sum has to be equal to 100",
                       u4Sum);
            return CLI_FAILURE;
        }

        /* Before setting we are clearing the Admin BW database
         * and if any error occurs then we are going to revert the changes 
         * to the already set values from au4BwBackup that we have taken before
         * clearing the global Admin Database */
        for (u4Index = 0; u4Index <= ETS_TCGID7; u4Index++)
        {
            nmhGetLldpXdot1dcbxAdminETSConTrafficClassBandwidth
                (i4IfIndex, u4Index, &au4BwBackup[u4Index]);
        }

        for (u4Index = 0; u4Index <= ETS_TCGID7; u4Index++)
        {
            nmhSetLldpXdot1dcbxAdminETSConTrafficClassBandwidth
                (i4IfIndex, u4Index, DCBX_ZERO);
        }

        for (u4Index = 0; u4Index <= ETS_TCGID7; u4Index++)
        {
            if (nmhTestv2LldpXdot1dcbxAdminETSConTrafficClassBandwidth
                (&u4ErrorCode, i4IfIndex, u4Index, au4ConBw[u4Index])
                != SNMP_SUCCESS)
            {
                /* Clearing the database to set the already set value */
                for (u4Index = 0; u4Index <= ETS_TCGID7; u4Index++)
                {
                    nmhSetLldpXdot1dcbxAdminETSConTrafficClassBandwidth
                        (i4IfIndex, u4Index, DCBX_ZERO);
                }
                /* Reverting the database to already set values */
                for (u4Index = 0; u4Index <= ETS_TCGID7; u4Index++)
                {
                    nmhSetLldpXdot1dcbxAdminETSConTrafficClassBandwidth
                        (i4IfIndex, u4Index, au4BwBackup[u4Index]);
                }
                return CLI_FAILURE;
            }
        }

        for (u4Index = 0; u4Index <= ETS_TCGID7; u4Index++)
        {
            if (au4ConBw[u4Index] != DCBX_ZERO)
            {
                nmhSetLldpXdot1dcbxAdminETSConTrafficClassBandwidth
                    (i4IfIndex, u4Index, au4ConBw[u4Index]);
            }
        }
    }                            /*ETS_CONFIG_TABLE */
    else
    {
        /* Setting Bandwidth in ETS Recommendation Table */
        for (u4Index = DCBX_ZERO; u4Index < ETS_MAX_TCGID_CONF; u4Index++)
        {
            u4Sum += au4ConBw[u4Index];
        }

        if (u4Sum != ETS_MAX_BW)
        {
            CliPrintf (CliHandle, "\r%% %s - [%d]\n\r",
                       "Error: Total Recommended Bandwidth sum has to be equal to 100",
                       u4Sum);
            return CLI_FAILURE;
        }

        /* Before setting we are clearing the Admin Recommended BW database
         * and if any error occurs then we are going to revert the changes 
         * to the already set values from au4BwBackup that we have taken before
         * clearing the global Admin Recommended Database */
        for (u4Index = 0; u4Index <= ETS_TCGID7; u4Index++)
        {
            nmhGetLldpXdot1dcbxAdminETSRecoTrafficClassBandwidth
                (i4IfIndex, u4Index, &au4BwBackup[u4Index]);
        }

        for (u4Index = 0; u4Index <= ETS_TCGID7; u4Index++)
        {
            nmhSetLldpXdot1dcbxAdminETSRecoTrafficClassBandwidth
                (i4IfIndex, u4Index, DCBX_ZERO);
        }

        for (u4Index = 0; u4Index <= ETS_TCGID7; u4Index++)
        {
            if (nmhTestv2LldpXdot1dcbxAdminETSRecoTrafficClassBandwidth
                (&u4ErrorCode, i4IfIndex, u4Index,
                 au4ConBw[u4Index]) != SNMP_SUCCESS)
            {
                /* Clearing the database to set the already set value */
                for (u4Index = 0; u4Index <= ETS_TCGID7; u4Index++)
                {
                    nmhSetLldpXdot1dcbxAdminETSRecoTrafficClassBandwidth
                        (i4IfIndex, u4Index, DCBX_ZERO);
                }

                /* Reverting the database to already set values */
                for (u4Index = 0; u4Index <= ETS_TCGID7; u4Index++)
                {
                    nmhSetLldpXdot1dcbxAdminETSRecoTrafficClassBandwidth
                        (i4IfIndex, u4Index, au4BwBackup[u4Index]);
                }
                return CLI_FAILURE;
            }
        }
        for (u4Index = 0; u4Index <= ETS_TCGID7; u4Index++)
        {
            if (au4ConBw[u4Index] != DCBX_ZERO)
            {
                nmhSetLldpXdot1dcbxAdminETSRecoTrafficClassBandwidth
                    (i4IfIndex, u4Index, au4ConBw[u4Index]);
            }
        }
    }
    return (CLI_SUCCESS);
}

/*****************************************************************************
 Function Name    : ETSCliSetTlvSelection                             
 Description      : This function will be used to enable/disable the 
                      transmission status of ETS TLV
 Input(s)         : CliHandle - CLI Session Handler.
                  :i4IfIndex - Port number for which the ETS TLV Transmission
                   to be set.
                  :u1ETSConfTlv - If the value is 1,
                   we need to configure the Transmission status of ETS
                   configuration TLV based on the value of u1Status.
                  :u1ETSRecoTlv - If the value is 1,
                   we need to configure the Transmission status of ETS
                   Recommendation TLV based on the value of u1Status.
                  :u1ETSTcSuppTlv - If the value is 1,
                     we need to configure the Transmission status of ETS
                    TC Supported TLV based on the value of u1Status.
                  :u1Status - Enable/Disable status for ETS TLV Transmission.
 Output(s)        : CLI_SET_ERR will be used in case any errors in the Test
                    Routine and
                    CLI_FATAL_ERROR (CliHanlde) will be used in case of any
                    errors in the Set Routine.                                  
 Return Value(s) : CLI_SUCCESS/CLI_FAILURE           
 Called By          : cli_process_dcbx_cmd                         
*****************************************************************************/

PRIVATE INT4
ETSCliSetTlvSelection (tCliHandle CliHandle, INT4 i4IfIndex,
                       UINT1 u1ETSConfTlv, UINT1 u1ETSRecoTlv,
                       UINT1 u1ETSTcSuppTlv, UINT1 u1Status)
{
    UINT4               u4ErrorCode = SNMP_ERR_NO_ERROR;

    UNUSED_PARAM (CliHandle);
    /*Enabling Configuration TLV Tranfer Status */
    if (u1ETSConfTlv != ETS_DISABLED)
    {
        if (nmhTestv2LldpXdot1dcbxConfigETSConfigurationTxEnable
            (&u4ErrorCode, i4IfIndex, DCBX_DEST_ADDR_INDEX,
             u1Status) != SNMP_SUCCESS)
        {
            return (CLI_FAILURE);
        }
        nmhSetLldpXdot1dcbxConfigETSConfigurationTxEnable
            (i4IfIndex, DCBX_DEST_ADDR_INDEX, u1Status);
    }

    /*Enabling Recommendation TLV Tranfer Status */
    if (u1ETSRecoTlv != ETS_DISABLED)
    {
        if (nmhTestv2LldpXdot1dcbxConfigETSRecommendationTxEnable
            (&u4ErrorCode, i4IfIndex, DCBX_DEST_ADDR_INDEX,
             u1Status) != SNMP_SUCCESS)
        {
            return (CLI_FAILURE);
        }
        nmhSetLldpXdot1dcbxConfigETSRecommendationTxEnable
            (i4IfIndex, DCBX_DEST_ADDR_INDEX, u1Status);
    }

    /*Enabling Traffic Class Supported TLV Tranfer Status */

    if (u1ETSTcSuppTlv != ETS_DISABLED)
    {

        if (nmhTestv2FslldpXdot1dcbxConfigTCSupportedTxEnable
            (&u4ErrorCode, i4IfIndex, DCBX_DEST_ADDR_INDEX,
             u1Status) != SNMP_SUCCESS)
        {
            return (CLI_FAILURE);
        }

        nmhSetFslldpXdot1dcbxConfigTCSupportedTxEnable
            (i4IfIndex, DCBX_DEST_ADDR_INDEX, u1Status);
    }

    return (CLI_SUCCESS);

}

/*****************************************************************************
 Function Name      : ETSCliClearCounters                             
 Description        :This function will be used to Clear the ETS Counters for
                     the specified port or for all the ports.
 Input(s)              : CliHandle - Handle to CLI session      
                       : i4IfIndex - Port number for which the ETS counters
                      to be cleared.
                       : u1Value - To mention whether to clear for specific port
                      or to clear for all ports.
 Output(s)          : CLI_SET_ERR will be used in case any errors in the Test
                      Routine and 
                      CLI_FATAL_ERROR (CliHanlde) will be used in case of any                          
                      errors in the Set Routine.                                                                           
 Return Value(s)    : CLI_SUCCESS/CLI_FAILURE           
 Called By          : cli_process_dcbx_cmd                         
*****************************************************************************/

PRIVATE INT4
ETSCliClearCounters (tCliHandle CliHandle, INT4 i4IfIndex)
{
    UINT4               u4ErrorCode = SNMP_ERR_NO_ERROR;

    UNUSED_PARAM (CliHandle);
    /* Clearing the ETS counter for all ports */
    if (i4IfIndex == DCBX_ZERO)
    {
        if (nmhTestv2FsETSClearCounters
            (&u4ErrorCode, ETS_ENABLED) != SNMP_SUCCESS)
        {
            return (CLI_FAILURE);
        }

        nmhSetFsETSClearCounters (ETS_ENABLED);
    }
    else
    {
        /* clearing the ETS counters per port */
        if (nmhTestv2FsETSClearTLVCounters (&u4ErrorCode, i4IfIndex,
                                            ETS_ENABLED) != SNMP_SUCCESS)
        {
            return (CLI_FAILURE);
        }

        nmhSetFsETSClearTLVCounters (i4IfIndex, ETS_ENABLED);
    }
    return (CLI_SUCCESS);

}

/*****************************************************************************
 Function Name       : ETSCliShowPortCounters                             
 Description         :This function will be used to Show the ETS counters for
                       ports            
 Input(s)               : CliHandle - Handle to CLI session      
                     : i4IfIndex - Port number for which the ETS counter 
                       information to be shown.
 Output(s)           : CLI_SET_ERR will be used in case any errors in the Test
                       Routine and 
                       CLI_FATAL_ERROR (CliHanlde) will be used in case of 
                       any errors in the Set Routine.
 Return Value(s)    : CLI_SUCCESS/CLI_FAILURE           
 Called By          : cli_process_dcbx_cmd                         
*****************************************************************************/
PRIVATE INT4
ETSCliShowPortCounters (tCliHandle CliHandle, INT4 i4IfIndex)
{
    INT4                i4Port = DCBX_ZERO;
    INT4                i4NextPort = DCBX_ZERO;

    CliPrintf (CliHandle, "\n\r");
    CliPrintf (CliHandle, "ETS  TLV Counter Information\r\n");
    CliPrintf (CliHandle, "----------------------------------------\r\n");

    if (i4IfIndex == DCBX_ZERO)
    {
        if (nmhGetFirstIndexFsETSPortTable (&i4NextPort) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\rNo ETS Port is created\n");
            CliPrintf (CliHandle,
                       "\r----------------------------------------\n");
            CliPrintf (CliHandle, "\r\n");
            return CLI_SUCCESS;
        }
        CliPrintf (CliHandle, "\r%-9s%-20s%-20s%-20s\n\r",
                   "Port", "Configuration TLV", "Recommended TLV",
                   "TC Supported TLV");
        CliPrintf (CliHandle, "\r%-9s%-6s%-6s%-8s%-6s%-6s%-8s%-6s%-6s%-6s\n\r",
                   "", "Tx", "Rx", "Error", "Tx", "Rx", "Error", "Tx", "Rx",
                   "Error");
        CliPrintf (CliHandle, "\n\r");
        do
        {
            i4Port = i4NextPort;
            ETSCliShowPortCountersInfo (CliHandle, i4Port);
        }
        while ((nmhGetNextIndexFsETSPortTable (i4Port, &i4NextPort)
                == SNMP_SUCCESS));
    }
    else
    {
        CliPrintf (CliHandle, "\r%-9s%-20s%-20s%-20s\n\r",
                   "Port", "Configuration TLV", "Recommended TLV",
                   "TC Supported TLV");
        CliPrintf (CliHandle, "\r%-9s%-6s%-6s%-8s%-6s%-6s%-8s%-6s%-6s%-6s\n\r",
                   "", "Tx", "Rx", "Error", "Tx", "Rx", "Error", "Tx", "Rx",
                   "Error");
        CliPrintf (CliHandle, "\n\r");
        ETSCliShowPortCountersInfo (CliHandle, i4IfIndex);
    }
    CliPrintf (CliHandle, "\r----------------------------------------\n");
    CliPrintf (CliHandle, "\n\r");
    return CLI_SUCCESS;
}

/*****************************************************************************
 Function Name       : ETSCliShowPortCountersInfo                             
 Description         :This function will be used to Show the ETS counters for
                      the specified port            
 Input(s)               : CliHandle - Handle to CLI session      
                     : i4IfIndex - Port number for which the ETS counter 
                       information to be shown.
 Output(s)           : CLI_SET_ERR will be used in case any errors in the Test
                       Routine and 
                       CLI_FATAL_ERROR (CliHanlde) will be used in case of 
                       any errors in the Set Routine.
 Return Value(s)    : CLI_SUCCESS/CLI_FAILURE           
 Called By          : cli_process_dcbx_cmd                         
*****************************************************************************/
PRIVATE INT4
ETSCliShowPortCountersInfo (tCliHandle CliHandle, INT4 i4IfIndex)
{
    UINT4               u4ConfRxTLVCounter = DCBX_ZERO;
    UINT4               u4RecoRxTLVCounter = DCBX_ZERO;
    UINT4               u4TcSuppRxTLVCounter = DCBX_ZERO;
    UINT4               u4ConfTxTLVCounter = DCBX_ZERO;
    UINT4               u4RecoTxTLVCounter = DCBX_ZERO;
    UINT4               u4TcSuppTxTLVCounter = DCBX_ZERO;
    UINT4               u4ConfErrorTLVCounter = DCBX_ZERO;
    UINT4               u4RecoErrorTLVCounter = DCBX_ZERO;
    UINT4               u4TcSuppErrorTLVCounter = DCBX_ZERO;
    UINT1               au1NameStr[CFA_MAX_PORT_NAME_LENGTH] = { DCBX_ZERO };

    CfaCliGetIfName ((UINT4) i4IfIndex, (INT1 *) au1NameStr);

    /* to get Rx Counters */
    nmhGetFsETSConfRxTLVCounter (i4IfIndex, &u4ConfRxTLVCounter);
    nmhGetFsETSRecoRxTLVCounter (i4IfIndex, &u4RecoRxTLVCounter);
    nmhGetFsETSTcSuppRxTLVCounter (i4IfIndex, &u4TcSuppRxTLVCounter);

    /* to get Tx Counters */
    nmhGetFsETSConfTxTLVCounter (i4IfIndex, &u4ConfTxTLVCounter);
    nmhGetFsETSRecoTxTLVCounter (i4IfIndex, &u4RecoTxTLVCounter);
    nmhGetFsETSTcSuppTxTLVCounter (i4IfIndex, &u4TcSuppTxTLVCounter);

    /* to get Rx Error */
    nmhGetFsETSConfRxTLVErrors (i4IfIndex, &u4ConfErrorTLVCounter);
    nmhGetFsETSRecoRxTLVErrors (i4IfIndex, &u4RecoErrorTLVCounter);
    nmhGetFsETSTcSuppRxTLVErrors (i4IfIndex, &u4TcSuppErrorTLVCounter);
    CliPrintf (CliHandle, "\r%-9s%-6d%-6d%-8d%-6d%-6d%-8d%-6d%-6d%-8d\n\r",
               au1NameStr, u4ConfTxTLVCounter, u4ConfRxTLVCounter,
               u4ConfErrorTLVCounter, u4RecoTxTLVCounter,
               u4RecoRxTLVCounter, u4RecoErrorTLVCounter,
               u4TcSuppTxTLVCounter, u4TcSuppRxTLVCounter,
               u4TcSuppErrorTLVCounter);
    CliPrintf (CliHandle, "\n\r");

    return CLI_SUCCESS;
}

/*****************************************************************************
 Function Name      : ETSCliShowPortInfo                             
 Description        : This function is used to display the             
                           ETS Port status                              
 Input(s)           : CliHandle    - Handle to CLI session               
                    : i4IfIndex    - Interface Index                     
 Output(s)          : None.                                              

 Return Value(s)    : CLI_SUCCESS/CLI_FAILURE                         
 Called By          : cli_process_dcbx_cmd                                 
 Calling Function   : SNMP nmh Routines                               
*****************************************************************************/

PRIVATE INT4
ETSCliShowPortInfo (tCliHandle CliHandle, INT4 i4IfIndex, UINT1 u1Detail)
{
    INT4                i4Port = DCBX_ZERO;
    INT4                i4NextPort = DCBX_ZERO;
    INT4                i4OperVersion = DCBX_VER_UNKNOWN;

    CliPrintf (CliHandle, "\n\r");
    if (i4IfIndex == DCBX_ZERO)
    {
        if (nmhGetFirstIndexFsETSPortTable (&i4NextPort) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r No ETS Port is Created\n");
            CliPrintf (CliHandle, "\n\r");
            return CLI_SUCCESS;
        }
        if (u1Detail != ETS_ENABLED)
        {
            CliPrintf (CliHandle, "\r%-9s %-9s\n", "Port", "Status");
            CliPrintf (CliHandle,
                       "\r-----------------------------------------------\n");
        }
        do
        {
            i4Port = i4NextPort;

            nmhGetFsDcbOperVersion (i4Port, &i4OperVersion);
            if (i4OperVersion == DCBX_MODE_CEE)
            {
                ETSCliShowCEEPortDetails (CliHandle, i4Port, u1Detail);
            }
            else
            {
                ETSCliShowPortDetails (CliHandle, i4Port, u1Detail);
            }

            if (u1Detail == ETS_ENABLED)
            {
                CliPrintf (CliHandle,
                           "\r-------------------------------------"
                           "----------\n");
                CliPrintf (CliHandle, "\n\r");
            }
        }
        while ((nmhGetNextIndexFsETSPortTable (i4Port, &i4NextPort)
                == SNMP_SUCCESS));
    }
    else
    {
        if (u1Detail != ETS_ENABLED)
        {
            CliPrintf (CliHandle, "\r%-9s %-9s\n", "Port", "Status");
            CliPrintf (CliHandle,
                       "\r-----------------------------------------------\n");
            CliPrintf (CliHandle, "\n\r");
        }

        nmhGetFsDcbOperVersion (i4IfIndex, &i4OperVersion);
        if (i4OperVersion == DCBX_MODE_CEE)
        {
            ETSCliShowCEEPortDetails (CliHandle, i4IfIndex, u1Detail);
        }
        else
        {
            ETSCliShowPortDetails (CliHandle, i4IfIndex, u1Detail);
        }
    }
    return CLI_SUCCESS;
}

/**************************************************************************
 Function Name      : ETSCliShowPortDetails                             
 Description        : This function is used to display the             
                           ETS Port status                              
 Input(s)           : CliHandle    - Handle to CLI session               
                    : i4IfIndex    - Interface Index                     
 Output(s)          : None.                                              

 Return Value(s)    : CLI_SUCCESS/CLI_FAILURE                         
 Called By          : cli_process_dcbx_cmd                                 
 Calling Function   : SNMP nmh Routines                               
**************************************************************************/

PRIVATE INT4
ETSCliShowPortDetails (tCliHandle CliHandle, INT4 i4IfIndex, UINT1 u1Detail)
{

    INT4                i4EtsRowStatus = DCBX_ZERO;
    UINT1               au1NameStr[CFA_MAX_PORT_NAME_LENGTH] = { DCBX_ZERO };
    INT4                i4ETSAdminMode = DCBX_ZERO;
    INT4                i4ETSOperState = DCBX_ZERO;
    INT4                i4ETSStateType = DCBX_ZERO;
    INT4                i4ETSTxStatus = DCBX_ZERO;
    INT4                i4ETSDcbxStatus = DCBX_ZERO;

    CfaCliGetIfName ((UINT4) i4IfIndex, (INT1 *) au1NameStr);
    /*CliPrintf (CliHandle,"\r Port     : %-9s \n",au1NameStr); */
    nmhGetFsETSRowStatus (i4IfIndex, &i4EtsRowStatus);

    if (u1Detail == DCBX_ENABLED)
    {
        if (i4EtsRowStatus == ACTIVE)
        {
            CliPrintf (CliHandle,
                       "\r-----------------------------------------------\n");
            CliPrintf (CliHandle,
                       "\r ETS Port %-9s Information \n", au1NameStr);
            /*            CliPrintf (CliHandle,
               "\r-----------------------------------------------\n");  */
            /*CliPrintf (CliHandle, "ETS is   :%-8s", "Enabled"); */
            CliPrintf (CliHandle,
                       "\r-----------------------------------------------\n");
            CliPrintf (CliHandle, "\r ETS Local Port Info \n");
            CliPrintf (CliHandle,
                       "\r----------------------------------------------------------\n");
            ETSCliShowLocalPortInfo (CliHandle, i4IfIndex);
            CliPrintf (CliHandle,
                       "\r-----------------------------------------------\n");
            CliPrintf (CliHandle, "\r ETS Admin Port Info \n");
            CliPrintf (CliHandle,
                       "\r---------------------------------------------------------\n");
            ETSCliShowAdminPortInfo (CliHandle, i4IfIndex);
            CliPrintf (CliHandle,
                       "\r-----------------------------------------------\n");
            CliPrintf (CliHandle, "\r ETS Remote Port Info \n");
            CliPrintf (CliHandle,
                       "\r---------------------------------------------------------\n");
            ETSCliShowRemPortInfo (CliHandle, i4IfIndex);
            CliPrintf (CliHandle,
                       "\r-----------------------------------------------\n");

            CliPrintf (CliHandle, "\r ETS Port Related Info \n");
            CliPrintf (CliHandle,
                       "\r-----------------------------------------------\n");
            nmhGetLldpXdot1dcbxConfigETSConfigurationTxEnable
                (i4IfIndex, DCBX_DEST_ADDR_INDEX, &i4ETSTxStatus);
            CliPrintf (CliHandle, "\r%-35s:", "ETS Conf TLV Tx and Rx Status");
            if (i4ETSTxStatus == DCBX_ENABLED)
            {
                CliPrintf (CliHandle, "Enabled\n");

            }
            else
            {
                CliPrintf (CliHandle, "Disabled\n");
            }
            nmhGetLldpXdot1dcbxConfigETSRecommendationTxEnable
                (i4IfIndex, DCBX_DEST_ADDR_INDEX, &i4ETSTxStatus);
            CliPrintf (CliHandle, "\r%-35s:", "ETS Reco TLV Tx and Rx Status");
            if (i4ETSTxStatus == DCBX_ENABLED)
            {
                CliPrintf (CliHandle, "Enabled\n");

            }
            else
            {
                CliPrintf (CliHandle, "Disabled\n");
            }
            nmhGetFslldpXdot1dcbxConfigTCSupportedTxEnable (i4IfIndex,
                                                            DCBX_DEST_ADDR_INDEX,
                                                            &i4ETSTxStatus);
            CliPrintf (CliHandle, "\r%-35s:",
                       "ETS TC Supp TLV Tx and Rx Status");
            if (i4ETSTxStatus == DCBX_ENABLED)
            {
                CliPrintf (CliHandle, "Enabled\n");

            }
            else
            {
                CliPrintf (CliHandle, "Disabled\n");
            }
            CliPrintf (CliHandle, "\n\r");

            nmhGetFsETSDcbxStatus (i4IfIndex, &i4ETSDcbxStatus);
            if (i4ETSDcbxStatus > DCBX_ZERO)
            {
                CliPrintf (CliHandle, "\r%-35s:%s\n", "ETS DCBX Status",
                           au1DcbxStatus[i4ETSDcbxStatus - 1]);
            }

            CliPrintf (CliHandle, "\n\r");

            nmhGetFsETSAdminMode (i4IfIndex, &i4ETSAdminMode);
            nmhGetFsETSDcbxOperState (i4IfIndex, &i4ETSOperState);
            nmhGetFsETSDcbxStateMachine (i4IfIndex, &i4ETSStateType);

            CliPrintf (CliHandle, "\r%-35s:", "ETS Port Mode");
            if (i4ETSAdminMode == DCBX_ADM_MODE_AUTO)
            {
                CliPrintf (CliHandle, "AUTO MODE\n");
            }
            if (i4ETSAdminMode == DCBX_ADM_MODE_ON)
            {
                CliPrintf (CliHandle, "ON MODE\n");
            }
            if (i4ETSAdminMode == DCBX_ADM_MODE_OFF)
            {
                CliPrintf (CliHandle, "OFF MODE\n");
            }
            CliPrintf (CliHandle, "\r%-35s:", "ETS Oper State");
            if (i4ETSOperState == DCBX_OPER_OFF)
            {
                CliPrintf (CliHandle, "OFF STATE\n");
            }
            if (i4ETSOperState == DCBX_OPER_INIT)
            {
                CliPrintf (CliHandle, "INIT STATE\n");
            }
            if (i4ETSOperState == DCBX_OPER_RECO)
            {
                CliPrintf (CliHandle, "RECO STATE\n");
            }
            CliPrintf (CliHandle, "\r%-35s:", "ETS State Machine Type");
            if (i4ETSStateType == DCBX_ASYM_TYPE)
            {
                CliPrintf (CliHandle, "Asymetric\n");
                CliPrintf (CliHandle,
                           "\r-----------------------------------------------\n");
                CliPrintf (CliHandle, "\n\r");
            }
            if (i4ETSStateType == DCBX_SYM_TYPE)
            {
                CliPrintf (CliHandle, "Symmetric\n");
                CliPrintf (CliHandle,
                           "\r-----------------------------------------------\n");
                CliPrintf (CliHandle, "\n\r");
            }
            if (i4ETSStateType == DCBX_FEAT_TYPE)
            {
                CliPrintf (CliHandle, "Feature\n");
                CliPrintf (CliHandle,
                           "\r-----------------------------------------------\n");
                CliPrintf (CliHandle, "\n\r");
            }
        }
        else
        {
            CliPrintf (CliHandle, "\r%-9s %-9s\n", "Port", "Status");
            CliPrintf (CliHandle,
                       "\r-----------------------------------------------\n");
            CliPrintf (CliHandle, "\r\n%-9s %-8s\r\n", au1NameStr, "Disabled");
            CliPrintf (CliHandle,
                       "\r-----------------------------------------------\n");
            CliPrintf (CliHandle, "\n\r");
        }
    }
    else
    {
        if (i4EtsRowStatus == ACTIVE)
        {
            CliPrintf (CliHandle, "\r%-9s %-8s\r\n", au1NameStr, "Enabled");
            CliPrintf (CliHandle,
                       "\r-----------------------------------------------\n");
            CliPrintf (CliHandle, "\n\r");
        }
        else
        {
            CliPrintf (CliHandle, "\r%-9s %-8s\r\n", au1NameStr, "Disabled");
            CliPrintf (CliHandle,
                       "\r-----------------------------------------------\n");
            CliPrintf (CliHandle, "\n\r");
            /*CliPrintf (CliHandle, "ETS is    :%-8s", "Disabled"); */
        }
    }
    CliPrintf (CliHandle, "\n\r");
    return CLI_SUCCESS;
}

/*****************************************************************************
* Function Name     : ETSCliShowLocalPortInfo                                  
* Description       : This function is used to display the ETS Local Port status                                 
* Input(s)          : CliHandle    - Handle to CLI session     
*                   : pETSPortEntry  - pointer to ETS port Entry  
                    : i4IfIndex -     Interface Index  
* Output(s)         : None.                                               
* Global Variables                                                         
* Referred          : None.                                               
* Global Variables                                                          
* Modified          : None.                                                
* Return Value(s)   : CLI_SUCCESS/CLI_FAILURE           
* Called By         : PFCCliShowPortInfo                                                
*****************************************************************************/
PRIVATE INT4
ETSCliShowLocalPortInfo (tCliHandle CliHandle, INT4 i4IfIndex)
{

    INT4                i4TgIdx = DCBX_ZERO;
    UINT4               u4PriIdx = DCBX_ZERO;
    INT4                i4ETSLocWilling = DCBX_ZERO;
    UINT4               u4ETSLocTCGID = DCBX_ZERO;
    INT4                i4ETSTcSup = DCBX_ZERO;
    UINT4               u4EtsConBw = DCBX_ZERO;
    UINT4               u4EtsRecoBw = DCBX_ZERO;
    INT4                i4EtsConTsa = DCBX_ZERO;
    INT4                i4EtsRecoTsa = DCBX_ZERO;
    INT4                i4ETSLocCBS = DCBX_ZERO;

    CliPrintf (CliHandle,
               "\r TGID   Bandwidth RecomBandwidth "
               " TSA  RecomTSA  Priority\n");
    CliPrintf (CliHandle,
               "\r----------------------------------------------------------\n");

    for (i4TgIdx = DCBX_ZERO; i4TgIdx <= ETS_MAX_TCGID_CONF; i4TgIdx++)
    {

        if (i4TgIdx < ETS_MAX_TCGID_CONF)
        {
            nmhGetLldpXdot1dcbxLocETSConTrafficClassBandwidth (i4IfIndex,
                                                               (UINT4) i4TgIdx,
                                                               &u4EtsConBw);
            nmhGetLldpXdot1dcbxLocETSRecoTrafficClassBandwidth (i4IfIndex,
                                                                (UINT4) i4TgIdx,
                                                                &u4EtsRecoBw);

            nmhGetLldpXdot1dcbxLocETSConTrafficSelectionAlgorithm (i4IfIndex,
                                                                   (UINT4)
                                                                   i4TgIdx,
                                                                   &i4EtsConTsa);
            nmhGetLldpXdot1dcbxLocETSRecoTrafficSelectionAlgorithm (i4IfIndex,
                                                                    (UINT4)
                                                                    i4TgIdx,
                                                                    &i4EtsRecoTsa);
        }
        if (i4TgIdx == ETS_MAX_TCGID_CONF)
        {
            i4TgIdx = ETS_DEF_TCGID;
        }
        if (i4TgIdx == DCBX_ZERO)
        {
            CliPrintf (CliHandle, "%6d", i4TgIdx);
        }
        else
        {
            CliPrintf (CliHandle, "%6d", i4TgIdx);
        }

        if (i4TgIdx < ETS_MAX_TCGID_CONF)
        {
            CliPrintf (CliHandle, "%8d%%", u4EtsConBw);
        }
        else
        {
            CliPrintf (CliHandle, "       -");
        }
        CliPrintf (CliHandle, "    ");
        if (i4TgIdx < ETS_MAX_TCGID_CONF)
        {
            CliPrintf (CliHandle, "%6d%%", u4EtsRecoBw);
        }
        else
        {
            CliPrintf (CliHandle, "      -");
        }

        CliPrintf (CliHandle, "        ");
        if (i4TgIdx < ETS_MAX_TCGID_CONF)
        {
            if (i4EtsConTsa == DCBX_STRICT_PRIORITY_ALGO)
            {
                CliPrintf (CliHandle, "%-5s", "SPA");
            }
            else if (i4EtsConTsa == DCBX_CREDIT_BASED_SHAPER_ALGO)
            {
                CliPrintf (CliHandle, "%-5s", "CBSA");
            }
            else if (i4EtsConTsa == DCBX_ENHANCED_TRANSMISSION_SELECTION_ALGO)
            {
                CliPrintf (CliHandle, "%-5s", "ETSA");
            }
            else if (i4EtsConTsa == DCBX_VENDOR_SPECIFIC_ALGO)
            {
                CliPrintf (CliHandle, "%-5s", "VSA");
            }
            CliPrintf (CliHandle, "   ");
        }
        else
        {
            CliPrintf (CliHandle, "  -");
        }

        if (i4TgIdx < ETS_MAX_TCGID_CONF)
        {
            if (i4EtsRecoTsa == DCBX_STRICT_PRIORITY_ALGO)
            {
                CliPrintf (CliHandle, "%-5s", "SPA");
            }
            else if (i4EtsRecoTsa == DCBX_CREDIT_BASED_SHAPER_ALGO)
            {
                CliPrintf (CliHandle, "%-5s", "CBSA");
            }
            else if (i4EtsRecoTsa == DCBX_ENHANCED_TRANSMISSION_SELECTION_ALGO)
            {
                CliPrintf (CliHandle, "%-5s", "ETSA");
            }
            else if (i4EtsRecoTsa == DCBX_VENDOR_SPECIFIC_ALGO)
            {
                CliPrintf (CliHandle, "%-5s", "VSA");
            }
        }
        else
        {
            CliPrintf (CliHandle, "       -   ");
        }
        CliPrintf (CliHandle, "  ");
        for (u4PriIdx = DCBX_ZERO; u4PriIdx < DCBX_MAX_PRIORITIES; u4PriIdx++)
        {
            nmhGetLldpXdot1dcbxLocETSConPriTrafficClass (i4IfIndex, u4PriIdx,
                                                         &u4ETSLocTCGID);
            if ((INT4) u4ETSLocTCGID == i4TgIdx)
            {
                CliPrintf (CliHandle, "%d ", u4PriIdx);
            }
        }
        CliPrintf (CliHandle, "\n\r");
    }
    CliPrintf (CliHandle, "\n\r");

    nmhGetLldpXdot1dcbxLocETSConTrafficClassesSupported (i4IfIndex,
                                                         (UINT4 *) &i4ETSTcSup);
    if (i4ETSTcSup == DCBX_ZERO)
    {
        i4ETSTcSup = ETS_TCGID8;
    }
    CliPrintf (CliHandle, "\r%-25s:%d\n",
               "Number of Traffic Class", i4ETSTcSup);
    nmhGetLldpXdot1dcbxLocETSConWilling (i4IfIndex, &i4ETSLocWilling);
    if (i4ETSLocWilling == ETS_ENABLED)
    {
        CliPrintf (CliHandle, "\r%-25s:%-8s\n", "Willing Status", "Enabled");
    }
    else
    {
        CliPrintf (CliHandle, "\r%-25s:%-8s\n", "Willing Status", "Disabled");
    }

    nmhGetLldpXdot1dcbxLocETSConCreditBasedShaperSupport (i4IfIndex,
                                                          &i4ETSLocCBS);
    if (i4ETSLocCBS == ETS_CBS_SUPPORTED)
    {
        CliPrintf
            (CliHandle, "\r%-25s:%-8s\n", "Credit Based Shaper Algorithm",
             "Supported");
    }
    else
    {
        CliPrintf
            (CliHandle, "\r%-25s:%-8s\n", "Credit Based Shaper Algorithm",
             "Not Supported");
    }

    CliPrintf (CliHandle, "\n\r");
    CliPrintf (CliHandle, "%s",
               "Terminology Used in Above Table:\r\n"
               "--------------------------------\r\n"
               "SPA  := Strict Priority Algorithm\r\n"
               "CBSA := Credit Based Shaper Algorithm\n\r"
               "ETSA := Enhanced Transmission Slection Algorithm\r\n"
               "VSA  := Vendor Specific Algorithm\r\n");

    CliPrintf (CliHandle, "\n\r");
    return (CLI_SUCCESS);
}

/*****************************************************************************
* Function Name     : ETSCliShowAdminPortInfo                                  
* Description       : This function is used to display the ETS Admin Port status                                 
* Input(s)          : CliHandle    - Handle to CLI session     
*                   : pETSPortEntry    - pointer to ETS port Entry  
                    : i4IfIndex -     Interface Index  
* Output(s)         : None.                                               
* Global Variables                                                         
* Referred          : None.                                               
* Global Variables                                                          
* Modified          : None.                                                
* Return Value(s)   : CLI_SUCCESS/CLI_FAILURE           
* Called By         : PFCCliShowPortInfo                                                
*****************************************************************************/
PRIVATE INT4
ETSCliShowAdminPortInfo (tCliHandle CliHandle, INT4 i4IfIndex)
{
    INT4                i4TgIdx = DCBX_ZERO;
    UINT4               u4PriIdx = DCBX_ZERO;
    UINT4               u4EtsConBw = DCBX_ZERO;
    UINT4               u4EtsRecoBw = DCBX_ZERO;
    INT4                i4ETSAdminWilling = DCBX_ZERO;
    UINT4               u4ETSAdminTCGID = DCBX_ZERO;
    INT4                i4ETSTcSup = DCBX_ZERO;
    INT4                i4EtsConTsa = DCBX_ZERO;
    INT4                i4EtsRecoTsa = DCBX_ZERO;
    INT4                i4ETSAdmCBS = DCBX_ZERO;

    CliPrintf (CliHandle,
               "\r TGID   Bandwidth RecomBandwidth "
               " TSA  RecomTSA  Priority\n");
    CliPrintf (CliHandle,
               "\r---------------------------------------------------------\n");

    for (i4TgIdx = DCBX_ZERO; i4TgIdx <= ETS_MAX_TCGID_CONF; i4TgIdx++)
    {
        if (i4TgIdx < ETS_MAX_TCGID_CONF)
        {
            nmhGetLldpXdot1dcbxAdminETSConTrafficClassBandwidth (i4IfIndex,
                                                                 (UINT4)
                                                                 i4TgIdx,
                                                                 &u4EtsConBw);
            nmhGetLldpXdot1dcbxAdminETSRecoTrafficClassBandwidth (i4IfIndex,
                                                                  (UINT4)
                                                                  i4TgIdx,
                                                                  &u4EtsRecoBw);
            nmhGetLldpXdot1dcbxAdminETSConTrafficSelectionAlgorithm (i4IfIndex,
                                                                     (UINT4)
                                                                     i4TgIdx,
                                                                     &i4EtsConTsa);
            nmhGetLldpXdot1dcbxAdminETSRecoTrafficSelectionAlgorithm (i4IfIndex,
                                                                      (UINT4)
                                                                      i4TgIdx,
                                                                      &i4EtsRecoTsa);
        }

        if (i4TgIdx == ETS_MAX_TCGID_CONF)
        {
            i4TgIdx = ETS_DEF_TCGID;
        }
        if (i4TgIdx == DCBX_ZERO)
        {
            CliPrintf (CliHandle, "%6d", i4TgIdx);
        }
        else
        {
            CliPrintf (CliHandle, "%6d", i4TgIdx);
        }

        if (i4TgIdx < ETS_MAX_TCGID_CONF)
        {
            CliPrintf (CliHandle, "%8d%%", u4EtsConBw);
        }
        else
        {
            CliPrintf (CliHandle, "       -");
        }
        CliPrintf (CliHandle, "    ");
        if (i4TgIdx < ETS_MAX_TCGID_CONF)
        {
            CliPrintf (CliHandle, "%6d%%", u4EtsRecoBw);
        }
        else
        {
            CliPrintf (CliHandle, "      -");
        }
        CliPrintf (CliHandle, "        ");
        if (i4TgIdx < ETS_MAX_TCGID_CONF)
        {
            if (i4EtsConTsa == DCBX_STRICT_PRIORITY_ALGO)
            {
                CliPrintf (CliHandle, "%-5s", "SPA");
            }
            else if (i4EtsConTsa == DCBX_CREDIT_BASED_SHAPER_ALGO)
            {
                CliPrintf (CliHandle, "%-5s", "CBSA");
            }
            else if (i4EtsConTsa == DCBX_ENHANCED_TRANSMISSION_SELECTION_ALGO)
            {
                CliPrintf (CliHandle, "%-5s", "ETSA");
            }
            else if (i4EtsConTsa == DCBX_VENDOR_SPECIFIC_ALGO)
            {
                CliPrintf (CliHandle, "%-5s", "VSA");
            }
            CliPrintf (CliHandle, "   ");
        }
        else
        {
            CliPrintf (CliHandle, "  -");
        }

        if (i4TgIdx < ETS_MAX_TCGID_CONF)
        {
            if (i4EtsRecoTsa == DCBX_STRICT_PRIORITY_ALGO)
            {
                CliPrintf (CliHandle, "%-5s", "SPA");
            }
            else if (i4EtsRecoTsa == DCBX_CREDIT_BASED_SHAPER_ALGO)
            {
                CliPrintf (CliHandle, "%-5s", "CBSA");
            }
            else if (i4EtsRecoTsa == DCBX_ENHANCED_TRANSMISSION_SELECTION_ALGO)
            {
                CliPrintf (CliHandle, "%-5s", "ETSA");
            }
            else if (i4EtsRecoTsa == DCBX_VENDOR_SPECIFIC_ALGO)
            {
                CliPrintf (CliHandle, "%-5s", "VSA");
            }
        }
        else
        {
            CliPrintf (CliHandle, "       -   ");
        }
        CliPrintf (CliHandle, "  ");
        for (u4PriIdx = DCBX_ZERO; u4PriIdx < DCBX_MAX_PRIORITIES; u4PriIdx++)
        {
            nmhGetLldpXdot1dcbxAdminETSConPriTrafficClass (i4IfIndex, u4PriIdx,
                                                           &u4ETSAdminTCGID);
            if ((INT4) u4ETSAdminTCGID == i4TgIdx)
            {
                CliPrintf (CliHandle, "%d ", u4PriIdx);
            }
        }
        CliPrintf (CliHandle, "\n\r");
    }
    CliPrintf (CliHandle, "\n\r");

    nmhGetLldpXdot1dcbxAdminETSConTrafficClassesSupported (i4IfIndex,
                                                           (UINT4 *)
                                                           &i4ETSTcSup);
    if (i4ETSTcSup == DCBX_ZERO)
    {
        i4ETSTcSup = ETS_TCGID8;
    }
    CliPrintf (CliHandle, "\r%-25s:%d\n", "Number of Traffic Class",
               i4ETSTcSup);
    nmhGetLldpXdot1dcbxAdminETSConWilling (i4IfIndex, &i4ETSAdminWilling);
    if (i4ETSAdminWilling == ETS_ENABLED)
    {
        CliPrintf (CliHandle, "\r%-25s:%-8s\n", "Willing Status", "Enabled");
    }
    else
    {
        CliPrintf (CliHandle, "\r%-25s:%-8s\n", "Willing Status", "Disabled");
    }

    nmhGetLldpXdot1dcbxAdminETSConCreditBasedShaperSupport (i4IfIndex,
                                                            &i4ETSAdmCBS);
    if (i4ETSAdmCBS == ETS_CBS_SUPPORTED)
    {
        CliPrintf (CliHandle,
                   "\r%-25s:%-8s\n", "Credit Based Shaper Algorithm",
                   "Supported");
    }
    else
    {
        CliPrintf (CliHandle,
                   "\r%-25s:%-8s\n", "Credit Based Shaper Algorithm",
                   "Not Supported");
    }

    CliPrintf (CliHandle, "\n\r");
    CliPrintf (CliHandle, "%s",
               "Terminology Used in Above Table:\r\n"
               "--------------------------------\r\n"
               "SPA  := Strict Priority Algorithm\r\n"
               "CBSA := Credit Based Shaper Algorithm\n\r"
               "ETSA := Enhanced Transmission Slection Algorithm\r\n"
               "VSA  := Vendor Specific Algorithm\r\n");
    CliPrintf (CliHandle, "\n\r");
    return (CLI_SUCCESS);
}

/*****************************************************************************
* Function Name     : ETSCliShowRemPortInfo                                  
* Description       : This function is used to display the ETS Remote Port status                                 
* Input(s)          : CliHandle    - Handle to CLI session     
*                   : pETSPortEntry    - pointer to ETS port Entry  
                    : i4IfIndex -     Interface Index  
* Output(s)         : None.                                               
* Global Variables                                                         
* Referred          : None.                                               
* Global Variables                                                          
* Modified          : None.                                                
* Return Value(s)   : CLI_SUCCESS/CLI_FAILURE           
* Called By         : PFCCliShowPortInfo                                                
*****************************************************************************/
PRIVATE INT4
ETSCliShowRemPortInfo (tCliHandle CliHandle, INT4 i4IfIndex)
{

    INT4                i4TgIdx = DCBX_ZERO;
    UINT4               u4PriIdx = DCBX_ZERO;
    INT4                i4ETSRemWilling = DCBX_ZERO;
    INT4                i4ETSRemTCGID = DCBX_ZERO;
    INT4                i4ETSTcSup = DCBX_ZERO;
    INT4                i4LldpV2RemLocalIfIndex = DCBX_ZERO;
    UINT4               u4LldpV2RemTimeMark = DCBX_ZERO;
    INT4                i4LldpV2RemIndex = DCBX_ZERO;
    INT4                i4LldpV2RemRecoLocalIfIndex = DCBX_ZERO;
    UINT4               u4LldpV2RemRecoTimeMark = DCBX_ZERO;
    INT4                i4LldpV2RemRecoIndex = DCBX_ZERO;
    UINT4               u4EtsConBw = DCBX_ZERO;
    UINT4               u4EtsRecoBw = DCBX_ZERO;
    INT4                i4EtsConTsa = DCBX_ZERO;
    INT4                i4EtsRecoTsa = DCBX_ZERO;
    UINT4               u4LldpV2RemLocalDestMACAddress = DCBX_ZERO;
    UINT4               u4LldpV2RemRecoLocalDestMACAddress = DCBX_ZERO;
    INT4                i4ETSRemCBS = DCBX_ZERO;
    UINT4               u4LldpV2RemTcgId = DCBX_ZERO;
    BOOL1               b1ConfTlvPresent = OSIX_FALSE;
    BOOL1               b1RecoTlvPresent = OSIX_FALSE;

    nmhGetNextIndexLldpXdot1dcbxRemETSBasicConfigurationTable
        (DCBX_ZERO, &u4LldpV2RemTimeMark, i4IfIndex,
         &i4LldpV2RemLocalIfIndex, DCBX_ZERO,
         &u4LldpV2RemLocalDestMACAddress, DCBX_ZERO, &i4LldpV2RemIndex);
    nmhGetNextIndexLldpXdot1dcbxRemETSRecoTrafficClassBandwidthTable
        (DCBX_ZERO, &u4LldpV2RemRecoTimeMark, i4IfIndex,
         &i4LldpV2RemRecoLocalIfIndex, DCBX_ZERO,
         &u4LldpV2RemRecoLocalDestMACAddress, DCBX_ZERO, &i4LldpV2RemRecoIndex,
         DCBX_ZERO, &u4LldpV2RemTcgId);

    if (((i4IfIndex != i4LldpV2RemLocalIfIndex) ||
         (i4LldpV2RemIndex == DCBX_ZERO)) &&
        ((i4IfIndex != i4LldpV2RemRecoLocalIfIndex) ||
         (i4LldpV2RemRecoIndex == DCBX_ZERO)))
    {
        CliPrintf (CliHandle, "\rNo Remote Entry is Present\n\r");
        return (CLI_SUCCESS);
    }
    if ((i4IfIndex == i4LldpV2RemLocalIfIndex) &&
        (i4LldpV2RemIndex != DCBX_ZERO))
    {
        b1ConfTlvPresent = OSIX_TRUE;
    }
    if ((i4IfIndex == i4LldpV2RemRecoLocalIfIndex) &&
        (i4LldpV2RemRecoIndex != DCBX_ZERO))
    {
        b1RecoTlvPresent = OSIX_TRUE;
    }
    CliPrintf (CliHandle,
               "\r TGID   Bandwidth RecomBandwidth "
               " TSA  RecomTSA  Priority\n");
    CliPrintf (CliHandle,
               "\r---------------------------------------------------------\n");
    for (i4TgIdx = DCBX_ZERO; i4TgIdx <= ETS_MAX_TCGID_CONF; i4TgIdx++)
    {
        if (i4TgIdx < ETS_MAX_TCGID_CONF)
        {
            if (b1ConfTlvPresent == OSIX_TRUE)
            {
                nmhGetLldpXdot1dcbxRemETSConTrafficClassBandwidth
                    (u4LldpV2RemTimeMark, i4IfIndex,
                     u4LldpV2RemLocalDestMACAddress, i4LldpV2RemIndex,
                     (UINT4) i4TgIdx, &(u4EtsConBw));

                nmhGetLldpXdot1dcbxRemETSConTrafficSelectionAlgorithm
                    (u4LldpV2RemTimeMark, i4IfIndex,
                     u4LldpV2RemLocalDestMACAddress, i4LldpV2RemIndex,
                     (UINT4) i4TgIdx, &(i4EtsConTsa));
            }

            if (b1RecoTlvPresent == OSIX_TRUE)
            {
                nmhGetLldpXdot1dcbxRemETSRecoTrafficClassBandwidth
                    (u4LldpV2RemTimeMark, i4IfIndex,
                     u4LldpV2RemLocalDestMACAddress, i4LldpV2RemIndex,
                     (UINT4) i4TgIdx, &(u4EtsRecoBw));

                nmhGetLldpXdot1dcbxRemETSRecoTrafficSelectionAlgorithm
                    (u4LldpV2RemTimeMark, i4IfIndex,
                     u4LldpV2RemLocalDestMACAddress, i4LldpV2RemIndex,
                     (UINT4) i4TgIdx, &(i4EtsRecoTsa));
            }
        }

        if (i4TgIdx == ETS_MAX_TCGID_CONF)
        {
            i4TgIdx = ETS_DEF_TCGID;
        }
        if (i4TgIdx == DCBX_ZERO)
        {
            CliPrintf (CliHandle, "%6d", i4TgIdx);
        }
        else
        {
            CliPrintf (CliHandle, "%6d", i4TgIdx);
        }

        if (i4TgIdx < ETS_MAX_TCGID_CONF)
        {
            if (b1ConfTlvPresent == OSIX_TRUE)
            {
                CliPrintf (CliHandle, "%8d%%", u4EtsConBw);
            }
            else
            {
                CliPrintf (CliHandle, "%8s", "NA");
            }
        }
        else
        {
            CliPrintf (CliHandle, "       -");
        }
        CliPrintf (CliHandle, "    ");
        if (i4TgIdx < ETS_MAX_TCGID_CONF)
        {
            if (b1RecoTlvPresent == OSIX_TRUE)
            {
                CliPrintf (CliHandle, "%6d%%", u4EtsRecoBw);
            }
            else
            {
                CliPrintf (CliHandle, "%-6s", "NA");
            }
        }
        else
        {
            CliPrintf (CliHandle, "      -");
        }

        CliPrintf (CliHandle, "        ");
        if (i4TgIdx < ETS_MAX_TCGID_CONF)
        {
            if (b1ConfTlvPresent == OSIX_TRUE)
            {
                if (i4EtsConTsa == DCBX_STRICT_PRIORITY_ALGO)
                {
                    CliPrintf (CliHandle, "%-5s", "SPA");
                }
                else if (i4EtsConTsa == DCBX_CREDIT_BASED_SHAPER_ALGO)
                {
                    CliPrintf (CliHandle, "%-5s", "CBSA");
                }
                else if (i4EtsConTsa ==
                         DCBX_ENHANCED_TRANSMISSION_SELECTION_ALGO)
                {
                    CliPrintf (CliHandle, "%-5s", "ETSA");
                }
                else if (i4EtsConTsa == DCBX_VENDOR_SPECIFIC_ALGO)
                {
                    CliPrintf (CliHandle, "%-5s", "VSA");
                }
            }
            else
            {
                CliPrintf (CliHandle, "%-5s", "NA");
            }
            CliPrintf (CliHandle, "   ");
        }
        else
        {
            CliPrintf (CliHandle, "  -");
        }

        if (i4TgIdx < ETS_MAX_TCGID_CONF)
        {
            if (b1RecoTlvPresent == OSIX_TRUE)
            {
                if (i4EtsRecoTsa == DCBX_STRICT_PRIORITY_ALGO)
                {
                    CliPrintf (CliHandle, "%-5s", "SPA");
                }
                else if (i4EtsRecoTsa == DCBX_CREDIT_BASED_SHAPER_ALGO)
                {
                    CliPrintf (CliHandle, "%-5s", "CBSA");
                }
                else if (i4EtsRecoTsa ==
                         DCBX_ENHANCED_TRANSMISSION_SELECTION_ALGO)
                {
                    CliPrintf (CliHandle, "%-5s", "ETSA");
                }
                else if (i4EtsRecoTsa == DCBX_VENDOR_SPECIFIC_ALGO)
                {
                    CliPrintf (CliHandle, "%-5s", "VSA");
                }
            }
            else
            {
                CliPrintf (CliHandle, "%-5s", "NA");
            }
        }
        else
        {
            CliPrintf (CliHandle, "       -   ");
        }
        CliPrintf (CliHandle, "  ");
        if (b1ConfTlvPresent == OSIX_TRUE)
        {
            for (u4PriIdx = DCBX_ZERO; u4PriIdx < DCBX_MAX_PRIORITIES;
                 u4PriIdx++)
            {

                nmhGetLldpXdot1dcbxRemETSConPriTrafficClass
                    (u4LldpV2RemTimeMark, i4LldpV2RemLocalIfIndex,
                     u4LldpV2RemLocalDestMACAddress, i4LldpV2RemIndex, u4PriIdx,
                     (UINT4 *) &i4ETSRemTCGID);
                if (i4ETSRemTCGID == i4TgIdx)
                {
                    CliPrintf (CliHandle, "%d ", u4PriIdx);
                }
            }
        }
        else
        {
            if (i4TgIdx < ETS_MAX_TCGID_CONF)
            {
                CliPrintf (CliHandle, "%-5s", "NA");
            }
        }
        CliPrintf (CliHandle, "\n\r");
    }
    CliPrintf (CliHandle, "\n\r");

    if (b1ConfTlvPresent == OSIX_TRUE)
    {
        nmhGetLldpXdot1dcbxRemETSConTrafficClassesSupported
            (u4LldpV2RemTimeMark, i4LldpV2RemLocalIfIndex,
             u4LldpV2RemLocalDestMACAddress, i4LldpV2RemIndex,
             (UINT4 *) &i4ETSTcSup);
        if (i4ETSTcSup == DCBX_ZERO)
        {
            i4ETSTcSup = ETS_TCGID8;
        }
        CliPrintf (CliHandle, "\r%-25s:%d\n", "Number of Traffic Class",
                   i4ETSTcSup);
        nmhGetLldpXdot1dcbxRemETSConWilling (u4LldpV2RemTimeMark,
                                             i4LldpV2RemLocalIfIndex,
                                             u4LldpV2RemLocalDestMACAddress,
                                             i4LldpV2RemIndex,
                                             &i4ETSRemWilling);
        if (i4ETSRemWilling == ETS_ENABLED)
        {
            CliPrintf (CliHandle, "\r%-25s:%-8s\n", "Willing Status",
                       "Enabled");
        }
        else
        {
            CliPrintf (CliHandle, "\r%-25s:%-8s\n", "Willing Status",
                       "Disabled");
        }

        nmhGetLldpXdot1dcbxRemETSConCreditBasedShaperSupport
            (u4LldpV2RemTimeMark, i4LldpV2RemLocalIfIndex,
             u4LldpV2RemLocalDestMACAddress, i4LldpV2RemIndex, &i4ETSRemCBS);
        if (i4ETSRemCBS == ETS_CBS_SUPPORTED)
        {
            CliPrintf (CliHandle,
                       "\r%-25s:%-8s\n", "Credit Based Shaper Algorithm",
                       "Supported");
        }
        else
        {
            CliPrintf (CliHandle,
                       "\r%-25s:%-8s\n", "Credit Based Shaper Algorithm",
                       "Not Supported");
        }
    }
    else
    {
        CliPrintf (CliHandle, "\r%-25s\n",
                   "Configuration TLV info from remote is NA");
    }

    CliPrintf (CliHandle, "\n\r");
    CliPrintf (CliHandle, "%s",
               "Terminology Used in Above Table:\r\n"
               "--------------------------------\r\n"
               "SPA  := Strict Priority Algorithm\r\n"
               "CBSA := Credit Based Shaper Algorithm\n\r"
               "ETSA := Enhanced Transmission Slection Algorithm\r\n"
               "VSA  := Vendor Specific Algorithm\r\n"
               "NA  := Remote information NOT AVAILABLE\r\n");
    CliPrintf (CliHandle, "\n\r");
    return (CLI_SUCCESS);
}

/***************************************************************************
* Function Name     : ETSCliShowGbl
* Description       : This function is used to display the ETS Global 
*                     Information
* Input(s)          : CliHandle    - Handle to CLI session
* Output(s)         : None.
* Global Variables
* Referred          : None.
* Global Variables
* Modified          : None.
* Return Value(s)   : CLI_SUCCESS/CLI_FAILURE
*****************************************************************************/

PRIVATE INT4
ETSCliShowGbl (tCliHandle CliHandle)
{
    UINT4               u4TrapCount = DCBX_ZERO;
    INT4                i4SysStatus = DCBX_ZERO;
    INT4                i4ModStatus = DCBX_ZERO;
    INT4                i4ETSTrapEnabled = DCBX_ZERO;

    CliPrintf (CliHandle, "\n\r");
    CliPrintf (CliHandle, "\r Priority Grouping GLOBAL Info \n");
    CliPrintf (CliHandle, "\r-----------------------------------\n");
    /*Showing  Min & MAX Threshold Vaues */
    nmhGetFsETSSystemControl (&i4SysStatus);
    nmhGetFsETSModuleStatus (&i4ModStatus);
    nmhGetFsETSGlobalEnableTrap (&i4ETSTrapEnabled);
    nmhGetFsETSGeneratedTrapCount (&u4TrapCount);
    if (i4SysStatus == ETS_START)
    {
        CliPrintf (CliHandle, "\r ETS System control status : Start\n");
    }
    else
    {
        CliPrintf (CliHandle, "\r ETS System control status : Shutdown\n");
    }

    if (i4ModStatus == ETS_ENABLED)
    {
        CliPrintf (CliHandle, "\r ETS Module status : Enabled\n");
    }
    else
    {
        CliPrintf (CliHandle, "\r ETS Module status : Disabled\n");
    }
    CliPrintf (CliHandle, "\r ETS Trap Count            : %d\n", u4TrapCount);

    CliPrintf (CliHandle, "\r Trap(s) Enabled:\n");
    CliPrintf (CliHandle, "\r ---------------- \n");

    if ((i4ETSTrapEnabled & ETS_ALL_TRAP) == ETS_ALL_TRAP)
    {
        CliPrintf (CliHandle, "\r All\n");
    }
    else if (i4ETSTrapEnabled == DCBX_ZERO)
    {
        CliPrintf (CliHandle, "\r None\n");
    }
    else
    {
        if ((i4ETSTrapEnabled & ETS_MODULE_STATUS_TRAP) ==
            ETS_MODULE_STATUS_TRAP)
        {
            CliPrintf (CliHandle, "\r ModuleStatusChange\n");
        }
        if ((i4ETSTrapEnabled & ETS_ADMIN_MODE_TRAP) == ETS_ADMIN_MODE_TRAP)
        {
            CliPrintf (CliHandle, "\r AdminModeChange\n");
        }
        if ((i4ETSTrapEnabled & ETS_PEER_STATUS_TRAP) == ETS_PEER_STATUS_TRAP)
        {
            CliPrintf (CliHandle, "\r PeerStatusChange\n");
        }
        if ((i4ETSTrapEnabled & ETS_SEM_TRAP) == ETS_SEM_TRAP)
        {
            CliPrintf (CliHandle, "\r SemChange\n");
        }
    }

    CliPrintf (CliHandle, "\n\r");
    return CLI_SUCCESS;

}

/*****************************************************************************
 Function Name      : PFCCliSetControlStatus
 Description        : This function will be used to start/shutdown 
                      the PFC Module on the subsystem 
 Input(s)           : CliHandle   - Handle to CLI session                
                    : u1Status - Start/Shutdown status for PFC module.
 Output(s)          : CLI_SET_ERR will be used in case any errors in the 
                      Test Routine and 
                      CLI_FATAL_ERROR (CliHanlde) will be used in case of any 
                      errors in the Set Routine.                                               
 Return Value(s)    : CLI_SUCCESS/CLI_FAILURE           
 Called By          : cli_process_dcbx_cmd                         
 Calling Function   : SNMP nmh Routines                         
*****************************************************************************/
PRIVATE INT4
PFCCliSetControlStatus (tCliHandle CliHandle, UINT1 u1Status)
{
    UINT4               u4ErrorCode = SNMP_ERR_NO_ERROR;

    /* Test the Given Vlaue if it success then Set */
    if (nmhTestv2FsPFCSystemControl (&u4ErrorCode, u1Status) != SNMP_SUCCESS)
    {
        return (CLI_FAILURE);
    }

    if (nmhSetFsPFCSystemControl (u1Status) != SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }

    return (CLI_SUCCESS);

}

/*****************************************************************************
 Function Name      : PFCCliSetModuleStatus
 Description        : This function will be used to enable/diable
                      the PFC Module on the subsystem.        
 Input(s)           : CliHandle    - Handle to CLI session                
                    : u1Status - enable/diable status for PFC module
 Output(s)          : CLI_SET_ERR will be used in case any errors in the
                      Test Routine and 
                      CLI_FATAL_ERROR (CliHanlde) will be used in case of any 
                      errors in the Set Routine.        
 Return Value(s)    : CLI_SUCCESS/CLI_FAILURE           
 Called By          : cli_process_dcbx_cmd                         
 Calling Function   : SNMP nmh Routines                         
*****************************************************************************/
PRIVATE INT4
PFCCliSetModuleStatus (tCliHandle CliHandle, UINT1 u1Status)
{
    UINT4               u4ErrorCode = SNMP_ERR_NO_ERROR;

    UNUSED_PARAM (CliHandle);
    /* Test the Given Vlaue if it success then Set */
    if (nmhTestv2FsPFCModuleStatus (&u4ErrorCode, u1Status) != SNMP_SUCCESS)
    {
        return (CLI_FAILURE);
    }

    nmhSetFsPFCModuleStatus (u1Status);
    return (CLI_SUCCESS);

}

/*****************************************************************************
 Function Name      : PFCCliSetPortMode
 Description        : This function will be used to set Priority flow 
                      control feature mode (Negotiable or Force-enable) on the
                      given port.  
                              
 Input(s)           : CliHandle    - Handle to CLI session                
                    : i4IfIndex - Port number for which the Admin mode to be 
                    set.
                    :u1AdminMode -Auto/On Mode.  
 Output(s)          : CLI_SET_ERR will be used in case any errors in the Test
                      Routine and 
                      CLI_FATAL_ERROR (CliHanlde) will be used in case of any 
                      errors in the Set Routine.   
 Return Value(s)     : CLI_SUCCESS/CLI_FAILURE           
 Called By           : cli_process_dcbx_cmd                         
 Calling Function    : SNMP nmh Routines                         
*****************************************************************************/
PRIVATE INT4
PFCCliSetPortMode (tCliHandle CliHandle, INT4 i4IfIndex, UINT1 u1AdminMode)
{
    UINT4               u4ErrorCode = SNMP_ERR_NO_ERROR;

    UNUSED_PARAM (CliHandle);
    /* Test the Given Vlaue if it success then Set */
    if (nmhTestv2FsPFCAdminMode (&u4ErrorCode, i4IfIndex, u1AdminMode)
        != SNMP_SUCCESS)
    {
        return (CLI_FAILURE);
    }

    nmhSetFsPFCAdminMode (i4IfIndex, u1AdminMode);
    return (CLI_SUCCESS);

}

/*****************************************************************************
 Function Name      : PFCCliSetPortStatus
 Description        : This function will be used to enable/disable the Priority
                      grouping Admin status on the given port.  
 Input(s)           : CliHandle    - Handle to CLI session                
                    : i4IfIndex - Port number for which the Priority flow 
                       control admin status to be set.
                    :u1PFCRowStatus -Enabled/Disabled.  
 Output(s)          : None.                                               
 Return Value(s)    : CLI_SUCCESS/CLI_FAILURE           
 Called By          : cli_process_dcbx_cmd                         
 Calling Function   : SNMP nmh Routines                         
*****************************************************************************/
PRIVATE INT4
PFCCliSetPortStatus (tCliHandle CliHandle, INT4 i4IfIndex, UINT1 u1PFCRowStatus)
{
    UINT4               u4ErrorCode = SNMP_ERR_NO_ERROR;

    /* Test the Given Vlaue if it success then Set */
    if (nmhTestv2FsPFCRowStatus (&u4ErrorCode, i4IfIndex,
                                 u1PFCRowStatus) != SNMP_SUCCESS)
    {
        return (CLI_FAILURE);
    }

    if (nmhSetFsPFCRowStatus (i4IfIndex, u1PFCRowStatus) != SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }
    return (CLI_SUCCESS);

}

/*****************************************************************************
 Function Name      : PFCCliSetTrapStatus
 Description        : This function will be used to Enable/disable the 
                      PFC Trap based on the Trap type specified.
 Input(s)           : CliHandle    - Handle to CLI session                
                    : u4TrapType - PFC Trap types for which it should be
                      enable/disable.
                    :u1Status - enable/disable status for PFC module.
 Output(s)          : CLI_SET_ERR will be used in case any errors in the 
                      Test Routine and 
                      CLI_FATAL_ERROR (CliHanlde) will be used in case of 
                      any errors in the Set Routine.
 Return Value(s)     : CLI_SUCCESS/CLI_FAILURE           
 Called By              : cli_process_dcbx_cmd                         
 Calling Function     : SNMP nmh Routines                         
*****************************************************************************/
PRIVATE INT4
PFCCliSetTrapStatus (tCliHandle CliHandle, UINT4 u4TrapType, UINT1 u1Status)
{
    UINT4               u4ErrorCode = SNMP_ERR_NO_ERROR;
    INT4                i4EtsTrapVal = DCBX_ZERO;

    UNUSED_PARAM (CliHandle);
    nmhGetFsPFCGlobalEnableTrap (&i4EtsTrapVal);
    /* Trap Status Enable */
    if (u1Status == DCBX_TRAP_ENABLE)
    {
        i4EtsTrapVal = i4EtsTrapVal | (INT4) u4TrapType;
    }
    else
    {
        /* Trap status Disable */
        i4EtsTrapVal = (i4EtsTrapVal) & (INT4) (~u4TrapType);
    }

    /* Test the Given Vlaue if it success then Set */
    if (nmhTestv2FsPFCGlobalEnableTrap (&u4ErrorCode, i4EtsTrapVal)
        != SNMP_SUCCESS)
    {
        return (CLI_FAILURE);
    }

    nmhSetFsPFCGlobalEnableTrap (i4EtsTrapVal);
    return (CLI_SUCCESS);

}

/*****************************************************************************
 Function Name      : PFCCliSetThreshold                                
 Description        : This function is used to configure the minimum      
                      maximum thershold for the switch             
 Input(s)           : CliHandle    - Handle to CLI session               
                    : u4MinThersod - Minimum Threshold Value             
                    : u4MaxThersod - Maximum Threshold Value           
 Output(s)          : CLI_SET_ERR will be used in case any errors in the Test
                      Routine and 
                      CLI_FATAL_ERROR (CliHanlde) will be used in case of
                      any errors in the Set Routine.
 Return Value(s)    : CLI_SUCCESS/CLI_FAILURE           
 Called By          : cli_process_dcbx_cmd                         
 Calling Function   : SNMP nmh Routines                         
*****************************************************************************/

PRIVATE INT4
PFCCliSetThreshold (tCliHandle CliHandle, UINT4 u4MinThersod,
                    UINT4 u4MaxThresod)
{

    UINT4               u4ErrorCode = SNMP_ERR_NO_ERROR;

    /* Test for Min PFC Threshold */
    if (nmhTestv2FsDcbPfcMinThreshold (&u4ErrorCode, u4MinThersod) !=
        SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }
    /* Set for Min Threshold */
    if (nmhSetFsDcbPfcMinThreshold (u4MinThersod) != SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    /* Test for Max Pfc Threshold */
    if (nmhTestv2FsDcbPfcMaxThreshold (&u4ErrorCode, u4MaxThresod) !=
        SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }
    /* Set for Max threshold */
    if (nmhSetFsDcbPfcMaxThreshold (u4MaxThresod) != SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/*****************************************************************************
 Function Name      : PFCCliSetWillingStatus                                
 Description        : This function will be used to set Priority flow 
                      control willing status on the given port.     
 Input(s)           : CliHandle    - Handle to CLI session               
                    :i4IfIndex - Port number for which the Willing 
                     status to be set.
                    :u1Status - PFC_ENABLED/PFC_DISABLED. 
 Output(s)          : CLI_SET_ERR will be used in case any errors in the Test
                      Routine and 
                      CLI_FATAL_ERROR (CliHanlde) will be used in case of
                      any errors in the Set Routine.
 Return Value(s)    : CLI_SUCCESS/CLI_FAILURE           
 Called By          : cli_process_dcbx_cmd                         
 Calling Function   : SNMP nmh Routines                         
*****************************************************************************/
PRIVATE INT4
PFCCliSetWillingStatus (tCliHandle CliHandle, INT4 i4IfIndex, UINT1 u1Status)
{
    UINT4               u4ErrorCode = SNMP_ERR_NO_ERROR;

    UNUSED_PARAM (CliHandle);
    /* Test the Given Vlaue if it success then Set */
    if (nmhTestv2LldpXdot1dcbxAdminPFCWilling
        (&u4ErrorCode, i4IfIndex, u1Status) != SNMP_SUCCESS)
    {
        return (CLI_FAILURE);
    }

    nmhSetLldpXdot1dcbxAdminPFCWilling (i4IfIndex, u1Status);
    return (CLI_SUCCESS);
}

/*****************************************************************************
 Function Name      : PFCCliSetFCPerPriority                              
 Description        : This function is used to enable/diable             
                       PFC for the list of priorities of the port      
                       of priorities to TCGID                                            
 Input(s)           : CliHandle    - Handle to CLI session     
                    : i4IfIndex    - Interface Index                  
                    : pi4PriList   - Priroity List                         
                    : i4Status     - Pfc Status                          
 Output(s)          : None.                                                
 Return Value(s)    : CLI_SUCCESS/CLI_FAILURE           
 Called By          : cli_process_dcbx_cmd                         
 Calling Function   : SNMP nmh Routines                         
*****************************************************************************/

PRIVATE INT4
PFCCliSetFCPerPriority (tCliHandle CliHandle, INT4 i4IfIndex, INT4 *pi4PriList,
                        INT4 i4Status)
{
    UINT4               u4ErrorCode = SNMP_ERR_NO_ERROR;
    UINT4               u4Priority = DCBX_ZERO;
    UINT4               u4PriIdx = DCBX_ZERO;

    UNUSED_PARAM (CliHandle);
    for (u4PriIdx = DCBX_ZERO; u4PriIdx < DCBX_MAX_PRIORITIES; u4PriIdx++)
    {
        if (pi4PriList[u4PriIdx] != DCBX_INVALID_ID)
        {
            /* Test the Given Vlaue if it success then Set */
            u4Priority = (UINT4) pi4PriList[u4PriIdx];
            if (nmhTestv2LldpXdot1dcbxAdminPFCEnableEnabled
                (&u4ErrorCode, i4IfIndex, u4Priority, i4Status) != SNMP_SUCCESS)
            {
                return (CLI_FAILURE);
            }
        }
    }

    for (u4PriIdx = DCBX_ZERO; u4PriIdx < DCBX_MAX_PRIORITIES; u4PriIdx++)
    {
        if (pi4PriList[u4PriIdx] != DCBX_INVALID_ID)
        {
            u4Priority = (UINT4) pi4PriList[u4PriIdx];
            if (nmhSetLldpXdot1dcbxAdminPFCEnableEnabled
                (i4IfIndex, u4Priority, i4Status) != SNMP_SUCCESS)
            {
                CLI_SET_ERR (CLI_ERR_PFC_INVALID_ENABLED_STATUS);
                return CLI_FAILURE;
            }
        }
    }

    return CLI_SUCCESS;
}

/*****************************************************************************
 Function Name      : PFCCliSetTlvSelection                             
 Description        : This function will be used to enable/disable the
                     transmission status of PFC TLV
 Input(s)              : CliHandle    - Handle to CLI session      
                       : i4IfIndex    - Port number for which the PFC TLV 
                      Transmission to be set.                  
                    : u1Status - Enable/Disable status for PFC TLV Transmission.
 Output(s)          : CLI_SET_ERR will be used in case any errors in the Test
                      Routine and 
                      CLI_FATAL_ERROR (CliHanlde) will be used in case of any
                      errors in the Set Routine.                                
 Return Value(s)    : CLI_SUCCESS/CLI_FAILURE           
 Called By          : cli_process_dcbx_cmd                         
*****************************************************************************/
PRIVATE INT4
PFCCliSetTlvSelection (tCliHandle CliHandle, INT4 i4IfIndex, UINT1 u1Status)
{
    UINT4               u4ErrorCode = SNMP_ERR_NO_ERROR;

    UNUSED_PARAM (CliHandle);
    /*Enabling PFC TLV Tranfer Status */
    if (nmhTestv2LldpXdot1dcbxConfigPFCTxEnable
        (&u4ErrorCode, i4IfIndex, DCBX_DEST_ADDR_INDEX,
         u1Status) != SNMP_SUCCESS)
    {
        return (CLI_FAILURE);
    }

    nmhSetLldpXdot1dcbxConfigPFCTxEnable
        (i4IfIndex, DCBX_DEST_ADDR_INDEX, u1Status);
    return (CLI_SUCCESS);

}

/*****************************************************************************
 Function Name       : PFCCliClearCounters                             
 Description         :This function will be used to Clear the PFC Counters
                      for the specified port or for all the ports.
 Input(s)               : CliHandle - Handle to CLI session      
                     : i4IfIndex -  Port number for which the PFC counters
                       to be cleared.
                     : u1Value - To mention whether to clear for specific port
                       or to clear for all ports.
 Output(s)           : CLI_SET_ERR will be used in case any errors in the Test
                       Routine and 
                       CLI_FATAL_ERROR (CliHanlde) will be used in case of 
                       any errors in the Set Routine.
 Return Value(s) : CLI_SUCCESS/CLI_FAILURE           
 Called By          : cli_process_dcbx_cmd                         
*****************************************************************************/

PRIVATE INT4
PFCCliClearCounters (tCliHandle CliHandle, INT4 i4IfIndex)
{
    UINT4               u4ErrorCode = SNMP_ERR_NO_ERROR;

    UNUSED_PARAM (CliHandle);
    /* Clear PFC counters for all ports */
    if (i4IfIndex == DCBX_ZERO)
    {
        if (nmhTestv2FsPFCClearCounters
            (&u4ErrorCode, PFC_ENABLED) != SNMP_SUCCESS)
        {
            return (CLI_FAILURE);
        }

        nmhSetFsPFCClearCounters (PFC_ENABLED);
    }
    else
    {
        /* Clear PFC counters per port */
        if (nmhTestv2FsPFCClearTLVCounters (&u4ErrorCode, i4IfIndex,
                                            PFC_ENABLED) != SNMP_SUCCESS)
        {
            return (CLI_FAILURE);
        }

        nmhSetFsPFCClearTLVCounters (i4IfIndex, PFC_ENABLED);
    }
    return (CLI_SUCCESS);
}

 /*****************************************************************************
 Function Name       : PFCCliShowPortCounters                             
 Description         :This function will be used to Show the PFC counters for
                       ports.
 Input(s)            : CliHandle - Handle to CLI session      
                     : i4IfIndex -  Port number for which the PFC counter 
                       information to be shown.
 Output(s)           : CLI_SET_ERR will be used in case any errors in the Test
                       Routine and 
                       CLI_FATAL_ERROR (CliHanlde) will be used in case of 
                       any errors in the Set Routine.
 Return Value(s)    : CLI_SUCCESS/CLI_FAILURE           
 Called By          : cli_process_dcbx_cmd                         
*****************************************************************************/
PRIVATE INT4
PFCCliShowPortCounters (tCliHandle CliHandle, INT4 i4IfIndex)
{

    UINT4               u4PFCTxTLVCounter = DCBX_ZERO;
    UINT4               u4PFCRxTLVCounter = DCBX_ZERO;
    UINT4               u4PFCErrorTLVCounter = DCBX_ZERO;
    UINT4               u4PFCTotalRxCounter = DCBX_ZERO;
    UINT4               u4PFCTotalTxCounter = DCBX_ZERO;
    UINT4               u4PfcStatsRx0 = DCBX_ZERO;
    UINT4               u4PfcStatsRx1 = DCBX_ZERO;
    UINT4               u4PfcStatsRx2 = DCBX_ZERO;
    UINT4               u4PfcStatsRx3 = DCBX_ZERO;
    UINT4               u4PfcStatsRx4 = DCBX_ZERO;
    UINT4               u4PfcStatsRx5 = DCBX_ZERO;
    UINT4               u4PfcStatsRx6 = DCBX_ZERO;
    UINT4               u4PfcStatsRx7 = DCBX_ZERO;
    UINT4               u4PfcStatsTx0 = DCBX_ZERO;
    UINT4               u4PfcStatsTx1 = DCBX_ZERO;
    UINT4               u4PfcStatsTx2 = DCBX_ZERO;
    UINT4               u4PfcStatsTx3 = DCBX_ZERO;
    UINT4               u4PfcStatsTx4 = DCBX_ZERO;
    UINT4               u4PfcStatsTx5 = DCBX_ZERO;
    UINT4               u4PfcStatsTx6 = DCBX_ZERO;
    UINT4               u4PfcStatsTx7 = DCBX_ZERO;
    INT4                i4Port = DCBX_ZERO;
    INT4                i4NextPort = DCBX_ZERO;
    UINT1               au1NameStr[CFA_MAX_PORT_NAME_LENGTH] = { DCBX_ZERO };

    if (i4IfIndex == DCBX_ZERO)
    {
        if (nmhGetFirstIndexFsPFCPortTable (&i4NextPort) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\rNo PFC Port is created\n");
            CliPrintf (CliHandle,
                       "\r----------------------------------------\n");
            CliPrintf (CliHandle, "\r\n");
            return CLI_SUCCESS;
        }

        do
        {
            i4Port = i4NextPort;
            CfaCliGetIfName ((UINT4) i4Port, (INT1 *) au1NameStr);
            nmhGetFsPFCTxTLVCounter (i4Port, &u4PFCTxTLVCounter);
            nmhGetFsPFCRxTLVCounter (i4Port, &u4PFCRxTLVCounter);
            nmhGetFsPFCRxTLVErrors (i4Port, &u4PFCErrorTLVCounter);
            nmhGetFsPFCRxPauseFrameP0Counter (i4Port, &u4PfcStatsRx0);
            nmhGetFsPFCRxPauseFrameP1Counter (i4Port, &u4PfcStatsRx1);
            nmhGetFsPFCRxPauseFrameP2Counter (i4Port, &u4PfcStatsRx2);
            nmhGetFsPFCRxPauseFrameP3Counter (i4Port, &u4PfcStatsRx3);
            nmhGetFsPFCRxPauseFrameP4Counter (i4Port, &u4PfcStatsRx4);
            nmhGetFsPFCRxPauseFrameP5Counter (i4Port, &u4PfcStatsRx5);
            nmhGetFsPFCRxPauseFrameP6Counter (i4Port, &u4PfcStatsRx6);
            nmhGetFsPFCRxPauseFrameP7Counter (i4Port, &u4PfcStatsRx7);
            nmhGetFsPFCTxPauseFrameP0Counter (i4Port, &u4PfcStatsTx0);
            nmhGetFsPFCTxPauseFrameP1Counter (i4Port, &u4PfcStatsTx1);
            nmhGetFsPFCTxPauseFrameP2Counter (i4Port, &u4PfcStatsTx2);
            nmhGetFsPFCTxPauseFrameP3Counter (i4Port, &u4PfcStatsTx3);
            nmhGetFsPFCTxPauseFrameP4Counter (i4Port, &u4PfcStatsTx4);
            nmhGetFsPFCTxPauseFrameP5Counter (i4Port, &u4PfcStatsTx5);
            nmhGetFsPFCTxPauseFrameP6Counter (i4Port, &u4PfcStatsTx6);
            nmhGetFsPFCTxPauseFrameP7Counter (i4Port, &u4PfcStatsTx7);
            /* Total RX PFC pause frames */
            u4PFCTotalRxCounter = (u4PfcStatsRx0 + u4PfcStatsRx1 +
                                   u4PfcStatsRx2 + u4PfcStatsRx3 +
                                   u4PfcStatsRx4 + u4PfcStatsRx5 +
                                   u4PfcStatsRx6 + u4PfcStatsRx7);
            /* Total TX PFC pause frames */
            u4PFCTotalTxCounter = (u4PfcStatsTx0 + u4PfcStatsTx1 +
                                   u4PfcStatsTx2 + u4PfcStatsTx3 +
                                   u4PfcStatsTx4 + u4PfcStatsTx5 +
                                   u4PfcStatsTx6 + u4PfcStatsTx7);

            CliPrintf (CliHandle, "\r%-9s\n", au1NameStr);
            CliPrintf (CliHandle, "\r%-1s%-15s%-2d\n", " ", "TLV Transmitted: ",
                       u4PFCTxTLVCounter);
            CliPrintf (CliHandle, "\r%-1s%-12s%-2d\n", " ", "TLV Received: ",
                       u4PFCRxTLVCounter);
            CliPrintf (CliHandle, "\r%-1s%-10s%-2d\n", " ", "TLV Errors: ",
                       u4PFCErrorTLVCounter);
            CliPrintf (CliHandle, "\r%-1s%-25s%-2d\n", " ",
                       "Total Rx PFC Pause Frames: ", u4PFCTotalRxCounter);
            CliPrintf (CliHandle, "\r%-1s%-27s%-2d\n\n", " ",
                       "Total Tx PFC Pause Frames: ", u4PFCTotalTxCounter);
            CliPrintf (CliHandle,
                       "\r%-1s---------------------------------------------"
                       "--------------------------------------------------------\n",
                       " ");
            CliPrintf (CliHandle,
                       "\r%-4s|%-10s |%-10s |%-10s |%-10s |%-10s |"
                       "%-10s |%-10s |%-10s |\n", " ", " Priority0",
                       " Priority1", " Priority2", " Priority3", " Priority4",
                       " Priority5", " Priority6", " Priority7 ");
            CliPrintf (CliHandle,
                       "\r%-1s---------------------------------------------"
                       "--------------------------------------------------------\n",
                       " ");
            CliPrintf (CliHandle,
                       "\r%-1s%-2s|%-10d |%-10d |%-10d |%-10d |%-10d |"
                       "%-10d |%-10d |%-10d\n", " ", "Rx ", u4PfcStatsRx0,
                       u4PfcStatsRx1, u4PfcStatsRx2, u4PfcStatsRx3,
                       u4PfcStatsRx4, u4PfcStatsRx5, u4PfcStatsRx6,
                       u4PfcStatsRx7);
            CliPrintf (CliHandle,
                       "\r%-1s---------------------------------------------"
                       "--------------------------------------------------------\n",
                       " ");
            CliPrintf (CliHandle,
                       "\r%-1s%-2s|%-10d |%-10d |%-10d |%-10d |%-10d |"
                       "%-10d |%-10d |%-10d\n", " ", "Tx ", u4PfcStatsTx0,
                       u4PfcStatsTx1, u4PfcStatsTx2, u4PfcStatsTx3,
                       u4PfcStatsTx4, u4PfcStatsTx5, u4PfcStatsTx6,
                       u4PfcStatsTx7);
            CliPrintf (CliHandle, "\r\n");
        }
        while ((nmhGetNextIndexFsPFCPortTable (i4Port, &i4NextPort)
                == SNMP_SUCCESS));
    }
    else
    {
        CfaCliGetIfName ((UINT4) i4IfIndex, (INT1 *) au1NameStr);
        nmhGetFsPFCTxTLVCounter (i4IfIndex, &u4PFCTxTLVCounter);
        nmhGetFsPFCRxTLVCounter (i4IfIndex, &u4PFCRxTLVCounter);
        nmhGetFsPFCRxTLVErrors (i4IfIndex, &u4PFCErrorTLVCounter);
        nmhGetFsPFCRxPauseFrameP0Counter (i4IfIndex, &u4PfcStatsRx0);
        nmhGetFsPFCRxPauseFrameP1Counter (i4IfIndex, &u4PfcStatsRx1);
        nmhGetFsPFCRxPauseFrameP2Counter (i4IfIndex, &u4PfcStatsRx2);
        nmhGetFsPFCRxPauseFrameP3Counter (i4IfIndex, &u4PfcStatsRx3);
        nmhGetFsPFCRxPauseFrameP4Counter (i4IfIndex, &u4PfcStatsRx4);
        nmhGetFsPFCRxPauseFrameP5Counter (i4IfIndex, &u4PfcStatsRx5);
        nmhGetFsPFCRxPauseFrameP6Counter (i4IfIndex, &u4PfcStatsRx6);
        nmhGetFsPFCRxPauseFrameP7Counter (i4IfIndex, &u4PfcStatsRx7);
        nmhGetFsPFCTxPauseFrameP0Counter (i4IfIndex, &u4PfcStatsTx0);
        nmhGetFsPFCTxPauseFrameP1Counter (i4IfIndex, &u4PfcStatsTx1);
        nmhGetFsPFCTxPauseFrameP2Counter (i4IfIndex, &u4PfcStatsTx2);
        nmhGetFsPFCTxPauseFrameP3Counter (i4IfIndex, &u4PfcStatsTx3);
        nmhGetFsPFCTxPauseFrameP4Counter (i4IfIndex, &u4PfcStatsTx4);
        nmhGetFsPFCTxPauseFrameP5Counter (i4IfIndex, &u4PfcStatsTx5);
        nmhGetFsPFCTxPauseFrameP6Counter (i4IfIndex, &u4PfcStatsTx6);
        nmhGetFsPFCTxPauseFrameP7Counter (i4IfIndex, &u4PfcStatsTx7);
        /* Total RX PFC pause frames */
        u4PFCTotalRxCounter = (u4PfcStatsRx0 + u4PfcStatsRx1 +
                               u4PfcStatsRx2 + u4PfcStatsRx3 +
                               u4PfcStatsRx4 + u4PfcStatsRx5 +
                               u4PfcStatsRx6 + u4PfcStatsRx7);
        /* Total TX PFC pause frames */
        u4PFCTotalTxCounter = (u4PfcStatsTx0 + u4PfcStatsTx1 +
                               u4PfcStatsTx2 + u4PfcStatsTx3 +
                               u4PfcStatsTx4 + u4PfcStatsTx5 +
                               u4PfcStatsTx6 + u4PfcStatsTx7);

        CliPrintf (CliHandle, "\r%-9s\n", au1NameStr);
        CliPrintf (CliHandle, "\r%-1s%-15s%-2d\n", " ", "TLV Transmitted: ",
                   u4PFCTxTLVCounter);
        CliPrintf (CliHandle, "\r%-1s%-12s%-2d\n", " ", "TLV Received: ",
                   u4PFCRxTLVCounter);
        CliPrintf (CliHandle, "\r%-1s%-10s%-2d\n", " ", "TLV Errors: ",
                   u4PFCErrorTLVCounter);
        CliPrintf (CliHandle, "\r%-1s%-25s%-2d\n", " ",
                   "Total Rx PFC Pause Frames: ", u4PFCTotalRxCounter);
        CliPrintf (CliHandle, "\r%-1s%-27s%-2d\n\n", " ",
                   "Total Tx PFC Pause Frames: ", u4PFCTotalTxCounter);
        CliPrintf (CliHandle,
                   "\r%-1s---------------------------------------------"
                   "--------------------------------------------------------\n",
                   " ");
        CliPrintf (CliHandle,
                   "\r%-4s|%-10s |%-10s |%-10s |%-10s |%-10s |"
                   "%-10s |%-10s |%-10s |\n", " ", " Priority0", " Priority1",
                   " Priority2", " Priority3", " Priority4", " Priority5",
                   " Priority6", " Priority7 ");
        CliPrintf (CliHandle,
                   "\r%-1s---------------------------------------------"
                   "--------------------------------------------------------\n",
                   " ");
        CliPrintf (CliHandle,
                   "\r%-1s%-2s|%-10d |%-10d |%-10d |%-10d |%-10d |"
                   "%-10d |%-10d |%-10d\n", " ", "Rx ", u4PfcStatsRx0,
                   u4PfcStatsRx1, u4PfcStatsRx2, u4PfcStatsRx3, u4PfcStatsRx4,
                   u4PfcStatsRx5, u4PfcStatsRx6, u4PfcStatsRx7);
        CliPrintf (CliHandle,
                   "\r%-1s---------------------------------------------"
                   "--------------------------------------------------------\n",
                   " ");
        CliPrintf (CliHandle,
                   "\r%-1s%-2s|%-10d |%-10d |%-10d |%-10d |%-10d |"
                   "%-10d |%-10d |%-10d\n", " ", "Tx ", u4PfcStatsTx0,
                   u4PfcStatsTx1, u4PfcStatsTx2, u4PfcStatsTx3, u4PfcStatsTx4,
                   u4PfcStatsTx5, u4PfcStatsTx6, u4PfcStatsTx7);
        CliPrintf (CliHandle, "\r\n");
    }
    return CLI_SUCCESS;
}

/*****************************************************************************
* Function Name     : PFCCliShowPortInfo                                  
* Description       : This function is used to display the                 
*                      PFC Port status                                 
* Input(s)          : CliHandle    - Handle to CLI session     
*                   : i4IfIndex    - Interface Index                  
* Output(s)         : None.                                               
* Global Variables                                                         
* Referred          : None.                                               
* Global Variables                                                          
* Modified          : None.                                                
* Return Value(s)   : CLI_SUCCESS/CLI_FAILURE           
* Called By         : cli_process_dcbx_cmd                         
* Calling Function  : SNMP nmh Routines                         
*****************************************************************************/

PRIVATE INT4
PFCCliShowPortInfo (tCliHandle CliHandle, INT4 i4IfIndex, UINT1 u1Detail)
{
    INT4                i4Port = DCBX_ZERO;
    INT4                i4NextPort = DCBX_ZERO;

    CliPrintf (CliHandle, "\n\r");
    if (i4IfIndex == DCBX_ZERO)
    {
        if (nmhGetFirstIndexFsPFCPortTable (&i4NextPort) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r No PFC Port is Created\n");
            CliPrintf (CliHandle, "\n\r");
            return CLI_SUCCESS;
        }
        if (u1Detail != DCBX_ENABLED)
        {
            CliPrintf (CliHandle, "\r%-9s %-9s\n", "Port", "Status");
            CliPrintf (CliHandle,
                       "\r-----------------------------------------------\n");
        }
        do
        {
            i4Port = i4NextPort;
            PFCCliShowPortDetails (CliHandle, i4Port, u1Detail);
            if (u1Detail == DCBX_ENABLED)
            {
                CliPrintf (CliHandle,
                           "\r-----------------------------------------"
                           "------\n");
                CliPrintf (CliHandle, "\n\r");
            }
        }
        while ((nmhGetNextIndexFsPFCPortTable (i4Port, &i4NextPort)
                == SNMP_SUCCESS));
    }
    else
    {
        if (u1Detail != DCBX_ENABLED)
        {
            CliPrintf (CliHandle, "\r%-9s %-9s\n", "Port", "Status");
            CliPrintf (CliHandle,
                       "\r-----------------------------------------------\n");
            CliPrintf (CliHandle, "\n\r");
        }
        PFCCliShowPortDetails (CliHandle, i4IfIndex, u1Detail);

    }
    return CLI_SUCCESS;
}

/*****************************************************************************
* Function Name     : PFCCliShowPortDetails
* Description       : This function is used to display the                 
*                     PFC Port status                                 
* Input(s)          : CliHandle    - Handle to CLI session     
*                   : i4IfIndex    - Interface Index                  
* Output(s)         : None.                                               
* Global Variables                                                         
* Referred          : None.                                               
* Global Variables                                                          
* Modified          : None.                                                
* Return Value(s)   : CLI_SUCCESS/CLI_FAILURE           
* Called By         : cli_process_dcbx_cmd                         
* Calling Function  : SNMP nmh Routines                         
*****************************************************************************/

PRIVATE INT4
PFCCliShowPortDetails (tCliHandle CliHandle, INT4 i4IfIndex, UINT1 u1Detail)
{

    INT4                i4PFCRowStatus = DCBX_ZERO;
    UINT1               au1NameStr[CFA_MAX_PORT_NAME_LENGTH] = { DCBX_ZERO };
    INT4                i4PFCAdminMode = DCBX_ZERO;
    INT4                i4PFCOperState = DCBX_ZERO;
    INT4                i4PFCStateType = DCBX_ZERO;
    INT4                i4PFCTxStatus = DCBX_ZERO;
    INT4                i4OperVersion = DCBX_VER_UNKNOWN;
    INT4                i4PFCDcbxStatus = DCBX_ZERO;

    CfaCliGetIfName ((UINT4) i4IfIndex, (INT1 *) au1NameStr);
    /*    CliPrintf (CliHandle, "%-9s", au1NameStr); */
    nmhGetFsPFCRowStatus (i4IfIndex, &i4PFCRowStatus);

    if (u1Detail == DCBX_ENABLED)
    {
        if (i4PFCRowStatus == ACTIVE)
        {
            CliPrintf (CliHandle,
                       "\r-----------------------------------------------\n");
            CliPrintf (CliHandle,
                       "\r PFC Port %-9s Information \n", au1NameStr);
            /*    CliPrintf (CliHandle, "PFC is   :%-8s", "Enabled"); */
            CliPrintf (CliHandle,
                       "\r-----------------------------------------------\n");
            CliPrintf (CliHandle, "\r PFC  Local Port Info \n");
            CliPrintf (CliHandle,
                       "\r-----------------------------------------------\n");
            PFCCliShowLocalPortInfo (CliHandle, i4IfIndex);
            CliPrintf (CliHandle,
                       "\r------------------------------------------------\n");
            CliPrintf (CliHandle, "\r PFC Admin Port Info \n");
            CliPrintf (CliHandle,
                       "\r-----------------------------------------------\n");
            PFCCliShowAdmPortInfo (CliHandle, i4IfIndex);
            CliPrintf (CliHandle,
                       "\r-----------------------------------------------\n");
            CliPrintf (CliHandle, "\r PFC Remote Port Info \n");
            CliPrintf (CliHandle,
                       "\r-----------------------------------------------\n");
            PFCCliShowRemPortInfo (CliHandle, i4IfIndex);
            CliPrintf (CliHandle,
                       "\r-----------------------------------------------\n");
            CliPrintf (CliHandle, "\r PFC Port Related Info \n");
            CliPrintf (CliHandle,
                       "\r-----------------------------------------------\n");
            nmhGetLldpXdot1dcbxConfigPFCTxEnable (i4IfIndex,
                                                  DCBX_DEST_ADDR_INDEX,
                                                  &i4PFCTxStatus);
            CliPrintf (CliHandle, "\r%-27s:", "PFC TLV Tx and Rx Status");
            if (i4PFCTxStatus == DCBX_ENABLED)
            {
                CliPrintf (CliHandle, "Enabled\n");

            }
            else
            {
                CliPrintf (CliHandle, "Disabled\n");
            }

            CliPrintf (CliHandle, "\n\r");

            nmhGetFsPFCDcbxStatus (i4IfIndex, &i4PFCDcbxStatus);
            if (i4PFCDcbxStatus > DCBX_ZERO)
            {
                CliPrintf (CliHandle, "\r%-27s:%s\n", "PFC DCBX Status",
                           au1DcbxStatus[i4PFCDcbxStatus - 1]);
            }

            CliPrintf (CliHandle, "\n\r");

            nmhGetFsPFCAdminMode (i4IfIndex, &i4PFCAdminMode);
            nmhGetFsPFCDcbxOperState (i4IfIndex, &i4PFCOperState);
            nmhGetFsPFCDcbxStateMachine (i4IfIndex, &i4PFCStateType);
            CliPrintf (CliHandle, "\r%-27s:", "PFC Port Mode");
            if (i4PFCAdminMode == DCBX_ADM_MODE_AUTO)
            {
                CliPrintf (CliHandle, "AUTO MODE \n");
            }
            if (i4PFCAdminMode == DCBX_ADM_MODE_ON)
            {
                CliPrintf (CliHandle, "ON MODE \n");
            }
            if (i4PFCAdminMode == DCBX_ADM_MODE_OFF)
            {
                CliPrintf (CliHandle, "OFF MODE \n");
            }

            nmhGetFsDcbOperVersion (i4IfIndex, &i4OperVersion);
            if (i4OperVersion == DCBX_MODE_CEE)
            {
                CliPrintf (CliHandle, "\r%-27s:", "PFC Oper State");
                if (i4PFCOperState == CEE_OPER_DISABLED)
                {
                    CliPrintf (CliHandle, "DISABLED\n");
                }
                if (i4PFCOperState == CEE_OPER_USE_LOCAL_CFG)
                {
                    CliPrintf (CliHandle, "USE LOCAL CFG\n");
                }
                if (i4PFCOperState == CEE_OPER_USE_PEER_CFG)
                {
                    CliPrintf (CliHandle, "USE PEER CFG\n");
                }
            }
            else
            {
                CliPrintf (CliHandle, "\r%-27s:", "PFC Oper State");
                if (i4PFCOperState == DCBX_OPER_OFF)
                {
                    CliPrintf (CliHandle, "OFF STATE\n");
                }
                if (i4PFCOperState == DCBX_OPER_INIT)
                {
                    CliPrintf (CliHandle, "INIT STATE\n");
                }
                if (i4PFCOperState == DCBX_OPER_RECO)
                {
                    CliPrintf (CliHandle, "RECO STATE\n");
                }
            }
            CliPrintf (CliHandle, "\r%-27s:", "PFC State Machine Type");
            if (i4PFCStateType == DCBX_ASYM_TYPE)
            {
                CliPrintf (CliHandle, "Asymetric\n");
                CliPrintf (CliHandle,
                           "\r-----------------------------------------------\n");
                CliPrintf (CliHandle, "\n\r");
            }
            if (i4PFCStateType == DCBX_SYM_TYPE)
            {
                CliPrintf (CliHandle, "Symmetric\n");
                CliPrintf (CliHandle,
                           "\r-----------------------------------------------\n");
                CliPrintf (CliHandle, "\n\r");
            }
            if (i4PFCStateType == DCBX_FEAT_TYPE)
            {
                CliPrintf (CliHandle, "Feature\n");
                CliPrintf (CliHandle,
                           "\r-----------------------------------------------\n");
                CliPrintf (CliHandle, "\n\r");
            }
        }
        else
        {
            CliPrintf (CliHandle, "\r%-9s %-9s\n", "Port", "Status");
            CliPrintf (CliHandle,
                       "\r-----------------------------------------------\n");
            CliPrintf (CliHandle, "\r\n%-9s %-8s\r\n", au1NameStr, "Disabled");
            CliPrintf (CliHandle,
                       "\r\n-----------------------------------------------\n");
            CliPrintf (CliHandle, "\n\r");
        }
    }
    else
    {
        if (i4PFCRowStatus == ACTIVE)
        {
            CliPrintf (CliHandle, "\r%-9s %-8s\r\n", au1NameStr, "Enabled");
            CliPrintf (CliHandle,
                       "\r-----------------------------------------------\n");
            CliPrintf (CliHandle, "\n\r");
        }
        else
        {
            CliPrintf (CliHandle, "\r%-9s %-8s\r\n", au1NameStr, "Disabled");
            CliPrintf (CliHandle,
                       "\r\n-----------------------------------------------\n");
            CliPrintf (CliHandle, "\n\r");
        }
    }

    CliPrintf (CliHandle, "\n\r");
    return CLI_SUCCESS;
}

/*****************************************************************************
* Function Name     : PFCCliShowLocalPortInfo                                  
* Description       : This function is used to display the PFC Local Port status                                 
* Input(s)          : CliHandle    - Handle to CLI session     
*                       : pPFCportEntry    - pointer to PFC table 
                : i4IfIndex -     Interface Index  
* Output(s)         : None.                                               
* Global Variables                                                         
* Referred          : None.                                               
* Global Variables                                                          
* Modified          : None.                                                
* Return Value(s)   : CLI_SUCCESS/CLI_FAILURE           
* Called By         : PFCCliShowPortInfo                                                
*****************************************************************************/
PRIVATE INT4
PFCCliShowLocalPortInfo (tCliHandle CliHandle, INT4 i4IfIndex)
{

    INT4                i4PFCLocWilling = DCBX_ZERO;
    UINT4               u4PFCLocCap = DCBX_ZERO;
    INT4                i4PFCLocMBC = DCBX_ZERO;
    UINT4               u4PriIdx = DCBX_ZERO;
    INT4                i4PfcStatus = DCBX_ZERO;

    nmhGetLldpXdot1dcbxLocPFCWilling (i4IfIndex, &i4PFCLocWilling);
    nmhGetLldpXdot1dcbxLocPFCCap (i4IfIndex, &u4PFCLocCap);
    nmhGetLldpXdot1dcbxLocPFCMBC (i4IfIndex, &i4PFCLocMBC);
    CliPrintf (CliHandle, "\r%-25s:", "PFC Willing Status");
    if (i4PFCLocWilling == PFC_ENABLED)
    {
        CliPrintf (CliHandle, "%-8s\r\n", "Enabled");
    }
    else
    {
        CliPrintf (CliHandle, "%-8s\r\n", "Disabled");
    }

    CliPrintf (CliHandle, "\r%-25s:", "PFC MBC");
    if (i4PFCLocMBC == DCBX_ENABLED)
    {
        CliPrintf (CliHandle, "%-8s\r\n", "Enabled");
    }
    else
    {
        CliPrintf (CliHandle, "%-8s\r\n", "Disabled");
    }
    CliPrintf (CliHandle, "\r%-25s:%d\r\n", "PFC Capability", u4PFCLocCap);
    CliPrintf (CliHandle, "\n");

    CliPrintf (CliHandle, "\r%-25s:", "Priority Enabled List");
    for (u4PriIdx = DCBX_ZERO; u4PriIdx < DCBX_MAX_PRIORITIES; u4PriIdx++)
    {
        nmhGetLldpXdot1dcbxLocPFCEnableEnabled (i4IfIndex, u4PriIdx,
                                                &i4PfcStatus);
        if (i4PfcStatus == PFC_ENABLED)
        {
            CliPrintf (CliHandle, "%d ", u4PriIdx);
        }
    }
    CliPrintf (CliHandle, "\n\r");
    CliPrintf (CliHandle, "\r%-25s:", "Priority Disabled List");
    for (u4PriIdx = DCBX_ZERO; u4PriIdx < DCBX_MAX_PRIORITIES; u4PriIdx++)
    {
        nmhGetLldpXdot1dcbxLocPFCEnableEnabled (i4IfIndex, u4PriIdx,
                                                &i4PfcStatus);
        if (i4PfcStatus == PFC_DISABLED)
        {
            CliPrintf (CliHandle, "%d ", u4PriIdx);

        }
    }
    CliPrintf (CliHandle, "\n\r");
    return CLI_SUCCESS;
}

/*****************************************************************************
* Function Name     : PFCCliShowAdmPortInfo                                  
* Description       : This function is used to display the PFC ADMIN status                                 
* Input(s)          : CliHandle    - Handle to CLI session     
*                       : pPFCportEntry    - pointer to PFC table 
                : i4IfIndex -     Interface Index  
* Output(s)         : None.                                               
* Global Variables                                                         
* Referred          : None.                                               
* Global Variables                                                          
* Modified          : None.                                                
* Return Value(s)   : CLI_SUCCESS/CLI_FAILURE           
* Called By         : PFCCliShowPortInfo                                                
*****************************************************************************/
PRIVATE INT4
PFCCliShowAdmPortInfo (tCliHandle CliHandle, INT4 i4IfIndex)
{
    INT4                i4PFCAdminWilling = DCBX_ZERO;
    UINT4               u4PFCAdminCap = DCBX_ZERO;
    INT4                i4PFCAdminMBC = DCBX_ZERO;
    UINT4               u4PriIdx = DCBX_ZERO;
    INT4                i4PfcStatus = DCBX_ZERO;

    nmhGetLldpXdot1dcbxAdminPFCWilling (i4IfIndex, &i4PFCAdminWilling);
    nmhGetLldpXdot1dcbxAdminPFCCap (i4IfIndex, &u4PFCAdminCap);
    nmhGetLldpXdot1dcbxAdminPFCMBC (i4IfIndex, &i4PFCAdminMBC);
    CliPrintf (CliHandle, "\r%-25s:", "PFC Willing Status");
    if (i4PFCAdminWilling == PFC_ENABLED)
    {
        CliPrintf (CliHandle, "%-8s", "Enabled \r\n");
    }
    else
    {
        CliPrintf (CliHandle, "%-8s", "Disabled \r\n");
    }

    CliPrintf (CliHandle, "\r%-25s:", "PFC MBC");
    if (i4PFCAdminMBC == DCBX_ENABLED)
    {
        CliPrintf (CliHandle, "%-8s", "Enabled \r\n");
    }
    else
    {
        CliPrintf (CliHandle, "%-8s", "Disabled \r\n");
    }
    CliPrintf (CliHandle, "\r%-25s:%d\n", "PFC Capability", u4PFCAdminCap);
    CliPrintf (CliHandle, "\n");

    CliPrintf (CliHandle, "\r%-25s:", "Priority Enabled List");
    for (u4PriIdx = DCBX_ZERO; u4PriIdx < DCBX_MAX_PRIORITIES; u4PriIdx++)
    {
        nmhGetLldpXdot1dcbxAdminPFCEnableEnabled (i4IfIndex, u4PriIdx,
                                                  &i4PfcStatus);
        if (i4PfcStatus == PFC_ENABLED)
        {
            CliPrintf (CliHandle, "%d ", u4PriIdx);
        }
    }
    CliPrintf (CliHandle, "\n\r");
    CliPrintf (CliHandle, "\r%-25s:", "Priority Disabled List");
    for (u4PriIdx = DCBX_ZERO; u4PriIdx < DCBX_MAX_PRIORITIES; u4PriIdx++)
    {
        nmhGetLldpXdot1dcbxAdminPFCEnableEnabled (i4IfIndex, u4PriIdx,
                                                  &i4PfcStatus);
        if (i4PfcStatus == PFC_DISABLED)
        {
            CliPrintf (CliHandle, "%d ", u4PriIdx);

        }
    }
    CliPrintf (CliHandle, "\n\r");
    return CLI_SUCCESS;
}

/*****************************************************************************
* Function Name     : PFCCliShowRemPortInfo                                  
* Description       : This function is used to display the PFC Remote status                                 
* Input(s)          : CliHandle    - Handle to CLI session     
*                       : pPFCportEntry    - pointer to PFC table 
                : i4IfIndex -     Interface Index  
* Output(s)         : None.                                               
* Global Variables                                                         
* Referred          : None.                                               
* Global Variables                                                          
* Modified          : None.                                                
* Return Value(s)   : CLI_SUCCESS/CLI_FAILURE           
* Called By         : PFCCliShowPortInfo                                                
*****************************************************************************/
PRIVATE INT4
PFCCliShowRemPortInfo (tCliHandle CliHandle, INT4 i4IfIndex)
{
    UINT4               u4LldpV2RemLocalDestMACAddress;
    UINT4               u4LldpV2RemTimeMark = DCBX_ZERO;
    INT4                i4LldpV2RemLocalIfIndex = DCBX_ZERO;
    INT4                i4LldpV2RemIndex = DCBX_ZERO;
    INT4                i4PFCRemWilling = DCBX_ZERO;
    UINT4               u4PFCRemCap = DCBX_ZERO;
    INT4                i4PFCRemMBC = DCBX_ZERO;
    UINT4               u4PriIdx = DCBX_ZERO;
    INT4                i4PfcStatus = DCBX_ZERO;

    nmhGetNextIndexLldpXdot1dcbxRemPFCBasicTable (DCBX_ZERO,
                                                  &u4LldpV2RemTimeMark,
                                                  i4IfIndex,
                                                  &i4LldpV2RemLocalIfIndex,
                                                  DCBX_ZERO,
                                                  &u4LldpV2RemLocalDestMACAddress,
                                                  DCBX_ZERO, &i4LldpV2RemIndex);

    if ((i4IfIndex != i4LldpV2RemLocalIfIndex) ||
        (i4LldpV2RemIndex == DCBX_ZERO))
    {
        CliPrintf (CliHandle, "\rNo Remote Entry is Present\n\r");
        return (CLI_SUCCESS);
    }

    nmhGetLldpXdot1dcbxRemPFCWilling (u4LldpV2RemTimeMark,
                                      i4IfIndex, u4LldpV2RemLocalDestMACAddress,
                                      i4LldpV2RemIndex, &i4PFCRemWilling);
    nmhGetLldpXdot1dcbxRemPFCCap (u4LldpV2RemTimeMark,
                                  i4IfIndex, u4LldpV2RemLocalDestMACAddress,
                                  i4LldpV2RemIndex, &u4PFCRemCap);
    nmhGetLldpXdot1dcbxRemPFCMBC (u4LldpV2RemTimeMark,
                                  i4IfIndex, u4LldpV2RemLocalDestMACAddress,
                                  i4LldpV2RemIndex, &i4PFCRemMBC);
    CliPrintf (CliHandle, "\r%-25s:", "PFC Willing Status");
    if (i4PFCRemWilling == PFC_ENABLED)
    {
        CliPrintf (CliHandle, "%-8s", "Enabled\r\n");
    }
    else
    {
        CliPrintf (CliHandle, "%-8s", "Disabled\r\n");
    }

    CliPrintf (CliHandle, "\r%-25s:", "PFC MBC");
    if (i4PFCRemMBC == DCBX_ENABLED)
    {
        CliPrintf (CliHandle, "%-8s", "Enabled\r\n");
    }
    else
    {
        CliPrintf (CliHandle, "%-8s", "Disabled\r\n");
    }
    CliPrintf (CliHandle, "\r%-25s:%d\n", "PFC Capability", u4PFCRemCap);

    CliPrintf (CliHandle, "\n");
    CliPrintf (CliHandle, "\r%-25s:", "Priority Enabled List");
    for (u4PriIdx = DCBX_ZERO; u4PriIdx < DCBX_MAX_PRIORITIES; u4PriIdx++)
    {
        nmhGetLldpXdot1dcbxRemPFCEnableEnabled (u4LldpV2RemTimeMark,
                                                i4IfIndex,
                                                u4LldpV2RemLocalDestMACAddress,
                                                i4LldpV2RemIndex, u4PriIdx,
                                                &i4PfcStatus);
        if (i4PfcStatus == PFC_ENABLED)
        {
            CliPrintf (CliHandle, "%d ", u4PriIdx);
        }
    }
    CliPrintf (CliHandle, "\n\r");
    CliPrintf (CliHandle, "\r%-25s:", "Priority Disabled List");
    for (u4PriIdx = DCBX_ZERO; u4PriIdx < DCBX_MAX_PRIORITIES; u4PriIdx++)
    {
        nmhGetLldpXdot1dcbxRemPFCEnableEnabled (u4LldpV2RemTimeMark,
                                                i4IfIndex,
                                                u4LldpV2RemLocalDestMACAddress,
                                                i4LldpV2RemIndex, u4PriIdx,
                                                &i4PfcStatus);
        if (i4PfcStatus == PFC_DISABLED)
        {
            CliPrintf (CliHandle, "%d ", u4PriIdx);

        }
    }
    CliPrintf (CliHandle, "\n\r");
    return CLI_SUCCESS;
}

/*****************************************************************************
 Function Name      : PFCCliShowGblInfo                 
 Description        : This function is used to display the
                          PFC Global Information                 
 Input(s)           : CliHandle    - Handle to CLI session
 Output(s)          : None.                                          
 Global Variables                                                     
 Referred           : None.                                          
 Global Variables                                                     
 Modified           : None.                                           
 Return Value(s)    : CLI_SUCCESS/CLI_FAILURE      
 Called By          : cli_process_dcbx_cmd                  
 Calling Function   : SNMP nmh Routines                    
*****************************************************************************/
PRIVATE INT4
PFCCliShowGblInfo (tCliHandle CliHandle)
{
    UINT4               u4PfcThreshold = DCBX_ZERO;
    UINT4               u4PfcMaxProfile = DCBX_ZERO;

    CliPrintf (CliHandle, "\n\r");
    CliPrintf (CliHandle, "\r Priority Flow Control GLOBAL Info \n");
    CliPrintf (CliHandle, "\r-----------------------------------\n");
    /*Showing  Min & MAX Threshold Vaues */
    nmhGetFsDcbPfcMinThreshold (&u4PfcThreshold);
    CliPrintf (CliHandle, "\r PFC Minimum ThreShold Value is:%d\n",
               u4PfcThreshold);
    nmhGetFsDcbPfcMaxThreshold (&u4PfcThreshold);
    CliPrintf (CliHandle, "\r PFC Maximum ThreShold Value is:%d\n",
               u4PfcThreshold);
    /*Number of Profiles Supported for PFC */
    nmhGetFsDcbMaxPfcProfiles (&u4PfcMaxProfile);
    CliPrintf (CliHandle,
               "\r PFC Maximum Profile Supported by the system is:%d\n",
               u4PfcMaxProfile);
    CliPrintf (CliHandle, "\n\r");
    return CLI_SUCCESS;

}

/************************************************************************
* Function Name     : PFCCliShowGbl
* Description       : This function is used to display the PFC Global
* Information
* Input(s)          : CliHandle    - Handle to CLI session
* Output(s)         : None.
* Global Variables
* Referred          : None.
* Global Variables
* Modified          : None.
* Return Value(s)   : CLI_SUCCESS/CLI_FAILURE
*************************************************************************/
PRIVATE INT4
PFCCliShowGbl (tCliHandle CliHandle)
{
    UINT4               u4TrapCount = DCBX_ZERO;
    INT4                i4SysStatus = DCBX_ZERO;
    INT4                i4ModStatus = DCBX_ZERO;
    INT4                i4PFCTrapEnabled = DCBX_ZERO;

    CliPrintf (CliHandle, "\n\r");
    CliPrintf (CliHandle, "\r Priority Flow Control GLOBAL Info \n");
    CliPrintf (CliHandle, "\r-----------------------------------\n");
    /*Showing  Min & MAX Threshold Vaues */
    nmhGetFsPFCSystemControl (&i4SysStatus);
    nmhGetFsPFCModuleStatus (&i4ModStatus);
    nmhGetFsPFCGlobalEnableTrap (&i4PFCTrapEnabled);
    nmhGetFsPFCGeneratedTrapCount (&u4TrapCount);
    if (i4SysStatus == PFC_START)
    {
        CliPrintf (CliHandle, "\r PFC System control status : Start\n");
    }
    else
    {
        CliPrintf (CliHandle, "\r PFC System control status : Shutdown\n");
    }

    if (i4ModStatus == PFC_ENABLED)
    {
        CliPrintf (CliHandle, "\r PFC Module status : Enabled\n");
    }
    else
    {
        CliPrintf (CliHandle, "\r PFC Module status : Disabled\n");
    }
    CliPrintf (CliHandle, "\r PFC Trap Count            : %d\n", u4TrapCount);

    CliPrintf (CliHandle, "\r Trap(s) Enabled:\n");
    CliPrintf (CliHandle, "\r ----------------\n");

    if ((i4PFCTrapEnabled & PFC_ALL_TRAP) == PFC_ALL_TRAP)
    {
        CliPrintf (CliHandle, "\r All\n");
    }
    else if (i4PFCTrapEnabled == DCBX_ZERO)
    {
        CliPrintf (CliHandle, "\r None\n");
    }
    else
    {
        if ((i4PFCTrapEnabled & PFC_MODULE_STATUS_TRAP) ==
            PFC_MODULE_STATUS_TRAP)
        {
            CliPrintf (CliHandle, "\r ModuleStatusChange\n");
        }
        if ((i4PFCTrapEnabled & PFC_ADMIN_MODE_TRAP) == PFC_ADMIN_MODE_TRAP)
        {
            CliPrintf (CliHandle, "\r AdminModeChange\n");
        }
        if ((i4PFCTrapEnabled & PFC_PEER_STATUS_TRAP) == PFC_PEER_STATUS_TRAP)
        {
            CliPrintf (CliHandle, "\r PeerStatusChange\n");
        }
        if ((i4PFCTrapEnabled & PFC_SEM_TRAP) == PFC_SEM_TRAP)
        {
            CliPrintf (CliHandle, "\r SemChange\n");
        }
    }
    CliPrintf (CliHandle, "\n\r");
    return CLI_SUCCESS;

}

/*****************************************************************************
 Function Name      : DcbxCliSetStatus
 Description        :This function will be used to enable/disable the DCBX
                     Admin status on the given  on the given port.  
 Input(s)           : CliHandle    - Handle to CLI session                
                    : i4IfIndex - Port number for which the DCBX admin 
                      status to be set.
                    :u1AdminStatus -Enabled/Disabled.  
 Output(s)          : CLI_SET_ERR will be used in case any errors in the 
                      Test Routine and 
                      CLI_FATAL_ERROR (CliHanlde) will be used in case of any
                      errors in the Set Routine.                                
 Return Value(s)     : CLI_SUCCESS/CLI_FAILURE           
 Called By              : cli_process_dcbx_cmd                         
 Calling Function     : SNMP nmh Routines                         
*****************************************************************************/
PRIVATE INT4
DcbxCliSetStatus (tCliHandle CliHandle, INT4 i4IfIndex, UINT1 u1AdminStatus)
{
    UINT4               u4ErrorCode = SNMP_ERR_NO_ERROR;

    UNUSED_PARAM (CliHandle);
    /* Test the Given Vlaue if it success then Set */
    if (nmhTestv2FsDCBXAdminStatus (&u4ErrorCode,
                                    i4IfIndex, u1AdminStatus) != SNMP_SUCCESS)
    {
        return (CLI_FAILURE);
    }

    nmhSetFsDCBXAdminStatus (i4IfIndex, u1AdminStatus);
    return (CLI_SUCCESS);

}

/*****************************************************************************
 Function Name      : DCBXCliSetDebugStatus                             
 Description        :This function will be used to Enable/disable the DCBX
                     Debug traces on the type specified.
 Input(s)              : CliHandle    - Handle to CLI session      
                       : i4TraceType - DCBX Trace types for which it should be
                      enabled/disabled.
                    : u1Status - enable/disable status for DCBX module.
 r
 Output(s)          : CLI_SET_ERR will be used in case any errors in the Test
                      Routine and 
                      CLI_FATAL_ERROR (CliHanlde) will be used in case of
                      any errors in the Set Routine.
 Return Value(s) : CLI_SUCCESS/CLI_FAILURE           
 Called By          : cli_process_dcbx_cmd                         
*****************************************************************************/

PRIVATE INT4
DCBXCliSetDebugStatus (tCliHandle CliHandle, INT4 i4TraceType, UINT1 u1Status)
{
    UINT4               u4ErrorCode = DCBX_ZERO;
    INT4                i4OriginalTrace = DCBX_ZERO;

    UNUSED_PARAM (CliHandle);
    nmhGetFsDcbxGlobalTraceLevel (&i4OriginalTrace);
    /* if the Status is to disable Debug status */
    if (u1Status == DCBX_DISABLED)
    {
        i4TraceType = i4OriginalTrace & (~i4TraceType);
    }
    else
    {
        i4TraceType = i4OriginalTrace | i4TraceType;
    }

    if (nmhTestv2FsDcbxGlobalTraceLevel (&u4ErrorCode, i4TraceType) !=
        SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }
    nmhSetFsDcbxGlobalTraceLevel (i4TraceType);
    return CLI_SUCCESS;
}

/***************************************************************************
   Function Name : DcbxConvertStrToPriArray                           
   Description   : This function is invoked to convert the given       
                   string to array of priority                  
   Input(s)      : pi1Str          : Pointer to the string.            
                   pi1Delimit      : Pointer to the delimiter string   
                   pi1ValidDelimit : Pointer to the valid characters   
                                        that can be present in pi1Str     
   Output(s)     : NULL                                                
   Returns       : pointer to any of the delimiter char in the given   
                   input string, if any of the delimiter char from the 
                   delimiter string is present, or NULL                
 ***************************************************************************/
PRIVATE INT4
DcbxConvertStrToPriArray (tCliHandle CliHandle, UINT1 *pu1Str,
                          INT4 *pi4PriArray)
{
    INT1               *pi1Temp = (INT1 *) pu1Str;
    INT1               *pi1Pos = NULL;
    INT1               *pi1Tempptr = NULL;
    UINT4               u4Count = DCBX_ZERO;
    UINT1               u1Flag = DCBX_ZERO;
    INT4                i4Index = DCBX_ZERO;
    INT4                i4CurVal = DCBX_ZERO;
    UINT1               u1PriArray[DCBX_MAX_PRIORITIES] = { 0 };
    UINT1              *pu1Array = NULL;
    INT1                i1Result = DCBX_ZERO;

    UNUSED_PARAM (CliHandle);

    if (pi1Temp == NULL)
    {
        return CLI_FAILURE;
    }
    pu1Array = u1PriArray;
    MEMSET (pu1Array, DCBX_ZERO, DCBX_MAX_PRIORITIES);
    pi1Tempptr = pi1Temp;
    while (*pi1Tempptr)
    {
        if ((*pi1Tempptr == ',') || (*pi1Tempptr == '-'))
        {
            if (pi1Tempptr == (INT1 *) pu1Str)
            {
                CliPrintf (CliHandle, "\r%% Invalid Priority input"
                           " Priority input should be in the range <0-7>\n");
                return CLI_FAILURE;
            }
            if ((pi1Tempptr + 1) != NULL)
            {
                if ((*(pi1Tempptr + 1) == ',') ||
                    (*(pi1Tempptr + 1) == '-') || (*(pi1Tempptr + 1) == 0))
                {
                    CliPrintf (CliHandle, "\r%% Invalid Priority input"
                               " Priority input should be in the range <0-7>\n");
                    return CLI_FAILURE;
                }
            }
        }
        pi1Pos = pi1Temp;
        /* Check for priority list seperater delimiter */
        if ((pi1Pos != NULL) && (*pi1Pos != ','))
        {
            if ((ISDIGIT (*pi1Pos)) != DCBX_ZERO)
            {
                i4CurVal = ATOI (pi1Pos);
                if ((i4CurVal < DCBX_ZERO) || (i4CurVal >= DCBX_MAX_PRIORITIES))
                {
                    CliPrintf (CliHandle, "\rInput limit exceeded. Please"
                               " enter an input in the range <0-7>\n");
                    return CLI_FAILURE;
                }
                if (u1Flag == DCBX_ONE)
                {
                    for (i4Index = ((pi4PriArray[u4Count]) + 1);
                         i4Index <= i4CurVal; i4Index++)
                    {
                        pi4PriArray[u4Count] = i4Index;
                        u4Count++;
                        OSIX_BITLIST_IS_BIT_SET (pu1Array, i4Index,
                                                 DCBX_MAX_PRIORITIES, i1Result);
                        if (i1Result == OSIX_TRUE)
                        {
                            CLI_SET_ERR (CLI_ERR_DCBX_SAME_PRIOIRTY);
                            return CLI_FAILURE;
                        }
                        else
                        {
                            OSIX_BITLIST_SET_BIT (pu1Array, i4Index,
                                                  DCBX_MAX_PRIORITIES);
                        }
                    }
                    u1Flag = DCBX_ZERO;
                }
                else
                {
                    pi4PriArray[u4Count] = i4CurVal;
                    u4Count++;
                    if (i4CurVal == DCBX_ZERO)
                    {
                        if (pu1Array[DCBX_ZERO] == DCBX_ONE)
                        {
                            i1Result = OSIX_TRUE;
                        }
                    }
                    else
                    {
                        OSIX_BITLIST_IS_BIT_SET (pu1Array, i4CurVal,
                                                 DCBX_MAX_PRIORITIES, i1Result);
                    }
                    if (i1Result == OSIX_TRUE)
                    {
                        CLI_SET_ERR (CLI_ERR_DCBX_SAME_PRIOIRTY);
                        return CLI_FAILURE;
                    }
                    else
                    {
                        if (i4CurVal == DCBX_ZERO)
                        {
                            pu1Array[DCBX_ZERO] = DCBX_ONE;
                        }
                        else
                        {
                            OSIX_BITLIST_SET_BIT (pu1Array, i4CurVal,
                                                  DCBX_MAX_PRIORITIES);
                        }
                    }
                }
            }
            else if ((*pi1Pos) == '-')
            {
                u1Flag = DCBX_ONE;
            }
            else
            {

                CliPrintf (CliHandle, "\r%% Invalid Priority input"
                           " Priority input should be in the range <0-7>\n");
                return CLI_FAILURE;
            }
        }
        pi1Temp = pi1Pos + DCBX_ONE;
        pi1Tempptr++;
    }
    return CLI_SUCCESS;

}

/*****************************************************************************
 Function Name      : DCBXCliShowPortInfo
 Description        : This function is used to display the
                          DCBX Port Information
 Input(s)           : CliHandle    - Handle to CLI session
 Output(s)          : None.
 Global Variables
 Referred           : None.
 Global Variables
 Modified           : None.
 Return Value(s)    : CLI_SUCCESS/CLI_FAILURE
 Called By          : cli_process_dcbx_cmd
 Calling Function   : SNMP nmh Routines
*****************************************************************************/

PRIVATE INT4
DCBXCliShowPortInfo (tCliHandle CliHandle)
{
    INT4                i4Port = DCBX_ZERO;
    INT4                i4NextPort = DCBX_ZERO;
    UINT1               au1NameStr[CFA_MAX_PORT_NAME_LENGTH] = { DCBX_ZERO };
    INT4                i4AdminStatus = DCBX_ZERO;
    INT4                i4DcbxMode = DCBX_ZERO;

    CliPrintf (CliHandle, "\n\r");

    if (nmhGetFirstIndexFsDCBXPortTable (&i4NextPort) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r No DCBX Port is Created\n");
        CliPrintf (CliHandle, "\n\r");
        return CLI_SUCCESS;
    }
    CliPrintf (CliHandle, "\r %-20s %-14s  %-14s \n", "Port", "Admin Status",
               "DCBX Mode");
    CliPrintf (CliHandle,
               "\r-----------------------------------------------\n\r");
    do
    {
        i4Port = i4NextPort;
        CfaCliGetIfName ((UINT4) i4Port, (INT1 *) au1NameStr);
        nmhGetFsDCBXAdminStatus (i4Port, &i4AdminStatus);
        if (i4AdminStatus == DCBX_ENABLED)
        {
            CliPrintf (CliHandle, " %-20s %-14s ", au1NameStr, "Enabled");
        }
        else
        {
            CliPrintf (CliHandle, " %-20s %-14s ", au1NameStr, "Disabled");

        }

        nmhGetFsDCBXMode (i4Port, &i4DcbxMode);
        if (i4DcbxMode == DCBX_MODE_CEE)
        {
            CliPrintf (CliHandle, " %-14s ", " CEE ");
        }
        else if (i4DcbxMode == DCBX_MODE_IEEE)
        {
            CliPrintf (CliHandle, " %-14s ", " IEEE ");
        }
        else
        {
            CliPrintf (CliHandle, " %-14s ", " AUTO ");
        }
        CliPrintf (CliHandle, "\n\r");
    }
    while ((nmhGetNextIndexFsDCBXPortTable (i4Port, &i4NextPort)
            == SNMP_SUCCESS));
    CliPrintf (CliHandle,
               "\r-----------------------------------------------\n");
    CliPrintf (CliHandle, "\n\r");
    return CLI_SUCCESS;
}

/****************************************************************************
 *                                                                           
 *     FUNCTION NAME    : DCBXCliShowRunningConfig                              
 *                                                                           
 *     DESCRIPTION      : This function displays current Dcbx  configurations   
 *                                                                           
 *     INPUT            : CliHandle - CliContext ID                             
 *                          
 *                                                                           
 *     OUTPUT           : None                                               
 *                                                                           
 *     RETURNS          : None                            
 *                                                                           
 *****************************************************************************/
PUBLIC INT4
DCBXCliShowRunningConfig (tCliHandle CliHandle, INT4 i4Index,
                          INT1 i1DcbxShowInt)
{
    INT4                i4SysControl = DCBX_ZERO;
    INT4                i4SysStatus = DCBX_ZERO;

    CliRegisterLock (CliHandle, DcbxLock, DcbxUnLock);
    DcbxLock ();

    /* If module is shutdown , return without process */
    if (gDcbxGlobalInfo.u1DCBXSystemCtrl == DCBX_SHUTDOWN)
    {
        DcbxUnLock ();
        CliUnRegisterLock (CliHandle);
        return (CLI_SUCCESS);
    }

    CEEShowRunningInterfaceConfig (CliHandle, i4Index, i1DcbxShowInt);

    /* Show running config for ETS */
    nmhGetFsETSSystemControl (&i4SysControl);
    if (i4SysControl != DCBX_SYS_CNTL_START)
    {
        CliPrintf (CliHandle, "shutdown ets \r\n");
        CliPrintf (CliHandle, "!\r\n");
    }
    else
    {
        nmhGetFsETSModuleStatus (&i4SysStatus);
        if (i4SysStatus != DCBX_SYS_STATUS_ENABLE)
        {
            CliPrintf (CliHandle, "ets disable \r\n");
            CliPrintf (CliHandle, "!\r\n");
        }
        ETSShowRunningInterfaceConfig (CliHandle, i4Index, i1DcbxShowInt);
    }
/*  Show running config for PFC*/
    nmhGetFsPFCSystemControl (&i4SysControl);
    if (i4SysControl != DCBX_SYS_CNTL_START)
    {
        CliPrintf (CliHandle, "shutdown pfc \r\n");
        CliPrintf (CliHandle, "!\r\n");
    }
    else
    {
        nmhGetFsPFCModuleStatus (&i4SysStatus);
        if (i4SysStatus != DCBX_SYS_STATUS_ENABLE)
        {
            CliPrintf (CliHandle, "pfc disable \r\n");
            CliPrintf (CliHandle, "!\r\n");
        }
        PFCShowRunningGlobalConfig (CliHandle);
        PFCShowRunningInterfaceConfig (CliHandle, i4Index, i1DcbxShowInt);
    }
/* show running config for Application Priority */
    nmhGetFsAppPriSystemControl (&i4SysControl);
    if (i4SysControl != DCBX_SYS_CNTL_START)
    {
        CliPrintf (CliHandle, "shutdown application-priority \r\n");
        CliPrintf (CliHandle, "!\r\n");
    }
    else
    {
        nmhGetFsAppPriModuleStatus (&i4SysStatus);
        if (i4SysStatus != DCBX_SYS_STATUS_ENABLE)
        {
            CliPrintf (CliHandle, "application-priority disable \r\n");
            CliPrintf (CliHandle, "!\r\n");
        }
        AppPriShowRunningInterfaceConfig (CliHandle, i4Index, i1DcbxShowInt);
    }

    DcbxUnLock ();
    CliUnRegisterLock (CliHandle);
    return (CLI_SUCCESS);
}

/****************************************************************************
 *                                                                           
 *     FUNCTION NAME    : ETSShowRunningInterfaceConfig                              
 *                                                                           
 *     DESCRIPTION      : This function displays current ETS  configurations    
 *                                                                           
 *     INPUT            : CliHandle - CliContext ID                             
 *                          
 *                                                                           
 *     OUTPUT           : None                                               
 *                                                                           
 *     RETURNS          : None                            
 *                                                                           
 *****************************************************************************/
PRIVATE INT4
ETSShowRunningInterfaceConfig (tCliHandle CliHandle, INT4 i4IntIndex,
                               INT1 i1DcbxShowInt)
{
    INT4                i4ETSAdminStatus = DCBX_ZERO;
    INT4                i4ETSAdminMode = DCBX_ZERO;
    INT4                i4ETSWilling = DCBX_ZERO;
    INT4                i4IfIndex = DCBX_ZERO;
    INT4                i4NextIfIndex = DCBX_ZERO;
    INT4                i4Priority = DCBX_ZERO;
    INT4                i4EtsTCGID = DCBX_ZERO;
    INT4                i4ETSTLVStatus = DCBX_ZERO;
    UINT4               au4EtsBW[DCBX_MAX_PRIORITIES] = { DCBX_ZERO };
    UINT1               au1IfName[CFA_MAX_PORT_NAME_LENGTH] = { DCBX_ZERO };
    UINT4               u4Index = DCBX_ZERO;
    INT4                i4EtsConTsa = DCBX_ZERO;
    INT4                i4EtsRecoTsa = DCBX_ZERO;

    if (nmhGetFirstIndexFsETSPortTable (&i4NextIfIndex) != SNMP_FAILURE)
    {
        do
        {
            i4IfIndex = i4NextIfIndex;

            /* Condition for showing DCBX detail on particular interface */
            if ((i1DcbxShowInt == OSIX_TRUE) && (i4IfIndex != i4IntIndex))
            {
                continue;
            }

            MEMSET (&au1IfName[DCBX_ZERO], DCBX_ZERO, CFA_MAX_PORT_NAME_LENGTH);
            CfaCliConfGetIfName ((UINT4) i4IfIndex,
                                 (INT1 *) &au1IfName[DCBX_ZERO]);
            CliPrintf (CliHandle, "interface %s\r\n", au1IfName);

            nmhGetFsETSRowStatus (i4IfIndex, &i4ETSAdminStatus);
            if (i4ETSAdminStatus == ACTIVE)
            {
                CliPrintf (CliHandle, "set priority-grouping enable \r\n");
            }

            nmhGetLldpXdot1dcbxLocETSConWilling (i4IfIndex, &i4ETSWilling);
            if (i4ETSWilling == ETS_ENABLED)
            {
                CliPrintf (CliHandle, "set priority-group willing enable \r\n");
            }

            nmhGetFsETSAdminMode (i4IfIndex, &i4ETSAdminMode);
            if (i4ETSAdminMode == DCBX_ADM_MODE_AUTO)
            {
                CliPrintf (CliHandle, "priority-grouping mode auto \r\n");
            }
            else if (i4ETSAdminMode == DCBX_ADM_MODE_ON)
            {
                CliPrintf (CliHandle, "priority-grouping mode on \r\n");
            }
            else
            {
                CliPrintf (CliHandle, "no priority-grouping mode  \r\n");
            }

            CliPrintf (CliHandle, "\r\n");
            for (i4Priority = DCBX_ZERO; i4Priority < DCBX_MAX_PRIORITIES;
                 i4Priority++)
            {
                nmhGetLldpXdot1dcbxAdminETSConPriTrafficClass (i4IfIndex,
                                                               (UINT4)
                                                               i4Priority,
                                                               (UINT4 *)
                                                               &i4EtsTCGID);
                if (i4EtsTCGID != DCBX_ZERO)
                {
                    CliPrintf (CliHandle,
                               "map priority %d priority-group %d\r\n",
                               i4Priority, i4EtsTCGID);
                }
            }

            for (u4Index = DCBX_ZERO; u4Index < ETS_MAX_TCGID_CONF; u4Index++)
            {
                nmhGetLldpXdot1dcbxAdminETSConTrafficClassBandwidth (i4IfIndex,
                                                                     u4Index,
                                                                     &(au4EtsBW
                                                                       [u4Index]));
            }

            if ((au4EtsBW[ETS_TCGID0] != ETS_MAX_BW) ||
                (au4EtsBW[ETS_TCGID1] != DCBX_ZERO) ||
                (au4EtsBW[ETS_TCGID2] != DCBX_ZERO) ||
                (au4EtsBW[ETS_TCGID3] != DCBX_ZERO) ||
                (au4EtsBW[ETS_TCGID4] != DCBX_ZERO) ||
                (au4EtsBW[ETS_TCGID5] != DCBX_ZERO) ||
                (au4EtsBW[ETS_TCGID6] != DCBX_ZERO) ||
                (au4EtsBW[ETS_TCGID7] != DCBX_ZERO))
            {
                CliPrintf (CliHandle, "set priority-group bandwidth  "
                           " %d %d %d %d %d %d %d %d\r\n",
                           au4EtsBW[ETS_TCGID0],
                           au4EtsBW[ETS_TCGID1],
                           au4EtsBW[ETS_TCGID2],
                           au4EtsBW[ETS_TCGID3],
                           au4EtsBW[ETS_TCGID4],
                           au4EtsBW[ETS_TCGID5],
                           au4EtsBW[ETS_TCGID6], au4EtsBW[ETS_TCGID7]);
            }

            CliPrintf (CliHandle, "\r\n");

            for (u4Index = DCBX_ZERO; u4Index < ETS_MAX_TCGID_CONF; u4Index++)
            {
                nmhGetLldpXdot1dcbxAdminETSRecoTrafficClassBandwidth (i4IfIndex,
                                                                      u4Index,
                                                                      &(au4EtsBW
                                                                        [u4Index]));
            }

            if ((au4EtsBW[ETS_TCGID0] != ETS_MAX_BW) ||
                (au4EtsBW[ETS_TCGID1] != DCBX_ZERO) ||
                (au4EtsBW[ETS_TCGID2] != DCBX_ZERO) ||
                (au4EtsBW[ETS_TCGID3] != DCBX_ZERO) ||
                (au4EtsBW[ETS_TCGID4] != DCBX_ZERO) ||
                (au4EtsBW[ETS_TCGID5] != DCBX_ZERO) ||
                (au4EtsBW[ETS_TCGID6] != DCBX_ZERO) ||
                (au4EtsBW[ETS_TCGID7] != DCBX_ZERO))
            {
                CliPrintf (CliHandle, "set priority-group reco bandwidth  "
                           "%d %d %d %d %d %d %d %d\r\n",
                           au4EtsBW[ETS_TCGID0],
                           au4EtsBW[ETS_TCGID1],
                           au4EtsBW[ETS_TCGID2],
                           au4EtsBW[ETS_TCGID3],
                           au4EtsBW[ETS_TCGID4],
                           au4EtsBW[ETS_TCGID5],
                           au4EtsBW[ETS_TCGID6], au4EtsBW[ETS_TCGID7]);
            }

            CliPrintf (CliHandle, "\r\n");

            for (u4Index = DCBX_ZERO; u4Index < ETS_MAX_TCGID_CONF; u4Index++)
            {
                nmhGetLldpXdot1dcbxAdminETSConTrafficSelectionAlgorithm
                    (i4IfIndex, u4Index, &i4EtsConTsa);
                if (i4EtsConTsa == DCBX_STRICT_PRIORITY_ALGO)
                {
                    CliPrintf (CliHandle,
                               "set traffic class %d traffic selection algorithm"
                               " strict-priority-algorithm \r\n", u4Index);
                }
                else if (i4EtsConTsa == DCBX_CREDIT_BASED_SHAPER_ALGO)
                {
                    CliPrintf (CliHandle,
                               "set traffic class %d traffic selection algorithm"
                               " credit-based-shaper-algorithm \r\n", u4Index);
                }
                else if (i4EtsConTsa ==
                         DCBX_ENHANCED_TRANSMISSION_SELECTION_ALGO)
                {
                    /* Not required to print, as this is the default case */
                    /* CliPrintf (CliHandle, "set traffic class %d traffic selection algorithm" */
                    /*" enhanced-transmission-selection-algorithm \r\n", */
                    /*u4Index); */
                }
                else if (i4EtsConTsa == DCBX_VENDOR_SPECIFIC_ALGO)
                {
                    CliPrintf (CliHandle,
                               "set traffic class %d traffic selection algorithm"
                               " vendor-specific-algorithm \r\n", u4Index);
                }
            }
            CliPrintf (CliHandle, "\r\n");

            for (u4Index = DCBX_ZERO; u4Index < ETS_MAX_TCGID_CONF; u4Index++)
            {
                nmhGetLldpXdot1dcbxAdminETSRecoTrafficSelectionAlgorithm
                    (i4IfIndex, u4Index, &i4EtsRecoTsa);
                if (i4EtsRecoTsa == DCBX_STRICT_PRIORITY_ALGO)
                {
                    CliPrintf (CliHandle,
                               "set traffic class %d recommended traffic selection algorithm"
                               " strict-priority-algorithm \r\n", u4Index);
                }
                else if (i4EtsRecoTsa == DCBX_CREDIT_BASED_SHAPER_ALGO)
                {
                    CliPrintf (CliHandle,
                               "set traffic class %d recommended traffic selection algorithm"
                               " credit-based-shaper-algorithm \r\n", u4Index);
                }
                else if (i4EtsRecoTsa ==
                         DCBX_ENHANCED_TRANSMISSION_SELECTION_ALGO)
                {
                    /* Not required to print, as this is the default case */
                    /* CliPrintf (CliHandle, "set traffic class %d recommended traffic selection algorithm" */
                    /*" enhanced-transmission-selection-algorithm \r\n", */
                    /*u4Index); */
                }
                else if (i4EtsRecoTsa == DCBX_VENDOR_SPECIFIC_ALGO)
                {
                    CliPrintf (CliHandle,
                               "set traffic class %d recommended traffic selection algorithm"
                               " vendor-specific-algorithm \r\n", u4Index);
                }
            }
            CliPrintf (CliHandle, "\r\n");

            nmhGetLldpXdot1dcbxConfigETSConfigurationTxEnable
                (i4IfIndex, DCBX_DEST_ADDR_INDEX, &i4ETSTLVStatus);
            if (i4ETSTLVStatus != ETS_DISABLED)
            {
                CliPrintf (CliHandle, "dcbx tlv-select etstlv configuration"
                           "\r\n");
            }
            nmhGetLldpXdot1dcbxConfigETSRecommendationTxEnable
                (i4IfIndex, DCBX_DEST_ADDR_INDEX, &i4ETSTLVStatus);
            if (i4ETSTLVStatus != ETS_DISABLED)
            {
                CliPrintf (CliHandle, "dcbx tlv-select etstlv "
                           " recommendation \r\n");
            }
            nmhGetFslldpXdot1dcbxConfigTCSupportedTxEnable (i4IfIndex,
                                                            DCBX_DEST_ADDR_INDEX,
                                                            &i4ETSTLVStatus);
            if (i4ETSTLVStatus != ETS_DISABLED)
            {
                CliPrintf (CliHandle, "dcbx tlv-select etstlv tc-supported "
                           "   \r\n");
            }

            CliPrintf (CliHandle, "\r\n!\r\n");
        }
        while (nmhGetNextIndexFsETSPortTable (i4IfIndex, &i4NextIfIndex)
               != SNMP_FAILURE);
    }

    return CLI_SUCCESS;
}

/************************************************************************
 * FUNCTION NAME    : CEEShowRunningInterfaceConfig                             
 *
 * DESCRIPTION      : This function displays current CEE configurations    
 *
 * INPUT            : CliHandle - CliContext ID
 *                    i4IntIndex - Interface number
 *                    i1DcbxShowInt - 
 *
 * OUTPUT           : None                                               
 *
 * RETURNS          : CLI_SUCCESS 
 *************************************************************************/
PRIVATE INT4
CEEShowRunningInterfaceConfig (tCliHandle CliHandle, INT4 i4IntIndex,
                               INT1 i1DcbxShowInt)
{
    INT4                i4IfIndex = DCBX_ZERO;
    INT4                i4NextIfIndex = i4IntIndex;
    INT4                i4DcbxMode = DCBX_ZERO;
    UINT1               au1IfName[CFA_MAX_PORT_NAME_LENGTH] = { DCBX_ZERO };

    if (nmhGetFirstIndexFsDCBXPortTable (&i4NextIfIndex) != SNMP_FAILURE)
    {
        do
        {
            i4IfIndex = i4NextIfIndex;

            nmhGetFsDCBXMode (i4IfIndex, &i4DcbxMode);
            if ((i1DcbxShowInt == OSIX_TRUE) && (i4IfIndex != i4IntIndex))
            {
                continue;
            }

            MEMSET (&au1IfName[DCBX_ZERO], DCBX_ZERO, CFA_MAX_PORT_NAME_LENGTH);

            CfaCliConfGetIfName ((UINT4) i4IfIndex,
                                 (INT1 *) &au1IfName[DCBX_ZERO]);

            if (i4DcbxMode == DCBX_MODE_CEE)
            {
                CliPrintf (CliHandle, "interface %s\r\n", au1IfName);
                CliPrintf (CliHandle, "dcbx-mode cee");
                CliPrintf (CliHandle, "\r\n!\r\n");
            }
            else if (i4DcbxMode == DCBX_MODE_IEEE)
            {
                CliPrintf (CliHandle, "interface %s\r\n", au1IfName);
                CliPrintf (CliHandle, "dcbx-mode ieee");
                CliPrintf (CliHandle, "\r\n!\r\n");
            }

        }
        while (nmhGetNextIndexFsDcbPortTable (i4IfIndex, &i4NextIfIndex)
               != SNMP_FAILURE);
    }
    return CLI_SUCCESS;
}

/****************************************************************************
 *     FUNCTION NAME    : PFCShowRunningInterfaceConfig                             
 *     DESCRIPTION      : This function displays current PFC configurations    
 *     INPUT            : CliHandle - CliContext ID                             
 *     OUTPUT           : None                                               
 *     RETURNS          : None                            
 *****************************************************************************/
PRIVATE INT4
PFCShowRunningInterfaceConfig (tCliHandle CliHandle, INT4 i4IntIndex,
                               INT1 i1DcbxShowInt)
{
    INT4                i4IfIndex = DCBX_ZERO;
    INT4                i4NextIfIndex = DCBX_ZERO;
    INT4                i4PfcAdminStatus = DCBX_ZERO;
    INT4                i4PfcAdminMode = DCBX_ZERO;
    INT4                i4PfcTLVStatus = DCBX_ZERO;
    INT4                i4PfcWilling = DCBX_ZERO;
    INT4                i4PfcStatus = DCBX_ZERO;

    INT4                i4Priority = DCBX_ZERO;
    UINT1               au1IfName[CFA_MAX_PORT_NAME_LENGTH] = { DCBX_ZERO };

    if (nmhGetFirstIndexFsPFCPortTable (&i4NextIfIndex) != SNMP_FAILURE)
    {
        do
        {
            i4IfIndex = i4NextIfIndex;

            /* Condition for showing DCBX detail on particular interface */
            if ((i1DcbxShowInt == OSIX_TRUE) && (i4IfIndex != i4IntIndex))
            {
                continue;
            }

            MEMSET (&au1IfName[DCBX_ZERO], DCBX_ZERO, CFA_MAX_PORT_NAME_LENGTH);
            CfaCliConfGetIfName ((UINT4) i4IfIndex,
                                 (INT1 *) &au1IfName[DCBX_ZERO]);
            CliPrintf (CliHandle, "interface %s\r\n", au1IfName);

            nmhGetFsPFCRowStatus (i4IfIndex, &i4PfcAdminStatus);
            if (i4PfcAdminStatus == ACTIVE)
            {
                CliPrintf (CliHandle, "set priority-flow-control enable\r\n");
            }

            nmhGetLldpXdot1dcbxLocPFCWilling (i4IfIndex, &i4PfcWilling);
            if (i4PfcWilling == PFC_ENABLED)
            {
                CliPrintf (CliHandle, "set priority-flow-control willing "
                           "enable\r\n");
            }

            nmhGetFsPFCAdminMode (i4IfIndex, &i4PfcAdminMode);
            if (i4PfcAdminMode == DCBX_ADM_MODE_AUTO)
            {
                CliPrintf (CliHandle, "priority-flow-control mode auto\r\n");
            }
            else if (i4PfcAdminMode == DCBX_ADM_MODE_ON)
            {
                CliPrintf (CliHandle, "priority-flow-control mode on\r\n");
            }
            else
            {
                CliPrintf (CliHandle, "no priority-flow-control mode \r\n");
            }
            CliPrintf (CliHandle, "\r\n");

            CliPrintf (CliHandle, "\r\n");

            for (i4Priority = DCBX_ZERO; i4Priority < DCBX_MAX_PRIORITIES;
                 i4Priority++)
            {
                nmhGetLldpXdot1dcbxAdminPFCEnableEnabled (i4IfIndex,
                                                          (UINT4) i4Priority,
                                                          &i4PfcStatus);

                if (i4PfcStatus == PFC_ENABLED)
                {
                    CliPrintf (CliHandle,
                               "set priority %d flow-control enable\r\n",
                               i4Priority);
                }
            }
            CliPrintf (CliHandle, "\r\n");
            nmhGetLldpXdot1dcbxConfigPFCTxEnable (i4IfIndex,
                                                  DCBX_DEST_ADDR_INDEX,
                                                  &i4PfcTLVStatus);
            if (i4PfcTLVStatus != PFC_DISABLED)
            {
                CliPrintf (CliHandle, "dcbx tlv-select pfctlv   \r\n");
            }
            CliPrintf (CliHandle, "\r\n!\r\n");
        }
        while (nmhGetNextIndexFsPFCPortTable (i4IfIndex, &i4NextIfIndex)
               != SNMP_FAILURE);
    }

    return CLI_SUCCESS;
}

/****************************************************************************
*     FUNCTION NAME    : AppPriShowRunningGlobalConfig
*     DESCRIPTION      : This function displays current Application Priority
*                        configurations
*     INPUT            : CliHandle - CliContext ID
*     OUTPUT           : None
*     RETURNS          : None
 *****************************************************************************/
INT4
AppPriShowRunningInterfaceConfig (tCliHandle CliHandle, INT4 i4IntIndex,
                                  INT1 i1DcbxShowInt)
{
    INT4                i4IfIndex = DCBX_ZERO;
    INT4                i4NextIfIndex = DCBX_ZERO;
    INT4                i4AppPriAdminStatus = DCBX_ZERO;
    INT4                i4AppPriAdminMode = DCBX_ZERO;
    INT4                i4AppPriTLVStatus = DCBX_ZERO;
    INT4                i4AppPriWilling = DCBX_ZERO;
    INT4                i4AppPriIfIndex = DCBX_ZERO;
    INT4                i4AppPriNextIfIndex = DCBX_ZERO;
    INT4                i4Selector = DCBX_ZERO;
    INT4                i4NextSelector = DCBX_ZERO;
    UINT4               u4Protocol = DCBX_ZERO;
    UINT4               u4Priority = DCBX_ZERO;
    UINT4               u4NextProtocol = DCBX_ZERO;
    UINT1               au1IfName[CFA_MAX_PORT_NAME_LENGTH] = { DCBX_ZERO };

    if (nmhGetFirstIndexFsAppPriPortTable (&i4NextIfIndex) != SNMP_FAILURE)
    {
        do
        {
            i4IfIndex = i4NextIfIndex;

            /* Condition for showing DCBX detail on particular interface */
            if ((i1DcbxShowInt == OSIX_TRUE) && (i4IfIndex != i4IntIndex))
            {
                continue;
            }

            MEMSET (&au1IfName[DCBX_ZERO], DCBX_ZERO, CFA_MAX_PORT_NAME_LENGTH);

            CfaCliConfGetIfName ((UINT4) i4IfIndex,
                                 (INT1 *) &au1IfName[DCBX_ZERO]);

            CliPrintf (CliHandle, "interface %s\r\n", au1IfName);

            nmhGetFsAppPriRowStatus (i4IfIndex, &i4AppPriAdminStatus);
            if (i4AppPriAdminStatus == ACTIVE)
            {
                CliPrintf (CliHandle, "set application-priority enable\r\n");
            }

            nmhGetFslldpXdot1dcbxAdminApplicationPriorityWilling (i4IfIndex,
                                                                  &i4AppPriWilling);
            if (i4AppPriWilling == APP_PRI_ENABLED)
            {
                CliPrintf (CliHandle,
                           "set application-priority willing enable \r\n");
            }

            nmhGetFsAppPriAdminMode (i4IfIndex, &i4AppPriAdminMode);
            if (i4AppPriAdminMode == DCBX_ADM_MODE_AUTO)
            {
                CliPrintf (CliHandle, "application-priority mode auto \r\n");
            }
            else if (i4AppPriAdminMode == DCBX_ADM_MODE_ON)
            {
                CliPrintf (CliHandle, "application-priority mode on \r\n");
            }
            else
            {
                CliPrintf (CliHandle, "no application-priority mode  \r\n");
            }
            CliPrintf (CliHandle, "\r\n");

            nmhGetLldpXdot1dcbxConfigApplicationPriorityTxEnable (i4IfIndex,
                                                                  DCBX_DEST_ADDR_INDEX,
                                                                  &i4AppPriTLVStatus);
            if (i4AppPriTLVStatus != APP_PRI_DISABLED)
            {
                CliPrintf (CliHandle,
                           "dcbx tlv-select application-priority-tlv   \r\n");
            }
            CliPrintf (CliHandle, "\r\n");

            if (nmhGetFirstIndexLldpXdot1dcbxAdminApplicationPriorityAppTable
                (&i4AppPriNextIfIndex, &i4NextSelector,
                 &u4NextProtocol) != SNMP_FAILURE)
            {

                do
                {
                    i4AppPriIfIndex = i4AppPriNextIfIndex;
                    i4Selector = i4NextSelector;
                    u4Protocol = u4NextProtocol;

                    if (i4AppPriIfIndex == i4IfIndex)
                    {
                        nmhGetLldpXdot1dcbxAdminApplicationPriorityAEPriority
                            (i4AppPriIfIndex, i4Selector, u4Protocol,
                             &u4Priority);
                        CliPrintf (CliHandle,
                                   "set selector %d protocol %d priority %d \r\n",
                                   i4Selector, u4Protocol, u4Priority);
                    }
                }
                while
                    ((nmhGetNextIndexLldpXdot1dcbxAdminApplicationPriorityAppTable (i4AppPriIfIndex, &i4AppPriNextIfIndex, i4Selector, &i4NextSelector, u4Protocol, &u4NextProtocol) != SNMP_FAILURE) && (i4AppPriNextIfIndex <= i4IfIndex));
            }
        }
        while (nmhGetNextIndexFsAppPriPortTable (i4IfIndex, &i4NextIfIndex)
               != SNMP_FAILURE);

        CliPrintf (CliHandle, "\r\n!\r\n");
    }
    return CLI_SUCCESS;
}

/****************************************************************************
 *     FUNCTION NAME    : PFCShowRunningGlobalConfig                          
 *     DESCRIPTION      : This function displays current PFC Global
 *                        configurations    
 *     INPUT            : CliHandle - CliContext ID                             
 *     OUTPUT           : None                                               
 *     RETURNS          : None                            
 *****************************************************************************/
PRIVATE INT4
PFCShowRunningGlobalConfig (tCliHandle CliHandle)
{
    UINT4               u4PfcMinThreshold = DCBX_ZERO;
    UINT4               u4PfcMaxThreshold = DCBX_ZERO;

    nmhGetFsDcbPfcMinThreshold (&u4PfcMinThreshold);
    nmhGetFsDcbPfcMaxThreshold (&u4PfcMaxThreshold);

    if ((u4PfcMinThreshold != PFC_MIN_THRES) ||
        (u4PfcMaxThreshold != PFC_MAX_THRES))
    {

        CliPrintf (CliHandle, "set pfc threshold limit min %d max %d\r\n",
                   u4PfcMinThreshold, u4PfcMaxThreshold);
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*     FUNCTION NAME    : IssDCBXShowDebugging                               */
/*                                                                           */
/*     DESCRIPTION      : This function prints DCBX Debug level              */
/*                        for interfaces                                     */
/*                                                                           */
/*     INPUT            : None                                               */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : None                                               */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
IssDCBXShowDebugging (tCliHandle CliHandle)
{
    INT4                i4TraceLevel = DCBX_ZERO;

    nmhGetFsDcbxGlobalTraceLevel (&i4TraceLevel);

    if (i4TraceLevel == DCBX_ZERO)
    {
        return;
    }

    CliPrintf (CliHandle, "\r\nDCBX :\n");

    if (i4TraceLevel == DCBX_ALL_TRC)
    {
        CliPrintf (CliHandle, "  DCBX All debugging is on\r\n");
        return;
    }
    if (i4TraceLevel & DCBX_MGMT_TRC)
    {
        CliPrintf (CliHandle, "  DCBX Management debugging is on\r\n");
    }
    if (i4TraceLevel & DCBX_RESOURCE_TRC)
    {
        CliPrintf (CliHandle, "  DCBX Resource debugging is on\r\n");
    }
    if (i4TraceLevel & DCBX_FAILURE_TRC)
    {
        CliPrintf (CliHandle, "  DCBX Failure debugging is on\r\n");
    }
    if (i4TraceLevel & DCBX_CONTROL_PLANE_TRC)
    {
        CliPrintf (CliHandle, "  DCBX Control Plane debugging is on\r\n");
    }
    if (i4TraceLevel & DCBX_SEM_TRC)
    {
        CliPrintf (CliHandle, "  DCBX SEM debugging is on\r\n");
    }
    if (i4TraceLevel & DCBX_TLV_TRC)
    {
        CliPrintf (CliHandle, "  DCBX TLV debugging is on\r\n");
    }
    if (i4TraceLevel & DCBX_RED_TRC)
    {
        CliPrintf (CliHandle, "  DCBX Redundancy debugging is on\r\n");
    }
    if (i4TraceLevel & DCBX_CRITICAL_TRC)
    {
        CliPrintf (CliHandle, "  DCBX Critical debugging is on\r\n");
    }
    return;
}

/*****************************************************************************
 Function Name      : AppPriCliSetControlStatus

 Description        : This function will be used to start/shutdown 
                      the application prioirty Module on the subsystem 
 
 Input(s)           : CliHandle   - Handle to CLI session                
                    : u1Status - Start/Shutdown status for AP module.
 
 Output(s)          : CLI_SET_ERR will be used in case any errors in the 
                      Test Routine and 
                      CLI_FATAL_ERROR (CliHanlde) will be used in case of any 
                      errors in the Set Routine.                                               
 
 Return Value(s)    : CLI_SUCCESS/CLI_FAILURE           
 
 Called By          : cli_process_dcbx_cmd                         
 
 Calling Function   : SNMP nmh Routines                         
*****************************************************************************/
INT4
AppPriCliSetControlStatus (tCliHandle CliHandle, UINT1 u1Status)
{
    UINT4               u4ErrorCode = DCBX_ZERO;

    if (nmhTestv2FsAppPriSystemControl (&u4ErrorCode, u1Status) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsAppPriSystemControl (u1Status) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;

}

/*****************************************************************************
 Function Name      : AppPriCliSetModuleStatus
 Description        : This function will be used to Enable/disable 
                      the Application Priority Module on the subsystem 
 Input(s)           : CliHandle   - Handle to CLI session                
                    : u1Status - enable/disable status for ETS module.
 Output(s)          : CLI_SET_ERR will be used in case any errors in the 
                      Test Routine and 
                      CLI_FATAL_ERROR (CliHanlde) will be used in case of any 
                       errors in the Set Routine.                                  
 Return Value(s)     : CLI_SUCCESS/CLI_FAILURE           
 Called By           : cli_process_dcbx_cmd                         
 Calling Function    : SNMP nmh Routines                         
*****************************************************************************/

INT4
AppPriCliSetModuleStatus (tCliHandle CliHandle, UINT1 u1Status)
{
    UINT4               u4ErrorCode = SNMP_ERR_NO_ERROR;

    UNUSED_PARAM (CliHandle);

    /* Test the Given Vlaue if it success then Set */
    if (nmhTestv2FsAppPriModuleStatus (&u4ErrorCode, u1Status) != SNMP_SUCCESS)
    {
        return (CLI_FAILURE);
    }

    nmhSetFsAppPriModuleStatus (u1Status);
    return (CLI_SUCCESS);

}

/*****************************************************************************
 Function Name      : AppPriCliSetPortStatus
 Description        : This function will be used to enable/disable 
                      the Application Priority Admin status on the given port.  
 Input(s)           : CliHandle    - Handle to CLI session                
                    : i4IfIndex - Port number for which the Priority grouping
                      admin status to be set.
                    :i1ETSRowStatus -Enabled/Disabled.  
 Output(s)          : CLI_SET_ERR will be used in case any errors in the
                      Test Routine and
                      CLI_FATAL_ERROR (CliHanlde) will be used in case of 
                      any errors in the Set Routine.
 Return Value(s)    : CLI_SUCCESS/CLI_FAILURE           
 Called By          : cli_process_dcbx_cmd                         
 Calling Function   : SNMP nmh Routines                         
*****************************************************************************/

INT4
AppPriCliSetPortStatus (tCliHandle CliHandle, INT4 i4IfIndex,
                        UINT1 i1APRowStatus)
{
    UINT4               u4ErrorCode = SNMP_ERR_NO_ERROR;

    /* Test the Given Vlaue if it success then Set */
    if (nmhTestv2FsAppPriRowStatus (&u4ErrorCode, i4IfIndex,
                                    i1APRowStatus) != SNMP_SUCCESS)
    {
        return (CLI_FAILURE);
    }

    if (nmhSetFsAppPriRowStatus (i4IfIndex, i1APRowStatus) != SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }
    return (CLI_SUCCESS);

}

/*****************************************************************************
 Function Name      : AppPriCliSetAdminMode
 Description        : This function will be used to set Application Priority 
                     feature mode (Negotiable or Force-enable) on the
                     given port. 
 Input(s)           : CliHandle    - Handle to CLI session                
                    : i4IfIndex - Port number for which the Admin mode
                    to be set.
                    :u1AdminMode -Auto/On Mode.  
 Output(s)          : CLI_SET_ERR will be used in case any errors in the
                      Test Routine and 
                      CLI_FATAL_ERROR (CliHanlde) will be used in case of
                      any errors in the Set Routine.                            
 Return Value(s)    : CLI_SUCCESS/CLI_FAILURE           
 Called By          : cli_process_dcbx_cmd                         
 Calling Function   : SNMP nmh Routines                         
*****************************************************************************/

INT4
AppPriCliSetAdminMode (tCliHandle CliHandle, INT4 i4IfIndex, UINT1 u1AdminMode)
{
    UINT4               u4ErrorCode = SNMP_ERR_NO_ERROR;

    UNUSED_PARAM (CliHandle);
    /* Test the Given Vlaue if it success then Set */
    if (nmhTestv2FsAppPriAdminMode (&u4ErrorCode, i4IfIndex,
                                    u1AdminMode) != SNMP_SUCCESS)
    {
        return (CLI_FAILURE);
    }
    nmhSetFsAppPriAdminMode (i4IfIndex, u1AdminMode);
    return (CLI_SUCCESS);

}

/*****************************************************************************
 Function Name      : AppPriCliSetTrapStatus
 Description        : This function will be used to Enable/disable the 
                      Application Priority trap based on the Trap type specified.
 Input(s)           : CliHandle    - Handle to CLI session                
                    : u4TrapType - Application Priority trap types for which it
                                   should be enable/disable.
                    :u1Status - enable/disable status for Application Priority
                                module.
 Output(s)          : CLI_SET_ERR will be used in case any errors in the
                      Test Routine and
                      CLI_FATAL_ERROR (CliHanlde) will be used in case of 
                      any errors in the Set Routine.                            
 Return Value(s)     : CLI_SUCCESS/CLI_FAILURE
 Called By           : cli_process_dcbx_cmd                         
 Calling Function    : SNMP nmh Routines                         
*****************************************************************************/
INT4
AppPriCliSetTrapStatus (tCliHandle CliHandle, UINT4 u4TrapType, UINT1 u1Status)
{
    UINT4               u4ErrorCode = SNMP_ERR_NO_ERROR;
    INT4                i4TrapVal = DCBX_ZERO;

    UNUSED_PARAM (CliHandle);
    nmhGetFsAppPriGlobalEnableTrap (&i4TrapVal);
    if (u1Status == DCBX_TRAP_ENABLE)
    {
        i4TrapVal = (INT4) (i4TrapVal | (INT4) u4TrapType);
    }
    else
    {
        i4TrapVal = (INT4) ((UINT4) (i4TrapVal) & (~u4TrapType));
    }

    /* Test the Given Vlaue if it success then Set */

    if (nmhTestv2FsAppPriGlobalEnableTrap (&u4ErrorCode, i4TrapVal)
        != SNMP_SUCCESS)
    {
        return (CLI_FAILURE);
    }

    nmhSetFsAppPriGlobalEnableTrap (i4TrapVal);
    return (CLI_SUCCESS);

}

/****************************************************************************
 Function Name      : AppPriCliSetWillingStatus                                
 Description        : This function will be used to set Application Priority
                      willing status on the given port.     
 Input(s)           : CliHandle    - Handle to CLI session               
                    :i4IfIndex - Port number for which the Willing status
                     to be set.
                    :u1Status - APP_PRI_ENABLED/APP_PRI_DISABLED. 
 Output(s)          : CLI_SET_ERR will be used in case any errors 
                      in the Test Routine and 
                      CLI_FATAL_ERROR (CliHanlde) will be used in case of 
                      any errors in the Set Routine.                            
 Return Value(s)    : CLI_SUCCESS/CLI_FAILURE           
 Called By          : cli_process_dcbx_cmd                         
 Calling Function   : SNMP nmh Routines                         
*****************************************************************************/
INT4
AppPriCliSetWillingStatus (tCliHandle CliHandle, INT4 i4IfIndex, UINT1 u1Status)
{
    UINT4               u4ErrorCode = SNMP_ERR_NO_ERROR;

    UNUSED_PARAM (CliHandle);
    /* Test the Given Vlaue if it success then Set */
    if (nmhTestv2FslldpXdot1dcbxAdminApplicationPriorityWilling
        (&u4ErrorCode, i4IfIndex, u1Status) != SNMP_SUCCESS)
    {
        return (CLI_FAILURE);
    }

    nmhSetFslldpXdot1dcbxAdminApplicationPriorityWilling (i4IfIndex, u1Status);
    return (CLI_SUCCESS);

}

/****************************************************************************
 Function Name      : AppPriCliSetAppToPriMapping                               
 Description        : This function will be used to map an Application 
                      to a particular Priority
 Input(s)           : CliHandle    - Handle to CLI session               
                    :i4IfIndex - Port number for which the Willing status
                     to be set.
                     i4Selector - Selector Field
                     u4Protocol - Protocol Id
                     u4Priority - Priority
                    : None. 
 Output(s)          : CLI_SET_ERR will be used in case any errors 
                      in the Test Routine and 
                      CLI_FATAL_ERROR (CliHanlde) will be used in case of 
                      any errors in the Set Routine.                            
 Return Value(s)    : CLI_SUCCESS/CLI_FAILURE           
 Called By          : cli_process_dcbx_cmd                         
 Calling Function   : SNMP nmh Routines                         
*****************************************************************************/
INT4
AppPriCliSetAppToPriMapping (tCliHandle CliHandle, INT4 i4IfIndex,
                             INT4 i4Selector, UINT4 u4Protocol,
                             UINT4 u4Priority)
{
    UINT4               u4ErrorCode = SNMP_ERR_NO_ERROR;

    /* Test the Given Vlaue if it success then Set */
    if (nmhTestv2LldpXdot1dcbxAdminApplicationPriorityAEPriority
        (&u4ErrorCode, i4IfIndex, i4Selector, u4Protocol, u4Priority) !=
        SNMP_SUCCESS)
    {
        return (CLI_FAILURE);
    }

    if (nmhSetLldpXdot1dcbxAdminApplicationPriorityAEPriority
        (i4IfIndex, i4Selector, u4Protocol, u4Priority) != SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return (CLI_SUCCESS);

}

/****************************************************************************
 Function Name      : AppPriCliUnSetAppToPriMapping                               
 Description        : This function will be used to unmap an Application 
                      to a particular Priority
 Input(s)           : CliHandle    - Handle to CLI session               
                    :i4IfIndex - Port number for which the Willing status
                     to be set.
                     i4Selector - Selector Field
                     u4Protocol - Protocol Id
                    : None. 
 Output(s)          : CLI_SET_ERR will be used in case any errors 
                      in the Test Routine and 
                      CLI_FATAL_ERROR (CliHanlde) will be used in case of 
                      any errors in the Set Routine.                            
 Return Value(s)    : CLI_SUCCESS/CLI_FAILURE           
 Called By          : cli_process_dcbx_cmd                         
 Calling Function   : SNMP nmh Routines                         
*****************************************************************************/
INT4
AppPriCliUnSetAppToPriMapping (tCliHandle CliHandle, INT4 i4IfIndex,
                               INT4 i4Selector, UINT4 u4Protocol)
{
    UINT4               u4ErrorCode = DCBX_ZERO;

    if (nmhTestv2FsAppPriXAppRowStatus (&u4ErrorCode, i4IfIndex, i4Selector,
                                        u4Protocol, DESTROY) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }
    if (nmhSetFsAppPriXAppRowStatus (i4IfIndex, i4Selector, u4Protocol,
                                     DESTROY) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return (CLI_SUCCESS);

}

/*****************************************************************************
 Function Name      : AppPriCliSetTlvSelection                             
 Description        : This function will be used to enable/disable the
                     transmission status of Application Prioirty TLV
 Input(s)              : CliHandle    - Handle to CLI session      
                       : i4IfIndex    - Port number for which the Application
                                        Priority TLV Transmission to be set.                  
                    : u1Status - Enable/Disable status for Application 
                      Priority TLV Transmission.
 Output(s)          : CLI_SET_ERR will be used in case any errors in the Test
                      Routine and 
                      CLI_FATAL_ERROR (CliHanlde) will be used in case of any
                      errors in the Set Routine.                                
 Return Value(s)    : CLI_SUCCESS/CLI_FAILURE           
 Called By          : cli_process_dcbx_cmd                         
*****************************************************************************/
INT4
AppPriCliSetTlvSelection (tCliHandle CliHandle, INT4 i4IfIndex, UINT1 u1Status)
{
    UINT4               u4ErrorCode = SNMP_ERR_NO_ERROR;

    UNUSED_PARAM (CliHandle);
    /*Enabling PFC TLV Tranfer Status */
    if (nmhTestv2LldpXdot1dcbxConfigApplicationPriorityTxEnable
        (&u4ErrorCode, i4IfIndex, DCBX_DEST_ADDR_INDEX,
         u1Status) != SNMP_SUCCESS)
    {
        return (CLI_FAILURE);
    }
    nmhSetLldpXdot1dcbxConfigApplicationPriorityTxEnable
        (i4IfIndex, DCBX_DEST_ADDR_INDEX, u1Status);
    return (CLI_SUCCESS);

}

/*****************************************************************************
 Function Name      : AppPriCliClearCounters                             
 Description        : This function will be used to Clear the 
                      Application Priority Counters for the 
                      specified port or for all the ports.
 Input(s)              : CliHandle - Handle to CLI session      
                       : i4IfIndex - Port number for which the 
                         Application Priority counters to be cleared.
                       : u1Value - To mention whether to clear for specific port
                      or to clear for all ports.
 Output(s)          : CLI_SET_ERR will be used in case any errors in the Test
                      Routine and 
                      CLI_FATAL_ERROR (CliHanlde) will be used in case of any                          
                      errors in the Set Routine.                                                                           
 Return Value(s)    : CLI_SUCCESS/CLI_FAILURE           
 Called By          : cli_process_dcbx_cmd                         
*****************************************************************************/

INT4
AppPriCliClearCounters (tCliHandle CliHandle, INT4 i4IfIndex)
{
    UINT4               u4ErrorCode = SNMP_ERR_NO_ERROR;

    UNUSED_PARAM (CliHandle);
    /* Clearing the Application Prioirty counter for all ports */
    if (i4IfIndex == DCBX_ZERO)
    {
        if (nmhTestv2FsAppPriClearCounters
            (&u4ErrorCode, APP_PRI_ENABLED) != SNMP_SUCCESS)
        {
            return (CLI_FAILURE);
        }
        nmhSetFsAppPriClearCounters (APP_PRI_ENABLED);
    }
    else
    {
        /* clearing the Application Priority counters per port */
        if (nmhTestv2FsAppPriClearTLVCounters (&u4ErrorCode, i4IfIndex,
                                               APP_PRI_ENABLED) != SNMP_SUCCESS)
        {
            return (CLI_FAILURE);
        }
        nmhSetFsAppPriClearTLVCounters (i4IfIndex, APP_PRI_ENABLED);
    }
    return (CLI_SUCCESS);

}

/***************************************************************************
* Function Name     : AppPriCliShowGbl
* Description       : This function is used to display the Application 
*                     Priority Global Information
* Input(s)          : CliHandle    - Handle to CLI session
* Output(s)         : None.
* Global Variables
* Referred          : None.
* Global Variables
* Modified          : None.
* Return Value(s)   : CLI_SUCCESS/CLI_FAILURE
*****************************************************************************/

INT4
AppPriCliShowGbl (tCliHandle CliHandle)
{
    UINT4               u4TrapCount = DCBX_ZERO;
    INT4                i4SysStatus = DCBX_ZERO;
    INT4                i4ModStatus = DCBX_ZERO;
    INT4                i4TrapEnabled = DCBX_ZERO;

    CliPrintf (CliHandle, "\n\r");
    CliPrintf (CliHandle, "\r Application Priority GLOBAL Info \n");
    CliPrintf (CliHandle, "\r-----------------------------------\n");

    nmhGetFsAppPriSystemControl (&i4SysStatus);
    nmhGetFsAppPriModuleStatus (&i4ModStatus);
    nmhGetFsAppPriGlobalEnableTrap (&i4TrapEnabled);
    nmhGetFsAppPriGeneratedTrapCount (&u4TrapCount);

    if (i4SysStatus == APP_PRI_START)
    {
        CliPrintf (CliHandle,
                   "\r Application Priority System control status : Start\n");
    }

    if (i4ModStatus == APP_PRI_ENABLED)
    {
        CliPrintf (CliHandle,
                   "\r Application Priority Module status         : Enabled\n");
    }
    else
    {
        CliPrintf (CliHandle,
                   "\r Application Priority Module status         : Disabled\n");
    }
    CliPrintf (CliHandle,
               "\r Application Priority Trap Count            : %d\n",
               u4TrapCount);

    CliPrintf (CliHandle, "\r Trap(s) Enabled:\n");
    CliPrintf (CliHandle, "\r ---------------- \n");

    if ((i4TrapEnabled & APP_PRI_ALL_TRAP) == APP_PRI_ALL_TRAP)
    {
        CliPrintf (CliHandle, "\r All\n");
    }
    else if (i4TrapEnabled == DCBX_ZERO)
    {
        CliPrintf (CliHandle, "\r None\n");
    }
    else
    {
        if ((i4TrapEnabled & APP_PRI_MODULE_STATUS_TRAP) ==
            APP_PRI_MODULE_STATUS_TRAP)
        {
            CliPrintf (CliHandle, "\r ModuleStatusChange\n");
        }
        if ((i4TrapEnabled & APP_PRI_ADMIN_MODE_TRAP) ==
            APP_PRI_ADMIN_MODE_TRAP)
        {
            CliPrintf (CliHandle, "\r AdminModeChange\n");
        }
        if ((i4TrapEnabled & APP_PRI_PEER_STATUS_TRAP) ==
            APP_PRI_PEER_STATUS_TRAP)
        {
            CliPrintf (CliHandle, "\r PeerStatusChange\n");
        }
        if ((i4TrapEnabled & APP_PRI_SEM_TRAP) == APP_PRI_SEM_TRAP)
        {
            CliPrintf (CliHandle, "\r SemChange\n");
        }
    }

    CliPrintf (CliHandle, "\n\r");
    return CLI_SUCCESS;

}

/*****************************************************************************
 Function Name      : AppPriCliShowPortInfo                             
 Description        : This function is used to display the             
                      Application Prioirty Port status                              
 Input(s)           : CliHandle    - Handle to CLI session               
                    : i4IfIndex    - Interface Index                     
 Output(s)          : None.                                              

 Return Value(s)    : CLI_SUCCESS/CLI_FAILURE                         
 Called By          : cli_process_dcbx_cmd                                 
 Calling Function   : SNMP nmh Routines                               
*****************************************************************************/

INT4
AppPriCliShowPortInfo (tCliHandle CliHandle, INT4 i4IfIndex, UINT1 u1Detail)
{
    INT4                i4Port = DCBX_ZERO;
    INT4                i4NextPort = DCBX_ZERO;

    CliPrintf (CliHandle, "\n\r");
    if (i4IfIndex == DCBX_ZERO)
    {
        if (nmhGetFirstIndexFsAppPriPortTable (&i4NextPort) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle,
                       "\r No Application Prioirty Port is Created\n");
            CliPrintf (CliHandle, "\n\r");
            return CLI_SUCCESS;
        }
        if (u1Detail != APP_PRI_ENABLED)
        {
            CliPrintf (CliHandle, "\r%-9s %-9s\n", "Port", "Status");
            CliPrintf (CliHandle,
                       "\r-----------------------------------------------\n");
        }
        do
        {
            i4Port = i4NextPort;
            AppPriCliShowPortDetails (CliHandle, i4Port, u1Detail);
            if (u1Detail == APP_PRI_ENABLED)
            {
                CliPrintf (CliHandle,
                           "\r-------------------------------------"
                           "----------\n");
                CliPrintf (CliHandle, "\n\r");
            }
        }
        while ((nmhGetNextIndexFsAppPriPortTable (i4Port, &i4NextPort)
                == SNMP_SUCCESS));
    }
    else
    {
        if (u1Detail != APP_PRI_ENABLED)
        {
            CliPrintf (CliHandle, "\r%-9s %-9s\n", "Port", "Status");
            CliPrintf (CliHandle,
                       "\r-----------------------------------------------\n");
        }
        AppPriCliShowPortDetails (CliHandle, i4IfIndex, u1Detail);
    }
    CliPrintf (CliHandle,
               "\r-----------------------------------------------\n");
    CliPrintf (CliHandle,
               "* Selector 2,3 will be ignored for CEE Operation! \r\n");
    CliPrintf (CliHandle, "\n\r");
    return CLI_SUCCESS;
}

/**************************************************************************
 Function Name      : AppPriCliShowPortDetails                             
 Description        : This function is used to display the             
                      Application Priority Port details
 Input(s)           : CliHandle    - Handle to CLI session               
                    : i4IfIndex    - Interface Index                     
 Output(s)          : None.                                              

 Return Value(s)    : CLI_SUCCESS/CLI_FAILURE                         
 Called By          : cli_process_dcbx_cmd                                 
 Calling Function   : SNMP nmh Routines                               
**************************************************************************/

INT4
AppPriCliShowPortDetails (tCliHandle CliHandle, INT4 i4IfIndex, UINT1 u1Detail)
{

    INT4                i4RowStatus = DCBX_ZERO;
    INT4                i4AdminMode = DCBX_ZERO;
    INT4                i4OperState = DCBX_ZERO;
    INT4                i4StateType = DCBX_ZERO;
    INT4                i4TxStatus = DCBX_ZERO;
    INT4                i4OperVersion = DCBX_VER_UNKNOWN;
    UINT1               au1NameStr[CFA_MAX_PORT_NAME_LENGTH] = { DCBX_ZERO };
    INT4                i4AppPriDcbxStatus = DCBX_ZERO;

    CfaCliGetIfName ((UINT4) i4IfIndex, (INT1 *) au1NameStr);
    nmhGetFsAppPriRowStatus (i4IfIndex, &i4RowStatus);

    if (u1Detail == DCBX_ENABLED)
    {
        if (i4RowStatus == ACTIVE)
        {
            CliPrintf (CliHandle,
                       "\r-----------------------------------------------\n");
            CliPrintf (CliHandle,
                       "\r Application Priority Port %-9s Information \n",
                       au1NameStr);
            CliPrintf (CliHandle,
                       "\r-----------------------------------------------\n");
            CliPrintf (CliHandle, "\r Application Priority Local Port Info \n");
            CliPrintf (CliHandle,
                       "\r-----------------------------------------------\n");
            AppPriCliShowLocalPortInfo (CliHandle, i4IfIndex);
            CliPrintf (CliHandle,
                       "\r-----------------------------------------------\n");
            CliPrintf (CliHandle, "\r Application Priority Admin Port Info \n");
            CliPrintf (CliHandle,
                       "\r-----------------------------------------------\n");
            AppPriCliShowAdminPortInfo (CliHandle, i4IfIndex);
            CliPrintf (CliHandle,
                       "\r-----------------------------------------------\n");
            CliPrintf (CliHandle,
                       "\r Application Priority Remote Port Info \n");
            CliPrintf (CliHandle,
                       "\r-----------------------------------------------\n");
            AppPriCliShowRemPortInfo (CliHandle, i4IfIndex);
            CliPrintf (CliHandle,
                       "\r-----------------------------------------------\n");
            CliPrintf (CliHandle,
                       "\r Application Priority Port Related Info \n");
            CliPrintf (CliHandle,
                       "\r-----------------------------------------------\n");
            nmhGetLldpXdot1dcbxConfigApplicationPriorityTxEnable (i4IfIndex,
                                                                  DCBX_DEST_ADDR_INDEX,
                                                                  &i4TxStatus);
            CliPrintf (CliHandle, "\r%-35s: ",
                       "Application Priority TLV Tx and Rx Status");
            if (i4TxStatus == DCBX_ENABLED)
            {
                CliPrintf (CliHandle, "Enabled\n");

            }
            else
            {
                CliPrintf (CliHandle, "Disabled\n");
            }

            CliPrintf (CliHandle, "\n\r");

            nmhGetFsAppPriDcbxStatus (i4IfIndex, &i4AppPriDcbxStatus);
            if (i4AppPriDcbxStatus > DCBX_ZERO)
            {
                CliPrintf (CliHandle, "\r%-35s: %s\n",
                           "Application Priority DCBX Status",
                           au1DcbxStatus[i4AppPriDcbxStatus - 1]);
            }

            CliPrintf (CliHandle, "\n\r");

            nmhGetFsAppPriAdminMode (i4IfIndex, &i4AdminMode);
            nmhGetFsAppPriDcbxOperState (i4IfIndex, &i4OperState);
            nmhGetFsAppPriDcbxStateMachine (i4IfIndex, &i4StateType);

            CliPrintf (CliHandle, "\r%-35s: ",
                       "Application Priority Port Mode");
            if (i4AdminMode == DCBX_ADM_MODE_AUTO)
            {
                CliPrintf (CliHandle, "AUTO MODE\n");
            }
            else if (i4AdminMode == DCBX_ADM_MODE_ON)
            {
                CliPrintf (CliHandle, "ON MODE\n");
            }
            else
            {
                CliPrintf (CliHandle, "OFF MODE\n");
            }

            nmhGetFsDcbOperVersion (i4IfIndex, &i4OperVersion);
            if (i4OperVersion == DCBX_MODE_CEE)
            {
                CliPrintf (CliHandle, "\r%-35s: ",
                           "Application Priority Oper State");
                if (i4OperState == CEE_OPER_DISABLED)
                {
                    CliPrintf (CliHandle, "DISABLED\n");
                }
                if (i4OperState == CEE_OPER_USE_LOCAL_CFG)
                {
                    CliPrintf (CliHandle, "USE LOCAL CFG\n");
                }
                if (i4OperState == CEE_OPER_USE_PEER_CFG)
                {
                    CliPrintf (CliHandle, "USE PEER CFG\n");
                }
            }
            else
            {
                CliPrintf (CliHandle, "\r%-35s: ",
                           "Application Priority Oper State");
                if (i4OperState == DCBX_OPER_OFF)
                {
                    CliPrintf (CliHandle, "OFF STATE\n");
                }
                else if (i4OperState == DCBX_OPER_INIT)
                {
                    CliPrintf (CliHandle, "INIT STATE\n");
                }
                else
                {
                    CliPrintf (CliHandle, "RECO STATE\n");
                }
            }
            CliPrintf (CliHandle, "\r%-35s: ",
                       "Application Priority State Machine Type");
            if (i4StateType == DCBX_ASYM_TYPE)
            {
                CliPrintf (CliHandle, "Asymetric\n");
            }
            else if (i4StateType == DCBX_SYM_TYPE)
            {
                CliPrintf (CliHandle, "Symmetric\n");
            }
            else if (i4StateType == DCBX_FEAT_TYPE)
            {
                CliPrintf (CliHandle, "Feature\n");
            }
        }
        else
        {
            CliPrintf (CliHandle, "\r%-9s %-9s\n", "Port", "Status");
            CliPrintf (CliHandle,
                       "\r-----------------------------------------------\n");
            CliPrintf (CliHandle, "%-9s %-8s", au1NameStr, "Disabled");
        }
    }
    else
    {
        if (i4RowStatus == ACTIVE)
        {
            CliPrintf (CliHandle, "%-9s %-8s", au1NameStr, "Enabled");
        }
        else
        {
            CliPrintf (CliHandle, "%-9s %-8s", au1NameStr, "Disabled");
        }
    }
    CliPrintf (CliHandle, "\n\r");
    return CLI_SUCCESS;
}

/*****************************************************************************
* Function Name     : AppPriCliShowLocalPortInfo                                  
* Description       : This function is used to display the ETS Local Port status                                 
* Input(s)          : CliHandle    - Handle to CLI session     
*                   : pPortEntry    - pointer to Application Priority port Entry  
                     : i4IfIndex -     Interface Index  
* Output(s)         : None.                                               
* Global Variables                                                         
* Referred          : None.                                               
* Global Variables                                                          
* Modified          : None.                                                
* Return Value(s)   : CLI_SUCCESS/CLI_FAILURE           
* Called By         : AppPriCliShowPortInfo                                                
*****************************************************************************/
INT4
AppPriCliShowLocalPortInfo (tCliHandle CliHandle, INT4 i4IfIndex)
{

    INT4                i4LocWilling = DCBX_ZERO;
    INT4                i4NextSelector = DCBX_ZERO;
    INT4                i4NextPort = DCBX_ZERO;
    INT4                i4CurSelector = DCBX_ZERO;
    INT4                i4CurPort = DCBX_ZERO;
    UINT4               u4Priority = DCBX_ZERO;
    UINT4               u4CurProtocol = DCBX_ZERO;
    UINT4               u4NextProtocol = DCBX_ZERO;

    if (nmhValidateIndexInstanceFslldpXdot1dcbxLocApplicationPriorityBasicTable
        (i4IfIndex) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }
    nmhGetFslldpXdot1dcbxLocApplicationPriorityWilling (i4IfIndex,
                                                        &i4LocWilling);
    if (i4LocWilling == APP_PRI_ENABLED)
    {
        CliPrintf (CliHandle, "\r%-25s:%-8s\n", "Willing Status", "Enabled");
    }
    else
    {
        CliPrintf (CliHandle, "\r%-25s:%-8s\n", "Willing Status", "Disabled");
    }
    CliPrintf (CliHandle, "\n\r");

    if (nmhGetFirstIndexLldpXdot1dcbxLocApplicationPriorityAppTable
        (&i4NextPort, &i4NextSelector,
         (UINT4 *) &u4NextProtocol) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }
    CliPrintf (CliHandle, "\r Selector Protocol Priority \n");
    CliPrintf (CliHandle, "\r-----------------------------\n");
    if (i4NextPort > i4IfIndex)
    {
        CliPrintf (CliHandle, "- \t - \t -\r\n");
    }
    else
    {
        do
        {
            i4CurSelector = i4NextSelector;
            u4CurProtocol = u4NextProtocol;
            i4CurPort = i4NextPort;
            if ((i4NextPort == i4IfIndex))
            {
                nmhGetLldpXdot1dcbxLocApplicationPriorityAEPriority
                    (i4NextPort, i4NextSelector, u4NextProtocol, &u4Priority);
                CliPrintf (CliHandle, "\r %5d %5d %10d\r\n", i4NextSelector,
                           u4NextProtocol, u4Priority);
            }
        }
        while (nmhGetNextIndexLldpXdot1dcbxLocApplicationPriorityAppTable
               (i4CurPort, &i4NextPort, i4CurSelector, &i4NextSelector,
                u4CurProtocol, &u4NextProtocol) != SNMP_FAILURE);
    }
    return (CLI_SUCCESS);
}

/*****************************************************************************
* Function Name     : AppPriCliShowAdminPortInfo                                  
* Description       : This function is used to display the Application 
*                     Priority Admin Port Information
* Input(s)          : CliHandle    - Handle to CLI session     
*                   : pPortEntry   - pointer to Application Priority port Entry  
                    : i4IfIndex    - Interface Index  
* Output(s)         : None.                                               
* Global Variables                                                         
* Referred          : None.                                               
* Global Variables                                                          
* Modified          : None.                                                
* Return Value(s)   : CLI_SUCCESS/CLI_FAILURE           
* Called By         : AppPriCliShowPortInfo                                                
*****************************************************************************/
INT4
AppPriCliShowAdminPortInfo (tCliHandle CliHandle, INT4 i4IfIndex)
{
    INT4                i4Willing = DCBX_ZERO;
    INT4                i4NextSelector = DCBX_ZERO;
    INT4                i4NextPort = DCBX_ZERO;
    INT4                i4CurSelector = DCBX_ZERO;
    INT4                i4CurPort = DCBX_ZERO;
    UINT4               u4Priority = DCBX_ZERO;
    UINT4               u4CurProtocol = DCBX_ZERO;
    UINT4               u4NextProtocol = DCBX_ZERO;

    if (nmhValidateIndexInstanceFslldpXdot1dcbxAdminApplicationPriorityBasicTable (i4IfIndex) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }
    nmhGetFslldpXdot1dcbxAdminApplicationPriorityWilling (i4IfIndex,
                                                          &i4Willing);
    if (i4Willing == APP_PRI_ENABLED)
    {
        CliPrintf (CliHandle, "\r%-25s:%-8s\n", "Willing Status", "Enabled");
    }
    else
    {
        CliPrintf (CliHandle, "\r%-25s:%-8s\n", "Willing Status", "Disabled");
    }
    CliPrintf (CliHandle, "\n\r");

    if (nmhGetFirstIndexLldpXdot1dcbxAdminApplicationPriorityAppTable
        (&i4NextPort, &i4NextSelector, &u4NextProtocol) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }
    CliPrintf (CliHandle, "\r Selector Protocol Priority \n");
    CliPrintf (CliHandle, "\r-----------------------------\n");
    if (i4NextPort > i4IfIndex)
    {
        CliPrintf (CliHandle, "- \t - \t -\r\n");
    }
    else
    {
        do
        {
            i4CurSelector = i4NextSelector;
            u4CurProtocol = u4NextProtocol;
            i4CurPort = i4NextPort;
            if ((i4NextPort == i4IfIndex))
            {
                nmhGetLldpXdot1dcbxAdminApplicationPriorityAEPriority
                    (i4NextPort, i4NextSelector, u4NextProtocol, &u4Priority);
                CliPrintf (CliHandle, "\r %5d %5d %10d\r\n", i4NextSelector,
                           u4NextProtocol, u4Priority);
            }
        }
        while (nmhGetNextIndexLldpXdot1dcbxAdminApplicationPriorityAppTable
               (i4CurPort, &i4NextPort, i4CurSelector, &i4NextSelector,
                u4CurProtocol, &u4NextProtocol) != SNMP_FAILURE);

    }
    return (CLI_SUCCESS);

}

/*********************************************************************************
* Function Name     : AppPriCliShowRemPortInfo                                  
* Description       : This function is used to display the ETS Remote Port status                                 
* Input(s)          : CliHandle    - Handle to CLI session     
*                   : pPortEntry   - pointer to Application Priority port Entry  
                    : i4IfIndex    - Interface Index  
* Output(s)         : None.                                               
* Global Variables                                                         
* Referred          : None.                                               
* Global Variables                                                          
* Modified          : None.                                                
* Return Value(s)   : CLI_SUCCESS/CLI_FAILURE           
* Called By         : PFCCliShowPortInfo                                                
*********************************************************************************/
INT4
AppPriCliShowRemPortInfo (tCliHandle CliHandle, INT4 i4IfIndex)
{
    INT4                i4Willing = DCBX_ZERO;
    INT4                i4NextSelector = DCBX_ZERO;
    INT4                i4NextPort = DCBX_ZERO;
    UINT4               u4Priority = DCBX_ZERO;
    INT4                i4CurSelector = DCBX_ZERO;
    UINT4               u4CurProtocol = DCBX_ZERO;
    UINT4               u4NextProtocol = DCBX_ZERO;
    INT4                i4CurPort = DCBX_ZERO;
    UINT4               u4LldpV2RemTimeMark = DCBX_ZERO;
    INT4                i4LldpV2RemIndex = DCBX_ZERO;
    UINT4               u4NextLldpV2RemTimeMark = DCBX_ZERO;
    INT4                i4NextLldpV2RemIndex = DCBX_ZERO;
    UINT4               u4LldpV2RemLocalDestMACAddress = DCBX_ZERO;
    UINT4               u4NextLldpV2RemLocalDestMACAddress;

    nmhGetNextIndexFslldpXdot1dcbxRemApplicationPriorityBasicTable
        (DCBX_ZERO, &u4LldpV2RemTimeMark, i4IfIndex,
         &i4NextPort, u4LldpV2RemLocalDestMACAddress,
         &u4NextLldpV2RemLocalDestMACAddress, DCBX_ZERO, &i4LldpV2RemIndex);
    if ((i4IfIndex != i4NextPort) || (i4LldpV2RemIndex == DCBX_ZERO))
    {
        CliPrintf (CliHandle, "\rNo Remote Entry is Present\n\r");
        return (CLI_SUCCESS);
    }

    nmhGetFslldpXdot1dcbxRemApplicationPriorityWilling (u4LldpV2RemTimeMark,
                                                        i4IfIndex,
                                                        u4NextLldpV2RemLocalDestMACAddress,
                                                        i4LldpV2RemIndex,
                                                        &i4Willing);
    if (i4Willing == APP_PRI_ENABLED)
    {
        CliPrintf (CliHandle, "\r%-25s:%-8s\n", "Willing Status", "Enabled");
    }
    else
    {
        CliPrintf (CliHandle, "\r%-25s:%-8s\n", "Willing Status", "Disabled");
    }
    CliPrintf (CliHandle, "\n\r");

    if (nmhGetFirstIndexLldpXdot1dcbxRemApplicationPriorityAppTable
        (&u4NextLldpV2RemTimeMark, &i4NextPort,
         &u4NextLldpV2RemLocalDestMACAddress, &i4NextLldpV2RemIndex,
         &i4NextSelector, &u4NextProtocol) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }
    CliPrintf (CliHandle, "\r Selector Protocol Priority \n");
    CliPrintf (CliHandle, "\r-----------------------------\n");
    if (i4NextPort > i4IfIndex)
    {
        CliPrintf (CliHandle, "- \t - \t -\r\n");
    }
    else
    {
        do
        {
            u4LldpV2RemTimeMark = u4NextLldpV2RemTimeMark;
            i4LldpV2RemIndex = i4NextLldpV2RemIndex;
            u4NextLldpV2RemLocalDestMACAddress = u4LldpV2RemLocalDestMACAddress;
            i4CurSelector = i4NextSelector;
            u4CurProtocol = u4NextProtocol;
            i4CurPort = i4NextPort;
            if ((i4NextPort == i4IfIndex))
            {
                nmhGetLldpXdot1dcbxRemApplicationPriorityAEPriority
                    (u4NextLldpV2RemTimeMark, i4NextPort,
                     u4NextLldpV2RemLocalDestMACAddress, i4NextLldpV2RemIndex,
                     i4NextSelector, u4NextProtocol, &u4Priority);
                CliPrintf (CliHandle, "\r %5d %5d %10d\r\n", i4NextSelector,
                           u4NextProtocol, u4Priority);
            }
        }
        while (nmhGetNextIndexLldpXdot1dcbxRemApplicationPriorityAppTable
               (DCBX_ZERO, &u4LldpV2RemTimeMark, i4CurPort, &i4NextPort,
                u4LldpV2RemLocalDestMACAddress,
                &u4NextLldpV2RemLocalDestMACAddress, DCBX_ZERO,
                &i4LldpV2RemIndex, i4CurSelector, &i4NextSelector,
                u4CurProtocol, &u4NextProtocol) != SNMP_FAILURE);

    }

    return (CLI_SUCCESS);

}

/*****************************************************************************
 Function Name       : AppPriCliShowPortCounters                             
 Description         : This function will be used to Show the Application
                       Priority counters for ports            
 Input(s)            : CliHandle - Handle to CLI session      
                     : i4IfIndex - Port number for which the Application Priority
                                   counter information to be shown.
 Output(s)           : CLI_SET_ERR will be used in case any errors in the Test
                       Routine and 
                       CLI_FATAL_ERROR (CliHanlde) will be used in case of 
                       any errors in the Set Routine.
 Return Value(s)    : CLI_SUCCESS/CLI_FAILURE           
 Called By          : cli_process_dcbx_cmd                         
*****************************************************************************/
INT4
AppPriCliShowPortCounters (tCliHandle CliHandle, INT4 i4IfIndex)
{

    UINT4               u4TxTLVCounter = DCBX_ZERO;
    UINT4               u4RxTLVCounter = DCBX_ZERO;
    UINT4               u4ErrorTLVCounter = DCBX_ZERO;
    UINT4               u4NumAppPriMapping = DCBX_ZERO;
    INT4                i4Port = DCBX_ZERO;
    INT4                i4NextPort = DCBX_ZERO;
    UINT1               au1NameStr[CFA_MAX_PORT_NAME_LENGTH] = { DCBX_ZERO };

    CliPrintf (CliHandle, "\n\r");
    CliPrintf (CliHandle, "Application Prioirty TLV  Counter Information\r\n");
    CliPrintf (CliHandle, "\r--------------------------------------------\n");

    if (i4IfIndex == DCBX_ZERO)
    {
        if (nmhGetFirstIndexFsAppPriPortTable (&i4NextPort) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle,
                       "\rNo Application Priority Port is created\n");
            CliPrintf (CliHandle,
                       "\r--------------------------------------------------\n");
            CliPrintf (CliHandle, "\r\n");
            return CLI_SUCCESS;
        }
        CliPrintf (CliHandle, "\r%-9s%-20s%-20s%-20s%-20s\n\r", "Port",
                   "TLV transmitted", "TLV Received", "TLV Errors",
                   "No. of Mapping in Last Received TLV");
        do
        {
            i4Port = i4NextPort;
            CfaCliGetIfName ((UINT4) i4Port, (INT1 *) au1NameStr);
            nmhGetFsAppPriTxTLVCounter (i4Port, &u4TxTLVCounter);
            nmhGetFsAppPriRxTLVCounter (i4Port, &u4RxTLVCounter);
            nmhGetFsAppPriRxTLVErrors (i4Port, &u4ErrorTLVCounter);
            nmhGetFsAppPriAppProtocols (i4Port, &u4NumAppPriMapping);
            CliPrintf (CliHandle, "\r%-9s%-20d%-20d%-20d%-20d\n", au1NameStr,
                       u4TxTLVCounter, u4RxTLVCounter,
                       u4ErrorTLVCounter, u4NumAppPriMapping);
        }
        while ((nmhGetNextIndexFsAppPriPortTable (i4Port, &i4NextPort)
                == SNMP_SUCCESS));
    }
    else
    {
        CliPrintf (CliHandle, "\r%-9s%-20s%-20s%-20s%-20s\n\r", "Port",
                   "TLV transmitted", "TLV Received", "TLV Errors",
                   "No. of AppPriMappings in Last Received TLV");
        CfaCliGetIfName ((UINT4) i4IfIndex, (INT1 *) au1NameStr);
        nmhGetFsAppPriTxTLVCounter (i4IfIndex, &u4TxTLVCounter);
        nmhGetFsAppPriRxTLVCounter (i4IfIndex, &u4RxTLVCounter);
        nmhGetFsAppPriRxTLVErrors (i4IfIndex, &u4ErrorTLVCounter);
        nmhGetFsAppPriAppProtocols (i4IfIndex, &u4NumAppPriMapping);
        CliPrintf (CliHandle, "\r%-9s%-20d%-20d%-20d%-20d\n", au1NameStr,
                   u4TxTLVCounter, u4RxTLVCounter, u4ErrorTLVCounter,
                   u4NumAppPriMapping);
    }
    CliPrintf (CliHandle, "\r----------------------------------------\n");
    CliPrintf (CliHandle, "\r\n");
    return CLI_SUCCESS;
}

/*****************************************************************************
  Function Name      : ETSCliSetTSAPerTC
  Description        : This function is used to SET  Traffic Selection Algorithm                       for each Trafic Class             
  Input(s)           : i4IfIndex    - Interface Index                  
                     : pi4TcNoList   - Ponter to the Trafic Class Number Array
                     : i4TsaAlgo    - Traffic Selection Algorithm Value
                     : u1UpdateAdmConOrAdmReco 
                         - 1 = Call the Admin Con nmh routines
                           0 = Call the Admin Reco nmh routines
  Output(s)          : None.                                                
  Return Value(s)    : CLI_SUCCESS/CLI_FAILURE           
  Called By          : cli_process_dcbx_cmd                         
 *****************************************************************************/
PRIVATE INT4
ETSCliSetTSAPerTC (INT4 i4IfIndex, INT4 *pi4TcNoList,
                   INT4 i4TsaAlgo, UINT1 u1UpdateAdmConOrAdmReco)
{
    UINT4               u4ErrorCode = SNMP_ERR_NO_ERROR;
    UINT4               u4Idx = DCBX_ZERO;

    if (pi4TcNoList == NULL)
    {
        return CLI_FAILURE;
    }

    for (u4Idx = DCBX_ZERO; u4Idx < ETS_MAX_TCGID_CONF; u4Idx++)
    {
        if (u1UpdateAdmConOrAdmReco == CLI_ETS_UPDATE_ADM_CON_TABLE)
        {
            if (pi4TcNoList[u4Idx] != DCBX_INVALID_ID)
            {
                /* Test the Given Vlaue */
                if (nmhTestv2LldpXdot1dcbxAdminETSConTrafficSelectionAlgorithm
                    (&u4ErrorCode, i4IfIndex, (UINT4) pi4TcNoList[u4Idx],
                     i4TsaAlgo) != SNMP_SUCCESS)
                {
                    return (CLI_FAILURE);
                }
            }
        }
        else
        {
            if (pi4TcNoList[u4Idx] != DCBX_INVALID_ID)
            {
                /* Test the Given Vlaue */
                if (nmhTestv2LldpXdot1dcbxAdminETSRecoTrafficSelectionAlgorithm
                    (&u4ErrorCode, i4IfIndex, (UINT4) pi4TcNoList[u4Idx],
                     i4TsaAlgo) != SNMP_SUCCESS)
                {
                    return (CLI_FAILURE);
                }
            }
        }
    }
    for (u4Idx = DCBX_ZERO; u4Idx < ETS_MAX_TCGID_CONF; u4Idx++)
    {
        if (u1UpdateAdmConOrAdmReco == DCBX_ONE)
        {
            if (pi4TcNoList[u4Idx] != DCBX_INVALID_ID)
            {
                /* Set the Given Vlaue */
                if (nmhSetLldpXdot1dcbxAdminETSConTrafficSelectionAlgorithm
                    (i4IfIndex, pi4TcNoList[u4Idx], i4TsaAlgo) != SNMP_SUCCESS)
                {
                    return (CLI_FAILURE);
                }
            }
        }
        else
        {
            if (pi4TcNoList[u4Idx] != DCBX_INVALID_ID)
            {
                /* Set the Given Vlaue */
                if (nmhSetLldpXdot1dcbxAdminETSRecoTrafficSelectionAlgorithm
                    (i4IfIndex, pi4TcNoList[u4Idx], i4TsaAlgo) != SNMP_SUCCESS)
                {
                    return (CLI_FAILURE);
                }
            }
        }
    }
    return CLI_SUCCESS;
}

/*****************************************************************************
 * Function Name      : DCBXCliClearCounters                             
 *
 * Description        : This function will be used to Clear the CEE Counters
 *                      for the specified port or for all the ports.
 *
 * Input(s)           : CliHandle - Handle to CLI session      
 *                    : i4IfIndex -  Port number for which the PFC counters
 *                       to be cleared.
 *
 * Output(s)          : CLI_SET_ERR will be used in case any errors in the Test
 *                      Routine.
 *
 * Return Value(s)    : CLI_SUCCESS/CLI_FAILURE           
 *
 * Called By          : cli_process_dcbx_cmd                         
*****************************************************************************/
PRIVATE INT4
DCBXCliClearCounters (tCliHandle CliHandle, INT4 i4IfIndex)
{
    UINT4               u4ErrorCode = SNMP_ERR_NO_ERROR;

    UNUSED_PARAM (CliHandle);
    if (i4IfIndex == DCBX_ZERO)
    {
        if (nmhTestv2FsDcbxCEEClearCounters
            (&u4ErrorCode, DCBX_ENABLED) != SNMP_SUCCESS)
        {
            return (CLI_FAILURE);
        }

        nmhSetFsDcbxCEEClearCounters (DCBX_ENABLED);
    }
    else
    {
        if (nmhTestv2FsDcbxCEETlvClearCounters (&u4ErrorCode, i4IfIndex,
                                                DCBX_ENABLED) != SNMP_SUCCESS)
        {
            return (CLI_FAILURE);
        }

        nmhSetFsDcbxCEETlvClearCounters (DCBX_ENABLED, i4IfIndex);
    }
    return (CLI_SUCCESS);
}

/*****************************************************************************
 * Function Name       : DcbxCeeCliSetTrapStatus
 *
 * Description         : This function will be used to Enable/disable the 
 *                       CEE traps based on the Trap type specified.
 *
 * Input(s)            : CliHandle  - Handle to CLI session                
 *                     : u4TrapType - CEE trap types for which it
 *                                    should be enable/disable.
 *                     : u1Status   - enable/disable status of the specified trap
 *                                    type.
 * Output(s)           : CLI_SET_ERR will be used in case any errors in the
 *                       Test Routines.
 *
 * Return Value(s)     : CLI_SUCCESS/CLI_FAILURE
 *
 * Called By           : cli_process_dcbx_cmd                         
 *
 * Calling Function    : SNMP nmh Routines                         
*****************************************************************************/
INT4
DcbxCeeCliSetTrapStatus (tCliHandle CliHandle, UINT4 u4TrapType, UINT1 u1Status)
{
    UINT4               u4ErrorCode = SNMP_ERR_NO_ERROR;
    INT4                i4TrapVal = DCBX_ZERO;

    UNUSED_PARAM (CliHandle);

    nmhGetFsDcbxCEEGlobalEnableTrap (&i4TrapVal);
    if (u1Status == DCBX_TRAP_ENABLE)
    {
        i4TrapVal = (INT4) (i4TrapVal | (INT4) u4TrapType);
    }
    else
    {
        i4TrapVal = (INT4) ((UINT4) (i4TrapVal) & (~u4TrapType));
    }

    /* Test the Given Vlaue if it success then Set */
    if (nmhTestv2FsDcbxCEEGlobalEnableTrap (&u4ErrorCode, i4TrapVal)
        != SNMP_SUCCESS)
    {
        return (CLI_FAILURE);
    }
    if (nmhSetFsDcbxCEEGlobalEnableTrap (i4TrapVal) != SNMP_SUCCESS)
    {
        return (CLI_FAILURE);
    }

    return (CLI_SUCCESS);
}

/*****************************************************************************
 * Function Name      : DcbxCliSetMode
 *
 * Description        : This function will be used to set the DCBX mode
 *
 * Input(s)           : CliHandle  - Handle to CLI session                
 *                    : i4IfIndex  - Port Index 
 *                    : i4DcbxMode - The DCBX mode to be set
 *
 * Output(s)          : CLI_SET_ERR will be used in case any errors in the 
 *                      Test Routine. 
 *
 * Return Value(s)    : CLI_SUCCESS/CLI_FAILURE           
 *
 * Called By          : cli_process_dcbx_cmd                         
 *
 * Calling Function   : SNMP nmh Routines                         
*****************************************************************************/
PRIVATE INT4
DcbxCliSetMode (tCliHandle CliHandle, INT4 i4IfIndex, INT4 i4DcbxMode)
{
    UINT4               u4ErrorCode = SNMP_ERR_NO_ERROR;

    UNUSED_PARAM (CliHandle);
    /* Test the Given Vlaue if it success then Set */
    if (nmhTestv2FsDCBXMode (&u4ErrorCode,
                             i4IfIndex, i4DcbxMode) != SNMP_SUCCESS)
    {
        return (CLI_FAILURE);
    }

    if (nmhSetFsDCBXMode (i4IfIndex, i4DcbxMode) != SNMP_SUCCESS)
    {
        return (CLI_FAILURE);
    }
    return (CLI_SUCCESS);
}

/*****************************************************************************
 * Function Name      : DCBXCliShowCtrlTlvInfo
 *
 * Description        : This function is used to display the
 *                      DCBX CEE Ctrl Tlv Information 
 *
 * Input(s)           : CliHandle    - Handle to CLI session
 *                       i4Index     - Interface Index 
 *
 * Output(s)          : None.
 *
 * Return Value(s)    : CLI_SUCCESS/CLI_FAILURE
 *
 * Called By          : cli_process_dcbx_cmd
 *
 * Calling Function   : SNMP nmh Routines
*****************************************************************************/
PRIVATE INT4
DCBXCliShowCtrlTlvInfo (tCliHandle CliHandle, INT4 i4Index)
{
    UINT4               u4SeqNo = DCBX_ZERO;
    UINT4               u4AckNo = DCBX_ZERO;
    UINT4               u4RcvdAckNo = DCBX_ZERO;
    UINT4               u4RxCounter = DCBX_ZERO;
    UINT4               u4TxCounter = DCBX_ZERO;

    CliPrintf (CliHandle, "\n\r");

    nmhGetFsDcbxCEECtrlSeqNo (i4Index, &u4SeqNo);
    nmhGetFsDcbxCEECtrlAckNo (i4Index, &u4AckNo);
    nmhGetFsDcbxCEECtrlRcvdAckNo (i4Index, &u4RcvdAckNo);
    nmhGetFsDcbxCEECtrlTxTLVCounter (i4Index, &u4TxCounter);
    nmhGetFsDcbxCEECtrlRxTLVCounter (i4Index, &u4RxCounter);

    CliPrintf (CliHandle, "Control TLV Information \r\n");
    CliPrintf (CliHandle, "------------------------------------\r\n");
    CliPrintf (CliHandle, "Sequence Number  : %d \r\n", u4SeqNo);
    CliPrintf (CliHandle, "Ack Number       : %d \r\n", u4AckNo);
    CliPrintf (CliHandle, "Recvd Ack Number : %d \r\n", u4RcvdAckNo);
    CliPrintf (CliHandle, "Rx Counter       : %u \r\n", u4RxCounter);
    CliPrintf (CliHandle, "Tx Counter       : %u \r\n", u4TxCounter);
    CliPrintf (CliHandle, "------------------------------------\r\n");
    CliPrintf (CliHandle, "\n\r");

    return CLI_SUCCESS;
}

/*****************************************************************************
 * Function Name      : DCBXCliShowPortsDetail
 *
 * Description        : This function is used to display the
 *                      DCBX Version Information for both IEEE and CEE.
 *                      Also displays Ctrl TLV Information for CEE.
 *
 * Input(s)           : CliHandle    - Handle to CLI session
 *                      i4Index     - Interface Index 
 *
 * Output(s)          : None.
 *
 * Return Value(s)    : CLI_SUCCESS/CLI_FAILURE
 *
 * Called By          : cli_process_dcbx_cmd
 *
 * Calling Function   : SNMP nmh Routines
*****************************************************************************/
PRIVATE INT4
DCBXCliShowPortsDetail (tCliHandle CliHandle, INT4 i4Index)
{
    INT4                i4Port = DCBX_ZERO;
    UINT1               au1NameStr[CFA_MAX_PORT_NAME_LENGTH] = { DCBX_ZERO };
    INT4                i4LocOperVersion = DCBX_ZERO;
    INT4                i4LocMaxVersion = DCBX_ZERO;
    INT4                i4RemOperVersion = DCBX_ZERO;
    INT4                i4RemMaxVersion = DCBX_ZERO;
    INT4                i4NextPort = i4Index;
    CONST CHR1         *DcbxCliModeString[] = { /*0 */ "CEE",
        /*1 */ "IEEE",
        /*2 */ "AUTO",
        /*3 */ "UNKNOWN"
    };

    CliPrintf (CliHandle, "\n\r");

    if (i4Index == DCBX_ZERO)
    {
        if (nmhGetFirstIndexFsDcbPortTable (&i4NextPort) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r DCBX is Not Enabled on Port\n");
            CliPrintf (CliHandle, "\n\r");
            return CLI_SUCCESS;
        }

        do
        {
            i4Port = i4NextPort;
            CfaCliGetIfName ((UINT4) i4Port, (INT1 *) au1NameStr);

            nmhGetFsDcbOperVersion (i4Port, &i4LocOperVersion);
            nmhGetFsDcbPeerOperVersion (i4Port, &i4RemOperVersion);
            nmhGetFsDcbMaxVersion (i4Port, &i4LocMaxVersion);
            nmhGetFsDcbPeerMaxVersion (i4Port, &i4RemMaxVersion);

            CliPrintf (CliHandle, "Port : %s \r\n", au1NameStr);
            CliPrintf (CliHandle, "\n\r");
            CliPrintf (CliHandle, "Version Detail : \r\n");
            CliPrintf (CliHandle, "---------------------------------\r\n");
            CliPrintf (CliHandle, "Admin Oper Version  : %s \r\n",
                       DcbxCliModeString[i4LocOperVersion]);
            CliPrintf (CliHandle, "Admin Max Version   : %s \r\n",
                       DcbxCliModeString[i4LocMaxVersion]);
            CliPrintf (CliHandle, "Remote Oper Version : %s \r\n",
                       DcbxCliModeString[i4RemOperVersion]);
            CliPrintf (CliHandle, "Remote Max Version  : %s \r\n",
                       DcbxCliModeString[i4RemMaxVersion]);
            CliPrintf (CliHandle, "---------------------------------\r\n");

            if (i4LocOperVersion == DCBX_MODE_CEE)
            {
                DCBXCliShowCtrlTlvInfo (CliHandle, i4Port);
            }
            CliPrintf (CliHandle, "\n\r");
        }
        while (nmhGetNextIndexFsDcbPortTable (i4Port, &i4NextPort)
               == SNMP_SUCCESS);
    }
    else
    {
        i4Port = i4Index;

        CfaCliGetIfName ((UINT4) i4Port, (INT1 *) au1NameStr);

        nmhGetFsDcbOperVersion (i4Port, &i4LocOperVersion);
        nmhGetFsDcbPeerOperVersion (i4Port, &i4RemOperVersion);
        nmhGetFsDcbMaxVersion (i4Port, &i4LocMaxVersion);
        nmhGetFsDcbPeerMaxVersion (i4Port, &i4RemMaxVersion);

        CliPrintf (CliHandle, "Port : %s \r\n", au1NameStr);
        CliPrintf (CliHandle, "\n\r");
        CliPrintf (CliHandle, "Version Detail : \r\n");
        CliPrintf (CliHandle, "---------------------------------\r\n");
        CliPrintf (CliHandle, "Admin Oper Version  : %s \r\n",
                   DcbxCliModeString[i4LocOperVersion]);
        CliPrintf (CliHandle, "Admin Max Version   : %s \r\n",
                   DcbxCliModeString[i4LocMaxVersion]);
        CliPrintf (CliHandle, "Remote Oper Version : %s \r\n",
                   DcbxCliModeString[i4RemOperVersion]);
        CliPrintf (CliHandle, "Remote Max Version  : %s \r\n",
                   DcbxCliModeString[i4RemMaxVersion]);
        CliPrintf (CliHandle, "---------------------------------\r\n");

        if (i4LocOperVersion == DCBX_MODE_CEE)
        {
            DCBXCliShowCtrlTlvInfo (CliHandle, i4Port);
        }
        CliPrintf (CliHandle, "\n\r");
    }

    return CLI_SUCCESS;
}

/**************************************************************************
 * Function Name      : ETSCliShowCEEPortDetails                             
 *
 * Description        : This function is used to display the             
 *                           ETS Port status.
 *
 * Input(s)           : CliHandle    - Handle to CLI session               
 *                    : i4IfIndex    - Interface Index                     
 *                    : u1Detail     - Detail output
 *
 * Output(s)          : None.                                              
 *
 * Return Value(s)    : CLI_SUCCESS/CLI_FAILURE
 *
 * Called By          : ETSCliShowPortInfo                                 
 *
 * Calling Function   : SNMP nmh Routines                               
**************************************************************************/
PRIVATE INT4
ETSCliShowCEEPortDetails (tCliHandle CliHandle, INT4 i4IfIndex, UINT1 u1Detail)
{
    INT4                i4EtsRowStatus = DCBX_ZERO;
    UINT1               au1NameStr[CFA_MAX_PORT_NAME_LENGTH] = { DCBX_ZERO };
    INT4                i4ETSAdminMode = DCBX_ZERO;
    INT4                i4ETSOperState = DCBX_ZERO;
    INT4                i4ETSStateType = DCBX_ZERO;
    INT4                i4ETSTxStatus = DCBX_ZERO;
    INT4                i4ETSDcbxStatus = DCBX_ZERO;

    CfaCliGetIfName ((UINT4) i4IfIndex, (INT1 *) au1NameStr);
    /*CliPrintf (CliHandle,"\r Port     : %-9s \n",au1NameStr); */
    nmhGetFsETSRowStatus (i4IfIndex, &i4EtsRowStatus);

    if (u1Detail == DCBX_ENABLED)
    {
        if (i4EtsRowStatus == ACTIVE)
        {
            CliPrintf (CliHandle,
                       "\r-----------------------------------------------\n");
            CliPrintf (CliHandle,
                       "\r ETS Port %-9s Information \n", au1NameStr);
            /*            CliPrintf (CliHandle,
               "\r-----------------------------------------------\n");  */
            /*CliPrintf (CliHandle, "ETS is   :%-8s", "Enabled"); */
            CliPrintf (CliHandle,
                       "\r-----------------------------------------------\n");
            CliPrintf (CliHandle, "\r ETS Local Port Info \n");
            CliPrintf (CliHandle,
                       "\r----------------------------------------------------------\n");
            ETSCliShowCEELocalPortInfo (CliHandle, i4IfIndex);
            CliPrintf (CliHandle,
                       "\r-----------------------------------------------\n");
            CliPrintf (CliHandle, "\r ETS Admin Port Info \n");
            CliPrintf (CliHandle,
                       "\r---------------------------------------------------------\n");
            ETSCliShowCEEAdminPortInfo (CliHandle, i4IfIndex);
            CliPrintf (CliHandle,
                       "\r-----------------------------------------------\n");
            CliPrintf (CliHandle, "\r ETS Remote Port Info \n");
            CliPrintf (CliHandle,
                       "\r---------------------------------------------------------\n");
            ETSCliShowCEERemPortInfo (CliHandle, i4IfIndex);
            CliPrintf (CliHandle,
                       "\r-----------------------------------------------\n");

            CliPrintf (CliHandle, "\r ETS Port Related Info \n");
            CliPrintf (CliHandle,
                       "\r-----------------------------------------------\n");
            nmhGetLldpXdot1dcbxConfigETSConfigurationTxEnable
                (i4IfIndex, DCBX_DEST_ADDR_INDEX, &i4ETSTxStatus);
            CliPrintf (CliHandle, "\r%-35s:", "ETS Conf TLV Tx and Rx Status");
            if (i4ETSTxStatus == DCBX_ENABLED)
            {
                CliPrintf (CliHandle, "Enabled\n");

            }
            else
            {
                CliPrintf (CliHandle, "Disabled\n");
            }
            nmhGetLldpXdot1dcbxConfigETSRecommendationTxEnable
                (i4IfIndex, DCBX_DEST_ADDR_INDEX, &i4ETSTxStatus);
            CliPrintf (CliHandle, "\r%-35s:", "ETS Reco TLV Tx and Rx Status");
            if (i4ETSTxStatus == DCBX_ENABLED)
            {
                CliPrintf (CliHandle, "Enabled\n");

            }
            else
            {
                CliPrintf (CliHandle, "Disabled\n");
            }
            nmhGetFslldpXdot1dcbxConfigTCSupportedTxEnable (i4IfIndex,
                                                            DCBX_DEST_ADDR_INDEX,
                                                            &i4ETSTxStatus);
            CliPrintf (CliHandle, "\r%-35s:",
                       "ETS TC Supp TLV Tx and Rx Status");
            if (i4ETSTxStatus == DCBX_ENABLED)
            {
                CliPrintf (CliHandle, "Enabled\n");

            }
            else
            {
                CliPrintf (CliHandle, "Disabled\n");
            }

            CliPrintf (CliHandle, "\n\r");

            nmhGetFsETSDcbxStatus (i4IfIndex, &i4ETSDcbxStatus);
            if (i4ETSDcbxStatus > DCBX_ZERO)
            {
                CliPrintf (CliHandle, "\r%-35s:%s\n", "ETS DCBX Status",
                           au1DcbxStatus[i4ETSDcbxStatus - 1]);
            }

            CliPrintf (CliHandle, "\n\r");

            nmhGetFsETSAdminMode (i4IfIndex, &i4ETSAdminMode);
            nmhGetFsETSDcbxOperState (i4IfIndex, &i4ETSOperState);
            nmhGetFsETSDcbxStateMachine (i4IfIndex, &i4ETSStateType);

            CliPrintf (CliHandle, "\r%-35s:", "ETS Port Mode");
            if (i4ETSAdminMode == DCBX_ADM_MODE_AUTO)
            {
                CliPrintf (CliHandle, "AUTO MODE\n");
            }
            if (i4ETSAdminMode == DCBX_ADM_MODE_ON)
            {
                CliPrintf (CliHandle, "ON MODE\n");
            }
            if (i4ETSAdminMode == DCBX_ADM_MODE_OFF)
            {
                CliPrintf (CliHandle, "OFF MODE\n");
            }
            CliPrintf (CliHandle, "\r%-35s:", "ETS Oper State");
            if (i4ETSOperState == CEE_OPER_DISABLED)
            {
                CliPrintf (CliHandle, "DISABLED\n");
            }
            if (i4ETSOperState == CEE_OPER_USE_LOCAL_CFG)
            {
                CliPrintf (CliHandle, "USE LOCAL CFG\n");
            }
            if (i4ETSOperState == CEE_OPER_USE_PEER_CFG)
            {
                CliPrintf (CliHandle, "USE PEER CFG\n");
            }
            CliPrintf (CliHandle, "\r%-35s:", "ETS State Machine Type");
            if (i4ETSStateType == DCBX_ASYM_TYPE)
            {
                CliPrintf (CliHandle, "Asymetric\n");
                CliPrintf (CliHandle,
                           "\r-----------------------------------------------\n");
                CliPrintf (CliHandle, "\n\r");
            }
            if (i4ETSStateType == DCBX_SYM_TYPE)
            {
                CliPrintf (CliHandle, "Symmetric\n");
                CliPrintf (CliHandle,
                           "\r-----------------------------------------------\n");
                CliPrintf (CliHandle, "\n\r");
            }
            if (i4ETSStateType == DCBX_FEAT_TYPE)
            {
                CliPrintf (CliHandle, "Feature\n");
                CliPrintf (CliHandle,
                           "\r-----------------------------------------------\n");
                CliPrintf (CliHandle, "\n\r");
            }
        }
        else
        {
            CliPrintf (CliHandle, "\r%-9s %-9s\n", "Port", "Status");
            CliPrintf (CliHandle,
                       "\r-----------------------------------------------\n");
            CliPrintf (CliHandle, "\r\n%-9s %-8s\r\n", au1NameStr, "Disabled");
            CliPrintf (CliHandle,
                       "\r-----------------------------------------------\n");
            CliPrintf (CliHandle, "\n\r");
        }
    }
    else
    {
        if (i4EtsRowStatus == ACTIVE)
        {
            CliPrintf (CliHandle, "\r%-9s %-8s\r\n", au1NameStr, "Enabled");
            CliPrintf (CliHandle,
                       "\r-----------------------------------------------\n");
            CliPrintf (CliHandle, "\n\r");
        }
        else
        {
            CliPrintf (CliHandle, "\r%-9s %-8s\r\n", au1NameStr, "Disabled");
            CliPrintf (CliHandle,
                       "\r-----------------------------------------------\n");
            CliPrintf (CliHandle, "\n\r");
            /*CliPrintf (CliHandle, "ETS is    :%-8s", "Disabled"); */
        }
    }
    CliPrintf (CliHandle, "\n\r");
    return CLI_SUCCESS;
}

/*****************************************************************************
 * Function Name     : ETSCliShowCEELocalPortInfo                                  
 *
 * Description       : This function is used to display the ETS Local Port status
 *
 * Input(s)          : CliHandle    - Handle to CLI session     
                     : i4IfIndex -     Interface Index  

 * Output(s)         : None.                                               
 *
 * Return Value(s)   : CLI_SUCCESS/CLI_FAILURE           
 *
 * Called By         : ETSCliShowCEEPortDetails                                                
 *****************************************************************************/
PRIVATE INT4
ETSCliShowCEELocalPortInfo (tCliHandle CliHandle, INT4 i4IfIndex)
{
    UINT4               u4PriIdx = DCBX_ZERO;
    UINT4               u4ETSLocTCGID = DCBX_ZERO;
    UINT4               u4EtsConBw = DCBX_ZERO;
    INT4                i4TgIdx = DCBX_ZERO;
    INT4                i4ETSLocWilling = DCBX_ZERO;
    INT4                i4ETSTcSup = DCBX_ZERO;
    INT4                i4EtsConTsa = DCBX_ZERO;

    CliPrintf (CliHandle, "\r  TGID   Bandwidth " " TSA  Priority\n");
    CliPrintf (CliHandle,
               "\r----------------------------------------------------------\n");

    for (i4TgIdx = DCBX_ZERO; i4TgIdx < ETS_MAX_TCGID_CONF; i4TgIdx++)
    {
        if (i4TgIdx < ETS_TCGID8)
        {
            nmhGetLldpXdot1dcbxLocETSConTrafficClassBandwidth (i4IfIndex,
                                                               (UINT4) i4TgIdx,
                                                               &u4EtsConBw);

            nmhGetLldpXdot1dcbxLocETSConTrafficSelectionAlgorithm (i4IfIndex,
                                                                   (UINT4)
                                                                   i4TgIdx,
                                                                   &i4EtsConTsa);
        }
        CliPrintf (CliHandle, "%6d", i4TgIdx);
        CliPrintf (CliHandle, "%8d%%", u4EtsConBw);
        CliPrintf (CliHandle, "    ");
        CliPrintf (CliHandle, "%-5s", " ETSA ");
        CliPrintf (CliHandle, "         ");
        for (u4PriIdx = DCBX_ZERO; u4PriIdx < DCBX_MAX_PRIORITIES; u4PriIdx++)
        {
            nmhGetLldpXdot1dcbxLocETSConPriTrafficClass (i4IfIndex, u4PriIdx,
                                                         &u4ETSLocTCGID);
            if ((INT4) u4ETSLocTCGID == i4TgIdx)
            {
                CliPrintf (CliHandle, "%d ", u4PriIdx);
            }
        }
        CliPrintf (CliHandle, "\n\r");
    }
    CliPrintf (CliHandle, "\n\r");

    nmhGetLldpXdot1dcbxLocETSConTrafficClassesSupported (i4IfIndex,
                                                         (UINT4 *) &i4ETSTcSup);
    if (i4ETSTcSup == DCBX_ZERO)
    {
        i4ETSTcSup = ETS_TCGID8;
    }
    CliPrintf (CliHandle, "\r%-25s:%d\n",
               "Number of Traffic Class", i4ETSTcSup);
    nmhGetLldpXdot1dcbxLocETSConWilling (i4IfIndex, &i4ETSLocWilling);
    if (i4ETSLocWilling == ETS_ENABLED)
    {
        CliPrintf (CliHandle, "\r%-25s:%-8s\n", "Willing Status", "Enabled");
    }
    else
    {
        CliPrintf (CliHandle, "\r%-25s:%-8s\n", "Willing Status", "Disabled");
    }

    CliPrintf (CliHandle, "\n\r");
    CliPrintf (CliHandle, "In CEE Operation, always TSA will be elected as \n"
               "Enhanced Transmission Selection Algorithm(ETSA). \r\n");

    CliPrintf (CliHandle, "\n\r");
    return (CLI_SUCCESS);
}

/*****************************************************************************
 * Function Name     : ETSCliShowCEEAdminPortInfo                                  
 *
 * Description       : This function is used to display the ETS Admin Port status                                 
 *
 * Input(s)          : CliHandle - Handle to CLI session     
                     : i4IfIndex - Interface Index  

 * Output(s)         : None.                                               
 *
 * Return Value(s)   : CLI_SUCCESS/CLI_FAILURE           
 *
 * Called By         : ETSCliShowCEEPortDetails                                                
*****************************************************************************/
PRIVATE INT4
ETSCliShowCEEAdminPortInfo (tCliHandle CliHandle, INT4 i4IfIndex)
{
    UINT4               u4PriIdx = DCBX_ZERO;
    UINT4               u4EtsConBw = DCBX_ZERO;
    UINT4               u4ETSAdminTCGID = DCBX_ZERO;
    INT4                i4TgIdx = DCBX_ZERO;
    INT4                i4ETSAdminWilling = DCBX_ZERO;
    INT4                i4ETSTcSup = DCBX_ZERO;
    INT4                i4EtsConTsa = DCBX_ZERO;
    INT4                i4ETSAdmCBS = DCBX_ZERO;

    CliPrintf (CliHandle, "\r TGID   Bandwidth " " TSA  Priority\n");
    CliPrintf (CliHandle,
               "\r---------------------------------------------------------\n");

    for (i4TgIdx = DCBX_ZERO; i4TgIdx < ETS_MAX_TCGID_CONF; i4TgIdx++)
    {
        if (i4TgIdx < ETS_MAX_TCGID_CONF)
        {
            nmhGetLldpXdot1dcbxAdminETSConTrafficClassBandwidth (i4IfIndex,
                                                                 (UINT4)
                                                                 i4TgIdx,
                                                                 &u4EtsConBw);
            nmhGetLldpXdot1dcbxAdminETSConTrafficSelectionAlgorithm (i4IfIndex,
                                                                     (UINT4)
                                                                     i4TgIdx,
                                                                     &i4EtsConTsa);
        }

        CliPrintf (CliHandle, "%6d", i4TgIdx);
        CliPrintf (CliHandle, "%8d%%", u4EtsConBw);
        CliPrintf (CliHandle, "    ");
        if (i4EtsConTsa == DCBX_STRICT_PRIORITY_ALGO)
        {
            CliPrintf (CliHandle, "%-5s", "SPA");
        }
        else if (i4EtsConTsa == DCBX_CREDIT_BASED_SHAPER_ALGO)
        {
            CliPrintf (CliHandle, "%-5s", "CBSA");
        }
        else if (i4EtsConTsa == DCBX_ENHANCED_TRANSMISSION_SELECTION_ALGO)
        {
            CliPrintf (CliHandle, "%-5s", "ETSA");
        }
        else if (i4EtsConTsa == DCBX_VENDOR_SPECIFIC_ALGO)
        {
            CliPrintf (CliHandle, "%-5s", "VSA");
        }
        CliPrintf (CliHandle, "        ");

        for (u4PriIdx = DCBX_ZERO; u4PriIdx < DCBX_MAX_PRIORITIES; u4PriIdx++)
        {
            nmhGetLldpXdot1dcbxAdminETSConPriTrafficClass (i4IfIndex, u4PriIdx,
                                                           &u4ETSAdminTCGID);
            if ((INT4) u4ETSAdminTCGID == i4TgIdx)
            {
                CliPrintf (CliHandle, "%d ", u4PriIdx);
            }
        }
        CliPrintf (CliHandle, "\n\r");
    }
    CliPrintf (CliHandle, "\n\r");

    nmhGetLldpXdot1dcbxAdminETSConTrafficClassesSupported (i4IfIndex,
                                                           (UINT4 *)
                                                           &i4ETSTcSup);
    if (i4ETSTcSup == DCBX_ZERO)
    {
        i4ETSTcSup = ETS_TCGID8;
    }
    CliPrintf (CliHandle, "\r%-25s:%d\n", "Number of Traffic Class",
               i4ETSTcSup);
    nmhGetLldpXdot1dcbxAdminETSConWilling (i4IfIndex, &i4ETSAdminWilling);
    if (i4ETSAdminWilling == ETS_ENABLED)
    {
        CliPrintf (CliHandle, "\r%-25s:%-8s\n", "Willing Status", "Enabled");
    }
    else
    {
        CliPrintf (CliHandle, "\r%-25s:%-8s\n", "Willing Status", "Disabled");
    }

    nmhGetLldpXdot1dcbxAdminETSConCreditBasedShaperSupport (i4IfIndex,
                                                            &i4ETSAdmCBS);
    if (i4ETSAdmCBS == ETS_CBS_SUPPORTED)
    {
        CliPrintf (CliHandle,
                   "\r%-25s: %-8s\n", "Credit Based Shaper Algorithm",
                   "Supported");
    }
    else
    {
        CliPrintf (CliHandle,
                   "\r%-25s: %-8s\n", "Credit Based Shaper Algorithm",
                   "Not Supported");
    }

    CliPrintf (CliHandle, "\n\r");
    CliPrintf (CliHandle, "%s",
               "Terminology Used in Above Table:\r\n"
               "--------------------------------\r\n"
               "SPA  := Strict Priority Algorithm\r\n"
               "CBSA := Credit Based Shaper Algorithm\n\r"
               "ETSA := Enhanced Transmission Selection Algorithm\r\n"
               "VSA  := Vendor Specific Algorithm\r\n");
    CliPrintf (CliHandle, "\n\r");
    return (CLI_SUCCESS);
}

/*****************************************************************************
 * Function Name     : ETSCliShowCEERemPortInfo                                  
 *
 * Description       : This function is used to display the ETS Remote 
 *                     Port status.
 *
 * Input(s)          : CliHandle - Handle to CLI session     
 *                   : i4IfIndex - Interface Index  
 *
 * Output(s)         : None.                                               
 *
 * Return Value(s)   : CLI_SUCCESS/CLI_FAILURE           
 *
 * Called By         : ETSCliShowCEEPortDetails                                                
*****************************************************************************/
PRIVATE INT4
ETSCliShowCEERemPortInfo (tCliHandle CliHandle, INT4 i4IfIndex)
{
    UINT4               u4PriIdx = DCBX_ZERO;
    UINT4               u4LldpV2RemTimeMark = DCBX_ZERO;
    UINT4               u4EtsConBw = DCBX_ZERO;
    INT4                i4TgIdx = DCBX_ZERO;
    INT4                i4ETSRemWilling = DCBX_ZERO;
    INT4                i4ETSRemTCGID = DCBX_ZERO;
    INT4                i4ETSTcSup = DCBX_ZERO;
    INT4                i4LldpV2RemLocalIfIndex = DCBX_ZERO;
    INT4                i4LldpV2RemIndex = DCBX_ZERO;
    INT4                i4EtsConTsa = DCBX_ZERO;
    UINT4               u4LldpV2RemLocalDestMACAddress;

    nmhGetNextIndexLldpXdot1dcbxRemETSBasicConfigurationTable
        (DCBX_ZERO, &u4LldpV2RemTimeMark, i4IfIndex,
         &i4LldpV2RemLocalIfIndex, DCBX_ZERO,
         &u4LldpV2RemLocalDestMACAddress, DCBX_ZERO, &i4LldpV2RemIndex);
    if ((i4IfIndex != i4LldpV2RemLocalIfIndex) ||
        (i4LldpV2RemIndex == DCBX_ZERO))
    {
        CliPrintf (CliHandle, "\rNo Remote Entry is Present\n\r");
        return (CLI_SUCCESS);
    }
    CliPrintf (CliHandle, "\r TGID   Bandwidth " " TSA  Priority\n");
    CliPrintf (CliHandle,
               "\r---------------------------------------------------------\n");
    for (i4TgIdx = DCBX_ZERO; i4TgIdx < ETS_MAX_TCGID_CONF; i4TgIdx++)
    {
        nmhGetLldpXdot1dcbxRemETSConTrafficClassBandwidth
            (u4LldpV2RemTimeMark, i4IfIndex, u4LldpV2RemLocalDestMACAddress,
             i4LldpV2RemIndex, (UINT4) i4TgIdx, &(u4EtsConBw));

        nmhGetLldpXdot1dcbxRemETSConTrafficSelectionAlgorithm
            (u4LldpV2RemTimeMark, i4IfIndex, u4LldpV2RemLocalDestMACAddress,
             i4LldpV2RemIndex, (UINT4) i4TgIdx, &(i4EtsConTsa));

        CliPrintf (CliHandle, "%6d", i4TgIdx);
        CliPrintf (CliHandle, "%8d%%", u4EtsConBw);

        CliPrintf (CliHandle, "    ");
        CliPrintf (CliHandle, "%-5s", " ETSA ");
        CliPrintf (CliHandle, "        ");

        for (u4PriIdx = DCBX_ZERO; u4PriIdx < DCBX_MAX_PRIORITIES; u4PriIdx++)
        {

            nmhGetLldpXdot1dcbxRemETSConPriTrafficClass (u4LldpV2RemTimeMark,
                                                         i4LldpV2RemLocalIfIndex,
                                                         u4LldpV2RemLocalDestMACAddress,
                                                         i4LldpV2RemIndex,
                                                         u4PriIdx,
                                                         (UINT4 *)
                                                         &i4ETSRemTCGID);
            if (i4ETSRemTCGID == i4TgIdx)
            {
                CliPrintf (CliHandle, "%d ", u4PriIdx);
            }
        }
        CliPrintf (CliHandle, "\n\r");
    }
    CliPrintf (CliHandle, "\n\r");

    nmhGetLldpXdot1dcbxRemETSConTrafficClassesSupported (u4LldpV2RemTimeMark,
                                                         i4LldpV2RemLocalIfIndex,
                                                         u4LldpV2RemLocalDestMACAddress,
                                                         i4LldpV2RemIndex,
                                                         (UINT4 *) &i4ETSTcSup);
    if (i4ETSTcSup == DCBX_ZERO)
    {
        i4ETSTcSup = ETS_TCGID8;
    }
    CliPrintf (CliHandle, "\r%-25s:%d\n", "Number of Traffic Class",
               i4ETSTcSup);
    nmhGetLldpXdot1dcbxRemETSConWilling (u4LldpV2RemTimeMark,
                                         i4LldpV2RemLocalIfIndex,
                                         u4LldpV2RemLocalDestMACAddress,
                                         i4LldpV2RemIndex, &i4ETSRemWilling);
    if (i4ETSRemWilling == ETS_ENABLED)
    {
        CliPrintf (CliHandle, "\r%-25s:%-8s\n", "Willing Status", "Enabled");
    }
    else
    {
        CliPrintf (CliHandle, "\r%-25s:%-8s\n", "Willing Status", "Disabled");
    }

    CliPrintf (CliHandle, "\n\r");
    CliPrintf (CliHandle, "In CEE Operation, always TSA will be elected as \n"
               "Enhanced Transmission Selection Algorithm (ETSA). \r\n");
    CliPrintf (CliHandle, "\n\r");
    return (CLI_SUCCESS);
}

#endif
