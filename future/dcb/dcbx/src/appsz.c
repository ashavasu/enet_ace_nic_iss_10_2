/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: appsz.c,v 1.5 2013/12/18 12:48:32 siva Exp $
 *
 * Description: This files for RADIUS.
 *******************************************************************/

#define _APPSZ_C
#include "dcbxinc.h"
extern INT4         IssSzRegisterModuleSizingParams (CHR1 * pu1ModName,
                                                     tFsModSizingParams *
                                                     pModSizingParams);
extern INT4         IssSzRegisterModulePoolId (CHR1 * pu1ModName,
                                               tMemPoolId * pModPoolId);
INT4
AppSizingMemCreateMemPools ()
{
    INT4                i4RetVal;
    INT4                i4SizingId;

    for (i4SizingId = 0; i4SizingId < APP_MAX_SIZING_ID; i4SizingId++)
    {
        i4RetVal = MemCreateMemPool (FsAPPSizingParams[i4SizingId].u4StructSize,
                                     FsAPPSizingParams[i4SizingId].
                                     u4PreAllocatedUnits,
                                     MEM_DEFAULT_MEMORY_TYPE,
                                     &(APPMemPoolIds[i4SizingId]));
        if (i4RetVal == (INT4) MEM_FAILURE)
        {
            AppSizingMemDeleteMemPools ();
            return OSIX_FAILURE;
        }
    }
    return OSIX_SUCCESS;
}

INT4
AppSzRegisterModuleSizingParams (CHR1 * pu1ModName)
{
    /* Copy the Module Name */
    IssSzRegisterModuleSizingParams (pu1ModName, FsAPPSizingParams);
    IssSzRegisterModulePoolId (pu1ModName, APPMemPoolIds);
    return OSIX_SUCCESS;
}

VOID
AppSizingMemDeleteMemPools ()
{
    INT4                i4SizingId;

    for (i4SizingId = 0; i4SizingId < APP_MAX_SIZING_ID; i4SizingId++)
    {
        if (APPMemPoolIds[i4SizingId] != 0)
        {
            MemDeleteMemPool (APPMemPoolIds[i4SizingId]);
            APPMemPoolIds[i4SizingId] = 0;
        }
    }
    return;
}
