/*************************************************************************
 * Copyright (C) 2010 Aricent Inc . All Rights Reserved
 * $Id: dcbxweb.c,v 1.16 2016/07/09 09:41:21 siva Exp $
 * Description: This file contains Functions to support web-specific pages 
 *              for ETS,PFC and DCBX .
****************************************************************************/

#include "dcbxinc.h"
#include "fsdcbxcli.h"
#include "stddcbcli.h"

/* Proto types for DCBX WEB */
/* ETS HTML Pages Prototypes */
PRIVATE VOID ETSIssProcessPriorityPageGet PROTO ((tHttp * pHttp));
PRIVATE VOID ETSIssProcessPriorityPageSet PROTO ((tHttp * pHttp));
PRIVATE VOID ETSIssProcessBandwidthPageGet PROTO ((tHttp * pHttp));
PRIVATE VOID ETSIssProcessBandwidthPageSet PROTO ((tHttp * pHttp));
PRIVATE VOID ETSIssProcessAdminInfoPageGet PROTO ((tHttp * pHttp));
PRIVATE VOID ETSIssProcessAdminInfoPageSet PROTO ((tHttp * pHttp));
PRIVATE VOID ETSIssProcessTsaInfoPageGet PROTO ((tHttp * pHttp));
PRIVATE VOID ETSIssProcessTsaInfoPageSet PROTO ((tHttp * pHttp));

/*PFC HTML pages Prototypes*/
PRIVATE VOID PFCIssProcessPageGet PROTO ((tHttp * pHttp));
PRIVATE VOID PFCIssProcessPageSet PROTO ((tHttp * pHttp));
PRIVATE VOID PFCIssProcessAdminInfoPageGet PROTO ((tHttp * pHttp));
PRIVATE VOID PFCIssProcessAdminInfoPageSet PROTO ((tHttp * pHttp));
/*Application Priority HTML pages Prototypes*/
PRIVATE VOID AppPriIssProcessPageGet PROTO ((tHttp * pHttp));
PRIVATE VOID AppPriIssProcessPageSet PROTO ((tHttp * pHttp));
PRIVATE VOID AppPriIssProcessAdminInfoPageGet PROTO ((tHttp * pHttp));
PRIVATE VOID AppPriIssProcessAdminInfoPageSet PROTO ((tHttp * pHttp));

/*********************************************************************
*  Function Name : PFCIssProcessPage
*  Description   : This function processes the request coming for the
*                  Dcb PFC page.
*
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/
PUBLIC VOID
PFCIssProcessPage (tHttp * pHttp)
{
    if (pHttp->i4Method == ISS_GET)
    {
        PFCIssProcessPageGet (pHttp);
    }
    else if (pHttp->i4Method == ISS_SET)
    {
        PFCIssProcessPageSet (pHttp);
    }
}

/*********************************************************************
*  Function Name : PFCIssProcessPageGet 
*  Description   : This function processes the get request coming for the
*                  DCB PFC page.
*
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/

PRIVATE VOID
PFCIssProcessPageGet (tHttp * pHttp)
{
    INT4                i4CurrentPort = DCBX_ZERO;
    INT4                i4NextPort = DCBX_ZERO;
    INT4                i4Priority = DCBX_ZERO;
    INT4                i4OutCome = DCBX_ZERO;
    INT4                i4RetVal = DCBX_ZERO;
    UINT4               u4Temp = DCBX_ZERO;
    INT1               *piIfName = NULL;
    UINT1               au1IfName[CFA_MAX_PORT_NAME_LENGTH] = { DCBX_ZERO };

    piIfName = (INT1 *) &au1IfName[DCBX_ZERO];
    pHttp->i4Write = DCBX_ZERO;

    WebnmRegisterLock (pHttp, DcbxLock, DcbxUnLock);
    DcbxLock ();

    WebnmSendString (pHttp, ISS_TABLE_FLAG);

    i4OutCome = (INT4) (nmhGetFirstIndexFsPFCPortTable (&i4NextPort));
    if (i4OutCome == SNMP_FAILURE)
    {
        DcbxUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
        return;
    }

    u4Temp = (UINT4) pHttp->i4Write;
    do
    {
        CfaCliGetIfName ((UINT4) i4NextPort, piIfName);
        pHttp->i4Write = (INT4) u4Temp;
        STRCPY (pHttp->au1KeyString, "PORT_NO_KEY_NAME");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", (CHR1 *) piIfName);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        STRCPY (pHttp->au1KeyString, "PORT_NO_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4NextPort);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        for (i4Priority = DCBX_ZERO; i4Priority < DCBX_MAX_PRIORITIES;
             i4Priority++)
        {
            if (nmhGetLldpXdot1dcbxAdminPFCEnableEnabled
                (i4NextPort, (UINT4) i4Priority, &i4RetVal) == SNMP_SUCCESS)
            {
                STRCPY (pHttp->au1KeyString, "PRIORITY_STATUS_KEY");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                (INT4) STRLEN (pHttp->au1DataString));
                WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);
            }
        }

        WebnmSendString (pHttp, ISS_TABLE_STOP_FLAG);

        i4CurrentPort = i4NextPort;
    }
    while ((nmhGetNextIndexFsPFCPortTable (i4CurrentPort,
                                           &i4NextPort) == SNMP_SUCCESS));

    DcbxUnLock ();
    WebnmUnRegisterLock (pHttp);
    WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                    (pHttp->i4HtmlSize - pHttp->i4Write));
}

/*********************************************************************
*  Function Name : PFCIssProcessPageSet
*  Description   : This function processes the set request coming for the
*                  DCB PFC page.
*
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/

PRIVATE VOID
PFCIssProcessPageSet (tHttp * pHttp)
{
    INT4                i4Priority = DCBX_ZERO;
    INT4                i4PortNo = DCBX_ZERO;
    INT4                i4PriStatus = DCBX_ZERO;
    UINT4               u4ErrCode = DCBX_ZERO;

    STRCPY (pHttp->au1Name, "PORT_NO");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4PortNo = ATOI (pHttp->au1Value);

    DcbxLock ();

    for (i4Priority = DCBX_ZERO; i4Priority < DCBX_MAX_PRIORITIES; i4Priority++)
    {
        HttpGetNameValue (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery,
                          (UINT4) (i4Priority + DCBX_WEB_COUNT_THREE));
        i4PriStatus = ATOI (pHttp->au1Value);
        if (nmhTestv2LldpXdot1dcbxAdminPFCEnableEnabled
            (&u4ErrCode, i4PortNo, (UINT4) i4Priority,
             i4PriStatus) == SNMP_FAILURE)
        {
            DcbxUnLock ();
            IssSendError (pHttp,
                          (CONST INT1 *)
                          "Unable to set the PFC Priority status ");
            return;
        }

        if (nmhSetLldpXdot1dcbxAdminPFCEnableEnabled
            (i4PortNo, i4Priority, i4PriStatus) == SNMP_FAILURE)
        {
            DcbxUnLock ();
            IssSendError (pHttp,
                          (CONST INT1 *)
                          "Unable to set the PFC Priority Status");
            return;
        }
    }

    DcbxUnLock ();
    PFCIssProcessPageGet (pHttp);
}

/*********************************************************************
*  Function Name : PFCIssProcessLocalPage 
*  Description   : This function processes the get request coming for the
*                  PFC Local page.
*
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/

PUBLIC VOID
PFCIssProcessLocalPage (tHttp * pHttp)
{
    INT4                i4CurrentPort = DCBX_ZERO;
    INT4                i4NextPort = DCBX_ZERO;
    INT4                i4Priority = DCBX_ZERO;
    INT4                i4OutCome = DCBX_ZERO;
    INT4                i4RetVal = DCBX_ZERO;
    UINT4               u4Temp = DCBX_ZERO;
    INT1               *piIfName = NULL;
    UINT1               au1IfName[CFA_MAX_PORT_NAME_LENGTH] = { DCBX_ZERO };

    piIfName = (INT1 *) &au1IfName[DCBX_ZERO];
    pHttp->i4Write = DCBX_ZERO;

    WebnmRegisterLock (pHttp, DcbxLock, DcbxUnLock);
    DcbxLock ();

    WebnmSendString (pHttp, ISS_TABLE_FLAG);

    i4OutCome = (INT4) (nmhGetFirstIndexFsPFCPortTable (&i4NextPort));
    if (i4OutCome == SNMP_FAILURE)
    {
        DcbxUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
        return;
    }

    u4Temp = (UINT4) pHttp->i4Write;
    do
    {
        CfaCliGetIfName ((UINT4) i4NextPort, piIfName);
        pHttp->i4Write = (INT4) u4Temp;
        STRCPY (pHttp->au1KeyString, "PORT_NO_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", (CHR1 *) piIfName);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        STRCPY (pHttp->au1KeyString, "LOCAL_PFC_WILLING_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        nmhGetLldpXdot1dcbxLocPFCWilling (i4NextPort, &i4RetVal);
        if (i4RetVal == DCBX_ENABLED)
        {
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", "Enabled");
        }
        else
        {
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", "Disabled");
        }

        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        STRCPY (pHttp->au1KeyString, "LOCAL_PFC_CAPABILITY_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        nmhGetLldpXdot1dcbxLocPFCCap (i4NextPort, (UINT4 *) &i4RetVal);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) HTTP_STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        STRCPY (pHttp->au1KeyString, "LOCAL_MBS_STATUS_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        nmhGetLldpXdot1dcbxLocPFCMBC (i4NextPort, &i4RetVal);
        if (i4RetVal == DCBX_ENABLED)
        {
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", "Enabled");
        }
        else
        {
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", "Disabled");
        }

        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        for (i4Priority = DCBX_ZERO; i4Priority < DCBX_MAX_PRIORITIES;
             i4Priority++)
        {
            if (nmhGetLldpXdot1dcbxLocPFCEnableEnabled
                (i4NextPort, (UINT4) i4Priority, &i4RetVal) == SNMP_SUCCESS)
            {
                STRCPY (pHttp->au1KeyString, "LOCAL_PRIORITY_STATUS_KEY");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                if (i4RetVal == PFC_ENABLED)
                {
                    SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", "Enabled");
                }
                else
                {
                    SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", "Disabled");
                }

                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                (INT4) STRLEN (pHttp->au1DataString));
                WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);
            }
        }

        WebnmSendString (pHttp, ISS_TABLE_STOP_FLAG);
        i4CurrentPort = i4NextPort;
    }
    while ((nmhGetNextIndexFsPFCPortTable (i4CurrentPort,
                                           &i4NextPort) == SNMP_SUCCESS));

    DcbxUnLock ();
    WebnmUnRegisterLock (pHttp);
    WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                    (pHttp->i4HtmlSize - pHttp->i4Write));
}

/*********************************************************************
*  Function Name : PFCIssProcessRemotePage 
*  Description   : This function processes the get request coming for the
*                  PFC Local page.
*
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/

PUBLIC VOID
PFCIssProcessRemotePage (tHttp * pHttp)
{
    INT4                i4CurrentPort = DCBX_ZERO;
    INT4                i4NextPort = DCBX_ZERO;
    INT4                i4Priority = DCBX_ZERO;
    INT4                i4OutCome = DCBX_ZERO;
    INT4                i4RetVal = DCBX_ZERO;
    UINT4               u4Temp = DCBX_ZERO;
    INT1               *piIfName = NULL;
    UINT1               au1IfName[CFA_MAX_PORT_NAME_LENGTH] = { DCBX_ZERO };
    UINT4               u4LldpV2RemLocalDestMACAddress = DCBX_ZERO;
    UINT4               u4LldpV2RemTimeMark = DCBX_ZERO;
    INT4                i4LldpV2RemLocalIfIndex = DCBX_ZERO;
    INT4                i4LldpV2RemIndex = DCBX_ZERO;


    piIfName = (INT1 *) &au1IfName[DCBX_ZERO];
    pHttp->i4Write = DCBX_ZERO;

    WebnmRegisterLock (pHttp, DcbxLock, DcbxUnLock);
    DcbxLock ();

    WebnmSendString (pHttp, ISS_TABLE_FLAG);

    i4OutCome = (INT4) (nmhGetFirstIndexFsPFCPortTable (&i4NextPort));

    if (i4OutCome == SNMP_FAILURE)
    {
        DcbxUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
        return;
    }

    u4Temp = (UINT4) pHttp->i4Write;
    do
    {
        nmhGetNextIndexLldpXdot1dcbxRemPFCBasicTable
            (DCBX_ZERO, &u4LldpV2RemTimeMark,
             i4NextPort, &i4LldpV2RemLocalIfIndex,
             DCBX_ZERO, &u4LldpV2RemLocalDestMACAddress,
             DCBX_ZERO, &i4LldpV2RemIndex);
        if (i4LldpV2RemIndex == DCBX_ZERO)
        {
            DcbxUnLock ();
            WebnmUnRegisterLock (pHttp);
            return;
        }

        CfaCliGetIfName ((UINT4) i4NextPort, piIfName);
        pHttp->i4Write = (INT4) u4Temp;
        STRCPY (pHttp->au1KeyString, "PORT_NO_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", (CHR1 *) piIfName);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        STRCPY (pHttp->au1KeyString, "REMOTE_PFC_WILLING_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        nmhGetLldpXdot1dcbxRemPFCWilling (u4LldpV2RemTimeMark,
                                          i4NextPort,
                                          u4LldpV2RemLocalDestMACAddress,
                                          i4LldpV2RemIndex, &i4RetVal);
        if (i4RetVal == DCBX_ENABLED)
        {
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", "Enabled");
        }
        else
        {
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", "Disabled");
        }

        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        /* Remote MAC address */
        STRCPY (pHttp->au1KeyString, "REMOTE_MAC_ADD_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", u4LldpV2RemLocalDestMACAddress);

        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        STRCPY (pHttp->au1KeyString, "REMOTE_PFC_CAPABILITY_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        nmhGetLldpXdot1dcbxRemPFCCap (u4LldpV2RemTimeMark,
                                      i4NextPort, u4LldpV2RemLocalDestMACAddress,
                                      i4LldpV2RemIndex, (UINT4 *) &i4RetVal);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) HTTP_STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        STRCPY (pHttp->au1KeyString, "REMOTE_MBS_STATUS_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        nmhGetLldpXdot1dcbxRemPFCMBC (u4LldpV2RemTimeMark,
                                      i4NextPort, u4LldpV2RemLocalDestMACAddress,
                                      i4LldpV2RemIndex, &i4RetVal);
        if (i4RetVal == DCBX_ENABLED)
        {
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", "Enabled");
        }
        else
        {
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", "Disabled");
        }

        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        for (i4Priority = DCBX_ZERO; i4Priority < DCBX_MAX_PRIORITIES;
             i4Priority++)
        {
            if (nmhGetLldpXdot1dcbxRemPFCEnableEnabled
                (u4LldpV2RemTimeMark, i4NextPort,
                 u4LldpV2RemLocalDestMACAddress,
                 i4LldpV2RemIndex, (UINT4) i4Priority,
                 &i4RetVal) == SNMP_SUCCESS)
            {
                STRCPY (pHttp->au1KeyString, "REMOTE_PRIORITY_STATUS_KEY");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                if (i4RetVal == PFC_ENABLED)
                {
                    SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", "Enabled");
                }
                else
                {
                    SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", "Disabled");
                }

                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                (INT4) STRLEN (pHttp->au1DataString));
                WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);
            }
        }

        WebnmSendString (pHttp, ISS_TABLE_STOP_FLAG);
        i4CurrentPort = i4NextPort;
    }
    while ((nmhGetNextIndexFsPFCPortTable (i4CurrentPort,
                                           &i4NextPort) == SNMP_SUCCESS));

    DcbxUnLock ();
    WebnmUnRegisterLock (pHttp);
    WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                    (pHttp->i4HtmlSize - pHttp->i4Write));
}

/*********************************************************************
 *  Function Name : PFCIssProcessAdminInfoPage
 *  Description   : This function processes the request coming for the  
 *                  PFC Admin Configuration page. 
 *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 *  Output(s)     : None.
 *  Return Values : None
 *********************************************************************/
PUBLIC VOID
PFCIssProcessAdminInfoPage (tHttp * pHttp)
{
    if (pHttp->i4Method == ISS_GET)
    {
        PFCIssProcessAdminInfoPageGet (pHttp);
    }
    else if (pHttp->i4Method == ISS_SET)
    {
        PFCIssProcessAdminInfoPageSet (pHttp);
    }
}

/*********************************************************************
 *  Function Name : PFCIssProcessAdminInfoPageGet
 *  Description   : This function processes the get request coming for the  
 *                  PFC Admin Configuration page. 
 *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 *  Output(s)     : None.
 *  Return Values : None
 *********************************************************************/
PRIVATE VOID
PFCIssProcessAdminInfoPageGet (tHttp * pHttp)
{
    INT4                i4CurrentPort = DCBX_ZERO;
    INT4                i4NextPort = DCBX_ZERO;
    INT4                i4RetVal = DCBX_ZERO;
    INT4                i4OutCome = DCBX_ZERO;
    UINT4               u4Temp = DCBX_ZERO;
    INT1               *piIfName = NULL;
    UINT1               au1IfName[CFA_MAX_PORT_NAME_LENGTH] = { DCBX_ZERO };

    MEMSET (au1IfName, DCBX_ZERO, CFA_MAX_PORT_NAME_LENGTH);
    piIfName = (INT1 *) &au1IfName[DCBX_ZERO];
    pHttp->i4Write = DCBX_ZERO;

    WebnmRegisterLock (pHttp, DcbxLock, DcbxUnLock);
    DcbxLock ();

    WebnmSendString (pHttp, ISS_TABLE_FLAG);
    i4OutCome = (INT4) (nmhGetFirstIndexFsPFCPortTable (&i4NextPort));

    if (i4OutCome == SNMP_FAILURE)
    {
        DcbxUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
        return;
    }

    u4Temp = (UINT4) pHttp->i4Write;
    do
    {
        CfaCliGetIfName ((UINT4) i4NextPort, piIfName);
        pHttp->i4Write = (INT4) u4Temp;
        /*port Number */
        STRCPY (pHttp->au1KeyString, "PORT_NO_KEY_NAME");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", (CHR1 *) piIfName);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        STRCPY (pHttp->au1KeyString, "PORT_NO_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4NextPort);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);
        /*Willing */
        STRCPY (pHttp->au1KeyString, "PFC_WILLING_STATUS_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        if (nmhGetLldpXdot1dcbxAdminPFCWilling (i4NextPort, &i4RetVal) ==
            SNMP_SUCCESS)
        {
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
        }

        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);
        /* PFC TLV */
        STRCPY (pHttp->au1KeyString, "PFC_TLV_STATUS_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        if (nmhGetLldpXdot1dcbxConfigPFCTxEnable (i4NextPort,
                                                  DCBX_DEST_ADDR_INDEX,
                                                  &i4RetVal) == SNMP_SUCCESS)
        {
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
        }
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        /*Admin Mode */
        STRCPY (pHttp->au1KeyString, "PFC_ADMIN_MODE_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        if (nmhGetFsPFCAdminMode (i4NextPort, &i4RetVal) == SNMP_SUCCESS)
        {
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
        }

        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        /* Opersation State */
        STRCPY (pHttp->au1KeyString, "OPER_STATE_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        if (nmhGetFsPFCDcbxOperState (i4NextPort, &i4RetVal) == SNMP_SUCCESS)
        {
            if (i4RetVal == PFC_OPER_OFF)
            {
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", "OFF_STATE");

            }
            else if (i4RetVal == PFC_OPER_INIT)
            {
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", "INIT_STATE");

            }
            else
            {
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", "RECO_STATE");

            }
        }
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        /* State Machine */
        STRCPY (pHttp->au1KeyString, "STATE_MACHNINE_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        if (nmhGetFsPFCDcbxStateMachine (i4NextPort, &i4RetVal) == SNMP_SUCCESS)
        {
            if (i4RetVal == DCBX_ASYM_STATE_MACHINE)
            {
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", "Asymmetric");

            }
            else
            {
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", "Symmetric");

            }
        }
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

         /*MBC*/ STRCPY (pHttp->au1KeyString, "MBC_STATUS_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        if (nmhGetLldpXdot1dcbxLocPFCMBC (i4NextPort, &i4RetVal)
            == SNMP_SUCCESS)
        {
            if (i4RetVal == DCBX_ENABLED)
            {
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", "Enabled");

            }
            else
            {
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", "Disabled");

            }

        }
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        /*PFC capability */
        STRCPY (pHttp->au1KeyString, "PFC_CAP_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        if (nmhGetLldpXdot1dcbxLocPFCCap (i4NextPort, (UINT4 *) &i4RetVal)
            == SNMP_SUCCESS)
        {
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
        }
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        WebnmSendString (pHttp, ISS_TABLE_STOP_FLAG);
        i4CurrentPort = i4NextPort;
    }
    while ((nmhGetNextIndexFsPFCPortTable (i4CurrentPort,
                                           &i4NextPort) == SNMP_SUCCESS));

    DcbxUnLock ();
    WebnmUnRegisterLock (pHttp);
    WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                    (pHttp->i4HtmlSize - pHttp->i4Write));
}

/*********************************************************************
 *  Function Name : PFCIssProcessAdminInfoPageSet
 *  Description   : This function processes the get request coming for the  
 *                  PFC Admin Configuration page. 
 *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 *  Output(s)     : None.
 *  Return Values : None
 *********************************************************************/
PRIVATE VOID
PFCIssProcessAdminInfoPageSet (tHttp * pHttp)
{
    INT4                i4WillingStatus = DCBX_ZERO;
    INT4                i4AdminMode = DCBX_ZERO;
    INT4                i4PFCTLVStatus = DCBX_ZERO;
    UINT4               u4ErrorCode = DCBX_ZERO;
    INT4                i4IfIndex = DCBX_ZERO;

    WebnmRegisterLock (pHttp, DcbxLock, DcbxUnLock);
    DcbxLock ();

    /* Port NO */
    STRCPY (pHttp->au1Name, "PORT_NO");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4IfIndex = ATOI (pHttp->au1Value);
    /* willing Status */
    STRCPY (pHttp->au1Name, "PFC_WILLING_STATUS");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4WillingStatus = ATOI (pHttp->au1Value);

    /* PFC TLV */
    STRCPY (pHttp->au1Name, "PFC_TLV_STATUS");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4PFCTLVStatus = ATOI (pHttp->au1Value);
    /* PFC Admin Mode */
    STRCPY (pHttp->au1Name, "PFC_ADMIN_MODE");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4AdminMode = ATOI (pHttp->au1Value);

    STRCPY (pHttp->au1Name, "ACTION");
    if ((HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                             pHttp->au1PostQuery)) != ENM_SUCCESS)
    {
        DcbxUnLock ();
        IssSendError (pHttp, (CONST INT1 *) "Invalid Request");
        return;
    }

    /* check for delete */
    if (STRCMP (pHttp->au1Value, "delete") == DCBX_EQUAL)
    {
        if (nmhTestv2FsPFCRowStatus (&u4ErrorCode, i4IfIndex,
                                     DESTROY) == SNMP_FAILURE)
        {
            DcbxUnLock ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp, (CONST INT1 *) "PFC Port not available.");
            return;
        }

        if (nmhSetFsPFCRowStatus (i4IfIndex, DESTROY) == SNMP_FAILURE)
        {
            DcbxUnLock ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp, (CONST INT1 *) "PFC port  not available.");
            return;
        }

    }
    else if ((STRCMP (pHttp->au1Value, "apply") == DCBX_EQUAL) ||
             (STRCMP (pHttp->au1Value, "Add") == DCBX_EQUAL))
    {
        if (STRCMP (pHttp->au1Value, "Add") == DCBX_EQUAL)
        {
            if (nmhTestv2FsPFCRowStatus (&u4ErrorCode, i4IfIndex,
                                         CREATE_AND_GO) == SNMP_FAILURE)
            {
                DcbxUnLock ();
                WebnmUnRegisterLock (pHttp);
                IssSendError (pHttp,
                              (CONST INT1 *)
                              "PFC can't be created on this port.");
                return;
            }

            if (nmhSetFsPFCRowStatus (i4IfIndex, CREATE_AND_GO) == SNMP_FAILURE)
            {
                DcbxUnLock ();
                WebnmUnRegisterLock (pHttp);
                IssSendError (pHttp,
                              (CONST INT1 *)
                              "PFC can't be created for this port.");
                return;
            }
        }
        /*willing */
        if (nmhTestv2LldpXdot1dcbxAdminPFCWilling (&u4ErrorCode, i4IfIndex,
                                                   i4WillingStatus) ==
            SNMP_FAILURE)
        {
            DcbxUnLock ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp,
                          (CONST INT1 *)
                          "Willing Status canot be set on this port.");
            return;
        }

        if (nmhSetLldpXdot1dcbxAdminPFCWilling (i4IfIndex, i4WillingStatus)
            == SNMP_FAILURE)
        {
            DcbxUnLock ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp,
                          (CONST INT1 *)
                          "Willing Status canot be set on this port..");
            return;
        }

        /*PFC TLV */
        if (nmhTestv2LldpXdot1dcbxConfigPFCTxEnable
            (&u4ErrorCode, i4IfIndex, DCBX_DEST_ADDR_INDEX,
             i4PFCTLVStatus) == SNMP_FAILURE)
        {
            DcbxUnLock ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp,
                          (CONST INT1 *)
                          "PFC TLV Status canot be set on this port.");
            return;
        }

        if (nmhSetLldpXdot1dcbxConfigPFCTxEnable
            (i4IfIndex, DCBX_DEST_ADDR_INDEX, i4PFCTLVStatus) == SNMP_FAILURE)
        {
            DcbxUnLock ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp,
                          (CONST INT1 *)
                          "PFC TLV Status canot be set on this port..");
            return;
        }
        /*Admin Mode */
        if (nmhTestv2FsPFCAdminMode (&u4ErrorCode, i4IfIndex,
                                     i4AdminMode) == SNMP_FAILURE)
        {
            DcbxUnLock ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp,
                          (CONST INT1 *)
                          "Admin Mode Status canot be set on this port.");
            return;
        }

        if (nmhSetFsPFCAdminMode (i4IfIndex, i4AdminMode) == SNMP_FAILURE)
        {
            DcbxUnLock ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp,
                          (CONST INT1 *)
                          "Admin mode Status canot be set on this port..");
            return;
        }

    }
    DcbxUnLock ();
    WebnmUnRegisterLock (pHttp);
    PFCIssProcessAdminInfoPageGet (pHttp);
    return;
}

/*********************************************************************
*  Function Name : ETSIssProcessPriorityPage
*  Description   : This function processes the request coming for the
*                  Dcb ETS Priority page.
*
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/
PUBLIC VOID
ETSIssProcessPriorityPage (tHttp * pHttp)
{
    if (pHttp->i4Method == ISS_GET)
    {
        ETSIssProcessPriorityPageGet (pHttp);
    }
    else if (pHttp->i4Method == ISS_SET)
    {
        ETSIssProcessPriorityPageSet (pHttp);
    }
}

/*********************************************************************
*  Function Name : ETSIssProcessPriorityPageGet 
*  Description   : This function processes the get request coming for the
*                  DCB ETS Priority page.
*
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/

PRIVATE VOID
ETSIssProcessPriorityPageGet (tHttp * pHttp)
{
    INT4                i4CurrentPort = DCBX_ZERO;
    INT4                i4NextPort = DCBX_ZERO;
    INT4                i4Priority = DCBX_ZERO;
    INT4                i4OutCome = DCBX_ZERO;
    INT4                i4RetVal = DCBX_ZERO;
    UINT4               u4Temp = DCBX_ZERO;
    INT1               *piIfName = NULL;
    UINT1               au1IfName[CFA_MAX_PORT_NAME_LENGTH] = { DCBX_ZERO };

    piIfName = (INT1 *) &au1IfName[DCBX_ZERO];
    pHttp->i4Write = DCBX_ZERO;

    WebnmRegisterLock (pHttp, DcbxLock, DcbxUnLock);
    DcbxLock ();

    WebnmSendString (pHttp, ISS_TABLE_FLAG);

    i4OutCome = (INT4) (nmhGetFirstIndexFsETSPortTable (&i4NextPort));

    if (i4OutCome == SNMP_FAILURE)
    {
        DcbxUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
        return;
    }

    u4Temp = (UINT4) pHttp->i4Write;
    do
    {
        CfaCliGetIfName ((UINT4) i4NextPort, piIfName);
        pHttp->i4Write = (INT4) u4Temp;
        STRCPY (pHttp->au1KeyString, "PORT_NO_KEY_NAME");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", (CHR1 *) piIfName);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        STRCPY (pHttp->au1KeyString, "PORT_NO_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4NextPort);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        for (i4Priority = DCBX_ZERO; i4Priority < DCBX_MAX_PRIORITIES;
             i4Priority++)
        {
            if (nmhGetLldpXdot1dcbxAdminETSConPriTrafficClass
                (i4NextPort, (UINT4) i4Priority,
                 (UINT4 *) &i4RetVal) == SNMP_SUCCESS)
            {
                STRCPY (pHttp->au1KeyString, "PGID_KEY");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                (INT4) STRLEN (pHttp->au1DataString));
                WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);
            }
        }

        WebnmSendString (pHttp, ISS_TABLE_STOP_FLAG);
        i4CurrentPort = i4NextPort;
    }
    while ((nmhGetNextIndexFsETSPortTable (i4CurrentPort,
                                           &i4NextPort) == SNMP_SUCCESS));

    DcbxUnLock ();
    WebnmUnRegisterLock (pHttp);
    WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                    (pHttp->i4HtmlSize - pHttp->i4Write));
}

/*********************************************************************
 *  Function Name : ETSIssProcessPriorityPageSet
 *  Description   : This function processes the set request coming for the
 *                  DCB ETS Priority page.
 *
 *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 *  Output(s)     : None.
 *  Return Values : None
 *********************************************************************/

PRIVATE VOID
ETSIssProcessPriorityPageSet (tHttp * pHttp)
{
    INT4                i4Priority = DCBX_ZERO;
    INT4                i4PortNo = DCBX_ZERO;
    INT4                i4PriGroup = DCBX_ZERO;
    UINT4               u4ErrCode = DCBX_ZERO;

    STRCPY (pHttp->au1Name, "PORT_NO");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4PortNo = ATOI (pHttp->au1Value);

    DcbxLock ();

    for (i4Priority = DCBX_ZERO; i4Priority < DCBX_MAX_PRIORITIES; i4Priority++)
    {
        HttpGetNameValue (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery,
                          (UINT4) (i4Priority + DCBX_WEB_COUNT_THREE));
        i4PriGroup = ATOI (pHttp->au1Value);
        if (nmhTestv2LldpXdot1dcbxAdminETSConPriTrafficClass
            (&u4ErrCode, i4PortNo, (UINT4) i4Priority,
             (UINT4) i4PriGroup) == SNMP_FAILURE)
        {
            DcbxUnLock ();
            IssSendError (pHttp,
                          (CONST INT1 *)
                          "Unable to set the ETS Priority Group  ");
            return;
        }

        if (nmhSetLldpXdot1dcbxAdminETSConPriTrafficClass
            (i4PortNo, (UINT4) i4Priority, (UINT4) i4PriGroup) == SNMP_FAILURE)
        {
            DcbxUnLock ();
            IssSendError (pHttp,
                          (CONST INT1 *)
                          "Unable to set the ETS Priority Group");
            return;
        }
    }

    DcbxUnLock ();
    ETSIssProcessPriorityPageGet (pHttp);
}

/*********************************************************************
*  Function Name : ETSIssProcessBandwidthPage
*  Description   : This function processes the request coming for the
*                  Dcb ETS Bandwidth page.
*
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/
PUBLIC VOID
ETSIssProcessBandwidthPage (tHttp * pHttp)
{
    if (pHttp->i4Method == ISS_GET)
    {
        ETSIssProcessBandwidthPageGet (pHttp);
    }
    else if (pHttp->i4Method == ISS_SET)
    {
        ETSIssProcessBandwidthPageSet (pHttp);
    }
}

/*********************************************************************
*  Function Name : ETSIssProcessBandwidthPageGet 
*  Description   : This function processes the get request coming for the
*                  DCB ETS Bandwidth page.
*
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/

PRIVATE VOID
ETSIssProcessBandwidthPageGet (tHttp * pHttp)
{
    INT4                i4CurrentPort = DCBX_ZERO;
    INT4                i4NextPort = DCBX_ZERO;
    INT4                i4OutCome = DCBX_ZERO;
    INT4                i4Pgid = DCBX_ZERO;
    UINT4               u4Temp = DCBX_ZERO;
    INT1               *piIfName = NULL;
    UINT1               au1IfName[CFA_MAX_PORT_NAME_LENGTH] = { DCBX_ZERO };
    UINT4               au4EtsBW[DCBX_MAX_PRIORITIES] = { DCBX_ZERO };

    MEMSET (au4EtsBW, DCBX_ZERO, sizeof (UINT4) * DCBX_MAX_PRIORITIES);

    piIfName = (INT1 *) &au1IfName[DCBX_ZERO];
    pHttp->i4Write = DCBX_ZERO;

    WebnmRegisterLock (pHttp, DcbxLock, DcbxUnLock);
    DcbxLock ();

    WebnmSendString (pHttp, ISS_TABLE_FLAG);

    i4OutCome = (INT4) (nmhGetFirstIndexFsETSPortTable (&i4NextPort));

    if (i4OutCome == SNMP_FAILURE)
    {
        DcbxUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
        return;
    }

    u4Temp = (UINT4) pHttp->i4Write;
    do
    {
        CfaCliGetIfName ((UINT4) i4NextPort, piIfName);
        pHttp->i4Write = (INT4) u4Temp;
        STRCPY (pHttp->au1KeyString, "PORT_NO_KEY_NAME");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", (CHR1 *) piIfName);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        STRCPY (pHttp->au1KeyString, "PORT_NO_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4NextPort);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        for (i4Pgid = DCBX_ZERO; i4Pgid < DCBX_MAX_PRIORITIES; i4Pgid++)
        {
            nmhGetLldpXdot1dcbxAdminETSConTrafficClassBandwidth
                (i4NextPort, (UINT4) i4Pgid, &(au4EtsBW[i4Pgid]));
            STRCPY (pHttp->au1KeyString, "BW_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", au4EtsBW[i4Pgid]);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);
        }

        MEMSET (au4EtsBW, DCBX_ZERO, sizeof (UINT4) * DCBX_MAX_PRIORITIES);

        for (i4Pgid = DCBX_ZERO; i4Pgid < DCBX_MAX_PRIORITIES; i4Pgid++)
        {
            nmhGetLldpXdot1dcbxAdminETSRecoTrafficClassBandwidth
                (i4NextPort, (UINT4) i4Pgid, &(au4EtsBW[i4Pgid]));

            STRCPY (pHttp->au1KeyString, "RECO_BW_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", au4EtsBW[i4Pgid]);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);
        }

        WebnmSendString (pHttp, ISS_TABLE_STOP_FLAG);
        i4CurrentPort = i4NextPort;
    }
    while ((nmhGetNextIndexFsETSPortTable (i4CurrentPort,
                                           &i4NextPort) == SNMP_SUCCESS));

    DcbxUnLock ();
    WebnmUnRegisterLock (pHttp);
    WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                    (pHttp->i4HtmlSize - pHttp->i4Write));
}

/*********************************************************************
*  Function Name : ETSIssProcessBandwidthPageSet 
*  Description   : This function processes the set request coming for the
*                  DCB PFC page.
*
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/

PRIVATE VOID
ETSIssProcessBandwidthPageSet (tHttp * pHttp)
{
    INT4                i4IfIndex = DCBX_ZERO;
    INT4                i4Pgid = DCBX_ZERO;
    INT4                i4Bandwidth = DCBX_ZERO;
    UINT4               u4ErrCode = DCBX_ZERO;
    UINT4               au4EtsBW[DCBX_MAX_PRIORITIES] = { DCBX_ZERO };
    UINT4               au4RecoEtsBW[DCBX_MAX_PRIORITIES] = { DCBX_ZERO };
    UINT4               u4Index = DCBX_ZERO;
    UINT4               au4ConBwBackup[ETS_MAX_TCGID_CONF] = { DCBX_ZERO };
    UINT4               au4RecoBwBackup[ETS_MAX_TCGID_CONF] = { DCBX_ZERO };
    UINT4               u4Sum = DCBX_ZERO;

    STRCPY (pHttp->au1Name, "PORT_NO");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4IfIndex = ATOI (pHttp->au1Value);

    DcbxLock ();

    for (i4Pgid = DCBX_ZERO; i4Pgid < DCBX_MAX_PRIORITIES; i4Pgid++)
    {
        HttpGetNameValue (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery,
                          (UINT4) (i4Pgid + DCBX_WEB_COUNT_THREE));
        i4Bandwidth = ATOI (pHttp->au1Value);
        au4EtsBW[i4Pgid] = (UINT4) i4Bandwidth;

    }

    for (i4Pgid = DCBX_ZERO; i4Pgid < DCBX_MAX_PRIORITIES; i4Pgid++)
    {
        HttpGetNameValue (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery,
                          (UINT4) (i4Pgid + DCBX_WEB_COUNT_ELEVEN));
        i4Bandwidth = ATOI (pHttp->au1Value);
        au4RecoEtsBW[i4Pgid] = (UINT4) i4Bandwidth;
    }
    for (i4Pgid = DCBX_ZERO; i4Pgid < ETS_MAX_TCGID_CONF; i4Pgid++)
    {
        u4Sum += au4EtsBW[i4Pgid];
    }

    if (u4Sum != ETS_MAX_BW)
    {
        IssSendError (pHttp,
                (CONST INT1 *) "Error: Total Bandwidth sum has to be equal to 100");
        return;
    }
    /* Before setting we are clearing the Admin BW database 
     * and if any error occurs then we are going to revert 
     * the changes to the already set values from au1ConBwBackup
     * that we have taken before clearing the global Admin Database */
    for (u4Index = 0; u4Index <= ETS_TCGID7; u4Index++)
    {
        nmhGetLldpXdot1dcbxAdminETSConTrafficClassBandwidth
            (i4IfIndex, u4Index, &au4ConBwBackup[u4Index]);
    }

    for (u4Index = 0; u4Index <= ETS_TCGID7; u4Index++)
    {
        nmhSetLldpXdot1dcbxAdminETSConTrafficClassBandwidth
            (i4IfIndex, u4Index, DCBX_ZERO);
    }


    for (u4Index = DCBX_ZERO; u4Index < DCBX_MAX_PRIORITIES; u4Index++)
    {
        if (nmhTestv2LldpXdot1dcbxAdminETSConTrafficClassBandwidth
            (&u4ErrCode, i4IfIndex, u4Index, au4EtsBW[u4Index]) == SNMP_FAILURE)
        {
            /* Clearing the database to set the already set value */
            for (u4Index = 0; u4Index <= ETS_TCGID7; u4Index++)
            {
                nmhSetLldpXdot1dcbxAdminETSConTrafficClassBandwidth
                    (i4IfIndex, u4Index, DCBX_ZERO);
            }
            /* Reverting the database to already set values */
            for (u4Index = 0; u4Index <= ETS_TCGID7; u4Index++)
            {
                nmhSetLldpXdot1dcbxAdminETSConTrafficClassBandwidth
                    (i4IfIndex, u4Index, au4ConBwBackup[u4Index]);
            }

            DcbxUnLock ();
            IssSendError (pHttp,
                          (CONST INT1 *) "Unable to set the ETS Bandwidth ");
            return;
        }
    }
    for (u4Index = DCBX_ZERO; u4Index < DCBX_MAX_PRIORITIES; u4Index++)
    {
        nmhSetLldpXdot1dcbxAdminETSConTrafficClassBandwidth
            (i4IfIndex, u4Index, au4EtsBW[u4Index]);
    }

    /* Recommendation Bandwidth */
    u4Sum = DCBX_ZERO;
    for (i4Pgid = DCBX_ZERO; i4Pgid < ETS_MAX_TCGID_CONF; i4Pgid++)
    {
        u4Sum += au4RecoEtsBW[i4Pgid];
    }

    if (u4Sum != ETS_MAX_BW)
    {
        IssSendError (pHttp,
                (CONST INT1 *) "Error: Total Recommended Bandwidth sum has to be equal to 100");
        return;
    }
    /* Before setting we are clearing the Admin Recommended BW database 
     * and if any error occurs then we are going to revert the changes 
     * to the already set values from au1RecoBwBackup that we have taken before 
     * clearing the global Admin Recommended Database */
    for (u4Index = 0; u4Index <= ETS_TCGID7; u4Index++)
    {
        nmhGetLldpXdot1dcbxAdminETSRecoTrafficClassBandwidth
            (i4IfIndex, u4Index, &au4RecoBwBackup[u4Index]);
    }

    for (u4Index = 0; u4Index <= ETS_TCGID7; u4Index++)
    {
        nmhSetLldpXdot1dcbxAdminETSRecoTrafficClassBandwidth
            (i4IfIndex, u4Index, DCBX_ZERO);
    }
    for (u4Index = DCBX_ZERO; u4Index < DCBX_MAX_PRIORITIES; u4Index++)
    {
        if (nmhTestv2LldpXdot1dcbxAdminETSRecoTrafficClassBandwidth
            (&u4ErrCode, i4IfIndex, u4Index,
             au4RecoEtsBW[u4Index]) == SNMP_FAILURE)
        {
            /* Clearing the database to set the already set value */
            for (u4Index = 0; u4Index <= ETS_TCGID7; u4Index++)
            {
                nmhSetLldpXdot1dcbxAdminETSRecoTrafficClassBandwidth
                    (i4IfIndex, u4Index, DCBX_ZERO);
            }
            /* Reverting the database to already set values */
            for (u4Index = 0; u4Index <= ETS_TCGID7; u4Index++)
            {
                nmhSetLldpXdot1dcbxAdminETSRecoTrafficClassBandwidth
                    (i4IfIndex, u4Index, au4RecoBwBackup[u4Index]);
            }
            DcbxUnLock ();
            IssSendError (pHttp,
                    (CONST INT1 *) "Unable to set the ETS Recommended Bandwidth ");
            return;
        }
    }

    for (u4Index = DCBX_ZERO; u4Index < DCBX_MAX_PRIORITIES; u4Index++)
    {
        nmhSetLldpXdot1dcbxAdminETSRecoTrafficClassBandwidth
            (i4IfIndex, u4Index, au4RecoEtsBW[u4Index]);
    }
    DcbxUnLock ();
    ETSIssProcessBandwidthPageGet (pHttp);
}

/*********************************************************************
*  Function Name : ETSIssProcessLocalInfoPage
*  Description   : This function processes the request coming for the
*                  Dcb ETS Local page.
*
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/
PUBLIC VOID
ETSIssProcessLocalInfoPage (tHttp * pHttp)
{
    INT4                i4CurrentPort = DCBX_ZERO;
    INT4                i4NextPort = DCBX_ZERO;
    INT4                i4Priority = DCBX_ZERO;
    INT4                i4RetVal = DCBX_ZERO;
    INT4                i4OutCome = DCBX_ZERO;
    INT4                i4Pgid = DCBX_ZERO;
    INT4                i4ETSLocWilling = DCBX_ZERO;
    INT4                i4ETSTcSup = DCBX_ZERO;
    UINT4               u4Temp = DCBX_ZERO;
    INT1               *piIfName = NULL;
    UINT1               au1IfName[CFA_MAX_PORT_NAME_LENGTH] = { DCBX_ZERO };
    UINT4               au4EtsBW[DCBX_MAX_PRIORITIES] = { DCBX_ZERO };
    INT4                i4TsaId = DCBX_ZERO;

    MEMSET (au4EtsBW, DCBX_ZERO, sizeof (UINT4) * DCBX_MAX_PRIORITIES);

    piIfName = (INT1 *) &au1IfName[DCBX_ZERO];
    pHttp->i4Write = DCBX_ZERO;

    WebnmRegisterLock (pHttp, DcbxLock, DcbxUnLock);
    DcbxLock ();

    WebnmSendString (pHttp, ISS_TABLE_FLAG);
    i4OutCome = (INT4) (nmhGetFirstIndexFsETSPortTable (&i4NextPort));

    if (i4OutCome == SNMP_FAILURE)
    {
        DcbxUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
        return;
    }

    u4Temp = (UINT4) pHttp->i4Write;
    do
    {
        CfaCliGetIfName ((UINT4) i4NextPort, piIfName);
        pHttp->i4Write = (INT4) u4Temp;
        /*port Number */
        STRCPY (pHttp->au1KeyString, "PORT_NO_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", (CHR1 *) piIfName);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        /*Willing */
        STRCPY (pHttp->au1KeyString, "LOCAL_WILLING_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        if (nmhGetLldpXdot1dcbxLocETSConWilling
            (i4NextPort, &i4ETSLocWilling) == SNMP_SUCCESS)
        {
            if (i4ETSLocWilling == DCBX_ENABLED)
            {
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", "Enabled");

            }
            else
            {
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", "Disabled");

            }
        }
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);
        /*no of TC */
        STRCPY (pHttp->au1KeyString, "NO_TC_SUPPORT_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        if (nmhGetLldpXdot1dcbxLocETSConTrafficClassesSupported
            (i4NextPort, (UINT4 *) &i4ETSTcSup) == SNMP_SUCCESS)
        {
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4ETSTcSup);
        }
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        /*Tgid to Pri mapping */

        for (i4Priority = DCBX_ZERO; i4Priority < DCBX_MAX_PRIORITIES;
             i4Priority++)
        {

            if (nmhGetLldpXdot1dcbxLocETSConPriTrafficClass
                (i4NextPort, (UINT4) i4Priority,
                 (UINT4 *) &i4RetVal) == SNMP_SUCCESS)
            {
                STRCPY (pHttp->au1KeyString, "LOCAL_PGID_KEY");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                (INT4) STRLEN (pHttp->au1DataString));
                WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);
            }
        }

        /*conf BW */
        for (i4Pgid = DCBX_ZERO; i4Pgid < DCBX_MAX_PRIORITIES; i4Pgid++)
        {
            if (nmhGetLldpXdot1dcbxLocETSConTrafficClassBandwidth
                (i4NextPort, (UINT4) i4Pgid,
                 &(au4EtsBW[i4Pgid])) == SNMP_SUCCESS)
            {
                STRCPY (pHttp->au1KeyString, "LOCAL_BW_KEY");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", au4EtsBW[i4Pgid]);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                (INT4) STRLEN (pHttp->au1DataString));
                WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);
            }
        }
        /*remo BW */
        MEMSET (au4EtsBW, DCBX_ZERO, sizeof (UINT4) * DCBX_MAX_PRIORITIES);

        for (i4Pgid = DCBX_ZERO; i4Pgid < DCBX_MAX_PRIORITIES; i4Pgid++)
        {
            if (nmhGetLldpXdot1dcbxLocETSRecoTrafficClassBandwidth
                (i4NextPort, (UINT4) i4Pgid,
                 &(au4EtsBW[i4Pgid])) == SNMP_SUCCESS)
            {
                STRCPY (pHttp->au1KeyString, "LOCAL_RECO_BW_KEY");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", au4EtsBW[i4Pgid]);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                (INT4) STRLEN (pHttp->au1DataString));
                WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);
            }
        }

        /****TSA INFORMATION*****/

        /*****CONFIGURATION*****/
        for (i4TsaId = DCBX_ZERO; i4TsaId < ETS_MAX_TCGID_CONF; i4TsaId++)
        {

            if (nmhGetLldpXdot1dcbxLocETSConTrafficSelectionAlgorithm
                (i4NextPort, (UINT4) i4TsaId, &i4RetVal) == SNMP_SUCCESS)
            {

                STRCPY (pHttp->au1KeyString, "LOCAL_TSA_KEY");
                WebnmSendString (pHttp, pHttp->au1KeyString);

                if (i4RetVal == DCBX_STRICT_PRIORITY_ALGO)
                {
                    SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", "SPA");
                }
                else if (i4RetVal == DCBX_CREDIT_BASED_SHAPER_ALGO)
                {
                    SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", "CBSA");
                }
                else if (i4RetVal == DCBX_ENHANCED_TRANSMISSION_SELECTION_ALGO)
                {
                    SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", "ETSA");
                }
                else if (i4RetVal == DCBX_VENDOR_SPECIFIC_ALGO)
                {
                    SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", "VSA");
                }

                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                (INT4) STRLEN (pHttp->au1DataString));
                WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);
            }

        }

       /*******Recommendation******/
        for (i4TsaId = DCBX_ZERO; i4TsaId < ETS_MAX_TCGID_CONF; i4TsaId++)
        {

            if (nmhGetLldpXdot1dcbxLocETSRecoTrafficSelectionAlgorithm
                (i4NextPort, (UINT4) i4TsaId, &i4RetVal) == SNMP_SUCCESS)
            {

                STRCPY (pHttp->au1KeyString, "LOCAL_RECO_TSA_KEY");
                WebnmSendString (pHttp, pHttp->au1KeyString);

                if (i4RetVal == DCBX_STRICT_PRIORITY_ALGO)
                {
                    SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", "SPA");
                }
                else if (i4RetVal == DCBX_CREDIT_BASED_SHAPER_ALGO)
                {
                    SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", "CBSA");
                }
                else if (i4RetVal == DCBX_ENHANCED_TRANSMISSION_SELECTION_ALGO)
                {
                    SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", "ETSA");
                }
                else if (i4RetVal == DCBX_VENDOR_SPECIFIC_ALGO)
                {
                    SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", "VSA");
                }

                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                (INT4) STRLEN (pHttp->au1DataString));
                WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);
            }
        }

        /********************************************/

        WebnmSendString (pHttp, ISS_TABLE_STOP_FLAG);
        i4CurrentPort = i4NextPort;
    }
    while ((nmhGetNextIndexFsETSPortTable (i4CurrentPort,
                                           &i4NextPort) == SNMP_SUCCESS));

    DcbxUnLock ();
    WebnmUnRegisterLock (pHttp);
    WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                    (pHttp->i4HtmlSize - pHttp->i4Write));
}

/*********************************************************************
*  Function Name : ETSIssProcessRemoteInfoPage
*  Description   : This function processes the request coming for the
*                  Dcb ETS Remote page.
*
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/
PUBLIC VOID
ETSIssProcessRemoteInfoPage (tHttp * pHttp)
{
    INT4                i4CurrentPort = DCBX_ZERO;
    INT4                i4NextPort = DCBX_ZERO;
    INT4                i4Priority = DCBX_ZERO;
    INT4                i4TsaId = DCBX_ZERO;
    INT4                i4RetVal = DCBX_ZERO;
    INT4                i4ETSRemTCGID = DCBX_ZERO;
    INT4                i4OutCome = DCBX_ZERO;
    INT4                i4Pgid = DCBX_ZERO;
    INT4                i4ETSRemWilling = DCBX_ZERO;
    INT4                i4ETSTcSup = DCBX_ZERO;
    UINT4               u4Temp = DCBX_ZERO;
    INT1               *piIfName = NULL;
    UINT1               au1IfName[CFA_MAX_PORT_NAME_LENGTH] = { DCBX_ZERO };
    UINT4               au4EtsBW[DCBX_MAX_PRIORITIES] = { DCBX_ZERO };
    INT4                i4LldpV2RemLocalIfIndex = DCBX_ZERO;
    UINT4               u4LldpV2RemTimeMark = DCBX_ZERO;
    INT4                i4LldpV2RemIndex = DCBX_ZERO;
    UINT4            u4LldpV2RemLocalDestMACAddress;

    MEMSET (au4EtsBW, DCBX_ZERO, sizeof (UINT4) * DCBX_MAX_PRIORITIES);

    piIfName = (INT1 *) &au1IfName[DCBX_ZERO];
    pHttp->i4Write = DCBX_ZERO;

    WebnmRegisterLock (pHttp, DcbxLock, DcbxUnLock);
    DcbxLock ();

    WebnmSendString (pHttp, ISS_TABLE_FLAG);

    i4OutCome = (INT4) (nmhGetFirstIndexFsETSPortTable (&i4NextPort));

    if (i4OutCome == SNMP_FAILURE)
    {
        DcbxUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
        return;
    }

    u4Temp = (UINT4) pHttp->i4Write;
    do
    {

        nmhGetNextIndexLldpXdot1dcbxRemETSBasicConfigurationTable
            (DCBX_ZERO, &u4LldpV2RemTimeMark,
             i4NextPort, &i4LldpV2RemLocalIfIndex,
             DCBX_ZERO, &u4LldpV2RemLocalDestMACAddress,
             DCBX_ZERO, &i4LldpV2RemIndex);
        if (i4LldpV2RemIndex == DCBX_ZERO)
        {
            DcbxUnLock ();
            WebnmUnRegisterLock (pHttp);
            return;
        }
        CfaCliGetIfName ((UINT4) i4NextPort, piIfName);
        pHttp->i4Write = (INT4) u4Temp;
        /*port Number */
        STRCPY (pHttp->au1KeyString, "PORT_NO_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", (CHR1 *) piIfName);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        /*Willing */
        STRCPY (pHttp->au1KeyString, "REMOTE_WILLING_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);

        if (nmhGetLldpXdot1dcbxRemETSConWilling (u4LldpV2RemTimeMark,
                                                 i4NextPort,
                                                 u4LldpV2RemLocalDestMACAddress,
                                                 i4LldpV2RemIndex,
                                                 &i4ETSRemWilling) ==
            SNMP_SUCCESS)
        {
            if (i4ETSRemWilling == DCBX_ENABLED)
            {
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", "Enabled");

            }
            else
            {
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", "Disabled");

            }
        }
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);
        /*no of TC */
        STRCPY (pHttp->au1KeyString, "REMOTE_TC_SUPPORT_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);

        if (nmhGetLldpXdot1dcbxRemETSConTrafficClassesSupported
            (u4LldpV2RemTimeMark, i4NextPort, u4LldpV2RemLocalDestMACAddress,
             i4LldpV2RemIndex, (UINT4 *) &i4ETSTcSup) == SNMP_SUCCESS)
        {
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4ETSTcSup);
        }
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        /* Remote MAC address */
        STRCPY (pHttp->au1KeyString, "REMOTE_MAC_ADD_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", u4LldpV2RemLocalDestMACAddress);

        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        /*Tgid to Pri mapping */

        for (i4Priority = DCBX_ZERO; i4Priority < DCBX_MAX_PRIORITIES;
             i4Priority++)
        {

            if (nmhGetLldpXdot1dcbxRemETSConPriTrafficClass
                (u4LldpV2RemTimeMark, i4NextPort, u4LldpV2RemLocalDestMACAddress,
                 i4LldpV2RemIndex, (UINT4) i4Priority,
                 (UINT4 *) &i4ETSRemTCGID) == SNMP_SUCCESS)
            {
                STRCPY (pHttp->au1KeyString, "REMOTE_PGID_KEY");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4ETSRemTCGID);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                (INT4) STRLEN (pHttp->au1DataString));
                WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);
            }
        }

        /*conf BW */
        for (i4Pgid = DCBX_ZERO; i4Pgid < DCBX_MAX_PRIORITIES; i4Pgid++)
        {
            if (nmhGetLldpXdot1dcbxRemETSConTrafficClassBandwidth
                (u4LldpV2RemTimeMark, i4NextPort, u4LldpV2RemLocalDestMACAddress,
                 i4LldpV2RemIndex, (UINT4) i4Pgid,
                 &(au4EtsBW[i4Pgid])) == SNMP_SUCCESS)
            {
                STRCPY (pHttp->au1KeyString, "REMOTE_BW_KEY");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", au4EtsBW[i4Pgid]);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                (INT4) STRLEN (pHttp->au1DataString));
                WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);
            }
        }

        /*remo BW */
        MEMSET (au4EtsBW, DCBX_ZERO, sizeof (UINT4) * DCBX_MAX_PRIORITIES);

        for (i4Pgid = DCBX_ZERO; i4Pgid < DCBX_MAX_PRIORITIES; i4Pgid++)
        {
            if (nmhGetLldpXdot1dcbxRemETSRecoTrafficClassBandwidth
                (u4LldpV2RemTimeMark, i4NextPort, u4LldpV2RemLocalDestMACAddress,
                 i4LldpV2RemIndex, (UINT4) i4Pgid,
                 &(au4EtsBW[i4Pgid])) == SNMP_SUCCESS)
            {
                STRCPY (pHttp->au1KeyString, "REMOTE_RECO_BW_KEY");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", au4EtsBW[i4Pgid]);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                (INT4) STRLEN (pHttp->au1DataString));
                WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);
            }
        }

        /*Remote TSA */
        for (i4TsaId = DCBX_ZERO, i4RetVal = DCBX_ZERO;
             i4TsaId < ETS_MAX_TCGID_CONF; i4TsaId++)
        {
            if (nmhGetLldpXdot1dcbxRemETSConTrafficSelectionAlgorithm
                (u4LldpV2RemTimeMark, i4NextPort, u4LldpV2RemLocalDestMACAddress,
                 i4LldpV2RemIndex, (UINT4) i4TsaId, &i4RetVal) == SNMP_SUCCESS)
            {

                STRCPY (pHttp->au1KeyString, "REMOTE_TSA_KEY");
                WebnmSendString (pHttp, pHttp->au1KeyString);

                if (i4RetVal == DCBX_STRICT_PRIORITY_ALGO)
                {
                    SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", "SPA");
                }
                else if (i4RetVal == DCBX_CREDIT_BASED_SHAPER_ALGO)
                {
                    SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", "CBSA");
                }
                else if (i4RetVal == DCBX_ENHANCED_TRANSMISSION_SELECTION_ALGO)
                {
                    SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", "ETSA");
                }
                else if (i4RetVal == DCBX_VENDOR_SPECIFIC_ALGO)
                {
                    SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", "VSA");
                }

                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                (INT4) STRLEN (pHttp->au1DataString));
                WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            }

        }

        for (i4TsaId = DCBX_ZERO, i4RetVal = DCBX_ZERO;
             i4TsaId < ETS_MAX_TCGID_CONF; i4TsaId++)
        {
            if (nmhGetLldpXdot1dcbxRemETSRecoTrafficSelectionAlgorithm
                (u4LldpV2RemTimeMark, i4NextPort, u4LldpV2RemLocalDestMACAddress,
                 i4LldpV2RemIndex, (UINT4) i4TsaId, &i4RetVal) == SNMP_SUCCESS)

            {

                STRCPY (pHttp->au1KeyString, "REMOTE_RECO_TSA_KEY");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                if (i4RetVal == DCBX_STRICT_PRIORITY_ALGO)
                {
                    SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", "SPA");
                }
                else if (i4RetVal == DCBX_CREDIT_BASED_SHAPER_ALGO)
                {
                    SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", "CBSA");
                }
                else if (i4RetVal == DCBX_ENHANCED_TRANSMISSION_SELECTION_ALGO)
                {
                    SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", "ETSA");
                }
                else if (i4RetVal == DCBX_VENDOR_SPECIFIC_ALGO)
                {
                    SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", "VSA");
                }

                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                (INT4) STRLEN (pHttp->au1DataString));
                WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            }
        }

        WebnmSendString (pHttp, ISS_TABLE_STOP_FLAG);
        i4CurrentPort = i4NextPort;
    }
    while ((nmhGetNextIndexFsETSPortTable (i4CurrentPort,
                                           &i4NextPort) == SNMP_SUCCESS));

    DcbxUnLock ();
    WebnmUnRegisterLock (pHttp);
    WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                    (pHttp->i4HtmlSize - pHttp->i4Write));
}

/*********************************************************************
 *  Function Name : ETSIssProcessAdminInfoPage
 *  Description   : This function processes the request coming for the  
 *                  ETS Admin Configuration page. 
 *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 *  Output(s)     : None.
 *  Return Values : None
 *********************************************************************/
PUBLIC VOID
ETSIssProcessAdminInfoPage (tHttp * pHttp)
{
    if (pHttp->i4Method == ISS_GET)
    {
        ETSIssProcessAdminInfoPageGet (pHttp);
    }
    else if (pHttp->i4Method == ISS_SET)
    {
        ETSIssProcessAdminInfoPageSet (pHttp);
    }
}

/*********************************************************************
 *  Function Name : ETSIssProcessAdminInfoPageGet
 *  Description   : This function processes the get request coming for the  
 *                  LA Interface Settings page. 
 *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 *  Output(s)     : None.
 *  Return Values : None
 *********************************************************************/
PRIVATE VOID
ETSIssProcessAdminInfoPageGet (tHttp * pHttp)
{
    INT4                i4CurrentPort = DCBX_ZERO;
    INT4                i4NextPort = DCBX_ZERO;
    INT4                i4RetVal = DCBX_ZERO;
    INT4                i4OutCome = DCBX_ZERO;
    INT4                i4ETSAdminWilling = DCBX_ZERO;
    INT4                i4ETSTcSup = DCBX_ZERO;
    UINT4               u4Temp = DCBX_ZERO;
    INT1               *piIfName = NULL;
    UINT1               au1IfName[CFA_MAX_PORT_NAME_LENGTH] = { DCBX_ZERO };
    UINT4               au4EtsBW[DCBX_MAX_PRIORITIES] = { DCBX_ZERO };

    MEMSET (au4EtsBW, DCBX_ZERO, sizeof (UINT4) * DCBX_MAX_PRIORITIES);

    piIfName = (INT1 *) &au1IfName[DCBX_ZERO];
    pHttp->i4Write = DCBX_ZERO;

    WebnmRegisterLock (pHttp, DcbxLock, DcbxUnLock);
    DcbxLock ();

    WebnmSendString (pHttp, ISS_TABLE_FLAG);
    i4OutCome = (INT4) (nmhGetFirstIndexFsETSPortTable (&i4NextPort));

    if (i4OutCome == SNMP_FAILURE)
    {
        DcbxUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
        return;
    }

    u4Temp = (UINT4) pHttp->i4Write;
    do
    {
        CfaCliGetIfName ((UINT4) i4NextPort, piIfName);
        pHttp->i4Write = (INT4) u4Temp;

        STRCPY (pHttp->au1KeyString, "PORT_NO_KEY_NAME");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", (CHR1 *) piIfName);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);
        STRCPY (pHttp->au1KeyString, "PORT_NO_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", (CHR1 *) piIfName);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        /*Willing */
        STRCPY (pHttp->au1KeyString, "WILLING_STATUS_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        if (nmhGetLldpXdot1dcbxAdminETSConWilling
            (i4NextPort, &i4ETSAdminWilling) == SNMP_SUCCESS)
        {
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4ETSAdminWilling);
        }

        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);
        /* Configuration TLV */
        STRCPY (pHttp->au1KeyString, "CONFIG_TLV_STATUS_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        if (nmhGetLldpXdot1dcbxConfigETSConfigurationTxEnable
            (i4NextPort, DCBX_DEST_ADDR_INDEX, &i4RetVal) == SNMP_SUCCESS)
        {
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
        }
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        /* Recommended TLV */
        STRCPY (pHttp->au1KeyString, "RECO_TLV_STATUS_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        if (nmhGetLldpXdot1dcbxConfigETSRecommendationTxEnable
            (i4NextPort, DCBX_DEST_ADDR_INDEX, &i4RetVal) == SNMP_SUCCESS)
        {
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
        }

        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        /*TC Support TLV */
        STRCPY (pHttp->au1KeyString, "TC_SUP_TLV_STATUS_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        if (nmhGetFslldpXdot1dcbxConfigTCSupportedTxEnable
            (i4NextPort, DCBX_DEST_ADDR_INDEX, &i4RetVal) == SNMP_SUCCESS)
        {
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
        }

        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        /*Admin Mode */
        STRCPY (pHttp->au1KeyString, "ADMIN_MODE_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        if (nmhGetFsETSAdminMode (i4NextPort, &i4RetVal) == SNMP_SUCCESS)
        {
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
        }

        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        /* Opersation State */
        STRCPY (pHttp->au1KeyString, "OPER_STATE_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        if (nmhGetFsETSDcbxOperState (i4NextPort, &i4RetVal) == SNMP_SUCCESS)
        {
            if (i4RetVal == ETS_OPER_OFF)
            {
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", "OFF_STATE");

            }
            else if (i4RetVal == ETS_OPER_INIT)
            {
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", "INIT_STATE");

            }
            else
            {
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", "RECO_STATE");

            }
        }
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        /* State Machine */
        STRCPY (pHttp->au1KeyString, "STATE_MACHNINE_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        if (nmhGetFsETSDcbxStateMachine (i4NextPort, &i4RetVal) == SNMP_SUCCESS)
        {
            if (i4RetVal == DCBX_ASYM_STATE_MACHINE)
            {
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", "Asymmetric");

            }
            else
            {
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", "Symmetric");

            }
        }
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        /*no of TC */
        STRCPY (pHttp->au1KeyString, "TC_SUPPORT_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);

        if (nmhGetLldpXdot1dcbxLocETSConTrafficClassesSupported
            (i4NextPort, (UINT4 *) &i4ETSTcSup) == SNMP_SUCCESS)
        {
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4ETSTcSup);
        }
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        i4CurrentPort = i4NextPort;
    }
    while ((nmhGetNextIndexFsETSPortTable (i4CurrentPort,
                                           &i4NextPort) == SNMP_SUCCESS));

    DcbxUnLock ();
    WebnmUnRegisterLock (pHttp);
    WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                    (pHttp->i4HtmlSize - pHttp->i4Write));
}

/*********************************************************************
 *  Function Name : ETSIssProcessAdminInfoPageSet
 *  Description   : This function processes the get request coming for the  
 *                  LA Interface Settings page. 
 *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 *  Output(s)     : None.
 *  Return Values : None
 *********************************************************************/
PRIVATE VOID
ETSIssProcessAdminInfoPageSet (tHttp * pHttp)
{
    INT4                i4WillingStatus = DCBX_ZERO;
    INT4                i4AdminMode = DCBX_ZERO;
    INT4                i4confTlvStatus = DCBX_ZERO;
    INT4                i4RecoTlvStatus = DCBX_ZERO;
    INT4                i4TcSupTlvStatus = DCBX_ZERO;
    UINT4               u4ErrorCode = DCBX_ZERO;
    INT4                i4IfIndex = DCBX_ZERO;

    /* Check whether the request is for ADD/DELETE */
    WebnmRegisterLock (pHttp, DcbxLock, DcbxUnLock);
    DcbxLock ();

    /* Port NO */
    STRCPY (pHttp->au1Name, "PORT_NO");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    issDecodeSpecialChar (pHttp->au1Value);
    if (CfaUtilGetInterfaceIndexFromAlias
        (pHttp->au1Value, (UINT4 *) &i4IfIndex) != CFA_SUCCESS)
    {
        IssSendError (pHttp, (CONST INT1 *) "Invalid Interface");
        DcbxUnLock ();
        WebnmUnRegisterLock (pHttp);
        return;
    }

    /* willing Status */
    STRCPY (pHttp->au1Name, "WILLING_STATUS");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4WillingStatus = ATOI (pHttp->au1Value);

    /* Config TLV */
    STRCPY (pHttp->au1Name, "CONFIG_TLV_STATUS");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4confTlvStatus = ATOI (pHttp->au1Value);
    /*Recom TLV */
    STRCPY (pHttp->au1Name, "RECO_TLV_STATUS");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4RecoTlvStatus = ATOI (pHttp->au1Value);
    /*TC Support TLV */
    STRCPY (pHttp->au1Name, "TC_SUP_TLV_STATUS");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4TcSupTlvStatus = ATOI (pHttp->au1Value);
    /* ETS Admin Mode */
    STRCPY (pHttp->au1Name, "ADMIN_MODE");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4AdminMode = ATOI (pHttp->au1Value);

    STRCPY (pHttp->au1Name, "ACTION");
    if ((HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                             pHttp->au1PostQuery)) != ENM_SUCCESS)
    {
        DcbxUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp, (CONST INT1 *) "Invalid Request");
        return;
    }

    /* check for delete */
    if (STRCMP (pHttp->au1Value, "delete") == DCBX_EQUAL)
    {
        if (nmhTestv2FsETSRowStatus (&u4ErrorCode, i4IfIndex,
                                     DESTROY) == SNMP_FAILURE)
        {
            DcbxUnLock ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp, (CONST INT1 *) "ETS Port not available.");
            return;
        }

        if (nmhSetFsETSRowStatus (i4IfIndex, DESTROY) == SNMP_FAILURE)
        {
            DcbxUnLock ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp, (CONST INT1 *) "ETS port  not available.");
            return;
        }

    }
    else if ((STRCMP (pHttp->au1Value, "apply") == DCBX_EQUAL) ||
             (STRCMP (pHttp->au1Value, "Add") == DCBX_EQUAL))
    {
        if (STRCMP (pHttp->au1Value, "Add") == DCBX_EQUAL)
        {
            if (nmhTestv2FsETSRowStatus (&u4ErrorCode, i4IfIndex,
                                         CREATE_AND_GO) == SNMP_FAILURE)
            {
                DcbxUnLock ();
                WebnmUnRegisterLock (pHttp);
                IssSendError (pHttp,
                              (CONST INT1 *)
                              "Ets can't be created on this port.");
                return;
            }

            if (nmhSetFsETSRowStatus (i4IfIndex, CREATE_AND_GO) == SNMP_FAILURE)
            {
                DcbxUnLock ();
                WebnmUnRegisterLock (pHttp);
                IssSendError (pHttp,
                              (CONST INT1 *)
                              "ETS can't be created for this port.");
                return;
            }
        }
        /*willing */
        if (nmhTestv2LldpXdot1dcbxAdminETSConWilling (&u4ErrorCode, i4IfIndex,
                                                      i4WillingStatus) ==
            SNMP_FAILURE)
        {
            DcbxUnLock ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp,
                          (CONST INT1 *)
                          "Willing Status canot be set on this port.");
            return;
        }

        if (nmhSetLldpXdot1dcbxAdminETSConWilling (i4IfIndex, i4WillingStatus)
            == SNMP_FAILURE)
        {
            DcbxUnLock ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp,
                          (CONST INT1 *)
                          "Willing Status canot be set on this port..");
            return;
        }

        /*COnfig TLV */
        if (nmhTestv2LldpXdot1dcbxConfigETSConfigurationTxEnable
            (&u4ErrorCode, i4IfIndex, DCBX_DEST_ADDR_INDEX,
             i4confTlvStatus) == SNMP_FAILURE)
        {
            DcbxUnLock ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp,
                          (CONST INT1 *)
                          "Config TLV Status canot be set on this port.");
            return;
        }

        if (nmhSetLldpXdot1dcbxConfigETSConfigurationTxEnable
            (i4IfIndex, DCBX_DEST_ADDR_INDEX, i4confTlvStatus) == SNMP_FAILURE)
        {
            DcbxUnLock ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp,
                          (CONST INT1 *)
                          "Config TLV Status canot be set on this port..");
            return;
        }
        /*reco TLV */
        if (nmhTestv2LldpXdot1dcbxConfigETSRecommendationTxEnable
            (&u4ErrorCode, i4IfIndex, DCBX_DEST_ADDR_INDEX,
             i4RecoTlvStatus) == SNMP_FAILURE)
        {
            DcbxUnLock ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp,
                          (CONST INT1 *)
                          "Recommeded TLV Status canot be set on this port.");
            return;
        }

        if (nmhSetLldpXdot1dcbxConfigETSRecommendationTxEnable
            (i4IfIndex, DCBX_DEST_ADDR_INDEX, i4RecoTlvStatus) == SNMP_FAILURE)
        {
            DcbxUnLock ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp,
                          (CONST INT1 *)
                          "Recommeded TLV Status canot be set on this port..");
            return;
        }
        /*TC Suppoted TLV */
        if (nmhTestv2FslldpXdot1dcbxConfigTCSupportedTxEnable
            (&u4ErrorCode, i4IfIndex, DCBX_DEST_ADDR_INDEX,
             i4TcSupTlvStatus) == SNMP_FAILURE)
        {
            DcbxUnLock ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp,
                          (CONST INT1 *)
                          "TC supported TLV Status canot be set on this port.");
            return;
        }

        if (nmhSetFslldpXdot1dcbxConfigTCSupportedTxEnable
            (i4IfIndex, DCBX_DEST_ADDR_INDEX, i4TcSupTlvStatus) == SNMP_FAILURE)
        {
            DcbxUnLock ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp,
                          (CONST INT1 *)
                          "TC supported TLV Status canot be set on this"
                          " port..");
            return;
        }
        /*Admin Mode */
        if (nmhTestv2FsETSAdminMode (&u4ErrorCode, i4IfIndex,
                                     i4AdminMode) == SNMP_FAILURE)
        {
            DcbxUnLock ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp,
                          (CONST INT1 *)
                          "Admin Mode Status canot be set on this port.");
            return;
        }

        if (nmhSetFsETSAdminMode (i4IfIndex, i4AdminMode) == SNMP_FAILURE)
        {
            DcbxUnLock ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp,
                          (CONST INT1 *)
                          "Admin mode Status canot be set on this port..");
            return;
        }

    }
    DcbxUnLock ();
    WebnmUnRegisterLock (pHttp);
    ETSIssProcessAdminInfoPageGet (pHttp);
    return;
}

/*********************************************************************
 *  Function Name : AppPriIssProcessAdminInfoPage
 *  Description   : This function processes the request coming for the
 *                  Application Priority Admin Configuration page.
 *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 *  Output(s)     : None.
 *  Return Values : None
 *********************************************************************/
PUBLIC VOID
AppPriIssProcessAdminInfoPage (tHttp * pHttp)
{
    if (pHttp->i4Method == ISS_GET)
    {
        AppPriIssProcessAdminInfoPageGet (pHttp);
    }
    else if (pHttp->i4Method == ISS_SET)
    {
        AppPriIssProcessAdminInfoPageSet (pHttp);
    }
}

/*********************************************************************
*  Function Name : AppPriIssProcessAdminInfoPageGet
*  Description   : This function processes the get request coming for the
*                  Application Priority Admin Configuration page.
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/
PRIVATE VOID
AppPriIssProcessAdminInfoPageGet (tHttp * pHttp)
{
    UINT1               au1IfName[CFA_MAX_PORT_NAME_LENGTH] = { DCBX_ZERO };
    UINT4               u4Temp = DCBX_ZERO;
    INT4                i4CurrentPort = DCBX_ZERO;
    INT4                i4NextPort = DCBX_ZERO;
    INT4                i4RetVal = DCBX_ZERO;
    INT4                i4OutCome = DCBX_ZERO;
    INT1               *piIfName = NULL;

    MEMSET (au1IfName, DCBX_ZERO, CFA_MAX_PORT_NAME_LENGTH);
    piIfName = (INT1 *) &au1IfName[DCBX_ZERO];

    pHttp->i4Write = DCBX_ZERO;

    WebnmRegisterLock (pHttp, DcbxLock, DcbxUnLock);
    DcbxLock ();

    WebnmSendString (pHttp, ISS_TABLE_FLAG);
    i4OutCome = (INT4) (nmhGetFirstIndexFsAppPriPortTable (&i4NextPort));

    if (i4OutCome == SNMP_FAILURE)
    {
        DcbxUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
        return;
    }

    u4Temp = (UINT4) pHttp->i4Write;
    do
    {
        pHttp->i4Write = (INT4) u4Temp;

        /*port Number */
        CfaCliGetIfName ((UINT4) i4NextPort, piIfName);
        STRCPY (pHttp->au1KeyString, "PORT_NO_KEY_NAME");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", (CHR1 *) piIfName);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        STRCPY (pHttp->au1KeyString, "PORT_NO_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4NextPort);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));

        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);
        /* Admin Table Willing */
        STRCPY (pHttp->au1KeyString, "APP_PRI_WILLING_STATUS_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        if (nmhGetFslldpXdot1dcbxAdminApplicationPriorityWilling
            (i4NextPort, &i4RetVal) == SNMP_SUCCESS)
        {
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
        }

        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);
        /* Application Priority TLV */
        STRCPY (pHttp->au1KeyString, "APP_PRI_TLV_STATUS_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        if (nmhGetLldpXdot1dcbxConfigApplicationPriorityTxEnable
            (i4NextPort, DCBX_DEST_ADDR_INDEX, &i4RetVal) == SNMP_SUCCESS)
        {
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
        }
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        /*Admin Mode */
        STRCPY (pHttp->au1KeyString, "APP_PRI_ADMIN_MODE_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        if (nmhGetFsAppPriAdminMode (i4NextPort, &i4RetVal) == SNMP_SUCCESS)
        {
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
        }

        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        /* Opersation State */
        STRCPY (pHttp->au1KeyString, "OPER_STATE_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        if (nmhGetFsAppPriDcbxOperState (i4NextPort, &i4RetVal) == SNMP_SUCCESS)
        {
            if (i4RetVal == APP_PRI_OPER_OFF)
            {
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", "OFF");

            }
            else if (i4RetVal == APP_PRI_OPER_INIT)
            {
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", "INIT");

            }
            else
            {
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", "RECO");

            }
        }
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        /* State Machine */
        STRCPY (pHttp->au1KeyString, "STATE_MACHNINE_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        if (nmhGetFsAppPriDcbxStateMachine
            (i4NextPort, &i4RetVal) == SNMP_SUCCESS)
        {
            if (i4RetVal == DCBX_ASYM_STATE_MACHINE)
            {
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", "Asymmetric");

            }
            else
            {
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", "Symmetric");

            }
        }
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        WebnmSendString (pHttp, ISS_TABLE_STOP_FLAG);
        i4CurrentPort = i4NextPort;
    }
    while ((nmhGetNextIndexFsAppPriPortTable (i4CurrentPort,
                                              &i4NextPort) == SNMP_SUCCESS));

    DcbxUnLock ();
    WebnmUnRegisterLock (pHttp);
    WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                    (pHttp->i4HtmlSize - pHttp->i4Write));
}

/*********************************************************************
*  Function Name : AppPriIssProcessAdminInfoPageSet
*  Description   : This function processes the get request coming for the
*                  Application Priority Admin Configuration page.
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
 *********************************************************************/
PRIVATE VOID
AppPriIssProcessAdminInfoPageSet (tHttp * pHttp)
{
    INT4                i4WillingStatus = DCBX_ZERO;
    INT4                i4AdminMode = DCBX_ZERO;
    INT4                i4AppPriTLVStatus = DCBX_ZERO;
    UINT4               u4ErrorCode = DCBX_ZERO;
    INT4                i4IfIndex = DCBX_ZERO;

    WebnmRegisterLock (pHttp, DcbxLock, DcbxUnLock);
    DcbxLock ();

    /* Port NO */
    STRCPY (pHttp->au1Name, "PORT_NO");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4IfIndex = ATOI (pHttp->au1Value);
    /* willing Status */
    STRCPY (pHttp->au1Name, "APP_PRI_WILLING_STATUS");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4WillingStatus = ATOI (pHttp->au1Value);

    /* Application Priority TLV */
    STRCPY (pHttp->au1Name, "APP_PRI_TLV_STATUS");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4AppPriTLVStatus = ATOI (pHttp->au1Value);
    /* Application Priority Admin Mode */
    STRCPY (pHttp->au1Name, "APP_PRI_ADMIN_MODE");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4AdminMode = ATOI (pHttp->au1Value);

    STRCPY (pHttp->au1Name, "ACTION");
    if ((HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                             pHttp->au1PostQuery)) != ENM_SUCCESS)
    {
        IssSendError (pHttp, (CONST INT1 *) "Invalid Request");
        return;
    }

    /* check for delete */
    if (STRCMP (pHttp->au1Value, "delete") == DCBX_EQUAL)
    {
        if (nmhTestv2FsAppPriRowStatus (&u4ErrorCode, i4IfIndex,
                                        DESTROY) == SNMP_FAILURE)
        {
            DcbxUnLock ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp,
                          (CONST INT1 *)
                          "Application Priority Port not available.");
            return;
        }

        if (nmhSetFsAppPriRowStatus (i4IfIndex, DESTROY) == SNMP_FAILURE)
        {
            DcbxUnLock ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp,
                          (CONST INT1 *)
                          "Application Priority port not available.");
            return;
        }

    }                            /* Check for Add and Apply */
    else if ((STRCMP (pHttp->au1Value, "apply") == DCBX_EQUAL) ||
             (STRCMP (pHttp->au1Value, "Add") == DCBX_EQUAL))
    {
        if (STRCMP (pHttp->au1Value, "Add") == DCBX_EQUAL)
        {
            if (nmhTestv2FsAppPriRowStatus (&u4ErrorCode, i4IfIndex,
                                            CREATE_AND_GO) == SNMP_FAILURE)
            {
                DcbxUnLock ();
                WebnmUnRegisterLock (pHttp);
                IssSendError (pHttp,
                              (CONST INT1 *) "Application Priority can't be"
                              " created on this port.");
                return;
            }

            if (nmhSetFsAppPriRowStatus
                (i4IfIndex, CREATE_AND_GO) == SNMP_FAILURE)
            {
                DcbxUnLock ();
                WebnmUnRegisterLock (pHttp);
                IssSendError (pHttp,
                              (CONST INT1 *) "Application Priority can't be"
                              " created for this port.");
                return;
            }
        }
        /*Admin Table Willing */
        if (nmhTestv2FslldpXdot1dcbxAdminApplicationPriorityWilling
            (&u4ErrorCode, i4IfIndex, i4WillingStatus) == SNMP_FAILURE)
        {
            DcbxUnLock ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp,
                          (CONST INT1 *)
                          "Willing Status canot be set on this port.");
            return;
        }

        if (nmhSetFslldpXdot1dcbxAdminApplicationPriorityWilling
            (i4IfIndex, i4WillingStatus) == SNMP_FAILURE)
        {
            DcbxUnLock ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp,
                          (CONST INT1 *)
                          "Willing Status canot be set on this port..");
            return;
        }

        /*Application Priority TLV */
        if (nmhTestv2LldpXdot1dcbxConfigApplicationPriorityTxEnable
            (&u4ErrorCode, i4IfIndex, DCBX_DEST_ADDR_INDEX,
             i4AppPriTLVStatus) == SNMP_FAILURE)
        {
            DcbxUnLock ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp,
                          (CONST INT1 *) "Application Priority TLV Status"
                          " canot be set on this port.");
            return;
        }

        if (nmhSetLldpXdot1dcbxConfigApplicationPriorityTxEnable (i4IfIndex,
                                                                  DCBX_DEST_ADDR_INDEX,
                                                                  i4AppPriTLVStatus)
            == SNMP_FAILURE)
        {
            DcbxUnLock ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp,
                          (CONST INT1 *) "Application Priority TLV Status"
                          " canot be set on this port..");
            return;
        }
        /*Admin Mode */
        if (nmhTestv2FsAppPriAdminMode (&u4ErrorCode, i4IfIndex,
                                        i4AdminMode) == SNMP_FAILURE)
        {
            DcbxUnLock ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp,
                          (CONST INT1 *)
                          "Admin Mode Status canot be set on this port.");
            return;
        }

        if (nmhSetFsAppPriAdminMode (i4IfIndex, i4AdminMode) == SNMP_FAILURE)
        {
            DcbxUnLock ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp,
                          (CONST INT1 *)
                          "Admin mode Status canot be set on this port..");
            return;
        }

    }
    DcbxUnLock ();
    WebnmUnRegisterLock (pHttp);
    AppPriIssProcessAdminInfoPageGet (pHttp);
    return;
}

/*********************************************************************
*  Function Name : AppPriIssProcessPage
*  Description   : This function processes the request coming for the
*                  Dcb Application Priority page.
*
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/
PUBLIC VOID
AppPriIssProcessPage (tHttp * pHttp)
{
    if (pHttp->i4Method == ISS_GET)
    {
        AppPriIssProcessPageGet (pHttp);
    }
    else if (pHttp->i4Method == ISS_SET)
    {
        AppPriIssProcessPageSet (pHttp);
    }
}

/*********************************************************************
*  Function Name : AppPriIssProcessPageGet 
*  Description   : This function processes the get request coming for the
*                  DCB Application Priority page.
*
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/

PRIVATE VOID
AppPriIssProcessPageGet (tHttp * pHttp)
{
    UINT1               au1IfName[CFA_MAX_PORT_NAME_LENGTH] = { DCBX_ZERO };
    UINT4               u4Priority = DCBX_ZERO;
    UINT4               u4Temp = DCBX_ZERO;
    INT4                i4CurrentPort = DCBX_ZERO;
    INT4                i4NextPort = DCBX_ZERO;
    INT4                i4Port = DCBX_ZERO;
    INT4                i4CurPort = DCBX_ZERO;
    INT4                i4NextSelector = DCBX_ZERO;
    INT4                i4NextProtocol = DCBX_ZERO;
    INT4                i4CurSelector = DCBX_ZERO;
    INT4                i4CurProtocol = DCBX_ZERO;
    INT4                i4OutCome = DCBX_ZERO;
    INT1               *piIfName = NULL;

    MEMSET (au1IfName, DCBX_ZERO, CFA_MAX_PORT_NAME_LENGTH);
    piIfName = (INT1 *) &au1IfName[DCBX_ZERO];

    pHttp->i4Write = DCBX_ZERO;

    WebnmRegisterLock (pHttp, DcbxLock, DcbxUnLock);
    DcbxLock ();

    while ((nmhGetNextIndexFsAppPriPortTable (i4CurrentPort,
                                              &i4NextPort) == SNMP_SUCCESS))
    {
        CfaCliGetIfName ((UINT4) i4NextPort, piIfName);
        STRCPY (pHttp->au1KeyString, "<! DCB_APP_PRI_PORT>");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString,
                 "<option value =\"%d\">%s\n", i4NextPort, (CHR1 *) piIfName);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        i4CurrentPort = i4NextPort;
    }
    WebnmSendString (pHttp, ISS_TABLE_FLAG);

    i4OutCome = (INT4) (nmhGetFirstIndexFsAppPriPortTable (&i4NextPort));
    if (i4OutCome == SNMP_FAILURE)
    {
        DcbxUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
        return;
    }

    i4OutCome =
        (INT4) (nmhGetFirstIndexLldpXdot1dcbxAdminApplicationPriorityAppTable
                (&i4Port, &i4NextSelector, (UINT4 *) &i4NextProtocol));
    if (i4OutCome == SNMP_FAILURE)
    {
        DcbxUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
        return;
    }

    i4CurrentPort = DCBX_ZERO;
    i4CurSelector = i4NextSelector;
    i4CurProtocol = i4NextProtocol;
    i4NextPort = DCBX_ZERO;

    u4Temp = (UINT4) pHttp->i4Write;

    while ((nmhGetNextIndexFsAppPriPortTable (i4CurrentPort,
                                              &i4NextPort) == SNMP_SUCCESS))
    {
        i4CurPort = DCBX_ZERO;
        while ((nmhGetNextIndexLldpXdot1dcbxAdminApplicationPriorityAppTable
                (i4CurPort, &i4Port, i4CurSelector, &i4NextSelector,
                 (UINT4) i4CurProtocol,
                 (UINT4 *) &i4NextProtocol) != SNMP_FAILURE))
        {
            if ((i4Port == i4NextPort))
            {

                pHttp->i4Write = (INT4) u4Temp;
                /* Port No */
                CfaCliGetIfName ((UINT4) i4NextPort, piIfName);
                STRCPY (pHttp->au1KeyString, "PORT_NO_KEY_NAME");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%s",
                         (CHR1 *) piIfName);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                (INT4) STRLEN (pHttp->au1DataString));
                WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

                STRCPY (pHttp->au1KeyString, "PORT_NO_KEY");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4NextPort);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                (INT4) STRLEN (pHttp->au1DataString));
                WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

                nmhGetLldpXdot1dcbxAdminApplicationPriorityAEPriority
                    (i4NextPort, i4NextSelector, (UINT4) i4NextProtocol,
                     &u4Priority);
                /*Admin Table Selector */
                STRCPY (pHttp->au1KeyString, "SELECTOR_KEY");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4NextSelector);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                (INT4) STRLEN (pHttp->au1DataString));
                WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);
                /* Admin Table Protocol */
                STRCPY (pHttp->au1KeyString, "PROTOCOL_KEY");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4NextProtocol);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                (INT4) STRLEN (pHttp->au1DataString));
                WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);
                /*Admin Table Priority */
                STRCPY (pHttp->au1KeyString, "PRIORITY_KEY");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", u4Priority);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                (INT4) STRLEN (pHttp->au1DataString));
                WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);
            }
            else
            {
                IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);

            }
            i4CurPort = i4Port;
            i4CurSelector = i4NextSelector;
            i4CurProtocol = i4NextProtocol;
        }
        WebnmSendString (pHttp, ISS_TABLE_STOP_FLAG);
        i4CurrentPort = i4NextPort;
    }

    DcbxUnLock ();
    WebnmUnRegisterLock (pHttp);
    WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                    (pHttp->i4HtmlSize - pHttp->i4Write));
}

/*********************************************************************
*  Function Name : AppPriIssProcessPageSet
*  Description   : This function processes the get request coming for the
*                  DCB Application Priority page.
*
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/

PRIVATE VOID
AppPriIssProcessPageSet (tHttp * pHttp)
{

    INT4                i4PortNo = DCBX_ZERO;
    INT4                i4Selector = DCBX_ZERO;
    INT4                i4Protocol = DCBX_ZERO;
    INT4                i4Priority = DCBX_ZERO;
    UINT4               u4ErrorCode = DCBX_ZERO;

    WebnmRegisterLock (pHttp, DcbxLock, DcbxUnLock);
    DcbxLock ();

    /* Port NO */
    STRCPY (pHttp->au1Name, "PORT_NO");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4PortNo = ATOI (pHttp->au1Value);
    /* Selector */
    STRCPY (pHttp->au1Name, "SELECTOR");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4Selector = ATOI (pHttp->au1Value);
    /* Protocol */
    STRCPY (pHttp->au1Name, "PROTOCOL");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4Protocol = ATOI (pHttp->au1Value);
    /* Priority */
    STRCPY (pHttp->au1Name, "PRIORITY");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4Priority = ATOI (pHttp->au1Value);
    /*Action */
    STRCPY (pHttp->au1Name, "ACTION");
    if ((HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                             pHttp->au1PostQuery)) != ENM_SUCCESS)
    {
        DcbxUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssSendError (pHttp, (CONST INT1 *) "Invalid Request");
        return;
    }

    /* check for delete */
    if (STRCMP (pHttp->au1Value, "delete") == DCBX_EQUAL)
    {
        if (nmhTestv2FsAppPriXAppRowStatus
            (&u4ErrorCode, i4PortNo, i4Selector,
             (UINT4) i4Protocol, DESTROY) == SNMP_FAILURE)
        {
            DcbxUnLock ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp,
                          (CONST INT1 *)
                          "Application To Priority Mapping not available.");
            return;
        }

        if (nmhSetFsAppPriXAppRowStatus
            (i4PortNo, i4Selector, i4Protocol, DESTROY) == SNMP_FAILURE)
        {
            DcbxUnLock ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp,
                          (CONST INT1 *)
                          "Application To Priority Mapping not available.");
            return;
        }

    }                            /*Check for Add and Apply */
    else if ((STRCMP (pHttp->au1Value, "apply") == DCBX_EQUAL) ||
             (STRCMP (pHttp->au1Value, "Add") == DCBX_EQUAL))
    {
        if (nmhTestv2LldpXdot1dcbxAdminApplicationPriorityAEPriority
            (&u4ErrorCode, i4PortNo, i4Selector, (UINT4) i4Protocol,
             (UINT4) i4Priority) == SNMP_FAILURE)
        {
            DcbxUnLock ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp,
                          (CONST INT1 *) "Application To Priority Mapping"
                          " can't be created on this port.");
            return;
        }

        if (nmhSetLldpXdot1dcbxAdminApplicationPriorityAEPriority
            (i4PortNo, i4Selector, i4Protocol, i4Priority) == SNMP_FAILURE)
        {
            DcbxUnLock ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp,
                          (CONST INT1 *) "Application To Priority Mapping"
                          " can't be created for this port.");
            return;
        }
    }
    DcbxUnLock ();
    WebnmUnRegisterLock (pHttp);
    AppPriIssProcessPageGet (pHttp);
    return;
}

/*********************************************************************
*  Function Name : AppPriIssProcessLocalPageGet 
*  Description   : This function processes the get request coming for the
*                  Application Priority Local page.
*
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/

PUBLIC VOID
AppPriIssProcessLocalPageGet (tHttp * pHttp)
{
    UINT1               au1IfName[CFA_MAX_PORT_NAME_LENGTH] = { DCBX_ZERO };
    UINT4               u4Temp = DCBX_ZERO;
    INT4                i4CurrentPort = DCBX_ZERO;
    INT4                i4CurPort = DCBX_ZERO;
    INT4                i4NextPort = DCBX_ZERO;
    INT4                i4Port = DCBX_ZERO;
    INT4                i4Priority = DCBX_ZERO;
    INT4                i4NextProtocol = DCBX_ZERO;
    INT4                i4CurProtocol = DCBX_ZERO;
    INT4                i4NextSelector = DCBX_ZERO;
    INT4                i4CurSelector = DCBX_ZERO;
    INT4                i4OutCome = DCBX_ZERO;
    INT4                i4RetVal = DCBX_ZERO;
    INT1               *piIfName = NULL;

    piIfName = (INT1 *) &au1IfName[DCBX_ZERO];
    pHttp->i4Write = DCBX_ZERO;

    WebnmRegisterLock (pHttp, DcbxLock, DcbxUnLock);
    DcbxLock ();

    WebnmSendString (pHttp, ISS_TABLE_FLAG);

    i4OutCome = (INT4) (nmhGetFirstIndexFsAppPriPortTable (&i4NextPort));
    if (i4OutCome == SNMP_FAILURE)
    {
        DcbxUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
        return;
    }

    i4OutCome =
        (INT4) (nmhGetFirstIndexLldpXdot1dcbxLocApplicationPriorityAppTable
                (&i4Port, &i4NextSelector, (UINT4 *) &i4NextProtocol));
    if (i4OutCome == SNMP_FAILURE)
    {
        DcbxUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
        return;
    }

    i4CurSelector = i4NextSelector;
    i4CurProtocol = i4NextProtocol;
    i4CurPort = i4NextPort;

    u4Temp = (UINT4) pHttp->i4Write;

    do
    {
        i4CurPort = DCBX_ZERO;
        while ((nmhGetNextIndexLldpXdot1dcbxLocApplicationPriorityAppTable
                (i4CurPort, &i4Port, i4CurSelector, &i4NextSelector,
                 (UINT4) i4CurProtocol,
                 (UINT4 *) &i4NextProtocol) != SNMP_FAILURE))
        {
            if ((i4Port == i4NextPort))
            {
                pHttp->i4Write = (INT4) u4Temp;
                /* Port NO */
                CfaCliGetIfName ((UINT4) i4NextPort, piIfName);
                STRCPY (pHttp->au1KeyString, "PORT_NO_KEY");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%s",
                         (CHR1 *) piIfName);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                (INT4) STRLEN (pHttp->au1DataString));
                WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);
                /* Local Table Willing */
                STRCPY (pHttp->au1KeyString, "LOCAL_APP_PRI_WILLING_KEY");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                nmhGetFslldpXdot1dcbxLocApplicationPriorityWilling (i4Port,
                                                                    &i4RetVal);
                if (i4RetVal == DCBX_ENABLED)
                {
                    SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", "Enabled");
                }
                else
                {
                    SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", "Disabled");
                }
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                (INT4) STRLEN (pHttp->au1DataString));
                WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

                nmhGetLldpXdot1dcbxLocApplicationPriorityAEPriority (i4Port,
                                                                     i4NextSelector,
                                                                     (UINT4)
                                                                     i4NextProtocol,
                                                                     (UINT4 *)
                                                                     &i4Priority);
                /* Local Table Selector */
                STRCPY (pHttp->au1KeyString, "LOCAL_APP_PRI_SELECTOR_KEY");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4NextSelector);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                (INT4) STRLEN (pHttp->au1DataString));
                WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);
                /* Local Table Protocol */
                STRCPY (pHttp->au1KeyString, "LOCAL_APP_PRI_PROTOCOL_KEY");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4NextProtocol);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                (INT4) STRLEN (pHttp->au1DataString));
                WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);
                /* Local Table Priority */
                STRCPY (pHttp->au1KeyString, "LOCAL_APP_PRI_PRIORITY_KEY");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4Priority);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                (INT4) STRLEN (pHttp->au1DataString));
                WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);
            }
            else
            {
                IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
            }
            i4CurPort = i4Port;
            i4CurSelector = i4NextSelector;
            i4CurProtocol = i4NextProtocol;
        }
        WebnmSendString (pHttp, ISS_TABLE_STOP_FLAG);
        i4CurrentPort = i4NextPort;
    }
    while ((nmhGetNextIndexFsAppPriPortTable (i4CurrentPort,
                                              &i4NextPort) == SNMP_SUCCESS));

    DcbxUnLock ();
    WebnmUnRegisterLock (pHttp);
    WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                    (pHttp->i4HtmlSize - pHttp->i4Write));
}

/*********************************************************************
*  Function Name : AppPriIssProcessRemotePageGet 
*  Description   : This function processes the get request coming for the
*                  Application Priority Local page.
*
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/

PUBLIC VOID
AppPriIssProcessRemotePageGet (tHttp * pHttp)
{
    UINT4               u4NextLldpV2RemLocalDestMACAddress = DCBX_ZERO;
    UINT4               u4LldpV2RemLocalDestMACAddress = DCBX_ZERO;
    UINT1               au1IfName[CFA_MAX_PORT_NAME_LENGTH] = { DCBX_ZERO };
    UINT4               u4Priority = DCBX_ZERO;
    UINT4               u4Temp = DCBX_ZERO;
    UINT4               u4LldpV2RemTimeMark = DCBX_ZERO;
    UINT4               u4NextLldpV2RemTimeMark = DCBX_ZERO;
    INT4                i4Willing = DCBX_ZERO;
    INT4                i4OutCome = DCBX_ZERO;
    INT4                i4NextSelector = DCBX_ZERO;
    INT4                i4NextProtocol = DCBX_ZERO;
    INT4                i4NextPort = DCBX_ZERO;
    INT4                i4CurrentPort = DCBX_ZERO;
    INT4                i4IfIndex = DCBX_ZERO;
    INT4                i4CurSelector = DCBX_ZERO;
    INT4                i4CurProtocol = DCBX_ZERO;
    INT4                i4CurPort = DCBX_ZERO;
    INT4                i4LldpV2RemIndex = DCBX_ZERO;
    INT4                i4NextLldpV2RemIndex = DCBX_ZERO;
    INT1               *piIfName = NULL;


    piIfName = (INT1 *) &au1IfName[DCBX_ZERO];

    pHttp->i4Write = DCBX_ZERO;

    WebnmRegisterLock (pHttp, DcbxLock, DcbxUnLock);
    DcbxLock ();

    WebnmSendString (pHttp, ISS_TABLE_FLAG);

    i4OutCome = (INT4) (nmhGetFirstIndexFsAppPriPortTable (&i4IfIndex));
    if (i4OutCome == SNMP_FAILURE)
    {
        DcbxUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
        return;
    }

    if (nmhGetFirstIndexLldpXdot1dcbxRemApplicationPriorityAppTable
        (&u4NextLldpV2RemTimeMark, &i4NextPort,
         &u4NextLldpV2RemLocalDestMACAddress, &i4NextLldpV2RemIndex,
         &i4NextSelector, (UINT4 *) &i4NextProtocol) == SNMP_FAILURE)
    {

        DcbxUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
        return;
    }
            
    i4CurSelector = i4NextSelector;
    i4CurProtocol = i4NextProtocol;
    i4CurPort = i4NextPort;

    u4Temp = (UINT4) pHttp->i4Write;
    do
    {
        while (nmhGetNextIndexLldpXdot1dcbxRemApplicationPriorityAppTable
               (u4LldpV2RemTimeMark, &u4NextLldpV2RemTimeMark,
                i4CurPort, &i4NextPort,
                u4LldpV2RemLocalDestMACAddress,
                &u4NextLldpV2RemLocalDestMACAddress,
                i4LldpV2RemIndex, &i4NextLldpV2RemIndex,
                i4CurSelector, &i4NextSelector, (UINT4) i4CurProtocol,
                (UINT4 *) &i4NextProtocol) != SNMP_FAILURE)
        {
            if ((i4NextPort == i4IfIndex))
            {
                pHttp->i4Write = (INT4) u4Temp;
                /* Port No */
                CfaCliGetIfName ((UINT4) i4IfIndex, piIfName);
                STRCPY (pHttp->au1KeyString, "PORT_NO_KEY");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%s",
                         (CHR1 *) piIfName);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                (INT4) STRLEN (pHttp->au1DataString));
                WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);
                /* Remote Table Willing */
                STRCPY (pHttp->au1KeyString, "REMOTE_APP_PRI_WILLING_KEY");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                nmhGetFslldpXdot1dcbxRemApplicationPriorityWilling
                    (u4LldpV2RemTimeMark, i4IfIndex,
                     u4NextLldpV2RemLocalDestMACAddress,
                     i4LldpV2RemIndex, &i4Willing);
                if (i4Willing == APP_PRI_ENABLED)
                {
                    SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", "Enabled");
                }
                else
                {
                    SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", "Disabled");
                }
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                (INT4) STRLEN (pHttp->au1DataString));
                WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);
                /* Remote Table MAC Address */
                STRCPY (pHttp->au1KeyString, "REMOTE_MAC_ADD_KEY");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", u4LldpV2RemLocalDestMACAddress);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                (INT4) STRLEN (pHttp->au1DataString));
                WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

                nmhGetLldpXdot1dcbxRemApplicationPriorityAEPriority
                    (u4NextLldpV2RemTimeMark, i4NextPort,
                     u4NextLldpV2RemLocalDestMACAddress,
                     i4NextLldpV2RemIndex, i4NextSelector,
                     (UINT4) i4NextProtocol, &u4Priority);
                /* Remote Table Selector */
                STRCPY (pHttp->au1KeyString, "REMOTE_APP_PRI_SELECTOR_KEY");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4NextSelector);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                (INT4) STRLEN (pHttp->au1DataString));
                WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);
                /* Remote Table Protocol */
                STRCPY (pHttp->au1KeyString, "REMOTE_APP_PRI_PROTOCOL_KEY");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4NextProtocol);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                (INT4) STRLEN (pHttp->au1DataString));
                WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);
                /* Remote Table Priority */
                STRCPY (pHttp->au1KeyString, "REMOTE_APP_PRI_PRIORITY_KEY");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", u4Priority);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                (INT4) STRLEN (pHttp->au1DataString));
                WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);
            }
            else
            {
                IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
            }
            u4LldpV2RemTimeMark = u4NextLldpV2RemTimeMark;
            i4LldpV2RemIndex = i4NextLldpV2RemIndex;
            u4LldpV2RemLocalDestMACAddress = u4NextLldpV2RemLocalDestMACAddress;
            i4CurSelector = i4NextSelector;
            i4CurProtocol = i4NextProtocol;
            i4CurPort = i4NextPort;
        }
        WebnmSendString (pHttp, ISS_TABLE_STOP_FLAG);
        i4CurrentPort = i4IfIndex;
    }
    while ((nmhGetNextIndexFsAppPriPortTable (i4CurrentPort,
                                              &i4IfIndex) == SNMP_SUCCESS));

    DcbxUnLock ();
    WebnmUnRegisterLock (pHttp);
    WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                    (pHttp->i4HtmlSize - pHttp->i4Write));
}

/*********************************************************************
 *  Function Name : ETSIssProcessTsaInfoPage
 *  Description   : This function processes the request coming for the
 *                   Dcb ETS TSA page.
 *  
 *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 *  Output(s)     : None.
 *  Return Values : None
 **********************************************************************/

PUBLIC VOID
ETSIssProcessTsaInfoPage (tHttp * pHttp)
{

    if (pHttp->i4Method == ISS_GET)
    {
        ETSIssProcessTsaInfoPageGet (pHttp);
    }
    else if (pHttp->i4Method == ISS_SET)
    {
        ETSIssProcessTsaInfoPageSet (pHttp);
    }
}

/********************************************************************
 *  Function Name : ETSIssProcessTsaInfoPageGet.
 *  Descripetion  : This function process get request coming for the
 *                   DCB ETS TSA Page.
 *  Input(s)      : pHttp - Pointer to the global HTTP data structure
 *  Output(s)     : None
 *  Return Value  : None
 *  
 ********************************************************************/

PRIVATE VOID
ETSIssProcessTsaInfoPageGet (tHttp * pHttp)
{

    INT4                i4CurrentPort = DCBX_ZERO;
    INT4                i4NextPort = DCBX_ZERO;
    INT4                i4TsaId = DCBX_ZERO;
    INT4                i4OutCome = DCBX_ZERO;
    INT4                i4RetVal = DCBX_ZERO;
    INT4                u4Temp = DCBX_ZERO;
    INT1               *piIfName = NULL;
    UINT1               au1IfName[CFA_MAX_PORT_NAME_LENGTH] = { DCBX_ZERO };

    piIfName = (INT1 *) &au1IfName[DCBX_ZERO];
    pHttp->i4Write = DCBX_ZERO;

    WebnmRegisterLock (pHttp, DcbxLock, DcbxUnLock);
    DcbxLock ();

    WebnmSendString (pHttp, ISS_TABLE_FLAG);

    i4OutCome = (INT4) (nmhGetFirstIndexFsETSPortTable (&i4NextPort));

    if (i4OutCome == SNMP_FAILURE)
    {
        DcbxUnLock ();
        WebnmUnRegisterLock (pHttp);
        IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
        return;
    }

    u4Temp = pHttp->i4Write;

    do
    {
        CfaCliGetIfName ((UINT4) i4NextPort, piIfName);
        pHttp->i4Write = u4Temp;
        STRCPY (pHttp->au1KeyString, "PORT_NO_KEY_NAME");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", (CHR1 *) piIfName);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        STRCPY (pHttp->au1KeyString, "PORT_NO_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4NextPort);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        for (i4TsaId = DCBX_ZERO; i4TsaId < DCBX_MAX_PRIORITIES; i4TsaId++)
        {
            if (nmhGetLldpXdot1dcbxAdminETSConTrafficSelectionAlgorithm
                (i4NextPort, (UINT4) i4TsaId, &i4RetVal) == SNMP_SUCCESS)
            {
                STRCPY (pHttp->au1KeyString, "TSA_KEY");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                (INT4) STRLEN (pHttp->au1DataString));
                WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);
            }
        }

        for (i4TsaId = DCBX_ZERO; i4TsaId < DCBX_MAX_PRIORITIES; i4TsaId++)
        {
            if (nmhGetLldpXdot1dcbxAdminETSRecoTrafficSelectionAlgorithm
                (i4NextPort, (UINT4) i4TsaId, &i4RetVal) == SNMP_SUCCESS)
            {
                STRCPY (pHttp->au1KeyString, "RECO_TSA_KEY");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                (INT4) STRLEN (pHttp->au1DataString));
                WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);
            }
        }

        WebnmSendString (pHttp, ISS_TABLE_STOP_FLAG);
        i4CurrentPort = i4NextPort;

    }
    while ((nmhGetNextIndexFsETSPortTable (i4CurrentPort,
                                           &i4NextPort) == SNMP_SUCCESS));

    DcbxUnLock ();
    WebnmUnRegisterLock (pHttp);
    WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                    (pHttp->i4HtmlSize - pHttp->i4Write));

}

/********************************************************************
 *  Funtion Name : ETSIssProcessTsaInfoPageSet.
 *  Descrioption : This Function process Set request coming for the DCB
 *                  ETS TSA page.
 *  Input(s)     : phttp - Pointer to the global HTTP data structure
 *  Ouput(s)     : None.
 *  Return value : None.
 *  
 *********************************************************************/

PRIVATE VOID
ETSIssProcessTsaInfoPageSet (tHttp * pHttp)
{

    INT4                i4PortNo = DCBX_ZERO;
    INT4                i4TsaId = DCBX_ZERO;
    UINT4               u4ErrCode = DCBX_ZERO;
    UINT1               au1TsaTable[ETS_MAX_TCGID_CONF] = { DCBX_ZERO };

    DcbxLock ();
    STRCPY (pHttp->au1Name, "PORT_NO");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4PortNo = ATOI (pHttp->au1Value);

    for (i4TsaId = DCBX_ZERO; i4TsaId < ETS_MAX_TCGID_CONF; i4TsaId++)
    {
        STRCPY (pHttp->au1Name, "CONPG");
        SPRINTF ((CHR1 *) (pHttp->au1Name + STRLEN (pHttp->au1Name)), "%d",
                 i4TsaId);
        MEMSET (pHttp->au1Value, DCBX_ZERO, sizeof (pHttp->au1Value));

        HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery);
        au1TsaTable[i4TsaId] = (UINT1) ATOI (&(pHttp->au1Value[DCBX_ZERO]));

        if (nmhTestv2LldpXdot1dcbxAdminETSConTrafficSelectionAlgorithm
            (&u4ErrCode, i4PortNo, (UINT4) i4TsaId,
             au1TsaTable[i4TsaId]) == SNMP_FAILURE)
        {
            DcbxUnLock ();
            IssSendError (pHttp,
                          (CONST INT1 *) "Unable to set the ETS Con TSA ");
            return;
        }
    }

    for (i4TsaId = DCBX_ZERO; i4TsaId < ETS_MAX_TCGID_CONF; i4TsaId++)
    {
        if (nmhSetLldpXdot1dcbxAdminETSConTrafficSelectionAlgorithm
            (i4PortNo, i4TsaId, au1TsaTable[i4TsaId]) == SNMP_FAILURE)
        {
            DcbxUnLock ();
            IssSendError (pHttp,
                          (CONST INT1 *) "Unable to set the ETS Con TSA");
            return;
        }
    }

    MEMSET (au1TsaTable, DCBX_ZERO, sizeof (au1TsaTable));
    for (i4TsaId = DCBX_ZERO; i4TsaId < ETS_MAX_TCGID_CONF; i4TsaId++)
    {
        STRCPY (pHttp->au1Name, "RECOPG");
        SPRINTF ((CHR1 *) (pHttp->au1Name + STRLEN (pHttp->au1Name)), "%d",
                 i4TsaId);
        MEMSET (pHttp->au1Value, DCBX_ZERO, sizeof (pHttp->au1Value));

        HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery);
        au1TsaTable[i4TsaId] = (UINT1) ATOI (&(pHttp->au1Value[DCBX_ZERO]));

        if (nmhTestv2LldpXdot1dcbxAdminETSRecoTrafficSelectionAlgorithm
            (&u4ErrCode, i4PortNo, (UINT4) i4TsaId,
             au1TsaTable[i4TsaId]) == SNMP_FAILURE)
        {
            DcbxUnLock ();
            IssSendError (pHttp,
                          (CONST INT1 *) "Unable to set the ETS Reco TSA ");
            return;
        }
    }

    for (i4TsaId = DCBX_ZERO; i4TsaId < ETS_MAX_TCGID_CONF; i4TsaId++)
    {
        if (nmhSetLldpXdot1dcbxAdminETSRecoTrafficSelectionAlgorithm
            (i4PortNo, i4TsaId, au1TsaTable[i4TsaId]) == SNMP_FAILURE)
        {
            DcbxUnLock ();
            IssSendError (pHttp,
                          (CONST INT1 *) "Unable to set the ETS Reco TSA");
            return;
        }
    }
    DcbxUnLock ();
    ETSIssProcessTsaInfoPageGet (pHttp);

}

/*********************************************************************
*  Function Name : IssProcessDcbHomePage
*  Description   : This function processes the request coming for the 
*                  DCB Home page
*                     
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/

PUBLIC VOID
DcbIssProcessHomePage (tHttp * pHttp)
{
    tSNMP_OCTET_STRING_TYPE OctetStr;
    OctetStr.pu1_OctetList = IssSysGetSwitchName ();
    OctetStr.i4_Length = (INT4) STRLEN (OctetStr.pu1_OctetList);

    STRCPY (pHttp->au1KeyString, "<! SWITCH_NAME>");
    WebnmSendString (pHttp, pHttp->au1KeyString);

    WebnmRegisterLock (pHttp, IssLock, IssUnLock);
    ISS_LOCK ();
    SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", OctetStr.pu1_OctetList);
    ISS_UNLOCK ();
    WebnmUnRegisterLock (pHttp);

    WebnmSockWrite (pHttp, pHttp->au1DataString,
                    (INT4) STRLEN (pHttp->au1DataString));

    STRCPY (pHttp->au1KeyString, "<! DCBX_LINKS>");
    WebnmSendString (pHttp, pHttp->au1KeyString);

    STRCPY (pHttp->au1DataString,
            "<p class=\"leftalign\">Through the <a class=\"thirdlevel\" "
            "href=\"/iss/cn_global_settings.html?Gambit=GAMBIT\">CN</a>"
            "link you can configure the CN Global and per Port Setting and"
            "Congestion point Parameters<br></p>\r\n");
    WebnmSockWrite (pHttp, pHttp->au1DataString,
                    (INT4) STRLEN (pHttp->au1DataString));

    STRCPY (pHttp->au1DataString,
            "<p class=\"leftalign\">Through the <a class=\"thirdlevel\"i"
            "href=\"/iss/dcbx_adminstatus.html?Gambit=GAMBIT\">DCBX</a> link"
            "you can configure the Dcbx status per port settings and to"
            "Enable or Disable Global traces. <br></p>\r\n");
    WebnmSockWrite (pHttp, pHttp->au1DataString,
                    (INT4) STRLEN (pHttp->au1DataString));

    STRCPY (pHttp->au1DataString,
            "<p class=\"leftalign\">Through the <a class=\"thirdlevel\""
            "href=\"/iss/ets_globalInfo.html?Gambit=GAMBIT\">ETS</a> link you "
            "can view and configure ETS Global Configurations like"
            "System control status,module status and traps, Port Configurations"
            "like port Mode,Bandwidth for TrafficClasses and Mapping priorities"
            "to Traffic classes and to view Local and Remote"
            "configuration. <br></p>\r\n");
    WebnmSockWrite (pHttp, pHttp->au1DataString,
                    (INT4) STRLEN (pHttp->au1DataString));
    STRCPY (pHttp->au1DataString,
            "<p class=\"leftalign\">Through the <a class=\"thirdlevel\""
            "href=\"/iss/pfc_GlobalInfo.html?Gambit=GAMBIT\">PFC</a> link you"
            "can view and configure PFC Global Configurations like System"
            "control status,Modue status and traps,Port Configurations like"
            "poRt Mode and setting  priority status and to view Local and"
            "Remote configuration. <br></p>\r\n");
    WebnmSockWrite (pHttp, pHttp->au1DataString,
                    (INT4) STRLEN (pHttp->au1DataString));
    STRCPY (pHttp->au1DataString,
            "<p class=\"leftalign\">Through the <a class=\"thirdlevel\""
            "href=\"/iss/ap_GlobalInfo.html?Gambit=GAMBIT\">Application "
            "Priority</a> link you can view and configure Application Priority"
            "Global Configurations like System"
            "control status,Modue status and traps,Port Configurations like"
            "port Mode and setting priority for applications and to view Local and"
            "Remote configuration. <br></p>\r\n");
    WebnmSockWrite (pHttp, pHttp->au1DataString,
                    (INT4) STRLEN (pHttp->au1DataString));

/*Free the buffer */
    /* free_octetstring (OctetStr); */
}
