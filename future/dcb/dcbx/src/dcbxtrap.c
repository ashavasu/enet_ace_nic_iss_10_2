/*****************************************************************************
 * Copyright (C) 2010 Aricent Inc . All Rights Reserved
 *
 * $Id: dcbxtrap.c,v 1.14 2016/07/02 10:01:02 siva Exp $
 *
 * Description: This file contains the functions to send the trap message.
 ******************************************************************************/
#include "dcbxinc.h"
#include "snmputil.h"
#ifdef SNMP_3_WANTED
#include "fsdcbx.h"
#endif

CHR1               *gac1ETSSysLogMsg[DCBX_MAX_SYSLOG_MSG] = {
    NULL,
    "ETS Module Status Changed",
    "ETS Port Admin Mode Changed",
    "ETS Remote Status Changed",
    "ETS Oper State Changed"
};
CHR1               *gac1PFCSysLogMsg[DCBX_MAX_SYSLOG_MSG] = {
    NULL,
    "PFC Module Status Changed",
    "PFC Port Admin Mode Changed",
    "PFC Remote Status Changed",
    "PFC Oper State Changed"
};
CHR1               *gac1AppPriSysLogMsg[DCBX_MAX_SYSLOG_MSG] = {
    NULL,
    "Application Priority Module Status Changed",
    "Application Priority Port Admin Mode Changed",
    "Application Priority Remote Status Changed",
    "Application Priority Oper State Changed"
};
CHR1 		        *gac1CEESysLogMsg[] = {
	NULL,
	"fsDcbxCEELldpTxDisabledTrap",
	"fsDcbxCEELldpRxDisabledTrap",
	"fsDcbxCEEDupControlTlvTrap",
	"CEE - Recieved No Response from Peer",
	"fsDcbxCEEDupFeatTlvTrap",
	"fsDcbxCEEPeerNoFeatTrap",
	"fsDcbxCEEFeatErrorTrap",
	"fsDcbxCEEAppPriProtocolNotSuppTrap",
	"fsDcbxCEEVersionChanged"
};

#ifdef SNMP_3_WANTED
/******************************************************************************
* Function :   DcbxMakeObjIdFrmString
*
* Description: This Function retuns the OID  of the given string for the 
*              given MIB.
*
* Input    :   pi1TextStr - pointer to the string.
*              pTableName - TableName has to be fetched.
*
* Output   :   None.
*
* Returns  :   pOidPtr or NULL
*******************************************************************************/

tSNMP_OID_TYPE     *
DcbxMakeObjIdFrmString (INT1 *pi1TextStr, UINT1 *pu1TableName)
{
    tSNMP_OID_TYPE     *pOidPtr = NULL;
    INT1               *pi1DotPtr = NULL;
    UINT2               u2Index = DCBX_ZERO;
    INT1                ai1TempBuffer[DCBX_OBJECT_NAME_LEN + DCBX_ONE]
        = { DCBX_ZERO };
    UINT1              *pu1TempPtr = NULL;
    struct MIB_OID     *pTableName = NULL;
    pTableName = (struct MIB_OID *) (VOID *) pu1TableName;

    pi1DotPtr = pi1TextStr + STRLEN ((INT1 *) pi1TextStr);
    pu1TempPtr = (UINT1 *) pi1TextStr;
    UINT2               u2Len = 0;

    for (u2Index = DCBX_ZERO;
         ((pu1TempPtr < (UINT1 *) pi1DotPtr) &&
          (u2Index < DCBX_OBJECT_NAME_LEN)); u2Index++)
    {
        ai1TempBuffer[u2Index] = (INT1) *pu1TempPtr++;
    }
    ai1TempBuffer[u2Index] = '\0';
    for (u2Index = DCBX_ZERO; pTableName[u2Index].pName != NULL; u2Index++)
    {
        if ((STRCMP
             (pTableName[u2Index].pName,
              (INT1 *) ai1TempBuffer) == DCBX_ZERO)
            && (STRLEN ((INT1 *) ai1TempBuffer) ==
                STRLEN (pTableName[u2Index].pName)))
        {
            u2Len =
                (UINT2) (STRLEN (pTableName[u2Index].pNumber) <
                        (sizeof(ai1TempBuffer)-1) ?
                        (STRLEN (pTableName[u2Index].pNumber)) : (sizeof(ai1TempBuffer)-1));

            STRNCPY ((INT1 *) ai1TempBuffer, pTableName[u2Index].pNumber,
                     u2Len);
            ai1TempBuffer[u2Len] = '\0';
            break;
        }
    }
    if (pTableName[u2Index].pName == NULL)
    {
        return (NULL);
    }

    /* Now we've got something with numbers instead of an alpha header */

    /* count the dots.  num +1 is the number of SID's */
    pOidPtr = SNMP_AGT_GetOidFromString ((INT1 *) ai1TempBuffer);

    return (pOidPtr);
}
#endif

/******************************************************************************
* Function Name      : DcbxSendNotification 
*
* Description        : This routine will send the snmp traps 
*
* Input(s)           : u1TrapId - Trap ID
*                      pNotifyInfo - Pointer to tDcbxTrapMsg
*
* Output(s)          : None
*
* Return Value(s)    : None
*****************************************************************************/
VOID
DcbxSendNotification (tDcbxTrapMsg * pNotifyInfo, UINT1 u1TrapId)
{

#ifdef SNMP_3_WANTED
    tSNMP_VAR_BIND     *pVbList = NULL;
    tSNMP_VAR_BIND     *pStartVb = NULL;
    tSNMP_OID_TYPE     *pEnterpriseOid = NULL;
    tSNMP_OID_TYPE     *pSnmpTrapOid = NULL;
    tSNMP_COUNTER64_TYPE u8CounterVal = { DCBX_ZERO, DCBX_ZERO };
    UINT4               au4SnmpTrapOid[] = DCBX_SNMP_TRAP_OID;
    UINT1               au1Buf[DCBX_OBJECT_NAME_LEN] = { DCBX_ZERO };
    tSNMP_OID_TYPE     *pOid = NULL;
    tSNMP_COUNTER64_TYPE SnmpCnt64Type;

    SnmpCnt64Type.msn = DCBX_ZERO;
    SnmpCnt64Type.lsn = DCBX_ZERO;

    /* Checking Trap is enabled or not */
    if (pNotifyInfo->u1Module == ETS_TRAP)
    {
        if ((gETSGlobalInfo.u4ETSTrap == DCBX_ZERO) ||
            ((!(gETSGlobalInfo.u4ETSTrap & ETS_MODULE_STATUS_TRAP)) &&
             (u1TrapId == MODULE_TRAP)) ||
            ((!(gETSGlobalInfo.u4ETSTrap & ETS_ADMIN_MODE_TRAP)) &&
             (u1TrapId == ADMIN_STATUS_TRAP)) ||
            ((!(gETSGlobalInfo.u4ETSTrap & ETS_PEER_STATUS_TRAP)) &&
             (u1TrapId == PEER_STATE_TRAP)) ||
            ((!(gETSGlobalInfo.u4ETSTrap & ETS_SEM_TRAP)) &&
             (u1TrapId == OPER_STATE_TRAP)))
        {
            return;
        }

    }
    else if (pNotifyInfo->u1Module == PFC_TRAP)
    {
        if ((gPFCGlobalInfo.u4PFCTrap == DCBX_ZERO) ||
            ((!(gPFCGlobalInfo.u4PFCTrap & PFC_MODULE_STATUS_TRAP)) &&
             (u1TrapId == MODULE_TRAP)) ||
            ((!(gPFCGlobalInfo.u4PFCTrap & PFC_ADMIN_MODE_TRAP)) &&
             (u1TrapId == ADMIN_STATUS_TRAP)) ||
            ((!(gPFCGlobalInfo.u4PFCTrap & PFC_PEER_STATUS_TRAP)) &&
             (u1TrapId == PEER_STATE_TRAP)) ||
            ((!(gPFCGlobalInfo.u4PFCTrap & PFC_SEM_TRAP)) &&
             (u1TrapId == OPER_STATE_TRAP)))
        {
            return;
        }

    }
    else
    {
        if ((gAppPriGlobalInfo.u4AppPriTrapStatus == DCBX_ZERO) ||
            ((!(gAppPriGlobalInfo.
                u4AppPriTrapStatus & APP_PRI_MODULE_STATUS_TRAP))
             && (u1TrapId == MODULE_TRAP))
            ||
            ((!(gAppPriGlobalInfo.u4AppPriTrapStatus & APP_PRI_ADMIN_MODE_TRAP))
             && (u1TrapId == ADMIN_STATUS_TRAP))
            ||
            ((!(gAppPriGlobalInfo.
                u4AppPriTrapStatus & APP_PRI_PEER_STATUS_TRAP))
             && (u1TrapId == PEER_STATE_TRAP))
            || ((!(gAppPriGlobalInfo.u4AppPriTrapStatus & APP_PRI_SEM_TRAP))
                && (u1TrapId == OPER_STATE_TRAP)))
        {
            return;
        }
    }

    pSnmpTrapOid = alloc_oid (DCBX_SNMPV2_TRAP_OID_LEN);
    if (pSnmpTrapOid == NULL)
    {
        DCBX_TRC (DCBX_FAILURE_TRC,
                  "DcbxSendNotification: OID Memory Allocation Failed\r\n");
        return;
    }
    MEMCPY (pSnmpTrapOid->pu4_OidList, au4SnmpTrapOid,
            DCBX_SNMPV2_TRAP_OID_LEN * sizeof (UINT4));

    SPRINTF ((char *) au1Buf, "fsDCBTraps");
    pEnterpriseOid =
        DcbxMakeObjIdFrmString ((INT1 *) au1Buf,
                                (UINT1 *) fs_dcbx_orig_mib_oid_table);
    if (pEnterpriseOid == NULL)
    {
        SNMP_FreeOid (pSnmpTrapOid);
        DCBX_TRC (DCBX_FAILURE_TRC,
                  "DcbxSendNotification: OID generation"
                  "from String Failed\r\n");
        return;
    }
    if (u1TrapId == MODULE_TRAP)
    {
        pEnterpriseOid->u4_Length = pEnterpriseOid->u4_Length - DCBX_ONE;
        if (pNotifyInfo->u1Module == ETS_TRAP)
        {
            pEnterpriseOid->pu4_OidList[pEnterpriseOid->u4_Length++] =
                MODULE_TRAP;
        }
        else if (pNotifyInfo->u1Module == PFC_TRAP)
        {
            pEnterpriseOid->pu4_OidList[pEnterpriseOid->u4_Length++] =
                PFC_TRAP_START + MODULE_TRAP;
        }
        else
        {
            pEnterpriseOid->pu4_OidList[pEnterpriseOid->u4_Length++] =
                APP_PRI_TRAP_START + MODULE_TRAP;
        }
        pVbList = (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind
            (pSnmpTrapOid, SNMP_DATA_TYPE_OBJECT_ID, DCBX_ZERO,
             DCBX_ZERO, NULL, pEnterpriseOid, u8CounterVal);
        if (pVbList == NULL)
        {
            SNMP_FreeOid (pEnterpriseOid);
            SNMP_FreeOid (pSnmpTrapOid);
            DCBX_TRC (DCBX_FAILURE_TRC,
                      "DcbxSendNotification: Variable Binding Failed\r\n");
            return;
        }

        pStartVb = pVbList;

        MEMSET (au1Buf, DCBX_ZERO, DCBX_OBJECT_NAME_LEN);
        if (pNotifyInfo->u1Module == ETS_TRAP)
        {
            SPRINTF ((char *) au1Buf, "fsETSModuleStatus");
        }
        else if (pNotifyInfo->u1Module == PFC_TRAP)
        {
            SPRINTF ((char *) au1Buf, "fsPFCModuleStatus");
        }
        else
        {
            SPRINTF ((char *) au1Buf, "fsAppPriModuleStatus");
        }

        pOid =
            DcbxMakeObjIdFrmString ((INT1 *) au1Buf,
                                    (UINT1 *) fs_dcbx_orig_mib_oid_table);
        if (pOid == NULL)
        {
            SNMP_free_snmp_vb_list (pStartVb);
            DCBX_TRC (DCBX_FAILURE_TRC,
                      "DcbxSendNotification: OID Not Found\r\n");
            return;
        }
        pVbList->pNextVarBind = SNMP_AGT_FormVarBind (pOid,
                                                      SNMP_DATA_TYPE_INTEGER32,
                                                      DCBX_ZERO,
                                                      pNotifyInfo->unTrapMsg.
                                                      u1ModuleStatus,
                                                      NULL, NULL,
                                                      SnmpCnt64Type);
        if (pVbList->pNextVarBind == NULL)
        {
            SNMP_free_snmp_vb_list (pStartVb);
            SNMP_FreeOid (pOid);
            DCBX_TRC (DCBX_FAILURE_TRC,
                      "DcbxSendNotification: Variable Binding Failed\r\n");
            return;
        }

        pVbList = pVbList->pNextVarBind;
        pVbList->pNextVarBind = NULL;
    }
    else
    {
        pEnterpriseOid->u4_Length = pEnterpriseOid->u4_Length - DCBX_ONE;
        if (pNotifyInfo->u1Module == ETS_TRAP)
        {
            pEnterpriseOid->pu4_OidList[pEnterpriseOid->u4_Length++]
                = (UINT4) u1TrapId;
        }
        else if (pNotifyInfo->u1Module == PFC_TRAP)
        {
            pEnterpriseOid->pu4_OidList[pEnterpriseOid->u4_Length++]
                = (PFC_TRAP_START + (UINT4) u1TrapId);
        }
        else
        {
            pEnterpriseOid->pu4_OidList[pEnterpriseOid->u4_Length++]
                = (APP_PRI_TRAP_START + (UINT4) u1TrapId);
        }
        pVbList = (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind
            (pSnmpTrapOid, SNMP_DATA_TYPE_OBJECT_ID, DCBX_ZERO, DCBX_ZERO, NULL,
             pEnterpriseOid, u8CounterVal);
        if (pVbList == NULL)
        {
            SNMP_FreeOid (pEnterpriseOid);
            SNMP_FreeOid (pSnmpTrapOid);
            DCBX_TRC (DCBX_FAILURE_TRC,
                      "DcbxSendNotification: Variable Binding Failed\r\n");
            return;
        }

        pStartVb = pVbList;
        MEMSET (au1Buf, DCBX_ZERO, DCBX_OBJECT_NAME_LEN);
        SPRINTF ((char *) au1Buf, "fsDcbTrapPortNumber");
        pOid =
            DcbxMakeObjIdFrmString ((INT1 *) au1Buf,
                                    (UINT1 *) fs_dcbx_orig_mib_oid_table);
        if (pOid == NULL)
        {
            SNMP_free_snmp_vb_list (pStartVb);
            DCBX_TRC (DCBX_FAILURE_TRC,
                      "DcbxSendNotification: OID Not Found\r\n");
            return;
        }

        pVbList->pNextVarBind = SNMP_AGT_FormVarBind (pOid,
                                                      SNMP_DATA_TYPE_INTEGER32,
                                                      DCBX_ZERO,
                                                      (INT4) pNotifyInfo->
                                                      u4IfIndex, NULL, NULL,
                                                      SnmpCnt64Type);
        if (pVbList->pNextVarBind == NULL)
        {
            SNMP_free_snmp_vb_list (pStartVb);
            SNMP_FreeOid (pOid);
            DCBX_TRC (DCBX_FAILURE_TRC,
                      "DcbxSendNotification: Variable Binding Failed\r\n");
            return;
        }

        pVbList = pVbList->pNextVarBind;

        if (u1TrapId == ADMIN_STATUS_TRAP)
        {
            MEMSET (au1Buf, DCBX_ZERO, DCBX_OBJECT_NAME_LEN);
            if (pNotifyInfo->u1Module == ETS_TRAP)
            {
                SPRINTF ((char *) au1Buf, "fsETSAdminMode");
            }
            else if (pNotifyInfo->u1Module == PFC_TRAP)
            {
                SPRINTF ((char *) au1Buf, "fsPFCAdminMode");
            }
            else
            {
                SPRINTF ((char *) au1Buf, "fsAppPriAdminMode");
            }
            pOid =
                DcbxMakeObjIdFrmString ((INT1 *) au1Buf,
                                        (UINT1 *) fs_dcbx_orig_mib_oid_table);
            if (pOid == NULL)
            {
                SNMP_free_snmp_vb_list (pStartVb);
                DCBX_TRC (DCBX_FAILURE_TRC,
                          "DcbxSendNotification: OID Not Found\r\n");
                return;
            }
            pVbList->pNextVarBind =
                SNMP_AGT_FormVarBind (pOid,
                                      SNMP_DATA_TYPE_INTEGER32,
                                      DCBX_ZERO,
                                      pNotifyInfo->unTrapMsg.u1AdminMode,
                                      NULL, NULL, SnmpCnt64Type);
            if (pVbList->pNextVarBind == NULL)
            {
                SNMP_free_snmp_vb_list (pStartVb);
                SNMP_FreeOid (pOid);
                DCBX_TRC (DCBX_FAILURE_TRC,
                          "DcbxSendNotification: Variable Binding Failed\r\n");
                return;
            }
        }
        else if (u1TrapId == PEER_STATE_TRAP)
        {
            MEMSET (au1Buf, DCBX_ZERO, DCBX_OBJECT_NAME_LEN);
            SPRINTF ((char *) au1Buf, "fsDcbPeerUpStatus");
            pOid =
                DcbxMakeObjIdFrmString ((INT1 *) au1Buf,
                                        (UINT1 *) fs_dcbx_orig_mib_oid_table);
            if (pOid == NULL)
            {
                SNMP_free_snmp_vb_list (pStartVb);
                DCBX_TRC (DCBX_FAILURE_TRC,
                          "DcbxSendNotification: OID Not Found\r\n");
                return;
            }
            pVbList->pNextVarBind =
                SNMP_AGT_FormVarBind (pOid,
                                      SNMP_DATA_TYPE_INTEGER32,
                                      DCBX_ZERO,
                                      pNotifyInfo->unTrapMsg.u1PeerStatus,
                                      NULL, NULL, SnmpCnt64Type);
            if (pVbList->pNextVarBind == NULL)
            {
                SNMP_free_snmp_vb_list (pStartVb);
                SNMP_FreeOid (pOid);
                DCBX_TRC (DCBX_FAILURE_TRC,
                          "DcbxSendNotification: Variable Binding Failed\r\n");
                return;
            }

        }
        else
        {
            MEMSET (au1Buf, DCBX_ZERO, DCBX_OBJECT_NAME_LEN);
            if (pNotifyInfo->u1Module == ETS_TRAP)
            {
                SPRINTF ((char *) au1Buf, "fsETSDcbxOperState");
            }
            else if (pNotifyInfo->u1Module == PFC_TRAP)
            {
                SPRINTF ((char *) au1Buf, "fsPFCDcbxOperState");
            }
            else
            {
                SPRINTF ((char *) au1Buf, "fsAppPriDcbxOperState");
            }
            pOid =
                DcbxMakeObjIdFrmString ((INT1 *) au1Buf,
                                        (UINT1 *) fs_dcbx_orig_mib_oid_table);
            if (pOid == NULL)
            {
                SNMP_free_snmp_vb_list (pStartVb);
                DCBX_TRC (DCBX_FAILURE_TRC,
                          "DcbxSendNotification: OID Not Found\r\n");
                return;
            }
            pVbList->pNextVarBind =
                SNMP_AGT_FormVarBind (pOid,
                                      SNMP_DATA_TYPE_INTEGER32,
                                      DCBX_ZERO,
                                      pNotifyInfo->unTrapMsg.u1OperState,
                                      NULL, NULL, SnmpCnt64Type);
            if (pVbList->pNextVarBind == NULL)
            {
                SNMP_free_snmp_vb_list (pStartVb);
                SNMP_FreeOid (pOid);
                DCBX_TRC (DCBX_FAILURE_TRC,
                          "DcbxSendNotification: Variable Binding Failed\r\n");
                return;
            }
        }
        pVbList = pVbList->pNextVarBind;
        pVbList->pNextVarBind = NULL;

    }
    if ((u1TrapId <= DCBX_ZERO) || (u1TrapId >= DCBX_MAX_SYSLOG_MSG))
    {
        SNMP_free_snmp_vb_list (pStartVb);
        return;
    }
    if (pNotifyInfo->u1Module == ETS_TRAP)
    {
        DcbxPortNotifyFaults (pStartVb, (UINT1 *) gac1ETSSysLogMsg[u1TrapId],
                              FM_NOTIFY_MOD_ID_DCBX);
        /*Incrementing ETS Trap Count */
        gETSGlobalInfo.u4ETSGeneratedTrapCount++;
    }
    else if (pNotifyInfo->u1Module == PFC_TRAP)
    {
        DcbxPortNotifyFaults (pStartVb, (UINT1 *) gac1PFCSysLogMsg[u1TrapId],
                              FM_NOTIFY_MOD_ID_DCBX);
        /*Incrementing PFC Trap Count */
        gPFCGlobalInfo.u4PFCGeneratedTrapCount++;
    }
    else
    {
        DcbxPortNotifyFaults (pStartVb, (UINT1 *) gac1AppPriSysLogMsg[u1TrapId],
                              FM_NOTIFY_MOD_ID_DCBX);
        /*Incrementing Application Priority Trap Count */
        gAppPriGlobalInfo.u4AppPriGeneratedTrapCount++;
    }
#else
    UNUSED_PARAM (u1TrapId);
    UNUSED_PARAM (pNotifyInfo);

#endif
}

/******************************************************************************
* Function Name      : DcbxCEESendPortNotification 
*
* Description        : This routine will send the snmp traps for CEE
* 			           which take port number as the key.
*
* Input(s)           : u4TrapId - Trap ID
*                      pNotifyInfo - Pointer to tDcbxCEETrapMsg
*
* Output(s)          : None
*
* Return Value(s)    : None
*****************************************************************************/
VOID
DcbxCEESendPortNotification (tDcbxCEETrapMsg * pNotifyInfo, UINT4 u4TrapId)
{
#ifdef SNMP_3_WANTED
    tSNMP_VAR_BIND       *pVbList = NULL;
    tSNMP_VAR_BIND       *pStartVb = NULL;
    tSNMP_OID_TYPE       *pEnterpriseOid = NULL;
    tSNMP_OID_TYPE       *pSnmpTrapOid = NULL;
    tSNMP_COUNTER64_TYPE  u8CounterVal = { DCBX_ZERO, DCBX_ZERO };
    tSNMP_OID_TYPE       *pOid = NULL;
    tSNMP_COUNTER64_TYPE  SnmpCnt64Type;
    tSNMP_OID_TYPE       *pverOid = NULL;
    tSNMP_OID_TYPE       *pfeatOid = NULL;
    UINT4                 au4SnmpTrapOid[] = DCBX_SNMP_TRAP_OID;
    UINT4                 u4TempTrapId = u4TrapId;
    UINT1                 u1TrapPos = 1;
    UINT1                 au1Buf[DCBX_OBJECT_NAME_LEN] = { DCBX_ZERO };

    SnmpCnt64Type.msn = DCBX_ZERO;
    SnmpCnt64Type.lsn = DCBX_ZERO;

    /* Checking Trap is enabled or not */
    if ((gCEEGlobalInfo.u4CEETrap == DCBX_ZERO) ||
            !(gCEEGlobalInfo.u4CEETrap & u4TrapId))
    {
        DCBX_TRC (DCBX_MGMT_TRC,
                "DcbxCEESendPortNotification: Recieved Trap is not Enabled!!\r\n");
        return;
    }

    /* Step 1 : Bind DCBX OID */
    pSnmpTrapOid = alloc_oid (DCBX_SNMPV2_TRAP_OID_LEN);
    if (pSnmpTrapOid == NULL)
    {
        DCBX_TRC (DCBX_MGMT_TRC,
                "DcbxCEESendPortNotification: OID Memory Allocation Failed\r\n");
        return;
    }
    MEMCPY (pSnmpTrapOid->pu4_OidList, au4SnmpTrapOid,
            DCBX_SNMPV2_TRAP_OID_LEN * sizeof (UINT4));

    DCBX_TRC (DCBX_MGMT_TRC,
            "DcbxCEESendPortNotification: SNMPv2 OID Obtained!\r\n");
    SPRINTF ((char *) au1Buf, "fsDCBTraps");
    pEnterpriseOid =
        DcbxMakeObjIdFrmString ((INT1 *) au1Buf,
                                (UINT1 *) fs_dcbx_orig_mib_oid_table);
    if (pEnterpriseOid == NULL)
    {
        SNMP_FreeOid (pSnmpTrapOid);
        DCBX_TRC (DCBX_MGMT_TRC,
                  "DcbxCEESendPortNotification: OID generation"
                  "from String Failed\r\n");
        return;
    }
    DCBX_TRC (DCBX_MGMT_TRC,
            "DcbxCEESendPortNotification: fsDCBTraps OID Bound!\r\n");

    pEnterpriseOid->u4_Length = pEnterpriseOid->u4_Length - DCBX_ONE;

    GET_SET_TRAP_POSITION(u4TempTrapId, u1TrapPos);

    pEnterpriseOid->pu4_OidList[pEnterpriseOid->u4_Length++] =
        (UINT4)(CEE_TRAP_START + u1TrapPos);
    DCBX_TRC (DCBX_MGMT_TRC,
            "DcbxCEESendPortNotification: Added Trap Id!\r\n");

    pVbList = (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind
        (pSnmpTrapOid, SNMP_DATA_TYPE_OBJECT_ID, DCBX_ZERO,
         DCBX_ZERO, NULL, pEnterpriseOid, u8CounterVal);
    if (pVbList == NULL)
    {
        SNMP_FreeOid (pEnterpriseOid);
        SNMP_FreeOid (pSnmpTrapOid);
        DCBX_TRC (DCBX_MGMT_TRC,
                "DcbxCEESendPortNotification: Trap Id Binding Failed\r\n");
        return;
    }
    /*End of Step 1*/
    pStartVb = pVbList;
    MEMSET (au1Buf, DCBX_ZERO, DCBX_OBJECT_NAME_LEN);

    /* Step 2 : Bind the trap port number in the next element*/
    SPRINTF ((char *) au1Buf, "fsDcbTrapPortNumber");
    pOid =
        DcbxMakeObjIdFrmString ((INT1 *) au1Buf,
                (UINT1 *) fs_dcbx_orig_mib_oid_table);
    if (pOid == NULL)
    {
        SNMP_free_snmp_vb_list (pStartVb);
        DCBX_TRC (DCBX_MGMT_TRC,
                "DcbxCEESendPortNotification: OID Not Found\r\n");
        return;
    }

    DCBX_TRC (DCBX_MGMT_TRC,
            "DcbxCEESendPortNotification: Added Port Number!\r\n");
    pVbList->pNextVarBind = SNMP_AGT_FormVarBind (pOid,
            SNMP_DATA_TYPE_INTEGER32,
            DCBX_ZERO,
            (INT4) pNotifyInfo->
            u4IfIndex, NULL, NULL,
            SnmpCnt64Type);
    if (pVbList->pNextVarBind == NULL)
    {
        SNMP_free_snmp_vb_list (pStartVb);
        SNMP_FreeOid (pOid);
        DCBX_TRC (DCBX_MGMT_TRC,
                "DcbxCEESendPortNotification: Port Number Binding Failed\r\n");
        return;
    }

    if(u4TrapId == DCBX_VER_CHG_TRAP)
    {
        pVbList = pVbList->pNextVarBind;
        MEMSET (au1Buf, DCBX_ZERO, DCBX_OBJECT_NAME_LEN);
        SPRINTF ((char *) au1Buf, "fsDcbOperVersion");
        pverOid =
            DcbxMakeObjIdFrmString ((INT1 *) au1Buf,
                    (UINT1 *) fs_dcbx_orig_mib_oid_table);
        if (pverOid == NULL)
        {
            SNMP_free_snmp_vb_list (pStartVb);
            DCBX_TRC (DCBX_MGMT_TRC,
                    "DcbxCEESendPortNotification: OID Not Found\r\n");
            return;
        }
        pVbList->pNextVarBind = SNMP_AGT_FormVarBind (pverOid,
                SNMP_DATA_TYPE_INTEGER32,
                DCBX_ZERO,
                (INT4) pNotifyInfo->unTrapMsg.
                u1OperVersion,
                NULL, NULL,
                SnmpCnt64Type);
        if (pVbList->pNextVarBind == NULL)
        {
            SNMP_free_snmp_vb_list (pStartVb);
            SNMP_FreeOid (pverOid);
            DCBX_TRC (DCBX_MGMT_TRC,
                    "DcbxCEESendPortNotification: Oper Version Binding Failed\r\n");
            return;
        }
        DCBX_TRC (DCBX_MGMT_TRC,
                "DcbxCEESendPortNotification: Added Oper Version!\r\n");
    }
    else if((u4TrapId == DCBX_DUP_FEAT_TLV_TRAP)|| 
            (u4TrapId == DCBX_NO_FEAT_TLV_TRAP) ||
            (u4TrapId == DCBX_PEER_TIMEOUT_TRAP) ||
            (u4TrapId == DCBX_FEAT_ERROR_TRAP))
    {
        pVbList = pVbList->pNextVarBind;
        MEMSET (au1Buf, DCBX_ZERO, DCBX_OBJECT_NAME_LEN);
        SPRINTF ((char *) au1Buf, "fsDcbFeatureType");
        pfeatOid =
            DcbxMakeObjIdFrmString ((INT1 *) au1Buf,
                    (UINT1 *) fs_dcbx_orig_mib_oid_table);
        if (pfeatOid == NULL)
        {
            SNMP_free_snmp_vb_list (pStartVb);
            DCBX_TRC (DCBX_MGMT_TRC,
                    "DcbxCEESendPortNotification: OID Not Found\r\n");
            return;
        }
        pVbList->pNextVarBind = SNMP_AGT_FormVarBind (pfeatOid,
                SNMP_DATA_TYPE_INTEGER32,
                DCBX_ZERO,
                (INT4) pNotifyInfo->unTrapMsg.
                u1FeatType,
                NULL, NULL,
                SnmpCnt64Type);
        if (pVbList->pNextVarBind == NULL)
        {
            SNMP_free_snmp_vb_list (pStartVb);
            SNMP_FreeOid (pfeatOid);
            DCBX_TRC (DCBX_MGMT_TRC,
                    "DcbxCEESendPortNotification: Feature Type Binding Failed\r\n");
            return;
        }

        DCBX_TRC (DCBX_MGMT_TRC,
                "DcbxCEESendPortNotification: Added Feature Type!\r\n");
    }
    pVbList = pVbList->pNextVarBind;
    pVbList->pNextVarBind = NULL;

    DcbxPortNotifyFaults (pStartVb, (UINT1 *) gac1CEESysLogMsg[u1TrapPos],
            FM_NOTIFY_MOD_ID_DCBX);

    DCBX_TRC (DCBX_MGMT_TRC,
            "DcbxCEESendPortNotification: Notified the Trap Msg!\r\n");
    /*Step 4 : Incrementing CEE Trap Count */
    gCEEGlobalInfo.u4CEETrapGeneratedCount++;
    DCBX_TRC (DCBX_MGMT_TRC,
            "DcbxCEESendPortNotification: Incremented Trap Count!\r\n");
#else
    UNUSED_PARAM (u4TrapId);
    UNUSED_PARAM (pNotifyInfo);

#endif
}

/* End of file */
