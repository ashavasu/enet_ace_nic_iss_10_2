/*************************************************************************
 * Copyright (C) 2010 Aricent Inc . All Rights Reserved
 * $Id: dcbxsem.c,v 1.2 2010/08/17 12:33:26 prabuc Exp $
 * Description: This file contains DCBX function which handles
 *               DCBX State Event Machine. 
****************************************************************************/
#include "dcbxinc.h"

PRIVATE UINT1       DcbxSemASymStateMachine (UINT1 u1LocalWilling);
PRIVATE UINT1       DcbxSemSymStateMachine (UINT4 u4IfIndex,
                                            UINT1 u1LocalWilling,
                                            UINT1 u1RemoteWilling,
                                            tMacAddr RemAddr);

/***************************************************************************
 *  FUNCTION NAME : DcbxSemUpdate
 * 
 *  DESCRIPTION   : This function is used to update the state machine
 *                  and send the oper state to respective applcation.
 * 
 *  INPUT         : u4IfIndex - Port Number.
 *                  pDcbxAppPortMsg - Appl Reg Info structure
 * 
 *  OUTPUT        : None
 * 
 *  RETURNS       : OSIX_SUCCESS/OSIX_FAILURE
 * 
 * **************************************************************************/
PUBLIC INT4
DcbxSemUpdate (UINT4 u4IfIndex, tDcbxAppRegInfo * pDcbxAppPortMsg)
{
    static tDcbxAppEntry TempDcbxAppEntry;
    /* This contains port list array which results in stack size
     * exceeds 500. So declaring this varaible as static */
    tDcbxAppEntry      *pDcbxAppEntry = NULL;
    tDcbxAppInfo        DcbxAppInfo;

    MEMSET (&DcbxAppInfo, DCBX_ZERO, sizeof (tDcbxAppInfo));
    MEMSET (&TempDcbxAppEntry, DCBX_ZERO, sizeof (tDcbxAppEntry));

    MEMCPY (&(TempDcbxAppEntry.DcbxAppId),
            &(pDcbxAppPortMsg->DcbxAppId), sizeof (tDcbxAppId));
    /* Get the application entry from the application table */
    pDcbxAppEntry = (tDcbxAppEntry *) RBTreeGet (gDcbxGlobalInfo.pRbDcbxAppTbl,
                                                 (tRBElem *) &
                                                 TempDcbxAppEntry);
    /* If entry is not found or the corresponding port bit is not set then
     * no need to start the state machine sine this application for the port 
     * is not registered */
    if ((pDcbxAppEntry == NULL) ||
        (pDcbxAppEntry->ApplMappedIfPortList[u4IfIndex] == OSIX_FALSE))
    {
        DCBX_TRC (DCBX_SEM_TRC | DCBX_FAILURE_TRC, "DcbxSemUpdate:"
                  "DCBX Port De Registration Failed- Not Registered!!!\r\n");
        return OSIX_FAILURE;
    }
    else
    {
        /* Fill the Information needs to send it to aplication regarding the 
         * State machine update */
        DcbxAppInfo.u4IfIndex = u4IfIndex;
        DcbxAppInfo.u4MsgType = DCBX_OPER_UPDATE;

        if (pDcbxAppPortMsg->u1ApplStateMachineType == DCBX_ASYM_STATE_MACHINE)
        {
            /* Call the Assymetric State Machine */
            DcbxAppInfo.u1ApplOperState = DcbxSemASymStateMachine
                (pDcbxAppPortMsg->u1ApplLocalWilling);
        }
        else
        {
            /* Call the Symetric State Machine */
            DcbxAppInfo.u1ApplOperState = DcbxSemSymStateMachine
                (u4IfIndex, pDcbxAppPortMsg->u1ApplLocalWilling,
                 pDcbxAppPortMsg->u1ApplRemoteWilling,
                 pDcbxAppPortMsg->au1RemDestMacAddr);
        }
        /* call the callback function registered for this application
         * with the state machine update information */
        DCBX_TRC (DCBX_SEM_TRC, "DcbxSemUpdate:"
                  "Update the application for the resulting"
                  "state by calling registered callback function!!!\r\n");
        pDcbxAppEntry->pAppCallBackFn (&DcbxAppInfo);
    }
    return OSIX_SUCCESS;
}

/***************************************************************************
 *  FUNCTION NAME : DcbxSemASymStateMachine
 * 
 *  DESCRIPTION   : This function is used to get the oper state by running
 *                  ASymmetric state machine.
 * 
 *  INPUT         : u1LocalWilling - Willing state of Local port.
 * 
 *  OUTPUT        : u1OperState - Resulting state in the state machine.
 * 
 *  RETURNS       : None.
 * 
 * **************************************************************************/
PRIVATE UINT1
DcbxSemASymStateMachine (UINT1 u1LocalWilling)
{
    /* If the state machine is of Assymetric type then the oper state is 
     * purely based on the willing bit */
    if (u1LocalWilling == DCBX_DISABLED)
    {
        DCBX_TRC (DCBX_SEM_TRC, "DcbxSemASymStateMachine:"
                  "State Machine Type : Assymmetric "
                  "State Machine Result : INIT !!!\r\n");
        return DCBX_OPER_INIT;
    }
    else
    {
        DCBX_TRC (DCBX_SEM_TRC, "DcbxSemASymStateMachine:"
                  "State Machine Type : Assymmetric "
                  "State Machine Result : RECO !!!\r\n");
        return DCBX_OPER_RECO;
    }
}

/***************************************************************************
 *  FUNCTION NAME : DcbxSemSymStateMachine
 * 
 *  DESCRIPTION   : This function is used to get the oper state by running
 *                  Symmetric state machine.
 * 
 *  INPUT         : u4IfIndex - Port Number
 *                  u1LocalWilling,u1RemoteWilling - Willing state of Local
 *                  and Remote.
 *                  RemAddr - Remote Dest Mac Addr to take decision if
 *                  both the willing is enabled.
 * 
 *  OUTPUT        : u1OperState - Resulting state in the state machine.
 * 
 *  RETURNS       : None.
 * 
 * **************************************************************************/
PRIVATE UINT1
DcbxSemSymStateMachine (UINT4 u4IfIndex,
                        UINT1 u1LocalWilling,
                        UINT1 u1RemoteWilling, tMacAddr RemAddr)
{
    INT4                i4Value = DCBX_ZERO;
    UINT1               au1LocMacAddr[MAC_ADDR_LEN];
    UINT1               u1OperState = DCBX_OPER_INIT;

    MEMSET (au1LocMacAddr, DCBX_ZERO, MAC_ADDR_LEN);
    /* If the state machine is of Symmetric type then the 
     * oper state is based on the following logic */
    if (u1LocalWilling == DCBX_DISABLED)
    {
        DCBX_TRC (DCBX_SEM_TRC, "DcbxSemSymStateMachine:"
                  "State Machine Type : Symmetric "
                  "State Machine Result : INIT !!!\r\n");
        u1OperState = DCBX_OPER_INIT;
    }
    else
    {
        if (u1RemoteWilling == DCBX_DISABLED)
        {
            DCBX_TRC (DCBX_SEM_TRC, "DcbxSemSymStateMachine:"
                      "State Machine Type : Symmetric "
                      "State Machine Result : RECO !!!\r\n");
            u1OperState = DCBX_OPER_RECO;
        }
        else
        {
            /*Compare the Mac Address and Decide oper state */
            DcbxPortCfaGetIfMacAddr (u4IfIndex, au1LocMacAddr);
            i4Value = DcbxUtlCompareMacaddr (au1LocMacAddr, RemAddr);

            if (i4Value == DCBX_LESSER)
            {
                DCBX_TRC (DCBX_SEM_TRC, "DcbxSemSymStateMachine:"
                          "State Machine Type : Symmetric "
                          "State Machine Result : INIT !!!\r\n");
                u1OperState = DCBX_OPER_INIT;
            }
            else if (i4Value == DCBX_GREATER)
            {
                DCBX_TRC (DCBX_SEM_TRC, "DcbxSemSymStateMachine:"
                          "State Machine Type : Symmetric "
                          "State Machine Result : RECO !!!\r\n");
                u1OperState = DCBX_OPER_RECO;
            }
            else
            {
                DCBX_TRC (DCBX_SEM_TRC | DCBX_FAILURE_TRC,
                          "DcbxSemSymStateMachine:"
                          "Local and Remote Mac Address Equal "
                          "which is nota correct scenario."
                          "Moving state to INIT "
                          "State Machine Type : Symmetric "
                          "State Machine Result : INIT !!!\r\n");
                u1OperState = DCBX_OPER_INIT;
            }

        }
    }
    return u1OperState;
}
