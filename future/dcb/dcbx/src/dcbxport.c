/*************************************************************************
 * Copyright (C) 2010 Aricent Inc . All Rights Reserved
 * $Id: dcbxport.c,v 1.11 2017/01/25 13:19:42 siva Exp $
 * Description: This file contains DCBX functions which uses other modules
 *              API.
****************************************************************************/
#include "dcbxinc.h"

/***************************************************************************
 *  FUNCTION NAME : DcbxPortGetNextValidPort
 * 
 *  DESCRIPTION   : This function is used to get the next valid port
 *                  that was created.
 * 
 *  INPUT         : u2PrevPort - Previous Port Number.
 *                  pu2PortIndex - Pointer to the current port number. 
 * 
 *  OUTPUT        : None
 * 
 *  RETURNS       : OSIX_SUCCESS/OSIX_FAILURE
 * 
 * **************************************************************************/
PUBLIC INT4
DcbxPortGetNextValidPort (UINT2 u2PrevPort, UINT2 *pu2PortIndex)
{

    if (L2IwfGetNextValidPort (u2PrevPort, pu2PortIndex) != L2IWF_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : DcbxPortNotifyFaults
 *
 *    DESCRIPTION      : This function Sends the trap message to the Fault
 *                       Manager.
 *
 *    INPUT            : tSNMP_VAR_BIND * - pointer the trapmessage.
 *                       pu1Msg  *        - Pointer to the log message.
 *                       u4ModuleId       - ModuleId
 *
 *    OUTPUT           : Calls the FmApiNotifyFaults to send the
 *                       Trap message to FaultManager.
 *
 *    RETURNS          : OSIX_SUCCESS/ OSIX_FAILURE
 *
 ****************************************************************************/
PUBLIC INT4
DcbxPortNotifyFaults (tSNMP_VAR_BIND * pTrapMsg, UINT1 *pu1Msg,
                      UINT4 u4ModuleId)
{
#ifdef FM_WANTED
    tFmFaultMsg         FmFaultMsg;

    MEMSET (&FmFaultMsg, DCBX_ZERO, sizeof (tFmFaultMsg));

    FmFaultMsg.pTrapMsg = pTrapMsg;
    FmFaultMsg.pSyslogMsg = pu1Msg;
    FmFaultMsg.u4ModuleId = u4ModuleId;
    if (FmApiNotifyFaults (&FmFaultMsg) == OSIX_FAILURE)
    {
        DCBX_TRC (DCBX_FAILURE_TRC,
                  "DcbxPortNotifyFaults: Sending "
                  "Notfication to FM Failed \r\n");
        SNMP_free_snmp_vb_list (pTrapMsg);
        return OSIX_FAILURE;
    }
#else
#ifdef SNMP_3_WANTED
    SNMP_free_snmp_vb_list (pTrapMsg);
#else
    UNUSED_PARAM (pTrapMsg);
#endif
    UNUSED_PARAM (pu1Msg);
    UNUSED_PARAM (u4ModuleId);
#endif
    return OSIX_SUCCESS;
}

/***************************************************************************
 *  FUNCTION NAME : DcbxPortL2IwfApplPortRequest
 * 
 *  DESCRIPTION   : This function is used to call the port related
 *                  L2IWF functions to post the DCBX message. 
 * 
 *  INPUT         : u4IfIndex - Port Number.
 *                  pLldpAppPortMsg - Information that needs to posted to
 *                  L2IWF.
 *                  u1MsgType - Reg/DeReg/Update
 * 
 *  OUTPUT        : None
 * 
 *  RETURNS       :OSIX_SUCCESS/OSIX_FAILURE
 * 
 * **************************************************************************/
PUBLIC INT4
DcbxPortL2IwfApplPortRequest (UINT4 u4IfIndex,
                              tLldpAppPortMsg * pLldpAppPortMsg,
                              UINT1 u1MsgType)
{
    /* Send the application port request handle function of L2IWF */
    if (L2IwfLldpHandleApplPortRequest (u4IfIndex, pLldpAppPortMsg,
                                        u1MsgType) != OSIX_SUCCESS)
    {
        DCBX_TRC_ARG6 (DCBX_CRITICAL_TRC, "DcbxPortL2IwfApplPortRequest: "
                "L2IwfLldpHandleApplPortRequest() -"
                "Registration with LLDP failed with Type=%d "
                "subtype=%d and OUI={0x%x|0x%x|0x%x} for port %d!!!\r\n",
                pLldpAppPortMsg->LldpAppId.u2TlvType,
                pLldpAppPortMsg->LldpAppId.u1SubType,
                pLldpAppPortMsg->LldpAppId.au1OUI[0],
                pLldpAppPortMsg->LldpAppId.au1OUI[1],
                pLldpAppPortMsg->LldpAppId.au1OUI[2],
                u4IfIndex);
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;

}

/***************************************************************************
 *  FUNCTION NAME : DcbxPortL2IwfApplRequest
 * 
 *  DESCRIPTION   : This function is used to call the Application related
 *                  L2IWF functions to post the DCBX message. 
 * 
 *  INPUT         : pL2LldpAppInfo - Information that needs to posted to
 *                  L2IWF.
 *                  u1MsgType - Reg/DeReg/Update
 * 
 *  OUTPUT        : None
 * 
 *  RETURNS       :OSIX_SUCCESS/OSIX_FAILURE
 * 
 * **************************************************************************/
PUBLIC INT4
DcbxPortL2IwfApplRequest (tL2LldpAppInfo * pL2LldpAppInfo, UINT1 u1MsgType)
{
    /* Call the respective L2IWF funtion */
    if (L2IwfLldpHandleApplRequest (pL2LldpAppInfo, u1MsgType) == OSIX_FAILURE)
    {
        DCBX_TRC_ARG5 (DCBX_CRITICAL_TRC, "DcbxPortL2IwfApplRequest: "
                "L2IwfLldpHandleApplRequest() -"
                "Registration with LLDP failed with Type=%d "
                "subtype=%d and OUI={0x%x|0x%x|0x%x}!!!\r\n",
                pL2LldpAppInfo->LldpAppId.u2TlvType,
                pL2LldpAppInfo->LldpAppId.u1SubType,
                pL2LldpAppInfo->LldpAppId.au1OUI[0],
                pL2LldpAppInfo->LldpAppId.au1OUI[1],
                pL2LldpAppInfo->LldpAppId.au1OUI[2]
                );
        return (OSIX_FAILURE);
    }
    return OSIX_SUCCESS;
}

/***************************************************************************
 *  FUNCTION NAME : DcbxPortCfaGetIfMacAddr
 * 
 *  DESCRIPTION   : This function is used to get the Mac Address from the 
 *                  CFA module for the given interface.
 * 
 *  INPUT         : u4IfIndex - Interface Index.
 *                  pu1MacAddr - Pointer to Get the Mac Address.
 * 
 *  OUTPUT        : None
 * 
 *  RETURNS       : OSIX_SUCCESS/OSIX_FAILURE
 * 
 * **************************************************************************/
PUBLIC INT4
DcbxPortCfaGetIfMacAddr (UINT4 u4IfIndex, UINT1 *pu1MacAddr)
{
    tCfaIfInfo          CfaIfInfo;
    MEMSET (&CfaIfInfo, DCBX_ZERO, sizeof (tCfaIfInfo));

    /* Call the respective CFA funtion */
    if (CfaGetIfInfo (u4IfIndex, &CfaIfInfo) != CFA_SUCCESS)
    {
        return (OSIX_FAILURE);
    }
    MEMCPY (pu1MacAddr, CfaIfInfo.au1MacAddr, MAC_ADDR_LEN);
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : DcbxPortRmRegisterProtocols                              */
/*                                                                           */
/* Description        : This function calls the RM module to register.       */
/*                                                                           */
/* Input(s)           : tRmRegParams - Reg. params to be provided by         */
/*                      protocols.                                           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : OSIX_SUCCESS/OSIX_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
PUBLIC UINT4
DcbxPortRmRegisterProtocols (tRmRegParams * pRmReg)
{
    UINT4               u4RetVal = OSIX_SUCCESS;

#ifdef L2RED_WANTED
    if (RmRegisterProtocols (pRmReg) == RM_FAILURE)
    {
        u4RetVal = OSIX_FAILURE;
    }
#else
    UNUSED_PARAM (pRmReg);
#endif
    return u4RetVal;
}

/*****************************************************************************/
/* Function Name      : DcbxPortRmDeRegisterProtocols                        */
/*                                                                           */
/* Description        : This function calls the RM module to deregister.     */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : OSIX_SUCCESS/OSIX_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
UINT4
DcbxPortRmDeRegisterProtocols (VOID)
{
    UINT4               u4RetVal = OSIX_SUCCESS;

#ifdef L2RED_WANTED
    if (RmDeRegisterProtocols (RM_DCBX_APP_ID) == RM_FAILURE)
    {
        u4RetVal = OSIX_FAILURE;
    }
#endif
    return u4RetVal;
}

/*****************************************************************************/
/* Function Name      : DcbxPortRmReleaseMemoryForMsg                        */
/*                                                                           */
/* Description        : This function calls the RM module to release the     */
/*                      memory allocated for sending the Standby node count. */
/*                                                                           */
/* Input(s)           : pu1Block - Mmemory block.                            */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : OSIX_SUCCESS/OSIX_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
UINT4
DcbxPortRmReleaseMemoryForMsg (UINT1 *pu1Block)
{
    UINT4               u4RetVal = OSIX_SUCCESS;

#ifdef L2RED_WANTED
    if (RmReleaseMemoryForMsg (pu1Block) == RM_FAILURE)
    {
        u4RetVal = OSIX_FAILURE;
    }

#else
    UNUSED_PARAM (pu1Block);
#endif
    return u4RetVal;
}

/*********************************************************
 * Function Name      : DcbxPortRmSetBulkUpdatesStatus
 *

 * Description        : This function calls the RM Bulk Status
 *                         Update funciotn.
 *
 * Input(s)           : None
 *
 * Output(s)          : None
 *
 * Return Value(s)    : OSIX_SUCCESS/OSIX_FAILURE
 *
 ****************************************************************/
PUBLIC UINT4
DcbxPortRmSetBulkUpdatesStatus (VOID)
{
    UINT4               u4RetVal = OSIX_SUCCESS;
#ifdef L2RED_WANTED
    if (RmSetBulkUpdatesStatus (RM_DCBX_APP_ID) == RM_FAILURE)
    {
        u4RetVal = OSIX_FAILURE;
    }
#endif
    return u4RetVal;
}

/****************************************************************************
 * Function Name      : DcbxPortRmGetStandbyNodeCount
 *
 * Description        : This function calls the RM module to get the number
 *                      of peer nodes that are up.

 *
 * Input(s)           : None
 *
 * Output(s)          : None
 *
 * Return Value(s)    : Number of Booted up standby nodes
 *
 ***************************************************************************/
PUBLIC UINT1
DcbxPortRmGetStandbyNodeCount (VOID)
{
#ifdef L2RED_WANTED
    return (RmGetStandbyNodeCount ());
#else
    return DCBX_ZERO;
#endif
}

/*********************************************************
 * Function Name      : DcbxPortRmGetNodeState
 *
 * Description        : This function calls the RM module
 *                         to get the node state.
 *
 * Input(s)           : None
 *
 * Output(s)          : None
 *
 * Return Value(s)    : RM_ACTIVE/RM_INIT_/RM_STANDBY
 *
 ****************************************************************/
PUBLIC UINT4
DcbxPortRmGetNodeState (VOID)
{
#ifdef L2RED_WANTED
    return (RmGetNodeState ());
#else
    return RM_ACTIVE;
#endif
}

/*********************************************************************
 * Function Name      : DcbxPortRmApiHandleProtocolEvent
 *
 * Description        : This function calls the RM module to intimate about
 *                      the protocol operations
 *
 * Input(s)           : pEvt->u4AppId - Application Id
 *                      pEvt->u4Event - Event send by protocols to RM
 *                                      RM_STANDBY_TO_ACTIVE_EVT_PROCESSED
 *                                      RM_IDLE_TO_ACTIVE_EVT_PROCESSED
 *                                      RM_STANDBY_EVT_PROCESSED)
 *                      pEvt->u4Err   - Error code (RM_MEMALLOC_FAIL
 *                                      RM_SENDTO_FAIL / RM_PROCESS_FAIL)
 *
 * Output(s)          : None
 *
 * Return Value(s)    : OSIX_SUCCESS/OSIX_FAILURE
 *
 *********************************************************************/
PUBLIC UINT4
DcbxPortRmApiHandleProtocolEvent (tRmProtoEvt * pEvt)
{
    UINT4               u4RetVal = OSIX_SUCCESS;
#ifdef L2RED_WANTED
    if (RmApiHandleProtocolEvent (pEvt) == RM_FAILURE)
    {
        u4RetVal = OSIX_FAILURE;
    }
#else
    UNUSED_PARAM (pEvt);
#endif
    return u4RetVal;
}

/***************************************************************************
 * FUNCTION NAME    : DcbxPortRmApiSendProtoAckToRM 
 *
 * DESCRIPTION      : This is the function used by protocols/applications 
 *                    to send acknowledgement to RM after processing the
 *                    sync-up message.
 *
 * INPUT            : tRmProtoAck contains
 *                    u4AppId  - Protocol Identifier
 *                    u4SeqNum - Sequence number of the RM message for
 *                    which this ACK is generated.
 *
 * OUTPUT           : None
 * 
 * RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 * 
 **************************************************************************/
PUBLIC INT4
DcbxPortRmApiSendProtoAckToRM (tRmProtoAck * pProtoAck)
{
    INT4                i4RetVal = OSIX_SUCCESS;
#ifdef L2RED_WANTED
    if (RmApiSendProtoAckToRM (pProtoAck) == RM_FAILURE)
    {
        i4RetVal = OSIX_FAILURE;
    }
#else
    UNUSED_PARAM (pProtoAck);
#endif
    return i4RetVal;
}

/*****************************************************************************/
/* Function Name      : DcbxPortRmEnqMsgToRm                                 */
/*                                                                           */
/* Description        : This function calls the RM Module to enqueue the     */
/*                      message from applications to RM task.                */
/*                                                                           */
/* Input(s)           : pRmMsg - msg from appl.                              */
/*                      u2DataLen - Len of msg.                              */
/*                      u4SrcEntId - EntId from which the msg has arrived.   */
/*                      u4DestEntId - EntId to which the msg has to be sent. */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
PUBLIC UINT4
DcbxPortRmEnqMsgToRm (tRmMsg * pRmMsg, UINT2 u2DataLen,
                      UINT4 u4SrcEntId, UINT4 u4DestEntId)
{
#ifdef L2RED_WANTED
    if (RmEnqMsgToRmFromAppl (pRmMsg, u2DataLen,
                              u4SrcEntId, u4DestEntId) == RM_FAILURE)
    {
        DCBX_TRC (DCBX_RED_TRC | DCBX_FAILURE_TRC,
                  "DcbxPortRmEnqMsgToRm: Enqueue " "message to RM Failed\r\n");
        RM_FREE (pRmMsg);
        return OSIX_FAILURE;
    }
#else
    UNUSED_PARAM (pRmMsg);
    UNUSED_PARAM (u2DataLen);
    UNUSED_PARAM (u4SrcEntId);
    UNUSED_PARAM (u4DestEntId);
#endif
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : DcbxL2IwfIsPortInPortChannel                         */
/*                                                                           */
/* Description        : This function calls the L2IWF module to check        */
/*                      whether the port is in port-channel.                 */
/*                                                                           */
/* Input(s)           : u4IfIndex - Interface Identifier                     */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
INT4
DcbxL2IwfIsPortInPortChannel (UINT4 u4IfIndex)
{
    return (L2IwfIsPortInPortChannel (u4IfIndex));
}
