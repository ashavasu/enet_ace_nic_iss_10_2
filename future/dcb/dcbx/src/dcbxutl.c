/*************************************************************************
 * Copyright (C) 2010 Aricent Inc . All Rights Reserved
 * $Id: dcbxutl.c,v 1.16 2017/01/25 13:19:42 siva Exp $
 * Description: This file contains DCBX utility function.
****************************************************************************/
/****************************************************************************/
/* Copyright (C) 2010 Aricent Inc . All Rights Reserved                     */
/*                                                                          */
/*  FILE NAME             : dcbxutl.c                                       */
/*  PRINCIPAL AUTHOR      : Aricent                                         */
/*  SUBSYSTEM NAME        : DCB                                             */
/*  MODULE NAME           : DCBX-UTL                                        */
/*  LANGUAGE              : C                                               */
/*  TARGET ENVIRONMENT    : Linux                                           */
/*  DATE OF FIRST RELEASE :                                                 */
/*  AUTHOR                : Aricent                                         */
/*  FUNCTIONS DEFINED(Applicable for Source files) :                        */
/*  DESCRIPTION           : This File contains the utility functions        */
/*                          for the Aricent DCBX Module.                    */
/****************************************************************************/
/*                                                                          */
/*  Change History                                                          */
/*  Version               :                                                 */
/*  Date(DD/MM/YYYY)      :                                                 */
/*  Modified by           :                                                 */
/*  Description of change :                                                 */
/****************************************************************************/
#include "dcbxinc.h"

/*****************************************************************************/
/* Function Name      : DcbxUtlDcbPortTblCmpFn                               */
/* Description        : This function is used as a Compare function in the   */
/*                      RBTree.                                              */
/*                      1.It Compares the DCBX Port Table Index              */
/*                                and return the values                      */
/* Input(s)           : e1 , e2 -  Elements of RBTree                        */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    :  -1/0/1                                              */
/* Called By          : RBTreeUtility                                        */
/* Calling Function   :                                                      */
/*****************************************************************************/

INT4
DcbxUtlPortTblCmpFn (tRBElem * pRBElem1, tRBElem * pRBElem2)
{
    tDcbxPortEntry     *pDcbxPortEntry1 = NULL;
    tDcbxPortEntry     *pDcbxPortEntry2 = NULL;

    pDcbxPortEntry1 = (tDcbxPortEntry *) pRBElem1;
    pDcbxPortEntry2 = (tDcbxPortEntry *) pRBElem2;

    /* Compare the DCB Port Index */

    if (pDcbxPortEntry1->u4IfIndex < pDcbxPortEntry2->u4IfIndex)
    {
        return (DCBX_LESSER);
    }
    else if (pDcbxPortEntry1->u4IfIndex > pDcbxPortEntry2->u4IfIndex)
    {
        return (DCBX_GREATER);
    }

    return (DCBX_EQUAL);
}

/*****************************************************************************/
/* Function Name      : DcbxValidatePort                                     */
/* Description        : This function is used to validate the Interface Index*/
/* Input(s)           : u4IfIndex -Interface Index                           */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/* Return Value(s)    : OSIX_SUCCESS or OSIX_FAILURE                       */
/* Called By          : DCBXUtility Function                                 */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
DcbxUtlValidatePort (UINT4 u4IfIndex)
{
    UINT4               u4ContextId = DCBX_INIT_VAL;
    UINT2               u2LocalPort = DCBX_INIT_VAL;
    tDcbxPortEntry     *pDcbxPortEntry = NULL;

    if ((u4IfIndex == DCBX_INIT_VAL) || (u4IfIndex > DCBX_MAX_PORT_LIST_SIZE))
    {
        DCBX_TRC_ARG2 (DCBX_FAILURE_TRC, "In %s :Invalid IfIndex %d.\r\n",
                       __FUNCTION__, u4IfIndex);
        return OSIX_FAILURE;
    }

    /* Currently, DCBX shouldn't be allowed in 
       Port-Channel Interface */
    if (CfaIsLaggInterface (u4IfIndex) == CFA_TRUE)
    {
        DCBX_TRC (DCBX_FAILURE_TRC, "DcbxUtlValidatePort: "
                "DCB configurations are not allowed in Port channel "
                "interface!!!\r\n");
        return OSIX_FAILURE;
    }

    /* If the port is not mapped to any context, then don't
     * allow enabling dcbx feature on port */
    if (VcmGetContextInfoFromIfIndex (u4IfIndex, &u4ContextId,
                                      &u2LocalPort) == VCM_FAILURE)
    {
        DCBX_TRC (DCBX_FAILURE_TRC, "DcbxUtlValidatePort: "
                  "Enabling DCBX feature failed - "
                  "Port not mapped to any context!!!\r\n ");
        return OSIX_FAILURE;
    }

    pDcbxPortEntry = DcbxUtlGetPortEntry (u4IfIndex);

    if (pDcbxPortEntry == NULL)
    {
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : DcbxUtlGetPortEntry                                  */
/* Description        : Returns pointer to port entry                        */
/* Input(s)           : u4IfIndex                                            */
/* Output(s)          : None.                                                */
/* Return Value(s)    : Pointer to  PORT entry                               */
/*****************************************************************************/
tDcbxPortEntry     *
DcbxUtlGetPortEntry (UINT4 u4IfIndex)
{
    tDcbxPortEntry      DcbxPortEntry;
    tDcbxPortEntry     *pDcbxPortEntry = NULL;

    MEMSET (&DcbxPortEntry, DCBX_ZERO, sizeof (tDcbxPortEntry));
    DcbxPortEntry.u4IfIndex = u4IfIndex;

    pDcbxPortEntry = (tDcbxPortEntry *)
        RBTreeGet (gDcbxGlobalInfo.pRbDcbxPortTbl, (tRBElem *) & DcbxPortEntry);
    if (pDcbxPortEntry == NULL)
    {
        DCBX_TRC_ARG1 (DCBX_FAILURE_TRC, "In DcbxUtlGetPortEntry: "
                  "Port Entry Id is Invalid!! for port %d\r\n",
                  u4IfIndex);
    }

    return (pDcbxPortEntry);
}

/***************************************************************************
 *  FUNCTION NAME : DcbxUtlCompareMacaddr
 * 
 *  DESCRIPTION   : This function is used to compare the Mac Address.
 * 
 *  INPUT         : addr1 - First Mac Address to compare
 *                  addr2 - Second Mac Address to Compare
 * 
 *  OUTPUT        : None
 * 
 *  RETURNS       : LESSER/GREATER/EQUAL
 * 
 * **************************************************************************/
INT4
DcbxUtlCompareMacaddr (tMacAddr addr1, tMacAddr addr2)
{

    INT4                i4Val = DCBX_EQUAL;
    /* Compare the two Mac addresses and returns the result */
    if (MEMCMP (addr1, addr2, MAC_ADDR_LEN) < DCBX_ZERO)
    {
        i4Val = DCBX_LESSER;
    }
    else if (MEMCMP (addr1, addr2, MAC_ADDR_LEN) > DCBX_ZERO)
    {
        i4Val = DCBX_GREATER;
    }
    else
    {
        i4Val = DCBX_EQUAL;
    }

    return i4Val;
}

/***************************************************************************
 *  FUNCTION NAME : DcbxUtlPortCreate
 * 
 *  DESCRIPTION   : This utility function is used to create the DCBX Port.
 * 
 *  INPUT         : u4IfIndex - Port Number.
 * 
 *  OUTPUT        : None
 * 
 *  RETURNS       : None
 * 
 * **************************************************************************/
PUBLIC INT4
DcbxUtlPortCreate (UINT4 u4IfIndex)
{

    tDcbxPortEntry      TempDcbxPortEntry;
    tDcbxPortEntry     *pDcbxPortEntry = NULL;
    UINT4               u4RetValue = 0;

    UNUSED_PARAM (u4RetValue);

    MEMSET (&TempDcbxPortEntry, DCBX_ZERO, sizeof (tDcbxPortEntry));

    /* Avoiding the DCBX protocol to be enabled in 
       Port-Channel Interface */
    if (CfaIsLaggInterface (u4IfIndex) == CFA_TRUE)
    {
        DCBX_TRC (DCBX_CONTROL_PLANE_TRC, "DcbxUtlPortCreate:"
                "DCBX port is not created for Port-channel"
                " interface!!!\r\n"); 
        return OSIX_SUCCESS;
    }

    TempDcbxPortEntry.u4IfIndex = u4IfIndex;

    pDcbxPortEntry =
        (tDcbxPortEntry *) RBTreeGet (gDcbxGlobalInfo.pRbDcbxPortTbl,
                                      (tRBElem *) & TempDcbxPortEntry);
    /* Get the port entry and if it is not present then create the port entry.
     * else retutn */
    if (pDcbxPortEntry == NULL)
    {
        /* Allocates memory for the port entry from the memory pool */
        if (((pDcbxPortEntry = (tDcbxPortEntry *)
              MemAllocMemBlk (gDcbxGlobalInfo.DcbxPortPoolId)) == NULL))
        {
            DCBX_TRC (DCBX_FAILURE_TRC, "DcbxUtlPortCreate:"
                      "DCBX port table Memory Allocation Failed!!!\r\n");
            return OSIX_FAILURE;
        }

        MEMSET (pDcbxPortEntry, DCBX_ZERO, sizeof (tDcbxPortEntry));
        pDcbxPortEntry->u4IfIndex = u4IfIndex;
        pDcbxPortEntry->u1DcbxAdminStatus = DCBX_ENABLED;
        pDcbxPortEntry->u1DcbxMode = DCBX_DEFAULT_MODE; 
        pDcbxPortEntry->u1ETSAdminStatus  = DCBX_DISABLED;
        pDcbxPortEntry->u1PFCAdminStatus = DCBX_DISABLED;
        pDcbxPortEntry->u1AppPriAdminStatus = DCBX_DISABLED;
        pDcbxPortEntry->u1OperVersion = DCBX_DEFAULT_VER;
        pDcbxPortEntry->u1MaxVersion = DCBX_VER_IEEE; 
        pDcbxPortEntry->u1PeerOperVersion = DCBX_VER_UNKNOWN;
        pDcbxPortEntry->u1PeerMaxVersion = DCBX_VER_UNKNOWN;
        pDcbxPortEntry->u1LldpTxStatus = DCBX_ENABLED;
        pDcbxPortEntry->u1LldpRxStatus = DCBX_ENABLED;
        /* Add the port entry to the port RB Tree */
        u4RetValue =
            RBTreeAdd (gDcbxGlobalInfo.pRbDcbxPortTbl,
                       (tRBElem *) pDcbxPortEntry);

        DCBX_TRC_ARG1 (DCBX_CONTROL_PLANE_TRC, "DcbxUtlPortCreate:"
                  "DCBX port entry created for Port %d",
                  u4IfIndex); 
    }
    else
    {
        DCBX_TRC (DCBX_CONTROL_PLANE_TRC, "DcbxUtlPortCreate: "
                  "DCBX Port table Entry already present!!!\r\n");
    }
    return OSIX_SUCCESS;
}

/***************************************************************************
 *  FUNCTION NAME : DcbxUtlPortDelete
 * 
 *  DESCRIPTION   : This utility function is used to delete DCBX port.
 * 
 *  INPUT         : u4IfIndex - Port Number.
 * 
 *  OUTPUT        : None
 * 
 *  RETURNS       : None
 * 
 * **************************************************************************/
PUBLIC VOID
DcbxUtlPortDelete (UINT4 u4IfIndex)
{
    tDcbxPortEntry      TempDcbxPortEntry;
    tDcbxPortEntry     *pDcbxPortEntry = NULL;

    MEMSET (&TempDcbxPortEntry, DCBX_ZERO, sizeof (tDcbxPortEntry));

    TempDcbxPortEntry.u4IfIndex = u4IfIndex;

    pDcbxPortEntry =
        (tDcbxPortEntry *) RBTreeGet (gDcbxGlobalInfo.pRbDcbxPortTbl,
                                      (tRBElem *) & TempDcbxPortEntry);
    /* Get the entry for the port table and remove the node from the Port RBTree
     * and releases the memory to the memory pool */
    if (pDcbxPortEntry != NULL)
    {
        /* Delete the ETS port so that it can De-Registers the 
         * application with DCBX and in turn with LLDP and
         * deltes the ETS port related infomration */
        ETSUtlDeletePort (u4IfIndex);
        /* Delete the PFC port so that it can De-Registers the 
         * application with DCBX and in turn with LLDP and
         * deltes the PFC port related infomration */
        PFCUtlDeletePort (u4IfIndex);
        /* Delete the AP port so that it can De-Registers the 
         * application with DCBX and in turn with LLDP and
         * deltes the AP port related infomration */
        AppPriUtlDeletePort (u4IfIndex);

        RBTreeRemove (gDcbxGlobalInfo.pRbDcbxPortTbl, (UINT1 *) pDcbxPortEntry);

        MemReleaseMemBlock (gDcbxGlobalInfo.DcbxPortPoolId,
                            (UINT1 *) pDcbxPortEntry);
        DCBX_TRC_ARG1 (DCBX_CONTROL_PLANE_TRC, "DcbxUtlPortCreate:"
                  "DCBX port entry deleted for Port %d",
                  u4IfIndex); 

    }
    else
    {
        DCBX_TRC (DCBX_CONTROL_PLANE_TRC, "DcbxUtlPortDelete: "
                "DCBX Port table Entry not present!!!\r\n");
    }
    return;
}

/****************************************************************************
*
*    FUNCTION NAME    : DcbxUtlAddPortToLAG
*
*    DESCRIPTION      : This function update DCBX database when a port added to 
*                       a port channel.
*
*    INPUT            : u4IfIndex - Port number
*                       u4PoIndex - Port Channel Index
*
*    OUTPUT           : None.
*
*    RETURNS          : None
*
****************************************************************************/
PUBLIC VOID
DcbxUtlAddPortToLAG (UINT4 u4PoIndex)
{
    tDcbxPortEntry     *pDcbxPortEntry = NULL;

    pDcbxPortEntry = DcbxUtlGetPortEntry (u4PoIndex);

    if (pDcbxPortEntry == NULL)
    {
        DCBX_TRC (DCBX_FAILURE_TRC, "DcbxUtlAddPortToLAG:"
                  "No Port Channel entry is created in DCBX !!!\r\n");
        return;

    }

    /* While dding the member port in the port Channel interface
     * Delete the port from the port database */

    /* Since new port is added to the Port Channel interface, need to 
     * update the LLDP with the TLV information for all the application
     * registered with DCBX for this port channel.So that LLDP
     * will transmit the TLV in the newly added port. So Sending Re-Reg
     * request to applications for Port Channel. */

    DcbxUtlPortAdminEnable (u4PoIndex);

    return;
}

/****************************************************************************
*
*    FUNCTION NAME    : DcbxUtlDelPortFromLAG
*
*    DESCRIPTION      : This function update DCBX database when a port removed 
*                       from a port channel.
*
*    INPUT            : u4IfIndex - Port number
*                       u4PoIndex - Port Channel Index
*
*    OUTPUT           : None.
*
*    RETURNS          : None
*
****************************************************************************/
PUBLIC VOID
DcbxUtlDelPortFromLAG (UINT4 u4IfIndex, UINT4 u4PoIndex)
{
    static tDcbxAppEntry TempDcbxAppEntry;
    /* This contains port list array which results in stack size
     * exceeds 500. So declaring this varaible as static */
    tDcbxAppEntry      *pDcbxAppEntry = NULL;
    tDcbxPortEntry     *pDcbxPortEntry = NULL;
    tLldpAppPortMsg     LldpAppPortMsg;

    pDcbxPortEntry = DcbxUtlGetPortEntry (u4PoIndex);

    if (pDcbxPortEntry == NULL)
    {
        DCBX_TRC (DCBX_FAILURE_TRC, "DcbxUtlDelPortFromLAG:"
                  "No Port Channel entry is created in DCBX !!!\r\n");
        return;

    }

    /* Since new port is added to the Port Channel interface, need to 
     * update the LLDP with the TLV information for all the application
     * registered with DCBX for this port channel.So that LLDP
     * will transmit the TLV in the newly added port. So Sending Re-Reg
     * request to applications for Port Channel. */

    MEMSET (&TempDcbxAppEntry, DCBX_ZERO, sizeof (tDcbxAppEntry));
    MEMSET (&LldpAppPortMsg, DCBX_ZERO, sizeof (tLldpAppPortMsg));

    /* During Port removal from LA, get all the application registered 
     * with the DCBX and scan for the port(port Channel)registration bit.
     * If this bit is set then send the De-Registration message
     * to LLDP for the removed port. So that LLDP PDU for the removed
     * port will be cleared on that port.*/
    pDcbxAppEntry = (tDcbxAppEntry *) RBTreeGetFirst
        (gDcbxGlobalInfo.pRbDcbxAppTbl);
    if (pDcbxAppEntry != NULL)
    {
        do
        {
            if (pDcbxAppEntry->ApplMappedIfPortList[u4PoIndex] == OSIX_TRUE)
            {
                if (MEMCMP (&(pDcbxAppEntry->DcbxAppId.au1OUI),gau1CEEOUI, 
                            DCBX_MAX_OUI_LEN) == 0)
                {
                    CEEUtlClearCeeCtrlParams (u4IfIndex);
                }
                MEMCPY (&(LldpAppPortMsg.LldpAppId),
                        &(pDcbxAppEntry->DcbxAppId), sizeof (tDcbxAppId));
                LldpAppPortMsg.u2TxAppTlvLen = DCBX_ZERO;
                LldpAppPortMsg.u4LldpInstSelector = DCBX_ZERO;
                LldpAppPortMsg.pAppCallBackFn = DcbxApiApplCallbkFunc;
                LldpAppPortMsg.bAppTlvTxStatus = OSIX_TRUE;
                LldpAppPortMsg.u1AppTlvRxStatus = OSIX_TRUE;
                LldpAppPortMsg.pu1TxAppTlv = NULL;

                if (DcbxPortL2IwfApplPortRequest
                    (u4IfIndex, &LldpAppPortMsg,
                     L2IWF_LLDP_APPL_PORT_DEREGISTER) != OSIX_SUCCESS)
                {
                    DCBX_TRC (DCBX_FAILURE_TRC, "DcbxUtlDelPortFromLAG:"
                              "DCBX Port DeRegistration with"
                              "LLDP Failed !!!\r\n");
                }
            }

            MEMCPY (&(TempDcbxAppEntry.DcbxAppId), &(pDcbxAppEntry->DcbxAppId),
                    sizeof (tDcbxAppId));
            pDcbxAppEntry = (tDcbxAppEntry *) RBTreeGetNext
                (gDcbxGlobalInfo.pRbDcbxAppTbl,
                 &TempDcbxAppEntry, DcbxUtlApplCmpFun);
        }
        while (pDcbxAppEntry != NULL);
    }
    return;
}

/***************************************************************************
 *  FUNCTION NAME : DcbxUtlDeletePorts
 * 
 *  DESCRIPTION   : This utility function is used to delete all 
 *                  the DCBX ports in the time of shutodwn.
 * 
 *  INPUT         : None
 * 
 *  OUTPUT        : None
 * 
 *  RETURNS       : None
 * 
 * **************************************************************************/
PUBLIC VOID
DcbxUtlDeletePorts (VOID)
{
    tDcbxPortEntry      TempDcbxPortEntry;
    tDcbxPortEntry     *pDcbxPortEntry = NULL;

    MEMSET (&TempDcbxPortEntry, DCBX_ZERO, sizeof (tDcbxPortEntry));

    /* Get the First port entry and delete the port entry if present */

    pDcbxPortEntry = (tDcbxPortEntry *) RBTreeGetFirst
        (gDcbxGlobalInfo.pRbDcbxPortTbl);
    if (pDcbxPortEntry != NULL)
    {
        do
        {
            /* call the delete port function which can take care of deleting the
             * ETS and PFC ports */
            TempDcbxPortEntry.u4IfIndex = pDcbxPortEntry->u4IfIndex;
            DcbxUtlPortDelete (pDcbxPortEntry->u4IfIndex);
            pDcbxPortEntry = (tDcbxPortEntry *) RBTreeGetNext
                (gDcbxGlobalInfo.pRbDcbxPortTbl, &TempDcbxPortEntry,
                 DcbxUtlPortTblCmpFn);
            /* Process for all the ports in the port RB Tree */
        }
        while (pDcbxPortEntry != NULL);
    }
    return;
}

/***************************************************************************
 *  FUNCTION NAME : DcbxUtlPortAdminDisable
 * 
 *  DESCRIPTION   : This utility function is used to Disable the DCBX
 *                  Admin status.
 * 
 *  INPUT         : u4IfIndex - Port Number.
 * 
 *  OUTPUT        : None
 * 
 *  RETURNS       : None
 * 
 * **************************************************************************/
PUBLIC VOID
DcbxUtlPortAdminDisable (UINT4 u4IfIndex)
{
    static tDcbxAppEntry TempDcbxAppEntry;
    /* This contains port list array which results in stack size
     * exceeds 500. So declaring this varaible as static */
    tLldpAppPortMsg     LldpAppPortMsg;
    tDcbxAppEntry      *pDcbxAppEntry = NULL;
    tDcbxAppInfo        DcbxAppInfo;

    MEMSET (&DcbxAppInfo, DCBX_ZERO, sizeof (tDcbxAppInfo));
    MEMSET (&TempDcbxAppEntry, DCBX_ZERO, sizeof (tDcbxAppEntry));
    MEMSET (&LldpAppPortMsg, DCBX_ZERO, sizeof (tLldpAppPortMsg));

    /* During Admin Disable get all the application registered 
     * with the DCBX and scan for the ports registration bit.
     * If this bit is set then send the De-Registration message
     * to LLDP and send the Age out message to the Application through
     * application call bcak funtion */
    pDcbxAppEntry = (tDcbxAppEntry *) RBTreeGetFirst
        (gDcbxGlobalInfo.pRbDcbxAppTbl);
    if (pDcbxAppEntry != NULL)
    {
        do
        {
            if (pDcbxAppEntry->ApplMappedIfPortList[u4IfIndex] == OSIX_TRUE)
            {

                /*Send AGE-OUT message to clear 
                 * the remote param and Do state change */
                DcbxAppInfo.u4IfIndex = u4IfIndex;
                DcbxAppInfo.u4MsgType = L2IWF_LLDP_APPL_TLV_AGED;
                pDcbxAppEntry->pAppCallBackFn (&DcbxAppInfo);
                if (MEMCMP (&(pDcbxAppEntry->DcbxAppId.au1OUI),gau1CEEOUI, 
                            DCBX_MAX_OUI_LEN) == 0)
                {
                    CEEUtlClearCeeCtrlParams (u4IfIndex);
                }
                MEMCPY (&(LldpAppPortMsg.LldpAppId),
                        &(pDcbxAppEntry->DcbxAppId), sizeof (tDcbxAppId));
                LldpAppPortMsg.u2TxAppTlvLen = DCBX_ZERO;
                LldpAppPortMsg.u4LldpInstSelector = DCBX_ZERO;
                LldpAppPortMsg.pAppCallBackFn = DcbxApiApplCallbkFunc;
                LldpAppPortMsg.bAppTlvTxStatus = OSIX_TRUE;
                LldpAppPortMsg.u1AppTlvRxStatus = OSIX_TRUE;
                LldpAppPortMsg.pu1TxAppTlv = NULL;

                if (DcbxPortL2IwfApplPortRequest
                        (u4IfIndex, &LldpAppPortMsg,
                         L2IWF_LLDP_APPL_PORT_DEREGISTER) != OSIX_SUCCESS)
                {
                    DCBX_TRC (DCBX_FAILURE_TRC, "DcbxUtlPortAdminDisable:"
                            "DCBX Port DeRegistration with LLDP "
                            "Failed !!!\r\n");
                }
            }

            MEMCPY (&(TempDcbxAppEntry.DcbxAppId), &(pDcbxAppEntry->DcbxAppId),
                    sizeof (tDcbxAppId));
            pDcbxAppEntry = (tDcbxAppEntry *) RBTreeGetNext
                (gDcbxGlobalInfo.pRbDcbxAppTbl,
                 &TempDcbxAppEntry, DcbxUtlApplCmpFun);
        }
        while (pDcbxAppEntry != NULL);
    }
    else
    {
        DCBX_TRC (DCBX_CONTROL_PLANE_TRC, "DcbxUtlPortAdminDisable:"
                  " No Application is registerd!!!\r\n");
    }
    return;
}

/***************************************************************************
 *  FUNCTION NAME : DcbxUtlPortAdminEnable
 * 
 *  DESCRIPTION   : This utility function is used to enable the DCBX
 *                  Admin status.
 * 
 *  INPUT         : u4IfIndex - Port Number
 * 
 *  OUTPUT        : None
 * 
 *  RETURNS       : None
 * 
 * **************************************************************************/
PUBLIC VOID
DcbxUtlPortAdminEnable (UINT4 u4IfIndex)
{
    static tDcbxAppEntry TempDcbxAppEntry;
    /* This contains port list array which results in stack size
     * exceeds 500. So declaring this varaible as static */
    tDcbxAppEntry      *pDcbxAppEntry = NULL;
    tDcbxAppInfo        DcbxAppInfo;

    MEMSET (&DcbxAppInfo, DCBX_ZERO, sizeof (tDcbxAppInfo));
    MEMSET (&TempDcbxAppEntry, DCBX_ZERO, sizeof (tDcbxAppEntry));

    /* Scan all the application and if this port is registered for 
     * this application send the Re-Registration request for this port 
     * to the application through the application callback 
     * function */
    pDcbxAppEntry = (tDcbxAppEntry *) RBTreeGetFirst
        (gDcbxGlobalInfo.pRbDcbxAppTbl);

    if (pDcbxAppEntry != NULL)
    {

        do
        {
            if (pDcbxAppEntry->ApplMappedIfPortList[u4IfIndex] == OSIX_TRUE)
            {
                DcbxAppInfo.u4IfIndex = u4IfIndex;
                DcbxAppInfo.u4MsgType = L2IWF_LLDP_APPL_RE_REG;
                pDcbxAppEntry->pAppCallBackFn (&DcbxAppInfo);
            }

            MEMCPY (&(TempDcbxAppEntry.DcbxAppId), &(pDcbxAppEntry->DcbxAppId),
                    sizeof (tDcbxAppId));
            pDcbxAppEntry = (tDcbxAppEntry *) RBTreeGetNext
                (gDcbxGlobalInfo.pRbDcbxAppTbl,
                 &TempDcbxAppEntry, DcbxUtlApplCmpFun);
        }
        while (pDcbxAppEntry != NULL);
    }
    else
    {
        DCBX_TRC (DCBX_CONTROL_PLANE_TRC, "DcbxUtlPortAdminEnable:"
                  " No Application is registered!!!\r\n");

    }
    return;
}

/***************************************************************************
 *  FUNCTION NAME : DcbxUtlCreateAllPorts
 * 
 *  DESCRIPTION   : This utility function is used by create all ports.
 * 
 *  INPUT         : None
 * 
 *  OUTPUT        : None
 * 
 *  RETURNS       : None
 * 
 * **************************************************************************/
PUBLIC VOID
DcbxUtlCreateAllPorts (VOID)
{
    UINT2               u2PortIndex = DCBX_ZERO;
    UINT2               u2PrevPort = DCBX_ZERO;

    while (DcbxPortGetNextValidPort (u2PrevPort, &u2PortIndex) == OSIX_SUCCESS)
    {
        DcbxUtlPortCreate ((UINT4) u2PortIndex);
        u2PrevPort = u2PortIndex;
        u2PortIndex = DCBX_ZERO;
    }
    return;
}

/***************************************************************************
 *  FUNCTION NAME : DcbxUtlApplCmpFun
 * 
 *  DESCRIPTION   : This utility function is used by RbTree for Appl table
 *                  creation.
 * 
 *  INPUT         : pElem1,pElem2 - RbTree Elements.
 * 
 *  OUTPUT        : None
 * 
 *  RETURNS       : LESSER/GREATER/EQUAL.
 * 
 * **************************************************************************/
PUBLIC INT4
DcbxUtlApplCmpFun (tRBElem * pElem1, tRBElem * pElem2)
{
    tDcbxAppEntry      *pRemEntry1 = NULL;
    tDcbxAppEntry      *pRemEntry2 = NULL;
    UINT1               au1TempOUI[DCBX_MAX_OUI_LEN] = { DCBX_ZERO };
    INT4                i4RetVal = DCBX_ZERO;

    pRemEntry1 = (tDcbxAppEntry *) pElem1;
    pRemEntry2 = (tDcbxAppEntry *) pElem2;

    /* Compare the First key - TLV Type  */
    if (pRemEntry1->DcbxAppId.u2TlvType != DCBX_ZERO)
    {
        if (pRemEntry1->DcbxAppId.u2TlvType > pRemEntry2->DcbxAppId.u2TlvType)
        {
            return DCBX_GREATER;
        }
        else if (pRemEntry1->DcbxAppId.u2TlvType <
                 pRemEntry2->DcbxAppId.u2TlvType)
        {
            return DCBX_LESSER;
        }
        /* Continue to other key if this key is equal */
    }

    MEMSET (au1TempOUI, DCBX_ZERO, DCBX_MAX_OUI_LEN);
    /* Compare the Second key - OUI  */
    if (MEMCMP (pRemEntry1->DcbxAppId.au1OUI, au1TempOUI,
                DCBX_MAX_OUI_LEN) != DCBX_ZERO)
    {
        i4RetVal = MEMCMP (pRemEntry1->DcbxAppId.au1OUI,
                           pRemEntry2->DcbxAppId.au1OUI, DCBX_MAX_OUI_LEN);
        if (i4RetVal > DCBX_ZERO)
        {
            return DCBX_GREATER;
        }
        else if (i4RetVal < DCBX_ZERO)
        {
            return DCBX_LESSER;
        }
        /* Continue to other key if this key is equal */
    }
    /* Compare the third key - TLV Sub Type */
    if (pRemEntry1->DcbxAppId.u1SubType != DCBX_ZERO)
    {
        if (pRemEntry1->DcbxAppId.u1SubType > pRemEntry2->DcbxAppId.u1SubType)
        {
            return DCBX_GREATER;
        }
        else if (pRemEntry1->DcbxAppId.u1SubType <
                 pRemEntry2->DcbxAppId.u1SubType)
        {
            return DCBX_LESSER;
        }
    }
    return DCBX_EQUAL;
}

/***************************************************************************
 *  FUNCTION NAME : DcbxUtlFillLldpInfo
 * 
 *  DESCRIPTION   : This function is used to fill the LLDP strucutre 
 *                  information that needs to be send it to L2IWF.
 * 
 *  INPUT         : pDcbxAppPortMsg - Information that was received by DCBX
 *                  by other modules.
 *                  pLldpAppPortMsg - Information that needs to posted to
 *                  L2IWF
 * 
 *  OUTPUT        : None
 * 
 *  RETURNS       : None
 * 
 * **************************************************************************/
PUBLIC VOID
DcbxUtlFillLldpInfo (tDcbxAppRegInfo * pDcbxAppPortMsg,
                     tLldpAppPortMsg * pLldpAppPortMsg)
{
    /* Fill the information required to send it to LLDP */
    MEMCPY (&(pLldpAppPortMsg->LldpAppId),
            &(pDcbxAppPortMsg->DcbxAppId), sizeof (tDcbxAppId));
    pLldpAppPortMsg->u2TxAppTlvLen = pDcbxAppPortMsg->u2TlvLen;
    /* TLV lengh */
    pLldpAppPortMsg->u4LldpInstSelector = DCBX_ZERO;
    pLldpAppPortMsg->pAppCallBackFn = DcbxApiApplCallbkFunc;
    /* Call back funtion registerd with LLDP */
    pLldpAppPortMsg->bAppTlvTxStatus = OSIX_TRUE;
    pLldpAppPortMsg->u1AppTlvRxStatus = OSIX_TRUE;
    /* TLV information */
    pLldpAppPortMsg->pu1TxAppTlv = pDcbxAppPortMsg->pu1ApplTlv;
    return;
}

/***************************************************************************
 *  FUNCTION NAME : DcbxUtlFillL2LldpInfo
 * 
 *  DESCRIPTION   : This function is used to fill the LLDP strucutre 
 *                  information that needs to be send it to L2IWF for
 *                  application request.
 * 
 *  INPUT         : pDcbxAppPortMsg  - Information that was received by DCBX
 *                  by other modules.
 *                  pL2LldpAppInfo  - Information that needs to posted to
 *                  L2IWF
 * 
 *  OUTPUT        : None
 * 
 *  RETURNS       : None
 * 
 * **************************************************************************/
PUBLIC VOID
DcbxUtlFillL2LldpInfo (tDcbxAppRegInfo * pDcbxAppPortMsg,
                       tL2LldpAppInfo * pL2LldpAppInfo)
{
    /* Fill the information required to send it to L2IWF */
    MEMCPY (&(pL2LldpAppInfo->LldpAppId),
            &(pDcbxAppPortMsg->DcbxAppId), sizeof (tDcbxAppId));
    pL2LldpAppInfo->pAppCallBackFn = DcbxApiApplCallbkFunc;
    return;
}

/********************************************************************
 * Function    :  DcbxUtlGetDCBXMode 
 *
 * Description :  Returns the DCBx Mode for the interface Index.
 *
 * Input       :  u4Index     - Interface Index. 
 *
 * Output      :  pu1DcbxMode - Dcbx Mode                
 *
 * Returns     :  None 
*********************************************************************/
VOID 
DcbxUtlGetDCBXMode (UINT4 u4IfIndex, UINT1 *pu1DcbxMode)
{
    tDcbxPortEntry     *pDcbxPortEntry = NULL;

    pDcbxPortEntry = DcbxUtlGetPortEntry (u4IfIndex);
    if (pDcbxPortEntry == NULL)
    {
        DCBX_TRC_ARG1 (DCBX_FAILURE_TRC, "In %s :DcbxUtlGetPortEntry() "
                "Returns FAILURE. \r\n", __FUNCTION__);
        return;
    }

    *pu1DcbxMode = pDcbxPortEntry->u1DcbxMode;
}

/****************************************************************************
 * Function    :  DcbxUtlGetOperVersion
 *
 * Description :  Returns the DCBx Version for the Interface Index.
 *
 * Input       :  u4Index - Interface Index. 
 *
 * Output      :  pu1DcbxVersion - Dcbx Version
 *
 * Returns     :  None 
 ***************************************************************************/
VOID DcbxUtlGetOperVersion (UINT4 u4IfIndex, UINT1 *pu1DcbxVersion)
{
    tDcbxPortEntry   *pDcbxPortEntry = NULL;
    pDcbxPortEntry = DcbxUtlGetPortEntry (u4IfIndex);

    if (pDcbxPortEntry == NULL)
    {
        DCBX_TRC_ARG1 (DCBX_FAILURE_TRC, "DcbxUtlGetOperVersion: "
                "DCBX port entry for port %d not present.!!!\r\n", u4IfIndex);
        return;
    }

    *pu1DcbxVersion = pDcbxPortEntry->u1OperVersion;
}

 /* CEE : Version Resolution Starts */
/***************************************************************************
 *  FUNCTION NAME : DcbxUtlHandleVerResolution
 *
 *  DESCRIPTION   : This function will be called to resolve the version
 *                  whether to use IEEE or CEE based on the received TLV and
 *                  based on Oper and Max version of local node.
 *
 *  INPUT         : pMsg - Message Buffer
 *
 *  OUTPUT        : None
 *
 *  RETURNS       : DCBX_SUCCESS/DCBX_FAILURE
 ***************************************************************************/
PUBLIC INT4
DcbxUtlHandleVerResolution (tDcbxQueueMsg * pMsg)
{
    UINT1                   u1PeerVersion   = DCBX_VER_UNKNOWN;
    UINT1                   u1OperVersion   = DCBX_VER_UNKNOWN;
    UINT1                   u1MaxVersion    = DCBX_VER_UNKNOWN;
    UINT1                   *pu1Buf         = NULL;
    INT4                    i4RetVal        = DCBX_FAILURE;
    tDcbxPortEntry          *pDcbxPortEntry = NULL;

    pDcbxPortEntry = DcbxUtlGetPortEntry (pMsg->u4IfIndex);

    if (NULL == pDcbxPortEntry)
    {
        return DCBX_FAILURE; /* Return failure from here as port entry is not present / NULL */
    }

    /* Get the received Message OUI to distinguish if IEEE or CEE */
    if (MEMCMP(pMsg->DcbxAppId.au1OUI, gau1CEEOUI, DCBX_MAX_OUI_LEN) == 0)
    {
        u1PeerVersion = DCBX_VER_CEE;
    }
    else if (MEMCMP(pMsg->DcbxAppId.au1OUI, gau1DcbxOUI, DCBX_MAX_OUI_LEN)== 0)
    {
        u1PeerVersion = DCBX_VER_IEEE;
    }

    if (L2IWF_LLDP_APPL_TLV_AGED == pMsg->u4MsgType)
    {
        if(u1PeerVersion != pDcbxPortEntry->u1OperVersion)
        {
            DCBX_TRC (DCBX_FAILURE_TRC, "DcbxUtlHandleVerResolution: "
                    "In Auto Mode, ageout TLV of unregistered version"
                    " should not be handled here!!!\r\n");
            return DCBX_FAILURE;
        }
        pDcbxPortEntry->u1PeerOperVersion = DCBX_VER_UNKNOWN;
        pDcbxPortEntry->u1PeerMaxVersion = DCBX_VER_UNKNOWN;
        return DCBX_SUCCESS;
    }
    if (L2IWF_LLDP_MULTIPLE_PEER_NOTIFY == pMsg->u4MsgType)
    {
        pDcbxPortEntry->u1PeerOperVersion = DCBX_VER_UNKNOWN;
        pDcbxPortEntry->u1PeerMaxVersion = DCBX_VER_UNKNOWN;
        return DCBX_SUCCESS;
    }
    
    pDcbxPortEntry->u1PeerOperVersion = DCBX_VER_UNKNOWN;
    pDcbxPortEntry->u1PeerMaxVersion = DCBX_VER_UNKNOWN;
    switch (u1PeerVersion)
    {
        case DCBX_VER_CEE:
            {
                DCBX_TRC (DCBX_CONTROL_PLANE_TRC, "DcbxUtlHandleVerResolution:"
                        "CEE TLV Received, version resolution starts!!!\r\n");

                /* Validate CEE packet */
                i4RetVal = CEEUtlValidateAndDropTLV (pMsg->u4IfIndex,
                        &pMsg->unMsgParam.ApplTlvParam);
                if (i4RetVal == DCBX_FAILURE)
                {
                    DCBX_TRC (DCBX_FAILURE_TRC, "DcbxUtlHandleVerResolution:"
                            "CEE Pkt validation Fails!!!\r\n");

                    /* Sync to STBY */
                    DCBX_TRC_ARG3 (DCBX_CONTROL_PLANE_TRC | DCBX_FAILURE_TRC, 
                            "\nu4IfIndex = %d pMsg->u4MsgType = %d Length = %d\n",
                            pMsg->u4IfIndex, pMsg->u4MsgType,
                            pMsg->unMsgParam.ApplTlvParam.u2RxTlvLen);

                    DcbxCEERedSendDynamicSyncInfo (pMsg);

                    return DCBX_FAILURE;
                }
                pu1Buf = pMsg->unMsgParam.ApplTlvParam.au1DcbxTlv;

                /*Move till operversion (6 + 2) bytes*/
                pu1Buf = pu1Buf + CEE_TLV_HDR_LEN + CEE_TLV_TYPE_LEN;

                DCBX_GET_1BYTE (pu1Buf, u1OperVersion);
                DCBX_GET_1BYTE (pu1Buf, u1MaxVersion);
                if (i4RetVal == DCBX_IGNORE)
                {
                    DCBX_TRC (DCBX_FAILURE_TRC, "DcbxUtlHandleVerResolution:"
                            "CEE Refresh Pkt - Dropping the pkt!!!\r\n");
                    /* Updating the Peer Oper and Max verison based on which is received*/
                    pDcbxPortEntry->u1PeerOperVersion = u1OperVersion;
                    pDcbxPortEntry->u1PeerMaxVersion = u1MaxVersion;
                    /* Any unsupported version to be marked as unknown */
                    if (pDcbxPortEntry->u1PeerMaxVersion > DCBX_VER_UNKNOWN)
                    {
                        pDcbxPortEntry->u1PeerMaxVersion = DCBX_VER_UNKNOWN;
                    }
                    return DCBX_FAILURE;
                }

                /* Sync to STBY */
                DCBX_TRC (DCBX_CONTROL_PLANE_TRC, "DcbxUtlHandleVerResolution:"
                            "Sync to STBY!!!\r\n");

                DCBX_TRC_ARG3 (DCBX_CONTROL_PLANE_TRC, 
                        "\nu4IfIndex = %d pMsg->u4MsgType = %d Length = %d\n", 
                        pMsg->u4IfIndex, pMsg->u4MsgType,
                        pMsg->unMsgParam.ApplTlvParam.u2RxTlvLen);

                DcbxCEERedSendDynamicSyncInfo (pMsg);

                if ((DCBX_MODE_AUTO == pDcbxPortEntry->u1DcbxMode) && \
                        (DCBX_VER_IEEE == pDcbxPortEntry->u1OperVersion))
                {
                    if ((DCBX_VER_CEE == u1OperVersion) && (DCBX_VER_IEEE == u1MaxVersion))
                    {
                        DCBX_TRC (DCBX_FAILURE_TRC, "DcbxUtlHandleVerResolution:"
                                "No Update::Remote to switch to IEEE version So "
                                "Local to continue operate in IEEE version!!!\r\n");
                        return DCBX_FAILURE;
                    }
                    else if ((DCBX_VER_CEE == u1OperVersion) && \
                            (DCBX_VER_CEE == u1MaxVersion || DCBX_VER_UNKNOWN == u1MaxVersion))
                    {
                        DCBX_TRC (DCBX_CONTROL_PLANE_TRC, "DcbxUtlHandleVerResolution:"
                                "SwitchMode::Local to switch to CEE version as "
                                "Remote can operate only in CEE version!!!\r\n");
                        if (DCBX_SUCCESS != DcbxUtlSwitchVersion(DCBX_VER_CEE, pMsg->u4IfIndex))
                        {
                            DCBX_TRC (DCBX_FAILURE_TRC, "DcbxUtlHandleVerResolution: DcbxUtlSwitchVersion FAILS!!!");
                            return DCBX_FAILURE;
                        }
                    }
                }
                else if ((DCBX_MODE_IEEE == pDcbxPortEntry->u1DcbxMode) && \
                        (DCBX_VER_IEEE == pDcbxPortEntry->u1OperVersion))
                {
                    DCBX_TRC (DCBX_FAILURE_TRC, "DcbxUtlHandleVerResolution:"
                            "Drop Pkt::Receiver is in IEEE mode!!!\r\n");
                    return DCBX_FAILURE;
                }
                /* Updating the Peer Oper and Max verison based on which is received*/
                pDcbxPortEntry->u1PeerOperVersion = u1OperVersion;
                pDcbxPortEntry->u1PeerMaxVersion = u1MaxVersion;
                /* Any unsupported version to be marked as unknown */
                if (pDcbxPortEntry->u1PeerMaxVersion > DCBX_VER_UNKNOWN)
                {
                    pDcbxPortEntry->u1PeerMaxVersion = DCBX_VER_UNKNOWN;
                }
            }
            break;

        case DCBX_VER_IEEE:
            {
                DCBX_TRC (DCBX_CONTROL_PLANE_TRC, "DcbxUtlHandleVerResolution:"
                        "IEEE TLV Received, version resolution starts!!!\r\n");
                if ((DCBX_MODE_AUTO == pDcbxPortEntry->u1DcbxMode) && \
                        (DCBX_VER_CEE == pDcbxPortEntry->u1OperVersion))
                {
                    DCBX_TRC (DCBX_CONTROL_PLANE_TRC, "DcbxUtlHandleVerResolution:"
                            "SwitchMode::Local to switch to IEEE version as "
                            "Remote can operate only in IEEE version!!!\r\n");
                    if (DCBX_SUCCESS != DcbxUtlSwitchVersion(DCBX_MODE_IEEE, pMsg->u4IfIndex))
                    {
                        DCBX_TRC (DCBX_FAILURE_TRC, "DcbxUtlHandleVerResolution: DcbxUtlSwitchVersion FAILS!!!");
                        return DCBX_FAILURE;
                    }
                }
                else if ((DCBX_MODE_CEE == pDcbxPortEntry->u1DcbxMode) && \
                        (DCBX_VER_CEE == pDcbxPortEntry->u1OperVersion))
                {
                    if (DCBX_VER_CEE == pDcbxPortEntry->u1MaxVersion)
                    {
                        DCBX_TRC (DCBX_FAILURE_TRC, "DcbxUtlHandleVerResolution:"
                                "No Update::Remote to switch to CEE version So "
                                "Local to continue operate in CEE version!!!\r\n");
                        return DCBX_FAILURE;
                    }
                }
                /* Updating the Peer Oper and Max verison based on which is received*/
                pDcbxPortEntry->u1PeerOperVersion = DCBX_VER_IEEE;
                pDcbxPortEntry->u1PeerMaxVersion = DCBX_VER_IEEE;
            }
            break;

        default:
            DCBX_TRC (DCBX_FAILURE_TRC, "DcbxUtlHandleVerResolution:"
                    "Some Unknown TLV Received!!!\r\n");
            break;
    }
    return DCBX_SUCCESS;
}

/***************************************************************************
 *  FUNCTION NAME : DcbxUtlSwitchVersion
 *
 *  DESCRIPTION   : This function is called while doing version resolution to
 *                  perform - register new mode, deregister / send aged out / trap 
 *                  for previous mode before switching to IEEE or CEE version.
 *
 *  INPUT         : u1SwitchToVer - Switch to the provided TLV
 *                  u4IfIndex  - on which interface
 *                  DcbxAppId  - AppId Info
 *
 *  OUTPUT        : None
 *
 *  RETURNS       : DCBX_SUCCESS/DCBX_FAILURE
 ***************************************************************************/
PUBLIC INT4
DcbxUtlSwitchVersion(UINT1 u1SwitchToVer, UINT4 u4IfIndex)
{
    tDcbxCEETrapMsg          NotifyCEETrap;
    tDcbxAppRegInfo          DcbxAppPortMsg;
    tAppPriPortEntry         *pAppPortEntry = NULL;
    tPFCPortEntry            *pPFCPortEntry = NULL;
    tETSPortEntry            *pETSPortEntry = NULL;
    tDcbxPortEntry           *pDcbxPortEntry = NULL;

    MEMSET (&NotifyCEETrap, DCBX_ZERO, sizeof (tDcbxCEETrapMsg));
    
    pDcbxPortEntry = DcbxUtlGetPortEntry (u4IfIndex);
    if (NULL == pDcbxPortEntry)
    {    
        return DCBX_FAILURE;    
    }
    /*1. Send Aged-out message
     *2. Raise Trap
     *3. Form and Send TLV to Control SM*/
    switch(u1SwitchToVer)
    {
        case DCBX_VER_CEE:
            {
                DCBX_TRC_ARG1 (DCBX_TLV_TRC | DCBX_CONTROL_PLANE_TRC , "DcbxUtlSwitchVersion:"
                        " Version Resolution called to switch from auto to CEE mode on port %d\r\n", u4IfIndex );

                /* Call Aged out event for IEEE */
                if (DCBX_SUCCESS != DcbxUtlCallIEEEAgeOut(u4IfIndex))
                {
                    DCBX_TRC (DCBX_FAILURE_TRC, "DcbxUtlSwitchVersion:"
                            "APP Entry not present!!! \r\n");
                    return DCBX_FAILURE;
                }

                /* Update Oper version */
                pDcbxPortEntry->u1OperVersion = DCBX_VER_CEE;

                /* Raise Trap */
                NotifyCEETrap.u4IfIndex = u4IfIndex;
                NotifyCEETrap.unTrapMsg.u1OperVersion = DCBX_VER_CEE;
                DcbxCEESendPortNotification (&NotifyCEETrap, DCBX_VER_CHG_TRAP);

                /* Now registering for IEEE for RxStatus */
                /* Update Statemachine parameter also */
                pAppPortEntry = AppPriUtlGetPortEntry(u4IfIndex);
                if (NULL != pAppPortEntry)
                {
                    AppPriUtlDeRegisterApplForPort(pAppPortEntry);
                    AppPriUtlRegisterApplForPort (pAppPortEntry);
                    pAppPortEntry->u1AppPriDcbxSemType = DCBX_FEAT_STATE_MACHINE;
                }

                pPFCPortEntry = PFCUtlGetPortEntry(u4IfIndex);
                if (NULL != pPFCPortEntry)
                {
                    PFCUtlDeRegisterApplForPort (pPFCPortEntry);
                    PFCUtlRegisterApplForPort (pPFCPortEntry);
                    pPFCPortEntry->u1PFCDcbxSemType = DCBX_FEAT_STATE_MACHINE;
                }

                pETSPortEntry = ETSUtlGetPortEntry(u4IfIndex);
                if (NULL != pETSPortEntry)
                {
                    ETSUtlDeRegisterApplForPort (pETSPortEntry, ETS_TLV_ALL);
                    ETSUtlRegisterApplForPort (pETSPortEntry, ETS_TLV_ALL);
                    pETSPortEntry->u1ETSDcbxSemType = DCBX_FEAT_STATE_MACHINE; 
                }
                
                /* DeRegistering for CEE */
                MEMSET (&DcbxAppPortMsg, DCBX_ZERO, sizeof (tDcbxAppRegInfo));
                CEE_GET_APPL_ID (DcbxAppPortMsg.DcbxAppId);

                CEEPostMsgToDCBX (DCBX_LLDP_PORT_DEREG, u4IfIndex, &DcbxAppPortMsg);
                /* Now registering for CEE for TxStatus and RxStatus */
                CEERegisterApplForPort (u4IfIndex, CEE_ETS_TLV_TYPE);
                CEERegisterApplForPort (u4IfIndex, CEE_PFC_TLV_TYPE);
                CEERegisterApplForPort (u4IfIndex, CEE_APP_PRI_TLV_TYPE);
            }
            break;
        case DCBX_VER_IEEE:
            {
                DCBX_TRC_ARG1 (DCBX_TLV_TRC | DCBX_CONTROL_PLANE_TRC, "DcbxUtlSwitchVersion:"
                        " Version Resolution called to switch from auto to IEEE mode on port %d\r\n", u4IfIndex );

                /* Update Oper version */
                pDcbxPortEntry->u1OperVersion = DCBX_VER_IEEE;

                /* Call Aged out event for CEE */
                DcbxUtlCallCEEAgeOut(u4IfIndex);
                
                /* Raise Trap */
                NotifyCEETrap.u4IfIndex = u4IfIndex;
                NotifyCEETrap.unTrapMsg.u1OperVersion = DCBX_VER_IEEE;
                DcbxCEESendPortNotification (&NotifyCEETrap, DCBX_VER_CHG_TRAP);

                /* DeRegistering for CEE */
                MEMSET (&DcbxAppPortMsg, DCBX_ZERO, sizeof (tDcbxAppRegInfo));
                CEE_GET_APPL_ID (DcbxAppPortMsg.DcbxAppId);

                CEEPostMsgToDCBX (DCBX_LLDP_PORT_DEREG, u4IfIndex, &DcbxAppPortMsg);

                /* Now registering for CEE for RxStatus */
                CEERegisterApplForPort (u4IfIndex, CEE_ETS_TLV_TYPE);
                CEERegisterApplForPort (u4IfIndex, CEE_PFC_TLV_TYPE);
                CEERegisterApplForPort (u4IfIndex, CEE_APP_PRI_TLV_TYPE);

                /* Now registering for IEEE for TxStatus and RxStatus */
                /* Update Statemachine parameter also */
                pAppPortEntry = AppPriUtlGetPortEntry(u4IfIndex);
                if (NULL != pAppPortEntry)
                {
                    AppPriUtlDeRegisterApplForPort (pAppPortEntry);
                    AppPriUtlRegisterApplForPort (pAppPortEntry);
                    pAppPortEntry->u1AppPriDcbxSemType = DCBX_SYM_STATE_MACHINE;
                }

                pPFCPortEntry = PFCUtlGetPortEntry(u4IfIndex);
                if (NULL != pPFCPortEntry)
                {
                    PFCUtlDeRegisterApplForPort (pPFCPortEntry);
                    PFCUtlRegisterApplForPort (pPFCPortEntry);
                    pPFCPortEntry->u1PFCDcbxSemType = DCBX_SYM_STATE_MACHINE;
                }

                pETSPortEntry = ETSUtlGetPortEntry(u4IfIndex);
                if (NULL != pETSPortEntry)
                {
                    ETSUtlDeRegisterApplForPort (pETSPortEntry, ETS_TLV_ALL);
                    ETSUtlRegisterApplForPort (pETSPortEntry, ETS_TLV_ALL);
                    pETSPortEntry->u1ETSDcbxSemType = DCBX_ASYM_STATE_MACHINE; 
                }
            }
            break;
        default:
            DCBX_TRC (DCBX_FAILURE_TRC, "DcbxUtlSwitchVersion:"
                      " invalid mode received!!!\r\n");
            break;

    }
    return DCBX_SUCCESS;
}

/***************************************************************************
 *  FUNCTION NAME : DcbxUtlSwitchMode
 *
 *  DESCRIPTION   : This function is called when mode is modified manually
 *                  and it will perform - register new mode, deregister / 
 *                  send aged out / trap for previous mode before switching
 *                  to IEEE or CEE or AUTO mode
 *
 *  INPUT         : u1NewMode  - Switch to the New Mode
 *                  u1PrevMode - Previous mode
 *                  u4IfIndex  - on which interface
 *
 *  OUTPUT        : None
 *
 *  RETURNS       : None
 ***************************************************************************/
PUBLIC VOID
DcbxUtlSwitchMode(UINT1 u1NewMode, UINT1 u1PrevMode, UINT4 u4IfIndex)
{
    tAppPriPortEntry         *pAppPortEntry = NULL;
    tPFCPortEntry            *pPFCPortEntry = NULL;
    tETSPortEntry            *pETSPortEntry = NULL;
    tDcbxPortEntry           *pDcbxPortEntry = NULL;
    
    pDcbxPortEntry = DcbxUtlGetPortEntry(u4IfIndex);
    if (NULL == pDcbxPortEntry)
    {
        DCBX_TRC (DCBX_FAILURE_TRC, "DcbxUtlSwitchMode:"
                " DCBX port Entry is NULL!!!\r\n");
        return;    
    }
    /*1. Send Aged-out message
     *2. Raise Trap
     *3. Form and Send TLV to Control SM*/
    switch(u1NewMode)
    {
        case DCBX_MODE_CEE:
            {
                DCBX_TRC_ARG1 (DCBX_CONTROL_PLANE_TRC | DCBX_TLV_TRC, "DcbxUtlSwitchMode:"
                        "Switching Mode to CEE for port %d!!!\r\n",u4IfIndex);

                /* Update Oper and MAX Version */
                pDcbxPortEntry->u1OperVersion = u1NewMode;
                pDcbxPortEntry->u1MaxVersion = u1NewMode;
                pDcbxPortEntry->u1PeerOperVersion = DCBX_VER_UNKNOWN;
                pDcbxPortEntry->u1PeerMaxVersion = DCBX_VER_UNKNOWN;

                /* If previuosly, it is IEEE mode then start sending Aged out, raise trap and 
                 * deregistering the same */ 
                if (DCBX_MODE_IEEE == u1PrevMode)
                {
                    DcbxUtlHandleSwitchModeChg (u1PrevMode, u4IfIndex);
                }
                /* If previously, it is AUTO then start sending Aged out, raise trap and 
                 * deregistering for both IEEE & CEE mode */ 
                else if (DCBX_MODE_AUTO == u1PrevMode)
                {
                    DcbxUtlHandleSwitchModeChg (DCBX_MODE_IEEE, u4IfIndex);
                    DcbxUtlHandleSwitchModeChg (DCBX_MODE_CEE, u4IfIndex);
                }

                /* Now Registering for CEE mode */
                CEERegisterApplForPort (u4IfIndex, CEE_ETS_TLV_TYPE);
                CEERegisterApplForPort (u4IfIndex, CEE_PFC_TLV_TYPE);
                CEERegisterApplForPort (u4IfIndex, CEE_APP_PRI_TLV_TYPE);
            }
            break;
        case DCBX_MODE_IEEE:
            {
                DCBX_TRC_ARG1 (DCBX_CONTROL_PLANE_TRC | DCBX_TLV_TRC, "DcbxUtlSwitchMode:"
                        "Switching Mode to IEEE for port %d!!!\r\n", u4IfIndex);

                /* Update Oper and MAX Version */
                pDcbxPortEntry->u1OperVersion = u1NewMode;
                pDcbxPortEntry->u1MaxVersion = u1NewMode;
                pDcbxPortEntry->u1PeerOperVersion = DCBX_VER_UNKNOWN;
                pDcbxPortEntry->u1PeerMaxVersion = DCBX_VER_UNKNOWN;

                /* If previuosly, it is CEE mode then start sending Aged out, raise trap and 
                 * deregistering the same */ 
                if (DCBX_MODE_CEE == u1PrevMode)              
                {
                    DcbxUtlHandleSwitchModeChg (u1PrevMode, u4IfIndex);
                }
                /* If previously, it is AUTO then start sending Aged out, raise trap and 
                 * deregistering for both IEEE & CEE mode */ 
                else if (DCBX_MODE_AUTO == u1PrevMode)
                {
                    DcbxUtlHandleSwitchModeChg (DCBX_MODE_IEEE, u4IfIndex);
                    DcbxUtlHandleSwitchModeChg (DCBX_MODE_CEE, u4IfIndex);
                }

                /* Now Registering for IEEE mode */
                pAppPortEntry = AppPriUtlGetPortEntry(u4IfIndex);
                if (NULL != pAppPortEntry)
                {
                    AppPriUtlRegisterApplForPort (pAppPortEntry);
                }

                pPFCPortEntry = PFCUtlGetPortEntry(u4IfIndex);
                if (NULL != pPFCPortEntry)
                {
                    PFCUtlRegisterApplForPort (pPFCPortEntry);
                }

                pETSPortEntry = ETSUtlGetPortEntry(u4IfIndex);
                if (NULL != pETSPortEntry)
                {
                    ETSUtlRegisterApplForPort (pETSPortEntry, ETS_TLV_ALL);
                }
            }
            break;

        case DCBX_MODE_AUTO:
            {
                DCBX_TRC_ARG1 (DCBX_CONTROL_PLANE_TRC | DCBX_TLV_TRC, "DcbxUtlSwitchMode:"
                        "Switching Mode to AUTO for port %d!!!\r\n",u4IfIndex);
                
                /* Update Oper and MAX Version */
                pDcbxPortEntry->u1OperVersion = DCBX_DEFAULT_VER; 
                pDcbxPortEntry->u1MaxVersion = DCBX_VER_IEEE;
                pDcbxPortEntry->u1PeerOperVersion = DCBX_VER_UNKNOWN;
                pDcbxPortEntry->u1PeerMaxVersion = DCBX_VER_UNKNOWN;

                DcbxUtlHandleSwitchModeChg (u1PrevMode, u4IfIndex);

                /* Now Registering for both IEEE and CEE mode */
                /* Registering for IEEE mode */
                pAppPortEntry = AppPriUtlGetPortEntry(u4IfIndex);
                if (NULL != pAppPortEntry)
                {
                    AppPriUtlRegisterApplForPort (pAppPortEntry);
                }

                pPFCPortEntry = PFCUtlGetPortEntry(u4IfIndex);
                if (NULL != pPFCPortEntry)
                {
                    PFCUtlRegisterApplForPort (pPFCPortEntry);
                }

                pETSPortEntry = ETSUtlGetPortEntry(u4IfIndex);
                if (NULL != pETSPortEntry)
                {
                    ETSUtlRegisterApplForPort (pETSPortEntry, ETS_TLV_ALL);
                }
                /* Registering for CEE mode */
                CEERegisterApplForPort (u4IfIndex, CEE_ETS_TLV_TYPE);
                CEERegisterApplForPort (u4IfIndex, CEE_PFC_TLV_TYPE);
                CEERegisterApplForPort (u4IfIndex, CEE_APP_PRI_TLV_TYPE);
            }
            break;
        default:
            DCBX_TRC (DCBX_FAILURE_TRC, "DcbxUtlSwitchMode:"
                      " invalid mode received!!!\r\n");
            break;

    }
    return;
}

/***************************************************************************
 *  FUNCTION NAME : DcbxUtlGetAppEntryBasedOnMode
 *
 *  DESCRIPTION   : The utility function called to get the App Entry based on 
 *                  the current Mode.
 *
 *  INPUT         : u1Mode     - Mode:IEEE or CEE
 *                  u4IfIndex  - on which interface
 *                  u1TlvType  - Sub Type reqd for IEEE features
 *
 *  OUTPUT        : None
 *
 *  RETURNS       : App Entry of Type tDcbxAppEntry
 ***************************************************************************/
tDcbxAppEntry *DcbxUtlGetAppEntryBasedOnMode (UINT1 u1Mode, UINT4 u4IfIndex, UINT1 u1TlvType)
{
    tDcbxAppEntry            *pDcbxAppEntry = NULL;
    tDcbxAppEntry            DcbxAppEntry;
    tDcbxAppId               DcbxAppId;

    MEMSET (&DcbxAppEntry, DCBX_ZERO, sizeof (tDcbxAppEntry));
    MEMSET (&DcbxAppId, DCBX_ZERO, sizeof(DcbxAppId));

    if (DCBX_MODE_CEE == u1Mode)
    {
        CEE_GET_APPL_ID (DcbxAppId);
    }
    else if (DCBX_MODE_IEEE == u1Mode)
    {
        switch(u1TlvType)
        {
            case APP_PRI_TLV_SUBTYPE:
                APP_PRI_GET_APPL_ID (DcbxAppId);
                break;
            case PFC_TLV_SUBTYPE:
                PFC_GET_APPL_ID (DcbxAppId);
                break;
            case ETS_CONF_TLV_SUB_TYPE:
                ETS_GET_CONF_APPL_ID (DcbxAppId);
                break;
            case ETS_RECO_TLV_SUB_TYPE:
                ETS_GET_RECO_APPL_ID (DcbxAppId);
                break;
            case ETS_TC_SUPP_TLV_SUB_TYPE:
                ETS_GET_TC_SUPP_APPL_ID (DcbxAppId);
                break;
            default:
                break;
        }
    }

    MEMCPY (&(DcbxAppEntry.DcbxAppId), &DcbxAppId, sizeof (tDcbxAppId));

    /* Get the application entry from the DCBX Application table */
    pDcbxAppEntry = (tDcbxAppEntry *) RBTreeGet (gDcbxGlobalInfo.pRbDcbxAppTbl,
            (tRBElem *) &DcbxAppEntry);

    if ((pDcbxAppEntry == NULL) || 
            (pDcbxAppEntry->ApplMappedIfPortList[u4IfIndex] == OSIX_FALSE))
    {
        DCBX_TRC (DCBX_FAILURE_TRC, "DcbxUtlGetAppEntryBasedOnMode: "
                "None of the features are enabled. So returning NULL!!!\r\n");
        pDcbxAppEntry = NULL;        
    }

    return (pDcbxAppEntry);
}

/***************************************************************************
 *  FUNCTION NAME : DcbxUtlHandleSwitchModeChg
 *
 *  DESCRIPTION   : The utility function called to send aged out, trap and
 *                  deregistering the mode based on the input received.
 *
 *  INPUT         : u1PrevVer      - Previous Mode :IEEE or CEE
 *                  u4IfIndex      - on which interface
 *
 *  OUTPUT        : None
 *
 *  RETURNS       : None
 ***************************************************************************/
PUBLIC VOID
DcbxUtlHandleSwitchModeChg (UINT1 u1PrevVer, UINT4 u4IfIndex)
{
    tDcbxAppRegInfo          DcbxAppPortMsg;
    tDcbxCEETrapMsg          NotifyCEETrap;
    tAppPriPortEntry         *pAppPortEntry = NULL;
    tPFCPortEntry            *pPFCPortEntry = NULL;
    tETSPortEntry            *pETSPortEntry = NULL;
    tDcbxPortEntry           *pDcbxPortEntry = NULL;

    MEMSET (&NotifyCEETrap, DCBX_ZERO, sizeof (tDcbxCEETrapMsg));

    pDcbxPortEntry = DcbxUtlGetPortEntry (u4IfIndex);
    if (NULL == pDcbxPortEntry)
    {
        return;
    }

    if (DCBX_MODE_IEEE == u1PrevVer)
    {
        /* Send Aged Out message for IEEE */
        if (DCBX_SUCCESS != DcbxUtlCallIEEEAgeOut(u4IfIndex))
        {
            DCBX_TRC (DCBX_FAILURE_TRC, "DcbxUtlHandleSwitchModeChg:"
                    "APP Entry not present!!! \r\n");
            return;
        }

        /* Raise TRAP */
        NotifyCEETrap.u4IfIndex = u4IfIndex;
        NotifyCEETrap.unTrapMsg.u1OperVersion = u1PrevVer;
        DcbxCEESendPortNotification (&NotifyCEETrap, DCBX_VER_CHG_TRAP);
        
        /* Deregister the IEEE mode */        
        pAppPortEntry = AppPriUtlGetPortEntry(u4IfIndex);
        if (NULL != pAppPortEntry)
        {
            AppPriUtlDeRegisterApplForPort (pAppPortEntry);
        }

        pPFCPortEntry = PFCUtlGetPortEntry(u4IfIndex);
        if (NULL != pPFCPortEntry)
        {
            PFCUtlDeRegisterApplForPort (pPFCPortEntry);
        }

        pETSPortEntry = ETSUtlGetPortEntry(u4IfIndex);
        if (NULL != pETSPortEntry)
        {
            ETSUtlDeRegisterApplForPort (pETSPortEntry, ETS_TLV_ALL);
        }
    }
    else if(DCBX_MODE_CEE == u1PrevVer)
    {
        /* Call Aged out event for CEE */
        DcbxUtlCallCEEAgeOut(u4IfIndex);

        /* Raise TRAP */
        NotifyCEETrap.u4IfIndex = u4IfIndex;
        NotifyCEETrap.unTrapMsg.u1OperVersion = u1PrevVer;
        DcbxCEESendPortNotification (&NotifyCEETrap, DCBX_VER_CHG_TRAP);

        /* Deregistering CEE */
        MEMSET (&DcbxAppPortMsg, DCBX_ZERO, sizeof (tDcbxAppRegInfo));
        CEE_GET_APPL_ID (DcbxAppPortMsg.DcbxAppId);

        CEEPostMsgToDCBX (DCBX_LLDP_PORT_DEREG, u4IfIndex,
                &DcbxAppPortMsg);
    }

    return;
}

/***************************************************************************
 *  FUNCTION NAME : DcbxUtlCallCEEAgeOut
 *
 *  DESCRIPTION   : The utility function called to perform aged out related 
 *                  steps.
 *
 *  INPUT         : u4IfIndex      - on which interface
 *
 *  OUTPUT        : None
 *
 *  RETURNS       : None
 ***************************************************************************/
PUBLIC VOID 
DcbxUtlCallCEEAgeOut(UINT4 u4IfIndex)
{
    tAppPriPortEntry         *pAppPortEntry = NULL;
    tPFCPortEntry            *pPFCPortEntry = NULL;
    tETSPortEntry            *pETSPortEntry = NULL;

    DCBX_TRC_ARG1 (DCBX_TLV_TRC, "DcbxUtlCallCEEAgeOut: "
            "Age out received on port %d!!!\r\n",u4IfIndex);
    pAppPortEntry = AppPriUtlGetPortEntry(u4IfIndex);
    if (NULL != pAppPortEntry)
    {
        CEEUtlHandleAppPriAgeOut(pAppPortEntry);
    }
    pPFCPortEntry = PFCUtlGetPortEntry(u4IfIndex);
    if (NULL != pPFCPortEntry)
    {
        CEEUtlHandlePFCAgeOut(pPFCPortEntry);
    }
    pETSPortEntry = ETSUtlGetPortEntry(u4IfIndex);
    if (NULL != pETSPortEntry)
    {
        CEEUtlHandleETSAgeOut(pETSPortEntry);
    }
}

/***************************************************************************
 *  FUNCTION NAME : DcbxUtlCallIEEEAgeOut
 *
 *  DESCRIPTION   : The utility function called to perform aged out related 
 *                  steps.
 *
 *  INPUT         : u4IfIndex      - on which interface
 *
 *  OUTPUT        : None
 *
 *  RETURNS       : DCBX_FAILURE/DCBX_SUCCESS
 ***************************************************************************/
PUBLIC INT4
DcbxUtlCallIEEEAgeOut(UINT4 u4IfIndex)
{
    UINT1                    u1TempSubType = DCBX_ZERO;
    INT4                     i4RetVal = DCBX_FAILURE;
    tDcbxAppInfo             DcbxAppInfo;
    tAppPriPortEntry         *pAppPortEntry = NULL;
    tPFCPortEntry            *pPFCPortEntry = NULL;
    tETSPortEntry            *pETSPortEntry = NULL;
    tDcbxAppEntry            *pDcbxAppEntry = NULL;

    DCBX_TRC_ARG1 (DCBX_TLV_TRC, "DcbxUtlCallIEEEAgeOut: "
            "Age out received on port %d!!!\r\n",u4IfIndex);
    pAppPortEntry = AppPriUtlGetPortEntry(u4IfIndex);
    if (NULL != pAppPortEntry)
    {
        pDcbxAppEntry = DcbxUtlGetAppEntryBasedOnMode (DCBX_MODE_IEEE,
                u4IfIndex, APP_PRI_TLV_SUBTYPE);
        if (NULL != pDcbxAppEntry)
        {
            MEMSET (&DcbxAppInfo, DCBX_ZERO, sizeof (tDcbxAppInfo));

            DcbxAppInfo.u4IfIndex = u4IfIndex;
            DcbxAppInfo.u4MsgType = L2IWF_LLDP_APPL_TLV_AGED;
            pDcbxAppEntry->pAppCallBackFn (&DcbxAppInfo);
            i4RetVal = DCBX_SUCCESS;
        }
    }

    pPFCPortEntry = PFCUtlGetPortEntry(u4IfIndex);
    if (NULL != pPFCPortEntry)
    {
        pDcbxAppEntry = DcbxUtlGetAppEntryBasedOnMode (DCBX_MODE_IEEE, 
                u4IfIndex, PFC_TLV_SUBTYPE);
        if (NULL != pDcbxAppEntry)
        {
            MEMSET (&DcbxAppInfo, DCBX_ZERO, sizeof (tDcbxAppInfo));

            DcbxAppInfo.u4IfIndex = u4IfIndex;
            DcbxAppInfo.u4MsgType = L2IWF_LLDP_APPL_TLV_AGED;
            pDcbxAppEntry->pAppCallBackFn (&DcbxAppInfo);
            i4RetVal = DCBX_SUCCESS;
        }
    }

    pETSPortEntry = ETSUtlGetPortEntry(u4IfIndex);
    if (NULL != pETSPortEntry)
    {
        for (u1TempSubType = ETS_TC_SUPP_TLV_SUB_TYPE; 
                u1TempSubType <= ETS_RECO_TLV_SUB_TYPE; u1TempSubType++)
        {
            pDcbxAppEntry = DcbxUtlGetAppEntryBasedOnMode (DCBX_MODE_IEEE,
                    u4IfIndex, u1TempSubType);
            if (NULL != pDcbxAppEntry)
            {
                MEMSET (&DcbxAppInfo, DCBX_ZERO, sizeof (tDcbxAppInfo));

                DcbxAppInfo.u4IfIndex = u4IfIndex;
                DcbxAppInfo.u4MsgType = L2IWF_LLDP_APPL_TLV_AGED;
                pDcbxAppEntry->pAppCallBackFn (&DcbxAppInfo);
                i4RetVal = DCBX_SUCCESS;
            }
        }
    }
    return i4RetVal;
}

/* CEE : Version Resolution ends */
/***************************************************************************
 *  FUNCTION NAME : DcbxPktDump
 * 
 *  DESCRIPTION   : This function is used to dump the dcbx packet.
 * 
 *  INPUT         : pu1Buf - Packet Buffer
 *                  u2Length -  Length of the packet buffer
 *                  u1PktType - Packet type Tx or rx 
 * 
 *  OUTPUT        : None
 * 
 *  RETURNS       : None
 * 
 * **************************************************************************/
PUBLIC VOID
DcbxPktDump (UINT1 *pu1Buf, UINT2 u2Length, 
                        UINT1 u1PktType)
{
    UINT2               u2Count = DCBX_ONE;
    CHR1                au1Buf[10];

    if(u1PktType == DCBX_TX)
    {
        DCBX_TRC (DCBX_TLV_TRC, "DcbxPktDump: "
                "[DCBX] : Dumping Transmitted DCBX frame!!!\r\n");
    }
    else if(u1PktType == DCBX_RX)
    {
        DCBX_TRC (DCBX_TLV_TRC, "DcbxPktDump: "
                "[DCBX] : Dumping Received DCBX frame!!!\r\n");
    }
    while(u2Count <= u2Length)
    {
        SPRINTF ((CHR1 *) au1Buf, "%02x ",  pu1Buf[u2Count-1]);
        UtlTrcPrint (au1Buf);
        u2Count++;
        if((u2Count % 16) == 0)
        {
            UtlTrcPrint ("\n");
        }
    }
    UtlTrcPrint ("\n");
}
/****** END OF FILE **********/
