/********************************************************************
 * Copyright (C) 2010 Aricent Inc . All Rights Reserved
 *
 * $Id: stddcbwr.c,v 1.9 2016/07/09 09:41:21 siva Exp $
 *
 * Description: Protocol Mid Level Routines
 *********************************************************************/
# include  "lr.h"
# include  "fssnmp.h"
# include  "stddcbwr.h"
# include  "stddcbdb.h"
# include  "dcbxinc.h"

VOID
RegisterSTDDCB ()
{
    SNMPRegisterMibWithLock (&stddcbOID, &stddcbEntry, DcbxLock, DcbxUnLock, 
                             SNMP_MSR_TGR_TRUE);
    SNMPAddSysorEntry (&stddcbOID, (const UINT1 *) "stddcb");
}

VOID
UnRegisterSTDDCB ()
{
    SNMPUnRegisterMib (&stddcbOID, &stddcbEntry);
    SNMPDelSysorEntry (&stddcbOID, (const UINT1 *) "stddcb");
}

INT4
GetNextIndexLldpXdot1dcbxConfigETSConfigurationTable (tSnmpIndex *
                                                      pFirstMultiIndex,
                                                      tSnmpIndex *
                                                      pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexLldpXdot1dcbxConfigETSConfigurationTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue),
             &(pNextMultiIndex->pIndex[1].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexLldpXdot1dcbxConfigETSConfigurationTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue),
             pFirstMultiIndex->pIndex[1].u4_ULongValue,
             &(pNextMultiIndex->pIndex[1].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
LldpXdot1dcbxConfigETSConfigurationTxEnableGet (tSnmpIndex * pMultiIndex,
                                                tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceLldpXdot1dcbxConfigETSConfigurationTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetLldpXdot1dcbxConfigETSConfigurationTxEnable
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
LldpXdot1dcbxConfigETSConfigurationTxEnableSet (tSnmpIndex * pMultiIndex,
                                                tRetVal * pMultiData)
{
    return (nmhSetLldpXdot1dcbxConfigETSConfigurationTxEnable
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].u4_ULongValue, pMultiData->i4_SLongValue));

}

INT4
LldpXdot1dcbxConfigETSConfigurationTxEnableTest (UINT4 *pu4Error,
                                                 tSnmpIndex * pMultiIndex,
                                                 tRetVal * pMultiData)
{
    return (nmhTestv2LldpXdot1dcbxConfigETSConfigurationTxEnable (pu4Error,
                                                                  pMultiIndex->
                                                                  pIndex[0].
                                                                  i4_SLongValue,
                                                                  pMultiIndex->
                                                                  pIndex[1].
                                                                  u4_ULongValue,
                                                                  pMultiData->
                                                                  i4_SLongValue));

}

INT4
LldpXdot1dcbxConfigETSConfigurationTableDep (UINT4 *pu4Error,
                                             tSnmpIndexList * pSnmpIndexList,
                                             tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2LldpXdot1dcbxConfigETSConfigurationTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexLldpXdot1dcbxConfigETSRecommendationTable (tSnmpIndex *
                                                       pFirstMultiIndex,
                                                       tSnmpIndex *
                                                       pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexLldpXdot1dcbxConfigETSRecommendationTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue),
             &(pNextMultiIndex->pIndex[1].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexLldpXdot1dcbxConfigETSRecommendationTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue),
             pFirstMultiIndex->pIndex[1].u4_ULongValue,
             &(pNextMultiIndex->pIndex[1].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
LldpXdot1dcbxConfigETSRecommendationTxEnableGet (tSnmpIndex * pMultiIndex,
                                                 tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceLldpXdot1dcbxConfigETSRecommendationTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetLldpXdot1dcbxConfigETSRecommendationTxEnable
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
LldpXdot1dcbxConfigETSRecommendationTxEnableSet (tSnmpIndex * pMultiIndex,
                                                 tRetVal * pMultiData)
{
    return (nmhSetLldpXdot1dcbxConfigETSRecommendationTxEnable
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].u4_ULongValue, pMultiData->i4_SLongValue));

}

INT4
LldpXdot1dcbxConfigETSRecommendationTxEnableTest (UINT4 *pu4Error,
                                                  tSnmpIndex * pMultiIndex,
                                                  tRetVal * pMultiData)
{
    return (nmhTestv2LldpXdot1dcbxConfigETSRecommendationTxEnable (pu4Error,
                                                                   pMultiIndex->
                                                                   pIndex[0].
                                                                   i4_SLongValue,
                                                                   pMultiIndex->
                                                                   pIndex[1].
                                                                   u4_ULongValue,
                                                                   pMultiData->
                                                                   i4_SLongValue));

}

INT4
LldpXdot1dcbxConfigETSRecommendationTableDep (UINT4 *pu4Error,
                                              tSnmpIndexList * pSnmpIndexList,
                                              tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2LldpXdot1dcbxConfigETSRecommendationTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexLldpXdot1dcbxConfigPFCTable (tSnmpIndex * pFirstMultiIndex,
                                         tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexLldpXdot1dcbxConfigPFCTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue),
             &(pNextMultiIndex->pIndex[1].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexLldpXdot1dcbxConfigPFCTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue),
             pFirstMultiIndex->pIndex[1].u4_ULongValue,
             &(pNextMultiIndex->pIndex[1].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
LldpXdot1dcbxConfigPFCTxEnableGet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceLldpXdot1dcbxConfigPFCTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetLldpXdot1dcbxConfigPFCTxEnable
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
LldpXdot1dcbxConfigPFCTxEnableSet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    return (nmhSetLldpXdot1dcbxConfigPFCTxEnable
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].u4_ULongValue, pMultiData->i4_SLongValue));

}

INT4
LldpXdot1dcbxConfigPFCTxEnableTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    return (nmhTestv2LldpXdot1dcbxConfigPFCTxEnable (pu4Error,
                                                     pMultiIndex->pIndex[0].
                                                     i4_SLongValue,
                                                     pMultiIndex->pIndex[1].
                                                     u4_ULongValue,
                                                     pMultiData->
                                                     i4_SLongValue));

}

INT4
LldpXdot1dcbxConfigPFCTableDep (UINT4 *pu4Error,
                                tSnmpIndexList * pSnmpIndexList,
                                tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2LldpXdot1dcbxConfigPFCTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexLldpXdot1dcbxConfigApplicationPriorityTable (tSnmpIndex *
                                                         pFirstMultiIndex,
                                                         tSnmpIndex *
                                                         pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexLldpXdot1dcbxConfigApplicationPriorityTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue),
             &(pNextMultiIndex->pIndex[1].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexLldpXdot1dcbxConfigApplicationPriorityTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue),
             pFirstMultiIndex->pIndex[1].u4_ULongValue,
             &(pNextMultiIndex->pIndex[1].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
LldpXdot1dcbxConfigApplicationPriorityTxEnableGet (tSnmpIndex * pMultiIndex,
                                                   tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceLldpXdot1dcbxConfigApplicationPriorityTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetLldpXdot1dcbxConfigApplicationPriorityTxEnable
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
LldpXdot1dcbxConfigApplicationPriorityTxEnableSet (tSnmpIndex * pMultiIndex,
                                                   tRetVal * pMultiData)
{
    return (nmhSetLldpXdot1dcbxConfigApplicationPriorityTxEnable
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].u4_ULongValue, pMultiData->i4_SLongValue));

}

INT4
LldpXdot1dcbxConfigApplicationPriorityTxEnableTest (UINT4 *pu4Error,
                                                    tSnmpIndex * pMultiIndex,
                                                    tRetVal * pMultiData)
{
    return (nmhTestv2LldpXdot1dcbxConfigApplicationPriorityTxEnable (pu4Error,
                                                                     pMultiIndex->
                                                                     pIndex[0].
                                                                     i4_SLongValue,
                                                                     pMultiIndex->
                                                                     pIndex[1].
                                                                     u4_ULongValue,
                                                                     pMultiData->
                                                                     i4_SLongValue));

}

INT4
LldpXdot1dcbxConfigApplicationPriorityTableDep (UINT4 *pu4Error,
                                                tSnmpIndexList * pSnmpIndexList,
                                                tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2LldpXdot1dcbxConfigApplicationPriorityTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexLldpXdot1dcbxLocETSBasicConfigurationTable (tSnmpIndex *
                                                        pFirstMultiIndex,
                                                        tSnmpIndex *
                                                        pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexLldpXdot1dcbxLocETSBasicConfigurationTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexLldpXdot1dcbxLocETSBasicConfigurationTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
LldpXdot1dcbxLocETSConCreditBasedShaperSupportGet (tSnmpIndex * pMultiIndex,
                                                   tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceLldpXdot1dcbxLocETSBasicConfigurationTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetLldpXdot1dcbxLocETSConCreditBasedShaperSupport
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
LldpXdot1dcbxLocETSConTrafficClassesSupportedGet (tSnmpIndex * pMultiIndex,
                                                  tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceLldpXdot1dcbxLocETSBasicConfigurationTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetLldpXdot1dcbxLocETSConTrafficClassesSupported
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
LldpXdot1dcbxLocETSConWillingGet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceLldpXdot1dcbxLocETSBasicConfigurationTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetLldpXdot1dcbxLocETSConWilling
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
GetNextIndexLldpXdot1dcbxLocETSConPriorityAssignmentTable (tSnmpIndex *
                                                           pFirstMultiIndex,
                                                           tSnmpIndex *
                                                           pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexLldpXdot1dcbxLocETSConPriorityAssignmentTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue),
             &(pNextMultiIndex->pIndex[1].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexLldpXdot1dcbxLocETSConPriorityAssignmentTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue),
             pFirstMultiIndex->pIndex[1].u4_ULongValue,
             &(pNextMultiIndex->pIndex[1].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
LldpXdot1dcbxLocETSConPriTrafficClassGet (tSnmpIndex * pMultiIndex,
                                          tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceLldpXdot1dcbxLocETSConPriorityAssignmentTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetLldpXdot1dcbxLocETSConPriTrafficClass
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
GetNextIndexLldpXdot1dcbxLocETSConTrafficClassBandwidthTable (tSnmpIndex *
                                                              pFirstMultiIndex,
                                                              tSnmpIndex *
                                                              pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexLldpXdot1dcbxLocETSConTrafficClassBandwidthTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue),
             &(pNextMultiIndex->pIndex[1].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexLldpXdot1dcbxLocETSConTrafficClassBandwidthTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue),
             pFirstMultiIndex->pIndex[1].u4_ULongValue,
             &(pNextMultiIndex->pIndex[1].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
LldpXdot1dcbxLocETSConTrafficClassBandwidthGet (tSnmpIndex * pMultiIndex,
                                                tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceLldpXdot1dcbxLocETSConTrafficClassBandwidthTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetLldpXdot1dcbxLocETSConTrafficClassBandwidth
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
GetNextIndexLldpXdot1dcbxLocETSConTrafficSelectionAlgorithmTable (tSnmpIndex *
                                                                  pFirstMultiIndex,
                                                                  tSnmpIndex *
                                                                  pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexLldpXdot1dcbxLocETSConTrafficSelectionAlgorithmTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue),
             &(pNextMultiIndex->pIndex[1].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexLldpXdot1dcbxLocETSConTrafficSelectionAlgorithmTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue),
             pFirstMultiIndex->pIndex[1].u4_ULongValue,
             &(pNextMultiIndex->pIndex[1].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
LldpXdot1dcbxLocETSConTrafficSelectionAlgorithmGet (tSnmpIndex * pMultiIndex,
                                                    tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceLldpXdot1dcbxLocETSConTrafficSelectionAlgorithmTable (pMultiIndex->pIndex[0].i4_SLongValue, pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetLldpXdot1dcbxLocETSConTrafficSelectionAlgorithm
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
GetNextIndexLldpXdot1dcbxLocETSRecoTrafficClassBandwidthTable (tSnmpIndex *
                                                               pFirstMultiIndex,
                                                               tSnmpIndex *
                                                               pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexLldpXdot1dcbxLocETSRecoTrafficClassBandwidthTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue),
             &(pNextMultiIndex->pIndex[1].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexLldpXdot1dcbxLocETSRecoTrafficClassBandwidthTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue),
             pFirstMultiIndex->pIndex[1].u4_ULongValue,
             &(pNextMultiIndex->pIndex[1].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
LldpXdot1dcbxLocETSRecoTrafficClassBandwidthGet (tSnmpIndex * pMultiIndex,
                                                 tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceLldpXdot1dcbxLocETSRecoTrafficClassBandwidthTable (pMultiIndex->pIndex[0].i4_SLongValue, pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetLldpXdot1dcbxLocETSRecoTrafficClassBandwidth
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
GetNextIndexLldpXdot1dcbxLocETSRecoTrafficSelectionAlgorithmTable (tSnmpIndex *
                                                                   pFirstMultiIndex,
                                                                   tSnmpIndex *
                                                                   pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexLldpXdot1dcbxLocETSRecoTrafficSelectionAlgorithmTable (&(pNextMultiIndex->pIndex[0].i4_SLongValue), &(pNextMultiIndex->pIndex[1].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexLldpXdot1dcbxLocETSRecoTrafficSelectionAlgorithmTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue),
             pFirstMultiIndex->pIndex[1].u4_ULongValue,
             &(pNextMultiIndex->pIndex[1].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
LldpXdot1dcbxLocETSRecoTrafficSelectionAlgorithmGet (tSnmpIndex * pMultiIndex,
                                                     tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceLldpXdot1dcbxLocETSRecoTrafficSelectionAlgorithmTable (pMultiIndex->pIndex[0].i4_SLongValue, pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetLldpXdot1dcbxLocETSRecoTrafficSelectionAlgorithm
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
GetNextIndexLldpXdot1dcbxLocPFCBasicTable (tSnmpIndex * pFirstMultiIndex,
                                           tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexLldpXdot1dcbxLocPFCBasicTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexLldpXdot1dcbxLocPFCBasicTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
LldpXdot1dcbxLocPFCWillingGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceLldpXdot1dcbxLocPFCBasicTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetLldpXdot1dcbxLocPFCWilling
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
LldpXdot1dcbxLocPFCMBCGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceLldpXdot1dcbxLocPFCBasicTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetLldpXdot1dcbxLocPFCMBC (pMultiIndex->pIndex[0].i4_SLongValue,
                                          &(pMultiData->i4_SLongValue)));

}

INT4
LldpXdot1dcbxLocPFCCapGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceLldpXdot1dcbxLocPFCBasicTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetLldpXdot1dcbxLocPFCCap (pMultiIndex->pIndex[0].i4_SLongValue,
                                          &(pMultiData->u4_ULongValue)));

}

INT4
GetNextIndexLldpXdot1dcbxLocPFCEnableTable (tSnmpIndex * pFirstMultiIndex,
                                            tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexLldpXdot1dcbxLocPFCEnableTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue),
             &(pNextMultiIndex->pIndex[1].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexLldpXdot1dcbxLocPFCEnableTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue),
             pFirstMultiIndex->pIndex[1].u4_ULongValue,
             &(pNextMultiIndex->pIndex[1].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
LldpXdot1dcbxLocPFCEnableEnabledGet (tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceLldpXdot1dcbxLocPFCEnableTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetLldpXdot1dcbxLocPFCEnableEnabled
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
GetNextIndexLldpXdot1dcbxLocApplicationPriorityAppTable (tSnmpIndex *
                                                         pFirstMultiIndex,
                                                         tSnmpIndex *
                                                         pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexLldpXdot1dcbxLocApplicationPriorityAppTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue),
             &(pNextMultiIndex->pIndex[1].i4_SLongValue),
             &(pNextMultiIndex->pIndex[2].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexLldpXdot1dcbxLocApplicationPriorityAppTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue),
             pFirstMultiIndex->pIndex[1].i4_SLongValue,
             &(pNextMultiIndex->pIndex[1].i4_SLongValue),
             pFirstMultiIndex->pIndex[2].u4_ULongValue,
             &(pNextMultiIndex->pIndex[2].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
LldpXdot1dcbxLocApplicationPriorityAEPriorityGet (tSnmpIndex * pMultiIndex,
                                                  tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceLldpXdot1dcbxLocApplicationPriorityAppTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetLldpXdot1dcbxLocApplicationPriorityAEPriority
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             pMultiIndex->pIndex[2].u4_ULongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
GetNextIndexLldpXdot1dcbxRemETSBasicConfigurationTable (tSnmpIndex *
                                                        pFirstMultiIndex,
                                                        tSnmpIndex *
                                                        pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexLldpXdot1dcbxRemETSBasicConfigurationTable
            (&(pNextMultiIndex->pIndex[0].u4_ULongValue),
             &(pNextMultiIndex->pIndex[1].i4_SLongValue),
             &(pNextMultiIndex->pIndex[2].u4_ULongValue),
             &(pNextMultiIndex->pIndex[3].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexLldpXdot1dcbxRemETSBasicConfigurationTable
            (pFirstMultiIndex->pIndex[0].u4_ULongValue,
             &(pNextMultiIndex->pIndex[0].u4_ULongValue),
             pFirstMultiIndex->pIndex[1].i4_SLongValue,
             &(pNextMultiIndex->pIndex[1].i4_SLongValue),
             pFirstMultiIndex->pIndex[2].u4_ULongValue,
             &(pNextMultiIndex->pIndex[2].u4_ULongValue),
             pFirstMultiIndex->pIndex[3].i4_SLongValue,
             &(pNextMultiIndex->pIndex[3].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    pNextMultiIndex->pIndex[2].pOctetStrValue->i4_Length = 6;
    return SNMP_SUCCESS;
}

INT4
LldpXdot1dcbxRemETSConCreditBasedShaperSupportGet (tSnmpIndex * pMultiIndex,
                                                   tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceLldpXdot1dcbxRemETSBasicConfigurationTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetLldpXdot1dcbxRemETSConCreditBasedShaperSupport
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             pMultiIndex->pIndex[2].u4_ULongValue,
             pMultiIndex->pIndex[3].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
LldpXdot1dcbxRemETSConTrafficClassesSupportedGet (tSnmpIndex * pMultiIndex,
                                                  tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceLldpXdot1dcbxRemETSBasicConfigurationTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetLldpXdot1dcbxRemETSConTrafficClassesSupported
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             pMultiIndex->pIndex[2].u4_ULongValue,
             pMultiIndex->pIndex[3].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
LldpXdot1dcbxRemETSConWillingGet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceLldpXdot1dcbxRemETSBasicConfigurationTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetLldpXdot1dcbxRemETSConWilling
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
              pMultiIndex->pIndex[2].u4_ULongValue,
              pMultiIndex->pIndex[3].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
GetNextIndexLldpXdot1dcbxRemETSConPriorityAssignmentTable (tSnmpIndex *
                                                           pFirstMultiIndex,
                                                           tSnmpIndex *
                                                           pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexLldpXdot1dcbxRemETSConPriorityAssignmentTable
            (&(pNextMultiIndex->pIndex[0].u4_ULongValue),
             &(pNextMultiIndex->pIndex[1].i4_SLongValue),
             &(pNextMultiIndex->pIndex[2].u4_ULongValue),
             &(pNextMultiIndex->pIndex[3].i4_SLongValue),
             &(pNextMultiIndex->pIndex[4].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexLldpXdot1dcbxRemETSConPriorityAssignmentTable
            (pFirstMultiIndex->pIndex[0].u4_ULongValue,
             &(pNextMultiIndex->pIndex[0].u4_ULongValue),
             pFirstMultiIndex->pIndex[1].i4_SLongValue,
             &(pNextMultiIndex->pIndex[1].i4_SLongValue),
             pFirstMultiIndex->pIndex[2].u4_ULongValue,
             &(pNextMultiIndex->pIndex[2].u4_ULongValue),
             pFirstMultiIndex->pIndex[3].i4_SLongValue,
             &(pNextMultiIndex->pIndex[3].i4_SLongValue),
             pFirstMultiIndex->pIndex[4].u4_ULongValue,
             &(pNextMultiIndex->pIndex[4].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    pNextMultiIndex->pIndex[2].pOctetStrValue->i4_Length = 6;
    return SNMP_SUCCESS;
}

INT4
LldpXdot1dcbxRemETSConPriTrafficClassGet (tSnmpIndex * pMultiIndex,
                                          tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceLldpXdot1dcbxRemETSConPriorityAssignmentTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].i4_SLongValue,
         pMultiIndex->pIndex[4].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetLldpXdot1dcbxRemETSConPriTrafficClass
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
              pMultiIndex->pIndex[2].u4_ULongValue,
              pMultiIndex->pIndex[3].i4_SLongValue,
             pMultiIndex->pIndex[4].u4_ULongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
GetNextIndexLldpXdot1dcbxRemETSConTrafficClassBandwidthTable (tSnmpIndex *
                                                              pFirstMultiIndex,
                                                              tSnmpIndex *
                                                              pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexLldpXdot1dcbxRemETSConTrafficClassBandwidthTable
            (&(pNextMultiIndex->pIndex[0].u4_ULongValue),
             &(pNextMultiIndex->pIndex[1].i4_SLongValue),
             &(pNextMultiIndex->pIndex[2].u4_ULongValue),
             &(pNextMultiIndex->pIndex[3].i4_SLongValue),
             &(pNextMultiIndex->pIndex[4].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexLldpXdot1dcbxRemETSConTrafficClassBandwidthTable
            (pFirstMultiIndex->pIndex[0].u4_ULongValue,
             &(pNextMultiIndex->pIndex[0].u4_ULongValue),
             pFirstMultiIndex->pIndex[1].i4_SLongValue,
             &(pNextMultiIndex->pIndex[1].i4_SLongValue),
             pFirstMultiIndex->pIndex[2].u4_ULongValue,
             &(pNextMultiIndex->pIndex[2].u4_ULongValue), 
             pFirstMultiIndex->pIndex[3].i4_SLongValue,
             &(pNextMultiIndex->pIndex[3].i4_SLongValue),
             pFirstMultiIndex->pIndex[4].u4_ULongValue,
             &(pNextMultiIndex->pIndex[4].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    pNextMultiIndex->pIndex[2].pOctetStrValue->i4_Length = 6;
    return SNMP_SUCCESS;
}

INT4
LldpXdot1dcbxRemETSConTrafficClassBandwidthGet (tSnmpIndex * pMultiIndex,
                                                tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceLldpXdot1dcbxRemETSConTrafficClassBandwidthTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].i4_SLongValue,
         pMultiIndex->pIndex[4].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetLldpXdot1dcbxRemETSConTrafficClassBandwidth
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             pMultiIndex->pIndex[2].u4_ULongValue, 
             pMultiIndex->pIndex[3].i4_SLongValue,
             pMultiIndex->pIndex[4].u4_ULongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
GetNextIndexLldpXdot1dcbxRemETSConTrafficSelectionAlgorithmTable (tSnmpIndex *
                                                                  pFirstMultiIndex,
                                                                  tSnmpIndex *
                                                                  pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexLldpXdot1dcbxRemETSConTrafficSelectionAlgorithmTable
            (&(pNextMultiIndex->pIndex[0].u4_ULongValue),
             &(pNextMultiIndex->pIndex[1].i4_SLongValue),
             &(pNextMultiIndex->pIndex[2].u4_ULongValue), 
             &(pNextMultiIndex->pIndex[3].i4_SLongValue),
             &(pNextMultiIndex->pIndex[4].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexLldpXdot1dcbxRemETSConTrafficSelectionAlgorithmTable
            (pFirstMultiIndex->pIndex[0].u4_ULongValue,
             &(pNextMultiIndex->pIndex[0].u4_ULongValue),
             pFirstMultiIndex->pIndex[1].i4_SLongValue,
             &(pNextMultiIndex->pIndex[1].i4_SLongValue),
             pFirstMultiIndex->pIndex[2].u4_ULongValue,
             &(pNextMultiIndex->pIndex[2].u4_ULongValue), 
             pFirstMultiIndex->pIndex[3].i4_SLongValue,
             &(pNextMultiIndex->pIndex[3].i4_SLongValue),
             pFirstMultiIndex->pIndex[4].u4_ULongValue,
             &(pNextMultiIndex->pIndex[4].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    pNextMultiIndex->pIndex[2].pOctetStrValue->i4_Length = 6;
    return SNMP_SUCCESS;
}

INT4
LldpXdot1dcbxRemETSConTrafficSelectionAlgorithmGet (tSnmpIndex * pMultiIndex,
                                                    tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceLldpXdot1dcbxRemETSConTrafficSelectionAlgorithmTable (pMultiIndex->pIndex[0].u4_ULongValue, pMultiIndex->pIndex[1].i4_SLongValue, pMultiIndex->pIndex[2].u4_ULongValue, pMultiIndex->pIndex[3].i4_SLongValue, pMultiIndex->pIndex[4].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetLldpXdot1dcbxRemETSConTrafficSelectionAlgorithm
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             pMultiIndex->pIndex[2].u4_ULongValue,
             pMultiIndex->pIndex[3].i4_SLongValue,
             pMultiIndex->pIndex[4].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
GetNextIndexLldpXdot1dcbxRemETSRecoTrafficClassBandwidthTable (tSnmpIndex *
                                                               pFirstMultiIndex,
                                                               tSnmpIndex *
                                                               pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexLldpXdot1dcbxRemETSRecoTrafficClassBandwidthTable
            (&(pNextMultiIndex->pIndex[0].u4_ULongValue),
             &(pNextMultiIndex->pIndex[1].i4_SLongValue),
             &(pNextMultiIndex->pIndex[2].u4_ULongValue),
             &(pNextMultiIndex->pIndex[3].i4_SLongValue),
             &(pNextMultiIndex->pIndex[4].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexLldpXdot1dcbxRemETSRecoTrafficClassBandwidthTable
            (pFirstMultiIndex->pIndex[0].u4_ULongValue,
             &(pNextMultiIndex->pIndex[0].u4_ULongValue),
             pFirstMultiIndex->pIndex[1].i4_SLongValue,
             &(pNextMultiIndex->pIndex[1].i4_SLongValue),
             pFirstMultiIndex->pIndex[2].u4_ULongValue,
             &(pNextMultiIndex->pIndex[2].u4_ULongValue), 
             pFirstMultiIndex->pIndex[3].i4_SLongValue,
             &(pNextMultiIndex->pIndex[3].i4_SLongValue),
             pFirstMultiIndex->pIndex[4].u4_ULongValue,
             &(pNextMultiIndex->pIndex[4].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    pNextMultiIndex->pIndex[2].pOctetStrValue->i4_Length = 6;
    return SNMP_SUCCESS;
}

INT4
LldpXdot1dcbxRemETSRecoTrafficClassBandwidthGet (tSnmpIndex * pMultiIndex,
                                                 tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceLldpXdot1dcbxRemETSRecoTrafficClassBandwidthTable (pMultiIndex->pIndex[0].u4_ULongValue, pMultiIndex->pIndex[1].i4_SLongValue, pMultiIndex->pIndex[2].u4_ULongValue, pMultiIndex->pIndex[3].i4_SLongValue, pMultiIndex->pIndex[4].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetLldpXdot1dcbxRemETSRecoTrafficClassBandwidth
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             pMultiIndex->pIndex[2].u4_ULongValue,
              pMultiIndex->pIndex[3].i4_SLongValue,
             pMultiIndex->pIndex[4].u4_ULongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
GetNextIndexLldpXdot1dcbxRemETSRecoTrafficSelectionAlgorithmTable (tSnmpIndex *
                                                                   pFirstMultiIndex,
                                                                   tSnmpIndex *
                                                                   pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexLldpXdot1dcbxRemETSRecoTrafficSelectionAlgorithmTable (&(pNextMultiIndex->pIndex[0].u4_ULongValue), &(pNextMultiIndex->pIndex[1].i4_SLongValue), &(pNextMultiIndex->pIndex[2].u4_ULongValue), &(pNextMultiIndex->pIndex[3].i4_SLongValue), &(pNextMultiIndex->pIndex[4].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexLldpXdot1dcbxRemETSRecoTrafficSelectionAlgorithmTable
            (pFirstMultiIndex->pIndex[0].u4_ULongValue,
             &(pNextMultiIndex->pIndex[0].u4_ULongValue),
             pFirstMultiIndex->pIndex[1].i4_SLongValue,
             &(pNextMultiIndex->pIndex[1].i4_SLongValue),
             pFirstMultiIndex->pIndex[2].u4_ULongValue,
             &(pNextMultiIndex->pIndex[2].u4_ULongValue),
             pFirstMultiIndex->pIndex[3].i4_SLongValue,
             &(pNextMultiIndex->pIndex[3].i4_SLongValue),
             pFirstMultiIndex->pIndex[4].u4_ULongValue,
             &(pNextMultiIndex->pIndex[4].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    pNextMultiIndex->pIndex[2].pOctetStrValue->i4_Length = 6;
    return SNMP_SUCCESS;
}

INT4
LldpXdot1dcbxRemETSRecoTrafficSelectionAlgorithmGet (tSnmpIndex * pMultiIndex,
                                                     tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceLldpXdot1dcbxRemETSRecoTrafficSelectionAlgorithmTable (pMultiIndex->pIndex[0].u4_ULongValue, pMultiIndex->pIndex[1].i4_SLongValue, pMultiIndex->pIndex[2].u4_ULongValue, pMultiIndex->pIndex[3].i4_SLongValue, pMultiIndex->pIndex[4].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetLldpXdot1dcbxRemETSRecoTrafficSelectionAlgorithm
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             pMultiIndex->pIndex[2].u4_ULongValue,
             pMultiIndex->pIndex[3].i4_SLongValue,
             pMultiIndex->pIndex[4].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
GetNextIndexLldpXdot1dcbxRemPFCBasicTable (tSnmpIndex * pFirstMultiIndex,
                                           tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexLldpXdot1dcbxRemPFCBasicTable
            (&(pNextMultiIndex->pIndex[0].u4_ULongValue),
             &(pNextMultiIndex->pIndex[1].i4_SLongValue),
             &(pNextMultiIndex->pIndex[2].u4_ULongValue),
             &(pNextMultiIndex->pIndex[3].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexLldpXdot1dcbxRemPFCBasicTable
            (pFirstMultiIndex->pIndex[0].u4_ULongValue,
             &(pNextMultiIndex->pIndex[0].u4_ULongValue),
             pFirstMultiIndex->pIndex[1].i4_SLongValue,
             &(pNextMultiIndex->pIndex[1].i4_SLongValue),
             pFirstMultiIndex->pIndex[2].u4_ULongValue,
             &(pNextMultiIndex->pIndex[2].u4_ULongValue),
             pFirstMultiIndex->pIndex[3].i4_SLongValue,
             &(pNextMultiIndex->pIndex[3].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    pNextMultiIndex->pIndex[2].pOctetStrValue->i4_Length = 6;
    return SNMP_SUCCESS;
}

INT4
LldpXdot1dcbxRemPFCWillingGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceLldpXdot1dcbxRemPFCBasicTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetLldpXdot1dcbxRemPFCWilling
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             pMultiIndex->pIndex[2].u4_ULongValue, 
             pMultiIndex->pIndex[3].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
LldpXdot1dcbxRemPFCMBCGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceLldpXdot1dcbxRemPFCBasicTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetLldpXdot1dcbxRemPFCMBC (pMultiIndex->pIndex[0].u4_ULongValue,
                                          pMultiIndex->pIndex[1].i4_SLongValue,
                                          pMultiIndex->pIndex[2].u4_ULongValue,
                                          pMultiIndex->pIndex[3].i4_SLongValue,
                                          &(pMultiData->i4_SLongValue)));

}

INT4
LldpXdot1dcbxRemPFCCapGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceLldpXdot1dcbxRemPFCBasicTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetLldpXdot1dcbxRemPFCCap (pMultiIndex->pIndex[0].u4_ULongValue,
                                          pMultiIndex->pIndex[1].i4_SLongValue,
                                           pMultiIndex->pIndex[2].u4_ULongValue,
                                          pMultiIndex->pIndex[3].i4_SLongValue,
                                          &(pMultiData->u4_ULongValue)));

}

INT4
GetNextIndexLldpXdot1dcbxRemPFCEnableTable (tSnmpIndex * pFirstMultiIndex,
                                            tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexLldpXdot1dcbxRemPFCEnableTable
            (&(pNextMultiIndex->pIndex[0].u4_ULongValue),
             &(pNextMultiIndex->pIndex[1].i4_SLongValue),
             &(pNextMultiIndex->pIndex[2].u4_ULongValue),
             &(pNextMultiIndex->pIndex[3].i4_SLongValue),
             &(pNextMultiIndex->pIndex[4].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexLldpXdot1dcbxRemPFCEnableTable
            (pFirstMultiIndex->pIndex[0].u4_ULongValue,
             &(pNextMultiIndex->pIndex[0].u4_ULongValue),
             pFirstMultiIndex->pIndex[1].i4_SLongValue,
             &(pNextMultiIndex->pIndex[1].i4_SLongValue),
             pFirstMultiIndex->pIndex[2].u4_ULongValue,
             &(pNextMultiIndex->pIndex[2].u4_ULongValue),
             pFirstMultiIndex->pIndex[3].i4_SLongValue,
             &(pNextMultiIndex->pIndex[3].i4_SLongValue),
             pFirstMultiIndex->pIndex[4].u4_ULongValue,
             &(pNextMultiIndex->pIndex[4].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    pNextMultiIndex->pIndex[2].pOctetStrValue->i4_Length = 6;
    return SNMP_SUCCESS;
}

INT4
LldpXdot1dcbxRemPFCEnableEnabledGet (tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceLldpXdot1dcbxRemPFCEnableTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].i4_SLongValue,
         pMultiIndex->pIndex[4].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetLldpXdot1dcbxRemPFCEnableEnabled
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             pMultiIndex->pIndex[2].u4_ULongValue, 
             pMultiIndex->pIndex[3].i4_SLongValue,
             pMultiIndex->pIndex[4].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
GetNextIndexLldpXdot1dcbxRemApplicationPriorityAppTable (tSnmpIndex *
                                                         pFirstMultiIndex,
                                                         tSnmpIndex *
                                                         pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexLldpXdot1dcbxRemApplicationPriorityAppTable
            (&(pNextMultiIndex->pIndex[0].u4_ULongValue),
             &(pNextMultiIndex->pIndex[1].i4_SLongValue),
             &(pNextMultiIndex->pIndex[2].u4_ULongValue),
             &(pNextMultiIndex->pIndex[3].i4_SLongValue),
             &(pNextMultiIndex->pIndex[4].i4_SLongValue),
             &(pNextMultiIndex->pIndex[5].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexLldpXdot1dcbxRemApplicationPriorityAppTable
            (pFirstMultiIndex->pIndex[0].u4_ULongValue,
             &(pNextMultiIndex->pIndex[0].u4_ULongValue),
             pFirstMultiIndex->pIndex[1].i4_SLongValue,
             &(pNextMultiIndex->pIndex[1].i4_SLongValue),
             pFirstMultiIndex->pIndex[2].u4_ULongValue,
             &(pNextMultiIndex->pIndex[2].u4_ULongValue),
             pFirstMultiIndex->pIndex[3].i4_SLongValue,
             &(pNextMultiIndex->pIndex[3].i4_SLongValue),
             pFirstMultiIndex->pIndex[4].i4_SLongValue,
             &(pNextMultiIndex->pIndex[4].i4_SLongValue),
             pFirstMultiIndex->pIndex[5].u4_ULongValue,
             &(pNextMultiIndex->pIndex[5].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    pNextMultiIndex->pIndex[2].pOctetStrValue->i4_Length = 6;
    return SNMP_SUCCESS;
}

INT4
LldpXdot1dcbxRemApplicationPriorityAEPriorityGet (tSnmpIndex * pMultiIndex,
                                                  tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceLldpXdot1dcbxRemApplicationPriorityAppTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].i4_SLongValue,
         pMultiIndex->pIndex[4].i4_SLongValue,
         pMultiIndex->pIndex[5].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetLldpXdot1dcbxRemApplicationPriorityAEPriority
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             pMultiIndex->pIndex[2].u4_ULongValue, 
             pMultiIndex->pIndex[3].i4_SLongValue,
             pMultiIndex->pIndex[4].i4_SLongValue,
             pMultiIndex->pIndex[5].u4_ULongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
GetNextIndexLldpXdot1dcbxAdminETSBasicConfigurationTable (tSnmpIndex *
                                                          pFirstMultiIndex,
                                                          tSnmpIndex *
                                                          pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexLldpXdot1dcbxAdminETSBasicConfigurationTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexLldpXdot1dcbxAdminETSBasicConfigurationTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
LldpXdot1dcbxAdminETSConCreditBasedShaperSupportGet (tSnmpIndex * pMultiIndex,
                                                     tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceLldpXdot1dcbxAdminETSBasicConfigurationTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetLldpXdot1dcbxAdminETSConCreditBasedShaperSupport
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
LldpXdot1dcbxAdminETSConTrafficClassesSupportedGet (tSnmpIndex * pMultiIndex,
                                                    tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceLldpXdot1dcbxAdminETSBasicConfigurationTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetLldpXdot1dcbxAdminETSConTrafficClassesSupported
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
LldpXdot1dcbxAdminETSConWillingGet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceLldpXdot1dcbxAdminETSBasicConfigurationTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetLldpXdot1dcbxAdminETSConWilling
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
LldpXdot1dcbxAdminETSConWillingSet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    return (nmhSetLldpXdot1dcbxAdminETSConWilling
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
LldpXdot1dcbxAdminETSConWillingTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    return (nmhTestv2LldpXdot1dcbxAdminETSConWilling (pu4Error,
                                                      pMultiIndex->pIndex[0].
                                                      i4_SLongValue,
                                                      pMultiData->
                                                      i4_SLongValue));

}

INT4
LldpXdot1dcbxAdminETSBasicConfigurationTableDep (UINT4 *pu4Error,
                                                 tSnmpIndexList *
                                                 pSnmpIndexList,
                                                 tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2LldpXdot1dcbxAdminETSBasicConfigurationTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexLldpXdot1dcbxAdminETSConPriorityAssignmentTable (tSnmpIndex *
                                                             pFirstMultiIndex,
                                                             tSnmpIndex *
                                                             pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexLldpXdot1dcbxAdminETSConPriorityAssignmentTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue),
             &(pNextMultiIndex->pIndex[1].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexLldpXdot1dcbxAdminETSConPriorityAssignmentTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue),
             pFirstMultiIndex->pIndex[1].u4_ULongValue,
             &(pNextMultiIndex->pIndex[1].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
LldpXdot1dcbxAdminETSConPriTrafficClassGet (tSnmpIndex * pMultiIndex,
                                            tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceLldpXdot1dcbxAdminETSConPriorityAssignmentTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetLldpXdot1dcbxAdminETSConPriTrafficClass
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
LldpXdot1dcbxAdminETSConPriTrafficClassSet (tSnmpIndex * pMultiIndex,
                                            tRetVal * pMultiData)
{
    return (nmhSetLldpXdot1dcbxAdminETSConPriTrafficClass
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].u4_ULongValue, pMultiData->u4_ULongValue));

}

INT4
LldpXdot1dcbxAdminETSConPriTrafficClassTest (UINT4 *pu4Error,
                                             tSnmpIndex * pMultiIndex,
                                             tRetVal * pMultiData)
{
    return (nmhTestv2LldpXdot1dcbxAdminETSConPriTrafficClass (pu4Error,
                                                              pMultiIndex->
                                                              pIndex[0].
                                                              i4_SLongValue,
                                                              pMultiIndex->
                                                              pIndex[1].
                                                              u4_ULongValue,
                                                              pMultiData->
                                                              u4_ULongValue));

}

INT4
LldpXdot1dcbxAdminETSConPriorityAssignmentTableDep (UINT4 *pu4Error,
                                                    tSnmpIndexList *
                                                    pSnmpIndexList,
                                                    tSNMP_VAR_BIND *
                                                    pSnmpvarbinds)
{
    return (nmhDepv2LldpXdot1dcbxAdminETSConPriorityAssignmentTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexLldpXdot1dcbxAdminETSConTrafficClassBandwidthTable (tSnmpIndex *
                                                                pFirstMultiIndex,
                                                                tSnmpIndex *
                                                                pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexLldpXdot1dcbxAdminETSConTrafficClassBandwidthTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue),
             &(pNextMultiIndex->pIndex[1].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexLldpXdot1dcbxAdminETSConTrafficClassBandwidthTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue),
             pFirstMultiIndex->pIndex[1].u4_ULongValue,
             &(pNextMultiIndex->pIndex[1].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
LldpXdot1dcbxAdminETSConTrafficClassBandwidthGet (tSnmpIndex * pMultiIndex,
                                                  tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceLldpXdot1dcbxAdminETSConTrafficClassBandwidthTable (pMultiIndex->pIndex[0].i4_SLongValue, pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetLldpXdot1dcbxAdminETSConTrafficClassBandwidth
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
LldpXdot1dcbxAdminETSConTrafficClassBandwidthSet (tSnmpIndex * pMultiIndex,
                                                  tRetVal * pMultiData)
{
    return (nmhSetLldpXdot1dcbxAdminETSConTrafficClassBandwidth
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].u4_ULongValue, pMultiData->u4_ULongValue));

}

INT4
LldpXdot1dcbxAdminETSConTrafficClassBandwidthTest (UINT4 *pu4Error,
                                                   tSnmpIndex * pMultiIndex,
                                                   tRetVal * pMultiData)
{
    return (nmhTestv2LldpXdot1dcbxAdminETSConTrafficClassBandwidth (pu4Error,
                                                                    pMultiIndex->
                                                                    pIndex[0].
                                                                    i4_SLongValue,
                                                                    pMultiIndex->
                                                                    pIndex[1].
                                                                    u4_ULongValue,
                                                                    pMultiData->
                                                                    u4_ULongValue));

}

INT4
LldpXdot1dcbxAdminETSConTrafficClassBandwidthTableDep (UINT4 *pu4Error,
                                                       tSnmpIndexList *
                                                       pSnmpIndexList,
                                                       tSNMP_VAR_BIND *
                                                       pSnmpvarbinds)
{
    return (nmhDepv2LldpXdot1dcbxAdminETSConTrafficClassBandwidthTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexLldpXdot1dcbxAdminETSConTrafficSelectionAlgorithmTable (tSnmpIndex *
                                                                    pFirstMultiIndex,
                                                                    tSnmpIndex *
                                                                    pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexLldpXdot1dcbxAdminETSConTrafficSelectionAlgorithmTable (&(pNextMultiIndex->pIndex[0].i4_SLongValue), &(pNextMultiIndex->pIndex[1].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexLldpXdot1dcbxAdminETSConTrafficSelectionAlgorithmTable (pFirstMultiIndex->pIndex[0].i4_SLongValue, &(pNextMultiIndex->pIndex[0].i4_SLongValue), pFirstMultiIndex->pIndex[1].u4_ULongValue, &(pNextMultiIndex->pIndex[1].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
LldpXdot1dcbxAdminETSConTrafficSelectionAlgorithmGet (tSnmpIndex * pMultiIndex,
                                                      tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceLldpXdot1dcbxAdminETSConTrafficSelectionAlgorithmTable (pMultiIndex->pIndex[0].i4_SLongValue, pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetLldpXdot1dcbxAdminETSConTrafficSelectionAlgorithm
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
LldpXdot1dcbxAdminETSConTrafficSelectionAlgorithmSet (tSnmpIndex * pMultiIndex,
                                                      tRetVal * pMultiData)
{
    return (nmhSetLldpXdot1dcbxAdminETSConTrafficSelectionAlgorithm
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].u4_ULongValue, pMultiData->i4_SLongValue));

}

INT4
LldpXdot1dcbxAdminETSConTrafficSelectionAlgorithmTest (UINT4 *pu4Error,
                                                       tSnmpIndex * pMultiIndex,
                                                       tRetVal * pMultiData)
{
    return (nmhTestv2LldpXdot1dcbxAdminETSConTrafficSelectionAlgorithm
            (pu4Error, pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].u4_ULongValue, pMultiData->i4_SLongValue));

}

INT4
LldpXdot1dcbxAdminETSConTrafficSelectionAlgorithmTableDep (UINT4 *pu4Error,
                                                           tSnmpIndexList *
                                                           pSnmpIndexList,
                                                           tSNMP_VAR_BIND *
                                                           pSnmpvarbinds)
{
    return (nmhDepv2LldpXdot1dcbxAdminETSConTrafficSelectionAlgorithmTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexLldpXdot1dcbxAdminETSRecoTrafficClassBandwidthTable (tSnmpIndex *
                                                                 pFirstMultiIndex,
                                                                 tSnmpIndex *
                                                                 pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexLldpXdot1dcbxAdminETSRecoTrafficClassBandwidthTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue),
             &(pNextMultiIndex->pIndex[1].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexLldpXdot1dcbxAdminETSRecoTrafficClassBandwidthTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue),
             pFirstMultiIndex->pIndex[1].u4_ULongValue,
             &(pNextMultiIndex->pIndex[1].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
LldpXdot1dcbxAdminETSRecoTrafficClassBandwidthGet (tSnmpIndex * pMultiIndex,
                                                   tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceLldpXdot1dcbxAdminETSRecoTrafficClassBandwidthTable (pMultiIndex->pIndex[0].i4_SLongValue, pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetLldpXdot1dcbxAdminETSRecoTrafficClassBandwidth
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
LldpXdot1dcbxAdminETSRecoTrafficClassBandwidthSet (tSnmpIndex * pMultiIndex,
                                                   tRetVal * pMultiData)
{
    return (nmhSetLldpXdot1dcbxAdminETSRecoTrafficClassBandwidth
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].u4_ULongValue, pMultiData->u4_ULongValue));

}

INT4
LldpXdot1dcbxAdminETSRecoTrafficClassBandwidthTest (UINT4 *pu4Error,
                                                    tSnmpIndex * pMultiIndex,
                                                    tRetVal * pMultiData)
{
    return (nmhTestv2LldpXdot1dcbxAdminETSRecoTrafficClassBandwidth (pu4Error,
                                                                     pMultiIndex->
                                                                     pIndex[0].
                                                                     i4_SLongValue,
                                                                     pMultiIndex->
                                                                     pIndex[1].
                                                                     u4_ULongValue,
                                                                     pMultiData->
                                                                     u4_ULongValue));

}

INT4
LldpXdot1dcbxAdminETSRecoTrafficClassBandwidthTableDep (UINT4 *pu4Error,
                                                        tSnmpIndexList *
                                                        pSnmpIndexList,
                                                        tSNMP_VAR_BIND *
                                                        pSnmpvarbinds)
{
    return (nmhDepv2LldpXdot1dcbxAdminETSRecoTrafficClassBandwidthTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexLldpXdot1dcbxAdminETSRecoTrafficSelectionAlgorithmTable (tSnmpIndex
                                                                     *
                                                                     pFirstMultiIndex,
                                                                     tSnmpIndex
                                                                     *
                                                                     pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexLldpXdot1dcbxAdminETSRecoTrafficSelectionAlgorithmTable (&(pNextMultiIndex->pIndex[0].i4_SLongValue), &(pNextMultiIndex->pIndex[1].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexLldpXdot1dcbxAdminETSRecoTrafficSelectionAlgorithmTable (pFirstMultiIndex->pIndex[0].i4_SLongValue, &(pNextMultiIndex->pIndex[0].i4_SLongValue), pFirstMultiIndex->pIndex[1].u4_ULongValue, &(pNextMultiIndex->pIndex[1].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
LldpXdot1dcbxAdminETSRecoTrafficSelectionAlgorithmGet (tSnmpIndex * pMultiIndex,
                                                       tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceLldpXdot1dcbxAdminETSRecoTrafficSelectionAlgorithmTable (pMultiIndex->pIndex[0].i4_SLongValue, pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetLldpXdot1dcbxAdminETSRecoTrafficSelectionAlgorithm
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
LldpXdot1dcbxAdminETSRecoTrafficSelectionAlgorithmSet (tSnmpIndex * pMultiIndex,
                                                       tRetVal * pMultiData)
{
    return (nmhSetLldpXdot1dcbxAdminETSRecoTrafficSelectionAlgorithm
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].u4_ULongValue, pMultiData->i4_SLongValue));

}

INT4
LldpXdot1dcbxAdminETSRecoTrafficSelectionAlgorithmTest (UINT4 *pu4Error,
                                                        tSnmpIndex *
                                                        pMultiIndex,
                                                        tRetVal * pMultiData)
{
    return (nmhTestv2LldpXdot1dcbxAdminETSRecoTrafficSelectionAlgorithm
            (pu4Error, pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].u4_ULongValue, pMultiData->i4_SLongValue));

}

INT4
LldpXdot1dcbxAdminETSRecoTrafficSelectionAlgorithmTableDep (UINT4 *pu4Error,
                                                            tSnmpIndexList *
                                                            pSnmpIndexList,
                                                            tSNMP_VAR_BIND *
                                                            pSnmpvarbinds)
{
    return (nmhDepv2LldpXdot1dcbxAdminETSRecoTrafficSelectionAlgorithmTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexLldpXdot1dcbxAdminPFCBasicTable (tSnmpIndex * pFirstMultiIndex,
                                             tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexLldpXdot1dcbxAdminPFCBasicTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexLldpXdot1dcbxAdminPFCBasicTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
LldpXdot1dcbxAdminPFCWillingGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceLldpXdot1dcbxAdminPFCBasicTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetLldpXdot1dcbxAdminPFCWilling
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
LldpXdot1dcbxAdminPFCMBCGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceLldpXdot1dcbxAdminPFCBasicTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetLldpXdot1dcbxAdminPFCMBC
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
LldpXdot1dcbxAdminPFCCapGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceLldpXdot1dcbxAdminPFCBasicTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetLldpXdot1dcbxAdminPFCCap
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
LldpXdot1dcbxAdminPFCWillingSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetLldpXdot1dcbxAdminPFCWilling
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
LldpXdot1dcbxAdminPFCWillingTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    return (nmhTestv2LldpXdot1dcbxAdminPFCWilling (pu4Error,
                                                   pMultiIndex->pIndex[0].
                                                   i4_SLongValue,
                                                   pMultiData->i4_SLongValue));

}

INT4
LldpXdot1dcbxAdminPFCBasicTableDep (UINT4 *pu4Error,
                                    tSnmpIndexList * pSnmpIndexList,
                                    tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2LldpXdot1dcbxAdminPFCBasicTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexLldpXdot1dcbxAdminPFCEnableTable (tSnmpIndex * pFirstMultiIndex,
                                              tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexLldpXdot1dcbxAdminPFCEnableTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue),
             &(pNextMultiIndex->pIndex[1].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexLldpXdot1dcbxAdminPFCEnableTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue),
             pFirstMultiIndex->pIndex[1].u4_ULongValue,
             &(pNextMultiIndex->pIndex[1].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
LldpXdot1dcbxAdminPFCEnableEnabledGet (tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceLldpXdot1dcbxAdminPFCEnableTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetLldpXdot1dcbxAdminPFCEnableEnabled
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
LldpXdot1dcbxAdminPFCEnableEnabledSet (tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    return (nmhSetLldpXdot1dcbxAdminPFCEnableEnabled
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].u4_ULongValue, pMultiData->i4_SLongValue));

}

INT4
LldpXdot1dcbxAdminPFCEnableEnabledTest (UINT4 *pu4Error,
                                        tSnmpIndex * pMultiIndex,
                                        tRetVal * pMultiData)
{
    return (nmhTestv2LldpXdot1dcbxAdminPFCEnableEnabled (pu4Error,
                                                         pMultiIndex->pIndex[0].
                                                         i4_SLongValue,
                                                         pMultiIndex->pIndex[1].
                                                         u4_ULongValue,
                                                         pMultiData->
                                                         i4_SLongValue));

}

INT4
LldpXdot1dcbxAdminPFCEnableTableDep (UINT4 *pu4Error,
                                     tSnmpIndexList * pSnmpIndexList,
                                     tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2LldpXdot1dcbxAdminPFCEnableTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexLldpXdot1dcbxAdminApplicationPriorityAppTable (tSnmpIndex *
                                                           pFirstMultiIndex,
                                                           tSnmpIndex *
                                                           pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexLldpXdot1dcbxAdminApplicationPriorityAppTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue),
             &(pNextMultiIndex->pIndex[1].i4_SLongValue),
             &(pNextMultiIndex->pIndex[2].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexLldpXdot1dcbxAdminApplicationPriorityAppTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue),
             pFirstMultiIndex->pIndex[1].i4_SLongValue,
             &(pNextMultiIndex->pIndex[1].i4_SLongValue),
             pFirstMultiIndex->pIndex[2].u4_ULongValue,
             &(pNextMultiIndex->pIndex[2].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
LldpXdot1dcbxAdminApplicationPriorityAEPriorityGet (tSnmpIndex * pMultiIndex,
                                                    tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceLldpXdot1dcbxAdminApplicationPriorityAppTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetLldpXdot1dcbxAdminApplicationPriorityAEPriority
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             pMultiIndex->pIndex[2].u4_ULongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
LldpXdot1dcbxAdminApplicationPriorityAEPrioritySet (tSnmpIndex * pMultiIndex,
                                                    tRetVal * pMultiData)
{
    return (nmhSetLldpXdot1dcbxAdminApplicationPriorityAEPriority
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             pMultiIndex->pIndex[2].u4_ULongValue, pMultiData->u4_ULongValue));

}

INT4
LldpXdot1dcbxAdminApplicationPriorityAEPriorityTest (UINT4 *pu4Error,
                                                     tSnmpIndex * pMultiIndex,
                                                     tRetVal * pMultiData)
{
    return (nmhTestv2LldpXdot1dcbxAdminApplicationPriorityAEPriority (pu4Error,
                                                                      pMultiIndex->
                                                                      pIndex[0].
                                                                      i4_SLongValue,
                                                                      pMultiIndex->
                                                                      pIndex[1].
                                                                      i4_SLongValue,
                                                                      pMultiIndex->
                                                                      pIndex[2].
                                                                      u4_ULongValue,
                                                                      pMultiData->
                                                                      u4_ULongValue));

}

INT4
LldpXdot1dcbxAdminApplicationPriorityAppTableDep (UINT4 *pu4Error,
                                                  tSnmpIndexList *
                                                  pSnmpIndexList,
                                                  tSNMP_VAR_BIND *
                                                  pSnmpvarbinds)
{
    return (nmhDepv2LldpXdot1dcbxAdminApplicationPriorityAppTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}
