/*************************************************************************
 * Copyright (C) 2010 Aricent Inc . All Rights Reserved
 * $Id: apputl.c,v 1.20 2017/08/24 11:59:43 siva Exp $
 * Description: This file contains Application Priority utility function.
****************************************************************************/
#include "dcbxinc.h"

/***************************************************************************
 *  FUNCTION NAME : AppPriUtlFormAndSendAppPriTLV
 * 
 *  DESCRIPTION   : This utility is used to from and send Application Priority
 *                  TLV.
 * 
 *  INPUT         : pPortEntry - Application Priority Port Entry
 *                  u1MsgType - Message Type - Port Reg/Port Update.
 * 
 *  OUTPUT        : None
 * 
 *  RETURNS       : None
 * 
 * **************************************************************************/
PUBLIC VOID
AppPriUtlFormAndSendAppPriTLV (tAppPriPortEntry * pPortEntry, UINT1 u1MsgType)
{

    tDcbxAppRegInfo     DcbxAppPortMsg;
    tAppPriMappingEntry *pAppPriMappingEntry = NULL;
    UINT1              *pu1Temp = NULL;
    UINT1               au1TlvBuf[APP_PRI_TLV_LEN] = { DCBX_ZERO };
    UINT4               u4Count = DCBX_ZERO;
    UINT2               u2TlvHeader = DCBX_ZERO;
    UINT2               u2TlvLength = DCBX_ZERO;
    UINT2               u2Protocol = DCBX_ZERO;
    UINT1               u1Value = DCBX_ZERO;
    UINT1               u1Temp = DCBX_ZERO;
    UINT1               u1Selector = DCBX_ZERO;

    MEMSET (&DcbxAppPortMsg, DCBX_ZERO, sizeof (tDcbxAppRegInfo));

    DcbxAppPortMsg.pApplCallBackFn = AppPriApiTlvCallBackFn;

    /* Calculate the number of application priority entries 
     * to be filled in TLV */
    for (u1Selector = DCBX_ONE;
         u1Selector <= APP_PRI_MAX_SELECTOR; u1Selector++)
    {
        TMO_SLL_Scan ((APP_PRI_LOC_TBL (pPortEntry, u1Selector - 1)),
                      pAppPriMappingEntry, tAppPriMappingEntry *)
        {
            if (pAppPriMappingEntry->u1RowStatus == ACTIVE)
            {
                u4Count++;
            }
        }
    }

    /* Refer : Section 38.5.5; IEEE 802.1Qaz/D1.2 */
    DcbxAppPortMsg.u2TlvLen = (UINT2) (APP_PRI_TLV_TYPE_TLV_LEN_SIZE +
                                       APP_PRI_TLV_INFO_STR_HDR_LEN +
                                       (u4Count * APP_PRI_TABLE_ENTRY_LEN));

    /* Form the TLV Header with the Preformed TLV */
    pu1Temp = au1TlvBuf;

    u2TlvLength = (UINT2) (APP_PRI_TLV_INFO_STR_HDR_LEN +
                           (APP_PRI_TABLE_ENTRY_LEN * u4Count));

    /* This macro will fill TLV type into first 7 bits of variable + 
     * Length into next 9 bits of the variable */
    DCBX_PUT_HEADER_LENGTH ((UINT2) APP_PRI_TLV_TYPE,
                            u2TlvLength, &u2TlvHeader);

    /* Fill the TLV header in to buffer */
    DCBX_PUT_2BYTE (pu1Temp, u2TlvHeader);
    /* Fill the OUI field in to buffer */
    DCBX_PUT_OUI (pu1Temp, gau1DcbxOUI);
    /* Fill the sub type field in to buffer */
    DCBX_PUT_1BYTE (pu1Temp, (UINT1) APP_PRI_TLV_SUBTYPE);

    /* This byte will be filled in the order
     * Willing Reserved
     * 1 bit    7 bits  */
    if (pPortEntry->AppPriAdmPortInfo.u1AppPriAdmWilling == APP_PRI_ENABLED)
    {
        u1Value = APP_PRI_WILLING_MASK;
    }

    /* Fill the reserved field in to buffer */
    DCBX_PUT_1BYTE (pu1Temp, u1Value);

    /* Filling the application priority table entries into buffer */
    for (u1Selector = DCBX_ONE; u1Selector <= APP_PRI_MAX_SELECTOR;
         u1Selector++)
    {
        TMO_SLL_Scan ((APP_PRI_LOC_TBL (pPortEntry, u1Selector - 1)),
                      pAppPriMappingEntry, tAppPriMappingEntry *)
        {
            if (pAppPriMappingEntry->u1RowStatus == ACTIVE)
            {

                /* This  byte will be filled in the order 
                 * Priority Reserved SelectorField
                 *  3 bits    2 bits  3 bits      */
                u1Value = u1Selector;
                u1Temp = pAppPriMappingEntry->u1AppPriPriority;
                u1Temp = (UINT1) (u1Temp << APP_PRI_PRIORITY_POSITION);
                u1Value = u1Value | u1Temp;
                DCBX_PUT_1BYTE (pu1Temp, u1Value);

                /* Next 2 bytes contain the protocol ID */
                u2Protocol = (UINT2) pAppPriMappingEntry->i4AppPriProtocol;
                DCBX_PUT_2BYTE (pu1Temp, u2Protocol);
            }
        }                        /* end of SLL SCAN */
    }                            /* end of for */

    DcbxAppPortMsg.pu1ApplTlv = au1TlvBuf;

    /* Get the Application ID to be filled in the DCBX Appl port structure */
    APP_PRI_GET_APPL_ID (DcbxAppPortMsg.DcbxAppId);

    DCBX_TRC_ARG1 (DCBX_TLV_TRC, "In AppPriUtlFormAndSendConfigTLV: Posting"
                   " Application Priority TLV to DCBX!!! for the port %d.\r\n",
                   pPortEntry->u4IfIndex);

    /* Update TLV Tx counters */
    pPortEntry->u4AppPriTLVTxCount = pPortEntry->u4AppPriTLVTxCount + 1;

    /* Packet Dump to Console */
    if (gDcbxGlobalInfo.u4DCBXTrcFlag & DCBX_TLV_TRC)
    {
        DcbxPktDump (au1TlvBuf, DcbxAppPortMsg.u2TlvLen, DCBX_TX);
    }

    AppPriPortPostMsgToDCBX (u1MsgType, pPortEntry->u4IfIndex, &DcbxAppPortMsg);

    /* Send Counter update Info to Stand by node */
    DcbxRedSendDynamicAppPriCounterInfo (pPortEntry,
                                         DCBX_RED_APP_PRI_TX_COUNTER);
    return;
}

/***************************************************************************
 *  FUNCTION NAME : AppPriUtlHandleTLV
 * 
 *  DESCRIPTION   : This utility is used to handle and process the Applciation
 *                  Priority TLV
 * 
 *  INPUT         : pPortEntry - Application Prioirty Port Entry.
 *                  ApplTlvParam - Appl TLV information.
 * 
 *  OUTPUT        : None
 * 
 *  RETURNS       : None
 * 
 * **************************************************************************/
PUBLIC VOID
AppPriUtlHandleTLV (tAppPriPortEntry * pPortEntry,
                    tApplTlvParam * pApplTlvParam)
{
    tDcbxTrapMsg        NotifyInfo;
    tAppPriMappingEntry *pAppPriMappingEntry = NULL;
    UINT1              *pu1Temp = NULL;
    UINT4               u4ProtocolCount = DCBX_ZERO;
    INT4                i4Selector = DCBX_ZERO;
    UINT2               u2Header = DCBX_ZERO;
    UINT2               u2Type = DCBX_ZERO;
    UINT2               u2Length = DCBX_ZERO;
    UINT2               u2Protocol = DCBX_ZERO;
    UINT1               u1Value = DCBX_ZERO;
    UINT1               u1Willing = APP_PRI_DISABLED;
    UINT1               u1Priority = DCBX_ZERO;

    MEMSET (&NotifyInfo, DCBX_ZERO, sizeof (tDcbxTrapMsg));
    SET_DCBX_STATUS (pPortEntry, pPortEntry->u1AppPriDcbxSemType,
                     DCBX_RED_APP_PRI_DCBX_STATUS_INFO, DCBX_STATUS_UNKNOWN);

    u2Length = pApplTlvParam->u2RxTlvLen;
    pu1Temp = pApplTlvParam->au1DcbxTlv;

    /* Get TLV Header */
    DCBX_GET_2BYTE (pu1Temp, u2Header);
    u2Type = u2Header & APP_PRI_TLV_TYPE_MASK;

    /* Validate TLV Length */
    u2Length = u2Header & APP_PRI_TLV_LEN_MASK;

    /* Validate TLV Type - All the 7 bits should be set = 127 */
    if (!(u2Type == APP_PRI_TLV_TYPE_MASK))
    {
        DCBX_TRC (DCBX_TLV_TRC | DCBX_FAILURE_TRC, "AppPriUtlHandleTLV:"
                  "Application Priority Received TLV Type "
                  "is incorrect !!!\r\n");
        /* Increment the Application Priority TLV Error Counter */
        pPortEntry->u4AppPriTLVRxError++;
        /* Send Counter Update Info to Standby */
        DcbxRedSendDynamicAppPriCounterInfo (pPortEntry,
                                             DCBX_RED_APP_PRI_ERR_COUNTER);
        return;
    }

    /* Validation for length field */
    u2Length = (UINT2) (u2Length - APP_PRI_TLV_INFO_STR_HDR_LEN);
    if ((u2Length % APP_PRI_TABLE_ENTRY_LEN) != DCBX_ZERO)
    {
        /* If the length is not correct then do not process the TLV  */
        DCBX_TRC ((DCBX_TLV_TRC | DCBX_FAILURE_TRC), "AppPriUtlHandleTLV:"
                  "Application Priority Received TLV Length is incorrect !!!\r\n");
        /* Incremet the Application Priority TLV Error Counter */
        pPortEntry->u4AppPriTLVRxError++;
        /* Send Counter Update Info to Standby */
        DcbxRedSendDynamicAppPriCounterInfo (pPortEntry,
                                             DCBX_RED_APP_PRI_ERR_COUNTER);
        return;
    }

    /*Validate OUI in incoming packet */
    if (MEMCMP (pu1Temp, gau1DcbxOUI, DCBX_MAX_OUI_LEN) != DCBX_ZERO)
    {
        /* If the OUI is not correct then do not process the TLV  */
        DCBX_TRC (DCBX_TLV_TRC | DCBX_FAILURE_TRC, "AppPriUtlHandleTLV:"
                  "Application Priority Received OUI is incorrect !!!\r\n");
        /* Incremet the Application Priority TLV Error Counter */
        pPortEntry->u4AppPriTLVRxError++;
        /* Send Counter Update Info to Standby */
        DcbxRedSendDynamicAppPriCounterInfo (pPortEntry,
                                             DCBX_RED_APP_PRI_ERR_COUNTER);
        return;
    }
    pu1Temp = pu1Temp + DCBX_MAX_OUI_LEN;

    /* Get TLV SubType and validate */
    DCBX_GET_1BYTE (pu1Temp, u1Value);
    if (!(u1Value & APP_PRI_TLV_SUBTYPE_MASK))
    {
        /* If the subtype is not correct then do not process the TLV  */
        DCBX_TRC (DCBX_TLV_TRC | DCBX_FAILURE_TRC, "AppPriUtlHandleTLV:"
                  "Application Priority Received sub type is incorrect !!!\r\n");
        /* Increment the Application Priority TLV Error Counter */
        pPortEntry->u4AppPriTLVRxError++;
        /* Send Counter Update Info to Standby */
        DcbxRedSendDynamicAppPriCounterInfo (pPortEntry,
                                             DCBX_RED_APP_PRI_ERR_COUNTER);
        return;
    }

    /* The application-priority mapping entries in the TLV might
     * contain new entries to be added , 
     * existing entries to be modified, and some entries present in the 
     * TLV might not be present in the application Priority mapping table
     * in the remote port info(which means those entries
     * have to be deleted).
     * To avoid unnecessary scanning of the entire Remote list when an 
     * application-priority entry in the TLV needs to be 
     * modified/added/deleted , the entire remote table is 
     * cleared and all the entries present in the TLV are added newly
     * in the remote table */

    AppPriUtlDeleteRemoteMappingEntries (pPortEntry->u4IfIndex);

    if (pPortEntry->AppPriRemPortInfo.i4AppPriRemIndex == DCBX_ZERO)
    {
        /* Send Trap Notification for Peer Up status only if
         * the same trap is already not sent */
        NotifyInfo.u4IfIndex = pPortEntry->u4IfIndex;
        NotifyInfo.u1Module = APP_PRI_TRAP;
        NotifyInfo.unTrapMsg.u1PeerStatus = APP_PRI_ENABLED;
        DcbxSendNotification (&NotifyInfo, PEER_STATE_TRAP);
    }

    /* Get the next 1 byte  */
    DCBX_GET_1BYTE (pu1Temp, u1Value);

    /* Get the willing status from this byte */
    if (u1Value & APP_PRI_WILLING_MASK)
    {
        u1Willing = APP_PRI_ENABLED;
    }
    else
    {
        u1Willing = APP_PRI_DISABLED;
    }

    /*Update Willing Bit */
    pPortEntry->AppPriRemPortInfo.u1AppPriRemWilling = u1Willing;

    /* Get the Application Priority Entries */
    while (u2Length >= APP_PRI_TABLE_ENTRY_LEN)
    {
        u4ProtocolCount++;

        /*Get the next byte -Selector, Reserved and Priority */
        DCBX_GET_1BYTE (pu1Temp, u1Value);

        /*Get Protocol Id */
        DCBX_GET_2BYTE (pu1Temp, u2Protocol);

        i4Selector = u1Value & APP_PRI_SELECTOR_MASK;

        /* Check if the selector received is a valid selector */
        if ((i4Selector < DCBX_ONE) || (i4Selector > APP_PRI_MAX_SELECTOR))
        {
            DCBX_TRC_ARG2 ((DCBX_TLV_TRC | DCBX_FAILURE_TRC), "In %s : "
                           "Invalid Selector %d received from the peer\r\n",
                           __FUNCTION__, i4Selector);
            /* Increment the Application Priority TLV Error Counter */
            pPortEntry->u4AppPriTLVRxError++;
            /* Send Counter Update Info to Standby */
            DcbxRedSendDynamicAppPriCounterInfo (pPortEntry,
                                                 DCBX_RED_APP_PRI_ERR_COUNTER);

            return;
        }

        u1Priority = u1Value & APP_PRI_PRIORITY_MASK;
        u1Priority = u1Priority >> APP_PRI_PRIORITY_POSITION;
        pAppPriMappingEntry = AppPriUtlGetAppPriMappingEntry
            (pPortEntry->u4IfIndex, i4Selector,
             (INT4) u2Protocol, APP_PRI_REMOTE);
        if (pAppPriMappingEntry == NULL)
        {
            pAppPriMappingEntry = AppPriUtlCreateAppPriMappingTblEntry
                (pPortEntry->u4IfIndex, i4Selector,
                 (INT4) u2Protocol, APP_PRI_REMOTE);
            if (pAppPriMappingEntry == NULL)
            {
                DCBX_TRC_ARG4 (DCBX_FAILURE_TRC, "In %s : "
                               "AppPriUtlCreateAppPriMappingTblEntry "
                               "Failed for port %d selector %d Protocol %d \r\n",
                               __FUNCTION__, pPortEntry->u4IfIndex, i4Selector,
                               u2Protocol);
                return;
            }
        }
        pAppPriMappingEntry->u1AppPriPriority = u1Priority;
        pAppPriMappingEntry->u1RowStatus = ACTIVE;
        u2Length = (UINT2) (u2Length - APP_PRI_TABLE_ENTRY_LEN);

    }

    /* Update the number of protocols in last received TLV for this Port */
    pPortEntry->u4AppPriAppProtocols = u4ProtocolCount;
    /* Send Counter Update Info to Standby */
    DcbxRedSendDynamicAppPriCounterInfo (pPortEntry,
                                         DCBX_RED_APP_PRI_PROTOCOL_COUNTER);

    /* Copy the Remote index paramters to the Remote Table for this port */
    pPortEntry->AppPriRemPortInfo.i4AppPriRemIndex = pApplTlvParam->i4RemIndex;
    pPortEntry->AppPriRemPortInfo.u4AppPriRemTimeMark =
        pApplTlvParam->u4RemLastUpdateTime;
    MEMCPY (pPortEntry->AppPriRemPortInfo.au1AppPriDestRemMacAddr,
            pApplTlvParam->RemNodeMacAddr, MAC_ADDR_LEN);
    pPortEntry->AppPriRemPortInfo.u4AppPriDestRemMacIndex =
        pApplTlvParam->u4RemLocalDestMACIndex;

    /* Increment the Application Priority Rx TLV counter */
    pPortEntry->u4AppPriTLVRxCount = pPortEntry->u4AppPriTLVRxCount + 1;

    AppPriApiIsCfgCompatible (pPortEntry);

    /* Send Counter Update Info to Standby */
    DcbxRedSendDynamicAppPriCounterInfo (pPortEntry,
                                         DCBX_RED_APP_PRI_RX_COUNTER);

    /* Send Dynamic Application Priority Remote Info update to Standby Node */
    DcbxRedSendDynamicAppPriRemInfo (pPortEntry);

    /* Indicate willing Change for Remote */
    AppPriUtlWillingChange (pPortEntry, OSIX_FALSE);

    /* Update the Local Port Information if the Operational State
     * is RECO and also Re-Program the hardware */
    if (pPortEntry->u1AppPriDcbxOperState == APP_PRI_OPER_RECO)
    {
        /* Delete the existing entries */
        if (AppPriUtlHwConfig (pPortEntry, APP_PRI_HW_DELETE) == OSIX_FAILURE)
        {
            DCBX_TRC_ARG4 (DCBX_CRITICAL_TRC, "In %s : "
                           "Oper state == recommended - Delete current HW entries "
                           "for port %d selector %d Protocol %d failed..\r\n",
                           __FUNCTION__, pPortEntry->u4IfIndex, i4Selector,
                           u2Protocol);
            return;
        }

        /* Program the Hw */
        if (AppPriUtlHwConfig (pPortEntry, APP_PRI_HW_CREATE) == OSIX_FAILURE)
        {
            DCBX_TRC_ARG4 (DCBX_CRITICAL_TRC, "In %s : "
                           "Oper state == recommended Create HW entries for port %d "
                           "selector %d Protocol %d with remote config failed...\r\n",
                           __FUNCTION__, pPortEntry->u4IfIndex, i4Selector,
                           u2Protocol);
            return;
        }
    }

    return;

}

/***************************************************************************
 *  FUNCTION NAME : AppPriUtlPortTblCmpFn
 * 
 *  DESCRIPTION   : This utility function is used by RbTree for Application 
 *                  priority port table.
 * 
 *  INPUT         : pRBElem1,pRBElem2 - RbTree Elements.
 * 
 *  OUTPUT        : None
 * 
 *  RETURNS       : LESSER/GREATER/EQUAL
 * 
 * **************************************************************************/
INT4
AppPriUtlPortTblCmpFn (tRBElem * pRBElem1, tRBElem * pRBElem2)
{
    tAppPriPortEntry   *pAppPortEntry1 = NULL;
    tAppPriPortEntry   *pAppPortEntry2 = NULL;

    pAppPortEntry1 = (tAppPriPortEntry *) pRBElem1;
    pAppPortEntry2 = (tAppPriPortEntry *) pRBElem2;

    /* Compare the DCB Port Index */

    if (pAppPortEntry1->u4IfIndex < pAppPortEntry2->u4IfIndex)
    {
        return (DCBX_LESSER);
    }
    else if (pAppPortEntry1->u4IfIndex > pAppPortEntry2->u4IfIndex)
    {
        return (DCBX_GREATER);
    }

    return (DCBX_EQUAL);
}

/*****************************************************************************/
/* Function Name      : APGetPortEntry                                      */
/* Description        : Returns pointer to port entry                        */
/* Input(s)           : u4IfIndex                                            */
/* Output(s)          : None.                                                */
/* Return Value(s)    : Pointer to  PORT entry                               */
/*****************************************************************************/
tAppPriPortEntry   *
AppPriUtlGetPortEntry (UINT4 u4IfIndex)
{
    tAppPriPortEntry    PortEntry;
    tAppPriPortEntry   *pPortEntry = NULL;

    MEMSET (&PortEntry, DCBX_ZERO, sizeof (tAppPriPortEntry));
    PortEntry.u4IfIndex = u4IfIndex;

    pPortEntry = (tAppPriPortEntry *)
        RBTreeGet (gAppPriGlobalInfo.pRbAppPriPortTbl, (tRBElem *) & PortEntry);
    if (pPortEntry == NULL)
    {
        DCBX_TRC_ARG2 (DCBX_FAILURE_TRC,
                       "%s : Port Entry (Port) Id %d is invalid \r\n",
                       __FUNCTION__, u4IfIndex);
    }

    return (pPortEntry);
}

/***************************************************************************
 *  FUNCTION NAME : AppPriUtlTlvStatusChange
 * 
 *  DESCRIPTION   : This utility is used to handle the ApplicationPriority
 *                  TLV Tx status change.
 * 
 *  INPUT         : pPortEntry - Application Priority Port Entry.
 *                  u1NewTlvStatus - TLV Tx Status.
 * 
 *  OUTPUT        : None
 * 
 *  RETURNS       : None
 * 
 * **************************************************************************/
PUBLIC VOID
AppPriUtlTlvStatusChange (tAppPriPortEntry * pPortEntry, UINT1 u1NewTlvStatus)
{
    if (u1NewTlvStatus == APP_PRI_ENABLED)
    {
        /* If the TLV TX new status is enabled then update 
         * the data structure and send the Registration request
         * with DCBX since Registration will happen only if
         * TX status is enabled */
        pPortEntry->u1AppPriTxStatus = APP_PRI_ENABLED;

        /* Here State change will be taken care by
         * TLV recieve. Initailly it will be in INIT state.
         * On receive of TLV update then it chage the state
         * according to the local willing and state machine 
         * oper state */
        AppPriUtlRegOrDeRegBasedOnMode (pPortEntry, u1NewTlvStatus);
    }
    else
    {
        /* Deregister the application with DCBX and change the 
         * TLV Tx status since deregistration happnes only if
         * the Tx status is already enabled. Change the state 
         * to INIT if the state is Reco */

        AppPriUtlRegOrDeRegBasedOnMode (pPortEntry, u1NewTlvStatus);

        if (pPortEntry->u1AppPriDcbxOperState == APP_PRI_OPER_RECO)
        {
            DCBX_TRC (DCBX_SEM_TRC, "AppPriUtlTlvStatusChange:"
                      "Since TLV Tx status is disbaled, changing the state"
                      "to INIT from RECO  !!!\r\n");
            AppPriUtlOperStateChange (pPortEntry, APP_PRI_OPER_INIT);
        }
        pPortEntry->u1AppPriTxStatus = APP_PRI_DISABLED;
    }
    return;
}

/***************************************************************************
 *  FUNCTION NAME : AppPriUtlRegisterApplForPort
 * 
 *  DESCRIPTION   : This utility function is used to register the Appl
 *                  with DCBX.
 * 
 *  INPUT         : pPortEntry - AppPri Port Entry.
 * 
 *  OUTPUT        : None
 * 
 *  RETURNS       : None
 * 
 * **************************************************************************/
/* This will be called from the TLV tx En and Module En and Admin mode 
 * changeto Auto*/
PUBLIC VOID
AppPriUtlRegisterApplForPort (tAppPriPortEntry * pPortEntry)
{
    /* If Module is not enabled then no need to register the application
     * with DCBX and it will registered for all ports which require 
     * registration when the Module status is enabled */
    if (gAppPriGlobalInfo.u1AppPriModStatus != APP_PRI_ENABLED)
    {
        DCBX_TRC (DCBX_CONTROL_PLANE_TRC, "AppPriUtlRegisterApplForPort:"
                  " No Registration Required as the Module is"
                  "not enabled!!!\r\n");
        return;
    }
    /* When the Admin mode of the port is not set to Auto then no need to 
     * register any application with DCBX since Application Priority feature 
     * is not ready to operate through DCBX and it can take the local
     * configuration with ON Mode */
    if (pPortEntry->u1AppPriAdminMode != APP_PRI_PORT_MODE_AUTO)
    {
        DCBX_TRC (DCBX_CONTROL_PLANE_TRC, "AppPriUtlRegisterApplForPort:"
                  " No Registration Required as the Mode is not Auto!!!\r\n");
        return;
    }
    /* Form and send the Application Priority TLV with Port registration request only if
     * TLV Tx status is enabled  */
    if (pPortEntry->u1AppPriTxStatus != APP_PRI_DISABLED)
    {
        DCBX_TRC_ARG1 (DCBX_TLV_TRC, "In AppPriUtlRegisterApplForPort: "
                       "TLV Tx status is Enabled for port %d, Forming and sending "
                       "AppPri TLV with Port Registration request !!!\r\n",
                       pPortEntry->u4IfIndex);
        AppPriUtlFormAndSendAppPriTLV (pPortEntry, DCBX_LLDP_PORT_REG);
    }

    SET_DCBX_STATUS (pPortEntry, pPortEntry->u1AppPriDcbxSemType,
                     DCBX_RED_APP_PRI_DCBX_STATUS_INFO,
                     DCBX_STATUS_PEER_NO_ADV_DCBX);
    return;

}

/***************************************************************************
 *  FUNCTION NAME : AppPriUtlDeRegisterApplForPort
 * 
 *  DESCRIPTION   : This utility is used to De Register the Appl with DCBX.
 * 
 *  INPUT         : pPortEntry - AppPri Port Entry.
 * 
 *  OUTPUT        : None
 * 
 *  RETURNS       : None
 * 
 * **************************************************************************/
/* This will be called from the TLV tx Dis and Module Dis and port 
 * Delete and Admin mode change to Dis/On */
PUBLIC VOID
AppPriUtlDeRegisterApplForPort (tAppPriPortEntry * pPortEntry)
{
    tDcbxAppRegInfo     DcbxAppPortMsg;
    /* If Module is not enabled then no need to De-register the application
     * with DCBX and it should have already De-registered for all ports during 
     * the module disable */
    if (gAppPriGlobalInfo.u1AppPriModStatus != APP_PRI_ENABLED)
    {
        DCBX_TRC (DCBX_CONTROL_PLANE_TRC, "AppPriUtlDeRegisterApplForPort:"
                  " No DeRegistration Required as the Module is "
                  "not enabled!!!\r\n");
        return;
    }
    /* When the Admin mode of the port is not set to Auto then no need to 
     * De-register any application with DCBX since Application Priority Feature
     * is not operating in the DCBX AUTO mode */
    if (pPortEntry->u1AppPriAdminMode != APP_PRI_PORT_MODE_AUTO)
    {
        DCBX_TRC (DCBX_CONTROL_PLANE_TRC, "AppPriUtlDeRegisterApplForPort:"
                  " No DeRegistration Required as the Mode is not Auto!!!\r\n");
        return;
    }

    if (pPortEntry->u1AppPriTxStatus != APP_PRI_DISABLED)
    {
        /* De-Registers the AppPri TLV with DCBX if TLV Tx status is enabled,
         * since if it is not enabled then it would not have registered with
         * DCBX */
        MEMSET (&DcbxAppPortMsg, DCBX_ZERO, sizeof (tDcbxAppRegInfo));
        APP_PRI_GET_APPL_ID (DcbxAppPortMsg.DcbxAppId);

        DCBX_TRC (DCBX_TLV_TRC, "AppPriUtlDeRegisterApplForPort:"
                  " Sending TLV DeRegistration Message!!!\r\n");
        AppPriPortPostMsgToDCBX (DCBX_LLDP_PORT_DEREG, pPortEntry->u4IfIndex,
                                 &DcbxAppPortMsg);
    }

    /* Clearing the remote parameter after Deregistration */
    AppPriUtlDeleteRemoteMappingEntries (pPortEntry->u4IfIndex);
    AppPriUtlDeleteRemotePortEntries (pPortEntry->u4IfIndex);

    return;
}

/***************************************************************************
 *  FUNCTION NAME : AppPriUtlDeleteRemotePortEntries
 *
 *  DESCRIPTION   : This utility function is used to delete
 *                  the Application Priority remote port entries.
 *
 *  INPUT         : IfIndex - Interface
 *
 *  OUTPUT        : None
 *
 *  RETURNS       : None
 *
 * **************************************************************************/
VOID
AppPriUtlDeleteRemotePortEntries (UINT4 u4IfIndex)
{
    tAppPriPortEntry   *pPortEntry = NULL;

    pPortEntry = AppPriUtlGetPortEntry (u4IfIndex);
    if (pPortEntry != NULL)
    {
        /* Initialize to defaults */
        pPortEntry->AppPriRemPortInfo.u4AppPriRemTimeMark = 0;
        pPortEntry->AppPriRemPortInfo.i4AppPriRemIndex = 0;
        pPortEntry->AppPriRemPortInfo.u1AppPriRemWilling = 0;
        pPortEntry->AppPriRemPortInfo.u4AppPriDestRemMacIndex = 0;
        MEMSET (pPortEntry->AppPriRemPortInfo.au1AppPriDestRemMacAddr, 0,
                MAC_ADDR_LEN);
    }
    return;
}

/***************************************************************************
 *  FUNCTION NAME : AppPriPortPostMsgToDCBX
 * 
 *  DESCRIPTION   : This utility function is used to post the TLV/Sem
 *                  Update message to DCBX.
 * 
 *  INPUT         : u1MsgType - Message type Reg/Update/Sem/DeReg.
 *                  u4IfIndex - Port Number.
 *                  pDcbxAppPortMsg - App Reg Info Structure
 * 
 *  OUTPUT        : None
 * 
 *  RETURNS       : None
 * 
 * **************************************************************************/
PUBLIC VOID
AppPriPortPostMsgToDCBX (UINT1 u1MsgType, UINT4 u4IfIndex,
                         tDcbxAppRegInfo * pDcbxAppPortMsg)
{
    switch (u1MsgType)
    {
        case DCBX_LLDP_PORT_UPDATE:
            /* Used to send the TLV updated information to the LLDP using 
             * DCBX function */
            if (DcbxApiApplPortUpdate (u4IfIndex, pDcbxAppPortMsg)
                != OSIX_SUCCESS)
            {
                DCBX_TRC_ARG1 (DCBX_TLV_TRC | DCBX_FAILURE_TRC,
                               "In AppPriPortPostMsgToDCBX : "
                               "DCBX Port TLV Update Failed!!! for the port %d.\r\n",
                               u4IfIndex);
            }
            break;

        case DCBX_LLDP_PORT_REG:
            /* Used to Register the application to the LLDP using 
             * DCBX function */
            if (DcbxApiApplPortReg (u4IfIndex, pDcbxAppPortMsg) != OSIX_SUCCESS)
            {
                DCBX_TRC_ARG1 (DCBX_TLV_TRC | DCBX_FAILURE_TRC,
                               "In AppPriPortPostMsgToDCBX : "
                               "DCBX Port Registration Failed!!! for the port %d.\r\n",
                               u4IfIndex);
            }
            break;

        case DCBX_LLDP_PORT_DEREG:
            /* Used to De-Register the application from the LLDP using 
             * DCBX function */
            if (DcbxApiApplPortDeReg (u4IfIndex, pDcbxAppPortMsg)
                != OSIX_SUCCESS)
            {
                DCBX_TRC_ARG1 (DCBX_TLV_TRC | DCBX_FAILURE_TRC,
                               "In AppPriPortPostMsgToDCBX : "
                               "DCBX Port DeRegistration Failed!!! for the port %d.\r\n",
                               u4IfIndex);
            }
            break;

        case DCBX_STATE_MACHINE:
            /* Used to run the DCBX state machine and get the 
             * necessary Operational state  */
            DCBX_TRC_ARG1 (DCBX_SEM_TRC,
                           "In AppPriPortPostMsgToDCBX : "
                           "Application Priority Port SEM update request to DCBX!!! "
                           "for the port %d.\r\n", u4IfIndex);
            DcbxSemUpdate (u4IfIndex, pDcbxAppPortMsg);
            break;

        default:
            DCBX_TRC_ARG1 (DCBX_FAILURE_TRC,
                           "In AppPriPortPostMsgToDCBX : "
                           "Invalid Message Type Received!!! for the port %d.\r\n",
                           u4IfIndex);
            break;
    }
    return;
}

/***************************************************************************
 *  FUNCTION NAME : AppPriUtlOperStateChange
 * 
 *  DESCRIPTION   : This utility is used to Change the oper state.
 * 
 *  INPUT         : pPortEntry - Application Priority port Entry.
 *                  u1APState - Application Priority Oper State.
 * 
 *  OUTPUT        : None
 * 
 *  RETURNS       : None
 * 
 * **************************************************************************/
PUBLIC VOID
AppPriUtlOperStateChange (tAppPriPortEntry * pPortEntry, UINT1 u1APState)
{
    tDcbxTrapMsg        NotifyInfo;

    /* State change should occur only if Module status is enabled. If module 
     * status is disabled then the state will be always OFF */
    if (gAppPriGlobalInfo.u1AppPriModStatus != APP_PRI_ENABLED)
    {
        DCBX_TRC (DCBX_SEM_TRC, "AppPriUtlOperStateChange:"
                  "Module status is disbaled. So State change will not happen"
                  "It will be in OFF state !!!\r\n");
        return;
    }
    /* If the Old Oper state and New Oper state are same then no need
     * to process further */
    if ((pPortEntry->u1AppPriDcbxOperState == u1APState) &&
        (pPortEntry->u1AppPriDcbxOperState != APP_PRI_OPER_RECO))
    {
        DCBX_TRC (DCBX_SEM_TRC, "AppPriUtlOperStateChange:"
                  "Both old and new state are same !!!\r\n");
        return;
    }

    /* Update the Local Parameters which are operation parameters
     * according to the state */
    DCBX_TRC (DCBX_SEM_TRC, "AppPriUtlOperStateChange:"
              "Updating the Local Paramters based on the state !!!\r\n");
    AppPriUtlUpdateLocalParam (pPortEntry, u1APState);

    /* Update Hw */
    if (u1APState == APP_PRI_OPER_OFF)
    {
        /* If peer in error, remove the hw entries */
        if (AppPriUtlHwConfig (pPortEntry, APP_PRI_HW_DELETE) == OSIX_FAILURE)
        {
            DCBX_TRC_ARG3 (DCBX_CRITICAL_TRC, "In func %s Line = %d: "
                           "Oper state == off: "
                           "Delete HW entries for port %d - Failed!!!\r\n",
                           __func__, __LINE__, pPortEntry->u4IfIndex);
        }
    }
    else
    {
        if (AppPriUtlHwConfig (pPortEntry, APP_PRI_HW_CREATE) == OSIX_FAILURE)
        {
            DCBX_TRC_ARG4 (DCBX_CRITICAL_TRC, "In func %s Line = %d: "
                           "Oper state (Auto/On) = %d: "
                           "HW entries for port %d unable to create!!!\r\n",
                           __func__, __LINE__, u1APState,
                           pPortEntry->u4IfIndex);
        }
    }

    /* Send Trap Notification */
    NotifyInfo.u4IfIndex = pPortEntry->u4IfIndex;
    NotifyInfo.u1Module = APP_PRI_TRAP;
    NotifyInfo.unTrapMsg.u1OperState = pPortEntry->u1AppPriDcbxOperState;
    DcbxSendNotification (&NotifyInfo, OPER_STATE_TRAP);

    return;
}

/***************************************************************************
 *  FUNCTION NAME : AppPriUtlUpdateLocalParam
 * 
 *  DESCRIPTION   : This utility is used to update the local parameter.
 * 
 *  INPUT         : pPortEntry - Application Priority Port Entry.
 *                  u1APState - Application Priority oper State.
 * 
 *  OUTPUT        : None
 * 
 *  RETURNS       : None
 * 
 * **************************************************************************/
PUBLIC VOID
AppPriUtlUpdateLocalParam (tAppPriPortEntry * pPortEntry, UINT1 u1APState)
{
    INT4                i4Selector = DCBX_ZERO;
    tDcbxTrapMsg        NotifyInfo;

    pPortEntry->u1AppPriDcbxOperState = u1APState;
    if ((u1APState == APP_PRI_OPER_INIT) || (u1APState == APP_PRI_OPER_OFF))
    {
        /* If the state is either INIT or OFF then local parameters will be 
         * same as Admin configuration parameters */
        /* Local Parameters are referred to Admin Parameters */
        for (i4Selector = DCBX_ONE; i4Selector <= APP_PRI_MAX_SELECTOR;
             i4Selector++)
        {
            pPortEntry->AppPriLocPortInfo.pAppPriLocMappingTbl[i4Selector - 1] =
                &(pPortEntry->AppPriAdmPortInfo.
                  AppPriAdmMappingTbl[i4Selector - 1]);
        }
    }
    else
    {
        /* If the state is RECO then local parametrs will be same as
         * Remote parameters */
        /* Local Parameters are referred to Remote Parameters */
        for (i4Selector = DCBX_ONE; i4Selector <= APP_PRI_MAX_SELECTOR;
             i4Selector++)
        {
            pPortEntry->AppPriLocPortInfo.pAppPriLocMappingTbl[i4Selector - 1] =
                &(pPortEntry->AppPriRemPortInfo.
                  AppPriRemMappingTbl[i4Selector - 1]);
        }

    }

    NotifyInfo.u4IfIndex = pPortEntry->u4IfIndex;
    NotifyInfo.u1Module = APP_PRI_TRAP;
    NotifyInfo.unTrapMsg.u1OperState = pPortEntry->u1AppPriDcbxOperState;
    DcbxSendNotification (&NotifyInfo, OPER_STATE_TRAP);
    return;
}

/***************************************************************************
 *  FUNCTION NAME : AppPriUtlWillingChange
 * 
 *  DESCRIPTION   : This utility is used to handle the willing parameter
 *                  change.
 * 
 *  INPUT         : pPortEntry - Application Priority Port Entry.
 *                  u1IsLoca1Willing - Whether Local willing changed or not.
 *                  If local willing then send the updated TLV.
 * 
 *  OUTPUT        : None
 * 
 *  RETURNS       : None
 * 
 * **************************************************************************/
PUBLIC VOID
AppPriUtlWillingChange (tAppPriPortEntry * pPortEntry, UINT1 u1IsLocalWilling)
{
    /* This will be called whenever there is a change in 
     * Remote/Local Willing Status */
    tDcbxAppRegInfo     DcbxAppPortMsg;
    UINT1               u1DcbxVersion = DCBX_VER_UNKNOWN;
    UINT1               u1PrevAppPriDcbxOperState = 0;
    UINT1               u1OperState = DCBX_ZERO;
    BOOL1               b1IsCompatible = DCBX_ZERO;

    MEMSET (&DcbxAppPortMsg, DCBX_ZERO, sizeof (tDcbxAppRegInfo));

    /* If Module status is disbaled then no need to send the 
     * state machine chage request to DCBX since remote paramters 
     * will not be present and the application would not have registered
     * with DCBX in Module Disable state */
    if (gAppPriGlobalInfo.u1AppPriModStatus != APP_PRI_ENABLED)
    {
        DCBX_TRC (DCBX_SEM_TRC, "AppPriUtlWillingChange:"
                  " No State machine change required as the "
                  "Module is not enabled!!!\r\n");
        return;
    }
    /* If the Admin mode is not AUTO then no need to send the state
     * machine chage request since it does not operate on DCBX */
    if (pPortEntry->u1AppPriAdminMode != APP_PRI_PORT_MODE_AUTO)
    {
        DCBX_TRC (DCBX_SEM_TRC, "AppPriUtlWillingChange:"
                  " No State machine change required as the"
                  " Mode is not Auto!!!\r\n");
        return;
    }
    /* If TLV Tx status is not enabled then no need to send the state
     * machine chage since it wouold have registered with DCBX */
    if (pPortEntry->u1AppPriTxStatus != APP_PRI_ENABLED)
    {
        DCBX_TRC (DCBX_CONTROL_PLANE_TRC, "AppPriUtlWillingChange:"
                  " TLV tranmsission status is not enabled."
                  "No SEM change request is needed!!!\r\n");
        return;
    }

    DcbxUtlGetOperVersion (pPortEntry->u4IfIndex, &u1DcbxVersion);
    u1PrevAppPriDcbxOperState = pPortEntry->u1AppPriDcbxOperState;
    if (u1DcbxVersion == DCBX_VER_IEEE)
    {
        /* If remote entry or Remote willing is not present 
         * then the state will be always INIT.
         * No need to state machine change request */
        if ((pPortEntry->AppPriRemPortInfo.u1AppPriRemWilling != DCBX_ZERO))
        {
            DCBX_TRC (DCBX_SEM_TRC, "AppPriUtlWillingChange:"
                      " Sending State machine change request message"
                      "to DCBX !!!\r\n");
            /* Fill the Information to be sent to DCBX */
            APP_PRI_GET_APPL_ID (DcbxAppPortMsg.DcbxAppId);
            MEMCPY (DcbxAppPortMsg.au1RemDestMacAddr,
                    pPortEntry->AppPriRemPortInfo.au1AppPriDestRemMacAddr,
                    MAC_ADDR_LEN);
            DcbxAppPortMsg.u4RemLocalDestMACIndex =
                pPortEntry->AppPriRemPortInfo.u4AppPriDestRemMacIndex;

            DcbxAppPortMsg.u1ApplLocalWilling =
                pPortEntry->AppPriLocPortInfo.u1AppPriLocWilling;
            DcbxAppPortMsg.u1ApplRemoteWilling =
                pPortEntry->AppPriRemPortInfo.u1AppPriRemWilling;
            DcbxAppPortMsg.u1ApplStateMachineType = DCBX_SYM_STATE_MACHINE;
            /* Post the mesage to DCBX for State machine chage request */
            AppPriPortPostMsgToDCBX (DCBX_STATE_MACHINE, pPortEntry->u4IfIndex,
                                     &DcbxAppPortMsg);
        }
        /* If there is a change in AppPriDcbxOperState or
           Local willing status  then send the updated TLV */
        if ((u1IsLocalWilling == OSIX_TRUE) ||
            (u1PrevAppPriDcbxOperState != pPortEntry->u1AppPriDcbxOperState))
        {
            DCBX_TRC (DCBX_TLV_TRC, "AppPriUtlWillingChange:"
                      "Forming and sending application priority TLV since Local willing status"
                      "has been changed !!!\r\n");
            /* Send the TLV Update since Local Willing has been changed */
            AppPriUtlFormAndSendAppPriTLV (pPortEntry, DCBX_LLDP_PORT_UPDATE);
        }
    }
    else if (u1DcbxVersion == DCBX_VER_CEE)
    {
        if (pPortEntry->u1DcbxStatus == DCBX_STATUS_CFG_NOT_COMPT)
        {
            b1IsCompatible = OSIX_FALSE;
        }
        else
        {
            b1IsCompatible = OSIX_TRUE;
        }

        u1OperState =
            CEEUtlCalculateOperState (pPortEntry->AppPriRemPortInfo.
                                      u1AppPriRemWilling, u1IsLocalWilling,
                                      b1IsCompatible);
        if ((u1OperState != pPortEntry->u1AppPriDcbxOperState)
            || (u1IsLocalWilling == OSIX_TRUE))
        {
            DCBX_TRC_ARG1 (DCBX_TLV_TRC, "PFCUtlWillingChange: "
                           "Form and send CEE TLV since Local Oper state"
                           "(or) Local willing status has been changed for port [%d]!!!\r\n",
                           pPortEntry->u4IfIndex);
            CEEFormAndSendTLV (pPortEntry->u4IfIndex, DCBX_LLDP_PORT_UPDATE,
                               DCBX_ZERO);
        }
    }
    return;

}

/***************************************************************************
 *  FUNCTION NAME : AppPriUtlDeleteAllPorts
 * 
 *  DESCRIPTION   : This utility function is used to delete all 
 *                  the Application Priority ports at the time of shutodwn.
 * 
 *  INPUT         : None
 * 
 *  OUTPUT        : None
 * 
 *  RETURNS       : None
 * 
 * **************************************************************************/
PUBLIC VOID
AppPriUtlDeleteAllPorts (VOID)
{
    tAppPriPortEntry    TempPortEntry;
    tAppPriPortEntry   *pPortEntry = NULL;

    MEMSET (&TempPortEntry, DCBX_ZERO, sizeof (tAppPriPortEntry));

    /* Get the First port entry and delete the port entry if present */
    pPortEntry = (tAppPriPortEntry *) RBTreeGetFirst
        (gAppPriGlobalInfo.pRbAppPriPortTbl);
    if (pPortEntry != NULL)
    {
        do
        {
            /* Delete the Application Priority port table entry */
            TempPortEntry.u4IfIndex = pPortEntry->u4IfIndex;
            AppPriUtlDeletePortTblEntry (pPortEntry);
            pPortEntry = (tAppPriPortEntry *) RBTreeGetNext
                (gAppPriGlobalInfo.pRbAppPriPortTbl, &TempPortEntry,
                 AppPriUtlPortTblCmpFn);
            /* Process for all the ports in the port RB Tree */
        }
        while (pPortEntry != NULL);
    }
    return;
}

/*****************************************************************************/
/* Function Name      : AppPriUtlDeletePortTblEntry                          */
/* Description        : This function is used to Delete an Entry from the    */
/*                      Application Priority Port Table ,it will do the      */
/*                      following Action                                     */
/*                      1. Remove the Entry from the Application Priority    */
/*                         Port Table                                        */
/*                      2. Clear the Removed Entry                           */
/*                      3. Release the Removed Entry's memory                */
/* Input(s)           : pAppPortEntry   - Pointer to  AppPriPortEntry        */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : gAppPriGlobalInfo                                    */
/* Global Variables                                                          */
/* Modified           : gAppPriGlobalInfo                                    */
/* Return Value(s)    : OSIX_SUCCESS or OSIX_FAILURE                         */
/* Called By          : SNMP Low Level                                       */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
AppPriUtlDeletePortTblEntry (tAppPriPortEntry * pAppPortEntry)
{
    /* Deregister all the application for this port with DCBX */
    AppPriUtlRegOrDeRegBasedOnMode (pAppPortEntry, APP_PRI_DISABLED);

    /*Disable Application Priority Featue */
    pAppPortEntry->u1AppPriAdminMode = APP_PRI_PORT_MODE_OFF;
    /*Change the state to OFF. This will take care of disabling
     * the feature in hardware */
    AppPriUtlOperStateChange (pAppPortEntry, APP_PRI_OPER_OFF);

    /* Remove all Application Priority Mapping table for that port */
    AppPriUtlDeleteAllAppPriMappingEntries (pAppPortEntry->u4IfIndex);

    /* Remove Entry from the Application Prioirty Port Table */
    RBTreeRemove (gAppPriGlobalInfo.pRbAppPriPortTbl, (UINT1 *) pAppPortEntry);
    /* Release the Removed Entry's memory */
    MemReleaseMemBlock (gAppPriGlobalInfo.AppPriPortPoolId,
                        (UINT1 *) (pAppPortEntry));
    return (OSIX_SUCCESS);
}

/***************************************************************************
 *  FUNCTION NAME : AppPriUtlModuleStatusChange
 * 
 *  DESCRIPTION   : This utility is used to handle the Module status change.
 * 
 *  INPUT         : u1Status - Module status -En/Dis
 * 
 *  OUTPUT        : None
 * 
 *  RETURNS       : None
 * 
 * **************************************************************************/
PUBLIC VOID
AppPriUtlModuleStatusChange (UINT1 u1Status)
{
    tAppPriPortEntry    TempPortEntry;
    tAppPriPortEntry   *pPortEntry = NULL;
    tDcbxTrapMsg        NotifyInfo;

    MEMSET (&NotifyInfo, DCBX_ZERO, sizeof (tDcbxTrapMsg));
    MEMSET (&TempPortEntry, DCBX_ZERO, sizeof (tAppPriPortEntry));

    pPortEntry = (tAppPriPortEntry *) RBTreeGetFirst
        (gAppPriGlobalInfo.pRbAppPriPortTbl);
    /* Get all the AppPri port entry and process the module status change */
    if (pPortEntry != NULL)
    {
        /* If Module status is to be enabled then first change the status and 
         * and then process since Registration with DCBX and State change occurs 
         * only if the module status is enabled */
        if (u1Status == APP_PRI_ENABLED)
        {
            gAppPriGlobalInfo.u1AppPriModStatus = APP_PRI_ENABLED;
        }
        do
        {
            /* Register or De-Register the AppPri applications with DCBX for
             * all the port and change the state to INIT or OFF. */
            if (u1Status == APP_PRI_ENABLED)
            {
                AppPriUtlRegOrDeRegBasedOnMode (pPortEntry, u1Status);

                if (pPortEntry->u1AppPriAdminMode != APP_PRI_PORT_MODE_OFF)
                {
                    DCBX_TRC (DCBX_SEM_TRC, "AppPriUtlModuleStatusChange:"
                              "Module Enable Changing the state to"
                              "INIT!!!\r\n");
                    AppPriUtlOperStateChange (pPortEntry, APP_PRI_OPER_INIT);
                }
            }
            else
            {
                DCBX_TRC (DCBX_SEM_TRC, "AppPriUtlModuleStatusChange:"
                          "Module Disable Changing the state to OFF!!!\r\n");

                AppPriUtlOperStateChange (pPortEntry, APP_PRI_OPER_OFF);
                AppPriUtlRegOrDeRegBasedOnMode (pPortEntry, u1Status);
            }

            TempPortEntry.u4IfIndex = pPortEntry->u4IfIndex;
            pPortEntry = (tAppPriPortEntry *) RBTreeGetNext
                (gAppPriGlobalInfo.pRbAppPriPortTbl,
                 &TempPortEntry, AppPriUtlPortTblCmpFn);
        }
        while (pPortEntry != NULL);
    }
    else
    {
        DCBX_TRC (DCBX_FAILURE_TRC, "AppPriUtlModuleStatusChange:"
                  " No AppPri Port is created!!!\r\n");
    }
    /* Update the Module Status Global Value */
    gAppPriGlobalInfo.u1AppPriModStatus = u1Status;

    /* Send Module status change Notification */
    NotifyInfo.u1Module = APP_PRI_TRAP;
    NotifyInfo.unTrapMsg.u1ModuleStatus = gAppPriGlobalInfo.u1AppPriModStatus;

    DcbxSendNotification (&NotifyInfo, MODULE_TRAP);

    return;
}

/***************************************************************************
 *  FUNCTION NAME : AppPriUtlAdminModeChange
 * 
 *  DESCRIPTION   : This utility is used to Handle the Admin Mode change.
 * 
 *  INPUT         : pPortEntry - AppPri Port Entry
 *                  u1NewAdminMode - Admin Mode Value.
 * 
 *  OUTPUT        : None
 * 
 *  RETURNS       : None
 * 
 * **************************************************************************/
PUBLIC VOID
AppPriUtlAdminModeChange (tAppPriPortEntry * pPortEntry, UINT1 u1NewAdminMode)
{
    tDcbxTrapMsg        NotifyInfo;

    MEMSET (&NotifyInfo, DCBX_ZERO, sizeof (tDcbxTrapMsg));

    if (pPortEntry->u1AppPriAdminMode == APP_PRI_PORT_MODE_AUTO)
    {
        /* This check is to ensure the Deregistration happens only in 
         * AUTO Mode. So Deregistartion of TLV happens before 
         * updating the Strucuture */
        AppPriUtlRegOrDeRegBasedOnMode (pPortEntry, APP_PRI_DISABLED);

        pPortEntry->u1AppPriAdminMode = u1NewAdminMode;
    }
    else
    {
        pPortEntry->u1AppPriAdminMode = u1NewAdminMode;
    }
    if (u1NewAdminMode == APP_PRI_PORT_MODE_AUTO)
    {
        /* If the NewAdmin mode is AUTO then Register the AppPri 
         *   applications with DCBX and change the state to INIT */
        AppPriUtlRegOrDeRegBasedOnMode (pPortEntry, APP_PRI_ENABLED);

        DCBX_TRC (DCBX_SEM_TRC, "AppPriUtlAdminModeChange:"
                  "Port Mode changing to Auto, Change the "
                  "state to INIT!!!\r\n");
        AppPriUtlOperStateChange (pPortEntry, APP_PRI_OPER_INIT);
    }
    else if (u1NewAdminMode == APP_PRI_PORT_MODE_ON)
    {
        /* If the NewAdmin mode is ON then change the state to INIT */
        DCBX_TRC (DCBX_SEM_TRC, "AppPriUtlAdminModeChange:"
                  "Port Mode changing to ON, Change the state to INIT!!!\r\n");
        AppPriUtlOperStateChange (pPortEntry, APP_PRI_OPER_INIT);
    }
    else
    {
        /* If the NewAdmin mode is OFF then chage the state to OFF
         *      i.e Disable the hardware configurations */
        DCBX_TRC (DCBX_SEM_TRC, "AppPriUtlAdminModeChange:"
                  "Port Mode changing to OFF, Change the state to OFF!!!\r\n");
        AppPriUtlOperStateChange (pPortEntry, APP_PRI_OPER_OFF);
    }
    /* Send Trap Notification */
    NotifyInfo.u4IfIndex = pPortEntry->u4IfIndex;
    NotifyInfo.u1Module = APP_PRI_TRAP;
    NotifyInfo.unTrapMsg.u1AdminMode = pPortEntry->u1AppPriAdminMode;

    DcbxSendNotification (&NotifyInfo, ADMIN_STATUS_TRAP);

    return;
}

/*****************************************************************************/
/* Function Name      : AppPriUtlCreatePortTblEntry                          */
/* Description        : This function is used to Create an Entry             */
/*                      it will do the following Action                      */
/*                      1. Allocate a memory block for the New Entry         */
/*                      2. Initialize the New Entry                          */
/*                      3. Add this New Entry into the AppPri Port Table     */
/* Input(s)           : u4IfIndex      - Index to the AppPri port Table      */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : gAPGlobalInfo                                        */
/* Global Variables                                                          */
/* Modified           : gAPGlobalInfo                                        */
/* Return Value(s)    : NULL / Pointer to  AppPriPortEntry                   */
/* Called By          : SNMP Low Level                                       */
/* Calling Function   :                                                      */
/*****************************************************************************/
tAppPriPortEntry   *
AppPriUtlCreatePortTblEntry (UINT4 u4IfIndex)
{
    tAppPriPortEntry   *pAppPriPortEntry = NULL;
    INT4                i4Selector = DCBX_ZERO;
    UINT1               u1DcbxMode = DCBX_ZERO;

    /* 1. Allocate a memory block for the New Entry */
    pAppPriPortEntry = (tAppPriPortEntry *)
        MemAllocMemBlk (gAppPriGlobalInfo.AppPriPortPoolId);
    if (pAppPriPortEntry == NULL)
    {
        DCBX_TRC_ARG2 ((DCBX_RESOURCE_TRC | DCBX_FAILURE_TRC),
                       "In %s : MemAllocMemBlk () Failed, "
                       "for AppPri Specified Port is %d\r\n", __FUNCTION__,
                       u4IfIndex);
        return (NULL);
    }

    /* 2. Initialize the AppPri Port Entry */
    MEMSET (pAppPriPortEntry, DCBX_INIT_VAL, sizeof (tAppPriPortEntry));
    pAppPriPortEntry->u4IfIndex = u4IfIndex;

    /*Default AppPri Status of the Port */
    pAppPriPortEntry->u1AppPriAdminMode = APP_PRI_PORT_MODE_OFF;

    /*Default AppPri Oper State of the Port */
    pAppPriPortEntry->u1AppPriDcbxOperState = APP_PRI_OPER_OFF;
    DcbxUtlGetDCBXMode (u4IfIndex, &u1DcbxMode);
    if (u1DcbxMode == DCBX_MODE_CEE)
    {
        pAppPriPortEntry->u1AppPriDcbxSemType = DCBX_FEAT_STATE_MACHINE;
    }
    else if (u1DcbxMode == DCBX_MODE_IEEE)
    {
        pAppPriPortEntry->u1AppPriDcbxSemType = DCBX_SYM_STATE_MACHINE;
    }
    else
    {
        pAppPriPortEntry->u1AppPriDcbxSemType = DCBX_DEF_APP_PRI_SM_TYPE;
    }
    /*Default Tx Status of the Port */
    pAppPriPortEntry->u1AppPriTxStatus = APP_PRI_DISABLED;

    pAppPriPortEntry->AppPriAdmPortInfo.u1AppPriAdmWilling = APP_PRI_DISABLED;

    pAppPriPortEntry->u1CurrentState = CEE_ST_WAIT_FOR_FEAT_TLV;
    pAppPriPortEntry->u1DcbxStatus = DCBX_STATUS_NOT_ADVERTISE;
    pAppPriPortEntry->bAppPriSyncd = APP_PRI_DISABLED;
    pAppPriPortEntry->bAppPriError = APP_PRI_DISABLED;
    pAppPriPortEntry->bAppPriPeerWilling = APP_PRI_DISABLED;

    /*Initialise the SLLs */
    for (i4Selector = DCBX_ONE; i4Selector <= APP_PRI_MAX_SELECTOR;
         i4Selector++)
    {
        TMO_SLL_Init (&(APP_PRI_ADM_TBL (pAppPriPortEntry, i4Selector - 1)));
        TMO_SLL_Init (&(APP_PRI_REM_TBL (pAppPriPortEntry, i4Selector - 1)));
        pAppPriPortEntry->AppPriLocPortInfo.pAppPriLocMappingTbl[i4Selector -
                                                                 1] =
            &(pAppPriPortEntry->AppPriAdmPortInfo.
              AppPriAdmMappingTbl[i4Selector - 1]);
    }

    /* Copy the default paramters to the Local Info */

    pAppPriPortEntry->AppPriLocPortInfo.u1AppPriLocWilling =
        pAppPriPortEntry->AppPriAdmPortInfo.u1AppPriAdmWilling;

    /* 3. Add this New Entry into the AppPri Port  Table */
    if (RBTreeAdd (gAppPriGlobalInfo.pRbAppPriPortTbl,
                   (tRBElem *) pAppPriPortEntry) != RB_SUCCESS)
    {
        MemReleaseMemBlock (gAppPriGlobalInfo.AppPriPortPoolId,
                            (UINT1 *) pAppPriPortEntry);

        DCBX_TRC_ARG2 (DCBX_FAILURE_TRC, "In %s : RBTreeAdd () Failed, "
                       "for AppPri port %d \r\n", __FUNCTION__, u4IfIndex);
        return (NULL);
    }
    return (pAppPriPortEntry);
}

/*****************************************************************************/
/* Function Name      : AppPriGetAppPriMappingEntry                          */
/* Description        : Returns pointer to Application Prioirty Mapping entry*/
/* Input(s)           : u4IfIndex  - Interface Index                         */
/*                      i4Selector - Selector Field                          */
/*                      i4Protocol - ApplicationId                           */
/* Output(s)          : None.                                                */
/* Return Value(s)    : Pointer to Application Priority Mapping entry        */
/*****************************************************************************/
tAppPriMappingEntry *
AppPriUtlGetAppPriMappingEntry (UINT4 u4IfIndex, INT4 i4Selector,
                                INT4 i4Protocol, UINT1 u1Type)
{
    tAppPriMappingEntry *pAppPriEntry = NULL;
    tAppPriPortEntry   *pPortEntry = NULL;
    tTMO_SLL           *pAppPriMappingTbl = NULL;

    pPortEntry = AppPriUtlGetPortEntry (u4IfIndex);

    if (pPortEntry == NULL)
    {
        DCBX_TRC_ARG2 (DCBX_FAILURE_TRC, "In %s :AppPriUtlGetPortEntry Failed, "
                       "for AppPri port %d \r\n", __FUNCTION__, u4IfIndex);
        return NULL;
    }

    switch (u1Type)
    {
        case APP_PRI_ADMIN:
            pAppPriMappingTbl = &(APP_PRI_ADM_TBL (pPortEntry, i4Selector - 1));
            break;
        case APP_PRI_LOCAL:
            pAppPriMappingTbl = APP_PRI_LOC_TBL (pPortEntry, i4Selector - 1);
            break;
        default:                /*APP_PRI_REMOTE */
            pAppPriMappingTbl = &(APP_PRI_REM_TBL (pPortEntry, i4Selector - 1));
            break;
    }

    if (pAppPriMappingTbl == NULL)
    {
        DCBX_TRC_ARG3 (DCBX_FAILURE_TRC, "In %s :AppPriMappingTable is NULL "
                       "for AppPri port %d selector %d \r\n", __FUNCTION__,
                       u4IfIndex, i4Selector);
        return NULL;

    }

    TMO_SLL_Scan (pAppPriMappingTbl, pAppPriEntry, tAppPriMappingEntry *)
    {
        if (pAppPriEntry->i4AppPriProtocol == i4Protocol)
        {
            return pAppPriEntry;
        }
    }
    DCBX_TRC_ARG4 (DCBX_CONTROL_PLANE_TRC,
                   "%s :AppPriMappingEntry for port Id %d"
                   " Selector %d  Protocol %d does not exist\r\n", __FUNCTION__,
                   u4IfIndex, i4Selector, i4Protocol);
    return NULL;
}

/*****************************************************************************/
/* Function Name      : AppPriUtlCreateAppPriMappingTblEntry                 */
/* Description        : This function is used to Create an Entry             */
/*                      it will do the following Action                      */
/*                      1. Allocate a memory block for the New Entry         */
/*                      2. Initialize the New Entry                          */
/*                      3. Add this New Entry into the AppPriMappingTable    */
/* Input(s)           : u4IfIndex      - Interface Index                     */
/*                      i4Selector     - Selector Field                      */
/*                      i4Protocol     - Application Id                      */
/*                      u4Prioirty     - Priority to be assigned to the      */
/*                                       application                         */
/*                      i1Type         - Indicaties in which RbTree the      */
/*                                       entry should be added (0 - Admin    */
/*                                       1-Local 2-Remote)                   */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : gAPGlobalInfo                                        */
/* Global Variables                                                          */
/* Modified           :                                                      */
/* Return Value(s)    : NULL / Pointer to  iAppPriMappingEntry               */
/* Called By          : SNMP Low Level                                       */
/* Calling Function   :                                                      */
/*****************************************************************************/
tAppPriMappingEntry *
AppPriUtlCreateAppPriMappingTblEntry (UINT4 u4IfIndex, INT4 i4Selector,
                                      INT4 i4Protocol, INT1 i1Type)
{
    tAppPriPortEntry   *pPortEntry = NULL;
    tAppPriMappingEntry *pAppPriEntry = NULL;
    tAppPriMappingEntry *pNextAppPriEntry = NULL;
    tAppPriMappingEntry *pPrevAppPriEntry = NULL;
    tTMO_SLL           *pAppPriMappingTbl = NULL;

    if ((i4Selector < DCBX_ONE) || (i4Selector > APP_PRI_MAX_SELECTOR))
    {
        DCBX_TRC_ARG3 (DCBX_FAILURE_TRC,
                       "%s: Received selector[%d] value is not correct "
                       "for port %d \r\n", __FUNCTION__, i4Selector, u4IfIndex);
        return NULL;
    }

    /* Get the port Entry for u4IfIndex */
    pPortEntry = AppPriUtlGetPortEntry (u4IfIndex);
    if (pPortEntry == NULL)
    {
        DCBX_TRC_ARG2 (DCBX_FAILURE_TRC,
                       "In %s: Application priority not present on "
                       "port %d \r\n", __FUNCTION__, u4IfIndex);
        return NULL;
    }

    switch (i1Type)
    {
        case APP_PRI_ADMIN:
            pAppPriMappingTbl = &(APP_PRI_ADM_TBL (pPortEntry, i4Selector - 1));
            break;
        case APP_PRI_LOCAL:
            pAppPriMappingTbl = APP_PRI_LOC_TBL (pPortEntry, i4Selector - 1);
            break;
        default:                /*APP_PRI_REMOTE */
            pAppPriMappingTbl = &(APP_PRI_REM_TBL (pPortEntry, i4Selector - 1));
            break;
    }

    /* 1. Allocate a memory block for the New Entry */
    pAppPriEntry = (tAppPriMappingEntry *)
        MemAllocMemBlk (gAppPriGlobalInfo.AppPriMappingPoolId);
    if (pAppPriEntry == NULL)
    {
        DCBX_TRC_ARG4 ((DCBX_RESOURCE_TRC | DCBX_FAILURE_TRC),
                       "In %s :MemAllocMemBlk() Failed, "
                       "for port %d Selector %d Application %d\r\n",
                       __FUNCTION__, u4IfIndex, i4Selector, i4Protocol);
        return (NULL);
    }

    /* 2. Initialize the AppPri Port Entry */
    /*MEMSET (pAppPriEntry, DCBX_INIT_VAL, sizeof (tAppPriMappingEntry)); */
    TMO_SLL_Init_Node (&(pAppPriEntry->AppPriMappingEntry));
    pAppPriEntry->i4AppPriProtocol = i4Protocol;

    /* 3. Add this New Entry into the Application Prioirty Mapping  Table */

    /* If the list is empty insert the node at end */
    if (TMO_SLL_Count (pAppPriMappingTbl) == DCBX_ZERO)
    {
        TMO_SLL_Add (pAppPriMappingTbl, &(pAppPriEntry->AppPriMappingEntry));
        return (pAppPriEntry);
    }

    /*Insert the node in lexicographical ascending order */
    TMO_SLL_Scan (pAppPriMappingTbl, pNextAppPriEntry, tAppPriMappingEntry *)
    {
        /* */
        if ((pPrevAppPriEntry == NULL) && (pNextAppPriEntry->i4AppPriProtocol >
                                           pAppPriEntry->i4AppPriProtocol))
        {
            TMO_SLL_Insert_In_Middle (pAppPriMappingTbl,
                                      &(pAppPriMappingTbl->Head),
                                      &(pAppPriEntry->AppPriMappingEntry),
                                      &(pNextAppPriEntry->AppPriMappingEntry));
            return (pAppPriEntry);
        }

        if (pNextAppPriEntry->i4AppPriProtocol > pAppPriEntry->i4AppPriProtocol)
        {
            TMO_SLL_Insert_In_Middle (pAppPriMappingTbl,
                                      &(pPrevAppPriEntry->AppPriMappingEntry),
                                      &(pAppPriEntry->AppPriMappingEntry),
                                      &(pNextAppPriEntry->AppPriMappingEntry));
            return (pAppPriEntry);
        }
        pPrevAppPriEntry = pNextAppPriEntry;
    }

    TMO_SLL_Add (pAppPriMappingTbl, &(pAppPriEntry->AppPriMappingEntry));

    return (pAppPriEntry);
}

/***************************************************************************
 *  FUNCTION NAME : AppPriUtlDeleteAllAppPriMappingEntries
 * 
 *  DESCRIPTION   : This utility function is used to delete all 
 *                  the Application Priority mapping entries
 *                  for a particular port.
 * 
 *  INPUT         : None
 * 
 *  OUTPUT        : None
 * 
 *  RETURNS       : None
 * 
 * **************************************************************************/
PUBLIC VOID
AppPriUtlDeleteAllAppPriMappingEntries (UINT4 u4IfIndex)
{
    tAppPriPortEntry   *pPortEntry = NULL;
    tAppPriMappingEntry *pNextAppPriEntry = NULL;
    tAppPriMappingEntry *pTempAppPriEntry = NULL;
    tTMO_SLL           *pAppPriMappingTbl = NULL;
    INT4                i4Selector = DCBX_ZERO;

    pPortEntry = AppPriUtlGetPortEntry (u4IfIndex);
    if (pPortEntry == NULL)
    {
        DCBX_TRC_ARG2 (DCBX_FAILURE_TRC, "%s: "
                       "AppPri not present on port: %d\n", __func__, u4IfIndex);
        return;
    }

    for (i4Selector = DCBX_ONE; i4Selector <= APP_PRI_MAX_SELECTOR;
         i4Selector++)
    {
        pAppPriMappingTbl = &(APP_PRI_ADM_TBL (pPortEntry, i4Selector - 1));
        if (TMO_SLL_Count (pAppPriMappingTbl) != DCBX_ZERO)
        {
            TMO_DYN_SLL_Scan (pAppPriMappingTbl,
                              pNextAppPriEntry, pTempAppPriEntry,
                              tAppPriMappingEntry *)
            {
                TMO_SLL_Delete (pAppPriMappingTbl,
                                &pNextAppPriEntry->AppPriMappingEntry);

                AppPriUtlDelFn (&pNextAppPriEntry->AppPriMappingEntry);

                /* To supress warning, no affect in functionality */
                pNextAppPriEntry = pTempAppPriEntry;
            }
        }
        TMO_SLL_Init (&(APP_PRI_ADM_TBL (pPortEntry, i4Selector - 1)));

        pAppPriMappingTbl = NULL;
        pNextAppPriEntry = NULL;
        pTempAppPriEntry = NULL;

        pAppPriMappingTbl = &(APP_PRI_REM_TBL (pPortEntry, i4Selector - 1));
        if (TMO_SLL_Count (pAppPriMappingTbl) != DCBX_ZERO)
        {
            TMO_DYN_SLL_Scan (pAppPriMappingTbl,
                              pNextAppPriEntry, pTempAppPriEntry,
                              tAppPriMappingEntry *)
            {
                TMO_SLL_Delete (pAppPriMappingTbl,
                                &pNextAppPriEntry->AppPriMappingEntry);

                AppPriUtlDelFn (&pNextAppPriEntry->AppPriMappingEntry);

                /* To supress warning, no affect in functionality */
                pNextAppPriEntry = pTempAppPriEntry;
            }
        }
        TMO_SLL_Init (&(APP_PRI_REM_TBL (pPortEntry, i4Selector - 1)));

        APP_PRI_LOC_TBL (pPortEntry, i4Selector - 1) = NULL;

        pAppPriMappingTbl = NULL;
        pNextAppPriEntry = NULL;
        pTempAppPriEntry = NULL;
    }
    return;
}

/***************************************************************************
 *  FUNCTION NAME : AppPriUtlDeleteRemoteMappingEntries
 * 
 *  DESCRIPTION   : This utility function is used to delete all 
 *                  the Application Priority mapping entries
 *                  for a particular port.
 * 
 *  INPUT         : None
 * 
 *  OUTPUT        : None
 * 
 *  RETURNS       : None
 * 
 * **************************************************************************/
PUBLIC VOID
AppPriUtlDeleteRemoteMappingEntries (UINT4 u4IfIndex)
{
    tAppPriPortEntry   *pPortEntry = NULL;
    tAppPriMappingEntry *pNextAppPriEntry = NULL;
    tAppPriMappingEntry *pTempAppPriEntry = NULL;
    tTMO_SLL           *pAppPriMappingTbl = NULL;
    INT4                i4Selector = DCBX_ZERO;
    UINT1               u1OperVersion = DCBX_ZERO;

    pPortEntry = AppPriUtlGetPortEntry (u4IfIndex);
    if (pPortEntry == NULL)
    {
        /* No entry present */
        return;
    }

    for (i4Selector = DCBX_ONE; i4Selector <= APP_PRI_MAX_SELECTOR;
         i4Selector++)
    {
        DcbxUtlGetOperVersion (pPortEntry->u4IfIndex, &u1OperVersion);
        if (u1OperVersion == DCBX_VER_CEE)
        {
            /* For CEE Selector 2 = TCP and 3 = UDP CEE is not valid */
            if ((i4Selector == DCBX_TWO) || (i4Selector == DCBX_THREE))
            {
                continue;
            }
        }

        pAppPriMappingTbl = &(APP_PRI_REM_TBL (pPortEntry, i4Selector - 1));
        if (TMO_SLL_Count (pAppPriMappingTbl) == DCBX_ZERO)
        {
            /* If no entries are present, check for next selector */
            continue;
        }

        TMO_DYN_SLL_Scan (pAppPriMappingTbl,
                          pNextAppPriEntry, pTempAppPriEntry,
                          tAppPriMappingEntry *)
        {
            TMO_SLL_Delete (&APP_PRI_REM_TBL (pPortEntry, i4Selector - 1),
                            &pNextAppPriEntry->AppPriMappingEntry);

            AppPriUtlDelFn (&pNextAppPriEntry->AppPriMappingEntry);

            /* To supress warning, no affect in functionality */
            pNextAppPriEntry = pTempAppPriEntry;
        }

        TMO_SLL_Init (&(APP_PRI_REM_TBL (pPortEntry, i4Selector - 1)));

    }
    return;
}

/*****************************************************************************/
/* Function Name      : AppPriUtlDeleteAppPriMappingEntry                    */
/* Description        : This function is used to Delete an Entry from the    */
/*                      Application Priority Port Table ,it will do the      */
/*                      following Action                                     */
/*                      1. Remove the Entry from the Application Priority    */
/*                         Port Table                                        */
/*                      2. Clear the Removed Entry                           */
/*                      3. Release the Removed Entry's memory                */
/* Input(s)           : pAppPortEntry   - Pointer to  AppPriPortEntry        */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : gAppPriGlobalInfo                                    */
/* Global Variables                                                          */
/* Modified           : gAppPriGlobalInfo                                    */
/* Return Value(s)    : OSIX_SUCCESS or OSIX_FAILURE                         */
/* Called By          : SNMP Low Level                                       */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
AppPriUtlDeleteAppPriMappingEntry (UINT4 u4IfIndex, INT4 i4Selector,
                                   INT4 i4Protocol, UINT1 u1Type)
{
    tAppPriMappingEntry *pAppPriMappingEntry = NULL;
    tAppPriPortEntry   *pAppPriPortEntry = NULL;
    tTMO_SLL           *pAppPriMappingTbl = NULL;

    pAppPriPortEntry = AppPriUtlGetPortEntry (u4IfIndex);
    if (pAppPriPortEntry == NULL)
    {
        return OSIX_FAILURE;
    }

    switch (u1Type)
    {
        case APP_PRI_ADMIN:
            pAppPriMappingEntry = AppPriUtlGetAppPriMappingEntry (u4IfIndex,
                                                                  i4Selector,
                                                                  i4Protocol,
                                                                  APP_PRI_ADMIN);
            pAppPriMappingTbl =
                &(APP_PRI_ADM_TBL (pAppPriPortEntry, i4Selector - 1));
            break;
        case APP_PRI_LOCAL:
            pAppPriMappingEntry = AppPriUtlGetAppPriMappingEntry (u4IfIndex,
                                                                  i4Selector,
                                                                  i4Protocol,
                                                                  APP_PRI_LOCAL);
            pAppPriMappingTbl =
                APP_PRI_LOC_TBL (pAppPriPortEntry, i4Selector - 1);
            break;
        case APP_PRI_REMOTE:
            pAppPriMappingEntry = AppPriUtlGetAppPriMappingEntry (u4IfIndex,
                                                                  i4Selector,
                                                                  i4Protocol,
                                                                  APP_PRI_REMOTE);
            pAppPriMappingTbl =
                &(APP_PRI_REM_TBL (pAppPriPortEntry, i4Selector - 1));
            break;
        default:
            break;
    }

    if (pAppPriMappingEntry == NULL)
    {
        return OSIX_FAILURE;
    }

    /* Remove Entry from the Application Prioirty Port Table */
    TMO_SLL_Delete (pAppPriMappingTbl,
                    &(pAppPriMappingEntry->AppPriMappingEntry));
    /* Release the Removed Entry's memory */
    MemReleaseMemBlock (gAppPriGlobalInfo.AppPriMappingPoolId,
                        (UINT1 *) (pAppPriMappingEntry));
    return (OSIX_SUCCESS);
}

/***************************************************************************
 *  FUNCTION NAME : AppPriUtlAdminParamChange
 * 
 *  DESCRIPTION   : This utility is used to handle the Admin Parameters
 *                  Change. It will indicate to the DCBX if Admin mode is 
 *                  Auto.
 * 
 *  INPUT         : pPortEntry - Applciation Priority Port Entry.
 * 
 *  OUTPUT        : None
 * 
 *  RETURNS       : None
 * 
 ***************************************************************************/
PUBLIC VOID
AppPriUtlAdminParamChange (tAppPriPortEntry * pPortEntry)
{
    UINT1               u1Version = 0;

    DcbxUtlGetOperVersion (pPortEntry->u4IfIndex, &u1Version);
    /* If Module status is disabled or Mode is OFF then no need to send 
     * the Admin Param change indication to the DCBX since the 
     * application itself would have not Registered with DCBX during 
     * the Module Disable.But need to update local param */
    if (pPortEntry->u1AppPriDcbxOperState == APP_PRI_OPER_OFF)
    {
        AppPriUtlUpdateLocalParam (pPortEntry, APP_PRI_OPER_OFF);
        if (u1Version == DCBX_VER_IEEE)
        {
            /* If the operating version is IEEE and if the mode is OFF
             * no need to send the updated TLV hence return from here */
            return;
        }
    }

    /* If Oper state is Reco then no need to update the local paramters 
     * as well as hardware since it will take remote parameters.
     * If Oper state is in INIT then update the Local paramters */
    if (pPortEntry->u1AppPriDcbxOperState == APP_PRI_OPER_INIT)
    {
        AppPriUtlUpdateLocalParam (pPortEntry, APP_PRI_OPER_INIT);

        if (AppPriUtlHwConfig (pPortEntry, APP_PRI_HW_CREATE) == OSIX_FAILURE)
        {
            DCBX_TRC_ARG2 (DCBX_CRITICAL_TRC, "In func %s: "
                           "OperState == INIT/LCLCFG, Failed to create "
                           "Hw Configuration for "
                           "IfIndex :%d!!!\r\n", __func__,
                           pPortEntry->u4IfIndex);
        }
    }

    /* If the Admin mode is AUTO then need to send the Admin Param
     * change indication to the DCBX with the TLV Updates  */

    /* If the TLV Tx status is not enabled then no need to send 
     * the Admin Param change indication to DCBX*/
    if ((pPortEntry->u1AppPriAdminMode == APP_PRI_PORT_MODE_AUTO) &&
        (pPortEntry->u1AppPriTxStatus != APP_PRI_DISABLED))
    {
        if (u1Version == DCBX_VER_CEE)
        {
            DCBX_TRC_ARG1 (DCBX_TLV_TRC,
                           "AppPriUtlAdminParamChange: Change in Admn"
                           "params for port[%d]. Update and Send the CEE TLV!!!\r\n",
                           pPortEntry->u4IfIndex);
            CEEFormAndSendTLV (pPortEntry->u4IfIndex, DCBX_LLDP_PORT_UPDATE,
                               DCBX_ZERO);
        }
        else if (u1Version == DCBX_VER_IEEE)
        {
            DCBX_TRC_ARG1 (DCBX_TLV_TRC,
                           "AppPriUtlAdminParamChange: Change in Admn"
                           "params for port[%d]. Update and Send Application "
                           "Priority TLV!!!\r\n", pPortEntry->u4IfIndex);
            AppPriUtlFormAndSendAppPriTLV (pPortEntry, DCBX_LLDP_PORT_UPDATE);
        }
    }
    return;
}

/***************************************************************************
 *  FUNCTION NAME : AppPriUtlHandleReRegReqFromDCBX
 * 
 *  DESCRIPTION   : This utility function is used to Handle the Re Reg
 *                  Message from DCBX.
 * 
 *  INPUT         : pPortEntry - Application Priority Port Entry.
 * 
 *  OUTPUT        : None
 * 
 *  RETURNS       : None
 * 
 * **************************************************************************/
PUBLIC VOID
AppPriUtlHandleReRegReqFromDCBX (tAppPriPortEntry * pPortEntry)
{
    /* For Re-Registration, Form the TLV and send it with
     * Port Registration request to the DCBX */
    AppPriUtlFormAndSendAppPriTLV (pPortEntry, DCBX_LLDP_PORT_REG);
    return;

}

/***************************************************************************
 *  FUNCTION NAME : AppPriUtlHandleAgedOutFromDCBX
 * 
 *  DESCRIPTION   : This function is used to handle the Application 
 *                  Priority TLV Age Out Message from DCBX.
 * 
 *  INPUT         : pPortEntry - Application Priority Port Entry.
 * 
 *  OUTPUT        : None
 * 
 *  RETURNS       : None
 * 
 * **************************************************************************/
PUBLIC VOID
AppPriUtlHandleAgedOutFromDCBX (tAppPriPortEntry * pPortEntry)
{
    tDcbxTrapMsg        NotifyInfo;

    MEMSET (&NotifyInfo, DCBX_ZERO, sizeof (tDcbxTrapMsg));
    /* If Age out message is recieved then move the state to INIT 
     * and clear the Remote Parameters */
    DCBX_TRC (DCBX_SEM_TRC, "AppPriUtlHandleAgedOutFromDCBX:"
              " Application Priority Age Out Event is received. "
              "Change the sate to INIT!!!\r\n");
    AppPriUtlOperStateChange (pPortEntry, APP_PRI_OPER_INIT);
    SET_DCBX_STATUS (pPortEntry, pPortEntry->u1AppPriDcbxSemType,
                     DCBX_RED_APP_PRI_DCBX_STATUS_INFO,
                     DCBX_STATUS_PEER_NO_ADV_FEAT);

    /* Send State Change information to Standby Node.
     * so that if it is RECO state it will be moved to
     * INIT in standby also.*/
    DcbxRedSendDynamicAppPriSemInfo (pPortEntry);

    /* Delete the Application Priority Mapping Tbl from the remote information */
    AppPriUtlDeleteRemoteMappingEntries (pPortEntry->u4IfIndex);
    AppPriUtlDeleteRemotePortEntries (pPortEntry->u4IfIndex);

    /* Send Remote table Clear information to Standby Node */
    DcbxRedSendDynamicAppPriRemInfo (pPortEntry);

    /* Send Trap Notification for Peer Down status */
    NotifyInfo.u4IfIndex = pPortEntry->u4IfIndex;
    NotifyInfo.u1Module = APP_PRI_TRAP;
    NotifyInfo.unTrapMsg.u1PeerStatus = APP_PRI_DISABLED;

    DcbxSendNotification (&NotifyInfo, PEER_STATE_TRAP);

    return;
}

/***************************************************************************
 *  FUNCTION NAME : AppPriUtlDelFn
 * 
 *  DESCRIPTION   : This function is used to relealse the memory to Mempool
 * 
 *  INPUT         : pAppPriEntry - Pointer to AppPriMappingEntry
 * 
 *  OUTPUT        : None
 * 
 *  RETURNS       : None
 * 
 * **************************************************************************/
PUBLIC VOID
AppPriUtlDelFn (tTMO_SLL_NODE * pAppPriEntry)
{
    MemReleaseMemBlock (gAppPriGlobalInfo.AppPriMappingPoolId,
                        (UINT1 *) pAppPriEntry);
    return;
}

/***************************************************************************
 *  FUNCTION NAME : AppPriUtlHwConfig
 * 
 *  DESCRIPTION   : This utility is used to configure the Application Priority
 *                  paramters in the hardware.
 * 
 *  INPUT         : pPortEntry - Application priority port Entry.
 *                  u1Status - Enable/Disable status.
 * 
 *  OUTPUT        : None
 * 
 *  RETURNS       : OSIX_SUCCESS/OSIX_FAILURE
 * 
 * **************************************************************************/
PUBLIC INT4
AppPriUtlHwConfig (tAppPriPortEntry * pPortEntry, UINT1 u1Status)
{
    tAppPriMappingEntry *pAppPriMappingEntry = NULL;
    INT4                i4Selector = DCBX_ZERO;
    UINT1               u1OperVersion = DCBX_ZERO;

    /* Update the Operational Application to Priority Mapping 
     * in the hardware */
    for (i4Selector = DCBX_ONE; i4Selector <= APP_PRI_MAX_SELECTOR;
         i4Selector++)
    {
        DcbxUtlGetOperVersion (pPortEntry->u4IfIndex, &u1OperVersion);
        if (u1OperVersion == DCBX_VER_CEE)
        {
            /* For CEE Selector 2 = TCP and 3 = UDP CEE is not valid */
            if ((i4Selector == DCBX_TWO) || (i4Selector == DCBX_THREE))
            {
                DCBX_TRC_ARG2 (DCBX_CONTROL_PLANE_TRC,
                               "In func %s: Skipping Selector %d for CEE!!\r\n",
                               __func__, i4Selector);
                continue;
            }
        }

        TMO_SLL_Scan (APP_PRI_LOC_TBL (pPortEntry, i4Selector - 1),
                      pAppPriMappingEntry, tAppPriMappingEntry *)
        {
            DCBX_TRC_ARG5 (DCBX_CONTROL_PLANE_TRC, "AppPriUtlHwConfig: "
                           "Program Hw with the parameters "
                           "IfIndex :%d Selector : %d Protocol : %d "
                           "Priority: %d and Status of entry :%d "
                           "!!!\r\n", pPortEntry->u4IfIndex, i4Selector,
                           pAppPriMappingEntry->i4AppPriProtocol,
                           pAppPriMappingEntry->u1AppPriPriority, u1Status);

            if (AppPriUtlHwConfigAppPriMappingEntry (pPortEntry->u4IfIndex,
                                                     i4Selector,
                                                     pAppPriMappingEntry,
                                                     u1Status) == OSIX_FAILURE)
            {
                DCBX_TRC_ARG5 (DCBX_CRITICAL_TRC, "In AppPriUtlHwConfig : "
                               "AppPriUtlHwConfigAppPriMappingEntry returned "
                               "Failure, HW not programmed for IfIndex: %d "
                               "Selector: %d Protocol: %d Priority: %d "
                               "Create(1)/Del(2): %d !!!",
                               pPortEntry->u4IfIndex, i4Selector,
                               pAppPriMappingEntry->i4AppPriProtocol,
                               pAppPriMappingEntry->u1AppPriPriority, u1Status);
                return OSIX_FAILURE;
            }
        }
    }
    return OSIX_SUCCESS;
}

/***************************************************************************
 *  FUNCTION NAME : AppPriUtlHwConfigAppPriMappingEntry
 * 
 *  DESCRIPTION   : This utility is used to configure the Application Priority
 *                  paramters in the hardware.
 * 
 *  INPUT         : pAppPriEntry - Application Priority Mapping Entry
 *                  u4IfIndex    - Interface Index
 *                  u1Status - Enable/Disable status.
 * 
 *  OUTPUT        : None
 * 
 *  RETURNS       : OSIX_SUCCESS/OSIX_FAILURE
 * 
 * **************************************************************************/
PUBLIC INT4
AppPriUtlHwConfigAppPriMappingEntry (UINT4 u4IfIndex, INT4 i4Selector,
                                     tAppPriMappingEntry * pAppPriEntry,
                                     UINT1 u1Status)
{
    tIssAclHwFilterInfo AclFilterInfo;

    MEMSET (&AclFilterInfo, DCBX_ZERO, sizeof (tIssAclHwFilterInfo));

    DCBX_TRC_ARG5 (DCBX_CONTROL_PLANE_TRC,
                   "AppPriUtlHwConfigAppPriMappingEntry:"
                   "Paramters for Hw Configuration are "
                   "IfIndex :%d Selector : %d Protocol : %d "
                   "Priority: %d and Status of entry :%d "
                   "!!!\r\n", u4IfIndex, i4Selector,
                   pAppPriEntry->i4AppPriProtocol,
                   pAppPriEntry->u1AppPriPriority, u1Status);

    AclFilterInfo.u1Priority = ISS_FILTER_MAX_PRIO;    /* 255 */
    AclFilterInfo.u1TagType = ISS_FILTER_SINGLE_TAG;

    /* If Selector is Ethertype (one), fill filtertype with L2Filter
     * else fill filter type with L3 Filter*/
    if (i4Selector == DCBX_ONE)
    {
        AclFilterInfo.u4Protocol = (UINT4) pAppPriEntry->i4AppPriProtocol;
        AclFilterInfo.u4PortNo = u4IfIndex;

        if (AppPriUtlConfigHwMapping
            (ISS_L2_FILTER, pAppPriEntry, &AclFilterInfo,
             u1Status) == OSIX_FAILURE)
        {
            DCBX_TRC_ARG2 (DCBX_CONTROL_PLANE_TRC, "In %s : "
                           "AppPriUtlConfigHwMapping returned "
                           "Failure for L2 Filter for IfIndex :%d ",
                           __func__, u4IfIndex);
            return OSIX_FAILURE;
        }
    }
    else if ((i4Selector == DCBX_TWO) || (i4Selector == DCBX_THREE))
    {
        AclFilterInfo.u4PortNo = (UINT4) pAppPriEntry->i4AppPriProtocol;
        AclFilterInfo.u4L3PortNo = u4IfIndex;

        if (i4Selector == DCBX_TWO)
        {
            AclFilterInfo.u4Protocol = ISS_TCP;
        }
        else if (i4Selector == DCBX_THREE)
        {
            AclFilterInfo.u4Protocol = ISS_UDP;
        }

        if (AppPriUtlConfigHwMapping
            (ISS_L3_FILTER, pAppPriEntry, &AclFilterInfo,
             u1Status) == OSIX_FAILURE)
        {
            DCBX_TRC_ARG3 (DCBX_CONTROL_PLANE_TRC, "In %s : "
                           "AppPriUtlConfigHwMapping returned "
                           "Failure for IfIndex :%d selector :%d",
                           __func__, u4IfIndex, i4Selector);
            return OSIX_FAILURE;
        }
    }
    else
    {
        AclFilterInfo.u4PortNo = (UINT4) pAppPriEntry->i4AppPriProtocol;
        AclFilterInfo.u4L3PortNo = u4IfIndex;
        AclFilterInfo.u4Protocol = ISS_TCP;

        if (AppPriUtlConfigHwMapping
            (ISS_L3_FILTER, pAppPriEntry, &AclFilterInfo,
             u1Status) == OSIX_FAILURE)
        {
            DCBX_TRC_ARG2 (DCBX_CONTROL_PLANE_TRC, "In %s : "
                           "AppPriUtlConfigHwMapping returned "
                           "Failure for setting TCP in HW for IfIndex :%d ",
                           __func__, u4IfIndex);
            return OSIX_FAILURE;
        }

        AclFilterInfo.u4PortNo = (UINT4) pAppPriEntry->i4AppPriProtocol;
        AclFilterInfo.u4L3PortNo = u4IfIndex;
        AclFilterInfo.u4Protocol = ISS_UDP;

        if (AppPriUtlConfigHwMapping
            (ISS_L3_FILTER, pAppPriEntry, &AclFilterInfo,
             u1Status) == OSIX_FAILURE)
        {
            DCBX_TRC_ARG2 (DCBX_CONTROL_PLANE_TRC, "In %s : "
                           "AppPriUtlConfigHwMapping returned "
                           "Failure for setting UDP in Hw for IfIndex :%d ",
                           __func__, u4IfIndex);
            return OSIX_FAILURE;
        }
    }

    return OSIX_SUCCESS;
}

/***************************************************************************
 *  FUNCTION NAME : AppPriUtlConfigHwMapping 
 * 
 *  DESCRIPTION   : This utility is used to configure the Application Priority
 *                  paramters in the hardware.
 * 
 *  INPUT         : pAppPriEntry - Application Priority Mapping Entry
 *                  u4IfIndex    - Interface Index
 *                  u1Status - Enable/Disable status.
 * 
 *  OUTPUT        : None
 * 
 *  RETURNS       : OSIX_SUCCESS/OSIX_FAILURE
 * 
 * **************************************************************************/
INT4
AppPriUtlConfigHwMapping (UINT1 u1FilterType,
                          tAppPriMappingEntry * pAppPriEntry,
                          tIssAclHwFilterInfo * pAclFilterInfo, UINT1 u1Status)
{
    tQosClassMapPriInfo QosClassMapPriInfo;

    MEMSET (&QosClassMapPriInfo, DCBX_ZERO, sizeof (tQosClassMapPriInfo));

    /* This function creates or updates a L2 or L3 Filter based on the 
     * FilterType and the FilterId created is updated in the 
     * structure AclFilterInfo */
    if (u1Status == APP_PRI_HW_CREATE)
    {
        DCBX_TRC_ARG1 (DCBX_CONTROL_PLANE_TRC,
                       "Call IssACLApiModifyFilterEntry() with Filter = %d\r\n",
                       u1FilterType);

        if (IssACLApiModifyFilterEntry (pAclFilterInfo, u1FilterType, TRUE) ==
            ISS_FAILURE)
        {
            /*Print trace and return success - Failure will happen in two cases:
             * 1) If entry already exists
             * 2) Actual failure to program HW. 
             */
            DCBX_TRC_ARG2 (DCBX_CONTROL_PLANE_TRC,
                           "In func %s: IssACLApiModifyFilterEntry"
                           "returned due to entry already exists "
                           "for port[%d]\r\n", __func__,
                           pAclFilterInfo->u4PortNo);
            return OSIX_SUCCESS;
        }
        /*Qos Configuration */
        QosClassMapPriInfo.u4FilterId = pAclFilterInfo->u4FilterId;
        QosClassMapPriInfo.u1FilterType = u1FilterType;
        QosClassMapPriInfo.u1Priority = pAppPriEntry->u1AppPriPriority;
        if (u1FilterType == ISS_L2_FILTER)
        {
            QosClassMapPriInfo.u4IfIndex = pAclFilterInfo->u4PortNo;
        }
        else
        {
            QosClassMapPriInfo.u4IfIndex = pAclFilterInfo->u4L3PortNo;
        }

        /* This function does the following for FilterAction = ISS_FILTER_CREATE:
         * 1. Creates a class map entry with the next free
         * class map Id and maps the filter created to that class.
         * 2. Creates a class to priority map entry and 
         * assigns the priority for the class created.
         * if FilterAction = ISS_FILTER_MODIFIED then,
         * Get the class map entry correponding to the filter Id and set the 
         * prority to the class.
         */

        DCBX_TRC_ARG5 (DCBX_CONTROL_PLANE_TRC,
                       "In %s : Line = %d "
                       "FilterId = %d u1FilterType = %d u1Priority = %d\r\n",
                       __FUNCTION__, __LINE__,
                       QosClassMapPriInfo.u4FilterId,
                       QosClassMapPriInfo.u1FilterType,
                       QosClassMapPriInfo.u1Priority);
        if (QosApiConfigAppPri (&QosClassMapPriInfo,
                                pAclFilterInfo->u1FilterAction) == QOS_FAILURE)
        {
            /*Print Critical trace and return success */
            DCBX_TRC_ARG1 ((DCBX_CRITICAL_TRC | DCBX_FAILURE_TRC),
                           "In %s : QosApiConfigAppPri "
                           "returned failure\r\n", __FUNCTION__);
            return OSIX_SUCCESS;
        }
    }
    else
    {
        DCBX_TRC (DCBX_CONTROL_PLANE_TRC,
                  "IssACLApiModifyFilterEntry Delete" "\r\n");
        if (IssACLApiGetFilterEntry (pAclFilterInfo, u1FilterType, FALSE) ==
            ISS_FAILURE)
        {
            DCBX_TRC_ARG1 (DCBX_FAILURE_TRC,
                           "In Func %s : IssAclApiGetAppPri: "
                           "Filter does not exist.\r\n", __FUNCTION__);
            return OSIX_SUCCESS;
        }

        /* Firt delete CLASS MAP then the filter */
        /* Qos Configuration */
        QosClassMapPriInfo.u4FilterId = pAclFilterInfo->u4FilterId;
        QosClassMapPriInfo.u1FilterType = u1FilterType;
        QosClassMapPriInfo.u1Priority = pAppPriEntry->u1AppPriPriority;
        if (u1FilterType == ISS_L2_FILTER)
        {
            QosClassMapPriInfo.u4IfIndex = pAclFilterInfo->u4PortNo;
        }
        else
        {
            QosClassMapPriInfo.u4IfIndex = pAclFilterInfo->u4L3PortNo;
        }

        /* This function does the following :
         * 1. Deletes the Class-map entry created for this filter
         * 2. Deletes the corresponding Class to priority map entry 
         */
        if (QosApiConfigAppPri (&QosClassMapPriInfo,
                                pAclFilterInfo->u1FilterAction) == QOS_FAILURE)
        {
            /* Print Critical trace and return success */
            DCBX_TRC_ARG1 ((DCBX_CRITICAL_TRC | DCBX_FAILURE_TRC),
                           "In %s : QosApiConfigAppPri "
                           "returned failure. QOS configuration failed "
                           "to apply!!!\r\n", __FUNCTION__);
            return OSIX_SUCCESS;
        }

        /* Delete the filter entry created for this Application 
         * Priority Mapping */
        if (IssACLApiModifyFilterEntry (pAclFilterInfo, u1FilterType, FALSE) ==
            ISS_FAILURE)
        {
            /*Print Critical trace and return success */
            DCBX_TRC_ARG1 ((DCBX_CRITICAL_TRC | DCBX_FAILURE_TRC),
                           "In %s : IssACLApiModifyFilterEntry returned "
                           "failure. ACL failed to configured!!!\r\n",
                           __FUNCTION__);
            return OSIX_SUCCESS;
        }
    }

    return OSIX_SUCCESS;
}

/***************************************************************************
 *  FUNCTION NAME : AppPriUtlDumpPkt
 * 
 *  DESCRIPTION   : This function is used to dump the dcbx packet.
 * 
 *  INPUT         : pu1Buf - Packet Buffer
 *                  u1Len  - Length of the packet buffer
 * 
 *  OUTPUT        : None
 * 
 *  RETURNS       : None
 * 
 * **************************************************************************/
PUBLIC VOID
AppPriUtlDumpPkt (UINT1 *pu1Buf, UINT4 u4Length)
{
    UINT4               u4Count = DCBX_ZERO;

    DCBX_PRINT (DCBX_ALL_FLAG, DCBX_ALL_FLAG, DCBX_MOD_NAME,
                "Buffer Content:\r\n");
    for (u4Count = DCBX_ZERO; u4Count < u4Length; u4Count++)
    {
        DCBX_PRINT (DCBX_ALL_FLAG, DCBX_ALL_FLAG, DCBX_MOD_NAME, "%x ",
                    pu1Buf[u4Count]);
    }
    DCBX_PRINT (DCBX_ALL_FLAG, DCBX_ALL_FLAG, DCBX_MOD_NAME, "\r\n");
}

/*****************************************************************************/
/* Function Name      : AppPriUtlRegOrDeRegBasedOnMode                       */
/*                                                                           */
/* Description        : This function is used to Register or DeRegister      */
/*                      based on the Mode and ENABLE/DISABLE status          */
/*                                                                           */
/* Input(s)           : pPortEntry- Pointer to the AppPri entry              */
/*                      u1Status  - ENABLE/DISABLE                           */
/*                                                                           */
/* Output(s)          : None.                                                */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
AppPriUtlRegOrDeRegBasedOnMode (tAppPriPortEntry * pPortEntry, UINT1 u1Status)
{
    UINT1               u1DcbxMode = DCBX_ZERO;

    DcbxUtlGetDCBXMode (pPortEntry->u4IfIndex, &u1DcbxMode);

    DCBX_TRC_ARG3 (DCBX_TLV_TRC, "In func AppPriUtlRegOrDeRegBasedOnMode: "
                   "Port = %d, DcbxMode = %s, Status = %s\r\n",
                   pPortEntry->u4IfIndex,
                   DCBX_GET_MODE (u1DcbxMode),
                   DCBX_GET_APP_PRI_MODE (u1Status));

    if (u1Status == APP_PRI_ENABLED)
    {
        if (u1DcbxMode == DCBX_MODE_IEEE)
        {
            AppPriUtlRegisterApplForPort (pPortEntry);
        }
        else if (u1DcbxMode == DCBX_MODE_CEE)
        {
            CEERegisterApplForPort (pPortEntry->u4IfIndex,
                                    CEE_APP_PRI_TLV_TYPE);
        }
        else
        {
            AppPriUtlRegisterApplForPort (pPortEntry);
            CEERegisterApplForPort (pPortEntry->u4IfIndex,
                                    CEE_APP_PRI_TLV_TYPE);
        }
        SET_DCBX_STATUS (pPortEntry, pPortEntry->u1AppPriDcbxSemType,
                         DCBX_RED_APP_PRI_DCBX_STATUS_INFO,
                         DCBX_STATUS_PEER_NO_ADV_DCBX);
    }
    else
    {
        if (u1DcbxMode == DCBX_MODE_IEEE)
        {
            AppPriUtlDeRegisterApplForPort (pPortEntry);
        }
        else if (u1DcbxMode == DCBX_MODE_CEE)
        {
            CEEDeRegisterApplForPort (pPortEntry->u4IfIndex,
                                      CEE_APP_PRI_TLV_TYPE);
            /* Clearing the remote parameter after Deregistration */
            AppPriUtlDeleteRemoteMappingEntries (pPortEntry->u4IfIndex);
            AppPriUtlDeleteRemotePortEntries (pPortEntry->u4IfIndex);
        }
        else
        {
            AppPriUtlDeRegisterApplForPort (pPortEntry);
            CEEDeRegisterApplForPort (pPortEntry->u4IfIndex,
                                      CEE_APP_PRI_TLV_TYPE);
        }
        SET_DCBX_STATUS (pPortEntry, pPortEntry->u1AppPriDcbxSemType,
                         DCBX_RED_APP_PRI_DCBX_STATUS_INFO,
                         DCBX_STATUS_NOT_ADVERTISE);
    }

    return;
}

/***************************************************************************
 *  FUNCTION NAME : AppPriUtlDeletePort 
 * 
 *  DESCRIPTION   : This utility function is used to delete the AppPri Port Entry.
 * 
 *  INPUT         : u4IfIndex - Port Number
 * 
 *  OUTPUT        : None
 * 
 *  RETURNS       : None
 * 
 * **************************************************************************/
PUBLIC VOID
AppPriUtlDeletePort (UINT4 u4IfIndex)
{
    tAppPriPortEntry   *pPortEntry = NULL;

    pPortEntry = AppPriUtlGetPortEntry (u4IfIndex);
    if (pPortEntry == NULL)
    {
        DCBX_TRC_ARG2 (DCBX_FAILURE_TRC, "%s: "
                       " AppPri Node not present on Port %d!!!\r\n", __func__,
                       u4IfIndex);
        return;
    }
    AppPriUtlDeletePortTblEntry (pPortEntry);
    return;
}

/************ END OF FILE ************/
