/********************************************************************
* Copyright (C) 2010 Aricent Inc . All Rights Reserved
*
* $Id: fsdcbxwr.c,v 1.11 2016/07/09 09:41:21 siva Exp $
*
* Description: Protocol Mid Level Routines
*********************************************************************/

# include  "lr.h"
# include  "fssnmp.h"
# include  "fsdcbxwr.h"
# include  "fsdcbxdb.h"
# include  "dcbxinc.h"

VOID
RegisterFSDCBX ()
{
    SNMPRegisterMibWithLock (&fsdcbxOID, &fsdcbxEntry, DcbxLock, DcbxUnLock, 
                     SNMP_MSR_TGR_TRUE);
    SNMPAddSysorEntry (&fsdcbxOID, (const UINT1 *) "fsdcbx");
}

VOID
UnRegisterFSDCBX ()
{
    SNMPUnRegisterMib (&fsdcbxOID, &fsdcbxEntry);
    SNMPDelSysorEntry (&fsdcbxOID, (const UINT1 *) "fsdcbx");
}

INT4
FsDcbPfcMinThresholdGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsDcbPfcMinThreshold (&(pMultiData->u4_ULongValue)));
}

INT4
FsDcbPfcMaxThresholdGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsDcbPfcMaxThreshold (&(pMultiData->u4_ULongValue)));
}

INT4
FsDcbMaxPfcProfilesGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsDcbMaxPfcProfiles (&(pMultiData->u4_ULongValue)));
}

INT4
FsDcbPfcMinThresholdSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsDcbPfcMinThreshold (pMultiData->u4_ULongValue));
}

INT4
FsDcbPfcMaxThresholdSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsDcbPfcMaxThreshold (pMultiData->u4_ULongValue));
}

INT4
FsDcbPfcMinThresholdTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                          tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsDcbPfcMinThreshold
            (pu4Error, pMultiData->u4_ULongValue));
}

INT4
FsDcbPfcMaxThresholdTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                          tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsDcbPfcMaxThreshold
            (pu4Error, pMultiData->u4_ULongValue));
}

INT4
FsDcbPfcMinThresholdDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                         tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsDcbPfcMinThreshold
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsDcbPfcMaxThresholdDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                         tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsDcbPfcMaxThreshold
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFsDcbPortTable (tSnmpIndex * pFirstMultiIndex,
                            tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsDcbPortTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsDcbPortTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}
INT4 FsDcbPFCAdminStatusGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsDcbPortTable(
		pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsDcbPFCAdminStatus(
		pMultiIndex->pIndex[0].i4_SLongValue,
		&(pMultiData->i4_SLongValue)));
}
INT4
FsDcbETSAdminStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsDcbPortTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsDcbETSAdminStatus (pMultiIndex->pIndex[0].i4_SLongValue,
                                       &(pMultiData->i4_SLongValue)));

}

INT4 FsDcbAppPriAdminStatusGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsDcbPortTable(
		pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
	return(nmhGetFsDcbAppPriAdminStatus(
		pMultiIndex->pIndex[0].i4_SLongValue,
        &(pMultiData->i4_SLongValue)));

}

INT4 FsDcbOperVersionGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsDcbPortTable(
		pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsDcbOperVersion(
		pMultiIndex->pIndex[0].i4_SLongValue,
		&(pMultiData->i4_SLongValue)));
}

INT4 FsDcbMaxVersionGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsDcbPortTable(
		pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
	return(nmhGetFsDcbMaxVersion(
		pMultiIndex->pIndex[0].i4_SLongValue,
		&(pMultiData->i4_SLongValue)));

}
INT4 FsDcbPeerOperVersionGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsDcbPortTable(
		pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
	return(nmhGetFsDcbPeerOperVersion(
		pMultiIndex->pIndex[0].i4_SLongValue,
		&(pMultiData->i4_SLongValue)));

}
INT4 FsDcbPeerMaxVersionGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsDcbPortTable(
		pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return(nmhGetFsDcbPeerMaxVersion(
                pMultiIndex->pIndex[0].i4_SLongValue,
                &(pMultiData->i4_SLongValue)));

}
INT4
FsDcbPortTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                   tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsDcbPortTable (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsDcbRowStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsDcbPortTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsDcbRowStatus (pMultiIndex->pIndex[0].i4_SLongValue,
                                  &(pMultiData->i4_SLongValue)));

}

INT4
FsDcbRowStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsDcbRowStatus (pMultiIndex->pIndex[0].i4_SLongValue,
                                  pMultiData->i4_SLongValue));

}

INT4
FsDcbRowStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                    tRetVal * pMultiData)
{
    return (nmhTestv2FsDcbRowStatus (pu4Error,
                                     pMultiIndex->pIndex[0].i4_SLongValue,
                                     pMultiData->i4_SLongValue));

}

INT4
FsDcbxGlobalTraceLevelGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsDcbxGlobalTraceLevel (&(pMultiData->i4_SLongValue)));
}

INT4
FsDcbxGlobalTraceLevelSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsDcbxGlobalTraceLevel (pMultiData->i4_SLongValue));
}

INT4
FsDcbxGlobalTraceLevelTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                            tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsDcbxGlobalTraceLevel
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FsDcbxGlobalTraceLevelDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                           tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsDcbxGlobalTraceLevel
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFsDCBXPortTable (tSnmpIndex * pFirstMultiIndex,
                             tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsDCBXPortTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsDCBXPortTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
FsDCBXAdminStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsDCBXPortTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsDCBXAdminStatus (pMultiIndex->pIndex[0].i4_SLongValue,
                                     &(pMultiData->i4_SLongValue)));

}

INT4
FsDCBXModeGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsDCBXPortTable(
		pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsDCBXMode(
		pMultiIndex->pIndex[0].i4_SLongValue,
		&(pMultiData->i4_SLongValue)));

}

INT4
FsDCBXAdminStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsDCBXAdminStatus (pMultiIndex->pIndex[0].i4_SLongValue,
                                     pMultiData->i4_SLongValue));

}

INT4
FsDCBXModeSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhSetFsDCBXMode(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiData->i4_SLongValue));

}

INT4
FsDCBXAdminStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                       tRetVal * pMultiData)
{
    return (nmhTestv2FsDCBXAdminStatus (pu4Error,
                                        pMultiIndex->pIndex[0].i4_SLongValue,
                                        pMultiData->i4_SLongValue));

}

INT4
FsDCBXModeTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhTestv2FsDCBXMode(pu4Error,
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiData->i4_SLongValue));

}

INT4
FsDCBXPortTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                    tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsDCBXPortTable (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsETSSystemControlGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsETSSystemControl (&(pMultiData->i4_SLongValue)));
}

INT4
FsETSModuleStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsETSModuleStatus (&(pMultiData->i4_SLongValue)));
}

INT4
FsETSClearCountersGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsETSClearCounters (&(pMultiData->i4_SLongValue)));
}

INT4
FsETSGlobalEnableTrapGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsETSGlobalEnableTrap (&(pMultiData->i4_SLongValue)));
}

INT4
FsETSGeneratedTrapCountGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsETSGeneratedTrapCount (&(pMultiData->u4_ULongValue)));
}

INT4
FsETSSystemControlSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsETSSystemControl (pMultiData->i4_SLongValue));
}

INT4
FsETSModuleStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsETSModuleStatus (pMultiData->i4_SLongValue));
}

INT4
FsETSClearCountersSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsETSClearCounters (pMultiData->i4_SLongValue));
}

INT4
FsETSGlobalEnableTrapSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsETSGlobalEnableTrap (pMultiData->i4_SLongValue));
}

INT4
FsETSGeneratedTrapCountSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsETSGeneratedTrapCount (pMultiData->u4_ULongValue));
}

INT4
FsETSSystemControlTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                        tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsETSSystemControl (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FsETSModuleStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                       tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsETSModuleStatus (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FsETSClearCountersTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                        tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsETSClearCounters (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FsETSGlobalEnableTrapTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                           tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsETSGlobalEnableTrap
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FsETSGeneratedTrapCountTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                             tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsETSGeneratedTrapCount
            (pu4Error, pMultiData->u4_ULongValue));
}

INT4
FsETSSystemControlDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                       tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsETSSystemControl
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsETSModuleStatusDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                      tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsETSModuleStatus
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsETSClearCountersDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                       tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsETSClearCounters
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsETSGlobalEnableTrapDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                          tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsETSGlobalEnableTrap
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsETSGeneratedTrapCountDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                            tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsETSGeneratedTrapCount
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFsETSPortTable (tSnmpIndex * pFirstMultiIndex,
                            tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsETSPortTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsETSPortTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
FsETSAdminModeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsETSPortTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsETSAdminMode (pMultiIndex->pIndex[0].i4_SLongValue,
                                  &(pMultiData->i4_SLongValue)));

}

INT4
FsETSDcbxOperStateGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsETSPortTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsETSDcbxOperState (pMultiIndex->pIndex[0].i4_SLongValue,
                                      &(pMultiData->i4_SLongValue)));

}

INT4
FsETSDcbxStateMachineGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsETSPortTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsETSDcbxStateMachine (pMultiIndex->pIndex[0].i4_SLongValue,
                                         &(pMultiData->i4_SLongValue)));

}

INT4
FsETSClearTLVCountersGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsETSPortTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsETSClearTLVCounters (pMultiIndex->pIndex[0].i4_SLongValue,
                                         &(pMultiData->i4_SLongValue)));

}

INT4
FsETSConfTxTLVCounterGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsETSPortTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsETSConfTxTLVCounter (pMultiIndex->pIndex[0].i4_SLongValue,
                                         &(pMultiData->u4_ULongValue)));

}

INT4
FsETSConfRxTLVCounterGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsETSPortTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsETSConfRxTLVCounter (pMultiIndex->pIndex[0].i4_SLongValue,
                                         &(pMultiData->u4_ULongValue)));

}

INT4
FsETSConfRxTLVErrorsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsETSPortTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsETSConfRxTLVErrors (pMultiIndex->pIndex[0].i4_SLongValue,
                                        &(pMultiData->u4_ULongValue)));

}

INT4
FsETSRecoTxTLVCounterGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsETSPortTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsETSRecoTxTLVCounter (pMultiIndex->pIndex[0].i4_SLongValue,
                                         &(pMultiData->u4_ULongValue)));

}

INT4
FsETSRecoRxTLVCounterGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsETSPortTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsETSRecoRxTLVCounter (pMultiIndex->pIndex[0].i4_SLongValue,
                                         &(pMultiData->u4_ULongValue)));

}

INT4
FsETSRecoRxTLVErrorsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsETSPortTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsETSRecoRxTLVErrors (pMultiIndex->pIndex[0].i4_SLongValue,
                                        &(pMultiData->u4_ULongValue)));

}

INT4
FsETSTcSuppTxTLVCounterGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsETSPortTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsETSTcSuppTxTLVCounter (pMultiIndex->pIndex[0].i4_SLongValue,
                                           &(pMultiData->u4_ULongValue)));

}

INT4
FsETSTcSuppRxTLVCounterGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsETSPortTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsETSTcSuppRxTLVCounter (pMultiIndex->pIndex[0].i4_SLongValue,
                                           &(pMultiData->u4_ULongValue)));

}

INT4
FsETSTcSuppRxTLVErrorsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsETSPortTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsETSTcSuppRxTLVErrors (pMultiIndex->pIndex[0].i4_SLongValue,
                                          &(pMultiData->u4_ULongValue)));

}

INT4
FsETSRowStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsETSPortTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsETSRowStatus (pMultiIndex->pIndex[0].i4_SLongValue,
                                  &(pMultiData->i4_SLongValue)));

}

INT4 FsETSSyncdGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsETSPortTable(
		pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsETSSyncd(
		pMultiIndex->pIndex[0].i4_SLongValue,
		&(pMultiData->i4_SLongValue)));

}

INT4 FsETSDcbxStatusGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsETSPortTable(
		pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsETSDcbxStatus(
		pMultiIndex->pIndex[0].i4_SLongValue,
		&(pMultiData->i4_SLongValue)));

}
INT4 FsETSErrorGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsETSPortTable(
		pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsETSError(
		pMultiIndex->pIndex[0].i4_SLongValue,
		&(pMultiData->i4_SLongValue)));

}
INT4
FsETSAdminModeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsETSAdminMode (pMultiIndex->pIndex[0].i4_SLongValue,
                                  pMultiData->i4_SLongValue));

}

INT4
FsETSClearTLVCountersSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsETSClearTLVCounters (pMultiIndex->pIndex[0].i4_SLongValue,
                                         pMultiData->i4_SLongValue));

}

INT4
FsETSRowStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsETSRowStatus (pMultiIndex->pIndex[0].i4_SLongValue,
                                  pMultiData->i4_SLongValue));

}

INT4
FsETSAdminModeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                    tRetVal * pMultiData)
{
    return (nmhTestv2FsETSAdminMode (pu4Error,
                                     pMultiIndex->pIndex[0].i4_SLongValue,
                                     pMultiData->i4_SLongValue));

}

INT4
FsETSClearTLVCountersTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                           tRetVal * pMultiData)
{
    return (nmhTestv2FsETSClearTLVCounters (pu4Error,
                                            pMultiIndex->pIndex[0].
                                            i4_SLongValue,
                                            pMultiData->i4_SLongValue));

}

INT4
FsETSRowStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                    tRetVal * pMultiData)
{
    return (nmhTestv2FsETSRowStatus (pu4Error,
                                     pMultiIndex->pIndex[0].i4_SLongValue,
                                     pMultiData->i4_SLongValue));

}

INT4
FsETSPortTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                   tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsETSPortTable (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsPFCSystemControlGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsPFCSystemControl (&(pMultiData->i4_SLongValue)));
}

INT4
FsPFCModuleStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsPFCModuleStatus (&(pMultiData->i4_SLongValue)));
}

INT4
FsPFCClearCountersGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsPFCClearCounters (&(pMultiData->i4_SLongValue)));
}

INT4
FsPFCGlobalEnableTrapGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsPFCGlobalEnableTrap (&(pMultiData->i4_SLongValue)));
}

INT4
FsPFCGeneratedTrapCountGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsPFCGeneratedTrapCount (&(pMultiData->u4_ULongValue)));
}

INT4
FsPFCSystemControlSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsPFCSystemControl (pMultiData->i4_SLongValue));
}

INT4
FsPFCModuleStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsPFCModuleStatus (pMultiData->i4_SLongValue));
}

INT4
FsPFCClearCountersSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsPFCClearCounters (pMultiData->i4_SLongValue));
}

INT4
FsPFCGlobalEnableTrapSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsPFCGlobalEnableTrap (pMultiData->i4_SLongValue));
}

INT4
FsPFCGeneratedTrapCountSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsPFCGeneratedTrapCount (pMultiData->u4_ULongValue));
}

INT4
FsPFCSystemControlTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                        tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsPFCSystemControl (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FsPFCModuleStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                       tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsPFCModuleStatus (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FsPFCClearCountersTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                        tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsPFCClearCounters (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FsPFCGlobalEnableTrapTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                           tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsPFCGlobalEnableTrap
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FsPFCGeneratedTrapCountTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                             tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsPFCGeneratedTrapCount
            (pu4Error, pMultiData->u4_ULongValue));
}

INT4
FsPFCSystemControlDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                       tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsPFCSystemControl
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsPFCModuleStatusDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                      tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsPFCModuleStatus
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsPFCClearCountersDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                       tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsPFCClearCounters
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsPFCGlobalEnableTrapDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                          tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsPFCGlobalEnableTrap
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsPFCGeneratedTrapCountDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                            tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsPFCGeneratedTrapCount
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFsPFCPortTable (tSnmpIndex * pFirstMultiIndex,
                            tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsPFCPortTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsPFCPortTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
FsPFCAdminModeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsPFCPortTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsPFCAdminMode (pMultiIndex->pIndex[0].i4_SLongValue,
                                  &(pMultiData->i4_SLongValue)));

}

INT4
FsPFCDcbxOperStateGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsPFCPortTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsPFCDcbxOperState (pMultiIndex->pIndex[0].i4_SLongValue,
                                      &(pMultiData->i4_SLongValue)));

}

INT4
FsPFCDcbxStateMachineGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsPFCPortTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsPFCDcbxStateMachine (pMultiIndex->pIndex[0].i4_SLongValue,
                                         &(pMultiData->i4_SLongValue)));

}

INT4
FsPFCClearTLVCountersGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsPFCPortTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsPFCClearTLVCounters (pMultiIndex->pIndex[0].i4_SLongValue,
                                         &(pMultiData->i4_SLongValue)));

}

INT4
FsPFCTxTLVCounterGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsPFCPortTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsPFCTxTLVCounter (pMultiIndex->pIndex[0].i4_SLongValue,
                                     &(pMultiData->u4_ULongValue)));

}

INT4
FsPFCRxTLVCounterGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsPFCPortTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsPFCRxTLVCounter (pMultiIndex->pIndex[0].i4_SLongValue,
                                     &(pMultiData->u4_ULongValue)));

}

INT4
FsPFCRxTLVErrorsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsPFCPortTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsPFCRxTLVErrors (pMultiIndex->pIndex[0].i4_SLongValue,
                                    &(pMultiData->u4_ULongValue)));

}

INT4
FsPFCRowStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsPFCPortTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsPFCRowStatus (pMultiIndex->pIndex[0].i4_SLongValue,
                                  &(pMultiData->i4_SLongValue)));

}

INT4 FsPFCSyncdGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsPFCPortTable(
		pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsPFCSyncd(
		pMultiIndex->pIndex[0].i4_SLongValue,
		&(pMultiData->i4_SLongValue)));

}
INT4 FsPFCDcbxStatusGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsPFCPortTable(
		pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsPFCDcbxStatus(
		pMultiIndex->pIndex[0].i4_SLongValue,
		&(pMultiData->i4_SLongValue)));

}

INT4 FsPFCRxPauseFrameCounterGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsPFCPortTable(
		pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsPFCRxPauseFrameCounter(
		pMultiIndex->pIndex[0].i4_SLongValue,
		&(pMultiData->u4_ULongValue)));

}
INT4 FsPFCTxPauseFrameCounterGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsPFCPortTable(
		pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsPFCTxPauseFrameCounter(
		pMultiIndex->pIndex[0].i4_SLongValue,
		&(pMultiData->u4_ULongValue)));

}
INT4 FsPFCRxPauseFrameP0CounterGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsPFCPortTable(
		pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsPFCRxPauseFrameP0Counter(
		pMultiIndex->pIndex[0].i4_SLongValue,
		&(pMultiData->u4_ULongValue)));

}
INT4 FsPFCRxPauseFrameP1CounterGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsPFCPortTable(
		pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsPFCRxPauseFrameP1Counter(
		pMultiIndex->pIndex[0].i4_SLongValue,
		&(pMultiData->u4_ULongValue)));

}
INT4 FsPFCRxPauseFrameP2CounterGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsPFCPortTable(
		pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsPFCRxPauseFrameP2Counter(
		pMultiIndex->pIndex[0].i4_SLongValue,
		&(pMultiData->u4_ULongValue)));

}
INT4 FsPFCRxPauseFrameP3CounterGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsPFCPortTable(
		pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsPFCRxPauseFrameP3Counter(
		pMultiIndex->pIndex[0].i4_SLongValue,
		&(pMultiData->u4_ULongValue)));

}
INT4 FsPFCRxPauseFrameP4CounterGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsPFCPortTable(
		pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsPFCRxPauseFrameP4Counter(
		pMultiIndex->pIndex[0].i4_SLongValue,
		&(pMultiData->u4_ULongValue)));

}
INT4 FsPFCRxPauseFrameP5CounterGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsPFCPortTable(
		pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsPFCRxPauseFrameP5Counter(
		pMultiIndex->pIndex[0].i4_SLongValue,
		&(pMultiData->u4_ULongValue)));

}
INT4 FsPFCRxPauseFrameP6CounterGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsPFCPortTable(
		pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsPFCRxPauseFrameP6Counter(
		pMultiIndex->pIndex[0].i4_SLongValue,
		&(pMultiData->u4_ULongValue)));

}
INT4 FsPFCRxPauseFrameP7CounterGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsPFCPortTable(
		pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsPFCRxPauseFrameP7Counter(
		pMultiIndex->pIndex[0].i4_SLongValue,
		&(pMultiData->u4_ULongValue)));

}
INT4 FsPFCTxPauseFrameP0CounterGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsPFCPortTable(
		pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsPFCTxPauseFrameP0Counter(
		pMultiIndex->pIndex[0].i4_SLongValue,
		&(pMultiData->u4_ULongValue)));

}
INT4 FsPFCTxPauseFrameP1CounterGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsPFCPortTable(
		pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsPFCTxPauseFrameP1Counter(
		pMultiIndex->pIndex[0].i4_SLongValue,
		&(pMultiData->u4_ULongValue)));

}
INT4 FsPFCTxPauseFrameP2CounterGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsPFCPortTable(
		pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsPFCTxPauseFrameP2Counter(
		pMultiIndex->pIndex[0].i4_SLongValue,
		&(pMultiData->u4_ULongValue)));

}
INT4 FsPFCTxPauseFrameP3CounterGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsPFCPortTable(
		pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsPFCTxPauseFrameP3Counter(
		pMultiIndex->pIndex[0].i4_SLongValue,
		&(pMultiData->u4_ULongValue)));

}
INT4 FsPFCTxPauseFrameP4CounterGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsPFCPortTable(
		pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsPFCTxPauseFrameP4Counter(
		pMultiIndex->pIndex[0].i4_SLongValue,
		&(pMultiData->u4_ULongValue)));

}
INT4 FsPFCTxPauseFrameP5CounterGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsPFCPortTable(
		pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsPFCTxPauseFrameP5Counter(
		pMultiIndex->pIndex[0].i4_SLongValue,
		&(pMultiData->u4_ULongValue)));

}
INT4 FsPFCTxPauseFrameP6CounterGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsPFCPortTable(
		pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsPFCTxPauseFrameP6Counter(
		pMultiIndex->pIndex[0].i4_SLongValue,
		&(pMultiData->u4_ULongValue)));

}
INT4 FsPFCTxPauseFrameP7CounterGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsPFCPortTable(
		pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsPFCTxPauseFrameP7Counter(
		pMultiIndex->pIndex[0].i4_SLongValue,
		&(pMultiData->u4_ULongValue)));

}
INT4 FsPFCDataFrameDiscardCounterGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsPFCPortTable(
		pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsPFCDataFrameDiscardCounter(
		pMultiIndex->pIndex[0].i4_SLongValue,
		&(pMultiData->u4_ULongValue)));

}
INT4 FsPFCClearPauseFrameCountersGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsPFCPortTable(
		pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsPFCClearPauseFrameCounters(
		pMultiIndex->pIndex[0].i4_SLongValue,
		&(pMultiData->i4_SLongValue)));

}
INT4 FsPFCErrorGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsPFCPortTable(
		pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsPFCError(
		pMultiIndex->pIndex[0].i4_SLongValue,
		&(pMultiData->i4_SLongValue)));

}

INT4
FsPFCAdminModeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsPFCAdminMode (pMultiIndex->pIndex[0].i4_SLongValue,
                                  pMultiData->i4_SLongValue));

}

INT4
FsPFCClearTLVCountersSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsPFCClearTLVCounters (pMultiIndex->pIndex[0].i4_SLongValue,
                                         pMultiData->i4_SLongValue));

}

INT4
FsPFCRowStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsPFCRowStatus (pMultiIndex->pIndex[0].i4_SLongValue,
                                  pMultiData->i4_SLongValue));

}
INT4 FsPFCClearPauseFrameCountersSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhSetFsPFCClearPauseFrameCounters(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiData->i4_SLongValue));

}

INT4
FsPFCAdminModeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                    tRetVal * pMultiData)
{
    return (nmhTestv2FsPFCAdminMode (pu4Error,
                                     pMultiIndex->pIndex[0].i4_SLongValue,
                                     pMultiData->i4_SLongValue));

}

INT4
FsPFCClearTLVCountersTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                           tRetVal * pMultiData)
{
    return (nmhTestv2FsPFCClearTLVCounters (pu4Error,
                                            pMultiIndex->pIndex[0].
                                            i4_SLongValue,
                                            pMultiData->i4_SLongValue));

}

INT4
FsPFCRowStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                    tRetVal * pMultiData)
{
    return (nmhTestv2FsPFCRowStatus (pu4Error,
                                     pMultiIndex->pIndex[0].i4_SLongValue,
                                     pMultiData->i4_SLongValue));

}

INT4 FsPFCClearPauseFrameCountersTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhTestv2FsPFCClearPauseFrameCounters(pu4Error,
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiData->i4_SLongValue));

}
INT4
FsPFCPortTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                   tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsPFCPortTable (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsAppPriSystemControlGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsAppPriSystemControl (&(pMultiData->i4_SLongValue)));
}

INT4
FsAppPriModuleStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsAppPriModuleStatus (&(pMultiData->i4_SLongValue)));
}

INT4
FsAppPriClearCountersGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsAppPriClearCounters (&(pMultiData->i4_SLongValue)));
}

INT4
FsAppPriGlobalEnableTrapGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsAppPriGlobalEnableTrap (&(pMultiData->i4_SLongValue)));
}

INT4
FsAppPriGeneratedTrapCountGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsAppPriGeneratedTrapCount (&(pMultiData->u4_ULongValue)));
}

INT4
FsAppPriSystemControlSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsAppPriSystemControl (pMultiData->i4_SLongValue));
}

INT4
FsAppPriModuleStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsAppPriModuleStatus (pMultiData->i4_SLongValue));
}

INT4
FsAppPriClearCountersSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsAppPriClearCounters (pMultiData->i4_SLongValue));
}

INT4
FsAppPriGlobalEnableTrapSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsAppPriGlobalEnableTrap (pMultiData->i4_SLongValue));
}

INT4
FsAppPriGeneratedTrapCountSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsAppPriGeneratedTrapCount (pMultiData->u4_ULongValue));
}

INT4
FsAppPriSystemControlTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                           tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsAppPriSystemControl
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FsAppPriModuleStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                          tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsAppPriModuleStatus
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FsAppPriClearCountersTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                           tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsAppPriClearCounters
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FsAppPriGlobalEnableTrapTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                              tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsAppPriGlobalEnableTrap
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FsAppPriGeneratedTrapCountTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsAppPriGeneratedTrapCount
            (pu4Error, pMultiData->u4_ULongValue));
}

INT4
FsAppPriSystemControlDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                          tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsAppPriSystemControl
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsAppPriModuleStatusDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                         tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsAppPriModuleStatus
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsAppPriClearCountersDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                          tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsAppPriClearCounters
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsAppPriGlobalEnableTrapDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                             tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsAppPriGlobalEnableTrap
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsAppPriGeneratedTrapCountDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                               tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsAppPriGeneratedTrapCount
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFsAppPriPortTable (tSnmpIndex * pFirstMultiIndex,
                               tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsAppPriPortTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsAppPriPortTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
FsAppPriAdminModeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsAppPriPortTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsAppPriAdminMode (pMultiIndex->pIndex[0].i4_SLongValue,
                                     &(pMultiData->i4_SLongValue)));

}

INT4
FsAppPriDcbxOperStateGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsAppPriPortTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsAppPriDcbxOperState (pMultiIndex->pIndex[0].i4_SLongValue,
                                         &(pMultiData->i4_SLongValue)));

}

INT4
FsAppPriDcbxStateMachineGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsAppPriPortTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsAppPriDcbxStateMachine
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsAppPriClearTLVCountersGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsAppPriPortTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsAppPriClearTLVCounters
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsAppPriTxTLVCounterGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsAppPriPortTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsAppPriTxTLVCounter (pMultiIndex->pIndex[0].i4_SLongValue,
                                        &(pMultiData->u4_ULongValue)));

}

INT4
FsAppPriRxTLVCounterGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsAppPriPortTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsAppPriRxTLVCounter (pMultiIndex->pIndex[0].i4_SLongValue,
                                        &(pMultiData->u4_ULongValue)));

}

INT4
FsAppPriRxTLVErrorsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsAppPriPortTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsAppPriRxTLVErrors (pMultiIndex->pIndex[0].i4_SLongValue,
                                       &(pMultiData->u4_ULongValue)));

}

INT4
FsAppPriAppProtocolsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsAppPriPortTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsAppPriAppProtocols (pMultiIndex->pIndex[0].i4_SLongValue,
                                        &(pMultiData->u4_ULongValue)));

}

INT4
FsAppPriRowStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsAppPriPortTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsAppPriRowStatus (pMultiIndex->pIndex[0].i4_SLongValue,
                                     &(pMultiData->i4_SLongValue)));

}

INT4 FsAppPriSyncdGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsAppPriPortTable(
		pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsAppPriSyncd(
		pMultiIndex->pIndex[0].i4_SLongValue,
		&(pMultiData->i4_SLongValue)));

}

INT4 FsAppPriDcbxStatusGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsAppPriPortTable(
                pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return(nmhGetFsAppPriDcbxStatus(
                pMultiIndex->pIndex[0].i4_SLongValue,
                &(pMultiData->i4_SLongValue)));

}

INT4 FsAppPriErrorGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsAppPriPortTable(
		pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsAppPriError(
		pMultiIndex->pIndex[0].i4_SLongValue,
		&(pMultiData->i4_SLongValue)));

}

INT4
FsAppPriAdminModeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsAppPriAdminMode (pMultiIndex->pIndex[0].i4_SLongValue,
                                     pMultiData->i4_SLongValue));

}

INT4
FsAppPriClearTLVCountersSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsAppPriClearTLVCounters
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
FsAppPriRowStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsAppPriRowStatus (pMultiIndex->pIndex[0].i4_SLongValue,
                                     pMultiData->i4_SLongValue));

}

INT4
FsAppPriAdminModeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                       tRetVal * pMultiData)
{
    return (nmhTestv2FsAppPriAdminMode (pu4Error,
                                        pMultiIndex->pIndex[0].i4_SLongValue,
                                        pMultiData->i4_SLongValue));

}

INT4
FsAppPriClearTLVCountersTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                              tRetVal * pMultiData)
{
    return (nmhTestv2FsAppPriClearTLVCounters (pu4Error,
                                               pMultiIndex->pIndex[0].
                                               i4_SLongValue,
                                               pMultiData->i4_SLongValue));

}

INT4
FsAppPriRowStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                       tRetVal * pMultiData)
{
    return (nmhTestv2FsAppPriRowStatus (pu4Error,
                                        pMultiIndex->pIndex[0].i4_SLongValue,
                                        pMultiData->i4_SLongValue));

}

INT4
FsAppPriPortTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                      tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsAppPriPortTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFsAppPriXAppTable (tSnmpIndex * pFirstMultiIndex,
                               tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsAppPriXAppTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue),
             &(pNextMultiIndex->pIndex[1].i4_SLongValue),
             &(pNextMultiIndex->pIndex[2].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsAppPriXAppTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue),
             pFirstMultiIndex->pIndex[1].i4_SLongValue,
             &(pNextMultiIndex->pIndex[1].i4_SLongValue),
             pFirstMultiIndex->pIndex[2].u4_ULongValue,
             &(pNextMultiIndex->pIndex[2].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
FsAppPriXAppRowStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsAppPriXAppTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsAppPriXAppRowStatus (pMultiIndex->pIndex[0].i4_SLongValue,
                                         pMultiIndex->pIndex[1].i4_SLongValue,
                                         pMultiIndex->pIndex[2].u4_ULongValue,
                                         &(pMultiData->i4_SLongValue)));

}

INT4
FsAppPriXAppRowStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsAppPriXAppRowStatus (pMultiIndex->pIndex[0].i4_SLongValue,
                                         pMultiIndex->pIndex[1].i4_SLongValue,
                                         pMultiIndex->pIndex[2].u4_ULongValue,
                                         pMultiData->i4_SLongValue));

}

INT4
FsAppPriXAppRowStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                           tRetVal * pMultiData)
{
    return (nmhTestv2FsAppPriXAppRowStatus (pu4Error,
                                            pMultiIndex->pIndex[0].
                                            i4_SLongValue,
                                            pMultiIndex->pIndex[1].
                                            i4_SLongValue,
                                            pMultiIndex->pIndex[2].
                                            u4_ULongValue,
                                            pMultiData->i4_SLongValue));

}

INT4
FsAppPriXAppTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                      tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsAppPriXAppTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFslldpXdot1dcbxLocApplicationPriorityBasicTable (tSnmpIndex *
                                                             pFirstMultiIndex,
                                                             tSnmpIndex *
                                                             pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFslldpXdot1dcbxLocApplicationPriorityBasicTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFslldpXdot1dcbxLocApplicationPriorityBasicTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
FslldpXdot1dcbxLocApplicationPriorityWillingGet (tSnmpIndex * pMultiIndex,
                                                 tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFslldpXdot1dcbxLocApplicationPriorityBasicTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFslldpXdot1dcbxLocApplicationPriorityWilling
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
GetNextIndexFslldpXdot1dcbxAdminApplicationPriorityBasicTable (tSnmpIndex *
                                                               pFirstMultiIndex,
                                                               tSnmpIndex *
                                                               pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFslldpXdot1dcbxAdminApplicationPriorityBasicTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFslldpXdot1dcbxAdminApplicationPriorityBasicTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
FslldpXdot1dcbxAdminApplicationPriorityWillingGet (tSnmpIndex * pMultiIndex,
                                                   tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFslldpXdot1dcbxAdminApplicationPriorityBasicTable (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFslldpXdot1dcbxAdminApplicationPriorityWilling
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FslldpXdot1dcbxAdminApplicationPriorityWillingSet (tSnmpIndex * pMultiIndex,
                                                   tRetVal * pMultiData)
{
    return (nmhSetFslldpXdot1dcbxAdminApplicationPriorityWilling
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
FslldpXdot1dcbxAdminApplicationPriorityWillingTest (UINT4 *pu4Error,
                                                    tSnmpIndex * pMultiIndex,
                                                    tRetVal * pMultiData)
{
    return (nmhTestv2FslldpXdot1dcbxAdminApplicationPriorityWilling (pu4Error,
                                                                     pMultiIndex->
                                                                     pIndex[0].
                                                                     i4_SLongValue,
                                                                     pMultiData->
                                                                     i4_SLongValue));

}

INT4
FslldpXdot1dcbxAdminApplicationPriorityBasicTableDep (UINT4 *pu4Error,
                                                      tSnmpIndexList *
                                                      pSnmpIndexList,
                                                      tSNMP_VAR_BIND *
                                                      pSnmpvarbinds)
{
    return (nmhDepv2FslldpXdot1dcbxAdminApplicationPriorityBasicTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFslldpXdot1dcbxRemApplicationPriorityBasicTable (tSnmpIndex *
                                                             pFirstMultiIndex,
                                                             tSnmpIndex *
                                                             pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFslldpXdot1dcbxRemApplicationPriorityBasicTable
            (&(pNextMultiIndex->pIndex[0].u4_ULongValue),
             &(pNextMultiIndex->pIndex[1].i4_SLongValue),
             &(pNextMultiIndex->pIndex[2].u4_ULongValue),
             &(pNextMultiIndex->pIndex[3].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFslldpXdot1dcbxRemApplicationPriorityBasicTable
            (pFirstMultiIndex->pIndex[0].u4_ULongValue,
             &(pNextMultiIndex->pIndex[0].u4_ULongValue),
             pFirstMultiIndex->pIndex[1].i4_SLongValue,
             &(pNextMultiIndex->pIndex[1].i4_SLongValue),
             pFirstMultiIndex->pIndex[2].u4_ULongValue,
             &(pNextMultiIndex->pIndex[2].u4_ULongValue),
             pFirstMultiIndex->pIndex[3].i4_SLongValue,
             &(pNextMultiIndex->pIndex[3].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    pNextMultiIndex->pIndex[2].pOctetStrValue->i4_Length = 6;
    return SNMP_SUCCESS;
}

INT4
FslldpXdot1dcbxRemApplicationPriorityWillingGet (tSnmpIndex * pMultiIndex,
                                                 tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFslldpXdot1dcbxRemApplicationPriorityBasicTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFslldpXdot1dcbxRemApplicationPriorityWilling
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             pMultiIndex->pIndex[2].u4_ULongValue,
             pMultiIndex->pIndex[3].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
GetNextIndexFslldpXdot1dcbxConfigTCSupportedTable (tSnmpIndex *
                                                   pFirstMultiIndex,
                                                   tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFslldpXdot1dcbxConfigTCSupportedTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue),
             &(pNextMultiIndex->pIndex[1].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFslldpXdot1dcbxConfigTCSupportedTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue),
             pFirstMultiIndex->pIndex[1].u4_ULongValue,
             &(pNextMultiIndex->pIndex[1].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
FslldpXdot1dcbxConfigTCSupportedTxEnableGet (tSnmpIndex * pMultiIndex,
                                             tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFslldpXdot1dcbxConfigTCSupportedTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFslldpXdot1dcbxConfigTCSupportedTxEnable
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FslldpXdot1dcbxConfigTCSupportedTxEnableSet (tSnmpIndex * pMultiIndex,
                                             tRetVal * pMultiData)
{
    return (nmhSetFslldpXdot1dcbxConfigTCSupportedTxEnable
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].u4_ULongValue, pMultiData->i4_SLongValue));

}

INT4
FslldpXdot1dcbxConfigTCSupportedTxEnableTest (UINT4 *pu4Error,
                                              tSnmpIndex * pMultiIndex,
                                              tRetVal * pMultiData)
{
    return (nmhTestv2FslldpXdot1dcbxConfigTCSupportedTxEnable (pu4Error,
                                                               pMultiIndex->
                                                               pIndex[0].
                                                               i4_SLongValue,
                                                               pMultiIndex->
                                                               pIndex[1].
                                                               u4_ULongValue,
                                                               pMultiData->
                                                               i4_SLongValue));

}

INT4
FslldpXdot1dcbxConfigTCSupportedTableDep (UINT4 *pu4Error,
                                          tSnmpIndexList * pSnmpIndexList,
                                          tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FslldpXdot1dcbxConfigTCSupportedTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFslldpXdot1dcbxLocTCSupportedTable (tSnmpIndex * pFirstMultiIndex,
                                                tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFslldpXdot1dcbxLocTCSupportedTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFslldpXdot1dcbxLocTCSupportedTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
FslldpXdot1dcbxLocTCSupportedGet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFslldpXdot1dcbxLocTCSupportedTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFslldpXdot1dcbxLocTCSupported
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
GetNextIndexFslldpXdot1dcbxRemTCSupportedTable (tSnmpIndex * pFirstMultiIndex,
                                                tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFslldpXdot1dcbxRemTCSupportedTable(
            &(pNextMultiIndex->pIndex[0].u4_ULongValue),
             &(pNextMultiIndex->pIndex[1].i4_SLongValue),
            &(pNextMultiIndex->pIndex[2].u4_ULongValue),
             &(pNextMultiIndex->pIndex[3].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFslldpXdot1dcbxRemTCSupportedTable
            (pFirstMultiIndex->pIndex[0].u4_ULongValue,
             &(pNextMultiIndex->pIndex[0].u4_ULongValue),
             pFirstMultiIndex->pIndex[1].i4_SLongValue,
             &(pNextMultiIndex->pIndex[1].i4_SLongValue),
             pFirstMultiIndex->pIndex[2].u4_ULongValue,
             &(pNextMultiIndex->pIndex[2].u4_ULongValue),
             pFirstMultiIndex->pIndex[3].i4_SLongValue,
             &(pNextMultiIndex->pIndex[3].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    pNextMultiIndex->pIndex[2].pOctetStrValue->i4_Length = 6;
    return SNMP_SUCCESS;
}

INT4
FslldpXdot1dcbxRemTCSupportedGet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFslldpXdot1dcbxRemTCSupportedTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFslldpXdot1dcbxRemTCSupported
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             pMultiIndex->pIndex[2].u4_ULongValue,
             pMultiIndex->pIndex[3].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
GetNextIndexFslldpXdot1dcbxAdminTCSupportedTable (tSnmpIndex * pFirstMultiIndex,
                                                  tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFslldpXdot1dcbxAdminTCSupportedTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFslldpXdot1dcbxAdminTCSupportedTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
FslldpXdot1dcbxAdminTCSupportedGet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFslldpXdot1dcbxAdminTCSupportedTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFslldpXdot1dcbxAdminTCSupported
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4 FsDcbxCEEGlobalEnableTrapGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	UNUSED_PARAM(pMultiIndex);
	return(nmhGetFsDcbxCEEGlobalEnableTrap(&(pMultiData->i4_SLongValue)));
}
INT4 FsDcbxCEEGeneratedTrapCountGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	UNUSED_PARAM(pMultiIndex);
	return(nmhGetFsDcbxCEEGeneratedTrapCount(&(pMultiData->u4_ULongValue)));
}
INT4 FsDcbxCEEClearCountersGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	UNUSED_PARAM(pMultiIndex);
	return(nmhGetFsDcbxCEEClearCounters(&(pMultiData->i4_SLongValue)));
}
INT4 FsDcbxCEEGlobalEnableTrapSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	UNUSED_PARAM(pMultiIndex);
	return(nmhSetFsDcbxCEEGlobalEnableTrap(pMultiData->i4_SLongValue));
}


INT4 FsDcbxCEEClearCountersSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	UNUSED_PARAM(pMultiIndex);
	return(nmhSetFsDcbxCEEClearCounters(pMultiData->i4_SLongValue));
}


INT4 FsDcbxCEEGlobalEnableTrapTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	UNUSED_PARAM(pMultiIndex);
	return(nmhTestv2FsDcbxCEEGlobalEnableTrap(pu4Error, pMultiData->i4_SLongValue));
}


INT4 FsDcbxCEEClearCountersTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	UNUSED_PARAM(pMultiIndex);
	return(nmhTestv2FsDcbxCEEClearCounters(pu4Error, pMultiData->i4_SLongValue));
}


INT4 FsDcbxCEEGlobalEnableTrapDep(UINT4 *pu4Error,tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpvarbinds)
{
	return(nmhDepv2FsDcbxCEEGlobalEnableTrap(pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4 FsDcbxCEEClearCountersDep(UINT4 *pu4Error,tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpvarbinds)
{
	return(nmhDepv2FsDcbxCEEClearCounters(pu4Error, pSnmpIndexList, pSnmpvarbinds));
}


INT4 GetNextIndexFsDcbxCEECtrlTable(tSnmpIndex *pFirstMultiIndex,tSnmpIndex * pNextMultiIndex)
{
	if (pFirstMultiIndex == NULL) 
	{
		if (nmhGetFirstIndexFsDcbxCEECtrlTable(
			&(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
		{
 			return SNMP_FAILURE;
		}
	}
	else
	{
		if (nmhGetNextIndexFsDcbxCEECtrlTable(
			pFirstMultiIndex->pIndex[0].i4_SLongValue,
			&(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
		{
			return SNMP_FAILURE;
		}
	}
	
	return SNMP_SUCCESS;
}
INT4 FsDcbxCEECtrlSeqNoGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsDcbxCEECtrlTable(
		pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsDcbxCEECtrlSeqNo(
		pMultiIndex->pIndex[0].i4_SLongValue,
		&(pMultiData->u4_ULongValue)));

}
INT4 FsDcbxCEECtrlAckNoGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsDcbxCEECtrlTable(
		pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsDcbxCEECtrlAckNo(
		pMultiIndex->pIndex[0].i4_SLongValue,
		&(pMultiData->u4_ULongValue)));

}
INT4 FsDcbxCEECtrlRcvdAckNoGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsDcbxCEECtrlTable(
		pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsDcbxCEECtrlRcvdAckNo(
		pMultiIndex->pIndex[0].i4_SLongValue,
		&(pMultiData->u4_ULongValue)));

}
INT4 FsDcbxCEECtrlTxTLVCounterGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsDcbxCEECtrlTable(
		pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsDcbxCEECtrlTxTLVCounter(
		pMultiIndex->pIndex[0].i4_SLongValue,
		&(pMultiData->u4_ULongValue)));

}
INT4 FsDcbxCEECtrlRxTLVCounterGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsDcbxCEECtrlTable(
		pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsDcbxCEECtrlRxTLVCounter(
		pMultiIndex->pIndex[0].i4_SLongValue,
		&(pMultiData->u4_ULongValue)));

}
INT4 FsDcbxCEECtrlRxTLVErrorCounterGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsDcbxCEECtrlTable(
		pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsDcbxCEECtrlRxTLVErrorCounter(
		pMultiIndex->pIndex[0].i4_SLongValue,
		&(pMultiData->u4_ULongValue)));

}
