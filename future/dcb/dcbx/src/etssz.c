/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: etssz.c,v 1.3 2013/11/29 11:04:12 siva Exp $
 *
 * Description: This files for RADIUS.
 *******************************************************************/

#define _ETSSZ_C
#include "dcbxinc.h"
extern INT4         IssSzRegisterModuleSizingParams (CHR1 * pu1ModName,
                                                     tFsModSizingParams *
                                                     pModSizingParams);
extern INT4         IssSzRegisterModulePoolId (CHR1 * pu1ModName,
                                               tMemPoolId * pModPoolId);
INT4
EtsSizingMemCreateMemPools ()
{
    INT4                i4RetVal;
    INT4                i4SizingId;

    for (i4SizingId = 0; i4SizingId < ETS_MAX_SIZING_ID; i4SizingId++)
    {
        i4RetVal = MemCreateMemPool (FsETSSizingParams[i4SizingId].u4StructSize,
                                     FsETSSizingParams[i4SizingId].
                                     u4PreAllocatedUnits,
                                     MEM_DEFAULT_MEMORY_TYPE,
                                     &(ETSMemPoolIds[i4SizingId]));
        if (i4RetVal == (INT4) MEM_FAILURE)
        {
            EtsSizingMemDeleteMemPools ();
            return OSIX_FAILURE;
        }
    }
    return OSIX_SUCCESS;
}

INT4
EtsSzRegisterModuleSizingParams (CHR1 * pu1ModName)
{
    /* Copy the Module Name */
    IssSzRegisterModuleSizingParams (pu1ModName, FsETSSizingParams);
    IssSzRegisterModulePoolId (pu1ModName, ETSMemPoolIds);
    return OSIX_SUCCESS;
}

VOID
EtsSizingMemDeleteMemPools ()
{
    INT4                i4SizingId;

    for (i4SizingId = 0; i4SizingId < ETS_MAX_SIZING_ID; i4SizingId++)
    {
        if (ETSMemPoolIds[i4SizingId] != 0)
        {
            MemDeleteMemPool (ETSMemPoolIds[i4SizingId]);
            ETSMemPoolIds[i4SizingId] = 0;
        }
    }
    return;
}
