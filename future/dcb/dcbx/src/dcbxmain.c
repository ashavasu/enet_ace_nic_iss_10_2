/*************************************************************************
 * Copyright (C) 2010 Aricent Inc . All Rights Reserved
 * $Id: dcbxmain.c,v 1.14 2016/03/05 12:17:53 siva Exp $
 * Description: This file contains DCBX task main loop and 
 *              initialization routines.
****************************************************************************/

#ifndef DCBXMAIN_C
#define DCBXMAIN_C

#include "dcbxinc.h"

/***************************************************************************
 *  FUNCTION NAME: DcbxMainTask
 * 
 *  DESCRIPTION      : Entry point function of DCBX task.
 * 
 *  INPUT            : pi1Arg - Pointer to the arguments
 * 
 *  OUTPUT           : None
 * 
 *  RETURNS          : None
 * 
 * **************************************************************************/
PUBLIC VOID
DcbxMainTask (INT1 *pi1Arg)
{

    UINT4               u4Events = DCBX_ZERO;

    UNUSED_PARAM (pi1Arg);

    /* Start the DCBX Task */
    if (DcbxMainTaskInit () == OSIX_FAILURE)
    {
        DCBX_TRC (DCBX_FAILURE_TRC,
                  "DcbxMainTask: Task Initialisation FAILED!!\r\n ");

        DcbxMainTaskDeInit ();

        /* Indicate the status of initialization to main routine */
        lrInitComplete (OSIX_FAILURE);
        return;
    }

    if (OsixGetTaskId (SELF, DCBX_TASK_NAME, &(gDcbxGlobalInfo.DcbxTaskId))
        == OSIX_FAILURE)
    {
        DCBX_TRC (DCBX_RESOURCE_TRC | DCBX_FAILURE_TRC,
                  "DCbxMainTask: Obtaining Task Id FAILED !!!\r\n");
        DcbxMainTaskDeInit ();

        /* Indicate the status of initialization to main routine */
        lrInitComplete (OSIX_FAILURE);
        return;
    }

    /* Register the protocol MIBS with SNMP */
    #ifdef SNMP_2_WANTED
    RegisterFSDCBX ();
    RegisterSTDDCB ();
    #endif /* SNMP_2_WANTED*/
    /* Register with RM */
    if (DcbxRedInitGlobalInfo () == OSIX_FAILURE)
    {
        DCBX_TRC (DCBX_RED_TRC | DCBX_FAILURE_TRC,
                  "DCbxMainTask:RM Registration Failed!!! \r\n");
        DcbxMainTaskDeInit ();

        /* Indicate the status of initialization to main routine */
        lrInitComplete (OSIX_FAILURE);
        return;
    }
    /* Indicate the status of initialization to main routine */
    lrInitComplete (OSIX_SUCCESS);

    for (;;)
    {

        if (OsixEvtRecv (gDcbxGlobalInfo.DcbxTaskId, DCBX_ALL_EVENTS,
                         OSIX_WAIT, &u4Events) == OSIX_SUCCESS)
        {
            /* This routine handles the events and process them. This 
             * routine is introduced to enable System time relinquish 
             * method. In this method, any long time operation can 
             * relinquish system time by calling a routine which will
             * receive events (Non-Blocking call) and process the events
             * by calling this routine. By this way system time is relinquished
             * as well as control returns to the original routine.
             * */
            DcbxMainProcessEvents (u4Events);

        }
    }                            /* end of while */
}

/****************************************************************************
 *  
 * FUNCTION NAME : DcbxMainTaskInit
 * 
 * DESCRIPTION      : This function creates the task queue, allocates
 *                    MemPools for task queue messages and creates protocol
 *                    semaphore.
 * INPUT            : None
 * 
 * OUTPUT           : None
 * 
 * RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 * 
 * ****************************************************************************/
PUBLIC INT4
DcbxMainTaskInit (VOID)
{
    MEMSET (&gDcbxGlobalInfo, DCBX_ZERO, sizeof (tDcbxGlobalInfo));

    /* DCBX Protocol creation of semaphore */
    if (OsixSemCrt (DCBX_PROTO_SEM, &(gDcbxGlobalInfo.DcbxSemId)) ==
        OSIX_FAILURE)
    {
        DCBX_TRC (DCBX_RESOURCE_TRC | DCBX_FAILURE_TRC,
                  "DCBXMainTaskInit: DCBX protocol semaphore creation "
                  "FAILED!!!\r\n");
        return (OSIX_FAILURE);
    }

    /* Semaphore is by default created with initial value 0. So GiveSem
     *      * is called before using the semaphore. */
    OsixSemGive (gDcbxGlobalInfo.DcbxSemId);

    /* DCBX Task creation of Queue */
    if (OsixQueCrt ((UINT1 *) DCBX_TASK_QUEUE_NAME, OSIX_MAX_Q_MSG_LEN,
                    DCBX_MAX_Q_DEPTH, &(gDcbxGlobalInfo.DcbxPktQId))
        == OSIX_FAILURE)
    {
        DCBX_TRC (DCBX_RESOURCE_TRC | DCBX_FAILURE_TRC,
                  "DCBXMainTaskInit: DCBX Task Q Creation FAILED !!!\r\n");
        return (OSIX_FAILURE);
    }

    /* Start the DCBX Main Module */
    if (DcbxMainModuleStart () == OSIX_FAILURE)
    {
        DCBX_TRC (DCBX_FAILURE_TRC,
                  "DCBXMainTaskInit: DCBX Module Start FAILED !!!\r\n");
        return (OSIX_FAILURE);
    }

    /* Start the ETS Module which will initailise the necessary memory 
     * and intialise the data strucutres */
    if (ETSMainModuleStart () == OSIX_FAILURE)
    {
        DCBX_TRC (DCBX_FAILURE_TRC,
                  "DCBXMainTaskInit: ETS Module Start FAILED !!!\r\n");
        return (OSIX_FAILURE);
    }

    /* Start the PFC Module which will initailise the necessary memory 
     * and intialise the data strucutres */
    if (PFCMainModuleStart () == OSIX_FAILURE)
    {
        DCBX_TRC (DCBX_FAILURE_TRC,
                  "DCBXMainTaskInit: PFC Module Start FAILED !!!\r\n");
        return (OSIX_FAILURE);
    }

    /* Start the PFC Module which will initailise the necessary memory 
     * and intialise the data strucutres */
    if (AppPriMainModuleStart () == OSIX_FAILURE)
    {
        DCBX_TRC (DCBX_FAILURE_TRC,
                  "DCBXMainTaskInit: Application Priority Module "
                  "Start FAILED !!!\r\n");
        return (OSIX_FAILURE);
    }

    /* Start the CEE Module which will initailise the necessary memory 
     * and intialise the data strucutres */
    if (CEEMainModuleStart () == OSIX_FAILURE)
    {
        DCBX_TRC (DCBX_FAILURE_TRC,
                  "DCBXMainTaskInit: CEE Module Start FAILED !!!\r\n");
        return (OSIX_FAILURE);
    }

    return (OSIX_SUCCESS);
}

/****************************************************************************
 *  
 *  FUNCTION NAME    : DcbxMainTaskDeInit
 *  
 *  DESCRIPTION      : This function deletes the task queue and protocol
 *                      semaphore, de-allocates the task Q message MemPool
 *  
 *  INPUT            : None
 *  
 *  OUTPUT           : None
 *  
 *  RETURNS          : None
 *  
 * ****************************************************************************/

PUBLIC VOID
DcbxMainTaskDeInit (VOID)
{
    /* De-Register from RM */
    DcbxRedDeInitGlobalInfo ();

    /* Shutdown the DCBX Main module which deinitialise all
     *    the data strucutures and release the memory*/
    DcbxMainModuleShutDown ();

    /* Since ETS Module runs on the DCBX Task, this should be 
     *    shutdown when the task is killed */
    ETSMainModuleShutDown ();

    /* Since PFC Module runs on the DCBX Task, this should be 
     *    shutdown when the task is killed */
    PFCMainModuleShutDown ();

    /* Since Application Priority Module runs on the DCBX Task,
     * this should be shutdown when the task is killed */
    AppPriMainModuleShutDown ();

    /* Delete the DCBX Queue */
    if (gDcbxGlobalInfo.DcbxPktQId != DCBX_ZERO)
    {
        OsixQueDel (gDcbxGlobalInfo.DcbxPktQId);
        gDcbxGlobalInfo.DcbxPktQId = DCBX_ZERO;
    }

    /* Delete the DCBX Semaphore */
    if (gDcbxGlobalInfo.DcbxSemId != DCBX_ZERO)
    {
        OsixSemDel (gDcbxGlobalInfo.DcbxSemId);
        gDcbxGlobalInfo.DcbxSemId = DCBX_ZERO;
    }

    return;

}

/****************************************************************************
 *  
 *  FUNCTION NAME    : DcbxMainModuleStart
 *  
 *  DESCRIPTION      : This function allocates memory pools for all tables
 *                     in Dcbx module. It also initalizes the global
 *                     structure.
 *  INPUT            : None
 *  
 *  OUTPUT           : None
 *  
 *  RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *  
 * ****************************************************************************/
PUBLIC INT4
DcbxMainModuleStart (VOID)
{
    /* Initialize Database Mempools */
    if (DcbxSizingMemCreateMemPools () == OSIX_FAILURE)
    {
        DCBX_TRC (DCBX_RESOURCE_TRC | DCBX_FAILURE_TRC, "DcbxMainModuleStart:"
                  " Dcbx Memory Initialization FAILED !!!\r\n");
        DcbxSizingMemDeleteMemPools ();
        return OSIX_FAILURE;
    }

    /*Assigning respective mempool Id's */
    DcbxMainAssignMempoolIds ();

    /* Create DCBX Port Table */
    gDcbxGlobalInfo.pRbDcbxPortTbl =
        RBTreeCreateEmbedded (FSAP_OFFSETOF (tDcbxPortEntry, RbNode),
                              DcbxUtlPortTblCmpFn);

    if (gDcbxGlobalInfo.pRbDcbxPortTbl == NULL)
    {
        DCBX_TRC (DCBX_RESOURCE_TRC | DCBX_FAILURE_TRC, "DcbxMainModuleStart: "
                  " Dcbx Port Table -RBTreeCreateEmbedded ()" " Failed!.\r\n");

        return OSIX_FAILURE;
    }

    /* Create DCBX Application Table */
    gDcbxGlobalInfo.pRbDcbxAppTbl =
        RBTreeCreateEmbedded (FSAP_OFFSETOF (tDcbxAppEntry, RbNode),
                              DcbxUtlApplCmpFun);

    if (gDcbxGlobalInfo.pRbDcbxAppTbl == NULL)
    {
        DCBX_TRC (DCBX_RESOURCE_TRC | DCBX_FAILURE_TRC, "DcbxMainModuleStart: "
                  " Dcbx Application Table -RBTreeCreateEmbedded ()"
                  " Failed!.\r\n");
        return OSIX_FAILURE;
    }
    /* Set default trace flag */
    gDcbxGlobalInfo.u4DCBXTrcFlag = DCBX_CRITICAL_TRC;
    /*Set System control status */
    gDcbxGlobalInfo.u1DCBXSystemCtrl = DCBX_START;

    /* Create all valid ports */
    DcbxUtlCreateAllPorts ();

    return OSIX_SUCCESS;

}

/****************************************************************************
 * 
 * FUNCTION NAME    : DcbxMainModuleShutDown
 * 
 * DESCRIPTION      : This function shuts down the entire DCBX module.
 * 
 * INPUT            : None
 * 
 * OUTPUT           : None
 *
 * RETURNS          : None
 *
 *****************************************************************************/
PUBLIC VOID
DcbxMainModuleShutDown (VOID)
{
    tDcbxQueueMsg      *pMsg = NULL;

    /* Release all the Queue Memory which are not processed while
     *       Shudown of the DCBX Module */
    while (OsixQueRecv (gDcbxGlobalInfo.DcbxPktQId, (UINT1 *) &pMsg,
                        OSIX_DEF_MSG_LEN, OSIX_NO_WAIT) == OSIX_SUCCESS)
    {
        /* Receive all the messages in the queue and release the memory */
        MemReleaseMemBlock (gDcbxGlobalInfo.DcbxQPktPoolId, (UINT1 *) pMsg);
    }

    /* Delete DCBX Port Table */
    if (gDcbxGlobalInfo.pRbDcbxPortTbl != NULL)
    {
        /* Send Delete port indication so that it delete the ETS and PFC
         * port in turn deregisters the application registered with LLDP
         * through the DCBX ports */
        DcbxUtlDeletePorts ();
        RBTreeDestroy (gDcbxGlobalInfo.pRbDcbxPortTbl, NULL, DCBX_ZERO);
        gDcbxGlobalInfo.pRbDcbxPortTbl = NULL;
    }

    /* Delete DCBX Application Table */
    if (gDcbxGlobalInfo.pRbDcbxAppTbl != NULL)
    {
        RBTreeDestroy (gDcbxGlobalInfo.pRbDcbxAppTbl, NULL, DCBX_ZERO);
        gDcbxGlobalInfo.pRbDcbxAppTbl = NULL;
    }
    /*Set System control status */
    gDcbxGlobalInfo.u1DCBXSystemCtrl = DCBX_SHUTDOWN;

    DcbxSizingMemDeleteMemPools ();
    return;
}

/***************************************************************************
 *  FUNCTION NAME    : DcbxMainProcessEvent
 *  
 *  DESCRIPTION      : This routine process the event and calls the Message
 *                      handler routine
 *  
 *  INPUT            : u4Event - Events
 * 
 *  OUTPUT           : None
 *  
 *  RETURNS          : VOID
 *  
 ***************************************************************************/
PUBLIC VOID
DcbxMainProcessEvents (UINT4 u4Events)
{
    /* Process the message which are posted for DCBX_QMSG_ENVET */
    if (u4Events & DCBX_QMSG_EVENT)
    {
        /* Call the Que Message Handler Function */
        DcbxQueMsgHandler ();
    }
    return;
}

/***************************************************************************
 *  FUNCTION NAME : ETSMainModuleStart
 * 
 *  DESCRIPTION   : This function is used to start the ETS Module
 * 
 *  INPUT         : None
 * 
 *  OUTPUT        : None
 * 
 *  RETURNS       : OSIX_SUCCESS/OSIX_FAILURE
 * 
 * **************************************************************************/
PUBLIC INT4
ETSMainModuleStart (VOID)
{
    /* Initialize ETS Mempools */
    if (EtsSizingMemCreateMemPools () == OSIX_FAILURE)
    {
        DCBX_TRC (DCBX_RESOURCE_TRC | DCBX_FAILURE_TRC, "ETSMainModuleStart:"
                  " Memory Initialization FAILED !!!\r\n");
        EtsSizingMemDeleteMemPools ();
        return OSIX_FAILURE;
    }
    /*Assigning respective mempool Id's */
    ETSMainAssignMempoolIds ();

    /* Create ETS Port Table which will have the ETS port related
     *      information */
    gETSGlobalInfo.pRbETSPortTbl =
        RBTreeCreateEmbedded (FSAP_OFFSETOF (tETSPortEntry, RbNode),
                              ETSUtlPortTblCmpFn);

    if (gETSGlobalInfo.pRbETSPortTbl == NULL)
    {
        DCBX_TRC (DCBX_RESOURCE_TRC | DCBX_FAILURE_TRC, "ETSMainModuleStart : "
                  "ETS Port  Table -RBTreeCreateEmbedded ()" " Failed!.\r\n");
        return OSIX_FAILURE;
    }

    /*Initialise the Default Global Values */
    gETSGlobalInfo.u1ETSSystemCtrl = ETS_START;
    gETSGlobalInfo.u1ETSModStatus = ETS_ENABLED;
    gETSGlobalInfo.u4ETSTrap = ETS_MODULE_STATUS_TRAP | ETS_ADMIN_MODE_TRAP;

    /* Construct the PreFormed TLV Header to be used in 
     * TLV forming and processing */
    ETSUtlConstructConfPreFormTlv ();
    ETSUtlConstructRecoPreFormTlv ();
    ETSUtlConstructTcSuppPreFormTlv ();

    return OSIX_SUCCESS;
}

/***************************************************************************
 *  FUNCTION NAME : ETSMainModuleShutDown
 * 
 *  DESCRIPTION   : This function is used to shutdown the ETS Module
 * 
 *  INPUT         : None
 * 
 *  OUTPUT        : None
 * 
 *  RETURNS       : None
 * 
 * **************************************************************************/
PUBLIC VOID
ETSMainModuleShutDown (VOID)
{
    /* Delete all the ETS port entries */
    ETSUtlDeleteAllPorts ();
    /* Intialise the default global variables and set module status as
     * disabled */
    gETSGlobalInfo.u1ETSModStatus = ETS_DISABLED;
    gETSGlobalInfo.u4ETSTrap = DCBX_ZERO;
    /* Delete ETS port Table */
    if (gETSGlobalInfo.pRbETSPortTbl != NULL)
    {
        RBTreeDestroy (gETSGlobalInfo.pRbETSPortTbl, NULL, DCBX_ZERO);
        gETSGlobalInfo.pRbETSPortTbl = NULL;
    }
    /* Release the ETS MemPools */
    EtsSizingMemDeleteMemPools ();
    return;
}

/***************************************************************************
 *  FUNCTION NAME : PFCMainModuleStart
 * 
 *  DESCRIPTION   : This function is used to start the PFC module.
 * 
 *  INPUT         : None
 * 
 *  OUTPUT        : None
 * 
 *  RETURNS       : OSIX_SUCCESS/OSIX_FAILURE
 * 
 * **************************************************************************/
PUBLIC INT4
PFCMainModuleStart (VOID)
{
    /* Initialize PFC  Mempools */
    if (PfcSizingMemCreateMemPools () == OSIX_FAILURE)
    {
        DCBX_TRC (DCBX_RESOURCE_TRC | DCBX_FAILURE_TRC, "PFCMainModuleStart:"
                  " Memory Initialization FAILED !!!\r\n");
        PfcSizingMemDeleteMemPools ();
        return OSIX_FAILURE;
    }

    /*Assigning respective mempool Id's */
    PFCMainAssignMempoolIds ();

    /* Create PFC Port Table which will have all the PFC port 
     * related information */
    gPFCGlobalInfo.pRbPFCPortTbl =
        RBTreeCreateEmbedded (FSAP_OFFSETOF (tPFCPortEntry, RbNode),
                              PFCUtlPortTblCmpFn);

    if (gPFCGlobalInfo.pRbPFCPortTbl == NULL)
    {
        DCBX_TRC (DCBX_RESOURCE_TRC | DCBX_FAILURE_TRC, "PFCMainModuleStart : "
                  "PFC Port Table -RBTreeCreateEmbedded ()" " Failed!.\r\n");
        return OSIX_FAILURE;
    }
    /* Initialise PFC Global Values */
    gPFCGlobalInfo.u1PFCSystemCtrl = PFC_START;
    gPFCGlobalInfo.u1PFCModStatus = PFC_ENABLED;
    gPFCGlobalInfo.u4PFCTrap = PFC_MODULE_STATUS_TRAP | PFC_ADMIN_MODE_TRAP;

    /* Construct the PreFormed TLV Header to be used in 
     * TLV forming and processing */
    PFCUtlConstructPreFormTlv ();

    /* Initialise the PFC profiles */
    PFCUtlInitPFCProfiles ();
    return OSIX_SUCCESS;
}

/***************************************************************************
 *  FUNCTION NAME : PFCMainModuleShutDown
 * 
 *  DESCRIPTION   : This function is used to shutdown the PFC module
 * 
 *  INPUT         : None
 * 
 *  OUTPUT        : None
 * 
 *  RETURNS       : None
 * 
 * **************************************************************************/
PUBLIC VOID
PFCMainModuleShutDown (VOID)
{
    /* Delete all the ETS port entries */
    PFCUtlDeleteAllPorts ();
    /* Intialise the default global variables and set module status as
     * disabled */
    gPFCGlobalInfo.u1PFCModStatus = PFC_DISABLED;
    gPFCGlobalInfo.u4PFCTrap = DCBX_ZERO;
    /* Delete PFC Port Table */
    if (gPFCGlobalInfo.pRbPFCPortTbl != NULL)
    {
        RBTreeDestroy (gPFCGlobalInfo.pRbPFCPortTbl, NULL, DCBX_ZERO);
        gPFCGlobalInfo.pRbPFCPortTbl = NULL;
    }
    /* DeIntialise the PFC MemPools */
    PfcSizingMemDeleteMemPools ();
    return;
}

/*****************************************************************************/
/* Function Name      : DcbxLock                                             */
/* Description        : This function is used to take the DCBX mutual        */
/*                      exclusion SEMa4 to avoid simultaneous access to      */
/*                      protocol data structures by the protocol task and    */
/*                      configuration task/thread.                           */
/* Input(s)           : None.                                                */
/* Output(s)          : None.                                                */
/* Return Value(s)    : DCBX_SUCCESS or DCBX_FAILURE                         */
/*****************************************************************************/
INT4
DcbxLock ()
{
    if (OsixSemTake (gDcbxGlobalInfo.DcbxSemId) == OSIX_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (SNMP_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : DcbxUnLock                                           */
/* Description        : This function is used to give the Dcbx mutual        */
/*                      exclusion SEMa4 to avoid simultaneous access to      */
/*                      protocol data structures by the protocol task and    */
/*                      configuration task/thread.                           */
/* Input(s)           : None.                                                */
/* Output(s)          : None.                                                */
/* Return Value(s)    : DCBX_SUCCESS or DCBX_FAILURE                         */
/*****************************************************************************/
INT4
DcbxUnLock ()
{
    if (OsixSemGive (gDcbxGlobalInfo.DcbxSemId) == OSIX_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (SNMP_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : DcbxMainAssignMempoolIds                             */
/*                                                                           */
/* Description        : This function is used to assign respective mempool   */
/*                       ID's for DCBX module.                               */
/*                                                                           */
/* Input (s)          : NONE.                                                */
/*                                                                           */
/* Output (s)         : NONE                                                 */
/*                                                                           */
/* Return Value(s)    : NONE                                                 */
/*                                                                           */
/*****************************************************************************/

VOID
DcbxMainAssignMempoolIds (VOID)
{
    /*tDcbxQueueMsg */
    gDcbxGlobalInfo.DcbxQPktPoolId =
        DCBXMemPoolIds[MAX_DCBX_QUEUE_MESG_SIZING_ID];

    /*tDcbxPortEntry */
    gDcbxGlobalInfo.DcbxPortPoolId =
        DCBXMemPoolIds[MAX_DCBX_PORT_TABLE_ENTRIES_SIZING_ID];

    /*tDcbxAppEntry */
    gDcbxGlobalInfo.DcbxApplPoolId =
        DCBXMemPoolIds[MAX_DCBX_APP_TABLE_ENTRIES_SIZING_ID];

    /*tMbsmProtoMsg */
    gDcbxGlobalInfo.DcbxMbsmPoolId =
        DCBXMemPoolIds[MAX_DCBX_MBSM_MAX_LC_SLOTS_SIZING_ID];
}

/*****************************************************************************/
/* Function Name      : ETSMainAssignMempoolIds                              */
/*                                                                           */
/* Description        : This function is used to assign respective mempool   */
/*                       ID's for ETS module.                                */
/*                                                                           */
/* Input (s)          : NONE.                                                */
/*                                                                           */
/* Output (s)         : NONE                                                 */
/*                                                                           */
/* Return Value(s)    : NONE                                                 */
/*                                                                           */
/*****************************************************************************/

VOID
ETSMainAssignMempoolIds (VOID)
{
    /*tETSPortEntry */
    gETSGlobalInfo.ETSPortPoolId =
        ETSMemPoolIds[MAX_DCBX_ETS_PORT_ENTRIES_SIZING_ID];
}

/*****************************************************************************/
/* Function Name      : PFCMainAssignMempoolIds                              */
/*                                                                           */
/* Description        : This function is used to assign respective mempool   */
/*                       ID's for PFC module.                                */
/*                                                                           */
/* Input (s)          : NONE.                                                */
/*                                                                           */
/* Output (s)         : NONE                                                 */
/*                                                                           */
/* Return Value(s)    : NONE                                                 */
/*                                                                           */
/*****************************************************************************/
VOID
PFCMainAssignMempoolIds (VOID)
{
    /*tPFCPortEntry */
    gPFCGlobalInfo.PFCPortPoolId =
        PFCMemPoolIds[MAX_DCBX_PFC_PORT_ENTRIES_SIZING_ID];
}

/***************************************************************************
 *  FUNCTION NAME : AppPriMainModuleStart
 * 
 *  DESCRIPTION   : This function is used to start the Application Priority
 *                  module.
 * 
 *  INPUT         : None
 * 
 *  OUTPUT        : None
 * 
 *  RETURNS       : OSIX_SUCCESS/OSIX_FAILURE
 * 
 * **************************************************************************/
PUBLIC INT4
AppPriMainModuleStart (VOID)
{
    /* Initialize Application Priority  Mempools */
    if (AppSizingMemCreateMemPools () == OSIX_FAILURE)
    {
        DCBX_TRC (DCBX_RESOURCE_TRC | DCBX_FAILURE_TRC, "AppPriMainModuleStart:"
                  " Memory Initialization FAILED !!!\r\n");
        AppSizingMemDeleteMemPools ();
        return OSIX_FAILURE;
    }

    /*Assigning respective mempool Id's */
    AppPriMainAssignMempoolIds ();

    /* Create Application Priority Port Table which will have all the 
     * Application priority port related information */
    gAppPriGlobalInfo.pRbAppPriPortTbl =
        RBTreeCreateEmbedded (FSAP_OFFSETOF (tAppPriPortEntry, RbNode),
                              AppPriUtlPortTblCmpFn);

    if (gAppPriGlobalInfo.pRbAppPriPortTbl == NULL)
    {
        DCBX_TRC (DCBX_RESOURCE_TRC | DCBX_FAILURE_TRC,
                  "AppPriMainModuleStart : "
                  "Application Priority Port Table -RBTreeCreateEmbedded ()"
                  " Failed!.\r\n");
        return OSIX_FAILURE;
    }
    /* Initialise Application Priority Global Values */
    gAppPriGlobalInfo.u1AppPriSystemCtrl = APP_PRI_START;
    gAppPriGlobalInfo.u1AppPriModStatus = APP_PRI_ENABLED;
    gAppPriGlobalInfo.u4AppPriTrapStatus = APP_PRI_MODULE_STATUS_TRAP |
        APP_PRI_ADMIN_MODE_TRAP;
    return OSIX_SUCCESS;
}

/***************************************************************************
 *  FUNCTION NAME : AppPriMainModuleShutDown
 * 
 *  DESCRIPTION   : This function is used to shutdown the Application Priority
 *                  module
 * 
 *  INPUT         : None
 * 
 *  OUTPUT        : None
 * 
 *  RETURNS       : None
 * 
 * **************************************************************************/
PUBLIC VOID
AppPriMainModuleShutDown (VOID)
{
    /* Delete all the Application Priority port entries */
    AppPriUtlDeleteAllPorts ();
    /* Intialise the default global variables and set module status as
     * disabled */
    gAppPriGlobalInfo.u1AppPriModStatus = APP_PRI_DISABLED;
    gAppPriGlobalInfo.u4AppPriTrapStatus = DCBX_ZERO;

    /* Delete Application Prioirty Port Table */
    if (gAppPriGlobalInfo.pRbAppPriPortTbl != NULL)
    {
        RBTreeDestroy (gAppPriGlobalInfo.pRbAppPriPortTbl, NULL, DCBX_ZERO);
        gAppPriGlobalInfo.pRbAppPriPortTbl = NULL;
    }

    /* DeIntialise the Application Prioirty MemPools */
    AppSizingMemDeleteMemPools ();
    return;
}

/*****************************************************************************/
/* Function Name      : AppPriMainAssignMempoolIds                           */
/*                                                                           */
/* Description        : This function is used to assign respective mempool   */
/*                       ID's for Application Priority module.               */
/*                                                                           */
/* Input (s)          : NONE.                                                */
/*                                                                           */
/* Output (s)         : NONE                                                 */
/*                                                                           */
/* Return Value(s)    : NONE                                                 */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
AppPriMainAssignMempoolIds (VOID)
{
    /*tAppPriPortEntry */
    gAppPriGlobalInfo.AppPriPortPoolId =
        APPMemPoolIds[MAX_DCBX_APP_PRI_PORTS_SIZING_ID];
    gAppPriGlobalInfo.AppPriMappingPoolId =
        APPMemPoolIds[MAX_DCBX_APP_PRI_ENTRIES_SIZING_ID];
}

/***************************************************************************
 *  FUNCTION NAME : CEEMainModuleStart
 * 
 *  DESCRIPTION   : This function is used to start the CEE module.
 * 
 *  INPUT         : None
 * 
 *  OUTPUT        : None
 * 
 *  RETURNS       : OSIX_SUCCESS/OSIX_FAILURE
 * 
 * **************************************************************************/
PUBLIC INT4
CEEMainModuleStart (VOID)
{
    /* Initialize CEE  Mempools */
    if (CeeSizingMemCreateMemPools () == OSIX_FAILURE)
    {
        DCBX_TRC (DCBX_RESOURCE_TRC | DCBX_FAILURE_TRC, "CeeMainModuleStart:"
                " Memory Initialization FAILED !!!\r\n");
        return OSIX_FAILURE;
    }

    /*Assigning respective mempool Id's */
    CEEMainAssignMempoolIds ();

    gCEEGlobalInfo.u4CEETrap = DCBX_ZERO;
    /* Create CEE Port Table which will have all the CEE port 
     * related information */
    gCEEGlobalInfo.pRbCeeCtrlTbl =
        RBTreeCreateEmbedded (FSAP_OFFSETOF (tCEECtrlEntry, RbNode),
                CEEUtlPortTblCmpFn);

    if (gCEEGlobalInfo.pRbCeeCtrlTbl == NULL)
    {
        DCBX_TRC (DCBX_RESOURCE_TRC | DCBX_FAILURE_TRC, "CEEMainModuleStart : "
                "CEE Ctrl Table -RBTreeCreateEmbedded ()" " Failed!.\r\n");
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}
/***************************************************************************
 *  FUNCTION NAME : CEEMainModuleShutDown
 * 
 *  DESCRIPTION   : This function is used to shutdown the entire CEE 
 *                  module
 * 
 *  INPUT         : None
 * 
 *  OUTPUT        : None
 * 
 *  RETURNS       : None
 * 
 * **************************************************************************/
PUBLIC VOID
CEEMainModuleShutDown (VOID)
{
    /* Delete all the CEE port entries */
    CEEUtlDeleteAllPorts ();
    /* Intialise the default global variables and set module status as
     * disabled */
    gCEEGlobalInfo.u4CEETrap = DCBX_ZERO;

    /* Delete CEE Port Table */
    if (gCEEGlobalInfo.pRbCeeCtrlTbl != NULL)
    {
        RBTreeDestroy (gCEEGlobalInfo.pRbCeeCtrlTbl, NULL, DCBX_ZERO);
        gCEEGlobalInfo.pRbCeeCtrlTbl = NULL;
    }

    /* DeIntialise the CEE MemPools */
    CeeSizingMemDeleteMemPools ();
    return;
}

/*****************************************************************************/
/* Function Name      : CEEMainAssignMempoolIds                              */
/*                                                                           */
/* Description        : This function is used to assign respective mempool   */
/*                      ID's for CEE module.                                 */
/*                                                                           */
/* Input (s)          : NONE.                                                */
/*                                                                           */
/* Output (s)         : NONE                                                 */
/*                                                                           */
/* Return Value(s)    : NONE                                                 */
/*                                                                           */
/*****************************************************************************/
VOID
CEEMainAssignMempoolIds (VOID)
{
    gCEEGlobalInfo.CEECtrlPoolId =
        CEEMemPoolIds[MAX_DCBX_CEE_PORT_ENTRIES_SIZING_ID];
}

#endif
