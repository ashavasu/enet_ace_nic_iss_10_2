/********************************************************************
 * Copyright (C) 2010 Aricent Inc . All Rights Reserved
 *
 * $Id: dcbxred.c,v 1.8 2017/01/25 13:19:42 siva Exp $
 *
 * Description: This file contains DCBX Redundancy support routines 
                and utility routines.
 *********************************************************************/
#ifndef DCBXRED_C
#define DCBXRED_C

#include "dcbxinc.h"
/***************************************************************************
 * FUNCTION NAME    : DcbxRedInitGlobalInfo   
 *
 * DESCRIPTION      : Initializes redundancy global variables.This function 
 *                    will get invoked during Dcbx Intialisation. 
 *                    It registers with RM.
 *
 * INPUT            : None
 *
 * OUTPUT           : None
 * 
 * RETURNS          : OSIX_SUCCESS/OSIX_FAILURE 
 * 
 **************************************************************************/
PUBLIC INT4
DcbxRedInitGlobalInfo (VOID)
{
    tRmRegParams        RmRegParams;

    MEMSET (&RmRegParams, DCBX_ZERO, sizeof (tRmRegParams));
    MEMSET (&(gDcbxGlobalInfo.DcbxRedGlobalInfo), DCBX_ZERO,
            sizeof (tDcbxRedGlobalInfo));

    RmRegParams.u4EntId = RM_DCBX_APP_ID;
    RmRegParams.pFnRcvPkt = DcbxRedRmCallBack;

    /* Registers the DCBX protocol with RM */
    if (DcbxPortRmRegisterProtocols (&RmRegParams) == OSIX_FAILURE)
    {
        DCBX_TRC (DCBX_RED_TRC | DCBX_FAILURE_TRC, "DcbxRedInitGlobalInfo:"
                  " Registration with RM FAILED !!!\r\n");
        return OSIX_FAILURE;
    }
    /* Default value of Node Status is IDLE */
    DCBX_RED_NODE_STATUS() = RM_INIT;
    DCBX_RED_STBY_NODE_UP_COUNT() = DcbxPortRmGetStandbyNodeCount ();

    return OSIX_SUCCESS;
}

/******************************************************************************
 * FUNCTION NAME    : DcbxRedDeInitGlobalInfo  
 *
 * DESCRIPTION      : Deinitializes redundancy global variables.This function 
 *                    will get invoked during BOOTUP failure. Also De-registers
 *                    with RM.
 *
 * INPUT            : None
 *
 * OUTPUT           : None
 * 
 * RETURNS          : None
 * 
 *****************************************************************************/
PUBLIC VOID
DcbxRedDeInitGlobalInfo (VOID)
{
    /* De Register the DCBX protocol with RM */
    DcbxPortRmDeRegisterProtocols ();
    MEMSET (&(gDcbxGlobalInfo.DcbxRedGlobalInfo), DCBX_ZERO,
            sizeof (tDcbxRedGlobalInfo));
    return;
}

/*****************************************************************************/
/* Function Name      : DcbxRedRmCallBack                                    */
/*                                                                           */
/* Description        : This function constructs a message containing the    */
/*                      given RM event and RM message and post it to the DCBX*/
/*                      queue.                                               */
/*                                                                           */
/* Input(s)           : u1Event - Event given by RM module                   */
/*                      pData   - Msg to be enqueue                          */
/*                      u2DataLen - Msg size                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : If msg is enqueued and event sent then OSIX_SUCCESS  */
/*                      Otherwise OSIX_FAILURE                               */
/*****************************************************************************/
PUBLIC INT4
DcbxRedRmCallBack (UINT1 u1Event, tRmMsg * pData, UINT2 u2DataLen)
{
    tDcbxQueueMsg      *pMsg = NULL;

    /* Check if Data received is valid or not */
    if (((u1Event == RM_MESSAGE) || (u1Event == RM_STANDBY_UP) ||
         (u1Event == RM_STANDBY_DOWN)) && (pData == NULL))
    {
        DCBX_TRC (DCBX_RED_TRC | DCBX_FAILURE_TRC,
                  "DcbxRedRmCallBack: Rcvd invalid message with pointer"
                  "  to buffer as NULL. \n");
        /* Message absent and hence no need to process */

        return OSIX_FAILURE;
    }

    /*Allocate memory for QueueMsg */
    if ((pMsg = (tDcbxQueueMsg *) MemAllocMemBlk
         (gDcbxGlobalInfo.DcbxQPktPoolId)) == NULL)
    {
        DCBX_TRC ((DCBX_RESOURCE_TRC | DCBX_FAILURE_TRC), "DcbxRedRmCallBack:"
                  "DCBX Queue Message memory Allocation Failed !!!\r\n");
        if (u1Event == RM_MESSAGE)
        {
            /* RM CRU Buffer Memory Release */
            RM_FREE (pData);
        }
        else if ((u1Event == RM_STANDBY_UP) || (u1Event == RM_STANDBY_DOWN))
        {
            /* Call RM API to release memory of data of these events */
            DcbxPortRmReleaseMemoryForMsg ((UINT1 *) pData);
        }
        return OSIX_FAILURE;

    }

    /*Initialise the data structure */
    MEMSET (pMsg, DCBX_ZERO, sizeof (tDcbxQueueMsg));

    pMsg->u4MsgType = DCBX_RM_MSG;
    pMsg->unMsgParam.DcbxRmMsg.pData = pData;
    pMsg->unMsgParam.DcbxRmMsg.u2DataLen = u2DataLen;
    pMsg->unMsgParam.DcbxRmMsg.u1Event = u1Event;

    /*Enqueue RM Message to DCBX and send the QMSG event to DCBX */
    if (DcbxQueEnqMsg (pMsg) == OSIX_FAILURE)
    {
        if (u1Event == RM_MESSAGE)
        {
            /* RM CRU Buffer Memory Release */
            RM_FREE (pData);
        }
        else if ((u1Event == RM_STANDBY_UP) || (u1Event == RM_STANDBY_DOWN))
        {
            /* Call RM API to release memory of data of these events */
            DcbxPortRmReleaseMemoryForMsg ((UINT1 *) pData);
        }

        DCBX_TRC (DCBX_RED_TRC | DCBX_FAILURE_TRC,
                  "DcbxRedRmCallBack: Enque RM Message to DCBX " "FAILEd \r\n");
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : DcbxRedHandleRmEvents
 *
 * DESCRIPTION      : This function process all the events and messages from
 *                    RM module.
 *
 * INPUT            : pMsg - pointer to DCBX Queue message.
 *
 * OUTPUT           : None.
 *
 * RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *
 **************************************************************************/
PUBLIC VOID
DcbxRedHandleRmEvents (tDcbxQueueMsg * pMsg)
{
    tRmNodeInfo        *pData = NULL;
    tRmProtoEvt         ProtoEvt;
    tRmProtoAck         ProtoAck;
    UINT4               u4SeqNum = DCBX_ZERO;

    MEMSET (&ProtoEvt, DCBX_ZERO, sizeof (tRmProtoEvt));
    MEMSET (&ProtoAck, DCBX_ZERO, sizeof (tRmProtoAck));

    ProtoEvt.u4AppId = RM_DCBX_APP_ID;
    ProtoEvt.u4Error = RM_NONE;

    switch (pMsg->unMsgParam.DcbxRmMsg.u1Event)
    {
        case GO_STANDBY:
            DcbxRedHandleGoStandby ();
            break;

        case GO_ACTIVE:
            DcbxRedHandleGoActive ();
            break;

        case RM_STANDBY_UP:
            DCBX_TRC ((DCBX_RED_TRC | DCBX_CONTROL_PLANE_TRC),
                      "DcbxRedHandleRmEvents: RM_STANDBY_UP event reached"
                      " Updating Standby Nodes Count.\r\n");
            pData = (tRmNodeInfo *) pMsg->unMsgParam.DcbxRmMsg.pData;

            if (pMsg->unMsgParam.DcbxRmMsg.u2DataLen == RM_NODE_COUNT_MSG_SIZE)
            {
                DCBX_RED_STBY_NODE_UP_COUNT() = pData->u1NumStandby;

                /* Before the arrival of STANDBY_UP event, Bulk Request has
                 * arrived from Peer DCBX in the Standby node. Hence send the
                 * Bulk updates Now.
                 */
                if (gDcbxGlobalInfo.DcbxRedGlobalInfo.bBulkReqRcvd == OSIX_TRUE)
                {
                    gDcbxGlobalInfo.DcbxRedGlobalInfo.bBulkReqRcvd = OSIX_FALSE;
                    DcbxRedHandleBulkRequest ();
                }
            }
            else
            {
                /* Data length is incorrect */
                DCBX_TRC ((DCBX_RED_TRC | DCBX_FAILURE_TRC),
                          "DcbxRedHandleRmEvents: RM_STANDBY_UP event reached"
                          "Data Lenght Incorrect. "
                          "Failed processing the event \r\n");
            }
            DcbxPortRmReleaseMemoryForMsg ((UINT1 *) pData);
            break;

        case RM_STANDBY_DOWN:
            DCBX_TRC ((DCBX_RED_TRC | DCBX_CONTROL_PLANE_TRC),
                      "DcbxRedHandleRmEvents: RM_STANDBY_DOWN event reached"
                      " Updating Standby Nodes Count.\r\n");
            pData = (tRmNodeInfo *) pMsg->unMsgParam.DcbxRmMsg.pData;

            if (pMsg->unMsgParam.DcbxRmMsg.u2DataLen == RM_NODE_COUNT_MSG_SIZE)
            {
                DCBX_RED_STBY_NODE_UP_COUNT() = pData->u1NumStandby;
            }
            else
            {
                /* Data length is incorrect */
                DCBX_TRC ((DCBX_RED_TRC | DCBX_FAILURE_TRC),
                          "DcbxRedHandleRmEvents: RM_STANDBY_DOWN event"
                          "received. Data Lenght Incorrect."
                          "Failed processing the event \r\n");
            }
            DcbxPortRmReleaseMemoryForMsg ((UINT1 *) pData);
            break;

        case RM_CONFIG_RESTORE_COMPLETE:
            DCBX_TRC ((DCBX_RED_TRC | DCBX_CONTROL_PLANE_TRC),
                      "DcbxRedHandleRmEvents: RM_CONFIG_RESTORE_COMPLETE"
                      "event received from RM\r\n");
            if (DCBX_RED_NODE_STATUS() == RM_INIT)
            {
                if (DcbxPortRmGetNodeState () == RM_STANDBY)
                {
                    DcbxRedHandleIdleToStandby ();
                    ProtoEvt.u4Event = RM_STANDBY_EVT_PROCESSED;
                    DcbxPortRmApiHandleProtocolEvent (&ProtoEvt);
                }
            }
            break;

        case L2_INITIATE_BULK_UPDATES:
            DCBX_GLOBAL_TRC("[STANDBY]: Received L2Initiate Bulk Update Message \r\n");
            DcbxRedSendBulkRequest ();
            break;

        case RM_MESSAGE:

            /* Read the sequence number from the RM Header */
            RM_PKT_GET_SEQNUM (pMsg->unMsgParam.DcbxRmMsg.pData, &u4SeqNum);
            DCBX_TRC (DCBX_CONTROL_PLANE_TRC, "DcbxRedHandleRmEvents "
                      "Processing RM_Msg.\r\n");
            /* Remove the RM Header */
            RM_STRIP_OFF_RM_HDR (pMsg->unMsgParam.DcbxRmMsg.pData,
                                 pMsg->unMsgParam.DcbxRmMsg.u2DataLen);

            ProtoAck.u4AppId = RM_DCBX_APP_ID;
            ProtoAck.u4SeqNumber = u4SeqNum;

            if (DCBX_RED_NODE_STATUS() == RM_ACTIVE)
            {
                /* Process the RM Messages at the Active Node */
                DcbxRedProcessPeerMsgAtActive (pMsg->unMsgParam.DcbxRmMsg.pData,
                                               pMsg->unMsgParam.DcbxRmMsg.
                                               u2DataLen);

            }
            else if (DCBX_RED_NODE_STATUS()
                     == RM_STANDBY)
            {
                /* Process the RM Messages at the Standby Node */
                DcbxRedProcessPeerMsgAtStandby (pMsg->unMsgParam.DcbxRmMsg.
                                                pData,
                                                pMsg->unMsgParam.DcbxRmMsg.
                                                u2DataLen);
            }
            else
            {
                DCBX_TRC (ALL_FAILURE_TRC,
                          "DcbxRedHandleRmEvents: Sync-up message"
                          "received at Idle Node!!!!\r\n");
            }

            RM_FREE (pMsg->unMsgParam.DcbxRmMsg.pData);

            /* Sending ACK to RM */
            DcbxPortRmApiSendProtoAckToRM (&ProtoAck);
            break;

        default:
            break;
    }
}

/***********************************************************************
 * FUNCTION NAME    : DcbxRedHandleGoActive
 *
 * DESCRIPTION      : This routine handles the GO_ACTIVE event and responds
 *                    to RM with an acknowledgement.
 *
 * INPUT            : None
 *
 * OUTPUT           : None
 *
 * RETURNS          : VOID
 *
 **********************************************************************/
PUBLIC VOID
DcbxRedHandleGoActive (VOID)
{
    tRmProtoEvt         ProtoEvt;

    MEMSET (&ProtoEvt, DCBX_ZERO, sizeof (tRmProtoEvt));

    ProtoEvt.u4AppId = RM_DCBX_APP_ID;
    ProtoEvt.u4Error = RM_NONE;

    /*check for node status */
    if (DCBX_RED_NODE_STATUS() == RM_ACTIVE)
    {
        DCBX_TRC ((DCBX_RED_TRC | DCBX_FAILURE_TRC),
                  "DcbxRedHandleGoActive:GO_ACTIVE event"
                  "readched when node is already in active state\r\n");
        return;
    }

    /* Idle to Active node status transition */
    if (DCBX_RED_NODE_STATUS() == RM_INIT)
    {
        DCBX_TRC ((DCBX_RED_TRC | DCBX_CONTROL_PLANE_TRC),
                  "DcbxRedHandleGoActive:" " Idle to Active status transition");

        /* Update RM event */
        DcbxRedHandleIdleToActive ();

        /* Update the node status */
        ProtoEvt.u4Event = RM_IDLE_TO_ACTIVE_EVT_PROCESSED;

    }

    /* Stand By to Active node transition */
    if (DCBX_RED_NODE_STATUS() == RM_STANDBY)
    {
        DCBX_TRC ((DCBX_RED_TRC | DCBX_CONTROL_PLANE_TRC),
                  "DcbxRedHandleGoActive:"
                  " StandBy to Active status transition");

        /* Update RM Event */
        DcbxRedHandleStandByToActive ();

        /* Update the node status */
        ProtoEvt.u4Event = RM_STANDBY_TO_ACTIVE_EVT_PROCESSED;
    }

    /*Before the arrival of STANDBY_UP event, Bulk Request has
     *arrived from Peer DCBX in the Standby node. Hence send the
     *Bulk updates Now.
     */
    if (gDcbxGlobalInfo.DcbxRedGlobalInfo.bBulkReqRcvd == OSIX_TRUE)
    {
        gDcbxGlobalInfo.DcbxRedGlobalInfo.bBulkReqRcvd = OSIX_FALSE;
        DcbxRedHandleBulkRequest ();
    }
    DcbxPortRmApiHandleProtocolEvent (&ProtoEvt);
    return;
}

/*****************************************************************************
 * FUNCTION NAME    : DcbxRedHandleIdleToActive
 *
 * DESCRIPTION      : This routine updates the node status on transition from
 *                    Idle to Active.
 *
 * INPUT            : None
 *
 * OUTPUT           : None
 *
 * RETURNS          : VOID
 *
 ****************************************************************************/
PUBLIC VOID
DcbxRedHandleIdleToActive (VOID)
{
    /* Process Idle to Active Event */
    DCBX_RED_NODE_STATUS() = RM_ACTIVE;
    DCBX_RED_STBY_NODE_UP_COUNT() = DcbxPortRmGetStandbyNodeCount ();
    return;
}

/*****************************************************************************
 * FUNCTION NAME    : DcbxRedHandleStandByToActive
 *
 * DESCRIPTION      : This routine updates the node status on transition from
 *                    Stand By to Active.
 *
 * INPUT            : None
 *
 * OUTPUT           : None
 *
 * RETURNS          : VOID
 *
 ****************************************************************************/
PUBLIC VOID
DcbxRedHandleStandByToActive (VOID)
{
    /* Process Standby to Active event */
    DCBX_RED_NODE_STATUS() = RM_ACTIVE;
    DCBX_RED_STBY_NODE_UP_COUNT() = DcbxPortRmGetStandbyNodeCount ();
    return;
}

/***********************************************************************
 * FUNCTION NAME    : DcbxRedHandleGoStandBy
 *
 * DESCRIPTION      : This routine handles the GO_STANDBY event and responds
 *                    to RM with an acknowledgement.
 *
 * INPUT            : None
 *
 * OUTPUT           : None
 *
 * RETURNS          : VOID
 *
 **********************************************************************/
PUBLIC VOID
DcbxRedHandleGoStandby (VOID)
{
    tRmProtoEvt         ProtoEvt;

    MEMSET (&ProtoEvt, DCBX_ZERO, sizeof (tRmProtoEvt));

    ProtoEvt.u4AppId = RM_DCBX_APP_ID;
    ProtoEvt.u4Error = RM_NONE;


    /*check for node status */
    if (DCBX_RED_NODE_STATUS() == RM_STANDBY)
    {
        DCBX_TRC ((DCBX_RED_TRC | DCBX_FAILURE_TRC),
                  "DcbxRedHandleGoStandBy:GO_STANDBY event"
                  "readched when node is already in standby state\r\n");
    }

    /* Idle to  Stand By received */
    else if (DCBX_RED_NODE_STATUS() == RM_INIT)
    {
        DCBX_TRC ((DCBX_RED_TRC | DCBX_CONTROL_PLANE_TRC),
                  "DcbxRedHandleGoStandBy:GO_STANDBY event"
                  " recived when node state is Idle. "
                  "Ignoring this event, node will become standby "
                  " when CONFIG_RESTORE_COMPLETE is received \r\n");
    }

    /*Active to StandBy transition */
    else if (DCBX_RED_NODE_STATUS() == RM_ACTIVE)
    {
        DCBX_TRC ((DCBX_RED_TRC | DCBX_CONTROL_PLANE_TRC),
                  "DcbxRedHandleGoStandBy: Active to Standby"
                  " transition...\r\n");

        /* Update RM Event */
        DcbxRedHandleActiveToStandby ();

        /* Update the node status */
        ProtoEvt.u4Event = RM_STANDBY_EVT_PROCESSED;
        DcbxPortRmApiHandleProtocolEvent (&ProtoEvt);
    }
    return;
}

/***************************************************************************
 * FUNCTION NAME    : DcbxRedHandleActiveToStandby
 *
 * DESCRIPTION      : On Active to Standby transition, the following actions
 *                    are performed,
 *                    1. Update the Node Status
 *                    2. ReInitialise the Number of standBy nodes to Zero.
 *
 * INPUT            : None
 *
 * OUTPUT           : None
 *
 * RETURNS          : None
 *
 **************************************************************************/
PUBLIC VOID
DcbxRedHandleActiveToStandby (VOID)
{
    /* Process the Active to Standby Event */
    DCBX_RED_NODE_STATUS() = RM_STANDBY;
    DCBX_RED_STBY_NODE_UP_COUNT() = DCBX_ZERO;
    return;
}

/******************************************************************************
 * FUNCTION NAME    : DcbxRedHandleIdleToStandby
 *
 * DESCRIPTION      : This routine updates the node status from idle to standby.
 *
 * INPUT            : None
 *
 * OUTPUT           : None
 *
 * RETURNS          : VOID
 *
 *****************************************************************************/
PUBLIC VOID
DcbxRedHandleIdleToStandby (VOID)
{
    /* Process the Idle to Standby Event */
    DCBX_RED_NODE_STATUS() = RM_STANDBY;
    DCBX_RED_STBY_NODE_UP_COUNT() = DCBX_ZERO;
    return;
}

/***************************************************************************
 * FUNCTION NAME    : DcbxRedProcessPeerMsgAtActive
 *
 * DESCRIPTION      : This routine handles messages from the other node
 *                    (Standby) at Active. The messages that are handled in
 *                    Active node are
 *                    1. RM_BULK_UPDT_REQ_MSG
 *
 * INPUT            : pMsg - RM Data buffer holding messages
 *                    u2DataLen - Length of data in buffer.
 *
 * OUTPUT           : None
 * 
 * RETURNS          : VOID
 * 
 **************************************************************************/
PUBLIC VOID
DcbxRedProcessPeerMsgAtActive (tRmMsg * pMsg, UINT2 u2DataLen)
{
    tRmProtoEvt         ProtoEvt;
    UINT4               u4OffSet = DCBX_ZERO;
    UINT2               u2Length = DCBX_ZERO;
    UINT1               u1MsgType = DCBX_ZERO;

    MEMSET (&ProtoEvt, DCBX_ZERO, sizeof (tRmProtoEvt));

    ProtoEvt.u4AppId = RM_DCBX_APP_ID;


    DCBX_RM_GET_1_BYTE (pMsg, &u4OffSet, u1MsgType);
    DCBX_RM_GET_2_BYTE (pMsg, &u4OffSet, u2Length);

    if (u4OffSet != u2DataLen)
    {
        /* Currently, the only RM packet expected to be processed at 
         * active node is Bulk Request message which has only Type and 
         * length. Hence this validation is done.
         */
        ProtoEvt.u4Error = RM_PROCESS_FAIL;

        DcbxPortRmApiHandleProtocolEvent (&ProtoEvt);
        return;
    }

    if (u2Length != DCBX_RED_BULK_REQ_SIZE)
    {
        ProtoEvt.u4Error = RM_PROCESS_FAIL;

        DcbxPortRmApiHandleProtocolEvent (&ProtoEvt);
        return;
    }

    if (u1MsgType == DCBX_RED_BULK_REQUEST_MSG)
    {
        if (DCBX_RED_STBY_NODE_UP_COUNT() == DCBX_ZERO)
        {
            /* This is a special case, where bulk request msg from
             * standby is coming before RM_STANDBY_UP. So no need to
             * process the bulk request now. Bulk updates will be send
             * on RM_STANDBY_UP event.
             */
            gDcbxGlobalInfo.DcbxRedGlobalInfo.bBulkReqRcvd = OSIX_TRUE;
            return;
        }
        DcbxRedHandleBulkRequest ();
    }
    return;
}

/***************************************************************************
 * FUNCTION NAME    : DcbxRedProcessPeerMsgAtStandby
 *
 * DESCRIPTION      : This routine handles messages from the other node
 *                    (Active) at standby. The messages that are handled in
 *                    Standby node are
 *                    1. RM_BULK_UPDT_TAIL_MSG
 *                    2. Dynamic sync-up messages
 *                    3. Dynamic bulk messages
 *
 * INPUT            : pMsg - RM Data buffer holding messages
 *                    u2DataLen - Length of data in buffer.
 *
 * OUTPUT           : None
 * 
 * RETURNS          : VOID
 * 
 **************************************************************************/
PUBLIC VOID
DcbxRedProcessPeerMsgAtStandby (tRmMsg * pMsg, UINT2 u2DataLen)
{
    UINT4               u4OffSet = DCBX_ZERO;
    UINT2               u2Length = DCBX_ZERO;
    UINT2               u2RemMsgLen = DCBX_ZERO;
    UINT2               u2MinLen = DCBX_ZERO;
    UINT1               u1MsgType = DCBX_ZERO;


    u2MinLen = DCBX_RED_TYPE_FIELD_SIZE + DCBX_RED_LEN_FIELD_SIZE;

    while ((u4OffSet + u2MinLen) <= u2DataLen)
    {
        DCBX_RM_GET_1_BYTE (pMsg, &u4OffSet, u1MsgType);

        DCBX_RM_GET_2_BYTE (pMsg, &u4OffSet, u2Length);

        if (u2Length < u2MinLen)
        {
            /* The Length field in the RM packet is less than minimum 
             * number of bytes, which is MessageType + Length. 
             */
            u4OffSet += (UINT4) u2Length;
            DCBX_TRC_ARG1 (DCBX_RED_TRC, "In func %s: "
                      "The Length field in the RM packet is less than minimum"
                      " number of bytes, which is MessageType + Length.\r\n", 
                      __func__);
            continue;
        }

        u2RemMsgLen = (UINT2) (u2Length - u2MinLen);

        if ((u4OffSet + u2RemMsgLen) > u2DataLen)
        {
            /* The Length field in the RM packet is wrong, hence continuing 
             * with the next packet */
            u4OffSet = u2DataLen;

            DCBX_TRC_ARG3 (DCBX_RED_TRC, "In func %s: "
                      "The Length field in the RM packet is wrong."
                      " u2RemMsgLen = %d  u2DataLen=%d \r\n", 
                      __func__, u2RemMsgLen, u2DataLen);
            continue;
        }

        switch (u1MsgType)
        {
            case DCBX_RED_BULK_UPD_TAIL_MESSAGE:
                u4OffSet = DCBX_ZERO;
                DcbxRedProcessBulkTailMsg (pMsg, &u4OffSet);
                break;
            case DCBX_RED_ETS_REM_INFO:
                DcbxRedProcessDynamicEtsRemInfo (pMsg, &u4OffSet, u2RemMsgLen);
                break;
            case DCBX_RED_ETS_SEM_INFO:
                DcbxRedProcessDynamicEtsSemInfo (pMsg, &u4OffSet, u2RemMsgLen);
                break;
            case DCBX_RED_PFC_REM_INFO:
                DcbxRedProcessDynamicPfcRemInfo (pMsg, &u4OffSet, u2RemMsgLen);
                break;
            case DCBX_RED_PFC_SEM_INFO:
                DcbxRedProcessDynamicPfcSemInfo (pMsg, &u4OffSet, u2RemMsgLen);
                break;
            case DCBX_RED_ETS_ALL_COUNTER_INFO:
                DcbxRedProcessDynamicEtsAllCounterInfo (pMsg, &u4OffSet,
                                                        u2RemMsgLen);
                break;
            case DCBX_RED_PFC_ALL_COUNTER_INFO:
                DcbxRedProcessDynamicPfcAllCounterInfo (pMsg, &u4OffSet,
                                                        u2RemMsgLen);
                break;
            case DCBX_RED_ETS_COUNTER_INFO:
                DcbxRedProcessDynamicEtsCounterInfo (pMsg, &u4OffSet,
                                                     u2RemMsgLen);
                break;
            case DCBX_RED_PFC_COUNTER_INFO:
                DcbxRedProcessDynamicPfcCounterInfo (pMsg, &u4OffSet,
                                                     u2RemMsgLen);
                break;
            case DCBX_CEE_RED_CTRL_COUNTER_INFO:
                DcbxCeeRedProcessDynCtrlCounterInfo (pMsg, &u4OffSet,
                                                     u2RemMsgLen);
                break;
            case DCBX_RED_APP_PRI_REM_INFO:
                DcbxRedProcessDynamicAppPriRemInfo (pMsg, &u4OffSet,
                                                    u2RemMsgLen);
                break;
            case DCBX_RED_APP_PRI_SEM_INFO:
                DcbxRedProcessDynamicAppPriSemInfo (pMsg, &u4OffSet,
                                                    u2RemMsgLen);
                break;
            case DCBX_RED_APP_PRI_COUNTER_INFO:
                DcbxRedProcessDynamicAppPriCounterInfo (pMsg, &u4OffSet,
                                                        u2RemMsgLen);
                break;
            case DCBX_RED_APP_PRI_ALL_COUNTER_INFO:
                DcbxRedProcessDynamicAppPriAllCounterInfo (pMsg, &u4OffSet,
                                                           u2RemMsgLen);
                break;
            case DCBX_CEE_RED_DYN_SYNC_INFO:
                DcbxCEERedProcDynamicSyncInfo (pMsg, &u4OffSet, u2RemMsgLen);
                break;
            case DCBX_CEE_RED_PFC_REM_INFO:
                DcbxCeeRedProcessDynamicPfcRemInfo (pMsg, &u4OffSet, 
                        u2RemMsgLen);
                break;
            case DCBX_CEE_RED_ETS_REM_INFO:
                DcbxCeeRedProcessDynamicEtsRemInfo (pMsg, &u4OffSet, 
                        u2RemMsgLen);
                break;
            case DCBX_CEE_RED_APP_PRI_REM_INFO:
                DcbxCeeRedProcessDynamicAppPriRemInfo (pMsg, &u4OffSet, 
                        u2RemMsgLen);
                break;
            case DCBX_CEE_RED_DCBX_PORT_INFO:
                DcbxRedProcessDynamicDcbxPortInfo (pMsg, &u4OffSet,
                                                u2RemMsgLen);
                break;
            case DCBX_CEE_RED_CTRL_INFO:
                DcbxCeeRedProcessBulkCtrlInfoSyncMsg (pMsg, &u4OffSet, u2RemMsgLen);
                break;
            case DCBX_CEE_RED_ALL_FEAT_AGE_OUT_INFO:
                DcbxCeeRedProcessDynSyncAgeOutInfo (pMsg, &u4OffSet,
                        u2RemMsgLen);
                break;
            case DCBX_RED_DCBX_STATUS_INFO:
                DcbxRedProcessDynamicDcbxStatusInfo (pMsg, &u4OffSet,
                        u2RemMsgLen);
                break;
            default:
                u4OffSet += (UINT4) u2Length;    /* Skip the attribute */
                break;
        }
    }
    return;
}

/***************************************************************************
 * FUNCTION NAME    : DcbxRedProcessDynamicEtsRemInfo
 *
 * DESCRIPTION      : This routine decodes the sync-up message from peer active
 *                    DCBX and updates ETS Reomte information in standby DCBX.
 *                    The timers are also started/stopped at the standby node.
 *
 * INPUT            : pMsg - RM Sync-up message
 *                    u2RemMsgLen - Length of value of Message
 *                    
 * OUTPUT           : u4OffSet - Offset value 
 * 
 * RETURNS          : VOID 
 * 
 **************************************************************************/
PUBLIC VOID
DcbxRedProcessDynamicEtsRemInfo (tRmMsg * pMsg, UINT4 *pu4OffSet,
                                 UINT2 u2RemMsgLen)
{
    tETSPortEntry      *pEtsPortInfo = NULL;
    tRmProtoEvt         ProtoEvt;
    UINT4               u4IfIndex = DCBX_ZERO;


    MEMSET (&ProtoEvt, DCBX_ZERO, sizeof (tRmProtoEvt));

    ProtoEvt.u4AppId = RM_DCBX_APP_ID;
    ProtoEvt.u4Event = RM_BULK_UPDT_ABORT;

    if (u2RemMsgLen != DCBX_RED_ETS_REM_INFO_VALUE_SIZE)
    {
        ProtoEvt.u4Error = RM_PROCESS_FAIL;

        DcbxPortRmApiHandleProtocolEvent (&ProtoEvt);
        *pu4OffSet += u2RemMsgLen;
        return;
    }

    DCBX_RM_GET_4_BYTE (pMsg, pu4OffSet, u4IfIndex);

    pEtsPortInfo = ETSUtlGetPortEntry (u4IfIndex);

    if (pEtsPortInfo == NULL)
    {
        DCBX_TRC (DCBX_RED_TRC | DCBX_FAILURE_TRC,
                  "DcbxRedProcessDynamicEtsRemInfo: No ETS Entry Found \n");
        return;
    }
    DCBX_RM_GET_4_BYTE (pMsg, pu4OffSet,
                        pEtsPortInfo->ETSRemPortInfo.u4ETSRemTimeMark);
    DCBX_RM_GET_4_BYTE (pMsg, pu4OffSet,
                        *(UINT4 *) &(pEtsPortInfo->ETSRemPortInfo.
                                     i4ETSRemIndex));
    DCBX_RM_GET_N_BYTE (pMsg, pEtsPortInfo->ETSRemPortInfo.au1ETSRemBW,
                        pu4OffSet, ETS_MAX_NUM_TCGID);
    DCBX_RM_GET_N_BYTE (pMsg, pEtsPortInfo->ETSRemPortInfo.au1ETSRemTCGID,
                        pu4OffSet, DCBX_MAX_PRIORITIES);
    DCBX_RM_GET_N_BYTE (pMsg, pEtsPortInfo->ETSRemPortInfo.au1ETSRemRecoBW,
                        pu4OffSet, ETS_MAX_NUM_TCGID);
    DCBX_RM_GET_N_BYTE (pMsg, pEtsPortInfo->ETSRemPortInfo.au1ETSDestRemMacAddr,
                        pu4OffSet, MAC_ADDR_LEN);
    DCBX_RM_GET_1_BYTE (pMsg, pu4OffSet,
                        pEtsPortInfo->ETSRemPortInfo.u1ETSRemNumTcSup);
    DCBX_RM_GET_1_BYTE (pMsg, pu4OffSet,
                        pEtsPortInfo->ETSRemPortInfo.u1ETSRemNumTCGSup);
    DCBX_RM_GET_1_BYTE (pMsg, pu4OffSet,
                        pEtsPortInfo->ETSRemPortInfo.u1ETSRemWilling);
    DCBX_RM_GET_1_BYTE (pMsg, pu4OffSet, pEtsPortInfo->u1RemTlvUpdStatus);
    return;
}

/***************************************************************************
 * FUNCTION NAME    : DcbxRedProcessDynamicEtsSemInfo
 *
 * DESCRIPTION      : This routine decodes the sync-up message from peer active
 *                    DCBX and updates the ETS SEM information in standby DCBX.
 *                    The timers are also started/stopped at the standby node.
 *
 * INPUT            : pMsg - RM Sync-up message
 *                    u2RemMsgLen - Length of value of Message
 *                    
 * OUTPUT           : u4OffSet - Offset value 
 * 
 * RETURNS          : VOID 
 * 
 **************************************************************************/
PUBLIC VOID
DcbxRedProcessDynamicEtsSemInfo (tRmMsg * pMsg, UINT4 *pu4OffSet,
                                 UINT2 u2RemMsgLen)
{
    tETSPortEntry      *pEtsPortInfo = NULL;
    tRmProtoEvt         ProtoEvt;
    UINT4               u4IfIndex = DCBX_ZERO;
    UINT1               u1OperState = DCBX_ZERO;


    MEMSET (&ProtoEvt, DCBX_ZERO, sizeof (tRmProtoEvt));

    ProtoEvt.u4AppId = RM_DCBX_APP_ID;
    ProtoEvt.u4Event = RM_BULK_UPDT_ABORT;

    if (u2RemMsgLen != DCBX_RED_ETS_SEM_INFO_VALUE_SIZE)
    {
        ProtoEvt.u4Error = RM_PROCESS_FAIL;

        DcbxPortRmApiHandleProtocolEvent (&ProtoEvt);
        *pu4OffSet += u2RemMsgLen;
        return;
    }

    DCBX_RM_GET_4_BYTE (pMsg, pu4OffSet, u4IfIndex);

    pEtsPortInfo = ETSUtlGetPortEntry (u4IfIndex);

    if (pEtsPortInfo == NULL)
    {
        DCBX_TRC (DCBX_RED_TRC | DCBX_FAILURE_TRC,
                  "DcbxRedProcessDynamicEtsSeRmInfo: No ETS Entry Found \n");
        return;
    }
    DCBX_RM_GET_1_BYTE (pMsg, pu4OffSet, u1OperState);

    DCBX_TRC_ARG2 (DCBX_CONTROL_PLANE_TRC, "DcbxRedProcessDynamicEtsSemInfo:"
                   " IfIndex = %d OperStatae = %d\n", u4IfIndex, u1OperState);

    /* Call Oper State Change */
    ETSUtlOperStateChange (pEtsPortInfo, u1OperState);

    return;
}

/***************************************************************************
 * FUNCTION NAME    : DcbxRedProcessDynamicEtsAllCounterInfo
 *
 * DESCRIPTION      : This routine decodes the sync-up message from peer active
 *                    DCBX and updates ETS Counter information in standby DCBX.
 *                    This will be used in Dynamic Bulk Update.
 *
 * INPUT            : pMsg - RM Sync-up message
 *                    u2RemMsgLen - Length of value of Message
 *                    
 * OUTPUT           : u4OffSet - Offset value 
 * 
 * RETURNS          : VOID 
 * 
 **************************************************************************/
PUBLIC VOID
DcbxRedProcessDynamicEtsAllCounterInfo (tRmMsg * pMsg, UINT4 *pu4OffSet,
                                        UINT2 u2RemMsgLen)
{
    tETSPortEntry      *pEtsPortInfo = NULL;
    tRmProtoEvt         ProtoEvt;
    UINT4               u4IfIndex = DCBX_ZERO;


    MEMSET (&ProtoEvt, DCBX_ZERO, sizeof (tRmProtoEvt));

    ProtoEvt.u4AppId = RM_DCBX_APP_ID;
    ProtoEvt.u4Event = RM_BULK_UPDT_ABORT;

    if (u2RemMsgLen != DCBX_RED_ETS_ALL_COUNTER_INFO_VALUE_SIZE)
    {
        ProtoEvt.u4Error = RM_PROCESS_FAIL;

        DcbxPortRmApiHandleProtocolEvent (&ProtoEvt);
        *pu4OffSet += u2RemMsgLen;
        return;
    }

    DCBX_RM_GET_4_BYTE (pMsg, pu4OffSet, u4IfIndex);

    pEtsPortInfo = ETSUtlGetPortEntry (u4IfIndex);

    if (pEtsPortInfo == NULL)
    {
        DCBX_TRC (DCBX_RED_TRC | DCBX_FAILURE_TRC,
                  "DcbxRedProcessDynamicEtsAllCounterInfo: No ETS Entry Found \n");
        return;
    }
    DCBX_RM_GET_4_BYTE (pMsg, pu4OffSet, pEtsPortInfo->u4ETSConfTxTLVCount);
    DCBX_RM_GET_4_BYTE (pMsg, pu4OffSet, pEtsPortInfo->u4ETSConfRxTLVCount);
    DCBX_RM_GET_4_BYTE (pMsg, pu4OffSet, pEtsPortInfo->u4ETSConfRxTLVError);
    DCBX_RM_GET_4_BYTE (pMsg, pu4OffSet, pEtsPortInfo->u4ETSRecoTxTLVCount);
    DCBX_RM_GET_4_BYTE (pMsg, pu4OffSet, pEtsPortInfo->u4ETSRecoRxTLVCount);
    DCBX_RM_GET_4_BYTE (pMsg, pu4OffSet, pEtsPortInfo->u4ETSRecoRxTLVError);
    DCBX_RM_GET_4_BYTE (pMsg, pu4OffSet, pEtsPortInfo->u4ETSTcSuppTxTLVCount);
    DCBX_RM_GET_4_BYTE (pMsg, pu4OffSet, pEtsPortInfo->u4ETSTcSuppRxTLVCount);
    DCBX_RM_GET_4_BYTE (pMsg, pu4OffSet, pEtsPortInfo->u4ETSTcSupRxTLVError);

    return;
}

/***************************************************************************
 * FUNCTION NAME    : DcbxRedProcessDynamicEtsCounterInfo
 *
 * DESCRIPTION      : This routine decodes the sync-up message from peer active
 *                    DCBX and updates ETS Counter information in standby DCBX.
 *
 * INPUT            : pMsg - RM Sync-up message
 *                    u2RemMsgLen - Length of value of Message
 *                    
 * OUTPUT           : u4OffSet - Offset value 
 * 
 * RETURNS          : VOID 
 * 
 **************************************************************************/
PUBLIC VOID
DcbxRedProcessDynamicEtsCounterInfo (tRmMsg * pMsg, UINT4 *pu4OffSet,
                                     UINT2 u2RemMsgLen)
{
    tETSPortEntry      *pEtsPortInfo = NULL;
    tRmProtoEvt         ProtoEvt;
    UINT4               u4IfIndex = DCBX_ZERO;
    UINT4               u4CounterVal = DCBX_ZERO;
    UINT1               u1CounterType = DCBX_ZERO;


    MEMSET (&ProtoEvt, DCBX_ZERO, sizeof (tRmProtoEvt));

    ProtoEvt.u4AppId = RM_DCBX_APP_ID;
    ProtoEvt.u4Event = RM_BULK_UPDT_ABORT;

    if (u2RemMsgLen != DCBX_RED_ETS_COUNTER_INFO_VALUE_SIZE)
    {
        ProtoEvt.u4Error = RM_PROCESS_FAIL;

        DcbxPortRmApiHandleProtocolEvent (&ProtoEvt);
        *pu4OffSet += u2RemMsgLen;
        return;
    }

    DCBX_RM_GET_1_BYTE (pMsg, pu4OffSet, u1CounterType);
    DCBX_RM_GET_4_BYTE (pMsg, pu4OffSet, u4IfIndex);

    pEtsPortInfo = ETSUtlGetPortEntry (u4IfIndex);

    if (pEtsPortInfo == NULL)
    {
        DCBX_TRC (DCBX_RED_TRC | DCBX_FAILURE_TRC,
                  "DcbxRedProcessDynamicEtsCounterInfo: No ETS Entry Found \n");
        return;
    }
    DCBX_RM_GET_4_BYTE (pMsg, pu4OffSet, u4CounterVal);
    if (u1CounterType == DCBX_RED_ETS_CONF_TX_COUNTER)
    {
        pEtsPortInfo->u4ETSConfTxTLVCount = u4CounterVal;
    }
    if (u1CounterType == DCBX_RED_ETS_CONF_RX_COUNTER)
    {
        pEtsPortInfo->u4ETSConfRxTLVCount = u4CounterVal;
    }
    if (u1CounterType == DCBX_RED_ETS_CONF_ERR_COUNTER)
    {
        pEtsPortInfo->u4ETSConfRxTLVError = u4CounterVal;
    }
    if (u1CounterType == DCBX_RED_ETS_RECO_TX_COUNTER)
    {
        pEtsPortInfo->u4ETSRecoTxTLVCount = u4CounterVal;
    }
    if (u1CounterType == DCBX_RED_ETS_RECO_RX_COUNTER)
    {
        pEtsPortInfo->u4ETSRecoRxTLVCount = u4CounterVal;
    }
    if (u1CounterType == DCBX_RED_ETS_RECO_ERR_COUNTER)
    {
        pEtsPortInfo->u4ETSRecoRxTLVError = u4CounterVal;
    }
    if (u1CounterType == DCBX_RED_ETS_TC_SUPP_TX_COUNTER)
    {
        pEtsPortInfo->u4ETSTcSuppTxTLVCount = u4CounterVal;
    }
    if (u1CounterType == DCBX_RED_ETS_TC_SUPP_RX_COUNTER)
    {
        pEtsPortInfo->u4ETSTcSuppRxTLVCount = u4CounterVal;
    }
    if (u1CounterType == DCBX_RED_ETS_TC_SUPP_ERR_COUNTER)
    {
        pEtsPortInfo->u4ETSTcSupRxTLVError = u4CounterVal;
    }

    return;
}

/***************************************************************************
 * FUNCTION NAME    : DcbxRedProcessDynamicPfcRemInfo
 *
 * DESCRIPTION      : This routine decodes the sync-up message from peer active
 *                    DCBX and updates PFC Reomte information in standby DCBX.
 *                    The timers are also started/stopped at the standby node.
 *
 * INPUT            : pMsg - RM Sync-up message
 *                    u2RemMsgLen - Length of value of Message
 *                    
 * OUTPUT           : u4OffSet - Offset value 
 * 
 * RETURNS          : VOID 
 * 
 **************************************************************************/
PUBLIC VOID
DcbxRedProcessDynamicPfcRemInfo (tRmMsg * pMsg, UINT4 *pu4OffSet,
                                 UINT2 u2RemMsgLen)
{
    tPFCPortEntry      *pPfcPortInfo = NULL;
    tRmProtoEvt         ProtoEvt;
    UINT4               u4IfIndex = DCBX_ZERO;


    MEMSET (&ProtoEvt, DCBX_ZERO, sizeof (tRmProtoEvt));

    ProtoEvt.u4AppId = RM_DCBX_APP_ID;
    ProtoEvt.u4Event = RM_BULK_UPDT_ABORT;

    if (u2RemMsgLen != DCBX_RED_PFC_REM_INFO_VALUE_SIZE)
    {
        ProtoEvt.u4Error = RM_PROCESS_FAIL;

        DcbxPortRmApiHandleProtocolEvent (&ProtoEvt);
        *pu4OffSet += (UINT4) u2RemMsgLen;
        return;
    }

    DCBX_RM_GET_4_BYTE (pMsg, pu4OffSet, u4IfIndex);

    pPfcPortInfo = PFCUtlGetPortEntry (u4IfIndex);

    if (pPfcPortInfo == NULL)
    {
        DCBX_TRC (DCBX_RED_TRC | DCBX_FAILURE_TRC,
                  "DcbxRedProcessDynamicPfcRemInfo: No PFC Entry Found \n");
        return;
    }
    DCBX_RM_GET_4_BYTE (pMsg, pu4OffSet,
                        pPfcPortInfo->PFCRemPortInfo.u4PFCRemTimeMark);
    DCBX_RM_GET_4_BYTE (pMsg, pu4OffSet,
                        *(UINT4 *) &(pPfcPortInfo->PFCRemPortInfo.
                                     i4PFCRemIndex));
    DCBX_RM_GET_N_BYTE (pMsg, pPfcPortInfo->PFCRemPortInfo.au1PFCDestRemMacAddr,
                        pu4OffSet, MAC_ADDR_LEN);
    DCBX_RM_GET_1_BYTE (pMsg, pu4OffSet,
                        pPfcPortInfo->PFCRemPortInfo.u1PFCRemWilling);
    DCBX_RM_GET_1_BYTE (pMsg, pu4OffSet,
                        pPfcPortInfo->PFCRemPortInfo.u1PFCRemCap);
    DCBX_RM_GET_1_BYTE (pMsg, pu4OffSet,
                        pPfcPortInfo->PFCRemPortInfo.u1PFCRemMBC);
    DCBX_RM_GET_1_BYTE (pMsg, pu4OffSet,
                        pPfcPortInfo->PFCRemPortInfo.u1PFCRemStatus);

    return;
}

/***************************************************************************
 * FUNCTION NAME    : DcbxRedProcessDynamicPfcSemInfo
 *
 * DESCRIPTION      : This routine decodes the sync-up message from peer active
 *                    DCBX and updates the PFC SEM information in standby DCBX.
 *                    The timers are also started/stopped at the standby node.
 *
 * INPUT            : pMsg - RM Sync-up message
 *                    u2RemMsgLen - Length of value of Message
 *                    
 * OUTPUT           : u4OffSet - Offset value 
 * 
 * RETURNS          : VOID 
 * 
 **************************************************************************/
PUBLIC VOID
DcbxRedProcessDynamicPfcSemInfo (tRmMsg * pMsg, UINT4 *pu4OffSet,
                                 UINT2 u2RemMsgLen)
{
    tPFCPortEntry      *pPfcPortInfo = NULL;
    tRmProtoEvt         ProtoEvt;
    UINT4               u4IfIndex = DCBX_ZERO;
    UINT1               u1OperState = DCBX_ZERO;

    MEMSET (&ProtoEvt, DCBX_ZERO, sizeof (tRmProtoEvt));


    ProtoEvt.u4AppId = RM_DCBX_APP_ID;
    ProtoEvt.u4Event = RM_BULK_UPDT_ABORT;

    if (u2RemMsgLen != DCBX_RED_PFC_SEM_INFO_VALUE_SIZE)
    {
        ProtoEvt.u4Error = RM_PROCESS_FAIL;

        DcbxPortRmApiHandleProtocolEvent (&ProtoEvt);
        *pu4OffSet += (UINT4) u2RemMsgLen;
        return;
    }

    DCBX_RM_GET_4_BYTE (pMsg, pu4OffSet, u4IfIndex);

    pPfcPortInfo = PFCUtlGetPortEntry (u4IfIndex);

    if (pPfcPortInfo == NULL)
    {
        DCBX_TRC (DCBX_RED_TRC | DCBX_FAILURE_TRC,
                  "DcbxRedProcessDynamicPfcSemInfo: No PFC Entry Found \n");
        return;
    }
    DCBX_RM_GET_1_BYTE (pMsg, pu4OffSet, u1OperState);

    /* Call Oper State Change */
    PFCUtlOperStateChange (pPfcPortInfo, u1OperState);

    return;
}

/***************************************************************************
 * FUNCTION NAME    : DcbxRedProcessDynamicPfcAllCounterInfo
 *
 * DESCRIPTION      : This routine decodes the sync-up message from peer active
 *                    DCBX and updates PFC Counter information in standby DCBX.
 *
 * INPUT            : pMsg - RM Sync-up message
 *                    u2RemMsgLen - Length of value of Message
 *                    
 * OUTPUT           : u4OffSet - Offset value 
 * 
 * RETURNS          : VOID 
 * 
 **************************************************************************/
PUBLIC VOID
DcbxRedProcessDynamicPfcAllCounterInfo (tRmMsg * pMsg, UINT4 *pu4OffSet,
                                        UINT2 u2RemMsgLen)
{
    tPFCPortEntry      *pPfcPortInfo = NULL;
    tRmProtoEvt         ProtoEvt;
    UINT4               u4IfIndex = DCBX_ZERO;


    MEMSET (&ProtoEvt, DCBX_ZERO, sizeof (tRmProtoEvt));

    ProtoEvt.u4AppId = RM_DCBX_APP_ID;
    ProtoEvt.u4Event = RM_BULK_UPDT_ABORT;

    if (u2RemMsgLen != DCBX_RED_PFC_ALL_COUNTER_INFO_VALUE_SIZE)
    {
        ProtoEvt.u4Error = RM_PROCESS_FAIL;

        DcbxPortRmApiHandleProtocolEvent (&ProtoEvt);
        *pu4OffSet += u2RemMsgLen;
        return;
    }

    DCBX_RM_GET_4_BYTE (pMsg, pu4OffSet, u4IfIndex);

    pPfcPortInfo = PFCUtlGetPortEntry (u4IfIndex);

    if (pPfcPortInfo == NULL)
    {
        DCBX_TRC (DCBX_RED_TRC | DCBX_FAILURE_TRC,
                  "DcbxRedProcessDynamicPfcAllCounterInfo: No PFC Entry Found \n");
        return;
    }
    DCBX_RM_GET_4_BYTE (pMsg, pu4OffSet, pPfcPortInfo->u4PFCTxTLVCount);
    DCBX_RM_GET_4_BYTE (pMsg, pu4OffSet, pPfcPortInfo->u4PFCRxTLVCount);
    DCBX_RM_GET_4_BYTE (pMsg, pu4OffSet, pPfcPortInfo->u4PFCRxTLVError);

    return;
}

/***************************************************************************
 * FUNCTION NAME    : DcbxRedProcessDynamicPfcCounterInfo
 *
 * DESCRIPTION      : This routine decodes the sync-up message from peer active
 *                    DCBX and updates PFC Counter information in standby DCBX.
 *
 * INPUT            : pMsg - RM Sync-up message
 *                    u2RemMsgLen - Length of value of Message
 *                    
 * OUTPUT           : u4OffSet - Offset value 
 * 
 * RETURNS          : VOID 
 * 
 **************************************************************************/
PUBLIC VOID
DcbxRedProcessDynamicPfcCounterInfo (tRmMsg * pMsg, UINT4 *pu4OffSet,
                                     UINT2 u2RemMsgLen)
{
    tPFCPortEntry      *pPfcPortInfo = NULL;
    tRmProtoEvt         ProtoEvt;
    UINT4               u4IfIndex = DCBX_ZERO;
    UINT4               u4CounterVal = DCBX_ZERO;
    UINT1               u1CounterType = DCBX_ZERO;


    MEMSET (&ProtoEvt, DCBX_ZERO, sizeof (tRmProtoEvt));

    ProtoEvt.u4AppId = RM_DCBX_APP_ID;
    ProtoEvt.u4Event = RM_BULK_UPDT_ABORT;

    if (u2RemMsgLen != DCBX_RED_PFC_COUNTER_INFO_VALUE_SIZE)
    {
        ProtoEvt.u4Error = RM_PROCESS_FAIL;

        DcbxPortRmApiHandleProtocolEvent (&ProtoEvt);

        *pu4OffSet += u2RemMsgLen;
        return;
    }

    DCBX_RM_GET_1_BYTE (pMsg, pu4OffSet, u1CounterType);
    DCBX_RM_GET_4_BYTE (pMsg, pu4OffSet, u4IfIndex);

    pPfcPortInfo = PFCUtlGetPortEntry (u4IfIndex);

    if (pPfcPortInfo == NULL)
    {
        DCBX_TRC (DCBX_RED_TRC | DCBX_FAILURE_TRC,
                  "DcbxRedProcessDynamicPfcCounterInfo: No PFC Entry Found \n");
        return;
    }
    DCBX_RM_GET_4_BYTE (pMsg, pu4OffSet, u4CounterVal);
    if (u1CounterType == DCBX_RED_PFC_TX_COUNTER)
    {
        pPfcPortInfo->u4PFCTxTLVCount = u4CounterVal;
    }
    if (u1CounterType == DCBX_RED_PFC_RX_COUNTER)
    {
        pPfcPortInfo->u4PFCRxTLVCount = u4CounterVal;
    }
    if (u1CounterType == DCBX_RED_PFC_ERR_COUNTER)
    {
        pPfcPortInfo->u4PFCRxTLVError = u4CounterVal;
    }
    return;
}

/***************************************************************************
 * FUNCTION NAME    : DcbxRedProcessBulkTailMsg 
 *
 * DESCRIPTION      : This routine process the bulk update tail message and 
 *                    send bulk updates.
 *
 * INPUT            : pMsg - RM Data buffer holding messages
 *                    u2DataLen - Length of data in buffer.
 *
 * OUTPUT           : pu4OffSet - Offset Value
 * 
 * RETURNS          : VOID
 * 
 **************************************************************************/
PUBLIC VOID
DcbxRedProcessBulkTailMsg (tRmMsg * pMsg, UINT4 *pu4OffSet)
{
    tRmProtoEvt         ProtoEvt;
    UINT2               u2Length = DCBX_ZERO;
    UINT1               u1MsgType = DCBX_ZERO;


    MEMSET (&ProtoEvt, DCBX_ZERO, sizeof (tRmProtoEvt));

    ProtoEvt.u4AppId = RM_DCBX_APP_ID;

    DCBX_RM_GET_1_BYTE (pMsg, pu4OffSet, u1MsgType);
    DCBX_RM_GET_2_BYTE (pMsg, pu4OffSet, u2Length);

    if (u2Length != DCBX_RED_BULK_UPD_TAIL_MSG_SIZE)
    {
        ProtoEvt.u4Error = RM_PROCESS_FAIL;

        DcbxPortRmApiHandleProtocolEvent (&ProtoEvt);

        return;
    }

    if (u1MsgType == DCBX_RED_BULK_UPD_TAIL_MESSAGE)
    {
        DCBX_GLOBAL_TRC("[STANDBY]: Received Bulk Update Tail Message \r\n");
        ProtoEvt.u4Error = RM_NONE;
        ProtoEvt.u4Event = RM_PROTOCOL_BULK_UPDT_COMPLETION;
        DcbxPortRmApiHandleProtocolEvent (&ProtoEvt);
    }
    return;
}

/*****************************************************************************/
/* Function Name      : DcbxRedSendBulkRequest                               */
/*                                                                           */
/* Description        : This function sends Bulk Request Message to the      */
/*                      Actuve Node.                                         */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
PUBLIC VOID
DcbxRedSendBulkRequest (VOID)
{
    tRmMsg             *pMsg = NULL;
    UINT4               u4Offset = DCBX_ZERO;

    pMsg = RM_ALLOC_TX_BUF (DCBX_RED_BULK_REQ_SIZE);

    DCBX_TRC ((DCBX_RED_TRC | DCBX_CONTROL_PLANE_TRC),
              "DcbxRedSendBulkRequest: Sending " "Bulk Request \r\n");

    if (pMsg == NULL)
    {
        DCBX_TRC (DCBX_RED_TRC | DCBX_FAILURE_TRC,
                  "DcbxRedSendBulkRequest: Allocation "
                  "of memory from RM Failed\n");
        return;
    }
    DCBX_RM_PUT_1_BYTE (pMsg, &u4Offset, DCBX_RED_BULK_REQUEST_MSG);
    DCBX_RM_PUT_2_BYTE (pMsg, &u4Offset, DCBX_RED_BULK_REQ_SIZE);
    
    DCBX_GLOBAL_TRC("[STANDBY]: Send Bulk Request Message \r\n");
    DcbxPortRmEnqMsgToRm (pMsg, (UINT2) u4Offset, RM_DCBX_APP_ID,
                          RM_DCBX_APP_ID);
    return;
}

/*****************************************************************************/
/* Function Name      : DcbxRedHandleBulkRequest                             */
/*                                                                           */
/* Description        : This function process the Bulk request Message       */
/*                      send by the Standby Node.                            */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
PUBLIC VOID
DcbxRedHandleBulkRequest (VOID)
{

    DCBX_GLOBAL_TRC("[ACTIVE]: Received Bulk Request \r\n");
    if (DCBX_RED_STBY_NODE_UP_COUNT() != DCBX_ZERO)
    {
        DcbxRedBulkSyncDcbxPortInfo();
        DcbxCeeRedBulkSyncCtrlInfo ();

        DcbxRedBulkSyncEtsRemInfo ();
        DcbxRedBulkSyncEtsSemInfo ();
        DcbxRedBulkSyncEtsCounterInfo ();
        DcbxCeeRedBulkSyncEtsRemInfo (); /* Send CEE related RemInfo. */

        DcbxRedBulkSyncPfcRemInfo ();
        DcbxRedBulkSyncPfcSemInfo ();
        DcbxRedBulkSyncPfcCounterInfo ();
        DcbxCeeRedBulkSyncPfcRemInfo (); /* Send CEE related RemInfo. */

        DcbxRedBulkSyncAppPriRemInfo ();
        DcbxRedBulkSyncAppPriSemInfo ();
        DcbxRedBulkSyncAppPriCounterInfo ();
        DcbxCeeRedBulkSyncAppPriRemInfo (); /* Send CEE related RemInfo. */
    }

    DcbxPortRmSetBulkUpdatesStatus ();
    DcbxRedSendBulkUpdTailMsg ();
    return;
}

/***************************************************************************
 * FUNCTION NAME    : DcbxRedSendBulkUpdTailMsg 
 *
 * DESCRIPTION      : This function will send the bulk update tail msg to the
 *                    standby node, which indicates the completion of Bulk
 *                    update process.
 *
 * INPUT            : None
 *
 * OUTPUT           : None
 * 
 * RETURNS          : None 
 * 
 **************************************************************************/
PUBLIC VOID
DcbxRedSendBulkUpdTailMsg (VOID)
{
    tRmMsg             *pMsg = NULL;
    UINT4               u4OffSet = DCBX_ZERO;


    pMsg = RM_ALLOC_TX_BUF (DCBX_RED_BULK_UPD_TAIL_MSG_SIZE);

    if (pMsg == NULL)
    {
        DCBX_TRC (DCBX_RED_TRC | DCBX_FAILURE_TRC,
                  "DcbxRedSendBulkUpdTailMsg: Allocation "
                  "of memory from RM Failed\n");
        return;
    }

    DCBX_RM_PUT_1_BYTE (pMsg, &u4OffSet, DCBX_RED_BULK_UPD_TAIL_MESSAGE);
    DCBX_RM_PUT_2_BYTE (pMsg, &u4OffSet, DCBX_RED_BULK_UPD_TAIL_MSG_SIZE);

    DCBX_GLOBAL_TRC("[ACTIVE]: Send Bulk Update Tail Message \r\n");
    /* This routine sends the message to RM and in case of failure 
     * releases the RM buffer memory
     */
    DcbxPortRmEnqMsgToRm (pMsg, (UINT2) u4OffSet, RM_DCBX_APP_ID,
                          RM_DCBX_APP_ID);

    return;
}

/*****************************************************************************/
/* Function Name      : DcbxRedBulkSyncEtsRemInfo                            */
/*                                                                           */
/* Description        : This function sends the ETS Remote table             */
/*                      Bulk Sync up Message to Stadnby Node from            */
/*                      the Active Node.                                     */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
PUBLIC VOID
DcbxRedBulkSyncEtsRemInfo (VOID)
{
    tRmMsg             *pMsg = NULL;
    tETSPortEntry      *pEtsPortInfo = NULL;
    tETSPortEntry       ETSPortEntry;
    UINT4               u4OffSet = DCBX_ZERO;


    pMsg = RM_ALLOC_TX_BUF (DCBX_DB_MAX_BUF_SIZE);

    if (pMsg == NULL)
    {
        DCBX_TRC (DCBX_RED_TRC | DCBX_FAILURE_TRC,
                  "DcbxRedBulkSyncEtsRemInfo: Allocation "
                  "of memory from RM Failed\n");
        return;
    }
    pEtsPortInfo = (tETSPortEntry *)
        RBTreeGetFirst (gETSGlobalInfo.pRbETSPortTbl);
    if (pEtsPortInfo == NULL)
    {
        DCBX_TRC (DCBX_RED_TRC | DCBX_CONTROL_PLANE_TRC,
                  "DcbxRedBulkSyncEtsRemInfo: No ETS entry" "is present \n");
        RM_FREE (pMsg);
        return;
    }

    while (pEtsPortInfo != NULL)
    {
        DcbxRedFormEtsRemInfoSyncMsg (pEtsPortInfo, pMsg, &u4OffSet);

        if ((DCBX_DB_MAX_BUF_SIZE - u4OffSet) < DCBX_RED_ETS_REM_INFO_SIZE)
        {

            if (DcbxPortRmEnqMsgToRm (pMsg, (UINT2) u4OffSet,
                                      RM_DCBX_APP_ID,
                                      RM_DCBX_APP_ID) == OSIX_FAILURE)
            {
                DCBX_TRC (DCBX_RED_TRC | DCBX_FAILURE_TRC,
                          "DcbxRedBulkSyncEtsRemInfo: Enqueue "
                          "message to RM Failed\r\n");
                return;
            }

            u4OffSet = DCBX_ZERO;
            pMsg = RM_ALLOC_TX_BUF (DCBX_DB_MAX_BUF_SIZE);

            if (pMsg == NULL)
            {
                DCBX_TRC (DCBX_RED_TRC | DCBX_FAILURE_TRC,
                          "DcbxRedBulkSyncEtsRemInfo: Allocation "
                          "of memory from RM Failed\n");
                return;
            }
        }

        MEMSET (&ETSPortEntry, DCBX_ZERO, sizeof (tETSPortEntry));
        ETSPortEntry.u4IfIndex = pEtsPortInfo->u4IfIndex;
        pEtsPortInfo = (tETSPortEntry *)
            RBTreeGetNext (gETSGlobalInfo.pRbETSPortTbl,
                           &ETSPortEntry, DcbxUtlPortTblCmpFn);
    }

    DcbxPortRmEnqMsgToRm (pMsg, (UINT2) u4OffSet, RM_DCBX_APP_ID,
                          RM_DCBX_APP_ID);
    return;
}

/*****************************************************************************/
/* Function Name      : DcbxRedBulkSyncEtsSemInfo                            */
/*                                                                           */
/* Description        : This function sends the ETS SEM Info                 */
/*                      Bulk Sync up Message to Stadnby Node from            */
/*                      the Active Node.                                     */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
PUBLIC VOID
DcbxRedBulkSyncEtsSemInfo (VOID)
{
    tRmMsg             *pMsg = NULL;
    tETSPortEntry      *pEtsPortInfo = NULL;
    tETSPortEntry       ETSPortEntry;
    UINT4               u4OffSet = DCBX_ZERO;


    pMsg = RM_ALLOC_TX_BUF (DCBX_DB_MAX_BUF_SIZE);

    if (pMsg == NULL)
    {
        DCBX_TRC (DCBX_RED_TRC | DCBX_FAILURE_TRC,
                  "DcbxRedBulkSyncEtsSemInfo: Allocation "
                  "of memory from RM Failed\n");
        return;
    }
    pEtsPortInfo = (tETSPortEntry *)
        RBTreeGetFirst (gETSGlobalInfo.pRbETSPortTbl);
    if (pEtsPortInfo == NULL)
    {
        DCBX_TRC (DCBX_RED_TRC | DCBX_CONTROL_PLANE_TRC,
                  "DcbxRedBulkSyncEtsSemInfo: No ETS entry" "is present \n");
        RM_FREE (pMsg);
        return;
    }

    while (pEtsPortInfo != NULL)
    {
        DcbxRedFormEtsSemInfoSyncMsg (pEtsPortInfo, pMsg, &u4OffSet);

        if ((DCBX_DB_MAX_BUF_SIZE - u4OffSet) < DCBX_RED_ETS_SEM_INFO_SIZE)
        {

            if (DcbxPortRmEnqMsgToRm (pMsg, (UINT2) u4OffSet,
                                      RM_DCBX_APP_ID,
                                      RM_DCBX_APP_ID) == OSIX_FAILURE)
            {
                DCBX_TRC (DCBX_RED_TRC | DCBX_FAILURE_TRC,
                          "DcbxRedBulkSyncEtsSemInfo: Enqueue "
                          "message to RM Failed\r\n");
                return;
            }

            u4OffSet = DCBX_ZERO;
            pMsg = RM_ALLOC_TX_BUF (DCBX_DB_MAX_BUF_SIZE);

            if (pMsg == NULL)
            {
                DCBX_TRC (DCBX_RED_TRC | DCBX_FAILURE_TRC,
                          "DcbxRedBulkSyncEtsSemInfo: Allocation "
                          "of memory from RM Failed\n");
                return;
            }
        }
        MEMSET (&ETSPortEntry, DCBX_ZERO, sizeof (tETSPortEntry));
        ETSPortEntry.u4IfIndex = pEtsPortInfo->u4IfIndex;
        pEtsPortInfo = (tETSPortEntry *)
            RBTreeGetNext (gETSGlobalInfo.pRbETSPortTbl,
                           &ETSPortEntry, DcbxUtlPortTblCmpFn);
    }

    DcbxPortRmEnqMsgToRm (pMsg, (UINT2) u4OffSet,
                          RM_DCBX_APP_ID, RM_DCBX_APP_ID);
    return;
}

/*****************************************************************************/
/* Function Name      : DcbxRedBulkSyncEtsCounterInfo                        */
/*                                                                           */
/* Description        : This function sends the ETS Counters Info            */
/*                      Bulk Sync up Message to Stadnby Node from            */
/*                      the Active Node.                                     */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
PUBLIC VOID
DcbxRedBulkSyncEtsCounterInfo (VOID)
{
    tRmMsg             *pMsg = NULL;
    tETSPortEntry      *pEtsPortInfo = NULL;
    tETSPortEntry       ETSPortEntry;
    UINT4               u4OffSet = DCBX_ZERO;


    pMsg = RM_ALLOC_TX_BUF (DCBX_DB_MAX_BUF_SIZE);

    if (pMsg == NULL)
    {
        DCBX_TRC (DCBX_RED_TRC | DCBX_FAILURE_TRC,
                  "DcbxRedBulkSyncEtsCounterInfo: Allocation "
                  "of memory from RM Failed\n");
        return;
    }
    pEtsPortInfo = (tETSPortEntry *)
        RBTreeGetFirst (gETSGlobalInfo.pRbETSPortTbl);
    if (pEtsPortInfo == NULL)
    {
        DCBX_TRC (DCBX_RED_TRC | DCBX_CONTROL_PLANE_TRC,
                  "DcbxRedBulkSyncEtsCounterInfo : No ETS entry"
                  "is present \n");
        RM_FREE (pMsg);
        return;
    }
    while (pEtsPortInfo != NULL)
    {
        DcbxRedFormEtsAllCounterInfoSyncMsg (pEtsPortInfo, pMsg, &u4OffSet);

        if ((DCBX_DB_MAX_BUF_SIZE - u4OffSet) <
            DCBX_RED_ETS_ALL_COUNTER_INFO_SIZE)
        {

            if (DcbxPortRmEnqMsgToRm (pMsg, (UINT2) u4OffSet,
                                      RM_DCBX_APP_ID,
                                      RM_DCBX_APP_ID) == OSIX_FAILURE)
            {
                DCBX_TRC (DCBX_RED_TRC | DCBX_FAILURE_TRC,
                          "DcbxRedBulkSyncEtsCounterInfo: Enqueue "
                          "message to RM Failed\r\n");
                return;
            }
            u4OffSet = DCBX_ZERO;
            pMsg = RM_ALLOC_TX_BUF (DCBX_DB_MAX_BUF_SIZE);

            if (pMsg == NULL)
            {
                DCBX_TRC (DCBX_RED_TRC | DCBX_FAILURE_TRC,
                          "DcbxRedBulkSyncEtsCounterInfo: Allocation "
                          "of memory from RM Failed\n");
                return;
            }
        }
        MEMSET (&ETSPortEntry, DCBX_ZERO, sizeof (tETSPortEntry));
        ETSPortEntry.u4IfIndex = pEtsPortInfo->u4IfIndex;
        pEtsPortInfo = (tETSPortEntry *)
            RBTreeGetNext (gETSGlobalInfo.pRbETSPortTbl,
                           &ETSPortEntry, DcbxUtlPortTblCmpFn);
    }

    DcbxPortRmEnqMsgToRm (pMsg, (UINT2) u4OffSet,
                          RM_DCBX_APP_ID, RM_DCBX_APP_ID);
    return;
}

/*****************************************************************************/
/* Function Name      : DcbxRedSendDynamicEtsRemInfo                         */
/*                                                                           */
/* Description        : This function sends the ETS Remote table             */
/*                      Dynamic Sync up Message to Stadnby Node from         */
/*                      the Active Node.                                     */
/*                                                                           */
/* Input(s)           : pEtsPortInfo - ETS Port Entry                        */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
PUBLIC VOID
DcbxRedSendDynamicEtsRemInfo (tETSPortEntry * pEtsPortInfo)
{
    tRmMsg             *pMsg = NULL;
    UINT4               u4OffSet = DCBX_ZERO;


    if ((DCBX_RED_NODE_STATUS() == RM_STANDBY) ||
        (DCBX_RED_STBY_NODE_UP_COUNT() == DCBX_ZERO))
    {
        DCBX_TRC (DCBX_RED_TRC | DCBX_CONTROL_PLANE_TRC,
                  "DcbxRedSendDynamicEtsRemInfo: No Need to send Sync"
                  "Message as Standby is down or the node is Active\n");
        return;
    }
    pMsg = RM_ALLOC_TX_BUF (DCBX_RED_ETS_REM_INFO_SIZE);

    if (pMsg == NULL)
    {
        DCBX_TRC (DCBX_RED_TRC | DCBX_FAILURE_TRC,
                  "DcbxRedSendDynamicEtsRemInfo: Allocation "
                  "of memory from RM Failed\n");
        return;
    }
    DcbxRedFormEtsRemInfoSyncMsg (pEtsPortInfo, pMsg, &u4OffSet);

    DcbxPortRmEnqMsgToRm (pMsg, (UINT2) u4OffSet,
                          RM_DCBX_APP_ID, RM_DCBX_APP_ID);
    return;
}

/*****************************************************************************/
/* Function Name      : DcbxRedSendDynamicEtsSemInfo                         */
/*                                                                           */
/* Description        : This function sends the ETS SEM information          */
/*                      Dynamic Sync up Message to Stadnby Node from         */
/*                      the Active Node.                                     */
/*                                                                           */
/* Input(s)           : pEtsPortInfo - ETS Port Entry                        */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
PUBLIC VOID
DcbxRedSendDynamicEtsSemInfo (tETSPortEntry * pEtsPortInfo)
{
    tRmMsg             *pMsg = NULL;
    UINT4               u4OffSet = DCBX_ZERO;


    if ((DCBX_RED_NODE_STATUS() == RM_STANDBY) ||
        (DCBX_RED_STBY_NODE_UP_COUNT() == DCBX_ZERO))
    {
        DCBX_TRC (DCBX_RED_TRC | DCBX_CONTROL_PLANE_TRC,
                  "DcbxRedSendDynamicEtsSemInfo: No Need to send Sync"
                  "Message as Standby is down or the node is Active\n");
        return;
    }

    pMsg = RM_ALLOC_TX_BUF (DCBX_RED_ETS_SEM_INFO_SIZE);

    if (pMsg == NULL)
    {
        DCBX_TRC (DCBX_RED_TRC | DCBX_FAILURE_TRC,
                  "DcbxRedSendDynamicEtsSemInfo: Allocation "
                  "of memory from RM  Failed\n");
        return;
    }
    DCBX_TRC (DCBX_CONTROL_PLANE_TRC, "DcbxRedSendDynamicEtsSemInfo:"
              "Calling DcbxRedFormEtsSemInfoSyncMsg\r\n");
    DcbxRedFormEtsSemInfoSyncMsg (pEtsPortInfo, pMsg, &u4OffSet);

    DcbxPortRmEnqMsgToRm (pMsg, (UINT2) u4OffSet,
                          RM_DCBX_APP_ID, RM_DCBX_APP_ID);
    return;
}

/*****************************************************************************/
/* Function Name      : DcbxRedSendDynamicEtsCounterInfo                      */
/*                                                                           */
/* Description        : This function sends the ETS Counter information      */
/*                      Dynamic Sync up Message to Stadnby Node from         */
/*                      the Active Node.                                     */
/*                                                                           */
/* Input(s)           : pEtsPortInfo - ETS Port Entry                        */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
PUBLIC VOID
DcbxRedSendDynamicEtsCounterInfo (tETSPortEntry * pEtsPortInfo,
                                  UINT1 u1CounterType)
{
    tRmMsg             *pMsg = NULL;
    UINT4               u4OffSet = DCBX_ZERO;


    if ((DCBX_RED_NODE_STATUS() == RM_STANDBY) ||
        (DCBX_RED_STBY_NODE_UP_COUNT() == DCBX_ZERO))
    {
        DCBX_TRC (DCBX_RED_TRC | DCBX_CONTROL_PLANE_TRC,
                  "DcbxRedSendDynamicEtsCounterInfo: No Need to send Sync"
                  "Message as Standby is down or the node is Active\n");
        return;
    }

    pMsg = RM_ALLOC_TX_BUF (DCBX_RED_ETS_COUNTER_INFO_SIZE);

    if (pMsg == NULL)
    {
        DCBX_TRC (DCBX_RED_TRC | DCBX_FAILURE_TRC,
                  "DcbxRedSendDynamicEtsCounterInfo: Allocation "
                  "of memory from RM  Failed\n");
        return;
    }
    DcbxRedFormEtsCounterInfoSyncMsg (pEtsPortInfo, pMsg, &u4OffSet,
                                      u1CounterType);
    DcbxPortRmEnqMsgToRm (pMsg, (UINT2) u4OffSet,
                          RM_DCBX_APP_ID, RM_DCBX_APP_ID);
    return;
}

/*****************************************************************************/
/* Function Name      : DcbxRedFormEtsRemInfoSyncMsg                         */
/*                                                                           */
/* Description        : This function forms the Dynamic Sync Up message for  */
/*                      ETS Remote table which needs to be sent to Standby   */
/*                      Node from the Active Node.                           */
/*                                                                           */
/* Input(s)           : pEtsPortInfo - ETS Port Entry                        */
/*                      pMsg - RM Message Structure that needs to be filled  */
/*                      pu4OffSet - OffSet Value                             */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
PUBLIC VOID
DcbxRedFormEtsRemInfoSyncMsg (tETSPortEntry * pEtsPortInfo,
                              tRmMsg * pMsg, UINT4 *pu4OffSet)
{
    UINT2               u2MsgLen = DCBX_ZERO;


    u2MsgLen = DCBX_RED_TYPE_FIELD_SIZE + DCBX_RED_LEN_FIELD_SIZE +
        DCBX_RED_ETS_REM_INFO_VALUE_SIZE;

    DCBX_RM_PUT_1_BYTE (pMsg, pu4OffSet, DCBX_RED_ETS_REM_INFO);
    DCBX_RM_PUT_2_BYTE (pMsg, pu4OffSet, u2MsgLen);
    DCBX_RM_PUT_4_BYTE (pMsg, pu4OffSet, pEtsPortInfo->u4IfIndex);
    DCBX_RM_PUT_4_BYTE (pMsg, pu4OffSet,
                        pEtsPortInfo->ETSRemPortInfo.u4ETSRemTimeMark);
    DCBX_RM_PUT_4_BYTE (pMsg, pu4OffSet,
                        pEtsPortInfo->ETSRemPortInfo.i4ETSRemIndex);
    DCBX_RM_PUT_N_BYTE (pMsg,
                        pEtsPortInfo->ETSRemPortInfo.au1ETSRemBW,
                        pu4OffSet, ETS_MAX_NUM_TCGID);
    DCBX_RM_PUT_N_BYTE (pMsg,
                        pEtsPortInfo->ETSRemPortInfo.au1ETSRemTCGID,
                        pu4OffSet, DCBX_MAX_PRIORITIES);
    DCBX_RM_PUT_N_BYTE (pMsg,
                        pEtsPortInfo->ETSRemPortInfo.au1ETSRemRecoBW,
                        pu4OffSet, ETS_MAX_NUM_TCGID);
    DCBX_RM_PUT_N_BYTE (pMsg,
                        pEtsPortInfo->ETSRemPortInfo.au1ETSDestRemMacAddr,
                        pu4OffSet, MAC_ADDR_LEN);
    DCBX_RM_PUT_1_BYTE (pMsg, pu4OffSet,
                        pEtsPortInfo->ETSRemPortInfo.u1ETSRemNumTcSup);
    DCBX_RM_PUT_1_BYTE (pMsg, pu4OffSet,
                        pEtsPortInfo->ETSRemPortInfo.u1ETSRemNumTCGSup);
    DCBX_RM_PUT_1_BYTE (pMsg, pu4OffSet,
                        pEtsPortInfo->ETSRemPortInfo.u1ETSRemWilling);
    DCBX_RM_PUT_1_BYTE (pMsg, pu4OffSet, pEtsPortInfo->u1RemTlvUpdStatus);
    return;
}

/*****************************************************************************/
/* Function Name      : DcbxRedFormEtsSemInfoSyncMsg                         */
/*                                                                           */
/* Description        : This function forms the Dynamic Sync Up message for  */
/*                      ETS SEM information which needs to be sent to Standby*/
/*                      Node from the Active Node.                           */
/*                                                                           */
/* Input(s)           : pEtsPortInfo - ETS Port Entry                        */
/*                      pMsg - RM Message Structure that needs to be filled  */
/*                      pu4OffSet - OffSet Value                             */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
PUBLIC VOID
DcbxRedFormEtsSemInfoSyncMsg (tETSPortEntry * pEtsPortInfo,
                              tRmMsg * pMsg, UINT4 *pu4OffSet)
{
    UINT2               u2MsgLen = DCBX_ZERO;

    u2MsgLen = DCBX_RED_TYPE_FIELD_SIZE + DCBX_RED_LEN_FIELD_SIZE +
        DCBX_RED_ETS_SEM_INFO_VALUE_SIZE;
    DCBX_TRC_ARG3 (DCBX_CONTROL_PLANE_TRC, "DcbxRedFormEtsSemInfoSyncMsg:"
                   "MsgLen:%d IfIndex:%d OperState:%d\r\n", u2MsgLen,
                   pEtsPortInfo->u4IfIndex, pEtsPortInfo->u1ETSDcbxOperState);
    DCBX_RM_PUT_1_BYTE (pMsg, pu4OffSet, DCBX_RED_ETS_SEM_INFO);
    DCBX_RM_PUT_2_BYTE (pMsg, pu4OffSet, u2MsgLen);
    DCBX_RM_PUT_4_BYTE (pMsg, pu4OffSet, pEtsPortInfo->u4IfIndex);
    DCBX_RM_PUT_1_BYTE (pMsg, pu4OffSet, pEtsPortInfo->u1ETSDcbxOperState);

    return;
}

/*****************************************************************************/
/* Function Name      : DcbxRedFormEtsCounterInfoSyncMsg                     */
/*                                                                           */
/* Description        : This function forms the Dynamic Sync Up message for  */
/*                      ETS Counter information which needs to be sent       */
/*                      to Standby Node from the Active Node.                */
/*                                                                           */
/* Input(s)           : pEtsPortInfo - ETS Port Entry                        */
/*                      pMsg - RM Message Structure that needs to be filled  */
/*                      pu4OffSet - OffSet Value                             */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
PUBLIC VOID
DcbxRedFormEtsCounterInfoSyncMsg (tETSPortEntry * pEtsPortInfo,
                                  tRmMsg * pMsg, UINT4 *pu4OffSet,
                                  UINT1 u1CounterType)
{
    UINT2               u2MsgLen = DCBX_ZERO;

    u2MsgLen = DCBX_RED_TYPE_FIELD_SIZE + DCBX_RED_LEN_FIELD_SIZE +
        DCBX_RED_ETS_COUNTER_INFO_VALUE_SIZE;
    DCBX_RM_PUT_1_BYTE (pMsg, pu4OffSet, DCBX_RED_ETS_COUNTER_INFO);
    DCBX_RM_PUT_2_BYTE (pMsg, pu4OffSet, u2MsgLen);
    DCBX_RM_PUT_1_BYTE (pMsg, pu4OffSet, u1CounterType);
    DCBX_RM_PUT_4_BYTE (pMsg, pu4OffSet, pEtsPortInfo->u4IfIndex);
    if (u1CounterType == DCBX_RED_ETS_CONF_TX_COUNTER)
    {
        DCBX_RM_PUT_4_BYTE (pMsg, pu4OffSet, pEtsPortInfo->u4ETSConfTxTLVCount);
    }
    if (u1CounterType == DCBX_RED_ETS_CONF_RX_COUNTER)
    {
        DCBX_RM_PUT_4_BYTE (pMsg, pu4OffSet, pEtsPortInfo->u4ETSConfRxTLVCount);
    }
    if (u1CounterType == DCBX_RED_ETS_CONF_ERR_COUNTER)
    {
        DCBX_RM_PUT_4_BYTE (pMsg, pu4OffSet, pEtsPortInfo->u4ETSConfRxTLVError);
    }
    if (u1CounterType == DCBX_RED_ETS_RECO_TX_COUNTER)
    {
        DCBX_RM_PUT_4_BYTE (pMsg, pu4OffSet, pEtsPortInfo->u4ETSRecoTxTLVCount);
    }
    if (u1CounterType == DCBX_RED_ETS_RECO_RX_COUNTER)
    {
        DCBX_RM_PUT_4_BYTE (pMsg, pu4OffSet, pEtsPortInfo->u4ETSRecoRxTLVCount);
    }
    if (u1CounterType == DCBX_RED_ETS_RECO_ERR_COUNTER)
    {
        DCBX_RM_PUT_4_BYTE (pMsg, pu4OffSet, pEtsPortInfo->u4ETSRecoRxTLVError);
    }
    if (u1CounterType == DCBX_RED_ETS_TC_SUPP_TX_COUNTER)
    {
        DCBX_RM_PUT_4_BYTE (pMsg, pu4OffSet,
                            pEtsPortInfo->u4ETSTcSuppTxTLVCount);
    }
    if (u1CounterType == DCBX_RED_ETS_TC_SUPP_RX_COUNTER)
    {
        DCBX_RM_PUT_4_BYTE (pMsg, pu4OffSet,
                            pEtsPortInfo->u4ETSTcSuppRxTLVCount);
    }
    if (u1CounterType == DCBX_RED_ETS_TC_SUPP_ERR_COUNTER)
    {
        DCBX_RM_PUT_4_BYTE (pMsg, pu4OffSet,
                            pEtsPortInfo->u4ETSTcSupRxTLVError);
    }
    return;
}

/*****************************************************************************/
/* Function Name      : DcbxRedFormEtsAllCounterInfoSyncMsg                  */
/*                                                                           */
/* Description        : This function forms the Dynamic Sync Up message for  */
/*                      ETS Counter information which needs to be sent to    */
/*                      Standby Node from the Active Node.                   */
/*                                                                           */
/* Input(s)           : pEtsPortInfo - ETS Port Entry                        */
/*                      pMsg - RM Message Structure that needs to be filled  */
/*                      pu4OffSet - OffSet Value                             */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
PUBLIC VOID
DcbxRedFormEtsAllCounterInfoSyncMsg (tETSPortEntry * pEtsPortInfo,
                                     tRmMsg * pMsg, UINT4 *pu4OffSet)
{
    UINT2               u2MsgLen = DCBX_ZERO;

    u2MsgLen = DCBX_RED_TYPE_FIELD_SIZE + DCBX_RED_LEN_FIELD_SIZE +
        DCBX_RED_ETS_ALL_COUNTER_INFO_VALUE_SIZE;

    DCBX_RM_PUT_1_BYTE (pMsg, pu4OffSet, DCBX_RED_ETS_ALL_COUNTER_INFO);
    DCBX_RM_PUT_2_BYTE (pMsg, pu4OffSet, u2MsgLen);
    DCBX_RM_PUT_4_BYTE (pMsg, pu4OffSet, pEtsPortInfo->u4IfIndex);
    DCBX_RM_PUT_4_BYTE (pMsg, pu4OffSet, pEtsPortInfo->u4ETSConfTxTLVCount);
    DCBX_RM_PUT_4_BYTE (pMsg, pu4OffSet, pEtsPortInfo->u4ETSConfRxTLVCount);
    DCBX_RM_PUT_4_BYTE (pMsg, pu4OffSet, pEtsPortInfo->u4ETSConfRxTLVError);
    DCBX_RM_PUT_4_BYTE (pMsg, pu4OffSet, pEtsPortInfo->u4ETSRecoTxTLVCount);
    DCBX_RM_PUT_4_BYTE (pMsg, pu4OffSet, pEtsPortInfo->u4ETSRecoRxTLVCount);
    DCBX_RM_PUT_4_BYTE (pMsg, pu4OffSet, pEtsPortInfo->u4ETSRecoRxTLVError);
    DCBX_RM_PUT_4_BYTE (pMsg, pu4OffSet, pEtsPortInfo->u4ETSTcSuppTxTLVCount);
    DCBX_RM_PUT_4_BYTE (pMsg, pu4OffSet, pEtsPortInfo->u4ETSTcSuppRxTLVCount);
    DCBX_RM_PUT_4_BYTE (pMsg, pu4OffSet, pEtsPortInfo->u4ETSTcSupRxTLVError);

    return;
}

/*****************************************************************************/
/* Function Name      : DcbxRedBulkSyncPfcRemInfo                            */
/*                                                                           */
/* Description        : This function sends the PFC Remote table             */
/*                      Bulk Sync up Message to Stadnby Node from            */
/*                      the Active Node.                                     */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
PUBLIC VOID
DcbxRedBulkSyncPfcRemInfo (VOID)
{
    tRmMsg             *pMsg = NULL;
    tPFCPortEntry      *pPfcPortInfo = NULL;
    tPFCPortEntry       PFCPortEntry;
    UINT4               u4OffSet = DCBX_ZERO;


    pMsg = RM_ALLOC_TX_BUF (DCBX_DB_MAX_BUF_SIZE);

    if (pMsg == NULL)
    {
        DCBX_TRC (DCBX_RED_TRC | DCBX_FAILURE_TRC,
                  "DcbxRedBulkSyncPfcRemInfo: Allocation "
                  "of memory from RM  Failed\n");
        return;
    }
    pPfcPortInfo = (tPFCPortEntry *)
        RBTreeGetFirst (gPFCGlobalInfo.pRbPFCPortTbl);
    if (pPfcPortInfo == NULL)
    {
        DCBX_TRC (DCBX_RED_TRC | DCBX_CONTROL_PLANE_TRC,
                  "DcbxRedBulkSyncPfcRemInfo: No PFC entry" "is present \n");
        RM_FREE (pMsg);
        return;
    }
    while (pPfcPortInfo != NULL)
    {
        DcbxRedFormPfcRemInfoSyncMsg (pPfcPortInfo, pMsg, &u4OffSet);

        if ((DCBX_DB_MAX_BUF_SIZE - u4OffSet) < DCBX_RED_PFC_REM_INFO_SIZE)
        {

            if (DcbxPortRmEnqMsgToRm (pMsg, (UINT2) u4OffSet,
                                      RM_DCBX_APP_ID,
                                      RM_DCBX_APP_ID) == OSIX_FAILURE)
            {
                DCBX_TRC (DCBX_RED_TRC | DCBX_FAILURE_TRC,
                          "DcbxRedBulkSyncPfcRemInfo: Enqueue "
                          "message to RM Failed\r\n");
                return;
            }

            u4OffSet = DCBX_ZERO;
            pMsg = RM_ALLOC_TX_BUF (DCBX_DB_MAX_BUF_SIZE);

            if (pMsg == NULL)
            {
                DCBX_TRC (DCBX_RED_TRC | DCBX_FAILURE_TRC,
                          "DcbxRedBulkSyncPfcRemInfo: Allocation "
                          "of memory from RM  Failed\n");
                return;
            }
        }
        MEMSET (&PFCPortEntry, DCBX_ZERO, sizeof (tPFCPortEntry));
        PFCPortEntry.u4IfIndex = pPfcPortInfo->u4IfIndex;
        pPfcPortInfo = (tPFCPortEntry *)
            RBTreeGetNext (gPFCGlobalInfo.pRbPFCPortTbl,
                           &PFCPortEntry, DcbxUtlPortTblCmpFn);
    }

    DcbxPortRmEnqMsgToRm (pMsg, (UINT2) u4OffSet, RM_DCBX_APP_ID,
                          RM_DCBX_APP_ID);
    return;
}

/*****************************************************************************/
/* Function Name      : DcbxRedBulkSyncPfcSemInfo                            */
/*                                                                           */
/* Description        : This function sends the PFC SEM Info                 */
/*                      Bulk Sync up Message to Stadnby Node from            */
/*                      the Active Node.                                     */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
PUBLIC VOID
DcbxRedBulkSyncPfcSemInfo (VOID)
{
    tRmMsg             *pMsg = NULL;
    tPFCPortEntry      *pPfcPortInfo = NULL;
    tPFCPortEntry       PFCPortEntry;
    UINT4               u4OffSet = DCBX_ZERO;


    pMsg = RM_ALLOC_TX_BUF (DCBX_DB_MAX_BUF_SIZE);

    if (pMsg == NULL)
    {
        DCBX_TRC (DCBX_RED_TRC | DCBX_FAILURE_TRC,
                  "DcbxRedBulkSyncPfcSemInfo: Allocation "
                  "of memory from RM  Failed\n");
        return;
    }
    pPfcPortInfo = (tPFCPortEntry *)
        RBTreeGetFirst (gPFCGlobalInfo.pRbPFCPortTbl);
    if (pPfcPortInfo == NULL)
    {
        DCBX_TRC (DCBX_RED_TRC | DCBX_CONTROL_PLANE_TRC,
                  "DcbxRedBulkSyncPfcSemInfo: No PFC entry" "is present \n");
        RM_FREE (pMsg);
        return;
    }
    while (pPfcPortInfo != NULL)
    {
        DcbxRedFormPfcSemInfoSyncMsg (pPfcPortInfo, pMsg, &u4OffSet);

        if ((DCBX_DB_MAX_BUF_SIZE - u4OffSet) < DCBX_RED_PFC_SEM_INFO_SIZE)
        {

            if (DcbxPortRmEnqMsgToRm (pMsg, (UINT2) u4OffSet,
                                      RM_DCBX_APP_ID,
                                      RM_DCBX_APP_ID) == OSIX_FAILURE)
            {
                DCBX_TRC (DCBX_RED_TRC | DCBX_FAILURE_TRC,
                          "DcbxRedBulkSyncPfcSemInfo: Enqueue "
                          "message to RM Failed\r\n");
                return;
            }

            u4OffSet = DCBX_ZERO;
            pMsg = RM_ALLOC_TX_BUF (DCBX_DB_MAX_BUF_SIZE);

            if (pMsg == NULL)
            {
                DCBX_TRC (DCBX_RED_TRC | DCBX_FAILURE_TRC,
                          "DcbxRedBulkSyncPfcSemInfo: Allocation "
                          "of memory from RM  Failed\n");
                return;
            }
        }
        MEMSET (&PFCPortEntry, DCBX_ZERO, sizeof (tPFCPortEntry));
        PFCPortEntry.u4IfIndex = pPfcPortInfo->u4IfIndex;
        pPfcPortInfo = (tPFCPortEntry *)
            RBTreeGetNext (gPFCGlobalInfo.pRbPFCPortTbl,
                           &PFCPortEntry, DcbxUtlPortTblCmpFn);
    }

    DcbxPortRmEnqMsgToRm (pMsg, (UINT2) u4OffSet, RM_DCBX_APP_ID,
                          RM_DCBX_APP_ID);
    return;
}

/*****************************************************************************/
/* Function Name      : DcbxRedBulkSyncPfcCounterInfo                        */
/*                                                                           */
/* Description        : This function sends the PFC Counters Info            */
/*                      Bulk Sync up Message to Stadnby Node from            */
/*                      the Active Node.                                     */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
PUBLIC VOID
DcbxRedBulkSyncPfcCounterInfo (VOID)
{
    tRmMsg             *pMsg = NULL;
    tPFCPortEntry      *pPfcPortInfo = NULL;
    tPFCPortEntry       PFCPortEntry;
    UINT4               u4OffSet = DCBX_ZERO;


    pMsg = RM_ALLOC_TX_BUF (DCBX_DB_MAX_BUF_SIZE);

    if (pMsg == NULL)
    {
        DCBX_TRC (DCBX_RED_TRC | DCBX_FAILURE_TRC,
                  "DcbxRedBulkSyncPfcCounterInfo: Allocation "
                  "of memory from RM Failed\n");
        return;
    }
    pPfcPortInfo = (tPFCPortEntry *)
        RBTreeGetFirst (gPFCGlobalInfo.pRbPFCPortTbl);
    if (pPfcPortInfo == NULL)
    {
        DCBX_TRC (DCBX_RED_TRC | DCBX_CONTROL_PLANE_TRC,
                  "DcbxRedBulkSyncPfcCounterInfo: No PFC entry"
                  "is present \n");
        RM_FREE (pMsg);
        return;
    }
    while (pPfcPortInfo != NULL)
    {
        DcbxRedFormPfcAllCounterInfoSyncMsg (pPfcPortInfo, pMsg, &u4OffSet);

        if ((DCBX_DB_MAX_BUF_SIZE - u4OffSet) <
            DCBX_RED_PFC_ALL_COUNTER_INFO_SIZE)
        {

            if (DcbxPortRmEnqMsgToRm (pMsg, (UINT2) u4OffSet,
                                      RM_DCBX_APP_ID,
                                      RM_DCBX_APP_ID) == OSIX_FAILURE)
            {

                DCBX_TRC (DCBX_RED_TRC | DCBX_FAILURE_TRC,
                          "DcbxRedBulkSyncPfcCounterInfo: Enqueue "
                          "message to RM Failed\r\n");
                return;
            }
            u4OffSet = DCBX_ZERO;
            pMsg = RM_ALLOC_TX_BUF (DCBX_DB_MAX_BUF_SIZE);

            if (pMsg == NULL)
            {
                DCBX_TRC (DCBX_RED_TRC | DCBX_FAILURE_TRC,
                          "DcbxRedBulkSyncPfcCounterInfo: Allocation "
                          "of memory from RM Failed\n");
                return;
            }
        }
        MEMSET (&PFCPortEntry, DCBX_ZERO, sizeof (tPFCPortEntry));
        PFCPortEntry.u4IfIndex = pPfcPortInfo->u4IfIndex;
        pPfcPortInfo = (tPFCPortEntry *)
            RBTreeGetNext (gPFCGlobalInfo.pRbPFCPortTbl,
                           &PFCPortEntry, DcbxUtlPortTblCmpFn);
    }

    DcbxPortRmEnqMsgToRm (pMsg, (UINT2) u4OffSet,
                          RM_DCBX_APP_ID, RM_DCBX_APP_ID);
    return;
}

/*****************************************************************************/
/* Function Name      : DcbxRedSendDynamicPfcRemInfo                         */
/*                                                                           */
/* Description        : This function sends the PFC Remote table             */
/*                      Dynamic Sync up Message to Stadnby Node from         */
/*                      the Active Node.                                     */
/*                                                                           */
/* Input(s)           : pPfcPortInfo - PFC Port Entry                        */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
PUBLIC VOID
DcbxRedSendDynamicPfcRemInfo (tPFCPortEntry * pPfcPortInfo)
{
    tRmMsg             *pMsg = NULL;
    UINT4               u4OffSet = DCBX_ZERO;


    if ((DCBX_RED_NODE_STATUS() == RM_STANDBY) ||
        (DCBX_RED_STBY_NODE_UP_COUNT() == DCBX_ZERO))
    {
        DCBX_TRC (DCBX_RED_TRC | DCBX_CONTROL_PLANE_TRC,
                  "DcbxRedSendDynamicPfcRemInfo: No Need to send Sync"
                  "Message as Standby is down or the node is Active\n");
        return;
    }
    pMsg = RM_ALLOC_TX_BUF (DCBX_RED_PFC_REM_INFO_SIZE);

    if (pMsg == NULL)
    {
        DCBX_TRC (DCBX_RED_TRC | DCBX_FAILURE_TRC,
                  "DcbxRedSendDynamicPfcRemInfo: Allocation "
                  "of memory from RM Failed\n");
        return;
    }
    DcbxRedFormPfcRemInfoSyncMsg (pPfcPortInfo, pMsg, &u4OffSet);
    DcbxPortRmEnqMsgToRm (pMsg, (UINT2) u4OffSet,
                          RM_DCBX_APP_ID, RM_DCBX_APP_ID);
    return;
}

/*****************************************************************************/
/* Function Name      : DcbxRedSendDynamicPfcSemInfo                         */
/*                                                                           */
/* Description        : This function sends the PFC SEM Information          */
/*                      Dynamic Sync up Message to Stadnby Node from         */
/*                      the Active Node.                                     */
/*                                                                           */
/* Input(s)           : pPfcPortInfo - PFC Port Entry                        */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
PUBLIC VOID
DcbxRedSendDynamicPfcSemInfo (tPFCPortEntry * pPfcPortInfo)
{
    tRmMsg             *pMsg = NULL;
    UINT4               u4OffSet = DCBX_ZERO;


    if ((DCBX_RED_NODE_STATUS() == RM_STANDBY) ||
        (DCBX_RED_STBY_NODE_UP_COUNT() == DCBX_ZERO))
    {
        DCBX_TRC (DCBX_RED_TRC | DCBX_CONTROL_PLANE_TRC,
                  "DcbxRedSendDynamicPfcSemInfo: No Need to send Sync"
                  "Message as Standby is down or the node is Active\n");
        return;
    }
    pMsg = RM_ALLOC_TX_BUF (DCBX_RED_PFC_SEM_INFO_SIZE);

    if (pMsg == NULL)
    {
        DCBX_TRC (DCBX_RED_TRC | DCBX_FAILURE_TRC,
                  "DcbxRedSendDynamicPfcSemInfo: Allocation "
                  "of memory from RM Failed\n");
        return;
    }
    DcbxRedFormPfcSemInfoSyncMsg (pPfcPortInfo, pMsg, &u4OffSet);
    DcbxPortRmEnqMsgToRm (pMsg, (UINT2) u4OffSet,
                          RM_DCBX_APP_ID, RM_DCBX_APP_ID);
    return;
}

/*****************************************************************************/
/* Function Name      : DcbxRedSendDynamicPfcCounterInfo                      */
/*                                                                           */
/* Description        : This function sends the PFC Counter information      */
/*                      Dynamic Sync up Message to Stadnby Node from         */
/*                      the Active Node.                                     */
/*                                                                           */
/* Input(s)           : pPfcPortInfo - PFC Port Entry                        */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
PUBLIC VOID
DcbxRedSendDynamicPfcCounterInfo (tPFCPortEntry * pPfcPortInfo,
                                  UINT1 u1CounterType)
{
    tRmMsg             *pMsg = NULL;
    UINT4               u4OffSet = DCBX_ZERO;


    if ((DCBX_RED_NODE_STATUS() == RM_STANDBY) ||
        (DCBX_RED_STBY_NODE_UP_COUNT() == DCBX_ZERO))
    {
        DCBX_TRC (DCBX_RED_TRC | DCBX_CONTROL_PLANE_TRC,
                  "DcbxRedSendDynamicPfcCounterInfo: No Need to send Sync"
                  "Message as Standby is down or the node is Active\n");
        return;
    }
    pMsg = RM_ALLOC_TX_BUF (DCBX_RED_PFC_COUNTER_INFO_SIZE);

    if (pMsg == NULL)
    {
        DCBX_TRC (DCBX_RED_TRC | DCBX_FAILURE_TRC,
                  "DcbxRedSendDynamicPfcCounterInfo: Allocation "
                  "of memory from RM Failed\n");
        return;
    }
    DcbxRedFormPfcCounterInfoSyncMsg (pPfcPortInfo, pMsg, &u4OffSet,
                                      u1CounterType);
    DcbxPortRmEnqMsgToRm (pMsg, (UINT2) u4OffSet,
                          RM_DCBX_APP_ID, RM_DCBX_APP_ID);
    return;
}

/*****************************************************************************/
/* Function Name      : DcbxRedFormPfcRemInfoSyncMsg                         */
/*                                                                           */
/* Description        : This function forms the Dynamic Sync Up message for  */
/*                      PFC Remote table which needs to be sent to Standby   */
/*                      Node from the Active Node.                           */
/*                                                                           */
/* Input(s)           : pPfcPortInfo - PFC Port Entry                        */
/*                      pMsg - RM Message Structure that needs to be filled  */
/*                      pu4OffSet - OffSet Value                             */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
PUBLIC VOID
DcbxRedFormPfcRemInfoSyncMsg (tPFCPortEntry * pPfcPortInfo,
                              tRmMsg * pMsg, UINT4 *pu4OffSet)
{
    UINT2               u2MsgLen = DCBX_ZERO;

    u2MsgLen = DCBX_RED_TYPE_FIELD_SIZE + DCBX_RED_LEN_FIELD_SIZE +
        DCBX_RED_PFC_REM_INFO_VALUE_SIZE;

    DCBX_RM_PUT_1_BYTE (pMsg, pu4OffSet, DCBX_RED_PFC_REM_INFO);
    DCBX_RM_PUT_2_BYTE (pMsg, pu4OffSet, u2MsgLen);
    DCBX_RM_PUT_4_BYTE (pMsg, pu4OffSet, pPfcPortInfo->u4IfIndex);
    DCBX_RM_PUT_4_BYTE (pMsg, pu4OffSet,
                        pPfcPortInfo->PFCRemPortInfo.u4PFCRemTimeMark);
    DCBX_RM_PUT_4_BYTE (pMsg, pu4OffSet,
                        pPfcPortInfo->PFCRemPortInfo.i4PFCRemIndex);
    DCBX_RM_PUT_N_BYTE (pMsg,
                        pPfcPortInfo->PFCRemPortInfo.au1PFCDestRemMacAddr,
                        pu4OffSet, MAC_ADDR_LEN);
    DCBX_RM_PUT_1_BYTE (pMsg, pu4OffSet,
                        pPfcPortInfo->PFCRemPortInfo.u1PFCRemWilling);
    DCBX_RM_PUT_1_BYTE (pMsg, pu4OffSet,
                        pPfcPortInfo->PFCRemPortInfo.u1PFCRemCap);
    DCBX_RM_PUT_1_BYTE (pMsg, pu4OffSet,
                        pPfcPortInfo->PFCRemPortInfo.u1PFCRemMBC);
    DCBX_RM_PUT_1_BYTE (pMsg, pu4OffSet,
                        pPfcPortInfo->PFCRemPortInfo.u1PFCRemStatus);

    return;
}

/*****************************************************************************/
/* Function Name      : DcbxRedFormPfcSemInfoSyncMsg                         */
/*                                                                           */
/* Description        : This function forms the Dynamic Sync Up message for  */
/*                      PFC SEM information which needs to be sent to Standby*/
/*                      Node from the Active Node.                           */
/*                                                                           */
/* Input(s)           : pPfcPortInfo - PFC Port Entry                        */
/*                      pMsg - RM Message Structure that needs to be filled  */
/*                      pu4OffSet - OffSet Value                             */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
PUBLIC VOID
DcbxRedFormPfcSemInfoSyncMsg (tPFCPortEntry * pPfcPortInfo,
                              tRmMsg * pMsg, UINT4 *pu4OffSet)
{
    UINT2               u2MsgLen = DCBX_ZERO;

    u2MsgLen = DCBX_RED_TYPE_FIELD_SIZE + DCBX_RED_LEN_FIELD_SIZE +
        DCBX_RED_PFC_SEM_INFO_VALUE_SIZE;

    DCBX_TRC_ARG3 (DCBX_CONTROL_PLANE_TRC, "DcbxRedFormPfcSemInfoSyncMsg:"
                   "MsgLen:%d IfIndex:%d OperState:%d\r\n", u2MsgLen,
                   pPfcPortInfo->u4IfIndex, pPfcPortInfo->u1PFCDcbxOperState);
    DCBX_RM_PUT_1_BYTE (pMsg, pu4OffSet, DCBX_RED_PFC_SEM_INFO);
    DCBX_RM_PUT_2_BYTE (pMsg, pu4OffSet, u2MsgLen);
    DCBX_RM_PUT_4_BYTE (pMsg, pu4OffSet, pPfcPortInfo->u4IfIndex);
    DCBX_RM_PUT_1_BYTE (pMsg, pu4OffSet, pPfcPortInfo->u1PFCDcbxOperState);

    return;
}

/*****************************************************************************/
/* Function Name      : DcbxRedFormPfcCounterInfoSyncMsg                     */
/*                                                                           */
/* Description        : This function forms the Dynamic Sync Up message for  */
/*                      PFC Counter information which needs to be sent       */
/*                      to Standby Node from the Active Node.                */
/*                                                                           */
/* Input(s)           : pPfcPortInfo - PFC Port Entry                        */
/*                      pMsg - RM Message Structure that needs to be filled  */
/*                      pu4OffSet - OffSet Value                             */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
PUBLIC VOID
DcbxRedFormPfcCounterInfoSyncMsg (tPFCPortEntry * pPfcPortInfo,
                                  tRmMsg * pMsg, UINT4 *pu4OffSet,
                                  UINT1 u1CounterType)
{
    UINT2               u2MsgLen = DCBX_ZERO;

    u2MsgLen = DCBX_RED_TYPE_FIELD_SIZE + DCBX_RED_LEN_FIELD_SIZE +
        DCBX_RED_PFC_COUNTER_INFO_VALUE_SIZE;
    DCBX_RM_PUT_1_BYTE (pMsg, pu4OffSet, DCBX_RED_PFC_COUNTER_INFO);
    DCBX_RM_PUT_2_BYTE (pMsg, pu4OffSet, u2MsgLen);
    DCBX_RM_PUT_1_BYTE (pMsg, pu4OffSet, u1CounterType);
    DCBX_RM_PUT_4_BYTE (pMsg, pu4OffSet, pPfcPortInfo->u4IfIndex);

    if (u1CounterType == DCBX_RED_PFC_TX_COUNTER)
    {
        DCBX_RM_PUT_4_BYTE (pMsg, pu4OffSet, pPfcPortInfo->u4PFCTxTLVCount);
    }
    if (u1CounterType == DCBX_RED_PFC_RX_COUNTER)
    {
        DCBX_RM_PUT_4_BYTE (pMsg, pu4OffSet, pPfcPortInfo->u4PFCRxTLVCount);
    }
    if (u1CounterType == DCBX_RED_PFC_ERR_COUNTER)
    {
        DCBX_RM_PUT_4_BYTE (pMsg, pu4OffSet, pPfcPortInfo->u4PFCRxTLVError);
    }
    return;
}

/*****************************************************************************/
/* Function Name      : DcbxRedFormPfcAllCounterInfoSyncMsg                  */
/*                                                                           */
/* Description        : This function forms the Dynamic Sync Up message for  */
/*                      PFC Counter information which needs to be sent to    */
/*                      Standby Node from the Active Node.                   */
/*                                                                           */
/* Input(s)           : pPfcPortInfo - Pfc Port Entry                        */
/*                      pMsg - RM Message Structure that needs to be filled  */
/*                      pu4OffSet - OffSet Value                             */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
PUBLIC VOID
DcbxRedFormPfcAllCounterInfoSyncMsg (tPFCPortEntry * pPfcPortInfo,
                                     tRmMsg * pMsg, UINT4 *pu4OffSet)
{
    UINT2               u2MsgLen = DCBX_ZERO;

    u2MsgLen = DCBX_RED_TYPE_FIELD_SIZE + DCBX_RED_LEN_FIELD_SIZE +
        DCBX_RED_PFC_ALL_COUNTER_INFO_VALUE_SIZE;

    DCBX_RM_PUT_1_BYTE (pMsg, pu4OffSet, DCBX_RED_PFC_ALL_COUNTER_INFO);
    DCBX_RM_PUT_2_BYTE (pMsg, pu4OffSet, u2MsgLen);
    DCBX_RM_PUT_4_BYTE (pMsg, pu4OffSet, pPfcPortInfo->u4IfIndex);
    DCBX_RM_PUT_4_BYTE (pMsg, pu4OffSet, pPfcPortInfo->u4PFCTxTLVCount);
    DCBX_RM_PUT_4_BYTE (pMsg, pu4OffSet, pPfcPortInfo->u4PFCRxTLVCount);
    DCBX_RM_PUT_4_BYTE (pMsg, pu4OffSet, pPfcPortInfo->u4PFCRxTLVError);
    return;
}

/***************************************************************************
 * FUNCTION NAME    : DcbxRedProcessDynamicAppPriAppRemInfo
 *
 * DESCRIPTION      : This routine decodes the sync-up message from peer active
 *                    DCBX and updates Application Priority Reomte 
 *                    Application Priority Maping entries 
 *                    in standby DCBX.
 *
 * INPUT            : pMsg - RM Sync-up message
 *                    u2RemMsgLen - Length of value of Message
 *                    
 * OUTPUT           : u4OffSet - Offset value 
 * 
 * RETURNS          : VOID 
 * 
 **************************************************************************/
PUBLIC VOID
DcbxRedProcessDynamicAppPriRemInfo (tRmMsg * pMsg, UINT4 *pu4OffSet,
                                    UINT2 u2RemMsgLen)
{
    tAppPriPortEntry   *pAppPriPortInfo = NULL;
    tRmProtoEvt         ProtoEvt;
    tAppPriMappingEntry *pAppPriEntry = NULL;
    UINT4               u4IfIndex = DCBX_ZERO;
    UINT4               u4Count = DCBX_ZERO;
    UINT4               u4Protocol = DCBX_ZERO;
    UINT1               u1Selector = DCBX_ZERO;
    UINT1               u1Priority = DCBX_ZERO;
    UINT4               u4PriIndex= DCBX_ZERO;

    MEMSET (&ProtoEvt, DCBX_ZERO, sizeof (tRmProtoEvt));

    ProtoEvt.u4AppId = RM_DCBX_APP_ID;
    ProtoEvt.u4Event = RM_BULK_UPDT_ABORT;

    if (u2RemMsgLen < DCBX_RED_APP_PRI_REM_INFO_VALUE_SIZE)
    {
        ProtoEvt.u4Error = RM_PROCESS_FAIL;
        DcbxPortRmApiHandleProtocolEvent (&ProtoEvt);
        *pu4OffSet += u2RemMsgLen;
        return;
    }

    DCBX_RM_GET_4_BYTE (pMsg, pu4OffSet, u4IfIndex);

    pAppPriPortInfo = AppPriUtlGetPortEntry (u4IfIndex);

    if (pAppPriPortInfo == NULL)
    {
        DCBX_TRC_ARG2 ((DCBX_RED_TRC | DCBX_FAILURE_TRC),
                "In %s: No Application Entry present for port %d \n",
                __FUNCTION__, u4IfIndex);
        return;
    }

    DCBX_RM_GET_4_BYTE (pMsg, pu4OffSet,
                        pAppPriPortInfo->AppPriRemPortInfo.u4AppPriRemTimeMark);
    
    DCBX_RM_GET_4_BYTE (pMsg, pu4OffSet, u4PriIndex);
    pAppPriPortInfo->AppPriRemPortInfo.i4AppPriRemIndex = (INT4)u4PriIndex;
    
    DCBX_RM_GET_N_BYTE (pMsg,
                        pAppPriPortInfo->AppPriRemPortInfo.
                        au1AppPriDestRemMacAddr, pu4OffSet, MAC_ADDR_LEN);
    DCBX_RM_GET_1_BYTE (pMsg, pu4OffSet,
                        pAppPriPortInfo->AppPriRemPortInfo.u1AppPriRemWilling);

    /* u4Count will determine the number of Application to Priority Mapping
     * entries  in the remote table */

    DCBX_TRC_ARG5 (DCBX_RED_TRC,
                "%s: port: %d RemTM: %d, RemIdx: %d, Willing: %d \n", __func__,
                pAppPriPortInfo->u4IfIndex,
                pAppPriPortInfo->AppPriRemPortInfo.u4AppPriRemTimeMark,
                pAppPriPortInfo->AppPriRemPortInfo.i4AppPriRemIndex,
                pAppPriPortInfo->AppPriRemPortInfo.u1AppPriRemWilling);

    DCBX_RM_GET_4_BYTE (pMsg, pu4OffSet, u4Count);

    /* Delete the remote entries currently present and update with the
     * remote table entries received from ACTIVE node*/
    AppPriUtlDeleteRemoteMappingEntries (pAppPriPortInfo->u4IfIndex);

    if (u2RemMsgLen != (UINT2) (DCBX_RED_APP_PRI_REM_INFO_VALUE_SIZE + 
                (u4Count * APP_PRI_TABLE_ENTRY_LEN)))
    {
        ProtoEvt.u4Error = RM_PROCESS_FAIL;
        DcbxPortRmApiHandleProtocolEvent (&ProtoEvt);
        *pu4OffSet += u2RemMsgLen;

        DCBX_TRC_ARG2 ((DCBX_RED_TRC | DCBX_FAILURE_TRC),
                       "In %s: Length received: [%d] is wrong. Return. \n",
                       __FUNCTION__, u2RemMsgLen);
        return;
    }

    /* Retrieve the remote Application to Priority Mapping entries 
     * from active node and create those entries in stand by */
    while (u4Count > DCBX_ZERO)
    {
        DCBX_RM_GET_1_BYTE (pMsg, pu4OffSet, u1Selector);
        DCBX_RM_GET_4_BYTE (pMsg, pu4OffSet, u4Protocol);
        DCBX_RM_GET_1_BYTE (pMsg, pu4OffSet, u1Priority);

        /* Print the entries getting added to the list */
        DCBX_TRC_ARG6 (DCBX_CRITICAL_TRC,
                "%s: NoOfTblEntries: %d "
                "Create entry with Selector: %d, Protocol: %d u1Priority: %d Port: %d\n",
                __FUNCTION__, u4Count, u1Selector, u4Protocol, u1Priority,
                pAppPriPortInfo->u4IfIndex);

        if ((u1Selector >= DCBX_ONE) && (u1Selector <= APP_PRI_MAX_SELECTOR))
        {
            pAppPriEntry = AppPriUtlCreateAppPriMappingTblEntry
                (u4IfIndex, (INT4) u1Selector, (INT4) u4Protocol, APP_PRI_REMOTE);
            if (pAppPriEntry == NULL)
            {
                DCBX_TRC_ARG1 ((DCBX_RED_TRC | DCBX_FAILURE_TRC),
                        "In %s: "
                        "AppPriUtlCreateAppPriMappingTblEntry "
                        "returned NULL \n", __FUNCTION__);
                return;
            }
            pAppPriEntry->u1AppPriPriority = u1Priority;
        }
        u4Count--;
    }

    return;
}

/***************************************************************************
 * FUNCTION NAME    : DcbxRedProcessDynamicAppPriSemInfo
 *
 * DESCRIPTION      : This routine decodes the sync-up message from peer active
 *                    DCBX and updates the Application Priority SEM
 *                    information in standby DCBX.
 *
 * INPUT            : pMsg - RM Sync-up message
 *                    u2RemMsgLen - Length of value of Message
 *                    
 * OUTPUT           : u4OffSet - Offset value 
 * 
 * RETURNS          : VOID 
 * 
 **************************************************************************/
PUBLIC VOID
DcbxRedProcessDynamicAppPriSemInfo (tRmMsg * pMsg, UINT4 *pu4OffSet,
                                    UINT2 u2RemMsgLen)
{
    tAppPriPortEntry   *pAppPriPortInfo = NULL;
    tRmProtoEvt         ProtoEvt;
    UINT4               u4IfIndex = DCBX_ZERO;
    UINT1               u1OperState = DCBX_ZERO;


    MEMSET (&ProtoEvt, DCBX_ZERO, sizeof (tRmProtoEvt));

    ProtoEvt.u4AppId = RM_DCBX_APP_ID;
    ProtoEvt.u4Event = RM_BULK_UPDT_ABORT;

    if (u2RemMsgLen != DCBX_RED_APP_PRI_SEM_INFO_VALUE_SIZE)
    {
        ProtoEvt.u4Error = RM_PROCESS_FAIL;

        DcbxPortRmApiHandleProtocolEvent (&ProtoEvt);
        *pu4OffSet += u2RemMsgLen;
        return;
    }

    DCBX_RM_GET_4_BYTE (pMsg, pu4OffSet, u4IfIndex);

    pAppPriPortInfo = AppPriUtlGetPortEntry (u4IfIndex);

    if (pAppPriPortInfo == NULL)
    {
        DCBX_TRC_ARG1 ((DCBX_RED_TRC | DCBX_FAILURE_TRC),
                       "In %s: No AppPriPort Entry Found \n", __FUNCTION__);
        return;
    }
    DCBX_RM_GET_1_BYTE (pMsg, pu4OffSet, u1OperState);

    DCBX_TRC_ARG3 (DCBX_CONTROL_PLANE_TRC, "IN %s:"
                   " IfIndex = %d OperStatae = %d\n", __FUNCTION__,
                   u4IfIndex, u1OperState);

    /* Update Operational parameters accordingly */
    AppPriUtlUpdateLocalParam (pAppPriPortInfo, u1OperState);

    return;
}

/***************************************************************************
 * FUNCTION NAME    : DcbxRedProcessDynamicAppPriAllCounterInfo
 *
 * DESCRIPTION      : This routine decodes the sync-up message from peer active
 *                    DCBX and updates Application Priority Counter
 *                    information in standby DCBX.
 *                    This will be used in Dynamic Bulk Update.
 *
 * INPUT            : pMsg - RM Sync-up message
 *                    u2RemMsgLen - Length of value of Message
 *                    
 * OUTPUT           : u4OffSet - Offset value 
 * 
 * RETURNS          : VOID 
 * 
 **************************************************************************/
PUBLIC VOID
DcbxRedProcessDynamicAppPriAllCounterInfo (tRmMsg * pMsg, UINT4 *pu4OffSet,
                                           UINT2 u2RemMsgLen)
{
    tAppPriPortEntry   *pAppPriPortInfo = NULL;
    tRmProtoEvt         ProtoEvt;
    UINT4               u4IfIndex = DCBX_ZERO;


    MEMSET (&ProtoEvt, DCBX_ZERO, sizeof (tRmProtoEvt));

    ProtoEvt.u4AppId = RM_DCBX_APP_ID;
    ProtoEvt.u4Event = RM_BULK_UPDT_ABORT;

    /* Validate the RM message received from active node */
    if (u2RemMsgLen != DCBX_RED_APP_PRI_ALL_COUNTER_INFO_VALUE_SIZE)
    {
        ProtoEvt.u4Error = RM_PROCESS_FAIL;
        DcbxPortRmApiHandleProtocolEvent (&ProtoEvt);
        *pu4OffSet += u2RemMsgLen;
        return;
    }

    DCBX_RM_GET_4_BYTE (pMsg, pu4OffSet, u4IfIndex);

    pAppPriPortInfo = AppPriUtlGetPortEntry (u4IfIndex);

    if (pAppPriPortInfo == NULL)
    {
        DCBX_TRC_ARG1 ((DCBX_RED_TRC | DCBX_FAILURE_TRC),
                       "In %s: No Application Priority Port Entry Found \n",
                       __FUNCTION__);
        return;
    }

    /* Get the counter values from active node and update them in stand by 
     * node */
    DCBX_RM_GET_4_BYTE (pMsg, pu4OffSet, pAppPriPortInfo->u4AppPriTLVTxCount);
    DCBX_RM_GET_4_BYTE (pMsg, pu4OffSet, pAppPriPortInfo->u4AppPriTLVRxCount);
    DCBX_RM_GET_4_BYTE (pMsg, pu4OffSet, pAppPriPortInfo->u4AppPriTLVRxError);
    DCBX_RM_GET_4_BYTE (pMsg, pu4OffSet, pAppPriPortInfo->u4AppPriAppProtocols);

    return;
}

/***************************************************************************
 * FUNCTION NAME    : DcbxRedProcessDynamicAppPriCounterInfo
 *
 * DESCRIPTION      : This routine decodes the sync-up message from peer active
 *                    DCBX and updates Application Priority Counter information
 *                    in standby DCBX.
 *
 * INPUT            : pMsg - RM Sync-up message
 *                    u2RemMsgLen - Length of value of Message
 *                    
 * OUTPUT           : u4OffSet - Offset value 
 * 
 * RETURNS          : VOID 
 * 
 **************************************************************************/
PUBLIC VOID
DcbxRedProcessDynamicAppPriCounterInfo (tRmMsg * pMsg, UINT4 *pu4OffSet,
                                        UINT2 u2RemMsgLen)
{
    tAppPriPortEntry   *pAppPriPortInfo = NULL;
    tRmProtoEvt         ProtoEvt;
    UINT4               u4IfIndex = DCBX_ZERO;
    UINT4               u4CounterVal = DCBX_ZERO;
    UINT1               u1CounterType = DCBX_ZERO;


    MEMSET (&ProtoEvt, DCBX_ZERO, sizeof (tRmProtoEvt));

    ProtoEvt.u4AppId = RM_DCBX_APP_ID;
    ProtoEvt.u4Event = RM_BULK_UPDT_ABORT;

    /* Validate the RM message received from active node */
    if (u2RemMsgLen != DCBX_RED_APP_PRI_COUNTER_INFO_VALUE_SIZE)
    {
        ProtoEvt.u4Error = RM_PROCESS_FAIL;

        DcbxPortRmApiHandleProtocolEvent (&ProtoEvt);
        *pu4OffSet += u2RemMsgLen;
        return;
    }

    /* u1CounterType indicates which counter to be updated */
    DCBX_RM_GET_1_BYTE (pMsg, pu4OffSet, u1CounterType);
    DCBX_RM_GET_4_BYTE (pMsg, pu4OffSet, u4IfIndex);

    pAppPriPortInfo = AppPriUtlGetPortEntry (u4IfIndex);
    if (pAppPriPortInfo == NULL)
    {
        DCBX_TRC_ARG2 ((DCBX_RED_TRC | DCBX_FAILURE_TRC),
                       "In %s: No Application Priority Entry Found " 
                       "for port %d\n", __FUNCTION__, u4IfIndex);
        return;
    }

    DCBX_RM_GET_4_BYTE (pMsg, pu4OffSet, u4CounterVal);
    /* Update the appropriate counter value in stand by node */
    if (u1CounterType == DCBX_RED_APP_PRI_TX_COUNTER)
    {
        pAppPriPortInfo->u4AppPriTLVTxCount = u4CounterVal;
        DCBX_TRC_ARG2 ((DCBX_RED_TRC | DCBX_MGMT_TRC),
                       "In %s: APP_PRI_TX_COUNTER = %d \r\n",
                       __FUNCTION__, u4CounterVal);
    }
    if (u1CounterType == DCBX_RED_APP_PRI_RX_COUNTER)
    {
        pAppPriPortInfo->u4AppPriTLVRxCount = u4CounterVal;
        DCBX_TRC_ARG2 ((DCBX_RED_TRC | DCBX_MGMT_TRC),
                       "In %s: APP_PRI_RX_COUNTER = %d \r\n",
                       __FUNCTION__, u4CounterVal);
    }
    if (u1CounterType == DCBX_RED_APP_PRI_ERR_COUNTER)
    {
        pAppPriPortInfo->u4AppPriTLVRxError = u4CounterVal;
        DCBX_TRC_ARG2 ((DCBX_RED_TRC | DCBX_MGMT_TRC),
                       "In %s: APP_PRI_ERR_COUNTER = %d \r\n",
                       __FUNCTION__, u4CounterVal);
    }
    if (u1CounterType == DCBX_RED_APP_PRI_PROTOCOL_COUNTER)
    {
        pAppPriPortInfo->u4AppPriAppProtocols = u4CounterVal;
        DCBX_TRC_ARG2 ((DCBX_RED_TRC | DCBX_MGMT_TRC),
                       "In %s: APP_PRI_PROTOCOLS = %d \r\n",
                       __FUNCTION__, u4CounterVal);
    }

    return;
}

/*****************************************************************************/
/* Function Name      : DcbxRedBulkSyncAppPriRemInfo                         */
/*                                                                           */
/* Description        : This function sends the Application Priority         */
/*                      Remote table                                         */
/*                      Bulk Sync up Message to Stadnby Node from            */
/*                      the Active Node.                                     */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
PUBLIC VOID
DcbxRedBulkSyncAppPriRemInfo (VOID)
{
    tRmMsg             *pMsg = NULL;
    tAppPriPortEntry   *pAppPriPortInfo;
    tAppPriPortEntry    AppPriPortEntry;
    UINT4               u4OffSet = DCBX_ZERO;


    pMsg = RM_ALLOC_TX_BUF (DCBX_DB_MAX_BUF_SIZE);

    if (pMsg == NULL)
    {
        DCBX_TRC_ARG1 ((DCBX_RED_TRC | DCBX_FAILURE_TRC),
                       "In %s: Allocation "
                       "of memory from RM Failed\n", __FUNCTION__);
        return;
    }

    /* send sync up message(s) only if application priority is configured 
     * on atleast one port */

    pAppPriPortInfo = (tAppPriPortEntry *)
        RBTreeGetFirst (gAppPriGlobalInfo.pRbAppPriPortTbl);
    if (pAppPriPortInfo == NULL)
    {
        DCBX_TRC_ARG1 ((DCBX_RED_TRC | DCBX_CONTROL_PLANE_TRC),
                       "In %s: No AppPriPortEntry found" "\r\n", __FUNCTION__);
        RM_FREE (pMsg);
        return;
    }

    /* For each port, form a RM message to carry remote application Priority
     * information on that port and enqueue that message on RM queue */
    while (pAppPriPortInfo != NULL)
    {
        DcbxRedFormAppPriRemInfoSyncMsg (pAppPriPortInfo, pMsg, &u4OffSet);

        if ((DCBX_DB_MAX_BUF_SIZE - u4OffSet) <
            ((DCBX_RED_APP_PRI_REM_INFO_SIZE) +
             (APP_PRI_TABLE_ENTRY_LEN * pAppPriPortInfo->u4AppPriAppProtocols)))
        {

            if (DcbxPortRmEnqMsgToRm (pMsg, (UINT2) u4OffSet,
                                      RM_DCBX_APP_ID,
                                      RM_DCBX_APP_ID) == OSIX_FAILURE)
            {
                DCBX_TRC_ARG1 ((DCBX_RED_TRC | DCBX_FAILURE_TRC),
                               "In %s: Enqueue "
                               "message to RM Failed\r\n", __FUNCTION__);
                return;
            }

            u4OffSet = DCBX_ZERO;
            pMsg = RM_ALLOC_TX_BUF (DCBX_DB_MAX_BUF_SIZE);

            if (pMsg == NULL)
            {
                DCBX_TRC_ARG1 ((DCBX_RED_TRC | DCBX_FAILURE_TRC),
                               "DcbxRedBulkSyncEtsRemInfo: Allocation "
                               "of memory from RM Failed\n", __FUNCTION__);
                return;
            }
        }

        MEMSET (&AppPriPortEntry, DCBX_ZERO, sizeof (tAppPriPortEntry));
        AppPriPortEntry.u4IfIndex = pAppPriPortInfo->u4IfIndex;
        pAppPriPortInfo = (tAppPriPortEntry *)
            RBTreeGetNext (gAppPriGlobalInfo.pRbAppPriPortTbl,
                           &AppPriPortEntry, AppPriUtlPortTblCmpFn);
    }

    DcbxPortRmEnqMsgToRm (pMsg, (UINT2) u4OffSet, RM_DCBX_APP_ID,
                          RM_DCBX_APP_ID);
    return;
}

/*****************************************************************************/
/* Function Name      : DcbxRedBulkSyncAppPriSemInfo                         */
/*                                                                           */
/* Description        : This function sends the Application Priority SEM     */
/*                      Info Bulk Sync up Message to Stadnby Node from       */
/*                      the Active Node.                                     */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
PUBLIC VOID
DcbxRedBulkSyncAppPriSemInfo (VOID)
{
    tRmMsg             *pMsg = NULL;
    tAppPriPortEntry   *pAppPriPortInfo = NULL;
    tAppPriPortEntry    AppPriPortEntry;
    UINT4               u4OffSet = DCBX_ZERO;


    pMsg = RM_ALLOC_TX_BUF (DCBX_DB_MAX_BUF_SIZE);

    if (pMsg == NULL)
    {
        DCBX_TRC_ARG1 ((DCBX_RED_TRC | DCBX_FAILURE_TRC),
                       "In %s: Allocation of memory from RM Failed\n",
                       __FUNCTION__);
        return;
    }

    /* send sync up message(s) only if application priority is configured 
     * on atleast one port */
    pAppPriPortInfo = (tAppPriPortEntry *)
        RBTreeGetFirst (gAppPriGlobalInfo.pRbAppPriPortTbl);
    if (pAppPriPortInfo == NULL)
    {
        DCBX_TRC_ARG1 ((DCBX_RED_TRC | DCBX_CONTROL_PLANE_TRC),
                       "In %s: No Application Priority Port entry",
                       __FUNCTION__);
        RM_FREE (pMsg);
        return;
    }

    /* For each port, form a RM message to carry remote application Priority
     * information on that port and enqueue that message on RM queue */
    while (pAppPriPortInfo != NULL)
    {
        DcbxRedFormAppPriSemInfoSyncMsg (pAppPriPortInfo, pMsg, &u4OffSet);

        if ((DCBX_DB_MAX_BUF_SIZE - u4OffSet) < DCBX_RED_APP_PRI_SEM_INFO_SIZE)
        {

            if (DcbxPortRmEnqMsgToRm (pMsg, (UINT2) u4OffSet,
                                      RM_DCBX_APP_ID,
                                      RM_DCBX_APP_ID) == OSIX_FAILURE)
            {
                DCBX_TRC_ARG1 ((DCBX_RED_TRC | DCBX_FAILURE_TRC),
                               "In %s: Enqueue message to RM Failed\r\n",
                               __FUNCTION__);
                return;
            }

            u4OffSet = DCBX_ZERO;
            pMsg = RM_ALLOC_TX_BUF (DCBX_DB_MAX_BUF_SIZE);

            if (pMsg == NULL)
            {
                DCBX_TRC_ARG1 ((DCBX_RED_TRC | DCBX_FAILURE_TRC),
                               "In %s: Allocation of memory from RM Failed\n",
                               __FUNCTION__);
                return;
            }
        }
        MEMSET (&AppPriPortEntry, DCBX_ZERO, sizeof (tAppPriPortEntry));
        AppPriPortEntry.u4IfIndex = pAppPriPortInfo->u4IfIndex;
        pAppPriPortInfo = (tAppPriPortEntry *)
            RBTreeGetNext (gAppPriGlobalInfo.pRbAppPriPortTbl,
                           &AppPriPortEntry, AppPriUtlPortTblCmpFn);
    }

    DcbxPortRmEnqMsgToRm (pMsg, (UINT2) u4OffSet,
                          RM_DCBX_APP_ID, RM_DCBX_APP_ID);
    return;
}

/*****************************************************************************/
/* Function Name      : DcbxRedBulkSyncAppPriCounterInfo                     */
/*                                                                           */
/* Description        : This function sends the Application Priority         */
/*                      Counters Info Bulk Sync up Message to                */
/*                      Stadnby Node from the Active Node.                   */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
PUBLIC VOID
DcbxRedBulkSyncAppPriCounterInfo (VOID)
{
    tRmMsg             *pMsg = NULL;
    tAppPriPortEntry   *pAppPriPortInfo = NULL;
    tAppPriPortEntry    AppPriPortEntry;
    UINT4               u4OffSet = DCBX_ZERO;


    pMsg = RM_ALLOC_TX_BUF (DCBX_DB_MAX_BUF_SIZE);

    if (pMsg == NULL)
    {
        DCBX_TRC_ARG1 ((DCBX_RED_TRC | DCBX_FAILURE_TRC),
                       "In %s: Allocation of memory from RM Failed\r\n",
                       __FUNCTION__);
        return;
    }

    /* send sync up message(s) only if application priority is configured 
     * on atleast one port */
    pAppPriPortInfo = (tAppPriPortEntry *)
        RBTreeGetFirst (gAppPriGlobalInfo.pRbAppPriPortTbl);
    if (pAppPriPortInfo == NULL)
    {
        DCBX_TRC_ARG1 ((DCBX_RED_TRC | DCBX_CONTROL_PLANE_TRC),
                       "In %s : No AppPriPort entry found\r\n", __FUNCTION__);
        RM_FREE (pMsg);
        return;
    }

    /* For each port, form a RM message to carry remote application Priority
     * information on that port and enqueue that message on RM queue */
    while (pAppPriPortInfo != NULL)
    {
        DcbxRedFormAppPriAllCounterInfoSyncMsg (pAppPriPortInfo, pMsg,
                                                &u4OffSet);
        if ((DCBX_DB_MAX_BUF_SIZE - u4OffSet) <
            DCBX_RED_APP_PRI_ALL_COUNTER_INFO_SIZE)
        {

            if (DcbxPortRmEnqMsgToRm (pMsg, (UINT2) u4OffSet,
                                      RM_DCBX_APP_ID,
                                      RM_DCBX_APP_ID) == OSIX_FAILURE)
            {
                DCBX_TRC_ARG1 ((DCBX_RED_TRC | DCBX_FAILURE_TRC),
                               "In %s: Enqueue message to RM Failed\r\n",
                               __FUNCTION__);
                return;
            }
            u4OffSet = DCBX_ZERO;
            pMsg = RM_ALLOC_TX_BUF (DCBX_DB_MAX_BUF_SIZE);

            if (pMsg == NULL)
            {
                DCBX_TRC_ARG1 ((DCBX_RED_TRC | DCBX_FAILURE_TRC),
                               "In %s: Allocation of memory from RM Failed\r\n",
                               __FUNCTION__);
                return;
            }
        }
        MEMSET (&AppPriPortEntry, DCBX_ZERO, sizeof (tAppPriPortEntry));
        AppPriPortEntry.u4IfIndex = pAppPriPortInfo->u4IfIndex;
        pAppPriPortInfo = (tAppPriPortEntry *)
            RBTreeGetNext (gAppPriGlobalInfo.pRbAppPriPortTbl,
                           &AppPriPortEntry, AppPriUtlPortTblCmpFn);
    }

    DcbxPortRmEnqMsgToRm (pMsg, (UINT2) u4OffSet,
                          RM_DCBX_APP_ID, RM_DCBX_APP_ID);
    return;
}

/*****************************************************************************/
/* Function Name      : DcbxRedSendDynamicAppPriRemInfo                      */
/*                                                                           */
/* Description        : This function sends the Application Priority Remote  */
/*                      table Dynamic Sync up Message to Stadnby Node from   */
/*                      the Active Node.                                     */
/*                                                                           */
/* Input(s)           : pAppPriPortInfo - Application Priority Port Entry    */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
PUBLIC VOID
DcbxRedSendDynamicAppPriRemInfo (tAppPriPortEntry * pAppPriPortInfo)
{
    tRmMsg             *pMsg = NULL;
    UINT4               u4OffSet = DCBX_ZERO;


    if ((DCBX_RED_NODE_STATUS() == RM_STANDBY) ||
        (DCBX_RED_STBY_NODE_UP_COUNT() == DCBX_ZERO))
    {
        DCBX_TRC_ARG1 ((DCBX_RED_TRC | DCBX_CONTROL_PLANE_TRC),
                       "In %s: No Need to send Sync"
                       "Message as Standby is down or the node is Active\n",
                       __FUNCTION__);
        return;
    }
    
    pMsg = RM_ALLOC_TX_BUF ((DCBX_RED_APP_PRI_REM_INFO_VALUE_SIZE) +
                            (APP_PRI_TABLE_ENTRY_LEN *
                             pAppPriPortInfo->u4AppPriAppProtocols));
    if (pMsg == NULL)
    {
        DCBX_TRC_ARG1 ((DCBX_RED_TRC | DCBX_FAILURE_TRC),
                       "In %s: Allocation of memory from RM Failed\n",
                       __FUNCTION__);
        return;
    }

    /* Form Application Priority remote information sync up message */
    DcbxRedFormAppPriRemInfoSyncMsg (pAppPriPortInfo, pMsg, &u4OffSet);

    /* Enqueue the message in RM queue */
    DcbxPortRmEnqMsgToRm (pMsg, (UINT2) u4OffSet,
                          RM_DCBX_APP_ID, RM_DCBX_APP_ID);
    return;
}

/*****************************************************************************/
/* Function Name      : DcbxRedSendDynamicAppPriSemInfo                      */
/*                                                                           */
/* Description        : This function sends the Application Priority SEM     */
/*                      information Dynamic Sync up Message to Stadnby Node  */
/*                      from the Active Node.                                */
/*                                                                           */
/* Input(s)           : pAppPriPortInfo - Application Priority Port Entry    */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
PUBLIC VOID
DcbxRedSendDynamicAppPriSemInfo (tAppPriPortEntry * pAppPriPortInfo)
{
    tRmMsg             *pMsg = NULL;
    UINT4               u4OffSet = DCBX_ZERO;


    if ((DCBX_RED_NODE_STATUS() == RM_STANDBY) ||
        (DCBX_RED_STBY_NODE_UP_COUNT() == DCBX_ZERO))
    {
        DCBX_TRC_ARG1 ((DCBX_RED_TRC | DCBX_CONTROL_PLANE_TRC),
                       "In %s: No Need to send Sync"
                       "Message as Standby is down or the node is Active\r\n",
                       __FUNCTION__);
        return;
    }

    pMsg = RM_ALLOC_TX_BUF (DCBX_RED_APP_PRI_SEM_INFO_SIZE);
    if (pMsg == NULL)
    {
        DCBX_TRC_ARG1 ((DCBX_RED_TRC | DCBX_FAILURE_TRC),
                       "In %s: Allocation of memory from RM Failed\r\n",
                       __FUNCTION__);
        return;
    }
    DCBX_TRC_ARG1 (DCBX_CONTROL_PLANE_TRC, "In %s:"
                   "Calling DcbxRedFormEtsSemInfoSyncMsg\r\n", __FUNCTION__);

    /* Form Application Priority SEM information sync up message */
    DcbxRedFormAppPriSemInfoSyncMsg (pAppPriPortInfo, pMsg, &u4OffSet);

    /* Enqueue the message in RM queue */
    DcbxPortRmEnqMsgToRm (pMsg, (UINT2) u4OffSet,
                          RM_DCBX_APP_ID, RM_DCBX_APP_ID);
    return;
}

/*****************************************************************************/
/* Function Name      : DcbxRedSendDynamicAppPriCounterInfo                  */
/*                                                                           */
/* Description        : This function sends the Application Priority Counter */
/*                      information Dynamic Sync up Message to Stadnby Node  */
/*                      from the Active Node.                                */
/*                                                                           */
/* Input(s)           : pAppPriPortInfo - Application Priority Port Entry    */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
PUBLIC VOID         DcbxRedSendDynamicAppPriCounterInfo
    (tAppPriPortEntry * pAppPriPortInfo, UINT1 u1CounterType)
{
    tRmMsg             *pMsg = NULL;
    UINT4               u4OffSet = DCBX_ZERO;


    if ((DCBX_RED_NODE_STATUS() == RM_STANDBY) ||
        (DCBX_RED_STBY_NODE_UP_COUNT() == DCBX_ZERO))
    {
        DCBX_TRC_ARG1 ((DCBX_RED_TRC | DCBX_CONTROL_PLANE_TRC),
                       "In %s: No Need to send Sync"
                       "Message as Standby is down or the node is Active\r\n",
                       __FUNCTION__);
        return;
    }

    pMsg = RM_ALLOC_TX_BUF (DCBX_RED_APP_PRI_COUNTER_INFO_SIZE);

    if (pMsg == NULL)
    {
        DCBX_TRC_ARG1 ((DCBX_RED_TRC | DCBX_FAILURE_TRC),
                       "In %s: Allocation of memory from RM Failed\n",
                       __FUNCTION__);
        return;
    }

    /* Form Application Priority SEM information sync up message */
    DcbxRedFormAppPriCounterInfoSyncMsg (pAppPriPortInfo, pMsg, &u4OffSet,
                                         u1CounterType);
    /* Enqueue the message in RM queue */
    DcbxPortRmEnqMsgToRm (pMsg, (UINT2) u4OffSet,
                          RM_DCBX_APP_ID, RM_DCBX_APP_ID);
    return;
}

/*****************************************************************************/
/* Function Name      : DcbxRedFormAppPriRemInfoSyncMsg                      */
/*                                                                           */
/* Description        : This function forms the Dynamic Sync Up message for  */
/*                      Application Priority Remote table which needs to be  */
/*                      sent to Standby Node from the Active Node.           */
/*                                                                           */
/* Input(s)           : pAppPriPortInfo - Application Priority Port Entry    */
/*                      pMsg - RM Message Structure that needs to be filled  */
/*                      pu4OffSet - OffSet Value                             */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
PUBLIC VOID
DcbxRedFormAppPriRemInfoSyncMsg (tAppPriPortEntry * pAppPriPortInfo,
                                 tRmMsg * pMsg, UINT4 *pu4OffSet)
{
    tAppPriMappingEntry *pAppPriEntry = NULL;
    UINT2               u2MsgLen = DCBX_ZERO;
    UINT1               u1Selector = DCBX_ZERO;
    UINT1               u1OperVersion = DCBX_ZERO;

    u2MsgLen = (UINT2) (DCBX_RED_APP_PRI_REM_INFO_SIZE + 
                        (pAppPriPortInfo->u4AppPriAppProtocols *
                         APP_PRI_TABLE_ENTRY_LEN));

    DCBX_RM_PUT_1_BYTE (pMsg, pu4OffSet, DCBX_RED_APP_PRI_REM_INFO);
    DCBX_RM_PUT_2_BYTE (pMsg, pu4OffSet, u2MsgLen);
    DCBX_RM_PUT_4_BYTE (pMsg, pu4OffSet, pAppPriPortInfo->u4IfIndex);
    DCBX_RM_PUT_4_BYTE (pMsg, pu4OffSet,
                        pAppPriPortInfo->AppPriRemPortInfo.u4AppPriRemTimeMark);
    DCBX_RM_PUT_4_BYTE (pMsg, pu4OffSet,
                        pAppPriPortInfo->AppPriRemPortInfo.i4AppPriRemIndex);
    DCBX_RM_PUT_N_BYTE (pMsg,
                        pAppPriPortInfo->AppPriRemPortInfo.
                        au1AppPriDestRemMacAddr, pu4OffSet, MAC_ADDR_LEN);
    DCBX_RM_PUT_1_BYTE (pMsg, pu4OffSet,
                        pAppPriPortInfo->AppPriRemPortInfo.u1AppPriRemWilling);
    /* pAppPriPortInfo->u4AppPriAppProtocols indicate the number of application
     * tp priority mapping entries in the remote table */
    DCBX_RM_PUT_4_BYTE (pMsg, pu4OffSet, pAppPriPortInfo->u4AppPriAppProtocols);

    /* Fill the application to priority mapping entries in the remote
     * table one by one */
    for (u1Selector = DCBX_ONE; u1Selector <= APP_PRI_MAX_SELECTOR;
         u1Selector++)
    {
        DcbxUtlGetOperVersion (pAppPriPortInfo->u4IfIndex , &u1OperVersion);
        if(u1OperVersion == DCBX_VER_CEE)
        {
            /* For CEE Selector 2 = TCP and 3 = UDP CEE is not valid */
            if ((u1Selector == DCBX_TWO) || (u1Selector == DCBX_THREE))
            {
                continue;
            }
        }

        TMO_SLL_Scan (&(APP_PRI_REM_TBL (pAppPriPortInfo, u1Selector - 1)),
                      pAppPriEntry, tAppPriMappingEntry *)
        {
            DCBX_RM_PUT_1_BYTE (pMsg, pu4OffSet, u1Selector);
            DCBX_RM_PUT_4_BYTE (pMsg, pu4OffSet,
                                pAppPriEntry->i4AppPriProtocol);
            DCBX_RM_PUT_1_BYTE (pMsg, pu4OffSet,
                                pAppPriEntry->u1AppPriPriority);
        }
    }
    return;
}

/*****************************************************************************/
/* Function Name      : DcbxRedFormAppPriSemInfoSyncMsg                      */
/*                                                                           */
/* Description        : This function forms the Dynamic Sync Up message for  */
/*                      Application Priority SEM information which needs to  */
/*                      be sent to Standby Node from the Active Node.        */
/*                                                                           */
/* Input(s)           : pAppPriPortInfo - Application Priority Port Entry    */
/*                      pMsg - RM Message Structure that needs to be filled  */
/*                      pu4OffSet - OffSet Value                             */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
PUBLIC VOID
DcbxRedFormAppPriSemInfoSyncMsg (tAppPriPortEntry * pAppPriPortInfo,
                                 tRmMsg * pMsg, UINT4 *pu4OffSet)
{
    UINT2               u2MsgLen = DCBX_ZERO;

    u2MsgLen = DCBX_RED_TYPE_FIELD_SIZE + DCBX_RED_LEN_FIELD_SIZE +
        DCBX_RED_APP_PRI_SEM_INFO_VALUE_SIZE;

    DCBX_TRC_ARG4 (DCBX_CONTROL_PLANE_TRC, "In %s:"
                   "MsgLen:%d IfIndex:%d OperState:%d\r\n", __FUNCTION__,
                   u2MsgLen, pAppPriPortInfo->u4IfIndex,
                   pAppPriPortInfo->u1AppPriDcbxOperState);

    DCBX_RM_PUT_1_BYTE (pMsg, pu4OffSet, DCBX_RED_APP_PRI_SEM_INFO);
    DCBX_RM_PUT_2_BYTE (pMsg, pu4OffSet, u2MsgLen);
    DCBX_RM_PUT_4_BYTE (pMsg, pu4OffSet, pAppPriPortInfo->u4IfIndex);
    DCBX_RM_PUT_1_BYTE (pMsg, pu4OffSet,
                        pAppPriPortInfo->u1AppPriDcbxOperState);

   return;
}

/*****************************************************************************/
/* Function Name      : DcbxRedFormAppPriCounterInfoSyncMsg                  */
/*                                                                           */
/* Description        : This function forms the Dynamic Sync Up message for  */
/*                      Application Priority Counter information which needs */
/*                      to be sent to Standby Node from the Active Node.     */
/*                                                                           */
/* Input(s)           : pAppPriPortInfo - Application Priority Port Entry    */
/*                      pMsg - RM Message Structure that needs to be filled  */
/*                      pu4OffSet - OffSet Value                             */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
PUBLIC VOID         DcbxRedFormAppPriCounterInfoSyncMsg
    (tAppPriPortEntry * pAppPriPortInfo,
     tRmMsg * pMsg, UINT4 *pu4OffSet, UINT1 u1CounterType)
{
    UINT2               u2MsgLen = DCBX_ZERO;

    u2MsgLen = DCBX_RED_TYPE_FIELD_SIZE + DCBX_RED_LEN_FIELD_SIZE +
        DCBX_RED_APP_PRI_COUNTER_INFO_VALUE_SIZE;
    DCBX_RM_PUT_1_BYTE (pMsg, pu4OffSet, DCBX_RED_APP_PRI_COUNTER_INFO);
    DCBX_RM_PUT_2_BYTE (pMsg, pu4OffSet, u2MsgLen);

    /* CounterType will indicate which value of which counter has been 
     * updated */
    DCBX_RM_PUT_1_BYTE (pMsg, pu4OffSet, u1CounterType);
    DCBX_RM_PUT_4_BYTE (pMsg, pu4OffSet, pAppPriPortInfo->u4IfIndex);
    switch (u1CounterType)
    {
        case DCBX_RED_APP_PRI_TX_COUNTER:
            DCBX_RM_PUT_4_BYTE (pMsg, pu4OffSet,
                                pAppPriPortInfo->u4AppPriTLVTxCount);
            break;

        case DCBX_RED_APP_PRI_RX_COUNTER:
            DCBX_RM_PUT_4_BYTE (pMsg, pu4OffSet,
                                pAppPriPortInfo->u4AppPriTLVRxCount);
            break;
        case DCBX_RED_APP_PRI_ERR_COUNTER:
            DCBX_RM_PUT_4_BYTE (pMsg, pu4OffSet,
                                pAppPriPortInfo->u4AppPriTLVRxError);
            break;
        default:                /* DCBX_RED_APP_PRI_PROTOCOL_COUNTER: */
            DCBX_RM_PUT_4_BYTE (pMsg, pu4OffSet,
                                pAppPriPortInfo->u4AppPriAppProtocols);
            break;
    }
    return;
}

/*****************************************************************************/
/* Function Name      : DcbxRedFormAppPriAllCounterInfoSyncMsg               */
/*                                                                           */
/* Description        : This function forms the Dynamic Sync Up message for  */
/*                      Application Priority Counter information which needs */
/*                      to be sent to Standby Node from the Active Node.     */
/*                                                                           */
/* Input(s)           : pAppPriPortInfo - Application Priority Port Entry    */
/*                      pMsg - RM Message Structure that needs to be filled  */
/*                      pu4OffSet - OffSet Value                             */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
PUBLIC VOID         DcbxRedFormAppPriAllCounterInfoSyncMsg
    (tAppPriPortEntry * pAppPriPortInfo, tRmMsg * pMsg, UINT4 *pu4OffSet)
{
    UINT2               u2MsgLen = DCBX_ZERO;

    u2MsgLen = DCBX_RED_TYPE_FIELD_SIZE + DCBX_RED_LEN_FIELD_SIZE +
        DCBX_RED_APP_PRI_ALL_COUNTER_INFO_VALUE_SIZE;


    DCBX_RM_PUT_1_BYTE (pMsg, pu4OffSet, DCBX_RED_APP_PRI_ALL_COUNTER_INFO);
    DCBX_RM_PUT_2_BYTE (pMsg, pu4OffSet, u2MsgLen);
    DCBX_RM_PUT_4_BYTE (pMsg, pu4OffSet, pAppPriPortInfo->u4IfIndex);
    DCBX_RM_PUT_4_BYTE (pMsg, pu4OffSet, pAppPriPortInfo->u4AppPriTLVTxCount);
    DCBX_RM_PUT_4_BYTE (pMsg, pu4OffSet, pAppPriPortInfo->u4AppPriTLVRxCount);
    DCBX_RM_PUT_4_BYTE (pMsg, pu4OffSet, pAppPriPortInfo->u4AppPriTLVRxError);
    DCBX_RM_PUT_4_BYTE (pMsg, pu4OffSet, pAppPriPortInfo->u4AppPriAppProtocols);

    return;
}

/*****************************************************************************
 * Function Name      : DcbxCEERedSendDynamicSyncInfo 
 *                                                                           
 * Description        : This function forms the Dynamic Sync Up message for  
 *                      CEE pkt received, which needs 
 *                      to be sent to Standby Node from the Active Node.     
 *                                                                           
 * Input(s)           : pMsg - RM Message Structure that needs to be filled  
 *                                                                           
 * Output(s)          : None                                                 
 *                                                                           
 * Return Value(s)    : None                                                 
 *****************************************************************************/
VOID DcbxCEERedSendDynamicSyncInfo (tDcbxQueueMsg *pMsg)
{
    tRmMsg             *pRmMsg = NULL;
    UINT4               u4OffSet = DCBX_ZERO;
    UINT2               u2MsgLen = DCBX_ZERO;


    if ((DCBX_RED_NODE_STATUS() == RM_STANDBY) ||
        (DCBX_RED_STBY_NODE_UP_COUNT() == DCBX_ZERO))
    {
        DCBX_TRC_ARG1 ((DCBX_RED_TRC | DCBX_CONTROL_PLANE_TRC),
                       "In %s: No Need to send Sync"
                       "Message as Standby is down or the node is Active\n",
                       __FUNCTION__);
        return;
    }

    if (pMsg == NULL)
    {
        DCBX_TRC_ARG1 ((DCBX_RED_TRC | DCBX_CONTROL_PLANE_TRC),
                "In %s: Msg is NULL. Return!!!\n",
                __func__);
        return;
    }

    pRmMsg = RM_ALLOC_TX_BUF (DCBX_DB_MAX_BUF_SIZE);
    if (pRmMsg == NULL)
    {
        DCBX_TRC_ARG1 ((DCBX_RED_TRC | DCBX_FAILURE_TRC),
                       "In %s: Allocation of memory from RM Failed\n",
                       __func__);
        return;
    }

    u2MsgLen = (UINT2) (DCBX_RED_TYPE_FIELD_SIZE + DCBX_RED_LEN_FIELD_SIZE +
                        + sizeof (tDcbxQueueMsg));

    DCBX_RM_PUT_1_BYTE (pRmMsg, &u4OffSet, DCBX_CEE_RED_DYN_SYNC_INFO);
    DCBX_RM_PUT_2_BYTE (pRmMsg, &u4OffSet, u2MsgLen);
    DCBX_RM_PUT_N_BYTE (pRmMsg, pMsg, &u4OffSet, sizeof (tDcbxQueueMsg));

    DCBX_TRC_ARG4 ((DCBX_RED_TRC | DCBX_CONTROL_PLANE_TRC), 
            "In func %s: u4MsgType = %d u2MsgLen = %d u2DataLen = %d\n", 
            __func__, pMsg->u4MsgType, u2MsgLen, 
            pMsg->unMsgParam.ApplTlvParam.u2RxTlvLen);

    /* Enqueue the message in RM queue */
    DcbxPortRmEnqMsgToRm (pRmMsg, (UINT2) u4OffSet,
                          RM_DCBX_APP_ID, RM_DCBX_APP_ID);
}

/*****************************************************************************
 * Function Name      : DcbxCEERedProcDynamicSyncInfo 
 *                                                                           
 * Description        : This function forms the Dynamic Sync Up message for  
 *                      CEE pkt received, which needs 
 *                      to be sent to Standby Node from the Active Node.     
 *                                                                           
 * Input(s)           : pMsg - RM Message Structure that needs to be filled  
 *                                                                           
 * Output(s)          : None                                                 
 *                                                                           
 * Return Value(s)    : None                                                 
 *****************************************************************************/
VOID DcbxCEERedProcDynamicSyncInfo (tRmMsg *pMsg, UINT4 *pu4OffSet,
                                           UINT2 u2RemMsgLen)
{
    tRmProtoEvt         ProtoEvt;
    tDcbxQueueMsg      *pQMsg = NULL;


    MEMSET (&ProtoEvt, DCBX_ZERO, sizeof (tRmProtoEvt));

    ProtoEvt.u4AppId = RM_DCBX_APP_ID;
    ProtoEvt.u4Event = RM_BULK_UPDT_ABORT;

    if (u2RemMsgLen > sizeof (tDcbxQueueMsg))
    {
        DCBX_TRC_ARG1 ((DCBX_RED_TRC | DCBX_FAILURE_TRC),
                       "In %s: Wrong length received!!\n",
                       __func__);
        ProtoEvt.u4Error = RM_PROCESS_FAIL;

        DcbxPortRmApiHandleProtocolEvent (&ProtoEvt);
        *pu4OffSet += u2RemMsgLen;
        return;
    }

    /*Allocate memory for QueueMsg */
    if ((pQMsg = (tDcbxQueueMsg *) MemAllocMemBlk
         (gDcbxGlobalInfo.DcbxQPktPoolId)) == NULL)
    {
        DCBX_TRC_ARG1 ((DCBX_RED_TRC | DCBX_RESOURCE_TRC),
                       "In %s: Allocation of memory from DcbxQue Failed\n",
                       __func__);
        ProtoEvt.u4Error = RM_PROCESS_FAIL;

        DcbxPortRmApiHandleProtocolEvent (&ProtoEvt);
        *pu4OffSet += u2RemMsgLen;
        return;
    }

    DCBX_RM_GET_N_BYTE (pMsg, pQMsg,
                        pu4OffSet, sizeof (tDcbxQueueMsg));

    DCBX_TRC_ARG4 ((DCBX_RED_TRC), "In func %s: pMsg->u4IfIndex = %d "
            "u4MsgType = %d u2DataLen = %d\n", 
            __func__, pQMsg->u4IfIndex , pQMsg->u4MsgType,
            pQMsg->unMsgParam.ApplTlvParam.u2RxTlvLen);

    DcbxQueProcessApplTlvMsg (pQMsg);

    /* Release the Memory for the respective Queue Message since it is
     * Processed */
    MemReleaseMemBlock (gDcbxGlobalInfo.DcbxQPktPoolId, (UINT1 *) pQMsg);
    return;
}

/*****************************************************************************/
/* Function Name      : DcbxCeeRedSendDynamicCtrlCounterInfo                 */
/*                                                                           */
/* Description        : This function sends the Ctrl Counter information     */
/*                      Dynamic Sync up Message to Stadnby Node from         */
/*                      the Active Node.                                     */
/*                                                                           */
/* Input(s)           : pCtrlInfo - Ctrl Port Entry                          */
/*                      u1CounterType - Tx/Rx/Rx Err count                   */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
DcbxCeeRedSendDynamicCtrlCounterInfo (tCEECtrlEntry *pCtrlInfo, UINT1 u1CounterType)
{
    tRmMsg             *pMsg = NULL;
    UINT4               u4OffSet = DCBX_ZERO;
    UINT2               u2MsgLen = DCBX_ZERO;


    if ((DCBX_RED_NODE_STATUS() == RM_STANDBY) ||
        (DCBX_RED_STBY_NODE_UP_COUNT() == DCBX_ZERO))
    {
        DCBX_TRC_ARG1 (DCBX_RED_TRC | DCBX_CONTROL_PLANE_TRC,
                  "In func %s: No Need to send Sync"
                  "Message as Standby is down or the node is Active\n", __func__);
        return;
    }

    pMsg = RM_ALLOC_TX_BUF (DCBX_CEE_RED_CTRL_COUNTER_INFO_SIZE);
    if (pMsg == NULL)
    {
        DCBX_TRC_ARG1 (DCBX_RED_TRC | DCBX_FAILURE_TRC,
                  "In func %s: Allocation "
                  "of memory from RM Failed\n", __func__);
        return;
    }

    u2MsgLen = DCBX_CEE_RED_CTRL_COUNTER_INFO_SIZE;

    DCBX_RM_PUT_1_BYTE (pMsg, &u4OffSet, DCBX_CEE_RED_CTRL_COUNTER_INFO);
    DCBX_RM_PUT_2_BYTE (pMsg, &u4OffSet, u2MsgLen);
    DCBX_RM_PUT_4_BYTE (pMsg, &u4OffSet, pCtrlInfo->u4IfIndex);
    DCBX_RM_PUT_1_BYTE (pMsg, &u4OffSet, u1CounterType);

    if (u1CounterType == DCBX_CEE_RED_CTRL_TX_COUNTER)
    {
        DCBX_RM_PUT_4_BYTE (pMsg, &u4OffSet, pCtrlInfo->u4CtrlTxTLVCount);
    }
    if (u1CounterType == DCBX_CEE_RED_CTRL_RX_COUNTER)
    {
        DCBX_RM_PUT_4_BYTE (pMsg, &u4OffSet, pCtrlInfo->u4CtrlRxTLVCount);
    }
    if (u1CounterType == DCBX_CEE_RED_CTRL_RX_ERR_COUNTER)
    {
        DCBX_RM_PUT_4_BYTE (pMsg, &u4OffSet, pCtrlInfo->u4CtrlRxTLVErrorCount);
    }
 
    DcbxPortRmEnqMsgToRm (pMsg, (UINT2) u4OffSet,
                          RM_DCBX_APP_ID, RM_DCBX_APP_ID);
    return;
}

/***************************************************************************
 * FUNCTION NAME    : DcbxCeeRedProcessDynCtrlCounterInfo
 *
 * DESCRIPTION      : This routine decodes the sync-up message from peer active
 *                    DCBX and updates Ctrl Counter information in standby DCBX.
 *
 * INPUT            : pMsg - RM Sync-up message
 *                    u2RemMsgLen - Length of value of Message
 *                    
 * OUTPUT           : u4OffSet - Offset value 
 * 
 * RETURNS          : VOID 
 * 
 **************************************************************************/
VOID
DcbxCeeRedProcessDynCtrlCounterInfo (tRmMsg * pMsg, UINT4 *pu4OffSet,
                                     UINT2 u2RemMsgLen)
{
    tCEECtrlEntry       *pCtrlEntry = NULL;
    tRmProtoEvt         ProtoEvt;
    UINT4               u4IfIndex = DCBX_ZERO;
    UINT4               u4CounterVal = DCBX_ZERO;
    UINT1               u1CounterType = DCBX_ZERO;


    MEMSET (&ProtoEvt, DCBX_ZERO, sizeof (tRmProtoEvt));

    ProtoEvt.u4AppId = RM_DCBX_APP_ID;
    ProtoEvt.u4Event = RM_BULK_UPDT_ABORT;

    if (u2RemMsgLen != DCBX_CEE_RED_CTRL_COUNTER_INFO_VALUE_SIZE)
    {
        ProtoEvt.u4Error = RM_PROCESS_FAIL;

        DcbxPortRmApiHandleProtocolEvent (&ProtoEvt);

        *pu4OffSet += u2RemMsgLen;
        return;
    }

    DCBX_RM_GET_4_BYTE (pMsg, pu4OffSet, u4IfIndex);
    DCBX_RM_GET_1_BYTE (pMsg, pu4OffSet, u1CounterType);

    pCtrlEntry = CEEUtlGetCtrlEntry (u4IfIndex);
    if (pCtrlEntry == NULL)
    {
        DCBX_TRC_ARG1 ((DCBX_RED_TRC | DCBX_FAILURE_TRC),
                       "In %s: Ctrl Entry Found \n", __FUNCTION__);
        return;
    }

    DCBX_RM_GET_4_BYTE (pMsg, pu4OffSet, u4CounterVal);
    if (u1CounterType == DCBX_CEE_RED_CTRL_TX_COUNTER)
    {
        pCtrlEntry->u4CtrlTxTLVCount = u4CounterVal;
    }
    if (u1CounterType == DCBX_CEE_RED_CTRL_RX_COUNTER)
    {
        pCtrlEntry->u4CtrlRxTLVCount = u4CounterVal;
    }
    if (u1CounterType == DCBX_CEE_RED_CTRL_RX_ERR_COUNTER)
    {
        pCtrlEntry->u4CtrlRxTLVErrorCount = u4CounterVal;
    }
    return;
}

/************ BULK UPDATE SYNC & PROCESSING ************/

/****************************************************************************
 * Function Name      : DcbxRedBulkSyncDcbxPortInfo
 *                                                                           
 * Description        : This function sends the DCBx Port Info table             
 *                      Bulk Sync up Message to Stadnby Node from            
 *                      the Active Node.                                     
 *                                                                           
 * Input(s)           : None                                                 
 *                                                                           
 * Output(s)          : None                                                 
 *                                                                           
 * Return Value(s)    : None                                                 
 *****************************************************************************/
VOID
DcbxRedBulkSyncDcbxPortInfo (VOID)
{
    tRmMsg             *pMsg = NULL;
    tDcbxPortEntry     *pDcbxPortEntry = NULL;
    tDcbxPortEntry      DcbxPortEntry;
    UINT4               u4OffSet = DCBX_ZERO;
    UINT2               u2MsgLen = DCBX_ZERO;


    pMsg = RM_ALLOC_TX_BUF (DCBX_DB_MAX_BUF_SIZE);
    if (pMsg == NULL)
    {
        DCBX_TRC_ARG1 (DCBX_RED_TRC | DCBX_RESOURCE_TRC,
                  "In func %s: Allocation "
                  "of memory from RM Failed\n", __func__);
        return;
    }

    pDcbxPortEntry = (tDcbxPortEntry *)
        RBTreeGetFirst (gDcbxGlobalInfo.pRbDcbxPortTbl);
    if (pDcbxPortEntry == NULL)
    {
        DCBX_TRC (DCBX_RED_TRC | DCBX_FAILURE_TRC,
                "DCBX Port Entry not found !!!\r\n");
        RM_FREE (pMsg);
        return;
    }
    
    while (pDcbxPortEntry != NULL)
    {
        /* Form Dcbx Port Info sync up message */
        u2MsgLen = (UINT2)(DCBX_CEE_RED_DCBX_PORT_INFO_SIZE);

        /* Fill the MSG Type and Length */
        DCBX_RM_PUT_1_BYTE (pMsg, &u4OffSet, DCBX_CEE_RED_DCBX_PORT_INFO);
        DCBX_RM_PUT_2_BYTE (pMsg, &u4OffSet, u2MsgLen);

        /* Fill the Interface Index, used to fetch the DCBx Port Entry */
        DCBX_RM_PUT_4_BYTE (pMsg, &u4OffSet, pDcbxPortEntry->u4IfIndex);
        /* Fill the Dcbx version details */
        DCBX_RM_PUT_1_BYTE (pMsg, &u4OffSet, pDcbxPortEntry->u1DcbxMode);
        DCBX_RM_PUT_1_BYTE (pMsg, &u4OffSet, pDcbxPortEntry->u1OperVersion);
        DCBX_RM_PUT_1_BYTE (pMsg, &u4OffSet, pDcbxPortEntry->u1MaxVersion);
        DCBX_RM_PUT_1_BYTE (pMsg, &u4OffSet, pDcbxPortEntry->u1PeerOperVersion);
        DCBX_RM_PUT_1_BYTE (pMsg, &u4OffSet, pDcbxPortEntry->u1PeerMaxVersion);
        DCBX_RM_PUT_1_BYTE (pMsg, &u4OffSet, pDcbxPortEntry->u1LldpTxStatus);
        DCBX_RM_PUT_1_BYTE (pMsg, &u4OffSet, pDcbxPortEntry->u1LldpRxStatus);

        if ((DCBX_DB_MAX_BUF_SIZE - u4OffSet) < u2MsgLen)
        {
            if (DcbxPortRmEnqMsgToRm (pMsg, (UINT2) u4OffSet,
                                      RM_DCBX_APP_ID,
                                      RM_DCBX_APP_ID) == OSIX_FAILURE)
            {
                DCBX_TRC_ARG1 (DCBX_RED_TRC | DCBX_FAILURE_TRC,
                          "In func %s: Enqueue "
                          "message to RM Failed\r\n", __func__);
                return;
            }

            u4OffSet = DCBX_ZERO;

            pMsg = RM_ALLOC_TX_BUF (DCBX_DB_MAX_BUF_SIZE);
            if (pMsg == NULL)
            {
                DCBX_TRC_ARG1 (DCBX_RED_TRC | DCBX_FAILURE_TRC,
                          "In func %s: Allocation "
                          "of memory from RM Failed\n", __func__);
                return;
            }
        }

        MEMSET (&DcbxPortEntry, DCBX_ZERO, sizeof (tDcbxPortEntry));
        DcbxPortEntry.u4IfIndex = pDcbxPortEntry->u4IfIndex;
        pDcbxPortEntry = (tDcbxPortEntry *)
            RBTreeGetNext (gDcbxGlobalInfo.pRbDcbxPortTbl,
                       &DcbxPortEntry, DcbxUtlPortTblCmpFn);
    }

    if (DcbxPortRmEnqMsgToRm (pMsg, (UINT2) u4OffSet, RM_DCBX_APP_ID,
                          RM_DCBX_APP_ID) == OSIX_FAILURE) 
    {
        DCBX_TRC_ARG1 (DCBX_RED_TRC | DCBX_FAILURE_TRC,
                "In func %s: Enqueue "
                "message to RM Failed\r\n", __func__);
        return;
    }

    return;
}

/***************************************************************************
 * FUNCTION NAME    : DcbxRedProcessDynamicDcbxPortInfo
 *
 * DESCRIPTION      : This routine decodes the sync-up message from peer active
 *                    DCBX and updates the DCBX Port information in
 *                    standby DCBX.
 *
 * INPUT            : pMsg - RM Sync-up message
 *                    u2RemMsgLen - Length of value of Message
 *                    
 * OUTPUT           : u4OffSet - Offset value 
 * 
 * RETURNS          : VOID 
 * 
 **************************************************************************/
VOID
DcbxRedProcessDynamicDcbxPortInfo (tRmMsg *pMsg, UINT4 *pu4OffSet,
                                 UINT2 u2RemMsgLen)
{
    tDcbxPortEntry    *pPortInfo = NULL;
    tRmProtoEvt        ProtoEvt;
    UINT4              u4IfIndex = DCBX_ZERO;

    MEMSET (&ProtoEvt, DCBX_ZERO, sizeof (tRmProtoEvt));


    ProtoEvt.u4AppId = RM_DCBX_APP_ID;
    ProtoEvt.u4Event = RM_BULK_UPDT_ABORT;

    /* Validate the length received */
    if (u2RemMsgLen != DCBX_CEE_RED_DCBX_PORT_INFO_VALUE_SIZE)
    {
        ProtoEvt.u4Error = RM_PROCESS_FAIL;

        DcbxPortRmApiHandleProtocolEvent (&ProtoEvt);
        *pu4OffSet += (UINT4) u2RemMsgLen;
        
        DCBX_TRC_ARG3 (DCBX_RED_TRC | DCBX_FAILURE_TRC,
                "In %s: Message length received [%d] is not equal to [%d]\n",
                __func__, u2RemMsgLen, DCBX_CEE_RED_DCBX_PORT_INFO_VALUE_SIZE);
        return;
    }

    /* Get the interface Index */
    DCBX_RM_GET_4_BYTE (pMsg, pu4OffSet, u4IfIndex);

    /* Get the Dcbx Port Entry using the fetched interface index */
    pPortInfo = DcbxUtlGetPortEntry (u4IfIndex);
    if (pPortInfo == NULL)
    {
        DCBX_TRC_ARG1 (DCBX_RED_TRC | DCBX_FAILURE_TRC,
                  "In func %s: DCBx Port Entry not found \n", __func__);
        return;
    }
    
    /* Get the Dcbx Port Entry details */
    DCBX_RM_GET_1_BYTE (pMsg, pu4OffSet, pPortInfo->u1DcbxMode);
    DCBX_RM_GET_1_BYTE (pMsg, pu4OffSet, pPortInfo->u1OperVersion);
    DCBX_RM_GET_1_BYTE (pMsg, pu4OffSet, pPortInfo->u1MaxVersion);
    DCBX_RM_GET_1_BYTE (pMsg, pu4OffSet, pPortInfo->u1PeerOperVersion);
    DCBX_RM_GET_1_BYTE (pMsg, pu4OffSet, pPortInfo->u1PeerMaxVersion);
    DCBX_RM_GET_1_BYTE (pMsg, pu4OffSet, pPortInfo->u1LldpTxStatus);
    DCBX_RM_GET_1_BYTE (pMsg, pu4OffSet, pPortInfo->u1LldpRxStatus);

    DCBX_TRC_ARG6 (DCBX_RED_TRC | DCBX_CONTROL_PLANE_TRC, 
            "In func %s: Recieved DCBX Port Entry with "
            "u1DcbxMode: %d u1OperVersion = %d u1MaxVersion = %d "
            "u1PeerOperVersion = %d u1PeerMaxVersion = %d\n", 
            __func__, pPortInfo->u1DcbxMode, pPortInfo->u1OperVersion,
            pPortInfo->u1MaxVersion, pPortInfo->u1PeerOperVersion,
            pPortInfo->u1PeerMaxVersion);

    DCBX_TRC_ARG1 (DCBX_RED_TRC | DCBX_CONTROL_PLANE_TRC,
            "Exit func %s\n", __func__);
    return;
}

/****************************************************************************
 * Function Name      : DcbxCeeRedBulkSyncCtrlInfo 
 *                                                                           
 * Description        : This function sends the CEE Ctrl table             
 *                      Bulk Sync up Message to Stadnby Node from         
 *                      the Active Node.                                     
 *                                                                           
 * Input(s)           : pCtrlEntry - CEE Ctrl Port Entry                        
 *                                                                           
 * Output(s)          : None                                                 
 *                                                                           
 * Return Value(s)    : None                                                 
 *****************************************************************************/
VOID
DcbxCeeRedBulkSyncCtrlInfo (VOID)
{
    tCEECtrlEntry CtrlInfo;
    tRmMsg        *pMsg       = NULL;
    tCEECtrlEntry *pCtrlEntry = NULL;
    UINT4         u4OffSet    = DCBX_ZERO;


    pMsg = RM_ALLOC_TX_BUF (DCBX_DB_MAX_BUF_SIZE);
    if (pMsg == NULL)
    {
        DCBX_TRC_ARG1 ((DCBX_RED_TRC | DCBX_RESOURCE_TRC),
                       "In %s: Allocation of memory from RM Failed\n",
                       __FUNCTION__);
        return;
    }

    pCtrlEntry = (tCEECtrlEntry *) RBTreeGetFirst
        (gCEEGlobalInfo.pRbCeeCtrlTbl);
    if (pCtrlEntry == NULL)
    {
        DCBX_TRC_ARG1 ((DCBX_RED_TRC | DCBX_FAILURE_TRC),
                       "In %s: Ctrl Entry not found\n",
                       __FUNCTION__);
        RM_FREE (pMsg);
        return;
    }

    /* For each port, form a RM message to carry Ctrl
     * information on that port and enqueue that message on RM queue */
    while (pCtrlEntry != NULL)
    {
        DcbxCeeRedFormCtrlInfoSyncMsg (pCtrlEntry, pMsg, &u4OffSet);

        if ((DCBX_DB_MAX_BUF_SIZE - u4OffSet) < DCBX_CEE_RED_CTRL_INFO_SIZE)
        {
            if (DcbxPortRmEnqMsgToRm (pMsg, (UINT2) u4OffSet,
                                      RM_DCBX_APP_ID,
                                      RM_DCBX_APP_ID) == OSIX_FAILURE)
            {
                DCBX_TRC_ARG1 ((DCBX_RED_TRC | DCBX_FAILURE_TRC),
                               "In %s: Enqueue message to RM Failed\r\n",
                               __FUNCTION__);
                return;
            }

            u4OffSet = DCBX_ZERO;

            pMsg = RM_ALLOC_TX_BUF (DCBX_DB_MAX_BUF_SIZE);
            if (pMsg == NULL)
            {
                DCBX_TRC_ARG1 ((DCBX_RED_TRC | DCBX_RESOURCE_TRC),
                               "In %s: Allocation of memory from RM Failed\n",
                               __FUNCTION__);
                return;
            }
        }

        MEMSET (&CtrlInfo, DCBX_ZERO, sizeof (tCEECtrlEntry));

        CtrlInfo.u4IfIndex = pCtrlEntry->u4IfIndex;
        pCtrlEntry = (tCEECtrlEntry *) RBTreeGetNext
            (gCEEGlobalInfo.pRbCeeCtrlTbl, &CtrlInfo,
             CEEUtlPortTblCmpFn);
    }

    DcbxPortRmEnqMsgToRm (pMsg, (UINT2) u4OffSet,
                          RM_DCBX_APP_ID, RM_DCBX_APP_ID);

    DCBX_TRC_ARG1 (DCBX_RED_TRC | DCBX_CONTROL_PLANE_TRC,
              "Exiting %s\n", __func__);
    return;
}

/*****************************************************************************
 * Function Name      : DcbxCeeRedFormCtrlInfoSyncMsg                      
 *                                                                           
 * Description        : This function forms the Sync Up message for  
 *                      Ctrl Information which needs to  
 *                      be sent to Standby Node from the Active Node.        
 *                                                                           
 * Input(s)           : pAppPriPortInfo - Application Priority Port Entry    
 *                      pMsg - RM Message Structure that needs to be filled  
 *                      pu4OffSet - OffSet Value                             
 *                                                                           
 * Output(s)          : None                                                 
 *                                                                           
 * Return Value(s)    : None                                                 
 *****************************************************************************/
VOID
DcbxCeeRedFormCtrlInfoSyncMsg (tCEECtrlEntry* pCtrlInfo,
                            tRmMsg * pMsg, UINT4 *pu4OffSet)
{
    UINT2               u2MsgLen = DCBX_ZERO;

    u2MsgLen = DCBX_CEE_RED_CTRL_INFO_SIZE;

    DCBX_TRC_ARG3 (DCBX_CONTROL_PLANE_TRC, "In %s: "
                   "MsgLen:%d IfIndex:%d\r\n", __FUNCTION__,
                   u2MsgLen, pCtrlInfo->u4IfIndex);

    DCBX_RM_PUT_1_BYTE (pMsg, pu4OffSet, DCBX_CEE_RED_CTRL_INFO);
    DCBX_RM_PUT_2_BYTE (pMsg, pu4OffSet, u2MsgLen);

    /* Fill the Interface, used to get the Ctrl Port Entry */
    DCBX_RM_PUT_4_BYTE (pMsg, pu4OffSet, pCtrlInfo->u4IfIndex);

    DCBX_RM_PUT_4_BYTE (pMsg, pu4OffSet, pCtrlInfo->u4CtrlSeqNo);
    DCBX_RM_PUT_4_BYTE (pMsg, pu4OffSet, pCtrlInfo->u4CtrlAckNo);
    DCBX_RM_PUT_4_BYTE (pMsg, pu4OffSet, pCtrlInfo->u4CtrlRcvdSeqNo);
    DCBX_RM_PUT_4_BYTE (pMsg, pu4OffSet, pCtrlInfo->u4CtrlRcvdAckNo);
    DCBX_RM_PUT_4_BYTE (pMsg, pu4OffSet, pCtrlInfo->u4CtrlAckMissCount);
    /* Counter Info */
    DCBX_RM_PUT_4_BYTE (pMsg, pu4OffSet, pCtrlInfo->u4CtrlTxTLVCount);
    DCBX_RM_PUT_4_BYTE (pMsg, pu4OffSet, pCtrlInfo->u4CtrlRxTLVCount);
    DCBX_RM_PUT_4_BYTE (pMsg, pu4OffSet, pCtrlInfo->u4CtrlRxTLVErrorCount);
    
    DCBX_TRC_ARG6 (DCBX_RED_TRC | DCBX_CONTROL_PLANE_TRC,
              "In func %s: Ctrl Info formed with "
              "u4IfIndex: [%d] u4CtrlSeqNo: [%d] u4CtrlRcvdSeqNo: [%d] "
              "u4CtrlAckNo:[%d] u4CtrlRcvdAckNo:[%d]\n", __func__, 
              pCtrlInfo->u4IfIndex, pCtrlInfo->u4CtrlSeqNo, 
              pCtrlInfo->u4CtrlRcvdSeqNo, pCtrlInfo->u4CtrlAckNo, 
              pCtrlInfo->u4CtrlRcvdAckNo);

    DCBX_TRC_ARG1 (DCBX_RED_TRC | DCBX_CONTROL_PLANE_TRC,
              "Exiting func %s\n", __func__);
    return;
}

/*****************************************************************************
 * Function Name      : DcbxCeeRedProcessBulkCtrlInfoSyncMsg
 *
 * Description        : This function forms the Dynamic Sync Up message for
 *                      Ctrl Information which needs to
 *                      be sent to Standby Node from the Active Node.
 *
 * Input(s)           : pAppPriPortInfo - Application Priority Port Entry
 *                      pMsg - RM Message Structure that needs to be filled
 *                      pu4OffSet - OffSet Value
 *
 * Output(s)          : None
 *
 * Return Value(s)    : None
 *****************************************************************************/
VOID
DcbxCeeRedProcessBulkCtrlInfoSyncMsg (tRmMsg * pMsg,
                      UINT4 *pu4OffSet, UINT2 u2MsgLen)
{
    tCEECtrlEntry       *pCtrlEntry = NULL;
    tRmProtoEvt         ProtoEvt;
    UINT4               u4IfIndex = DCBX_ZERO;


    MEMSET (&ProtoEvt, DCBX_ZERO, sizeof (tRmProtoEvt));

    ProtoEvt.u4AppId = RM_DCBX_APP_ID;
    ProtoEvt.u4Event = RM_BULK_UPDT_ABORT;

    if (u2MsgLen != (DCBX_CEE_RED_CTRL_INFO_VALUE_SIZE +
                DCBX_CEE_RED_CTRL_ALL_COUNTER_INFO_VALUE_SIZE)) 
    {
        ProtoEvt.u4Error = RM_PROCESS_FAIL;

        DcbxPortRmApiHandleProtocolEvent (&ProtoEvt);
        *pu4OffSet += u2MsgLen;

        DCBX_TRC_ARG3 (DCBX_RED_TRC | DCBX_FAILURE_TRC,
                "Message length received [%d] is not equal to [%d] "
                "Exiting func %s\n", u2MsgLen,
                (DCBX_CEE_RED_CTRL_INFO_VALUE_SIZE + \
                 DCBX_CEE_RED_CTRL_ALL_COUNTER_INFO_VALUE_SIZE), __func__);
        return;
    }

    DCBX_RM_GET_4_BYTE (pMsg, pu4OffSet, u4IfIndex);

    pCtrlEntry = CEEUtlGetCtrlEntry (u4IfIndex);
    if (pCtrlEntry == NULL)
    {
        DCBX_TRC_ARG1 ((DCBX_RED_TRC | DCBX_FAILURE_TRC),
                       "In %s: Ctrl Entry Found \n", __FUNCTION__);
        return;
    }

    DCBX_RM_GET_4_BYTE (pMsg, pu4OffSet, pCtrlEntry->u4CtrlSeqNo);
    DCBX_RM_GET_4_BYTE (pMsg, pu4OffSet, pCtrlEntry->u4CtrlAckNo);
    DCBX_RM_GET_4_BYTE (pMsg, pu4OffSet, pCtrlEntry->u4CtrlRcvdSeqNo);
    DCBX_RM_GET_4_BYTE (pMsg, pu4OffSet, pCtrlEntry->u4CtrlRcvdAckNo);
    DCBX_RM_GET_4_BYTE (pMsg, pu4OffSet, pCtrlEntry->u4CtrlAckMissCount);
    /* Get the counters */
    DCBX_RM_GET_4_BYTE (pMsg, pu4OffSet, pCtrlEntry->u4CtrlTxTLVCount);
    DCBX_RM_GET_4_BYTE (pMsg, pu4OffSet, pCtrlEntry->u4CtrlRxTLVCount);
    DCBX_RM_GET_4_BYTE (pMsg, pu4OffSet, pCtrlEntry->u4CtrlRxTLVErrorCount);

    DCBX_TRC_ARG5 ((DCBX_RED_TRC | DCBX_CONTROL_PLANE_TRC),
            "In %s: Received message with \nu4CtrlSeqNo: [%d] "
            "u4CtrlRcvdSeqNo: [%d] \nu4CtrlAckNo: [%d] \nu4CtrlRcvdAckNo: [%d]\n",
            __func__, pCtrlEntry->u4CtrlSeqNo, pCtrlEntry->u4CtrlRcvdSeqNo,
            pCtrlEntry->u4CtrlAckNo, pCtrlEntry->u4CtrlRcvdAckNo);

    DCBX_TRC_ARG1 ((DCBX_RED_TRC | DCBX_CONTROL_PLANE_TRC),
            "Exiting %s\n", __func__);

    return;
}

/****************************************************************************
 * Function Name      : DcbxCeeRedBulkSyncEtsRemInfo                         
 *                                                                           
 * Description        : This function sends the CEE ETS Remote table             
 *                      Bulk Sync up Message to Stadnby Node from            
 *                      the Active Node.                                     
 *                                                                           
 * Input(s)           : None                                                 
 *                                                                           
 * Output(s)          : None                                                 
 *                                                                           
 * Return Value(s)    : None                                                 
 *****************************************************************************/
VOID
DcbxCeeRedBulkSyncEtsRemInfo (VOID)
{
    tETSPortEntry       ETSPortEntry;
    tRmMsg             *pMsg = NULL;
    tETSPortEntry      *pEtsPortInfo = NULL;
    UINT4               u4OffSet = DCBX_ZERO;


    pMsg = RM_ALLOC_TX_BUF (DCBX_DB_MAX_BUF_SIZE);
    if (pMsg == NULL)
    {
        DCBX_TRC (DCBX_RED_TRC | DCBX_RESOURCE_TRC,
                  "DcbxCeeRedBulkSyncEtsRemInfo: Allocation "
                  "of memory from RM Failed\n");
        return;
    }
    
    pEtsPortInfo = (tETSPortEntry *)
        RBTreeGetFirst (gETSGlobalInfo.pRbETSPortTbl);
    if (pEtsPortInfo == NULL)
    {
        DCBX_TRC (DCBX_RED_TRC | DCBX_CONTROL_PLANE_TRC,
                  "DcbxCeeRedBulkSyncEtsRemInfo: ETS entry not present \n");
        RM_FREE (pMsg);
        return;
    }

    while (pEtsPortInfo != NULL)
    {
        DcbxCeeRedFormEtsRemInfoSyncMsg (pEtsPortInfo, pMsg, &u4OffSet);

        if ((DCBX_DB_MAX_BUF_SIZE - u4OffSet) < DCBX_CEE_RED_ETS_INFO_SIZE)
        {
            if (DcbxPortRmEnqMsgToRm (pMsg, (UINT2) u4OffSet,
                                      RM_DCBX_APP_ID,
                                      RM_DCBX_APP_ID) == OSIX_FAILURE)
            {
                DCBX_TRC (DCBX_RED_TRC | DCBX_FAILURE_TRC,
                          "DcbxCeeRedBulkSyncEtsRemInfo: Enqueue "
                          "message to RM Failed\r\n");
                return;
            }

            u4OffSet = DCBX_ZERO;
            
            pMsg = RM_ALLOC_TX_BUF (DCBX_DB_MAX_BUF_SIZE);
            if (pMsg == NULL)
            {
                DCBX_TRC (DCBX_RED_TRC | DCBX_RESOURCE_TRACE,
                          "DcbxCeeRedBulkSyncEtsRemInfo: Allocation "
                          "of memory from RM Failed\n");
                return;
            }
        }

        MEMSET (&ETSPortEntry, DCBX_ZERO, sizeof (tETSPortEntry));
        ETSPortEntry.u4IfIndex = pEtsPortInfo->u4IfIndex;
        pEtsPortInfo = (tETSPortEntry *)
            RBTreeGetNext (gETSGlobalInfo.pRbETSPortTbl,
                           &ETSPortEntry, DcbxUtlPortTblCmpFn);
    }

    DcbxPortRmEnqMsgToRm (pMsg, (UINT2) u4OffSet, RM_DCBX_APP_ID,
                          RM_DCBX_APP_ID);
    return;
}

/****************************************************************************
 * Function Name      : DcbxCeeRedFormEtsRemInfoSyncMsg                         
 *                                                                           
 * Description        : This function forms the Dynamic Sync Up message for  
 *                      CEE ETS Remote table which needs to be sent to Standby   
 *                      Node from the Active Node.                           
 *                                                                           
 * Input(s)           : pEtsPortInfo - ETS Port Entry                        
 *                      pMsg - RM Message Structure that needs to be filled  
 *                      pu4OffSet - OffSet Value                             
 *                                                                           
 * Output(s)          : None                                                 
 *                                                                           
 * Return Value(s)    : None                                                 
 *****************************************************************************/
VOID
DcbxCeeRedFormEtsRemInfoSyncMsg (tETSPortEntry * pEtsPortInfo,
                              tRmMsg * pMsg, UINT4 *pu4OffSet)
{
    UINT2               u2MsgLen = DCBX_ZERO;


    u2MsgLen = (UINT2) DCBX_CEE_RED_ETS_INFO_SIZE; 

    DCBX_RM_PUT_1_BYTE (pMsg, pu4OffSet, DCBX_CEE_RED_ETS_REM_INFO);
    DCBX_RM_PUT_2_BYTE (pMsg, pu4OffSet, u2MsgLen);

    /* Fill the interface */
    DCBX_RM_PUT_4_BYTE (pMsg, pu4OffSet, pEtsPortInfo->u4IfIndex);
    /* Fill the ETS info */
    DCBX_RM_PUT_1_BYTE (pMsg, pu4OffSet, pEtsPortInfo->bETSPeerWilling);
    DCBX_RM_PUT_1_BYTE (pMsg, pu4OffSet, pEtsPortInfo->u1CurrentState);
    DCBX_RM_PUT_1_BYTE (pMsg, pu4OffSet, pEtsPortInfo->bETSSyncd);
    DCBX_RM_PUT_1_BYTE (pMsg, pu4OffSet, pEtsPortInfo->bETSError);
    DCBX_RM_PUT_1_BYTE (pMsg, pu4OffSet, pEtsPortInfo->u1DcbxStatus);

    DCBX_TRC_ARG3 ((DCBX_RED_TRC | DCBX_CONTROL_PLANE_TRC),
            "In %s: Form message with bETSPeerWilling: [%d] Length: [%d]\n", 
            __func__, pEtsPortInfo->bETSPeerWilling, u2MsgLen);

    DCBX_TRC_ARG1 (DCBX_RED_TRC | DCBX_CONTROL_PLANE_TRC,
              "Exit %s\n", __func__);
    return;
}

/***************************************************************************
 * FUNCTION NAME    : DcbxCeeRedProcessDynamicEtsRemInfo
 *
 * DESCRIPTION      : This routine decodes the sync-up message from peer active
 *                    DCBX and updates CEE ETS Reomte 
 *                    entriy in standby DCBX.
 *
 * INPUT            : pMsg - RM Sync-up message
 *                    u2RemMsgLen - Length of value of Message
 *                    
 * OUTPUT           : u4OffSet - Offset value 
 * 
 * RETURNS          : VOID 
 * 
 **************************************************************************/
VOID
DcbxCeeRedProcessDynamicEtsRemInfo (tRmMsg * pMsg, UINT4 *pu4OffSet,
                                    UINT2 u2RemMsgLen)
{
    tETSPortEntry      *pEtsPortInfo = NULL;
    tRmProtoEvt         ProtoEvt;
    UINT4               u4IfIndex = DCBX_ZERO;


    MEMSET (&ProtoEvt, DCBX_ZERO, sizeof (tRmProtoEvt));

    ProtoEvt.u4AppId = RM_DCBX_APP_ID;
    ProtoEvt.u4Event = RM_BULK_UPDT_ABORT;

    if (u2RemMsgLen != DCBX_CEE_RED_ETS_INFO_VALUE_SIZE)
    {
        ProtoEvt.u4Error = RM_PROCESS_FAIL;

        DcbxPortRmApiHandleProtocolEvent (&ProtoEvt);
        *pu4OffSet += u2RemMsgLen;

        DCBX_TRC_ARG3 ((DCBX_RED_TRC | DCBX_FAILURE_TRC),
                "In %s: Message length received [%d] is not equal to [%d]. "
                "Hence return.\n", __func__, u2RemMsgLen, 
                DCBX_CEE_RED_ETS_INFO_VALUE_SIZE);
        return;
    }

    DCBX_RM_GET_4_BYTE (pMsg, pu4OffSet, u4IfIndex);

    pEtsPortInfo = ETSUtlGetPortEntry (u4IfIndex);
    if (pEtsPortInfo == NULL)
    {
        DCBX_TRC_ARG1 (DCBX_RED_TRC | DCBX_FAILURE_TRC,
                  "In func %s: ETS Entry Not Found.\n", __func__);
        return;
    }

    DCBX_RM_GET_1_BYTE (pMsg, pu4OffSet, pEtsPortInfo->bETSPeerWilling);
    DCBX_RM_GET_1_BYTE (pMsg, pu4OffSet, pEtsPortInfo->u1CurrentState);
    DCBX_RM_GET_1_BYTE (pMsg, pu4OffSet, pEtsPortInfo->bETSSyncd);
    DCBX_RM_GET_1_BYTE (pMsg, pu4OffSet, pEtsPortInfo->bETSError);
    DCBX_RM_GET_1_BYTE (pMsg, pu4OffSet, pEtsPortInfo->u1DcbxStatus);

    DCBX_TRC_ARG2 (DCBX_RED_TRC | DCBX_CONTROL_PLANE_TRC,
            "In func %s: Message received with bETSPeerWilling: [%d].\n", 
            __func__, pEtsPortInfo->bETSPeerWilling);

    DCBX_TRC_ARG1 (DCBX_RED_TRC | DCBX_FAILURE_TRC,
            "Exit func %s.\n", __func__);
    return;
}

/****************************************************************************
 * Function Name      : DcbxCeeRedBulkSyncPfcRemInfo                            
 *                                                                           
 * Description        : This function sends the CEE PFC Remote table        
 *                      Bulk Sync up Message to Stadnby Node from            
 *                      the Active Node.                                     
 *                                                                           
 * Input(s)           : None                                                 
 *                                                                           
 * Output(s)          : None                                                 
 *                                                                           
 * Return Value(s)    : None                                                 
 *****************************************************************************/
VOID
DcbxCeeRedBulkSyncPfcRemInfo (VOID)
{
    tPFCPortEntry       PFCPortEntry;
    tRmMsg             *pMsg = NULL;
    tPFCPortEntry      *pPfcPortInfo = NULL;
    UINT4               u4OffSet = DCBX_ZERO;



    pMsg = RM_ALLOC_TX_BUF (DCBX_DB_MAX_BUF_SIZE);
    if (pMsg == NULL)
    {
        DCBX_TRC_ARG1 (DCBX_RED_TRC | DCBX_RESOURCE_TRC,
                  "In func %s: Allocation "
                  "of memory from RM Failed\n", __func__);
        return;
    }

    pPfcPortInfo = (tPFCPortEntry *)
        RBTreeGetFirst (gPFCGlobalInfo.pRbPFCPortTbl);
    if (pPfcPortInfo == NULL)
    {
        DCBX_TRC_ARG1 (DCBX_RED_TRC | DCBX_FAILURE_TRC,
                  "In func %s: PFC entry not present\n", __func__);
        RM_FREE (pMsg);
        return;
    }

    while (pPfcPortInfo != NULL)
    {
        DcbxCeeRedFormPfcRemInfoSyncMsg (pPfcPortInfo, pMsg, &u4OffSet);

        if ((DCBX_DB_MAX_BUF_SIZE - u4OffSet) < DCBX_CEE_RED_PFC_INFO_SIZE)
        {
            if (DcbxPortRmEnqMsgToRm (pMsg, (UINT2) u4OffSet,
                                      RM_DCBX_APP_ID,
                                      RM_DCBX_APP_ID) == OSIX_FAILURE)
            {
                DCBX_TRC_ARG1 (DCBX_RED_TRC | DCBX_FAILURE_TRC,
                          "In func %s: Enqueue "
                          "message to RM Failed\r\n", __func__);
                return;
            }

            u4OffSet = DCBX_ZERO;

            pMsg = RM_ALLOC_TX_BUF (DCBX_DB_MAX_BUF_SIZE);
            if (pMsg == NULL)
            {
                DCBX_TRC_ARG1 (DCBX_RED_TRC | DCBX_RESOURCE_TRC,
                          "In func %s: Allocation "
                          "of memory from RM Failed\n", __func__);
                return;
            }
        }

        MEMSET (&PFCPortEntry, DCBX_ZERO, sizeof (tPFCPortEntry));
        PFCPortEntry.u4IfIndex = pPfcPortInfo->u4IfIndex;
        pPfcPortInfo = (tPFCPortEntry *)
            RBTreeGetNext (gPFCGlobalInfo.pRbPFCPortTbl,
                           &PFCPortEntry, DcbxUtlPortTblCmpFn);
    }

    DcbxPortRmEnqMsgToRm (pMsg, (UINT2) u4OffSet, RM_DCBX_APP_ID,
                          RM_DCBX_APP_ID);
    return;
}

/****************************************************************************
 * Function Name      : DcbxCeeRedFormPfcRemInfoSyncMsg                         
 *                                                                           
 * Description        : This function forms the Dynamic Sync Up message for  
 *                      CEE PFC Remote table which needs to be sent to Standby   
 *                      Node from the Active Node.                           
 *                                                                           
 * Input(s)           : pPfcPortInfo - PFC Port Entry                        
 *                      pMsg - RM Message Structure that needs to be filled  
 *                      pu4OffSet - OffSet Value                             
 *                                                                           
 * Output(s)          : None                                                 
 *                                                                           
 * Return Value(s)    : None                                                 
 *****************************************************************************/
VOID
DcbxCeeRedFormPfcRemInfoSyncMsg (tPFCPortEntry * pPfcPortInfo,
                              tRmMsg * pMsg, UINT4 *pu4OffSet)
{
    UINT2               u2MsgLen = DCBX_ZERO;

    u2MsgLen = (UINT2) (DCBX_CEE_RED_PFC_INFO_SIZE);


    /* Fill the message Type and Length */
    DCBX_RM_PUT_1_BYTE (pMsg, pu4OffSet, DCBX_CEE_RED_PFC_REM_INFO);
    DCBX_RM_PUT_2_BYTE (pMsg, pu4OffSet, u2MsgLen);

    /* Fill the Interface Index */
    DCBX_RM_PUT_4_BYTE (pMsg, pu4OffSet, pPfcPortInfo->u4IfIndex);
    /* Fill the PFC Peer Willing bit */
    DCBX_RM_PUT_1_BYTE (pMsg, pu4OffSet,
                        pPfcPortInfo->bPFCPeerWilling);
     /* Fill the DcbxStatus*/
    DCBX_RM_PUT_1_BYTE (pMsg, pu4OffSet, pPfcPortInfo->u1DcbxStatus);
    DCBX_RM_PUT_1_BYTE (pMsg, pu4OffSet, pPfcPortInfo->u1CurrentState);
    DCBX_RM_PUT_1_BYTE (pMsg, pu4OffSet, pPfcPortInfo->bPFCSyncd);
    DCBX_RM_PUT_1_BYTE (pMsg, pu4OffSet, pPfcPortInfo->bPFCError);
   
    DCBX_TRC_ARG2 ((DCBX_RED_TRC | DCBX_CONTROL_PLANE_TRC),
            "In %s: Formed message for DCBX_CEE_RED_PFC_REM_INFO "
            "bPFCPeerWilling: [%d]\n", __func__, 
            pPfcPortInfo->bPFCPeerWilling);

    DCBX_TRC_ARG1 (DCBX_RED_TRC | DCBX_CONTROL_PLANE_TRC, "Exiting %s", 
            __func__);
    return;
}

/***************************************************************************
 * FUNCTION NAME    : DcbxCeeRedProcessDynamicPfcRemInfo 
 *
 * DESCRIPTION      : This routine decodes the sync-up message from peer active
 *                    DCBX and updates CEE PFC Reomte info
 *                    in standby DCBX.
 *
 * INPUT            : pMsg - RM Sync-up message
 *                    u2RemMsgLen - Length of value of Message
 *                    
 * OUTPUT           : u4OffSet - Offset value 
 * 
 * RETURNS          : VOID 
 * 
 **************************************************************************/
VOID
DcbxCeeRedProcessDynamicPfcRemInfo (tRmMsg * pMsg, UINT4 *pu4OffSet,
                                    UINT2 u2RemMsgLen)
{
    tPFCPortEntry      *pPfcPortInfo = NULL;
    tRmProtoEvt         ProtoEvt;
    UINT4               u4IfIndex = DCBX_ZERO;


    MEMSET (&ProtoEvt, DCBX_ZERO, sizeof (tRmProtoEvt));

    ProtoEvt.u4AppId = RM_DCBX_APP_ID;
    ProtoEvt.u4Event = RM_BULK_UPDT_ABORT;

    if (u2RemMsgLen != DCBX_CEE_RED_PFC_INFO_VALUE_SIZE)
    {
        ProtoEvt.u4Error = RM_PROCESS_FAIL;

        DcbxPortRmApiHandleProtocolEvent (&ProtoEvt);
        *pu4OffSet += (UINT4) u2RemMsgLen;

        DCBX_TRC_ARG3 (DCBX_RED_TRC | DCBX_FAILURE_TRC,
                "In %s: Message length received [%d] is not equal to [%d]\n",
                __func__, u2RemMsgLen, DCBX_CEE_RED_PFC_INFO_VALUE_SIZE);
        return;
    }

    DCBX_RM_GET_4_BYTE (pMsg, pu4OffSet, u4IfIndex);

    pPfcPortInfo = PFCUtlGetPortEntry (u4IfIndex);
    if (pPfcPortInfo == NULL)
    {
        DCBX_TRC_ARG1 (DCBX_RED_TRC | DCBX_FAILURE_TRC,
                  "In func %s: PFC Entry not Found. Return.\n", __func__);
        return;
    }

    DCBX_RM_GET_1_BYTE (pMsg, pu4OffSet, pPfcPortInfo->bPFCPeerWilling);
    DCBX_RM_GET_1_BYTE (pMsg, pu4OffSet, pPfcPortInfo->u1DcbxStatus);
    DCBX_RM_GET_1_BYTE (pMsg, pu4OffSet, pPfcPortInfo->u1CurrentState);
    DCBX_RM_GET_1_BYTE (pMsg, pu4OffSet, pPfcPortInfo->bPFCSyncd);
    DCBX_RM_GET_1_BYTE (pMsg, pu4OffSet, pPfcPortInfo->bPFCError);

    DCBX_TRC_ARG2 (DCBX_RED_TRC | DCBX_CONTROL_PLANE_TRC,
            "In func %s: Message Received with bPFCPeerWilling: [%d].\n",
            __func__, pPfcPortInfo->bPFCPeerWilling);

    DCBX_TRC_ARG1 (DCBX_RED_TRC | DCBX_FAILURE_TRC,
            "Exiting func %s.\n", __func__);
    return;
}

/****************************************************************************
 * Function Name      : DcbxCeeRedBulkSyncAppPriRemInfo                         
 *                                                                           
 * Description        : This function sends the CEE Application Priority         
 *                      Remote table Bulk Sync up Message to Stadnby Node from
 *                      the Active Node.                                     
 *                                                                           
 * Input(s)           : None                                                 
 *                                                                           
 * Output(s)          : None                                                 
 *                                                                           
 * Return Value(s)    : None                                                 
 *****************************************************************************/
VOID
DcbxCeeRedBulkSyncAppPriRemInfo (VOID)
{
    tRmMsg             *pMsg = NULL;
    tAppPriPortEntry   *pAppPriPortInfo;
    tAppPriPortEntry    AppPriPortEntry;
    UINT4               u4OffSet = DCBX_ZERO;


    pMsg = RM_ALLOC_TX_BUF (DCBX_DB_MAX_BUF_SIZE);
    if (pMsg == NULL)
    {
        DCBX_TRC_ARG1 (DCBX_RED_TRC | DCBX_RESOURCE_TRC,
                       "In %s: Allocation "
                       "of memory from RM Failed\n", __FUNCTION__);
        return;
    }

    /* send sync up message(s) only if application priority is configured 
     * on atleast one port */
    pAppPriPortInfo = (tAppPriPortEntry *)
        RBTreeGetFirst (gAppPriGlobalInfo.pRbAppPriPortTbl);
    if (pAppPriPortInfo == NULL)
    {
        DCBX_TRC_ARG1 ((DCBX_RED_TRC | DCBX_FAILURE_TRC),
                       "In %s: AppPriPortEntry not found" "\r\n", __FUNCTION__);
        RM_FREE (pMsg);
        return;
    }

    /* For each port, form a RM message to carry remote application Priority
     * information on that port and enqueue that message on RM queue */
    while (pAppPriPortInfo != NULL)
    {
        DcbxCeeRedFormAppPriRemInfoSyncMsg (pAppPriPortInfo, pMsg, &u4OffSet);

        if ((DCBX_DB_MAX_BUF_SIZE - u4OffSet) <
                (DCBX_CEE_RED_APP_PRI_INFO_SIZE))
        {
            if (DcbxPortRmEnqMsgToRm (pMsg, (UINT2) u4OffSet,
                                      RM_DCBX_APP_ID,
                                      RM_DCBX_APP_ID) == OSIX_FAILURE)
            {
                DCBX_TRC_ARG1 ((DCBX_RED_TRC | DCBX_FAILURE_TRC),
                               "In %s: Enqueue "
                               "message to RM Failed\r\n", __FUNCTION__);
                return;
            }

            u4OffSet = DCBX_ZERO;

            pMsg = RM_ALLOC_TX_BUF (DCBX_DB_MAX_BUF_SIZE);
            if (pMsg == NULL)
            {
                DCBX_TRC_ARG1 ((DCBX_RED_TRC | DCBX_RESOURCE_TRC),
                               "In func %s: Allocation "
                               "of memory from RM Failed\n", __FUNCTION__);
                return;
            }
        }

        MEMSET (&AppPriPortEntry, DCBX_ZERO, sizeof (tAppPriPortEntry));
        AppPriPortEntry.u4IfIndex = pAppPriPortInfo->u4IfIndex;
        pAppPriPortInfo = (tAppPriPortEntry *)
            RBTreeGetNext (gAppPriGlobalInfo.pRbAppPriPortTbl,
                           &AppPriPortEntry, AppPriUtlPortTblCmpFn);
    }

    DcbxPortRmEnqMsgToRm (pMsg, (UINT2) u4OffSet, RM_DCBX_APP_ID,
                          RM_DCBX_APP_ID);
    return;
}

/****************************************************************************
 * Function Name      : DcbxCeeRedFormAppPriRemInfoSyncMsg                   
 *                                                                           
 * Description        : This function forms the Dynamic Sync Up message for  
 *                      CEE Application Priority Remote table which needs to be
 *                      sent to Standby Node from the Active Node.           
 *                                                                           
 * Input(s)           : pAppPriPortInfo - Application Priority Port Entry    
 *                      pMsg - RM Message Structure that needs to be filled  
 *                      pu4OffSet - OffSet Value                             
 *                                                                           
 * Output(s)          : None                                                 
 *                                                                           
 * Return Value(s)    : None                                                 
 *****************************************************************************/
VOID
DcbxCeeRedFormAppPriRemInfoSyncMsg (tAppPriPortEntry *pAppPriPortInfo,
                                 tRmMsg *pMsg, UINT4 *pu4OffSet)
{
    UINT2               u2MsgLen = DCBX_ZERO;


    u2MsgLen = (UINT2) (DCBX_CEE_RED_APP_PRI_INFO_SIZE);

    /* Fill the MSG Type and Length */
    DCBX_RM_PUT_1_BYTE (pMsg, pu4OffSet, DCBX_CEE_RED_APP_PRI_REM_INFO);
    DCBX_RM_PUT_2_BYTE (pMsg, pu4OffSet, u2MsgLen);

    /* Fill the Interface Index */
    DCBX_RM_PUT_4_BYTE (pMsg, pu4OffSet, pAppPriPortInfo->u4IfIndex);
    /* Fill the AppPriPeerWilling */
    DCBX_RM_PUT_1_BYTE (pMsg, pu4OffSet,
                        pAppPriPortInfo->bAppPriPeerWilling);
    /* Fill the DcbxStatus*/
    DCBX_RM_PUT_1_BYTE (pMsg, pu4OffSet, pAppPriPortInfo->u1DcbxStatus);
    DCBX_RM_PUT_1_BYTE (pMsg, pu4OffSet, pAppPriPortInfo->u1CurrentState);
    DCBX_RM_PUT_1_BYTE (pMsg, pu4OffSet, pAppPriPortInfo->bAppPriSyncd);
    DCBX_RM_PUT_1_BYTE (pMsg, pu4OffSet, pAppPriPortInfo->bAppPriError);
    
    DCBX_TRC_ARG3 (DCBX_RED_TRC | DCBX_CONTROL_PLANE_TRC, 
                "In %s: Form message with bAppPriPeerWilling[%d] u2MsgLen[%d].\n",
                __func__, pAppPriPortInfo->bAppPriPeerWilling, u2MsgLen);

    DCBX_TRC_ARG1 ((DCBX_RED_TRC | DCBX_CONTROL_PLANE_TRC),
                   "Exit %s\n", __FUNCTION__);
    return;
}

/***************************************************************************
 * FUNCTION NAME    : DcbxCeeRedProcessDynamicAppPriAppRemInfo
 *
 * DESCRIPTION      : This routine decodes the sync-up message from peer active
 *                    DCBX and updates Application Priority Reomte 
 *                    Application Priority Maping entries 
 *                    in standby DCBX.
 *
 * INPUT            : pMsg - RM Sync-up message
 *                    u2RemMsgLen - Length of value of Message
 *                    
 * OUTPUT           : u4OffSet - Offset value 
 * 
 * RETURNS          : VOID 
 * 
 **************************************************************************/
VOID
DcbxCeeRedProcessDynamicAppPriRemInfo (tRmMsg * pMsg, UINT4 *pu4OffSet,
                                    UINT2 u2RemMsgLen)
{
    tAppPriPortEntry   *pAppPriPortInfo = NULL;
    tRmProtoEvt         ProtoEvt;
    UINT4               u4IfIndex = DCBX_ZERO;


    MEMSET (&ProtoEvt, DCBX_ZERO, sizeof (tRmProtoEvt));

    ProtoEvt.u4AppId = RM_DCBX_APP_ID;
    ProtoEvt.u4Event = RM_BULK_UPDT_ABORT;

    if (u2RemMsgLen < DCBX_CEE_RED_APP_PRI_INFO_VALUE_SIZE)
    {
        ProtoEvt.u4Error = RM_PROCESS_FAIL;
        DcbxPortRmApiHandleProtocolEvent (&ProtoEvt);
        *pu4OffSet += u2RemMsgLen;

        DCBX_TRC_ARG3 ((DCBX_RED_TRC | DCBX_FAILURE_TRC),
                "In %s: Message length received [%d] is not equal to [%d]. "
                "Hence return.\n", __func__, u2RemMsgLen, 
                DCBX_CEE_RED_APP_PRI_INFO_VALUE_SIZE);
        return;
    }

    DCBX_RM_GET_4_BYTE (pMsg, pu4OffSet, u4IfIndex);

    pAppPriPortInfo = AppPriUtlGetPortEntry (u4IfIndex);
    if (pAppPriPortInfo == NULL)
    {
        DCBX_TRC_ARG1 ((DCBX_RED_TRC | DCBX_FAILURE_TRC),
                       "In %s: AppPriUtlGetPortEntry not found.\n",
                       __func__);
        return;
    }

    DCBX_RM_GET_1_BYTE (pMsg, pu4OffSet,
                        pAppPriPortInfo->bAppPriPeerWilling);
    DCBX_RM_GET_1_BYTE (pMsg, pu4OffSet, pAppPriPortInfo->u1DcbxStatus);
    DCBX_RM_GET_1_BYTE (pMsg, pu4OffSet, pAppPriPortInfo->u1CurrentState);
    DCBX_RM_GET_1_BYTE (pMsg, pu4OffSet, pAppPriPortInfo->bAppPriSyncd);
    DCBX_RM_GET_1_BYTE (pMsg, pu4OffSet, pAppPriPortInfo->bAppPriError);

    DCBX_TRC_ARG2 (DCBX_RED_TRC | DCBX_CONTROL_PLANE_TRC, 
            "In %s: Received message with bAppPriPeerWilling[%d].\n",
                       __func__, pAppPriPortInfo->bAppPriPeerWilling);

    DCBX_TRC_ARG1 ((DCBX_RED_TRC | DCBX_CONTROL_PLANE_TRC),
                   "Exit %s\n", __FUNCTION__);
    return;
}
/************ END of BULK UPDATE SYNC & PROCESSING ************/

/****************************************************************************
 * Function Name      : DcbxCeeRedSendDynSyncAgeOutInfo
 *                                                                           
 * Description        : This function sends the AgeOut Info 
 *                      Dynamic Sync up Message to Stadnby Node from         
 *                      the Active Node.                                     
 *                                                                           
 * Input(s)           : u4IfIndex - Interface Index 
 *                                                                           
 * Output(s)          : None                                                 
 *                                                                           
 * Return Value(s)    : None                                                 
 *****************************************************************************/
VOID
DcbxCeeRedSendDynSyncAgeOutInfo  (UINT4 u4IfIndex)
{
    tRmMsg             *pMsg = NULL;
    UINT4               u4OffSet = DCBX_ZERO;
    UINT2               u2MsgLen = DCBX_ZERO;


    if ((DCBX_RED_NODE_STATUS() == RM_STANDBY) ||
        (DCBX_RED_STBY_NODE_UP_COUNT() == DCBX_ZERO))
    {
        DCBX_TRC_ARG1 (DCBX_RED_TRC | DCBX_CONTROL_PLANE_TRC,
                  "%s: No Need to send Sync "
                  "message as Standby is down or the node is not Active\n", 
                  __func__);
        return;
    }

    pMsg = RM_ALLOC_TX_BUF (DCBX_CEE_RED_ALL_FEAT_AGEOUT_INFO_SIZE);
    if (pMsg == NULL)
    {
        DCBX_TRC_ARG1 (DCBX_RED_TRC | DCBX_FAILURE_TRC,
                  "%s: Allocation "
                  "of memory from RM Failed\n", __func__);
        return;
    }

    DCBX_TRC_ARG1 (DCBX_RED_TRC | DCBX_CONTROL_PLANE_TRC,
              "In %s: calling DcbxCeeRedFormEtsInfoSyncMsg\n", __func__);
    
    u2MsgLen = DCBX_CEE_RED_ALL_FEAT_AGEOUT_INFO_SIZE; 

    DCBX_RM_PUT_1_BYTE (pMsg, &u4OffSet, DCBX_CEE_RED_ALL_FEAT_AGE_OUT_INFO);
    DCBX_RM_PUT_2_BYTE (pMsg, &u4OffSet, u2MsgLen);
    DCBX_RM_PUT_4_BYTE (pMsg, &u4OffSet, u4IfIndex);

    DCBX_TRC_ARG2 (DCBX_RED_TRC | DCBX_CONTROL_PLANE_TRC,
              "In %s: Post Msg to RM. u4IfIndex %d\n", __func__, u4IfIndex);
    /* Post MSG to RM */
    DcbxPortRmEnqMsgToRm (pMsg, (UINT2) u4OffSet,
                          RM_DCBX_APP_ID, RM_DCBX_APP_ID);

    DCBX_TRC_ARG1 (DCBX_RED_TRC | DCBX_CONTROL_PLANE_TRC,
              "Exit %s\n", __func__);
    return;
}

/***************************************************************************
 * FUNCTION NAME    : DcbxCeeRedProcessDynSyncAgeOutInfo 
 *
 * DESCRIPTION      : This routine decodes the sync-up message from peer active
 *                    DCBX and updates CEE entry in standby DCBX.
 *
 * INPUT            : pMsg - RM Sync-up message
 *                    u2RemMsgLen - Length of value of Message
 *                    
 * OUTPUT           : u4OffSet - Offset value 
 * 
 * RETURNS          : VOID 
 * 
 **************************************************************************/
VOID
DcbxCeeRedProcessDynSyncAgeOutInfo (tRmMsg * pMsg, UINT4 *pu4OffSet,
                                    UINT2 u2RemMsgLen)
{
    tRmProtoEvt         ProtoEvt;
    UINT4               u4IfIndex = DCBX_ZERO;


    MEMSET (&ProtoEvt, DCBX_ZERO, sizeof (tRmProtoEvt));

    ProtoEvt.u4AppId = RM_DCBX_APP_ID;
    ProtoEvt.u4Event = RM_BULK_UPDT_ABORT;

    if (u2RemMsgLen != DCBX_CEE_RED_INTF_LEN)
    {
        ProtoEvt.u4Error = RM_PROCESS_FAIL;

        DcbxPortRmApiHandleProtocolEvent (&ProtoEvt);
        *pu4OffSet += u2RemMsgLen;

        DCBX_TRC_ARG3 ((DCBX_RED_TRC | DCBX_FAILURE_TRC),
                "In %s: Message length received [%d] is not equal to [%d]. "
                "Hence return.\n", __func__, u2RemMsgLen, 
                DCBX_CEE_RED_INTF_LEN);
        return;
    }

    DCBX_RM_GET_4_BYTE (pMsg, pu4OffSet, u4IfIndex);

    DCBX_TRC_ARG2 (DCBX_RED_TRC | DCBX_FAILURE_TRC,
            "In func %s - Received Interface %d\n", __func__, u4IfIndex);

    CEEUtlHandleAgedOutFromDCBX (u4IfIndex);

    DCBX_TRC_ARG1 (DCBX_RED_TRC | DCBX_FAILURE_TRC,
            "Exit func %s.\n", __func__);
    return;
}

/*****************************************************************************/
/* Function Name      : DcbxRedSendDynamicDcbxStatusInfo                     */
/*                                                                           */
/* Description        : This function sends the DCBX status information      */
/*                      Dynamic Sync up Message to Stadnby Node from         */
/*                      the Active Node.                                     */
/*                                                                           */
/* Input(s)           : u1MsgType                                            */
/*                      u4IfIndex                                            */
/*                      u1DcbxStatus                                         */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
DcbxRedSendDynamicDcbxStatusInfo (UINT1 u1ApplType, UINT4 u4IfIndex, UINT1 u1DcbxStatus)
{
    tRmMsg             *pMsg = NULL;
    UINT4               u4OffSet = DCBX_ZERO;
    UINT2               u2MsgLen = DCBX_ZERO;

    u2MsgLen = DCBX_RED_TYPE_FIELD_SIZE + DCBX_RED_LEN_FIELD_SIZE +
        DCBX_RED_DCBX_STATUS_INFO_VALUE_SIZE;

    if ((DCBX_RED_NODE_STATUS() == RM_STANDBY) ||
        (DCBX_RED_STBY_NODE_UP_COUNT() == DCBX_ZERO))
    {
        DCBX_TRC (DCBX_RED_TRC | DCBX_CONTROL_PLANE_TRC,
                  "DcbxRedSendDynamicDcbxStatusInfo: No Need to send Sync"
                  "Message as Standby is down or the node is Active\n");
        return;
    }

    pMsg = RM_ALLOC_TX_BUF (DCBX_RED_DCBX_STATUS_INFO_SIZE);
    if (pMsg == NULL)
    {
        DCBX_TRC (DCBX_RED_TRC | DCBX_RESOURCE_TRACE,
                  "DcbxRedSendDynamicDcbxStatusInfo: Allocation "
                  "of memory from RM Failed\n");
        return;
    }

    DCBX_RM_PUT_1_BYTE (pMsg, &u4OffSet, (UINT1) DCBX_RED_DCBX_STATUS_INFO);
    DCBX_RM_PUT_2_BYTE (pMsg, &u4OffSet, u2MsgLen);
    DCBX_RM_PUT_1_BYTE (pMsg, &u4OffSet, u1ApplType); /* PFC/ETS/AP */
    DCBX_RM_PUT_4_BYTE (pMsg, &u4OffSet, u4IfIndex);
    DCBX_RM_PUT_1_BYTE (pMsg, &u4OffSet, u1DcbxStatus);

    DcbxPortRmEnqMsgToRm (pMsg, (UINT2) u4OffSet,
                          RM_DCBX_APP_ID, RM_DCBX_APP_ID);
    return;
}

/***************************************************************************
 * FUNCTION NAME    : DcbxRedProcessDynamicDcbxStatusInfo
 *
 * DESCRIPTION      : This routine decodes the sync-up message from peer active
 *                    DCBX and updates the DCBX status information in standby DCBX.
 *
 * INPUT            : pMsg - RM Sync-up message
 *                    u2RemMsgLen - Length of value of Message
 *
 * OUTPUT           : u4OffSet - Offset value
 *
 * RETURNS          : NONE
 *
 **************************************************************************/
VOID
DcbxRedProcessDynamicDcbxStatusInfo (tRmMsg * pMsg, UINT4 *pu4OffSet,
                                 UINT2 u2RemMsgLen)
{
    tETSPortEntry      *pEtsPortInfo = NULL;
    tPFCPortEntry      *pPfcPortInfo = NULL;
    tAppPriPortEntry   *pAppPriPortInfo = NULL;
    tRmProtoEvt         ProtoEvt;
    UINT4               u4IfIndex = DCBX_ZERO;
    UINT1               u1DcbxStatus = DCBX_ZERO;
    UINT1               u1ApplType = DCBX_ZERO;

    MEMSET (&ProtoEvt, DCBX_ZERO, sizeof (tRmProtoEvt));

    ProtoEvt.u4AppId = RM_DCBX_APP_ID;
    ProtoEvt.u4Event = RM_BULK_UPDT_ABORT;

    if (u2RemMsgLen != DCBX_RED_DCBX_STATUS_INFO_VALUE_SIZE)
    {
        ProtoEvt.u4Error = RM_PROCESS_FAIL;

        DcbxPortRmApiHandleProtocolEvent (&ProtoEvt);
        *pu4OffSet += u2RemMsgLen;
        return;
    }

    DCBX_RM_GET_1_BYTE (pMsg, pu4OffSet, u1ApplType);
    DCBX_RM_GET_4_BYTE (pMsg, pu4OffSet, u4IfIndex);
    DCBX_RM_GET_1_BYTE (pMsg, pu4OffSet, u1DcbxStatus);

    switch (u1ApplType)
    {
        case DCBX_RED_ETS_DCBX_STATUS_INFO:
            pEtsPortInfo = ETSUtlGetPortEntry (u4IfIndex);
            if (pEtsPortInfo == NULL)
            {
                DCBX_TRC_ARG2 (DCBX_RED_TRC | DCBX_FAILURE_TRC,
                        "%s: ETS Entry not Found on port %d\n", __func__, u4IfIndex);
                return;
            }
            pEtsPortInfo->u1DcbxStatus = u1DcbxStatus;
            break;
        case DCBX_RED_PFC_DCBX_STATUS_INFO:
            pPfcPortInfo = PFCUtlGetPortEntry (u4IfIndex);
            if (pPfcPortInfo == NULL)
            {
                DCBX_TRC_ARG2 (DCBX_RED_TRC | DCBX_FAILURE_TRC,
                        "%s: PFC Entry not Found on port %d\n", __func__, u4IfIndex);
                return;
            }
            pPfcPortInfo->u1DcbxStatus = u1DcbxStatus;
            break;
        case DCBX_RED_APP_PRI_DCBX_STATUS_INFO:
            pAppPriPortInfo = AppPriUtlGetPortEntry (u4IfIndex);
            if (pAppPriPortInfo == NULL)
            {
                DCBX_TRC_ARG2 (DCBX_RED_TRC | DCBX_FAILURE_TRC,
                        "%s: AppPri Entry not Found on port %d\n", __func__, u4IfIndex);
                return;
            }
            pAppPriPortInfo->u1DcbxStatus = u1DcbxStatus;
            break;
        default:
            DCBX_TRC_ARG2 (DCBX_RED_TRC | DCBX_FAILURE_TRC,
                    "%s: Wrong application received on port %d\n", __func__, u4IfIndex);
            return;
    }

    DCBX_TRC_ARG3 (DCBX_RED_TRC, "%s:"
                   " IfIndex = %d u1DcbxStatus = %d\n", __func__, u4IfIndex, u1DcbxStatus);

    return;
}

#endif
