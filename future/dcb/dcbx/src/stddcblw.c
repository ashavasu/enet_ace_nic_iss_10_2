/********************************************************************
 * Copyright (C) 2010 Aricent Inc . All Rights Reserved
 *
 * $Id: stddcblw.c,v 1.27 2017/12/06 09:29:08 siva Exp $
 *
 * Description: Protocol Low Level Routines
 *********************************************************************/
# include  "dcbxinc.h"
/* LOW LEVEL Routines for Table : LldpXdot1dcbxConfigETSConfigurationTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceLldpXdot1dcbxConfigETSConfigurationTable
 Input       :  The Indices
                LldpV2PortConfigIfIndex
                LldpV2PortConfigDestAddressIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

PRIVATE INT4
 
 
 
 DcbxRemETSCompareFunction (UINT4 u4RemTimeMark1, INT4 i4LocIfIndex1,
                            UINT4 u4RemMacIndex1, INT4 i4RemIfIndex1,
                            UINT4 u4RemTimeMark2, INT4 i4LocIfIndex2,
                            UINT4 u4RemMacIndex2, INT4 i4RemIfIndex2);

INT1
nmhValidateIndexInstanceLldpXdot1dcbxConfigETSConfigurationTable (INT4
                                                                  i4LldpV2PortConfigIfIndex,
                                                                  UINT4
                                                                  u4LldpV2PortConfigDestAddressIndex)
{
    tETSPortEntry      *pETSPortEntry = NULL;

    ETS_SHUTDOWN_CHECK;

    DCBX_TRC_ARG3 (DCBX_MGMT_TRC, "%s, Args: "
                   "i4LldpV2PortConfigIfIndex: %d u4LldpV2PortConfigDestAddressIndex: "
                   "%d \r\n", __func__, i4LldpV2PortConfigIfIndex,
                   u4LldpV2PortConfigDestAddressIndex);

    pETSPortEntry = ETSUtlGetPortEntry ((UINT4) i4LldpV2PortConfigIfIndex);
    if (pETSPortEntry == NULL)
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : ETSUtlGetPortEntry() "
                       "Returns NULL. \r\n", __FUNCTION__);
        return SNMP_FAILURE;
    }

    /* Destination Address Index will always be 1.
     *      * LLDP can not assign more than 1 to this Index.
     *           */
    if (u4LldpV2PortConfigDestAddressIndex != DCBX_DEST_ADDR_INDEX)
    {
        DCBX_TRC (DCBX_MGMT_TRC, WRONG_VAL);
        return (SNMP_FAILURE);
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFirstIndexLldpXdot1dcbxConfigETSConfigurationTable
 Input       :  The Indices
                LldpV2PortConfigIfIndex
                LldpV2PortConfigDestAddressIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexLldpXdot1dcbxConfigETSConfigurationTable (INT4
                                                          *pi4LldpV2PortConfigIfIndex,
                                                          UINT4
                                                          *pu4LldpV2PortConfigDestAddressIndex)
{
    tETSPortEntry      *pETSPortEntry = NULL;

    ETS_SHUTDOWN_CHECK;

    DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "%s\r\n", __func__);

    pETSPortEntry = (tETSPortEntry *)
        RBTreeGetFirst (gETSGlobalInfo.pRbETSPortTbl);
    if (pETSPortEntry == NULL)
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : RbTreeGetFirst Node () "
                       "Returns NULL. \r\n", __FUNCTION__);
        return SNMP_FAILURE;
    }

    *pi4LldpV2PortConfigIfIndex = (INT4) pETSPortEntry->u4IfIndex;
    *pu4LldpV2PortConfigDestAddressIndex = DCBX_DEST_ADDR_INDEX;
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetNextIndexLldpXdot1dcbxConfigETSConfigurationTable
 Input       :  The Indices
                LldpV2PortConfigIfIndex
                nextLldpV2PortConfigIfIndex
                LldpV2PortConfigDestAddressIndex
                nextLldpV2PortConfigDestAddressIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexLldpXdot1dcbxConfigETSConfigurationTable (INT4
                                                         i4LldpV2PortConfigIfIndex,
                                                         INT4
                                                         *pi4NextLldpV2PortConfigIfIndex,
                                                         UINT4
                                                         u4LldpV2PortConfigDestAddressIndex,
                                                         UINT4
                                                         *pu4NextLldpV2PortConfigDestAddressIndex)
{
    tETSPortEntry      *pETSPortEntry = NULL;
    tETSPortEntry       ETSPortEntry;
    INT4                i4IfIndex = i4LldpV2PortConfigIfIndex;
    UINT4               u4AddrIndex = u4LldpV2PortConfigDestAddressIndex;

    ETS_SHUTDOWN_CHECK;

    DCBX_TRC_ARG3 (DCBX_MGMT_TRC, "%s, Args: i4LldpV2PortConfigIfIndex: %d "
                   "u4LldpV2PortConfigDestAddressIndex: %d\r\n",
                   __func__, i4LldpV2PortConfigIfIndex,
                   u4LldpV2PortConfigDestAddressIndex);

    pETSPortEntry = ETSUtlGetPortEntry ((UINT4) i4IfIndex);

    if ((pETSPortEntry == NULL) || (DCBX_ZERO < u4AddrIndex))
    {
        MEMSET (&ETSPortEntry, DCBX_ZERO, sizeof (tETSPortEntry));
        ETSPortEntry.u4IfIndex = (UINT4) i4LldpV2PortConfigIfIndex;
        pETSPortEntry = (tETSPortEntry *)
            RBTreeGetNext (gETSGlobalInfo.pRbETSPortTbl,
                           &ETSPortEntry, DcbxUtlPortTblCmpFn);
        if (pETSPortEntry == NULL)
        {
            DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : Rbtree GetNext Node () "
                           "Returns NULL. \r\n", __FUNCTION__);
            return SNMP_FAILURE;
        }
    }

    *pi4NextLldpV2PortConfigIfIndex = (INT4) pETSPortEntry->u4IfIndex;
    *pu4NextLldpV2PortConfigDestAddressIndex = DCBX_DEST_ADDR_INDEX;
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetLldpXdot1dcbxConfigETSConfigurationTxEnable
 Input       :  The Indices
                LldpV2PortConfigIfIndex
                LldpV2PortConfigDestAddressIndex

                The Object 
                retValLldpXdot1dcbxConfigETSConfigurationTxEnable
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetLldpXdot1dcbxConfigETSConfigurationTxEnable (INT4
                                                   i4LldpV2PortConfigIfIndex,
                                                   UINT4
                                                   u4LldpV2PortConfigDestAddressIndex,
                                                   INT4
                                                   *pi4RetValLldpXdot1dcbxConfigETSConfigurationTxEnable)
{
    tETSPortEntry      *pETSPortEntry = NULL;

    DCBX_TRC_ARG3 (DCBX_MGMT_TRC, "%s, Args: i4LldpV2PortConfigIfIndex:%d "
                   "u4LldpV2PortConfigDestAddressIndex: %d \r\n",
                   __func__, i4LldpV2PortConfigIfIndex,
                   u4LldpV2PortConfigDestAddressIndex);

    pETSPortEntry = ETSUtlGetPortEntry ((UINT4) i4LldpV2PortConfigIfIndex);
    if (pETSPortEntry == NULL)
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : ETSUtlGetPortEntry () "
                       " Returns FAILURE. \r\n", __FUNCTION__);
        return SNMP_FAILURE;
    }

    *pi4RetValLldpXdot1dcbxConfigETSConfigurationTxEnable =
        pETSPortEntry->u1ETSConfigTxStatus;
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetLldpXdot1dcbxConfigETSConfigurationTxEnable
 Input       :  The Indices
                LldpV2PortConfigIfIndex
                LldpV2PortConfigDestAddressIndex

                The Object 
                setValLldpXdot1dcbxConfigETSConfigurationTxEnable
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetLldpXdot1dcbxConfigETSConfigurationTxEnable (INT4
                                                   i4LldpV2PortConfigIfIndex,
                                                   UINT4
                                                   u4LldpV2PortConfigDestAddressIndex,
                                                   INT4
                                                   i4SetValLldpXdot1dcbxConfigETSConfigurationTxEnable)
{
    tETSPortEntry      *pETSPortEntry = NULL;

    DCBX_TRC_ARG4 (DCBX_MGMT_TRC, "%s, Args: i4LldpV2PortConfigIfIndex: %d "
                   "u4LldpV2PortConfigDestAddressIndex: %d "
                   "i4SetValLldpXdot1dcbxConfigETSConfigurationTxEnable: %d\r\n",
                   __func__, i4LldpV2PortConfigIfIndex,
                   u4LldpV2PortConfigDestAddressIndex,
                   i4SetValLldpXdot1dcbxConfigETSConfigurationTxEnable);

    pETSPortEntry = ETSUtlGetPortEntry ((UINT4) i4LldpV2PortConfigIfIndex);
    if (pETSPortEntry == NULL)
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : ETSUtlGetPortEntry () "
                       " Returns FAILURE. \r\n", __FUNCTION__);
        return SNMP_FAILURE;
    }

    if (pETSPortEntry->u1ETSConfigTxStatus !=
        (UINT1) i4SetValLldpXdot1dcbxConfigETSConfigurationTxEnable)
    {
        ETSUtlTlvStatusChange (pETSPortEntry,
                               (UINT1)
                               i4SetValLldpXdot1dcbxConfigETSConfigurationTxEnable,
                               ETS_CONF_TLV_SUB_TYPE);
        /* Updation of TLV Tx enable data strucure will
         *          * be done inside the TLV status change function */
    }
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2LldpXdot1dcbxConfigETSConfigurationTxEnable
 Input       :  The Indices
                LldpV2PortConfigIfIndex
                LldpV2PortConfigDestAddressIndex

                The Object 
                testValLldpXdot1dcbxConfigETSConfigurationTxEnable
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2LldpXdot1dcbxConfigETSConfigurationTxEnable (UINT4 *pu4ErrorCode,
                                                      INT4
                                                      i4LldpV2PortConfigIfIndex,
                                                      UINT4
                                                      u4LldpV2PortConfigDestAddressIndex,
                                                      INT4
                                                      i4TestValLldpXdot1dcbxConfigETSConfigurationTxEnable)
{
    tETSPortEntry      *pETSPortEntry = NULL;

    ETS_SHUTDOWN_CHECK_TEST;

    DCBX_TRC_ARG4 (DCBX_MGMT_TRC, "%s, Args: i4LldpV2PortConfigIfIndex: %d"
                   " u4LldpV2PortConfigDestAddressIndex: %d "
                   "i4TestValLldpXdot1dcbxConfigETSConfigurationTxEnable: %d\r\n",
                   __func__, i4LldpV2PortConfigIfIndex,
                   u4LldpV2PortConfigDestAddressIndex,
                   i4TestValLldpXdot1dcbxConfigETSConfigurationTxEnable);

    pETSPortEntry = ETSUtlGetPortEntry ((UINT4) i4LldpV2PortConfigIfIndex);
    if (pETSPortEntry == NULL)
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : ETSUtlGetPortEntry () "
                       " Returns FAILURE. \r\n", __FUNCTION__);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_ERR_ETS_NO_PORT_ENTRY);
        return SNMP_FAILURE;
    }

    if ((i4TestValLldpXdot1dcbxConfigETSConfigurationTxEnable != ETS_ENABLED)
        && (i4TestValLldpXdot1dcbxConfigETSConfigurationTxEnable !=
            ETS_DISABLED))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_ERR_ETS_INVALID_TX_STATUS);
        return SNMP_FAILURE;
    }

    /* Destination Address Index will always be 1.
     * LLDP can not assign more than 1 to this Index.
     */
    if (u4LldpV2PortConfigDestAddressIndex != DCBX_DEST_ADDR_INDEX)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }
    return SNMP_SUCCESS;

}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2LldpXdot1dcbxConfigETSConfigurationTable
 Input       :  The Indices
                LldpV2PortConfigIfIndex
                LldpV2PortConfigDestAddressIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2LldpXdot1dcbxConfigETSConfigurationTable (UINT4 *pu4ErrorCode,
                                                  tSnmpIndexList *
                                                  pSnmpIndexList,
                                                  tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : LldpXdot1dcbxConfigETSRecommendationTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceLldpXdot1dcbxConfigETSRecommendationTable
 Input       :  The Indices
                LldpV2PortConfigIfIndex
                LldpV2PortConfigDestAddressIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceLldpXdot1dcbxConfigETSRecommendationTable (INT4
                                                                   i4LldpV2PortConfigIfIndex,
                                                                   UINT4
                                                                   u4LldpV2PortConfigDestAddressIndex)
{
    tETSPortEntry      *pETSPortEntry = NULL;

    ETS_SHUTDOWN_CHECK;

    DCBX_TRC_ARG3 (DCBX_MGMT_TRC, "%s, Args: i4LldpV2PortConfigIfIndex: %d"
                   " u4LldpV2PortConfigDestAddressIndex:%d \r\n",
                   __func__, i4LldpV2PortConfigIfIndex,
                   u4LldpV2PortConfigDestAddressIndex);

    pETSPortEntry = ETSUtlGetPortEntry ((UINT4) i4LldpV2PortConfigIfIndex);
    if (pETSPortEntry == NULL)
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : ETSUtlGetPortEntry() "
                       "Returns NULL. \r\n", __FUNCTION__);
        return SNMP_FAILURE;
    }

    /* Destination Address Index will always be 1.
     * LLDP can not assign more than 1 to this Index.
     */
    if (u4LldpV2PortConfigDestAddressIndex != DCBX_DEST_ADDR_INDEX)
    {
        DCBX_TRC (DCBX_MGMT_TRC, WRONG_VAL);
        return (SNMP_FAILURE);
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFirstIndexLldpXdot1dcbxConfigETSRecommendationTable
 Input       :  The Indices
                LldpV2PortConfigIfIndex
                LldpV2PortConfigDestAddressIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexLldpXdot1dcbxConfigETSRecommendationTable (INT4
                                                           *pi4LldpV2PortConfigIfIndex,
                                                           UINT4
                                                           *pu4LldpV2PortConfigDestAddressIndex)
{
    tETSPortEntry      *pETSPortEntry = NULL;

    ETS_SHUTDOWN_CHECK;

    DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "%s \r\n", __func__);

    pETSPortEntry = (tETSPortEntry *)
        RBTreeGetFirst (gETSGlobalInfo.pRbETSPortTbl);
    if (pETSPortEntry == NULL)
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : RbTreeGetFirst Node () "
                       "Returns NULL. \r\n", __FUNCTION__);
        return SNMP_FAILURE;
    }

    *pi4LldpV2PortConfigIfIndex = (INT4) pETSPortEntry->u4IfIndex;
    *pu4LldpV2PortConfigDestAddressIndex = DCBX_DEST_ADDR_INDEX;
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetNextIndexLldpXdot1dcbxConfigETSRecommendationTable
 Input       :  The Indices
                LldpV2PortConfigIfIndex
                nextLldpV2PortConfigIfIndex
                LldpV2PortConfigDestAddressIndex
                nextLldpV2PortConfigDestAddressIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexLldpXdot1dcbxConfigETSRecommendationTable (INT4
                                                          i4LldpV2PortConfigIfIndex,
                                                          INT4
                                                          *pi4NextLldpV2PortConfigIfIndex,
                                                          UINT4
                                                          u4LldpV2PortConfigDestAddressIndex,
                                                          UINT4
                                                          *pu4NextLldpV2PortConfigDestAddressIndex)
{
    tETSPortEntry      *pETSPortEntry = NULL;
    tETSPortEntry       ETSPortEntry;
    INT4                i4IfIndex = i4LldpV2PortConfigIfIndex;
    UINT4               u4AddrIndex = u4LldpV2PortConfigDestAddressIndex;

    ETS_SHUTDOWN_CHECK;

    DCBX_TRC_ARG3 (DCBX_MGMT_TRC, "%s, Args: "
                   "i4LldpV2PortConfigIfIndex: %d, u4LldpV2PortConfigDestAddressIndex: %d\r\n",
                   __func__, i4LldpV2PortConfigIfIndex,
                   u4LldpV2PortConfigDestAddressIndex);

    pETSPortEntry = ETSUtlGetPortEntry ((UINT4) i4IfIndex);

    if ((pETSPortEntry == NULL) || (DCBX_ZERO < u4AddrIndex))
    {
        MEMSET (&ETSPortEntry, DCBX_ZERO, sizeof (tETSPortEntry));
        ETSPortEntry.u4IfIndex = (UINT4) i4LldpV2PortConfigIfIndex;
        pETSPortEntry = (tETSPortEntry *)
            RBTreeGetNext (gETSGlobalInfo.pRbETSPortTbl,
                           &ETSPortEntry, DcbxUtlPortTblCmpFn);
        if (pETSPortEntry == NULL)
        {
            DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : Rbtree GetNext Node () "
                           "Returns NULL. \r\n", __FUNCTION__);
            return SNMP_FAILURE;
        }
    }

    *pi4NextLldpV2PortConfigIfIndex = (INT4) pETSPortEntry->u4IfIndex;
    *pu4NextLldpV2PortConfigDestAddressIndex = DCBX_DEST_ADDR_INDEX;
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetLldpXdot1dcbxConfigETSRecommendationTxEnable
 Input       :  The Indices
                LldpV2PortConfigIfIndex
                LldpV2PortConfigDestAddressIndex

                The Object 
                retValLldpXdot1dcbxConfigETSRecommendationTxEnable
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetLldpXdot1dcbxConfigETSRecommendationTxEnable (INT4
                                                    i4LldpV2PortConfigIfIndex,
                                                    UINT4
                                                    u4LldpV2PortConfigDestAddressIndex,
                                                    INT4
                                                    *pi4RetValLldpXdot1dcbxConfigETSRecommendationTxEnable)
{
    tETSPortEntry      *pETSPortEntry = NULL;

    DCBX_TRC_ARG3 (DCBX_MGMT_TRC, "%s, Args: "
                   "i4LldpV2PortConfigIfIndex: %d u4LldpV2PortConfigDestAddressIndex: %d\r\n",
                   __func__, i4LldpV2PortConfigIfIndex,
                   u4LldpV2PortConfigDestAddressIndex);

    pETSPortEntry = ETSUtlGetPortEntry ((UINT4) i4LldpV2PortConfigIfIndex);
    if (pETSPortEntry == NULL)
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : ETSUtlGetPortEntry () "
                       " Returns FAILURE. \r\n", __FUNCTION__);
        return SNMP_FAILURE;
    }

    *pi4RetValLldpXdot1dcbxConfigETSRecommendationTxEnable =
        pETSPortEntry->u1ETSRecoTxStatus;
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetLldpXdot1dcbxConfigETSRecommendationTxEnable
 Input       :  The Indices
                LldpV2PortConfigIfIndex
                LldpV2PortConfigDestAddressIndex

                The Object 
                setValLldpXdot1dcbxConfigETSRecommendationTxEnable
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetLldpXdot1dcbxConfigETSRecommendationTxEnable (INT4
                                                    i4LldpV2PortConfigIfIndex,
                                                    UINT4
                                                    u4LldpV2PortConfigDestAddressIndex,
                                                    INT4
                                                    i4SetValLldpXdot1dcbxConfigETSRecommendationTxEnable)
{
    tETSPortEntry      *pETSPortEntry = NULL;

    DCBX_TRC_ARG4 (DCBX_MGMT_TRC, "%s, Args: "
                   "i4LldpV2PortConfigIfIndex: %d"
                   "u4LldpV2PortConfigDestAddressIndex: %d"
                   "i4SetValLldpXdot1dcbxConfigETSRecommendationTxEnable: %d\r\n",
                   __func__, i4LldpV2PortConfigIfIndex,
                   u4LldpV2PortConfigDestAddressIndex,
                   i4SetValLldpXdot1dcbxConfigETSRecommendationTxEnable);

    pETSPortEntry = ETSUtlGetPortEntry ((UINT4) i4LldpV2PortConfigIfIndex);
    if (pETSPortEntry == NULL)
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : ETSUtlGetPortEntry () "
                       " Returns FAILURE. \r\n", __FUNCTION__);
        return SNMP_FAILURE;
    }

    if (pETSPortEntry->u1ETSRecoTxStatus !=
        (UINT1) i4SetValLldpXdot1dcbxConfigETSRecommendationTxEnable)
    {

        ETSUtlTlvStatusChange (pETSPortEntry,
                               (UINT1)
                               i4SetValLldpXdot1dcbxConfigETSRecommendationTxEnable,
                               ETS_RECO_TLV_SUB_TYPE);
        /* Updation of TLV Tx enable data strucure will
         *          * be done inside the TLV status change function */
    }
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2LldpXdot1dcbxConfigETSRecommendationTxEnable
 Input       :  The Indices
                LldpV2PortConfigIfIndex
                LldpV2PortConfigDestAddressIndex

                The Object 
                testValLldpXdot1dcbxConfigETSRecommendationTxEnable
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2LldpXdot1dcbxConfigETSRecommendationTxEnable (UINT4 *pu4ErrorCode,
                                                       INT4
                                                       i4LldpV2PortConfigIfIndex,
                                                       UINT4
                                                       u4LldpV2PortConfigDestAddressIndex,
                                                       INT4
                                                       i4TestValLldpXdot1dcbxConfigETSRecommendationTxEnable)
{
    tETSPortEntry      *pETSPortEntry = NULL;

    ETS_SHUTDOWN_CHECK_TEST;

    DCBX_TRC_ARG4 (DCBX_MGMT_TRC, "%s, Args: "
                   "i4LldpV2PortConfigIfIndex: %d"
                   "u4LldpV2PortConfigDestAddressIndex: %d"
                   "i4TestValLldpXdot1dcbxConfigETSRecommendationTxEnable: %d\r\n",
                   __func__, i4LldpV2PortConfigIfIndex,
                   u4LldpV2PortConfigDestAddressIndex,
                   i4TestValLldpXdot1dcbxConfigETSRecommendationTxEnable);

    pETSPortEntry = ETSUtlGetPortEntry ((UINT4) i4LldpV2PortConfigIfIndex);
    if (pETSPortEntry == NULL)
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : ETSUtlGetPortEntry () "
                       " Returns FAILURE. \r\n", __FUNCTION__);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_ERR_ETS_NO_PORT_ENTRY);
        return SNMP_FAILURE;
    }

    if ((i4TestValLldpXdot1dcbxConfigETSRecommendationTxEnable !=
         ETS_ENABLED)
        && (i4TestValLldpXdot1dcbxConfigETSRecommendationTxEnable !=
            ETS_DISABLED))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_ERR_ETS_INVALID_TX_STATUS);
        return SNMP_FAILURE;
    }

    /* Destination Address Index will always be 1.
     * LLDP can not assign more than 1 to this Index.
     */
    if (u4LldpV2PortConfigDestAddressIndex != DCBX_DEST_ADDR_INDEX)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }

    return SNMP_SUCCESS;

}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2LldpXdot1dcbxConfigETSRecommendationTable
 Input       :  The Indices
                LldpV2PortConfigIfIndex
                LldpV2PortConfigDestAddressIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2LldpXdot1dcbxConfigETSRecommendationTable (UINT4 *pu4ErrorCode,
                                                   tSnmpIndexList *
                                                   pSnmpIndexList,
                                                   tSNMP_VAR_BIND *
                                                   pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : LldpXdot1dcbxConfigPFCTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceLldpXdot1dcbxConfigPFCTable
 Input       :  The Indices
                LldpV2PortConfigIfIndex
                LldpV2PortConfigDestAddressIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceLldpXdot1dcbxConfigPFCTable (INT4
                                                     i4LldpV2PortConfigIfIndex,
                                                     UINT4
                                                     u4LldpV2PortConfigDestAddressIndex)
{
    tPFCPortEntry      *pPFCPortEntry = NULL;

    PFC_SHUTDOWN_CHECK;

    DCBX_TRC_ARG3 (DCBX_MGMT_TRC, "%s, Args: "
                   "i4LldpV2PortConfigIfIndex: %d u4LldpV2PortConfigDestAddressIndex: %d\r\n",
                   __func__, i4LldpV2PortConfigIfIndex,
                   u4LldpV2PortConfigDestAddressIndex);

    pPFCPortEntry = PFCUtlGetPortEntry ((UINT4) i4LldpV2PortConfigIfIndex);
    if (pPFCPortEntry == NULL)
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : PFCUtlGetPortEntry() "
                       "Returns NULL. \r\n", __FUNCTION__);
        return SNMP_FAILURE;
    }

    /* Destination Address Index will always be 1.
     * LLDP can not assign more than 1 to this Index.
     */
    if (u4LldpV2PortConfigDestAddressIndex != DCBX_DEST_ADDR_INDEX)
    {
        DCBX_TRC (DCBX_MGMT_TRC, WRONG_VAL);
        return (SNMP_FAILURE);
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFirstIndexLldpXdot1dcbxConfigPFCTable
 Input       :  The Indices
                LldpV2PortConfigIfIndex
                LldpV2PortConfigDestAddressIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexLldpXdot1dcbxConfigPFCTable (INT4 *pi4LldpV2PortConfigIfIndex,
                                             UINT4
                                             *pu4LldpV2PortConfigDestAddressIndex)
{
    tPFCPortEntry      *pPFCPortEntry = NULL;

    PFC_SHUTDOWN_CHECK;

    DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "%s \r\n", __func__);

    pPFCPortEntry = (tPFCPortEntry *)
        RBTreeGetFirst (gPFCGlobalInfo.pRbPFCPortTbl);

    if (pPFCPortEntry == NULL)
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : RBtreeGetFirst() "
                       "Returns NULL. \r\n", __FUNCTION__);
        return SNMP_FAILURE;
    }

    *pi4LldpV2PortConfigIfIndex = (INT4) pPFCPortEntry->u4IfIndex;
    *pu4LldpV2PortConfigDestAddressIndex = DCBX_DEST_ADDR_INDEX;
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetNextIndexLldpXdot1dcbxConfigPFCTable
 Input       :  The Indices
                LldpV2PortConfigIfIndex
                nextLldpV2PortConfigIfIndex
                LldpV2PortConfigDestAddressIndex
                nextLldpV2PortConfigDestAddressIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexLldpXdot1dcbxConfigPFCTable (INT4 i4LldpV2PortConfigIfIndex,
                                            INT4
                                            *pi4NextLldpV2PortConfigIfIndex,
                                            UINT4
                                            u4LldpV2PortConfigDestAddressIndex,
                                            UINT4
                                            *pu4NextLldpV2PortConfigDestAddressIndex)
{
    tPFCPortEntry      *pPFCPortEntry = NULL;
    tPFCPortEntry       PFCPortEntry;
    INT4                i4IfIndex = i4LldpV2PortConfigIfIndex;
    UINT4               u4AddrIndex = u4LldpV2PortConfigDestAddressIndex;

    PFC_SHUTDOWN_CHECK;

    DCBX_TRC_ARG3 (DCBX_MGMT_TRC, "%s, Args: "
                   "i4LldpV2PortConfigIfIndex: %d u4LldpV2PortConfigDestAddressIndex: %d\r\n",
                   __func__, i4LldpV2PortConfigIfIndex,
                   u4LldpV2PortConfigDestAddressIndex);

    pPFCPortEntry = PFCUtlGetPortEntry ((UINT4) i4IfIndex);

    if ((pPFCPortEntry == NULL) || (DCBX_ZERO < u4AddrIndex))
    {
        MEMSET (&PFCPortEntry, DCBX_ZERO, sizeof (tPFCPortEntry));
        PFCPortEntry.u4IfIndex = (UINT4) i4LldpV2PortConfigIfIndex;
        pPFCPortEntry = (tPFCPortEntry *)
            RBTreeGetNext (gPFCGlobalInfo.pRbPFCPortTbl,
                           &PFCPortEntry, DcbxUtlPortTblCmpFn);
        if (pPFCPortEntry == NULL)
        {
            DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : Rbtree GetNext Node () "
                           "Returns NULL. \r\n", __FUNCTION__);
            return SNMP_FAILURE;
        }
    }

    *pi4NextLldpV2PortConfigIfIndex = (INT4) pPFCPortEntry->u4IfIndex;
    *pu4NextLldpV2PortConfigDestAddressIndex = DCBX_DEST_ADDR_INDEX;
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetLldpXdot1dcbxConfigPFCTxEnable
 Input       :  The Indices
                LldpV2PortConfigIfIndex
                LldpV2PortConfigDestAddressIndex

                The Object 
                retValLldpXdot1dcbxConfigPFCTxEnable
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetLldpXdot1dcbxConfigPFCTxEnable (INT4 i4LldpV2PortConfigIfIndex,
                                      UINT4 u4LldpV2PortConfigDestAddressIndex,
                                      INT4
                                      *pi4RetValLldpXdot1dcbxConfigPFCTxEnable)
{
    tPFCPortEntry      *pPFCPortEntry = NULL;

    DCBX_TRC_ARG3 (DCBX_MGMT_TRC, "%s, Args: "
                   "i4LldpV2PortConfigIfIndex: %d u4LldpV2PortConfigDestAddressIndex: %d\r\n",
                   __func__, i4LldpV2PortConfigIfIndex,
                   u4LldpV2PortConfigDestAddressIndex);

    pPFCPortEntry = PFCUtlGetPortEntry ((UINT4) i4LldpV2PortConfigIfIndex);
    if (pPFCPortEntry == NULL)
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : PFCGetDataPortEntry () "
                       " Returns FAILURE. \r\n", __FUNCTION__);
        return SNMP_FAILURE;
    }

    *pi4RetValLldpXdot1dcbxConfigPFCTxEnable = pPFCPortEntry->u1PFCTxStatus;
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetLldpXdot1dcbxConfigPFCTxEnable
 Input       :  The Indices
                LldpV2PortConfigIfIndex
                LldpV2PortConfigDestAddressIndex

                The Object 
                setValLldpXdot1dcbxConfigPFCTxEnable
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetLldpXdot1dcbxConfigPFCTxEnable (INT4 i4LldpV2PortConfigIfIndex,
                                      UINT4 u4LldpV2PortConfigDestAddressIndex,
                                      INT4
                                      i4SetValLldpXdot1dcbxConfigPFCTxEnable)
{
    tPFCPortEntry      *pPFCPortEntry = NULL;

    DCBX_TRC_ARG4 (DCBX_MGMT_TRC, "%s, Args: "
                   "i4LldpV2PortConfigIfIndex: %d "
                   "u4LldpV2PortConfigDestAddressIndex: %d"
                   "i4SetValLldpXdot1dcbxConfigPFCTxEnable: %d\r\n",
                   __func__, i4LldpV2PortConfigIfIndex,
                   u4LldpV2PortConfigDestAddressIndex,
                   i4SetValLldpXdot1dcbxConfigPFCTxEnable);

    pPFCPortEntry = PFCUtlGetPortEntry ((UINT4) i4LldpV2PortConfigIfIndex);
    if (pPFCPortEntry == NULL)
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : PFCGetDataPortEntry () "
                       " Returns FAILURE. \r\n", __FUNCTION__);
        return SNMP_FAILURE;
    }
    if (pPFCPortEntry->u1PFCTxStatus !=
        (UINT1) i4SetValLldpXdot1dcbxConfigPFCTxEnable)
    {
        PFCUtlTlvStatusChange (pPFCPortEntry,
                               (UINT1) i4SetValLldpXdot1dcbxConfigPFCTxEnable);

        /* Updation of TLV Tx enable data strucure will
         * be done inside the TLV status change function */
    }
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2LldpXdot1dcbxConfigPFCTxEnable
 Input       :  The Indices
                LldpV2PortConfigIfIndex
                LldpV2PortConfigDestAddressIndex

                The Object 
                testValLldpXdot1dcbxConfigPFCTxEnable
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2LldpXdot1dcbxConfigPFCTxEnable (UINT4 *pu4ErrorCode,
                                         INT4 i4LldpV2PortConfigIfIndex,
                                         UINT4
                                         u4LldpV2PortConfigDestAddressIndex,
                                         INT4
                                         i4TestValLldpXdot1dcbxConfigPFCTxEnable)
{
    tPFCPortEntry      *pPFCPortEntry = NULL;

    PFC_SHUTDOWN_CHECK_TEST;

    DCBX_TRC_ARG4 (DCBX_MGMT_TRC, "%s, Args: "
                   "i4LldpV2PortConfigIfIndex: %d "
                   "u4LldpV2PortConfigDestAddressIndex: %d "
                   "i4TestValLldpXdot1dcbxConfigPFCTxEnable: %d\r\n",
                   __func__, i4LldpV2PortConfigIfIndex,
                   u4LldpV2PortConfigDestAddressIndex,
                   i4TestValLldpXdot1dcbxConfigPFCTxEnable);

    pPFCPortEntry = PFCUtlGetPortEntry ((UINT4) i4LldpV2PortConfigIfIndex);
    if (pPFCPortEntry == NULL)
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : PFCGetDataPortEntry () "
                       " Returns FAILURE. \r\n", __FUNCTION__);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_ERR_PFC_NO_PORT_ENTRY);
        return SNMP_FAILURE;
    }

    if ((i4TestValLldpXdot1dcbxConfigPFCTxEnable != PFC_ENABLED)
        && (i4TestValLldpXdot1dcbxConfigPFCTxEnable != PFC_DISABLED))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_ERR_PFC_INVALID_TX_STATUS);
        return SNMP_FAILURE;
    }

    /* Destination Address Index will always be 1.
     * LLDP can not assign more than 1 to this Index.
     */
    if (u4LldpV2PortConfigDestAddressIndex != DCBX_DEST_ADDR_INDEX)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }

    return SNMP_SUCCESS;

}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2LldpXdot1dcbxConfigPFCTable
 Input       :  The Indices
                LldpV2PortConfigIfIndex
                LldpV2PortConfigDestAddressIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2LldpXdot1dcbxConfigPFCTable (UINT4 *pu4ErrorCode,
                                     tSnmpIndexList * pSnmpIndexList,
                                     tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : LldpXdot1dcbxConfigApplicationPriorityTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceLldpXdot1dcbxConfigApplicationPriorityTable
 Input       :  The Indices
                LldpV2PortConfigIfIndex
                LldpV2PortConfigDestAddressIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceLldpXdot1dcbxConfigApplicationPriorityTable (INT4
                                                                     i4LldpV2PortConfigIfIndex,
                                                                     UINT4
                                                                     u4LldpV2PortConfigDestAddressIndex)
{
    tAppPriPortEntry   *pAppPriPortEntry = NULL;

    APP_PRI_SHUTDOWN_CHECK;

    DCBX_TRC_ARG3 (DCBX_MGMT_TRC, "%s, Args: "
                   "i4LldpV2PortConfigIfIndex: %d "
                   "u4LldpV2PortConfigDestAddressIndex: %d\r\n",
                   __func__, i4LldpV2PortConfigIfIndex,
                   u4LldpV2PortConfigDestAddressIndex);

    pAppPriPortEntry =
        AppPriUtlGetPortEntry ((UINT4) i4LldpV2PortConfigIfIndex);
    if (pAppPriPortEntry == NULL)
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : AppPriUtlGetPortEntry() "
                       "Returns NULL. \r\n", __FUNCTION__);
        return SNMP_FAILURE;
    }

    /* Destination Address Index will always be 1.
     *      * LLDP can not assign more than 1 to this Index.
     *           */
    if (u4LldpV2PortConfigDestAddressIndex != DCBX_DEST_ADDR_INDEX)
    {
        DCBX_TRC (DCBX_MGMT_TRC, WRONG_VAL);
        return (SNMP_FAILURE);
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexLldpXdot1dcbxConfigApplicationPriorityTable
 Input       :  The Indices
                LldpV2PortConfigIfIndex
                LldpV2PortConfigDestAddressIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexLldpXdot1dcbxConfigApplicationPriorityTable (INT4
                                                             *pi4LldpV2PortConfigIfIndex,
                                                             UINT4
                                                             *pu4LldpV2PortConfigDestAddressIndex)
{
    tAppPriPortEntry   *pAppPriPortEntry = NULL;

    APP_PRI_SHUTDOWN_CHECK;

    DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "%s\r\n", __func__);

    pAppPriPortEntry = (tAppPriPortEntry *)
        RBTreeGetFirst (gAppPriGlobalInfo.pRbAppPriPortTbl);

    if (pAppPriPortEntry == NULL)
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : RBtreeGetFirst() "
                       "Returns NULL. \r\n", __FUNCTION__);
        return SNMP_FAILURE;
    }

    *pi4LldpV2PortConfigIfIndex = (INT4) pAppPriPortEntry->u4IfIndex;
    *pu4LldpV2PortConfigDestAddressIndex = DCBX_DEST_ADDR_INDEX;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexLldpXdot1dcbxConfigApplicationPriorityTable
 Input       :  The Indices
                LldpV2PortConfigIfIndex
                nextLldpV2PortConfigIfIndex
                LldpV2PortConfigDestAddressIndex
                nextLldpV2PortConfigDestAddressIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexLldpXdot1dcbxConfigApplicationPriorityTable (INT4
                                                            i4LldpV2PortConfigIfIndex,
                                                            INT4
                                                            *pi4NextLldpV2PortConfigIfIndex,
                                                            UINT4
                                                            u4LldpV2PortConfigDestAddressIndex,
                                                            UINT4
                                                            *pu4NextLldpV2PortConfigDestAddressIndex)
{
    tAppPriPortEntry   *pAppPriPortEntry = NULL;
    tAppPriPortEntry    AppPriPortEntry;

    APP_PRI_SHUTDOWN_CHECK;

    DCBX_TRC_ARG3 (DCBX_MGMT_TRC, "%s, Args: "
                   "4LldpV2PortConfigIfIndex = %d u4LldpV2PortConfigDestAddressIndex = %d\r\n",
                   __func__, i4LldpV2PortConfigIfIndex,
                   u4LldpV2PortConfigDestAddressIndex);

    MEMSET (&AppPriPortEntry, DCBX_ZERO, sizeof (tPFCPortEntry));

    AppPriPortEntry.u4IfIndex = (UINT4) i4LldpV2PortConfigIfIndex;
    pAppPriPortEntry = (tAppPriPortEntry *)
        RBTreeGetNext (gAppPriGlobalInfo.pRbAppPriPortTbl,
                       &AppPriPortEntry, AppPriUtlPortTblCmpFn);
    if (pAppPriPortEntry == NULL)
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : RBTree GetNext Node () "
                       "Returns NULL. \r\n", __FUNCTION__);
        return SNMP_FAILURE;
    }

    *pi4NextLldpV2PortConfigIfIndex = (INT4) pAppPriPortEntry->u4IfIndex;
    *pu4NextLldpV2PortConfigDestAddressIndex = DCBX_DEST_ADDR_INDEX;
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetLldpXdot1dcbxConfigApplicationPriorityTxEnable
 Input       :  The Indices
                LldpV2PortConfigIfIndex
                LldpV2PortConfigDestAddressIndex

                The Object 
                retValLldpXdot1dcbxConfigApplicationPriorityTxEnable
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetLldpXdot1dcbxConfigApplicationPriorityTxEnable (INT4
                                                      i4LldpV2PortConfigIfIndex,
                                                      UINT4
                                                      u4LldpV2PortConfigDestAddressIndex,
                                                      INT4
                                                      *pi4RetValLldpXdot1dcbxConfigApplicationPriorityTxEnable)
{
    tAppPriPortEntry   *pAppPriPortEntry = NULL;

    APP_PRI_SHUTDOWN_CHECK;

    DCBX_TRC_ARG3 (DCBX_MGMT_TRC, "%s, Args: "
                   "i4LldpV2PortConfigIfIndex: %d "
                   "u4LldpV2PortConfigDestAddressIndex: %d\r\n",
                   __func__, i4LldpV2PortConfigIfIndex,
                   u4LldpV2PortConfigDestAddressIndex);

    pAppPriPortEntry =
        AppPriUtlGetPortEntry ((UINT4) i4LldpV2PortConfigIfIndex);

    if (pAppPriPortEntry == NULL)
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : PFCGetDataPortEntry () "
                       " Returns FAILURE. \r\n", __FUNCTION__);
        return SNMP_FAILURE;
    }

    *pi4RetValLldpXdot1dcbxConfigApplicationPriorityTxEnable =
        pAppPriPortEntry->u1AppPriTxStatus;
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetLldpXdot1dcbxConfigApplicationPriorityTxEnable
 Input       :  The Indices
                LldpV2PortConfigIfIndex
                LldpV2PortConfigDestAddressIndex

                The Object 
                setValLldpXdot1dcbxConfigApplicationPriorityTxEnable
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetLldpXdot1dcbxConfigApplicationPriorityTxEnable (INT4
                                                      i4LldpV2PortConfigIfIndex,
                                                      UINT4
                                                      u4LldpV2PortConfigDestAddressIndex,
                                                      INT4
                                                      i4SetValLldpXdot1dcbxConfigApplicationPriorityTxEnable)
{
    tAppPriPortEntry   *pAppPriPortEntry = NULL;

    DCBX_TRC_ARG4 (DCBX_MGMT_TRC, "%s, Args: "
                   "i4LldpV2PortConfigIfIndex: %d "
                   "u4LldpV2PortConfigDestAddressIndex: %d "
                   "i4SetValLldpXdot1dcbxConfigApplicationPriorityTxEnable: %d\r\n",
                   __func__, i4LldpV2PortConfigIfIndex,
                   u4LldpV2PortConfigDestAddressIndex,
                   i4SetValLldpXdot1dcbxConfigApplicationPriorityTxEnable);

    pAppPriPortEntry =
        AppPriUtlGetPortEntry ((UINT4) i4LldpV2PortConfigIfIndex);
    if (pAppPriPortEntry == NULL)
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : AppPriGetDataPortEntry () "
                       " Returns FAILURE. \r\n", __FUNCTION__);
        return SNMP_FAILURE;
    }
    if (pAppPriPortEntry->u1AppPriTxStatus !=
        (UINT1) i4SetValLldpXdot1dcbxConfigApplicationPriorityTxEnable)
    {
        AppPriUtlTlvStatusChange (pAppPriPortEntry,
                                  (UINT1)
                                  i4SetValLldpXdot1dcbxConfigApplicationPriorityTxEnable);

        /* Updation of TLV Tx enable data strucure will
         *          * be done inside the TLV status change function */
    }
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2LldpXdot1dcbxConfigApplicationPriorityTxEnable
 Input       :  The Indices
                LldpV2PortConfigIfIndex
                LldpV2PortConfigDestAddressIndex

                The Object 
                testValLldpXdot1dcbxConfigApplicationPriorityTxEnable
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2LldpXdot1dcbxConfigApplicationPriorityTxEnable (UINT4 *pu4ErrorCode,
                                                         INT4
                                                         i4LldpV2PortConfigIfIndex,
                                                         UINT4
                                                         u4LldpV2PortConfigDestAddressIndex,
                                                         INT4
                                                         i4TestValLldpXdot1dcbxConfigApplicationPriorityTxEnable)
{
    tAppPriPortEntry   *pAppPortEntry = NULL;

    APP_PRI_SHUTDOWN_CHECK_TEST;

    DCBX_TRC_ARG4 (DCBX_MGMT_TRC, "%s, Args: "
                   "i4LldpV2PortConfigIfIndex: %d "
                   "u4LldpV2PortConfigDestAddressIndex: %d "
                   "i4TestValLldpXdot1dcbxConfigApplicationPriorityTxEnable: %d\r\n",
                   __func__, i4LldpV2PortConfigIfIndex,
                   u4LldpV2PortConfigDestAddressIndex,
                   i4TestValLldpXdot1dcbxConfigApplicationPriorityTxEnable);

    pAppPortEntry = AppPriUtlGetPortEntry ((UINT4) i4LldpV2PortConfigIfIndex);
    if (pAppPortEntry == NULL)
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : PFCGetDataPortEntry () "
                       " Returns FAILURE. \r\n", __FUNCTION__);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_ERR_APP_PRI_NO_PORT_ENTRY);
        return SNMP_FAILURE;
    }

    if ((i4TestValLldpXdot1dcbxConfigApplicationPriorityTxEnable !=
         APP_PRI_ENABLED)
        && (i4TestValLldpXdot1dcbxConfigApplicationPriorityTxEnable !=
            APP_PRI_DISABLED))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_ERR_PFC_INVALID_TX_STATUS);
        return SNMP_FAILURE;
    }

    /* Destination Address Index will always be 1.
     *      * LLDP can not assign more than 1 to this Index.
     *           */
    if (u4LldpV2PortConfigDestAddressIndex != DCBX_DEST_ADDR_INDEX)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }

    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2LldpXdot1dcbxConfigApplicationPriorityTable
 Input       :  The Indices
                LldpV2PortConfigIfIndex
                LldpV2PortConfigDestAddressIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2LldpXdot1dcbxConfigApplicationPriorityTable (UINT4 *pu4ErrorCode,
                                                     tSnmpIndexList *
                                                     pSnmpIndexList,
                                                     tSNMP_VAR_BIND *
                                                     pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : LldpXdot1dcbxLocETSBasicConfigurationTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceLldpXdot1dcbxLocETSBasicConfigurationTable
 Input       :  The Indices
                LldpV2LocPortIfIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceLldpXdot1dcbxLocETSBasicConfigurationTable (INT4
                                                                    i4LldpV2LocPortIfIndex)
{
    tETSPortEntry      *pETSPortEntry = NULL;

    ETS_SHUTDOWN_CHECK;

    DCBX_TRC_ARG2 (DCBX_MGMT_TRC, "%s, Args: "
                   "i4LldpV2LocPortIfIndex: %d\r\n",
                   __func__, i4LldpV2LocPortIfIndex);

    pETSPortEntry = ETSUtlGetPortEntry ((UINT4) i4LldpV2LocPortIfIndex);
    if (pETSPortEntry == NULL)
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : ETSUtlGetPortEntry() "
                       "Returns NULL. \r\n", __FUNCTION__);
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFirstIndexLldpXdot1dcbxLocETSBasicConfigurationTable
 Input       :  The Indices
                LldpV2LocPortIfIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexLldpXdot1dcbxLocETSBasicConfigurationTable (INT4
                                                            *pi4LldpV2LocPortIfIndex)
{
    tETSPortEntry      *pETSPortEntry = NULL;

    ETS_SHUTDOWN_CHECK;

    DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "%s\r\n", __func__);

    pETSPortEntry = (tETSPortEntry *)
        RBTreeGetFirst (gETSGlobalInfo.pRbETSPortTbl);
    if (pETSPortEntry == NULL)
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : RbTreeGetFirst Node () "
                       "Returns NULL. \r\n", __FUNCTION__);
        return SNMP_FAILURE;
    }

    *pi4LldpV2LocPortIfIndex = (INT4) pETSPortEntry->u4IfIndex;
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetNextIndexLldpXdot1dcbxLocETSBasicConfigurationTable
 Input       :  The Indices
                LldpV2LocPortIfIndex
                nextLldpV2LocPortIfIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexLldpXdot1dcbxLocETSBasicConfigurationTable (INT4
                                                           i4LldpV2LocPortIfIndex,
                                                           INT4
                                                           *pi4NextLldpV2LocPortIfIndex)
{

    tETSPortEntry      *pETSPortEntry = NULL;
    tETSPortEntry       ETSPortEntry;

    ETS_SHUTDOWN_CHECK;

    DCBX_TRC_ARG2 (DCBX_MGMT_TRC, "%s, Args: "
                   "i4LldpV2LocPortIfIndex: %d\r\n",
                   __func__, i4LldpV2LocPortIfIndex);

    MEMSET (&ETSPortEntry, DCBX_ZERO, sizeof (tETSPortEntry));

    ETSPortEntry.u4IfIndex = (UINT4) i4LldpV2LocPortIfIndex;
    pETSPortEntry = (tETSPortEntry *)
        RBTreeGetNext (gETSGlobalInfo.pRbETSPortTbl,
                       &ETSPortEntry, DcbxUtlPortTblCmpFn);
    if (pETSPortEntry == NULL)
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : Rbtree GetNext Node () "
                       "Returns NULL. \r\n", __FUNCTION__);
        return SNMP_FAILURE;
    }

    *pi4NextLldpV2LocPortIfIndex = (INT4) pETSPortEntry->u4IfIndex;
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetLldpXdot1dcbxLocETSConCreditBasedShaperSupport
 Input       :  The Indices
                LldpV2LocPortIfIndex

                The Object 
                retValLldpXdot1dcbxLocETSConCreditBasedShaperSupport
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetLldpXdot1dcbxLocETSConCreditBasedShaperSupport (INT4
                                                      i4LldpV2LocPortIfIndex,
                                                      INT4
                                                      *pi4RetValLldpXdot1dcbxLocETSConCreditBasedShaperSupport)
{
    tETSPortEntry      *pETSPortEntry = NULL;

    DCBX_TRC_ARG2 (DCBX_MGMT_TRC, "%s, Args: "
                   "i4LldpV2LocPortIfIndex: %d\r\n",
                   __func__, i4LldpV2LocPortIfIndex);

    pETSPortEntry = ETSUtlGetPortEntry ((UINT4) i4LldpV2LocPortIfIndex);
    if (pETSPortEntry == NULL)
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : ETSUtlGetPortEntry () "
                       " Returns FAILURE. \r\n", __FUNCTION__);
        return SNMP_FAILURE;
    }
    *pi4RetValLldpXdot1dcbxLocETSConCreditBasedShaperSupport =
        pETSPortEntry->ETSLocPortInfo.u1ETSLocCBS;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetLldpXdot1dcbxLocETSConTrafficClassesSupported
 Input       :  The Indices
                LldpV2LocPortIfIndex

                The Object 
                retValLldpXdot1dcbxLocETSConTrafficClassesSupported
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetLldpXdot1dcbxLocETSConTrafficClassesSupported (INT4
                                                     i4LldpV2LocPortIfIndex,
                                                     UINT4
                                                     *pu4RetValLldpXdot1dcbxLocETSConTrafficClassesSupported)
{
    /* nmhGetLldpXdot1dcbxLocTCSupported */
    tETSPortEntry      *pETSPortEntry = NULL;

    DCBX_TRC_ARG2 (DCBX_MGMT_TRC, "%s, Args: "
                   "i4LldpV2LocPortIfIndex: %d\r\n",
                   __func__, i4LldpV2LocPortIfIndex);

    pETSPortEntry = ETSUtlGetPortEntry ((UINT4) i4LldpV2LocPortIfIndex);
    if (pETSPortEntry == NULL)
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : ETSUtlGetPortEntry () "
                       " Returns FAILURE. \r\n", __FUNCTION__);
        return SNMP_FAILURE;
    }
    *pu4RetValLldpXdot1dcbxLocETSConTrafficClassesSupported =
        pETSPortEntry->ETSLocPortInfo.u1ETSLocNumTcSup;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetLldpXdot1dcbxLocETSConWilling
 Input       :  The Indices
                LldpV2LocPortIfIndex

                The Object 
                retValLldpXdot1dcbxLocETSConWilling
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetLldpXdot1dcbxLocETSConWilling (INT4 i4LldpV2LocPortIfIndex,
                                     INT4
                                     *pi4RetValLldpXdot1dcbxLocETSConWilling)
{
    tETSPortEntry      *pETSPortEntry = NULL;

    DCBX_TRC_ARG2 (DCBX_MGMT_TRC, "%s, Args: "
                   "i4LldpV2LocPortIfIndex: %d\r\n",
                   __func__, i4LldpV2LocPortIfIndex);

    pETSPortEntry = ETSUtlGetPortEntry ((UINT4) i4LldpV2LocPortIfIndex);
    if (pETSPortEntry == NULL)
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : ETSUtlGetPortEntry () "
                       " Returns FAILURE. \r\n", __FUNCTION__);
        return SNMP_FAILURE;
    }

    *pi4RetValLldpXdot1dcbxLocETSConWilling =
        (pETSPortEntry->ETSLocPortInfo).u1ETSLocWilling;
    return SNMP_SUCCESS;

}

/* LOW LEVEL Routines for Table : LldpXdot1dcbxLocETSConPriorityAssignmentTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceLldpXdot1dcbxLocETSConPriorityAssignmentTable
 Input       :  The Indices
                LldpV2LocPortIfIndex
                LldpXdot1dcbxLocETSConPriority
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceLldpXdot1dcbxLocETSConPriorityAssignmentTable (INT4
                                                                       i4LldpV2LocPortIfIndex,
                                                                       UINT4
                                                                       u4LldpXdot1dcbxLocETSConPriority)
{
    tETSPortEntry      *pETSPortEntry = NULL;

    ETS_SHUTDOWN_CHECK;

    DCBX_TRC_ARG3 (DCBX_MGMT_TRC, "%s, Args: "
                   "i4LldpV2LocPortIfIndex: %d u4LldpXdot1dcbxLocETSConPriority: %d\r\n",
                   __func__, i4LldpV2LocPortIfIndex,
                   u4LldpXdot1dcbxLocETSConPriority);

    pETSPortEntry = ETSUtlGetPortEntry ((UINT4) i4LldpV2LocPortIfIndex);
    if (pETSPortEntry == NULL)
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : ETSUtlGetPortEntry() "
                       "Returns NULL. \r\n", __FUNCTION__);
        return SNMP_FAILURE;
    }

    if (((INT4) (u4LldpXdot1dcbxLocETSConPriority) < DCBX_ZERO) ||
        (u4LldpXdot1dcbxLocETSConPriority >= DCBX_MAX_PRIORITIES))
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : Priority is not in range(0-7)"
                       " \r\n", __FUNCTION__);
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFirstIndexLldpXdot1dcbxLocETSConPriorityAssignmentTable
 Input       :  The Indices
                LldpV2LocPortIfIndex
                LldpXdot1dcbxLocETSConPriority
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexLldpXdot1dcbxLocETSConPriorityAssignmentTable (INT4
                                                               *pi4LldpV2LocPortIfIndex,
                                                               UINT4
                                                               *pu4LldpXdot1dcbxLocETSConPriority)
{
    tETSPortEntry      *pETSPortEntry = NULL;

    ETS_SHUTDOWN_CHECK;

    DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "%s \r\n", __func__);

    pETSPortEntry = (tETSPortEntry *)
        RBTreeGetFirst (gETSGlobalInfo.pRbETSPortTbl);
    if (pETSPortEntry == NULL)
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : RbTree GetFirst() "
                       "Returns NULL. \r\n", __FUNCTION__);
        return SNMP_FAILURE;
    }

    *pi4LldpV2LocPortIfIndex = (INT4) pETSPortEntry->u4IfIndex;
    *pu4LldpXdot1dcbxLocETSConPriority = DCBX_ZERO;
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetNextIndexLldpXdot1dcbxLocETSConPriorityAssignmentTable
 Input       :  The Indices
                LldpV2LocPortIfIndex
                nextLldpV2LocPortIfIndex
                LldpXdot1dcbxLocETSConPriority
                nextLldpXdot1dcbxLocETSConPriority
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexLldpXdot1dcbxLocETSConPriorityAssignmentTable (INT4
                                                              i4LldpV2LocPortIfIndex,
                                                              INT4
                                                              *pi4NextLldpV2LocPortIfIndex,
                                                              UINT4
                                                              u4LldpXdot1dcbxLocETSConPriority,
                                                              UINT4
                                                              *pu4NextLldpXdot1dcbxLocETSConPriority)
{
    tETSPortEntry      *pETSPortEntry = NULL;
    tETSPortEntry       ETSPortEntry;

    ETS_SHUTDOWN_CHECK;

    DCBX_TRC_ARG3 (DCBX_MGMT_TRC, "%s, Args: "
                   "i4LldpV2LocPortIfIndex: %d "
                   "u4LldpXdot1dcbxLocETSConPriority: %d\r\n",
                   __func__, i4LldpV2LocPortIfIndex,
                   u4LldpXdot1dcbxLocETSConPriority);

    MEMSET (&ETSPortEntry, DCBX_ZERO, sizeof (tETSPortEntry));
    ETSPortEntry.u4IfIndex = (UINT4) i4LldpV2LocPortIfIndex;
    pETSPortEntry = ETSUtlGetPortEntry ((UINT4) i4LldpV2LocPortIfIndex);
    if ((pETSPortEntry == NULL) ||
        (u4LldpXdot1dcbxLocETSConPriority >= DCBX_MAX_VALID_PRI))
    {
        pETSPortEntry = (tETSPortEntry *)
            RBTreeGetNext (gETSGlobalInfo.pRbETSPortTbl,
                           &ETSPortEntry, DcbxUtlPortTblCmpFn);
        if (pETSPortEntry == NULL)
        {

            DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : RbTreeGetNExt() "
                           "Returns NULL. \r\n", __FUNCTION__);
            return SNMP_FAILURE;
        }

        *pi4NextLldpV2LocPortIfIndex = (INT4) pETSPortEntry->u4IfIndex;
        *pu4NextLldpXdot1dcbxLocETSConPriority = DCBX_ZERO;
    }
    else
    {
        *pi4NextLldpV2LocPortIfIndex = i4LldpV2LocPortIfIndex;
        *pu4NextLldpXdot1dcbxLocETSConPriority =
            u4LldpXdot1dcbxLocETSConPriority + DCBX_MIN_VAL;
    }
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetLldpXdot1dcbxLocETSConPriTrafficClass
 Input       :  The Indices
                LldpV2LocPortIfIndex
                LldpXdot1dcbxLocETSConPriority

                The Object 
                retValLldpXdot1dcbxLocETSConPriTrafficClass
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetLldpXdot1dcbxLocETSConPriTrafficClass (INT4 i4LldpV2LocPortIfIndex,
                                             UINT4
                                             u4LldpXdot1dcbxLocETSConPriority,
                                             UINT4
                                             *pu4RetValLldpXdot1dcbxLocETSConPriTrafficClass)
{
    /*   nmhGetLldpXdot1dcbxLocETSConTrafficClassGroup */
    tETSPortEntry      *pETSPortEntry = NULL;

    DCBX_TRC_ARG3 (DCBX_MGMT_TRC, "%s, Args: "
                   "i4LldpV2LocPortIfIndex: %d "
                   "u4LldpXdot1dcbxLocETSConPriority: %d\r\n",
                   __func__, i4LldpV2LocPortIfIndex,
                   u4LldpXdot1dcbxLocETSConPriority);

    pETSPortEntry = ETSUtlGetPortEntry ((UINT4) i4LldpV2LocPortIfIndex);
    if (pETSPortEntry == NULL)
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : ETSUtlGetPortEntry() "
                       "Returns NULL. \r\n", __FUNCTION__);
        return SNMP_FAILURE;
    }

    *pu4RetValLldpXdot1dcbxLocETSConPriTrafficClass =
        (INT4) ((pETSPortEntry->ETSLocPortInfo).
                au1ETSLocTCGID[u4LldpXdot1dcbxLocETSConPriority]);
    return SNMP_SUCCESS;

}

/* LOW LEVEL Routines for Table : LldpXdot1dcbxLocETSConTrafficClassBandwidthTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceLldpXdot1dcbxLocETSConTrafficClassBandwidthTable
 Input       :  The Indices
                LldpV2LocPortIfIndex
                LldpXdot1dcbxLocETSConTrafficClass
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceLldpXdot1dcbxLocETSConTrafficClassBandwidthTable (INT4
                                                                          i4LldpV2LocPortIfIndex,
                                                                          UINT4
                                                                          u4LldpXdot1dcbxLocETSConTrafficClass)
{
    tETSPortEntry      *pETSPortEntry = NULL;

    ETS_SHUTDOWN_CHECK;

    DCBX_TRC_ARG3 (DCBX_MGMT_TRC, "%s, Args: "
                   "i4LldpV2LocPortIfIndex: %d "
                   "u4LldpXdot1dcbxLocETSConTrafficClass: %d\r\n",
                   __func__, i4LldpV2LocPortIfIndex,
                   u4LldpXdot1dcbxLocETSConTrafficClass);

    pETSPortEntry = ETSUtlGetPortEntry ((UINT4) i4LldpV2LocPortIfIndex);
    if (pETSPortEntry == NULL)
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : ETSUtlGetPortEntry() "
                       "Returns NULL. \r\n", __FUNCTION__);
        return SNMP_FAILURE;
    }
    if (((INT4) u4LldpXdot1dcbxLocETSConTrafficClass <
         DCBX_ZERO)
        ||
        ((u4LldpXdot1dcbxLocETSConTrafficClass >=
          ETS_MAX_TCGID_CONF)
         && ((u4LldpXdot1dcbxLocETSConTrafficClass) != ETS_DEF_TCGID)))
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC,
                       "In %s : Traffic class is not in range(0-7)"
                       " \r\n", __FUNCTION__);
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFirstIndexLldpXdot1dcbxLocETSConTrafficClassBandwidthTable
 Input       :  The Indices
                LldpV2LocPortIfIndex
                LldpXdot1dcbxLocETSConTrafficClass
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexLldpXdot1dcbxLocETSConTrafficClassBandwidthTable (INT4
                                                                  *pi4LldpV2LocPortIfIndex,
                                                                  UINT4
                                                                  *pu4LldpXdot1dcbxLocETSConTrafficClass)
{
    tETSPortEntry      *pETSPortEntry = NULL;

    ETS_SHUTDOWN_CHECK;

    DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "%s \r\n", __func__);

    pETSPortEntry = (tETSPortEntry *)
        RBTreeGetFirst (gETSGlobalInfo.pRbETSPortTbl);
    if (pETSPortEntry == NULL)
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : ETSUtlGetPortEntry() "
                       "Returns NULL. \r\n", __FUNCTION__);
        return SNMP_FAILURE;
    }
    *pi4LldpV2LocPortIfIndex = (INT4) pETSPortEntry->u4IfIndex;
    *pu4LldpXdot1dcbxLocETSConTrafficClass = DCBX_ZERO;
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetNextIndexLldpXdot1dcbxLocETSConTrafficClassBandwidthTable
 Input       :  The Indices
                LldpV2LocPortIfIndex
                nextLldpV2LocPortIfIndex
                LldpXdot1dcbxLocETSConTrafficClass
                nextLldpXdot1dcbxLocETSConTrafficClass
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexLldpXdot1dcbxLocETSConTrafficClassBandwidthTable (INT4
                                                                 i4LldpV2LocPortIfIndex,
                                                                 INT4
                                                                 *pi4NextLldpV2LocPortIfIndex,
                                                                 UINT4
                                                                 u4LldpXdot1dcbxLocETSConTrafficClass,
                                                                 UINT4
                                                                 *pu4NextLldpXdot1dcbxLocETSConTrafficClass)
{
    tETSPortEntry      *pETSPortEntry = NULL;
    tETSPortEntry       ETSPortEntry;

    ETS_SHUTDOWN_CHECK;

    DCBX_TRC_ARG3 (DCBX_MGMT_TRC, "%s, Args: "
                   "i4LldpV2LocPortIfIndex: %d "
                   "u4LldpXdot1dcbxLocETSConTrafficClass: %d\r\n",
                   __func__, i4LldpV2LocPortIfIndex,
                   u4LldpXdot1dcbxLocETSConTrafficClass);

    MEMSET (&ETSPortEntry, DCBX_ZERO, sizeof (tETSPortEntry));

    ETSPortEntry.u4IfIndex = (UINT4) i4LldpV2LocPortIfIndex;
    pETSPortEntry = ETSUtlGetPortEntry ((UINT4) i4LldpV2LocPortIfIndex);

    if ((pETSPortEntry == NULL) ||
        (u4LldpXdot1dcbxLocETSConTrafficClass >= ETS_TCGID7))
    {
        pETSPortEntry = (tETSPortEntry *)
            RBTreeGetNext (gETSGlobalInfo.pRbETSPortTbl,
                           &ETSPortEntry, DcbxUtlPortTblCmpFn);
        if (pETSPortEntry == NULL)
        {

            DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : RbTreeGetNExt() "
                           "Returns NULL. \r\n", __FUNCTION__);
            return SNMP_FAILURE;
        }
        *pi4NextLldpV2LocPortIfIndex = (INT4) pETSPortEntry->u4IfIndex;
        *pu4NextLldpXdot1dcbxLocETSConTrafficClass = DCBX_ZERO;
    }
    else
    {
        *pi4NextLldpV2LocPortIfIndex = i4LldpV2LocPortIfIndex;
        *pu4NextLldpXdot1dcbxLocETSConTrafficClass =
            u4LldpXdot1dcbxLocETSConTrafficClass + DCBX_MIN_VAL;
    }
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetLldpXdot1dcbxLocETSConTrafficClassBandwidth
 Input       :  The Indices
                LldpV2LocPortIfIndex
                LldpXdot1dcbxLocETSConTrafficClass

                The Object 
                retValLldpXdot1dcbxLocETSConTrafficClassBandwidth
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetLldpXdot1dcbxLocETSConTrafficClassBandwidth (INT4 i4LldpV2LocPortIfIndex,
                                                   UINT4
                                                   u4LldpXdot1dcbxLocETSConTrafficClass,
                                                   UINT4
                                                   *pu4RetValLldpXdot1dcbxLocETSConTrafficClassBandwidth)
{
    tETSPortEntry      *pETSPortEntry = NULL;

    DCBX_TRC_ARG3 (DCBX_MGMT_TRC, "%s, Args: "
                   "i4LldpV2LocPortIfIndex: %d "
                   "u4LldpXdot1dcbxLocETSConTrafficClass: %d\r\n",
                   __func__, i4LldpV2LocPortIfIndex,
                   u4LldpXdot1dcbxLocETSConTrafficClass);

    pETSPortEntry = ETSUtlGetPortEntry ((UINT4) i4LldpV2LocPortIfIndex);
    if (pETSPortEntry == NULL)
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : ETSUtlGetPortEntry ()"
                       "Returns NULL. \r\n", __FUNCTION__);
        return SNMP_FAILURE;
    }
    *pu4RetValLldpXdot1dcbxLocETSConTrafficClassBandwidth =
        (INT4) ((pETSPortEntry->ETSLocPortInfo).
                au1ETSLocBW[u4LldpXdot1dcbxLocETSConTrafficClass]);
    return SNMP_SUCCESS;

}

/* LOW LEVEL Routines for Table : LldpXdot1dcbxLocETSConTrafficSelectionAlgorithmTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceLldpXdot1dcbxLocETSConTrafficSelectionAlgorithmTable
 Input       :  The Indices
                LldpV2LocPortIfIndex
                LldpXdot1dcbxLocETSConTSATrafficClass
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceLldpXdot1dcbxLocETSConTrafficSelectionAlgorithmTable
    (INT4 i4LldpV2LocPortIfIndex, UINT4 u4LldpXdot1dcbxLocETSConTSATrafficClass)
{
    tETSPortEntry      *pETSPortEntry = NULL;

    if (DcbxUtlValidatePort ((UINT4) i4LldpV2LocPortIfIndex) == OSIX_FAILURE)
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : DcbxUtlValidatePort() "
                       "Returns Failure. \r\n", __FUNCTION__);
        CLI_SET_ERR (CLI_ERR_DCBX_NO_PORT);
        return SNMP_FAILURE;
    }

    ETS_SHUTDOWN_CHECK;

    DCBX_TRC_ARG3 (DCBX_MGMT_TRC, "%s, Args: "
                   "i4LldpV2LocPortIfIndex: %d u4LldpXdot1dcbxLocETSConTSATrafficClass :%d\r\n",
                   __func__, i4LldpV2LocPortIfIndex,
                   u4LldpXdot1dcbxLocETSConTSATrafficClass);

    pETSPortEntry = ETSUtlGetPortEntry ((UINT4) i4LldpV2LocPortIfIndex);
    if (pETSPortEntry == NULL)
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : ETSUtlGetPortEntry () "
                       " Returns FAILURE. \r\n", __FUNCTION__);
        CLI_SET_ERR (CLI_ERR_ETS_NO_PORT_ENTRY);
        return SNMP_FAILURE;
    }

    if (((INT4) u4LldpXdot1dcbxLocETSConTSATrafficClass <
         DCBX_ZERO)
        ||
        ((u4LldpXdot1dcbxLocETSConTSATrafficClass >=
          ETS_MAX_TCGID_CONF)
         && ((u4LldpXdot1dcbxLocETSConTSATrafficClass) != ETS_DEF_TCGID)))
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC,
                       "In %s : Traffic class is not in range(0-7)" " \r\n",
                       __FUNCTION__);
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFirstIndexLldpXdot1dcbxLocETSConTrafficSelectionAlgorithmTable
 Input       :  The Indices
                LldpV2LocPortIfIndex
                LldpXdot1dcbxLocETSConTSATrafficClass
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexLldpXdot1dcbxLocETSConTrafficSelectionAlgorithmTable (INT4
                                                                      *pi4LldpV2LocPortIfIndex,
                                                                      UINT4
                                                                      *pu4LldpXdot1dcbxLocETSConTSATrafficClass)
{
    tETSPortEntry      *pETSPortEntry = NULL;

    ETS_SHUTDOWN_CHECK;

    DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "%s\r\n", __func__);

    pETSPortEntry = (tETSPortEntry *)
        RBTreeGetFirst (gETSGlobalInfo.pRbETSPortTbl);
    if (pETSPortEntry == NULL)
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : RbTreeGetFirst Node () "
                       "Returns NULL. \r\n", __FUNCTION__);
        return SNMP_FAILURE;
    }

    *pi4LldpV2LocPortIfIndex = (INT4) pETSPortEntry->u4IfIndex;
    *pu4LldpXdot1dcbxLocETSConTSATrafficClass = DCBX_ZERO;
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetNextIndexLldpXdot1dcbxLocETSConTrafficSelectionAlgorithmTable
 Input       :  The Indices
                LldpV2LocPortIfIndex
                nextLldpV2LocPortIfIndex
                LldpXdot1dcbxLocETSConTSATrafficClass
                nextLldpXdot1dcbxLocETSConTSATrafficClass
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexLldpXdot1dcbxLocETSConTrafficSelectionAlgorithmTable (INT4
                                                                     i4LldpV2LocPortIfIndex,
                                                                     INT4
                                                                     *pi4NextLldpV2LocPortIfIndex,
                                                                     UINT4
                                                                     u4LldpXdot1dcbxLocETSConTSATrafficClass,
                                                                     UINT4
                                                                     *pu4NextLldpXdot1dcbxLocETSConTSATrafficClass)
{
    tETSPortEntry      *pETSPortEntry = NULL;
    tETSPortEntry       ETSPortEntry;

    ETS_SHUTDOWN_CHECK;

    DCBX_TRC_ARG3 (DCBX_MGMT_TRC, "%s, Args: "
                   "i4LldpV2LocPortIfIndex: %d "
                   "u4LldpXdot1dcbxLocETSConTSATrafficClass: %d\r\n",
                   __func__, i4LldpV2LocPortIfIndex,
                   u4LldpXdot1dcbxLocETSConTSATrafficClass);

    MEMSET (&ETSPortEntry, DCBX_ZERO, sizeof (tETSPortEntry));

    ETSPortEntry.u4IfIndex = (UINT4) i4LldpV2LocPortIfIndex;
    pETSPortEntry = ETSUtlGetPortEntry ((UINT4) i4LldpV2LocPortIfIndex);
    if ((pETSPortEntry == NULL) ||
        (u4LldpXdot1dcbxLocETSConTSATrafficClass >= ETS_TCGID7))
    {
        pETSPortEntry = (tETSPortEntry *)
            RBTreeGetNext (gETSGlobalInfo.pRbETSPortTbl,
                           &ETSPortEntry, DcbxUtlPortTblCmpFn);
        if (pETSPortEntry == NULL)
        {

            DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : RbTreeGetNExt() "
                           "Returns NULL. \r\n", __FUNCTION__);
            return SNMP_FAILURE;
        }
        *pi4NextLldpV2LocPortIfIndex = (INT4) pETSPortEntry->u4IfIndex;
        *pu4NextLldpXdot1dcbxLocETSConTSATrafficClass = DCBX_ZERO;
    }
    else
    {
        *pi4NextLldpV2LocPortIfIndex = i4LldpV2LocPortIfIndex;
        *pu4NextLldpXdot1dcbxLocETSConTSATrafficClass =
            u4LldpXdot1dcbxLocETSConTSATrafficClass + DCBX_MIN_VAL;
    }

    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetLldpXdot1dcbxLocETSConTrafficSelectionAlgorithm
 Input       :  The Indices
                LldpV2LocPortIfIndex
                LldpXdot1dcbxLocETSConTSATrafficClass

                The Object 
                retValLldpXdot1dcbxLocETSConTrafficSelectionAlgorithm
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetLldpXdot1dcbxLocETSConTrafficSelectionAlgorithm (INT4
                                                       i4LldpV2LocPortIfIndex,
                                                       UINT4
                                                       u4LldpXdot1dcbxLocETSConTSATrafficClass,
                                                       INT4
                                                       *pi4RetValLldpXdot1dcbxLocETSConTrafficSelectionAlgorithm)
{
    tETSPortEntry      *pETSPortEntry = NULL;

    DCBX_TRC_ARG3 (DCBX_MGMT_TRC, "%s, Args: "
                   "i4LldpV2LocPortIfIndex: %d "
                   "u4LldpXdot1dcbxLocETSConTSATrafficClass: %d\r\n",
                   __func__, i4LldpV2LocPortIfIndex,
                   u4LldpXdot1dcbxLocETSConTSATrafficClass);

    pETSPortEntry = ETSUtlGetPortEntry ((UINT4) i4LldpV2LocPortIfIndex);
    if (pETSPortEntry == NULL)
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : ETSUtlGetPortEntry () "
                       " Returns FAILURE. \r\n", __FUNCTION__);
        return SNMP_FAILURE;
    }

    *pi4RetValLldpXdot1dcbxLocETSConTrafficSelectionAlgorithm =
        pETSPortEntry->ETSLocPortInfo.
        au1ETSLocTsaTable[u4LldpXdot1dcbxLocETSConTSATrafficClass];
    return SNMP_SUCCESS;

}

/* LOW LEVEL Routines for Table : LldpXdot1dcbxLocETSRecoTrafficClassBandwidthTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceLldpXdot1dcbxLocETSRecoTrafficClassBandwidthTable
 Input       :  The Indices
                LldpV2LocPortIfIndex
                LldpXdot1dcbxLocETSRecoTrafficClass
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceLldpXdot1dcbxLocETSRecoTrafficClassBandwidthTable (INT4
                                                                           i4LldpV2LocPortIfIndex,
                                                                           UINT4
                                                                           u4LldpXdot1dcbxLocETSRecoTrafficClass)
{
    tETSPortEntry      *pETSPortEntry = NULL;

    ETS_SHUTDOWN_CHECK;

    DCBX_TRC_ARG3 (DCBX_MGMT_TRC, "%s, Args: "
                   "i4LldpV2LocPortIfIndex: %d u4LldpXdot1dcbxLocETSRecoTrafficClass: %d\r\n",
                   __func__, i4LldpV2LocPortIfIndex,
                   u4LldpXdot1dcbxLocETSRecoTrafficClass);

    pETSPortEntry = ETSUtlGetPortEntry ((UINT4) i4LldpV2LocPortIfIndex);
    if (pETSPortEntry == NULL)
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : ETSUtlGetPortEntry() "
                       "Returns NULL. \r\n", __FUNCTION__);
        return SNMP_FAILURE;
    }
    if (((INT4) u4LldpXdot1dcbxLocETSRecoTrafficClass <
         DCBX_ZERO)
        ||
        ((u4LldpXdot1dcbxLocETSRecoTrafficClass >=
          ETS_MAX_TCGID_CONF)
         && ((u4LldpXdot1dcbxLocETSRecoTrafficClass) != ETS_DEF_TCGID)))
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC,
                       "In %s : Traffic class is not in range(0-7)" " \r\n",
                       __FUNCTION__);
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexLldpXdot1dcbxLocETSRecoTrafficClassBandwidthTable
 Input       :  The Indices
                LldpV2LocPortIfIndex
                LldpXdot1dcbxLocETSRecoTrafficClass
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexLldpXdot1dcbxLocETSRecoTrafficClassBandwidthTable (INT4
                                                                   *pi4LldpV2LocPortIfIndex,
                                                                   UINT4
                                                                   *pu4LldpXdot1dcbxLocETSRecoTrafficClass)
{
    tETSPortEntry      *pETSPortEntry = NULL;

    ETS_SHUTDOWN_CHECK;

    DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "%s\r\n", __func__);

    pETSPortEntry = (tETSPortEntry *)
        RBTreeGetFirst (gETSGlobalInfo.pRbETSPortTbl);

    if (pETSPortEntry == NULL)
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : ETSUtlGetPortEntry() "
                       "Returns NULL. \r\n", __FUNCTION__);
        return SNMP_FAILURE;
    }
    *pi4LldpV2LocPortIfIndex = (INT4) pETSPortEntry->u4IfIndex;
    *pu4LldpXdot1dcbxLocETSRecoTrafficClass = DCBX_ZERO;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexLldpXdot1dcbxLocETSRecoTrafficClassBandwidthTable
 Input       :  The Indices
                LldpV2LocPortIfIndex
                nextLldpV2LocPortIfIndex
                LldpXdot1dcbxLocETSRecoTrafficClass
                nextLldpXdot1dcbxLocETSRecoTrafficClass
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexLldpXdot1dcbxLocETSRecoTrafficClassBandwidthTable (INT4
                                                                  i4LldpV2LocPortIfIndex,
                                                                  INT4
                                                                  *pi4NextLldpV2LocPortIfIndex,
                                                                  UINT4
                                                                  u4LldpXdot1dcbxLocETSRecoTrafficClass,
                                                                  UINT4
                                                                  *pu4NextLldpXdot1dcbxLocETSRecoTrafficClass)
{
    tETSPortEntry      *pETSPortEntry = NULL;
    tETSPortEntry       ETSPortEntry;

    ETS_SHUTDOWN_CHECK;

    DCBX_TRC_ARG3 (DCBX_MGMT_TRC, "%s, Args: "
                   "i4LldpV2LocPortIfIndex: %d "
                   "u4LldpXdot1dcbxLocETSRecoTrafficClass: %d\r\n", __func__,
                   i4LldpV2LocPortIfIndex,
                   u4LldpXdot1dcbxLocETSRecoTrafficClass);

    MEMSET (&ETSPortEntry, DCBX_ZERO, sizeof (tETSPortEntry));

    ETSPortEntry.u4IfIndex = (UINT4) i4LldpV2LocPortIfIndex;
    pETSPortEntry = ETSUtlGetPortEntry ((UINT4) i4LldpV2LocPortIfIndex);

    if ((pETSPortEntry == NULL) ||
        (u4LldpXdot1dcbxLocETSRecoTrafficClass >= ETS_TCGID7))
    {
        pETSPortEntry = (tETSPortEntry *)
            RBTreeGetNext (gETSGlobalInfo.pRbETSPortTbl,
                           &ETSPortEntry, DcbxUtlPortTblCmpFn);
        if (pETSPortEntry == NULL)
        {

            DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : RbTreeGetNExt() "
                           "Returns NULL. \r\n", __FUNCTION__);
            return SNMP_FAILURE;
        }
        *pi4NextLldpV2LocPortIfIndex = (INT4) pETSPortEntry->u4IfIndex;
        *pu4NextLldpXdot1dcbxLocETSRecoTrafficClass = DCBX_ZERO;
    }
    else
    {
        *pi4NextLldpV2LocPortIfIndex = i4LldpV2LocPortIfIndex;
        *pu4NextLldpXdot1dcbxLocETSRecoTrafficClass =
            u4LldpXdot1dcbxLocETSRecoTrafficClass + DCBX_MIN_VAL;
    }
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetLldpXdot1dcbxLocETSRecoTrafficClassBandwidth
 Input       :  The Indices
                LldpV2LocPortIfIndex
                LldpXdot1dcbxLocETSRecoTrafficClass

                The Object 
                retValLldpXdot1dcbxLocETSRecoTrafficClassBandwidth
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetLldpXdot1dcbxLocETSRecoTrafficClassBandwidth (INT4 i4LldpV2LocPortIfIndex,
                                                    UINT4
                                                    u4LldpXdot1dcbxLocETSRecoTrafficClass,
                                                    UINT4
                                                    *pu4RetValLldpXdot1dcbxLocETSRecoTrafficClassBandwidth)
{
    tETSPortEntry      *pETSPortEntry = NULL;

    DCBX_TRC_ARG3 (DCBX_MGMT_TRC, "%s, Args: "
                   "i4LldpV2LocPortIfIndex: %d "
                   "u4LldpXdot1dcbxLocETSRecoTrafficClass: %d\r\n",
                   __func__, i4LldpV2LocPortIfIndex,
                   u4LldpXdot1dcbxLocETSRecoTrafficClass);

    pETSPortEntry = ETSUtlGetPortEntry ((UINT4) i4LldpV2LocPortIfIndex);
    if (pETSPortEntry == NULL)
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : ETSUtlGetPortEntry ()"
                       "Returns NULL. \r\n", __FUNCTION__);
        return SNMP_FAILURE;
    }
    *pu4RetValLldpXdot1dcbxLocETSRecoTrafficClassBandwidth =
        (INT4) ((pETSPortEntry->ETSLocPortInfo).
                au1ETSLocRecoBW[u4LldpXdot1dcbxLocETSRecoTrafficClass]);
    return SNMP_SUCCESS;

}

/* LOW LEVEL Routines for Table : LldpXdot1dcbxLocETSRecoTrafficSelectionAlgorithmTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceLldpXdot1dcbxLocETSRecoTrafficSelectionAlgorithmTable
 Input       :  The Indices
                LldpV2LocPortIfIndex
                LldpXdot1dcbxLocETSRecoTSATrafficClass
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1 
     
     
     
     
     
     
     
    nmhValidateIndexInstanceLldpXdot1dcbxLocETSRecoTrafficSelectionAlgorithmTable
    (INT4 i4LldpV2LocPortIfIndex,
     UINT4 u4LldpXdot1dcbxLocETSRecoTSATrafficClass)
{
    tETSPortEntry      *pETSPortEntry = NULL;

    ETS_SHUTDOWN_CHECK;

    DCBX_TRC_ARG3 (DCBX_MGMT_TRC, "%s, Args: "
                   "i4LldpV2LocPortIfIndex: %d "
                   "u4LldpXdot1dcbxLocETSRecoTSATrafficClass: %d\r\n",
                   __func__, i4LldpV2LocPortIfIndex,
                   u4LldpXdot1dcbxLocETSRecoTSATrafficClass);

    pETSPortEntry = ETSUtlGetPortEntry ((UINT4) i4LldpV2LocPortIfIndex);
    if (pETSPortEntry == NULL)
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : ETSUtlGetPortEntry() "
                       "Returns NULL. \r\n", __FUNCTION__);
        return SNMP_FAILURE;
    }

    if (((INT4) u4LldpXdot1dcbxLocETSRecoTSATrafficClass <
         DCBX_ZERO)
        ||
        ((u4LldpXdot1dcbxLocETSRecoTSATrafficClass >=
          ETS_MAX_TCGID_CONF)
         && ((u4LldpXdot1dcbxLocETSRecoTSATrafficClass) != ETS_DEF_TCGID)))
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC,
                       "In %s : Tsa table index is not in range(0-7)"
                       " \r\n", __FUNCTION__);
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFirstIndexLldpXdot1dcbxLocETSRecoTrafficSelectionAlgorithmTable
 Input       :  The Indices
                LldpV2LocPortIfIndex
                LldpXdot1dcbxLocETSRecoTSATrafficClass
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexLldpXdot1dcbxLocETSRecoTrafficSelectionAlgorithmTable (INT4
                                                                       *pi4LldpV2LocPortIfIndex,
                                                                       UINT4
                                                                       *pu4LldpXdot1dcbxLocETSRecoTSATrafficClass)
{
    tETSPortEntry      *pETSPortEntry = NULL;

    ETS_SHUTDOWN_CHECK;

    DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "%s\r\n", __func__);

    pETSPortEntry = (tETSPortEntry *)
        RBTreeGetFirst (gETSGlobalInfo.pRbETSPortTbl);
    if (pETSPortEntry == NULL)
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : RbTree GetFirst() "
                       "Returns NULL. \r\n", __FUNCTION__);
        return SNMP_FAILURE;
    }

    *pi4LldpV2LocPortIfIndex = (INT4) pETSPortEntry->u4IfIndex;
    *pu4LldpXdot1dcbxLocETSRecoTSATrafficClass = DCBX_ZERO;
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetNextIndexLldpXdot1dcbxLocETSRecoTrafficSelectionAlgorithmTable
 Input       :  The Indices
                LldpV2LocPortIfIndex
                nextLldpV2LocPortIfIndex
                LldpXdot1dcbxLocETSRecoTSATrafficClass
                nextLldpXdot1dcbxLocETSRecoTSATrafficClass
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexLldpXdot1dcbxLocETSRecoTrafficSelectionAlgorithmTable (INT4
                                                                      i4LldpV2LocPortIfIndex,
                                                                      INT4
                                                                      *pi4NextLldpV2LocPortIfIndex,
                                                                      UINT4
                                                                      u4LldpXdot1dcbxLocETSRecoTSATrafficClass,
                                                                      UINT4
                                                                      *pu4NextLldpXdot1dcbxLocETSRecoTSATrafficClass)
{
    tETSPortEntry      *pETSPortEntry = NULL;
    tETSPortEntry       ETSPortEntry;

    ETS_SHUTDOWN_CHECK;

    DCBX_TRC_ARG3 (DCBX_MGMT_TRC, "%s, Args: "
                   "i4LldpV2LocPortIfIndex: %d "
                   "u4LldpXdot1dcbxLocETSRecoTSATrafficClass: %d\r\n",
                   __func__, i4LldpV2LocPortIfIndex,
                   u4LldpXdot1dcbxLocETSRecoTSATrafficClass);

    MEMSET (&ETSPortEntry, DCBX_ZERO, sizeof (tETSPortEntry));

    ETSPortEntry.u4IfIndex = (UINT4) i4LldpV2LocPortIfIndex;
    pETSPortEntry = ETSUtlGetPortEntry ((UINT4) i4LldpV2LocPortIfIndex);
    if ((pETSPortEntry == NULL) ||
        (u4LldpXdot1dcbxLocETSRecoTSATrafficClass >= ETS_TCGID7))
    {
        pETSPortEntry = (tETSPortEntry *)
            RBTreeGetNext (gETSGlobalInfo.pRbETSPortTbl,
                           &ETSPortEntry, DcbxUtlPortTblCmpFn);
        if (pETSPortEntry == NULL)
        {

            DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : RbTreeGetNExt() "
                           "Returns NULL. \r\n", __FUNCTION__);
            return SNMP_FAILURE;
        }
        *pi4NextLldpV2LocPortIfIndex = (INT4) pETSPortEntry->u4IfIndex;
        *pu4NextLldpXdot1dcbxLocETSRecoTSATrafficClass = DCBX_ZERO;
    }
    else
    {
        *pi4NextLldpV2LocPortIfIndex = i4LldpV2LocPortIfIndex;
        *pu4NextLldpXdot1dcbxLocETSRecoTSATrafficClass =
            u4LldpXdot1dcbxLocETSRecoTSATrafficClass + DCBX_MIN_VAL;
    }
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetLldpXdot1dcbxLocETSRecoTrafficSelectionAlgorithm
 Input       :  The Indices
                LldpV2LocPortIfIndex
                LldpXdot1dcbxLocETSRecoTSATrafficClass

                The Object 
                retValLldpXdot1dcbxLocETSRecoTrafficSelectionAlgorithm
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetLldpXdot1dcbxLocETSRecoTrafficSelectionAlgorithm (INT4
                                                        i4LldpV2LocPortIfIndex,
                                                        UINT4
                                                        u4LldpXdot1dcbxLocETSRecoTSATrafficClass,
                                                        INT4
                                                        *pi4RetValLldpXdot1dcbxLocETSRecoTrafficSelectionAlgorithm)
{
    tETSPortEntry      *pETSPortEntry = NULL;

    DCBX_TRC_ARG3 (DCBX_MGMT_TRC, "%s, Args: "
                   "i4LldpV2LocPortIfIndex: %d "
                   "u4LldpXdot1dcbxLocETSRecoTSATrafficClass: %d\r\n",
                   __func__, i4LldpV2LocPortIfIndex,
                   u4LldpXdot1dcbxLocETSRecoTSATrafficClass);

    pETSPortEntry = ETSUtlGetPortEntry ((UINT4) i4LldpV2LocPortIfIndex);
    if (pETSPortEntry == NULL)
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : ETSUtlGetPortEntry () "
                       " Returns FAILURE. \r\n", __FUNCTION__);
        return SNMP_FAILURE;
    }
    *pi4RetValLldpXdot1dcbxLocETSRecoTrafficSelectionAlgorithm =
        pETSPortEntry->ETSLocPortInfo.
        au1ETSLocRecoTsaTable[u4LldpXdot1dcbxLocETSRecoTSATrafficClass];
    return SNMP_SUCCESS;

}

/* LOW LEVEL Routines for Table : LldpXdot1dcbxLocPFCBasicTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceLldpXdot1dcbxLocPFCBasicTable
 Input       :  The Indices
                LldpV2LocPortIfIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceLldpXdot1dcbxLocPFCBasicTable (INT4
                                                       i4LldpV2LocPortIfIndex)
{
    tPFCPortEntry      *pPFCPortEntry = NULL;

    PFC_SHUTDOWN_CHECK;

    DCBX_TRC_ARG2 (DCBX_MGMT_TRC, "%s, Args: "
                   "i4LldpV2LocPortIfIndex: %d\r\n",
                   __func__, i4LldpV2LocPortIfIndex);

    pPFCPortEntry = PFCUtlGetPortEntry ((UINT4) i4LldpV2LocPortIfIndex);
    if (pPFCPortEntry == NULL)
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : PFCUtlGetPortEntry() "
                       "Returns NULL. \r\n", __FUNCTION__);
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFirstIndexLldpXdot1dcbxLocPFCBasicTable
 Input       :  The Indices
                LldpV2LocPortIfIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexLldpXdot1dcbxLocPFCBasicTable (INT4 *pi4LldpV2LocPortIfIndex)
{
    tPFCPortEntry      *pPFCPortEntry = NULL;

    if (gPFCGlobalInfo.u1PFCSystemCtrl == PFC_SHUTDOWN)
    {
        DCBX_TRC (DCBX_MGMT_TRC, PFC_MODULE_SHUTDOW_ERROR_MSG);
        return (SNMP_FAILURE);
    }

    DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "%s\r\n", __func__);

    pPFCPortEntry = (tPFCPortEntry *)
        RBTreeGetFirst (gPFCGlobalInfo.pRbPFCPortTbl);
    if (pPFCPortEntry == NULL)
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : RBtreeGetFirst() "
                       "Returns NULL. \r\n", __FUNCTION__);
        return SNMP_FAILURE;
    }

    *pi4LldpV2LocPortIfIndex = (INT4) pPFCPortEntry->u4IfIndex;
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetNextIndexLldpXdot1dcbxLocPFCBasicTable
 Input       :  The Indices
                LldpV2LocPortIfIndex
                nextLldpV2LocPortIfIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexLldpXdot1dcbxLocPFCBasicTable (INT4 i4LldpV2LocPortIfIndex,
                                              INT4 *pi4NextLldpV2LocPortIfIndex)
{
    tPFCPortEntry      *pPFCPortEntry = NULL;
    tPFCPortEntry       PFCPortEntry;

    PFC_SHUTDOWN_CHECK;

    MEMSET (&PFCPortEntry, DCBX_ZERO, sizeof (tPFCPortEntry));
    PFCPortEntry.u4IfIndex = (UINT4) i4LldpV2LocPortIfIndex;
    pPFCPortEntry = (tPFCPortEntry *)
        RBTreeGetNext (gPFCGlobalInfo.pRbPFCPortTbl,
                       &PFCPortEntry, DcbxUtlPortTblCmpFn);
    if (pPFCPortEntry == NULL)
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : RBtreeGetNExt() "
                       "Returns NULL. \r\n", __FUNCTION__);
        return SNMP_FAILURE;
    }

    *pi4NextLldpV2LocPortIfIndex = (INT4) pPFCPortEntry->u4IfIndex;
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetLldpXdot1dcbxLocPFCWilling
 Input       :  The Indices
                LldpV2LocPortIfIndex

                The Object 
                retValLldpXdot1dcbxLocPFCWilling
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetLldpXdot1dcbxLocPFCWilling (INT4 i4LldpV2LocPortIfIndex,
                                  INT4 *pi4RetValLldpXdot1dcbxLocPFCWilling)
{
    tPFCPortEntry      *pPFCPortEntry = NULL;

    pPFCPortEntry = PFCUtlGetPortEntry ((UINT4) i4LldpV2LocPortIfIndex);
    if (pPFCPortEntry == NULL)
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : ETSGetDataPortEntry () "
                       " Returns FAILURE. \r\n", __FUNCTION__);
        return SNMP_FAILURE;
    }

    *pi4RetValLldpXdot1dcbxLocPFCWilling =
        pPFCPortEntry->PFCLocPortInfo.u1PFCLocWilling;
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetLldpXdot1dcbxLocPFCMBC
 Input       :  The Indices
                LldpV2LocPortIfIndex

                The Object 
                retValLldpXdot1dcbxLocPFCMBC
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetLldpXdot1dcbxLocPFCMBC (INT4 i4LldpV2LocPortIfIndex,
                              INT4 *pi4RetValLldpXdot1dcbxLocPFCMBC)
{
    tPFCPortEntry      *pPFCPortEntry = NULL;

    pPFCPortEntry = PFCUtlGetPortEntry ((UINT4) i4LldpV2LocPortIfIndex);
    if (pPFCPortEntry == NULL)
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : ETSGetDataPortEntry () "
                       " Returns FAILURE. \r\n", __FUNCTION__);
        return SNMP_FAILURE;
    }

    *pi4RetValLldpXdot1dcbxLocPFCMBC =
        pPFCPortEntry->PFCLocPortInfo.u1PFCLocMBC;
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetLldpXdot1dcbxLocPFCCap
 Input       :  The Indices
                LldpV2LocPortIfIndex

                The Object 
                retValLldpXdot1dcbxLocPFCCap
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetLldpXdot1dcbxLocPFCCap (INT4 i4LldpV2LocPortIfIndex,
                              UINT4 *pu4RetValLldpXdot1dcbxLocPFCCap)
{
    tPFCPortEntry      *pPFCPortEntry = NULL;

    pPFCPortEntry = PFCUtlGetPortEntry ((UINT4) i4LldpV2LocPortIfIndex);
    if (pPFCPortEntry == NULL)
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : ETSGetDataPortEntry () "
                       " Returns FAILURE. \r\n", __FUNCTION__);
        return SNMP_FAILURE;
    }

    *pu4RetValLldpXdot1dcbxLocPFCCap =
        pPFCPortEntry->PFCLocPortInfo.u1PFCLocCap;
    return SNMP_SUCCESS;

}

/* LOW LEVEL Routines for Table : LldpXdot1dcbxLocPFCEnableTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceLldpXdot1dcbxLocPFCEnableTable
 Input       :  The Indices
                LldpV2LocPortIfIndex
                LldpXdot1dcbxLocPFCEnablePriority
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceLldpXdot1dcbxLocPFCEnableTable (INT4
                                                        i4LldpV2LocPortIfIndex,
                                                        UINT4
                                                        u4LldpXdot1dcbxLocPFCEnablePriority)
{
    tPFCPortEntry      *pPFCPortEntry = NULL;

    PFC_SHUTDOWN_CHECK;

    pPFCPortEntry = PFCUtlGetPortEntry ((UINT4) i4LldpV2LocPortIfIndex);
    if (pPFCPortEntry == NULL)
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : PFCUtlGetPortEntry() "
                       "Returns NULL. \r\n", __FUNCTION__);
        return SNMP_FAILURE;
    }

    if (((INT4) u4LldpXdot1dcbxLocPFCEnablePriority < DCBX_ZERO)
        || (u4LldpXdot1dcbxLocPFCEnablePriority >= DCBX_MAX_PRIORITIES))
    {
        DCBX_TRC (DCBX_MGMT_TRC, WRONG_VAL);
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFirstIndexLldpXdot1dcbxLocPFCEnableTable
 Input       :  The Indices
                LldpV2LocPortIfIndex
                LldpXdot1dcbxLocPFCEnablePriority
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexLldpXdot1dcbxLocPFCEnableTable (INT4 *pi4LldpV2LocPortIfIndex,
                                                UINT4
                                                *pu4LldpXdot1dcbxLocPFCEnablePriority)
{
    tPFCPortEntry      *pPFCPortEntry = NULL;

    pPFCPortEntry = (tPFCPortEntry *)
        RBTreeGetFirst (gPFCGlobalInfo.pRbPFCPortTbl);
    if (pPFCPortEntry == NULL)
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : RBtreeGetFirst() "
                       "Returns NULL. \r\n", __FUNCTION__);
        return SNMP_FAILURE;
    }

    *pi4LldpV2LocPortIfIndex = (INT4) pPFCPortEntry->u4IfIndex;
    *pu4LldpXdot1dcbxLocPFCEnablePriority = DCBX_ZERO;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexLldpXdot1dcbxLocPFCEnableTable
 Input       :  The Indices
                LldpV2LocPortIfIndex
                nextLldpV2LocPortIfIndex
                LldpXdot1dcbxLocPFCEnablePriority
                nextLldpXdot1dcbxLocPFCEnablePriority
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexLldpXdot1dcbxLocPFCEnableTable (INT4 i4LldpV2LocPortIfIndex,
                                               INT4
                                               *pi4NextLldpV2LocPortIfIndex,
                                               UINT4
                                               u4LldpXdot1dcbxLocPFCEnablePriority,
                                               UINT4
                                               *pu4NextLldpXdot1dcbxLocPFCEnablePriority)
{
    tPFCPortEntry      *pPFCPortEntry = NULL;
    tPFCPortEntry       PFCPortEntry;

    PFC_SHUTDOWN_CHECK;

    MEMSET (&PFCPortEntry, DCBX_ZERO, sizeof (tPFCPortEntry));
    PFCPortEntry.u4IfIndex = (UINT4) i4LldpV2LocPortIfIndex;
    pPFCPortEntry = PFCUtlGetPortEntry ((UINT4) i4LldpV2LocPortIfIndex);

    if ((pPFCPortEntry == NULL) ||
        (u4LldpXdot1dcbxLocPFCEnablePriority >= DCBX_MAX_VALID_PRI))
    {
        pPFCPortEntry = (tPFCPortEntry *)
            RBTreeGetNext (gPFCGlobalInfo.pRbPFCPortTbl,
                           &PFCPortEntry, DcbxUtlPortTblCmpFn);

        if (pPFCPortEntry == NULL)
        {
            DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : RBtreeGetNExt() "
                           "Returns NULL. \r\n", __FUNCTION__);
            return SNMP_FAILURE;
        }
        *pi4NextLldpV2LocPortIfIndex = (INT4) pPFCPortEntry->u4IfIndex;
        *pu4NextLldpXdot1dcbxLocPFCEnablePriority = DCBX_ZERO;
    }
    else
    {
        *pi4NextLldpV2LocPortIfIndex = i4LldpV2LocPortIfIndex;
        *pu4NextLldpXdot1dcbxLocPFCEnablePriority =
            u4LldpXdot1dcbxLocPFCEnablePriority + DCBX_MIN_VAL;
    }

    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetLldpXdot1dcbxLocPFCEnableEnabled
 Input       :  The Indices
                LldpV2LocPortIfIndex
                LldpXdot1dcbxLocPFCEnablePriority

                The Object 
                retValLldpXdot1dcbxLocPFCEnableEnabled
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetLldpXdot1dcbxLocPFCEnableEnabled (INT4 i4LldpV2LocPortIfIndex,
                                        UINT4
                                        u4LldpXdot1dcbxLocPFCEnablePriority,
                                        INT4
                                        *pi4RetValLldpXdot1dcbxLocPFCEnableEnabled)
{
    tPFCPortEntry      *pPFCPortEntry = NULL;
    UINT1              *pu1PfcStatus = NULL;
    UINT1               u1PfcValue = DCBX_ZERO;

    pPFCPortEntry = PFCUtlGetPortEntry ((UINT4) i4LldpV2LocPortIfIndex);
    if (pPFCPortEntry == NULL)
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : PFCUtlGetPortEntry() "
                       "Returns NULL. \r\n", __FUNCTION__);
        return SNMP_FAILURE;
    }

    pu1PfcStatus = &((pPFCPortEntry->PFCLocPortInfo).u1PFCLocStatus);

    u1PfcValue = *pu1PfcStatus;
    u1PfcValue = (UINT1) (u1PfcValue >> u4LldpXdot1dcbxLocPFCEnablePriority);
    u1PfcValue = u1PfcValue & 0x01;

    if (u1PfcValue == DCBX_ZERO)
    {
        *pi4RetValLldpXdot1dcbxLocPFCEnableEnabled = PFC_DISABLED;
    }
    else
    {
        *pi4RetValLldpXdot1dcbxLocPFCEnableEnabled = PFC_ENABLED;
    }
    return SNMP_SUCCESS;

}

/* LOW LEVEL Routines for Table : LldpXdot1dcbxLocApplicationPriorityAppTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceLldpXdot1dcbxLocApplicationPriorityAppTable
 Input       :  The Indices
                LldpV2LocPortIfIndex
                LldpXdot1dcbxLocApplicationPriorityAESelector
                LldpXdot1dcbxLocApplicationPriorityAEProtocol
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceLldpXdot1dcbxLocApplicationPriorityAppTable (INT4
                                                                     i4LldpV2LocPortIfIndex,
                                                                     INT4
                                                                     i4LldpXdot1dcbxLocApplicationPriorityAESelector,
                                                                     UINT4
                                                                     u4LldpXdot1dcbxLocApplicationPriorityAEProtocol)
{

    tAppPriPortEntry   *pAppPortEntry = NULL;

    APP_PRI_SHUTDOWN_CHECK;

    pAppPortEntry = AppPriUtlGetPortEntry ((UINT4) i4LldpV2LocPortIfIndex);
    if (pAppPortEntry == NULL)
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : AppPriUtlGetPortEntry() "
                       "Returns NULL. \r\n", __FUNCTION__);
        return SNMP_FAILURE;
    }

    if (((INT4) u4LldpXdot1dcbxLocApplicationPriorityAEProtocol < DCBX_ZERO) ||
        (u4LldpXdot1dcbxLocApplicationPriorityAEProtocol >
         APP_PRI_MAX_PROTOCOL_ID))
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : "
                       "i4LldpXdot1dcbxLocApplicationPriorityAEProtocol "
                       "exceeded allowed range. \r\n", __FUNCTION__);
        return SNMP_FAILURE;
    }
    if ((i4LldpXdot1dcbxLocApplicationPriorityAESelector < DCBX_ONE) ||
        (i4LldpXdot1dcbxLocApplicationPriorityAESelector >
         APP_PRI_MAX_SELECTOR))
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : "
                       "i4LldpXdot1dcbxLocApplicationPriorityAESelector "
                       "exceeded allowed range. \r\n", __FUNCTION__);
        return SNMP_FAILURE;
    }

    if (APP_PRI_LOC_TBL (pAppPortEntry,
                         i4LldpXdot1dcbxLocApplicationPriorityAESelector - 1) ==
        NULL)
    {
        DCBX_TRC_ARG3 (DCBX_MGMT_TRC, "In %s : AppPriMappingTable "
                       "is NULL for port %d selector %d \r\n", __FUNCTION__,
                       i4LldpV2LocPortIfIndex,
                       i4LldpXdot1dcbxLocApplicationPriorityAESelector);
        return SNMP_FAILURE;
    }

    if (AppPriUtlGetAppPriMappingEntry ((UINT4) i4LldpV2LocPortIfIndex,
                                        i4LldpXdot1dcbxLocApplicationPriorityAESelector,
                                        (INT4)
                                        u4LldpXdot1dcbxLocApplicationPriorityAEProtocol,
                                        APP_PRI_LOCAL) == NULL)
    {
        DCBX_TRC_ARG4 (DCBX_MGMT_TRC, "In %s : AppPriMappingEntry "
                       "is NULL for port %d selector %d Application %d\r\n",
                       __FUNCTION__,
                       i4LldpV2LocPortIfIndex,
                       i4LldpXdot1dcbxLocApplicationPriorityAESelector,
                       u4LldpXdot1dcbxLocApplicationPriorityAEProtocol);
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexLldpXdot1dcbxLocApplicationPriorityAppTable
 Input       :  The Indices
                LldpV2LocPortIfIndex
                LldpXdot1dcbxLocApplicationPriorityAESelector
                LldpXdot1dcbxLocApplicationPriorityAEProtocol
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexLldpXdot1dcbxLocApplicationPriorityAppTable (INT4
                                                             *pi4LldpV2LocPortIfIndex,
                                                             INT4
                                                             *pi4LldpXdot1dcbxLocApplicationPriorityAESelector,
                                                             UINT4
                                                             *pu4LldpXdot1dcbxLocApplicationPriorityAEProtocol)
{
    return (nmhGetNextIndexLldpXdot1dcbxLocApplicationPriorityAppTable
            (DCBX_ZERO, pi4LldpV2LocPortIfIndex, DCBX_ONE,
             pi4LldpXdot1dcbxLocApplicationPriorityAESelector, DCBX_ZERO,
             pu4LldpXdot1dcbxLocApplicationPriorityAEProtocol));

}

/****************************************************************************
 Function    :  nmhGetNextIndexLldpXdot1dcbxLocApplicationPriorityAppTable
 Input       :  The Indices
                LldpV2LocPortIfIndex
                nextLldpV2LocPortIfIndex
                LldpXdot1dcbxLocApplicationPriorityAESelector
                nextLldpXdot1dcbxLocApplicationPriorityAESelector
                LldpXdot1dcbxLocApplicationPriorityAEProtocol
                nextLldpXdot1dcbxLocApplicationPriorityAEProtocol
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexLldpXdot1dcbxLocApplicationPriorityAppTable (INT4
                                                            i4LldpV2LocPortIfIndex,
                                                            INT4
                                                            *pi4NextLldpV2LocPortIfIndex,
                                                            INT4
                                                            i4LldpXdot1dcbxLocApplicationPriorityAESelector,
                                                            INT4
                                                            *pi4NextLldpXdot1dcbxLocApplicationPriorityAESelector,
                                                            UINT4
                                                            u4LldpXdot1dcbxLocApplicationPriorityAEProtocol,
                                                            UINT4
                                                            *pu4NextLldpXdot1dcbxLocApplicationPriorityAEProtocol)
{
    tAppPriPortEntry    AppPortEntry;
    tAppPriPortEntry   *pAppPortEntry = NULL;
    tAppPriMappingEntry *pAppPriMappingEntry = NULL;
    INT4                i4NextAPPortNumber = DCBX_ZERO;
    INT4                i4APPortNumber = DCBX_ZERO;
    INT4                i4Selector = DCBX_ZERO;
    INT4                i4Protocol = DCBX_ZERO;
    UINT1               u1OperVersion = DCBX_VER_UNKNOWN;

    APP_PRI_SHUTDOWN_CHECK;

    MEMSET (&AppPortEntry, DCBX_ZERO, sizeof (tAppPriPortEntry));

    i4NextAPPortNumber = i4LldpV2LocPortIfIndex;
    i4APPortNumber = i4NextAPPortNumber;

    pAppPortEntry = AppPriUtlGetPortEntry ((UINT4) i4APPortNumber);
    if (pAppPortEntry == NULL)
    {
        MEMSET (&AppPortEntry, DCBX_ZERO, sizeof (tAppPriPortEntry));
        AppPortEntry.u4IfIndex = (UINT4) i4APPortNumber;
        pAppPortEntry = NULL;
        pAppPortEntry = (tAppPriPortEntry *)
            RBTreeGetNext (gAppPriGlobalInfo.pRbAppPriPortTbl,
                           &AppPortEntry, AppPriUtlPortTblCmpFn);
    }

    while (pAppPortEntry != NULL)
    {
        i4APPortNumber = (INT4) pAppPortEntry->u4IfIndex;

        if (pAppPortEntry->u4IfIndex == (UINT4) i4LldpV2LocPortIfIndex)
        {
            i4Selector = i4LldpXdot1dcbxLocApplicationPriorityAESelector;
        }
        else
        {
            i4Selector = DCBX_ONE;
        }

        for (; i4Selector <= APP_PRI_MAX_SELECTOR; i4Selector++)
        {
            DcbxUtlGetOperVersion (pAppPortEntry->u4IfIndex, &u1OperVersion);
            if (u1OperVersion == DCBX_VER_CEE)
            {
                /* For CEE Selector 2 = TCP and 3 = UDP CEE is not valid */
                if (i4Selector == DCBX_TWO || i4Selector == DCBX_THREE)
                {
                    DCBX_TRC_ARG2 (DCBX_MGMT_TRC, "In %s : "
                                   "Skipping Selector %d for CEE.\r\n",
                                   __func__, i4Selector);
                    continue;
                }
            }

            if (TMO_SLL_Count
                (APP_PRI_LOC_TBL (pAppPortEntry, i4Selector - 1)) != DCBX_ZERO)
            {
                if ((pAppPortEntry->u4IfIndex == (UINT4) i4LldpV2LocPortIfIndex)
                    && (i4Selector ==
                        i4LldpXdot1dcbxLocApplicationPriorityAESelector))
                {
                    i4Protocol =
                        (INT4) (u4LldpXdot1dcbxLocApplicationPriorityAEProtocol
                                + DCBX_ONE);
                }
                else
                {
                    i4Protocol = DCBX_ZERO;
                }
                TMO_SLL_Scan (APP_PRI_LOC_TBL (pAppPortEntry, i4Selector - 1),
                              pAppPriMappingEntry, tAppPriMappingEntry *)
                {
                    if (pAppPriMappingEntry->i4AppPriProtocol >= i4Protocol)
                    {
                        *pi4NextLldpV2LocPortIfIndex = i4APPortNumber;
                        *pi4NextLldpXdot1dcbxLocApplicationPriorityAESelector =
                            i4Selector;
                        *pu4NextLldpXdot1dcbxLocApplicationPriorityAEProtocol =
                            (UINT4) pAppPriMappingEntry->i4AppPriProtocol;
                        return SNMP_SUCCESS;
                    }
                }
            }                    /*end of if TMO_SLL_Count */
        }                        /* end of for loop -selector */

        MEMSET (&AppPortEntry, DCBX_ZERO, sizeof (tAppPriPortEntry));
        AppPortEntry.u4IfIndex = pAppPortEntry->u4IfIndex;
        pAppPortEntry = NULL;
        pAppPortEntry = (tAppPriPortEntry *)
            RBTreeGetNext (gAppPriGlobalInfo.pRbAppPriPortTbl,
                           &AppPortEntry, AppPriUtlPortTblCmpFn);
    }
    return SNMP_FAILURE;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetLldpXdot1dcbxLocApplicationPriorityAEPriority
 Input       :  The Indices
                LldpV2LocPortIfIndex
                LldpXdot1dcbxLocApplicationPriorityAESelector
                LldpXdot1dcbxLocApplicationPriorityAEProtocol

                The Object 
                retValLldpXdot1dcbxLocApplicationPriorityAEPriority
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetLldpXdot1dcbxLocApplicationPriorityAEPriority (INT4
                                                     i4LldpV2LocPortIfIndex,
                                                     INT4
                                                     i4LldpXdot1dcbxLocApplicationPriorityAESelector,
                                                     UINT4
                                                     u4LldpXdot1dcbxLocApplicationPriorityAEProtocol,
                                                     UINT4
                                                     *pu4RetValLldpXdot1dcbxLocApplicationPriorityAEPriority)
{
    tAppPriMappingEntry *pAppPriMappingEntry = NULL;
    UINT1               u1OperVersion = DCBX_VER_UNKNOWN;

    APP_PRI_SHUTDOWN_CHECK;

    pAppPriMappingEntry = AppPriUtlGetAppPriMappingEntry
        ((UINT4) i4LldpV2LocPortIfIndex,
         i4LldpXdot1dcbxLocApplicationPriorityAESelector,
         (INT4) u4LldpXdot1dcbxLocApplicationPriorityAEProtocol, APP_PRI_LOCAL);
    if (pAppPriMappingEntry == NULL)
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC,
                       "In %s : nmhGetLldpXdot1dcbxLocApplicationPriorityAEPriority "
                       " AppPriUtlGetAppPriMappingEntry returned NULL. \r\n",
                       __FUNCTION__);
        return SNMP_FAILURE;
    }

    DcbxUtlGetOperVersion ((UINT4) i4LldpV2LocPortIfIndex, &u1OperVersion);
    if (u1OperVersion == DCBX_VER_CEE)
    {
        /* For CEE Selector 2 = TCP and 3 = UDP CEE is not valid */
        if (i4LldpXdot1dcbxLocApplicationPriorityAESelector == DCBX_TWO ||
            i4LldpXdot1dcbxLocApplicationPriorityAESelector == DCBX_THREE)
        {
            DCBX_TRC_ARG2 (DCBX_MGMT_TRC, "In %s : "
                           "Selector %d for CEE is not valid. Return!!!\r\n",
                           __func__,
                           i4LldpXdot1dcbxLocApplicationPriorityAESelector);
            return SNMP_FAILURE;
        }
    }

    *pu4RetValLldpXdot1dcbxLocApplicationPriorityAEPriority =
        pAppPriMappingEntry->u1AppPriPriority;
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : LldpXdot1dcbxRemETSBasicConfigurationTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceLldpXdot1dcbxRemETSBasicConfigurationTable
 Input       :  The Indices
                LldpV2RemTimeMark
                LldpV2RemLocalIfIndex
                u4LldpV2RemLocalDestMACAddress
                LldpV2RemIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceLldpXdot1dcbxRemETSBasicConfigurationTable (UINT4
                                                                    u4LldpV2RemTimeMark,
                                                                    INT4
                                                                    i4LldpV2RemLocalIfIndex,
                                                                    UINT4
                                                                    u4LldpV2RemLocalDestMACAddress,
                                                                    INT4
                                                                    i4LldpV2RemIndex)
{
    tETSPortEntry      *pETSPortEntry = NULL;
    tETSRemPortInfo    *pETSRemPortInfo = NULL;

    ETS_SHUTDOWN_CHECK;

    pETSPortEntry = ETSUtlGetPortEntry ((UINT4) i4LldpV2RemLocalIfIndex);
    if (pETSPortEntry == NULL)
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : ETSUtlGetPortEntry() "
                       "Returns NULL. \r\n", __FUNCTION__);
        return SNMP_FAILURE;
    }

    pETSRemPortInfo = &(pETSPortEntry->ETSRemPortInfo);
    if (pETSPortEntry->ETSRemPortInfo.i4ETSRemIndex == DCBX_ZERO)
    {
        DCBX_TRC (DCBX_MGMT_TRC, NIL_INFO);
        return SNMP_FAILURE;
    }

    if ((pETSPortEntry->ETSRemPortInfo.i4ETSRemIndex < DCBX_ZERO) ||
        (pETSPortEntry->ETSRemPortInfo.i4ETSRemIndex > DCBX_REM_MAX_INDEX))
    {
        DCBX_TRC (DCBX_MGMT_TRC, INDEX_NOT_IN_RANGE);
        return SNMP_FAILURE;
    }

    if (pETSPortEntry->ETSRemPortInfo.u4ETSDestRemMacIndex == DCBX_ZERO)
    {
        DCBX_TRC (DCBX_MGMT_TRC, WRONG_VAL);
        return SNMP_FAILURE;
    }

    if ((pETSRemPortInfo->u4ETSRemTimeMark != u4LldpV2RemTimeMark)
        || (pETSRemPortInfo->i4ETSRemIndex != i4LldpV2RemIndex)
        || (pETSRemPortInfo->u4ETSDestRemMacIndex !=
            u4LldpV2RemLocalDestMACAddress))
    {
        DCBX_TRC (DCBX_MGMT_TRC, MISMATCH);
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFirstIndexLldpXdot1dcbxRemETSBasicConfigurationTable
 Input       :  The Indices
                LldpV2RemTimeMark
                LldpV2RemLocalIfIndex
                u4LldpV2RemLocalDestMACAddress
                LldpV2RemIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexLldpXdot1dcbxRemETSBasicConfigurationTable (UINT4
                                                            *pu4LldpV2RemTimeMark,
                                                            INT4
                                                            *pi4LldpV2RemLocalIfIndex,
                                                            UINT4
                                                            *pu4LldpV2RemLocalDestMACAddress,
                                                            INT4
                                                            *pi4LldpV2RemIndex)
{
    ETS_SHUTDOWN_CHECK;

    return (nmhGetNextIndexLldpXdot1dcbxRemETSBasicConfigurationTable
            (DCBX_ZERO, pu4LldpV2RemTimeMark,
             DCBX_ZERO, pi4LldpV2RemLocalIfIndex,
             DCBX_ZERO, pu4LldpV2RemLocalDestMACAddress,
             DCBX_ZERO, pi4LldpV2RemIndex));

}

/****************************************************************************
 Function    :  nmhGetNextIndexLldpXdot1dcbxRemETSBasicConfigurationTable
 Input       :  The Indices
                LldpV2RemTimeMark
                nextLldpV2RemTimeMark
                LldpV2RemLocalIfIndex
                nextLldpV2RemLocalIfIndex
                u4LldpV2RemLocalDestMACAddress
                nextLldpV2RemLocalDestMACAddress
                LldpV2RemIndex
                nextLldpV2RemIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexLldpXdot1dcbxRemETSBasicConfigurationTable (UINT4
                                                           u4LldpV2RemTimeMark,
                                                           UINT4
                                                           *pu4NextLldpV2RemTimeMark,
                                                           INT4
                                                           i4LldpV2RemLocalIfIndex,
                                                           INT4
                                                           *pi4NextLldpV2RemLocalIfIndex,
                                                           UINT4
                                                           u4LldpV2RemLocalDestMACAddress,
                                                           UINT4
                                                           *pu4NextLldpV2RemLocalDestMACAddress,
                                                           INT4
                                                           i4LldpV2RemIndex,
                                                           INT4
                                                           *pi4NextLldpV2RemIndex)
{
    tETSPortEntry      *pETSPortEntry = NULL;
    tETSPortEntry       ETSPortEntry;
    INT4                i4IfIndex = i4LldpV2RemLocalIfIndex;
    INT4                i4RemIndex = i4LldpV2RemIndex;

    UNUSED_PARAM (u4LldpV2RemTimeMark);
    UNUSED_PARAM (u4LldpV2RemLocalDestMACAddress);

    ETS_SHUTDOWN_CHECK;

    pETSPortEntry = ETSUtlGetPortEntry ((UINT4) i4IfIndex);
    if ((pETSPortEntry == NULL) ||
        (i4RemIndex >= pETSPortEntry->ETSRemPortInfo.i4ETSRemIndex) ||
        (!(pETSPortEntry->u1RemTlvUpdStatus & ETS_CONF_TLV_REM_UPD)))

    {
        MEMSET (&ETSPortEntry, DCBX_ZERO, sizeof (tETSPortEntry));
        ETSPortEntry.u4IfIndex = (UINT4) i4IfIndex;
        do
        {
            pETSPortEntry = (tETSPortEntry *)
                RBTreeGetNext (gETSGlobalInfo.pRbETSPortTbl,
                               &ETSPortEntry, DcbxUtlPortTblCmpFn);
            if ((pETSPortEntry != NULL) &&
                (pETSPortEntry->ETSRemPortInfo.i4ETSRemIndex != DCBX_ZERO) &&
                (pETSPortEntry->u1RemTlvUpdStatus & ETS_CONF_TLV_REM_UPD))
            {
                *pi4NextLldpV2RemLocalIfIndex = (INT4) pETSPortEntry->u4IfIndex;
                *pu4NextLldpV2RemTimeMark =
                    pETSPortEntry->ETSRemPortInfo.u4ETSRemTimeMark;
                *pu4NextLldpV2RemLocalDestMACAddress =
                    pETSPortEntry->ETSRemPortInfo.u4ETSDestRemMacIndex;
                *pi4NextLldpV2RemIndex =
                    pETSPortEntry->ETSRemPortInfo.i4ETSRemIndex;
                return SNMP_SUCCESS;
            }
            if (pETSPortEntry != NULL)
            {
                MEMSET (&ETSPortEntry, DCBX_ZERO, sizeof (tETSPortEntry));
                ETSPortEntry.u4IfIndex = pETSPortEntry->u4IfIndex;
            }
        }
        while (pETSPortEntry != NULL);
    }
    else
    {
        *pi4NextLldpV2RemLocalIfIndex = (INT4) pETSPortEntry->u4IfIndex;
        *pu4NextLldpV2RemTimeMark =
            pETSPortEntry->ETSRemPortInfo.u4ETSRemTimeMark;
        *pu4NextLldpV2RemLocalDestMACAddress =
            pETSPortEntry->ETSRemPortInfo.u4ETSDestRemMacIndex;
        *pi4NextLldpV2RemIndex = pETSPortEntry->ETSRemPortInfo.i4ETSRemIndex;
        return SNMP_SUCCESS;
    }
    DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : Rbtree GetNext Node () "
                   "Returns NULL. \r\n", __FUNCTION__);
    return SNMP_FAILURE;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetLldpXdot1dcbxRemETSConCreditBasedShaperSupport
 Input       :  The Indices
                LldpV2RemTimeMark
                LldpV2RemLocalIfIndex
                u4LldpV2RemLocalDestMACAddress
                LldpV2RemIndex

                The Object 
                retValLldpXdot1dcbxRemETSConCreditBasedShaperSupport
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetLldpXdot1dcbxRemETSConCreditBasedShaperSupport (UINT4 u4LldpV2RemTimeMark,
                                                      INT4
                                                      i4LldpV2RemLocalIfIndex,
                                                      UINT4
                                                      u4LldpV2RemLocalDestMACAddress,
                                                      INT4 i4LldpV2RemIndex,
                                                      INT4
                                                      *pi4RetValLldpXdot1dcbxRemETSConCreditBasedShaperSupport)
{
    tETSPortEntry      *pETSPortEntry = NULL;

    UNUSED_PARAM (u4LldpV2RemTimeMark);
    UNUSED_PARAM (i4LldpV2RemIndex);
    UNUSED_PARAM (u4LldpV2RemLocalDestMACAddress);

    pETSPortEntry = ETSUtlGetPortEntry ((UINT4) i4LldpV2RemLocalIfIndex);
    if (pETSPortEntry == NULL)
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : ETSUtlGetPortEntry () "
                       " Returns FAILURE. \r\n", __FUNCTION__);
        return SNMP_FAILURE;
    }
    if (pETSPortEntry->ETSRemPortInfo.i4ETSRemIndex == DCBX_ZERO)
    {
        DCBX_TRC (DCBX_MGMT_TRC, NIL_INFO);
        return SNMP_FAILURE;
    }

    *pi4RetValLldpXdot1dcbxRemETSConCreditBasedShaperSupport =
        pETSPortEntry->ETSRemPortInfo.u1ETSRemCBS;
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetLldpXdot1dcbxRemETSConTrafficClassesSupported
 Input       :  The Indices
                LldpV2RemTimeMark
                LldpV2RemLocalIfIndex
                u4LldpV2RemLocalDestMACAddress
                LldpV2RemIndex

                The Object 
                retValLldpXdot1dcbxRemETSConTrafficClassesSupported
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetLldpXdot1dcbxRemETSConTrafficClassesSupported (UINT4 u4LldpV2RemTimeMark,
                                                     INT4
                                                     i4LldpV2RemLocalIfIndex,
                                                     UINT4
                                                     u4LldpV2RemLocalDestMACAddress,
                                                     INT4 i4LldpV2RemIndex,
                                                     UINT4
                                                     *pu4RetValLldpXdot1dcbxRemETSConTrafficClassesSupported)
{
    /*  nmhGetLldpXdot1dcbxRemTCSupported  */
    tETSPortEntry      *pETSPortEntry = NULL;

    UNUSED_PARAM (u4LldpV2RemTimeMark);
    UNUSED_PARAM (i4LldpV2RemIndex);
    UNUSED_PARAM (u4LldpV2RemLocalDestMACAddress);

    pETSPortEntry = ETSUtlGetPortEntry ((UINT4) i4LldpV2RemLocalIfIndex);
    if (pETSPortEntry == NULL)
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : ETSUtlGetPortEntry() "
                       "Returns NULL. \r\n", __FUNCTION__);
        return SNMP_FAILURE;
    }

    if (pETSPortEntry->ETSRemPortInfo.i4ETSRemIndex == DCBX_ZERO)
    {
        DCBX_TRC (DCBX_MGMT_TRC, NIL_INFO);
        return SNMP_FAILURE;
    }

    *pu4RetValLldpXdot1dcbxRemETSConTrafficClassesSupported =
        pETSPortEntry->ETSRemPortInfo.u1ETSRemNumTcSup;
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetLldpXdot1dcbxRemETSConWilling
 Input       :  The Indices
                LldpV2RemTimeMark
                LldpV2RemLocalIfIndex
                u4LldpV2RemLocalDestMACAddress
                LldpV2RemIndex

                The Object 
                retValLldpXdot1dcbxRemETSConWilling
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetLldpXdot1dcbxRemETSConWilling (UINT4 u4LldpV2RemTimeMark,
                                     INT4 i4LldpV2RemLocalIfIndex,
                                     UINT4 u4LldpV2RemLocalDestMACAddress,
                                     INT4 i4LldpV2RemIndex,
                                     INT4
                                     *pi4RetValLldpXdot1dcbxRemETSConWilling)
{
    tETSPortEntry      *pETSPortEntry = NULL;

    UNUSED_PARAM (u4LldpV2RemTimeMark);
    UNUSED_PARAM (i4LldpV2RemIndex);
    UNUSED_PARAM (u4LldpV2RemLocalDestMACAddress);

    pETSPortEntry = ETSUtlGetPortEntry ((UINT4) i4LldpV2RemLocalIfIndex);
    if (pETSPortEntry == NULL)
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : ETSUtlGetPortEntry() "
                       "Returns NULL. \r\n", __FUNCTION__);
        return SNMP_FAILURE;
    }

    if (pETSPortEntry->ETSRemPortInfo.i4ETSRemIndex == DCBX_ZERO)
    {
        DCBX_TRC (DCBX_MGMT_TRC, NIL_INFO);
        return SNMP_FAILURE;
    }

    *pi4RetValLldpXdot1dcbxRemETSConWilling =
        pETSPortEntry->ETSRemPortInfo.u1ETSRemWilling;

    return SNMP_SUCCESS;

}

/* LOW LEVEL Routines for Table : LldpXdot1dcbxRemETSConPriorityAssignmentTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceLldpXdot1dcbxRemETSConPriorityAssignmentTable
 Input       :  The Indices
                LldpV2RemTimeMark
                LldpV2RemLocalIfIndex
                u4LldpV2RemLocalDestMACAddress
                LldpV2RemIndex
                LldpXdot1dcbxRemETSConPriority
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceLldpXdot1dcbxRemETSConPriorityAssignmentTable (UINT4
                                                                       u4LldpV2RemTimeMark,
                                                                       INT4
                                                                       i4LldpV2RemLocalIfIndex,
                                                                       UINT4
                                                                       u4LldpV2RemLocalDestMACAddress,
                                                                       INT4
                                                                       i4LldpV2RemIndex,
                                                                       UINT4
                                                                       u4LldpXdot1dcbxRemETSConPriority)
{
    tETSPortEntry      *pETSPortEntry = NULL;
    tETSRemPortInfo    *pETSRemPortInfo = NULL;

    ETS_SHUTDOWN_CHECK;

    pETSPortEntry = ETSUtlGetPortEntry ((UINT4) i4LldpV2RemLocalIfIndex);
    if (pETSPortEntry == NULL)
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : ETSUtlGetPortEntry() "
                       "Returns NULL. \r\n", __FUNCTION__);
        return SNMP_FAILURE;
    }

    pETSRemPortInfo = &(pETSPortEntry->ETSRemPortInfo);
    if (pETSPortEntry->ETSRemPortInfo.i4ETSRemIndex == DCBX_ZERO)
    {
        DCBX_TRC (DCBX_MGMT_TRC, NIL_INFO);
        return SNMP_FAILURE;
    }

    if ((pETSPortEntry->ETSRemPortInfo.i4ETSRemIndex < DCBX_ZERO)
        || (pETSPortEntry->ETSRemPortInfo.i4ETSRemIndex > DCBX_REM_MAX_INDEX))
    {
        DCBX_TRC (DCBX_MGMT_TRC, INDEX_NOT_IN_RANGE);
        return SNMP_FAILURE;
    }
    if (pETSPortEntry->ETSRemPortInfo.u4ETSDestRemMacIndex == DCBX_ZERO)
    {
        DCBX_TRC (DCBX_MGMT_TRC, WRONG_VAL);
        return SNMP_FAILURE;
    }

    if ((pETSRemPortInfo->u4ETSRemTimeMark != u4LldpV2RemTimeMark)
        || (pETSRemPortInfo->i4ETSRemIndex != i4LldpV2RemIndex)
        || (pETSRemPortInfo->u4ETSDestRemMacIndex !=
            u4LldpV2RemLocalDestMACAddress))
    {
        DCBX_TRC (DCBX_MGMT_TRC, MISMATCH);
    }

    if (((INT4) u4LldpXdot1dcbxRemETSConPriority < DCBX_ZERO) ||
        (u4LldpXdot1dcbxRemETSConPriority >= DCBX_MAX_PRIORITIES))
    {
        DCBX_TRC (DCBX_MGMT_TRC, WRONG_VAL);
        return SNMP_FAILURE;

    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFirstIndexLldpXdot1dcbxRemETSConPriorityAssignmentTable
 Input       :  The Indices
                LldpV2RemTimeMark
                LldpV2RemLocalIfIndex
                u4LldpV2RemLocalDestMACAddress
                LldpV2RemIndex
                LldpXdot1dcbxRemETSConPriority
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexLldpXdot1dcbxRemETSConPriorityAssignmentTable (UINT4
                                                               *pu4LldpV2RemTimeMark,
                                                               INT4
                                                               *pi4LldpV2RemLocalIfIndex,
                                                               UINT4
                                                               *pu4LldpV2RemLocalDestMACAddress,
                                                               INT4
                                                               *pi4LldpV2RemIndex,
                                                               UINT4
                                                               *pu4LldpXdot1dcbxRemETSConPriority)
{

    ETS_SHUTDOWN_CHECK;
    return (nmhGetNextIndexLldpXdot1dcbxRemETSConPriorityAssignmentTable
            (DCBX_ZERO, pu4LldpV2RemTimeMark,
             DCBX_ZERO, pi4LldpV2RemLocalIfIndex,
             DCBX_ZERO, pu4LldpV2RemLocalDestMACAddress,
             DCBX_ZERO, pi4LldpV2RemIndex,
             DCBX_ZERO, pu4LldpXdot1dcbxRemETSConPriority));

}

/****************************************************************************
 Function    :  nmhGetNextIndexLldpXdot1dcbxRemETSConPriorityAssignmentTable
 Input       :  The Indices
                LldpV2RemTimeMark
                nextLldpV2RemTimeMark
                LldpV2RemLocalIfIndex
                nextLldpV2RemLocalIfIndex
                u4LldpV2RemLocalDestMACAddress
                nextLldpV2RemLocalDestMACAddress
                LldpV2RemIndex
                nextLldpV2RemIndex
                LldpXdot1dcbxRemETSConPriority
                nextLldpXdot1dcbxRemETSConPriority
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexLldpXdot1dcbxRemETSConPriorityAssignmentTable (UINT4
                                                              u4LldpV2RemTimeMark,
                                                              UINT4
                                                              *pu4NextLldpV2RemTimeMark,
                                                              INT4
                                                              i4LldpV2RemLocalIfIndex,
                                                              INT4
                                                              *pi4NextLldpV2RemLocalIfIndex,
                                                              UINT4
                                                              u4LldpV2RemLocalDestMACAddress,
                                                              UINT4
                                                              *pu4NextLldpV2RemLocalDestMACAddress,
                                                              INT4
                                                              i4LldpV2RemIndex,
                                                              INT4
                                                              *pi4NextLldpV2RemIndex,
                                                              UINT4
                                                              u4LldpXdot1dcbxRemETSConPriority,
                                                              UINT4
                                                              *pu4NextLldpXdot1dcbxRemETSConPriority)
{
    tETSPortEntry      *pETSPortEntry = NULL;
    tETSPortEntry       ETSPortEntry;
    UINT4               u4TempLldpV2RemTimeMark = 0xFFFFFFFF;
    UINT4               u4TempLldpV2RemLocalDestMACAddress = 0xFFFFFFFF;
    INT4                i4TempLldpV2RemLocalIfIndex = 0x7FFFFFFF;
    INT4                i4TempLldpV2RemIndex = 0x7FFFFFFF;
    INT1                i1EntryFoundFlag = OSIX_FALSE;

    ETS_SHUTDOWN_CHECK;

    MEMSET (&ETSPortEntry, DCBX_ZERO, sizeof (tETSPortEntry));

    pETSPortEntry = ETSUtlGetPortEntry ((UINT4) i4LldpV2RemLocalIfIndex);

    if ((pETSPortEntry != NULL) &&
        (pETSPortEntry->ETSRemPortInfo.i4ETSRemIndex != DCBX_ZERO) &&
        (pETSPortEntry->u1RemTlvUpdStatus & ETS_CONF_TLV_REM_UPD) &&
        (u4LldpXdot1dcbxRemETSConPriority < DCBX_SEVEN))
    {
        *pu4NextLldpV2RemTimeMark = u4LldpV2RemTimeMark;
        *pi4NextLldpV2RemLocalIfIndex = i4LldpV2RemLocalIfIndex;
        *pu4NextLldpV2RemLocalDestMACAddress = u4LldpV2RemLocalDestMACAddress;
        *pi4NextLldpV2RemIndex = i4LldpV2RemIndex;
        *pu4NextLldpXdot1dcbxRemETSConPriority =
            u4LldpXdot1dcbxRemETSConPriority + DCBX_ONE;
        return SNMP_SUCCESS;
    }

    MEMSET (&ETSPortEntry, DCBX_ZERO, sizeof (tETSPortEntry));
    ETSPortEntry.u4IfIndex = 0;

    do
    {
        pETSPortEntry = (tETSPortEntry *)
            RBTreeGetNext (gETSGlobalInfo.pRbETSPortTbl,
                           &ETSPortEntry, DcbxUtlPortTblCmpFn);

        if (pETSPortEntry != NULL)
        {
            /* If remote index is zero, means no information is available 
             * hence this entry shall be skipped */
            if ((pETSPortEntry->ETSRemPortInfo.i4ETSRemIndex == DCBX_ZERO) ||
                (!(pETSPortEntry->u1RemTlvUpdStatus & ETS_CONF_TLV_REM_UPD)))
            {
                MEMSET (&ETSPortEntry, DCBX_ZERO, sizeof (tETSPortEntry));
                ETSPortEntry.u4IfIndex = pETSPortEntry->u4IfIndex;
                continue;
            }

            /* if (scankey < key) */
            if (DcbxRemETSCompareFunction
                (pETSPortEntry->ETSRemPortInfo.u4ETSRemTimeMark,
                 (INT4) pETSPortEntry->u4IfIndex,
                 pETSPortEntry->ETSRemPortInfo.u4ETSDestRemMacIndex,
                 pETSPortEntry->ETSRemPortInfo.i4ETSRemIndex,
                 u4LldpV2RemTimeMark, i4LldpV2RemLocalIfIndex,
                 u4LldpV2RemLocalDestMACAddress,
                 i4LldpV2RemIndex) == DCBX_LESSER)
            {
                MEMSET (&ETSPortEntry, DCBX_ZERO, sizeof (tETSPortEntry));
                ETSPortEntry.u4IfIndex = pETSPortEntry->u4IfIndex;
                continue;
            }
            /* if (scankey > key) */
            else if (DcbxRemETSCompareFunction
                     (pETSPortEntry->ETSRemPortInfo.u4ETSRemTimeMark,
                      (INT4) pETSPortEntry->u4IfIndex,
                      pETSPortEntry->ETSRemPortInfo.u4ETSDestRemMacIndex,
                      pETSPortEntry->ETSRemPortInfo.i4ETSRemIndex,
                      u4LldpV2RemTimeMark, i4LldpV2RemLocalIfIndex,
                      u4LldpV2RemLocalDestMACAddress,
                      i4LldpV2RemIndex) == DCBX_GREATER)
            {
                /* if (scankey <=tempkey) */
                if (DcbxRemETSCompareFunction
                    (pETSPortEntry->ETSRemPortInfo.u4ETSRemTimeMark,
                     (INT4) pETSPortEntry->u4IfIndex,
                     pETSPortEntry->ETSRemPortInfo.u4ETSDestRemMacIndex,
                     pETSPortEntry->ETSRemPortInfo.i4ETSRemIndex,
                     u4TempLldpV2RemTimeMark, i4TempLldpV2RemLocalIfIndex,
                     u4TempLldpV2RemLocalDestMACAddress,
                     i4TempLldpV2RemIndex) != DCBX_GREATER)
                {
                    /* TempKey = ScanKey; */
                    u4TempLldpV2RemTimeMark =
                        pETSPortEntry->ETSRemPortInfo.u4ETSRemTimeMark;
                    i4TempLldpV2RemLocalIfIndex =
                        (INT4) pETSPortEntry->u4IfIndex;
                    u4TempLldpV2RemLocalDestMACAddress =
                        pETSPortEntry->ETSRemPortInfo.u4ETSDestRemMacIndex,
                        i4TempLldpV2RemIndex =
                        pETSPortEntry->ETSRemPortInfo.i4ETSRemIndex;
                    MEMSET (&ETSPortEntry, DCBX_ZERO, sizeof (tETSPortEntry));
                    ETSPortEntry.u4IfIndex = pETSPortEntry->u4IfIndex;
                    i1EntryFoundFlag = OSIX_TRUE;
                    continue;
                }
                MEMSET (&ETSPortEntry, DCBX_ZERO, sizeof (tETSPortEntry));
                ETSPortEntry.u4IfIndex = pETSPortEntry->u4IfIndex;
            }
            else
            {
                MEMSET (&ETSPortEntry, DCBX_ZERO, sizeof (tETSPortEntry));
                ETSPortEntry.u4IfIndex = pETSPortEntry->u4IfIndex;
                continue;
            }
        }
    }
    while (pETSPortEntry != NULL);

    if (i1EntryFoundFlag == OSIX_TRUE)
    {
        *pu4NextLldpV2RemTimeMark = u4TempLldpV2RemTimeMark;
        *pi4NextLldpV2RemLocalIfIndex = i4TempLldpV2RemLocalIfIndex;
        *pu4NextLldpV2RemLocalDestMACAddress =
            u4TempLldpV2RemLocalDestMACAddress;
        *pi4NextLldpV2RemIndex = i4TempLldpV2RemIndex;
        *pu4NextLldpXdot1dcbxRemETSConPriority = DCBX_ZERO;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetLldpXdot1dcbxRemETSConPriTrafficClass
 Input       :  The Indices
                LldpV2RemTimeMark
                LldpV2RemLocalIfIndex
                u4LldpV2RemLocalDestMACAddress
                LldpV2RemIndex
                LldpXdot1dcbxRemETSConPriority

                The Object 
                retValLldpXdot1dcbxRemETSConPriTrafficClass
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetLldpXdot1dcbxRemETSConPriTrafficClass (UINT4 u4LldpV2RemTimeMark,
                                             INT4 i4LldpV2RemLocalIfIndex,
                                             UINT4
                                             u4LldpV2RemLocalDestMACAddress,
                                             INT4 i4LldpV2RemIndex,
                                             UINT4
                                             u4LldpXdot1dcbxRemETSConPriority,
                                             UINT4
                                             *pu4RetValLldpXdot1dcbxRemETSConPriTrafficClass)
{
    /*nmhGetLldpXdot1dcbxRemETSConTrafficClassGroup */
    tETSPortEntry      *pETSPortEntry = NULL;

    UNUSED_PARAM (u4LldpV2RemTimeMark);
    UNUSED_PARAM (i4LldpV2RemIndex);
    UNUSED_PARAM (u4LldpV2RemLocalDestMACAddress);

    pETSPortEntry = ETSUtlGetPortEntry ((UINT4) i4LldpV2RemLocalIfIndex);
    if (pETSPortEntry == NULL)
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : ETSUtlGetPortEntry() "
                       "Returns NULL. \r\n", __FUNCTION__);
        return SNMP_FAILURE;
    }

    if (pETSPortEntry->ETSRemPortInfo.i4ETSRemIndex == DCBX_ZERO)
    {
        DCBX_TRC (DCBX_MGMT_TRC, NIL_INFO);
        return SNMP_FAILURE;
    }

    *pu4RetValLldpXdot1dcbxRemETSConPriTrafficClass =
        (UINT4) ((pETSPortEntry->ETSRemPortInfo).
                 au1ETSRemTCGID[u4LldpXdot1dcbxRemETSConPriority]);

    return SNMP_SUCCESS;

}

/* LOW LEVEL Routines for Table : LldpXdot1dcbxRemETSConTrafficClassBandwidthTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceLldpXdot1dcbxRemETSConTrafficClassBandwidthTable
 Input       :  The Indices
                LldpV2RemTimeMark
                LldpV2RemLocalIfIndex
                u4LldpV2RemLocalDestMACAddress
                LldpV2RemIndex
                LldpXdot1dcbxRemETSConTrafficClass
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceLldpXdot1dcbxRemETSConTrafficClassBandwidthTable (UINT4
                                                                          u4LldpV2RemTimeMark,
                                                                          INT4
                                                                          i4LldpV2RemLocalIfIndex,
                                                                          UINT4
                                                                          u4LldpV2RemLocalDestMACAddress,
                                                                          INT4
                                                                          i4LldpV2RemIndex,
                                                                          UINT4
                                                                          u4LldpXdot1dcbxRemETSConTrafficClass)
{
    tETSPortEntry      *pETSPortEntry = NULL;
    tETSRemPortInfo    *pETSRemPortInfo = NULL;
    INT4                i4RetValue = DCBX_ZERO;

    ETS_SHUTDOWN_CHECK;

    pETSPortEntry = ETSUtlGetPortEntry ((UINT4) i4LldpV2RemLocalIfIndex);
    if (pETSPortEntry == NULL)
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : ETSUtlGetPortEntry() "
                       "Returns NULL. \r\n", __FUNCTION__);
        return SNMP_FAILURE;
    }
    pETSRemPortInfo = &(pETSPortEntry->ETSRemPortInfo);
    if (pETSPortEntry->ETSRemPortInfo.i4ETSRemIndex == DCBX_ZERO)
    {
        DCBX_TRC (DCBX_MGMT_TRC, NIL_INFO);
        return SNMP_FAILURE;
    }

    if ((pETSPortEntry->ETSRemPortInfo.i4ETSRemIndex < DCBX_ZERO)
        || (pETSPortEntry->ETSRemPortInfo.i4ETSRemIndex > DCBX_REM_MAX_INDEX))
    {
        DCBX_TRC (DCBX_MGMT_TRC, INDEX_NOT_IN_RANGE);
        return SNMP_FAILURE;
    }

    if (pETSPortEntry->ETSRemPortInfo.u4ETSDestRemMacIndex == DCBX_ZERO)
        if (i4RetValue == DCBX_ZERO)
        {
            DCBX_TRC (DCBX_MGMT_TRC, WRONG_VAL);
            return SNMP_FAILURE;
        }

    if ((pETSRemPortInfo->u4ETSRemTimeMark != u4LldpV2RemTimeMark)
        || (pETSRemPortInfo->i4ETSRemIndex != i4LldpV2RemIndex)
        || (pETSRemPortInfo->u4ETSDestRemMacIndex !=
            u4LldpV2RemLocalDestMACAddress))
    {
        DCBX_TRC (DCBX_MGMT_TRC, MISMATCH);
    }
    if (((INT4) u4LldpXdot1dcbxRemETSConTrafficClass <
         DCBX_ZERO)
        ||
        ((u4LldpXdot1dcbxRemETSConTrafficClass >=
          ETS_MAX_TCGID_CONF)
         && ((u4LldpXdot1dcbxRemETSConTrafficClass) != ETS_DEF_TCGID)))
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC,
                       "In %s : Traffic class is not in range(0-7)" " \r\n",
                       __FUNCTION__);
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFirstIndexLldpXdot1dcbxRemETSConTrafficClassBandwidthTable
 Input       :  The Indices
                LldpV2RemTimeMark
                LldpV2RemLocalIfIndex
                u4LldpV2RemLocalDestMACAddress
                LldpV2RemIndex
                LldpXdot1dcbxRemETSConTrafficClass
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexLldpXdot1dcbxRemETSConTrafficClassBandwidthTable (UINT4
                                                                  *pu4LldpV2RemTimeMark,
                                                                  INT4
                                                                  *pi4LldpV2RemLocalIfIndex,
                                                                  UINT4
                                                                  *pu4LldpV2RemLocalDestMACAddress,
                                                                  INT4
                                                                  *pi4LldpV2RemIndex,
                                                                  UINT4
                                                                  *pu4LldpXdot1dcbxRemETSConTrafficClass)
{

    ETS_SHUTDOWN_CHECK;
    return (nmhGetNextIndexLldpXdot1dcbxRemETSConTrafficClassBandwidthTable
            (DCBX_ZERO, pu4LldpV2RemTimeMark,
             DCBX_ZERO, pi4LldpV2RemLocalIfIndex,
             DCBX_ZERO, pu4LldpV2RemLocalDestMACAddress,
             DCBX_ZERO, pi4LldpV2RemIndex,
             DCBX_ZERO, pu4LldpXdot1dcbxRemETSConTrafficClass));
}

/****************************************************************************
 Function    :  nmhGetNextIndexLldpXdot1dcbxRemETSConTrafficClassBandwidthTable
 Input       :  The Indices
                LldpV2RemTimeMark
                nextLldpV2RemTimeMark
                LldpV2RemLocalIfIndex
                nextLldpV2RemLocalIfIndex
                u4LldpV2RemLocalDestMACAddress
                nextLldpV2RemLocalDestMACAddress
                LldpV2RemIndex
                nextLldpV2RemIndex
                LldpXdot1dcbxRemETSConTrafficClass
                nextLldpXdot1dcbxRemETSConTrafficClass
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexLldpXdot1dcbxRemETSConTrafficClassBandwidthTable (UINT4
                                                                 u4LldpV2RemTimeMark,
                                                                 UINT4
                                                                 *pu4NextLldpV2RemTimeMark,
                                                                 INT4
                                                                 i4LldpV2RemLocalIfIndex,
                                                                 INT4
                                                                 *pi4NextLldpV2RemLocalIfIndex,
                                                                 UINT4
                                                                 u4LldpV2RemLocalDestMACAddress,
                                                                 UINT4
                                                                 *pu4NextLldpV2RemLocalDestMACAddress,
                                                                 INT4
                                                                 i4LldpV2RemIndex,
                                                                 INT4
                                                                 *pi4NextLldpV2RemIndex,
                                                                 UINT4
                                                                 u4LldpXdot1dcbxRemETSConTrafficClass,
                                                                 UINT4
                                                                 *pu4NextLldpXdot1dcbxRemETSConTrafficClass)
{
    tETSPortEntry      *pETSPortEntry = NULL;
    tETSPortEntry       ETSPortEntry;
    UINT4               u4TempLldpV2RemTimeMark = 0xFFFFFFFF;
    INT4                i4TempLldpV2RemLocalIfIndex = 0x7FFFFFFF;
    INT4                i4TempLldpV2RemIndex = 0x7FFFFFFF;
    UINT4               u4TempLldpV2RemLocalDestMACAddress = 0xFFFFFFFF;
    INT1                i1EntryFoundFlag = OSIX_FALSE;

    ETS_SHUTDOWN_CHECK;

    MEMSET (&ETSPortEntry, DCBX_ZERO, sizeof (tETSPortEntry));

    pETSPortEntry = ETSUtlGetPortEntry ((UINT4) i4LldpV2RemLocalIfIndex);

    if ((pETSPortEntry != NULL) &&
        (pETSPortEntry->ETSRemPortInfo.i4ETSRemIndex != DCBX_ZERO) &&
        (pETSPortEntry->u1RemTlvUpdStatus & ETS_CONF_TLV_REM_UPD) &&
        (u4LldpXdot1dcbxRemETSConTrafficClass < DCBX_SEVEN))
    {
        *pu4NextLldpV2RemTimeMark = u4LldpV2RemTimeMark;
        *pi4NextLldpV2RemLocalIfIndex = i4LldpV2RemLocalIfIndex;
        *pu4NextLldpV2RemLocalDestMACAddress = u4LldpV2RemLocalDestMACAddress;
        *pi4NextLldpV2RemIndex = i4LldpV2RemIndex;
        *pu4NextLldpXdot1dcbxRemETSConTrafficClass =
            u4LldpXdot1dcbxRemETSConTrafficClass + DCBX_ONE;
        return SNMP_SUCCESS;
    }

    MEMSET (&ETSPortEntry, DCBX_ZERO, sizeof (tETSPortEntry));
    ETSPortEntry.u4IfIndex = 0;

    do
    {
        pETSPortEntry = (tETSPortEntry *)
            RBTreeGetNext (gETSGlobalInfo.pRbETSPortTbl,
                           &ETSPortEntry, DcbxUtlPortTblCmpFn);

        if (pETSPortEntry != NULL)
        {
            /* If remote index is zero, means no information is available 
             * hence this entry shall be skipped */
            if ((pETSPortEntry->ETSRemPortInfo.i4ETSRemIndex == DCBX_ZERO) ||
                (!(pETSPortEntry->u1RemTlvUpdStatus & ETS_CONF_TLV_REM_UPD)))
            {
                MEMSET (&ETSPortEntry, DCBX_ZERO, sizeof (tETSPortEntry));
                ETSPortEntry.u4IfIndex = pETSPortEntry->u4IfIndex;
                continue;
            }

            /* if (scankey < key) */
            if (DcbxRemETSCompareFunction
                (pETSPortEntry->ETSRemPortInfo.u4ETSRemTimeMark,
                 (INT4) pETSPortEntry->u4IfIndex,
                 (pETSPortEntry->ETSRemPortInfo.
                  u4ETSDestRemMacIndex),
                 pETSPortEntry->ETSRemPortInfo.i4ETSRemIndex,
                 u4LldpV2RemTimeMark, i4LldpV2RemLocalIfIndex,
                 u4LldpV2RemLocalDestMACAddress,
                 i4LldpV2RemIndex) == DCBX_LESSER)
            {
                MEMSET (&ETSPortEntry, DCBX_ZERO, sizeof (tETSPortEntry));
                ETSPortEntry.u4IfIndex = pETSPortEntry->u4IfIndex;
                continue;
            }
            /* if (scankey > key) */
            else if (DcbxRemETSCompareFunction
                     (pETSPortEntry->ETSRemPortInfo.u4ETSRemTimeMark,
                      (INT4) pETSPortEntry->u4IfIndex,
                      (pETSPortEntry->ETSRemPortInfo.
                       u4ETSDestRemMacIndex),
                      pETSPortEntry->ETSRemPortInfo.i4ETSRemIndex,
                      u4LldpV2RemTimeMark, i4LldpV2RemLocalIfIndex,
                      u4LldpV2RemLocalDestMACAddress,
                      i4LldpV2RemIndex) == DCBX_GREATER)
            {
                /* if (scankey <=tempkey) */
                if (DcbxRemETSCompareFunction
                    (pETSPortEntry->ETSRemPortInfo.u4ETSRemTimeMark,
                     (INT4) pETSPortEntry->u4IfIndex,
                     pETSPortEntry->ETSRemPortInfo.
                     u4ETSDestRemMacIndex,
                     pETSPortEntry->ETSRemPortInfo.i4ETSRemIndex,
                     u4TempLldpV2RemTimeMark, i4TempLldpV2RemLocalIfIndex,
                     u4TempLldpV2RemLocalDestMACAddress,
                     i4TempLldpV2RemIndex) != DCBX_GREATER)
                {
                    /* TempKey = ScanKey; */
                    u4TempLldpV2RemTimeMark =
                        pETSPortEntry->ETSRemPortInfo.u4ETSRemTimeMark;
                    i4TempLldpV2RemLocalIfIndex =
                        (INT4) pETSPortEntry->u4IfIndex;
                    u4TempLldpV2RemLocalDestMACAddress =
                        pETSPortEntry->ETSRemPortInfo.u4ETSDestRemMacIndex;
                    i4TempLldpV2RemIndex =
                        pETSPortEntry->ETSRemPortInfo.i4ETSRemIndex;
                    MEMSET (&ETSPortEntry, DCBX_ZERO, sizeof (tETSPortEntry));
                    ETSPortEntry.u4IfIndex = pETSPortEntry->u4IfIndex;
                    i1EntryFoundFlag = OSIX_TRUE;
                    continue;
                }
                MEMSET (&ETSPortEntry, DCBX_ZERO, sizeof (tETSPortEntry));
                ETSPortEntry.u4IfIndex = pETSPortEntry->u4IfIndex;
            }
            else
            {
                MEMSET (&ETSPortEntry, DCBX_ZERO, sizeof (tETSPortEntry));
                ETSPortEntry.u4IfIndex = pETSPortEntry->u4IfIndex;
                continue;
            }
        }
    }
    while (pETSPortEntry != NULL);

    if (i1EntryFoundFlag == OSIX_TRUE)
    {
        *pu4NextLldpV2RemTimeMark = u4TempLldpV2RemTimeMark;
        *pi4NextLldpV2RemLocalIfIndex = i4TempLldpV2RemLocalIfIndex;
        *pu4NextLldpV2RemLocalDestMACAddress =
            u4TempLldpV2RemLocalDestMACAddress;
        *pi4NextLldpV2RemIndex = i4TempLldpV2RemIndex;
        *pu4NextLldpXdot1dcbxRemETSConTrafficClass = DCBX_ZERO;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetLldpXdot1dcbxRemETSConTrafficClassBandwidth
 Input       :  The Indices
                LldpV2RemTimeMark
                LldpV2RemLocalIfIndex
                u4LldpV2RemLocalDestMACAddress
                LldpV2RemIndex
                LldpXdot1dcbxRemETSConTrafficClass

                The Object 
                retValLldpXdot1dcbxRemETSConTrafficClassBandwidth
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetLldpXdot1dcbxRemETSConTrafficClassBandwidth (UINT4 u4LldpV2RemTimeMark,
                                                   INT4 i4LldpV2RemLocalIfIndex,
                                                   UINT4
                                                   u4LldpV2RemLocalDestMACAddress,
                                                   INT4 i4LldpV2RemIndex,
                                                   UINT4
                                                   u4LldpXdot1dcbxRemETSConTrafficClass,
                                                   UINT4
                                                   *pu4RetValLldpXdot1dcbxRemETSConTrafficClassBandwidth)
{
    tETSPortEntry      *pETSPortEntry = NULL;

    UNUSED_PARAM (u4LldpV2RemTimeMark);
    UNUSED_PARAM (i4LldpV2RemIndex);
    UNUSED_PARAM (u4LldpV2RemLocalDestMACAddress);

    pETSPortEntry = ETSUtlGetPortEntry ((UINT4) i4LldpV2RemLocalIfIndex);
    if (pETSPortEntry == NULL)
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : ETSUtlGetPortEntry() "
                       "Returns NULL. \r\n", __FUNCTION__);
        return SNMP_FAILURE;
    }

    if (pETSPortEntry->ETSRemPortInfo.i4ETSRemIndex == DCBX_ZERO)
    {
        DCBX_TRC (DCBX_MGMT_TRC, NIL_INFO);
        return SNMP_FAILURE;
    }
    *pu4RetValLldpXdot1dcbxRemETSConTrafficClassBandwidth =
        (INT4) ((pETSPortEntry->ETSRemPortInfo).
                au1ETSRemBW[u4LldpXdot1dcbxRemETSConTrafficClass]);

    return SNMP_SUCCESS;

}

/* LOW LEVEL Routines for Table : LldpXdot1dcbxRemETSConTrafficSelectionAlgorithmTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceLldpXdot1dcbxRemETSConTrafficSelectionAlgorithmTable
 Input       :  The Indices
                LldpV2RemTimeMark
                LldpV2RemLocalIfIndex
                u4LldpV2RemLocalDestMACAddress
                LldpV2RemIndex
                LldpXdot1dcbxRemETSConTSATrafficClass
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1 
     
     
     
     
     
     
     
    nmhValidateIndexInstanceLldpXdot1dcbxRemETSConTrafficSelectionAlgorithmTable
    (UINT4 u4LldpV2RemTimeMark, INT4 i4LldpV2RemLocalIfIndex,
     UINT4 u4LldpV2RemLocalDestMACAddress, INT4 i4LldpV2RemIndex,
     UINT4 u4LldpXdot1dcbxRemETSConTSATrafficClass)
{
    tETSPortEntry      *pETSPortEntry = NULL;
    tETSRemPortInfo    *pETSRemPortInfo = NULL;

    ETS_SHUTDOWN_CHECK;

    pETSPortEntry = ETSUtlGetPortEntry ((UINT4) i4LldpV2RemLocalIfIndex);
    if (pETSPortEntry == NULL)
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : ETSUtlGetPortEntry() "
                       "Returns NULL. \r\n", __FUNCTION__);
        return SNMP_FAILURE;
    }

    pETSRemPortInfo = &(pETSPortEntry->ETSRemPortInfo);
    if (pETSPortEntry->ETSRemPortInfo.i4ETSRemIndex == DCBX_ZERO)
    {
        DCBX_TRC (DCBX_MGMT_TRC, NIL_INFO);
        return SNMP_FAILURE;
    }

    if ((pETSPortEntry->ETSRemPortInfo.i4ETSRemIndex < DCBX_ZERO)
        || (pETSPortEntry->ETSRemPortInfo.i4ETSRemIndex > DCBX_REM_MAX_INDEX))
    {
        DCBX_TRC (DCBX_MGMT_TRC, INDEX_NOT_IN_RANGE);
        return SNMP_FAILURE;
    }

    if (pETSPortEntry->ETSRemPortInfo.u4ETSDestRemMacIndex == DCBX_ZERO)
    {
        DCBX_TRC (DCBX_MGMT_TRC, WRONG_VAL);
        return SNMP_FAILURE;
    }

    if ((pETSRemPortInfo->u4ETSRemTimeMark != u4LldpV2RemTimeMark)
        || (pETSRemPortInfo->i4ETSRemIndex != i4LldpV2RemIndex)
        || (pETSRemPortInfo->u4ETSDestRemMacIndex !=
            u4LldpV2RemLocalDestMACAddress))
    {
        DCBX_TRC (DCBX_MGMT_TRC, MISMATCH);
    }

    if (((INT4) u4LldpXdot1dcbxRemETSConTSATrafficClass <
         DCBX_ZERO)
        ||
        ((u4LldpXdot1dcbxRemETSConTSATrafficClass >=
          ETS_MAX_TCGID_CONF)
         && ((u4LldpXdot1dcbxRemETSConTSATrafficClass) != ETS_DEF_TCGID)))
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : TC ID is not in range "
                       "(0-7) \r\n", __FUNCTION__);
        CLI_SET_ERR (CLI_ERR_ETS_INVALID_TCGID);
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexLldpXdot1dcbxRemETSConTrafficSelectionAlgorithmTable
 Input       :  The Indices
                LldpV2RemTimeMark
                LldpV2RemLocalIfIndex
                u4LldpV2RemLocalDestMACAddress
                LldpV2RemIndex
                LldpXdot1dcbxRemETSConTSATrafficClass
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexLldpXdot1dcbxRemETSConTrafficSelectionAlgorithmTable (UINT4
                                                                      *pu4LldpV2RemTimeMark,
                                                                      INT4
                                                                      *pi4LldpV2RemLocalIfIndex,
                                                                      UINT4
                                                                      *pu4LldpV2RemLocalDestMACAddress,
                                                                      INT4
                                                                      *pi4LldpV2RemIndex,
                                                                      UINT4
                                                                      *pu4LldpXdot1dcbxRemETSConTSATrafficClass)
{
    ETS_SHUTDOWN_CHECK;
    return (nmhGetNextIndexLldpXdot1dcbxRemETSConTrafficSelectionAlgorithmTable
            (DCBX_ZERO, pu4LldpV2RemTimeMark,
             DCBX_ZERO, pi4LldpV2RemLocalIfIndex,
             DCBX_ZERO, pu4LldpV2RemLocalDestMACAddress,
             DCBX_ZERO, pi4LldpV2RemIndex,
             DCBX_ZERO, pu4LldpXdot1dcbxRemETSConTSATrafficClass));

}

/****************************************************************************
 Function    :  nmhGetNextIndexLldpXdot1dcbxRemETSConTrafficSelectionAlgorithmTable
 Input       :  The Indices
                LldpV2RemTimeMark
                nextLldpV2RemTimeMark
                LldpV2RemLocalIfIndex
                nextLldpV2RemLocalIfIndex
                u4LldpV2RemLocalDestMACAddress
                nextLldpV2RemLocalDestMACAddress
                LldpV2RemIndex
                nextLldpV2RemIndex
                LldpXdot1dcbxRemETSConTSATrafficClass
                nextLldpXdot1dcbxRemETSConTSATrafficClass
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexLldpXdot1dcbxRemETSConTrafficSelectionAlgorithmTable (UINT4
                                                                     u4LldpV2RemTimeMark,
                                                                     UINT4
                                                                     *pu4NextLldpV2RemTimeMark,
                                                                     INT4
                                                                     i4LldpV2RemLocalIfIndex,
                                                                     INT4
                                                                     *pi4NextLldpV2RemLocalIfIndex,
                                                                     UINT4
                                                                     u4LldpV2RemLocalDestMACAddress,
                                                                     UINT4
                                                                     *pu4NextLldpV2RemLocalDestMACAddress,
                                                                     INT4
                                                                     i4LldpV2RemIndex,
                                                                     INT4
                                                                     *pi4NextLldpV2RemIndex,
                                                                     UINT4
                                                                     u4LldpXdot1dcbxRemETSConTSATrafficClass,
                                                                     UINT4
                                                                     *pu4NextLldpXdot1dcbxRemETSConTSATrafficClass)
{
    tETSPortEntry      *pETSPortEntry = NULL;
    tETSPortEntry       ETSPortEntry;
    INT4                i4IfIndex = i4LldpV2RemLocalIfIndex;
    INT4                i4RemIndex = i4LldpV2RemIndex;

    UNUSED_PARAM (u4LldpV2RemTimeMark);
    UNUSED_PARAM (u4LldpV2RemLocalDestMACAddress);
    ETS_SHUTDOWN_CHECK;

    pETSPortEntry = ETSUtlGetPortEntry ((UINT4) i4IfIndex);

    if ((pETSPortEntry == NULL) ||
        (pETSPortEntry->ETSRemPortInfo.i4ETSRemIndex == DCBX_ZERO) ||
        (i4RemIndex > pETSPortEntry->ETSRemPortInfo.i4ETSRemIndex) ||
        (!(pETSPortEntry->u1RemTlvUpdStatus & ETS_CONF_TLV_REM_UPD)) ||
        ((i4RemIndex == pETSPortEntry->ETSRemPortInfo.i4ETSRemIndex)
         && (u4LldpXdot1dcbxRemETSConTSATrafficClass >= ETS_TCGID7)))

    {
        MEMSET (&ETSPortEntry, DCBX_ZERO, sizeof (tETSPortEntry));
        ETSPortEntry.u4IfIndex = (UINT4) i4IfIndex;
        do
        {
            pETSPortEntry = (tETSPortEntry *)
                RBTreeGetNext (gETSGlobalInfo.pRbETSPortTbl,
                               &ETSPortEntry, DcbxUtlPortTblCmpFn);
            if ((pETSPortEntry != NULL) &&
                (pETSPortEntry->ETSRemPortInfo.i4ETSRemIndex != DCBX_ZERO) &&
                (pETSPortEntry->u1RemTlvUpdStatus & ETS_CONF_TLV_REM_UPD))
            {
                *pi4NextLldpV2RemLocalIfIndex = (INT4) pETSPortEntry->u4IfIndex;
                *pu4NextLldpV2RemTimeMark =
                    pETSPortEntry->ETSRemPortInfo.u4ETSRemTimeMark;
                *pu4NextLldpV2RemLocalDestMACAddress =
                    pETSPortEntry->ETSRemPortInfo.u4ETSDestRemMacIndex;
                *pi4NextLldpV2RemIndex =
                    pETSPortEntry->ETSRemPortInfo.i4ETSRemIndex;
                *pu4NextLldpXdot1dcbxRemETSConTSATrafficClass = DCBX_ZERO;
                return SNMP_SUCCESS;
            }
            if (pETSPortEntry != NULL)
            {
                MEMSET (&ETSPortEntry, DCBX_ZERO, sizeof (tETSPortEntry));
                ETSPortEntry.u4IfIndex = pETSPortEntry->u4IfIndex;
            }
        }
        while (pETSPortEntry != NULL);
    }
    else
    {
        *pi4NextLldpV2RemLocalIfIndex = (INT4) pETSPortEntry->u4IfIndex;
        *pu4NextLldpV2RemTimeMark =
            pETSPortEntry->ETSRemPortInfo.u4ETSRemTimeMark;
        *pu4NextLldpV2RemLocalDestMACAddress =
            pETSPortEntry->ETSRemPortInfo.u4ETSDestRemMacIndex;
        *pi4NextLldpV2RemIndex = pETSPortEntry->ETSRemPortInfo.i4ETSRemIndex;
        if (i4RemIndex < pETSPortEntry->ETSRemPortInfo.i4ETSRemIndex)
        {
            *pu4NextLldpXdot1dcbxRemETSConTSATrafficClass = DCBX_ZERO;
        }
        else
        {
            *pu4NextLldpXdot1dcbxRemETSConTSATrafficClass =
                u4LldpXdot1dcbxRemETSConTSATrafficClass + DCBX_MIN_VAL;
        }
        return SNMP_SUCCESS;
    }
    DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : Rbtree GetNext Node () "
                   "Returns NULL. \r\n", __FUNCTION__);
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetLldpXdot1dcbxRemETSConTrafficSelectionAlgorithm
 Input       :  The Indices
                LldpV2RemTimeMark
                LldpV2RemLocalIfIndex
                u4LldpV2RemLocalDestMACAddress
                LldpV2RemIndex
                LldpXdot1dcbxRemETSConTSATrafficClass

                The Object 
                retValLldpXdot1dcbxRemETSConTrafficSelectionAlgorithm
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetLldpXdot1dcbxRemETSConTrafficSelectionAlgorithm (UINT4
                                                       u4LldpV2RemTimeMark,
                                                       INT4
                                                       i4LldpV2RemLocalIfIndex,
                                                       UINT4
                                                       u4LldpV2RemLocalDestMACAddress,
                                                       INT4 i4LldpV2RemIndex,
                                                       UINT4
                                                       u4LldpXdot1dcbxRemETSConTSATrafficClass,
                                                       INT4
                                                       *pi4RetValLldpXdot1dcbxRemETSConTrafficSelectionAlgorithm)
{
    tETSPortEntry      *pETSPortEntry = NULL;

    UNUSED_PARAM (u4LldpV2RemTimeMark);
    UNUSED_PARAM (i4LldpV2RemIndex);
    UNUSED_PARAM (u4LldpV2RemLocalDestMACAddress);

    pETSPortEntry = ETSUtlGetPortEntry ((UINT4) i4LldpV2RemLocalIfIndex);
    if (pETSPortEntry == NULL)
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : ETSUtlGetPortEntry() "
                       "Returns NULL. \r\n", __FUNCTION__);
        return SNMP_FAILURE;
    }

    if (pETSPortEntry->ETSRemPortInfo.i4ETSRemIndex == DCBX_ZERO)
    {
        DCBX_TRC (DCBX_MGMT_TRC, NIL_INFO);
        return SNMP_FAILURE;
    }

    *pi4RetValLldpXdot1dcbxRemETSConTrafficSelectionAlgorithm =
        (INT4) ((pETSPortEntry->ETSRemPortInfo).
                au1ETSRemTsaTable[u4LldpXdot1dcbxRemETSConTSATrafficClass]);

    return SNMP_SUCCESS;

}

/* LOW LEVEL Routines for Table : LldpXdot1dcbxRemETSRecoTrafficClassBandwidthTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceLldpXdot1dcbxRemETSRecoTrafficClassBandwidthTable
 Input       :  The Indices
                LldpV2RemTimeMark
                LldpV2RemLocalIfIndex
                u4LldpV2RemLocalDestMACAddress
                LldpV2RemIndex
                LldpXdot1dcbxRemETSRecoTrafficClass
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceLldpXdot1dcbxRemETSRecoTrafficClassBandwidthTable (UINT4
                                                                           u4LldpV2RemTimeMark,
                                                                           INT4
                                                                           i4LldpV2RemLocalIfIndex,
                                                                           UINT4
                                                                           u4LldpV2RemLocalDestMACAddress,
                                                                           INT4
                                                                           i4LldpV2RemIndex,
                                                                           UINT4
                                                                           u4LldpXdot1dcbxRemETSRecoTrafficClass)
{
    tETSPortEntry      *pETSPortEntry = NULL;
    tETSRemPortInfo    *pETSRemPortInfo = NULL;

    ETS_SHUTDOWN_CHECK;

    pETSPortEntry = ETSUtlGetPortEntry ((UINT4) i4LldpV2RemLocalIfIndex);
    if (pETSPortEntry == NULL)
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : ETSUtlGetPortEntry() "
                       "Returns NULL. \r\n", __FUNCTION__);
        return SNMP_FAILURE;
    }

    pETSRemPortInfo = &(pETSPortEntry->ETSRemPortInfo);
    if (pETSPortEntry->ETSRemPortInfo.i4ETSRemIndex == DCBX_ZERO)
    {
        DCBX_TRC (DCBX_MGMT_TRC, NIL_INFO);
        return SNMP_FAILURE;
    }

    if ((pETSPortEntry->ETSRemPortInfo.i4ETSRemIndex < DCBX_ZERO)
        || (pETSPortEntry->ETSRemPortInfo.i4ETSRemIndex > DCBX_REM_MAX_INDEX))
    {
        DCBX_TRC (DCBX_MGMT_TRC, INDEX_NOT_IN_RANGE);
        return SNMP_FAILURE;
    }

    if (pETSPortEntry->ETSRemPortInfo.u4ETSDestRemMacIndex == DCBX_ZERO)
    {
        DCBX_TRC (DCBX_MGMT_TRC, WRONG_VAL);
        return SNMP_FAILURE;
    }

    if ((pETSRemPortInfo->u4ETSRemTimeMark != u4LldpV2RemTimeMark)
        || (pETSRemPortInfo->i4ETSRemIndex != i4LldpV2RemIndex)
        || (pETSRemPortInfo->u4ETSDestRemMacIndex !=
            u4LldpV2RemLocalDestMACAddress))
    {
        DCBX_TRC (DCBX_MGMT_TRC, MISMATCH);
    }
    if (((INT4) u4LldpXdot1dcbxRemETSRecoTrafficClass <
         DCBX_ZERO)
        ||
        ((u4LldpXdot1dcbxRemETSRecoTrafficClass >=
          ETS_MAX_TCGID_CONF)
         && ((u4LldpXdot1dcbxRemETSRecoTrafficClass) != ETS_DEF_TCGID)))
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC,
                       "In %s : Traffic class is not in range(0-7)" " \r\n",
                       __FUNCTION__);
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexLldpXdot1dcbxRemETSRecoTrafficClassBandwidthTable
 Input       :  The Indices
                LldpV2RemTimeMark
                LldpV2RemLocalIfIndex
                u4LldpV2RemLocalDestMACAddress
                LldpV2RemIndex
                LldpXdot1dcbxRemETSRecoTrafficClass
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexLldpXdot1dcbxRemETSRecoTrafficClassBandwidthTable (UINT4
                                                                   *pu4LldpV2RemTimeMark,
                                                                   INT4
                                                                   *pi4LldpV2RemLocalIfIndex,
                                                                   UINT4
                                                                   *pu4LldpV2RemLocalDestMACAddress,
                                                                   INT4
                                                                   *pi4LldpV2RemIndex,
                                                                   UINT4
                                                                   *pu4LldpXdot1dcbxRemETSRecoTrafficClass)
{
    return (nmhGetNextIndexLldpXdot1dcbxRemETSRecoTrafficClassBandwidthTable
            (DCBX_ZERO, pu4LldpV2RemTimeMark,
             DCBX_ZERO, pi4LldpV2RemLocalIfIndex,
             DCBX_ZERO, pu4LldpV2RemLocalDestMACAddress,
             DCBX_ZERO, pi4LldpV2RemIndex,
             DCBX_ZERO, pu4LldpXdot1dcbxRemETSRecoTrafficClass));

}

/****************************************************************************
 Function    :  nmhGetNextIndexLldpXdot1dcbxRemETSRecoTrafficClassBandwidthTable
 Input       :  The Indices
                LldpV2RemTimeMark
                nextLldpV2RemTimeMark
                LldpV2RemLocalIfIndex
                nextLldpV2RemLocalIfIndex
                u4LldpV2RemLocalDestMACAddress
                nextLldpV2RemLocalDestMACAddress
                LldpV2RemIndex
                nextLldpV2RemIndex
                LldpXdot1dcbxRemETSRecoTrafficClass
                nextLldpXdot1dcbxRemETSRecoTrafficClass
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexLldpXdot1dcbxRemETSRecoTrafficClassBandwidthTable (UINT4
                                                                  u4LldpV2RemTimeMark,
                                                                  UINT4
                                                                  *pu4NextLldpV2RemTimeMark,
                                                                  INT4
                                                                  i4LldpV2RemLocalIfIndex,
                                                                  INT4
                                                                  *pi4NextLldpV2RemLocalIfIndex,
                                                                  UINT4
                                                                  u4LldpV2RemLocalDestMACAddress,
                                                                  UINT4
                                                                  *pu4NextLldpV2RemLocalDestMACAddress,
                                                                  INT4
                                                                  i4LldpV2RemIndex,
                                                                  INT4
                                                                  *pi4NextLldpV2RemIndex,
                                                                  UINT4
                                                                  u4LldpXdot1dcbxRemETSRecoTrafficClass,
                                                                  UINT4
                                                                  *pu4NextLldpXdot1dcbxRemETSRecoTrafficClass)
{
    tETSPortEntry      *pETSPortEntry = NULL;
    tETSPortEntry       ETSPortEntry;
    UINT4               u4TempLldpV2RemTimeMark = 0xFFFFFFFF;
    INT4                i4TempLldpV2RemLocalIfIndex = 0x7FFFFFFF;
    INT4                i4TempLldpV2RemIndex = 0x7FFFFFFF;
    UINT4               u4TempLldpV2RemLocalDestMACAddress = 0xFFFFFFFF;
    INT1                i1EntryFoundFlag = OSIX_FALSE;

    ETS_SHUTDOWN_CHECK;

    MEMSET (&ETSPortEntry, DCBX_ZERO, sizeof (tETSPortEntry));

    pETSPortEntry = ETSUtlGetPortEntry ((UINT4) i4LldpV2RemLocalIfIndex);

    if ((pETSPortEntry != NULL) &&
        (pETSPortEntry->ETSRemPortInfo.i4ETSRemIndex != DCBX_ZERO) &&
        (pETSPortEntry->u1RemTlvUpdStatus & ETS_RECO_TLV_REM_UPD) &&
        (u4LldpXdot1dcbxRemETSRecoTrafficClass < DCBX_SEVEN))
    {
        *pu4NextLldpV2RemTimeMark = u4LldpV2RemTimeMark;
        *pi4NextLldpV2RemLocalIfIndex = i4LldpV2RemLocalIfIndex;
        *pu4NextLldpV2RemLocalDestMACAddress = u4LldpV2RemLocalDestMACAddress;
        *pi4NextLldpV2RemIndex = pETSPortEntry->ETSRemPortInfo.i4ETSRemIndex;
        *pu4NextLldpXdot1dcbxRemETSRecoTrafficClass =
            u4LldpXdot1dcbxRemETSRecoTrafficClass + DCBX_ONE;
        return SNMP_SUCCESS;
    }

    MEMSET (&ETSPortEntry, DCBX_ZERO, sizeof (tETSPortEntry));
    ETSPortEntry.u4IfIndex = 0;

    do
    {
        pETSPortEntry = (tETSPortEntry *)
            RBTreeGetNext (gETSGlobalInfo.pRbETSPortTbl,
                           &ETSPortEntry, DcbxUtlPortTblCmpFn);

        if (pETSPortEntry != NULL)
        {
            /* If remote index is zero, means no information is available 
             * hence this entry shall be skipped */
            if ((pETSPortEntry->ETSRemPortInfo.i4ETSRemIndex == DCBX_ZERO)
                || (!(pETSPortEntry->u1RemTlvUpdStatus & ETS_RECO_TLV_REM_UPD)))
            {
                MEMSET (&ETSPortEntry, DCBX_ZERO, sizeof (tETSPortEntry));
                ETSPortEntry.u4IfIndex = pETSPortEntry->u4IfIndex;
                continue;
            }

            /* if (scankey < key) */
            if (DcbxRemETSCompareFunction
                (pETSPortEntry->ETSRemPortInfo.u4ETSRemTimeMark,
                 (INT4) pETSPortEntry->u4IfIndex,
                 (pETSPortEntry->ETSRemPortInfo.
                  u4ETSDestRemMacIndex),
                 pETSPortEntry->ETSRemPortInfo.i4ETSRemIndex,
                 u4LldpV2RemTimeMark, i4LldpV2RemLocalIfIndex,
                 u4LldpV2RemLocalDestMACAddress,
                 i4LldpV2RemIndex) == DCBX_LESSER)
            {
                MEMSET (&ETSPortEntry, DCBX_ZERO, sizeof (tETSPortEntry));
                ETSPortEntry.u4IfIndex = pETSPortEntry->u4IfIndex;
                continue;
            }
            /* if (scankey > key) */
            else if (DcbxRemETSCompareFunction
                     (pETSPortEntry->ETSRemPortInfo.u4ETSRemTimeMark,
                      (INT4) pETSPortEntry->u4IfIndex,
                      (pETSPortEntry->ETSRemPortInfo.
                       u4ETSDestRemMacIndex),
                      pETSPortEntry->ETSRemPortInfo.i4ETSRemIndex,
                      u4LldpV2RemTimeMark, i4LldpV2RemLocalIfIndex,
                      u4LldpV2RemLocalDestMACAddress,
                      i4LldpV2RemIndex) == DCBX_GREATER)
            {
                /* if (scankey <=tempkey) */
                if (DcbxRemETSCompareFunction
                    (pETSPortEntry->ETSRemPortInfo.u4ETSRemTimeMark,
                     (INT4) pETSPortEntry->u4IfIndex,
                     (pETSPortEntry->ETSRemPortInfo.
                      u4ETSDestRemMacIndex),
                     pETSPortEntry->ETSRemPortInfo.i4ETSRemIndex,
                     u4TempLldpV2RemTimeMark, i4TempLldpV2RemLocalIfIndex,
                     u4TempLldpV2RemLocalDestMACAddress,
                     i4TempLldpV2RemIndex) != DCBX_GREATER)
                {
                    /* TempKey = ScanKey; */
                    u4TempLldpV2RemTimeMark =
                        pETSPortEntry->ETSRemPortInfo.u4ETSRemTimeMark;
                    i4TempLldpV2RemLocalIfIndex =
                        (INT4) pETSPortEntry->u4IfIndex;
                    u4TempLldpV2RemLocalDestMACAddress =
                        pETSPortEntry->ETSRemPortInfo.u4ETSDestRemMacIndex;
                    i4TempLldpV2RemIndex =
                        pETSPortEntry->ETSRemPortInfo.i4ETSRemIndex;
                    MEMSET (&ETSPortEntry, DCBX_ZERO, sizeof (tETSPortEntry));
                    ETSPortEntry.u4IfIndex = pETSPortEntry->u4IfIndex;
                    i1EntryFoundFlag = OSIX_TRUE;
                    continue;
                }
                MEMSET (&ETSPortEntry, DCBX_ZERO, sizeof (tETSPortEntry));
                ETSPortEntry.u4IfIndex = pETSPortEntry->u4IfIndex;
            }
            else
            {
                MEMSET (&ETSPortEntry, DCBX_ZERO, sizeof (tETSPortEntry));
                ETSPortEntry.u4IfIndex = pETSPortEntry->u4IfIndex;
                continue;
            }
        }
    }
    while (pETSPortEntry != NULL);

    if (i1EntryFoundFlag == OSIX_TRUE)
    {
        *pu4NextLldpV2RemTimeMark = u4TempLldpV2RemTimeMark;
        *pi4NextLldpV2RemLocalIfIndex = i4TempLldpV2RemLocalIfIndex;
        *pu4NextLldpV2RemLocalDestMACAddress =
            u4TempLldpV2RemLocalDestMACAddress;
        *pi4NextLldpV2RemIndex = i4TempLldpV2RemIndex;
        *pu4NextLldpXdot1dcbxRemETSRecoTrafficClass = DCBX_ZERO;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetLldpXdot1dcbxRemETSRecoTrafficClassBandwidth
 Input       :  The Indices
                LldpV2RemTimeMark
                LldpV2RemLocalIfIndex
                u4LldpV2RemLocalDestMACAddress
                LldpV2RemIndex
                LldpXdot1dcbxRemETSRecoTrafficClass

                The Object 
                retValLldpXdot1dcbxRemETSRecoTrafficClassBandwidth
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetLldpXdot1dcbxRemETSRecoTrafficClassBandwidth (UINT4 u4LldpV2RemTimeMark,
                                                    INT4
                                                    i4LldpV2RemLocalIfIndex,
                                                    UINT4
                                                    u4LldpV2RemLocalDestMACAddress,
                                                    INT4 i4LldpV2RemIndex,
                                                    UINT4
                                                    u4LldpXdot1dcbxRemETSRecoTrafficClass,
                                                    UINT4
                                                    *pu4RetValLldpXdot1dcbxRemETSRecoTrafficClassBandwidth)
{
    tETSPortEntry      *pETSPortEntry = NULL;

    UNUSED_PARAM (u4LldpV2RemTimeMark);
    UNUSED_PARAM (i4LldpV2RemIndex);
    UNUSED_PARAM (u4LldpV2RemLocalDestMACAddress);

    pETSPortEntry = ETSUtlGetPortEntry ((UINT4) i4LldpV2RemLocalIfIndex);
    if (pETSPortEntry == NULL)
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : ETSUtlGetPortEntry() "
                       "Returns NULL. \r\n", __FUNCTION__);
        return SNMP_FAILURE;
    }

    if (pETSPortEntry->ETSRemPortInfo.i4ETSRemIndex == DCBX_ZERO)
    {
        DCBX_TRC (DCBX_MGMT_TRC, NIL_INFO);
        return SNMP_FAILURE;
    }
    *pu4RetValLldpXdot1dcbxRemETSRecoTrafficClassBandwidth =
        (INT4) pETSPortEntry->ETSRemPortInfo.
        au1ETSRemRecoBW[u4LldpXdot1dcbxRemETSRecoTrafficClass];
    return SNMP_SUCCESS;

}

/* LOW LEVEL Routines for Table : LldpXdot1dcbxRemETSRecoTrafficSelectionAlgorithmTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceLldpXdot1dcbxRemETSRecoTrafficSelectionAlgorithmTable
 Input       :  The Indices
                LldpV2RemTimeMark
                LldpV2RemLocalIfIndex
                u4LldpV2RemLocalDestMACAddress
                LldpV2RemIndex
                LldpXdot1dcbxRemETSRecoTSATrafficClass
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1 
     
     
     
     
     
     
     
    nmhValidateIndexInstanceLldpXdot1dcbxRemETSRecoTrafficSelectionAlgorithmTable
    (UINT4 u4LldpV2RemTimeMark, INT4 i4LldpV2RemLocalIfIndex,
     UINT4 u4LldpV2RemLocalDestMACAddress, INT4 i4LldpV2RemIndex,
     UINT4 u4LldpXdot1dcbxRemETSRecoTSATrafficClass)
{
    tETSPortEntry      *pETSPortEntry = NULL;
    tETSRemPortInfo    *pETSRemPortInfo = NULL;
    ETS_SHUTDOWN_CHECK;

    pETSPortEntry = ETSUtlGetPortEntry ((UINT4) i4LldpV2RemLocalIfIndex);
    if (pETSPortEntry == NULL)
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : ETSUtlGetPortEntry() "
                       "Returns NULL. \r\n", __FUNCTION__);
        return SNMP_FAILURE;
    }

    pETSRemPortInfo = &(pETSPortEntry->ETSRemPortInfo);
    if (pETSPortEntry->ETSRemPortInfo.i4ETSRemIndex == DCBX_ZERO)
    {
        DCBX_TRC (DCBX_MGMT_TRC, NIL_INFO);
        return SNMP_FAILURE;
    }

    if ((pETSPortEntry->ETSRemPortInfo.i4ETSRemIndex < DCBX_ZERO)
        || (pETSPortEntry->ETSRemPortInfo.i4ETSRemIndex > DCBX_REM_MAX_INDEX))
    {
        DCBX_TRC (DCBX_MGMT_TRC, INDEX_NOT_IN_RANGE);
        return SNMP_FAILURE;
    }

    if (pETSPortEntry->ETSRemPortInfo.u4ETSDestRemMacIndex == DCBX_ZERO)
    {
        DCBX_TRC (DCBX_MGMT_TRC, WRONG_VAL);
        return SNMP_FAILURE;
    }

    if ((pETSRemPortInfo->u4ETSRemTimeMark != u4LldpV2RemTimeMark)
        || (pETSRemPortInfo->i4ETSRemIndex != i4LldpV2RemIndex)
        || (pETSRemPortInfo->u4ETSDestRemMacIndex !=
            u4LldpV2RemLocalDestMACAddress))
    {
        DCBX_TRC (DCBX_MGMT_TRC, MISMATCH);
        return SNMP_FAILURE;
    }
    if (((INT4) u4LldpXdot1dcbxRemETSRecoTSATrafficClass <
         DCBX_ZERO)
        ||
        ((u4LldpXdot1dcbxRemETSRecoTSATrafficClass >=
          ETS_MAX_TCGID_CONF)
         && ((u4LldpXdot1dcbxRemETSRecoTSATrafficClass) != ETS_DEF_TCGID)))
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : TCGID is not in range "
                       "(0-7) \r\n", __FUNCTION__);
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexLldpXdot1dcbxRemETSRecoTrafficSelectionAlgorithmTable
 Input       :  The Indices
                LldpV2RemTimeMark
                LldpV2RemLocalIfIndex
                u4LldpV2RemLocalDestMACAddress
                LldpV2RemIndex
                LldpXdot1dcbxRemETSRecoTSATrafficClass
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexLldpXdot1dcbxRemETSRecoTrafficSelectionAlgorithmTable (UINT4
                                                                       *pu4LldpV2RemTimeMark,
                                                                       INT4
                                                                       *pi4LldpV2RemLocalIfIndex,
                                                                       UINT4
                                                                       *pu4LldpV2RemLocalDestMACAddress,
                                                                       INT4
                                                                       *pi4LldpV2RemIndex,
                                                                       UINT4
                                                                       *pu4LldpXdot1dcbxRemETSRecoTSATrafficClass)
{
    ETS_SHUTDOWN_CHECK;
    return (nmhGetNextIndexLldpXdot1dcbxRemETSRecoTrafficSelectionAlgorithmTable
            (DCBX_ZERO, pu4LldpV2RemTimeMark,
             DCBX_ZERO, pi4LldpV2RemLocalIfIndex,
             DCBX_ZERO, pu4LldpV2RemLocalDestMACAddress,
             DCBX_ZERO, pi4LldpV2RemIndex,
             DCBX_ZERO, pu4LldpXdot1dcbxRemETSRecoTSATrafficClass));

}

/****************************************************************************
 Function    :  nmhGetNextIndexLldpXdot1dcbxRemETSRecoTrafficSelectionAlgorithmTable
 Input       :  The Indices
                LldpV2RemTimeMark
                nextLldpV2RemTimeMark
                LldpV2RemLocalIfIndex
                nextLldpV2RemLocalIfIndex
                u4LldpV2RemLocalDestMACAddress
                nextLldpV2RemLocalDestMACAddress
                LldpV2RemIndex
                nextLldpV2RemIndex
                LldpXdot1dcbxRemETSRecoTSATrafficClass
                nextLldpXdot1dcbxRemETSRecoTSATrafficClass
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexLldpXdot1dcbxRemETSRecoTrafficSelectionAlgorithmTable (UINT4
                                                                      u4LldpV2RemTimeMark,
                                                                      UINT4
                                                                      *pu4NextLldpV2RemTimeMark,
                                                                      INT4
                                                                      i4LldpV2RemLocalIfIndex,
                                                                      INT4
                                                                      *pi4NextLldpV2RemLocalIfIndex,
                                                                      UINT4
                                                                      u4LldpV2RemLocalDestMACAddress,
                                                                      UINT4
                                                                      *pu4NextLldpV2RemLocalDestMACAddress,
                                                                      INT4
                                                                      i4LldpV2RemIndex,
                                                                      INT4
                                                                      *pi4NextLldpV2RemIndex,
                                                                      UINT4
                                                                      u4LldpXdot1dcbxRemETSRecoTSATrafficClass,
                                                                      UINT4
                                                                      *pu4NextLldpXdot1dcbxRemETSRecoTSATrafficClass)
{
    tETSPortEntry      *pETSPortEntry = NULL;
    tETSPortEntry       ETSPortEntry;
    INT4                i4IfIndex = i4LldpV2RemLocalIfIndex;
    INT4                i4RemIndex = i4LldpV2RemIndex;

    UNUSED_PARAM (u4LldpV2RemTimeMark);
    UNUSED_PARAM (u4LldpV2RemLocalDestMACAddress);
    ETS_SHUTDOWN_CHECK;

    pETSPortEntry = ETSUtlGetPortEntry ((UINT4) i4IfIndex);

    if ((pETSPortEntry == NULL) ||
        (pETSPortEntry->ETSRemPortInfo.i4ETSRemIndex == DCBX_ZERO) ||
        (i4RemIndex > pETSPortEntry->ETSRemPortInfo.i4ETSRemIndex) ||
        (!(pETSPortEntry->u1RemTlvUpdStatus & ETS_RECO_TLV_REM_UPD)) ||
        ((i4RemIndex == pETSPortEntry->ETSRemPortInfo.i4ETSRemIndex)
         && (u4LldpXdot1dcbxRemETSRecoTSATrafficClass >= ETS_TCGID7)))

    {
        MEMSET (&ETSPortEntry, DCBX_ZERO, sizeof (tETSPortEntry));
        ETSPortEntry.u4IfIndex = (UINT4) i4IfIndex;
        do
        {
            pETSPortEntry = (tETSPortEntry *)
                RBTreeGetNext (gETSGlobalInfo.pRbETSPortTbl,
                               &ETSPortEntry, DcbxUtlPortTblCmpFn);
            if ((pETSPortEntry != NULL) &&
                (pETSPortEntry->ETSRemPortInfo.i4ETSRemIndex != DCBX_ZERO) &&
                (pETSPortEntry->u1RemTlvUpdStatus & ETS_RECO_TLV_REM_UPD))
            {
                *pi4NextLldpV2RemLocalIfIndex = (INT4) pETSPortEntry->u4IfIndex;
                *pu4NextLldpV2RemTimeMark =
                    pETSPortEntry->ETSRemPortInfo.u4ETSRemTimeMark;
                *pu4NextLldpV2RemLocalDestMACAddress =
                    pETSPortEntry->ETSRemPortInfo.u4ETSDestRemMacIndex;
                *pi4NextLldpV2RemIndex =
                    pETSPortEntry->ETSRemPortInfo.i4ETSRemIndex;
                *pu4NextLldpXdot1dcbxRemETSRecoTSATrafficClass = DCBX_ZERO;
                return SNMP_SUCCESS;
            }
            if (pETSPortEntry != NULL)
            {
                MEMSET (&ETSPortEntry, DCBX_ZERO, sizeof (tETSPortEntry));
                ETSPortEntry.u4IfIndex = pETSPortEntry->u4IfIndex;
            }
        }
        while (pETSPortEntry != NULL);
    }
    else
    {
        *pi4NextLldpV2RemLocalIfIndex = (INT4) pETSPortEntry->u4IfIndex;
        *pu4NextLldpV2RemTimeMark =
            pETSPortEntry->ETSRemPortInfo.u4ETSRemTimeMark;
        *pu4NextLldpV2RemLocalDestMACAddress =
            pETSPortEntry->ETSRemPortInfo.u4ETSDestRemMacIndex;
        *pi4NextLldpV2RemIndex = pETSPortEntry->ETSRemPortInfo.i4ETSRemIndex;
        if (i4RemIndex < pETSPortEntry->ETSRemPortInfo.i4ETSRemIndex)
        {
            *pu4NextLldpXdot1dcbxRemETSRecoTSATrafficClass = DCBX_ZERO;
        }
        else
        {
            *pu4NextLldpXdot1dcbxRemETSRecoTSATrafficClass =
                u4LldpXdot1dcbxRemETSRecoTSATrafficClass + DCBX_MIN_VAL;
        }
        return SNMP_SUCCESS;
    }
    DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : Rbtree GetNext Node () "
                   "Returns NULL. \r\n", __FUNCTION__);
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetLldpXdot1dcbxRemETSRecoTrafficSelectionAlgorithm
 Input       :  The Indices
                LldpV2RemTimeMark
                LldpV2RemLocalIfIndex
                u4LldpV2RemLocalDestMACAddress
                LldpV2RemIndex
                LldpXdot1dcbxRemETSRecoTSATrafficClass

                The Object 
                retValLldpXdot1dcbxRemETSRecoTrafficSelectionAlgorithm
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetLldpXdot1dcbxRemETSRecoTrafficSelectionAlgorithm (UINT4
                                                        u4LldpV2RemTimeMark,
                                                        INT4
                                                        i4LldpV2RemLocalIfIndex,
                                                        UINT4
                                                        u4LldpV2RemLocalDestMACAddress,
                                                        INT4 i4LldpV2RemIndex,
                                                        UINT4
                                                        u4LldpXdot1dcbxRemETSRecoTSATrafficClass,
                                                        INT4
                                                        *pi4RetValLldpXdot1dcbxRemETSRecoTrafficSelectionAlgorithm)
{
    tETSPortEntry      *pETSPortEntry = NULL;

    UNUSED_PARAM (u4LldpV2RemTimeMark);
    UNUSED_PARAM (i4LldpV2RemIndex);
    UNUSED_PARAM (u4LldpV2RemLocalDestMACAddress);

    pETSPortEntry = ETSUtlGetPortEntry ((UINT4) i4LldpV2RemLocalIfIndex);
    if (pETSPortEntry == NULL)
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : ETSUtlGetPortEntry() "
                       "Returns NULL. \r\n", __FUNCTION__);
        return SNMP_FAILURE;
    }

    if (pETSPortEntry->ETSRemPortInfo.i4ETSRemIndex == DCBX_ZERO)
    {
        DCBX_TRC (DCBX_MGMT_TRC, NIL_INFO);
        return SNMP_FAILURE;
    }

    *pi4RetValLldpXdot1dcbxRemETSRecoTrafficSelectionAlgorithm =
        (INT4) ((pETSPortEntry->ETSRemPortInfo).
                au1ETSRemRecoTsaTable
                [u4LldpXdot1dcbxRemETSRecoTSATrafficClass]);

    return SNMP_SUCCESS;

}

/* LOW LEVEL Routines for Table : LldpXdot1dcbxRemPFCBasicTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceLldpXdot1dcbxRemPFCBasicTable
 Input       :  The Indices
                LldpV2RemTimeMark
                LldpV2RemLocalIfIndex
                u4LldpV2RemLocalDestMACAddress
                LldpV2RemIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceLldpXdot1dcbxRemPFCBasicTable (UINT4
                                                       u4LldpV2RemTimeMark,
                                                       INT4
                                                       i4LldpV2RemLocalIfIndex,
                                                       UINT4
                                                       u4LldpV2RemLocalDestMACAddress,
                                                       INT4 i4LldpV2RemIndex)
{
    tPFCPortEntry      *pPFCPortEntry = NULL;
    tPFCRemPortInfo    *pPFCRemPortInfo = NULL;
    PFC_SHUTDOWN_CHECK;

    pPFCPortEntry = PFCUtlGetPortEntry ((UINT4) i4LldpV2RemLocalIfIndex);
    if (pPFCPortEntry == NULL)
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : PFCUtlGetPortEntry() "
                       "Returns NULL. \r\n", __FUNCTION__);
        return SNMP_FAILURE;
    }

    pPFCRemPortInfo = &(pPFCPortEntry->PFCRemPortInfo);
    if (pPFCPortEntry->PFCRemPortInfo.i4PFCRemIndex == DCBX_ZERO)
    {
        DCBX_TRC (DCBX_MGMT_TRC, NIL_INFO);
        return SNMP_FAILURE;
    }

    if ((pPFCPortEntry->PFCRemPortInfo.i4PFCRemIndex < DCBX_ZERO) ||
        (pPFCPortEntry->PFCRemPortInfo.i4PFCRemIndex > DCBX_REM_MAX_INDEX))
    {
        DCBX_TRC (DCBX_MGMT_TRC, INDEX_NOT_IN_RANGE);
        return SNMP_FAILURE;
    }
    if (pPFCPortEntry->PFCRemPortInfo.u4PFCDestRemMacIndex == DCBX_ZERO)
    {
        DCBX_TRC (DCBX_MGMT_TRC, WRONG_VAL);
        return SNMP_FAILURE;
    }

    if ((pPFCRemPortInfo->u4PFCRemTimeMark != u4LldpV2RemTimeMark) ||
        (pPFCRemPortInfo->i4PFCRemIndex != i4LldpV2RemIndex) ||
        (pPFCRemPortInfo->u4PFCDestRemMacIndex !=
         u4LldpV2RemLocalDestMACAddress))
    {
        DCBX_TRC (DCBX_MGMT_TRC, MISMATCH);
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexLldpXdot1dcbxRemPFCBasicTable
 Input       :  The Indices
                LldpV2RemTimeMark
                LldpV2RemLocalIfIndex
                u4LldpV2RemLocalDestMACAddress
                LldpV2RemIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexLldpXdot1dcbxRemPFCBasicTable (UINT4 *pu4LldpV2RemTimeMark,
                                               INT4 *pi4LldpV2RemLocalIfIndex,
                                               UINT4
                                               *pu4LldpV2RemLocalDestMACAddress,
                                               INT4 *pi4LldpV2RemIndex)
{
    PFC_SHUTDOWN_CHECK;
    return (nmhGetNextIndexLldpXdot1dcbxRemPFCBasicTable
            (DCBX_ZERO, pu4LldpV2RemTimeMark, DCBX_ZERO,
             pi4LldpV2RemLocalIfIndex, DCBX_ZERO,
             pu4LldpV2RemLocalDestMACAddress, DCBX_ZERO, pi4LldpV2RemIndex));

}

/****************************************************************************
 Function    :  nmhGetNextIndexLldpXdot1dcbxRemPFCBasicTable
 Input       :  The Indices
                LldpV2RemTimeMark
                nextLldpV2RemTimeMark
                LldpV2RemLocalIfIndex
                nextLldpV2RemLocalIfIndex
                u4LldpV2RemLocalDestMACAddress
                nextLldpV2RemLocalDestMACAddress
                LldpV2RemIndex
                nextLldpV2RemIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexLldpXdot1dcbxRemPFCBasicTable (UINT4 u4LldpV2RemTimeMark,
                                              UINT4 *pu4NextLldpV2RemTimeMark,
                                              INT4 i4LldpV2RemLocalIfIndex,
                                              INT4
                                              *pi4NextLldpV2RemLocalIfIndex,
                                              UINT4
                                              u4LldpV2RemLocalDestMACAddress,
                                              UINT4
                                              *pu4NextLldpV2RemLocalDestMACAddress,
                                              INT4 i4LldpV2RemIndex,
                                              INT4 *pi4NextLldpV2RemIndex)
{
    tPFCPortEntry      *pPFCPortEntry = NULL;
    tPFCPortEntry       PFCPortEntry;
    INT4                i4IfIndex = i4LldpV2RemLocalIfIndex;
    INT4                i4RemIndex = i4LldpV2RemIndex;

    UNUSED_PARAM (u4LldpV2RemTimeMark);
    UNUSED_PARAM (u4LldpV2RemLocalDestMACAddress);
    PFC_SHUTDOWN_CHECK;

    pPFCPortEntry = PFCUtlGetPortEntry ((UINT4) i4IfIndex);

    if ((pPFCPortEntry == NULL) ||
        (i4RemIndex >= pPFCPortEntry->PFCRemPortInfo.i4PFCRemIndex))

    {
        MEMSET (&PFCPortEntry, DCBX_ZERO, sizeof (tPFCPortEntry));
        PFCPortEntry.u4IfIndex = (UINT4) i4IfIndex;
        do
        {
            pPFCPortEntry = (tPFCPortEntry *)
                RBTreeGetNext (gPFCGlobalInfo.pRbPFCPortTbl,
                               &PFCPortEntry, DcbxUtlPortTblCmpFn);
            if ((pPFCPortEntry != NULL) &&
                (pPFCPortEntry->PFCRemPortInfo.i4PFCRemIndex != DCBX_ZERO))
            {
                *pi4NextLldpV2RemLocalIfIndex = (INT4) pPFCPortEntry->u4IfIndex;
                *pu4NextLldpV2RemTimeMark =
                    pPFCPortEntry->PFCRemPortInfo.u4PFCRemTimeMark;
                *pu4NextLldpV2RemLocalDestMACAddress =
                    pPFCPortEntry->PFCRemPortInfo.u4PFCDestRemMacIndex;
                *pi4NextLldpV2RemIndex =
                    pPFCPortEntry->PFCRemPortInfo.i4PFCRemIndex;
                return SNMP_SUCCESS;
            }
            if (pPFCPortEntry != NULL)
            {
                MEMSET (&PFCPortEntry, DCBX_ZERO, sizeof (tPFCPortEntry));
                PFCPortEntry.u4IfIndex = pPFCPortEntry->u4IfIndex;
            }
        }
        while (pPFCPortEntry != NULL);
    }
    else
    {

        *pi4NextLldpV2RemLocalIfIndex = (INT4) pPFCPortEntry->u4IfIndex;
        *pu4NextLldpV2RemTimeMark =
            pPFCPortEntry->PFCRemPortInfo.u4PFCRemTimeMark;
        *pu4NextLldpV2RemLocalDestMACAddress =
            pPFCPortEntry->PFCRemPortInfo.u4PFCDestRemMacIndex;
        *pi4NextLldpV2RemIndex = pPFCPortEntry->PFCRemPortInfo.i4PFCRemIndex;
        return SNMP_SUCCESS;
    }
    DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : Rbtree GetNext Node () "
                   "Returns NULL. \r\n", __FUNCTION__);
    return SNMP_FAILURE;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetLldpXdot1dcbxRemPFCWilling
 Input       :  The Indices
                LldpV2RemTimeMark
                LldpV2RemLocalIfIndex
                u4LldpV2RemLocalDestMACAddress
                LldpV2RemIndex

                The Object 
                retValLldpXdot1dcbxRemPFCWilling
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetLldpXdot1dcbxRemPFCWilling (UINT4 u4LldpV2RemTimeMark,
                                  INT4 i4LldpV2RemLocalIfIndex,
                                  UINT4 u4LldpV2RemLocalDestMACAddress,
                                  INT4 i4LldpV2RemIndex,
                                  INT4 *pi4RetValLldpXdot1dcbxRemPFCWilling)
{
    tPFCPortEntry      *pPFCPortEntry = NULL;

    UNUSED_PARAM (u4LldpV2RemTimeMark);
    UNUSED_PARAM (i4LldpV2RemIndex);
    UNUSED_PARAM (u4LldpV2RemLocalDestMACAddress);

    pPFCPortEntry = PFCUtlGetPortEntry ((UINT4) i4LldpV2RemLocalIfIndex);
    if (pPFCPortEntry == NULL)
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : PFCUtlGetPortEntry() "
                       "Returns NULL. \r\n", __FUNCTION__);
        return SNMP_FAILURE;
    }

    if (pPFCPortEntry->PFCRemPortInfo.i4PFCRemIndex == DCBX_ZERO)
    {
        DCBX_TRC (DCBX_MGMT_TRC, NIL_INFO);
        return SNMP_FAILURE;
    }

    *pi4RetValLldpXdot1dcbxRemPFCWilling =
        pPFCPortEntry->PFCRemPortInfo.u1PFCRemWilling;

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetLldpXdot1dcbxRemPFCMBC
 Input       :  The Indices
                LldpV2RemTimeMark
                LldpV2RemLocalIfIndex
                u4LldpV2RemLocalDestMACAddress
                LldpV2RemIndex

                The Object 
                retValLldpXdot1dcbxRemPFCMBC
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetLldpXdot1dcbxRemPFCMBC (UINT4 u4LldpV2RemTimeMark,
                              INT4 i4LldpV2RemLocalIfIndex,
                              UINT4 u4LldpV2RemLocalDestMACAddress,
                              INT4 i4LldpV2RemIndex,
                              INT4 *pi4RetValLldpXdot1dcbxRemPFCMBC)
{
    tPFCPortEntry      *pPFCPortEntry = NULL;

    UNUSED_PARAM (u4LldpV2RemTimeMark);
    UNUSED_PARAM (i4LldpV2RemIndex);
    UNUSED_PARAM (u4LldpV2RemLocalDestMACAddress);

    pPFCPortEntry = PFCUtlGetPortEntry ((UINT4) i4LldpV2RemLocalIfIndex);
    if (pPFCPortEntry == NULL)
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : PFCUtlGetPortEntry() "
                       "Returns NULL. \r\n", __FUNCTION__);
        return SNMP_FAILURE;
    }

    if (pPFCPortEntry->PFCRemPortInfo.i4PFCRemIndex == DCBX_ZERO)
    {
        DCBX_TRC (DCBX_MGMT_TRC, NIL_INFO);
        return SNMP_FAILURE;
    }

    *pi4RetValLldpXdot1dcbxRemPFCMBC =
        pPFCPortEntry->PFCRemPortInfo.u1PFCRemMBC;

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetLldpXdot1dcbxRemPFCCap
 Input       :  The Indices
                LldpV2RemTimeMark
                LldpV2RemLocalIfIndex
                u4LldpV2RemLocalDestMACAddress
                LldpV2RemIndex

                The Object 
                retValLldpXdot1dcbxRemPFCCap
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetLldpXdot1dcbxRemPFCCap (UINT4 u4LldpV2RemTimeMark,
                              INT4 i4LldpV2RemLocalIfIndex,
                              UINT4 u4LldpV2RemLocalDestMACAddress,
                              INT4 i4LldpV2RemIndex,
                              UINT4 *pu4RetValLldpXdot1dcbxRemPFCCap)
{
    tPFCPortEntry      *pPFCPortEntry = NULL;

    UNUSED_PARAM (u4LldpV2RemTimeMark);
    UNUSED_PARAM (i4LldpV2RemIndex);
    UNUSED_PARAM (u4LldpV2RemLocalDestMACAddress);

    pPFCPortEntry = PFCUtlGetPortEntry ((UINT4) i4LldpV2RemLocalIfIndex);
    if (pPFCPortEntry == NULL)
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : PFCUtlGetPortEntry() "
                       "Returns NULL. \r\n", __FUNCTION__);
        return SNMP_FAILURE;
    }

    if (pPFCPortEntry->PFCRemPortInfo.i4PFCRemIndex == DCBX_ZERO)
    {
        DCBX_TRC (DCBX_MGMT_TRC, NIL_INFO);
        return SNMP_FAILURE;
    }

    *pu4RetValLldpXdot1dcbxRemPFCCap =
        pPFCPortEntry->PFCRemPortInfo.u1PFCRemCap;
    return SNMP_SUCCESS;

}

/* LOW LEVEL Routines for Table : LldpXdot1dcbxRemPFCEnableTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceLldpXdot1dcbxRemPFCEnableTable
 Input       :  The Indices
                LldpV2RemTimeMark
                LldpV2RemLocalIfIndex
                u4LldpV2RemLocalDestMACAddress
                LldpV2RemIndex
                LldpXdot1dcbxRemPFCEnablePriority
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceLldpXdot1dcbxRemPFCEnableTable (UINT4
                                                        u4LldpV2RemTimeMark,
                                                        INT4
                                                        i4LldpV2RemLocalIfIndex,
                                                        UINT4
                                                        u4LldpV2RemLocalDestMACAddress,
                                                        INT4 i4LldpV2RemIndex,
                                                        UINT4
                                                        u4LldpXdot1dcbxRemPFCEnablePriority)
{
    tPFCPortEntry      *pPFCPortEntry = NULL;
    tPFCRemPortInfo    *pPFCRemPortInfo = NULL;
    PFC_SHUTDOWN_CHECK;

    pPFCPortEntry = PFCUtlGetPortEntry ((UINT4) i4LldpV2RemLocalIfIndex);
    if (pPFCPortEntry == NULL)
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : PFCUtlGetPortEntry() "
                       "Returns NULL. \r\n", __FUNCTION__);
        return SNMP_FAILURE;
    }

    pPFCRemPortInfo = &(pPFCPortEntry->PFCRemPortInfo);
    if (pPFCPortEntry->PFCRemPortInfo.i4PFCRemIndex == DCBX_ZERO)
    {
        DCBX_TRC (DCBX_MGMT_TRC, NIL_INFO);
        return SNMP_FAILURE;
    }

    if (pPFCPortEntry->PFCRemPortInfo.u4PFCRemTimeMark == DCBX_ZERO)
    {
        DCBX_TRC (DCBX_MGMT_TRC, WRONG_VAL);
        return SNMP_FAILURE;
    }

    if ((pPFCPortEntry->PFCRemPortInfo.i4PFCRemIndex < DCBX_ZERO) ||
        (pPFCPortEntry->PFCRemPortInfo.i4PFCRemIndex > DCBX_REM_MAX_INDEX))
    {
        DCBX_TRC (DCBX_MGMT_TRC, INDEX_NOT_IN_RANGE);
        return SNMP_FAILURE;
    }

    if (pPFCPortEntry->PFCRemPortInfo.u4PFCDestRemMacIndex == DCBX_ZERO)
    {
        DCBX_TRC (DCBX_MGMT_TRC, WRONG_VAL);
        return SNMP_FAILURE;
    }

    if ((pPFCRemPortInfo->u4PFCRemTimeMark != u4LldpV2RemTimeMark)
        || (pPFCRemPortInfo->i4PFCRemIndex != i4LldpV2RemIndex)
        || (pPFCRemPortInfo->u4PFCDestRemMacIndex !=
            u4LldpV2RemLocalDestMACAddress))
    {
        DCBX_TRC (DCBX_MGMT_TRC, MISMATCH);
    }

    if (((INT4) (u4LldpXdot1dcbxRemPFCEnablePriority) <
         DCBX_ZERO)
        || (u4LldpXdot1dcbxRemPFCEnablePriority >= DCBX_MAX_PRIORITIES))
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : Priority is not in range "
                       "(0-7) \r\n", __FUNCTION__);
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFirstIndexLldpXdot1dcbxRemPFCEnableTable
 Input       :  The Indices
                LldpV2RemTimeMark
                LldpV2RemLocalIfIndex
                u4LldpV2RemLocalDestMACAddress
                LldpV2RemIndex
                LldpXdot1dcbxRemPFCEnablePriority
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexLldpXdot1dcbxRemPFCEnableTable (UINT4 *pu4LldpV2RemTimeMark,
                                                INT4 *pi4LldpV2RemLocalIfIndex,
                                                UINT4
                                                *pu4LldpV2RemLocalDestMACAddress,
                                                INT4 *pi4LldpV2RemIndex,
                                                UINT4
                                                *pu4LldpXdot1dcbxRemPFCEnablePriority)
{
    PFC_SHUTDOWN_CHECK;
    return (nmhGetNextIndexLldpXdot1dcbxRemPFCEnableTable
            (DCBX_ZERO, pu4LldpV2RemTimeMark, DCBX_ZERO,
             pi4LldpV2RemLocalIfIndex, DCBX_ZERO,
             pu4LldpV2RemLocalDestMACAddress, DCBX_ZERO,
             pi4LldpV2RemIndex, DCBX_ZERO,
             pu4LldpXdot1dcbxRemPFCEnablePriority));

}

/****************************************************************************
 Function    :  nmhGetNextIndexLldpXdot1dcbxRemPFCEnableTable
 Input       :  The Indices
                LldpV2RemTimeMark
                nextLldpV2RemTimeMark
                LldpV2RemLocalIfIndex
                nextLldpV2RemLocalIfIndex
                u4LldpV2RemLocalDestMACAddress
                nextLldpV2RemLocalDestMACAddress
                LldpV2RemIndex
                nextLldpV2RemIndex
                LldpXdot1dcbxRemPFCEnablePriority
                nextLldpXdot1dcbxRemPFCEnablePriority
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexLldpXdot1dcbxRemPFCEnableTable (UINT4 u4LldpV2RemTimeMark,
                                               UINT4 *pu4NextLldpV2RemTimeMark,
                                               INT4 i4LldpV2RemLocalIfIndex,
                                               INT4
                                               *pi4NextLldpV2RemLocalIfIndex,
                                               UINT4
                                               u4LldpV2RemLocalDestMACAddress,
                                               UINT4
                                               *pu4NextLldpV2RemLocalDestMACAddress,
                                               INT4 i4LldpV2RemIndex,
                                               INT4 *pi4NextLldpV2RemIndex,
                                               UINT4
                                               u4LldpXdot1dcbxRemPFCEnablePriority,
                                               UINT4
                                               *pu4NextLldpXdot1dcbxRemPFCEnablePriority)
{
    tPFCPortEntry      *pPFCPortEntry = NULL;
    tPFCPortEntry       PFCPortEntry;
    INT4                i4IfIndex = i4LldpV2RemLocalIfIndex;
    INT4                i4RemIndex = i4LldpV2RemIndex;

    UNUSED_PARAM (u4LldpV2RemTimeMark);
    UNUSED_PARAM (u4LldpV2RemLocalDestMACAddress);
    PFC_SHUTDOWN_CHECK;
    pPFCPortEntry = PFCUtlGetPortEntry ((UINT4) i4IfIndex);

    if ((pPFCPortEntry == NULL) ||
        (pPFCPortEntry->PFCRemPortInfo.i4PFCRemIndex == DCBX_ZERO) ||
        (i4RemIndex > pPFCPortEntry->PFCRemPortInfo.i4PFCRemIndex) ||
        ((i4RemIndex == pPFCPortEntry->PFCRemPortInfo.i4PFCRemIndex)
         && (u4LldpXdot1dcbxRemPFCEnablePriority >= DCBX_MAX_VALID_PRI)))

    {
        MEMSET (&PFCPortEntry, DCBX_ZERO, sizeof (tPFCPortEntry));
        PFCPortEntry.u4IfIndex = (UINT4) i4IfIndex;
        do
        {
            pPFCPortEntry = (tPFCPortEntry *)
                RBTreeGetNext (gPFCGlobalInfo.pRbPFCPortTbl,
                               &PFCPortEntry, DcbxUtlPortTblCmpFn);
            if ((pPFCPortEntry != NULL) &&
                (pPFCPortEntry->PFCRemPortInfo.i4PFCRemIndex != DCBX_ZERO))
            {
                *pi4NextLldpV2RemLocalIfIndex = (INT4) pPFCPortEntry->u4IfIndex;
                *pu4NextLldpV2RemTimeMark =
                    pPFCPortEntry->PFCRemPortInfo.u4PFCRemTimeMark;
                *pu4NextLldpV2RemLocalDestMACAddress =
                    pPFCPortEntry->PFCRemPortInfo.u4PFCDestRemMacIndex;
                *pi4NextLldpV2RemIndex =
                    pPFCPortEntry->PFCRemPortInfo.i4PFCRemIndex;
                *pu4NextLldpXdot1dcbxRemPFCEnablePriority = DCBX_ZERO;
                return SNMP_SUCCESS;
            }
            if (pPFCPortEntry != NULL)
            {
                MEMSET (&PFCPortEntry, DCBX_ZERO, sizeof (tPFCPortEntry));
                PFCPortEntry.u4IfIndex = pPFCPortEntry->u4IfIndex;
            }
        }
        while (pPFCPortEntry != NULL);
    }
    else
    {
        *pi4NextLldpV2RemLocalIfIndex = (INT4) pPFCPortEntry->u4IfIndex;
        *pu4NextLldpV2RemTimeMark =
            pPFCPortEntry->PFCRemPortInfo.u4PFCRemTimeMark;
        *pu4NextLldpV2RemLocalDestMACAddress =
            pPFCPortEntry->PFCRemPortInfo.u4PFCDestRemMacIndex;
        *pi4NextLldpV2RemIndex = pPFCPortEntry->PFCRemPortInfo.i4PFCRemIndex;
        if (i4RemIndex < pPFCPortEntry->PFCRemPortInfo.i4PFCRemIndex)
        {
            *pu4NextLldpXdot1dcbxRemPFCEnablePriority = DCBX_ZERO;
        }
        else
        {
            *pu4NextLldpXdot1dcbxRemPFCEnablePriority =
                u4LldpXdot1dcbxRemPFCEnablePriority + DCBX_MIN_VAL;
        }
        return SNMP_SUCCESS;
    }
    DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : Rbtree GetNext Node () "
                   "Returns NULL. \r\n", __FUNCTION__);
    return SNMP_FAILURE;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetLldpXdot1dcbxRemPFCEnableEnabled
 Input       :  The Indices
                LldpV2RemTimeMark
                LldpV2RemLocalIfIndex
                u4LldpV2RemLocalDestMACAddress
                LldpV2RemIndex
                LldpXdot1dcbxRemPFCEnablePriority

                The Object 
                retValLldpXdot1dcbxRemPFCEnableEnabled
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetLldpXdot1dcbxRemPFCEnableEnabled (UINT4 u4LldpV2RemTimeMark,
                                        INT4 i4LldpV2RemLocalIfIndex,
                                        UINT4 u4LldpV2RemLocalDestMACAddress,
                                        INT4 i4LldpV2RemIndex,
                                        UINT4
                                        u4LldpXdot1dcbxRemPFCEnablePriority,
                                        INT4
                                        *pi4RetValLldpXdot1dcbxRemPFCEnableEnabled)
{
    tPFCPortEntry      *pPFCPortEntry = NULL;
    UINT1              *pu1PfcStatus = NULL;
    UINT1               u1PfcValue = DCBX_ZERO;

    UNUSED_PARAM (u4LldpV2RemTimeMark);
    UNUSED_PARAM (i4LldpV2RemIndex);
    UNUSED_PARAM (u4LldpV2RemLocalDestMACAddress);

    pPFCPortEntry = PFCUtlGetPortEntry ((UINT4) i4LldpV2RemLocalIfIndex);
    if (pPFCPortEntry == NULL)
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : PFCUtlGetPortEntry() "
                       "Returns NULL. \r\n", __FUNCTION__);
        return SNMP_FAILURE;
    }

    if (pPFCPortEntry->PFCRemPortInfo.i4PFCRemIndex == DCBX_ZERO)
    {
        DCBX_TRC (DCBX_MGMT_TRC, NIL_INFO);
        return SNMP_FAILURE;
    }

    pu1PfcStatus = &((pPFCPortEntry->PFCRemPortInfo).u1PFCRemStatus);

    u1PfcValue = *pu1PfcStatus;
    u1PfcValue = (UINT1) (u1PfcValue >> u4LldpXdot1dcbxRemPFCEnablePriority);
    u1PfcValue = u1PfcValue & 0x01;

    if (u1PfcValue == DCBX_ZERO)
    {
        *pi4RetValLldpXdot1dcbxRemPFCEnableEnabled = PFC_DISABLED;
    }
    else
    {
        *pi4RetValLldpXdot1dcbxRemPFCEnableEnabled = PFC_ENABLED;
    }
    return SNMP_SUCCESS;

}

/* LOW LEVEL Routines for Table : LldpXdot1dcbxRemApplicationPriorityAppTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceLldpXdot1dcbxRemApplicationPriorityAppTable
 Input       :  The Indices
                LldpV2RemTimeMark
                LldpV2RemLocalIfIndex
                u4LldpV2RemLocalDestMACAddress
                LldpV2RemIndex
                LldpXdot1dcbxRemApplicationPriorityAESelector
                LldpXdot1dcbxRemApplicationPriorityAEProtocol
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceLldpXdot1dcbxRemApplicationPriorityAppTable (UINT4
                                                                     u4LldpV2RemTimeMark,
                                                                     INT4
                                                                     i4LldpV2RemLocalIfIndex,
                                                                     UINT4
                                                                     u4LldpV2RemLocalDestMACAddress,
                                                                     INT4
                                                                     i4LldpV2RemIndex,
                                                                     INT4
                                                                     i4LldpXdot1dcbxRemApplicationPriorityAESelector,
                                                                     UINT4
                                                                     u4LldpXdot1dcbxRemApplicationPriorityAEProtocol)
{

    tAppPriPortEntry   *pAppPortEntry = NULL;
    tAppPriRemPortInfo *pAppRemPortInfo = NULL;
    APP_PRI_SHUTDOWN_CHECK;

    pAppPortEntry = AppPriUtlGetPortEntry ((UINT4) i4LldpV2RemLocalIfIndex);
    if (pAppPortEntry == NULL)
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : "
                       "nmhValidateIndexInstanceLldpXdot1dcbxRemApplicationPriorityAppTable "
                       "AppPriUtlGetPortEntry() Returned NULL. \r\n",
                       __FUNCTION__);
        return SNMP_FAILURE;
    }
    if (((INT4) u4LldpXdot1dcbxRemApplicationPriorityAEProtocol < DCBX_ZERO) ||
        (u4LldpXdot1dcbxRemApplicationPriorityAEProtocol >
         APP_PRI_MAX_PROTOCOL_ID))
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : "
                       "i4LldpXdot1dcbxLocApplicationPriorityAEProtocol "
                       "exceeded allowed range. \r\n", __FUNCTION__);
        return SNMP_FAILURE;
    }
    if ((i4LldpXdot1dcbxRemApplicationPriorityAESelector < DCBX_ONE) ||
        (i4LldpXdot1dcbxRemApplicationPriorityAESelector >
         APP_PRI_MAX_SELECTOR))
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : "
                       "i4LldpXdot1dcbxLocApplicationPriorityAESelector "
                       "exceeded allowed range. \r\n", __FUNCTION__);
        return SNMP_FAILURE;
    }

    if (TMO_SLL_Count (&(APP_PRI_REM_TBL (pAppPortEntry,
                                          i4LldpXdot1dcbxRemApplicationPriorityAESelector
                                          - 1))) == DCBX_ZERO)
    {
        DCBX_TRC_ARG3 (DCBX_MGMT_TRC, "In %s : "
                       "nmhValidateIndexInstanceLldpXdot1dcbxRemApplicationPriorityAppTable "
                       "AppPriMappingTable is NULL for port %d selector %d \r\n",
                       __FUNCTION__, i4LldpV2RemLocalIfIndex,
                       i4LldpXdot1dcbxRemApplicationPriorityAESelector);
        return SNMP_FAILURE;
    }

    if (AppPriUtlGetAppPriMappingEntry ((UINT4) i4LldpV2RemLocalIfIndex,
                                        i4LldpXdot1dcbxRemApplicationPriorityAESelector,
                                        (INT4)
                                        u4LldpXdot1dcbxRemApplicationPriorityAEProtocol,
                                        APP_PRI_REMOTE) == NULL)
    {
        DCBX_TRC_ARG4 (DCBX_MGMT_TRC, "In %s : "
                       "nmhValidateIndexInstanceLldpXdot1dcbxRemApplicationPriorityAppTable "
                       "AppPriMappingEntry is NULL for port %d selector %d Application %d\r\n",
                       __FUNCTION__, i4LldpV2RemLocalIfIndex,
                       i4LldpXdot1dcbxRemApplicationPriorityAESelector,
                       u4LldpXdot1dcbxRemApplicationPriorityAEProtocol);
        return SNMP_FAILURE;
    }
    pAppRemPortInfo = &(pAppPortEntry->AppPriRemPortInfo);
    if ((pAppPortEntry->AppPriRemPortInfo.i4AppPriRemIndex < DCBX_ZERO) ||
        (pAppPortEntry->AppPriRemPortInfo.i4AppPriRemIndex >
         DCBX_REM_MAX_INDEX))
    {
        DCBX_TRC (DCBX_MGMT_TRC, INDEX_NOT_IN_RANGE);
        return SNMP_FAILURE;
    }

    if ((pAppPortEntry->AppPriRemPortInfo.u4AppPriDestRemMacIndex == DCBX_ZERO))
    {
        DCBX_TRC (DCBX_MGMT_TRC, WRONG_VAL);
        return SNMP_FAILURE;
    }

    if ((pAppRemPortInfo->u4AppPriRemTimeMark != u4LldpV2RemTimeMark) ||
        (pAppRemPortInfo->i4AppPriRemIndex != i4LldpV2RemIndex) ||
        (pAppRemPortInfo->u4AppPriDestRemMacIndex !=
         u4LldpV2RemLocalDestMACAddress))
    {
        DCBX_TRC (DCBX_MGMT_TRC, MISMATCH);
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexLldpXdot1dcbxRemApplicationPriorityAppTable
 Input       :  The Indices
                LldpV2RemTimeMark
                LldpV2RemLocalIfIndex
                u4LldpV2RemLocalDestMACAddress
                LldpV2RemIndex
                LldpXdot1dcbxRemApplicationPriorityAESelector
                LldpXdot1dcbxRemApplicationPriorityAEProtocol
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexLldpXdot1dcbxRemApplicationPriorityAppTable (UINT4
                                                             *pu4LldpV2RemTimeMark,
                                                             INT4
                                                             *pi4LldpV2RemLocalIfIndex,
                                                             UINT4
                                                             *pu4LldpV2RemLocalDestMACAddress,
                                                             INT4
                                                             *pi4LldpV2RemIndex,
                                                             INT4
                                                             *pi4LldpXdot1dcbxRemApplicationPriorityAESelector,
                                                             UINT4
                                                             *pu4LldpXdot1dcbxRemApplicationPriorityAEProtocol)
{
    APP_PRI_SHUTDOWN_CHECK;
    return (nmhGetNextIndexLldpXdot1dcbxRemApplicationPriorityAppTable
            (DCBX_ZERO, pu4LldpV2RemTimeMark, DCBX_ZERO,
             pi4LldpV2RemLocalIfIndex, DCBX_ZERO,
             pu4LldpV2RemLocalDestMACAddress, DCBX_ZERO, pi4LldpV2RemIndex,
             DCBX_ONE, pi4LldpXdot1dcbxRemApplicationPriorityAESelector,
             DCBX_ZERO, pu4LldpXdot1dcbxRemApplicationPriorityAEProtocol));

}

/****************************************************************************
 Function    :  nmhGetNextIndexLldpXdot1dcbxRemApplicationPriorityAppTable
 Input       :  The Indices
                LldpV2RemTimeMark
                nextLldpV2RemTimeMark
                LldpV2RemLocalIfIndex
                nextLldpV2RemLocalIfIndex
                u4LldpV2RemLocalDestMACAddress
                nextLldpV2RemLocalDestMACAddress
                LldpV2RemIndex
                nextLldpV2RemIndex
                LldpXdot1dcbxRemApplicationPriorityAESelector
                nextLldpXdot1dcbxRemApplicationPriorityAESelector
                LldpXdot1dcbxRemApplicationPriorityAEProtocol
                nextLldpXdot1dcbxRemApplicationPriorityAEProtocol
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexLldpXdot1dcbxRemApplicationPriorityAppTable (UINT4
                                                            u4LldpV2RemTimeMark,
                                                            UINT4
                                                            *pu4NextLldpV2RemTimeMark,
                                                            INT4
                                                            i4LldpV2RemLocalIfIndex,
                                                            INT4
                                                            *pi4NextLldpV2RemLocalIfIndex,
                                                            UINT4
                                                            u4LldpV2RemLocalDestMACAddress,
                                                            UINT4
                                                            *pu4NextLldpV2RemLocalDestMACAddress,
                                                            INT4
                                                            i4LldpV2RemIndex,
                                                            INT4
                                                            *pi4NextLldpV2RemIndex,
                                                            INT4
                                                            i4LldpXdot1dcbxRemApplicationPriorityAESelector,
                                                            INT4
                                                            *pi4NextLldpXdot1dcbxRemApplicationPriorityAESelector,
                                                            UINT4
                                                            u4LldpXdot1dcbxRemApplicationPriorityAEProtocol,
                                                            UINT4
                                                            *pu4NextLldpXdot1dcbxRemApplicationPriorityAEProtocol)
{

    tAppPriPortEntry   *pAppPortEntry = NULL;
    tAppPriPortEntry    AppPortEntry;
    tAppPriMappingEntry *pAppPriMappingEntry = NULL;
    INT4                i4NextAPPortNumber = DCBX_ZERO;
    INT4                i4APPortNumber = DCBX_ZERO;
    INT4                i4Selector = DCBX_ZERO;
    INT4                i4Protocol = DCBX_ZERO;

    UNUSED_PARAM (u4LldpV2RemTimeMark);
    UNUSED_PARAM (u4LldpV2RemLocalDestMACAddress);
    UNUSED_PARAM (i4LldpV2RemIndex);
    APP_PRI_SHUTDOWN_CHECK;

    MEMSET (&AppPortEntry, DCBX_ZERO, sizeof (tAppPriPortEntry));

    i4NextAPPortNumber = i4LldpV2RemLocalIfIndex;
    i4APPortNumber = i4NextAPPortNumber;
    pAppPortEntry = AppPriUtlGetPortEntry ((UINT4) i4APPortNumber);
    if (pAppPortEntry == NULL)
    {
        MEMSET (&AppPortEntry, DCBX_ZERO, sizeof (tAppPriPortEntry));
        AppPortEntry.u4IfIndex = (UINT4) i4APPortNumber;
        pAppPortEntry = NULL;
        pAppPortEntry = (tAppPriPortEntry *)
            RBTreeGetNext (gAppPriGlobalInfo.pRbAppPriPortTbl,
                           &AppPortEntry, AppPriUtlPortTblCmpFn);
    }
    while (pAppPortEntry != NULL)
    {
        i4APPortNumber = (INT4) (pAppPortEntry->u4IfIndex);

        if (pAppPortEntry->u4IfIndex == (UINT4) i4LldpV2RemLocalIfIndex)
        {
            i4Selector = i4LldpXdot1dcbxRemApplicationPriorityAESelector;
        }
        else
        {
            i4Selector = DCBX_ONE;
        }
        for (; i4Selector <= APP_PRI_MAX_SELECTOR; i4Selector++)
        {

            if (TMO_SLL_Count
                (&(APP_PRI_REM_TBL (pAppPortEntry, i4Selector - 1))) !=
                DCBX_ZERO)
            {
                if ((pAppPortEntry->u4IfIndex ==
                     (UINT4) i4LldpV2RemLocalIfIndex)
                    && (i4Selector ==
                        i4LldpXdot1dcbxRemApplicationPriorityAESelector))
                {
                    i4Protocol =
                        (INT4) (u4LldpXdot1dcbxRemApplicationPriorityAEProtocol
                                + DCBX_ONE);
                }
                else
                {
                    i4Protocol = DCBX_ZERO;
                }
                TMO_SLL_Scan (&
                              (APP_PRI_REM_TBL (pAppPortEntry, i4Selector - 1)),
                              pAppPriMappingEntry, tAppPriMappingEntry *)
                {
                    if (pAppPriMappingEntry->i4AppPriProtocol >= i4Protocol)
                    {
                        *pu4NextLldpV2RemTimeMark =
                            pAppPortEntry->AppPriRemPortInfo.
                            u4AppPriRemTimeMark;
                        *pu4NextLldpV2RemLocalDestMACAddress =
                            pAppPortEntry->AppPriRemPortInfo.
                            u4AppPriDestRemMacIndex;
                        *pi4NextLldpV2RemIndex =
                            pAppPortEntry->AppPriRemPortInfo.i4AppPriRemIndex;

                        *pi4NextLldpV2RemLocalIfIndex = i4APPortNumber;
                        *pi4NextLldpXdot1dcbxRemApplicationPriorityAESelector =
                            i4Selector;
                        *pu4NextLldpXdot1dcbxRemApplicationPriorityAEProtocol =
                            (UINT4) pAppPriMappingEntry->i4AppPriProtocol;
                        return SNMP_SUCCESS;
                    }
                }
            }                    /*end of if TMO_SLL_Count */
        }                        /* end of for loop -selector */

        MEMSET (&AppPortEntry, DCBX_ZERO, sizeof (tAppPriPortEntry));
        AppPortEntry.u4IfIndex = pAppPortEntry->u4IfIndex;
        pAppPortEntry = NULL;
        pAppPortEntry = (tAppPriPortEntry *)
            RBTreeGetNext (gAppPriGlobalInfo.pRbAppPriPortTbl,
                           &AppPortEntry, AppPriUtlPortTblCmpFn);
    }
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetLldpXdot1dcbxRemApplicationPriorityAEPriority
 Input       :  The Indices
                LldpV2RemTimeMark
                LldpV2RemLocalIfIndex
                u4LldpV2RemLocalDestMACAddress
                LldpV2RemIndex
                LldpXdot1dcbxRemApplicationPriorityAESelector
                LldpXdot1dcbxRemApplicationPriorityAEProtocol

                The Object 
                retValLldpXdot1dcbxRemApplicationPriorityAEPriority
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetLldpXdot1dcbxRemApplicationPriorityAEPriority (UINT4 u4LldpV2RemTimeMark,
                                                     INT4
                                                     i4LldpV2RemLocalIfIndex,
                                                     UINT4
                                                     u4LldpV2RemLocalDestMACAddress,
                                                     INT4 i4LldpV2RemIndex,
                                                     INT4
                                                     i4LldpXdot1dcbxRemApplicationPriorityAESelector,
                                                     UINT4
                                                     u4LldpXdot1dcbxRemApplicationPriorityAEProtocol,
                                                     UINT4
                                                     *pu4RetValLldpXdot1dcbxRemApplicationPriorityAEPriority)
{
    tAppPriMappingEntry *pAppPriMappingEntry = NULL;

    UNUSED_PARAM (u4LldpV2RemTimeMark);
    UNUSED_PARAM (u4LldpV2RemLocalDestMACAddress);
    UNUSED_PARAM (i4LldpV2RemIndex);
    APP_PRI_SHUTDOWN_CHECK;
    pAppPriMappingEntry = AppPriUtlGetAppPriMappingEntry
        ((UINT4) i4LldpV2RemLocalIfIndex,
         i4LldpXdot1dcbxRemApplicationPriorityAESelector,
         (INT4) u4LldpXdot1dcbxRemApplicationPriorityAEProtocol,
         APP_PRI_REMOTE);
    if (pAppPriMappingEntry == NULL)
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC,
                       "In %s : nmhGetLldpXdot1dcbxRemApplicationPriorityAEPriority "
                       " AppPriUtlGetAppPriMappingEntry returned NULL. \r\n",
                       __FUNCTION__);

        return SNMP_FAILURE;
    }
    *pu4RetValLldpXdot1dcbxRemApplicationPriorityAEPriority =
        pAppPriMappingEntry->u1AppPriPriority;
    return SNMP_SUCCESS;

}

/* LOW LEVEL Routines for Table : LldpXdot1dcbxAdminETSBasicConfigurationTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceLldpXdot1dcbxAdminETSBasicConfigurationTable
 Input       :  The Indices
                LldpV2LocPortIfIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceLldpXdot1dcbxAdminETSBasicConfigurationTable (INT4
                                                                      i4LldpV2LocPortIfIndex)
{
    tETSPortEntry      *pETSPortEntry = NULL;

    ETS_SHUTDOWN_CHECK;

    pETSPortEntry = ETSUtlGetPortEntry ((UINT4) i4LldpV2LocPortIfIndex);
    if (pETSPortEntry == NULL)
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : ETSUtlGetPortEntry() "
                       "Returns NULL. \r\n", __FUNCTION__);
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFirstIndexLldpXdot1dcbxAdminETSBasicConfigurationTable
 Input       :  The Indices
                LldpV2LocPortIfIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexLldpXdot1dcbxAdminETSBasicConfigurationTable (INT4
                                                              *pi4LldpV2LocPortIfIndex)
{
    tETSPortEntry      *pETSPortEntry = NULL;

    ETS_SHUTDOWN_CHECK;
    pETSPortEntry = (tETSPortEntry *)
        RBTreeGetFirst (gETSGlobalInfo.pRbETSPortTbl);
    if (pETSPortEntry == NULL)
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : RbTreeGetFirst Node () "
                       "Returns NULL. \r\n", __FUNCTION__);
        return SNMP_FAILURE;
    }
    *pi4LldpV2LocPortIfIndex = (INT4) pETSPortEntry->u4IfIndex;
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetNextIndexLldpXdot1dcbxAdminETSBasicConfigurationTable
 Input       :  The Indices
                LldpV2LocPortIfIndex
                nextLldpV2LocPortIfIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexLldpXdot1dcbxAdminETSBasicConfigurationTable (INT4
                                                             i4LldpV2LocPortIfIndex,
                                                             INT4
                                                             *pi4NextLldpV2LocPortIfIndex)
{
    tETSPortEntry      *pETSPortEntry = NULL;
    tETSPortEntry       ETSPortEntry;

    ETS_SHUTDOWN_CHECK;
    MEMSET (&ETSPortEntry, DCBX_ZERO, sizeof (tETSPortEntry));
    ETSPortEntry.u4IfIndex = (UINT4) i4LldpV2LocPortIfIndex;
    pETSPortEntry = (tETSPortEntry *)
        RBTreeGetNext (gETSGlobalInfo.pRbETSPortTbl,
                       &ETSPortEntry, DcbxUtlPortTblCmpFn);
    if (pETSPortEntry == NULL)
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : Rbtree GetNext Node () "
                       "Returns NULL. \r\n", __FUNCTION__);
        return SNMP_FAILURE;
    }
    *pi4NextLldpV2LocPortIfIndex = (INT4) pETSPortEntry->u4IfIndex;
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetLldpXdot1dcbxAdminETSConCreditBasedShaperSupport
 Input       :  The Indices
                LldpV2LocPortIfIndex

                The Object 
                retValLldpXdot1dcbxAdminETSConCreditBasedShaperSupport
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetLldpXdot1dcbxAdminETSConCreditBasedShaperSupport (INT4
                                                        i4LldpV2LocPortIfIndex,
                                                        INT4
                                                        *pi4RetValLldpXdot1dcbxAdminETSConCreditBasedShaperSupport)
{
    tETSPortEntry      *pETSPortEntry = NULL;

    pETSPortEntry = ETSUtlGetPortEntry ((UINT4) i4LldpV2LocPortIfIndex);
    if (pETSPortEntry == NULL)
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : ETSUtlGetPortEntry () "
                       " Returns FAILURE. \r\n", __FUNCTION__);
        return SNMP_FAILURE;
    }

    *pi4RetValLldpXdot1dcbxAdminETSConCreditBasedShaperSupport =
        pETSPortEntry->ETSAdmPortInfo.u1ETSAdmCBS;
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetLldpXdot1dcbxAdminETSConTrafficClassesSupported
 Input       :  The Indices
                LldpV2LocPortIfIndex

                The Object 
                retValLldpXdot1dcbxAdminETSConTrafficClassesSupported
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetLldpXdot1dcbxAdminETSConTrafficClassesSupported (INT4
                                                       i4LldpV2LocPortIfIndex,
                                                       UINT4
                                                       *pu4RetValLldpXdot1dcbxAdminETSConTrafficClassesSupported)
{
    tETSPortEntry      *pETSPortEntry = NULL;

    pETSPortEntry = ETSUtlGetPortEntry ((UINT4) i4LldpV2LocPortIfIndex);
    if (pETSPortEntry == NULL)
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : ETSUtlGetPortEntry () "
                       " Returns FAILURE. \r\n", __FUNCTION__);
        return SNMP_FAILURE;
    }

    *pu4RetValLldpXdot1dcbxAdminETSConTrafficClassesSupported =
        pETSPortEntry->ETSAdmPortInfo.u1ETSAdmNumTcSup;
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetLldpXdot1dcbxAdminETSConWilling
 Input       :  The Indices
                LldpV2LocPortIfIndex

                The Object 
                retValLldpXdot1dcbxAdminETSConWilling
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetLldpXdot1dcbxAdminETSConWilling (INT4 i4LldpV2LocPortIfIndex,
                                       INT4
                                       *pi4RetValLldpXdot1dcbxAdminETSConWilling)
{
    tETSPortEntry      *pETSPortEntry = NULL;

    pETSPortEntry = ETSUtlGetPortEntry ((UINT4) i4LldpV2LocPortIfIndex);
    if (pETSPortEntry == NULL)
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : ETSUtlGetPortEntry () "
                       " Returns FAILURE. \r\n", __FUNCTION__);
        return SNMP_FAILURE;
    }

    *pi4RetValLldpXdot1dcbxAdminETSConWilling =
        (pETSPortEntry->ETSAdmPortInfo).u1ETSAdmWilling;
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetLldpXdot1dcbxAdminETSConWilling
 Input       :  The Indices
                LldpV2LocPortIfIndex

                The Object 
                setValLldpXdot1dcbxAdminETSConWilling
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetLldpXdot1dcbxAdminETSConWilling (INT4 i4LldpV2LocPortIfIndex,
                                       INT4
                                       i4SetValLldpXdot1dcbxAdminETSConWilling)
{
    tETSPortEntry      *pETSPortEntry = NULL;

    DCBX_TRC_ARG3 (DCBX_MGMT_TRC, "%s, Args: "
                   "i4LldpV2LocPortIfIndex: %d "
                   "i4SetValLldpXdot1dcbxAdminETSConWilling: %d\r\n",
                   __func__, i4LldpV2LocPortIfIndex,
                   i4SetValLldpXdot1dcbxAdminETSConWilling);

    pETSPortEntry = ETSUtlGetPortEntry ((UINT4) i4LldpV2LocPortIfIndex);
    if (pETSPortEntry == NULL)
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : ETSUtlGetPortEntry () "
                       " Returns FAILURE. \r\n", __FUNCTION__);
        return SNMP_FAILURE;
    }

    if (pETSPortEntry->ETSAdmPortInfo.u1ETSAdmWilling
        != (UINT1) i4SetValLldpXdot1dcbxAdminETSConWilling)
    {
        pETSPortEntry->ETSAdmPortInfo.u1ETSAdmWilling =
            (UINT1) i4SetValLldpXdot1dcbxAdminETSConWilling;
        pETSPortEntry->ETSLocPortInfo.u1ETSLocWilling =
            pETSPortEntry->ETSAdmPortInfo.u1ETSAdmWilling;
        ETSUtlWillingChange (pETSPortEntry, OSIX_TRUE);
    }
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2LldpXdot1dcbxAdminETSConWilling
 Input       :  The Indices
                LldpV2LocPortIfIndex

                The Object 
                testValLldpXdot1dcbxAdminETSConWilling
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2LldpXdot1dcbxAdminETSConWilling (UINT4 *pu4ErrorCode,
                                          INT4 i4LldpV2LocPortIfIndex,
                                          INT4
                                          i4TestValLldpXdot1dcbxAdminETSConWilling)
{
    tETSPortEntry      *pETSPortEntry = NULL;

    ETS_SHUTDOWN_CHECK_TEST;
    pETSPortEntry = ETSUtlGetPortEntry ((UINT4) i4LldpV2LocPortIfIndex);
    if (pETSPortEntry == NULL)
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : ETSUtlGetPortEntry () "
                       " Returns FAILURE. \r\n", __FUNCTION__);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_ERR_ETS_NO_PORT_ENTRY);
        return SNMP_FAILURE;
    }

    if ((i4TestValLldpXdot1dcbxAdminETSConWilling != ETS_ENABLED)
        && (i4TestValLldpXdot1dcbxAdminETSConWilling != ETS_DISABLED))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_ERR_ETS_INVALID_WILLING_STATUS);
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2LldpXdot1dcbxAdminETSBasicConfigurationTable
 Input       :  The Indices
                LldpV2LocPortIfIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2LldpXdot1dcbxAdminETSBasicConfigurationTable (UINT4 *pu4ErrorCode,
                                                      tSnmpIndexList *
                                                      pSnmpIndexList,
                                                      tSNMP_VAR_BIND *
                                                      pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : LldpXdot1dcbxAdminETSConPriorityAssignmentTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceLldpXdot1dcbxAdminETSConPriorityAssignmentTable
 Input       :  The Indices
                LldpV2LocPortIfIndex
                LldpXdot1dcbxAdminETSConPriority
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceLldpXdot1dcbxAdminETSConPriorityAssignmentTable (INT4
                                                                         i4LldpV2LocPortIfIndex,
                                                                         UINT4
                                                                         u4LldpXdot1dcbxAdminETSConPriority)
{
    tETSPortEntry      *pETSPortEntry = NULL;

    ETS_SHUTDOWN_CHECK;
    pETSPortEntry = ETSUtlGetPortEntry ((UINT4) i4LldpV2LocPortIfIndex);
    if (pETSPortEntry == NULL)
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : ETSUtlGetPortEntry() "
                       "Returns NULL. \r\n", __FUNCTION__);
        return SNMP_FAILURE;
    }

    if (((INT4) (u4LldpXdot1dcbxAdminETSConPriority) < DCBX_ZERO)
        || (u4LldpXdot1dcbxAdminETSConPriority >= DCBX_MAX_PRIORITIES))
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC,
                       "In %s : Priority is not in range(0-7)"
                       " \r\n", __FUNCTION__);
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFirstIndexLldpXdot1dcbxAdminETSConPriorityAssignmentTable
 Input       :  The Indices
                LldpV2LocPortIfIndex
                LldpXdot1dcbxAdminETSConPriority
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexLldpXdot1dcbxAdminETSConPriorityAssignmentTable (INT4
                                                                 *pi4LldpV2LocPortIfIndex,
                                                                 UINT4
                                                                 *pu4LldpXdot1dcbxAdminETSConPriority)
{
    tETSPortEntry      *pETSPortEntry = NULL;

    ETS_SHUTDOWN_CHECK;
    pETSPortEntry = (tETSPortEntry *)
        RBTreeGetFirst (gETSGlobalInfo.pRbETSPortTbl);
    if (pETSPortEntry == NULL)
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : RbTree GetFirst() "
                       "Returns NULL. \r\n", __FUNCTION__);
        return SNMP_FAILURE;
    }

    *pi4LldpV2LocPortIfIndex = (INT4) pETSPortEntry->u4IfIndex;
    *pu4LldpXdot1dcbxAdminETSConPriority = DCBX_ZERO;
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetNextIndexLldpXdot1dcbxAdminETSConPriorityAssignmentTable
 Input       :  The Indices
                LldpV2LocPortIfIndex
                nextLldpV2LocPortIfIndex
                LldpXdot1dcbxAdminETSConPriority
                nextLldpXdot1dcbxAdminETSConPriority
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexLldpXdot1dcbxAdminETSConPriorityAssignmentTable (INT4
                                                                i4LldpV2LocPortIfIndex,
                                                                INT4
                                                                *pi4NextLldpV2LocPortIfIndex,
                                                                UINT4
                                                                u4LldpXdot1dcbxAdminETSConPriority,
                                                                UINT4
                                                                *pu4NextLldpXdot1dcbxAdminETSConPriority)
{
    tETSPortEntry      *pETSPortEntry = NULL;
    tETSPortEntry       ETSPortEntry;

    ETS_SHUTDOWN_CHECK;

    MEMSET (&ETSPortEntry, DCBX_ZERO, sizeof (tETSPortEntry));
    ETSPortEntry.u4IfIndex = (UINT4) i4LldpV2LocPortIfIndex;
    pETSPortEntry = ETSUtlGetPortEntry ((UINT4) i4LldpV2LocPortIfIndex);
    if ((pETSPortEntry == NULL) ||
        (u4LldpXdot1dcbxAdminETSConPriority >= DCBX_MAX_VALID_PRI))
    {
        pETSPortEntry = (tETSPortEntry *)
            RBTreeGetNext (gETSGlobalInfo.pRbETSPortTbl,
                           &ETSPortEntry, DcbxUtlPortTblCmpFn);
        if (pETSPortEntry == NULL)
        {

            DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : RbTreeGetNExt() "
                           "Returns NULL. \r\n", __FUNCTION__);
            return SNMP_FAILURE;
        }
        *pi4NextLldpV2LocPortIfIndex = (INT4) pETSPortEntry->u4IfIndex;
        *pu4NextLldpXdot1dcbxAdminETSConPriority = DCBX_ZERO;
    }
    else
    {
        *pi4NextLldpV2LocPortIfIndex = i4LldpV2LocPortIfIndex;
        *pu4NextLldpXdot1dcbxAdminETSConPriority =
            u4LldpXdot1dcbxAdminETSConPriority + DCBX_MIN_VAL;
    }

    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetLldpXdot1dcbxAdminETSConPriTrafficClass
 Input       :  The Indices
                LldpV2LocPortIfIndex
                LldpXdot1dcbxAdminETSConPriority

                The Object 
                retValLldpXdot1dcbxAdminETSConPriTrafficClass
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetLldpXdot1dcbxAdminETSConPriTrafficClass (INT4 i4LldpV2LocPortIfIndex,
                                               UINT4
                                               u4LldpXdot1dcbxAdminETSConPriority,
                                               UINT4
                                               *pu4RetValLldpXdot1dcbxAdminETSConPriTrafficClass)
{
    /*nmhGetLldpXdot1dcbxAdminETSConTrafficClassGroup */
    tETSPortEntry      *pETSPortEntry = NULL;

    pETSPortEntry = ETSUtlGetPortEntry ((UINT4) i4LldpV2LocPortIfIndex);
    if (pETSPortEntry == NULL)
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : ETSUtlGetPortEntry ()"
                       "Returns NULL. \r\n", __FUNCTION__);
        return SNMP_FAILURE;
    }

    *pu4RetValLldpXdot1dcbxAdminETSConPriTrafficClass =
        (INT4) ((pETSPortEntry->ETSAdmPortInfo).
                au1ETSAdmTCGID[u4LldpXdot1dcbxAdminETSConPriority]);
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetLldpXdot1dcbxAdminETSConPriTrafficClass
 Input       :  The Indices
                LldpV2LocPortIfIndex
                LldpXdot1dcbxAdminETSConPriority

                The Object 
                setValLldpXdot1dcbxAdminETSConPriTrafficClass
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetLldpXdot1dcbxAdminETSConPriTrafficClass (INT4 i4LldpV2LocPortIfIndex,
                                               UINT4
                                               u4LldpXdot1dcbxAdminETSConPriority,
                                               UINT4
                                               u4SetValLldpXdot1dcbxAdminETSConPriTrafficClass)
{
    /*nmhSetLldpXdot1dcbxAdminETSConTrafficClassGroup */
    tETSPortEntry      *pETSPortEntry = NULL;
    UINT1               u1OperVersion = DCBX_VER_UNKNOWN;

    DCBX_TRC_ARG4 (DCBX_MGMT_TRC, "%s, Args: "
                   "i4LldpV2LocPortIfIndex: %d "
                   "u4LldpXdot1dcbxAdminETSConPriority: %d "
                   "u4SetValLldpXdot1dcbxAdminETSConPriTrafficClass: %d\r\n",
                   __func__, i4LldpV2LocPortIfIndex,
                   u4LldpXdot1dcbxAdminETSConPriority,
                   u4SetValLldpXdot1dcbxAdminETSConPriTrafficClass);

    pETSPortEntry = ETSUtlGetPortEntry ((UINT4) i4LldpV2LocPortIfIndex);
    if (pETSPortEntry == NULL)
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : ETSUtlGetPortEntry "
                       "Returns NULL. \r\n", __FUNCTION__);
        return SNMP_FAILURE;
    }
    DcbxUtlGetOperVersion ((UINT4) i4LldpV2LocPortIfIndex, &u1OperVersion);
    if ((pETSPortEntry->ETSAdmPortInfo).
        au1ETSAdmTCGID[u4LldpXdot1dcbxAdminETSConPriority] !=
        (UINT1) u4SetValLldpXdot1dcbxAdminETSConPriTrafficClass)
    {
        (pETSPortEntry->ETSAdmPortInfo).
            au1ETSAdmTCGID[u4LldpXdot1dcbxAdminETSConPriority] =
            (UINT1) u4SetValLldpXdot1dcbxAdminETSConPriTrafficClass;
        if (u1OperVersion == DCBX_VER_CEE)
        {
            ETSUtlAdminParamChange (pETSPortEntry, ETS_CEE_TLV_SUB_TYPE);
        }
        else
        {
            ETSUtlAdminParamChange (pETSPortEntry, ETS_CONF_TLV_SUB_TYPE);
            ETSUtlAdminParamChange (pETSPortEntry, ETS_RECO_TLV_SUB_TYPE);
        }
    }

    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2LldpXdot1dcbxAdminETSConPriTrafficClass
 Input       :  The Indices
                LldpV2LocPortIfIndex
                LldpXdot1dcbxAdminETSConPriority

                The Object 
                testValLldpXdot1dcbxAdminETSConPriTrafficClass
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2LldpXdot1dcbxAdminETSConPriTrafficClass (UINT4 *pu4ErrorCode,
                                                  INT4 i4LldpV2LocPortIfIndex,
                                                  UINT4
                                                  u4LldpXdot1dcbxAdminETSConPriority,
                                                  UINT4
                                                  u4TestValLldpXdot1dcbxAdminETSConPriTrafficClass)
{
    /* nmhTestv2LldpXdot1dcbxAdminETSConTrafficClassGroup */
    tETSPortEntry      *pETSPortEntry = NULL;

    ETS_SHUTDOWN_CHECK_TEST;
    if (DcbxUtlValidatePort ((UINT4) i4LldpV2LocPortIfIndex) == OSIX_FAILURE)
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : DcbxUtlValidatePort() "
                       "Returns Failure. \r\n", __FUNCTION__);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_ERR_DCBX_NO_PORT);
        return SNMP_FAILURE;
    }

    pETSPortEntry = ETSUtlGetPortEntry ((UINT4) i4LldpV2LocPortIfIndex);
    if (pETSPortEntry == NULL)
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : ETSUtlGetPortEntry() "
                       "Returns NULL. \r\n", __FUNCTION__);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_ERR_ETS_NO_PORT_ENTRY);
        return SNMP_FAILURE;
    }

    if (((INT4) (u4LldpXdot1dcbxAdminETSConPriority) < DCBX_ZERO)
        || (u4LldpXdot1dcbxAdminETSConPriority >= DCBX_MAX_PRIORITIES))
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : Priority is not in range "
                       "(0-7) \r\n", __FUNCTION__);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_ERR_ETS_INVALID_PRIORITY);
        return SNMP_FAILURE;
    }
    if (((INT4) u4TestValLldpXdot1dcbxAdminETSConPriTrafficClass <
         DCBX_ZERO)
        ||
        ((u4TestValLldpXdot1dcbxAdminETSConPriTrafficClass >=
          ETS_MAX_TCGID_CONF)
         && (u4TestValLldpXdot1dcbxAdminETSConPriTrafficClass !=
             ETS_DEF_TCGID)))

    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : TCGID is not in range "
                       "(0-7) \r\n", __FUNCTION__);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_ERR_ETS_INVALID_TCGID);
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2LldpXdot1dcbxAdminETSConPriorityAssignmentTable
 Input       :  The Indices
                LldpV2LocPortIfIndex
                LldpXdot1dcbxAdminETSConPriority
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2LldpXdot1dcbxAdminETSConPriorityAssignmentTable (UINT4 *pu4ErrorCode,
                                                         tSnmpIndexList *
                                                         pSnmpIndexList,
                                                         tSNMP_VAR_BIND *
                                                         pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : LldpXdot1dcbxAdminETSConTrafficClassBandwidthTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceLldpXdot1dcbxAdminETSConTrafficClassBandwidthTable
 Input       :  The Indices
                LldpV2LocPortIfIndex
                LldpXdot1dcbxAdminETSConTrafficClass
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceLldpXdot1dcbxAdminETSConTrafficClassBandwidthTable (INT4
                                                                            i4LldpV2LocPortIfIndex,
                                                                            UINT4
                                                                            u4LldpXdot1dcbxAdminETSConTrafficClass)
{
    tETSPortEntry      *pETSPortEntry = NULL;

    ETS_SHUTDOWN_CHECK;

    pETSPortEntry = ETSUtlGetPortEntry ((UINT4) i4LldpV2LocPortIfIndex);
    if (pETSPortEntry == NULL)
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : ETSUtlGetPortEntry() "
                       "Returns NULL. \r\n", __FUNCTION__);
        return SNMP_FAILURE;
    }
    if (((INT4) u4LldpXdot1dcbxAdminETSConTrafficClass <
         DCBX_ZERO)
        ||
        ((u4LldpXdot1dcbxAdminETSConTrafficClass >=
          ETS_MAX_TCGID_CONF)
         && ((u4LldpXdot1dcbxAdminETSConTrafficClass) != ETS_DEF_TCGID)))
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : TCGID is not in range "
                       "(0-7) \r\n", __FUNCTION__);
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFirstIndexLldpXdot1dcbxAdminETSConTrafficClassBandwidthTable
 Input       :  The Indices
                LldpV2LocPortIfIndex
                LldpXdot1dcbxAdminETSConTrafficClass
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexLldpXdot1dcbxAdminETSConTrafficClassBandwidthTable (INT4
                                                                    *pi4LldpV2LocPortIfIndex,
                                                                    UINT4
                                                                    *pu4LldpXdot1dcbxAdminETSConTrafficClass)
{
    tETSPortEntry      *pETSPortEntry = NULL;

    ETS_SHUTDOWN_CHECK;

    pETSPortEntry = (tETSPortEntry *)
        RBTreeGetFirst (gETSGlobalInfo.pRbETSPortTbl);
    if (pETSPortEntry == NULL)
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : ETSUtlGetPortEntry() "
                       "Returns NULL. \r\n", __FUNCTION__);
        return SNMP_FAILURE;
    }
    *pi4LldpV2LocPortIfIndex = (INT4) pETSPortEntry->u4IfIndex;
    *pu4LldpXdot1dcbxAdminETSConTrafficClass = DCBX_ZERO;
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetNextIndexLldpXdot1dcbxAdminETSConTrafficClassBandwidthTable
 Input       :  The Indices
                LldpV2LocPortIfIndex
                nextLldpV2LocPortIfIndex
                LldpXdot1dcbxAdminETSConTrafficClass
                nextLldpXdot1dcbxAdminETSConTrafficClass
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexLldpXdot1dcbxAdminETSConTrafficClassBandwidthTable (INT4
                                                                   i4LldpV2LocPortIfIndex,
                                                                   INT4
                                                                   *pi4NextLldpV2LocPortIfIndex,
                                                                   UINT4
                                                                   u4LldpXdot1dcbxAdminETSConTrafficClass,
                                                                   UINT4
                                                                   *pu4NextLldpXdot1dcbxAdminETSConTrafficClass)
{
    tETSPortEntry      *pETSPortEntry = NULL;
    tETSPortEntry       ETSPortEntry;

    ETS_SHUTDOWN_CHECK;

    MEMSET (&ETSPortEntry, DCBX_ZERO, sizeof (tETSPortEntry));
    ETSPortEntry.u4IfIndex = (UINT4) i4LldpV2LocPortIfIndex;
    pETSPortEntry = ETSUtlGetPortEntry ((UINT4) i4LldpV2LocPortIfIndex);

    if ((pETSPortEntry == NULL) ||
        (u4LldpXdot1dcbxAdminETSConTrafficClass >= ETS_TCGID7))
    {
        pETSPortEntry = (tETSPortEntry *)
            RBTreeGetNext (gETSGlobalInfo.pRbETSPortTbl,
                           &ETSPortEntry, DcbxUtlPortTblCmpFn);
        if (pETSPortEntry == NULL)
        {

            DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : RbTreeGetNExt() "
                           "Returns NULL. \r\n", __FUNCTION__);
            return SNMP_FAILURE;
        }
        *pi4NextLldpV2LocPortIfIndex = (INT4) pETSPortEntry->u4IfIndex;
        *pu4NextLldpXdot1dcbxAdminETSConTrafficClass = DCBX_ZERO;
    }
    else
    {
        *pi4NextLldpV2LocPortIfIndex = i4LldpV2LocPortIfIndex;
        *pu4NextLldpXdot1dcbxAdminETSConTrafficClass =
            u4LldpXdot1dcbxAdminETSConTrafficClass + DCBX_MIN_VAL;
    }
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetLldpXdot1dcbxAdminETSConTrafficClassBandwidth
 Input       :  The Indices
                LldpV2LocPortIfIndex
                LldpXdot1dcbxAdminETSConTrafficClass

                The Object 
                retValLldpXdot1dcbxAdminETSConTrafficClassBandwidth
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetLldpXdot1dcbxAdminETSConTrafficClassBandwidth (INT4
                                                     i4LldpV2LocPortIfIndex,
                                                     UINT4
                                                     u4LldpXdot1dcbxAdminETSConTrafficClass,
                                                     UINT4
                                                     *pu4RetValLldpXdot1dcbxAdminETSConTrafficClassBandwidth)
{
    tETSPortEntry      *pETSPortEntry = NULL;

    pETSPortEntry = ETSUtlGetPortEntry ((UINT4) i4LldpV2LocPortIfIndex);
    if (pETSPortEntry == NULL)
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : ETSUtlGetPortEntry ()"
                       "Returns NULL. \r\n", __FUNCTION__);
        return SNMP_FAILURE;
    }
    *pu4RetValLldpXdot1dcbxAdminETSConTrafficClassBandwidth =
        (INT4) ((pETSPortEntry->ETSAdmPortInfo).
                au1ETSAdmBW[u4LldpXdot1dcbxAdminETSConTrafficClass]);
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetLldpXdot1dcbxAdminETSConTrafficClassBandwidth
 Input       :  The Indices
                LldpV2LocPortIfIndex
                LldpXdot1dcbxAdminETSConTrafficClass

                The Object 
                setValLldpXdot1dcbxAdminETSConTrafficClassBandwidth
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetLldpXdot1dcbxAdminETSConTrafficClassBandwidth (INT4
                                                     i4LldpV2LocPortIfIndex,
                                                     UINT4
                                                     u4LldpXdot1dcbxAdminETSConTrafficClass,
                                                     UINT4
                                                     u4SetValLldpXdot1dcbxAdminETSConTrafficClassBandwidth)
{
    tETSPortEntry      *pETSPortEntry = NULL;
    UINT1               u1OperVersion = DCBX_VER_UNKNOWN;

    DCBX_TRC_ARG4 (DCBX_MGMT_TRC, "%s, Args: "
                   "i4LldpV2LocPortIfIndex: %d "
                   "u4LldpXdot1dcbxAdminETSConTrafficClass: %d "
                   "u4SetValLldpXdot1dcbxAdminETSConTrafficClassBandwidth: %d\r\n",
                   __func__, i4LldpV2LocPortIfIndex,
                   u4LldpXdot1dcbxAdminETSConTrafficClass,
                   u4SetValLldpXdot1dcbxAdminETSConTrafficClassBandwidth);

    pETSPortEntry = ETSUtlGetPortEntry ((UINT4) i4LldpV2LocPortIfIndex);
    if (pETSPortEntry == NULL)
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : ETSUtlGetPortEntry ()"
                       "Returns NULL. \r\n", __FUNCTION__);
        return SNMP_FAILURE;
    }
    DcbxUtlGetOperVersion ((UINT4) i4LldpV2LocPortIfIndex, &u1OperVersion);

    if ((pETSPortEntry->ETSAdmPortInfo).
        au1ETSAdmBW[u4LldpXdot1dcbxAdminETSConTrafficClass] !=
        (UINT1) u4SetValLldpXdot1dcbxAdminETSConTrafficClassBandwidth)
    {
        (pETSPortEntry->ETSAdmPortInfo).
            au1ETSAdmBW[u4LldpXdot1dcbxAdminETSConTrafficClass] =
            (UINT1) u4SetValLldpXdot1dcbxAdminETSConTrafficClassBandwidth;
        if (u1OperVersion == DCBX_VER_CEE)
        {
            ETSUtlAdminParamChange (pETSPortEntry, ETS_CEE_TLV_SUB_TYPE);
        }
        else
        {
            ETSUtlAdminParamChange (pETSPortEntry, ETS_CONF_TLV_SUB_TYPE);
        }
    }
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2LldpXdot1dcbxAdminETSConTrafficClassBandwidth
 Input       :  The Indices
                LldpV2LocPortIfIndex
                LldpXdot1dcbxAdminETSConTrafficClass

                The Object 
                testValLldpXdot1dcbxAdminETSConTrafficClassBandwidth
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2LldpXdot1dcbxAdminETSConTrafficClassBandwidth (UINT4 *pu4ErrorCode,
                                                        INT4
                                                        i4LldpV2LocPortIfIndex,
                                                        UINT4
                                                        u4LldpXdot1dcbxAdminETSConTrafficClass,
                                                        UINT4
                                                        u4TestValLldpXdot1dcbxAdminETSConTrafficClassBandwidth)
{
    tETSPortEntry      *pETSPortEntry = NULL;
    UINT4               u4BwSum =
        u4TestValLldpXdot1dcbxAdminETSConTrafficClassBandwidth;
    UINT1               u1Index = DCBX_ZERO;

    ETS_SHUTDOWN_CHECK_TEST;
    if (DcbxUtlValidatePort ((UINT4) i4LldpV2LocPortIfIndex) == OSIX_FAILURE)
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : DcbxUtlValidatePort() "
                       "Returns Failure. \r\n", __FUNCTION__);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_ERR_DCBX_NO_PORT);
        return SNMP_FAILURE;
    }

    pETSPortEntry = ETSUtlGetPortEntry ((UINT4) i4LldpV2LocPortIfIndex);
    if (pETSPortEntry == NULL)
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : ETSUtlGetPortEntry() "
                       "Returns NULL. \r\n", __FUNCTION__);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_ERR_ETS_NO_PORT_ENTRY);
        return SNMP_FAILURE;
    }
    if (((INT4) u4LldpXdot1dcbxAdminETSConTrafficClass <
         DCBX_ZERO)
        ||
        ((u4LldpXdot1dcbxAdminETSConTrafficClass >=
          ETS_MAX_TCGID_CONF)
         && ((u4LldpXdot1dcbxAdminETSConTrafficClass) != ETS_DEF_TCGID)))
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : TCGID is not in range "
                       "(0-7) \r\n", __FUNCTION__);
        return SNMP_FAILURE;
    }

    for (u1Index = DCBX_ZERO; u1Index < ETS_MAX_TCGID_CONF; u1Index++)
    {
        if (u1Index == u4LldpXdot1dcbxAdminETSConTrafficClass)
        {
            continue;
        }
        u4BwSum = u4BwSum + pETSPortEntry->ETSAdmPortInfo.au1ETSAdmBW[u1Index];
    }
    /* User is only allowed to configure the BW value whose total BW sum = 100 */
    if (u4BwSum > ETS_MAX_BW)
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : Sum of Bandwidth for all TCGID "
                       "exceeds 100 \r\n", __FUNCTION__);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_ERR_ETS_INVALID_BW_SUM);
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2LldpXdot1dcbxAdminETSConTrafficClassBandwidthTable
 Input       :  The Indices
                LldpV2LocPortIfIndex
                LldpXdot1dcbxAdminETSConTrafficClass
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2LldpXdot1dcbxAdminETSConTrafficClassBandwidthTable (UINT4 *pu4ErrorCode,
                                                            tSnmpIndexList *
                                                            pSnmpIndexList,
                                                            tSNMP_VAR_BIND *
                                                            pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : LldpXdot1dcbxAdminETSConTrafficSelectionAlgorithmTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceLldpXdot1dcbxAdminETSConTrafficSelectionAlgorithmTable
 Input       :  The Indices
                LldpV2LocPortIfIndex
                LldpXdot1dcbxAdminETSConTSATrafficClass
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1 
     
     
     
     
     
     
     
    nmhValidateIndexInstanceLldpXdot1dcbxAdminETSConTrafficSelectionAlgorithmTable
    (INT4 i4LldpV2LocPortIfIndex,
     UINT4 u4LldpXdot1dcbxAdminETSConTSATrafficClass)
{
    tETSPortEntry      *pETSPortEntry = NULL;

    if (DcbxUtlValidatePort ((UINT4) i4LldpV2LocPortIfIndex) == OSIX_FAILURE)
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : DcbxUtlValidatePort() "
                       "Returns Failure. \r\n", __FUNCTION__);
        CLI_SET_ERR (CLI_ERR_DCBX_NO_PORT);
        return SNMP_FAILURE;
    }

    ETS_SHUTDOWN_CHECK;
    pETSPortEntry = ETSUtlGetPortEntry ((UINT4) i4LldpV2LocPortIfIndex);
    if (pETSPortEntry == NULL)
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : ETSUtlGetPortEntry () "
                       " Returns FAILURE. \r\n", __FUNCTION__);
        CLI_SET_ERR (CLI_ERR_ETS_NO_PORT_ENTRY);
        return SNMP_FAILURE;
    }
    if (((INT4) u4LldpXdot1dcbxAdminETSConTSATrafficClass <
         DCBX_ZERO)
        ||
        ((u4LldpXdot1dcbxAdminETSConTSATrafficClass >=
          ETS_MAX_TCGID_CONF)
         && ((u4LldpXdot1dcbxAdminETSConTSATrafficClass) != ETS_DEF_TCGID)))
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : TCGID is not in range "
                       "(0-7) \r\n", __FUNCTION__);
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFirstIndexLldpXdot1dcbxAdminETSConTrafficSelectionAlgorithmTable
 Input       :  The Indices
                LldpV2LocPortIfIndex
                LldpXdot1dcbxAdminETSConTSATrafficClass
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexLldpXdot1dcbxAdminETSConTrafficSelectionAlgorithmTable (INT4
                                                                        *pi4LldpV2LocPortIfIndex,
                                                                        UINT4
                                                                        *pu4LldpXdot1dcbxAdminETSConTSATrafficClass)
{
    tETSPortEntry      *pETSPortEntry = NULL;

    ETS_SHUTDOWN_CHECK;

    pETSPortEntry = (tETSPortEntry *)
        RBTreeGetFirst (gETSGlobalInfo.pRbETSPortTbl);
    if (pETSPortEntry == NULL)
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : RbTreeGetFirst Node () "
                       "Returns NULL. \r\n", __FUNCTION__);
        return SNMP_FAILURE;
    }

    *pi4LldpV2LocPortIfIndex = (INT4) pETSPortEntry->u4IfIndex;
    *pu4LldpXdot1dcbxAdminETSConTSATrafficClass = DCBX_ZERO;
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetNextIndexLldpXdot1dcbxAdminETSConTrafficSelectionAlgorithmTable
 Input       :  The Indices
                LldpV2LocPortIfIndex
                nextLldpV2LocPortIfIndex
                LldpXdot1dcbxAdminETSConTSATrafficClass
                nextLldpXdot1dcbxAdminETSConTSATrafficClass
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexLldpXdot1dcbxAdminETSConTrafficSelectionAlgorithmTable (INT4
                                                                       i4LldpV2LocPortIfIndex,
                                                                       INT4
                                                                       *pi4NextLldpV2LocPortIfIndex,
                                                                       UINT4
                                                                       u4LldpXdot1dcbxAdminETSConTSATrafficClass,
                                                                       UINT4
                                                                       *pu4NextLldpXdot1dcbxAdminETSConTSATrafficClass)
{
    tETSPortEntry      *pETSPortEntry = NULL;
    tETSPortEntry       ETSPortEntry;

    ETS_SHUTDOWN_CHECK;

    MEMSET (&ETSPortEntry, DCBX_ZERO, sizeof (tETSPortEntry));
    ETSPortEntry.u4IfIndex = (UINT4) i4LldpV2LocPortIfIndex;
    pETSPortEntry = ETSUtlGetPortEntry ((UINT4) i4LldpV2LocPortIfIndex);
    if ((pETSPortEntry == NULL) ||
        (u4LldpXdot1dcbxAdminETSConTSATrafficClass >= ETS_TCGID7))
    {
        pETSPortEntry = (tETSPortEntry *)
            RBTreeGetNext (gETSGlobalInfo.pRbETSPortTbl,
                           &ETSPortEntry, DcbxUtlPortTblCmpFn);
        if (pETSPortEntry == NULL)
        {

            DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : RbTreeGetNExt() "
                           "Returns NULL. \r\n", __FUNCTION__);
            return SNMP_FAILURE;
        }
        *pi4NextLldpV2LocPortIfIndex = (INT4) pETSPortEntry->u4IfIndex;
        *pu4NextLldpXdot1dcbxAdminETSConTSATrafficClass = DCBX_ZERO;
    }
    else
    {
        *pi4NextLldpV2LocPortIfIndex = i4LldpV2LocPortIfIndex;
        *pu4NextLldpXdot1dcbxAdminETSConTSATrafficClass =
            u4LldpXdot1dcbxAdminETSConTSATrafficClass + DCBX_MIN_VAL;
    }
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetLldpXdot1dcbxAdminETSConTrafficSelectionAlgorithm
 Input       :  The Indices
                LldpV2LocPortIfIndex
                LldpXdot1dcbxAdminETSConTSATrafficClass

                The Object 
                retValLldpXdot1dcbxAdminETSConTrafficSelectionAlgorithm
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetLldpXdot1dcbxAdminETSConTrafficSelectionAlgorithm (INT4
                                                         i4LldpV2LocPortIfIndex,
                                                         UINT4
                                                         u4LldpXdot1dcbxAdminETSConTSATrafficClass,
                                                         INT4
                                                         *pi4RetValLldpXdot1dcbxAdminETSConTrafficSelectionAlgorithm)
{
    tETSPortEntry      *pETSPortEntry = NULL;

    pETSPortEntry = ETSUtlGetPortEntry ((UINT4) i4LldpV2LocPortIfIndex);
    if (pETSPortEntry == NULL)
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : ETSUtlGetPortEntry () "
                       " Returns FAILURE. \r\n", __FUNCTION__);
        return SNMP_FAILURE;
    }

    *pi4RetValLldpXdot1dcbxAdminETSConTrafficSelectionAlgorithm =
        pETSPortEntry->ETSAdmPortInfo.
        au1ETSAdmTsaTable[u4LldpXdot1dcbxAdminETSConTSATrafficClass];
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetLldpXdot1dcbxAdminETSConTrafficSelectionAlgorithm
 Input       :  The Indices
                LldpV2LocPortIfIndex
                LldpXdot1dcbxAdminETSConTSATrafficClass

                The Object 
                setValLldpXdot1dcbxAdminETSConTrafficSelectionAlgorithm
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetLldpXdot1dcbxAdminETSConTrafficSelectionAlgorithm (INT4
                                                         i4LldpV2LocPortIfIndex,
                                                         UINT4
                                                         u4LldpXdot1dcbxAdminETSConTSATrafficClass,
                                                         INT4
                                                         i4SetValLldpXdot1dcbxAdminETSConTrafficSelectionAlgorithm)
{
    tETSPortEntry      *pETSPortEntry = NULL;
    UINT1               u1OperVersion = DCBX_VER_UNKNOWN;

    DCBX_TRC_ARG4 (DCBX_MGMT_TRC, "%s, Args: "
                   "i4LldpV2LocPortIfIndex: %d "
                   "u4LldpXdot1dcbxAdminETSConTSATrafficClass: %d "
                   "i4SetValLldpXdot1dcbxAdminETSConTrafficSelectionAlgorithm: %d\r\n",
                   __func__, i4LldpV2LocPortIfIndex,
                   u4LldpXdot1dcbxAdminETSConTSATrafficClass,
                   i4SetValLldpXdot1dcbxAdminETSConTrafficSelectionAlgorithm);

    pETSPortEntry = ETSUtlGetPortEntry ((UINT4) i4LldpV2LocPortIfIndex);
    if (pETSPortEntry == NULL)
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : ETSUtlGetPortEntry () "
                       " Returns FAILURE. \r\n", __FUNCTION__);
        return SNMP_FAILURE;
    }
    DcbxUtlGetOperVersion ((UINT4) i4LldpV2LocPortIfIndex, &u1OperVersion);

    if (pETSPortEntry->ETSAdmPortInfo.
        au1ETSAdmTsaTable[u4LldpXdot1dcbxAdminETSConTSATrafficClass]
        != (UINT1) i4SetValLldpXdot1dcbxAdminETSConTrafficSelectionAlgorithm)
    {
        pETSPortEntry->ETSAdmPortInfo.
            au1ETSAdmTsaTable[u4LldpXdot1dcbxAdminETSConTSATrafficClass]
            = (UINT1) i4SetValLldpXdot1dcbxAdminETSConTrafficSelectionAlgorithm;

        if (u1OperVersion == DCBX_VER_CEE)
        {
            ETSUtlAdminParamChange (pETSPortEntry, ETS_CEE_TLV_SUB_TYPE);
        }
        else
        {
            ETSUtlAdminParamChange (pETSPortEntry, ETS_CONF_TLV_SUB_TYPE);
        }
    }

    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2LldpXdot1dcbxAdminETSConTrafficSelectionAlgorithm
 Input       :  The Indices
                LldpV2LocPortIfIndex
                LldpXdot1dcbxAdminETSConTSATrafficClass

                The Object 
                testValLldpXdot1dcbxAdminETSConTrafficSelectionAlgorithm
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2LldpXdot1dcbxAdminETSConTrafficSelectionAlgorithm (UINT4 *pu4ErrorCode,
                                                            INT4
                                                            i4LldpV2LocPortIfIndex,
                                                            UINT4
                                                            u4LldpXdot1dcbxAdminETSConTSATrafficClass,
                                                            INT4
                                                            i4TestValLldpXdot1dcbxAdminETSConTrafficSelectionAlgorithm)
{
    tETSPortEntry      *pETSPortEntry = NULL;

    ETS_SHUTDOWN_CHECK_TEST;
    if (DcbxUtlValidatePort ((UINT4) i4LldpV2LocPortIfIndex) == OSIX_FAILURE)
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : DcbxUtlValidatePort() "
                       "Returns Failure. \r\n", __FUNCTION__);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_ERR_DCBX_NO_PORT);
        return SNMP_FAILURE;
    }

    pETSPortEntry = ETSUtlGetPortEntry ((UINT4) i4LldpV2LocPortIfIndex);
    if (pETSPortEntry == NULL)
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : ETSUtlGetPortEntry() "
                       "Returns NULL. \r\n", __FUNCTION__);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_ERR_ETS_NO_PORT_ENTRY);
        return SNMP_FAILURE;
    }

    if (((INT4) u4LldpXdot1dcbxAdminETSConTSATrafficClass <
         DCBX_ZERO)
        ||
        ((u4LldpXdot1dcbxAdminETSConTSATrafficClass >=
          ETS_MAX_TCGID_CONF)
         && ((u4LldpXdot1dcbxAdminETSConTSATrafficClass) != ETS_DEF_TCGID)))
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : TCGID is not in range "
                       "(0-7) \r\n", __FUNCTION__);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_ERR_ETS_INVALID_TCGID);
        return SNMP_FAILURE;
    }
    switch (i4TestValLldpXdot1dcbxAdminETSConTrafficSelectionAlgorithm)
    {
        case DCBX_STRICT_PRIORITY_ALGO:
        case DCBX_CREDIT_BASED_SHAPER_ALGO:
        case DCBX_ENHANCED_TRANSMISSION_SELECTION_ALGO:
        case DCBX_VENDOR_SPECIFIC_ALGO:
            break;
        default:
            DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : TSA table entry doesn't "
                           "match with the valid values\r\n", __FUNCTION__);
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            CLI_SET_ERR (CLI_ERR_ETS_INVALID_TCGID);
            return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */
/****************************************************************************
 Function    :  nmhDepv2LldpXdot1dcbxAdminETSConTrafficSelectionAlgorithmTable
 Input       :  The Indices
                LldpV2LocPortIfIndex
                LldpXdot1dcbxAdminETSConTSATrafficClass
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2LldpXdot1dcbxAdminETSConTrafficSelectionAlgorithmTable (UINT4
                                                                *pu4ErrorCode,
                                                                tSnmpIndexList *
                                                                pSnmpIndexList,
                                                                tSNMP_VAR_BIND *
                                                                pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : LldpXdot1dcbxAdminETSRecoTrafficClassBandwidthTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceLldpXdot1dcbxAdminETSRecoTrafficClassBandwidthTable
 Input       :  The Indices
                LldpV2LocPortIfIndex
                LldpXdot1dcbxAdminETSRecoTrafficClass
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1 
     
     
     
     
     
     
     
    nmhValidateIndexInstanceLldpXdot1dcbxAdminETSRecoTrafficClassBandwidthTable
    (INT4 i4LldpV2LocPortIfIndex, UINT4 u4LldpXdot1dcbxAdminETSRecoTrafficClass)
{
    tETSPortEntry      *pETSPortEntry = NULL;

    ETS_SHUTDOWN_CHECK;

    pETSPortEntry = ETSUtlGetPortEntry ((UINT4) i4LldpV2LocPortIfIndex);
    if (pETSPortEntry == NULL)
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : ETSUtlGetPortEntry() "
                       "Returns NULL. \r\n", __FUNCTION__);
        return SNMP_FAILURE;
    }
    if (((INT4) u4LldpXdot1dcbxAdminETSRecoTrafficClass <
         DCBX_ZERO)
        ||
        ((u4LldpXdot1dcbxAdminETSRecoTrafficClass >=
          ETS_MAX_TCGID_CONF)
         && ((u4LldpXdot1dcbxAdminETSRecoTrafficClass) != ETS_DEF_TCGID)))
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : TCGID is not in range "
                       "(0-7) \r\n", __FUNCTION__);
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFirstIndexLldpXdot1dcbxAdminETSRecoTrafficClassBandwidthTable
 Input       :  The Indices
                LldpV2LocPortIfIndex
                LldpXdot1dcbxAdminETSRecoTrafficClass
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexLldpXdot1dcbxAdminETSRecoTrafficClassBandwidthTable (INT4
                                                                     *pi4LldpV2LocPortIfIndex,
                                                                     UINT4
                                                                     *pu4LldpXdot1dcbxAdminETSRecoTrafficClass)
{
    tETSPortEntry      *pETSPortEntry = NULL;

    ETS_SHUTDOWN_CHECK;

    pETSPortEntry = (tETSPortEntry *)
        RBTreeGetFirst (gETSGlobalInfo.pRbETSPortTbl);
    if (pETSPortEntry == NULL)
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : ETSUtlGetPortEntry() "
                       "Returns NULL. \r\n", __FUNCTION__);
        return SNMP_FAILURE;
    }

    *pi4LldpV2LocPortIfIndex = (INT4) pETSPortEntry->u4IfIndex;
    *pu4LldpXdot1dcbxAdminETSRecoTrafficClass = DCBX_ZERO;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexLldpXdot1dcbxAdminETSRecoTrafficClassBandwidthTable
 Input       :  The Indices
                LldpV2LocPortIfIndex
                nextLldpV2LocPortIfIndex
                LldpXdot1dcbxAdminETSRecoTrafficClass
                nextLldpXdot1dcbxAdminETSRecoTrafficClass
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexLldpXdot1dcbxAdminETSRecoTrafficClassBandwidthTable (INT4
                                                                    i4LldpV2LocPortIfIndex,
                                                                    INT4
                                                                    *pi4NextLldpV2LocPortIfIndex,
                                                                    UINT4
                                                                    u4LldpXdot1dcbxAdminETSRecoTrafficClass,
                                                                    UINT4
                                                                    *pu4NextLldpXdot1dcbxAdminETSRecoTrafficClass)
{
    tETSPortEntry      *pETSPortEntry = NULL;
    tETSPortEntry       ETSPortEntry;

    ETS_SHUTDOWN_CHECK;

    MEMSET (&ETSPortEntry, DCBX_ZERO, sizeof (tETSPortEntry));
    ETSPortEntry.u4IfIndex = (UINT4) i4LldpV2LocPortIfIndex;
    pETSPortEntry = ETSUtlGetPortEntry ((UINT4) i4LldpV2LocPortIfIndex);

    if ((pETSPortEntry == NULL) ||
        (u4LldpXdot1dcbxAdminETSRecoTrafficClass >= ETS_TCGID7))
    {
        pETSPortEntry = (tETSPortEntry *)
            RBTreeGetNext (gETSGlobalInfo.pRbETSPortTbl,
                           &ETSPortEntry, DcbxUtlPortTblCmpFn);
        if (pETSPortEntry == NULL)
        {

            DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : RbTreeGetNExt() "
                           "Returns NULL. \r\n", __FUNCTION__);
            return SNMP_FAILURE;
        }
        *pi4NextLldpV2LocPortIfIndex = (INT4) pETSPortEntry->u4IfIndex;
        *pu4NextLldpXdot1dcbxAdminETSRecoTrafficClass = DCBX_ZERO;
    }
    else
    {
        *pi4NextLldpV2LocPortIfIndex = i4LldpV2LocPortIfIndex;
        *pu4NextLldpXdot1dcbxAdminETSRecoTrafficClass =
            u4LldpXdot1dcbxAdminETSRecoTrafficClass + DCBX_MIN_VAL;
    }
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetLldpXdot1dcbxAdminETSRecoTrafficClassBandwidth
 Input       :  The Indices
                LldpV2LocPortIfIndex
                LldpXdot1dcbxAdminETSRecoTrafficClass

                The Object 
                retValLldpXdot1dcbxAdminETSRecoTrafficClassBandwidth
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetLldpXdot1dcbxAdminETSRecoTrafficClassBandwidth (INT4
                                                      i4LldpV2LocPortIfIndex,
                                                      UINT4
                                                      u4LldpXdot1dcbxAdminETSRecoTrafficClass,
                                                      UINT4
                                                      *pu4RetValLldpXdot1dcbxAdminETSRecoTrafficClassBandwidth)
{
    tETSPortEntry      *pETSPortEntry = NULL;

    pETSPortEntry = ETSUtlGetPortEntry ((UINT4) i4LldpV2LocPortIfIndex);
    if (pETSPortEntry == NULL)
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : ETSUtlGetPortEntry ()"
                       "Returns NULL. \r\n", __FUNCTION__);
        return SNMP_FAILURE;
    }
    *pu4RetValLldpXdot1dcbxAdminETSRecoTrafficClassBandwidth =
        (INT4) ((pETSPortEntry->ETSAdmPortInfo).
                au1ETSAdmRecoBW[u4LldpXdot1dcbxAdminETSRecoTrafficClass]);
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetLldpXdot1dcbxAdminETSRecoTrafficClassBandwidth
 Input       :  The Indices
                LldpV2LocPortIfIndex
                LldpXdot1dcbxAdminETSRecoTrafficClass

                The Object 
                setValLldpXdot1dcbxAdminETSRecoTrafficClassBandwidth
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetLldpXdot1dcbxAdminETSRecoTrafficClassBandwidth (INT4
                                                      i4LldpV2LocPortIfIndex,
                                                      UINT4
                                                      u4LldpXdot1dcbxAdminETSRecoTrafficClass,
                                                      UINT4
                                                      u4SetValLldpXdot1dcbxAdminETSRecoTrafficClassBandwidth)
{
    tETSPortEntry      *pETSPortEntry = NULL;
    UINT1               u1OperVersion = DCBX_VER_UNKNOWN;

    DCBX_TRC_ARG4 (DCBX_MGMT_TRC, "%s, Args: "
                   "i4LldpV2LocPortIfIndex: %d "
                   "u4LldpXdot1dcbxAdminETSRecoTrafficClass: %d "
                   "u4LldpXdot1dcbxAdminETSRecoTrafficClass: %d\r\n",
                   __func__, i4LldpV2LocPortIfIndex,
                   u4LldpXdot1dcbxAdminETSRecoTrafficClass,
                   u4LldpXdot1dcbxAdminETSRecoTrafficClass);

    pETSPortEntry = ETSUtlGetPortEntry ((UINT4) i4LldpV2LocPortIfIndex);
    if (pETSPortEntry == NULL)
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : ETSUtlGetPortEntry ()"
                       "Returns NULL. \r\n", __FUNCTION__);
        return SNMP_FAILURE;
    }
    DcbxUtlGetOperVersion ((UINT4) i4LldpV2LocPortIfIndex, &u1OperVersion);

    if ((pETSPortEntry->ETSAdmPortInfo).
        au1ETSAdmRecoBW[u4LldpXdot1dcbxAdminETSRecoTrafficClass] !=
        (UINT1) u4SetValLldpXdot1dcbxAdminETSRecoTrafficClassBandwidth)
    {
        (pETSPortEntry->ETSAdmPortInfo).
            au1ETSAdmRecoBW[u4LldpXdot1dcbxAdminETSRecoTrafficClass] =
            (UINT1) u4SetValLldpXdot1dcbxAdminETSRecoTrafficClassBandwidth;

        if (u1OperVersion == DCBX_VER_CEE)
        {
            ETSUtlAdminParamChange (pETSPortEntry, ETS_CEE_TLV_SUB_TYPE);
        }
        else
        {
            ETSUtlAdminParamChange (pETSPortEntry, ETS_RECO_TLV_SUB_TYPE);
        }
    }
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2LldpXdot1dcbxAdminETSRecoTrafficClassBandwidth
 Input       :  The Indices
                LldpV2LocPortIfIndex
                LldpXdot1dcbxAdminETSRecoTrafficClass

                The Object 
                testValLldpXdot1dcbxAdminETSRecoTrafficClassBandwidth
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2LldpXdot1dcbxAdminETSRecoTrafficClassBandwidth (UINT4 *pu4ErrorCode,
                                                         INT4
                                                         i4LldpV2LocPortIfIndex,
                                                         UINT4
                                                         u4LldpXdot1dcbxAdminETSRecoTrafficClass,
                                                         UINT4
                                                         u4TestValLldpXdot1dcbxAdminETSRecoTrafficClassBandwidth)
{
    tETSPortEntry      *pETSPortEntry = NULL;
    UINT4               u4BwSum =
        u4TestValLldpXdot1dcbxAdminETSRecoTrafficClassBandwidth;
    UINT1               u1Index = DCBX_ZERO;

    ETS_SHUTDOWN_CHECK_TEST;

    if (DcbxUtlValidatePort ((UINT4) i4LldpV2LocPortIfIndex) == OSIX_FAILURE)
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : DcbxUtlValidatePort() "
                       "Returns Failure. \r\n", __FUNCTION__);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_ERR_DCBX_NO_PORT);
        return SNMP_FAILURE;
    }

    pETSPortEntry = ETSUtlGetPortEntry ((UINT4) i4LldpV2LocPortIfIndex);
    if (pETSPortEntry == NULL)
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : ETSUtlGetPortEntry() "
                       "Returns NULL. \r\n", __FUNCTION__);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_ERR_ETS_NO_PORT_ENTRY);
        return SNMP_FAILURE;
    }
    if (((INT4) u4LldpXdot1dcbxAdminETSRecoTrafficClass <
         DCBX_ZERO)
        ||
        ((u4LldpXdot1dcbxAdminETSRecoTrafficClass >=
          ETS_MAX_TCGID_CONF)
         && ((u4LldpXdot1dcbxAdminETSRecoTrafficClass) != ETS_DEF_TCGID)))
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : TCGID is not in range "
                       "(0-7) \r\n", __FUNCTION__);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_ERR_ETS_INVALID_TCGID);

        return SNMP_FAILURE;
    }

    for (u1Index = DCBX_ZERO; u1Index < ETS_MAX_TCGID_CONF; u1Index++)
    {
        if (u1Index == u4LldpXdot1dcbxAdminETSRecoTrafficClass)
        {
            continue;
        }
        u4BwSum =
            u4BwSum + pETSPortEntry->ETSAdmPortInfo.au1ETSAdmRecoBW[u1Index];
    }
    /* User is only allowed to configure the BW value whose total BW sum = 100 */
    if (u4BwSum > ETS_MAX_BW)
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : Sum of Bandwidth for all TCGID "
                       "exceeds 100 \r\n", __FUNCTION__);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_ERR_ETS_INVALID_BW_SUM);
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2LldpXdot1dcbxAdminETSRecoTrafficClassBandwidthTable
 Input       :  The Indices
                LldpV2LocPortIfIndex
                LldpXdot1dcbxAdminETSRecoTrafficClass
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2LldpXdot1dcbxAdminETSRecoTrafficClassBandwidthTable (UINT4
                                                             *pu4ErrorCode,
                                                             tSnmpIndexList *
                                                             pSnmpIndexList,
                                                             tSNMP_VAR_BIND *
                                                             pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : LldpXdot1dcbxAdminETSRecoTrafficSelectionAlgorithmTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceLldpXdot1dcbxAdminETSRecoTrafficSelectionAlgorithmTable
 Input       :  The Indices
                LldpV2LocPortIfIndex
                LldpXdot1dcbxAdminETSRecoTSATrafficClass
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1 
     
     
     
     
     
     
     
    nmhValidateIndexInstanceLldpXdot1dcbxAdminETSRecoTrafficSelectionAlgorithmTable
    (INT4 i4LldpV2LocPortIfIndex,
     UINT4 u4LldpXdot1dcbxAdminETSRecoTSATrafficClass)
{
    tETSPortEntry      *pETSPortEntry = NULL;

    if (DcbxUtlValidatePort ((UINT4) i4LldpV2LocPortIfIndex) == OSIX_FAILURE)
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : DcbxUtlValidatePort() "
                       "Returns Failure. \r\n", __FUNCTION__);
        CLI_SET_ERR (CLI_ERR_DCBX_NO_PORT);
        return SNMP_FAILURE;
    }

    ETS_SHUTDOWN_CHECK;
    pETSPortEntry = ETSUtlGetPortEntry ((UINT4) i4LldpV2LocPortIfIndex);
    if (pETSPortEntry == NULL)
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : ETSUtlGetPortEntry () "
                       " Returns FAILURE. \r\n", __FUNCTION__);
        CLI_SET_ERR (CLI_ERR_ETS_NO_PORT_ENTRY);
        return SNMP_FAILURE;
    }

    if (((INT4) u4LldpXdot1dcbxAdminETSRecoTSATrafficClass <
         DCBX_ZERO)
        ||
        ((u4LldpXdot1dcbxAdminETSRecoTSATrafficClass >=
          ETS_MAX_TCGID_CONF)
         && ((u4LldpXdot1dcbxAdminETSRecoTSATrafficClass) != ETS_DEF_TCGID)))
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : TCGID is not in range "
                       "(0-7) \r\n", __FUNCTION__);
        CLI_SET_ERR (CLI_ERR_ETS_INVALID_TCGID);
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexLldpXdot1dcbxAdminETSRecoTrafficSelectionAlgorithmTable
 Input       :  The Indices
                LldpV2LocPortIfIndex
                LldpXdot1dcbxAdminETSRecoTSATrafficClass
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexLldpXdot1dcbxAdminETSRecoTrafficSelectionAlgorithmTable (INT4
                                                                         *pi4LldpV2LocPortIfIndex,
                                                                         UINT4
                                                                         *pu4LldpXdot1dcbxAdminETSRecoTSATrafficClass)
{
    tETSPortEntry      *pETSPortEntry = NULL;

    ETS_SHUTDOWN_CHECK;

    pETSPortEntry = (tETSPortEntry *)
        RBTreeGetFirst (gETSGlobalInfo.pRbETSPortTbl);
    if (pETSPortEntry == NULL)
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : RbTreeGetFirst Node () "
                       "Returns NULL. \r\n", __FUNCTION__);
        return SNMP_FAILURE;
    }

    *pi4LldpV2LocPortIfIndex = (INT4) pETSPortEntry->u4IfIndex;
    *pu4LldpXdot1dcbxAdminETSRecoTSATrafficClass = DCBX_ZERO;
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetNextIndexLldpXdot1dcbxAdminETSRecoTrafficSelectionAlgorithmTable
 Input       :  The Indices
                LldpV2LocPortIfIndex
                nextLldpV2LocPortIfIndex
                LldpXdot1dcbxAdminETSRecoTSATrafficClass
                nextLldpXdot1dcbxAdminETSRecoTSATrafficClass
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexLldpXdot1dcbxAdminETSRecoTrafficSelectionAlgorithmTable (INT4
                                                                        i4LldpV2LocPortIfIndex,
                                                                        INT4
                                                                        *pi4NextLldpV2LocPortIfIndex,
                                                                        UINT4
                                                                        u4LldpXdot1dcbxAdminETSRecoTSATrafficClass,
                                                                        UINT4
                                                                        *pu4NextLldpXdot1dcbxAdminETSRecoTSATrafficClass)
{
    tETSPortEntry      *pETSPortEntry = NULL;
    tETSPortEntry       ETSPortEntry;

    ETS_SHUTDOWN_CHECK;

    MEMSET (&ETSPortEntry, DCBX_ZERO, sizeof (tETSPortEntry));
    ETSPortEntry.u4IfIndex = (UINT4) i4LldpV2LocPortIfIndex;
    pETSPortEntry = ETSUtlGetPortEntry ((UINT4) i4LldpV2LocPortIfIndex);
    if ((pETSPortEntry == NULL) ||
        (u4LldpXdot1dcbxAdminETSRecoTSATrafficClass >= ETS_TCGID7))
    {
        pETSPortEntry = (tETSPortEntry *)
            RBTreeGetNext (gETSGlobalInfo.pRbETSPortTbl,
                           &ETSPortEntry, DcbxUtlPortTblCmpFn);
        if (pETSPortEntry == NULL)
        {

            DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : RbTreeGetNExt() "
                           "Returns NULL. \r\n", __FUNCTION__);
            return SNMP_FAILURE;
        }
        *pi4NextLldpV2LocPortIfIndex = (INT4) pETSPortEntry->u4IfIndex;
        *pu4NextLldpXdot1dcbxAdminETSRecoTSATrafficClass = DCBX_ZERO;
    }
    else
    {
        *pi4NextLldpV2LocPortIfIndex = i4LldpV2LocPortIfIndex;
        *pu4NextLldpXdot1dcbxAdminETSRecoTSATrafficClass =
            u4LldpXdot1dcbxAdminETSRecoTSATrafficClass + DCBX_MIN_VAL;
    }
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetLldpXdot1dcbxAdminETSRecoTrafficSelectionAlgorithm
 Input       :  The Indices
                LldpV2LocPortIfIndex
                LldpXdot1dcbxAdminETSRecoTSATrafficClass

                The Object 
                retValLldpXdot1dcbxAdminETSRecoTrafficSelectionAlgorithm
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetLldpXdot1dcbxAdminETSRecoTrafficSelectionAlgorithm (INT4
                                                          i4LldpV2LocPortIfIndex,
                                                          UINT4
                                                          u4LldpXdot1dcbxAdminETSRecoTSATrafficClass,
                                                          INT4
                                                          *pi4RetValLldpXdot1dcbxAdminETSRecoTrafficSelectionAlgorithm)
{
    tETSPortEntry      *pETSPortEntry = NULL;

    pETSPortEntry = ETSUtlGetPortEntry ((UINT4) i4LldpV2LocPortIfIndex);
    if (pETSPortEntry == NULL)
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : ETSUtlGetPortEntry () "
                       " Returns FAILURE. \r\n", __FUNCTION__);
        return SNMP_FAILURE;
    }

    *pi4RetValLldpXdot1dcbxAdminETSRecoTrafficSelectionAlgorithm =
        pETSPortEntry->ETSAdmPortInfo.
        au1ETSAdmRecoTsaTable[u4LldpXdot1dcbxAdminETSRecoTSATrafficClass];
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetLldpXdot1dcbxAdminETSRecoTrafficSelectionAlgorithm
 Input       :  The Indices
                LldpV2LocPortIfIndex
                LldpXdot1dcbxAdminETSRecoTSATrafficClass

                The Object 
                setValLldpXdot1dcbxAdminETSRecoTrafficSelectionAlgorithm
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetLldpXdot1dcbxAdminETSRecoTrafficSelectionAlgorithm (INT4
                                                          i4LldpV2LocPortIfIndex,
                                                          UINT4
                                                          u4LldpXdot1dcbxAdminETSRecoTSATrafficClass,
                                                          INT4
                                                          i4SetValLldpXdot1dcbxAdminETSRecoTrafficSelectionAlgorithm)
{
    tETSPortEntry      *pETSPortEntry = NULL;
    UINT1               u1OperVersion = DCBX_VER_UNKNOWN;

    DCBX_TRC_ARG4 (DCBX_MGMT_TRC, "%s, Args: "
                   "i4LldpV2LocPortIfIndex: %d "
                   "u4LldpXdot1dcbxAdminETSRecoTSATrafficClass: %d "
                   "i4SetValLldpXdot1dcbxAdminETSRecoTrafficSelectionAlgorithm: %d\r\n",
                   __func__, i4LldpV2LocPortIfIndex,
                   u4LldpXdot1dcbxAdminETSRecoTSATrafficClass,
                   i4SetValLldpXdot1dcbxAdminETSRecoTrafficSelectionAlgorithm);

    pETSPortEntry = ETSUtlGetPortEntry ((UINT4) i4LldpV2LocPortIfIndex);
    if (pETSPortEntry == NULL)
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : ETSUtlGetPortEntry () "
                       " Returns FAILURE. \r\n", __FUNCTION__);
        return SNMP_FAILURE;
    }
    DcbxUtlGetOperVersion ((UINT4) i4LldpV2LocPortIfIndex, &u1OperVersion);

    if (pETSPortEntry->ETSAdmPortInfo.
        au1ETSAdmRecoTsaTable[u4LldpXdot1dcbxAdminETSRecoTSATrafficClass]
        != (UINT1) i4SetValLldpXdot1dcbxAdminETSRecoTrafficSelectionAlgorithm)
    {
        pETSPortEntry->ETSAdmPortInfo.
            au1ETSAdmRecoTsaTable[u4LldpXdot1dcbxAdminETSRecoTSATrafficClass]
            =
            (UINT1) i4SetValLldpXdot1dcbxAdminETSRecoTrafficSelectionAlgorithm;

        if (u1OperVersion == DCBX_VER_CEE)
        {
            ETSUtlAdminParamChange (pETSPortEntry, ETS_CEE_TLV_SUB_TYPE);
        }
        else
        {
            ETSUtlAdminParamChange (pETSPortEntry, ETS_RECO_TLV_SUB_TYPE);
        }

    }

    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2LldpXdot1dcbxAdminETSRecoTrafficSelectionAlgorithm
 Input       :  The Indices
                LldpV2LocPortIfIndex
                LldpXdot1dcbxAdminETSRecoTSATrafficClass

                The Object 
                testValLldpXdot1dcbxAdminETSRecoTrafficSelectionAlgorithm
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2LldpXdot1dcbxAdminETSRecoTrafficSelectionAlgorithm (UINT4
                                                             *pu4ErrorCode,
                                                             INT4
                                                             i4LldpV2LocPortIfIndex,
                                                             UINT4
                                                             u4LldpXdot1dcbxAdminETSRecoTSATrafficClass,
                                                             INT4
                                                             i4TestValLldpXdot1dcbxAdminETSRecoTrafficSelectionAlgorithm)
{
    tETSPortEntry      *pETSPortEntry = NULL;

    ETS_SHUTDOWN_CHECK_TEST;
    if (DcbxUtlValidatePort ((UINT4) i4LldpV2LocPortIfIndex) == OSIX_FAILURE)
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : DcbxUtlValidatePort() "
                       "Returns Failure. \r\n", __FUNCTION__);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_ERR_DCBX_NO_PORT);
        return SNMP_FAILURE;
    }

    pETSPortEntry = ETSUtlGetPortEntry ((UINT4) i4LldpV2LocPortIfIndex);
    if (pETSPortEntry == NULL)
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : ETSUtlGetPortEntry() "
                       "Returns NULL. \r\n", __FUNCTION__);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_ERR_ETS_NO_PORT_ENTRY);
        return SNMP_FAILURE;
    }

    if (((INT4) u4LldpXdot1dcbxAdminETSRecoTSATrafficClass <
         DCBX_ZERO)
        ||
        ((u4LldpXdot1dcbxAdminETSRecoTSATrafficClass >=
          ETS_MAX_TCGID_CONF)
         && ((u4LldpXdot1dcbxAdminETSRecoTSATrafficClass) != ETS_DEF_TCGID)))
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : TCGID is not in range "
                       "(0-7) \r\n", __FUNCTION__);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_ERR_ETS_INVALID_TCGID);

        return SNMP_FAILURE;
    }

    switch (i4TestValLldpXdot1dcbxAdminETSRecoTrafficSelectionAlgorithm)
    {
        case DCBX_STRICT_PRIORITY_ALGO:
        case DCBX_CREDIT_BASED_SHAPER_ALGO:
        case DCBX_ENHANCED_TRANSMISSION_SELECTION_ALGO:
        case DCBX_VENDOR_SPECIFIC_ALGO:
            break;
        default:
            DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : TSA table entry doesn't "
                           "match with the valid values\r\n", __FUNCTION__);
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            CLI_SET_ERR (CLI_ERR_ETS_INVALID_TCGID);
            return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2LldpXdot1dcbxAdminETSRecoTrafficSelectionAlgorithmTable
 Input       :  The Indices
                LldpV2LocPortIfIndex
                LldpXdot1dcbxAdminETSRecoTSATrafficClass
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2LldpXdot1dcbxAdminETSRecoTrafficSelectionAlgorithmTable (UINT4
                                                                 *pu4ErrorCode,
                                                                 tSnmpIndexList
                                                                 *
                                                                 pSnmpIndexList,
                                                                 tSNMP_VAR_BIND
                                                                 * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : LldpXdot1dcbxAdminPFCBasicTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceLldpXdot1dcbxAdminPFCBasicTable
 Input       :  The Indices
                LldpV2LocPortIfIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceLldpXdot1dcbxAdminPFCBasicTable (INT4
                                                         i4LldpV2LocPortIfIndex)
{
    tPFCPortEntry      *pPFCPortEntry = NULL;

    PFC_SHUTDOWN_CHECK;

    pPFCPortEntry = PFCUtlGetPortEntry ((UINT4) i4LldpV2LocPortIfIndex);
    if (pPFCPortEntry == NULL)
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : PFCUtlGetPortEntry() "
                       "Returns NULL. \r\n", __FUNCTION__);
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFirstIndexLldpXdot1dcbxAdminPFCBasicTable
 Input       :  The Indices
                LldpV2LocPortIfIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexLldpXdot1dcbxAdminPFCBasicTable (INT4 *pi4LldpV2LocPortIfIndex)
{
    tPFCPortEntry      *pPFCPortEntry = NULL;

    PFC_SHUTDOWN_CHECK;

    pPFCPortEntry = (tPFCPortEntry *)
        RBTreeGetFirst (gPFCGlobalInfo.pRbPFCPortTbl);
    if (pPFCPortEntry == NULL)
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : RBtreeGetFirst() "
                       "Returns NULL. \r\n", __FUNCTION__);
        return SNMP_FAILURE;
    }

    *pi4LldpV2LocPortIfIndex = (INT4) pPFCPortEntry->u4IfIndex;
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetNextIndexLldpXdot1dcbxAdminPFCBasicTable
 Input       :  The Indices
                LldpV2LocPortIfIndex
                nextLldpV2LocPortIfIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexLldpXdot1dcbxAdminPFCBasicTable (INT4 i4LldpV2LocPortIfIndex,
                                                INT4
                                                *pi4NextLldpV2LocPortIfIndex)
{
    tPFCPortEntry      *pPFCPortEntry = NULL;
    tPFCPortEntry       PFCPortEntry;

    PFC_SHUTDOWN_CHECK;

    MEMSET (&PFCPortEntry, DCBX_ZERO, sizeof (tPFCPortEntry));
    PFCPortEntry.u4IfIndex = (UINT4) i4LldpV2LocPortIfIndex;
    pPFCPortEntry = (tPFCPortEntry *)
        RBTreeGetNext (gPFCGlobalInfo.pRbPFCPortTbl,
                       &PFCPortEntry, DcbxUtlPortTblCmpFn);
    if (pPFCPortEntry == NULL)
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : Rbtree GetNext Node () "
                       "Returns NULL. \r\n", __FUNCTION__);
        return SNMP_FAILURE;
    }

    *pi4NextLldpV2LocPortIfIndex = (INT4) pPFCPortEntry->u4IfIndex;
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetLldpXdot1dcbxAdminPFCWilling
 Input       :  The Indices
                LldpV2LocPortIfIndex

                The Object 
                retValLldpXdot1dcbxAdminPFCWilling
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetLldpXdot1dcbxAdminPFCWilling (INT4 i4LldpV2LocPortIfIndex,
                                    INT4 *pi4RetValLldpXdot1dcbxAdminPFCWilling)
{
    tPFCPortEntry      *pPFCPortEntry = NULL;

    pPFCPortEntry = PFCUtlGetPortEntry ((UINT4) i4LldpV2LocPortIfIndex);
    if (pPFCPortEntry == NULL)
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : ETSGetDataPortEntry () "
                       " Returns FAILURE. \r\n", __FUNCTION__);
        return SNMP_FAILURE;
    }

    *pi4RetValLldpXdot1dcbxAdminPFCWilling =
        pPFCPortEntry->PFCAdmPortInfo.u1PFCAdmWilling;
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetLldpXdot1dcbxAdminPFCMBC
 Input       :  The Indices
                LldpV2LocPortIfIndex

                The Object 
                retValLldpXdot1dcbxAdminPFCMBC
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetLldpXdot1dcbxAdminPFCMBC (INT4 i4LldpV2LocPortIfIndex,
                                INT4 *pi4RetValLldpXdot1dcbxAdminPFCMBC)
{
    tPFCPortEntry      *pPFCPortEntry = NULL;

    pPFCPortEntry = PFCUtlGetPortEntry ((UINT4) i4LldpV2LocPortIfIndex);
    if (pPFCPortEntry == NULL)
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : ETSGetDataPortEntry () "
                       " Returns FAILURE. \r\n", __FUNCTION__);
        return SNMP_FAILURE;
    }

    *pi4RetValLldpXdot1dcbxAdminPFCMBC =
        pPFCPortEntry->PFCAdmPortInfo.u1PFCAdmMBC;
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetLldpXdot1dcbxAdminPFCCap
 Input       :  The Indices
                LldpV2LocPortIfIndex

                The Object 
                retValLldpXdot1dcbxAdminPFCCap
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetLldpXdot1dcbxAdminPFCCap (INT4 i4LldpV2LocPortIfIndex,
                                UINT4 *pu4RetValLldpXdot1dcbxAdminPFCCap)
{
    tPFCPortEntry      *pPFCPortEntry = NULL;

    pPFCPortEntry = PFCUtlGetPortEntry ((UINT4) i4LldpV2LocPortIfIndex);
    if (pPFCPortEntry == NULL)
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : ETSGetDataPortEntry () "
                       " Returns FAILURE. \r\n", __FUNCTION__);
        return SNMP_FAILURE;
    }

    *pu4RetValLldpXdot1dcbxAdminPFCCap =
        pPFCPortEntry->PFCAdmPortInfo.u1PFCAdmCap;
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetLldpXdot1dcbxAdminPFCWilling
 Input       :  The Indices
                LldpV2LocPortIfIndex

                The Object 
                setValLldpXdot1dcbxAdminPFCWilling
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetLldpXdot1dcbxAdminPFCWilling (INT4 i4LldpV2LocPortIfIndex,
                                    INT4 i4SetValLldpXdot1dcbxAdminPFCWilling)
{
    tPFCPortEntry      *pPFCPortEntry = NULL;

    DCBX_TRC_ARG3 (DCBX_MGMT_TRC, "%s, Args: "
                   "i4LldpV2LocPortIfIndex: %d "
                   "i4SetValLldpXdot1dcbxAdminPFCWilling: %d\r\n",
                   __func__, i4LldpV2LocPortIfIndex,
                   i4SetValLldpXdot1dcbxAdminPFCWilling);

    pPFCPortEntry = PFCUtlGetPortEntry ((UINT4) i4LldpV2LocPortIfIndex);
    if (pPFCPortEntry == NULL)
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : PFCUtlGetPortEntry () "
                       " Returns FAILURE. \r\n", __FUNCTION__);
        return SNMP_FAILURE;
    }

    if (pPFCPortEntry->PFCAdmPortInfo.u1PFCAdmWilling !=
        (UINT1) i4SetValLldpXdot1dcbxAdminPFCWilling)
    {
        pPFCPortEntry->PFCAdmPortInfo.u1PFCAdmWilling =
            (UINT1) i4SetValLldpXdot1dcbxAdminPFCWilling;
        pPFCPortEntry->PFCLocPortInfo.u1PFCLocWilling =
            (UINT1) i4SetValLldpXdot1dcbxAdminPFCWilling;

        PFCUtlWillingChange (pPFCPortEntry, OSIX_TRUE);
    }
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2LldpXdot1dcbxAdminPFCWilling
 Input       :  The Indices
                LldpV2LocPortIfIndex

                The Object 
                testValLldpXdot1dcbxAdminPFCWilling
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2LldpXdot1dcbxAdminPFCWilling (UINT4 *pu4ErrorCode,
                                       INT4 i4LldpV2LocPortIfIndex,
                                       INT4
                                       i4TestValLldpXdot1dcbxAdminPFCWilling)
{
    tPFCPortEntry      *pPFCPortEntry = NULL;

    PFC_SHUTDOWN_CHECK_TEST;

    pPFCPortEntry = PFCUtlGetPortEntry ((UINT4) i4LldpV2LocPortIfIndex);
    if (pPFCPortEntry == NULL)
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : PPFCUtlGetPortEntry () "
                       " Returns FAILURE. \r\n", __FUNCTION__);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_ERR_PFC_NO_PORT_ENTRY);
        return SNMP_FAILURE;
    }

    if ((i4TestValLldpXdot1dcbxAdminPFCWilling != PFC_ENABLED)
        && (i4TestValLldpXdot1dcbxAdminPFCWilling != PFC_DISABLED))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_ERR_PFC_INVALID_WILLING_STATUS);
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2LldpXdot1dcbxAdminPFCBasicTable
 Input       :  The Indices
                LldpV2LocPortIfIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2LldpXdot1dcbxAdminPFCBasicTable (UINT4 *pu4ErrorCode,
                                         tSnmpIndexList * pSnmpIndexList,
                                         tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : LldpXdot1dcbxAdminPFCEnableTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceLldpXdot1dcbxAdminPFCEnableTable
 Input       :  The Indices
                LldpV2LocPortIfIndex
                LldpXdot1dcbxAdminPFCEnablePriority
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceLldpXdot1dcbxAdminPFCEnableTable (INT4
                                                          i4LldpV2LocPortIfIndex,
                                                          UINT4
                                                          u4LldpXdot1dcbxAdminPFCEnablePriority)
{
    tPFCPortEntry      *pPFCPortEntry = NULL;

    PFC_SHUTDOWN_CHECK;
    pPFCPortEntry = PFCUtlGetPortEntry ((UINT4) i4LldpV2LocPortIfIndex);
    if (pPFCPortEntry == NULL)
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : PFCUtlGetPortEntry() "
                       "Returns NULL. \r\n", __FUNCTION__);
        return SNMP_FAILURE;
    }

    if (((INT4) (u4LldpXdot1dcbxAdminPFCEnablePriority) <
         DCBX_ZERO)
        || (u4LldpXdot1dcbxAdminPFCEnablePriority >= DCBX_MAX_PRIORITIES))
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : Priority is not in range "
                       "(0-7) \r\n", __FUNCTION__);
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFirstIndexLldpXdot1dcbxAdminPFCEnableTable
 Input       :  The Indices
                LldpV2LocPortIfIndex
                LldpXdot1dcbxAdminPFCEnablePriority
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexLldpXdot1dcbxAdminPFCEnableTable (INT4 *pi4LldpV2LocPortIfIndex,
                                                  UINT4
                                                  *pu4LldpXdot1dcbxAdminPFCEnablePriority)
{
    tPFCPortEntry      *pPFCPortEntry = NULL;

    PFC_SHUTDOWN_CHECK;
    pPFCPortEntry = (tPFCPortEntry *)
        RBTreeGetFirst (gPFCGlobalInfo.pRbPFCPortTbl);
    if (pPFCPortEntry == NULL)
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : RBtreeGetFirst() "
                       "Returns NULL. \r\n", __FUNCTION__);
        return SNMP_FAILURE;
    }

    *pi4LldpV2LocPortIfIndex = (INT4) pPFCPortEntry->u4IfIndex;
    *pu4LldpXdot1dcbxAdminPFCEnablePriority = DCBX_ZERO;
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetNextIndexLldpXdot1dcbxAdminPFCEnableTable
 Input       :  The Indices
                LldpV2LocPortIfIndex
                nextLldpV2LocPortIfIndex
                LldpXdot1dcbxAdminPFCEnablePriority
                nextLldpXdot1dcbxAdminPFCEnablePriority
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexLldpXdot1dcbxAdminPFCEnableTable (INT4 i4LldpV2LocPortIfIndex,
                                                 INT4
                                                 *pi4NextLldpV2LocPortIfIndex,
                                                 UINT4
                                                 u4LldpXdot1dcbxAdminPFCEnablePriority,
                                                 UINT4
                                                 *pu4NextLldpXdot1dcbxAdminPFCEnablePriority)
{
    tPFCPortEntry      *pPFCPortEntry = NULL;
    tPFCPortEntry       PFCPortEntry;

    PFC_SHUTDOWN_CHECK;

    MEMSET (&PFCPortEntry, DCBX_ZERO, sizeof (tPFCPortEntry));
    PFCPortEntry.u4IfIndex = (UINT4) i4LldpV2LocPortIfIndex;
    pPFCPortEntry = PFCUtlGetPortEntry ((UINT4) i4LldpV2LocPortIfIndex);
    if ((pPFCPortEntry == NULL) ||
        (u4LldpXdot1dcbxAdminPFCEnablePriority >= DCBX_MAX_VALID_PRI))
    {
        pPFCPortEntry = (tPFCPortEntry *)
            RBTreeGetNext (gPFCGlobalInfo.pRbPFCPortTbl,
                           &PFCPortEntry, DcbxUtlPortTblCmpFn);
        if (pPFCPortEntry == NULL)
        {
            DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : RBtreeGetNExt() "
                           "Returns NULL. \r\n", __FUNCTION__);
            return SNMP_FAILURE;
        }
        *pi4NextLldpV2LocPortIfIndex = (INT4) pPFCPortEntry->u4IfIndex;
        *pu4NextLldpXdot1dcbxAdminPFCEnablePriority = DCBX_ZERO;
    }
    else
    {
        *pi4NextLldpV2LocPortIfIndex = i4LldpV2LocPortIfIndex;
        *pu4NextLldpXdot1dcbxAdminPFCEnablePriority =
            u4LldpXdot1dcbxAdminPFCEnablePriority + DCBX_MIN_VAL;
    }

    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetLldpXdot1dcbxAdminPFCEnableEnabled
 Input       :  The Indices
                LldpV2LocPortIfIndex
                LldpXdot1dcbxAdminPFCEnablePriority

                The Object 
                retValLldpXdot1dcbxAdminPFCEnableEnabled
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetLldpXdot1dcbxAdminPFCEnableEnabled (INT4 i4LldpV2LocPortIfIndex,
                                          UINT4
                                          u4LldpXdot1dcbxAdminPFCEnablePriority,
                                          INT4
                                          *pi4RetValLldpXdot1dcbxAdminPFCEnableEnabled)
{
    tPFCPortEntry      *pPFCPortEntry = NULL;
    UINT1              *pu1PfcStatus = NULL;
    UINT1               u1PfcValue = DCBX_ZERO;

    pPFCPortEntry = PFCUtlGetPortEntry ((UINT4) i4LldpV2LocPortIfIndex);
    if (pPFCPortEntry == NULL)
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : ETSUtlGetPortEntry() "
                       "Returns NULL. \r\n", __FUNCTION__);
        return SNMP_FAILURE;
    }

    pu1PfcStatus = &((pPFCPortEntry->PFCAdmPortInfo).u1PFCAdmStatus);

    u1PfcValue = *pu1PfcStatus;
    u1PfcValue = (UINT1) (u1PfcValue >> u4LldpXdot1dcbxAdminPFCEnablePriority);
    u1PfcValue = u1PfcValue & 0x01;

    if (u1PfcValue == DCBX_ZERO)
    {
        *pi4RetValLldpXdot1dcbxAdminPFCEnableEnabled = PFC_DISABLED;
    }
    else
    {
        *pi4RetValLldpXdot1dcbxAdminPFCEnableEnabled = PFC_ENABLED;
    }
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetLldpXdot1dcbxAdminPFCEnableEnabled
 Input       :  The Indices
                LldpV2LocPortIfIndex
                LldpXdot1dcbxAdminPFCEnablePriority

                The Object 
                setValLldpXdot1dcbxAdminPFCEnableEnabled
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetLldpXdot1dcbxAdminPFCEnableEnabled (INT4 i4LldpV2LocPortIfIndex,
                                          UINT4
                                          u4LldpXdot1dcbxAdminPFCEnablePriority,
                                          INT4
                                          i4SetValLldpXdot1dcbxAdminPFCEnableEnabled)
{
    tPFCPortEntry      *pPFCPortEntry = NULL;
    UINT1               u1NewPfcProfile = DCBX_ZERO;
    UINT1               u1NewPfc = DCBX_MIN_VAL;

    DCBX_TRC_ARG4 (DCBX_MGMT_TRC, "%s, Args: "
                   "i4LldpV2LocPortIfIndex: %d "
                   "u4LldpXdot1dcbxAdminPFCEnablePriority: %d "
                   "i4SetValLldpXdot1dcbxAdminPFCEnableEnabled: %d\r\n",
                   __func__, i4LldpV2LocPortIfIndex,
                   u4LldpXdot1dcbxAdminPFCEnablePriority,
                   i4SetValLldpXdot1dcbxAdminPFCEnableEnabled);

    pPFCPortEntry = PFCUtlGetPortEntry ((UINT4) i4LldpV2LocPortIfIndex);
    if (pPFCPortEntry == NULL)
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : ETSUtlGetPortEntry() "
                       "Returns NULL. \r\n", __FUNCTION__);
        return SNMP_FAILURE;
    }

    u1NewPfcProfile = pPFCPortEntry->PFCAdmPortInfo.u1PFCAdmStatus;
    u1NewPfc = (UINT1) (u1NewPfc << u4LldpXdot1dcbxAdminPFCEnablePriority);

    if (i4SetValLldpXdot1dcbxAdminPFCEnableEnabled == DCBX_ENABLED)
    {
        if ((u1NewPfcProfile & u1NewPfc) == u1NewPfc)
        {
            return SNMP_SUCCESS;
        }
        u1NewPfcProfile = u1NewPfcProfile | u1NewPfc;
    }
    else
    {
        if ((u1NewPfcProfile & u1NewPfc) == u1NewPfc)
        {
            u1NewPfc = (UINT1) (~u1NewPfc);
            u1NewPfcProfile = u1NewPfcProfile & u1NewPfc;
        }
        else
        {
            return SNMP_SUCCESS;
        }
    }

    pPFCPortEntry->PFCAdmPortInfo.u1PFCAdmStatus = u1NewPfcProfile;

    if (PFCUtlAdminParamChange (pPFCPortEntry) == OSIX_FAILURE)
    {
        pPFCPortEntry->PFCAdmPortInfo.u1PFCAdmStatus =
            pPFCPortEntry->PFCLocPortInfo.u1PFCLocStatus;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2LldpXdot1dcbxAdminPFCEnableEnabled
 Input       :  The Indices
                LldpV2LocPortIfIndex
                LldpXdot1dcbxAdminPFCEnablePriority

                The Object 
                testValLldpXdot1dcbxAdminPFCEnableEnabled
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2LldpXdot1dcbxAdminPFCEnableEnabled (UINT4 *pu4ErrorCode,
                                             INT4 i4LldpV2LocPortIfIndex,
                                             UINT4
                                             u4LldpXdot1dcbxAdminPFCEnablePriority,
                                             INT4
                                             i4TestValLldpXdot1dcbxAdminPFCEnableEnabled)
{
    tPFCPortEntry      *pPFCPortEntry = NULL;

    if (DcbxUtlValidatePort ((UINT4) i4LldpV2LocPortIfIndex) == OSIX_FAILURE)
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : DcbxUtlValidatePort() "
                       "Returns Failure. \r\n", __FUNCTION__);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_ERR_DCBX_NO_PORT);
        return SNMP_FAILURE;
    }
    PFC_SHUTDOWN_CHECK_TEST;
    pPFCPortEntry = PFCUtlGetPortEntry ((UINT4) i4LldpV2LocPortIfIndex);
    if (pPFCPortEntry == NULL)
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : PFCUtlGetPortEntry() "
                       "Returns NULL. \r\n", __FUNCTION__);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_ERR_PFC_NO_PORT_ENTRY);
        return SNMP_FAILURE;
    }

    if (((INT4) (u4LldpXdot1dcbxAdminPFCEnablePriority) <
         DCBX_ZERO)
        || (u4LldpXdot1dcbxAdminPFCEnablePriority >= DCBX_MAX_PRIORITIES))
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : Priority is not in range "
                       "(0-7) \r\n", __FUNCTION__);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_ERR_PFC_INVALID_PRIORITY);
        return SNMP_FAILURE;
    }

    if ((i4TestValLldpXdot1dcbxAdminPFCEnableEnabled != DCBX_ENABLED)
        && (i4TestValLldpXdot1dcbxAdminPFCEnableEnabled != DCBX_DISABLED))
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : Status value is wrong "
                       "Enabled/Disabled is supported \r\n", __FUNCTION__);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_ERR_PFC_INVALID_ENABLED_STATUS);
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2LldpXdot1dcbxAdminPFCEnableTable
 Input       :  The Indices
                LldpV2LocPortIfIndex
                LldpXdot1dcbxAdminPFCEnablePriority
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2LldpXdot1dcbxAdminPFCEnableTable (UINT4 *pu4ErrorCode,
                                          tSnmpIndexList * pSnmpIndexList,
                                          tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : LldpXdot1dcbxAdminApplicationPriorityAppTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceLldpXdot1dcbxAdminApplicationPriorityAppTable
 Input       :  The Indices
                LldpV2LocPortIfIndex
                LldpXdot1dcbxAdminApplicationPriorityAESelector
                LldpXdot1dcbxAdminApplicationPriorityAEProtocol
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceLldpXdot1dcbxAdminApplicationPriorityAppTable (INT4
                                                                       i4LldpV2LocPortIfIndex,
                                                                       INT4
                                                                       i4LldpXdot1dcbxAdminApplicationPriorityAESelector,
                                                                       UINT4
                                                                       u4LldpXdot1dcbxAdminApplicationPriorityAEProtocol)
{
    tAppPriPortEntry   *pAppPortEntry = NULL;

    APP_PRI_SHUTDOWN_CHECK;

    pAppPortEntry = AppPriUtlGetPortEntry ((UINT4) i4LldpV2LocPortIfIndex);
    if (pAppPortEntry == NULL)
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : AppPriUtlGetPortEntry() "
                       "Returns NULL. \r\n", __FUNCTION__);
        return SNMP_FAILURE;
    }
    if (((INT4) u4LldpXdot1dcbxAdminApplicationPriorityAEProtocol < DCBX_ZERO)
        || (u4LldpXdot1dcbxAdminApplicationPriorityAEProtocol >
            APP_PRI_MAX_PROTOCOL_ID))
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : "
                       "i4LldpXdot1dcbxLocApplicationPriorityAEProtocol "
                       "exceeded allowed range. \r\n", __FUNCTION__);
        return SNMP_FAILURE;
    }
    if ((i4LldpXdot1dcbxAdminApplicationPriorityAESelector < DCBX_ONE) ||
        (i4LldpXdot1dcbxAdminApplicationPriorityAESelector >
         APP_PRI_MAX_SELECTOR))
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : "
                       "i4LldpXdot1dcbxLocApplicationPriorityAESelector "
                       "exceeded allowed range. \r\n", __FUNCTION__);
        return SNMP_FAILURE;
    }

    if (TMO_SLL_Count (&(APP_PRI_ADM_TBL (pAppPortEntry,
                                          i4LldpXdot1dcbxAdminApplicationPriorityAESelector
                                          - 1))) == DCBX_ZERO)
    {
        DCBX_TRC_ARG3 (DCBX_MGMT_TRC, "In %s : AppPriMappingTable "
                       "is NULL for port %d selector %d \r\n", __FUNCTION__,
                       i4LldpV2LocPortIfIndex,
                       i4LldpXdot1dcbxAdminApplicationPriorityAESelector);
        return SNMP_FAILURE;
    }

    if (AppPriUtlGetAppPriMappingEntry ((UINT4) i4LldpV2LocPortIfIndex,
                                        i4LldpXdot1dcbxAdminApplicationPriorityAESelector,
                                        (INT4)
                                        u4LldpXdot1dcbxAdminApplicationPriorityAEProtocol,
                                        APP_PRI_ADMIN) == NULL)
    {
        DCBX_TRC_ARG4 (DCBX_MGMT_TRC, "In %s : AppPriMappingEntry "
                       "is NULL for port %d selector %d Application %d\r\n",
                       __FUNCTION__,
                       i4LldpV2LocPortIfIndex,
                       i4LldpXdot1dcbxAdminApplicationPriorityAESelector,
                       u4LldpXdot1dcbxAdminApplicationPriorityAEProtocol);
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexLldpXdot1dcbxAdminApplicationPriorityAppTable
 Input       :  The Indices
                LldpV2LocPortIfIndex
                LldpXdot1dcbxAdminApplicationPriorityAESelector
                LldpXdot1dcbxAdminApplicationPriorityAEProtocol
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexLldpXdot1dcbxAdminApplicationPriorityAppTable (INT4
                                                               *pi4LldpV2LocPortIfIndex,
                                                               INT4
                                                               *pi4LldpXdot1dcbxAdminApplicationPriorityAESelector,
                                                               UINT4
                                                               *pu4LldpXdot1dcbxAdminApplicationPriorityAEProtocol)
{
    return (nmhGetNextIndexLldpXdot1dcbxAdminApplicationPriorityAppTable
            (DCBX_ZERO, pi4LldpV2LocPortIfIndex, DCBX_ONE,
             pi4LldpXdot1dcbxAdminApplicationPriorityAESelector, DCBX_ZERO,
             pu4LldpXdot1dcbxAdminApplicationPriorityAEProtocol));

}

/****************************************************************************
 Function    :  nmhGetNextIndexLldpXdot1dcbxAdminApplicationPriorityAppTable
 Input       :  The Indices
                LldpV2LocPortIfIndex
                nextLldpV2LocPortIfIndex
                LldpXdot1dcbxAdminApplicationPriorityAESelector
                nextLldpXdot1dcbxAdminApplicationPriorityAESelector
                LldpXdot1dcbxAdminApplicationPriorityAEProtocol
                nextLldpXdot1dcbxAdminApplicationPriorityAEProtocol
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexLldpXdot1dcbxAdminApplicationPriorityAppTable (INT4
                                                              i4LldpV2LocPortIfIndex,
                                                              INT4
                                                              *pi4NextLldpV2LocPortIfIndex,
                                                              INT4
                                                              i4LldpXdot1dcbxAdminApplicationPriorityAESelector,
                                                              INT4
                                                              *pi4NextLldpXdot1dcbxAdminApplicationPriorityAESelector,
                                                              UINT4
                                                              u4LldpXdot1dcbxAdminApplicationPriorityAEProtocol,
                                                              UINT4
                                                              *pu4NextLldpXdot1dcbxAdminApplicationPriorityAEProtocol)
{

    tAppPriPortEntry   *pAppPortEntry = NULL;
    tAppPriPortEntry    AppPortEntry;
    tAppPriMappingEntry *pAppPriMappingEntry = NULL;
    INT4                i4NextAPPortNumber = DCBX_ZERO;
    INT4                i4APPortNumber = DCBX_ZERO;
    INT4                i4Selector = DCBX_ZERO;
    INT4                i4Protocol = DCBX_ZERO;

    APP_PRI_SHUTDOWN_CHECK;

    MEMSET (&AppPortEntry, DCBX_ZERO, sizeof (tAppPriPortEntry));

    i4NextAPPortNumber = i4LldpV2LocPortIfIndex;
    i4APPortNumber = i4NextAPPortNumber;
    pAppPortEntry = AppPriUtlGetPortEntry ((UINT4) i4APPortNumber);
    if (pAppPortEntry == NULL)
    {
        MEMSET (&AppPortEntry, DCBX_ZERO, sizeof (tAppPriPortEntry));
        AppPortEntry.u4IfIndex = (UINT4) i4APPortNumber;
        pAppPortEntry = NULL;
        pAppPortEntry = (tAppPriPortEntry *)
            RBTreeGetNext (gAppPriGlobalInfo.pRbAppPriPortTbl,
                           &AppPortEntry, AppPriUtlPortTblCmpFn);
    }
    while (pAppPortEntry != NULL)
    {
        i4APPortNumber = (INT4) (pAppPortEntry->u4IfIndex);

        if (pAppPortEntry->u4IfIndex == (UINT4) i4LldpV2LocPortIfIndex)
        {
            i4Selector = i4LldpXdot1dcbxAdminApplicationPriorityAESelector;
        }
        else
        {
            i4Selector = DCBX_ONE;
        }
        for (; i4Selector <= APP_PRI_MAX_SELECTOR; i4Selector++)
        {

            if (TMO_SLL_Count
                (&(APP_PRI_ADM_TBL (pAppPortEntry, i4Selector - 1))) !=
                DCBX_ZERO)
            {
                if ((pAppPortEntry->u4IfIndex == (UINT4) i4LldpV2LocPortIfIndex)
                    && (i4Selector ==
                        i4LldpXdot1dcbxAdminApplicationPriorityAESelector))
                {
                    i4Protocol =
                        (INT4)
                        (u4LldpXdot1dcbxAdminApplicationPriorityAEProtocol +
                         DCBX_ONE);
                }
                else
                {
                    i4Protocol = DCBX_ZERO;
                }
                TMO_SLL_Scan (&
                              (APP_PRI_ADM_TBL (pAppPortEntry, i4Selector - 1)),
                              pAppPriMappingEntry, tAppPriMappingEntry *)
                {
                    if (pAppPriMappingEntry->i4AppPriProtocol >= i4Protocol)
                    {
                        *pi4NextLldpV2LocPortIfIndex = i4APPortNumber;
                        *pi4NextLldpXdot1dcbxAdminApplicationPriorityAESelector
                            = i4Selector;
                        *pu4NextLldpXdot1dcbxAdminApplicationPriorityAEProtocol
                            = (UINT4) pAppPriMappingEntry->i4AppPriProtocol;
                        return SNMP_SUCCESS;
                    }
                }
            }                    /*end of if TMO_SLL_Count */
        }                        /* end of for loop -selector */

        MEMSET (&AppPortEntry, DCBX_ZERO, sizeof (tAppPriPortEntry));
        AppPortEntry.u4IfIndex = pAppPortEntry->u4IfIndex;
        pAppPortEntry = NULL;
        pAppPortEntry = (tAppPriPortEntry *)
            RBTreeGetNext (gAppPriGlobalInfo.pRbAppPriPortTbl,
                           &AppPortEntry, AppPriUtlPortTblCmpFn);
    }
    return SNMP_FAILURE;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetLldpXdot1dcbxAdminApplicationPriorityAEPriority
 Input       :  The Indices
                LldpV2LocPortIfIndex
                LldpXdot1dcbxAdminApplicationPriorityAESelector
                LldpXdot1dcbxAdminApplicationPriorityAEProtocol

                The Object 
                retValLldpXdot1dcbxAdminApplicationPriorityAEPriority
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetLldpXdot1dcbxAdminApplicationPriorityAEPriority (INT4
                                                       i4LldpV2LocPortIfIndex,
                                                       INT4
                                                       i4LldpXdot1dcbxAdminApplicationPriorityAESelector,
                                                       UINT4
                                                       u4LldpXdot1dcbxAdminApplicationPriorityAEProtocol,
                                                       UINT4
                                                       *pu4RetValLldpXdot1dcbxAdminApplicationPriorityAEPriority)
{
    tAppPriMappingEntry *pAppPriMappingEntry = NULL;

    APP_PRI_SHUTDOWN_CHECK;
    pAppPriMappingEntry = AppPriUtlGetAppPriMappingEntry
        ((UINT4) i4LldpV2LocPortIfIndex,
         i4LldpXdot1dcbxAdminApplicationPriorityAESelector,
         (INT4) u4LldpXdot1dcbxAdminApplicationPriorityAEProtocol,
         APP_PRI_ADMIN);
    if (pAppPriMappingEntry == NULL)
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC,
                       "In %s : nmhGetLldpXdot1dcbxAdminApplicationPriorityAEPriority "
                       " AppPriUtlGetAppPriMappingEntry returned NULL. \r\n",
                       __FUNCTION__);

        return SNMP_FAILURE;
    }
    *pu4RetValLldpXdot1dcbxAdminApplicationPriorityAEPriority =
        pAppPriMappingEntry->u1AppPriPriority;
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetLldpXdot1dcbxAdminApplicationPriorityAEPriority
 Input       :  The Indices
                LldpV2LocPortIfIndex
                LldpXdot1dcbxAdminApplicationPriorityAESelector
                LldpXdot1dcbxAdminApplicationPriorityAEProtocol

                The Object 
                setValLldpXdot1dcbxAdminApplicationPriorityAEPriority
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetLldpXdot1dcbxAdminApplicationPriorityAEPriority (INT4
                                                       i4LldpV2LocPortIfIndex,
                                                       INT4
                                                       i4LldpXdot1dcbxAdminApplicationPriorityAESelector,
                                                       UINT4
                                                       u4LldpXdot1dcbxAdminApplicationPriorityAEProtocol,
                                                       UINT4
                                                       u4SetValLldpXdot1dcbxAdminApplicationPriorityAEPriority)
{
    tAppPriPortEntry   *pAppPortEntry = NULL;
    tAppPriMappingEntry *pAppPriMappingEntry = NULL;

    DCBX_TRC_ARG5 (DCBX_MGMT_TRC, "%s, Args: "
                   "i4LldpV2LocPortIfIndex: %d "
                   "i4LldpXdot1dcbxAdminApplicationPriorityAESelector: %d "
                   "u4LldpXdot1dcbxAdminApplicationPriorityAEProtocol: %d "
                   "u4SetValLldpXdot1dcbxAdminApplicationPriorityAEPriority: %d\r\n",
                   __func__, i4LldpV2LocPortIfIndex,
                   i4LldpXdot1dcbxAdminApplicationPriorityAESelector,
                   u4LldpXdot1dcbxAdminApplicationPriorityAEProtocol,
                   u4SetValLldpXdot1dcbxAdminApplicationPriorityAEPriority);

    pAppPortEntry = AppPriUtlGetPortEntry ((UINT4) i4LldpV2LocPortIfIndex);
    if (pAppPortEntry == NULL)
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : AppPriUtlGetPortEntry () "
                       " Returns FAILURE. \r\n", __FUNCTION__);
        return SNMP_FAILURE;
    }

    pAppPriMappingEntry = AppPriUtlGetAppPriMappingEntry
        ((UINT4) i4LldpV2LocPortIfIndex,
         i4LldpXdot1dcbxAdminApplicationPriorityAESelector,
         (INT4) u4LldpXdot1dcbxAdminApplicationPriorityAEProtocol,
         APP_PRI_ADMIN);
    if (pAppPriMappingEntry == NULL)
    {
        pAppPriMappingEntry = AppPriUtlCreateAppPriMappingTblEntry
            ((UINT4) i4LldpV2LocPortIfIndex,
             i4LldpXdot1dcbxAdminApplicationPriorityAESelector,
             (INT4) u4LldpXdot1dcbxAdminApplicationPriorityAEProtocol,
             APP_PRI_ADMIN);
        if (pAppPriMappingEntry == NULL)
        {
            DCBX_TRC_ARG1 (DCBX_MGMT_TRC,
                           "In %s : "
                           " AppPriUtlGetAppPriMappingEntry returned NULL. \r\n",
                           __FUNCTION__);

            return SNMP_FAILURE;
        }
    }
    else
    {
        if (pAppPriMappingEntry->u1AppPriPriority == (UINT1)
            u4SetValLldpXdot1dcbxAdminApplicationPriorityAEPriority)
        {

            DCBX_TRC_ARG3 (DCBX_MGMT_TRC,
                           "In %s : "
                           " pAppPriMappingEntry->u1AppPriPriority = %d"
                           " AdminApplicationPriorityAEPriority = %d.\r\n",
                           __FUNCTION__,
                           pAppPriMappingEntry->u1AppPriPriority,
                           u4SetValLldpXdot1dcbxAdminApplicationPriorityAEPriority);
            return SNMP_SUCCESS;
        }
        /* Delete application Priority mapping entry from hardware and recreate */
        if (AppPriUtlHwConfigAppPriMappingEntry
            ((UINT4) i4LldpV2LocPortIfIndex,
             i4LldpXdot1dcbxAdminApplicationPriorityAESelector,
             pAppPriMappingEntry, APP_PRI_HW_DELETE) == OSIX_FAILURE)
        {
            DCBX_TRC_ARG2 (DCBX_CRITICAL_TRC,
                           "In %s : AppPriUtlHwConfigAppPriMappingEntry "
                           "returned FAILURE. Unable to delete Hw entries for "
                           "port [%d] \r\n", __FUNCTION__,
                           i4LldpV2LocPortIfIndex);
            return SNMP_SUCCESS;    /* CHECK WHETHER TO RETURN FAILURE or SUCCESS */
        }
    }

    /* If Oper state is INIT, update H/w entries accordingly */
    if (pAppPortEntry->u1AppPriDcbxOperState == APP_PRI_OPER_INIT)
    {
        /*Update the entry with new Priority in control-plane 
         * AppPriUtlHwConfig will be called in AppPriUtlAdminParamChange*/
        pAppPriMappingEntry->u1AppPriPriority =
            (UINT1) u4SetValLldpXdot1dcbxAdminApplicationPriorityAEPriority;
        pAppPriMappingEntry->u1RowStatus = ACTIVE;
        AppPriUtlAdminParamChange (pAppPortEntry);

    }
    else
    {
        /*Update the entry with new Priority in control-plane */
        pAppPriMappingEntry->u1AppPriPriority =
            (UINT1) u4SetValLldpXdot1dcbxAdminApplicationPriorityAEPriority;
        pAppPriMappingEntry->u1RowStatus = ACTIVE;
        AppPriUtlAdminParamChange (pAppPortEntry);
    }
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2LldpXdot1dcbxAdminApplicationPriorityAEPriority
 Input       :  The Indices
                LldpV2LocPortIfIndex
                LldpXdot1dcbxAdminApplicationPriorityAESelector
                LldpXdot1dcbxAdminApplicationPriorityAEProtocol

                The Object 
                testValLldpXdot1dcbxAdminApplicationPriorityAEPriority
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2LldpXdot1dcbxAdminApplicationPriorityAEPriority (UINT4 *pu4ErrorCode,
                                                          INT4
                                                          i4LldpV2LocPortIfIndex,
                                                          INT4
                                                          i4LldpXdot1dcbxAdminApplicationPriorityAESelector,
                                                          UINT4
                                                          u4LldpXdot1dcbxAdminApplicationPriorityAEProtocol,
                                                          UINT4
                                                          u4TestValLldpXdot1dcbxAdminApplicationPriorityAEPriority)
{

    tAppPriPortEntry   *pAppPortEntry = NULL;
    tAppPriMappingEntry *pAppPriMappingEntry = NULL;
    tAppPriMappingEntry *pAdmEntry = NULL;
    tAppPriMappingEntry *pNextAdmEntry = NULL;
    tAppPriPortEntry   *pPortEntry = NULL;
    tTMO_SLL           *pAppPriAdmTbl = NULL;
    UINT4               u4Count = DCBX_ZERO;

    APP_PRI_SHUTDOWN_CHECK_TEST;

    pAppPortEntry = AppPriUtlGetPortEntry ((UINT4) i4LldpV2LocPortIfIndex);
    if (pAppPortEntry == NULL)
    {
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : AppPriUtlGetPortEntry () "
                       " Returns FAILURE. \r\n", __FUNCTION__);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_ERR_APP_PRI_NO_PORT_ENTRY);
        return SNMP_FAILURE;
    }

    if ((i4LldpXdot1dcbxAdminApplicationPriorityAESelector < DCBX_ONE) ||
        (i4LldpXdot1dcbxAdminApplicationPriorityAESelector >
         APP_PRI_MAX_SELECTOR))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_ERR_APP_PRI_INVALID_SELECTOR);
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC,
                       " %s : nmhTestv2LldpXdot1dcbxAdminApplicationPriorityAEPriority"
                       " Invalid value for Application Selector. \r\n",
                       __FUNCTION__);
        return SNMP_FAILURE;
    }

    if (((INT4) u4LldpXdot1dcbxAdminApplicationPriorityAEProtocol < DCBX_ZERO)
        || (u4LldpXdot1dcbxAdminApplicationPriorityAEProtocol >
            APP_PRI_MAX_PROTOCOL_ID))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_ERR_APP_PRI_INVALID_PROTOCOL);
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC,
                       "In %s : nmhTestv2LldpXdot1dcbxAdminApplicationPriorityAEPriority"
                       " Invalid value for Application Protocol ID. \r\n",
                       __FUNCTION__);
        return SNMP_FAILURE;
    }

    if (u4TestValLldpXdot1dcbxAdminApplicationPriorityAEPriority >=
        DCBX_MAX_PRIORITIES)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_ERR_APP_PRI_INVALID_PRIORITY);
        DCBX_TRC_ARG1 (DCBX_MGMT_TRC,
                       "In %s : nmhTestv2LldpXdot1dcbxAdminApplicationPriorityAEPriority"
                       " Invalid value for Application Priority. \r\n",
                       __FUNCTION__);
        return SNMP_FAILURE;
    }
    /* Check if maximum number of entries is already created */
    pAppPriMappingEntry =
        AppPriUtlGetAppPriMappingEntry ((UINT4) i4LldpV2LocPortIfIndex,
                                        i4LldpXdot1dcbxAdminApplicationPriorityAESelector,
                                        (INT4)
                                        u4LldpXdot1dcbxAdminApplicationPriorityAEProtocol,
                                        APP_PRI_ADMIN);
    if (pAppPriMappingEntry == NULL)
    {
        u4Count = TMO_SLL_Count (&(APP_PRI_ADM_TBL (pAppPortEntry,
                                                    i4LldpXdot1dcbxAdminApplicationPriorityAESelector
                                                    - 1)));
        if (u4Count > APP_PRI_MAX_PROTOCOL)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            CLI_SET_ERR (CLI_ERR_APP_PRI_MAX_APP_PRI_MAPPING);
            DCBX_TRC_ARG1 (DCBX_MGMT_TRC,
                           "In %s : nmhTestv2LldpXdot1dcbxAdminApplicationPriorityAEPriority"
                           " Maximum number of Application to Priority Mappings already "
                           "done \r\n", __FUNCTION__);
            return SNMP_FAILURE;
        }
    }

    /* FCOE and FIP priorities should always match */
    if ((i4LldpXdot1dcbxAdminApplicationPriorityAESelector == DCBX_ONE) &&
        (DCBX_IS_FCOE_OR_FIP_PROTOCOL
         (u4LldpXdot1dcbxAdminApplicationPriorityAEProtocol) == OSIX_TRUE))
    {
        pPortEntry = AppPriUtlGetPortEntry ((UINT4) i4LldpV2LocPortIfIndex);
        if (pPortEntry != NULL)
        {
            pAppPriAdmTbl = &(APP_PRI_ADM_TBL (pPortEntry,
                                               (i4LldpXdot1dcbxAdminApplicationPriorityAESelector
                                                - 1)));
            /* Look for FCOE */
            pAdmEntry = TMO_SLL_First (pAppPriAdmTbl);
            while (pAdmEntry != NULL)
            {
                /* Check for FCOE or FIP protocols */
                if ((DCBX_IS_FCOE_OR_FIP_PROTOCOL (pAdmEntry->i4AppPriProtocol)
                     == OSIX_TRUE)
                    && (pAdmEntry->i4AppPriProtocol !=
                        (INT4)
                        u4LldpXdot1dcbxAdminApplicationPriorityAEProtocol))
                {
                    if (pAdmEntry->u1AppPriPriority !=
                        (UINT1)
                        u4TestValLldpXdot1dcbxAdminApplicationPriorityAEPriority)
                    {
                        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                        CLI_SET_ERR (CLI_ERR_APP_PRI_FCOE_FIP_PRI_NOT_MATCH);
                        DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s: "
                                       " FCOE and FIP priorities do not match\r\n",
                                       __FUNCTION__);
                        return SNMP_FAILURE;
                    }
                    break;
                }
                /* Get next Adm entry */
                pNextAdmEntry = TMO_SLL_Next (pAppPriAdmTbl,
                                              &(pAdmEntry->AppPriMappingEntry));
                pAdmEntry = pNextAdmEntry;
            }
        }
    }

    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2LldpXdot1dcbxAdminApplicationPriorityAppTable
 Input       :  The Indices
                LldpV2LocPortIfIndex
                LldpXdot1dcbxAdminApplicationPriorityAESelector
                LldpXdot1dcbxAdminApplicationPriorityAEProtocol
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2LldpXdot1dcbxAdminApplicationPriorityAppTable (UINT4 *pu4ErrorCode,
                                                       tSnmpIndexList *
                                                       pSnmpIndexList,
                                                       tSNMP_VAR_BIND *
                                                       pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

PRIVATE INT4
DcbxRemETSCompareFunction (UINT4 u4RemTimeMark1, INT4 i4LocIfIndex1,
                           UINT4 u4RemMacIndex1, INT4 i4RemIfIndex1,
                           UINT4 u4RemTimeMark2, INT4 i4LocIfIndex2,
                           UINT4 u4RemMacIndex2, INT4 i4RemIfIndex2)
{
    if (u4RemTimeMark1 > u4RemTimeMark2)
    {
        return (DCBX_GREATER);
    }
    else if (u4RemTimeMark1 < u4RemTimeMark2)
    {
        return (DCBX_LESSER);
    }
    else
    {
        if (i4LocIfIndex1 > i4LocIfIndex2)
        {
            return (DCBX_GREATER);
        }
        else if (i4LocIfIndex1 < i4LocIfIndex2)
        {
            return (DCBX_LESSER);
        }
        else
        {
            if (u4RemMacIndex1 > u4RemMacIndex2)
            {
                return (DCBX_GREATER);
            }
            else if (u4RemMacIndex1 < u4RemMacIndex2)
            {
                return (DCBX_LESSER);
            }
            else
            {
                if (i4RemIfIndex1 > i4RemIfIndex2)
                {
                    return (DCBX_GREATER);
                }
                else if (i4RemIfIndex1 < i4RemIfIndex2)
                {
                    return (DCBX_LESSER);
                }
                else
                {
                    return (DCBX_EQUAL);
                }
            }
        }
    }
}
