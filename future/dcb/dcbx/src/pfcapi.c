/*************************************************************************
 * Copyright (C) 2010 Aricent Inc . All Rights Reserved
 * $Id: pfcapi.c,v 1.5 2016/05/25 10:06:10 siva Exp $
 * Description: This file contains PFC function Exported to DCBX.
****************************************************************************/
#include "dcbxinc.h"

/***************************************************************************
 *  FUNCTION NAME : PFCApiTlvCallBackFn
 * 
 *  DESCRIPTION   : This call back function will be registered with DCBX
 *                  to hanlde the PFC TLV related messages
 * 
 *  INPUT         : tDcbxAppInfo - Structure with TLV info.
 * 
 *  OUTPUT        : None
 * 
 *  RETURNS       : None
 * 
 * **************************************************************************/
PUBLIC VOID
PFCApiTlvCallBackFn (tDcbxAppInfo * pDcbxAppInfo)
{

    tPFCPortEntry      *pPortEntry = NULL;

    pPortEntry = PFCUtlGetPortEntry (pDcbxAppInfo->u4IfIndex);

    /* Get the PFC port entry  and if the port entry is not present
     * then return without processing the message from DCBX */

    if (pPortEntry == NULL)
    {
        DCBX_TRC (DCBX_FAILURE_TRC, "PFCApiTlvCallBackFn:"
                  " No PFC Port is created!!!\r\n");
        return;
    }

    switch (pDcbxAppInfo->u4MsgType)
    {
        case L2IWF_LLDP_APPL_TLV_RECV:
            /* TLV Received from the LLDP through DCBX,
             * process the TLV infomration and fill the remote table */
            DCBX_TRC (DCBX_TLV_TRC, "PFCApiTlvCallBackFn:"
                      "DCBX PFC TLV Received  !!!\r\n");
            PFCUtlHandleTLV (pPortEntry, &pDcbxAppInfo->ApplTlvParam);
            break;
        case L2IWF_LLDP_APPL_TLV_AGED:
        case L2IWF_LLDP_MULTIPLE_PEER_NOTIFY:
            /* Process the Age Out event from the LLDP through DCBX */
            DCBX_TRC (DCBX_TLV_TRC, "PFCApiTlvCallBackFn:"
                      "DCBX PFC TLV Age Out Received  !!!\r\n");
            PFCUtlHandleAgedOutFromDCBX (pPortEntry);
            break;
        case L2IWF_LLDP_APPL_RE_REG:
            /* Handle the Re-registration request from the LLDP */
            DCBX_TRC (DCBX_TLV_TRC, "PFCApiTlvCallBackFn:"
                      "DCBX PFC TLV Re Registration RequestReceived  !!!\r\n");
            PFCUtlHandleReRegReqFromDCBX (pPortEntry);
            break;
        case DCBX_OPER_UPDATE:
            /* State machine update from the DCBX */
            DCBX_TRC (DCBX_SEM_TRC, "PFCApiTlvCallBackFn:"
                      "DCBX PFC Sem Update Received  !!!\r\n");
            /* Change the state with the DCBX Oper
             * state received from the DCBX */
            PFCUtlOperStateChange (pPortEntry, pDcbxAppInfo->u1ApplOperState);
            /* Send SEM update event to Standby Node */
            DcbxRedSendDynamicPfcSemInfo (pPortEntry);
            break;

        default:
            break;
    }
    return;
}

/***************************************************************************
 *  FUNCTION NAME : PFCApiIsCfgCompatible
 * 
 *  DESCRIPTION   : This function is used to check the compatibility between
 *                  local and the peer for PFC Feature
 * 
 *  INPUT         : pPFCPortEntry - PFC Table Port Entry.
 * 
 *  OUTPUT        : None
 * 
 *  RETURNS       : TRUE/FALSE
 * 
 * **************************************************************************/
PUBLIC BOOL1
PFCApiIsCfgCompatible(tPFCPortEntry *pPFCPortEntry)
{
    DCBX_TRC (DCBX_TLV_TRC, "In PFCApiIsCfgCompatible : "
            "Comparing Local and Remote PFC configurations\r\n");

    if(pPFCPortEntry->PFCRemPortInfo.u1PFCRemStatus ==
            pPFCPortEntry->PFCAdmPortInfo.u1PFCAdmStatus)
    {
        if(pPFCPortEntry->PFCRemPortInfo.u1PFCRemWilling != PFC_ENABLED)
        {
            SET_DCBX_STATUS(pPFCPortEntry, pPFCPortEntry->u1PFCDcbxSemType, 
                    DCBX_RED_PFC_DCBX_STATUS_INFO, DCBX_STATUS_PEER_NW_CFG_COMPAT);
        }
        DCBX_TRC (DCBX_TLV_TRC, "In PFCApiIsCfgCompatible : "
                "Local and Remote PFC configuration are Compatible!\r\n");
        return OSIX_TRUE;
    }

    DCBX_TRC (DCBX_TLV_TRC, "In PFCApiIsCfgCompatible : "
            "Local and Remote PFC configuration are not Compatible!\r\n");
    SET_DCBX_STATUS(pPFCPortEntry, pPFCPortEntry->u1PFCDcbxSemType, 
            DCBX_RED_PFC_DCBX_STATUS_INFO, DCBX_STATUS_CFG_NOT_COMPT);
    return OSIX_FALSE;
}

