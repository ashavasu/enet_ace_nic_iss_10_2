/*************************************************************************
 * Copyright (C) 2010 Aricent Inc . All Rights Reserved
 * $Id: ceeapi.c,v 1.2 2016/05/25 10:06:09 siva Exp $
 * Description: This file contains CEE function Exported to DCBX.
****************************************************************************/
#include "dcbxinc.h"

/***************************************************************************
 *  FUNCTION NAME : CEEApiTlvCallBackFn
 * 
 *  DESCRIPTION   : This call back function will be registered with DCBX
 *                  to handle the CEE TLV related messages
 * 
 *  INPUT         : tDcbxAppInfo - Structure with TLV info.
 * 
 *  OUTPUT        : None
 * 
 *  RETURNS       : None
 * 
 * **************************************************************************/
PUBLIC VOID
CEEApiTlvCallBackFn (tDcbxAppInfo * pDcbxAppInfo)
{
    tCEECtrlEntry      *pCEECtrlEntry = NULL;
    tCeeTLVInfo        CeeFeatTlvParam;

    switch (pDcbxAppInfo->u4MsgType)
    {
        case L2IWF_LLDP_APPL_TLV_RECV:
            DCBX_TRC (DCBX_TLV_TRC, "In CEEApiTlvCallBackFn: "
                      "DCBX CEE TLV Received !!!\r\n");
            CEEUtlHandleTLV (pDcbxAppInfo->u4IfIndex, &pDcbxAppInfo->ApplTlvParam);
            break;
        case L2IWF_LLDP_APPL_TLV_AGED:
            /* Process the Age Out event from the LLDP through DCBX */
            DCBX_TRC (DCBX_TLV_TRC, "In CEEApiTlvCallBackFn: "
                      "DCBX CEE TLV Age Out Received !!!\r\n");
            CEEUtlHandleAgedOutFromDCBX (pDcbxAppInfo->u4IfIndex);
            break;
        case L2IWF_LLDP_APPL_RE_REG:
            /* Handddle the Re-registration request from the LLDP */
            DCBX_TRC (DCBX_TLV_TRC, "In CEEApiTlvCallBackFn: "
                      "DCBX CEE TLV Re Registration RequestReceived !!!\r\n");
            CEEHandleReRegReqFromDCBX (pDcbxAppInfo->u4IfIndex);
            break;
        case L2IWF_LLDP_MULTIPLE_PEER_NOTIFY:
                pCEECtrlEntry = CEEUtlGetCtrlEntry (pDcbxAppInfo->u4IfIndex);
                if (pCEECtrlEntry == NULL)
                {
                    DCBX_TRC_ARG2 (DCBX_TLV_TRC, "%s : Failed to fetch the"
                            "Ctrl Entry for Port Id %d \r\n",
                            __FUNCTION__, pDcbxAppInfo->u4IfIndex);
                    return;
                }

                CeeFeatTlvParam.u4IfIndex = pDcbxAppInfo->u4IfIndex;
                pCEECtrlEntry->u4CtrlSeqNo     = DCBX_ZERO;
                pCEECtrlEntry->u4CtrlRcvdSeqNo = DCBX_ZERO;
                pCEECtrlEntry->u4CtrlAckNo     = DCBX_ZERO;
                pCEECtrlEntry->u4CtrlRcvdAckNo = DCBX_ZERO;
                pCEECtrlEntry->u4CtrlSyncNo    = DCBX_ZERO;
                pCEECtrlEntry->u4CtrlAckMissCount = DCBX_ZERO;

                CEESemRun (CEE_ETS_TLV_TYPE,&CeeFeatTlvParam,CEE_EV_INIT);
                CEESemRun (CEE_PFC_TLV_TYPE,&CeeFeatTlvParam,CEE_EV_INIT);
                CEESemRun (CEE_APP_PRI_TLV_TYPE,&CeeFeatTlvParam,CEE_EV_INIT);
                CEEFormAndSendTLV (pCEECtrlEntry->u4IfIndex, DCBX_LLDP_PORT_UPDATE, DCBX_ZERO);
            break;
        default:
            DCBX_TRC (DCBX_TLV_TRC, "In CEEApiTlvCallBackFn: "
                      "DCBX-CEE Received invalid Msg type !!!\r\n");
            break;
    }
    return;
}

/* END OF FILE */
