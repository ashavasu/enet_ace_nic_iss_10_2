/*************************************************************************
 * Copyright (C) 2010 Aricent Inc . All Rights Reserved
 * $Id: etsutl.c,v 1.30 2017/12/28 10:01:36 siva Exp $
 * Description: This file contains ETS utility function.
****************************************************************************/
#ifndef ETS_UTL_C
#define ETS_UTL_C

#include "dcbxinc.h"
#ifdef DCBX_ETS_QOS_WANTED
#include "msr.h"
UINT1               gu1DefaultConfFlag[SYS_DEF_MAX_PHYSICAL_INTERFACES] = { 0 };
#endif
/***************************************************************************
 *  FUNCTION NAME : ETSUtlPortTblCmpFn
 * 
 *  DESCRIPTION   : This utility function is used by RbTree for ETS port 
 *                  table.
 * 
 *  INPUT         : pRBElem1,pRBElem2 - RbTree Elements.
 * 
 *  OUTPUT        : None
 * 
 *  RETURNS       : LESSER/GREATER/EQUAL
 * 
 * **************************************************************************/
INT4
ETSUtlPortTblCmpFn (tRBElem * pRBElem1, tRBElem * pRBElem2)
{
    tETSPortEntry      *pETSPortEntry1 = NULL;
    tETSPortEntry      *pETSPortEntry2 = NULL;

    pETSPortEntry1 = (tETSPortEntry *) pRBElem1;
    pETSPortEntry2 = (tETSPortEntry *) pRBElem2;

    /* Compare the DCB Port Index */

    if (pETSPortEntry1->u4IfIndex < pETSPortEntry2->u4IfIndex)
    {
        return (DCBX_LESSER);
    }
    else if (pETSPortEntry1->u4IfIndex > pETSPortEntry2->u4IfIndex)
    {
        return (DCBX_GREATER);
    }

    return (DCBX_EQUAL);
}

/*****************************************************************************/
/* Function Name      : ETSUtlCreatePortTblEntry                             */

/* Description        : This function is used to Create an Entry             */
/*                      it will do the following Action                      */
/*                      1. Allocate a memory block for the New Entry         */
/*                      2. Initialize the New Entry                          */
/*                      3. Add this New Entry into the ETS Port Table        */
/*                                                                           */
/* Input(s)           : u4IfIndex      - Index to the ETS port Table         */
/*                                                                           */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : gETSGlobalInfo                                       */
/* Global Variables                                                          */
/* Modified           : gETSGlobalInfo                                       */
/* Return Value(s)    : NULL / Pointer to  ETSPortEntry                      */
/* Called By          : SNMP Low Level                                       */
/* Calling Function   :                                                      */
/*****************************************************************************/
tETSPortEntry      *
ETSUtlCreatePortTblEntry (UINT4 u4IfIndex)
{
    tETSPortEntry      *pETSPortEntry = NULL;
    UINT4               u4PgIdx = DCBX_ZERO;
    UINT4               u4TcSupp = DCBX_ZERO;
    UINT1               u1DcbxMode = DCBX_MODE_AUTO;

    /* 1. Allocate a memory block for the New Entry */
    pETSPortEntry = (tETSPortEntry *)
        MemAllocMemBlk (gETSGlobalInfo.ETSPortPoolId);
    if (pETSPortEntry == NULL)
    {
        DCBX_TRC_ARG2 ((DCBX_RESOURCE_TRC | DCBX_FAILURE_TRC),
                       "In %s : MemAllocMemBlk () Failed ,"
                       "for ETS Specified Port is %d \r\n", __FUNCTION__,
                       u4IfIndex);
        return (NULL);
    }

    /* 2. Initialize the ETS Port Entry */
    MEMSET (pETSPortEntry, DCBX_INIT_VAL, sizeof (tETSPortEntry));
    pETSPortEntry->u4IfIndex = u4IfIndex;

    /*Default ETS Status of the Port */
    pETSPortEntry->u1ETSAdminMode = ETS_PORT_MODE_OFF;

    /*Default ETS Oper State of the Port */
    pETSPortEntry->u1ETSDcbxOperState = ETS_OPER_OFF;

    pETSPortEntry->u1ETSConfigTxStatus = ETS_DISABLED;
    pETSPortEntry->u1ETSRecoTxStatus = ETS_DISABLED;
    pETSPortEntry->u1TcSupportTxStatus = ETS_DISABLED;
    DcbxUtlGetDCBXMode (u4IfIndex, &u1DcbxMode);
    if (u1DcbxMode == DCBX_MODE_CEE)
    {
        pETSPortEntry->u1ETSDcbxSemType = DCBX_FEAT_STATE_MACHINE;
    }
    else if (u1DcbxMode == DCBX_MODE_IEEE)
    {
        pETSPortEntry->u1ETSDcbxSemType = DCBX_ASYM_STATE_MACHINE;
    }
    else
    {
        pETSPortEntry->u1ETSDcbxSemType = DCBX_DEF_ETS_SM_TYPE;
    }

    pETSPortEntry->u1CurrentState = CEE_ST_WAIT_FOR_FEAT_TLV;
    pETSPortEntry->u1DcbxStatus = DCBX_STATUS_NOT_ADVERTISE;
    pETSPortEntry->bETSSyncd = ETS_DISABLED;
    pETSPortEntry->bETSError = ETS_DISABLED;
    pETSPortEntry->bETSPeerWilling = ETS_DISABLED;

    QosApiGetMaxTCSupport (pETSPortEntry->u4IfIndex, &u4TcSupp);
    pETSPortEntry->ETSAdmPortInfo.u1ETSAdmNumTcSup = (UINT1) u4TcSupp;

    if (pETSPortEntry->ETSAdmPortInfo.u1ETSAdmNumTcSup == ETS_TCGID8)
    {
        pETSPortEntry->ETSAdmPortInfo.u1ETSAdmNumTCGSup = DCBX_ZERO;
    }
    else
    {
        pETSPortEntry->ETSAdmPortInfo.u1ETSAdmNumTCGSup =
            pETSPortEntry->ETSAdmPortInfo.u1ETSAdmNumTcSup;
    }

    pETSPortEntry->ETSAdmPortInfo.u1ETSAdmWilling = ETS_DISABLED;

    for (u4PgIdx = ETS_TCGID0; u4PgIdx <= ETS_TCGID7; u4PgIdx++)
    {
        /* Setting the default BW values to the TCs */
        if (u4PgIdx == ETS_TCGID0)
        {
            pETSPortEntry->ETSAdmPortInfo.au1ETSAdmBW[u4PgIdx] = ETS_MAX_BW;
            pETSPortEntry->ETSAdmPortInfo.au1ETSAdmRecoBW[u4PgIdx] = ETS_MAX_BW;
        }
        else
        {
            pETSPortEntry->ETSAdmPortInfo.au1ETSAdmBW[u4PgIdx] = 0;
            pETSPortEntry->ETSAdmPortInfo.au1ETSAdmRecoBW[u4PgIdx] = 0;
        }

        /* Setting the default TSA as ETS */
        pETSPortEntry->ETSAdmPortInfo.au1ETSAdmTsaTable[u4PgIdx]
            = DCBX_ENHANCED_TRANSMISSION_SELECTION_ALGO;
        pETSPortEntry->ETSAdmPortInfo.au1ETSAdmRecoTsaTable[u4PgIdx]
            = DCBX_ENHANCED_TRANSMISSION_SELECTION_ALGO;
    }
    /* Copy Default parameters to Local Info */
    MEMCPY (&(pETSPortEntry->ETSLocPortInfo),
            &(pETSPortEntry->ETSAdmPortInfo), sizeof (tETSLocPortInfo));

    /* 3. Add this New Entry into the ETS Port  Table */
    if (RBTreeAdd (gETSGlobalInfo.pRbETSPortTbl,
                   (tRBElem *) pETSPortEntry) != RB_SUCCESS)
    {
        MemReleaseMemBlock (gETSGlobalInfo.ETSPortPoolId,
                            (UINT1 *) pETSPortEntry);
        DCBX_TRC_ARG2 (DCBX_FAILURE_TRC, "In %s : RBTreeAdd () Failed ,"
                       "for DCB port %d \r\n", __FUNCTION__, u4IfIndex);
        return (NULL);
    }

    return (pETSPortEntry);
}

/*****************************************************************************/
/* Function Name      : ETSUtlDeletePortTblEntry                             */
/* Description        : This function is used to Delete an Entry from the    */
/*                      ETS Port Table ,it will do the following Action      */
/*                      1. Remove the Entry from the ETS Port Table          */
/*                      2. Clear the Removed Entry                           */
/*                      3. Release the Removed Entry's memory                */
/* Input(s)           : pETSPortEntry   - Pointer to  ETSPortEntry           */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : gETSGlobalInfo                                       */
/* Global Variables                                                          */
/* Modified           : gETSGlobalInfo                                       */
/* Return Value(s)    : OSIX_SUCCESS or OSIX_FAILURE                        */
/* Called By          : SNMP Low Level                                       */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
ETSUtlDeletePortTblEntry (tETSPortEntry * pETSPortEntry)
{
    /* Deregister all the application for this port with DCBX */
    ETSUtlRegOrDeRegBasedOnMode (pETSPortEntry, ETS_DISABLED, ETS_TLV_ALL);

    /*Disable ETS Featue */
    pETSPortEntry->u1ETSAdminMode = ETS_PORT_MODE_OFF;
    /* Change the state to OFF. While changing the state
     * it will disable the ETS feature in the hardware */
    ETSUtlOperStateChange (pETSPortEntry, ETS_OPER_OFF);

    /* Remove Entry from the ETS Port Table */
    RBTreeRemove (gETSGlobalInfo.pRbETSPortTbl, (UINT1 *) pETSPortEntry);

    /* Release the Removed Entry's memory */
    MemReleaseMemBlock (gETSGlobalInfo.ETSPortPoolId, (UINT1 *) pETSPortEntry);
    return (OSIX_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : ETSUtlGetPortEntry                                   */
/* Description        : Returns pointer to port entry                        */
/* Input(s)           : u4IfIndex                                            */
/* Output(s)          : None.                                                */
/* Return Value(s)    : Pointer to  PORT entry                               */
/*****************************************************************************/
tETSPortEntry      *
ETSUtlGetPortEntry (UINT4 u4IfIndex)
{
    tETSPortEntry       PortEntry;
    tETSPortEntry      *pPortEntry = NULL;

    MEMSET (&PortEntry, DCBX_ZERO, sizeof (tETSPortEntry));
    PortEntry.u4IfIndex = u4IfIndex;
    pPortEntry = (tETSPortEntry *)
        RBTreeGet (gETSGlobalInfo.pRbETSPortTbl, (tRBElem *) & PortEntry);
    if (pPortEntry == NULL)
    {
        DCBX_TRC_ARG2 (DCBX_FAILURE_TRC,
                       "%s : Port Entry (Port) Id %d is invalid \r\n",
                       __FUNCTION__, u4IfIndex);
    }

    return (pPortEntry);
}

/***************************************************************************
 *  FUNCTION NAME : ETSUtlConstructConfPreFormTlv
 * 
 *  DESCRIPTION   : This utility function is used to form the ETS Conf
 *                  TLV header.
 * 
 *  INPUT         : None
 * 
 *  OUTPUT        : None
 * 
 *  RETURNS       : None
 * 
 * **************************************************************************/
PUBLIC VOID
ETSUtlConstructConfPreFormTlv (VOID)
{
    UINT2               u2TlvHeader = DCBX_ZERO;
    UINT1              *pu1Temp = NULL;

    MEMSET (gau1ETSConfPreFormedTlv, DCBX_ZERO,
            sizeof (gau1ETSConfPreFormedTlv));
    /*Form TLV Header */
    DCBX_PUT_HEADER_LENGTH ((UINT2) ETS_TLV_TYPE, (UINT2) ETS_CONF_TLV_INFO_LEN,
                            &u2TlvHeader);
    /* put tlv header in buffer */
    pu1Temp = gau1ETSConfPreFormedTlv;
    DCBX_PUT_2BYTE (pu1Temp, u2TlvHeader);
    DCBX_PUT_OUI (pu1Temp, gau1DcbxOUI);
    DCBX_PUT_1BYTE (pu1Temp, (UINT1) ETS_CONF_TLV_SUB_TYPE);
    return;
}

/***************************************************************************
 *  FUNCTION NAME : ETSUtlConstructRecoPreFormTlv
 * 
 *  DESCRIPTION   : This utility function is used to form the Reco TLV
 *                  header.
 * 
 *  INPUT         : None
 * 
 *  OUTPUT        : None
 * 
 *  RETURNS       : None
 * 
 * **************************************************************************/
PUBLIC VOID
ETSUtlConstructRecoPreFormTlv (VOID)
{
    UINT2               u2TlvHeader = DCBX_ZERO;
    UINT1              *pu1Temp = NULL;
    MEMSET (gau1ETSRecoPreFormedTlv, DCBX_ZERO,
            sizeof (gau1ETSRecoPreFormedTlv));

    /*Form TLV Header */
    DCBX_PUT_HEADER_LENGTH ((UINT2) ETS_TLV_TYPE, (UINT2) ETS_RECO_TLV_INFO_LEN,
                            &u2TlvHeader);
    /* put tlv header in  buffer */
    pu1Temp = gau1ETSRecoPreFormedTlv;
    DCBX_PUT_2BYTE (pu1Temp, u2TlvHeader);
    DCBX_PUT_OUI (pu1Temp, gau1DcbxOUI);
    DCBX_PUT_1BYTE (pu1Temp, (UINT1) ETS_RECO_TLV_SUB_TYPE);
    return;
}

/***************************************************************************
 *  FUNCTION NAME : ETSUtlConstructTcSuppPreFormTlv
 * 
 *  DESCRIPTION   : This utility function is used to form the TC Supp
 *                  TLV header.
 * 
 *  INPUT         : None
 * 
 *  OUTPUT        : None
 * 
 *  RETURNS       : None
 * 
 * **************************************************************************/
PUBLIC VOID
ETSUtlConstructTcSuppPreFormTlv (VOID)
{
    UINT2               u2TlvHeader = DCBX_ZERO;
    UINT1              *pu1Temp = NULL;
    MEMSET (gau1ETSTcSuppPreFormedTlv, DCBX_ZERO,
            sizeof (gau1ETSTcSuppPreFormedTlv));

    /*Form TLV Header */
    DCBX_PUT_HEADER_LENGTH ((UINT2) ETS_TLV_TYPE,
                            (UINT2) ETS_TC_SUPP_TLV_INFO_LEN, &u2TlvHeader);
    /* put tlv header in  buffer */
    pu1Temp = gau1ETSTcSuppPreFormedTlv;
    DCBX_PUT_2BYTE (pu1Temp, u2TlvHeader);
    DCBX_PUT_OUI (pu1Temp, gau1DcbxOUI);
    DCBX_PUT_1BYTE (pu1Temp, (UINT1) ETS_TC_SUPP_TLV_SUB_TYPE);
    return;
}

/***************************************************************************
 *  FUNCTION NAME : ETSPortPostMsgToDCBX
 * 
 *  DESCRIPTION   : This utility function is used to post the TLV/Sem
 *                  Update message to DCBX.
 * 
 *  INPUT         : u1MsgType - Message type Reg/Update/Sem/DeReg.
 *                  u4IfIndex - Port Number.
 *                  pDcbxAppPortMsg - App Reg Info Structure
 * 
 *  OUTPUT        : None
 * 
 *  RETURNS       : None
 * 
 * **************************************************************************/
PUBLIC VOID
ETSPortPostMsgToDCBX (UINT1 u1MsgType, UINT4 u4IfIndex,
                      tDcbxAppRegInfo * pDcbxAppPortMsg)
{

    /* Based on the mesasge type, it calls the respective DCBX functions */
    switch (u1MsgType)
    {
        case DCBX_LLDP_PORT_UPDATE:
            /* Used to send the TLV updated information to the LDDP using 
             * DCBX function */
            if (DcbxApiApplPortUpdate (u4IfIndex, pDcbxAppPortMsg)
                != OSIX_SUCCESS)
            {
                DCBX_TRC (DCBX_TLV_TRC | DCBX_FAILURE_TRC,
                          "ETSPortPostMsgToDCBX:"
                          "DCBX Port TLV Update Failed !!!\r\n");
            }
            break;

        case DCBX_LLDP_PORT_REG:
            /* Used to Register the application to the LDDP using 
             * DCBX function */
            if (DcbxApiApplPortReg (u4IfIndex, pDcbxAppPortMsg) != OSIX_SUCCESS)
            {
                DCBX_TRC (DCBX_TLV_TRC | DCBX_FAILURE_TRC,
                          "ETSPortPostMsgToDCBX:"
                          "DCBX Port Registration Failed !!!\r\n");
            }
            break;

        case DCBX_LLDP_PORT_DEREG:
            /* Used to De-Register the application from the LDDP using 
             * DCBX function */
            if (DcbxApiApplPortDeReg (u4IfIndex, pDcbxAppPortMsg) !=
                OSIX_SUCCESS)
            {
                DCBX_TRC (DCBX_TLV_TRC | DCBX_FAILURE_TRC,
                          "ETSPortPostMsgToDCBX:"
                          "DCBX Port DeRegistration Failed !!!\r\n");
            }
            break;

        case DCBX_STATE_MACHINE:
            /* Used to run the DCBX state machine and get the necessary 
             * Operational state  */
            DCBX_TRC (DCBX_SEM_TRC,
                      "ETSPortPostMsgToDCBX:"
                      "ETS Port SEM update request to DCBX!!!\r\n");
            DcbxSemUpdate (u4IfIndex, pDcbxAppPortMsg);
            break;
        default:
            DCBX_TRC (DCBX_FAILURE_TRC,
                      "ETSPortPostMsgToDCBX:"
                      "Invalid Message Type Received !!!\r\n");
            break;
    }
    return;
}

/***************************************************************************
 *  FUNCTION NAME : ETSUtlHandleAgedOutFromDCBX 
 * 
 *  DESCRIPTION   : This function is used to handle the TLV Age Out
 *                  Message from DCBX.
 * 
 *  INPUT         : pPortEntry - ETS Port Entry.
 *                  u1TlvSubType - TLV Sub type - Conf/Reco/TcSupp
 * 
 *  OUTPUT        : None
 * 
 *  RETURNS       : None
 * 
 * **************************************************************************/
PUBLIC VOID
ETSUtlHandleAgedOutFromDCBX (tETSPortEntry * pPortEntry, UINT1 u1TlvSubType)
{
    if (u1TlvSubType == ETS_CONF_TLV_SUB_TYPE)
    {
        SET_DCBX_STATUS (pPortEntry, pPortEntry->u1ETSDcbxSemType,
                         DCBX_RED_ETS_DCBX_STATUS_INFO,
                         DCBX_STATUS_PEER_NO_ADV_FEAT);
    }
    if ((u1TlvSubType == ETS_CONF_TLV_SUB_TYPE ||
         u1TlvSubType == ETS_RECO_TLV_SUB_TYPE))
    {
        /* If Age out message is recieved for Conf TLV Or Reco TLV then move
         * the state to INIT */
        DCBX_TRC (DCBX_SEM_TRC, "ETSUtlHandleAgedOutFromDCBX:"
                  "ETS/RECO Age Out Event is received. Change the"
                  " state to INIT!!!\r\n");
        ETSUtlOperStateChange (pPortEntry, ETS_OPER_INIT);
        /* Send State Change information to Standby Node.
         * so that if it is RECO state it will be moved to
         * INIT in standby also.*/
        DcbxRedSendDynamicEtsSemInfo (pPortEntry);
    }
    /* If Age out message is recieved then clear the  
     * TLV related Remote Parameters */
    ETSUtlClearRemoteParam (pPortEntry, u1TlvSubType);
    /* Send Remote table Clear information to Standby Node */
    DcbxRedSendDynamicEtsRemInfo (pPortEntry);
    return;
}

/***************************************************************************
 *  FUNCTION NAME : ETSUtlClearRemoteParam
 * 
 *  DESCRIPTION   : This utility function is used to Clear the remote param.
 * 
 *  INPUT         : pPortEntry - ETS Port Entry.
 *                  u1TlvType - TLV type - Conf/Reco/TCSupp.
 * 
 *  OUTPUT        : None
 * 
 *  RETURNS       : None
 * 
 * **************************************************************************/
PUBLIC VOID
ETSUtlClearRemoteParam (tETSPortEntry * pPortEntry, UINT1 u1TlvType)
{
    tDcbxTrapMsg        NotifyInfo;

    MEMSET (&NotifyInfo, DCBX_ZERO, sizeof (tDcbxTrapMsg));

    if (u1TlvType == ETS_CONF_TLV_SUB_TYPE)
    {
        /* If the Conf TLV updation happened already then
         * clear the Remote Parameter.Here updation of  Local information 
         * will be taken care in the State change.*/
        if (pPortEntry->u1RemTlvUpdStatus & ETS_CONF_TLV_REM_UPD)
        {
            MEMSET (pPortEntry->ETSRemPortInfo.au1ETSRemBW, DCBX_ZERO,
                    ETS_MAX_NUM_TCGID);

            MEMSET (pPortEntry->ETSRemPortInfo.au1ETSRemTsaTable, DCBX_ZERO,
                    ETS_MAX_TCGID_CONF);
            MEMSET (pPortEntry->ETSRemPortInfo.au1ETSRemTCGID,
                    DCBX_ZERO, DCBX_MAX_PRIORITIES);
            pPortEntry->ETSRemPortInfo.u1ETSRemNumTCGSup = DCBX_ZERO;
            pPortEntry->ETSRemPortInfo.u1ETSRemWilling = DCBX_ZERO;
            /* Update the Remote update status for this TLV */
            pPortEntry->u1RemTlvUpdStatus =
                (UINT1) (pPortEntry->u1RemTlvUpdStatus &
                         (~(ETS_CONF_TLV_REM_UPD)));
        }

    }
    else if (u1TlvType == ETS_TC_SUPP_TLV_SUB_TYPE)
    {
        /* If the TC Supported TLV updation happened already then
         * clear the Remote Parameter and also update the 
         * Local information if the state is in RX */
        if (pPortEntry->u1RemTlvUpdStatus & ETS_TC_SUPP_TLV_REM_UPD)
        {
            if (pPortEntry->u1ETSDcbxOperState == ETS_OPER_RECO)
            {
                pPortEntry->ETSLocPortInfo.u1ETSLocNumTcSup =
                    pPortEntry->ETSAdmPortInfo.u1ETSAdmNumTcSup;
            }
            pPortEntry->ETSRemPortInfo.u1ETSRemNumTcSup = DCBX_ZERO;
            /* Update the Remote update status for this TLV */
            pPortEntry->u1RemTlvUpdStatus =
                (UINT1) (pPortEntry->u1RemTlvUpdStatus &
                         (~(ETS_TC_SUPP_TLV_REM_UPD)));
        }
    }
    else
    {
        /* If the ETS Reco TLV updation happened already then
         * clear the Remote Parameter and also update the 
         * Local information if the state is in RX */
        if (pPortEntry->u1RemTlvUpdStatus & ETS_RECO_TLV_REM_UPD)
        {
            /* Update the Remote update status for this TLV */
            pPortEntry->u1RemTlvUpdStatus =
                (UINT1) (pPortEntry->u1RemTlvUpdStatus
                         & (~(ETS_RECO_TLV_REM_UPD)));
            if (pPortEntry->u1ETSDcbxOperState == ETS_OPER_RECO)
            {
                MEMCPY (pPortEntry->ETSLocPortInfo.au1ETSLocRecoBW,
                        pPortEntry->ETSAdmPortInfo.au1ETSAdmRecoBW,
                        ETS_MAX_NUM_TCGID);

                MEMCPY (pPortEntry->ETSLocPortInfo.au1ETSLocRecoTsaTable,
                        pPortEntry->ETSAdmPortInfo.au1ETSAdmRecoTsaTable,
                        ETS_MAX_TCGID_CONF);
                /* Program the Hardware since it is Recommended state and
                 * Reco BW updation flag was set, it would have taken the
                 * Reco BW value in the hardware. So we need to reprogram
                 * the harware with BW updated from the CONF TLV, So 
                 * updated the mask and pragramming the hardware */
                ETSUtlHwConfig (pPortEntry, ETS_ENABLED);
            }
            MEMSET (pPortEntry->ETSRemPortInfo.au1ETSRemRecoBW, DCBX_ZERO,
                    ETS_MAX_NUM_TCGID);

            MEMSET (pPortEntry->ETSRemPortInfo.au1ETSRemRecoTsaTable,
                    DCBX_ZERO, ETS_MAX_TCGID_CONF);
            MEMSET (pPortEntry->ETSRemPortInfo.au1ETSRemRecoTCGID,
                    DCBX_ZERO, DCBX_MAX_PRIORITIES);
        }
    }
    if (pPortEntry->u1RemTlvUpdStatus == DCBX_ZERO)
    {
        /*If there is no updated parameters present in Remote table, 
         * Clear the  Remote index paramters */
        MEMSET (pPortEntry->ETSRemPortInfo.au1ETSDestRemMacAddr, DCBX_ZERO,
                MAC_ADDR_LEN);
        pPortEntry->ETSRemPortInfo.u4ETSDestRemMacIndex = DCBX_ZERO;
        pPortEntry->ETSRemPortInfo.i4ETSRemIndex = DCBX_ZERO;
        pPortEntry->ETSRemPortInfo.u4ETSRemTimeMark = DCBX_ZERO;

        /* Send Trap Notification for Peer Down Status */
        NotifyInfo.u4IfIndex = pPortEntry->u4IfIndex;
        NotifyInfo.u1Module = ETS_TRAP;
        NotifyInfo.unTrapMsg.u1PeerStatus = ETS_DISABLED;
        DcbxSendNotification (&NotifyInfo, PEER_STATE_TRAP);
    }
    return;
}

/***************************************************************************
 *  FUNCTION NAME : ETSUtlHandleReRegReqFromDCBX
 * 
 *  DESCRIPTION   : This utility function is used to Handle the Re Reg
 *                  Message from DCBX.
 * 
 *  INPUT         : pPortEntry - ETS Port Entry.
 *                  u1TlvSubType - TLV Type -Conf/Reco/TcSupp
 * 
 *  OUTPUT        : None
 * 
 *  RETURNS       : None
 * 
 * **************************************************************************/
PUBLIC VOID
ETSUtlHandleReRegReqFromDCBX (tETSPortEntry * pPortEntry, UINT1 u1TlvSubType)
{
    /* For Re-Registration, Form the TLV and send it with
     * Port Registration request to the DCBX */
    if (u1TlvSubType == ETS_CONF_TLV_SUB_TYPE)
    {
        ETSUtlFormAndSendConfigTLV (pPortEntry, DCBX_LLDP_PORT_REG);
    }
    else if (u1TlvSubType == ETS_RECO_TLV_SUB_TYPE)
    {
        ETSUtlFormAndSendRecoTLV (pPortEntry, DCBX_LLDP_PORT_REG);
    }
    else
    {
        ETSUtlFormAndSendTCSupportTLV (pPortEntry, DCBX_LLDP_PORT_REG);
    }
    return;
}

/***************************************************************************
 *  FUNCTION NAME : ETSUtlDeleteAllPorts
 * 
 *  DESCRIPTION   : This utility function is used to delete all 
 *                  the ETS ports at the time of shutdown.
 * 
 *  INPUT         : None
 * 
 *  OUTPUT        : None
 * 
 *  RETURNS       : None
 * 
 * **************************************************************************/
PUBLIC VOID
ETSUtlDeleteAllPorts (VOID)
{
    tETSPortEntry       TempPortEntry;
    tETSPortEntry      *pPortEntry = NULL;

    MEMSET (&TempPortEntry, DCBX_ZERO, sizeof (tETSPortEntry));

    /* Get the First port entry and delete the port entry if present */
    pPortEntry = (tETSPortEntry *) RBTreeGetFirst
        (gETSGlobalInfo.pRbETSPortTbl);
    if (pPortEntry != NULL)
    {
        do
        {
            /* Delete the ETS port table entry */
            TempPortEntry.u4IfIndex = pPortEntry->u4IfIndex;
            ETSUtlDeletePortTblEntry (pPortEntry);
            pPortEntry = (tETSPortEntry *) RBTreeGetNext
                (gETSGlobalInfo.pRbETSPortTbl, &TempPortEntry,
                 ETSUtlPortTblCmpFn);
            /* Process for all the ports in the port RB Tree */
        }
        while (pPortEntry != NULL);
    }
    return;
}

/***************************************************************************
 *  FUNCTION NAME : ETSUtlDeletePort
 * 
 *  DESCRIPTION   : This utility function is used to delete the ETS Port.
 * 
 *  INPUT         : u4IfIndex - Port Number
 * 
 *  OUTPUT        : None
 * 
 *  RETURNS       : None
 * 
 * **************************************************************************/
PUBLIC VOID
ETSUtlDeletePort (UINT4 u4IfIndex)
{
    tETSPortEntry      *pPortEntry = NULL;
    pPortEntry = ETSUtlGetPortEntry (u4IfIndex);
    /* Get the ETS port entry */
    if (pPortEntry == NULL)
    {
        DCBX_TRC (DCBX_CONTROL_PLANE_TRC, "ETSUtlDeletePort:"
                  " No ETS Port is created!!!\r\n");
        return;
    }
    /* Deletes this ETS port entry */
    ETSUtlDeletePortTblEntry (pPortEntry);
    return;
}

/***************************************************************************
 *  FUNCTION NAME : ETSUtlRegisterApplForPort
 * 
 *  DESCRIPTION   : This utility function is used to register the Appl
 *                  with DCBX.
 * 
 *  INPUT         : pPortEntry - ETS Port Entry.
 *                  u1TlvType - TLV Type - Reco/Conf/TcSupp.
 * 
 *  OUTPUT        : None
 * 
 *  RETURNS       : None
 * 
 * **************************************************************************/
PUBLIC VOID         ETSUtlRegisterApplForPort
    (tETSPortEntry * pPortEntry, UINT1 u1TlvType)
{
    /* If Module is not enabled then no need to register the application
     * with DCBX and it will registered for all ports which require 
     * registration when the Module status is enabled */
    if (gETSGlobalInfo.u1ETSModStatus != ETS_ENABLED)
    {
        DCBX_TRC (DCBX_CONTROL_PLANE_TRC, "ETSUtlRegisterApplForPort:"
                  " No Registration Required as the Module"
                  "is not enabled!!!\r\n");
        return;
    }
    /* When the Admin mode of the port is not set to Auto then no need to 
     * register any application with DCBX since ETS is not ready to operate
     * throught DCBX and it can take the local configuration with ON Mode */
    if (pPortEntry->u1ETSAdminMode != ETS_PORT_MODE_AUTO)
    {
        DCBX_TRC (DCBX_CONTROL_PLANE_TRC, "ETSUtlRegisterApplForPort:"
                  " No Registration Required as the Mode is not Auto!!!\r\n");
        return;
    }
    /* Form and send the Conf TLV with Port registration request only if
     * TLV tx status is enabled */
    if (((u1TlvType == ETS_CONF_TLV_SUB_TYPE) || (u1TlvType == ETS_TLV_ALL)) &&
        (pPortEntry->u1ETSConfigTxStatus != ETS_DISABLED))
    {
        DCBX_TRC (DCBX_TLV_TRC, "ETSUtlRegisterApplForPort:"
                  "Forming and sending ETS Conf TLV!!!\r\n");
        ETSUtlFormAndSendConfigTLV (pPortEntry, DCBX_LLDP_PORT_REG);

    }
    /* Form and send the Reco TLV with Port registration request only if
     * TLV tx status is enabled  */
    if (((u1TlvType == ETS_RECO_TLV_SUB_TYPE) || (u1TlvType == ETS_TLV_ALL)) &&
        (pPortEntry->u1ETSRecoTxStatus != ETS_DISABLED))
    {
        DCBX_TRC (DCBX_TLV_TRC, "ETSUtlRegisterApplForPort:"
                  "Forming and sending ETS Reco TLV!!!\r\n");
        ETSUtlFormAndSendRecoTLV (pPortEntry, DCBX_LLDP_PORT_REG);
    }
    /* Form and send the TC Supp TLV with Port registration request only if
     * TLV tx status is enabled  */
    if (((u1TlvType == ETS_TC_SUPP_TLV_SUB_TYPE) || (u1TlvType == ETS_TLV_ALL))
        && (pPortEntry->u1TcSupportTxStatus != ETS_DISABLED))
    {
        DCBX_TRC (DCBX_TLV_TRC, "ETSUtlRegisterApplForPort:"
                  "Forming and sending ETS TC SUpp TLV!!!\r\n");
        ETSUtlFormAndSendTCSupportTLV (pPortEntry, DCBX_LLDP_PORT_REG);
    }

    return;
}

/***************************************************************************
 *  FUNCTION NAME : ETSUtlDeRegisterApplForPort
 * 
 *  DESCRIPTION   : This utility is used to De Register the Appl with DCBX.
 * 
 *  INPUT         : pPortEntry - ETS Port Entry.
 *                  u1TlvType - TLV Type - Conf/Reco/TCSupp
 * 
 *  OUTPUT        : None
 * 
 *  RETURNS       : None
 * 
 * **************************************************************************/
PUBLIC VOID
ETSUtlDeRegisterApplForPort (tETSPortEntry * pPortEntry, UINT1 u1TlvType)
{

    tDcbxAppRegInfo     DcbxAppPortMsg;
    /* If Module is not enabled then no need to De-register the application
     * with DCBX and it should have already De-registered for all ports during 
     * the module disable */
    if (gETSGlobalInfo.u1ETSModStatus != ETS_ENABLED)
    {
        DCBX_TRC (DCBX_CONTROL_PLANE_TRC, "ETSUtlDeRegisterApplForPort:"
                  " No DeRegistration Required as the Module is"
                  "not enabled!!!\r\n");
        return;
    }
    /* When the Admin mode of the port is not set to Auto then no need to 
     * De-register any application with DCBX since ETS is not operating
     * in the DCBX AUTO mode */

    if (pPortEntry->u1ETSAdminMode != ETS_PORT_MODE_AUTO)
    {
        DCBX_TRC (DCBX_CONTROL_PLANE_TRC, "ETSUtlDeRegisterApplForPort:"
                  " No DeRegistration Required as the Mode is not Auto!!!\r\n");
        return;
    }
    /* De-Registers the ETS Conf TLV with DCBX if TLV Tx status is enabled,
     * since if it is not enabled then it would not have registered with DCBX */
    if (((u1TlvType == ETS_CONF_TLV_SUB_TYPE) || (u1TlvType == ETS_TLV_ALL)) &&
        (pPortEntry->u1ETSConfigTxStatus != ETS_DISABLED))
    {
        DCBX_TRC (DCBX_TLV_TRC, "ETSUtlDeRegisterApplForPort:"
                  " Sending ETS Conf  TLV DeRegistration Message!!!\r\n");
        MEMSET (&DcbxAppPortMsg, DCBX_ZERO, sizeof (tDcbxAppRegInfo));
        ETS_GET_CONF_APPL_ID (DcbxAppPortMsg.DcbxAppId);
        ETSPortPostMsgToDCBX (DCBX_LLDP_PORT_DEREG, pPortEntry->u4IfIndex,
                              &DcbxAppPortMsg);
        /*Clearing Remote Params */
        ETSUtlClearRemoteParam (pPortEntry, ETS_CONF_TLV_SUB_TYPE);

    }
    /* De-Registers the ETS Reco TLV with DCBX if TLV Tx status is enabled,
     * since if it is not enabled then it would not have registered with DCBX */
    if (((u1TlvType == ETS_RECO_TLV_SUB_TYPE) || (u1TlvType == ETS_TLV_ALL)) &&
        (pPortEntry->u1ETSRecoTxStatus != ETS_DISABLED))
    {
        DCBX_TRC (DCBX_TLV_TRC, "ETSUtlDeRegisterApplForPort:"
                  " Sending ETS Reco TLV DeRegistration Message!!!\r\n");
        MEMSET (&DcbxAppPortMsg, DCBX_ZERO, sizeof (tDcbxAppRegInfo));
        ETS_GET_RECO_APPL_ID (DcbxAppPortMsg.DcbxAppId);
        ETSPortPostMsgToDCBX (DCBX_LLDP_PORT_DEREG, pPortEntry->u4IfIndex,
                              &DcbxAppPortMsg);
        ETSUtlClearRemoteParam (pPortEntry, ETS_RECO_TLV_SUB_TYPE);
    }
    /* De-Registers the ETS TC Supp TLV with DCBX if TLV Tx status is enabled,
     * since if it is not enabled then it would not have registered with DCBX */
    if (((u1TlvType == ETS_TC_SUPP_TLV_SUB_TYPE) || (u1TlvType == ETS_TLV_ALL))
        && (pPortEntry->u1TcSupportTxStatus != ETS_DISABLED))
    {
        DCBX_TRC (DCBX_TLV_TRC, "ETSUtlDeRegisterApplForPort:"
                  " Sending ETS Tc Supp TLV DeRegistration Message!!!\r\n");
        MEMSET (&DcbxAppPortMsg, DCBX_ZERO, sizeof (tDcbxAppRegInfo));
        ETS_GET_TC_SUPP_APPL_ID (DcbxAppPortMsg.DcbxAppId);
        ETSPortPostMsgToDCBX (DCBX_LLDP_PORT_DEREG, pPortEntry->u4IfIndex,
                              &DcbxAppPortMsg);
        ETSUtlClearRemoteParam (pPortEntry, ETS_TC_SUPP_TLV_SUB_TYPE);
    }
    return;
}

/***************************************************************************
 *  FUNCTION NAME : ETSUtlModuleStatusChange
 * 
 *  DESCRIPTION   : This utility is used to handle the Module status change.
 * 
 *  INPUT         : u1Status - Module status -En/Dis
 * 
 *  OUTPUT        : None
 * 
 *  RETURNS       : None
 * 
 * **************************************************************************/
PUBLIC VOID
ETSUtlModuleStatusChange (UINT1 u1Status)
{
    tETSPortEntry       TempPortEntry;
    tETSPortEntry      *pPortEntry = NULL;
    tDcbxTrapMsg        NotifyInfo;

    MEMSET (&TempPortEntry, DCBX_ZERO, sizeof (tETSPortEntry));
    MEMSET (&NotifyInfo, DCBX_ZERO, sizeof (tDcbxTrapMsg));

    pPortEntry = (tETSPortEntry *) RBTreeGetFirst
        (gETSGlobalInfo.pRbETSPortTbl);
    /* Get all the ETS port entry and process the module status chage */
    if (pPortEntry != NULL)
    {
        /* If Module status is to be enabled then first change the status and 
         * and then process since Registration with DCBX and State change occurs 
         * only if the module status is enabled */
        if (u1Status == ETS_ENABLED)
        {
            gETSGlobalInfo.u1ETSModStatus = ETS_ENABLED;
        }
        do
        {
            /* Register or DeRegister the ETS applications with DCBX for all
             * the port and change the state to INIT or OFF */
            if (u1Status == ETS_ENABLED)
            {
                ETSUtlRegOrDeRegBasedOnMode (pPortEntry, u1Status, ETS_TLV_ALL);
                if (pPortEntry->u1ETSAdminMode != ETS_PORT_MODE_OFF)
                {
                    DCBX_TRC (DCBX_SEM_TRC, "ETSUtlModuleStatusChange:"
                              "Module Enable Changing the state to "
                              "INIT!!!\r\n");

                    ETSUtlOperStateChange (pPortEntry, ETS_OPER_INIT);
                }
            }
            else
            {
                /* DeRegister the ETS applications with DCBX for all 
                 * the port and
                 * chage the state to OFF */
                DCBX_TRC (DCBX_SEM_TRC, "ETSUtlModuleStatusChange:"
                          "Module Disable Changing the state to OFF!!!\r\n");

                ETSUtlOperStateChange (pPortEntry, ETS_OPER_OFF);
                ETSUtlRegOrDeRegBasedOnMode (pPortEntry, u1Status, ETS_TLV_ALL);
            }
            TempPortEntry.u4IfIndex = pPortEntry->u4IfIndex;
            pPortEntry = (tETSPortEntry *) RBTreeGetNext
                (gETSGlobalInfo.pRbETSPortTbl, &TempPortEntry,
                 ETSUtlPortTblCmpFn);
        }
        while (pPortEntry != NULL);
    }
    else
    {
        DCBX_TRC (DCBX_CONTROL_PLANE_TRC, "ETSUtlModuleStatusChange:"
                  " No ETS Port is created!!!\r\n");
    }
    /* Update the Module Status Global Value */
    gETSGlobalInfo.u1ETSModStatus = u1Status;

    NotifyInfo.u1Module = ETS_TRAP;
    NotifyInfo.unTrapMsg.u1ModuleStatus = gETSGlobalInfo.u1ETSModStatus;

    DcbxSendNotification (&NotifyInfo, MODULE_TRAP);
    return;
}

/***************************************************************************
 *  FUNCTION NAME : ETSUtlAdminModeChange
 * 
 *  DESCRIPTION   : This utility is used to Handle the Admin Mode change.
 * 
 *  INPUT         : pPortEntry - ETS Port Entry
 *                  u1NewAdminMode - Admin Mode Value.
 * 
 *  OUTPUT        : None
 * 
 *  RETURNS       : None
 * 
 * **************************************************************************/
PUBLIC VOID
ETSUtlAdminModeChange (tETSPortEntry * pPortEntry, UINT1 u1NewAdminMode)
{
    tDcbxTrapMsg        NotifyInfo;

    MEMSET (&NotifyInfo, DCBX_ZERO, sizeof (tDcbxTrapMsg));
    if (pPortEntry->u1ETSAdminMode == ETS_PORT_MODE_AUTO)
    {
        /* This check is to ensure the Deregistration happens only in 
         * AUTO Mode. So Deregistartion of TLV happens before 
         * updating the Strucuture */

        ETSUtlRegOrDeRegBasedOnMode (pPortEntry, ETS_DISABLED, ETS_TLV_ALL);

        pPortEntry->u1ETSAdminMode = u1NewAdminMode;
    }
    else
    {
        pPortEntry->u1ETSAdminMode = u1NewAdminMode;
    }
    if (u1NewAdminMode == ETS_PORT_MODE_AUTO)
    {
        /* If the NewAdmin mode is AUTO then Register the ETS 
         *   applications with DCBX and change the state to INIT */
        ETSUtlRegOrDeRegBasedOnMode (pPortEntry, ETS_ENABLED, ETS_TLV_ALL);

        DCBX_TRC (DCBX_SEM_TRC, "ETSUtlAdminModeChange:"
                  "Port Mode changing to Auto, Change the state "
                  "to INIT!!!\r\n");

        ETSUtlOperStateChange (pPortEntry, ETS_OPER_INIT);
    }
    else if (u1NewAdminMode == ETS_PORT_MODE_ON)
    {
        /* If the NewAdmin mode is ON then change the state to INIT */
        DCBX_TRC (DCBX_SEM_TRC, "ETSUtlAdminModeChange:"
                  "Port Mode changing to ON, Change the state to INIT!!!\r\n");

        ETSUtlOperStateChange (pPortEntry, ETS_OPER_INIT);
    }
    else
    {
        /* If the NewAdmin mode is OFF then chage the state to OFF
         *      i.e Disable the hardware configurations */
        DCBX_TRC (DCBX_SEM_TRC, "ETSUtlAdminModeChange:"
                  "Port Mode changing to OFF, Change the state to OFF!!!\r\n");

        ETSUtlOperStateChange (pPortEntry, ETS_OPER_OFF);
        SET_DCBX_STATUS (pPortEntry, pPortEntry->u1ETSDcbxSemType,
                         DCBX_RED_ETS_DCBX_STATUS_INFO, DCBX_STATUS_DISABLED);
    }
    /* Send Trap Notification */
    NotifyInfo.u4IfIndex = pPortEntry->u4IfIndex;
    NotifyInfo.u1Module = ETS_TRAP;
    NotifyInfo.unTrapMsg.u1AdminMode = pPortEntry->u1ETSAdminMode;

    DcbxSendNotification (&NotifyInfo, ADMIN_STATUS_TRAP);
    return;
}

/***************************************************************************
 *  FUNCTION NAME : ETSUtlOperStateChange
 * 
 *  DESCRIPTION   : This utility is used to Change the oper state.
 * 
 *  INPUT         : pPortEntry - ETS port Entry.
 *                  u1ETSState - ETS Oper State.
 * 
 *  OUTPUT        : None
 * 
 *  RETURNS       : None
 * 
 * **************************************************************************/
PUBLIC VOID
ETSUtlOperStateChange (tETSPortEntry * pPortEntry, UINT1 u1ETSState)
{
    tDcbxTrapMsg        NotifyInfo;
    UINT1               u1Status = ETS_ENABLED;
    UINT1               u1OperVersion = DCBX_VER_UNKNOWN;
    INT4                i4RetVal = DCBX_ZERO;
    MEMSET (&NotifyInfo, DCBX_ZERO, sizeof (tDcbxTrapMsg));
    /* State change should occur only if Module status is enabled. If module 
     * status is disabled then the state will be always OFF */
    if (gETSGlobalInfo.u1ETSModStatus != ETS_ENABLED)
    {
        DCBX_TRC (DCBX_SEM_TRC, "ETSUtlOperStateChange:"
                  "Module status is disbaled. So State change will not happen"
                  "It will be in OFF state !!!\r\n");
        return;
    }
    /* If the Old Oper state and New Oper state are same then no need
     * to process further */
    if ((pPortEntry->u1ETSDcbxOperState == u1ETSState) &&
        (pPortEntry->u1ETSDcbxOperState != ETS_OPER_RECO))
    {
        DCBX_TRC (DCBX_SEM_TRC, "ETSUtlOperStateChange:"
                  "Both old and new state are same !!!\r\n");
        return;
    }
    DcbxUtlGetOperVersion (pPortEntry->u4IfIndex, &u1OperVersion);
    /* Update the Local Parameters which are operation parameters
     * according to the state */
    DCBX_TRC (DCBX_SEM_TRC, "ETSUtlOperStateChange:"
              "Updating the Local Paramters based on the state !!!\r\n");
    if (u1OperVersion == DCBX_VER_CEE)
    {
        CEEUtlUpdatETSLocalParam (pPortEntry, u1ETSState);
    }
    else
    {
        ETSUtlUpdateLocalParam (pPortEntry, u1ETSState);
    }

    if ((u1ETSState == ETS_OPER_INIT) || (u1ETSState == ETS_OPER_RECO))
    {
        /* If the state is either INIT and RX then Hardware configurations 
         * should be enabled */
        u1Status = ETS_ENABLED;
    }
    else
    {
        /* If the state is OFF then Hardware confgirations should be disabled */
        u1Status = ETS_DISABLED;
    }
    pPortEntry->u1ETSDcbxOperState = u1ETSState;
    /* Configure the hardware */

    if (u1OperVersion == DCBX_VER_CEE)
    {
        i4RetVal = CeeUtlETSHwConfig (pPortEntry, u1Status);
    }
    else
    {
        i4RetVal = ETSUtlHwConfig (pPortEntry, u1Status);
    }
    UNUSED_PARAM (i4RetVal);

    /* Send Trap Notification */
    NotifyInfo.u4IfIndex = pPortEntry->u4IfIndex;
    NotifyInfo.u1Module = ETS_TRAP;
    NotifyInfo.unTrapMsg.u1OperState = pPortEntry->u1ETSDcbxOperState;
    DcbxSendNotification (&NotifyInfo, OPER_STATE_TRAP);
    return;
}

/***************************************************************************
 *  FUNCTION NAME : ETSUtlUpdateLocalParam
 * 
 *  DESCRIPTION   : This utility is used to update the local parameter.
 * 
 *  INPUT         : pPortEntry - ETS Port Entry.
 *                  u1ETSState - ETS oper State.
 * 
 *  OUTPUT        : None
 * 
 *  RETURNS       : None
 * 
 * **************************************************************************/
PUBLIC VOID
ETSUtlUpdateLocalParam (tETSPortEntry * pPortEntry, UINT1 u1ETSState)
{
    if ((u1ETSState == ETS_OPER_INIT) || (u1ETSState == ETS_OPER_OFF))
    {
        /* If the state is either INIT or OFF then local parameters will be 
         * same as Admin configuration parameters */
        MEMCPY (pPortEntry->ETSLocPortInfo.au1ETSLocBW,
                pPortEntry->ETSAdmPortInfo.au1ETSAdmBW, ETS_MAX_NUM_TCGID);

        MEMCPY (pPortEntry->ETSLocPortInfo.au1ETSLocRecoBW,
                pPortEntry->ETSAdmPortInfo.au1ETSAdmRecoBW, ETS_MAX_NUM_TCGID);

        MEMCPY (pPortEntry->ETSLocPortInfo.au1ETSLocTCGID,
                pPortEntry->ETSAdmPortInfo.au1ETSAdmTCGID, DCBX_MAX_PRIORITIES);

        MEMCPY (pPortEntry->ETSLocPortInfo.au1ETSLocRecoTsaTable,
                pPortEntry->ETSAdmPortInfo.au1ETSAdmRecoTsaTable,
                ETS_MAX_TCGID_CONF);

        MEMCPY (pPortEntry->ETSLocPortInfo.au1ETSLocTsaTable,
                pPortEntry->ETSAdmPortInfo.au1ETSAdmTsaTable,
                ETS_MAX_TCGID_CONF);

        pPortEntry->ETSLocPortInfo.u1ETSLocNumTCGSup =
            pPortEntry->ETSAdmPortInfo.u1ETSAdmNumTCGSup;
        pPortEntry->ETSLocPortInfo.u1ETSLocNumTcSup =
            pPortEntry->ETSAdmPortInfo.u1ETSAdmNumTcSup;

    }
    else
    {
        /* If the state is RX then local parametrs will be same as
         * Remote parameters */

        MEMCPY (pPortEntry->ETSLocPortInfo.au1ETSLocBW,
                pPortEntry->ETSRemPortInfo.au1ETSRemBW, ETS_MAX_NUM_TCGID);

        MEMCPY (pPortEntry->ETSLocPortInfo.au1ETSLocTCGID,
                pPortEntry->ETSRemPortInfo.au1ETSRemRecoTCGID,
                DCBX_MAX_PRIORITIES);

        MEMCPY (pPortEntry->ETSLocPortInfo.au1ETSLocTsaTable,
                pPortEntry->ETSRemPortInfo.au1ETSRemTsaTable,
                DCBX_MAX_PRIORITIES);

        pPortEntry->ETSLocPortInfo.u1ETSLocNumTCGSup =
            pPortEntry->ETSRemPortInfo.u1ETSRemNumTCGSup;

        /* Local Recommended BW will get updated with Remote Recommended BW
         * only if Reco TLV updation happened already */
        if (pPortEntry->u1RemTlvUpdStatus & ETS_RECO_TLV_REM_UPD)
        {
            MEMCPY (pPortEntry->ETSLocPortInfo.au1ETSLocRecoBW,
                    pPortEntry->ETSRemPortInfo.au1ETSRemRecoBW,
                    ETS_MAX_NUM_TCGID);

        }

        /* Local Recommended TSA table will get updated with Remote Recommended
         * TSA table only if Reco TLV updation happened already */
        if (pPortEntry->u1RemTlvUpdStatus & ETS_RECO_TLV_REM_UPD)
        {
            MEMCPY (pPortEntry->ETSLocPortInfo.au1ETSLocRecoTsaTable,
                    pPortEntry->ETSRemPortInfo.au1ETSRemRecoTsaTable,
                    ETS_MAX_TCGID_CONF);

        }

        /* Local TC Supported will get updated with Remote TC Supoprted
         * only if TC Supp TLV updation happened already */
        if (pPortEntry->u1RemTlvUpdStatus & ETS_TC_SUPP_TLV_REM_UPD)
        {
            pPortEntry->ETSLocPortInfo.u1ETSLocNumTcSup =
                pPortEntry->ETSRemPortInfo.u1ETSRemNumTcSup;
        }
        /* Send the TLV Update since Local Willing has been changed */
        ETSUtlFormAndSendConfigTLV (pPortEntry, DCBX_LLDP_PORT_UPDATE);

    }
}

/***************************************************************************
 *  FUNCTION NAME : ETSUtlTlvStatusChange
 * 
 *  DESCRIPTION   : This utility is used to handle the ETS TLV Tx status 
 *                  change.
 * 
 *  INPUT         : pPortEntry - ETS Port Entry.
 *                  u1NewTlvStatus - TLV Tx Status.
 *                  u1TlvType - TLV Type -Conf/Reco/TCSupp
 * 
 *  OUTPUT        : None
 * 
 *  RETURNS       : None
 * 
 * **************************************************************************/
PUBLIC VOID
ETSUtlTlvStatusChange (tETSPortEntry * pPortEntry,
                       UINT1 u1NewTlvStatus, UINT1 u1TlvType)
{
    UINT1               u1DcbxMode = 0;

    DcbxUtlGetDCBXMode (pPortEntry->u4IfIndex, &u1DcbxMode);

    switch (u1TlvType)
    {
        case ETS_CONF_TLV_SUB_TYPE:
            if (u1NewTlvStatus == ETS_ENABLED)
            {
                /* If the TLV TX new status is enabled then update 
                 * the data structure and send the Registration request
                 * with DCBX since Registration will happen only if
                 * TX status is enabled */
                pPortEntry->u1ETSConfigTxStatus = ETS_ENABLED;
                ETSUtlRegOrDeRegBasedOnMode (pPortEntry, u1NewTlvStatus,
                                             u1TlvType);
            }
            else
            {
                /* Deregister the application with DCBX and change the 
                 * TLV Tx status since deregistration happnes only if
                 * the Tx status is already enabled. Change the state 
                 * to INIT if the state is in Reco */

                ETSUtlRegOrDeRegBasedOnMode (pPortEntry, u1NewTlvStatus,
                                             u1TlvType);
                pPortEntry->u1ETSConfigTxStatus = ETS_DISABLED;
                if (pPortEntry->u1ETSDcbxOperState == ETS_OPER_RECO)
                {
                    DCBX_TRC (DCBX_SEM_TRC, "ETSUtlTlvStatusChange:"
                              "Since Conf TLV Tx status is disbaled,"
                              "changing the state to INIT from RECO  !!!\r\n");
                    ETSUtlOperStateChange (pPortEntry, ETS_OPER_INIT);
                }
            }
            break;
        case ETS_RECO_TLV_SUB_TYPE:
            if (u1NewTlvStatus == ETS_ENABLED)
            {
                /* If the TLV TX new status is enabled then update 
                 * the data structure and send the Registration request
                 * with DCBX since Registration will happen only if
                 * TX status is enabled */
                pPortEntry->u1ETSRecoTxStatus = ETS_ENABLED;
                ETSUtlRegOrDeRegBasedOnMode (pPortEntry, u1NewTlvStatus,
                                             u1TlvType);
            }
            else
            {
                /* Deregister the application with DCBX and change the 
                 * TLV Tx status since deregistration happnes only if
                 * the Tx status is already enabled. Change the state 
                 * to INIT */
                ETSUtlRegOrDeRegBasedOnMode (pPortEntry, u1NewTlvStatus,
                                             u1TlvType);
                pPortEntry->u1ETSRecoTxStatus = ETS_DISABLED;
                if (pPortEntry->u1ETSDcbxOperState == ETS_OPER_RECO)
                {
                    DCBX_TRC (DCBX_SEM_TRC, "ETSUtlTlvStatusChange:"
                              "Since RECO TLV Tx status is disbaled,"
                              "changing the state to INIT from RECO  !!!\r\n");
                    ETSUtlOperStateChange (pPortEntry, ETS_OPER_INIT);
                }
            }
            break;
        case ETS_TC_SUPP_TLV_SUB_TYPE:
            if (u1NewTlvStatus == ETS_ENABLED)
            {
                /* If the TLV TX new status is enabled then update 
                 * the data structure and send the Registration request
                 * with DCBX since Registration will happen only if
                 * TX status is enabled */
                pPortEntry->u1TcSupportTxStatus = ETS_ENABLED;
                ETSUtlRegOrDeRegBasedOnMode (pPortEntry, u1NewTlvStatus,
                                             u1TlvType);
            }
            else
            {
                /* Deregister the application with DCBX and change the 
                 * TLV Tx status since deregistration happnes only if
                 * the Tx status is already enabled. Change the state 
                 * to INIT */
                ETSUtlRegOrDeRegBasedOnMode (pPortEntry, u1NewTlvStatus,
                                             u1TlvType);
                pPortEntry->u1TcSupportTxStatus = ETS_DISABLED;
            }
            break;
        default:
            break;
    }

    return;
}

/***************************************************************************
 *  FUNCTION NAME : ETSUtlAdminParamChange
 * 
 *  DESCRIPTION   : This utility is used to handle the Admin Parameters
 *                  Change. It will indicate to the DCBX if Admin mode is 
 *                  Auto.
 * 
 *  INPUT         : pPortEntry - ETS Port Entry.
 *                  u1TlvType - TLV Type - Conf/Reco/TcSupp
 * 
 *  OUTPUT        : None
 * 
 *  RETURNS       : None
 * 
 * **************************************************************************/
PUBLIC VOID
ETSUtlAdminParamChange (tETSPortEntry * pPortEntry, UINT1 u1TlvType)
{
    UINT1               u1DcbxVersion = DCBX_VER_UNKNOWN;

    /* If Module status is disabled then no need to send the Admin Param
     * channge indication to the DCBX since the application itself would have 
     * Registered with DCBX during the Module Disable.But Need to update 
     * the Local Parmaters*/

    if (pPortEntry->u1ETSDcbxOperState == ETS_OPER_OFF)
    {
        /* Update the Local parameters and no need to configure the hardware
         * since oper state is OFF */
        if (u1TlvType == ETS_CEE_TLV_SUB_TYPE)
        {
            CEEUtlUpdatETSLocalParam (pPortEntry, ETS_OPER_OFF);
        }
        else
        {
            ETSUtlUpdateLocalParam (pPortEntry, ETS_OPER_OFF);
            /* If the operating version is IEEE and if the mode is OFF
             * no need to send the updated TLV hence return from here. */
            return;
        }
    }

    /* If Oper state is Reco then no need to update the local paramters as 
     * well as hardware since it will take remote parameters.
     * If Oper state is in INIT then update the Local paramters */
    if (pPortEntry->u1ETSDcbxOperState == ETS_OPER_INIT)
    {
        if (u1TlvType == ETS_CEE_TLV_SUB_TYPE)
        {
            CEEUtlUpdatETSLocalParam (pPortEntry, ETS_OPER_INIT);
        }
        else
        {
            ETSUtlUpdateLocalParam (pPortEntry, ETS_OPER_INIT);
        }
        /* If TLV type is Conf then update the hardware since Conf TLV type
         * alone carries the necessary information required for updation 
         * in the INIT state */
        if (u1TlvType == ETS_CONF_TLV_SUB_TYPE)
        {
            if (ETSUtlHwConfig (pPortEntry, ETS_ENABLED) != OSIX_SUCCESS)
            {
                DCBX_TRC (DCBX_FAILURE_TRC, "ETSUtlAdminParamChange:"
                          "Programmin the ETS paramters in the"
                          " hardware FAILED !!!\r\n");
            }
        }
        else if (u1TlvType == ETS_CEE_TLV_SUB_TYPE)
        {
            if (CeeUtlETSHwConfig (pPortEntry, ETS_ENABLED) != OSIX_SUCCESS)
            {
                DCBX_TRC (DCBX_FAILURE_TRC, "ETSUtlAdminParamChange:"
                          "Programmin the ETS paramters in the"
                          " hardware FAILED !!!\r\n");
            }
        }
    }
    /* If the Admin mode is AUTO then need to send the Admin Param
     * change indication to the DCBX with the TLV Updates  */
    if ((pPortEntry->u1ETSAdminMode == ETS_PORT_MODE_AUTO) &&
        (pPortEntry->u1ETSConfigTxStatus != ETS_DISABLED))
    {
        DcbxUtlGetOperVersion (pPortEntry->u4IfIndex, &u1DcbxVersion);

        /* ETS Recommendation TLV not included in CEE DCBX frames */
        if ((u1DcbxVersion == DCBX_VER_CEE) &&
            (u1TlvType == ETS_CEE_TLV_SUB_TYPE))
        {
            DCBX_TRC_ARG1 (DCBX_TLV_TRC, "ETSUtlAdminParamChange: Change in "
                           "Admn params for port[%d]. Form and Send CEE TLV!!!\r\n",
                           pPortEntry->u4IfIndex);
            CEEFormAndSendTLV (pPortEntry->u4IfIndex, DCBX_LLDP_PORT_UPDATE,
                               DCBX_ZERO);
        }
        else if (u1DcbxVersion == DCBX_VER_IEEE)
        {
            /* If the TLV Tx status is not enabled then no need to send the Admin Param
             * change indication */
            if ((u1TlvType == ETS_CONF_TLV_SUB_TYPE) &&
                (pPortEntry->u1ETSConfigTxStatus != ETS_DISABLED))
            {
                DCBX_TRC (DCBX_TLV_TRC, "ETSUtlAdminParamChange:"
                          "Forming and Sending the ETS Conf TLV!!!\r\n");

                ETSUtlFormAndSendConfigTLV (pPortEntry, DCBX_LLDP_PORT_UPDATE);

            }
            /* If the TLV Tx status is not enabled then no need to send the Admin Param
             * change indication */
            if ((u1TlvType == ETS_RECO_TLV_SUB_TYPE) &&
                (pPortEntry->u1ETSRecoTxStatus != ETS_DISABLED))
            {
                DCBX_TRC (DCBX_TLV_TRC, "ETSUtlAdminParamChange:"
                          "Forming and Sending the ETS Reco TLV!!!\r\n");
                ETSUtlFormAndSendRecoTLV (pPortEntry, DCBX_LLDP_PORT_UPDATE);
            }
            /* If the TLV Tx status is not enabled then no need to send the Admin Param
             * change indication */
            if ((u1TlvType == ETS_TC_SUPP_TLV_SUB_TYPE) &&
                (pPortEntry->u1TcSupportTxStatus != ETS_DISABLED))
            {
                DCBX_TRC (DCBX_TLV_TRC, "ETSUtlAdminParamChange:"
                          "Forming and Sending the TC Supp TLV!!!\r\n");
                ETSUtlFormAndSendTCSupportTLV (pPortEntry,
                                               DCBX_LLDP_PORT_UPDATE);
            }
        }
    }
    return;
}

/***************************************************************************
 *  FUNCTION NAME : ETSUtlWillingChange
 * 
 *  DESCRIPTION   : This utility is used to handle the willing parameter
 *                  change.
 * 
 *  INPUT         : pPortEntry - ETS Port Entry.
 *                  u1IsLoca1Willing - Whether Local willing change or not.
 *                  If local willing then send the updated TLV.
 * 
 *  OUTPUT        : None
 * 
 *  RETURNS       : None
 * 
 * **************************************************************************/
PUBLIC VOID
ETSUtlWillingChange (tETSPortEntry * pPortEntry, UINT1 u1IsLoca1Willing)
{
    /* This will be called whenever there is a change in 
     * Remote/Local Willing Status */
    tDcbxAppRegInfo     DcbxAppPortMsg;
    UINT1               u1OperState = DCBX_ZERO;
    UINT1               u1DcbxVersion = DCBX_VER_UNKNOWN;

    MEMSET (&DcbxAppPortMsg, DCBX_ZERO, sizeof (tDcbxAppRegInfo));
    /* If Module status is disbaled then no need to send the 
     * state machine chage request to DCBX since remote paramters 
     * will not be present and the application would not have registered
     * with DCBX in Module Disable state*/
    if (gETSGlobalInfo.u1ETSModStatus != ETS_ENABLED)
    {
        DCBX_TRC (DCBX_SEM_TRC, "ETSUtlWillingChange:"
                  " No State machine change required as the "
                  "Module is not enabled!!!\r\n");
        return;
    }
    /* If the Admin mode is not AUTO then no need to send the state
     * machine chage request since it does not operate on DCBX */
    if (pPortEntry->u1ETSAdminMode != ETS_PORT_MODE_AUTO)
    {
        DCBX_TRC (DCBX_SEM_TRC, "ETSUtlWillingChange:"
                  " No State machine change required as the"
                  " Mode is not Auto!!!\r\n");
        return;
    }
    /* If TLV Tx status is not enabled then no need to send the state
     * machine chage since it would have registered with DCBX */
    if (pPortEntry->u1ETSConfigTxStatus != ETS_ENABLED)
    {
        DCBX_TRC (DCBX_CONTROL_PLANE_TRC, "ETSUtlWillingChange:"
                  "ETS Conf TLV tranmsission status is not enabled."
                  "No SEM change request is needed!!!\r\n");
        return;
    }

    DcbxUtlGetOperVersion (pPortEntry->u4IfIndex, &u1DcbxVersion);
    if (u1DcbxVersion == DCBX_VER_IEEE)
    {
        /* If recommended TLV is not recieved by the peer then 
         * No need to send state machine change request 
         * else send the state machine change request */
        if (pPortEntry->u1RemTlvUpdStatus & ETS_RECO_TLV_REM_UPD)
        {

            DCBX_TRC (DCBX_SEM_TRC, "ETSUtlWillingChange:"
                      " Sending State machine change request message"
                      "to DCBX !!!\r\n");

            /* Fill the Information to be sent to DCBX */
            ETS_GET_CONF_APPL_ID (DcbxAppPortMsg.DcbxAppId);
            DcbxAppPortMsg.u1ApplLocalWilling =
                pPortEntry->ETSLocPortInfo.u1ETSLocWilling;
            DcbxAppPortMsg.u1ApplRemoteWilling =
                pPortEntry->ETSRemPortInfo.u1ETSRemWilling;
            DcbxAppPortMsg.u1ApplStateMachineType = DCBX_ASYM_STATE_MACHINE;
            /* Post the mesage to DCBX for State machine chage request */
            ETSPortPostMsgToDCBX (DCBX_STATE_MACHINE, pPortEntry->u4IfIndex,
                                  &DcbxAppPortMsg);
        }
        /* If there is a change in local willing then send the updated TLV */
        if (u1IsLoca1Willing == OSIX_TRUE)
        {
            DCBX_TRC (DCBX_TLV_TRC, "ETSUtlWillingChange:"
                      "Forming and sending ETS Conf TLV since Local willing status"
                      "has been changed !!!\r\n");

            /* Send the TLV Update since Local Willing has been changed */
            ETSUtlFormAndSendConfigTLV (pPortEntry, DCBX_LLDP_PORT_UPDATE);
        }
    }
    else if (u1DcbxVersion == DCBX_VER_CEE)
    {

        DCBX_TRC_ARG2 (DCBX_TLV_TRC, "ETSUtlWillingChange: "
                       "CEE: Calculate Oper state "
                       "RemoteWilling = %d, LocalWilling = %d \r\n",
                       pPortEntry->ETSRemPortInfo.u1ETSRemWilling,
                       u1IsLoca1Willing);
        u1OperState =
            CEEUtlCalculateOperState (pPortEntry->ETSRemPortInfo.
                                      u1ETSRemWilling, u1IsLoca1Willing,
                                      DCBX_SUCCESS);
        if ((u1OperState != pPortEntry->u1ETSDcbxOperState)
            || (u1IsLoca1Willing == OSIX_TRUE))
        {
            DCBX_TRC_ARG1 (DCBX_TLV_TRC, "ETSUtlWillingChange: "
                           "Form and send CEE TLV since Local Oper state"
                           "(or) Local willing status has been changed for port [%d]!!!\r\n",
                           pPortEntry->u4IfIndex);
            CEEFormAndSendTLV (pPortEntry->u4IfIndex, DCBX_LLDP_PORT_UPDATE,
                               DCBX_ZERO);
        }
    }
    return;
}

/***************************************************************************
 *  FUNCTION NAME : ETSUtlFormAndSendConfigTLV
 * 
 *  DESCRIPTION   : This utility is used to from and send Conf TLV
 * 
 *  INPUT         : pPortEntry - ETS Port Entry.
 *                  u1MsgType - Message Type - Port Reg/Port Update.
 * 
 *  OUTPUT        : None
 * 
 *  RETURNS       : None
 * 
 * **************************************************************************/
PUBLIC VOID
ETSUtlFormAndSendConfigTLV (tETSPortEntry * pPortEntry, UINT1 u1MsgType)
{
    UINT1               au1TlvBuf[ETS_CONF_TLV_LEN] = { DCBX_ZERO };
    UINT1               u1Value = DCBX_ZERO;
    UINT1              *pu1Temp = NULL;
    tDcbxAppRegInfo     DcbxAppPortMsg;

    MEMSET (&DcbxAppPortMsg, DCBX_ZERO, sizeof (tDcbxAppRegInfo));

    /* Get teh Appication ID to be filled in the DCBX Appl port strucutre */
    ETS_GET_CONF_APPL_ID (DcbxAppPortMsg.DcbxAppId);

    DcbxAppPortMsg.pApplCallBackFn = ETSApiConfTlvCallBackFn;
    DcbxAppPortMsg.u2TlvLen = ETS_CONF_TLV_LEN;

    /* Form the TLV Header with the Preformed TLV */
    pu1Temp = au1TlvBuf;
    MEMCPY (pu1Temp, gau1ETSConfPreFormedTlv, ETS_PREFORMED_TLV_LEN);
    pu1Temp = pu1Temp + ETS_PREFORMED_TLV_LEN;

    /* This byte will be filled in the order 
     *  Willing  CBS   Reserved   Max TCs
     *  1bit     1Bit   3bit       3 bits */

    if (pPortEntry->ETSAdmPortInfo.u1ETSAdmWilling == ETS_ENABLED)
    {
        u1Value = ETS_WILLING_MASK;
    }

    if (pPortEntry->ETSAdmPortInfo.u1ETSAdmCBS == ETS_CBS_SUPPORTED)
    {
        u1Value = u1Value | ETS_CBS_MASK;
    }
    u1Value = u1Value | pPortEntry->ETSAdmPortInfo.u1ETSAdmNumTCGSup;
    DCBX_PUT_1BYTE (pu1Temp, u1Value);

    /* Fill the Priority Assignement paramters for the next 4 octets */
    ETS_FILL_PRI_MAPPING (pu1Temp, pPortEntry->ETSLocPortInfo.au1ETSLocTCGID);

    /* Fill the Bandwidth parameters for the next 8 octets */
    ETS_FILL_TCG_BW (pu1Temp, pPortEntry->ETSLocPortInfo.au1ETSLocBW);

    /* Fill the TSA parameters for the next 8 octets */
    ETS_FILL_TSA_TABLE (pu1Temp, pPortEntry->ETSLocPortInfo.au1ETSLocTsaTable);

    /* Post the mesage to DCBX with message type received */
    DcbxAppPortMsg.pu1ApplTlv = au1TlvBuf;
    DCBX_TRC (DCBX_TLV_TRC, "ETSUtlFormAndSendConfigTLV:"
              " Posting ETS Conf TLV to DCBX!!!\r\n");

    /* update the Config TLV Tx Counters */
    pPortEntry->u4ETSConfTxTLVCount = pPortEntry->u4ETSConfTxTLVCount + 1;
    /* Packet Dump to Console */
    if (gDcbxGlobalInfo.u4DCBXTrcFlag & DCBX_TLV_TRC)
    {
        DcbxPktDump (au1TlvBuf, DcbxAppPortMsg.u2TlvLen, DCBX_TX);
    }

    ETSPortPostMsgToDCBX (u1MsgType, pPortEntry->u4IfIndex, &DcbxAppPortMsg);

    /* Send Counter Update Info to Standby */
    DcbxRedSendDynamicEtsCounterInfo (pPortEntry, DCBX_RED_ETS_CONF_TX_COUNTER);

    return;
}

/***************************************************************************
 *  FUNCTION NAME : ETSUtlFormAndSendRecoTLV
 * 
 *  DESCRIPTION   : This utility is used to form and send Reco TLV
 * 
 *  INPUT         : pPortEntry - ETS Port Entry.
 *                  u1MsgType - Message Type - Port Reg/Port Update.
 * 
 *  OUTPUT        : None
 * 
 *  RETURNS       : None
 * 
 * **************************************************************************/
PUBLIC VOID
ETSUtlFormAndSendRecoTLV (tETSPortEntry * pPortEntry, UINT1 u1MsgType)
{
    UINT1               au1TlvBuf[ETS_RECO_TLV_LEN] = { DCBX_ZERO };
    UINT1               u1Reserv = DCBX_ZERO;
    UINT1              *pu1Temp = NULL;
    tDcbxAppRegInfo     DcbxAppPortMsg;

    MEMSET (&DcbxAppPortMsg, DCBX_ZERO, sizeof (tDcbxAppRegInfo));

    /* Get teh Appication ID to be filled in the DCBX Appl port strucutre */
    ETS_GET_RECO_APPL_ID (DcbxAppPortMsg.DcbxAppId);

    DcbxAppPortMsg.pApplCallBackFn = ETSApiRecoTlvCallBackFn;
    DcbxAppPortMsg.u2TlvLen = ETS_RECO_TLV_LEN;
    /* Form the TLV Header with the Preformed TLV */
    pu1Temp = au1TlvBuf;
    MEMCPY (pu1Temp, gau1ETSRecoPreFormedTlv, ETS_PREFORMED_TLV_LEN);
    pu1Temp = pu1Temp + ETS_PREFORMED_TLV_LEN;

    /* This byte will be filled in the order 
     *     Reserved  - 8bits */

    DCBX_PUT_1BYTE (pu1Temp, u1Reserv);

    /* Fill the Priority Assignement paramters for the next 4 octets */
    ETS_FILL_PRI_MAPPING (pu1Temp, pPortEntry->ETSAdmPortInfo.au1ETSAdmTCGID);
    /* Fill the Recommended Bandwidth parameters for the next 8 octets */
    ETS_FILL_TCG_BW (pu1Temp, pPortEntry->ETSAdmPortInfo.au1ETSAdmRecoBW);

    /* Fill the Recommended TSA table parameters for the next 8 octets */
    ETS_FILL_TSA_TABLE (pu1Temp,
                        pPortEntry->ETSAdmPortInfo.au1ETSAdmRecoTsaTable);

    /* Post the mesage to DCBX with message type received */
    DcbxAppPortMsg.pu1ApplTlv = au1TlvBuf;
    DCBX_TRC (DCBX_TLV_TRC, "ETSUtlFormAndSendRecoTLV:"
              " Posting ETS Reco TLV to DCBX!!!\r\n");

    /* Update the Recomendation TLV Tx Counters */
    pPortEntry->u4ETSRecoTxTLVCount = pPortEntry->u4ETSRecoTxTLVCount + 1;

    /* Packet Dump to Console */
    if (gDcbxGlobalInfo.u4DCBXTrcFlag & DCBX_TLV_TRC)
    {
        DcbxPktDump (au1TlvBuf, DcbxAppPortMsg.u2TlvLen, DCBX_TX);
    }

    ETSPortPostMsgToDCBX (u1MsgType, pPortEntry->u4IfIndex, &DcbxAppPortMsg);

    /* Send Counter Update Info to Standby */
    DcbxRedSendDynamicEtsCounterInfo (pPortEntry, DCBX_RED_ETS_RECO_TX_COUNTER);
    return;
}

/***************************************************************************
 *  FUNCTION NAME : ETSUtlFormAndSendTcSupportTLV
 * 
 *  DESCRIPTION   : This utility is used to from and send TcSupp TLV
 * 
 *  INPUT         : pPortEntry - ETS Port Entry.
 *                  u1MsgType - Message Type - Port Reg/Port Update.
 * 
 *  OUTPUT        : None
 * 
 *  RETURNS       : None
 * 
 * **************************************************************************/
PUBLIC VOID
ETSUtlFormAndSendTCSupportTLV (tETSPortEntry * pPortEntry, UINT1 u1MsgType)
{
    UINT1               au1TlvBuf[ETS_TC_SUPP_TLV_LEN] = { DCBX_ZERO };
    UINT1              *pu1Temp = NULL;
    tDcbxAppRegInfo     DcbxAppPortMsg;

    MEMSET (au1TlvBuf, DCBX_ZERO, ETS_TC_SUPP_TLV_LEN);
    MEMSET (&DcbxAppPortMsg, DCBX_ZERO, sizeof (tDcbxAppRegInfo));

    /* Get teh Appication ID to be filled in the DCBX Appl port strucutre */
    ETS_GET_TC_SUPP_APPL_ID (DcbxAppPortMsg.DcbxAppId);

    DcbxAppPortMsg.pApplCallBackFn = ETSApiTcSuppTlvCallBackFn;
    DcbxAppPortMsg.u2TlvLen = ETS_TC_SUPP_TLV_LEN;
    /* Form the TLV Header with the Preformed TLV */
    pu1Temp = au1TlvBuf;
    MEMCPY (pu1Temp, gau1ETSTcSuppPreFormedTlv, ETS_PREFORMED_TLV_LEN);
    pu1Temp = pu1Temp + ETS_PREFORMED_TLV_LEN;

    /* This byte will be filled in the order 
     *     Reserved  -      TC SUppp
     *         5bits 3 bits     */

    DCBX_PUT_1BYTE (pu1Temp, pPortEntry->ETSAdmPortInfo.u1ETSAdmNumTcSup);
    /* Post the mesage to DCBX with message type received */
    DcbxAppPortMsg.pu1ApplTlv = au1TlvBuf;
    DCBX_TRC (DCBX_TLV_TRC, "ETSUtlFormAndSendTCSupportTLV:"
              " Posting ETS TC Supp TLV to DCBX!!!\r\n");

    /* Update the TC Support TLV Tx Counters */
    pPortEntry->u4ETSTcSuppTxTLVCount = pPortEntry->u4ETSTcSuppTxTLVCount + 1;

    /* Packet Dump to Console */
    if (gDcbxGlobalInfo.u4DCBXTrcFlag & DCBX_TLV_TRC)
    {
        DcbxPktDump (au1TlvBuf, DcbxAppPortMsg.u2TlvLen, DCBX_TX);
    }

    ETSPortPostMsgToDCBX (u1MsgType, pPortEntry->u4IfIndex, &DcbxAppPortMsg);

    /* Send Counter Update Info to Standby */
    DcbxRedSendDynamicEtsCounterInfo (pPortEntry,
                                      DCBX_RED_ETS_TC_SUPP_TX_COUNTER);
    return;
}

/***************************************************************************
 *  FUNCTION NAME : ETSUtlHandleConfigTLV
 * 
 *  DESCRIPTION   : This utility is used to handle and process the Conf TLV.
 * 
 *  INPUT         : pPortEntry -  ETS Port Entry.
 *                  ApplTlvParam - Appl TLV information.
 * 
 *  OUTPUT        : None
 * 
 *  RETURNS       : None
 * 
 * **************************************************************************/
PUBLIC VOID
ETSUtlHandleConfigTLV (tETSPortEntry * pPortEntry,
                       tApplTlvParam * pApplTlvParam)
{
    UINT4               u4PriMapping = DCBX_ZERO;
    INT4                i4Compatibility = DCBX_ZERO;
    UINT1               u1Value = DCBX_ZERO;
    UINT1              *pu1Temp = NULL;
    UINT1               u1RemMaxTCG = DCBX_ZERO;
    UINT1               u1LocalMaxTCG = DCBX_ZERO;
    UINT1               u1Willing = ETS_DISABLED;
    UINT1               u1CBS = ETS_CBS_NOT_SUPPORTED;
    UINT1               au1EtsTCGID[DCBX_MAX_PRIORITIES] = { DCBX_ZERO };
    UINT1               au1EtsBW[ETS_MAX_TCGID_CONF] = { DCBX_ZERO };
    UINT1               au1EtsTsaTable[ETS_MAX_TCGID_CONF] = { DCBX_ZERO };
    tDcbxTrapMsg        NotifyInfo;

    MEMSET (&NotifyInfo, DCBX_ZERO, sizeof (tDcbxTrapMsg));
    SET_DCBX_STATUS (pPortEntry, pPortEntry->u1ETSDcbxSemType,
                     DCBX_RED_ETS_DCBX_STATUS_INFO, DCBX_STATUS_UNKNOWN);

    if (pApplTlvParam->u2RxTlvLen != ETS_CONF_TLV_LEN)
    {
        /* If TLV length is not correct, then do not process the TLV */
        DCBX_TRC (DCBX_TLV_TRC | DCBX_FAILURE_TRC, "ETSUtlHandleConfigTLV:"
                  "ETS Conf Received TLV length is incorrect !!!\r\n");
        /* Update received Error Config TLV Counters */
        pPortEntry->u4ETSConfRxTLVError = pPortEntry->u4ETSConfRxTLVError + 1;

        /* Send Counter Update Info to Standby */
        DcbxRedSendDynamicEtsCounterInfo (pPortEntry,
                                          DCBX_RED_ETS_CONF_ERR_COUNTER);

        return;
    }

    pu1Temp = pApplTlvParam->au1DcbxTlv;
    if (MEMCMP (pu1Temp, gau1ETSConfPreFormedTlv, ETS_PREFORMED_TLV_LEN)
        != DCBX_ZERO)
    {
        /* If the Header is not correct then do not process the TLV  */
        DCBX_TRC (DCBX_TLV_TRC | DCBX_FAILURE_TRC, "ETSUtlHandleConfigTLV:"
                  "ETS Conf TLV Received Header is incorrect !!!\r\n");
        /* Update received Error Config TLV Counters */
        pPortEntry->u4ETSConfRxTLVError = pPortEntry->u4ETSConfRxTLVError + 1;

        /* Send Counter Update Info to Standby */
        DcbxRedSendDynamicEtsCounterInfo (pPortEntry,
                                          DCBX_RED_ETS_CONF_ERR_COUNTER);

        return;
    }

    pu1Temp = pu1Temp + ETS_PREFORMED_TLV_LEN;

    /* Get the TLV information from the Application TLV paramters */
    DCBX_GET_1BYTE (pu1Temp, u1Value);

    if (u1Value & ETS_WILLING_MASK)
    {
        u1Willing = ETS_ENABLED;
    }
    if (u1Value & ETS_CBS_MASK)
    {
        u1CBS = ETS_ENABLED;
    }

    u1RemMaxTCG = u1Value & ETS_MAX_TCG_MASK;

    /* Get the Priorrity assignment Octets of length 4 */
    DCBX_GET_4BYTE (pu1Temp, u4PriMapping);

    /* Copy the Priority Assignment table from TLV */
    ETS_FILL_PRI_MAPPING_FROM_TLV (u4PriMapping, au1EtsTCGID);

    /* Copy the Bandwidht paramters from TLV */
    ETS_FILL_TCG_BW_FROM_TLV (pu1Temp, au1EtsBW);

    /* Copy the Bandwidht paramters from TLV */
    ETS_FILL_TCG_TSA_TABLE_FROM_TLV (pu1Temp, au1EtsTsaTable);

    /* validation against Max TCG supported in local and remote system */
    u1LocalMaxTCG = pPortEntry->ETSAdmPortInfo.u1ETSAdmNumTCGSup;

    if (u1LocalMaxTCG == DCBX_ZERO)
    {
        u1LocalMaxTCG = ETS_MAX_TCGID_CONF;
    }

    if (u1RemMaxTCG == DCBX_ZERO)
    {
        u1RemMaxTCG = ETS_MAX_TCGID_CONF;
    }
    /* Validate the following:
     * 1. TCG in Priority assignment table and bandwidth table of remote device
     *    against Max TCG  supported by local and remote devices.
     * 2. Sum of Bandwidth allocated to TCGs should be equal to 100
     * 3. TSA table has to contain valid values */
    if (ETSUtlValidateTCG (u1LocalMaxTCG, u1RemMaxTCG,
                           au1EtsTCGID, au1EtsBW,
                           au1EtsTsaTable) == OSIX_FAILURE)
    {
        /* Update received Error Config TLV Counters */
        pPortEntry->u4ETSConfRxTLVError = pPortEntry->u4ETSConfRxTLVError + 1;
        /* Send Counter Update Info to Standby */
        DcbxRedSendDynamicEtsCounterInfo (pPortEntry,
                                          DCBX_RED_ETS_CONF_ERR_COUNTER);
        return;
    }

    /* Copy the Remote index paramters to the Remote Table for this port */
    pPortEntry->ETSRemPortInfo.i4ETSRemIndex = pApplTlvParam->i4RemIndex;
    pPortEntry->ETSRemPortInfo.u4ETSRemTimeMark =
        pApplTlvParam->u4RemLastUpdateTime;
    MEMCPY (pPortEntry->ETSRemPortInfo.au1ETSDestRemMacAddr,
            pApplTlvParam->RemLocalDestMacAddr, MAC_ADDR_LEN);
    pPortEntry->ETSRemPortInfo.u4ETSDestRemMacIndex =
        pApplTlvParam->u4RemLocalDestMACIndex;

    /* Copy the Remote paramters to the Remote Table */
    pPortEntry->ETSRemPortInfo.u1ETSRemWilling = u1Willing;
    pPortEntry->ETSRemPortInfo.u1ETSRemCBS = u1CBS;
    if (u1RemMaxTCG == ETS_MAX_TCGID_CONF)
    {
        pPortEntry->ETSRemPortInfo.u1ETSRemNumTCGSup = DCBX_ZERO;
    }
    else
    {
        pPortEntry->ETSRemPortInfo.u1ETSRemNumTCGSup = u1RemMaxTCG;
    }

    /* Updating Trafffic classes supported info in the remote table
       using the ETS configuration TLV */
    pPortEntry->ETSRemPortInfo.u1ETSRemNumTcSup = u1RemMaxTCG;
    /* Copy the Priority Assignement paramters to the Remote Table */
    MEMCPY (pPortEntry->ETSRemPortInfo.au1ETSRemTCGID, au1EtsTCGID,
            DCBX_MAX_PRIORITIES);

    /* Copy the Bandwidth paramters to the Remote table */
    MEMCPY (pPortEntry->ETSRemPortInfo.au1ETSRemBW, au1EtsBW,
            ETS_MAX_TCGID_CONF);

    /* Copy the TSA table paramters to the Remote table */
    MEMCPY (pPortEntry->ETSRemPortInfo.au1ETSRemTsaTable, au1EtsTsaTable,
            ETS_MAX_TCGID_CONF);

    if (pPortEntry->u1RemTlvUpdStatus == DCBX_ZERO)
    {
        /* Send Trap Notification for Peer Up Status only
           if the same trap is already not sent */
        NotifyInfo.u4IfIndex = pPortEntry->u4IfIndex;
        NotifyInfo.u1Module = ETS_TRAP;
        NotifyInfo.unTrapMsg.u1PeerStatus = ETS_ENABLED;
        DcbxSendNotification (&NotifyInfo, PEER_STATE_TRAP);
    }
    /* Set the Remote Updation Status for this TLV */
    pPortEntry->u1RemTlvUpdStatus =
        pPortEntry->u1RemTlvUpdStatus | ETS_CONF_TLV_REM_UPD;

    /* Update received  Config TLV Counter */
    pPortEntry->u4ETSConfRxTLVCount = pPortEntry->u4ETSConfRxTLVCount + 1;

    /* Calling compatibility to set dcbx status */
    i4Compatibility = ETSApiIsCfgCompatible (pPortEntry);
    if ((i4Compatibility == OSIX_TRUE) &&
        (pPortEntry->u1DcbxStatus != DCBX_STATUS_PEER_NW_CFG_COMPAT))
    {
        SET_DCBX_STATUS (pPortEntry, pPortEntry->u1ETSDcbxSemType,
                         DCBX_RED_ETS_DCBX_STATUS_INFO, DCBX_STATUS_OK);
    }

    /* Send Remote table information to Standby Node */
    DcbxRedSendDynamicEtsRemInfo (pPortEntry);

    /* Send Counter Update Info to Standby */
    DcbxRedSendDynamicEtsCounterInfo (pPortEntry, DCBX_RED_ETS_CONF_RX_COUNTER);
    return;
}

/***************************************************************************
 *  FUNCTION NAME : ETSUtlHandleRecoTLV
 * 
 *  DESCRIPTION   : This utility is used to handle and process the Reco TLV.
 * 
 *  INPUT         : pPortEntry - ETS Port Entry.
 *                  ApplTlvParam - Appl TLV information.
 * 
 *  OUTPUT        : None
 * 
 *  RETURNS       : None
 * 
 * **************************************************************************/
PUBLIC VOID
ETSUtlHandleRecoTLV (tETSPortEntry * pPortEntry, tApplTlvParam * pApplTlvParam)
{
    UINT4               u4Count = DCBX_ZERO;
    UINT1               u1Value = (UINT1) DCBX_ZERO;
    UINT1              *pu1Temp = NULL;
    UINT4               u4PriMapping = DCBX_ZERO;
    UINT1               au1EtsTCGID[DCBX_MAX_PRIORITIES] = { DCBX_ZERO };
    UINT1               au1EtsBW[ETS_MAX_TCGID_CONF] = { DCBX_ZERO };
    UINT1               au1EtsTsaTable[ETS_MAX_TCGID_CONF] = { DCBX_ZERO };
    UINT1               u1CheckMaxTCG = DCBX_ZERO;
    UINT1               u1SumBW = DCBX_ZERO;
    tDcbxTrapMsg        NotifyInfo;

    MEMSET (&NotifyInfo, DCBX_ZERO, sizeof (tDcbxTrapMsg));

    if (pApplTlvParam->u2RxTlvLen != ETS_RECO_TLV_LEN)
    {
        /* If TLV length is not correct, then do not process the TLV */
        DCBX_TRC (DCBX_TLV_TRC | DCBX_FAILURE_TRC, "ETSUtlHandleRecoTLV:"
                  "ETS Reco Received TLV length is incorrect !!!\r\n");
        /* Update received Error Recomended TLV Counter */
        pPortEntry->u4ETSRecoRxTLVError = pPortEntry->u4ETSRecoRxTLVError + 1;
        /* Send Counter Update Info to Standby */
        DcbxRedSendDynamicEtsCounterInfo (pPortEntry,
                                          DCBX_RED_ETS_RECO_ERR_COUNTER);

        return;
    }

    pu1Temp = pApplTlvParam->au1DcbxTlv;

    if (MEMCMP (pu1Temp, gau1ETSRecoPreFormedTlv,
                ETS_PREFORMED_TLV_LEN) != DCBX_ZERO)
    {
        /* If the Header is not correct then do not process the TLV  */
        DCBX_TRC (DCBX_TLV_TRC | DCBX_FAILURE_TRC, "ETSUtlHandleRecoTLV:"
                  "ETS Reco TLV Received Header is incorrect !!!\r\n");
        /* Update received Error Recomended TLV Counter */
        pPortEntry->u4ETSRecoRxTLVError = pPortEntry->u4ETSRecoRxTLVError + 1;
        /* Send Counter Update Info to Standby */
        DcbxRedSendDynamicEtsCounterInfo (pPortEntry,
                                          DCBX_RED_ETS_RECO_ERR_COUNTER);

        return;
    }

    pu1Temp = pu1Temp + ETS_PREFORMED_TLV_LEN;

    /* Get the Reserved bits */
    DCBX_GET_1BYTE (pu1Temp, u1Value);
    UNUSED_PARAM (u1Value);

    /* Get the Priorrity assignment Octets of length 4 */
    DCBX_GET_4BYTE (pu1Temp, u4PriMapping);

    /* Copy the Priority Assignment table from TLV */
    ETS_FILL_PRI_MAPPING_FROM_TLV (u4PriMapping, au1EtsTCGID);
    /* Copy the Bandwidth paramters from TLV */
    ETS_FILL_TCG_BW_FROM_TLV (pu1Temp, au1EtsBW);
    /* Copy the Bandwidth paramters from TLV */
    ETS_FILL_TCG_TSA_TABLE_FROM_TLV (pu1Temp, au1EtsTsaTable);

    u1CheckMaxTCG = pPortEntry->ETSAdmPortInfo.u1ETSAdmNumTCGSup;

    if (u1CheckMaxTCG == DCBX_ZERO)
    {
        u1CheckMaxTCG = ETS_MAX_TCGID_CONF;
    }

    for (u4Count = DCBX_ZERO; u4Count < u1CheckMaxTCG; u4Count++)
    {
        u1SumBW = ((UINT1) (u1SumBW + au1EtsBW[u4Count]));
    }
    if (u1SumBW != ETS_MAX_BW)
    {
        /* If TLV has more TCGID than Maximum TCGID supported in the 
         * local system,then do not process the TLV */
        DCBX_TRC (DCBX_TLV_TRC | DCBX_FAILURE_TRC, "ETSUtlHandleRecoTLV:"
                  "ETS Reco Received TLV has BW not equal to 100 or"
                  "TLV has more TCGID than Max TCGID !!!\r\n");
        /* Update received Error Recomended TLV Counter */
        pPortEntry->u4ETSRecoRxTLVError = pPortEntry->u4ETSRecoRxTLVError + 1;
        /* Send Counter Update Info to Standby */
        DcbxRedSendDynamicEtsCounterInfo (pPortEntry,
                                          DCBX_RED_ETS_RECO_ERR_COUNTER);

        return;
    }

    /* Validate the recieved RECO TSA Table */
    for (u4Count = DCBX_ZERO; u4Count < ETS_MAX_TCGID_CONF; u4Count++)
    {
        switch (au1EtsTsaTable[u4Count])
        {
            case DCBX_STRICT_PRIORITY_ALGO:
            case DCBX_CREDIT_BASED_SHAPER_ALGO:
            case DCBX_ENHANCED_TRANSMISSION_SELECTION_ALGO:
            case DCBX_VENDOR_SPECIFIC_ALGO:
                break;
            default:
                /* If the Recieved TSA table entry doesn't 
                 * match with any of the valid TSA,
                 * then do not process the TLV */
                DCBX_TRC (DCBX_TLV_TRC | DCBX_FAILURE_TRC,
                          "ETSUtlHandleRecoTLV:"
                          "ETS Conf Received TLV has TSA Entry doesn't matching"
                          "to valid TSA !!!\r\n");
                /* Update received Error Recomended TLV Counter */
                pPortEntry->u4ETSRecoRxTLVError =
                    pPortEntry->u4ETSRecoRxTLVError + 1;
                /* Send Counter Update Info to Standby */
                DcbxRedSendDynamicEtsCounterInfo (pPortEntry,
                                                  DCBX_RED_ETS_RECO_ERR_COUNTER);
                return;
        }
    }

    /* Copy the Remote index paramters to the Remote Table for this port */
    pPortEntry->ETSRemPortInfo.i4ETSRemIndex = pApplTlvParam->i4RemIndex;
    pPortEntry->ETSRemPortInfo.u4ETSRemTimeMark =
        pApplTlvParam->u4RemLastUpdateTime;
    MEMCPY (pPortEntry->ETSRemPortInfo.au1ETSDestRemMacAddr,
            pApplTlvParam->RemLocalDestMacAddr, MAC_ADDR_LEN);
    pPortEntry->ETSRemPortInfo.u4ETSDestRemMacIndex =
        pApplTlvParam->u4RemLocalDestMACIndex;

    /* Copy the Priority Assignement paramters to the Remote Table */
    MEMCPY (pPortEntry->ETSRemPortInfo.au1ETSRemRecoTCGID, au1EtsTCGID,
            DCBX_MAX_PRIORITIES);

    /* Copy the Reco Bandwidth paramters to the Remote table */
    MEMCPY (pPortEntry->ETSRemPortInfo.au1ETSRemRecoBW, au1EtsBW,
            ETS_MAX_TCGID_CONF);

    /* Copy the Reco TSA paramters to the Remote table */
    MEMCPY (pPortEntry->ETSRemPortInfo.au1ETSRemRecoTsaTable, au1EtsTsaTable,
            ETS_MAX_TCGID_CONF);

    if (pPortEntry->u1RemTlvUpdStatus == DCBX_ZERO)
    {
        /* Send Trap Notification for Peer Up Status only
           if the same trap is already not sent */
        NotifyInfo.u4IfIndex = pPortEntry->u4IfIndex;
        NotifyInfo.u1Module = ETS_TRAP;
        NotifyInfo.unTrapMsg.u1PeerStatus = ETS_ENABLED;
        DcbxSendNotification (&NotifyInfo, PEER_STATE_TRAP);
    }

    /* Set the Remote Updation Status for this TLV */
    pPortEntry->u1RemTlvUpdStatus =
        pPortEntry->u1RemTlvUpdStatus | ETS_RECO_TLV_REM_UPD;

    /* Update received Recommended TLV Counters */
    pPortEntry->u4ETSRecoRxTLVCount = pPortEntry->u4ETSRecoRxTLVCount + 1;

    /* Since willing parameter is updated from the Remote,
     * start the state machine change request by calling willing
     * change function */
    ETSUtlWillingChange (pPortEntry, OSIX_FALSE);

    /* Send Remote table information to Standby Node */
    DcbxRedSendDynamicEtsRemInfo (pPortEntry);
    /* Send Counter Update Info to Standby */
    DcbxRedSendDynamicEtsCounterInfo (pPortEntry, DCBX_RED_ETS_RECO_RX_COUNTER);
    return;
}

/***************************************************************************
 *  FUNCTION NAME : ETSUtlHandleTcSupportTLV
 * 
 *  DESCRIPTION   : This utility is used to handle and process the TcSupp TLV.
 * 
 *  INPUT         : pPortEntry - ETS Port Entry.
 *                  ApplTlvParam - Appl TLV information.
 * 
 *  OUTPUT        : None
 * 
 *  RETURNS       : None
 * 
 * **************************************************************************/
PUBLIC VOID
ETSUtlHandleTcSupportTLV (tETSPortEntry * pPortEntry,
                          tApplTlvParam * pApplTlvParam)
{
    UINT1               u1Value = DCBX_ZERO;
    UINT1              *pu1Temp = NULL;
    tDcbxTrapMsg        NotifyInfo;

    MEMSET (&NotifyInfo, DCBX_ZERO, sizeof (tDcbxTrapMsg));

    if (pApplTlvParam->u2RxTlvLen != ETS_TC_SUPP_TLV_LEN)
    {
        /* If TLV length is not correct, then do not process the TLV */
        DCBX_TRC (DCBX_TLV_TRC | DCBX_FAILURE_TRC, "ETSUtlHandleTcSupportTLV:"
                  "ETS TC Supp Received TLV length is incorrect !!!\r\n");
        /* Update received Error TC supported TLV Counter */
        pPortEntry->u4ETSTcSupRxTLVError = pPortEntry->u4ETSTcSupRxTLVError + 1;
        /* Send Counter Update Info to Standby */
        DcbxRedSendDynamicEtsCounterInfo (pPortEntry,
                                          DCBX_RED_ETS_TC_SUPP_ERR_COUNTER);

        return;
    }

    pu1Temp = pApplTlvParam->au1DcbxTlv;

    if (MEMCMP (pu1Temp, gau1ETSTcSuppPreFormedTlv,
                ETS_PREFORMED_TLV_LEN) != DCBX_ZERO)
    {
        /* If the Header is not correct then do not process the TLV  */
        DCBX_TRC (DCBX_TLV_TRC | DCBX_FAILURE_TRC, "ETSUtlHandleTcSupportTLV:"
                  "ETS TC Supp TLV Received Header is incorrect !!!\r\n");
        /* Update received Error TC supported TLV Counter */
        pPortEntry->u4ETSTcSupRxTLVError = pPortEntry->u4ETSTcSupRxTLVError + 1;
        /* Send Counter Update Info to Standby */
        DcbxRedSendDynamicEtsCounterInfo (pPortEntry,
                                          DCBX_RED_ETS_TC_SUPP_ERR_COUNTER);

        return;
    }

    pu1Temp = pu1Temp + ETS_PREFORMED_TLV_LEN;

    /* Copy the Remote index paramters to the Remote Table for this port */

    pPortEntry->ETSRemPortInfo.i4ETSRemIndex = pApplTlvParam->i4RemIndex;
    pPortEntry->ETSRemPortInfo.u4ETSRemTimeMark =
        pApplTlvParam->u4RemLastUpdateTime;
    MEMCPY (pPortEntry->ETSRemPortInfo.au1ETSDestRemMacAddr,
            pApplTlvParam->RemLocalDestMacAddr, MAC_ADDR_LEN);
    pPortEntry->ETSRemPortInfo.u4ETSDestRemMacIndex =
        pApplTlvParam->u4RemLocalDestMACIndex;

    /* Get the Reserved bits */
    DCBX_GET_1BYTE (pu1Temp, u1Value);

    /* Traffic class supported value will be in last 3 bits of the byte */
    pPortEntry->ETSRemPortInfo.u1ETSRemNumTcSup = u1Value & ETS_TC_SUPP_MASK;

    if (pPortEntry->u1RemTlvUpdStatus == DCBX_ZERO)
    {
        /* Send Trap Notification for Peer Up Status only
           if the same trap is already not sent */
        NotifyInfo.u4IfIndex = pPortEntry->u4IfIndex;
        NotifyInfo.u1Module = ETS_TRAP;
        NotifyInfo.unTrapMsg.u1PeerStatus = ETS_ENABLED;
        DcbxSendNotification (&NotifyInfo, PEER_STATE_TRAP);
    }

    /* Set the Remote Updation Status for this TLV */
    pPortEntry->u1RemTlvUpdStatus =
        pPortEntry->u1RemTlvUpdStatus | ETS_TC_SUPP_TLV_REM_UPD;
    /* Update received  TC supported TLV Counter */
    pPortEntry->u4ETSTcSuppRxTLVCount = pPortEntry->u4ETSTcSuppRxTLVCount + 1;

    /* Update the Local Port Information if the Operational State
     * is RECO and also Re-Program the hardware */
    if (pPortEntry->u1ETSDcbxOperState == ETS_OPER_RECO)
    {
        ETSUtlUpdateLocalParam (pPortEntry, pPortEntry->u1ETSDcbxOperState);
        ETSUtlHwConfig (pPortEntry, ETS_ENABLED);
    }
    /* Send Remote table information to Standby Node */
    DcbxRedSendDynamicEtsRemInfo (pPortEntry);
    /* Send Counter Update Info to Standby */
    DcbxRedSendDynamicEtsCounterInfo (pPortEntry,
                                      DCBX_RED_ETS_TC_SUPP_RX_COUNTER);

    return;
}

/***************************************************************************
 *  FUNCTION NAME : EtsUtlMcQueueDettachAttach
 * 
 *  DESCRIPTION   : This utility is used to dettach and attach MC queues
 * 
 *  INPUT         : pPortEntry   - ETS Port number
 *                  u4ScheId     - MC queue scheduler id
 *                  u1u1ScheAlgo - MC queue Algorithm
 * 
 *  OUTPUT        : None
 * 
 *  RETURNS       : QOS_SUCESS/QOS_FAILURE
 * 
 * **************************************************************************/

PUBLIC INT4
EtsUtlMcQueueDettachAttach (UINT4 u4IfIndex, UINT4 u4ScheId, UINT1 u1ScheAlgo)
{
    UINT4               u4QId;
    /* Dettach and Attach MC Queues */
    for (u4QId = QOS_QUEUE_MAX_NUM_UCOSQ + 1; u4QId <= ETS_MAX_NUM_TCGID;
         u4QId++)
    {
        /* Dettach MC Queues */
        if (QosApiDeAttachQueue (u4IfIndex, u4QId) == QOS_FAILURE)
        {
            DCBX_TRC (DCBX_FAILURE_TRC, "EtsUtlMcQueueDettachAttach:"
                      "DeAttach MC Queue FAILED !!!\r\n");
            return QOS_FAILURE;
        }

    }
    for (u4QId = QOS_QUEUE_MAX_NUM_UCOSQ + 1; u4QId <= ETS_MAX_NUM_TCGID;
         u4QId++)
    {
        /* Attach to MC Queue Scheduler */
        if (QosApiAttachQueue (u4IfIndex,
                               u4QId, u4ScheId, u1ScheAlgo, 0) == QOS_FAILURE)
        {
            DCBX_TRC (DCBX_FAILURE_TRC, "EtsUtlMcQueueDettachAttach:"
                      "Attach MC Queue FAILED !!!\r\n");
            return QOS_FAILURE;
        }
    }
    return QOS_SUCCESS;
}

/***************************************************************************
 *  FUNCTION NAME : EtsUtlEtsDeleteScheduler 
 * 
 *  DESCRIPTION   : This utility is used to
 * 
 *  INPUT         : u4IfIndex - ETS Port 
 * 
 *  OUTPUT        : None
 * 
 *  RETURNS       : QOS_SUCESS/QOS_FAILURE
 * 
 * **************************************************************************/

PUBLIC INT4
EtsUtlEtsDeleteScheduler (tETSPortEntry * pPortEntry)
{
    UINT4               u4Index;

    /* Dettach Newly Attached Queues in the Previous ETS flow
     * And Attach to default scheduler node.  
     */
    for (u4Index = 0; u4Index < pPortEntry->ETSQosPortInfo.u4EtsUsedUcQueueCnt;
         u4Index++)
    {
        if (pPortEntry->ETSQosPortInfo.u4EtsUsedUcQueueId[u4Index] != 0)
        {
            /* Dettach Queue */
            if (QosApiDeAttachQueue
                (pPortEntry->u4IfIndex,
                 pPortEntry->ETSQosPortInfo.u4EtsUsedUcQueueId[u4Index]) ==
                QOS_FAILURE)
            {
                DCBX_TRC (DCBX_FAILURE_TRC, "EtsUtlEtsDeleteScheduler:"
                          "Detach Queue FAILED !!!\r\n");
                return OSIX_FAILURE;
            }
            /* Attach Queue to default scheduler */
            if (QosApiAttachQueue (pPortEntry->u4IfIndex,
                                   pPortEntry->ETSQosPortInfo.
                                   u4EtsUsedUcQueueId[u4Index],
                                   QOS_HL_DEF_S3_SCHEDULER1_ID, 0,
                                   0) == QOS_FAILURE)
            {
                DCBX_TRC (DCBX_FAILURE_TRC, "EtsUtlEtsDeleteScheduler:"
                          "Attach Queue FAILED !!!\r\n");
                return OSIX_FAILURE;
            }
            pPortEntry->ETSQosPortInfo.u4EtsUsedUcQueueId[u4Index] = 0;
        }
    }

    /* Dettach and Attach MC Queues */
    if (EtsUtlMcQueueDettachAttach
        (pPortEntry->u4IfIndex, QOS_HL_DEF_S3_SCHEDULER2_ID, 0) == QOS_FAILURE)
    {
        DCBX_TRC (DCBX_FAILURE_TRC, "ETSUtlHwConfig:"
                  "Dettach the existing scheduler and Attach to Default MC Queue FAILED !!!\r\n");
        return OSIX_FAILURE;
    }

    /* Delete Newly Added Schedulers for WDRR and SP Queues in the Previous ETS flow */
    for (u4Index = 0; u4Index < pPortEntry->ETSQosPortInfo.u4EtsUsedUcScheCnt;
         u4Index++)
    {
        if (pPortEntry->ETSQosPortInfo.u4EtsUsedUcScheId[u4Index] != 0)
        {
            if (QosApiDeleteSchedular
                (pPortEntry->u4IfIndex,
                 pPortEntry->ETSQosPortInfo.u4EtsUsedUcScheId[u4Index]) ==
                QOS_FAILURE)
            {
                DCBX_TRC (DCBX_FAILURE_TRC, "EtsUtlEtsDeleteScheduler:"
                          "UC Scheduler Delete FAILED !!!\r\n");
                return OSIX_FAILURE;
            }
            pPortEntry->ETSQosPortInfo.u4EtsUsedUcScheId[u4Index] = 0;
        }
    }
    /* Delete Newly Added Scheduler for MC Queues in the Previous ETS flow */
    if (pPortEntry->ETSQosPortInfo.u4EtsUsedMcScheId != 0)
    {
        if (QosApiDeleteSchedular
            (pPortEntry->u4IfIndex,
             pPortEntry->ETSQosPortInfo.u4EtsUsedMcScheId) == QOS_FAILURE)
        {
            DCBX_TRC (DCBX_FAILURE_TRC, "EtsUtlEtsDeleteScheduler:"
                      "MC Scheduler Delete FAILED !!!\r\n");
            return OSIX_FAILURE;
        }
        pPortEntry->ETSQosPortInfo.u4EtsUsedMcScheId = 0;
    }

    pPortEntry->ETSQosPortInfo.u4EtsUsedUcQueueCnt = 0;
    pPortEntry->ETSQosPortInfo.u4EtsUsedUcScheCnt = 0;
    pPortEntry->ETSQosPortInfo.u4EtsUsedMcScheCnt = 0;
    return OSIX_SUCCESS;
}

/***************************************************************************
 *  FUNCTION NAME : ETSUtlHwConfig
 * 
 *  DESCRIPTION   : This utility is used to configure the ETS paramters in 
 *                  the hardware by calling QOS API.
 * 
 *  INPUT         : pPortEntry - ETS port Entry.
 *                  u1Status - Enable/Disable status.
 * 
 *  OUTPUT        : None
 * 
 *  RETURNS       : OSIX_SUCCESS/OSIX_FAILURE
 * 
 * **************************************************************************/
PUBLIC INT4
ETSUtlHwConfig (tETSPortEntry * pPortEntry, UINT1 u1Status)
{
#ifdef DCBX_ETS_QOS_WANTED
    UINT4               u4PriIdx = DCBX_ZERO;
    tQosEtsHwEntry      EtsHwEntry;
    tQosEgressTxEntry   EgressTxEntry;
    UINT1               au1EtsBW[ETS_MAX_TCGID_CONF] = { DCBX_ZERO };
    UINT1               au1EtsTsa[ETS_MAX_TCGID_CONF] = { DCBX_ZERO };
    UINT4               u4TCGId = DCBX_ZERO;
    UINT4               u4QId = 0;
    UINT4               u4EtsCurTGs[ETS_MAX_TCGID_CONF] = { DCBX_ZERO };
    UINT4               u1EtsCurTGAlgo[ETS_MAX_TCGID_CONF] = { DCBX_ZERO };
    UINT4               u4ScheIndex = 0;
    UINT4               u4ScheBaseIndex = 0;
    UINT4               u4QueIndex = 0;
    UINT1               u1EtsWdrrFlag;

    MEMSET (&EtsHwEntry, DCBX_ZERO, sizeof (EtsHwEntry));
    MEMSET (&EgressTxEntry, DCBX_ZERO, sizeof (EgressTxEntry));

    EgressTxEntry.u4IfIndex = pPortEntry->u4IfIndex;
    EgressTxEntry.u1Status = EGRESS_TX_ENABLE;
    EgressTxEntry.i4FilterId = pPortEntry->i4EtsFilterId;

    /* Change the Egress Traffic Status to ENABLED */
    if (QosApiQosHwEgressTxStatus (&EgressTxEntry) == QOS_FAILURE)
    {
        DCBX_TRC (DCBX_FAILURE_TRC, "ETSUtlHwConfig:"
                  "Change of Egress Tx Status FAILED !!!\r\n");
        return OSIX_FAILURE;
    }
    /* Check If the state is in Recommended and Reco TLV updation
     * happened already. If so then Reco BW need to take precedence
     * over the Config BW */
    if (pPortEntry->u1ETSDcbxOperState == ETS_OPER_RECO)
    {
        MEMCPY (au1EtsBW, pPortEntry->ETSLocPortInfo.au1ETSLocRecoBW,
                ETS_MAX_TCGID_CONF);
        MEMCPY (au1EtsTsa, pPortEntry->ETSLocPortInfo.au1ETSLocRecoTsaTable,
                ETS_MAX_TCGID_CONF);

    }
    else
    {
        MEMCPY (au1EtsBW, pPortEntry->ETSLocPortInfo.au1ETSLocBW,
                ETS_MAX_TCGID_CONF);
        MEMCPY (au1EtsTsa, pPortEntry->ETSLocPortInfo.au1ETSLocTsaTable,
                ETS_MAX_TCGID_CONF);
    }

    EgressTxEntry.u1Status = EGRESS_TX_DISABLE;
    /* For Each Priority Configure the Bandwidth and Related TCGID 
     * in the hardware using QOS API */
    if (u1Status == ETS_ENABLED)
    {
        for (u4PriIdx = DCBX_ZERO; u4PriIdx < DCBX_MAX_PRIORITIES; u4PriIdx++)
        {
            u4TCGId = pPortEntry->ETSLocPortInfo.au1ETSLocTCGID[u4PriIdx];
            u4EtsCurTGs[u4TCGId] = TRUE;
            /* DCBx Algo to Qos Algo Mapping */
            switch (au1EtsTsa[u4TCGId])
            {
                case DCBX_STRICT_PRIORITY_ALGO:
                    EtsHwEntry.u1ETSTsa = QOS_SCHED_ALGO_SP;
                    break;
                case DCBX_CREDIT_BASED_SHAPER_ALGO:
                    EtsHwEntry.u1ETSTsa = QOS_SCHED_NOT_SUPPORTED_ALGO;
                    break;
                case DCBX_ENHANCED_TRANSMISSION_SELECTION_ALGO:
                    EtsHwEntry.u1ETSTsa = QOS_SCHED_ALGO_DRR;
                    break;
                case DCBX_VENDOR_SPECIFIC_ALGO:
                    EtsHwEntry.u1ETSTsa = QOS_SCHED_NOT_SUPPORTED_ALGO;
                    break;
                default:
                    DCBX_TRC (DCBX_FAILURE_TRC, "ETSUtlHwConfig:"
                              "Invalid Traffic selection Algorithm value !!!\r\n");

                    /* Change the Egress Traffic Status to DISABLED */
                    if (QosApiQosHwEgressTxStatus (&EgressTxEntry) ==
                        QOS_FAILURE)
                    {
                        DCBX_TRC (DCBX_FAILURE_TRC, "ETSUtlHwConfig:"
                                  "Change of Egress Tx Status FAILED !!!\r\n");
                    }

                    return OSIX_FAILURE;
            }

            u1EtsCurTGAlgo[u4TCGId] = EtsHwEntry.u1ETSTsa;
            if (u1EtsCurTGAlgo[u4TCGId] == QOS_SCHED_ALGO_DRR)
            {
                u1EtsWdrrFlag = TRUE;
            }

            DCBX_TRC_ARG6 (DCBX_CONTROL_PLANE_TRC, "ETSUtlHwConfig:"
                           "Paramters for QosApiConfigEts are "
                           "IfIndex :%d Priority : %d TCGID : %d "
                           "BW for TCGID %d : %d and Status of entry :%d "
                           "!!!\r\n", pPortEntry->u4IfIndex, u4PriIdx,
                           pPortEntry->ETSLocPortInfo.au1ETSLocTCGID[u4PriIdx],
                           u4TCGId, au1EtsBW[u4PriIdx], u1Status);

            DCBX_TRC_ARG2 (DCBX_CONTROL_PLANE_TRC,
                           "TSA Entry for TCID: %d is %d", u4PriIdx,
                           au1EtsTsa[u4PriIdx]);

            /* Fill the struct to be programmed to HW */
            EtsHwEntry.u4IfIndex = pPortEntry->u4IfIndex;
            EtsHwEntry.u4PriIdx = u4PriIdx;
            EtsHwEntry.u1ETSBw = au1EtsBW[u4TCGId];
            EtsHwEntry.u1EtsPortStatus = u1Status;
            EtsHwEntry.u1EtsTCGID = (UINT1) u4TCGId;

        }

        if ((MsrIsMibRestoreInProgress () == MSR_TRUE)
            && (!gu1DefaultConfFlag[pPortEntry->u4IfIndex]))
        {
            if (QosApiQoSAddDefHierarchyTblEntries (pPortEntry->u4IfIndex) ==
                QOS_FAILURE)
            {
                DCBX_TRC (DCBX_FAILURE_TRC, "ETSUtlHwConfig:"
                          "Default HL Configuration FAILED !!!\r\n");
                /* Change the Egress Traffic Status to DISABLED */
                if (QosApiQosHwEgressTxStatus (&EgressTxEntry) == QOS_FAILURE)
                {
                    DCBX_TRC (DCBX_FAILURE_TRC, "ETSUtlHwConfig:"
                              "Change of Egress Tx Status FAILED !!!\r\n");
                }
                return OSIX_FAILURE;
            }
            gu1DefaultConfFlag[pPortEntry->u4IfIndex] = TRUE;
        }

        /* Delete newly added S3 Schedulers
         * Below check is added not try to delete the schedulers 
         * in the first time.
         */
        if ((pPortEntry->ETSQosPortInfo.u4EtsUsedMcScheCnt != 0) &&
            (pPortEntry->ETSQosPortInfo.u4EtsUsedUcScheCnt != 0) &&
            (pPortEntry->ETSQosPortInfo.u4EtsUsedUcQueueCnt != 0))
        {
            if (EtsUtlEtsDeleteScheduler (pPortEntry) == OSIX_FAILURE)
            {
                DCBX_TRC (DCBX_FAILURE_TRC, "ETSUtlHwConfig:"
                          "Deletion of S3 Scheduler and Queue Delete FAILED !!!\r\n");
                /* Change the Egress Traffic Status to DISABLED */
                if (QosApiQosHwEgressTxStatus (&EgressTxEntry) == QOS_FAILURE)
                {
                    DCBX_TRC (DCBX_FAILURE_TRC, "ETSUtlHwConfig:"
                              "Change of Egress Tx Status FAILED !!!\r\n");
                }
                return OSIX_FAILURE;
            }
        }

        u4ScheBaseIndex = QOS_HL_DEF_S3_SCHEDULER3_ID;
        /* Create S3 Scheduler node for WDRR Queues */
        if (u1EtsWdrrFlag)
        {
            if (QosApiCreateSchedular (pPortEntry->u4IfIndex,
                                       u4ScheBaseIndex,
                                       QOS_S3_SCHEDULER,
                                       QOS_SCHED_ALGO_DRR) == QOS_FAILURE)
            {
                DCBX_TRC (DCBX_FAILURE_TRC, "ETSUtlHwConfig:"
                          "Creation of S3 Scheduler node for WDRR Queue - FAILED !!!\r\n");
                /* Change the Egress Traffic Status to DISABLED */
                if (QosApiQosHwEgressTxStatus (&EgressTxEntry) == QOS_FAILURE)
                {
                    DCBX_TRC (DCBX_FAILURE_TRC, "ETSUtlHwConfig:"
                              "Change of Egress Tx Status FAILED !!!\r\n");
                }
                return OSIX_FAILURE;
            }
            pPortEntry->ETSQosPortInfo.u4EtsUsedUcScheId[u4ScheIndex] =
                u4ScheBaseIndex;
            pPortEntry->ETSQosPortInfo.u4EtsUsedUcScheCnt++;
        }

        for (u4TCGId = DCBX_ZERO; u4TCGId < ETS_MAX_TCGID_CONF; u4TCGId++)
        {
            if (u4EtsCurTGs[u4TCGId] == TRUE)
            {
                if (u1EtsCurTGAlgo[u4TCGId] == QOS_SCHED_ALGO_DRR)
                {
                    u4QId = u4TCGId + 1;

                    /* Detach ETS WDRR Queues from the default S3 scheduler */
                    if (QosApiDeAttachQueue (pPortEntry->u4IfIndex, u4QId) ==
                        QOS_FAILURE)
                    {
                        DCBX_TRC (DCBX_FAILURE_TRC, "ETSUtlHwConfig:"
                                  "Detach ETS WDRR Queues from the default S3 scheduler - FAILED !!!\r\n");
                        /* Change the Egress Traffic Status to DISABLED */
                        if (QosApiQosHwEgressTxStatus (&EgressTxEntry) ==
                            QOS_FAILURE)
                        {
                            DCBX_TRC (DCBX_FAILURE_TRC, "ETSUtlHwConfig:"
                                      "Change of Egress Tx Status FAILED !!!\r\n");
                        }
                        return OSIX_FAILURE;
                    }
                    /* Attach ETS WDRR Queue to new scheduler */
                    if (QosApiAttachQueue (pPortEntry->u4IfIndex,
                                           u4QId,
                                           u4ScheBaseIndex,
                                           QOS_SCHED_ALGO_RR,
                                           au1EtsBW[u4TCGId]) == QOS_FAILURE)
                    {
                        DCBX_TRC (DCBX_FAILURE_TRC, "ETSUtlHwConfig:"
                                  "Attach ETS WDRR Queue to new scheduler - FAILED !!!\r\n");
                        /* Change the Egress Traffic Status to DISABLED */
                        if (QosApiQosHwEgressTxStatus (&EgressTxEntry) ==
                            QOS_FAILURE)
                        {
                            DCBX_TRC (DCBX_FAILURE_TRC, "ETSUtlHwConfig:"
                                      "Change of Egress Tx Status FAILED !!!\r\n");
                        }
                        return OSIX_FAILURE;
                    }
                    pPortEntry->ETSQosPortInfo.u4EtsQToSchIndex[u4QId] =
                        u4ScheBaseIndex;
                    pPortEntry->ETSQosPortInfo.
                        u4EtsUsedUcQueueId[u4QueIndex++] = u4QId;
                    pPortEntry->ETSQosPortInfo.u4EtsUsedUcQueueCnt++;
                }
            }
        }
        u4ScheBaseIndex++;
        u4ScheIndex++;

        for (u4TCGId = DCBX_ZERO; u4TCGId < ETS_MAX_TCGID_CONF; u4TCGId++)
        {
            if (u4EtsCurTGs[u4TCGId] == TRUE)
            {
                if (u1EtsCurTGAlgo[u4TCGId] == QOS_SCHED_ALGO_SP)
                {
                    u4QId = u4TCGId + 1;
                    pPortEntry->ETSQosPortInfo.u4EtsQToSchIndex[u4QId] =
                        u4ScheBaseIndex;

                    /* Create S3 Scheduler node for each SP Queue */
                    if (QosApiCreateSchedular (pPortEntry->u4IfIndex,
                                               u4ScheBaseIndex,
                                               QOS_S3_SCHEDULER,
                                               QOS_SCHED_ALGO_SP) ==
                        QOS_FAILURE)
                    {
                        DCBX_TRC (DCBX_FAILURE_TRC, "ETSUtlHwConfig:"
                                  "Creation of S3 Scheduler node for each SP Queue - FAILED !!!\r\n");
                        /* Change the Egress Traffic Status to DISABLED */
                        if (QosApiQosHwEgressTxStatus (&EgressTxEntry) ==
                            QOS_FAILURE)
                        {
                            DCBX_TRC (DCBX_FAILURE_TRC, "ETSUtlHwConfig:"
                                      "Change of Egress Tx Status FAILED !!!\r\n");
                        }
                        return OSIX_FAILURE;
                    }
                    pPortEntry->ETSQosPortInfo.u4EtsUsedUcScheId[u4ScheIndex] =
                        u4ScheBaseIndex;
                    pPortEntry->ETSQosPortInfo.u4EtsUsedUcScheCnt++;

                    /* Detach ETS SP Queue from the default S3 scheduler */
                    if (QosApiDeAttachQueue (pPortEntry->u4IfIndex, u4QId) ==
                        QOS_FAILURE)
                    {
                        DCBX_TRC (DCBX_FAILURE_TRC, "ETSUtlHwConfig:"
                                  "Detach ETS SP Queues from the default S3 scheduler - FAILED !!!\r\n");
                        /* Change the Egress Traffic Status to DISABLED */
                        if (QosApiQosHwEgressTxStatus (&EgressTxEntry) ==
                            QOS_FAILURE)
                        {
                            DCBX_TRC (DCBX_FAILURE_TRC, "ETSUtlHwConfig:"
                                      "Change of Egress Tx Status FAILED !!!\r\n");
                        }
                        return OSIX_FAILURE;
                    }

                    /* Attach ETS SP Queue to new scheduler */
                    if (QosApiAttachQueue (pPortEntry->u4IfIndex,
                                           u4QId,
                                           u4ScheBaseIndex,
                                           QOS_SCHED_ALGO_SP, 0) == QOS_FAILURE)
                    {
                        DCBX_TRC (DCBX_FAILURE_TRC, "ETSUtlHwConfig:"
                                  "Attach ETS SP Queue to new scheduler - FAILED !!!\r\n");
                        /* Change the Egress Traffic Status to DISABLED */
                        if (QosApiQosHwEgressTxStatus (&EgressTxEntry) ==
                            QOS_FAILURE)
                        {
                            DCBX_TRC (DCBX_FAILURE_TRC, "ETSUtlHwConfig:"
                                      "Change of Egress Tx Status FAILED !!!\r\n");
                        }
                        return OSIX_FAILURE;
                    }

                    pPortEntry->ETSQosPortInfo.
                        u4EtsUsedUcQueueId[u4QueIndex++] = u4QId;
                    pPortEntry->ETSQosPortInfo.u4EtsUsedUcQueueCnt++;
                    u4ScheBaseIndex++;
                    u4ScheIndex++;
                }
            }
        }

        /* Create last S3 Scheduler node for all MC Queues */
        if (QosApiCreateSchedular (pPortEntry->u4IfIndex,
                                   u4ScheBaseIndex,
                                   QOS_S3_SCHEDULER,
                                   QOS_SCHED_ALGO_RR) == QOS_FAILURE)
        {
            DCBX_TRC (DCBX_FAILURE_TRC, "ETSUtlHwConfig:"
                      "Creation of last S3 Scheduler node for all MC Queues - FAILED !!!\r\n");
            /* Change the Egress Traffic Status to DISABLED */
            if (QosApiQosHwEgressTxStatus (&EgressTxEntry) == QOS_FAILURE)
            {
                DCBX_TRC (DCBX_FAILURE_TRC, "ETSUtlHwConfig:"
                          "Change of Egress Tx Status FAILED !!!\r\n");
            }
            return OSIX_FAILURE;
        }
        pPortEntry->ETSQosPortInfo.u4EtsUsedMcScheId = u4ScheBaseIndex;
        pPortEntry->ETSQosPortInfo.u4EtsUsedMcScheCnt++;

        /* Dettach All MC queues from default S3 scheduler and Attach to last S3 scheduler */
        if (EtsUtlMcQueueDettachAttach (pPortEntry->u4IfIndex,
                                        u4ScheBaseIndex,
                                        QOS_SCHED_ALGO_RR) == QOS_FAILURE)
        {
            DCBX_TRC (DCBX_FAILURE_TRC, "ETSUtlHwConfig:"
                      "Dettach All MC queues from default S3 scheduler and Attach to last S3 scheduler - FAILED !!!\r\n");
            /* Change the Egress Traffic Status to DISABLED */
            if (QosApiQosHwEgressTxStatus (&EgressTxEntry) == QOS_FAILURE)
            {
                DCBX_TRC (DCBX_FAILURE_TRC, "ETSUtlHwConfig:"
                          "Change of Egress Tx Status FAILED !!!\r\n");
            }
            return OSIX_FAILURE;
        }

        /* Attach all the S3 scheduler to S2 scheduler with SP algorithm 
         * Queue Algorithm update is required after scheduler algorithm update
         */
        if (QosApiUpdateSchedularAlgo (pPortEntry->u4IfIndex,
                                       QOS_HL_DEF_S2_SCHEDULER_ID,
                                       QOS_SCHED_ALGO_SP) == QOS_FAILURE)
        {
            DCBX_TRC (DCBX_FAILURE_TRC, "ETSUtlHwConfig:"
                      "Attach all the S3 scheduler to S2 scheduler with SP algorithm - FAILED !!!\r\n");
            /* Change the Egress Traffic Status to DISABLED */
            if (QosApiQosHwEgressTxStatus (&EgressTxEntry) == QOS_FAILURE)
            {
                DCBX_TRC (DCBX_FAILURE_TRC, "ETSUtlHwConfig:"
                          "Change of Egress Tx Status FAILED !!!\r\n");
            }
            return OSIX_FAILURE;
        }

        /* Algorithm update for all ETS WDRR and SP queues */
        for (u4TCGId = DCBX_ZERO; u4TCGId < 8; u4TCGId++)
        {
            if (u4EtsCurTGs[u4TCGId] == TRUE)
            {
                u4QId = u4TCGId + 1;
                if (u1EtsCurTGAlgo[u4TCGId] == QOS_SCHED_ALGO_DRR)
                {
                    if (QosApiAttachQueue (pPortEntry->u4IfIndex,
                                           u4QId,
                                           pPortEntry->ETSQosPortInfo.
                                           u4EtsQToSchIndex[u4QId],
                                           u1EtsCurTGAlgo[u4TCGId],
                                           au1EtsBW[u4TCGId]) == QOS_FAILURE)
                    {
                        DCBX_TRC (DCBX_FAILURE_TRC, "ETSUtlHwConfig:"
                                  "Algorithm update for all ETS WDRR queues - FAILED !!!\r\n");
                        /* Change the Egress Traffic Status to DISABLED */
                        if (QosApiQosHwEgressTxStatus (&EgressTxEntry) ==
                            QOS_FAILURE)
                        {
                            DCBX_TRC (DCBX_FAILURE_TRC, "ETSUtlHwConfig:"
                                      "Change of Egress Tx Status FAILED !!!\r\n");
                        }
                        return OSIX_FAILURE;
                    }
                }
                else if (u1EtsCurTGAlgo[u4TCGId] == QOS_SCHED_ALGO_SP)
                {
                    if (QosApiAttachQueue (pPortEntry->u4IfIndex,
                                           u4QId,
                                           pPortEntry->ETSQosPortInfo.
                                           u4EtsQToSchIndex[u4QId],
                                           u1EtsCurTGAlgo[u4TCGId],
                                           0) == QOS_FAILURE)
                    {
                        DCBX_TRC (DCBX_FAILURE_TRC, "ETSUtlHwConfig:"
                                  "Algorithm update for all ETS SP queues - FAILED !!!\r\n");
                        /* Change the Egress Traffic Status to DISABLED */
                        if (QosApiQosHwEgressTxStatus (&EgressTxEntry) ==
                            QOS_FAILURE)
                        {
                            DCBX_TRC (DCBX_FAILURE_TRC, "ETSUtlHwConfig:"
                                      "Change of Egress Tx Status FAILED !!!\r\n");
                        }
                        return OSIX_FAILURE;
                    }
                }

            }
        }
    }
    else
    {
        /* If ETS is disbaled
         * Remove All ETS Flow scheduler and queues
         * Attach queues with the default scheduler
         * Configre default Schdulers with default values
         * Configure the Queue with the deafult values
         */
        if ((pPortEntry->ETSQosPortInfo.u4EtsUsedMcScheCnt != 0) &&
            (pPortEntry->ETSQosPortInfo.u4EtsUsedUcScheCnt != 0) &&
            (pPortEntry->ETSQosPortInfo.u4EtsUsedUcQueueCnt != 0))
        {
            if (EtsUtlEtsDeleteScheduler (pPortEntry) == OSIX_FAILURE)
            {
                DCBX_TRC (DCBX_FAILURE_TRC, "ETSUtlHwConfig:"
                          "Removal of ETS Flow Schedulers and Queues FAILED !!!\r\n");
                /* Change the Egress Traffic Status to DISABLED */
                if (QosApiQosHwEgressTxStatus (&EgressTxEntry) == QOS_FAILURE)
                {
                    DCBX_TRC (DCBX_FAILURE_TRC, "ETSUtlHwConfig:"
                              "Change of Egress Tx Status FAILED !!!\r\n");
                }
                return OSIX_FAILURE;
            }
        }
        /* Attach Default S3 scheduler to S2 scheduler with default algorithm(RR) */
        if (QosApiUpdateSchedularAlgo (pPortEntry->u4IfIndex,
                                       QOS_HL_DEF_S2_SCHEDULER_ID,
                                       QOS_SCHED_ALGO_RR) == QOS_FAILURE)
        {
            DCBX_TRC (DCBX_FAILURE_TRC, "ETSUtlHwConfig:"
                      "Attach Default S3 scheduler to S2 scheduler with default algorithm(RR) - FAILED !!!\r\n");
            /* Change the Egress Traffic Status to DISABLED */
            if (QosApiQosHwEgressTxStatus (&EgressTxEntry) == QOS_FAILURE)
            {
                DCBX_TRC (DCBX_FAILURE_TRC, "ETSUtlHwConfig:"
                          "Change of Egress Tx Status FAILED !!!\r\n");
            }
            return OSIX_FAILURE;
        }
        pPortEntry->ETSQosPortInfo.u4EtsUsedUcQueueCnt = 0;
        pPortEntry->ETSQosPortInfo.u4EtsUsedUcScheCnt = 0;
        pPortEntry->ETSQosPortInfo.u4EtsUsedMcScheCnt = 0;
    }
    /* Change the Egress Traffic Status to DISABLED */
    if (QosApiQosHwEgressTxStatus (&EgressTxEntry) == QOS_FAILURE)
    {
        DCBX_TRC (DCBX_FAILURE_TRC, "ETSUtlHwConfig:"
                  "Change of Egress Tx Status FAILED !!!\r\n");
    }
#else
    UNUSED_PARAM (pPortEntry);
    UNUSED_PARAM (u1Status);
#endif /* DCBX_ETS_QOS_WANTED */
    return OSIX_SUCCESS;
}

/***************************************************************************
 *  FUNCTION NAME : ETSUtlCountBitSet
 *
 *  DESCRIPTION   : This utility is used to count the number of bits set in 
 *                  the given byte
 *
 *  INPUT         : pu1Byte - Byte on which the number of bits to be set
 *
 *  OUTPUT        : Nothing
 *
 *  RETURNS       : u1Count - Number of bits set on the byte
 *
 * **************************************************************************/

UINT1
ETSUtlCountBitSet (UINT1 *pu1Byte)
{
    UINT2               u2BitPos = DCBX_ZERO;
    UINT1               u1Count = DCBX_ZERO;
    INT1                i1Result = DCBX_ZERO;

    for (u2BitPos = DCBX_MIN_VAL; u2BitPos <= DCBX_MAX_PRIORITIES; u2BitPos++)
    {
        OSIX_BITLIST_IS_BIT_SET (pu1Byte, u2BitPos, DCBX_MIN_VAL, i1Result);
        if (i1Result == OSIX_TRUE)
        {
            u1Count++;
        }
    }
    return u1Count;
}

/***************************************************************************
 *  FUNCTION NAME : ETSUtlValidateTCG
 *
 *  DESCRIPTION   : This utility is used to validate TCG in received ETS TLV
 *                  
 *
 *  INPUT         : u1LocalMaxTCG - Max TCG supported in Local system
 *                  u1RemMaxTCG   - Max TCG supported in Remote system
 *                  au1EtsTCGID   - Priority to Priority Group mapping in 
 *                                  remote system
 *                  au1ETSBW      - Bandwidth allocation table in remote system
 *                  pau1EtsTsaTable - TSA table of remote system 
 *
 *  OUTPUT        : Nothing
 *
 *  RETURNS       : OSIX_SUCCESS / OSIX_FAILURE
 *
 * **************************************************************************/
UINT4
ETSUtlValidateTCG (UINT1 u1LocalMaxTCG, UINT1 u1RemMaxTCG,
                   UINT1 *pau1EtsTCGID, UINT1 *pau1EtsBW,
                   UINT1 *pau1EtsTsaTable)
{
    UINT4               u4Count = DCBX_ZERO;
    UINT1               u1TCGSet = DCBX_ZERO;
    UINT1              *pu1TCGSet = NULL;
    UINT1               u1RemTCCount = DCBX_ZERO;
    UINT1               u1RemBWTCCount = DCBX_ZERO;
    UINT1               u1SumBW = DCBX_ZERO;

    pu1TCGSet = &u1TCGSet;
    for (u4Count = DCBX_ZERO; u4Count < DCBX_MAX_PRIORITIES; u4Count++)
    {
        /*validate TCGID range in received TLV */
        if ((pau1EtsTCGID[u4Count] != ETS_DEF_TCGID) &&
            (pau1EtsTCGID[u4Count] >= ETS_MAX_TCGID_CONF))
        {
            /* IF TLV has a non-supported TCGID, do not process the packet */
            DCBX_TRC ((DCBX_TLV_TRC | DCBX_FAILURE_TRC), "ETSUtlValidateTCG:"
                      "ETS Conf Received TLV has a non-supported TCGID\r\n");
            return OSIX_FAILURE;

        }
        if (pau1EtsTCGID[u4Count] != ETS_DEF_TCGID)
        {
            OSIX_BITLIST_SET_BIT (pu1TCGSet, (pau1EtsTCGID[u4Count] + 1),
                                  DCBX_MIN_VAL);
        }
    }
    /* 
       As practically the number of TCs supported can be different 
       between switches and ToR. 

       The validation for the ETS Max TCG values supported between 
       the local & remote nodes shall be relaxed for the DCBX ETS 
       attributes negotiation. 
     */

    u1RemTCCount = ETSUtlCountBitSet (pu1TCGSet);
    if (u1RemTCCount > u1RemMaxTCG)
    {
        /* If TLV has more TCGID than Maximum TCGID supported in the 
         * local system , then do not process the TLV */
        DCBX_TRC (DCBX_TLV_TRC | DCBX_FAILURE_TRC, "ETSUtlValidateTCG:"
                  "ETS Conf Received TLV has more TCGID"
                  "than Max TCGID  !!!\r\n");
        return OSIX_FAILURE;
    }
    UNUSED_PARAM (u1LocalMaxTCG);

    u1TCGSet = DCBX_ZERO;

    for (u4Count = DCBX_ZERO; u4Count < ETS_MAX_TCGID_CONF; u4Count++)
    {
        u1SumBW = ((UINT1) (u1SumBW + pau1EtsBW[u4Count]));
        if (pau1EtsBW[u4Count] != DCBX_ZERO)
        {
            OSIX_BITLIST_SET_BIT (pu1TCGSet, (u4Count + 1), DCBX_MIN_VAL);
        }
    }

    /* 
       As mentioned above, ignoring the local & remote node ETS 
       Max TCG values  
     */

    u1RemBWTCCount = ETSUtlCountBitSet (pu1TCGSet);
    if (u1RemBWTCCount > u1RemMaxTCG)
    {
        /* If bandwidth allocation is done for  more TCGID than Maximum TCGID
         *  supported in the local system , then do not process the TLV */
        DCBX_TRC (DCBX_TLV_TRC | DCBX_FAILURE_TRC, "ETSUtlValidateTCG:"
                  "ETS Conf Received TLV has more TCGID"
                  "than Max TCGID  !!!\r\n");
        return OSIX_FAILURE;
    }
    /*
     * To check later if this validation is required
     if (u1RemBWTCCount != u1RemTCCount)
     {
     DCBX_TRC (DCBX_TLV_TRC | DCBX_FAILURE_TRC, "ETSUtlValidateTCG:"
     "BW config in received ETS TLV is not matching the"
     "configured TCGs  !!!\r\n");
     return OSIX_FAILURE;
     }
     */

    if (u1SumBW != ETS_MAX_BW)
    {
        /* If the sum of bandwidth not equal to 100,
         * then do not process the TLV */
        DCBX_TRC (DCBX_TLV_TRC | DCBX_FAILURE_TRC, "ETSUtlValidateTCG:"
                  "ETS Conf Received TLV has BW not equal to 100 or"
                  "TLV has more TCGID than Max TCGID !!!\r\n");
        return OSIX_FAILURE;
    }
    if (pau1EtsTsaTable != NULL)
    {
        /* Validate the recieved RECO TSA Table */
        for (u4Count = DCBX_ZERO; u4Count < ETS_MAX_TCGID_CONF; u4Count++)
        {
            switch (pau1EtsTsaTable[u4Count])
            {
                case DCBX_STRICT_PRIORITY_ALGO:
                case DCBX_CREDIT_BASED_SHAPER_ALGO:
                case DCBX_ENHANCED_TRANSMISSION_SELECTION_ALGO:
                case DCBX_VENDOR_SPECIFIC_ALGO:
                    break;
                default:
                    /* If the Recieved TSA table entry doesn't 
                     * match with any of the valid TSA,
                     * then do not process the TLV */
                    DCBX_TRC (DCBX_TLV_TRC | DCBX_FAILURE_TRC,
                              "ETSUtlValidateTCG:"
                              "ETS Conf Received TLV has TSA Entry doesn't matching"
                              "to valid TSA !!!\r\n");
                    return OSIX_FAILURE;
            }
        }
    }
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : ETSUtlRegOrDeRegBasedOnMode                          */
/*                                                                           */
/* Description        : This function is used to Register or DeRegister      */
/*                      based on the Mode and ENABLE/DISABLE status          */
/*                                                                           */
/* Input(s)           : pPortEntry- Pointer to the ETS entry                 */
/*                      u1Status  - ENABLE/DISABLE                           */
/*                      u1TlvType - Reco/Conf/TcSupp/ALL                     */
/*                                                                           */
/* Output(s)          : None.                                                */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/* Called By          : ETSUtlDeletePortTblEntry                             */
/*                      ETSUtlModuleStatusChange                             */
/*                      ETSUtlAdminModeChange                                */
/*                      ETSUtlTlvStatusChange                                */
/*****************************************************************************/
VOID
ETSUtlRegOrDeRegBasedOnMode (tETSPortEntry * pPortEntry,
                             UINT1 u1Status, UINT1 u1TlvType)
{
    UINT1               u1DcbxMode = DCBX_ZERO;

    DcbxUtlGetDCBXMode (pPortEntry->u4IfIndex, &u1DcbxMode);
    DCBX_TRC_ARG3 (DCBX_TLV_TRC, "ETSUtlRegBasedOnMode: "
                   "Port = %d, DcbxMode = %s, Status = %s\r\n",
                   pPortEntry->u4IfIndex,
                   DCBX_GET_MODE (u1DcbxMode), DCBX_GET_ETS_MODE (u1Status));

    if (u1Status == ETS_ENABLED)
    {
        if (u1DcbxMode == DCBX_MODE_IEEE)
        {
            ETSUtlRegisterApplForPort (pPortEntry, u1TlvType);
        }
        else if (u1DcbxMode == DCBX_MODE_CEE)
        {
            if ((u1TlvType == ETS_CONF_TLV_SUB_TYPE) ||
                (u1TlvType == ETS_TLV_ALL))
            {
                CEERegisterApplForPort (pPortEntry->u4IfIndex,
                                        CEE_ETS_TLV_TYPE);
            }
        }
        else
        {
            ETSUtlRegisterApplForPort (pPortEntry, u1TlvType);
            if ((u1TlvType == ETS_CONF_TLV_SUB_TYPE) ||
                (u1TlvType == ETS_TLV_ALL))
            {
                CEERegisterApplForPort (pPortEntry->u4IfIndex,
                                        CEE_ETS_TLV_TYPE);
            }
        }
        if ((u1TlvType == ETS_CONF_TLV_SUB_TYPE) || (u1TlvType == ETS_TLV_ALL))
        {
            SET_DCBX_STATUS (pPortEntry, pPortEntry->u1ETSDcbxSemType,
                             DCBX_RED_ETS_DCBX_STATUS_INFO,
                             DCBX_STATUS_PEER_NO_ADV_DCBX);
        }
    }
    else
    {
        DCBX_TRC (DCBX_CONTROL_PLANE_TRC, "ETSUtlRegOrDeRegBasedOnMode: "
                  "Registering ETS TLV.!!!\r\n");
        if (u1DcbxMode == DCBX_MODE_IEEE)
        {
            ETSUtlDeRegisterApplForPort (pPortEntry, u1TlvType);
        }
        else if ((u1DcbxMode == DCBX_MODE_CEE) &&
                 ((u1TlvType == ETS_CONF_TLV_SUB_TYPE) ||
                  (u1TlvType == ETS_TLV_ALL)))
        {
            /* It will registered only Configuration TLV for CEE */
            CEEDeRegisterApplForPort (pPortEntry->u4IfIndex, CEE_ETS_TLV_TYPE);
            CEEUtlClearETSRemoteParam (pPortEntry);
        }
        else
        {
            ETSUtlDeRegisterApplForPort (pPortEntry, u1TlvType);
            if ((u1TlvType == ETS_CONF_TLV_SUB_TYPE) ||
                (u1TlvType == ETS_TLV_ALL))
            {
                CEEDeRegisterApplForPort (pPortEntry->u4IfIndex,
                                          CEE_ETS_TLV_TYPE);
            }
            /* No Need to clear the Remote Param for CEE as they will be
             * removed by the above function */
        }

        if ((u1TlvType == ETS_CONF_TLV_SUB_TYPE) || (u1TlvType == ETS_TLV_ALL))
        {
            /* DCBX status is based on config TLV for both IEEE and CEE */
            SET_DCBX_STATUS (pPortEntry, pPortEntry->u1ETSDcbxSemType,
                             DCBX_RED_ETS_DCBX_STATUS_INFO,
                             DCBX_STATUS_NOT_ADVERTISE);
        }
    }
    return;
}

/***************************************************************************
 *  FUNCTION NAME : ETSUtlCompareTCG
 *
 *  DESCRIPTION   : This utility is used to validate TCG in received ETS TLV
 *                  
 *
 *  INPUT         : pau1AdmEtsTCGID   - Priority to Priority Group mapping in 
 *                                      Local system
 *                  pau1RemEtsTCGID   - Priority to Priority Group mapping in 
 *                                      remote system
 *
 *  OUTPUT        : Nothing
 *
 *  RETURNS       : OSIX_SUCCESS / OSIX_FAILURE
 *
 * **************************************************************************/
UINT4
ETSUtlCompareTCG (UINT1 *pau1AdmEtsTCGID, UINT1 *pau1RemEtsTCGID)
{
    UINT4               u4Count = DCBX_ZERO;
    UINT1               u1TCGSet = DCBX_ZERO;
    UINT1              *pu1TCGSet = NULL;
    UINT1               u1AdmTCCount = DCBX_ZERO;
    UINT1               u1RemTCCount = DCBX_ZERO;

    pu1TCGSet = &u1TCGSet;
    for (u4Count = DCBX_ZERO; u4Count < DCBX_MAX_PRIORITIES; u4Count++)
    {
        if (pau1AdmEtsTCGID[u4Count] != ETS_DEF_TCGID)
        {
            OSIX_BITLIST_SET_BIT (pu1TCGSet, (pau1AdmEtsTCGID[u4Count] + 1),
                                  DCBX_MIN_VAL);
        }
    }
    u1AdmTCCount = ETSUtlCountBitSet (pu1TCGSet);
    u1TCGSet = DCBX_ZERO;
    for (u4Count = DCBX_ZERO; u4Count < DCBX_MAX_PRIORITIES; u4Count++)
    {
        if (pau1RemEtsTCGID[u4Count] != ETS_DEF_TCGID)
        {
            OSIX_BITLIST_SET_BIT (pu1TCGSet, (pau1RemEtsTCGID[u4Count] + 1),
                                  DCBX_MIN_VAL);
        }
    }
    u1RemTCCount = ETSUtlCountBitSet (pu1TCGSet);

    if (u1AdmTCCount != u1RemTCCount)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}
#endif /* ETS_UTL_C */
