/*************************************************************************
 * Copyright (C) 2010 Aricent Inc . All Rights Reserved
 * $Id: ceeutl.c,v 1.6 2017/01/25 13:19:41 siva Exp $
 * Description: This file contains CEE utility function.
 *************************************************************************/

#include "dcbxinc.h"

/***************************************************************************
 *  FUNCTION NAME : CEEUtlPortTblCmpFn
 * 
 *  DESCRIPTION   : This utility function is used by RbTree for CEE Ctrl
 *                  table.
 * 
 *  INPUT         : pRBElem1,pRBElem2 - RbTree Elements.
 * 
 *  OUTPUT        : None
 * 
 *  RETURNS       : LESSER/GREATER/EQUAL
 * 
 * **************************************************************************/
INT4
CEEUtlPortTblCmpFn (tRBElem * pRBElem1, tRBElem * pRBElem2)
{
    tCEECtrlEntry      *pCEECtrlEntry1 = NULL;
    tCEECtrlEntry      *pCEECtrlEntry2 = NULL;

    pCEECtrlEntry1 = (tCEECtrlEntry *) pRBElem1;
    pCEECtrlEntry2 = (tCEECtrlEntry *) pRBElem2;

    /* Compare the DCB Port Index */

    if (pCEECtrlEntry1->u4IfIndex < pCEECtrlEntry2->u4IfIndex)
    {
        return (DCBX_LESSER);
    }
    else if (pCEECtrlEntry1->u4IfIndex > pCEECtrlEntry2->u4IfIndex)
    {
        return (DCBX_GREATER);
    }

    return (DCBX_EQUAL);
}

/*****************************************************************************
 * Function Name      : CEEUtlGetCtrlEntry                                   
 * Description        : Returns pointer to port entry                        
 * Input(s)           : u4IfIndex - Interface number                         
 * Output(s)          : None.                                                
 * Return Value(s)    : Pointer to Ctrl PORT entry                           
 *****************************************************************************/
tCEECtrlEntry  *
CEEUtlGetCtrlEntry (UINT4 u4IfIndex)
{
    tCEECtrlEntry       CtrlEntry;
    tCEECtrlEntry      *pCtrlEntry = NULL;

    MEMSET (&CtrlEntry, DCBX_ZERO, sizeof (tCEECtrlEntry));
    CtrlEntry.u4IfIndex = u4IfIndex;

    pCtrlEntry = (tCEECtrlEntry *)
        RBTreeGet (gCEEGlobalInfo.pRbCeeCtrlTbl, (tRBElem *) & CtrlEntry);
    if (pCtrlEntry == NULL)
    {
        DCBX_TRC_ARG2 (DCBX_FAILURE_TRC, "%s : Failed in fetching the Ctrl Entry for "
                "Port Id %d \r\n", __FUNCTION__, u4IfIndex);
    }

    return (pCtrlEntry);
}

/****************************************************************************
 * Function Name      : CEEUtlCreateCtrlTblEntry                             
 * Description        : This function is used to Create an Entry             
 *                      it will do the following Action                      
 *                      1. Allocate a memory block for the New Entry         
 *                      2. Initialize the New Entry                          
 *                      3. Add this New Entry into the CEE Ctrl Table        
 * Input(s)           : u4IfIndex      - Index to the CEE Ctrl Table         
 * Output(s)          : None.                                                
 * Global Variables                                                          
 * Referred           : gCEEGlobalInfo                                       
 * Global Variables                                                          
 * Modified           : gCEEGlobalInfo                                       
 * Return Value(s)    : NULL / Pointer to  CEEPortEntry                      
 * Called By          : SNMP Low Level                                       
 * Calling Function   :                                                      
 *****************************************************************************/
tCEECtrlEntry *
CEEUtlCreateCtrlTblEntry (UINT4 u4IfIndex)
{
    tCEECtrlEntry      *pCEECtrlEntry = NULL;

    /* 1. Allocate a memory block for the New Entry */
    pCEECtrlEntry = CEEUtlGetCtrlEntry (u4IfIndex);
    if (pCEECtrlEntry == NULL)
    {
        pCEECtrlEntry = (tCEECtrlEntry *)
            MemAllocMemBlk (gCEEGlobalInfo.CEECtrlPoolId);
        if (pCEECtrlEntry == NULL)
        {
            DCBX_TRC_ARG2 ((DCBX_RESOURCE_TRC | DCBX_FAILURE_TRC), "In %s : MemAllocMemBlk () Failed ,"
                    "for CEE Ctrl entry with port %d \r\n", __FUNCTION__,
                    u4IfIndex);
            return pCEECtrlEntry;
        }

        /* 2. Initialize the CEE Port Entry */
        MEMSET (pCEECtrlEntry, DCBX_INIT_VAL, sizeof (tCEECtrlEntry));
        pCEECtrlEntry->u4IfIndex = u4IfIndex;


        /*Default Seqno of the Port */
        pCEECtrlEntry->u4CtrlSeqNo = DCBX_ZERO; 
        pCEECtrlEntry->u4CtrlAckNo = DCBX_ZERO; 
        pCEECtrlEntry->u4CtrlRcvdAckNo = DCBX_ZERO; 
        pCEECtrlEntry->u4CtrlRcvdSeqNo = DCBX_ZERO; 
        pCEECtrlEntry->u4FeatEnabledCount = DCBX_ZERO;
        pCEECtrlEntry->u4CtrlSyncNo    = DCBX_ZERO;
        pCEECtrlEntry->u4CtrlAckMissCount = DCBX_ZERO;
        /* 3. Add this New Entry into the CEE Ctrl  Table */
        if (RBTreeAdd (gCEEGlobalInfo.pRbCeeCtrlTbl,
                    (tRBElem *) pCEECtrlEntry) != RB_SUCCESS)
        {
            MemReleaseMemBlock (gCEEGlobalInfo.CEECtrlPoolId,
                    (UINT1 *) pCEECtrlEntry);

            DCBX_TRC_ARG2 (DCBX_FAILURE_TRC | DCBX_CRITICAL_TRC, "In %s : RBTreeAdd () Failed ,"
                    "for CEE Ctrl table with port %d \r\n", __FUNCTION__, 
                    u4IfIndex);
            return (NULL);
        }
        DCBX_TRC_ARG2 (DCBX_CONTROL_PLANE_TRC, "In %s :"
                "Created CEE Ctrl entry for port %d \r\n", __FUNCTION__,
                u4IfIndex);
        pCEECtrlEntry->u4FeatEnabledCount++;
    }
    else
    {
        pCEECtrlEntry->u4FeatEnabledCount++;
    }

    return (pCEECtrlEntry);
}

/*****************************************************************************
 * Function Name      : CEEUtlDeletCtrlTblEntry                              
 * Description        : This function is used to Delete an Entry from the    
 *                      PFC Port Table ,it will do the following Action      
 *                      1. Remove the Entry from the PFC Port Table          
 *                      2. Clear the Removed Entry                           
 *                      3. Release the Removed Entry's memory                
 * Input(s)           : pCEECtrlEntry   - Pointer to  PCEECtrlEntry          
 * Output(s)          : None.                                                
 * Global Variables                                                          
 * Referred           : gCEEGlobalInfo                                       
 * Global Variables                                                          
 * Modified           : gCEEGlobalInfo                                       
 * Return Value(s)    : OSIX_SUCCESS or OSIX_FAILURE                         
 * Called By          : SNMP Low Level                                       
 * Calling Function   :                                                      
 *****************************************************************************/
INT4
CEEUtlDeleteCtrlTblEntry (tCEECtrlEntry * pCEECtrlEntry)
{
    if (pCEECtrlEntry == NULL)
    {
        DCBX_TRC_ARG1 (DCBX_CONTROL_PLANE_TRC, "In %s : Received pCEECtrlEntry is NULL",
                __FUNCTION__);
        return (OSIX_FAILURE);
    }

    pCEECtrlEntry->u4FeatEnabledCount--;
    if (pCEECtrlEntry->u4FeatEnabledCount > DCBX_ZERO)
    {
        return (OSIX_SUCCESS);
    }

    RBTreeRemove (gCEEGlobalInfo.pRbCeeCtrlTbl, (UINT1 *) pCEECtrlEntry);
    DCBX_TRC_ARG2 (DCBX_CONTROL_PLANE_TRC, "In %s :"
            "Deleted CEE Ctrl entry for port %d \r\n", __FUNCTION__,
            pCEECtrlEntry->u4IfIndex);
    /* Release the Removed Entry's memory */
    MemReleaseMemBlock (gCEEGlobalInfo.CEECtrlPoolId,
            (UINT1 *) (pCEECtrlEntry));
    return (OSIX_SUCCESS);
}

/***************************************************************************
 *  FUNCTION NAME : CEEUtlDeleteAllPorts
 * 
 *  DESCRIPTION   : This utility function is used to delete all 
 *                  the CEE ports at the time of shutodwn.
 * 
 *  INPUT         : None
 * 
 *  OUTPUT        : None
 * 
 *  RETURNS       : None
 * 
 * **************************************************************************/
VOID
CEEUtlDeleteAllPorts (VOID)
{
    tCEECtrlEntry       TempCtrlEntry;
    tCEECtrlEntry      *pCEECtrlEntry = NULL;

    MEMSET (&TempCtrlEntry, DCBX_ZERO, sizeof (tCEECtrlEntry));

    /* Get the First port entry and delete the port entry if present */
    pCEECtrlEntry = (tCEECtrlEntry *) RBTreeGetFirst
        (gCEEGlobalInfo.pRbCeeCtrlTbl);
    if (pCEECtrlEntry != NULL)
    {
        do
        {
            /* Delete the CEE port table entry */
            TempCtrlEntry.u4IfIndex = pCEECtrlEntry->u4IfIndex;
            CEEUtlDeleteCtrlTblEntry (pCEECtrlEntry);
            pCEECtrlEntry = (tCEECtrlEntry *) RBTreeGetNext
                (gCEEGlobalInfo.pRbCeeCtrlTbl, &TempCtrlEntry,
                 CEEUtlPortTblCmpFn);
            /* Process for all the ports in the port RB Tree */
        }
        while (pCEECtrlEntry != NULL);
    }
    DCBX_TRC_ARG1 (DCBX_CONTROL_PLANE_TRC, "In %s :"
            "Deleted CEE Ctrl entry for all ports \r\n", __FUNCTION__);
    return;
}

/***************************************************************************
 *  FUNCTION NAME : CEEUtlHandleAgedOutFromDCBX
 * 
 *  DESCRIPTION   : This utility is used to handle age out of CEE TLV.
 * 
 *  INPUT         : u4IfIndex - Interface index 
 * 
 *  OUTPUT        : None
 * 
 *  RETURNS       : None
 * 
 * **************************************************************************/
VOID 
CEEUtlHandleAgedOutFromDCBX (UINT4 u4IfIndex)
{
    tCeeTLVInfo        CeeFeatTlvParam;

    MEMSET (&CeeFeatTlvParam, DCBX_ZERO, sizeof (tCeeTLVInfo));
	CeeFeatTlvParam.u4IfIndex = u4IfIndex;

	CeeUtlHandleCtrlTlvAgedOut (u4IfIndex);

    /* PG */
	CEESemRun (CEE_ETS_TLV_TYPE, &CeeFeatTlvParam, CEE_EV_NO_DCBX);
    /* PFC */
	CEESemRun (CEE_PFC_TLV_TYPE, &CeeFeatTlvParam, CEE_EV_NO_DCBX);
    /* Application Priority */
	CEESemRun (CEE_APP_PRI_TLV_TYPE, &CeeFeatTlvParam, CEE_EV_NO_DCBX);

    /* Sync to STBY */
    DcbxCeeRedSendDynSyncAgeOutInfo (u4IfIndex);

    return;
}

/***************************************************************************
 *  FUNCTION NAME : CeeUtlHandleCtrlTlvAgedOut
 * 
 *  DESCRIPTION   : This utility is used to handle age out of CEE control TLV.
 * 
 *  INPUT         : U4IfIndex - Interface Index. 
 * 
 *  OUTPUT        : None
 * 
 *  RETURNS       : None
 * 
 * **************************************************************************/
VOID 
CeeUtlHandleCtrlTlvAgedOut (UINT4 u4IfIndex)
{
    tCEECtrlEntry            *pCtrlEntry = NULL;
    tDcbxPortEntry           *pDcbxPortEntry = NULL;

    pCtrlEntry = CEEUtlGetCtrlEntry (u4IfIndex);
    if (pCtrlEntry == NULL)
    {
        DCBX_TRC_ARG2 (DCBX_FAILURE_TRC, "%s : Failed in fetching the Ctrl Entry for"
                " Port Id %d \r\n", __FUNCTION__, u4IfIndex);
        return;
    }

    pCtrlEntry->u4CtrlSeqNo     = DCBX_ZERO;
    pCtrlEntry->u4CtrlRcvdSeqNo = DCBX_ZERO;
    pCtrlEntry->u4CtrlAckNo     = DCBX_ZERO;
    pCtrlEntry->u4CtrlRcvdAckNo = DCBX_ZERO;
    pCtrlEntry->u4CtrlSyncNo    = DCBX_ZERO;
    pCtrlEntry->u4CtrlAckMissCount = DCBX_ZERO;

    /*During ageout if operversion is CEE and mode is auto switch to IEEE mode*/
    pDcbxPortEntry = DcbxUtlGetPortEntry (u4IfIndex);
    if (NULL != pDcbxPortEntry)
    {
        if ((DCBX_MODE_AUTO == pDcbxPortEntry->u1DcbxMode) && \
            (DCBX_DEFAULT_VER != pDcbxPortEntry->u1OperVersion))
        {
            DcbxUtlSwitchVersion(DCBX_DEFAULT_VER, u4IfIndex);
        }

        /* Reset the Peer Oper and Max Version to UNKNOWN */
        pDcbxPortEntry->u1PeerOperVersion = DCBX_VER_UNKNOWN;        
        pDcbxPortEntry->u1PeerMaxVersion = DCBX_VER_UNKNOWN;
    }
    
    return;
}

/************************************************************************
 * FUNCTION NAME : CEEUtlHandleTLV
 *
 * DESCRIPTION   : This utility is used to handle and process the CEE TLV.
 *
 * INPUT         : u4IfIndex - Interface Index. 
 *                 ApplTlvParam - CEE TLV information.
 *
 * OUTPUT        : None
 *
 * RETURNS       : None
 *
 ***************************************************************************/
VOID
CEEUtlHandleTLV (UINT4 u4IfIndex, tApplTlvParam * pApplTlvParam)
{
    UINT2               u2Value = DCBX_ZERO;
    UINT2               u2RxTlvLen = DCBX_ZERO;
    BOOL1       		bETSTlvFlag = DCBX_FAILURE; /*Flag used to track if ETS TLV recvd or not*/
    BOOL1 		        bPFCTlvFlag = DCBX_FAILURE; /*Flag used to track if PFC TLV recvd or not*/
    BOOL1       		bAppPriTlvFlag = DCBX_FAILURE; /*Flag used to track if APP Priority TLV recvd or not*/
    BOOL1               bSomethingChangedLoc = OSIX_FALSE;
    BOOL1               bFeatError = DCBX_DISABLED;
    BOOL1               bAPTlvRxd = OSIX_FALSE;
    BOOL1               bPFCTlvRxd = OSIX_FALSE;
    BOOL1               bETSTlvRxd = OSIX_FALSE;
    UINT1              *pu1Buf = NULL;
    tPFCPortEntry      *pPFCPortEntry = NULL;
    tETSPortEntry      *pETSPortEntry = NULL;
    tAppPriPortEntry   *pAppPriPortEntry = NULL;
    tCEECtrlEntry      *pCEECtrlEntry = NULL;
    tCeeTLVInfo         CeeTlvInfo;
    tDcbxCEETrapMsg     NotifyInfo;

    MEMSET (&NotifyInfo, DCBX_ZERO, sizeof (tDcbxCEETrapMsg));
    MEMSET (&CeeTlvInfo, DCBX_ZERO, sizeof (tCeeTLVInfo));

    pu1Buf = pApplTlvParam->au1DcbxTlv;

    pCEECtrlEntry = CEEUtlGetCtrlEntry (u4IfIndex);
    if (pCEECtrlEntry == NULL)
    {
        DCBX_TRC_ARG2 (DCBX_FAILURE_TRC, "In %s : Failed in fetching the Ctrl Entry"
                "for Port Id %d \r\n", __FUNCTION__, u4IfIndex);
        return;
    }
   
    /*
       <-----------------------6 octects------------------->
       ______________________________________________________
       |          |            |                |           |
       |  TLV     |   Length   | OUI = "001B21" | Subtype =2|
       | Type =127|            |   3 octets     | 1 octect  |
       |__________|____________|________________|___________|

    */

    /* SETP 1 : Skip till Protocol Sub TLV's start */ 
    u2RxTlvLen = pApplTlvParam->u2RxTlvLen;
    pu1Buf  = pu1Buf + CEE_TLV_HDR_LEN;
    u2RxTlvLen = (UINT2)(u2RxTlvLen - CEE_TLV_HDR_LEN);

    DCBX_TRC_ARG1 (DCBX_TLV_TRC, "In CEEUtlHandleTLV : "
            "Received DCBX TLV with length %d !!!\r\n",u2RxTlvLen);

    CeeTlvInfo.u4RemLastUpdateTime = pApplTlvParam->u4RemLastUpdateTime;
    CeeTlvInfo.i4RemIndex= pApplTlvParam->i4RemIndex;

    MEMCPY(CeeTlvInfo.RemDestMacAddr, 
            pApplTlvParam->RemLocalDestMacAddr, MAC_ADDR_LEN);
    
    CeeTlvInfo.u4RemLocalDestMACIndex =
                        pApplTlvParam->u4RemLocalDestMACIndex;

    CeeTlvInfo.u4IfIndex = u4IfIndex;

    while((u2RxTlvLen != 0) && (u2RxTlvLen < pApplTlvParam->u2RxTlvLen))
    { 
        /* STEP 2 : Parse Recieved TLV's one by one */
        /* Get the next 2 bytes (Type 7 bits + Length 9 bits) */
        DCBX_GET_2BYTE (pu1Buf, u2Value);
        u2RxTlvLen = (UINT2)(u2RxTlvLen - CEE_TLV_TYPE_LEN);
        CeeTlvInfo.u1TlvType = (UINT1) ((u2Value & CEE_TLV_TYPE_MASK) >> DCBX_TLV_LENGTH_IN_BITS);
        CeeTlvInfo.u2TlvLen =u2Value & CEE_TLV_LEN_MASK;

        /* Using the maximum Feat TLV len to intialize */
        MEMSET (CeeTlvInfo.au1CeeApplTlv, DCBX_ZERO, CEE_APP_PRI_MAX_TLV_LEN); 
        MEMCPY (CeeTlvInfo.au1CeeApplTlv, pu1Buf, CeeTlvInfo.u2TlvLen);
        pu1Buf = pu1Buf + CeeTlvInfo.u2TlvLen;
        MEMSET (&NotifyInfo, DCBX_ZERO, sizeof (tDcbxCEETrapMsg));

        switch(CeeTlvInfo.u1TlvType)
        {
            case CEE_CTRL_TLV_TYPE : /* CTRL TLV*/
                /*Check if Ctrl TLV has been recieved already */
                DCBX_TRC_ARG2 (DCBX_TLV_TRC, "In CEEUtlHandleTLV: "
                        "CEE CTRL TLV Received!!! for the port %d with length "
                        "%d \r\n", u4IfIndex, CeeTlvInfo.u2TlvLen);

                pCEECtrlEntry->u4CtrlRxTLVCount++;

                /* Send Counter update Info to Stand by node */
                DcbxCeeRedSendDynamicCtrlCounterInfo (pCEECtrlEntry,
                        DCBX_CEE_RED_CTRL_RX_COUNTER);

                CEEUtlHandleCtrlTLV (pCEECtrlEntry, &CeeTlvInfo);
                if (pCEECtrlEntry->u4CtrlSyncNo > pCEECtrlEntry->u4CtrlSeqNo)
                {
                    bSomethingChangedLoc = OSIX_TRUE;
                }

                break; 
            case CEE_ETS_TLV_TYPE : /* ETS TLV */
                bETSTlvRxd = OSIX_TRUE; /* Flag to know the TLV received */
                pETSPortEntry = ETSUtlGetPortEntry((UINT4) u4IfIndex);
                if(pETSPortEntry == NULL)
                {
                    DCBX_TRC_ARG1 (DCBX_TLV_TRC | DCBX_FAILURE_TRC, 
                            "In CEEUtlHandleTLV: ETS not enabled in port %d,"
                            "Hence ignoring the received TLV!!!\r\n", u4IfIndex);	
                    break;
                }

                if(bETSTlvFlag == DCBX_SUCCESS)
                {
                    DCBX_TRC_ARG1 (DCBX_TLV_TRC | DCBX_FAILURE_TRC, 
                            "In CEEUtlHandleTLV: "
                            "ETS Duplicate TLV has been received!!! "
                            "in port %d\r\n", u4IfIndex);	

                    CEEUtlHandlePktConfigErrors(CEE_ETS_TLV_TYPE, CeeTlvInfo,
                            CEE_EV_DUP_TLV_RECEIVED);
                    pETSPortEntry->u4ETSConfRxTLVError++;
                    break;
                }

                DCBX_TRC_ARG2 (DCBX_TLV_TRC, "In CEEUtlHandleTLV: "
                        "ETS TLV Received!!! in port %d with length %d !!!\r\n",
                        u4IfIndex, CeeTlvInfo.u2TlvLen);	

                bFeatError = pETSPortEntry->bETSError;
                CEESemRun (CEE_ETS_TLV_TYPE, &CeeTlvInfo,
                        CEE_EV_FEAT_TLV_RECEIVED);

                if (bFeatError != pETSPortEntry->bETSError)
                {
                    bSomethingChangedLoc = OSIX_TRUE;
                }
                bETSTlvFlag = DCBX_SUCCESS;
                break;

            case CEE_PFC_TLV_TYPE : /* PFC TLV */
                bPFCTlvRxd = OSIX_TRUE; /* Flag to know the TLV received */
                pPFCPortEntry = PFCUtlGetPortEntry ((UINT4) u4IfIndex);
                if(pPFCPortEntry == NULL)
                {
                    DCBX_TRC_ARG1 (DCBX_TLV_TRC | DCBX_FAILURE_TRC, 
                            "In CEEUtlHandleTLV: PFC not enabled in port %d, "
                            "Hence ignoring the received TLV!!!\r\n",
                            u4IfIndex);	
                    break;
                }

                if(bPFCTlvFlag == DCBX_SUCCESS)
                {
                    DCBX_TRC_ARG1 (DCBX_TLV_TRC | DCBX_FAILURE_TRC, 
                            "In CEEUtlHandleTLV: "
                            "PFC Duplicate TLV has been received!!! in port "
                            "%d\r\n", u4IfIndex);	

                    CEEUtlHandlePktConfigErrors(CEE_PFC_TLV_TYPE, CeeTlvInfo,
                            CEE_EV_DUP_TLV_RECEIVED);
                    pPFCPortEntry->u4PFCRxTLVError++;
                    break;
                }

                DCBX_TRC_ARG2 (DCBX_TLV_TRC, "CEEUtlHandleTLV:"
                        "PFC TLV Received!!! in port %d with length %d !!!\r\n",
                        u4IfIndex, CeeTlvInfo.u2TlvLen);	

                bFeatError = pPFCPortEntry->bPFCError;
                CEESemRun (CEE_PFC_TLV_TYPE,
                           &CeeTlvInfo, CEE_EV_FEAT_TLV_RECEIVED);

                if (bFeatError != pPFCPortEntry->bPFCError)
                {
                    bSomethingChangedLoc = OSIX_TRUE;
                }
                bPFCTlvFlag = DCBX_SUCCESS;
                break;

            case CEE_APP_PRI_TLV_TYPE : /* APP TLV */
                bAPTlvRxd = OSIX_TRUE; /* Flag to know the TLV received */
                pAppPriPortEntry = AppPriUtlGetPortEntry(u4IfIndex);
                if(pAppPriPortEntry == NULL)
                {
                    DCBX_TRC_ARG1 (DCBX_TLV_TRC | DCBX_FAILURE_TRC, 
                            "In CEEUtlHandleTLV: Application Priority not enabled "
                            "in port %d,Hence ignoring the received TLV!!!\r\n",
                            u4IfIndex);	
                    break;
                }

                if(bAppPriTlvFlag == DCBX_SUCCESS)
                {
                    DCBX_TRC_ARG1 (DCBX_TLV_TRC | DCBX_FAILURE_TRC, 
                            "In CEEUtlHandleTLV: "
                            "AppPri Duplicate TLV has been received!!! "
                            "in port %d\r\n", u4IfIndex);	

                    CEEUtlHandlePktConfigErrors(CEE_APP_PRI_TLV_TYPE,
                            CeeTlvInfo, CEE_EV_DUP_TLV_RECEIVED);
                    pAppPriPortEntry->u4AppPriTLVRxError++;
                    break;
                }

                DCBX_TRC_ARG2 (DCBX_TLV_TRC, "In CEEUtlHandleTLV: "
                        "Application Priority TLV Received!!! in port %d"
                        "with length %d \r\n", u4IfIndex, CeeTlvInfo.u2TlvLen);	

                bFeatError = pAppPriPortEntry->bAppPriError;
                CEESemRun (CEE_APP_PRI_TLV_TYPE, &CeeTlvInfo,
                           CEE_EV_FEAT_TLV_RECEIVED);

                if (bFeatError != pAppPriPortEntry->bAppPriError)
                {
                    bSomethingChangedLoc = OSIX_TRUE;
                }
                bAppPriTlvFlag = DCBX_SUCCESS;
                break;
            default : 
                DCBX_TRC (DCBX_FAILURE_TRC, "CEEUtlHandleTLV: "
                        "Received Invalid TLV Type!!!\r\n");	
                break;
        }
        u2RxTlvLen = (UINT2)(u2RxTlvLen - CeeTlvInfo.u2TlvLen);
    }

    if(bETSTlvFlag == DCBX_FAILURE)
    {
        pETSPortEntry = ETSUtlGetPortEntry((UINT4) u4IfIndex);
        if(pETSPortEntry != NULL)
        {
            NotifyInfo.u4IfIndex = u4IfIndex; 
            NotifyInfo.unTrapMsg.u1FeatType = ETS_TRAP;
            DcbxCEESendPortNotification(&NotifyInfo, DCBX_NO_FEAT_TLV_TRAP);

            DCBX_TRC_ARG1 (DCBX_TLV_TRC | DCBX_FAILURE_TRC, 
                    "In CEEUtlHandleTLV: "
                    "ETS Feature TLV has not been received in port %d!!!\r\n",
                    u4IfIndex);	
            CEESemRun (CEE_ETS_TLV_TYPE, &CeeTlvInfo,
                       CEE_EV_NO_FEAT_TLV_RECEIVED);
        }
        else
        {
            DCBX_TRC (DCBX_FAILURE_TRC, "CEEUtlHandleTLV: "
                    "ETS port entry not received!!!\r\n");	
        }
    }
    DCBX_TRC_ARG4 (DCBX_TLV_TRC, "Peer detected. NEW/UPDATE CEE Packet received "
            "with tlvs [PFC = %d ETS = %d AP = %d] "
            "on port %d!!!\r\n", bPFCTlvRxd, bETSTlvRxd, bAPTlvRxd,
            u4IfIndex);	

    if(bPFCTlvFlag == DCBX_FAILURE)
    {
        pPFCPortEntry = PFCUtlGetPortEntry ((UINT4) u4IfIndex);
        if(pPFCPortEntry != NULL)
        {
            NotifyInfo.u4IfIndex = u4IfIndex; 
            NotifyInfo.unTrapMsg.u1FeatType = PFC_TRAP;
            DcbxCEESendPortNotification(&NotifyInfo, DCBX_NO_FEAT_TLV_TRAP);

            DCBX_TRC_ARG1 (DCBX_TLV_TRC | DCBX_FAILURE_TRC, 
                    "In CEEUtlHandleTLV: "
                    "PFC Feature TLV has not been received!!! in port %d\r\n",
                    u4IfIndex);	
            CEESemRun (CEE_PFC_TLV_TYPE, &CeeTlvInfo,
                       CEE_EV_NO_FEAT_TLV_RECEIVED);
        }
        else
        {
            DCBX_TRC (DCBX_FAILURE_TRC, "CEEUtlHandleTLV: "
                    "PFC port entry not received!!!\r\n");	
        }
    }

    if(bAppPriTlvFlag == DCBX_FAILURE)
    {
        pAppPriPortEntry = AppPriUtlGetPortEntry ((UINT4) u4IfIndex);
        if(pAppPriPortEntry != NULL)
        {
            NotifyInfo.u4IfIndex = u4IfIndex; 
            NotifyInfo.unTrapMsg.u1FeatType = APP_PRI_TRAP;
            DcbxCEESendPortNotification(&NotifyInfo, DCBX_NO_FEAT_TLV_TRAP);

            DCBX_TRC_ARG1 (DCBX_TLV_TRC | DCBX_FAILURE_TRC, "In CEEUtlHandleTLV: "
                    "Applicaiton Priority Feature TLV has not been received!!!"
                    " in port %d\r\n", u4IfIndex);	
            CEESemRun (CEE_APP_PRI_TLV_TYPE, &CeeTlvInfo, CEE_EV_NO_FEAT_TLV_RECEIVED);
        }
        else
        {
            DCBX_TRC (DCBX_FAILURE_TRC, "CEEUtlHandleTLV: "
                    "Application Priority port entry not received!!!\r\n");	
        }
    }

    if (pCEECtrlEntry->u4CtrlAckNo != pCEECtrlEntry->u4CtrlRcvdSeqNo)
    {
        if (bSomethingChangedLoc == OSIX_TRUE)
        {
            pCEECtrlEntry->u4CtrlSeqNo++;
        }
        DCBX_TRC_ARG1 (DCBX_TLV_TRC, "CEEUtlHandleTLV: "
                    "Info: New CEE Packet received "
                    "on port %d!!!\r\n", u4IfIndex);	
        pCEECtrlEntry->u4CtrlAckNo = pCEECtrlEntry->u4CtrlRcvdSeqNo; 
        CEEFormAndSendTLV (u4IfIndex, DCBX_LLDP_CEE_ACK, DCBX_ZERO);
    }
    else if (bSomethingChangedLoc == OSIX_TRUE)
    {
        DCBX_TRC_ARG1 (DCBX_TLV_TRC, "CEEUtlHandleTLV: "
                    "Info: CEE update pkt received "
                    "on port %d!!!\r\n", u4IfIndex);	
        pCEECtrlEntry->u4CtrlAckNo = pCEECtrlEntry->u4CtrlRcvdSeqNo; 
        CEEFormAndSendTLV (u4IfIndex, DCBX_LLDP_PORT_UPDATE, DCBX_ZERO);
    }
    return;
}

/************************************************************************
 * FUNCTION NAME : CEEUtlHandleCtrlTLV
 *
 * DESCRIPTION   : This utility is used to handle and process the CEE CTRL
 *                 TLV.
 *
 * INPUT         : pCEECtrlEntry - CEE Ctrl Table Entry.
 *                 CeeTLVInfo - CTRL TLV information.
 *
 * OUTPUT        : None
 *
 * RETURNS       : None
 *
 ***************************************************************************/
VOID
CEEUtlHandleCtrlTLV (tCEECtrlEntry *pCEECtrlEntry, tCeeTLVInfo *pCeeTLVInfo)
{
	UINT4               u4Value = DCBX_ZERO;
	UINT1              *pu1Buf = NULL;

    /*
     DCBx CONTROL TLV
    _________________________________________________________________________________________
    |          |            |           |           |                  |                    |
    |          |            | Operating | Max       |                  |                    |
    | Type = 1 | Length= 10 | Version   | Version   |    Seq No        |      Ack No        |
    |__________|____________|___________|___________|__________________|____________________|
       7 bits       9 bits    1 octet      1 octet       4 octet               4 octet       
                                                                 
    */

	pu1Buf = pCeeTLVInfo->au1CeeApplTlv;
	pu1Buf += CEE_TLV_TYPE_LEN;  /*Skip Oper Version and Max Version octets*/

	/*Fetch SeqNo*/
	DCBX_GET_4BYTE(pu1Buf, u4Value);
	pCEECtrlEntry->u4CtrlRcvdSeqNo = u4Value;

	/*Fetch Rcvd AckNo*/
	DCBX_GET_4BYTE(pu1Buf, u4Value); 
	pCEECtrlEntry->u4CtrlRcvdAckNo = u4Value;
    if (pCEECtrlEntry->u4CtrlSeqNo == pCEECtrlEntry->u4CtrlRcvdAckNo)
    {
        DCBX_TRC (DCBX_TLV_TRC, "In CEEUtlHandleCtrlTLV: "
                "Acknowledgement received hence resetting "
                "the u4CtrlAckMissCount!!!\r\n");
        pCEECtrlEntry->u4CtrlAckMissCount = DCBX_ZERO;
    }
	return;
}

/*************************************************************************
 * FUNCTION NAME : CEEHandlePFCTlv
 *
 * DESCRIPTION   : This utility is used to process and fill the Remote 
 * 		           values in PFC TLV.
 *
 * INPUT         : pPFCPortEntry - PFC Table Entry.
 *                 pCeeFeatTLVInfo - Recieved TLV information.
 *                 pu1OperState - Operating State
 *
 * OUTPUT        : None
 *
 * RETURNS       : None
 *
 ***************************************************************************/
INT4
CEEHandlePFCTlv (tPFCPortEntry *pPFCPortEntry, tCeeTLVInfo *pCeeFeatTLVInfo,
                 UINT1 *pu1OperState)
{
    UINT2   	    u2BitPos 	    = DCBX_ZERO; 
    UINT1   	    u1Value 	    = DCBX_ZERO;
    UINT1   	    u1Capability 	= DCBX_ZERO;
    UINT1   	    u1Willing 	    = DCBX_ZERO; 
    UINT1   	    u1Error     	= DCBX_ZERO; 
    UINT1   	    u1PeerEnable	= DCBX_ZERO; 
    UINT1   	    u1Count 	    = DCBX_ZERO;
    UINT1   	    u1PFCEnabled	= DCBX_ZERO; 
    UINT1           u1PfcCap        = DCBX_ZERO;
    UINT1  		    *pu1PfcStatus   = NULL;
    UINT1  		    *pu1Buf 	    = NULL;
    BOOL1   	    bResult	        = OSIX_FALSE;
    BOOL1    	    bCompatibility  = OSIX_FALSE;

    /*
    * PRIORITY-BASED FLOW CONTROL TLV
    ___________________________________________________________________________________________________________
    |          |            |           |           |  |  |   |        |          |             |             |   
    |          |            | Operating | Max       |  |  |   |  Re-   |          | PFC Enabled | Max TCs W/  |
    | Type = 3 | Length= 6  | Version   | Version   |En|W.|Err| served | SubType  |   Table     | PFC Support |
    |__________|____________|___________|___________|__|__|___|________|__________|_____________|_____________|
       7 bits       9 bits    1 octet      1 octet   1  1   1   5 bits   1 octect    1 octet        1 octect
                                                    bit bit bit
    */

    SET_DCBX_STATUS (pPFCPortEntry, pPFCPortEntry->u1PFCDcbxSemType, 
            DCBX_RED_PFC_DCBX_STATUS_INFO, DCBX_STATUS_UNKNOWN);

    DCBX_TRC_ARG1 (DCBX_TLV_TRC , "CEEHandlePFCTlv: "
            "Received CEE PFC TLV!!!, for the port %d\r\n",
            pPFCPortEntry->u4IfIndex);

    if (pCeeFeatTLVInfo->u2TlvLen != CEE_PFC_TLV_PAYLOAD_LEN)
    {
        DCBX_TRC_ARG1 (DCBX_TLV_TRC | DCBX_FAILURE_TRC, "In PFCUtlHandleTLV: "
                "PFC Received TLV length is incorrect !!!, Length Received[%d]"
                "\r\n", pCeeFeatTLVInfo->u2TlvLen);
        /* Incremet the PFC TLV Error Counter */
        pPFCPortEntry->u4PFCRxTLVError++;
        /* Send Counter Update Info to Standby */
        return DCBX_FAILURE;
    }

    pu1Buf = pCeeFeatTLVInfo->au1CeeApplTlv;
    pu1Buf += CEE_TLV_TYPE_LEN;  /*Skip Oper Version and Max Version octets*/

    /*Fetch Enable, Willing and Error bit*/
    DCBX_GET_1BYTE(pu1Buf, u1Value); 
    u1PeerEnable = u1Value & CEE_ENABLE_BIT_MASK;
    if (!u1PeerEnable)
    {
        u1PeerEnable  = PFC_DISABLED;
        SET_DCBX_STATUS (pPFCPortEntry, pPFCPortEntry->u1PFCDcbxSemType,
                DCBX_RED_PFC_DCBX_STATUS_INFO, DCBX_STATUS_PEER_DISABLED);

        DCBX_TRC (DCBX_TLV_TRC , "CEEHandlePFCTlv: "
                "Feature disabled in peer!\r\n");
        return DCBX_FAILURE;
    }

    u1Willing = u1Value & CEE_WILLING_BIT_MASK;
    if (u1Willing)
    {
        u1Willing = PFC_ENABLED;
    }
    else
    {
        u1Willing  = PFC_DISABLED;
    }

    u1Error = u1Value & CEE_ERROR_BIT_MASK;

    pPFCPortEntry->bPFCPeerWilling = (BOOL1) u1Willing;

    /* Fetch the sub type octet */
    DCBX_GET_1BYTE(pu1Buf, u1Value);
    if (u1Value != DCBX_ZERO)
    {
        DCBX_TRC_ARG1 (DCBX_FAILURE_TRC , "In CEEHandlePFCTlv: "
                "Invalid sub type value %d !\r\n",u1Value);
        pPFCPortEntry->u4PFCRxTLVError++;
        return DCBX_FAILURE;
    }

    /* Fetch PFC Enabled Table */
    DCBX_GET_1BYTE(pu1Buf, u1Value); 
    u1PFCEnabled = u1Value;

    /* Fetch Max TC supported / PFC capability */
    DCBX_GET_1BYTE(pu1Buf, u1Value); 
    u1Capability = u1Value; 

    /* Number of priorities reported in the PFC Enable field must not exceed
     * the number of priorities supported in the PFC Cap Filed */
    for (u2BitPos = DCBX_MIN_VAL; u2BitPos <= DCBX_MAX_PRIORITIES; u2BitPos++)
    {
        pu1PfcStatus = &u1PFCEnabled;

        DcbxUtlOsixBitlistIsBitSet (pu1PfcStatus, u2BitPos,
                DCBX_MIN_VAL, &bResult);

        if (bResult == OSIX_TRUE)
        {
            u1Count = (UINT1) (u1Count + 1);
        }
    }

    if (((u1PfcCap == DCBX_ZERO) && (u1Count <= DCBX_MAX_PRIORITIES))
        || (u1Count <= u1PfcCap))
    {
        pPFCPortEntry->PFCRemPortInfo.i4PFCRemIndex = pCeeFeatTLVInfo->i4RemIndex;
        pPFCPortEntry->PFCRemPortInfo.u4PFCRemTimeMark =
            pCeeFeatTLVInfo->u4RemLastUpdateTime;
        pPFCPortEntry->PFCRemPortInfo.u4PFCDestRemMacIndex =
                        pCeeFeatTLVInfo->u4RemLocalDestMACIndex;

        MEMCPY (pPFCPortEntry->PFCRemPortInfo.au1PFCDestRemMacAddr,
                pCeeFeatTLVInfo->RemDestMacAddr, MAC_ADDR_LEN);
        /* Copy the Remote index paramters to the Remote Table for this port */
        pPFCPortEntry->PFCRemPortInfo.u1PFCRemStatus = u1PFCEnabled;
        pPFCPortEntry->PFCRemPortInfo.u1PFCRemCap = u1Capability;
        pPFCPortEntry->PFCRemPortInfo.u1PFCRemWilling = u1Willing;
        DCBX_TRC (DCBX_TLV_TRC , "In CEEHandlePFCTlv: "
                "Remote Port Info Successfully filled with all values!!\r\n");
    }
    else
    {
        DCBX_TRC (DCBX_TLV_TRC | DCBX_FAILURE_TRC, "In PFCUtlHandleTLV: "
                "Number of PFC Enbaled Priorities exceeds PFC Capability !!!\r\n");
        /* Incremet the PFC TLV Error Counter */
        pPFCPortEntry->u4PFCRxTLVError++;
        /* Send Counter Update Info to Standby */
        return DCBX_FAILURE;
    }

    bCompatibility = PFCApiIsCfgCompatible(pPFCPortEntry);

    *pu1OperState =  CEEUtlCalculateOperState(pPFCPortEntry->PFCRemPortInfo.u1PFCRemWilling, 
            pPFCPortEntry->PFCAdmPortInfo.u1PFCAdmWilling,
            bCompatibility);

    if((*pu1OperState != CEE_OPER_DISABLED) &&
       (pPFCPortEntry->u1DcbxStatus != DCBX_STATUS_PEER_NW_CFG_COMPAT) &&
       (pPFCPortEntry->u1DcbxStatus != DCBX_STATUS_CFG_NOT_COMPT))
    {
        SET_DCBX_STATUS (pPFCPortEntry, pPFCPortEntry->u1PFCDcbxSemType, 
                DCBX_RED_PFC_DCBX_STATUS_INFO, DCBX_STATUS_OK);
    }

    if (u1Error)
    {
        SET_DCBX_STATUS (pPFCPortEntry, pPFCPortEntry->u1PFCDcbxSemType, 
                DCBX_RED_PFC_DCBX_STATUS_INFO, DCBX_STATUS_PEER_IN_ERROR);
    }
    return DCBX_SUCCESS;
}

/************************************************************************
 * FUNCTION NAME : CEEHandleETSTlv
 *
 * DESCRIPTION   : This utility is used to process and fill the Remote 
 *   		       values in ETS TLV.
 *
 * INPUT         : pETSPortEntry - ETS Table Entry.
 *                 pCeeFeatTLVInfo - Recieved TLV information.
 *                 pu1OperState - Operating State
 *
 * OUTPUT        : None
 *
 * RETURNS       : None
 *
 ***************************************************************************/
INT4
CEEHandleETSTlv (tETSPortEntry *pETSPortEntry, tCeeTLVInfo *pCeeFeatTLVInfo,
                 UINT1 *pu1OperState)
{
    UINT4         u4PriMapping    = DCBX_ZERO;
    UINT1		  u1Value         = DCBX_ZERO;
    UINT1         u1RemMaxTCG     = DCBX_ZERO;
    UINT1         u1LocalMaxTCG   = DCBX_ZERO;
    UINT1   	  u1Error 	      = DCBX_ZERO; 
    UINT1   	  u1PeerEnable	  = DCBX_ZERO; 
    UINT1         u1Willing       = DCBX_ZERO;
    UINT1   	  *pu1Buf         = NULL;
    UINT1         au1EtsTCGID[DCBX_MAX_PRIORITIES] = { DCBX_ZERO };
    UINT1         au1EtsBW[ETS_MAX_TCGID_CONF]     = { DCBX_ZERO };

    DCBX_TRC_ARG1 (DCBX_TLV_TRC , "CEEHandleETSTlv: "
            "Received CEE ETS TLV on port %d\r\n",
            pETSPortEntry->u4IfIndex);

    SET_DCBX_STATUS(pETSPortEntry, pETSPortEntry->u1ETSDcbxSemType, 
            DCBX_RED_ETS_DCBX_STATUS_INFO, DCBX_STATUS_UNKNOWN);

    if (pCeeFeatTLVInfo->u2TlvLen != CEE_PG_TLV_PAYLOAD_LEN)
    {
        /* If TLV length is not correct, then do not process the TLV */
        DCBX_TRC (DCBX_TLV_TRC | DCBX_FAILURE_TRC, "In CEEHandleETSTlv: "
                "ETS Conf Received TLV length is incorrect !!!\r\n");
        /* Update received Error Config TLV Counters */
        pETSPortEntry->u4ETSConfRxTLVError = pETSPortEntry->u4ETSConfRxTLVError + 1;

        /* Send Counter Update Info to Standby */
        return DCBX_FAILURE;
    }

    /*
    PRIORITY-BASED FLOW CONTROL TLV
    _______________________________________________________________________________________________________________________________________
    |          |            |           |           |  |  |   |      |          |                      |                         |        |  
    |          |            | Operating | Max       |  |  |   | Re-  |          | Priority grouping to |Priority grouping Percent|Max Num.|
    | Type = 2 | Length= 17 | Version   | Version   |En|W.|Err|served| SubType  |        priority      |       Allocation        | of TCs |
    |__________|____________|___________|___________|__|__|___|______|__________|______________________|_________________________|________|
       7 bits       9 bits    1 octet      1 octet   1  1   1    5     1 octect        4 octets                8 octects          1 octet
                                                    bit bit bit bits 
    */

    pu1Buf =  pCeeFeatTLVInfo->au1CeeApplTlv; 
    pu1Buf += CEE_TLV_TYPE_LEN;  /*Skip Oper Version and Max Version octets*/

    /*Fetch Enable, Willing and Error bit*/
    DCBX_GET_1BYTE(pu1Buf, u1Value);
    u1PeerEnable = u1Value & CEE_ENABLE_BIT_MASK;
    if (!u1PeerEnable)
    {
        u1PeerEnable  = ETS_DISABLED;

        SET_DCBX_STATUS(pETSPortEntry, pETSPortEntry->u1ETSDcbxSemType, 
                DCBX_RED_ETS_DCBX_STATUS_INFO, DCBX_STATUS_PEER_DISABLED);
        DCBX_TRC (DCBX_TLV_TRC , "In CEEHandleETSTlv: "
                "Feature disabled in peer!\r\n");
        return DCBX_FAILURE;
    }

    u1Willing = u1Value & CEE_WILLING_BIT_MASK;
    if (u1Willing)
    {
        u1Willing = ETS_ENABLED;
    }
    else
    {
        u1Willing  = ETS_DISABLED;
    }

    u1Error = u1Value & CEE_ERROR_BIT_MASK;
    pETSPortEntry->bETSPeerWilling = (BOOL1)u1Willing;

    /* Fetch the sub type octet */
    DCBX_GET_1BYTE(pu1Buf, u1Value);
    if (u1Value != DCBX_ZERO)
    {
        DCBX_TRC_ARG1 (DCBX_TLV_TRC , "In CEEHandleETSTlv: "
                "Invalid sub type value %d !\r\n",u1Value);
        pETSPortEntry->u4ETSConfRxTLVError = pETSPortEntry->u4ETSConfRxTLVError + 1;
        return DCBX_FAILURE;
    }

    /* Get the Priorrity assignment Octets of length 4 */
    DCBX_GET_4BYTE (pu1Buf, u4PriMapping);
    /* Copy the Priority Assignment table from TLV */
    ETS_FILL_PRI_MAPPING_FROM_TLV (u4PriMapping, au1EtsTCGID);

    /* Copy the Bandwidth paramters from TLV */
    ETS_FILL_TCG_BW_FROM_TLV (pu1Buf, au1EtsBW);
    u1LocalMaxTCG = pETSPortEntry->ETSAdmPortInfo.u1ETSAdmNumTCGSup;

    if (u1LocalMaxTCG == DCBX_ZERO)
    {
        u1LocalMaxTCG = ETS_MAX_TCGID_CONF;
    }
    /*Fetch Max NumTCsSupported */
    DCBX_GET_1BYTE(pu1Buf, u1Value);
    u1RemMaxTCG = u1Value;

    if ((u1RemMaxTCG == DCBX_ZERO) || (u1RemMaxTCG > ETS_MAX_TCGID_CONF))
    {
        DCBX_TRC_ARG1 (DCBX_TLV_TRC , "In CEEHandleETSTlv: "
                "Invalid TC value %d !\r\n",u1RemMaxTCG);
        pETSPortEntry->u4ETSConfRxTLVError = pETSPortEntry->u4ETSConfRxTLVError + 1;
        return DCBX_FAILURE;
    }

    if (ETSUtlValidateTCG (u1LocalMaxTCG, u1RemMaxTCG,
                au1EtsTCGID,
                au1EtsBW,
                NULL) == OSIX_FAILURE)
    {
        DCBX_TRC (DCBX_TLV_TRC | DCBX_FAILURE_TRC, "In CEEHandleETSTlv: "
                "ETS validation failed at ETSUtlValidateTCG!!\r\n");
        return DCBX_FAILURE;
    }
    pETSPortEntry->ETSRemPortInfo.i4ETSRemIndex = pCeeFeatTLVInfo->i4RemIndex;
    pETSPortEntry->ETSRemPortInfo.u4ETSRemTimeMark =
        pCeeFeatTLVInfo->u4RemLastUpdateTime;
    MEMCPY (pETSPortEntry->ETSRemPortInfo.au1ETSDestRemMacAddr,
            pCeeFeatTLVInfo->RemDestMacAddr, MAC_ADDR_LEN);
    pETSPortEntry->ETSRemPortInfo.u4ETSDestRemMacIndex =
                        pCeeFeatTLVInfo->u4RemLocalDestMACIndex;

    /* Copy the Remote paramters to the Remote Table */
    pETSPortEntry->ETSRemPortInfo.u1ETSRemWilling = u1Willing;
    if (u1RemMaxTCG == ETS_MAX_TCGID_CONF)
    {
        pETSPortEntry->ETSRemPortInfo.u1ETSRemNumTCGSup = DCBX_ZERO;
        pETSPortEntry->ETSRemPortInfo.u1ETSRemNumTcSup = DCBX_ZERO;
    }
    else
    {
        pETSPortEntry->ETSRemPortInfo.u1ETSRemNumTCGSup = u1RemMaxTCG;
        pETSPortEntry->ETSRemPortInfo.u1ETSRemNumTcSup = u1RemMaxTCG;
    }
    /* Copy the Priority Assignement paramters to the Remote Table */
    MEMCPY (pETSPortEntry->ETSRemPortInfo.au1ETSRemTCGID, au1EtsTCGID,
            DCBX_MAX_PRIORITIES);

    /* Copy the Bandwidth paramters to the Remote table */
    MEMCPY (pETSPortEntry->ETSRemPortInfo.au1ETSRemBW, au1EtsBW,
            ETS_MAX_TCGID_CONF);

    /* Set the Remote Updation Status for this TLV */
    pETSPortEntry->u1RemTlvUpdStatus |= ETS_CEE_TLV_REM_UPD;

    ETSApiIsCfgCompatible(pETSPortEntry);

    *pu1OperState =
        CEEUtlCalculateOperState(pETSPortEntry->ETSRemPortInfo.u1ETSRemWilling,
                pETSPortEntry->ETSAdmPortInfo.u1ETSAdmWilling, OSIX_TRUE);

    if((*pu1OperState != CEE_OPER_DISABLED) &&
            (pETSPortEntry->u1DcbxStatus != DCBX_STATUS_PEER_NW_CFG_COMPAT) &&
            (pETSPortEntry->u1DcbxStatus != DCBX_STATUS_CFG_NOT_COMPT))
    {
        SET_DCBX_STATUS(pETSPortEntry, pETSPortEntry->u1ETSDcbxSemType, 
                DCBX_RED_ETS_DCBX_STATUS_INFO, DCBX_STATUS_OK);
    }

    if (ETSUtlCompareTCG (pETSPortEntry->ETSAdmPortInfo.au1ETSAdmTCGID, 
            pETSPortEntry->ETSRemPortInfo.au1ETSRemTCGID) == OSIX_FAILURE) 
    {
        /*Deviation from standard: To support CISCO-LIKE behaviour,if local 
         * number of TCs configured differs Peer number of TCs configured, 
         * return cfg-not-compatible and send error bit*/ 
        SET_DCBX_STATUS(pETSPortEntry, pETSPortEntry->u1ETSDcbxSemType, 
                DCBX_RED_ETS_DCBX_STATUS_INFO, DCBX_STATUS_CFG_NOT_COMPT);
        *pu1OperState = CEE_OPER_DISABLED;
    }

    /*
     *  Deviation from standard: 
     *  For the requirement of the following implementation, DCBX Status
     *  setting based on PEER bit shall not be considered :-
     *
     *  1. if local number of TCs configured != Peer number
     *  of TCs configured, return cfg-not-compatible. 
     *  2. if local number of TCs configured == Peer number of TCs configured,
     *  ignore error bit from the peer and do not disable DCB feature.
     *
     else if (u1Error)
     {
        SET_DCBX_STATUS(pETSPortEntry, pETSPortEntry->u1ETSDcbxSemType, 
        DCBX_RED_ETS_DCBX_STATUS_INFO, DCBX_STATUS_PEER_IN_ERROR);
     }
    */

    DCBX_TRC_ARG1 (DCBX_TLV_TRC, "Exit func CEEHandleETSTlv: u1Error = %d\r\n",
            u1Error);
    return DCBX_SUCCESS;
}

/************************************************************************
 * FUNCTION NAME : CEEHandleAppPriTlv
 *
 * DESCRIPTION   : This utility is used to process and fill the Remote 
 *   		       values in App Pri TLV.
 *
 * INPUT         : pAppPriPortEntry - App Pri Table Port.
 *                 pCeeFeatTLVInfo - Recieved TLV information.
 *                 pu1OperState - Operating State.
 *
 * OUTPUT        : None
 *
 * RETURNS       : OperState
 *
 ***************************************************************************/
INT4 
CEEHandleAppPriTlv(tAppPriPortEntry *pAppPriPortEntry, 
        tCeeTLVInfo *pCeeFeatTLVInfo, UINT1 *pu1OperState) 
{
    UINT4               u4ProtocolCount = DCBX_ZERO;
    INT4                i4Selector = DCBX_ZERO;
    UINT2               u2Temp = DCBX_ZERO;
    UINT2               u2Length = DCBX_ZERO;
    UINT2               u2Protocol = DCBX_ZERO;
    UINT1               u1Value = DCBX_ZERO;
    UINT1        	    *pu1Buf = NULL;
    UINT1               u1Priority = DCBX_ZERO;
    UINT1   	        u1Error = DCBX_ZERO; 
    UINT1   	        u1PeerEnable = DCBX_ZERO; 
    UINT1               u1Willing = DCBX_ZERO;
    BOOL1               bCompatibility = OSIX_FALSE;
    tAppPriMappingEntry *pAppPriMappingEntry = NULL;

    DCBX_TRC_ARG1 (DCBX_TLV_TRC , "CEEHandleAppPriTlv: "
            "Received CEE AppPri TLV on port %d\r\n",
            pAppPriPortEntry->u4IfIndex);
    /*
     * APPLICATION PRIORITY TLV HEADER
    _________________________________________________________________________________________________________
    |          |            |           |           |  |  |   |        |            |                       |    
    |          |            | Operating | Max       |  |  |   |        |            |                       |    
    | Type = 4 | Length= 16 | Version   | Version   |En|W.|Err|Reserved|Sub_Type = 0|   Application Table   |  
    |__________|____________|___________|___________|__|__|___|________|____________|_______________________|
       7 bits       9 bits    1 octet      1 octet   1  1   1   5 bits   1 octect      Multiple of 6 octets
                                                    bit bit bit
    */

    SET_DCBX_STATUS(pAppPriPortEntry, pAppPriPortEntry->u1AppPriDcbxSemType, 
            DCBX_RED_APP_PRI_DCBX_STATUS_INFO, DCBX_STATUS_UNKNOWN); 

    pu1Buf =  pCeeFeatTLVInfo->au1CeeApplTlv; 
    pu1Buf += CEE_TLV_TYPE_LEN;/*Skip Oper Version and Max Version octets*/
    u2Length = pCeeFeatTLVInfo->u2TlvLen;

    /*Fetch Enable, Willing and Error bit*/
    DCBX_GET_1BYTE(pu1Buf, u1Value);
    u1PeerEnable = u1Value & CEE_ENABLE_BIT_MASK;
    if (!u1PeerEnable)
    {
        u1PeerEnable  = APP_PRI_DISABLED;
        SET_DCBX_STATUS (pAppPriPortEntry, pAppPriPortEntry->u1AppPriDcbxSemType, 
                DCBX_RED_APP_PRI_DCBX_STATUS_INFO, DCBX_STATUS_PEER_DISABLED);
        DCBX_TRC (DCBX_TLV_TRC , "CEEHandleAppPriTlv: "
                "Feature disabled in peer!\r\n");
        return DCBX_FAILURE;
    }

    u1Willing = u1Value & CEE_WILLING_BIT_MASK;
    if (u1Willing)
    {
        u1Willing = APP_PRI_ENABLED;
    }
    else
    {
        u1Willing  = APP_PRI_DISABLED;
    }

    u1Error = u1Value & CEE_ERROR_BIT_MASK;
    /* Fetch the sub type octet */
    DCBX_GET_1BYTE(pu1Buf, u1Value);
    if (u1Value != DCBX_ZERO)
    {
        DCBX_TRC_ARG1 (DCBX_TLV_TRC , "CEEHandleAppPriTlv: "
                "Invalid sub type value %d !\r\n",u1Value);
        pAppPriPortEntry->u4AppPriTLVRxError++;
        return DCBX_FAILURE;
    }

    u2Length = (UINT2)(u2Length - CEE_APP_PRI_TLV_INFO_STR_LEN);
    if ((u2Length % CEE_APP_PRI_TABLE_ENTRY_LEN) != DCBX_ZERO)
    {
        /* If the length is not correct then do not process the TLV  */
        DCBX_TRC ((DCBX_TLV_TRC | DCBX_FAILURE_TRC), "AppPriUtlHandleTLV: "
                "Application Priority Received TLV Length is incorrect !!!\r\n");
        /* Incremet the Application Priority TLV Error Counter */
        pAppPriPortEntry->u4AppPriTLVRxError++;
        /* Send Counter Update Info to Standby */
        return DCBX_FAILURE;
    }
    pAppPriPortEntry->bAppPriPeerWilling = (BOOL1)u1Willing;

    AppPriUtlDeleteRemoteMappingEntries (pAppPriPortEntry->u4IfIndex);

    pAppPriPortEntry->AppPriRemPortInfo.u1AppPriRemWilling = u1Willing;

    while (u2Length >= CEE_APP_PRI_TABLE_ENTRY_LEN)
    {
        u4ProtocolCount++;
        /*Get Protocol Id */
        DCBX_GET_2BYTE (pu1Buf, u2Protocol);

        /*Get Selection Field */
        DCBX_GET_1BYTE (pu1Buf, u1Value);

        i4Selector = u1Value & CEE_APP_PRI_SELECTOR_BIT_MASK;
        /* Check if the selector received is a valid selector */
        if (i4Selector == CEE_APP_PRI_ETHERTYPE_SEL)
        {
            i4Selector = CEE_APP_PRI_IEEE_ETHERTYPE_SEL; 
        }
        else if (i4Selector == CEE_APP_PRI_TCP_UDP_SEL)
        {
            i4Selector = CEE_APP_PRI_IEEE_TCP_UDP_SEL; 
        }
        else
        {
            DCBX_TRC_ARG3 (DCBX_TLV_TRC, "In %s : "
                    "Received invalid selector %d on port %d \r\n",
                    __FUNCTION__, i4Selector,pAppPriPortEntry->u4IfIndex);
            return DCBX_FAILURE;
        }

        DCBX_GET_2BYTE (pu1Buf, u2Temp);

        /*Get User Map Priority Value */
        DCBX_GET_1BYTE (pu1Buf, u1Priority);
        APP_PRI_GET_UPM_FRM_CEE_TO_IEEE (u1Priority);

        pAppPriMappingEntry = AppPriUtlGetAppPriMappingEntry
            (pAppPriPortEntry->u4IfIndex, i4Selector,
             (INT4) u2Protocol, (UINT1) APP_PRI_REMOTE);
        if (pAppPriMappingEntry == NULL)
        {
            pAppPriMappingEntry = AppPriUtlCreateAppPriMappingTblEntry
                (pAppPriPortEntry->u4IfIndex, i4Selector,
                 (INT4) u2Protocol, (INT1) APP_PRI_REMOTE);
            if (pAppPriMappingEntry == NULL)
            {
                DCBX_TRC_ARG4 (DCBX_TLV_TRC, "In %s : "
                        "AppPriUtlCreateAppPriMappingTblEntry: "
                        "Failed for port %d selector %d Protocol %d \r\n",
                        __FUNCTION__, pAppPriPortEntry->u4IfIndex, i4Selector,
                        u2Protocol);
                return DCBX_FAILURE;
            }
        }

        pAppPriMappingEntry->u1AppPriPriority = u1Priority;
        pAppPriMappingEntry->u1RowStatus = (UINT1)ACTIVE;
        u2Length = (UINT2) (u2Length - CEE_APP_PRI_TABLE_ENTRY_LEN);
    } /* end of while */

    pAppPriPortEntry->AppPriRemPortInfo.i4AppPriRemIndex = pCeeFeatTLVInfo->i4RemIndex;
    pAppPriPortEntry->AppPriRemPortInfo.u4AppPriRemTimeMark =
        pCeeFeatTLVInfo->u4RemLastUpdateTime;
    MEMCPY (pAppPriPortEntry->AppPriRemPortInfo.au1AppPriDestRemMacAddr,
            pCeeFeatTLVInfo->RemDestMacAddr, MAC_ADDR_LEN);
    pAppPriPortEntry->AppPriRemPortInfo.u4AppPriDestRemMacIndex =
                        pCeeFeatTLVInfo->u4RemLocalDestMACIndex;

    /*Update the number of protocols in last received TLV for this Port */
    pAppPriPortEntry->u4AppPriAppProtocols = u4ProtocolCount;

    bCompatibility = AppPriApiIsCfgCompatible(pAppPriPortEntry);

    *pu1OperState =
        CEEUtlCalculateOperState(pAppPriPortEntry->AppPriRemPortInfo.u1AppPriRemWilling,
                pAppPriPortEntry->AppPriAdmPortInfo.u1AppPriAdmWilling,
                bCompatibility); 

    /* Deviation from standard: ignore PEER IN ERROR for special case */
    if ((pAppPriPortEntry->u1DcbxStatus != DCBX_STATUS_CFG_NOT_COMP_BUT_SUPP)
            && (u1Error))
    {
        SET_DCBX_STATUS(pAppPriPortEntry, pAppPriPortEntry->u1AppPriDcbxSemType, 
                DCBX_RED_APP_PRI_DCBX_STATUS_INFO, DCBX_STATUS_PEER_IN_ERROR);
    }
    else if (pAppPriPortEntry->u1DcbxStatus == DCBX_STATUS_CFG_NOT_COMP_BUT_SUPP)
    {
        /*In DCBX_STATUS_CFG_NOT_COMP_BUT_SUPP state always local cfg will be 
         * used*/
        *pu1OperState = CEE_OPER_USE_LOCAL_CFG;
    }
    return DCBX_SUCCESS;
}


/************************************************************************
 * FUNCTION NAME : CEEUtlCalculateOperState
 *
 * DESCRIPTION   : Feature State Machine to compute the oper state 
 *
 * INPUT         : u1RemWilling - Remote Willing Status
 *    			   u1AdmWilling - Admin Willing Status
 *   		 	   bIsCompatible - Compatibility Value
 *
 * OUTPUT        : None
 *
 * RETURNS       : OperState
 *
 ***************************************************************************/
UINT1
CEEUtlCalculateOperState (UINT1 u1RemWilling, UINT1 u1AdmWilling, 
                         BOOL1 bIsCompatible)
{
	if(u1RemWilling == u1AdmWilling)
	{
		if(bIsCompatible)
		{
			DCBX_TRC (DCBX_TLV_TRC , "In CEEUtlCalculateOperState: "
					"OperState = CEE_USE_LOCAL_CFG\r\n");
			return CEE_OPER_USE_LOCAL_CFG;
		}
		else
		{
			DCBX_TRC (DCBX_TLV_TRC , "In CEEUtlCalculateOperState: "
					"OperState = CEE_OPER_DISABLED\r\n");
			return CEE_OPER_DISABLED;
		}
	}
    else if(u1AdmWilling == DCBX_ENABLED)
	{
		DCBX_TRC (DCBX_TLV_TRC , "In CEEUtlCalculateOperState: "
				"OperState = CEE_USE_PEER_CFG\r\n");
		return CEE_OPER_USE_PEER_CFG;
	}
	else if(u1RemWilling == DCBX_ENABLED)
	{
		DCBX_TRC (DCBX_TLV_TRC , "In CEEUtlCalculateOperState: "
				"OperState = CEE_USE_LOCAL_CFG\r\n");
		return CEE_OPER_USE_LOCAL_CFG;
	}
	else
	{
		DCBX_TRC (DCBX_TLV_TRC , "In CEEUtlCalculateOperState: "
				"OperState = CEE_OPER_DISABLED!!!\r\n");
		return CEE_OPER_DISABLED;
	}
}

/***************************************************************************
 *  FUNCTION NAME : CEEUtlClearETSRemoteParam
 * 
 *  DESCRIPTION   : This utility function is used to Clear the remote param.
 * 
 *  INPUT         : pPortEntry - ETS Port Entry.
 * 
 *  OUTPUT        : None
 * 
 *  RETURNS       : None
 * 
 * **************************************************************************/
VOID
CEEUtlClearETSRemoteParam (tETSPortEntry * pPortEntry)
{

	if (pPortEntry->u1RemTlvUpdStatus & ETS_CEE_TLV_REM_UPD)
	{
		MEMSET (pPortEntry->ETSRemPortInfo.au1ETSRemBW, DCBX_ZERO,
				ETS_MAX_NUM_TCGID);

		MEMSET (pPortEntry->ETSRemPortInfo.au1ETSRemTCGID,
				DCBX_INVALID_ID, DCBX_MAX_PRIORITIES);
		pPortEntry->ETSRemPortInfo.u1ETSRemNumTCGSup = DCBX_ZERO;
		pPortEntry->ETSRemPortInfo.u1ETSRemWilling = DCBX_ZERO;
		pPortEntry->ETSRemPortInfo.u1ETSRemNumTcSup = DCBX_ZERO;
		/* Update the Remote update status for this TLV */
		pPortEntry->u1RemTlvUpdStatus =
			(UINT1) (pPortEntry->u1RemTlvUpdStatus &
					(~(ETS_CEE_TLV_REM_UPD)));

		MEMSET (pPortEntry->ETSRemPortInfo.au1ETSDestRemMacAddr, DCBX_ZERO,
				MAC_ADDR_LEN);
        pPortEntry->ETSRemPortInfo.u4ETSDestRemMacIndex = DCBX_ZERO;
		pPortEntry->ETSRemPortInfo.i4ETSRemIndex = DCBX_ZERO;
		pPortEntry->ETSRemPortInfo.u4ETSRemTimeMark = DCBX_ZERO;

	}
	return;
}

/**************************************************************************
 * FUNCTION NAME : CEEUtlUpdatETSLocalParam
 *
 * DESCRIPTION   : This utility is used to update the local parameter.
 *
 * INPUT         : pPortEntry - ETS Port Entry.
 *                 u1ETSState - ETS oper State.
 *
 * OUTPUT        : None
 *
 * RETURNS       : None
 *
 ***************************************************************************/
VOID
CEEUtlUpdatETSLocalParam (tETSPortEntry * pPortEntry, UINT1 u1ETSState)
{
    if ((u1ETSState == ETS_OPER_INIT) || (u1ETSState == ETS_OPER_OFF))
    {
        /* If the state is either INIT or OFF then local parameters will be 
         * same as Admin configuration parameters */
        MEMCPY (pPortEntry->ETSLocPortInfo.au1ETSLocBW,
                pPortEntry->ETSAdmPortInfo.au1ETSAdmBW, ETS_MAX_NUM_TCGID);

        MEMCPY (pPortEntry->ETSLocPortInfo.au1ETSLocTCGID,
                pPortEntry->ETSAdmPortInfo.au1ETSAdmTCGID, DCBX_MAX_PRIORITIES);

        pPortEntry->ETSLocPortInfo.u1ETSLocNumTCGSup =
            pPortEntry->ETSAdmPortInfo.u1ETSAdmNumTCGSup;

        pPortEntry->ETSLocPortInfo.u1ETSLocNumTcSup =
            pPortEntry->ETSAdmPortInfo.u1ETSAdmNumTcSup;

    }
	else
	{
		/* If the state is RX then local parametrs will be same as
		 * Remote parameters */

		MEMCPY (pPortEntry->ETSLocPortInfo.au1ETSLocBW,
						pPortEntry->ETSRemPortInfo.au1ETSRemBW, ETS_MAX_NUM_TCGID);

		MEMCPY (pPortEntry->ETSLocPortInfo.au1ETSLocTCGID,
						pPortEntry->ETSRemPortInfo.au1ETSRemTCGID, DCBX_MAX_PRIORITIES);

		pPortEntry->ETSLocPortInfo.u1ETSLocNumTCGSup =
				pPortEntry->ETSRemPortInfo.u1ETSRemNumTCGSup;

		pPortEntry->ETSLocPortInfo.u1ETSLocNumTcSup =
				pPortEntry->ETSRemPortInfo.u1ETSRemNumTcSup;
	}
}

/**************************************************************************
 * FUNCTION NAME : CeeUtlETSHwConfig
 *
 * DESCRIPTION   : This utility is used to configure the ETS paramters in 
 *                 the hardware by calling QOS API.
 *
 * INPUT         : pPortEntry - ETS port Entry.
 *                 u1Status - Enable/Disable status.
 *
 * OUTPUT        : None
 *
 * RETURNS       : OSIX_SUCCESS/OSIX_FAILURE
 *
 ***************************************************************************/
INT4
CeeUtlETSHwConfig (tETSPortEntry * pPortEntry, UINT1 u1Status)
{
#ifdef DCBX_ETS_QOS_WANTED
    tQosEtsHwEntry      EtsHwEntry;
    UINT4               u4PriIdx = DCBX_ZERO;
    UINT1               au1EtsBW[ETS_MAX_TCGID_CONF] = { DCBX_ZERO };
    UINT4               u4TCGId = DCBX_ZERO;
    UINT4               u4EtsCurTGs[ETS_MAX_TCGID_CONF] = {DCBX_ZERO};
    UINT4               u4ScheIndex=0;
    UINT4               u4ScheBaseIndex=0;
    UINT4               u4QId = 0;
    UINT4               u4QueIndex=0;

    DCBX_TRC_ARG2 (DCBX_CONTROL_PLANE_TRC, "In Func: %s u1Status = %d \n",
            __func__, u1Status);

    MEMSET (&EtsHwEntry, DCBX_ZERO, sizeof (EtsHwEntry));

    MEMCPY (au1EtsBW, pPortEntry->ETSLocPortInfo.au1ETSLocBW,
                ETS_MAX_TCGID_CONF);
    /* For Each Priority Configure the Bandwidth and Related TCGID 
     * in the hardware using QOS API */
    if (u1Status == ETS_ENABLED)
    {
        for (u4PriIdx = DCBX_ZERO; u4PriIdx < DCBX_MAX_PRIORITIES; u4PriIdx++)
        {
            u4TCGId = pPortEntry->ETSLocPortInfo.au1ETSLocTCGID[u4PriIdx];
            u4EtsCurTGs[u4TCGId] = TRUE;
            DCBX_TRC_ARG6 (DCBX_CONTROL_PLANE_TRC, "CeeUtlETSHwConfig:"
                    "Paramters for QosApiConfigEts are "
                    "IfIndex :%d Priority : %d TCGID : %d "
                    "BW for TCGID %d : %d and Status of entry :%d "
                    "!!!\r\n", pPortEntry->u4IfIndex, u4PriIdx,
                    pPortEntry->ETSLocPortInfo.au1ETSLocTCGID[u4PriIdx],
                    u4TCGId, au1EtsBW[u4TCGId], u1Status);

            /* Fill the struct to be programmed to HW */
            EtsHwEntry.u4IfIndex = pPortEntry->u4IfIndex;
            EtsHwEntry.u4PriIdx = u4PriIdx;
            EtsHwEntry.u1ETSBw = au1EtsBW[u4TCGId];
            EtsHwEntry.u1EtsPortStatus = u1Status;
            EtsHwEntry.u1EtsTCGID = (UINT1) u4TCGId;
        }
        /* Delete newly added S3 Schedulers
         * Below check is added not try to delete the schedulers 
         * in the first time.
         */

        if((pPortEntry->ETSQosPortInfo.u4EtsUsedMcScheCnt != 0) &&
                (pPortEntry->ETSQosPortInfo.u4EtsUsedUcScheCnt != 0) &&
                (pPortEntry->ETSQosPortInfo.u4EtsUsedUcQueueCnt != 0))
        {
            if (EtsUtlEtsDeleteScheduler(pPortEntry) == OSIX_FAILURE)
            {
                DCBX_TRC (DCBX_FAILURE_TRC, "CeeUtlETSHwConfig:" 
                        "Deletion of S3 Scheduler and Queue Delete FAILED !!!\r\n");
                return OSIX_FAILURE;
            }
        }

        u4ScheBaseIndex = QOS_HL_DEF_S3_SCHEDULER3_ID;
        /* Create S3 Scheduler node for WDRR Queues*/
        if (QosApiCreateSchedular(pPortEntry->u4IfIndex,
                    u4ScheBaseIndex,
                    QOS_S3_SCHEDULER, 
                    QOS_SCHED_ALGO_DRR) == QOS_FAILURE)
        {
            DCBX_TRC (DCBX_FAILURE_TRC, "CeeUtlETSHwConfig:" 
                    "Creation of S3 Scheduler node for WDRR Queue - FAILED !!!\r\n");
            return OSIX_FAILURE;
        }
        pPortEntry->ETSQosPortInfo.u4EtsUsedUcScheId[u4ScheIndex] = u4ScheBaseIndex;
        pPortEntry->ETSQosPortInfo.u4EtsUsedUcScheCnt++;

        for (u4TCGId = DCBX_ZERO ; u4TCGId < ETS_MAX_TCGID_CONF; u4TCGId++)
        {
            if (u4EtsCurTGs[u4TCGId] == TRUE)
            {
                u4QId = u4TCGId + 1;

                /* Detach ETS WDRR Queues from the default S3 scheduler */
                if (QosApiDeAttachQueue (pPortEntry->u4IfIndex,u4QId) == QOS_FAILURE)
                {
                    DCBX_TRC (DCBX_FAILURE_TRC, "CeeUtlETSHwConfig:"
                            "Detach ETS WDRR Queues from the default S3 scheduler - FAILED !!!\r\n");
                    return OSIX_FAILURE;
                }
                /* Attach ETS WDRR Queue to new scheduler */
                if(QosApiAttachQueue ( pPortEntry->u4IfIndex,
                            u4QId,
                            u4ScheBaseIndex,
                            QOS_SCHED_ALGO_DRR,
                            au1EtsBW[u4TCGId]) == QOS_FAILURE)
                {
                    DCBX_TRC (DCBX_FAILURE_TRC, "CeeUtlETSHwConfig:"
                            "Attach ETS WDRR Queue to new scheduler - FAILED !!!\r\n");
                    return OSIX_FAILURE;
                }
                pPortEntry->ETSQosPortInfo.u4EtsQToSchIndex[u4QId]=u4ScheBaseIndex;
                pPortEntry->ETSQosPortInfo.u4EtsUsedUcQueueId[u4QueIndex++]= u4QId;
                pPortEntry->ETSQosPortInfo.u4EtsUsedUcQueueCnt ++;
            }
        }
        u4ScheBaseIndex++;
        u4ScheIndex++;

        /* Create last S3 Scheduler node for all MC Queues */
        if (QosApiCreateSchedular(pPortEntry->u4IfIndex,
                    u4ScheBaseIndex,
                    QOS_S3_SCHEDULER, 
                    QOS_SCHED_ALGO_RR) == QOS_FAILURE)
        {
            DCBX_TRC (DCBX_FAILURE_TRC, "CeeUtlETSHwConfig:" 
                    "Creation of last S3 Scheduler node for all MC Queues - FAILED !!!\r\n");
            return OSIX_FAILURE;
        }
        pPortEntry->ETSQosPortInfo.u4EtsUsedMcScheId = u4ScheBaseIndex;
        pPortEntry->ETSQosPortInfo.u4EtsUsedMcScheCnt++;

        /* Dettach All MC queues from default S3 scheduler and Attach to last S3 scheduler */
        if (EtsUtlMcQueueDettachAttach( pPortEntry->u4IfIndex, 
                    u4ScheBaseIndex, 
                    QOS_SCHED_ALGO_RR) == QOS_FAILURE)
        {
            DCBX_TRC (DCBX_FAILURE_TRC, "CeeUtlETSHwConfig:" 
                    "Dettach All MC queues from default S3 scheduler and "
                    "Attach to last S3 scheduler - FAILED !!!\r\n");
            return OSIX_FAILURE;
        }

        /* Attach all the S3 scheduler to S2 scheduler with SP algorithm 
         * Queue Algorithm update is required after scheduler algorithm update
         */
        if (QosApiUpdateSchedularAlgo (pPortEntry->u4IfIndex,
                    QOS_HL_DEF_S2_SCHEDULER_ID, 
                    QOS_SCHED_ALGO_SP) == QOS_FAILURE)
        {
            DCBX_TRC (DCBX_FAILURE_TRC, "CeeUtlETSHwConfig:" 
                    "Attach all the S3 scheduler to S2 scheduler with SP "
                    "algorithm - FAILED !!!\r\n");
            return OSIX_FAILURE;
        }

        /* Algorithm update for all ETS WDRR and SP queues */
        for (u4TCGId = DCBX_ZERO ; u4TCGId < ETS_MAX_TCGID_CONF; u4TCGId++)
        {
            if (u4EtsCurTGs[u4TCGId] == TRUE)
            {
                u4QId = u4TCGId + 1;
                if(QosApiAttachQueue ( pPortEntry->u4IfIndex,
                            u4QId,
                            pPortEntry->ETSQosPortInfo.u4EtsQToSchIndex[u4QId],
                            QOS_SCHED_ALGO_DRR,
                            au1EtsBW[u4TCGId]) == QOS_FAILURE)
                {
                    DCBX_TRC (DCBX_FAILURE_TRC, "CeeUtlETSHwConfig:"
                            "Algorithm update for all ETS WDRR queues - FAILED !!!\r\n");
                    return OSIX_FAILURE;
                }
            }
        }
    }
    else
    {
        /* If ETS is disbaled
         * Remove All ETS Flow scheduler and queues
         * Attach queues with the default scheduler
         * Configre default Schdulers with default values
         * Configure the Queue with the deafult values
         */
        if((pPortEntry->ETSQosPortInfo.u4EtsUsedMcScheCnt != 0) &&
                (pPortEntry->ETSQosPortInfo.u4EtsUsedUcScheCnt != 0) &&
                (pPortEntry->ETSQosPortInfo.u4EtsUsedUcQueueCnt != 0))
        {
            if (EtsUtlEtsDeleteScheduler(pPortEntry) == OSIX_FAILURE)
            {
                DCBX_TRC (DCBX_FAILURE_TRC, "ETSUtlHwConfig:" 
                        "Removal of ETS Flow Schedulers and Queues FAILED !!!\r\n");
                return OSIX_FAILURE;
            }
        }
        /* Attach Default S3 scheduler to S2 scheduler with default algorithm(RR) */
        if (QosApiUpdateSchedularAlgo (pPortEntry->u4IfIndex,
                    QOS_HL_DEF_S2_SCHEDULER_ID, 
                    QOS_SCHED_ALGO_RR) == QOS_FAILURE)
        {
            DCBX_TRC (DCBX_FAILURE_TRC, "ETSUtlHwConfig:" 
                    "Attach Default S3 scheduler to S2 scheduler with default "
                    "algorithm(RR) - FAILED !!!\r\n");
            return OSIX_FAILURE;
        }
        pPortEntry->ETSQosPortInfo.u4EtsUsedUcQueueCnt = 0;
        pPortEntry->ETSQosPortInfo.u4EtsUsedUcScheCnt = 0;
        pPortEntry->ETSQosPortInfo.u4EtsUsedMcScheCnt = 0;
    }
#else
    DCBX_TRC_ARG2 (DCBX_CONTROL_PLANE_TRC, "In Func: %s u1Status = %d \n",
            __func__, u1Status);
    UNUSED_PARAM (pPortEntry);
    UNUSED_PARAM (u1Status);
#endif /* DCBX_ETS_QOS_WANTED */

    DCBX_TRC_ARG1 (DCBX_CONTROL_PLANE_TRC, "Exit Func: %s\n", __func__);
    return OSIX_SUCCESS;
}

/***************************************************************************
*  FUNCTION NAME : CEEUtlHandlePktConfigErrors
*
*  DESCRIPTION   : This utility is used to perform necessary actions
*                  in case Duplicate Or No Feature TLV or incorrect oper version
*                  received.
*
*  INPUT         : u1TlvType - CEE TLV Type
*                  CeeFeatTLVInfo - TLV information.
*                  i4Event - Event name
*
*  OUTPUT        : None
*
*  RETURNS       : None
*
***************************************************************************/
VOID
CEEUtlHandlePktConfigErrors (UINT1 u1TlvType, tCeeTLVInfo CeeTLVInfo, INT4 i4Event)
{
    tDcbxCEETrapMsg     NotifyInfo;
    tDcbxPortEntry     *pDcbxPortEntry = NULL;

    pDcbxPortEntry = DcbxUtlGetPortEntry (CeeTLVInfo.u4IfIndex);
    if (pDcbxPortEntry == NULL)
    {
        return;
    }
    if (DCBX_VER_CEE != pDcbxPortEntry->u1OperVersion)
    {
        DCBX_TRC_ARG1 (DCBX_TLV_TRC | DCBX_CONTROL_PLANE_TRC, 
                "In CEEUtlHandlePktConfigErrors: "
                "CEE pkt config error detected, Ignoring the packet!!! "
                "- port %d\r\n", CeeTLVInfo.u4IfIndex);
        return;
    }

    MEMSET (&NotifyInfo, DCBX_ZERO, sizeof (tDcbxCEETrapMsg));

    switch(u1TlvType)
    {
        case CEE_CTRL_TLV_TYPE:
            {
                /* Updating SM for Duplicate or incorrect oper/max version rcvd
                 * in CTRL TLV */
                CEEUtlUpdateSmForPktErrors (CeeTLVInfo, i4Event);

                if (CEE_EV_DUP_TLV_RECEIVED == i4Event)
                {
                    NotifyInfo.u4IfIndex = CeeTLVInfo.u4IfIndex; 
                    DcbxCEESendPortNotification(&NotifyInfo, DCBX_DUP_CTRL_TLV_TRAP);
                }
            }
            break;
        case CEE_ETS_TLV_TYPE:
            {
                if (CEE_EV_DUP_TLV_RECEIVED == i4Event)
                {
                    NotifyInfo.u4IfIndex = CeeTLVInfo.u4IfIndex; 
                    NotifyInfo.unTrapMsg.u1FeatType = ETS_TRAP;
                    DcbxCEESendPortNotification(&NotifyInfo,DCBX_DUP_FEAT_TLV_TRAP);

                    CEESemRun (CEE_ETS_TLV_TYPE, &CeeTLVInfo, CEE_EV_DUP_TLV_RECEIVED);
                }
                else if (CEE_EV_NO_FEAT_TLV_RECEIVED == i4Event)
                {
                    NotifyInfo.u4IfIndex = CeeTLVInfo.u4IfIndex;
                    NotifyInfo.unTrapMsg.u1FeatType = ETS_TRAP;
                    DcbxCEESendPortNotification(&NotifyInfo, DCBX_NO_FEAT_TLV_TRAP);

                    CEESemRun (CEE_ETS_TLV_TYPE, &CeeTLVInfo, CEE_EV_NO_FEAT_TLV_RECEIVED);
                }
                else if (CEE_EV_INVALID_PKT_RECEIVED == i4Event)
                {
                    CEEUtlUpdateSmForPktErrors (CeeTLVInfo, CEE_EV_INVALID_PKT_RECEIVED);
                }
            }
            break;
        case CEE_PFC_TLV_TYPE:
            {
                if (CEE_EV_DUP_TLV_RECEIVED == i4Event)
                {
                    NotifyInfo.u4IfIndex = CeeTLVInfo.u4IfIndex; 
                    NotifyInfo.unTrapMsg.u1FeatType = PFC_TRAP;
                    DcbxCEESendPortNotification(&NotifyInfo, DCBX_DUP_FEAT_TLV_TRAP);

                    CEESemRun (CEE_PFC_TLV_TYPE, &CeeTLVInfo, CEE_EV_DUP_TLV_RECEIVED);
                }
                else if (CEE_EV_NO_FEAT_TLV_RECEIVED == i4Event)
                {
                    NotifyInfo.u4IfIndex = CeeTLVInfo.u4IfIndex;
                    NotifyInfo.unTrapMsg.u1FeatType = PFC_TRAP;
                    DcbxCEESendPortNotification(&NotifyInfo, DCBX_NO_FEAT_TLV_TRAP);

                    CEESemRun (CEE_PFC_TLV_TYPE, &CeeTLVInfo, CEE_EV_NO_FEAT_TLV_RECEIVED);
                }
                else if (CEE_EV_INVALID_PKT_RECEIVED == i4Event)
                {
                    CEEUtlUpdateSmForPktErrors (CeeTLVInfo, CEE_EV_INVALID_PKT_RECEIVED);
                }
            }            
            break;
        case CEE_APP_PRI_TLV_TYPE:
            { 
                if (CEE_EV_DUP_TLV_RECEIVED == i4Event)
                {
                    NotifyInfo.u4IfIndex = CeeTLVInfo.u4IfIndex; 
                    NotifyInfo.unTrapMsg.u1FeatType = APP_PRI_TRAP;
                    DcbxCEESendPortNotification(&NotifyInfo, DCBX_DUP_FEAT_TLV_TRAP);

                    CEESemRun (CEE_APP_PRI_TLV_TYPE, &CeeTLVInfo, CEE_EV_DUP_TLV_RECEIVED);
                }
                else if (CEE_EV_NO_FEAT_TLV_RECEIVED == i4Event)
                {
                    NotifyInfo.u4IfIndex = CeeTLVInfo.u4IfIndex;
                    NotifyInfo.unTrapMsg.u1FeatType = APP_PRI_TRAP;
                    DcbxCEESendPortNotification(&NotifyInfo, DCBX_NO_FEAT_TLV_TRAP);
                    CEESemRun (CEE_APP_PRI_TLV_TYPE, &CeeTLVInfo, CEE_EV_NO_FEAT_TLV_RECEIVED);
                }
                else if (CEE_EV_INVALID_PKT_RECEIVED == i4Event)
                {
                    CEEUtlUpdateSmForPktErrors (CeeTLVInfo, CEE_EV_INVALID_PKT_RECEIVED);
                }
            }
            break;
        default:
            DCBX_TRC (DCBX_TLV_TRC | DCBX_FAILURE_TRC, 
                    "CEEUtlHandlePktConfigErrors: Invalid TLV Type!!!\r\n");
            break;
    }
    return;
}

/***************************************************************************
*  FUNCTION NAME : CEEUtlValidateAndDropTLV
*
*  DESCRIPTION   : This utility is used to check and drop the CEE TLV in case
*                  Duplicate Ctrl TLV or mismatch in oper/max version is received
*
*  INPUT         : u4IfIndex    - Port number.
*                  ApplTlvParam - Appl TLV information.
*
*  OUTPUT        : None
*
*  RETURNS       : DCBX_SUCCESS / DCBX_FAILURE
*
***************************************************************************/
INT4 
CEEUtlValidateAndDropTLV (UINT4 u4IfIndex, tApplTlvParam * pApplTlvParam)
{
    UINT4               u4Value = DCBX_ZERO;
    UINT4               u4SeqNo = DCBX_ZERO;
    UINT2               u2Value = DCBX_ZERO;
    UINT2               u2RxTlvLen = DCBX_ZERO;
    UINT1              *pu1Buf = NULL;
    UINT1               u1FirstOperVersion = DCBX_VER_UNKNOWN;
    UINT1               u1FirstMaxVersion = DCBX_VER_UNKNOWN;
    UINT1               u1OperVersion = DCBX_VER_UNKNOWN;
    UINT1              *pu1CtrlTlv = NULL;
    UINT1               u1MaxVersion = DCBX_VER_UNKNOWN;
    UINT1               u1CtrlTlvFlag = DCBX_FAILURE;
    BOOL1               bFlag = OSIX_FALSE;
    INT4                i4RetVal = DCBX_FAILURE;
    tCeeTLVInfo        CeeTLVInfo;
    tPFCPortEntry      *pPFCPortEntry = NULL;
    tETSPortEntry      *pETSPortEntry = NULL;
    tAppPriPortEntry   *pAppPriPortEntry = NULL;
    tCEECtrlEntry      *pCEECtrlEntry = NULL;
    tDcbxPortEntry     *pDcbxPortEntry = NULL;

    MEMSET (&CeeTLVInfo, DCBX_ZERO, sizeof (tCeeTLVInfo));
    pDcbxPortEntry = DcbxUtlGetPortEntry (u4IfIndex);

    if (pDcbxPortEntry == NULL)
    {
        return DCBX_FAILURE;
    }

    if ((DCBX_DISABLED == pDcbxPortEntry->u1LldpTxStatus) ||
            (DCBX_DISABLED == pDcbxPortEntry->u1LldpRxStatus))
    {
        DCBX_TRC_ARG1 (DCBX_FAILURE_TRC, "In CEEUtlValidateAndDropTLV: "
                " LLDP Tx / Rx is disabled in port %d, Drop Pkt!!!\r\n",
                u4IfIndex);
        return DCBX_FAILURE;
    }

    u2RxTlvLen = pApplTlvParam->u2RxTlvLen;
    pu1Buf = pApplTlvParam->au1DcbxTlv;
    
    /* STEP 1 : Skip till Protocol Sub TLV's start */ 
    pu1Buf = pu1Buf + CEE_TLV_HDR_LEN;
    u2RxTlvLen = (UINT2)(u2RxTlvLen - CEE_TLV_HDR_LEN);

    CeeTLVInfo.u4RemLastUpdateTime = pApplTlvParam->u4RemLastUpdateTime;
    CeeTLVInfo.i4RemIndex= pApplTlvParam->i4RemIndex;

    MEMCPY(CeeTLVInfo.RemDestMacAddr, 
            pApplTlvParam->RemLocalDestMacAddr, MAC_ADDR_LEN);
    
    CeeTLVInfo.u4RemLocalDestMACIndex =
                        pApplTlvParam->u4RemLocalDestMACIndex;

    CeeTLVInfo.u4IfIndex = u4IfIndex;

    while((u2RxTlvLen != 0) && (u2RxTlvLen < pApplTlvParam->u2RxTlvLen))
    { 
        /* STEP 2 : Parse Recieved TLV's one by one */
        /* Get the next 2 bytes (Type 7 bits + Length 9 bits)  */
        DCBX_GET_2BYTE (pu1Buf, u2Value);
        u2RxTlvLen = (UINT2)(u2RxTlvLen - CEE_TLV_TYPE_LEN);
        CeeTLVInfo.u1TlvType = (UINT1)((u2Value & CEE_TLV_TYPE_MASK) >> DCBX_TLV_LENGTH_IN_BITS);
        CeeTLVInfo.u2TlvLen = u2Value & CEE_TLV_LEN_MASK;
        /* Using the maximum Feat TLV len to intialize */
        MEMSET (CeeTLVInfo.au1CeeApplTlv, DCBX_ZERO, CEE_APP_PRI_MAX_TLV_LEN); 
        MEMCPY (CeeTLVInfo.au1CeeApplTlv, pu1Buf, CeeTLVInfo.u2TlvLen);
        if (!bFlag)
        {
            DCBX_GET_1BYTE (pu1Buf, u1FirstOperVersion);
            DCBX_GET_1BYTE (pu1Buf, u1FirstMaxVersion);
            u1OperVersion = u1FirstOperVersion;
            u1MaxVersion = u1FirstMaxVersion;
            bFlag = OSIX_TRUE; /* Flag kept to copy the oper and max version recvd in 
                                * first TLV only and compare these with other TLV's
                                * oper and max version */
        }
        else
        {
            DCBX_GET_1BYTE (pu1Buf, u1OperVersion);
            DCBX_GET_1BYTE (pu1Buf, u1MaxVersion);        
        }
        
        pu1Buf = pu1Buf +  CeeTLVInfo.u2TlvLen - CEE_TLV_TYPE_LEN;
        switch( CeeTLVInfo.u1TlvType)
        {
            case CEE_CTRL_TLV_TYPE : /* CTRL TLV*/
                pCEECtrlEntry = CEEUtlGetCtrlEntry (u4IfIndex);
                if (pCEECtrlEntry == NULL)
                {
                    DCBX_TRC_ARG2 (DCBX_TLV_TRC, "%s : Failed to fetch the"
                            "Ctrl Entry for Port Id %d \r\n",
                            __FUNCTION__, u4IfIndex);
                    return DCBX_FAILURE;
                }
                /*Check if Ctrl TLV has been recieved already */
                if(u1CtrlTlvFlag == DCBX_SUCCESS)
                {
                    DCBX_TRC (DCBX_TLV_TRC | DCBX_FAILURE_TRC, "CEEUtlValidateAndDropTLV: "
                            "Control Duplicate TLV has been received!!!\r\n");	
                    CEEUtlHandlePktConfigErrors(CEE_CTRL_TLV_TYPE, CeeTLVInfo, CEE_EV_DUP_TLV_RECEIVED);

                    pCEECtrlEntry->u4CtrlRxTLVErrorCount++;
                    
                    /* Send Counter update Info to Stand by node */
                    DcbxCeeRedSendDynamicCtrlCounterInfo (pCEECtrlEntry,
                            DCBX_CEE_RED_CTRL_RX_ERR_COUNTER);

                    return DCBX_FAILURE;
                }
                /* Check len of CTRL_TLV should be as per CEE standard ie 10bytes */
                if (CeeTLVInfo.u2TlvLen > CEE_CONTROL_TLV_PAYLOAD_LEN)
                {
                    DCBX_TRC (DCBX_TLV_TRC | DCBX_FAILURE_TRC, 
                            "CEEUtlValidateAndDropTLV: Received DCBX frame "
                            "with incorrect control TLV Length!!!\r\n");

                    pCEECtrlEntry->u4CtrlRxTLVErrorCount++;
                    /* Send Counter update Info to Stand by node */
                    DcbxCeeRedSendDynamicCtrlCounterInfo (pCEECtrlEntry,
                            DCBX_CEE_RED_CTRL_RX_ERR_COUNTER);
                    return DCBX_FAILURE;
                }
                /* Check Oper and Max version should be same throughout the TLVs &
                 * should be CEE */
                if ((u1OperVersion != DCBX_VER_CEE) || 
                        ((u1FirstOperVersion != u1OperVersion) || 
                         (u1FirstMaxVersion != u1MaxVersion)))
                {
                    DCBX_TRC_ARG2 (DCBX_TLV_TRC | DCBX_FAILURE_TRC, 
                            "CEEUtlValidateAndDropTLV: Received DCBX frame "
                            "with different Oper %d / Max Version %d!!!\r\n",
                            u1OperVersion, u1MaxVersion);	
                    CEEUtlHandlePktConfigErrors(CEE_CTRL_TLV_TYPE, CeeTLVInfo, CEE_EV_INVALID_PKT_RECEIVED);
     
                    pCEECtrlEntry->u4CtrlRxTLVErrorCount++; 

                    /* Send Counter update Info to Stand by node */
                    DcbxCeeRedSendDynamicCtrlCounterInfo (pCEECtrlEntry,
                            DCBX_CEE_RED_CTRL_RX_ERR_COUNTER);
                    return DCBX_FAILURE;
                }
                /* Check for missing Ack only when it is CEE operversion;
                 * This is done to handle case when version switching from IEEE
                 * to CEE */
                pu1CtrlTlv = CeeTLVInfo.au1CeeApplTlv;
                pu1CtrlTlv += CEE_TLV_TYPE_LEN;  /*Skip Oper Version and Max Version octets*/

                /*Fetch SeqNo*/
                DCBX_GET_4BYTE(pu1CtrlTlv, u4Value);
                u4SeqNo = u4Value;
                
                if (u4SeqNo == DCBX_ZERO)
                {
                    DCBX_TRC_ARG1 (DCBX_TLV_TRC | DCBX_FAILURE_TRC, 
                            "CEEUtlValidateAndDropTLV: Received DCBX frame "
                            "with invalid SeqNo %d!!!\r\n",
                            u4SeqNo);	
                    CEEUtlHandlePktConfigErrors(CEE_CTRL_TLV_TYPE, CeeTLVInfo, CEE_EV_INVALID_PKT_RECEIVED);
                    pCEECtrlEntry->u4CtrlRxTLVErrorCount++; 
                    
                    /* Send Counter update Info to Stand by node */
                    DcbxCeeRedSendDynamicCtrlCounterInfo (pCEECtrlEntry,
                            DCBX_CEE_RED_CTRL_RX_ERR_COUNTER);
                    return DCBX_FAILURE;
                }

                if (DCBX_VER_CEE == pDcbxPortEntry->u1OperVersion)
                {
                    i4RetVal = CEEUtlCheckForAckMiss (pCEECtrlEntry,&CeeTLVInfo);
                    if (i4RetVal == DCBX_FAILURE)
                    {
                        DCBX_TRC_ARG1 (DCBX_TLV_TRC , 
                                "CEEUtlValidateAndDropTLV: Received DCBX refresh frame "
                                "on port: %d. Ignore!!!\r\n", u4IfIndex);	
                        return DCBX_IGNORE;
                    }
                }
                u1CtrlTlvFlag = DCBX_SUCCESS;
                break; 

            case CEE_ETS_TLV_TYPE : /* ETS TLV */
                pETSPortEntry = ETSUtlGetPortEntry(u4IfIndex);
                if(pETSPortEntry != NULL)
                {
                    /* Check Oper and Max version should be same throughout the
                     * TLVs & should be CEE */
                    if ((u1OperVersion != DCBX_VER_CEE) ||
                            ((u1FirstOperVersion != u1OperVersion) || 
                             (u1FirstMaxVersion != u1MaxVersion)))
                    {
                        DCBX_TRC_ARG2 (DCBX_TLV_TRC | DCBX_FAILURE_TRC, 
                                "CEEUtlValidateAndDropTLV: Received DCBX frame "
                                "with different Oper %d / Max Version %d!!!\r\n",
                                u1OperVersion, u1MaxVersion);	
                        CEEUtlHandlePktConfigErrors(CEE_ETS_TLV_TYPE, CeeTLVInfo, CEE_EV_INVALID_PKT_RECEIVED);
                        return DCBX_FAILURE;
                    }
                }
                break;

            case CEE_PFC_TLV_TYPE : /* PFC TLV */
                pPFCPortEntry = PFCUtlGetPortEntry (u4IfIndex);
                if(pPFCPortEntry != NULL)
                {
                    /* Check Oper and Max version should be same throughout the
                     * TLVs & should be CEE */
                    if ((u1OperVersion != DCBX_VER_CEE) || 
                            ((u1FirstOperVersion != u1OperVersion) || 
                             (u1FirstMaxVersion != u1MaxVersion)))
                    {
                        DCBX_TRC_ARG2 (DCBX_TLV_TRC | DCBX_FAILURE_TRC, 
                                "CEEUtlValidateAndDropTLV: Received DCBX frame "
                                "with different Oper %d / Max Version %d!!!\r\n",
                                u1OperVersion, u1MaxVersion);	
                        CEEUtlHandlePktConfigErrors(CEE_PFC_TLV_TYPE, CeeTLVInfo, CEE_EV_INVALID_PKT_RECEIVED);
                        return DCBX_FAILURE;
                    }
                }
                break;

            case CEE_APP_PRI_TLV_TYPE : /* APP TLV */
                pAppPriPortEntry = AppPriUtlGetPortEntry((UINT4) u4IfIndex);
                if(pAppPriPortEntry != NULL)
                {
                    /* Check Oper and Max version should be same throughout the
                     * TLVs & should be CEE */
                    if ((u1OperVersion != DCBX_VER_CEE) || 
                            ((u1FirstOperVersion != u1OperVersion) || 
                             (u1FirstMaxVersion != u1MaxVersion)))
                    {
                        DCBX_TRC_ARG2 (DCBX_TLV_TRC | DCBX_FAILURE_TRC, 
                                "CEEUtlValidateAndDropTLV: Received DCBX frame "
                                "with different Oper %d / Max Version %d!!!\r\n",
                                u1OperVersion, u1MaxVersion);	
                        CEEUtlHandlePktConfigErrors(CEE_APP_PRI_TLV_TYPE, CeeTLVInfo, CEE_EV_INVALID_PKT_RECEIVED);
                        return DCBX_FAILURE;
                    }
                }
                break;

            default : 
                DCBX_TRC (DCBX_TLV_TRC | DCBX_FAILURE_TRC, 
                        "CEEUtlValidateAndDropTLV: Received Invalid "
                        "TLV Type!!!\r\n");
                return DCBX_FAILURE;
                break;

        }
        u2RxTlvLen = (UINT2)(u2RxTlvLen - CeeTLVInfo.u2TlvLen);
    }

    return DCBX_SUCCESS;
}
/**************************************************************************
  *  FUNCTION NAME : CEEUtlSetSemType
  *
  *  DESCRIPTION   : This utility is used to set the statemachine type 
  *                  for each feature.
  *
  *  INPUT         : i4FsDCBXPortNumber - port index 
  *                  u1Mode - IEEE/CEE.
  *
  *  OUTPUT        : None
  *
  *  RETURNS       : None
  *
  * **************************************************************************/
VOID
CEEUtlSetSemType (INT4 i4FsDCBXPortNumber, UINT1 u1Mode)
{
    tPFCPortEntry      *pPFCPortEntry = NULL;
    tETSPortEntry      *pETSPortEntry = NULL;
    tAppPriPortEntry   *pAppPriPortEntry = NULL;

    pETSPortEntry = ETSUtlGetPortEntry((UINT4) i4FsDCBXPortNumber);
    if(pETSPortEntry != NULL)
    {
        if ((u1Mode == DCBX_MODE_IEEE ) || (u1Mode == DCBX_MODE_AUTO))
        {
            pETSPortEntry->u1ETSDcbxSemType = DCBX_ASYM_STATE_MACHINE;
        }
        else if (u1Mode == DCBX_MODE_CEE)
        {
            pETSPortEntry->u1ETSDcbxSemType = DCBX_FEAT_STATE_MACHINE;
        }
    }
    pPFCPortEntry = PFCUtlGetPortEntry ((UINT4) i4FsDCBXPortNumber);
    if(pPFCPortEntry != NULL)
    {
        if ((u1Mode == DCBX_MODE_IEEE ) || (u1Mode == DCBX_MODE_AUTO))
        {
            pPFCPortEntry->u1PFCDcbxSemType = DCBX_SYM_STATE_MACHINE;
        }
        else if (u1Mode == DCBX_MODE_CEE)
        {
            pPFCPortEntry->u1PFCDcbxSemType = DCBX_FEAT_STATE_MACHINE;
        }
    }
    pAppPriPortEntry = AppPriUtlGetPortEntry ((UINT4) i4FsDCBXPortNumber);
    if(pAppPriPortEntry != NULL)
    {
        if ((u1Mode == DCBX_MODE_IEEE ) || (u1Mode == DCBX_MODE_AUTO))
        {
            pAppPriPortEntry->u1AppPriDcbxSemType = DCBX_SYM_STATE_MACHINE;
        }
        else if (u1Mode == DCBX_MODE_CEE)
        {
            pAppPriPortEntry->u1AppPriDcbxSemType = DCBX_FEAT_STATE_MACHINE;
        }
    }
}

/***************************************************************************
 *  FUNCTION NAME : CEEUtlHandleETSAgeOut 
 * 
 *  DESCRIPTION   : This function is used to handle the CEE ETS TLV Age Out
 *                  Message from DCBX.
 * 
 *  INPUT         : pPortEntry - ETS Port Entry.
 * 
 *  OUTPUT        : None
 * 
 *  RETURNS       : None
 * 
 * **************************************************************************/
VOID
CEEUtlHandleETSAgeOut (tETSPortEntry * pPortEntry)
{

    SET_DCBX_STATUS(pPortEntry, pPortEntry->u1ETSDcbxSemType,
            DCBX_RED_ETS_DCBX_STATUS_INFO, DCBX_STATUS_PEER_NO_ADV_FEAT);

    DCBX_TRC_ARG1 (DCBX_SEM_TRC, "CEEUtlHandleETSAgeOut: "
            "Clear remote entries and Change the"
            "state to DISABLED on port [%d]!!!\r\n", pPortEntry->u4IfIndex);

    ETSUtlOperStateChange (pPortEntry, ETS_OPER_OFF);

    /* Send State Change information to Standby Node.
     * so that if it is RECO state it will be moved to
     * INIT in standby also.*/
     CEEUtlClearETSRemoteParam (pPortEntry);

    return;
}

/**************************************************************************
 * FUNCTION NAME : CEEUtlHandlePFCAgeOut
 *
 * DESCRIPTION   : This function is used to handle the CEE PFC TLV Age Out
 *                 Message from DCBX.
 *
 * INPUT         : pPortEntry - PFC Port Entry.
 *
 * OUTPUT        : None
 *
 * RETURNS       : None
 *
 ***************************************************************************/
VOID
CEEUtlHandlePFCAgeOut (tPFCPortEntry * pPortEntry)
{
    /* If Age out message is recieved then move the state to INIT 
     * and clear the Remote Parameters */
    DCBX_TRC (DCBX_TLV_TRC | DCBX_SEM_TRC, 
            "CEEUtlHandlePFCAgeOut: Clear remote entries and "
            "change the state to DISABLED!!!\r\n");
    
    /* Send Remote table Clear information to Standby Node */
    SET_DCBX_STATUS(pPortEntry, pPortEntry->u1PFCDcbxSemType, 
            DCBX_RED_PFC_DCBX_STATUS_INFO, DCBX_STATUS_PEER_NO_ADV_FEAT);

    PFCUtlOperStateChange (pPortEntry, PFC_OPER_OFF);
    /* Send State Change information to Standby Node.
     * so that if it is RECO state it will be moved to
     * INIT in standby also.*/

    MEMSET (&(pPortEntry->PFCRemPortInfo), DCBX_ZERO, sizeof (tPFCRemPortInfo));

    return;
}

/**************************************************************************
 * FUNCTION NAME : CEEUtlHandleAppPriAgeOut
 *
 * DESCRIPTION   : This function is used to handle the CEE Application 
 *                 Priority TLV Age Out Message from DCBX.
 *
 * INPUT         : pPortEntry - Application Priority Port Entry.
 *
 * OUTPUT        : None
 *
 * RETURNS       : None
 *
 ***************************************************************************/
VOID
CEEUtlHandleAppPriAgeOut (tAppPriPortEntry * pPortEntry)
{
    /* If Age out message is recieved then move the state to INIT 
     * and clear the Remote Parameters */
    DCBX_TRC_ARG1 (DCBX_TLV_TRC | DCBX_SEM_TRC, "CEEUtlHandleAppPriAgeOut: "
              "Clear remote entries and "
              "change the state to DISABLED on port[%d]!!!\r\n",
              pPortEntry->u4IfIndex);

    AppPriUtlOperStateChange (pPortEntry, APP_PRI_OPER_OFF);

    SET_DCBX_STATUS(pPortEntry, pPortEntry->u1AppPriDcbxSemType, 
            DCBX_RED_APP_PRI_DCBX_STATUS_INFO, DCBX_STATUS_PEER_NO_ADV_FEAT);

    /* Delete the Application Priority Mapping Tbl from the remote information */
    AppPriUtlDeleteRemoteMappingEntries (pPortEntry->u4IfIndex);
    AppPriUtlDeleteRemotePortEntries (pPortEntry->u4IfIndex);

    return;
}

/***************************************************************************
 *   FUNCTION NAME : CEEUtlCheckForAckMiss
 * 
 *   DESCRIPTION   : This utility is used to check and drop the refresh frame
 *                   Also if the ACK is not received within 3 interval the state
 *                   machine will be restarted.
 * 
 *   INPUT         : u4IfIndex    - Port number.
 *                   ApplTlvParam - Appl TLV information.
 * 
 *   OUTPUT        : None
 * 
 *   RETURNS       : DCBX_SUCCESS / DCBX_FAILURE
 * 
 ****************************************************************************/
INT4
CEEUtlCheckForAckMiss (tCEECtrlEntry *pCEECtrlEntry, tCeeTLVInfo *pCeeTLVInfo)
{
    UINT4               u4Value = DCBX_ZERO;
    UINT4               u4SeqNo = DCBX_ZERO;
    UINT4               u4AckNo = DCBX_ZERO;
    UINT1              *pu1Buf = NULL;
    BOOL1               bRefreshFrame = OSIX_FALSE;
    tCeeTLVInfo         CeeFeatTlvParam;

    MEMSET (&CeeFeatTlvParam, DCBX_ZERO, sizeof (tCeeTLVInfo));

	CeeFeatTlvParam.u4IfIndex = pCEECtrlEntry->u4IfIndex;

    pu1Buf = pCeeTLVInfo->au1CeeApplTlv;
    pu1Buf += CEE_TLV_TYPE_LEN;  /*Skip Oper Version and Max Version octets*/

    /*Fetch SeqNo*/
    DCBX_GET_4BYTE(pu1Buf, u4Value);
    u4SeqNo = u4Value;

    /*Fetch Rcvd AckNo*/
    DCBX_GET_4BYTE(pu1Buf, u4Value); 
    u4AckNo = u4Value;

    DCBX_TRC_ARG4 (DCBX_TLV_TRC, "In CEEUtlCheckForAckMiss : "
            "Local node SeqNo:%d and AckNo:%d "
            "Remote node SeqNo:%d and AckNo:%d\r\n",
            pCEECtrlEntry->u4CtrlSeqNo, pCEECtrlEntry->u4CtrlAckNo,
            u4SeqNo, u4AckNo);
    if (pCEECtrlEntry->u4CtrlSeqNo > u4AckNo)
    {
        pCEECtrlEntry->u4CtrlSeqNo        = DCBX_ZERO;
        pCEECtrlEntry->u4CtrlRcvdSeqNo    = DCBX_ZERO;
        pCEECtrlEntry->u4CtrlAckNo        = DCBX_ZERO;
        pCEECtrlEntry->u4CtrlRcvdAckNo    = DCBX_ZERO;
        pCEECtrlEntry->u4CtrlSyncNo       = DCBX_ZERO;
        pCEECtrlEntry->u4CtrlAckMissCount = DCBX_ZERO;

        CEESemRun (CEE_ETS_TLV_TYPE, &CeeFeatTlvParam,CEE_EV_INIT);
        CEESemRun (CEE_PFC_TLV_TYPE, &CeeFeatTlvParam,CEE_EV_INIT);
        CEESemRun (CEE_APP_PRI_TLV_TYPE, &CeeFeatTlvParam,CEE_EV_INIT);

        DCBX_TRC_ARG1 (DCBX_TLV_TRC, "Info: DCBX Peer has restarted. "
                "Restart the local state machine for port %d!!!\r\n", pCEECtrlEntry->u4IfIndex);
        CEEFormAndSendTLV (pCEECtrlEntry->u4IfIndex, DCBX_LLDP_PORT_UPDATE, DCBX_ZERO);
        return DCBX_SUCCESS;
    }

    if ((pCEECtrlEntry->u4CtrlRcvdSeqNo == u4SeqNo) &&
            (pCEECtrlEntry->u4CtrlRcvdAckNo == u4AckNo))/*Refresh frame recevied*/
    {
        bRefreshFrame = OSIX_TRUE;
    }
    if (pCEECtrlEntry->u4CtrlSeqNo != pCEECtrlEntry->u4CtrlRcvdAckNo) /*Waiting for ACK*/
    {
        if (bRefreshFrame == OSIX_TRUE)
        {
            pCEECtrlEntry->u4CtrlAckMissCount++;
            DCBX_TRC_ARG2 (DCBX_TLV_TRC, "In %s : Ack Miss detected! u4CtrlAckMissCount:%d "
                    "and AckNo: %d \r\n", __FUNCTION__, pCEECtrlEntry->u4CtrlAckMissCount); 
            if (pCEECtrlEntry->u4CtrlAckMissCount == CEE_MAX_ACK_MISS_CNT)
            {
                pCEECtrlEntry->u4CtrlSeqNo        = DCBX_ZERO;
                pCEECtrlEntry->u4CtrlRcvdSeqNo    = DCBX_ZERO;
                pCEECtrlEntry->u4CtrlAckNo        = DCBX_ZERO;
                pCEECtrlEntry->u4CtrlRcvdAckNo    = DCBX_ZERO;
                pCEECtrlEntry->u4CtrlSyncNo       = DCBX_ZERO;
                pCEECtrlEntry->u4CtrlAckMissCount = DCBX_ZERO;

                CEESemRun (CEE_ETS_TLV_TYPE, &CeeFeatTlvParam,CEE_EV_INIT);
                CEESemRun (CEE_PFC_TLV_TYPE, &CeeFeatTlvParam,CEE_EV_INIT);
                CEESemRun (CEE_APP_PRI_TLV_TYPE, &CeeFeatTlvParam,CEE_EV_INIT);

                DCBX_TRC_ARG1 (DCBX_TLV_TRC, "Acknowledgement not received from peer. "
                        "Restart the state machine for %d!!!\r\n", pCEECtrlEntry->u4IfIndex);
                CEEFormAndSendTLV (pCEECtrlEntry->u4IfIndex, DCBX_LLDP_PORT_UPDATE, DCBX_ZERO);
                return DCBX_FAILURE;
            }
        }
    }

    if (bRefreshFrame == OSIX_TRUE)
    {
        return DCBX_FAILURE;
    }
    DCBX_TRC_ARG1 (DCBX_TLV_TRC, "Info: DCBX Peer detected on port %d!!!\r\n", pCEECtrlEntry->u4IfIndex);
    return DCBX_SUCCESS;
}

/**************************************************************************
 * FUNCTION NAME : CEEUtlHandleLLdpTxRxNotify
 *
 * DESCRIPTION   : This function is used to handle the notification 
 *                 from LLDP for TX and RX enable as well as disable
 *
 * INPUT         : u4IfIndex - Interface Id
 *                 u4TxOrRxStatus - Status for TX and RX 
 *
 * OUTPUT        : None
 *
 * RETURNS       : None
 *
 ***************************************************************************/
PUBLIC VOID
CEEUtlHandleLLdpTxRxNotify(UINT4 u4IfIndex, UINT4 u4TxOrRxStatus)
{
    tDcbxPortEntry     *pDcbxPortEntry = NULL;
    tDcbxCEETrapMsg     NotifyCEETrap;

    pDcbxPortEntry = DcbxUtlGetPortEntry (u4IfIndex);
    if (pDcbxPortEntry == NULL)
    {
        return;
    }

    MEMSET (&NotifyCEETrap, DCBX_ZERO, sizeof (tDcbxCEETrapMsg));

    switch(u4TxOrRxStatus)
    {
        case L2IWF_LLDP_TX_ENABLE_NOTIFY:
            {
                DCBX_TRC_ARG2 (DCBX_CONTROL_PLANE_TRC, 
                        "In CEEUtlHandleLLdpTxRxNotify: "
                        "Current LldpTxStatus for port %d is %d for "
                        "TX_ENABLE_NOTIFY \r\n", u4IfIndex,
                        pDcbxPortEntry->u1LldpTxStatus);

                if (DCBX_ENABLED != pDcbxPortEntry->u1LldpTxStatus)
                {
                    pDcbxPortEntry->u1LldpTxStatus = DCBX_ENABLED;

                    if ((DCBX_MODE_AUTO == pDcbxPortEntry->u1DcbxMode) ||
                            (DCBX_MODE_CEE == pDcbxPortEntry->u1DcbxMode))
                    {
                        DCBX_TRC (DCBX_CONTROL_PLANE_TRC, 
                                "CEEUtlHandleLLdpTxRxNotify: "
                                "Registering CEE as LLDP TX is enabled now!!!\r\n");

                        CEERegisterApplForPort (u4IfIndex, CEE_ETS_TLV_TYPE);
                        CEERegisterApplForPort (u4IfIndex, CEE_PFC_TLV_TYPE);
                        CEERegisterApplForPort (u4IfIndex, CEE_APP_PRI_TLV_TYPE);
                    }
                }
            }
            break;
        case L2IWF_LLDP_RX_ENABLE_NOTIFY:
            {
                DCBX_TRC_ARG1 (DCBX_CONTROL_PLANE_TRC, 
                        "Current LldpRxStatus:%d for RX_ENABLE_NOTIFY\r\n", 
                        pDcbxPortEntry->u1LldpRxStatus);

                if (DCBX_ENABLED != pDcbxPortEntry->u1LldpRxStatus)
                {
                    pDcbxPortEntry->u1LldpRxStatus = DCBX_ENABLED;

                    if ((DCBX_MODE_AUTO == pDcbxPortEntry->u1DcbxMode) ||
                            (DCBX_MODE_CEE == pDcbxPortEntry->u1DcbxMode))
                    {
                        DCBX_TRC (DCBX_CONTROL_PLANE_TRC, 
                                "CEEUtlHandleLLdpTxRxNotify: "
                                "Registering CEE as LLDP RX is enabled now!!!\r\n");

                        CEERegisterApplForPort (u4IfIndex, CEE_ETS_TLV_TYPE);
                        CEERegisterApplForPort (u4IfIndex, CEE_PFC_TLV_TYPE);
                        CEERegisterApplForPort (u4IfIndex, CEE_APP_PRI_TLV_TYPE);
                    }
                }
            }
            break;
        case L2IWF_LLDP_TX_DISABLE_NOTIFY:
            {
                DCBX_TRC_ARG1 (DCBX_CONTROL_PLANE_TRC, 
                        "Current LldpTxStatus: %d for TX_DISABLE_NOTIFY\r\n", 
                        pDcbxPortEntry->u1LldpTxStatus);

                if (DCBX_DISABLED != pDcbxPortEntry->u1LldpTxStatus)
                {
                    pDcbxPortEntry->u1LldpTxStatus = DCBX_DISABLED;

                    NotifyCEETrap.u4IfIndex = u4IfIndex;
                    NotifyCEETrap.unTrapMsg.u1OperVersion = DCBX_VER_CEE;
                    DcbxCEESendPortNotification (&NotifyCEETrap, DCBX_LLDP_TX_DISABLE_TRAP);

                    CEEUtlHandleLLdpTxRxDisableState(u4IfIndex);
                }
            }
            break;
        case L2IWF_LLDP_RX_DISABLE_NOTIFY:
            {
                DCBX_TRC_ARG1 (DCBX_CONTROL_PLANE_TRC, 
                        "Current LldpRxStatus: %d for RX_DISABLE_NOTIFY\r\n", 
                        pDcbxPortEntry->u1LldpRxStatus);

                if (DCBX_DISABLED != pDcbxPortEntry->u1LldpRxStatus)
                {
                    pDcbxPortEntry->u1LldpRxStatus = DCBX_DISABLED;

                    NotifyCEETrap.u4IfIndex = u4IfIndex;
                    NotifyCEETrap.unTrapMsg.u1OperVersion = DCBX_VER_CEE;
                    DcbxCEESendPortNotification (&NotifyCEETrap, DCBX_LLDP_RX_DISABLE_TRAP);

                    CEEUtlHandleLLdpTxRxDisableState(u4IfIndex);
                }
            }
            break;
        default:
            DCBX_TRC (DCBX_FAILURE_TRC, 
                    "CEEUtlHandleLLdpTxRxNotify: Unknown Msg!!!\r\n");
            break;
    }
    return;
}

/**************************************************************************
 * FUNCTION NAME : CEEUtlHandleLLdpTxRxDisableState
 *
 * DESCRIPTION   : This function is used to handle the Tx and RX disable
 *
 * INPUT         : u4IfIndex - Interface Id
 *
 * OUTPUT        : None
 *
 * RETURNS       : None
 *
 ***************************************************************************/
PUBLIC VOID
CEEUtlHandleLLdpTxRxDisableState(UINT4 u4IfIndex)
{
    tDcbxPortEntry     *pDcbxPortEntry = NULL;
    tDcbxAppRegInfo     DcbxAppPortMsg;

    pDcbxPortEntry = DcbxUtlGetPortEntry (u4IfIndex);
    if (pDcbxPortEntry == NULL)
    {
        return;
    }

    if ((DCBX_MODE_AUTO == pDcbxPortEntry->u1DcbxMode) && \
            (DCBX_DEFAULT_VER != pDcbxPortEntry->u1OperVersion))
    {
        /* During ageout if operversion is not default version and mode is auto switch to default
         * mode */
        DcbxUtlSwitchVersion(DCBX_DEFAULT_VER, u4IfIndex); /* This will take care of CEE AgeOut and IEEE register*/

        /* No need to call IEEE/CEE Deregister explicit here as in 
         * DcbxUtlSwitchVersion() de-register of IEEE/CEE is done once 
         * and again call to IEEE/CEE register wil fail as Tx/Rx is disabled */
    }
    if (DCBX_VER_CEE == pDcbxPortEntry->u1OperVersion)
    {
        /*Call CEE ageout*/
        DcbxUtlCallCEEAgeOut(u4IfIndex);

        /*Call CEE Deregister*/
        MEMSET (&DcbxAppPortMsg, DCBX_ZERO, sizeof (tDcbxAppRegInfo));
        CEE_GET_APPL_ID (DcbxAppPortMsg.DcbxAppId);

        CEEPostMsgToDCBX (DCBX_LLDP_PORT_DEREG, u4IfIndex,
                &DcbxAppPortMsg);
    }
}

/**************************************************************************
 * FUNCTION NAME : CEEUtlUpdateSmForPktErrors
 *
 * DESCRIPTION   : This function is used to call the SM for respective features
 *
 * INPUT         : u4IfIndex - Interface Id
 *                 pCeeAppTlvParam - Application Info structure
 *                 i4Event   - Event can be duplicate or incorrect version tlv
 *
 * OUTPUT        : None
 *
 * RETURNS       : None
 *
 ***************************************************************************/
VOID 
CEEUtlUpdateSmForPktErrors (tCeeTLVInfo CeeTLVInfo, INT4 i4Event)
{
    tPFCPortEntry      *pPFCPortEntry    = NULL;
    tETSPortEntry      *pETSPortEntry    = NULL;
    tAppPriPortEntry   *pAppPriPortEntry = NULL;
    tCEECtrlEntry      *pCEECtrlEntry    = NULL;

    pCEECtrlEntry = CEEUtlGetCtrlEntry (CeeTLVInfo.u4IfIndex);
    if (pCEECtrlEntry != NULL)
    {
        pCEECtrlEntry->u4CtrlSeqNo     = DCBX_ZERO;
        pCEECtrlEntry->u4CtrlRcvdSeqNo = DCBX_ZERO;
        pCEECtrlEntry->u4CtrlAckNo     = DCBX_ZERO;
        pCEECtrlEntry->u4CtrlRcvdAckNo = DCBX_ZERO; 
        pCEECtrlEntry->u4CtrlSyncNo    = DCBX_ZERO;
        pCEECtrlEntry->u4CtrlAckMissCount = DCBX_ZERO;
    }
    /* Updating ETS Entry */
    pETSPortEntry = ETSUtlGetPortEntry(CeeTLVInfo.u4IfIndex);
    if(pETSPortEntry != NULL)
    {
        pETSPortEntry->u4ETSConfRxTLVError++;
        CEESemRun (CEE_ETS_TLV_TYPE, &CeeTLVInfo, i4Event);
    }
    /* Updating PFC Entry */
    pPFCPortEntry = PFCUtlGetPortEntry (CeeTLVInfo.u4IfIndex);
    if(pPFCPortEntry != NULL)
    {
        pPFCPortEntry->u4PFCRxTLVError++;
        CEESemRun (CEE_PFC_TLV_TYPE, &CeeTLVInfo, i4Event);
    }
    /* Updating Priority Entry */
    pAppPriPortEntry = AppPriUtlGetPortEntry(CeeTLVInfo.u4IfIndex);
    if(pAppPriPortEntry != NULL)
    {
        pAppPriPortEntry->u4AppPriTLVRxError++;
        CEESemRun (CEE_APP_PRI_TLV_TYPE, &CeeTLVInfo, i4Event);
    }
}
/**************************************************************************
 * FUNCTION NAME : CEEUtlClearCeeCtrlParams
 *
 * DESCRIPTION   : This function is used to clear all cee related control TLV
 *                 params during DeRegistering CEE.
 *
 * INPUT         : u4IfIndex - Interface Id
 *
 * OUTPUT        : None
 *
 * RETURNS       : None
 *
 ***************************************************************************/
VOID 
CEEUtlClearCeeCtrlParams (UINT4 u4IfIndex)
{
    tCEECtrlEntry      *pCtrlEntry    = NULL;
    tDcbxPortEntry     *pDcbxPortEntry = NULL;

    pCtrlEntry = CEEUtlGetCtrlEntry (u4IfIndex);
    if (pCtrlEntry != NULL)
    {
        pCtrlEntry->u4CtrlSeqNo        = DCBX_ZERO;
        pCtrlEntry->u4CtrlRcvdSeqNo    = DCBX_ZERO;
        pCtrlEntry->u4CtrlAckNo        = DCBX_ZERO;
        pCtrlEntry->u4CtrlRcvdAckNo    = DCBX_ZERO;
        pCtrlEntry->u4CtrlSyncNo       = DCBX_ZERO;
        pCtrlEntry->u4CtrlAckMissCount = DCBX_ZERO;
        pCtrlEntry->u4CtrlTxTLVCount   = DCBX_ZERO;
        pCtrlEntry->u4CtrlRxTLVCount   = DCBX_ZERO;
        pCtrlEntry->u4CtrlRxTLVErrorCount = DCBX_ZERO;
    }
    /* Clear the Peer Oper and Max version */
    pDcbxPortEntry = DcbxUtlGetPortEntry (u4IfIndex);

    if (pDcbxPortEntry != NULL)
    {
        pDcbxPortEntry->u1PeerOperVersion = DCBX_VER_UNKNOWN;
        pDcbxPortEntry->u1PeerMaxVersion = DCBX_VER_UNKNOWN;
    }
}
/* END OF FILE */
