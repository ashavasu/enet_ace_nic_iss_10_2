/*************************************************************************
 * Copyright (C) 2010 Aricent Inc . All Rights Reserved
 * $Id: dcbxapi.c,v 1.13 2017/01/25 13:19:41 siva Exp $
 * Description: This file contains DCBX API which are exported
 *               to other modules.
****************************************************************************/

#include "dcbxinc.h"
/***************************************************************************
 *  FUNCTION NAME : DcbxApiApplCallbkFunc
 * 
 *  DESCRIPTION   : This call back function will be registered with LLDP
 *                  to handle the TLV Reception/Re-Registration/Age-Out 
 *                  events.
 * 
 *  INPUT         : pLldpAppTlv - LLDP Appl Tlv Structure
 * 
 *  OUTPUT        : None
 * 
 *  RETURNS       : None
 * 
 * **************************************************************************/
PUBLIC VOID
DcbxApiApplCallbkFunc (tLldpAppTlv * pLldpAppTlv)
{

    tDcbxQueueMsg      *pMsg = NULL;
    /* Allocates the memory from the queue memory pool */
    if ((pMsg = (tDcbxQueueMsg *) MemAllocMemBlk
         (gDcbxGlobalInfo.DcbxQPktPoolId)) == NULL)
    {
        DCBX_TRC (DCBX_RESOURCE_TRC | DCBX_CRITICAL_TRC, "DcbxApiApplCallbkFunc:"
                  "DCBX Queue Message memory Allocation Failed !!!\r\n");
        return;
    }

    /* Fill the information to be sent to DCBX queue from the LLDP
     * structure */
    MEMSET (pMsg, DCBX_ZERO, sizeof (tDcbxQueueMsg));
    pMsg->u4MsgType = pLldpAppTlv->u4MsgType;
    pMsg->u4IfIndex = pLldpAppTlv->u4RxAppTlvPortId;
    /* If TLV information is not present then no need to copy. This occurs when 
     * there is Age Out message received from the LLDP module  */
    if (pLldpAppTlv->pu1RxAppTlv != NULL)
    {
        MEMCPY (pMsg->unMsgParam.ApplTlvParam.au1DcbxTlv,
                pLldpAppTlv->pu1RxAppTlv, pLldpAppTlv->u2RxAppTlvLen);
    }
    MEMCPY (&(pMsg->DcbxAppId), &(pLldpAppTlv->LldpAppId), sizeof (tDcbxAppId));
    pMsg->unMsgParam.ApplTlvParam.i4RemIndex = pLldpAppTlv->i4RemIndex;
    pMsg->unMsgParam.ApplTlvParam.u4RemLastUpdateTime =
        pLldpAppTlv->u4RxAppTlvTimeStamp;
    pMsg->unMsgParam.ApplTlvParam.u2RxTlvLen = pLldpAppTlv->u2RxAppTlvLen;
    MEMCPY (pMsg->unMsgParam.ApplTlvParam.RemLocalDestMacAddr,
            pLldpAppTlv->RemLocalDestMacAddr, MAC_ADDR_LEN);
    MEMCPY (pMsg->unMsgParam.ApplTlvParam.RemNodeMacAddr, 
	    pLldpAppTlv->RemNodeMacAddr, MAC_ADDR_LEN);
    pMsg->unMsgParam.ApplTlvParam.u4RemLocalDestMACIndex = 
        pLldpAppTlv->u4RemLocalDestMACIndex;
    /* Post it to the DCBX queue */
    if (DcbxQueEnqMsg (pMsg) == OSIX_FAILURE)
    {
        DCBX_TRC (DCBX_CRITICAL_TRC, "DcbxApiApplCallbkFunc:"
                  "DCBX EnQueue Message into queue Failed !!!\r\n");
    }
    return;

}

/***************************************************************************
 *  FUNCTION NAME : DcbxApiPortRequest
 * 
 *  DESCRIPTION   : This call back function will notify the port creation/
 *                  deletion to the DCBX module.
 * 
 *  INPUT         : u4PortId - Port Number.
 *                  u1Status - Message Type- Create/Delete/Map/UnMap
 * 
 *  OUTPUT        : None
 * 
 *  RETURNS       : None
 * 
 * **************************************************************************/
PUBLIC INT4
DcbxApiPortRequest (UINT4 u4PortId, UINT1 u1Status)
{
    tDcbxQueueMsg      *pMsg = NULL;

    if (gDcbxGlobalInfo.u1DCBXSystemCtrl != DCBX_START)
    {
        /* If system control status is not up, then return 
         * success without processing the port request */
        return OSIX_SUCCESS;
    }
    /* Allocates the memory for message queue from the memory pool */
    if ((pMsg = (tDcbxQueueMsg *) MemAllocMemBlk
         (gDcbxGlobalInfo.DcbxQPktPoolId)) == NULL)
    {
        DCBX_TRC (DCBX_RESOURCE_TRC | DCBX_CRITICAL_TRC,
                  "DcbxApiPortRequest:"
                  "DCBX Queue Message memory Allocation Failed !!!\r\n");

        return OSIX_FAILURE;
    }
    /* Send the port creation/deleteion message to the DCBX */
    MEMSET (pMsg, DCBX_ZERO, sizeof (tDcbxQueueMsg));
    pMsg->u4IfIndex = u4PortId;

    switch (u1Status)
    {
        case DCBX_CREATE_IF_MSG:
        case DCBX_MAP_IF_MSG:
            pMsg->u4MsgType = DCBX_CREATE_PORT_MSG;
            break;
        case DCBX_DELETE_IF_MSG:
        case DCBX_UNMAP_IF_MSG:
            pMsg->u4MsgType = DCBX_DELETE_PORT_MSG;
            break;
        default:
            DCBX_TRC (DCBX_FAILURE_TRC, "DcbxApiPortRequest:"
                      "Invalid Port Message recieved !!!\r\n");
            MemReleaseMemBlock (gDcbxGlobalInfo.DcbxQPktPoolId, (UINT1 *) pMsg);
            return OSIX_FAILURE;
    }

    if (DcbxQueEnqMsg (pMsg) == OSIX_FAILURE)
    {
        DCBX_TRC (DCBX_CRITICAL_TRC, "DcbxApiPortRequest:"
                  "DCBX EnQueue Message into queue Failed !!!\r\n");
        return OSIX_FAILURE;

    }
    return OSIX_SUCCESS;
}

/***************************************************************************
 *  FUNCTION NAME : DcbxApiPortToPortChannelRequest
 * 
 *  DESCRIPTION   : This call back function will notify the member port 
 *                  addition/removal from LAGG interface to the DCBX module.
 * 
 *  INPUT         : u4PortId - Port Number.
 *                  u4PortChId - Port Channel ID
 *                  u1Status - Message Type- Add/Remove
 * 
 *  OUTPUT        : None
 * 
 *  RETURNS       : None
 * 
 * **************************************************************************/
PUBLIC INT4
DcbxApiPortToPortChannelRequest (UINT4 u4PortId, UINT4 u4PortChId,
                                 UINT1 u1Status)
{
    tDcbxQueueMsg      *pMsg = NULL;

    if (gDcbxGlobalInfo.u1DCBXSystemCtrl != DCBX_START)
    {
        /* If system control status is not up, then return 
         * success without processing the port request */
        return OSIX_SUCCESS;
    }
    /* Allocates the memory for message queue from the memory pool */
    if ((pMsg = (tDcbxQueueMsg *) MemAllocMemBlk
         (gDcbxGlobalInfo.DcbxQPktPoolId)) == NULL)
    {
        DCBX_TRC (DCBX_RESOURCE_TRC | DCBX_FAILURE_TRC,
                  "DcbxApiPortToPortChannelRequest:"
                  "DCBX Queue Message memory Allocation Failed !!!\r\n");

        return OSIX_FAILURE;
    }
    /* Send the port creation message to the DCBX */
    MEMSET (pMsg, DCBX_ZERO, sizeof (tDcbxQueueMsg));
    /* Send the Port Channel interface in which the port is added/removed */
    pMsg->u4IfIndex = u4PortId;
    pMsg->unMsgParam.u4PortChannelId = u4PortChId;

    switch (u1Status)
    {
        case DCBX_ADD_LA_MEM_MSG:
            pMsg->u4MsgType = DCBX_ADD_LAG_MEMBER_MSG;
            break;
        case DCBX_REM_LA_MEM_MSG:
            pMsg->u4MsgType = DCBX_REM_LAG_MEMBER_MSG;
            break;
        default:
            DCBX_TRC (DCBX_FAILURE_TRC, "DcbxApiPortToPortChannelRequest:"
                      "Invalid Port Message recieved !!!\r\n");
            MemReleaseMemBlock (gDcbxGlobalInfo.DcbxQPktPoolId, (UINT1 *) pMsg);
            return OSIX_FAILURE;
    }

    if (DcbxQueEnqMsg (pMsg) == OSIX_FAILURE)
    {
        DCBX_TRC (DCBX_FAILURE_TRC, "DcbxApiPortToPortChannelRequest:"
                  "DCBX EnQueue Message into queue Failed !!!\r\n");
        return OSIX_FAILURE;

    }
    return OSIX_SUCCESS;
}

/***************************************************************************
 *  FUNCTION NAME : DcbxApiApplPortUpdate
 * 
 *  DESCRIPTION   : This function is used to post the port update message
 *                  to LLDP.
 * 
 *  INPUT         : u4IfIndex - Port Number.
 *                  pDcbxAppPortMsg - Structure contails Appl Reg Info
 * 
 *  OUTPUT        : None
 * 
 *  RETURNS       : OSIX_SUCCESS/OSIX_FAILURE
 * 
 * **************************************************************************/
PUBLIC INT4
DcbxApiApplPortUpdate (UINT4 u4IfIndex, tDcbxAppRegInfo * pDcbxAppPortMsg)
{
    static tDcbxAppEntry TempDcbxAppEntry;
    /* This contains port list array which results in stack size
     * exceeds 500. So declaring this varaible as static */
    tLldpAppPortMsg     LldpAppPortMsg;
    tDcbxAppEntry      *pDcbxAppEntry = NULL;
    tDcbxPortEntry     *pDcbxPortEntry = NULL;

    MEMSET (&TempDcbxAppEntry, DCBX_ZERO, sizeof (tDcbxAppEntry));
    MEMCPY (&(TempDcbxAppEntry.DcbxAppId),
            &(pDcbxAppPortMsg->DcbxAppId), sizeof (tDcbxAppId));

    /* Get the application entry from the DCBX Application table */
    pDcbxAppEntry = (tDcbxAppEntry *) RBTreeGet (gDcbxGlobalInfo.pRbDcbxAppTbl,
                                                 (tRBElem *) &
                                                 TempDcbxAppEntry);

    /* Check if the Application is already registered for the particular port
     * in the DCBX. If not return failure*/
    if ((pDcbxAppEntry == NULL) ||
        (pDcbxAppEntry->ApplMappedIfPortList[u4IfIndex] == OSIX_FALSE))
    {
        DCBX_TRC_ARG1 (DCBX_CRITICAL_TRC | DCBX_FAILURE_TRC, "DcbxApiApplPortUpdate:"
                  "DCBX Port TLV Update FAILED - Not Registered with LLDP for port %d!!!\r\n", u4IfIndex);
        return OSIX_FAILURE;
    }
    pDcbxPortEntry = DcbxUtlGetPortEntry (u4IfIndex);
    if ((pDcbxPortEntry != NULL) &&
        (pDcbxPortEntry->u1DcbxAdminStatus == DCBX_ENABLED))
    {
        MEMSET (&LldpAppPortMsg, DCBX_ZERO, sizeof (tLldpAppPortMsg));
        /* Fill the Required LLDP Paramters */
        DcbxUtlFillLldpInfo (pDcbxAppPortMsg, &LldpAppPortMsg);

        /* If mode is auto, match the OUI and enable TlvTxStatus based on
         * Operating version only */
        if (pDcbxPortEntry->u1DcbxMode == DCBX_MODE_AUTO)
        {
            if ((pDcbxPortEntry->u1OperVersion == DCBX_VER_IEEE) &&
                    ((MEMCMP (pDcbxAppPortMsg->DcbxAppId.au1OUI, gau1DcbxOUI,
                              DCBX_MAX_OUI_LEN) == 0)))
            {
                LldpAppPortMsg.bAppTlvTxStatus = OSIX_TRUE;
            }
            else if ((pDcbxPortEntry->u1OperVersion == DCBX_VER_CEE) &&
                    ((MEMCMP (pDcbxAppPortMsg->DcbxAppId.au1OUI, gau1CEEOUI,
                              DCBX_MAX_OUI_LEN) == 0)))
            {
                LldpAppPortMsg.bAppTlvTxStatus = OSIX_TRUE;
                LldpAppPortMsg.u1AppRefreshFrameNotify = OSIX_TRUE;
            }
            else
            {
                LldpAppPortMsg.bAppTlvTxStatus = OSIX_FALSE;
            }
        }
        else if (pDcbxPortEntry->u1DcbxMode == DCBX_MODE_CEE)
        {
            LldpAppPortMsg.u1AppRefreshFrameNotify = OSIX_TRUE;
        }


        /* Call the LLDP API to update the TLV infomratin for the port */
        if (DcbxPortL2IwfApplPortRequest (u4IfIndex, &LldpAppPortMsg,
                                          L2IWF_LLDP_APPL_PORT_UPDATE)
            != OSIX_SUCCESS)
        {
            DCBX_TRC_ARG6 (DCBX_CRITICAL_TRC, "DcbxApiApplPortUpdate:"
                    "DcbxPortL2IwfApplPortRequest() - L2IWF_LLDP_APPL_PORT_UPDATE"
                    " TLV update message failed with Type=%d "
                  "subtype=%d and OUI={0x%x|0x%x|0x%x} for port %d successfull!!!\r\n",
                  pDcbxAppPortMsg->DcbxAppId.u2TlvType,
                  pDcbxAppPortMsg->DcbxAppId.u1SubType,
                  pDcbxAppPortMsg->DcbxAppId.au1OUI[0],
                  pDcbxAppPortMsg->DcbxAppId.au1OUI[1],
                  pDcbxAppPortMsg->DcbxAppId.au1OUI[2],
                  u4IfIndex);

            return OSIX_FAILURE;
        }
    }
    return OSIX_SUCCESS;
}

/***************************************************************************
 *  FUNCTION NAME : DcbxApiApplPortReg
 * 
 *  DESCRIPTION   : This function is used to post the Port Registration
 *                  message to LLDP
 * 
 *  INPUT         : u4IfIndex - Port Number.
 *                  pDcbxAppPortMsg - Structure to DCBX Appl reg Info
 * 
 *  OUTPUT        : None
 * 
 *  RETURNS       : OSIX_SUCCESS/OSIX_FAILURE
 * 
 * **************************************************************************/
PUBLIC INT4
DcbxApiApplPortReg (UINT4 u4IfIndex, tDcbxAppRegInfo * pDcbxAppPortMsg)
{
    static tDcbxAppEntry TempDcbxAppEntry;
    /* This contains port list array which results in stack size
     * exceeds 500. So declaring this varaible as static */
    tLldpAppPortMsg     LldpAppPortMsg;
    tL2LldpAppInfo      L2LldpAppInfo;
    tDcbxAppEntry      *pDcbxAppEntry = NULL;
    tDcbxPortEntry     *pDcbxPortEntry = NULL;
    UINT4               u4RetValue = 0;

    UNUSED_PARAM (u4RetValue);

    MEMSET (&TempDcbxAppEntry, DCBX_ZERO, sizeof (tDcbxAppEntry));
    MEMCPY (&(TempDcbxAppEntry.DcbxAppId),
            &(pDcbxAppPortMsg->DcbxAppId), sizeof (tDcbxAppId));

    /* Get the application entry from the Application table */
    pDcbxAppEntry =
        (tDcbxAppEntry *) RBTreeGet (gDcbxGlobalInfo.pRbDcbxAppTbl,
                                     (tRBElem *) & TempDcbxAppEntry);
    /* If not present then add the application node to the 
     * application table since first time this application is 
     * registered and update the coresponding port bit to true. */
    if (pDcbxAppEntry == NULL)
    {
        /* Allocate the memory for application entry from 
         * the memory pool */
        if ((pDcbxAppEntry = (tDcbxAppEntry *)
             MemAllocMemBlk (gDcbxGlobalInfo.DcbxApplPoolId)) == NULL)
        {
            DCBX_TRC (DCBX_RESOURCE_TRC | DCBX_CRITICAL_TRC,
                      "DcbxApiApplPortReg: "
                      "DCBX Application table Memory Allocation Failed!!!\r\n");
            return OSIX_FAILURE;
        }

        MEMSET (pDcbxAppEntry, DCBX_ZERO, sizeof (tDcbxAppEntry));
        /* Fill the necssary infomration and update the Port list bit */
        MEMCPY (&(pDcbxAppEntry->DcbxAppId),
                &(pDcbxAppPortMsg->DcbxAppId), sizeof (tDcbxAppId));
        pDcbxAppEntry->pAppCallBackFn = pDcbxAppPortMsg->pApplCallBackFn;
        pDcbxAppEntry->ApplMappedIfPortList[u4IfIndex] = OSIX_TRUE;

        /* Add the application entry to the RB Tree */
        u4RetValue =
            RBTreeAdd (gDcbxGlobalInfo.pRbDcbxAppTbl,
                       (tRBElem *) pDcbxAppEntry);

        /* Since this application is registered with DCBX for the first time
         * it needs to be registered with L2IWF for getting the 
         * re registration request when the LLDP module goes down 
         * and comes up */
        /* Fill the necssary infprmation for L2IWF registration */
        MEMSET (&L2LldpAppInfo, DCBX_ZERO, sizeof (tL2LldpAppInfo));
        DcbxUtlFillL2LldpInfo (pDcbxAppPortMsg, &L2LldpAppInfo);

        DCBX_TRC_ARG1 (DCBX_TLV_TRC, "In DcbxApiApplPortReg: "
                  "DCBX Application registration request with L2IWF!!! "
                  "for the port %d\r\n",u4IfIndex);
        /* Call the respective L2IWF funtion */
        if (DcbxPortL2IwfApplRequest (&L2LldpAppInfo,
                                      L2IWF_LLDP_APPL_REGISTER) == OSIX_FAILURE)
        {
            DCBX_TRC_ARG6 (DCBX_CRITICAL_TRC, "DcbxApiApplPortReg: "
                    "DcbxPortL2IwfApplRequest() - L2IWF_LLDP_APPL_REGISTER"
                    "TLV Registration with LLDP failed with Type=%d "
                    "subtype=%d and OUI={0x%x|0x%x|0x%x} for port %d!!!\r\n",
                    pDcbxAppPortMsg->DcbxAppId.u2TlvType,
                    pDcbxAppPortMsg->DcbxAppId.u1SubType,
                    pDcbxAppPortMsg->DcbxAppId.au1OUI[0],
                    pDcbxAppPortMsg->DcbxAppId.au1OUI[1],
                    pDcbxAppPortMsg->DcbxAppId.au1OUI[2],
                    u4IfIndex);
            return (OSIX_FAILURE);
        }
        DCBX_TRC_ARG1 (DCBX_TLV_TRC, "In DcbxApiApplPortReg: "
                  "DCBX Application registered Successfully!!! with L2IWF "
                  "for the port %d\r\n",u4IfIndex);
    }
    else
    {
        /* If the apllication is already registered with DCBX then 
         * set the port registration bit */
        pDcbxAppEntry->ApplMappedIfPortList[u4IfIndex] = OSIX_TRUE;
    }

    MEMSET (&LldpAppPortMsg, DCBX_ZERO, sizeof (tLldpAppPortMsg));

    pDcbxPortEntry = DcbxUtlGetPortEntry (u4IfIndex);
    if ((pDcbxPortEntry != NULL) &&
        (pDcbxPortEntry->u1DcbxAdminStatus == DCBX_ENABLED))
    {
        /* Fill the Required LLDP Paramters */
        DcbxUtlFillLldpInfo (pDcbxAppPortMsg, &LldpAppPortMsg);

        /* If mode is auto, match the appid and enable TlvTxStatus based on
         * Operating version only */
        if (pDcbxPortEntry->u1DcbxMode == DCBX_MODE_AUTO)
        {
            if ((pDcbxPortEntry->u1OperVersion == DCBX_VER_IEEE) &&
                    ((MEMCMP (pDcbxAppPortMsg->DcbxAppId.au1OUI, gau1DcbxOUI,
                              DCBX_MAX_OUI_LEN) == 0)))
            {
                LldpAppPortMsg.bAppTlvTxStatus = OSIX_TRUE;
            }
            else if ((pDcbxPortEntry->u1OperVersion == DCBX_VER_CEE) &&
                    ((MEMCMP (pDcbxAppPortMsg->DcbxAppId.au1OUI, gau1CEEOUI,
                              DCBX_MAX_OUI_LEN) == 0)))
            {
                LldpAppPortMsg.bAppTlvTxStatus = OSIX_TRUE;
                LldpAppPortMsg.u1AppRefreshFrameNotify = OSIX_TRUE;
            }
            else
            {
                LldpAppPortMsg.bAppTlvTxStatus = OSIX_FALSE;
            }
        }
        else if (pDcbxPortEntry->u1DcbxMode == DCBX_MODE_CEE)
        {
            LldpAppPortMsg.u1AppRefreshFrameNotify = OSIX_TRUE;
        }

        if (DcbxPortL2IwfApplPortRequest (u4IfIndex, &LldpAppPortMsg,
                                          L2IWF_LLDP_APPL_PORT_REGISTER)
            != OSIX_SUCCESS)
        {
            DCBX_TRC_ARG6 (DCBX_CRITICAL_TRC, "DcbxApiApplPortReg: "
                    "DcbxPortL2IwfApplPortRequest() - L2IWF_LLDP_APPL_PORT_REGISTER"
                    "TLV Registration with LLDP failed with Type=%d "
                  "subtype=%d and OUI={0x%x|0x%x|0x%x} for port %d!!!\r\n",
                  pDcbxAppPortMsg->DcbxAppId.u2TlvType,
                  pDcbxAppPortMsg->DcbxAppId.u1SubType,
                  pDcbxAppPortMsg->DcbxAppId.au1OUI[0],
                  pDcbxAppPortMsg->DcbxAppId.au1OUI[1],
                  pDcbxAppPortMsg->DcbxAppId.au1OUI[2],
                  u4IfIndex);

            return OSIX_FAILURE;
        }

        if (pDcbxAppPortMsg->DcbxAppId.u1SubType == CEE_TLV_SUBTYPE)
            DCBX_TRC_ARG6 (DCBX_CRITICAL_TRC, "DCBX Registered with LLDP with Type: %d "
                "Subtype: %d and OUI: {0x%x|0x%x|0x%x} for port %d!!!\r\n", 
                pDcbxAppPortMsg->DcbxAppId.u2TlvType,
                pDcbxAppPortMsg->DcbxAppId.u1SubType,
                pDcbxAppPortMsg->DcbxAppId.au1OUI[0],
                pDcbxAppPortMsg->DcbxAppId.au1OUI[1],
                pDcbxAppPortMsg->DcbxAppId.au1OUI[2],
                u4IfIndex);
    }
    return OSIX_SUCCESS;

}

/***************************************************************************
 *  FUNCTION NAME : DcbxApiApplPortDeReg
 * 
 *  DESCRIPTION   : This function is used to De Register Appl from the 
 *                  LLDP.
 * 
 *  INPUT         : u4IfIndex - Port Number.
 *                  pDcbxAppPortMsg - Structure to store the DCBX Appl Reg 
 *                  Info.
 * 
 *  OUTPUT        : None
 * 
 *  RETURNS       : OSIX_SUCCESS/OSIX_FAILURE
 * 
 * **************************************************************************/
PUBLIC INT4
DcbxApiApplPortDeReg (UINT4 u4IfIndex, tDcbxAppRegInfo * pDcbxAppPortMsg)
{
    static tDcbxAppEntry TempDcbxAppEntry;
    /* This contains port list array which results in stack size
     * exceeds 500. So declaring this varaible as static */
    tLldpAppPortMsg     LldpAppPortMsg;
    tL2LldpAppInfo      L2LldpAppInfo;
    tDcbxAppEntry      *pDcbxAppEntry = NULL;
    tDcbxPortEntry     *pDcbxPortEntry = NULL;
    UINT4               u4PorNum = DCBX_ZERO;
    BOOL1               i1FoundFlag = OSIX_FALSE;

    MEMSET (&TempDcbxAppEntry, DCBX_ZERO, sizeof (tDcbxAppEntry));
    MEMCPY (&(TempDcbxAppEntry.DcbxAppId),
            &(pDcbxAppPortMsg->DcbxAppId), sizeof (tDcbxAppId));
    /* Get the application entry from the Application table */
    pDcbxAppEntry = (tDcbxAppEntry *) RBTreeGet (gDcbxGlobalInfo.pRbDcbxAppTbl,
                                                 (tRBElem *) &
                                                 TempDcbxAppEntry);
    /* If the entry is not present then return FAILURE since no port for this 
     * application is registered with DCBX. If entry is present but the 
     * corresponding port bit is not set then return FAILURE since the 
     * particular port is not registered with DCBX for this application */
    if ((pDcbxAppEntry == NULL) ||
        (pDcbxAppEntry->ApplMappedIfPortList[u4IfIndex] == OSIX_FALSE))
    {
        DCBX_TRC_ARG6 (DCBX_CRITICAL_TRC, "DcbxApiApplPortDeReg:"
                  "Cause for failure: "
                  "1. Port not registered earlier\n"
                  "2. Registerd, but port bit is not set "
                  "Parmas: Type=%d "
                  "subtype=%d and OUI={0x%x|0x%x|0x%x} for port %d!!!\r\n",
                  pDcbxAppPortMsg->DcbxAppId.u2TlvType,
                  pDcbxAppPortMsg->DcbxAppId.u1SubType,
                  pDcbxAppPortMsg->DcbxAppId.au1OUI[0],
                  pDcbxAppPortMsg->DcbxAppId.au1OUI[1],
                  pDcbxAppPortMsg->DcbxAppId.au1OUI[2],
                  u4IfIndex);
        return OSIX_FAILURE;
    }
    else
    {
        /* Make the port registration bit as not set */
        pDcbxAppEntry->ApplMappedIfPortList[u4IfIndex] = OSIX_FALSE;
        /* Scan all the port bit list and if no port is registered then remove 
         * this application entry from the application table */
        for (u4PorNum = DCBX_ZERO; u4PorNum < DCBX_MAX_PORT_LIST_SIZE;
             u4PorNum++)
        {
            if (pDcbxAppEntry->ApplMappedIfPortList[u4PorNum] == OSIX_TRUE)
            {
                i1FoundFlag = OSIX_TRUE;
                break;
            }
        }
        /* If no port is registered then remove the application entry and
         * De register this apllication with L2IWF since this application
         * no longer interacts with LLDP  */
        if (i1FoundFlag == OSIX_FALSE)
        {
            RBTreeRemove (gDcbxGlobalInfo.pRbDcbxAppTbl,
                          (UINT1 *) pDcbxAppEntry);
            MemReleaseMemBlock (gDcbxGlobalInfo.DcbxApplPoolId,
                                (UINT1 *) pDcbxAppEntry);

            MEMSET (&L2LldpAppInfo, DCBX_ZERO, sizeof (tL2LldpAppInfo));
            /* Fill the necessary infomration to send it L2IWF */
            DcbxUtlFillL2LldpInfo (pDcbxAppPortMsg, &L2LldpAppInfo);
            DCBX_TRC (DCBX_TLV_TRC, "In DcbxApiApplPortDeReg: "
                      "DCBX Application De register request with L2IWF!!!\r\n");
            /* De Registration of this application with L2IWF */
            if (DcbxPortL2IwfApplRequest (&L2LldpAppInfo,
                                          L2IWF_LLDP_APPL_DEREGISTER) ==
                OSIX_FAILURE)
            {
                DCBX_TRC_ARG6 (DCBX_CRITICAL_TRC, "DcbxApiApplPortDeReg:"
                        "DcbxPortL2IwfApplRequest() - L2IWF_LLDP_APPL_DEREGISTER"
                        "Type=%d "
                        "subtype=%d and OUI={0x%x|0x%x|0x%x} for port %d!!!\r\n",
                        pDcbxAppPortMsg->DcbxAppId.u2TlvType,
                        pDcbxAppPortMsg->DcbxAppId.u1SubType,
                        pDcbxAppPortMsg->DcbxAppId.au1OUI[0],
                        pDcbxAppPortMsg->DcbxAppId.au1OUI[1],
                        pDcbxAppPortMsg->DcbxAppId.au1OUI[2],
                        u4IfIndex);
                return (OSIX_FAILURE);
            }
        }
    }

    pDcbxPortEntry = DcbxUtlGetPortEntry (u4IfIndex);
    if ((pDcbxPortEntry != NULL) &&
        (pDcbxPortEntry->u1DcbxAdminStatus == DCBX_ENABLED))
    {
        MEMSET (&LldpAppPortMsg, DCBX_ZERO, sizeof (tLldpAppPortMsg));
        /* Fill the Required LLDP Paramters */
        DcbxUtlFillLldpInfo (pDcbxAppPortMsg, &LldpAppPortMsg);

        if (pDcbxPortEntry->u1DcbxMode == DCBX_MODE_AUTO)
        {
            if ((pDcbxPortEntry->u1OperVersion == DCBX_VER_IEEE) &&
                    ((MEMCMP (pDcbxAppPortMsg->DcbxAppId.au1OUI, gau1DcbxOUI,
                              DCBX_MAX_OUI_LEN) == 0)))
            {
                LldpAppPortMsg.bAppTlvTxStatus = OSIX_TRUE;
            }
            else if ((pDcbxPortEntry->u1OperVersion == DCBX_VER_CEE) &&
                    ((MEMCMP (pDcbxAppPortMsg->DcbxAppId.au1OUI, gau1CEEOUI,
                              DCBX_MAX_OUI_LEN) == 0)))
            {
                LldpAppPortMsg.bAppTlvTxStatus = OSIX_TRUE;
                LldpAppPortMsg.u1AppRefreshFrameNotify = OSIX_TRUE;
            }
            else
            {
                LldpAppPortMsg.bAppTlvTxStatus = OSIX_FALSE;
            }
        }
        else if (pDcbxPortEntry->u1DcbxMode == DCBX_MODE_CEE)
        {
            LldpAppPortMsg.u1AppRefreshFrameNotify = OSIX_TRUE;
        }

        if (DcbxPortL2IwfApplPortRequest (u4IfIndex, &LldpAppPortMsg,
                                          L2IWF_LLDP_APPL_PORT_DEREGISTER)
            != OSIX_SUCCESS)
        {
            DCBX_TRC_ARG6 (DCBX_CRITICAL_TRC, "DcbxApiApplPortDeReg:"
                    "DcbxPortL2IwfApplRequest() - L2IWF_LLDP_APPL_PORT_DEREGISTER"
                    "Degistration not successfull for Type=%d "
                  "subtype=%d and OUI={0x%x|0x%x|0x%x} for port %d!!!\r\n",
                  pDcbxAppPortMsg->DcbxAppId.u2TlvType,
                  pDcbxAppPortMsg->DcbxAppId.u1SubType,
                  pDcbxAppPortMsg->DcbxAppId.au1OUI[0],
                  pDcbxAppPortMsg->DcbxAppId.au1OUI[1],
                  pDcbxAppPortMsg->DcbxAppId.au1OUI[2],
                  u4IfIndex);

            return OSIX_FAILURE;
        }            

        if (pDcbxAppPortMsg->DcbxAppId.u1SubType == CEE_TLV_SUBTYPE)
            DCBX_TRC_ARG6 (DCBX_CRITICAL_TRC, "DCBX TLV De-Registered with LLDP"
                "Type: %d "
                "Subtype: %d and OUI: {0x%x|0x%x|0x%x} for port %d!!!\r\n", 
                pDcbxAppPortMsg->DcbxAppId.u2TlvType,
                pDcbxAppPortMsg->DcbxAppId.u1SubType,
                pDcbxAppPortMsg->DcbxAppId.au1OUI[0],
                pDcbxAppPortMsg->DcbxAppId.au1OUI[1],
                pDcbxAppPortMsg->DcbxAppId.au1OUI[2],
                u4IfIndex);
    }
    return OSIX_SUCCESS;
}

/***************************************************************************
 *  FUNCTION NAME : DcbxApiGetPfcInfo
 * 
 *  DESCRIPTION   : This function is used to fill the PFC realted info
 *                  whenever needed by QOSX HW Audit process.
 *                  Since PFC NPAPI is present in the QOSX, hardware audit
 *                  process for PFC NPAPI will be done by QOSX and 
 *                  these info are needed by QOSX.
 * 
 *  INPUT         : pPfcHwEntry - PFC Hardware fill structure
 *                  u1Type - Either Fill Entry or Update Profile ID Type.
 *  
 *  OUTPUT        : None
 * 
 *  RETURNS       : None
 * 
 * **************************************************************************/
PUBLIC VOID
DcbxApiGetPfcInfo (UINT1 u1PfcProfile, UINT1 *pu1PfcHwCfgFlag, UINT4
                   *pu4PfcMinThreshold, UINT4 *pu4PfcMaxThreshold,
                   INT4 *pi4PfcHwProfileId, UINT1 u1Type)
{
    if (u1Type == DCBX_PFC_FILL_PROFILE_ENTRY)
    {
        if (*pu1PfcHwCfgFlag == PFC_THR_CFG)
        {
            *pu4PfcMinThreshold = gPFCGlobalInfo.u4PFCMinThresh;
            *pu4PfcMaxThreshold = gPFCGlobalInfo.u4PFCMaxThresh;
        }
        else
        {
            if (gPFCGlobalInfo.gaPFCProfileEntry[u1PfcProfile].u1IsStatusValid
                == OSIX_FALSE)
            {
                *pu1PfcHwCfgFlag = PFC_DEL_PROFILE;
            }
            else
            {
                *pi4PfcHwProfileId =
                    gPFCGlobalInfo.gaPFCProfileEntry[u1PfcProfile].
                    i4PfcHwProfileId;
            }

        }
    }
    else if (u1Type == DCBX_PFC_UPDATE_PROFILE_ID)
    {

        if (*pu1PfcHwCfgFlag == PFC_DEL_PROFILE)
        {
            gPFCGlobalInfo.gaPFCProfileEntry[u1PfcProfile].i4PfcHwProfileId
                = PFC_INVALID_ID;
        }
        if (*pu1PfcHwCfgFlag == PFC_PORT_CFG)
        {
            gPFCGlobalInfo.gaPFCProfileEntry[u1PfcProfile].i4PfcHwProfileId =
                *pi4PfcHwProfileId;
        }
    }
    return;
}
/*************************************************************************** 
 *  FUNCTION NAME : DcbxApiVerifyTlvisDcbx 
 * 
 *  DESCRIPTION   : This function is used to verify received TLV is registered 
 *                  by DCBX module or not 
 * 
 *  INPUT         : u2TlvType - Tlv Type 
 *                : u1SubType - Sub Type 
 *                : au1OUI    - OUI 
 * 
 *  OUTPUT        : None 
 * 
 *  RETURNS       : OSIX_TRUE/OSIX_FALSE 
 * 
 * **************************************************************************/ 
PUBLIC INT4 
DcbxApiVerifyTlvisDcbx (UINT2 u2TlvType, UINT1 u1SubType, UINT1 *pau1OUI) 
{ 
     if ((u2TlvType == ETS_TLV_TYPE) && 
          ((u1SubType == ETS_TC_SUPP_TLV_SUB_TYPE) || 
           (u1SubType == ETS_CONF_TLV_SUB_TYPE) || 
           (u1SubType == ETS_RECO_TLV_SUB_TYPE)) && 
           (MEMCMP (pau1OUI, gau1DcbxOUI, DCBX_MAX_OUI_LEN) == 0)) 
     { 
           return OSIX_TRUE; 
     } 
     if ((u2TlvType == PFC_TLV_TYPE) && 
          (u1SubType == PFC_TLV_SUBTYPE) && 
          (MEMCMP (pau1OUI, gau1DcbxOUI, DCBX_MAX_OUI_LEN) == 0)) 
     { 
          return OSIX_TRUE; 
     } 
     if ((u2TlvType == APP_PRI_TLV_TYPE) && 
         (u1SubType == APP_PRI_TLV_SUBTYPE) && 
         (MEMCMP (pau1OUI, gau1DcbxOUI, DCBX_MAX_OUI_LEN) == 0)) 
     { 
          return OSIX_TRUE; 
     } 
     if ((u2TlvType == CEE_TLV_TYPE) && 
         (u1SubType == CEE_TLV_SUBTYPE) && 
         (MEMCMP (pau1OUI, gau1CEEOUI, DCBX_MAX_OUI_LEN) == 0)) 
     { 
          return OSIX_TRUE; 
     } 
     return OSIX_FALSE; 
} 

/***************************************************************************
 *  FUNCTION NAME : DcbxApiModuleStart
 * 
 *  DESCRIPTION   : This API is used to Start the DCBX module from the 
 *                  RM Module during connect lost and restoration 
 *                  scenario. 
 * 
 *  INPUT         : None
 *  
 *  OUTPUT        : None
 * 
 *  RETURNS       : OSIX_SUCCESS/OSIX_FAILURE
 * 
 * **************************************************************************/
PUBLIC INT4
DcbxApiModuleStart (VOID)
{
    DcbxLock ();
    /* Start the DCBX Main Module */
    if (DcbxMainModuleStart () == OSIX_FAILURE)
    {
        DCBX_TRC (DCBX_FAILURE_TRC,
                  "DcbxApiModuleStart: DCBX Module Start FAILED !!!\r\n");
        DcbxUnLock ();
        return (OSIX_FAILURE);
    }

    /* Start the ETS Module which will initailise the necessary memory 
     * and intialise the data strucutres */
    if (ETSMainModuleStart () == OSIX_FAILURE)
    {
        DCBX_TRC (DCBX_FAILURE_TRC,
                  "DcbxApiModuleStart: ETS Module Start FAILED !!!\r\n");
        DcbxUnLock ();
        return (OSIX_FAILURE);
    }

    /* Start the PFC Module which will initailise the necessary memory 
     * and intialise the data strucutres */
    if (PFCMainModuleStart () == OSIX_FAILURE)
    {
        DCBX_TRC (DCBX_FAILURE_TRC,
                  "DcbxApiModuleStart: PFC Module Start FAILED !!!\r\n");
        DcbxUnLock ();
        return (OSIX_FAILURE);
    }
    /* Register with RM */
    if (DcbxRedInitGlobalInfo () == OSIX_FAILURE)
    {
        DCBX_TRC (DCBX_RED_TRC | DCBX_FAILURE_TRC,
                  "DcbxApiModuleStart :RM Registration Failed!!! \r\n");
        DcbxUnLock ();
        return (OSIX_FAILURE);
    }
    DcbxUnLock ();
    return OSIX_SUCCESS;
}

/***************************************************************************
 *  FUNCTION NAME : DcbxApiModuleShutDown
 * 
 *  DESCRIPTION   : This API is used to Shutdown the DCBX module from the 
 *                  RM Module during connect lost and restoration 
 *                  scenario. 
 * 
 *  INPUT         : None
 *  
 *  OUTPUT        : None
 * 
 *  RETURNS       : None
 * 
 * **************************************************************************/
PUBLIC VOID
DcbxApiModuleShutDown (VOID)
{
    DcbxLock ();
    /* De-Register from RM */
    DcbxRedDeInitGlobalInfo ();
    /*Shutdown the DCBX Main Module */
    DcbxMainModuleShutDown ();
    /* Shutdown the ETS Module */
    ETSMainModuleShutDown ();
    /* Shutdown the PFC Module */
    PFCMainModuleShutDown ();
    DcbxUnLock ();
    return;
}

/***************************************************************************
 *  FUNCTION NAME :  DcbxApiIsDcbxStarted 
 * 
 *  DESCRIPTION   : This API is used to check whether the DCBX module 
 *                  is enabled. 
 * 
 *  INPUT         : None
 *  
 *  OUTPUT        : None
 * 
 *  RETURNS       : OSIX_SUCCESS/OSIX_FAILURE
 * 
 * **************************************************************************/
PUBLIC UINT1 DcbxApiIsDcbxEnabled (VOID)
{
    if (gDcbxGlobalInfo.u1DCBXSystemCtrl == DCBX_START &&
        gPFCGlobalInfo.u1PFCModStatus == PFC_ENABLED  &&
        gAppPriGlobalInfo.u1AppPriModStatus == APP_PRI_ENABLED)
    {
    	return OSIX_SUCCESS;
    }
    else
    {
        return OSIX_FAILURE;
    }

}
/***************************************************************************
 *  FUNCTION NAME : DcbxApiIsDcbxOperStateUp 
 * 
 *  DESCRIPTION   : This API is used to check whether the Dcbx oper state
 *                  is up for the given interface index 
 * 
 *  INPUT         : Interface Index
 *  
 *  OUTPUT        : None
 * 
 *  RETURNS       : OSIX_SUCCESS/OSIX_FAILURE
 * 
 * **************************************************************************/
PUBLIC UINT1 DcbxApiIsDcbxOperStateUp (UINT4 u4IfIndex)
{
    DcbxLock ();

    tPFCPortEntry      *pPFCPortEntry = NULL;
    tAppPriPortEntry   *pAppPriPortEntry = NULL;

    /* Retrieve the PFC Port Entry from the given interface index*/
    pPFCPortEntry = PFCUtlGetPortEntry (u4IfIndex);
    if (pPFCPortEntry == NULL)
    {
        DCBX_TRC_ARG1 (DCBX_FAILURE_TRC, "In %s : PFCUtlGetPortEntry () "
                       "Returns FAILURE. \r\n", __FUNCTION__);
        DcbxUnLock ();
        return OSIX_FAILURE;
    }
    pAppPriPortEntry = AppPriUtlGetPortEntry (u4IfIndex);
    if (pAppPriPortEntry == NULL)
    {
        DCBX_TRC_ARG1 (DCBX_FAILURE_TRC, "In %s : AppPriUtlGetPortEntry() "
                       "Returns FAILURE. \r\n", __FUNCTION__);
        DcbxUnLock ();
        return OSIX_FAILURE;
    }
    if ((pPFCPortEntry->u1PFCDcbxOperState != PFC_OPER_OFF) &&
        (pAppPriPortEntry->u1AppPriDcbxOperState != APP_PRI_OPER_OFF))
    {
        /* If both the PFC and Application priority oper state are not off,
         * then return SUCCESS, else return FAILURE*/
        DcbxUnLock ();
        return OSIX_SUCCESS;
    }
    DcbxUnLock ();
    return OSIX_FAILURE;
}
/***************************************************************************
 *  FUNCTION NAME : DcbxApiIsDcbxAdminStateUp 
 * 
 *  DESCRIPTION   : This API is used to check whether the Dcbx admin state
 *                  is up for the given interface index 
 * 
 *  INPUT         : Interface Index
 *  
 *  OUTPUT        : None
 * 
 *  RETURNS       : OSIX_SUCCESS/OSIX_FAILURE
 * 
 * **************************************************************************/
PUBLIC UINT1 DcbxApiIsDcbxAdminStateUp (UINT4 u4IfIndex)
{
    DcbxLock ();
    tDcbxPortEntry     *pDcbxPortEntry = NULL;

    pDcbxPortEntry = DcbxUtlGetPortEntry ((UINT4) u4IfIndex);
    if (pDcbxPortEntry == NULL)
    {
        DCBX_TRC_ARG1 (DCBX_FAILURE_TRC, "In %s : DcbxUtlGetPortEntry () "
                "Returns FAILURE. \r\n", __FUNCTION__);
        DcbxUnLock ();
        return OSIX_FAILURE;
    }
    if (pDcbxPortEntry->u1DcbxAdminStatus == DCBX_ENABLED)
    {
        /* If the DCBX admin state is enabled 
         * then return SUCCESS, else return FAILURE*/
        DcbxUnLock ();
        return OSIX_SUCCESS;
    }
    DcbxUnLock ();
    return OSIX_FAILURE;
}

/*************************************************************************** 
 *  FUNCTION NAME : DcbxApiVerifyTlvIsCee
 * 
 *  DESCRIPTION   : This function is used to verify received TLV is CEE TLV 
 * 
 *  INPUT         : au1OUI    - OUI 
 * 
 *  OUTPUT        : None 
 * 
 *  RETURNS       : OSIX_TRUE/OSIX_FALSE 
 * 
 * **************************************************************************/ 
PUBLIC INT4 
DcbxApiVerifyTlvIsCee (UINT1 *pau1OUI, UINT4 u4IfIndex) 
{ 
    tDcbxPortEntry     *pDcbxPortEntry = NULL;
    
    pDcbxPortEntry = DcbxUtlGetPortEntry (u4IfIndex);
    if (pDcbxPortEntry != NULL)
    {
        if (DCBX_MODE_AUTO == pDcbxPortEntry->u1DcbxMode)
        {
            if (MEMCMP (pau1OUI, gau1CEEOUI, DCBX_MAX_OUI_LEN) == 0) 
            { 
                return OSIX_TRUE; 
            }
        }
    }
    return OSIX_FALSE; 
} 

/*************************************************************************** 
 *  FUNCTION NAME : DcbxApiVerifyTlvIsIeee
 * 
 *  DESCRIPTION   : This function is used to verify received TLV is IEEE TLV 
 * 
 *  INPUT         : au1OUI    - OUI 
 * 
 *  OUTPUT        : None 
 * 
 *  RETURNS       : OSIX_TRUE/OSIX_FALSE 
 * 
 * **************************************************************************/ 
PUBLIC INT4 
DcbxApiVerifyTlvIsIeee (UINT1 *pau1OUI, UINT4 u4IfIndex) 
{ 
    tDcbxPortEntry     *pDcbxPortEntry = NULL;

    pDcbxPortEntry = DcbxUtlGetPortEntry (u4IfIndex);
    if (pDcbxPortEntry != NULL)
    {
        if (DCBX_MODE_AUTO == pDcbxPortEntry->u1DcbxMode)
        {
            if (MEMCMP (pau1OUI, gau1DcbxOUI, DCBX_MAX_OUI_LEN) == 0) 
            { 
                return OSIX_TRUE; 
            } 
        }
    }
    return OSIX_FALSE; 
} 
