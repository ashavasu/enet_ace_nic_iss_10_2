/*************************************************************************
 * Copyright (C) 2010 Aricent Inc . All Rights Reserved
 * $Id: ceetlv.c,v 1.7 2017/08/24 11:59:43 siva Exp $
 * Description: This file contains CEE TLV related functions.
**************************************************************************/
#include "dcbxinc.h"
#include "ceedefn.h"
#include "dcbxglob.h"

/***************************************************************************
 *  FUNCTION NAME : CEEFormAndSendTLV 
 * 
 *  DESCRIPTION   : This function is used to from and send CEE TLV.
 * 
 *  INPUT         : u4IfIndex - Interface
 *                  u1MsgType - MsgType to be posted to DCBx
 *                  u1DeRegFeatType - The feature to be excluded in TLV formation
 *  OUTPUT        : None
 * 
 *  RETURNS       : None
 * 
 * **************************************************************************/
VOID
CEEFormAndSendTLV (UINT4 u4IfIndex, UINT1 u1MsgType, UINT1 u1DeRegFeatType)
{
    tDcbxAppRegInfo     DcbxAppPortMsg;
    tDcbxPortEntry     *pDcbxPortEntry = NULL;
    tPFCPortEntry      *pPFCEntry = NULL;
    tETSPortEntry      *pPGEntry = NULL;
    tAppPriPortEntry   *pAppPriEntry = NULL;
    tCEECtrlEntry      *pCtrlEntry;
    UINT2               u2TlvLen = DCBX_ZERO;
    UINT2               u2TlvHeader = 0;
    UINT2               u2Pktlen = 0;
    UINT1               au1CEETlv[CEE_TLV_MAX_LEN] = { 0 };
    UINT1               u1AdminMode = DCBX_ADM_MODE_OFF;
    UINT1               u1TxStatus = DCBX_DISABLED;
    UINT1              *pu1Buf = NULL;
    UINT1              *pu1BaseOffset = NULL;
    UINT1              *pu1LenOffset = NULL;
    UINT1               u1OperVer = 0;
    UINT1               u1MaxVer = 0;
    UINT1               u1ApTblLength = 0;
    BOOL1               b1PFCTLVTx = OSIX_FALSE;
    BOOL1               b1ETSTLVTx = OSIX_FALSE;
    BOOL1               b1AppPriTLVTx = OSIX_FALSE;

    DCBX_TRC_ARG2 (DCBX_MGMT_TRC, "In %s : Constructing CEE TLV for the port %d"
                   "\r\n", __func__, u4IfIndex);

    MEMSET (&DcbxAppPortMsg, DCBX_ZERO, sizeof (tDcbxAppRegInfo));

    pu1Buf = au1CEETlv;
    pu1LenOffset = pu1Buf;        /* Pointer used to fill length at a later point */
    pu1BaseOffset = pu1Buf;        /* Pointer to hold the base address */

    pDcbxPortEntry = DcbxUtlGetPortEntry (u4IfIndex);
    if (pDcbxPortEntry == NULL)
    {
        DCBX_TRC_ARG1 (DCBX_FAILURE_TRC, "In %s : DcbxUtlGetPortEntry() "
                       "Returns FAILURE. \r\n", __FUNCTION__);
        return;
    }

    pCtrlEntry = CEEUtlGetCtrlEntry (u4IfIndex);
    if (pCtrlEntry == NULL)
    {
        DCBX_TRC_ARG2 (DCBX_FAILURE_TRC, "%s : "
                       "pCtrlEntry is null. Line = %d \r\n", __func__,
                       __LINE__);
        return;
    }

    if (u1MsgType == DCBX_LLDP_CEE_ACK)
    {
        if (pCtrlEntry->u4CtrlSeqNo == DCBX_ZERO)
        {
            pCtrlEntry->u4CtrlSeqNo++;
        }
        pCtrlEntry->u4CtrlSyncNo = pCtrlEntry->u4CtrlSeqNo;
        u1MsgType = DCBX_LLDP_PORT_UPDATE;
        DCBX_TRC_ARG3 (DCBX_TLV_TRC, "In %s : "
                       "Info: DCBX port: %d send Ack to peer for RcvdSeqNo: %d \r\n",
                       __func__, u4IfIndex, pCtrlEntry->u4CtrlRcvdSeqNo);
    }
    else
    {
        if (pCtrlEntry->u4CtrlSeqNo == pCtrlEntry->u4CtrlRcvdAckNo)
        {
            pCtrlEntry->u4CtrlSeqNo++;
            pCtrlEntry->u4CtrlSyncNo = pCtrlEntry->u4CtrlSeqNo;
            DCBX_TRC_ARG3 (DCBX_TLV_TRC, "In %s : "
                           "Info: DCBX  port: %d send "
                           "update TLV with SeqNo: %d \r\n", __func__,
                           u4IfIndex, pCtrlEntry->u4CtrlSeqNo);
        }
        else
        {
            pCtrlEntry->u4CtrlSyncNo++;
            DCBX_TRC_ARG2 (DCBX_TLV_TRC,
                           "Info: DCBX Peer not detected for seqNo %d "
                           "on port %d\r\n", pCtrlEntry->u4CtrlSeqNo,
                           u4IfIndex);
            return;
        }
    }

    u1OperVer = pDcbxPortEntry->u1OperVersion;
    u1MaxVer = pDcbxPortEntry->u1MaxVersion;

    /* Construct Type(127), Length, OUI(0x001b21) and Sub-Type (2) */
    CEEConstructTLVHeader (pu1Buf);
    pu1Buf += CEE_TLV_HDR_LEN;

    /* Construct Control TLV */
    if (CEEFormControlTLV (pu1Buf, u4IfIndex, u1OperVer, u1MaxVer) !=
        DCBX_SUCCESS)
    {
        DCBX_TRC_ARG2 (DCBX_CRITICAL_TRC, "%s: Failed to construct "
                       "Control TLV for the port %d. \r\n", __func__,
                       u4IfIndex);
        return;
    }

    pu1Buf += CEE_CONTROL_TLV_LEN;

    pPFCEntry = PFCUtlGetPortEntry (u4IfIndex);
    if (pPFCEntry != NULL)
    {
        u1AdminMode = pPFCEntry->u1PFCAdminMode;
        u1TxStatus = pPFCEntry->u1PFCTxStatus;
    }

    if ((gPFCGlobalInfo.u1PFCModStatus == PFC_ENABLED) &&
        (u1TxStatus == PFC_ENABLED) &&
        (u1AdminMode == PFC_PORT_MODE_AUTO) &&
        (u1DeRegFeatType != CEE_PFC_TLV_TYPE))
    {
        /* Construct PFC TLV */
        CEEFormPFCTLV (pu1Buf, u4IfIndex, u1OperVer, u1MaxVer);

        pu1Buf += CEE_PFC_TLV_LEN;
        b1PFCTLVTx = OSIX_TRUE;
    }
    else
    {
        DCBX_TRC_ARG6 (DCBX_TLV_TRC, "%s: "
                       "PFC TLV not formed for port %d. "
                       "PFCModStatus = %d TxStatus = %d "
                       "AdminMode = %d u1DeRegFeatType = %d \r\n", __func__,
                       u4IfIndex, gPFCGlobalInfo.u1PFCModStatus, u1TxStatus,
                       u1AdminMode, u1DeRegFeatType);
    }

    u1AdminMode = DCBX_ADM_MODE_OFF;
    u1TxStatus = DCBX_DISABLED;
    pPGEntry = ETSUtlGetPortEntry (u4IfIndex);
    if (pPGEntry != NULL)
    {
        u1AdminMode = pPGEntry->u1ETSAdminMode;
        u1TxStatus = pPGEntry->u1ETSConfigTxStatus;
    }

    if ((gETSGlobalInfo.u1ETSModStatus == ETS_ENABLED) &&
        (u1TxStatus == ETS_ENABLED) &&
        (u1AdminMode == ETS_PORT_MODE_AUTO) &&
        (u1DeRegFeatType != CEE_ETS_TLV_TYPE))
    {
        /* Construct Priority Grouping TLV */
        CEEFormPriGrpTlv (pu1Buf, u4IfIndex, u1OperVer, u1MaxVer);

        pu1Buf += CEE_PG_TLV_LEN;
        b1ETSTLVTx = OSIX_TRUE;
    }
    else
    {
        DCBX_TRC_ARG6 (DCBX_TLV_TRC, "%s: "
                       "ETS TLV not formed for port %d. "
                       "PFCModStatus = %d TxStatus = %d "
                       "AdminMode = %d u1DeRegFeatType = %d \r\n", __func__,
                       u4IfIndex, gETSGlobalInfo.u1ETSModStatus, u1TxStatus,
                       u1AdminMode, u1DeRegFeatType);
    }

    u1AdminMode = DCBX_ADM_MODE_OFF;
    u1TxStatus = DCBX_DISABLED;
    pAppPriEntry = AppPriUtlGetPortEntry (u4IfIndex);
    if (pAppPriEntry != NULL)
    {
        u1AdminMode = pAppPriEntry->u1AppPriAdminMode;
        u1TxStatus = pAppPriEntry->u1AppPriTxStatus;
    }
    if ((gAppPriGlobalInfo.u1AppPriModStatus == APP_PRI_ENABLED) &&
        (u1TxStatus == APP_PRI_ENABLED) &&
        (u1AdminMode == APP_PRI_PORT_MODE_AUTO) &&
        (u1DeRegFeatType != CEE_APP_PRI_TLV_TYPE))
    {
        /* Construct Application Priority TLV */
        CEEFormAppPriTLV (pu1Buf, u4IfIndex, u1OperVer, u1MaxVer,
                          &u1ApTblLength);

        pu1Buf += (u1ApTblLength + CEE_TLV_TYPE_LEN);
        b1AppPriTLVTx = OSIX_TRUE;
    }
    else
    {
        DCBX_TRC_ARG6 (DCBX_TLV_TRC, "%s: "
                       "AppPri TLV not formed for port %d. "
                       "AppPriModStatus = %d TxStatus = %d "
                       "AdminMode = %d u1DeRegFeatType = %d \r\n", __func__,
                       u4IfIndex, gAppPriGlobalInfo.u1AppPriModStatus,
                       u1TxStatus, u1AdminMode, u1DeRegFeatType);
    }

    /* This callback will be used for processing the TLV when a PDU is Rxd */
    DcbxAppPortMsg.pApplCallBackFn = CEEApiTlvCallBackFn;

    /* Get the Application ID to be filled in the DCBX Appl port structure */
    CEE_GET_APPL_ID (DcbxAppPortMsg.DcbxAppId);

    u2TlvLen = (UINT2) (pu1Buf - pu1LenOffset);

    /*Form TLV Header */
    DCBX_PUT_HEADER_LENGTH ((UINT2) CEE_TLV_TYPE,
                            (UINT2) (u2TlvLen - (UINT2) CEE_TLV_TYPE_LEN),
                            &u2TlvHeader);
    DCBX_PUT_2BYTE (pu1LenOffset, u2TlvHeader);

    /* FIll the total length */
    DcbxAppPortMsg.u2TlvLen = u2TlvLen;
    DcbxAppPortMsg.pu1ApplTlv = pu1BaseOffset;

    /* Packet Dump to Console */
    if (gDcbxGlobalInfo.u4DCBXTrcFlag & DCBX_TLV_TRC)
    {
        u2Pktlen = (UINT2) (DcbxAppPortMsg.u2TlvLen + CEE_TLV_TYPE_LEN);    /* To print TLV type and Length */
        DcbxPktDump (pu1BaseOffset, u2Pktlen, DCBX_TX);
    }

    DCBX_TRC_ARG2 (DCBX_TLV_TRC, "In CEEFormAndSendTLV"
                   " Posting CEE TLV to DCBX with [TLV Length = %d] for the port %d"
                   "\r\n", u2TlvLen, u4IfIndex);

    CEEPostMsgToDCBX (u1MsgType, u4IfIndex, &DcbxAppPortMsg);

    DCBX_TRC_ARG4 (DCBX_TLV_TRC,
                   "CEE TLV tx'd with [PFC = %d ETS = %d AppPri = %d] "
                   "on port %d\r\n", b1PFCTLVTx, b1ETSTLVTx, b1AppPriTLVTx,
                   u4IfIndex);

    return;
}

/***************************************************************************
 *  FUNCTION NAME : CEEConstructTLVHeader
 *
 *  DESCRIPTION   : This function is used to form the CEE TLV
 *                  header.
 *
 *  INPUT         : **pPu1Buf - Pointer to TLV Buffer  
 *
 *  OUTPUT        : None
 *
 *  RETURNS       : None
 *
 * **************************************************************************/
VOID
CEEConstructTLVHeader (UINT1 *pPu1Buf)
{
    UINT2               u2TlvHeader = DCBX_ZERO;
    UINT2               u2TlvLen = DCBX_ZERO;

    /*Form TLV Header */
    DCBX_PUT_HEADER_LENGTH ((UINT2) CEE_TLV_TYPE, u2TlvLen, &u2TlvHeader);
    /* The actual length will be calculated and replaced at the end */
    DCBX_PUT_2BYTE (pPu1Buf, u2TlvHeader);
    DCBX_PUT_OUI (pPu1Buf, gau1CEEOUI);
    DCBX_PUT_1BYTE (pPu1Buf, (UINT1) CEE_TLV_SUBTYPE);
    DCBX_TRC_ARG1 (DCBX_TLV_TRC, "In %s : "
                   "CEE TLV Header Contruction Successful!!!\r\n", __func__);
    return;
}

/***************************************************************************
 *  FUNCTION NAME : CEEFormControlTLV 
 * 
 *  DESCRIPTION   : This function is used to form Control TLV.
 * 
 *  INPUT         : **pPu1Buf - Pointer to the buffer
 *                  u4IfIndex - Interface Index
 *                  u1OperVer - Operating version
 *                  u1MaxVer  - Max version 
 * 
 *  OUTPUT        : None
 * 
 *  RETURNS       : None
 * 
 * **************************************************************************/
UINT1
CEEFormControlTLV (UINT1 *pPu1Buf, UINT4 u4IfIndex, UINT1 u1OperVer,
                   UINT1 u1MaxVer)
{
    tCEECtrlEntry      *pCtrlEntry;
    UINT2               u2TlvHeader = DCBX_ZERO;
    UINT4               u4SeqNo = DCBX_ZERO;
    UINT4               u4AckNo = DCBX_ZERO;

    DCBX_TRC_ARG2 (DCBX_TLV_TRC, "In %s : "
                   "Constructing CEE Control TLV for the port %d\r\n", __func__,
                   u4IfIndex);

    pCtrlEntry = CEEUtlGetCtrlEntry (u4IfIndex);
    if (pCtrlEntry == NULL)
    {
        DCBX_TRC_ARG2 (DCBX_FAILURE_TRC, "In %s : "
                       "pCtrlEntry is null. Line = %d \r\n", __func__,
                       __LINE__);
        return DCBX_FAILURE;
    }

    /*
       DCBx CONTROL TLV
       _________________________________________________________________________________________
       |          |            |           |           |                  |                    |
       |          |            | Operating | Max       |                  |                    |
       | Type = 1 | Length= 10 | Version   | Version   |    Seq No        |      Ack No        |
       |__________|____________|___________|___________|__________________|____________________|
       7 bits       9 bits    1 octet      1 octet       4 octet               4 octet       

     */

    /*Form TLV Header */
    DCBX_PUT_HEADER_LENGTH ((UINT2) CEE_CONTROL_TLV_TYPE,
                            (UINT2) CEE_CONTROL_TLV_PAYLOAD_LEN, &u2TlvHeader);

    /* Fill the TLV Type and Length in to the buffer. */
    DCBX_PUT_2BYTE (pPu1Buf, u2TlvHeader);
    /* Fill the Operating version */
    DCBX_PUT_1BYTE (pPu1Buf, u1OperVer);
    /* Fill the Max version */
    DCBX_PUT_1BYTE (pPu1Buf, u1MaxVer);

    u4SeqNo = pCtrlEntry->u4CtrlSeqNo;
    u4AckNo = pCtrlEntry->u4CtrlAckNo;

    DCBX_PUT_4BYTE (pPu1Buf, u4SeqNo);
    DCBX_PUT_4BYTE (pPu1Buf, u4AckNo);

    /* Increment the TLV counter */
    pCtrlEntry->u4CtrlTxTLVCount++;

    /* Send Counter update Info to Stand by node */
    DcbxCeeRedSendDynamicCtrlCounterInfo (pCtrlEntry,
                                          DCBX_CEE_RED_CTRL_TX_COUNTER);

    DCBX_TRC_ARG3 (DCBX_TLV_TRC, "In %s : CEE Control TLV construction "
                   "Successful!!! for the port %d updated TxTlvCount[%d].\r\n",
                   __func__, u4IfIndex, pCtrlEntry->u4CtrlTxTLVCount);

    return DCBX_SUCCESS;
}

/***************************************************************************
 *  FUNCTION NAME : CEEFormPFCTLV
 * 
 *  DESCRIPTION   : This function is used to form PFC TLV.
 * 
 *  INPUT         : pPu1Buf - Pointer to the buffer
 *                  u4IfIndex - Interface 
 *                  u1OperVer - Operating version
 *                  u1MaxVer - Max Version
 * 
 *  OUTPUT        : None
 * 
 *  RETURNS       : None
 * 
 * **************************************************************************/
PUBLIC VOID
CEEFormPFCTLV (UINT1 *pPu1Buf, UINT4 u4IfIndex, UINT1 u1OperVer, UINT1 u1MaxVer)
{
    tPFCPortEntry      *pPFCEntry = NULL;
    tDcbxPortEntry     *pDcbxPortEntry = NULL;
    UINT2               u2TlvTypeLen = DCBX_ZERO;
    UINT1               u1Vector = DCBX_ZERO;

    DCBX_TRC_ARG2 (INIT_SHUT_TRC | DCBX_TLV_TRC,
                   "In %s : Constructing CEE PFC TLV " " for the port[%d]\r\n",
                   __func__, u4IfIndex);

    pDcbxPortEntry = DcbxUtlGetPortEntry (u4IfIndex);
    if (pDcbxPortEntry == NULL)
    {
        DCBX_TRC_ARG2 (DCBX_CRITICAL_TRC | DCBX_TLV_TRC, "In %s: "
                       "DCBX not enabled on this Port %d \r\n",
                       __func__, u4IfIndex);
        return;
    }

    pPFCEntry = PFCUtlGetPortEntry (u4IfIndex);
    if (pPFCEntry == NULL)
    {
        DCBX_TRC_ARG2 (DCBX_FAILURE_TRC | DCBX_MGMT_TRC, "In %s : "
                       "PFC not enabled on this Port %d \r\n",
                       __FUNCTION__, u4IfIndex);
        return;
    }

    /*
     * PRIORITY-BASED FLOW CONTROL TLV
     ___________________________________________________________________________________________________________
     |          |            |           |           |  |  |   |        |          |             |             |   
     |          |            | Operating | Max       |  |  |   |  Re-   |          | PFC Enabled | Max TCs W/  |
     | Type = 3 | Length= 6  | Version   | Version   |En|W.|Err| served | SubType  |   Table     | PFC Support |
     |__________|____________|___________|___________|__|__|___|________|__________|_____________|_____________|
     7 bits       9 bits    1 octet      1 octet   1  1   1   5 bits   1 octect    1 octet        1 octect
     bit bit bit
     */

    /* This macro will fill TLV type into first 7 bits of variable + 
     * Length into next 9 bits of the variable */
    DCBX_PUT_HEADER_LENGTH ((UINT2) CEE_PFC_TLV_TYPE,
                            (UINT2) CEE_PFC_TLV_PAYLOAD_LEN, &u2TlvTypeLen);

    /* Fill the TLV Type and Length in to the buffer. */
    DCBX_PUT_2BYTE (pPu1Buf, u2TlvTypeLen);
    /* Fill the Operating version */
    DCBX_PUT_1BYTE (pPu1Buf, u1OperVer);
    /* Fill the Max version */
    DCBX_PUT_1BYTE (pPu1Buf, u1MaxVer);

    if (pDcbxPortEntry->u1PFCAdminStatus == PFC_ENABLED)
    {
        u1Vector = CEE_ENABLE_BIT_MASK;
    }
    if (pPFCEntry->PFCAdmPortInfo.u1PFCAdmWilling == PFC_ENABLED)
    {
        u1Vector = u1Vector | CEE_WILLING_BIT_MASK;
    }
    if (pPFCEntry->bPFCError == DCBX_ENABLED)
    {
        u1Vector = u1Vector | CEE_ERROR_BIT_MASK;
    }

    /* Fill the vector */
    DCBX_PUT_1BYTE (pPu1Buf, u1Vector);
    /* Fill the sub type field in to buffer */
    DCBX_PUT_1BYTE (pPu1Buf, CEE_FEATURE_SUB_TLV_SUBTYPE);

    /* PFC Enable vector */
    DCBX_PUT_1BYTE (pPu1Buf, pPFCEntry->PFCAdmPortInfo.u1PFCAdmStatus);

    DCBX_PUT_1BYTE (pPu1Buf, pPFCEntry->PFCAdmPortInfo.u1PFCAdmCap);

    /* Increment the counter */
    pPFCEntry->u4PFCTxTLVCount++;

    /* Send Counter update Info to Stand by node */
    DcbxRedSendDynamicPfcCounterInfo (pPFCEntry, DCBX_RED_PFC_TX_COUNTER);

    DCBX_TRC_ARG3 (DCBX_TLV_TRC,
                   "In %s : CEE PFC TLV constructed successfully!!!"
                   " with TlvCount[%d] for the port %d.\r\n", __func__,
                   pPFCEntry->u4PFCTxTLVCount, u4IfIndex);
    return;
}

/***************************************************************************
 *  FUNCTION NAME : CEEFormPriGrpTlv
 *
 *  DESCRIPTION   : This function is used to form Priority Group TLV
 *
 *  INPUT         : pPu1Buf - Pointer to the buffer
 *                  u4IfIndex - Interface 
 *                  u1OperVer - Operating version
 *                  u1MaxVer - Max Version
 *
 *  OUTPUT        : None
 *
 *  RETURNS       : None
 *
 * **************************************************************************/
VOID
CEEFormPriGrpTlv (UINT1 *pPu1Buf, UINT4 u4IfIndex, UINT1 u1OperVer,
                  UINT1 u1MaxVer)
{
    tETSPortEntry      *pPGEntry = NULL;
    tDcbxPortEntry     *pDcbxPortEntry = NULL;
    UINT4               u4TCGSupp = DCBX_ZERO;
    UINT2               u2TlvTypeLen = DCBX_ZERO;
    UINT1               u1Vector = DCBX_ZERO;

    DCBX_TRC_ARG2 (DCBX_TLV_TRC, "In %s : Constructing CEE PriGrouping TLV "
                   "for the Port[%d]\r\n", __func__, u4IfIndex);

    pDcbxPortEntry = DcbxUtlGetPortEntry (u4IfIndex);
    if (pDcbxPortEntry == NULL)
    {
        DCBX_TRC_ARG2 (DCBX_CRITICAL_TRC | DCBX_TLV_TRC, "%s : "
                       "DCBX not enabled on this port %d \r\n",
                       __func__, u4IfIndex);
        return;
    }

    pPGEntry = ETSUtlGetPortEntry (u4IfIndex);
    if (pPGEntry == NULL)
    {
        DCBX_TRC_ARG2 (DCBX_CRITICAL_TRC | DCBX_TLV_TRC, "%s: "
                       "ETS not enabled on this port. portEntry not found for Port Id %d\r\n",
                       __FUNCTION__, u4IfIndex);
        return;
    }

    /*
       PRIORITY-BASED FLOW CONTROL TLV
       _______________________________________________________________________________________________________________________________________
       |          |            |           |           |  |  |   |      |          |                      |                         |        |  
       |          |            | Operating | Max       |  |  |   | Re-  |          | Priority grouping to |Priority grouping Percent|Max Num.|
       | Type = 2 | Length= 17 | Version   | Version   |En|W.|Err|served| SubType  |        priority      |       Allocation        | of TCs |
       |__________|____________|___________|___________|__|__|___|______|__________|______________________|_________________________|________|
       7 bits       9 bits    1 octet      1 octet   1  1   1    5     1 octect        4 octets                8 octects          1 octet
       bit bit bit bits 
     */

    /* This macro will fill TLV type into first 7 bits of variable + 
     * Length into next 9 bits of the variable */
    DCBX_PUT_HEADER_LENGTH ((UINT2) CEE_PG_TLV_TYPE,
                            (UINT2) CEE_PG_TLV_PAYLOAD_LEN, &u2TlvTypeLen);

    /* Fill the TLV Type and Length in to the buffer. */
    DCBX_PUT_2BYTE (pPu1Buf, u2TlvTypeLen);
    /* Fill the Operating version */
    DCBX_PUT_1BYTE (pPu1Buf, u1OperVer);
    /* Fill the Max version */
    DCBX_PUT_1BYTE (pPu1Buf, u1MaxVer);

    if (pDcbxPortEntry->u1ETSAdminStatus == DCBX_ENABLED)
    {
        u1Vector = CEE_ENABLE_BIT_MASK;
    }
    if (pPGEntry->ETSAdmPortInfo.u1ETSAdmWilling == DCBX_ENABLED)
    {
        u1Vector = u1Vector | CEE_WILLING_BIT_MASK;
    }
    if (pPGEntry->bETSError == DCBX_ENABLED)
    {
        u1Vector = u1Vector | CEE_ERROR_BIT_MASK;
    }

    /* Fill the vector */
    DCBX_PUT_1BYTE (pPu1Buf, u1Vector);
    /* Fill the sub type field in to buffer */
    DCBX_PUT_1BYTE (pPu1Buf, CEE_FEATURE_SUB_TLV_SUBTYPE);

    /* Fill the Priority group to priority paramters for the next 4 octets */
    ETS_FILL_PRI_MAPPING (pPu1Buf, pPGEntry->ETSAdmPortInfo.au1ETSAdmTCGID);

    /* Fill the Priority Group Percent Allocation for the next 8 octets */
    ETS_FILL_TCG_BW (pPu1Buf, pPGEntry->ETSAdmPortInfo.au1ETSAdmBW);

    /* Fill the Max Num of TCs for the next 1 octets */
    QosApiGetMaxTCSupport (pPGEntry->u4IfIndex, &u4TCGSupp);
    DCBX_PUT_1BYTE (pPu1Buf, (UINT1) u4TCGSupp);

    /* Update TLV Tx counters */
    pPGEntry->u4ETSConfTxTLVCount++;

    /* Send Counter update Info to Stand by node */
    DcbxRedSendDynamicEtsCounterInfo (pPGEntry, DCBX_RED_ETS_CONF_TX_COUNTER);

    DCBX_TRC_ARG2 (DCBX_TLV_TRC, "In %s : CEE PriGrouping TLV constructed "
                   "successfully!!! for the port %d.\r\n", __func__, u4IfIndex);
    return;
}

/***************************************************************************
 *  FUNCTION NAME : CEEAppPriFormAndSendAppPriTLV 
 * 
 *  DESCRIPTION   : This function is used to form Application Priority
 *                  TLV. 
 * 
 *  INPUT         : pPu1TlvBuf - Pointer to the buffer 
 *                  u4IfIndex - Interface 
 *                  u1OperVer - Operating version
 *                  u1MaxVer - Max Version
 * 
 *  OUTPUT        : None
 * 
 *  RETURNS       : None
 * 
 * **************************************************************************/
VOID
CEEFormAppPriTLV (UINT1 *pPu1Buf, UINT4 u4IfIndex, UINT1 u1OperVer,
                  UINT1 u1MaxVer, UINT1 *pu1ApTblLen)
{
    tDcbxPortEntry     *pDcbxPortEntry = NULL;
    tAppPriPortEntry   *pAppPriEntry = NULL;
    tAppPriMappingEntry *pAppPriMappingEntry = NULL;
    UINT2               u2AppPriTblCount = DCBX_ZERO;
    UINT2               u2TlvTypeLen = DCBX_ZERO;
    UINT2               u2TlvLength = DCBX_ZERO;
    UINT2               u2Protocol = DCBX_ZERO;
    UINT1              *pu1LenOffset = NULL;
    UINT1               u1Vector = DCBX_ZERO;
    UINT1               u1UserPriMap = DCBX_ZERO;
    UINT1               u1Selector = DCBX_ZERO;
    UINT1               u1OUIValue = DCBX_ZERO;

    DCBX_TRC_ARG2 (DCBX_TLV_TRC, "In %s : Constructing CEE AppPri TLV "
                   "for the port %d \r\n", __func__, u4IfIndex);

    pDcbxPortEntry = DcbxUtlGetPortEntry (u4IfIndex);
    if (pDcbxPortEntry == NULL)
    {
        DCBX_TRC_ARG2 (DCBX_CRITICAL_TRC | DCBX_TLV_TRC, "%s : "
                       "DCBX is not enabled on this Port = %d\r\n",
                       __func__, u4IfIndex);
        return;
    }

    pAppPriEntry = AppPriUtlGetPortEntry (u4IfIndex);
    if (pAppPriEntry == NULL)
    {
        DCBX_TRC_ARG2 (DCBX_CRITICAL_TRC | DCBX_TLV_TRC, "%s : "
                       "AppPri not enabled on this port. PortEntry node not found for port: %d \r\n",
                       __func__, u4IfIndex);
        return;
    }

    /*
     * APPLICATION PRIORITY TLV HEADER
     _________________________________________________________________________________________________________
     |          |            |           |           |  |  |   |        |            |                       |    
     |          |            | Operating | Max       |  |  |   |        |            |                       |    
     | Type = 4 | Length= 16 | Version   | Version   |En|W.|Err|Reserved|Sub_Type = 0|   Application Table   |  
     |__________|____________|___________|___________|__|__|___|________|____________|_______________________|
     7 bits       9 bits    1 octet      1 octet   1  1   1   5 bits   1 octect      Multiple of 6 octets
     bit bit bit
     */

    /* Form the TLV Header with the Preformed TLV */
    pu1LenOffset = pPu1Buf;        /* To fill the exact TLV length at later point */

    /* Fill the TLV Type and Length in to the buffer. 
     * The exact value will be filled later. */
    DCBX_PUT_2BYTE (pPu1Buf, u2TlvTypeLen);
    /* Fill the Operating version */
    DCBX_PUT_1BYTE (pPu1Buf, u1OperVer);
    /* Fill the Max version */
    DCBX_PUT_1BYTE (pPu1Buf, u1MaxVer);

    /* This next 1 byte will be filled in the order
     *  ---------------------------
     *  | En | W | Err | Reserved |
     *  ---------------------------
     *    1    1    1     5
     *   bit  bit  bit   bits
     */

    /* Enable bit - if the DCB feature is enabled set it. */
    if (pDcbxPortEntry->u1AppPriAdminStatus == APP_PRI_ENABLED)
    {
        u1Vector = CEE_ENABLE_BIT_MASK;
    }
    /* Willing bit */
    if (pAppPriEntry->AppPriAdmPortInfo.u1AppPriAdmWilling == APP_PRI_ENABLED)
    {
        u1Vector = u1Vector | CEE_WILLING_BIT_MASK;
    }
    /* Error bit */
    if (pAppPriEntry->bAppPriError == DCBX_ENABLED)
    {
        u1Vector = u1Vector | CEE_ERROR_BIT_MASK;
    }

    /* Fill the vector */
    DCBX_PUT_1BYTE (pPu1Buf, u1Vector);
    /* Fill the sub type field in to buffer */
    DCBX_PUT_1BYTE (pPu1Buf, (UINT1) CEE_FEATURE_SUB_TLV_SUBTYPE);

    /*
       Application table:
       ____________________________________________________________
       | Application | Upper | Sel. |              | User Priority |
       | Protocol ID | OUI   | Field|   Lower OUI  |    Map        |
       |_____________|_______|______|______________|_______________|
       2 octets     6 bits   2       2 octets       1 octect
       bits
     */

    /* Filling the application priority table entries into buffer */
    for (u1Selector = DCBX_ONE; u1Selector <= APP_PRI_MAX_SELECTOR;
         u1Selector++)
    {
        /* Selector 2 = TCP and 3 = UDP for CEE is not valid */
        if ((u1Selector == DCBX_TWO) || (u1Selector == DCBX_THREE))
            continue;

        TMO_SLL_Scan (&(APP_PRI_ADM_TBL (pAppPriEntry, (u1Selector - 1))),
                      pAppPriMappingEntry, tAppPriMappingEntry *)
        {
            if (pAppPriMappingEntry->u1RowStatus == ACTIVE)
            {
                /* 2 bytes contain the protocol ID */
                u2Protocol = (UINT2) pAppPriMappingEntry->i4AppPriProtocol;

                DCBX_PUT_2BYTE (pPu1Buf, u2Protocol);

                /* Get the first index in the array which is 0x00 */
                u1OUIValue = gau1CEEOUI[0];
                if (u1Selector == CEE_APP_PRI_IEEE_ETHERTYPE_SEL)
                {
                    u1OUIValue = u1OUIValue | CEE_APP_PRI_ETHERTYPE_SEL;
                }
                else if (u1Selector == CEE_APP_PRI_IEEE_TCP_UDP_SEL)
                {
                    u1OUIValue = u1OUIValue | CEE_APP_PRI_TCP_UDP_SEL;
                }

                DCBX_PUT_1BYTE (pPu1Buf, u1OUIValue);

                /* Copy the 2nd and 3rd bytes to the buf */
                MEMCPY (pPu1Buf, (gau1CEEOUI + 1), DCBX_TWO);
                pPu1Buf += (UINT1) DCBX_TWO;
                u1UserPriMap = pAppPriMappingEntry->u1AppPriPriority;
                APP_PRI_GET_UPM_FRM_IEEE_TO_CEE (u1UserPriMap);
                DCBX_PUT_1BYTE (pPu1Buf, u1UserPriMap);
                u1UserPriMap = (UINT1) DCBX_ZERO;

                /* Calcuate the application priority table count */
                u2AppPriTblCount++;
            }
        }                        /* end of SLL SCAN */
    }                            /* end of for */

    u2TlvLength = (UINT2) ((CEE_APP_PRI_TLV_HDR_LEN) +
                           ((CEE_APP_PRI_TABLE_LEN) * (u2AppPriTblCount)));

    /* This macro will fill TLV type into first 7 bits of variable + 
     * Length into next 9 bits of the variable */
    DCBX_PUT_HEADER_LENGTH ((UINT2) CEE_APP_PRI_TLV_TYPE,
                            u2TlvLength, &u2TlvTypeLen);

    /* Fill the TLV Type and Length in to the buffer. */
    DCBX_PUT_2BYTE (pu1LenOffset, u2TlvTypeLen);

    *pu1ApTblLen = (UINT1) u2TlvLength;

    /* Update TLV Tx counters - Do we need this? */
    pAppPriEntry->u4AppPriTLVTxCount++;

    /* Send Counter update Info to Stand by node */
    DcbxRedSendDynamicAppPriCounterInfo (pAppPriEntry,
                                         DCBX_RED_APP_PRI_TX_COUNTER);
    DCBX_TRC_ARG2 (DCBX_TLV_TRC, "In %s : CEE AppPri TLV constructed "
                   "successfully!!! for the port %d.\r\n", __func__, u4IfIndex);
    return;
}

/***************************************************************************
 *  FUNCTION NAME : CEEPostMsgToDCBX 
 * 
 *  DESCRIPTION   : This utility function is used to post the TLV/Sem
 *                  Update message to DCBX.
 * 
 *  INPUT         : u1MsgType - Message type Reg/Update/Sem/DeReg.
 *                  u4IfIndex - Port Number.
 *                  pDcbxAppPortMsg - App Reg Info Structure
 * 
 *  OUTPUT        : None
 * 
 *  RETURNS       : None
 * 
 * **************************************************************************/
VOID
CEEPostMsgToDCBX (UINT1 u1MsgType, UINT4 u4IfIndex,
                  tDcbxAppRegInfo * pDcbxAppPortMsg)
{

    DCBX_TRC_ARG1 (DCBX_MGMT_TRC, "In %s : Posting Msg to DCBX.\r\n", __func__);

    switch (u1MsgType)
    {
        case DCBX_LLDP_PORT_UPDATE:
            /* Used to send the TLV updated information to the LLDP using 
             * DCBX function */
            if (DcbxApiApplPortUpdate (u4IfIndex, pDcbxAppPortMsg)
                != OSIX_SUCCESS)
            {
                DCBX_TRC_ARG1 (DCBX_CRITICAL_TRC | DCBX_FAILURE_TRC,
                               "In CEEPostMsgToDCBX: "
                               "DCBX TLV Update Failed !!!, for the port %d"
                               "\r\n", u4IfIndex);
            }
            break;

        case DCBX_LLDP_PORT_REG:
            /* Used to Register the application to the LLDP using 
             * DCBX function */
            if (DcbxApiApplPortReg (u4IfIndex, pDcbxAppPortMsg) != OSIX_SUCCESS)
            {
                DCBX_TRC_ARG1 (DCBX_CRITICAL_TRC | DCBX_FAILURE_TRC,
                               "In CEEPostMsgToDCBX: "
                               "DCBX Registration Failed with LLDP!!!, for the port %d"
                               "\r\n", u4IfIndex);
            }
            break;

        case DCBX_LLDP_PORT_DEREG:
            /* Used to De-Register the application from the LLDP using 
             * DCBX function */
            if (DcbxApiApplPortDeReg (u4IfIndex, pDcbxAppPortMsg)
                != OSIX_SUCCESS)
            {
                DCBX_TRC_ARG1 (DCBX_CRITICAL_TRC,
                               "In CEEPostMsgToDCBX: "
                               "DCBX TLV DeRegistration with LLDP Failed !!!, for the port %d"
                               "\r\n", u4IfIndex);
            }
            /* Clear Ctrl TLV params */
            CEEUtlClearCeeCtrlParams (u4IfIndex);

            break;

        case DCBX_STATE_MACHINE:
            /* Used to run the DCBX state machine and get the 
             * necessary Operational state  */
            DCBX_TRC (DCBX_SEM_TRC,
                      "CEEPostMsgToDCBX:"
                      "ApplicationPriority Port SEM update request to DCBX!!!\r\n");
            DcbxSemUpdate (u4IfIndex, pDcbxAppPortMsg);
            break;

        default:
            DCBX_TRC (DCBX_FAILURE_TRC,
                      "CEEPostMsgToDCBX:"
                      "Invalid Message Type Received !!!\r\n");
            break;
    }

    DCBX_TRC_ARG1 (DCBX_MGMT_TRC,
                   "In %s : Successfully posted Msg to DCBX.\r\n", __func__);

    return;
}

/***************************************************************************
 *  FUNCTION NAME : CEERegisterApplForPort
 * 
 *  DESCRIPTION   : This function is used to register the Appl
 *                  with DCBX.
 * 
 *  INPUT         : pPortEntry - PFC Port Entry.
 * 
 *  OUTPUT        : None
 * 
 *  RETURNS       : None
 * 
 * **************************************************************************/
/* This will be called from the TLV tx En and Module En and Admin mode 
 * changeto Auto*/
VOID
CEERegisterApplForPort (UINT4 u4IfIndex, UINT1 u1FeatType)
{
    tDcbxAppEntry      *pDcbxAppEntry = NULL;
    tPFCPortEntry      *pPFCEntry = NULL;
    tETSPortEntry      *pPGEntry = NULL;
    tAppPriPortEntry   *pAppPriEntry = NULL;
    tDcbxPortEntry     *pDcbxPortEntry = NULL;
    UINT1               u1RegReqd = OSIX_FALSE;

    DCBX_TRC_ARG3 (DCBX_CONTROL_PLANE_TRACE,
                   "In %s : CEE Registering Application for port: %d "
                   "for feature %s \r\n", __func__, u4IfIndex,
                   DCBX_GET_FEATURE_TYPE_MODE (u1FeatType));

    pDcbxPortEntry = DcbxUtlGetPortEntry (u4IfIndex);

    if (pDcbxPortEntry == NULL)
    {
        return;
    }

    if ((DCBX_DISABLED == pDcbxPortEntry->u1LldpTxStatus) ||
        (DCBX_DISABLED == pDcbxPortEntry->u1LldpRxStatus))
    {
        DCBX_TRC_ARG1 (DCBX_FAILURE_TRC, "In CEERegisterApplForPort :"
                       " LLDP Tx / Rx is disabled! for port %d, Hence Return!!!\r\n",
                       u4IfIndex);
        return;
    }

    /* Note : When the Admin mode of the port is not set to Auto then no need to
     * register any application with DCBX since feature is not ready to operate
     * through DCBX and it can take the local configuration with ON Mode */
    switch (u1FeatType)
    {
        case CEE_ETS_TLV_TYPE:
            if (gETSGlobalInfo.u1ETSModStatus == ETS_DISABLED)
            {
                DCBX_TRC_ARG1 (DCBX_FAILURE_TRC, "In CEERegisterApplForPort :"
                               " Module status for ETS is disabled! for the port %d, "
                               "Hence Return!!!\r\n", u4IfIndex);
                break;
            }
            pPGEntry = ETSUtlGetPortEntry (u4IfIndex);
            if (pPGEntry != NULL)
            {
                if (pPGEntry->u1ETSAdminMode != ETS_PORT_MODE_AUTO)
                {
                    DCBX_TRC_ARG1 (DCBX_FAILURE_TRC,
                                   "In CEERegisterApplForPort :"
                                   " ETS Module registration is not reqd as the "
                                   "Mode is not Auto! for the port %d\r\n",
                                   u4IfIndex);
                    break;
                }
                if (pPGEntry->u1ETSConfigTxStatus == ETS_DISABLED)
                {
                    DCBX_TRC_ARG1 (DCBX_FAILURE_TRC,
                                   "In CEERegisterApplForPort :"
                                   " Tx Status for ETS is disabled! for the port %d, "
                                   "Hence Return!!!\r\n", u4IfIndex);
                    break;
                }
            }
            else
            {
                DCBX_TRC_ARG1 (DCBX_FAILURE_TRC, "In CEERegisterApplForPort :"
                               " Port Entry is NULL for the port: %d ! Hence Return!!!\r\n",
                               u4IfIndex);
                break;
            }
            u1RegReqd = OSIX_TRUE;
            break;

        case CEE_PFC_TLV_TYPE:
            if (gPFCGlobalInfo.u1PFCModStatus == PFC_DISABLED)
            {
                DCBX_TRC (DCBX_FAILURE_TRC, "CEERegisterApplForPort:"
                          " Module status for PFC is disabled. Return!!!\r\n");
                break;
            }
            pPFCEntry = PFCUtlGetPortEntry (u4IfIndex);
            if (pPFCEntry != NULL)
            {
                if (pPFCEntry->u1PFCAdminMode != PFC_PORT_MODE_AUTO)
                {
                    DCBX_TRC (DCBX_FAILURE_TRC, "CEERegisterApplForPort:"
                              " PFC Module registration is not reqd as the "
                              "Mode is not Auto.\r\n");
                    break;
                }
                if (pPFCEntry->u1PFCTxStatus == PFC_DISABLED)
                {
                    DCBX_TRC (DCBX_FAILURE_TRC, "CEERegisterApplForPort:"
                              " Tx Status for PFC is disabled. Return!!!\r\n");
                    break;
                }
            }
            else
            {
                DCBX_TRC (DCBX_FAILURE_TRC, "CEERegisterApplForPort:"
                          " Port Entry is NULL. Return!!!\r\n");
                break;
            }
            u1RegReqd = OSIX_TRUE;
            break;

        case CEE_APP_PRI_TLV_TYPE:
            if (gAppPriGlobalInfo.u1AppPriModStatus == APP_PRI_DISABLED)
            {
                DCBX_TRC (DCBX_FAILURE_TRC, "CEERegisterApplForPort:"
                          " Module status for App Priority is disabled. "
                          "Return!!!\r\n");
                break;
            }
            pAppPriEntry = AppPriUtlGetPortEntry (u4IfIndex);
            if (pAppPriEntry != NULL)
            {
                if (pAppPriEntry->u1AppPriAdminMode != APP_PRI_PORT_MODE_AUTO)
                {
                    DCBX_TRC (DCBX_FAILURE_TRC, "CEERegisterApplForPort:"
                              " App Priority Module registration is not reqd "
                              "as the Mode is not Auto.\r\n");
                    break;
                }
                if (pAppPriEntry->u1AppPriTxStatus == APP_PRI_DISABLED)
                {
                    DCBX_TRC (DCBX_FAILURE_TRC, "CEERegisterApplForPort:"
                              " Tx Status for App Priority is disabled. "
                              "Return!!!\r\n");
                    break;
                }
            }
            else
            {
                DCBX_TRC (DCBX_FAILURE_TRC, "CEERegisterApplForPort:"
                          " Port Entry is NULL. Return!!!\r\n");
                break;
            }
            u1RegReqd = OSIX_TRUE;
            break;

        default:
            DCBX_TRC (DCBX_FAILURE_TRC, "CEERegisterApplForPort:"
                      " Received Registration Request for unknown feature.!!!\r\n");
            break;

    }
    if (u1RegReqd == OSIX_TRUE)
    {
        /* Get the application entry from the DCBX Application table */
        pDcbxAppEntry = DcbxUtlGetAppEntryBasedOnMode (DCBX_MODE_CEE,
                                                       u4IfIndex, DCBX_ZERO);

        /* For CEE, we call this function for PFC, ETS and AP to Register.
         * We will register only once from any of the feature and update 
         * for others. */
        if ((pDcbxAppEntry == NULL) ||
            (pDcbxAppEntry->ApplMappedIfPortList[u4IfIndex] == OSIX_FALSE))
        {
            DCBX_TRC_ARG2 (DCBX_TLV_TRC, "In CEERegisterApplForPort :"
                           "ModuleStatus,TxStatus = Enabled Mode=Auto for the Feature:"
                           " %s, Hence Forming and Sending"
                           " CEE TLV with Port Registration request!!! for the port %d\r\n",
                           ((u1FeatType == CEE_ETS_TLV_TYPE) ? "ETS" :
                            ((u1FeatType ==
                              CEE_PFC_TLV_TYPE) ? "PFC" : "AppPri")),
                           u4IfIndex);
            /* For Re-Registration, Form the TLV and send it with
             * Port Registration request to the DCBX */
            CEEFormAndSendTLV (u4IfIndex, DCBX_LLDP_PORT_REG, DCBX_ZERO);
        }
        else
        {
            DCBX_TRC_ARG2 (DCBX_TLV_TRC, "In CEERegisterApplForPort :"
                           "ModuleStatus,TxStatus = Enabled Mode=Auto,"
                           " Port Registration already done! with any of the feature ETS/PFC/AppPri!!! Hence Forming and Sending"
                           " CEE TLV with Update request!!! for Feature: %s, for port %d\r\n",
                           ((u1FeatType == CEE_ETS_TLV_TYPE) ? "ETS" :
                            ((u1FeatType ==
                              CEE_PFC_TLV_TYPE) ? "PFC" : "AppPri")),
                           u4IfIndex);
            CEEFormAndSendTLV (u4IfIndex, DCBX_LLDP_PORT_UPDATE, DCBX_ZERO);
        }
    }
    return;
}

/***************************************************************************
 *  FUNCTION NAME : CEEDeRegisterApplForPort
 * 
 *  DESCRIPTION   : This function is used to De Register the Appl with DCBX.
 * 
 *  INPUT         : u4IfIndex - Interface
 * 
 *  OUTPUT        : None
 * 
 *  RETURNS       : None
 * 
 * **************************************************************************/
/* This will be called from the TLV tx Dis and #Module Dis and port 
 * Delete and Admin mode change to Dis/On */
VOID
CEEDeRegisterApplForPort (UINT4 u4IfIndex, UINT1 u1FeatType)
{
    tDcbxAppRegInfo     DcbxAppPortMsg;
    tPFCPortEntry      *pPFCEntry = NULL;
    tETSPortEntry      *pPGEntry = NULL;
    tAppPriPortEntry   *pAppPriEntry = NULL;
    BOOL1               bAppPriRegFlag = OSIX_FALSE;
    BOOL1               bETSRegFlag = OSIX_FALSE;
    BOOL1               bPFCRegFlag = OSIX_FALSE;
    BOOL1               bDeRegFlag = OSIX_FALSE;

    DCBX_TRC_ARG2 (DCBX_TLV_TRC, "In %s : DeRegistering application with DCBX"
                   " for the port %d\r\n", __func__, u4IfIndex);

    pPFCEntry = PFCUtlGetPortEntry (u4IfIndex);
    if (pPFCEntry != NULL)
    {
        if ((pPFCEntry->u1PFCAdminMode == PFC_PORT_MODE_AUTO) &&
            (pPFCEntry->u1PFCTxStatus == PFC_ENABLED) &&
            (gPFCGlobalInfo.u1PFCModStatus == PFC_ENABLED))
        {
            bPFCRegFlag = OSIX_TRUE;
        }
    }

    pPGEntry = ETSUtlGetPortEntry (u4IfIndex);
    if (pPGEntry != NULL)
    {
        if ((pPGEntry->u1ETSAdminMode == ETS_PORT_MODE_AUTO) &&
            (pPGEntry->u1ETSConfigTxStatus == ETS_ENABLED) &&
            (gETSGlobalInfo.u1ETSModStatus == ETS_ENABLED))
        {
            bETSRegFlag = OSIX_TRUE;
        }
    }

    pAppPriEntry = AppPriUtlGetPortEntry (u4IfIndex);
    if (pAppPriEntry != NULL)
    {
        if ((pAppPriEntry->u1AppPriAdminMode == APP_PRI_PORT_MODE_AUTO) &&
            (pAppPriEntry->u1AppPriTxStatus == APP_PRI_ENABLED) &&
            (gAppPriGlobalInfo.u1AppPriModStatus == APP_PRI_ENABLED))
        {
            bAppPriRegFlag = OSIX_TRUE;
        }
    }

    if (u1FeatType == CEE_ETS_TLV_TYPE)
    {
        if (bETSRegFlag == OSIX_FALSE)
        {
            DCBX_TRC (DCBX_FAILURE_TRC, "CEEDeRegisterApplForPort:"
                      " No DeRegistration Required as ETS Module is "
                      "not Registered!!!\r\n");
            return;
        }
        if ((bPFCRegFlag == OSIX_FALSE) && (bAppPriRegFlag == OSIX_FALSE))
        {
            bDeRegFlag = OSIX_TRUE;
        }
    }
    if (u1FeatType == CEE_PFC_TLV_TYPE)
    {
        if (bPFCRegFlag == OSIX_FALSE)
        {
            DCBX_TRC (DCBX_FAILURE_TRC, "CEEDeRegisterApplForPort:"
                      " No DeRegistration Required as PFC Module is "
                      "not Registered!!!\r\n");
            return;
        }
        if ((bETSRegFlag == OSIX_FALSE) && (bAppPriRegFlag == OSIX_FALSE))
        {
            bDeRegFlag = OSIX_TRUE;
        }
    }
    if (u1FeatType == CEE_APP_PRI_TLV_TYPE)
    {
        if (bAppPriRegFlag == OSIX_FALSE)
        {
            DCBX_TRC (DCBX_FAILURE_TRC, "CEEDeRegisterApplForPort:"
                      " No DeRegistration Required as APP Pri Module is "
                      "not Registered!!!\r\n");
            return;
        }
        if ((bETSRegFlag == OSIX_FALSE) && (bPFCRegFlag == OSIX_FALSE))
        {
            bDeRegFlag = OSIX_TRUE;
        }
    }

    /* DeRegFlag is set to ture if any feature is disable  and
     * Deg Port request is sent else Port Update request*/
    if (bDeRegFlag == OSIX_TRUE)
    {

        /* De-Register with LLDP if all the TLV TxStatus is Disabled. */
        MEMSET (&DcbxAppPortMsg, DCBX_ZERO, sizeof (tDcbxAppRegInfo));
        CEE_GET_APPL_ID (DcbxAppPortMsg.DcbxAppId);

        DCBX_TRC_ARG1 (DCBX_TLV_TRC, "In CEEDeRegisterApplForPort:"
                       " TLV TxStatus for features ETS/PFC/AppPri are DISABLED in port %d,"
                       " Hence Sending TLV DeRegistration Message!!!\r\n",
                       u4IfIndex);

        CEEPostMsgToDCBX (DCBX_LLDP_PORT_DEREG, u4IfIndex, &DcbxAppPortMsg);
    }
    else
    {
        DCBX_TRC_ARG2 (DCBX_TLV_TRC, "In %s : TLV TxStatus is not DISABLED "
                       "for features ETS/PFC/AppPri in port %d"
                       "Hence Sending port update request\r\n", __func__,
                       u4IfIndex);
        CEEFormAndSendTLV (u4IfIndex, DCBX_LLDP_PORT_UPDATE, u1FeatType);
    }

    return;
}

/***************************************************************************
 *  FUNCTION NAME : CEEHandleReRegReqFromDCBX
 * 
 *  DESCRIPTION   : This function is used to Handle the Re Reg
 *                  Message from DCBX. Will be called from Callbackfunc.
 * 
 *  INPUT         : pPortEntry - PFC Port Entry.
 * 
 *  OUTPUT        : None
 * 
 *  RETURNS       : None
 * 
 * **************************************************************************/
VOID
CEEHandleReRegReqFromDCBX (UINT4 u4IfIndex)
{
    DCBX_TRC_ARG1 (DCBX_TLV_TRC, "%s : Re Registering application\r\n",
                   __func__);

    /* For Re-Registration, Form the TLV and send it with
     * Port Registration request to the DCBX */
    CEEFormAndSendTLV (u4IfIndex, DCBX_LLDP_PORT_REG, DCBX_ZERO);

    DCBX_TRC_ARG1 (DCBX_TLV_TRC, "Existing %s\r\n", __func__);

    return;
}

/*********************** END OF FILE ********************/
