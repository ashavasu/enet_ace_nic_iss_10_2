/*************************************************************************
 * Copyright (C) 2010 Aricent Inc . All Rights Reserved
 * $Id: apapi.c,v 1.7 2017/11/17 11:42:08 siva Exp $
 * Description: This file contains Application Priority function Exported
 * to DCBX.
****************************************************************************/
#include "dcbxinc.h"

/***************************************************************************
 *  FUNCTION NAME : APApiTlvCallBackFn
 * 
 *  DESCRIPTION   : This call back function will be registered with DCBX
 *                  to hanlde the Application Priority TLV related messages
 * 
 *  INPUT         : tDcbxAppInfo - Structure with TLV info.
 * 
 *  OUTPUT        : None
 * 
 *  RETURNS       : None
 * 
 * **************************************************************************/
PUBLIC VOID
AppPriApiTlvCallBackFn (tDcbxAppInfo * pDcbxAppInfo)
{

    tAppPriPortEntry   *pPortEntry = NULL;

    pPortEntry = AppPriUtlGetPortEntry (pDcbxAppInfo->u4IfIndex);

    /* Get the Application Priority port entry  and if the port entry is not
     * present then return without processing the message from DCBX */

    if (pPortEntry == NULL)
    {
        DCBX_TRC (DCBX_FAILURE_TRC, "APApiTlvCallBackFn:"
                  " No Applciation Priority Port is created!!!\r\n");
        return;
    }

    switch (pDcbxAppInfo->u4MsgType)
    {
        case L2IWF_LLDP_APPL_TLV_RECV:
            /* TLV Received from the LLDP through DCBX,
             * process the TLV infomration and fill the remote table */
            DCBX_TRC (DCBX_TLV_TRC, "APApiTlvCallBackFn:"
                      "DCBX Application Priority TLV Received  !!!\r\n");
            AppPriUtlHandleTLV (pPortEntry, &pDcbxAppInfo->ApplTlvParam);
            break;
        case L2IWF_LLDP_APPL_TLV_AGED:
        case L2IWF_LLDP_MULTIPLE_PEER_NOTIFY:
            /* Process the Age Out event from the LLDP through DCBX */
            DCBX_TRC (DCBX_TLV_TRC, "APApiTlvCallBackFn:"
                      "DCBX Application Priority TLV Age Out Received  !!!\r\n");
            AppPriUtlHandleAgedOutFromDCBX (pPortEntry);
            break;
        case L2IWF_LLDP_APPL_RE_REG:
            /* Handle the Re-registration request from the LLDP */
            DCBX_TRC (DCBX_TLV_TRC, "APApiTlvCallBackFn:"
                      "DCBX Application Priority TLV Re Registration "
                      "RequestReceived  !!!\r\n");
            AppPriUtlHandleReRegReqFromDCBX (pPortEntry);
            break;
        case DCBX_OPER_UPDATE:
            /* State machine update from the DCBX */
            DCBX_TRC (DCBX_SEM_TRC, "APApiTlvCallBackFn:"
                      "DCBX Application Priority Sem Update Received  !!!\r\n");
            /* Change the state with the DCBX Oper
             * state received from the DCBX */
            AppPriUtlOperStateChange (pPortEntry,
                                      pDcbxAppInfo->u1ApplOperState);
            /* Send SEM update event to Standby Node */
            DcbxRedSendDynamicAppPriSemInfo (pPortEntry);
            break;

        default:
            break;
    }
    return;
}

/***************************************************************************
 *  FUNCTION NAME : AppPriApiGetAppPriMapping
 * 
 *  DESCRIPTION   : This function is used to Get an Application to 
 *                  Priority mapping entry on a specific port. This API is
 *                  called from Upper layer protocols.
 * 
 *  INPUT         : tAppPriApiMappingInfo - Structure with Mapping Info to be 
 *                  created
 * 
 *  OUTPUT        : None
 * 
 *  RETURNS       : None
 * 
 * **************************************************************************/
PUBLIC INT4
AppPriApiGetAppPriMapping (tAppPriApiMappingInfo * pAppPriApimappingInfo)
{

    tAppPriMappingEntry *pAppPriMappingEntry = NULL;

    if (pAppPriApimappingInfo == NULL)
    {
        DCBX_TRC (DCBX_FAILURE_TRC,
                  "AppPriApiAddAppPriMapping: pAppPriApimappingInfo is NULL");
        return OSIX_FAILURE;
    }

    pAppPriMappingEntry = AppPriUtlGetAppPriMappingEntry
        (pAppPriApimappingInfo->u4IfIndex, pAppPriApimappingInfo->i4Selector,
         pAppPriApimappingInfo->i4Protocol, APP_PRI_ADMIN);

    /* If the entry is not pressent, return Failure
     * else if the entry is already present, update the priority in 
     * pAppPriApimappingInfo */

    if (pAppPriMappingEntry == NULL)
    {
        pAppPriApimappingInfo->i4ErrorCode = DCBX_APP_PRI_MAPPING_NOT_FOUND;
        return OSIX_FAILURE;
    }

    pAppPriApimappingInfo->u1Priority = pAppPriMappingEntry->u1AppPriPriority;
    return OSIX_SUCCESS;
}

/***************************************************************************
 *  FUNCTION NAME : AppPriApiAddAppPriMapping
 * 
 *  DESCRIPTION   : This function is used to create an Application to 
 *                  Priority mapping entry on a specific port. This API is
 *                  called from Upper layer protocols.
 * 
 *  INPUT         : tAppPriApiMappingInfo - Structure with Mapping Info to be 
 *                  created
 * 
 *  OUTPUT        : None
 * 
 *  RETURNS       : None
 * 
 * **************************************************************************/
PUBLIC INT4
AppPriApiAddAppPriMapping (tAppPriApiMappingInfo * pAppPriApimappingInfo)
{

    tAppPriMappingEntry *pAppPriMappingEntry = NULL;
    tAppPriPortEntry   *pAppPortEntry = NULL;

    if (pAppPriApimappingInfo == NULL)
    {
        DCBX_TRC (DCBX_FAILURE_TRC,
                  "AppPriApiAddAppPriMapping: pAppPriApimappingInfo is NULL");
        return OSIX_FAILURE;
    }

    /*Since this API can be called by external applications, module lock 
     * should be taken */
    DcbxLock ();
    pAppPortEntry = AppPriUtlGetPortEntry (pAppPriApimappingInfo->u4IfIndex);

    if (pAppPortEntry == NULL)
    {
        DCBX_TRC (DCBX_FAILURE_TRC,
                  "AppPriApiAddAppPriMapping: pAppPortEntry is NULL");
        DcbxUnLock ();
        return OSIX_FAILURE;
    }

    pAppPriMappingEntry = AppPriUtlGetAppPriMappingEntry
        (pAppPriApimappingInfo->u4IfIndex, pAppPriApimappingInfo->i4Selector,
         pAppPriApimappingInfo->i4Protocol, APP_PRI_ADMIN);

    /* If the entry is not pressent, create an entry with the given data in 
     * pAppPriApimappingInfo
     * else if the entry is already present, update the entry with the priority
     * specified in pAppPriApimappingInfo */

    if (pAppPriMappingEntry == NULL)
    {
        pAppPriMappingEntry = AppPriUtlCreateAppPriMappingTblEntry
            (pAppPriApimappingInfo->u4IfIndex,
             pAppPriApimappingInfo->i4Selector,
             pAppPriApimappingInfo->i4Protocol, APP_PRI_ADMIN);
        if (pAppPriMappingEntry == NULL)
        {
            DCBX_TRC_ARG1 (DCBX_FAILURE_TRC,
                           "In %s: AppPriUtlCreateAppPriMappingTblEntry "
                           " returns NULL", __FUNCTION__);
            pAppPriApimappingInfo->i4ErrorCode =
                DCBX_APP_PRI_MAPPING_CREATION_FAILED;
            DcbxUnLock ();
            return OSIX_FAILURE;
        }
    }
    /* If Oper state is INIT, update H/w entries accordingly */
    if (pAppPortEntry->u1AppPriDcbxOperState == APP_PRI_OPER_INIT)
    {
        /* Delete application Priority mapping entry from hardware */
        if (AppPriUtlHwConfigAppPriMappingEntry
            (pAppPriApimappingInfo->u4IfIndex,
             pAppPriApimappingInfo->i4Selector,
             pAppPriMappingEntry, APP_PRI_HW_DELETE) == OSIX_FAILURE)
        {
            DCBX_TRC_ARG1 (DCBX_FAILURE_TRC,
                           "In %s : AppPriUtlHwConfigAppPriMappingEntry "
                           "returned FAILURE. \r\n", __FUNCTION__);
            pAppPriApimappingInfo->i4ErrorCode =
                DCBX_APP_PRI_MAPPING_CREATION_FAILED;
            DcbxUnLock ();
            return SNMP_FAILURE;
        }

        /*Update the entry with new Priority in control-plane */
        pAppPriMappingEntry->u1AppPriPriority =
            pAppPriApimappingInfo->u1Priority;
        AppPriUtlAdminParamChange (pAppPortEntry);

        /* Create application Priority mapping entry in hardware */
        if (AppPriUtlHwConfigAppPriMappingEntry
            (pAppPriApimappingInfo->u4IfIndex,
             pAppPriApimappingInfo->i4Selector,
             pAppPriMappingEntry, APP_PRI_HW_CREATE) == OSIX_FAILURE)
        {
            DCBX_TRC_ARG1 (DCBX_FAILURE_TRC,
                           "In %s : AppPriUtlHwConfigAppPriMappingEntry "
                           "returned FAILURE. \r\n", __FUNCTION__);
            pAppPriApimappingInfo->i4ErrorCode =
                DCBX_APP_PRI_MAPPING_CREATION_FAILED;
            DcbxUnLock ();
            return SNMP_FAILURE;
        }
    }
    else
    {
        /*Update the entry with new Priority in control-plane */
        pAppPriMappingEntry->u1AppPriPriority =
            pAppPriApimappingInfo->u1Priority;
        AppPriUtlAdminParamChange (pAppPortEntry);
    }
    pAppPriMappingEntry->u1RowStatus = ACTIVE;
    DcbxUnLock ();
    return OSIX_SUCCESS;
}

/***************************************************************************
 *  FUNCTION NAME : AppPriApiRemoveAppPriMapping
 * 
 *  DESCRIPTION   : This function is used to remove an Application to 
 *                  Priority mapping entry on a specific port. This API is
 *                  called from Upper layer protocols.
 * 
 *  INPUT         : tAppPriApiMappingInfo - Structure with Mapping Info to be 
 *                  created
 * 
 *  OUTPUT        : None
 * 
 *  RETURNS       : None
 * 
 * **************************************************************************/
PUBLIC INT4
AppPriApiRemoveAppPriMapping (tAppPriApiMappingInfo * pAppPriApimappingInfo)
{

    tAppPriMappingEntry *pAppPriMappingEntry = NULL;
    tAppPriPortEntry   *pAppPortEntry = NULL;

    if (pAppPriApimappingInfo == NULL)
    {
        DCBX_TRC (DCBX_FAILURE_TRC,
                  "AppPriApiAddAppPriMapping: pAppPriApimappingInfo is NULL");
        return OSIX_FAILURE;
    }

    /*Since this API can be called by external applications, module lock 
     * should be taken */
    DcbxLock ();

    pAppPortEntry = AppPriUtlGetPortEntry (pAppPriApimappingInfo->u4IfIndex);

    if (pAppPortEntry == NULL)
    {
        DCBX_TRC_ARG1 (DCBX_FAILURE_TRC,
                       "In %s: pAppPortEntry is NULL", __FUNCTION__);
        DcbxUnLock ();
        return OSIX_FAILURE;
    }
    pAppPriMappingEntry = AppPriUtlGetAppPriMappingEntry
        (pAppPriApimappingInfo->u4IfIndex, pAppPriApimappingInfo->i4Selector,
         pAppPriApimappingInfo->i4Protocol, APP_PRI_ADMIN);
    /* If Applciation Priority Mapping with the given data is not present,
     * return Failure
     * else if the entry is present, delete that entry */

    if (pAppPriMappingEntry == NULL)
    {
        pAppPriApimappingInfo->i4ErrorCode = DCBX_APP_PRI_MAPPING_NOT_FOUND;
        DcbxUnLock ();
        return OSIX_FAILURE;
    }

    /* If Oper state is INIT, update H/w entries accordingly */
    if (pAppPortEntry->u1AppPriDcbxOperState == APP_PRI_OPER_INIT)
    {
        /* Delete application Priority mapping entry from hardware */
        if (AppPriUtlHwConfigAppPriMappingEntry
            (pAppPriApimappingInfo->u4IfIndex,
             pAppPriApimappingInfo->i4Selector,
             pAppPriMappingEntry, APP_PRI_HW_DELETE) == OSIX_FAILURE)
        {
            DCBX_TRC_ARG1 (DCBX_FAILURE_TRC,
                           "In %s : AppPriUtlHwConfigAppPriMappingEntry "
                           "returned FAILURE. \r\n", __FUNCTION__);
            pAppPriApimappingInfo->i4ErrorCode =
                DCBX_APP_PRI_MAPPING_DELETION_FAILED;
            DcbxUnLock ();
            return SNMP_FAILURE;
        }
    }

    /* Delete the application priority mapping entry in control-plane
     * database */
    if (AppPriUtlDeleteAppPriMappingEntry (pAppPriApimappingInfo->u4IfIndex,
                                           pAppPriApimappingInfo->i4Selector,
                                           pAppPriApimappingInfo->i4Protocol,
                                           APP_PRI_ADMIN) == OSIX_FAILURE)
    {
        pAppPriApimappingInfo->i4ErrorCode =
            DCBX_APP_PRI_MAPPING_DELETION_FAILED;
        DcbxUnLock ();
        return OSIX_FAILURE;
    }
    AppPriUtlAdminParamChange (pAppPortEntry);

    DcbxUnLock ();
    return OSIX_SUCCESS;
}

/***************************************************************************
 *  FUNCTION NAME : AppPriApiIsCfgCompatible
 * 
 *  DESCRIPTION   : This function is used to check the compatibility between
 *                  local and the peer for Application Priority Feature
 * 
 *  INPUT         : pAppPriPortEntry - AppPri Table entry.
 *                  i4Selector - Selector for checking compatibility
 *                  i4Protocol - Ethertype/TCP/UDP
 * 
 *  OUTPUT        : None
 * 
 *  RETURNS       : TRUE/FALSE
 * 
 * **************************************************************************/
PUBLIC              BOOL1
AppPriApiIsCfgCompatible (tAppPriPortEntry * pPortEntry)
{
    tAppPriMappingEntry *pAppPriRemMappingEntry = NULL;
    tAppPriMappingEntry *pAppPriAdmMappingEntry = NULL;
    tAppPriMappingEntry *pNextAppPriAdmMappingEntry = NULL;
    tAppPriMappingEntry *pNextAppPriRemMappingEntry = NULL;
    tTMO_SLL           *pAppPriAdmTbl = NULL;
    tTMO_SLL           *pAppPriRemTbl = NULL;
    INT4                i4Selector = DCBX_ZERO;
    UINT1               u1OperVersion = DCBX_ZERO;
    UINT1               u1LclNonFcoeCount = DCBX_ZERO;
    UINT1               u1RemNonFcoeCount = DCBX_ZERO;
    BOOL1               b1LclFcoeExist = OSIX_FALSE;
    BOOL1               b1RemFcoeExist = OSIX_FALSE;
    BOOL1               b1LclFipExist = OSIX_FALSE;
    BOOL1               b1RemFipExist = OSIX_FALSE;
    BOOL1               b1FcoeFipExist = OSIX_FALSE;

    for (i4Selector = DCBX_ONE; i4Selector <= APP_PRI_MAX_SELECTOR;
         i4Selector++)
    {
        DcbxUtlGetOperVersion (pPortEntry->u4IfIndex, &u1OperVersion);
        if (u1OperVersion == DCBX_VER_CEE)
        {
            /* Skip selector 2 and 3 for CEE */
            if (i4Selector == DCBX_TWO || i4Selector == DCBX_THREE)
            {
                continue;
            }
        }

        /* Get the ADM & REM SLL maintained for each selector using the
         * PortEntry */
        pAppPriAdmTbl = &(APP_PRI_ADM_TBL (pPortEntry, i4Selector - 1));
        pAppPriRemTbl = &(APP_PRI_REM_TBL (pPortEntry, i4Selector - 1));
        if ((TMO_SLL_Count (pAppPriAdmTbl) == 0) &&
            (TMO_SLL_Count (pAppPriRemTbl) == 0))
        {
            /* If the selector not present in Adm or Remote, 
             * continue for other selector(s) */
            continue;
        }

        /* Get the Adm and Rem Protocol and Priority using SLL Table for the
         * selector */
        pAppPriAdmMappingEntry = TMO_SLL_First (pAppPriAdmTbl);
        pAppPriRemMappingEntry = TMO_SLL_First (pAppPriRemTbl);
        if ((pAppPriAdmMappingEntry == NULL) ||
            (pAppPriRemMappingEntry == NULL))
        {
            DCBX_TRC_ARG2 (DCBX_CONTROL_PLANE_TRC,
                           "In AppPriApiIsCfgCompatible: "
                           "AppPriMappingEntry for port %d i4Selector %d "
                           "doesn't exist.\r\n", pPortEntry->u4IfIndex,
                           i4Selector);

            SET_DCBX_STATUS (pPortEntry, pPortEntry->u1AppPriDcbxSemType,
                             DCBX_RED_APP_PRI_DCBX_STATUS_INFO,
                             DCBX_STATUS_CFG_NOT_COMPT);
            return OSIX_FALSE;
        }

        b1LclFcoeExist = OSIX_FALSE;
        b1LclFipExist = OSIX_FALSE;
        b1RemFcoeExist = OSIX_FALSE;
        b1RemFipExist = OSIX_FALSE;
        /* The following will be used to collect FCOE and FIP flags for Adm and
         * Remote */
        if (i4Selector == DCBX_ONE)
        {

            /* Check for Remote entries for FCOE */
            while (pAppPriAdmMappingEntry != NULL)
            {
                if (DCBX_IS_FCOE_PROTOCOL
                    (pAppPriAdmMappingEntry->i4AppPriProtocol))
                    b1LclFcoeExist = OSIX_TRUE;
                else if (DCBX_IS_FIP_PROTOCOL
                         (pAppPriAdmMappingEntry->i4AppPriProtocol))
                    b1LclFipExist = OSIX_TRUE;
                else
                    u1LclNonFcoeCount++;

                pNextAppPriAdmMappingEntry = TMO_SLL_Next (pAppPriAdmTbl,
                                                           &
                                                           (pAppPriAdmMappingEntry->
                                                            AppPriMappingEntry));
                pAppPriAdmMappingEntry = pNextAppPriAdmMappingEntry;
            }

            /* Check for Remote entries for FCOE */
            while (pAppPriRemMappingEntry != NULL)
            {
                if (DCBX_IS_FCOE_PROTOCOL
                    (pAppPriRemMappingEntry->i4AppPriProtocol))
                    b1RemFcoeExist = OSIX_TRUE;
                else if (DCBX_IS_FIP_PROTOCOL
                         (pAppPriRemMappingEntry->i4AppPriProtocol))
                    b1RemFipExist = OSIX_TRUE;
                else
                    u1RemNonFcoeCount++;

                pNextAppPriRemMappingEntry = TMO_SLL_Next (pAppPriRemTbl,
                                                           &
                                                           (pAppPriRemMappingEntry->
                                                            AppPriMappingEntry));
                pAppPriRemMappingEntry = pNextAppPriRemMappingEntry;
            }
        }

        /* If FCOE present in both local & peer, handle special case. Hence do
         * not check for count */
        if (((b1LclFcoeExist == OSIX_FALSE) || (b1RemFcoeExist == OSIX_FALSE))
            && (TMO_SLL_Count (pAppPriAdmTbl) != TMO_SLL_Count (pAppPriRemTbl)))
        {
            DCBX_TRC (DCBX_CONTROL_PLANE_TRC, "AppPriApiIsCfgCompatible: "
                      "DCBX STATUS = CFG NOT Compatible\r\n");

            SET_DCBX_STATUS (pPortEntry, pPortEntry->u1AppPriDcbxSemType,
                             DCBX_RED_APP_PRI_DCBX_STATUS_INFO,
                             DCBX_STATUS_CFG_NOT_COMPT);
            return OSIX_FALSE;
        }

        /* The compatible method for the AP TLV should return TRUE if the
         * common subset of local and remote protocols is matched and
         * the local config will be usedm, based on willing bit. 
         * If there are any APs that are not
         * common, the discrepancy will be reported locally (e.g., a new DCBX
         * status CfgNotCompatibleButSupported). As noted, if partner
         * advertises FCOE, act as if partner had also advertised FIP at the
         * same priority as FCOE. */
        /* ______________________________________________________________
           |          Local               |           Peer               |
           | Selector Protocol Priority   |  Selector Protocol Priority  |
           | -----------------------------| ---------------------------- |
           |    1     FCOE      3         |    1      FCOE         3     |
           |    1     FIP       3         |                              |
           |______________________________|______________________________| 
         */
        if ((b1LclFcoeExist == OSIX_TRUE) && (b1RemFcoeExist == OSIX_TRUE) &&
            (b1LclFipExist == OSIX_TRUE) && (b1RemFipExist == OSIX_TRUE))
            b1FcoeFipExist = OSIX_TRUE;    /* This flag is maintained to handle if both FIP and FCOE present */

        if ((b1LclFcoeExist == OSIX_TRUE) && (b1RemFcoeExist == OSIX_TRUE))
        {
            /* To handle this special case the below function is used. */
            if (AppPriCompareFcoeMappings
                (pAppPriAdmTbl, pAppPriRemTbl, b1FcoeFipExist) == OSIX_FALSE)
            {
                SET_DCBX_STATUS (pPortEntry, pPortEntry->u1AppPriDcbxSemType,
                                 DCBX_RED_APP_PRI_DCBX_STATUS_INFO,
                                 DCBX_STATUS_CFG_NOT_COMPT);
                return OSIX_FALSE;
            }

            /* If Non FCOE protocols exists (matched) and if admin and remote
             * subset not matched, set the cfgNotCompatibleButSupported
             * status. */
            if (u1LclNonFcoeCount != u1RemNonFcoeCount)
            {
                SET_DCBX_STATUS (pPortEntry, pPortEntry->u1AppPriDcbxSemType,
                                 DCBX_RED_APP_PRI_DCBX_STATUS_INFO,
                                 DCBX_STATUS_CFG_NOT_COMP_BUT_SUPP);
                return OSIX_TRUE;
            }

            /* If NonFcoe protocols does not exists, no need to check for match. 
             * If exists compare for priority match. */
            if ((u1LclNonFcoeCount != DCBX_ZERO) &&
                AppPriCompareNonFcoeMappings (pAppPriAdmTbl, pAppPriRemTbl) ==
                OSIX_FALSE)
            {
                SET_DCBX_STATUS (pPortEntry, pPortEntry->u1AppPriDcbxSemType,
                                 DCBX_RED_APP_PRI_DCBX_STATUS_INFO,
                                 DCBX_STATUS_CFG_NOT_COMP_BUT_SUPP);
                return OSIX_TRUE;
            }
        }
        else
        {
            /* Compare all protocols for priority match */
            if (AppPriCompareAllMappingEntry (pAppPriAdmTbl, pAppPriRemTbl) ==
                OSIX_FALSE)
            {
                SET_DCBX_STATUS (pPortEntry, pPortEntry->u1AppPriDcbxSemType,
                                 DCBX_RED_APP_PRI_DCBX_STATUS_INFO,
                                 DCBX_STATUS_CFG_NOT_COMPT);
                return OSIX_FALSE;
            }
        }
    }                            /* End of for loop */

    DCBX_TRC (DCBX_CONTROL_PLANE_TRC, "AppPriApiIsCfgCompatible: "
              "Local and Remote configurations are compatible\r\n");

    if (pPortEntry->AppPriRemPortInfo.u1AppPriRemWilling == APP_PRI_DISABLED)
    {
        DCBX_TRC (DCBX_CONTROL_PLANE_TRC, "AppPriApiIsCfgCompatible: "
                  "DCBX STATUS = PEER NotWilling CFG Compatible\r\n");

        SET_DCBX_STATUS (pPortEntry, pPortEntry->u1AppPriDcbxSemType,
                         DCBX_RED_APP_PRI_DCBX_STATUS_INFO,
                         DCBX_STATUS_PEER_NW_CFG_COMPAT);
    }
    else
    {
        DCBX_TRC (DCBX_CONTROL_PLANE_TRC, "AppPriApiIsCfgCompatible: "
                  "DCBX STATUS = DCBX_STATUS_OK\r\n");
        SET_DCBX_STATUS (pPortEntry, pPortEntry->u1AppPriDcbxSemType,
                         DCBX_RED_APP_PRI_DCBX_STATUS_INFO, DCBX_STATUS_OK);
    }

    return OSIX_TRUE;
}

/***************************************************************************
 *  FUNCTION NAME : AppPriCompareAllMappingEntry 
 * 
 *  DESCRIPTION   : This function is used to check the compatibility between
 *                  local and the peer for Application Priority Feature
 * 
 *  INPUT         : pAppPriRemTbl - AppPri Admin Table entry.
 *                  pAppPriRemTbl - AppPri Remote Table Entry.
 * 
 *  OUTPUT        : None
 * 
 *  RETURNS       : TRUE/FALSE
 * 
 * **************************************************************************/
BOOL1
AppPriCompareAllMappingEntry (tTMO_SLL * pAppPriAdmTbl,
                              tTMO_SLL * pAppPriRemTbl)
{
    tAppPriMappingEntry *pRemEntry = NULL;
    tAppPriMappingEntry *pAdmEntry = NULL;
    tAppPriMappingEntry *pNextAdmEntry = NULL;
    tAppPriMappingEntry *pNextRemEntry = NULL;

    DCBX_TRC_ARG1 (DCBX_CONTROL_PLANE_TRC, "In Func: %s\n", __func__);

    pAdmEntry = TMO_SLL_First (pAppPriAdmTbl);
    pRemEntry = TMO_SLL_First (pAppPriRemTbl);
    while ((pAdmEntry != NULL) && (pRemEntry != NULL))
    {
        DCBX_TRC_ARG5 (DCBX_CONTROL_PLANE_TRC, "In Func: %s "
                       "Compare Adm Protocol: %d with Rem Protocol: %d and "
                       "Adm Priority: %d with Rem Priority: %d\n",
                       __func__, pAdmEntry->i4AppPriProtocol,
                       pRemEntry->i4AppPriProtocol, pAdmEntry->u1AppPriPriority,
                       pRemEntry->u1AppPriPriority);

        if ((pAdmEntry->i4AppPriProtocol != pRemEntry->i4AppPriProtocol) ||
            (pAdmEntry->u1AppPriPriority != pRemEntry->u1AppPriPriority))
        {
            return OSIX_FALSE;
        }

        pNextRemEntry = TMO_SLL_Next (pAppPriRemTbl,
                                      &(pRemEntry->AppPriMappingEntry));
        pRemEntry = pNextRemEntry;

        /* Get next Adm entry */
        pNextAdmEntry =
            TMO_SLL_Next (pAppPriAdmTbl, &(pAdmEntry->AppPriMappingEntry));
        pAdmEntry = pNextAdmEntry;
    }

    DCBX_TRC_ARG1 (DCBX_CONTROL_PLANE_TRC, "Exit Func: %s - Match found\n",
                   __func__);
    return OSIX_TRUE;
}

/***************************************************************************
 *  FUNCTION NAME : AppPriCompareFcoeMappings
 * 
 *  DESCRIPTION   : This function is used to check the compatibility between
 *                  local and the peer for Application Priority Feature
 * 
 *  INPUT         : pAdmEntry - AppPri Admin Table Entry.
 *                  pRemEntry - AppPri Remote Table Entry. 
 * 
 *  OUTPUT        : None
 * 
 *  RETURNS       : TRUE/FALSE
 * 
 * **************************************************************************/
BOOL1
AppPriCompareFcoeMappings (tTMO_SLL * pAppPriAdmTbl, tTMO_SLL * pAppPriRemTbl,
                           BOOL1 b1FcoeFipExist)
{
    tAppPriMappingEntry *pRemEntry = NULL;
    tAppPriMappingEntry *pAdmEntry = NULL;
    tAppPriMappingEntry *pNextAdmEntry = NULL;
    tAppPriMappingEntry *pNextRemEntry = NULL;

    /* If FCOE and FIP exists in both Adm and Remote entries */
    if (b1FcoeFipExist == OSIX_TRUE)
    {
        /* Adm entries */
        pAdmEntry = TMO_SLL_First (pAppPriAdmTbl);
        while (pAdmEntry != NULL)
        {
            /* Skip for non FCOE protocols */
            if (DCBX_IS_FCOE_OR_FIP_PROTOCOL (pAdmEntry->i4AppPriProtocol) ==
                OSIX_FALSE)
            {
                /* Get next Adm entry */
                pNextAdmEntry =
                    TMO_SLL_Next (pAppPriAdmTbl,
                                  &(pAdmEntry->AppPriMappingEntry));
                pAdmEntry = pNextAdmEntry;
                continue;
            }

            /* Remote entries */
            pRemEntry = TMO_SLL_First (pAppPriRemTbl);
            while (pRemEntry != NULL)
            {
                /* Skip for non FCOE protocols */
                if (DCBX_IS_FCOE_OR_FIP_PROTOCOL (pRemEntry->i4AppPriProtocol)
                    == OSIX_FALSE)
                {
                    /* Get the next remote entry */
                    pNextRemEntry =
                        TMO_SLL_Next (pAppPriRemTbl,
                                      &(pRemEntry->AppPriMappingEntry));
                    pRemEntry = pNextRemEntry;
                    continue;
                }

                /* In case FCOE/FIP doesnot match */
                if (pAdmEntry->i4AppPriProtocol == pRemEntry->i4AppPriProtocol)
                {
                    if (pAdmEntry->u1AppPriPriority !=
                        pRemEntry->u1AppPriPriority)
                    {
                        return OSIX_FALSE;
                    }
                    else
                        break;
                }
                /* Get the next remote entry */
                pNextRemEntry = TMO_SLL_Next (pAppPriRemTbl,
                                              &(pRemEntry->AppPriMappingEntry));
                pRemEntry = pNextRemEntry;
            }

            /* Get next Adm entry */
            pNextAdmEntry = TMO_SLL_Next (pAppPriAdmTbl,
                                          &(pAdmEntry->AppPriMappingEntry));
            pAdmEntry = pNextAdmEntry;
        }
    }
    else
    {
        /* Adm entries */
        pAdmEntry = TMO_SLL_First (pAppPriAdmTbl);
        while (pAdmEntry != NULL)
        {
            if (DCBX_IS_FCOE_OR_FIP_PROTOCOL (pAdmEntry->i4AppPriProtocol) ==
                OSIX_FALSE)
            {
                /* Get next Adm entry */
                pNextAdmEntry = TMO_SLL_Next (pAppPriAdmTbl,
                                              &(pAdmEntry->AppPriMappingEntry));
                pAdmEntry = pNextAdmEntry;
                continue;
            }

            /* Remote entries */
            pRemEntry = TMO_SLL_First (pAppPriRemTbl);
            while (pRemEntry != NULL)
            {
                if (DCBX_IS_FCOE_OR_FIP_PROTOCOL (pRemEntry->i4AppPriProtocol)
                    == OSIX_FALSE)
                {
                    /* Get the next remote entry */
                    pNextRemEntry = TMO_SLL_Next (pAppPriRemTbl,
                                                  &(pRemEntry->
                                                    AppPriMappingEntry));
                    pRemEntry = pNextRemEntry;
                    continue;
                }

                /* In case FCOE/FIP doesnot match */
                if (pAdmEntry->u1AppPriPriority != pRemEntry->u1AppPriPriority)
                {
                    return OSIX_FALSE;
                }
                /* Get the next remote entry */
                pNextRemEntry = TMO_SLL_Next (pAppPriRemTbl,
                                              &(pRemEntry->AppPriMappingEntry));
                pRemEntry = pNextRemEntry;
            }

            /* Get next Adm entry */
            pNextAdmEntry = TMO_SLL_Next (pAppPriAdmTbl,
                                          &(pAdmEntry->AppPriMappingEntry));
            pAdmEntry = pNextAdmEntry;
        }
    }

    return OSIX_TRUE;
}

/***************************************************************************
 *  FUNCTION NAME : AppPriCompareNonFcoeMappings 
 * 
 *  DESCRIPTION   : This function is used to check the compatibility between
 *                  local and the peer for Application Priority Feature
 * 
 *  INPUT         : pAdmEntry - AppPri Admin Table Entry.
 *                  pRemEntry - AppPri Remote Table Entry. 
 * 
 *  OUTPUT        : None
 * 
 *  RETURNS       : TRUE/FALSE
 * 
 * **************************************************************************/
BOOL1
AppPriCompareNonFcoeMappings (tTMO_SLL * pAppPriAdmTbl,
                              tTMO_SLL * pAppPriRemTbl)
{
    tAppPriMappingEntry *pRemEntry = NULL;
    tAppPriMappingEntry *pAdmEntry = NULL;
    tAppPriMappingEntry *pNextAdmEntry = NULL;
    tAppPriMappingEntry *pNextRemEntry = NULL;
    BOOL1               b1PriMatchFound = OSIX_FALSE;

    /* Adm entries */
    pAdmEntry = TMO_SLL_First (pAppPriAdmTbl);
    while (pAdmEntry != NULL)
    {
        if (DCBX_IS_FCOE_OR_FIP_PROTOCOL (pAdmEntry->i4AppPriProtocol) ==
            OSIX_TRUE)
        {
            /* Get next Adm entry */
            pNextAdmEntry = TMO_SLL_Next (pAppPriAdmTbl,
                                          &(pAdmEntry->AppPriMappingEntry));
            pAdmEntry = pNextAdmEntry;

            continue;
        }

        b1PriMatchFound = OSIX_FALSE;
        /* Remote entries */
        pRemEntry = TMO_SLL_First (pAppPriRemTbl);
        while (pRemEntry != NULL)
        {
            if (DCBX_IS_FCOE_OR_FIP_PROTOCOL (pRemEntry->i4AppPriProtocol) ==
                OSIX_TRUE)
            {
                /* Get the next remote entry */
                pNextRemEntry = TMO_SLL_Next (pAppPriRemTbl,
                                              &(pRemEntry->AppPriMappingEntry));
                pRemEntry = pNextRemEntry;
                continue;
            }

            /* In case FCOE/FIP doesnot match */
            if (pAdmEntry->i4AppPriProtocol == pRemEntry->i4AppPriProtocol)
            {
                if (pAdmEntry->u1AppPriPriority == pRemEntry->u1AppPriPriority)
                {
                    b1PriMatchFound = OSIX_TRUE;
                    break;
                }
                else
                {
                    /* Priorities not matched. */
                    return OSIX_FALSE;
                }
            }
            /* Get the next remote entry */
            pNextRemEntry = TMO_SLL_Next (pAppPriRemTbl,
                                          &(pRemEntry->AppPriMappingEntry));
            pRemEntry = pNextRemEntry;
        }

        if (b1PriMatchFound == OSIX_FALSE)
        {
            return OSIX_FALSE;
        }

        /* Get next Adm entry */
        pNextAdmEntry = TMO_SLL_Next (pAppPriAdmTbl,
                                      &(pAdmEntry->AppPriMappingEntry));
        pAdmEntry = pNextAdmEntry;
    }

    return OSIX_TRUE;
}

/************ END OF FILE ************/
