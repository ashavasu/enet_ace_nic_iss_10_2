/*************************************************************************
 * Copyright (C) 2010 Aricent Inc . All Rights Reserved
 * $Id: pfcutl.c,v 1.26 2017/08/24 11:59:43 siva Exp $
 * Description: This file contains PFC utility function.
****************************************************************************/
#include "dcbxinc.h"

/***************************************************************************
 *  FUNCTION NAME : PFCUtlPortTblCmpFn
 * 
 *  DESCRIPTION   : This utility function is used by RbTree for PFC port 
 *                  table.
 * 
 *  INPUT         : pRBElem1,pRBElem2 - RbTree Elements.
 * 
 *  OUTPUT        : None
 * 
 *  RETURNS       : LESSER/GREATER/EQUAL
 * 
 * **************************************************************************/
INT4
PFCUtlPortTblCmpFn (tRBElem * pRBElem1, tRBElem * pRBElem2)
{
    tPFCPortEntry      *pPFCPortEntry1 = NULL;
    tPFCPortEntry      *pPFCPortEntry2 = NULL;

    pPFCPortEntry1 = (tPFCPortEntry *) pRBElem1;
    pPFCPortEntry2 = (tPFCPortEntry *) pRBElem2;

    /* Compare the DCB Port Index */

    if (pPFCPortEntry1->u4IfIndex < pPFCPortEntry2->u4IfIndex)
    {
        return (DCBX_LESSER);
    }
    else if (pPFCPortEntry1->u4IfIndex > pPFCPortEntry2->u4IfIndex)
    {
        return (DCBX_GREATER);
    }

    return (DCBX_EQUAL);
}

/*****************************************************************************/
/* Function Name      : PFCGetPortEntry                                      */
/* Description        : Returns pointer to port entry                        */
/* Input(s)           : u4IfIndex                                            */
/* Output(s)          : None.                                                */
/* Return Value(s)    : Pointer to  PORT entry                               */
/*****************************************************************************/
tPFCPortEntry      *
PFCUtlGetPortEntry (UINT4 u4IfIndex)
{
    tPFCPortEntry       PortEntry;
    tPFCPortEntry      *pPortEntry = NULL;

    MEMSET (&PortEntry, DCBX_ZERO, sizeof (tPFCPortEntry));
    PortEntry.u4IfIndex = u4IfIndex;

    pPortEntry = (tPFCPortEntry *)
        RBTreeGet (gPFCGlobalInfo.pRbPFCPortTbl, (tRBElem *) & PortEntry);
    if (pPortEntry == NULL)
    {
        DCBX_TRC_ARG2 (DCBX_FAILURE_TRC,
                       "%s : Port Entry (Port) Id %dis invalid \r\n",
                       __FUNCTION__, u4IfIndex);
    }

    return (pPortEntry);
}

/*****************************************************************************/
/* Function Name      : PFCUtlCreatePortTblEntry                             */
/* Description        : This function is used to Create an Entry             */
/*                      it will do the following Action                      */
/*                      1. Allocate a memory block for the New Entry         */
/*                      2. Initialize the New Entry                          */
/*                      3. Add this New Entry into the PFC Port Table        */
/* Input(s)           : u4IfIndex      - Index to the PFC port Table         */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : gPFCGlobalInfo                                       */
/* Global Variables                                                          */
/* Modified           : gPFCGlobalInfo                                       */
/* Return Value(s)    : NULL / Pointer to  PFCPortEntry                      */
/* Called By          : SNMP Low Level                                       */
/* Calling Function   :                                                      */
/*****************************************************************************/
tPFCPortEntry      *
PFCUtlCreatePortTblEntry (UINT4 u4IfIndex)
{
    tPFCPortEntry      *pPFCPortEntry = NULL;
    UINT4               u4TcSupp = DCBX_ZERO;
    UINT1               u1DcbxMode = DCBX_MODE_AUTO;

    pPFCPortEntry = PFCUtlGetPortEntry (u4IfIndex);
    if (pPFCPortEntry != NULL)
    {
        DCBX_TRC_ARG2 (DCBX_CRITICAL_TRC,
                       "In %s : PFC Port Entry is already present "
                       "for specified port %d\r\n", __FUNCTION__, u4IfIndex);
        return (NULL);
    }

    /* 1. Allocate a memory block for the New Entry */
    pPFCPortEntry = (tPFCPortEntry *)
        MemAllocMemBlk (gPFCGlobalInfo.PFCPortPoolId);
    if (pPFCPortEntry == NULL)
    {
        DCBX_TRC_ARG2 ((DCBX_RESOURCE_TRC | DCBX_FAILURE_TRC |
                        DCBX_CRITICAL_TRC),
                       "In %s : MemAllocMemBlk () Failed ,"
                       "for PFC Specified Port is %d \r\n", __FUNCTION__,
                       u4IfIndex);
        return (NULL);
    }

    /* 2. Initialize the PFC Port Entry */
    MEMSET (pPFCPortEntry, DCBX_INIT_VAL, sizeof (tPFCPortEntry));
    pPFCPortEntry->u4IfIndex = u4IfIndex;

    /*Default PFC Status of the Port */
    pPFCPortEntry->u1PFCAdminMode = PFC_PORT_MODE_OFF;

    /*Default PFC Oper State of the Port */
    pPFCPortEntry->u1PFCDcbxOperState = PFC_OPER_OFF;

    DcbxUtlGetDCBXMode (u4IfIndex, &u1DcbxMode);
    if (u1DcbxMode == DCBX_MODE_CEE)
    {
        pPFCPortEntry->u1PFCDcbxSemType = DCBX_FEAT_STATE_MACHINE;
    }
    else if (u1DcbxMode == DCBX_MODE_IEEE)
    {
        pPFCPortEntry->u1PFCDcbxSemType = DCBX_SYM_STATE_MACHINE;
    }
    else
    {
        pPFCPortEntry->u1PFCDcbxSemType = DCBX_DEF_PFC_SM_TYPE;
    }
    /*Default Tx Status of the Port */
    pPFCPortEntry->u1PFCTxStatus = PFC_DISABLED;

    pPFCPortEntry->PFCAdmPortInfo.u1PFCAdmWilling = PFC_DISABLED;
    pPFCPortEntry->PFCAdmPortInfo.u1PFCAdmMBC = PFC_DISABLED;

    QosApiGetMaxTCSupport (pPFCPortEntry->u4IfIndex, &u4TcSupp);
    pPFCPortEntry->PFCAdmPortInfo.u1PFCAdmCap = (UINT1) u4TcSupp;

    pPFCPortEntry->PFCAdmPortInfo.u1PFCAdmStatus = DCBX_ZERO;

    pPFCPortEntry->u1CurrentState = CEE_ST_WAIT_FOR_FEAT_TLV;
    pPFCPortEntry->u1DcbxStatus = DCBX_STATUS_NOT_ADVERTISE;
    pPFCPortEntry->bPFCSyncd = PFC_DISABLED;
    pPFCPortEntry->bPFCError = PFC_DISABLED;
    pPFCPortEntry->bPFCPeerWilling = PFC_DISABLED;
    /* Copy the default paramters to the Local Info */

    MEMCPY (&(pPFCPortEntry->PFCLocPortInfo),
            &(pPFCPortEntry->PFCAdmPortInfo), sizeof (tPFCLocPortInfo));

    /* 3. Add this New Entry into the PFC Port  Table */
    if (RBTreeAdd (gPFCGlobalInfo.pRbPFCPortTbl,
                   (tRBElem *) pPFCPortEntry) != RB_SUCCESS)
    {
        MemReleaseMemBlock (gPFCGlobalInfo.PFCPortPoolId,
                            (UINT1 *) pPFCPortEntry);

        DCBX_TRC_ARG2 (DCBX_FAILURE_TRC | DCBX_CRITICAL_TRC,
                       "In %s : RBTreeAdd () Failed ," "for PFC port %d \r\n",
                       __FUNCTION__, u4IfIndex);
        return (NULL);
    }

    return (pPFCPortEntry);
}

/*****************************************************************************/
/* Function Name      : PFCUtlDeletePortTblEntry                             */
/* Description        : This function is used to Delete an Entry from the    */
/*                      PFC Port Table ,it will do the following Action      */
/*                      1. Remove the Entry from the PFC Port Table          */
/*                      2. Clear the Removed Entry                           */
/*                      3. Release the Removed Entry's memory                */
/* Input(s)           : pPFCPortEntry   - Pointer to  PFCPortEntry           */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : gPFCGlobalInfo                                       */
/* Global Variables                                                          */
/* Modified           : gPFCGlobalInfo                                       */
/* Return Value(s)    : OSIX_SUCCESS or OSIX_FAILURE                      */
/* Called By          : SNMP Low Level                                       */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
PFCUtlDeletePortTblEntry (tPFCPortEntry * pPFCPortEntry)
{
    /* Deregister all the application for this port with DCBX */
    PFCUtlRegOrDeRegBasedOnMode (pPFCPortEntry, PFC_DISABLED);

    /*Disable PFC Featue */
    pPFCPortEntry->u1PFCAdminMode = PFC_PORT_MODE_OFF;
    /*Change the state to OFF. This will take care of disabling
     * the feature in hardware */
    PFCUtlOperStateChange (pPFCPortEntry, PFC_OPER_OFF);

    /* Remove Entry from the PFC Port Table */
    RBTreeRemove (gPFCGlobalInfo.pRbPFCPortTbl, (UINT1 *) pPFCPortEntry);
    /* Release the Removed Entry's memory */
    MemReleaseMemBlock (gPFCGlobalInfo.PFCPortPoolId,
                        (UINT1 *) (pPFCPortEntry));
    return (OSIX_SUCCESS);
}

/***************************************************************************
 *  FUNCTION NAME : PFCUtlConstructPreFormTlv
 * 
 *  DESCRIPTION   : This utility function is used to form the PFC TLV
 *                  header.
 * 
 *  INPUT         : None
 * 
 *  OUTPUT        : None
 * 
 *  RETURNS       : None
 * 
 * **************************************************************************/
PUBLIC VOID
PFCUtlConstructPreFormTlv (VOID)
{
    UINT2               u2TlvHeader = DCBX_ZERO;
    UINT1              *pu1Temp = NULL;
    MEMSET (gau1PFCPreFormedTlv, DCBX_ZERO, sizeof (gau1PFCPreFormedTlv));

    /*Form TLV Header */
    DCBX_PUT_HEADER_LENGTH ((UINT2) PFC_TLV_TYPE, (UINT2) PFC_TLV_INFO_LEN,
                            &u2TlvHeader);
    /* put tlv header in  buffer */
    pu1Temp = gau1PFCPreFormedTlv;
    DCBX_PUT_2BYTE (pu1Temp, u2TlvHeader);
    DCBX_PUT_OUI (pu1Temp, gau1DcbxOUI);
    DCBX_PUT_1BYTE (pu1Temp, (UINT1) PFC_TLV_SUBTYPE);
    return;
}

/***************************************************************************
 *  FUNCTION NAME : PFCPortPostMsgToDCBX
 * 
 *  DESCRIPTION   : This utility function is used to post the TLV/Sem
 *                  Update message to DCBX.
 * 
 *  INPUT         : u1MsgType - Message type Reg/Update/Sem/DeReg.
 *                  u4IfIndex - Port Number.
 *                  pDcbxAppPortMsg - App Reg Info Structure
 * 
 *  OUTPUT        : None
 * 
 *  RETURNS       : None
 * 
 * **************************************************************************/
PUBLIC VOID
PFCPortPostMsgToDCBX (UINT1 u1MsgType, UINT4 u4IfIndex,
                      tDcbxAppRegInfo * pDcbxAppPortMsg)
{
    switch (u1MsgType)
    {
        case DCBX_LLDP_PORT_UPDATE:
            /* Used to send the TLV updated information to the LDDP using 
             * DCBX function */
            if (DcbxApiApplPortUpdate (u4IfIndex, pDcbxAppPortMsg)
                != OSIX_SUCCESS)
            {
                DCBX_TRC (DCBX_TLV_TRC | DCBX_FAILURE_TRC,
                          "PFCPortPostMsgToDCBX:"
                          "DCBX Port TLV Update Failed !!!\r\n");
            }
            break;

        case DCBX_LLDP_PORT_REG:
            /* Used to Register the application to the LDDP using 
             * DCBX function */
            if (DcbxApiApplPortReg (u4IfIndex, pDcbxAppPortMsg) != OSIX_SUCCESS)
            {
                DCBX_TRC (DCBX_TLV_TRC | DCBX_FAILURE_TRC,
                          "PFCPortPostMsgToDCBX:"
                          "DCBX Port Registration Failed !!!\r\n");
            }
            break;

        case DCBX_LLDP_PORT_DEREG:
            /* Used to De-Register the application from the LDDP using 
             * DCBX function */
            if (DcbxApiApplPortDeReg (u4IfIndex, pDcbxAppPortMsg)
                != OSIX_SUCCESS)
            {
                DCBX_TRC (DCBX_TLV_TRC | DCBX_FAILURE_TRC,
                          "PFCPortPostMsgToDCBX:"
                          "DCBX Port DeRegistration Failed !!!\r\n");
            }
            break;

        case DCBX_STATE_MACHINE:
            /* Used to run the DCBX state machine and get the 
             * necessary Operational state  */
            DCBX_TRC (DCBX_SEM_TRC,
                      "PFCPortPostMsgToDCBX:"
                      "PFC Port SEM update request to DCBX!!!\r\n");
            DcbxSemUpdate (u4IfIndex, pDcbxAppPortMsg);
            break;

        default:
            DCBX_TRC (DCBX_FAILURE_TRC,
                      "PFCPortPostMsgToDCBX:"
                      "Invalid Message Type Received !!!\r\n");
            break;
    }
    return;
}

/***************************************************************************
 *  FUNCTION NAME : PFCUtlHandleReRegReqFromDCBX
 * 
 *  DESCRIPTION   : This utility function is used to Handle the Re Reg
 *                  Message from DCBX.
 * 
 *  INPUT         : pPortEntry - PFC Port Entry.
 * 
 *  OUTPUT        : None
 * 
 *  RETURNS       : None
 * 
 * **************************************************************************/
PUBLIC VOID
PFCUtlHandleReRegReqFromDCBX (tPFCPortEntry * pPortEntry)
{
    /* For Re-Registration, Form the TLV and send it with
     * Port Registration request to the DCBX */
    PFCUtlFormAndSendPFCTLV (pPortEntry, DCBX_LLDP_PORT_REG);
    return;

}

/***************************************************************************
 *  FUNCTION NAME : PFCUtlHandleAgedOutFromDCBX
 * 
 *  DESCRIPTION   : This function is used to handle the PFC TLV Age Out
 *                  Message from DCBX.
 * 
 *  INPUT         : pPortEntry - PFC Port Entry.
 * 
 *  OUTPUT        : None
 * 
 *  RETURNS       : None
 * 
 * **************************************************************************/
PUBLIC VOID
PFCUtlHandleAgedOutFromDCBX (tPFCPortEntry * pPortEntry)
{
    tDcbxTrapMsg        NotifyInfo;

    MEMSET (&NotifyInfo, DCBX_ZERO, sizeof (tDcbxTrapMsg));
    /* If Age out message is recieved then move the state to INIT 
     * and clear the Remote Parameters */
    DCBX_TRC (DCBX_SEM_TRC, "PFCUtlHandleAgedOutFromDCBX:"
              " PFC Age Out Event is received. Change the sate to INIT!!!\r\n");
    SET_DCBX_STATUS (pPortEntry, pPortEntry->u1PFCDcbxSemType,
                     DCBX_RED_PFC_DCBX_STATUS_INFO,
                     DCBX_STATUS_PEER_NO_ADV_FEAT);

    PFCUtlOperStateChange (pPortEntry, PFC_OPER_INIT);
    /* Send State Change information to Standby Node.
     * so that if it is RECO state it will be moved to
     * INIT in standby also.*/
    DcbxRedSendDynamicPfcSemInfo (pPortEntry);

    MEMSET (&(pPortEntry->PFCRemPortInfo), DCBX_ZERO, sizeof (tPFCRemPortInfo));
    /* Send Remote table Clear information to Standby Node */
    DcbxRedSendDynamicPfcRemInfo (pPortEntry);

    /* Send Trap Notification for Peer Down status */
    NotifyInfo.u4IfIndex = pPortEntry->u4IfIndex;
    NotifyInfo.u1Module = PFC_TRAP;
    NotifyInfo.unTrapMsg.u1PeerStatus = PFC_DISABLED;

    DcbxSendNotification (&NotifyInfo, PEER_STATE_TRAP);

    return;
}

/***************************************************************************
 *  FUNCTION NAME : PFCUtlDeleteAllPorts
 * 
 *  DESCRIPTION   : This utility function is used to delete all 
 *                  the PFC ports at the time of shutodwn.
 * 
 *  INPUT         : None
 * 
 *  OUTPUT        : None
 * 
 *  RETURNS       : None
 * 
 * **************************************************************************/
PUBLIC VOID
PFCUtlDeleteAllPorts (VOID)
{
    tPFCPortEntry       TempPortEntry;
    tPFCPortEntry      *pPortEntry = NULL;

    MEMSET (&TempPortEntry, DCBX_ZERO, sizeof (tPFCPortEntry));

    /* Get the First port entry and delete the port entry if present */
    pPortEntry = (tPFCPortEntry *) RBTreeGetFirst
        (gPFCGlobalInfo.pRbPFCPortTbl);
    if (pPortEntry != NULL)
    {
        do
        {
            /* Delete the PFC port table entry */
            TempPortEntry.u4IfIndex = pPortEntry->u4IfIndex;
            PFCUtlDeletePortTblEntry (pPortEntry);
            pPortEntry = (tPFCPortEntry *) RBTreeGetNext
                (gPFCGlobalInfo.pRbPFCPortTbl, &TempPortEntry,
                 PFCUtlPortTblCmpFn);
            /* Process for all the ports in the port RB Tree */
        }
        while (pPortEntry != NULL);
    }
    return;
}

/***************************************************************************
 *  FUNCTION NAME : PFCUtlDeletePort
 * 
 *  DESCRIPTION   : This utility function is used to delete the PFC Port.
 * 
 *  INPUT         : u4IfIndex - Port Number
 * 
 *  OUTPUT        : None
 * 
 *  RETURNS       : None
 * 
 * **************************************************************************/
PUBLIC VOID
PFCUtlDeletePort (UINT4 u4IfIndex)
{
    tPFCPortEntry      *pPortEntry = NULL;
    pPortEntry = PFCUtlGetPortEntry (u4IfIndex);
    /* Get the PFC port entry */
    if (pPortEntry == NULL)
    {
        DCBX_TRC (DCBX_CONTROL_PLANE_TRC, "PFCUtlDeletePort:"
                  " No PFC Port is created!!!\r\n");
        return;
    }
    /* Deletes this PFC port entry */
    PFCUtlDeletePortTblEntry (pPortEntry);
    return;
}

/***************************************************************************
 *  FUNCTION NAME : PFCUtlRegisterApplForPort
 * 
 *  DESCRIPTION   : This utility function is used to register the Appl
 *                  with DCBX.
 * 
 *  INPUT         : pPortEntry - PFC Port Entry.
 * 
 *  OUTPUT        : None
 * 
 *  RETURNS       : None
 * 
 * **************************************************************************/
/* This will be called from the TLV tx En and Module En and Admin mode 
 * changeto Auto*/
PUBLIC VOID
PFCUtlRegisterApplForPort (tPFCPortEntry * pPortEntry)
{
    /* If Module is not enabled then no need to register the application
     * with DCBX and it will registered for all ports which require 
     * registration when the Module status is enabled */
    if (gPFCGlobalInfo.u1PFCModStatus != PFC_ENABLED)
    {
        DCBX_TRC (DCBX_CONTROL_PLANE_TRC, "PFCUtlRegisterApplForPort:"
                  " No Registration Required as the Module is"
                  "not enabled!!!\r\n");
        return;
    }
    /* When the Admin mode of the port is not set to Auto then no need to 
     * register any application with DCBX since PFC is not ready to operate
     * through DCBX and it can take the local configuration with ON Mode */
    if (pPortEntry->u1PFCAdminMode != PFC_PORT_MODE_AUTO)
    {
        DCBX_TRC (DCBX_CONTROL_PLANE_TRC, "PFCUtlRegisterApplForPort:"
                  " No Registration Required as the Mode is not Auto!!!\r\n");
        return;
    }
    /* Form and send the TC Supp TLV with Port registration request only if
     * TLV Tx status is enabled  */
    if (pPortEntry->u1PFCTxStatus != PFC_DISABLED)
    {
        DCBX_TRC (DCBX_TLV_TRC, "PFCUtlRegisterApplForPort:"
                  " Forming and  Sending PFC TLV !!!\r\n");
        PFCUtlFormAndSendPFCTLV (pPortEntry, DCBX_LLDP_PORT_REG);
    }

    SET_DCBX_STATUS (pPortEntry, pPortEntry->u1PFCDcbxSemType,
                     DCBX_RED_PFC_DCBX_STATUS_INFO,
                     DCBX_STATUS_PEER_NO_ADV_DCBX);
    return;

}

/***************************************************************************
 *  FUNCTION NAME : PFCUtlDeRegisterApplForPort
 * 
 *  DESCRIPTION   : This utility is used to De Register the Appl with DCBX.
 * 
 *  INPUT         : pPortEntry - PFC Port Entry.
 * 
 *  OUTPUT        : None
 * 
 *  RETURNS       : None
 * 
 * **************************************************************************/
/* This will be called from the TLV tx Dis and #Module Dis and port 
 * Delete and Admin mode change to Dis/On */
PUBLIC VOID
PFCUtlDeRegisterApplForPort (tPFCPortEntry * pPortEntry)
{
    tDcbxAppRegInfo     DcbxAppPortMsg;
    /* If Module is not enabled then no need to De-register the application
     * with DCBX and it should have already De-registered for all ports during 
     * the module disable */
    if (gPFCGlobalInfo.u1PFCModStatus != PFC_ENABLED)
    {
        DCBX_TRC (DCBX_CONTROL_PLANE_TRC, "PFCUtlDeRegisterApplForPort:"
                  " No DeRegistration Required as the Module is "
                  "not enabled!!!\r\n");
        return;
    }
    /* When the Admin mode of the port is not set to Auto then no need to 
     * De-register any application with DCBX since PFC is not operating
     * in the DCBX AUTO mode */
    if (pPortEntry->u1PFCAdminMode != PFC_PORT_MODE_AUTO)
    {
        DCBX_TRC (DCBX_CONTROL_PLANE_TRC, "PFCUtlDeRegisterApplForPort:"
                  " No DeRegistration Required as the Mode is not Auto!!!\r\n");
        return;
    }

    /* De-Registers the PFC TLV with DCBX if TLV Tx status is enabled,
     * since if it is not enabled then it would not have registered with DCBX */
    if (pPortEntry->u1PFCTxStatus != PFC_DISABLED)
    {
        MEMSET (&DcbxAppPortMsg, DCBX_ZERO, sizeof (tDcbxAppRegInfo));
        PFC_GET_APPL_ID (DcbxAppPortMsg.DcbxAppId);
        DCBX_TRC (DCBX_TLV_TRC, "PFCUtlDeRegisterApplForPort:"
                  " Sending TLV DeRegistration Message!!!\r\n");
        PFCPortPostMsgToDCBX (DCBX_LLDP_PORT_DEREG, pPortEntry->u4IfIndex,
                              &DcbxAppPortMsg);
    }

    /* Clearing the remote parameter after Deregistration */
    MEMSET (&(pPortEntry->PFCRemPortInfo), DCBX_ZERO, sizeof (tPFCRemPortInfo));
    return;
}

/***************************************************************************
 *  FUNCTION NAME : PFCUtlModuleStatusChange
 * 
 *  DESCRIPTION   : This utility is used to handle the Module status change.
 * 
 *  INPUT         : u1Status - Module status -En/Dis
 * 
 *  OUTPUT        : None
 * 
 *  RETURNS       : None
 * 
 * **************************************************************************/
PUBLIC VOID
PFCUtlModuleStatusChange (UINT1 u1Status)
{
    tPFCPortEntry       TempPortEntry;
    tPFCPortEntry      *pPortEntry = NULL;
    tDcbxTrapMsg        NotifyInfo;

    MEMSET (&NotifyInfo, DCBX_ZERO, sizeof (tDcbxTrapMsg));
    MEMSET (&TempPortEntry, DCBX_ZERO, sizeof (tPFCPortEntry));

    pPortEntry = (tPFCPortEntry *) RBTreeGetFirst
        (gPFCGlobalInfo.pRbPFCPortTbl);
    /* Get all the PFC port entry and process the module status chage */
    if (pPortEntry != NULL)
    {
        /* If Module status is to be enabled then first change the status and 
         * and then process since Registration with DCBX and State change occurs 
         * only if the module status is enabled */
        if (u1Status == PFC_ENABLED)
        {
            gPFCGlobalInfo.u1PFCModStatus = PFC_ENABLED;
        }
        do
        {
            /* Register or DeRegister the PFC applications with DCBX for all
             * the port and change the state to INIT or OFF */
            if (u1Status == PFC_ENABLED)
            {
                PFCUtlRegOrDeRegBasedOnMode (pPortEntry, u1Status);
                if (pPortEntry->u1PFCAdminMode != PFC_PORT_MODE_OFF)
                {
                    DCBX_TRC (DCBX_SEM_TRC, "PFCUtlModuleStatusChange:"
                              "Module Enable Changing the state to"
                              "INIT!!!\r\n");
                    PFCUtlOperStateChange (pPortEntry, PFC_OPER_INIT);
                }
            }
            else
            {
                /* Refer to the end of the function for the need of below line */
                DCBX_TRC (DCBX_SEM_TRC, "PFCUtlModuleStatusChange:"
                          "Module Disable Changing the state to OFF!!!\r\n");
                PFCUtlOperStateChange (pPortEntry, PFC_OPER_OFF);
                PFCUtlRegOrDeRegBasedOnMode (pPortEntry, u1Status);
            }

            TempPortEntry.u4IfIndex = pPortEntry->u4IfIndex;
            pPortEntry = (tPFCPortEntry *) RBTreeGetNext
                (gPFCGlobalInfo.pRbPFCPortTbl,
                 &TempPortEntry, PFCUtlPortTblCmpFn);
        }
        while (pPortEntry != NULL);
    }
    else
    {
        DCBX_TRC (DCBX_CONTROL_PLANE_TRC, "PFCUtlModuleStatusChange:"
                  " No PFC Port is created!!!\r\n");
    }
    /* Update the Module Status Global Value */
    gPFCGlobalInfo.u1PFCModStatus = u1Status;

    /* Send Module status change Notification */
    NotifyInfo.u1Module = PFC_TRAP;
    NotifyInfo.unTrapMsg.u1ModuleStatus = gPFCGlobalInfo.u1PFCModStatus;

    DcbxSendNotification (&NotifyInfo, MODULE_TRAP);

    return;
}

/***************************************************************************
 *  FUNCTION NAME : PFCUtlAdminModeChange
 * 
 *  DESCRIPTION   : This utility is used to Handle the Admin Mode change.
 * 
 *  INPUT         : pPortEntry - PFC Port Entry
 *                  u1NewAdminMode - Admin Mode Value.
 * 
 *  OUTPUT        : None
 * 
 *  RETURNS       : None
 * 
 * **************************************************************************/
PUBLIC VOID
PFCUtlAdminModeChange (tPFCPortEntry * pPortEntry, UINT1 u1NewAdminMode)
{
    tDcbxTrapMsg        NotifyInfo;

    MEMSET (&NotifyInfo, DCBX_ZERO, sizeof (tDcbxTrapMsg));

    if (pPortEntry->u1PFCAdminMode == PFC_PORT_MODE_AUTO)
    {
        /* This check is to ensure the Deregistration happens only in
         * AUTO Mode. So Deregistartion of TLV happens before
         * updating the Strucuture */
        PFCUtlRegOrDeRegBasedOnMode (pPortEntry, PFC_DISABLED);

        pPortEntry->u1PFCAdminMode = u1NewAdminMode;
    }
    else
    {
        pPortEntry->u1PFCAdminMode = u1NewAdminMode;
    }
    if (u1NewAdminMode == PFC_PORT_MODE_AUTO)
    {
        /* If the NewAdmin mode is AUTO then Register the PFC 
         *   applications with DCBX and change the state to INIT */
        PFCUtlRegOrDeRegBasedOnMode (pPortEntry, PFC_ENABLED);

        DCBX_TRC (DCBX_SEM_TRC, "PFCUtlAdminModeChange:"
                  "Port Mode changing to Auto, Change the"
                  "state to INIT!!!\r\n");
        PFCUtlOperStateChange (pPortEntry, PFC_OPER_INIT);
    }
    else if (u1NewAdminMode == PFC_PORT_MODE_ON)
    {
        /* If the NewAdmin mode is ON then change the state to INIT */
        DCBX_TRC (DCBX_SEM_TRC, "PFCUtlAdminModeChange:"
                  "Port Mode changing to ON, Change the state to INIT!!!\r\n");
        PFCUtlOperStateChange (pPortEntry, PFC_OPER_INIT);
    }
    else
    {
        /* If the NewAdmin mode is OFF then chage the state to OFF
         * i.e Disable the hardware configurations */
        DCBX_TRC (DCBX_SEM_TRC, "PFCUtlAdminModeChange:"
                  "Port Mode changing to OFF, Change the state to OFF!!!\r\n");
        PFCUtlOperStateChange (pPortEntry, PFC_OPER_OFF);
        SET_DCBX_STATUS (pPortEntry, pPortEntry->u1PFCDcbxSemType,
                         DCBX_RED_PFC_DCBX_STATUS_INFO, DCBX_STATUS_DISABLED);
    }
    /* Send Trap Notification */
    NotifyInfo.u4IfIndex = pPortEntry->u4IfIndex;
    NotifyInfo.u1Module = PFC_TRAP;
    NotifyInfo.unTrapMsg.u1AdminMode = pPortEntry->u1PFCAdminMode;

    DcbxSendNotification (&NotifyInfo, ADMIN_STATUS_TRAP);

    return;
}

/***************************************************************************
 *  FUNCTION NAME : PFCUtlOperStateChange
 * 
 *  DESCRIPTION   : This utility is used to Change the oper state.
 * 
 *  INPUT         : pPortEntry - PFC port Entry.
 *                  u1PFCState - PFC Oper State.
 * 
 *  OUTPUT        : None
 * 
 *  RETURNS       : None
 * 
 * **************************************************************************/
PUBLIC VOID
PFCUtlOperStateChange (tPFCPortEntry * pPortEntry, UINT1 u1PFCState)
{
    /* State change should occur only if Module status is enabled. If module 
     * status is disabled then the state will be always OFF */
    if (gPFCGlobalInfo.u1PFCModStatus != PFC_ENABLED)
    {
        DCBX_TRC (DCBX_SEM_TRC, "PFCUtlOperStateChange:"
                  "Module status is disbaled. So State change will not happen"
                  "It will be in OFF state !!!\r\n");
        return;
    }
    /* If the Old Oper state and New Oper state are same then no need
     * to process further */
    if ((pPortEntry->u1PFCDcbxOperState == u1PFCState) &&
        (pPortEntry->u1PFCDcbxOperState != PFC_OPER_RECO))
    {
        DCBX_TRC (DCBX_SEM_TRC, "PFCUtlOperStateChange:"
                  "Both old and new state are same !!!\r\n");
        return;
    }
    /* Update the Local Parameters which are operation parameters
     * according to the state */
    DCBX_TRC (DCBX_SEM_TRC, "PFCUtlOperStateChange:"
              "Updating the Local Paramters based on the state !!!\r\n");
    PFCUtlUpdateLocalParam (pPortEntry, u1PFCState);

    return;
}

/***************************************************************************
 *  FUNCTION NAME : PFCUtlUpdateLocalParam
 * 
 *  DESCRIPTION   : This utility is used to update the local parameter.
 * 
 *  INPUT         : pPortEntry - PFC Port Entry.
 *                  u1PFCState - PFC oper State.
 * 
 *  OUTPUT        : None
 * 
 *  RETURNS       : None
 * 
 * **************************************************************************/
PUBLIC INT4
PFCUtlUpdateLocalParam (tPFCPortEntry * pPortEntry, UINT1 u1PFCState)
{
    tDcbxTrapMsg        NotifyInfo;
    INT4                i4RetVal = OSIX_SUCCESS;
    UINT1               u1NewPfcProfile = DCBX_ZERO;

    MEMSET (&NotifyInfo, DCBX_ZERO, sizeof (tDcbxTrapMsg));
    /* First Call the hardware with the new profile (i.e pfc status for all
     * priorities) according to the state without changing the local param.
     * If the profile creation is success in the hardware then update
     * the local paramter and Oper State else retain the old state */
    if (u1PFCState == PFC_OPER_INIT)
    {
        u1NewPfcProfile = pPortEntry->PFCAdmPortInfo.u1PFCAdmStatus;
        i4RetVal =
            PFCUtlConfigProfile (pPortEntry, u1NewPfcProfile, PFC_ENABLED);
    }
    else if (u1PFCState == PFC_OPER_RECO)
    {
        u1NewPfcProfile = pPortEntry->PFCRemPortInfo.u1PFCRemStatus;
        i4RetVal =
            PFCUtlConfigProfile (pPortEntry, u1NewPfcProfile, PFC_ENABLED);
    }
    else
    {
        i4RetVal =
            PFCUtlConfigProfile (pPortEntry, u1NewPfcProfile, PFC_DISABLED);
    }
    /* If Hardware configuration success then update oper state and local
     * parameters. */
    if (i4RetVal == OSIX_SUCCESS)
    {
        pPortEntry->u1PFCDcbxOperState = u1PFCState;
        if ((u1PFCState == PFC_OPER_INIT) || (u1PFCState == PFC_OPER_OFF))
        {
            /* If the state is either INIT or OFF then local parameters will be 
             * same as Admin configuration parameters */

            pPortEntry->PFCLocPortInfo.u1PFCLocCap =
                pPortEntry->PFCAdmPortInfo.u1PFCAdmCap;
            pPortEntry->PFCLocPortInfo.u1PFCLocMBC =
                pPortEntry->PFCAdmPortInfo.u1PFCAdmMBC;
            pPortEntry->PFCLocPortInfo.u1PFCLocStatus =
                pPortEntry->PFCAdmPortInfo.u1PFCAdmStatus;

        }
        else
        {
            /* If the state is RX then local parametrs will be same as
             * Remote parameters */

            pPortEntry->PFCLocPortInfo.u1PFCLocCap =
                pPortEntry->PFCRemPortInfo.u1PFCRemCap;
            pPortEntry->PFCLocPortInfo.u1PFCLocMBC =
                pPortEntry->PFCRemPortInfo.u1PFCRemMBC;
            pPortEntry->PFCLocPortInfo.u1PFCLocStatus =
                pPortEntry->PFCRemPortInfo.u1PFCRemStatus;

        }
        /* Send Oper State change Notification */
        NotifyInfo.u4IfIndex = pPortEntry->u4IfIndex;
        NotifyInfo.u1Module = PFC_TRAP;
        NotifyInfo.unTrapMsg.u1OperState = pPortEntry->u1PFCDcbxOperState;
        DcbxSendNotification (&NotifyInfo, OPER_STATE_TRAP);

        return OSIX_SUCCESS;
    }
    DCBX_TRC (DCBX_SEM_TRC | DCBX_FAILURE_TRC, "PFCUtlUpdateLocalParam:"
              "While updating the Local paramters, Programming the "
              "hardware FAILED !!!\r\n");
    return OSIX_FAILURE;
}

/***************************************************************************
 *  FUNCTION NAME : PFCUtlTlvStatusChange
 * 
 *  DESCRIPTION   : This utility is used to handle the PFC TLV Tx status 
 *                  change.
 * 
 *  INPUT         : pPortEntry - PFC Port Entry.
 *                  u1NewTlvStatus - TLV Tx Status.
 * 
 *  OUTPUT        : None
 * 
 *  RETURNS       : None
 * 
 * **************************************************************************/
PUBLIC VOID
PFCUtlTlvStatusChange (tPFCPortEntry * pPortEntry, UINT1 u1NewTlvStatus)
{
    if (u1NewTlvStatus == PFC_ENABLED)
    {
        /* If the TLV TX new status is enabled then update 
         * the data structure and send the Registration request
         * with DCBX since Registration will happen only if
         * TX status is enabled */
        pPortEntry->u1PFCTxStatus = PFC_ENABLED;

        /* Here State change will be taken care by
         * TLV recieve. Initailly it will be in INIT state.
         * On receive of TLV update then it chage the state
         * according to the local willing and state machine 
         * oper state */

        PFCUtlRegOrDeRegBasedOnMode (pPortEntry, u1NewTlvStatus);
    }
    else
    {
        /* Deregister the application with DCBX and change the 
         * TLV Tx status since deregistration happnes only if
         * the Tx status is already enabled. Change the state 
         * to INIT if the state is Reco */

        PFCUtlRegOrDeRegBasedOnMode (pPortEntry, u1NewTlvStatus);

        if (pPortEntry->u1PFCDcbxOperState == PFC_OPER_RECO)
        {
            DCBX_TRC (DCBX_SEM_TRC, "PFCUtlTlvStatusChange:"
                      "Since TLV Tx status is disbaled, changing the state"
                      "to INIT from RECO  !!!\r\n");
            PFCUtlOperStateChange (pPortEntry, PFC_OPER_INIT);
        }
        pPortEntry->u1PFCTxStatus = PFC_DISABLED;
    }
    return;
}

/***************************************************************************
 *  FUNCTION NAME : PFCUtlAdminParamChange
 * 
 *  DESCRIPTION   : This utility is used to handle the Admin Parameters
 *                  Change. It will indicate to the DCBX if Admin mode is 
 *                  Auto.
 * 
 *  INPUT         : pPortEntry - PFC Port Entry.
 * 
 *  OUTPUT        : None
 * 
 *  RETURNS       : None
 * 
 * **************************************************************************/
PUBLIC INT4
PFCUtlAdminParamChange (tPFCPortEntry * pPortEntry)
{
    UINT1               u1DcbxVersion = DCBX_VER_UNKNOWN;

    /* If Module status is disabled or Mode is OFF then no need to send 
     * the Admin Param change indication to the DCBX since the 
     * application itself would have Registered with DCBX during 
     * the Module Disable.But need to update local param */

    DcbxUtlGetOperVersion (pPortEntry->u4IfIndex, &u1DcbxVersion);
    if (pPortEntry->u1PFCDcbxOperState == PFC_OPER_OFF)
    {
        pPortEntry->PFCLocPortInfo.u1PFCLocStatus =
            pPortEntry->PFCAdmPortInfo.u1PFCAdmStatus;
        if (u1DcbxVersion == DCBX_VER_IEEE)
        {
            /* If the operating version is IEEE and if the mode is OFF
             * no need to send the updated TLV hence return from here*/
            return OSIX_SUCCESS;
        }
    }

    /* If Oper state is Reco then no need to update the local paramters 
     * as well as hardware since it will take remote parameters.
     * If Oper state is in INIT then update the Local paramters */
    if (pPortEntry->u1PFCDcbxOperState == PFC_OPER_INIT)
    {
        if (PFCUtlUpdateLocalParam (pPortEntry, PFC_OPER_INIT) == OSIX_FAILURE)
        {
            DCBX_TRC (DCBX_FAILURE_TRC, "PFCUtlAdminParamChange:"
                      "Updating Local Parameter FAILED!!!\r\n");
            return OSIX_FAILURE;
        }
    }

    /* If the Admin mode is AUTO then need to send the Admin Param
     * change indication to the DCBX with the TLV Updates  */

    /* If the TLV Tx status is not enabled then no need to send 
     * the Admin Param change indication to DCBX*/
    if ((pPortEntry->u1PFCAdminMode == PFC_PORT_MODE_AUTO) &&
        (pPortEntry->u1PFCTxStatus != PFC_DISABLED))
    {
        if (u1DcbxVersion == DCBX_VER_CEE)
        {
            DCBX_TRC_ARG1 (DCBX_TLV_TRC, "PFCUtlAdminParamChange: "
                           "Change in PFC dmin params for port[%d]. "
                           "Form and Send the CEE TLV!!!\r\n",
                           pPortEntry->u4IfIndex);

            CEEFormAndSendTLV (pPortEntry->u4IfIndex, DCBX_LLDP_PORT_UPDATE,
                               DCBX_ZERO);
        }
        else if (u1DcbxVersion == DCBX_VER_IEEE)
        {
            DCBX_TRC (DCBX_TLV_TRC, "PFCUtlAdminParamChange:"
                      "Forming and Sending the PFC TLV!!!\r\n");
            PFCUtlFormAndSendPFCTLV (pPortEntry, DCBX_LLDP_PORT_UPDATE);
        }
    }
    return OSIX_SUCCESS;
}

/***************************************************************************
 *  FUNCTION NAME : PFCUtlWillingChange
 * 
 *  DESCRIPTION   : This utility is used to handle the willing parameter
 *                  change.
 * 
 *  INPUT         : pPortEntry - PFC Port Entry.
 *                  u1IsLoca1Willing - Whether Local willing change or not.
 *                  If local willing then send the updated TLV.
 * 
 *  OUTPUT        : None
 * 
 *  RETURNS       : None
 * 
 * **************************************************************************/
PUBLIC VOID
PFCUtlWillingChange (tPFCPortEntry * pPortEntry, UINT1 u1IsLocalWilling)
{
    /* This will be called whenever there is a change in 
     * Remote/Local Willing Status */
    tDcbxAppRegInfo     DcbxAppPortMsg;
    UINT1               u1DcbxVersion = DCBX_VER_UNKNOWN;
    UINT1               u1OperState = DCBX_ZERO;
    UINT1               u1IsCompatible = DCBX_ZERO;

    MEMSET (&DcbxAppPortMsg, DCBX_ZERO, sizeof (tDcbxAppRegInfo));

    /* If Module status is disbaled then no need to send the 
     * state machine chage request to DCBX since remote paramters 
     * will not be present and the application would not have registered
     * with DCBX in Module Disable state*/
    if (gPFCGlobalInfo.u1PFCModStatus != PFC_ENABLED)
    {
        DCBX_TRC (DCBX_SEM_TRC, "PFCUtlWillingChange:"
                  " No State machine change required as the "
                  "Module is not enabled!!!\r\n");
        return;
    }
    /* If the Admin mode is not AUTO then no need to send the state
     * machine chage request since it does not operate on DCBX */
    if (pPortEntry->u1PFCAdminMode != PFC_PORT_MODE_AUTO)
    {
        DCBX_TRC (DCBX_SEM_TRC, "PFCUtlWillingChange:"
                  " No State machine change required as the"
                  " Mode is not Auto!!!\r\n");
        return;
    }
    /* If TLV Tx status is not enabled then no need to send the state
     * machine chage since it wouold have registered with DCBX */
    if (pPortEntry->u1PFCTxStatus != PFC_ENABLED)
    {
        DCBX_TRC (DCBX_CONTROL_PLANE_TRC, "PFCUtlWillingChange:"
                  "PFC TLV tranmsission status is not enabled."
                  "No SEM change request is needed!!!\r\n");
        return;
    }

    DcbxUtlGetOperVersion (pPortEntry->u4IfIndex, &u1DcbxVersion);
    if (u1DcbxVersion == DCBX_VER_IEEE)
    {
        /* If remote entry or Remote willing is not present 
         * then the state will be always INIT.
         * No need to state machine change request */
        if (pPortEntry->PFCRemPortInfo.u1PFCRemWilling != DCBX_ZERO)
        {
            DCBX_TRC (DCBX_SEM_TRC, "PFCUtlWillingChange:"
                      " Sending State machine change request message"
                      "to DCBX !!!\r\n");
            /* Fill the Information to be sent to DCBX */
            PFC_GET_APPL_ID (DcbxAppPortMsg.DcbxAppId);
            MEMCPY (DcbxAppPortMsg.au1RemDestMacAddr,
                    pPortEntry->PFCRemPortInfo.au1PFCDestRemMacAddr,
                    MAC_ADDR_LEN);

            DcbxAppPortMsg.u4RemLocalDestMACIndex =
                pPortEntry->PFCRemPortInfo.u4PFCDestRemMacIndex;

            DcbxAppPortMsg.u1ApplLocalWilling =
                pPortEntry->PFCLocPortInfo.u1PFCLocWilling;
            DcbxAppPortMsg.u1ApplRemoteWilling =
                pPortEntry->PFCRemPortInfo.u1PFCRemWilling;
            DcbxAppPortMsg.u1ApplStateMachineType = DCBX_SYM_STATE_MACHINE;
            /* Post the mesage to DCBX for State machine chage request */
            PFCPortPostMsgToDCBX (DCBX_STATE_MACHINE, pPortEntry->u4IfIndex,
                                  &DcbxAppPortMsg);
        }
        /* If there is a change in local willing then send the updated TLV */
        if (u1IsLocalWilling == OSIX_TRUE)
        {
            DCBX_TRC (DCBX_TLV_TRC, "PFCUtlWillingChange:"
                      "Forming and sending PFC TLV since Local willing status"
                      "has been changed !!!\r\n");
            /* Send the TLV Update since Local Willing has been changed */
            PFCUtlFormAndSendPFCTLV (pPortEntry, DCBX_LLDP_PORT_UPDATE);
        }
    }
    else if (u1DcbxVersion == DCBX_VER_CEE)
    {
        if (pPortEntry->u1DcbxStatus == DCBX_STATUS_CFG_NOT_COMPT)
        {
            u1IsCompatible = OSIX_FALSE;
        }
        else
        {
            u1IsCompatible = OSIX_TRUE;
        }

        DCBX_TRC_ARG3 (DCBX_TLV_TRC, "PFCUtlWillingChange: "
                       "CEE: Calculate Oper state "
                       "RemoteWilling = %d, LocalWilling = %d Compatible = %d\r\n",
                       pPortEntry->PFCRemPortInfo.u1PFCRemWilling,
                       u1IsLocalWilling, u1IsCompatible);
        u1OperState =
            CEEUtlCalculateOperState (pPortEntry->PFCRemPortInfo.
                                      u1PFCRemWilling, u1IsLocalWilling,
                                      (BOOL1) u1IsCompatible);
        if ((u1OperState != pPortEntry->u1PFCDcbxOperState)
            || (u1IsLocalWilling == OSIX_TRUE))
        {
            DCBX_TRC_ARG1 (DCBX_TLV_TRC, "PFCUtlWillingChange: "
                           "Form and send CEE TLV since Local Oper state"
                           "(or) Local willing status has been changed for port [%d]!!!\r\n",
                           pPortEntry->u4IfIndex);
            /* Send the TLV Update since Local Willing has been changed */
            CEEFormAndSendTLV (pPortEntry->u4IfIndex, DCBX_LLDP_PORT_UPDATE,
                               DCBX_ZERO);
        }
    }
    return;

}

/***************************************************************************
 *  FUNCTION NAME : PFCUtlFormAndSendPFCTLV
 * 
 *  DESCRIPTION   : This utility is used to from and send PFC TLV.
 * 
 *  INPUT         : pPortEntry - PFC Port Entry
 *                  u1MsgType - Message Type - Port Reg/Port Update.
 * 
 *  OUTPUT        : None
 * 
 *  RETURNS       : None
 * 
 * **************************************************************************/
PUBLIC VOID
PFCUtlFormAndSendPFCTLV (tPFCPortEntry * pPortEntry, UINT1 u1MsgType)
{
    UINT1               au1TlvBuf[PFC_TLV_LEN] = { DCBX_ZERO };
    UINT1              *pu1Temp = NULL;
    UINT1               u1Value = DCBX_ZERO;
    UINT1               u1Willing = DCBX_ZERO;
    UINT1               u1ValMBC = DCBX_ZERO;
    tDcbxAppRegInfo     DcbxAppPortMsg;

    MEMSET (&DcbxAppPortMsg, DCBX_ZERO, sizeof (tDcbxAppRegInfo));

    /* Get teh Appication ID to be filled in the DCBX Appl port strucutre */
    PFC_GET_APPL_ID (DcbxAppPortMsg.DcbxAppId);

    DcbxAppPortMsg.pApplCallBackFn = PFCApiTlvCallBackFn;
    DcbxAppPortMsg.u2TlvLen = PFC_TLV_LEN;
    /* Form the TLV Header with the Preformed TLV */
    pu1Temp = au1TlvBuf;
    MEMCPY (pu1Temp, gau1PFCPreFormedTlv, PFC_PREFORMED_TLV_LEN);
    pu1Temp = pu1Temp + PFC_PREFORMED_TLV_LEN;

    /* This byte will be filled in the order 
     *     Willing    MBC    Reserved  - PFC Cap
     *         1 bit   1 bit    2 bits    4 bits    */

    if (pPortEntry->PFCAdmPortInfo.u1PFCAdmWilling == PFC_ENABLED)
    {
        u1Willing = PFC_WILLING_MASK;
    }
    if (pPortEntry->PFCAdmPortInfo.u1PFCAdmMBC == PFC_ENABLED)
    {
        u1ValMBC = PFC_MBC_MASK;
    }

    u1Value = u1Willing | u1ValMBC | (pPortEntry->PFCAdmPortInfo.u1PFCAdmCap);
    DCBX_PUT_1BYTE (pu1Temp, u1Value);
    if (pPortEntry->PFCAdmPortInfo.u1PFCAdmWilling == PFC_ENABLED)
    {
        DCBX_PUT_1BYTE (pu1Temp, pPortEntry->PFCLocPortInfo.u1PFCLocStatus);
    }
    else
    {
        DCBX_PUT_1BYTE (pu1Temp, pPortEntry->PFCAdmPortInfo.u1PFCAdmStatus);
    }

    /* Post the mesage to DCBX with message type received */
    DcbxAppPortMsg.pu1ApplTlv = au1TlvBuf;
    DCBX_TRC (DCBX_TLV_TRC, "PFCUtlFormAndSendPFCTLV:"
              " Posting PFC TLV to DCBX!!!\r\n");
    /* Increment the PFC Tx TLV Counters */
    pPortEntry->u4PFCTxTLVCount = pPortEntry->u4PFCTxTLVCount + 1;

    /* Packet Dump to Console */
    if (gDcbxGlobalInfo.u4DCBXTrcFlag & DCBX_TLV_TRC)
    {
        DcbxPktDump (au1TlvBuf, DcbxAppPortMsg.u2TlvLen, DCBX_TX);
    }

    PFCPortPostMsgToDCBX (u1MsgType, pPortEntry->u4IfIndex, &DcbxAppPortMsg);
    /* Send Counter Update Info to Standby */
    DcbxRedSendDynamicPfcCounterInfo (pPortEntry, DCBX_RED_PFC_TX_COUNTER);
    return;
}

/***************************************************************************
 *  FUNCTION NAME : PFCUtlHandleTLV
 * 
 *  DESCRIPTION   : This utility is used to handle and process the PFC TLV.
 * 
 *  INPUT         : pPortEntry - PFC Port Entry.
 *                  ApplTlvParam - Appl TLV information.
 * 
 *  OUTPUT        : None
 * 
 *  RETURNS       : None
 * 
 * **************************************************************************/
PUBLIC VOID
PFCUtlHandleTLV (tPFCPortEntry * pPortEntry, tApplTlvParam * pApplTlvParam)
{
    BOOL1               i1Result = OSIX_FALSE;
    UINT1               u1Value = DCBX_ZERO;
    UINT1              *pu1Temp = NULL;
    UINT1               u1PfcMBC = PFC_DISABLED;
    UINT1               u1PfcCap = DCBX_ZERO;
    UINT1               u1Willing = PFC_DISABLED;
    UINT1               u1Count = DCBX_ZERO;
    UINT1              *pu1PfcStatus = NULL;
    UINT2               u2BitPos = DCBX_ZERO;
    BOOL1               bCompatibility = DCBX_ZERO;

    tDcbxTrapMsg        NotifyInfo;

    MEMSET (&NotifyInfo, DCBX_ZERO, sizeof (tDcbxTrapMsg));
    SET_DCBX_STATUS (pPortEntry, pPortEntry->u1PFCDcbxSemType,
                     DCBX_RED_PFC_DCBX_STATUS_INFO, DCBX_STATUS_UNKNOWN);

    if (pApplTlvParam->u2RxTlvLen != PFC_TLV_LEN)
    {
        /* If TLV length is not correct, then do not process the TLV */
        DCBX_TRC (DCBX_TLV_TRC | DCBX_FAILURE_TRC, "PFCUtlHandleTLV:"
                  "PFC Received TLV length is incorrect !!!\r\n");
        /* Incremet the PFC TLV Error Counter */
        pPortEntry->u4PFCRxTLVError++;
        /* Send Counter Update Info to Standby */
        DcbxRedSendDynamicPfcCounterInfo (pPortEntry, DCBX_RED_PFC_ERR_COUNTER);
        return;
    }

    pu1Temp = pApplTlvParam->au1DcbxTlv;

    if (MEMCMP (pu1Temp, gau1PFCPreFormedTlv,
                PFC_PREFORMED_TLV_LEN) != DCBX_ZERO)
    {
        /* If the Header is not correct then do not process the TLV  */
        DCBX_TRC (DCBX_TLV_TRC | DCBX_FAILURE_TRC, "PFCUtlHandleTLV:"
                  "PFC Received Header is incorrect !!!\r\n");
        /* Incremet the PFC TLV Error Counter */
        pPortEntry->u4PFCRxTLVError++;
        /* Send Counter Update Info to Standby */
        DcbxRedSendDynamicPfcCounterInfo (pPortEntry, DCBX_RED_PFC_ERR_COUNTER);
        return;
    }

    pu1Temp = pu1Temp + PFC_PREFORMED_TLV_LEN;

    if (pPortEntry->PFCRemPortInfo.i4PFCRemIndex == DCBX_ZERO)
    {
        /* Send Trap Notification for Peer Up status only if
         * the same trap is already not sent */
        NotifyInfo.u4IfIndex = pPortEntry->u4IfIndex;
        NotifyInfo.u1Module = PFC_TRAP;
        NotifyInfo.unTrapMsg.u1PeerStatus = PFC_ENABLED;
        DcbxSendNotification (&NotifyInfo, PEER_STATE_TRAP);
    }

    /* Get the next 1 byte  */
    DCBX_GET_1BYTE (pu1Temp, u1Value);

    /* Get the willing , MBC , PFC Cap value from this byte */
    if (u1Value & PFC_WILLING_MASK)
    {
        u1Willing = PFC_ENABLED;
    }
    if (u1Value & PFC_MBC_MASK)
    {
        u1PfcMBC = PFC_ENABLED;
    }
    u1PfcCap = u1Value & PFC_CAP_MASK;

    /* Get the next 1 byte  */
    /* Get PFC Enabled status from this byte */
    DCBX_GET_1BYTE (pu1Temp, u1Value);

    /* Number of priorities reported in the PFC Enable field must not exceed 
     * the number of priorities supported in the PFC Cap Filed */

    /*TODO NOTE: A node does not have to support 8 classes of service in order to be considered
     * capable of accepting any valid DesiredCfg. It may fulfill the requirements of the
     * configuration by combining priorities or priority groups requiring similar service (e.g. PFC
     * configuration and bandwidth management) into a traffic class. Details about decision to
     * combine various priorities in single traffic class is out of scope of this document. */
    for (u2BitPos = DCBX_MIN_VAL; u2BitPos <= DCBX_MAX_PRIORITIES; u2BitPos++)
    {
        pu1PfcStatus = &u1Value;

        DcbxUtlOsixBitlistIsBitSet (pu1PfcStatus, u2BitPos,
                                    DCBX_MIN_VAL, &i1Result);

        if (i1Result == OSIX_TRUE)
        {
            u1Count = (UINT1) (u1Count + 1);
        }
    }

    if (((u1PfcCap == DCBX_ZERO) && (u1Count <= DCBX_MAX_PRIORITIES))
        || (u1Count <= u1PfcCap))
    {
        /* Copy the Remote index paramters to the Remote Table for this port */
        pPortEntry->PFCRemPortInfo.i4PFCRemIndex = pApplTlvParam->i4RemIndex;
        pPortEntry->PFCRemPortInfo.u4PFCRemTimeMark =
            pApplTlvParam->u4RemLastUpdateTime;
        MEMCPY (pPortEntry->PFCRemPortInfo.au1PFCDestRemMacAddr,
                pApplTlvParam->RemNodeMacAddr, MAC_ADDR_LEN);
        pPortEntry->PFCRemPortInfo.u4PFCDestRemMacIndex =
            pApplTlvParam->u4RemLocalDestMACIndex;

        /*Updating  willing , MBC , PFC Cap value  */
        pPortEntry->PFCRemPortInfo.u1PFCRemWilling = u1Willing;
        pPortEntry->PFCRemPortInfo.u1PFCRemMBC = u1PfcMBC;
        pPortEntry->PFCRemPortInfo.u1PFCRemCap = u1PfcCap;

        /* Update PFC Enabled status */
        pPortEntry->PFCRemPortInfo.u1PFCRemStatus = u1Value;
    }
    else
    {

        DCBX_TRC (DCBX_TLV_TRC | DCBX_FAILURE_TRC, "PFCUtlHandleTLV:"
                  "Number of PFC Enbaled Priorities exceeds PFC Capability !!!\r\n");
        /* Incremet the PFC TLV Error Counter */
        pPortEntry->u4PFCRxTLVError++;
        /* Send Counter Update Info to Standby */
        DcbxRedSendDynamicPfcCounterInfo (pPortEntry, DCBX_RED_PFC_ERR_COUNTER);
        return;
    }
    bCompatibility = PFCApiIsCfgCompatible (pPortEntry);
    if ((bCompatibility == OSIX_TRUE) &&
        (pPortEntry->u1DcbxStatus != DCBX_STATUS_PEER_NW_CFG_COMPAT))
    {
        SET_DCBX_STATUS (pPortEntry, pPortEntry->u1PFCDcbxSemType,
                         DCBX_RED_PFC_DCBX_STATUS_INFO, DCBX_STATUS_OK);
    }

    /* Increment the PFC Rx TLV counter */
    pPortEntry->u4PFCRxTLVCount = pPortEntry->u4PFCRxTLVCount + 1;

    /* Send Dynamic PFC Remote table update to Standby Node */
    DcbxRedSendDynamicPfcRemInfo (pPortEntry);
    if (pPortEntry->PFCLocPortInfo.u1PFCLocWilling == PFC_ENABLED)
    {
        PFCUtlWillingChange (pPortEntry, OSIX_TRUE);
    }
    else
    {
        /* Indicate willing Change for Remote */
        PFCUtlWillingChange (pPortEntry, OSIX_FALSE);
    }

    /* Send Counter Update Info to Standby */
    DcbxRedSendDynamicPfcCounterInfo (pPortEntry, DCBX_RED_PFC_RX_COUNTER);
    return;
}

/***************************************************************************
 *  FUNCTION NAME : PFCUtlConfigProfile
 * 
 *  DESCRIPTION   : This function is used to update the hardware with 
 *                  PFC paramters.
 * 
 *  INPUT         : pPortEntry - PFC Port Entry.
 *                  u1NewPfcProfile - New PFC profile to be created.
 *                  u1Status - En/Dis Status
 * 
 *  OUTPUT        : None
 * 
 *  RETURNS       : OSIX_SUCCESS/OSIX_FAILURE
 * 
 * **************************************************************************/
PUBLIC INT4
PFCUtlConfigProfile (tPFCPortEntry * pPortEntry,
                     UINT1 u1NewPfcProfile, UINT1 u1Status)
{
    INT4                i4RetVal = OSIX_SUCCESS;

    if (u1Status == PFC_ENABLED)
    {
        i4RetVal = PFCUtlUpdateProfile (pPortEntry, DCBX_ZERO,
                                        u1NewPfcProfile, PFC_PORT_CFG);
    }
    else
    {
        i4RetVal = PFCUtlUpdateProfile (pPortEntry, DCBX_ZERO,
                                        u1NewPfcProfile, PFC_DEL_PROFILE);
    }
    return i4RetVal;
}

/*****************************************************************************/
/* Function Name      : PFCUtlUpdateProfile
 * */
/* Description        : This function is used to Update the                  */
/*                             PFC Profile Entry and Hardware                */
/* Input(s)           : pDcbxPortNode - Pointer to the DCB Port Node         */
/*                    : u1PfcPriority - PFC Priority                         */
/*                    : u1NewPfcProfile - PFC Profile                        */
/*                    : u1Flag - Flag to take action on Profile              */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/* Return Value(s)    : PFC_SUCCESS or PFC_FAILURE                           */
/* Called By          : SNMP Low Level                                       */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
PFCUtlUpdateProfile (tPFCPortEntry * pPortEntry,
                     UINT1 u1PfcPriority, UINT1 u1NewPfcProfile, UINT1 u1Flag)
{
    INT4                i4RetVal = OSIX_SUCCESS;

    switch (u1Flag)
    {
        case PFC_PORT_CFG:
            /* Delete from Old Profile Entry  */
            PFCUtlDeleteProfile (pPortEntry->PFCLocPortInfo.u1PFCLocStatus);

            /* Add this port in new Profile Entry */
            i4RetVal =
                PFCUtlCreateProfile (pPortEntry->PFCLocPortInfo.u1PFCLocStatus,
                                     u1NewPfcProfile, u1Flag);
            if (i4RetVal == OSIX_SUCCESS)
            {
                i4RetVal =
                    PFCUtlHwConfigPfc (pPortEntry->u4IfIndex, u1PfcPriority,
                                       u1NewPfcProfile, u1Flag);
            }
            break;
        case PFC_DEL_PROFILE:
            /* Delete from Old Profile */
            PFCUtlDeleteProfile (pPortEntry->PFCLocPortInfo.u1PFCLocStatus);
            /* Associate to Default Profile in Hardware */
            i4RetVal = PFCUtlHwConfigPfc (pPortEntry->u4IfIndex, PFC_INIT_VAL,
                                          PFC_INIT_VAL, PFC_PORT_CFG);
            break;
        default:
            return i4RetVal;
    }

    return i4RetVal;
}

/*****************************************************************************/
/* Function Name      : PFCUtlCreateProfile
 * */
/* Description        : This function is used to create the                  */
/*                             PFC Profile Entry and update Hardware         */
/* Input(s)           : pDcbxPortNode - Pointer to the DCB Port Node         */
/*                    : u1Flag - Flag to take action on Profile              */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/* Return Value(s)    : PFC_SUCCESS or PFC_FAILURE                           */
/* Called By          : PFC Utility Function                                 */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
PFCUtlCreateProfile (UINT1 u1OldProfile, UINT1 u1NewPfcProfile, UINT1 u1Flag)
{
    UINT4               u4NumProfile =
        (UINT4) gPFCGlobalInfo.u2PFCMaxNumOfProfiles;

    /* New Profile search */
    if (gPFCGlobalInfo.gaPFCProfileEntry[u1NewPfcProfile].u1IsStatusValid
        == OSIX_TRUE)
    {
        /* Profile entry is already present, so update the number
         *            of ports for that. */
        gPFCGlobalInfo.gaPFCProfileEntry[u1NewPfcProfile].u4PfcNumOfPorts++;
    }
    else
    {
        if (u4NumProfile < PFC_MAX_PROFILES)
        {
            gPFCGlobalInfo.gaPFCProfileEntry[u1NewPfcProfile].u1IsStatusValid =
                OSIX_TRUE;
            gPFCGlobalInfo.gaPFCProfileEntry[u1NewPfcProfile].u4PfcNumOfPorts++;

            gPFCGlobalInfo.u2PFCMaxNumOfProfiles++;
        }
        else
        {
            /* New Profile cannot be created */
            if (u1Flag == PFC_PORT_CFG)
            {
                /* PFC status is configured for a priority. Since new profile
                 * creation is failing, retain the old profile association
                 * by again incrementing the number ports for that profile */

                gPFCGlobalInfo.gaPFCProfileEntry[u1OldProfile].
                    u4PfcNumOfPorts++;
            }
            DCBX_TRC (DCBX_FAILURE_TRC, "PFCUtlCreateProfile:"
                      "PFC Profile creation FAILED !!!\r\n");
            return OSIX_FAILURE;
        }
    }
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : PFCUtlDeleteProfile
 * */
/* Description        : This function is used to Delete the                  */
/*                             PFC Profile Entry and in Hardware             */
/* Input(s)           : pDcbxPortNode - Pointer to the DCB Port Node         */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/* Return Value(s)    : PFC_SUCCESS or PFC_FAILURE                           */
/* Called By          : PFC Utility Function                                 */
/* Calling Function   :                                                      */
/*****************************************************************************/

INT4
PFCUtlDeleteProfile (UINT1 u1OldProfile)
{

    gPFCGlobalInfo.gaPFCProfileEntry[u1OldProfile].u4PfcNumOfPorts--;

    /* Delete the Old Profile */
    if ((gPFCGlobalInfo.gaPFCProfileEntry[u1OldProfile].u4PfcNumOfPorts ==
         PFC_INIT_VAL) && (u1OldProfile != PFC_INIT_VAL))
    {
        PFCUtlHwConfigPfc (PFC_INIT_VAL, PFC_INIT_VAL, u1OldProfile,
                           PFC_DEL_PROFILE);
        gPFCGlobalInfo.u2PFCMaxNumOfProfiles--;
        gPFCGlobalInfo.gaPFCProfileEntry[u1OldProfile].u1IsStatusValid =
            OSIX_FALSE;
        gPFCGlobalInfo.gaPFCProfileEntry[u1OldProfile].i4PfcHwProfileId =
            PFC_INVALID_ID;
    }
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : PFCUtlHwConfigPfc                                   */
/* Description        : This function is used to call the PFC NPAPI          */
/*                        through QOS                                        */
/* Input(s)           : pPfcHwEntry - Pointer to the Pfc Hardware Entry      */
/*                              to be updated in the Hardware                */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/* Return Value(s)    : PFC_SUCCESS or PFC_FAILURE                           */
/* Called By          : PFC Utility functions                                */
/* Calling Function   :                                                      */
/*****************************************************************************/

INT4
PFCUtlHwConfigPfc (UINT4 u4IfIndex, UINT1 u1PfcPriority,
                   UINT1 u1Profile, UINT1 u1Flag)
{
    tQosPfcHwEntry      PfcHwEntry;
    INT4                i4RetVal = OSIX_SUCCESS;

    /*Fill the PFC HW Entry */
    PfcHwEntry.u4Ifindex = u4IfIndex;
    PfcHwEntry.u4PfcMinThreshold = gPFCGlobalInfo.u4PFCMinThresh;
    PfcHwEntry.u4PfcMaxThreshold = gPFCGlobalInfo.u4PFCMaxThresh;
    PfcHwEntry.u1PfcHwCfgFlag = u1Flag;
    PfcHwEntry.u1PfcProfileBmp = u1Profile;
    PfcHwEntry.u1PfcPriority = u1PfcPriority;

    if (u1Flag != PFC_THR_CFG)
    {
        PfcHwEntry.i4PfcHwProfileId =
            gPFCGlobalInfo.gaPFCProfileEntry[u1Profile].i4PfcHwProfileId;
    }
    else
    {
        PfcHwEntry.i4PfcHwProfileId = PFC_INVALID_ID;
    }

    DCBX_TRC_ARG6 (DCBX_CONTROL_PLANE_TRC, "PFCUtlHwConfigPfc:"
                   "Paramters for QosApiConfigPfc are "
                   "IfIndex : %d Hardware Profile Id : %d PFC Profile : %d "
                   "Threshold Min : %d Max : %d Flag Value : %d "
                   "!!!\r\n", PfcHwEntry.u4Ifindex,
                   PfcHwEntry.i4PfcHwProfileId,
                   u1Profile, gPFCGlobalInfo.u4PFCMinThresh,
                   gPFCGlobalInfo.u4PFCMaxThresh, u1Flag);

    i4RetVal = QosApiConfigPfc (&PfcHwEntry);

    /*Program the Hardware with the Configured PFC Values */
    /* If the profile gets deleted, then update the hw profile id
     *        as invalid. */
    if (PfcHwEntry.u1PfcHwCfgFlag == PFC_DEL_PROFILE)
    {
        gPFCGlobalInfo.gaPFCProfileEntry[u1Profile].i4PfcHwProfileId
            = PFC_INVALID_ID;
    }
    /* In case PFC status configuration, Get the Hw Profile id
     *        and update the same in the profile entry. */
    if (PfcHwEntry.u1PfcHwCfgFlag == PFC_PORT_CFG)
    {
        gPFCGlobalInfo.gaPFCProfileEntry[u1Profile].i4PfcHwProfileId =
            PfcHwEntry.i4PfcHwProfileId;
    }
    return i4RetVal;
}

/*****************************************************************************/
/* Function Name      : PFCUtlInitPFCProfiles                                */
/* Description        : This function is used to initalise the profile table */
/* Input(s)           : None                                                 */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/* Return Value(s)    : None                                                 */
/* Called By          : PFC Utility Function                                 */
/* Calling Function   :                                                      */
/*****************************************************************************/

VOID
PFCUtlInitPFCProfiles (VOID)
{
    UINT4               u4Count = PFC_INIT_VAL;

    gPFCGlobalInfo.u2PFCMaxNumOfProfiles = PFC_INIT_VAL;
    gPFCGlobalInfo.u4PFCMinThresh = PFC_MIN_THRES;
    gPFCGlobalInfo.u4PFCMaxThresh = PFC_MAX_THRES;

    for (u4Count = PFC_INIT_VAL; u4Count < PFC_MAX_PROFILES; u4Count++)
    {
        gPFCGlobalInfo.gaPFCProfileEntry[u4Count].u4PfcNumOfPorts
            = PFC_INIT_VAL;
        gPFCGlobalInfo.gaPFCProfileEntry[u4Count].i4PfcHwProfileId
            = PFC_INVALID_ID;
        gPFCGlobalInfo.gaPFCProfileEntry[u4Count].u1IsStatusValid = OSIX_FALSE;
    }
}

/********************************************************************************/
/* Function Name      : DcbxUtlOsixBitlistIsBitSet                               */
/* Description        : This function is used to check whether bit is set,      */
/*             For coverity fix the FSAP macro OSIX_BITLIST_IS_BIT_SET */
/*             is converted as util function. Whenever a change is     */
/*             made in the macro this function also needs to be        */
/*             changed                                 */
/* Input(s)           : au1BitArray - BitArray to be checked,            */
/*             u2BitNumber - Bit Index value,                */
/*             i4ArraySize - Maximum bit array possible,        */
/*             bResult     - boolean value to check TRUE/FALSE         */
/* Output(s)          : None.                                                   */
/* Global Variables                                                             */
/* Referred           : None                                                    */
/* Global Variables                                                             */
/* Modified           : None                                                    */
/* Return Value(s)    : None                                                    */
/* Called By          : PFC Utility Function                                    */
/* Calling Function   :                                                         */
/********************************************************************************/

VOID
DcbxUtlOsixBitlistIsBitSet (UINT1 *au1BitArray, UINT2 u2BitNumber,
                            INT4 i4ArraySize, BOOL1 * bResult)
{
    UINT2               u2BitNumberBytePos;
    UINT2               u2BitNumberBitPos;
    u2BitNumberBytePos = (UINT2) (u2BitNumber / BITS_PER_BYTE);
    u2BitNumberBitPos = (UINT2) (u2BitNumber % BITS_PER_BYTE);
    *bResult = OSIX_FALSE;
    if (u2BitNumberBitPos == (UINT2) 0)
    {
        u2BitNumberBytePos = (UINT2) (u2BitNumberBytePos - 1);
    }

    if (u2BitNumberBytePos < ((UINT2) i4ArraySize))
    {
        if ((au1BitArray[u2BitNumberBytePos]
             & gau1BitMaskMap[u2BitNumberBitPos]) != 0)
        {

            *bResult = OSIX_TRUE;
        }
    }
}

/*****************************************************************************/
/* Function Name      : PFCUtlRegBasedOnMode                                 */
/*                                                                           */
/* Description        : This function is used to Register or DeRegister      */
/*                      based on the Mode and ENABLE/DISABLE status          */
/*                                                                           */
/* Input(s)           : pPortEntry - Pointer to PFC Entry                    */
/*                      u1Status   - ENABLE/DISABLE                          */
/*                                                                           */
/* Output(s)          : None.                                                */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/* Called By          : PFCUtlDeletePortTblEntry                             */
/*                      PFCUtlModuleStatusChange                             */
/*                      PFCUtlAdminModeChange                                */
/*                      PFCUtlTlvStatusChange                                */
/*****************************************************************************/
VOID
PFCUtlRegOrDeRegBasedOnMode (tPFCPortEntry * pPortEntry, UINT1 u1Status)
{
    UINT1               u1DcbxMode = DCBX_ZERO;

    DcbxUtlGetDCBXMode (pPortEntry->u4IfIndex, &u1DcbxMode);

    DCBX_TRC_ARG3 (DCBX_TLV_TRC, "In func PFCUtlRegBasedOnMode: "
                   "Port = %d, DcbxMode = %s, Status = %s\r\n",
                   pPortEntry->u4IfIndex,
                   DCBX_GET_MODE (u1DcbxMode), DCBX_GET_PFC_MODE (u1Status));

    if (u1Status == PFC_ENABLED)
    {
        /* Form and send the TC Supp TLV with Port registration request only if
         * TLV Tx status is enabled  */
        if (u1DcbxMode == DCBX_MODE_IEEE)
        {
            PFCUtlRegisterApplForPort (pPortEntry);
        }
        else if (u1DcbxMode == DCBX_MODE_CEE)
        {
            CEERegisterApplForPort (pPortEntry->u4IfIndex, CEE_PFC_TLV_TYPE);
        }
        else
        {
            PFCUtlRegisterApplForPort (pPortEntry);
            CEERegisterApplForPort (pPortEntry->u4IfIndex, CEE_PFC_TLV_TYPE);
        }
        SET_DCBX_STATUS (pPortEntry, pPortEntry->u1PFCDcbxSemType,
                         DCBX_RED_PFC_DCBX_STATUS_INFO,
                         DCBX_STATUS_PEER_NO_ADV_DCBX);
    }
    else
    {

        if (u1DcbxMode == DCBX_MODE_IEEE)
        {
            PFCUtlDeRegisterApplForPort (pPortEntry);
        }
        else if (u1DcbxMode == DCBX_MODE_CEE)
        {
            CEEDeRegisterApplForPort (pPortEntry->u4IfIndex, CEE_PFC_TLV_TYPE);
            /* Clearing the remote parameter after Deregistration */
            MEMSET (&(pPortEntry->PFCRemPortInfo), DCBX_ZERO,
                    sizeof (tPFCRemPortInfo));
        }
        else
        {
            PFCUtlDeRegisterApplForPort (pPortEntry);
            CEEDeRegisterApplForPort (pPortEntry->u4IfIndex, CEE_PFC_TLV_TYPE);
        }
        SET_DCBX_STATUS (pPortEntry, pPortEntry->u1PFCDcbxSemType,
                         DCBX_RED_PFC_DCBX_STATUS_INFO,
                         DCBX_STATUS_NOT_ADVERTISE);
    }

    return;
}

/********* END OF FILE **************/
