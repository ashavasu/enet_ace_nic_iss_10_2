/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: dcbxsz.c,v 1.3 2013/11/29 11:04:11 siva Exp $
 *
 * Description: This files for RADIUS.
 *******************************************************************/

#define _DCBXSZ_C
#include "dcbxinc.h"
extern INT4         IssSzRegisterModuleSizingParams (CHR1 * pu1ModName,
                                                     tFsModSizingParams *
                                                     pModSizingParams);
extern INT4         IssSzRegisterModulePoolId (CHR1 * pu1ModName,
                                               tMemPoolId * pModPoolId);
INT4
DcbxSizingMemCreateMemPools ()
{
    INT4                i4RetVal;
    INT4                i4SizingId;

    for (i4SizingId = 0; i4SizingId < DCBX_MAX_SIZING_ID; i4SizingId++)
    {
        i4RetVal =
            MemCreateMemPool (FsDCBXSizingParams[i4SizingId].u4StructSize,
                              FsDCBXSizingParams[i4SizingId].
                              u4PreAllocatedUnits, MEM_DEFAULT_MEMORY_TYPE,
                              &(DCBXMemPoolIds[i4SizingId]));
        if (i4RetVal == (INT4) MEM_FAILURE)
        {
            DcbxSizingMemDeleteMemPools ();
            return OSIX_FAILURE;
        }
    }
    return OSIX_SUCCESS;
}

INT4
DcbxSzRegisterModuleSizingParams (CHR1 * pu1ModName)
{
    /* Copy the Module Name */
    IssSzRegisterModuleSizingParams (pu1ModName, FsDCBXSizingParams);
    IssSzRegisterModulePoolId (pu1ModName, DCBXMemPoolIds);
    return OSIX_SUCCESS;
}

VOID
DcbxSizingMemDeleteMemPools ()
{
    INT4                i4SizingId;

    for (i4SizingId = 0; i4SizingId < DCBX_MAX_SIZING_ID; i4SizingId++)
    {
        if (DCBXMemPoolIds[i4SizingId] != 0)
        {
            MemDeleteMemPool (DCBXMemPoolIds[i4SizingId]);
            DCBXMemPoolIds[i4SizingId] = 0;
        }
    }
    return;
}
