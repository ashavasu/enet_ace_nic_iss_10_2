/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: ceesz.c,v 1.1 2016/03/05 12:19:43 siva Exp $
 *
 * Description: This files intializes the CEE Sizing paramas.
 *******************************************************************/

#define _CEESZ_C
#include "dcbxinc.h"
extern INT4         IssSzRegisterModuleSizingParams (CHR1 * pu1ModName,
                                                     tFsModSizingParams *
                                                     pModSizingParams);
extern INT4         IssSzRegisterModulePoolId (CHR1 * pu1ModName,
                                               tMemPoolId * pModPoolId);

/***************************************************************************
 * FUNCTION NAME  : CeeSizingMemCreateMemPools
 *
 * DESCRIPTION    : Creates Memory Pools for CEE function requirements. 
 *  
 * RETURNS        : OSIX_SUCCESS/OSIX_FAILURE
***************************************************************************/
INT4
CeeSizingMemCreateMemPools ()
{
    INT4                i4RetVal;
    INT4                i4SizingId;
    
    for (i4SizingId = 0; i4SizingId < CEE_MAX_SIZING_ID; i4SizingId++)
    {
        i4RetVal = MemCreateMemPool (FsCEESizingParams[i4SizingId].u4StructSize,
                                     FsCEESizingParams[i4SizingId].
                                     u4PreAllocatedUnits,
                                     MEM_DEFAULT_MEMORY_TYPE,
                                     &(CEEMemPoolIds[i4SizingId]));
        if (i4RetVal == (INT4) MEM_FAILURE)
        {
            CeeSizingMemDeleteMemPools ();
            return OSIX_FAILURE;
        }
    }
    return OSIX_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME  : CeeSzRegisterModuleSizingParams
 *
 * DESCRIPTION    : Register Sizing Parameters related to 
 *                    Memory Pools for CEE function requirements. 
 *  
 * RETURNS        : OSIX_SUCCESS/OSIX_FAILURE
***************************************************************************/
INT4
CeeSzRegisterModuleSizingParams (CHR1 * pu1ModName)
{
    /* Copy the Module Name */
    IssSzRegisterModuleSizingParams (pu1ModName, FsCEESizingParams);
    IssSzRegisterModulePoolId (pu1ModName, CEEMemPoolIds);
    return OSIX_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME  : CeeSizingMemDeleteMemPools
 *
 * DESCRIPTION    : Delete Memory Pools created for CEE function requirements. 
 *  
 * RETURNS        : None 
***************************************************************************/
VOID
CeeSizingMemDeleteMemPools ()
{
    INT4                i4SizingId;

    for (i4SizingId = 0; i4SizingId < CEE_MAX_SIZING_ID; i4SizingId++)
    {
        if (CEEMemPoolIds[i4SizingId] != 0)
        {
            MemDeleteMemPool (CEEMemPoolIds[i4SizingId]);
            CEEMemPoolIds[i4SizingId] = 0;
        }
    }
    return;
}

