/********************************************************************
 * Copyright (C) 2010 Aricent Inc . All Rights Reserved
 *
 * $Id: dcbxmbsm.c,v 1.7 2017/01/25 13:19:42 siva Exp $
 *
 * Description: This file contains DCBX MBSM support routines 
                and utility routines.
 *********************************************************************/
#include "dcbxinc.h"
/*****************************************************************************/
/* Function Name      : DcbxApiMbsmNotification                              */
/*                                                                           */
/* Description        : This function constructs the DCBX Q Msg and calls    */
/*                      the Q Message event to process this Meg.             */
/*                                                                           */
/*                      This function will be called from the MBSM module    */
/*                      when an indication for the Card Insertion is         */
/*                      received.                                            */
/*                                                                           */
/* Input(s)           :  tMbsmProtoMsg - Structure containing the SlotInfo,  */
/*                                       PortInfo and the Protocol Id.       */
/*                    :  i4Event       - Event to be processed . Currently it*/
/*                                       is MBSM_MSG_CARD_INSERT event       */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : MBSM_FAILURE / MBSM_SUCCESS                          */
/*****************************************************************************/
PUBLIC INT4
DcbxApiMbsmNotification (tMbsmProtoMsg * pProtoMsg, INT4 i4Event)
{
    tDcbxQueueMsg      *pMsg = NULL;
    tMbsmProtoAckMsg    MbsmProtoAckMsg;

    if (pProtoMsg == NULL)
    {
        return MBSM_FAILURE;
    }
    if (i4Event == MBSM_MSG_CARD_REMOVE)
    {
        MEMSET (&MbsmProtoAckMsg, DCBX_ZERO, sizeof (tMbsmProtoAckMsg));
        MbsmProtoAckMsg.i4ProtoCookie = pProtoMsg->i4ProtoCookie;
        MbsmProtoAckMsg.i4SlotId = pProtoMsg->MbsmSlotInfo.i4SlotId;
        MbsmProtoAckMsg.i4RetStatus = MBSM_SUCCESS;
        MbsmSendAckFromProto (&MbsmProtoAckMsg);
        return MBSM_SUCCESS;
    }
    if (gDcbxGlobalInfo.u1DCBXSystemCtrl != DCBX_START)    /*Shutdown check */
    {
        MEMSET (&MbsmProtoAckMsg, DCBX_ZERO, sizeof (tMbsmProtoAckMsg));
        MbsmProtoAckMsg.i4ProtoCookie = pProtoMsg->i4ProtoCookie;
        MbsmProtoAckMsg.i4SlotId = pProtoMsg->MbsmSlotInfo.i4SlotId;
        MbsmProtoAckMsg.i4RetStatus = MBSM_SUCCESS;
        MbsmSendAckFromProto (&MbsmProtoAckMsg);
        return MBSM_SUCCESS;
    }
    /* Allocates the memory from the queue memory pool */
    if ((pMsg = (tDcbxQueueMsg *) MemAllocMemBlk
         (gDcbxGlobalInfo.DcbxQPktPoolId)) == NULL)
    {
        DCBX_TRC ((DCBX_RESOURCE_TRC | DCBX_FAILURE_TRC),
                  "DcbxApiMbsmNotification:"
                  "DCBX Queue Message memory Allocation Failed !!!\r\n");
        MEMSET (&MbsmProtoAckMsg, DCBX_ZERO, sizeof (tMbsmProtoAckMsg));
        MbsmProtoAckMsg.i4ProtoCookie = pProtoMsg->i4ProtoCookie;
        MbsmProtoAckMsg.i4SlotId = pProtoMsg->MbsmSlotInfo.i4SlotId;
        MbsmProtoAckMsg.i4RetStatus = MBSM_SUCCESS;
        MbsmSendAckFromProto (&MbsmProtoAckMsg);
        return MBSM_FAILURE;
    }

    MEMSET (pMsg, 0, sizeof (tDcbxQueueMsg));

    if (((pMsg->unMsgParam.pMbsmProtoMsg) = MemAllocMemBlk
         (gDcbxGlobalInfo.DcbxMbsmPoolId)) == NULL)
    {
        DCBX_TRC ((DCBX_RESOURCE_TRC | DCBX_FAILURE_TRC),
                  "DcbxApiMbsmNotification:"
                  " Message memory Allocation Failed for pMbsmProtoMsg !!!\r\n");
        MEMSET (&MbsmProtoAckMsg, DCBX_ZERO, sizeof (tMbsmProtoAckMsg));
        MbsmProtoAckMsg.i4ProtoCookie = pProtoMsg->i4ProtoCookie;
        MbsmProtoAckMsg.i4SlotId = pProtoMsg->MbsmSlotInfo.i4SlotId;
        MbsmProtoAckMsg.i4RetStatus = MBSM_SUCCESS;
        MbsmSendAckFromProto (&MbsmProtoAckMsg);
        MemReleaseMemBlock (gDcbxGlobalInfo.DcbxQPktPoolId, (UINT1 *) pMsg);
        return MBSM_FAILURE;
    }
    pMsg->u4MsgType = DCBX_MBSM_MSG_CARD_INSERT;
    MEMCPY (pMsg->unMsgParam.pMbsmProtoMsg, pProtoMsg, sizeof (tMbsmProtoMsg));
    /* Post it to the DCBX queue */
    if (DcbxQueEnqMsg (pMsg) == OSIX_FAILURE)
    {
        DCBX_TRC (DCBX_FAILURE_TRC, "DcbxApiMbsmNotification:"
                  "DCBX EnQueue Message into queue Failed !!!\r\n");
        MEMSET (&MbsmProtoAckMsg, DCBX_ZERO, sizeof (tMbsmProtoAckMsg));
        MbsmProtoAckMsg.i4ProtoCookie = pProtoMsg->i4ProtoCookie;
        MbsmProtoAckMsg.i4SlotId = pProtoMsg->MbsmSlotInfo.i4SlotId;
        MbsmProtoAckMsg.i4RetStatus = MBSM_SUCCESS;
        MbsmSendAckFromProto (&MbsmProtoAckMsg);
        MemReleaseMemBlock (gDcbxGlobalInfo.DcbxMbsmPoolId,
                            (UINT1 *) (pMsg->unMsgParam.pMbsmProtoMsg));
        return MBSM_FAILURE;
    }
    DCBX_TRC (DCBX_CRITICAL_TRC, "DcbxApiMbsmNotification:"
                  "DCBX EnQueue Message successfully posted !!!\r\n");
    return MBSM_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : DcbxMbsmProcessMsg                                   */
/*                                                                           */
/* Description        : This function process the MBSM message and calls the */
/*                      respective function to programm the entries in Hw.   */
/*                                                                           */
/* Input(s)           :  tMbsmProtoMsg - Structure containing the SlotInfo,  */
/*                                       PortInfo and the Protocol Id.       */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
PUBLIC VOID
DcbxMbsmProcessMsg (tMbsmProtoMsg * pProtoMsg)
{
    tMbsmProtoAckMsg    MbsmProtoAckMsg;

    if (pProtoMsg != NULL)
    {
        DcbxMbsmConfigPfc (&pProtoMsg->MbsmPortInfo, &pProtoMsg->MbsmSlotInfo);
        DcbxMbsmConfigEts (&pProtoMsg->MbsmPortInfo, &pProtoMsg->MbsmSlotInfo);
        /* For Application Priority, it is not required to initiate as it will 
         * be taken care by QOS and ACL configuration */
    }

    MEMSET (&MbsmProtoAckMsg, DCBX_ZERO, sizeof (tMbsmProtoAckMsg));
    MbsmProtoAckMsg.i4ProtoCookie = pProtoMsg->i4ProtoCookie;
    MbsmProtoAckMsg.i4SlotId = pProtoMsg->MbsmSlotInfo.i4SlotId;
    MbsmProtoAckMsg.i4RetStatus = MBSM_SUCCESS;
    DCBX_TRC (DCBX_CRITICAL_TRC, "DcbxMbsmProcessMsg:"
                  "Send Ack to MBSM !!!\r\n");
    MbsmSendAckFromProto (&MbsmProtoAckMsg);
    return;
}

/*****************************************************************************/
/* Function Name      : DcbxMbsmConfigPfc                                    */
/*                                                                           */
/* Description        : This function scans the PFC entries for the ports    */
/*                      present in the slot info and calls the QOS Api to    */
/*                      programm in the hardware.                            */
/*                                                                           */
/* Input(s)           : MbsmPortInfo - Port Info.                            */
/*                      MbsmSlotInfo - Slot Info.                            */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
PUBLIC VOID
DcbxMbsmConfigPfc (tMbsmPortInfo * pMbsmPortInfo, tMbsmSlotInfo * pSlotInfo)
{
    tQosPfcHwEntry      PfcHwEntry;
    tPFCPortEntry      *pPFCPortEntry = NULL;
    UINT4               u4Port = DCBX_ZERO;
    UINT2               u2ByteIndex = DCBX_ZERO;
    UINT2               u2BitIndex = DCBX_ZERO;
    UINT1               u1PortFlag = DCBX_ZERO;
    UINT1               u1Profile = DCBX_ZERO;
    UINT1               u1IsSetInPortListStatus = OSIX_FALSE;

    UNUSED_PARAM (pSlotInfo);

    for (u2ByteIndex = DCBX_ZERO; u2ByteIndex < BRG_PORT_LIST_SIZE;
         u2ByteIndex++)
    {
        u1PortFlag = pMbsmPortInfo->PortList[u2ByteIndex];
        for (u2BitIndex = DCBX_ZERO; ((u2BitIndex < BITS_PER_BYTE)
                                      && (u1PortFlag != DCBX_ZERO));
             u2BitIndex++)
        {
            if ((u1PortFlag & VLAN_BIT8) != DCBX_ZERO)
            {
                u4Port = (UINT4) ((u2ByteIndex * BITS_PER_BYTE) +
                                  u2BitIndex + 1);
                OSIX_BITLIST_IS_BIT_SET (MBSM_PORT_INFO_PORTLIST_STATUS
                                         (pMbsmPortInfo), u4Port,
                                         sizeof (tPortList),
                                         u1IsSetInPortListStatus);
                if (OSIX_FALSE == u1IsSetInPortListStatus)
                {

                    MEMSET (&PfcHwEntry, DCBX_ZERO, sizeof (tQosPfcHwEntry));
                    pPFCPortEntry = PFCUtlGetPortEntry (u4Port);
                    if (pPFCPortEntry != NULL)
                    {
                        u1Profile =
                            pPFCPortEntry->PFCLocPortInfo.u1PFCLocStatus;
                        /*Fill the PFC HW Entry */
                        PfcHwEntry.u4Ifindex = u4Port;
                        PfcHwEntry.u4PfcMinThreshold =
                            gPFCGlobalInfo.u4PFCMinThresh;
                        PfcHwEntry.u4PfcMaxThreshold =
                            gPFCGlobalInfo.u4PFCMaxThresh;
                        PfcHwEntry.u1PfcHwCfgFlag = PFC_PORT_CFG;
                        PfcHwEntry.u1PfcProfileBmp = u1Profile;
                        PfcHwEntry.i4PfcHwProfileId = PFC_INVALID_ID;

                        QosApiConfigPfc (&PfcHwEntry);

                        if (PfcHwEntry.u1PfcHwCfgFlag == PFC_PORT_CFG)
                        {
                            gPFCGlobalInfo.gaPFCProfileEntry[u1Profile].
                                i4PfcHwProfileId = PfcHwEntry.i4PfcHwProfileId;
                        }
                    }
                }
            }
            u1PortFlag = (UINT1) (u1PortFlag << 1);
        }
    }
    return;
}
/*****************************************************************************/
/* Function Name      : DcbxMbsmConfigAppPri                                 */
/*                                                                           */
/* Description        : This function scans the AppPri entries for the ports */
/*                      present in the slot info and calls the QOS Api       */
/*                      & Filter to program into Hardware.                   */
/*                                                                           */
/* Input(s)           : MbsmPortInfo - Port Info.                            */
/*                      MbsmSlotInfo - Slot Info.                            */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
/* This function is not used */
/* For AppPri, it is not required to initiate mbsm call from dcbx */
/* It will be taken care by QOS and ACL configuration */
PUBLIC VOID
DcbxMbsmConfigAppPri (tMbsmPortInfo * pMbsmPortInfo, tMbsmSlotInfo * pSlotInfo)
{
    tAppPriPortEntry    *pAppPriPortEntry = NULL;    
    UINT4               u4Port = DCBX_ZERO;
    UINT2               u2ByteIndex = DCBX_ZERO;
    UINT2               u2BitIndex = DCBX_ZERO;
    UINT1               u1PortFlag = DCBX_ZERO;
    UINT1               u1Status = DCBX_ZERO;
    UINT1               u1IsSetInPortListStatus = OSIX_FALSE;

    UNUSED_PARAM (pSlotInfo);

    for (u2ByteIndex = DCBX_ZERO; u2ByteIndex < BRG_PORT_LIST_SIZE;
            u2ByteIndex++)
    {
        u1PortFlag = pMbsmPortInfo->PortList[u2ByteIndex];
        for (u2BitIndex = DCBX_ZERO; ((u2BitIndex < BITS_PER_BYTE)
                    && (u1PortFlag != DCBX_ZERO));
                u2BitIndex++)
        {
            if ((u1PortFlag & VLAN_BIT8) != DCBX_ZERO)
            {
                u4Port = (UINT4) ((u2ByteIndex * BITS_PER_BYTE) +
                        u2BitIndex + 1);
                OSIX_BITLIST_IS_BIT_SET (MBSM_PORT_INFO_PORTLIST_STATUS
                        (pMbsmPortInfo), u4Port,
                        sizeof (tPortList),
                        u1IsSetInPortListStatus);
                if (OSIX_FALSE == u1IsSetInPortListStatus)
                {
                    pAppPriPortEntry = AppPriUtlGetPortEntry (u4Port);
                    if (pAppPriPortEntry != NULL)
                    {
                        u1Status = pAppPriPortEntry->u1AppPriDcbxOperState;
                        AppPriUtlHwConfig (pAppPriPortEntry, u1Status);
                    }
                }
            }
        
            u1PortFlag = (UINT1) (u1PortFlag << 1);
        }
    }
    return;
}
/*****************************************************************************/
/* Function Name      : DcbxMbsmConfigEts                                    */
/*                                                                           */
/* Description        : This function scans the ETS entries for the ports    */
/*                      present in the slot info and calls the QOS Api to    */
/*                      programm in the hardware.                            */
/*                                                                           */
/* Input(s)           : MbsmPortInfo - Port Info.                            */
/*                      MbsmSlotInfo - Slot Info.                            */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
PUBLIC VOID
DcbxMbsmConfigEts (tMbsmPortInfo * pMbsmPortInfo, tMbsmSlotInfo * pSlotInfo)
{
    tETSPortEntry      *pPortEntry = NULL;
    UINT1               u1OperVersion = DCBX_VER_UNKNOWN;
    UINT1               u1Status = DCBX_ZERO;
    UINT4               u4Port = DCBX_ZERO;
    UINT2               u2ByteIndex = DCBX_ZERO;
    UINT2               u2BitIndex = DCBX_ZERO;
    UINT1               u1PortFlag = DCBX_ZERO;
    UINT1               u1IsSetInPortListStatus = OSIX_FALSE;
    
    UNUSED_PARAM (pSlotInfo);

    for (u2ByteIndex = DCBX_ZERO; u2ByteIndex < BRG_PORT_LIST_SIZE;
         u2ByteIndex++)
    {
        u1PortFlag = pMbsmPortInfo->PortList[u2ByteIndex];
        for (u2BitIndex = DCBX_ZERO; ((u2BitIndex < BITS_PER_BYTE)
                                      && (u1PortFlag != DCBX_ZERO));
             u2BitIndex++)
        {
            if ((u1PortFlag & VLAN_BIT8) != DCBX_ZERO)
            {
                u4Port = (UINT4) ((u2ByteIndex * BITS_PER_BYTE) +
                                  u2BitIndex + 1);
                OSIX_BITLIST_IS_BIT_SET (MBSM_PORT_INFO_PORTLIST_STATUS
                                         (pMbsmPortInfo), u4Port,
                                         sizeof (tPortList),
                                         u1IsSetInPortListStatus);
                if (OSIX_FALSE == u1IsSetInPortListStatus)
                {
                    pPortEntry = ETSUtlGetPortEntry (u4Port);
                    if (pPortEntry != NULL)
                    {
                        u1Status = gETSGlobalInfo.u1ETSModStatus;
                        DcbxUtlGetOperVersion (pPortEntry->u4IfIndex, &u1OperVersion);
                        if (u1OperVersion == DCBX_VER_CEE)
                        {
                            CeeUtlETSHwConfig (pPortEntry, u1Status);
                        }
                        else                        
                        {
                            ETSUtlHwConfig (pPortEntry, u1Status);
                        }
                    }
                }
            }
            u1PortFlag = (UINT1) (u1PortFlag << 1);
        }
    }
    return;
}
