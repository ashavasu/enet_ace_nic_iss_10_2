/*************************************************************************
 * Copyright (C) 2010 Aricent Inc . All Rights Reserved
 * $Id: etsapi.c,v 1.5 2016/05/25 10:06:10 siva Exp $
 * Description: This file contains ETS function Exported to DCBX.
****************************************************************************/
#include "dcbxinc.h"

/***************************************************************************
 *  FUNCTION NAME : ETSApiConfTlvCallBackFn
 * 
 *  DESCRIPTION   : This call back function will be registered with DCBX
 *                  to hanlde the Conf TLV related messages
 * 
 *  INPUT         : tDcbxAppInfo - Structure with TLV info.
 * 
 *  OUTPUT        : None
 * 
 *  RETURNS       : None
 * 
 * **************************************************************************/
PUBLIC VOID
ETSApiConfTlvCallBackFn (tDcbxAppInfo * pDcbxAppInfo)
{
    tETSPortEntry      *pPortEntry = NULL;
    /* Get the ETS port entry  and if the port entry is not present
     * then return without processing the message from DCBX */
    pPortEntry = ETSUtlGetPortEntry (pDcbxAppInfo->u4IfIndex);
    if (pPortEntry == NULL)
    {
        DCBX_TRC (DCBX_FAILURE_TRC, "ETSApiConfTlvCallBackFn:"
                  " No ETS Port is created!!!\r\n");
        return;
    }

    switch (pDcbxAppInfo->u4MsgType)
    {
        case L2IWF_LLDP_APPL_TLV_RECV:
            /* TLV Received from the LLDP through DCBX,
             * process the TLV infomration and fill the remote table */
            DCBX_TRC (DCBX_TLV_TRC, "ETSApiConfTlvCallBackFn:"
                      "ETS Conf TLV recieved !!!\r\n");
            ETSUtlHandleConfigTLV (pPortEntry, &pDcbxAppInfo->ApplTlvParam);
            break;
        case L2IWF_LLDP_APPL_TLV_AGED:
        case L2IWF_LLDP_MULTIPLE_PEER_NOTIFY:
            /* Process the Age Out event from the LLDP through DCBX */
            DCBX_TRC (DCBX_TLV_TRC, "ETSApiConfTlvCallBackFn:"
                      "ETS Conf TLV Age Out recieved !!!\r\n");
            ETSUtlHandleAgedOutFromDCBX (pPortEntry, ETS_CONF_TLV_SUB_TYPE);
            break;
        case L2IWF_LLDP_APPL_RE_REG:
            /* Handle the Re-registration request from the LLDP */
            DCBX_TRC (DCBX_TLV_TRC, "ETSApiConfTlvCallBackFn:"
                      "ETS Conf TLV Re Registration request recieved !!!\r\n");
            ETSUtlHandleReRegReqFromDCBX (pPortEntry, ETS_CONF_TLV_SUB_TYPE);
            break;
        case DCBX_OPER_UPDATE:
            /* State machine update from the DCBX */
            DCBX_TRC (DCBX_SEM_TRC, "ETSApiConfTlvCallBackFn:"
                      "ETS State Machine Update Recieved !!!\r\n");
            /* Change the state with the DCBX Oper state received from the
             * DCBX */
            ETSUtlOperStateChange (pPortEntry, pDcbxAppInfo->u1ApplOperState);
            /* Send SEM update info to Standby Event */
            DcbxRedSendDynamicEtsSemInfo (pPortEntry);
            break;
        default:
            break;
    }
    return;
}

/***************************************************************************
 *  FUNCTION NAME : ETSApiRecoTlvCallBackFn
 * 
 *  DESCRIPTION   : This call back function will be registered with DCBX
 *                  to hanlde the Reco TLV related messages
 * 
 *  INPUT         : tDcbxAppInfo - Structure with TLV info.
 * 
 *  OUTPUT        : None
 * 
 *  RETURNS       : None
 * 
 * **************************************************************************/
PUBLIC VOID
ETSApiRecoTlvCallBackFn (tDcbxAppInfo * pDcbxAppInfo)
{
    tETSPortEntry      *pPortEntry = NULL;
    /* Get the ETS port entry  and if the port entry is not present
     * then return without processing the message from DCBX */
    pPortEntry = ETSUtlGetPortEntry (pDcbxAppInfo->u4IfIndex);
    if (pPortEntry == NULL)
    {
        DCBX_TRC (DCBX_FAILURE_TRC, "ETSApiRecoTlvCallBackFn:"
                  " No ETS Port is created!!!\r\n");
        return;
    }

    switch (pDcbxAppInfo->u4MsgType)
    {
        case L2IWF_LLDP_APPL_TLV_RECV:
            /* TLV Received from the LLDP through DCBX,
             * process the TLV infomration and fill the remote table */
            DCBX_TRC (DCBX_TLV_TRC, "ETSApiRecoTlvCallBackFn:"
                      "ETS Reco TLV recieved !!!\r\n");
            ETSUtlHandleRecoTLV (pPortEntry, &pDcbxAppInfo->ApplTlvParam);
            break;
        case L2IWF_LLDP_APPL_TLV_AGED:
        case L2IWF_LLDP_MULTIPLE_PEER_NOTIFY:
            /* Process the Age Out event from the LLDP through DCBX */
            DCBX_TRC (DCBX_TLV_TRC, "ETSApiRecoTlvCallBackFn:"
                      "ETS Reco TLV Age Out recieved !!!\r\n");
            ETSUtlHandleAgedOutFromDCBX (pPortEntry, ETS_RECO_TLV_SUB_TYPE);
            break;
        case L2IWF_LLDP_APPL_RE_REG:
            /* Handle the Re-registration request from the LLDP */
            DCBX_TRC (DCBX_TLV_TRC, "ETSApiRecoTlvCallBackFn:"
                      "ETS Reco TLV Re Registration request recieved !!!\r\n");
            ETSUtlHandleReRegReqFromDCBX (pPortEntry, ETS_RECO_TLV_SUB_TYPE);
            break;
        default:
            break;
    }
    return;

}

/***************************************************************************
 *  FUNCTION NAME : ETSApiTcSuppTlvCallBackFn
 * 
 *  DESCRIPTION   : This call back function will be registered with DCBX
 *                  to hanlde the TC Supp TLV related messages
 * 
 *  INPUT         : tDcbxAppInfo - Structure with TLV info.
 * 
 *  OUTPUT        : None
 * 
 *  RETURNS       : None
 * 
 * **************************************************************************/
PUBLIC VOID
ETSApiTcSuppTlvCallBackFn (tDcbxAppInfo * pDcbxAppInfo)
{
    tETSPortEntry      *pPortEntry = NULL;
    /* Get the ETS port entry  and if the port entry is not present
     * then return without processing the message from DCBX */
    pPortEntry = ETSUtlGetPortEntry (pDcbxAppInfo->u4IfIndex);
    if (pPortEntry == NULL)
    {
        DCBX_TRC (DCBX_FAILURE_TRC, "ETSApiTcSuppTlvCallBackFn:"
                  " No ETS Port is created!!!\r\n");
        return;
    }

    switch (pDcbxAppInfo->u4MsgType)
    {
        case L2IWF_LLDP_APPL_TLV_RECV:
            /* TLV Received from the LLDP through DCBX,
             * process the TLV infomration and fill the remote table */
            DCBX_TRC (DCBX_TLV_TRC, "ETSApiTcSuppTlvCallBackFn:"
                      "ETS TC Supp TLV recieved !!!\r\n");
            ETSUtlHandleTcSupportTLV (pPortEntry, &pDcbxAppInfo->ApplTlvParam);
            break;
        case L2IWF_LLDP_APPL_TLV_AGED:
        case L2IWF_LLDP_MULTIPLE_PEER_NOTIFY:
            /* Process the Age Out event from the LLDP through DCBX */
            DCBX_TRC (DCBX_TLV_TRC, "ETSApiTcSuppTlvCallBackFn:"
                      "ETS Tc Supp TLV Age Out recieved !!!\r\n");
            ETSUtlHandleAgedOutFromDCBX (pPortEntry, ETS_TC_SUPP_TLV_SUB_TYPE);
            break;
        case L2IWF_LLDP_APPL_RE_REG:
            /* Handle the Re-registration request from the LLDP */
            DCBX_TRC (DCBX_TLV_TRC, "ETSApiTcSuppTlvCallBackFn:"
                      "ETS TC Supp TLV Re Registration request"
                      "recieved !!!\r\n");
            ETSUtlHandleReRegReqFromDCBX (pPortEntry, ETS_TC_SUPP_TLV_SUB_TYPE);
            break;
        default:
            break;
    }
    return;

}

/***************************************************************************
 *  FUNCTION NAME : ETSApiIsCfgCompatible
 * 
 *  DESCRIPTION   : This function is used to check the compatibility between
 *                  local and the peer for ETS Feature
 * 
 *  INPUT         : pETSPortEntry - ETS Table Port Entry.
 * 
 *  OUTPUT        : None
 * 
 *  RETURNS       : TRUE/FALSE
 * 
 * **************************************************************************/
PUBLIC BOOL1
ETSApiIsCfgCompatible(tETSPortEntry *pETSPortEntry)
{
    UINT1 u1DcbxMode = DCBX_ZERO;

    if(pETSPortEntry->ETSAdmPortInfo.u1ETSAdmNumTCGSup ==
            pETSPortEntry->ETSRemPortInfo.u1ETSRemNumTCGSup)
    {
        if ((MEMCMP (pETSPortEntry->ETSRemPortInfo.au1ETSRemTCGID, 
                     pETSPortEntry->ETSAdmPortInfo.au1ETSAdmTCGID,
                     DCBX_MAX_PRIORITIES) == DCBX_ZERO) && 
            (MEMCMP (pETSPortEntry->ETSRemPortInfo.au1ETSRemBW,
                     pETSPortEntry->ETSAdmPortInfo.au1ETSAdmBW,
                     ETS_MAX_TCGID_CONF) == DCBX_ZERO))
        {
            DcbxUtlGetDCBXMode(pETSPortEntry->u4IfIndex , &u1DcbxMode);

            if(u1DcbxMode == DCBX_MODE_IEEE)
            {
                if (MEMCMP (pETSPortEntry->ETSRemPortInfo.au1ETSRemTsaTable,
                            pETSPortEntry->ETSAdmPortInfo.au1ETSAdmTsaTable,
                            ETS_MAX_TCGID_CONF) != DCBX_ZERO)
                {
                    DCBX_TRC (DCBX_TLV_TRC, "ETSApiIsCfgCompatible :"
                            "Local and Remote ETS TSA Table configurations"
                             "are not compatible \r\n");
                    SET_DCBX_STATUS (pETSPortEntry, pETSPortEntry->u1ETSDcbxSemType, 
                            DCBX_RED_ETS_DCBX_STATUS_INFO, DCBX_STATUS_CFG_NOT_COMPT);

                    return OSIX_FALSE;
                }
            }

            if(pETSPortEntry->ETSRemPortInfo.u1ETSRemWilling == ETS_DISABLED)
            {
                DCBX_TRC (DCBX_TLV_TRC, "ETSApiIsCfgCompatible : "
                    "Local and Remote ETS configurations are compatible,"
                    "But Remote willing is disabled! \r\n");
                SET_DCBX_STATUS (pETSPortEntry, pETSPortEntry->u1ETSDcbxSemType,
                        DCBX_RED_ETS_DCBX_STATUS_INFO, DCBX_STATUS_PEER_NW_CFG_COMPAT);
                return OSIX_TRUE;
            }

            DCBX_TRC (DCBX_TLV_TRC, "ETSApiIsCfgCompatible :"
                    "Local and Remote ETS configurations are compatible \r\n");
            SET_DCBX_STATUS (pETSPortEntry, pETSPortEntry->u1ETSDcbxSemType, 
                    DCBX_RED_ETS_DCBX_STATUS_INFO, DCBX_STATUS_OK);
            return OSIX_TRUE;
        }
    }

    DCBX_TRC (DCBX_TLV_TRC, "ETSApiIsCfgCompatible :"
            "Local and Remote ETS configurations are not compatible \r\n");

    SET_DCBX_STATUS (pETSPortEntry, pETSPortEntry->u1ETSDcbxSemType, 
            DCBX_RED_ETS_DCBX_STATUS_INFO, DCBX_STATUS_CFG_NOT_COMPT);
    return OSIX_FALSE;
}

#ifdef DCBX_ETS_QOS_WANTED    
/***************************************************************************
 *  FUNCTION NAME : EtsApiIsEtsAdminModeUp 
 * 
 *  DESCRIPTION   : This function is used to check the ETS admin mode statu
 * 
 *  INPUT         : u4IfIndex - Interface Index 
 * 
 *  OUTPUT        : None
 * 
 *  RETURNS       : OSIX_SUCCESS/OSIX_FAILURE
 * 
 * **************************************************************************/
UINT1 EtsApiIsEtsAdminModeUp (UINT4 u4IfIndex)
{
    tETSPortEntry     *pEtsPortEntry = NULL;

    DcbxLock ();

    pEtsPortEntry = ETSUtlGetPortEntry ((UINT4) u4IfIndex);
    if (pEtsPortEntry == NULL)
    {
        DCBX_TRC_ARG1 (DCBX_FAILURE_TRC, "In %s : DcbxApiIsEtsAdminModeUp () "
                "Returns FAILURE. \r\n", __FUNCTION__);

        DcbxUnLock ();
        return OSIX_FAILURE;
    }

    if ((pEtsPortEntry->u1ETSAdminMode == DCBX_ADM_MODE_AUTO) || 
           (pEtsPortEntry->u1ETSAdminMode == DCBX_ADM_MODE_ON))
    {
        /* If the DCBX admin state is enabled
         * then return SUCCESS, else return FAILURE*/

        DcbxUnLock ();
        return OSIX_SUCCESS;
    }

    DcbxUnLock ();
    return OSIX_FAILURE;
}
#endif
