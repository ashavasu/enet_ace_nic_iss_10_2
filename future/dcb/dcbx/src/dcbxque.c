/*************************************************************************
 * Copyright (C) 2010 Aricent Inc . All Rights Reserved
 * $Id: dcbxque.c,v 1.10 2017/01/25 13:19:42 siva Exp $
 * Description: This file contains DCBX function which handles
 *               DCBX Queue processing. 
****************************************************************************/
#include "dcbxinc.h"

const CHR1 *au1MsgType [17] =
{
    "UNUSED",
    "L2IWF_LLDP_APPL_TLV_RECV",
    "L2IWF_LLDP_APPL_TLV_AGED",
    "L2IWF_LLDP_APPL_RE_REG",
    "L2IWF_LLDP_APPL_PDU_RECV",
    "L2IWF_LLDP_APPL_NEIGH_EXIST_IND",
    "L2IWF_LLDP_TX_ENABLE_NOTIFY",
    "L2IWF_LLDP_RX_ENABLE_NOTIFY",
    "L2IWF_LLDP_TX_DISABLE_NOTIFY",
    "L2IWF_LLDP_RX_DISABLE_NOTIFY",
    "L2IWF_LLDP_MULTIPLE_PEER_NOTIFY",
    "DCBX_CREATE_PORT_MSG",
    "DCBX_DELETE_PORT_MSG",
    "DCBX_ADD_LAG_MEMBER_MSG",
    "DCBX_REM_LAG_MEMBER_MSG",
    "DCBX_RM_MSG",
    "DCBX_MBSM_MSG_CARD_INSERT"
};
/***************************************************************************
 *  FUNCTION NAME : DcbxQueEnqMsg
 * 
 *  DESCRIPTION   : This function is used to post the message in DCBX Task.
 * 
 *  INPUT         : pMsg - Message buffer
 * 
 *  OUTPUT        : None
 * 
 *  RETURNS       : OSIX_SUCCESS/OSIX_FAILURE
 * 
 * **************************************************************************/
PUBLIC INT4
DcbxQueEnqMsg (tDcbxQueueMsg * pMsg)
{
    UINT4   u4MsgsInQ = 0;

    if (OsixGetNumMsgsInQ ((UINT4) 0, DCBX_TASK_QUEUE_NAME,
            &u4MsgsInQ) == OSIX_SUCCESS)
    {
        if (u4MsgsInQ > DCBX_TASK_QUEUE_LIMIT)
        {
            if (pMsg->u4MsgType == L2IWF_LLDP_APPL_TLV_RECV)
            {
                DCBX_TRC (DCBX_CRITICAL_TRC, "DcbxQueEnqMsg:"
                        "Queue size exceeds the limit of received packet."
                        "Hence dropping!\r\n");
                MemReleaseMemBlock (gDcbxGlobalInfo.DcbxQPktPoolId, (UINT1 *) pMsg);
                return OSIX_FAILURE;
            }
            else
            {
                DCBX_TRC_ARG1 (DCBX_CRITICAL_TRC, "DcbxQueEnqMsg:"
                        "Reaching Queue threshold .... not dropping because of"
                        "packet type: %d\r\n",
                        pMsg->u4MsgType);
            }
        }
    }
    /* Send the message to the DCBX queue */
    if (OsixQueSend (gDcbxGlobalInfo.DcbxPktQId, (UINT1 *) &pMsg,
                     OSIX_DEF_MSG_LEN) == OSIX_FAILURE)
    {
        /* Release the allocated memory for the queue if the posting
         * message to the queue fails */
        MemReleaseMemBlock (gDcbxGlobalInfo.DcbxQPktPoolId, (UINT1 *) pMsg);
        DCBX_TRC (DCBX_FAILURE_TRC, "DcbxQueEnqMsg:"
                  "Osix Queue send Failed!!! !!!\r\n");
        return OSIX_FAILURE;
    }
    /* Send the QMSG_EVENT to the DCBX Task  */
    OsixEvtSend (gDcbxGlobalInfo.DcbxTaskId, DCBX_QMSG_EVENT);

    if ((pMsg->u4MsgType == DCBX_CREATE_PORT_MSG) ||
        (pMsg->u4MsgType == DCBX_DELETE_PORT_MSG) ||
        (pMsg->u4MsgType == DCBX_ADD_LAG_MEMBER_MSG) ||
        (pMsg->u4MsgType == DCBX_REM_LAG_MEMBER_MSG))
    {
        L2_SYNC_TAKE_SEM ();
    }
    return OSIX_SUCCESS;
}

/***************************************************************************
 *  FUNCTION NAME : DcbxQueMsgHandler
 * 
 *  DESCRIPTION   : This function is used to process the message posted
 *                  in DBCX queue.
 * 
 *  INPUT         : None
 * 
 *  OUTPUT        : None
 * 
 *  RETURNS       : None
 * 
 * **************************************************************************/
PUBLIC VOID
DcbxQueMsgHandler (VOID)
{
    tDcbxQueueMsg      *pQMsg = NULL;

    while (OsixQueRecv (gDcbxGlobalInfo.DcbxPktQId, (UINT1 *) &pQMsg,
                        OSIX_DEF_MSG_LEN, OSIX_NO_WAIT) == OSIX_SUCCESS)
    {
        DCBX_TRC_ARG2 (DCBX_CONTROL_PLANE_TRC | DCBX_TLV_TRC, "DcbxQueMsgHandler:"
                "Received event [%s] on port %d \r\n",
                au1MsgType[pQMsg->u4MsgType],pQMsg->u4IfIndex); 

        DcbxLock ();

        switch (pQMsg->u4MsgType)
        {
            case L2IWF_LLDP_APPL_TLV_RECV:
            case L2IWF_LLDP_APPL_TLV_AGED:
            case L2IWF_LLDP_MULTIPLE_PEER_NOTIFY:
                /*Process the TLV related Application Message received 
                 *    from the LLDP */
                if (pQMsg->u4MsgType == L2IWF_LLDP_MULTIPLE_PEER_NOTIFY)
                {
                    DCBX_TRC_ARG1 (DCBX_CRITICAL_TRC, "DcbxQueMsgHandler: "
                            "Received Multiple peer event for port %d !!!\r\n",pQMsg->u4IfIndex);
                }
                DcbxQueProcessApplTlvMsg (pQMsg);
                break;
            case L2IWF_LLDP_APPL_RE_REG:
                /*Process the Applicatin Re Registratin Message received 
                 *    from the LLDP */
                DcbxQueProcessReRegMsg (pQMsg);
                break;
            case DCBX_CREATE_PORT_MSG:
                /* Process the Creation of Port message */
                DcbxUtlPortCreate (pQMsg->u4IfIndex);
                L2_SYNC_GIVE_SEM ();
                break;
            case DCBX_DELETE_PORT_MSG:
                /* Process the Creation of Port message */
                DcbxUtlPortDelete (pQMsg->u4IfIndex);
                L2_SYNC_GIVE_SEM ();
                break;
            case DCBX_ADD_LAG_MEMBER_MSG:
                /* 
                   Currently, DCBX is not supported in Port-Channel 
                   interface. Hence, ignoring the addition of port in 
                   Port Channel processing. 
                */
                /* Process the Addition of port in Port Channel */
                /* DcbxUtlAddPortToLAG (pQMsg->unMsgParam.u4PortChannelId); 
                */
                L2_SYNC_GIVE_SEM ();
                break;
            case DCBX_REM_LAG_MEMBER_MSG:
                /* 
                   Currently, DCBX is not supported in Port-Channel 
                   interface. Hence, ignoring the deletion of port in 
                   Port Channel processing. 
                */
                /* Process the Deletion of port in Port Channel */
                /* DcbxUtlDelPortFromLAG (pQMsg->u4IfIndex,
                                       pQMsg->unMsgParam.u4PortChannelId);
                */
                L2_SYNC_GIVE_SEM ();
                break;
            case DCBX_RM_MSG:
                /* Process Dcbx RM Message */
                DcbxRedHandleRmEvents (pQMsg);
                break;
#ifdef MBSM_WANTED
            case DCBX_MBSM_MSG_CARD_INSERT:
                DCBX_TRC (DCBX_CRITICAL_TRC, "DcbxQueMsgHandler:"
                  "DCBX DCBX_MBSM_MSG_CARD_INSERT recieved !!!\r\n");
                DcbxMbsmProcessMsg (pQMsg->unMsgParam.pMbsmProtoMsg);
                MemReleaseMemBlock (gDcbxGlobalInfo.DcbxMbsmPoolId,
                                    (UINT1 *) (pQMsg->unMsgParam.
                                               pMbsmProtoMsg));
                break;
#endif
            case L2IWF_LLDP_TX_ENABLE_NOTIFY:
            case L2IWF_LLDP_RX_ENABLE_NOTIFY:
            case L2IWF_LLDP_TX_DISABLE_NOTIFY:
            case L2IWF_LLDP_RX_DISABLE_NOTIFY:
                CEEUtlHandleLLdpTxRxNotify(pQMsg->u4IfIndex, pQMsg->u4MsgType);
                break;
            default:
                DCBX_TRC (DCBX_FAILURE_TRC, "DcbxQueMsgHandler:"
                          "Invalid Message type is received. !!!\r\n");
                /* Default : Do Nothing. Print the trace Message */
                break;
        }
        /* Release the Memory for the respective Queue Message since it is
         *  Processed */
        MemReleaseMemBlock (gDcbxGlobalInfo.DcbxQPktPoolId, (UINT1 *) pQMsg);

        DcbxUnLock ();
    }
    return;
}

/***************************************************************************
 *  FUNCTION NAME : DcbxQueProcessApplTlvMsg
 * 
 *  DESCRIPTION   : This function is used to process the TLV message posted
 *                  in DBCX queue.
 * 
 *  INPUT         : pMsg - Message Buffer
 * 
 *  OUTPUT        : None
 * 
 *  RETURNS       : None
 * 
 * **************************************************************************/
PUBLIC VOID
DcbxQueProcessApplTlvMsg (tDcbxQueueMsg * pMsg)
{
    static tDcbxAppEntry TempDcbxAppEntry;
    /* This contains port list array which results in stack size
     * exceeds 500. So declaring this varaible as static */
    tDcbxAppEntry      *pDcbxAppEntry = NULL;
    tDcbxAppInfo        DcbxAppInfo;

    /* CEE : Version Resolution Starts */
    if (DCBX_SUCCESS != DcbxUtlHandleVerResolution(pMsg))
    {
        DCBX_TRC_ARG1 (DCBX_TLV_TRC, "DcbxQueProcessApplTlvMsg: "
                  "Version Resolution not successful on port %d "
                  "!!!\r\n", pMsg->u4IfIndex);
        return;
    }

    /* Packet Dump to Console */
    if (gDcbxGlobalInfo.u4DCBXTrcFlag & DCBX_TLV_TRC)
    {
        DcbxPktDump (pMsg->unMsgParam.ApplTlvParam.au1DcbxTlv, 
                pMsg->unMsgParam.ApplTlvParam.u2RxTlvLen, 
                DCBX_RX);
    }

    /* CEE : Version Resolution ends */
    MEMSET (&TempDcbxAppEntry, DCBX_ZERO, sizeof (tDcbxAppEntry));
    MEMSET (&DcbxAppInfo, DCBX_ZERO, sizeof (tDcbxAppInfo));
    MEMCPY (&(TempDcbxAppEntry.DcbxAppId),
            &(pMsg->DcbxAppId), sizeof (tDcbxAppId));

    /* Get the application entry from the application table */
    pDcbxAppEntry =
        (tDcbxAppEntry *) RBTreeGet (gDcbxGlobalInfo.pRbDcbxAppTbl,
                                     (tRBElem *) & TempDcbxAppEntry);

    /* If entry is not present or the corresponding port registration bit 
     * is not set then return without processing the message */
    if ((pDcbxAppEntry == NULL) ||
        (pDcbxAppEntry->ApplMappedIfPortList[pMsg->u4IfIndex] == OSIX_FALSE))
    {
        DCBX_TRC_ARG1 (DCBX_TLV_TRC | DCBX_CONTROL_PLANE_TRACE |
                       DCBX_CRITICAL_TRC,
                       "DcbxQueProcessApplTlvMsg: "
                       "DCBX port %d process message from LLDP "
                       "FAILED - Not Registered!!!\r\n", pMsg->u4IfIndex);
        return;
    }

    /* If the entry is present, then post the TLV information using 
     * the application callbcak function to the application */
    DcbxAppInfo.u4IfIndex = pMsg->u4IfIndex;
    DcbxAppInfo.u4MsgType = pMsg->u4MsgType;
    MEMCPY (&(DcbxAppInfo.ApplTlvParam), &(pMsg->unMsgParam.ApplTlvParam),
            sizeof (tApplTlvParam));

    pDcbxAppEntry->pAppCallBackFn (&DcbxAppInfo);

    return;
}

/***************************************************************************
 *  FUNCTION NAME : DcbxQueProcessReRegMsg
 * 
 *  DESCRIPTION   : This function is used to process the Re Registration
 *                  message posted in DCBX Que
 * 
 *  INPUT         : pMsg - Message Buffer
 * 
 *  OUTPUT        : None
 * 
 *  RETURNS       : None
 * 
 * **************************************************************************/
PUBLIC VOID
DcbxQueProcessReRegMsg (tDcbxQueueMsg * pMsg)
{
    static tDcbxAppEntry TempDcbxAppEntry;
    /* This contains port list array which results in stack size
     * exceeds 500. So declaring this varaible as static */
    tDcbxAppEntry      *pDcbxAppEntry = NULL;
    tDcbxAppInfo        DcbxAppInfo;
    UINT4               u4Count = DCBX_ZERO;

    MEMSET (&TempDcbxAppEntry, DCBX_ZERO, sizeof (tDcbxAppEntry));
    MEMSET (&DcbxAppInfo, DCBX_ZERO, sizeof (tDcbxAppInfo));
    MEMCPY (&(TempDcbxAppEntry.DcbxAppId),
            &(pMsg->DcbxAppId), sizeof (tDcbxAppId));

    DCBX_TRC_ARG1 (DCBX_CONTROL_PLANE_TRC, "DcbxQueProcessReRegMsg:"
            "Received RE_REG request for port %d \r\n",
            pMsg->u4IfIndex); 

    /* Get the application entry from the apllication table */
    pDcbxAppEntry =
        (tDcbxAppEntry *) RBTreeGet (gDcbxGlobalInfo.pRbDcbxAppTbl,
                                     (tRBElem *) & TempDcbxAppEntry);
    /* If not present then no need to send re-registration request */
    if (pDcbxAppEntry == NULL)
    {
        DCBX_TRC (DCBX_FAILURE_TRC, "In DcbxQueProcessReRegMsg:"
                  "DCBX port process message from LLDP"
                  "FAILED - Not Registered!!!\r\n");
        return;

    }
    /* Scan the ports list and  then send the Re-registration 
     * request to all the ports that are registered with this application 
     * in DCBX */
    for (u4Count = DCBX_ZERO; u4Count < DCBX_MAX_PORT_LIST_SIZE; u4Count++)
    {
        if (pDcbxAppEntry->ApplMappedIfPortList[u4Count] == OSIX_TRUE)
        {
            DcbxAppInfo.u4IfIndex = u4Count;
            DcbxAppInfo.u4MsgType = pMsg->u4MsgType;
            pDcbxAppEntry->pAppCallBackFn (&DcbxAppInfo);
        }
    }
    return;
}
