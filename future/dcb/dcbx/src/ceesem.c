/*************************************************************************
 * Copyright (C) 2010 Aricent Inc . All Rights Reserved
 * $Id: ceesem.c,v 1.3 2016/05/25 10:06:09 siva Exp $
 * Description: This file contains CEE utility function.
****************************************************************************/
#include "dcbxinc.h"

/***************************************************************************
 *  FUNCTION NAME : CEESemRun
 * 
 *  DESCRIPTION   : Based on the received event this function will invoke 
 *                  appropriate action routines.
 * 
 *  INPUT         : u1TlvType       - Type of TLV recieved.
 *                  pCeeAppTlvParam - Application Info structure 
 *                  i4Event - Event received 
 * 
 *  OUTPUT        : None
 * 
 *  RETURNS       : None
 * 
 * **************************************************************************/
PUBLIC VOID
CEESemRun (UINT1 u1TlvType, tCeeTLVInfo * pCeeAppTlvParam, INT4 i4Event)
{
    UINT1              u1ActionProcId       = (UINT1) CEE_INVALID_VALUE;
    tETSPortEntry      *pETSPortEntry       = NULL;
    tPFCPortEntry      *pPFCPortEntry       = NULL;
    tAppPriPortEntry   *pAppPriPortEntry    = NULL;

    switch(u1TlvType)
    {
        case CEE_ETS_TLV_TYPE :/*ETS TLV*/
            pETSPortEntry = ETSUtlGetPortEntry (pCeeAppTlvParam->u4IfIndex);
            if (pETSPortEntry == NULL)
            {
                DCBX_TRC (DCBX_FAILURE_TRC,  "CEESemRun: "
                        "No ETS Port is created!!!\r\n");
                return;
            }

            if ((pETSPortEntry->u1ETSAdminMode != ETS_PORT_MODE_AUTO) ||
                    (pETSPortEntry->u1ETSConfigTxStatus != ETS_ENABLED) ||
                    (gETSGlobalInfo.u1ETSModStatus != ETS_ENABLED))
            {
                DCBX_TRC_ARG1 (DCBX_TLV_TRC,  "In CEESemRun: "
                        "Ignoring ETS TLV, "
                        "as TxStatus/ModuleStatus is not enabled "
                        "or mode is not auto!!! for the port %d\r\n",
                        pCeeAppTlvParam->u4IfIndex);
                return;                                
            }

            DCBX_TRC_ARG3 (DCBX_SEM_TRC, "CEE-SEM[%d]: CEESemRun: "
                    "Received Event = %s in State = %s \r\n",
                    pCeeAppTlvParam->u4IfIndex,
                    gau1CeeEvntStr[i4Event],
                    gau1CeeStateStr[pETSPortEntry->u1CurrentState]);
            u1ActionProcId =
                gau1CeeSem[i4Event][pETSPortEntry->u1CurrentState];

            break;

        case CEE_PFC_TLV_TYPE :/*PFC TLV*/
            /* Get the PFC port entry  and if the port entry is not present
             * then return without processing the message from DCBX */
            pPFCPortEntry = PFCUtlGetPortEntry (pCeeAppTlvParam->u4IfIndex);
            if (pPFCPortEntry == NULL)
            {
                DCBX_TRC (DCBX_FAILURE_TRC, "CEESemRun: "
                        "No PFC Port is created!!!\r\n");
                return;
            }
            
            if ((pPFCPortEntry->u1PFCAdminMode != PFC_PORT_MODE_AUTO) ||
                    (pPFCPortEntry->u1PFCTxStatus != PFC_ENABLED) ||
                    (gPFCGlobalInfo.u1PFCModStatus != PFC_ENABLED))
            {
                DCBX_TRC_ARG1 (DCBX_TLV_TRC,  "In CEESemRun: "
                        "Ignoring PFC TLV, "
                        "as TxStatus/ModuleStatus is not enabled "
                        "or mode is not auto!!! for the port %d\r\n",
                        pCeeAppTlvParam->u4IfIndex);
                return;                                
            }

            DCBX_TRC_ARG3 (DCBX_SEM_TRC, "CEE-SEM[%d]: CEESemRun: "
                    "Received Event = %s in State = %s \r\n",
                    pCeeAppTlvParam->u4IfIndex,
                    gau1CeeEvntStr[i4Event], 
                    gau1CeeStateStr[pPFCPortEntry->u1CurrentState]);
            u1ActionProcId =
                gau1CeeSem[i4Event][pPFCPortEntry->u1CurrentState];
            break;

        case CEE_APP_PRI_TLV_TYPE :/*Application Priority TLV*/
            pAppPriPortEntry = AppPriUtlGetPortEntry (pCeeAppTlvParam->u4IfIndex);
            if (pAppPriPortEntry == NULL)
            {
                DCBX_TRC (DCBX_FAILURE_TRC, "CEESemRun:"
                        " No Applciation Priority Port is created!!!\r\n");
                return;
            }

            if ((pAppPriPortEntry->u1AppPriAdminMode != APP_PRI_PORT_MODE_AUTO) ||
                    (pAppPriPortEntry->u1AppPriTxStatus != APP_PRI_ENABLED) ||
                    (gAppPriGlobalInfo.u1AppPriModStatus != APP_PRI_ENABLED))
            {
                DCBX_TRC_ARG1 (DCBX_TLV_TRC,  "In CEESemRun: "
                        "Ignoring Application Priority TLV, "
                        "as TxStatus/ModuleStatus is not enabled "
                        "or mode is not auto!!! for the port %d\r\n",
                        pCeeAppTlvParam->u4IfIndex);
                return;                                
            }

            DCBX_TRC_ARG3 (DCBX_SEM_TRC, "CEE-SEM[%d]: CEESemRun: "
                    "Received Event = %s in State = %s\r\n",
                    pCeeAppTlvParam->u4IfIndex,
                    gau1CeeEvntStr[i4Event], 
                    gau1CeeStateStr[pAppPriPortEntry->u1CurrentState]);
            u1ActionProcId =
                gau1CeeSem[i4Event][pAppPriPortEntry->u1CurrentState];
            break;
        default:
            {
                DCBX_TRC_ARG1 (DCBX_FAILURE_TRC, "CEE-SEM[%d]: CEESemRun: "
                        "Invalid TLV type!!!\r\n", pCeeAppTlvParam->u4IfIndex);
                return;
            }
    }

    /* Call the correcponding Action Procedure */
    if (u1ActionProcId < CEE_MAX_SEM_FN_PTRS)
    {
        (*gaCeeActionProc[u1ActionProcId]) (u1TlvType, pCeeAppTlvParam);
    }
    return;
}

/***************************************************************************
 *  FUNCTION NAME : CEESemEventIgnore
 * 
 *  DESCRIPTION   : This function will handle the invalid events
 * 
 *  INPUT         : u1TlvType       - Type of TLV recieved.
 *                  pCeeAppTlvParam - Application Info structure 
 * 
 *  OUTPUT        : None
 * 
 *  RETURNS       : None
 * 
 * **************************************************************************/
 VOID
CEESemEventIgnore (UINT1 u1TlvType, tCeeTLVInfo * pCeeAppTlvParam)
{
    UNUSED_PARAM (u1TlvType);
    UNUSED_PARAM (pCeeAppTlvParam);
    DCBX_TRC (DCBX_TLV_TRC , "CEESemEventIgnore:"
            "Event impossible!!!\r\n");
    return;
}
/***************************************************************************
 *  FUNCTION NAME : CEESmStateDelAgedInfo
 * 
 *  DESCRIPTION   : This procedure is invoked when enters to the state 
 *                  CEE_ST_DELETE_AGED_INFO 
 *
 *  INPUT         : u1TlvType       - Type of TLV recieved.
 *                  pCeeAppTlvParam - Application Info structure 
 * 
 *  OUTPUT        : None
 * 
 *  RETURNS       : None
 * 
 * **************************************************************************/
    VOID
CEESmStateDelAgedInfo (UINT1 u1TlvType, tCeeTLVInfo * pCeeAppTlvParam)
{
    tETSPortEntry      *pETSPortEntry = NULL;
    tPFCPortEntry      *pPFCPortEntry = NULL;
    tAppPriPortEntry   *pAppPriPortEntry = NULL;
    tDcbxCEETrapMsg     NotifyInfo;

    MEMSET(&NotifyInfo, DCBX_ZERO, sizeof(tDcbxCEETrapMsg));
    NotifyInfo.u4IfIndex = pCeeAppTlvParam->u4IfIndex; 

    switch(u1TlvType)
    {
        case CEE_ETS_TLV_TYPE :/*ETS TLV*/
            pETSPortEntry = ETSUtlGetPortEntry (pCeeAppTlvParam->u4IfIndex);
            if (pETSPortEntry == NULL)
            {
                DCBX_TRC (DCBX_FAILURE_TRC,  "CEESmStateDelAgedInfo:"
                        " No ETS Port is created!!!\r\n");
                return;
            }

            pETSPortEntry->bETSSyncd = DCBX_DISABLED;
            pETSPortEntry->bETSError = DCBX_ENABLED;

            /* Raise the Trap */
            NotifyInfo.unTrapMsg.u1FeatType = ETS_TRAP;
            DcbxCEESendPortNotification(&NotifyInfo, DCBX_PEER_TIMEOUT_TRAP);

            CEEUtlHandleETSAgeOut (pETSPortEntry);

            SET_DCBX_STATUS (pETSPortEntry, pETSPortEntry->u1ETSDcbxSemType, 
                    DCBX_RED_ETS_DCBX_STATUS_INFO, DCBX_STATUS_PEER_NO_ADV_DCBX);

            DCBX_TRC_ARG2 (DCBX_CONTROL_PLANE_TRC,
                    "CEE-SEM[%d]: Moving from  %s state to CEE_ST_DELETE_AGED_INFO state\r\n",
                    pCeeAppTlvParam->u4IfIndex, gau1CeeStateStr[pETSPortEntry->u1CurrentState]);

            CEE_SET_CURRENT_STATE (pETSPortEntry, CEE_ST_DELETE_AGED_INFO);
            CEESmStateWaitForFeatTlv (u1TlvType, pCeeAppTlvParam);

            break;

        case CEE_PFC_TLV_TYPE :/*PFC TLV*/

            /* Get the PFC port entry  and if the port entry is not present
             * then return without processing the message from DCBX */
            pPFCPortEntry = PFCUtlGetPortEntry (pCeeAppTlvParam->u4IfIndex);
            if (pPFCPortEntry == NULL)
            {
                DCBX_TRC_ARG1 (DCBX_FAILURE_TRC, "CEESmStateDelAgedInfo:"
                        " Entry not created for PFC on Port[%d]!!!\r\n",
                        pCeeAppTlvParam->u4IfIndex);
                return;
            }

            pPFCPortEntry->bPFCSyncd = DCBX_DISABLED;
            pPFCPortEntry->bPFCError = DCBX_ENABLED;

            NotifyInfo.unTrapMsg.u1FeatType = PFC_TRAP;
            DcbxCEESendPortNotification(&NotifyInfo,DCBX_PEER_TIMEOUT_TRAP);

            CEEUtlHandlePFCAgeOut (pPFCPortEntry);

            SET_DCBX_STATUS (pPFCPortEntry, pPFCPortEntry->u1PFCDcbxSemType, 
                    DCBX_RED_PFC_DCBX_STATUS_INFO, DCBX_STATUS_PEER_NO_ADV_DCBX);

            DCBX_TRC_ARG2 (DCBX_CONTROL_PLANE_TRC,
                    "CEE-SEM[%d]: Moving from  %s state to CEE_ST_DELETE_AGED_INFO state\r\n",
                    pCeeAppTlvParam->u4IfIndex,gau1CeeStateStr[pPFCPortEntry->u1CurrentState]);

            CEE_SET_CURRENT_STATE (pPFCPortEntry, CEE_ST_DELETE_AGED_INFO);

            CEESmStateWaitForFeatTlv (u1TlvType, pCeeAppTlvParam);
            break;

        case CEE_APP_PRI_TLV_TYPE :/*Application Priority TLV*/
            pAppPriPortEntry = AppPriUtlGetPortEntry (pCeeAppTlvParam->u4IfIndex);
            if (pAppPriPortEntry == NULL)
            {
                DCBX_TRC_ARG1 (DCBX_FAILURE_TRC, "CEESmStateDelAgedInfo:"
                        " Entry not created for Applciation Priority on Port[%d]!!!\r\n",
                        pCeeAppTlvParam->u4IfIndex);
                return;
            }

            pAppPriPortEntry->bAppPriSyncd = DCBX_DISABLED;
            pAppPriPortEntry->bAppPriError = DCBX_ENABLED;

            /* Send trap */
            NotifyInfo.unTrapMsg.u1FeatType = APP_PRI_TRAP;
            DcbxCEESendPortNotification(&NotifyInfo, DCBX_PEER_TIMEOUT_TRAP);

            /* Clear the remote entries */
            CEEUtlHandleAppPriAgeOut (pAppPriPortEntry);

            SET_DCBX_STATUS (pAppPriPortEntry, pAppPriPortEntry->u1AppPriDcbxSemType, 
                    DCBX_RED_APP_PRI_DCBX_STATUS_INFO, DCBX_STATUS_PEER_NO_ADV_DCBX);

            DCBX_TRC_ARG2 (CONTROL_PLANE_TRC,
                    "CEE-SEM[%d]: Moving from %s state to "
                    "CEE_ST_DELETE_AGED_INFO state\r\n", pCeeAppTlvParam->u4IfIndex,
                    gau1CeeStateStr[pAppPriPortEntry->u1CurrentState]);

            CEE_SET_CURRENT_STATE (pAppPriPortEntry, CEE_ST_DELETE_AGED_INFO);

            CEESmStateWaitForFeatTlv (u1TlvType, pCeeAppTlvParam);
            break;
        default:
            {
                DCBX_TRC_ARG1 (DCBX_FAILURE_TRC, 
                        "CEE-SEM[%d]: CEESmStateDelAgedInfo: "
                        "Invalid TLV type!!!\r\n", pCeeAppTlvParam->u4IfIndex);
                break;
            }
    }
    return;
}
/***************************************************************************
 *  FUNCTION NAME : CEESmStateRxFeatTlv
 * 
 *  DESCRIPTION   : This function is invoked when enters to the state 
 *                  CEE_ST_WAIT_FOR_FEAT_TLV
 *  INPUT         : u1TlvType       - Type of TLV recieved.
 *                  pCeeAppTlvParam - Application Info structure 
 * 
 *  OUTPUT        : None
 * 
 *  RETURNS       : None
 * 
 * **************************************************************************/
 VOID
CEESmStateRxFeatTlv (UINT1 u1TlvType, tCeeTLVInfo * pCeeAppTlvParam)
{
    tETSPortEntry      *pETSPortEntry = NULL;
    tPFCPortEntry      *pPFCPortEntry = NULL;
    tAppPriPortEntry   *pAppPriPortEntry = NULL;
    UINT1              u1OperState = DCBX_ZERO;

    switch(u1TlvType)
    {
        case CEE_ETS_TLV_TYPE :/*ETS TLV*/
            pETSPortEntry = ETSUtlGetPortEntry (pCeeAppTlvParam->u4IfIndex);
            if (pETSPortEntry == NULL)
            {
                DCBX_TRC (DCBX_FAILURE_TRC, "CEESmStateRxFeatTlv:"
                        " No ETS Port is created!!!\r\n");
                return;
            }
            if (CEEHandleETSTlv(pETSPortEntry, pCeeAppTlvParam, &u1OperState) == DCBX_SUCCESS)
            {
                pETSPortEntry->u4ETSConfRxTLVCount++;

                if (u1OperState == CEE_OPER_USE_LOCAL_CFG)
                {
                    CEESmStateUseLocalCfg(u1TlvType,pCeeAppTlvParam);
                }
                else if (u1OperState == CEE_OPER_USE_PEER_CFG)
                {
                    CEESmStateUsePeerCfg(u1TlvType,pCeeAppTlvParam);
                }
                else 
                {
                    CEESmStateCfgNotCompatible(u1TlvType,pCeeAppTlvParam);
                }
            }
            else
            {
                pETSPortEntry->bETSSyncd = DCBX_ENABLED;
                pETSPortEntry->bETSError = DCBX_ENABLED;
                CEEUtlHandleETSAgeOut (pETSPortEntry);
                CEESmStateWaitForFeatTlv (u1TlvType, pCeeAppTlvParam);
            }

            break;

        case CEE_PFC_TLV_TYPE :/*PFC TLV*/
            pPFCPortEntry = PFCUtlGetPortEntry (pCeeAppTlvParam->u4IfIndex);

            /* Get the PFC port entry  and if the port entry is not present
             *      * then return without processing the message from DCBX */

            if (pPFCPortEntry == NULL)
            {
                DCBX_TRC (DCBX_FAILURE_TRC, "CEESmStateRxFeatTlv:"
                        " No PFC Port is created!!!\r\n");
                return;
            }
            if (CEEHandlePFCTlv(pPFCPortEntry, pCeeAppTlvParam, &u1OperState) == DCBX_SUCCESS)
            {

                pPFCPortEntry->u4PFCRxTLVCount++;

                if (u1OperState == CEE_OPER_USE_LOCAL_CFG)
                {
                    CEESmStateUseLocalCfg(u1TlvType,pCeeAppTlvParam);
                }
                else if (u1OperState == CEE_OPER_USE_PEER_CFG)
                {
                    CEESmStateUsePeerCfg(u1TlvType,pCeeAppTlvParam);
                }
                else 
                {
                    CEESmStateCfgNotCompatible(u1TlvType,pCeeAppTlvParam);
                }
            }
            else
            {
                pPFCPortEntry->bPFCSyncd = DCBX_ENABLED;
                pPFCPortEntry->bPFCError = DCBX_ENABLED;
                CEEUtlHandlePFCAgeOut (pPFCPortEntry);
                CEESmStateWaitForFeatTlv (u1TlvType, pCeeAppTlvParam);
            }


            break;

        case CEE_APP_PRI_TLV_TYPE :/*Application Priority TLV*/
            pAppPriPortEntry = AppPriUtlGetPortEntry (pCeeAppTlvParam->u4IfIndex);
            if (pAppPriPortEntry == NULL)
            {
                DCBX_TRC (DCBX_FAILURE_TRC, "CEESmStateRxFeatTlv:"
                        " No Applciation Priority Port is created!!!\r\n");
                return;
            }
            if (CEEHandleAppPriTlv(pAppPriPortEntry, pCeeAppTlvParam, &u1OperState) == DCBX_SUCCESS)
            {

                pAppPriPortEntry->u4AppPriTLVRxCount++;
                if (u1OperState == CEE_OPER_USE_LOCAL_CFG)
                {
                    CEESmStateUseLocalCfg(u1TlvType,pCeeAppTlvParam);
                }
                else if (u1OperState == CEE_OPER_USE_PEER_CFG)
                {
                    CEESmStateUsePeerCfg(u1TlvType,pCeeAppTlvParam);
                }
                else 
                {
                    CEESmStateCfgNotCompatible(u1TlvType,pCeeAppTlvParam);
                }
            }
            else
            {
                pAppPriPortEntry->bAppPriSyncd = DCBX_ENABLED;
                pAppPriPortEntry->bAppPriError = DCBX_ENABLED;
                CEEUtlHandleAppPriAgeOut (pAppPriPortEntry);
                CEESmStateWaitForFeatTlv (u1TlvType, pCeeAppTlvParam);
            }

            break;
        default:
            {
                DCBX_TRC_ARG1 (DCBX_FAILURE_TRC, 
                        "CEE-SEM[%d]: CEESmStateRxFeatTlv: "
                        "Invalid TLV type!!!\r\n", pCeeAppTlvParam->u4IfIndex);
                break;
            }
    }
    return;
}
/***************************************************************************
 *  FUNCTION NAME : CEESmStateNoFeatTlv
 * 
 *  DESCRIPTION   : This function is invoked when the FEAT TLV is not
 *                  received.
 *
 *  INPUT         : u1TlvType       - Type of TLV recieved.
 *                  pCeeAppTlvParam - Application Info structure 
 * 
 *  OUTPUT        : None
 * 
 *  RETURNS       : None 
 * 
 * **************************************************************************/
 VOID
CEESmStateNoFeatTlv (UINT1 u1TlvType, tCeeTLVInfo * pCeeAppTlvParam)
{
    tDcbxCEETrapMsg     NotifyInfo;
    tETSPortEntry      *pETSPortEntry = NULL;
    tPFCPortEntry      *pPFCPortEntry = NULL;
    tAppPriPortEntry   *pAppPriPortEntry = NULL;
    UINT1               u1CurrentState = DCBX_ZERO;

    MEMSET(&NotifyInfo, DCBX_ZERO, sizeof(tDcbxCEETrapMsg));

    NotifyInfo.u4IfIndex = pCeeAppTlvParam->u4IfIndex; 
    switch(u1TlvType)
    {
        case CEE_ETS_TLV_TYPE :/*ETS TLV*/
            pETSPortEntry = ETSUtlGetPortEntry (pCeeAppTlvParam->u4IfIndex);
            if (pETSPortEntry == NULL)
            {
                DCBX_TRC_ARG1 (DCBX_FAILURE_TRC, "CEESmStateNoFeatTlv:"
                        " ETS entry not created on Port[%d]!!!\r\n",
                        pCeeAppTlvParam->u4IfIndex);
                return;
            }

            u1CurrentState = pETSPortEntry->u1CurrentState;

            pETSPortEntry->bETSSyncd = DCBX_ENABLED;
            pETSPortEntry->bETSError = DCBX_ENABLED;

            SET_DCBX_STATUS(pETSPortEntry, pETSPortEntry->u1ETSDcbxSemType, 
                    DCBX_RED_ETS_DCBX_STATUS_INFO, DCBX_STATUS_PEER_NO_ADV_FEAT);

            NotifyInfo.unTrapMsg.u1FeatType = ETS_TRAP;
            DcbxCEESendPortNotification(&NotifyInfo,DCBX_NO_FEAT_TLV_TRAP);

            CEEUtlHandleETSAgeOut (pETSPortEntry);

            DCBX_TRC_ARG2 (CONTROL_PLANE_TRC,
                    "CEE-SEM[%d]: Moving from %s state to "
                    "CEE_ST_WAIT_FOR_FEAT_TLV state\r\n", pCeeAppTlvParam->u4IfIndex,
                    gau1CeeStateStr[u1CurrentState]);

            CEESmStateWaitForFeatTlv (u1TlvType, pCeeAppTlvParam);

            break;

        case CEE_PFC_TLV_TYPE :/*PFC TLV*/
            /* Get the PFC port entry and if the port entry is not present
             * then return without processing the message from DCBX */
            pPFCPortEntry = PFCUtlGetPortEntry (pCeeAppTlvParam->u4IfIndex);
            if (pPFCPortEntry == NULL)
            {
                DCBX_TRC_ARG1 (DCBX_FAILURE_TRC, "CEESmStateNoFeatTlv:"
                        " PFC entry not created on Port[%d]!!!\r\n",
                        pCeeAppTlvParam->u4IfIndex);
                return;
            }

            u1CurrentState = pPFCPortEntry->u1CurrentState;

            pPFCPortEntry->bPFCSyncd = DCBX_ENABLED;
            pPFCPortEntry->bPFCError = DCBX_ENABLED;

            SET_DCBX_STATUS (pPFCPortEntry, pPFCPortEntry->u1PFCDcbxSemType, 
                    DCBX_RED_PFC_DCBX_STATUS_INFO, DCBX_STATUS_PEER_NO_ADV_FEAT);

            NotifyInfo.unTrapMsg.u1FeatType = PFC_TRAP;
            DcbxCEESendPortNotification(&NotifyInfo,DCBX_NO_FEAT_TLV_TRAP);

            CEEUtlHandlePFCAgeOut (pPFCPortEntry);

            DCBX_TRC_ARG2 (CONTROL_PLANE_TRC,
                    "CEE-SEM[%d]: Moving from %s state to "
                    "CEE_ST_WAIT_FOR_FEAT_TLV state\r\n", pCeeAppTlvParam->u4IfIndex,
                    gau1CeeStateStr[u1CurrentState]);

            CEESmStateWaitForFeatTlv (u1TlvType, pCeeAppTlvParam);

            break;
        case CEE_APP_PRI_TLV_TYPE :/*Application Priority TLV*/
            pAppPriPortEntry = AppPriUtlGetPortEntry (pCeeAppTlvParam->u4IfIndex);
            if (pAppPriPortEntry == NULL)
            {
                DCBX_TRC_ARG1 (DCBX_FAILURE_TRC, "CEESmStateNoFeatTlv:"
                        " Applciation Priority entry not created on Port!!!\r\n",
                        pCeeAppTlvParam->u4IfIndex);
                return;
            }

            u1CurrentState = pAppPriPortEntry->u1CurrentState;

            pAppPriPortEntry->bAppPriSyncd = DCBX_ENABLED;
            pAppPriPortEntry->bAppPriError = DCBX_ENABLED;

            SET_DCBX_STATUS (pAppPriPortEntry, pAppPriPortEntry->u1AppPriDcbxSemType, 
                    DCBX_RED_APP_PRI_DCBX_STATUS_INFO, DCBX_STATUS_PEER_NO_ADV_FEAT);

            NotifyInfo.unTrapMsg.u1FeatType = APP_PRI_TRAP;
            DcbxCEESendPortNotification(&NotifyInfo,DCBX_NO_FEAT_TLV_TRAP);

            CEEUtlHandleAppPriAgeOut (pAppPriPortEntry);

            DCBX_TRC_ARG2 (CONTROL_PLANE_TRC,
                    "CEE-SEM[%d]: Moving from %s state to "
                    "CEE_ST_WAIT_FOR_FEAT_TLV state\r\n", pCeeAppTlvParam->u4IfIndex,
                    gau1CeeStateStr[u1CurrentState]);

            CEESmStateWaitForFeatTlv (u1TlvType, pCeeAppTlvParam);

            break;
        default:
            {
                DCBX_TRC_ARG1 (DCBX_FAILURE_TRC, 
                        "CEE-SEM[%d]: CEESmStateNoFeatTlv: "
                        "Invalid TLV type!!!\r\n", pCeeAppTlvParam->u4IfIndex);
                break;
            }
    }

    return;
}

/***************************************************************************
 *  FUNCTION NAME : CEESmStateWaitForFeatTlv
 * 
 *  DESCRIPTION   : This function is invoked when enters to the state. 
 *                  CEE_ST_WAIT_FOR_FEAT_TLV 
 *
 *  INPUT         : u1TlvType       - Type of TLV recieved.
 *                  pCeeAppTlvParam - Application Info structure 
 * 
 *  OUTPUT        : None
 * 
 *  RETURNS       : None
 * 
 * **************************************************************************/
 VOID
CEESmStateWaitForFeatTlv (UINT1 u1TlvType, tCeeTLVInfo * pCeeAppTlvParam)
{
    tETSPortEntry      *pETSPortEntry = NULL;
    tPFCPortEntry      *pPFCPortEntry = NULL;
    tAppPriPortEntry   *pAppPriPortEntry = NULL;
    switch(u1TlvType)
    {
        case CEE_ETS_TLV_TYPE :/*ETS TLV*/
            pETSPortEntry = ETSUtlGetPortEntry (pCeeAppTlvParam->u4IfIndex);
            if (pETSPortEntry == NULL)
            {
                DCBX_TRC (DCBX_FAILURE_TRC, "CEESmStateWaitForFeatTlv:"
                        " No ETS Port is created!!!\r\n");
                return;
            }
            DCBX_TRC_ARG2 (CONTROL_PLANE_TRC,
                    "CEE-SEM[%d]: Moving from  %s state to CEE_ST_WAIT_FOR_FEAT_TLV state\r\n",
                    pCeeAppTlvParam->u4IfIndex,gau1CeeStateStr[pETSPortEntry->u1CurrentState]);
            CEE_SET_CURRENT_STATE (pETSPortEntry, CEE_ST_WAIT_FOR_FEAT_TLV);


            break;

        case CEE_PFC_TLV_TYPE :/*PFC TLV*/
            pPFCPortEntry = PFCUtlGetPortEntry (pCeeAppTlvParam->u4IfIndex);

            /* Get the PFC port entry  and if the port entry is not present
             *      * then return without processing the message from DCBX */

            if (pPFCPortEntry == NULL)
            {
                DCBX_TRC (DCBX_FAILURE_TRC, "CEESmStateWaitForFeatTlv:"
                        " No PFC Port is created!!!\r\n");
                return;
            }
            DCBX_TRC_ARG2 (CONTROL_PLANE_TRC,
                    "CEE-SEM[%d]: Moving from  %s state to CEE_ST_WAIT_FOR_FEAT_TLV state\r\n",
                    pCeeAppTlvParam->u4IfIndex,gau1CeeStateStr[pPFCPortEntry->u1CurrentState]);
            CEE_SET_CURRENT_STATE (pPFCPortEntry, CEE_ST_WAIT_FOR_FEAT_TLV);

            break;

        case CEE_APP_PRI_TLV_TYPE :/*Application Priority TLV*/
            pAppPriPortEntry = AppPriUtlGetPortEntry (pCeeAppTlvParam->u4IfIndex);
            if (pAppPriPortEntry == NULL)
            {
                DCBX_TRC (DCBX_FAILURE_TRC, "CEESmStateWaitForFeatTlv:"
                        " No Applciation Priority Port is created!!!\r\n");
                return;
            }
            DCBX_TRC_ARG2 (CONTROL_PLANE_TRC,
                    "CEE-SEM[%d]: Moving from  %s state to CEE_ST_WAIT_FOR_FEAT_TLV state\r\n",
                    pCeeAppTlvParam->u4IfIndex,gau1CeeStateStr[pAppPriPortEntry->u1CurrentState]);
            CEE_SET_CURRENT_STATE (pAppPriPortEntry, CEE_ST_WAIT_FOR_FEAT_TLV);

            break;
        default:
            {
                DCBX_TRC_ARG1 (DCBX_FAILURE_TRC, 
                        "CEE-SEM[%d]: CEESmStateWaitForFeatTlv: "
                        "Invalid TLV type!!!\r\n", pCeeAppTlvParam->u4IfIndex);
                break;
            }
    }
    return;
}
/***************************************************************************
 *  FUNCTION NAME : CEESmStateUseLocalCfg
 * 
 *  DESCRIPTION   : This function is invoked after receiving the FEAT_TLV 
 *                  based on operstate 
 *  INPUT         : u1TlvType       - Type of TLV recieved.
 *                  pCeeAppTlvParam - Application Info structure 
 * 
 *  OUTPUT        : None
 * 
 *  RETURNS       : None 
 * 
 * **************************************************************************/
PUBLIC VOID
CEESmStateUseLocalCfg (UINT1 u1TlvType, tCeeTLVInfo * pCeeAppTlvParam)
{
    tETSPortEntry      *pETSPortEntry = NULL;
    tPFCPortEntry      *pPFCPortEntry = NULL;
    tAppPriPortEntry   *pAppPriPortEntry = NULL;

    switch(u1TlvType)
    {
        case CEE_ETS_TLV_TYPE :/*ETS TLV*/
            pETSPortEntry = ETSUtlGetPortEntry (pCeeAppTlvParam->u4IfIndex);
            if (pETSPortEntry == NULL)
            {
                DCBX_TRC (DCBX_FAILURE_TRC, "CEESmStateUseLocalCfg:"
                        " No ETS Port is created!!!\r\n");
                return;
            }
            pETSPortEntry->bETSSyncd = DCBX_ENABLED;
            pETSPortEntry->bETSError = DCBX_DISABLED;
                ETSUtlOperStateChange(pETSPortEntry, ETS_OPER_INIT);
            DCBX_TRC_ARG2 (CONTROL_PLANE_TRC,
                    "CEE-SEM[%d]: Moving from  %s state to CEE_ST_USE_LOC_CFG state\r\n",
                    pCeeAppTlvParam->u4IfIndex,gau1CeeStateStr[pETSPortEntry->u1CurrentState]);
            CEE_SET_CURRENT_STATE (pETSPortEntry, CEE_ST_USE_LOC_CFG);
            CEESmStateWaitForFeatTlv (u1TlvType, pCeeAppTlvParam);


            break;

        case CEE_PFC_TLV_TYPE :/*PFC TLV*/
            pPFCPortEntry = PFCUtlGetPortEntry (pCeeAppTlvParam->u4IfIndex);

            /* Get the PFC port entry  and if the port entry is not present
             *      * then return without processing the message from DCBX */

            if (pPFCPortEntry == NULL)
            {
                DCBX_TRC (DCBX_FAILURE_TRC, "CEESmStateUseLocalCfg:"
                        " No PFC Port is created!!!\r\n");
                return;
            }
            pPFCPortEntry->bPFCSyncd = DCBX_ENABLED;
            pPFCPortEntry->bPFCError = DCBX_DISABLED;
            if (pPFCPortEntry->u1DcbxStatus == DCBX_STATUS_PEER_IN_ERROR)
            {
                PFCUtlOperStateChange(pPFCPortEntry, PFC_OPER_OFF);
            }
            else
            {
                PFCUtlOperStateChange(pPFCPortEntry, PFC_OPER_INIT);
            }

            DCBX_TRC_ARG2 (CONTROL_PLANE_TRC,
                    "CEE-SEM[%d]: Moving from  %s state to CEE_ST_USE_LOC_CFG state\r\n",
                    pCeeAppTlvParam->u4IfIndex,gau1CeeStateStr[pPFCPortEntry->u1CurrentState]);
            CEE_SET_CURRENT_STATE (pPFCPortEntry, CEE_ST_USE_LOC_CFG);
            CEESmStateWaitForFeatTlv (u1TlvType, pCeeAppTlvParam);

            break;

        case CEE_APP_PRI_TLV_TYPE :/*Application Priority TLV*/
            pAppPriPortEntry = AppPriUtlGetPortEntry (pCeeAppTlvParam->u4IfIndex);
            if (pAppPriPortEntry == NULL)
            {
                DCBX_TRC (DCBX_FAILURE_TRC, "CEESmStateUseLocalCfg:"
                        " No Applciation Priority Port is created!!!\r\n");
                return;
            }
            pAppPriPortEntry->bAppPriSyncd = DCBX_ENABLED;
            pAppPriPortEntry->bAppPriError = DCBX_DISABLED;
            if (pAppPriPortEntry->u1DcbxStatus == DCBX_STATUS_PEER_IN_ERROR)
            {
                AppPriUtlOperStateChange(pAppPriPortEntry, APP_PRI_OPER_OFF);
            }
            else
            {
                AppPriUtlOperStateChange(pAppPriPortEntry, APP_PRI_OPER_INIT);
            }
            DCBX_TRC_ARG2 (CONTROL_PLANE_TRC,
                    "CEE-SEM[%d]: Moving from  %s state to CEE_ST_USE_LOC_CFG state\r\n",
                    pCeeAppTlvParam->u4IfIndex,gau1CeeStateStr[pAppPriPortEntry->u1CurrentState]);
            CEE_SET_CURRENT_STATE (pAppPriPortEntry, CEE_ST_USE_LOC_CFG);

            CEESmStateWaitForFeatTlv (u1TlvType, pCeeAppTlvParam);

            break;
        default:
            {
                DCBX_TRC_ARG1 (DCBX_FAILURE_TRC, 
                        "CEE-SEM[%d]: CEESmStateUseLocalCfg: "
                        "Invalid TLV type!!!\r\n", pCeeAppTlvParam->u4IfIndex);
                break;
            }
    }
    return;
}

/***************************************************************************
 *  FUNCTION NAME : CEESmStateUsePeerCfg
 * 
 *  DESCRIPTION   : This function is invoked after receiving the FEAT_TLV 
 *                  based on operstate 
 *  INPUT         : u1TlvType       - Type of TLV recieved.
 *                  pCeeAppTlvParam - Application Info structure
 * 
 *  OUTPUT        : None
 * 
 *  RETURNS       : None 
 * 
 * **************************************************************************/
PUBLIC VOID
CEESmStateUsePeerCfg (UINT1 u1TlvType, tCeeTLVInfo * pCeeAppTlvParam)
{
    tETSPortEntry      *pETSPortEntry = NULL;
    tPFCPortEntry      *pPFCPortEntry = NULL;
    tAppPriPortEntry   *pAppPriPortEntry = NULL;

    switch(u1TlvType)
    {
        case CEE_ETS_TLV_TYPE :/*ETS TLV*/
            pETSPortEntry = ETSUtlGetPortEntry (pCeeAppTlvParam->u4IfIndex);
            if (pETSPortEntry == NULL)
            {
                DCBX_TRC (DCBX_FAILURE_TRC, "CEESmStateUsePeerCfg:"
                        " No ETS Port is created!!!\r\n");
                return;
            }
            pETSPortEntry->bETSSyncd = DCBX_ENABLED;
            pETSPortEntry->bETSError = DCBX_DISABLED;
            if (pETSPortEntry->u1DcbxStatus == DCBX_STATUS_PEER_IN_ERROR)
            {
                ETSUtlOperStateChange(pETSPortEntry, ETS_OPER_INIT);
                DCBX_TRC_ARG2 (CONTROL_PLANE_TRC,
                        "CEE-SEM[%d]: Moving from  %s state to CEE_ST_USE_LOC_CFG state\r\n",
                        pCeeAppTlvParam->u4IfIndex,gau1CeeStateStr[pETSPortEntry->u1CurrentState]);
                CEE_SET_CURRENT_STATE (pETSPortEntry, CEE_ST_USE_LOC_CFG);
            }
            else
            {
                ETSUtlOperStateChange(pETSPortEntry, ETS_OPER_RECO);
                DCBX_TRC_ARG2 (CONTROL_PLANE_TRC,
                        "CEE-SEM[%d]: Moving from  %s state to CEE_ST_USE_PEER_CFG state\r\n",
                        pCeeAppTlvParam->u4IfIndex,gau1CeeStateStr[pETSPortEntry->u1CurrentState]);
                CEE_SET_CURRENT_STATE (pETSPortEntry, CEE_ST_USE_PEER_CFG);
            }

            CEESmStateWaitForFeatTlv (u1TlvType, pCeeAppTlvParam);


            break;

        case CEE_PFC_TLV_TYPE :/*PFC TLV*/
            pPFCPortEntry = PFCUtlGetPortEntry (pCeeAppTlvParam->u4IfIndex);

            /* Get the PFC port entry  and if the port entry is not present
             *      * then return without processing the message from DCBX */

            if (pPFCPortEntry == NULL)
            {
                DCBX_TRC (DCBX_FAILURE_TRC, "CEESmStateUsePeerCfg:"
                        " No PFC Port is created!!!\r\n");
                return;
            }
            pPFCPortEntry->bPFCSyncd = DCBX_ENABLED;
            pPFCPortEntry->bPFCError = DCBX_DISABLED;
            if (pPFCPortEntry->u1DcbxStatus == DCBX_STATUS_PEER_IN_ERROR)
            {
                PFCUtlOperStateChange(pPFCPortEntry, PFC_OPER_OFF);
            }
            else
            {
                PFCUtlOperStateChange(pPFCPortEntry, PFC_OPER_RECO);
            }

            DCBX_TRC_ARG2 (CONTROL_PLANE_TRC,
                    "CEE-SEM[%d]: Moving from  %s state to CEE_ST_USE_PEER_CFG state\r\n",
                    pCeeAppTlvParam->u4IfIndex,gau1CeeStateStr[pPFCPortEntry->u1CurrentState]);
            CEE_SET_CURRENT_STATE (pPFCPortEntry, CEE_ST_USE_PEER_CFG);
            CEESmStateWaitForFeatTlv (u1TlvType, pCeeAppTlvParam);

            break;

        case CEE_APP_PRI_TLV_TYPE :/*Application Priority TLV*/
            pAppPriPortEntry = AppPriUtlGetPortEntry (pCeeAppTlvParam->u4IfIndex);
            if (pAppPriPortEntry == NULL)
            {
                DCBX_TRC (DCBX_FAILURE_TRC, "CEESmStateUsePeerCfg:"
                        " No Applciation Priority Port is created!!!\r\n");
                return;
            }
            pAppPriPortEntry->bAppPriSyncd = DCBX_ENABLED;
            pAppPriPortEntry->bAppPriError = DCBX_DISABLED;
            if (pAppPriPortEntry->u1DcbxStatus == DCBX_STATUS_PEER_IN_ERROR)
            {
                AppPriUtlOperStateChange(pAppPriPortEntry, APP_PRI_OPER_OFF);
            }
            else
            {
                AppPriUtlOperStateChange(pAppPriPortEntry, APP_PRI_OPER_RECO);
            }
            DCBX_TRC_ARG2 (CONTROL_PLANE_TRC,
                    "CEE-SEM[%d]: Moving from  %s state to CEE_ST_USE_PEER_CFG state\r\n",
                    pCeeAppTlvParam->u4IfIndex,gau1CeeStateStr[pAppPriPortEntry->u1CurrentState]);
            CEE_SET_CURRENT_STATE (pAppPriPortEntry, CEE_ST_USE_PEER_CFG);

            CEESmStateWaitForFeatTlv (u1TlvType, pCeeAppTlvParam);

            break;
        default:
            {
                DCBX_TRC_ARG1 (DCBX_FAILURE_TRC, 
                        "CEE-SEM[%d]: CEESmStateUsePeerCfg: "
                        "Invalid TLV type!!!\r\n", pCeeAppTlvParam->u4IfIndex);
                break;
            }
    }
    return;
}

/***************************************************************************
 *  FUNCTION NAME : CEESmStateCfgNotCompatible
 * 
 *  DESCRIPTION   : This function is invoked after receiving the FEAT_TLV 
 *                  based on operstate 
 *  INPUT         : u1TlvType       - Type of TLV recieved.
 *                  pCeeAppTlvParam - Application Info structure
 * 
 *  OUTPUT        : None
 * 
 *  RETURNS       : None
 * 
 * **************************************************************************/
PUBLIC VOID
CEESmStateCfgNotCompatible (UINT1 u1TlvType, tCeeTLVInfo * pCeeAppTlvParam)
{
    tETSPortEntry      *pETSPortEntry = NULL;
    tPFCPortEntry      *pPFCPortEntry = NULL;
    tAppPriPortEntry   *pAppPriPortEntry = NULL;
    UINT1              u1CurrentState = DCBX_ZERO;
    tDcbxCEETrapMsg     NotifyInfo;
    MEMSET(&NotifyInfo, DCBX_ZERO, sizeof(tDcbxCEETrapMsg));

    NotifyInfo.u4IfIndex = pCeeAppTlvParam->u4IfIndex; 
    switch(u1TlvType)
    {
        case CEE_ETS_TLV_TYPE :/*ETS TLV*/
            pETSPortEntry = ETSUtlGetPortEntry (pCeeAppTlvParam->u4IfIndex);
            if (pETSPortEntry == NULL)
            {
                DCBX_TRC (DCBX_FAILURE_TRC, "CEESmStateCfgNotCompatible:"
                        " No ETS Port is created!!!\r\n");
                return;
            }
            u1CurrentState = pETSPortEntry->u1CurrentState;
            pETSPortEntry->bETSSyncd = DCBX_ENABLED;
            pETSPortEntry->bETSError = DCBX_ENABLED;
            NotifyInfo.unTrapMsg.u1FeatType = ETS_TRAP;
            DcbxCEESendPortNotification(&NotifyInfo,DCBX_FEAT_ERROR_TRAP);
            ETSUtlOperStateChange(pETSPortEntry, ETS_OPER_INIT);
            DCBX_TRC_ARG2 (CONTROL_PLANE_TRC,
                    "CEE-SEM[%d]: Moving from  %s state to CEE_CFG_NOT_COMPATIBLE state\r\n",
                    pCeeAppTlvParam->u4IfIndex,gau1CeeStateStr[u1CurrentState]);
            CEE_SET_CURRENT_STATE (pETSPortEntry, CEE_ST_USE_LOC_CFG);
            CEESmStateWaitForFeatTlv (u1TlvType, pCeeAppTlvParam);


            break;

        case CEE_PFC_TLV_TYPE :/*PFC TLV*/
            pPFCPortEntry = PFCUtlGetPortEntry (pCeeAppTlvParam->u4IfIndex);

            /* Get the PFC port entry  and if the port entry is not present
             *      * then return without processing the message from DCBX */

            if (pPFCPortEntry == NULL)
            {
                DCBX_TRC (DCBX_FAILURE_TRC, "CEESmStateCfgNotCompatible:"
                        " No PFC Port is created!!!\r\n");
                return;
            }
            u1CurrentState = pPFCPortEntry->u1CurrentState;
            pPFCPortEntry->bPFCSyncd = DCBX_DISABLED;
            pPFCPortEntry->bPFCError = DCBX_ENABLED;
            NotifyInfo.unTrapMsg.u1FeatType = PFC_TRAP;
            DcbxCEESendPortNotification(&NotifyInfo,DCBX_FEAT_ERROR_TRAP);
            PFCUtlOperStateChange(pPFCPortEntry, PFC_OPER_OFF);

            DCBX_TRC_ARG2 (CONTROL_PLANE_TRC,
                    "CEE-SEM[%d]: Moving from  %s state to CEE_CFG_NOT_COMPATIBLE state\r\n",
                    pCeeAppTlvParam->u4IfIndex,gau1CeeStateStr[u1CurrentState]);
            CEE_SET_CURRENT_STATE (pPFCPortEntry, CEE_ST_CFG_NOT_CMP);
            CEESmStateWaitForFeatTlv (u1TlvType, pCeeAppTlvParam);

            break;

        case CEE_APP_PRI_TLV_TYPE :/*Application Priority TLV*/
            pAppPriPortEntry = AppPriUtlGetPortEntry (pCeeAppTlvParam->u4IfIndex);
            if (pAppPriPortEntry == NULL)
            {
                DCBX_TRC (DCBX_FAILURE_TRC, "CEESmStateCfgNotCompatible:"
                        " No Applciation Priority Port is created!!!\r\n");
                return;
            }
            u1CurrentState = pAppPriPortEntry->u1CurrentState;
            pAppPriPortEntry->bAppPriSyncd = DCBX_DISABLED;
            pAppPriPortEntry->bAppPriError = DCBX_ENABLED;
            NotifyInfo.unTrapMsg.u1FeatType = APP_PRI_TRAP;
            DcbxCEESendPortNotification(&NotifyInfo,DCBX_FEAT_ERROR_TRAP);
            AppPriUtlOperStateChange(pAppPriPortEntry, APP_PRI_OPER_OFF);
            DCBX_TRC_ARG2 (CONTROL_PLANE_TRC,
                    "CEE-SEM[%d]: Moving from  %s state to CEE_CFG_NOT_COMPATIBLE state\r\n",
                    pCeeAppTlvParam->u4IfIndex,gau1CeeStateStr[u1CurrentState]);
            CEE_SET_CURRENT_STATE (pAppPriPortEntry, CEE_ST_CFG_NOT_CMP);

            CEESmStateWaitForFeatTlv (u1TlvType, pCeeAppTlvParam);

            break;
        default:
            {
                DCBX_TRC_ARG1 (DCBX_FAILURE_TRC, 
                        "CEE-SEM[%d]: CEESmStateCfgNotCompatible: "
                        "Invalid TLV type!!!\r\n", pCeeAppTlvParam->u4IfIndex);
                break;
            }
    }
    return;
}

/****************************************************************************
 *  FUNCTION NAME : CEESmStateRxDupTlv
 * 
 *  DESCRIPTION   : This function is invoked when there is a change in feature 
 *  		        ERROR state
 *  INPUT         : u1TlvType       - Type of TLV recieved. 
 *                  pCeeAppTlvParam - Application Info structure 
 * 
 *  OUTPUT        : None
 * 
 *  RETURNS       : None
 * 
 * **************************************************************************/
 VOID
CEESmStateRxDupTlv (UINT1 u1TlvType, tCeeTLVInfo * pCeeAppTlvParam)
{
	tETSPortEntry      *pETSPortEntry = NULL;
	tPFCPortEntry      *pPFCPortEntry = NULL;
	tAppPriPortEntry   *pAppPriPortEntry = NULL;
	UINT1              u1CurrentState = DCBX_ZERO;
	tDcbxCEETrapMsg     NotifyInfo;
    MEMSET(&NotifyInfo, DCBX_ZERO, sizeof(tDcbxCEETrapMsg));

    NotifyInfo.u4IfIndex = pCeeAppTlvParam->u4IfIndex; 
	switch(u1TlvType)
	{
		case CEE_ETS_TLV_TYPE :/*ETS TLV*/
			pETSPortEntry = ETSUtlGetPortEntry (pCeeAppTlvParam->u4IfIndex);
			if (pETSPortEntry == NULL)
			{
				DCBX_TRC (DCBX_FAILURE_TRC, "CEESmStateRxDupTlv:"
						" No ETS Port is created!!!\r\n");
				return;
			}

			u1CurrentState = pETSPortEntry->u1CurrentState;
			
            pETSPortEntry->bETSSyncd = DCBX_DISABLED;
			pETSPortEntry->bETSError = DCBX_ENABLED;
			
            NotifyInfo.unTrapMsg.u1FeatType = ETS_TRAP;
			DcbxCEESendPortNotification(&NotifyInfo, DCBX_DUP_FEAT_TLV_TRAP);
            
            CEEUtlHandleETSAgeOut (pETSPortEntry);

		
            DCBX_TRC_ARG2 (CONTROL_PLANE_TRC,
					"CEE-SEM[%d]: Moving from  %s state to DUP_TLV_RCVD state\r\n",
					pCeeAppTlvParam->u4IfIndex,gau1CeeStateStr[u1CurrentState]);
			CEE_SET_CURRENT_STATE (pETSPortEntry, CEE_ST_RCVD_DUP_TLV);
			CEESmStateWaitForFeatTlv (u1TlvType, pCeeAppTlvParam);

			break;
		case CEE_PFC_TLV_TYPE :/*PFC TLV*/
			pPFCPortEntry = PFCUtlGetPortEntry (pCeeAppTlvParam->u4IfIndex);

			/* Get the PFC port entry  and if the port entry is not present
			 * then return without processing the message from DCBX */

			if (pPFCPortEntry == NULL)
			{
				DCBX_TRC (DCBX_FAILURE_TRC, "CEESmStateRxDupTlv:"
						" No PFC Port is created!!!\r\n");
				return;
			}
		
            pPFCPortEntry->bPFCSyncd = DCBX_DISABLED;
			pPFCPortEntry->bPFCError = DCBX_ENABLED;

			NotifyInfo.unTrapMsg.u1FeatType = PFC_TRAP;
			DcbxCEESendPortNotification(&NotifyInfo, DCBX_DUP_FEAT_TLV_TRAP);

            CEEUtlHandlePFCAgeOut (pPFCPortEntry);

			DCBX_TRC_ARG2 (CONTROL_PLANE_TRC,
					"CEE-SEM[%d]: Moving from  %s state to DUP_TLV_RCVD state\r\n",
					pCeeAppTlvParam->u4IfIndex,gau1CeeStateStr[u1CurrentState]);
			CEE_SET_CURRENT_STATE (pPFCPortEntry, CEE_ST_RCVD_DUP_TLV);

			CEESmStateWaitForFeatTlv (u1TlvType, pCeeAppTlvParam);

			break;
		case CEE_APP_PRI_TLV_TYPE :/*Application Priority TLV*/
			pAppPriPortEntry = AppPriUtlGetPortEntry (pCeeAppTlvParam->u4IfIndex);
			if (pAppPriPortEntry == NULL)
			{
				DCBX_TRC (DCBX_FAILURE_TRC, "CEESmStateRxDupTlv:"
						" No Applciation Priority Port is created!!!\r\n");
				return;
			}

			pAppPriPortEntry->bAppPriSyncd = DCBX_DISABLED;
			pAppPriPortEntry->bAppPriError = DCBX_ENABLED;

			NotifyInfo.unTrapMsg.u1FeatType = APP_PRI_TRAP;
			DcbxCEESendPortNotification(&NotifyInfo,DCBX_DUP_FEAT_TLV_TRAP);

            CEEUtlHandleAppPriAgeOut (pAppPriPortEntry);
			
			DCBX_TRC_ARG2 (CONTROL_PLANE_TRC,
					"CEE-SEM[%d]: Moving from  %s state to DUP_TLV_RCVD state\r\n",
					pCeeAppTlvParam->u4IfIndex,gau1CeeStateStr[u1CurrentState]);
            CEE_SET_CURRENT_STATE (pAppPriPortEntry, CEE_ST_RCVD_DUP_TLV);

			CEESmStateWaitForFeatTlv (u1TlvType, pCeeAppTlvParam);
			break;
        default:
            {
                DCBX_TRC_ARG1 (DCBX_FAILURE_TRC, 
                        "CEE-SEM[%d]: CEESmStateRxDupTlv: "
                        "Invalid TLV type!!!\r\n", pCeeAppTlvParam->u4IfIndex);
                break;
            }
	}
    return;
}

/***************************************************************************
 *  FUNCTION NAME : CEESmStateFeatInit
 * 
 *  DESCRIPTION   : This procedure is invoked when enters to INIT state 
 *
 *  INPUT         : u1TlvType       - Type of TLV recieved.
 *                  pCeeAppTlvParam - Application Info structure 
 * 
 *  OUTPUT        : None
 * 
 *  RETURNS       : None
 * 
 * **************************************************************************/
VOID
CEESmStateFeatInit (UINT1 u1TlvType, tCeeTLVInfo * pCeeAppTlvParam)
{
    tETSPortEntry      *pETSPortEntry = NULL;
    tPFCPortEntry      *pPFCPortEntry = NULL;
    tAppPriPortEntry   *pAppPriPortEntry = NULL;
    tDcbxCEETrapMsg     NotifyInfo;

    MEMSET(&NotifyInfo, DCBX_ZERO, sizeof(tDcbxCEETrapMsg));
    NotifyInfo.u4IfIndex = pCeeAppTlvParam->u4IfIndex; 

    switch(u1TlvType)
    {
        case CEE_ETS_TLV_TYPE :/*ETS TLV*/
            pETSPortEntry = ETSUtlGetPortEntry (pCeeAppTlvParam->u4IfIndex);
            if (pETSPortEntry == NULL)
            {
                DCBX_TRC (DCBX_FAILURE_TRC,  "CEESmStateFeatInit:"
                        " No ETS Port is created!!!\r\n");
                return;
            }

            pETSPortEntry->bETSSyncd = DCBX_ENABLED;
            pETSPortEntry->bETSError = DCBX_DISABLED;


            ETSUtlHandleAgedOutFromDCBX (pETSPortEntry, ETS_CONF_TLV_SUB_TYPE);
            DCBX_TRC_ARG2 (CONTROL_PLANE_TRC,
                    "CEE-SEM[%d]: Moving from  %s state to CEE_ST_FEAT_INIT state\r\n",
                    pCeeAppTlvParam->u4IfIndex,gau1CeeStateStr[pETSPortEntry->u1CurrentState]);
            CEE_SET_CURRENT_STATE (pETSPortEntry, CEE_ST_FEAT_INIT);

            CEESmStateWaitForFeatTlv (u1TlvType, pCeeAppTlvParam);

            break;
        case CEE_PFC_TLV_TYPE :/*PFC TLV*/
            pPFCPortEntry = PFCUtlGetPortEntry (pCeeAppTlvParam->u4IfIndex);

            /* Get the PFC port entry  and if the port entry is not present
             *      * then return without processing the message from DCBX */

            if (pPFCPortEntry == NULL)
            {
                DCBX_TRC (DCBX_FAILURE_TRC, "CEESmStateFeatInit:"
                        " No PFC Port is created!!!\r\n");
                return;
            }

            pPFCPortEntry->bPFCSyncd = DCBX_ENABLED;
            pPFCPortEntry->bPFCError = DCBX_DISABLED;

            PFCUtlHandleAgedOutFromDCBX (pPFCPortEntry);

            DCBX_TRC_ARG2 (CONTROL_PLANE_TRC,
                    "CEE-SEM[%d]: Moving from  %s state to CEE_ST_FEAT_INIT state\r\n",
                    pCeeAppTlvParam->u4IfIndex,gau1CeeStateStr[pPFCPortEntry->u1CurrentState]);
            CEE_SET_CURRENT_STATE (pPFCPortEntry, CEE_ST_FEAT_INIT);

            CEESmStateWaitForFeatTlv (u1TlvType, pCeeAppTlvParam);
            break;
        case CEE_APP_PRI_TLV_TYPE :/*Application Priority TLV*/
            pAppPriPortEntry = AppPriUtlGetPortEntry (pCeeAppTlvParam->u4IfIndex);
            if (pAppPriPortEntry == NULL)
            {
                DCBX_TRC (DCBX_FAILURE_TRC, "CEESmStateFeatInit:"
                        " No Applciation Priority Port is created!!!\r\n");
                return;
            }

            pAppPriPortEntry->bAppPriSyncd = DCBX_ENABLED;
            pAppPriPortEntry->bAppPriError = DCBX_DISABLED;

            AppPriUtlHandleAgedOutFromDCBX (pAppPriPortEntry);

            DCBX_TRC_ARG2 (CONTROL_PLANE_TRC,
                    "CEE-SEM[%d]: Moving from  %s state to CEE_ST_DELETE_AGED_INFO state\r\n",
                    pCeeAppTlvParam->u4IfIndex,gau1CeeStateStr[pAppPriPortEntry->u1CurrentState]);
            CEE_SET_CURRENT_STATE (pAppPriPortEntry, CEE_ST_FEAT_INIT);

            CEESmStateWaitForFeatTlv (u1TlvType, pCeeAppTlvParam);
            break;
        default:
            {
                DCBX_TRC_ARG1 (DCBX_FAILURE_TRC, 
                        "CEE-SEM[%d]: CEESmStateFeatInit: "
                        "Invalid TLV type!!!\r\n", pCeeAppTlvParam->u4IfIndex);
                break;
            }
    }
    return;
}

/****************************************************************************
 *  FUNCTION NAME : CEESmStateInvalidPktRcvd
 * 
 *  DESCRIPTION   : This function is invoked when the any invalid CEE pkt is  
 *  		        received
 *  INPUT         : u1TlvType       - Type of TLV recieved. 
 *                  pCeeAppTlvParam - Application Info structure 
 * 
 *  OUTPUT        : None
 * 
 *  RETURNS       : None
 * 
 * **************************************************************************/
VOID
CEESmStateInvalidPktRcvd (UINT1 u1TlvType, tCeeTLVInfo * pCeeAppTlvParam)
{
    tETSPortEntry      *pETSPortEntry    = NULL;
    tPFCPortEntry      *pPFCPortEntry    = NULL;
    tAppPriPortEntry   *pAppPriPortEntry = NULL;

    DCBX_TRC_ARG1 (CONTROL_PLANE_TRC,
            "CEESmStateInvalidPktRcvd: Rcvd Invalid TLV for TLV type %d\r\n",
            u1TlvType);

    switch(u1TlvType)
    {
        case CEE_ETS_TLV_TYPE :/*ETS TLV*/
            pETSPortEntry = ETSUtlGetPortEntry (pCeeAppTlvParam->u4IfIndex);
            if (pETSPortEntry == NULL)
            {
                DCBX_TRC_ARG1 (DCBX_FAILURE_TRC, "CEESmStateInvalidPktRcvd:"
                        " Entry not created for ETS on Port[%d]!!!\r\n", 
                        pCeeAppTlvParam->u4IfIndex);
                return;
            }

            SET_DCBX_STATUS(pETSPortEntry, pETSPortEntry->u1ETSDcbxSemType, 
                    DCBX_RED_ETS_DCBX_STATUS_INFO, DCBX_STATUS_UNKNOWN);

            pETSPortEntry->bETSSyncd = DCBX_DISABLED;
            pETSPortEntry->bETSError = DCBX_ENABLED;

            CEEUtlHandleETSAgeOut (pETSPortEntry);
            break;

        case CEE_PFC_TLV_TYPE :/*PFC TLV*/
            pPFCPortEntry = PFCUtlGetPortEntry (pCeeAppTlvParam->u4IfIndex);
            if (pPFCPortEntry == NULL)
            {
                DCBX_TRC_ARG1 (DCBX_FAILURE_TRC, "CEESmStateInvalidPktRcvd:"
                        " Entry not created for PFC on Port[%d]!!!\r\n",
                        pCeeAppTlvParam->u4IfIndex);
                return;
            }

            SET_DCBX_STATUS (pPFCPortEntry, pPFCPortEntry->u1PFCDcbxSemType, 
                    DCBX_RED_PFC_DCBX_STATUS_INFO, DCBX_STATUS_UNKNOWN);

            pPFCPortEntry->bPFCSyncd = DCBX_DISABLED;
            pPFCPortEntry->bPFCError = DCBX_ENABLED;

            CEEUtlHandlePFCAgeOut (pPFCPortEntry);
            break;

        case CEE_APP_PRI_TLV_TYPE :/*Application Priority TLV*/
            pAppPriPortEntry = AppPriUtlGetPortEntry (pCeeAppTlvParam->u4IfIndex);
            if (pAppPriPortEntry == NULL)
            {
                DCBX_TRC_ARG1 (DCBX_FAILURE_TRC, "CEESmStateInvalidPktRcvd:"
                        " Entry not created for Applciation Priority on Port[%d]!!!\r\n",
                        pCeeAppTlvParam->u4IfIndex);
                return;
            }
            
            SET_DCBX_STATUS (pAppPriPortEntry, pAppPriPortEntry->u1AppPriDcbxSemType, 
                    DCBX_RED_APP_PRI_DCBX_STATUS_INFO, DCBX_STATUS_UNKNOWN);
            
            pAppPriPortEntry->bAppPriSyncd = DCBX_DISABLED;
            pAppPriPortEntry->bAppPriError = DCBX_ENABLED;
            
            CEEUtlHandleAppPriAgeOut (pAppPriPortEntry);
            break;

        default:
            {
                DCBX_TRC_ARG1 (DCBX_FAILURE_TRC, 
                        "CEE-SEM[%d]: CEESmStateInvalidPktRcvd: "
                        "Invalid TLV type!!!\r\n", pCeeAppTlvParam->u4IfIndex);
                break;
            }
    }
    return;
}
/******** END OF FILE ***********/
