/******************************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: ofctmr.c,v 1.5 2014/08/14 11:10:00 siva Exp $
 *
 * Description : This file contains procedures containing Ofc Timer
 *               related operations
 *****************************************************************************/
#include "ofcinc.h"
#include "fssocket.h"
#include "ofcflow.h"
#include "ofccntlr.h"
#include "ofcapi.h"

PRIVATE tOfcTmrDesc gaOfcTmrDesc[OFC_MAX_TMRS];

/* Prototypes of the functions private to this file only */
PRIVATE VOID OfcDeleteFlowOnIdleTmrExpiry PROTO ((VOID *));
PRIVATE VOID OfcDeleteFlowOnHardTmrExpiry PROTO ((VOID *));
PRIVATE VOID        TmrConnRetryTimer (VOID *pArg);
PRIVATE VOID        TmrEchoReqTimer (VOID *pArg);
PRIVATE VOID        TmrEchoInterTimer (VOID *pArg);

/****************************************************************************
*                                                                           *
* Function     : OfcTmrInitTmrDesc                                         *
*                                                                           *
* Description  : Initialize Ofc Timer Decriptors                           *
*                                                                           *
* Input        : None                                                       *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : None                                                       *
*                                                                           *
*                                                                           *
*****************************************************************************/

PUBLIC VOID
OfcTmrInitTmrDesc ()
{
    tOfcFsofcCfgEntry  *pOfcFsofcCfgEntry = NULL;

    OFC_TRC_FUNC ((OFC_FN_ENTRY, "FUNC:OfcTmrInitTmrDesc\n"));

    pOfcFsofcCfgEntry = (tOfcFsofcCfgEntry *)
        RBTreeGetFirst (gOfcGlobals.OfcGlbMib.FsofcCfgTable);
    if (pOfcFsofcCfgEntry == NULL)
    {
        OFC_TRC ((OFC_TMR_TRC, "\r\nOfcTmrInitTmrDesc Failure\n"));
        return;
    }

    /* CONN RETRY TIMER */
    gaOfcTmrDesc[OFC_CONTROLLER_TMR].pTmrExpFunc = TmrConnRetryTimer;
    gaOfcTmrDesc[OFC_CONTROLLER_TMR].i2Offset =
        (INT2) (OFC_GET_OFFSET (tOfcFsofcControllerConnEntry, ConnRetryTimer));

    /* HARD TIMER */
    gaOfcTmrDesc[OFC_HARD_TIMER].pTmrExpFunc =
        (VOID *) OfcDeleteFlowOnHardTmrExpiry;
    if (pOfcFsofcCfgEntry->MibObject.i4FsofcSupportedVersion ==
        OFC_VERSION_V100)
    {
        gaOfcTmrDesc[OFC_HARD_TIMER].i2Offset =
            (INT2) (FSAP_OFFSETOF (tOfcFlowEntry, OfcHardTimer));
    }
    else
    {
        gaOfcTmrDesc[OFC_HARD_TIMER].i2Offset =
            (INT2) (FSAP_OFFSETOF (tOfcFsofcFlowEntry, OfcHardTimer));
    }

    /* IDLE TIMER */
    gaOfcTmrDesc[OFC_IDLE_TIMER].pTmrExpFunc =
        (VOID *) OfcDeleteFlowOnIdleTmrExpiry;
    if (pOfcFsofcCfgEntry->MibObject.i4FsofcSupportedVersion ==
        OFC_VERSION_V100)
    {
        gaOfcTmrDesc[OFC_IDLE_TIMER].i2Offset =
            (INT2) (FSAP_OFFSETOF (tOfcFlowEntry, OfcIdleTimer));
    }
    else
    {
        gaOfcTmrDesc[OFC_IDLE_TIMER].i2Offset =
            (INT2) (FSAP_OFFSETOF (tOfcFsofcFlowEntry, OfcIdleTimer));
    }

    /* ECHO REQUEST TIMER */
    gaOfcTmrDesc[OFC_ECHOREQ_TMR].pTmrExpFunc = TmrEchoReqTimer;
    gaOfcTmrDesc[OFC_ECHOREQ_TMR].i2Offset =
        (INT2) (OFC_GET_OFFSET (tOfcFsofcControllerConnEntry, EchoReqTimer));

    /* ECHO INTERVAL TIMER */
    gaOfcTmrDesc[OFC_ECHOINTER_TMR].pTmrExpFunc = TmrEchoInterTimer;
    gaOfcTmrDesc[OFC_ECHOINTER_TMR].i2Offset =
        (INT2) (OFC_GET_OFFSET (tOfcFsofcControllerConnEntry, EchoInterTimer));

    OFC_TRC_FUNC ((OFC_FN_EXIT, "FUNC:EXIT OfcTmrInitTmrDesc\n"));
}

/****************************************************************************
*                                                                           *
* Function     : OfcTmrStartTmr                                            *
*                                                                           *
* Description  : Starts Ofc Timer                                          *
*                                                                           *
* Input        : pOfcTmr - pointer to OfcTmr structure                    *
*                eOfcTmrId - OFC timer ID                                 *
*                u4Secs     - ticks in seconds                              *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : OSIX_SUCCESS/OSIX_FAILURE                                  *
*                                                                           *
*                                                                           *
*****************************************************************************/

PUBLIC VOID
OfcTmrStartTmr (tOfcTmr * pOfcTmr, enOfcTmrId eOfcTmrId, UINT4 u4Secs)
{

    OFC_TRC_FUNC ((OFC_FN_ENTRY, "FUNC:OfcTmrStartTmr\n"));

    pOfcTmr->eOfcTmrId = eOfcTmrId;

    if (TmrStartTimer (gOfcGlobals.ofcTmrLst, &(pOfcTmr->tmrNode),
                       (UINT4) NO_OF_TICKS_PER_SEC * u4Secs) == TMR_FAILURE)
    {
        OFC_TRC ((OFC_TMR_TRC, "Tmr Start Failure\n"));
    }

    OFC_TRC_FUNC ((OFC_FN_EXIT, "FUNC: Exit OfcTmrStartTmr\n"));
    return;
}

/****************************************************************************
*                                                                           *
* Function     : OfcTmrRestartTmr                                          *
*                                                                           *
* Description  :  ReStarts Ofc Timer                                       *
*                                                                           *
* Input        : pOfcTmr - pointer to OfcTmr structure                    *
*                eOfcTmrId - OFC timer ID                                 *
*                u4Secs     - ticks in seconds                              *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : None                                                       *
*                                                                           *
*                                                                           *
*****************************************************************************/

PUBLIC VOID
OfcTmrRestartTmr (tOfcTmr * pOfcTmr, enOfcTmrId eOfcTmrId, UINT4 u4Secs)
{
    OFC_TRC_FUNC ((OFC_FN_ENTRY, "FUNC:OfcTmrRestartTmr\n"));

    TmrStopTimer (gOfcGlobals.ofcTmrLst, &(pOfcTmr->tmrNode));
    OfcTmrStartTmr (pOfcTmr, eOfcTmrId, u4Secs);

    OFC_TRC_FUNC ((OFC_FN_EXIT, "FUNC: Exit OfcTmrStartTmr\n"));
    return;
}

/****************************************************************************
*                                                                           *
* Function     : OfcTmrStopTmr                                             *
*                                                                           *
* Description  : Restarts Ofc Timer                                        *
*                                                                           *
* Input        : pOfcTmr - pointer to OfcTmr structure                    *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : None                                                       *
*                                                                           *
*                                                                           *
*****************************************************************************/

PUBLIC VOID
OfcTmrStopTmr (tOfcTmr * pOfcTmr)
{
    OFC_TRC_FUNC ((OFC_FN_ENTRY, "FUNC:OfcTmrStopTmr\n"));

    TmrStopTimer (gOfcGlobals.ofcTmrLst, &(pOfcTmr->tmrNode));

    OFC_TRC_FUNC ((OFC_FN_EXIT, "FUNC: Exit OfcTmrStopTmr\n"));
    return;
}

/****************************************************************************
*                                                                           *
* Function     : OfcTmrHandleExpiry                                        *
*                                                                           *
* Description  : This procedure is invoked when the event indicating a time *
*                expiry occurs. This procedure finds the expired timers and *
*                invokes the corresponding timer routines.                  *
*                                                                           *
* Input        : None                                                       *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : None                                                       *
*                                                                           *
*                                                                           *
*****************************************************************************/
PUBLIC VOID
OfcTmrHandleExpiry (VOID)
{
    tTmrAppTimer       *pExpiredTimers = NULL;
    enOfcTmrId          eOfcTmrId;
    INT2                i2Offset = OFC_ZERO;

    OFC_TRC_FUNC ((OFC_FN_ENTRY, "FUNC: OfcTmrHandleExpiry\n"));
    while ((pExpiredTimers = TmrGetNextExpiredTimer (gOfcGlobals.ofcTmrLst))
           != NULL)
    {

        eOfcTmrId = ((tOfcTmr *) pExpiredTimers)->eOfcTmrId;
        i2Offset = gaOfcTmrDesc[eOfcTmrId].i2Offset;

        if (i2Offset < OFC_ZERO)
        {

            /* The timer function does not take any parameter */

            (*(gaOfcTmrDesc[eOfcTmrId].pTmrExpFunc)) (NULL);

        }
        else
        {

            /* The timer function requires a parameter */

            (*(gaOfcTmrDesc[eOfcTmrId].pTmrExpFunc))
                ((UINT1 *) pExpiredTimers - i2Offset);
        }
    }
    OFC_TRC_FUNC ((OFC_FN_EXIT, "FUNC: Exit OfcTmrHandleExpiry\n"));
    return;

}

/*****************************************************************************/
/* Function     : TmrConnRetryTimer                                          */
/* Description  :  Timer expiry handler for controller connection retry timer*/
/* Input        : pArg                                                       */
/* Output       : None                                                       */
/* Returns      : VOID                                                       */
/*****************************************************************************/
PRIVATE VOID
TmrConnRetryTimer (VOID *pArg)
{
    tOfcFsofcControllerConnEntry lOfcControllerConnEntry;
    tOfcFsofcCfgEntry   OfcFsofcCfgEntry;
    tOfcFsofcCfgEntry  *pOfcFsofcCfgEntry = NULL;
    tOfcFsofcControllerConnEntry *pOfcFsofcControllerConnEntry = NULL;

    INT4                i4RetValue = OFC_ZERO;
    UINT4               u4TempIntervel = OFC_ZERO;

    MEMSET (&OfcFsofcCfgEntry, OFC_ZERO, sizeof (tOfcFsofcCfgEntry));
    MEMSET (&lOfcControllerConnEntry, OFC_ZERO,
            sizeof (tOfcFsofcControllerConnEntry));

    pOfcFsofcControllerConnEntry = (tOfcFsofcControllerConnEntry *) pArg;

    OfcFsofcCfgEntry.MibObject.u4FsofcContextId =
        pOfcFsofcControllerConnEntry->MibObject.u4FsofcContextId;

    pOfcFsofcCfgEntry =
        RBTreeGet (gOfcGlobals.OfcGlbMib.FsofcCfgTable,
                   (tRBElem *) & (OfcFsofcCfgEntry));
    if (pOfcFsofcCfgEntry == NULL)
    {
        OFC_TRC_FUNC ((OFC_UTIL_TRC, "\r\nFUNC:TmrConnRetryTimer:"
                       "Get CFG Entry FAiled\n"));
        return;
    }

    if (pOfcFsofcControllerConnEntry->MibObject.i4FsofcControllerConnState !=
        OFC_CONNECTED)
    {
        /* 
         * Update the Auxilary connection sockets and Clear the data structure, 
         * if the Primary connection is Lost 
         */
        if (pOfcFsofcControllerConnEntry->MibObject.
            i4FsofcControllerConnAuxId == OFC_ZERO)
        {
            i4RetValue =
                OfcUpdateControllerConnState (pOfcFsofcControllerConnEntry,
                                              OFC_NOT_CONNECTED);
            if (i4RetValue == OSIX_FAILURE)
            {
                OFC_TRC_FUNC ((OFC_UTIL_TRC, "FUNC:TmrConnRetryTimer: "
                               "No Auxiliary connections present\n"));
            }
        }

        /* Update the Client's Connection state */
        OfcUpdateCfgConnState (pOfcFsofcControllerConnEntry);
    }

    if (pOfcFsofcControllerConnEntry->MibObject.i4FsofcControllerConnState ==
        OFC_NOT_CONNECTED)
    {
        /* No need to start timers for auxilary connections */
        if (pOfcFsofcControllerConnEntry->MibObject.
            i4FsofcControllerConnAuxId == OFC_ZERO)
        {
            /* 
             * Only when the Controller socket state is not connected
             * try reconnecting. Otherwise SelAddWrFd will take care of reconnecting
             * Close all the timers, if running before closing the connections 
             */
            if (pOfcFsofcControllerConnEntry->ConnRetryTimer.tmrNode.
                u2Flags & OFC_TMR_RUNNING)
            {
                OfcTmrStopTmr (&(pOfcFsofcControllerConnEntry->ConnRetryTimer));
            }
            if (pOfcFsofcControllerConnEntry->EchoReqTimer.tmrNode.
                u2Flags & OFC_TMR_RUNNING)
            {
                OfcTmrStopTmr (&(pOfcFsofcControllerConnEntry->EchoReqTimer));
            }
            if (pOfcFsofcControllerConnEntry->EchoInterTimer.tmrNode.
                u2Flags & OFC_TMR_RUNNING)
            {
                OfcTmrStopTmr (&(pOfcFsofcControllerConnEntry->EchoInterTimer));
            }

            SelRemoveFd (pOfcFsofcControllerConnEntry->i4SocketDesc);
            close (pOfcFsofcControllerConnEntry->i4SocketDesc);
            pOfcFsofcControllerConnEntry->i4SocketDesc = OFC_MINUS_ONE;

            /* 
             * Delete all the flows if the connection failure mode is failstand alone
             */

            if ((pOfcFsofcCfgEntry->u4OfcClientState == OFC_NOT_CONNECTED) &&
                (pOfcFsofcCfgEntry->MibObject.i4FsofcSwitchModeOnConnFailure ==
                 OFC_SWITCHMODE_FAIL_STANDALONE))
            {
                Ofc131FlowDeleteAllFlows (pOfcFsofcCfgEntry);

                /* 
                 * Make the openflow ports visible to the ISS context 
                 */
                i4RetValue =
                    OfcHandleOpenflowPorts (OfcFsofcCfgEntry.MibObject.
                                            u4FsofcContextId,
                                            CFA_SUBTYPE_SISP_INTERFACE);

                if (i4RetValue == OFC_FAILURE)
                {
                    return;
                }
            }
            OfcCreateController (pOfcFsofcControllerConnEntry);
        }
    }
    else if (pOfcFsofcControllerConnEntry->MibObject.
             i4FsofcControllerConnState == OFC_CONNECT_INPROGRESS)
    {
        /* No need to start timers for auxilary connections */
        if (pOfcFsofcControllerConnEntry->MibObject.
            i4FsofcControllerConnAuxId == OFC_ZERO)
        {
            /* Restart the timer alone as SelAddWrFd will take care of reconnecting */
            u4TempIntervel =
                (UINT4) (OFC_CONTROLLER_INTERVAL *
                         (OFC_ONE << pOfcFsofcControllerConnEntry->
                          i4ConnRetryCount));
            OfcTmrStartTmr (&(pOfcFsofcControllerConnEntry->ConnRetryTimer),
                            OFC_CONTROLLER_TMR, u4TempIntervel);
            pOfcFsofcControllerConnEntry->i4ConnRetryCount++;
            if (pOfcFsofcControllerConnEntry->i4ConnRetryCount >=
                OFC_THIRTY_TWO)
            {
                pOfcFsofcControllerConnEntry->i4ConnRetryCount = OFC_ZERO;
            }

            SelRemoveFd (pOfcFsofcControllerConnEntry->i4SocketDesc);
            close (pOfcFsofcControllerConnEntry->i4SocketDesc);
            pOfcFsofcControllerConnEntry->i4SocketDesc = OFC_MINUS_ONE;

            /* 
             * Delete all the flows if the connection failure mode is failstand alone
             */
            if ((pOfcFsofcCfgEntry->u4OfcClientState == OFC_NOT_CONNECTED) &&
                (pOfcFsofcCfgEntry->MibObject.i4FsofcSwitchModeOnConnFailure ==
                 OFC_SWITCHMODE_FAIL_STANDALONE))
            {
                Ofc131FlowDeleteAllFlows (pOfcFsofcCfgEntry);

                /* 
                 * Make the openflow ports visible to the ISS context 
                 */
                i4RetValue =
                    OfcHandleOpenflowPorts (OfcFsofcCfgEntry.MibObject.
                                            u4FsofcContextId,
                                            CFA_SUBTYPE_SISP_INTERFACE);

                if (i4RetValue == OFC_FAILURE)
                {
                    return;
                }
            }
            OfcCreateController (pOfcFsofcControllerConnEntry);
        }
    }
    return;
}

/*****************************************************************************/
/* Function     : OfcDeleteFlowOnHardTmrExpiry                               */
/* Description  : Timer expiry handler for hardtime out                      */
/* Input        : pArg                                                       */
/* Output       : None                                                       */
/* Returns      : VOID                                                       */
/*****************************************************************************/
PRIVATE VOID
OfcDeleteFlowOnHardTmrExpiry (VOID *pArg)
{
    tOfcFlowEntry      *pOfcFlowEntry = NULL;
    tOfcFsofcFlowEntry *pOfcFsofcFlowEntry = NULL;
    tOfcFsofcCfgEntry  *pOfcFsofcCfgEntry = NULL;

    pOfcFsofcCfgEntry = (tOfcFsofcCfgEntry *)
        RBTreeGetFirst (gOfcGlobals.OfcGlbMib.FsofcCfgTable);
    if (pOfcFsofcCfgEntry == NULL)
    {
        OFC_TRC ((OFC_TMR_TRC, "\r\nOfcDeleteFlowOnHardTmrExpiry "
                  "Get CFG Entry Failure for Default Context\n"));
        return;
    }

    if (pOfcFsofcCfgEntry->MibObject.i4FsofcSupportedVersion ==
        OFC_VERSION_V100)
    {
        pOfcFlowEntry = (tOfcFlowEntry *) pArg;
        OfcFlowDeleteFlow (pOfcFsofcCfgEntry, pOfcFlowEntry,
                           OFPFC_DELETE_STRICT, OFPRR_HARD_TIMEOUT);
    }
    else
    {
        pOfcFsofcFlowEntry = (tOfcFsofcFlowEntry *) pArg;
        pOfcFsofcCfgEntry = NULL;
        pOfcFsofcCfgEntry =
            OfcGetFsofcCfgEntry (pOfcFsofcFlowEntry->MibObject.
                                 u4FsofcContextId);
        if (pOfcFsofcCfgEntry == NULL)
        {
            OFC131_TRC_FUNC ((OFC_TMR_TRC, "\r\nOfcDeleteFlowOnHardTmrExpiry "
                              "Get CFG Entry Failed\n"));
            return;
        }

        Ofc131FlowDeleteFlow (pOfcFsofcCfgEntry, pOfcFsofcFlowEntry,
                              OFPRR_HARD_TIMEOUT);
    }

    return;
}

/*****************************************************************************/
/* Function     : OfcDeleteFlowOnIdleTmrExpiry                               */
/* Description  : Timer expiry handler for idletime out                      */
/* Input        : pArg                                                       */
/* Output       : None                                                       */
/* Returns      : VOID                                                       */
/*****************************************************************************/
PRIVATE VOID
OfcDeleteFlowOnIdleTmrExpiry (VOID *pArg)
{
    tOfcFlowEntry      *pOfcFlowEntry = NULL;
    tOfcFsofcFlowEntry *pOfcFsofcFlowEntry = NULL;
    tOfcFsofcCfgEntry  *pOfcFsofcCfgEntry = NULL;
#ifdef NPAPI_WANTED
    INT4                i4RetValue = FNP_FAILURE;
    tOfcHwInfo          OfcHwInfo;
    MEMSET (&OfcHwInfo, 0, sizeof (tOfcHwInfo));
#endif
    pOfcFsofcCfgEntry = (tOfcFsofcCfgEntry *)
        RBTreeGetFirst (gOfcGlobals.OfcGlbMib.FsofcCfgTable);
    if (pOfcFsofcCfgEntry == NULL)
    {
        OFC_TRC ((OFC_TMR_TRC, "\r\nOfcDeleteFlowOnIdleTmrExpiry "
                  "Get CFG Entry Failure for Default Context\n"));
        return;
    }

    if (pOfcFsofcCfgEntry->MibObject.i4FsofcSupportedVersion ==
        OFC_VERSION_V100)
    {
        pOfcFlowEntry = (tOfcFlowEntry *) pArg;
#ifdef NPAPI_WANTED
        OfcHwInfo.OfcVer = OFC_VER_100;
        OfcHwInfo.OfcCmd = OFC_NP_GET_FLOW_STATS;
        OfcHwInfo.OfcHwEntry.OfcFlowStats.u4FlowIndex =
            pOfcFlowEntry->u4FlowIndex;
        OfcHwInfo.OfcHwEntry.OfcFlowStats.u8PktCount.u4Lo =
            pOfcFlowEntry->u4PktCount;
        OfcHwInfo.OfcHwEntry.OfcFlowStats.u8ByteCount.u4Lo =
            pOfcFlowEntry->u4ByteCount;

        i4RetValue = OfcFsNpHwConfigOfcInfo (&OfcHwInfo);
        if (i4RetValue != FNP_SUCCESS)
        {
            OFC_TRC_FUNC ((OFC_TMR_TRC,
                           "\r\nFUNC:OfcDeleteFlowOnIdleTmrExpiry: "
                           "NPAPI failure in getting FLOW STATS\n"));
        }
        if (OfcHwInfo.OfcHwEntry.OfcFlowStats.u8PktCount.u4Lo <=
            pOfcFlowEntry->u4PktCount)
        {
            OfcFlowDeleteFlow (pOfcFsofcCfgEntry, pOfcFlowEntry,
                               OFPFC_DELETE_STRICT, OFPRR_IDLE_TIMEOUT);
            return;
        }
        pOfcFlowEntry->u4PktCount =
            OfcHwInfo.OfcHwEntry.OfcFlowStats.u8PktCount.u4Lo;
        pOfcFlowEntry->u4ByteCount =
            OfcHwInfo.OfcHwEntry.OfcFlowStats.u8ByteCount.u4Lo;
        OfcTmrStartTmr (&(pOfcFlowEntry->OfcIdleTimer), OFC_IDLE_TIMER,
                        pOfcFlowEntry->u2IdleTimeout);
#else
        OfcFlowDeleteFlow (pOfcFsofcCfgEntry, pOfcFlowEntry,
                           OFPFC_DELETE_STRICT, OFPRR_IDLE_TIMEOUT);
#endif
    }
    else
    {
        pOfcFsofcFlowEntry = (tOfcFsofcFlowEntry *) pArg;
        pOfcFsofcCfgEntry = NULL;
        pOfcFsofcCfgEntry =
            OfcGetFsofcCfgEntry (pOfcFsofcFlowEntry->MibObject.
                                 u4FsofcContextId);
        if (pOfcFsofcCfgEntry == NULL)
        {
            OFC131_TRC_FUNC ((OFC_TMR_TRC, "\r\nOfcDeleteFlowOnIdleTmrExpiry "
                              "Get CFG Entry Failed\n"));
            return;
        }

        Ofc131FlowDeleteFlow (pOfcFsofcCfgEntry, pOfcFsofcFlowEntry,
                              OFPRR_IDLE_TIMEOUT);
    }

    return;
}

/*********************************************************************************************/
/* Function     : TmrEchoInterTimer                                                          */
/* Description  : Timer expiry handler for Echo Request Intervel Timer                       */
/* Input        : pArg                                                                     */
/* Output       : None                                                                       */
/* Returns      : VOID                                                                       */
/*********************************************************************************************/

PRIVATE VOID
TmrEchoInterTimer (VOID *pArg)
{
    tOfcFsofcControllerConnEntry *pConnPtr =
        (tOfcFsofcControllerConnEntry *) pArg;
    tOfpEchoReqMsg      OfpEchoReqMsg;
    UINT4               u4VarLen = sizeof (tOfp131EchoReqMsg);
    tOfp131EchoReqMsg   Ofp131EchoReqMsg;
    tOfcFsofcCfgEntry  *pOfcFsofcCfgEntry = NULL;
    pConnPtr->u4IsRecieved = FALSE;

    /* Send the echo request message to the controller to check for the liveness 
     * and start the Echo Request timer */

    pOfcFsofcCfgEntry = (tOfcFsofcCfgEntry *)
        RBTreeGetFirst (gOfcGlobals.OfcGlbMib.FsofcCfgTable);
    if (pOfcFsofcCfgEntry == NULL)
    {
        OFC_TRC ((OFC_TMR_TRC, "\r\nTmrEchoInterTimer: "
                  "Get CFG Entry Failure\n"));
        return;
    }

    if (pOfcFsofcCfgEntry->MibObject.i4FsofcSupportedVersion ==
        OFC_VERSION_V100)
    {
        OfcEchoReqMsgPktSend (&OfpEchoReqMsg, pConnPtr);
    }
    else
    {
        Ofc131PktEchoReqMsgSend (&Ofp131EchoReqMsg, pConnPtr, u4VarLen);
    }

    /* Restart the timer alone as SelAddWrFd will take care of reconnecting */
    OfcTmrStartTmr (&(pConnPtr->EchoReqTimer), OFC_ECHOREQ_TMR,
                    OFC_ECHOREQ_INTERVAL);

    return;
}

/*********************************************************************************************/
/* Function     : TmrEchoReqTimer                                                            */
/* Description  : Timer expiry handler Echo Request - Reply Wait Timer                       */
/* Input        : pArg                                                                    */
/* Output       : None                                                                       */
/* Returns      : VOID                                                                       */
/*********************************************************************************************/

PRIVATE VOID
TmrEchoReqTimer (VOID *pArg)
{
    tOfcFsofcControllerConnEntry *pOfcFsofcControllerConnEntry =
        (tOfcFsofcControllerConnEntry *) pArg;
    INT4                i4RetValue = OFC_ZERO;

    /* Start the echo interval timer if echo reply is got from the controller 
     * and make u4IsRecieved to FALSE in Echo Interval Timer*/
    if (TRUE == pOfcFsofcControllerConnEntry->u4IsRecieved)
    {
        OfcTmrStartTmr (&(pOfcFsofcControllerConnEntry->EchoInterTimer),
                        OFC_ECHOINTER_TMR, OFC_ECHOINTER_INTERVAL);
    }
    else
    {
        /* Start the connection retry timer with state "OFC_CONNECT_INPROGRESS", if there is
           no echo reply from the controller. If the retry count is value is less than 
           OFC_MAX_CONN_RETRIES value and increment retry count by 1. Else set retry count 
           to "0" and start the retry timer with State "OFC_NOT_CONNECTED". */
        if (pOfcFsofcControllerConnEntry->i4ConnRetryCount <
            OFC_MAX_CONN_RETRIES)
        {
            pOfcFsofcControllerConnEntry->i4ConnRetryCount++;
            pOfcFsofcControllerConnEntry->MibObject.i4FsofcControllerConnState =
                OFC_CONNECT_INPROGRESS;
            i4RetValue =
                OfcUpdateControllerConnState (pOfcFsofcControllerConnEntry,
                                              OFC_NOT_CONNECTED);
            if (i4RetValue == OSIX_FAILURE)
            {
                OFC_TRC_FUNC ((OFC_TMR_TRC, "FUNC:TmrConnRetryTimer: "
                               "No Auxiliary connections present\n"));
            }

            OfcTmrStartTmr (&(pOfcFsofcControllerConnEntry->EchoInterTimer),
                            OFC_ECHOINTER_TMR, OFC_ECHOINTER_INTERVAL);
        }
        else
        {
            pOfcFsofcControllerConnEntry->i4ConnRetryCount = OFC_ZERO;
            pOfcFsofcControllerConnEntry->MibObject.i4FsofcControllerConnState =
                OFC_NOT_CONNECTED;
            TmrConnRetryTimer (pOfcFsofcControllerConnEntry);
        }
    }

    return;
}

/*-----------------------------------------------------------------------*/
/*                       End of the file  ofctmr.c                      */
/*-----------------------------------------------------------------------*/
