/********************************************************************
 * Copyright (C) 2013 Aricent Inc . All Rights Reserved
 *
 * $Id: ofcpkt.c,v 1.3 2014/02/14 14:16:14 siva Exp $
 *
 * Description: This file contains the Ofc pkt process related routines
 *********************************************************************/

#include "ofcinc.h"
#include "ofcpkt.h"
#include "ofcintf.h"
#include "ofcsz.h"
#include "iss.h"
#include "fssocket.h"
#include "ofcflow.h"
#include "ofccli.h"
#include "ofcapi.h"

PRIVATE UINT1       au1TxData[OFC_MAX_CONTROLLER_PKT_LEN];
PRIVATE UINT2       gu2MissSendLen = OFC_ONE_TWENTY_EIGHT;
PRIVATE UINT4       OfcPktOutPortFromActionHdr (tOfpActionHdr * pActionHdr,
                                                INT4 i4ActionLen);
extern INT4         CfaGddEthWrite (UINT1 *pu1Buf, UINT2 u2IfIndex,
                                    UINT4 u4Size);

#define OFC_INVALID_INDEX 0xffff
#define OFC_WILDCARD_MATCH 0xffffffff
#define OFC_FLOOD_INDEX 0xfffb
/****************************************************************************
 * Function    :  OfcOfpu1VersionValidate
 * Description :  This function is for u1Version Validate 
 * Input       :  pOfpPkt Pointer to the header and pConnPtr Conn Ptr
 * Output      :  pu4Error - Error on failure 
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE 
 ****************************************************************************/
INT4
OfcOfpu1VersionValidate (tOfpPkt * pOfpPkt, UINT4 *pu4Error, VOID *pConnPtr)
{

    OFC_TRC_FUNC ((OFC_FN_ENTRY, "FUNC:OfcOfpu1VersionValidate"));
    UNUSED_PARAM (pu4Error);
    UNUSED_PARAM (pConnPtr);
    if (pOfpPkt->u1Version != OFC_VERSION_ONE)
    {
        OFC_TRC_FUNC ((OFC_PKT_TRC,
                       "FUNC:OfcOfpu1VersionValidate: Failed value=%d",
                       pOfpPkt->u1Version));
        *pu4Error = OFC_OFP_U1VERSION_VALIDATE_ERR;
        return OSIX_FAILURE;
    }
    OFC_TRC_FUNC ((OFC_FN_EXIT, "FUNC:OfcOfpu1VersionValidate"));
    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  OfcOfpu1TypeValidate
 * Description :  This function is for u1Type Validate 
 * Input       :  pOfpPkt Pointer to the header and pConnPtr Conn Ptr
 * Output      :  pu4Error - Error on failure 
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE 
 ****************************************************************************/
INT4
OfcOfpu1TypeValidate (tOfpPkt * pOfpPkt, UINT4 *pu4Error, VOID *pConnPtr)
{

    OFC_TRC_FUNC ((OFC_FN_ENTRY, "FUNC:OfcOfpu1TypeValidate"));
    UNUSED_PARAM (pu4Error);
    UNUSED_PARAM (pConnPtr);
    if (pOfpPkt->u1Type > OFC_TWENTY_ONE)
    {
        OFC_TRC_FUNC ((OFC_PKT_TRC,
                       "FUNC:OfcOfpu1TypeValidate:Range Failed value=%d",
                       pOfpPkt->u1Type));
        *pu4Error = OFC_OFP_U1TYPE_VALIDATE_ERR;
        return OSIX_FAILURE;
    }
    OFC_TRC_FUNC ((OFC_FN_EXIT, "FUNC:OfcOfpu1TypeValidate"));
    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  OfcOfpu2LengthValidate
 * Description :  This function is for u2Length Validate 
 * Input       :  pOfpPkt Pointer to the header and pConnPtr Conn Ptr
 * Output      :  pu4Error - Error on failure 
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE 
 ****************************************************************************/
INT4
OfcOfpu2LengthValidate (tOfpPkt * pOfpPkt, UINT4 *pu4Error, VOID *pConnPtr)
{

    OFC_TRC_FUNC ((OFC_FN_ENTRY, "FUNC:OfcOfpu2LengthValidate"));
    UNUSED_PARAM (pu4Error);
    UNUSED_PARAM (pConnPtr);
    UNUSED_PARAM (pOfpPkt);
    OFC_TRC_FUNC ((OFC_FN_EXIT, "FUNC:OfcOfpu2LengthValidate"));
    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  OfcOfpu4XidValidate
 * Description :  This function is for u4Xid Validate 
 * Input       :  pOfpPkt Pointer to the header and pConnPtr Conn Ptr
 * Output      :  pu4Error - Error on failure 
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE 
 ****************************************************************************/
INT4
OfcOfpu4XidValidate (tOfpPkt * pOfpPkt, UINT4 *pu4Error, VOID *pConnPtr)
{

    OFC_TRC_FUNC ((OFC_FN_ENTRY, "FUNC:OfcOfpu4XidValidate"));
    UNUSED_PARAM (pu4Error);
    UNUSED_PARAM (pConnPtr);
    UNUSED_PARAM (pOfpPkt);
    OFC_TRC_FUNC ((OFC_FN_EXIT, "FUNC:OfcOfpu4XidValidate"));
    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  OfcHelloau1DummyValidate
 * Description :  This function is for au1Dummy Validate 
 * Input       :  pOfpHello Pointer to the header and pConnPtr Conn Ptr
 * Output      :  pu4Error - Error on failure 
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE 
 ****************************************************************************/
INT4
OfcHelloau1DummyValidate (tOfpHello * pOfpHello, UINT4 *pu4Error,
                          VOID *pConnPtr)
{

    OFC_TRC_FUNC ((OFC_FN_ENTRY, "FUNC:OfcHelloau1DummyValidate"));
    UNUSED_PARAM (pu4Error);
    UNUSED_PARAM (pConnPtr);
    UNUSED_PARAM (pOfpHello);
    OFC_TRC_FUNC ((OFC_FN_EXIT, "FUNC:OfcHelloau1DummyValidate"));
    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  OfcEchoReqMsgau1DataValidate
 * Description :  This function is for au1Data Validate 
 * Input       :  pOfpEchoReqMsg Pointer to the header and pConnPtr Conn Ptr
 * Output      :  pu4Error - Error on failure 
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE 
 ****************************************************************************/
INT4
OfcEchoReqMsgau1DataValidate (tOfpEchoReqMsg * pOfpEchoReqMsg, UINT4 *pu4Error,
                              VOID *pConnPtr)
{

    OFC_TRC_FUNC ((OFC_FN_ENTRY, "FUNC:OfcEchoReqMsgau1DataValidate"));
    UNUSED_PARAM (pu4Error);
    UNUSED_PARAM (pConnPtr);
    UNUSED_PARAM (pOfpEchoReqMsg);
    OFC_TRC_FUNC ((OFC_FN_EXIT, "FUNC:OfcEchoReqMsgau1DataValidate"));
    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  OfcEchoRplyMsgau1DataValidate
 * Description :  This function is for au1Data Validate 
 * Input       :  pOfpEchoRplyMsg Pointer to the header and pConnPtr Conn Ptr
 * Output      :  pu4Error - Error on failure 
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE 
 ****************************************************************************/
INT4
OfcEchoRplyMsgau1DataValidate (tOfpEchoRplyMsg * pOfpEchoRplyMsg,
                               UINT4 *pu4Error, VOID *pConnPtr)
{

    OFC_TRC_FUNC ((OFC_FN_ENTRY, "FUNC:OfcEchoRplyMsgau1DataValidate"));
    UNUSED_PARAM (pu4Error);
    UNUSED_PARAM (pConnPtr);
    UNUSED_PARAM (pOfpEchoRplyMsg);
    OFC_TRC_FUNC ((OFC_FN_EXIT, "FUNC:OfcEchoRplyMsgau1DataValidate"));
    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  OfcExperimenterHdru4ExperimenterValidate
 * Description :  This function is for u4Experimenter Validate 
 * Input       :  pOfpExperimenterHdr Pointer to the header and pConnPtr Conn Ptr
 * Output      :  pu4Error - Error on failure 
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE 
 ****************************************************************************/
INT4
OfcExperimenterHdru4ExperimenterValidate (tOfpExperimenterHdr *
                                          pOfpExperimenterHdr, UINT4 *pu4Error,
                                          VOID *pConnPtr)
{

    OFC_TRC_FUNC ((OFC_FN_ENTRY,
                   "FUNC:OfcExperimenterHdru4ExperimenterValidate"));
    UNUSED_PARAM (pu4Error);
    UNUSED_PARAM (pConnPtr);
    UNUSED_PARAM (pOfpExperimenterHdr);
    OFC_TRC_FUNC ((OFC_FN_EXIT,
                   "FUNC:OfcExperimenterHdru4ExperimenterValidate"));
    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  OfcFeatureReqau1DummyValidate
 * Description :  This function is for au1Dummy Validate 
 * Input       :  pOfpFeatureReq Pointer to the header and pConnPtr Conn Ptr
 * Output      :  pu4Error - Error on failure 
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE 
 ****************************************************************************/
INT4
OfcFeatureReqau1DummyValidate (tOfpFeatureReq * pOfpFeatureReq, UINT4 *pu4Error,
                               VOID *pConnPtr)
{
    OFC_TRC_FUNC ((OFC_FN_ENTRY, "FUNC:OfcFeatureReqau1DummyValidate"));
    UNUSED_PARAM (pu4Error);
    UNUSED_PARAM (pConnPtr);
    UNUSED_PARAM (pOfpFeatureReq);
    OFC_TRC_FUNC ((OFC_FN_EXIT, "FUNC:OfcFeatureReqau1DummyValidate"));
    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  OfcConfigGetau1DummyValidate
 * Description :  This function is for au1Dummy Validate 
 * Input       :  pOfpConfigGet Pointer to the header and pConnPtr Conn Ptr
 * Output      :  pu4Error - Error on failure 
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE 
 ****************************************************************************/
INT4
OfcConfigGetau1DummyValidate (tOfpConfigGet * pOfpConfigGet, UINT4 *pu4Error,
                              VOID *pConnPtr)
{

    OFC_TRC_FUNC ((OFC_FN_ENTRY, "FUNC:OfcConfigGetau1DummyValidate"));
    UNUSED_PARAM (pu4Error);
    UNUSED_PARAM (pConnPtr);
    UNUSED_PARAM (pOfpConfigGet);
    OFC_TRC_FUNC ((OFC_FN_EXIT, "FUNC:OfcConfigGetau1DummyValidate"));
    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  OfcConfigSetu2FlagsValidate
 * Description :  This function is for u2Flags Validate 
 * Input       :  pOfpConfigSet Pointer to the header and pConnPtr Conn Ptr
 * Output      :  pu4Error - Error on failure 
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE 
 ****************************************************************************/
INT4
OfcConfigSetu2FlagsValidate (tOfpConfigSet * pOfpConfigSet, UINT4 *pu4Error,
                             VOID *pConnPtr)
{

    OFC_TRC_FUNC ((OFC_FN_ENTRY, "FUNC:OfcConfigSetu2FlagsValidate"));
    UNUSED_PARAM (pu4Error);
    UNUSED_PARAM (pConnPtr);
    if (pOfpConfigSet->u2Flags > OFC_THREE)
    {
        OFC_TRC_FUNC ((OFC_PKT_TRC,
                       "FUNC:OfcConfigSetu2FlagsValidate:Range Failed value=%d",
                       pOfpConfigSet->u2Flags));
        *pu4Error = OFC_CONFIGSET_U2FLAGS_VALIDATE_ERR;
        return OSIX_FAILURE;
    }
    OFC_TRC_FUNC ((OFC_FN_EXIT, "FUNC:OfcConfigSetu2FlagsValidate"));
    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  OfcConfigSetu2MissSendLenValidate
 * Description :  This function is for u2MissSendLen Validate 
 * Input       :  pOfpConfigSet Pointer to the header and pConnPtr Conn Ptr
 * Output      :  pu4Error - Error on failure 
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE 
 ****************************************************************************/
INT4
OfcConfigSetu2MissSendLenValidate (tOfpConfigSet * pOfpConfigSet,
                                   UINT4 *pu4Error, VOID *pConnPtr)
{

    OFC_TRC_FUNC ((OFC_FN_ENTRY, "FUNC:OfcConfigSetu2MissSendLenValidate"));
    UNUSED_PARAM (pu4Error);
    UNUSED_PARAM (pConnPtr);
    UNUSED_PARAM (pOfpConfigSet);
    OFC_TRC_FUNC ((OFC_FN_EXIT, "FUNC:OfcConfigSetu2MissSendLenValidate"));
    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  OfcPktOutu4BufferIdValidate
 * Description :  This function is for u4BufferId Validate 
 * Input       :  pOfpPktOut Pointer to the header and pConnPtr Conn Ptr
 * Output      :  pu4Error - Error on failure 
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE 
 ****************************************************************************/
INT4
OfcPktOutu4BufferIdValidate (tOfpPktOut * pOfpPktOut, UINT4 *pu4Error,
                             VOID *pConnPtr)
{

    OFC_TRC_FUNC ((OFC_FN_ENTRY, "FUNC:OfcPktOutu4BufferIdValidate"));
    UNUSED_PARAM (pu4Error);
    UNUSED_PARAM (pConnPtr);
    UNUSED_PARAM (pOfpPktOut);
    OFC_TRC_FUNC ((OFC_FN_EXIT, "FUNC:OfcPktOutu4BufferIdValidate"));
    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  OfcPktOutu2InPortValidate
 * Description :  This function is for u2InPort Validate 
 * Input       :  pOfpPktOut Pointer to the header and pConnPtr Conn Ptr
 * Output      :  pu4Error - Error on failure 
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE 
 ****************************************************************************/
INT4
OfcPktOutu2InPortValidate (tOfpPktOut * pOfpPktOut, UINT4 *pu4Error,
                           VOID *pConnPtr)
{

    OFC_TRC_FUNC ((OFC_FN_ENTRY, "FUNC:OfcPktOutu2InPortValidate"));
    UNUSED_PARAM (pu4Error);
    UNUSED_PARAM (pConnPtr);
    UNUSED_PARAM (pOfpPktOut);
    OFC_TRC_FUNC ((OFC_FN_EXIT, "FUNC:OfcPktOutu2InPortValidate"));
    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  OfcPktOutu2ActionsLenValidate
 * Description :  This function is for u2ActionsLen Validate 
 * Input       :  pOfpPktOut Pointer to the header and pConnPtr Conn Ptr
 * Output      :  pu4Error - Error on failure 
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE 
 ****************************************************************************/
INT4
OfcPktOutu2ActionsLenValidate (tOfpPktOut * pOfpPktOut, UINT4 *pu4Error,
                               VOID *pConnPtr)
{

    OFC_TRC_FUNC ((OFC_FN_ENTRY, "FUNC:OfcPktOutu2ActionsLenValidate"));
    UNUSED_PARAM (pu4Error);
    UNUSED_PARAM (pConnPtr);
    UNUSED_PARAM (pOfpPktOut);
    OFC_TRC_FUNC ((OFC_FN_EXIT, "FUNC:OfcPktOutu2ActionsLenValidate"));
    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  OfcPktOutActionsValidate
 * Description :  This function is for Actions Validate 
 * Input       :  pOfpPktOut Pointer to the header and pConnPtr Conn Ptr
 * Output      :  pu4Error - Error on failure 
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE 
 ****************************************************************************/
INT4
OfcPktOutActionsValidate (tOfpPktOut * pOfpPktOut, UINT4 *pu4Error,
                          VOID *pConnPtr)
{

    OFC_TRC_FUNC ((OFC_FN_ENTRY, "FUNC:OfcPktOutActionsValidate"));
    UNUSED_PARAM (pu4Error);
    UNUSED_PARAM (pConnPtr);
    UNUSED_PARAM (pOfpPktOut);
    OFC_TRC_FUNC ((OFC_FN_EXIT, "FUNC:OfcPktOutActionsValidate"));
    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  OfcFlowModMatchValidate
 * Description :  This function is for Match Validate 
 * Input       :  pOfpFlowMod Pointer to the header and pConnPtr Conn Ptr
 * Output      :  pu4Error - Error on failure 
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE 
 ****************************************************************************/
INT4
OfcFlowModMatchValidate (tOfpFlowMod * pOfpFlowMod, UINT4 *pu4Error,
                         VOID *pConnPtr)
{

    OFC_TRC_FUNC ((OFC_FN_ENTRY, "FUNC:OfcFlowModMatchValidate"));
    UNUSED_PARAM (pu4Error);
    UNUSED_PARAM (pConnPtr);
    UNUSED_PARAM (pOfpFlowMod);
    OFC_TRC_FUNC ((OFC_FN_EXIT, "FUNC:OfcFlowModMatchValidate"));
    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  OfcFlowModu8CookieValidate
 * Description :  This function is for u8Cookie Validate 
 * Input       :  pOfpFlowMod Pointer to the header and pConnPtr Conn Ptr
 * Output      :  pu4Error - Error on failure 
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE 
 ****************************************************************************/
INT4
OfcFlowModu8CookieValidate (tOfpFlowMod * pOfpFlowMod, UINT4 *pu4Error,
                            VOID *pConnPtr)
{

    OFC_TRC_FUNC ((OFC_FN_ENTRY, "FUNC:OfcFlowModu8CookieValidate"));
    UNUSED_PARAM (pu4Error);
    UNUSED_PARAM (pConnPtr);
    UNUSED_PARAM (pOfpFlowMod);
    OFC_TRC_FUNC ((OFC_FN_EXIT, "FUNC:OfcFlowModu8CookieValidate"));
    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  OfcFlowModu2CommandValidate
 * Description :  This function is for u2Command Validate 
 * Input       :  pOfpFlowMod Pointer to the header and pConnPtr Conn Ptr
 * Output      :  pu4Error - Error on failure 
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE 
 ****************************************************************************/
INT4
OfcFlowModu2CommandValidate (tOfpFlowMod * pOfpFlowMod, UINT4 *pu4Error,
                             VOID *pConnPtr)
{

    OFC_TRC_FUNC ((OFC_FN_ENTRY, "FUNC:OfcFlowModu2CommandValidate"));
    UNUSED_PARAM (pu4Error);
    UNUSED_PARAM (pConnPtr);
    if (pOfpFlowMod->u2Command > OFC_FOUR)
    {
        OFC_TRC_FUNC ((OFC_PKT_TRC,
                       "FUNC:OfcFlowModu2CommandValidate:Range Failed value=%d",
                       pOfpFlowMod->u2Command));
        *pu4Error = OFC_FLOWMOD_U2COMMAND_VALIDATE_ERR;
        return OSIX_FAILURE;
    }
    OFC_TRC_FUNC ((OFC_FN_EXIT, "FUNC:OfcFlowModu2CommandValidate"));
    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  OfcFlowModu2IdleTimeoutValidate
 * Description :  This function is for u2IdleTimeout Validate 
 * Input       :  pOfpFlowMod Pointer to the header and pConnPtr Conn Ptr
 * Output      :  pu4Error - Error on failure 
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE 
 ****************************************************************************/
INT4
OfcFlowModu2IdleTimeoutValidate (tOfpFlowMod * pOfpFlowMod, UINT4 *pu4Error,
                                 VOID *pConnPtr)
{

    OFC_TRC_FUNC ((OFC_FN_ENTRY, "FUNC:OfcFlowModu2IdleTimeoutValidate"));
    UNUSED_PARAM (pu4Error);
    UNUSED_PARAM (pConnPtr);
    UNUSED_PARAM (pOfpFlowMod);
    OFC_TRC_FUNC ((OFC_FN_EXIT, "FUNC:OfcFlowModu2IdleTimeoutValidate"));
    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  OfcFlowModu2HardTimeoutValidate
 * Description :  This function is for u2HardTimeout Validate 
 * Input       :  pOfpFlowMod Pointer to the header and pConnPtr Conn Ptr
 * Output      :  pu4Error - Error on failure 
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE 
 ****************************************************************************/
INT4
OfcFlowModu2HardTimeoutValidate (tOfpFlowMod * pOfpFlowMod, UINT4 *pu4Error,
                                 VOID *pConnPtr)
{

    OFC_TRC_FUNC ((OFC_FN_ENTRY, "FUNC:OfcFlowModu2HardTimeoutValidate"));
    UNUSED_PARAM (pu4Error);
    UNUSED_PARAM (pConnPtr);
    UNUSED_PARAM (pOfpFlowMod);
    OFC_TRC_FUNC ((OFC_FN_EXIT, "FUNC:OfcFlowModu2HardTimeoutValidate"));
    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  OfcFlowModu2PriorityValidate
 * Description :  This function is for u2Priority Validate 
 * Input       :  pOfpFlowMod Pointer to the header and pConnPtr Conn Ptr
 * Output      :  pu4Error - Error on failure 
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE 
 ****************************************************************************/
INT4
OfcFlowModu2PriorityValidate (tOfpFlowMod * pOfpFlowMod, UINT4 *pu4Error,
                              VOID *pConnPtr)
{

    OFC_TRC_FUNC ((OFC_FN_ENTRY, "FUNC:OfcFlowModu2PriorityValidate"));
    UNUSED_PARAM (pu4Error);
    UNUSED_PARAM (pConnPtr);
    UNUSED_PARAM (pOfpFlowMod);
    OFC_TRC_FUNC ((OFC_FN_EXIT, "FUNC:OfcFlowModu2PriorityValidate"));
    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  OfcFlowModu4BufIdValidate
 * Description :  This function is for u4BufId Validate 
 * Input       :  pOfpFlowMod Pointer to the header and pConnPtr Conn Ptr
 * Output      :  pu4Error - Error on failure 
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE 
 ****************************************************************************/
INT4
OfcFlowModu4BufIdValidate (tOfpFlowMod * pOfpFlowMod, UINT4 *pu4Error,
                           VOID *pConnPtr)
{

    OFC_TRC_FUNC ((OFC_FN_ENTRY, "FUNC:OfcFlowModu4BufIdValidate"));
    UNUSED_PARAM (pu4Error);
    UNUSED_PARAM (pConnPtr);
    UNUSED_PARAM (pOfpFlowMod);
    OFC_TRC_FUNC ((OFC_FN_EXIT, "FUNC:OfcFlowModu4BufIdValidate"));
    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  OfcFlowModu2OutPortValidate
 * Description :  This function is for u2OutPort Validate 
 * Input       :  pOfpFlowMod Pointer to the header and pConnPtr Conn Ptr
 * Output      :  pu4Error - Error on failure 
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE 
 ****************************************************************************/
INT4
OfcFlowModu2OutPortValidate (tOfpFlowMod * pOfpFlowMod, UINT4 *pu4Error,
                             VOID *pConnPtr)
{

    OFC_TRC_FUNC ((OFC_FN_ENTRY, "FUNC:OfcFlowModu2OutPortValidate"));
    UNUSED_PARAM (pu4Error);
    UNUSED_PARAM (pConnPtr);
    UNUSED_PARAM (pOfpFlowMod);
    OFC_TRC_FUNC ((OFC_FN_EXIT, "FUNC:OfcFlowModu2OutPortValidate"));
    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  OfcFlowModu2FlagsValidate
 * Description :  This function is for u2Flags Validate 
 * Input       :  pOfpFlowMod Pointer to the header and pConnPtr Conn Ptr
 * Output      :  pu4Error - Error on failure 
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE 
 ****************************************************************************/
INT4
OfcFlowModu2FlagsValidate (tOfpFlowMod * pOfpFlowMod, UINT4 *pu4Error,
                           VOID *pConnPtr)
{

    OFC_TRC_FUNC ((OFC_FN_ENTRY, "FUNC:OfcFlowModu2FlagsValidate"));
    UNUSED_PARAM (pu4Error);
    UNUSED_PARAM (pConnPtr);
    UNUSED_PARAM (pOfpFlowMod);
    OFC_TRC_FUNC ((OFC_FN_EXIT, "FUNC:OfcFlowModu2FlagsValidate"));
    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  OfcFlowModActionsValidate
 * Description :  This function is for Actions Validate 
 * Input       :  pOfpFlowMod Pointer to the header and pConnPtr Conn Ptr
 * Output      :  pu4Error - Error on failure 
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE 
 ****************************************************************************/
INT4
OfcFlowModActionsValidate (tOfpFlowMod * pOfpFlowMod, UINT4 *pu4Error,
                           VOID *pConnPtr)
{

    OFC_TRC_FUNC ((OFC_FN_ENTRY, "FUNC:OfcFlowModActionsValidate"));
    UNUSED_PARAM (pu4Error);
    UNUSED_PARAM (pConnPtr);
    UNUSED_PARAM (pOfpFlowMod);
    OFC_TRC_FUNC ((OFC_FN_EXIT, "FUNC:OfcFlowModActionsValidate"));
    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  OfcPortModu4PortNoValidate
 * Description :  This function is for u4PortNo Validate 
 * Input       :  pOfpPortMod Pointer to the header and pConnPtr Conn Ptr
 * Output      :  pu4Error - Error on failure 
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE 
 ****************************************************************************/
INT4
OfcPortModu4PortNoValidate (tOfpPortMod * pOfpPortMod, UINT4 *pu4Error,
                            VOID *pConnPtr)
{

    OFC_TRC_FUNC ((OFC_FN_ENTRY, "FUNC:OfcPortModu4PortNoValidate"));
    UNUSED_PARAM (pu4Error);
    UNUSED_PARAM (pConnPtr);
    UNUSED_PARAM (pOfpPortMod);
    OFC_TRC_FUNC ((OFC_FN_EXIT, "FUNC:OfcPortModu4PortNoValidate"));
    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  OfcPortModau1PadValidate
 * Description :  This function is for au1Pad Validate 
 * Input       :  pOfpPortMod Pointer to the header and pConnPtr Conn Ptr
 * Output      :  pu4Error - Error on failure 
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE 
 ****************************************************************************/
INT4
OfcPortModau1PadValidate (tOfpPortMod * pOfpPortMod, UINT4 *pu4Error,
                          VOID *pConnPtr)
{

    OFC_TRC_FUNC ((OFC_FN_ENTRY, "FUNC:OfcPortModau1PadValidate"));
    UNUSED_PARAM (pu4Error);
    UNUSED_PARAM (pConnPtr);
    UNUSED_PARAM (pOfpPortMod);
    OFC_TRC_FUNC ((OFC_FN_EXIT, "FUNC:OfcPortModau1PadValidate"));
    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  OfcPortModau1HwAddrValidate
 * Description :  This function is for au1HwAddr Validate 
 * Input       :  pOfpPortMod Pointer to the header and pConnPtr Conn Ptr
 * Output      :  pu4Error - Error on failure 
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE 
 ****************************************************************************/
INT4
OfcPortModau1HwAddrValidate (tOfpPortMod * pOfpPortMod, UINT4 *pu4Error,
                             VOID *pConnPtr)
{

    OFC_TRC_FUNC ((OFC_FN_ENTRY, "FUNC:OfcPortModau1HwAddrValidate"));
    UNUSED_PARAM (pu4Error);
    UNUSED_PARAM (pConnPtr);
    UNUSED_PARAM (pOfpPortMod);
    OFC_TRC_FUNC ((OFC_FN_EXIT, "FUNC:OfcPortModau1HwAddrValidate"));
    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  OfcPortModau1Pad2Validate
 * Description :  This function is for au1Pad2 Validate 
 * Input       :  pOfpPortMod Pointer to the header and pConnPtr Conn Ptr
 * Output      :  pu4Error - Error on failure 
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE 
 ****************************************************************************/
INT4
OfcPortModau1Pad2Validate (tOfpPortMod * pOfpPortMod, UINT4 *pu4Error,
                           VOID *pConnPtr)
{

    OFC_TRC_FUNC ((OFC_FN_ENTRY, "FUNC:OfcPortModau1Pad2Validate"));
    UNUSED_PARAM (pu4Error);
    UNUSED_PARAM (pConnPtr);
    UNUSED_PARAM (pOfpPortMod);
    OFC_TRC_FUNC ((OFC_FN_EXIT, "FUNC:OfcPortModau1Pad2Validate"));
    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  OfcPortModu4ConfigValidate
 * Description :  This function is for u4Config Validate 
 * Input       :  pOfpPortMod Pointer to the header and pConnPtr Conn Ptr
 * Output      :  pu4Error - Error on failure 
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE 
 ****************************************************************************/
INT4
OfcPortModu4ConfigValidate (tOfpPortMod * pOfpPortMod, UINT4 *pu4Error,
                            VOID *pConnPtr)
{

    OFC_TRC_FUNC ((OFC_FN_ENTRY, "FUNC:OfcPortModu4ConfigValidate"));
    UNUSED_PARAM (pu4Error);
    UNUSED_PARAM (pConnPtr);
    UNUSED_PARAM (pOfpPortMod);
    OFC_TRC_FUNC ((OFC_FN_EXIT, "FUNC:OfcPortModu4ConfigValidate"));
    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  OfcPortModu4MaskValidate
 * Description :  This function is for u4Mask Validate 
 * Input       :  pOfpPortMod Pointer to the header and pConnPtr Conn Ptr
 * Output      :  pu4Error - Error on failure 
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE 
 ****************************************************************************/
INT4
OfcPortModu4MaskValidate (tOfpPortMod * pOfpPortMod, UINT4 *pu4Error,
                          VOID *pConnPtr)
{

    OFC_TRC_FUNC ((OFC_FN_ENTRY, "FUNC:OfcPortModu4MaskValidate"));
    UNUSED_PARAM (pu4Error);
    UNUSED_PARAM (pConnPtr);
    UNUSED_PARAM (pOfpPortMod);
    OFC_TRC_FUNC ((OFC_FN_EXIT, "FUNC:OfcPortModu4MaskValidate"));
    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  OfcPortModu4AdvertiseValidate
 * Description :  This function is for u4Advertise Validate 
 * Input       :  pOfpPortMod Pointer to the header and pConnPtr Conn Ptr
 * Output      :  pu4Error - Error on failure 
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE 
 ****************************************************************************/
INT4
OfcPortModu4AdvertiseValidate (tOfpPortMod * pOfpPortMod, UINT4 *pu4Error,
                               VOID *pConnPtr)
{

    OFC_TRC_FUNC ((OFC_FN_ENTRY, "FUNC:OfcPortModu4AdvertiseValidate"));
    UNUSED_PARAM (pu4Error);
    UNUSED_PARAM (pConnPtr);
    UNUSED_PARAM (pOfpPortMod);
    OFC_TRC_FUNC ((OFC_FN_EXIT, "FUNC:OfcPortModu4AdvertiseValidate"));
    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  OfcPortModau1Pad3Validate
 * Description :  This function is for au1Pad3 Validate 
 * Input       :  pOfpPortMod Pointer to the header and pConnPtr Conn Ptr
 * Output      :  pu4Error - Error on failure 
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE 
 ****************************************************************************/
INT4
OfcPortModau1Pad3Validate (tOfpPortMod * pOfpPortMod, UINT4 *pu4Error,
                           VOID *pConnPtr)
{

    OFC_TRC_FUNC ((OFC_FN_ENTRY, "FUNC:OfcPortModau1Pad3Validate"));
    UNUSED_PARAM (pu4Error);
    UNUSED_PARAM (pConnPtr);
    UNUSED_PARAM (pOfpPortMod);
    OFC_TRC_FUNC ((OFC_FN_EXIT, "FUNC:OfcPortModau1Pad3Validate"));
    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  OfcStatsRequ2TypeValidate
 * Description :  This function is for u2Type Validate 
 * Input       :  pOfpStatsReq Pointer to the header and pConnPtr Conn Ptr
 * Output      :  pu4Error - Error on failure 
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE 
 ****************************************************************************/
INT4
OfcStatsRequ2TypeValidate (tOfpStatsReq * pOfpStatsReq, UINT4 *pu4Error,
                           VOID *pConnPtr)
{

    OFC_TRC_FUNC ((OFC_FN_ENTRY, "FUNC:OfcStatsRequ2TypeValidate"));
    UNUSED_PARAM (pu4Error);
    UNUSED_PARAM (pConnPtr);
    UNUSED_PARAM (pOfpStatsReq);
    OFC_TRC_FUNC ((OFC_FN_EXIT, "FUNC:OfcStatsRequ2TypeValidate"));
    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  OfcStatsRequ2FlagsValidate
 * Description :  This function is for u2Flags Validate 
 * Input       :  pOfpStatsReq Pointer to the header and pConnPtr Conn Ptr
 * Output      :  pu4Error - Error on failure 
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE 
 ****************************************************************************/
INT4
OfcStatsRequ2FlagsValidate (tOfpStatsReq * pOfpStatsReq, UINT4 *pu4Error,
                            VOID *pConnPtr)
{

    OFC_TRC_FUNC ((OFC_FN_ENTRY, "FUNC:OfcStatsRequ2FlagsValidate"));
    UNUSED_PARAM (pu4Error);
    UNUSED_PARAM (pConnPtr);
    UNUSED_PARAM (pOfpStatsReq);
    OFC_TRC_FUNC ((OFC_FN_EXIT, "FUNC:OfcStatsRequ2FlagsValidate"));
    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  OfcStatsReqau1BodyValidate
 * Description :  This function is for au1Body Validate 
 * Input       :  pOfpStatsReq Pointer to the header and pConnPtr Conn Ptr
 * Output      :  pu4Error - Error on failure 
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE 
 ****************************************************************************/
INT4
OfcStatsReqau1BodyValidate (tOfpStatsReq * pOfpStatsReq, UINT4 *pu4Error,
                            VOID *pConnPtr)
{

    OFC_TRC_FUNC ((OFC_FN_ENTRY, "FUNC:OfcStatsReqau1BodyValidate"));
    UNUSED_PARAM (pu4Error);
    UNUSED_PARAM (pConnPtr);
    UNUSED_PARAM (pOfpStatsReq);
    OFC_TRC_FUNC ((OFC_FN_EXIT, "FUNC:OfcStatsReqau1BodyValidate"));
    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  OfcQueueRequ2PortValidate
 * Description :  This function is for u2Port Validate 
 * Input       :  pOfpQueueReq Pointer to the header and pConnPtr Conn Ptr
 * Output      :  pu4Error - Error on failure 
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE 
 ****************************************************************************/
INT4
OfcQueueRequ2PortValidate (tOfpQueueReq * pOfpQueueReq, UINT4 *pu4Error,
                           VOID *pConnPtr)
{

    OFC_TRC_FUNC ((OFC_FN_ENTRY, "FUNC:OfcQueueRequ2PortValidate"));
    UNUSED_PARAM (pu4Error);
    UNUSED_PARAM (pConnPtr);
    UNUSED_PARAM (pOfpQueueReq);
    OFC_TRC_FUNC ((OFC_FN_EXIT, "FUNC:OfcQueueRequ2PortValidate"));
    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  OfcQueueReqau1PadValidate
 * Description :  This function is for au1Pad Validate 
 * Input       :  pOfpQueueReq Pointer to the header and pConnPtr Conn Ptr
 * Output      :  pu4Error - Error on failure 
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE 
 ****************************************************************************/
INT4
OfcQueueReqau1PadValidate (tOfpQueueReq * pOfpQueueReq, UINT4 *pu4Error,
                           VOID *pConnPtr)
{

    OFC_TRC_FUNC ((OFC_FN_ENTRY, "FUNC:OfcQueueReqau1PadValidate"));
    UNUSED_PARAM (pu4Error);
    UNUSED_PARAM (pConnPtr);
    UNUSED_PARAM (pOfpQueueReq);
    OFC_TRC_FUNC ((OFC_FN_EXIT, "FUNC:OfcQueueReqau1PadValidate"));
    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  OfcOfpu1VersionAction
 * Description :  This function is for u1Version Action  
 * Input       :  pOfpPkt Pointer to the header and pConnPtr Conn Ptr
 * Output      :  pu4Error - Error on failure 
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE 
 ****************************************************************************/
INT4
OfcOfpu1VersionAction (tOfpPkt * pOfpPkt, UINT4 *pu4Error, VOID *pConnPtr)
{

    INT4                i4RetVal = OSIX_SUCCESS;
    OFC_TRC_FUNC ((OFC_FN_ENTRY, "FUNC:OfcOfpu1VersionAction"));
    UNUSED_PARAM (pu4Error);
    UNUSED_PARAM (pConnPtr);
    UNUSED_PARAM (pOfpPkt);
    OFC_TRC_FUNC ((OFC_FN_EXIT, "FUNC:OfcOfpu1VersionAction"));
    return i4RetVal;
}

/****************************************************************************
 * Function    :  OfcOfpu1TypeAction
 * Description :  This function is for u1Type Action  
 * Input       :  pOfpPkt Pointer to the header and pConnPtr Conn Ptr
 * Output      :  pu4Error - Error on failure 
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE 
 ****************************************************************************/
INT4
OfcOfpu1TypeAction (tOfpPkt * pOfpPkt, UINT4 *pu4Error, VOID *pConnPtr)
{

    INT4                i4RetVal = OSIX_SUCCESS;
    OFC_TRC_FUNC ((OFC_FN_ENTRY, "FUNC:OfcOfpu1TypeAction"));
    switch (pOfpPkt->u1Type)
    {
        case OFC_ZERO:
            i4RetVal =
                OfcHelloPktParse (&(pOfpPkt->hl.Hello), pu4Error, pConnPtr);
            break;
        case OFC_TWO:
            i4RetVal =
                OfcEchoReqMsgPktParse (&(pOfpPkt->hl.EchoReqMsg), pu4Error,
                                       pConnPtr);
            break;
        case OFC_THREE:
            i4RetVal =
                OfcEchoRplyMsgPktParse (&(pOfpPkt->hl.EchoRplyMsg), pu4Error,
                                        pConnPtr);
            break;
        case OFC_FOUR:
            i4RetVal =
                OfcExperimenterHdrPktParse (&(pOfpPkt->hl.ExperimenterHdr),
                                            pu4Error, pConnPtr);
            break;
        case OFC_FIVE:
            i4RetVal =
                OfcFeatureReqPktParse (&(pOfpPkt->hl.FeatureReq), pu4Error,
                                       pConnPtr);
            break;
        case OFC_SEVEN:
            i4RetVal =
                OfcConfigGetPktParse (&(pOfpPkt->hl.ConfigGet), pu4Error,
                                      pConnPtr);
            break;
        case OFC_NINE:
            i4RetVal =
                OfcConfigSetPktParse (&(pOfpPkt->hl.ConfigSet), pu4Error,
                                      pConnPtr);
            break;
        case OFC_THIRTEEN:
            i4RetVal =
                OfcPktOutPktParse (&(pOfpPkt->hl.PktOut), pu4Error, pConnPtr);
            break;
        case OFC_FOURTEEN:
            i4RetVal =
                OfcFlowModPktParse (&(pOfpPkt->hl.FlowMod), pu4Error, pConnPtr);
            break;
        case OFC_FIFTEEN:
            i4RetVal =
                OfcPortModPktParse (&(pOfpPkt->hl.PortMod), pu4Error, pConnPtr);
            break;
        case OFC_SIXTEEN:
            i4RetVal =
                OfcStatsReqPktParse (&(pOfpPkt->hl.StatsReq), pu4Error,
                                     pConnPtr);
            break;
        case OFC_EIGHTEEN:
            i4RetVal =
                OfcBarrierReqPktParse (&(pOfpPkt->hl.BarrierReq), pu4Error,
                                       pConnPtr);
            break;
        case OFC_TWENTY:
            i4RetVal =
                OfcQueueReqPktParse (&(pOfpPkt->hl.QueueReq), pu4Error,
                                     pConnPtr);
            break;
        default:
            break;
    }
    OFC_TRC_FUNC ((OFC_FN_EXIT, "FUNC:OfcOfpu1TypeAction"));
    return i4RetVal;
}

/****************************************************************************
 * Function    :  OfcOfpu2LengthAction
 * Description :  This function is for u2Length Action  
 * Input       :  pOfpPkt Pointer to the header and pConnPtr Conn Ptr
 * Output      :  pu4Error - Error on failure 
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE 
 ****************************************************************************/
INT4
OfcOfpu2LengthAction (tOfpPkt * pOfpPkt, UINT4 *pu4Error, VOID *pConnPtr)
{
    INT4                i4RetVal = OSIX_SUCCESS;
    OFC_TRC_FUNC ((OFC_FN_ENTRY, "FUNC:OfcOfpu2LengthAction"));
    UNUSED_PARAM (pu4Error);
    ((tOfcFsofcControllerConnEntry *) pConnPtr)->u4RxLength = pOfpPkt->u2Length;
    OFC_TRC_FUNC ((OFC_FN_EXIT, "FUNC:OfcOfpu2LengthAction"));
    return i4RetVal;
}

/****************************************************************************
 * Function    :  OfcOfpu4XidAction
 * Description :  This function is for u4Xid Action  
 * Input       :  pOfpPkt Pointer to the header and pConnPtr Conn Ptr
 * Output      :  pu4Error - Error on failure 
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE 
 ****************************************************************************/
INT4
OfcOfpu4XidAction (tOfpPkt * pOfpPkt, UINT4 *pu4Error, VOID *pConnPtr)
{

    INT4                i4RetVal = OSIX_SUCCESS;
    OFC_TRC_FUNC ((OFC_FN_ENTRY, "FUNC:OfcOfpu4XidAction"));
    UNUSED_PARAM (pu4Error);
    ((tOfcFsofcControllerConnEntry *) pConnPtr)->u4RxXid = pOfpPkt->u4Xid;
    OFC_TRC_FUNC ((OFC_FN_EXIT, "FUNC:OfcOfpu4XidAction"));
    return i4RetVal;
}

/****************************************************************************
 * Function    :  OfcHelloau1DummyAction
 * Description :  This function is for au1Dummy Action  
 * Input       :  pOfpHello Pointer to the header and pConnPtr Conn Ptr
 * Output      :  pu4Error - Error on failure 
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE 
 ****************************************************************************/
INT4
OfcHelloau1DummyAction (tOfpHello * pOfpHello, UINT4 *pu4Error, VOID *pConnPtr)
{

    INT4                i4RetVal = OSIX_SUCCESS;
    OFC_TRC_FUNC ((OFC_FN_ENTRY, "FUNC:OfcHelloau1DummyAction"));
    UNUSED_PARAM (pu4Error);
    OfcHelloPktSend (pOfpHello, pConnPtr);
    OFC_TRC_FUNC ((OFC_FN_EXIT, "FUNC:OfcHelloau1DummyAction"));
    return i4RetVal;
}

/****************************************************************************
 * Function    :  OfcEchoReqMsgau1DataAction
 * Description :  This function is for au1Data Action  
 * Input       :  pOfpEchoReqMsg Pointer to the header and pConnPtr Conn Ptr
 * Output      :  pu4Error - Error on failure 
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE 
 ****************************************************************************/
INT4
OfcEchoReqMsgau1DataAction (tOfpEchoReqMsg * pOfpEchoReqMsg, UINT4 *pu4Error,
                            VOID *pConnPtr)
{
    INT4                i4RetVal = OSIX_SUCCESS;
    OFC_TRC_FUNC ((OFC_FN_ENTRY, "FUNC:OfcEchoReqMsgau1DataAction"));
    UNUSED_PARAM (pu4Error);
    /* Request type cast to reply so data copy is not required */
    OfcEchoRplyMsgPktSend ((tOfpEchoRplyMsg *) pOfpEchoReqMsg, pConnPtr);
    OFC_TRC_FUNC ((OFC_FN_EXIT, "FUNC:OfcEchoReqMsgau1DataAction"));
    return i4RetVal;
}

/****************************************************************************
 * Function    :  OfcEchoRplyMsgau1DataAction
 * Description :  This function is for au1Data Action  
 * Input       :  pOfpEchoRplyMsg Pointer to the header and pConnPtr Conn Ptr
 * Output      :  pu4Error - Error on failure 
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE 
 ****************************************************************************/
INT4
OfcEchoRplyMsgau1DataAction (tOfpEchoRplyMsg * pOfpEchoRplyMsg, UINT4 *pu4Error,
                             VOID *pConnPtr)
{

    INT4                i4RetVal = OSIX_SUCCESS;
    OFC_TRC_FUNC ((OFC_FN_ENTRY, "FUNC:OfcEchoRplyMsgau1DataAction"));
    UNUSED_PARAM (pu4Error);
    UNUSED_PARAM (pConnPtr);
    UNUSED_PARAM (pOfpEchoRplyMsg);
    OFC_TRC_FUNC ((OFC_FN_EXIT, "FUNC:OfcEchoRplyMsgau1DataAction"));
    return i4RetVal;
}

/****************************************************************************
 * Function    :  OfcExperimenterHdru4ExperimenterAction
 * Description :  This function is for u4Experimenter Action  
 * Input       :  pOfpExperimenterHdr Pointer to the header and pConnPtr Conn Ptr
 * Output      :  pu4Error - Error on failure 
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE 
 ****************************************************************************/
INT4
OfcExperimenterHdru4ExperimenterAction (tOfpExperimenterHdr *
                                        pOfpExperimenterHdr, UINT4 *pu4Error,
                                        VOID *pConnPtr)
{

    /* We will not be supporting any vendor for now, return error */
    OFC_TRC_FUNC ((OFC_FN_ENTRY,
                   "FUNC:OfcExperimenterHdru4ExperimenterAction"));
    *pu4Error = OFC_EXPERIMENTERHDR_U4EXPERIMENTER_VALIDATE_ERR;
    UNUSED_PARAM (pConnPtr);
    UNUSED_PARAM (pOfpExperimenterHdr);
    OFC_TRC_FUNC ((OFC_FN_EXIT, "FUNC:OfcExperimenterHdru4ExperimenterAction"));
    return OSIX_FAILURE;
}

/****************************************************************************
 * Function    :  OfcFeatureReqau1DummyAction
 * Description :  This function is for au1Dummy Action  
 * Input       :  pOfpFeatureReq Pointer to the header and pConnPtr Conn Ptr
 * Output      :  pu4Error - Error on failure 
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE 
 ****************************************************************************/
INT4
OfcFeatureReqau1DummyAction (tOfpFeatureReq * pOfpFeatureReq, UINT4 *pu4Error,
                             VOID *pConnPtr)
{
    INT4                i4RetVal = OSIX_SUCCESS;
    UINT4               u4Count = OFC_ZERO;
    UINT1              *pu1DataPathId = NULL;
    tOfpPhyPort        *pOfpPhyPort = NULL;
    tOfcFsofcIfEntry   *pOfcFsofcIfEntry = NULL;
    tOfpFeatureRply    *pOfpFeatureRply =
        (tOfpFeatureRply *) (VOID *) pOfpFeatureReq;
    tOfcFsofcControllerConnEntry *pLocal = pConnPtr;
    tOfcFsofcCfgEntry  *pOfcFsofcCfgEntry = NULL;
    UINT1               u1State = CFA_IF_DOWN;

    OFC_TRC_FUNC ((OFC_FN_ENTRY, "\r\nFUNC:OfcFeatureReqau1DummyAction\n"));
    UNUSED_PARAM (pu4Error);

    pOfcFsofcCfgEntry =
        OfcGetFsofcCfgEntry (pLocal->MibObject.u4FsofcContextId);
    if (pOfcFsofcCfgEntry == NULL)
    {
        OFC131_TRC_FUNC ((OFC_PKT_TRC, "\r\nOfcFeatureReqau1DummyAction: "
                          "Get CFG Entry Failed\n"));
        return OSIX_FAILURE;
    }

    /* Update Data Path ID */
    pu1DataPathId = (UINT1 *) &pOfpFeatureRply->u8DatapathiId;
    pu1DataPathId[OFC_ZERO] = pu1DataPathId[1] = OFC_ZERO;
    pu1DataPathId[OFC_ONE] =
        (UINT1) pOfcFsofcCfgEntry->MibObject.u4FsofcContextId;
    MEMCPY (&pu1DataPathId[OFC_TWO], IssGetSwitchMacFromNvRam (),
            CFA_ENET_ADDR_LEN);

    pOfpFeatureRply->u4NoOfBuf = OFP_MAX_BUF_CACHE;
    pOfpFeatureRply->u1NoOfTables = OFP_MAX_NO_TABLES;
    pOfpFeatureRply->u4Capabilities = OFPC_PORT_STATS | OFPC_FLOW_STATS;

    if (pOfcFsofcCfgEntry->MibObject.i4FsofcPortStpStatus == OPENFLOW_ENABLE)
    {
        pOfpFeatureRply->u4Capabilities =
            pOfpFeatureRply->u4Capabilities | OFPC_STP;
    }

    if (pOfcFsofcCfgEntry->MibObject.i4FsofcIpReassembleStatus ==
        OPENFLOW_ENABLE)
    {
        pOfpFeatureRply->u4Capabilities =
            pOfpFeatureRply->u4Capabilities | OFPC_IP_REASM;
    }

    pOfpFeatureRply->u4Actions =
        OFC_ONE << OFPAT_OUTPUT | OFC_ONE << OFPAT_SET_DL_SRC | OFC_ONE <<
        OFPAT_SET_DL_DST;

    pLocal->u4TxDataLength = OFC_ZERO;

    pOfcFsofcIfEntry = OfcGetFirstFsofcIfTable ();

    while (pOfcFsofcIfEntry != NULL)
    {
        pOfpPhyPort = &pOfpFeatureRply->PhyPort[u4Count];
        pOfpPhyPort->u2Port =
            OSIX_HTONS (pOfcFsofcIfEntry->MibObject.i4FsofcIfIndex);
        MEMCPY (pOfpPhyPort->au1HwAddr, IssGetSwitchMacFromNvRam (),
                CFA_ENET_ADDR_LEN);
        MEMCPY (pOfpPhyPort->au1Name,
                pOfcFsofcIfEntry->MibObject.au1FsofcIfAlias, OFC_SIXTEEN);
        pOfpPhyPort->au1Name[15] = OFC_ZERO;
        pOfpPhyPort->u4Config = OFC_PORT_CONFIG_ALL;
        CfaGetIfOperStatus ((UINT4)
                            (pOfcFsofcIfEntry->MibObject.i4FsofcIfIndex),
                            &u1State);
        pOfpPhyPort->u4State = OSIX_HTONL (OFPPS_LINK_DOWN);
        if (CFA_IF_UP == u1State)
        {
            pOfpPhyPort->u4State = OSIX_HTONL (OFPPS_LINK_UP);
        }
        pOfpPhyPort->u4Curr = OFC_ZERO;
        pOfpPhyPort->u4Advertised = OFC_ZERO;
        pOfpPhyPort->u4Supported = OFC_ZERO;
        pOfpPhyPort->u4Peer = OFC_ZERO;
        pLocal->u4TxDataLength += sizeof (tOfpPhyPort);
        u4Count++;
        pOfcFsofcIfEntry = OfcGetNextFsofcIfTable (pOfcFsofcIfEntry);
    }
    i4RetVal = OfcFeatureRplyPktSend (pOfpFeatureRply, pConnPtr);
    OFC_TRC_FUNC ((OFC_FN_EXIT, "FUNC:OfcFeatureReqau1DummyAction"));
    return i4RetVal;
}

/****************************************************************************
 * Function    :  OfcConfigGetau1DummyAction
 * Description :  This function is for au1Dummy Action  
 * Input       :  pOfpConfigGet Pointer to the header and pConnPtr Conn Ptr
 * Output      :  pu4Error - Error on failure 
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE 
 ****************************************************************************/
INT4
OfcConfigGetau1DummyAction (tOfpConfigGet * pOfpConfigGet, UINT4 *pu4Error,
                            VOID *pConnPtr)
{

    INT4                i4RetVal = OSIX_SUCCESS;
    tOfpConfigRply     *pOfpConfigRply =
        (tOfpConfigRply *) (VOID *) pOfpConfigGet;
    OFC_TRC_FUNC ((OFC_FN_ENTRY, "FUNC:OfcConfigGetau1DummyAction"));
    UNUSED_PARAM (pu4Error);
    UNUSED_PARAM (pConnPtr);
    UNUSED_PARAM (pOfpConfigGet);
    pOfpConfigRply->u2Flags = OFC_ZERO;
    pOfpConfigRply->u2MissSendLen = gu2MissSendLen;
    OfcConfigRplyPktSend (pOfpConfigRply, pConnPtr);
    OFC_TRC_FUNC ((OFC_FN_EXIT, "FUNC:OfcConfigGetau1DummyAction"));
    return i4RetVal;
}

/****************************************************************************
 * Function    :  OfcConfigSetu2FlagsAction
 * Description :  This function is for u2Flags Action  
 * Input       :  pOfpConfigSet Pointer to the header and pConnPtr Conn Ptr
 * Output      :  pu4Error - Error on failure 
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE 
 ****************************************************************************/
INT4
OfcConfigSetu2FlagsAction (tOfpConfigSet * pOfpConfigSet, UINT4 *pu4Error,
                           VOID *pConnPtr)
{

    INT4                i4RetVal = OSIX_SUCCESS;
    OFC_TRC_FUNC ((OFC_FN_ENTRY, "FUNC:OfcConfigSetu2FlagsAction"));
    UNUSED_PARAM (pu4Error);
    UNUSED_PARAM (pConnPtr);
    UNUSED_PARAM (pOfpConfigSet);
    OFC_TRC_FUNC ((OFC_FN_EXIT, "FUNC:OfcConfigSetu2FlagsAction"));
    return i4RetVal;
}

/****************************************************************************
 * Function    :  OfcConfigSetu2MissSendLenAction
 * Description :  This function is for u2MissSendLen Action  
 * Input       :  pOfpConfigSet Pointer to the header and pConnPtr Conn Ptr
 * Output      :  pu4Error - Error on failure 
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE 
 ****************************************************************************/
INT4
OfcConfigSetu2MissSendLenAction (tOfpConfigSet * pOfpConfigSet, UINT4 *pu4Error,
                                 VOID *pConnPtr)
{

    INT4                i4RetVal = OSIX_SUCCESS;
    OFC_TRC_FUNC ((OFC_FN_ENTRY, "FUNC:OfcConfigSetu2MissSendLenAction"));
    UNUSED_PARAM (pu4Error);
    UNUSED_PARAM (pConnPtr);
    gu2MissSendLen = pOfpConfigSet->u2MissSendLen;
    OFC_TRC_FUNC ((OFC_FN_EXIT, "FUNC:OfcConfigSetu2MissSendLenAction"));
    return i4RetVal;
}

/****************************************************************************
 * Function    :  OfcPktOutu4BufferIdAction
 * Description :  This function is for u4BufferId Action  
 * Input       :  pOfpPktOut Pointer to the header and pConnPtr Conn Ptr
 * Output      :  pu4Error - Error on failure 
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE 
 ****************************************************************************/
INT4
OfcPktOutu4BufferIdAction (tOfpPktOut * pOfpPktOut, UINT4 *pu4Error,
                           VOID *pConnPtr)
{

    INT4                i4RetVal = OSIX_SUCCESS;
    OFC_TRC_FUNC ((OFC_FN_ENTRY, "FUNC:OfcPktOutu4BufferIdAction"));
    UNUSED_PARAM (pu4Error);
    UNUSED_PARAM (pConnPtr);
    UNUSED_PARAM (pOfpPktOut);
    OFC_TRC_FUNC ((OFC_FN_EXIT, "FUNC:OfcPktOutu4BufferIdAction"));
    return i4RetVal;
}

/****************************************************************************
 * Function    :  OfcPktOutu2InPortAction
 * Description :  This function is for u2InPort Action  
 * Input       :  pOfpPktOut Pointer to the header and pConnPtr Conn Ptr
 * Output      :  pu4Error - Error on failure 
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE 
 ****************************************************************************/
INT4
OfcPktOutu2InPortAction (tOfpPktOut * pOfpPktOut, UINT4 *pu4Error,
                         VOID *pConnPtr)
{

    INT4                i4RetVal = OSIX_SUCCESS;
    OFC_TRC_FUNC ((OFC_FN_ENTRY, "FUNC:OfcPktOutu2InPortAction"));
    UNUSED_PARAM (pu4Error);
    UNUSED_PARAM (pConnPtr);
    UNUSED_PARAM (pOfpPktOut);
    OFC_TRC_FUNC ((OFC_FN_EXIT, "FUNC:OfcPktOutu2InPortAction"));
    return i4RetVal;
}

/****************************************************************************
 * Function    :  OfcPktOutu2ActionsLenAction
 * Description :  This function is for u2ActionsLen Action  
 * Input       :  pOfpPktOut Pointer to the header and pConnPtr Conn Ptr
 * Output      :  pu4Error - Error on failure 
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE 
 ****************************************************************************/
INT4
OfcPktOutu2ActionsLenAction (tOfpPktOut * pOfpPktOut, UINT4 *pu4Error,
                             VOID *pConnPtr)
{

    INT4                i4RetVal = OSIX_SUCCESS;
    OFC_TRC_FUNC ((OFC_FN_ENTRY, "FUNC:OfcPktOutu2ActionsLenAction"));
    UNUSED_PARAM (pu4Error);
    UNUSED_PARAM (pConnPtr);
    UNUSED_PARAM (pOfpPktOut);
    OFC_TRC_FUNC ((OFC_FN_EXIT, "FUNC:OfcPktOutu2ActionsLenAction"));
    return i4RetVal;
}

/****************************************************************************
 * Function    :  OfcPktOutActionsAction
 * Description :  This function is for Actions Action  
 * Input       :  pOfpPktOut Pointer to the header and pConnPtr Conn Ptr
 * Output      :  pu4Error - Error on failure 
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE 
 ****************************************************************************/
INT4
OfcPktOutActionsAction (tOfpPktOut * pOfpPktOut, UINT4 *pu4Error,
                        VOID *pConnPtr)
{

    INT4                i4RetVal = OSIX_SUCCESS;
    UINT1              *pu1Buf = NULL;
    UINT4               u4DataLen = OFC_ZERO;
    UINT4               u4IfIndex = OFC_ZERO;
    tOfcFsofcControllerConnEntry *pLocal = pConnPtr;
    OFC_TRC_FUNC ((OFC_FN_ENTRY, "FUNC:OfcPktOutActionsAction"));
    UNUSED_PARAM (pu4Error);

    pu1Buf = (UINT1 *) pOfpPktOut;
    pu1Buf += (OFC_EIGHT + pOfpPktOut->u2ActionsLen);
    u4DataLen =
        (UINT4) (pLocal->u4RxLength -
                 ((UINT4) (pOfpPktOut->u2ActionsLen) + (UINT4) OFC_EIGHT +
                  (UINT4) OFC_EIGHT));
    u4IfIndex =
        OfcPktOutPortFromActionHdr (pOfpPktOut->Actions,
                                    (INT4) pOfpPktOut->u2ActionsLen);
    if (u4IfIndex == OFC_FLOOD_INDEX)
    {
        tOfcFsofcIfEntry   *pOfcFsofcIfEntry = NULL;
        pOfcFsofcIfEntry = OfcGetFirstFsofcIfTable ();
        while (pOfcFsofcIfEntry != NULL)
        {
            if (pOfcFsofcIfEntry->MibObject.i4FsofcIfIndex !=
                pOfpPktOut->u2InPort)
            {
                CfaGddEthWrite (pu1Buf,
                                (UINT2) (pOfcFsofcIfEntry->MibObject.
                                         i4FsofcIfIndex), u4DataLen);
            }
            pOfcFsofcIfEntry = OfcGetNextFsofcIfTable (pOfcFsofcIfEntry);
        }
    }
    else if (u4IfIndex != OFC_INVALID_INDEX)
    {
        CfaGddEthWrite (pu1Buf, (UINT2) u4IfIndex, u4DataLen);
    }
    OFC_TRC_FUNC ((OFC_FN_EXIT, "FUNC:OfcPktOutActionsAction"));
    return i4RetVal;
}

/****************************************************************************
 * Function    :  OfcFlowModMatchAction
 * Description :  This function is for Match Action  
 * Input       :  pOfpFlowMod Pointer to the header and pConnPtr Conn Ptr
 * Output      :  pu4Error - Error on failure 
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE 
 ****************************************************************************/
INT4
OfcFlowModMatchAction (tOfpFlowMod * pOfpFlowMod, UINT4 *pu4Error,
                       VOID *pConnPtr)
{

    INT4                i4RetVal = OSIX_SUCCESS;
    OFC_TRC_FUNC ((OFC_FN_ENTRY, "FUNC:OfcFlowModMatchAction"));
    UNUSED_PARAM (pu4Error);
    UNUSED_PARAM (pConnPtr);
    UNUSED_PARAM (pOfpFlowMod);
    OFC_TRC_FUNC ((OFC_FN_EXIT, "FUNC:OfcFlowModMatchAction"));
    return i4RetVal;
}

/****************************************************************************
 * Function    :  OfcFlowModu8CookieAction
 * Description :  This function is for u8Cookie Action  
 * Input       :  pOfpFlowMod Pointer to the header and pConnPtr Conn Ptr
 * Output      :  pu4Error - Error on failure 
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE 
 ****************************************************************************/
INT4
OfcFlowModu8CookieAction (tOfpFlowMod * pOfpFlowMod, UINT4 *pu4Error,
                          VOID *pConnPtr)
{

    INT4                i4RetVal = OSIX_SUCCESS;
    OFC_TRC_FUNC ((OFC_FN_ENTRY, "FUNC:OfcFlowModu8CookieAction"));
    UNUSED_PARAM (pu4Error);
    UNUSED_PARAM (pConnPtr);
    UNUSED_PARAM (pOfpFlowMod);
    OFC_TRC_FUNC ((OFC_FN_EXIT, "FUNC:OfcFlowModu8CookieAction"));
    return i4RetVal;
}

/****************************************************************************
 * Function    :  OfcFlowModu2CommandAction
 * Description :  This function is for u2Command Action  
 * Input       :  pOfpFlowMod Pointer to the header and pConnPtr Conn Ptr
 * Output      :  pu4Error - Error on failure 
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE 
 ****************************************************************************/
INT4
OfcFlowModu2CommandAction (tOfpFlowMod * pOfpFlowMod, UINT4 *pu4Error,
                           VOID *pConnPtr)
{

    INT4                i4RetVal = OSIX_SUCCESS;
    OFC_TRC_FUNC ((OFC_FN_ENTRY, "FUNC:OfcFlowModu2CommandAction"));
    UNUSED_PARAM (pu4Error);
    UNUSED_PARAM (pConnPtr);
    UNUSED_PARAM (pOfpFlowMod);
    OFC_TRC_FUNC ((OFC_FN_EXIT, "FUNC:OfcFlowModu2CommandAction"));
    return i4RetVal;
}

/****************************************************************************
 * Function    :  OfcFlowModu2IdleTimeoutAction
 * Description :  This function is for u2IdleTimeout Action  
 * Input       :  pOfpFlowMod Pointer to the header and pConnPtr Conn Ptr
 * Output      :  pu4Error - Error on failure 
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE 
 ****************************************************************************/
INT4
OfcFlowModu2IdleTimeoutAction (tOfpFlowMod * pOfpFlowMod, UINT4 *pu4Error,
                               VOID *pConnPtr)
{

    INT4                i4RetVal = OSIX_SUCCESS;
    OFC_TRC_FUNC ((OFC_FN_ENTRY, "FUNC:OfcFlowModu2IdleTimeoutAction"));
    UNUSED_PARAM (pu4Error);
    UNUSED_PARAM (pConnPtr);
    UNUSED_PARAM (pOfpFlowMod);
    OFC_TRC_FUNC ((OFC_FN_EXIT, "FUNC:OfcFlowModu2IdleTimeoutAction"));
    return i4RetVal;
}

/****************************************************************************
 * Function    :  OfcFlowModu2HardTimeoutAction
 * Description :  This function is for u2HardTimeout Action  
 * Input       :  pOfpFlowMod Pointer to the header and pConnPtr Conn Ptr
 * Output      :  pu4Error - Error on failure 
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE 
 ****************************************************************************/
INT4
OfcFlowModu2HardTimeoutAction (tOfpFlowMod * pOfpFlowMod, UINT4 *pu4Error,
                               VOID *pConnPtr)
{

    INT4                i4RetVal = OSIX_SUCCESS;
    OFC_TRC_FUNC ((OFC_FN_ENTRY, "FUNC:OfcFlowModu2HardTimeoutAction"));
    UNUSED_PARAM (pu4Error);
    UNUSED_PARAM (pConnPtr);
    UNUSED_PARAM (pOfpFlowMod);
    OFC_TRC_FUNC ((OFC_FN_EXIT, "FUNC:OfcFlowModu2HardTimeoutAction"));
    return i4RetVal;
}

/****************************************************************************
 * Function    :  OfcFlowModu2PriorityAction
 * Description :  This function is for u2Priority Action  
 * Input       :  pOfpFlowMod Pointer to the header and pConnPtr Conn Ptr
 * Output      :  pu4Error - Error on failure 
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE 
 ****************************************************************************/
INT4
OfcFlowModu2PriorityAction (tOfpFlowMod * pOfpFlowMod, UINT4 *pu4Error,
                            VOID *pConnPtr)
{

    INT4                i4RetVal = OSIX_SUCCESS;
    OFC_TRC_FUNC ((OFC_FN_ENTRY, "FUNC:OfcFlowModu2PriorityAction"));
    UNUSED_PARAM (pu4Error);
    UNUSED_PARAM (pConnPtr);
    UNUSED_PARAM (pOfpFlowMod);
    OFC_TRC_FUNC ((OFC_FN_EXIT, "FUNC:OfcFlowModu2PriorityAction"));
    return i4RetVal;
}

/****************************************************************************
 * Function    :  OfcFlowModu4BufIdAction
 * Description :  This function is for u4BufId Action  
 * Input       :  pOfpFlowMod Pointer to the header and pConnPtr Conn Ptr
 * Output      :  pu4Error - Error on failure 
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE 
 ****************************************************************************/
INT4
OfcFlowModu4BufIdAction (tOfpFlowMod * pOfpFlowMod, UINT4 *pu4Error,
                         VOID *pConnPtr)
{

    INT4                i4RetVal = OSIX_SUCCESS;
    OFC_TRC_FUNC ((OFC_FN_ENTRY, "FUNC:OfcFlowModu4BufIdAction"));
    UNUSED_PARAM (pu4Error);
    UNUSED_PARAM (pConnPtr);
    UNUSED_PARAM (pOfpFlowMod);
    OFC_TRC_FUNC ((OFC_FN_EXIT, "FUNC:OfcFlowModu4BufIdAction"));
    return i4RetVal;
}

/****************************************************************************
 * Function    :  OfcFlowModu2OutPortAction
 * Description :  This function is for u2OutPort Action  
 * Input       :  pOfpFlowMod Pointer to the header and pConnPtr Conn Ptr
 * Output      :  pu4Error - Error on failure 
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE 
 ****************************************************************************/
INT4
OfcFlowModu2OutPortAction (tOfpFlowMod * pOfpFlowMod, UINT4 *pu4Error,
                           VOID *pConnPtr)
{

    INT4                i4RetVal = OSIX_SUCCESS;
    OFC_TRC_FUNC ((OFC_FN_ENTRY, "FUNC:OfcFlowModu2OutPortAction"));
    UNUSED_PARAM (pu4Error);
    UNUSED_PARAM (pConnPtr);
    UNUSED_PARAM (pOfpFlowMod);
    OFC_TRC_FUNC ((OFC_FN_EXIT, "FUNC:OfcFlowModu2OutPortAction"));
    return i4RetVal;
}

/****************************************************************************
 * Function    :  OfcFlowModu2FlagsAction
 * Description :  This function is for u2Flags Action  
 * Input       :  pOfpFlowMod Pointer to the header and pConnPtr Conn Ptr
 * Output      :  pu4Error - Error on failure 
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE 
 ****************************************************************************/
INT4
OfcFlowModu2FlagsAction (tOfpFlowMod * pOfpFlowMod, UINT4 *pu4Error,
                         VOID *pConnPtr)
{

    INT4                i4RetVal = OSIX_SUCCESS;
    OFC_TRC_FUNC ((OFC_FN_ENTRY, "FUNC:OfcFlowModu2FlagsAction"));
    UNUSED_PARAM (pu4Error);
    UNUSED_PARAM (pConnPtr);
    UNUSED_PARAM (pOfpFlowMod);
    OFC_TRC_FUNC ((OFC_FN_EXIT, "FUNC:OfcFlowModu2FlagsAction"));
    return i4RetVal;
}

/****************************************************************************
 * Function    :  OfcFlowModActionsAction
 * Description :  This function is for Actions Action  
 * Input       :  pOfpFlowMod Pointer to the header and pConnPtr Conn Ptr
 * Output      :  pu4Error - Error on failure 
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE 
 ****************************************************************************/
INT4
OfcFlowModActionsAction (tOfpFlowMod * pOfpFlowMod, UINT4 *pu4Error,
                         VOID *pConnPtr)
{

    INT4                i4RetVal = OSIX_SUCCESS;
    INT4                i4ActionLen = OFC_ZERO;
    tOfcFlowEntry       FlowMsg;
    tOfcFsofcCfgEntry  *pOfcFsofcCfgEntry = NULL;
    tOfcFsofcControllerConnEntry *pLocal = pConnPtr;
    tOfpActionHdr      *pActionHdr = &(pOfpFlowMod->Actions[0]);
    tOfcActOutPort     *pActOutport = NULL;
    tOfcActDlAddr      *pActDlAddr = NULL;

    OFC_TRC_FUNC ((OFC_FN_ENTRY, "FUNC:OfcFlowModActionsAction"));

    MEMSET (&FlowMsg, OFC_ZERO, sizeof (tOfcFlowEntry));

    pOfcFsofcCfgEntry =
        OfcGetFsofcCfgEntry (pLocal->MibObject.u4FsofcContextId);
    if (pOfcFsofcCfgEntry == NULL)
    {
        OFC_TRC_FUNC ((OFC_PKT_TRC, "\r\nOfcFlowModActionsAction: "
                       "Get CFG Entry Failed\n"));
        return OSIX_FAILURE;
    }

    OfcFlowMatchToKey (&pOfpFlowMod->Match, &FlowMsg.key);
    FlowMsg.u2Command = pOfpFlowMod->u2Command;
    FlowMsg.u8Cookie = pOfpFlowMod->u8Cookie;
    FlowMsg.u2Priority = pOfpFlowMod->u2Priority;
    FlowMsg.u4BufId = pOfpFlowMod->u4BufId;
    FlowMsg.u4OutPort = pOfpFlowMod->u2OutPort;
    FlowMsg.u2Flags = pOfpFlowMod->u2Flags;
    FlowMsg.u2IdleTimeout = pOfpFlowMod->u2IdleTimeout;
    FlowMsg.u2HardTimeout = pOfpFlowMod->u2HardTimeout;

    FlowMsg.u4NumActs = OFC_ZERO;
    i4ActionLen =
        (INT4) ((INT4) pLocal->u4RxLength - (INT4) OFC_EIGHT -
                (INT4) OFC_SIXTY_FOUR);

    while (i4ActionLen > OFC_ZERO)
    {
        pActionHdr->u2Type = OSIX_NTOHS (pActionHdr->u2Type);
        pActionHdr->u2Len = OSIX_NTOHS (pActionHdr->u2Len);
        switch (pActionHdr->u2Type)
        {
            case OFPAT_OUTPUT:
                pActOutport = (tOfcActOutPort *) pActionHdr;
                pActOutport->u2OutPort = OSIX_NTOHS (pActOutport->u2OutPort);
                pActOutport->u2MaxLen = OSIX_NTOHS (pActOutport->u2MaxLen);
                MEMCPY (&FlowMsg.OfcActs[FlowMsg.u4NumActs], pActOutport,
                        pActionHdr->u2Len);
                FlowMsg.u4NumActs++;
                break;

            case OFPAT_SET_DL_SRC:
            case OFPAT_SET_DL_DST:
                pActDlAddr = (tOfcActDlAddr *) pActionHdr;
                MEMCPY (&FlowMsg.OfcActs[FlowMsg.u4NumActs], pActDlAddr,
                        pActionHdr->u2Len);
                FlowMsg.u4NumActs++;
                break;

            default:
                *pu4Error = OFC_FLOWMOD_ACTIONS_VALIDATE_ERR;
                return OSIX_FAILURE;
        }
        i4ActionLen -= pActionHdr->u2Len;
        pActionHdr =
            (tOfpActionHdr
             *) ((VOID *) (((UINT1 *) pActionHdr) + pActionHdr->u2Len));
        if (FlowMsg.u4NumActs == OFC_MAX_ACTIONS)
        {
            break;
        }
    }
    OfcFlowHandleFlowModMsgs (pOfcFsofcCfgEntry, &FlowMsg);

    OFC_TRC_FUNC ((OFC_FN_EXIT, "FUNC:OfcFlowModActionsAction"));
    return i4RetVal;
}

/****************************************************************************
 * Function    :  OfcPortModu4PortNoAction
 * Description :  This function is for u4PortNo Action  
 * Input       :  pOfpPortMod Pointer to the header and pConnPtr Conn Ptr
 * Output      :  pu4Error - Error on failure 
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE 
 ****************************************************************************/
INT4
OfcPortModu4PortNoAction (tOfpPortMod * pOfpPortMod, UINT4 *pu4Error,
                          VOID *pConnPtr)
{

    INT4                i4RetVal = OSIX_SUCCESS;
    OFC_TRC_FUNC ((OFC_FN_ENTRY, "FUNC:OfcPortModu4PortNoAction"));
    UNUSED_PARAM (pu4Error);
    UNUSED_PARAM (pConnPtr);
    UNUSED_PARAM (pOfpPortMod);
    OFC_TRC_FUNC ((OFC_FN_EXIT, "FUNC:OfcPortModu4PortNoAction"));
    return i4RetVal;
}

/****************************************************************************
 * Function    :  OfcPortModau1PadAction
 * Description :  This function is for au1Pad Action  
 * Input       :  pOfpPortMod Pointer to the header and pConnPtr Conn Ptr
 * Output      :  pu4Error - Error on failure 
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE 
 ****************************************************************************/
INT4
OfcPortModau1PadAction (tOfpPortMod * pOfpPortMod, UINT4 *pu4Error,
                        VOID *pConnPtr)
{

    INT4                i4RetVal = OSIX_SUCCESS;
    OFC_TRC_FUNC ((OFC_FN_ENTRY, "FUNC:OfcPortModau1PadAction"));
    UNUSED_PARAM (pu4Error);
    UNUSED_PARAM (pConnPtr);
    UNUSED_PARAM (pOfpPortMod);
    OFC_TRC_FUNC ((OFC_FN_EXIT, "FUNC:OfcPortModau1PadAction"));
    return i4RetVal;
}

/****************************************************************************
 * Function    :  OfcPortModau1HwAddrAction
 * Description :  This function is for au1HwAddr Action  
 * Input       :  pOfpPortMod Pointer to the header and pConnPtr Conn Ptr
 * Output      :  pu4Error - Error on failure 
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE 
 ****************************************************************************/
INT4
OfcPortModau1HwAddrAction (tOfpPortMod * pOfpPortMod, UINT4 *pu4Error,
                           VOID *pConnPtr)
{

    INT4                i4RetVal = OSIX_SUCCESS;
    OFC_TRC_FUNC ((OFC_FN_ENTRY, "FUNC:OfcPortModau1HwAddrAction"));
    UNUSED_PARAM (pu4Error);
    UNUSED_PARAM (pConnPtr);
    UNUSED_PARAM (pOfpPortMod);
    OFC_TRC_FUNC ((OFC_FN_EXIT, "FUNC:OfcPortModau1HwAddrAction"));
    return i4RetVal;
}

/****************************************************************************
 * Function    :  OfcPortModau1Pad2Action
 * Description :  This function is for au1Pad2 Action  
 * Input       :  pOfpPortMod Pointer to the header and pConnPtr Conn Ptr
 * Output      :  pu4Error - Error on failure 
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE 
 ****************************************************************************/
INT4
OfcPortModau1Pad2Action (tOfpPortMod * pOfpPortMod, UINT4 *pu4Error,
                         VOID *pConnPtr)
{

    INT4                i4RetVal = OSIX_SUCCESS;
    OFC_TRC_FUNC ((OFC_FN_ENTRY, "FUNC:OfcPortModau1Pad2Action"));
    UNUSED_PARAM (pu4Error);
    UNUSED_PARAM (pConnPtr);
    UNUSED_PARAM (pOfpPortMod);
    OFC_TRC_FUNC ((OFC_FN_EXIT, "FUNC:OfcPortModau1Pad2Action"));
    return i4RetVal;
}

/****************************************************************************
 * Function    :  OfcPortModu4ConfigAction
 * Description :  This function is for u4Config Action  
 * Input       :  pOfpPortMod Pointer to the header and pConnPtr Conn Ptr
 * Output      :  pu4Error - Error on failure 
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE 
 ****************************************************************************/
INT4
OfcPortModu4ConfigAction (tOfpPortMod * pOfpPortMod, UINT4 *pu4Error,
                          VOID *pConnPtr)
{
    tCfaIfInfo          cfaifInfo;
    tOfpPortStatus      OfpPortSt;
    tOfcFsofcIfEntry    lOfcFsofcIfEntry;
    tOfcFsofcIfEntry   *pOfcFsofcIfEntry = NULL;
    UINT1               u1OperStatus = OFC_ZERO;
    UINT4               u4PortNum = (UINT4) pOfpPortMod->u2PortNo;
    UINT4               u4Action = OFC_ZERO;

    OFC_TRC_FUNC ((OFC_FN_ENTRY, "FUNC:OfcPortModu4ConfigAction"));
    MEMSET (&lOfcFsofcIfEntry, OFC_ZERO, sizeof (tOfcFsofcIfEntry));
    MEMSET (&cfaifInfo, OFC_ZERO, sizeof (tCfaIfInfo));
    MEMSET (&OfpPortSt, OFC_ZERO, sizeof (tOfpPortStatus));
    if (CfaGetIfInfo (u4PortNum, &cfaifInfo) == CFA_FAILURE)
    {
        OFC_TRC_FUNC ((OFC_PKT_TRC, "CfaGetIfInfo Failed for Port Num : %d\n",
                       u4PortNum));
        return OSIX_FAILURE;
    }

    u4Action = pOfpPortMod->u4Config & pOfpPortMod->u4Mask;
    if ((u4Action & OFPPC_PORT_DOWN) == OFPPC_PORT_DOWN)    /*OFPPC_PORT_DOWN bit set */
    {

        if (cfaifInfo.u1IfOperStatus == CFA_IF_DOWN)
        {
            OFC131_TRC_FUNC ((OFC_PKT_TRC,
                              "Operstatus for Port Num : %d\n is already down",
                              u4PortNum));
            return OSIX_SUCCESS;
        }
        else
        {
            u1OperStatus = CFA_IF_DOWN;
            CfaInterfaceStatusChangeIndication (u4PortNum, u1OperStatus);
            OfpPortSt.PhyPort.u2Port = (UINT2) u4PortNum;
            MEMCPY(OfpPortSt.PhyPort.au1Name, cfaifInfo.au1IfAliasName, OFC_SIXTEEN);
            OfpPortSt.PhyPort.u4State = (UINT4) u1OperStatus;
            MEMCPY(OfpPortSt.PhyPort.au1HwAddr, cfaifInfo.au1MacAddr,
                            sizeof(OfpPortSt.PhyPort.au1HwAddr));
            OfpPortSt.PhyPort.u4Curr = OFC_ZERO;
            OfpPortSt.PhyPort.u4Supported = OFC_ZERO;
            OfpPortSt.PhyPort.u4Advertised = OFC_ZERO;
            OfpPortSt.PhyPort.u4Peer = OFC_ZERO;
            OfpPortSt.u1Reason = OFPPR_MODIFY;

            MEMSET (&lOfcFsofcIfEntry.MibObject, OFC_ZERO,
                    sizeof (lOfcFsofcIfEntry.MibObject));
            lOfcFsofcIfEntry.MibObject.u4FsofcContextId =
                (INT4) OFC_DEFAULT_CONTEXT;
            lOfcFsofcIfEntry.MibObject.i4FsofcIfIndex = (INT4) (u4PortNum);
            pOfcFsofcIfEntry = RBTreeGet (gOfcGlobals.OfcGlbMib.FsofcIfTable,
                                          (tRBElem *) & (lOfcFsofcIfEntry));
            if (pOfcFsofcIfEntry == NULL)
            {
                OFC_TRC_FUNC ((OFC_PKT_TRC,
                               "No Entry Present in FsofcIfTable for  Port : %d\n ",
                               u4PortNum));
                return OSIX_FAILURE;
            }
            pOfcFsofcIfEntry->u4Config =
                pOfcFsofcIfEntry->u4Config | OFPPC_PORT_DOWN;
            pOfcFsofcIfEntry->MibObject.i4FsofcIfOperStatus =
                (INT4) u1OperStatus;

            OfpPortSt.PhyPort.u4Config = pOfcFsofcIfEntry->u4Config;

            /*Sending Port Status Message to the controller */
            OfcPortStatusPktSend (&OfpPortSt, NULL);
        }
    }

    if ((u4Action & OFPPC_NO_RECV) == OFPPC_NO_RECV)    /*OFPPC_NO_RECV bit set */
    {
        MEMSET (&lOfcFsofcIfEntry.MibObject, OFC_ZERO,
                sizeof (lOfcFsofcIfEntry.MibObject));
        lOfcFsofcIfEntry.MibObject.u4FsofcContextId =
            (INT4) OFC_DEFAULT_CONTEXT;
        lOfcFsofcIfEntry.MibObject.i4FsofcIfIndex = (INT4) (u4PortNum);
        pOfcFsofcIfEntry = RBTreeGet (gOfcGlobals.OfcGlbMib.FsofcIfTable,
                                      (tRBElem *) & (lOfcFsofcIfEntry));
        if (pOfcFsofcIfEntry == NULL)
        {
            OFC_TRC_FUNC ((OFC_PKT_TRC,
                           "No Entry Present in FsofcIfTable for  Port : %d\n ",
                           u4PortNum));
            return OSIX_FAILURE;

        }
        pOfcFsofcIfEntry->u4Config =
            (pOfcFsofcIfEntry->u4Config | OFPPC_NO_RECV);

    }

    if ((u4Action & OFPPC_NO_FWD) == OFPPC_NO_FWD)    /*OFPPC_NO_FWD bit set */
    {
        MEMSET (&lOfcFsofcIfEntry.MibObject, OFC_ZERO,
                sizeof (lOfcFsofcIfEntry.MibObject));
        lOfcFsofcIfEntry.MibObject.u4FsofcContextId =
            (INT4) OFC_DEFAULT_CONTEXT;
        lOfcFsofcIfEntry.MibObject.i4FsofcIfIndex = (INT4) (u4PortNum);
        pOfcFsofcIfEntry = RBTreeGet (gOfcGlobals.OfcGlbMib.FsofcIfTable,
                                      (tRBElem *) & (lOfcFsofcIfEntry));
        if (pOfcFsofcIfEntry == NULL)
        {
            OFC_TRC_FUNC ((OFC_PKT_TRC,
                           "No Entry Present in FsofcIfTable for  Port : %d\n ",
                           u4PortNum));
            return OSIX_FAILURE;
        }
        pOfcFsofcIfEntry->u4Config =
            (pOfcFsofcIfEntry->u4Config | OFPPC_NO_FWD);
    }

    if ((u4Action & OFPPC_NO_PACKET_IN) == OFPPC_NO_PACKET_IN)    /*OFPPC_NO_PACKET_IN bit set */
    {
        MEMSET (&lOfcFsofcIfEntry.MibObject, OFC_ZERO,
                sizeof (lOfcFsofcIfEntry.MibObject));
        lOfcFsofcIfEntry.MibObject.u4FsofcContextId =
            (INT4) OFC_DEFAULT_CONTEXT;
        lOfcFsofcIfEntry.MibObject.i4FsofcIfIndex = (INT4) (u4PortNum);
        pOfcFsofcIfEntry = RBTreeGet (gOfcGlobals.OfcGlbMib.FsofcIfTable,
                                      (tRBElem *) & (lOfcFsofcIfEntry));
        if (pOfcFsofcIfEntry == NULL)
        {
            OFC_TRC_FUNC ((OFC_PKT_TRC,
                           "No Entry Present in FsofcIfTable for  Port : %d\n ",
                           u4PortNum));
            return OSIX_FAILURE;
        }
        pOfcFsofcIfEntry->u4Config =
            (pOfcFsofcIfEntry->u4Config | OFPPC_NO_PACKET_IN);
    }

    UNUSED_PARAM (pu4Error);
    UNUSED_PARAM (pConnPtr);

    OFC_TRC_FUNC ((OFC_FN_EXIT, "FUNC:OfcPortModu4ConfigAction"));
    return OSIX_SUCCESS;

}

/****************************************************************************
 * Function    :  OfcPortModu4MaskAction
 * Description :  This function is for u4Mask Action  
 * Input       :  pOfpPortMod Pointer to the header and pConnPtr Conn Ptr
 * Output      :  pu4Error - Error on failure 
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE 
 ****************************************************************************/
INT4
OfcPortModu4MaskAction (tOfpPortMod * pOfpPortMod, UINT4 *pu4Error,
                        VOID *pConnPtr)
{

    INT4                i4RetVal = OSIX_SUCCESS;
    OFC_TRC_FUNC ((OFC_FN_ENTRY, "FUNC:OfcPortModu4MaskAction"));
    UNUSED_PARAM (pu4Error);
    UNUSED_PARAM (pConnPtr);
    UNUSED_PARAM (pOfpPortMod);
    OFC_TRC_FUNC ((OFC_FN_EXIT, "FUNC:OfcPortModu4MaskAction"));
    return i4RetVal;
}

/****************************************************************************
 * Function    :  OfcPortModu4AdvertiseAction
 * Description :  This function is for u4Advertise Action  
 * Input       :  pOfpPortMod Pointer to the header and pConnPtr Conn Ptr
 * Output      :  pu4Error - Error on failure 
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE 
 ****************************************************************************/
INT4
OfcPortModu4AdvertiseAction (tOfpPortMod * pOfpPortMod, UINT4 *pu4Error,
                             VOID *pConnPtr)
{

    INT4                i4RetVal = OSIX_SUCCESS;
    OFC_TRC_FUNC ((OFC_FN_ENTRY, "FUNC:OfcPortModu4AdvertiseAction"));
    UNUSED_PARAM (pu4Error);
    UNUSED_PARAM (pConnPtr);
    UNUSED_PARAM (pOfpPortMod);
    OFC_TRC_FUNC ((OFC_FN_EXIT, "FUNC:OfcPortModu4AdvertiseAction"));
    return i4RetVal;
}

/****************************************************************************
 * Function    :  OfcPortModau1Pad3Action
 * Description :  This function is for au1Pad3 Action  
 * Input       :  pOfpPortMod Pointer to the header and pConnPtr Conn Ptr
 * Output      :  pu4Error - Error on failure 
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE 
 ****************************************************************************/
INT4
OfcPortModau1Pad3Action (tOfpPortMod * pOfpPortMod, UINT4 *pu4Error,
                         VOID *pConnPtr)
{

    INT4                i4RetVal = OSIX_SUCCESS;
    OFC_TRC_FUNC ((OFC_FN_ENTRY, "FUNC:OfcPortModau1Pad3Action"));
    UNUSED_PARAM (pu4Error);
    UNUSED_PARAM (pConnPtr);
    UNUSED_PARAM (pOfpPortMod);
    OFC_TRC_FUNC ((OFC_FN_EXIT, "FUNC:OfcPortModau1Pad3Action"));
    return i4RetVal;
}

/****************************************************************************
 * Function    :  OfcStatsRequ2TypeAction
 * Description :  This function is for u2Type Action  
 * Input       :  pOfpStatsReq Pointer to the header and pConnPtr Conn Ptr
 * Output      :  pu4Error - Error on failure 
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE 
 ****************************************************************************/
INT4
OfcStatsRequ2TypeAction (tOfpStatsReq * pOfpStatsReq, UINT4 *pu4Error,
                         VOID *pConnPtr)
{
    tOfcFsofcControllerConnEntry *pLocal = pConnPtr;
    tOfpStatsRply      *pOfpStatsRply = (tOfpStatsRply *) pOfpStatsReq;
    tOfpDescStats      *pOfpDescStats = NULL;
    tOfcActOutPort     *pActionPort = NULL;
    tOfpFlowStatsReq   *pOfpFlowStatsReq = NULL;
    tOfpFlowStatsRply  *pOfpFlowStatsRply = NULL;
    tOfcFlowEntry      *pFlow = NULL;

    UINT4               u4HashIndex = OFC_ZERO;
    UINT4               u4ActionCount = OFC_ZERO;
    UINT4               u4Seconds = OFC_ZERO;
    INT4                i4RetVal = OSIX_SUCCESS;

    tOfcFlowMatch       Key;
    tOfpMatch           Match;

    tOfpPortStats      *pOfpPortStats = NULL;
    tOfpAggStatsRply   *pOfpAggStatsRply = NULL;
    tOfpPortStatsReq    OfpPortStatsReq;
    tOfpPortStats       OfpPortStats;
    tIfCountersStruct   IfCounter;

    tOfcFsofcIfEntry   *pOfcFsofcIfEntry = NULL;
    tOfcFsofcCfgEntry  *pOfcFsofcCfgEntry = NULL;

    UNUSED_PARAM (pu4Error);
    OFC_TRC_FUNC ((OFC_FN_ENTRY, "FUNC:OfcStatsRequ2TypeAction"));

    pOfcFsofcCfgEntry =
        OfcGetFsofcCfgEntry (pLocal->MibObject.u4FsofcContextId);
    if (pOfcFsofcCfgEntry == NULL)
    {
        OFC_TRC_FUNC ((OFC_PKT_TRC, "\r\nOfcStatsRequ2TypeAction: "
                       "Get CFG Entry Failed\n"));
        return OSIX_FAILURE;
    }

    switch (pOfpStatsRply->u2Type)
    {
        case OFPST_DESC:
            pOfpDescStats =
                (tOfpDescStats *) MemAllocMemBlk (OFC_DESC_STATS_POOLID);
            if (NULL == pOfpDescStats)
            {
                OFC_TRC_FUNC ((OFC_PKT_TRC,
                               "\r\nMemory Allocation failed for pOfpDescStats\n"));
                return OSIX_FAILURE;
            }
            STRCPY (pOfpDescStats->au1MfrDesc, "Aricent");
            STRCPY (pOfpDescStats->au1HwDesc, "PC Linux");
            STRCPY (pOfpDescStats->au1SwDesc, "ISS");
            STRCPY (pOfpDescStats->au1SerialNum, "1.0");
            STRCPY (pOfpDescStats->au1DpDesc, "Aricent OFC");
            pLocal->u4TxDataLength = sizeof (tOfpDescStats);
            MEMCPY (pOfpStatsRply->au1Body, pOfpDescStats,
                    sizeof (tOfpDescStats));
            MemReleaseMemBlock (OFC_DESC_STATS_POOLID, (UINT1 *) pOfpDescStats);
            break;

        case OFPST_FLOW:
            u4Seconds = OsixGetSysUpTime ();
            pOfpFlowStatsReq =
                (tOfpFlowStatsReq *) (VOID *) pOfpStatsReq->au1Body;
            pOfpFlowStatsReq->u2OutPort =
                OSIX_NTOHS (pOfpFlowStatsReq->u2OutPort);
            pOfpFlowStatsReq->Match.u4WildCard =
                OSIX_NTOHL (pOfpFlowStatsReq->Match.u4WildCard);
            pLocal->u4TxDataLength = OFC_ZERO;
            if (OFC_WILDCARD_MATCH == pOfpFlowStatsReq->Match.u4WildCard)
            {
                pOfpFlowStatsRply =
                    (tOfpFlowStatsRply *) (VOID *) pOfpStatsReq->au1Body;
                TMO_HASH_Scan_Table (pOfcFsofcCfgEntry->pFlowtable, u4HashIndex)
                {
                    TMO_HASH_Scan_Bucket (pOfcFsofcCfgEntry->pFlowtable,
                                          u4HashIndex, pFlow, tOfcFlowEntry *)
                    {
                        MEMSET (pOfpFlowStatsRply, OFC_ZERO,
                                sizeof (tOfpFlowStatsRply));
                        OfcKeyToFlowMatch (&pFlow->key,
                                           &pOfpFlowStatsRply->Match);
                        pLocal->u4TxDataLength += sizeof (tOfpFlowStatsRply);
                        pOfpFlowStatsRply->u2Length =
                            OSIX_HTONS (sizeof (tOfpFlowStatsRply));
                        pOfpFlowStatsRply->u1TableId = pFlow->u1TableId;
                        pOfpFlowStatsRply->u4DurationSec =
                            OSIX_HTONL ((u4Seconds - pFlow->u4Used));
                        pOfpFlowStatsRply->u4DurationNSec = OFC_ZERO;
                        pOfpFlowStatsRply->u2Priority =
                            OSIX_HTONS (pFlow->u2Priority);
                        pOfpFlowStatsRply->u2IdleTimeout =
                            OSIX_HTONS (pFlow->u2IdleTimeout);
                        pOfpFlowStatsRply->u2HardTimeout =
                            OSIX_HTONS (pFlow->u2HardTimeout);
                        pOfpFlowStatsRply->u8Cookie = pFlow->u8Cookie;
                        FSAP_U8_ASSIGN_LO (&pOfpFlowStatsRply->u8PktCount,
                                           OSIX_HTONL (pFlow->u4PktCount));
                        FSAP_U8_ASSIGN_LO (&pOfpFlowStatsRply->u8ByteCount,
                                           OSIX_HTONL (pFlow->u4ByteCount));
                        pActionPort =
                            (tOfcActOutPort *) & (pOfpFlowStatsRply->
                                                  Actions[0]);
                        MEMSET (pActionPort, OFC_ZERO, sizeof (tOfcActOutPort));
                        for (u4ActionCount = OFC_ZERO;
                             u4ActionCount < pFlow->u4NumActs; u4ActionCount++)
                        {
                            switch (pFlow->OfcActs[u4ActionCount].ActOutport.
                                    u2Type)
                            {
                                case OFPAT_OUTPUT:
                                    pActionPort->u2Type =
                                        OSIX_HTONS (pFlow->
                                                    OfcActs[u4ActionCount].
                                                    ActOutport.u2Type);
                                    pActionPort->u2Len =
                                        OSIX_HTONS (pFlow->
                                                    OfcActs[u4ActionCount].
                                                    ActOutport.u2Len);
                                    pActionPort->u2OutPort =
                                        OSIX_HTONS (pFlow->
                                                    OfcActs[u4ActionCount].
                                                    ActOutport.u2OutPort);
                                    pActionPort->u2MaxLen =
                                        OSIX_HTONS (pFlow->
                                                    OfcActs[u4ActionCount].
                                                    ActOutport.u2MaxLen);
                                    break;

                                default:
                                    break;
                            }
                        }
                        pOfpFlowStatsRply++;
                    }
                }
            }
            else
            {
                MEMSET (&Key, OFC_ZERO, sizeof (Key));
                MEMSET (&Match, OFC_ZERO, sizeof (Match));
                MEMCPY (&Match, &pOfpFlowStatsReq->Match, sizeof (tOfpMatch));
                OfcFlowMatchToKey (&Match, &Key);
                pOfpFlowStatsRply =
                    (tOfpFlowStatsRply *) (VOID *) pOfpStatsReq->au1Body;
                MEMCPY (&pOfpFlowStatsRply->Match, &Match, sizeof (tOfpMatch));
                pOfpFlowStatsRply->u1TableId = pOfpFlowStatsReq->u1TableId;
                pFlow = NULL;
                pActionPort = NULL;
                u4ActionCount = OFC_ZERO;

                pFlow =
                    (tOfcFlowEntry *) OfcFlowLookup (pOfcFsofcCfgEntry, &Key);
                /* Execute the actions specified in the pFlow */
                if (pFlow != NULL)
                {
                    pLocal->u4TxDataLength += sizeof (tOfpFlowStatsRply);

                    pOfpFlowStatsRply->u2Length =
                        OSIX_HTONS (sizeof (tOfpFlowStatsRply));
                    pOfpFlowStatsRply->u4DurationSec =
                        OSIX_HTONL ((u4Seconds - pFlow->u4Used));
                    pOfpFlowStatsRply->u4DurationNSec = OFC_ZERO;
                    pOfpFlowStatsRply->u2Priority =
                        OSIX_HTONS (pFlow->u2Priority);
                    pOfpFlowStatsRply->u2IdleTimeout =
                        OSIX_HTONS (pFlow->u2IdleTimeout);
                    pOfpFlowStatsRply->u2HardTimeout =
                        OSIX_HTONS (pFlow->u2HardTimeout);
                    pOfpFlowStatsRply->u8Cookie = pFlow->u8Cookie;
                    FSAP_U8_ASSIGN_LO (&pOfpFlowStatsRply->u8PktCount,
                                       OSIX_HTONL (pFlow->u4PktCount));
                    FSAP_U8_ASSIGN_LO (&pOfpFlowStatsRply->u8ByteCount,
                                       OSIX_HTONL (pFlow->u4ByteCount));
                    pActionPort =
                        (tOfcActOutPort *) & (pOfpFlowStatsRply->Actions[0]);
                    MEMSET (pActionPort, OFC_ZERO, sizeof (tOfcActOutPort));
                    for (u4ActionCount = OFC_ZERO;
                         u4ActionCount < pFlow->u4NumActs; u4ActionCount++)
                    {
                        switch (pFlow->OfcActs[u4ActionCount].ActOutport.u2Type)
                        {
                            case OFCAT_OUTPUT:
                                pActionPort->u2Type =
                                    OSIX_HTONS (pFlow->OfcActs[u4ActionCount].
                                                ActOutport.u2Type);
                                pActionPort->u2Len =
                                    OSIX_HTONS (pFlow->OfcActs[u4ActionCount].
                                                ActOutport.u2Len);
                                pActionPort->u2OutPort =
                                    OSIX_HTONS (pFlow->OfcActs[u4ActionCount].
                                                ActOutport.u2OutPort);
                                pActionPort->u2MaxLen =
                                    OSIX_HTONS (pFlow->OfcActs[u4ActionCount].
                                                ActOutport.u2MaxLen);

                                break;

                            default:
                                break;
                        }
                    }
                }
            }
            break;

        case OFPST_AGGREGATE:
            pOfpAggStatsRply =
                (tOfpAggStatsRply *) (VOID *) pOfpStatsReq->au1Body;
            MEMSET (pOfpAggStatsRply, OFC_ZERO, sizeof (tOfpAggStatsRply));
            pLocal->u4TxDataLength = sizeof (tOfpAggStatsRply);
            break;

        case OFPST_TABLE:
            break;

        case OFPST_PORT:
            MEMCPY (&OfpPortStatsReq, pOfpStatsReq->au1Body,
                    sizeof (tOfpPortStatsReq));
            OfpPortStatsReq.u2PortNo = OSIX_NTOHS (OfpPortStatsReq.u2PortNo);
            pOfpPortStats = (tOfpPortStats *) (VOID *) pOfpStatsRply->au1Body;
            if (OfpPortStatsReq.u2PortNo == OFC_INVALID_INDEX)
            {
                pOfcFsofcIfEntry = NULL;
                pLocal->u4TxDataLength = OFC_ZERO;
                pOfcFsofcIfEntry = OfcGetFirstFsofcIfTable ();
                while (pOfcFsofcIfEntry != NULL)
                {
                    MEMSET (&OfpPortStats, OFC_ZERO, sizeof (tOfpPortStats));
                    MEMSET (&IfCounter, OFC_ZERO, sizeof (tIfCountersStruct));
                    CfaGetIfCounters ((UINT4)
                                      (pOfcFsofcIfEntry->MibObject.
                                       i4FsofcIfIndex), &IfCounter);
                    FSAP_U8_ASSIGN_LO (&OfpPortStats.u8RxPkts,
                                       OSIX_HTONL ((IfCounter.u4InUcastPkts +
                                                    IfCounter.
                                                    u4InMulticastPkts +
                                                    IfCounter.
                                                    u4InBroadcastPkts)));
                    FSAP_U8_ASSIGN_LO (&OfpPortStats.u8TxPkts,
                                       OSIX_HTONL ((IfCounter.u4OutUcastPkts +
                                                    IfCounter.
                                                    u4OutMulticastPkts +
                                                    IfCounter.
                                                    u4OutBroadcastPkts)));
                    FSAP_U8_ASSIGN_LO (&OfpPortStats.u8RxBytes,
                                       OSIX_HTONL (IfCounter.u4InOctets));
                    FSAP_U8_ASSIGN_LO (&OfpPortStats.u8TxBytes,
                                       OSIX_HTONL (IfCounter.u4OutOctets));
                    FSAP_U8_ASSIGN_LO (&OfpPortStats.u8RxErrors,
                                       OSIX_HTONL (IfCounter.u4InErrors));
                    FSAP_U8_ASSIGN_LO (&OfpPortStats.u8TxErrors,
                                       OSIX_HTONL (IfCounter.u4OutErrors));
                    FSAP_U8_ASSIGN_LO (&OfpPortStats.u8RxDropped,
                                       OSIX_HTONL (IfCounter.u4InDiscards));
                    FSAP_U8_ASSIGN_LO (&OfpPortStats.u8TxDropped,
                                       OSIX_HTONL (IfCounter.u4OutDiscards));
                    OfpPortStats.u2PortNo =
                        OSIX_HTONS (pOfcFsofcIfEntry->MibObject.i4FsofcIfIndex);
                    if ((pLocal->u4TxDataLength + sizeof (tOfpPortStats)) <
                        OFC_MAX_CONTROLLER_PKT_LEN)
                    {
                        MEMCPY (pOfpPortStats, &OfpPortStats,
                                sizeof (tOfpPortStats));
                        pOfpPortStats++;
                        pLocal->u4TxDataLength =
                            (UINT4) (pLocal->u4TxDataLength +
                                     (UINT4) sizeof (tOfpPortStats));
                        pOfcFsofcIfEntry =
                            OfcGetNextFsofcIfTable (pOfcFsofcIfEntry);
                    }

                }
            }
            else
            {
                MEMSET (&OfpPortStats, OFC_ZERO, sizeof (tOfpPortStats));
                MEMSET (&IfCounter, OFC_ZERO, sizeof (tIfCountersStruct));
                CfaGetIfCounters (OfpPortStatsReq.u2PortNo, &IfCounter);
                FSAP_U8_ASSIGN_LO (&OfpPortStats.u8RxPkts,
                                   OSIX_HTONL ((IfCounter.u4InUcastPkts +
                                                IfCounter.u4InMulticastPkts +
                                                IfCounter.u4InBroadcastPkts)));
                FSAP_U8_ASSIGN_LO (&OfpPortStats.u8TxPkts,
                                   OSIX_HTONL ((IfCounter.u4OutUcastPkts +
                                                IfCounter.u4OutMulticastPkts +
                                                IfCounter.u4OutBroadcastPkts)));
                FSAP_U8_ASSIGN_LO (&OfpPortStats.u8RxBytes,
                                   OSIX_HTONL (IfCounter.u4InOctets));
                FSAP_U8_ASSIGN_LO (&OfpPortStats.u8TxBytes,
                                   OSIX_HTONL (IfCounter.u4OutOctets));
                FSAP_U8_ASSIGN_LO (&OfpPortStats.u8RxErrors,
                                   OSIX_HTONL (IfCounter.u4InErrors));
                FSAP_U8_ASSIGN_LO (&OfpPortStats.u8TxErrors,
                                   OSIX_HTONL (IfCounter.u4OutErrors));
                FSAP_U8_ASSIGN_LO (&OfpPortStats.u8RxDropped,
                                   OSIX_HTONL (IfCounter.u4InDiscards));
                FSAP_U8_ASSIGN_LO (&OfpPortStats.u8TxDropped,
                                   OSIX_HTONL (IfCounter.u4OutDiscards));
                OfpPortStats.u2PortNo = OSIX_HTONS (OfpPortStatsReq.u2PortNo);
                MEMCPY (pOfpStatsRply->au1Body, &OfpPortStats,
                        sizeof (tOfpPortStats));
                pLocal->u4TxDataLength = sizeof (tOfpPortStats);
            }
            break;

        case OFPST_QUEUE:
            break;

        case OFPST_VENDOR:
            break;

        default:
            break;
    }
    i4RetVal = OfcStatsRplyPktSend (pOfpStatsRply, pConnPtr);
    OFC_TRC_FUNC ((OFC_FN_EXIT, "FUNC:OfcStatsRequ2TypeAction"));

    return i4RetVal;
}

/****************************************************************************
 * Function    :  OfcStatsRequ2FlagsAction
 * Description :  This function is for u2Flags Action  
 * Input       :  pOfpStatsReq Pointer to the header and pConnPtr Conn Ptr
 * Output      :  pu4Error - Error on failure 
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE 
 ****************************************************************************/
INT4
OfcStatsRequ2FlagsAction (tOfpStatsReq * pOfpStatsReq, UINT4 *pu4Error,
                          VOID *pConnPtr)
{

    INT4                i4RetVal = OSIX_SUCCESS;
    OFC_TRC_FUNC ((OFC_FN_ENTRY, "FUNC:OfcStatsRequ2FlagsAction"));
    UNUSED_PARAM (pu4Error);
    UNUSED_PARAM (pConnPtr);
    UNUSED_PARAM (pOfpStatsReq);
    OFC_TRC_FUNC ((OFC_FN_EXIT, "FUNC:OfcStatsRequ2FlagsAction"));
    return i4RetVal;
}

/****************************************************************************
 * Function    :  OfcStatsReqau1BodyAction
 * Description :  This function is for au1Body Action  
 * Input       :  pOfpStatsReq Pointer to the header and pConnPtr Conn Ptr
 * Output      :  pu4Error - Error on failure 
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE 
 ****************************************************************************/
INT4
OfcStatsReqau1BodyAction (tOfpStatsReq * pOfpStatsReq, UINT4 *pu4Error,
                          VOID *pConnPtr)
{

    INT4                i4RetVal = OSIX_SUCCESS;
    OFC_TRC_FUNC ((OFC_FN_ENTRY, "FUNC:OfcStatsReqau1BodyAction"));
    UNUSED_PARAM (pu4Error);
    UNUSED_PARAM (pConnPtr);
    UNUSED_PARAM (pOfpStatsReq);
    OFC_TRC_FUNC ((OFC_FN_EXIT, "FUNC:OfcStatsReqau1BodyAction"));
    return i4RetVal;
}

/****************************************************************************
 * Function    :  OfcBarrierReqau1DummyAction
 * Description :  This function is for au1Dummy Action  
 * Input       :  pOfpBarrierReq Pointer to the header and pConnPtr Conn Ptr
 * Output      :  pu4Error - Error on failure 
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE 
 ****************************************************************************/
INT4
OfcBarrierReqau1DummyAction (tOfpBarrierReq * pOfpBarrierReq, UINT4 *pu4Error,
                             VOID *pConnPtr)
{

    INT4                i4RetVal = OSIX_SUCCESS;
    OFC_TRC_FUNC ((OFC_FN_ENTRY, "FUNC:OfcBarrierReqau1DummyAction"));
    OfcBarrierRplyPktSend (NULL, pConnPtr);
    UNUSED_PARAM (pu4Error);
    UNUSED_PARAM (pOfpBarrierReq);
    OFC_TRC_FUNC ((OFC_FN_EXIT, "FUNC:OfcBarrierReqau1DummyAction"));
    return i4RetVal;
}

/****************************************************************************
 * Function    :  OfcQueueRequ2PortAction
 * Description :  This function is for u2Port Action  
 * Input       :  pOfpQueueReq Pointer to the header and pConnPtr Conn Ptr
 * Output      :  pu4Error - Error on failure 
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE 
 ****************************************************************************/
INT4
OfcQueueRequ2PortAction (tOfpQueueReq * pOfpQueueReq, UINT4 *pu4Error,
                         VOID *pConnPtr)
{

    INT4                i4RetVal = OSIX_SUCCESS;
    OFC_TRC_FUNC ((OFC_FN_ENTRY, "FUNC:OfcQueueRequ2PortAction"));
    UNUSED_PARAM (pu4Error);
    UNUSED_PARAM (pConnPtr);
    UNUSED_PARAM (pOfpQueueReq);
    OFC_TRC_FUNC ((OFC_FN_EXIT, "FUNC:OfcQueueRequ2PortAction"));
    return i4RetVal;
}

/****************************************************************************
 * Function    :  OfcQueueReqau1PadAction
 * Description :  This function is for au1Pad Action  
 * Input       :  pOfpQueueReq Pointer to the header and pConnPtr Conn Ptr
 * Output      :  pu4Error - Error on failure 
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE 
 ****************************************************************************/
INT4
OfcQueueReqau1PadAction (tOfpQueueReq * pOfpQueueReq, UINT4 *pu4Error,
                         VOID *pConnPtr)
{

    INT4                i4RetVal = OSIX_SUCCESS;
    OFC_TRC_FUNC ((OFC_FN_ENTRY, "FUNC:OfcQueueReqau1PadAction"));
    *pu4Error = OFC_QUEUEREQ_U2PORT_VALIDATE_ERR;
    i4RetVal = OSIX_FAILURE;
    UNUSED_PARAM (pConnPtr);
    UNUSED_PARAM (pOfpQueueReq);
    OFC_TRC_FUNC ((OFC_FN_EXIT, "FUNC:OfcQueueReqau1PadAction"));
    return i4RetVal;
}

/****************************************************************************
 * Function    :  OfcPktErrorHandle
 * Description :  This function is for pkt parse error handle  
 * Input       :  pOfpPkt Pointer to the header, Error Code and pConnPtr Conn Ptr
 * Output      :  None 
 * Returns     :  None 
 ****************************************************************************/
VOID
OfcPktErrorHandle (tOfpPkt * pOfpPkt, UINT4 u4Error, VOID *pConnPtr)
{
    tOfpErrMsg          OfpErrorMsg;
    UNUSED_PARAM (pConnPtr);

    MEMSET (&OfpErrorMsg, 0, sizeof (tOfpErrMsg));
    switch (u4Error)
    {
        case OFC_OFP_U1VERSION_VALIDATE_ERR:
            if (pOfpPkt->u1Type == OFPT_HELLO)
            {
                OfpErrorMsg.u2Type = OFPET_HELLO_FAILED;
            }
            else
            {
                OfpErrorMsg.u2Type = OFPET_BAD_REQUEST;
            }

            OfpErrorMsg.u2Code = (UINT2) OFC_OFP_U1VERSION_VALIDATE_ERR;
            OfcErrMsgPktSend (&OfpErrorMsg, NULL);
            break;
        case OFC_OFP_U1TYPE_VALIDATE_ERR:
            OfpErrorMsg.u2Type = OFPET_BAD_REQUEST;
            OfpErrorMsg.u2Code = (UINT2) OFC_OFP_U1TYPE_VALIDATE_ERR;
            OfcErrMsgPktSend (&OfpErrorMsg, NULL);
            break;
        case OFC_OFP_U2LENGTH_VALIDATE_ERR:
            OfpErrorMsg.u2Type = OFPET_BAD_REQUEST;
            OfpErrorMsg.u2Code = (UINT2) OFC_OFP_U2LENGTH_VALIDATE_ERR;
            OfcErrMsgPktSend (&OfpErrorMsg, NULL);
            break;
        case OFC_EXPERIMENTERHDR_U4EXPERIMENTER_VALIDATE_ERR:
            OfpErrorMsg.u2Type = OFPET_BAD_REQUEST;
            OfpErrorMsg.u2Code = (UINT2) OFPBAC_BAD_VENDOR;
            OfcErrMsgPktSend (&OfpErrorMsg, NULL);
            break;
        case OFC_FLOWMOD_ACTIONS_VALIDATE_ERR:
            OfpErrorMsg.u2Type = OFPET_FLOW_MOD_FAILED;
            OfpErrorMsg.u2Code = (UINT2) OFPFMFC_UNSUPPORTED;
            OfcErrMsgPktSend (&OfpErrorMsg, NULL);
            break;
        case OFC_QUEUEREQ_U2PORT_VALIDATE_ERR:
            OfpErrorMsg.u2Type = OFPET_BAD_REQUEST;
            OfpErrorMsg.u2Code = (UINT2) OFPBRC_BAD_STAT;
            OfcErrMsgPktSend (&OfpErrorMsg, NULL);
            break;
        default:
            break;
    }
}

/****************************************************************************
 * Function    :  OfcOfpPktSend
 * Description :  This function is for pkt send routine  
 * Input       :  pOfpPkt Pointer to the header and pConnPtr Conn Ptr
 * Output      :  None 
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE 
 ****************************************************************************/
INT4
OfcOfpPktSend (tOfpPkt * pOfpPkt, VOID *pConnPtr)
{
    INT4                i4Return = OSIX_SUCCESS;
    UINT4               u4Len = pOfpPkt->u2Length;
    tOfcFsofcControllerConnEntry *pLocal = NULL;

    pOfpPkt->u2Length = OSIX_HTONS (pOfpPkt->u2Length);
    if (pConnPtr != NULL)
    {
        pOfpPkt->u4Xid = OSIX_HTONL (pOfpPkt->u4Xid);
        i4Return = OfcPktSend ((UINT1 *) pOfpPkt, u4Len, pConnPtr);
    }
    else
    {
        pLocal = OfcGetFirstFsofcControllerConnTable ();
        while (pLocal != NULL)
        {
            pOfpPkt->u4Xid = OSIX_HTONL (((pLocal->u4TxXid) + OFC_ONE));
            i4Return = OfcPktSend ((UINT1 *) pOfpPkt, u4Len, pLocal);
            if (i4Return == OSIX_FAILURE)
            {
                return OSIX_FAILURE;
            }
            pLocal = OfcGetNextFsofcControllerConnTable (pLocal);
        }
    }

    return i4Return;
}

/****************************************************************************
 * Function    :  OfcHelloPktSend
 * Description :  This function is for pkt send routine  
 * Input       :  pOfpHello Pointer to the header and pConnPtr Conn Ptr
 * Output      :  None 
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE 
 ****************************************************************************/
INT4
OfcHelloPktSend (tOfpHello * pOfpHello, VOID *pConnPtr)
{
    INT4                i4Return = OSIX_SUCCESS;
    tOfpPkt             Ofp;
    MEMSET (&Ofp, OFC_ZERO, sizeof (tOfpPkt));
    /* We do not initiate Hello so this pointer shoud not be NULL */
    if (pConnPtr == NULL)
    {
        return OSIX_FAILURE;
    }
    UNUSED_PARAM (pOfpHello);
    Ofp.u1Version = OFP_VERSION;
    Ofp.u1Type = OFPT_HELLO;
    Ofp.u2Length = OFP_HEADER_LEN;
    Ofp.u4Xid = ((tOfcFsofcControllerConnEntry *) pConnPtr)->u4RxXid;
    i4Return = OfcOfpPktSend (&Ofp, pConnPtr);
    return i4Return;
}

/****************************************************************************
 * Function    :  OfcErrMsgPktSend
 * Description :  This function is for pkt send routine  
 * Input       :  pOfpErrMsg Pointer to the header and pConnPtr Conn Ptr
 * Output      :  None 
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE 
 ****************************************************************************/
INT4
OfcErrMsgPktSend (tOfpErrMsg * pOfpErrMsg, VOID *pConnPtr)
{
    INT4                i4Return = OSIX_SUCCESS;
    tOfpPkt            *pOfp = (tOfpPkt *) (VOID *) au1TxData;
    tOfcFsofcControllerConnEntry *pLocal = pConnPtr;
    tOfcFsofcCfgEntry  *pOfcFsofcCfgEntry = NULL;

    UINT4               u4Len = STRLEN (pOfpErrMsg->au1Data);

    pOfcFsofcCfgEntry = (tOfcFsofcCfgEntry *)
        RBTreeGetFirst (gOfcGlobals.OfcGlbMib.FsofcCfgTable);

    if (pOfcFsofcCfgEntry == NULL)
    {
        OFC131_TRC_FUNC ((OFC_PKT_TRC, "\r\nOfc131PktFeatureReqau1DummyAction "
                          "Get CFG Entry Failed\n"));
        return OSIX_FAILURE;
    }

    pOfpErrMsg->u2Type = OSIX_HTONS (pOfpErrMsg->u2Type);
    pOfpErrMsg->u2Code = OSIX_HTONS (pOfpErrMsg->u2Code);
    if (pOfcFsofcCfgEntry->MibObject.i4FsofcSupportedVersion ==
        OFC_VERSION_V131)
    {
        pOfp->u1Version = OFC_FOUR;
    }
    else if (pOfcFsofcCfgEntry->MibObject.i4FsofcSupportedVersion ==
             OFC_VERSION_V100)
    {
        pOfp->u1Version = OFC_ONE;
    }
    pOfp->u1Type = OFPT_ERROR;
    pOfp->u2Length =
        (UINT2) ((UINT2) u4Len + (UINT2) OFP_HEADER_LEN + (UINT2) OFC_FOUR);
    if (pLocal != NULL)
    {
        pOfp->u4Xid = pLocal->u4RxXid;
    }
    MEMCPY (&pOfp->hl.ErrMsg, pOfpErrMsg, u4Len + OFC_FOUR);
    i4Return = OfcOfpPktSend (pOfp, pConnPtr);
    return i4Return;
}

/****************************************************************************
 * Function    :  OfcEchoReqMsgPktSend
 * Description :  This function is for pkt send routine  
 * Input       :  pOfpEchoReqMsg Pointer to the header and pConnPtr Conn Ptr
 * Output      :  None 
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE 
 ****************************************************************************/
INT4
OfcEchoReqMsgPktSend (tOfpEchoReqMsg * pOfpEchoReqMsg, VOID *pConnPtr)
{
    INT4                i4Return = OSIX_SUCCESS;
    tOfcFsofcControllerConnEntry *pLocal = pConnPtr;
    tOfpPkt            *pOfp = (tOfpPkt *) (VOID *) au1TxData;

    if (pLocal == NULL)
    {
        /* You can not send to Echo Req to all cotnroller at a time */
        return OSIX_FAILURE;
    }
    pOfp->u1Version = OFP_VERSION;
    pOfp->u1Type = OFPT_ECHO_REQUEST;
    pOfp->u2Length =
        (UINT2) ((UINT2) (pLocal->u4TxDataLength) + (UINT2) OFP_HEADER_LEN);
    pLocal->u4TxXid++;
    pOfp->u4Xid = pLocal->u4TxXid;
    MEMCPY (pOfp->hl.EchoReqMsg.au1Data, pOfpEchoReqMsg->au1Data,
            pLocal->u4TxDataLength);
    i4Return = OfcOfpPktSend (pOfp, pConnPtr);
    return i4Return;
}

/****************************************************************************
 * Function    :  OfcEchoRplyMsgPktSend
 * Description :  This function is for pkt send routine  
 * Input       :  pOfpEchoRplyMsg Pointer to the header and pConnPtr Conn Ptr
 * Output      :  None 
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE 
 ****************************************************************************/
INT4
OfcEchoRplyMsgPktSend (tOfpEchoRplyMsg * pOfpEchoRplyMsg, VOID *pConnPtr)
{
    INT4                i4Return = OSIX_SUCCESS;
    tOfcFsofcControllerConnEntry *pLocal = pConnPtr;
    tOfpPkt            *pOfp = (tOfpPkt *) (VOID *) au1TxData;
    pOfp->u1Version = OFP_VERSION;
    pOfp->u1Type = OFPT_ECHO_REPLY;
    pOfp->u2Length = (UINT2) (pLocal->u4RxLength);
    pOfp->u4Xid = pLocal->u4RxXid;
    MEMCPY (pOfp->hl.EchoRplyMsg.au1Data, pOfpEchoRplyMsg->au1Data,
            (pLocal->u4RxLength - OFP_HEADER_LEN));
    i4Return = OfcOfpPktSend (pOfp, pConnPtr);
    return i4Return;
}

/****************************************************************************
 * Function    :  OfcFeatureRplyPktSend
 * Description :  This function is for pkt send routine  
 * Input       :  pOfpFeatureRply Pointer to the header and pConnPtr Conn Ptr
 * Output      :  None 
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE 
 ****************************************************************************/
INT4
OfcFeatureRplyPktSend (tOfpFeatureRply * pOfpFeatureRply, VOID *pConnPtr)
{
    INT4                i4Return = OSIX_SUCCESS;
    tOfpPkt            *pOfp = (tOfpPkt *) (VOID *) au1TxData;
    tOfcFsofcControllerConnEntry *pLocal = pConnPtr;

    pOfpFeatureRply->u4NoOfBuf = OSIX_HTONL (pOfpFeatureRply->u4NoOfBuf);
    pOfpFeatureRply->u4Capabilities =
        OSIX_HTONL (pOfpFeatureRply->u4Capabilities);
    pOfpFeatureRply->u4Actions = OSIX_HTONL (pOfpFeatureRply->u4Actions);

    pOfp->u1Version = OFP_VERSION;
    pOfp->u1Type = OFPT_FEATURES_REPLY;
    pOfp->u2Length =
        (UINT2) ((UINT2) (pLocal->u4TxDataLength) + (UINT2) OFP_HEADER_LEN +
                 (UINT2) OFC_TWENTY_FOUR);
    pOfp->u4Xid = pLocal->u4RxXid;
    MEMCPY (&pOfp->hl.FeatureRply, pOfpFeatureRply,
            pLocal->u4TxDataLength + OFC_TWENTY_FOUR);
    i4Return = OfcOfpPktSend (pOfp, pConnPtr);
    return i4Return;
}

/****************************************************************************
 * Function    :  OfcConfigRplyPktSend
 * Description :  This function is for pkt send routine  
 * Input       :  pOfpConfigRply Pointer to the header and pConnPtr Conn Ptr
 * Output      :  None 
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE 
 ****************************************************************************/
INT4
OfcConfigRplyPktSend (tOfpConfigRply * pOfpConfigRply, VOID *pConnPtr)
{
    INT4                i4Return = OSIX_SUCCESS;
    tOfpPkt             Ofp;
    tOfcFsofcControllerConnEntry *pLocal = pConnPtr;
    MEMSET (&Ofp, OFC_ZERO, sizeof (tOfpPkt));
    pOfpConfigRply->u2Flags = OSIX_HTONS (pOfpConfigRply->u2Flags);
    pOfpConfigRply->u2MissSendLen = OSIX_HTONS (pOfpConfigRply->u2MissSendLen);

    Ofp.hl.ConfigRply = *pOfpConfigRply;
    Ofp.u1Version = OFP_VERSION;
    Ofp.u1Type = OFPT_GET_CONFIG_REPLY;
    Ofp.u2Length = OFP_HEADER_LEN + OFC_FOUR;
    Ofp.u4Xid = pLocal->u4RxXid;
    i4Return = OfcOfpPktSend (&Ofp, pConnPtr);
    return i4Return;
}

/****************************************************************************
 * Function    :  OfcPktInPktSend
 * Description :  This function is for pkt send routine  
 * Input       :  pOfpPktIn Pointer to the header and pConnPtr Conn Ptr
 * Output      :  None 
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE 
 ****************************************************************************/
INT4
OfcPktInPktSend (tOfpPktIn * pOfpPktIn, VOID *pConnPtr)
{
    INT4                i4Return = OSIX_SUCCESS;
    tOfpPkt            *pOfp = (tOfpPkt *) (VOID *) au1TxData;
    tOfcFsofcControllerConnEntry *pLocal = pConnPtr;
    UINT2               u2FrameLen = pOfpPktIn->u2TotLen;
    pOfpPktIn->u4BufId = OSIX_HTONL (pOfpPktIn->u4BufId);
    pOfpPktIn->u2TotLen = OSIX_HTONS (pOfpPktIn->u2TotLen);
    pOfpPktIn->u2InPort = OSIX_HTONS (pOfpPktIn->u2InPort);

    pOfp->u1Version = OFP_VERSION;
    pOfp->u1Type = OFPT_PACKET_IN;
    pOfp->u2Length = (UINT2) (u2FrameLen + OFP_HEADER_LEN + OFC_TEN);
    if (pLocal != NULL)
    {
        pOfp->u4Xid = (pLocal->u4TxXid) + OFC_ONE;
    }
    MEMCPY (&pOfp->hl.PktIn, pOfpPktIn, u2FrameLen + OFC_TEN);
    i4Return = OfcOfpPktSend (pOfp, pConnPtr);
    return i4Return;
}

/****************************************************************************
 * Function    :  OfcFlowRemPktSend
 * Description :  This function is for pkt send routine  
 * Input       :  pOfpFlowRem Pointer to the header and pConnPtr Conn Ptr
 * Output      :  None 
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE 
 ****************************************************************************/
INT4
OfcFlowRemPktSend (tOfpFlowRem * pOfpFlowRem, VOID *pConnPtr)
{
    INT4                i4Return = OSIX_SUCCESS;
    tOfpPkt             Ofp;
    tOfcFsofcControllerConnEntry *pLocal = pConnPtr;

    MEMSET (&Ofp, OFC_ZERO, sizeof (tOfpPkt));
    pOfpFlowRem->u2Priority = OSIX_HTONS (pOfpFlowRem->u2Priority);
    pOfpFlowRem->u4DurationSec = OSIX_HTONL (pOfpFlowRem->u4DurationSec);
    pOfpFlowRem->u4DurationNSec = OSIX_HTONL (pOfpFlowRem->u4DurationNSec);
    pOfpFlowRem->u2IdleTimeOut = OSIX_HTONS (pOfpFlowRem->u2IdleTimeOut);
    Ofp.hl.FlowRem = *pOfpFlowRem;

    Ofp.u1Version = OFP_VERSION;
    Ofp.u1Type = OFPT_FLOW_REMOVED;
    Ofp.u2Length = OFP_HEADER_LEN + sizeof (tOfpFlowRem);
    if (pLocal != NULL)
    {
        pLocal->u4TxXid++;
        Ofp.u4Xid = pLocal->u4TxXid;
    }
    i4Return = OfcOfpPktSend (&Ofp, pConnPtr);
    return i4Return;
}

/****************************************************************************
 * Function    :  OfcPortStatusPktSend
 * Description :  This function is for pkt send routine  
 * Input       :  pOfpPortStatus Pointer to the header and pConnPtr Conn Ptr
 * Output      :  None 
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE 
 ****************************************************************************/
INT4
OfcPortStatusPktSend (tOfpPortStatus * pOfpPortStatus, VOID *pConnPtr)
{
    INT4                i4Return = OSIX_SUCCESS;
    tOfpPkt             Ofp;
    tOfcFsofcControllerConnEntry *pLocal = pConnPtr;
    MEMSET (&Ofp, OFC_ZERO, sizeof (tOfpPkt));
    pOfpPortStatus->PhyPort.u2Port =
        OSIX_HTONS (pOfpPortStatus->PhyPort.u2Port);
    pOfpPortStatus->PhyPort.u4Config =
        OSIX_HTONL (pOfpPortStatus->PhyPort.u4Config);
    pOfpPortStatus->PhyPort.u4State =
        OSIX_HTONL (pOfpPortStatus->PhyPort.u4State);
    pOfpPortStatus->PhyPort.u4Curr =
        OSIX_HTONL (pOfpPortStatus->PhyPort.u4Curr);
    pOfpPortStatus->PhyPort.u4Advertised =
        OSIX_HTONL (pOfpPortStatus->PhyPort.u4Advertised);
    pOfpPortStatus->PhyPort.u4Supported =
        OSIX_HTONL (pOfpPortStatus->PhyPort.u4Supported);
    pOfpPortStatus->PhyPort.u4Peer =
        OSIX_HTONL (pOfpPortStatus->PhyPort.u4Peer);

    Ofp.u1Version = OFP_VERSION;
    Ofp.u1Type = OFPT_PORT_STATUS;
    Ofp.u2Length = OFP_HEADER_LEN + sizeof (tOfpPortStatus);
    if (pLocal != NULL)
    {
        pLocal->u4TxXid++;
        Ofp.u4Xid = pLocal->u4TxXid;
    }
    Ofp.hl.PortStatus = *pOfpPortStatus;
    i4Return = OfcOfpPktSend (&Ofp, pConnPtr);
    return i4Return;
}

/****************************************************************************
 * Function    :  OfcStatsRplyPktSend
 * Description :  This function is for pkt send routine  
 * Input       :  pOfpStatsRply Pointer to the header and pConnPtr Conn Ptr
 * Output      :  None 
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE 
 ****************************************************************************/
INT4
OfcStatsRplyPktSend (tOfpStatsRply * pOfpStatsRply, VOID *pConnPtr)
{
    INT4                i4Return = OSIX_SUCCESS;
    tOfpPkt            *pOfp = (tOfpPkt *) (VOID *) au1TxData;
    tOfcFsofcControllerConnEntry *pLocal = pConnPtr;

    pOfpStatsRply->u2Type = OSIX_HTONS (pOfpStatsRply->u2Type);
    pOfpStatsRply->u2Flags = OSIX_HTONS (pOfpStatsRply->u2Flags);

    pOfp->u1Version = OFP_VERSION;
    pOfp->u1Type = OFPT_STATS_REPLY;
    pOfp->u2Length =
        (UINT2) ((UINT2) pLocal->u4TxDataLength + OFP_HEADER_LEN +
                 (UINT2) OFC_FOUR);
    pOfp->u4Xid = pLocal->u4RxXid;
    MEMCPY (&pOfp->hl.StatsRply, pOfpStatsRply,
            pLocal->u4TxDataLength + OFC_FOUR);

    i4Return = OfcOfpPktSend (pOfp, pConnPtr);
    return i4Return;
}

/****************************************************************************
 * Function    :  OfcBarrierRplyPktSend
 * Description :  This function is for pkt send routine  
 * Input       :  pOfpBarrierRply Pointer to the header and pConnPtr Conn Ptr
 * Output      :  None 
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE 
 ****************************************************************************/
INT4
OfcBarrierRplyPktSend (tOfpBarrierRply * pOfpBarrierRply, VOID *pConnPtr)
{
    tOfpPkt            *pOfp = (tOfpPkt *) (VOID *) au1TxData;
    tOfcFsofcControllerConnEntry *pLocal = pConnPtr;
    tOfpPkt            *pTempOfp = (tOfpPkt *) (VOID *) pLocal->pRcvPkt;

    pOfp->u1Version = OFP_VERSION;
    pOfp->u1Type = OFPT_BARRIER_REPLY;
    pOfp->u2Length = OFC_EIGHT;
    pOfp->u4Xid = OSIX_NTOHL (pTempOfp->u4Xid);
    OfcOfpPktSend (pOfp, pConnPtr);
    UNUSED_PARAM (pOfpBarrierRply);
    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  OfcPktSend
 * Description :  This function is for pkt send routinet to lower layer  
 * Input       :  pOfpQueueRply Pointer to the buffer and pConnPtr Conn Ptr
 * Output      :  None 
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE 
 ****************************************************************************/
INT4
OfcPktSend (UINT1 *pBuf, UINT4 u4Length, VOID *pConnPtr)
{
    if (send (((tOfcFsofcControllerConnEntry *) pConnPtr)->i4SocketDesc,
              pBuf, (INT4) u4Length, 0) == OFC_MINUS_ONE)
    {
        OFC_TRC_FUNC ((OFC_PKT_TRC, "FUNC:OfcPktSend: Failed on socket=%d",
                       ((tOfcFsofcControllerConnEntry *) pConnPtr)->
                       i4SocketDesc));
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
};

/************************************************************************
 *  Function Name   : OfcPktOutPortFromActionHdr
 *  Description     : An util to fetch the o/p port from Action
 *                    header
 *  Input           : pActionHdr - Action Hdr
 *                  : i4ActionLen = Action len
 *  Output          : None
 *
 *  Returns         : returns Outport
 ************************************************************************/
PRIVATE UINT4
OfcPktOutPortFromActionHdr (tOfpActionHdr * pActionHdr, INT4 i4ActionLen)
{
    tOfcActOutPort     *pActOutport = NULL;
    while (i4ActionLen > OFC_ZERO)
    {
        pActionHdr->u2Type = OSIX_NTOHS (pActionHdr->u2Type);
        pActionHdr->u2Len = OSIX_NTOHS (pActionHdr->u2Len);
        switch (pActionHdr->u2Type)
        {
            case OFPAT_OUTPUT:
                pActOutport = (tOfcActOutPort *) pActionHdr;
                pActOutport->u2OutPort = OSIX_NTOHS (pActOutport->u2OutPort);
                return ((UINT4) pActOutport->u2OutPort);

            default:
                break;
        }
        i4ActionLen -= pActionHdr->u2Len;
        pActionHdr =
            (tOfpActionHdr
             *) ((VOID *) (((UINT1 *) pActionHdr) + pActionHdr->u2Len));
    }
    return OFC_INVALID_INDEX;
}
