#define _OFCIDXSZ_C

#include "ofcidxsz.h"
#include "ofcidxgl.h"

extern INT4  IssSzRegisterModuleSizingParams( CHR1 *pu1ModName, tFsModSizingParams *pModSizingParams); 
INT4  OfcIndexmgrSizingMemCreateMemPools()
{
    UINT4 u4RetVal;
    INT4 i4SizingId;

    for( i4SizingId=0; i4SizingId < INDEXMGR_MAX_SIZING_ID; i4SizingId++) {
        u4RetVal = MemCreateMemPool( 
                          FsOFCINDEXMGRSizingParams[i4SizingId].u4StructSize,
                          FsOFCINDEXMGRSizingParams[i4SizingId].u4PreAllocatedUnits,
                          MEM_DEFAULT_MEMORY_TYPE,
                          &(OFCINDEXMGRMemPoolIds[ i4SizingId]));
        if( u4RetVal == MEM_FAILURE) {
            OfcIndexmgrSizingMemDeleteMemPools();
            return OSIX_FAILURE;
        }
    }
    return OSIX_SUCCESS;
}


INT4   OfcIndexmgrSzRegisterModuleSizingParams( CHR1 *pu1ModName)
{
      /* Copy the Module Name */ 
      IssSzRegisterModuleSizingParams( pu1ModName, FsOFCINDEXMGRSizingParams); 
      return OSIX_SUCCESS; 
}


VOID  OfcIndexmgrSizingMemDeleteMemPools()
{
    INT4 i4SizingId;

    for( i4SizingId=0; i4SizingId < INDEXMGR_MAX_SIZING_ID; i4SizingId++) {
        if(OFCINDEXMGRMemPoolIds[i4SizingId] != 0)
        {
            MemDeleteMemPool( OFCINDEXMGRMemPoolIds[ i4SizingId] );
            OFCINDEXMGRMemPoolIds[i4SizingId] = 0;
        }
    }
    return;
}
