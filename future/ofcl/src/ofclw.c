/********************************************************************
* Copyright (C) 2013 Aricent Inc . All Rights Reserved
*
* Id: ofclw.c 
*
* Description: This file contains some of low level functions used by protocol in Ofc module
*********************************************************************/

#include "ofcinc.h"

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsofcCfgTable
 Input       :  The Indices
                FsofcContextId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsofcCfgTable (UINT4 u4FsofcContextId)
{
    UNUSED_PARAM (u4FsofcContextId);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsofcControllerConnTable
 Input       :  The Indices
                FsofcContextId
                FsofcControllerIpAddrType
                FsofcControllerIpAddress
                FsofcControllerConnAuxId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsofcControllerConnTable (UINT4 u4FsofcContextId,
                                                  INT4
                                                  i4FsofcControllerIpAddrType,
                                                  tSNMP_OCTET_STRING_TYPE *
                                                  pFsofcControllerIpAddress,
                                                  INT4
                                                  i4FsofcControllerConnAuxId)
{
    UNUSED_PARAM (u4FsofcContextId);
    UNUSED_PARAM (i4FsofcControllerIpAddrType);
    UNUSED_PARAM (pFsofcControllerIpAddress);
    UNUSED_PARAM (i4FsofcControllerConnAuxId);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsofcIfTable
 Input       :  The Indices
                FsofcContextId
                FsofcIfIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsofcIfTable (UINT4 u4FsofcContextId,
                                      INT4 i4FsofcIfIndex)
{
    UNUSED_PARAM (u4FsofcContextId);
    UNUSED_PARAM (i4FsofcIfIndex);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsofcFlowTable
 Input       :  The Indices
                FsofcContextId
                FsofcTableIndex
                FsofcFlowIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsofcFlowTable (UINT4 u4FsofcContextId,
                                        UINT4 u4FsofcTableIndex,
                                        UINT4 u4FsofcFlowIndex)
{
    UNUSED_PARAM (u4FsofcContextId);
    UNUSED_PARAM (u4FsofcTableIndex);
    UNUSED_PARAM (u4FsofcFlowIndex);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsofcGroupTable
 Input       :  The Indices
                FsofcContextId
                FsofcGroupIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsofcGroupTable (UINT4 u4FsofcContextId,
                                         UINT4 u4FsofcGroupIndex)
{
    UNUSED_PARAM (u4FsofcContextId);
    UNUSED_PARAM (u4FsofcGroupIndex);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsofcMeterTable
 Input       :  The Indices
                FsofcContextId
                FsofcMeterIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsofcMeterTable (UINT4 u4FsofcContextId,
                                         UINT4 u4FsofcMeterIndex)
{
    UNUSED_PARAM (u4FsofcContextId);
    UNUSED_PARAM (u4FsofcMeterIndex);
    return SNMP_SUCCESS;
}
