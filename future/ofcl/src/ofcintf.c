/*****************************************************************************/
/* Copyright (C) 2013 Aricent Inc . All Rights Reserved
 * ******************************************************************************
 * * *    FILE  NAME             : ofcintf.c
 * * *    PRINCIPAL AUTHOR       : Aricent Inc.
 * * *    SUBSYSTEM NAME         : OFC
 * * *    MODULE NAME            : OFC-Interfaces
 * * *    LANGUAGE               : ANSI-C
 * * *    TARGET ENVIRONMENT     : Linux (Portable)
 * * *    DATE OF FIRST RELEASE  :
 * * *    DESCRIPTION            : This file contains routines for interface table
 * * *                             implementations.
 * * *
 * * * $Id: ofcintf.c,v 1.6 2015/02/27 10:54:25 siva Exp $
 * * *---------------------------------------------------------------------------*/

#include "lr.h"
#include "cfa.h"
#include "ofcl.h"
#include "ofcintf.h"
#include "ofcacts.h"
#include "ofcflow.h"
#include "ofcinc.h"
#include "ofcsz.h"
#include "ofcvlan.h"
#include "ofcapi.h"
#include "ofcidxsz.h"
#include "ofcidxgl.h"
#ifdef NPAPI_WANTED
#include "ofcnp.h"
#include "npapi.h"
#endif

PRIVATE INT4        OfcInterfacePipelineProcessEventSend (UINT4 u4IfIndex,
                                                          tCRU_BUF_CHAIN_HEADER
                                                          * pBuf);
extern INT4         CfaGddEthWrite (UINT1 *pu1Buf, UINT2 u2IfIndex,
                                    UINT4 u4Size);

/*****************************************************************************
* Function    :  OpenflowNotifyPortStatus
* Input       :  u4IfIndex
*                u1OperStatus
* Description :  This Routine is called when the Operational Status of the
*                openflow port changes and notifies the Openflow OCntroller
*                of the Port Status changes

* Output      :  None
* Returns     :  OFC_SUCCESS or OFC_FAILURE
* *************************************************************************/
INT4
OpenflowNotifyPortStatus (UINT4 u4IfIndex, INT1 i1OperStatus)
{
    tOfpPortStatus     *pOfpPortSt = NULL;
    tOfp131PortStatus  *pOfp131PortStat = NULL;
    tOfcFsofcIfEntry   *pOfcFsofcIfEntry = NULL;
    tOfcFsofcCfgEntry  *pOfcFsofcCfgEntry = NULL;
    tOfcFsofcIfEntry    lOfcFsofcIfEntry;
    tCfaIfInfo          cfaifInfo;
    UINT4               u4Mask = OFC_ZERO;
    UINT4               u4ContextId = OFC_ZERO;
    INT4                i4RetValue = OFC_FAILURE;

#ifdef NPAPI_WANTED
    tOfcHwInfo          OfcHwInfo;
#endif

    OFC_TRC_FUNC ((OFC_FN_ENTRY, "Entry :: OpenflowNotifyPortStatus \n"));

#ifdef NPAPI_WANTED
    MEMSET (&OfcHwInfo, OFC_ZERO, sizeof (tOfcHwInfo));
#endif
    MEMSET (&cfaifInfo, OFC_ZERO, sizeof (cfaifInfo));
    if (CfaGetIfInfo (u4IfIndex, &cfaifInfo) == CFA_FAILURE)
    {
        return OSIX_FAILURE;
    }

    if (OfcGetContextIdFromIfIndex (u4IfIndex, &u4ContextId) != OFC_SUCCESS)
    {
        OFC_TRC ((OFC_UTIL_TRC, "\r\nOpenflowNotifyPortStatus: "
                  "Get Context Failed\n"));
        return OSIX_FAILURE;
    }

    MEMSET (&lOfcFsofcIfEntry.MibObject, OFC_ZERO,
            sizeof (lOfcFsofcIfEntry.MibObject));
    lOfcFsofcIfEntry.MibObject.u4FsofcContextId = u4ContextId;
    lOfcFsofcIfEntry.MibObject.i4FsofcIfIndex = (INT4) (u4IfIndex);
    pOfcFsofcIfEntry = RBTreeGet (gOfcGlobals.OfcGlbMib.FsofcIfTable,
                                  (tRBElem *) & (lOfcFsofcIfEntry));

    pOfcFsofcCfgEntry = (tOfcFsofcCfgEntry *)
        RBTreeGetFirst (gOfcGlobals.OfcGlbMib.FsofcCfgTable);
    if (pOfcFsofcCfgEntry == NULL)
    {
        OFC_TRC ((OFC_UTIL_TRC,
                  "\r\nOpenflowNotifyPortStatus: Fail to get CFG Entry.\n"));
        return OSIX_FAILURE;
    }

    if (pOfcFsofcCfgEntry->MibObject.i4FsofcSupportedVersion ==
        OFC_VERSION_V100)
    {
        /* Allocate memory for the new node */
        pOfpPortSt = (tOfpPortStatus *) MemAllocMemBlk (OFC_PORT_STATUS_POOLID);
        if (pOfpPortSt == NULL)
        {
            OFC_TRC ((OFC_UTIL_TRC,
                      "pOfpPortSt: Fail to Allocate Memory.\r\n"));
            return OFC_FAILURE;
        }

        MEMSET (pOfpPortSt, OFC_ZERO, sizeof (tOfpPortStatus));
        pOfpPortSt->PhyPort.u2Port = (UINT2) u4IfIndex;
        MEMCPY (pOfpPortSt->PhyPort.au1Name, cfaifInfo.au1IfAliasName,
                OFC_SIXTEEN);
        pOfpPortSt->PhyPort.u4State = (UINT4) i1OperStatus;
        MEMCPY (pOfpPortSt->PhyPort.au1HwAddr, cfaifInfo.au1MacAddr,
                sizeof (pOfpPortSt->PhyPort.au1HwAddr));

        if (pOfcFsofcIfEntry != NULL)
        {
            if (i1OperStatus == CFA_IF_DOWN)
            {
                u4Mask = OFC_ONE << OFC_ZERO;
                pOfpPortSt->PhyPort.u4Config =
                    pOfcFsofcIfEntry->u4Config | u4Mask;
                pOfcFsofcIfEntry->MibObject.i4FsofcIfOperStatus =
                    (INT4) i1OperStatus;
            }
            else
            {
                pOfpPortSt->PhyPort.u4Config = pOfcFsofcIfEntry->u4Config;
                pOfcFsofcIfEntry->MibObject.i4FsofcIfOperStatus =
                    (INT4) i1OperStatus;
            }
        }
        else
        {
            if (u4ContextId != OFC_INVALID_CONTEXT)
            {
                OFC_TRC_FUNC ((OFC_FN_EXIT, "\n Modify Failed, "
                               "due to non-existing If MIB Table Entry \n"));
                MemReleaseMemBlock (OFC_PORT_STATUS_POOLID,
                                    (UINT1 *) pOfpPortSt);
                return OFC_FAILURE;
            }
        }

        pOfpPortSt->PhyPort.u4Curr = OFC_ZERO;
        pOfpPortSt->PhyPort.u4Supported = OFC_ZERO;
        pOfpPortSt->PhyPort.u4Advertised = OFC_ZERO;
        pOfpPortSt->PhyPort.u4Peer = OFC_ZERO;

        pOfpPortSt->u1Reason = OFPPR_MODIFY;

#ifdef NPAPI_WANTED
        if (i1OperStatus == CFA_IF_DOWN)
        {
            OfcHwInfo.OfcHwEntry.OfcHwIfInfo.u4IfIndex = u4IfIndex;
            OfcHwInfo.OfcVer = OFC_VER_100;
            OfcHwInfo.OfcCmd = OFC_NP_PORT_DOWN;
            if (OfcFsNpHwConfigOfcInfo (&OfcHwInfo) != FNP_SUCCESS)
            {
                OFC_TRC ((OFC_UTIL_TRC,
                          "pOfp131PortStat: Fail to Program HW for Port Down - V100.\r\n"));
            }
        }
        else
        {
            OfcHwInfo.OfcHwEntry.OfcHwIfInfo.u4IfIndex = u4IfIndex;
            OfcHwInfo.OfcVer = OFC_VER_100;
            OfcHwInfo.OfcCmd = OFC_NP_PORT_UP;
            if (OfcFsNpHwConfigOfcInfo (&OfcHwInfo) != FNP_SUCCESS)
            {
                OFC_TRC ((OFC_UTIL_TRC,
                          "pOfp131PortStat: Fail to Program HW for Port Up - V100.\r\n"));
            }
        }
#endif

        /* 
         * Send Port Status Notification to Controller, only when port is mapped to
         * a valid context 
         */
        if (u4ContextId != (UINT4) OFC_INVALID_CONTEXT)
        {
            i4RetValue =
                OfcInterfacePortStatusEventSend (pOfpPortSt, u4ContextId);
            if (i4RetValue != OFC_SUCCESS)
            {
                MemReleaseMemBlock (OFC_PORT_STATUS_POOLID,
                                    (UINT1 *) pOfpPortSt);
            }
        }
        else
        {
            MemReleaseMemBlock (OFC_PORT_STATUS_POOLID, (UINT1 *) pOfpPortSt);
        }
    }
    else
    {
        /* Allocate memory for the new node */
        pOfp131PortStat =
            (tOfp131PortStatus *) MemAllocMemBlk (OFC_PORT_STATUS_POOLID);
        if (pOfp131PortStat == NULL)
        {
            OFC_TRC ((OFC_UTIL_TRC,
                      "pOfp131PortStat: Fail to Allocate Memory.\r\n"));
            return OFC_FAILURE;
        }

        MEMSET (pOfp131PortStat, OFC_ZERO, sizeof (tOfp131PortStatus));
        pOfp131PortStat->PortDesc.u4PortNo = (UINT4) u4IfIndex;
        MEMCPY (pOfp131PortStat->PortDesc.au1Name, cfaifInfo.au1IfAliasName,
                OFC_SIXTEEN);
        pOfp131PortStat->PortDesc.u4State = (UINT4) i1OperStatus;
        MEMCPY (pOfp131PortStat->PortDesc.au1HwAddr, cfaifInfo.au1MacAddr,
                sizeof (pOfp131PortStat->PortDesc.au1HwAddr));

        if (pOfcFsofcIfEntry != NULL)
        {
            if (i1OperStatus == CFA_IF_DOWN)
            {
                u4Mask = OFC_ONE << OFC_ZERO;
                pOfp131PortStat->PortDesc.u4Config =
                    pOfcFsofcIfEntry->u4Config | u4Mask;
                pOfcFsofcIfEntry->MibObject.i4FsofcIfOperStatus =
                    (INT4) i1OperStatus;
            }
            else
            {
                pOfp131PortStat->PortDesc.u4Config = pOfcFsofcIfEntry->u4Config;
                pOfcFsofcIfEntry->MibObject.i4FsofcIfOperStatus =
                    (INT4) i1OperStatus;
            }
        }
        else
        {
            if (u4ContextId != OFC_INVALID_CONTEXT)
            {
                OFC_TRC_FUNC ((OFC_FN_EXIT, "\n Modify Failed, "
                               "due to non-existing If MIB Table Entry \n"));
                MemReleaseMemBlock (OFC_PORT_STATUS_POOLID,
                                    (UINT1 *) pOfp131PortStat);
                return OFC_FAILURE;
            }
        }

        pOfp131PortStat->PortDesc.u4Curr = OFC_ZERO;
        pOfp131PortStat->PortDesc.u4Supported = OFC_ZERO;
        pOfp131PortStat->PortDesc.u4Advertised = OFC_ZERO;
        pOfp131PortStat->PortDesc.u4Peer = OFC_ZERO;
        pOfp131PortStat->PortDesc.u4CurrSpeed = cfaifInfo.u4IfSpeed;
        pOfp131PortStat->PortDesc.u4MaxSpeed = cfaifInfo.u4IfHighSpeed;
        pOfp131PortStat->u1Reason = OFPPR_MODIFY;

#ifdef NPAPI_WANTED
        if (i1OperStatus == CFA_IF_DOWN)
        {
            OfcHwInfo.OfcHwEntry.OfcHwIfInfo.u4IfIndex = u4IfIndex;
            OfcHwInfo.OfcVer = OFC_VER_131;
            OfcHwInfo.OfcCmd = OFC_NP_PORT_DOWN;
            if (OfcFsNpHwConfigOfcInfo (&OfcHwInfo) != FNP_SUCCESS)
            {
                OFC_TRC ((OFC_UTIL_TRC,
                          "pOfp131PortStat: Fail to Program HW for Port Down - V131.\r\n"));
            }
        }
        else
        {
            OfcHwInfo.OfcHwEntry.OfcHwIfInfo.u4IfIndex = u4IfIndex;
            OfcHwInfo.OfcVer = OFC_VER_131;
            OfcHwInfo.OfcCmd = OFC_NP_PORT_UP;
            if (OfcFsNpHwConfigOfcInfo (&OfcHwInfo) != FNP_SUCCESS)
            {
                OFC_TRC ((OFC_UTIL_TRC,
                          "pOfp131PortStat: Fail to Program HW for Port Up - V131.\r\n"));
            }
        }
#endif

        /* 
         * Send Port Status Notification to Controller, only when port is mapped to
         * a valid context 
         */
        if (u4ContextId != (UINT4) OFC_INVALID_CONTEXT)
        {
            i4RetValue =
                OfcInterfacePortStatusEventSend (pOfp131PortStat, u4ContextId);
            if (i4RetValue != OFC_SUCCESS)
            {
                MemReleaseMemBlock (OFC_PORT_STATUS_POOLID,
                                    (UINT1 *) pOfp131PortStat);
            }
        }
        else
        {
            MemReleaseMemBlock (OFC_PORT_STATUS_POOLID,
                                (UINT1 *) pOfp131PortStat);
        }
    }

    OFC_TRC_FUNC ((OFC_FN_EXIT, "EXIT :: OpenflowNotifyPortStatus \n"));
    return OFC_SUCCESS;
}

/******************************************************************************
* Function    :  OpenflowPortMapUnmap
* Input       :  u4IfIndex
*                u4Context
*                i1action
* Description :  This Routine is called to Map/Unmap a openflow interface to
*                given context.Also this function handles the Port Creation/
*                Deletion Notification to Controller during Map/Unmap
* Output      :  None
* Returns     :  OFC_SUCCESS or OFC_FAILURE
******************************************************************************/
INT4
OpenflowPortMapUnmap (UINT4 u4IfIndex, UINT4 u4Context, INT1 i1action)
{
    tOfcFsofcIfEntry    lOfcFsofcIfEntry;

    OFC_TRC_FUNC ((OFC_FN_ENTRY, "\r\nENTRY :: OpenflowPortMapUnmap \n"));

    MEMSET (&lOfcFsofcIfEntry.MibObject, OFC_ZERO,
            sizeof (lOfcFsofcIfEntry.MibObject));
    MEMSET (&lOfcFsofcIfEntry, OFC_ZERO, sizeof (lOfcFsofcIfEntry));

    if (i1action == OSIX_TRUE)
    {
        /* 
         * Deleting the Existing Mapping to Default Context by removing the entire If Entry
         * and re-create the If Entry with new Context ID 
         */
        if ((OpenflowNotifyPortCreateDelete (u4IfIndex, OFC_INVALID_CONTEXT,
                                             OF_PORT_DELETE)) == OFC_FAILURE)
        {
            OFC_TRC ((OFC_UTIL_TRC,
                      "\r\nUn Mapping the Interface from Default Context is Failed, "
                      "Returning FAILURE\n"));
            return OFC_FAILURE;
        }
        if ((OpenflowNotifyPortCreateDelete (u4IfIndex, u4Context,
                                             OF_PORT_CREATE)) == OFC_FAILURE)
        {
            OFC_TRC ((OFC_UTIL_TRC,
                      "\r\nMapping the Interface to given Context is Failed, "
                      "Returning FAILURE\n"));
            return OFC_FAILURE;
        }

        lOfcFsofcIfEntry.MibObject.u4FsofcContextId = u4Context;
        lOfcFsofcIfEntry.MibObject.i4FsofcIfIndex = (INT4) u4IfIndex;
        lOfcFsofcIfEntry.MibObject.u4FsofcIfContextId = u4Context;
        if (OfcUpdateInterfaceMapping (&lOfcFsofcIfEntry) == OFC_FAILURE)
        {
            OFC_TRC ((OFC_UTIL_TRC,
                      "\r\nMapping Update to given Context is Failed, "
                      "Returning FAILURE\n"));
            return OFC_FAILURE;
        }
    }
    else if (i1action == OSIX_FALSE)
    {
        /* 
         * Deleting the Existing Mapping to assigned Context by removing the entire If Entry
         * and re-create the If Entry with default Context ID 
         */
        if ((OpenflowNotifyPortCreateDelete (u4IfIndex, u4Context,
                                             OF_PORT_DELETE)) == OFC_FAILURE)
        {
            OFC_TRC ((OFC_UTIL_TRC,
                      "\r\nUn Mapping the Interface from assigned Context is Failed, "
                      "Returning FAILURE\n"));
            return OFC_FAILURE;
        }
        if ((OpenflowNotifyPortCreateDelete (u4IfIndex, OFC_INVALID_CONTEXT,
                                             OF_PORT_CREATE)) == OFC_FAILURE)
        {
            OFC_TRC ((OFC_UTIL_TRC, "\r\nMapping to Default Context is Failed, "
                      "Returning FAILURE\n"));
            return OFC_FAILURE;
        }

        lOfcFsofcIfEntry.MibObject.u4FsofcContextId = OFC_DEFAULT_CONTEXT;
        lOfcFsofcIfEntry.MibObject.i4FsofcIfIndex = (INT4) u4IfIndex;
        lOfcFsofcIfEntry.MibObject.u4FsofcIfContextId = OFC_INVALID_CONTEXT;
        if (OfcUpdateInterfaceMapping (&lOfcFsofcIfEntry) == OFC_FAILURE)
        {
            OFC_TRC ((OFC_UTIL_TRC,
                      "\r\nMapping Update to given Context is Failed, "
                      "Returning FAILURE\n"));
            return OFC_FAILURE;
        }
    }
    else
    {
        OFC_TRC ((OFC_UTIL_TRC, "\r\nInvalid Option other than MAP and UNMAP, "
                  "Returning FAILURE\n"));
        return OFC_FAILURE;
    }

    OFC_TRC_FUNC ((OFC_FN_EXIT, "\r\nEXIT :: OpenflowPortMapUnmap \n"));
    return OFC_SUCCESS;
}

/****************************************************************************** 
* Function    :  OpenflowNotifyPortCreateDelete
* Input       :  u4IfIndex
*                i1action
* Description :  This Routine is called to notify the Controller 
*                when an openflow port is created or deleted.      
* Output      :  None
* Returns     :  OFC_SUCCESS or OFC_FAILURE
******************************************************************************/
INT4
OpenflowNotifyPortCreateDelete (UINT4 u4IfIndex, UINT4 u4Context, INT1 i1action)
{
    tOfpPortStatus     *pOfpPortSt = NULL;
    tOfp131PortStatus  *pOfp131PortStat = NULL;
    tOfcFsofcIfEntry   *pOfcFsofcIfEntry = NULL;
    tOfcFsofcCfgEntry  *pOfcFsofcCfgEntry = NULL;
    tCfaIfInfo          cfaifInfo;
    tOfcFsofcIfEntry    lOfcFsofcIfEntry;
    UINT4               u4Config = OFC_ZERO;
    UINT4               u4IfContext = OFC_DEFAULT_CONTEXT;

#ifdef NPAPI_WANTED
    tOfcHwInfo          OfcHwInfo;
#endif

    OFC_TRC_FUNC ((OFC_FN_ENTRY, "ENTRY :: OpenflowNotifyPortCreateDelete \n"));

#ifdef NPAPI_WANTED
    MEMSET (&OfcHwInfo, OFC_ZERO, sizeof (tOfcHwInfo));
#endif
    MEMSET (&lOfcFsofcIfEntry.MibObject, OFC_ZERO,
            sizeof (lOfcFsofcIfEntry.MibObject));
    MEMSET (&lOfcFsofcIfEntry, OFC_ZERO, sizeof (lOfcFsofcIfEntry));
    MEMSET (&cfaifInfo, OFC_ZERO, sizeof (cfaifInfo));

    if (CfaGetIfInfo (u4IfIndex, &cfaifInfo) == CFA_FAILURE)
    {
        OFC_TRC ((OFC_UTIL_TRC, "\r\nUnable to get the Interface Info from CFA,"
                  "Return FAILURE\n"));
        return OFC_FAILURE;
    }

    pOfcFsofcCfgEntry = (tOfcFsofcCfgEntry *)
        RBTreeGetFirst (gOfcGlobals.OfcGlbMib.FsofcCfgTable);
    if (pOfcFsofcCfgEntry == NULL)
    {
        OFC_TRC ((OFC_UTIL_TRC,
                  "\r\nOpenflowNotifyPortCreateDelete: Fail to get CFG Entry.\n"));
        return OSIX_FAILURE;
    }

    if (i1action == OFPPR_ADD)
    {
        /* Create the port entry in the OFC Database */
        lOfcFsofcIfEntry.MibObject.i4FsofcIfIndex = (INT4) u4IfIndex;
        lOfcFsofcIfEntry.MibObject.i4FsofcIfType = cfaifInfo.u1IfType;
        lOfcFsofcIfEntry.MibObject.i4FsofcIfOperStatus =
            cfaifInfo.u1IfOperStatus;

        if (u4Context == OFC_INVALID_CONTEXT)
        {
            lOfcFsofcIfEntry.MibObject.u4FsofcContextId = OFC_DEFAULT_CONTEXT;
        }
        else
        {
            lOfcFsofcIfEntry.MibObject.u4FsofcContextId = u4Context;
        }

        /* Update the Interface Mapping to Invalid Context */
        lOfcFsofcIfEntry.MibObject.u4FsofcIfContextId = u4Context;
        lOfcFsofcIfEntry.MibObject.i4FsofcIfAliasLen =
            sizeof (lOfcFsofcIfEntry.MibObject.au1FsofcIfAlias);
        lOfcFsofcIfEntry.u4Config = OFC_ZERO;
        MEMCPY (lOfcFsofcIfEntry.MibObject.au1FsofcIfAlias,
                cfaifInfo.au1IfAliasName,
                sizeof (lOfcFsofcIfEntry.MibObject.au1FsofcIfAlias));

        MEMCPY (lOfcFsofcIfEntry.au1Name, cfaifInfo.au1IfAliasName,
                OFC_SIXTEEN);

        MEMCPY (lOfcFsofcIfEntry.au1HwAddr, cfaifInfo.au1MacAddr,
                sizeof (lOfcFsofcIfEntry.au1HwAddr));
        /* Fill in the information for the queue here */
        lOfcFsofcIfEntry.Queue[OFC_ZERO].u4QueueId = OFC_ZERO;
        lOfcFsofcIfEntry.Queue[OFC_ZERO].QueueProp[OFC_ZERO].u2Property =
            OFCQT_MIN_RATE;
        lOfcFsofcIfEntry.Queue[OFC_ZERO].QueueProp[OFC_ZERO].u2Rate =
            OFCQ_MIN_RATE_UNCF;
        lOfcFsofcIfEntry.Queue[OFC_ZERO].QueueProp[OFC_ONE].u2Property =
            OFCQT_MAX_RATE;
        lOfcFsofcIfEntry.Queue[OFC_ZERO].QueueProp[OFC_ONE].u2Rate =
            OFCQ_MAX_RATE_UNCF;

#ifdef NPAPI_WANTED
        lOfcFsofcIfEntry.u4DflFlowIndex =
            OfcIndexMgrGetIndexBasedOnFlag (OFC_ONE, TRUE);
#endif

        pOfcFsofcIfEntry = OfcFsofcIfTableCreateApi (&lOfcFsofcIfEntry);
        if (pOfcFsofcIfEntry == NULL)
        {
            OFC_TRC ((OFC_UTIL_TRC, "\r\nUnable to create Interface Entry,"
                      " Return FAILURE\n"));
            return OFC_FAILURE;
        }

        /* Inform the controller about the new openflow port */
        if (pOfcFsofcCfgEntry->MibObject.i4FsofcSupportedVersion ==
            OFC_VERSION_V100)
        {
            /* Allocate memory for the new node */
            pOfpPortSt =
                (tOfpPortStatus *) MemAllocMemBlk (OFC_PORT_STATUS_POOLID);
            if (pOfpPortSt == NULL)
            {
                OFC_TRC ((OFC_UTIL_TRC,
                          "pOfpPortSt: Fail to Allocate Memory.\r\n"));
                return OFC_FAILURE;
            }

            MEMSET (pOfpPortSt, OFC_ZERO, sizeof (tOfpPortStatus));
            pOfpPortSt->PhyPort.u2Port = (UINT2) u4IfIndex;
            pOfpPortSt->PhyPort.u4Config = OFC_PORT_CONFIG_ALL;
            pOfpPortSt->PhyPort.u4Curr = OFC_ZERO;
            pOfpPortSt->PhyPort.u4Supported = OFC_ZERO;
            pOfpPortSt->PhyPort.u4Advertised = OFC_ZERO;
            pOfpPortSt->PhyPort.u4Peer = OFC_ZERO;
            pOfpPortSt->PhyPort.u4State = cfaifInfo.u1IfOperStatus;
            pOfpPortSt->u1Reason = OFPPR_ADD;
            MEMCPY (pOfpPortSt->PhyPort.au1Name, cfaifInfo.au1IfAliasName,
                    OFC_SIXTEEN);
            MEMCPY (pOfpPortSt->PhyPort.au1HwAddr, cfaifInfo.au1MacAddr,
                    sizeof (pOfpPortSt->PhyPort.au1HwAddr));
#ifdef NPAPI_WANTED
            if (i1action == OFPPR_ADD)
            {
                OfcHwInfo.OfcHwEntry.OfcHwIfInfo.u4IfIndex = u4IfIndex;
                OfcHwInfo.OfcHwEntry.OfcHwIfInfo.u4FlowIndex =
                    pOfcFsofcIfEntry->u4DflFlowIndex;
                OfcHwInfo.OfcVer = OFC_VER_100;
                OfcHwInfo.OfcCmd = OFC_NP_PORT_ADD;
                OFC_TRC ((OFC_UTIL_TRC,
                          "\r\nInvoking NPAPI Port Add with  flow index %d ifindex %d",
                          OfcHwInfo.OfcHwEntry.OfcHwIfInfo.u4FlowIndex,
                          u4IfIndex));
                if (OfcFsNpHwConfigOfcInfo (&OfcHwInfo) != FNP_SUCCESS)
                {
                    OFC_TRC ((OFC_UTIL_TRC, "\r\npOfp131PortStat: "
                              "Fail to Program HW for Port Add - V100.\n"));
                }
            }
#endif
            /* 
             * Send Port Create/Delete Notification to Controller, only when port is mapped to 
             * a valid context 
             */
            if (pOfcFsofcIfEntry->MibObject.u4FsofcIfContextId !=
                (UINT4) OFC_INVALID_CONTEXT)
            {
                if ((OfcInterfacePortStatusEventSend (pOfpPortSt,
                                                      pOfcFsofcIfEntry->
                                                      MibObject.
                                                      u4FsofcIfContextId)) ==
                    OFC_FAILURE)
                {
                    MemReleaseMemBlock (OFC_PORT_STATUS_POOLID,
                                        (UINT1 *) pOfpPortSt);
                }
            }
            else
            {
                MemReleaseMemBlock (OFC_PORT_STATUS_POOLID,
                                    (UINT1 *) pOfpPortSt);
            }

        }
        else
        {
            /* Allocate memory for the new node */
            pOfp131PortStat =
                (tOfp131PortStatus *) MemAllocMemBlk (OFC_PORT_STATUS_POOLID);
            if (pOfp131PortStat == NULL)
            {
                OFC_TRC ((OFC_UTIL_TRC,
                          "pOfp131PortStat: Fail to Allocate Memory.\r\n"));
                return OFC_FAILURE;
            }

            MEMSET (pOfp131PortStat, OFC_ZERO, sizeof (tOfp131PortStatus));

            pOfp131PortStat->PortDesc.u4PortNo = u4IfIndex;
            pOfp131PortStat->PortDesc.u4Config = OFC_ZERO;
            pOfp131PortStat->PortDesc.u4Curr = OFC_ZERO;
            pOfp131PortStat->PortDesc.u4Supported = OFC_ZERO;
            pOfp131PortStat->PortDesc.u4Advertised = OFC_ZERO;
            pOfp131PortStat->PortDesc.u4Peer = OFC_ZERO;
            pOfp131PortStat->PortDesc.u4CurrSpeed = cfaifInfo.u4IfSpeed;
            pOfp131PortStat->PortDesc.u4MaxSpeed = cfaifInfo.u4IfHighSpeed;
            pOfp131PortStat->u1Reason = OFPPR_ADD;
            pOfp131PortStat->PortDesc.u4State = cfaifInfo.u1IfOperStatus;
            MEMCPY (pOfp131PortStat->PortDesc.au1Name, cfaifInfo.au1IfAliasName,
                    OFC_SIXTEEN);
            MEMCPY (pOfp131PortStat->PortDesc.au1HwAddr, cfaifInfo.au1MacAddr,
                    sizeof (pOfp131PortStat->PortDesc.au1HwAddr));
#ifdef NPAPI_WANTED
            if (i1action == OFPPR_ADD)
            {
                OfcHwInfo.OfcHwEntry.OfcHwIfInfo.u4IfIndex = u4IfIndex;
                OfcHwInfo.OfcHwEntry.OfcHwIfInfo.u4FlowIndex =
                    pOfcFsofcIfEntry->u4DflFlowIndex;
                OfcHwInfo.OfcVer = OFC_VER_131;
                OfcHwInfo.OfcCmd = OFC_NP_PORT_ADD;
                OFC_TRC ((OFC_UTIL_TRC,
                          "\r\nInvoking NPAPI Port Add with  flow index %d ifindex %d",
                          OfcHwInfo.OfcHwEntry.OfcHwIfInfo.u4FlowIndex,
                          u4IfIndex));
                if (OfcFsNpHwConfigOfcInfo (&OfcHwInfo) != FNP_SUCCESS)
                {
                    OFC_TRC ((OFC_UTIL_TRC,
                              "pOfp131PortStat: Fail to Program HW for Port Add - V131.\r\n"));
                }
            }
#endif
            /* 
             * Send Port Create/Delete Notification to Controller, only when port is mapped to 
             * a valid context 
             */
            if (pOfcFsofcIfEntry->MibObject.u4FsofcIfContextId !=
                (UINT4) OFC_INVALID_CONTEXT)
            {
                if (((OfcInterfacePortStatusEventSend (pOfp131PortStat,
                                                       pOfcFsofcIfEntry->
                                                       MibObject.
                                                       u4FsofcIfContextId))) ==
                    OFC_FAILURE)
                {
                    MemReleaseMemBlock (OFC_PORT_STATUS_POOLID,
                                        (UINT1 *) pOfp131PortStat);
                }
            }
            else
            {
                MemReleaseMemBlock (OFC_PORT_STATUS_POOLID,
                                    (UINT1 *) pOfp131PortStat);
            }
        }
    }
    else if (i1action == OFPPR_DELETE)
    {
        /* Delete the port information */
        MEMSET (&lOfcFsofcIfEntry.MibObject, OFC_ZERO,
                sizeof (lOfcFsofcIfEntry.MibObject));

        if (u4Context == OFC_INVALID_CONTEXT)
        {
            lOfcFsofcIfEntry.MibObject.u4FsofcContextId = OFC_DEFAULT_CONTEXT;
        }
        else
        {
            lOfcFsofcIfEntry.MibObject.u4FsofcContextId = u4Context;
        }
        lOfcFsofcIfEntry.MibObject.i4FsofcIfIndex = (INT4) u4IfIndex;
        pOfcFsofcIfEntry = RBTreeGet (gOfcGlobals.OfcGlbMib.FsofcIfTable,
                                      (tRBElem *) & (lOfcFsofcIfEntry));
        if (pOfcFsofcIfEntry != NULL)
        {
            /* 
             * Store the config flag to fill-in for controller 
             */
            u4Config = pOfcFsofcIfEntry->u4Config;
            u4IfContext = u4Context;

#ifdef NPAPI_WANTED
            OfcHwInfo.OfcHwEntry.OfcHwIfInfo.u4FlowIndex =
                pOfcFsofcIfEntry->u4DflFlowIndex;

            /* Release the flow index and update if entry with value "0" */
            OfcIndexMgrRelIndex (OFC_ONE, pOfcFsofcIfEntry->u4DflFlowIndex);
#endif

            RBTreeRem (gOfcGlobals.OfcGlbMib.FsofcIfTable, pOfcFsofcIfEntry);
            MemReleaseMemBlock (OFC_FSOFCIFTABLE_POOLID,
                                (UINT1 *) pOfcFsofcIfEntry);
        }

        /* Inform the Controller regarding the port deletion */
        if (pOfcFsofcCfgEntry->MibObject.i4FsofcSupportedVersion ==
            OFC_VERSION_V100)
        {
            /* Allocate memory for the new node */
            pOfpPortSt =
                (tOfpPortStatus *) MemAllocMemBlk (OFC_PORT_STATUS_POOLID);
            if (pOfpPortSt == NULL)
            {
                OFC_TRC ((OFC_UTIL_TRC,
                          "pOfpPortSt: Fail to Allocate Memory.\r\n"));
                return OFC_FAILURE;
            }

            MEMSET (pOfpPortSt, OFC_ZERO, sizeof (tOfpPortStatus));
            pOfpPortSt->PhyPort.u2Port = (UINT2) u4IfIndex;
            pOfpPortSt->PhyPort.u4State = cfaifInfo.u1IfOperStatus;
            pOfpPortSt->PhyPort.u4Config = OFC_PORT_CONFIG_ALL;
            pOfpPortSt->PhyPort.u4Curr = OFC_ZERO;
            pOfpPortSt->PhyPort.u4Supported = OFC_ZERO;
            pOfpPortSt->PhyPort.u4Advertised = OFC_ZERO;
            pOfpPortSt->PhyPort.u4Peer = OFC_ZERO;
            pOfpPortSt->u1Reason = OFPPR_DELETE;
            MEMCPY (pOfpPortSt->PhyPort.au1Name, cfaifInfo.au1IfAliasName,
                    OFC_SIXTEEN);
            MEMCPY (pOfpPortSt->PhyPort.au1HwAddr, cfaifInfo.au1MacAddr,
                    sizeof (pOfpPortSt->PhyPort.au1HwAddr));
#ifdef NPAPI_WANTED
            if (i1action == OFPPR_DELETE)
            {
                OfcHwInfo.OfcHwEntry.OfcHwIfInfo.u4IfIndex = u4IfIndex;
                OfcHwInfo.OfcVer = OFC_VER_100;
                OfcHwInfo.OfcCmd = OFC_NP_PORT_DELETE;
                OFC_TRC ((OFC_UTIL_TRC,
                          "\r\nInvoking NPAPI Port Delete with  ifindex %d",
                          u4IfIndex));
                if (OfcFsNpHwConfigOfcInfo (&OfcHwInfo) != FNP_SUCCESS)
                {
                    OFC_TRC ((OFC_UTIL_TRC,
                              "Np Hw Update Failed for Port Delete - V100.\r\n"));
                }
            }
#endif
            /* 
             * Send Port Create/Delete Notification to Controller, only when port is mapped to 
             * a valid context 
             */
            if (u4IfContext != ((UINT4) OFC_INVALID_CONTEXT))
            {
                if (((OfcInterfacePortStatusEventSend
                      (pOfpPortSt, u4IfContext))) == OFC_FAILURE)
                {
                    MemReleaseMemBlock (OFC_PORT_STATUS_POOLID,
                                        (UINT1 *) pOfpPortSt);
                }
            }
            else
            {
                MemReleaseMemBlock (OFC_PORT_STATUS_POOLID,
                                    (UINT1 *) pOfpPortSt);
            }
        }
        else
        {
            /* Allocate memory for the new node */
            pOfp131PortStat =
                (tOfp131PortStatus *) MemAllocMemBlk (OFC_PORT_STATUS_POOLID);
            if (pOfp131PortStat == NULL)
            {
                OFC_TRC ((OFC_UTIL_TRC,
                          "pOfp131PortStat: Fail to Allocate Memory.\r\n"));
                return OFC_FAILURE;
            }

            MEMSET (pOfp131PortStat, OFC_ZERO, sizeof (tOfp131PortStatus));

            pOfp131PortStat->PortDesc.u4PortNo = u4IfIndex;
            pOfp131PortStat->PortDesc.u4State = cfaifInfo.u1IfOperStatus;
            pOfp131PortStat->PortDesc.u4Config = u4Config;
            pOfp131PortStat->PortDesc.u4Curr = OFC_ZERO;
            pOfp131PortStat->PortDesc.u4Supported = OFC_ZERO;
            pOfp131PortStat->PortDesc.u4Advertised = OFC_ZERO;
            pOfp131PortStat->PortDesc.u4Peer = OFC_ZERO;
            pOfp131PortStat->PortDesc.u4CurrSpeed = cfaifInfo.u4IfSpeed;
            pOfp131PortStat->PortDesc.u4MaxSpeed = cfaifInfo.u4IfHighSpeed;
            pOfp131PortStat->u1Reason = OFPPR_DELETE;
            MEMCPY (pOfp131PortStat->PortDesc.au1HwAddr, cfaifInfo.au1MacAddr,
                    sizeof (pOfp131PortStat->PortDesc.au1HwAddr));

#ifdef NPAPI_WANTED
            if (i1action == OFPPR_DELETE)
            {
                OfcHwInfo.OfcHwEntry.OfcHwIfInfo.u4IfIndex = u4IfIndex;
                OfcHwInfo.OfcVer = OFC_VER_131;
                OfcHwInfo.OfcCmd = OFC_NP_PORT_DELETE;
                OFC_TRC ((OFC_UTIL_TRC,
                          "\r\nInvoking NPAPI Port Delete with  ifindex %d",
                          u4IfIndex));
                if (OfcFsNpHwConfigOfcInfo (&OfcHwInfo) != FNP_SUCCESS)
                {
                    OFC_TRC ((OFC_UTIL_TRC,
                              "Np Hw Update Failed for Port Delete - V131.\r\n"));
                }
            }
#endif
            /* 
             * Send Port Create/Delete Notification to Controller, only when port is mapped to 
             * a valid context 
             */
            if (u4IfContext != ((UINT4) OFC_INVALID_CONTEXT))
            {
                if ((OfcInterfacePortStatusEventSend
                     (pOfp131PortStat, u4IfContext)) == OFC_FAILURE)
                {
                    MemReleaseMemBlock (OFC_PORT_STATUS_POOLID,
                                        (UINT1 *) pOfp131PortStat);
                }
            }
            else
            {
                MemReleaseMemBlock (OFC_PORT_STATUS_POOLID,
                                    (UINT1 *) pOfp131PortStat);
            }
        }
    }
    else
    {
        OFC_TRC ((OFC_UTIL_TRC, "\r\nInvalid Action Specified,"
                  " Return FAILURE\n"));
        return OFC_FAILURE;
    }

    OFC_TRC_FUNC ((OFC_FN_EXIT, "EXIT :: OpenflowNotifyPortCreateDelete \n"));
    return OFC_SUCCESS;
}

/******************************************************************************
* Function    :  OpenflowNotifyPktRecv
* Input       :  u4IfIndex
*                pBuf
* Description :  This Routine is called when a new packet arrives at CFA
* Output      :  None
* Returns     :  OFC_SUCCESS or OFC_FAILURE
* **************************************************************************/
INT4
OpenflowNotifyPktRecv (UINT4 u4IfIndex, tCRU_BUF_CHAIN_HEADER * pBuf)
{
    OFC_TRC_FUNC ((OFC_FN_ENTRY, "Entry :: OpenflowNotifyPktRecv \n"));

    if ((OfcInterfacePipelineProcessEventSend (u4IfIndex, pBuf)) == OFC_FAILURE)
    {
        OFC_TRC_FUNC ((OFC_FN_EXIT, "EXIT :: OpenflowNotifyPktRecv \n"));
        return OFC_FAILURE;
    }

    OFC_TRC_FUNC ((OFC_FN_EXIT, "EXIT :: OpenflowNotifyPktRecv \n"));
    return OFC_SUCCESS;
}

/*******************************************************************************************
* Function    :  OfcInterfacePortStatusEventSend
* Input       :  OfpPortStatus
* Description :  This Routine is used to send  Port Status packet send Event to OFC Task 
* Output      :  None
* Returns     :  OFC_SUCCESS or OFC_FAILURE
********************************************************************************************/
INT4
OfcInterfacePortStatusEventSend (VOID *pPortStatus, UINT4 u4ContextId)
{
    tOfcQueMsg         *pOfcPortMsgInfo = NULL;
    tOfcFsofcCfgEntry  *pOfcFsofcCfgEntry = NULL;

    pOfcFsofcCfgEntry = (tOfcFsofcCfgEntry *)
        RBTreeGetFirst (gOfcGlobals.OfcGlbMib.FsofcCfgTable);
    if (pOfcFsofcCfgEntry == NULL)
    {
        OFC_TRC_FUNC ((OFC_UTIL_TRC, "\r\nOfcInterfacePortStatusEventSend: "
                       "Get Default CFG Entry Failed\n"));
        return OFC_FAILURE;
    }

    pOfcPortMsgInfo = (tOfcQueMsg *) MemAllocMemBlk (OFC_QUEUE_POOLID);
    if (pOfcPortMsgInfo == NULL)
    {
        OFC_TRC ((OFC_UTIL_TRC, "\r\nOfcInterfacePortStatusEventSend: "
                  "Mem Allocation Failed\n"));
        return OFC_FAILURE;
    }

    MEMSET (pOfcPortMsgInfo, OFC_ZERO, sizeof (tOfcQueMsg));

    pOfcPortMsgInfo->u4MsgType = OFC_PORT_STATUS_SEND;
    pOfcPortMsgInfo->pPortStat = pPortStatus;
    pOfcPortMsgInfo->u4ContextId = u4ContextId;

    if (OsixQueSend (gOfcGlobals.ofcQueId, (UINT1 *) &(pOfcPortMsgInfo),
                     OSIX_DEF_MSG_LEN) == OSIX_FAILURE)
    {
        OFC_TRC ((OFC_UTIL_TRC, "\r\nOfcInterfacePortStatusEventSend: "
                  "Queue send failed\r\n"));
        MemReleaseMemBlock (OFC_QUEUE_POOLID, (UINT1 *) pOfcPortMsgInfo);
        return OFC_FAILURE;
    }

    if (OsixEvtSend (gOfcGlobals.ofcTaskId, OFC_QUEUE_EVENT) == OSIX_FAILURE)
    {
        OFC_TRC ((OFC_UTIL_TRC, "\r\nOfcInterfacePortStatusEventSend: "
                  "Unable to post event to OFC \n"));
        MemReleaseMemBlock (OFC_QUEUE_POOLID, (UINT1 *) pOfcPortMsgInfo);
        return OFC_FAILURE;
    }

    OFC_TRC_FUNC ((OFC_FN_EXIT, "EXIT :: OfcInterfacePortStatusEventSend \n"));
    KW_FALSEPOSITIVE_FIX (pOfcPortMsgInfo);
    return OFC_SUCCESS;
}

/*******************************************************************************************
* Function    :  OfcInterfacePipelineProcessEventSend
* Input       :  u4IfIndex
*                pBuf
* Description :  This Routine is used to send packet reception Event to OFC Task
* Output      :  None
* Returns     :  OFC_SUCCESS or OFC_FAILURE
* ********************************************************************************************/
INT4
OfcInterfacePipelineProcessEventSend (UINT4 u4IfIndex,
                                      tCRU_BUF_CHAIN_HEADER * pBuf)
{
    tOfcQueMsg         *pOfcPipelineMsg = NULL;
    UINT4               u4ContextId = OFC_ZERO;

    OFC_TRC_FUNC ((OFC_FN_ENTRY,
                   "Entry :: OfcInterfacePipelineProcessEventSend \n"));

    pOfcPipelineMsg = (tOfcQueMsg *) MemAllocMemBlk (OFC_QUEUE_POOLID);

    if (pOfcPipelineMsg == NULL)
    {
        OFC_TRC ((OFC_UTIL_TRC, "\r\nOfcInterfacePipelineProcessEventSend: "
                  "Memory Allocation Failed for pOfcPipelineMsg\n"));
        return OFC_FAILURE;
    }

    MEMSET (pOfcPipelineMsg, OFC_ZERO, sizeof (tOfcQueMsg));

    pOfcPipelineMsg->u4MsgType = OFC_PIPELINE_NOTIFY_SEND;
    pOfcPipelineMsg->u4IfIndex = u4IfIndex;

    /* Get the Context ID for the InPut IfIndex */
    if (OfcGetContextIdFromIfIndex (u4IfIndex, &u4ContextId) != OFC_SUCCESS)
    {
        /* port might be mapped to hybrid */
        if (OfcHybridRxPktFromIssCheck (u4IfIndex, &u4ContextId) == OFC_SUCCESS)
        {
            pOfcPipelineMsg->u4IfIndex = OFPP_LOCAL;
        }
        else
        {
            MemReleaseMemBlock (OFC_QUEUE_POOLID, (UINT1 *) pOfcPipelineMsg);
            OFC_TRC ((OFC_UTIL_TRC, "\r\n Get Context ID failed\r\n"));
            return OFC_FAILURE;
        }
    }
    pOfcPipelineMsg->u4ContextId = u4ContextId;
    pOfcPipelineMsg->pBuf = pBuf;

    if (OsixQueSend (gOfcGlobals.ofcQueId, (UINT1 *) &(pOfcPipelineMsg),
                     OSIX_DEF_MSG_LEN) == OSIX_FAILURE)
    {
        MemReleaseMemBlock (OFC_QUEUE_POOLID, (UINT1 *) pOfcPipelineMsg);
        OFC_TRC ((OFC_UTIL_TRC, "\r\nOfcInterfacePipelineProcessEventSend: "
                  "Queue send failed\r\n"));
        return OFC_FAILURE;
    }

    if (OsixEvtSend (gOfcGlobals.ofcTaskId, OFC_QUEUE_EVENT) == OSIX_FAILURE)
    {
        OFC_TRC ((OFC_UTIL_TRC, "\r\nOfcInterfacePipelineProcessEventSend: "
                  "Unable to post event to OFC \n"));
        MemReleaseMemBlock (OFC_QUEUE_POOLID, (UINT1 *) pOfcPipelineMsg);
        return OFC_FAILURE;
    }

    OFC_TRC_FUNC ((OFC_FN_EXIT,
                   "EXIT :: OfcInterfacePipelineProcessEventSend \n"));
    KW_FALSEPOSITIVE_FIX (pOfcPipelineMsg);
    return OFC_SUCCESS;
}

/*******************************************************************************************
* Function    :  OfcInterfacePacketSend
* Input       :  u4IfIndex
*                pBuf
* Description :  This Routine is used to send packet out
* Output      :  None
* Returns     :  OFC_SUCCESS or OFC_FAILURE
* ********************************************************************************************/
INT4
OfcInterfacePacketSend (tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 u4IfIndex,
                        UINT4 u4PktSize)
{
    tCfaIfInfo          CfaIfInfo;
    UINT2               u2Port = OFC_ZERO;
    UINT2               u2ByteInd = OFC_ZERO;
    UINT2               u2BitIndex = OFC_ZERO;
    UINT1               u1PortFlag = OFC_ZERO;
    UINT1              *pu1Ports = NULL;
    INT4                i4RetVal = OSIX_SUCCESS;
    tOfcFsofcIfEntry    lOfcFsofcIfEntry;
    tOfcFsofcIfEntry   *pOfcFsofcIfEntry = NULL;
    tOfcFsofcCfgEntry  *pOfcFsofcCfgEntry = NULL;
    MEMSET (&CfaIfInfo, OFC_ZERO, sizeof (CfaIfInfo));

    pOfcFsofcCfgEntry = (tOfcFsofcCfgEntry *)
        RBTreeGetFirst (gOfcGlobals.OfcGlbMib.FsofcCfgTable);
    if (pOfcFsofcCfgEntry == NULL)
    {
        OFC_TRC_FUNC ((OFC_UTIL_TRC, "\r\nOfcInterfacePacketSend: "
                       "Get CFG Entry Failed\n"));
        return OFC_FAILURE;
    }

    MEMSET (pOfcFsofcCfgEntry->pDataBuf, OFC_ZERO, OFC_MAX_CONTROLLER_PKT_LEN);
    if (CfaGetIfInfo (u4IfIndex, &CfaIfInfo) == CFA_FAILURE)
    {
        return OFC_FAILURE;
    }
    if (CRU_BUF_Copy_FromBufChain (pBuf, pOfcFsofcCfgEntry->pDataBuf,
                                   OFC_ZERO, u4PktSize) == CRU_FAILURE)
    {
        OFC_TRC_FUNC ((OFC_UTIL_TRC, "\r\nOfcInterfacePacketSend: "
                       "CRU Copy Failed, return FAILURE\n"));
        return OFC_FAILURE;
    }

    if ((CfaIfInfo.u1IfType == CFA_L2VLAN)
        && (CfaIfInfo.u1IfSubType == CFA_SUBTYPE_OPENFLOW_INTERFACE))
    {
        /* Update Vlan Statistics */
        lOfcFsofcIfEntry.MibObject.u4FsofcContextId = OFC_DEFAULT_CONTEXT;
        lOfcFsofcIfEntry.MibObject.i4FsofcIfIndex = (INT4) (u4IfIndex);
        pOfcFsofcIfEntry = RBTreeGet (gOfcGlobals.OfcGlbMib.FsofcIfTable,
                                      (tRBElem *) & (lOfcFsofcIfEntry));

        if (pOfcFsofcIfEntry == NULL)
        {
            OFC_TRC_FUNC ((OFC_UTIL_TRC, "\r\nOfcInterfacePacketSend: "
                           "No Entry available in OfcIfTable\n"));
            return OFC_FAILURE;

        }

        pu1Ports = pOfcFsofcIfEntry->MibObject.au1FsofcVlanEgressPorts;
        for (u2ByteInd = OFC_ZERO; u2ByteInd < CONTEXT_PORT_LIST_SIZE;
             u2ByteInd++)
        {
            if (pu1Ports[u2ByteInd] == OFC_ZERO)
            {
                continue;
            }
            u1PortFlag = pu1Ports[u2ByteInd];

            for (u2BitIndex = OFC_ZERO;
                 ((u2BitIndex < OFC_VLAN_PORTS_PER_BYTE)
                  && (u1PortFlag != OFC_ZERO)); u2BitIndex++)
            {
                if ((u1PortFlag & OFC_VLAN_BIT8) == OFC_ZERO)
                {
                    u1PortFlag = (UINT1) (u1PortFlag << OFC_ONE);
                    continue;
                }
                u2Port =
                    (UINT2) ((u2ByteInd * OFC_VLAN_PORTS_PER_BYTE) +
                             u2BitIndex + OFC_ONE);

                if (u2Port >= OFC_VLAN_MAX_PORTS + OFC_ONE)
                {
                    break;
                }
                i4RetVal =
                    CfaGddEthWrite (pOfcFsofcCfgEntry->pDataBuf, u2Port,
                                    u4PktSize);
                u1PortFlag = (UINT1) (u1PortFlag << OFC_ONE);
            }
        }
        pOfcFsofcIfEntry->MibObject.u4FsofcVlanOutFrames++;
    }
    else
    {
        i4RetVal =
            CfaGddEthWrite (pOfcFsofcCfgEntry->pDataBuf, (UINT2) u4IfIndex,
                            u4PktSize);
    }
    return i4RetVal;
}
