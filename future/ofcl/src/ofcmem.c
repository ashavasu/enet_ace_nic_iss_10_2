/*****************************************************************************
 * Copyright (C) 2013 Aricent Inc . All Rights Reserved
 *
 * $Id: ofcmem.c,v 1.3 2015/02/27 10:54:25 siva Exp $
 *
 * Description: This file contains routines memory allocations per table
 *****************************************************************************/

#include "ofcdefn.h"
#include "ofcapi.h"
#include "ofcsz.h"

/*****************************************************************************
 * Function    : OfcMemFreeForFlowEntry 
 * Description : This routine will free memory allocated for flowentry
 * Input       : u1TableId - Table Identifier
 *               pMemPool  - Pointer to flow entry mempool
 * Output      : None 
 * Returns     : None 
 *****************************************************************************/
VOID
OfcMemFreeForFlowEntry (UINT1 u1TableId, UINT1 *pMemPool)
{
    UINT4               u4PoolId = OFC_ZERO;

    OFC_TRC_FUNC ((OFC_FN_ENTRY, "OfcMemFreeForFlowEntry Entry\n"));

    /*
     * If the TableId falls into category of 1 to 9, free the
     * mempools from its respective mempool else from the default
     * mempool.
     */
    if (u1TableId > OFC_NINE)
    {
        u1TableId = OFC_ZERO;
    }

    u4PoolId = OFCMemPoolIds[MAX_OFC_FSOFCFLOWTABLE_SIZING_ID + u1TableId];
    MemReleaseMemBlock (u4PoolId, pMemPool);

    OFC_TRC_FUNC ((OFC_FN_EXIT, "OfcMemFreeForFlowEntry Exit\n"));
}

/*****************************************************************************
 * Function    : OfcMemFreeForFlowMatch 
 * Description : This routine will free memory allocated for match field 
 * Input       : i1TableId - Table Identifier
 *               pMemPool  - Pointer to flow match mempool
 * Output      : None 
 * Returns     : None 
 *****************************************************************************/
VOID
OfcMemFreeForFlowMatch (INT1 i1TableId, UINT1 *pMemPool)
{
    UINT4               u4PoolId = OFC_ZERO;

    OFC_TRC_FUNC ((OFC_FN_ENTRY, "OfcMemFreeForFlowMatch Entry\n"));

    /*
     * If the TableId falls into category of 1 to 9, free the
     * mempools from its respective mempool else from the default
     * mempool.
     */
    if (i1TableId > OFC_NINE)
    {
        i1TableId = OFC_ZERO;
    }

    u4PoolId = OFCMemPoolIds[MAX_OFC_FLOW_MATCH_SIZING_ID + i1TableId];
    MemReleaseMemBlock (u4PoolId, pMemPool);

    OFC_TRC_FUNC ((OFC_FN_EXIT, "OfcMemFreeForFlowMatch Exit\n"));
}

/*****************************************************************************
 * Function    : OfcMemFreeForInstruction 
 * Description : This routine will free memory allocated for instruction
 * Input       : u1TableId - Table Identifier
 *               pMemPool  - Pointer to instruction mempool
 * Output      : None 
 * Returns     : None 
 *****************************************************************************/
VOID
OfcMemFreeForInstruction (UINT1 u1TableId, UINT1 *pMemPool)
{
    UINT4               u4PoolId = OFC_ZERO;

    OFC_TRC_FUNC ((OFC_FN_ENTRY, "OfcMemFreeForInstruction Entry\n"));

    /*
     * If the TableId falls into category of 1 to 9, free the
     * mempools from its respective mempool else from the default
     * mempool.
     */
    if (u1TableId > OFC_NINE)
    {
        u1TableId = OFC_ZERO;
    }

    u4PoolId = OFCMemPoolIds[MAX_OFC_INSTRUCTIONS_SIZING_ID + u1TableId];
    MemReleaseMemBlock (u4PoolId, pMemPool);

    OFC_TRC_FUNC ((OFC_FN_EXIT, "OfcMemFreeForInstruction Exit\n"));
}

/*****************************************************************************
 * Function    : OfcMemFreeForAction 
 * Description : This routine will free memory allocated for action 
 * Input       : u1TableId - Table Identifier
 *               pMemPool  - Pointer to action mempool
 * Output      : None 
 * Returns     : None 
 *****************************************************************************/
VOID
OfcMemFreeForAction (UINT1 u1TableId, UINT1 *pMemPool)
{
    UINT4               u4PoolId = OFC_ZERO;

    OFC_TRC_FUNC ((OFC_FN_ENTRY, "OfcMemFreeForAction Entry\n"));

    /*
     * If the TableId falls into category of 1 to 9, free the
     * mempools from its respective mempool else from the default
     * mempool.
     */
    if (u1TableId > OFC_NINE)
    {
        u1TableId = OFC_ZERO;
    }

    u4PoolId = OFCMemPoolIds[MAX_OFC_ACTIONS_SIZING_ID + u1TableId];
    MemReleaseMemBlock (u4PoolId, pMemPool);

    OFC_TRC_FUNC ((OFC_FN_EXIT, "OfcMemFreeForAction Exit\n"));
}

/*****************************************************************************
 * Function    : OfcMemFreeForSetField 
 * Description : This routine will free memory allocated for set field 
 * Input       : u1TableId - Table Identifier
 *               pMemPool  - Pointer to SetField mempool
 * Output      : None 
 * Returns     : None 
 *****************************************************************************/
VOID
OfcMemFreeForSetField (UINT1 u1TableId, UINT1 *pMemPool)
{
    UINT4               u4PoolId = OFC_ZERO;

    OFC_TRC_FUNC ((OFC_FN_ENTRY, "OfcMemFreeForSetField Entry\n"));

    /*
     * If the TableId falls into category of 1 to 9, free the
     * mempools from its respective mempool else from the default
     * mempool.
     */
    if (u1TableId > OFC_NINE)
    {
        u1TableId = OFC_ZERO;
    }

    u4PoolId = OFCMemPoolIds[MAX_OFC_SET_FIELD_SIZING_ID + u1TableId];
    MemReleaseMemBlock (u4PoolId, pMemPool);

    OFC_TRC_FUNC ((OFC_FN_EXIT, "OfcMemFreeForSetField Exit\n"));
}

/*****************************************************************************
 * Function    : OfcMemAllocForFlowEntry 
 * Description : This routine will allocate memory for flow entry 
 * Input       : u1TableId  - Table Identifier
 * Output      : None
 * Returns     : pFlowEntry - pointer to the mempool for flow entry 
 *****************************************************************************/
tOfcFsofcFlowEntry *
OfcMemAllocForFlowEntry (UINT1 u1TableId)
{
    tOfcFsofcFlowEntry *pFlowEntry = NULL;
    UINT4               u4PoolId = OFC_ZERO;

    OFC_TRC_FUNC ((OFC_FN_ENTRY, "OfcMemAllocForFlowEntry Entry\n"));

    /*
     * If the TableId falls into category of 1 to 9, allocate the
     * mempools from its respective mempool else from the default
     * mempool.
     */
    if (u1TableId > OFC_NINE)
    {
        u1TableId = OFC_ZERO;
    }

    u4PoolId = OFCMemPoolIds[MAX_OFC_FSOFCFLOWTABLE_SIZING_ID + u1TableId];
    pFlowEntry = (tOfcFsofcFlowEntry *) MemAllocMemBlk (u4PoolId);

    OFC_TRC_FUNC ((OFC_FN_EXIT, "OfcMemAllocForFlowEntry Exit\n"));
    return pFlowEntry;
}

/*****************************************************************************
 * Function    : OfcMemAllocForFlowMatch 
 * Description : This routine will allocate memory for flow match 
 * Input       : u1TableId  - Table Identifier
 * Output      : None
 * Returns     : pFlowMatch - pointer to the mempool for match field
 *****************************************************************************/
tOfcWcFlowMatch    *
OfcMemAllocForFlowMatch (INT1 i1TableId)
{
    tOfcWcFlowMatch    *pFlowMatch = NULL;
    UINT4               u4PoolId = OFC_ZERO;

    OFC_TRC_FUNC ((OFC_FN_ENTRY, "OfcMemAllocForFlowMatch Entry\n"));

    /*
     * If the TableId falls into category of 1 to 9, allocate the
     * mempools from its respective mempool else from the default
     * mempool.
     */
    if (i1TableId > OFC_NINE)
    {
        i1TableId = OFC_ZERO;
    }

    u4PoolId = OFCMemPoolIds[MAX_OFC_FLOW_MATCH_SIZING_ID + i1TableId];
    pFlowMatch = (tOfcWcFlowMatch *) MemAllocMemBlk (u4PoolId);

    OFC_TRC_FUNC ((OFC_FN_EXIT, "OfcMemAllocForFlowMatch Exit\n"));
    return pFlowMatch;
}

/*****************************************************************************
 * Function    : OfcMemAllocForInstruction 
 * Description : This routine will allocate memory for instruction
 * Input       : u1TableId  - Table Identifier
 * Output      : None
 * Returns     : pInstr - pointer to the mempool for instruction 
 *****************************************************************************/
tOfcInstr          *
OfcMemAllocForInstruction (UINT1 u1TableId)
{
    tOfcInstr          *pInstr = NULL;
    UINT4               u4PoolId = OFC_ZERO;

    OFC_TRC_FUNC ((OFC_FN_ENTRY, "OfcMemAllocForInstruction Entry\n"));

    /*
     * If the TableId falls into category of 1 to 9, allocate the
     * mempools from its respective mempool else from the default
     * mempool.
     */
    if (u1TableId > OFC_NINE)
    {
        u1TableId = OFC_ZERO;
    }

    u4PoolId = OFCMemPoolIds[MAX_OFC_INSTRUCTIONS_SIZING_ID + u1TableId];
    pInstr = (tOfcInstr *) MemAllocMemBlk (u4PoolId);

    OFC_TRC_FUNC ((OFC_FN_EXIT, "OfcMemAllocForInstruction Exit\n"));
    return pInstr;
}

/*****************************************************************************
 * Function    : OfcMemAllocForAction 
 * Description : This routine will allocate memory for action 
 * Input       : u1TableId  - Table Identifier
 * Output      : None
 * Returns     : pAct - pointer to the mempool for action 
 *****************************************************************************/
tOfcActs           *
OfcMemAllocForAction (UINT1 u1TableId)
{
    tOfcActs           *pAct = NULL;
    UINT4               u4PoolId = OFC_ZERO;

    OFC_TRC_FUNC ((OFC_FN_ENTRY, "OfcMemAllocForAction Entry\n"));

    /*
     * If the TableId falls into category of 1 to 9, allocate the
     * mempools from its respective mempool else from the default
     * mempool.
     */
    if (u1TableId > OFC_NINE)
    {
        u1TableId = OFC_ZERO;
    }

    u4PoolId = OFCMemPoolIds[MAX_OFC_ACTIONS_SIZING_ID + u1TableId];
    pAct = (tOfcActs *) MemAllocMemBlk (u4PoolId);

    OFC_TRC_FUNC ((OFC_FN_EXIT, "OfcMemAllocForAction Exit\n"));
    return pAct;
}

/*****************************************************************************
 * Function    : OfcMemAllocForSetField 
 * Description : This routine will allocate memory for set field 
 * Input       : u1TableId  - Table Identifier
 * Output      : None
 * Returns     : pSetFld - pointer to the mempool for set field 
 *****************************************************************************/
tOfcSetFld         *
OfcMemAllocForSetField (UINT1 u1TableId)
{
    tOfcSetFld         *pSetFld = NULL;
    UINT4               u4PoolId = OFC_ZERO;

    OFC_TRC_FUNC ((OFC_FN_ENTRY, "OfcMemAllocForSetField Entry\n"));

    /*
     * If the TableId falls into category of 1 to 9, allocate the
     * mempools from its respective mempool else from the default
     * mempool.
     */
    if (u1TableId > OFC_NINE)
    {
        u1TableId = OFC_ZERO;
    }

    u4PoolId = OFCMemPoolIds[MAX_OFC_SET_FIELD_SIZING_ID + u1TableId];
    pSetFld = (tOfcSetFld *) MemAllocMemBlk (u4PoolId);

    OFC_TRC_FUNC ((OFC_FN_EXIT, "OfcMemAllocForSetField Exit\n"));
    return pSetFld;
}

/*****************************************************************************
 * Function    : OfcActOutputPortListFree 
 * Description : This routine will release the memory of output port list 
 * Input       : pActOutPortList - Output port SLL
 * Output      : None
 * Returns     : None
 *****************************************************************************/
VOID
OfcActOutputPortListFree (tOfcSll * pActOutPortList)
{
    tOfcActOutputPort  *pActNextOutPort = NULL;
    tOfcActOutputPort  *pActOutput = NULL;

    if (TMO_SLL_Count (pActOutPortList) == 0)
    {
        return;
    }

    TMO_DYN_SLL_Scan (pActOutPortList, pActOutput, pActNextOutPort,
                      tOfcActOutputPort *)
    {
        TMO_SLL_Delete (pActOutPortList, (tTMO_SLL_NODE *) pActOutput);
        MemReleaseMemBlock (OFC_OUT_PORT_POOLID, (UINT1 *) pActOutput);
    }
    return;
}
