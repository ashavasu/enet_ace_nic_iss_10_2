/********************************************************************
 * Copyright (C) 2013 Aricent Inc . All Rights Reserved
 *
 * $Id: ofc131pkt.c,v 1.21 2018/01/11 11:18:35 siva Exp $
 *
 * Description: This file contains the Ofc131 pkt process related routines
 *****************************************************************************/

#include "ofcinc.h"
#include "ofccli.h"
#include "ofc131pkt.h"
#include "ofcintf.h"
#include "ofcflow.h"
#include "ofcsz.h"
#include "ofcgroup.h"
#include "fssocket.h"
#include "ofcmpmsg.h"
#include "ofcacts.h"
#include "ofcapi.h"
#include "ofcmeter.h"
#include "fsssl.h"

#ifdef DPA_WANTED
#include "ofc131dpa.h"
#endif /* DPA_WANTED */

static UINT1        gau1DataBuf[OFC131_MAX_PKT_LEN];
extern UINT4        gu4FlowType;

/*****************************************************************************
 * Function    :  Ofc131PktActsValidate
 * Description :  This routine is to validate the actions and set fields 
 * Input       :  pFlowTable   - Pointer to the flow table entry 
 *                u2Type       - Actions type, list or set
 *                pActionHdr   - Pointer to actions header
 *                u2ActsLength - Length of the actions
 * Output      :  None 
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE 
 *****************************************************************************/
PRIVATE UINT4
Ofc131PktActsValidate (tOfcFsofcFlowTable * pFlowTable, UINT2 u2Type,
                       tOfp131ActionHdr * pActionHdr, UINT2 u2ActsLength,
                       UINT4 *pu4Error)
{
    FS_UINT8            u8SetFieldsBitMap;
    tOxmTlv            *pSetFldTlv = NULL;
    UINT4               u4ActionsBitMap = OFC_ZERO;
    UINT2               u2ActType = OFC_ZERO;
    UINT2               u2SetFldLen = OFC_ZERO;
    UINT1               u1Field = OFC_ZERO;
    UINT1               u1OxmLen = OFC_ZERO;

    OFC131_TRC_FUNC ((OFC131_FN_ENTRY, "FUNC:Ofc131PktActsValidate\n"));

    MEMSET (&u8SetFieldsBitMap, OFC_ZERO, OFC_EIGHT);
    while (u2ActsLength > OFC_ZERO)
    {
        u2ActType = OSIX_NTOHS (pActionHdr->u2Type);
        if (u2ActType &&
            ((u2ActType < OFC_ELEVEN) || (u2ActType > OFC_TWENTY_SEVEN)))
        {
            *pu4Error = OFC131_FLOWMOD_ACT_VALIDATE_ERR;
            return OFC_FAILURE;
        }
        else
        {
            if (u2ActType == OFCAT_SET_FIELD)
            {
                /* Parsing Set fields */
                pSetFldTlv = (tOxmTlv *) (VOID *)
                    (((UINT1 *) (pActionHdr)) + OFC_FOUR);
                u2SetFldLen =
                    (UINT2) (OSIX_NTOHS (pActionHdr->u2Len) - OFC_FOUR);
                while (u2SetFldLen > OFC_ZERO)
                {
                    if (pSetFldTlv->u2Class == OFC_ZERO)    /* possible padding */
                    {
                        break;
                    }

                    if (pSetFldTlv->u2Class !=
                        OSIX_HTONS (OFPXMC_OPENFLOW_BASIC))
                    {
                        *pu4Error = OFC131_FLOWMOD_SETFIELD_VALIDATE_ERR;
                        return OSIX_FAILURE;
                    }

                    u1Field = pSetFldTlv->u1Field >> OFC_ONE;
                    if (u1Field > OFC_THIRTY_NINE || u1Field < OFC_THREE)
                    {
                        *pu4Error = OFC131_FLOWMOD_SETFIELD_VALIDATE_ERR;
                        return OSIX_FAILURE;
                    }
                    else if (u1Field > OFC_THIRTY_ONE)
                    {
                        u8SetFieldsBitMap.u4Hi |= (UINT4) (OFC_ONE << (u1Field -
                                                                       OFC_THIRTY_TWO));
                    }
                    else
                    {
                        u8SetFieldsBitMap.u4Lo |= (UINT4) (OFC_ONE << u1Field);
                    }
                    u1OxmLen = pSetFldTlv->u1Len;

                    if (u2SetFldLen < (sizeof (tOxmTlv) + u1OxmLen))
                    {
                        /* OFPBAC_BAD_SET_LEN */
                        *pu4Error = OFC131_FLOWMOD_ACT_SETLEN_VALIDATE_ERR;
                        return OSIX_FAILURE;
                    }
                    u2SetFldLen = (UINT2) (u2SetFldLen - (sizeof (tOxmTlv) +
                                                          u1OxmLen));
                    pSetFldTlv = (tOxmTlv *) (VOID *) (((UINT1 *) pSetFldTlv) +
                                                       (sizeof (tOxmTlv) +
                                                        u1OxmLen));
                }

                if (u2Type == OFCIT_WRITE_ACTIONS)
                {
                    if (((u8SetFieldsBitMap.u4Hi &
                          (pFlowTable->u8WrtStFldsBitMap.u4Hi)) !=
                         u8SetFieldsBitMap.u4Hi) ||
                        ((u8SetFieldsBitMap.u4Lo &
                          (pFlowTable->u8WrtStFldsBitMap.u4Lo)) !=
                         u8SetFieldsBitMap.u4Lo))
                    {
                        OFC131_TRC_FUNC ((OFC_PKT_TRC, "Write Set fields"
                                          " validation failed\n"));
                        *pu4Error = OFC131_FLOWMOD_SETFIELD_VALIDATE_ERR;
                        return OSIX_FAILURE;
                    }
                }
                else if (u2Type == OFCIT_APPLY_ACTIONS)
                {
                    if (((u8SetFieldsBitMap.u4Hi &
                          (pFlowTable->u8AppStFldsBitMap.u4Hi)) !=
                         u8SetFieldsBitMap.u4Hi) ||
                        ((u8SetFieldsBitMap.u4Lo &
                          (pFlowTable->u8AppStFldsBitMap.u4Lo)) !=
                         u8SetFieldsBitMap.u4Lo))
                    {
                        OFC131_TRC_FUNC ((OFC_PKT_TRC, "Apply Set fields"
                                          " validation failed\n"));
                        *pu4Error = OFC131_FLOWMOD_SETFIELD_VALIDATE_ERR;
                        return OSIX_FAILURE;
                    }
                }
            }
        }
        u4ActionsBitMap |= (UINT4) (OFC_ONE << u2ActType);
        if (u2ActsLength < OSIX_NTOHS (pActionHdr->u2Len))
        {
            /* OFPBAC_BAD_LEN */
            *pu4Error = OFC131_FLOWMOD_ACT_LEN_VALIDATE_ERR;
            return OSIX_FAILURE;
        }
        u2ActsLength =
            (UINT2) (u2ActsLength - (OSIX_NTOHS (pActionHdr->u2Len)));
        pActionHdr =
            (tOfp131ActionHdr *) (VOID *) (((UINT1 *) pActionHdr) +
                                           OSIX_NTOHS (pActionHdr->u2Len));
    }

    if (u2Type == OFCIT_WRITE_ACTIONS)
    {
        if ((u4ActionsBitMap &
             (pFlowTable->u4WrtActsBitMap)) != u4ActionsBitMap)
        {
            OFC131_TRC_FUNC ((OFC_PKT_TRC,
                              "Write Actions Validation failed\n"));
            *pu4Error = OFC131_FLOWMOD_ACT_VALIDATE_ERR;
            return OSIX_FAILURE;
        }
    }
    else if (u2Type == OFCIT_APPLY_ACTIONS)
    {
        if ((u4ActionsBitMap &
             (pFlowTable->u4AppActsBitMap)) != u4ActionsBitMap)
        {
            OFC131_TRC_FUNC ((OFC_PKT_TRC,
                              "Apply Actions Validation failed\n"));
            *pu4Error = OFC131_FLOWMOD_ACT_VALIDATE_ERR;
            return OSIX_FAILURE;
        }
    }
    OFC131_TRC_FUNC ((OFC131_FN_EXIT, "FUNC:Ofc131PktActsValidate\n"));
    return OFC_SUCCESS;
}

/*****************************************************************************
 * Function    :  Ofc131PktOfpu1VersionValidate
 * Description :  This function is for openflow message protocol version 
 *                validation 
 * Input       :  pOfp131Pkt  - Pointer to the packet received
 *                pConnPtr    - Connection Pointer
 *                pBuf        - CRU Buffer Pointer
 * Output      :  pu4Error    - Error on failure 
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE 
 *****************************************************************************/
INT4
Ofc131PktOfpu1VersionValidate (tOfp131Pkt * pOfp131Pkt, VOID *pConnPtr,
                               tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 *pu4Error)
{
    UINT2               u2Type = OFC_ZERO;
    UINT4               u4BitMap = OFC_ZERO;

    OFC131_TRC_FUNC ((OFC131_FN_ENTRY, "FUNC:Ofc131Ofpu1VersionValidate\n"));

    /* except for hello, all other commands should have 131 version */
    if ((pOfp131Pkt->u1Type != OFC_ZERO) &&
        (pOfp131Pkt->u1Version != OFP131_VERSION))
    {
        OFC131_TRC_FUNC ((OFC_PKT_TRC, "Invalid Version = %d\n",
                          pOfp131Pkt->u1Version));
        *pu4Error = OFC131_OFP_U1VERSION_VALIDATE_ERR;
        return OSIX_FAILURE;
    }

    if (pOfp131Pkt->u1Version < OFP131_VERSION)
    {
        OFC131_TRC_FUNC ((OFC_PKT_TRC, "Hello Failed, Version = %d\n",
                          pOfp131Pkt->u1Version));
        *pu4Error = OFC131_OFP_U1VERSION_VALIDATE_ERR;
        return OSIX_FAILURE;
    }

    /* for higher versions, check for the bit map */
    if (pOfp131Pkt->u1Version > OFP131_VERSION)
    {
        /* request should have atleast one bit map */
        if (pOfp131Pkt->u2Length < (OFC131_HELLO_BM_OFFSET + OFC_FOUR))
        {
            /* No Bitmaps */
            OFC131_TRC_FUNC ((OFC_PKT_TRC, "No Hello Bitmaps \n"));
            *pu4Error = OFC131_OFP_U1VERSION_VALIDATE_ERR;
            return OSIX_FAILURE;
        }

        OFC131_GET_2_BYTE (pBuf, OFC131_HELLO_OFFSET, u2Type);
        if (u2Type != OFPHET_VERSIONBITMAP)
        {
            /* Invalid type in the hello elem header */
            OFC131_TRC_FUNC ((OFC_PKT_TRC, "Invalid Hello Elem Header \n"));
            *pu4Error = OFC131_OFP_U1VERSION_VALIDATE_ERR;
            return OSIX_FAILURE;
        }

        OFC131_GET_4_BYTE (pBuf, OFC131_HELLO_BM_OFFSET, u4BitMap);
        if (!(u4BitMap & OFC131_HLO_MASK))
        {
            /* Not in supported list */
            OFC131_TRC_FUNC ((OFC_PKT_TRC, "Not in supported list \n"));
            *pu4Error = OFC131_OFP_U1VERSION_VALIDATE_ERR;
            return OSIX_FAILURE;
        }
    }
    UNUSED_PARAM (pConnPtr);
    OFC131_TRC_FUNC ((OFC131_FN_EXIT, "FUNC:Ofc131Ofpu1VersionValidate\n"));
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function    :  Ofc131PktOfpu1TypeValidate
 * Description :  This function is for openflow message type validation 
 * Input       :  pOfp131Pkt - Pointer to the packet received
 *                pconnPtr   - Connection Pointer
 *                pBuf       - CRU Buffer Pointer
 * Output      :  pu4Error   - Error on failure 
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE 
 *****************************************************************************/
INT4
Ofc131PktOfpu1TypeValidate (tOfp131Pkt * pOfp131Pkt, VOID *pConnPtr,
                            tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 *pu4Error)
{
    OFC131_TRC_FUNC ((OFC131_FN_ENTRY, "FUNC:Ofc131Ofpu1TypeValidate\n"));

    if (pOfp131Pkt->u1Type > OFC_TWENTY_NINE)
    {
        OFC131_TRC_FUNC ((OFC_PKT_TRC, "Message Type Validation Failed = %d\n",
                          pOfp131Pkt->u1Type));
        *pu4Error = OFC131_OFP_U1TYPE_VALIDATE_ERR;
        return OSIX_FAILURE;
    }
    UNUSED_PARAM (pu4Error);
    UNUSED_PARAM (pConnPtr);
    UNUSED_PARAM (pBuf);
    OFC131_TRC_FUNC ((OFC131_FN_EXIT, "FUNC:Ofc131Ofpu1TypeValidate\n"));
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function    :  Ofc131PktOfpu2LengthValidate
 * Description :  This function is for openflow message length validation 
 * Input       :  pOfp131Pkt - Pointer to the packet received
 *                pConnPtr   - Connection Pointer
 *                pBuf       - CRU Buffer Pointer
 * Output      :  pu4Error   - Error on failure 
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE 
 *****************************************************************************/
INT4
Ofc131PktOfpu2LengthValidate (tOfp131Pkt * pOfp131Pkt, VOID *pConnPtr,
                              tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 *pu4Error)
{
    /* 
     * openflow messages can be of variable length,
     * so length validation will be taken care in the respective
     * message handlers.
     */
    UNUSED_PARAM (pu4Error);
    UNUSED_PARAM (pConnPtr);
    UNUSED_PARAM (pBuf);
    UNUSED_PARAM (pOfp131Pkt);
    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  Ofc131PktOfpu4XidValidate
 * Description :  This function is for openflow message Xid validation 
 * Input       :  pOfp131Pkt - Pointer to the packet received
 *                pConnPtr   - Connection Pointer
 *                pBuf       - CRU Buffer Pointer
 * Output      :  pu4Error   - Error on failure 
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE 
 ****************************************************************************/
INT4
Ofc131PktOfpu4XidValidate (tOfp131Pkt * pOfp131Pkt, VOID *pConnPtr,
                           tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 *pu4Error)
{
    UNUSED_PARAM (pu4Error);
    UNUSED_PARAM (pConnPtr);
    UNUSED_PARAM (pBuf);
    UNUSED_PARAM (pOfp131Pkt);
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function    :  Ofc131PktHelloOfpHelloElemHdrValidate
 * Description :  This function is for validating Hello Elem Header 
 * Input       :  pOfp131Hello - Pointer to the Hello message received
 *                pConnPtr     - Connection Pointer
 *                pBuf         - CRU Buffer Pointer
 * Output      :  pu4Error     - Error on failure 
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE 
 *****************************************************************************/
INT4
Ofc131PktHelloOfpHelloElemHdrValidate (tOfp131Hello * pOfp131Hello,
                                       VOID *pConnPtr,
                                       tCRU_BUF_CHAIN_HEADER * pBuf,
                                       UINT4 *pu4Error)
{
    UNUSED_PARAM (pu4Error);
    UNUSED_PARAM (pConnPtr);
    UNUSED_PARAM (pBuf);
    UNUSED_PARAM (pOfp131Hello);
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function    :  Ofc131PktEchoReqMsgau1DataValidate
 * Description :  This function is for validating echo request data
 * Input       :  pOfp131EchoReqMsg - Pointer to the Echo request
 *                pConnPtr          - Connection Pointer
 *                pBuf              - CRU Buffer Pointer
 * Output      :  pu4Error          - Error on failure 
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE 
 *****************************************************************************/
INT4
Ofc131PktEchoReqMsgau1DataValidate (tOfp131EchoReqMsg * pOfp131EchoReqMsg,
                                    VOID *pConnPtr,
                                    tCRU_BUF_CHAIN_HEADER * pBuf,
                                    UINT4 *pu4Error)
{
    UNUSED_PARAM (pu4Error);
    UNUSED_PARAM (pConnPtr);
    UNUSED_PARAM (pBuf);
    UNUSED_PARAM (pOfp131EchoReqMsg);
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function    :  Ofc131PktEchoRplyMsgau1DataValidate
 * Description :  This function is for validating echo reply data
 * Input       :  pOfp131EchoRplyMsg - Pointer to Echo Reply
 *                pConnPtr           - Connection Pointer
 *                pBuf               - CRU Buffer Pointer
 * Output      :  pu4Error           - Error on failure 
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE 
 *****************************************************************************/
INT4
Ofc131PktEchoRplyMsgau1DataValidate (tOfp131EchoRplyMsg * pOfp131EchoRplyMsg,
                                     VOID *pConnPtr,
                                     tCRU_BUF_CHAIN_HEADER * pBuf,
                                     UINT4 *pu4Error)
{
    UNUSED_PARAM (pu4Error);
    UNUSED_PARAM (pConnPtr);
    UNUSED_PARAM (pBuf);
    UNUSED_PARAM (pOfp131EchoRplyMsg);
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function    :  Ofc131PktExperimenterHdru4ExperimenterValidate
 * Description :  This function is for validating experimenter header
 * Input       :  pOfp131ExperimenterHdr - Experimenter Header Pointer
 *                pConnPtr               - Connection Pointer
 *                pBuf                   - CRU Buffer Pointer
 * Output      :  pu4Error               - Error on failure 
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE 
 *****************************************************************************/
INT4
Ofc131PktExperimenterHdru4ExperimenterValidate (tOfp131ExperimenterHdr *
                                                pOfp131ExperimenterHdr,
                                                VOID *pConnPtr,
                                                tCRU_BUF_CHAIN_HEADER * pBuf,
                                                UINT4 *pu4Error)
{
    UNUSED_PARAM (pu4Error);
    UNUSED_PARAM (pConnPtr);
    UNUSED_PARAM (pBuf);
    UNUSED_PARAM (pOfp131ExperimenterHdr);
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function    :  Ofc131PktExperimenterHdru4ExpTypeValidate
 * Description :  This function is for validating experimenter type
 * Input       :  pOfp131ExperimenterHdr  - Experimenter Header Pointer
 *                pConnPtr                - Connection Pointer
 *                pBuf                    - CRU Buffer Pointer
 * Output      :  pu4Error                - Error on failure 
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE 
 *****************************************************************************/
INT4
Ofc131PktExperimenterHdru4ExpTypeValidate (tOfp131ExperimenterHdr *
                                           pOfp131ExperimenterHdr,
                                           VOID *pConnPtr,
                                           tCRU_BUF_CHAIN_HEADER * pBuf,
                                           UINT4 *pu4Error)
{
    UNUSED_PARAM (pu4Error);
    UNUSED_PARAM (pConnPtr);
    UNUSED_PARAM (pBuf);
    UNUSED_PARAM (pOfp131ExperimenterHdr);
    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  Ofc131PktFeatureReqau1DummyValidate
 * Description :  This function is for validating packet feature dummy 
 * Input       :  pOfp131FeatureReq  - Pointer to the packet feature
 *                pConnPtr           - Connection Pointer
 *                pBuf               - CRU Buffer Pointer
 * Output      :  pu4Error           - Error on failure 
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE 
 ****************************************************************************/
INT4
Ofc131PktFeatureReqau1DummyValidate (tOfp131FeatureReq * pOfp131FeatureReq,
                                     VOID *pConnPtr,
                                     tCRU_BUF_CHAIN_HEADER * pBuf,
                                     UINT4 *pu4Error)
{
    UNUSED_PARAM (pu4Error);
    UNUSED_PARAM (pConnPtr);
    UNUSED_PARAM (pBuf);
    UNUSED_PARAM (pOfp131FeatureReq);
    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  Ofc131PktConfigGetau1DummyValidate
 * Description :  This function is for validate packet config dummy
 * Input       :  pOfp131ConfigGet - Pointer to Config Get request
 *                pConnPtr         - Connection Pointer
 *                pBuf             - CRU Buffer Pointer
 * Output      :  pu4Error         - Error on failure 
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE 
 ****************************************************************************/
INT4
Ofc131PktConfigGetau1DummyValidate (tOfp131ConfigGet * pOfp131ConfigGet,
                                    VOID *pConnPtr,
                                    tCRU_BUF_CHAIN_HEADER * pBuf,
                                    UINT4 *pu4Error)
{
    UNUSED_PARAM (pu4Error);
    UNUSED_PARAM (pConnPtr);
    UNUSED_PARAM (pBuf);
    UNUSED_PARAM (pOfp131ConfigGet);
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function    :  Ofc131PktConfigSetu2FlagsValidate
 * Description :  This function is for  validating Config Set Request Flags 
 * Input       :  pOfp131ConfigSet - Pointer to Config Set Request
 *                pConnPtr         - Connection Pointer
 *                pBuf             - CRU Buffer Pointer
 * Output      :  pu4Error         - Error on failure 
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE 
 *****************************************************************************/
INT4
Ofc131PktConfigSetu2FlagsValidate (tOfp131ConfigSet * pOfp131ConfigSet,
                                   VOID *pConnPtr, tCRU_BUF_CHAIN_HEADER * pBuf,
                                   UINT4 *pu4Error)
{
    UNUSED_PARAM (pu4Error);
    UNUSED_PARAM (pConnPtr);
    UNUSED_PARAM (pBuf);
    UNUSED_PARAM (pOfp131ConfigSet);
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function    :  Ofc131PktConfigSetu2MissSendLenValidate
 * Description :  This function is for validating "sendmisslen" in Config Set
 * Input       :  pOfp131ConfigSet - Pointer to Config Set
 *                pConnPtr         - Connection Pointer
 *                pBuf             - CRU Buffer Pointer
 * Output      :  pu4Error         - Error on failure 
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE 
 *****************************************************************************/
INT4
Ofc131PktConfigSetu2MissSendLenValidate (tOfp131ConfigSet * pOfp131ConfigSet,
                                         VOID *pConnPtr,
                                         tCRU_BUF_CHAIN_HEADER * pBuf,
                                         UINT4 *pu4Error)
{
    UNUSED_PARAM (pu4Error);
    UNUSED_PARAM (pConnPtr);
    UNUSED_PARAM (pBuf);
    UNUSED_PARAM (pOfp131ConfigSet);
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function    :  Ofc131PktPktOutu4BufferIdValidate
 * Description :  This function is for validate BufferId in PacketOut Request
 * Input       :  pOfp131PktOut  - Pointer to Packet Out Message
 *                pConnPtr       - Connection Pointer
 *                pBuf           - CRU Buffer Pointer
 * Output      :  pu4Error       - Error on failure 
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE 
 *****************************************************************************/
INT4
Ofc131PktPktOutu4BufferIdValidate (tOfp131PktOut * pOfp131PktOut,
                                   VOID *pConnPtr, tCRU_BUF_CHAIN_HEADER * pBuf,
                                   UINT4 *pu4Error)
{
    UNUSED_PARAM (pu4Error);
    UNUSED_PARAM (pConnPtr);
    UNUSED_PARAM (pBuf);
    UNUSED_PARAM (pOfp131PktOut);
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function    :  Ofc131PktPktOutu4InPortValidate
 * Description :  This function is for validate InPort in Packet Out Message
 * Input       :  pOfp131PktOut - Pointer to Packet Out Message
 *                pConnPtr      - Connection Pointer
 *                pBuf          - CRU Buffer Pointer
 * Output      :  pu4Error      - Error on failure 
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE 
 *****************************************************************************/
INT4
Ofc131PktPktOutu4InPortValidate (tOfp131PktOut * pOfp131PktOut, VOID *pConnPtr,
                                 tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 *pu4Error)
{
    UNUSED_PARAM (pu4Error);
    UNUSED_PARAM (pConnPtr);
    UNUSED_PARAM (pBuf);
    UNUSED_PARAM (pOfp131PktOut);
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function    :  Ofc131PktPktOutu2ActionsLenValidate
 * Description :  This function is for validating Actions Length in Packet Out
 * Input       :  pOfp131PktOut  - Pointer to Packet Out Message
 *                pConnPtr       - Connection Pointer
 *                pBuf           - CRU Buffer Pointer
 * Output      :  pu4Error       - Error on failure 
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE 
 *****************************************************************************/
INT4
Ofc131PktPktOutu2ActionsLenValidate (tOfp131PktOut * pOfp131PktOut,
                                     VOID *pConnPtr,
                                     tCRU_BUF_CHAIN_HEADER * pBuf,
                                     UINT4 *pu4Error)
{
    UNUSED_PARAM (pu4Error);
    UNUSED_PARAM (pConnPtr);
    UNUSED_PARAM (pBuf);
    UNUSED_PARAM (pOfp131PktOut);
    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  Ofc131PktPktOutau1PadValidate
 * Description :  This function is for padding validation in Packet Out
 * Input       :  pOfp131PktOut  - Pointer to Packet Out Message
 *                pConnPtr       - Connection Pointer
 *                pBuf           - CRU Buffer Pointer
 * Output      :  pu4Error       - Error on failure 
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE 
 ****************************************************************************/
INT4
Ofc131PktPktOutau1PadValidate (tOfp131PktOut * pOfp131PktOut, VOID *pConnPtr,
                               tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 *pu4Error)
{
    UNUSED_PARAM (pu4Error);
    UNUSED_PARAM (pConnPtr);
    UNUSED_PARAM (pBuf);
    UNUSED_PARAM (pOfp131PktOut);
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function    :  Ofc131PktPktOutOfpActionHdrValidate
 * Description :  This function is for validating Action Header in Packet Out
 * Input       :  pOfp131PktOut  - Pointer to Packet Out Message
 *                pConnPtr       - Connection Pointer
 *                pBuf           - CRU Buffer Pointer
 * Output      :  pu4Error       - Error on failure 
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE 
 *****************************************************************************/
INT4
Ofc131PktPktOutOfpActionHdrValidate (tOfp131PktOut * pOfp131PktOut,
                                     VOID *pConnPtr,
                                     tCRU_BUF_CHAIN_HEADER * pBuf,
                                     UINT4 *pu4Error)
{
    UNUSED_PARAM (pu4Error);
    UNUSED_PARAM (pConnPtr);
    UNUSED_PARAM (pBuf);
    UNUSED_PARAM (pOfp131PktOut);
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function    :  Ofc131PktFlowModu8CookieValidate
 * Description :  This function is for validating Cookie in Flow Mod Message
 * Input       :  pOfp131FlowMod - Pointer to Flow Mod Message
 *                pConnPtr       - Connection Pointer
 *                pBuf           - CRU Buffer Pointer
 * Output      :  pu4Error       - Error on failure 
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE 
 *****************************************************************************/
INT4
Ofc131PktFlowModu8CookieValidate (tOfp131FlowMod * pOfp131FlowMod,
                                  VOID *pConnPtr, tCRU_BUF_CHAIN_HEADER * pBuf,
                                  UINT4 *pu4Error)
{
    UNUSED_PARAM (pu4Error);
    UNUSED_PARAM (pConnPtr);
    UNUSED_PARAM (pBuf);
    UNUSED_PARAM (pOfp131FlowMod);
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function    :  Ofc131PktFlowModu8CookieMaskValidate
 * Description :  This function is for validating Cookie Mask in Flow Mod
 * Input       :  pOfp131FlowMod  - Pointer to Flow Mod Message
 *                pConnPtr        - Connection Pointer
 *                pBuf            - CRU Buffer Pointer
 * Output      :  pu4Error        - Error on failure 
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE 
 *****************************************************************************/
INT4
Ofc131PktFlowModu8CookieMaskValidate (tOfp131FlowMod * pOfp131FlowMod,
                                      VOID *pConnPtr,
                                      tCRU_BUF_CHAIN_HEADER * pBuf,
                                      UINT4 *pu4Error)
{
    UNUSED_PARAM (pu4Error);
    UNUSED_PARAM (pConnPtr);
    UNUSED_PARAM (pBuf);
    UNUSED_PARAM (pOfp131FlowMod);
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function    :  Ofc131PktFlowModu1TableIdValidate
 * Description :  This function is for validating TableId in Flow Mod 
 * Input       :  pOfp131FlowMod  - Pointer to Flow Mod Message
 *                pConnPtr        - Connection Pointer
 *                pBuf            - CRU Buffer Pointer
 * Output      :  pu4Error        - Error on failure 
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE 
 *****************************************************************************/
INT4
Ofc131PktFlowModu1TableIdValidate (tOfp131FlowMod * pOfp131FlowMod,
                                   VOID *pConnPtr, tCRU_BUF_CHAIN_HEADER * pBuf,
                                   UINT4 *pu4Error)
{
    UNUSED_PARAM (pu4Error);
    UNUSED_PARAM (pConnPtr);
    UNUSED_PARAM (pBuf);
    UNUSED_PARAM (pOfp131FlowMod);
    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  Ofc131PktFlowModu1CommandValidate
 * Description :  This function is for validating Command in Flow Mod 
 * Input       :  pOfp131FlowMod - Pointer to Flow Mod Message
 *                pConnPtr       - Connection Pointer
 *                pBuf           - CRU Buffer Pointer
 * Output      :  pu4Error       - Error on failure 
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE 
 ****************************************************************************/
INT4
Ofc131PktFlowModu1CommandValidate (tOfp131FlowMod * pOfp131FlowMod,
                                   VOID *pConnPtr,
                                   tCRU_BUF_CHAIN_HEADER * pBuf,
                                   UINT4 *pu4Error)
{
    /*
     * This function is dummy, Command validation is taken care by
     * flow module.
     */
    UNUSED_PARAM (pOfp131FlowMod);
    UNUSED_PARAM (pConnPtr);
    UNUSED_PARAM (pBuf);
    UNUSED_PARAM (pu4Error);
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function    :  Ofc131PktFlowModu2IdleTimeoutValidate
 * Description :  This function is for validating IdleTimeout in Flow Mod 
 * Input       :  pOfp131FlowMod - Pointer to Flow Mod Message
 *                pConnPtr       - Connection Pointer
 *                pBuf           - CRU Buffer Pointer
 * Output      :  pu4Error       - Error on failure 
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE 
 *****************************************************************************/
INT4
Ofc131PktFlowModu2IdleTimeoutValidate (tOfp131FlowMod * pOfp131FlowMod,
                                       VOID *pConnPtr,
                                       tCRU_BUF_CHAIN_HEADER * pBuf,
                                       UINT4 *pu4Error)
{
    UNUSED_PARAM (pu4Error);
    UNUSED_PARAM (pConnPtr);
    UNUSED_PARAM (pBuf);
    UNUSED_PARAM (pOfp131FlowMod);
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function    :  Ofc131PktFlowModu2HardTimeoutValidate
 * Description :  This function is for validating HardTimeout in Flow Mod 
 * Input       :  pOfp131FlowMod - Pointer to Flow Mod Message
 *                pConnPtr       - Connection Pointer
 *                pBuf           - CRU Buffer Pointer
 * Output      :  pu4Error       - Error on failure 
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE 
 *****************************************************************************/
INT4
Ofc131PktFlowModu2HardTimeoutValidate (tOfp131FlowMod * pOfp131FlowMod,
                                       VOID *pConnPtr,
                                       tCRU_BUF_CHAIN_HEADER * pBuf,
                                       UINT4 *pu4Error)
{
    UNUSED_PARAM (pu4Error);
    UNUSED_PARAM (pConnPtr);
    UNUSED_PARAM (pBuf);
    UNUSED_PARAM (pOfp131FlowMod);
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function    :  Ofc131PktFlowModu2PriorityValidate
 * Description :  This function is for validating Priority in Flow Mod
 * Input       :  pOfp131FlowMod - Pointer to Flow Mod Message
 *                pConnPtr       - Connection Pointer
 *                pBuf           - CRU Buffer Pointer
 * Output      :  pu4Error       - Error on failure 
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE 
 *****************************************************************************/
INT4
Ofc131PktFlowModu2PriorityValidate (tOfp131FlowMod * pOfp131FlowMod,
                                    VOID *pConnPtr,
                                    tCRU_BUF_CHAIN_HEADER * pBuf,
                                    UINT4 *pu4Error)
{
    UNUSED_PARAM (pu4Error);
    UNUSED_PARAM (pConnPtr);
    UNUSED_PARAM (pBuf);
    UNUSED_PARAM (pOfp131FlowMod);
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function    :  Ofc131PktFlowModu4BufIdValidate
 * Description :  This function is for validating Buffer Id in Flow Mod
 * Input       :  pOfp131FlowMod - Pointer to Flow Mod Message
 *                pConnPtr       - Connection Pointer
 *                pBuf           - CRU Buffer Pointer
 * Output      :  pu4Error       - Error on failure 
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE 
 *****************************************************************************/
INT4
Ofc131PktFlowModu4BufIdValidate (tOfp131FlowMod * pOfp131FlowMod,
                                 VOID *pConnPtr, tCRU_BUF_CHAIN_HEADER * pBuf,
                                 UINT4 *pu4Error)
{
    UNUSED_PARAM (pu4Error);
    UNUSED_PARAM (pConnPtr);
    UNUSED_PARAM (pBuf);
    UNUSED_PARAM (pOfp131FlowMod);
    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  Ofc131PktFlowModu4OutPortValidate
 * Description :  This function is for validating OutPort in Flow Mod
 * Input       :  pOfp131FlowMod - Pointer to Flow Mod Message
 *                pConnPtr       - Connection Pointer
 *                pBuf           - CRU Buffer Pointer
 * Output      :  pu4Error       - Error on failure 
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE 
 ****************************************************************************/
INT4
Ofc131PktFlowModu4OutPortValidate (tOfp131FlowMod * pOfp131FlowMod,
                                   VOID *pConnPtr, tCRU_BUF_CHAIN_HEADER * pBuf,
                                   UINT4 *pu4Error)
{
    UNUSED_PARAM (pu4Error);
    UNUSED_PARAM (pConnPtr);
    UNUSED_PARAM (pBuf);
    UNUSED_PARAM (pOfp131FlowMod);
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function    :  Ofc131PktFlowModu4OutGroupValidate
 * Description :  This function is for validating OutGroup in Flow Mod
 * Input       :  pOfp131FlowMod - Pointer to Flow Mod Message
 *                pConnPtr       - Connection Pointer
 *                pBuf           - CRU Buffer Pointer
 * Output      :  pu4Error       - Error on failure 
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE 
 *****************************************************************************/
INT4
Ofc131PktFlowModu4OutGroupValidate (tOfp131FlowMod * pOfp131FlowMod,
                                    VOID *pConnPtr,
                                    tCRU_BUF_CHAIN_HEADER * pBuf,
                                    UINT4 *pu4Error)
{
    UNUSED_PARAM (pu4Error);
    UNUSED_PARAM (pConnPtr);
    UNUSED_PARAM (pBuf);
    UNUSED_PARAM (pOfp131FlowMod);
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function    :  Ofc131PktFlowModu2FlagsValidate
 * Description :  This function is for validating Flags in Flow Mod
 * Input       :  pOfp131FlowMod - Pointer to Flow Mod Message
 *                pConnPtr       - Connection Pointer
 *                pBuf           - CRU Buffer Pointer
 * Output      :  pu4Error       - Error on failure 
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE 
 *****************************************************************************/
INT4
Ofc131PktFlowModu2FlagsValidate (tOfp131FlowMod * pOfp131FlowMod,
                                 VOID *pConnPtr, tCRU_BUF_CHAIN_HEADER * pBuf,
                                 UINT4 *pu4Error)
{
    UNUSED_PARAM (pu4Error);
    UNUSED_PARAM (pConnPtr);
    UNUSED_PARAM (pBuf);
    UNUSED_PARAM (pOfp131FlowMod);
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function    :  Ofc131PktFlowModau1PadValidate
 * Description :  This function is for validating Padding in Flow Mod
 * Input       :  pOfp131FlowMod - Pointer to Flow Mod Message
 *                pConnPtr       - Connection Pointer
 *                pBuf           - CRU Buffer Pointer
 * Output      :  pu4Error       - Error on failure 
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE 
 *****************************************************************************/
INT4
Ofc131PktFlowModau1PadValidate (tOfp131FlowMod * pOfp131FlowMod, VOID *pConnPtr,
                                tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 *pu4Error)
{
    UNUSED_PARAM (pu4Error);
    UNUSED_PARAM (pConnPtr);
    UNUSED_PARAM (pBuf);
    UNUSED_PARAM (pOfp131FlowMod);
    return OSIX_SUCCESS;
}

/******************************************************************************
 * Function    :  Ofc131PktFlowModOfpMatchValidate
 * Description :  This routine validates match fields, instructions, actions
 *                and set fields as part of Flow Mod Request 
 * Input       :  pOfp131FlowMod  - Pointer to Flow Mod Request 
 *                pConnPtr        - Connection Pointer
 *                pBuf            - CRU Buffer containing the entire message
 * Output      :  pu4Error        - Error on failure 
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE 
 *****************************************************************************/
INT4
Ofc131PktFlowModOfpMatchValidate (tOfp131FlowMod * pOfp131FlowMod,
                                  VOID *pConnPtr,
                                  tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 *pu4Error)
{
    FS_UINT8            u8FlowMatchBitMap;
    tOfcFsofcFlowTable *pOfcFsofcFlowTable = NULL;
    tOfp131Match       *pOfpMatch = NULL;
    tOfp131Instruction *pInstrHdr = NULL;
    tOfp131ActionHdr   *pActionHdr = NULL;
    tOxmTlv            *pOxmTlv = NULL;
    UINT4               u4RetVal = OFC_ZERO;
    UINT2               u2Length = OFC_ZERO;
    UINT2               u2TempLength = OFC_ZERO;
    UINT2               u2Type = OFC_ZERO;
    UINT1               u1OnlyMatch = OFC_ZERO;
    UINT1               u1InstrBitMap = OFC_ZERO;
    UINT1               u1OneByte = OFC_ZERO;
    UINT1               u1OxmLen = OFC_ZERO;
    UINT4               u4Temp = OFC_ZERO;
    tOfcFsofcControllerConnEntry *pLocal = pConnPtr;

#ifdef DPA_WANTED
    UINT1               u1LocalTabId = OFC_ZERO;
#endif /* DPA_WANTED */

    OFC131_TRC_FUNC ((OFC131_FN_ENTRY, "FUNC:Ofc131FlowModOfpMatchValidate\n"));

    /* Skip Validations if command is delete and table ID is all */
    if ((pOfp131FlowMod->u1Command == OFPFC_DELETE) ||
        (pOfp131FlowMod->u1Command == OFPFC_DELETE_STRICT))
    {
        if (pOfp131FlowMod->u1TableId == OFPTT_ALL)
        {
            return OSIX_SUCCESS;
        }
        /* No need to validate instructions and actions for delete */
        u1OnlyMatch = OFC_ONE;
    }

#ifdef DPA_WANTED
    if (OfcGetLocalTableId (pOfp131FlowMod->u1TableId, &u1LocalTabId)
        == OFC_FAILURE)
    {
        return OFC_FAILURE;
    }
    pOfp131FlowMod->u1TableId = u1LocalTabId;
#endif /* DPA_WANTED */

    /* Fetch the Flow Table */
    pOfcFsofcFlowTable = OfcGetFlowTableEntry (OFC_DEFAULT_CONTEXT,
                                               pOfp131FlowMod->u1TableId);

    if (pOfcFsofcFlowTable == NULL)
    {
        OFC131_TRC_FUNC ((OFC_PKT_TRC, "Flow Table Entry not found for"
                          "Flow Table index %d\n", pOfp131FlowMod->u1TableId));
        *pu4Error = OFC131_FLOWMOD_INVALID_TABLE_ERR;
        return OSIX_FAILURE;
    }

    MEMSET (&u8FlowMatchBitMap, OFC_ZERO, sizeof (u8FlowMatchBitMap));

    /* Prepare bitmap out of flow mod to validate the match fields */
    pOfpMatch = (tOfp131Match *) (VOID *) (pLocal->pRcvPkt +
                                           OFC131_FLOWMOD_OFPMATCH_OFFSET);
    pOxmTlv = (tOxmTlv *) (VOID *) (pLocal->pRcvPkt +
                                    OFC131_FLOWMOD_OFPMATCH_OFFSET + OFC_FOUR);
    u2Length = (UINT2) (OSIX_NTOHS (pOfpMatch->u2Length) - OFC_FOUR);

    /* Validate the Match Type */
    if (pOfpMatch->u2Type != OSIX_HTONS (OFPMT_OXM))
    {
        OFC131_TRC_FUNC ((OFC_PKT_TRC, "Invalid Match Type\n"));
        *pu4Error = OFC131_FLOWMOD_BAD_TYPE_ERR;
        return OSIX_FAILURE;
    }

    if ((pOxmTlv->u1Field == OFC_ZERO) && (pOxmTlv->u1Len == OFC_ZERO))
    {
        /* No Match Fields, skip while loop below */
        u2Length = OFC_ZERO;
    }

    while (u2Length > OFC_ZERO)
    {
        /* Read the TLV's in the match structure */
        if (pOxmTlv->u2Class != OSIX_HTONS (OFPXMC_OPENFLOW_BASIC))
        {
            OFC131_TRC_FUNC ((OFC_PKT_TRC, "Invalid Oxm Tlv Class\n"));
            *pu4Error = OFC131_FLOWMOD_BAD_FIELD_ERR;
            return OSIX_FAILURE;
        }
        u1OneByte = pOxmTlv->u1Field >> OFC_ONE;
        if (u1OneByte > OFC_THIRTY_NINE)
        {
            *pu4Error = OFC131_FLOWMOD_BAD_FIELD_ERR;
            return OSIX_FAILURE;
        }
        else if (u1OneByte > OFC_THIRTY_ONE)
        {
            u4Temp = (UINT4) (OFC_ONE << (u1OneByte - OFC_THIRTY_TWO));
            if (!((u8FlowMatchBitMap.u4Hi) & (u4Temp)))
            {
                u8FlowMatchBitMap.u4Hi |= (UINT4) (OFC_ONE <<
                                                   (u1OneByte -
                                                    OFC_THIRTY_TWO));
            }
            else
            {
                *pu4Error = OFC131_FLOWMOD_DUPLICATE_FIELD_ERROR;
                return OSIX_FAILURE;
            }
        }
        else
        {
            u4Temp = (UINT4) (OFC_ONE << u1OneByte);
            if (!((u8FlowMatchBitMap.u4Lo) & (u4Temp)))
            {
                u8FlowMatchBitMap.u4Lo |= (UINT4) (OFC_ONE << u1OneByte);
            }
            else
            {
                *pu4Error = OFC131_FLOWMOD_DUPLICATE_FIELD_ERROR;
                return OSIX_FAILURE;
            }
        }
        u1OxmLen = pOxmTlv->u1Len;
        if (u2Length < (sizeof (tOxmTlv) + u1OxmLen))
        {
            /* OFPBMC_BAD_LEN */
            return OSIX_FAILURE;
        }
        u2Length = (UINT2) (u2Length - (sizeof (tOxmTlv) + u1OxmLen));
        pOxmTlv = (tOxmTlv *) (VOID *) (((UINT1 *) pOxmTlv) +
                                        (sizeof (tOxmTlv) + u1OxmLen));
    }

    if (((u8FlowMatchBitMap.u4Hi &
          (pOfcFsofcFlowTable->u8FlowMatchBitMap.u4Hi)) !=
         u8FlowMatchBitMap.u4Hi) ||
        ((u8FlowMatchBitMap.u4Lo &
          (pOfcFsofcFlowTable->u8FlowMatchBitMap.u4Lo)) !=
         u8FlowMatchBitMap.u4Lo))
    {
        OFC131_TRC_FUNC ((OFC_PKT_TRC,
                          "FlowMatch bit map Validation failed\n"));
        *pu4Error = OFC131_FLOWMOD_BAD_FIELD_ERR;
        return OSIX_FAILURE;
    }

    if (u1OnlyMatch == OFC_ONE)
    {
        return OFC_SUCCESS;
    }

    /* Prepare bitmap out of flow mod to validate the instructions */
    u2Length = OSIX_NTOHS (pOfpMatch->u2Length);
    /* pad to 64 bits */
    if ((u2Length % OFC_EIGHT) != OFC_ZERO)
    {
        u2Length = (UINT2) (u2Length + OFC_EIGHT - (u2Length % OFC_EIGHT));
    }
    u2Length = (UINT2) (OFC131_FLOWMOD_OFPMATCH_OFFSET + u2Length);
    pInstrHdr = (tOfp131Instruction *) (VOID *) (pLocal->pRcvPkt + u2Length);
    u2Length = (UINT2) (pLocal->u4RxLength - u2Length);

    /* Parse Instructions */
    while (u2Length > OFC_ZERO)
    {
        u2Type = OSIX_NTOHS (pInstrHdr->u2Type);
        if ((!u2Type) || (u2Type > OFC_SIX))
        {
            *pu4Error = OFC131_FLOWMOD_UNKNOWN_INSTR_ERR;
            return OFC_FAILURE;
        }
        else
        {
            if ((u2Type == OFCIT_WRITE_ACTIONS) ||
                (u2Type == OFCIT_APPLY_ACTIONS))
            {
                u2TempLength = (UINT2) (OSIX_NTOHS (pInstrHdr->u2Len) -
                                        OFC_EIGHT);
                pActionHdr = (tOfp131ActionHdr *) (VOID *)
                    (((UINT1 *) pInstrHdr) + OFC_EIGHT);
                u4RetVal = Ofc131PktActsValidate (pOfcFsofcFlowTable, u2Type,
                                                  pActionHdr, u2TempLength,
                                                  pu4Error);
                if (u4RetVal == OSIX_FAILURE)
                {
                    /* pu4Error is already filled */
                    return OSIX_FAILURE;
                }
            }
            u1InstrBitMap |= (UINT1) (OFC_ONE << u2Type);
        }

        if (u2Length < OSIX_NTOHS (pInstrHdr->u2Len))
        {
            /* OFPBIC_BAD_LEN */
            return OSIX_FAILURE;
        }
        u2Length = (UINT2) (u2Length - (OSIX_NTOHS (pInstrHdr->u2Len)));
        pInstrHdr = (tOfp131Instruction *) (VOID *)
            (((UINT1 *) pInstrHdr) + OSIX_NTOHS (pInstrHdr->u2Len));
    }

    if (((u1InstrBitMap &
          (pOfcFsofcFlowTable->u1InstrsBitMap)) != u1InstrBitMap))
    {
        OFC131_TRC_FUNC ((OFC_PKT_TRC, "Instructions validation failed\n"));
        *pu4Error = OFC131_FLOWMOD_UNSUP_INSTR_ERR;
        return OSIX_FAILURE;
    }
    UNUSED_PARAM (pBuf);
    OFC131_TRC_FUNC ((OFC131_FN_EXIT, "FUNC:Ofc131FlowModOfpMatchValidate\n"));
    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  Ofc131PktGroupModu2CmdValidate
 * Description :  This function is for validating Command in Group Mod 
 * Input       :  pOfp131GroupMod - Pointer to Group Mod Message
 *                pConnPtr        - Connection Pointer
 *                pBuf            - CRU Buffer Pointer
 * Output      :  pu4Error        - Error on failure 
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE 
 ****************************************************************************/
INT4
Ofc131PktGroupModu2CmdValidate (tOfp131GroupMod * pOfp131GroupMod,
                                VOID *pConnPtr, tCRU_BUF_CHAIN_HEADER * pBuf,
                                UINT4 *pu4Error)
{
    OFC131_TRC_FUNC ((OFC131_FN_ENTRY, "FUNC:Ofc131GroupModu2CmdValidate\n"));

    if (pOfp131GroupMod->u2Cmd > OFC_TWO)
    {
        OFC131_TRC_FUNC ((OFC_PKT_TRC, "Group Command Range Failed value = %d",
                          pOfp131GroupMod->u2Cmd));
        *pu4Error = OFC131_GROUPMOD_U2CMD_VALIDATE_ERR;
        return OSIX_FAILURE;
    }
    UNUSED_PARAM (pu4Error);
    UNUSED_PARAM (pConnPtr);
    UNUSED_PARAM (pBuf);
    OFC131_TRC_FUNC ((OFC131_FN_EXIT, "FUNC:Ofc131GroupModu2CmdValidate\n"));
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function    :  Ofc131PktGroupModu1TypeValidate
 * Description :  This function is for validating Type in Group Mod 
 * Input       :  pOfp131GroupMod - Pointer to Group Mod Message
 *                pConnPtr        - Connection Pointer
 *                pBuf            - CRU Buffer Pointer
 * Output      :  pu4Error        - Error on failure 
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE 
 *****************************************************************************/
INT4
Ofc131PktGroupModu1TypeValidate (tOfp131GroupMod * pOfp131GroupMod,
                                 VOID *pConnPtr, tCRU_BUF_CHAIN_HEADER * pBuf,
                                 UINT4 *pu4Error)
{
    OFC131_TRC_FUNC ((OFC131_FN_ENTRY, "FUNC:Ofc131GroupModu1TypeValidate\n"));

    if (pOfp131GroupMod->u1Type > OFC_THREE)
    {
        OFC131_TRC_FUNC ((OFC_PKT_TRC, "Group Type Range Failed value = %d",
                          pOfp131GroupMod->u1Type));
        *pu4Error = OFC131_GROUPMOD_U1TYPE_VALIDATE_ERR;
        return OSIX_FAILURE;
    }
    UNUSED_PARAM (pu4Error);
    UNUSED_PARAM (pConnPtr);
    UNUSED_PARAM (pBuf);
    OFC131_TRC_FUNC ((OFC131_FN_EXIT, "FUNC:Ofc131GroupModu1TypeValidate\n"));
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function    :  Ofc131PktGroupModu1PadValidate
 * Description :  This function is for validating Pad in Group Mod 
 * Input       :  pOfp131GroupMod - Pointer to Group Mod Message
 *                pConnPtr        - Connection Pointer
 *                pBuf            - CRU Buffer Pointer
 * Output      :  pu4Error        - Error on failure 
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE 
 *****************************************************************************/
INT4
Ofc131PktGroupModu1PadValidate (tOfp131GroupMod * pOfp131GroupMod,
                                VOID *pConnPtr, tCRU_BUF_CHAIN_HEADER * pBuf,
                                UINT4 *pu4Error)
{
    UNUSED_PARAM (pu4Error);
    UNUSED_PARAM (pConnPtr);
    UNUSED_PARAM (pBuf);
    UNUSED_PARAM (pOfp131GroupMod);
    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  Ofc131PktGroupModu4GroupIdValidate
 * Description :  This function is for validating Group ID in Group Mod 
 * Input       :  pOfp131GroupMod - Pointer to Group Mod Message
 *                pConnPtr        - Connection Pointer
 *                pBuf            - CRU Buffer Pointer
 * Output      :  pu4Error        - Error on failure 
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE 
 ****************************************************************************/
INT4
Ofc131PktGroupModu4GroupIdValidate (tOfp131GroupMod * pOfp131GroupMod,
                                    VOID *pConnPtr,
                                    tCRU_BUF_CHAIN_HEADER * pBuf,
                                    UINT4 *pu4Error)
{
    OFC131_TRC_FUNC ((OFC131_FN_ENTRY,
                      "FUNC:Ofc131GroupModu4GroupIdValidate\n"));

    if (pOfp131GroupMod->u2Cmd == OFCGC_ADD)
    {
        if (OSIX_NTOHL (pOfp131GroupMod->u4GroupId) > OFCG_MAX)
        {
            *pu4Error = OFC131_GROUPMOD_U4GROUPID_VALIDATE_ERR;
            return OSIX_FAILURE;
        }
    }
    UNUSED_PARAM (pConnPtr);
    UNUSED_PARAM (pBuf);
    OFC131_TRC_FUNC ((OFC131_FN_EXIT,
                      "FUNC:Ofc131GroupModu4GroupIdValidate\n"));
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function    :  Ofc131PktGroupModOfpBucketValidate
 * Description :  This function is for validating Action Bucket in Group Mod 
 * Input       :  pOfp131GroupMod - Pointer to Group Mod Message
 *                pConnPtr        - Connection Pointer
 *                pBuf            - CRU Buffer Pointer
 * Output      :  pu4Error        - Error on failure 
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE 
 *****************************************************************************/
INT4
Ofc131PktGroupModOfpBucketValidate (tOfp131GroupMod * pOfp131GroupMod,
                                    VOID *pConnPtr,
                                    tCRU_BUF_CHAIN_HEADER * pBuf,
                                    UINT4 *pu4Error)
{
    UNUSED_PARAM (pu4Error);
    UNUSED_PARAM (pConnPtr);
    UNUSED_PARAM (pBuf);
    UNUSED_PARAM (pOfp131GroupMod);
    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  Ofc131PktPortModu4PortNoValidate
 * Description :  This function is for validating Port Number in Port Mod
 * Input       :  pOfp131PortMod  - Pointer to Port Mod Message
 *                pConnPtr        - Connection Pointer
 *                pBuf            - CRU Buffer Pointer
 * Output      :  pu4Error        - Error on failure 
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE 
 ****************************************************************************/
INT4
Ofc131PktPortModu4PortNoValidate (tOfp131PortMod * pOfp131PortMod,
                                  VOID *pConnPtr, tCRU_BUF_CHAIN_HEADER * pBuf,
                                  UINT4 *pu4Error)
{
    UNUSED_PARAM (pu4Error);
    UNUSED_PARAM (pConnPtr);
    UNUSED_PARAM (pBuf);
    UNUSED_PARAM (pOfp131PortMod);
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function    :  Ofc131PktPortModau1PadValidate
 * Description :  This function is for validating Padding in Port Mod
 * Input       :  pOfp131PortMod  - Pointer to Port Mod Message
 *                pConnPtr        - Connection Pointer
 *                pBuf            - CRU Buffer Pointer
 * Output      :  pu4Error        - Error on failure 
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE 
 *****************************************************************************/
INT4
Ofc131PktPortModau1PadValidate (tOfp131PortMod * pOfp131PortMod, VOID *pConnPtr,
                                tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 *pu4Error)
{
    UNUSED_PARAM (pu4Error);
    UNUSED_PARAM (pConnPtr);
    UNUSED_PARAM (pBuf);
    UNUSED_PARAM (pOfp131PortMod);
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function    :  Ofc131PktPortModau1HwAddrValidate
 * Description :  This function is for validating Hardware Address in Port Mod
 * Input       :  pOfp131PortMod  - Pointer to Port Mod Message
 *                pConnPtr        - Connection Pointer
 *                pBuf            - CRU Buffer Pointer
 * Output      :  pu4Error        - Error on failure 
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE 
 *****************************************************************************/
INT4
Ofc131PktPortModau1HwAddrValidate (tOfp131PortMod * pOfp131PortMod,
                                   VOID *pConnPtr, tCRU_BUF_CHAIN_HEADER * pBuf,
                                   UINT4 *pu4Error)
{
    UNUSED_PARAM (pu4Error);
    UNUSED_PARAM (pConnPtr);
    UNUSED_PARAM (pBuf);
    UNUSED_PARAM (pOfp131PortMod);
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function    :  Ofc131PktPortModau1Pad2Validate
 * Description :  This function is for validating Pad2 in Port Mod
 * Input       :  pOfp131PortMod  - Pointer to Port Mod Message
 *                pConnPtr        - Connection Pointer
 *                pBuf            - CRU Buffer Pointer
 * Output      :  pu4Error        - Error on failure 
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE 
 *****************************************************************************/
INT4
Ofc131PktPortModau1Pad2Validate (tOfp131PortMod * pOfp131PortMod,
                                 VOID *pConnPtr, tCRU_BUF_CHAIN_HEADER * pBuf,
                                 UINT4 *pu4Error)
{
    UNUSED_PARAM (pu4Error);
    UNUSED_PARAM (pConnPtr);
    UNUSED_PARAM (pBuf);
    UNUSED_PARAM (pOfp131PortMod);
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function    :  Ofc131PktPortModu4ConfigValidate
 * Description :  This function is for validating Config in Port Mod
 * Input       :  pOfp131PortMod  - Pointer to Port Mod Message
 *                pConnPtr        - Connection Pointer
 *                pBuf            - CRU Buffer Pointer
 * Output      :  pu4Error        - Error on failure 
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE 
 *****************************************************************************/
INT4
Ofc131PktPortModu4ConfigValidate (tOfp131PortMod * pOfp131PortMod,
                                  VOID *pConnPtr, tCRU_BUF_CHAIN_HEADER * pBuf,
                                  UINT4 *pu4Error)
{
    UNUSED_PARAM (pOfp131PortMod);
    UNUSED_PARAM (pu4Error);
    UNUSED_PARAM (pConnPtr);
    UNUSED_PARAM (pBuf);
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function    :  Ofc131PktPortModu4MaskValidate
 * Description :  This function is for validating Mask in Port Mod
 * Input       :  pOfp131PortMod  - Pointer to Port Mod Message
 *                pConnPtr        - Connection Pointer
 *                pBuf            - CRU Buffer Pointer
 * Output      :  pu4Error        - Error on failure 
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE 
 *****************************************************************************/
INT4
Ofc131PktPortModu4MaskValidate (tOfp131PortMod * pOfp131PortMod, VOID *pConnPtr,
                                tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 *pu4Error)
{
    UNUSED_PARAM (pu4Error);
    UNUSED_PARAM (pConnPtr);
    UNUSED_PARAM (pBuf);
    UNUSED_PARAM (pOfp131PortMod);
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function    :  Ofc131PktPortModu4AdvertiseValidate
 * Description :  This function is for validating Advertise in Port Mod
 * Input       :  pOfp131PortMod  - Pointer to Port Mod Message
 *                pConnPtr        - Connection Pointer
 *                pBuf            - CRU Buffer Pointer
 * Output      :  pu4Error        - Error on failure 
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE 
 *****************************************************************************/
INT4
Ofc131PktPortModu4AdvertiseValidate (tOfp131PortMod * pOfp131PortMod,
                                     VOID *pConnPtr,
                                     tCRU_BUF_CHAIN_HEADER * pBuf,
                                     UINT4 *pu4Error)
{
    UNUSED_PARAM (pu4Error);
    UNUSED_PARAM (pConnPtr);
    UNUSED_PARAM (pBuf);
    UNUSED_PARAM (pOfp131PortMod);
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function    :  Ofc131PktPortModau1Pad3Validate
 * Description :  This function is for validating Pad3 in Port Mod
 * Input       :  pOfp131PortMod  - Pointer to Port Mod Message
 *                pConnPtr        - Connection Pointer
 *                pBuf            - CRU Buffer Pointer
 * Output      :  pu4Error        - Error on failure 
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE 
 *****************************************************************************/
INT4
Ofc131PktPortModau1Pad3Validate (tOfp131PortMod * pOfp131PortMod,
                                 VOID *pConnPtr, tCRU_BUF_CHAIN_HEADER * pBuf,
                                 UINT4 *pu4Error)
{
    UNUSED_PARAM (pu4Error);
    UNUSED_PARAM (pConnPtr);
    UNUSED_PARAM (pBuf);
    UNUSED_PARAM (pOfp131PortMod);
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function    :  Ofc131PktMultiPartRequ2TypeValidate
 * Description :  This function is for validating Type in Multipart Request
 * Input       :  pOfp131MultiPartReq  - Pointer to Multipart Request Message
 *                pConnPtr             - Connection Pointer
 *                pBuf                 - CRU Buffer Pointer
 * Output      :  pu4Error             - Error on failure 
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE 
 *****************************************************************************/
INT4
Ofc131PktMultiPartRequ2TypeValidate (tOfp131MultiPartReq * pOfp131MultiPartReq,
                                     VOID *pConnPtr,
                                     tCRU_BUF_CHAIN_HEADER * pBuf,
                                     UINT4 *pu4Error)
{
    UNUSED_PARAM (pu4Error);
    UNUSED_PARAM (pConnPtr);
    UNUSED_PARAM (pBuf);
    UNUSED_PARAM (pOfp131MultiPartReq);
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function    :  Ofc131PktMultiPartRequ2FlagsValidate
 * Description :  This function is for validating Flags in Multipart Request
 * Input       :  pOfp131MultiPartReq  - Pointer to Multipart Request Message
 *                pConnPtr             - Connection Pointer
 *                pBuf                 - CRU Buffer Pointer
 * Output      :  pu4Error             - Error on failure 
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE 
 *****************************************************************************/
INT4
Ofc131PktMultiPartRequ2FlagsValidate (tOfp131MultiPartReq * pOfp131MultiPartReq,
                                      VOID *pConnPtr,
                                      tCRU_BUF_CHAIN_HEADER * pBuf,
                                      UINT4 *pu4Error)
{
    UNUSED_PARAM (pu4Error);
    UNUSED_PARAM (pConnPtr);
    UNUSED_PARAM (pBuf);
    UNUSED_PARAM (pOfp131MultiPartReq);
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function    :  Ofc131PktMultiPartReqau1PadValidate
 * Description :  This function is for validating Padding in Multipart Request
 * Input       :  pOfp131MultiPartReq  - Pointer to Multipart Request Message
 *                pConnPtr             - Connection Pointer
 *                pBuf                 - CRU Buffer Pointer
 * Output      :  pu4Error             - Error on failure 
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE 
 *****************************************************************************/
INT4
Ofc131PktMultiPartReqau1PadValidate (tOfp131MultiPartReq * pOfp131MultiPartReq,
                                     VOID *pConnPtr,
                                     tCRU_BUF_CHAIN_HEADER * pBuf,
                                     UINT4 *pu4Error)
{
    UNUSED_PARAM (pu4Error);
    UNUSED_PARAM (pConnPtr);
    UNUSED_PARAM (pBuf);
    UNUSED_PARAM (pOfp131MultiPartReq);
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function    :  Ofc131PktMultiPartReqau1BodyValidate
 * Description :  This function is for validating Body of Multipart Request
 * Input       :  pOfp131MultiPartReq  - Pointer to Multipart Request Message
 *                pConnPtr             - Connection Pointer
 *                pBuf                 - CRU Buffer Pointer
 * Output      :  pu4Error             - Error on failure 
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE 
 *****************************************************************************/
INT4
Ofc131PktMultiPartReqau1BodyValidate (tOfp131MultiPartReq * pOfp131MultiPartReq,
                                      VOID *pConnPtr,
                                      tCRU_BUF_CHAIN_HEADER * pBuf,
                                      UINT4 *pu4Error)
{
    UNUSED_PARAM (pu4Error);
    UNUSED_PARAM (pConnPtr);
    UNUSED_PARAM (pBuf);
    UNUSED_PARAM (pOfp131MultiPartReq);
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function    :  Ofc131PktBarrierReqau1DummyValidate
 * Description :  This function is for validating Dummy of Barrier Request
 * Input       :  pOfp131BarrierReq - Pointer to Barrier Request Message
 *                pConnPtr          - Connection Pointer
 *                pBuf              - CRU Buffer Pointer
 * Output      :  pu4Error          - Error on failure 
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE 
 *****************************************************************************/
INT4
Ofc131PktBarrierReqau1DummyValidate (tOfp131BarrierReq * pOfp131BarrierReq,
                                     VOID *pConnPtr,
                                     tCRU_BUF_CHAIN_HEADER * pBuf,
                                     UINT4 *pu4Error)
{
    UNUSED_PARAM (pu4Error);
    UNUSED_PARAM (pConnPtr);
    UNUSED_PARAM (pBuf);
    UNUSED_PARAM (pOfp131BarrierReq);
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function    :  Ofc131PktQueueRequ4PortValidate
 * Description :  This function is for validating Port in Queue Request
 * Input       :  pOfp131QueueReq - Pointer to Queue Request Message
 *                pConnPtr        - Connection Pointer
 *                pBuf            - CRU Buffer Pointer
 * Output      :  pu4Error        - Error on failure 
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE 
 *****************************************************************************/
INT4
Ofc131PktQueueRequ4PortValidate (tOfp131QueueReq * pOfp131QueueReq,
                                 VOID *pConnPtr, tCRU_BUF_CHAIN_HEADER * pBuf,
                                 UINT4 *pu4Error)
{
    UNUSED_PARAM (pu4Error);
    UNUSED_PARAM (pConnPtr);
    UNUSED_PARAM (pBuf);
    UNUSED_PARAM (pOfp131QueueReq);
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function    :  Ofc131PktQueueReqau1PadValidate
 * Description :  This function is for validating Pad in Queue Request
 * Input       :  pOfp131QueueReq - Pointer to Queue Request Message
 *                pConnPtr        - Connection Pointer
 *                pBuf            - CRU Buffer Pointer
 * Output      :  pu4Error        - Error on failure 
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE 
 *****************************************************************************/
INT4
Ofc131PktQueueReqau1PadValidate (tOfp131QueueReq * pOfp131QueueReq,
                                 VOID *pConnPtr, tCRU_BUF_CHAIN_HEADER * pBuf,
                                 UINT4 *pu4Error)
{
    UNUSED_PARAM (pu4Error);
    UNUSED_PARAM (pConnPtr);
    UNUSED_PARAM (pBuf);
    UNUSED_PARAM (pOfp131QueueReq);
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function    :  Ofc131PktRoleRequ4RoleValidate
 * Description :  This function is for validating Role in Role Request
 * Input       :  pOfp131RoleReq - Pointer to Role Request Message
 *                pConnPtr       - Connection Pointer
 *                pBuf           - CRU Buffer Pointer
 * Output      :  pu4Error       - Error on failure 
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE 
 *****************************************************************************/
INT4
Ofc131PktRoleRequ4RoleValidate (tOfp131RoleReq * pOfp131RoleReq, VOID *pConnPtr,
                                tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 *pu4Error)
{
    UNUSED_PARAM (pu4Error);
    UNUSED_PARAM (pConnPtr);
    UNUSED_PARAM (pBuf);
    UNUSED_PARAM (pOfp131RoleReq);
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function    :  Ofc131PktRoleReqau1PadValidate
 * Description :  This function is for validating Padding in Role Request
 * Input       :  pOfp131RoleReq - Pointer to Role Request Message
 *                pConnPtr       - Connection Pointer
 *                pBuf           - CRU Buffer Pointer
 * Output      :  pu4Error       - Error on failure 
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE 
 *****************************************************************************/
INT4
Ofc131PktRoleReqau1PadValidate (tOfp131RoleReq * pOfp131RoleReq, VOID *pConnPtr,
                                tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 *pu4Error)
{
    UNUSED_PARAM (pu4Error);
    UNUSED_PARAM (pConnPtr);
    UNUSED_PARAM (pBuf);
    UNUSED_PARAM (pOfp131RoleReq);
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function    :  Ofc131PktRoleRequ8GenIdValidate
 * Description :  This function is for validating GenerationId in Role Request
 * Input       :  pOfp131RoleReq - Pointer to Role Request Message
 *                pConnPtr       - Connection Pointer
 *                pBuf           - CRU Buffer Pointer
 * Output      :  pu4Error       - Error on failure 
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE 
 *****************************************************************************/
INT4
Ofc131PktRoleRequ8GenIdValidate (tOfp131RoleReq * pOfp131RoleReq,
                                 VOID *pConnPtr, tCRU_BUF_CHAIN_HEADER * pBuf,
                                 UINT4 *pu4Error)
{
    UNUSED_PARAM (pu4Error);
    UNUSED_PARAM (pConnPtr);
    UNUSED_PARAM (pBuf);
    UNUSED_PARAM (pOfp131RoleReq);
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function    :  Ofc131PktAsyncConfigGetau1DummyValidate
 * Description :  This function is for validating Dummy in AsyncConfig Request 
 * Input       :  pOfp131AsyncConfigGet - Pointer to Async Config Get Request
 *                pConnPtr              - Connection Pointer
 *                pBuf                  - CRU Buffer Pointer
 * Output      :  pu4Error              - Error on failure 
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE 
 *****************************************************************************/
INT4
Ofc131PktAsyncConfigGetau1DummyValidate (tOfp131AsyncConfigGet *
                                         pOfp131AsyncConfigGet, VOID *pConnPtr,
                                         tCRU_BUF_CHAIN_HEADER * pBuf,
                                         UINT4 *pu4Error)
{
    UNUSED_PARAM (pu4Error);
    UNUSED_PARAM (pConnPtr);
    UNUSED_PARAM (pBuf);
    UNUSED_PARAM (pOfp131AsyncConfigGet);
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function    :  Ofc131PktAsyncConfigSetau4PktInMaskValidate
 * Description :  This function is for validating Packet-In Mask in
 *                AsyncConfig Set Request 
 * Input       :  pOfp131AsyncConfigSet - Pointer to Async Config Set Request
 *                pConnPtr              - Connection Pointer
 *                pBuf                  - CRU Buffer Pointer
 * Output      :  pu4Error              - Error on failure 
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE 
 *****************************************************************************/
INT4
Ofc131PktAsyncConfigSetau4PktInMaskValidate (tOfp131AsyncConfigSet *
                                             pOfp131AsyncConfigSet,
                                             VOID *pConnPtr,
                                             tCRU_BUF_CHAIN_HEADER * pBuf,
                                             UINT4 *pu4Error)
{
    UNUSED_PARAM (pu4Error);
    UNUSED_PARAM (pConnPtr);
    UNUSED_PARAM (pBuf);
    UNUSED_PARAM (pOfp131AsyncConfigSet);
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function    :  Ofc131PktAsyncConfigSetau4PortStatusMaskValidate
 * Description :  This function is for validating Packet-In Mask in
 *                AsyncConfig Set Request 
 * Input       :  pOfp131AsyncConfigSet - Pointer to Async Config Set Request
 *                pConnPtr              - Connection Pointer
 *                pBuf                  - CRU Buffer Pointer
 * Output      :  pu4Error              - Error on failure 
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE 
 *****************************************************************************/
INT4
Ofc131PktAsyncConfigSetau4PortStatusMaskValidate (tOfp131AsyncConfigSet *
                                                  pOfp131AsyncConfigSet,
                                                  VOID *pConnPtr,
                                                  tCRU_BUF_CHAIN_HEADER * pBuf,
                                                  UINT4 *pu4Error)
{
    UNUSED_PARAM (pu4Error);
    UNUSED_PARAM (pConnPtr);
    UNUSED_PARAM (pBuf);
    UNUSED_PARAM (pOfp131AsyncConfigSet);
    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  Ofc131PktAsyncConfigSetau4FlowRemovedMaskValidate
 * Description :  This function is for validating Flow Removed Mask in
 *                AsyncConfig Set Request 
 * Input       :  pOfp131AsyncConfigSet - Pointer to Async Config Set Request
 *                pConnPtr              - Connection Pointer
 *                pBuf                  - CRU Buffer Pointer
 * Output      :  pu4Error              - Error on failure 
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE 
 ****************************************************************************/
INT4
Ofc131PktAsyncConfigSetau4FlowRemovedMaskValidate (tOfp131AsyncConfigSet *
                                                   pOfp131AsyncConfigSet,
                                                   VOID *pConnPtr,
                                                   tCRU_BUF_CHAIN_HEADER * pBuf,
                                                   UINT4 *pu4Error)
{
    UNUSED_PARAM (pu4Error);
    UNUSED_PARAM (pConnPtr);
    UNUSED_PARAM (pBuf);
    UNUSED_PARAM (pOfp131AsyncConfigSet);
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function    :  Ofc131PktMeterModu2CmdValidate
 * Description :  This function is for validating Command in Meter Mod
 * Input       :  pOfp131MeterMod  - Pointer to Meter Mod Message
 *                pConnPtr         - Connection Pointer
 *                pBuf             - CRU Buffer Pointer
 * Output      :  pu4Error         - Error on failure 
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE 
 *****************************************************************************/
INT4
Ofc131PktMeterModu2CmdValidate (tOfp131MeterMod * pOfp131MeterMod,
                                VOID *pConnPtr, tCRU_BUF_CHAIN_HEADER * pBuf,
                                UINT4 *pu4Error)
{
    OFC131_TRC_FUNC ((OFC131_FN_ENTRY, "FUNC:Ofc131MeterModu2CmdValidate\n"));
    if (pOfp131MeterMod->u2Cmd > OFC_TWO)
    {
        OFC131_TRC_FUNC ((OFC_PKT_TRC, "Command Range Failed value = %d",
                          pOfp131MeterMod->u2Cmd));
        *pu4Error = OFC131_METERMOD_U2CMD_VALIDATE_ERR;
        return OSIX_FAILURE;
    }
    UNUSED_PARAM (pu4Error);
    UNUSED_PARAM (pConnPtr);
    UNUSED_PARAM (pBuf);
    OFC131_TRC_FUNC ((OFC131_FN_EXIT, "FUNC:Ofc131MeterModu2CmdValidate\n"));
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function    :  Ofc131PktMeterModu2FlagsValidate
 * Description :  This function is for validating Flags in Meter Mod
 * Input       :  pOfp131MeterMod  - Pointer to Meter Mod Message
 *                pConnPtr         - Connection Pointer
 *                pBuf             - CRU Buffer Pointer
 * Output      :  pu4Error         - Error on failure 
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE 
 *****************************************************************************/
INT4
Ofc131PktMeterModu2FlagsValidate (tOfp131MeterMod * pOfp131MeterMod,
                                  VOID *pConnPtr,
                                  tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 *pu4Error)
{
    OFC131_TRC_FUNC ((OFC131_FN_ENTRY, "FUNC:Ofc131MeterModu2FlagsValidate\n"));

    if (pOfp131MeterMod->u2Flags & OFC131_INVALID_METERMOD_FLAGS)
    {
        *pu4Error = OFC131_METERMOD_U2FLAGS_VALIDATE_ERR;
        return OSIX_FAILURE;
    }
    UNUSED_PARAM (pConnPtr);
    UNUSED_PARAM (pBuf);
    OFC131_TRC_FUNC ((OFC131_FN_EXIT, "FUNC:Ofc131MeterModu2FlagsValidate\n"));

    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function    :  Ofc131PktMeterModu4MeterIdValidate
 * Description :  This function is for validating MeterId in Meter Mod
 * Input       :  pOfp131MeterMod  - Pointer to Meter Mod Message
 *                pConnPtr         - Connection Pointer
 *                pBuf             - CRU Buffer Pointer
 * Output      :  pu4Error         - Error on failure 
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE 
 *****************************************************************************/
INT4
Ofc131PktMeterModu4MeterIdValidate (tOfp131MeterMod * pOfp131MeterMod,
                                    VOID *pConnPtr,
                                    tCRU_BUF_CHAIN_HEADER * pBuf,
                                    UINT4 *pu4Error)
{
    tOfp131ErrMsg       Ofp131ErrorMsg;

    OFC131_TRC_FUNC ((OFC131_FN_ENTRY,
                      "FUNC:Ofc131MeterModu4MeterIdValidate\n"));

    MEMSET (&Ofp131ErrorMsg, OFC_ZERO, sizeof (Ofp131ErrorMsg));

    if ((pOfp131MeterMod->u2Cmd == OFPMC_ADD) ||
        (pOfp131MeterMod->u2Cmd == OFPMC_MODIFY))
    {
        /* Meter id starts with One,so ensure the boundary condition checking */
        if ((pOfp131MeterMod->u4MeterId < OFC_ONE) ||
            (pOfp131MeterMod->u4MeterId > OFPM_MAX))
        {
            Ofp131ErrorMsg.u2Type = OFPET_131_METER_MOD_FAILED;
            Ofp131ErrorMsg.u2Code = OFPMMFC_INVALID_METER;
            Ofc131PktErrMsgSend (&Ofp131ErrorMsg, pConnPtr,
                                 sizeof (Ofp131ErrorMsg.au1Data));
            return OSIX_FAILURE;
        }
    }
    else
    {
        /* Meter id starts with One,so ensure the boundary condition checking */
        if ((pOfp131MeterMod->u4MeterId < OFC_ONE) ||
            (pOfp131MeterMod->u4MeterId > OFPM_MAX))
        {
            /*
             * For Delete messages, to delete all the meters, meter id can be
             * OFPM_ALL
             */

            if (pOfp131MeterMod->u4MeterId != OFPM_ALL)
            {
                Ofp131ErrorMsg.u2Type = OFPET_131_METER_MOD_FAILED;
                Ofp131ErrorMsg.u2Code = OFPMMFC_INVALID_METER;
                Ofc131PktErrMsgSend (&Ofp131ErrorMsg, pConnPtr,
                                     sizeof (Ofp131ErrorMsg.au1Data));
                return OSIX_FAILURE;
            }
        }
    }
    UNUSED_PARAM (pu4Error);
    UNUSED_PARAM (pBuf);
    OFC131_TRC_FUNC ((OFC131_FN_EXIT,
                      "FUNC:Ofc131MeterModu4MeterIdValidate\n"));
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function    :  Ofc131PktMeterModOfpMeterBandHdrValidate
 * Description :  This function is for validating MeterBand in Meter Mod
 * Input       :  pOfp131MeterMod  - Pointer to Meter Mod Message
 *                pConnPtr         - Connection Pointer
 *                pBuf             - CRU Buffer Pointer
 * Output      :  pu4Error         - Error on failure 
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE 
 *****************************************************************************/
INT4
Ofc131PktMeterModOfpMeterBandHdrValidate (tOfp131MeterMod * pOfp131MeterMod,
                                          VOID *pConnPtr,
                                          tCRU_BUF_CHAIN_HEADER * pBuf,
                                          UINT4 *pu4Error)
{
    UNUSED_PARAM (pu4Error);
    UNUSED_PARAM (pConnPtr);
    UNUSED_PARAM (pBuf);
    UNUSED_PARAM (pOfp131MeterMod);
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function    :  Ofc131PktOfpu1VersionAction
 * Description :  This function is for validating Version in OpenFlow Message
 * Input       :  pOfp131Pkt  - Pointer to OpenFlow Message Packet
 *                pConnPtr    - Connection Pointer
 *                pBuf        - CRU Buffer Pointer
 * Output      :  pu4Error    - Error on failure 
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE 
 *****************************************************************************/
INT4
Ofc131PktOfpu1VersionAction (tOfp131Pkt * pOfp131Pkt, VOID *pConnPtr,
                             tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 *pu4Error)
{
    UNUSED_PARAM (pu4Error);
    UNUSED_PARAM (pConnPtr);
    UNUSED_PARAM (pBuf);
    UNUSED_PARAM (pOfp131Pkt);
    return OFC_SUCCESS;
}

/*****************************************************************************
 * Function    :  Ofc131PktOfpu1TypeAction
 * Description :  This function does the processing of each openflow message
 *                recieved, based on the type.
 * Input       :  pOfp131Pkt  - Pointer to OpenFlow Message Packet
 *                pConnPtr    - Connection Pointer
 *                pBuf        - CRU Buffer Pointer
 * Output      :  pu4Error    - Error on failure 
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE 
 *****************************************************************************/
INT4
Ofc131PktOfpu1TypeAction (tOfp131Pkt * pOfp131Pkt, VOID *pLocal,
                          tCRU_BUF_CHAIN_HEADER * pBuf1, UINT4 *pu4Error)
{
    tOfp131Hello        Hello;
    tOfp131EchoReqMsg   EchoReqMsg;
    tOfp131EchoRplyMsg  EchoRplyMsg;
    tOfp131ExperimenterHdr ExperimenterHdr;
    tOfp131FeatureReq   FeatureReq;
    tOfp131ConfigGet    ConfigGet;
    tOfp131ConfigSet    ConfigSet;
    tOfp131PktOut       PktOut;
    tOfp131FlowMod      FlowMod;
    tOfp131GroupMod     GroupMod;
    tOfp131PortMod      PortMod;
    tOfp131MultiPartReq MultiPartReq;
    tOfp131BarrierReq   BarrierReq;
    tOfp131QueueReq     QueueReq;
    tOfp131RoleReq      RoleReq;
    tOfp131AsyncConfigGet AsyncConfigGet;
    tOfp131AsyncConfigSet AsyncConfigSet;
    tOfp131MeterMod     MeterMod;
    tOfp131ErrMsg       Ofp131ErrorMsg;
    tCRU_BUF_CHAIN_HEADER *pBuf = pBuf1;
    tOfcFsofcControllerConnEntry *pConnPtr = NULL;
    INT4                i4RetVal = OSIX_SUCCESS;
    INT4                i4ControllerRole = OFC_ZERO;

    OFC131_TRC_FUNC ((OFC131_FN_ENTRY, "FUNC:Ofc131Ofpu1TypeAction\n"));

    pConnPtr = (tOfcFsofcControllerConnEntry *) pLocal;
    if (pOfp131Pkt->u1Type == OFP131T_ECHO_REPLY)
    {
        /* echo pairing failed */
        if (pConnPtr->u4TxXid != pOfp131Pkt->u4Xid)
        {
            return OSIX_SUCCESS;
        }
    }
    pConnPtr->u4RxXid = pOfp131Pkt->u4Xid;
    i4ControllerRole = pConnPtr->MibObject.i4FsofcControllerRole;
    switch (pOfp131Pkt->u1Type)
    {
        case OFP131T_HELLO:
            i4RetVal = Ofc131PktHelloParse (&Hello, pConnPtr, pBuf, pu4Error);
            break;

        case OFP131T_ERROR:
            /* We will not recieve error msgs from controller */
            break;

        case OFP131T_ECHO_REQUEST:
            i4RetVal = Ofc131PktEchoReqMsgParse (&EchoReqMsg, pConnPtr, pBuf,
                                                 pu4Error);
            break;

        case OFP131T_ECHO_REPLY:
            i4RetVal = Ofc131PktEchoRplyMsgParse (&EchoRplyMsg, pConnPtr, pBuf,
                                                  pu4Error);
            break;

        case OFP131T_EXPERIMENTER:
            MEMSET (&ExperimenterHdr, OFC_ZERO,
                    sizeof (tOfp131ExperimenterHdr));
            OFC131_GET_4_BYTE (pBuf,
                               OFC131_EXPERIMENTERHDR_U4EXPERIMENTER_OFFSET,
                               ExperimenterHdr.u4Experimenter);
            OFC131_GET_4_BYTE (pBuf, OFC131_EXPERIMENTERHDR_U4EXPTYPE_OFFSET,
                               ExperimenterHdr.u4ExpType);
            i4RetVal =
                Ofc131PktExperimenterHdrParse (&ExperimenterHdr, pConnPtr, pBuf,
                                               pu4Error);
            break;

        case OFP131T_FEATURES_REQUEST:
            i4RetVal = Ofc131PktFeatureReqParse (&FeatureReq, pConnPtr, pBuf,
                                                 pu4Error);
            break;

        case OFP131T_GET_CONFIG_REQUEST:
            i4RetVal = Ofc131PktConfigGetParse (&ConfigGet, pConnPtr, pBuf,
                                                pu4Error);
            break;

        case OFP131T_SET_CONFIG:
            MEMSET (&ConfigSet, OFC_ZERO, sizeof (tOfp131ConfigSet));
            OFC131_GET_2_BYTE (pBuf, OFC131_CONFIGSET_U2FLAGS_OFFSET,
                               ConfigSet.u2Flags);
            OFC131_GET_2_BYTE (pBuf, OFC131_CONFIGSET_U2MISSSENDLEN_OFFSET,
                               ConfigSet.u2MissSendLen);
            i4RetVal = Ofc131PktConfigSetParse (&ConfigSet, pConnPtr, pBuf,
                                                pu4Error);
            break;

        case OFP131T_PACKET_OUT:
            MEMSET (&PktOut, OFC_ZERO, sizeof (tOfp131PktOut));
            MEMSET (&Ofp131ErrorMsg, OFC_ZERO, sizeof (tOfp131ErrMsg));

            if (OPENFLOW_CONNROLE_SLAVE == i4ControllerRole)
            {
                Ofp131ErrorMsg.u2Type = OFPET_BAD_REQUEST;
                Ofp131ErrorMsg.u2Code = OFPBRC_IS_SLAVE;
                Ofc131PktErrMsgSend (&Ofp131ErrorMsg, pConnPtr,
                                     (INT4) sizeof (Ofp131ErrorMsg.au1Data));
                return OSIX_FAILURE;
            }
            OFC131_GET_4_BYTE (pBuf, OFC131_PKTOUT_U4BUFFERID_OFFSET,
                               PktOut.u4BufferId);
            OFC131_GET_4_BYTE (pBuf, OFC131_PKTOUT_U4INPORT_OFFSET,
                               PktOut.u4InPort);
            OFC131_GET_2_BYTE (pBuf, OFC131_PKTOUT_U2ACTIONSLEN_OFFSET,
                               PktOut.u2ActionsLen);
            OFC131_GET_N_BYTE (pBuf, OFC131_PKTOUT_AU1PAD_OFFSET, PktOut.au1Pad,
                               (OFC_SIX * sizeof (UINT1)));
            i4RetVal = Ofc131PktPktOutParse (&PktOut, pConnPtr, pBuf, pu4Error);
            break;

        case OFP131T_FLOW_MOD:
            MEMSET (&FlowMod, OFC_ZERO, sizeof (tOfp131FlowMod));
            MEMSET (&Ofp131ErrorMsg, OFC_ZERO, sizeof (tOfp131ErrMsg));

            if (OPENFLOW_CONNROLE_SLAVE == i4ControllerRole)
            {
                Ofp131ErrorMsg.u2Type = OFPET_BAD_REQUEST;
                Ofp131ErrorMsg.u2Code = OFPBRC_IS_SLAVE;
                Ofc131PktErrMsgSend (&Ofp131ErrorMsg, pConnPtr,
                                     (INT4) sizeof (Ofp131ErrorMsg.au1Data));
                return OSIX_FAILURE;
            }
            OFC131_GET_8_BYTE (pBuf, OFC131_FLOWMOD_U8COOKIE_OFFSET,
                               FlowMod.u8Cookie);
            OFC131_GET_8_BYTE (pBuf, OFC131_FLOWMOD_U8COOKIEMASK_OFFSET,
                               FlowMod.u8CookieMask);
            OFC131_GET_1_BYTE (pBuf, OFC131_FLOWMOD_U1TABLEID_OFFSET,
                               FlowMod.u1TableId);
            OFC131_GET_1_BYTE (pBuf, OFC131_FLOWMOD_U1COMMAND_OFFSET,
                               FlowMod.u1Command);
            OFC131_GET_2_BYTE (pBuf, OFC131_FLOWMOD_U2IDLETIMEOUT_OFFSET,
                               FlowMod.u2IdleTimeout);
            OFC131_GET_2_BYTE (pBuf, OFC131_FLOWMOD_U2HARDTIMEOUT_OFFSET,
                               FlowMod.u2HardTimeout);
            OFC131_GET_2_BYTE (pBuf, OFC131_FLOWMOD_U2PRIORITY_OFFSET,
                               FlowMod.u2Priority);
            OFC131_GET_4_BYTE (pBuf, OFC131_FLOWMOD_U4BUFID_OFFSET,
                               FlowMod.u4BufId);
            OFC131_GET_4_BYTE (pBuf, OFC131_FLOWMOD_U4OUTPORT_OFFSET,
                               FlowMod.u4OutPort);
            OFC131_GET_4_BYTE (pBuf, OFC131_FLOWMOD_U4OUTGROUP_OFFSET,
                               FlowMod.u4OutGroup);
            OFC131_GET_2_BYTE (pBuf, OFC131_FLOWMOD_U2FLAGS_OFFSET,
                               FlowMod.u2Flags);
            OFC131_GET_N_BYTE (pBuf, OFC131_FLOWMOD_AU1PAD_OFFSET,
                               FlowMod.au1Pad, OFC_TWO);
            i4RetVal = Ofc131PktFlowModParse (&FlowMod, pConnPtr, pBuf,
                                              pu4Error);
            break;

        case OFP131T_GROUP_MOD:
            MEMSET (&GroupMod, OFC_ZERO, sizeof (tOfp131GroupMod));
            MEMSET (&Ofp131ErrorMsg, OFC_ZERO, sizeof (tOfp131ErrMsg));

            if (OPENFLOW_CONNROLE_SLAVE == i4ControllerRole)
            {
                Ofp131ErrorMsg.u2Type = OFPET_BAD_REQUEST;
                Ofp131ErrorMsg.u2Code = OFPBRC_IS_SLAVE;
                Ofc131PktErrMsgSend (&Ofp131ErrorMsg, pConnPtr,
                                     (INT4) sizeof (Ofp131ErrorMsg.au1Data));
                return OSIX_FAILURE;
            }
            OFC131_GET_2_BYTE (pBuf, OFC131_GROUPMOD_U2CMD_OFFSET,
                               GroupMod.u2Cmd);
            OFC131_GET_1_BYTE (pBuf, OFC131_GROUPMOD_U1TYPE_OFFSET,
                               GroupMod.u1Type);
            OFC131_GET_1_BYTE (pBuf, OFC131_GROUPMOD_U1PAD_OFFSET,
                               GroupMod.u1Pad);
            OFC131_GET_4_BYTE (pBuf, OFC131_GROUPMOD_U4GROUPID_OFFSET,
                               GroupMod.u4GroupId);
            i4RetVal = Ofc131PktGroupModParse (&GroupMod, pConnPtr, pBuf,
                                               pu4Error);
            break;

        case OFP131T_PORT_MOD:
            MEMSET (&PortMod, OFC_ZERO, sizeof (tOfp131PortMod));
            MEMSET (&Ofp131ErrorMsg, OFC_ZERO, sizeof (tOfp131ErrMsg));

            if (OPENFLOW_CONNROLE_SLAVE == i4ControllerRole)
            {
                Ofp131ErrorMsg.u2Type = OFPET_BAD_REQUEST;
                Ofp131ErrorMsg.u2Code = OFPBRC_IS_SLAVE;
                Ofc131PktErrMsgSend (&Ofp131ErrorMsg, pConnPtr,
                                     (INT4) sizeof (Ofp131ErrorMsg.au1Data));
                return OSIX_FAILURE;
            }
            OFC131_GET_4_BYTE (pBuf, OFC131_PORTMOD_U4PORTNO_OFFSET,
                               PortMod.u4PortNo);
            OFC131_GET_N_BYTE (pBuf, OFC131_PORTMOD_AU1PAD_OFFSET,
                               PortMod.au1Pad, OFC_FOUR);
            OFC131_GET_N_BYTE (pBuf, OFC131_PORTMOD_AU1HWADDR_OFFSET,
                               PortMod.au1HwAddr, OFC_SIX);
            OFC131_GET_N_BYTE (pBuf, OFC131_PORTMOD_AU1PAD2_OFFSET,
                               PortMod.au1Pad2, OFC_TWO);
            OFC131_GET_4_BYTE (pBuf, OFC131_PORTMOD_U4CONFIG_OFFSET,
                               PortMod.u4Config);
            OFC131_GET_4_BYTE (pBuf, OFC131_PORTMOD_U4MASK_OFFSET,
                               PortMod.u4Mask);
            OFC131_GET_4_BYTE (pBuf, OFC131_PORTMOD_U4ADVERTISE_OFFSET,
                               PortMod.u4Advertise);
            OFC131_GET_N_BYTE (pBuf, OFC131_PORTMOD_AU1PAD3_OFFSET,
                               PortMod.au1Pad3, OFC_FOUR);
            i4RetVal = Ofc131PktPortModParse (&PortMod, pConnPtr, pBuf,
                                              pu4Error);
            break;

        case OFP131T_TABLE_MOD:
            /* DEPRECATED */
            break;

        case OFP131T_MULTIPART_REQUEST:
            MEMSET (&MultiPartReq, OFC_ZERO, sizeof (tOfp131MultiPartReq));
            OFC131_GET_2_BYTE (pBuf, OFC131_MULTIPARTREQ_U2TYPE_OFFSET,
                               MultiPartReq.u2Type);
            OFC131_GET_2_BYTE (pBuf, OFC131_MULTIPARTREQ_U2FLAGS_OFFSET,
                               MultiPartReq.u2Flags);
            OFC131_GET_N_BYTE (pBuf, OFC131_MULTIPARTREQ_AU1PAD_OFFSET,
                               MultiPartReq.au1Pad, OFC_FOUR);
            i4RetVal = Ofc131PktMultiPartReqParse (&MultiPartReq, pConnPtr,
                                                   pBuf, pu4Error);
            break;

        case OFP131T_BARRIER_REQUEST:
            i4RetVal = Ofc131PktBarrierReqParse (&BarrierReq, pConnPtr, pBuf,
                                                 pu4Error);
            break;

        case OFP131T_QUEUE_GET_CONFIG_REQUEST:
            MEMSET (&QueueReq, OFC_ZERO, sizeof (tOfp131QueueReq));
            OFC131_GET_4_BYTE (pBuf, OFC131_QUEUEREQ_U4PORT_OFFSET,
                               QueueReq.u4Port);
            OFC131_GET_N_BYTE (pBuf, OFC131_QUEUEREQ_AU1PAD_OFFSET,
                               QueueReq.au1Pad, OFC_FOUR);
            i4RetVal = Ofc131PktQueueReqParse (&QueueReq, pConnPtr, pBuf,
                                               pu4Error);
            break;

        case OFP131T_ROLE_REQUEST:
            MEMSET (&RoleReq, OFC_ZERO, sizeof (tOfp131RoleReq));
            OFC131_GET_4_BYTE (pBuf, OFC131_ROLEREQ_U4ROLE_OFFSET,
                               RoleReq.u4Role);
            OFC131_GET_N_BYTE (pBuf, OFC131_ROLEREQ_AU1PAD_OFFSET,
                               RoleReq.au1Pad, OFC_FOUR);
            OFC131_GET_8_BYTE (pBuf, OFC131_ROLEREQ_U8GENID_OFFSET,
                               RoleReq.u8GenId);
            i4RetVal = Ofc131PktRoleReqParse (&RoleReq, pConnPtr, pBuf,
                                              pu4Error);
            break;

        case OFP131T_GET_ASYNC_CONFIG_REQUEST:
            i4RetVal = Ofc131PktAsyncConfigGetParse (&AsyncConfigGet, pConnPtr,
                                                     pBuf, pu4Error);
            break;

        case OFP131T_SET_ASYNC:
            MEMSET (&AsyncConfigSet, OFC_ZERO, sizeof (tOfp131AsyncConfigSet));
            OFC131_GET_N_BYTE (pBuf, OFC131_ASYNCCONFIGSET_AU4PKTINMASK_OFFSET,
                               AsyncConfigSet.au4PktInMask, OFC_EIGHT);
            OFC131_GET_N_BYTE (pBuf,
                               OFC131_ASYNCCONFIGSET_AU4PORTSTATUSMASK_OFFSET,
                               AsyncConfigSet.au4PortStatusMask, OFC_EIGHT);
            OFC131_GET_N_BYTE (pBuf,
                               OFC131_ASYNCCONFIGSET_AU4FLOWREMOVEDMASK_OFFSET,
                               AsyncConfigSet.au4FlowRemovedMask, OFC_EIGHT);
            i4RetVal = Ofc131PktAsyncConfigSetParse (&AsyncConfigSet, pConnPtr,
                                                     pBuf, pu4Error);
            break;

        case OFP131T_METER_MOD:
            MEMSET (&MeterMod, OFC_ZERO, sizeof (tOfp131MeterMod));
            OFC131_GET_2_BYTE (pBuf, OFC131_METERMOD_U2CMD_OFFSET,
                               MeterMod.u2Cmd);
            OFC131_GET_2_BYTE (pBuf, OFC131_METERMOD_U2FLAGS_OFFSET,
                               MeterMod.u2Flags);
            OFC131_GET_4_BYTE (pBuf, OFC131_METERMOD_U4METERID_OFFSET,
                               MeterMod.u4MeterId);
            i4RetVal = Ofc131PktMeterModParse (&MeterMod, pConnPtr, pBuf,
                                               pu4Error);
            break;

        default:
            break;
    }
    OFC131_TRC_FUNC ((OFC131_FN_EXIT, "FUNC:Ofc131Ofpu1TypeAction\n"));
    return i4RetVal;
}

/*****************************************************************************
 * Function    :  Ofc131PktOfpu2LengthAction
 * Description :  This function is for processing length of openflow packet
 * Input       :  pOfp131Pkt  - Pointer to OpenFlow Message Packet
 *                pConnPtr    - Connection Pointer
 *                pBuf        - CRU Buffer Pointer
 * Output      :  pu4Error    - Error on failure 
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE 
 *****************************************************************************/
INT4
Ofc131PktOfpu2LengthAction (tOfp131Pkt * pOfp131Pkt, VOID *pConnPtr,
                            tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 *pu4Error)
{
    UNUSED_PARAM (pu4Error);
    UNUSED_PARAM (pConnPtr);
    UNUSED_PARAM (pBuf);
    UNUSED_PARAM (pOfp131Pkt);
    return OFC_SUCCESS;
}

/*****************************************************************************
 * Function    :  Ofc131PktOfpu4XidAction
 * Description :  This function is for processing Xid of openflow packet
 * Input       :  pOfp131Pkt  - Pointer to OpenFlow Message Packet
 *                pConnPtr    - Connection Pointer
 *                pBuf        - CRU Buffer Pointer
 * Output      :  pu4Error    - Error on failure 
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE 
 *****************************************************************************/
INT4
Ofc131PktOfpu4XidAction (tOfp131Pkt * pOfp131Pkt, VOID *pConnPtr,
                         tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 *pu4Error)
{
    UNUSED_PARAM (pu4Error);
    UNUSED_PARAM (pConnPtr);
    UNUSED_PARAM (pBuf);
    UNUSED_PARAM (pOfp131Pkt);
    return OFC_SUCCESS;
}

/*****************************************************************************
 * Function    :  Ofc131PktHelloOfpHelloElemHdrAction
 * Description :  This function is for processing Xid of openflow packet
 * Input       :  pOfp131Hello - Pointer to OpenFlow Hello Message
 *                pConnPtr     - Connection Pointer
 *                pBuf         - CRU Buffer Pointer
 * Output      :  pu4Error     - Error on failure 
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE 
 *****************************************************************************/
INT4
Ofc131PktHelloOfpHelloElemHdrAction (tOfp131Hello * pOfp131Hello,
                                     VOID *pConnPtr,
                                     tCRU_BUF_CHAIN_HEADER * pBuf,
                                     UINT4 *pu4Error)
{
    UNUSED_PARAM (pu4Error);
    UNUSED_PARAM (pBuf);
    Ofc131PktHelloSend (pOfp131Hello, pConnPtr, OFC_FOUR);
    return OFC_SUCCESS;
}

/*****************************************************************************
 * Function    :  Ofc131PktEchoReqMsgau1DataAction
 * Description :  This function is for processing Data of Echo Request 
 * Input       :  pOfp131EchoReqMsg - Pointer to Echo Request
 *                pConnPtr          - Connection Pointer
 *                pBuf              - CRU Buffer Pointer
 * Output      :  pu4Error          - Error on failure 
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE 
 *****************************************************************************/
INT4
Ofc131PktEchoReqMsgau1DataAction (tOfp131EchoReqMsg * pOfp131EchoReqMsg,
                                  VOID *pConnPtr, tCRU_BUF_CHAIN_HEADER * pBuf,
                                  UINT4 *pu4Error)
{

    tOfcFsofcControllerConnEntry *pLocal = NULL;
    UINT4               u4VarLen = OFC_ZERO;

    OFC131_TRC_FUNC ((OFC131_FN_ENTRY, "FUNC:Ofc131EchoReqMsgau1DataAction\n"));

    pLocal = (tOfcFsofcControllerConnEntry *) pConnPtr;
    u4VarLen = pLocal->u4RxLength - OFC131_ECHOREQMSG_AU1DATA_OFFSET;

    /* Typecast Request to Reply, so data copy is not required */
    if ((Ofc131PktEchoRplyMsgSend ((tOfp131EchoRplyMsg *) pOfp131EchoReqMsg,
                                   pConnPtr, u4VarLen)) != OSIX_SUCCESS)
    {
        OFC131_TRC_FUNC ((OFC_PKT_TRC, "Ofc131PktEchoRplyMsgSend Failed\n"));
    }
    UNUSED_PARAM (pu4Error);
    UNUSED_PARAM (pBuf);
    OFC131_TRC_FUNC ((OFC131_FN_EXIT, "FUNC:Ofc131EchoReqMsgau1DataAction\n"));
    return OFC_SUCCESS;
}

/*****************************************************************************
 * Function    :  Ofc131PktEchoRplyMsgau1DataAction
 * Description :  This function is for processing Data of Echo Reply 
 * Input       :  pOfp131EchoRplyMsg - Pointer to Echo Reply
 *                pConnPtr           - Connection Pointer
 *                pBuf               - CRU Buffer Pointer
 * Output      :  pu4Error           - Error on failure 
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE 
 *****************************************************************************/
INT4
Ofc131PktEchoRplyMsgau1DataAction (tOfp131EchoRplyMsg * pOfp131EchoRplyMsg,
                                   VOID *pConnPtr, tCRU_BUF_CHAIN_HEADER * pBuf,
                                   UINT4 *pu4Error)
{
    UNUSED_PARAM (pu4Error);
    UNUSED_PARAM (pConnPtr);
    UNUSED_PARAM (pBuf);
    UNUSED_PARAM (pOfp131EchoRplyMsg);
    return OFC_SUCCESS;
}

/****************************************************************************
 * Function    :  Ofc131PktExperimenterHdru4ExperimenterAction
 * Description :  This function is for processing Experimenter Header  
 * Input       :  pOfp131ExperimenterHdr - Pointer to Experimenter Request
 *                pConnPtr               - Connection Pointer
 *                pBuf                   - CRU Buffer Pointer
 * Output      :  pu4Error               - Error on failure 
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE 
 ****************************************************************************/
INT4
Ofc131PktExperimenterHdru4ExperimenterAction (tOfp131ExperimenterHdr *
                                              pOfp131ExperimenterHdr,
                                              VOID *pConnPtr,
                                              tCRU_BUF_CHAIN_HEADER * pBuf,
                                              UINT4 *pu4Error)
{
    UNUSED_PARAM (pu4Error);
    UNUSED_PARAM (pConnPtr);
    UNUSED_PARAM (pBuf);
    UNUSED_PARAM (pOfp131ExperimenterHdr);
    return OFC_SUCCESS;
}

/******************************************************************************
 * Function    :  Ofc131PktExperimenterHdru4ExpTypeAction
 * Description :  This function is for processing Experimenter Type 
 * Input       :  pExprHdr - Pointer to the Experimenter header
 *                pConnPtr - Controller connection pointer
 *                pBuf     - Pointer to openflow packet CRU Buffer
 * Output      :  pu4Error - Error on failure 
 * Returns     :  OFC_SUCCESS or OFC_FAILURE 
 *****************************************************************************/
INT4
Ofc131PktExperimenterHdru4ExpTypeAction (tOfp131ExperimenterHdr *
                                         pOfp131ExperimenterHdr, VOID *pConnPtr,
                                         tCRU_BUF_CHAIN_HEADER * pBuf,
                                         UINT4 *pu4Error)
{
    /* 
     * We don't support any experimenter messages for now. Any additions
     * can be appropriately included here.
     */
    *pu4Error = OFC131_EXPERIMENTERHDR_U4EXPTYPE_VALIDATE_ERR;
    UNUSED_PARAM (pConnPtr);
    UNUSED_PARAM (pBuf);
    UNUSED_PARAM (pOfp131ExperimenterHdr);

    /* We don't support any experimenter messages for now. Any additions
     * can be appropriately included here.
     */
    *pu4Error = OFC131_EXPERIMENTERHDR_U4EXPTYPE_VALIDATE_ERR;
    OFC131_TRC_FUNC ((OFC131_FN_EXIT, "Ofc131ExperimenterHdru4ExpTypeAction"));
    return OFC_FAILURE;
}

/*****************************************************************************
 * Function    :  Ofc131PktFeatureReqau1DummyAction
 * Description :  This function is for processing dummy in packet feature  
 * Input       :  pOfp131FeatureReq - Pointer to Packet Feature Request
 *                pConnPtr          - Connection Pointer
 *                pBuf              - CRU Buffer Pointer
 * Output      :  pu4Error          - Error on failure 
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE 
 *****************************************************************************/
INT4
Ofc131PktFeatureReqau1DummyAction (tOfp131FeatureReq * pOfp131FeatureReq,
                                   VOID *pConnPtr, tCRU_BUF_CHAIN_HEADER * pBuf,
                                   UINT4 *pu4Error)
{
    tOfp131FeatureRply  FeatureRply;
    INT4                i4RetVal = OSIX_SUCCESS;
    UINT1              *pu1DataPathId = NULL;
    UINT1              *pu1Mac = NULL;

    tOfcFsofcControllerConnEntry *pLocal = pConnPtr;
    tOfcFsofcCfgEntry  *pOfcFsofcCfgEntry = NULL;

    OFC131_TRC_FUNC ((OFC131_FN_ENTRY,
                      "\r\nFUNC:Ofc131FeatureReqau1DummyAction\n"));

    MEMSET (&FeatureRply, OFC_ZERO, sizeof (tOfp131FeatureRply));

    pOfcFsofcCfgEntry =
        OfcGetFsofcCfgEntry (pLocal->MibObject.u4FsofcContextId);
    if (pOfcFsofcCfgEntry == NULL)
    {
        OFC131_TRC_FUNC ((OFC_PKT_TRC, "\r\nOfc131PktFeatureReqau1DummyAction "
                          "Get CFG Entry Failed\n"));
        return OSIX_FAILURE;
    }

    /* Update Data Path ID */
    pu1DataPathId = (UINT1 *) &FeatureRply.u8DatapathId;
    /* Upper two bits are implementer defined */
    pu1DataPathId[OFC_ZERO] = OFC_ZERO;
    pu1DataPathId[OFC_ONE] =
        (UINT1) pOfcFsofcCfgEntry->MibObject.u4FsofcContextId;
    if ((pu1Mac = IssGetSwitchMacFromNvRam ()) != NULL)
    {
        MEMCPY (&pu1DataPathId[OFC_TWO], pu1Mac, MAC_ADDR_LEN);
    }

    FeatureRply.u4NoOfBuf = OFP_MAX_BUF_CACHE;
    FeatureRply.u1NoOfTables = (UINT1) pOfcFsofcCfgEntry->u2NumOfTables;
    FeatureRply.u4Capabilities = (OFPC_131_PORT_STATS |
                                  OFPC_131_FLOW_STATS |
                                  OFPC_131_TABLE_STATS | OFPC_131_GROUP_STATS);

    if (pOfcFsofcCfgEntry->MibObject.i4FsofcPortStpStatus == OPENFLOW_ENABLE)
    {
        FeatureRply.u4Capabilities = FeatureRply.u4Capabilities |
            OFPC_131_PORT_BLOCKED;
    }

    if (pOfcFsofcCfgEntry->MibObject.i4FsofcIpReassembleStatus ==
        OPENFLOW_ENABLE)
    {
        FeatureRply.u4Capabilities = FeatureRply.u4Capabilities |
            OFPC_131_IP_REASM;
    }

    i4RetVal = Ofc131PktFeatureRplySend (&FeatureRply, pConnPtr,
                                         OFP131_VARLEN_ZERO);
    if (i4RetVal != OSIX_SUCCESS)
    {
        OFC131_TRC_FUNC ((OFC_PKT_TRC, "\r\nOfc131PktFeatureReqau1DummyAction: "
                          "Ofc131PktFeatureRplySend Failed\n"));
    }

    UNUSED_PARAM (pu4Error);
    UNUSED_PARAM (pBuf);
    UNUSED_PARAM (pOfp131FeatureReq);
    OFC131_TRC_FUNC ((OFC131_FN_EXIT,
                      "\r\nFUNC:Ofc131FeatureReqau1DummyAction\n"));
    return i4RetVal;
}

/*****************************************************************************
 * Function    :  Ofc131PktConfigGetau1DummyAction
 * Description :  This function is for processing Dummy in Config Get Request
 * Input       :  pOfp131ConfigGet - Pointer to Config Get Request
 *                pConnPtr         - Connection Pointer
 *                pBuf             - CRU Buffer Pointer
 * Output      :  pu4Error         - Error on failure 
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE 
 *****************************************************************************/
INT4
Ofc131PktConfigGetau1DummyAction (tOfp131ConfigGet * pOfp131ConfigGet,
                                  VOID *pConnPtr, tCRU_BUF_CHAIN_HEADER * pBuf,
                                  UINT4 *pu4Error)
{
    tOfp131ConfigRply   Ofp131ConfigRply;
    INT4                i4RetVal = OSIX_SUCCESS;

    OFC131_TRC_FUNC ((OFC131_FN_ENTRY, "FUNC:Ofc131ConfigGetau1DummyAction\n"));

    MEMSET (&Ofp131ConfigRply, OFC_ZERO, sizeof (tOfp131ConfigRply));
    Ofp131ConfigRply.u2Flags = OFC_ZERO;    /* No special handling for fragments */
    Ofp131ConfigRply.u2MissSendLen = (UINT2) OFPCML_NO_BUFFER;
    i4RetVal = Ofc131PktConfigRplySend (&Ofp131ConfigRply, pConnPtr,
                                        OFP131_VARLEN_ZERO);

    if (i4RetVal != OSIX_SUCCESS)
    {
        OFC131_TRC_FUNC ((OFC_PKT_TRC, "Ofc131PktConfigRplySend Failed\n"));
    }
    UNUSED_PARAM (pu4Error);
    UNUSED_PARAM (pBuf);
    UNUSED_PARAM (pOfp131ConfigGet);
    OFC131_TRC_FUNC ((OFC131_FN_EXIT, "FUNC:Ofc131ConfigGetau1DummyAction\n"));
    return i4RetVal;
}

/*****************************************************************************
 * Function    :  Ofc131PktConfigSetu2FlagsAction
 * Description :  This function is for processing Flags in Config Set Request  
 * Input       :  pOfp131ConfigSet - Pointer to Config Set Request
 *                pConnPtr         - Connection Pointer
 *                pBuf             - CRU Buffer Pointer
 * Output      :  pu4Error         - Error on failure 
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE 
 *****************************************************************************/
INT4
Ofc131PktConfigSetu2FlagsAction (tOfp131ConfigSet * pOfp131ConfigSet,
                                 VOID *pConnPtr, tCRU_BUF_CHAIN_HEADER * pBuf,
                                 UINT4 *pu4Error)
{
    UNUSED_PARAM (pu4Error);
    UNUSED_PARAM (pConnPtr);
    UNUSED_PARAM (pBuf);
    UNUSED_PARAM (pOfp131ConfigSet);
    return OFC_SUCCESS;
}

/*****************************************************************************
 * Function    :  Ofc131PktConfigSetu2MissSendLenAction
 * Description :  This function is for processing MissSendLen in Config Set  
 * Input       :  pOfp131ConfigSet - Pointer to Config Set Request
 *                pConnPtr         - Connection Pointer
 *                pBuf             - CRU Buffer Pointer
 * Output      :  pu4Error         - Error on failure 
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE 
 *****************************************************************************/
INT4
Ofc131PktConfigSetu2MissSendLenAction (tOfp131ConfigSet * pOfp131ConfigSet,
                                       VOID *pConnPtr,
                                       tCRU_BUF_CHAIN_HEADER * pBuf,
                                       UINT4 *pu4Error)
{
    UNUSED_PARAM (pu4Error);
    UNUSED_PARAM (pConnPtr);
    UNUSED_PARAM (pBuf);
    UNUSED_PARAM (pOfp131ConfigSet);
    return OFC_SUCCESS;
}

/*****************************************************************************
 * Function    :  Ofc131PktPktOutu4BufferIdAction
 * Description :  This function is for processing BufferId in Packet Out  
 * Input       :  pOfp131PktOut - Pointer to Packet Out Request
 *                pConnPtr      - Connection Pointer
 *                pBuf          - CRU Buffer Pointer
 * Output      :  pu4Error      - Error on failure 
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE 
 *****************************************************************************/
INT4
Ofc131PktPktOutu4BufferIdAction (tOfp131PktOut * pOfp131PktOut, VOID *pConnPtr,
                                 tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 *pu4Error)
{
    UNUSED_PARAM (pu4Error);
    UNUSED_PARAM (pConnPtr);
    UNUSED_PARAM (pBuf);
    UNUSED_PARAM (pOfp131PktOut);
    return OFC_SUCCESS;
}

/*****************************************************************************
 * Function    :  Ofc131PktPktOutu4InPortAction
 * Description :  This function is for processing InPort in Packet Out  
 * Input       :  pOfp131PktOut - Pointer to Packet Out Request
 *                pConnPtr      - Connection Pointer
 *                pBuf          - CRU Buffer Pointer
 * Output      :  pu4Error      - Error on failure 
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE 
 *****************************************************************************/
INT4
Ofc131PktPktOutu4InPortAction (tOfp131PktOut * pOfp131PktOut, VOID *pConnPtr,
                               tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 *pu4Error)
{
    UNUSED_PARAM (pu4Error);
    UNUSED_PARAM (pConnPtr);
    UNUSED_PARAM (pBuf);
    UNUSED_PARAM (pOfp131PktOut);
    return OFC_SUCCESS;
}

/*****************************************************************************
 * Function    :  Ofc131PktPktOutu2ActionsLenAction
 * Description :  This function is for processing Actions in Packet Out
 * Input       :  pOfp131PktOut - Pointer to Packet Out Request
 *                pConnPtr      - Connection Pointer
 *                pBuf          - CRU Buffer Pointer
 * Output      :  pu4Error      - Error on failure 
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE 
 *****************************************************************************/
INT4
Ofc131PktPktOutu2ActionsLenAction (tOfp131PktOut * pOfp131PktOut,
                                   VOID *pConnPtr, tCRU_BUF_CHAIN_HEADER * pBuf,
                                   UINT4 *pu4Error)
{
    UNUSED_PARAM (pu4Error);
    UNUSED_PARAM (pConnPtr);
    UNUSED_PARAM (pBuf);
    UNUSED_PARAM (pOfp131PktOut);
    return OFC_SUCCESS;
}

/*****************************************************************************
 * Function    :  Ofc131PktPktOutau1PadAction
 * Description :  This function is for processing Padding in Packet Out
 * Input       :  pOfp131PktOut - Pointer to Packet Out Request
 *                pConnPtr      - Connection Pointer
 *                pBuf          - CRU Buffer Pointer
 * Output      :  pu4Error      - Error on failure 
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE 
 *****************************************************************************/
INT4
Ofc131PktPktOutau1PadAction (tOfp131PktOut * pOfp131PktOut, VOID *pConnPtr,
                             tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 *pu4Error)
{
    UNUSED_PARAM (pu4Error);
    UNUSED_PARAM (pConnPtr);
    UNUSED_PARAM (pBuf);
    UNUSED_PARAM (pOfp131PktOut);
    return OFC_SUCCESS;
}

/****************************************************************************
 * Function    :  Ofc131PktHandleActions
 * Description :  This function applies all actions in action list for the
 *                data frame in PacketOut message
 * Input       :  pOfp131PktOut - Pointer to Packet Out Request
                  pBuf          - CRU Buffer Pointer
                  pActList      - Pointer to Actions List
                  u4ContextId   - Value of the Context ID
 * Output      :  None 
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE 
 *****************************************************************************/
PRIVATE INT4
Ofc131PktHandleActions (tOfp131PktOut * pOfp131PktOut,
                        tCRU_BUF_CHAIN_HEADER * pBuf,
                        tOfcSll * pActList, UINT4 u4ContextId)
{
    tOfcSll             ActOutPortList;
    tOfcActOutputPort  *pActOutput = NULL;
    tOfcFsofcIfEntry    IfEntry;
    tOfcFsofcGroupEntry GroupEntry;
    FS_UINT8            u8OrigCounter;
    FS_UINT8            u8TempCounter;
    tOfcFsofcGroupEntry *pGroupEntry = NULL;
    tOfcFsofcIfEntry   *pOfcFsofcIfEntry = NULL;
    tCRU_BUF_CHAIN_HEADER *pDupBuf = NULL;
    INT4                i4GroupId = OFC_INVALID_GROUP;
    UINT4               u4NoRecv = OFC_ZERO;
    UINT4               u4NoFwd = OFC_ZERO;
    UINT4               u4PktSize = OFC_ZERO;
    UINT4               u4RetVal = OSIX_SUCCESS;
    UINT1               u1OperStatus = OFC_ZERO;

    OFC131_TRC_FUNC ((OFC131_FN_ENTRY, "FUNC:Ofc131PktHandleActions\n"));

    MEMSET (&GroupEntry, OFC_ZERO, sizeof (tOfcFsofcGroupEntry));
    MEMSET (&IfEntry, OFC_ZERO, sizeof (tOfcFsofcIfEntry));

    u4PktSize = CRU_BUF_Get_ChainValidByteCount (pBuf);

    TMO_SLL_Init (&ActOutPortList);
    /* Check if the no recv flag is set for this input port */
    IfEntry.MibObject.u4FsofcContextId = u4ContextId;
    IfEntry.MibObject.i4FsofcIfIndex = (INT4) pOfp131PktOut->u4InPort;

    if (OfcGetAllFsofcIfTable (&IfEntry) == OFC_SUCCESS)
    {
        u4NoRecv = (IfEntry.u4Config) >> OFC_TWO;

        if (u4NoRecv & OFC_ONE)
        {
            /* Drop the packet(s) received by this port */
            OFC131_TRC_FUNC ((OFC_ACTS_TRC,
                              "Drop the Packet, return FAILURE\n"));
            return OSIX_FAILURE;
        }

    }

    if (Ofc131ActsApplyActionList (pBuf, pActList, &i4GroupId, &ActOutPortList)
        == OSIX_FAILURE)
    {
        OFC131_TRC_FUNC ((OFC_PKT_TRC, "Apply Action, " "return FAILURE\n"));
        OfcActOutputPortListFree (&ActOutPortList);
        return OSIX_FAILURE;
    }

    /* If the Group is set, send it to group for further processing */
    if ((i4GroupId != OFC_INVALID_GROUP))
    {
        while ((UINT4) i4GroupId < OFCG_MAX)
        {
            /* Get the group entry */
            GroupEntry.MibObject.u4FsofcContextId = u4ContextId;
            GroupEntry.MibObject.u4FsofcGroupIndex = (UINT4) i4GroupId;
            pGroupEntry = RBTreeGet (gOfcGlobals.OfcGlbMib.FsofcGroupTable,
                                     (tRBElem *) & (GroupEntry));
            if (pGroupEntry == NULL)
            {
                OFC_TRC_FUNC ((OFC_ACTS_TRC, "No Group entry corresponding to"
                               " the given i4GroupId" "return FAILURE\n"));
                CfaIfSetInDiscard (pOfp131PktOut->u4InPort);
                OfcActOutputPortListFree (&ActOutPortList);
                return OFC_FAILURE;
            }

            /* Update the Group Statistics */
            /* Packet Count */
            u8OrigCounter.u4Lo =
                pGroupEntry->MibObject.u8FsofcGroupPacketCount.lsn;
            u8OrigCounter.u4Hi =
                pGroupEntry->MibObject.u8FsofcGroupPacketCount.msn;
            FSAP_U8_INC (&u8OrigCounter);
            pGroupEntry->MibObject.u8FsofcGroupPacketCount.lsn =
                u8OrigCounter.u4Lo;
            pGroupEntry->MibObject.u8FsofcGroupPacketCount.msn =
                u8OrigCounter.u4Hi;

            /* Byte Count */
            u8OrigCounter.u4Lo =
                pGroupEntry->MibObject.u8FsofcGroupByteCount.lsn;
            u8OrigCounter.u4Hi =
                pGroupEntry->MibObject.u8FsofcGroupByteCount.msn;
            u8TempCounter.u4Lo = u4PktSize;
            u8TempCounter.u4Hi = OFC_ZERO;
            FSAP_U8_ADD (&u8TempCounter, &u8OrigCounter, &u8TempCounter);
            pGroupEntry->MibObject.u8FsofcGroupByteCount.lsn =
                u8TempCounter.u4Lo;
            pGroupEntry->MibObject.u8FsofcGroupByteCount.msn =
                u8OrigCounter.u4Hi;

            /*
             * i4GroupId will be changed as part of the group processing
             * accordingly, else below line will help to break the loop.
             */
            i4GroupId = OFCG_MAX;

            /* Apply Actions List in Group */
            u4RetVal = Ofc131ActsApplyActionList (pBuf, pActList,
                                                  &i4GroupId, &ActOutPortList);
            if (OSIX_FAILURE == u4RetVal)
            {
                /* Apply Action List in Group Failed */
                OFC131_TRC_FUNC ((OFC_PKT_TRC,
                                  "Apply action in group FAILURE\n"));
                OfcActOutputPortListFree (&ActOutPortList);
                return OSIX_FAILURE;
            }
        }
    }

#ifdef DPA_WANTED
    if (TMO_SLL_Count (&ActOutPortList) != OFC_ZERO)
    {
        TMO_SLL_Scan (&ActOutPortList, pActOutput, tOfcActOutputPort *)
        {

            if (pActOutput->OfcOutput.u4Port != OFC_INVALID_PORT)
            {
                if (Ofc131DpaPacketSend (pBuf, pOfp131PktOut->u4InPort,
                                         pActOutput->OfcOutput.u4Port,
                                         u4PktSize) != OFC_SUCCESS)
                {
                    return OSIX_FAILURE;
                }
                return OSIX_SUCCESS;
            }
        }
    }
    else
    {
        return OSIX_FAILURE;
    }
#endif /* DPA_WANTED */

    /* If OutputPort is set, send the packet to specified port 
       Only Valid Output Ports will be in SLL. */
    if (TMO_SLL_Count (&ActOutPortList) != OFC_ZERO)
    {
        TMO_SLL_Scan (&ActOutPortList, pActOutput, tOfcActOutputPort *)
        {
            if (pActOutput->OfcOutput.u4Port == OFPP_FLOOD)
            {
                pOfcFsofcIfEntry = (tOfcFsofcIfEntry *)
                    RBTreeGetFirst (gOfcGlobals.OfcGlbMib.FsofcIfTable);

                if (pOfcFsofcIfEntry != NULL)
                {
                    do
                    {
                        /* Send to all ports except ingress port */
                        if ((pOfcFsofcIfEntry->MibObject.i4FsofcIfIndex !=
                             (INT4) pOfp131PktOut->u4InPort) &&
                            (pOfcFsofcIfEntry->u4VlanId == OFC_ZERO))
                        {
                            /*
                             * Do not send out on logical interfaces as it will be
                             * already send on member ports of logical interface
                             */
                            OfcInterfacePacketSend (pBuf,
                                                    (UINT4)
                                                    pOfcFsofcIfEntry->MibObject.
                                                    i4FsofcIfIndex, u4PktSize);
                        }
                    }
                    while ((pOfcFsofcIfEntry =
                            (tOfcFsofcIfEntry *)
                            RBTreeGetNext (gOfcGlobals.OfcGlbMib.FsofcIfTable,
                                           pOfcFsofcIfEntry, NULL)) != NULL);
                }
            }
            /* If OutputPort is set, send the packet to specified port */
            else if ((INT2) pActOutput->OfcOutput.u4Port != OFC_INVALID_PORT)
            {
                if (pActOutput->OfcOutput.u4Port == OFPP_TABLE)
                {
                    /*
                     * Submit the Packet to Pipeline process 
                     * Duplicate the CRU Buf to give it to Pipeline
                     */
                    pDupBuf = CRU_BUF_Duplicate_BufChain (pBuf);

                    if (pDupBuf == NULL)
                    {
                        OFC131_TRC_FUNC ((OFC_PKT_TRC,
                                          "Duplicate CRU BufFAILURE\n"));
                        OfcActOutputPortListFree (&ActOutPortList);
                        return OSIX_FAILURE;
                    }
                    return (Ofc131ActsPipelineProcess
                            (OFPP_CONTROLLER, u4ContextId, pDupBuf));
                }

                /* Check whether the output port is up */
                CfaGetIfOperStatus (pActOutput->OfcOutput.u4Port,
                                    &(u1OperStatus));
                if (u1OperStatus == CFA_IF_DOWN)
                {
                    OFC131_TRC_FUNC ((OFC_PKT_TRC, "Output IF Down, "
                                      "return FAILURE\n"));
                    OfcActOutputPortListFree (&ActOutPortList);
                    return OSIX_FAILURE;
                }

                /* Check if the no forward flag is set for this ouput port */
                IfEntry.MibObject.u4FsofcContextId = u4ContextId;
                IfEntry.MibObject.i4FsofcIfIndex =
                    (INT4) pActOutput->OfcOutput.u4Port;
                if (OfcGetAllFsofcIfTable (&IfEntry) == OFC_SUCCESS)
                {
                    u4NoFwd = (IfEntry.u4Config) >> OFC_FIVE;
                    if (u4NoFwd & OFC_ONE)
                    {
                        OFC131_TRC_FUNC ((OFC_PKT_TRC, "Drop the Packet, "
                                          "return FAILURE\n"));
                        OfcActOutputPortListFree (&ActOutPortList);
                        return OSIX_FAILURE;
                    }
                }
                /* Sent the packet out */
                if (u4PktSize > OFC_ZERO)
                {
                    OfcInterfacePacketSend (pBuf, pActOutput->OfcOutput.u4Port,
                                            u4PktSize);
                }
            }
        }
        OfcActOutputPortListFree (&ActOutPortList);
    }

    OFC131_TRC_FUNC ((OFC131_FN_EXIT, "FUNC:Ofc131PktHandleActions\n"));
    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  Ofc131PktParseSetFld
 * Description :  This function is processing SetFields from Action Header
 *                and adds to the ActSetFld SLL 
 * Input       :  u1TableId     - Table Identifier
 *                pOfcActSetFld - Pointer to Set Fields
 *                i4SetFldLen   - Set Fields Length
 *                pActionHdr    - Pointer to Action Header  
 *                pu4Error      - Error on failure
 * Output      :  None 
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE 
 ****************************************************************************/
PRIVATE INT4
Ofc131PktParseSetFld (UINT1 u1TableId, tOfcActSetFld * pOfcActSetFld,
                      INT4 i4SetFldLen, tOfp131ActionHdr * pActionHdr,
                      UINT4 *pu4Error)
{
    tOxmTlv            *pOxmTlv = NULL;
    tOfcSetFld         *pOfcSetFld = NULL;
    UINT2               u2MaxVID = OFC_ZERO;
    UINT2               u2TempVlanVID = OFC_ZERO;

    OFC131_TRC_FUNC ((OFC131_FN_ENTRY, "FUNC:Ofc131PktParseSetFld\n"));

    pOxmTlv = (tOxmTlv *) ((VOID *) (((UINT1 *) (pActionHdr)) + OFC_FOUR));
    TMO_SLL_Init (&(pOfcActSetFld->SetFldList));
    while (i4SetFldLen > OFC_ZERO)
    {
        pOxmTlv->u2Class = OSIX_NTOHS (pOxmTlv->u2Class);
        /* Read the TLV's in the SetFld structure */
        if (pOxmTlv->u2Class != OFPXMC_OPENFLOW_BASIC)
        {
            /* It can be padding So check for zero value */
            if (pOxmTlv->u2Class == OFC_ZERO)
            {
                break;
            }
            OFC131_TRC_FUNC ((OFC_PKT_TRC, "Invalid Oxm Tlv Class\n"));
            return OSIX_FAILURE;
        }
        pOfcSetFld = OfcMemAllocForSetField (u1TableId);
        if (pOfcSetFld == NULL)
        {
            OFC131_TRC_FUNC ((OFC_PKT_TRC,
                              "MemAllocMemBlk for SetField Failed\n"));
            return OSIX_FAILURE;
        }
        MEMSET (pOfcSetFld, OFC_ZERO, sizeof (tOfcSetFld));
        pOfcSetFld->u2Type = (pOxmTlv->u1Field) >> OFC_ONE;

#ifndef DPA_WANTED
        if (pOfcSetFld->u2Type == OFCXMT_OFB_VLAN_VID)
        {
            u2MaxVID = OFC_MAX_VLANID;
            MEMCPY (&u2TempVlanVID, pOxmTlv->au1Value, OFC_TWO);
            u2TempVlanVID = (OSIX_HTONS (u2TempVlanVID)) & 0xFFF;
            if (OSIX_HTONS (u2TempVlanVID) > u2MaxVID)
            {
                *pu4Error = OFC131_FLOWMOD_ACT_SETVALUE_VALIDATE_ERR;
                OfcMemFreeForSetField (u1TableId, (UINT1 *) pOfcSetFld);
                return OSIX_FAILURE;
            }
        }
#endif
        MEMCPY (pOfcSetFld->SetFld.au1Fld, pOxmTlv->au1Value, pOxmTlv->u1Len);
        TMO_SLL_Add (&pOfcActSetFld->SetFldList, &pOfcSetFld->Node);
        KW_FALSEPOSITIVE_FIX (pOfcSetFld);
        i4SetFldLen -= (pOxmTlv->u1Len + OFC_FOUR);
        pOxmTlv = (tOxmTlv *) ((VOID *) (((UINT1 *) pOxmTlv) +
                                         (sizeof (tOxmTlv)) + pOxmTlv->u1Len));
    }
    OFC131_TRC_FUNC ((OFC131_FN_EXIT, "FUNC:Ofc131PktParseSetFld\n"));
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function    :  Ofc131PktAddActionToList
 * Description :  This function is for processing the actions as part of
 *                Action Header and adding to the SLL List 
 * Input       :  u1TableId     - Table Identifier 
 *                pOfcInstrList - Pointer to the Instruction List
 *                pActionHdr    - Pointer to the Action Header
 *                i4ActionLen   - Actions Length
 * Output      :  pu4Outport    - OutPort, if any
 *                pu4Outgroup   - OutGroup, if any
 *                pu4Error      - Error, in case occured
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE 
 *****************************************************************************/
PRIVATE INT4
Ofc131PktAddActionToList (UINT1 u1TableId, tOfcSll * pOfcInstrList,
                          tOfp131ActionHdr * pActionHdr, INT4 i4ActionLen,
                          UINT4 *pu4Outport, UINT4 *pu4Outgroup,
                          UINT4 *pu4Error, VOID *pConnPtr)
{
    tOfcActs           *pOfcActs = NULL;
    tOfcActOutput      *pOfcActOutput = NULL;
    tOfcActSetMplsTtl  *pOfcActSetMplsTtl = NULL;
    tOfcActPush        *pOfcActPush = NULL;
    tOfcActPopMpls     *pOfcActPopMpls = NULL;
    tOfcActSetQue      *pOfcActSetQue = NULL;
    tOfcActGroup       *pOfcActGroup = NULL;
    tOfcActSetNwTtl    *pOfcActSetNwTtl = NULL;
    tOfcActExprHdr     *pOfcActExprHdr = NULL;
    tOfcFsofcCfgEntry  *pOfcFsofcCfgEntry = NULL;
    tOfcFsofcControllerConnEntry *pLocal = NULL;
    tOfcSetFld         *pOfcSetFld = NULL;
    tOfcSetFld         *pNextOfcSetFld = NULL;
    tOfcActSetFld      *pOfcActSetFld = NULL;
    INT4                i4SetFldTlvLen = OFC_ZERO;
    INT4                i4RetVal = OSIX_SUCCESS;

    OFC131_TRC_FUNC ((OFC131_FN_ENTRY, "FUNC:Ofc131PktAddActionToList\n"));

    pLocal = (tOfcFsofcControllerConnEntry *) pConnPtr;

    pOfcFsofcCfgEntry =
        OfcGetFsofcCfgEntry (pLocal->MibObject.u4FsofcContextId);

    if (pOfcFsofcCfgEntry == NULL)
    {
        OFC131_TRC_FUNC ((OFC_PKT_TRC, "\r\nOfc131PktAddActionToList: "
                          "Get CFG Entry Failed\n"));
        return OFC_FAILURE;
    }

    while (i4ActionLen > OFC_ZERO)
    {
        pOfcActs = OfcMemAllocForAction (u1TableId);
        if (pOfcActs == NULL)
        {
            OFC131_TRC_FUNC ((OFC_PKT_TRC,
                              "MemAllocMemBlk for Action Failed\n"));
            return OSIX_FAILURE;
        }
        MEMSET (pOfcActs, OFC_ZERO, sizeof (tOfcActs));
        TMO_SLL_Init_Node (&(pOfcActs->Node));
        pOfcActs->u2Type = OSIX_NTOHS (pActionHdr->u2Type);
        pActionHdr->u2Len = OSIX_NTOHS (pActionHdr->u2Len);

        switch (pOfcActs->u2Type)
        {
            case OFCAT_OUTPUT:
                pOfcActOutput = (tOfcActOutput *) ((VOID *)
                                                   (((UINT1 *) pActionHdr) +
                                                    OFC_FOUR));
                pOfcActOutput->u4Port = OSIX_NTOHL (pOfcActOutput->u4Port);

                if (pOfcActOutput->u4Port > SYS_DEF_MAX_PHYSICAL_INTERFACES)
                {
                    /* reserved ports are valid output ports, except OFPP_ANY */
                    if (!((pOfcActOutput->u4Port >= OFPP_IN_PORT) &&
                          (pOfcActOutput->u4Port <= OFPP_LOCAL)))
                    {
                        /* logical ports are valid output ports */
                        if (!((pOfcActOutput->u4Port >= CFA_MIN_OF_IF_INDEX) &&
                              (pOfcActOutput->u4Port <= CFA_MAX_OF_IF_INDEX)))
                        {
                            /* output port is out of range ,so it is invalid */
                            *pu4Error = OFC131_FLOWMOD_ACT_PORT_VALIDATE_ERR;
                            OfcMemFreeForAction (u1TableId, (UINT1 *) pOfcActs);
                            return OSIX_FAILURE;
                        }
                    }

                    if ((pOfcActOutput->u4Port == OFPP_NORMAL) ||
                        (pOfcActOutput->u4Port == OFPP_FLOOD))
                    {
                        if (pOfcFsofcCfgEntry->MibObject.i4FsofcModuleStatus ==
                            OFC_ZERO)
                        {
                            *pu4Error = OFC131_FLOWMOD_ACT_PORT_VALIDATE_ERR;
                            OfcMemFreeForAction (u1TableId, (UINT1 *) pOfcActs);
                            return OSIX_FAILURE;
                        }
                    }
                }

                if (pu4Outport != NULL)
                {
                    *pu4Outport = pOfcActOutput->u4Port;
                }
                pOfcActOutput->u2MaxLen = OSIX_NTOHS (pOfcActOutput->u2MaxLen);
                MEMCPY (&pOfcActs->Acts.Output, pOfcActOutput,
                        (pActionHdr->u2Len - OFC_FOUR));
                TMO_SLL_Add (pOfcInstrList, &(pOfcActs->Node));
                break;

            case OFCAT_COPY_TTL_OUT:
            case OFCAT_COPY_TTL_IN:
            case OFCAT_DEC_MPLS_TTL:
            case OFCAT_POP_VLAN:
            case OFCAT_DEC_NW_TTL:
            case OFCAT_POP_PBB:
                /* Do Nothing. Type alone is copied */
                TMO_SLL_Add (pOfcInstrList, &(pOfcActs->Node));
                break;

            case OFCAT_SET_MPLS_TTL:
                pOfcActSetMplsTtl = (tOfcActSetMplsTtl *) ((VOID *)
                                                           (((UINT1 *)
                                                             pActionHdr) +
                                                            OFC_FOUR));

                if (pOfcActSetMplsTtl->u1MplsTtl == OFC_ZERO)
                {
                    /* OFPBAC_BAD_ARGUMENT */
                    *pu4Error = OFC131_FLOWMOD_ACT_VALUE_VALIDATE_ERR;
                    OfcMemFreeForAction (u1TableId, (UINT1 *) pOfcActs);
                    return OSIX_FAILURE;
                }
                MEMCPY (&pOfcActs->Acts.SetMplsTtl, pOfcActSetMplsTtl,
                        (pActionHdr->u2Len - OFC_FOUR));
                TMO_SLL_Add (pOfcInstrList, &(pOfcActs->Node));
                break;

            case OFCAT_PUSH_VLAN:
            case OFCAT_PUSH_MPLS:
            case OFCAT_PUSH_PBB:
                pOfcActPush = (tOfcActPush *) ((VOID *)
                                               (((UINT1 *) pActionHdr) +
                                                OFC_FOUR));
                pOfcActPush->u2EthType = OSIX_NTOHS (pOfcActPush->u2EthType);

                if (pOfcActs->u2Type == OFCAT_PUSH_VLAN)
                {
                    if ((pOfcActPush->u2EthType != OFC_L2EXT_CVLAN) &&
                        (pOfcActPush->u2EthType != OFC_L2EXT_SVLAN))
                    {
                        /* OFPBAC_BAD_ARGUMENT */
                        *pu4Error = OFC131_FLOWMOD_ACT_VALUE_VALIDATE_ERR;
                        OfcMemFreeForAction (u1TableId, (UINT1 *) pOfcActs);
                        return OSIX_FAILURE;
                    }
                }
                else if (pOfcActs->u2Type == OFCAT_PUSH_MPLS)
                {
                    if ((pOfcActPush->u2EthType != OFC_L2EXT_MPLS_1) &&
                        (pOfcActPush->u2EthType != OFC_L2EXT_MPLS_2))
                    {
                        /* OFPBAC_BAD_ARGUMENT */
                        *pu4Error = OFC131_FLOWMOD_ACT_VALUE_VALIDATE_ERR;
                        OfcMemFreeForAction (u1TableId, (UINT1 *) pOfcActs);
                        return OSIX_FAILURE;
                    }
                }
                else            /* PBB */
                {
                    if (pOfcActPush->u2EthType != OFC_L2EXT_PBB)
                    {
                        /* OFPBAC_BAD_ARGUMENT */
                        *pu4Error = OFC131_FLOWMOD_ACT_VALUE_VALIDATE_ERR;
                        OfcMemFreeForAction (u1TableId, (UINT1 *) pOfcActs);
                        return OSIX_FAILURE;
                    }
                }
                MEMCPY (&pOfcActs->Acts.Push, pOfcActPush,
                        (pActionHdr->u2Len - OFC_FOUR));
                TMO_SLL_Add (pOfcInstrList, &(pOfcActs->Node));
                break;

            case OFCAT_POP_MPLS:
                pOfcActPopMpls = (tOfcActPopMpls *) ((VOID *)
                                                     (((UINT1 *) pActionHdr) +
                                                      OFC_FOUR));
                pOfcActPopMpls->u2EthType =
                    OSIX_NTOHS (pOfcActPopMpls->u2EthType);
                MEMCPY (&pOfcActs->Acts.PopMpls, pOfcActPopMpls,
                        (pActionHdr->u2Len - OFC_FOUR));
                TMO_SLL_Add (pOfcInstrList, &(pOfcActs->Node));
                break;

            case OFCAT_SET_QUEUE:
                pOfcActSetQue = (tOfcActSetQue *) ((VOID *)
                                                   (((UINT1 *) pActionHdr) +
                                                    OFC_FOUR));
                pOfcActSetQue->u4QueId = OSIX_NTOHL (pOfcActSetQue->u4QueId);
                MEMCPY (&pOfcActs->Acts.SetQue, pOfcActSetQue,
                        (pActionHdr->u2Len - OFC_FOUR));
                TMO_SLL_Add (pOfcInstrList, &(pOfcActs->Node));
                break;

            case OFCAT_GROUP:
                pOfcActGroup = (tOfcActGroup *) ((VOID *)
                                                 (((UINT1 *) pActionHdr) +
                                                  OFC_FOUR));
                pOfcActGroup->u4GroupId = OSIX_NTOHL (pOfcActGroup->u4GroupId);
                if (pOfcActGroup->u4GroupId > OFCG_MAX)
                {
                    /* output group is out of range, so it is invalid */
                    *pu4Error = OFC131_FLOWMOD_ACT_GROUP_VALIDATE_ERR;
                    OfcMemFreeForAction (u1TableId, (UINT1 *) pOfcActs);
                    return OSIX_FAILURE;
                }
                if (pu4Outgroup != NULL)
                {
                    *pu4Outgroup = pOfcActGroup->u4GroupId;
                }
                MEMCPY (&pOfcActs->Acts.Group, pOfcActGroup,
                        (pActionHdr->u2Len - OFC_FOUR));
                TMO_SLL_Add (pOfcInstrList, &(pOfcActs->Node));
                break;

            case OFCAT_SET_NW_TTL:
                pOfcActSetNwTtl = (tOfcActSetNwTtl *) ((VOID *)
                                                       (((UINT1 *) pActionHdr) +
                                                        OFC_FOUR));
                if (pOfcActSetNwTtl->u1NwTtl == OFC_ZERO)
                {
                    /* OFPBAC_BAD_ARGUMENT */
                    *pu4Error = OFC131_FLOWMOD_ACT_VALUE_VALIDATE_ERR;
                    OfcMemFreeForAction (u1TableId, (UINT1 *) pOfcActs);
                    return OSIX_FAILURE;
                }
                MEMCPY (&pOfcActs->Acts.SetNwTtl, pOfcActSetNwTtl,
                        (pActionHdr->u2Len - OFC_FOUR));
                TMO_SLL_Add (pOfcInstrList, &(pOfcActs->Node));
                break;

            case OFCAT_EXPERIMENTER:
                pOfcActExprHdr = (tOfcActExprHdr *) ((VOID *)
                                                     (((UINT1 *) pActionHdr) +
                                                      OFC_FOUR));
                pOfcActExprHdr->u4Experimenter =
                    OSIX_NTOHL (pOfcActExprHdr->u4Experimenter);
                MEMCPY (&pOfcActs->Acts.ExprHdr, pOfcActExprHdr,
                        (pActionHdr->u2Len - OFC_FOUR));
                TMO_SLL_Add (pOfcInstrList, &(pOfcActs->Node));
                break;

            case OFCAT_SET_FIELD:
                /* Parsing Set fields */
                i4SetFldTlvLen = pActionHdr->u2Len - OFC_FOUR;
                i4RetVal = Ofc131PktParseSetFld (u1TableId,
                                                 (tOfcActSetFld *) &
                                                 pOfcActs->Acts, i4SetFldTlvLen,
                                                 pActionHdr, pu4Error);
                if (i4RetVal != OSIX_SUCCESS)
                {
                    pOfcActSetFld = (tOfcActSetFld *) & (pOfcActs->Acts);
                    TMO_DYN_SLL_Scan (&pOfcActSetFld->SetFldList, pOfcSetFld,
                                      pNextOfcSetFld, tOfcSetFld *)
                    {
                        TMO_SLL_Delete (&pOfcActSetFld->SetFldList,
                                        (tTMO_SLL_NODE *) pOfcSetFld);
                        OfcMemFreeForSetField (u1TableId, (UINT1 *) pOfcSetFld);
                        if (NULL == pNextOfcSetFld)
                        {
                            break;
                        }
                    }
                    OfcMemFreeForAction (u1TableId, (UINT1 *) pOfcActs);
                    return OSIX_FAILURE;
                }
                TMO_SLL_Add (pOfcInstrList, &(pOfcActs->Node));
                break;

            default:
                OfcMemFreeForAction (u1TableId, (UINT1 *) pOfcActs);
                break;
        }
        KW_FALSEPOSITIVE_FIX (pOfcActs);
        i4ActionLen -= pActionHdr->u2Len;
        pActionHdr = (tOfp131ActionHdr *) ((VOID *)
                                           (((UINT1 *) pActionHdr) +
                                            pActionHdr->u2Len));
    }
    OFC131_TRC_FUNC ((OFC131_FN_EXIT, "FUNC:Ofc131PktAddActionToList\n"));
    return i4RetVal;
}

/*****************************************************************************
 * Function    :  Ofc131PktParsePktOutAction
 * Description :  This function is for parsing Actions part of Packetout 
 * Input       :  tOfp131ActionHdr - Pointer to Action Header
 *                i4ActionLen      - Actions Length
 * Output      :  pOfcSll          - Pointer to SLL
 *                pu4Error         - Error
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE 
 *****************************************************************************/
PRIVATE INT4
Ofc131PktParsePktOutAction (tOfp131ActionHdr * pActionHdr, tOfcSll * pOfcSll,
                            INT4 i4ActionLen, UINT4 *pu4Error, VOID *pConnPtr)
{
    tOfcActs           *pOfcActs = NULL;
    tOfcActs           *pOfcNextActs = NULL;
    INT4                i4RetVal = OFC_SUCCESS;

    OFC131_TRC_FUNC ((OFC131_FN_ENTRY, "FUNC:Ofc131PktParsePktOutAction\n"));

    i4RetVal = Ofc131PktAddActionToList (OFC_ZERO, pOfcSll, pActionHdr,
                                         i4ActionLen, NULL, NULL,
                                         pu4Error, pConnPtr);
    if (i4RetVal != OSIX_SUCCESS)
    {
        TMO_DYN_SLL_Scan (pOfcSll, pOfcActs, pOfcNextActs, tOfcActs *)
        {
            TMO_SLL_Delete (pOfcSll, (tTMO_SLL_NODE *) pOfcActs);
            OfcMemFreeForAction (OFC_ZERO, (UINT1 *) pOfcActs);
            if (NULL == pOfcNextActs)
            {
                break;
            }
        }
    }
    OFC131_TRC_FUNC ((OFC131_FN_EXIT, "FUNC:Ofc131PktParsePktOutAction\n"));
    return i4RetVal;
}

/*****************************************************************************
 * Function    :  Ofc131PktPktOutOfpActionHdrAction
 * Description :  This function is for processing Actions part of PacketOut
 *                Message  
 * Input       :  pOfp131PktOut - Pointer to PacketOut Message
 *                pConnPtr      - Connection Pointer
 *                pBuf          - CRU Buffer Pointer
 * Output      :  pu4Error      - Error on failure 
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE 
 *****************************************************************************/
INT4
Ofc131PktPktOutOfpActionHdrAction (tOfp131PktOut * pOfp131PktOut,
                                   VOID *pConnPtr, tCRU_BUF_CHAIN_HEADER * pBuf,
                                   UINT4 *pu4Error)
{
    tOfcSll             ActList;    /* Action List  */
    tOfcFsofcControllerConnEntry *pLocal = NULL;
    tOfp131ActionHdr   *pActionHdr = NULL;
    INT4                i4RetVal = OSIX_SUCCESS;
    tOfcActs           *pOfcActs = NULL;
    tOfcActs           *pOfcNextActs = NULL;

    OFC131_TRC_FUNC ((OFC131_FN_ENTRY,
                      "FUNC:Ofc131PktOutOfpActionHdrAction\n"));

    pLocal = (tOfcFsofcControllerConnEntry *) pConnPtr;
    pActionHdr = (tOfp131ActionHdr *) ((VOID *)
                                       (pLocal->pRcvPkt +
                                        OFC131_PKTOUT_OFPACTIONHDR_OFFSET));

    /* Move the offset to data */
    if (CRU_BUF_Move_ValidOffset (pBuf, (UINT4) (OFC_TWENTY_FOUR +
                                                 pOfp131PktOut->u2ActionsLen))
        != OSIX_SUCCESS)
    {
        OFC131_TRC_FUNC ((OFC_PKT_TRC, "CRU_BUF_Move_ValidOffset Failed\n"));
        return OSIX_FAILURE;
    }

    TMO_SLL_Init (&(ActList));
    i4RetVal = Ofc131PktParsePktOutAction (pActionHdr, &ActList,
                                           (INT4) pOfp131PktOut->u2ActionsLen,
                                           pu4Error, pConnPtr);
    if (i4RetVal != OSIX_SUCCESS)
    {
        OFC131_TRC_FUNC ((OFC_PKT_TRC, "Ofc131PktParsePktOutAction Failed\n"));
        return i4RetVal;
    }

    i4RetVal = Ofc131PktHandleActions (pOfp131PktOut, pBuf, &ActList,
                                       pLocal->MibObject.u4FsofcContextId);
    if (i4RetVal != OSIX_SUCCESS)
    {
        OFC131_TRC_FUNC ((OFC_PKT_TRC, "Ofc131PktHandleActions Failed\n"));
        return i4RetVal;
    }
    UNUSED_PARAM (pu4Error);

    /* Deleting the list */
    TMO_DYN_SLL_Scan (&ActList, pOfcActs, pOfcNextActs, tOfcActs *)
    {
        TMO_SLL_Delete (&ActList, (tTMO_SLL_NODE *) pOfcActs);
        OfcMemFreeForAction (OFC_ZERO, (UINT1 *) pOfcActs);
        if (NULL == pOfcNextActs)
        {
            break;
        }
    }

    OFC131_TRC_FUNC ((OFC131_FN_EXIT, "FUNC:Ofc131PktOutOfpActionHdrAction\n"));
    return i4RetVal;
}

/*****************************************************************************
 * Function    :  Ofc131PktFlowModu8CookieAction
 * Description :  This function is for processing Cookie in Flow Mod   
 * Input       :  pOfp131FlowMod  - Pointer to Flow Mod Message
 *                pConnPtr        - Connection Pointer
 *                pBuf            - CRU Buffer Pointer
 * Output      :  pu4Error        - Error on failure 
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE 
 *****************************************************************************/
INT4
Ofc131PktFlowModu8CookieAction (tOfp131FlowMod * pOfp131FlowMod, VOID *pConnPtr,
                                tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 *pu4Error)
{
    UNUSED_PARAM (pu4Error);
    UNUSED_PARAM (pConnPtr);
    UNUSED_PARAM (pBuf);
    UNUSED_PARAM (pOfp131FlowMod);
    return OFC_SUCCESS;
}

/*****************************************************************************
 * Function    :  Ofc131PktFlowModu8CookieMaskAction
 * Description :  This function is for processing Cookie Mask in Flow Mod   
 * Input       :  pOfp131FlowMod  - Pointer to Flow Mod Message
 *                pConnPtr        - Connection Pointer
 *                pBuf            - CRU Buffer Pointer
 * Output      :  pu4Error        - Error on failure 
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE 
 *****************************************************************************/
INT4
Ofc131PktFlowModu8CookieMaskAction (tOfp131FlowMod * pOfp131FlowMod,
                                    VOID *pConnPtr,
                                    tCRU_BUF_CHAIN_HEADER * pBuf,
                                    UINT4 *pu4Error)
{
    UNUSED_PARAM (pu4Error);
    UNUSED_PARAM (pConnPtr);
    UNUSED_PARAM (pBuf);
    UNUSED_PARAM (pOfp131FlowMod);
    return OFC_SUCCESS;
}

/*****************************************************************************
 * Function    :  Ofc131PktFlowModu1TableIdAction
 * Description :  This function is for processing TableId in Flow Mod   
 * Input       :  pOfp131FlowMod  - Pointer to Flow Mod Message
 *                pConnPtr        - Connection Pointer
 *                pBuf            - CRU Buffer Pointer
 * Output      :  pu4Error        - Error on failure 
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE 
 *****************************************************************************/
INT4
Ofc131PktFlowModu1TableIdAction (tOfp131FlowMod * pOfp131FlowMod,
                                 VOID *pConnPtr, tCRU_BUF_CHAIN_HEADER * pBuf,
                                 UINT4 *pu4Error)
{
    UNUSED_PARAM (pu4Error);
    UNUSED_PARAM (pConnPtr);
    UNUSED_PARAM (pBuf);
    UNUSED_PARAM (pOfp131FlowMod);
    return OFC_SUCCESS;
}

/*****************************************************************************
 * Function    :  Ofc131PktFlowModu1CommandAction
 * Description :  This function is for processing Command in Flow Mod   
 * Input       :  pOfp131FlowMod  - Pointer to Flow Mod Message
 *                pConnPtr        - Connection Pointer
 *                pBuf            - CRU Buffer Pointer
 * Output      :  pu4Error        - Error on failure 
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE 
 *****************************************************************************/
INT4
Ofc131PktFlowModu1CommandAction (tOfp131FlowMod * pOfp131FlowMod,
                                 VOID *pConnPtr, tCRU_BUF_CHAIN_HEADER * pBuf,
                                 UINT4 *pu4Error)
{
    UNUSED_PARAM (pu4Error);
    UNUSED_PARAM (pConnPtr);
    UNUSED_PARAM (pBuf);
    UNUSED_PARAM (pOfp131FlowMod);
    return OFC_SUCCESS;
}

/*****************************************************************************
 * Function    :  Ofc131PktFlowModu2IdleTimeoutAction
 * Description :  This function is for processing IdleTimeout in Flow Mod
 * Input       :  pOfp131FlowMod  - Pointer to Flow Mod Message
 *                pConnPtr        - Connection Pointer
 *                pBuf            - CRU Buffer Pointer
 * Output      :  pu4Error        - Error on failure 
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE 
 *****************************************************************************/
INT4
Ofc131PktFlowModu2IdleTimeoutAction (tOfp131FlowMod * pOfp131FlowMod,
                                     VOID *pConnPtr,
                                     tCRU_BUF_CHAIN_HEADER * pBuf,
                                     UINT4 *pu4Error)
{
    UNUSED_PARAM (pu4Error);
    UNUSED_PARAM (pConnPtr);
    UNUSED_PARAM (pBuf);
    UNUSED_PARAM (pOfp131FlowMod);
    return OFC_SUCCESS;
}

/*****************************************************************************
 * Function    :  Ofc131PktFlowModu2HardTimeoutAction
 * Description :  This function is for processing Hard Timeout in Flow Mod
 * Input       :  pOfp131FlowMod  - Pointer to Flow Mod Message
 *                pConnPtr        - Connection Pointer
 *                pBuf            - CRU Buffer Pointer
 * Output      :  pu4Error        - Error on failure 
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE 
 *****************************************************************************/
INT4
Ofc131PktFlowModu2HardTimeoutAction (tOfp131FlowMod * pOfp131FlowMod,
                                     VOID *pConnPtr,
                                     tCRU_BUF_CHAIN_HEADER * pBuf,
                                     UINT4 *pu4Error)
{
    UNUSED_PARAM (pu4Error);
    UNUSED_PARAM (pConnPtr);
    UNUSED_PARAM (pBuf);
    UNUSED_PARAM (pOfp131FlowMod);
    return OFC_SUCCESS;
}

/*****************************************************************************
 * Function    :  Ofc131PktFlowModu2PriorityAction
 * Description :  This function is for processing Priority in Flow Mod
 * Input       :  pOfp131FlowMod  - Pointer to Flow Mod Message
 *                pConnPtr        - Connection Pointer
 *                pBuf            - CRU Buffer Pointer
 * Output      :  pu4Error        - Error on failure 
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE 
 *****************************************************************************/
INT4
Ofc131PktFlowModu2PriorityAction (tOfp131FlowMod * pOfp131FlowMod,
                                  VOID *pConnPtr, tCRU_BUF_CHAIN_HEADER * pBuf,
                                  UINT4 *pu4Error)
{
    UNUSED_PARAM (pu4Error);
    UNUSED_PARAM (pConnPtr);
    UNUSED_PARAM (pBuf);
    UNUSED_PARAM (pOfp131FlowMod);
    return OFC_SUCCESS;
}

/*****************************************************************************
 * Function    :  Ofc131PktFlowModu4BufIdAction
 * Description :  This function is for processing Buffer Id in Flow Mod
 * Input       :  pOfp131FlowMod  - Pointer to Flow Mod Message
 *                pConnPtr        - Connection Pointer
 *                pBuf            - CRU Buffer Pointer
 * Output      :  pu4Error        - Error on failure 
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE 
 *****************************************************************************/
INT4
Ofc131PktFlowModu4BufIdAction (tOfp131FlowMod * pOfp131FlowMod, VOID *pConnPtr,
                               tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 *pu4Error)
{
    UNUSED_PARAM (pu4Error);
    UNUSED_PARAM (pConnPtr);
    UNUSED_PARAM (pBuf);
    UNUSED_PARAM (pOfp131FlowMod);
    return OFC_SUCCESS;
}

/*****************************************************************************
 * Function    :  Ofc131PktFlowModu4OutPortAction
 * Description :  This function is for processing OutPort in Flow Mod
 * Input       :  pOfp131FlowMod  - Pointer to Flow Mod Message
 *                pConnPtr        - Connection Pointer
 *                pBuf            - CRU Buffer Pointer
 * Output      :  pu4Error        - Error on failure 
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE 
 *****************************************************************************/
INT4
Ofc131PktFlowModu4OutPortAction (tOfp131FlowMod * pOfp131FlowMod,
                                 VOID *pConnPtr, tCRU_BUF_CHAIN_HEADER * pBuf,
                                 UINT4 *pu4Error)
{
    UNUSED_PARAM (pu4Error);
    UNUSED_PARAM (pConnPtr);
    UNUSED_PARAM (pBuf);
    UNUSED_PARAM (pOfp131FlowMod);
    return OFC_SUCCESS;
}

/*****************************************************************************
 * Function    :  Ofc131PktFlowModu4OutGroupAction
 * Description :  This function is for processing OutGroup in Flow Mod
 * Input       :  pOfp131FlowMod  - Pointer to Flow Mod Message
 *                pConnPtr        - Connection Pointer
 *                pBuf            - CRU Buffer Pointer
 * Output      :  pu4Error        - Error on failure 
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE 
 *****************************************************************************/
INT4
Ofc131PktFlowModu4OutGroupAction (tOfp131FlowMod * pOfp131FlowMod,
                                  VOID *pConnPtr, tCRU_BUF_CHAIN_HEADER * pBuf,
                                  UINT4 *pu4Error)
{
    UNUSED_PARAM (pu4Error);
    UNUSED_PARAM (pConnPtr);
    UNUSED_PARAM (pBuf);
    UNUSED_PARAM (pOfp131FlowMod);
    return OFC_SUCCESS;
}

/*****************************************************************************
 * Function    :  Ofc131PktFlowModu2FlagsAction
 * Description :  This function is for processing Flags in Flow Mod
 * Input       :  pOfp131FlowMod  - Pointer to Flow Mod Message
 *                pConnPtr        - Connection Pointer
 *                pBuf            - CRU Buffer Pointer
 * Output      :  pu4Error        - Error on failure 
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE 
 *****************************************************************************/
INT4
Ofc131PktFlowModu2FlagsAction (tOfp131FlowMod * pOfp131FlowMod, VOID *pConnPtr,
                               tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 *pu4Error)
{
    UNUSED_PARAM (pu4Error);
    UNUSED_PARAM (pConnPtr);
    UNUSED_PARAM (pBuf);
    UNUSED_PARAM (pOfp131FlowMod);
    return OFC_SUCCESS;
}

/*****************************************************************************
 * Function    :  Ofc131PktFlowModau1PadAction
 * Description :  This function is for processing Padding in Flow Mod
 * Input       :  pOfp131FlowMod  - Pointer to Flow Mod Message
 *                pConnPtr        - Connection Pointer
 *                pBuf            - CRU Buffer Pointer
 * Output      :  pu4Error        - Error on failure 
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE 
 *****************************************************************************/
INT4
Ofc131PktFlowModau1PadAction (tOfp131FlowMod * pOfp131FlowMod, VOID *pConnPtr,
                              tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 *pu4Error)
{
    UNUSED_PARAM (pu4Error);
    UNUSED_PARAM (pConnPtr);
    UNUSED_PARAM (pBuf);
    UNUSED_PARAM (pOfp131FlowMod);
    return OFC_SUCCESS;
}

/*****************************************************************************
 * Function    :  Ofc131PktFlowModOfpMatchAction
 * Description :  This function is for processing Match and Rest in Flow Mod
 * Input       :  pOfp131FlowMod  - Pointer to Flow Mod Message
 *                pConnPtr        - Connection Pointer
 *                pBuf            - CRU Buffer Pointer
 * Output      :  pu4Error        - Error on failure 
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE 
 *****************************************************************************/
INT4
Ofc131PktFlowModOfpMatchAction (tOfp131FlowMod * pOfp131FlowMod,
                                VOID *pConnPtr,
                                tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 *pu4Error)
{
    tOfcFsofcControllerConnEntry *pLocal = pConnPtr;
    tOfcFsofcCfgEntry  *pOfcFsofcCfgEntry = NULL;

    OFC131_TRC_FUNC ((OFC131_FN_ENTRY,
                      "\r\nFUNC:Ofc131FlowModOfpMatchAction\n"));

    pOfcFsofcCfgEntry =
        OfcGetFsofcCfgEntry (pLocal->MibObject.u4FsofcContextId);
    if (pOfcFsofcCfgEntry == NULL)
    {
        OFC131_TRC_FUNC ((OFC_PKT_TRC, "\r\nOfc131PktFlowModOfpMatchAction: "
                          "Get CFG Entry Failed\n"));
        return OFC_FAILURE;
    }

    if (Ofc131FlowHandleFlowModMsg
        (pOfcFsofcCfgEntry, pConnPtr, pOfp131FlowMod, pu4Error) != OFC_SUCCESS)
    {
        OFC131_TRC_FUNC ((OFC_PKT_TRC,
                          "\r\nHandling Flow Mod Message Failed\n"));
        return OFC_FAILURE;
    }
    UNUSED_PARAM (pBuf);
    OFC131_TRC_FUNC ((OFC131_FN_EXIT,
                      "\r\nFUNC:Ofc131FlowModOfpMatchAction\n"));
    return OFC_SUCCESS;
}

/*****************************************************************************
 * Function    :  Ofc131PktParseBucketAction
 * Description :  This function is used for parsing Bucket in Group Mod 
 * Input       :  pActionHdr  - Pointer to Action Header
 *                i4ActionLen - Actions Length
 * Output      :  pBucket     - Pointer to Bucket
 *                pu4Error    - Pointer to Error
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE 
 *****************************************************************************/
PRIVATE INT4
Ofc131PktParseBucketAction (tOfp131ActionHdr * pActionHdr,
                            tOfcGroupBucket * pBucket, INT4 i4ActionLen,
                            UINT4 *pu4Error, VOID *pConnPtr)
{
    tOfcActs           *pOfcActs = NULL;
    tOfcActs           *pOfcNextActs = NULL;
    INT4                i4RetVal = OFC_SUCCESS;

    OFC131_TRC_FUNC ((OFC131_FN_ENTRY, "FUNC:Ofc131PktParseBucketAction\n"));

    TMO_SLL_Init (&(pBucket->ActList));
    i4RetVal =
        Ofc131PktAddActionToList (OFC_ZERO, &(pBucket->ActList), pActionHdr,
                                  i4ActionLen, NULL, NULL, pu4Error, pConnPtr);
    if (i4RetVal != OSIX_SUCCESS)
    {
        TMO_DYN_SLL_Scan (&pBucket->ActList, pOfcActs, pOfcNextActs, tOfcActs *)
        {
            TMO_SLL_Delete (&pBucket->ActList, (tTMO_SLL_NODE *) pOfcActs);
            OfcMemFreeForAction (OFC_ZERO, (UINT1 *) pOfcActs);
            if (NULL == pOfcNextActs)
            {
                break;
            }
        }
    }
    OFC131_TRC_FUNC ((OFC131_FN_EXIT, "FUNC:Ofc131PktParseBucketAction\n"));
    return i4RetVal;
}

/*****************************************************************************
 * Function    :  Ofc131PktGroupModu2CmdAction
 * Description :  This function is for processing Command in Group Mod  
 * Input       :  pOfp131GroupMod - Pointer to Group Mod Message
 *                pConnPtr        - Connection Pointer
 *                pBuf            - CRU Buffer Pointer
 * Output      :  pu4Error        - Error on failure 
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE 
 *****************************************************************************/
INT4
Ofc131PktGroupModu2CmdAction (tOfp131GroupMod * pOfp131GroupMod,
                              VOID *pConnPtr,
                              tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 *pu4Error)
{
    tOfcFsofcCfgEntry  *pOfcFsofcCfgEntry = NULL;
    tOfcFsofcGroupEntry *pGrpEntry = NULL;
    tOfcFsofcControllerConnEntry *pLocal = NULL;
    tOfp131ActionHdr   *pActionHdr = NULL;
    tOfp131Bucket      *pTmpBucket = NULL;
    tOfcGroupBucket    *pBucket = NULL;
    tOfcGroupBucket    *pFirstBucket = NULL;
    INT4                i4RetVal = OSIX_SUCCESS;
    INT4                i4BucketLen = OFC_ZERO;
    INT4                i4ActionLen = OFC_ZERO;
    INT4                i4PrevBucketLen = OFC_ZERO;

    OFC131_TRC_FUNC ((OFC131_FN_ENTRY, "FUNC:Ofc131GroupModu2CmdAction\n"));

    pLocal = (tOfcFsofcControllerConnEntry *) pConnPtr;
    pOfcFsofcCfgEntry =
        OfcGetFsofcCfgEntry (pLocal->MibObject.u4FsofcContextId);
    if (pOfcFsofcCfgEntry == NULL)
    {
        OFC131_TRC_FUNC ((OFC_PKT_TRC, "\r\nOfc131GroupModu2CmdAction: "
                          "Get CFG Entry Failed\n"));
        return OSIX_FAILURE;
    }

    pTmpBucket = (tOfp131Bucket *) ((VOID *)
                                    (pLocal->pRcvPkt +
                                     OFC131_GROUPMOD_OFPBUCKET_OFFSET));

    i4BucketLen = (INT4) ((INT4) pLocal->u4RxLength -
                          OFC131_GROUPMOD_OFPBUCKET_OFFSET);
    i4ActionLen =
        (INT4) ((INT4) (OSIX_HTONS (pTmpBucket->u2Len)) - OFC_SIXTEEN);

    /* Validating for buckets, we support only one as of now */
    if (i4BucketLen > (INT4) ((INT4) sizeof (tOfp131Bucket) + i4ActionLen))
    {
        /* request has more than one bucket */
        *pu4Error = OFC131_GROUPMOD_MORE_BUCKET_ERR;
        return OSIX_FAILURE;
    }

    pGrpEntry = (tOfcFsofcGroupEntry *) MemAllocMemBlk (OFC_GROUP_ENTRY_POOLID);
    if (pGrpEntry == NULL)
    {
        OFC131_TRC_FUNC ((OFC_PKT_TRC,
                          "MemAllocMemBlk Failed for GroupEntry\n"));
        *pu4Error = OFC131_GROUPMOD_TABLE_FULL;
        return OSIX_FAILURE;
    }
    MEMSET (pGrpEntry, OFC_ZERO, sizeof (tOfcFsofcGroupEntry));
    pGrpEntry->MibObject.u4FsofcGroupIndex = pOfp131GroupMod->u4GroupId;
    pGrpEntry->MibObject.i4FsofcGroupType = (INT4) pOfp131GroupMod->u1Type;
    pGrpEntry->MibObject.i4FsofcGroupActionBucketsLen = OFC_ZERO;

    TMO_SLL_Init (&(pGrpEntry->BucketList));

    if (pOfp131GroupMod->u2Cmd == OFCGC_DELETE)
    {
        i4BucketLen = OFC_ZERO;
    }

    while (i4BucketLen > OFC_ZERO)
    {
        pBucket = (tOfcGroupBucket *) MemAllocMemBlk (OFC_ACTION_BUCKET_POOLID);
        if (pBucket == NULL)
        {
            OFC131_TRC_FUNC ((OFC_PKT_TRC,
                              "MemAllocMemBlk Failed for Bucket\n"));
            /* unable to allocate memory for bucket */
            *pu4Error = OFC131_GROUPMOD_MORE_BUCKET_ERR;
            Ofc131GrpMemReleaseBuckets (&(pGrpEntry->BucketList));
            MemReleaseMemBlock (OFC_GROUP_ENTRY_POOLID, (UINT1 *) pGrpEntry);
            return OSIX_FAILURE;
        }
        MEMSET (pBucket, OFC_ZERO, sizeof (tOfcGroupBucket));
        pBucket->u2Weight = OSIX_NTOHS (pTmpBucket->u2Weight);
        pBucket->u4WatchPort = OSIX_NTOHL (pTmpBucket->u4WatchPort);
        pBucket->u4WatchGroup = OSIX_NTOHL (pTmpBucket->u4WatchGroup);
        pTmpBucket->u2Len = OSIX_NTOHS (pTmpBucket->u2Len);
        i4ActionLen = (INT4) pTmpBucket->u2Len - OFC_SIXTEEN;
        pActionHdr = (tOfp131ActionHdr *) ((VOID *) ((pLocal->pRcvPkt) +
                                                     OFC131_GROUPMOD_OFPBUCKET_OFFSET
                                                     + OFC_SIXTEEN +
                                                     i4PrevBucketLen));
        i4RetVal =
            Ofc131PktParseBucketAction (pActionHdr, pBucket, i4ActionLen,
                                        pu4Error, pConnPtr);
        i4PrevBucketLen = i4PrevBucketLen + (INT4) pTmpBucket->u2Len;
        if (i4RetVal != OSIX_SUCCESS)
        {
            OFC131_TRC_FUNC ((OFC_PKT_TRC,
                              "Ofc131FlowHandleFlowModMsgs Failed\n"));
            /* error in parsing the bucket */
            *pu4Error = OFC131_GROUPMOD_BAD_BUCKET_ERR;
            Ofc131GrpMemReleaseBuckets (&(pGrpEntry->BucketList));
            MemReleaseMemBlock (OFC_GROUP_ENTRY_POOLID, (UINT1 *) pGrpEntry);
            MemReleaseMemBlock (OFC_ACTION_BUCKET_POOLID, (UINT1 *) pBucket);
            return OSIX_FAILURE;
        }

        TMO_SLL_Add (&pGrpEntry->BucketList, &(pBucket->Node));
        pGrpEntry->MibObject.i4FsofcGroupActionBucketsLen++;

        i4BucketLen -= (INT4) pTmpBucket->u2Len;
        pTmpBucket = (tOfp131Bucket *) ((VOID *)
                                        (((UINT1 *) pTmpBucket) +
                                         pTmpBucket->u2Len));

        if (pGrpEntry->MibObject.i4FsofcGroupActionBucketsLen++
            == OFC_MAX_BUCKETS)
        {
            OFC131_TRC_FUNC ((OFC_PKT_TRC, "Max buckets reached\n"));
            break;

        }
    }
    if (pOfp131GroupMod->u1Type == OFCGT_SELECT)
    {
        pFirstBucket = (tOfcGroupBucket *) TMO_SLL_First
            (&(pGrpEntry->BucketList));
        if (pFirstBucket != NULL)
        {
            pFirstBucket->u1SelectFlag = OFC_ONE;
        }
    }

    Ofc131GroupHandleGroupModMsgs (pOfcFsofcCfgEntry, pGrpEntry,
                                   pOfp131GroupMod->u2Cmd);
    UNUSED_PARAM (pBuf);
    UNUSED_PARAM (pConnPtr);
    OFC131_TRC_FUNC ((OFC131_FN_EXIT, "FUNC:Ofc131GroupModu2CmdAction\n"));
    return i4RetVal;
}

/*****************************************************************************
 * Function    :  Ofc131PktGroupModu1TypeAction
 * Description :  This function is for processing Type in Group Mod 
 * Input       :  pOfp131GroupMod - Pointer to Group Mod Message
 *                pConnPtr        - Connection Pointer
 *                pBuf            - CRU Buffer Pointer
 * Output      :  pu4Error        - Error on failure 
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE 
 *****************************************************************************/
INT4
Ofc131PktGroupModu1TypeAction (tOfp131GroupMod * pOfp131GroupMod,
                               VOID *pConnPtr, tCRU_BUF_CHAIN_HEADER * pBuf,
                               UINT4 *pu4Error)
{
    UNUSED_PARAM (pu4Error);
    UNUSED_PARAM (pConnPtr);
    UNUSED_PARAM (pBuf);
    UNUSED_PARAM (pOfp131GroupMod);
    return OFC_SUCCESS;
}

/*****************************************************************************
 * Function    :  Ofc131PktGroupModu1PadAction
 * Description :  This function is for processing Padding in Group Mod 
 * Input       :  pOfp131GroupMod - Pointer to Group Mod Message
 *                pConnPtr        - Connection Pointer
 *                pBuf            - CRU Buffer Pointer
 * Output      :  pu4Error        - Error on failure 
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE 
 *****************************************************************************/
INT4
Ofc131PktGroupModu1PadAction (tOfp131GroupMod * pOfp131GroupMod, VOID *pConnPtr,
                              tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 *pu4Error)
{
    UNUSED_PARAM (pu4Error);
    UNUSED_PARAM (pConnPtr);
    UNUSED_PARAM (pBuf);
    UNUSED_PARAM (pOfp131GroupMod);
    return OFC_SUCCESS;
}

/*****************************************************************************
 * Function    :  Ofc131PktGroupModu4GroupIdAction
 * Description :  This function is for processing Group Id in Group Mod 
 * Input       :  pOfp131GroupMod - Pointer to Group Mod Message
 *                pConnPtr        - Connection Pointer
 *                pBuf            - CRU Buffer Pointer
 * Output      :  pu4Error        - Error on failure 
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE 
 *****************************************************************************/
INT4
Ofc131PktGroupModu4GroupIdAction (tOfp131GroupMod * pOfp131GroupMod,
                                  VOID *pConnPtr, tCRU_BUF_CHAIN_HEADER * pBuf,
                                  UINT4 *pu4Error)
{
    UNUSED_PARAM (pu4Error);
    UNUSED_PARAM (pConnPtr);
    UNUSED_PARAM (pBuf);
    UNUSED_PARAM (pOfp131GroupMod);
    return OFC_SUCCESS;
}

/*****************************************************************************
 * Function    :  Ofc131PktGroupModOfpBucketAction
 * Description :  This function is for processing Group Bucket in Group Mod 
 * Input       :  pOfp131GroupMod - Pointer to Group Mod Message
 *                pConnPtr        - Connection Pointer
 *                pBuf            - CRU Buffer Pointer
 * Output      :  pu4Error        - Error on failure 
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE 
 *****************************************************************************/
INT4
Ofc131PktGroupModOfpBucketAction (tOfp131GroupMod * pOfp131GroupMod,
                                  VOID *pConnPtr, tCRU_BUF_CHAIN_HEADER * pBuf,
                                  UINT4 *pu4Error)
{
    UNUSED_PARAM (pu4Error);
    UNUSED_PARAM (pConnPtr);
    UNUSED_PARAM (pBuf);
    UNUSED_PARAM (pOfp131GroupMod);
    return OFC_SUCCESS;
}

/*****************************************************************************
 * Function    :  Ofc131PktPortModu4PortNoAction
 * Description :  This function is for processing Port Number in Port Mod 
 * Input       :  pOfp131PortMod - Pointer to Port Mod Message
 *                pConnPtr       - Connection Pointer
 *                pBuf           - CRU Buffer Pointer
 * Output      :  pu4Error       - Error on failure 
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE 
 *****************************************************************************/
INT4
Ofc131PktPortModu4PortNoAction (tOfp131PortMod * pOfp131PortMod, VOID *pConnPtr,
                                tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 *pu4Error)
{
    UNUSED_PARAM (pu4Error);
    UNUSED_PARAM (pConnPtr);
    UNUSED_PARAM (pBuf);
    UNUSED_PARAM (pOfp131PortMod);
    return OFC_SUCCESS;
}

/*****************************************************************************
 * Function    :  Ofc131PktPortModau1PadAction
 * Description :  This function is for processing Padding in Port Mod 
 * Input       :  pOfp131PortMod - Pointer to Port Mod Message
 *                pConnPtr       - Connection Pointer
 *                pBuf           - CRU Buffer Pointer
 * Output      :  pu4Error       - Error on failure 
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE 
 *****************************************************************************/
INT4
Ofc131PktPortModau1PadAction (tOfp131PortMod * pOfp131PortMod, VOID *pConnPtr,
                              tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 *pu4Error)
{
    UNUSED_PARAM (pu4Error);
    UNUSED_PARAM (pConnPtr);
    UNUSED_PARAM (pBuf);
    UNUSED_PARAM (pOfp131PortMod);
    return OFC_SUCCESS;
}

/*****************************************************************************
 * Function    :  Ofc131PktPortModau1HwAddrAction
 * Description :  This function is for processing Hardware Address in Port Mod 
 * Input       :  pOfp131PortMod - Pointer to Port Mod Message
 *                pConnPtr       - Connection Pointer
 *                pBuf           - CRU Buffer Pointer
 * Output      :  pu4Error       - Error on failure 
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE 
 *****************************************************************************/
INT4
Ofc131PktPortModau1HwAddrAction (tOfp131PortMod * pOfp131PortMod,
                                 VOID *pConnPtr, tCRU_BUF_CHAIN_HEADER * pBuf,
                                 UINT4 *pu4Error)
{
    UNUSED_PARAM (pu4Error);
    UNUSED_PARAM (pConnPtr);
    UNUSED_PARAM (pBuf);
    UNUSED_PARAM (pOfp131PortMod);
    return OFC_SUCCESS;
}

/*****************************************************************************
 * Function    :  Ofc131PktPortModau1Pad2Action
 * Description :  This function is for processing Padding2 in Port Mod 
 * Input       :  pOfp131PortMod - Pointer to Port Mod Message
 *                pConnPtr       - Connection Pointer
 *                pBuf           - CRU Buffer Pointer
 * Output      :  pu4Error       - Error on failure 
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE 
 *****************************************************************************/
INT4
Ofc131PktPortModau1Pad2Action (tOfp131PortMod * pOfp131PortMod, VOID *pConnPtr,
                               tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 *pu4Error)
{
    UNUSED_PARAM (pu4Error);
    UNUSED_PARAM (pConnPtr);
    UNUSED_PARAM (pBuf);
    UNUSED_PARAM (pOfp131PortMod);
    return OFC_SUCCESS;
}

/*****************************************************************************
 * Function    :  Ofc131PktPortModu4ConfigAction
 * Description :  This function is for processing Config in Port Mod 
 * Input       :  pOfp131PortMod - Pointer to Port Mod Message
 *                pConnPtr       - Connection Pointer
 *                pBuf           - CRU Buffer Pointer
 * Output      :  pu4Error       - Error on failure 
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE 
 *****************************************************************************/
INT4
Ofc131PktPortModu4ConfigAction (tOfp131PortMod * pOfp131PortMod, VOID *pConnPtr,
                                tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 *pu4Error)
{
    tCfaIfInfo          cfaifInfo;
    tOfp131ErrMsg       Ofp131ErrorMsg;
    tOfp131PortStatus   Ofp131PortStat;
    tOfcFsofcIfEntry    lOfcFsofcIfEntry;
    tOfcFsofcIfEntry   *pOfcFsofcIfEntry = NULL;
    INT4                i4RetVal = OSIX_SUCCESS;
    UINT4               u4PortNum = OFC_ZERO;
    UINT4               u4Action = OFC_ZERO;
#ifdef DPA_WANTED
    UINT1               u1PortState = OFC_ZERO;
#endif /* DPA_WANTED */

    OFC131_TRC_FUNC ((OFC131_FN_ENTRY, "FUNC:Ofc131PortModu4ConfigValidate\n"));

    MEMSET (&Ofp131ErrorMsg, OFC_ZERO, sizeof (tOfp131ErrMsg));
    MEMSET (&cfaifInfo, OFC_ZERO, sizeof (cfaifInfo));
    MEMSET (&Ofp131PortStat, OFC_ZERO, sizeof (tOfp131PortStatus));
    u4PortNum = pOfp131PortMod->u4PortNo;

#ifdef DPA_WANTED
    u4Action = pOfp131PortMod->u4Config & pOfp131PortMod->u4Mask;
    if ((u4Action & OFPPC_PORT_DOWN) == OFPPC_PORT_DOWN)
    {
        u1PortState = OFPPC_PORT_DOWN;
    }

    if (Ofc131DpaPortUpdate (u4PortNum, u1PortState,
                             pOfp131PortMod->u4Advertise) != OFC_SUCCESS)
    {
        return OFC_FAILURE;
    }
    /* port status will be sent after receiving an event from DPA */
    return OFC_SUCCESS;
#endif /* DPA_WANTED */

    if (CfaGetIfInfo (u4PortNum, &cfaifInfo) == CFA_FAILURE)
    {
        Ofp131ErrorMsg.u2Type = OFPET_131_PORT_MOD_FAILED;
        Ofp131ErrorMsg.u2Code = OFPPMFC_BAD_PORT;
        i4RetVal = Ofc131PktErrMsgSend (&Ofp131ErrorMsg, pConnPtr,
                                        sizeof (Ofp131ErrorMsg.au1Data));
        OFC131_TRC_FUNC ((OFC_PKT_TRC,
                          "CfaGetIfInfo Failed for Port Num : %d\n",
                          u4PortNum));
        return OSIX_FAILURE;
    }

    u4Action = pOfp131PortMod->u4Config & pOfp131PortMod->u4Mask;
    if ((u4Action & OFPPC_PORT_DOWN) == OFPPC_PORT_DOWN)
    {
        if (cfaifInfo.u1IfOperStatus == CFA_IF_DOWN)
        {
            OFC131_TRC_FUNC ((OFC_PKT_TRC,
                              "Operstatus for Port Num : %d is already down\n",
                              u4PortNum));
            return OSIX_SUCCESS;
        }
        else
        {
            CfaInterfaceStatusChangeIndication (u4PortNum, CFA_IF_DOWN);
            Ofp131PortStat.PortDesc.u4PortNo = (UINT2) u4PortNum;
            MEMCPY (Ofp131PortStat.PortDesc.au1Name, cfaifInfo.au1IfAliasName,
                    OFC_SIXTEEN);

            MEMCPY (Ofp131PortStat.PortDesc.au1HwAddr, cfaifInfo.au1MacAddr,
                    sizeof (Ofp131PortStat.PortDesc.au1HwAddr));
            Ofp131PortStat.PortDesc.u4Curr = OFC_ZERO;
            Ofp131PortStat.PortDesc.u4Supported = OFC_ZERO;
            Ofp131PortStat.PortDesc.u4Advertised = OFC_ZERO;
            Ofp131PortStat.PortDesc.u4Peer = OFC_ZERO;
            Ofp131PortStat.PortDesc.u4CurrSpeed = cfaifInfo.u4IfSpeed;
            Ofp131PortStat.PortDesc.u4MaxSpeed = cfaifInfo.u4IfHighSpeed;
            Ofp131PortStat.u1Reason = OFPPR_MODIFY;

            MEMSET (&lOfcFsofcIfEntry.MibObject, OFC_ZERO,
                    sizeof (lOfcFsofcIfEntry.MibObject));
            lOfcFsofcIfEntry.MibObject.u4FsofcContextId =
                (INT4) OFC_DEFAULT_CONTEXT;
            lOfcFsofcIfEntry.MibObject.i4FsofcIfIndex = (INT4) (u4PortNum);
            pOfcFsofcIfEntry = RBTreeGet (gOfcGlobals.OfcGlbMib.FsofcIfTable,
                                          (tRBElem *) & (lOfcFsofcIfEntry));
            if (pOfcFsofcIfEntry == NULL)
            {
                OFC131_TRC_FUNC ((OFC_PKT_TRC, "No Entry Present in "
                                  "FsofcIfTable for  Port : %d\n", u4PortNum));
                return OSIX_FAILURE;
            }
            pOfcFsofcIfEntry->u4Config =
                pOfcFsofcIfEntry->u4Config | OFPPC_PORT_DOWN;
            pOfcFsofcIfEntry->MibObject.i4FsofcIfOperStatus =
                (INT4) OFPPS_LINK_DOWN;

            Ofp131PortStat.PortDesc.u4Config = pOfcFsofcIfEntry->u4Config;

            /* Sending Port Status Message to the controller */
            i4RetVal = Ofc131PktPortStatusSend (&Ofp131PortStat, NULL,
                                                OFP131_VARLEN_ZERO);
            if (i4RetVal != OSIX_SUCCESS)
            {
                OFC131_TRC_FUNC ((OFC_PKT_TRC,
                                  "Ofc131PktPortStatusSend Failed\n"));
                return OSIX_FAILURE;
            }
        }
    }

    /* No Packets should be received on this port (drop packets received) */
    if ((u4Action & OFPPC_NO_RECV) == OFPPC_NO_RECV)
    {
        MEMSET (&lOfcFsofcIfEntry.MibObject, OFC_ZERO,
                sizeof (lOfcFsofcIfEntry.MibObject));
        lOfcFsofcIfEntry.MibObject.u4FsofcContextId =
            (INT4) OFC_DEFAULT_CONTEXT;
        lOfcFsofcIfEntry.MibObject.i4FsofcIfIndex = (INT4) u4PortNum;
        pOfcFsofcIfEntry = RBTreeGet (gOfcGlobals.OfcGlbMib.FsofcIfTable,
                                      (tRBElem *) & (lOfcFsofcIfEntry));
        if (pOfcFsofcIfEntry == NULL)
        {
            OFC131_TRC_FUNC ((OFC_PKT_TRC, "No Entry Present in FsofcIfTable "
                              "for  Port : %d\n", u4PortNum));
            return OSIX_FAILURE;
        }
        pOfcFsofcIfEntry->u4Config = pOfcFsofcIfEntry->u4Config | OFPPC_NO_RECV;
    }

    /* No Packets should be forwarded on this port (drop packets to forward) */
    if ((u4Action & OFPPC_NO_FWD) == OFPPC_NO_FWD)
    {
        MEMSET (&lOfcFsofcIfEntry.MibObject, OFC_ZERO,
                sizeof (lOfcFsofcIfEntry.MibObject));
        lOfcFsofcIfEntry.MibObject.u4FsofcContextId =
            (INT4) OFC_DEFAULT_CONTEXT;
        lOfcFsofcIfEntry.MibObject.i4FsofcIfIndex = (INT4) u4PortNum;
        pOfcFsofcIfEntry = RBTreeGet (gOfcGlobals.OfcGlbMib.FsofcIfTable,
                                      (tRBElem *) & (lOfcFsofcIfEntry));
        if (pOfcFsofcIfEntry == NULL)
        {
            OFC131_TRC_FUNC ((OFC_PKT_TRC, "No Entry Present in FsofcIfTable "
                              "for  Port : %d\n", u4PortNum));
            return OSIX_FAILURE;
        }
        pOfcFsofcIfEntry->u4Config = pOfcFsofcIfEntry->u4Config | OFPPC_NO_FWD;
    }

    /* No Packet-In for the Packets received on this Port */
    if ((u4Action & OFPPC_NO_PACKET_IN) == OFPPC_NO_PACKET_IN)
    {
        MEMSET (&lOfcFsofcIfEntry.MibObject, OFC_ZERO,
                sizeof (lOfcFsofcIfEntry.MibObject));
        lOfcFsofcIfEntry.MibObject.u4FsofcContextId =
            (INT4) OFC_DEFAULT_CONTEXT;
        lOfcFsofcIfEntry.MibObject.i4FsofcIfIndex = (INT4) u4PortNum;
        pOfcFsofcIfEntry = RBTreeGet (gOfcGlobals.OfcGlbMib.FsofcIfTable,
                                      (tRBElem *) & (lOfcFsofcIfEntry));
        if (pOfcFsofcIfEntry == NULL)
        {
            OFC131_TRC_FUNC ((OFC_PKT_TRC, "No Entry Present in FsofcIfTable "
                              "for  Port : %d\n", u4PortNum));
            return OSIX_FAILURE;
        }
        pOfcFsofcIfEntry->u4Config = pOfcFsofcIfEntry->u4Config |
            OFPPC_NO_PACKET_IN;
    }
    UNUSED_PARAM (pu4Error);
    UNUSED_PARAM (pBuf);
    UNUSED_PARAM (pConnPtr);
    OFC131_TRC_FUNC ((OFC131_FN_EXIT, "FUNC:Ofc131PortModu4ConfigValidate\n"));
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function    :  Ofc131PktPortModu4MaskAction
 * Description :  This function is for processing Mask in Port Mod
 * Input       :  pOfp131PortMod  - Pointer to Port Mod Message
 *                pConnPtr        - Connection Pointer
 *                pBuf            - CRU Buffer Pointer
 * Output      :  pu4Error        - Error on failure 
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE 
 *****************************************************************************/
INT4
Ofc131PktPortModu4MaskAction (tOfp131PortMod * pOfp131PortMod, VOID *pConnPtr,
                              tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 *pu4Error)
{
    UNUSED_PARAM (pu4Error);
    UNUSED_PARAM (pConnPtr);
    UNUSED_PARAM (pBuf);
    UNUSED_PARAM (pOfp131PortMod);
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function    :  Ofc131PktPortModu4AdvertiseAction
 * Description :  This function is for processing Advertise in Port Mod
 * Input       :  pOfp131PortMod  - Pointer to Port Mod Message
 *                pConnPtr        - Connection Pointer
 *                pBuf            - CRU Buffer Pointer
 * Output      :  pu4Error        - Error on failure 
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE 
 *****************************************************************************/
INT4
Ofc131PktPortModu4AdvertiseAction (tOfp131PortMod * pOfp131PortMod,
                                   VOID *pConnPtr, tCRU_BUF_CHAIN_HEADER * pBuf,
                                   UINT4 *pu4Error)
{
    UNUSED_PARAM (pu4Error);
    UNUSED_PARAM (pConnPtr);
    UNUSED_PARAM (pBuf);
    UNUSED_PARAM (pOfp131PortMod);
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function    :  Ofc131PktPortModau1Pad3Action
 * Description :  This function is for processing Padding3 in Port Mod
 * Input       :  pOfp131PortMod  - Pointer to Port Mod Message
 *                pConnPtr        - Connection Pointer
 *                pBuf            - CRU Buffer Pointer
 * Output      :  pu4Error        - Error on failure 
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE 
 *****************************************************************************/
INT4
Ofc131PktPortModau1Pad3Action (tOfp131PortMod * pOfp131PortMod, VOID *pConnPtr,
                               tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 *pu4Error)
{
    UNUSED_PARAM (pu4Error);
    UNUSED_PARAM (pConnPtr);
    UNUSED_PARAM (pBuf);
    UNUSED_PARAM (pOfp131PortMod);
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function    :  Ofc131PktMultiPartRequ2TypeAction
 * Description :  This function is for processing Type in Multipart Message  
 * Input       :  pOfp131MultiPartReq - Pointer Multipart Request
 *                pConnPtr            - Connection Pointer
 *                pBuf                - CRU Buffer Pointer
 * Output      :  pu4Error            - Error on failure 
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE 
 *****************************************************************************/
INT4
Ofc131PktMultiPartRequ2TypeAction (tOfp131MultiPartReq * pOfp131MultiPartReq,
                                   VOID *pConnPtr, tCRU_BUF_CHAIN_HEADER * pBuf,
                                   UINT4 *pu4Error)
{
    INT4                i4RetVal = OSIX_SUCCESS;

    OFC131_TRC_FUNC ((OFC131_FN_ENTRY,
                      "FUNC:Ofc131MultiPartRequ2TypeAction\n"));

    switch (pOfp131MultiPartReq->u2Type)
    {
            /* Switch description */
        case OFCMP_DESC:
            i4RetVal = Ofc131MultipartOfcDesc (pOfp131MultiPartReq, pConnPtr,
                                               pBuf, pu4Error);
            break;

            /* Individual Flor statistics */
        case OFCMP_FLOW:
            i4RetVal = Ofc131MultipartIndividualFlowStats (pOfp131MultiPartReq,
                                                           pConnPtr, pBuf,
                                                           pu4Error);
            break;

            /* Aggregate Flow statistics */
        case OFCMP_AGGREGATE:
            i4RetVal = Ofc131MultipartAggFlowStats (pOfp131MultiPartReq,
                                                    pConnPtr, pBuf, pu4Error);
            break;

            /* Table statistics */
        case OFCMP_TABLE:
            i4RetVal = Ofc131MultipartFlowTableStats (pOfp131MultiPartReq,
                                                      pConnPtr, pBuf, pu4Error);
            break;

            /* Port statistics */
        case OFCMP_PORT_STATS:
            i4RetVal = Ofc131MultipartPortStats (pOfp131MultiPartReq, pConnPtr,
                                                 pBuf, pu4Error);
            break;

            /* Group statistics */
        case OFCMP_GROUP:
            i4RetVal = Ofc131MultipartGroupStats (pOfp131MultiPartReq, pConnPtr,
                                                  pBuf, pu4Error);
            break;

            /* Group description */
        case OFCMP_GROUP_DESC:
            i4RetVal = Ofc131MultipartGroupDesc (pOfp131MultiPartReq, pConnPtr,
                                                 pBuf, pu4Error);
            break;

            /* Group features */
        case OFCMP_GROUP_FEATURES:
            i4RetVal = Ofc131MultipartGroupFeatures (pOfp131MultiPartReq,
                                                     pConnPtr, pBuf, pu4Error);
            break;

            /* Table Features */
        case OFCMP_TABLE_FEATURES:
            i4RetVal = Ofc131MultipartTableFeatures (pOfp131MultiPartReq,
                                                     pConnPtr, pBuf, pu4Error);
            break;

            /* Port Description */
        case OFCMP_PORT_DESC:
            i4RetVal = Ofc131MultipartPortDesc (pOfp131MultiPartReq, pConnPtr,
                                                pBuf, pu4Error);
            break;

            /* Queue Statistics for a Port */
        case OFCMP_QUEUE:
            i4RetVal = Ofc131MultipartQueueStats (pOfp131MultiPartReq,
                                                  pConnPtr, pBuf, pu4Error);
            break;

            /* Experimenter */
        case OFCMP_EXPERIMENTER:
            i4RetVal = Ofc131MultipartExperimenter (pOfp131MultiPartReq,
                                                    pConnPtr, pBuf, pu4Error);
            break;

            /* Meter Description */
        case OFCMP_METER:
            i4RetVal = Ofc131MultipartMeterDesc (pOfp131MultiPartReq, pConnPtr,
                                                 pBuf, pu4Error);
            break;

            /* Meter Configuration */
        case OFCMP_METER_CONFIG:
            i4RetVal = Ofc131MultipartMeterConfig (pOfp131MultiPartReq,
                                                   pConnPtr, pBuf, pu4Error);
            break;

            /* Meter Features */
        case OFCMP_METER_FEATURES:
            i4RetVal = Ofc131MultipartMeterFeatures (pOfp131MultiPartReq,
                                                     pConnPtr, pBuf, pu4Error);
            break;

        default:
            /* Unknown Multipart Type */
            i4RetVal = OSIX_FAILURE;
            break;
    }
    OFC131_TRC_FUNC ((OFC131_FN_EXIT, "FUNC:Ofc131MultiPartRequ2TypeAction\n"));
    return i4RetVal;
}

/*****************************************************************************
 * Function    :  Ofc131PktMultiPartRequ2FlagsAction
 * Description :  This function is for processing Flags in Multipart Message  
 * Input       :  pOfp131MultiPartReq - Pointer Multipart Request
 *                pConnPtr            - Connection Pointer
 *                pBuf                - CRU Buffer Pointer
 * Output      :  pu4Error            - Error on failure 
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE 
 *****************************************************************************/
INT4
Ofc131PktMultiPartRequ2FlagsAction (tOfp131MultiPartReq * pOfp131MultiPartReq,
                                    VOID *pConnPtr,
                                    tCRU_BUF_CHAIN_HEADER * pBuf,
                                    UINT4 *pu4Error)
{
    UNUSED_PARAM (pu4Error);
    UNUSED_PARAM (pConnPtr);
    UNUSED_PARAM (pBuf);
    UNUSED_PARAM (pOfp131MultiPartReq);
    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  Ofc131PktMultiPartReqau1PadAction
 * Description :  This function is for processing Padding in Multipart Message  
 * Input       :  pOfp131MultiPartReq - Pointer Multipart Request
 *                pConnPtr            - Connection Pointer
 *                pBuf                - CRU Buffer Pointer
 * Output      :  pu4Error            - Error on failure 
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE 
 *****************************************************************************/
INT4
Ofc131PktMultiPartReqau1PadAction (tOfp131MultiPartReq * pOfp131MultiPartReq,
                                   VOID *pConnPtr, tCRU_BUF_CHAIN_HEADER * pBuf,
                                   UINT4 *pu4Error)
{
    UNUSED_PARAM (pu4Error);
    UNUSED_PARAM (pConnPtr);
    UNUSED_PARAM (pBuf);
    UNUSED_PARAM (pOfp131MultiPartReq);
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function    :  Ofc131PktMultiPartReqau1BodyAction
 * Description :  This function is for processing Body of Multipart Message  
 * Input       :  pOfp131MultiPartReq - Pointer Multipart Request
 *                pConnPtr            - Connection Pointer
 *                pBuf                - CRU Buffer Pointer
 * Output      :  pu4Error            - Error on failure 
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE 
 *****************************************************************************/
INT4
Ofc131PktMultiPartReqau1BodyAction (tOfp131MultiPartReq * pOfp131MultiPartReq,
                                    VOID *pConnPtr,
                                    tCRU_BUF_CHAIN_HEADER * pBuf,
                                    UINT4 *pu4Error)
{
    UNUSED_PARAM (pOfp131MultiPartReq);
    UNUSED_PARAM (pConnPtr);
    UNUSED_PARAM (pu4Error);
    UNUSED_PARAM (pBuf);
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function    :  Ofc131PktBarrierReqau1DummyAction
 * Description :  This function is for processing Dummy in Barrier Request  
 * Input       :  pOfp131BarrierReq - Pointer to Barrier Request
 *                pConnPtr          - Connection Pointer
 *                pBuf              - CRU Buffer Pointer
 * Output      :  pu4Error          - Error on failure 
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE 
 *****************************************************************************/
INT4
Ofc131PktBarrierReqau1DummyAction (tOfp131BarrierReq * pOfp131BarrierReq,
                                   VOID *pConnPtr, tCRU_BUF_CHAIN_HEADER * pBuf,
                                   UINT4 *pu4Error)
{
    tOfcFsofcControllerConnEntry *pLocal = NULL;
    tOfp131BarrierRply *pOfp131BarrierRply = NULL;
    INT4                i4RetVal = OSIX_SUCCESS;
    UINT4               u4VarLen = OFC_ZERO;

    OFC131_TRC_FUNC ((OFC131_FN_ENTRY,
                      "FUNC:Ofc131BarrierReqau1DummyAction\n"));

    pLocal = (tOfcFsofcControllerConnEntry *) pConnPtr;
    pOfp131BarrierRply = (tOfp131BarrierRply *) ((VOID *) pOfp131BarrierReq);
    u4VarLen = pLocal->u4RxLength - OFC_EIGHT;

    i4RetVal = Ofc131PktBarrierRplySend (pOfp131BarrierRply,
                                         pConnPtr, u4VarLen);
    if (i4RetVal != OSIX_SUCCESS)
    {
        OFC131_TRC_FUNC ((OFC_PKT_TRC, "Ofc131PktBarrierRplySend Failed\n"));
    }
    UNUSED_PARAM (pu4Error);
    UNUSED_PARAM (pBuf);
    OFC131_TRC_FUNC ((OFC131_FN_EXIT, "FUNC:Ofc131BarrierReqau1DummyAction\n"));
    return i4RetVal;
}

/****************************************************************************
 * Function    :  Ofc131PktQueueRequ4PortAction
 * Description :  This function is for processing Port in Queue Request  
 * Input       :  pOfp131QueueReq - Pointer to Queue Request Message
 *                pConnPtr        - Connection Pointer
 *                pBuf            - CRU Buffer Pointer
 * Output      :  pu4Error        - Error on failure 
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE 
 ****************************************************************************/
INT4
Ofc131PktQueueRequ4PortAction (tOfp131QueueReq * pQueueReq, VOID *pConnPtr,
                               tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 *pu4Error)
{
    tOfcFsofcIfEntry    IfEntry;
    tOfcFsofcIfEntry   *pIfEntry = NULL;
    tOfp131QueueRply   *pQueReply = NULL;
    tOfp131PktQueue    *pPktQue = NULL;
    tOfp131QueueHdr    *pQueHdr = NULL;    /* Queue Property Header */
    tOfp131QueueProp   *pQueProp = NULL;    /* Queue Property - rate */
    UINT4               u4VarLen = OFC_ZERO;
    UINT4               u4PortNum = OFC_ZERO;
    tOfcFsofcControllerConnEntry *pLocal = pConnPtr;
    tOfcFsofcCfgEntry  *pOfcFsofcCfgEntry = NULL;

    OFC131_TRC_FUNC ((OFC131_FN_ENTRY, "FUNC:Ofc131QueueRequ4PortAction\n"));

    pQueReply = (tOfp131QueueRply *) MemAllocMemBlk (OFC_MULTIPART_POOLID);
    if (pQueReply == NULL)
    {
        OFC131_TRC_FUNC ((OFC_PKT_TRC,
                          "MemAllocMemBlk Failed, return FAILURE\n"));
        return OFC_FAILURE;
    }

    pOfcFsofcCfgEntry =
        OfcGetFsofcCfgEntry (pLocal->MibObject.u4FsofcContextId);
    if (pOfcFsofcCfgEntry == NULL)
    {
        OFC131_TRC_FUNC ((OFC_PKT_TRC, "\r\nOfc131PktQueueRequ4PortAction: "
                          "Get CFG Entry Failed\n"));
        MemReleaseMemBlock (OFC_MULTIPART_POOLID, (UINT1 *) pQueReply);
        return OFC_FAILURE;
    }

#ifdef DPA_WANTED
    if (Ofc131DpaQueueConfigGet (pQueueReq->u4Port, pQueReply, &u4VarLen)
        == OFC_SUCCESS)
    {
        Ofc131PktQueueRplySend (pQueReply, pConnPtr, u4VarLen);
    }
    MemReleaseMemBlock (OFC_MULTIPART_POOLID, (UINT1 *) pQueReply);
    return OFC_SUCCESS;
#endif /* DPA_WANTED */

    pQueReply->u4Port = OSIX_HTONL (pQueueReq->u4Port);
    if (pQueueReq->u4Port != OFPP_ANY)
    {
        /* Request is only for one port */
        MEMSET (&IfEntry, OFC_ZERO, sizeof (tOfcFsofcIfEntry));
        IfEntry.MibObject.i4FsofcIfIndex = (INT4) pQueueReq->u4Port;
        IfEntry.MibObject.u4FsofcContextId =
            pOfcFsofcCfgEntry->MibObject.u4FsofcContextId;

        if (OfcGetAllFsofcIfTable (&IfEntry) != OFC_SUCCESS)
        {
            /* Return Error */
            OFC131_TRC_FUNC ((OFC_PKT_TRC, "Unable to fetch the If-Entry for,"
                              " the requested port, return FAILURE\n"));
            *pu4Error = OFC131_QUEUE_INVALID_PORT;
            MemReleaseMemBlock (OFC_MULTIPART_POOLID, (UINT1 *) pQueReply);
            return OFC_FAILURE;
        }

        /* Fill in Queue Information */
        pPktQue = (tOfp131PktQueue *) ((VOID *) (((UINT1 *) pQueReply) +
                                                 sizeof (tOfp131QueueRply)));
        pPktQue->u4QueueId = OFC_DEFAULT_QUEUE;
        pPktQue->u4Port = OSIX_HTONL (pQueueReq->u4Port);
        pPktQue->u2Len = OSIX_HTONS (OFC131_QUEUE_DESCRIPTION_SIZE);

        /* Fill in the Queue Properties - MIN RATE */
        pQueHdr = (tOfp131QueueHdr *) ((VOID *) (((UINT1 *) pPktQue) +
                                                 sizeof (tOfp131PktQueue)));
        pQueHdr->u2Property = OSIX_HTONS (OFCQT_MIN_RATE);
        pQueHdr->u2Len = OSIX_HTONS (OFC131_QUEUE_PROPERTY_SIZE);

        /* Fill in the Min-Rate */
        pQueProp = (tOfp131QueueProp *) ((VOID *) (((UINT1 *) pQueHdr) +
                                                   sizeof (tOfp131QueueHdr)));
        pQueProp->u2Rate = OSIX_HTONS (OFCQ_MIN_RATE_UNCF);

        /* Fill in the Queue Properties - MAX RATE */
        pQueHdr = (tOfp131QueueHdr *) ((VOID *) (((UINT1 *) pPktQue) +
                                                 sizeof (tOfp131PktQueue) +
                                                 OFC131_QUEUE_PROPERTY_SIZE));
        pQueHdr->u2Property = OSIX_HTONS (OFCQT_MAX_RATE);
        pQueHdr->u2Len = OSIX_HTONS (OFC131_QUEUE_PROPERTY_SIZE);

        /* Fill in the Max-Rate */
        pQueProp = (tOfp131QueueProp *) ((VOID *) (((UINT1 *) pQueHdr) +
                                                   sizeof (tOfp131QueueHdr)));
        pQueProp->u2Rate = OSIX_HTONS (OFCQ_MAX_RATE_UNCF);

        /* Variable length starts from the first queue information itself */
        u4VarLen = sizeof (tOfp131QueueRply) + OFC131_QUEUE_DESCRIPTION_SIZE;
    }
    else
    {
        /* Request spans for multiple ports */
        pIfEntry =
            (tOfcFsofcIfEntry *) RBTreeGetFirst (gOfcGlobals.OfcGlbMib.
                                                 FsofcIfTable);
        if (pIfEntry == NULL)
        {
            /* Return Error */
            OFC131_TRC_FUNC ((OFC_PKT_TRC, "Unable to get the first If-Entry,"
                              " return FAILURE\n"));
            *pu4Error = OFC131_QUEUE_INVALID_PORT;
            MemReleaseMemBlock (OFC_MULTIPART_POOLID, (UINT1 *) pQueReply);
            return OFC_FAILURE;
        }

        while (pIfEntry != NULL)
        {
            u4PortNum = (UINT4) pIfEntry->MibObject.i4FsofcIfIndex;

            /* Fill in Queue Information */
            pPktQue = (tOfp131PktQueue *) ((VOID *) (((UINT1 *) pQueReply) +
                                                     sizeof
                                                     (tOfp131QueueRply)));
            pPktQue->u4QueueId = OFC_DEFAULT_QUEUE;
            pPktQue->u4Port = OSIX_HTONL (u4PortNum);
            pPktQue->u2Len = OSIX_HTONS (OFC131_QUEUE_DESCRIPTION_SIZE);

            /* Fill in the Queue Properties - MIN RATE */
            pQueHdr = (tOfp131QueueHdr *) ((VOID *) (((UINT1 *) pPktQue) +
                                                     sizeof (tOfp131PktQueue)));
            pQueHdr->u2Property = OSIX_HTONS (OFCQT_MIN_RATE);
            pQueHdr->u2Len = OSIX_HTONS (OFC131_QUEUE_PROPERTY_SIZE);

            /* Fill in the Min-Rate */
            pQueProp = (tOfp131QueueProp *) ((VOID *) (((UINT1 *) pQueHdr) +
                                                       sizeof
                                                       (tOfp131QueueHdr)));
            pQueProp->u2Rate = OSIX_HTONS (OFCQ_MIN_RATE_UNCF);

            /* Fill in the Queue Properties - MAX RATE */
            pQueHdr = (tOfp131QueueHdr *) ((VOID *) (((UINT1 *) pPktQue) +
                                                     sizeof (tOfp131PktQueue) +
                                                     OFC131_QUEUE_PROPERTY_SIZE));
            pQueHdr->u2Property = OSIX_HTONS (OFCQT_MAX_RATE);
            pQueHdr->u2Len = OSIX_HTONS (OFC131_QUEUE_PROPERTY_SIZE);

            /* Fill in the Max-Rate */
            pQueProp = (tOfp131QueueProp *) ((VOID *) (((UINT1 *) pQueHdr) +
                                                       sizeof
                                                       (tOfp131QueueHdr)));
            pQueProp->u2Rate = OSIX_HTONS (OFCQ_MAX_RATE_UNCF);

            /* Increment Length */
            u4VarLen += sizeof (tOfp131QueueRply) +
                OFC131_QUEUE_DESCRIPTION_SIZE;
            if ((tOfcFsofcIfEntry *)
                RBTreeGetNext (gOfcGlobals.OfcGlbMib.FsofcIfTable, pIfEntry,
                               NULL) != NULL)
            {
                pQueReply =
                    (tOfp131QueueRply
                     *) ((VOID *) (((UINT1 *) pQueReply) + u4VarLen));
            }

            /* we are using the multipart mem-pool */
            if (OFC_MAX_MULTIPART_MSG_SIZE < (u4VarLen +
                                              sizeof (tOfp131QueueRply) +
                                              OFC131_QUEUE_DESCRIPTION_SIZE))
            {
                /* We can quit now, but sending what all we can is fine! */
                break;
            }
            /* Fetch the Next Port */
            pIfEntry =
                (tOfcFsofcIfEntry *) RBTreeGetNext (gOfcGlobals.OfcGlbMib.
                                                    FsofcIfTable, pIfEntry,
                                                    NULL);
        }
    }
    Ofc131PktQueueRplySend (pQueReply, pConnPtr, u4VarLen);
    MemReleaseMemBlock (OFC_MULTIPART_POOLID, (UINT1 *) pQueReply);
    UNUSED_PARAM (pConnPtr);
    UNUSED_PARAM (pBuf);
    OFC131_TRC_FUNC ((OFC131_FN_EXIT, "FUNC:Ofc131QueueRequ4PortAction\n"));
    return OFC_SUCCESS;
}

/*****************************************************************************
 * Function    :  Ofc131PktQueueReqau1PadAction
 * Description :  This function is for processing Pad in Queue Request
 * Input       :  pOfp131QueueReq  - Pointer to Queue Request Message
 *                pConnPtr         - Connection Pointer
 *                pBuf             - CRU Buffer Pointer
 * Output      :  pu4Error - Error on failure 
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE 
 *****************************************************************************/
INT4
Ofc131PktQueueReqau1PadAction (tOfp131QueueReq * pOfp131QueueReq,
                               VOID *pConnPtr, tCRU_BUF_CHAIN_HEADER * pBuf,
                               UINT4 *pu4Error)
{
    UNUSED_PARAM (pu4Error);
    UNUSED_PARAM (pConnPtr);
    UNUSED_PARAM (pBuf);
    UNUSED_PARAM (pOfp131QueueReq);
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function    :  Ofc131PktRoleRequ4RoleAction
 * Description :  This function is for processing Role in Role Request  
 * Input       :  pOfp131RoleReq - Pointer to Role Request Message
 *                pConnPtr       - Connection Pointer
 *                pBuf           - CRU Buffer Pointer
 * Output      :  pu4Error       - Error on failure 
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE 
 *****************************************************************************/
INT4
Ofc131PktRoleRequ4RoleAction (tOfp131RoleReq * pOfp131RoleReq, VOID *pConnPtr,
                              tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 *pu4Error)
{
    tOfp131RoleRply     Ofp131RoleRply;
    INT4                i4RetVal = OSIX_SUCCESS;

    OFC131_TRC_FUNC ((OFC131_FN_ENTRY, "FUNC:Ofc131RoleRequ4RoleAction\n"));

    MEMSET (&Ofp131RoleRply, OFC_ZERO, sizeof (tOfp131RoleRply));

    i4RetVal = (INT4) OfcConnectionRoleHandler (pOfp131RoleReq, pConnPtr,
                                                &Ofp131RoleRply.u4Role);
    if (i4RetVal == OFC_FAILURE)
    {
        return OSIX_FAILURE;
    }

    Ofp131RoleRply.u8GenId.u4Hi = pOfp131RoleReq->u8GenId.u4Hi;
    Ofp131RoleRply.u8GenId.u4Lo = pOfp131RoleReq->u8GenId.u4Lo;

    i4RetVal = Ofc131PktRoleRplySend (&Ofp131RoleRply, pConnPtr,
                                      OFP131_VARLEN_ZERO);
    if (i4RetVal != OSIX_SUCCESS)
    {
        OFC131_TRC_FUNC ((OFC_PKT_TRC, "Sending role reply Failed\n"));
    }
    UNUSED_PARAM (pu4Error);
    UNUSED_PARAM (pBuf);
    OFC131_TRC_FUNC ((OFC131_FN_EXIT, "FUNC:Ofc131RoleRequ4RoleAction\n"));
    return i4RetVal;
}

/*****************************************************************************
 * Function    :  Ofc131PktRoleReqau1PadAction
 * Description :  This function is for processing Padding in Role Request  
 * Input       :  pOfp131RoleReq - Pointer to Role Request Message
 *                pConnPtr       - Connection Pointer
 *                pBuf           - CRU Buffer Pointer
 * Output      :  pu4Error       - Error on failure 
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE 
 *****************************************************************************/
INT4
Ofc131PktRoleReqau1PadAction (tOfp131RoleReq * pOfp131RoleReq, VOID *pConnPtr,
                              tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 *pu4Error)
{
    UNUSED_PARAM (pu4Error);
    UNUSED_PARAM (pConnPtr);
    UNUSED_PARAM (pBuf);
    UNUSED_PARAM (pOfp131RoleReq);
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function    :  Ofc131PktRoleRequ8GenIdAction
 * Description :  This function is for processing GenerationId in Role Request
 * Input       :  pOfp131RoleReq - Pointer to Role Request Message
 *                pConnPtr       - Connection Pointer
 *                pBuf           - CRU Buffer Pointer
 * Output      :  pu4Error       - Error on failure 
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE 
 *****************************************************************************/
INT4
Ofc131PktRoleRequ8GenIdAction (tOfp131RoleReq * pOfp131RoleReq, VOID *pConnPtr,
                               tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 *pu4Error)
{
    UNUSED_PARAM (pu4Error);
    UNUSED_PARAM (pConnPtr);
    UNUSED_PARAM (pBuf);
    UNUSED_PARAM (pOfp131RoleReq);
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function    :  Ofc131PktAsyncConfigGetau1DummyAction
 * Description :  This function sends the reply for the 
 *                Async Get Config Request  
 * Input       :  pAsyncCfgGet  - Pointer to the Get Request message
 *                pConnPtr      - Connection Pointer
 *                pBuf          - CRU Buffer Pointer
 * Output      :  pu4Error      - Error on failure 
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE 
 ******************************************************************************/
INT4
Ofc131PktAsyncConfigGetau1DummyAction (tOfp131AsyncConfigGet * pAsyncCfgGet,
                                       VOID *pConnPtr,
                                       tCRU_BUF_CHAIN_HEADER * pBuf,
                                       UINT4 *pu4Error)
{
    tOfp131AsyncConfigRply AsyncCfgRply;
    tOfcFsofcControllerConnEntry *pLocal = NULL;
    INT4                i4ControllerRole = OFC_ZERO;
    INT4                i4RetVal = OFC_FAILURE;

    MEMSET (&AsyncCfgRply, OFC_ZERO, sizeof (tOfp131AsyncConfigRply));
    pLocal = (tOfcFsofcControllerConnEntry *) pConnPtr;
    i4ControllerRole = pLocal->MibObject.i4FsofcControllerRole;

    if ((OPENFLOW_CONNROLE_MASTER == i4ControllerRole) ||
        (OPENFLOW_CONNROLE_EQUAL == i4ControllerRole))
    {
        AsyncCfgRply.au4PktInMask[OFC_ZERO] =
            OSIX_HTONL (pLocal->au4PktInMask[OFC_ZERO]);
        AsyncCfgRply.au4PortStatusMask[OFC_ZERO] =
            OSIX_HTONL (pLocal->au4PortStatusMask[OFC_ZERO]);
        AsyncCfgRply.au4FlowRemovedMask[OFC_ZERO] =
            OSIX_HTONL (pLocal->au4FlowRemovedMask[OFC_ZERO]);
    }
    else if (OPENFLOW_CONNROLE_SLAVE == i4ControllerRole)
    {
        AsyncCfgRply.au4PktInMask[OFC_ONE] =
            OSIX_HTONL (pLocal->au4PktInMask[OFC_ONE]);
        AsyncCfgRply.au4PortStatusMask[OFC_ONE] =
            OSIX_HTONL (pLocal->au4PortStatusMask[OFC_ONE]);
        AsyncCfgRply.au4FlowRemovedMask[OFC_ONE] =
            OSIX_HTONL (pLocal->au4FlowRemovedMask[OFC_ONE]);
    }
    i4RetVal = Ofc131PktAsyncConfigRplySend (&AsyncCfgRply, NULL,
                                             OFP131_VARLEN_ZERO);
    if (i4RetVal != OSIX_SUCCESS)
    {
        OFC131_TRC_FUNC ((OFC_PKT_TRC, "Ofc131PktBarrierRplySend Failed\n"));
    }
    UNUSED_PARAM (pu4Error);
    UNUSED_PARAM (pBuf);
    UNUSED_PARAM (pAsyncCfgGet);
    OFC131_TRC_FUNC ((OFC131_FN_EXIT,
                      "FUNC:Ofc131AsyncConfigGetau1DummyAction\n"));
    return i4RetVal;
}

/*****************************************************************************
 * Function    :  Ofc131PktAsyncConfigSetau4PktInMaskAction
 * Description :  This function is for processing PacketIn Mask in 
 *                AsyncConfigSet Request  
 * Input       :  pOfp131AsyncConfigSet - Pointer to AsyncConfigSet Request
 *                pConnPtr              - Connection Pointer
 *                pBuf                  - CRU Buffer Pointer
 * Output      :  pu4Error              - Error on failure 
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE 
 *****************************************************************************/
INT4
Ofc131PktAsyncConfigSetau4PktInMaskAction (tOfp131AsyncConfigSet *
                                           pAsyncConfigSet,
                                           VOID *pConnPtr,
                                           tCRU_BUF_CHAIN_HEADER * pBuf,
                                           UINT4 *pu4Error)
{
    tOfcFsofcControllerConnEntry *pLocal = NULL;
    INT4                i4ControllerRole = OFC_ZERO;

    OFC131_TRC_FUNC ((OFC131_FN_ENTRY,
                      "FUNC:Ofc131AsyncConfigSetau4PktInMaskAction\n"));

    pLocal = (tOfcFsofcControllerConnEntry *) pConnPtr;
    i4ControllerRole = pLocal->MibObject.i4FsofcControllerRole;

    if ((OPENFLOW_CONNROLE_MASTER == i4ControllerRole)
        || (OPENFLOW_CONNROLE_EQUAL == i4ControllerRole))
    {
        pLocal->au4PktInMask[OFC_ZERO] =
            OSIX_NTOHL (pAsyncConfigSet->au4PktInMask[OFC_ZERO]);
    }
    else if (OPENFLOW_CONNROLE_SLAVE == i4ControllerRole)
    {
        pLocal->au4PktInMask[OFC_ONE] =
            OSIX_NTOHL (pAsyncConfigSet->au4PktInMask[OFC_ONE]);
    }
    UNUSED_PARAM (pu4Error);
    UNUSED_PARAM (pBuf);
    OFC131_TRC_FUNC ((OFC131_FN_EXIT,
                      "FUNC:Ofc131AsyncConfigSetau4PktInMaskAction\n"));
    return OFC_SUCCESS;
}

/****************************************************************************
 * Function    :  Ofc131PktAsyncConfigSetau4PortStatusMaskAction
 * Description :  This function is for processing PortStatus Mask in 
 *                AsyncConfigSet Request  
 * Input       :  pOfp131AsyncConfigSet - Pointer to AsyncConfigSet Request
 *                pConnPtr              - Connection Pointer
 *                pBuf                  - CRU Buffer Pointer
 * Output      :  pu4Error              - Error on failure 
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE 
 ****************************************************************************/
INT4
Ofc131PktAsyncConfigSetau4PortStatusMaskAction (tOfp131AsyncConfigSet *
                                                pAsyncConfigSet,
                                                VOID *pConnPtr,
                                                tCRU_BUF_CHAIN_HEADER * pBuf,
                                                UINT4 *pu4Error)
{
    tOfcFsofcControllerConnEntry *pLocal = NULL;
    INT4                i4ControllerRole = OFC_ZERO;

    OFC131_TRC_FUNC ((OFC131_FN_ENTRY,
                      "FUNC:Ofc131AsyncConfigSetau4PktInMaskAction\n"));

    pLocal = (tOfcFsofcControllerConnEntry *) pConnPtr;
    i4ControllerRole = pLocal->MibObject.i4FsofcControllerRole;

    if ((OPENFLOW_CONNROLE_MASTER == i4ControllerRole) ||
        (OPENFLOW_CONNROLE_EQUAL == i4ControllerRole))
    {
        pLocal->au4PortStatusMask[OFC_ZERO] =
            OSIX_NTOHL (pAsyncConfigSet->au4PortStatusMask[OFC_ZERO]);
    }
    else if (OPENFLOW_CONNROLE_SLAVE == i4ControllerRole)
    {
        pLocal->au4PortStatusMask[OFC_ONE] =
            OSIX_NTOHL (pAsyncConfigSet->au4PortStatusMask[OFC_ONE]);
    }
    UNUSED_PARAM (pu4Error);
    UNUSED_PARAM (pBuf);
    OFC131_TRC_FUNC ((OFC131_FN_EXIT,
                      "FUNC:Ofc131AsyncConfigSetau4PktInMaskAction\n"));
    return OFC_SUCCESS;
}

/*****************************************************************************
 * Function    :  Ofc131PktAsyncConfigSetau4FlowRemovedMaskAction
 * Description :  This function is for processing Flow Removed Mask in 
 *                AsyncConfigSet Request  
 * Input       :  pOfp131AsyncConfigSet - Pointer to AsyncConfigSet Request
 *                pConnPtr              - Connection Pointer
 *                pBuf                  - CRU Buffer Pointer
 * Output      :  pu4Error              - Error on failure 
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE 
 *****************************************************************************/
INT4
Ofc131PktAsyncConfigSetau4FlowRemovedMaskAction (tOfp131AsyncConfigSet *
                                                 pAsyncConfigSet,
                                                 VOID *pConnPtr,
                                                 tCRU_BUF_CHAIN_HEADER * pBuf,
                                                 UINT4 *pu4Error)
{
    tOfcFsofcControllerConnEntry *pLocal = NULL;
    INT4                i4ControllerRole = OFC_ZERO;

    OFC131_TRC_FUNC ((OFC131_FN_ENTRY,
                      "FUNC:Ofc131AsyncConfigSetau4PktInMaskAction\n"));

    pLocal = (tOfcFsofcControllerConnEntry *) pConnPtr;
    i4ControllerRole = pLocal->MibObject.i4FsofcControllerRole;
    if ((OPENFLOW_CONNROLE_MASTER == i4ControllerRole) ||
        (OPENFLOW_CONNROLE_EQUAL == i4ControllerRole))
    {
        pLocal->au4FlowRemovedMask[OFC_ZERO] =
            OSIX_NTOHL (pAsyncConfigSet->au4FlowRemovedMask[OFC_ZERO]);
    }
    else if (OPENFLOW_CONNROLE_SLAVE == i4ControllerRole)
    {
        pLocal->au4FlowRemovedMask[OFC_ONE] =
            OSIX_NTOHL (pAsyncConfigSet->au4FlowRemovedMask[OFC_ONE]);
    }
    UNUSED_PARAM (pu4Error);
    UNUSED_PARAM (pBuf);
    OFC131_TRC_FUNC ((OFC131_FN_EXIT,
                      "FUNC:Ofc131AsyncConfigSetau4PktInMaskAction\n"));
    return OFC_SUCCESS;
}

/*****************************************************************************
 * Function    :  Ofc131PktMeterModu2CmdAction
 * Description :  This function is for processing Meter Mod Message
 * Input       :  pOfp131MeterMod - Pointer to Meter Mod Message
 *                pConnPtr        - Connection Pointer
 *                pBuf            - CRU Buffer Pointer
 * Output      :  pu4Error        - Error on failure 
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE 
 *****************************************************************************/
INT4
Ofc131PktMeterModu2CmdAction (tOfp131MeterMod * pOfp131MeterMod, VOID *pConnPtr,
                              tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 *pu4Error)
{
    tOfp131ErrMsg       Ofp131ErrorMsg;
    tOfcFsofcCfgEntry  *pOfcFsofcCfgEntry = NULL;
    tOfcFsofcMeterEntry *pOfcMtrEntry = NULL;
    tOfp131MeterBandHdr *pOfpMtrBandHdr = NULL;
    tOfpMtrDrop        *pOfpMtrDrop = NULL;
    tOfpMtrDscp        *pOfpMtrDscp = NULL;
    tOfpMtrExpr        *pOfpMtrExp = NULL;
    tOfcMeterBand      *pMtrBand = NULL;
    tOfcFsofcControllerConnEntry *pLocal = NULL;
    UINT4               u4MeterLen = OFC_ZERO;
    UINT4               u4TempLen = OFC_ZERO;

    OFC131_TRC_FUNC ((OFC131_FN_ENTRY, "FUNC:Ofc131MeterModu2CmdAction\n"));

    MEMSET (&Ofp131ErrorMsg, OFC_ZERO, sizeof (tOfp131ErrMsg));
    pLocal = (tOfcFsofcControllerConnEntry *) pConnPtr;

    pOfcFsofcCfgEntry =
        OfcGetFsofcCfgEntry (pLocal->MibObject.u4FsofcContextId);
    if (pOfcFsofcCfgEntry == NULL)
    {
        OFC131_TRC_FUNC ((OFC_PKT_TRC, "\r\nOfc131PktFeatureReqau1DummyAction "
                          "Get CFG Entry Failed\n"));
        return OSIX_FAILURE;
    }

    /* Moving the offset to the ofpmeterband  starting */
    pOfpMtrBandHdr = (tOfp131MeterBandHdr *) ((VOID *) (pLocal->pRcvPkt +
                                                        OFC131_METERBAND_OFFSET));

    pOfcMtrEntry =
        (tOfcFsofcMeterEntry *) MemAllocMemBlk (OFC_FSOFCMETERTABLE_POOLID);
    if (NULL == pOfcMtrEntry)
    {
        /* Error code to send to the controller */
        Ofp131ErrorMsg.u2Type = OFPET_131_METER_MOD_FAILED;
        Ofp131ErrorMsg.u2Code = OFPMMFC_OUT_OF_METERS;
        Ofc131PktErrMsgSend (&Ofp131ErrorMsg, pConnPtr,
                             sizeof (Ofp131ErrorMsg.au1Data));

        OFC131_TRC_FUNC ((OFC_PKT_TRC,
                          "MemAllocMemBlk Failed for MeterEntry\n"));
        return OSIX_FAILURE;
    }

    MEMSET (pOfcMtrEntry, OFC_ZERO, sizeof (tOfcFsofcMeterEntry));
    pOfcMtrEntry->MibObject.u4FsofcMeterIndex = pOfp131MeterMod->u4MeterId;
    pOfcMtrEntry->u2Flags = pOfp131MeterMod->u2Flags;
    pOfcMtrEntry->MibObject.u4FsofcContextId =
        pOfcFsofcCfgEntry->MibObject.u4FsofcContextId;

    /*
     * Total meterband length is (Total received length) - 
     * (Openflow header ,8 bytes + meter mod header ,8 bytes)
     */

    u4MeterLen = pLocal->u4RxLength - OFC131_METERBAND_OFFSET;

    TMO_SLL_Init (&(pOfcMtrEntry->BandList));
    u4TempLen = OFC_ZERO;

    /* iterating the Loop till the meterband is present */
    while (u4MeterLen > OFC_ZERO)
    {
        pMtrBand = (tOfcMeterBand *) MemAllocMemBlk (OFC_METER_BAND_POOLID);
        if (NULL == pMtrBand)
        {
            Ofp131ErrorMsg.u2Type = OFPET_131_METER_MOD_FAILED;
            Ofp131ErrorMsg.u2Code = OFPMMFC_OUT_OF_BANDS;
            Ofc131PktErrMsgSend (&Ofp131ErrorMsg, pConnPtr,
                                 sizeof (Ofp131ErrorMsg.au1Data));
            Ofc131MeterMemReleaseBands (&pOfcMtrEntry->BandList);
            MemReleaseMemBlock (OFC_FSOFCMETERTABLE_POOLID,
                                (UINT1 *) pOfcMtrEntry);
            OFC131_TRC_FUNC ((OFC_PKT_TRC,
                              "MemAllocMemBlk Failed for Meterband\n"));
            return OSIX_FAILURE;
        }

        MEMSET (pMtrBand, OFC_ZERO, sizeof (tOfcMeterBand));
        pMtrBand->u2Type = OSIX_NTOHS (pOfpMtrBandHdr->u2Type);

        switch (pMtrBand->u2Type)
        {
                /*
                 * Typecasting the MeterBand Hdr based on its type
                 * to the corresponding Structure
                 */

            case OFPMBT_DROP:
                pOfpMtrDrop = (tOfpMtrDrop *) ((VOID *) pOfpMtrBandHdr);
                pMtrBand->u4Rate = OSIX_NTOHL (pOfpMtrDrop->u4Rate);
                pMtrBand->u4BurstSize = OSIX_NTOHL (pOfpMtrDrop->u4BurstSize);
                break;
            case OFPMBT_DSCP_REMARK:
                pOfpMtrDscp = (tOfpMtrDscp *) ((VOID *) pOfpMtrBandHdr);
                pMtrBand->u4Rate = OSIX_NTOHL (pOfpMtrDscp->u4Rate);
                pMtrBand->u4BurstSize = OSIX_NTOHL (pOfpMtrDscp->u4BurstSize);
                pMtrBand->BandFlds.u1PrecLvl = pOfpMtrDscp->au1PrecLevel;
                break;
            case OFPMBT_EXPERIMENTER:
                pOfpMtrExp = (tOfpMtrExpr *) ((VOID *) pOfpMtrBandHdr);
                pMtrBand->u4Rate = OSIX_NTOHL (pOfpMtrExp->u4Rate);
                pMtrBand->u4BurstSize = OSIX_NTOHL (pOfpMtrExp->u4BurstSize);
                pMtrBand->BandFlds.u4Experimenter =
                    OSIX_NTOHL (pOfpMtrExp->u4Experimenter);
                break;
            default:
                Ofp131ErrorMsg.u2Type = OFPET_131_METER_MOD_FAILED;
                Ofp131ErrorMsg.u2Code = OFPMMFC_BAD_BAND;
                Ofc131PktErrMsgSend (&Ofp131ErrorMsg, pConnPtr,
                                     sizeof (Ofp131ErrorMsg.au1Data));
                Ofc131MeterMemReleaseBands (&pOfcMtrEntry->BandList);
                MemReleaseMemBlock (OFC_FSOFCMETERTABLE_POOLID,
                                    (UINT1 *) pOfcMtrEntry);
                MemReleaseMemBlock (OFC_METER_BAND_POOLID, (UINT1 *) pMtrBand);
                return OSIX_FAILURE;
        }

        TMO_SLL_Add (&(pOfcMtrEntry->BandList), &(pMtrBand->Node));
        KW_FALSEPOSITIVE_FIX (pMtrBand);
        u4MeterLen = u4MeterLen - (UINT4) sizeof (tOfpMtrDrop);

        /* 
         * Moving the Offset to point to the Next Meterband structure. 
         * Since the size of all the three bands is as same as Dropband,
         * its size is used while calucalting offset
         * */

        u4TempLen += (UINT4) sizeof (tOfpMtrDrop);
        pOfpMtrBandHdr = (tOfp131MeterBandHdr *) ((VOID *)
                                                  (((UINT1 *) pOfpMtrBandHdr) +
                                                   sizeof (tOfpMtrDrop)));
    }

    Ofc131MeterHandleMeterModMsgs (pOfcFsofcCfgEntry, pOfcMtrEntry,
                                   (pOfp131MeterMod->u2Cmd));
    UNUSED_PARAM (pu4Error);
    UNUSED_PARAM (pBuf);

    OFC131_TRC_FUNC ((OFC131_FN_EXIT, "FUNC:Ofc131MeterModu2CmdAction\n"));
    return OFC_SUCCESS;
}

/*****************************************************************************
 * Function    :  Ofc131PktMeterModu2FlagsAction
 * Description :  This function is for processing Flags in Meter Mod
 * Input       :  pOfp131MeterMod - Pointer to Meter Mod Message
 *                pConnPtr        - Connection Pointer
 *                pBuf            - CRU Buffer Pointer
 * Output      :  pu4Error        - Error on failure 
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE 
 *****************************************************************************/
INT4
Ofc131PktMeterModu2FlagsAction (tOfp131MeterMod * pOfp131MeterMod,
                                VOID *pConnPtr, tCRU_BUF_CHAIN_HEADER * pBuf,
                                UINT4 *pu4Error)
{
    UNUSED_PARAM (pu4Error);
    UNUSED_PARAM (pConnPtr);
    UNUSED_PARAM (pBuf);
    UNUSED_PARAM (pOfp131MeterMod);
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function    :  Ofc131PktMeterModu4MeterIdAction
 * Description :  This function is for processing MeterId in Meter Mod
 * Input       :  pOfp131MeterMod - Pointer to Meter Mod Message
 *                pConnPtr        - Connection Pointer
 *                pBuf            - CRU Buffer Pointer
 * Output      :  pu4Error        - Error on failure 
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE 
 *****************************************************************************/
INT4
Ofc131PktMeterModu4MeterIdAction (tOfp131MeterMod * pOfp131MeterMod,
                                  VOID *pConnPtr, tCRU_BUF_CHAIN_HEADER * pBuf,
                                  UINT4 *pu4Error)
{
    UNUSED_PARAM (pu4Error);
    UNUSED_PARAM (pConnPtr);
    UNUSED_PARAM (pBuf);
    UNUSED_PARAM (pOfp131MeterMod);
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function    :  Ofc131PktMeterModOfpMeterBandHdrAction
 * Description :  This function is for processing MeterBand in Meter Mod
 * Input       :  pOfp131MeterMod - Pointer to Meter Mod Message
 *                pConnPtr        - Connection Pointer
 *                pBuf            - CRU Buffer Pointer
 * Output      :  pu4Error        - Error on failure 
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE 
 *****************************************************************************/
INT4
Ofc131PktMeterModOfpMeterBandHdrAction (tOfp131MeterMod * pOfp131MeterMod,
                                        VOID *pConnPtr,
                                        tCRU_BUF_CHAIN_HEADER * pBuf,
                                        UINT4 *pu4Error)
{
    UNUSED_PARAM (pu4Error);
    UNUSED_PARAM (pConnPtr);
    UNUSED_PARAM (pBuf);
    UNUSED_PARAM (pOfp131MeterMod);
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function    :  Ofc131PktErrorHandle
 * Description :  This function is for parsing Errors generated and sending
 *                appropriate Error Message to Controller  
 * Input       :  u4Error  - Error Generated
 *                pConnPtr - Connection Pointer
 *                pBuf     - CRU Buffer Pointer
 * Output      :  None 
 * Returns     :  None 
 *****************************************************************************/
VOID
Ofc131PktErrorHandle (UINT4 u4Error, VOID *pConnPtr,
                      tCRU_BUF_CHAIN_HEADER * pBuf)
{
    tOfp131Pkt          Ofp;
    tOfp131ErrMsg       Ofp131ErrorMsg;
    tOfcFsofcControllerConnEntry *pLocal = NULL;

    OFC131_TRC_FUNC ((OFC131_FN_ENTRY, "FUNC:Ofc131PktErrorHandle\n"));

    MEMSET (&Ofp131ErrorMsg, OFC_ZERO, sizeof (tOfp131ErrMsg));
    MEMSET (&Ofp, OFC_ZERO, sizeof (tOfp131Pkt));
    pLocal = (tOfcFsofcControllerConnEntry *) pConnPtr;

    OFC131_GET_1_BYTE (pBuf, OFC131_OFP_U1TYPE_OFFSET, Ofp.u1Type);
    switch (u4Error)
    {
        case OFC131_OFP_U1VERSION_VALIDATE_ERR:
            if (Ofp.u1Type == OFPT_HELLO)
            {
                Ofp131ErrorMsg.u2Type = OFPET_HELLO_FAILED;
            }
            else
            {
                Ofp131ErrorMsg.u2Type = OFPET_BAD_REQUEST;
            }

            Ofp131ErrorMsg.u2Code = OFC_OFP_U1VERSION_VALIDATE_ERR;
            Ofc131PktErrMsgSend (&Ofp131ErrorMsg, pConnPtr,
                                 (INT4) sizeof (Ofp131ErrorMsg.au1Data));

            /*
             * Only when the Controller socket state is not connected
             * try reconnecting. Otherwise SelAddWrFd will take care of reconnecting
             * Close all the timers, if running before closing the connections
             */
            if (pLocal->ConnRetryTimer.tmrNode.u2Flags & OFC_TMR_RUNNING)
            {
                OfcTmrStopTmr (&(pLocal->ConnRetryTimer));
            }
            if (pLocal->EchoReqTimer.tmrNode.u2Flags & OFC_TMR_RUNNING)
            {
                OfcTmrStopTmr (&(pLocal->EchoReqTimer));
            }
            if (pLocal->EchoInterTimer.tmrNode.u2Flags & OFC_TMR_RUNNING)
            {
                OfcTmrStopTmr (&(pLocal->EchoInterTimer));
            }

            /* Close the connection after sending the error */
            SelRemoveFd (pLocal->i4SocketDesc);
            close (pLocal->i4SocketDesc);
            pLocal->i4SocketDesc = OFC_MINUS_ONE;
            pLocal->MibObject.i4FsofcControllerConnState = OFC_NOT_CONNECTED;
            break;

        case OFC131_OFP_U1TYPE_VALIDATE_ERR:
            Ofp131ErrorMsg.u2Type = OFPET_BAD_REQUEST;
            Ofp131ErrorMsg.u2Code = OFC_OFP_U1TYPE_VALIDATE_ERR;
            Ofc131PktErrMsgSend (&Ofp131ErrorMsg, pConnPtr,
                                 (INT4) sizeof (Ofp131ErrorMsg.au1Data));
            break;

        case OFC131_EXPERIMENTERHDR_U4EXPTYPE_VALIDATE_ERR:
            Ofp131ErrorMsg.u2Type = OFPET_BAD_REQUEST;
            Ofp131ErrorMsg.u2Code = OFCBRC_BAD_EXPERIMENTER;
            Ofc131PktErrMsgSend (&Ofp131ErrorMsg, pConnPtr,
                                 (INT4) sizeof (Ofp131ErrorMsg.au1Data));
            break;

        case OFC131_FLOWMOD_INVALID_TABLE_ERR:
            Ofp131ErrorMsg.u2Type = OFPET_131_FLOW_MOD_FAILED;
            Ofp131ErrorMsg.u2Code = OFPFMFC_131_BAD_TABLE_ID;
            Ofc131PktErrMsgSend (&Ofp131ErrorMsg, pConnPtr,
                                 (INT4) sizeof (Ofp131ErrorMsg.au1Data));
            break;

        case OFC131_FLOWMOD_TABLE_FULL_ERR:
            /* Table is Full */
            Ofp131ErrorMsg.u2Type = OFPET_131_FLOW_MOD_FAILED;
            Ofp131ErrorMsg.u2Code = OFPFMFC_131_TABLE_FULL;
            Ofc131PktErrMsgSend (&Ofp131ErrorMsg,
                                 pConnPtr,
                                 (INT4) sizeof (Ofp131ErrorMsg.au1Data));
            break;

        case OFC131_FLOWMOD_INVALID_FLAGS_ERR:
            /* Invalid Flags */
            Ofp131ErrorMsg.u2Type = OFPET_131_FLOW_MOD_FAILED;
            Ofp131ErrorMsg.u2Code = OFPFMFC_131_BAD_FLAGS;
            Ofc131PktErrMsgSend (&Ofp131ErrorMsg,
                                 pConnPtr,
                                 (INT4) sizeof (Ofp131ErrorMsg.au1Data));
            break;

        case OFC131_FLOWMOD_UNKNOWN_COMMAND_ERR:
            /* Unknown Flow Mod Command Type */
            Ofp131ErrorMsg.u2Type = OFPET_131_FLOW_MOD_FAILED;
            Ofp131ErrorMsg.u2Code = OFPFMFC_131_BAD_COMMAND;
            Ofc131PktErrMsgSend (&Ofp131ErrorMsg,
                                 pConnPtr, sizeof (Ofp131ErrorMsg.au1Data));
            break;

        case OFC131_FLOWMOD_OVERLAP_EXIST_ERR:
            /* Overlapping entry exists for Flow Add */
            Ofp131ErrorMsg.u2Type = OFPET_131_FLOW_MOD_FAILED;
            Ofp131ErrorMsg.u2Code = OFPFMFC_131_OVERLAP;
            Ofc131PktErrMsgSend (&Ofp131ErrorMsg,
                                 pConnPtr, sizeof (Ofp131ErrorMsg.au1Data));
            break;

        case OFC131_FLOWMOD_OFPMATCH_VALIDATE_ERR:
            /* Unsupported Match in Table */
            Ofp131ErrorMsg.u2Type = OFPET_131_BAD_MATCH;
            Ofp131ErrorMsg.u2Code = OFPBMC_BAD_TYPE;
            Ofc131PktErrMsgSend (&Ofp131ErrorMsg,
                                 pConnPtr, sizeof (Ofp131ErrorMsg.au1Data));
            break;

        case OFC131_FLOWMOD_INSTR_VALIDATE_ERR:
            /* Unsupported Instruction in Table */
            Ofp131ErrorMsg.u2Type = OFPET_131_BAD_INSTRUCTION;
            Ofp131ErrorMsg.u2Code = OFPBIC_UNSUP_INST;
            Ofc131PktErrMsgSend (&Ofp131ErrorMsg,
                                 pConnPtr, sizeof (Ofp131ErrorMsg.au1Data));
            break;

        case OFC131_FLOWMOD_UNKNOWN_INSTR_ERR:
            Ofp131ErrorMsg.u2Type = OFPET_131_BAD_INSTRUCTION;
            Ofp131ErrorMsg.u2Code = OFPBIC_UNKNOWN_INST;
            Ofc131PktErrMsgSend (&Ofp131ErrorMsg,
                                 pConnPtr, sizeof (Ofp131ErrorMsg.au1Data));
            break;

        case OFC131_FLOWMOD_BAD_TYPE_ERR:
            Ofp131ErrorMsg.u2Type = OFPET_131_BAD_MATCH;
            Ofp131ErrorMsg.u2Code = OFPBMC_BAD_TYPE;
            Ofc131PktErrMsgSend (&Ofp131ErrorMsg,
                                 pConnPtr, sizeof (Ofp131ErrorMsg.au1Data));
            break;

        case OFC131_FLOWMOD_BAD_FIELD_ERR:
            Ofp131ErrorMsg.u2Type = OFPET_131_BAD_MATCH;
            Ofp131ErrorMsg.u2Code = OFPBMC_BAD_FIELD;
            Ofc131PktErrMsgSend (&Ofp131ErrorMsg,
                                 pConnPtr, sizeof (Ofp131ErrorMsg.au1Data));
            break;

        case OFC131_FLOWMOD_BAD_VALUE_ERR:
            Ofp131ErrorMsg.u2Type = OFPET_131_BAD_MATCH;
            Ofp131ErrorMsg.u2Code = OFPBMC_BAD_VALUE;
            Ofc131PktErrMsgSend (&Ofp131ErrorMsg,
                                 pConnPtr, sizeof (Ofp131ErrorMsg.au1Data));
            break;

        case OFC131_FLOWMOD_ACT_VALIDATE_ERR:
            /* Unsupported Action in Table */
            Ofp131ErrorMsg.u2Type = OFPET_131_BAD_ACTION;
            Ofp131ErrorMsg.u2Code = OFPBAC_BAD_TYPE;
            Ofc131PktErrMsgSend (&Ofp131ErrorMsg,
                                 pConnPtr, sizeof (Ofp131ErrorMsg.au1Data));
            break;

        case OFC131_FLOWMOD_ACT_PORT_VALIDATE_ERR:
            Ofp131ErrorMsg.u2Type = OFPET_131_BAD_ACTION;
            Ofp131ErrorMsg.u2Code = OFPBAC_BAD_OUT_PORT;
            Ofc131PktErrMsgSend (&Ofp131ErrorMsg,
                                 pConnPtr, sizeof (Ofp131ErrorMsg.au1Data));
            break;

        case OFC131_FLOWMOD_ACT_GROUP_VALIDATE_ERR:
            Ofp131ErrorMsg.u2Type = OFPET_131_BAD_ACTION;
            Ofp131ErrorMsg.u2Code = OFPBAC_BAD_OUT_GROUP;
            Ofc131PktErrMsgSend (&Ofp131ErrorMsg,
                                 pConnPtr, sizeof (Ofp131ErrorMsg.au1Data));
            break;

        case OFC131_FLOWMOD_SETFIELD_VALIDATE_ERR:
            /* Unsupported Set Fields in Table */
            Ofp131ErrorMsg.u2Type = OFPET_131_BAD_ACTION;
            Ofp131ErrorMsg.u2Code = OFPBAC_BAD_SET_TYPE;
            Ofc131PktErrMsgSend (&Ofp131ErrorMsg,
                                 pConnPtr, sizeof (Ofp131ErrorMsg.au1Data));
            break;

        case OFC131_GROUPMOD_U2CMD_VALIDATE_ERR:
            /* Unsupported Group Command */
            Ofp131ErrorMsg.u2Type = OFPET_131_GROUP_MOD_FAILED;
            Ofp131ErrorMsg.u2Code = ISS_OFPGMFC_BAD_COMMAND;
            Ofc131PktErrMsgSend (&Ofp131ErrorMsg,
                                 pConnPtr, sizeof (Ofp131ErrorMsg.au1Data));
            break;

        case OFC131_GROUPMOD_U1TYPE_VALIDATE_ERR:
            /* Unsupported Group Type */
            Ofp131ErrorMsg.u2Type = OFPET_131_GROUP_MOD_FAILED;
            Ofp131ErrorMsg.u2Code = ISS_OFPGMFC_BAD_TYPE;
            Ofc131PktErrMsgSend (&Ofp131ErrorMsg,
                                 pConnPtr, sizeof (Ofp131ErrorMsg.au1Data));
            break;

        case OFC131_GROUPMOD_TABLE_FULL:
            /* Out of range groups */
            Ofp131ErrorMsg.u2Type = OFPET_131_GROUP_MOD_FAILED;
            Ofp131ErrorMsg.u2Code = ISS_OFPGMFC_OUT_OF_GROUPS;
            Ofc131PktErrMsgSend (&Ofp131ErrorMsg,
                                 pConnPtr, sizeof (Ofp131ErrorMsg.au1Data));
            break;

        case OFC131_GROUPMOD_MORE_BUCKET_ERR:
            /* Out of range buckets for group */
            Ofp131ErrorMsg.u2Type = OFPET_131_GROUP_MOD_FAILED;
            Ofp131ErrorMsg.u2Code = ISS_OFPGMFC_OUT_OF_BUCKETS;
            Ofc131PktErrMsgSend (&Ofp131ErrorMsg,
                                 pConnPtr, sizeof (Ofp131ErrorMsg.au1Data));
            break;

        case OFC131_GROUPMOD_BAD_BUCKET_ERR:
            /* Error in Bucket */
            Ofp131ErrorMsg.u2Type = OFPET_131_GROUP_MOD_FAILED;
            Ofp131ErrorMsg.u2Code = ISS_OFPGMFC_BAD_BUCKET;
            Ofc131PktErrMsgSend (&Ofp131ErrorMsg,
                                 pConnPtr, sizeof (Ofp131ErrorMsg.au1Data));
            break;

        case OFC131_GROUPMOD_U4GROUPID_VALIDATE_ERR:
            /* Unsupported Group Id */
            Ofp131ErrorMsg.u2Type = OFPET_131_GROUP_MOD_FAILED;
            Ofp131ErrorMsg.u2Code = ISS_OFPGMFC_INVALID_GROUP;
            Ofc131PktErrMsgSend (&Ofp131ErrorMsg,
                                 pConnPtr, sizeof (Ofp131ErrorMsg.au1Data));
            break;

        case OFC131_QUEUE_INVALID_PORT:
            Ofp131ErrorMsg.u2Type = OFPET_131_QUEUE_OP_FAILED;
            Ofp131ErrorMsg.u2Code = OFPQOFC_BAD_PORT;
            Ofc131PktErrMsgSend (&Ofp131ErrorMsg, pConnPtr,
                                 sizeof (Ofp131ErrorMsg.au1Data));
            break;

        case OFC131_QUEUE_INVALID_QUEUE:
            Ofp131ErrorMsg.u2Type = OFPET_131_QUEUE_OP_FAILED;
            Ofp131ErrorMsg.u2Code = OFPQOFC_BAD_QUEUE;
            Ofc131PktErrMsgSend (&Ofp131ErrorMsg, pConnPtr,
                                 sizeof (Ofp131ErrorMsg.au1Data));
            break;

        case OFC131_METERMOD_U2CMD_VALIDATE_ERR:
            Ofp131ErrorMsg.u2Type = OFPET_131_METER_MOD_FAILED;
            Ofp131ErrorMsg.u2Code = OFPMMFC_BAD_COMMAND;
            Ofc131PktErrMsgSend (&Ofp131ErrorMsg,
                                 pConnPtr, sizeof (Ofp131ErrorMsg.au1Data));
            break;

        case OFC131_METERMOD_U2FLAGS_VALIDATE_ERR:
            Ofp131ErrorMsg.u2Type = OFPET_131_METER_MOD_FAILED;
            Ofp131ErrorMsg.u2Code = OFPMMFC_BAD_FLAGS;
            Ofc131PktErrMsgSend (&Ofp131ErrorMsg,
                                 pConnPtr, sizeof (Ofp131ErrorMsg.au1Data));
            break;
        case OFC131_FLOWMOD_DUPLICATE_FIELD_ERROR:
            Ofp131ErrorMsg.u2Type = OFPET_131_BAD_MATCH;
            Ofp131ErrorMsg.u2Code = OFPBMC_DUP_FIELD;
            Ofc131PktErrMsgSend (&Ofp131ErrorMsg,
                                 pConnPtr, sizeof (Ofp131ErrorMsg.au1Data));
            break;

        case OFC131_FLOWMOD_ACT_VALUE_VALIDATE_ERR:
            Ofp131ErrorMsg.u2Type = OFPET_131_BAD_ACTION;
            Ofp131ErrorMsg.u2Code = OFPBAC_BAD_ARGUMENT;
            Ofc131PktErrMsgSend (&Ofp131ErrorMsg,
                                 pConnPtr, sizeof (Ofp131ErrorMsg.au1Data));
            break;

        case OFC131_FLOWMOD_ACT_SETVALUE_VALIDATE_ERR:
            Ofp131ErrorMsg.u2Type = OFPET_131_BAD_ACTION;
            Ofp131ErrorMsg.u2Code = OFPBAC_BAD_SET_ARGUMENT;
            Ofc131PktErrMsgSend (&Ofp131ErrorMsg,
                                 pConnPtr, sizeof (Ofp131ErrorMsg.au1Data));
            break;

        case OFC131_FLOWMOD_ACT_SETLEN_VALIDATE_ERR:
            Ofp131ErrorMsg.u2Type = OFPET_131_BAD_ACTION;
            Ofp131ErrorMsg.u2Code = OFPBAC_BAD_SET_LEN;
            Ofc131PktErrMsgSend (&Ofp131ErrorMsg,
                                 pConnPtr, sizeof (Ofp131ErrorMsg.au1Data));
            break;

        case OFC131_FLOWMOD_ACT_LEN_VALIDATE_ERR:
            Ofp131ErrorMsg.u2Type = OFPET_131_BAD_ACTION;
            Ofp131ErrorMsg.u2Code = OFPBAC_BAD_LEN;
            Ofc131PktErrMsgSend (&Ofp131ErrorMsg,
                                 pConnPtr, sizeof (Ofp131ErrorMsg.au1Data));
            break;

        default:
            /* Unknown Error Generated */
            break;
    }
    UNUSED_PARAM (pConnPtr);
    UNUSED_PARAM (pBuf);
    OFC131_TRC_FUNC ((OFC131_FN_EXIT, "FUNC:Ofc131PktErrorHandle\n"));
}

/*****************************************************************************
 * Function    :  Ofc131PktOfpSend
 * Description :  This function is for sending OpenFlow packet to Controller 
 * Input       :  pConnPtr - Connection Pointer
 *                pBuf     - CRU Buffer Pointer  
 * Output      :  None 
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE 
 *****************************************************************************/
INT4
Ofc131PktOfpSend (tCRU_BUF_CHAIN_HEADER * pBuf, VOID *pConnPtr)
{
    tOfcFsofcControllerConnEntry *pLocal = NULL;
    UINT4               u4Length = OFC_ZERO;
    INT4                i4Return = OSIX_SUCCESS;

    OFC131_TRC_FUNC ((OFC131_FN_ENTRY, "FUNC:Ofc131PktOfpSend\n"));

    u4Length = CRU_BUF_Get_ChainValidByteCount (pBuf);
    if (CRU_BUF_Copy_FromBufChain (pBuf, gau1DataBuf,
                                   OFC_ZERO, u4Length) == CRU_FAILURE)
    {
        OFC131_TRC_FUNC ((OFC_PKT_TRC, "CRU Copy Failed, " "return FAILURE\n"));
        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
        return OSIX_FAILURE;
    }

    if (pConnPtr != NULL)
    {
        i4Return = Ofc131PktSend (gau1DataBuf, u4Length, pConnPtr);
    }
    else
    {
        pLocal = OfcGetFirstFsofcControllerConnTable ();
        while (pLocal != NULL)
        {
            i4Return = Ofc131PktSend (gau1DataBuf, u4Length, (VOID *) pLocal);
            if (i4Return == OSIX_FAILURE)
            {
                /* try sending for other controllers */
            }
            pLocal = OfcGetNextFsofcControllerConnTable (pLocal);
        }
    }
    CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
    OFC131_TRC_FUNC ((OFC131_FN_EXIT, "FUNC:Ofc131PktOfpSend\n"));
    return i4Return;
}

/*****************************************************************************
 * Function    :  Ofc131PktHelloSend
 * Description :  This function is for sending Hello to Controller  
 * Input       :  pOfp131Hello - Pointer to Hello Packet being Sent
 *                pConnPtr     - Connection Pointer
 *                u4VarLen     - Variable Length             
 * Output      :  None 
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE 
 *****************************************************************************/
INT4
Ofc131PktHelloSend (tOfp131Hello * pOfp131Hello, VOID *pConnPtr, UINT4 u4VarLen)
{
    tOfp131Pkt          Ofp;
    tCRU_BUF_CHAIN_HEADER *pBuf = NULL;
    tOfp131HelloElemHdr *pHelloElemHdr = NULL;
    INT4                i4Return = OSIX_SUCCESS;

    OFC131_TRC_FUNC ((OFC131_FN_ENTRY, "FUNC:Ofc131PktHelloSend\n"));

    MEMSET (&Ofp, OFC_ZERO, sizeof (tOfp131Pkt));

    pHelloElemHdr = &(pOfp131Hello->OfpHelloElemHdr[OFC_ZERO]);
    pHelloElemHdr->u2Type = (UINT2) OFPHET_VERSIONBITMAP;
    pHelloElemHdr->u2Length = (UINT2) ((UINT4) sizeof (tOfp131HelloElemHdr) +
                                       u4VarLen);
    pHelloElemHdr->au4BitMaps[OFC_ZERO] = OSIX_HTONL (OFC131_HLO_MASK);

    Ofp.u1Version = OFP131_VERSION;
    Ofp.u1Type = OFPT_HELLO;
    Ofp.u2Length = (UINT2) ((UINT2) OFP_HEADER_LEN +
                            (UINT2) sizeof (tOfp131HelloElemHdr) +
                            (UINT2) u4VarLen);
    if (pConnPtr != NULL)
    {
        Ofp.u4Xid = ((tOfcFsofcControllerConnEntry *) pConnPtr)->u4RxXid;
        Ofp.u4Xid = OFC_ONE;
    }

    if ((pBuf = CRU_BUF_Allocate_MsgBufChain (OFC131_MAX_PKT_LEN, OFC_ZERO))
        == NULL)
    {
        OFC131_TRC_FUNC ((OFC_PKT_TRC, "pBuf CRU Buf allocation failed\n"));
        return OSIX_FAILURE;
    }
    OFC131_ASSIGN_1_BYTE (pBuf, OFC131_OFP_U1VERSION_OFFSET, Ofp.u1Version);
    OFC131_ASSIGN_1_BYTE (pBuf, OFC131_OFP_U1TYPE_OFFSET, Ofp.u1Type);
    OFC131_ASSIGN_2_BYTE (pBuf, OFC131_OFP_U2LENGTH_OFFSET, Ofp.u2Length);
    OFC131_ASSIGN_4_BYTE (pBuf, OFC131_OFP_U4XID_OFFSET, Ofp.u4Xid);
    OFC131_ASSIGN_2_BYTE (pBuf, (OFC131_HELLO_OFPHELLOELEMHDR_OFFSET +
                                 OFC131_HELLOELEMENT_U2TYPE_OFFSET),
                          pHelloElemHdr->u2Type);
    OFC131_ASSIGN_2_BYTE (pBuf,
                          (OFC131_HELLO_OFPHELLOELEMHDR_OFFSET +
                           OFC131_HELLOELEMENT_U2LENGTH_OFFSET),
                          pHelloElemHdr->u2Length);
    OFC131_ASSIGN_N_BYTE (pBuf,
                          (OFC131_HELLO_OFPHELLOELEMHDR_OFFSET +
                           OFC131_HELLOELEMENT_AU4BITMAPS_OFFSET),
                          pHelloElemHdr->au4BitMaps, u4VarLen);

    i4Return = Ofc131PktOfpSend (pBuf, pConnPtr);
    OFC131_TRC_FUNC ((OFC131_FN_EXIT, "FUNC:Ofc131PktHelloSend\n"));
    return i4Return;
}

/*****************************************************************************
 * Function    :  Ofc131PktErrMsgSend
 * Description :  This function is for sending Error Message to Controller 
 * Input       :  pOfp131ErrMsg - Pointer to Error Message being sent
 *                pConnPtr      - Connection Pointer
 *                u4VarLen      - Variable Length
 * Output      :  None 
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE 
 *****************************************************************************/
INT4
Ofc131PktErrMsgSend (tOfp131ErrMsg * pOfp131ErrMsg, VOID *pConnPtr,
                     INT4 u4VarLen)
{
    tOfp131Pkt          Ofp;
    tCRU_BUF_CHAIN_HEADER *pBuf = NULL;
    INT4                i4Return = OSIX_SUCCESS;

    OFC131_TRC_FUNC ((OFC131_FN_ENTRY, "FUNC:Ofc131PktErrMsgSend\n"));

    MEMSET (&Ofp, OFC_ZERO, sizeof (tOfp131Pkt));

    /*  Structure Elements */
    Ofp.u1Version = OFP131_VERSION;
    Ofp.u1Type = OFP131T_ERROR;
    Ofp.u2Length = (UINT2) ((UINT2) OFP_HEADER_LEN +
                            ((UINT2) sizeof (tOfp131ErrMsg)) +
                            (UINT2) u4VarLen);
    if (pConnPtr != NULL)
    {
        Ofp.u4Xid = ((tOfcFsofcControllerConnEntry *) pConnPtr)->u4RxXid;
    }
    if ((pBuf =
         CRU_BUF_Allocate_MsgBufChain (OFC131_MAX_PKT_LEN, OFC_ZERO)) == NULL)
    {
        OFC131_TRC_FUNC ((OFC_PKT_TRC, "pBuf CRU Buf allocation failed\n"));
        return OSIX_FAILURE;
    }
    OFC131_ASSIGN_1_BYTE (pBuf, OFC131_OFP_U1VERSION_OFFSET, Ofp.u1Version);
    OFC131_ASSIGN_1_BYTE (pBuf, OFC131_OFP_U1TYPE_OFFSET, Ofp.u1Type);
    OFC131_ASSIGN_2_BYTE (pBuf, OFC131_OFP_U2LENGTH_OFFSET, Ofp.u2Length);
    OFC131_ASSIGN_4_BYTE (pBuf, OFC131_OFP_U4XID_OFFSET, Ofp.u4Xid);
    OFC131_ASSIGN_2_BYTE (pBuf, OFC131_ERRMSG_U2TYPE_OFFSET,
                          pOfp131ErrMsg->u2Type);
    OFC131_ASSIGN_2_BYTE (pBuf, OFC131_ERRMSG_U2CODE_OFFSET,
                          pOfp131ErrMsg->u2Code);
    OFC131_ASSIGN_N_BYTE (pBuf, OFC131_ERRMSG_AU1DATA_OFFSET,
                          pOfp131ErrMsg->au1Data, (UINT2) u4VarLen);
    i4Return = Ofc131PktOfpSend (pBuf, pConnPtr);
    OFC131_TRC_FUNC ((OFC131_FN_EXIT, "FUNC:Ofc131PktErrMsgSend\n"));
    return i4Return;
}

/*****************************************************************************
 * Function    :  Ofc131PktEchoReqMsgSend
 * Description :  This function is for sending Echo Request to Controller
 * Input       :  pOfp131EchoReqMsg - Pointer to Echo Request being sent
 *                pConnPtr          - Connection Pointer
 *                u4VarLen          - Variable Length
 * Output      :  None 
 * Returns     :  OSI2_SUCCESS or OSIX_FAILURE 
 *****************************************************************************/
INT4
Ofc131PktEchoReqMsgSend (tOfp131EchoReqMsg * pOfp131EchoReqMsg, VOID *pConnPtr,
                         UINT4 u4VarLen)
{
    tOfp131Pkt          Ofp;
    tCRU_BUF_CHAIN_HEADER *pBuf = NULL;
    tOfcFsofcControllerConnEntry *pLocal = pConnPtr;
    INT4                i4Return = OSIX_SUCCESS;

    OFC131_TRC_FUNC ((OFC131_FN_ENTRY, "FUNC:Ofc131PktEchoReqMsgSend\n"));

    MEMSET (&Ofp, OFC_ZERO, sizeof (tOfp131Pkt));

    /*  Structure Elements */
    Ofp.u1Version = OFP131_VERSION;
    Ofp.u1Type = OFP131T_ECHO_REQUEST;
    Ofp.u2Length = (UINT2) ((UINT2) OFP_HEADER_LEN + (UINT2) u4VarLen);
    if (pLocal != NULL)
    {
        pLocal->u4TxXid++;
        Ofp.u4Xid = pLocal->u4TxXid;
    }
    if ((pBuf =
         CRU_BUF_Allocate_MsgBufChain (OFC131_MAX_PKT_LEN, OFC_ZERO)) == NULL)
    {
        OFC131_TRC_FUNC ((OFC_PKT_TRC, "pBuf CRU Buf allocation failed\n"));
        return OSIX_FAILURE;
    }
    OFC131_ASSIGN_1_BYTE (pBuf, OFC131_OFP_U1VERSION_OFFSET, Ofp.u1Version);
    OFC131_ASSIGN_1_BYTE (pBuf, OFC131_OFP_U1TYPE_OFFSET, Ofp.u1Type);
    OFC131_ASSIGN_2_BYTE (pBuf, OFC131_OFP_U2LENGTH_OFFSET, Ofp.u2Length);
    OFC131_ASSIGN_4_BYTE (pBuf, OFC131_OFP_U4XID_OFFSET, Ofp.u4Xid);
    OFC131_ASSIGN_N_BYTE (pBuf, OFC131_ECHOREQMSG_AU1DATA_OFFSET,
                          pOfp131EchoReqMsg->au1Data, u4VarLen);
    i4Return = Ofc131PktOfpSend (pBuf, pConnPtr);
    OFC131_TRC_FUNC ((OFC131_FN_EXIT, "FUNC:Ofc131PktEchoReqMsgSend\n"));
    return i4Return;
}

/*****************************************************************************
 * Function    :  Ofc131PktEchoRplyMsgSend
 * Description :  This function is for sending Echo Reply to Controller
 * Input       :  pOfp131EchoReqMsg - Pointer to Echo Reply being sent
 *                pConnPtr          - Connection Pointer
 *                u4VarLen          - Variable Length
 * Output      :  None 
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE 
 *****************************************************************************/
INT4
Ofc131PktEchoRplyMsgSend (tOfp131EchoRplyMsg * pOfp131EchoRplyMsg,
                          VOID *pConnPtr, UINT4 u4VarLen)
{
    tOfp131Pkt          Ofp;
    tCRU_BUF_CHAIN_HEADER *pBuf = NULL;
    INT4                i4Return = OSIX_SUCCESS;

    OFC131_TRC_FUNC ((OFC131_FN_ENTRY, "FUNC:Ofc131PktEchoRplyMsgSend\n"));

    MEMSET (&Ofp, OFC_ZERO, sizeof (tOfp131Pkt));

    /*  Structure Elements */
    Ofp.u1Version = OFP131_VERSION;
    Ofp.u1Type = OFP131T_ECHO_REPLY;
    Ofp.u2Length = (UINT2) ((UINT2) OFP_HEADER_LEN + (UINT2) u4VarLen);
    if (pConnPtr != NULL)
    {
        Ofp.u4Xid = ((tOfcFsofcControllerConnEntry *) pConnPtr)->u4RxXid;
    }

    if ((pBuf =
         CRU_BUF_Allocate_MsgBufChain (OFC131_MAX_PKT_LEN, OFC_ZERO)) == NULL)
    {
        OFC131_TRC_FUNC ((OFC_PKT_TRC, "pBuf CRU Buf allocation failed\n"));
        return OSIX_FAILURE;
    }
    OFC131_ASSIGN_1_BYTE (pBuf, OFC131_OFP_U1VERSION_OFFSET, Ofp.u1Version);
    OFC131_ASSIGN_1_BYTE (pBuf, OFC131_OFP_U1TYPE_OFFSET, Ofp.u1Type);
    OFC131_ASSIGN_2_BYTE (pBuf, OFC131_OFP_U2LENGTH_OFFSET, Ofp.u2Length);
    OFC131_ASSIGN_4_BYTE (pBuf, OFC131_OFP_U4XID_OFFSET, Ofp.u4Xid);
    OFC131_ASSIGN_N_BYTE (pBuf, OFC131_ECHORPLYMSG_AU1DATA_OFFSET,
                          pOfp131EchoRplyMsg->au1Data, u4VarLen);

    i4Return = Ofc131PktOfpSend (pBuf, pConnPtr);
    OFC131_TRC_FUNC ((OFC131_FN_EXIT, "FUNC:Ofc131PktEchoRplyMsgSend\n"));
    return i4Return;
}

/*****************************************************************************
 * Function    :  Ofc131PktFeatureRplySend
 * Description :  This function is for sending Packet Features Reply
 *                to Controller
 * Input       :  pOfp131FeatureRply - Pointer to Packet Features Reply
 *                pConnPtr           - Connection Pointer
 *                u4VarLen           - Variable Length
 * Output      :  None 
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE 
 *****************************************************************************/
INT4
Ofc131PktFeatureRplySend (tOfp131FeatureRply * pOfp131FeatureRply,
                          VOID *pConnPtr, UINT4 u4VarLen)
{
    tOfp131Pkt          Ofp;
    tCRU_BUF_CHAIN_HEADER *pBuf = NULL;
    tOfcFsofcControllerConnEntry *pLocal = NULL;
    INT4                i4Return = OSIX_SUCCESS;

    OFC131_TRC_FUNC ((OFC131_FN_ENTRY, "FUNC:Ofc131PktFeatureRplySend\n"));

    MEMSET (&Ofp, OFC_ZERO, sizeof (tOfp131Pkt));
    pLocal = (tOfcFsofcControllerConnEntry *) pConnPtr;

    pOfp131FeatureRply->u4Reserved = OFC_ZERO;
    pOfp131FeatureRply->u1AuxId =
        (UINT1) pLocal->MibObject.i4FsofcControllerConnAuxId;

    /*  Structure Elements */
    Ofp.u1Version = OFP131_VERSION;
    Ofp.u1Type = OFP131T_FEATURES_REPLY;
    Ofp.u2Length = (UINT2) OFP_HEADER_LEN + (UINT2) sizeof (tOfp131FeatureRply);
    Ofp.u4Xid = pLocal->u4RxXid;

    if ((pBuf =
         CRU_BUF_Allocate_MsgBufChain (OFC131_MAX_PKT_LEN, OFC_ZERO)) == NULL)
    {
        OFC131_TRC_FUNC ((OFC_PKT_TRC, "pBuf CRU Buf allocation failed\n"));
        return OSIX_FAILURE;
    }
    OFC131_ASSIGN_1_BYTE (pBuf, OFC131_OFP_U1VERSION_OFFSET, Ofp.u1Version);
    OFC131_ASSIGN_1_BYTE (pBuf, OFC131_OFP_U1TYPE_OFFSET, Ofp.u1Type);
    OFC131_ASSIGN_2_BYTE (pBuf, OFC131_OFP_U2LENGTH_OFFSET, Ofp.u2Length);
    OFC131_ASSIGN_4_BYTE (pBuf, OFC131_OFP_U4XID_OFFSET, Ofp.u4Xid);
    OFC131_ASSIGN_N_BYTE (pBuf, OFC131_FEATURERPLY_U8DATAPATHIID_OFFSET,
                          &pOfp131FeatureRply->u8DatapathId, OFC_EIGHT);
    OFC131_ASSIGN_4_BYTE (pBuf, OFC131_FEATURERPLY_U4NOOFBUF_OFFSET,
                          pOfp131FeatureRply->u4NoOfBuf);
    OFC131_ASSIGN_1_BYTE (pBuf, OFC131_FEATURERPLY_U1NOOFTABLES_OFFSET,
                          pOfp131FeatureRply->u1NoOfTables);
    OFC131_ASSIGN_1_BYTE (pBuf, OFC131_FEATURERPLY_U1AUXID_OFFSET,
                          pOfp131FeatureRply->u1AuxId);
    OFC131_ASSIGN_N_BYTE (pBuf, OFC131_FEATURERPLY_AU1PAD_OFFSET,
                          pOfp131FeatureRply->au1Pad, OFC_TWO);
    OFC131_ASSIGN_4_BYTE (pBuf, OFC131_FEATURERPLY_U4CAPABILITIES_OFFSET,
                          pOfp131FeatureRply->u4Capabilities);
    OFC131_ASSIGN_4_BYTE (pBuf, OFC131_FEATURERPLY_U4RESERVED_OFFSET,
                          pOfp131FeatureRply->u4Reserved);

    i4Return = Ofc131PktOfpSend (pBuf, pConnPtr);
    UNUSED_PARAM (u4VarLen);
    OFC131_TRC_FUNC ((OFC131_FN_EXIT, "FUNC:Ofc131PktFeatureRplySend\n"));
    return i4Return;
}

/*****************************************************************************
 * Function    :  Ofc131PktConfigRplySend
 * Description :  This function is for sending Config Reply to Controller
 * Input       :  pOfp131ConfigRply  - Pointer to Config Reply Message
 *                pConnPtr           - Connection Pointer
 *                u4VarLen           - Variable Length
 * Output      :  None 
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE 
 *****************************************************************************/
INT4
Ofc131PktConfigRplySend (tOfp131ConfigRply * pOfp131ConfigRply, VOID *pConnPtr,
                         UINT4 u4VarLen)
{
    tOfp131Pkt          Ofp;
    tCRU_BUF_CHAIN_HEADER *pBuf = NULL;
    INT4                i4Return = OSIX_SUCCESS;

    OFC131_TRC_FUNC ((OFC131_FN_ENTRY, "FUNC:Ofc131PktConfigRplySend\n"));

    MEMSET (&Ofp, OFC_ZERO, sizeof (tOfp131Pkt));

    /*  Structure Elements */
    Ofp.u1Version = OFP131_VERSION;
    Ofp.u1Type = OFP131T_GET_CONFIG_REPLY;
    Ofp.u2Length = (UINT2) OFP_HEADER_LEN + (UINT2) sizeof (tOfp131ConfigRply);
    if (pConnPtr != NULL)
    {
        Ofp.u4Xid = ((tOfcFsofcControllerConnEntry *) pConnPtr)->u4RxXid;
    }
    if ((pBuf = CRU_BUF_Allocate_MsgBufChain (OFC131_MAX_PKT_LEN, OFC_ZERO))
        == NULL)
    {
        OFC131_TRC_FUNC ((OFC_PKT_TRC, "pBuf CRU Buf allocation failed\n"));
        return OSIX_FAILURE;
    }
    OFC131_ASSIGN_1_BYTE (pBuf, OFC131_OFP_U1VERSION_OFFSET, Ofp.u1Version);
    OFC131_ASSIGN_1_BYTE (pBuf, OFC131_OFP_U1TYPE_OFFSET, Ofp.u1Type);
    OFC131_ASSIGN_2_BYTE (pBuf, OFC131_OFP_U2LENGTH_OFFSET, Ofp.u2Length);
    OFC131_ASSIGN_4_BYTE (pBuf, OFC131_OFP_U4XID_OFFSET, Ofp.u4Xid);
    OFC131_ASSIGN_2_BYTE (pBuf, OFC131_CONFIGRPLY_U2FLAGS_OFFSET,
                          pOfp131ConfigRply->u2Flags);
    OFC131_ASSIGN_2_BYTE (pBuf, OFC131_CONFIGRPLY_U2MISSSENDLEN_OFFSET,
                          pOfp131ConfigRply->u2MissSendLen);
    UNUSED_PARAM (u4VarLen);
    i4Return = Ofc131PktOfpSend (pBuf, pConnPtr);
    OFC131_TRC_FUNC ((OFC131_FN_EXIT, "FUNC:Ofc131PktConfigRplySend\n"));
    return i4Return;
}

/*****************************************************************************
 * Function    :  Ofc131PktPktInSend
 * Description :  This function is for sending Packet-In to Controller
 * Input       :  pOfp131PktIn - Pointer to Packet-In Message
 *                pConnPtr     - Connection Pointer
 *                u4VarLen     - Variable Length
 * Output      :  None 
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE 
 *****************************************************************************/
INT4
Ofc131PktPktInSend (tOfp131PktIn * pOfp131PktIn, VOID *pConnPtr, UINT4 u4VarLen)
{
    tOfp131Pkt          Ofp;
    tCRU_BUF_CHAIN_HEADER *pBuf = NULL;
    INT4                i4Return = OSIX_SUCCESS;

    OFC131_TRC_FUNC ((OFC131_FN_ENTRY, "FUNC:Ofc131PktPktInSend\n"));

    MEMSET (&Ofp, OFC_ZERO, sizeof (tOfp131Pkt));

    /*  Structure Elements */
    Ofp.u1Version = OFP131_VERSION;
    Ofp.u1Type = OFP131T_PACKET_IN;
    Ofp.u2Length = (UINT2) (OFP_HEADER_LEN + OFC_SIXTEEN + (UINT2) u4VarLen);
    if (pConnPtr != NULL)
    {
        Ofp.u4Xid = ((tOfcFsofcControllerConnEntry *) pConnPtr)->u4RxXid;
    }
    if ((pBuf = CRU_BUF_Allocate_MsgBufChain (OFC131_MAX_PKT_LEN, OFC_ZERO))
        == NULL)
    {
        OFC131_TRC_FUNC ((OFC_PKT_TRC, "pBuf CRU Buf allocation failed\n"));
        return OSIX_FAILURE;
    }
    OFC131_ASSIGN_1_BYTE (pBuf, OFC131_OFP_U1VERSION_OFFSET, Ofp.u1Version);
    OFC131_ASSIGN_1_BYTE (pBuf, OFC131_OFP_U1TYPE_OFFSET, Ofp.u1Type);
    OFC131_ASSIGN_2_BYTE (pBuf, OFC131_OFP_U2LENGTH_OFFSET, Ofp.u2Length);
    OFC131_ASSIGN_4_BYTE (pBuf, OFC131_OFP_U4XID_OFFSET, Ofp.u4Xid);
    OFC131_ASSIGN_4_BYTE (pBuf, OFC131_PKTIN_U4BUFID_OFFSET,
                          pOfp131PktIn->u4BufId);
    OFC131_ASSIGN_2_BYTE (pBuf, OFC131_PKTIN_U2TOTLEN_OFFSET,
                          pOfp131PktIn->u2TotLen);
    OFC131_ASSIGN_1_BYTE (pBuf, OFC131_PKTIN_U1REASON_OFFSET,
                          pOfp131PktIn->u1Reason);
    OFC131_ASSIGN_1_BYTE (pBuf, OFC131_PKTIN_U1TABLEID_OFFSET,
                          pOfp131PktIn->u1TableId);
    OFC131_ASSIGN_8_BYTE (pBuf, OFC131_PKTIN_U4COOKIE_OFFSET,
                          pOfp131PktIn->u8Cookie);
    OFC131_ASSIGN_N_BYTE (pBuf, OFC131_PKTIN_OFPMATCH_OFFSET,
                          &(pOfp131PktIn->OfpMatch), u4VarLen);
    i4Return = Ofc131PktOfpSend (pBuf, pConnPtr);
    OFC131_TRC_FUNC ((OFC131_FN_EXIT, "FUNC:Ofc131PktPktInSend\n"));
    return i4Return;
}

/*****************************************************************************
 * Function    :  Ofc131PktFlowRemSend
 * Description :  This function is for sending Flow Removal Message
 *                to Controller
 * Input       :  pOfp131FlowRem - Pointer to Flow Removal Message
 *                pConnPtr       - Connection Pointer
 *                u4VarLen       - Variable Length
 * Output      :  None 
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE 
 *****************************************************************************/
INT4
Ofc131PktFlowRemSend (tOfp131FlowRem * pOfp131FlowRem, VOID *pConnPtr,
                      UINT4 u4VarLen)
{
    tOfp131Pkt          Ofp;
    tCRU_BUF_CHAIN_HEADER *pBuf = NULL;
    INT4                i4Return = OSIX_SUCCESS;
    UINT1               u1PadBytes = OFC_ZERO;
    UINT1               au1Pad[OFC_SEVEN];

    OFC131_TRC_FUNC ((OFC131_FN_ENTRY, "FUNC:Ofc131PktFlowRemSend\n"));

    MEMSET (&Ofp, OFC_ZERO, sizeof (tOfp131Pkt));
    MEMSET (au1Pad, OFC_ZERO, OFC_SEVEN);
    /*  Structure Elements */
    Ofp.u1Version = OFP131_VERSION;
    Ofp.u1Type = OFP131T_FLOW_REMOVED;
    Ofp.u2Length =
        (UINT2) ((UINT2) OFP_HEADER_LEN + (UINT2) sizeof (tOfp131FlowRem) +
                 OSIX_HTONS (pOfp131FlowRem->OfpMatch.u2Length) -
                 (UINT2) sizeof (tOfp131Match));

    if (pConnPtr != NULL)
    {
        Ofp.u4Xid = ((tOfcFsofcControllerConnEntry *) pConnPtr)->u4RxXid;
    }

    if ((pBuf = CRU_BUF_Allocate_MsgBufChain (OFC131_MAX_PKT_LEN, OFC_ZERO))
        == NULL)
    {
        OFC131_TRC_FUNC ((OFC_PKT_TRC, "pBuf CRU Buf allocation failed\n"));
        return OSIX_FAILURE;
    }
    OFC131_ASSIGN_1_BYTE (pBuf, OFC131_OFP_U1VERSION_OFFSET, Ofp.u1Version);
    OFC131_ASSIGN_1_BYTE (pBuf, OFC131_OFP_U1TYPE_OFFSET, Ofp.u1Type);
    OFC131_ASSIGN_2_BYTE (pBuf, OFC131_OFP_U2LENGTH_OFFSET, Ofp.u2Length);
    OFC131_ASSIGN_4_BYTE (pBuf, OFC131_OFP_U4XID_OFFSET, Ofp.u4Xid);
    OFC131_ASSIGN_8_BYTE (pBuf, OFC131_FLOWREM_U8COOKIE_OFFSET,
                          pOfp131FlowRem->u8Cookie);
    OFC131_ASSIGN_2_BYTE (pBuf, OFC131_FLOWREM_U2PRIORITY_OFFSET,
                          pOfp131FlowRem->u2Priority);
    OFC131_ASSIGN_1_BYTE (pBuf, OFC131_FLOWREM_U1REASON_OFFSET,
                          pOfp131FlowRem->u1Reason);
    OFC131_ASSIGN_1_BYTE (pBuf, OFC131_FLOWREM_U1TABLEID_OFFSET,
                          pOfp131FlowRem->u1TableId);
    OFC131_ASSIGN_4_BYTE (pBuf, OFC131_FLOWREM_U4SEC_OFFSET,
                          pOfp131FlowRem->u4Sec);
    OFC131_ASSIGN_4_BYTE (pBuf, OFC131_FLOWREM_U4NSEC_OFFSET,
                          pOfp131FlowRem->u4NSec);
    OFC131_ASSIGN_2_BYTE (pBuf, OFC131_FLOWREM_U2IDLETIMEOUT_OFFSET,
                          pOfp131FlowRem->u2IdleTimeOut);
    OFC131_ASSIGN_2_BYTE (pBuf, OFC131_FLOWREM_U2HARDTIMEOUT_OFFSET,
                          pOfp131FlowRem->u2HardTimeOut);
    OFC131_ASSIGN_8_BYTE (pBuf, OFC131_FLOWREM_U8PKTCOUNT_OFFSET,
                          pOfp131FlowRem->u8PktCount);
    OFC131_ASSIGN_8_BYTE (pBuf, OFC131_FLOWREM_U8BYTECOUNT_OFFSET,
                          pOfp131FlowRem->u8ByteCount);
    OFC131_ASSIGN_2_BYTE (pBuf, OFC131_FLOWREM_OFPMATCH_OFFSET +
                          OFC131_MATCH_U2TYPE_OFFSET,
                          OSIX_HTONS (pOfp131FlowRem->OfpMatch.u2Type));
    OFC131_ASSIGN_2_BYTE (pBuf, OFC131_FLOWREM_OFPMATCH_OFFSET +
                          OFC131_MATCH_U2LENGTH_OFFSET,
                          OSIX_HTONS (pOfp131FlowRem->OfpMatch.u2Length));
    OFC131_ASSIGN_N_BYTE (pBuf, OFC131_FLOWREM_OFPMATCH_OFFSET +
                          OFC131_MATCH_AU1OXMFIELDS_OFFSET,
                          pOfp131FlowRem->OfpMatch.au1OxmFields,
                          (UINT2) (OSIX_HTONS
                                   (pOfp131FlowRem->OfpMatch.u2Length) -
                                   OFC_FOUR));

    /* A.2.3.1 Check if padding is required in ofp_match to make it multiple of 8 */
    u1PadBytes =
        (UINT1) (OFC_EIGHT -
                 (UINT1) (OSIX_HTONS (pOfp131FlowRem->OfpMatch.u2Length) %
                          OFC_EIGHT));
    if (u1PadBytes)
    {
        OFC131_ASSIGN_N_BYTE (pBuf, Ofp.u2Length, au1Pad, u1PadBytes);
        /* Need to increase the packet size also */
        Ofp.u2Length = (UINT2) (Ofp.u2Length + (UINT2) u1PadBytes);
        OFC131_ASSIGN_2_BYTE (pBuf, OFC131_OFP_U2LENGTH_OFFSET, Ofp.u2Length);
    }

    i4Return = Ofc131PktOfpSend (pBuf, pConnPtr);
    UNUSED_PARAM (u4VarLen);
    OFC131_TRC_FUNC ((OFC131_FN_EXIT, "FUNC:Ofc131PktFlowRemSend\n"));
    return i4Return;
}

/****************************************************************************
 * Function    :  Ofc131PktPortStatusSend
 * Description :  This function is for sending Port Status Message
 *                to Controller
 * Input       :  pOfp131PortStatus - Pointer to Port Status Message
 *                pConnPtr          - Connection Pointer
 *                u4VarLen          - Variable Length
 * Output      :  None 
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE 
 ****************************************************************************/
INT4
Ofc131PktPortStatusSend (tOfp131PortStatus * pOfp131PortStatus, VOID *pConnPtr,
                         UINT4 u4VarLen)
{
    tOfp131Pkt          Ofp;
    tCRU_BUF_CHAIN_HEADER *pBuf = NULL;
    INT4                i4Return = OSIX_SUCCESS;

    OFC131_TRC_FUNC ((OFC131_FN_ENTRY, "FUNC:Ofc131PktPortStatusSend\n"));

    MEMSET (&Ofp, OFC_ZERO, sizeof (tOfp131Pkt));

    Ofp.u1Version = OFP131_VERSION;
    Ofp.u1Type = OFP131T_PORT_STATUS;
    Ofp.u2Length = OFP_HEADER_LEN + sizeof (tOfp131PortStatus);

    if (pConnPtr != NULL)
    {
        Ofp.u4Xid = ((tOfcFsofcControllerConnEntry *) pConnPtr)->u4RxXid;
    }
    if ((pBuf = CRU_BUF_Allocate_MsgBufChain (OFC131_MAX_PKT_LEN, OFC_ZERO))
        == NULL)
    {
        OFC131_TRC_FUNC ((OFC_PKT_TRC, "pBuf CRU Buf allocation failed\n"));
        return OSIX_FAILURE;
    }
    OFC131_ASSIGN_1_BYTE (pBuf, OFC131_OFP_U1VERSION_OFFSET, Ofp.u1Version);
    OFC131_ASSIGN_1_BYTE (pBuf, OFC131_OFP_U1TYPE_OFFSET, Ofp.u1Type);
    OFC131_ASSIGN_2_BYTE (pBuf, OFC131_OFP_U2LENGTH_OFFSET, Ofp.u2Length);
    OFC131_ASSIGN_4_BYTE (pBuf, OFC131_OFP_U4XID_OFFSET, Ofp.u4Xid);
    OFC131_ASSIGN_1_BYTE (pBuf, OFC131_PORTSTATUS_U1REASON_OFFSET,
                          pOfp131PortStatus->u1Reason);
    OFC131_ASSIGN_N_BYTE (pBuf, OFC131_PORTSTATUS_AU1PAD_OFFSET,
                          pOfp131PortStatus->au1Pad, OFC_SEVEN);
    OFC131_ASSIGN_4_BYTE (pBuf,
                          (OFC131_PORTSTATUS_PORTDESC_OFFSET +
                           OFC131_PORTDESC_U4PORTNO_OFFSET),
                          pOfp131PortStatus->PortDesc.u4PortNo);
    OFC131_ASSIGN_N_BYTE (pBuf,
                          (OFC131_PORTSTATUS_PORTDESC_OFFSET +
                           OFC131_PORTDESC_AU1PAD_OFFSET),
                          pOfp131PortStatus->PortDesc.au1Pad, OFC_FOUR);
    OFC131_ASSIGN_N_BYTE (pBuf,
                          (OFC131_PORTSTATUS_PORTDESC_OFFSET +
                           OFC131_PORTDESC_AU1HWADDR_OFFSET),
                          pOfp131PortStatus->PortDesc.au1HwAddr, OFC_SIX);
    OFC131_ASSIGN_N_BYTE (pBuf,
                          (OFC131_PORTSTATUS_PORTDESC_OFFSET +
                           OFC131_PORTDESC_AU1PAD2_OFFSET),
                          pOfp131PortStatus->PortDesc.au1Pad2, OFC_TWO);
    OFC131_ASSIGN_N_BYTE (pBuf,
                          (OFC131_PORTSTATUS_PORTDESC_OFFSET +
                           OFC131_PORTDESC_AU1NAME_OFFSET),
                          pOfp131PortStatus->PortDesc.au1Name, OFC_SIXTEEN);
    OFC131_ASSIGN_4_BYTE (pBuf,
                          (OFC131_PORTSTATUS_PORTDESC_OFFSET +
                           OFC131_PORTDESC_U4CONFIG_OFFSET),
                          pOfp131PortStatus->PortDesc.u4Config);
    OFC131_ASSIGN_4_BYTE (pBuf,
                          (OFC131_PORTSTATUS_PORTDESC_OFFSET +
                           OFC131_PORTDESC_U4STATE_OFFSET),
                          pOfp131PortStatus->PortDesc.u4State);
    OFC131_ASSIGN_4_BYTE (pBuf,
                          (OFC131_PORTSTATUS_PORTDESC_OFFSET +
                           OFC131_PORTDESC_U4CURR_OFFSET),
                          pOfp131PortStatus->PortDesc.u4Curr);
    OFC131_ASSIGN_4_BYTE (pBuf,
                          (OFC131_PORTSTATUS_PORTDESC_OFFSET +
                           OFC131_PORTDESC_U4ADVERTISED_OFFSET),
                          pOfp131PortStatus->PortDesc.u4Advertised);
    OFC131_ASSIGN_4_BYTE (pBuf,
                          (OFC131_PORTSTATUS_PORTDESC_OFFSET +
                           OFC131_PORTDESC_U4SUPPORTED_OFFSET),
                          pOfp131PortStatus->PortDesc.u4Supported);
    OFC131_ASSIGN_4_BYTE (pBuf,
                          (OFC131_PORTSTATUS_PORTDESC_OFFSET +
                           OFC131_PORTDESC_U4PEER_OFFSET),
                          pOfp131PortStatus->PortDesc.u4Peer);
    OFC131_ASSIGN_4_BYTE (pBuf,
                          (OFC131_PORTSTATUS_PORTDESC_OFFSET +
                           OFC131_PORTDESC_U4CURRSPEED_OFFSET),
                          pOfp131PortStatus->PortDesc.u4CurrSpeed);
    OFC131_ASSIGN_4_BYTE (pBuf,
                          (OFC131_PORTSTATUS_PORTDESC_OFFSET +
                           OFC131_PORTDESC_U4MAXSPEED_OFFSET),
                          pOfp131PortStatus->PortDesc.u4MaxSpeed);
    UNUSED_PARAM (u4VarLen);
    i4Return = Ofc131PktOfpSend (pBuf, pConnPtr);
    OFC131_TRC_FUNC ((OFC131_FN_EXIT, "FUNC:Ofc131PktPortStatusSend\n"));
    return i4Return;
}

/*****************************************************************************
 * Function    :  Ofc131PktMultiPartRplySend
 * Description :  This function is for sending Multipart Reply Message
 *                to Controller
 * Input       :  pOfp131MultiPartRply - Pointer to Multipart Reply Message
 *                pConnPtr             - Connection Pointer
 *                u4VarLen             - Variable Length
 * Output      :  None 
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE 
 *****************************************************************************/
INT4
Ofc131PktMultiPartRplySend (tOfp131MultiPartRply * pOfp131MultiPartRply,
                            VOID *pConnPtr, UINT4 u4VarLen)
{
    tOfp131Pkt          Ofp;
    tCRU_BUF_CHAIN_HEADER *pBuf = NULL;
    tOfcFsofcControllerConnEntry *pLocal = NULL;
    INT4                i4Return = OSIX_SUCCESS;

    OFC131_TRC_FUNC ((OFC131_FN_ENTRY, "FUNC:Ofc131PktMultiPartRplySend\n"));

    MEMSET (&Ofp, OFC_ZERO, sizeof (tOfp131Pkt));
    pLocal = (tOfcFsofcControllerConnEntry *) pConnPtr;

    /*  Structure Elements */
    Ofp.u1Version = OFP131_VERSION;
    Ofp.u1Type = OFP131T_MULTIPART_REPLY;
    Ofp.u2Length = (UINT2) (OFP_HEADER_LEN + OFC_EIGHT + (UINT2) u4VarLen);
    Ofp.u4Xid = pLocal->u4RxXid;

    if ((pBuf =
         CRU_BUF_Allocate_MsgBufChain (OFC131_MAX_PKT_LEN, OFC_ZERO)) == NULL)
    {
        OFC131_TRC_FUNC ((OFC_PKT_TRC, "pBuf CRU Buf allocation failed\n"));
        return OSIX_FAILURE;
    }
    OFC131_ASSIGN_1_BYTE (pBuf, OFC131_OFP_U1VERSION_OFFSET, Ofp.u1Version);
    OFC131_ASSIGN_1_BYTE (pBuf, OFC131_OFP_U1TYPE_OFFSET, Ofp.u1Type);
    OFC131_ASSIGN_2_BYTE (pBuf, OFC131_OFP_U2LENGTH_OFFSET, Ofp.u2Length);
    OFC131_ASSIGN_4_BYTE (pBuf, OFC131_OFP_U4XID_OFFSET, Ofp.u4Xid);
    OFC131_ASSIGN_2_BYTE (pBuf, OFC131_MULTIPARTRPLY_U2TYPE_OFFSET,
                          pOfp131MultiPartRply->u2Type);
    OFC131_ASSIGN_2_BYTE (pBuf, OFC131_MULTIPARTRPLY_U2FLAGS_OFFSET,
                          pOfp131MultiPartRply->u2Flags);
    OFC131_ASSIGN_N_BYTE (pBuf, OFC131_MULTIPARTRPLY_AU1PAD_OFFSET,
                          pOfp131MultiPartRply->au1Pad, OFC_FOUR);
    OFC131_ASSIGN_N_BYTE (pBuf, OFC131_MULTIPARTRPLY_AU1BODY_OFFSET,
                          pOfp131MultiPartRply->au1Body, u4VarLen);

    i4Return = Ofc131PktOfpSend (pBuf, pConnPtr);
    OFC131_TRC_FUNC ((OFC131_FN_EXIT, "FUNC:Ofc131PktMultiPartRplySend\n"));
    return i4Return;
}

/*****************************************************************************
 * Function    :  Ofc131PktBarrierRplySend
 * Description :  This function is for sending Barrier Reply Message
 *                to Controller
 * Input       :  pOfp131BarrierRply - Pointer to Barrier Reply Message
 *                pConnPtr           - Connection Pointer
 *                u4VarLen           - Variable Length
 * Output      :  None 
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE 
 *****************************************************************************/
INT4
Ofc131PktBarrierRplySend (tOfp131BarrierRply * pOfp131BarrierRply,
                          VOID *pConnPtr, UINT4 u4VarLen)
{

    tOfp131Pkt          Ofp;
    tCRU_BUF_CHAIN_HEADER *pBuf = NULL;
    tOfcFsofcControllerConnEntry *pLocal = NULL;
    INT4                i4Return = OSIX_SUCCESS;

    OFC131_TRC_FUNC ((OFC131_FN_ENTRY, "FUNC:Ofc131PktBarrierRplySend\n"));

    MEMSET (&Ofp, OFC_ZERO, sizeof (tOfp131Pkt));

    pLocal = (tOfcFsofcControllerConnEntry *) pConnPtr;
    pLocal->u4TxDataLength = OFC_ZERO;

    /*  Structure Elements */
    Ofp.u1Version = OFP131_VERSION;
    Ofp.u1Type = OFP131T_BARRIER_REPLY;
    Ofp.u2Length = (UINT2) (OFP_HEADER_LEN + (UINT2) u4VarLen);
    Ofp.u4Xid = pLocal->u4RxXid;

    if ((pBuf =
         CRU_BUF_Allocate_MsgBufChain (OFC131_MAX_PKT_LEN, OFC_ZERO)) == NULL)
    {
        OFC131_TRC_FUNC ((OFC_PKT_TRC, "pBuf CRU Buf allocation failed\n"));
        return OSIX_FAILURE;
    }

    OFC131_ASSIGN_1_BYTE (pBuf, OFC131_OFP_U1VERSION_OFFSET, Ofp.u1Version);
    OFC131_ASSIGN_1_BYTE (pBuf, OFC131_OFP_U1TYPE_OFFSET, Ofp.u1Type);
    OFC131_ASSIGN_2_BYTE (pBuf, OFC131_OFP_U2LENGTH_OFFSET, Ofp.u2Length);
    OFC131_ASSIGN_4_BYTE (pBuf, OFC131_OFP_U4XID_OFFSET, Ofp.u4Xid);
    OFC131_ASSIGN_N_BYTE (pBuf, OFC131_BARRIERREQ_AU1DUMMY_OFFSET,
                          pOfp131BarrierRply, u4VarLen);

    i4Return = Ofc131PktOfpSend (pBuf, pConnPtr);
    OFC131_TRC_FUNC ((OFC131_FN_EXIT, "FUNC:Ofc131PktBarrierRplySend\n"));
    return i4Return;
}

/****************************************************************************
 * Function    :  Ofc131PktQueueRplySend
 * Description :  This function is for sending Queue Reply Message
 *                to Controller
 * Input       :  pOfp131QueueRply - Pointer to Queue Reply Message
 *                pConnPtr         - Connection Pointer
 *                u4VarLen         - Variable Length
 * Output      :  None 
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE 
 ****************************************************************************/
INT4
Ofc131PktQueueRplySend (tOfp131QueueRply * pOfp131QueueRply, VOID *pConnPtr,
                        UINT4 u4VarLen)
{
    tOfp131Pkt          Ofp;
    tCRU_BUF_CHAIN_HEADER *pBuf = NULL;

    OFC131_TRC_FUNC ((OFC131_FN_ENTRY, "FUNC:Ofc131PktQueueRplySend\n"));

    MEMSET (&Ofp, OFC_ZERO, sizeof (tOfp131Pkt));

    /*  Structure Elements */
    Ofp.u1Version = OFP131_VERSION;
    Ofp.u1Type = OFP131T_QUEUE_GET_CONFIG_REPLY;
    Ofp.u2Length = (UINT2) (OFP_HEADER_LEN + (UINT2) u4VarLen);

    if (pConnPtr != NULL)
    {
        Ofp.u4Xid = ((tOfcFsofcControllerConnEntry *) pConnPtr)->u4RxXid;
    }

    if ((pBuf =
         CRU_BUF_Allocate_MsgBufChain (OFC131_MAX_PKT_LEN, OFC_ZERO)) == NULL)
    {
        OFC131_TRC_FUNC ((OFC_PKT_TRC, "pBuf CRU Buf allocation failed\n"));
        return OSIX_FAILURE;
    }
    OFC131_ASSIGN_1_BYTE (pBuf, OFC131_OFP_U1VERSION_OFFSET, Ofp.u1Version);
    OFC131_ASSIGN_1_BYTE (pBuf, OFC131_OFP_U1TYPE_OFFSET, Ofp.u1Type);
    OFC131_ASSIGN_2_BYTE (pBuf, OFC131_OFP_U2LENGTH_OFFSET, Ofp.u2Length);
    OFC131_ASSIGN_4_BYTE (pBuf, OFC131_OFP_U4XID_OFFSET, Ofp.u4Xid);
    /* Variable Length starts from the first queue itself */
    OFC131_ASSIGN_N_BYTE (pBuf, OFC131_QUEUERPLY_OFFSET,
                          pOfp131QueueRply, u4VarLen);

    OFC131_TRC_FUNC ((OFC131_FN_EXIT, "FUNC:Ofc131PktQueueRplySend\n"));
    return (Ofc131PktOfpSend (pBuf, pConnPtr));
}

/*****************************************************************************
 * Function    :  Ofc131PktRoleRplySend
 * Description :  This function is for sending Role Reply Message
 *                to Controller
 * Input       :  pOfp131RoleRply - Pointer to Role Reply Message
 *                pConnPtr        - Connection Pointer
 *                u4VarLen        - Variable Length
 * Output      :  None 
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE 
 *****************************************************************************/
INT4
Ofc131PktRoleRplySend (tOfp131RoleRply * pOfp131RoleRply, VOID *pConnPtr,
                       UINT4 u4VarLen)
{
    tOfp131Pkt          Ofp;
    tCRU_BUF_CHAIN_HEADER *pBuf = NULL;
    INT4                i4Return = OSIX_SUCCESS;

    OFC131_TRC_FUNC ((OFC131_FN_ENTRY, "FUNC:Ofc131PktRoleRplySend\n"));

    MEMSET (&Ofp, OFC_ZERO, sizeof (tOfp131Pkt));

    /*  Structure Elements */
    Ofp.u1Version = OFP131_VERSION;
    Ofp.u1Type = OFP131T_ROLE_REPLY;
    Ofp.u2Length = OFP_HEADER_LEN + sizeof (tOfp131RoleRply);

    if (pConnPtr != NULL)
    {
        Ofp.u4Xid = ((tOfcFsofcControllerConnEntry *) pConnPtr)->u4RxXid;
    }
    if ((pBuf =
         CRU_BUF_Allocate_MsgBufChain (OFC131_MAX_PKT_LEN, OFC_ZERO)) == NULL)
    {
        OFC131_TRC_FUNC ((OFC_PKT_TRC, "pBuf CRU Buf allocation failed\n"));
        return OSIX_FAILURE;
    }
    OFC131_ASSIGN_1_BYTE (pBuf, OFC131_OFP_U1VERSION_OFFSET, Ofp.u1Version);
    OFC131_ASSIGN_1_BYTE (pBuf, OFC131_OFP_U1TYPE_OFFSET, Ofp.u1Type);
    OFC131_ASSIGN_2_BYTE (pBuf, OFC131_OFP_U2LENGTH_OFFSET, Ofp.u2Length);
    OFC131_ASSIGN_4_BYTE (pBuf, OFC131_OFP_U4XID_OFFSET, Ofp.u4Xid);
    OFC131_ASSIGN_4_BYTE (pBuf, OFC131_ROLERPLY_U4ROLE_OFFSET,
                          pOfp131RoleRply->u4Role);
    OFC131_ASSIGN_N_BYTE (pBuf, OFC131_ROLERPLY_AU1PAD_OFFSET,
                          pOfp131RoleRply->au1Pad, OFC_FOUR);
    OFC131_ASSIGN_8_BYTE (pBuf, OFC131_ROLERPLY_U8GENID_OFFSET,
                          pOfp131RoleRply->u8GenId);
    i4Return = Ofc131PktOfpSend (pBuf, pConnPtr);
    UNUSED_PARAM (u4VarLen);
    OFC131_TRC_FUNC ((OFC131_FN_EXIT, "FUNC:Ofc131PktRoleRplySend\n"));
    return i4Return;
}

/*****************************************************************************
 * Function    :  Ofc131PktAsyncConfigRplySend
 * Description :  This function is for sending Async Config Reply Message
 *                to Controller
 * Input       :  pOfp131AsyncConfigRply - Pointer to AsyncConfig Reply
 *                pConnPtr        - Connection Pointer
 *                u4VarLen        - Variable Length
 * Output      :  None 
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE 
 *****************************************************************************/
INT4
Ofc131PktAsyncConfigRplySend (tOfp131AsyncConfigRply * pOfp131AsyncConfigRply,
                              VOID *pConnPtr, UINT4 u4VarLen)
{
    tOfp131Pkt          Ofp;
    tCRU_BUF_CHAIN_HEADER *pBuf = NULL;
    INT4                i4Return = OSIX_SUCCESS;

    OFC131_TRC_FUNC ((OFC131_FN_ENTRY, "FUNC:Ofc131PktAsyncConfigRplySend\n"));

    MEMSET (&Ofp, OFC_ZERO, sizeof (tOfp131Pkt));

    /*  Structure Elements */
    Ofp.u1Version = OFP131_VERSION;
    Ofp.u1Type = OFP131T_GET_ASYNC_CONFIG_REPLY;
    Ofp.u2Length = OFP_HEADER_LEN + sizeof (tOfp131AsyncConfigRply);

    if (pConnPtr != NULL)
    {
        Ofp.u4Xid = ((tOfcFsofcControllerConnEntry *) pConnPtr)->u4RxXid;
    }
    if ((pBuf =
         CRU_BUF_Allocate_MsgBufChain (OFC131_MAX_PKT_LEN, OFC_ZERO)) == NULL)
    {
        OFC131_TRC_FUNC ((OFC_PKT_TRC, "pBuf CRU Buf allocation failed\n"));
        return OSIX_FAILURE;
    }
    OFC131_ASSIGN_1_BYTE (pBuf, OFC131_OFP_U1VERSION_OFFSET, Ofp.u1Version);
    OFC131_ASSIGN_1_BYTE (pBuf, OFC131_OFP_U1TYPE_OFFSET, Ofp.u1Type);
    OFC131_ASSIGN_2_BYTE (pBuf, OFC131_OFP_U2LENGTH_OFFSET, Ofp.u2Length);
    OFC131_ASSIGN_4_BYTE (pBuf, OFC131_OFP_U4XID_OFFSET, Ofp.u4Xid);
    OFC131_ASSIGN_N_BYTE (pBuf, OFC131_ASYNCCONFIGRPLY_AU4PKTINMASK_OFFSET,
                          pOfp131AsyncConfigRply->au4PktInMask, OFC_EIGHT);
    OFC131_ASSIGN_N_BYTE (pBuf, OFC131_ASYNCCONFIGRPLY_AU4PORTSTATUSMASK_OFFSET,
                          pOfp131AsyncConfigRply->au4PortStatusMask, OFC_EIGHT);
    OFC131_ASSIGN_N_BYTE (pBuf,
                          OFC131_ASYNCCONFIGRPLY_AU4FLOWREMOVEDMASK_OFFSET,
                          pOfp131AsyncConfigRply->au4FlowRemovedMask,
                          OFC_EIGHT);
    i4Return = Ofc131PktOfpSend (pBuf, pConnPtr);
    UNUSED_PARAM (u4VarLen);
    OFC131_TRC_FUNC ((OFC131_FN_EXIT, "FUNC:Ofc131PktAsyncConfigRplySend\n"));
    return i4Return;
}

/*****************************************************************************
 * Function    :  Ofc131PktSend
 * Description :  This function is for sending packet to lower (socket) layer
 * Input       :  pDataBuf - Pointer to Data Buffer
 *                pConnPtr - Connection Pointer
 *                u4Length - Length in Bytes of content in Data Buffer
 * Output      :  None 
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE 
 *****************************************************************************/
INT4
Ofc131PktSend (UINT1 *pDataBuf, UINT4 u4Length, VOID *pConnPtr)
{
    OFC131_TRC_FUNC ((OFC131_FN_ENTRY, "FUNC:Ofc131PktSend\n"));
#ifdef SSL_WANTED
    if (((tOfcFsofcControllerConnEntry *) pConnPtr)->pssl != NULL)
    {
        if (SslArWriteImmediate
            (((tOfcFsofcControllerConnEntry *) pConnPtr)->pssl, pDataBuf,
             &u4Length) == SSL_FAILURE)
        {
            OFC131_TRC_FUNC ((OFC_PKT_TRC, "Failed on socket = %d\n",
                              ((tOfcFsofcControllerConnEntry *)
                               pConnPtr)->i4SocketDesc));
            return OSIX_FAILURE;
        }
    }
    else
    {
#endif
        if (send (((tOfcFsofcControllerConnEntry *) pConnPtr)->i4SocketDesc,
                  pDataBuf, (INT4) u4Length, OFC_ZERO) == OFC_MINUS_ONE)
        {
            OFC131_TRC_FUNC ((OFC_PKT_TRC, "Failed on socket = %d\n",
                              ((tOfcFsofcControllerConnEntry *)
                               pConnPtr)->i4SocketDesc));
            return OSIX_FAILURE;
        }
#ifdef SSL_WANTED
    }
#endif
    OFC131_TRC_FUNC ((OFC131_FN_EXIT, "FUNC:Ofc131PktSend\n"));
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function    :  Ofc131PktOxmParseMatchTlv
 * Description :  This function is used to parse Oxm Match TLV structure
 * Input       :  i1TableId       - Table Identifier
 *                pOfpMatch       - Pointer to Match Fields in OXM format
 * Output      :  pMatchKeyList   - Pointer to Match Fields SLL
 *                pu1FlowMatchCnt - Pointer containing number of Match Fields 
 *                pu4Error        - Pointer to the error
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE 
 *****************************************************************************/
INT4
Ofc131PktOxmParseMatchTlv (INT1 i1TableId, tOfp131Match * pOfpMatch,
                           tOfcSll * pKeyMatchList, UINT1 *pu1FlowMatchCnt,
                           UINT1 *pu1IsFlowWild, UINT4 *pu4Error)
{
    tOxmTlv            *pOxmTlv = NULL;
    tOfcWcFlowMatch    *pWcFlowMatch = NULL;
    UINT4               u4MatchTlvLen = OFC_ZERO;
    UINT1               u1OxmLen = OFC_ZERO;
    UINT1               u1HasOxmLen = OFC_ZERO;
    UINT1               u1FlowMatchCount = OFC_ZERO;
    UINT4               u4InPort = OFC_ZERO;
    UINT1               au1Mask[OFC_SIXTEEN];
    UINT2               u2VlanVID = OFC_ZERO;

    OFC131_TRC_FUNC ((OFC131_FN_ENTRY, "FUNC:Ofc131PktOxmParseMatchTlv\n"));

    /* The match type is just validated */
    if (pOfpMatch->u2Type != OSIX_HTONS (OFPMT_OXM))
    {
        OFC131_TRC_FUNC ((OFC_PKT_TRC, "Match Type is invalid\n"));
        return OSIX_FAILURE;
    }

    /* Initialize SLL */
    TMO_SLL_Init (pKeyMatchList);

    pOxmTlv = (tOxmTlv *) ((VOID *) (((UINT1 *) pOfpMatch) + OFC_FOUR));
    u4MatchTlvLen = (UINT4) (OSIX_NTOHS (pOfpMatch->u2Length) - OFC_FOUR);

    MEMSET (au1Mask, OFC_ALL_ONE, OFC_SIXTEEN);
    while ((u4MatchTlvLen > OFC_ZERO))
    {
        /* Read the TLV's in the match structure */
        if (pOxmTlv->u2Class != OSIX_HTONS (OFPXMC_OPENFLOW_BASIC))
        {
            OFC131_TRC_FUNC ((OFC_PKT_TRC, "Oxm Tlv Class is invalid\n"));
            return OFC_FAILURE;
        }

        pWcFlowMatch = OfcMemAllocForFlowMatch (i1TableId);
        if (pWcFlowMatch == NULL)
        {
            OFC131_TRC_FUNC ((OFC_PKT_TRC,
                              "MemAllocMemBlk for FlowMatchKey Failed\n"));
            return OSIX_FAILURE;
        }
        MEMSET (pWcFlowMatch, OFC_ZERO, sizeof (tOfcWcFlowMatch));

        pWcFlowMatch->u1HasMask = pOxmTlv->u1Field & HM_BIT_MASK;
        pWcFlowMatch->u2Type = pOxmTlv->u1Field >> OFC_ONE;
        u1OxmLen = pOxmTlv->u1Len;

        if (pWcFlowMatch->u1HasMask)
        {
            u1HasOxmLen = u1OxmLen / OFC_TWO;
            MEMCPY ((pWcFlowMatch->FldValue.au1Fld), pOxmTlv->au1Value,
                    u1HasOxmLen);
            MEMCPY ((pWcFlowMatch->FldMask.au1Fld),
                    &(pOxmTlv->au1Value[u1HasOxmLen]), u1HasOxmLen);
            /* set this flow as wild carded flow */
            *pu1IsFlowWild = OFC_ONE;
        }
        else
        {
            MEMCPY ((pWcFlowMatch->FldValue.au1Fld), pOxmTlv->au1Value,
                    u1OxmLen);
            /* update the length of each */
            pWcFlowMatch->u1Length = u1OxmLen;
            MEMCPY ((pWcFlowMatch->FldMask.au1Fld), au1Mask, sizeof (au1Mask));
        }

        if (pWcFlowMatch->u2Type == OFCXMT_OFB_IN_PORT)
        {
            MEMCPY ((UINT1 *) &u4InPort, pWcFlowMatch->FldValue.au1Fld,
                    OFC_FOUR);
            u4InPort = OSIX_NTOHL (u4InPort);

            if ((u4InPort == OFPP_NORMAL) || (u4InPort == OFPP_FLOOD))
            {
                *pu4Error = OFC131_FLOWMOD_BAD_VALUE_ERR;
                /* Release the mem-pool */
                OfcMemFreeForFlowMatch (i1TableId, (UINT1 *) pWcFlowMatch);
                return OFC_FAILURE;
            }
        }

        if (pWcFlowMatch->u2Type == OFCXMT_OFB_VLAN_VID)
        {
            MEMCPY (&u2VlanVID, &(pWcFlowMatch->FldValue.au1Fld), OFC_TWO);
            u2VlanVID = OSIX_NTOHS (u2VlanVID);

            /* Take 12 bits of Vlan ID from the flow mod message if Vlan tag is present
             * in the message */

            if (u2VlanVID & OFPVID_PRESENT)
            {
                u2VlanVID &= OFC_PK_VLAN_VID_MASK;
            }
            u2VlanVID = OSIX_HTONS (u2VlanVID);
            MEMCPY (&(pWcFlowMatch->FldValue.au1Fld), &u2VlanVID, OFC_TWO);
        }

        u4MatchTlvLen = u4MatchTlvLen - ((sizeof (tOxmTlv)) + u1OxmLen);
        pOxmTlv = (tOxmTlv *) ((VOID *) (((UINT1 *) pOxmTlv) +
                                         (sizeof (tOxmTlv)) + u1OxmLen));
        u1FlowMatchCount++;
        /* Add it to the list */
        TMO_SLL_Add (pKeyMatchList, &pWcFlowMatch->Node);
        KW_FALSEPOSITIVE_FIX (pWcFlowMatch);
    }
    *pu1FlowMatchCnt = u1FlowMatchCount;

    /* check with exact matching is disabled */
    if (gu4FlowType == OFC_FLOW_TYPE_WILD)
    {
        /* set this flow as wild carded flow */
        *pu1IsFlowWild = OFC_ONE;
    }

    OFC131_TRC_FUNC ((OFC131_FN_EXIT, "FUNC:Ofc131PktOxmParseMatchTlv\n"));
    return OFC_SUCCESS;
}

/*****************************************************************************
 *  Function Name   : Ofc131PktCheckIsInstrTypeGreater
 *  Description     : This function checks whether the Second Instr. type has
 *                    higher priority than the First Instr. type
 *  Input           : pOfcInstr1   - Pointer to First Instruction
 *                    pOfcInstr2   - Pointer to Second Instruction
 *  Output          : OSIX_SUCCESS - if the Instr2 > Instr1.
 *                    OSIX_FAILURE - otherwise.
 *  Returns         : OSIX_SUCCESS or OSIX_FAILURE
 *****************************************************************************/
PRIVATE INT4
Ofc131PktCheckIsInstrTypeGreater (tOfcInstr * pOfcInstr1,
                                  tOfcInstr * pOfcInstr2)
{
    OFC131_TRC_FUNC ((OFC131_FN_ENTRY,
                      "FUNC:Ofc131PktCheckIsInstrTypeGreater\n"));

    /*
     * The priority of instructions is as follows:
     *     1. Meter
     *     2. Apply Actions
     *     3. Clear Actions
     *     4. Wrire Actions
     *     5. Write Metadate
     *     6. Goto Table
     */
    if ((pOfcInstr2->u2Type == OFCIT_METER) &&
        (pOfcInstr1->u2Type != OFCIT_METER))
    {
        return OSIX_SUCCESS;
    }
    else if ((pOfcInstr2->u2Type == OFCIT_APPLY_ACTIONS) &&
             ((pOfcInstr1->u2Type == OFCIT_CLEAR_ACTIONS) ||
              (pOfcInstr1->u2Type == OFCIT_WRITE_METADATA) ||
              (pOfcInstr1->u2Type == OFCIT_GOTO_TABLE) ||
              (pOfcInstr1->u2Type == OFCIT_WRITE_ACTIONS)))
    {
        return OSIX_SUCCESS;
    }
    else if ((pOfcInstr2->u2Type == OFCIT_CLEAR_ACTIONS) &&
             ((pOfcInstr1->u2Type == OFCIT_WRITE_ACTIONS) ||
              (pOfcInstr1->u2Type == OFCIT_WRITE_METADATA) ||
              (pOfcInstr1->u2Type == OFCIT_GOTO_TABLE)))
    {
        return OSIX_SUCCESS;
    }
    else if ((pOfcInstr2->u2Type == OFCIT_WRITE_ACTIONS) &&
             ((pOfcInstr1->u2Type == OFCIT_WRITE_METADATA) ||
              (pOfcInstr1->u2Type == OFCIT_GOTO_TABLE)))
    {
        return OSIX_SUCCESS;
    }
    else if ((pOfcInstr2->u2Type == OFCIT_WRITE_METADATA) &&
             (pOfcInstr1->u2Type == OFCIT_GOTO_TABLE))
    {
        return OSIX_SUCCESS;
    }
    else if (pOfcInstr2->u2Type == OFCIT_GOTO_TABLE)
    {
        /* Goto will be executed last */
        return OSIX_FAILURE;
    }
    OFC131_TRC_FUNC ((OFC131_FN_EXIT,
                      "FUNC:Ofc131PktCheckIsInstrTypeGreater\n"));
    return OSIX_FAILURE;
}

/*****************************************************************************
 *  Function Name   : Ofc131PktInsertInstrInList
 *  Description     : This function inserts the given Instruction into the
 *                    specified Instruction List in correct position so that 
                      the list is in sorted order.
 *  Input           : pInstrList - Pointer to the List 
 *                    pOfcInstr  - Pointer to the Instruction to be inserted
 *  Output          : None
 *  Returns         : None
 *****************************************************************************/
PRIVATE VOID
Ofc131PktInsertInstrInList (tOfcSll * pInstrList, tOfcInstr * pOfcInstr)
{
    tOfcInstr          *pTmpOfcInstr = NULL;
    tOfcInstr          *pPrevOfcInstr = NULL;

    OFC131_TRC_FUNC ((OFC131_FN_ENTRY, "FUNC:Ofc131PktInsertInstrInList\n"));

    TMO_SLL_Scan (pInstrList, pTmpOfcInstr, tOfcInstr *)
    {
        if (pTmpOfcInstr != NULL)
        {
            /* 
             * Instruction has to be inserted if it has highest priority
             * Ofc131PktCheckIsInstrTypeGreater will return OSIX_SUCCESS
             */
            if (Ofc131PktCheckIsInstrTypeGreater (pTmpOfcInstr, pOfcInstr) ==
                OSIX_SUCCESS)
            {
                break;
            }
            else
            {
                pPrevOfcInstr = pTmpOfcInstr;
            }
        }
    }
    if (pPrevOfcInstr == NULL)
    {
        /* Node to be inserted in first position */
        TMO_SLL_Insert (pInstrList, NULL, &(pOfcInstr->Node));
    }
    else if (pTmpOfcInstr == NULL)
    {
        /* Node to be inserted in last position */
        TMO_SLL_Insert (pInstrList, &(pPrevOfcInstr->Node), &(pOfcInstr->Node));
    }
    else
    {
        /* Node to be inserted in middle */
        TMO_SLL_Insert_In_Middle (pInstrList,
                                  &(pPrevOfcInstr->Node),
                                  &(pOfcInstr->Node), &(pTmpOfcInstr->Node));
    }
    OFC131_TRC_FUNC ((OFC131_FN_EXIT, "FUNC:Ofc131PktInsertInstrInList\n"));
}

/*****************************************************************************
 * Function    :  Ofc131PktParseFlowAction
 * Description :  This function is used to parse Action structure
 * Input       :  u1TableId        - Table Identifier
 *                pActionHdr       - Pointer to Actions Header
 *                i4ActionLen      - Actions Length
 * Output      :  pOfcInstrActions - Pointer to Instruction List
 *                pu4Outport       - Poiner to Output Port
 *                pu4OutGroup      - Poiner to Out Group
 *                pu4Error         - Pointer to Error
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE 
 *****************************************************************************/
PRIVATE INT4
Ofc131PktParseFlowAction (UINT1 u1TableId, tOfp131ActionHdr * pActionHdr,
                          tOfcInstrActions * pOfcInstrActions, INT4 i4ActionLen,
                          UINT4 *pu4Outport, UINT4 *pu4OutGroup,
                          UINT4 *pu4Error, VOID *pConnPtr)
{
    tOfcActs           *pOfcActs = NULL;
    tOfcActs           *pOfcNextActs = NULL;
    INT4                i4RetVal = OFC_SUCCESS;

    OFC131_TRC_FUNC ((OFC131_FN_ENTRY, "FUNC:Ofc131PktParseFlowAction\n"));

    TMO_SLL_Init (&(pOfcInstrActions->ActList));
    i4RetVal =
        Ofc131PktAddActionToList (u1TableId, &(pOfcInstrActions->ActList),
                                  pActionHdr, i4ActionLen, pu4Outport,
                                  pu4OutGroup, pu4Error, pConnPtr);
    if (i4RetVal != OSIX_SUCCESS)
    {
        TMO_DYN_SLL_Scan (&pOfcInstrActions->ActList, pOfcActs, pOfcNextActs,
                          tOfcActs *)
        {
            TMO_SLL_Delete (&pOfcInstrActions->ActList,
                            (tTMO_SLL_NODE *) pOfcActs);
            OfcMemFreeForAction (u1TableId, (UINT1 *) pOfcActs);
            if (NULL == pOfcNextActs)
            {
                break;
            }
        }
    }
    OFC131_TRC_FUNC ((OFC131_FN_EXIT, "FUNC:Ofc131PktParseFlowAction\n"));
    return i4RetVal;
}

/*****************************************************************************
 * Function    :  Ofc131PktParseInstr
 * Description :  This function is used to parse Instruction structure
 * Input       :  pConnPtr   - Connection Pointer
 *                u2MatchLen - Length of the Match Fields
 *                pFlowEntry - Pointer to the Flow Entry
 *                u4InstrLen - Length of the Instructions
 * Output      :  pu4Error   - Pointer to the Error
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE 
 *****************************************************************************/
INT4
Ofc131PktParseInstr (VOID *pConnPtr, UINT2 u2MatchLen,
                     tOfcFsofcFlowEntry * pFlowEntry, UINT4 u4InstrLen,
                     UINT4 *pu4Error)
{
    tOfp131ErrMsg       Ofp131ErrorMsg;
    tOfcInstr          *pOfcInstr = NULL;
    tOfp131Instruction *pInstrHdr = NULL;
    tOfcInstrGotoTable *pOfcInstrGotoTable = NULL;
    tOfcInstrWrMetadata *pOfcInstrWrMetadata = NULL;
    tOfcInstrMeter     *pOfcInstrMeter = NULL;
    tOfcInstrExpr      *pOfcInstrExpr = NULL;
    tOfp131ActionHdr   *pActionHdr = NULL;
    INT4                i4ActionLen = OFC_ZERO;
    INT4                i4RetVal = OSIX_SUCCESS;
    UINT2               u2Len = OFC_ZERO;
    UINT4               u4OrigInstrLen = u4InstrLen;
    UINT1               u1TableId = OFC_ZERO;

    tOfcFsofcControllerConnEntry *pLocal = pConnPtr;
    tOfcFsofcCfgEntry  *pOfcFsofcCfgEntry = NULL;

    OFC131_TRC_FUNC ((OFC131_FN_ENTRY, "FUNC:Ofc131PktParseInstr\n"));

    MEMSET (&Ofp131ErrorMsg, OFC_ZERO, sizeof (tOfp131ErrMsg));

    pInstrHdr = (tOfp131Instruction *) ((VOID *) (pLocal->pRcvPkt +
                                                  OFC131_FLOWMOD_OFPMATCH_OFFSET
                                                  + u2MatchLen));

    /* Initialise Instruction list */
    TMO_SLL_Init (&(pFlowEntry->InstrList));

    pOfcFsofcCfgEntry =
        OfcGetFsofcCfgEntry (pLocal->MibObject.u4FsofcContextId);
    if (pOfcFsofcCfgEntry == NULL)
    {
        OFC131_TRC_FUNC ((OFC_PKT_TRC, "\r\nOfc131PktParseInstr: "
                          "Get CFG Entry Failed\n"));
        return OSIX_FAILURE;
    }

    u1TableId = (UINT1) pFlowEntry->u4TableIndex;

    /* Parse Instructions */
    while (u4InstrLen > OFC_ZERO)
    {
        pOfcInstr = OfcMemAllocForInstruction (u1TableId);
        if (pOfcInstr == NULL)
        {
            OFC131_TRC_FUNC ((OFC_PKT_TRC,
                              "MemAlloc for Instruction Failed\n"));
            return OSIX_FAILURE;
        }

        MEMSET (pOfcInstr, OFC_ZERO, sizeof (tOfcInstr));
        TMO_SLL_Init_Node (&(pOfcInstr->Node));
        pOfcInstr->u2Type = OSIX_NTOHS (pInstrHdr->u2Type);
        u2Len = OSIX_NTOHS (pInstrHdr->u2Len);

        switch (pOfcInstr->u2Type)
        {
            case OFCIT_GOTO_TABLE:
                pOfcInstrGotoTable = (tOfcInstrGotoTable *)
                    ((VOID *) (((UINT1 *) pInstrHdr) + OFC_FOUR));

#ifndef DPA_WANTED                /* DPA takes care of validation anyway */
                /*
                 * Goto table Id should be > Flow table id and
                 * less then max value
                 */
                if ((pOfcInstrGotoTable->u1TableId <=
                     (UINT1) pFlowEntry->u4TableIndex) ||
                    (pOfcInstrGotoTable->u1TableId >=
                     (UINT1) pOfcFsofcCfgEntry->u2NumOfTables))
                {
                    Ofp131ErrorMsg.u2Type = OFPET_131_BAD_INSTRUCTION;
                    Ofp131ErrorMsg.u2Code = OFPFMFC_131_BAD_TABLE_ID;
                    i4RetVal = Ofc131PktErrMsgSend (&Ofp131ErrorMsg,
                                                    pConnPtr,
                                                    sizeof
                                                    (Ofp131ErrorMsg.au1Data));
                    OFC131_TRC_FUNC ((OFC_PKT_TRC,
                                      "Invalid Goto Table index " "%d\n",
                                      pOfcInstrGotoTable->u1TableId));
                    OfcMemFreeForInstruction (u1TableId, (UINT1 *) pOfcInstr);
                    return OSIX_FAILURE;
                }
#endif /* DPA_WANTED */

                pOfcInstr->Instr.GotoTable.u1TableId =
                    pOfcInstrGotoTable->u1TableId;
                Ofc131PktInsertInstrInList (&pFlowEntry->InstrList, pOfcInstr);
                break;

            case OFCIT_WRITE_METADATA:
                pOfcInstrWrMetadata = (tOfcInstrWrMetadata *)
                    ((VOID *) (((UINT1 *) pInstrHdr) + OFC_EIGHT));
                MEMCPY (&pOfcInstr->Instr.Metadata.u8Metadata,
                        &pOfcInstrWrMetadata->u8Metadata, OFC_EIGHT);
                MEMCPY (&pOfcInstr->Instr.Metadata.u8MetadataMask,
                        &pOfcInstrWrMetadata->u8MetadataMask, OFC_EIGHT);
                Ofc131PktInsertInstrInList (&pFlowEntry->InstrList, pOfcInstr);
                break;

            case OFCIT_METER:
                pOfcInstrMeter = (tOfcInstrMeter *)
                    ((VOID *) (((UINT1 *) pInstrHdr) + OFC_FOUR));
                pOfcInstr->Instr.Meter.u4MeterId =
                    OSIX_NTOHL (pOfcInstrMeter->u4MeterId);
                Ofc131PktInsertInstrInList (&pFlowEntry->InstrList, pOfcInstr);
                break;

            case OFCIT_EXPERIMENTER:
                pOfcInstrExpr = (tOfcInstrExpr *)
                    ((VOID *) (((UINT1 *) pInstrHdr) + OFC_FOUR));
                pOfcInstr->Instr.Experimenter.u4Experimenter =
                    OSIX_NTOHL (pOfcInstrExpr->u4Experimenter);
                Ofc131PktInsertInstrInList (&pFlowEntry->InstrList, pOfcInstr);
                break;

            case OFCIT_WRITE_ACTIONS:
            case OFCIT_APPLY_ACTIONS:
                /* Parsing Actions */
                i4ActionLen = u2Len - OFC_EIGHT;
                pActionHdr = (tOfp131ActionHdr *)
                    ((VOID *) (pLocal->pRcvPkt +
                               OFC131_FLOWMOD_OFPMATCH_OFFSET +
                               u2MatchLen +
                               (u4OrigInstrLen - u4InstrLen) + OFC_EIGHT));

                /* i4OrigInstrLen - i4InstrLen gives instrlen read so far */
                i4RetVal = Ofc131PktParseFlowAction (u1TableId, pActionHdr,
                                                     (tOfcInstrActions *) &
                                                     pOfcInstr->Instr,
                                                     i4ActionLen,
                                                     &pFlowEntry->u4OutPort,
                                                     &pFlowEntry->u4OutGrp,
                                                     pu4Error, pConnPtr);

                if (i4RetVal != OSIX_SUCCESS)
                {
                    OFC131_TRC_FUNC ((OFC_PKT_TRC,
                                      "Invalid Write/Aplly Actions"));
                    OfcMemFreeForInstruction (u1TableId, (UINT1 *) pOfcInstr);
                    return OSIX_FAILURE;
                }
                Ofc131PktInsertInstrInList (&pFlowEntry->InstrList, pOfcInstr);
                break;

            case OFCIT_CLEAR_ACTIONS:
                /* just add the node */
                Ofc131PktInsertInstrInList (&pFlowEntry->InstrList, pOfcInstr);
                break;

            default:
                OFC131_TRC_FUNC ((OFC_PKT_TRC,
                                  "Invalid Actions, Entered Default Case"));
                OfcMemFreeForInstruction (u1TableId, (UINT1 *) pOfcInstr);
                Ofp131ErrorMsg.u2Type = OFPET_131_BAD_INSTRUCTION;
                Ofp131ErrorMsg.u2Code = OFPBIC_UNKNOWN_INST;
                i4RetVal = Ofc131PktErrMsgSend (&Ofp131ErrorMsg, pConnPtr,
                                                sizeof
                                                (Ofp131ErrorMsg.au1Data));
                return OFC_FAILURE;
        }
        u4InstrLen -= u2Len;
        pInstrHdr = (tOfp131Instruction *) ((VOID *) (((UINT1 *) pInstrHdr) +
                                                      u2Len));
        KW_FALSEPOSITIVE_FIX (pOfcInstr);
    }
    OFC131_TRC_FUNC ((OFC131_FN_EXIT, "FUNC:Ofc131PktParseInstr\n"));
    return i4RetVal;
}

/******************************************************************************
 * Function Name  :  Ofc131PktMatchStructToTlv
 * Description    :  This routine forms the TLV flow match from the array of
 *                   flow match fields.
 * Input          :  pOfcWcFlowMatch - Pointer to Flow Match Struct array.
 *                   u1NumMatch      - Number of Flow Matches in Struct array. 
 * Output         :  pMatchTlv       - Pointer to TLV Flow Match
 * Returns        :  None
 *****************************************************************************/
VOID
Ofc131PktMatchStructToTlv (tOfcSll * pKeyMatchList,
                           UINT1 u1NumMatch, tOfp131Match * pMatchTlv)
{
    tOfcWcFlowMatch    *pOfcWcFlowMatch = NULL;
    UINT2               u2TlvLen = OFC_ZERO;
    UINT1               u1FldLen = OFC_ZERO;
    UINT1              *pu1TempVal = OFC_ZERO;
    tOxmTlv            *pOxmTlv = NULL;

    OFC131_TRC_FUNC ((OFC131_FN_ENTRY, "FUNC:Ofc131PktMatchStructToTlv\n"));

    pMatchTlv->u2Type = OSIX_HTONS (OFPMT_OXM);
    pOxmTlv = (tOxmTlv *) ((VOID *) (((UINT1 *) (pMatchTlv)) + OFC_FOUR));

    if (u1NumMatch == OFC_ZERO)
    {
        /* Flow with zero match fields */
        u2TlvLen = OFC_FOUR;    /* for padding */
    }

    TMO_SLL_Scan (pKeyMatchList, pOfcWcFlowMatch, tOfcWcFlowMatch *)
    {
        pOxmTlv->u2Class = OSIX_HTONS (OFPXMC_OPENFLOW_BASIC);
        /* Fill in the type */
        pOxmTlv->u1Field = (UINT1) pOfcWcFlowMatch->u2Type;
        /* Length of this field */
        switch (pOxmTlv->u1Field)
        {
            case OFCXMT_OFB_VLAN_PCP:    /* VLAN Priority */
            case OFCXMT_OFB_IP_DSCP:    /* IP DSCP (6 bits in ToS field) */
            case OFCXMT_OFB_IP_ECN:    /* IP ECN (2 bits in ToS field) */
            case OFCXMT_OFB_IP_PROTO:    /* IPv4 Protocol */
            case OFCXMT_OFB_ICMPV4_TYPE:    /* ICMP Type */
            case OFCXMT_OFB_ICMPV4_CODE:    /* ICMP Code */
            case OFCXMT_OFB_ICMPV6_TYPE:    /* ICMPv6 Type */
            case OFCXMT_OFB_ICMPV6_CODE:    /* ICMPv6 Code */
            case OFCXMT_OFB_MPLS_TC:    /* MPLS TC */
            case OFCXMT_OFB_MPLS_BOS:    /* MPLS BoS bit */
                u1FldLen = OFC_ONE;
                break;

            case OFCXMT_OFB_ETH_TYPE:    /* Ethernet Type */
            case OFCXMT_OFB_VLAN_VID:    /* VLAN ID */
            case OFCXMT_OFB_TCP_SRC:    /* TCP Source Port */
            case OFCXMT_OFB_TCP_DST:    /* TCP Destination Port */
            case OFCXMT_OFB_UDP_SRC:    /* UDP Source Port */
            case OFCXMT_OFB_UDP_DST:    /* UDP Destination Port */
            case OFCXMT_OFB_SCTP_SRC:    /* SCTP Source Port */
            case OFCXMT_OFB_SCTP_DST:    /* SCTP Destination Port */
            case OFCXMT_OFB_ARP_OP:    /* ARP OpCode */
            case OFCXMT_OFB_IPV6_EXTHDR:    /* IPv6 Ext Header pseudo-field */
                u1FldLen = OFC_TWO;
                break;

            case OFCXMT_OFB_IPV6_FLABEL:    /* IPv6 Flow Label */
            case OFCXMT_OFB_MPLS_LABEL:    /* MPLS Label */
            case OFCXMT_OFB_PBB_ISID:    /* PBB I-SID */
                u1FldLen = OFC_THREE;
                break;

            case OFCXMT_OFB_IN_PORT:    /* Switch input port */
            case OFCXMT_OFB_IN_PHY_PORT:    /* Switch physical input port */
            case OFCXMT_OFB_IPV4_SRC:    /* IPv4 Source Address */
            case OFCXMT_OFB_IPV4_DST:    /* IPv4 Destination Address */
            case OFCXMT_OFB_ARP_SPA:    /* ARP Source IPv4 Address */
            case OFCXMT_OFB_ARP_TPA:    /* ARP Target IPv4 Address */
                u1FldLen = OFC_FOUR;
                break;

            case OFCXMT_OFB_ETH_DST:    /* Ethernet Destination Address */
            case OFCXMT_OFB_ETH_SRC:    /* Ethernet Source Address */
            case OFCXMT_OFB_ARP_SHA:    /* ARP Source Hardware Address */
            case OFCXMT_OFB_ARP_THA:    /* ARP Destination Hardware Address */
            case OFCXMT_OFB_IPV6_ND_SLL:    /* Source Link Layer for ND */
            case OFCXMT_OFB_IPV6_ND_TLL:    /* Target Link Layer for ND */
                u1FldLen = OFC_SIX;
                break;

            case OFCXMT_OFB_METADATA:    /* Metadata passed between tables */
            case OFCXMT_OFB_TUNNEL_ID:    /* Logical Port Metadata */
                u1FldLen = OFC_EIGHT;
                break;

            case OFCXMT_OFB_IPV6_SRC:    /* IPv6 Source Address */
            case OFCXMT_OFB_IPV6_DST:    /* IPv6 Destination Address */
            case OFCXMT_OFB_IPV6_ND_TARGET:    /* Target Address for ND */
                u1FldLen = OFC_SIXTEEN;
                break;

            default:
                /* we should not be getting this */
                break;
        }
        pOxmTlv->u1Field = (UINT1) (pOxmTlv->u1Field << OFC_ONE);
        /* set the hash bit, in case present */
        if (pOfcWcFlowMatch->u1HasMask)
        {
            pOxmTlv->u1Field = pOxmTlv->u1Field | HM_BIT_MASK;
        }

        pOxmTlv->u1Len = u1FldLen;
        /* Copy the match field value */
        MEMCPY (pOxmTlv->au1Value, (pOfcWcFlowMatch->FldValue.au1Fld),
                u1FldLen);

        /* Copy the mask, in case present */
        if (pOfcWcFlowMatch->u1HasMask)
        {
            pu1TempVal = NULL;
            pu1TempVal = pOxmTlv->au1Value;
            pu1TempVal += u1FldLen;
            MEMCPY (pu1TempVal, pOfcWcFlowMatch->FldMask.au1Fld, u1FldLen);
            /* length doubles, on presence of mask */
            pOxmTlv->u1Len = (UINT1) ((UINT1) OFC_TWO * pOxmTlv->u1Len);
        }

        u2TlvLen = (UINT2) (u2TlvLen + (pOxmTlv->u1Len + OFC_FOUR));
        /* Move to Next FlowMatch */
        pOxmTlv = (tOxmTlv *) ((VOID *) (((UINT1 *) pOxmTlv) + pOxmTlv->u1Len +
                                         OFC_FOUR));
    }
    pMatchTlv->u2Length = OSIX_HTONS ((u2TlvLen + OFC_FOUR));
    OFC131_TRC_FUNC ((OFC131_FN_EXIT, "FUNC:Ofc131PktMatchStructToTlv\n"));
}
