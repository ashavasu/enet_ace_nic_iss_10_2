/******************************************************************************
 * Copyright (C) 2013 Aricent Inc . All Rights Reserved
 *
 * $Id: ofchybrid.c,v 1.4 2014/08/14 11:11:23 siva Exp $
 *
 * Description: This file contains routines for openflow hybrid processing 
 ******************************************************************************/

#include "rstp.h"
#include "ofcapi.h"
#include "ofchybrid.h"

/* 
 * The below array contains the Hybrid Port information indexed
 * by the context Id.
 */
static tOfcHybridPortDb gaHybridPortDb[MAX_OFC_FSOFCCFGTABLE];
extern INT4         CfaGddEthWrite (UINT1 *pu1Buf, UINT2 u2IfIndex,
                                    UINT4 u4Size);

/******************************************************************************
 * Function    : OfcHybridPortCheck
 * Description : This routine verifies logical port for Hybrid is created by
 *               user.
 * Input       : u4ContextId  - Context for which the tunnel is mapped.
 *               i4IntfType    - Interface Type (L2/L3)
 *               i4IntfIdx     - Interface Index
 * Output      : NONE 
 * Returns     : OFC_SUCCESS or OFC_FAILURE
 ******************************************************************************/
UINT4
OfcHybridPortCheck (UINT4 u4ContextId, INT4 i4IntfType, INT4 i4IntfIdx)
{
    tTnlIfEntry         TnlIfEntry;
    UINT4               u4HybridPort = OFC_ZERO;
    UINT4               u4RetVal = OFC_FAILURE;
    UINT1               au1IfNameTnl[] = "tunnel";
    UINT1               au1IfNameLag[] = "po";
    UINT1               au1Alias[ISS_ENT_PHY_ALIAS_LEN];

    OFC_TRC_FUNC ((OFC_FN_ENTRY, "OfcHybridPortCheck\n"));

    MEMSET (&TnlIfEntry, OFC_ZERO, sizeof (tTnlIfEntry));
    if (i4IntfType == OFC_HYBRID_PORT_L3)
    {
        /* Check if the port is created by admin */
        SPRINTF ((CHR1 *) au1Alias, "%s%u", au1IfNameTnl, i4IntfIdx);
        u4RetVal = CfaGetInterfaceIndexFromName (au1Alias, &u4HybridPort);
        if (u4RetVal == OSIX_FAILURE)
        {
            /* No tunnel created with the given index */
            OFC_TRC_FUNC ((OFC_HYBRID_TRC, "No tunnel mapped to %d\n",
                           i4IntfIdx));
            return OFC_FAILURE;
        }

        if (CfaGetTnlEntryFromIfIndex (u4HybridPort, &TnlIfEntry) ==
            CFA_FAILURE)
        {
            /* No tunnel created with the given index */
            OFC_TRC_FUNC ((OFC_HYBRID_TRC, "No tunnel with index %d\n",
                           u4HybridPort));
            return OFC_FAILURE;
        }

        if (TnlIfEntry.u4EncapsMethod != TNL_TYPE_OPENFLOW)
        {
            /* tunnel created is not of type openflow */
            OFC_TRC_FUNC ((OFC_HYBRID_TRC, "tunnel is not openflow %d\n",
                           u4HybridPort));
            return OFC_FAILURE;
        }
    }
    else if (i4IntfType == OFC_HYBRID_PORT_L2)
    {
        /* Check if the port is created by admin */
        SPRINTF ((CHR1 *) au1Alias, "%s%u", au1IfNameLag, i4IntfIdx);
        u4RetVal = CfaGetInterfaceIndexFromName (au1Alias, &u4HybridPort);
        if (u4RetVal == OSIX_FAILURE)
        {
            /* No loopback created with the given index */
            OFC_TRC_FUNC ((OFC_HYBRID_TRC, "No port channel mapped to %d\n",
                           i4IntfIdx));
            return OFC_FAILURE;
        }
    }
    else
    {
        /* should be for disabling hybrid */
        OFC_TRC_FUNC ((OFC_HYBRID_TRC, "disabling hybrid\n"));
        return OFC_SUCCESS;
    }
    OFC_TRC_FUNC ((OFC_HYBRID_TRC, "Hybrid port - %d\n", u4HybridPort));
    gaHybridPortDb[u4ContextId].u4HybridPort = u4HybridPort;
    gaHybridPortDb[u4ContextId].u1IntfType = (UINT1) i4IntfType;

    OFC_TRC_FUNC ((OFC_FN_EXIT, "OfcHybridPortCheck\n"));
    return OFC_SUCCESS;
}

/******************************************************************************
 * Function    : OfcHybridPortCreate
 * Description : This routine sets virtual port for Hybrid
 * Input       : u4ContextId  - Context for which the tunnel is mapped.
 *               i4IntfType   - Interface Type (L2/L3)
 *               i4IntfIdx    - Interface Index
 * Output      : None 
 * Returns     : OFC_SUCCESS or OFC_FAILURE
 ******************************************************************************/
UINT4
OfcHybridPortCreate (UINT4 u4ContextId, INT4 i4IntfType, INT4 i4IntfIdx)
{
    OFC_TRC_FUNC ((OFC_FN_ENTRY, "OfcHybridPortCreate\n"));

    if (gaHybridPortDb[u4ContextId].u1Set == OFC_ONE)
    {
        /* dummy create */
        return OFC_SUCCESS;
    }

    if ((gaHybridPortDb[u4ContextId].u1IntfType == OFC_ZERO) &&
        (gaHybridPortDb[u4ContextId].u4HybridPort == OFC_ZERO))
    {
        /* msr */
        if (OfcHybridPortCheck (u4ContextId, i4IntfType, i4IntfIdx) !=
            OFC_SUCCESS)
        {
            /* msr failed */
            OFC_TRC_FUNC ((OFC_HYBRID_TRC, "MSR failed for hybrid\n"));
            return OFC_FAILURE;
        }
    }

    if (gaHybridPortDb[u4ContextId].u1IntfType != (UINT1) i4IntfType)
    {
        /* Type mismatch */
        OFC_TRC_FUNC ((OFC_HYBRID_TRC, "interface type mismatch in set\n"));
        return OFC_FAILURE;
    }

    if (i4IntfType == OFC_HYBRID_PORT_L2)
    {
        L2IwfPortOperStatusIndication (gaHybridPortDb[u4ContextId].u4HybridPort,
                                       CFA_IF_UP);
    }
    gaHybridPortDb[u4ContextId].u1Set = OFC_ONE;

    OFC_TRC_FUNC ((OFC_FN_EXIT, "OfcHybridPortCreate\n"));
    return OFC_SUCCESS;
}

/******************************************************************************
 * Function    : OfcHybridPortDelete
 * Description : This routine deletes virtual port created for Hybrid in CFA
 * Input       : u4ContextId - Context in which the virtual port is deleted 
 * Output      : None
 * Returns     : OFC_SUCCESS or OFC_FAILURE
 ******************************************************************************/
UINT4
OfcHybridPortDelete (UINT4 u4ContextId)
{
    UINT4               u4HybridPort = OFC_ZERO;
    UINT1               u1IntfType = OFC_ZERO;

    OFC_TRC_FUNC ((OFC_FN_ENTRY, "OfcHybridPortDelete\n"));

    if (gaHybridPortDb[u4ContextId].u1Set != OFC_ONE)
    {
        /* dummy delete */
        return OFC_SUCCESS;
    }
    u4HybridPort = gaHybridPortDb[u4ContextId].u4HybridPort;
    u1IntfType = gaHybridPortDb[u4ContextId].u1IntfType;

    if (u1IntfType == OFC_HYBRID_PORT_L3)
    {
        /* Port Deletion is not required for L3 */
    }
    else if (u1IntfType == OFC_HYBRID_PORT_L2)
    {
        L2IwfPortOperStatusIndication (u4HybridPort, CFA_IF_DOWN);
    }
    else
    {
        /* this should not happen */
        OFC_TRC_FUNC ((OFC_HYBRID_TRC, "Invalid Tunnel type %d\n", u1IntfType));
        return OFC_FAILURE;
    }
    gaHybridPortDb[u4ContextId].u4HybridPort = OFC_ZERO;
    gaHybridPortDb[u4ContextId].u1IntfType = OFC_ZERO;
    gaHybridPortDb[u4ContextId].u1Set = OFC_ZERO;
    OFC_TRC_FUNC ((OFC_FN_EXIT, "OfcHybridPortDelete\n"));
    return OFC_SUCCESS;
}

/******************************************************************************
 * Function    : OfcHybridSendPktToIss
 * Description : This routine sends the data packets to ISS (Hybrid Normal
 *               Processing)
 * Input       : u4ContextId - Context Id 
 *               pBuf        - Pointer to CRU Buffer 
 * Output      : None
 * Returns     : OFC_SUCCESS or OFC_FAILURE
 ******************************************************************************/
UINT4
OfcHybridSendPktToIss (UINT4 u4ContextId, tCRU_BUF_CHAIN_HEADER * pBuf)
{
    UINT4               u4IfIndex = OFC_ZERO;
    UINT4               u4PktSize = OFC_ZERO;
    INT4                i4RetVal = CFA_FAILURE;

    OFC_TRC_FUNC ((OFC_FN_ENTRY, "OfcHybridSendPktToIss\n"));

    /* Validate the ContextId */
    if (u4ContextId == (UINT4) OFC_INVALID_CONTEXT)
    {
        OFC_TRC_FUNC ((OFC_HYBRID_TRC, "Invalid Context Id\n"));
        return OFC_FAILURE;
    }
    u4IfIndex = gaHybridPortDb[u4ContextId].u4HybridPort;

    /* increment the counters */
    u4PktSize = CRU_BUF_Get_ChainValidByteCount (pBuf);
    CFA_IF_SET_IN_UCAST (u4IfIndex);
    CFA_IF_SET_IN_OCTETS (u4IfIndex, u4PktSize);

    /* Set the CRU Buf properties */
    GET_MODULE_DATA_PTR (pBuf)->u4IfIndex = u4IfIndex;
    GET_MODULE_DATA_PTR (pBuf)->u4Protocol = CFA_ENET_IPV4;
    GET_MODULE_DATA_PTR (pBuf)->u1PktType = CFA_LINK_UCAST;
    GET_MODULE_DATA_PTR (pBuf)->u1EncapType = CFA_ENCAP_ENETV2;
    GET_MODULE_DATA_PTR (pBuf)->u1Command = OFC_TO_CFA;

    /* Deliver the packet to CFA to handle appropriately */
    i4RetVal = CfaHandlePktFromOfc (pBuf);
    if (i4RetVal == CFA_FAILURE)
    {
        OFC_TRC_FUNC ((OFC_HYBRID_TRC, "Cfa failed to handle the packet\n"));
        return OFC_FAILURE;
    }
    OFC_TRC_FUNC ((OFC_FN_EXIT, "OfcHybridSendPktToIss\n"));
    return OFC_SUCCESS;
}

/******************************************************************************
 * Function    : OfcHybridRxPktFromIssCheck
 * Description : This routine verifys if the packets are from ISS to OFCL
 *               in hybrid processing
 * Input       : u4IfIndex    - Input Port 
 * Output      : pu4ContextId - Pointer to Context Id
 * Returns     : OFC_SUCCESS or OFC_FAILURE
 ******************************************************************************/
UINT4
OfcHybridRxPktFromIssCheck (UINT4 u4IfIndex, UINT4 *pu4ContextId)
{
    UINT1               u1Iter = OFC_ZERO;    /* Iterator for Contexts */

    OFC_TRC_FUNC ((OFC_FN_ENTRY, "OfcHybridRxPktFromIssCheck\n"));
    /*
     * Go through the hybrid data base to see if we have a matching
     * entry with port passed in.
     */
    for (u1Iter = OFC_ZERO; u1Iter < MAX_OFC_FSOFCCFGTABLE; u1Iter++)
    {
        if (gaHybridPortDb[u1Iter].u4HybridPort == u4IfIndex)
        {
            *pu4ContextId = u1Iter;
            return OFC_SUCCESS;
        }
    }
    OFC_TRC_FUNC ((OFC_FN_EXIT, "OfcHybridRxPktFromIssCheck\n"));
    return OFC_FAILURE;
}

/******************************************************************************
 * Function    : OfcHybridHandlePktFromIss
 * Description : This routine handles the data packets sent from various
 *               modules in ISS and enqueues them into OFC queue for further
 *               processing
 * Input       : pBuf - Pointer to CRU Buffer 
 * Output      : None
 * Returns     : OFC_SUCCESS or OFC_FAILURE
 ******************************************************************************/
UINT4
OfcHybridHandlePktFromIss (UINT4 u4IfIndex, tCRU_BUF_CHAIN_HEADER * pBuf)
{
    UINT4               u4RetVal = OFC_ZERO;
    UINT4               u4PktSize = OFC_ZERO;

    OFC_TRC_FUNC ((OFC_FN_ENTRY, "OfcHybridHandlePktFromIss\n"));

    /* increment the counters */
    u4PktSize = CRU_BUF_Get_ChainValidByteCount (pBuf);
    CFA_IF_SET_OUT_UCAST (u4IfIndex);
    CFA_IF_SET_OUT_OCTETS (u4IfIndex, u4PktSize);

    /* Post it to the OFC queue for pipeline processing */
    u4RetVal = (UINT4) (OpenflowNotifyPktRecv (u4IfIndex, pBuf));
    if (u4RetVal == OFC_FAILURE)
    {
        /* just return, buffers are already released */
        OFC_TRC_FUNC ((OFC_HYBRID_TRC, "Posting to OFC queue failed\n"));
        return OFC_FAILURE;
    }
    OFC_TRC_FUNC ((OFC_FN_EXIT, "OfcHybridHandlePktFromIss\n"));
    return OFC_SUCCESS;
}

/******************************************************************************
 * Function    : OfcHybridFloodOnAllPorts
 * Description : This routine floods the data packet on all the switch physical
 *               interfaces. 
 * Input       : u4IfIndex - Interface Index
 *               pBuf      - Pointer to CRU Buffer 
 * Output      : None
 * Returns     : OFC_SUCCESS or OFC_FAILURE
 ******************************************************************************/
UINT4
OfcHybridFloodOnAllPorts (tOfcFsofcCfgEntry * pOfcFsofcCfgEntry,
                          UINT4 u4IfIndex, tCRU_BUF_CHAIN_HEADER * pBuf)
{
    tOfcFsofcIfEntry    IfEntry;
    UINT4               u4PktSize = OFC_ZERO;
    UINT4               u4IntfNum = OFC_ZERO;
    UINT4               u4NoFwd = OFC_ZERO;
    UINT1               u1OperStatus = OFC_ZERO;
    UINT4               u4ContextId =
        pOfcFsofcCfgEntry->MibObject.u4FsofcContextId;

    OFC_TRC_FUNC ((OFC_FN_ENTRY, "OfcHybridFloodOnAllPorts\n"));

    MEMSET (&IfEntry, OFC_ZERO, sizeof (IfEntry));
    MEMSET (pOfcFsofcCfgEntry->pDataBuf, OFC_ZERO, OFC_MAX_CONTROLLER_PKT_LEN);

    u4PktSize = CRU_BUF_Get_ChainValidByteCount (pBuf);
    if (CRU_BUF_Copy_FromBufChain (pBuf, pOfcFsofcCfgEntry->pDataBuf, OFC_ZERO,
                                   u4PktSize) == CRU_FAILURE)
    {
        OFC_TRC_FUNC ((OFC_HYBRID_TRC, "CRU Copy Failed\n"));
        return OFC_FAILURE;
    }

    /* loop through all the physical interfaces */
    for (u4IntfNum = OFC_ONE; u4IntfNum < SYS_DEF_MAX_PHYSICAL_INTERFACES;
         u4IntfNum++)
    {
        if (u4IfIndex == u4IntfNum)
        {
            /* ingress port, continue */
            OFC_TRC_FUNC ((OFC_HYBRID_TRC, "same as ingress port %d\n",
                           u4IfIndex));
            continue;
        }

        /* check whether port is valid */
        if (CfaGetIfOperStatus (u4IntfNum, &u1OperStatus) == CFA_FAILURE)
        {
            OFC_TRC_FUNC ((OFC_HYBRID_TRC, "invalid port %d\n", u4IntfNum));
            continue;
        }

        /* check for port oper status */
        if (u1OperStatus == CFA_IF_DOWN)
        {
            OFC_TRC_FUNC ((OFC_HYBRID_TRC, "port %d down\n", u4IntfNum));
            continue;
        }

        /* check if the port is openflow or not  */
        IfEntry.MibObject.u4FsofcContextId = u4ContextId;
        IfEntry.MibObject.i4FsofcIfIndex = (INT4) u4IntfNum;
        if (OfcGetAllFsofcIfTable (&IfEntry) == OFC_SUCCESS)
        {
            /* check if no forward flag is set */
            u4NoFwd = (OSIX_HTONL (IfEntry.u4Config)) >> OFC_FIVE;
            if (u4NoFwd & OFC_ONE)
            {
                OFC_TRC_FUNC ((OFC_HYBRID_TRC, "Do not fwd Packet on %d\n",
                               u4IntfNum));
                continue;
            }
        }
        else                    /* ISS port */
        {
            /* check for STP status */
            if (L2IwfGetInstPortState (OFC_ZERO, u4IntfNum) !=
                AST_PORT_STATE_FORWARDING)
            {
                OFC_TRC_FUNC ((OFC_HYBRID_TRC, "stp disabled port %d\n",
                               u4IntfNum));
                continue;
            }
        }

        CFA_IF_SET_OUT_UCAST (u4IntfNum);
        CFA_IF_SET_OUT_OCTETS (u4IntfNum, u4PktSize);
        /* send out on this port */
        CfaGddEthWrite (pOfcFsofcCfgEntry->pDataBuf, (UINT2) u4IntfNum,
                        u4PktSize);
    }
    OFC_TRC_FUNC ((OFC_FN_EXIT, "OfcHybridFloodOnAllPorts\n"));
    return OFC_SUCCESS;
}
