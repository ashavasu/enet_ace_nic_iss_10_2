/********************************************************************
* Copyright (C) 2013 Aricent Inc . All Rights Reserved
*
* $Id: ofcdsg.c,v 1.2 2014/01/31 13:07:23 siva Exp $
*
* Description: This file contains the routines for DataStructure access for the module Ofc 
*********************************************************************/

#include "ofcinc.h"

/****************************************************************************
 Function    :  OfcGetFirstFsofcCfgTable
 Input       :  NONE
 Description :  This Routine is used to take
                first index from a table
 Returns     :  pOfcFsofcCfgEntry or NULL
****************************************************************************/
tOfcFsofcCfgEntry  *
OfcGetFirstFsofcCfgTable ()
{
    tOfcFsofcCfgEntry  *pOfcFsofcCfgEntry = NULL;

    pOfcFsofcCfgEntry =
        (tOfcFsofcCfgEntry *) RBTreeGetFirst (gOfcGlobals.
                                              OfcGlbMib.FsofcCfgTable);

    return pOfcFsofcCfgEntry;
}

/****************************************************************************
 Function    :  OfcGetNextFsofcCfgTable
 Input       :  pCurrentOfcFsofcCfgEntry
 Description :  This Routine is used to take
                next index from a table
 Returns     :  pNextOfcFsofcCfgEntry or NULL
****************************************************************************/
tOfcFsofcCfgEntry  *
OfcGetNextFsofcCfgTable (tOfcFsofcCfgEntry * pCurrentOfcFsofcCfgEntry)
{
    tOfcFsofcCfgEntry  *pNextOfcFsofcCfgEntry = NULL;

    pNextOfcFsofcCfgEntry =
        (tOfcFsofcCfgEntry *) RBTreeGetNext (gOfcGlobals.
                                             OfcGlbMib.FsofcCfgTable,
                                             (tRBElem *)
                                             pCurrentOfcFsofcCfgEntry, NULL);

    return pNextOfcFsofcCfgEntry;
}

/****************************************************************************
 Function    :  OfcGetFsofcCfgTable
 Input       :  pOfcFsofcCfgEntry
 Description :  This Routine is used to take
                Row for the index from a table
 Returns     :  pGetOfcFsofcCfgEntry or NULL
****************************************************************************/
tOfcFsofcCfgEntry  *
OfcGetFsofcCfgTable (tOfcFsofcCfgEntry * pOfcFsofcCfgEntry)
{
    tOfcFsofcCfgEntry  *pGetOfcFsofcCfgEntry = NULL;

    pGetOfcFsofcCfgEntry =
        (tOfcFsofcCfgEntry *) RBTreeGet (gOfcGlobals.OfcGlbMib.FsofcCfgTable,
                                         (tRBElem *) pOfcFsofcCfgEntry);

    return pGetOfcFsofcCfgEntry;
}

/****************************************************************************
 Function    :  OfcGetFirstFsofcControllerConnTable
 Input       :  NONE
 Description :  This Routine is used to take
                first index from a table
 Returns     :  pOfcFsofcControllerConnEntry or NULL
****************************************************************************/
tOfcFsofcControllerConnEntry *
OfcGetFirstFsofcControllerConnTable ()
{
    tOfcFsofcControllerConnEntry *pOfcFsofcControllerConnEntry = NULL;

    pOfcFsofcControllerConnEntry =
        (tOfcFsofcControllerConnEntry *) RBTreeGetFirst (gOfcGlobals.
                                                         OfcGlbMib.
                                                         FsofcControllerConnTable);

    return pOfcFsofcControllerConnEntry;
}

/****************************************************************************
 Function    :  OfcGetNextFsofcControllerConnTable
 Input       :  pCurrentOfcFsofcControllerConnEntry
 Description :  This Routine is used to take
                next index from a table
 Returns     :  pNextOfcFsofcControllerConnEntry or NULL
****************************************************************************/
tOfcFsofcControllerConnEntry *
OfcGetNextFsofcControllerConnTable (tOfcFsofcControllerConnEntry *
                                    pCurrentOfcFsofcControllerConnEntry)
{
    tOfcFsofcControllerConnEntry *pNextOfcFsofcControllerConnEntry = NULL;

    pNextOfcFsofcControllerConnEntry =
        (tOfcFsofcControllerConnEntry *) RBTreeGetNext (gOfcGlobals.
                                                        OfcGlbMib.
                                                        FsofcControllerConnTable,
                                                        (tRBElem *)
                                                        pCurrentOfcFsofcControllerConnEntry,
                                                        NULL);

    return pNextOfcFsofcControllerConnEntry;
}

/****************************************************************************
 Function    :  OfcGetFsofcControllerConnTable
 Input       :  pOfcFsofcControllerConnEntry
 Description :  This Routine is used to take
                Row for the index from a table
 Returns     :  pGetOfcFsofcControllerConnEntry or NULL
****************************************************************************/
tOfcFsofcControllerConnEntry *
OfcGetFsofcControllerConnTable (tOfcFsofcControllerConnEntry *
                                pOfcFsofcControllerConnEntry)
{
    tOfcFsofcControllerConnEntry *pGetOfcFsofcControllerConnEntry = NULL;

    pGetOfcFsofcControllerConnEntry =
        (tOfcFsofcControllerConnEntry *) RBTreeGet (gOfcGlobals.
                                                    OfcGlbMib.
                                                    FsofcControllerConnTable,
                                                    (tRBElem *)
                                                    pOfcFsofcControllerConnEntry);

    return pGetOfcFsofcControllerConnEntry;
}

/****************************************************************************
 Function    :  OfcGetFirstFsofcIfTable
 Input       :  NONE
 Description :  This Routine is used to take
                first index from a table
 Returns     :  pOfcFsofcIfEntry or NULL
****************************************************************************/
tOfcFsofcIfEntry   *
OfcGetFirstFsofcIfTable ()
{
    tOfcFsofcIfEntry   *pOfcFsofcIfEntry = NULL;

    pOfcFsofcIfEntry =
        (tOfcFsofcIfEntry *) RBTreeGetFirst (gOfcGlobals.
                                             OfcGlbMib.FsofcIfTable);

    return pOfcFsofcIfEntry;
}

/****************************************************************************
 Function    :  OfcGetNextFsofcIfTable
 Input       :  pCurrentOfcFsofcIfEntry
 Description :  This Routine is used to take
                next index from a table
 Returns     :  pNextOfcFsofcIfEntry or NULL
****************************************************************************/
tOfcFsofcIfEntry   *
OfcGetNextFsofcIfTable (tOfcFsofcIfEntry * pCurrentOfcFsofcIfEntry)
{
    tOfcFsofcIfEntry   *pNextOfcFsofcIfEntry = NULL;

    pNextOfcFsofcIfEntry =
        (tOfcFsofcIfEntry *) RBTreeGetNext (gOfcGlobals.
                                            OfcGlbMib.FsofcIfTable,
                                            (tRBElem *)
                                            pCurrentOfcFsofcIfEntry, NULL);

    return pNextOfcFsofcIfEntry;
}

/****************************************************************************
 Function    :  OfcGetFsofcIfTable
 Input       :  pOfcFsofcIfEntry
 Description :  This Routine is used to take
                Row for the index from a table
 Returns     :  pGetOfcFsofcIfEntry or NULL
****************************************************************************/
tOfcFsofcIfEntry   *
OfcGetFsofcIfTable (tOfcFsofcIfEntry * pOfcFsofcIfEntry)
{
    tOfcFsofcIfEntry   *pGetOfcFsofcIfEntry = NULL;

    pGetOfcFsofcIfEntry =
        (tOfcFsofcIfEntry *) RBTreeGet (gOfcGlobals.OfcGlbMib.FsofcIfTable,
                                        (tRBElem *) pOfcFsofcIfEntry);

    return pGetOfcFsofcIfEntry;
}

/****************************************************************************
 Function    :  OfcGetFirstFsofcFlowTable
 Input       :  NONE
 Description :  This Routine is used to take
                first index from a table
 Returns     :  pOfcFsofcFlowEntry or NULL
****************************************************************************/
tOfcFsofcFlowEntry *
OfcGetFirstFsofcFlowTable ()
{
    tOfcFsofcFlowEntry *pOfcFsofcFlowEntry = NULL;

    pOfcFsofcFlowEntry =
        (tOfcFsofcFlowEntry *) RBTreeGetFirst (gOfcGlobals.
                                               OfcGlbMib.FsofcFlowTable);

    return pOfcFsofcFlowEntry;
}

/****************************************************************************
 Function    :  OfcGetNextFsofcFlowTable
 Input       :  pCurrentOfcFsofcFlowEntry
 Description :  This Routine is used to take
                next index from a table
 Returns     :  pNextOfcFsofcFlowEntry or NULL
****************************************************************************/
tOfcFsofcFlowEntry *
OfcGetNextFsofcFlowTable (tOfcFsofcFlowEntry * pCurrentOfcFsofcFlowEntry)
{
    tOfcFsofcFlowEntry *pNextOfcFsofcFlowEntry = NULL;

    pNextOfcFsofcFlowEntry =
        (tOfcFsofcFlowEntry *) RBTreeGetNext (gOfcGlobals.
                                              OfcGlbMib.FsofcFlowTable,
                                              (tRBElem *)
                                              pCurrentOfcFsofcFlowEntry, NULL);

    return pNextOfcFsofcFlowEntry;
}

/****************************************************************************
 Function    :  OfcGetFirstFsofcGroupTable
 Input       :  NONE
 Description :  This Routine is used to take
                first index from a table
 Returns     :  pOfcFsofcGroupEntry or NULL
****************************************************************************/
tOfcFsofcGroupEntry *
OfcGetFirstFsofcGroupTable ()
{
    tOfcFsofcGroupEntry *pOfcFsofcGroupEntry = NULL;

    pOfcFsofcGroupEntry =
        (tOfcFsofcGroupEntry *) RBTreeGetFirst (gOfcGlobals.
                                                OfcGlbMib.FsofcGroupTable);

    return pOfcFsofcGroupEntry;
}

/****************************************************************************
 Function    :  OfcGetNextFsofcGroupTable
 Input       :  pCurrentOfcFsofcGroupEntry
 Description :  This Routine is used to take
                next index from a table
 Returns     :  pNextOfcFsofcGroupEntry or NULL
****************************************************************************/
tOfcFsofcGroupEntry *
OfcGetNextFsofcGroupTable (tOfcFsofcGroupEntry * pCurrentOfcFsofcGroupEntry)
{
    tOfcFsofcGroupEntry *pNextOfcFsofcGroupEntry = NULL;

    pNextOfcFsofcGroupEntry =
        (tOfcFsofcGroupEntry *) RBTreeGetNext (gOfcGlobals.
                                               OfcGlbMib.FsofcGroupTable,
                                               (tRBElem *)
                                               pCurrentOfcFsofcGroupEntry,
                                               NULL);

    return pNextOfcFsofcGroupEntry;
}

/****************************************************************************
 Function    :  OfcGetFirstFsofcMeterTable
 Input       :  NONE
 Description :  This Routine is used to take
                first index from a table
 Returns     :  pOfcFsofcMeterEntry or NULL
****************************************************************************/
tOfcFsofcMeterEntry *
OfcGetFirstFsofcMeterTable ()
{
    tOfcFsofcMeterEntry *pOfcFsofcMeterEntry = NULL;

    pOfcFsofcMeterEntry =
        (tOfcFsofcMeterEntry *) RBTreeGetFirst (gOfcGlobals.
                                                OfcGlbMib.FsofcMeterTable);

    return pOfcFsofcMeterEntry;
}

/****************************************************************************
 Function    :  OfcGetNextFsofcMeterTable
 Input       :  pCurrentOfcFsofcMeterEntry
 Description :  This Routine is used to take
                next index from a table
 Returns     :  pNextOfcFsofcMeterEntry or NULL
****************************************************************************/
tOfcFsofcMeterEntry *
OfcGetNextFsofcMeterTable (tOfcFsofcMeterEntry * pCurrentOfcFsofcMeterEntry)
{
    tOfcFsofcMeterEntry *pNextOfcFsofcMeterEntry = NULL;

    pNextOfcFsofcMeterEntry =
        (tOfcFsofcMeterEntry *) RBTreeGetNext (gOfcGlobals.
                                               OfcGlbMib.FsofcMeterTable,
                                               (tRBElem *)
                                               pCurrentOfcFsofcMeterEntry,
                                               NULL);

    return pNextOfcFsofcMeterEntry;
}
