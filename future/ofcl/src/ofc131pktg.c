/********************************************************************
 * Copyright (C) 2013 Aricent Inc . All Rights Reserved
 *
 * $Id: ofc131pktg.c,v 1.3 2013/10/22 10:46:57 siva Exp $
 *
 * Description: This file contains the Ofc131 pkt process related routines
 *********************************************************************/

#include "ofcinc.h"
#include "ofc131pkt.h"

/****************************************************************************
 * Function    :  Ofc131PktOfpParse
 * Description :  This function is for Ofp Header Parsing  
 * Input       :  pOfp131Pkt Pointer to the header,connection and buffer
 * Output      :  pu4Error - Error on failure 
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE 
 ****************************************************************************/
INT4
Ofc131PktOfpParse (tOfp131Pkt * pOfp131Pkt, VOID *pConnPtr,
                   tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 *pu4Error)
{
    tOfcFsofcControllerConnEntry *pConn = NULL;

    if (pConnPtr == NULL)
    {
        return OSIX_FAILURE;
    }
    else
    {

        pConn = (tOfcFsofcControllerConnEntry *) pConnPtr;
    }

    if (pConn != NULL)
    {
        OfcTmrRestartTmr (&(pConn->EchoInterTimer),
                          OFC_ECHOINTER_TMR, OFC_ECHOINTER_INTERVAL);
    }
    if (Ofc131PktOfpu1VersionValidate (pOfp131Pkt, pConnPtr, pBuf, pu4Error) ==
        OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    if (Ofc131PktOfpu1TypeValidate (pOfp131Pkt, pConnPtr, pBuf, pu4Error) ==
        OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    if (Ofc131PktOfpu2LengthValidate (pOfp131Pkt, pConnPtr, pBuf, pu4Error) ==
        OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    if (Ofc131PktOfpu4XidValidate (pOfp131Pkt, pConnPtr, pBuf, pu4Error) ==
        OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    if (Ofc131PktOfpu1VersionAction (pOfp131Pkt, pConnPtr, pBuf, pu4Error) ==
        OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    if (Ofc131PktOfpu2LengthAction (pOfp131Pkt, pConnPtr, pBuf, pu4Error) ==
        OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    if (Ofc131PktOfpu4XidAction (pOfp131Pkt, pConnPtr, pBuf, pu4Error) ==
        OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    if (Ofc131PktOfpu1TypeAction (pOfp131Pkt, pConnPtr, pBuf, pu4Error) ==
        OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  Ofc131PktHelloParse
 * Description :  This function is for Hello Header Parsing  
 * Input       :  pOfp131Hello Pointer to the header,connection and buffer
 * Output      :  pu4Error - Error on failure 
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE 
 ****************************************************************************/
INT4
Ofc131PktHelloParse (tOfp131Hello * pOfp131Hello, VOID *pConnPtr,
                     tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 *pu4Error)
{
    if (Ofc131PktHelloOfpHelloElemHdrValidate
        (pOfp131Hello, pConnPtr, pBuf, pu4Error) == OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    if (Ofc131PktHelloOfpHelloElemHdrAction
        (pOfp131Hello, pConnPtr, pBuf, pu4Error) == OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  Ofc131PktEchoReqMsgParse
 * Description :  This function is for EchoReqMsg Header Parsing  
 * Input       :  pOfp131EchoReqMsg Pointer to the header,connection and buffer
 * Output      :  pu4Error - Error on failure 
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE 
 ****************************************************************************/
INT4
Ofc131PktEchoReqMsgParse (tOfp131EchoReqMsg * pOfp131EchoReqMsg, VOID *pConnPtr,
                          tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 *pu4Error)
{
    if (Ofc131PktEchoReqMsgau1DataValidate
        (pOfp131EchoReqMsg, pConnPtr, pBuf, pu4Error) == OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    if (Ofc131PktEchoReqMsgau1DataAction
        (pOfp131EchoReqMsg, pConnPtr, pBuf, pu4Error) == OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  Ofc131PktEchoRplyMsgParse
 * Description :  This function is for EchoRplyMsg Header Parsing  
 * Input       :  pOfp131EchoRplyMsg Pointer to the header,connection and buffer
 * Output      :  pu4Error - Error on failure 
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE 
 ****************************************************************************/
INT4
Ofc131PktEchoRplyMsgParse (tOfp131EchoRplyMsg * pOfp131EchoRplyMsg,
                           VOID *pConnPtr, tCRU_BUF_CHAIN_HEADER * pBuf,
                           UINT4 *pu4Error)
{
    if (Ofc131PktEchoRplyMsgau1DataValidate
        (pOfp131EchoRplyMsg, pConnPtr, pBuf, pu4Error) == OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    if (Ofc131PktEchoRplyMsgau1DataAction
        (pOfp131EchoRplyMsg, pConnPtr, pBuf, pu4Error) == OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    ((tOfcFsofcControllerConnEntry *) (pConnPtr))->u4IsRecieved = TRUE;
    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  Ofc131PktExperimenterHdrParse
 * Description :  This function is for ExperimenterHdr Header Parsing  
 * Input       :  pOfp131ExperimenterHdr Pointer to the header,connection and buffer
 * Output      :  pu4Error - Error on failure 
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE 
 ****************************************************************************/
INT4
Ofc131PktExperimenterHdrParse (tOfp131ExperimenterHdr * pOfp131ExperimenterHdr,
                               VOID *pConnPtr, tCRU_BUF_CHAIN_HEADER * pBuf,
                               UINT4 *pu4Error)
{
    if (Ofc131PktExperimenterHdru4ExperimenterValidate
        (pOfp131ExperimenterHdr, pConnPtr, pBuf, pu4Error) == OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    if (Ofc131PktExperimenterHdru4ExpTypeValidate
        (pOfp131ExperimenterHdr, pConnPtr, pBuf, pu4Error) == OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    if (Ofc131PktExperimenterHdru4ExperimenterAction
        (pOfp131ExperimenterHdr, pConnPtr, pBuf, pu4Error) == OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    if (Ofc131PktExperimenterHdru4ExpTypeAction
        (pOfp131ExperimenterHdr, pConnPtr, pBuf, pu4Error) == OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  Ofc131PktFeatureReqParse
 * Description :  This function is for FeatureReq Header Parsing  
 * Input       :  pOfp131FeatureReq Pointer to the header,connection and buffer
 * Output      :  pu4Error - Error on failure 
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE 
 ****************************************************************************/
INT4
Ofc131PktFeatureReqParse (tOfp131FeatureReq * pOfp131FeatureReq, VOID *pConnPtr,
                          tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 *pu4Error)
{
    if (Ofc131PktFeatureReqau1DummyValidate
        (pOfp131FeatureReq, pConnPtr, pBuf, pu4Error) == OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    if (Ofc131PktFeatureReqau1DummyAction
        (pOfp131FeatureReq, pConnPtr, pBuf, pu4Error) == OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  Ofc131PktConfigGetParse
 * Description :  This function is for ConfigGet Header Parsing  
 * Input       :  pOfp131ConfigGet Pointer to the header,connection and buffer
 * Output      :  pu4Error - Error on failure 
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE 
 ****************************************************************************/
INT4
Ofc131PktConfigGetParse (tOfp131ConfigGet * pOfp131ConfigGet, VOID *pConnPtr,
                         tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 *pu4Error)
{
    if (Ofc131PktConfigGetau1DummyValidate
        (pOfp131ConfigGet, pConnPtr, pBuf, pu4Error) == OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    if (Ofc131PktConfigGetau1DummyAction
        (pOfp131ConfigGet, pConnPtr, pBuf, pu4Error) == OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  Ofc131PktConfigSetParse
 * Description :  This function is for ConfigSet Header Parsing  
 * Input       :  pOfp131ConfigSet Pointer to the header,connection and buffer
 * Output      :  pu4Error - Error on failure 
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE 
 ****************************************************************************/
INT4
Ofc131PktConfigSetParse (tOfp131ConfigSet * pOfp131ConfigSet, VOID *pConnPtr,
                         tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 *pu4Error)
{
    if (Ofc131PktConfigSetu2FlagsValidate
        (pOfp131ConfigSet, pConnPtr, pBuf, pu4Error) == OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    if (Ofc131PktConfigSetu2MissSendLenValidate
        (pOfp131ConfigSet, pConnPtr, pBuf, pu4Error) == OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    if (Ofc131PktConfigSetu2FlagsAction
        (pOfp131ConfigSet, pConnPtr, pBuf, pu4Error) == OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    if (Ofc131PktConfigSetu2MissSendLenAction
        (pOfp131ConfigSet, pConnPtr, pBuf, pu4Error) == OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  Ofc131PktPktOutParse
 * Description :  This function is for PktOut Header Parsing  
 * Input       :  pOfp131PktOut Pointer to the header,connection and buffer
 * Output      :  pu4Error - Error on failure 
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE 
 ****************************************************************************/
INT4
Ofc131PktPktOutParse (tOfp131PktOut * pOfp131PktOut, VOID *pConnPtr,
                      tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 *pu4Error)
{
    if (Ofc131PktPktOutu4BufferIdValidate
        (pOfp131PktOut, pConnPtr, pBuf, pu4Error) == OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    if (Ofc131PktPktOutu4InPortValidate
        (pOfp131PktOut, pConnPtr, pBuf, pu4Error) == OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    if (Ofc131PktPktOutu2ActionsLenValidate
        (pOfp131PktOut, pConnPtr, pBuf, pu4Error) == OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    if (Ofc131PktPktOutau1PadValidate (pOfp131PktOut, pConnPtr, pBuf, pu4Error)
        == OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    if (Ofc131PktPktOutOfpActionHdrValidate
        (pOfp131PktOut, pConnPtr, pBuf, pu4Error) == OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    if (Ofc131PktPktOutu4BufferIdAction
        (pOfp131PktOut, pConnPtr, pBuf, pu4Error) == OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    if (Ofc131PktPktOutu4InPortAction (pOfp131PktOut, pConnPtr, pBuf, pu4Error)
        == OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    if (Ofc131PktPktOutu2ActionsLenAction
        (pOfp131PktOut, pConnPtr, pBuf, pu4Error) == OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    if (Ofc131PktPktOutau1PadAction (pOfp131PktOut, pConnPtr, pBuf, pu4Error) ==
        OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    if (Ofc131PktPktOutOfpActionHdrAction
        (pOfp131PktOut, pConnPtr, pBuf, pu4Error) == OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  Ofc131PktFlowModParse
 * Description :  This function is for FlowMod Header Parsing  
 * Input       :  pOfp131FlowMod Pointer to the header,connection and buffer
 * Output      :  pu4Error - Error on failure 
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE 
 ****************************************************************************/
INT4
Ofc131PktFlowModParse (tOfp131FlowMod * pOfp131FlowMod, VOID *pConnPtr,
                       tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 *pu4Error)
{
    if (Ofc131PktFlowModu8CookieValidate
        (pOfp131FlowMod, pConnPtr, pBuf, pu4Error) == OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    if (Ofc131PktFlowModu8CookieMaskValidate
        (pOfp131FlowMod, pConnPtr, pBuf, pu4Error) == OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    if (Ofc131PktFlowModu1TableIdValidate
        (pOfp131FlowMod, pConnPtr, pBuf, pu4Error) == OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    if (Ofc131PktFlowModu1CommandValidate
        (pOfp131FlowMod, pConnPtr, pBuf, pu4Error) == OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    if (Ofc131PktFlowModu2IdleTimeoutValidate
        (pOfp131FlowMod, pConnPtr, pBuf, pu4Error) == OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    if (Ofc131PktFlowModu2HardTimeoutValidate
        (pOfp131FlowMod, pConnPtr, pBuf, pu4Error) == OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    if (Ofc131PktFlowModu2PriorityValidate
        (pOfp131FlowMod, pConnPtr, pBuf, pu4Error) == OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    if (Ofc131PktFlowModu4BufIdValidate
        (pOfp131FlowMod, pConnPtr, pBuf, pu4Error) == OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    if (Ofc131PktFlowModu4OutPortValidate
        (pOfp131FlowMod, pConnPtr, pBuf, pu4Error) == OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    if (Ofc131PktFlowModu4OutGroupValidate
        (pOfp131FlowMod, pConnPtr, pBuf, pu4Error) == OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    if (Ofc131PktFlowModu2FlagsValidate
        (pOfp131FlowMod, pConnPtr, pBuf, pu4Error) == OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    if (Ofc131PktFlowModau1PadValidate
        (pOfp131FlowMod, pConnPtr, pBuf, pu4Error) == OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    if (Ofc131PktFlowModOfpMatchValidate
        (pOfp131FlowMod, pConnPtr, pBuf, pu4Error) == OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    if (Ofc131PktFlowModu8CookieAction
        (pOfp131FlowMod, pConnPtr, pBuf, pu4Error) == OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    if (Ofc131PktFlowModu8CookieMaskAction
        (pOfp131FlowMod, pConnPtr, pBuf, pu4Error) == OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    if (Ofc131PktFlowModu1TableIdAction
        (pOfp131FlowMod, pConnPtr, pBuf, pu4Error) == OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    if (Ofc131PktFlowModu1CommandAction
        (pOfp131FlowMod, pConnPtr, pBuf, pu4Error) == OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    if (Ofc131PktFlowModu2IdleTimeoutAction
        (pOfp131FlowMod, pConnPtr, pBuf, pu4Error) == OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    if (Ofc131PktFlowModu2HardTimeoutAction
        (pOfp131FlowMod, pConnPtr, pBuf, pu4Error) == OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    if (Ofc131PktFlowModu2PriorityAction
        (pOfp131FlowMod, pConnPtr, pBuf, pu4Error) == OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    if (Ofc131PktFlowModu4BufIdAction (pOfp131FlowMod, pConnPtr, pBuf, pu4Error)
        == OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    if (Ofc131PktFlowModu4OutPortAction
        (pOfp131FlowMod, pConnPtr, pBuf, pu4Error) == OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    if (Ofc131PktFlowModu4OutGroupAction
        (pOfp131FlowMod, pConnPtr, pBuf, pu4Error) == OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    if (Ofc131PktFlowModu2FlagsAction (pOfp131FlowMod, pConnPtr, pBuf, pu4Error)
        == OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    if (Ofc131PktFlowModau1PadAction (pOfp131FlowMod, pConnPtr, pBuf, pu4Error)
        == OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    if (Ofc131PktFlowModOfpMatchAction
        (pOfp131FlowMod, pConnPtr, pBuf, pu4Error) == OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  Ofc131PktGroupModParse
 * Description :  This function is for GroupMod Header Parsing  
 * Input       :  pOfp131GroupMod Pointer to the header,connection and buffer
 * Output      :  pu4Error - Error on failure 
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE 
 ****************************************************************************/
INT4
Ofc131PktGroupModParse (tOfp131GroupMod * pOfp131GroupMod, VOID *pConnPtr,
                        tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 *pu4Error)
{
    if (Ofc131PktGroupModu2CmdValidate
        (pOfp131GroupMod, pConnPtr, pBuf, pu4Error) == OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    if (Ofc131PktGroupModu1TypeValidate
        (pOfp131GroupMod, pConnPtr, pBuf, pu4Error) == OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    if (Ofc131PktGroupModu1PadValidate
        (pOfp131GroupMod, pConnPtr, pBuf, pu4Error) == OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    if (Ofc131PktGroupModu4GroupIdValidate
        (pOfp131GroupMod, pConnPtr, pBuf, pu4Error) == OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    if (Ofc131PktGroupModOfpBucketValidate
        (pOfp131GroupMod, pConnPtr, pBuf, pu4Error) == OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    if (Ofc131PktGroupModu2CmdAction (pOfp131GroupMod, pConnPtr, pBuf, pu4Error)
        == OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    if (Ofc131PktGroupModu1TypeAction
        (pOfp131GroupMod, pConnPtr, pBuf, pu4Error) == OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    if (Ofc131PktGroupModu1PadAction (pOfp131GroupMod, pConnPtr, pBuf, pu4Error)
        == OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    if (Ofc131PktGroupModu4GroupIdAction
        (pOfp131GroupMod, pConnPtr, pBuf, pu4Error) == OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    if (Ofc131PktGroupModOfpBucketAction
        (pOfp131GroupMod, pConnPtr, pBuf, pu4Error) == OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  Ofc131PktPortModParse
 * Description :  This function is for PortMod Header Parsing  
 * Input       :  pOfp131PortMod Pointer to the header,connection and buffer
 * Output      :  pu4Error - Error on failure 
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE 
 ****************************************************************************/
INT4
Ofc131PktPortModParse (tOfp131PortMod * pOfp131PortMod, VOID *pConnPtr,
                       tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 *pu4Error)
{
    if (Ofc131PktPortModu4PortNoValidate
        (pOfp131PortMod, pConnPtr, pBuf, pu4Error) == OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    if (Ofc131PktPortModau1PadValidate
        (pOfp131PortMod, pConnPtr, pBuf, pu4Error) == OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    if (Ofc131PktPortModau1HwAddrValidate
        (pOfp131PortMod, pConnPtr, pBuf, pu4Error) == OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    if (Ofc131PktPortModau1Pad2Validate
        (pOfp131PortMod, pConnPtr, pBuf, pu4Error) == OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    if (Ofc131PktPortModu4ConfigValidate
        (pOfp131PortMod, pConnPtr, pBuf, pu4Error) == OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    if (Ofc131PktPortModu4MaskValidate
        (pOfp131PortMod, pConnPtr, pBuf, pu4Error) == OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    if (Ofc131PktPortModu4AdvertiseValidate
        (pOfp131PortMod, pConnPtr, pBuf, pu4Error) == OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    if (Ofc131PktPortModau1Pad3Validate
        (pOfp131PortMod, pConnPtr, pBuf, pu4Error) == OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    if (Ofc131PktPortModu4PortNoAction
        (pOfp131PortMod, pConnPtr, pBuf, pu4Error) == OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    if (Ofc131PktPortModau1PadAction (pOfp131PortMod, pConnPtr, pBuf, pu4Error)
        == OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    if (Ofc131PktPortModau1HwAddrAction
        (pOfp131PortMod, pConnPtr, pBuf, pu4Error) == OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    if (Ofc131PktPortModau1Pad2Action (pOfp131PortMod, pConnPtr, pBuf, pu4Error)
        == OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    if (Ofc131PktPortModu4ConfigAction
        (pOfp131PortMod, pConnPtr, pBuf, pu4Error) == OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    if (Ofc131PktPortModu4MaskAction (pOfp131PortMod, pConnPtr, pBuf, pu4Error)
        == OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    if (Ofc131PktPortModu4AdvertiseAction
        (pOfp131PortMod, pConnPtr, pBuf, pu4Error) == OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    if (Ofc131PktPortModau1Pad3Action (pOfp131PortMod, pConnPtr, pBuf, pu4Error)
        == OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  Ofc131PktMultiPartReqParse
 * Description :  This function is for MultiPartReq Header Parsing  
 * Input       :  pOfp131MultiPartReq Pointer to the header,connection and buffer
 * Output      :  pu4Error - Error on failure 
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE 
 ****************************************************************************/
INT4
Ofc131PktMultiPartReqParse (tOfp131MultiPartReq * pOfp131MultiPartReq,
                            VOID *pConnPtr, tCRU_BUF_CHAIN_HEADER * pBuf,
                            UINT4 *pu4Error)
{
    if (Ofc131PktMultiPartRequ2TypeValidate
        (pOfp131MultiPartReq, pConnPtr, pBuf, pu4Error) == OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    if (Ofc131PktMultiPartRequ2FlagsValidate
        (pOfp131MultiPartReq, pConnPtr, pBuf, pu4Error) == OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    if (Ofc131PktMultiPartReqau1PadValidate
        (pOfp131MultiPartReq, pConnPtr, pBuf, pu4Error) == OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    if (Ofc131PktMultiPartReqau1BodyValidate
        (pOfp131MultiPartReq, pConnPtr, pBuf, pu4Error) == OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    if (Ofc131PktMultiPartRequ2TypeAction
        (pOfp131MultiPartReq, pConnPtr, pBuf, pu4Error) == OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    if (Ofc131PktMultiPartRequ2FlagsAction
        (pOfp131MultiPartReq, pConnPtr, pBuf, pu4Error) == OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    if (Ofc131PktMultiPartReqau1PadAction
        (pOfp131MultiPartReq, pConnPtr, pBuf, pu4Error) == OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    if (Ofc131PktMultiPartReqau1BodyAction
        (pOfp131MultiPartReq, pConnPtr, pBuf, pu4Error) == OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  Ofc131PktBarrierReqParse
 * Description :  This function is for BarrierReq Header Parsing  
 * Input       :  pOfp131BarrierReq Pointer to the header,connection and buffer
 * Output      :  pu4Error - Error on failure 
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE 
 ****************************************************************************/
INT4
Ofc131PktBarrierReqParse (tOfp131BarrierReq * pOfp131BarrierReq, VOID *pConnPtr,
                          tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 *pu4Error)
{
    if (Ofc131PktBarrierReqau1DummyValidate
        (pOfp131BarrierReq, pConnPtr, pBuf, pu4Error) == OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    if (Ofc131PktBarrierReqau1DummyAction
        (pOfp131BarrierReq, pConnPtr, pBuf, pu4Error) == OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  Ofc131PktQueueReqParse
 * Description :  This function is for QueueReq Header Parsing  
 * Input       :  pOfp131QueueReq Pointer to the header,connection and buffer
 * Output      :  pu4Error - Error on failure 
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE 
 ****************************************************************************/
INT4
Ofc131PktQueueReqParse (tOfp131QueueReq * pOfp131QueueReq, VOID *pConnPtr,
                        tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 *pu4Error)
{
    if (Ofc131PktQueueRequ4PortValidate
        (pOfp131QueueReq, pConnPtr, pBuf, pu4Error) == OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    if (Ofc131PktQueueReqau1PadValidate
        (pOfp131QueueReq, pConnPtr, pBuf, pu4Error) == OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    if (Ofc131PktQueueRequ4PortAction
        (pOfp131QueueReq, pConnPtr, pBuf, pu4Error) == OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    if (Ofc131PktQueueReqau1PadAction
        (pOfp131QueueReq, pConnPtr, pBuf, pu4Error) == OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  Ofc131PktRoleReqParse
 * Description :  This function is for RoleReq Header Parsing  
 * Input       :  pOfp131RoleReq Pointer to the header,connection and buffer
 * Output      :  pu4Error - Error on failure 
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE 
 ****************************************************************************/
INT4
Ofc131PktRoleReqParse (tOfp131RoleReq * pOfp131RoleReq, VOID *pConnPtr,
                       tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 *pu4Error)
{
    if (Ofc131PktRoleRequ4RoleValidate
        (pOfp131RoleReq, pConnPtr, pBuf, pu4Error) == OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    if (Ofc131PktRoleReqau1PadValidate
        (pOfp131RoleReq, pConnPtr, pBuf, pu4Error) == OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    if (Ofc131PktRoleRequ8GenIdValidate
        (pOfp131RoleReq, pConnPtr, pBuf, pu4Error) == OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    if (Ofc131PktRoleRequ4RoleAction (pOfp131RoleReq, pConnPtr, pBuf, pu4Error)
        == OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    if (Ofc131PktRoleReqau1PadAction (pOfp131RoleReq, pConnPtr, pBuf, pu4Error)
        == OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    if (Ofc131PktRoleRequ8GenIdAction (pOfp131RoleReq, pConnPtr, pBuf, pu4Error)
        == OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  Ofc131PktAsyncConfigGetParse
 * Description :  This function is for AsyncConfigGet Header Parsing  
 * Input       :  pOfp131AsyncConfigGet Pointer to the header,connection and buffer
 * Output      :  pu4Error - Error on failure 
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE 
 ****************************************************************************/
INT4
Ofc131PktAsyncConfigGetParse (tOfp131AsyncConfigGet * pOfp131AsyncConfigGet,
                              VOID *pConnPtr, tCRU_BUF_CHAIN_HEADER * pBuf,
                              UINT4 *pu4Error)
{
    if (Ofc131PktAsyncConfigGetau1DummyValidate
        (pOfp131AsyncConfigGet, pConnPtr, pBuf, pu4Error) == OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    if (Ofc131PktAsyncConfigGetau1DummyAction
        (pOfp131AsyncConfigGet, pConnPtr, pBuf, pu4Error) == OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  Ofc131PktAsyncConfigSetParse
 * Description :  This function is for AsyncConfigSet Header Parsing  
 * Input       :  pOfp131AsyncConfigSet Pointer to the header,connection and buffer
 * Output      :  pu4Error - Error on failure 
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE 
 ****************************************************************************/
INT4
Ofc131PktAsyncConfigSetParse (tOfp131AsyncConfigSet * pOfp131AsyncConfigSet,
                              VOID *pConnPtr, tCRU_BUF_CHAIN_HEADER * pBuf,
                              UINT4 *pu4Error)
{
    if (Ofc131PktAsyncConfigSetau4PktInMaskValidate
        (pOfp131AsyncConfigSet, pConnPtr, pBuf, pu4Error) == OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    if (Ofc131PktAsyncConfigSetau4PortStatusMaskValidate
        (pOfp131AsyncConfigSet, pConnPtr, pBuf, pu4Error) == OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    if (Ofc131PktAsyncConfigSetau4FlowRemovedMaskValidate
        (pOfp131AsyncConfigSet, pConnPtr, pBuf, pu4Error) == OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    if (Ofc131PktAsyncConfigSetau4PktInMaskAction
        (pOfp131AsyncConfigSet, pConnPtr, pBuf, pu4Error) == OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    if (Ofc131PktAsyncConfigSetau4PortStatusMaskAction
        (pOfp131AsyncConfigSet, pConnPtr, pBuf, pu4Error) == OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    if (Ofc131PktAsyncConfigSetau4FlowRemovedMaskAction
        (pOfp131AsyncConfigSet, pConnPtr, pBuf, pu4Error) == OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  Ofc131PktMeterModParse
 * Description :  This function is for MeterMod Header Parsing  
 * Input       :  pOfp131MeterMod Pointer to the header,connection and buffer
 * Output      :  pu4Error - Error on failure 
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE 
 ****************************************************************************/
INT4
Ofc131PktMeterModParse (tOfp131MeterMod * pOfp131MeterMod, VOID *pConnPtr,
                        tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 *pu4Error)
{
    if (Ofc131PktMeterModu2CmdValidate
        (pOfp131MeterMod, pConnPtr, pBuf, pu4Error) == OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    if (Ofc131PktMeterModu2FlagsValidate
        (pOfp131MeterMod, pConnPtr, pBuf, pu4Error) == OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    if (Ofc131PktMeterModu4MeterIdValidate
        (pOfp131MeterMod, pConnPtr, pBuf, pu4Error) == OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    if (Ofc131PktMeterModOfpMeterBandHdrValidate
        (pOfp131MeterMod, pConnPtr, pBuf, pu4Error) == OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    if (Ofc131PktMeterModu2CmdAction (pOfp131MeterMod, pConnPtr, pBuf, pu4Error)
        == OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    if (Ofc131PktMeterModu2FlagsAction
        (pOfp131MeterMod, pConnPtr, pBuf, pu4Error) == OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    if (Ofc131PktMeterModu4MeterIdAction
        (pOfp131MeterMod, pConnPtr, pBuf, pu4Error) == OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    if (Ofc131PktMeterModOfpMeterBandHdrAction
        (pOfp131MeterMod, pConnPtr, pBuf, pu4Error) == OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  Ofc131PktParse
 * Description :  This function is for Module Pkt Processing entry function 
 * Input       :  Pointer to pBuf
 * Output      :  if any error, output Error code  
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE 
 ****************************************************************************/
INT4
Ofc131PktParse (tCRU_BUF_CHAIN_HEADER * pBuf, VOID *pConnPtr)
{
    UINT4               u4Error = 0;
    tOfp131Pkt          Ofp;

    MEMSET (&Ofp, OFC_ZERO, OFC131_HELLO_OFFSET);

    OFC131_GET_1_BYTE (pBuf, OFC131_OFP_U1VERSION_OFFSET, Ofp.u1Version);
    OFC131_GET_1_BYTE (pBuf, OFC131_OFP_U1TYPE_OFFSET, Ofp.u1Type);
    OFC131_GET_2_BYTE (pBuf, OFC131_OFP_U2LENGTH_OFFSET, Ofp.u2Length);
    OFC131_GET_4_BYTE (pBuf, OFC131_OFP_U4XID_OFFSET, Ofp.u4Xid);
    if (Ofc131PktOfpParse (&Ofp, pConnPtr, pBuf, &u4Error) == OSIX_FAILURE)
    {
        Ofc131PktErrorHandle (u4Error, pConnPtr, pBuf);
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}
