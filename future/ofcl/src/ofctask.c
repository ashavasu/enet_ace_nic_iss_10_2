/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: ofctask.c,v 1.1.1.1 2013/09/14 10:21:31 siva Exp $
 * Description:This file contains procedures related to
 *             OFC - Task Initialization
 *******************************************************************/

#include "ofcinc.h"
#include "ofcl.h"

/*****************************************************************************/
/*                                                                           */
/* Function     : OfcTaskSpawnOfcTask()                                    */
/*                                                                           */
/* Description  : This procedure is provided to Spawn Ofc Task              */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSIX_SUCCESS, if OFC Task is successfully spawned         */
/*                OSIX_FAILURE, otherwise                                    */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
OfcTaskSpawnOfcTask (INT1 *pi1Arg)
{

    UNUSED_PARAM (pi1Arg);
    OFC_TRC_FUNC ((OFC_FN_ENTRY, "FUNC:OfcTaskSpawnOfcTask\n"));

    /* task initializations */
    if (OfcMainTaskInit () == OSIX_FAILURE)
    {
        OFC_TRC ((OFC_TASK_TRC, " !!!!! OFC TASK INIT FAILURE  !!!!! \n"));
        OfcMainDeInit ();
        lrInitComplete (OSIX_FAILURE);
        return;
    }

    /* spawn ofc task */

    if (OsixGetTaskId (SELF, (UINT1 *) OFC_TASK_NAME, &(gOfcGlobals.ofcTaskId))
       == OSIX_FAILURE)
       {
        OFC_TRC ((OFC_TASK_TRC, " !!!!! Unable to get OFC Task ID  !!!!! \n"));
        OfcMainDeInit ();
        lrInitComplete (OSIX_FAILURE);
        return ;
    }

    OfcTaskRegisterOfcMibs ();
    lrInitComplete (OSIX_SUCCESS);
    OfcMainTask ();
    OFC_TRC_FUNC ((OFC_FN_EXIT, "EXIT:OfcTaskSpawnOfcTask\n"));
    return;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : OfcTaskRegisterOfcMibs()                                   */
/*                                                                           */
/* Description  : This procedure is provided to register Open Flow mibs      */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSIX_SUCCESS/OSIX_FAILURE                                  */
/*****************************************************************************/
PUBLIC INT4 OfcTaskRegisterOfcMibs ()
{
    RegisterFSOFC ();
    RegisterFSOFC ();
    return OSIX_SUCCESS;
}

/*------------------------------------------------------------------------*/
/*                        End of the file  ofctask.c                     */
/*------------------------------------------------------------------------*/
