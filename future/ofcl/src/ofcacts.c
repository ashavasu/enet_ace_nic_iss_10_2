/****************************************************************************
* Copyright (C) 2013 Aricent Inc . All Rights Reserved
*
* $Id: ofcacts.c,v 1.5 2014/08/14 11:11:22 siva Exp $
*
* Description: This file contains the routines for Pipeline Processing 
******************************************************************************/

#include"cfa.h"
#include"ofcacts.h"
#include"ofcsz.h"
#include"ofcflow.h"
#include"ofcpkt.h"
#include"ofcapi.h"

PRIVATE INT4        OfcFlowExtract (UINT1 *pu1Pkt, UINT4 u4IfIndex,
                                    tOfcFlowMatch * flow);
extern INT4         CfaGddEthWrite (UINT1 *pu1Buf, UINT2 u2IfIndex,
                                    UINT4 u4Size);

UINT1               gau1DataBuf[OFC_MAX_DATA_FRAME_SIZE];    /* To extract key from CRU Buf */
UINT1               gau1PktInBuf[OFC_MAX_DATA_FRAME_SIZE];    /* For data to PktIn Message */
static UINT1        gau1BroadcastMac[CFA_ENET_ADDR_LEN] =
    { OFC_ALL_ONE, OFC_ALL_ONE, OFC_ALL_ONE, OFC_ALL_ONE, OFC_ALL_ONE,
    OFC_ALL_ONE
};

/******************************************************************************
 * Function    :  OfcFlowExtract
 * Description :  This routine extracts the Flow Match fields from the
 *                Flow Entry.
 *
 * Input       :  pu1Pkt    - Pointer to the received packet buffer.
 *                u4IfIndex - Interface Index.
 * Output      :  pFlow     - Pointer to the Flow Match Structure.
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE
 *****************************************************************************/
PRIVATE INT4
OfcFlowExtract (UINT1 *pu1Pkt, UINT4 u4IfIndex, tOfcFlowMatch * pFlow)
{
    tEnetV2Header      *pEthHdr = NULL;
    t_IP_HEADER        *pIpHdr = NULL;
    tIp6Hdr            *pIp6Hdr = NULL;
    tArpHdr            *pArpHdr = NULL;
    tTcpHdr            *pTcpHdr = NULL;
    tUdpHdr            *pUdpHdr = NULL;
    tIcmpHdr           *pIcmpHdr = NULL;

    OFC_TRC_FUNC ((OFC_FN_ENTRY, "FUNC:OfcFlowExtract\n"));

    pFlow->u4InIfIndex = u4IfIndex;

    /* Link layer. */
    pEthHdr = (tEnetV2Header *) pu1Pkt;
    MEMCPY (pFlow->au1DstMacAddr, pEthHdr->au1DstAddr, CFA_ENET_ADDR_LEN);
    MEMCPY (pFlow->au1SrcMacAddr, pEthHdr->au1SrcAddr, CFA_ENET_ADDR_LEN);

    pFlow->u2EthType = OSIX_NTOHS (pEthHdr->u2LenOrType);

    /* Network layer. */
    if (pFlow->u2EthType == CFA_ENET_IPV4)
    {
        pIpHdr = (t_IP_HEADER *) ((VOID *) (pu1Pkt + sizeof (tEnetV2Header)));

        if (pIpHdr)
        {
            pFlow->u4IpSrc = OSIX_NTOHL (pIpHdr->u4Src);
            pFlow->u4IpDst = OSIX_NTOHL (pIpHdr->u4Dest);
            pFlow->u1IpProto = pIpHdr->u1Proto;

            if (!(pIpHdr->u2Fl_offs & OSIX_HTONS (IP_FRAG_OFF_MASK)))
            {
                if (pFlow->u1IpProto == CFA_TCP)
                {
                    pTcpHdr = (tTcpHdr *) ((VOID *) (pu1Pkt +
                                                     sizeof (tEnetV2Header) +
                                                     CFA_IP_HDR_LEN));
                    pFlow->u2TpSrcPort = OSIX_NTOHS (pTcpHdr->u2Src);
                    pFlow->u2TpDstPort = OSIX_NTOHS (pTcpHdr->u2Dst);
                }
                else if (pFlow->u1IpProto == CFA_UDP)
                {
                    pUdpHdr = (tUdpHdr *) ((VOID *) (pu1Pkt +
                                                     sizeof (tEnetV2Header) +
                                                     CFA_IP_HDR_LEN));
                    pFlow->u2TpSrcPort = OSIX_NTOHS (pUdpHdr->u2Src);
                    pFlow->u2TpDstPort = OSIX_NTOHS (pUdpHdr->u2Dst);
                }
                else if (pFlow->u1IpProto == CFA_ICMP)
                {
                    pIcmpHdr = (tIcmpHdr *) ((VOID *) (pu1Pkt +
                                                       sizeof (tEnetV2Header) +
                                                       CFA_IP_HDR_LEN));
                    /* Fill in the TCP Fields itself */
                    pFlow->u2TpSrcPort = pIcmpHdr->u1Type;
                    pFlow->u2TpDstPort = pIcmpHdr->u1Code;
                }
            }
        }
    }
    else if (pFlow->u2EthType == CFA_ENET_IPV6)
    {
        pIp6Hdr = (tIp6Hdr *) ((VOID *) (pu1Pkt + sizeof (tEnetV2Header)));

        if (pIp6Hdr)
        {
            MEMCPY (pFlow->au1Ip6Src.u1_addr,
                    pIp6Hdr->srcAddr.u1_addr, IP6_ADDR_SIZE);
            MEMCPY (pFlow->au1Ip6Dst.u1_addr,
                    pIp6Hdr->dstAddr.u1_addr, IP6_ADDR_SIZE);

            pFlow->u1IpProto = pIp6Hdr->u1Nh;

            if (pFlow->u1IpProto == CFA_TCP)
            {
                pTcpHdr = (tTcpHdr *) ((VOID *) (pu1Pkt +
                                                 sizeof (tEnetV2Header) +
                                                 sizeof (tIp6Hdr)));
                pFlow->u2TpSrcPort = OSIX_NTOHS (pTcpHdr->u2Src);
                pFlow->u2TpDstPort = OSIX_NTOHS (pTcpHdr->u2Dst);
            }
            else if (pFlow->u1IpProto == CFA_UDP)
            {
                pUdpHdr = (tUdpHdr *) ((VOID *) (pu1Pkt +
                                                 sizeof (tEnetV2Header) +
                                                 sizeof (tIp6Hdr)));
                pFlow->u2TpSrcPort = OSIX_NTOHS (pUdpHdr->u2Src);
                pFlow->u2TpDstPort = OSIX_NTOHS (pUdpHdr->u2Dst);
            }
            else if (pFlow->u1IpProto == NH_ICMP6)
            {
                pIcmpHdr = (tIcmpHdr *) ((VOID *) (pu1Pkt +
                                                   sizeof (tEnetV2Header) +
                                                   sizeof (tIp6Hdr)));
                /* Fill in the TCP Fields itself */
                pFlow->u2TpSrcPort = pIcmpHdr->u1Type;
                pFlow->u2TpDstPort = pIcmpHdr->u1Code;
            }
        }
    }
    else if (pFlow->u2EthType == CFA_ENET_ARP)
    {
        UINT1              *pu1Data = NULL;
        pArpHdr = (tArpHdr *) ((VOID *) (pu1Pkt + sizeof (tEnetV2Header)));
        pu1Data = (UINT1 *) pArpHdr;
        pFlow->u1IpProto = (UINT1) OSIX_NTOHS (pArpHdr->u2OpCode);
        MEMCPY (&(pFlow->u4IpSrc), pu1Data + sizeof (tArpHdr) + OFC_SIX,
                OFC_FOUR);
        MEMCPY (&(pFlow->u4IpDst),
                pu1Data + sizeof (tArpHdr) + OFC_SIX + OFC_FOUR + OFC_SIX,
                OFC_FOUR);
        pFlow->u4IpSrc = OSIX_NTOHL (pFlow->u4IpSrc);
        pFlow->u4IpDst = OSIX_NTOHL (pFlow->u4IpDst);
    }
    else if (pFlow->u2EthType == OFC_DISCOVERY)
    {
        /* discovery packet allow to go to controller */
    }
    else if (pFlow->u2EthType == OFC_LLDP)
    {
        /* LLDP packet allow to go to controller */
    }
    else
    {
        /* Unsupported Header */
        OFC_TRC_FUNC ((OFC_ACTS_TRC, "Unsupported Header, Type = %d, "
                       "return FAILURE\n", pFlow->u2EthType));
        return OFC_FAILURE;
    }

    OFC_TRC_FUNC ((OFC_FN_EXIT, "FUNC:OfcFlowExtract SUCCESS\n"));
    return OFC_SUCCESS;
}

/******************************************************************************
 * Function    :  OfcPipelineProcess
 * Description :  This routine performs the pipeline processing on the received
 *                data packet. If there is a matching flow entry in the flow
 *                table, the corresponding actions are executed. If no match
 *                is found, the packet is sent to the controller or dropped.
 *
 * Input       :  u4IfIndex - Interface Index.
 *                pBuf      - Pointer to the received packet buffer.
 * Output      :  None
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE
 *****************************************************************************/
INT4
OfcPipelineProcess (UINT4 u4IfIndex, UINT4 u4ContextId,
                    tCRU_BUF_CHAIN_HEADER * pBuf)
{
    tOfcFlowMatch       Key;
    tOfcFlowEntry      *pFlow = NULL;
    tOfcQueMsg         *pQueMsg = NULL;
    tOfcActHdr         *pActHdr = NULL;
    tOfcActOutPort     *pOutPort = NULL;
    tOfcActDlAddr      *pDlSrc = NULL;
    tOfcActDlAddr      *pDlDst = NULL;
    tOfcFsofcIfEntry    lOfcFsofcIfEntry;
    tOfcFsofcIfEntry   *pOfcFsofcIfEntry = NULL;
    tOfcFsofcCfgEntry  *pOfcFsofcCfgEntry = NULL;
    tCfaIfInfo          CfaIfInfo;
    UINT4               u4PktSize = OFC_ZERO;
    UINT1               u1OperStatus = OFC_ZERO;
    INT4                i4Acts = OFC_ZERO;
    INT4                i4NumActs = OFC_ZERO;
    UINT2               u2OutPort = OFC_INVALID_PORT;

    OFC_TRC_FUNC ((OFC_FN_ENTRY, "FUNC:OfcPipelineProcess\n"));

    MEMSET (gau1DataBuf, OFC_ZERO, sizeof (gau1DataBuf));
    MEMSET (&lOfcFsofcIfEntry.MibObject, OFC_ZERO,
            sizeof (lOfcFsofcIfEntry.MibObject));
    if (CfaGetIfInfo (u4IfIndex, &CfaIfInfo) == CFA_FAILURE)
    {
        return OFC_FAILURE;
    }

    if ((CfaIfInfo.u1IfType == CFA_L2VLAN)
        && (CfaIfInfo.u1IfSubType == CFA_SUBTYPE_OPENFLOW_INTERFACE))
    {
        /* Update Vlan Statistics */
        lOfcFsofcIfEntry.MibObject.u4FsofcContextId = u4ContextId;
        lOfcFsofcIfEntry.MibObject.i4FsofcIfIndex = (INT4) (u4IfIndex);
        pOfcFsofcIfEntry = RBTreeGet (gOfcGlobals.OfcGlbMib.FsofcIfTable,
                                      (tRBElem *) & (lOfcFsofcIfEntry));

        if (pOfcFsofcIfEntry != NULL)
        {
            OFC_TRC_FUNC ((OFC_ACTS_TRC, "No Entry available in OfcIfTable\n"));
            lOfcFsofcIfEntry.MibObject.u4FsofcVlanInFrames++;
        }
    }
    /* Get the IF operstatus */
    u1OperStatus = CfaIfInfo.u1IfOperStatus;

    if (u1OperStatus == CFA_IF_DOWN)
    {
        OFC_TRC_FUNC ((OFC_ACTS_TRC, "Input IF Down, return FAILURE\n"));
        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
        CfaIfSetInDiscard (u4IfIndex);
        return OFC_FAILURE;
    }

    u4PktSize = CRU_BUF_Get_ChainValidByteCount (pBuf);
    if (CRU_BUF_Copy_FromBufChain (pBuf, gau1DataBuf,
                                   OFC_ZERO, u4PktSize) == CRU_FAILURE)
    {
        OFC_TRC_FUNC ((OFC_ACTS_TRC, "CRU Copy Failed, return FAILURE\n"));
        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
        CfaIfSetInDiscard (u4IfIndex);
        return OFC_FAILURE;
    }

    /* Extract pFlow, from pBuf into key */
    MEMSET (&Key, OFC_ZERO, sizeof (Key));
    if (OfcFlowExtract (gau1DataBuf, u4IfIndex, &Key) == OFC_FAILURE)
    {
        OFC_TRC_FUNC ((OFC_ACTS_TRC, "Flow Extract Failed, return FAILURE\n"));
        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
        CfaIfSetInDiscard (u4IfIndex);
        return OFC_FAILURE;
    }

    pOfcFsofcCfgEntry = OfcGetFsofcCfgEntry (u4ContextId);
    if (pOfcFsofcCfgEntry == NULL)
    {
        OFC_TRC_FUNC ((OFC_ACTS_TRC, "\r\nOfcPipelineProccessing: "
                       "Get CFG Entry Failed\n"));
        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
        CfaIfSetInDiscard (u4IfIndex);
        return OFC_FAILURE;
    }

    /* Table lookup to get the matching pFlow entry */
    pFlow = (tOfcFlowEntry *) OfcFlowLookup (pOfcFsofcCfgEntry, &Key);

    /* Execute the actions specified in the pFlow */
    if (pFlow != NULL)
    {
        /* Update the counters */
        pFlow->u4PktCount = pFlow->u4PktCount + OFC_ONE;
        pFlow->u4ByteCount = pFlow->u4ByteCount + u4PktSize;

        /* Update the Counters in MibObject */
        OfcUpdateFlowEntryMibObjectStats (pFlow);

        /* Re-Starting the Timers */
        if (pFlow->u2IdleTimeout != OFC_ZERO)
        {
            OfcTmrRestartTmr (&(pFlow->OfcIdleTimer), OFC_IDLE_TIMER,
                              pFlow->u2IdleTimeout);
        }

        i4NumActs = (INT4) pFlow->u4NumActs;
        for (i4Acts = OFC_ZERO; i4Acts < i4NumActs; i4Acts++)
        {
            pActHdr = (tOfcActHdr *) ((VOID *) (&(pFlow->OfcActs[i4Acts])));
            switch (pActHdr->u2Type)
            {
                case OFPAT_OUTPUT:
                    pOutPort = (tOfcActOutPort *) (&(pFlow->OfcActs[i4Acts]));
                    u2OutPort = pOutPort->u2OutPort;
                    /* Note: As of now there is no buffering mechanism to
                     * to consider the max length for sending the packet to
                     * controller. We will be sending full packet.
                     */
                    break;

                case OFPAT_SET_DL_SRC:
                    pDlSrc = (tOfcActDlAddr *) (&(pFlow->OfcActs[i4Acts]));
                    CRU_BUF_Copy_OverBufChain (pBuf, pDlSrc->au1DlAddr,
                                               CFA_ENET_ADDR_LEN,
                                               CFA_ENET_ADDR_LEN);
                    break;

                case OFPAT_SET_DL_DST:
                    pDlDst = (tOfcActDlAddr *) (&(pFlow->OfcActs[i4Acts]));
                    CRU_BUF_Copy_OverBufChain (pBuf, pDlDst->au1DlAddr,
                                               OFC_ZERO, CFA_ENET_ADDR_LEN);
                    break;

                default:
                    /* Unsupported action */
                    OFC_TRC_FUNC ((OFC_ACTS_TRC,
                                   "Unsupported Action, Type = %d, "
                                   "return FAILURE\n", pActHdr->u2Type));
                    CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
                    CfaIfSetInDiscard (u4IfIndex);
                    return OFC_FAILURE;
            }
        }
    }
    else if (MEMCMP (Key.au1DstMacAddr, gau1BroadcastMac, CFA_ENET_ADDR_LEN) ==
             OFC_ZERO)
    {
        pOfcFsofcIfEntry = OfcGetFirstFsofcIfTable ();
        while (pOfcFsofcIfEntry != NULL)
        {
            CfaGddEthWrite (gau1DataBuf,
                            (UINT2) pOfcFsofcIfEntry->MibObject.i4FsofcIfIndex,
                            u4PktSize);
            pOfcFsofcIfEntry = OfcGetNextFsofcIfTable (pOfcFsofcIfEntry);
        }
        OFC_TRC_FUNC ((OFC_FN_EXIT, "FUNC:OfcPipelineProcess SUCCESS\n"));
        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
        return OFC_SUCCESS;
    }

    if (u2OutPort != (UINT2) OFC_INVALID_PORT)
    {
        if (u2OutPort == OFC_CONTROLLER)
        {
            pQueMsg = (tOfcQueMsg *) MemAllocMemBlk (OFC_QUEUE_POOLID);

            if (pQueMsg == NULL)
            {
                /* MemPool Allocation failed */
                OFC_TRC_FUNC ((OFC_ACTS_TRC, "MemPool Allocated Failed, "
                               "return FAILURE\n"));
                CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
                CfaIfSetInDiscard (u4IfIndex);
                return OFC_FAILURE;
            }
            pQueMsg->u4MsgType = OFCR_ACTION;
            pQueMsg->u4ContextId = u4ContextId;
            pQueMsg->u4IfIndex = u4IfIndex;
            pQueMsg->pBuf = pBuf;

            /* Post The Message to Queue */
            if (OfcQueSendMsg (pQueMsg) == OSIX_FAILURE)
            {
                OFC_TRC_FUNC ((OFC_ACTS_TRC, "Posting to Queue Failed, "
                               "return FAILURE\n"));
                /* Just return, buffers are already released */
                CfaIfSetInDiscard (u4IfIndex);
                return OSIX_FAILURE;
            }
        }
        else
        {
            /* Check whether the output port is up */
            CfaGetIfOperStatus (u4IfIndex, &(u1OperStatus));

            if (u1OperStatus == CFA_IF_DOWN)
            {
                OFC_TRC_FUNC ((OFC_ACTS_TRC, "Output IF Down, "
                               "return FAILURE\n"));
                CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
                CfaIfSetInDiscard (u4IfIndex);
                return OFC_FAILURE;
            }

            MEMSET (gau1DataBuf, OFC_ZERO, sizeof (gau1DataBuf));
            if (CRU_BUF_Copy_FromBufChain (pBuf, gau1DataBuf,
                                           OFC_ZERO, u4PktSize) == CRU_FAILURE)
            {
                OFC_TRC_FUNC ((OFC_ACTS_TRC, "CRU Copy Failed, "
                               "return FAILURE\n"));
                CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
                CfaIfSetInDiscard (u4IfIndex);
                return OFC_FAILURE;
            }

            CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
            CfaGddEthWrite (gau1DataBuf, u2OutPort, u4PktSize);
        }
    }
    else
    {                            /* No Output Action specified */

        /* Get the default pFlow miss behavior from SNMP */
        if (pOfcFsofcCfgEntry->MibObject.i4FsofcDefaultFlowMissBehaviour ==
            OFC_TWO)
        {
            pQueMsg = (tOfcQueMsg *) MemAllocMemBlk (OFC_QUEUE_POOLID);

            if (pQueMsg == NULL)
            {
                /* MemPool Allocation failed */
                OFC_TRC_FUNC ((OFC_ACTS_TRC, "MemPool Allocated Failed, "
                               "return FAILURE\n"));
                CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
                CfaIfSetInDiscard (u4IfIndex);
                return OFC_FAILURE;
            }
            pQueMsg->u4MsgType = OFCR_NO_MATCH;
            pQueMsg->u4ContextId = u4ContextId;
            pQueMsg->u4IfIndex = u4IfIndex;
            pQueMsg->pBuf = pBuf;

            /* 
             * If the default switch mode on connection failure is fail secure 
             * and client connection state is in disconnected state, 
             * then discard the packets 
             */
            if ((pOfcFsofcCfgEntry->u4OfcClientState == OFC_NOT_CONNECTED) &&
                (pOfcFsofcCfgEntry->MibObject.i4FsofcSwitchModeOnConnFailure ==
                 OFC_SWITCHMODE_FAIL_SECURE))
            {
                CfaIfSetInDiscard (u4IfIndex);
                OFC_TRC_FUNC ((OFC_ACTS_TRC, "Drop the Packet, "
                               "return FAILURE\n"));
                CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
                MemReleaseMemBlock (OFC_QUEUE_POOLID, (UINT1 *) pQueMsg);
                return OFC_SUCCESS;
            }

            /* Post the Message to Queue */
            if (OfcQueSendMsg (pQueMsg) == OSIX_FAILURE)
            {
                OFC_TRC_FUNC ((OFC_ACTS_TRC, "Posting to Queue Failed, "
                               "return FAILURE\n"));
                /* Just return, buffers are already released */
                CfaIfSetInDiscard (u4IfIndex);
                return OSIX_FAILURE;
            }
        }
        else
        {                        /* Drop the Packet */
            OFC_TRC_FUNC ((OFC_ACTS_TRC, "Drop the Packet, return FAILURE\n"));
            CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
            CfaIfSetInDiscard (u4IfIndex);
            return OFC_FAILURE;
        }
    }
    OFC_TRC_FUNC ((OFC_FN_EXIT, "FUNC:OfcPipelineProcess SUCCESS\n"));
    return OFC_SUCCESS;
}

/******************************************************************************
 * Function    :  OfcSendPktIn
 * Description :  This routine sends the Pkt In message
 *
 * Input       :  u4IfIndex - Interface Index.
 *                pBuf      - Pointer to the received packet buffer.
 *                pOfcFsofcControllerConnEntry - Pointer to Cntlr Entry
 * Output      :  None
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE
 *****************************************************************************/
INT4
OfcSendPktIn (UINT4 u4IfIndex, UINT4 u4MsgType, tCRU_BUF_CHAIN_HEADER * pBuf,
              tOfcFsofcControllerConnEntry * pOfcFsofcControllerConnEntry)
{
    tOfpPktIn          *pPktIn = NULL;
    UINT4               u4PktSize = OFC_ZERO;

    OFC_TRC_FUNC ((OFC_FN_ENTRY, "FUNC:OfcSendPktIn\n"));

    MEMSET (gau1PktInBuf, OFC_ZERO, sizeof (gau1PktInBuf));
    u4PktSize = CRU_BUF_Get_ChainValidByteCount (pBuf);

    pPktIn = (tOfpPktIn *) ((VOID *) gau1PktInBuf);
    pPktIn->u4BufId = OFC_NO_BUFFER;    /* Sending full packet for now */
    pPktIn->u2TotLen = (UINT2) u4PktSize;
    pPktIn->u1Reason = (UINT1) u4MsgType;
    pPktIn->u2InPort = (UINT2) u4IfIndex;

    if (CRU_BUF_Copy_FromBufChain (pBuf, pPktIn->au1Data,
                                   OFC_ZERO, u4PktSize) == CRU_FAILURE)
    {
        OFC_TRC_FUNC ((OFC_ACTS_TRC, "CRU Copy Failed, return FAILURE\n"));
        return OFC_FAILURE;
    }

    OfcPktInPktSend (pPktIn, pOfcFsofcControllerConnEntry);

    OFC_TRC_FUNC ((OFC_FN_EXIT, "FUNC:OfcSendPktIn SUCCESS\n"));
    return OFC_SUCCESS;
}

/*******************************************************************************
 * Function    :  OfcParseStructToStr
 * Description :  This routine parses the Flow Entry to form a string
 *                of actions which are present in the Flow Entry.
 *
 * Input       :  pFlowEntry - Flow Entry.
 * Output      :  pStr       - String of actions in the Flow Entry
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE
 ******************************************************************************/
INT4
OfcParseStructToStr (CHR1 * pStr, tOfcFlowEntry * pFlowEntry)
{
    unOfcActs          *pOfcActs = NULL;
    tOfcActHdr         *pActHdr = NULL;
    INT4                i4Acts = OFC_ZERO;
    INT4                i4NumActs = OFC_ZERO;
    INT4                i4Index = OFC_ZERO;
    CHR1                au1Buff[OFC_TWO_FIFTY_SIX];

    OFC_TRC_FUNC ((OFC_FN_ENTRY, "FUNC:OfcParseStructToStr\n"));

    MEMSET (au1Buff, OFC_ZERO, sizeof (au1Buff));
    i4NumActs = (INT4) pFlowEntry->u4NumActs;

    for (i4Acts = OFC_ZERO; i4Acts < i4NumActs; i4Acts++)
    {
        pOfcActs = (unOfcActs *) (&(pFlowEntry->OfcActs[i4Acts]));
        MEMSET (au1Buff, OFC_ZERO, sizeof (au1Buff));
        pActHdr = (tOfcActHdr *) pOfcActs;

        switch (pActHdr->u2Type)
        {
            case OFPAT_OUTPUT:
                STRNCAT (pStr, "OUTPUT_PORT=", OFC_TWELVE);
                SNPRINTF (au1Buff, sizeof (au1Buff), "%u",
                          pOfcActs->ActOutport.u2OutPort);
                SNPRINTF (pStr + strlen (pStr), sizeof (au1Buff), "%s",
                          au1Buff);
                break;

            case OFPAT_SET_DL_SRC:
                STRNCAT (pStr, "SET_DL_SRC=", OFC_ELEVEN);
                i4Index = OFC_ZERO;

                while (i4Index < CFA_ENET_ADDR_LEN)
                {
                    MEMSET (au1Buff, OFC_ZERO, sizeof (au1Buff));
                    SNPRINTF (au1Buff, sizeof (au1Buff), "%u",
                              pOfcActs->ActDlAddr.au1DlAddr[i4Index]);
                    SNPRINTF (pStr + strlen (pStr), sizeof (au1Buff), "%s",
                              au1Buff);

                    /* MAC Address Format */
                    if (i4Index < (CFA_ENET_ADDR_LEN - OFC_ONE))
                    {
                        STRNCAT (pStr, ":", OFC_ONE);
                    }
                    i4Index++;
                }
                break;

            case OFPAT_SET_DL_DST:
                STRNCAT (pStr, "SET_DL_DST=", OFC_ELEVEN);
                i4Index = OFC_ZERO;

                while (i4Index < CFA_ENET_ADDR_LEN)
                {
                    MEMSET (au1Buff, OFC_ZERO, sizeof (au1Buff));
                    SNPRINTF (au1Buff, sizeof (au1Buff), "%u",
                              pOfcActs->ActDlAddr.au1DlAddr[i4Index]);
                    SNPRINTF (pStr + strlen (pStr), sizeof (au1Buff), "%s",
                              au1Buff);

                    /* MAC Address Format */
                    if (i4Index < (CFA_ENET_ADDR_LEN - OFC_ONE))
                    {
                        STRNCAT (pStr, ":", OFC_ONE);
                    }
                    i4Index++;
                }
                break;

            default:
                /* Unsupported Action */
                OFC_TRC_FUNC ((OFC_ACTS_TRC, "Unsupported Action, Type = %d "
                               "return FAILURE\n", pActHdr->u2Type));
                return OFC_FAILURE;
        }

        /* Add ',' after each action */
        if (i4Acts != (i4NumActs - OFC_ONE))
        {
            STRNCAT (pStr, ",", OFC_ONE);
        }
    }
    OFC_TRC_FUNC ((OFC_FN_EXIT, "FUNC:OfcParseStructToStr SUCCESS\n"));
    return OFC_SUCCESS;
}
