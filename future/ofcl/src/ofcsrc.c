/********************************************************************
* Copyright (C) 2013 Aricent Inc . All Rights Reserved
*
* $Id: ofcsrc.c,v 1.3 2014/07/21 11:36:16 siva Exp $
*
* Description: This file holds the functions for the ShowRunningConfig
*              for Ofc module 
*********************************************************************/

#include "ofcinc.h"
#include "ofcsrc.h"
#include "ofcsrcdefn.h"
#include "utilcli.h"

/****************************************************************************
 Function    : OfcShowRunningConfig
 Description : This function displays the current configuration
                of OFC module
 Input       : CliHandle - Handle to the cli context
                u4Module - Specified module for configuration 
 Output      : None
 Returns     : CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
OfcShowRunningConfig (tCliHandle CliHandle, UINT4 u4Module, ...)
{
    CliRegisterLock (CliHandle, OfcMainTaskLock, OfcMainTaskUnLock);
    OFC_LOCK;

    CliPrintf (CliHandle, "!\r\n");
    OfcShowRunningConfigScalar (CliHandle, u4Module);
    OfcShowRunningConfigFsofcCfgTable (CliHandle, u4Module);
    OfcShowRunningConfigFsofcControllerConnTable (CliHandle, u4Module);

    CliUnRegisterLock (CliHandle);
    OFC_UNLOCK;
    return CLI_SUCCESS;
}

/****************************************************************************
 Function    : OfcShowRunningConfigScalar
 Description : This function displays the current configuration
                of the OFC scalars
 Input       : CliHandle - Handle to the cli context
                u4Module - Specified module for configuration 
 Output      : None
 Returns     : CLI_SUCCESS always
****************************************************************************/
INT4
OfcShowRunningConfigScalar (tCliHandle CliHandle, UINT4 u4Module, ...)
{
    UNUSED_PARAM (CliHandle);
    UNUSED_PARAM (u4Module);
    return CLI_SUCCESS;
}

/****************************************************************************
 Function    : OfcShowRunningConfigFsofcCfgTable
 Description : This function displays the current configuration
                of FsofcCfgTable table
 Input       : CliHandle - Handle to the cli context
                u4Module - Specified module for configuration
 Output      : None
 Returns     : CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
OfcShowRunningConfigFsofcCfgTable (tCliHandle CliHandle, UINT4 u4Module, ...)
{
    tOfcFsofcCfgEntry   OfcFsofcCfgEntry;
    UINT4               u4FsofcContextId = OFC_SRC_ZERO;
    UINT4               u4NextFsofcContextId = OFC_SRC_ZERO;

    MEMSET (&OfcFsofcCfgEntry, OFC_SRC_ZERO, sizeof (tOfcFsofcCfgEntry));

    if (SNMP_SUCCESS != nmhGetFirstIndexFsofcCfgTable (&u4FsofcContextId))
    {
        return CLI_SUCCESS;
    }

    OfcFsofcCfgEntry.MibObject.u4FsofcContextId = u4FsofcContextId;

    while (OFC_SRC_ONE)
    {
        if (OfcGetAllFsofcCfgTable (&OfcFsofcCfgEntry) != OSIX_SUCCESS)
        {
            return CLI_SUCCESS;
        }

        if (OfcFsofcCfgEntry.MibObject.i4FsofcModuleStatus
            != OFC_DEF_FSOFCMODULESTATUS)
        {
            CliPrintf (CliHandle, "ofc context-id %d ", u4FsofcContextId);
            CliPrintf (CliHandle,
                       "module-status %s ",
                       ((OfcFsofcCfgEntry.MibObject.
                         i4FsofcModuleStatus != OFC_ZERO) ?
                        ((OfcFsofcCfgEntry.MibObject.i4FsofcModuleStatus ==
                          OFC_SRC_ONE) ? "l2" : "l3") : "disable"));

        }
        if (OfcFsofcCfgEntry.MibObject.i4FsofcDefaultFlowMissBehaviour
            != OFC_DEF_FSOFCDEFAULTFLOWMISSBEHAVIOUR)
        {
            CliPrintf (CliHandle, "\r\nofc context-id %d ", u4FsofcContextId);
            CliPrintf (CliHandle,
                       "dflt-flow-miss-action %s ",
                       ((OfcFsofcCfgEntry.MibObject.
                         i4FsofcDefaultFlowMissBehaviour ==
                         OFC_SRC_ONE) ? "drop" : "sendToController"));
        }
        if (OfcFsofcCfgEntry.MibObject.i4FsofcControlPktBuffering
            != OFC_DEF_FSOFCCONTROLPKTBUFFERING)
        {
            CliPrintf (CliHandle, "\r\nofc context-id %d ", u4FsofcContextId);
            CliPrintf (CliHandle,
                       "ctrlpkt-buffering %s ",
                       ((OfcFsofcCfgEntry.MibObject.
                         i4FsofcControlPktBuffering ==
                         OFC_SRC_ONE) ? "enable" : "disable"));

        }
        if (OfcFsofcCfgEntry.MibObject.i4FsofcIpReassembleStatus
            != OFC_DEF_FSOFCIPREASSEMBLESTATUS)
        {
            CliPrintf (CliHandle, "\r\nofc context-id %d ", u4FsofcContextId);
            CliPrintf (CliHandle,
                       "ip-reassemble %s ",
                       ((OfcFsofcCfgEntry.MibObject.
                         i4FsofcIpReassembleStatus ==
                         OFC_SRC_ONE) ? "enable" : "disable"));

        }
        if (OfcFsofcCfgEntry.MibObject.i4FsofcPortStpStatus
            != OFC_DEF_FSOFCPORTSTPSTATUS)
        {
            CliPrintf (CliHandle, "\r\nofc context-id %d ", u4FsofcContextId);
            CliPrintf (CliHandle,
                       "stp-status %s ",
                       ((OfcFsofcCfgEntry.MibObject.
                         i4FsofcPortStpStatus ==
                         OFC_SRC_ONE) ? "enable" : "disable"));

        }
        if (OfcFsofcCfgEntry.MibObject.i4FsofcSwitchModeOnConnFailure
            != OFC_DEF_FSOFCSWITCHMODEONCONNFAILURE)
        {
            CliPrintf (CliHandle, "\r\nofc context-id %d ", u4FsofcContextId);
            CliPrintf (CliHandle,
                       "switchMode-onConnFailure %s ",
                       ((OfcFsofcCfgEntry.MibObject.
                         i4FsofcSwitchModeOnConnFailure ==
                         OFC_SRC_ONE) ? "failSecure" : "failStandAlone"));

        }
        CliPrintf (CliHandle, "\r\n");

        if (SNMP_SUCCESS !=
            nmhGetNextIndexFsofcCfgTable (u4FsofcContextId,
                                          &u4NextFsofcContextId))
        {
            break;
        }

        OfcFsofcCfgEntry.MibObject.u4FsofcContextId = u4NextFsofcContextId;
        u4FsofcContextId = u4NextFsofcContextId;
    }
    UNUSED_PARAM (u4Module);
    return CLI_SUCCESS;
}

/****************************************************************************
 Function    : OfcShowRunningConfigFsofcControllerConnTable
 Description : This function displays the current configuration
                of FsofcControllerConnTable table
 Input       : CliHandle - Handle to the cli context
                u4Module - Specified module for configuration 
 Output      : None
 Returns     : CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
OfcShowRunningConfigFsofcControllerConnTable (tCliHandle CliHandle,
                                              UINT4 u4Module, ...)
{
    tOfcFsofcControllerConnEntry OfcFsofcControllerConnEntry;
    INT4                i4FsofcControllerIpAddrType = OFC_SRC_ZERO;
    INT4                i4NextFsofcControllerIpAddrType = OFC_SRC_ZERO;
    tSNMP_OCTET_STRING_TYPE FsofcControllerIpAddressSrc;
    tSNMP_OCTET_STRING_TYPE NextFsofcControllerIpAddress;
    UINT1
         
         
                  au1FsofcControllerIpAddress[OFC_MAX_IP_ADDRESS_ARRAY_LENGTH];
    UINT1
         
         
         
         
              au1NextFsofcControllerIpAddress[OFC_MAX_IP_ADDRESS_ARRAY_LENGTH];
    CHR1               *pFsofcControllerIpAddress = NULL;
    INT4                i4FsofcControllerConnAuxId = OFC_SRC_ZERO;
    INT4                i4NextFsofcControllerConnAuxId = OFC_SRC_ZERO;
    UINT4               u4FsofcContextId = OFC_SRC_ZERO;
    UINT4               u4NextFsofcContextId = OFC_SRC_ZERO;
    UINT4               u4Flag = OFC_SRC_ZERO;
    UINT4               u4TmpAddr = OFC_SRC_ZERO;
    UINT1               au1TmpAddr[OFC_MAX_IP_LENGTH];
    UINT1               au1FsofcContextId[256];

    UNUSED_PARAM (CliHandle);
    MEMSET (au1FsofcControllerIpAddress, OFC_SRC_ZERO,
            sizeof (au1FsofcControllerIpAddress));
    MEMSET (au1TmpAddr, OFC_SRC_ZERO, OFC_MAX_IP_LENGTH);
    MEMSET (&FsofcControllerIpAddressSrc, OFC_SRC_ZERO,
            sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&NextFsofcControllerIpAddress, OFC_SRC_ZERO,
            sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&OfcFsofcControllerConnEntry, OFC_SRC_ZERO,
            sizeof (tOfcFsofcControllerConnEntry));
    MEMSET (au1FsofcContextId, OFC_ZERO, 256);
    FsofcControllerIpAddressSrc.pu1_OctetList = au1FsofcControllerIpAddress;
    NextFsofcControllerIpAddress.pu1_OctetList =
        au1NextFsofcControllerIpAddress;

    if (SNMP_SUCCESS != nmhGetFirstIndexFsofcControllerConnTable
        (&u4FsofcContextId, &i4FsofcControllerIpAddrType,
         &FsofcControllerIpAddressSrc, &i4FsofcControllerConnAuxId))
    {
        return CLI_SUCCESS;
    }

    OfcFsofcControllerConnEntry.MibObject.u4FsofcContextId = u4FsofcContextId;
    OfcFsofcControllerConnEntry.MibObject.i4FsofcControllerIpAddrType =
        i4FsofcControllerIpAddrType;
    MEMCPY (OfcFsofcControllerConnEntry.MibObject.au1FsofcControllerIpAddress,
            FsofcControllerIpAddressSrc.pu1_OctetList,
            FsofcControllerIpAddressSrc.i4_Length);
    OfcFsofcControllerConnEntry.MibObject.i4FsofcControllerIpAddressLen =
        FsofcControllerIpAddressSrc.i4_Length;
    OfcFsofcControllerConnEntry.MibObject.i4FsofcControllerConnAuxId =
        i4FsofcControllerConnAuxId;

    while (OFC_SRC_ONE)
    {
        if (OfcGetAllFsofcControllerConnTable (&OfcFsofcControllerConnEntry) !=
            OSIX_SUCCESS)
        {
            return CLI_SUCCESS;
        }

        OfcUtlConversionForFsofcControllerIpAddress (pFsofcControllerIpAddress,
                                                     &OfcFsofcControllerConnEntry);
        /*Convert Array to Dotted String */
        CliStrToIp4Addr (OfcFsofcControllerConnEntry.MibObject.
                         au1FsofcControllerIpAddress, au1TmpAddr);
        u4TmpAddr = INET_ADDR (au1TmpAddr);
        CLI_CONVERT_IPADDR_TO_STR (pFsofcControllerIpAddress, u4TmpAddr);
        CliPrintf (CliHandle,
                   "ofc-controller context-id %d ip-address %s aux-id %d ",
                   OfcFsofcControllerConnEntry.MibObject.
                   u4FsofcContextId,
                   pFsofcControllerIpAddress,
                   OfcFsofcControllerConnEntry.MibObject.
                   i4FsofcControllerConnAuxId);
        if (OfcFsofcControllerConnEntry.MibObject.i4FsofcControllerConnPort
            != OFC_DEF_FSOFCCONTROLLERCONNPORT)
        {
            CliPrintf (CliHandle,
                       "conn-port %d ",
                       OfcFsofcControllerConnEntry.MibObject.
                       i4FsofcControllerConnPort);
            u4Flag = OFC_SRC_ONE;

        }
        if (OfcFsofcControllerConnEntry.MibObject.i4FsofcControllerConnProtocol
            != OFC_DEF_FSOFCCONTROLLERCONNPROTOCOL)
        {
            if (u4Flag == OFC_SRC_ONE)
            {
                CliPrintf (CliHandle,
                           "\r\nofc-controller context-id %d ip-address %s aux-id %d ",
                           OfcFsofcControllerConnEntry.MibObject.
                           u4FsofcContextId,
                           pFsofcControllerIpAddress,
                           OfcFsofcControllerConnEntry.MibObject.
                           i4FsofcControllerConnAuxId);
            }
            CliPrintf (CliHandle,
                       "conn-protocol %s ",
                       ((OfcFsofcControllerConnEntry.MibObject.
                         i4FsofcControllerConnProtocol ==
                         OFC_SRC_ONE) ? "tcp" : "ssl"));

        }

        if (OfcFsofcControllerConnEntry.MibObject.i4FsofcControllerConnBand
            != OFC_DEF_FSOFCCONTROLLERCONNBAND)
        {
            CliPrintf (CliHandle,
                       "ofc-controller context-id %d ip-address %s aux-id %d conn-band %d %d \r\n",
                       OfcFsofcControllerConnEntry.MibObject.u4FsofcContextId,
                       pFsofcControllerIpAddress,
                       OfcFsofcControllerConnEntry.
                       MibObject.i4FsofcControllerConnAuxId,
                       OfcFsofcControllerConnEntry.
                       MibObject.i4FsofcControllerConnBand,
                       OfcFsofcControllerConnEntry.
                       MibObject.i4FsofcControllerConnBand);

        }
        CliPrintf (CliHandle, "\r\n");

        if (SNMP_SUCCESS !=
            nmhGetNextIndexFsofcControllerConnTable (u4FsofcContextId,
                                                     &u4NextFsofcContextId,
                                                     i4FsofcControllerIpAddrType,
                                                     &i4NextFsofcControllerIpAddrType,
                                                     &FsofcControllerIpAddressSrc,
                                                     &NextFsofcControllerIpAddress,
                                                     i4FsofcControllerConnAuxId,
                                                     &i4NextFsofcControllerConnAuxId))
        {
            break;
        }

        OfcFsofcControllerConnEntry.MibObject.u4FsofcContextId =
            u4NextFsofcContextId;
        OfcFsofcControllerConnEntry.MibObject.i4FsofcControllerIpAddrType =
            i4NextFsofcControllerIpAddrType;
        MEMCPY (OfcFsofcControllerConnEntry.
                MibObject.au1FsofcControllerIpAddress,
                NextFsofcControllerIpAddress.pu1_OctetList,
                NextFsofcControllerIpAddress.i4_Length);
        OfcFsofcControllerConnEntry.MibObject.i4FsofcControllerIpAddressLen =
            NextFsofcControllerIpAddress.i4_Length;
        OfcFsofcControllerConnEntry.MibObject.i4FsofcControllerConnAuxId =
            i4NextFsofcControllerConnAuxId;
        u4FsofcContextId = u4NextFsofcContextId;
        i4FsofcControllerIpAddrType = i4NextFsofcControllerIpAddrType;
        MEMCPY (FsofcControllerIpAddressSrc.pu1_OctetList,
                NextFsofcControllerIpAddress.pu1_OctetList,
                NextFsofcControllerIpAddress.i4_Length);
        FsofcControllerIpAddressSrc.i4_Length =
            NextFsofcControllerIpAddress.i4_Length;
        i4FsofcControllerConnAuxId = i4NextFsofcControllerConnAuxId;
    }
    UNUSED_PARAM (u4Module);
    return CLI_SUCCESS;
}
