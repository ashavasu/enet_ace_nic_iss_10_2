/******************************************************************************
 * Copyright (C) 2013 Aricent Inc . All Rights Reserved
 *
 * $Id: ofcque.c,v 1.4 2014/01/31 13:07:24 siva Exp $
 *
 * Description: This file contains the routines for Queue Processing
 ******************************************************************************/

#include "ofcinc.h"
#include "ofcsz.h"
#include "ofcacts.h"
#include "ofcintf.h"
#include "ofccli.h"
#include "ofc131pkt.h"
#include "ofcvlan.h"
#include "ofcinstr.h"
#include "ofcapi.h"

/******************************************************************************
 * Function     : OfcQueProcessMsgs  
 * Description  : Processes the messages from Queue
 *
 * Input        : None
 * Output       : None
 * Returns      : None
******************************************************************************/

PUBLIC VOID
OfcQueProcessMsgs ()
{
    tOfcQueMsg         *pQueMsg = NULL;
    tOfpPortStatus     *pOfpPortSt = NULL;
    tOfp131PortStatus  *pOfp131PortStat = NULL;
    tOfcFsofcControllerConnEntry *pControllerEntry = NULL;
    tOfcFsofcCfgEntry  *pOfcFsofcCfgEntry = NULL;
    UINT4               u4VarLen = OFC_ZERO;

    OFC_TRC_FUNC ((OFC_FN_ENTRY, "FUNC:OfcQueProcessMsgs\n"));

    while (OsixQueRecv (gOfcGlobals.ofcQueId, (UINT1 *) &pQueMsg,
                        OSIX_DEF_MSG_LEN, OFC_ZERO) == OSIX_SUCCESS)
    {
        if ((pQueMsg->u4MsgType == OFCR_NO_MATCH) ||
            (pQueMsg->u4MsgType == OFCR_ACTION) ||
            (pQueMsg->u4MsgType == OFC_PORT_STATUS_SEND))
        {
            /* 
             * The sockid will not be avilable for these msgs
             * So scan through all controllers and send out the Pkt
             */
            if (gOfcGlobals.OfcGlbMib.FsofcControllerConnTable != NULL)
            {
                pControllerEntry = RBTreeGetFirst (gOfcGlobals.OfcGlbMib.
                                                   FsofcControllerConnTable);
                if (pControllerEntry != NULL)
                {
                    do
                    {
                        if (pControllerEntry->MibObject.u4FsofcContextId !=
                            pQueMsg->u4ContextId)
                        {
                            OFC131_TRC_FUNC ((OFC_PKT_TRC,
                                              "\r\nOfcQueProcessMsgs: "
                                              "This Cntlr entry doesn't "
                                              "belongs current context\n"));
                            continue;
                        }

                        pOfcFsofcCfgEntry =
                            OfcGetFsofcCfgEntry (pControllerEntry->MibObject.
                                                 u4FsofcContextId);
                        if (pOfcFsofcCfgEntry == NULL)
                        {
                            OFC131_TRC_FUNC ((OFC_PKT_TRC,
                                              "\r\nOfcQueProcessMsgs: "
                                              "Get CFG Entry Failed\n"));
                            continue;
                        }

                        if ((OPENFLOW_CONNROLE_MASTER ==
                             pControllerEntry->MibObject.i4FsofcControllerRole)
                            || (OPENFLOW_CONNROLE_EQUAL ==
                                pControllerEntry->MibObject.
                                i4FsofcControllerRole))
                        {
                            if (pOfcFsofcCfgEntry->MibObject.
                                i4FsofcSupportedVersion == OFC_VERSION_V100)
                            {
                                if (pQueMsg->u4MsgType != OFC_PORT_STATUS_SEND)
                                {
                                    if ((pControllerEntry->
                                         au4PktInMask[OFC_ZERO] >> pQueMsg->
                                         u4MsgType) & OFC_ONE)
                                    {
                                        OfcSendPktIn (pQueMsg->u4IfIndex,
                                                      pQueMsg->u4MsgType,
                                                      pQueMsg->pBuf,
                                                      pControllerEntry);
                                    }
                                }
                                else
                                {
                                    pOfpPortSt =
                                        (tOfpPortStatus *) pQueMsg->pPortStat;
                                    if ((pControllerEntry->
                                         au4PortStatusMask[OFC_ZERO] >>
                                         pOfpPortSt->u1Reason) & OFC_ONE)
                                    {
                                        OfcPortStatusPktSend (pOfpPortSt,
                                                              (VOID *)
                                                              pControllerEntry);
                                    }
                                }
                            }
                            else
                            {
                                if (pQueMsg->u4MsgType != OFC_PORT_STATUS_SEND)
                                {
                                    if ((pControllerEntry->
                                         au4PktInMask[OFC_ZERO] >> pQueMsg->
                                         u4MsgType) & OFC_ONE)
                                    {
                                        Ofc131ActsSendPktIn (pQueMsg->u4IfIndex,
                                                             pQueMsg->u4Index,
                                                             pQueMsg->u4MsgType,
                                                             pQueMsg->u8Cookie,
                                                             pQueMsg->u1TableId,
                                                             pQueMsg->pOfcMatch,
                                                             pQueMsg->pBuf,
                                                             pControllerEntry);
                                    }
                                }
                                else
                                {
                                    pOfp131PortStat =
                                        (tOfp131PortStatus *) pQueMsg->
                                        pPortStat;
                                    if ((pControllerEntry->
                                         au4PortStatusMask[OFC_ZERO] >>
                                         pOfp131PortStat->u1Reason) & OFC_ONE)
                                    {
                                        Ofc131PktPortStatusSend
                                            (pOfp131PortStat,
                                             (VOID *) pControllerEntry,
                                             u4VarLen);
                                    }
                                }
                            }
                        }
                        else if (OPENFLOW_CONNROLE_SLAVE ==
                                 pControllerEntry->MibObject.
                                 i4FsofcControllerRole)
                        {
                            if (pOfcFsofcCfgEntry->MibObject.
                                i4FsofcSupportedVersion == OFC_VERSION_V100)
                            {
                                if (pQueMsg->u4MsgType != OFC_PORT_STATUS_SEND)
                                {
                                    if ((pControllerEntry->
                                         au4PktInMask[OFC_ONE] >> pQueMsg->
                                         u4MsgType) & OFC_ONE)
                                    {
                                        OfcSendPktIn (pQueMsg->u4IfIndex,
                                                      pQueMsg->u4MsgType,
                                                      pQueMsg->pBuf,
                                                      pControllerEntry);
                                    }
                                }
                                else
                                {
                                    pOfpPortSt = (tOfpPortStatus *)
                                        pQueMsg->pPortStat;
                                    if ((pControllerEntry->
                                         au4PortStatusMask[OFC_ZERO] >>
                                         pOfpPortSt->u1Reason) & OFC_ONE)
                                    {
                                        OfcPortStatusPktSend (pOfpPortSt,
                                                              (VOID *)
                                                              pControllerEntry);
                                    }
                                }
                            }
                            else
                            {
                                if (pQueMsg->u4MsgType != OFC_PORT_STATUS_SEND)
                                {
                                    if ((pControllerEntry->
                                         au4PktInMask[OFC_ONE] >> pQueMsg->
                                         u4MsgType) & OFC_ONE)
                                    {
                                        Ofc131ActsSendPktIn (pQueMsg->u4IfIndex,
                                                             pQueMsg->u4Index,
                                                             pQueMsg->u4MsgType,
                                                             pQueMsg->u8Cookie,
                                                             pQueMsg->u1TableId,
                                                             pQueMsg->pOfcMatch,
                                                             pQueMsg->pBuf,
                                                             pControllerEntry);
                                    }
                                }
                                else
                                {
                                    pOfp131PortStat = (tOfp131PortStatus *)
                                        pQueMsg->pPortStat;
                                    if ((pControllerEntry->
                                         au4PortStatusMask[OFC_ZERO] >>
                                         pOfp131PortStat->u1Reason) & OFC_ONE)
                                    {
                                        Ofc131PktPortStatusSend
                                            (pOfp131PortStat,
                                             (VOID *) pControllerEntry,
                                             u4VarLen);
                                    }
                                }
                            }
                        }
                    }
                    while ((pControllerEntry =
                            RBTreeGetNext (gOfcGlobals.OfcGlbMib.
                                           FsofcControllerConnTable,
                                           pControllerEntry, NULL)) != NULL);
                }
            }
        }

        if (pQueMsg->u4MsgType == OFC_READ_FD)
        {
            OFC_TRC ((OFC_QUE_TRC,
                      "Process Ofc Que Msg OFC_CONTROLLER_PKT_RECV\n"));
            OfcProcessReceivePacket (pQueMsg->u4ContextId, pQueMsg->i4SockFd);
        }

        if (pQueMsg->u4MsgType == OFC_WRITE_FD)
        {
            OFC_TRC ((OFC_QUE_TRC,
                      "Process Ofc Que Msg OFC_CONTROLLER_PKT_RECV\n"));
            OfcControllerProcessSocketWrite (pQueMsg->u4ContextId,
                                             pQueMsg->i4SockFd);
        }

        if (pQueMsg->u4MsgType == OFC_VLAN_CREATE)
        {
            OFC_TRC ((OFC_QUE_TRC, "Process Ofc Que Msg OFC_VLAN_CREATE\n"));
            OfcVlanCreate (pQueMsg->u4VlanId);
        }

        if (pQueMsg->u4MsgType == OFC_VLAN_DELETE)
        {
            OFC_TRC ((OFC_QUE_TRC, "Process Ofc Que Msg OFC_VLAN_CREATE\n"));
            OfcVlanDelete (pQueMsg->u4VlanId);
        }

        if (pQueMsg->u4MsgType == OFC_VLAN_ADD_MEMBER)
        {
            OFC_TRC ((OFC_QUE_TRC, "Process Ofc Que Msg OFC_VLAN_CREATE\n"));
            OfcVlanConfigurePorts (pQueMsg->u4VlanId, pQueMsg->pu1MemberPorts,
                                   pQueMsg->pu1UntaggedPorts, pQueMsg->u4Flag);
            UtilPlstReleaseLocalPortList (pQueMsg->pu1MemberPorts);
            UtilPlstReleaseLocalPortList (pQueMsg->pu1UntaggedPorts);
        }

        if (pQueMsg->u4MsgType == OFC_PIPELINE_NOTIFY_SEND)
        {
            pOfcFsofcCfgEntry = NULL;
            pOfcFsofcCfgEntry = OfcGetFsofcCfgEntry (pQueMsg->u4ContextId);
            if (pOfcFsofcCfgEntry == NULL)
            {
                OFC131_TRC_FUNC ((OFC_PKT_TRC, "\r\nOfcQueProcessMsgs: "
                                  "Get CFG Entry Failed\n"));
                MemReleaseMemBlock (OFC_QUEUE_POOLID, (UINT1 *) pQueMsg);
                return;
            }

            OFC_TRC ((OFC_QUE_TRC,
                      "Notifying Ofc Pipeline process on pkt recv \n"));
            if (pOfcFsofcCfgEntry->MibObject.i4FsofcSupportedVersion ==
                OFC_VERSION_V100)
            {
                OfcPipelineProcess (pQueMsg->u4IfIndex,
                                    pOfcFsofcCfgEntry->MibObject.
                                    u4FsofcContextId, pQueMsg->pBuf);
            }
            else
            {
                Ofc131ActsPipelineProcess (pQueMsg->u4IfIndex,
                                           pOfcFsofcCfgEntry->MibObject.
                                           u4FsofcContextId, pQueMsg->pBuf);
            }
        }

        if (pQueMsg->u4MsgType == OFC_PORT_STATUS_SEND)
        {
            MemReleaseMemBlock (OFC_PORT_STATUS_POOLID,
                                (UINT1 *) pQueMsg->pPortStat);
        }

        if ((pQueMsg->u4MsgType == OFCR_ACTION) ||
            (pQueMsg->u4MsgType == OFCR_NO_MATCH))
        {
            if (pQueMsg->pOfcMatch != NULL)
            {
                /* Release FlowMatch MemPool */
                MemReleaseMemBlock (OFC_FLOW_MATCH_TLV_POOLID,
                                    (UINT1 *) pQueMsg->pOfcMatch);
            }
            /* Release the CRU Buffer */
            CRU_BUF_Release_MsgBufChain (pQueMsg->pBuf, FALSE);
        }

        MemReleaseMemBlock (OFC_QUEUE_POOLID, (UINT1 *) pQueMsg);
    }

    OFC_TRC_FUNC ((OFC_FN_EXIT, "FUNC:Exit OfcQueProcessMsgs\n"));
}

/******************************************************************************
 * Function     : OfcQueSendMsg
 * Description  : Enqueues the msgs
 *
 * Input        : pQueMsg  - Pointer to the Message, to be Enqueued
 * Output       : None
 * Returns      : OSIX_SUCCESS/OSIX_FAILURE
 *****************************************************************************/
PUBLIC INT4
OfcQueSendMsg (tOfcQueMsg * pQueMsg)
{
    OFC_TRC_FUNC ((OFC_FN_ENTRY, "FUNC:OfcQueSendMsg\n"));
    if (OsixQueSend (gOfcGlobals.ofcQueId, (UINT1 *) &pQueMsg,
                     OSIX_DEF_MSG_LEN) == OSIX_FAILURE)
    {
        /*
         * Including the logic of releasing the buffers
         * inline here. Can be moved to new API, in case
         * more types are handled.
         */

        /* 1. Release the CRU BUffer */
        if (((pQueMsg->u4MsgType == OFCR_NO_MATCH) ||
             (pQueMsg->u4MsgType == OFCR_ACTION)) && (pQueMsg->pBuf != NULL))
        {
            CRU_BUF_Release_MsgBufChain (pQueMsg->pBuf, FALSE);
        }

        /* 2. Release the MemPools */
        if (pQueMsg->pOfcMatch != NULL)
        {
            if (MemReleaseMemBlock (OFC_FLOW_MATCH_TLV_POOLID,
                                    (UINT1 *) pQueMsg->pOfcMatch) !=
                MEM_SUCCESS)
            {
                OFC_TRC ((OFC_QUE_TRC, "Releasing Queue MemPool Failed,"
                          " return Failure\n"));
            }
        }

        if (MemReleaseMemBlock (OFC_QUEUE_POOLID,
                                (UINT1 *) pQueMsg) != MEM_SUCCESS)
        {
            OFC_TRC ((OFC_QUE_TRC, "Releasing Queue MemPool Failed,"
                      " return Failure\n"));
        }

        OFC_TRC ((OFC_QUE_TRC, "Sending Queue Msg Failed, return Failure\n"));
        return OSIX_FAILURE;
    }

    if (OsixEvtSend (gOfcGlobals.ofcTaskId, OFC_QUEUE_EVENT) == OSIX_FAILURE)
    {
        /*
         * Including the logic of releasing the buffers
         * inline here. Can be moved to new API, in case
         * more types are handled.
         */

        /* 1. Release the CRU BUffer */
        if (((pQueMsg->u4MsgType == OFCR_NO_MATCH) ||
             (pQueMsg->u4MsgType == OFCR_ACTION)) && (pQueMsg->pBuf != NULL))
        {
            CRU_BUF_Release_MsgBufChain (pQueMsg->pBuf, FALSE);
        }

        /* 2. Release the MemPools */
        if (pQueMsg->pOfcMatch != NULL)
        {
            if (MemReleaseMemBlock (OFC_FLOW_MATCH_TLV_POOLID,
                                    (UINT1 *) pQueMsg->pOfcMatch) !=
                MEM_SUCCESS)
            {
                OFC_TRC ((OFC_QUE_TRC, "Releasing Queue MemPool Failed,"
                          " return Failure\n"));
            }
        }

        if (MemReleaseMemBlock (OFC_QUEUE_POOLID,
                                (UINT1 *) pQueMsg) != MEM_SUCCESS)
        {
            OFC_TRC ((OFC_QUE_TRC, "Releasing Queue MemPool Failed,"
                      " return Failure\n"));
        }

        OFC_TRC ((OFC_QUE_TRC, "Sending Event Msg Failed, return Failure\n"));
        return OSIX_FAILURE;
    }

    OFC_TRC_FUNC ((OFC_FN_EXIT, "FUNC:OfcQueSendMsg\n"));
    return OSIX_SUCCESS;
}

/*-----------------------------------------------------------------------*/
/*                       End of the file  ofcque.c                      */
/*-----------------------------------------------------------------------*/
