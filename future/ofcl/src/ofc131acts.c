/********************************************************************
* Copyright (C) 2013 Aricent Inc . All Rights Reserved
*
* $Id: ofc131acts.c,v 1.16 2017/09/11 13:33:51 siva Exp $
* 
* Description: This file contains the routines for Pipeline Processing 
**********************************************************************/

#include "cfa.h"
#include "ofcflow.h"
#include "ofcacts.h"
#include "ofcinstr.h"
#include "ofcapi.h"
#include "ofcvlan.h"
#include "ofcdefn.h"
#include "ofcgroup.h"
#include "ofcsz.h"
#include "ofchybrid.h"

#ifdef NPAPI_WANTED
#ifndef OPENFLOW_TCAM
#include "ofcnp.h"
#endif
#endif

static tOfcActionSet gActionSet;
static UINT1        gau1Metadata[OFC_EIGHT];
tCfaIfInfo          gCfaIfInfo;
static UINT1        au1ExactMatch[MAX_OFC_EXACT_LENGTH];
static UINT1        au1IfName[CFA_MAX_IFALIAS_LENGTH];
static UINT1        gau1GrpPktBuf[OFC131_MAX_PKT_LEN];

/******************************************************************************
 * Function    :  Ofc131ActsComputeNumBitsSet 
 * Description :  This routine calculates the number of bits set.
 * Input       :  u8Bitmap   - Value for which the number of bits set to be
 *                             computed
 * Output      :  pu1NumBits - Pointer to the Count holding the number of bits 
 * Returns     :  None 
 *****************************************************************************/
PUBLIC VOID
Ofc131ActsComputeNumBitsSet (FS_UINT8 u8Bitmap, UINT1 *pu1NumBits)
{
    UINT4               u4Value = OFC_ZERO;
    UINT4               u4Count = OFC_ZERO;
    UINT2               u2Loop = OFC_ZERO;

    OFC_TRC_FUNC ((OFC_FN_ENTRY, "Ofc131ActsComputeNumBitsSet Entry\n"));

    if ((u8Bitmap.u4Lo == OFC_ZERO) && (u8Bitmap.u4Hi == OFC_ZERO))
    {
        *pu1NumBits = OFC_ZERO;
    }

    u4Value = u8Bitmap.u4Lo;
    for (u2Loop = OFC_ZERO; u2Loop < OFC_TWO; u2Loop++)
    {
        if (u4Value != OFC_ZERO)
        {
            u4Value = u4Value - ((u4Value >> OFC_ONE) & 0x55555555);
            u4Value = (u4Value & 0x33333333) +
                ((u4Value >> OFC_TWO) & 0x33333333);
            u4Count = u4Count + ((((u4Value + (u4Value >> OFC_FOUR)) &
                                   0xF0F0F0F) * 0x1010101) >> OFC_TWENTY_FOUR);
        }
        u4Value = u8Bitmap.u4Hi;
    }
    *pu1NumBits = (UINT1) u4Count;
    OFC_TRC_FUNC ((OFC_FN_EXIT, "Ofc131ActsComputeNumBitsSet Exit\n"));
}

/******************************************************************************
 * Function    :  Ofc131ActsComputeIPChecksum
 * Description :  This routine calculates the IPv4 Header check sum for the
 *                received data packet.
 * Input       :  pPkt      - Pointer to the received packet CRU buffer.
 *                u1Size    - IPv4 Header Size
 *                u1Offset  - IPv4 Header Offset 
 * Output      :  None.
 * Returns     :  Computed checksum in Network Byte Order. 
 *****************************************************************************/
PRIVATE UINT2
Ofc131ActsComputeIPChecksum (tCRU_BUF_CHAIN_HEADER * pPkt,
                             UINT1 u1Size, UINT2 u2Offset)
{
    UINT1              *pu1Buf = NULL;
    UINT4               u4Sum = OFC_ZERO;
    UINT2               u2Tmp = OFC_ZERO;
    UINT1               au1Buf[OFC_MAX_IPV4_HDR_SIZE];

    OFC_TRC_FUNC ((OFC_FN_ENTRY, "FUNC:Ofc131ActsComputeIPChecksum\n"));

    MEMSET (au1Buf, OFC_ZERO, OFC_MAX_IPV4_HDR_SIZE);
    /* Copy the total IPv4 Header header into the Buffer */
    CRU_BUF_Copy_FromBufChain (pPkt, au1Buf, (UINT4) u2Offset, (UINT4) u1Size);

    /* set the existing check-sum field to zero */
    MEMSET (&au1Buf[OFC_TEN], OFC_ZERO, OFC_TWO);
    pu1Buf = au1Buf;
    /* Caliculate the checsum */
    while (u1Size > OFC_ONE)
    {
        u2Tmp = *((UINT2 *) (VOID *) pu1Buf);
        u4Sum += u2Tmp;
        pu1Buf += sizeof (UINT2);
        u1Size = (UINT1) (u1Size - (sizeof (UINT2)));
    }

    if (u1Size == OFC_ONE)
    {
        u2Tmp = OFC_ZERO;
        *((UINT1 *) &u2Tmp) = *pu1Buf;
        u4Sum += u2Tmp;
    }
    u4Sum = (u4Sum >> IP_SIXTEEN) + (u4Sum & IP_BIT_ALL);
    u4Sum += (u4Sum >> IP_SIXTEEN);
    u2Tmp = (UINT2) ~(u4Sum);

    OFC_TRC_FUNC ((OFC_FN_EXIT, "FUNC:Ofc131ActsComputeIPChecksum SUCCESS\n"));
    return (u2Tmp);
}

/******************************************************************************
 * Function    : Ofc131ActsComputeHdrOffset 
 * Description : This routine calculates the Protocol Header offsets.
 *               As of now it gives the header offsets for VLAN, MPLS,
 *               IPv4&IPv6, TCP, UDP, SCTP, ICMPv4&ICMPv6 and ND. 
 * Input       : pPkt        - Pointer to Data Packet CRU Buffer 
 *               u2Proto     - Protocol, to which offset is required
 * Output      : pu2ActProto - Pointer to Actual Protocol present.
 *               pu2Offset   - Pointer to Offset of the required header
 * Returns     :  OFC_SUCCESS or OFC_FAILURE
 ******************************************************************************/
PRIVATE UINT4
Ofc131ActsComputeHdrOffset (tCRU_BUF_CHAIN_HEADER * pPkt, UINT2 u2Proto,
                            UINT2 *pu2ActProto, UINT2 *pu2Offset)
{
    UINT2               u2EthType = OFC_ZERO;
    UINT1               u1IpProto = OFC_ZERO;
    UINT1               u1IpOffset = OFC_ZERO;

    OFC_TRC_FUNC ((OFC_FN_ENTRY, "FUNC:Ofc131ActsComputeHdrOffset\n"));

    *pu2Offset = OFC_ZERO;
    /* offset will always start zero */
    *pu2Offset = (UINT2) (*pu2Offset + (UINT2) OFC_ETHTYPE_OFFSET);
    /* First Know the EthType */
    CRU_BUF_Copy_FromBufChain (pPkt, (UINT1 *) &u2EthType,
                               (UINT4) *pu2Offset, (UINT4) OFC_ETHTYPE_SIZE);

    /* Compute number of VLAN Headers, if any */
    if ((u2EthType == OSIX_HTONS (OFC_L2EXT_CVLAN)) ||
        (u2EthType == OSIX_HTONS (OFC_L2EXT_SVLAN)))
    {
        if (u2Proto == OFC_VLAN_PROTO)
        {
            *pu2ActProto = u2EthType;
            return OFC_SUCCESS;
        }

        while (OFC_TRUE)
        {
            *pu2Offset = (UINT2) (*pu2Offset + (UINT2) VLAN_TAG_LEN);
            CRU_BUF_Copy_FromBufChain (pPkt, (UINT1 *) &u2EthType,
                                       (UINT4) *pu2Offset,
                                       (UINT4) OFC_ETHTYPE_SIZE);

            if ((u2EthType == OSIX_HTONS (OFC_L2EXT_CVLAN)) ||
                (u2EthType == OSIX_HTONS (OFC_L2EXT_SVLAN)))
            {
                /* Continue */
            }
            else
            {
                break;
            }
        }
    }

    /* Assuming no MPLS headers if it is PBB */
    if (u2EthType == OSIX_HTONS (OFC_L2EXT_PBB))
    {
        if (u2Proto == OFC_L2EXT_PBB)
        {
            return OFC_SUCCESS;
        }
        /*
         * Computing the Offset for IP or next only.
         * SVlan followed by only one CVlan is assumed
         */
        *pu2Offset = (UINT2) (*pu2Offset + (UINT2) OFC_ISID_TAGGED_HDR_SIZE +
                              (UINT2) OFC_VLAN_TAG_LEN +
                              (UINT2) OFC_VLAN_TAG_LEN);
    }

    /* Assuming no PBB header if it is MPLS */
    /*
     * NOTE: Due to complexity in assuming the next header after MPLS, 
     *       All the L3 layer Header offsets are computed assuming MPLS
     *       Header is not present or already stripped off.
     */
    if (((u2EthType == OSIX_HTONS (OFC_L2EXT_MPLS_1)) ||
         (u2EthType == OSIX_HTONS (OFC_L2EXT_MPLS_2))) &&
        (u2Proto == OFC_MPLS_PROTO))
    {
        *pu2Offset = (UINT2) (*pu2Offset + (UINT2) OFC_ETHTYPE_SIZE);
        *pu2ActProto = u2EthType;
        return OFC_SUCCESS;
    }

    if ((u2EthType == OSIX_HTONS (OFC_L3_ARP)) && (u2Proto == OFC_L3_ARP))
    {
        *pu2Offset = (UINT2) (*pu2Offset + (UINT2) OFC_ETHTYPE_SIZE);
        return OFC_SUCCESS;
    }

    if ((u2EthType == OSIX_HTONS (OFC_L3_ND)) && (u2Proto == OFC_L3_ND))
    {
        *pu2Offset = (UINT2) (*pu2Offset + (UINT2) OFC_ETHTYPE_SIZE);
        return OFC_SUCCESS;
    }

    if (u2EthType == OSIX_HTONS (OFC_L3_IP4))
    {
        *pu2Offset = (UINT2) (*pu2Offset + (UINT2) OFC_ETHTYPE_SIZE);
        /* u2Proto is already in Host Byte Order */
        if (u2Proto == OFC_IP_PROTO)
        {
            *pu2ActProto = OSIX_HTONS (u2EthType);
            return OFC_SUCCESS;
        }
        /* Get IPv4 Header Length */
        CRU_BUF_Copy_FromBufChain (pPkt, &u1IpOffset, (UINT4) *pu2Offset,
                                   OFC_ONE);
        u1IpOffset = (UINT1) ((UINT1) ((u1IpOffset &
                                        (UINT1) OFC_IPV4_IHL_UNMASK)) *
                              (UINT1) OFC_IPV4_WORD_SIZE);

        /* Get the Next Layer3/Layer4 Protocol */
        CRU_BUF_Copy_FromBufChain (pPkt, (UINT1 *) &u1IpProto,
                                   (UINT4) (*pu2Offset +
                                            (UINT2) OFC_IPV4_PROTO_OFFSET),
                                   (UINT4) OFC_ONE);
    }

    if (u2EthType == OSIX_HTONS (OFC_L3_IP6))
    {
        *pu2Offset = (UINT2) (*pu2Offset + (UINT2) OFC_ETHTYPE_SIZE);
        /* u2Proto is already in Host Byte Order */
        if (u2Proto == OFC_IP_PROTO)
        {
            *pu2ActProto = OSIX_NTOHS (u2EthType);
            return OFC_SUCCESS;
        }
        /* Get IPv6 Header Length */
        u1IpOffset = OFC_IPV6_HDR_SIZE;

        /* Get the Next Layer3/Layer4 Protocol */
        CRU_BUF_Copy_FromBufChain (pPkt, (UINT1 *) &u1IpProto,
                                   (UINT4) (*pu2Offset +
                                            (UINT2) OFC_IPV6_PROTO_OFFSET),
                                   (UINT4) OFC_ONE);
    }

    /* Offset for the next headers */
    *pu2Offset = (UINT2) (*pu2Offset + (UINT2) u1IpOffset);

    if ((u1IpProto == OFC_L4PROTO_TCP) && (u2Proto == OFC_L4PROTO_TCP))
    {
        return OFC_SUCCESS;
    }

    if ((u1IpProto == OFC_L4PROTO_UDP) && (u2Proto == OFC_L4PROTO_UDP))
    {
        return OFC_SUCCESS;
    }

    if ((u1IpProto == OFC_L4PROTO_SCTP) && (u2Proto == OFC_L4PROTO_SCTP))
    {
        return OFC_SUCCESS;
    }

    if ((u1IpProto == OFC_L4PROTO_ICMP) && (u2Proto == OFC_L4PROTO_ICMP))
    {
        return OFC_SUCCESS;
    }

    if ((u1IpProto == OFC_L4PROTO_ICMP6) && (u2Proto == OFC_L4PROTO_ICMP6))
    {
        return OFC_SUCCESS;
    }
    OFC_TRC_FUNC ((OFC_FN_EXIT, "FUNC:Ofc131ActsComputeHdrOffset FAILURE\n"));
    return OFC_FAILURE;
}

/******************************************************************************
 * Function    :  Ofc131ActsSetIPExtHdrsBM
 * Description :  This routine set the corresponding bits in the BitMap when
 *                the IPv6 Extension Headers are present in received data
 *                packet. 
 * Input       :  pPkt        - Pointer to the Data Packet CRU Buffer
 *                u2Offset    - Offset of the IPv6 Header in the CRU Buffer
 * Output      :  pu2ExtHdrBM - BitMap of the IPv6 extension headers present.
 * Returns     :  OFC_SUCCESS/OFC_FAILURE
 *****************************************************************************/
PRIVATE UINT4
Ofc131ActsSetIPExtHdrsBM (tCRU_BUF_CHAIN_HEADER * pPkt, UINT2 u2Offset,
                          UINT2 *pu2ExtHdrBM)
{
    tIp6Hdr             Ip6Hdr;
    tOfcExtHdr          ExtHdr;
    UINT1               u1ExtHdrType = OFC_ZERO;
    UINT1               u1NxtHdr = OFC_ONE;
    UINT1               u1HopHdrCnt = OFC_ZERO;
    UINT1               u1RouteHdrCnt = OFC_ZERO;
    UINT1               u1FragHdrCnt = OFC_ZERO;
    UINT1               u1DestHdrCnt = OFC_ZERO;
    UINT1               u1AuthHdrCnt = OFC_ZERO;
    UINT2               u2Len = sizeof (tIp6Hdr);

    MEMSET (&Ip6Hdr, OFC_ZERO, sizeof (tIp6Hdr));
    CRU_BUF_Copy_FromBufChain (pPkt, (UINT1 *) &Ip6Hdr, (UINT4) u2Offset,
                               (UINT4) sizeof (tIp6Hdr));
    u1ExtHdrType = Ip6Hdr.u1Nh;
    while (u1NxtHdr)
    {
        switch (u1ExtHdrType)
        {
            case OFC_L4PROTO_TCP:
            case OFC_L4PROTO_UDP:
            case OFC_L4PROTO_ICMP6:
                /* Upper Layer Protocols, Add as neccessary */
                u1NxtHdr = OFC_ZERO;
                break;

            case OFC_IP6EXT_NO_NEXT_HDR:
                /* No Next Header, return */
                *pu2ExtHdrBM = *pu2ExtHdrBM | OFCIEH_NONEXT;
                u1NxtHdr = OFC_ZERO;
                break;

            case OFC_IP6EXT_HOP_HDR:
                /* Hop-by-Hop Options Extension Header */
                if (u2Len != sizeof (tIp6Hdr))
                {
                    /* Hop-by-Hop Should always be first */
                    return OFC_FAILURE;
                }
                MEMSET (&ExtHdr, OFC_ZERO, sizeof (tOfcExtHdr));
                CRU_BUF_Copy_FromBufChain (pPkt, (UINT1 *) &ExtHdr,
                                           (UINT4) (u2Offset + u2Len),
                                           (UINT4) (sizeof (tOfcExtHdr)));
                u1ExtHdrType = ExtHdr.u1NxtHdr;
                u2Len =
                    (UINT2) (u2Len + ((ExtHdr.u1HdrLen + OFC_ONE) * OFC_EIGHT));

                *pu2ExtHdrBM = *pu2ExtHdrBM | OFCIEH_HOP;
                u1HopHdrCnt++;
                break;

            case OFC_IP6EXT_ROUTE_HDR:
                /* Routing Extension Header */
                MEMSET (&ExtHdr, OFC_ZERO, sizeof (tOfcExtHdr));
                CRU_BUF_Copy_FromBufChain (pPkt, (UINT1 *) &ExtHdr,
                                           (UINT4) (u2Offset + u2Len),
                                           (UINT4) (sizeof (tOfcExtHdr)));
                u1ExtHdrType = ExtHdr.u1NxtHdr;
                u2Len =
                    (UINT2) (u2Len + ((ExtHdr.u1HdrLen + OFC_ONE) * OFC_EIGHT));

                *pu2ExtHdrBM = *pu2ExtHdrBM | OFCIEH_HOP;
                u1RouteHdrCnt++;
                break;

            case OFC_IP6EXT_FRAG_HDR:
                /* Fragment Extension Header */
                MEMSET (&ExtHdr, OFC_ZERO, sizeof (tOfcExtHdr));
                CRU_BUF_Copy_FromBufChain (pPkt, (UINT1 *) &ExtHdr,
                                           (UINT4) (u2Offset + u2Len),
                                           (UINT4) (sizeof (tOfcExtHdr)));
                u1ExtHdrType = ExtHdr.u1NxtHdr;
                u2Len = (UINT2) (u2Len + OFC_EIGHT);

                *pu2ExtHdrBM = *pu2ExtHdrBM | OFCIEH_FRAG;
                u1FragHdrCnt++;
                break;

            case OFC_IP6EXT_DEST_HDR:
                /* Destination Options Header */
                MEMSET (&ExtHdr, OFC_ZERO, sizeof (tOfcExtHdr));
                CRU_BUF_Copy_FromBufChain (pPkt, (UINT1 *) &ExtHdr,
                                           (UINT4) (u2Offset + u2Len),
                                           (UINT4) (sizeof (tOfcExtHdr)));
                u1ExtHdrType = ExtHdr.u1NxtHdr;
                u2Len =
                    (UINT2) (u2Len + ((ExtHdr.u1HdrLen + OFC_ONE) * OFC_EIGHT));

                *pu2ExtHdrBM = *pu2ExtHdrBM | OFCIEH_DEST;
                u1DestHdrCnt++;
                break;

            case OFC_IP6EXT_AUTH_HDR:
                /* Authentication Header */
                MEMSET (&ExtHdr, OFC_ZERO, sizeof (tOfcExtHdr));
                CRU_BUF_Copy_FromBufChain (pPkt, (UINT1 *) &ExtHdr,
                                           (UINT4) (u2Offset + u2Len),
                                           (UINT4) (sizeof (tOfcExtHdr)));
                u1ExtHdrType = ExtHdr.u1NxtHdr;
                /* Length in this header is in 4-Octet units minus 2 */
                u2Len =
                    (UINT2) (u2Len +
                             ((ExtHdr.u1HdrLen +
                               (UINT1) OFC_TWO) * (UINT1) OFC_FOUR));

                *pu2ExtHdrBM = *pu2ExtHdrBM | OFCIEH_AUTH;
                u1AuthHdrCnt++;
                break;

            case OFC_IP6EXT_ESP_HDR:
                /* Encapsulation Security Payload Header */
                /* 
                 * Due to complexity in computing the ESP Header size,
                 * we stop processing the extension headers here.
                 */
                *pu2ExtHdrBM = *pu2ExtHdrBM | OFCIEH_ESP;
                u1NxtHdr = OFC_ZERO;
                break;

            default:
                /* Invalid Extension Header */
                OFC_TRC_FUNC ((OFC_ACTS_TRC, "Invalid IPv6 Extension Header, "
                               "return FAILURE\n"));
                return OFC_FAILURE;
        }
    }

    /*
     * Unexpected repeats encountered bit is set in case any of the headers
     * occur more than once, except Destination Hdr where two can be expected
     */
    if ((u1HopHdrCnt > OFC_ONE) || (u1RouteHdrCnt > OFC_ONE) ||
        (u1FragHdrCnt > OFC_ONE) || (u1AuthHdrCnt > OFC_ONE) ||
        (u1DestHdrCnt > OFC_TWO))
    {
        *pu2ExtHdrBM = *pu2ExtHdrBM | OFCIEH_UNREP;
    }
    return OFC_SUCCESS;
}

/******************************************************************************
 * Function    :  Ofc131ActsSendPktOnPort
 * Description :  This routine isends the packet on the specified port
 * Input       :  u4IfIndex    - Interface Index.
 *                u4ContextId  - Context ID.
 *                pPkt         - Pointer to the received packet buffer.
 * Output      :  None
 * Returns     :  OFC_SUCCESS or OFC_FAILURE
 ******************************************************************************/
PRIVATE VOID
Ofc131ActsSendPktOnPort (UINT4 u4OutputPort, UINT4 u4ContextId,
                         tCRU_BUF_CHAIN_HEADER * pPkt)
{
    tOfcFsofcIfEntry    IfEntry;
    UINT1               u1OperStatus = OFC_ZERO;
    UINT4               u4NoFwd = OFC_ZERO;

    MEMSET (&IfEntry, OFC_ZERO, sizeof (IfEntry));

    /* Check whether the output port is up */
    if (CfaGetIfOperStatus (u4OutputPort, &u1OperStatus) == CFA_FAILURE)
    {
        OFC_TRC_FUNC ((OFC_ACTS_TRC, "Invalid input port," "return FAILURE\n"));
        return;
    }

    if (u1OperStatus == CFA_IF_DOWN)
    {
        OFC_TRC_FUNC ((OFC_ACTS_TRC, "Output IF Down, " "return FAILURE\n"));
        return;
    }

    /* Check if the no forward flag is set for this ouput port */
    IfEntry.MibObject.u4FsofcContextId = u4ContextId;
    IfEntry.MibObject.i4FsofcIfIndex = (INT4) u4OutputPort;
    if (OfcGetAllFsofcIfTable (&IfEntry) == OFC_SUCCESS)
    {
        u4NoFwd = (OSIX_HTONL (IfEntry.u4Config)) >> OFC_FIVE;
        if (u4NoFwd & OFC_ONE)
        {
            OFC_TRC_FUNC ((OFC_ACTS_TRC, "Do not fwd d Packet, "
                           "return FAILURE\n"));
            return;
        }
    }
    /* Sent the packet out */
    OfcInterfacePacketSend (pPkt, u4OutputPort,
                            CRU_BUF_Get_ChainValidByteCount (pPkt));
    return;
}

/******************************************************************************
 * Function    :  Ofc131ActsPrepareKeyFromMplsPkt
 * Description :  This routine copies the IPV4/IPV6 Source and Destination address 
 *                from the MPLS data packet.
 * Input       :  pPkt          - Pointer to the received packet buffer.
 *                pOxmFld       - Pointer to the IPV4/IPv6 address
 *                u2Offset      - Offset of the required header
 *                pu4TempLength - Length of the match field
 *                u1Field       - Match field either IPV$/IPV6 SRC and DST
 *                pFlowWcMatch  - Match field to add to the SLL node
 *
 * Output      :  None
 * Returns     :  OFC_SUCCESS or OFC_FAILURE
 ******************************************************************************/

PRIVATE UINT4
Ofc131ActsPrepareKeyFromMplsPkt (tCRU_BUF_CHAIN_HEADER * pPkt,
                                 tOxmFld * pOxmFld, UINT2 u2Offset,
                                 UINT4 *pu4TempLength, UINT1 u1Field,
                                 tOfcWcFlowMatch * pFlowWcMatch)
{
    UINT2               u2TempOffset = OFC_ZERO;
    UINT1               u1OneByte = OFC_ZERO;

    OFC_TRC_FUNC ((OFC_FN_ENTRY, "FUNC:Ofc131ActsPrepareKeyFromMplsPkt\r\n"));
    /* Copy BOS field from pkt to count the number of
     * stacked MPLS headers */
    u2TempOffset = u2Offset;
    while (OFC_TRUE)
    {
        CRU_BUF_Copy_FromBufChain (pPkt, &u1OneByte,
                                   (UINT4) ((UINT4) u2TempOffset +
                                            OFC_TWO), OFC_ONE);
        if (u1OneByte & OFC_MPLS_BOS_CHCK_MASK)
        {
            /* Check for the Version in the IP Header */
            CRU_BUF_Copy_FromBufChain (pPkt, &u1OneByte,
                                       (UINT4) (u2TempOffset +
                                                OFC_FOUR), OFC_ONE);
            u1OneByte = (UINT1) ((u1OneByte & OFC_IP_HEADER_VER_MASK) >>
                                 OFC_IP_HDR_VER_OFFSET_BITS);
            if (u1OneByte == OFC_FOUR)
            {
                /* Packet has IPV4 header in it */
                if (u1Field == OFCXMT_OFB_IPV4_SRC)
                {
                    CRU_BUF_Copy_FromBufChain (pPkt, pOxmFld->au1Fld,
                                               (UINT4) (u2TempOffset +
                                                        OFC_FOUR +
                                                        OFC_IPV4_SRC_OFFSET),
                                               OFC_IPV4_ADDR_LEN);
                }
                else if (u1Field == OFCXMT_OFB_IPV4_DST)
                {
                    CRU_BUF_Copy_FromBufChain (pPkt, pOxmFld->au1Fld,
                                               (UINT4) (u2TempOffset +
                                                        OFC_FOUR +
                                                        OFC_IPV4_DST_OFFSET),
                                               OFC_IPV4_ADDR_LEN);
                }
                *pu4TempLength = OFC_IPV4_ADDR_LEN;
                return OFC_SUCCESS;
            }
            else if (u1OneByte == OFC_SIX)
            {
                /* Packet has IPV6 header in it */
                if (u1Field == OFCXMT_OFB_IPV6_SRC)
                {
                    CRU_BUF_Copy_FromBufChain (pPkt, pOxmFld->au1Fld,
                                               (UINT4) (u2TempOffset +
                                                        OFC_FOUR +
                                                        OFC_IPV6_SRC_OFFSET),
                                               OFC_IPV6_ADDR_LEN);
                }
                else if (u1Field == OFCXMT_OFB_IPV6_DST)
                {
                    CRU_BUF_Copy_FromBufChain (pPkt, pOxmFld->au1Fld,
                                               (UINT4) (u2TempOffset +
                                                        OFC_FOUR +
                                                        OFC_IPV6_DST_OFFSET),
                                               OFC_IPV6_ADDR_LEN);
                }
                *pu4TempLength = OFC_IPV6_ADDR_LEN;
                return OFC_SUCCESS;
            }
            else
            {
                OFC_TRC_FUNC ((OFC_ACTS_TRC,
                               "Invalid MPLS Data Packet, return FAILURE\n"));
                OfcMemFreeForFlowMatch (OFC_MINUS_ONE, (UINT1 *) pFlowWcMatch);
                return OFC_FAILURE;
            }
        }
        else
        {
            /* Move to next label and check if it is a last 
             * MPLS label in the data packet */

            u2TempOffset = (UINT2) (u2TempOffset + OFC_FOUR);
            continue;
        }
    }                            /* End of while */
}

/******************************************************************************
 * Function    :  Ofc131ActsPrepareKey 
 * Description :  This routine prepares the Flow Match Key fron the incoming
 *                packet according to the match fields specified in the Flow
 *                match Bitmap.
 * Input       :  u8FlowMatchBM - BitMap containing types of Match Fields 
 *                u1NumBits     - Number of Flow Match Fields
 *                pPkt          - Pointer to the received packet buffer.
 * Output      :  pKeyMatchList - Pointer to the Flow Match fields SLL
 *                pu1ExactMatch - Pointer to the Exact Match
 * Returns     :  OFC_SUCCESS or OFC_FAILURE
 *****************************************************************************/
PRIVATE UINT4
Ofc131ActsPrepareKey (FS_UINT8 u8FlowMatchBM, UINT1 u1NumBits,
                      tCRU_BUF_CHAIN_HEADER * pPkt,
                      tOfcSll * pKeyMatchList, UINT1 *pu1ExactMatch,
                      UINT4 u4IfIndex, UINT4 u4LogicPort)
{
    tOxmFld            *pOxmFld = NULL;
    tOfcWcFlowMatch    *pFlowWcMatch = NULL;
    UINT4               u4LoopValue = OFC_ZERO;
    UINT4               u4RetVal = OFC_ZERO;
    UINT2               u2ActType = OFC_ZERO;    /* Actual Protocol Type */
    UINT2               u2Offset = OFC_ZERO;
    UINT2               u2TempOffset = OFC_ZERO;
    UINT1               u1Field = OFC_ZERO;
    UINT1               u1OneByte = OFC_ZERO;
    UINT1               u1TempOneByte = OFC_ZERO;
    UINT2               u2TwoByte = OFC_ZERO;
    UINT4               u4FourByte = OFC_ZERO;
    UINT4               u4TempLength = OFC_ZERO;
    UINT4               u4ExactLength = OFC_ZERO;    /* Exact match length */

    /* Initialize SLL */
    TMO_SLL_Init (pKeyMatchList);

    if ((u8FlowMatchBM.u4Lo == OFC_ZERO) && (u8FlowMatchBM.u4Hi == OFC_ZERO))
    {
        /* Null Bitmap */
        return OFC_FAILURE;
    }

    u4LoopValue = u8FlowMatchBM.u4Lo;
    while (u1NumBits)
    {
        if (u1Field == OFC_THIRTY_TWO)
        {
            u4LoopValue = u8FlowMatchBM.u4Hi;
        }

        if (!((u4LoopValue >> u1Field) & OFC_ONE))
        {
            u1Field = (UINT1) (u1Field + OFC_ONE);
            continue;
        }

        pFlowWcMatch = OfcMemAllocForFlowMatch (OFC_MINUS_ONE);
        if (pFlowWcMatch == NULL)
        {
            OFC131_TRC_FUNC ((OFC_PKT_TRC,
                              "MemAllocMemBlk for FlowMatchKey Failed\n"));
            return OSIX_FAILURE;
        }
        MEMSET (pFlowWcMatch, OFC_ZERO, sizeof (tOfcWcFlowMatch));

        pOxmFld = (tOxmFld *) ((VOID *) &(pFlowWcMatch->FldValue));
        u4TempLength = OFC_ZERO;
        switch (u1Field)
        {
            case OFCXMT_OFB_IN_PORT:
                /* Need to get the logical port number in case present */
                u4LogicPort = OSIX_HTONL (u4LogicPort);
                MEMCPY (pOxmFld->au1Fld, &u4LogicPort, OFC_FOUR);
                u4TempLength = OFC_FOUR;
                break;

            case OFCXMT_OFB_IN_PHY_PORT:
                u4IfIndex = OSIX_HTONL (u4IfIndex);
                MEMCPY (pOxmFld->au1Fld, &u4IfIndex, OFC_FOUR);
                u4TempLength = OFC_FOUR;
                break;

            case OFCXMT_OFB_METADATA:
                MEMCPY (pOxmFld->au1Fld, gau1Metadata, OFC_EIGHT);
                u4TempLength = OFC_EIGHT;
                break;

            case OFCXMT_OFB_ETH_DST:
                CRU_BUF_Copy_FromBufChain (pPkt, pOxmFld->au1Fld,
                                           OFC_ZERO, OFC_ETH_ADDR_LEN);
                u4TempLength = OFC_ETH_ADDR_LEN;
                break;

            case OFCXMT_OFB_ETH_SRC:
                CRU_BUF_Copy_FromBufChain (pPkt, pOxmFld->au1Fld,
                                           OFC_ETH_SRC_OFFSET,
                                           OFC_ETH_ADDR_LEN);
                u4TempLength = OFC_ETH_ADDR_LEN;
                break;

            case OFCXMT_OFB_ETH_TYPE:
                CRU_BUF_Copy_FromBufChain (pPkt, pOxmFld->au1Fld,
                                           OFC_ETHTYPE_OFFSET,
                                           OFC_ETHTYPE_SIZE);
                u4TempLength = OFC_ETHTYPE_SIZE;
                break;

            case OFCXMT_OFB_VLAN_VID:
                u4RetVal = Ofc131ActsComputeHdrOffset (pPkt, OFC_VLAN_PROTO,
                                                       &u2ActType, &u2Offset);
                if (OFC_FAILURE == u4RetVal)
                {
                    OFC_TRC_FUNC ((OFC_ACTS_TRC, "MatchField VLAN VID - "
                                   "No VLAN Header, FAILURE\n"));
                    OfcMemFreeForFlowMatch (OFC_MINUS_ONE,
                                            (UINT1 *) pFlowWcMatch);
                    return OFC_FAILURE;
                }
                CRU_BUF_Copy_FromBufChain (pPkt, (UINT1 *) &u2TwoByte,
                                           (UINT4) (u2Offset +
                                                    (UINT2)
                                                    OFC_VLAN_PROTO_SIZE),
                                           (UINT4) OFC_TWO);

                u2TwoByte = OSIX_NTOHS (u2TwoByte);
                u2TwoByte = u2TwoByte & OFC_PK_VLAN_VID_MASK;
                u2TwoByte = OSIX_HTONS (u2TwoByte);
                MEMCPY (pOxmFld->au1Fld, &u2TwoByte, OFC_TWO);
                u4TempLength = OFC_TWO;
                break;

            case OFCXMT_OFB_VLAN_PCP:
                u4RetVal = Ofc131ActsComputeHdrOffset (pPkt, OFC_VLAN_PROTO,
                                                       &u2ActType, &u2Offset);
                if (OFC_FAILURE == u4RetVal)
                {
                    OFC_TRC_FUNC ((OFC_ACTS_TRC, "MatchField VLAN PCP - "
                                   "No VLAN Header, FAILURE\n"));
                    OfcMemFreeForFlowMatch (OFC_MINUS_ONE,
                                            (UINT1 *) pFlowWcMatch);
                    return OFC_FAILURE;
                }
                CRU_BUF_Copy_FromBufChain (pPkt, &u1OneByte,
                                           (UINT4) (u2Offset +
                                                    OFC_VLAN_PROTO_SIZE),
                                           OFC_ONE);

                u1OneByte = u1OneByte >> OFC_FIVE;
                MEMCPY (pOxmFld->au1Fld, &u1OneByte, OFC_ONE);
                u4TempLength = OFC_ONE;
                break;

                /* IP DSCP - 6 bits in ToS Field */
            case OFCXMT_OFB_IP_DSCP:
                u4RetVal = Ofc131ActsComputeHdrOffset (pPkt, OFC_IP_PROTO,
                                                       &u2ActType, &u2Offset);
                if (OFC_FAILURE == u4RetVal)
                {
                    OFC_TRC_FUNC ((OFC_ACTS_TRC, "MatchField IP DSCP - "
                                   "No IP Header, FAILURE\n"));
                    OfcMemFreeForFlowMatch (OFC_MINUS_ONE,
                                            (UINT1 *) pFlowWcMatch);
                    return OFC_FAILURE;
                }

                if (u2ActType == OFC_L3_IP4)
                {
                    /* Copy ECN */
                    CRU_BUF_Copy_FromBufChain (pPkt, &u1OneByte,
                                               (UINT4) (u2Offset +
                                                        OFC_IPV4_DSCP_OFFSET),
                                               OFC_ONE);

                    u1OneByte = u1OneByte >> OFC_TWO;
                    MEMCPY (pOxmFld->au1Fld, &u1OneByte, OFC_ONE);
                    u4TempLength = OFC_ONE;
                }
                else
                {                /* It is IPv6 */
                    /* Copy ECN */
                    CRU_BUF_Copy_FromBufChain (pPkt, (UINT1 *) &u2TwoByte,
                                               (UINT4) u2Offset, OFC_TWO);

                    u2TwoByte = OSIX_NTOHS (u2TwoByte);
                    u2TwoByte = u2TwoByte >> OFC_SIX;
                    u2TwoByte = u2TwoByte & OFC_PK_IPV6_DSCP_MASK;
                    u2TwoByte = OSIX_HTONS (u2TwoByte);
                    MEMCPY (pOxmFld->au1Fld, &u2TwoByte, OFC_TWO);
                    u4TempLength = OFC_TWO;
                }
                break;

                /* IP ECN - 2 bits in ToS Field */
            case OFCXMT_OFB_IP_ECN:
                u4RetVal = Ofc131ActsComputeHdrOffset (pPkt, OFC_IP_PROTO,
                                                       &u2ActType, &u2Offset);
                if (OFC_FAILURE == u4RetVal)
                {
                    OFC_TRC_FUNC ((OFC_ACTS_TRC, "MatchField IP ECN - "
                                   "No IP Header, FAILURE\n"));
                    OfcMemFreeForFlowMatch (OFC_MINUS_ONE,
                                            (UINT1 *) pFlowWcMatch);
                    return OFC_FAILURE;
                }

                if (u2ActType == OFC_L3_IP4)
                {
                    /* Copy DSCP */
                    CRU_BUF_Copy_FromBufChain (pPkt, &u1OneByte,
                                               (UINT4) (u2Offset +
                                                        OFC_IPV4_DSCP_OFFSET),
                                               OFC_ONE);

                    u1OneByte = u1OneByte & OFC_PK_IPV4_ECN_MASK;
                    MEMCPY (pOxmFld->au1Fld, &u1OneByte, OFC_ONE);
                    u4TempLength = OFC_ONE;
                }
                else
                {                /* It is IPv6 */
                    /* Copy DSCP */
                    CRU_BUF_Copy_FromBufChain (pPkt, (UINT1 *) &u2TwoByte,
                                               (UINT4) u2Offset, OFC_TWO);

                    u2TwoByte = OSIX_NTOHS (u2TwoByte);
                    u2TwoByte = u2TwoByte >> OFC_FOUR;
                    u2TwoByte = u2TwoByte & OFC_PK_IPV6_ECN_MASK;
                    u2TwoByte = OSIX_HTONS (u2TwoByte);
                    MEMCPY (pOxmFld->au1Fld, &u2TwoByte, OFC_TWO);
                    u4TempLength = OFC_TWO;
                }
                break;

            case OFCXMT_OFB_IP_PROTO:
                u4RetVal = Ofc131ActsComputeHdrOffset (pPkt, OFC_IP_PROTO,
                                                       &u2ActType, &u2Offset);
                if (OFC_FAILURE == u4RetVal)
                {
                    OFC_TRC_FUNC ((OFC_ACTS_TRC, "MatchField IP Protocol - "
                                   "No IP Header, FAILURE\n"));
                    OfcMemFreeForFlowMatch (OFC_MINUS_ONE,
                                            (UINT1 *) pFlowWcMatch);
                    return OFC_FAILURE;
                }
                if (u2ActType == OFC_L3_IP4)
                {
                    CRU_BUF_Copy_FromBufChain (pPkt, pOxmFld->au1Fld,
                                               (UINT4) (u2Offset +
                                                        OFC_IPV4_PROTO_OFFSET),
                                               OFC_ONE);
                    u4TempLength = OFC_ONE;
                }
                else
                {                /* It is IPv6 */
                    CRU_BUF_Copy_FromBufChain (pPkt, pOxmFld->au1Fld,
                                               (UINT4) (u2Offset +
                                                        OFC_IPV6_PROTO_OFFSET),
                                               OFC_ONE);
                    u4TempLength = OFC_ONE;
                }
                break;

            case OFCXMT_OFB_IPV4_SRC:
                u4RetVal = Ofc131ActsComputeHdrOffset (pPkt, OFC_IP_PROTO,
                                                       &u2ActType, &u2Offset);
                if (OFC_FAILURE == u4RetVal)
                {
                    OFC_TRC_FUNC ((OFC_ACTS_TRC, "MatchField IPv4 Source - "
                                   "No IP Header, FAILURE\n"));
                    OfcMemFreeForFlowMatch (OFC_MINUS_ONE,
                                            (UINT1 *) pFlowWcMatch);
                    return OFC_FAILURE;
                }

                if (u2ActType == OFC_L3_IP4)
                {
                    CRU_BUF_Copy_FromBufChain (pPkt, pOxmFld->au1Fld,
                                               (UINT4) (u2Offset +
                                                        OFC_IPV4_SRC_OFFSET),
                                               OFC_IPV4_ADDR_LEN);

                    u4TempLength = OFC_IPV4_ADDR_LEN;
                }
                else if (u2ActType == OSIX_HTONS (OFC_L2EXT_MPLS_1))
                {
                    u4RetVal =
                        Ofc131ActsPrepareKeyFromMplsPkt (pPkt, pOxmFld,
                                                         u2Offset,
                                                         &u4TempLength, u1Field,
                                                         pFlowWcMatch);
                    if (u4RetVal == OFC_FAILURE)
                    {
                        OFC_TRC_FUNC ((OFC_ACTS_TRC, "MatchField IPv4 Source - "
                                       "No IPv4 Header, FAILURE\n"));
                        return OFC_FAILURE;
                    }
                }
                else
                {                /* It is IPv6 */
                    OFC_TRC_FUNC ((OFC_ACTS_TRC, "MatchField IPv4 Source - "
                                   "No IPv4 Header, FAILURE\n"));
                    OfcMemFreeForFlowMatch (OFC_MINUS_ONE,
                                            (UINT1 *) pFlowWcMatch);
                    return OFC_FAILURE;
                }
                break;

            case OFCXMT_OFB_IPV4_DST:
                u4RetVal = Ofc131ActsComputeHdrOffset (pPkt, OFC_IP_PROTO,
                                                       &u2ActType, &u2Offset);
                if (OFC_FAILURE == u4RetVal)
                {
                    OFC_TRC_FUNC ((OFC_ACTS_TRC,
                                   "MatchField IPv4 Destination - "
                                   "No IP Header, FAILURE\n"));
                    OfcMemFreeForFlowMatch (OFC_MINUS_ONE,
                                            (UINT1 *) pFlowWcMatch);
                    return OFC_FAILURE;
                }

                if (u2ActType == OFC_L3_IP4)
                {
                    CRU_BUF_Copy_FromBufChain (pPkt, pOxmFld->au1Fld,
                                               (UINT4) (u2Offset +
                                                        OFC_IPV4_DST_OFFSET),
                                               OFC_IPV4_ADDR_LEN);
                    u4TempLength = OFC_IPV4_ADDR_LEN;
                }
                else if (u2ActType == OSIX_HTONS (OFC_L2EXT_MPLS_1))
                {
                    u4RetVal = Ofc131ActsPrepareKeyFromMplsPkt (pPkt, pOxmFld,
                                                                u2Offset,
                                                                &u4TempLength,
                                                                u1Field,
                                                                pFlowWcMatch);
                    if (u4RetVal == OFC_FAILURE)
                    {
                        OFC_TRC_FUNC ((OFC_ACTS_TRC, "MatchField IPv4 Source - "
                                       "No IPv4 Header, FAILURE\n"));
                        return OFC_FAILURE;
                    }
                }
                else
                {
                    /* It is IPv6 */
                    OFC_TRC_FUNC ((OFC_ACTS_TRC,
                                   "MatchField IPv4 Destination - "
                                   "No IPv4 Header, FAILURE\n"));
                    OfcMemFreeForFlowMatch (OFC_MINUS_ONE,
                                            (UINT1 *) pFlowWcMatch);
                    return OFC_FAILURE;
                }
                break;

            case OFCXMT_OFB_TCP_SRC:
                u4RetVal = Ofc131ActsComputeHdrOffset (pPkt, OFC_L4PROTO_TCP,
                                                       &u2ActType, &u2Offset);
                if (OFC_FAILURE == u4RetVal)
                {
                    OFC_TRC_FUNC ((OFC_ACTS_TRC, "MatchField TCP Source - "
                                   "No TCP Header, FAILURE\n"));
                    OfcMemFreeForFlowMatch (OFC_MINUS_ONE,
                                            (UINT1 *) pFlowWcMatch);
                    return OFC_FAILURE;
                }
                CRU_BUF_Copy_FromBufChain (pPkt, pOxmFld->au1Fld,
                                           (UINT4) u2Offset, OFC_L4_ADDR_LEN);
                u4TempLength = OFC_L4_ADDR_LEN;
                break;

            case OFCXMT_OFB_TCP_DST:
                u4RetVal = Ofc131ActsComputeHdrOffset (pPkt, OFC_L4PROTO_TCP,
                                                       &u2ActType, &u2Offset);
                if (OFC_FAILURE == u4RetVal)
                {
                    OFC_TRC_FUNC ((OFC_ACTS_TRC, "MatchField TCP Destination - "
                                   "No TCP Header, FAILURE\n"));
                    OfcMemFreeForFlowMatch (OFC_MINUS_ONE,
                                            (UINT1 *) pFlowWcMatch);
                    return OFC_FAILURE;
                }
                CRU_BUF_Copy_FromBufChain (pPkt, pOxmFld->au1Fld,
                                           (UINT4) (u2Offset +
                                                    (UINT2) OFC_L4_DST_OFFSET),
                                           OFC_L4_ADDR_LEN);
                u4TempLength = OFC_L4_ADDR_LEN;
                break;

            case OFCXMT_OFB_UDP_SRC:
                u4RetVal = Ofc131ActsComputeHdrOffset (pPkt, OFC_L4PROTO_UDP,
                                                       &u2ActType, &u2Offset);
                if (OFC_FAILURE == u4RetVal)
                {
                    OFC_TRC_FUNC ((OFC_ACTS_TRC, "MatchField UDP Source - "
                                   "No UDP Header, FAILURE\n"));
                    OfcMemFreeForFlowMatch (OFC_MINUS_ONE,
                                            (UINT1 *) pFlowWcMatch);
                    return OFC_FAILURE;
                }
                CRU_BUF_Copy_FromBufChain (pPkt, pOxmFld->au1Fld,
                                           (UINT4) u2Offset, OFC_L4_ADDR_LEN);
                u4TempLength = OFC_L4_ADDR_LEN;
                break;

            case OFCXMT_OFB_UDP_DST:
                u4RetVal = Ofc131ActsComputeHdrOffset (pPkt, OFC_L4PROTO_UDP,
                                                       &u2ActType, &u2Offset);
                if (OFC_FAILURE == u4RetVal)
                {
                    OFC_TRC_FUNC ((OFC_ACTS_TRC, "MatchField UDP Destination - "
                                   "No UDP Header, FAILURE\n"));
                    OfcMemFreeForFlowMatch (OFC_MINUS_ONE,
                                            (UINT1 *) pFlowWcMatch);
                    return OFC_FAILURE;
                }
                CRU_BUF_Copy_FromBufChain (pPkt, pOxmFld->au1Fld,
                                           (UINT4) (u2Offset +
                                                    (UINT2) OFC_L4_DST_OFFSET),
                                           OFC_L4_ADDR_LEN);
                u4TempLength = OFC_L4_ADDR_LEN;
                break;

            case OFCXMT_OFB_SCTP_SRC:
                u4RetVal = Ofc131ActsComputeHdrOffset (pPkt, OFC_L4PROTO_SCTP,
                                                       &u2ActType, &u2Offset);
                if (OFC_FAILURE == u4RetVal)
                {
                    OFC_TRC_FUNC ((OFC_ACTS_TRC, "MatchField SCTP Source - "
                                   "No SCTP Header, FAILURE\n"));
                    OfcMemFreeForFlowMatch (OFC_MINUS_ONE,
                                            (UINT1 *) pFlowWcMatch);
                    return OFC_FAILURE;
                }
                CRU_BUF_Copy_FromBufChain (pPkt, pOxmFld->au1Fld,
                                           (UINT4) u2Offset, OFC_L4_ADDR_LEN);
                u4TempLength = OFC_L4_ADDR_LEN;
                break;

            case OFCXMT_OFB_SCTP_DST:
                u4RetVal = Ofc131ActsComputeHdrOffset (pPkt, OFC_L4PROTO_SCTP,
                                                       &u2ActType, &u2Offset);
                if (OFC_FAILURE == u4RetVal)
                {
                    OFC_TRC_FUNC ((OFC_ACTS_TRC,
                                   "MatchField SCTP Destination - "
                                   "No SCTP Header, FAILURE\n"));
                    OfcMemFreeForFlowMatch (OFC_MINUS_ONE,
                                            (UINT1 *) pFlowWcMatch);
                    return OFC_FAILURE;
                }
                CRU_BUF_Copy_FromBufChain (pPkt, pOxmFld->au1Fld,
                                           (UINT4) (u2Offset +
                                                    (UINT2) OFC_L4_DST_OFFSET),
                                           OFC_L4_ADDR_LEN);
                u4TempLength = OFC_L4_ADDR_LEN;
                break;

            case OFCXMT_OFB_ICMPV4_TYPE:
                u4RetVal = Ofc131ActsComputeHdrOffset (pPkt, OFC_L4PROTO_ICMP,
                                                       &u2ActType, &u2Offset);
                if (OFC_FAILURE == u4RetVal)
                {
                    OFC_TRC_FUNC ((OFC_ACTS_TRC, "MatchField ICMPv4 Type - "
                                   "No ICMPv4 Header, FAILURE\n"));
                    OfcMemFreeForFlowMatch (OFC_MINUS_ONE,
                                            (UINT1 *) pFlowWcMatch);
                    return OFC_FAILURE;
                }
                CRU_BUF_Copy_FromBufChain (pPkt, pOxmFld->au1Fld,
                                           (UINT4) u2Offset, OFC_ONE);
                u4TempLength = OFC_ONE;
                break;

            case OFCXMT_OFB_ICMPV4_CODE:
                u4RetVal = Ofc131ActsComputeHdrOffset (pPkt, OFC_L4PROTO_ICMP,
                                                       &u2ActType, &u2Offset);
                if (OFC_FAILURE == u4RetVal)
                {
                    OFC_TRC_FUNC ((OFC_ACTS_TRC, "MatchField ICMPv4 Code - "
                                   "No ICMPv4 Header, FAILURE\n"));
                    OfcMemFreeForFlowMatch (OFC_MINUS_ONE,
                                            (UINT1 *) pFlowWcMatch);
                    return OFC_FAILURE;
                }
                CRU_BUF_Copy_FromBufChain (pPkt, pOxmFld->au1Fld,
                                           (UINT4) (u2Offset +
                                                    OFC_ICMP4_CODE_OFFSET),
                                           OFC_ONE);
                u4TempLength = OFC_ONE;
                break;

            case OFCXMT_OFB_ARP_OP:
                u4RetVal = Ofc131ActsComputeHdrOffset (pPkt, OFC_L3_ARP,
                                                       &u2ActType, &u2Offset);
                if (OFC_FAILURE == u4RetVal)
                {
                    OFC_TRC_FUNC ((OFC_ACTS_TRC, "MatchField ARP OpCode - "
                                   "No ARP Header, FAILURE\n"));
                    OfcMemFreeForFlowMatch (OFC_MINUS_ONE,
                                            (UINT1 *) pFlowWcMatch);
                    return OFC_FAILURE;
                }
                CRU_BUF_Copy_FromBufChain (pPkt, pOxmFld->au1Fld,
                                           (UINT4) (u2Offset +
                                                    OFC_ARP_OP_OFFSET),
                                           OFC_ARP_OP_LEN);
                u4TempLength = OFC_ARP_OP_LEN;
                break;

            case OFCXMT_OFB_ARP_SPA:
                u4RetVal = Ofc131ActsComputeHdrOffset (pPkt, OFC_L3_ARP,
                                                       &u2ActType, &u2Offset);
                if (OFC_FAILURE == u4RetVal)
                {
                    OFC_TRC_FUNC ((OFC_ACTS_TRC, "MatchField ARP Source IPv4 "
                                   "Address - No ARP Header, FAILURE\n"));
                    OfcMemFreeForFlowMatch (OFC_MINUS_ONE,
                                            (UINT1 *) pFlowWcMatch);
                    return OFC_FAILURE;
                }
                CRU_BUF_Copy_FromBufChain (pPkt, pOxmFld->au1Fld,
                                           (UINT4) (u2Offset +
                                                    OFC_ARP_SPA_OFFSET),
                                           OFC_ARP_PA_LEN);
                u4TempLength = OFC_ARP_PA_LEN;
                break;

            case OFCXMT_OFB_ARP_TPA:
                u4RetVal = Ofc131ActsComputeHdrOffset (pPkt, OFC_L3_ARP,
                                                       &u2ActType, &u2Offset);
                if (OFC_FAILURE == u4RetVal)
                {
                    OFC_TRC_FUNC ((OFC_ACTS_TRC, "MatchField ARP Target IPv4 "
                                   "Address - No ARP Header, FAILURE\n"));
                    OfcMemFreeForFlowMatch (OFC_MINUS_ONE,
                                            (UINT1 *) pFlowWcMatch);
                    return OFC_FAILURE;
                }
                CRU_BUF_Copy_FromBufChain (pPkt, pOxmFld->au1Fld,
                                           (UINT4) (u2Offset +
                                                    OFC_ARP_TPA_OFFSET),
                                           OFC_ARP_PA_LEN);
                u4TempLength = OFC_ARP_PA_LEN;
                break;

            case OFCXMT_OFB_ARP_SHA:
                u4RetVal = Ofc131ActsComputeHdrOffset (pPkt, OFC_L3_ARP,
                                                       &u2ActType, &u2Offset);
                if (OFC_FAILURE == u4RetVal)
                {
                    OFC_TRC_FUNC ((OFC_ACTS_TRC,
                                   "MatchField ARP Source Hardware"
                                   " Address - No ARP Header, FAILURE\n"));
                    OfcMemFreeForFlowMatch (OFC_MINUS_ONE,
                                            (UINT1 *) pFlowWcMatch);
                    return OFC_FAILURE;
                }
                CRU_BUF_Copy_FromBufChain (pPkt, pOxmFld->au1Fld,
                                           (UINT4) (u2Offset +
                                                    OFC_ARP_SHA_OFFSET),
                                           OFC_ARP_HA_LEN);
                u4TempLength = OFC_ARP_HA_LEN;
                break;

            case OFCXMT_OFB_ARP_THA:
                u4RetVal = Ofc131ActsComputeHdrOffset (pPkt, OFC_L3_ARP,
                                                       &u2ActType, &u2Offset);
                if (OFC_FAILURE == u4RetVal)
                {
                    OFC_TRC_FUNC ((OFC_ACTS_TRC,
                                   "MatchField ARP Target Hardware"
                                   " Address - No ARP Header, FAILURE\n"));
                    OfcMemFreeForFlowMatch (OFC_MINUS_ONE,
                                            (UINT1 *) pFlowWcMatch);
                    return OFC_FAILURE;
                }
                CRU_BUF_Copy_FromBufChain (pPkt, pOxmFld->au1Fld,
                                           (UINT4) (u2Offset +
                                                    OFC_ARP_THA_OFFSET),
                                           OFC_ARP_HA_LEN);
                u4TempLength = OFC_ARP_HA_LEN;
                break;

            case OFCXMT_OFB_IPV6_SRC:
                u4RetVal = Ofc131ActsComputeHdrOffset (pPkt, OFC_IP_PROTO,
                                                       &u2ActType, &u2Offset);
                if (OFC_FAILURE == u4RetVal)
                {
                    OFC_TRC_FUNC ((OFC_ACTS_TRC, "MatchField IPv6 Source "
                                   "Address - No IP Header, FAILURE\n"));
                    OfcMemFreeForFlowMatch (OFC_MINUS_ONE,
                                            (UINT1 *) pFlowWcMatch);
                    return OFC_FAILURE;
                }

                if (u2ActType == OFC_L3_IP6)
                {
                    CRU_BUF_Copy_FromBufChain (pPkt, pOxmFld->au1Fld,
                                               (UINT4) (u2Offset +
                                                        OFC_IPV6_SRC_OFFSET),
                                               OFC_IPV6_ADDR_LEN);
                    u4TempLength = OFC_IPV6_ADDR_LEN;
                }
                else if (u2ActType == OSIX_HTONS (OFC_L2EXT_MPLS_1))
                {
                    u4RetVal = Ofc131ActsPrepareKeyFromMplsPkt (pPkt, pOxmFld,
                                                                u2Offset,
                                                                &u4TempLength,
                                                                u1Field,
                                                                pFlowWcMatch);
                    if (u4RetVal == OFC_FAILURE)
                    {
                        OFC_TRC_FUNC ((OFC_ACTS_TRC, "MatchField IPv4 Source - "
                                       "No IPv4 Header, FAILURE\n"));
                        return OFC_FAILURE;
                    }
                }
                else
                {
                    /* It is IPv4 */
                    OFC_TRC_FUNC ((OFC_ACTS_TRC, "MatchField IPv6 Source "
                                   "Address - No IPv6 Header, FAILURE\n"));
                    OfcMemFreeForFlowMatch (OFC_MINUS_ONE,
                                            (UINT1 *) pFlowWcMatch);
                    return OFC_FAILURE;
                }
                break;

            case OFCXMT_OFB_IPV6_DST:
                u4RetVal = Ofc131ActsComputeHdrOffset (pPkt, OFC_IP_PROTO,
                                                       &u2ActType, &u2Offset);
                if (OFC_FAILURE == u4RetVal)
                {
                    OFC_TRC_FUNC ((OFC_ACTS_TRC, "MatchField IPv6 Destination "
                                   "Address - No IP Header, FAILURE\n"));
                    OfcMemFreeForFlowMatch (OFC_MINUS_ONE,
                                            (UINT1 *) pFlowWcMatch);
                    return OFC_FAILURE;
                }

                if (u2ActType == OFC_L3_IP6)
                {
                    CRU_BUF_Copy_FromBufChain (pPkt, pOxmFld->au1Fld,
                                               (UINT4) (u2Offset +
                                                        OFC_IPV6_DST_OFFSET),
                                               OFC_IPV6_ADDR_LEN);
                    u4TempLength = OFC_IPV6_ADDR_LEN;
                }
                else if (u2ActType == OSIX_HTONS (OFC_L2EXT_MPLS_1))
                {
                    u4RetVal = Ofc131ActsPrepareKeyFromMplsPkt (pPkt, pOxmFld,
                                                                u2Offset,
                                                                &u4TempLength,
                                                                u1Field,
                                                                pFlowWcMatch);
                    if (u4RetVal == OFC_FAILURE)
                    {
                        OFC_TRC_FUNC ((OFC_ACTS_TRC, "MatchField IPv4 Source - "
                                       "No IPv4 Header, FAILURE\n"));
                        return OFC_FAILURE;
                    }
                }
                else
                {
                    /* It is IPv4 */
                    OFC_TRC_FUNC ((OFC_ACTS_TRC, "MatchField IPv6 Destination "
                                   "Address - No IPv6 Header, FAILURE\n"));
                    OfcMemFreeForFlowMatch (OFC_MINUS_ONE,
                                            (UINT1 *) pFlowWcMatch);
                    return OFC_FAILURE;
                }
                break;

            case OFCXMT_OFB_IPV6_FLABEL:
                u4RetVal = Ofc131ActsComputeHdrOffset (pPkt, OFC_IP_PROTO,
                                                       &u2ActType, &u2Offset);
                if (OFC_FAILURE == u4RetVal)
                {
                    OFC_TRC_FUNC ((OFC_ACTS_TRC, "MatchField IPv6 Flow Label "
                                   "- No IP Header, FAILURE\n"));
                    OfcMemFreeForFlowMatch (OFC_MINUS_ONE,
                                            (UINT1 *) pFlowWcMatch);
                    return OFC_FAILURE;
                }

                if (u2ActType == OFC_L3_IP6)
                {
                    /* Copy Flow Label */
                    CRU_BUF_Copy_FromBufChain (pPkt, (UINT1 *) &u4FourByte,
                                               (UINT4) u2Offset, OFC_FOUR);

                    u4FourByte = OSIX_NTOHL (u4FourByte);
                    u4FourByte = u4FourByte & OFC_PK_IPV6_FLABEL_MASK;
                    u4FourByte = OSIX_HTONL (u4FourByte);
                    MEMCPY (pOxmFld->au1Fld, &u4FourByte, OFC_FOUR);
                    u4TempLength = OFC_FOUR;
                }
                else
                {
                    /* It is IPv4 */
                    OFC_TRC_FUNC ((OFC_ACTS_TRC, "MatchField IPv6 Flow Label "
                                   "- No IPv6 Header, FAILURE\n"));
                    OfcMemFreeForFlowMatch (OFC_MINUS_ONE,
                                            (UINT1 *) pFlowWcMatch);
                    return OFC_FAILURE;
                }
                break;

            case OFCXMT_OFB_ICMPV6_TYPE:
                u4RetVal = Ofc131ActsComputeHdrOffset (pPkt, OFC_L4PROTO_ICMP6,
                                                       NULL, &u2Offset);
                if (OFC_FAILURE == u4RetVal)
                {
                    OFC_TRC_FUNC ((OFC_ACTS_TRC, "MatchField ICMPv6 Type - "
                                   "No ICMPv6 Header, FAILURE\n"));
                    OfcMemFreeForFlowMatch (OFC_MINUS_ONE,
                                            (UINT1 *) pFlowWcMatch);
                    return OFC_FAILURE;
                }

                CRU_BUF_Copy_FromBufChain (pPkt, pOxmFld->au1Fld,
                                           (UINT4) u2Offset, OFC_ONE);
                u4TempLength = OFC_ONE;
                break;

            case OFCXMT_OFB_ICMPV6_CODE:
                u4RetVal = Ofc131ActsComputeHdrOffset (pPkt, OFC_L4PROTO_ICMP6,
                                                       NULL, &u2Offset);
                if (OFC_FAILURE == u4RetVal)
                {
                    OFC_TRC_FUNC ((OFC_ACTS_TRC, "MatchField ICMPv6 Code - "
                                   "No ICMPv6 Header, FAILURE\n"));
                    OfcMemFreeForFlowMatch (OFC_MINUS_ONE,
                                            (UINT1 *) pFlowWcMatch);
                    return OFC_FAILURE;
                }

                CRU_BUF_Copy_FromBufChain (pPkt, pOxmFld->au1Fld,
                                           (UINT4) (u2Offset +
                                                    OFC_ICMP6_CODE_OFFSET),
                                           OFC_ONE);
                u4TempLength = OFC_ONE;
                break;

            case OFCXMT_OFB_IPV6_ND_SLL:
                u4RetVal = Ofc131ActsComputeHdrOffset (pPkt, OFC_L3_ND,
                                                       NULL, &u2Offset);
                if (OFC_FAILURE == u4RetVal)
                {
                    OFC_TRC_FUNC ((OFC_ACTS_TRC, "MatchField ND SLL "
                                   "- No ND Header, FAILURE\n"));
                    OfcMemFreeForFlowMatch (OFC_MINUS_ONE,
                                            (UINT1 *) pFlowWcMatch);
                    return OFC_FAILURE;
                }
                /* Fetch the ND Type */
                CRU_BUF_Copy_FromBufChain (pPkt, &u1OneByte,
                                           (UINT4) u2Offset, OFC_ONE);
                /* Fetch The ND Option Offset */
                switch (u1OneByte)
                {
                    case ROUTER_SOLICITATION:
                        u2TempOffset = OFC_ND_RS_OPTION_OFFSET;
                        break;

                    case ROUTER_ADVERTISEMENT:
                        u2TempOffset = OFC_ND_RA_OPTION_OFFSET;
                        break;

                    case NEIGHBOR_SOLICITATION:
                        u2TempOffset = OFC_ND_NS_OPTION_OFFSET;
                        break;

                    default:
                        /* Invalid ND Type */
                        OFC_TRC_FUNC ((OFC_ACTS_TRC, "MatchField ND SLL - "
                                       "Invalid ND Type , FAILURE\n"));
                        OfcMemFreeForFlowMatch (OFC_MINUS_ONE,
                                                (UINT1 *) pFlowWcMatch);
                        return OFC_FAILURE;
                }
                u2Offset = (UINT2) (u2Offset + u2TempOffset);

                /* Copy the ND Option */
                CRU_BUF_Copy_FromBufChain (pPkt, &u1OneByte,
                                           (UINT4) u2Offset, (UINT4) OFC_ONE);
                /* Copy the ND Option Length, in case any */
                CRU_BUF_Copy_FromBufChain (pPkt, &u1TempOneByte,
                                           (UINT4) (u2Offset + (UINT2) OFC_ONE),
                                           (UINT4) OFC_ONE);

                if (ND_SLL == u1OneByte)
                {
                    CRU_BUF_Copy_FromBufChain (pPkt, pOxmFld->au1Fld,
                                               (UINT4) (u2Offset +
                                                        (UINT2) OFC_TWO),
                                               (UINT4) (u1TempOneByte -
                                                        (UINT1) OFC_TWO));
                    u4TempLength = (UINT4) (u1TempOneByte - (UINT1) OFC_TWO);
                }
                else
                {
                    /* No ND SLL present */
                    OFC_TRC_FUNC ((OFC_ACTS_TRC, "MatchField ND SLL - "
                                   "No ND SLL present, FAILURE\n"));
                    OfcMemFreeForFlowMatch (OFC_MINUS_ONE,
                                            (UINT1 *) pFlowWcMatch);
                    return OFC_FAILURE;
                }
                break;

            case OFCXMT_OFB_IPV6_ND_TLL:
                u4RetVal = Ofc131ActsComputeHdrOffset (pPkt, OFC_L3_ND,
                                                       NULL, &u2Offset);
                if (OFC_FAILURE == u4RetVal)
                {
                    OFC_TRC_FUNC ((OFC_ACTS_TRC, "MatchField ND TLL - "
                                   "No ND Header, FAILURE\n"));
                    OfcMemFreeForFlowMatch (OFC_MINUS_ONE,
                                            (UINT1 *) pFlowWcMatch);
                    return OFC_FAILURE;
                }
                /* Fetch the ND Type */
                CRU_BUF_Copy_FromBufChain (pPkt, &u1OneByte, (UINT4) u2Offset,
                                           OFC_ONE);
                /* Fetch The ND Option Offset */
                switch (u1OneByte)
                {
                    case NEIGHBOR_ADVERTISEMENT:
                        u2TempOffset = (UINT2) OFC_ND_NA_OPTION_OFFSET;
                        break;

                    case REDIRECT:
                        u2TempOffset = (UINT2) OFC_ND_RD_OPTION_OFFSET;
                        break;

                    default:
                        /* Invalid ND Type */
                        OFC_TRC_FUNC ((OFC_ACTS_TRC, "MatchField ND TLL - "
                                       "Invalid ND Type, FAILURE\n"));
                        OfcMemFreeForFlowMatch (OFC_MINUS_ONE,
                                                (UINT1 *) pFlowWcMatch);
                        return OFC_FAILURE;
                }
                u2Offset = (UINT2) (u2Offset + u2TempOffset);

                /* Copy the ND Option */
                CRU_BUF_Copy_FromBufChain (pPkt, &u1OneByte,
                                           (UINT4) u2Offset, OFC_ONE);

                /* Copy the ND Option Length, in case any */
                CRU_BUF_Copy_FromBufChain (pPkt, &u1TempOneByte,
                                           (UINT4) (u2Offset + (UINT2) OFC_ONE),
                                           (UINT4) OFC_ONE);

                if (ND_TLL == u1OneByte)
                {
                    CRU_BUF_Copy_FromBufChain (pPkt, pOxmFld->au1Fld,
                                               (UINT4) (u2Offset +
                                                        (UINT2) OFC_TWO),
                                               (UINT4) (u1TempOneByte -
                                                        (UINT1) OFC_TWO));
                    u4TempLength = (UINT4) (u1TempOneByte - (UINT1) OFC_TWO);
                }
                else
                {
                    /* No ND TLL present */
                    OFC_TRC_FUNC ((OFC_ACTS_TRC, "MatchField ND TLL - "
                                   "No ND TLL present, FAILURE\n"));
                    OfcMemFreeForFlowMatch (OFC_MINUS_ONE,
                                            (UINT1 *) pFlowWcMatch);
                    return OFC_FAILURE;
                }
                break;

            case OFCXMT_OFB_IPV6_ND_TARGET:
                u4RetVal = Ofc131ActsComputeHdrOffset (pPkt, OFC_L3_ND,
                                                       NULL, &u2Offset);
                if (OFC_FAILURE == u4RetVal)
                {
                    OFC_TRC_FUNC ((OFC_ACTS_TRC, "MatchField ND Target - "
                                   "No ND Header, FAILURE\n"));
                    OfcMemFreeForFlowMatch (OFC_MINUS_ONE,
                                            (UINT1 *) pFlowWcMatch);
                    return OFC_FAILURE;
                }
                /* Fetch the ND Type */
                CRU_BUF_Copy_FromBufChain (pPkt, &u1OneByte,
                                           (UINT4) u2Offset, OFC_ONE);
                /* Fetch The ND Target Offset */
                switch (u1OneByte)
                {
                    case NEIGHBOR_SOLICITATION:
                        break;

                    case NEIGHBOR_ADVERTISEMENT:
                        break;

                    case REDIRECT:
                        break;

                    default:
                        /* Invalid Type */
                        OFC_TRC_FUNC ((OFC_ACTS_TRC, "MatchField ND Target - "
                                       "Invalid Type, FAILURE\n"));
                        OfcMemFreeForFlowMatch (OFC_MINUS_ONE,
                                                (UINT1 *) pFlowWcMatch);
                        return OFC_FAILURE;
                }
                u2Offset = (UINT2) (u2Offset + OFC_ND_TARGET_OFFSET);
                /* Copy the Target IPv6 Address */
                CRU_BUF_Copy_FromBufChain (pPkt, pOxmFld->au1Fld,
                                           (UINT4) u2Offset, OFC_SIXTEEN);
                u4TempLength = OFC_SIXTEEN;
                break;

            case OFCXMT_OFB_MPLS_LABEL:
                u4RetVal = Ofc131ActsComputeHdrOffset (pPkt, OFC_MPLS_PROTO,
                                                       &u2ActType, &u2Offset);
                if (OFC_FAILURE == u4RetVal)
                {
                    OFC_TRC_FUNC ((OFC_ACTS_TRC, "MatchField MPLS Label - "
                                   "No MPLS Header, FAILURE\n"));
                    OfcMemFreeForFlowMatch (OFC_MINUS_ONE,
                                            (UINT1 *) pFlowWcMatch);
                    return OFC_FAILURE;
                }
                /* Copy Flow Label */
                CRU_BUF_Copy_FromBufChain (pPkt, (UINT1 *) &u4FourByte,
                                           (UINT4) u2Offset, OFC_FOUR);

                u4FourByte = OSIX_NTOHL (u4FourByte);
                u4FourByte = u4FourByte >> OFC_TWELVE;
                u4FourByte = OSIX_HTONL (u4FourByte);
                MEMCPY (pOxmFld->au1Fld, &u4FourByte, OFC_FOUR);
                u4TempLength = OFC_FOUR;
                break;

            case OFCXMT_OFB_MPLS_TC:
                u4RetVal = Ofc131ActsComputeHdrOffset (pPkt, OFC_MPLS_PROTO,
                                                       &u2ActType, &u2Offset);
                if (OFC_FAILURE == u4RetVal)
                {
                    OFC_TRC_FUNC ((OFC_ACTS_TRC, "MatchField MPLS TC - "
                                   "No MPLS Header, FAILURE\n"));
                    OfcMemFreeForFlowMatch (OFC_MINUS_ONE,
                                            (UINT1 *) pFlowWcMatch);
                    return OFC_FAILURE;
                }

                /* Copy TC */
                CRU_BUF_Copy_FromBufChain (pPkt, &u1OneByte,
                                           (UINT4) (u2Offset +
                                                    OFC_MPLS_LABEL16_TC_BOS_OFFSET),
                                           OFC_ONE);

                u1OneByte = u1OneByte & OFC_PK_MPLS_TC_MASK;
                u1OneByte = u1OneByte >> OFC_ONE;
                MEMCPY (pOxmFld->au1Fld, &u1OneByte, OFC_ONE);
                u4TempLength = OFC_ONE;
                break;

            case OFCXMT_OFB_MPLS_BOS:
                u4RetVal = Ofc131ActsComputeHdrOffset (pPkt, OFC_MPLS_PROTO,
                                                       &u2ActType, &u2Offset);
                if (OFC_FAILURE == u4RetVal)
                {
                    OFC_TRC_FUNC ((OFC_ACTS_TRC, "MatchField MPLS BoS - "
                                   "No MPLS Header, FAILURE\n"));
                    OfcMemFreeForFlowMatch (OFC_MINUS_ONE,
                                            (UINT1 *) pFlowWcMatch);
                    return OFC_FAILURE;
                }

                /* Copy BoS */
                CRU_BUF_Copy_FromBufChain (pPkt, &u1OneByte,
                                           (UINT4) (u2Offset +
                                                    OFC_MPLS_LABEL16_TC_BOS_OFFSET),
                                           OFC_ONE);

                u1OneByte = u1OneByte & OFC_PK_MPLS_BOS_MASK;
                MEMCPY (pOxmFld->au1Fld, &u1OneByte, OFC_ONE);
                u4TempLength = OFC_ONE;
                break;

            case OFCXMT_OFB_PBB_ISID:
                u4RetVal = Ofc131ActsComputeHdrOffset (pPkt, OFC_L2EXT_PBB,
                                                       NULL, &u2Offset);
                if (OFC_FAILURE == u4RetVal)
                {
                    OFC_TRC_FUNC ((OFC_ACTS_TRC, "MatchField PBB ISID - "
                                   "No PBB Header, FAILURE\n"));
                    OfcMemFreeForFlowMatch (OFC_MINUS_ONE,
                                            (UINT1 *) pFlowWcMatch);
                    return OFC_FAILURE;
                }

                /* Copy the existing Flow Lable, CoS and TTL */
                CRU_BUF_Copy_FromBufChain (pPkt, (UINT1 *) &u4FourByte,
                                           (UINT4) (u2Offset +
                                                    (UINT2)
                                                    OFC_PBB_ISID_OFFSET),
                                           (UINT4) OFC_PBB_ISID_SIZE);

                u4FourByte = OSIX_NTOHL (u4FourByte);
                u4FourByte = u4FourByte & OFC_PK_PBB_ISID_MASK;
                u4FourByte = OSIX_HTONL (u4FourByte);
                MEMCPY (pOxmFld->au1Fld, &u4FourByte, OFC_FOUR);
                u4TempLength = OFC_FOUR;
                break;

            case OFCXMT_OFB_TUNNEL_ID:
                /* Will be default zero */
                break;

            case OFCXMT_OFB_IPV6_EXTHDR:
                u1OneByte = OFC_ZERO;
                u2TwoByte = OFC_ZERO;

                u4RetVal = Ofc131ActsComputeHdrOffset (pPkt, OFC_IP_PROTO,
                                                       &u2ActType, &u2Offset);
                if (OFC_FAILURE == u4RetVal)
                {
                    OFC_TRC_FUNC ((OFC_ACTS_TRC, "MatchField IPv6 Extention "
                                   "Header - No IP Header, FAILURE\n"));
                    OfcMemFreeForFlowMatch (OFC_MINUS_ONE,
                                            (UINT1 *) pFlowWcMatch);
                    return OFC_FAILURE;
                }

                if (u2ActType == OFC_L3_IP6)
                {
                    Ofc131ActsSetIPExtHdrsBM (pPkt, u2Offset, &u2TwoByte);
                    u2TwoByte = OSIX_HTONS (u2TwoByte);
                    MEMCPY (pOxmFld->au1Fld, &u2TwoByte, OFC_TWO);
                    u4TempLength = OFC_TWO;
                }
                else
                {
                    /* It is IPv4 */
                    OFC_TRC_FUNC ((OFC_ACTS_TRC, "MatchField IPv6 Extension "
                                   "Header - No IPv6 Header, FAILURE\n"));
                    OfcMemFreeForFlowMatch (OFC_MINUS_ONE,
                                            (UINT1 *) pFlowWcMatch);
                    return OFC_FAILURE;
                }
                break;

            default:
                /* Unknown Match Field */
                OFC_TRC_FUNC ((OFC_ACTS_TRC, "Unknown MatchField, FAILURE\n"));
                OfcMemFreeForFlowMatch (OFC_MINUS_ONE, (UINT1 *) pFlowWcMatch);
                return OFC_FAILURE;
        }
        pFlowWcMatch->u2Type = u1Field;
        u1NumBits = (UINT1) (u1NumBits - OFC_ONE);
        u1Field = (UINT1) (u1Field + OFC_ONE);

        /* Add it to the list */
        TMO_SLL_Add (pKeyMatchList, &pFlowWcMatch->Node);

        /* Prepare the Exact Match */
        u4ExactLength = (UINT4) (u4ExactLength + u4TempLength);
        if (u4ExactLength > MAX_OFC_EXACT_LENGTH)
        {
            OFC_TRC_FUNC ((OFC_ACTS_TRC, "Exact Match exceeding max length\n"));
            return OFC_FAILURE;
        }
        MEMCPY (pu1ExactMatch, pOxmFld->au1Fld, u4TempLength);
        pu1ExactMatch = pu1ExactMatch + u4TempLength;
    }
    return OFC_SUCCESS;
}

/******************************************************************************
 * Function    :  Ofc131ActsFlowExtract
 * Description :  This routine extracts the Flow Match fields from
 *                received data packet.
 * Input       :  pPkt      - Pointer to the received packet buffer.
 *                u4IfIndex - Interface Index.
 * Output      :  pFlow     - Pointer to the Flow Match Structure.
 * Returns     :  OFC_SUCCESS or OFC_FAILURE
 *****************************************************************************/
#ifdef NPAPI_WANTED
#ifndef OPENFLOW_TCAM
PRIVATE UINT4
Ofc131ActsFlowExtract (UINT1 *pPkt, UINT4 u4IfIndex, tOfcExactFlowMatch * pFlow)
{
    tEnetV2Header      *pEthHdr = NULL;
    t_IP_HEADER        *pIpHdr = NULL;
    tIp6Hdr            *pIp6Hdr = NULL;
    tArpHdr            *pArpHdr = NULL;
    tTcpHdr            *pTcpHdr = NULL;
    tUdpHdr            *pUdpHdr = NULL;
    tIcmpHdr           *pIcmpHdr = NULL;

    OFC_TRC_FUNC ((OFC_FN_ENTRY, "FUNC:Ofc131ActsFlowExtract\n"));

    pFlow->u4InIfIndex = u4IfIndex;

    /* Link layer. */
    pEthHdr = (tEnetV2Header *) ((VOID *) pPkt);
    MEMCPY (pFlow->au1DstMacAddr, pEthHdr->au1DstAddr, CFA_ENET_ADDR_LEN);
    MEMCPY (pFlow->au1SrcMacAddr, pEthHdr->au1SrcAddr, CFA_ENET_ADDR_LEN);

    pFlow->u2EthType = OSIX_NTOHS (pEthHdr->u2LenOrType);

    /* Network layer. */
    if (pFlow->u2EthType == CFA_ENET_IPV4)
    {
        pIpHdr = (t_IP_HEADER *) ((VOID *) (pPkt + sizeof (tEnetV2Header)));

        if (pIpHdr)
        {
            pFlow->u4Ip4Src = OSIX_NTOHL (pIpHdr->u4Src);
            pFlow->u4Ip4Dst = OSIX_NTOHL (pIpHdr->u4Dest);
            pFlow->u1IpProto = pIpHdr->u1Proto;

            if (!(pIpHdr->u2Fl_offs & OSIX_HTONS (IP_FRAG_OFF_MASK)))
            {
                if (pFlow->u1IpProto == CFA_TCP)
                {
                    pTcpHdr = (tTcpHdr *) ((VOID *) (pPkt +
                                                     sizeof (tEnetV2Header) +
                                                     CFA_IP_HDR_LEN));
                    pFlow->u2TpSrc = OSIX_NTOHS (pTcpHdr->u2Src);
                    pFlow->u2TpDst = OSIX_NTOHS (pTcpHdr->u2Dst);
                }
                else if (pFlow->u1IpProto == CFA_UDP)
                {
                    pUdpHdr = (tUdpHdr *) ((VOID *) (pPkt +
                                                     sizeof (tEnetV2Header) +
                                                     CFA_IP_HDR_LEN));
                    pFlow->u2TpSrc = OSIX_NTOHS (pUdpHdr->u2Src);
                    pFlow->u2TpDst = OSIX_NTOHS (pUdpHdr->u2Dst);
                }
                else if (pFlow->u1IpProto == CFA_ICMP)
                {
                    pIcmpHdr = (tIcmpHdr *) ((VOID *) (pPkt +
                                                       sizeof (tEnetV2Header) +
                                                       CFA_IP_HDR_LEN));
                    /* Fill in the TCP Fields itself */
                    pFlow->u2TpSrc = pIcmpHdr->u1Type;
                    pFlow->u2TpDst = pIcmpHdr->u1Code;
                }
            }
        }
    }
    else if (pFlow->u2EthType == OFC_L3_IP6)
    {
        pIp6Hdr = (tIp6Hdr *) ((VOID *) (pPkt + sizeof (tEnetV2Header)));

        if (pIp6Hdr)
        {
            MEMCPY (pFlow->au1Ip6Src.u1_addr, pIp6Hdr->srcAddr.u1_addr,
                    IP6_ADDR_SIZE);
            MEMCPY (pFlow->au1Ip6Dst.u1_addr, pIp6Hdr->dstAddr.u1_addr,
                    IP6_ADDR_SIZE);

            pFlow->u1IpProto = pIp6Hdr->u1Nh;

            if (pFlow->u1IpProto == CFA_TCP)
            {
                pTcpHdr = (tTcpHdr *) ((VOID *) (pPkt +
                                                 sizeof (tEnetV2Header) +
                                                 sizeof (tIp6Hdr)));
                pFlow->u2TpSrc = OSIX_NTOHS (pTcpHdr->u2Src);
                pFlow->u2TpDst = OSIX_NTOHS (pTcpHdr->u2Dst);
            }
            else if (pFlow->u1IpProto == CFA_UDP)
            {
                pUdpHdr = (tUdpHdr *) ((VOID *) (pPkt +
                                                 sizeof (tEnetV2Header) +
                                                 sizeof (tIp6Hdr)));
                pFlow->u2TpSrc = OSIX_NTOHS (pUdpHdr->u2Src);
                pFlow->u2TpDst = OSIX_NTOHS (pUdpHdr->u2Dst);
            }
            else if (pFlow->u1IpProto == NH_ICMP6)
            {
                pIcmpHdr = (tIcmpHdr *) ((VOID *) (pPkt +
                                                   sizeof (tEnetV2Header) +
                                                   sizeof (tIp6Hdr)));
                /* Fill in the TCP Fields itself */
                pFlow->u2TpSrc = pIcmpHdr->u1Type;
                pFlow->u2TpDst = pIcmpHdr->u1Code;
            }
        }
    }
    else if (pFlow->u2EthType == CFA_ENET_ARP)
    {
        pArpHdr = (tArpHdr *) (((VOID *) pPkt + sizeof (tEnetV2Header)));
        pFlow->u1IpProto = (UINT1) OSIX_NTOHS (pArpHdr->u2OpCode);
        MEMCPY (&(pFlow->u4Ip4Src), pPkt + sizeof (tArpHdr) + OFC_SIX,
                OFC_FOUR);
        MEMCPY (&(pFlow->u4Ip4Dst), pPkt + sizeof (tArpHdr) + OFC_SIX +
                OFC_FOUR + OFC_SIX, OFC_FOUR);
        pFlow->u4Ip4Src = OSIX_NTOHL (pFlow->u4Ip4Src);
        pFlow->u4Ip4Dst = OSIX_NTOHL (pFlow->u4Ip4Dst);

    }
    else
    {
        /* Unsupported Header */
        OFC_TRC_FUNC ((OFC_ACTS_TRC, "Unsupported Header, Type = %d, "
                       "return FAILURE\n", pFlow->u2EthType));
        return OFC_FAILURE;
    }
    OFC_TRC_FUNC ((OFC_FN_EXIT, "FUNC:Ofc131ActsFlowExtract SUCCESS\n"));
    return OFC_SUCCESS;
}
#endif
#endif

/******************************************************************************
 * Function    : Ofc131ActsPushVlanTag 
 * Description : This routine pushes a new VLAN header onto the packet. 
 * Input       : pPkt - Pointer to the packet CRU buffer. 
 * Output      : None
 * Returns     : OFC_SUCCESS/OFC_FAILURE  
 *****************************************************************************/
PRIVATE UINT4
Ofc131ActsPushVlanTag (tCRU_BUF_CHAIN_HEADER * pPkt, UINT2 u2VlanProto)
{
    UINT2               u2VlanTci = OFC_ZERO;
    UINT2               u2EthType = OFC_ZERO;
    UINT1               au1Buf[CFA_VLAN_TAGGED_HEADER_SIZE];

    OFC_TRC_FUNC ((OFC_FN_ENTRY, "FUNC:Ofc131ActsPushVlanTag\n"));
    MEMSET (au1Buf, OFC_ZERO, CFA_VLAN_TAGGED_HEADER_SIZE);

    /* Copy the Ethernet Destination and Source MAC Address */
    CRU_BUF_Copy_FromBufChain (pPkt, au1Buf, OFC_ZERO, OFC_VLAN_TAG_OFFSET);

    /* Copy the Ethernet Type */
    CRU_BUF_Copy_FromBufChain (pPkt, (UINT1 *) &u2EthType, OFC_TWELVE, OFC_TWO);

    u2EthType = OSIX_NTOHS (u2EthType);

    /* Check if VLAN Header is present */
    if ((u2EthType == OFC_L2EXT_CVLAN) || (u2EthType == OFC_L2EXT_SVLAN))
    {
        /* Copy the VLAN ID and Priority from the Outer most VLAN Tag */
        CRU_BUF_Copy_FromBufChain (pPkt, (UINT1 *) &u2VlanTci,
                                   (UINT4) (OFC_VLAN_TAG_OFFSET +
                                            OFC_VLAN_PROTO_SIZE),
                                   OFC_VLAN_PROTO_SIZE);
    }
    else
    {
        /*
         * PBB or First VLAN Tag to be inserted,
         * TCI Field will be updated by "Set Field" Actions.
         */
    }
    u2VlanProto = OSIX_HTONS (u2VlanProto);
    MEMCPY (&au1Buf[OFC_VLAN_TAG_OFFSET], (UINT1 *) &u2VlanProto,
            OFC_VLAN_PROTO_SIZE);
    MEMCPY (&au1Buf[OFC_VLAN_TAG_OFFSET + OFC_TWO], (UINT1 *) &u2VlanTci,
            OFC_VLAN_PROTO_SIZE);

    CRU_BUF_Move_ValidOffset (pPkt, OFC_VLAN_TAG_OFFSET);
    CRU_BUF_Prepend_BufChain (pPkt, au1Buf, CFA_VLAN_TAGGED_HEADER_SIZE);

    OFC_TRC_FUNC ((OFC_FN_EXIT, "FUNC:Ofc131ActsPushVlanTag SUCCESS\n"));
    return OFC_SUCCESS;
}

/******************************************************************************
 * Function    : Ofc131ActsPushMplsTag 
 * Description : This routine pushes a new MPLS shim header onto the packet. 
 * Input       : pPkt - Pointer to the packet CRU buffer 
 * Output      : None
 * Returns     : OFC_SUCCESS/OFC_FAILURE 
 *****************************************************************************/
PRIVATE UINT4
Ofc131ActsPushMplsTag (tCRU_BUF_CHAIN_HEADER * pPkt, UINT2 u2EthType)
{
    tEnetV2Header       EthHdr;
    UINT4               u4MplsTag = OFC_ZERO;
    UINT2               u2Offset = OFC_ZERO;
    UINT2               u2TtlOffset = OFC_ZERO;
    UINT2               u2TempEthType = OFC_ZERO;
    UINT1               u1OneByte = OFC_ZERO;
    UINT2               u2ActProto = OFC_ZERO;
    UINT1               au1Buf[OFC_MAX_HDR_LEN];

    OFC_TRC_FUNC ((OFC_FN_ENTRY, "FUNC:Ofc131ActsPushMplsTag\n"));
    MEMSET (&EthHdr, OFC_ZERO, CFA_ENET_V2_HEADER_SIZE);
    MEMSET (&au1Buf, OFC_ZERO, OFC_MAX_HDR_LEN);
    CRU_BUF_Copy_FromBufChain (pPkt, (UINT1 *) &EthHdr, OFC_ZERO,
                               CFA_ENET_V2_HEADER_SIZE);

    u2Offset = OFC_FOURTEEN;
    /* Check if MPLS Header is present */
    if ((EthHdr.u2LenOrType == OSIX_HTONS (OFC_L2EXT_MPLS_1)) ||
        (EthHdr.u2LenOrType == OSIX_HTONS (OFC_L2EXT_MPLS_2)))
    {
        /* Copy MPLS Label, traffic class and TTL */
        CRU_BUF_Copy_FromBufChain (pPkt, (UINT1 *) &u4MplsTag,
                                   CFA_ENET_V2_HEADER_SIZE, MPLS_HDR_LEN);
        u4MplsTag = OSIX_HTONL (u4MplsTag);
        u4MplsTag = u4MplsTag | (OFC_ONE << OFC_EIGHT);

    }
    else if (EthHdr.u2LenOrType == OSIX_HTONS (OFC_L2EXT_CVLAN))
    {
        /* Only One VLAN present, Check if there is an MPLS Header after it */
        CRU_BUF_Copy_FromBufChain (pPkt, (UINT1 *) &u2TempEthType,
                                   (UINT4) (CFA_ENET_V2_HEADER_SIZE + OFC_TWO),
                                   OFC_TWO);

        if ((u2TempEthType == OSIX_HTONS (OFC_L2EXT_MPLS_1)) ||
            (u2TempEthType == OSIX_HTONS (OFC_L2EXT_MPLS_2)))
        {
            /* Copy MPLS Label, traffic class and TTL */
            CRU_BUF_Copy_FromBufChain (pPkt, (UINT1 *) &u4MplsTag,
                                       (UINT4) (CFA_ENET_V2_HEADER_SIZE +
                                                OFC_FOUR), MPLS_HDR_LEN);
            u4MplsTag = OSIX_HTONL (u4MplsTag);
            u4MplsTag = u4MplsTag | (OFC_ONE << OFC_EIGHT);

        }
        /* Update the Offset for actual Copy in Packet */
        u2Offset = (UINT2) (u2Offset + OFC_FOUR);
    }
    else if (EthHdr.u2LenOrType == OSIX_HTONS (OFC_L2EXT_SVLAN))
    {
        /*
         *  Multiple VLAN Tags may be present, goto the last VAN Tag
         *  to find out if there are MPLS Headers following.
         */
        if (Ofc131ActsComputeHdrOffset (pPkt, OFC_MPLS_PROTO,
                                        &u2ActProto, &u2Offset) == OFC_SUCCESS)
        {
            /* Copy MPLS Label, traffic class and TTL */
            CRU_BUF_Copy_FromBufChain (pPkt, (UINT1 *) &u4MplsTag,
                                       (UINT4) u2Offset, (UINT4) MPLS_HDR_LEN);
            u4MplsTag = OSIX_HTONL (u4MplsTag);
            u4MplsTag = u4MplsTag | (OFC_ONE << OFC_EIGHT);
        }
        else if (Ofc131ActsComputeHdrOffset (pPkt, OFC_IP_PROTO,
                                             &u2ActProto,
                                             &u2Offset) == OFC_SUCCESS)
        {
            /* Do Nothing */
        }
        /* Offset is already updated */
    }

    /* Copy the headers till offset from packet */
    CRU_BUF_Copy_FromBufChain (pPkt, au1Buf, (UINT4) OFC_ZERO,
                               (UINT4) u2Offset);

    /* */
    if ((EthHdr.u2LenOrType == OSIX_HTONS (OFC_L3_IP4)) ||
        (u2ActProto == OFC_L3_IP4) ||
        (u2TempEthType == OSIX_HTONS (OFC_L3_IP4)))
    {
        /* Copy TTL from IPv4 */
        u2TtlOffset = OFC_EIGHT;    /* Define a MACRO */
        /* Copy TTL */
        CRU_BUF_Copy_FromBufChain (pPkt, &u1OneByte,
                                   (UINT4) (u2Offset + u2TtlOffset),
                                   (UINT4) OFC_ONE);
        u4MplsTag = u1OneByte;    /* Copy TTL */
        u2EthType = OSIX_HTONS (u2EthType);
        if (u2Offset < OFC_MAX_HDR_LEN)
        {
            MEMCPY (&au1Buf[u2Offset - OFC_TWO], (UINT1 *) &u2EthType, OFC_TWO);
        }
    }
    else if ((EthHdr.u2LenOrType == OSIX_HTONS (OFC_L3_IP6)) ||
             (u2ActProto == OFC_L3_IP6) ||
             (u2TempEthType == OSIX_HTONS (OFC_L3_IP6)))
    {
        /* Copy TTL from IPv6 */
        u2TtlOffset = OFC_SEVEN;    /* Offset of Hop Limit in IPv6 from Start */
        /* Copy TTL */
        CRU_BUF_Copy_FromBufChain (pPkt, &u1OneByte,
                                   (UINT4) (u2Offset + u2TtlOffset), OFC_ONE);
        u4MplsTag = u1OneByte;    /* Copy TTL */
        u2EthType = OSIX_HTONS (u2EthType);
        if (u2Offset < OFC_MAX_HDR_LEN)
        {
            MEMCPY (&au1Buf[u2Offset - OFC_TWO], (UINT1 *) &u2EthType, OFC_TWO);
        }
    }
    else if (EthHdr.u2LenOrType == OSIX_HTONS (OFC_L3_ARP))
    {
        u4MplsTag = OFC_ZERO;
        u2EthType = OSIX_HTONS (u2EthType);
        u2Offset = OFC_FOURTEEN;
        if (u2Offset < OFC_MAX_HDR_LEN)
        {
            MEMCPY (&au1Buf[u2Offset - OFC_TWO], (UINT1 *) &u2EthType, OFC_TWO);
        }
    }
    u4MplsTag = OSIX_HTONL (u4MplsTag);
    /* Place the MPLS Header after the offset */
    if (u2Offset < OFC_MAX_HDR_LEN)
    {
        MEMCPY (&au1Buf[u2Offset], (UINT1 *) &u4MplsTag, MPLS_HDR_LEN);
    }
    /* Move the CRU Buf to after the new MPLS Header */
    CRU_BUF_Move_ValidOffset (pPkt, (UINT4) u2Offset);
    CRU_BUF_Prepend_BufChain (pPkt, au1Buf, (UINT4) (u2Offset + MPLS_HDR_LEN));

    OFC_TRC_FUNC ((OFC_FN_EXIT, "FUNC:Ofc131ActsPushMplsTag SUCCESS\n"));
    return OFC_SUCCESS;
}

/******************************************************************************
 * Function    : Ofc131ActsPushPbbTag 
 * Description : This routine pushes a new PBB service instance header
 *               (I-TAG TCI) onto the packet
 * Input       : pPkt - Pointer to the packet CRU buffer 
 * Output      : None
 * Returns     : OFC_SUCCESS/OFC_FAILURE
 *****************************************************************************/

PRIVATE UINT4
Ofc131ActsPushPbbTag (tCRU_BUF_CHAIN_HEADER * pPkt, UINT2 u2Proto)
{
    UINT4               u4InstTag = OFC_ZERO;
    UINT2               u2EthType = OFC_ZERO;
    UINT1               au1Buf[OFC_EIGHTEEN];

    OFC_TRC_FUNC ((OFC_FN_ENTRY, "FUNC:Ofc131ActsPushPbbTag\n"));
    MEMSET (au1Buf, OFC_ZERO, OFC_ISID_TAGGED_HDR_SIZE);

    /*Check if the eth_type of the cru buf is OFC_L2EXT_SVLAN */
    CRU_BUF_Copy_FromBufChain (pPkt, (UINT1 *) &u2EthType, OFC_ETHTYPE_OFFSET,
                               OFC_ETHTYPE_SIZE);

    /* Copy the Ethernet Destination and Source MAC Address */
    CRU_BUF_Copy_FromBufChain (pPkt, au1Buf, OFC_ZERO, OFC_ETHTYPE_OFFSET);

    /* QinQ Header to be present */
    if (u2EthType == OSIX_HTONS (OFC_L2EXT_SVLAN))
    {
        /* Copy the Priority alone from VLAN Tag */
        CRU_BUF_Copy_FromBufChain (pPkt, (UINT1 *) &u4InstTag,
                                   (UINT4) (OFC_VLAN_TAG_OFFSET +
                                            OFC_VLAN_PROTO_SIZE),
                                   OFC_VLAN_PROTO_SIZE);
    }
    else
    {
        OFC_TRC_FUNC ((OFC_ACTS_TRC, "No S-VLAN Header, FAILURE\n"));
        return OFC_FAILURE;
    }
    u2Proto = OSIX_HTONS (u2Proto);
    /* Copy the EthType */
    MEMCPY (&au1Buf[OFC_ETHTYPE_OFFSET], (UINT1 *) &u2Proto, OFC_ETHTYPE_SIZE);

    /* Copy the PBB Service Instance Tag */
    MEMCPY (&au1Buf[CFA_ENET_V2_HEADER_SIZE], (UINT1 *) &u4InstTag, OFC_FOUR);

    /* Add Tag along with the B-DA and B-SA to start */
    CRU_BUF_Prepend_BufChain (pPkt, au1Buf, OFC_ISID_TAGGED_HDR_SIZE);
    OFC_TRC_FUNC ((OFC_FN_EXIT, "FUNC:Ofc131ActsPushPbbTag SUCCESS\n"));
    return OFC_SUCCESS;
}

/******************************************************************************
 * Function    : Ofc131ActsPopVlanTag 
 * Description : This routine pops the outer-most VLAN header from the packet. 
 * Input       : pPkt - Pointer to the packet CRU buffer 
 * Output      : None
 * Returns     : OFC_SUCCESS/OFC_FAILURE
 *****************************************************************************/
PRIVATE UINT4
Ofc131ActsPopVlanTag (tCRU_BUF_CHAIN_HEADER * pPkt)
{
    tEnetV2Header       EthHdr;
    UINT1               au1Buf[OFC_VLAN_TAG_OFFSET];

    OFC_TRC_FUNC ((OFC_FN_ENTRY, "FUNC:Ofc131ActsPopVlanTag\n"));
    MEMSET (&EthHdr, OFC_ZERO, CFA_ENET_V2_HEADER_SIZE);
    MEMSET (&au1Buf, OFC_ZERO, OFC_VLAN_TAG_OFFSET);
    CRU_BUF_Copy_FromBufChain (pPkt, (UINT1 *) &EthHdr, OFC_ZERO,
                               CFA_ENET_V2_HEADER_SIZE);

    /* Check if VLAN Header is present */
    if ((EthHdr.u2LenOrType == OSIX_HTONS (OFC_L2EXT_CVLAN)) ||
        (EthHdr.u2LenOrType == OSIX_HTONS (OFC_L2EXT_SVLAN)))
    {
        /* Always remove the outermost header */
        CRU_BUF_Copy_FromBufChain (pPkt, au1Buf, OFC_ZERO, OFC_VLAN_TAG_OFFSET);
        CRU_BUF_Move_ValidOffset (pPkt,
                                  (UINT4) (OFC_VLAN_TAG_OFFSET +
                                           OFC_VLAN_TAG_LEN));
        CRU_BUF_Prepend_BufChain (pPkt, au1Buf, OFC_VLAN_TAG_OFFSET);
    }
    else
    {
        /* No VLAN Header */
        OFC_TRC_FUNC ((OFC_ACTS_TRC, "No VLAN Header, FAILURE\n"));
        return OFC_FAILURE;
    }
    OFC_TRC_FUNC ((OFC_FN_EXIT, "FUNC:Ofc131ActsPopVlanTag SUCCESS\n"));
    return OFC_SUCCESS;
}

/*******************************************************************************
 * Function    : Ofc131ActsPopMplsTag 
 * Description : This routine pops the outer-most MPLS tag or shim header from 
 *               the packet.
 * Input       : pPkt - Pointer to the packet CRU buffer  
 * Output      : None
 * Returns     : OFC_SUCCESS/OFC_FAILURE
 ******************************************************************************/
PRIVATE UINT4
Ofc131ActsPopMplsTag (tCRU_BUF_CHAIN_HEADER * pPkt, UINT2 u2EthType)
{
    tEnetV2Header       EthHdr;
    UINT2               u2ActProto = OFC_ZERO;
    UINT2               u2Offset = OFC_ZERO;
    UINT1               au1Buf[OFC_MAX_HDR_LEN];

    OFC_TRC_FUNC ((OFC_FN_ENTRY, "FUNC:Ofc131ActsPopMplsTag\n"));

    MEMSET (&EthHdr, OFC_ZERO, CFA_ENET_V2_HEADER_SIZE);
    MEMSET (&au1Buf, OFC_ZERO, OFC_MAX_HDR_LEN);
    CRU_BUF_Copy_FromBufChain (pPkt, (UINT1 *) &EthHdr, OFC_ZERO,
                               CFA_ENET_V2_HEADER_SIZE);

    u2EthType = OSIX_HTONS (u2EthType);
    CRU_BUF_Copy_OverBufChain (pPkt, (UINT1 *) &u2EthType,
                               (UINT4) OFC_ETHTYPE_OFFSET, (UINT4) OFC_TWO);
    /* Check if MPLS Header is present */
    if (EthHdr.u2LenOrType == OSIX_HTONS (OFC_L2EXT_MPLS_1) ||
        EthHdr.u2LenOrType == OSIX_HTONS (OFC_L2EXT_MPLS_2))
    {
        /* Always remove the outermost header */
        CRU_BUF_Copy_FromBufChain (pPkt, au1Buf, OFC_ZERO,
                                   CFA_ENET_V2_HEADER_SIZE);
        CRU_BUF_Move_ValidOffset (pPkt,
                                  (UINT4) CFA_ENET_V2_HEADER_SIZE +
                                  MPLS_HDR_LEN);
        CRU_BUF_Prepend_BufChain (pPkt, au1Buf, CFA_ENET_V2_HEADER_SIZE);
        return OFC_SUCCESS;
    }
    else if ((EthHdr.u2LenOrType == OSIX_HTONS (OFC_L2EXT_CVLAN)) ||
             (EthHdr.u2LenOrType == OSIX_HTONS (OFC_L2EXT_SVLAN)))
    {
        if (Ofc131ActsComputeHdrOffset (pPkt, OFC_MPLS_PROTO,
                                        &u2ActProto, &u2Offset) == OFC_SUCCESS)
        {

            /* Always remove the outermost header */
            CRU_BUF_Copy_FromBufChain (pPkt, au1Buf, OFC_ZERO, u2Offset);
            CRU_BUF_Move_ValidOffset (pPkt, (UINT4) (u2Offset + MPLS_HDR_LEN));
            CRU_BUF_Prepend_BufChain (pPkt, au1Buf, (UINT4) u2Offset);

            return OFC_SUCCESS;
        }
    }
    OFC_TRC_FUNC ((OFC_FN_EXIT, "FUNC:Ofc131ActsPopMplsTag FAILURE\n"));
    return OFC_FAILURE;
}

/*******************************************************************************
 * Function    : Ofc131ActsPopPbbTag 
 * Description : This routine pops the outer-most PBB service instance header 
 *               (I-TAG TCI) from the packet along with B-DA and B-SA.
 * Input       : pPkt - Pointer to the packet CRU buffer 
 * Output      : None
 * Returns     : OFC_SUCCESS/OFC_FAILURE
 ******************************************************************************/
PRIVATE UINT4
Ofc131ActsPopPbbTag (tCRU_BUF_CHAIN_HEADER * pPkt)
{
    tEnetV2Header       EthHdr;

    OFC_TRC_FUNC ((OFC_FN_ENTRY, "FUNC:Ofc131ActsPopPbbTag\n"));
    MEMSET (&EthHdr, OFC_ZERO, CFA_ENET_V2_HEADER_SIZE);
    CRU_BUF_Copy_FromBufChain (pPkt, (UINT1 *) &EthHdr, OFC_ZERO,
                               CFA_ENET_V2_HEADER_SIZE);

    if (EthHdr.u2LenOrType == OSIX_HTONS (OFC_L2EXT_PBB))
    {
        /*
         * This action removes the B-DA and B-SA also.
         * Move to end of I-TAG instance Header.
         */
        CRU_BUF_Move_ValidOffset (pPkt, OFC_ISID_TAGGED_HDR_SIZE);
    }
    else
    {
        /* 
         * Either No PBB Header or BVLAN Tag is not removed
         */
        OFC_TRC_FUNC ((OFC_ACTS_TRC, "No PBB Header or B-VLAN Tag"
                       " not removed, FAILURE\n"));
        return OFC_FAILURE;
    }
    OFC_TRC_FUNC ((OFC_FN_EXIT, "FUNC:Ofc131ActsPopPbbTag SUCCESS\n"));
    return OFC_SUCCESS;
}

/******************************************************************************
 * Function    : Ofc131ActsApplySetFlds 
 * Description : This routine will Set the Fields as per the SetFields Action
 *               in the Action List. 
 * Input       : pPkt        - Pointer to the packet CRU Buffer
 *               pSetFldList - Pointer to the SetField SLL 
 * Output      : None
 * Returns     : OFC_SUCCESS or OFC_FAILURE
 ******************************************************************************/
PRIVATE UINT4
Ofc131ActsApplySetFlds (tCRU_BUF_CHAIN_HEADER * pPkt, tOfcSll * pSetFldList,
                        UINT1 *pu1BosFlag)
{
    tOfcSetFld         *pSetFld = NULL;
    tOxmFld            *pOxmFld = NULL;
    UINT1               u1OneByte = OFC_ZERO;
    UINT1               u1TempOneByte = OFC_ZERO;
    UINT2               u2ActType = OFC_ZERO;
    UINT2               u2TwoByte = OFC_ZERO;
    UINT2               u2TempTwoByte = OFC_ZERO;
    UINT2               u2Offset = OFC_ZERO;
    UINT2               u2TempOffset = OFC_ZERO;
    UINT4               u4FourByte = OFC_ZERO;
    UINT4               u4TempFourByte = OFC_ZERO;
    UINT4               u4RetVal = OFC_ZERO;

    OFC_TRC_FUNC ((OFC_FN_ENTRY, "FUNC:Ofc131ActsApplySetFlds\n"));
    TMO_SLL_Scan (pSetFldList, pSetFld, tOfcSetFld *)
    {
        pOxmFld = &(pSetFld->SetFld);
        switch (pSetFld->u2Type)
        {
            case OFCXMT_OFB_ETH_DST:
                CRU_BUF_Copy_OverBufChain (pPkt, pOxmFld->au1Fld,
                                           OFC_ZERO, OFC_ETH_ADDR_LEN);
                break;

            case OFCXMT_OFB_ETH_SRC:
                CRU_BUF_Copy_OverBufChain (pPkt, pOxmFld->au1Fld,
                                           OFC_ETH_SRC_OFFSET,
                                           OFC_ETH_ADDR_LEN);
                break;

            case OFCXMT_OFB_ETH_TYPE:
                CRU_BUF_Copy_OverBufChain (pPkt, pOxmFld->au1Fld,
                                           OFC_ETHTYPE_OFFSET,
                                           OFC_ETHTYPE_SIZE);
                break;

            case OFCXMT_OFB_VLAN_VID:
                u4RetVal = Ofc131ActsComputeHdrOffset (pPkt, OFC_VLAN_PROTO,
                                                       &u2ActType, &u2Offset);
                if (OFC_FAILURE == u4RetVal)
                {
                    OFC_TRC_FUNC ((OFC_ACTS_TRC, "SetField VLAN VID - "
                                   "No VLAN Header, FAILURE\n"));
                    return OFC_FAILURE;
                }
                CRU_BUF_Copy_FromBufChain (pPkt, (UINT1 *) &u2TwoByte,
                                           (UINT4) (u2Offset +
                                                    OFC_VLAN_PROTO_SIZE),
                                           (UINT4) OFC_TWO);
                u2TwoByte = OSIX_HTONS (u2TwoByte);
                u2TwoByte = u2TwoByte & OFC_VLAN_VID_UNMASK;
                MEMCPY (&u2TempTwoByte, pOxmFld->au1Fld, OFC_TWO);
                u2TempTwoByte = OSIX_HTONS (u2TempTwoByte);
                u2TwoByte = u2TwoByte | u2TempTwoByte;
                u2TwoByte = OSIX_HTONS (u2TwoByte);
                CRU_BUF_Copy_OverBufChain (pPkt, (UINT1 *) &u2TwoByte,
                                           (UINT4) (u2Offset +
                                                    (UINT2)
                                                    OFC_VLAN_PROTO_SIZE),
                                           (UINT4) OFC_TWO);
                break;

            case OFCXMT_OFB_VLAN_PCP:
                u4RetVal = Ofc131ActsComputeHdrOffset (pPkt, OFC_VLAN_PROTO,
                                                       &u2ActType, &u2Offset);
                if (OFC_FAILURE == u4RetVal)
                {
                    OFC_TRC_FUNC ((OFC_ACTS_TRC, "SetField VLAN PCP - "
                                   "No VLAN Header, FAILURE\n"));
                    return OFC_FAILURE;
                }
                CRU_BUF_Copy_FromBufChain (pPkt, &u1OneByte,
                                           (UINT4) (u2Offset +
                                                    (UINT2)
                                                    OFC_VLAN_PROTO_SIZE),
                                           OFC_ONE);

                u1OneByte = u1OneByte & OFC_VLAN_PCP_UNMASK;
                u1TempOneByte = pOxmFld->au1Fld[OFC_ZERO];
                u1OneByte = (UINT1) (u1OneByte | (u1TempOneByte << OFC_FIVE));

                CRU_BUF_Copy_OverBufChain (pPkt, &u1OneByte,
                                           (UINT4) (u2Offset +
                                                    (UINT2)
                                                    OFC_VLAN_PROTO_SIZE),
                                           OFC_ONE);
                break;

                /* IP DSCP - 6 bits in ToS Field */
            case OFCXMT_OFB_IP_DSCP:
                u4RetVal = Ofc131ActsComputeHdrOffset (pPkt, OFC_IP_PROTO,
                                                       &u2ActType, &u2Offset);
                if (OFC_FAILURE == u4RetVal)
                {
                    OFC_TRC_FUNC ((OFC_ACTS_TRC, "SetField IP DSCP - "
                                   "No IP Header, FAILURE\n"));
                    return OFC_FAILURE;
                }

                if (u2ActType == OFC_L3_IP4)
                {
                    /* Copy the existing ECN */
                    CRU_BUF_Copy_FromBufChain (pPkt, &u1OneByte,
                                               (UINT4) (u2Offset +
                                                        (UINT2)
                                                        OFC_IPV4_DSCP_OFFSET),
                                               (UINT4) OFC_ONE);

                    u1OneByte = u1OneByte & OFC_IPV4_DSCP_UNMASK;
                    u1TempOneByte = pOxmFld->au1Fld[(UINT4) OFC_ZERO];
                    u1OneByte = (UINT1) (u1OneByte |
                                         (u1TempOneByte << OFC_TWO));

                    CRU_BUF_Copy_OverBufChain (pPkt, &u1OneByte,
                                               (UINT4) (u2Offset +
                                                        (UINT2) OFC_ONE),
                                               (UINT4) OFC_ONE);

                    /* Recalculate the Checksum */
                    u1OneByte = OFC_ZERO;
                    u2TwoByte = OFC_ZERO;
                    CRU_BUF_Copy_FromBufChain (pPkt, &u1OneByte,
                                               (UINT4) u2Offset,
                                               (UINT4) OFC_ONE);
                    u1OneByte = (UINT1) ((UINT1) ((u1OneByte &
                                                   (UINT1) OFC_IPV4_IHL_UNMASK))
                                         * (UINT1) OFC_IPV4_WORD_SIZE);
                    u2TwoByte =
                        Ofc131ActsComputeIPChecksum (pPkt, u1OneByte, u2Offset);
                    CRU_BUF_Copy_OverBufChain (pPkt, (UINT1 *) &u2TwoByte,
                                               (UINT4) (u2Offset +
                                                        (UINT2)
                                                        OFC_IPV4_CHKSUM_OFFSET),
                                               (UINT4) OFC_TWO);
                }
                else
                {                /* It is IPv6 */
                    /* Copy the existing ECN */
                    CRU_BUF_Copy_FromBufChain (pPkt, (UINT1 *) &u2TwoByte,
                                               (UINT4) u2Offset,
                                               (UINT4) OFC_TWO);
                    u2TwoByte = OSIX_HTONS (u2TwoByte);
                    u2TwoByte = u2TwoByte & OFC_IPV6_DSCP_UNMASK;
                    MEMCPY (&u2TempTwoByte, pOxmFld->au1Fld, OFC_TWO);
                    u2TempTwoByte = OSIX_HTONS (u2TempTwoByte);
                    u2TwoByte = (UINT2) (u2TwoByte |
                                         (UINT2) (u2TempTwoByte << (UINT2)
                                                  OFC_SIX));
                    u2TwoByte = OSIX_HTONS (u2TwoByte);

                    CRU_BUF_Copy_OverBufChain (pPkt, (UINT1 *) &u2TwoByte,
                                               (UINT4) u2Offset,
                                               (UINT4) OFC_TWO);
                }
                break;

                /* IP ECN - 2 bits in ToS Field */
            case OFCXMT_OFB_IP_ECN:
                u4RetVal = Ofc131ActsComputeHdrOffset (pPkt, OFC_IP_PROTO,
                                                       &u2ActType, &u2Offset);
                if (OFC_FAILURE == u4RetVal)
                {
                    OFC_TRC_FUNC ((OFC_ACTS_TRC, "SetField IP ECN - "
                                   "No IP Header, FAILURE\n"));
                    return OFC_FAILURE;
                }

                if (u2ActType == OFC_L3_IP4)
                {
                    /* Copy the existing DSCP */
                    CRU_BUF_Copy_FromBufChain (pPkt, &u1OneByte,
                                               (UINT4) (u2Offset +
                                                        OFC_IPV4_DSCP_OFFSET),
                                               (UINT4) OFC_ONE);

                    u1OneByte = u1OneByte & OFC_IPV4_ECN_UNMASK;
                    u1TempOneByte = pOxmFld->au1Fld[OFC_ZERO];
                    u1OneByte = u1OneByte | u1TempOneByte;

                    CRU_BUF_Copy_OverBufChain (pPkt, &u1OneByte,
                                               (UINT4) (u2Offset + OFC_ONE),
                                               (UINT4) OFC_ONE);

                    /* Recalculate Checksum and copy into packet */
                    u1OneByte = OFC_ZERO;
                    u2TwoByte = OFC_ZERO;
                    CRU_BUF_Copy_FromBufChain (pPkt, &u1OneByte,
                                               (UINT4) u2Offset,
                                               (UINT4) OFC_ONE);
                    u1OneByte = (UINT1) ((UINT1)
                                         (u1OneByte & (UINT1)
                                          OFC_IPV4_IHL_UNMASK) *
                                         (UINT1) OFC_IPV4_WORD_SIZE);
                    u2TwoByte =
                        Ofc131ActsComputeIPChecksum (pPkt, u1OneByte, u2Offset);
                    CRU_BUF_Copy_OverBufChain (pPkt, (UINT1 *) &u2TwoByte,
                                               (UINT4) (u2Offset +
                                                        OFC_IPV4_CHKSUM_OFFSET),
                                               (UINT4) OFC_TWO);
                }
                else
                {                /* It is IPv6 */
                    /* Copy the existing DSCP */
                    CRU_BUF_Copy_FromBufChain (pPkt, (UINT1 *) &u2TwoByte,
                                               (UINT4) u2Offset,
                                               (UINT4) OFC_TWO);

                    u2TwoByte = OSIX_HTONS (u2TwoByte);
                    u2TwoByte = u2TwoByte & OFC_IPV6_ECN_UNMASK;
                    MEMCPY (&u2TempTwoByte, pOxmFld->au1Fld, OFC_TWO);
                    u2TempTwoByte = OSIX_HTONS (u2TempTwoByte);
                    u2TwoByte = (UINT2) (u2TwoByte |
                                         (UINT2) (u2TempTwoByte << (UINT2)
                                                  OFC_FOUR));
                    u2TwoByte = OSIX_HTONS (u2TwoByte);

                    CRU_BUF_Copy_OverBufChain (pPkt, (UINT1 *) &u2TwoByte,
                                               (UINT4) u2Offset,
                                               (UINT4) OFC_TWO);
                }
                break;

            case OFCXMT_OFB_IP_PROTO:
                u4RetVal = Ofc131ActsComputeHdrOffset (pPkt, OFC_IP_PROTO,
                                                       &u2ActType, &u2Offset);
                if (OFC_FAILURE == u4RetVal)
                {
                    OFC_TRC_FUNC ((OFC_ACTS_TRC, "SetField IP Protocol - "
                                   "No IP Header, FAILURE\n"));
                    return OFC_FAILURE;
                }

                if (u2ActType == OFC_L3_IP4)
                {
                    CRU_BUF_Copy_OverBufChain (pPkt, pOxmFld->au1Fld,
                                               (UINT4) (u2Offset + OFC_NINE),
                                               (UINT4) OFC_ONE);

                    /* Recalculate checksum and copy it into packet */
                    u1OneByte = OFC_ZERO;
                    u2TwoByte = OFC_ZERO;
                    CRU_BUF_Copy_FromBufChain (pPkt, &u1OneByte,
                                               (UINT4) u2Offset,
                                               (UINT4) OFC_ONE);
                    u1OneByte = (UINT1) ((UINT1)
                                         (u1OneByte & (UINT1)
                                          OFC_IPV4_IHL_UNMASK) *
                                         (UINT1) OFC_FOUR);
                    u2TwoByte =
                        Ofc131ActsComputeIPChecksum (pPkt, u1OneByte, u2Offset);
                    CRU_BUF_Copy_OverBufChain (pPkt, (UINT1 *) &u2TwoByte,
                                               (UINT4) (u2Offset + OFC_TEN),
                                               OFC_TWO);
                }
                else
                {                /* It is IPv6 */
                    CRU_BUF_Copy_OverBufChain (pPkt, pOxmFld->au1Fld,
                                               (UINT4) (u2Offset + OFC_SIX),
                                               OFC_ONE);
                }
                break;

            case OFCXMT_OFB_IPV4_SRC:
                u4RetVal = Ofc131ActsComputeHdrOffset (pPkt, OFC_IP_PROTO,
                                                       &u2ActType, &u2Offset);
                if (OFC_FAILURE == u4RetVal)
                {
                    OFC_TRC_FUNC ((OFC_ACTS_TRC, "SetField IPv4 Source - "
                                   "No IP Header, FAILURE\n"));
                    return OFC_FAILURE;
                }

                if (u2ActType == OFC_L3_IP4)
                {
                    CRU_BUF_Copy_OverBufChain (pPkt, pOxmFld->au1Fld,
                                               (UINT4) (u2Offset +
                                                        OFC_IPV4_SRC_OFFSET),
                                               OFC_IPV4_ADDR_LEN);

                    /* Recalculate checksum and copy it into packet */
                    u1OneByte = OFC_ZERO;
                    u2TwoByte = OFC_ZERO;
                    CRU_BUF_Copy_FromBufChain (pPkt, &u1OneByte,
                                               (UINT4) u2Offset,
                                               (UINT4) OFC_ONE);
                    u1OneByte = (UINT1) ((UINT1) (u1OneByte &
                                                  (UINT1) OFC_IPV4_IHL_UNMASK) *
                                         (UINT1) OFC_IPV4_WORD_SIZE);
                    u2TwoByte = Ofc131ActsComputeIPChecksum (pPkt, u1OneByte,
                                                             u2Offset);
                    CRU_BUF_Copy_OverBufChain (pPkt, (UINT1 *) &u2TwoByte,
                                               (UINT4) (u2Offset +
                                                        OFC_IPV4_CHKSUM_OFFSET),
                                               (UINT4) OFC_TWO);
                }
                else
                {                /* It is IPv6 */
                    OFC_TRC_FUNC ((OFC_ACTS_TRC, "SetField IPv4 Source - "
                                   "No IPv4 Header, FAILURE\n"));
                    return OFC_FAILURE;
                }
                break;

            case OFCXMT_OFB_IPV4_DST:
                u4RetVal = Ofc131ActsComputeHdrOffset (pPkt, OFC_IP_PROTO,
                                                       &u2ActType, &u2Offset);
                if (OFC_FAILURE == u4RetVal)
                {
                    OFC_TRC_FUNC ((OFC_ACTS_TRC, "SetField IPv4 Destination - "
                                   "No IP Header, FAILURE\n"));
                    return OFC_FAILURE;
                }

                if (u2ActType == OFC_L3_IP4)
                {
                    CRU_BUF_Copy_OverBufChain (pPkt, pOxmFld->au1Fld,
                                               (UINT4) (u2Offset +
                                                        OFC_IPV4_DST_OFFSET),
                                               (UINT4) OFC_IPV4_ADDR_LEN);

                    /* Recalculate Checksum and copy it into packet */
                    u1OneByte = OFC_ZERO;
                    u2TwoByte = OFC_ZERO;
                    CRU_BUF_Copy_FromBufChain (pPkt, &u1OneByte,
                                               (UINT4) u2Offset, OFC_ONE);
                    u1OneByte = (UINT1) ((UINT1) (u1OneByte &
                                                  (UINT1) OFC_IPV4_IHL_UNMASK) *
                                         (UINT1) OFC_IPV4_WORD_SIZE);
                    u2TwoByte = Ofc131ActsComputeIPChecksum (pPkt, u1OneByte,
                                                             u2Offset);
                    CRU_BUF_Copy_OverBufChain (pPkt, (UINT1 *) &u2TwoByte,
                                               (UINT4) (u2Offset +
                                                        OFC_IPV4_CHKSUM_OFFSET),
                                               OFC_TWO);
                }
                else
                {
                    /* It is IPv6 */
                    OFC_TRC_FUNC ((OFC_ACTS_TRC, "SetField IPv4 Destination - "
                                   "No IPv4 Header, FAILURE\n"));
                    return OFC_FAILURE;
                }
                break;

            case OFCXMT_OFB_TCP_SRC:
                u4RetVal = Ofc131ActsComputeHdrOffset (pPkt, OFC_L4PROTO_TCP,
                                                       &u2ActType, &u2Offset);
                if (OFC_FAILURE == u4RetVal)
                {
                    OFC_TRC_FUNC ((OFC_ACTS_TRC, "SetField TCP Source - "
                                   "No TCP Header, FAILURE\n"));
                    return OFC_FAILURE;
                }
                CRU_BUF_Copy_OverBufChain (pPkt, pOxmFld->au1Fld,
                                           (UINT4) u2Offset, OFC_L4_ADDR_LEN);
                break;

            case OFCXMT_OFB_TCP_DST:
                u4RetVal = Ofc131ActsComputeHdrOffset (pPkt, OFC_L4PROTO_TCP,
                                                       &u2ActType, &u2Offset);
                if (OFC_FAILURE == u4RetVal)
                {
                    OFC_TRC_FUNC ((OFC_ACTS_TRC, "SetField TCP Destination - "
                                   "No TCP Header, FAILURE\n"));
                    return OFC_FAILURE;
                }
                CRU_BUF_Copy_OverBufChain (pPkt, pOxmFld->au1Fld,
                                           (UINT4) (u2Offset +
                                                    OFC_L4_DST_OFFSET),
                                           OFC_L4_ADDR_LEN);
                break;

            case OFCXMT_OFB_UDP_SRC:
                u4RetVal = Ofc131ActsComputeHdrOffset (pPkt, OFC_L4PROTO_UDP,
                                                       &u2ActType, &u2Offset);
                if (OFC_FAILURE == u4RetVal)
                {
                    OFC_TRC_FUNC ((OFC_ACTS_TRC, "SetField UDP Source - "
                                   "No UDP Header, FAILURE\n"));
                    return OFC_FAILURE;
                }
                CRU_BUF_Copy_OverBufChain (pPkt, pOxmFld->au1Fld,
                                           (UINT4) u2Offset, OFC_L4_ADDR_LEN);
                break;

            case OFCXMT_OFB_UDP_DST:
                u4RetVal = Ofc131ActsComputeHdrOffset (pPkt, OFC_L4PROTO_UDP,
                                                       &u2ActType, &u2Offset);
                if (OFC_FAILURE == u4RetVal)
                {
                    OFC_TRC_FUNC ((OFC_ACTS_TRC, "SetField UDP Destination - "
                                   "No UDP Header, FAILURE\n"));
                    return OFC_FAILURE;
                }
                CRU_BUF_Copy_OverBufChain (pPkt, pOxmFld->au1Fld,
                                           (UINT4) (u2Offset +
                                                    OFC_L4_DST_OFFSET),
                                           OFC_L4_ADDR_LEN);
                break;

            case OFCXMT_OFB_SCTP_SRC:
                u4RetVal = Ofc131ActsComputeHdrOffset (pPkt, OFC_L4PROTO_SCTP,
                                                       &u2ActType, &u2Offset);
                if (OFC_FAILURE == u4RetVal)
                {
                    OFC_TRC_FUNC ((OFC_ACTS_TRC, "SetField SCTP Source - "
                                   "No SCTP Header, FAILURE\n"));
                    return OFC_FAILURE;
                }
                CRU_BUF_Copy_OverBufChain (pPkt, pOxmFld->au1Fld,
                                           (UINT4) u2Offset, OFC_L4_ADDR_LEN);
                break;

            case OFCXMT_OFB_SCTP_DST:
                u4RetVal = Ofc131ActsComputeHdrOffset (pPkt, OFC_L4PROTO_SCTP,
                                                       &u2ActType, &u2Offset);
                if (OFC_FAILURE == u4RetVal)
                {
                    OFC_TRC_FUNC ((OFC_ACTS_TRC, "SetField SCTP Destination - "
                                   "No SCTP Header, FAILURE\n"));
                    return OFC_FAILURE;
                }
                CRU_BUF_Copy_OverBufChain (pPkt, pOxmFld->au1Fld,
                                           (UINT4) (u2Offset +
                                                    OFC_L4_DST_OFFSET),
                                           OFC_L4_ADDR_LEN);
                break;

            case OFCXMT_OFB_ICMPV4_TYPE:
                u4RetVal = Ofc131ActsComputeHdrOffset (pPkt, OFC_L4PROTO_ICMP,
                                                       &u2ActType, &u2Offset);
                if (OFC_FAILURE == u4RetVal)
                {
                    OFC_TRC_FUNC ((OFC_ACTS_TRC, "SetField ICMPv4 Type - "
                                   "No ICMPv4 Header, FAILURE\n"));
                    return OFC_FAILURE;
                }
                CRU_BUF_Copy_OverBufChain (pPkt, pOxmFld->au1Fld,
                                           (UINT4) u2Offset, OFC_ONE);
                break;

            case OFCXMT_OFB_ICMPV4_CODE:
                u4RetVal = Ofc131ActsComputeHdrOffset (pPkt, OFC_L4PROTO_ICMP,
                                                       &u2ActType, &u2Offset);
                if (OFC_FAILURE == u4RetVal)
                {
                    OFC_TRC_FUNC ((OFC_ACTS_TRC, "SetField ICMPv4 Code - "
                                   "No ICMPv4 Header, FAILURE\n"));
                    return OFC_FAILURE;
                }
                CRU_BUF_Copy_OverBufChain (pPkt, pOxmFld->au1Fld,
                                           (UINT4) (u2Offset +
                                                    OFC_ICMP4_CODE_OFFSET),
                                           OFC_ONE);
                break;

            case OFCXMT_OFB_ARP_OP:
                u4RetVal = Ofc131ActsComputeHdrOffset (pPkt, OFC_L3_ARP,
                                                       &u2ActType, &u2Offset);
                if (OFC_FAILURE == u4RetVal)
                {
                    OFC_TRC_FUNC ((OFC_ACTS_TRC, "SetField ARP OpCode - "
                                   "No ARP Header, FAILURE\n"));
                    return OFC_FAILURE;
                }
                CRU_BUF_Copy_OverBufChain (pPkt, pOxmFld->au1Fld,
                                           (UINT4) ((UINT4) u2Offset +
                                                    (UINT4) OFC_ARP_OP_OFFSET),
                                           OFC_ARP_OP_LEN);
                break;

            case OFCXMT_OFB_ARP_SPA:
                u4RetVal = Ofc131ActsComputeHdrOffset (pPkt, OFC_L3_ARP,
                                                       &u2ActType, &u2Offset);
                if (OFC_FAILURE == u4RetVal)
                {
                    OFC_TRC_FUNC ((OFC_ACTS_TRC, "SetField ARP Source IPv4 "
                                   "Address - No ARP Header, FAILURE\n"));
                    return OFC_FAILURE;
                }
                CRU_BUF_Copy_OverBufChain (pPkt, pOxmFld->au1Fld,
                                           (UINT4) ((UINT4) u2Offset +
                                                    (UINT4) OFC_ARP_SPA_OFFSET),
                                           OFC_ARP_PA_LEN);
                break;

            case OFCXMT_OFB_ARP_TPA:
                u4RetVal = Ofc131ActsComputeHdrOffset (pPkt, OFC_L3_ARP,
                                                       &u2ActType, &u2Offset);
                if (OFC_FAILURE == u4RetVal)
                {
                    OFC_TRC_FUNC ((OFC_ACTS_TRC, "SetField ARP Target IPv4 "
                                   "Address - No ARP Header, FAILURE\n"));
                    return OFC_FAILURE;
                }
                CRU_BUF_Copy_OverBufChain (pPkt, pOxmFld->au1Fld,
                                           (UINT4) ((UINT4) u2Offset +
                                                    (UINT4) OFC_ARP_TPA_OFFSET),
                                           OFC_ARP_PA_LEN);
                break;

            case OFCXMT_OFB_ARP_SHA:
                u4RetVal = Ofc131ActsComputeHdrOffset (pPkt, OFC_L3_ARP,
                                                       &u2ActType, &u2Offset);
                if (OFC_FAILURE == u4RetVal)
                {
                    OFC_TRC_FUNC ((OFC_ACTS_TRC, "SetField ARP Source Hardware "
                                   "Address - No ARP Header, FAILURE\n"));
                    return OFC_FAILURE;
                }
                CRU_BUF_Copy_OverBufChain (pPkt, pOxmFld->au1Fld,
                                           (UINT4) ((UINT4) u2Offset +
                                                    (UINT4) OFC_ARP_SHA_OFFSET),
                                           OFC_ARP_HA_LEN);
                break;

            case OFCXMT_OFB_ARP_THA:
                u4RetVal = Ofc131ActsComputeHdrOffset (pPkt, OFC_L3_ARP,
                                                       &u2ActType, &u2Offset);
                if (OFC_FAILURE == u4RetVal)
                {
                    OFC_TRC_FUNC ((OFC_ACTS_TRC, "SetField ARP Target Hardware "
                                   "Address - No ARP Header, FAILURE\n"));
                    return OFC_FAILURE;
                }
                CRU_BUF_Copy_OverBufChain (pPkt, pOxmFld->au1Fld,
                                           (UINT4) ((UINT4) u2Offset +
                                                    (UINT4) OFC_ARP_THA_OFFSET),
                                           OFC_ARP_HA_LEN);
                break;

            case OFCXMT_OFB_IPV6_SRC:
                u4RetVal = Ofc131ActsComputeHdrOffset (pPkt, OFC_IP_PROTO,
                                                       &u2ActType, &u2Offset);
                if (OFC_FAILURE == u4RetVal)
                {
                    OFC_TRC_FUNC ((OFC_ACTS_TRC, "SetField IPv6 Source "
                                   "Address - No IP Header, FAILURE\n"));
                    return OFC_FAILURE;
                }

                if (u2ActType == OFC_L3_IP6)
                {
                    CRU_BUF_Copy_OverBufChain (pPkt, pOxmFld->au1Fld,
                                               (UINT4) ((UINT4) u2Offset +
                                                        (UINT4)
                                                        OFC_IPV6_SRC_OFFSET),
                                               OFC_IPV6_ADDR_LEN);
                }
                else
                {
                    /* It is IPv4 */
                    OFC_TRC_FUNC ((OFC_ACTS_TRC, "SetField IPv6 Source "
                                   "Address - No IPv6 Header, FAILURE\n"));
                    return OFC_FAILURE;
                }
                break;

            case OFCXMT_OFB_IPV6_DST:
                u4RetVal = Ofc131ActsComputeHdrOffset (pPkt, OFC_IP_PROTO,
                                                       &u2ActType, &u2Offset);
                if (OFC_FAILURE == u4RetVal)
                {
                    OFC_TRC_FUNC ((OFC_ACTS_TRC, "SetField IPv6 Destination "
                                   "Address - No IP Header, FAILURE\n"));
                    return OFC_FAILURE;
                }

                if (u2ActType == OFC_L3_IP6)
                {
                    CRU_BUF_Copy_OverBufChain (pPkt, pOxmFld->au1Fld,
                                               (UINT4) ((UINT4) u2Offset +
                                                        (UINT4)
                                                        OFC_IPV6_DST_OFFSET),
                                               OFC_IPV6_ADDR_LEN);
                }
                else
                {
                    /* It is IPv4 */
                    OFC_TRC_FUNC ((OFC_ACTS_TRC, "SetField IPv6 Destination "
                                   "Address - No IPv6 Header, FAILURE\n"));
                    return OFC_FAILURE;
                }
                break;

            case OFCXMT_OFB_IPV6_FLABEL:
                u4RetVal = Ofc131ActsComputeHdrOffset (pPkt, OFC_IP_PROTO,
                                                       &u2ActType, &u2Offset);
                if (OFC_FAILURE == u4RetVal)
                {
                    OFC_TRC_FUNC ((OFC_ACTS_TRC, "SetField IPv6 Flow Label "
                                   "- No IP Header, FAILURE\n"));
                    return OFC_FAILURE;
                }

                if (u2ActType == OFC_L3_IP6)
                {
                    /* Copy the existing Flow Label */
                    CRU_BUF_Copy_FromBufChain (pPkt, (UINT1 *) &u4FourByte,
                                               u2Offset, OFC_FOUR);
                    u4FourByte = OSIX_HTONL (u4FourByte);
                    u4FourByte = u4FourByte & OFC_IPV6_FLABEL_UNMASK;
                    MEMCPY (&u4TempFourByte, pOxmFld->au1Fld, OFC_FOUR);
                    u4TempFourByte = OSIX_HTONL (u4TempFourByte);
                    u4FourByte = u4FourByte | u4TempFourByte;

                    u4FourByte = OSIX_HTONL (u4FourByte);
                    CRU_BUF_Copy_OverBufChain (pPkt, (UINT1 *) &u4FourByte,
                                               u2Offset, OFC_FOUR);
                }
                else
                {
                    /* It is IPv4 */
                    OFC_TRC_FUNC ((OFC_ACTS_TRC, "SetField IPv6 Flow Label "
                                   "- No IPv6 Header, FAILURE\n"));
                    return OFC_FAILURE;
                }
                break;

            case OFCXMT_OFB_ICMPV6_TYPE:
                u4RetVal = Ofc131ActsComputeHdrOffset (pPkt, OFC_L4PROTO_ICMP6,
                                                       NULL, &u2Offset);
                if (OFC_FAILURE == u4RetVal)
                {
                    OFC_TRC_FUNC ((OFC_ACTS_TRC, "SetField ICMPv6 Type - "
                                   "No ICMPv6 Header, FAILURE\n"));
                    return OFC_FAILURE;
                }

                CRU_BUF_Copy_OverBufChain (pPkt, pOxmFld->au1Fld,
                                           u2Offset, OFC_ONE);
                break;

            case OFCXMT_OFB_ICMPV6_CODE:
                u4RetVal = Ofc131ActsComputeHdrOffset (pPkt, OFC_L4PROTO_ICMP6,
                                                       NULL, &u2Offset);
                if (OFC_FAILURE == u4RetVal)
                {
                    OFC_TRC_FUNC ((OFC_ACTS_TRC, "SetField ICMPv6 Code - "
                                   "No ICMPv6 Header, FAILURE\n"));
                    return OFC_FAILURE;
                }

                CRU_BUF_Copy_OverBufChain (pPkt, pOxmFld->au1Fld,
                                           (UINT4) ((UINT4) u2Offset +
                                                    (UINT4)
                                                    OFC_ICMP6_CODE_OFFSET),
                                           OFC_ONE);
                break;

            case OFCXMT_OFB_IPV6_ND_SLL:
                u4RetVal = Ofc131ActsComputeHdrOffset (pPkt, OFC_L3_ND,
                                                       NULL, &u2Offset);
                if (OFC_FAILURE == u4RetVal)
                {
                    OFC_TRC_FUNC ((OFC_ACTS_TRC, "SetField ND SLL "
                                   "- No ND Header, FAILURE\n"));
                    return OFC_FAILURE;
                }
                /* Fetch the ND Type */
                CRU_BUF_Copy_FromBufChain (pPkt, &u1OneByte, u2Offset, OFC_ONE);
                /* Fetch The ND Option Offset */
                switch (u1OneByte)
                {
                    case ROUTER_SOLICITATION:
                        u2TempOffset = OFC_ND_RS_OPTION_OFFSET;
                        break;

                    case ROUTER_ADVERTISEMENT:
                        u2TempOffset = OFC_ND_RA_OPTION_OFFSET;
                        break;

                    case NEIGHBOR_SOLICITATION:
                        u2TempOffset = OFC_ND_NS_OPTION_OFFSET;
                        break;

                    default:
                        /* Invalid ND Type */
                        OFC_TRC_FUNC ((OFC_ACTS_TRC, "SetField ND SLL - "
                                       "Invalid ND Type , FAILURE\n"));
                        return OFC_FAILURE;
                }
                u2Offset = (UINT2) (u2Offset + u2TempOffset);

                /* Copy the ND Option */
                CRU_BUF_Copy_FromBufChain (pPkt, &u1OneByte, u2Offset, OFC_ONE);
                /* Copy the ND Option Length, in case any */
                CRU_BUF_Copy_FromBufChain (pPkt, &u1TempOneByte,
                                           (UINT4) ((UINT4) u2Offset + OFC_ONE),
                                           OFC_ONE);

                if (ND_SLL == u1OneByte)
                {
                    CRU_BUF_Copy_OverBufChain (pPkt, pOxmFld->au1Fld,
                                               (UINT4) ((UINT4) u2Offset +
                                                        (UINT4) OFC_TWO),
                                               (UINT4) ((UINT4) u1TempOneByte -
                                                        (UINT4) OFC_TWO));
                }
                else
                {
                    /* No ND SLL present */
                    OFC_TRC_FUNC ((OFC_ACTS_TRC, "SetField ND SLL - "
                                   "No ND SLL present, FAILURE\n"));
                    return OFC_FAILURE;
                }
                break;

            case OFCXMT_OFB_IPV6_ND_TLL:
                u4RetVal = Ofc131ActsComputeHdrOffset (pPkt, OFC_L3_ND,
                                                       NULL, &u2Offset);
                if (OFC_FAILURE == u4RetVal)
                {
                    OFC_TRC_FUNC ((OFC_ACTS_TRC, "SetField ND TLL - "
                                   "No ND Header, FAILURE\n"));
                    return OFC_FAILURE;
                }
                /* Fetch the ND Type */
                CRU_BUF_Copy_FromBufChain (pPkt, &u1OneByte, u2Offset, OFC_ONE);
                /* Fetch The ND Option Offset */
                switch (u1OneByte)
                {
                    case NEIGHBOR_ADVERTISEMENT:
                        u2TempOffset = OFC_ND_NA_OPTION_OFFSET;
                        break;

                    case REDIRECT:
                        u2TempOffset = OFC_ND_RD_OPTION_OFFSET;
                        break;

                    default:
                        /* Invalid ND Type */
                        OFC_TRC_FUNC ((OFC_ACTS_TRC, "SetField ND TLL - "
                                       "Invalid ND Type, FAILURE\n"));
                        return OFC_FAILURE;
                }
                u2Offset = (UINT2) (u2Offset + u2TempOffset);

                /* Copy the ND Option */
                CRU_BUF_Copy_FromBufChain (pPkt, &u1OneByte, u2Offset, OFC_ONE);

                /* Copy the ND Option Length, in case any */
                CRU_BUF_Copy_FromBufChain (pPkt, &u1TempOneByte,
                                           (UINT4) ((UINT4) u2Offset +
                                                    (UINT4) OFC_ONE), OFC_ONE);

                if (ND_TLL == u1OneByte)
                {
                    CRU_BUF_Copy_OverBufChain (pPkt, pOxmFld->au1Fld,
                                               (UINT4) ((UINT4) u2Offset +
                                                        OFC_TWO),
                                               (UINT4) ((UINT4) u1TempOneByte -
                                                        (UINT4) OFC_TWO));
                }
                else
                {
                    /* No ND TLL present */
                    OFC_TRC_FUNC ((OFC_ACTS_TRC, "SetField ND TLL - "
                                   "No ND TLL present, FAILURE\n"));
                    return OFC_FAILURE;
                }
                break;

            case OFCXMT_OFB_IPV6_ND_TARGET:
                u4RetVal = Ofc131ActsComputeHdrOffset (pPkt, OFC_L3_ND,
                                                       NULL, &u2Offset);
                if (OFC_FAILURE == u4RetVal)
                {
                    OFC_TRC_FUNC ((OFC_ACTS_TRC, "SetField ND Target - "
                                   "No ND Header, FAILURE\n"));
                    return OFC_FAILURE;
                }
                /* Fetch the ND Type */
                CRU_BUF_Copy_FromBufChain (pPkt, &u1OneByte, u2Offset, OFC_ONE);
                /* Fetch The ND Target Offset */
                switch (u1OneByte)
                {
                    case NEIGHBOR_SOLICITATION:
                        break;

                    case NEIGHBOR_ADVERTISEMENT:
                        break;

                    case REDIRECT:
                        break;

                    default:
                        /* Invalid Type */
                        OFC_TRC_FUNC ((OFC_ACTS_TRC, "SetField ND Target - "
                                       "Invalid Type, FAILURE\n"));
                        return OFC_FAILURE;
                }
                u2Offset = (UINT2) (u2Offset + OFC_ND_TARGET_OFFSET);
                /* Copy the Target IPv6 Address */
                CRU_BUF_Copy_OverBufChain (pPkt, pOxmFld->au1Fld,
                                           u2Offset, OFC_SIXTEEN);
                break;

            case OFCXMT_OFB_MPLS_LABEL:
                u4RetVal = Ofc131ActsComputeHdrOffset (pPkt, OFC_MPLS_PROTO,
                                                       &u2ActType, &u2Offset);
                if (OFC_FAILURE == u4RetVal)
                {
                    OFC_TRC_FUNC ((OFC_ACTS_TRC, "SetField MPLS Label - "
                                   "No MPLS Header, FAILURE\n"));
                    return OFC_FAILURE;
                }
                /* Copy the existing  TTL */
                CRU_BUF_Copy_FromBufChain (pPkt, (UINT1 *) &u4FourByte,
                                           u2Offset, OFC_FOUR);

                u4FourByte = OSIX_HTONL (u4FourByte);
                u4FourByte = u4FourByte & OFC_MPLS_LABEL_UNMASK;
                MEMCPY (&u4TempFourByte, pOxmFld->au1Fld, OFC_FOUR);
                u4TempFourByte = OSIX_HTONL (u4TempFourByte);
                /* Set the BOS field of the first MPLS header and 
                 * remaining MPLS labels with BOS field as zero */

                if (*pu1BosFlag == OFC_ZERO)
                {
                    u4TempFourByte =
                        (UINT4) ((u4TempFourByte << OFC_FOUR) | OFC_ONE);
                    *pu1BosFlag = OFC_ONE;
                }
                else
                {
                    u4TempFourByte =
                        (UINT4) ((u4TempFourByte << OFC_FOUR) | OFC_ZERO);
                }
                u4FourByte =
                    (UINT4) ((u4TempFourByte << OFC_EIGHT) | u4FourByte);

                u4FourByte = OSIX_HTONL (u4FourByte);
                CRU_BUF_Copy_OverBufChain (pPkt, (UINT1 *) &u4FourByte,
                                           u2Offset, OFC_FOUR);
                break;

            case OFCXMT_OFB_MPLS_TC:
                u4RetVal = Ofc131ActsComputeHdrOffset (pPkt, OFC_MPLS_PROTO,
                                                       &u2ActType, &u2Offset);
                if (OFC_FAILURE == u4RetVal)
                {
                    OFC_TRC_FUNC ((OFC_ACTS_TRC, "SetField MPLS TC - "
                                   "No MPLS Header, FAILURE\n"));
                    return OFC_FAILURE;
                }

                /* Copy the existing BoS */
                CRU_BUF_Copy_FromBufChain (pPkt, &u1OneByte,
                                           (UINT4) ((UINT4) u2Offset +
                                                    (UINT4)
                                                    OFC_MPLS_LABEL16_TC_BOS_OFFSET),
                                           OFC_ONE);

                u1OneByte = u1OneByte & OFC_MPLS_TC_UNMASK;
                u1TempOneByte = pOxmFld->au1Fld[OFC_ZERO];
                u1OneByte = (UINT1) (u1OneByte |
                                     (UINT1) (u1TempOneByte << (UINT1)
                                              OFC_ONE));

                CRU_BUF_Copy_OverBufChain (pPkt, &u1OneByte,
                                           (UINT4) ((UINT4) u2Offset +
                                                    (UINT4)
                                                    OFC_MPLS_LABEL16_TC_BOS_OFFSET),
                                           OFC_ONE);

                break;

            case OFCXMT_OFB_MPLS_BOS:
                u4RetVal = Ofc131ActsComputeHdrOffset (pPkt, OFC_MPLS_PROTO,
                                                       &u2ActType, &u2Offset);
                if (OFC_FAILURE == u4RetVal)
                {
                    OFC_TRC_FUNC ((OFC_ACTS_TRC, "SetField MPLS BoS - "
                                   "No MPLS Header, FAILURE\n"));
                    return OFC_FAILURE;
                }
                /* Copy the existing CoS */
                CRU_BUF_Copy_FromBufChain (pPkt, &u1OneByte,
                                           (UINT4) ((UINT4) u2Offset +
                                                    (UINT4)
                                                    OFC_MPLS_LABEL16_TC_BOS_OFFSET),
                                           OFC_ONE);
                u1OneByte = u1OneByte & OFC_MPLS_BOS_UNMASK;
                u1TempOneByte = pOxmFld->au1Fld[OFC_ZERO];
                u1OneByte = u1OneByte | u1TempOneByte;

                CRU_BUF_Copy_OverBufChain (pPkt, &u1OneByte,
                                           (UINT4) ((UINT4) u2Offset +
                                                    (UINT4)
                                                    OFC_MPLS_LABEL16_TC_BOS_OFFSET),
                                           OFC_ONE);
                break;

            case OFCXMT_OFB_PBB_ISID:
                u4RetVal = Ofc131ActsComputeHdrOffset (pPkt, OFC_L2EXT_PBB,
                                                       NULL, &u2Offset);
                if (OFC_FAILURE == u4RetVal)
                {
                    OFC_TRC_FUNC ((OFC_ACTS_TRC, "SetField PBB ISID - "
                                   "No PBB Header, FAILURE\n"));
                    return OFC_FAILURE;
                }

                /* Copy the existing Flow Lable, CoS and TTL */
                CRU_BUF_Copy_FromBufChain (pPkt, (UINT1 *) &u4FourByte,
                                           (UINT4) ((UINT4) u2Offset +
                                                    (UINT4)
                                                    OFC_PBB_ISID_OFFSET),
                                           OFC_PBB_ISID_SIZE);

                u4FourByte = OSIX_HTONL (u4FourByte);
                u4FourByte = u4FourByte & OFC_PBB_ISID_UNMASK;
                MEMCPY (&u4TempFourByte, pOxmFld->au1Fld, OFC_FOUR);
                u4TempFourByte = OSIX_HTONL (u4TempFourByte);
                u4FourByte = u4FourByte | u4TempFourByte;
                u4FourByte = OSIX_HTONL (u4FourByte);

                CRU_BUF_Copy_OverBufChain (pPkt, (UINT1 *) &u4FourByte,
                                           (UINT4) ((UINT4) u2Offset +
                                                    (UINT4)
                                                    OFC_PBB_ISID_OFFSET),
                                           OFC_PBB_ISID_SIZE);
                break;

            case OFCXMT_OFB_TUNNEL_ID:
                break;

            case OFCXMT_OFB_IPV6_EXTHDR:
                break;

            default:
                /* Unknown Set Field */
                return OFC_FAILURE;
        }
    }
    OFC_TRC_FUNC ((OFC_FN_EXIT, "FUNC:Ofc131ActsApplySetFlds SUCCESS\n"));
    return OFC_SUCCESS;
}

/******************************************************************************
 * Function    : Ofc131ActsApplyActionList 
 * Description : This routine applies actions part of the Apply Actions
 *               instructions onto the packet. 
 * Input       : pPkt       - Pointer to Packet CRU Buffer
 *               pActList   - Pointer to Actions List
 * Output      : pi4GroupId - Pointer to Group Entry, in case any
 *               pOutput    - Pointer to Output SLL, in case any 
 * Returns     : OFC_SUCCESS or OFC_FAILURE
 ******************************************************************************/
PUBLIC UINT4
Ofc131ActsApplyActionList (tCRU_BUF_CHAIN_HEADER * pPkt, tOfcSll * pActList,
                           INT4 *pi4GroupId, tOfcSll * pOutput)
{
    tOfcActs           *pOfcActs = NULL;
    tOfcActGroup       *pActGroup = NULL;
    tOfcActSetNwTtl    *pActSetNwTtl = NULL;
    tOfcActSetMplsTtl  *pActSetMplsTtl = NULL;
    tOfcActSetFld      *pActSetFld = NULL;
    tOfcActPush        *pActPush = NULL;
    tOfcActPopMpls     *pActPopMpls = NULL;
    tOfcActOutputPort  *pActOutPort = NULL;
    UINT4               u4RetVal = OFC_ZERO;
    UINT2               u2ActType = OFC_ZERO;
    UINT2               u2Offset = OFC_ZERO;
    UINT1               u1OneByte = OFC_ZERO;
    UINT2               u2TwoByte = OFC_ZERO;
    UINT1               u1BosFlag = OFC_ZERO;

    OFC_TRC_FUNC ((OFC_FN_ENTRY, "FUNC:Ofc131ActsApplyActionList \n"));
    TMO_SLL_Scan (pActList, pOfcActs, tOfcActs *)
    {
        switch (pOfcActs->u2Type)
        {
            case OFCAT_OUTPUT:
                /* Add the Output port in the SLL. SLL is added to include 
                 * multiple output ports in the action */
                pActOutPort = (tOfcActOutputPort *) MemAllocMemBlk
                    (OFC_OUT_PORT_POOLID);
                if (pActOutPort == NULL)
                {
                    OFC131_TRC_FUNC ((OFC_PKT_TRC,
                                      "MemAllocMemBlk for OutPort Failed\n"));
                    return OSIX_FAILURE;
                }
                MEMSET (pActOutPort, OFC_ZERO, sizeof (tOfcActOutputPort));
                pActOutPort->OfcOutput.u4Port = pOfcActs->Acts.Output.u4Port;
                pActOutPort->OfcOutput.u2MaxLen =
                    pOfcActs->Acts.Output.u2MaxLen;
                TMO_SLL_Add (pOutput, &(pActOutPort->Node));
                break;

                /* Add a new VLAN Tag */
            case OFCAT_PUSH_VLAN:
                pActPush = (tOfcActPush *) ((VOID *) &(pOfcActs->Acts));
                u4RetVal = Ofc131ActsPushVlanTag (pPkt, pActPush->u2EthType);
                if (OFC_FAILURE == u4RetVal)
                {
                    /* Push VLAN Tag Failed */
                    OFC_TRC_FUNC ((OFC_ACTS_TRC, "Push VLAN Tag FAILURE\n"));
                    return OFC_FAILURE;
                }
                break;

                /* Remove the outermost VLAN Tag */
            case OFCAT_POP_VLAN:
                u4RetVal = Ofc131ActsPopVlanTag (pPkt);
                if (OFC_FAILURE == u4RetVal)
                {
                    /* Pop VLAN Tag Failed */
                    OFC_TRC_FUNC ((OFC_ACTS_TRC, "Pop VLAN Tag FAILURE\n"));
                    return OFC_FAILURE;
                }
                break;

                /* Add a new MPLS Header */
            case OFCAT_PUSH_MPLS:
                pActPush = (tOfcActPush *) & (pOfcActs->Acts);
                u4RetVal = Ofc131ActsPushMplsTag (pPkt, pActPush->u2EthType);
                if (OFC_FAILURE == u4RetVal)
                {
                    /* Push MPLS Tag Failed */
                    OFC_TRC_FUNC ((OFC_ACTS_TRC, "Push MPLS Tag FAILURE\n"));
                    return OFC_FAILURE;
                }
                break;

                /* Remove the outermost MPLS Tag */
            case OFCAT_POP_MPLS:
                pActPopMpls = (tOfcActPopMpls *) & (pOfcActs->Acts);
                u4RetVal = Ofc131ActsPopMplsTag (pPkt, pActPopMpls->u2EthType);
                if (OFC_FAILURE == u4RetVal)
                {
                    /* Pop MPLS Tag Failed */
                    OFC_TRC_FUNC ((OFC_ACTS_TRC, "Pop MPLS Tag FAILURE\n"));
                    return OFC_FAILURE;
                }
                break;

                /* Add a new PBB Instance Header */
            case OFCAT_PUSH_PBB:
                pActPush = (tOfcActPush *) & (pOfcActs->Acts);
                u4RetVal = Ofc131ActsPushPbbTag (pPkt, pActPush->u2EthType);
                if (OFC_FAILURE == u4RetVal)
                {
                    /* Push PBB Tag Failed */
                    OFC_TRC_FUNC ((OFC_ACTS_TRC, "Push PBB Tag FAILURE\n"));
                    return OFC_FAILURE;
                }
                break;

                /* Remove the outermost PBB Instance Header */
            case OFCAT_POP_PBB:
                u4RetVal = Ofc131ActsPopPbbTag (pPkt);
                if (OFC_FAILURE == u4RetVal)
                {
                    /* Pop PBB Tag Failed */
                    OFC_TRC_FUNC ((OFC_ACTS_TRC, "Push PBB Tag FAILURE\n"));
                    return OFC_FAILURE;
                }
                break;

                /* Copy the Group ID */
            case OFCAT_GROUP:
                pActGroup = (tOfcActGroup *) & (pOfcActs->Acts);
                MEMCPY (pi4GroupId, &(pActGroup->u4GroupId), OFC_FOUR);
                break;

                /* Set the IPV4 TTL or IPV6 HopLimit */
            case OFCAT_SET_NW_TTL:
                pActSetNwTtl = (tOfcActSetNwTtl *) & (pOfcActs->Acts);

                /* Get the Offset for copying the new TTL */
                u4RetVal = Ofc131ActsComputeHdrOffset (pPkt, OFC_IP_PROTO,
                                                       &u2ActType, &u2Offset);

                if (OFC_FAILURE == u4RetVal)
                {
                    OFC_TRC_FUNC ((OFC_ACTS_TRC, "Set Network TTL FAILURE\n"));
                    return OFC_FAILURE;
                }

                if (u2ActType == OFC_L3_IP4)
                {
                    CRU_BUF_Copy_OverBufChain (pPkt, &(pActSetNwTtl->u1NwTtl),
                                               (UINT4) ((UINT4) u2Offset +
                                                        (UINT4)
                                                        OFC_IPV4_TTL_OFFSET),
                                               OFC_ONE);

                    u1OneByte = OFC_ZERO;
                    u2TwoByte = OFC_ZERO;
                    /* Recaliculate checksum and copy it back */
                    CRU_BUF_Copy_FromBufChain (pPkt, &u1OneByte,
                                               u2Offset, OFC_ONE);
                    u1OneByte =
                        (UINT1) ((UINT1)
                                 (u1OneByte & (UINT1) OFC_IPV4_IHL_UNMASK) *
                                 (UINT1) OFC_IPV4_WORD_SIZE);
                    u2TwoByte =
                        Ofc131ActsComputeIPChecksum (pPkt, u1OneByte, u2Offset);
                    CRU_BUF_Copy_OverBufChain (pPkt, (UINT1 *) &u2TwoByte,
                                               (UINT4) ((UINT4) u2Offset +
                                                        (UINT4)
                                                        OFC_IPV4_CHKSUM_OFFSET),
                                               OFC_TWO);
                }
                else
                {
                    /* It is IPv6 */
                    CRU_BUF_Copy_OverBufChain (pPkt, &(pActSetNwTtl->u1NwTtl),
                                               (UINT4) ((UINT4) u2Offset +
                                                        (UINT4)
                                                        OFC_IPV6_TTL_OFFSET),
                                               OFC_ONE);
                }
                break;

                /* Decrement IPV4 TTL or IPV6 HopLimit by 1 */
            case OFCAT_DEC_NW_TTL:
                /* Get the Offset for decrementing the TTL */
                u4RetVal = Ofc131ActsComputeHdrOffset (pPkt, OFC_IP_PROTO,
                                                       &u2ActType, &u2Offset);
                if (u4RetVal == OFC_FAILURE)
                {
                    OFC_TRC_FUNC ((OFC_ACTS_TRC, "Decrement Network TTL - "
                                   "return FAILURE\n"));
                    return OFC_FAILURE;
                }
                if (u2ActType == OFC_L3_IP4)
                {
                    CRU_BUF_Copy_FromBufChain (pPkt, &u1OneByte,
                                               (UINT4) ((UINT4) u2Offset +
                                                        (UINT4)
                                                        OFC_IPV4_TTL_OFFSET),
                                               OFC_ONE);

                    if (u1OneByte < OFC_TWO)
                    {
                        OFC_TRC_FUNC ((OFC_ACTS_TRC, "IP4 TTL is/will be zero,"
                                       "return FAILURE\n"));
                        return OFC_FAILURE;
                    }
                    u1OneByte = (UINT1) (u1OneByte - OFC_ONE);
                    CRU_BUF_Copy_OverBufChain (pPkt, &u1OneByte,
                                               (UINT4) ((UINT4) u2Offset +
                                                        OFC_IPV4_TTL_OFFSET),
                                               OFC_ONE);

                    u1OneByte = OFC_ZERO;
                    u2TwoByte = OFC_ZERO;
                    /* Recalculate Checksum and copy it back into packet */
                    CRU_BUF_Copy_FromBufChain (pPkt, &u1OneByte,
                                               u2Offset, OFC_ONE);
                    u1OneByte =
                        (UINT1) ((UINT1)
                                 (u1OneByte & (UINT1) OFC_IPV4_IHL_UNMASK) *
                                 (UINT1) OFC_IPV4_WORD_SIZE);
                    u2TwoByte =
                        Ofc131ActsComputeIPChecksum (pPkt, u1OneByte, u2Offset);
                    CRU_BUF_Copy_OverBufChain (pPkt, (UINT1 *) &u2TwoByte,
                                               (UINT4) ((UINT4) u2Offset +
                                                        OFC_IPV4_CHKSUM_OFFSET),
                                               OFC_TWO);
                }
                else
                {
                    /* It is IPv6 */
                    CRU_BUF_Copy_FromBufChain (pPkt, &u1OneByte,
                                               (UINT4) ((UINT4) u2Offset +
                                                        OFC_IPV6_TTL_OFFSET),
                                               OFC_ONE);
                    if (u1OneByte < OFC_TWO)
                    {
                        OFC_TRC_FUNC ((OFC_ACTS_TRC, "IP6 TTL is/will be zero,"
                                       "return FAILURE\n"));
                        return OFC_FAILURE;
                    }
                    u1OneByte = (UINT1) (u1OneByte - OFC_ONE);
                    CRU_BUF_Copy_OverBufChain (pPkt, &u1OneByte,
                                               (UINT4) ((UINT4) u2Offset +
                                                        OFC_IPV6_TTL_OFFSET),
                                               OFC_ONE);
                }
                break;

                /* Set the MPLS TTL */
            case OFCAT_SET_MPLS_TTL:
                pActSetMplsTtl = (tOfcActSetMplsTtl *) & (pOfcActs->Acts);

                /* Get the Offset for copying the new TTL */
                u4RetVal = Ofc131ActsComputeHdrOffset (pPkt, OFC_MPLS_PROTO,
                                                       &u2ActType, &u2Offset);
                if (u4RetVal == OFC_FAILURE)
                {
                    OFC_TRC_FUNC ((OFC_ACTS_TRC, "Set MPLS TTL FAILURE\n"));
                    return OFC_FAILURE;
                }

                CRU_BUF_Copy_OverBufChain (pPkt, &(pActSetMplsTtl->u1MplsTtl),
                                           (UINT4) (UINT4) (u2Offset +
                                                            (UINT4)
                                                            OFC_MPLS_TTL_OFFSET),
                                           OFC_ONE);
                break;

                /* Decrement MPLS TTL by 1 */
            case OFCAT_DEC_MPLS_TTL:
                /* Get the Offset for decrementing the TTL */
                u4RetVal = Ofc131ActsComputeHdrOffset (pPkt, OFC_MPLS_PROTO,
                                                       &u2ActType, &u2Offset);
                if (u4RetVal == OFC_FAILURE)
                {
                    OFC_TRC_FUNC ((OFC_ACTS_TRC,
                                   "Decrment MPLS TTL FAILURE\n"));
                    return OFC_FAILURE;
                }
                CRU_BUF_Copy_FromBufChain (pPkt, &u1OneByte,
                                           (UINT4) ((UINT4) u2Offset +
                                                    OFC_MPLS_TTL_OFFSET),
                                           OFC_ONE);

                if (u1OneByte < OFC_TWO)
                {
                    OFC_TRC_FUNC ((OFC_ACTS_TRC, "IP4 TTL is/will be zero,"
                                   "return FAILURE\n"));
                    return OFC_FAILURE;
                }
                u1OneByte = (UINT1) (u1OneByte - OFC_ONE);
                CRU_BUF_Copy_OverBufChain (pPkt, &u1OneByte,
                                           (UINT4) ((UINT4) u2Offset +
                                                    OFC_MPLS_TTL_OFFSET),
                                           OFC_ONE);
                break;

                /* Copy the TTL IN */
            case OFCAT_COPY_TTL_IN:
                /*
                 * IP-to-IP, MPLS-to-MPLS or MPLS-to-IP
                 */
                /* Check if there is a MPLS Header */
                u4RetVal = Ofc131ActsComputeHdrOffset (pPkt, OFC_MPLS_PROTO,
                                                       &u2ActType, &u2Offset);

                if (u4RetVal == OFC_FAILURE)
                {
                    /* No MPLS Header, its IP-to-IP so do nothing */
                }
                else
                {

                    /* Check if there is another MPLS Header */
                    CRU_BUF_Copy_FromBufChain (pPkt, &u1OneByte,
                                               (UINT4) ((UINT4) u2Offset +
                                                        OFC_FOUR), OFC_ONE);
                    if (u1OneByte & OFC_MPLS_BOS_CHCK_MASK)
                    {
                        CRU_BUF_Copy_FromBufChain (pPkt, &u1OneByte,
                                                   (UINT4) (u2Offset +
                                                            OFC_MPLS_TTL_OFFSET),
                                                   OFC_ONE);
                        if (u1OneByte == OFC_ZERO)
                        {
                            return OFC_FAILURE;
                        }
                        /* Outermost MPLS -to- Next Outermost MPLS */
                        CRU_BUF_Copy_OverBufChain (pPkt, &u1OneByte,
                                                   (UINT4) ((UINT4) u2Offset +
                                                            MPLS_HDR_LEN +
                                                            OFC_MPLS_TTL_OFFSET),
                                                   OFC_ONE);
                    }
                    else
                    {
                        /* MPLS-to-IP */
                    }
                }
                break;

                /* Copy TTL Out */
            case OFCAT_COPY_TTL_OUT:
                /*
                 * IP-to-IP, MPLS-to-MPLS or IP-to-MPLS
                 */
                /* Check if there is a MPLS Header */
                u4RetVal = Ofc131ActsComputeHdrOffset (pPkt, OFC_MPLS_PROTO,
                                                       &u2ActType, &u2Offset);

                if (u4RetVal == OFC_FAILURE)
                {
                    /* No MPLS Header, its IP-to-IP so do nothing */
                }
                else
                {
                    /* Check if there is another MPLS Header */
                    CRU_BUF_Copy_FromBufChain (pPkt, &u1OneByte,
                                               (UINT4) ((UINT4) u2Offset +
                                                        (UINT4) OFC_FOUR),
                                               OFC_ONE);
                    if (u1OneByte & OFC_MPLS_BOS_CHCK_MASK)
                    {
                        /* Next Outermost MPLS -to- Outermost MPLS */
                        CRU_BUF_Copy_FromBufChain (pPkt, &u1OneByte,
                                                   (UINT4) (u2Offset +
                                                            OFC_MPLS_TTL_OFFSET),
                                                   OFC_ONE);
                        if (u1OneByte == OFC_ZERO)
                        {
                            return OFC_FAILURE;
                        }
                        CRU_BUF_Copy_OverBufChain (pPkt, &u1OneByte,
                                                   (UINT4) (u2Offset +
                                                            MPLS_HDR_LEN +
                                                            OFC_MPLS_TTL_OFFSET),
                                                   OFC_ONE);
                    }
                    else
                    {
                        /* IP-to-MPLS */
                    }
                }
                break;

                /* Set Field Action */
            case OFCAT_SET_FIELD:
                pActSetFld = (tOfcActSetFld *) & (pOfcActs->Acts);

                u4RetVal = Ofc131ActsApplySetFlds (pPkt,
                                                   &(pActSetFld->SetFldList),
                                                   &u1BosFlag);
                if (u4RetVal == OFC_FAILURE)
                {
                    OFC_TRC_FUNC ((OFC_ACTS_TRC, "Set Field Action FAILURE\n"));
                    /* Apply SetFields Failed */
                    return OFC_FAILURE;
                }
                break;

                /* Set Queue Action */
            case OFCAT_SET_QUEUE:
                /* Code to be added */
                break;

            case OFCAT_EXPERIMENTER:

            default:
                /* Unsupported action */
                OFC_TRC_FUNC ((OFC_ACTS_TRC, "UnSupported Action FAILURE\n"));
                return OFC_FAILURE;
        }
    }
    OFC_TRC_FUNC ((OFC_FN_EXIT, "FUNC:Ofc131ActsApplyActionList SUCCESS\n"));
    return OFC_SUCCESS;
}

/******************************************************************************
 * Function    : Ofc131ActsApplyActionSet 
 * Description : This routine applies all the actions as part of the
 *               Action Set onto the data packet. 
 * Input       : pPkt        - Pointer to the packet CRU Buffer
 * Output      : pi4GroupId  - Pointer to Group Entry, in case any
 *               pOutput     - Pointer to Output SLL, in case any 
 * Returns     : OFC_SUCCESS or OFC_FAILURE
 ******************************************************************************/
PRIVATE UINT4
Ofc131ActsApplyActionSet (tCRU_BUF_CHAIN_HEADER * pPkt, INT4 *pi4GroupId,
                          tOfcSll * pOutput)
{
    tOfcActOutput      *pActOutput = NULL;
    tOfcActGroup       *pActGroup = NULL;
    tOfcActSetNwTtl    *pActSetNwTtl = NULL;
    tOfcActSetMplsTtl  *pActSetMplsTtl = NULL;
    tOfcActPush        *pActPush = NULL;
    tOfcActPopMpls     *pActPopMpls = NULL;
    tOxmFld            *pOxmFld = NULL;
    tOfcActOutputPort  *pActOutPort = NULL;
    UINT4               u4RetVal = OFC_ZERO;
    UINT2               u2Offset = OFC_ZERO;
    UINT2               u2TempOffset = OFC_ZERO;
    UINT2               u2ActType = OFC_ZERO;
    UINT2               u2SetField = OFC_ZERO;
    UINT1               u1OneByte = OFC_ZERO;
    UINT1               u1TempOneByte = OFC_ZERO;
    UINT2               u2TwoByte = OFC_ZERO;
    UINT2               u2TempTwoByte = OFC_ZERO;
    UINT4               u4FourByte = OFC_ZERO;
    UINT4               u4TempFourByte = OFC_ZERO;

    OFC_TRC_FUNC ((OFC_FN_ENTRY, "FUNC:Ofc131ActsApplyActionSet\n"));
    /*
     * Actions from Action Set has to be applied according to the order
     * the cases are written in switch cases.
     * Packet Tx/Rx Module will make sure the actions in the
     * Flow Entry are Ordered in the required manner. 
     */
    /* Action - Copy TTL from OuterMost to NextOuterMost Header */
    if (gActionSet.au1ActsBitMap[OFCAT_COPY_TTL_IN])
    {
        /*
         * IP-to-IP, MPLS-to-MPLS or MPLS-to-IP
         */
        /* Check if there is a MPLS Header */
        u4RetVal = Ofc131ActsComputeHdrOffset (pPkt, OFC_MPLS_PROTO,
                                               &u2ActType, &u2Offset);

        if (u4RetVal == OFC_FAILURE)
        {
            /* No MPLS Header, Not Handling Ip-in-Ip Case */
        }
        else
        {
            /* Check if there is another MPLS Header */
            CRU_BUF_Copy_FromBufChain (pPkt, &u1OneByte,
                                       (UINT4) ((UINT4) u2Offset +
                                                (UINT4) OFC_TWO), OFC_ONE);

            if (u1OneByte & OFC_MPLS_BOS_CHCK_MASK)
            {
                CRU_BUF_Copy_FromBufChain (pPkt, &u1OneByte,
                                           (UINT4) (u2Offset +
                                                    OFC_MPLS_TTL_OFFSET),
                                           OFC_ONE);
                if (u1OneByte == OFC_ZERO)
                {
                    OFC_TRC_FUNC ((OFC_ACTS_TRC, "[AS] Copy ttl value zero\n"));
                    return OFC_FAILURE;
                }
                /* Outermost MPLS -to- Next Outermost MPLS */
                CRU_BUF_Copy_OverBufChain (pPkt, &u1OneByte,
                                           (UINT4) (u2Offset + MPLS_HDR_LEN +
                                                    OFC_MPLS_TTL_OFFSET),
                                           OFC_ONE);
            }
            else
            {
                /* MPLS-to-IP */
            }
        }
    }

    /* Action - Remove the OuterMost VLAN Header */
    if (gActionSet.au1ActsBitMap[OFCAT_POP_VLAN])
    {
        u4RetVal = Ofc131ActsPopVlanTag (pPkt);
        if (OFC_FAILURE == u4RetVal)
        {
            /* Pop VLAN Tag Failed */
            OFC_TRC_FUNC ((OFC_ACTS_TRC, "[AS] Pop VLAN Tag FAILURE\n"));
            return OFC_FAILURE;
        }
    }

    /* Action - Remove the OuterMost MPLS Header */
    if (gActionSet.au1ActsBitMap[OFCAT_POP_MPLS])
    {
        pActPopMpls = (tOfcActPopMpls *) &
            (gActionSet.Acts[OFC_ASO_POP_MPLS].PopMpls);
        u4RetVal = Ofc131ActsPopMplsTag (pPkt, pActPopMpls->u2EthType);
        if (OFC_FAILURE == u4RetVal)
        {
            /* Pop MPLS Tag Failed */
            OFC_TRC_FUNC ((OFC_ACTS_TRC, "[AS] Pop MPLS Tag FAILURE\n"));
            return OFC_FAILURE;
        }
    }

    /* Action - Remove the OuterMost PBB Header */
    if (gActionSet.au1ActsBitMap[OFCAT_POP_PBB])
    {
        u4RetVal = Ofc131ActsPopPbbTag (pPkt);
        if (OFC_FAILURE == u4RetVal)
        {
            /* Pop PBB Tag Failed */
            OFC_TRC_FUNC ((OFC_ACTS_TRC, "[AS] Pop PBB Tag FAILURE\n"));
            return OFC_FAILURE;
        }
    }

    /* Action - Add a new MPLS Header */
    if (gActionSet.au1ActsBitMap[OFCAT_PUSH_MPLS])
    {
        pActPush = (tOfcActPush *) & (gActionSet.Acts[OFC_ASO_PUSH_MPLS].Push);
        u4RetVal = Ofc131ActsPushMplsTag (pPkt, pActPush->u2EthType);
        if (OFC_FAILURE == u4RetVal)
        {
            /* Push MPLS Tag Failed */
            OFC_TRC_FUNC ((OFC_ACTS_TRC, "[AS] Push MPLS Tag FAILURE\n"));
            return OFC_FAILURE;
        }
    }

    /* Action - Add a new PBB Header */
    if (gActionSet.au1ActsBitMap[OFCAT_PUSH_PBB])
    {
        pActPush = (tOfcActPush *) & (gActionSet.Acts[OFC_ASO_PUSH_PBB].Push);
        u4RetVal = Ofc131ActsPushPbbTag (pPkt, pActPush->u2EthType);
        if (OFC_FAILURE == u4RetVal)
        {
            /* Push PBB Tag Failed */
            OFC_TRC_FUNC ((OFC_ACTS_TRC, "[AS] Push MPLS Tag FAILURE\n"));
            return OFC_FAILURE;
        }
    }

    /* Action - Add a new VLAN Header */
    if (gActionSet.au1ActsBitMap[OFCAT_PUSH_VLAN])
    {
        pActPush = (tOfcActPush *) & (gActionSet.Acts[OFC_ASO_PUSH_VLAN].Push);
        u4RetVal = Ofc131ActsPushVlanTag (pPkt, pActPush->u2EthType);
        if (OFC_FAILURE == u4RetVal)
        {
            /* Push VLAN Tag Failed */
            OFC_TRC_FUNC ((OFC_ACTS_TRC, "[AS] Push VLAN Tag FAILURE\n"));
            return OFC_FAILURE;
        }
    }

    /* Action - Copy TTL from NextOuterMost to OuterMost Header */
    if (gActionSet.au1ActsBitMap[OFCAT_COPY_TTL_OUT])
    {
        /*
         * IP-to-IP, MPLS-to-MPLS or IP-to-MPLS
         */
        /* Check if there is a MPLS Header */
        u4RetVal = Ofc131ActsComputeHdrOffset (pPkt, OFC_MPLS_PROTO,
                                               &u2ActType, &u2Offset);

        if (u4RetVal == OFC_FAILURE)
        {
            /* No MPLS Header, its IP-to-IP so do nothing */
        }
        else
        {
            /* Check if there is another MPLS Header */
            CRU_BUF_Copy_FromBufChain (pPkt, &u1OneByte,
                                       (UINT4) ((UINT4) u2Offset +
                                                (UINT4) OFC_TWO), OFC_ONE);

            if (u1OneByte & OFC_MPLS_BOS_CHCK_MASK)
            {
                /* Next Outermost MPLS -to- Outermost MPLS */
                CRU_BUF_Copy_FromBufChain (pPkt, &u1OneByte,
                                           (UINT4) (u2Offset +
                                                    OFC_MPLS_TTL_OFFSET),
                                           OFC_ONE);
                if (u1OneByte == OFC_ZERO)
                {
                    /* ttl zero */
                    return OFC_FAILURE;
                }
                CRU_BUF_Copy_OverBufChain (pPkt, &u1OneByte,
                                           (UINT4) (u2Offset + MPLS_HDR_LEN +
                                                    OFC_MPLS_TTL_OFFSET),
                                           OFC_ONE);
            }
            else
            {
                /* Do Nothing */
            }
        }
    }

    /* Action - Decrement MPLS TTL */
    if (gActionSet.au1ActsBitMap[OFCAT_DEC_MPLS_TTL])
    {
        /* Get the Offset for decrementing the TTL */
        u4RetVal = Ofc131ActsComputeHdrOffset (pPkt, OFC_MPLS_PROTO,
                                               &u2ActType, &u2Offset);
        if (u4RetVal == OFC_FAILURE)
        {
            OFC_TRC_FUNC ((OFC_ACTS_TRC, "[AS] Decrement MPLS TTL FAILURE\n"));
            return OFC_FAILURE;
        }
        CRU_BUF_Copy_FromBufChain (pPkt, &u1OneByte,
                                   (UINT4) (u2Offset + OFC_MPLS_TTL_OFFSET),
                                   OFC_ONE);

        if (u1OneByte < OFC_TWO)
        {
            OFC_TRC_FUNC ((OFC_ACTS_TRC, "[AS] MPLS ttl is/will be zero\n"));
            return OFC_FAILURE;
        }
        u1OneByte = (UINT1) (u1OneByte - OFC_ONE);

        CRU_BUF_Copy_OverBufChain (pPkt, &u1OneByte,
                                   (UINT4) (u2Offset + OFC_MPLS_TTL_OFFSET),
                                   OFC_ONE);
    }

    /* Action - Decrement IPv4 TTL or IPv6 HopLimit */
    if (gActionSet.au1ActsBitMap[OFCAT_DEC_NW_TTL])
    {
        /* Get the Offset for decrementing the TTL */
        u4RetVal = Ofc131ActsComputeHdrOffset (pPkt, OFC_IP_PROTO,
                                               &u2ActType, &u2Offset);
        if (u4RetVal == OFC_FAILURE)
        {
            OFC_TRC_FUNC ((OFC_ACTS_TRC,
                           "[AS] Decrement NetworkTTL FAILURE\n"));
            return OFC_FAILURE;
        }

        if (u2ActType == OFC_L3_IP4)
        {
            /* Decrement the TTL */
            CRU_BUF_Copy_FromBufChain (pPkt, &u1OneByte,
                                       (UINT4) (u2Offset + OFC_IPV4_TTL_OFFSET),
                                       OFC_ONE);

            if (u1OneByte < OFC_TWO)
            {
                OFC_TRC_FUNC ((OFC_ACTS_TRC, "[AS] IP4 ttl is/will be zero\n"));
                return OFC_FAILURE;
            }
            u1OneByte = (UINT1) (u1OneByte - OFC_ONE);
            CRU_BUF_Copy_OverBufChain (pPkt, &u1OneByte,
                                       (UINT4) (u2Offset + OFC_IPV4_TTL_OFFSET),
                                       OFC_ONE);

            u1OneByte = OFC_ZERO;

            /* Get the Header Length for recaliculating checksum */
            CRU_BUF_Copy_FromBufChain (pPkt, &u1OneByte, u2Offset, OFC_ONE);
            u1OneByte =
                (UINT1) ((UINT1) (u1OneByte & (UINT1) OFC_IPV4_IHL_UNMASK) *
                         (UINT1) OFC_IPV4_WORD_SIZE);

            u2TwoByte = OFC_ZERO;
            /* Copy the Checksum */
            u2TwoByte = Ofc131ActsComputeIPChecksum (pPkt, u1OneByte, u2Offset);
            CRU_BUF_Copy_OverBufChain (pPkt, (UINT1 *) &u2TwoByte,
                                       (UINT4) ((UINT4) (u2Offset +
                                                         (UINT4)
                                                         OFC_IPV4_CHKSUM_OFFSET)),
                                       OFC_TWO);
        }
        else
        {
            /* It is IPv6 */
            CRU_BUF_Copy_FromBufChain (pPkt, &u1OneByte,
                                       (UINT4) ((UINT4) u2Offset +
                                                (UINT4) OFC_IPV6_TTL_OFFSET),
                                       OFC_ONE);

            if (u1OneByte < OFC_TWO)
            {
                OFC_TRC_FUNC ((OFC_ACTS_TRC, "[AS] IP6 ttl is/will be zero\n"));
                return OFC_FAILURE;
            }
            u1OneByte = (UINT1) (u1OneByte - OFC_ONE);
            CRU_BUF_Copy_OverBufChain (pPkt, &u1OneByte,
                                       (UINT4) (u2Offset + OFC_IPV6_TTL_OFFSET),
                                       OFC_ONE);
        }
    }

    /* Action - Set the MPLS TTL to given Value */
    if (gActionSet.au1ActsBitMap[OFCAT_SET_MPLS_TTL])
    {
        pActSetMplsTtl = (tOfcActSetMplsTtl *) &
            (gActionSet.Acts[OFC_ASO_SET_MPLS_TTL].SetMplsTtl);

        /* Get the Offset for copying the new TTL */
        u4RetVal = Ofc131ActsComputeHdrOffset (pPkt, OFC_MPLS_PROTO,
                                               &u2ActType, &u2Offset);
        if (u4RetVal == OFC_FAILURE)
        {
            OFC_TRC_FUNC ((OFC_ACTS_TRC, "[AS] Set MPLS TTL FAILURE\n"));
            return OFC_FAILURE;
        }
        CRU_BUF_Copy_OverBufChain (pPkt, &(pActSetMplsTtl->u1MplsTtl),
                                   (UINT4) ((UINT4) u2Offset +
                                            (UINT4) OFC_MPLS_TTL_OFFSET),
                                   OFC_ONE);

    }

    /* Action - Set the IPv4 TTL or IPv6 HopLimit to given Value */
    if (gActionSet.au1ActsBitMap[OFCAT_SET_NW_TTL])
    {
        pActSetNwTtl = (tOfcActSetNwTtl *) &
            (gActionSet.Acts[OFC_ASO_SET_NW_TTL].SetNwTtl);

        /* Get the Offset for copying the new TTL */
        u4RetVal = Ofc131ActsComputeHdrOffset (pPkt, OFC_IP_PROTO,
                                               &u2ActType, &u2Offset);

        if (OFC_FAILURE == u4RetVal)
        {
            OFC_TRC_FUNC ((OFC_ACTS_TRC, "[AS] Set Network TTL FAILURE\n"));
            return OFC_FAILURE;
        }

        if (u2ActType == OFC_L3_IP4)
        {
            CRU_BUF_Copy_OverBufChain (pPkt, &(pActSetNwTtl->u1NwTtl),
                                       (UINT4) ((UINT4) u2Offset +
                                                (UINT4) OFC_IPV4_TTL_OFFSET),
                                       OFC_ONE);

            /* Recalculate the Checksum */
            u1OneByte = OFC_ZERO;
            u2TwoByte = OFC_ZERO;
            CRU_BUF_Copy_FromBufChain (pPkt, &u1OneByte, u2Offset, OFC_ONE);
            u1OneByte =
                (UINT1) ((UINT1) (u1OneByte & (UINT1) OFC_IPV4_IHL_UNMASK) *
                         (UINT1) OFC_IPV4_WORD_SIZE);
            u2TwoByte = Ofc131ActsComputeIPChecksum (pPkt, u1OneByte, u2Offset);

            u1OneByte = (UINT1) u2TwoByte;    /* Length "value" copy */
            /* Copy it into the packet */
            CRU_BUF_Copy_OverBufChain (pPkt, &u1OneByte,
                                       (UINT4) ((UINT4) u2Offset +
                                                (UINT4) OFC_IPV4_CHKSUM_OFFSET),
                                       OFC_ONE);
        }
        else
        {
            /* It is IPv6 */
            CRU_BUF_Copy_OverBufChain (pPkt, &(pActSetNwTtl->u1NwTtl),
                                       (UINT4) ((UINT4) u2Offset +
                                                (UINT4) OFC_IPV6_TTL_OFFSET),
                                       OFC_ONE);
        }
    }

    /* Action - Apply the SetFields */
    if (gActionSet.au1ActsBitMap[OFCAT_SET_FIELD])
    {
        u2SetField = OFC_ZERO;
        while (u2SetField < OFC_MAX_SETFIELDS)
        {
            if (gActionSet.au1SetFldBitMap[u2SetField])
            {
                pOxmFld = &(gActionSet.SetFld[u2SetField]);
                switch (u2SetField)
                {
                    case OFCXMT_OFB_ETH_DST:
                        CRU_BUF_Copy_OverBufChain (pPkt, pOxmFld->au1Fld,
                                                   OFC_ZERO, OFC_ETH_ADDR_LEN);
                        break;

                    case OFCXMT_OFB_ETH_SRC:
                        CRU_BUF_Copy_OverBufChain (pPkt, pOxmFld->au1Fld,
                                                   OFC_ETH_SRC_OFFSET,
                                                   OFC_ETH_ADDR_LEN);
                        break;

                    case OFCXMT_OFB_ETH_TYPE:
                        CRU_BUF_Copy_OverBufChain (pPkt, pOxmFld->au1Fld,
                                                   OFC_ETHTYPE_OFFSET,
                                                   OFC_ETHTYPE_SIZE);
                        break;

                    case OFCXMT_OFB_VLAN_VID:
                        /* Check the presence of VLAN Tag */
                        u4RetVal =
                            Ofc131ActsComputeHdrOffset (pPkt, OFC_VLAN_PROTO,
                                                        &u2ActType, &u2Offset);
                        if (OFC_FAILURE == u4RetVal)
                        {
                            OFC_TRC_FUNC ((OFC_ACTS_TRC,
                                           "[AS] SetField VLAN VID -"
                                           " No VLAN Header, FAILURE\n"));
                            return OFC_FAILURE;
                        }
                        CRU_BUF_Copy_FromBufChain (pPkt, (UINT1 *) &u2TwoByte,
                                                   (UINT4) ((UINT4) u2Offset +
                                                            (UINT4)
                                                            OFC_VLAN_PROTO_SIZE),
                                                   OFC_TWO);

                        u2TwoByte = OSIX_NTOHS (u2TwoByte);
                        /* VID includes CFI, set existing CFI to zero as well */
                        u2TwoByte = u2TwoByte & OFC_VLAN_VID_UNMASK;
                        MEMCPY (&u2TempTwoByte, pOxmFld->au1Fld, OFC_TWO);
                        u2TempTwoByte = OSIX_NTOHS (u2TempTwoByte);
                        u2TwoByte = u2TwoByte | u2TempTwoByte;
                        u2TwoByte = OSIX_HTONS (u2TwoByte);
                        CRU_BUF_Copy_OverBufChain (pPkt, (UINT1 *) &u2TwoByte,
                                                   (UINT4) ((UINT4) u2Offset +
                                                            (UINT4)
                                                            OFC_VLAN_PROTO_SIZE),
                                                   OFC_TWO);
                        break;

                    case OFCXMT_OFB_VLAN_PCP:
                        u4RetVal =
                            Ofc131ActsComputeHdrOffset (pPkt, OFC_VLAN_PROTO,
                                                        &u2ActType, &u2Offset);
                        if (OFC_FAILURE == u4RetVal)
                        {
                            OFC_TRC_FUNC ((OFC_ACTS_TRC,
                                           "[AS] SetField VLAN PCP -"
                                           " No VLAN Header, FAILURE\n"));
                            return OFC_FAILURE;
                        }
                        CRU_BUF_Copy_FromBufChain (pPkt, &u1OneByte,
                                                   (UINT4) ((UINT4) u2Offset +
                                                            (UINT4)
                                                            OFC_VLAN_PROTO_SIZE),
                                                   OFC_ONE);

                        u1OneByte = u1OneByte & OFC_VLAN_PCP_UNMASK;
                        u1TempOneByte = pOxmFld->au1Fld[OFC_ZERO];
                        u1OneByte =
                            (UINT1) (u1OneByte |
                                     (UINT1) (u1TempOneByte << (UINT1)
                                              OFC_FIVE));

                        CRU_BUF_Copy_OverBufChain (pPkt, &u1OneByte,
                                                   (UINT4) ((UINT4) u2Offset +
                                                            (UINT4)
                                                            OFC_VLAN_PROTO_SIZE),
                                                   OFC_ONE);
                        break;

                    case OFCXMT_OFB_IP_DSCP:
                        u4RetVal =
                            Ofc131ActsComputeHdrOffset (pPkt, OFC_IP_PROTO,
                                                        &u2ActType, &u2Offset);

                        if (OFC_FAILURE == u4RetVal)
                        {
                            OFC_TRC_FUNC ((OFC_ACTS_TRC,
                                           "[AS] SetField IP DSCP - "
                                           "No IP Header, FAILURE\n"));
                            return OFC_FAILURE;
                        }

                        if (u2ActType == OFC_L3_IP4)
                        {
                            /* Copy the existing ECN */
                            CRU_BUF_Copy_FromBufChain (pPkt, &u1OneByte,
                                                       (UINT4) ((UINT4) u2Offset
                                                                +
                                                                (UINT4)
                                                                OFC_IPV4_DSCP_OFFSET),
                                                       OFC_ONE);

                            u1OneByte = u1OneByte & OFC_IPV4_DSCP_UNMASK;
                            u1TempOneByte = pOxmFld->au1Fld[OFC_ZERO];
                            u1OneByte = (UINT1) (u1OneByte |
                                                 (UINT1) (u1TempOneByte <<
                                                          (UINT1) OFC_TWO));

                            CRU_BUF_Copy_OverBufChain (pPkt, &u1OneByte,
                                                       (UINT4) ((UINT4) u2Offset
                                                                +
                                                                (UINT4)
                                                                OFC_ONE),
                                                       OFC_ONE);

                            /* Recalculate the Checksum */
                            u1OneByte = OFC_ZERO;
                            u2TwoByte = OFC_ZERO;
                            CRU_BUF_Copy_FromBufChain (pPkt, &u1OneByte,
                                                       u2Offset, OFC_ONE);
                            u1OneByte = (UINT1) ((u1OneByte & (UINT1)
                                                  OFC_IPV4_IHL_UNMASK) *
                                                 (UINT1) OFC_IPV4_WORD_SIZE);
                            u2TwoByte = Ofc131ActsComputeIPChecksum (pPkt,
                                                                     u1OneByte,
                                                                     u2Offset);
                            /* Copy the new Checksum into the packet */
                            CRU_BUF_Copy_OverBufChain (pPkt,
                                                       (UINT1 *) &u2TwoByte,
                                                       (UINT4) ((UINT4) u2Offset
                                                                +
                                                                (UINT4)
                                                                OFC_IPV4_CHKSUM_OFFSET),
                                                       OFC_TWO);
                        }
                        else
                        {        /* It is IPv6 */
                            /* Copy the existing ECN */
                            CRU_BUF_Copy_FromBufChain (pPkt,
                                                       (UINT1 *) &u2TwoByte,
                                                       u2Offset, OFC_TWO);

                            u2TwoByte = OSIX_NTOHS (u2TwoByte);
                            u2TwoByte = u2TwoByte & OFC_IPV6_DSCP_UNMASK;
                            MEMCPY (&u2TempTwoByte, pOxmFld->au1Fld, OFC_TWO);
                            u2TempTwoByte = OSIX_NTOHS (u2TempTwoByte);
                            u2TwoByte = (UINT2) (u2TwoByte |
                                                 ((UINT2) u2TempTwoByte <<
                                                  (UINT2) OFC_SIX));

                            u2TwoByte = OSIX_HTONS (u2TwoByte);
                            CRU_BUF_Copy_OverBufChain (pPkt,
                                                       (UINT1 *) &u2TwoByte,
                                                       u2Offset, OFC_TWO);
                        }
                        break;

                    case OFCXMT_OFB_IP_ECN:
                        u4RetVal =
                            Ofc131ActsComputeHdrOffset (pPkt, OFC_IP_PROTO,
                                                        &u2ActType, &u2Offset);

                        if (OFC_FAILURE == u4RetVal)
                        {
                            OFC_TRC_FUNC ((OFC_ACTS_TRC,
                                           "[AS] SetField IP ECN - "
                                           "No IP Header, FAILURE\n"));
                            return OFC_FAILURE;
                        }

                        if (u2ActType == OFC_L3_IP4)
                        {
                            /* Copy the existing DSCP */
                            CRU_BUF_Copy_FromBufChain (pPkt, &u1OneByte,
                                                       (UINT4) ((UINT4) u2Offset
                                                                +
                                                                (UINT4)
                                                                OFC_IPV4_DSCP_OFFSET),
                                                       OFC_ONE);
                            u1OneByte = u1OneByte & OFC_IPV4_ECN_UNMASK;
                            u1TempOneByte = pOxmFld->au1Fld[OFC_ZERO];
                            u1OneByte = u1OneByte | u1TempOneByte;

                            CRU_BUF_Copy_OverBufChain (pPkt, &u1OneByte,
                                                       (UINT4) ((UINT4) u2Offset
                                                                +
                                                                (UINT4)
                                                                OFC_ONE),
                                                       OFC_ONE);

                            /* Recalculate the Checksum */
                            u1OneByte = OFC_ZERO;
                            u2TwoByte = OFC_ZERO;

                            CRU_BUF_Copy_FromBufChain (pPkt, &u1OneByte,
                                                       u2Offset, OFC_ONE);
                            u1OneByte = (UINT1) ((UINT1) (u1OneByte &
                                                          (UINT1)
                                                          OFC_IPV4_IHL_UNMASK) *
                                                 (UINT1) OFC_IPV4_WORD_SIZE);
                            u2TwoByte =
                                Ofc131ActsComputeIPChecksum (pPkt, u1OneByte,
                                                             u2Offset);
                            CRU_BUF_Copy_OverBufChain (pPkt,
                                                       (UINT1 *) &u2TwoByte,
                                                       (UINT4) ((UINT4) u2Offset
                                                                +
                                                                (UINT4)
                                                                OFC_IPV4_CHKSUM_OFFSET),
                                                       OFC_TWO);

                        }
                        else
                        {        /* It is IPv6 */
                            /* Copy the existing DSCP */
                            CRU_BUF_Copy_FromBufChain (pPkt,
                                                       (UINT1 *) &u2TwoByte,
                                                       u2Offset, OFC_TWO);

                            u2TwoByte = OSIX_NTOHS (u2TwoByte);
                            u2TwoByte = u2TwoByte & OFC_IPV6_ECN_UNMASK;
                            MEMCPY (&u2TempTwoByte, pOxmFld->au1Fld, OFC_TWO);
                            u2TempTwoByte = OSIX_NTOHS (u2TempTwoByte);
                            u2TwoByte = (UINT2) (u2TwoByte |
                                                 (UINT2) (u2TempTwoByte <<
                                                          (UINT2) OFC_FOUR));

                            u2TwoByte = OSIX_HTONS (u2TwoByte);
                            CRU_BUF_Copy_OverBufChain (pPkt,
                                                       (UINT1 *) &u2TwoByte,
                                                       u2Offset, OFC_TWO);
                        }
                        break;

                    case OFCXMT_OFB_IP_PROTO:
                        u4RetVal =
                            Ofc131ActsComputeHdrOffset (pPkt, OFC_IP_PROTO,
                                                        &u2ActType, &u2Offset);
                        if (OFC_FAILURE == u4RetVal)
                        {
                            OFC_TRC_FUNC ((OFC_ACTS_TRC,
                                           "[AS] SetField IP Protocol "
                                           "- No IP Header, FAILURE\n"));
                            return OFC_FAILURE;
                        }

                        if (u2ActType == OFC_L3_IP4)
                        {
                            CRU_BUF_Copy_OverBufChain (pPkt, pOxmFld->au1Fld,
                                                       (UINT4) ((UINT4) u2Offset
                                                                +
                                                                (UINT4)
                                                                OFC_IPV4_PROTO_OFFSET),
                                                       OFC_ONE);

                            /* Recalculate the Checksum */
                            u1OneByte = OFC_ZERO;
                            u2TwoByte = OFC_ZERO;

                            CRU_BUF_Copy_FromBufChain (pPkt, &u1OneByte,
                                                       u2Offset, OFC_ONE);
                            u1OneByte = (UINT1) ((UINT1) (u1OneByte &
                                                          (UINT1)
                                                          OFC_IPV4_IHL_UNMASK) *
                                                 (UINT1) OFC_IPV4_WORD_SIZE);
                            u2TwoByte =
                                Ofc131ActsComputeIPChecksum (pPkt, u1OneByte,
                                                             u2Offset);

                            CRU_BUF_Copy_OverBufChain (pPkt,
                                                       (UINT1 *) &u2TwoByte,
                                                       (UINT4) ((UINT4) u2Offset
                                                                +
                                                                (UINT4)
                                                                OFC_IPV4_CHKSUM_OFFSET),
                                                       OFC_TWO);
                        }
                        else
                        {
                            /* It is IPv6 */
                            CRU_BUF_Copy_OverBufChain (pPkt, pOxmFld->au1Fld,
                                                       (UINT4) ((UINT4) u2Offset
                                                                +
                                                                (UINT4)
                                                                OFC_IPV6_PROTO_OFFSET),
                                                       OFC_ONE);
                        }
                        break;

                    case OFCXMT_OFB_IPV4_SRC:
                        u4RetVal =
                            Ofc131ActsComputeHdrOffset (pPkt, OFC_IP_PROTO,
                                                        &u2ActType, &u2Offset);
                        if (OFC_FAILURE == u4RetVal)
                        {
                            OFC_TRC_FUNC ((OFC_ACTS_TRC,
                                           "[AS] SetField IPv4 Source "
                                           "- No IP Header, FAILURE\n"));
                            return OFC_FAILURE;
                        }

                        if (u2ActType == OFC_L3_IP4)
                        {
                            CRU_BUF_Copy_OverBufChain (pPkt, pOxmFld->au1Fld,
                                                       (UINT4) ((UINT4) u2Offset
                                                                +
                                                                OFC_IPV4_SRC_OFFSET),
                                                       OFC_IPV4_ADDR_LEN);
                            /* Recalculate the Checksum */
                            u1OneByte = OFC_ZERO;
                            u2TwoByte = OFC_ZERO;
                            CRU_BUF_Copy_FromBufChain (pPkt, &u1OneByte,
                                                       u2Offset, OFC_ONE);
                            u1OneByte =
                                (UINT1) ((UINT1)
                                         (u1OneByte & (UINT1)
                                          OFC_IPV4_IHL_UNMASK) *
                                         (UINT1) OFC_IPV4_WORD_SIZE);
                            u2TwoByte =
                                Ofc131ActsComputeIPChecksum (pPkt, u1OneByte,
                                                             u2Offset);
                            /* Copy the new checksum into the packet */
                            CRU_BUF_Copy_OverBufChain (pPkt,
                                                       (UINT1 *) &u2TwoByte,
                                                       (UINT4) ((UINT4) u2Offset
                                                                +
                                                                (UINT4)
                                                                OFC_IPV4_CHKSUM_OFFSET),
                                                       OFC_TWO);
                        }
                        else
                        {
                            /* It is IPv6 */
                            OFC_TRC_FUNC ((OFC_ACTS_TRC,
                                           "[AS] SetField IPv4 Source "
                                           "- No IPv4 Header, FAILURE\n"));
                            return OFC_FAILURE;
                        }
                        break;

                    case OFCXMT_OFB_IPV4_DST:
                        u4RetVal =
                            Ofc131ActsComputeHdrOffset (pPkt, OFC_IP_PROTO,
                                                        &u2ActType, &u2Offset);
                        if (OFC_FAILURE == u4RetVal)
                        {
                            OFC_TRC_FUNC ((OFC_ACTS_TRC,
                                           "[AS] SetField IPv4 Dest "
                                           "- No IP Header, FAILURE\n"));
                            return OFC_FAILURE;
                        }

                        if (u2ActType == OFC_L3_IP4)
                        {
                            CRU_BUF_Copy_OverBufChain (pPkt, pOxmFld->au1Fld,
                                                       (UINT4) ((UINT4) u2Offset
                                                                +
                                                                (UINT4)
                                                                OFC_IPV4_DST_OFFSET),
                                                       OFC_IPV4_ADDR_LEN);

                            /* Recalculate the Checksum */
                            u1OneByte = OFC_ZERO;
                            u2TwoByte = OFC_ZERO;
                            CRU_BUF_Copy_FromBufChain (pPkt, &u1OneByte,
                                                       u2Offset, OFC_ONE);
                            u1OneByte = (UINT1) ((UINT1) (u1OneByte &
                                                          (UINT1)
                                                          OFC_IPV4_IHL_UNMASK) *
                                                 (UINT1) OFC_IPV4_WORD_SIZE);
                            u2TwoByte =
                                Ofc131ActsComputeIPChecksum (pPkt, u1OneByte,
                                                             u2Offset);
                            /* Copy the new checksum into the packet */
                            CRU_BUF_Copy_OverBufChain (pPkt,
                                                       (UINT1 *) &u2TwoByte,
                                                       (UINT4) ((UINT4) u2Offset
                                                                +
                                                                (UINT4)
                                                                OFC_IPV4_CHKSUM_OFFSET),
                                                       OFC_TWO);
                        }
                        else
                        {
                            /* It is IPv6 */
                            OFC_TRC_FUNC ((OFC_ACTS_TRC,
                                           "[AS] SetField IPv4 Dest "
                                           "- No IPv4 Header, FAILURE\n"));
                            return OFC_FAILURE;
                        }
                        break;

                    case OFCXMT_OFB_TCP_SRC:
                        u4RetVal =
                            Ofc131ActsComputeHdrOffset (pPkt, OFC_L4PROTO_TCP,
                                                        NULL, &u2Offset);
                        if (OFC_FAILURE == u4RetVal)
                        {
                            OFC_TRC_FUNC ((OFC_ACTS_TRC,
                                           "[AS] SetField TCP Source -"
                                           " No TCP Header, FAILURE\n"));
                            return OFC_FAILURE;
                        }
                        CRU_BUF_Copy_OverBufChain (pPkt, pOxmFld->au1Fld,
                                                   u2Offset, OFC_L4_ADDR_LEN);
                        break;

                    case OFCXMT_OFB_TCP_DST:
                        u4RetVal =
                            Ofc131ActsComputeHdrOffset (pPkt, OFC_L4PROTO_TCP,
                                                        NULL, &u2Offset);
                        if (OFC_FAILURE == u4RetVal)
                        {
                            OFC_TRC_FUNC ((OFC_ACTS_TRC,
                                           "[AS] SetField TCP Dest -"
                                           " No TCP Header, FAILURE\n"));
                            return OFC_FAILURE;
                        }
                        CRU_BUF_Copy_OverBufChain (pPkt, pOxmFld->au1Fld,
                                                   (UINT4) ((UINT4) u2Offset +
                                                            (UINT4)
                                                            OFC_L4_DST_OFFSET),
                                                   OFC_L4_ADDR_LEN);
                        break;

                    case OFCXMT_OFB_UDP_SRC:
                        u4RetVal = Ofc131ActsComputeHdrOffset (pPkt,
                                                               OFC_L4PROTO_UDP,
                                                               NULL, &u2Offset);
                        if (OFC_FAILURE == u4RetVal)
                        {
                            OFC_TRC_FUNC ((OFC_ACTS_TRC,
                                           "[AS] SetField UDP Source -"
                                           " No UDP Header, FAILURE\n"));
                            return OFC_FAILURE;
                        }
                        CRU_BUF_Copy_OverBufChain (pPkt, pOxmFld->au1Fld,
                                                   u2Offset, OFC_L4_ADDR_LEN);
                        break;

                    case OFCXMT_OFB_UDP_DST:
                        u4RetVal = Ofc131ActsComputeHdrOffset (pPkt,
                                                               OFC_L4PROTO_UDP,
                                                               NULL, &u2Offset);
                        if (OFC_FAILURE == u4RetVal)
                        {
                            OFC_TRC_FUNC ((OFC_ACTS_TRC,
                                           "[AS] SetField UDP Dest -"
                                           " No UDP Header, FAILURE\n"));
                            return OFC_FAILURE;
                        }
                        CRU_BUF_Copy_OverBufChain (pPkt, pOxmFld->au1Fld,
                                                   (UINT4) ((UINT4) u2Offset +
                                                            (UINT4)
                                                            OFC_L4_DST_OFFSET),
                                                   OFC_L4_ADDR_LEN);
                        break;

                    case OFCXMT_OFB_SCTP_SRC:
                        u4RetVal = Ofc131ActsComputeHdrOffset (pPkt,
                                                               OFC_L4PROTO_SCTP,
                                                               NULL, &u2Offset);
                        if (OFC_FAILURE == u4RetVal)
                        {
                            OFC_TRC_FUNC ((OFC_ACTS_TRC,
                                           "[AS] SetField SCTP Source "
                                           "- No SCTP Header, FAILURE\n"));
                            return OFC_FAILURE;
                        }
                        CRU_BUF_Copy_OverBufChain (pPkt, pOxmFld->au1Fld,
                                                   u2Offset, OFC_L4_ADDR_LEN);
                        break;

                    case OFCXMT_OFB_SCTP_DST:
                        u4RetVal = Ofc131ActsComputeHdrOffset (pPkt,
                                                               OFC_L4PROTO_SCTP,
                                                               NULL, &u2Offset);
                        if (OFC_FAILURE == u4RetVal)
                        {
                            OFC_TRC_FUNC ((OFC_ACTS_TRC,
                                           "[AS] SetField SCTP Dest "
                                           "- No SCTP Header, FAILURE\n"));
                            return OFC_FAILURE;
                        }
                        CRU_BUF_Copy_OverBufChain (pPkt, pOxmFld->au1Fld,
                                                   (UINT4) ((UINT4) u2Offset +
                                                            (UINT4)
                                                            OFC_L4_DST_OFFSET),
                                                   OFC_L4_ADDR_LEN);
                        break;

                    case OFCXMT_OFB_ICMPV4_TYPE:
                        u4RetVal = Ofc131ActsComputeHdrOffset (pPkt,
                                                               OFC_L4PROTO_ICMP,
                                                               NULL, &u2Offset);
                        if (OFC_FAILURE == u4RetVal)
                        {
                            OFC_TRC_FUNC ((OFC_ACTS_TRC,
                                           "[AS] SetField ICMPv4 Type "
                                           "- No ICMPv4 Header, FAILURE\n"));
                            return OFC_FAILURE;
                        }
                        CRU_BUF_Copy_OverBufChain (pPkt, pOxmFld->au1Fld,
                                                   u2Offset, OFC_ONE);
                        break;

                    case OFCXMT_OFB_ICMPV4_CODE:
                        u4RetVal = Ofc131ActsComputeHdrOffset (pPkt,
                                                               OFC_L4PROTO_ICMP,
                                                               NULL, &u2Offset);
                        if (OFC_FAILURE == u4RetVal)
                        {
                            OFC_TRC_FUNC ((OFC_ACTS_TRC,
                                           "[AS] SetField ICMPv4 Code "
                                           "- No ICMPv4 Header, FAILURE\n"));
                            return OFC_FAILURE;
                        }
                        CRU_BUF_Copy_OverBufChain (pPkt, pOxmFld->au1Fld,
                                                   (UINT4) ((UINT4) u2Offset +
                                                            (UINT4)
                                                            OFC_ICMP4_CODE_OFFSET),
                                                   OFC_ONE);
                        break;

                    case OFCXMT_OFB_ARP_OP:
                        u4RetVal = Ofc131ActsComputeHdrOffset (pPkt, OFC_L3_ARP,
                                                               NULL, &u2Offset);
                        if (OFC_FAILURE == u4RetVal)
                        {
                            OFC_TRC_FUNC ((OFC_ACTS_TRC,
                                           "[AS] SetField ARP OpCode "
                                           "- No ARP Header, FAILURE\n"));
                            return OFC_FAILURE;
                        }
                        CRU_BUF_Copy_OverBufChain (pPkt, pOxmFld->au1Fld,
                                                   (UINT4) ((UINT4) u2Offset +
                                                            (UINT4)
                                                            OFC_ARP_OP_OFFSET),
                                                   OFC_ARP_OP_LEN);
                        break;

                    case OFCXMT_OFB_ARP_SPA:
                        u4RetVal = Ofc131ActsComputeHdrOffset (pPkt, OFC_L3_ARP,
                                                               NULL, &u2Offset);
                        if (OFC_FAILURE == u4RetVal)
                        {
                            OFC_TRC_FUNC ((OFC_ACTS_TRC,
                                           "[AS] SetField ARP Source IPv4 Address"
                                           " -  No ARP Header, FAILURE\n"));
                            return OFC_FAILURE;
                        }
                        CRU_BUF_Copy_OverBufChain (pPkt, pOxmFld->au1Fld,
                                                   (UINT4) ((UINT4) u2Offset +
                                                            (UINT4)
                                                            OFC_ARP_SPA_OFFSET),
                                                   OFC_ARP_PA_LEN);
                        break;

                    case OFCXMT_OFB_ARP_TPA:
                        u4RetVal = Ofc131ActsComputeHdrOffset (pPkt, OFC_L3_ARP,
                                                               NULL, &u2Offset);
                        if (OFC_FAILURE == u4RetVal)
                        {
                            OFC_TRC_FUNC ((OFC_ACTS_TRC,
                                           "[AS] SetField ARP Target IPv4 Address"
                                           " - No ARP Header, FAILURE\n"));
                            return OFC_FAILURE;
                        }
                        CRU_BUF_Copy_OverBufChain (pPkt, pOxmFld->au1Fld,
                                                   (UINT4) ((UINT4) u2Offset +
                                                            (UINT4)
                                                            OFC_ARP_TPA_OFFSET),
                                                   OFC_ARP_PA_LEN);
                        break;

                    case OFCXMT_OFB_ARP_SHA:
                        u4RetVal = Ofc131ActsComputeHdrOffset (pPkt, OFC_L3_ARP,
                                                               NULL, &u2Offset);
                        if (OFC_FAILURE == u4RetVal)
                        {
                            OFC_TRC_FUNC ((OFC_ACTS_TRC, "[AS] SetField ARP "
                                           "Source Hardware Address - "
                                           "No ARP Header, FAILURE\n"));
                            return OFC_FAILURE;
                        }
                        CRU_BUF_Copy_OverBufChain (pPkt, pOxmFld->au1Fld,
                                                   (UINT4) ((UINT4) u2Offset +
                                                            (UINT4)
                                                            OFC_ARP_SHA_OFFSET),
                                                   OFC_ARP_HA_LEN);
                        break;

                    case OFCXMT_OFB_ARP_THA:
                        u4RetVal = Ofc131ActsComputeHdrOffset (pPkt, OFC_L3_ARP,
                                                               NULL, &u2Offset);
                        if (OFC_FAILURE == u4RetVal)
                        {
                            OFC_TRC_FUNC ((OFC_ACTS_TRC, "[AS] SetField ARP "
                                           "Target Hardware Address - "
                                           "No ARP Header, FAILURE\n"));
                            return OFC_FAILURE;
                        }
                        CRU_BUF_Copy_OverBufChain (pPkt, pOxmFld->au1Fld,
                                                   (UINT4) ((UINT4) u2Offset +
                                                            (UINT4)
                                                            OFC_ARP_THA_OFFSET),
                                                   OFC_ARP_HA_LEN);
                        break;

                    case OFCXMT_OFB_IPV6_SRC:
                        u4RetVal =
                            Ofc131ActsComputeHdrOffset (pPkt, OFC_IP_PROTO,
                                                        &u2ActType, &u2Offset);
                        if (OFC_FAILURE == u4RetVal)
                        {
                            OFC_TRC_FUNC ((OFC_ACTS_TRC, "[AS] SetField "
                                           "IPv6 Source Address - "
                                           "No IP Header, FAILURE\n"));
                            return OFC_FAILURE;
                        }

                        if (u2ActType == OFC_L3_IP6)
                        {
                            CRU_BUF_Copy_OverBufChain (pPkt, pOxmFld->au1Fld,
                                                       (UINT4) ((UINT4) u2Offset
                                                                +
                                                                (UINT4)
                                                                OFC_IPV6_SRC_OFFSET),
                                                       OFC_IPV6_ADDR_LEN);
                        }
                        else
                        {
                            /* It is IPv4 */
                            OFC_TRC_FUNC ((OFC_ACTS_TRC, "[AS] SetField "
                                           "IPv6 Source Address - "
                                           "No IPv6 Header, FAILURE\n"));
                            return OFC_FAILURE;
                        }
                        break;

                    case OFCXMT_OFB_IPV6_DST:
                        u4RetVal =
                            Ofc131ActsComputeHdrOffset (pPkt, OFC_IP_PROTO,
                                                        &u2ActType, &u2Offset);
                        if (OFC_FAILURE == u4RetVal)
                        {
                            OFC_TRC_FUNC ((OFC_ACTS_TRC, "[AS] SetField "
                                           "IPv6 Destination Address - "
                                           "No IP Header, FAILURE\n"));
                            return OFC_FAILURE;
                        }

                        if (u2ActType == OFC_L3_IP6)
                        {
                            CRU_BUF_Copy_OverBufChain (pPkt, pOxmFld->au1Fld,
                                                       (UINT4) ((UINT4) u2Offset
                                                                +
                                                                (UINT4)
                                                                OFC_IPV6_DST_OFFSET),
                                                       OFC_IPV6_ADDR_LEN);
                        }
                        else
                        {
                            /* It is IPv4 */
                            OFC_TRC_FUNC ((OFC_ACTS_TRC, "[AS] SetField "
                                           "IPv6 Destination Address - "
                                           "No IPv6 Header, FAILURE\n"));
                            return OFC_FAILURE;
                        }
                        break;

                    case OFCXMT_OFB_IPV6_FLABEL:
                        u4RetVal =
                            Ofc131ActsComputeHdrOffset (pPkt, OFC_IP_PROTO,
                                                        &u2ActType, &u2Offset);
                        if (OFC_FAILURE == u4RetVal)
                        {
                            OFC_TRC_FUNC ((OFC_ACTS_TRC, "[AS] SetField "
                                           "IPv6 Flow Label - "
                                           "No IP Header, FAILURE\n"));
                            return OFC_FAILURE;
                        }

                        if (u2ActType == OFC_L3_IP6)
                        {
                            /* Copy the existing Flow Label */
                            CRU_BUF_Copy_FromBufChain (pPkt,
                                                       (UINT1 *) &u4FourByte,
                                                       u2Offset, OFC_FOUR);
                            u4FourByte = OSIX_NTOHL (u4FourByte);
                            u4FourByte = u4FourByte & OFC_IPV6_FLABEL_UNMASK;
                            MEMCPY (&u4TempFourByte, pOxmFld->au1Fld, OFC_FOUR);
                            u4TempFourByte = OSIX_NTOHL (u4TempFourByte);
                            u4FourByte = u4FourByte | u4TempFourByte;

                            u4FourByte = OSIX_NTOHL (u4FourByte);
                            CRU_BUF_Copy_OverBufChain (pPkt,
                                                       (UINT1 *) &u4FourByte,
                                                       u2Offset, OFC_FOUR);
                        }
                        else
                        {
                            /* It is IPv4 */
                            OFC_TRC_FUNC ((OFC_ACTS_TRC, "[AS] SetField "
                                           "IPv6 Flow Label - "
                                           "No IPv6 Header, FAILURE\n"));
                            return OFC_FAILURE;
                        }
                        break;

                    case OFCXMT_OFB_ICMPV6_TYPE:
                        u4RetVal = Ofc131ActsComputeHdrOffset (pPkt,
                                                               OFC_L4PROTO_ICMP6,
                                                               NULL, &u2Offset);
                        if (OFC_FAILURE == u4RetVal)
                        {
                            OFC_TRC_FUNC ((OFC_ACTS_TRC, "[AS] SetField "
                                           "ICMPv6 Type - "
                                           "No ICMPv6 Header, FAILURE\n"));
                            return OFC_FAILURE;
                        }
                        CRU_BUF_Copy_OverBufChain (pPkt, pOxmFld->au1Fld,
                                                   u2Offset, OFC_ONE);
                        break;

                    case OFCXMT_OFB_ICMPV6_CODE:
                        u4RetVal = Ofc131ActsComputeHdrOffset (pPkt,
                                                               OFC_L4PROTO_ICMP6,
                                                               NULL, &u2Offset);
                        if (OFC_FAILURE == u4RetVal)
                        {
                            OFC_TRC_FUNC ((OFC_ACTS_TRC, "[AS] SetField "
                                           "ICMPv6 Code - "
                                           "No ICMPv6 Header, FAILURE\n"));
                            return OFC_FAILURE;
                        }
                        CRU_BUF_Copy_OverBufChain (pPkt, pOxmFld->au1Fld,
                                                   (UINT4) ((UINT4) u2Offset +
                                                            (UINT4)
                                                            OFC_ICMP6_CODE_OFFSET),
                                                   OFC_ONE);
                        break;

                    case OFCXMT_OFB_IPV6_ND_TARGET:
                        u4RetVal = Ofc131ActsComputeHdrOffset (pPkt, OFC_L3_ND,
                                                               NULL, &u2Offset);
                        if (OFC_FAILURE == u4RetVal)
                        {
                            OFC_TRC_FUNC ((OFC_ACTS_TRC, "[AS] SetField "
                                           "ND Target - "
                                           "No ND Header, FAILURE\n"));
                            return OFC_FAILURE;
                        }

                        /* Fetch the ND Type */
                        CRU_BUF_Copy_FromBufChain (pPkt, &u1OneByte,
                                                   u2Offset, OFC_ONE);
                        /* Fetch The ND Target Offset */
                        switch (u1OneByte)
                        {
                            case NEIGHBOR_SOLICITATION:
                                break;

                            case NEIGHBOR_ADVERTISEMENT:
                                break;

                            case REDIRECT:
                                break;

                            default:
                                /* Invalid Type */
                                OFC_TRC_FUNC ((OFC_ACTS_TRC, "[AS] SetField "
                                               "ND Target - "
                                               "Invalid Type, FAILURE\n"));
                                return OFC_FAILURE;
                        }

                        u2Offset =
                            (UINT2) (u2Offset + (UINT2) OFC_ND_TARGET_OFFSET);
                        /* Copy the Target IPv6 Address */
                        CRU_BUF_Copy_OverBufChain (pPkt, pOxmFld->au1Fld,
                                                   u2Offset, OFC_SIXTEEN);
                        break;

                    case OFCXMT_OFB_IPV6_ND_SLL:
                        u4RetVal = Ofc131ActsComputeHdrOffset (pPkt, OFC_L3_ND,
                                                               &u2ActType,
                                                               &u2Offset);
                        if (OFC_FAILURE == u4RetVal)
                        {
                            OFC_TRC_FUNC ((OFC_ACTS_TRC, "[AS] SetField "
                                           "ND SLL - "
                                           "No ND Header, FAILURE\n"));
                            return OFC_FAILURE;
                        }
                        /* Fetch the ND Type */
                        CRU_BUF_Copy_FromBufChain (pPkt, &u1OneByte,
                                                   u2Offset, OFC_ONE);
                        /* Fetch The ND Option Offset */
                        switch (u1OneByte)
                        {
                            case ROUTER_SOLICITATION:
                                u2TempOffset = OFC_ND_RS_OPTION_OFFSET;
                                break;

                            case ROUTER_ADVERTISEMENT:
                                u2TempOffset = OFC_ND_RA_OPTION_OFFSET;
                                break;

                            case NEIGHBOR_SOLICITATION:
                                u2TempOffset = OFC_ND_NS_OPTION_OFFSET;
                                break;

                            default:
                                /* Invalid ND Type */
                                OFC_TRC_FUNC ((OFC_ACTS_TRC, "[AS] SetField "
                                               "ND SLL - "
                                               "Invalid ND Type, FAILURE\n"));
                                return OFC_FAILURE;
                        }
                        u2Offset = (UINT2) (u2Offset + u2TempOffset);

                        /* Copy the ND Option */
                        CRU_BUF_Copy_FromBufChain (pPkt, &u1OneByte,
                                                   u2Offset, OFC_ONE);
                        /* Copy the ND Option Length, in case any */
                        CRU_BUF_Copy_FromBufChain (pPkt, &u1TempOneByte,
                                                   (UINT4) ((UINT4) u2Offset +
                                                            (UINT4) OFC_ONE),
                                                   OFC_ONE);

                        if (ND_SLL == u1OneByte)
                        {
                            CRU_BUF_Copy_OverBufChain (pPkt, pOxmFld->au1Fld,
                                                       (UINT4) ((UINT4) u2Offset
                                                                +
                                                                (UINT4)
                                                                OFC_TWO),
                                                       (UINT4) ((UINT4)
                                                                u1TempOneByte -
                                                                (UINT4)
                                                                OFC_TWO));
                        }
                        else
                        {
                            /* No ND SLL present */
                            OFC_TRC_FUNC ((OFC_ACTS_TRC, "[AS] SetField "
                                           "ND SLL - "
                                           "No ND SLL present, FAILURE\n"));
                            return OFC_FAILURE;
                        }
                        break;

                    case OFCXMT_OFB_IPV6_ND_TLL:
                        u4RetVal = Ofc131ActsComputeHdrOffset (pPkt, OFC_L3_ND,
                                                               NULL, &u2Offset);
                        if (OFC_FAILURE == u4RetVal)
                        {
                            OFC_TRC_FUNC ((OFC_ACTS_TRC, "[AS] SetField "
                                           "ND TLL - "
                                           "No ND Header, FAILURE\n"));
                            return OFC_FAILURE;
                        }
                        /* Fetch the ND Type */
                        CRU_BUF_Copy_FromBufChain (pPkt, &u1OneByte,
                                                   u2Offset, OFC_ONE);
                        /* Fetch The ND Option Offset */
                        switch (u1OneByte)
                        {
                            case NEIGHBOR_ADVERTISEMENT:
                                u2TempOffset = OFC_ND_NA_OPTION_OFFSET;
                                break;

                            case REDIRECT:
                                u2TempOffset = OFC_ND_RD_OPTION_OFFSET;
                                break;

                            default:
                                /* Invalid ND Type */
                                OFC_TRC_FUNC ((OFC_ACTS_TRC, "[AS] SetField "
                                               "ND TLL - "
                                               "Invalid ND Type, FAILURE\n"));
                                return OFC_FAILURE;
                        }
                        u2Offset = (UINT2) (u2Offset + u2TempOffset);

                        /* Copy the ND Option */
                        CRU_BUF_Copy_FromBufChain (pPkt, &u1OneByte,
                                                   u2Offset, OFC_ONE);
                        /* Copy the ND Option Length, in case any */
                        CRU_BUF_Copy_FromBufChain (pPkt, &u1TempOneByte,
                                                   (UINT4) ((UINT4) u2Offset +
                                                            (UINT4) OFC_ONE),
                                                   OFC_ONE);

                        if (ND_TLL == u1OneByte)
                        {
                            CRU_BUF_Copy_OverBufChain (pPkt, pOxmFld->au1Fld,
                                                       (UINT4) ((UINT4) u2Offset
                                                                +
                                                                (UINT4)
                                                                OFC_TWO),
                                                       (UINT4) ((UINT4)
                                                                u1TempOneByte -
                                                                (UINT4)
                                                                OFC_TWO));
                        }
                        else
                        {
                            /* No ND SLL present */
                            OFC_TRC_FUNC ((OFC_ACTS_TRC, "[AS] SetField "
                                           "ND TLL - "
                                           "No ND SLL present, FAILURE\n"));
                            return OFC_FAILURE;
                        }
                        break;

                    case OFCXMT_OFB_MPLS_LABEL:
                        u4RetVal =
                            Ofc131ActsComputeHdrOffset (pPkt, OFC_MPLS_PROTO,
                                                        &u2ActType, &u2Offset);
                        if (OFC_FAILURE == u4RetVal)
                        {
                            OFC_TRC_FUNC ((OFC_ACTS_TRC, "[AS] SetField "
                                           "MPLS Label - "
                                           "No MPLS Header, FAILURE\n"));
                            return OFC_FAILURE;
                        }
                        /* Copy the existing TTL */
                        CRU_BUF_Copy_FromBufChain (pPkt, (UINT1 *) &u4FourByte,
                                                   u2Offset, OFC_FOUR);

                        u4FourByte = OSIX_NTOHL (u4FourByte);
                        u4FourByte = u4FourByte & OFC_MPLS_LABEL_UNMASK;
                        MEMCPY (&u4TempFourByte, pOxmFld->au1Fld, OFC_FOUR);
                        u4TempFourByte = OSIX_NTOHL (u4TempFourByte);
                        u4FourByte =
                            u4FourByte | (u4TempFourByte << OFC_TWELVE);
                        u4FourByte = OSIX_NTOHL (u4FourByte);
                        CRU_BUF_Copy_OverBufChain (pPkt, (UINT1 *) &u4FourByte,
                                                   u2Offset, OFC_FOUR);
                        break;

                    case OFCXMT_OFB_MPLS_TC:
                        u4RetVal =
                            Ofc131ActsComputeHdrOffset (pPkt, OFC_MPLS_PROTO,
                                                        &u2ActType, &u2Offset);
                        if (OFC_FAILURE == u4RetVal)
                        {
                            OFC_TRC_FUNC ((OFC_ACTS_TRC, "[AS] SetField "
                                           "MPLS TC - "
                                           "No MPLS Header, FAILURE\n"));
                            return OFC_FAILURE;
                        }
                        /* Copy the existing BoS */
                        CRU_BUF_Copy_FromBufChain (pPkt, &u1OneByte,
                                                   (UINT4) ((UINT4) u2Offset +
                                                            (UINT4)
                                                            OFC_MPLS_LABEL16_TC_BOS_OFFSET),
                                                   OFC_ONE);
                        u1OneByte = u1OneByte & OFC_MPLS_TC_UNMASK;
                        u1TempOneByte = pOxmFld->au1Fld[OFC_ZERO];
                        u1OneByte = (UINT1) (u1OneByte |
                                             (UINT1) (u1TempOneByte << (UINT1)
                                                      OFC_ONE));

                        CRU_BUF_Copy_OverBufChain (pPkt, &u1OneByte,
                                                   (UINT4) ((UINT4) u2Offset +
                                                            (UINT4)
                                                            OFC_MPLS_LABEL16_TC_BOS_OFFSET),
                                                   OFC_ONE);
                        break;

                    case OFCXMT_OFB_MPLS_BOS:
                        u4RetVal =
                            Ofc131ActsComputeHdrOffset (pPkt, OFC_MPLS_PROTO,
                                                        &u2ActType, &u2Offset);
                        if (OFC_FAILURE == u4RetVal)
                        {
                            OFC_TRC_FUNC ((OFC_ACTS_TRC, "[AS] SetField "
                                           "MPLS BoS - "
                                           "No MPLS Header, FAILURE\n"));
                            return OFC_FAILURE;
                        }
                        /* Copy the existing CoS */
                        CRU_BUF_Copy_FromBufChain (pPkt, &u1OneByte,
                                                   (UINT4) ((UINT4) u2Offset +
                                                            (UINT4)
                                                            OFC_MPLS_LABEL16_TC_BOS_OFFSET),
                                                   OFC_ONE);
                        u1OneByte = u1OneByte & OFC_MPLS_BOS_UNMASK;
                        u1TempOneByte = pOxmFld->au1Fld[OFC_ZERO];
                        u1OneByte = u1OneByte | u1TempOneByte;

                        CRU_BUF_Copy_OverBufChain (pPkt, &u1OneByte,
                                                   (UINT4) ((UINT4) u2Offset +
                                                            (UINT4)
                                                            OFC_MPLS_LABEL16_TC_BOS_OFFSET),
                                                   OFC_ONE);
                        break;

                    case OFCXMT_OFB_PBB_ISID:
                        u4RetVal =
                            Ofc131ActsComputeHdrOffset (pPkt, OFC_L2EXT_PBB,
                                                        NULL, &u2Offset);
                        if (OFC_FAILURE == u4RetVal)
                        {
                            OFC_TRC_FUNC ((OFC_ACTS_TRC, "[AS] SetField "
                                           "PBB ISID - "
                                           "No PBB Header, FAILURE\n"));
                            return OFC_FAILURE;
                        }
                        /* Copy the existing Flow Lable, CoS and TTL */
                        CRU_BUF_Copy_FromBufChain (pPkt, (UINT1 *) &u4FourByte,
                                                   (UINT4) ((UINT4) u2Offset +
                                                            (UINT4)
                                                            OFC_PBB_ISID_OFFSET),
                                                   OFC_PBB_ISID_SIZE);
                        u4FourByte = OSIX_NTOHL (u4FourByte);
                        u4FourByte = u4FourByte & OFC_PBB_ISID_UNMASK;
                        MEMCPY (&u4TempFourByte, pOxmFld->au1Fld, OFC_FOUR);
                        u4TempFourByte = OSIX_NTOHL (u4TempFourByte);
                        u4FourByte = u4FourByte | u4TempFourByte;
                        u4FourByte = OSIX_NTOHL (u4FourByte);
                        CRU_BUF_Copy_OverBufChain (pPkt, (UINT1 *) &u4FourByte,
                                                   (UINT4) ((UINT4) u2Offset +
                                                            (UINT4)
                                                            OFC_PBB_ISID_OFFSET),
                                                   OFC_PBB_ISID_SIZE);
                        break;

                    case OFCXMT_OFB_TUNNEL_ID:
                        break;

                    case OFCXMT_OFB_IPV6_EXTHDR:
                        break;

                    default:
                        /* Unsupported Set Field */
                        OFC_TRC_FUNC ((OFC_ACTS_TRC,
                                       "[AS] Unsupported SetField\n"));
                        return OFC_FAILURE;
                }
            }
            u2SetField = (UINT2) (u2SetField + (UINT2) OFC_ONE);
        }
    }

    /* Action - Set the given queue for the port */
    if (gActionSet.au1ActsBitMap[OFCAT_SET_QUEUE])
    {
        /* No Supporting as of D2 */
    }

    /* Action - Send the Packet to the Group Table */
    if (gActionSet.au1ActsBitMap[OFCAT_GROUP])
    {
        pActGroup = (tOfcActGroup *) & (gActionSet.Acts[OFC_ASO_GROUP]);
        MEMCPY (pi4GroupId, &(pActGroup->u4GroupId), OFC_FOUR);
    }

    /* Action - Send the Packet onto the Output port */
    if (gActionSet.au1ActsBitMap[OFCAT_OUTPUT])
    {
        /* Add the Output port in the SLL. SLL is added to include 
         * multiple output ports in the action */
        pActOutput = (tOfcActOutput *) & (gActionSet.Acts[OFC_ASO_OUTPUT]);
        pActOutPort = (tOfcActOutputPort *) MemAllocMemBlk
            (OFC_OUT_PORT_POOLID);
        if (pActOutPort == NULL)
        {
            OFC131_TRC_FUNC ((OFC_PKT_TRC,
                              "MemAllocMemBlk for OutPort Failed\n"));
            return OSIX_FAILURE;
        }
        MEMSET (pActOutPort, OFC_ZERO, sizeof (tOfcActOutputPort));
        pActOutPort->OfcOutput.u4Port = pActOutput->u4Port;
        pActOutPort->OfcOutput.u2MaxLen = pActOutput->u2MaxLen;
        TMO_SLL_Add (pOutput, &(pActOutPort->Node));
    }
    OFC_TRC_FUNC ((OFC_FN_EXIT, "FUNC:Ofc131ActsApplyActionSet SUCCESS\n"));
    return OFC_SUCCESS;
}

/******************************************************************************
 * Function    : tOfc131ActsUpdateActionSet 
 * Description : This routine parses the actions stored in the Acts SLL
 *               part of the Write Action Instruction and updates the
 *               Global Action Set, which will be applied on the packet
 *               at last.
 * Input       : pActList  - Action list for Write Actions.
 * Output      : ActionSet - Global Action Set, is updated. 
 * Returns     : OFC_SUCCESS or OFC_FAILURE
 ******************************************************************************/
PRIVATE UINT4
Ofc131ActsUpdateActionSet (tOfcSll * pActList)
{
    tOfcActs           *pOfcActs = NULL;
    tOfcSll            *pSetFldList = NULL;
    tOfcActSetFld      *pActsSetFld = NULL;
    tOfcSetFld         *pSetFld = NULL;
    UINT2               u2Type = OFC_ZERO;

    OFC_TRC_FUNC ((OFC_FN_ENTRY, "FUNC:Ofc131ActsApplyActionSet\n"));
    TMO_SLL_Scan (pActList, pOfcActs, tOfcActs *)
    {
        switch (pOfcActs->u2Type)
        {
            case OFCAT_COPY_TTL_OUT:
            case OFCAT_COPY_TTL_IN:
            case OFCAT_DEC_MPLS_TTL:
            case OFCAT_DEC_NW_TTL:
            case OFCAT_POP_VLAN:
            case OFCAT_POP_PBB:
                /* Nothing to do, except setting the bitmap */
                break;

            case OFCAT_OUTPUT:
                MEMCPY (&(gActionSet.Acts[OFC_ASO_OUTPUT]),
                        &(pOfcActs->Acts), sizeof (unOfcInstr));
                break;

            case OFCAT_SET_MPLS_TTL:
                MEMCPY (&(gActionSet.Acts[OFC_ASO_SET_MPLS_TTL]),
                        &(pOfcActs->Acts), sizeof (unOfcInstr));
                break;

            case OFCAT_PUSH_VLAN:
                MEMCPY (&(gActionSet.Acts[OFC_ASO_PUSH_VLAN]),
                        &(pOfcActs->Acts), sizeof (unOfcInstr));
                break;

            case OFCAT_PUSH_MPLS:
                MEMCPY (&(gActionSet.Acts[OFC_ASO_PUSH_MPLS]),
                        &(pOfcActs->Acts), sizeof (unOfcInstr));
                break;

            case OFCAT_POP_MPLS:
                MEMCPY (&(gActionSet.Acts[OFC_ASO_POP_MPLS]),
                        &(pOfcActs->Acts), sizeof (unOfcInstr));
                break;

            case OFCAT_SET_QUEUE:
                MEMCPY (&(gActionSet.Acts[OFC_ASO_SET_QUEUE]),
                        &(pOfcActs->Acts), sizeof (unOfcInstr));
                break;

            case OFCAT_GROUP:
                MEMCPY (&(gActionSet.Acts[OFC_ASO_GROUP]),
                        &(pOfcActs->Acts), sizeof (unOfcInstr));
                break;

            case OFCAT_SET_NW_TTL:
                MEMCPY (&(gActionSet.Acts[OFC_ASO_SET_NW_TTL]),
                        &(pOfcActs->Acts), sizeof (unOfcInstr));
                break;

            case OFCAT_SET_FIELD:
                pActsSetFld = (tOfcActSetFld *) & (pOfcActs->Acts);
                pSetFldList = (tOfcSll *) & (pActsSetFld->SetFldList);

                TMO_SLL_Scan (pSetFldList, pSetFld, tOfcSetFld *)
                {
                    /* Copy the SetFieds to Action Set */
                    if (pSetFld->u2Type < OFC_MAX_SETFIELDS)
                    {
                        u2Type = pSetFld->u2Type;
                        MEMCPY (&(gActionSet.SetFld[u2Type]),
                                &(pSetFld->SetFld), OFC_SIXTEEN);

                    }
                    else
                    {
                        /* Unsupported SetField */
                        OFC_TRC_FUNC ((OFC_ACTS_TRC, "Unsupported SetField,"
                                       " return FAILURE\n"));
                        return OFC_FAILURE;
                    }
                    /* Set the respective SetFields BitMap */
                    gActionSet.au1SetFldBitMap[pSetFld->u2Type] = OFC_ONE;
                }
                break;

            case OFCAT_PUSH_PBB:
                MEMCPY (&(gActionSet.Acts[OFC_ASO_PUSH_PBB]),
                        &(pOfcActs->Acts), sizeof (unOfcInstr));
                break;

            case OFCAT_EXPERIMENTER:
            default:
                /* Unsupported Action */
                OFC_TRC_FUNC ((OFC_ACTS_TRC, "Unsupported Action,"
                               " return FAILURE\n"));
                return OFC_FAILURE;
        }
        /* Set the respective Actions BitMap */
        gActionSet.au1ActsBitMap[pOfcActs->u2Type] = OFC_ONE;
    }
    OFC_TRC_FUNC ((OFC_FN_EXIT, "FUNC:Ofc131ActsApplyActionSet SUCCESS\n"));
    return OFC_SUCCESS;
}

/******************************************************************************
 * Function    :  OfcActsParseInstructions 
 * Description :  This routine processes the instructions which are part of the
 *                matching flow entry on the matched packet.
 * Input       :  pPkt        - Pointer to received packet CRU buffer.
 *                pInstrList  - Pointer to Instruction SLL List
 * Output      :  pu1TableId  - Pointer to Next Flow Table, in case any.
 *                pi4GroupId  - Pointer to Group Entry, in case any.
 *                pOutput     - Pointer to Output Port SLL, in case any. 
 * Returns     :  OFC_SUCCESS or OFC_FAILURE
 ******************************************************************************/
PRIVATE UINT4
Ofc131ActsProcesInstructions (tCRU_BUF_CHAIN_HEADER * pPkt,
                              tOfcSll * pInstrList, UINT1 *pu1TableId,
                              INT4 *pi4GroupId, tOfcSll * pOutput)
{
    tOfcInstr          *pOfcInstr = NULL;
    tOfcInstrWrMetadata *pMetadata = NULL;
    tOfcInstrGotoTable *pGotoTable = NULL;
    tOfcInstrActions   *pActions = NULL;
    UINT4               u4RetVal = OFC_ZERO;

    OFC_TRC_FUNC ((OFC_FN_ENTRY, "FUNC:Ofc131ActsProcesInstructions\n"));
    TMO_SLL_Scan (pInstrList, pOfcInstr, tOfcInstr *)
    {
        /*
         * Instructions has to be processed according to the order
         * the cases are written in switch cases.
         * Packet Tx/Rx Module will make sure the Instructions in the
         * Flow Entry are Ordered in the required manner. 
         */
        switch (pOfcInstr->u2Type)
        {
            case OFCIT_METER:
                /* no doing anything */
                break;

            case OFCIT_APPLY_ACTIONS:
                pActions = (tOfcInstrActions *) & (pOfcInstr->Instr);
                u4RetVal = Ofc131ActsApplyActionList (pPkt,
                                                      &(pActions->ActList),
                                                      pi4GroupId, pOutput);
                if (u4RetVal != OFC_SUCCESS)
                {
                    OFC_TRC_FUNC ((OFC_ACTS_TRC, "apply actions failed,"
                                   " return FAILURE\n"));
                    return OFC_FAILURE;
                }
                break;

            case OFCIT_CLEAR_ACTIONS:
                /* Clear the Action Set */
                MEMSET (&gActionSet, OFC_ZERO, sizeof (tOfcActionSet));
                break;

            case OFCIT_WRITE_ACTIONS:
                /* Update the Action Set */
                pActions = (tOfcInstrActions *) & (pOfcInstr->Instr);
                Ofc131ActsUpdateActionSet (&(pActions->ActList));
                break;

            case OFCIT_WRITE_METADATA:
                /* Update the MetaData */
                pMetadata = (tOfcInstrWrMetadata *) & (pOfcInstr->Instr);
                MEMCPY (gau1Metadata, &(pMetadata->u8Metadata), OFC_EIGHT);
                break;

            case OFCIT_GOTO_TABLE:
                pGotoTable = (tOfcInstrGotoTable *) & (pOfcInstr->Instr);
                MEMCPY (pu1TableId, &(pGotoTable->u1TableId), OFC_ONE);
                break;

            case OFCIT_EXPERIMENTER:

            default:
                /* Unsupported Instruction */
                OFC_TRC_FUNC ((OFC_ACTS_TRC, "Unsupported Instruction,"
                               " return FAILURE\n"));
                return OFC_FAILURE;
        }
    }
    OFC_TRC_FUNC ((OFC_FN_EXIT, "FUNC:Ofc131ActsProcesInstructions SUCCESS\n"));
    return OFC_SUCCESS;
}

/******************************************************************************
 * Function    :  Ofc131ActsSendPktIn
 * Description :  This routine sends the Pkt-In message to controller
 * Input       :  u4IfIndex - Interface Index.
 *                pPkt      - Pointer to the received packet buffer.
 *                pOfcFsofcControllerConnEntry - Pointer to Cntlr Entry.
 * Output      :  None
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE
 *****************************************************************************/
INT4
Ofc131ActsSendPktIn (UINT4 u4IfIndex, UINT4 u4LogicPort,
                     UINT4 u4MsgType, FS_UINT8 u8Cookie,
                     UINT1 u1TableId, tOfp131Match * pOfcMatch,
                     tCRU_BUF_CHAIN_HEADER * pPkt,
                     tOfcFsofcControllerConnEntry *
                     pOfcFsofcControllerConnEntry)
{
    tOfp131PktIn       *pOfp131PktIn = NULL;
    tOxmTlv            *pOxmTlv = NULL;
    UINT4               u4PktSize = OFC_ZERO;
    UINT2               u2Offset = OFC_ZERO;
    UINT2               u2TotalSize = OFC_ZERO;
    UINT2               u2Length = OFC_ZERO;
    UINT4               u4Port = OFC_ZERO;

    OFC_TRC_FUNC ((OFC_FN_ENTRY, "FUNC:Ofc131ActsSendPktIn\n"));

    /* Get the packet size */
    u4PktSize = CRU_BUF_Get_ChainValidByteCount (pPkt);

    /* Allocate memory from mem-pool */
    pOfp131PktIn = (tOfp131PktIn *) MemAllocMemBlk (OFC_PKTIN_POOLID);
    if (pOfp131PktIn == NULL)
    {
        OFC_TRC_FUNC ((OFC_ACTS_TRC, "MemAlloc for PktIn Failed"
                       " return FAILURE\n"));
        return OFC_FAILURE;
    }
    MEMSET (pOfp131PktIn, OFC_ZERO, OFC_MAX_PKTIN_SIZE);

    u2TotalSize =
        (UINT2) ((UINT2) u4PktSize + (UINT2) OSIX_NTOHS (pOfcMatch->u2Length) +
                 (UINT2) OFC_SIXTEEN);
    if (u2TotalSize > OFC_MAX_PKTIN_SIZE)
    {
        OFC_TRC_FUNC ((OFC_ACTS_TRC, "Total PktIn Size more than MemPool Size,"
                       " return FAILURE\n"));
        MemReleaseMemBlock (OFC_PKTIN_POOLID, (UINT1 *) pOfp131PktIn);
        return OFC_FAILURE;
    }

    pOfp131PktIn->u4BufId = OFC_NO_BUFFER;    /* Send full packet */
    pOfp131PktIn->u2TotLen = (UINT2) u4PktSize;
    pOfp131PktIn->u1Reason = (UINT1) u4MsgType;
    pOfp131PktIn->u1TableId = u1TableId;
    pOfp131PktIn->u8Cookie.u4Lo = u8Cookie.u4Lo;
    pOfp131PktIn->u8Cookie.u4Hi = u8Cookie.u4Hi;

    /* No Match scenario - Fill Standard Match Fields */
    if (OSIX_NTOHS (pOfcMatch->u2Length) == OFC_ZERO)
    {
        pOfcMatch->u2Type = OSIX_HTONS (OFPMT_OXM);
        u2Length = OFC_FOUR;

        /* Filling for In-Port */
        pOxmTlv = (tOxmTlv *) ((VOID *) (((UINT1 *) pOfcMatch) + OFC_FOUR));
        pOxmTlv->u2Class = OSIX_HTONS (OFPXMC_OPENFLOW_BASIC);
        pOxmTlv->u1Field = OFCXMT_OFB_IN_PORT << OFC_ONE;
        pOxmTlv->u1Len = OFC_FOUR;

        if (u4LogicPort == OFC_ZERO)
        {
            u4LogicPort = u4IfIndex;
        }
        u4Port = OSIX_HTONL (u4LogicPort);
        MEMCPY (pOxmTlv->au1Value, &u4Port, OFC_FOUR);

        u2Length = (UINT2) (u2Length + (UINT2) OFC_EIGHT);
        pOxmTlv = (tOxmTlv *) ((VOID *) (((UINT1 *) pOxmTlv) + OFC_EIGHT));

        /* Filling for Physical Port, incase logical port exists */
        if (u4LogicPort != u4IfIndex)
        {
            pOxmTlv->u2Class = OSIX_HTONS (OFPXMC_OPENFLOW_BASIC);
            pOxmTlv->u1Field = OFCXMT_OFB_IN_PHY_PORT << OFC_ONE;
            pOxmTlv->u1Len = OFC_FOUR;
            u4Port = OSIX_HTONL (u4IfIndex);
            MEMCPY (pOxmTlv->au1Value, &u4Port, OFC_FOUR);

            u2Length = (UINT2) (u2Length + (UINT2) OFC_EIGHT);
            pOxmTlv = (tOxmTlv *) ((VOID *) (((UINT1 *) pOxmTlv) + OFC_EIGHT));
        }

        /* Filling for Ethernet Source Address */
        pOxmTlv->u2Class = OSIX_HTONS (OFPXMC_OPENFLOW_BASIC);
        pOxmTlv->u1Field = OFCXMT_OFB_ETH_SRC << OFC_ONE;
        pOxmTlv->u1Len = OFC_SIX;
        CRU_BUF_Copy_FromBufChain (pPkt, pOxmTlv->au1Value, OFC_ETH_SRC_OFFSET,
                                   OFC_ETH_ADDR_LEN);

        u2Length = (UINT2) (u2Length + OFC_TEN);
        pOxmTlv = (tOxmTlv *) ((VOID *) (((UINT1 *) pOxmTlv) + OFC_TEN));

        /* Filling for Ethernet Destination Address */
        pOxmTlv->u2Class = OSIX_HTONS (OFPXMC_OPENFLOW_BASIC);
        pOxmTlv->u1Field = OFCXMT_OFB_ETH_DST << OFC_ONE;
        pOxmTlv->u1Len = OFC_SIX;
        CRU_BUF_Copy_FromBufChain (pPkt, pOxmTlv->au1Value, OFC_ZERO,
                                   OFC_ETH_ADDR_LEN);

        u2Length = (UINT2) (u2Length + OFC_TEN);
    }
    else
    {
        /* Copy the existing Flow Match */
        u2Length = OSIX_NTOHS (pOfcMatch->u2Length);

        pOfcMatch->u2Type = OSIX_HTONS (OFPMT_OXM);

        /* Filling for In-Port */
        pOxmTlv = (tOxmTlv *) ((VOID *) (((UINT1 *) pOfcMatch) + u2Length));
        pOxmTlv->u2Class = OSIX_HTONS (OFPXMC_OPENFLOW_BASIC);
        pOxmTlv->u1Field = OFCXMT_OFB_IN_PORT << OFC_ONE;
        pOxmTlv->u1Len = OFC_FOUR;

        if (u4LogicPort == OFC_ZERO)
        {
            u4LogicPort = u4IfIndex;
        }
        u4Port = OSIX_HTONL (u4LogicPort);
        MEMCPY (pOxmTlv->au1Value, &u4Port, OFC_FOUR);

        u2Length = (UINT2) (u2Length + (UINT2) OFC_EIGHT);
    }
    MEMCPY ((((UINT1 *) pOfp131PktIn) + OFC_SIXTEEN), pOfcMatch, u2Length);
    pOfp131PktIn->OfpMatch.u2Length = OSIX_HTONS (u2Length);

    /* Add padding if required */
    if (u2Length % OFC_EIGHT)
    {
        u2Length = (UINT2) (u2Length + OFC_EIGHT - (u2Length % OFC_EIGHT));
    }

    /* Calculate the offset to start copying the actual packet */
    u2Offset = (UINT2) (u2Length + OFC_EIGHTEEN);    /* includes 2 Byte padding */

    /* Copy the packet */
    if (CRU_BUF_Copy_FromBufChain (pPkt, (((UINT1 *) pOfp131PktIn) + u2Offset),
                                   OFC_ZERO, u4PktSize) == CRU_FAILURE)
    {
        OFC_TRC_FUNC ((OFC_ACTS_TRC, "CRU Copy Failed, return FAILURE\n"));
        MemReleaseMemBlock (OFC_PKTIN_POOLID, (UINT1 *) pOfp131PktIn);
        return OFC_FAILURE;
    }

    /* 
     * Send packet-in.
     * Variable Length Start from Match.
     */
    Ofc131PktPktInSend (pOfp131PktIn,
                        pOfcFsofcControllerConnEntry,
                        (u4PktSize + u2Length + OFC_TWO));
    MemReleaseMemBlock (OFC_PKTIN_POOLID, (UINT1 *) pOfp131PktIn);
    OFC_TRC_FUNC ((OFC_FN_EXIT, "FUNC:Ofc131ActsSendPktIn SUCCESS\n"));
    return OFC_SUCCESS;
}

/******************************************************************************
 * Function    :  Ofc131ActsPipelineProcess
 * Description :  This routine performs the pipeline processing on the received
 *                data packet. If there is a matching flow entry in the flow
 *                table, the corresponding actions are executed. If no match
 *                is found, the packet is sent to the controller or dropped.
 * Input       :  u4IfIndex     - Interface Index.
 *                u4ContextId   - Context ID.
 *                pPkt          - Pointer to the received packet buffer.
 * Output      :  None
 * Returns     :  OFC_SUCCESS or OFC_FAILURE
 ******************************************************************************/
INT4
Ofc131ActsPipelineProcess (UINT4 u4IfIndex, UINT4 u4ContextId,
                           tCRU_BUF_CHAIN_HEADER * pPkt)
{
    tOfcFsofcCfgEntry   CfgEntry;
    tOfcFsofcIfEntry    IfEntry;
    tOfcFsofcGroupEntry GroupEntry;
    tOfcSll             KeyMatchList;
    FS_UINT8            u8FlowMatchBM;
    FS_UINT8            u8OrigCounter;
    FS_UINT8            u8TempCounter;
    tOfcGroupBucket    *pOfcGroupBucket = NULL;
    tOfcGroupBucket    *pBucket = NULL;
    tOfcGroupBucket    *pNextBucket = NULL;
    tOfcGroupBucket    *pOfcPrevGroupBucket = NULL;
    tOfcFsofcFlowTable *pFlowTable = NULL;
    tOfcFsofcGroupEntry *pGroupEntry = NULL;
    tOfcFsofcFlowEntry *pFlowEntry = NULL;
    tOfcFsofcFlowEntry *pTblMissFlowEntry = NULL;
    tOfcQueMsg         *pQueMsg = NULL;
    tOfcSll            *pActList = NULL;
    tOfcSll             ActOutPortList;
    tOfcFsofcIfEntry   *pOfcFsofcIfEntry = NULL;
    tOfcActOutputPort  *pActOutput = NULL;
    UINT1              *pu1Alias = NULL;
    INT4                i4GroupId = OFC_INVALID_GROUP;
    UINT4               u4OutputPort = OFC131_INVALID_PORT;
    UINT1               u1NumBits = OFC_ZERO;
    UINT1               u1TableId = OFC_ZERO;
    UINT1               u1FlMiTabId = OFC_ZERO;    /* Flow Miss Table Id */
    UINT4               u4RetVal = OFC_ZERO;
    UINT1               u1OperStatus = OFC_ZERO;
    UINT4               u4PktSize = OFC_ZERO;
    UINT4               u4NoRecv = OFC_ZERO;
    UINT4               u4NoPktIn = OFC_ZERO;
    UINT4               u4Index = OFC_ZERO;
    UINT4               u4Indx = u4IfIndex;    /* physical or logical */
    UINT4               u4Length = OFC_ZERO;
    tOfcFsofcCfgEntry  *pOfcFsofcCfgEntry = NULL;
    tCRU_BUF_CHAIN_HEADER *pDupBuf = NULL;
#ifdef NPAPI_WANTED
#ifndef OPENFLOW_TCAM
    tOfcHwInfo          OfcHwInfo;
    tOfcExactFlowMatch  ExactFlowMatch;
    tOfcExactFlowEntry *pExactFlowEntry = NULL;
    UINT1               au1PktHdrs[OFC_MAX_PROTO_HDRS_LEN];
#endif
#endif

    OFC_TRC_FUNC ((OFC_FN_ENTRY, "FUNC:Ofc131ActsPipelineProcess\n"));

    MEMSET (&IfEntry, OFC_ZERO, sizeof (IfEntry));
    MEMSET (&CfgEntry, OFC_ZERO, sizeof (CfgEntry));
    MEMSET (&GroupEntry, OFC_ZERO, sizeof (GroupEntry));
    MEMSET (&gCfaIfInfo, OFC_ZERO, sizeof (tCfaIfInfo));
    MEMSET (&u8FlowMatchBM, OFC_ZERO, sizeof (u8FlowMatchBM));
    MEMSET (&u8OrigCounter, OFC_ZERO, sizeof (u8OrigCounter));
    MEMSET (&u8TempCounter, OFC_ZERO, sizeof (u8TempCounter));
    MEMSET (au1IfName, OFC_ZERO, CFA_MAX_IFALIAS_LENGTH);
    MEMSET (au1ExactMatch, OFC_ZERO, MAX_OFC_EXACT_LENGTH);
    MEMSET (gau1GrpPktBuf, OFC_ZERO, OFC131_MAX_PKT_LEN);
    TMO_SLL_Init (&ActOutPortList);
#ifdef NPAPI_WANTED
#ifndef OPENFLOW_TCAM
    MEMSET (au1PktHdrs, OFC_ZERO, OFC_MAX_PROTO_HDRS_LEN);
    MEMSET (&OfcHwInfo, OFC_ZERO, sizeof (tOfcHwInfo));
#endif
#endif

    IfEntry.MibObject.u4FsofcContextId = u4ContextId;
    IfEntry.MibObject.i4FsofcIfIndex = (INT4) u4IfIndex;
    if (OfcGetAllFsofcIfTable (&IfEntry) == OFC_SUCCESS)
    {
        /* Check if it is part of Logical interface (Vlan) */
        if (IfEntry.u4VlanId != OFC_ZERO)
        {
            pu1Alias = au1IfName;
            STRCPY (au1IfName, OPENFLOW_VLAN_ALIAS_PREFIX);
            SPRINTF ((CHR1 *) pu1Alias, "%s%u", au1IfName, IfEntry.u4VlanId);

            if (OfcGetInterfaceIndexFromName (pu1Alias, &u4Index)
                == OSIX_FAILURE)
            {
                CfaIfSetInDiscard (u4IfIndex);
                return OFC_FAILURE;
            }
            IfEntry.MibObject.u4FsofcContextId = u4ContextId;
            IfEntry.MibObject.i4FsofcIfIndex = (INT4) u4Index;
            pOfcFsofcIfEntry = RBTreeGet (gOfcGlobals.OfcGlbMib.FsofcIfTable,
                                          (tRBElem *) & (IfEntry));

            if (pOfcFsofcIfEntry != NULL)
            {
                /* Update Vlan Statistics */
                pOfcFsofcIfEntry->MibObject.u4FsofcVlanInFrames++;

                /* This logical index has to be used for flow match */
                u4Indx = u4Index;

            }
            else
            {
                CfaIfSetInDiscard (u4IfIndex);
                return OFC_FAILURE;
            }
        }
    }
    else if (!((u4IfIndex == OFPP_CONTROLLER) || (u4IfIndex == OFPP_LOCAL)))
    {
        /*
         * only controller and local can be used as input ports
         * in the logical port range. Discard the data packets coming
         * on any other ports.
         */
        CfaIfSetInDiscard (u4IfIndex);
        return OFC_FAILURE;
    }

    /* Get the IfInfo for the incoming port, will be accessing the counters */
    if (CfaGetIfInfo (u4IfIndex, &gCfaIfInfo) != OSIX_SUCCESS)
    {
        if (!((u4IfIndex == OFPP_CONTROLLER) || (u4IfIndex == OFPP_LOCAL)))
        {
            CfaIfSetInDiscard (u4IfIndex);
            OFC_TRC_FUNC ((OFC_ACTS_TRC, "Input IF Not found,"
                           "return FAILURE\n"));
            return OFC_FAILURE;
        }
    }

    MEMSET (&gCfaIfInfo, OFC_ZERO, sizeof (tCfaIfInfo));

    /* Get the IfInfo for the physical port or logical port if present */
    if (CfaGetIfInfo (u4Indx, &gCfaIfInfo) != OSIX_SUCCESS)
    {
        if (!((u4IfIndex == OFPP_CONTROLLER) || (u4IfIndex == OFPP_LOCAL)))
        {
            CfaIfSetInDiscard (u4IfIndex);
            OFC_TRC_FUNC ((OFC_ACTS_TRC, "Input IF Not found,"
                           "return FAILURE\n"));
            return OFC_FAILURE;
        }
    }
    /* Get the IF operstatus */
    u1OperStatus = gCfaIfInfo.u1IfOperStatus;

    if (u1OperStatus == CFA_IF_DOWN)
    {
        if (!((u4IfIndex == OFPP_CONTROLLER) || (u4IfIndex == OFPP_LOCAL)))
        {
            CfaIfSetInDiscard (u4IfIndex);
            OFC_TRC_FUNC ((OFC_ACTS_TRC, "Input IF Down, return FAILURE\n"));
            CRU_BUF_Release_MsgBufChain (pPkt, FALSE);
            return OFC_FAILURE;
        }
    }

    /* Get the port config flags */
    MEMSET (&IfEntry, OFC_ZERO, sizeof (IfEntry));
    IfEntry.MibObject.u4FsofcContextId = u4ContextId;
    IfEntry.MibObject.i4FsofcIfIndex = (INT4) u4Indx;
    if (OfcGetAllFsofcIfTable (&IfEntry) == OFC_SUCCESS)
    {
        u4NoRecv = IfEntry.u4Config >> OFC_TWO;
        u4NoPktIn = IfEntry.u4Config >> OFC_SIX;

        if (u4NoRecv & OFC_ONE)
        {
            /* Drop the packet(s) received by this port */
            CfaIfSetInDiscard (u4IfIndex);
            OFC_TRC_FUNC ((OFC_ACTS_TRC, "Drop the Packet, return FAILURE\n"));
            CRU_BUF_Release_MsgBufChain (pPkt, FALSE);
            return OFC_FAILURE;
        }
    }

    /* Get the packet size */
    u4PktSize = CRU_BUF_Get_ChainValidByteCount (pPkt);

    /* Start updating the Action Set */
    MEMSET (&gActionSet, OFC_ZERO, sizeof (tOfcActionSet));

    /* 
     * If the ChipSet is non-tcam, the flow module will not insert
     * the flow entry. Pipeline processing has to retrieve exact match
     * from the incoming packet and need to install the flow entry with
     * the resultant action set.
     */
#ifdef NPAPI_WANTED
#ifndef OPENFLOW_TCAM
    /* Allocate for the Dll Node */
    pExactFlowEntry = (tOfcExactFlowEntry *)
        MemAllocMemBlk (OFC_NON_TCAM_POOLID);

    if (pExactFlowEntry == NULL)
    {
        CfaIfSetInDiscard (u4IfIndex);
        OFC_TRC_FUNC ((OFC_ACTS_TRC, "MemAlloc of Non-Tcam Failed,"
                       " return FAILURE\n"));
        CRU_BUF_Release_MsgBufChain (pPkt, FALSE);
        return OFC_FAILURE;
    }

    MEMSET (pExactFlowEntry, OFC_ZERO, sizeof (tOfcExactFlowEntry));

    /* Only copying the headers for extracting the flow match */
    if (CRU_BUF_Copy_FromBufChain (pPkt, au1PktHdrs,
                                   OFC_ZERO,
                                   OFC_MAX_PROTO_HDRS_LEN) == CRU_FAILURE)
    {
        OFC_TRC_FUNC ((OFC_ACTS_TRC, "CRU Copy Failed, return FAILURE\n"));
        CfaIfSetInDiscard (u4IfIndex);
        CRU_BUF_Release_MsgBufChain (pPkt, FALSE);
        /* Release the mem-pool */
        MemReleaseMemBlock (OFC_NON_TCAM_POOLID, (UINT1 *) pExactFlowEntry);
        return OFC_FAILURE;
    }

    /* Extract exact-flow */
    MEMSET (&ExactFlowMatch, OFC_ZERO, sizeof (tOfcExactFlowMatch));
    if (Ofc131ActsFlowExtract (au1PktHdrs, u4Indx,
                               &ExactFlowMatch) == OFC_FAILURE)
    {
        OFC_TRC_FUNC ((OFC_ACTS_TRC, "Flow Extract Failed, "
                       "return FAILURE\n"));
        CfaIfSetInDiscard (u4IfIndex);
        CRU_BUF_Release_MsgBufChain (pPkt, FALSE);
        /* Release the mem-pool */
        MemReleaseMemBlock (OFC_NON_TCAM_POOLID, (UINT1 *) pExactFlowEntry);
        return OFC_FAILURE;
    }
#endif
#endif

    pOfcFsofcCfgEntry = OfcGetFsofcCfgEntry (u4ContextId);
    if (pOfcFsofcCfgEntry == NULL)
    {
        OFC_TRC_FUNC ((OFC_ACTS_TRC, "\r\nOfc131PipelineProccessing: "
                       "Get CFG Entry Failed\n"));
        CfaIfSetInDiscard (u4IfIndex);
        CRU_BUF_Release_MsgBufChain (pPkt, FALSE);
        return OFC_FAILURE;
    }

    MEMSET (&u8TempCounter, OFC_ZERO, sizeof (FS_UINT8));
    /* Pipeline processing always starts with Table 0 */
    while (u1TableId < pOfcFsofcCfgEntry->u2NumOfTables)
    {
        /* Fetch the Flow Table */
        pFlowTable = OfcGetFlowTableEntry (u4ContextId, u1TableId);
        if (pFlowTable == NULL)
        {
            OFC_TRC_FUNC ((OFC_ACTS_TRC, "No Flow Table, " "return FAILURE\n"));
            CfaIfSetInDiscard (u4IfIndex);
            CRU_BUF_Release_MsgBufChain (pPkt, FALSE);
            return OFC_FAILURE;    /* else what can we do */
        }

        /* Update the table statistics */
        FSAP_U8_INC (&(pFlowTable->u8LookupCount));

        /* Get the Flow Match BM and prepare the Key accordingly */
        MEMSET (&u8FlowMatchBM, OFC_ZERO, sizeof (u8FlowMatchBM));
        u8FlowMatchBM = pFlowTable->u8FlowMatchBitMap;
        /* Compute number of 1s in bitmap */
        Ofc131ActsComputeNumBitsSet (u8FlowMatchBM, &u1NumBits);

        /* 
         * If there is no logical port, physical port should be treated as 
         * OFPXMT_OFB_IN_PORT in matching. So updating logical port with physical
         * port here.
         */
        if (u4Index == OFC_ZERO)
        {
            u4Index = u4IfIndex;
        }

        /* Prepare the key */
        MEMSET (au1ExactMatch, OFC_ZERO, MAX_OFC_EXACT_LENGTH);
        Ofc131ActsPrepareKey (u8FlowMatchBM, u1NumBits, pPkt, &KeyMatchList,
                              au1ExactMatch, u4IfIndex, u4Index);
        /* Retrieve the Flow Entry */
        pFlowEntry = (tOfcFsofcFlowEntry *) (VOID *)
            Ofc131FlowLookupForPipeLine (pOfcFsofcCfgEntry, &KeyMatchList,
                                         au1ExactMatch, u1TableId);

        /* Free Match List */
        Ofc131FlowMatchListFree (OFC_MINUS_ONE, &KeyMatchList);

        if (pFlowEntry != NULL && pFlowEntry->u1FlowMatchCount != OFC_ZERO)
        {
            /* Update the Flow Table statistics */
            FSAP_U8_INC (&(pFlowTable->u8MatchedCount));

            /* Update the Flow Entry statistics */
            /* Packet Count */
            u8OrigCounter.u4Lo =
                pFlowEntry->MibObject.u8FsofcFlowPacketCount.lsn;
            u8OrigCounter.u4Hi =
                pFlowEntry->MibObject.u8FsofcFlowPacketCount.msn;
            FSAP_U8_INC (&u8OrigCounter);
            pFlowEntry->MibObject.u8FsofcFlowPacketCount.lsn =
                u8OrigCounter.u4Lo;
            pFlowEntry->MibObject.u8FsofcFlowPacketCount.msn =
                u8OrigCounter.u4Hi;

            /* Byte Count */
            u8OrigCounter.u4Lo = pFlowEntry->MibObject.u8FsofcFlowByteCount.lsn;
            u8OrigCounter.u4Hi = pFlowEntry->MibObject.u8FsofcFlowByteCount.msn;
            u8TempCounter.u4Lo = u4PktSize;
            u8TempCounter.u4Hi = OFC_ZERO;
            FSAP_U8_ADD (&u8TempCounter, &u8OrigCounter, &u8TempCounter);
            pFlowEntry->MibObject.u8FsofcFlowByteCount.lsn = u8TempCounter.u4Lo;
            pFlowEntry->MibObject.u8FsofcFlowByteCount.msn = u8TempCounter.u4Hi;

            /*
             * u1TableId will be changed as part of the instruction processing
             * accordingly, else below line will help to break the loop.
             */
            u1FlMiTabId = u1TableId;    /* Will be used in Pkt-In */
            u1TableId = (UINT1) pOfcFsofcCfgEntry->u2NumOfTables;

            /* Process Instructions */
            u4RetVal = Ofc131ActsProcesInstructions (pPkt,
                                                     &(pFlowEntry->InstrList),
                                                     &u1TableId, &i4GroupId,
                                                     &ActOutPortList);
            if (OFC_FAILURE == u4RetVal)
            {
                /* Process Instruction Failed */
                OFC_TRC_FUNC ((OFC_ACTS_TRC, "Process Instructions FAILURE\n"));
                CfaIfSetInDiscard (u4IfIndex);
                CRU_BUF_Release_MsgBufChain (pPkt, FALSE);
                OfcActOutputPortListFree (&ActOutPortList);
                return OFC_FAILURE;
            }
        }
        else
        {
            if ((pFlowEntry != NULL)
                && (pFlowEntry->u1FlowMatchCount == OFC_ZERO))
            {
                pTblMissFlowEntry = pFlowEntry;
            }

            /* Can we get the table miss from the same table */
            if (u1TableId < ((UINT1) pOfcFsofcCfgEntry->u2NumOfTables))
            {
                u1TableId++;
                continue;
            }
            pFlowEntry = pTblMissFlowEntry;
            u1TableId = (UINT1) pOfcFsofcCfgEntry->u2NumOfTables;
            u1FlMiTabId = u1TableId;
        }
    }

    /* Apply Action Set after the last Flow Entry */
    u4RetVal = Ofc131ActsApplyActionSet (pPkt, &i4GroupId, &ActOutPortList);
    if (OFC_FAILURE == u4RetVal)
    {
        /* Apply Action Set Failed */
        OFC_TRC_FUNC ((OFC_ACTS_TRC, "Apply Action Set FAILURE\n"));
        CfaIfSetInDiscard (u4IfIndex);
        CRU_BUF_Release_MsgBufChain (pPkt, FALSE);
        OfcActOutputPortListFree (&ActOutPortList);
        return OFC_FAILURE;
    }

#ifdef NPAPI_WANTED
#ifndef OPENFLOW_TCAM
    /* Trigger npapi to add the flow in non-tcam chipset */
    if (OfcFsNpHwConfigOfcInfo (&OfcHwInfo) != FNP_SUCCESS)
    {
        /* Unable to Add flow */
        CfaIfSetInDiscard (u4IfIndex);
        CRU_BUF_Release_MsgBufChain (pPkt, FALSE);
        /* Release the mem-pool */
        MemReleaseMemBlock (OFC_NON_TCAM_POOLID, (UINT1 *) pExactFlowEntry);
        return OFC_FAILURE;
    }

    /* Start the timer and add the exact flow entry to the DLL */
    MEMSET (pExactFlowEntry, OFC_ZERO, sizeof (tOfcExactFlowEntry));
    MEMCPY (&(pExactFlowEntry->OfcFlowMatch), &ExactFlowMatch,
            sizeof (tOfcExactFlowMatch));
    if (pExactFlowEntry->u2IdleTimeout != OFC_ZERO)
    {
        OfcTmrStartTmr (&(pExactFlowEntry->OfcIdleTimer),
                        OFC_NONTCAM_IDLE_TMR, pExactFlowEntry->u2IdleTimeout);
    }
    TMO_DLL_Add (&(pOfcFsofcCfgEntry->OfcExactFlowList),
                 (tOfcDllNode *) pExactFlowEntry);
#endif
#endif

    /* If the Group is set, send it to group for further processing */
    if ((i4GroupId != OFC_INVALID_GROUP))
    {
        while ((UINT4) i4GroupId < OFCG_MAX)
        {
            /* Get the group entry */
            GroupEntry.MibObject.u4FsofcContextId = u4ContextId;
            GroupEntry.MibObject.u4FsofcGroupIndex = (UINT4) i4GroupId;

            pGroupEntry = RBTreeGet (gOfcGlobals.OfcGlbMib.FsofcGroupTable,
                                     (tRBElem *) & (GroupEntry));
            if (pGroupEntry == NULL)
            {
                OFC_TRC_FUNC ((OFC_ACTS_TRC, "No Group entry corresponding to"
                               " the given i4GroupId" "return FAILURE\n"));
                CfaIfSetInDiscard (u4IfIndex);
                CRU_BUF_Release_MsgBufChain (pPkt, FALSE);
                OfcActOutputPortListFree (&ActOutPortList);
                return OFC_FAILURE;
            }
            /* Update the Group Statistics */
            /* Increment the packet count */
            u8OrigCounter.u4Lo =
                pGroupEntry->MibObject.u8FsofcGroupPacketCount.lsn;
            u8OrigCounter.u4Hi =
                pGroupEntry->MibObject.u8FsofcGroupPacketCount.msn;
            FSAP_U8_INC (&u8OrigCounter);
            pGroupEntry->MibObject.u8FsofcGroupPacketCount.lsn =
                u8OrigCounter.u4Lo;
            pGroupEntry->MibObject.u8FsofcGroupPacketCount.msn =
                u8OrigCounter.u4Hi;

            /* Increment the byte count */
            u8OrigCounter.u4Lo =
                pGroupEntry->MibObject.u8FsofcGroupByteCount.lsn;
            u8OrigCounter.u4Hi =
                pGroupEntry->MibObject.u8FsofcGroupByteCount.msn;
            u8TempCounter.u4Lo = u4PktSize;
            u8TempCounter.u4Hi = OFC_ZERO;
            FSAP_U8_ADD (&u8TempCounter, &u8OrigCounter, &u8TempCounter);

            pGroupEntry->MibObject.u8FsofcGroupByteCount.lsn =
                u8TempCounter.u4Lo;
            pGroupEntry->MibObject.u8FsofcGroupByteCount.msn =
                u8TempCounter.u4Hi;
            /*
             * i4GroupId will be changed as part of the group processing
             * accordingly, else below line will help to break the loop.
             */

            i4GroupId = OFCG_MAX;

            /* Apply Actions List in Group */
            if (pGroupEntry->MibObject.i4FsofcGroupType != OFCGT_SELECT)
            {
                pOfcGroupBucket = (tOfcGroupBucket *) TMO_SLL_First
                    (&(pGroupEntry->BucketList));
                if (pOfcGroupBucket == NULL)
                {
                    OFC_TRC_FUNC ((OFC_ACTS_TRC, "No Bucket in Group entry"
                                   "return FAILURE\n"));
                    CfaIfSetInDiscard (u4IfIndex);
                    CRU_BUF_Release_MsgBufChain (pPkt, FALSE);
                    OfcActOutputPortListFree (&ActOutPortList);
                    return OFC_FAILURE;
                }
                pActList = &(pOfcGroupBucket->ActList);
                u4RetVal = Ofc131ActsApplyActionList (pPkt, pActList,
                                                      &i4GroupId,
                                                      &ActOutPortList);
            }

            if (pGroupEntry->MibObject.i4FsofcGroupType == OFCGT_ALL)
            {
                while (pOfcGroupBucket != NULL)
                {
                    u4Length = CRU_BUF_Get_ChainValidByteCount (pPkt);
                    if (CRU_BUF_Copy_FromBufChain (pPkt, gau1GrpPktBuf,
                                                   OFC_ZERO,
                                                   u4Length) == CRU_FAILURE)
                    {
                        OFC131_TRC_FUNC ((OFC_PKT_TRC,
                                          "CRU Copy Failed, "
                                          "return FAILURE\n"));
                        CRU_BUF_Release_MsgBufChain (pPkt, FALSE);
                        OfcActOutputPortListFree (&ActOutPortList);
                        return OSIX_FAILURE;
                    }
                    pOfcPrevGroupBucket = pOfcGroupBucket;
                    pOfcGroupBucket = (tOfcGroupBucket *) TMO_SLL_Next
                        (&(pGroupEntry->BucketList),
                         (tTMO_SLL_NODE *) pOfcPrevGroupBucket);
                    if (pOfcGroupBucket != NULL)
                    {
                        pActList = &(pOfcGroupBucket->ActList);
                        u4RetVal = Ofc131ActsApplyActionList (pPkt, pActList,
                                                              &i4GroupId,
                                                              &ActOutPortList);
                        if (OFC_FAILURE == u4RetVal)
                        {
                            /* Apply Action List in Group Failed */
                            OFC_TRC_FUNC ((OFC_ACTS_TRC,
                                           "Apply action in group FAILURE\n"));
                            CfaIfSetInDiscard (u4IfIndex);
                            CRU_BUF_Release_MsgBufChain (pPkt, FALSE);
                            OfcActOutputPortListFree (&ActOutPortList);
                            return OFC_FAILURE;
                        }

                        /*
                         * Only normal interfaces are handled for Handling
                         * Group Type - ALL
                         * Only One level of ALL - bucket list is handled
                         * i.e no support of recursive ALL Group type
                         */
                        TMO_SLL_Scan (&ActOutPortList, pActOutput,
                                      tOfcActOutputPort *)
                        {
                            u4OutputPort = pActOutput->OfcOutput.u4Port;
                            if ((u4OutputPort != OFC131_INVALID_PORT)
                                && (u4OutputPort != OFPP_CONTROLLER)
                                && (u4OutputPort != OFPP_IN_PORT)
                                && (u4OutputPort != OFC131_INVALID_PORT)
                                && (u4OutputPort != OFPP_ALL)
                                && (u4OutputPort != OFPP_NORMAL)
                                && (u4OutputPort != OFPP_LOCAL)
                                && (u4OutputPort != OFPP_FLOOD))
                            {
                                Ofc131ActsSendPktOnPort (u4OutputPort,
                                                         u4ContextId, pPkt);
                            }
                            else
                            {
                                OFC_TRC_FUNC ((OFC_ACTS_TRC,
                                               "Unsupported Port Type For Group Type ALL\n"));
                                CfaIfSetInDiscard (u4IfIndex);
                                CRU_BUF_Release_MsgBufChain (pPkt, FALSE);
                                OfcActOutputPortListFree (&ActOutPortList);
                                return OFC_FAILURE;
                            }
                        }
                    }
                    CRU_BUF_Copy_OverBufChain (pPkt, gau1GrpPktBuf,
                                               OFC_ZERO, u4Length);
                }
                CRU_BUF_Release_MsgBufChain (pPkt, FALSE);
                OFC_TRC_FUNC ((OFC_FN_EXIT,
                               "FUNC:Ofc131ActsPipelineProcess SUCCESS\n"));
                return OFC_SUCCESS;
            }
            else if (pGroupEntry->MibObject.i4FsofcGroupType == OFCGT_SELECT)
            {
                TMO_SLL_Scan (&(pGroupEntry->BucketList), pBucket,
                              tOfcGroupBucket *)
                {
                    if (pBucket->u1SelectFlag == OFC_ONE)
                    {
                        pActList = &(pBucket->ActList);
                        u4RetVal = Ofc131ActsApplyActionList (pPkt, pActList,
                                                              &i4GroupId,
                                                              &ActOutPortList);
                        if (OFC_FAILURE == u4RetVal)
                        {
                            /* Apply Action List in Group Failed */
                            OFC_TRC_FUNC ((OFC_ACTS_TRC,
                                           "Apply action in group FAILURE\n"));
                            CfaIfSetInDiscard (u4IfIndex);
                            CRU_BUF_Release_MsgBufChain (pPkt, FALSE);
                            OfcActOutputPortListFree (&ActOutPortList);
                            return OFC_FAILURE;
                        }

                        pBucket->u1SelectFlag = OFC_ZERO;
                        pNextBucket = (tOfcGroupBucket *) TMO_SLL_Next
                            (&(pGroupEntry->BucketList),
                             (tTMO_SLL_NODE *) pBucket);
                        if (pNextBucket == NULL)
                        {
                            pNextBucket = (tOfcGroupBucket *) TMO_SLL_First
                                (&(pGroupEntry->BucketList));
                        }
                        pNextBucket->u1SelectFlag = OFC_ONE;
                        break;
                    }
                }
            }
        }
    }

    /* If OutputPort is set, send the packet to specified port 
       Only Valid Output Ports will be in SLL. */
    if (TMO_SLL_Count (&ActOutPortList) != OFC_ZERO)
    {
        /* Scan through the SLL and send the data packet on all the ports */
        TMO_SLL_Scan (&ActOutPortList, pActOutput, tOfcActOutputPort *)
        {
            u4OutputPort = pActOutput->OfcOutput.u4Port;
            if ((u4OutputPort == OFPP_CONTROLLER) ||
                ((u4OutputPort == OFPP_IN_PORT)
                 && (u4IfIndex == OFPP_CONTROLLER)))
            {
                /*
                 * If the No Packet-In msg flag is set on the input port,
                 * drop the packet.
                 */
                if (u4NoPktIn & OFC_ONE)
                {
                    CfaIfSetInDiscard (u4IfIndex);
                    OFC_TRC_FUNC ((OFC_ACTS_TRC, "Drop the Packet, "
                                   "return FAILURE\n"));
                    CRU_BUF_Release_MsgBufChain (pPkt, FALSE);
                    OfcActOutputPortListFree (&ActOutPortList);
                    return OFC_FAILURE;
                }
                pDupBuf = CRU_BUF_Duplicate_BufChain (pPkt);

                if (pDupBuf == NULL)
                {
                    OFC131_TRC_FUNC ((OFC_PKT_TRC,
                                      "Duplicate CRU BufFAILURE\n"));
                    CRU_BUF_Release_MsgBufChain (pPkt, FALSE);
                    OfcActOutputPortListFree (&ActOutPortList);
                    return OFC_FAILURE;
                }

                pQueMsg = (tOfcQueMsg *) MemAllocMemBlk (OFC_QUEUE_POOLID);

                if (pQueMsg == NULL)
                {
                    /* MemPool Allocation failed */
                    OFC_TRC_FUNC ((OFC_ACTS_TRC, "MemPool Allocated Failed, "
                                   "return FAILURE\n"));
                    CfaIfSetInDiscard (u4IfIndex);
                    CRU_BUF_Release_MsgBufChain (pPkt, FALSE);
                    CRU_BUF_Release_MsgBufChain (pDupBuf, FALSE);
                    OfcActOutputPortListFree (&ActOutPortList);
                    return OFC_FAILURE;
                }
                MEMSET (pQueMsg, OFC_ZERO, sizeof (tOfcQueMsg));
                pQueMsg->u4MsgType = OFCR_ACTION;
                pQueMsg->u4ContextId = u4ContextId;
                pQueMsg->u4IfIndex = u4IfIndex;
                pQueMsg->u4Index = u4Index;
                pQueMsg->pBuf = pDupBuf;
                if (pFlowEntry != NULL)
                {
                    pQueMsg->u1TableId = (UINT1) (pFlowEntry->u4TableIndex);
                    MEMCPY (&(pQueMsg->u8Cookie), &(pFlowEntry->u8Cookie),
                            OFC_EIGHT);
                }
                else
                {
                    pQueMsg->u1TableId = u1FlMiTabId;
                    pQueMsg->u8Cookie.u4Lo = OFC_UINT4_ALL_MASK;
                    pQueMsg->u8Cookie.u4Hi = OFC_UINT4_ALL_MASK;
                }
                /* Allocate from Flow Match Mempool */
                pQueMsg->pOfcMatch = (tOfp131Match *) MemAllocMemBlk
                    (OFC_FLOW_MATCH_TLV_POOLID);
                if (pQueMsg->pOfcMatch == NULL)
                {
                    /* MemPool Allocation failed */
                    OFC_TRC_FUNC ((OFC_ACTS_TRC, "MemPool Allocated Failed, "
                                   "return FAILURE\n"));
                    CfaIfSetInDiscard (u4IfIndex);
                    CRU_BUF_Release_MsgBufChain (pPkt, FALSE);
                    CRU_BUF_Release_MsgBufChain (pDupBuf, FALSE);
                    MemReleaseMemBlock (OFC_QUEUE_POOLID, (UINT1 *) pQueMsg);
                    OfcActOutputPortListFree (&ActOutPortList);
                    return OFC_FAILURE;
                }
                MEMSET (pQueMsg->pOfcMatch, OFC_ZERO,
                        OFC_MAX_FLOW_MATCH_TLV_SIZE);

                /* */
                if (pFlowEntry != NULL)
                {
                    Ofc131PktMatchStructToTlv (&pFlowEntry->MatchList,
                                               pFlowEntry->u1FlowMatchCount,
                                               pQueMsg->pOfcMatch);
                }

                /* Post The Message to Queue */
                if (OfcQueSendMsg (pQueMsg) == OFC_FAILURE)
                {
                    OFC_TRC_FUNC ((OFC_ACTS_TRC, "Posting to Queue Failed, "
                                   "return FAILURE\n"));
                    /* Just return, buffers are already released */
                    CfaIfSetInDiscard (u4IfIndex);
                    OfcActOutputPortListFree (&ActOutPortList);
                    return OFC_FAILURE;
                }
            }
            else if (u4OutputPort == OFPP_ALL)
            {
                pOfcFsofcIfEntry = (tOfcFsofcIfEntry *)
                    RBTreeGetFirst (gOfcGlobals.OfcGlbMib.FsofcIfTable);

                if (pOfcFsofcIfEntry != NULL)
                {
                    do
                    {
                        /* Send to all ports except ingress port */
                        if ((pOfcFsofcIfEntry->MibObject.i4FsofcIfIndex !=
                             (INT4) u4IfIndex) &&
                            (pOfcFsofcIfEntry->u4VlanId == OFC_ZERO))
                        {
                            /*
                             * Do not send out on logical interfaces as it will be
                             * already send on member ports of logical interface
                             */
                            Ofc131ActsSendPktOnPort ((UINT4)
                                                     (pOfcFsofcIfEntry->
                                                      MibObject.i4FsofcIfIndex),
                                                     u4ContextId, pPkt);
                        }
                    }
                    while ((pOfcFsofcIfEntry =
                            (tOfcFsofcIfEntry *)
                            RBTreeGetNext (gOfcGlobals.OfcGlbMib.FsofcIfTable,
                                           pOfcFsofcIfEntry, NULL)) != NULL);
                }
            }
            else if (u4OutputPort == OFPP_IN_PORT)
            {
                /* Send out on the ingress port */
                Ofc131ActsSendPktOnPort (u4IfIndex, u4ContextId, pPkt);
            }
            else if ((u4OutputPort == OFPP_NORMAL)
                     || (u4OutputPort == OFPP_LOCAL))
            {
                /* Check if the hybrid mode is enabled */
                if (pOfcFsofcCfgEntry->MibObject.i4FsofcModuleStatus !=
                    OFC_ZERO)
                {
                    pDupBuf = CRU_BUF_Duplicate_BufChain (pPkt);

                    if (pDupBuf == NULL)
                    {
                        OFC131_TRC_FUNC ((OFC_PKT_TRC,
                                          "Duplicate CRU BufFAILURE\n"));
                        CRU_BUF_Release_MsgBufChain (pPkt, FALSE);
                        OfcActOutputPortListFree (&ActOutPortList);
                        return OFC_FAILURE;
                    }
                    /* Deliver to traditional pipeline, which is ISS */
                    OfcHybridSendPktToIss (u4ContextId, pDupBuf);
                }
                else
                {
                    CfaIfSetInDiscard (u4IfIndex);
                    OFC_TRC_FUNC ((OFC_ACTS_TRC, "Drop the Packet, "
                                   "Hybrid mode not enabled\n"));
                    CRU_BUF_Release_MsgBufChain (pPkt, FALSE);
                    OfcActOutputPortListFree (&ActOutPortList);
                    return OFC_FAILURE;
                }
            }
            else if (u4OutputPort == OFPP_FLOOD)
            {
                /* Flood on all the standarad ports of the switch */
                OfcHybridFloodOnAllPorts (pOfcFsofcCfgEntry, u4IfIndex, pPkt);
            }
            else
            {
                Ofc131ActsSendPktOnPort (u4OutputPort, u4ContextId, pPkt);
            }
        }
        CRU_BUF_Release_MsgBufChain (pPkt, FALSE);
        OfcActOutputPortListFree (&ActOutPortList);
    }
    else
    {                            /* No Output Action specified */
        /* No action is set or Table miss */
        CfgEntry.MibObject.u4FsofcContextId = u4ContextId;

        /* Get the default Flow miss behavior from SNMP */
        if ((OfcGetAllFsofcCfgTable (&CfgEntry) == OFC_SUCCESS) &&
            (CfgEntry.MibObject.i4FsofcDefaultFlowMissBehaviour == OFC_TWO))
        {
            /*
             * If the No Packet-In msg flag is set on the input port,
             * drop the packet.
             */
            if (u4NoPktIn & OFC_ONE)
            {
                CfaIfSetInDiscard (u4IfIndex);
                OFC_TRC_FUNC ((OFC_ACTS_TRC, "Drop the Packet, "
                               "return FAILURE\n"));
                CRU_BUF_Release_MsgBufChain (pPkt, FALSE);
                return OFC_FAILURE;
            }

            /* 
             * If the default switch mode on connection failure is fail secure 
             * and client connection state is in disconnected state, 
             * then discard the packets 
             */
            if ((CfgEntry.u4OfcClientState == OFC_NOT_CONNECTED) &&
                (CfgEntry.MibObject.i4FsofcSwitchModeOnConnFailure ==
                 OFC_SWITCHMODE_FAIL_SECURE))
            {
                CfaIfSetInDiscard (u4IfIndex);
                OFC_TRC_FUNC ((OFC_ACTS_TRC, "Drop the Packet, "
                               "return FAILURE\n"));
                CRU_BUF_Release_MsgBufChain (pPkt, FALSE);
                return OFC_SUCCESS;
            }

            pQueMsg = (tOfcQueMsg *) MemAllocMemBlk (OFC_QUEUE_POOLID);
            if (pQueMsg == NULL)
            {
                /* MemPool Allocation failed */
                OFC_TRC_FUNC ((OFC_ACTS_TRC, "MemPool Allocated Failed, "
                               "return FAILURE\n"));
                CfaIfSetInDiscard (u4IfIndex);
                CRU_BUF_Release_MsgBufChain (pPkt, FALSE);
                return OFC_FAILURE;
            }
            MEMSET (pQueMsg, OFC_ZERO, sizeof (tOfcQueMsg));

            pQueMsg->u4MsgType = OFCR_NO_MATCH;
            pQueMsg->u4ContextId = u4ContextId;
            pQueMsg->u4IfIndex = u4IfIndex;
            pQueMsg->u4Index = u4Index;
            pQueMsg->pBuf = pPkt;
            if (pFlowEntry != NULL)
            {
                pQueMsg->u1TableId = (UINT1) (pFlowEntry->u4TableIndex);
                MEMCPY (&(pQueMsg->u8Cookie), &(pFlowEntry->u8Cookie),
                        OFC_EIGHT);
            }
            else
            {
                pQueMsg->u1TableId = u1FlMiTabId;
                pQueMsg->u8Cookie.u4Lo = OFC_UINT4_ALL_MASK;
                pQueMsg->u8Cookie.u4Hi = OFC_UINT4_ALL_MASK;
            }

            /* Allocate from Flow Match Mempool */
            pQueMsg->pOfcMatch = (tOfp131Match *) MemAllocMemBlk
                (OFC_FLOW_MATCH_TLV_POOLID);
            if (pQueMsg->pOfcMatch == NULL)
            {
                /* MemPool Allocation failed */
                OFC_TRC_FUNC ((OFC_ACTS_TRC, "MemPool Allocated Failed, "
                               "return FAILURE\n"));
                /* Release the mem-pool */
                CfaIfSetInDiscard (u4IfIndex);
                MemReleaseMemBlock (OFC_QUEUE_POOLID, (UINT1 *) pQueMsg);
                CRU_BUF_Release_MsgBufChain (pPkt, FALSE);
                return OFC_FAILURE;
            }

            MEMSET (pQueMsg->pOfcMatch, OFC_ZERO, OFC_MAX_FLOW_MATCH_TLV_SIZE);
            /* */
            if (pFlowEntry != NULL)
            {
                Ofc131PktMatchStructToTlv (&pFlowEntry->MatchList,
                                           pFlowEntry->u1FlowMatchCount,
                                           pQueMsg->pOfcMatch);
            }

            /* Post the Message to Queue */
            if (OfcQueSendMsg (pQueMsg) == OFC_FAILURE)
            {
                OFC_TRC_FUNC ((OFC_ACTS_TRC, "Posting to Queue Failed, "
                               "return FAILURE\n"));
                /* Just return, buffers are already released */
                CfaIfSetInDiscard (u4IfIndex);
                return OFC_FAILURE;
            }
        }
        else
        {                        /* Drop the Packet */
            CfaIfSetInDiscard (u4IfIndex);
            OFC_TRC_FUNC ((OFC_ACTS_TRC, "Drop the Packet, return FAILURE\n"));
            CRU_BUF_Release_MsgBufChain (pPkt, FALSE);
            return OFC_FAILURE;
        }
    }

    OFC_TRC_FUNC ((OFC_FN_EXIT, "FUNC:Ofc131ActsPipelineProcess SUCCESS\n"));
    return OFC_SUCCESS;
}
