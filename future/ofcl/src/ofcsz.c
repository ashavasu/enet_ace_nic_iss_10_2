/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: ofcsz.c,v 1.3 2014/01/31 13:07:23 siva Exp $
 *
 * Description: This files for OFCL.
 *******************************************************************/

#define _OFCSZ_C
#include "ofcinc.h"
#include "ofcapi.h"
#include "ofcsz.h"
extern INT4         IssSzRegisterModuleSizingParams (CHR1 * pu1ModName,
                                                     tFsModSizingParams *
                                                     pModSizingParams);
extern INT4         IssSzRegisterModulePoolId (CHR1 * pu1ModName,
                                               tMemPoolId * pModPoolId);

INT4
OfcSizingMemCreateMemPools ()
{
    UINT4               u4RetVal = OFC_ZERO;
    INT4                i4SizingId = OFC_ZERO;

    for (i4SizingId = OFC_ZERO; i4SizingId < OFC_MAX_SIZING_ID; i4SizingId++)
    {
        u4RetVal =
            MemCreateMemPool (FsOFCSizingParams[i4SizingId].u4StructSize,
                              FsOFCSizingParams[i4SizingId].
                              u4PreAllocatedUnits, MEM_DEFAULT_MEMORY_TYPE,
                              &(OFCMemPoolIds[i4SizingId]));
        if (u4RetVal == MEM_FAILURE)
        {
            OfcSizingMemDeleteMemPools ();
            return OSIX_FAILURE;
        }
    }
    return OSIX_SUCCESS;
}

INT4
OfcSzRegisterModuleSizingParams (CHR1 * pu1ModName)
{
    /* Copy the Module Name */
    IssSzRegisterModuleSizingParams (pu1ModName, FsOFCSizingParams);
    IssSzRegisterModulePoolId (pu1ModName, OFCMemPoolIds);
    return OSIX_SUCCESS;
}

VOID
OfcSizingMemDeleteMemPools ()
{
    INT4                i4SizingId = OFC_ZERO;

    for (i4SizingId = OFC_ZERO; i4SizingId < OFC_MAX_SIZING_ID; i4SizingId++)
    {
        if (OFCMemPoolIds[i4SizingId] != OFC_ZERO)
        {
            MemDeleteMemPool (OFCMemPoolIds[i4SizingId]);
            OFCMemPoolIds[i4SizingId] = OFC_ZERO;
        }
    }
    return;
}
