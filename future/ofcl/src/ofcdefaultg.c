/********************************************************************
* Copyright (C) 2013 Aricent Inc . All Rights Reserved
*
* $Id: ofcdefaultg.c,v 1.3 2014/03/01 11:36:47 siva Exp $
*
* Description: This file contains the routines to initialize the
*              mib objects for the module Ofc 
*********************************************************************/

#include "ofcinc.h"

/****************************************************************************
* Function    : OfcInitializeMibFsofcCfgTable
* Input       : pOfcFsofcCfgEntry
* Description : It Intialize the given structure
* Output      : None
* Returns     : OSIX_SUCCESS or OSIX_FAILURE
*****************************************************************************/
INT4
OfcInitializeMibFsofcCfgTable (tOfcFsofcCfgEntry * pOfcFsofcCfgEntry)
{

    pOfcFsofcCfgEntry->MibObject.i4FsofcModuleStatus = 0;

    pOfcFsofcCfgEntry->MibObject.i4FsofcSupportedVersion = 2;

    pOfcFsofcCfgEntry->MibObject.i4FsofcDefaultFlowMissBehaviour = 2;

    pOfcFsofcCfgEntry->MibObject.i4FsofcControlPktBuffering = 2;

    pOfcFsofcCfgEntry->MibObject.i4FsofcIpReassembleStatus = 2;

    pOfcFsofcCfgEntry->MibObject.i4FsofcPortStpStatus = 2;

    pOfcFsofcCfgEntry->MibObject.i4FsofcSwitchModeOnConnFailure = 2;

    pOfcFsofcCfgEntry->MibObject.i4FsofcHybrid = -1;

    return OSIX_SUCCESS;
}

/****************************************************************************
* Function    : OfcInitializeMibFsofcControllerConnTable
* Input       : pOfcFsofcControllerConnEntry
* Description : It Intialize the given structure
* Output      : None
* Returns     : OSIX_SUCCESS or OSIX_FAILURE
*****************************************************************************/
INT4
OfcInitializeMibFsofcControllerConnTable (tOfcFsofcControllerConnEntry *
                                          pOfcFsofcControllerConnEntry)
{

    pOfcFsofcControllerConnEntry->MibObject.i4FsofcControllerConnPort = 6633;

    pOfcFsofcControllerConnEntry->MibObject.i4FsofcControllerConnProtocol = 1;

    pOfcFsofcControllerConnEntry->MibObject.i4FsofcControllerRole = 1;

    pOfcFsofcControllerConnEntry->MibObject.i4FsofcControllerConnEchoReqCount =
        0;

    pOfcFsofcControllerConnEntry->MibObject.i4FsofcControllerConnBand = 1;

    return OSIX_SUCCESS;
}
