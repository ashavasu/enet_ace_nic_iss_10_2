/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: ofcidxmg.c,v 1.2 2014/04/30 10:10:25 siva Exp $
 *
 * Description: This file contains functions that are associated
 *              with L2VpnIndex Table Manager.
 ********************************************************************/

 /*---------------------------------------------------------------------
 *  The OFC Flow index table manager has been created to provide support for 
 *  the OFC Flow Indexing requirements. However the usage of this library does 
 *  not restrict to this alone, rather it can be used by any application 
 *  which requires to maintain an unique set of identifiers, obtain and 
 *  release an identifier from that set.
 *
 *  The functions included in this file are 
 *  1) To initialise the index manager
 *  2) To shutdown the index manager
 *  3) Obtain a new index from an index space/group
 *  4) Obtain a specified  index from an index space/group
 *  5) Release an index to an index space/group
 *  
 *------------------------------------------------------------------------*/
#ifndef _OFCIDXMG_C
#define _OFCIDXMG_C

#include "ofcidxsz.h"
#include "ofcidxgl.h"
#include "ofcl.h"
#include "ofcapi.h"
#define MAX_INDEXMGR_GRPS 1
extern UINT4        IssSzGetSizingParamsForModule (CHR1 * pu1ModName,
                                                   CHR1 * pu1MacroName);
UINT1               OfcIndexManagerInit (VOID);

tOsixSemId          gOfcIndexMgrSemId;
tIndexMgrGrpInfo   *gpOfcIndexMgrGrpInfo = NULL;
UINT4               au4OfcGrpMaxIndices[] = { MAX_OFC_FLOW_INDEX };

tIndexMgrGrpTable   OfcIndexMgrGrpEntry[] = {
    {"OFC", "MAX_OFC_FLOW_INDEX"}
};
UINT4               aOfcAvlByteBmap[] = {
    0xFF000000, 0x00FF0000, 0x0000FF00, 0x000000FF
};
UINT4               aOfcAvlBitmap[] = {
    0x80000000, 0x40000000, 0x20000000, 0x10000000,
    0x08000000, 0x04000000, 0x02000000, 0x01000000,
    0x00800000, 0x00400000, 0x00200000, 0x00100000,
    0x00080000, 0x00040000, 0x00020000, 0x00010000,
    0x00008000, 0x00004000, 0x00002000, 0x00001000,
    0x00000800, 0x00000400, 0x00000200, 0x00000100,
    0x00000080, 0x00000040, 0x00000020, 0x00000010,
    0x00000008, 0x00000004, 0x00000002, 0x00000001
};
UINT4               aOfcAsgnBitmap[] = {
    0x7FFFFFFF, 0xBFFFFFFF, 0xDFFFFFFF, 0xEFFFFFFF,
    0xF7FFFFFF, 0xFBFFFFFF, 0xFDFFFFFF, 0xFEFFFFFF,
    0xFF7FFFFF, 0xFFBFFFFF, 0xFFDFFFFF, 0xFFEFFFFF,
    0xFFF7FFFF, 0xFFFBFFFF, 0xFFFDFFFF, 0xFFFEFFFF,
    0xFFFF7FFF, 0xFFFFBFFF, 0xFFFFDFFF, 0xFFFFEFFF,
    0xFFFFF7FF, 0xFFFFFBFF, 0xFFFFFDFF, 0xFFFFFEFF,
    0xFFFFFF7F, 0xFFFFFFBF, 0xFFFFFFDF, 0xFFFFFFEF,
    0xFFFFFFF7, 0xFFFFFFFB, 0xFFFFFFFD, 0xFFFFFFFE
};

/*---------------------------------------------------------------------------
 * Function    : OfcIndexMgrInitWithSem
 * Description : This funciton creates the index manager semaphore and 
 *               initialises the index manager.
 * Inputs      : None. 
 * Outputs     : The global variable gpOfcIndexMgrGrpInfo will be allocated
 *               memory and suitably updated in case of successful 
 *               allocation. Another Pool Id corresponds to the free memory
 *               pool assigned to all required chunks for every group.
 * Returns     : OFC_SUCCESS on successful memory allocation 
 *               and initialisation. Returns OFC_FAILURE o.w.
---------------------------------------------------------------------------*/
UINT1
OfcIndexMgrInitWithSem (VOID)
{
    /* Semaphore Create */
    if (OsixCreateSem
        (OFC_INDEXMGR_SEM_NAME, OFC_ONE, OFC_ZERO,
         &gOfcIndexMgrSemId) != OSIX_SUCCESS)
    {
        OFC_FLOW_DBG (DBG_ERR_CRT, "INDEXMGR : Semaphore Create Failed\n");
        return OFC_FAILURE;
    }
    if (OfcIndexManagerInit () == OFC_FAILURE)
    {
        OFC_FLOW_DBG (DBG_ERR_CRT, "INDEXMGR : Initialization Failed\n");
        return OFC_FAILURE;
    }
    return OFC_SUCCESS;
}

/*---------------------------------------------------------------------------
 * Function    : OfcIndexManagerInit
 * Description : This function allocates the necessary structures for the 
 *               Index Tbl manager. This function must be invoked before 
 *               using any other routines associated with the Index Manager.
 * Inputs      : None. 
 * Outputs     : The global variable gpOfcIndexMgrGrpInfo will be allocated
 *               memory and suitably updated in case of successful 
 *               allocation. Another pool id corresponds to the free memory
 *               pool assigned to all required chunks for every group.
 * Returns     : OFC_SUCCESS on successful memory allocation 
 *               and initialisation. Returns OFC_FAILURE o.w.
---------------------------------------------------------------------------*/
UINT1
OfcIndexManagerInit (VOID)
{
    UINT1               u1Index1 = OFC_ZERO;
    UINT1               u1Index2 = OFC_ZERO;
    UINT4               u4Index = OFC_ZERO;
    UINT4               u4TotalChunksReqd = OFC_ZERO;    /* sum of maxchunks for every grp */
    UINT4               u4MaxIndices = OFC_ZERO;
    tIndexMgrGrpInfo   *pIndexMgrGrpInfo = NULL;
    tIndexMgrChunkInfo *pIndexMgrChunkInfo = NULL;

    if (FsOFCINDEXMGRSizingParams[MAX_INDEXMGR_INDEX_MGR_GRP_SIZING_ID].
        u4PreAllocatedUnits != MAX_INDEXMGR_INDEX_MGR_GRP)
    {
        FsOFCINDEXMGRSizingParams[MAX_INDEXMGR_INDEX_MGR_GRP_SIZING_ID].
            u4StructSize
            = (sizeof (tIndexMgrGrpInfo) *
               FsOFCINDEXMGRSizingParams[MAX_INDEXMGR_INDEX_MGR_GRP_SIZING_ID].
               u4PreAllocatedUnits);
        FsOFCINDEXMGRSizingParams[MAX_INDEXMGR_INDEX_MGR_GRP_SIZING_ID].
            u4PreAllocatedUnits = MAX_INDEXMGR_INDEX_MGR_GRP;
    }

    if (FsOFCINDEXMGRSizingParams[MAX_INDEXMGR_INDEX_MGR_CHUNK_SIZING_ID].
        u4PreAllocatedUnits != MAX_INDEXMGR_INDEX_MGR_CHUNK)
    {
        FsOFCINDEXMGRSizingParams[MAX_INDEXMGR_INDEX_MGR_CHUNK_SIZING_ID].
            u4StructSize
            = (sizeof (tIndexMgrChunkInfo) *
               FsOFCINDEXMGRSizingParams
               [MAX_INDEXMGR_INDEX_MGR_CHUNK_SIZING_ID].u4PreAllocatedUnits);
        FsOFCINDEXMGRSizingParams[MAX_INDEXMGR_INDEX_MGR_CHUNK_SIZING_ID].
            u4PreAllocatedUnits = MAX_INDEXMGR_INDEX_MGR_CHUNK;
    }

    for (u1Index1 = OFC_ZERO; u1Index1 < MAX_INDEXMGR_GRPS; u1Index1++)
    {
        u4MaxIndices = OFC_ZERO;

        u4MaxIndices
            = IssSzGetSizingParamsForModule (OfcIndexMgrGrpEntry[u1Index1].
                                             pc1ModName,
                                             OfcIndexMgrGrpEntry[u1Index1].
                                             pc1GrpName);

        if (u4MaxIndices != OFC_ZERO)
        {
            au4OfcGrpMaxIndices[u1Index1] = u4MaxIndices;
        }
    }

    /* Create Mempools for Index manager. */
    if (OfcIndexmgrSizingMemCreateMemPools () == OSIX_FAILURE)
    {
        return OFC_FAILURE;
    }

    /* Memory for Index Groups structure being allocated */
    if (NULL == (gpOfcIndexMgrGrpInfo = MemAllocMemBlk (GROUP_INFO_POOL_ID)))
    {
        OFC_FLOW_DBG (DBG_ERR_CRT,
                      "INDEXMGR : Index Group Info - Mem alloc Failure\n");
        return OFC_FAILURE;
    }

    /* Now to initialize each group */
    if (OFC_FAILURE == OfcIndexMgrInitGrps ())
    {
        /* Free all allocated memory & exit */
        MemReleaseMemBlock (GROUP_INFO_POOL_ID, (UINT1 *) gpOfcIndexMgrGrpInfo);
        gpOfcIndexMgrGrpInfo = NULL;
        OFC_FLOW_DBG (DBG_ERR_CRT,
                      "INDEXMGR : Index Group Info Init Failure\n");
        return OFC_FAILURE;
    }

    /* Now to allocate memory for the Max-chunk-info structs for each grp */
    pIndexMgrGrpInfo = gpOfcIndexMgrGrpInfo;
    for (u1Index1 = OFC_ZERO; u1Index1 < MAX_INDEXMGR_GRPS;
         u1Index1++, pIndexMgrGrpInfo++)
    {
        if (NULL == (pIndexMgrGrpInfo->pIndexChunkInfoTbl =
                     MemAllocMemBlk (CHUNK_INFO_POOL_ID)))
        {
            /* Release all memory unto this group & exit */
            pIndexMgrGrpInfo = gpOfcIndexMgrGrpInfo;
            for (u1Index2 = OFC_ZERO; u1Index2 < u1Index1;
                 u1Index2++, pIndexMgrGrpInfo++)
            {
                MemReleaseMemBlock (CHUNK_INFO_POOL_ID, (UINT1 *)
                                    pIndexMgrGrpInfo->pIndexChunkInfoTbl);
                pIndexMgrGrpInfo->pIndexChunkInfoTbl = NULL;
            }

            MemReleaseMemBlock (GROUP_INFO_POOL_ID,
                                (UINT1 *) gpOfcIndexMgrGrpInfo);
            gpOfcIndexMgrGrpInfo = NULL;

            OFC_FLOW_DBG (DBG_ERR_CRT,
                          "INDEXMGR : Chunk-info-table - Mem alloc Failure\n");

            return OFC_FAILURE;
        }

        /* Initialize the chunk-info structure array */
        MEMSET ((VOID *) pIndexMgrGrpInfo->pIndexChunkInfoTbl, OFC_ZERO,
                sizeof (tIndexMgrChunkInfo) *
                pIndexMgrGrpInfo->u4MaxChunksSprtd);

        u4TotalChunksReqd += pIndexMgrGrpInfo->u4MaxChunksSprtd;
    }

    /* Now to assign appropriate no. of chunks to each group */
    pIndexMgrGrpInfo = gpOfcIndexMgrGrpInfo;
    for (u1Index1 = OFC_ZERO; u1Index1 < MAX_INDEXMGR_GRPS;
         u1Index1++, pIndexMgrGrpInfo++)
    {
        /* Now to allocate memory from pool for every chunk-info per group */
        pIndexMgrChunkInfo = pIndexMgrGrpInfo->pIndexChunkInfoTbl;
        for (u4Index = OFC_ZERO; u4Index < pIndexMgrGrpInfo->u4MaxChunksSprtd;
             u4Index++, pIndexMgrChunkInfo++)
        {
            /* Allocate memory for the actual chunk */

            pIndexMgrChunkInfo->pu4IndexChunk = (UINT4 *)
                MemAllocMemBlk (CHUNK_TBL_POOL_ID);

            if (pIndexMgrChunkInfo->pu4IndexChunk == NULL)
            {
                OfcIndexMgrDeInit ();
                OFC_FLOW_DBG (DBG_ERR_CRT,
                              "INDEXMGR : Init chunk mem-alloc failure.\n");
                return OFC_FAILURE;
            }

            MEMSET ((VOID *) pIndexMgrChunkInfo->pu4IndexChunk,
                    0xFF, MAX_INDEXMGR_CHUNK_SIZE);
            /* All bits 1 => all avail */
            pIndexMgrGrpInfo->u4NumChunksInGrp++;
        }
    }
    return OFC_SUCCESS;
}

/*------------------------------------------------------------------------
 * * Function    : OfcIndexMgrLock
 * * Description : This function takes the index manager lock.
 * * Inputs      : None
 * * Outputs     : None
 * * Returns     : None
 * ------------------------------------------------------------------------*/
VOID
OfcIndexMgrLock (VOID)
{
    OsixSemTake (gOfcIndexMgrSemId);
}

/*------------------------------------------------------------------------
 * * Function    : OfcIndexMgrUnLock
 * * Description : This function gives the index manager lock.
 * * Inputs      : None
 * * Outputs     : None
 * * Returns     : None
 * ------------------------------------------------------------------------*/
VOID
OfcIndexMgrUnLock (VOID)
{
    OsixSemGive (gOfcIndexMgrSemId);
}

/*------------------------------------------------------------------------
* Function    : OfcIndexMgrDeInit
* Description : This function releases all the memory allocated for the 
*               entire data-structure. (Use for graceful shutdown)
* Inputs      : None
* Outputs     : None
* Returns     : OFC_SUCCESS/OFC_FAILURE
------------------------------------------------------------------------*/

UINT1
OfcIndexMgrDeInit (VOID)
{
    UINT1               u1Index = OFC_ZERO;    /* for groups */
    UINT4               u4Index = OFC_ZERO;    /* for chunk infos per group */
    tIndexMgrGrpInfo   *pIndexMgrGrpInfo = gpOfcIndexMgrGrpInfo;
    tIndexMgrChunkInfo *pIndexMgrChunkInfo = NULL;

    /* Now to release the memory in reverse order of allocation */
    for (u1Index = OFC_ZERO; u1Index < MAX_INDEXMGR_GRPS;
         u1Index++, pIndexMgrGrpInfo++)
    {
        /* Now to go thru' the chunk-info list */
        pIndexMgrChunkInfo = pIndexMgrGrpInfo->pIndexChunkInfoTbl;

        for (u4Index = OFC_ZERO; u4Index < pIndexMgrGrpInfo->u4NumChunksInGrp;
             u4Index++, pIndexMgrChunkInfo++)
        {
            /* Release memory for the actual chunks to the pool */
            if (NULL == pIndexMgrGrpInfo->pIndexChunkInfoTbl)
            {
                continue;        /* This will never happen actually */
            }

            MemReleaseMemBlock (CHUNK_TBL_POOL_ID,
                                (UINT1 *) pIndexMgrChunkInfo->pu4IndexChunk);
            pIndexMgrChunkInfo->pu4IndexChunk = NULL;
        }

        /* Now to release memory for every chunk-info per group */
        MemReleaseMemBlock (CHUNK_INFO_POOL_ID,
                            (UINT1 *) pIndexMgrGrpInfo->pIndexChunkInfoTbl);
        pIndexMgrGrpInfo->pIndexChunkInfoTbl = NULL;
    }

    MemReleaseMemBlock (GROUP_INFO_POOL_ID, (UINT1 *) gpOfcIndexMgrGrpInfo);
    pIndexMgrGrpInfo = gpOfcIndexMgrGrpInfo = NULL;

    /* Delete Index Manager Memory Pools. */
    OfcIndexmgrSizingMemDeleteMemPools ();

    return OFC_SUCCESS;
}

/*--------------------------------------------------------------------------
 * Function    : OfcIndexMgrGetIndexBasedOnFlag 
 * Description : This function returns a unique index for the specified 
 *               group. Returns 0 if all indices for the
 *               specified group have been exhausted.
 * Input       : GroupID (1 = OFC Flow Indices). 
 *               Flag(FALSE = 0 ,TRUE=1) 
 * Outputs     : None
 * Returns     : Unique Index if available (ranging from 1 to max in that 
 *               group), 0 o.w.
--------------------------------------------------------------------------*/

UINT4
OfcIndexMgrGetIndexBasedOnFlag (UINT1 u1GrpID, BOOL1 bFlag)
{
    UINT4               u4Index = OFC_ZERO;
    UINT1               u1BytePos = OFC_ZERO;
    UINT1               u1BitPos = OFC_ZERO;
    tIndexMgrGrpInfo   *pIndexMgrGrpInfo = gpOfcIndexMgrGrpInfo;
    tIndexMgrChunkInfo *pIndexMgrChunkInfo = NULL;
    UINT4              *pu4AvailOffset = NULL;

    if (gpOfcIndexMgrGrpInfo == NULL)
    {
        OFC_FLOW_DBG (DBG_ERR_CRT,
                      "INDEXMGR :Index Manager is not initialized \n");
        return OFC_ZERO;
    }

    /* Check if indices are available for the mentioned group */
    if (INDEXMGR_GRP_FULL == OfcIndexMgrCheckGrpFull (u1GrpID))
    {
        return OFC_ZERO;
    }

    pIndexMgrGrpInfo += (u1GrpID - OFC_ONE);
    pIndexMgrChunkInfo = pIndexMgrGrpInfo->pIndexChunkInfoTbl;
    pIndexMgrChunkInfo += pIndexMgrGrpInfo->u4AvailChunkID;
    pu4AvailOffset = pIndexMgrChunkInfo->pu4IndexChunk;
    pu4AvailOffset += pIndexMgrChunkInfo->u4CurOffset;

    /* Now to check each byte of it to get the avail-byte & bit positions */
    if (OFC_FAILURE ==
        OfcIndexMgrGetAvailByteBitPos (*pu4AvailOffset, &u1BytePos, &u1BitPos))
    {
        /* This is never possible */
        OFC_FLOW_DBG (DBG_ERR_CRT,
                      "INDEXMGR : GetIndex : Byte, bit pos. fail \n");
        return OFC_ZERO;
    }

    /* Now to calculate the index value corresponding to this bit found */
    u4Index =
        (UINT4) ((pIndexMgrGrpInfo->u4AvailChunkID *
                  (UINT4) MAX_INDEXMGR_INDICES_PER_CHUNK) +
                 (pIndexMgrChunkInfo->u4CurOffset *
                  (UINT4) MAX_INDEXMGR_INDICES_PER_ENTRY) +
                 ((UINT4) u1BytePos * (UINT4) INDEXMGR_BYTE_BLOCK_SIZE) +
                 (UINT4) (u1BitPos));

    /* If Index Manager Flag is Set , mark the bit as allocated and adjust to the next available offset */

    if (bFlag == TRUE)
    {

        /* Now to mark that bit as allocated (make it zero) */
        *pu4AvailOffset &=
            aOfcAsgnBitmap[(u1BytePos * INDEXMGR_BYTE_BLOCK_SIZE) + u1BitPos];

        /* Now to increment the allocated-indices count */
        pIndexMgrChunkInfo->u4NumKeysAlloc++;
        pIndexMgrGrpInfo->u4TotalKeysAlloc++;

        /* Now to adjust the next available offset and/or block */
        if (OFC_FAILURE == OfcIndexMgrUpdateChunkOffset (u1GrpID))
        {
            /* ideally, shouldn't fail */
            OFC_FLOW_DBG (DBG_ERR_CRT,
                          "INDEXMGR : Offset Adjustment Failure \n");
            return OFC_ZERO;
        }
    }

    /* as u4Index is from 1 (& not 0) to max. sprtd. by grp */
    return (u4Index + OFC_ONE);
}

/*--------------------------------------------------------------------------
 * Function    : OfcIndexMgrGetAvailableIndex 
 * Description : This function returns a unique index for the specified 
 *               group. Returns 0 if all indices for the
 *               specified group have been exhausted.
 * Input       : GroupID (1 = OFC FlowIndices). 
 * Outputs     : None
 * Returns     : Unique Index if available (ranging from 1 to max in that 
 *               group), 0 o.w.
--------------------------------------------------------------------------*/

UINT4
OfcIndexMgrGetAvailableIndex (UINT1 u1GrpID)
{
    UINT4               u4Index = OFC_ZERO;
    OFC_INDEXMGR_LOCK ();
    u4Index = OfcIndexMgrGetIndexBasedOnFlag (u1GrpID, FALSE);
    OFC_INDEXMGR_UNLOCK ();
    return u4Index;
}

/*--------------------------------------------------------------------------
 * Function    : OfcIndexMgrSetIndexBasedOnFlag 
 * Description : This function returns the Index Value specified , if present .
 *          Returns 0 otherwise.
 * Inputs      : GroupID (1 = OFC Flow Indices). 
 *               bFlag(FALSE = 0 ,TRUE=1)
 *               Index Value. 
 * Outputs     : None
 * Returns     : Unique Index if available (ranging from 1 to max in that 
 *               group), 0 o.w.
--------------------------------------------------------------------------*/
UINT4
OfcIndexMgrSetIndexBasedOnFlag (UINT1 u1GrpID, UINT4 u4Index, BOOL1 bFlag)
{
    UINT4               u4BitPos = OFC_ZERO;
    UINT4               u4ChunkID = OFC_ZERO;
    UINT4               u4Offset = OFC_ZERO;
    tIndexMgrGrpInfo   *pIndexMgrGrpInfo = gpOfcIndexMgrGrpInfo;
    tIndexMgrChunkInfo *pIndexMgrChunkInfo = NULL;
    UINT4              *pu4AvailOffset = NULL;

    /* First check if the group id  is valid */

    if ((u1GrpID < OFC_ONE) || (u1GrpID > MAX_INDEXMGR_GRPS))
    {
        OFC_FLOW_DBG (DBG_ERR_CRT, "INDEXMGR :INVALID GROUP ID \n");
        return OFC_ZERO;
    }

    /* Check if the index to be set  is valid */
    if ((u4Index < OFC_ONE)
        || (u4Index > au4OfcGrpMaxIndices[u1GrpID - OFC_ONE]))
    {
        OFC_FLOW_DBG2 (DBG_ERR_CRT,
                       "INDEXMGR : Set Index FAILURE - %d not in range for Group %d\n",
                       u4Index, u1GrpID);
        return OFC_ZERO;
    }

    pIndexMgrGrpInfo += (u1GrpID - OFC_ONE);

    /* To Calculate the appropriate bit position based on the Index Value  */

    u4ChunkID = ((u4Index - OFC_ONE) / MAX_INDEXMGR_INDICES_PER_CHUNK);
    u4Offset =
        (((u4Index -
           OFC_ONE) % MAX_INDEXMGR_INDICES_PER_CHUNK) /
         MAX_INDEXMGR_INDICES_PER_ENTRY);
    u4BitPos =
        (((u4Index -
           OFC_ONE) % MAX_INDEXMGR_INDICES_PER_CHUNK) %
         MAX_INDEXMGR_INDICES_PER_ENTRY);

    /* Now to reach that offset */
    pIndexMgrChunkInfo = pIndexMgrGrpInfo->pIndexChunkInfoTbl;
    pIndexMgrChunkInfo += u4ChunkID;
    pu4AvailOffset = pIndexMgrChunkInfo->pu4IndexChunk;
    pu4AvailOffset += u4Offset;

    /* To Check if this particular bit is already allocated or not. If not allocated , set it . 
     * Otherwise return failure */

    if ((*pu4AvailOffset & (~(aOfcAsgnBitmap[u4BitPos]))) == OFC_ZERO)
    {
        /* Index is Already Allocated */
        return OFC_ZERO;
    }

    else
    {
        if (bFlag == TRUE)
        {
            /* Now to mark that bit as allocated (make it zero) */
            (*pu4AvailOffset) &= aOfcAsgnBitmap[u4BitPos];

            /* Now to increment the allocated-indices count */
            pIndexMgrChunkInfo->u4NumKeysAlloc++;
            pIndexMgrGrpInfo->u4TotalKeysAlloc++;

            /* Now to adjust the next available offset and/or block */
            if (OFC_ZERO == OfcIndexMgrUpdateChunkOffset (u1GrpID))
            {
                /* ideally, shouldn't fail */
                OFC_FLOW_DBG (DBG_ERR_CRT,
                              "INDEXMGR : Offset Adjustment Failure \n");
                return OFC_ZERO;
            }
        }
    }
    return u4Index;
}

/*--------------------------------------------------------------------------
 * Function    : OfcIndexMgrSetIndex 
 * Description : This function returns the Index Value specified , if present .
 *          Returns 0 otherwise.
 * Inputs       : GroupID (1 = PwIndices, 2 = InSegment Indices, 
 *               3 = OutSegment Indices, 4 = XC Indices,
 *               5 = Lbl Stack Indices, 6 = FTN Indices, 7 = LDP Entity indices,
 *               8 = Max LSP Indices, 9 = VPN Identifiers.). 
 *               Index Value. 
 * Outputs     : None
 * Returns     : Unique Index if available (ranging from 1 to max in that 
 *               group), 0 o.w.
--------------------------------------------------------------------------*/

UINT4
OfcIndexMgrSetIndex (UINT1 u1GrpID, UINT4 u4Index)
{
    UINT4               u4SetIndex = OFC_ZERO;
    OFC_INDEXMGR_LOCK ();
    u4SetIndex = OfcIndexMgrSetIndexBasedOnFlag (u1GrpID, u4Index, TRUE);
    OFC_INDEXMGR_UNLOCK ();
    return u4SetIndex;
}

/*--------------------------------------------------------------------------
 * Function    : OfcIndexMgrCheckIndex 
 * Description : This function returns the Index Value specified , if present .
 *          Returns 0 otherwise.
 * Inputs       : GroupID (1 = PwIndices, 2 = InSegment Indices, 
 *               3 = OutSegment Indices, 4 = XC Indices,
 *               5 = Lbl Stack Indices, 6 = FTN Indices, 7 = LDP Entity indices,
 *               8 = Max LSP Indices, 9 = VPN Identifiers.).
 *               Index value
 * Outputs     : None
 * Returns     : Unique Index if available (ranging from 1 to max in that 
 *               group), 0 o.w.
--------------------------------------------------------------------------*/

UINT4
OfcIndexMgrCheckIndex (UINT1 u1GrpID, UINT4 u4Index)
{
    UINT4               u4CheckIndex = OFC_ZERO;
    OFC_INDEXMGR_LOCK ();
    u4CheckIndex = OfcIndexMgrSetIndexBasedOnFlag (u1GrpID, u4Index, FALSE);
    OFC_INDEXMGR_UNLOCK ();
    return u4CheckIndex;
}

/*---------------------------------------------------------------------------
 * Function    : OfcIndexMgrRelIndex 
 * Description : This function releases an index to the specified group. 
 * Inputs       : GroupID (1 = PwIndices, 2 = InSegment Indices, 
 *               3 = OutSegment Indices, 4 = XC Indices,
 *               5 = Lbl Stack Indices, 6 = FTN Indices, 7 = LDP Entity indices,
 *               8 = Max LSP Indices, 9 = VPN Identifiers.).
 *               Index value
 * Outputs     : None
 * Returns     : OFC_SUCCESS if able to 
 *               0 otherwise.
---------------------------------------------------------------------------*/

UINT1
OfcIndexMgrRelIndex (UINT1 u1GrpID, UINT4 u4Index)
{
    UINT4               u4BitPos = OFC_ZERO;
    UINT4               u4ChunkID = OFC_ZERO;
    UINT4               u4Offset = OFC_ZERO;
    tIndexMgrGrpInfo   *pIndexMgrGrpInfo = gpOfcIndexMgrGrpInfo;
    tIndexMgrChunkInfo *pIndexMgrChunkInfo = NULL;
    UINT4              *pu4AvailOffset = NULL;

    /* First check if the group id  is valid */

    if ((u1GrpID < OFC_ONE) || (u1GrpID > MAX_INDEXMGR_GRPS))
    {
        OFC_FLOW_DBG (DBG_ERR_CRT, "INDEXMGR :INVALID GROUP ID  \n");
        return OFC_ZERO;
    }

    OFC_INDEXMGR_LOCK ();

    pIndexMgrGrpInfo += (u1GrpID - OFC_ONE);

    /* Check if the index to be released is valid */
    if ((u4Index < OFC_ONE)
        || (u4Index > au4OfcGrpMaxIndices[u1GrpID - OFC_ONE]))
    {
        OFC_FLOW_DBG2 (DBG_ERR_CRT,
                       "INDEXMGR : Rel Index FAILURE - %d not in range for Group %d\n",
                       u4Index, u1GrpID);
        OFC_INDEXMGR_UNLOCK ();
        return OFC_ZERO;
    }

    /* OK to release an already released index (situation shouldn't occur) */

    u4ChunkID = ((u4Index - OFC_ONE) / MAX_INDEXMGR_INDICES_PER_CHUNK);
    u4Offset =
        (((u4Index -
           OFC_ONE) % MAX_INDEXMGR_INDICES_PER_CHUNK) /
         MAX_INDEXMGR_INDICES_PER_ENTRY);
    u4BitPos =
        (((u4Index -
           OFC_ONE) % MAX_INDEXMGR_INDICES_PER_CHUNK) %
         MAX_INDEXMGR_INDICES_PER_ENTRY);

    /* Now to reach that offset */
    pIndexMgrChunkInfo = pIndexMgrGrpInfo->pIndexChunkInfoTbl;
    pIndexMgrChunkInfo += u4ChunkID;
    pu4AvailOffset = pIndexMgrChunkInfo->pu4IndexChunk;
    pu4AvailOffset += u4Offset;

    /* Now to make this particular bit zero */
    (*pu4AvailOffset) |= aOfcAvlBitmap[u4BitPos];

    /* Now to readjust the available-offset order of these 2 if's imp */
    if (MAX_INDEXMGR_INDICES_PER_CHUNK == pIndexMgrChunkInfo->u4NumKeysAlloc)
    {
        /* offset was positioned at start, so now adjust it */
        pIndexMgrChunkInfo->u4CurOffset = u4Offset;
    }

    if (u4Offset < (pIndexMgrChunkInfo->u4CurOffset))
    {
        pIndexMgrChunkInfo->u4CurOffset = u4Offset;
    }

    /* Now to readjust the available-chunkID (order of these 2 if's is imp) */
    if (pIndexMgrGrpInfo->u4TotalKeysAlloc ==
        au4OfcGrpMaxIndices[u1GrpID - OFC_ONE])
    {
        /* Available chunk was positioned at start, so now readjust */
        pIndexMgrGrpInfo->u4AvailChunkID = u4ChunkID;
    }

    if (u4ChunkID < (pIndexMgrGrpInfo->u4AvailChunkID))
    {
        pIndexMgrGrpInfo->u4AvailChunkID = u4ChunkID;
    }

    /* Now to decrement the no. of keys allocated */
    (pIndexMgrChunkInfo->u4NumKeysAlloc)--;
    (pIndexMgrGrpInfo->u4TotalKeysAlloc)--;

    OFC_INDEXMGR_UNLOCK ();
    return OFC_SUCCESS;
}

/*------------------------------------------------------------------------
* Function    : IndexMgrInitGroups
* Description : Initializes the fields of all the group structures involved
* Inputs      : None
* Outputs     : None
* Returns     : OFC_SUCCESS/OFC_FAILURE 
-------------------------------------------------------------------------*/

UINT1
OfcIndexMgrInitGrps ()
{
    UINT1               u1Index = OFC_ZERO;
    tIndexMgrGrpInfo   *pIndexMgrGrpInfo = gpOfcIndexMgrGrpInfo;

    for (u1Index = 0; u1Index < MAX_INDEXMGR_GRPS;
         u1Index++, pIndexMgrGrpInfo++)
    {
        pIndexMgrGrpInfo->u4NumChunksInGrp = OFC_ZERO;
        pIndexMgrGrpInfo->u4AvailChunkID = OFC_ZERO;
        pIndexMgrGrpInfo->u4TotalKeysAlloc = OFC_ZERO;
        pIndexMgrGrpInfo->pIndexChunkInfoTbl = NULL;

        pIndexMgrGrpInfo->u4MaxChunksSprtd =
            (((au4OfcGrpMaxIndices[u1Index] % MAX_INDEXMGR_INDICES_PER_CHUNK) ==
              OFC_ZERO) ? (au4OfcGrpMaxIndices[u1Index] /
                           MAX_INDEXMGR_INDICES_PER_CHUNK)
             : (au4OfcGrpMaxIndices[u1Index] / MAX_INDEXMGR_INDICES_PER_CHUNK +
                OFC_ONE));
    }

    return OFC_SUCCESS;
}

/*---------------------------------------------------------------------------
 * Function    : OfcIndexMgrUpdateChunkOffset 
 * Description : This function searches for a new offset for allocation of 
 *               index in the same chunk that delivered the latest index. 
 *               If this chunk is full, it proceeds with other chunks.
 * Inputs       : GroupID (1 = PwIndices, 2 = InSegment Indices, 
 *               3 = OutSegment Indices, 4 = XC Indices,
 *               5 = Lbl Stack Indices, 6 = FTN Indices, 7 = LDP Entity indices,
 *               8 = Max LSP Indices, 9 = VPN Identifiers.).
 *               Index value
 * Outputs     : None
 * Returns     : OFC_SUCCESS if updation successful 
 *               0 otherwise.
---------------------------------------------------------------------------*/

UINT1
OfcIndexMgrUpdateChunkOffset (UINT1 u1GrpID)
{
    UINT4               u4MaxIndices = OFC_ZERO;
    UINT4               u4Index = OFC_ZERO;
    tIndexMgrGrpInfo   *pIndexMgrGrpInfo = gpOfcIndexMgrGrpInfo;
    tIndexMgrChunkInfo *pIndexMgrChunkInfo = NULL;
    UINT4              *pu4AvailOffset = NULL;

    if ((u1GrpID < OFC_ONE) || (u1GrpID > MAX_INDEXMGR_GRPS))
    {
        return OFC_FAILURE;
    }

    pIndexMgrGrpInfo += (u1GrpID - OFC_ONE);
    pIndexMgrChunkInfo = pIndexMgrGrpInfo->pIndexChunkInfoTbl;
    pIndexMgrChunkInfo += pIndexMgrGrpInfo->u4AvailChunkID;

    if (MAX_INDEXMGR_INDICES_PER_CHUNK == pIndexMgrChunkInfo->u4NumKeysAlloc)
    {
        /* This chunk is full */
        pIndexMgrChunkInfo->u4CurOffset = OFC_ZERO;    /* reposition to 1st */

        u4MaxIndices = au4OfcGrpMaxIndices[u1GrpID - OFC_ONE];

        if (u4MaxIndices == pIndexMgrGrpInfo->u4TotalKeysAlloc)
        {
            /* All chunks are full */
            pIndexMgrGrpInfo->u4AvailChunkID = OFC_ZERO;
            /* Reposition to start */
            /* Nothing else needs to be done */
            return OFC_SUCCESS;
        }
        else
        {
            /* Search for a new Chunk from next chunk */
            pu4AvailOffset = pIndexMgrChunkInfo->pu4IndexChunk;
            pu4AvailOffset += pIndexMgrChunkInfo->u4CurOffset;

            for (u4Index = ((pIndexMgrGrpInfo->u4AvailChunkID) + OFC_ONE);
                 u4Index < pIndexMgrGrpInfo->u4NumChunksInGrp; u4Index++)
            {
                pIndexMgrChunkInfo++;

                if (pIndexMgrChunkInfo->u4NumKeysAlloc
                    < MAX_INDEXMGR_INDICES_PER_CHUNK)
                {
                    /* This chunk is available */
                    pIndexMgrGrpInfo->u4AvailChunkID = u4Index;
                    return OFC_SUCCESS;
                }
            }
        }
    }
    else                        /* search for an offset in this chunk itself */
    {
        pu4AvailOffset = pIndexMgrChunkInfo->pu4IndexChunk;
        pu4AvailOffset += pIndexMgrChunkInfo->u4CurOffset;

        for (u4Index = pIndexMgrChunkInfo->u4CurOffset;
             u4Index < MAX_INDEXMGR_ENT_PER_CHUNK; u4Index++, pu4AvailOffset++)
        {
            if ((*pu4AvailOffset) != OFC_ZERO)
            {
                pIndexMgrChunkInfo->u4CurOffset = u4Index;
                break;
            }
        }
    }

    return OFC_SUCCESS;
}

/*---------------------------------------------------------------------------
 * Function    : OfcIndexMgrCheckGrpFull 
 * Description : This function checks if indices are available for the 
 *               specified group. 
 * Inputs      : GroupID (1 = Ofc Flow Indices).
 *               Index value
 * Outputs     : None
 * Returns     : INDEXMGR_GRP_FULL if all indices allocated 
 *               INDEXMGR_GRP_VACANT otherwise.
---------------------------------------------------------------------------*/

UINT1
OfcIndexMgrCheckGrpFull (UINT1 u1GrpID)
{
    tIndexMgrGrpInfo   *pIndexMgrGrpInfo = gpOfcIndexMgrGrpInfo;

    /* First check if the group id  is valid */
    if ((u1GrpID < OFC_ONE) || (u1GrpID > MAX_INDEXMGR_GRPS))
    {
        OFC_FLOW_DBG (DBG_ERR_CRT, "INDEXMGR :INVALID GROUP ID \n");
        return INDEXMGR_GRP_FULL;
    }

    pIndexMgrGrpInfo += (u1GrpID - OFC_ONE);

    if (pIndexMgrGrpInfo->u4TotalKeysAlloc ==
        au4OfcGrpMaxIndices[u1GrpID - OFC_ONE])
    {
        OFC_FLOW_DBG (DBG_ERR_CRT,
                      "INDEXMGR : Index Unavailable : Group full \n");
        return INDEXMGR_GRP_FULL;
    }

    return INDEXMGR_GRP_VACANT;
}

/*---------------------------------------------------------------------------
 * Function    : OfcIndexMgrGetAvailByteBitPos
 * Description : This function inspects the UINT4 Value to find the 1st bit 
 *               position which is allocatable (1).
 * Inputs      : u4AvailIndexBitmap : U4 value to inspect in.
 * Outputs     : pu1BytePos : Shall contain position of the byte holding the
 *               1st allocatable bit pos. (0 to 3)
 *               pu1BitPos : Bit pos. of allocatable bit in the byte.(0-7)   
 * Returns     : OFC_SUCCESS if able to 
 *               0 otherwise. (should never happen)
---------------------------------------------------------------------------*/

UINT1
OfcIndexMgrGetAvailByteBitPos (UINT4 u4AvailIndexBitmap, UINT1 *pu1BytePos,
                               UINT1 *pu1BitPos)
{
    /*if no bit available(u4AvailIndexBitmap == 0), return failure */
    if (OFC_ZERO == u4AvailIndexBitmap)    /* Ideally should never occur */
    {
        return OFC_ZERO;
    }

    /* Now to check every byte to get a (bit = 1) */
    for (*pu1BytePos = OFC_ZERO; *pu1BytePos < sizeof (u4AvailIndexBitmap);
         (*pu1BytePos)++)
    {
        if (u4AvailIndexBitmap & aOfcAvlByteBmap[*pu1BytePos])
        {
            break;
        }
    }

    if (*pu1BytePos == sizeof (u4AvailIndexBitmap))
    {
        return OFC_FAILURE;
    }

    /* Now to get the bit position */
    for (*pu1BitPos = OFC_ZERO; *pu1BitPos < INDEXMGR_BYTE_BLOCK_SIZE;
         (*pu1BitPos)++)
    {
        if (u4AvailIndexBitmap &
            aOfcAvlBitmap[((*pu1BytePos) * INDEXMGR_BYTE_BLOCK_SIZE)
                          + (*pu1BitPos)])
        {
            break;
        }
    }

    return OFC_SUCCESS;
}

/*--------------------------------------------------------------------------
FAQ on Index manager Library:
1) Scope and usage of this library.
2) How do we initialise this libarary.
3) Configuration and Debug support available in this libarary.
---------------------------------------------------------------------------*/
/*                        End of file ofcidxmg.c                           */
/*-------------------------------------------------------------------------*/
#endif
