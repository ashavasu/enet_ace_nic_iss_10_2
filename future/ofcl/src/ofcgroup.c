/*****************************************************************************/
/* Copyright (C) 2013 Aricent Inc . All Rights Reserved
 * ****************************************************************************
 *    FILE  NAME             : ofcgroup.c                                      
 *    PRINCIPAL AUTHOR       : Aricent Inc.                                                  
 *    SUBSYSTEM NAME         : OFC                                              
 *    MODULE NAME            : OFC-GROUPTABLE                                  
 *    LANGUAGE               : ANSI-C                                          
 *    TARGET ENVIRONMENT     : Linux (Portable)                                                        
 *    DATE OF FIRST RELEASE  :                                                 
 *    DESCRIPTION            : This file contains routines for GROUP table     
 *                             implementations.                                                              
* $Id: ofcgroup.c,v 1.8 2018/01/11 11:18:35 siva Exp $
 *----------------------------------------------------------------------------*/
#include "ofcflow.h"
#include "ofcgroup.h"
#include "ofcsz.h"
#include "ofcapi.h"

#ifdef NPAPI_WANTED
#include "ofcnp.h"
#endif /* NPAPI_WANTED */

PRIVATE INT4        Ofc131GroupAddEntry (tOfcFsofcGroupEntry * pGrpEntry);
PRIVATE UINT4       Ofc131GroupModifyEntry (tOfcFsofcGroupEntry * pGrpEntry);
PRIVATE UINT4       Ofc131GroupDeleteEntry (tOfcFsofcGroupEntry * pGrpEntry);
PRIVATE UINT4       Ofc131GroupDeleteAllEntry (UINT4 u4ContextId);
PRIVATE VOID        Ofc131GrpMemRelease (tOfcFsofcGroupEntry * pGrpEntry);
PRIVATE VOID        Ofc131GroupDeleteFlows (tOfcFsofcGroupEntry * pGrpEntry);

/*****************************************************************************
* Function Name : Ofc131grpMemReleaseBuckets                                        
* Description   : Function to handle the Deletion of the GroupBuckets          
*                                                                             
* Input(s)      : *pBucketList - SLLto GrpActions                   
* Output(s)     : None                                                      
* Return(s)     : VOID                           
*****************************************************************************/
PUBLIC VOID
Ofc131GrpMemReleaseBuckets (tOfcSll * pBucketList)
{

    tOfcGroupBucket    *pOfcGrpBucket = NULL;
    tOfcGroupBucket    *pOfcGrpNext = NULL;
    tOfcActs           *pOfcNextActs = NULL;
    tOfcActs           *pOfcActs = NULL;
    tOfcSetFld         *pOfcSetFld = NULL;
    tOfcSetFld         *pOfcNextSet = NULL;

    OFC_TRC_FUNC ((OFC_FN_ENTRY, "FUNC:Ofc131GrpMemReleaseBuckets\n"));

    TMO_DYN_SLL_Scan (pBucketList, pOfcGrpBucket, pOfcGrpNext,
                      tOfcGroupBucket *)
    {
        TMO_DYN_SLL_Scan (&pOfcGrpBucket->ActList, pOfcActs, pOfcNextActs,
                          tOfcActs *)
        {
            if (pOfcActs->u2Type == OFCAT_SET_FIELD)
            {
                TMO_DYN_SLL_Scan (&pOfcActs->Acts.SetFld.SetFldList, pOfcSetFld,
                                  pOfcNextSet, tOfcSetFld *)
                {
                    TMO_SLL_Delete (&pOfcActs->Acts.SetFld.SetFldList,
                                    (tTMO_SLL_NODE *) pOfcSetFld);
                    OfcMemFreeForSetField (OFC_ZERO, (UINT1 *) pOfcSetFld);
                    if (NULL == pOfcNextSet)
                    {
                        break;
                    }
                }
            }
            TMO_SLL_Delete (&pOfcGrpBucket->ActList,
                            (tTMO_SLL_NODE *) pOfcActs);
            OfcMemFreeForAction (OFC_ZERO, (UINT1 *) pOfcActs);
            if (NULL == pOfcNextActs)
            {
                break;
            }
        }
        TMO_SLL_Delete (pBucketList, (tTMO_SLL_NODE *) pOfcGrpBucket);
        MemReleaseMemBlock (OFC_ACTION_BUCKET_POOLID, (UINT1 *) pOfcGrpBucket);
        if (NULL == pOfcGrpNext)
        {
            break;
        }
    }
    OFC_TRC_FUNC ((OFC_FN_EXIT, "FUNC:Ofc131GrpMemReleaseBuckets\n"));
}

/*****************************************************************************
* Function Name : Ofc131GrpMemRelease                                        
* Description   : Function to handle deletion of Grouptable          
*                                                                             
* Input(s)      : *pOfcGrpEntry -Pointer to the GroupTable                   
* Output(s)     : None                                                      
* Return(s)     : VOID                  
*****************************************************************************/
PRIVATE VOID
Ofc131GrpMemRelease (tOfcFsofcGroupEntry * pGrpEntry)
{

    OFC_TRC_FUNC ((OFC_FN_ENTRY, "FUNC:Ofc131GrpMemRelease\n"));

    Ofc131GrpMemReleaseBuckets (&pGrpEntry->BucketList);
    MemReleaseMemBlock (OFC_GROUP_ENTRY_POOLID, (UINT1 *) pGrpEntry);

    OFC_TRC_FUNC ((OFC_FN_EXIT, "FUNC:Ofc131GrpMemRelease\n"));
}

/*****************************************************************************
* Function Name : Ofc131GroupHandleGroupModMsgs                                        
* Description   : Function to handle the GroupModify request messages          
*                                                                             
* Input(s)      : *pOfcGroupMod - Open Flow Client Instance                   
* Output(s)     : None                                                      
* Return(s)     : OFC_FAILURE/OFC_SUCCESS                                   
*****************************************************************************/
PUBLIC VOID
Ofc131GroupHandleGroupModMsgs (tOfcFsofcCfgEntry * pOfc,
                               tOfcFsofcGroupEntry * pGrpEntry, UINT2 u2cmd)
{
    tOfpErrMsg          OfpErrorMsg;

    OFC_TRC_FUNC ((OFC_FN_ENTRY, "\r\nFUNC:Ofc131GroupHandleGroupModMsgs\n"));

    UNUSED_PARAM (pOfc);
    MEMSET (&OfpErrorMsg, OFC_ZERO, sizeof (tOfpErrMsg));

    switch (u2cmd)
    {
        case OFCGC_ADD:
            Ofc131GroupAddEntry (pGrpEntry);
            break;
        case OFCGC_MODIFY:
            Ofc131GroupModifyEntry (pGrpEntry);
            break;
        case OFCGC_DELETE:
            Ofc131GroupDeleteEntry (pGrpEntry);
            Ofc131GrpMemRelease (pGrpEntry);
            break;
        default:
            OFC_TRC_FUNC ((OFC_GRPTBL_TRC,
                           "\r\nFUNC:Ofc131GroupHandleGroupModMsgs:"
                           "Default Sending Error with bad command option,Returning FAILURE\n"));
            OfpErrorMsg.u2Type = OFPET_131_GROUP_MOD_FAILED;
            OfpErrorMsg.u2Code = ISS_OFPGMFC_BAD_COMMAND;
            OfcErrMsgPktSend (&OfpErrorMsg, NULL);
            Ofc131GrpMemRelease (pGrpEntry);
            break;
    }
    OFC_TRC_FUNC ((OFC_FN_EXIT, "\r\nFUNC:Ofc131GroupHandleGroupModMsgs\n"));
}

/*****************************************************************************
* Function Name : ofc131GroupAddEntry                                     
* Description   : Function to handle the GroupAdd messages           
*                                                                           
* Input(s)      : *pOfcGroupMod - Open Flow Client Instance                 
* Output(s)     : None                                                      
* Return(s)     : OFC_FAILURE/OFC_SUCCESS                                   
*****************************************************************************/
PRIVATE INT4
Ofc131GroupAddEntry (tOfcFsofcGroupEntry * pGrpEntry)
{
    tOfcFsofcGroupEntry OfcFsofcGroupEntry;
    tOfcFsofcGroupEntry *pOfcFsofcGroupEntry = &OfcFsofcGroupEntry;
    tOfpErrMsg          OfpErrorMsg;
    CHR1                au1ActionString[OFC_MAX_ACTION_STR_LEN];
    INT4                i4RetValue = OFC_ZERO;

#ifdef NPAPI_WANTED
    tOfcHwInfo          OfcHwInfo;
    MEMSET (&OfcHwInfo, OFC_ZERO, sizeof (tOfcHwInfo));
#endif

    OFC_TRC_FUNC ((OFC_FN_ENTRY, "FUNC:Ofc131GroupAddEntry\n"));

    MEMSET (&OfpErrorMsg, OFC_ZERO, sizeof (tOfpErrMsg));
    MEMSET (&au1ActionString, OFC_ZERO, OFC_MAX_ACTION_STR_LEN);
    MEMSET (&OfcFsofcGroupEntry, OFC_ZERO, sizeof (tOfcFsofcGroupEntry));

    pOfcFsofcGroupEntry->MibObject.u4FsofcGroupIndex =
        pGrpEntry->MibObject.u4FsofcGroupIndex;
    pOfcFsofcGroupEntry->MibObject.u4FsofcContextId =
        pGrpEntry->MibObject.u4FsofcContextId;

    i4RetValue = OfcGetAllFsofcGroupTable (pOfcFsofcGroupEntry);
    if (OSIX_SUCCESS == i4RetValue)
    {
        OfpErrorMsg.u2Type = OFPET_131_GROUP_MOD_FAILED;
        OfpErrorMsg.u2Code = ISS_OFPGMFC_GROUP_EXISTS;
        i4RetValue = OfcErrMsgPktSend (&OfpErrorMsg, NULL);
        OFC_TRC_FUNC ((OFC_GRPTBL_TRC,
                       "FUNC:Ofc131GroupAddEntry: Send Error Message to Controller Success, through Pkt Tx/Rx API\n"));

        Ofc131GrpMemRelease (pGrpEntry);
        return OFC_FAILURE;
    }

    i4RetValue =
        Ofc131GroupActionStructToStr (au1ActionString,
                                      &(pGrpEntry->BucketList));
    if (OFC_FAILURE == i4RetValue)
    {
        OFC_TRC_FUNC ((OFC_GRPTBL_TRC,
                       "FUNC:ofcGroupAddEntry:Action values corrupted"));
        OfpErrorMsg.u2Type = OFPET_131_BAD_ACTION;
        OfpErrorMsg.u2Code = OFPBAC_BAD_TYPE;
        i4RetValue = OfcErrMsgPktSend (&OfpErrorMsg, NULL);
        Ofc131GrpMemRelease (pGrpEntry);
        return OFC_FAILURE;
    }

    MEMCPY (&(pGrpEntry->MibObject.au1FsofcGroupActionBuckets), au1ActionString,
            OFC_MAX_ACTION_STR_LEN);
    pGrpEntry->MibObject.i4FsofcGroupActionBucketsLen = OFC_MAX_ACTION_STR_LEN;
    pGrpEntry->MibObject.u4FsofcGroupDurationSec = OsixGetSysUpTime ();

    if (RBTreeAdd
        (gOfcGlobals.OfcGlbMib.FsofcGroupTable,
         (tRBElem *) pGrpEntry) != RB_SUCCESS)
    {
        OFC_TRC_FUNC ((OFC_GRPTBL_TRC,
                       "FUNC:Ofc131GroupAddEntry: Error in updating the RBTRee\n"));
        OfpErrorMsg.u2Type = OFPET_131_GROUP_MOD_FAILED;
        OfpErrorMsg.u2Code = ISS_OFPGMFC_OUT_OF_GROUPS;
        i4RetValue = OfcErrMsgPktSend (&OfpErrorMsg, NULL);
        Ofc131GrpMemRelease (pGrpEntry);
        OFC_TRC_FUNC ((OFC_GRPTBL_TRC,
                       "FUNC:Ofc131GroupAddEntry: Send Error Message to Controller Success, through Pkt Tx/Rx API\n"));
        return OFC_FAILURE;
    }

    OFC_TRC_FUNC ((OFC_GRPTBL_TRC,
                   "FUNC:Ofc131GroupAddEntry: Invoking NPAPI for ADD\n"));
#ifdef NPAPI_WANTED
    OfcHwInfo.OfcCmd = OFC_NP_GROUP_ADD;
    OfcHwInfo.OfcVer = OFC_VER_131;
    OfcHwInfo.OfcHwEntry.OfcHwGroupInfo.u4FsofcGroupIndex =
        pGrpEntry->MibObject.u4FsofcGroupIndex;
    OfcHwInfo.OfcHwEntry.OfcHwGroupInfo.u4FsofcGroupDurationSec =
        pGrpEntry->MibObject.u4FsofcGroupDurationSec;
    OfcHwInfo.OfcHwEntry.OfcHwGroupInfo.pBucketList = &pGrpEntry->BucketList;

    i4RetValue = OfcFsNpHwConfigOfcInfo (&OfcHwInfo);
    if (i4RetValue != FNP_SUCCESS)
    {
        OFC_TRC_FUNC ((OFC_GRPTBL_TRC,
                       "FUNC:OfcFsNpHwConfigOfcInfo: NPAPI Failed in ADD\n"));
        return OFC_FAILURE;
    }
#endif /* NPAPI_WANTED */

    OFC_TRC_FUNC ((OFC_FN_EXIT, "FUNC:Ofc131GroupAddEntry\n"));
    return OFC_SUCCESS;
}

/*****************************************************************************
* Function Name : Ofc131GroupModifyEntry                                      
* Description   : Function to handle the GroupModify messages           
*                                                                           
* Input(s)      : pGrpEntry->pointer to the GrpEntry            
*                           
* Output(s)     : None                                                      
* Return(s)     : OFC_FAILURE/OFC_SUCCESS                                   
*****************************************************************************/
PRIVATE UINT4
Ofc131GroupModifyEntry (tOfcFsofcGroupEntry * pGrpEntry)
{
    tOfcFsofcGroupEntry OfcFsofcGroupEntry;
    tOfpErrMsg          OfpErrorMsg;
    tOfcFsofcGroupEntry *pOfcFsofcGrpEntry = NULL;
    CHR1                au1ActionString[OFC_MAX_ACTION_STR_LEN];
    INT4                i4RetValue = OFC_ZERO;

    OFC_TRC_FUNC ((OFC_FN_ENTRY, "FUNC:Ofc131GroupModifyEntry\n"));

    MEMSET (&OfpErrorMsg, OFC_ZERO, sizeof (tOfpErrMsg));

    MEMSET (&OfcFsofcGroupEntry, OFC_ZERO, sizeof (tOfcFsofcGroupEntry));
    MEMSET (&au1ActionString, OFC_ZERO, sizeof (au1ActionString));

    OfcFsofcGroupEntry.MibObject.u4FsofcContextId =
        pGrpEntry->MibObject.u4FsofcContextId;
    OfcFsofcGroupEntry.MibObject.u4FsofcGroupIndex =
        pGrpEntry->MibObject.u4FsofcGroupIndex;

    pOfcFsofcGrpEntry = RBTreeGet (gOfcGlobals.OfcGlbMib.FsofcGroupTable,
                                   (tRBElem *) & (OfcFsofcGroupEntry));
    if (NULL == pOfcFsofcGrpEntry)
    {
        OFC_TRC_FUNC ((OFC_GRPTBL_TRC,
                       "FUNC:OfcGroupModifyEntry:Group Does not Exists\n"));
        OfpErrorMsg.u2Type = OFPET_131_GROUP_MOD_FAILED;
        OfpErrorMsg.u2Code = ISS_OFPGMFC_UNKNOWN_GROUP;
        i4RetValue = OfcErrMsgPktSend (&OfpErrorMsg, NULL);

        /* Releasing the Memory */

        Ofc131GrpMemRelease (pGrpEntry);
        return OFC_FAILURE;
    }
    else
    {
        OFC_TRC_FUNC ((OFC_GRPTBL_TRC, "FUNC:OfcGroupModifyEntry: "
                       "Deleting the previously existing the entry\n"));
        Ofc131GroupDeleteEntry (pOfcFsofcGrpEntry);
    }

    i4RetValue = Ofc131GroupAddEntry (pGrpEntry);
    if (OFC_FAILURE == i4RetValue)
    {
        OfpErrorMsg.u2Type = OFPET_131_GROUP_MOD_FAILED;
        OfpErrorMsg.u2Code = ISS_OFPGMFC_UNKNOWN_GROUP;
        i4RetValue = OfcErrMsgPktSend (&OfpErrorMsg, NULL);
        OFC_TRC_FUNC ((OFC_GRPTBL_TRC,
                       "FUNC:ofcGroupModifyEntry:updating in the RBTree Fails\n"));
        return OFC_FAILURE;
    }

    OFC_TRC_FUNC ((OFC_FN_EXIT, "FUNC:Ofc131GroupModifyEntry\n"));
    return OFC_SUCCESS;
}

/*****************************************************************************
* Function Name : ofc131GroupDeleteEntry                                         
* Description   : Function to handle the GroupDelete messages        
*                                                                          
* Input(s)      : *pOfcGroupMod - GroupEntry Message         
*            *pofc       - OpenFlow client instance           
* Output(s)     : None                                                     
* Return(s)     : OFC_SUCCESS                                   
*****************************************************************************/
PRIVATE UINT4
Ofc131GroupDeleteEntry (tOfcFsofcGroupEntry * pGrpEntry)
{
    tOfcFsofcGroupEntry *pOfcFsofcGroupEntry = NULL;
    tOfcFsofcGroupEntry OfcFsofcGroupEntry;

#ifdef NPAPI_WANTED
    tOfcHwInfo          OfcHwInfo;
    INT4                i4RetValue = OFC_ZERO;

    MEMSET (&OfcHwInfo, OFC_ZERO, sizeof (tOfcHwInfo));
#endif

    OFC_TRC_FUNC ((OFC_FN_ENTRY, "FUNC:Ofc131GroupDeleteEntry\n"));

    if (OFCG_ALL == pGrpEntry->MibObject.u4FsofcGroupIndex)
    {
        Ofc131GroupDeleteAllEntry (pGrpEntry->MibObject.u4FsofcContextId);
        OFC_TRC_FUNC ((OFC_FN_EXIT, "FUNC:Ofc131GroupDeleteEntry\n"));
        return OFC_SUCCESS;
    }
    else
    {
        OfcFsofcGroupEntry.MibObject.u4FsofcContextId =
            pGrpEntry->MibObject.u4FsofcContextId;
        OfcFsofcGroupEntry.MibObject.u4FsofcGroupIndex =
            pGrpEntry->MibObject.u4FsofcGroupIndex;

        pOfcFsofcGroupEntry = RBTreeGet (gOfcGlobals.OfcGlbMib.FsofcGroupTable,
                                         (tRBElem *) & (OfcFsofcGroupEntry));
        if (NULL != pOfcFsofcGroupEntry)
        {
#ifdef NPAPI_WANTED
            OfcHwInfo.OfcVer = OFC_VER_131;
            OfcHwInfo.OfcCmd = OFC_NP_GROUP_DELETE;
            OfcHwInfo.OfcHwEntry.OfcHwGroupInfo.u4FsofcGroupIndex =
                pOfcFsofcGroupEntry->MibObject.u4FsofcGroupIndex;

            i4RetValue = OfcFsNpHwConfigOfcInfo (&OfcHwInfo);
            if (i4RetValue != FNP_SUCCESS)
            {
                OFC_TRC_FUNC ((OFC_GRPTBL_TRC,
                               "FUNC:OfcFsNpHwConfigOfcInfo: NPAPI Failed in Delete\n"));
                return OFC_FAILURE;
            }
#endif /* NPAPI_WANTED */

            /* Call the routine to delete the flows whichever has Group Action's Index maps,
               to the GROUP_MOD message Index */
            Ofc131GroupDeleteFlows (&OfcFsofcGroupEntry);
            RBTreeRem (gOfcGlobals.OfcGlbMib.FsofcGroupTable,
                       pOfcFsofcGroupEntry);
            Ofc131GrpMemRelease (pOfcFsofcGroupEntry);

            OFC_TRC_FUNC ((OFC_GRPTBL_TRC,
                           "FUNC:OfcGroupDeleteEntry: Invoking NPAPI for DELETE\n"));
        }
        OFC_TRC_FUNC ((OFC_FN_EXIT, "FUNC:Ofc131GroupDeleteEntry\n"));
        return OFC_SUCCESS;
    }
}

/*****************************************************************************
 * * Function Name : Ofc131GroupDeleteAllEntry
 * * Description   : Function to handle the GroupDelete messages
 * *
 * * Input(s)      : PGrpEntry->pointe to the groupMsgs ,
 * *                          
 * * Output(s)     : None
 * * Return(s)     : OFC_SUCCESS/OFC_FAILURE
 * **************************************************************************/
PRIVATE UINT4
Ofc131GroupDeleteAllEntry (UINT4 u4ContextId)
{
    tOfcFsofcGroupEntry *pOfcCurGrpEntry = NULL;
    tOfcFsofcGroupEntry OfcFsofcGrpEntry;

#ifdef NPAPI_WANTED
    INT4                i4RetValue = OFC_ZERO;
    tOfcHwInfo          OfcHwInfo;

    MEMSET (&OfcHwInfo, OFC_ZERO, sizeof (tOfcHwInfo));
#endif

    OFC_TRC_FUNC ((OFC_FN_ENTRY, "FUNC:Ofc131GroupDeleteAllEntry\n"));

    MEMSET (&OfcFsofcGrpEntry, OFC_ZERO, sizeof (OfcFsofcGrpEntry));

    OfcFsofcGrpEntry.MibObject.u4FsofcContextId = u4ContextId;
    OfcFsofcGrpEntry.MibObject.u4FsofcGroupIndex = OFCG_ALL;

    /* Call the routine to delete the flows whichever has Group Action  */

    Ofc131GroupDeleteFlows (&OfcFsofcGrpEntry);

    while ((pOfcCurGrpEntry = OfcGetFirstFsofcGroupTable ()) != NULL)
    {
#ifdef NPAPI_WANTED
        OfcHwInfo.OfcVer = OFC_VER_131;
        OfcHwInfo.OfcCmd = OFC_NP_GROUP_DELETE;
        OfcHwInfo.OfcHwEntry.OfcHwGroupInfo.u4FsofcGroupIndex =
            pOfcCurGrpEntry->MibObject.u4FsofcGroupIndex;

        i4RetValue = OfcFsNpHwConfigOfcInfo (&OfcHwInfo);
        if (i4RetValue != FNP_SUCCESS)
        {
            OFC_TRC_FUNC ((OFC_GRPTBL_TRC,
                           "FUNC:OfcFsNpHwConfigOfcInfo: NPAPI Failed in Delete\n"));
            return OFC_FAILURE;
        }
#endif /* NPAPI_WANTED */
        RBTreeRem (gOfcGlobals.OfcGlbMib.FsofcGroupTable, pOfcCurGrpEntry);
        Ofc131GrpMemRelease (pOfcCurGrpEntry);
    }

    OFC_TRC_FUNC ((OFC_FN_EXIT, "FUNC:Ofc131GroupDeleteAllEntry\n"));
    return OFC_SUCCESS;
}

/*****************************************************************************
 * * Function Name : Ofc131GroupDeleteAllEntry
 * * Description   : Function to handle the GroupDelete messages
 * *
 * * Input(s)      : PGrpEntry->pointe to the groupMsgs ,
 * *
 * * Output(s)     : None
 * * Return(s)     : OFC_SUCCESS/OFC_FAILURE
 * **************************************************************************/
PRIVATE VOID
Ofc131GroupDeleteFlows (tOfcFsofcGroupEntry * pGrpEntry)
{
    tOfcFsofcFlowEntry *pCurFlowEntry = NULL;
    tOfcFsofcCfgEntry  *pOfcFsofcCfgEntry = NULL;

    OFC_TRC_FUNC ((OFC_FN_ENTRY, "FUNC:Ofc131GroupDeleteFlows\n"));

    pCurFlowEntry = (tOfcFsofcFlowEntry *)
        RBTreeGetFirst (gOfcGlobals.OfcGlbMib.FsofcFlowTable);

    pOfcFsofcCfgEntry =
        OfcGetFsofcCfgEntry (pGrpEntry->MibObject.u4FsofcContextId);
    if (pOfcFsofcCfgEntry == NULL)
    {
        OFC_TRC_FUNC ((OFC_GRPTBL_TRC, "\r\nOfc131GroupDeleteFlows: "
                       "Get CFG Entry Failed\n"));
        return;
    }

    while (NULL != pCurFlowEntry)
    {
        if (pCurFlowEntry->u4OutGrp == pGrpEntry->MibObject.u4FsofcGroupIndex ||
            pGrpEntry->MibObject.u4FsofcGroupIndex == OFCG_ALL)
        {
            /* 
             * Delete the flow entries from the flowtable ,
             * whichever has Group Action index similiar to the GROUP_MOD
             * message's GroupIndex
             */
            Ofc131FlowDeleteFlow (pOfcFsofcCfgEntry, pCurFlowEntry,
                                  OFPRR_GROUP_DELETE);
        }
        pCurFlowEntry = (tOfcFsofcFlowEntry *)
            RBTreeGetNext (gOfcGlobals.OfcGlbMib.FsofcFlowTable,
                           (tRBElem *) pCurFlowEntry, NULL);
    }                            /* End of While loop */

    OFC_TRC_FUNC ((OFC_FN_EXIT, "FUNC:Ofc131GroupDeleteFlows\n"));
}
