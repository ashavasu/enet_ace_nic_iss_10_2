
/********************************************************************
* Copyright (C) 2013 Aricent Inc . All Rights Reserved
*
* $Id: ofcshcli.c,v 1.6 2014/03/15 14:29:47 siva Exp $
*
* Description: This file contains the Ofc show commands related 
*              routines
*********************************************************************/

#ifndef __OFCSHCLI_C__
#define __OFCSHCLI_C__

#include "ofcinc.h"
#include "ofccli.h"
#include "ofcvlan.h"
#include "ofcapi.h"
#define OFC_MAX_IP_LEN 20
#define OFC_MAX_MAC_LEN 20
#define OFC_MAX_COUNT_LEN 64
/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : OfcCliLocalIpToString                              */
/*                                                                           */
/*     DESCRIPTION      : This function converts given IP address to string  */
/*                                                                           */
/*     INPUT            : pu1IpAddr - Pointer to IP Address                  */
/*                      : pu1String - Pointer to copy in string form         */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*                                                                           */
/*     RETURNS          : NONE                                               */
/*                                                                           */
/*****************************************************************************/
PRIVATE VOID
OfcCliLocalIpToString (UINT1 *pu1String, UINT1 *pu1IpAddr)
{
    sprintf ((CHR1 *) pu1String, "%d.%d.%d.%d",
             pu1IpAddr[3], pu1IpAddr[2], pu1IpAddr[1], pu1IpAddr[0]);
}

/****************************************************************************
 * Function    :  cli_process_Ofc_show_cmd
 * Description :  This function is exported to CLI module to handle the
                OFC cli show commands to take the corresponding action.

 * Input       :  Variable arguments

 * Output      :  None 

 * Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
cli_process_Ofc_show_cmd (tCliHandle CliHandle, UINT4 u4Command, ...)
{
    va_list             ap;
    UINT4              *args[OFC_CLI_MAX_ARGS];
    INT1                i1Argno = OFC_ZERO;
    UINT4               u4ErrCode = OFC_ZERO;
    UINT4               u4IfIndex = OFC_ZERO;
    UINT4               u4RetStatus = CLI_SUCCESS;
    UINT4               u4CmdType = OFC_ZERO;
    INT4                i4Inst;

    UNUSED_PARAM (u4CmdType);
    MEMSET (&args, OFC_ZERO, OFC_CLI_MAX_ARGS);
    va_start (ap, u4Command);

    i4Inst = va_arg (ap, INT4);

    if (i4Inst != 0)
    {
        u4IfIndex = (UINT4) i4Inst;
    }
    UNUSED_PARAM (u4IfIndex);
    while (1)
    {
        args[i1Argno++] = va_arg (ap, UINT4 *);
        if (i1Argno == OFC_CLI_MAX_ARGS)
            break;
    }

    va_end (ap);
    switch (u4Command)
    {
        case CLI_OFC_SHOW_INTERFACE_INFO:
            OfcCliShowFsofcIfTable (CliHandle, ((UINT4 *) args[0]),
                                    ((INT4 *) args[1]));
            break;
        case CLI_OFC_SHOW_CLIENTCFG_INFO:
            OfcCliShowFsofcCfgTable (CliHandle, ((UINT4 *) args[0]));
            break;
        case CLI_OFC_SHOW_CNTRLRCFG_INFO:
            OfcCliShowFsofcControllerConnTable (CliHandle, ((UINT4 *) args[0]));
            break;
        case CLI_OFC_SHOW_FLOWS:
            OfcCliShowFsofcFlowTable (CliHandle, ((UINT4 *) args[0]),
                                      ((UINT4 *) args[1]), ((UINT4 *) args[2]));
            break;
        case CLI_OFC_SHOW_GROUPS:
            OfcCliShowFsofcGroupTable (CliHandle, ((UINT4 *) args[0]),
                                       ((UINT4 *) args[1]));
            break;
        case CLI_OFC_SHOW_METERS:
            OfcCliShowFsofcMeterTable (CliHandle, ((UINT4 *) args[0]),
                                       ((UINT4 *) args[1]));
            break;
        default:
            break;
    }
    if ((u4RetStatus == CLI_FAILURE)
        && (CLI_GET_ERR (&u4ErrCode) == CLI_SUCCESS))
    {
        if ((u4ErrCode > 0) && (u4ErrCode < CLI_OFC_MAX_ERR))
        {
            CliPrintf (CliHandle, "\r%s", OfcCliErrString[u4ErrCode]);
        }
        CLI_SET_ERR (0);
    }
    CLI_SET_CMD_STATUS (u4RetStatus);

    return (INT4) u4RetStatus;

}

/****************************************************************************
 Function    :  OfcCliShowFsofcIfTable
 Input       :            
 Description :  This Routine Take the Indices &
                Gets the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
OfcCliShowFsofcIfTable (tCliHandle CliHandle, UINT4 *pu4FsofcContextId,
                        INT4 *pi4FsofcIfIndex)
{
    tSNMP_OCTET_STRING_TYPE EgressPorts;
    tSNMP_OCTET_STRING_TYPE UntaggedPorts;
    UINT4               u4PagingStatus = CLI_SUCCESS;
    UINT1              *pau1EgressPortList = NULL;
    UINT1              *pau1UntaggedPortList = NULL;
    INT4                i4RetVal = SNMP_FAILURE;
    tOfcFsofcIfEntry    OfcFsofcIfEntry;
    INT4                i4FsofcIfIndex = OFC_ZERO;
    INT4                i4NextFsofcIfIndex = OFC_ZERO;
    UINT4               u4FsofcContextId = OFC_ZERO;
    UINT4               u4NextFsofcContextId = OFC_ZERO;

    MEMSET (&EgressPorts, OFC_ZERO, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&UntaggedPorts, OFC_ZERO, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&OfcFsofcIfEntry, OFC_ZERO, sizeof (tOfcFsofcIfEntry));

    if ((pu4FsofcContextId == NULL) && (pi4FsofcIfIndex == NULL))

    {
        i4RetVal =
            nmhGetFirstIndexFsofcIfTable (&u4FsofcContextId, &i4FsofcIfIndex);
        while (SNMP_SUCCESS == i4RetVal)
        {
            OfcFsofcIfEntry.MibObject.u4FsofcContextId = u4FsofcContextId;

            OfcFsofcIfEntry.MibObject.i4FsofcIfIndex = i4FsofcIfIndex;

            if (OfcGetAllFsofcIfTable (&OfcFsofcIfEntry) == OSIX_SUCCESS)
            {
                CliPrintf (CliHandle, "Openflow-Client Interface Info\r\n");
                CliPrintf (CliHandle,
                           "-----------------------------------------------------------------------------------\r\n");
                CliPrintf (CliHandle,
                           "CxtID %2d InterfaceID %3d IfType %2d IfAlias %8s Oper-Status %2d IfCxtID %2d\r\n",
                           OfcFsofcIfEntry.MibObject.u4FsofcContextId,
                           OfcFsofcIfEntry.MibObject.i4FsofcIfIndex,
                           OfcFsofcIfEntry.MibObject.i4FsofcIfType,
                           OfcFsofcIfEntry.MibObject.au1FsofcIfAlias,
                           OfcFsofcIfEntry.MibObject.i4FsofcIfOperStatus,
                           OfcFsofcIfEntry.MibObject.u4FsofcIfContextId);
                if (STRSTR
                    (OfcFsofcIfEntry.MibObject.au1FsofcIfAlias,
                     OPENFLOW_VLAN_ALIAS_PREFIX) != NULL)
                {
                    pau1EgressPortList =
                        UtilPlstAllocLocalPortList (OFC_VLAN_PORTS_LEN);
                    if (pau1EgressPortList == NULL)
                    {
                        OFC_TRC ((OFC_TASK_TRC,
                                  "OfcCliShowFsofcIfTable: Error in allocating memory for pau1OldEgressPortList\r\n"));

                        return OSIX_FAILURE;
                    }
                    MEMSET (pau1EgressPortList, 0, OFC_VLAN_PORTS_LEN);

                    pau1UntaggedPortList =
                        UtilPlstAllocLocalPortList (OFC_VLAN_PORTS_LEN);
                    if (pau1UntaggedPortList == NULL)
                    {
                        OFC_TRC ((OFC_TASK_TRC,
                                  "OfcCliShowFsofcIfTable: Error in allocating memory "
                                  "for pau1OldUntaggedPortList\r\n"));
                        UtilPlstReleaseLocalPortList (pau1EgressPortList);
                        return OSIX_FAILURE;
                    }
                    MEMSET (pau1UntaggedPortList, 0, OFC_VLAN_PORTS_LEN);
                    EgressPorts.i4_Length = OFC_VLAN_PORTS_LEN;
                    EgressPorts.pu1_OctetList = pau1EgressPortList;

                    UntaggedPorts.i4_Length = OFC_VLAN_PORTS_LEN;
                    UntaggedPorts.pu1_OctetList = pau1UntaggedPortList;
                    nmhGetFsofcVlanEgressPorts (OfcFsofcIfEntry.MibObject.
                                                u4FsofcContextId,
                                                OfcFsofcIfEntry.MibObject.
                                                i4FsofcIfIndex, &EgressPorts);
                    nmhGetFsofcVlanEgressPorts (OfcFsofcIfEntry.MibObject.
                                                u4FsofcContextId,
                                                OfcFsofcIfEntry.MibObject.
                                                i4FsofcIfIndex, &UntaggedPorts);
                    if (FsUtilBitListIsAllZeros
                        (EgressPorts.pu1_OctetList,
                         sizeof (tPortList)) == OSIX_TRUE)
                    {
                        CliPrintf (CliHandle,
                                   "Member Ports        : None \r\n");
                    }
                    else
                    {

                        if (CliOctetToIfName
                            (CliHandle, "Member Ports        :",
                             &EgressPorts, BRG_MAX_PHY_PLUS_LAG_INT_PORTS,
                             sizeof (tPortList), 0, &u4PagingStatus,
                             6) == CLI_FAILURE)
                        {
                            UtilPlstReleaseLocalPortList (pau1EgressPortList);
                            UtilPlstReleaseLocalPortList (pau1UntaggedPortList);
                            return CLI_FAILURE;
                        }
                    }
                    if (FsUtilBitListIsAllZeros (UntaggedPorts.pu1_OctetList,
                                                 sizeof (tPortList)) ==
                        OSIX_TRUE)
                    {
                        CliPrintf (CliHandle,
                                   "Untagged Ports        : None \r\n");
                    }
                    else
                    {

                        if (CliOctetToIfName
                            (CliHandle, "Untagged Ports        :",
                             &UntaggedPorts, BRG_MAX_PHY_PLUS_LAG_INT_PORTS,
                             sizeof (tPortList), 0, &u4PagingStatus,
                             6) == CLI_FAILURE)
                        {
                            UtilPlstReleaseLocalPortList (pau1EgressPortList);
                            UtilPlstReleaseLocalPortList (pau1UntaggedPortList);
                            return CLI_FAILURE;
                        }
                    }

                    CliPrintf (CliHandle, "VInFrames %4d VOutFrames %4d\r\n",
                               OfcFsofcIfEntry.MibObject.u4FsofcVlanInFrames,
                               OfcFsofcIfEntry.MibObject.u4FsofcVlanOutFrames);
                    UtilPlstReleaseLocalPortList (pau1EgressPortList);
                    UtilPlstReleaseLocalPortList (pau1UntaggedPortList);
                }

            }
            i4RetVal =
                nmhGetNextIndexFsofcIfTable (u4FsofcContextId,
                                             &u4NextFsofcContextId,
                                             i4FsofcIfIndex,
                                             &i4NextFsofcIfIndex);
            u4FsofcContextId = u4NextFsofcContextId;
            i4FsofcIfIndex = i4NextFsofcIfIndex;
        }
    }
    else if ((pu4FsofcContextId != NULL) && (pi4FsofcIfIndex != NULL))
    {
        OfcFsofcIfEntry.MibObject.u4FsofcContextId = *pu4FsofcContextId;

        OfcFsofcIfEntry.MibObject.i4FsofcIfIndex = *pi4FsofcIfIndex;

        if (OfcGetAllFsofcIfTable (&OfcFsofcIfEntry) == OSIX_SUCCESS)
        {
            CliPrintf (CliHandle, "Openflow-Client Interface Info\r\n");
            CliPrintf (CliHandle,
                       "-------------------------------------------------------------------------\r\n");
            CliPrintf (CliHandle,
                       "CxtID %2d InterfaceID %3d IfType %2d IfAlias %8s Oper-Status %2d IfCxtID %2d EgressPortList %8s UnTaggedPortList %8s VInFrames %4d VOutFrames %4d\r\n",
                       OfcFsofcIfEntry.MibObject.u4FsofcContextId,
                       OfcFsofcIfEntry.MibObject.i4FsofcIfIndex,
                       OfcFsofcIfEntry.MibObject.i4FsofcIfType,
                       OfcFsofcIfEntry.MibObject.au1FsofcIfAlias,
                       OfcFsofcIfEntry.MibObject.i4FsofcIfOperStatus,
                       OfcFsofcIfEntry.MibObject.u4FsofcIfContextId,
                       OfcFsofcIfEntry.MibObject.au1FsofcVlanEgressPorts,
                       OfcFsofcIfEntry.MibObject.au1FsofcVlanUntaggedPorts,
                       OfcFsofcIfEntry.MibObject.u4FsofcVlanInFrames,
                       OfcFsofcIfEntry.MibObject.u4FsofcVlanOutFrames);
        }
    }
    else
    {
        CliPrintf (CliHandle,
                   "Invalid Command.Enter all options or none of the options\r\n");
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/****************************************************************************
 Function    :  OfcCliShowFsofcCfgTable
 Input       :            
 Description :  This Routine Take the Indices &
                Gets the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
OfcCliShowFsofcCfgTable (tCliHandle CliHandle, UINT4 *pu4FsofcContextId)
{
    INT4                i4RetVal = SNMP_FAILURE;
    tOfcFsofcCfgEntry   OfcFsofcCfgEntry;
    UINT4               u4FsofcContextId = OFC_ZERO;
    UINT4               u4NextFsofcContextId = OFC_ZERO;

    MEMSET (&OfcFsofcCfgEntry, OFC_ZERO, sizeof (tOfcFsofcCfgEntry));
    if ((pu4FsofcContextId == NULL))

    {
        i4RetVal = nmhGetFirstIndexFsofcCfgTable (&u4FsofcContextId);
        while (SNMP_SUCCESS == i4RetVal)
        {
            OfcFsofcCfgEntry.MibObject.u4FsofcContextId = u4FsofcContextId;

            if (OfcGetAllFsofcCfgTable (&OfcFsofcCfgEntry) == OSIX_SUCCESS)
            {
                CliPrintf (CliHandle, "Openflow-Client Configurations\r\n");
                CliPrintf (CliHandle,
                           "--------------------------------------------------------------------\r\n");
                CliPrintf (CliHandle,
                           "CxtID %2d HBDStatus %2d SupportedVer %2d HBDMode %1d DfltFlowMiss %1d P-Buffering %1d IP-Reassemble %1d stpStatus %1d swMode %1d Conn-State %1d\r\n",
                           OfcFsofcCfgEntry.MibObject.u4FsofcContextId,
                           OfcFsofcCfgEntry.MibObject.i4FsofcModuleStatus,
                           OfcFsofcCfgEntry.MibObject.i4FsofcSupportedVersion,
                           OfcFsofcCfgEntry.MibObject.i4FsofcHybrid,
                           OfcFsofcCfgEntry.MibObject.
                           i4FsofcDefaultFlowMissBehaviour,
                           OfcFsofcCfgEntry.MibObject.
                           i4FsofcControlPktBuffering,
                           OfcFsofcCfgEntry.MibObject.i4FsofcIpReassembleStatus,
                           OfcFsofcCfgEntry.MibObject.i4FsofcPortStpStatus,
                           OfcFsofcCfgEntry.MibObject.
                           i4FsofcSwitchModeOnConnFailure,
                           OfcFsofcCfgEntry.u4OfcClientState);

            }
            i4RetVal =
                nmhGetNextIndexFsofcCfgTable (u4FsofcContextId,
                                              &u4NextFsofcContextId);
            u4FsofcContextId = u4NextFsofcContextId;
        }
    }
    else if (pu4FsofcContextId != NULL)
    {
        OfcFsofcCfgEntry.MibObject.u4FsofcContextId = *pu4FsofcContextId;

        if (OfcGetAllFsofcCfgTable (&OfcFsofcCfgEntry) == OSIX_SUCCESS)
        {
            CliPrintf (CliHandle, "Openflow-Client Configurations\r\n");
            CliPrintf (CliHandle,
                       "--------------------------------------------------------------------\r\n");
            CliPrintf (CliHandle,
                       "CxtID %2d HDBStatus %2d SupportedVer %2d HBDMode %1d DfltFlowMiss %1d P-Buffering %1d IP-Reassemble %1d stpStatus %1d swMode %1d Conn-State %1d\r\n",
                       OfcFsofcCfgEntry.MibObject.u4FsofcContextId,
                       OfcFsofcCfgEntry.MibObject.i4FsofcModuleStatus,
                       OfcFsofcCfgEntry.MibObject.i4FsofcSupportedVersion,
                       OfcFsofcCfgEntry.MibObject.i4FsofcHybrid,
                       OfcFsofcCfgEntry.MibObject.
                       i4FsofcDefaultFlowMissBehaviour,
                       OfcFsofcCfgEntry.MibObject.i4FsofcControlPktBuffering,
                       OfcFsofcCfgEntry.MibObject.i4FsofcIpReassembleStatus,
                       OfcFsofcCfgEntry.MibObject.i4FsofcPortStpStatus,
                       OfcFsofcCfgEntry.MibObject.
                       i4FsofcSwitchModeOnConnFailure,
                       OfcFsofcCfgEntry.u4OfcClientState);

        }
    }
    return CLI_SUCCESS;
}

/****************************************************************************
 Function    :  OfcCliShowFsofcControllerConnTable
 Input       :            
 Description :  This Routine Take the Indices &
                Gets the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
OfcCliShowFsofcControllerConnTable (tCliHandle CliHandle,
                                    UINT4 *pu4FsofcContextId)
{
    INT4                i4RetVal = SNMP_FAILURE;
    tOfcFsofcControllerConnEntry OfcFsofcControllerConnEntry;
    INT4                i4FsofcControllerIpAddrType = OFC_ZERO;
    INT4                i4NextFsofcControllerIpAddrType = OFC_ZERO;
    tSNMP_OCTET_STRING_TYPE FsofcControllerIpAddressSrc;
    tSNMP_OCTET_STRING_TYPE NextFsofcControllerIpAddress;
    UINT1               au1FsofcControllerIpAddress[128];
    UINT1               au1NextFsofcControllerIpAddress[128];
    INT4                i4FsofcControllerConnAuxId = OFC_ZERO;
    INT4                i4NextFsofcControllerConnAuxId = OFC_ZERO;
    UINT4               u4FsofcContextId = OFC_ZERO;
    UINT4               u4NextFsofcContextId = OFC_ZERO;
    UINT1               au1Ip[OFC_MAX_IP_LEN];

    MEMSET (&au1Ip, OFC_ZERO, OFC_MAX_IP_LEN);
    MEMSET (&OfcFsofcControllerConnEntry, OFC_ZERO,
            sizeof (tOfcFsofcControllerConnEntry));
    MEMSET (&FsofcControllerIpAddressSrc, OFC_ZERO,
            sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&NextFsofcControllerIpAddress, OFC_ZERO,
            sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1FsofcControllerIpAddress, OFC_ZERO,
            sizeof (au1FsofcControllerIpAddress));
    MEMSET (au1NextFsofcControllerIpAddress, OFC_ZERO,
            sizeof (au1NextFsofcControllerIpAddress));

    FsofcControllerIpAddressSrc.pu1_OctetList = au1FsofcControllerIpAddress;
    NextFsofcControllerIpAddress.pu1_OctetList =
        au1NextFsofcControllerIpAddress;
    FsofcControllerIpAddressSrc.i4_Length = 128;
    NextFsofcControllerIpAddress.i4_Length = 128;

    if ((pu4FsofcContextId == NULL))

    {
        i4RetVal =
            nmhGetFirstIndexFsofcControllerConnTable (&u4FsofcContextId,
                                                      &i4FsofcControllerIpAddrType,
                                                      &FsofcControllerIpAddressSrc,
                                                      &i4FsofcControllerConnAuxId);
        while (SNMP_SUCCESS == i4RetVal)
        {
            OfcFsofcControllerConnEntry.MibObject.u4FsofcContextId =
                u4FsofcContextId;

            OfcFsofcControllerConnEntry.MibObject.i4FsofcControllerIpAddrType =
                i4FsofcControllerIpAddrType;

            MEMCPY (OfcFsofcControllerConnEntry.MibObject.
                    au1FsofcControllerIpAddress,
                    FsofcControllerIpAddressSrc.pu1_OctetList,
                    FsofcControllerIpAddressSrc.i4_Length);

            OfcFsofcControllerConnEntry.MibObject.
                i4FsofcControllerIpAddressLen =
                FsofcControllerIpAddressSrc.i4_Length;

            OfcFsofcControllerConnEntry.MibObject.i4FsofcControllerConnAuxId =
                i4FsofcControllerConnAuxId;

            if (OfcGetAllFsofcControllerConnTable (&OfcFsofcControllerConnEntry)
                == OSIX_SUCCESS)
            {
                MEMSET (au1Ip, OFC_ZERO, OFC_MAX_IP_LEN);
                OfcCliLocalIpToString (au1Ip,
                                       OfcFsofcControllerConnEntry.MibObject.
                                       au1FsofcControllerIpAddress);
                CliPrintf (CliHandle,
                           "Openflow-Client Controller Configurations\r\n");
                CliPrintf (CliHandle,
                           "--------------------------------------------------------------------------\r\n");
                CliPrintf (CliHandle,
                           "CxtID %2d IpAddr %16s AuxID %2d Port %5d Proto %2d Role %2d Band %2d State %2d\r\n",
                           OfcFsofcControllerConnEntry.MibObject.
                           u4FsofcContextId, au1Ip,
                           OfcFsofcControllerConnEntry.MibObject.
                           i4FsofcControllerConnAuxId,
                           OfcFsofcControllerConnEntry.MibObject.
                           i4FsofcControllerConnPort,
                           OfcFsofcControllerConnEntry.MibObject.
                           i4FsofcControllerConnProtocol,
                           OfcFsofcControllerConnEntry.MibObject.
                           i4FsofcControllerRole,
                           OfcFsofcControllerConnEntry.MibObject.
                           i4FsofcControllerConnBand,
                           OfcFsofcControllerConnEntry.MibObject.
                           i4FsofcControllerConnState);

            }
            i4RetVal =
                nmhGetNextIndexFsofcControllerConnTable (u4FsofcContextId,
                                                         &u4NextFsofcContextId,
                                                         i4FsofcControllerIpAddrType,
                                                         &i4NextFsofcControllerIpAddrType,
                                                         &FsofcControllerIpAddressSrc,
                                                         &NextFsofcControllerIpAddress,
                                                         i4FsofcControllerConnAuxId,
                                                         &i4NextFsofcControllerConnAuxId);
            u4FsofcContextId = u4NextFsofcContextId;
            FsofcControllerIpAddressSrc = NextFsofcControllerIpAddress;
            i4FsofcControllerConnAuxId = i4NextFsofcControllerConnAuxId;
        }
    }
    else if (pu4FsofcContextId != NULL)
    {
        CliPrintf (CliHandle, "Openflow-Client Controller Configurations\r\n");
        CliPrintf (CliHandle,
                   "--------------------------------------------------------------------------\r\n");

        i4RetVal =
            nmhGetFirstIndexFsofcControllerConnTable (&u4FsofcContextId,
                                                      &i4FsofcControllerIpAddrType,
                                                      &FsofcControllerIpAddressSrc,
                                                      &i4FsofcControllerConnAuxId);

        while (SNMP_SUCCESS == i4RetVal)
        {
            if (u4FsofcContextId == *pu4FsofcContextId)
            {
                OfcFsofcControllerConnEntry.MibObject.u4FsofcContextId =
                    u4FsofcContextId;

                OfcFsofcControllerConnEntry.MibObject.
                    i4FsofcControllerIpAddrType = i4FsofcControllerIpAddrType;

                MEMCPY (OfcFsofcControllerConnEntry.MibObject.
                        au1FsofcControllerIpAddress,
                        FsofcControllerIpAddressSrc.pu1_OctetList,
                        FsofcControllerIpAddressSrc.i4_Length);

                OfcFsofcControllerConnEntry.MibObject.
                    i4FsofcControllerIpAddressLen =
                    FsofcControllerIpAddressSrc.i4_Length;

                OfcFsofcControllerConnEntry.MibObject.
                    i4FsofcControllerConnAuxId = i4FsofcControllerConnAuxId;

                if (OfcGetAllFsofcControllerConnTable
                    (&OfcFsofcControllerConnEntry) == OSIX_SUCCESS)
                {
                    MEMSET (au1Ip, OFC_ZERO, OFC_MAX_IP_LEN);
                    OfcCliLocalIpToString (au1Ip,
                                           OfcFsofcControllerConnEntry.
                                           MibObject.
                                           au1FsofcControllerIpAddress);
                    CliPrintf (CliHandle,
                               "CxtID %2d IpAddr %16s AuxID %2d Port %5d Proto %2d Role %2d Band %2d State %2d\r\n",
                               OfcFsofcControllerConnEntry.MibObject.
                               u4FsofcContextId, au1Ip,
                               OfcFsofcControllerConnEntry.MibObject.
                               i4FsofcControllerConnAuxId,
                               OfcFsofcControllerConnEntry.MibObject.
                               i4FsofcControllerConnPort,
                               OfcFsofcControllerConnEntry.MibObject.
                               i4FsofcControllerConnProtocol,
                               OfcFsofcControllerConnEntry.MibObject.
                               i4FsofcControllerRole,
                               OfcFsofcControllerConnEntry.MibObject.
                               i4FsofcControllerConnBand,
                               OfcFsofcControllerConnEntry.MibObject.
                               i4FsofcControllerConnState);
                }
            }
            i4RetVal =
                nmhGetNextIndexFsofcControllerConnTable (u4FsofcContextId,
                                                         &u4NextFsofcContextId,
                                                         i4FsofcControllerIpAddrType,
                                                         &i4NextFsofcControllerIpAddrType,
                                                         &FsofcControllerIpAddressSrc,
                                                         &NextFsofcControllerIpAddress,
                                                         i4FsofcControllerConnAuxId,
                                                         &i4NextFsofcControllerConnAuxId);
            u4FsofcContextId = u4NextFsofcContextId;
            FsofcControllerIpAddressSrc = NextFsofcControllerIpAddress;
            i4FsofcControllerConnAuxId = i4NextFsofcControllerConnAuxId;
        }
    }
    return CLI_SUCCESS;
}

/****************************************************************************
 Function    :  OfcCliShowFsofcFlowTable
 Input       :            
 Description :  This Routine Take the Indices &
                Gets the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
OfcCliShowFsofcFlowTable (tCliHandle CliHandle, UINT4 *pu4FsofcContextId,
                          UINT4 *pu4FsofcTableIndex, UINT4 *pu4FsofcFlowIndex)
{
    INT4                i4RetVal = SNMP_FAILURE;
    static tOfcFsofcFlowEntry OfcFsofcFlowEntry;
    UINT4               u4FsofcTableIndex = OFC_ZERO;
    UINT4               u4NextFsofcTableIndex = OFC_ZERO;
    UINT4               u4FsofcFlowIndex = OFC_ZERO;
    UINT4               u4NextFsofcFlowIndex = OFC_ZERO;
    UINT4               u4FsofcContextId = OFC_ZERO;
    UINT4               u4NextFsofcContextId = OFC_ZERO;
    FS_UINT8            u8PktCount;
    FS_UINT8            u8ByteCount;

    UINT1               au1ByteCount[OFC_MAX_COUNT_LEN];
    UINT1               au1PktCount[OFC_MAX_COUNT_LEN];
    MEMSET (&OfcFsofcFlowEntry, OFC_ZERO, sizeof (tOfcFsofcFlowEntry));
    MEMSET (&OfcFsofcFlowEntry.MibObject, OFC_ZERO,
            sizeof (tOfcMibFsofcFlowEntry));
    MEMSET (&u8PktCount, OFC_ZERO, sizeof (FS_UINT8));
    MEMSET (&u8ByteCount, OFC_ZERO, sizeof (FS_UINT8));
    MEMSET (au1ByteCount, OFC_ZERO, OFC_MAX_COUNT_LEN);
    MEMSET (au1PktCount, OFC_ZERO, OFC_MAX_COUNT_LEN);

    if ((pu4FsofcContextId == NULL) && (pu4FsofcTableIndex == NULL)
        && (pu4FsofcFlowIndex == NULL))

    {
        i4RetVal =
            nmhGetFirstIndexFsofcFlowTable (&u4FsofcContextId,
                                            &u4FsofcTableIndex,
                                            &u4FsofcFlowIndex);
        while (SNMP_SUCCESS == i4RetVal)
        {
            OfcFsofcFlowEntry.MibObject.u4FsofcContextId = u4FsofcContextId;

            OfcFsofcFlowEntry.MibObject.u4FsofcTableIndex = u4FsofcTableIndex;

            OfcFsofcFlowEntry.MibObject.u4FsofcFlowIndex = u4FsofcFlowIndex;

            if (OfcGetAllFsofcFlowTable (&OfcFsofcFlowEntry) == OSIX_SUCCESS)
            {
                FSAP_U8_CLR (&u8PktCount);
                FSAP_U8_CLR (&u8ByteCount);
                MEMSET (au1PktCount, OFC_ZERO, OFC_MAX_COUNT_LEN);
                MEMSET (au1ByteCount, OFC_ZERO, OFC_MAX_COUNT_LEN);
                FSAP_U8_ASSIGN_HI (&u8ByteCount,
                                   OfcFsofcFlowEntry.MibObject.
                                   u8FsofcFlowByteCount.msn);
                FSAP_U8_ASSIGN_LO (&u8ByteCount,
                                   OfcFsofcFlowEntry.MibObject.
                                   u8FsofcFlowByteCount.lsn);
                FSAP_U8_ASSIGN_HI (&u8PktCount,
                                   OfcFsofcFlowEntry.MibObject.
                                   u8FsofcFlowPacketCount.msn);
                FSAP_U8_ASSIGN_LO (&u8PktCount,
                                   OfcFsofcFlowEntry.MibObject.
                                   u8FsofcFlowPacketCount.lsn);
                FSAP_U8_2STR (&u8ByteCount, (CHR1 *) & au1ByteCount);
                FSAP_U8_2STR (&u8PktCount, (CHR1 *) & au1PktCount);
                CliPrintf (CliHandle, "Openflow-Client Flow Table Info\r\n");
                CliPrintf (CliHandle,
                           "--------------------------------------------------------------------\r\n");
                CliPrintf (CliHandle,
                           "CxtID %2d TableId %3d FlowID %6d MatchField %16s Action %16s IdleTimeout %4d HardTimeout %4d ByteCnt %s PktCnt %s DurSec %4d\r\n",
                           OfcFsofcFlowEntry.MibObject.u4FsofcContextId,
                           OfcFsofcFlowEntry.MibObject.u4FsofcTableIndex,
                           OfcFsofcFlowEntry.MibObject.u4FsofcFlowIndex,
                           OfcFsofcFlowEntry.MibObject.au1FsofcFlowMatchField,
                           OfcFsofcFlowEntry.MibObject.au1FsofcFlowOutputAction,
                           OfcFsofcFlowEntry.MibObject.u4FsofcFlowIdleTimeout,
                           OfcFsofcFlowEntry.MibObject.u4FsofcFlowHardTimeout,
                           au1ByteCount,
                           au1PktCount,
                           OfcFsofcFlowEntry.MibObject.u4FsofcFlowDurationSec);

            }
            i4RetVal =
                nmhGetNextIndexFsofcFlowTable (u4FsofcContextId,
                                               &u4NextFsofcContextId,
                                               u4FsofcTableIndex,
                                               &u4NextFsofcTableIndex,
                                               u4FsofcFlowIndex,
                                               &u4NextFsofcFlowIndex);
            u4FsofcContextId = u4NextFsofcContextId;
            u4FsofcTableIndex = u4NextFsofcTableIndex;
            u4FsofcFlowIndex = u4NextFsofcFlowIndex;
        }
    }
    else
    {

        if ((pu4FsofcContextId != NULL) && (pu4FsofcTableIndex != NULL)
            && (pu4FsofcFlowIndex != NULL))
        {
            OfcFsofcFlowEntry.MibObject.u4FsofcContextId = *pu4FsofcContextId;

            OfcFsofcFlowEntry.MibObject.u4FsofcTableIndex = *pu4FsofcTableIndex;

            OfcFsofcFlowEntry.MibObject.u4FsofcFlowIndex = *pu4FsofcFlowIndex;

            if (OfcGetAllFsofcFlowTable (&OfcFsofcFlowEntry) == OSIX_SUCCESS)
            {
                FSAP_U8_CLR (&u8PktCount);
                FSAP_U8_CLR (&u8ByteCount);
                MEMSET (au1PktCount, OFC_ZERO, OFC_MAX_COUNT_LEN);
                MEMSET (au1ByteCount, OFC_ZERO, OFC_MAX_COUNT_LEN);
                FSAP_U8_ASSIGN_HI (&u8ByteCount,
                                   OfcFsofcFlowEntry.MibObject.
                                   u8FsofcFlowByteCount.msn);
                FSAP_U8_ASSIGN_LO (&u8ByteCount,
                                   OfcFsofcFlowEntry.MibObject.
                                   u8FsofcFlowByteCount.lsn);
                FSAP_U8_ASSIGN_HI (&u8PktCount,
                                   OfcFsofcFlowEntry.MibObject.
                                   u8FsofcFlowPacketCount.msn);
                FSAP_U8_ASSIGN_LO (&u8PktCount,
                                   OfcFsofcFlowEntry.MibObject.
                                   u8FsofcFlowPacketCount.lsn);
                FSAP_U8_2STR (&u8ByteCount, (CHR1 *) & au1ByteCount);
                FSAP_U8_2STR (&u8PktCount, (CHR1 *) & au1PktCount);

                CliPrintf (CliHandle, "Openflow-Client Flow Table Info\r\n");
                CliPrintf (CliHandle,
                           "--------------------------------------------------------------------\r\n");
                CliPrintf (CliHandle,
                           "CxtID %2d TableId %3d FlowID %6d MatchField %16s Action %16s IdleTimeout %4d HardTimeout %4d ByteCnt %s PktCnt %s DurSec %4d\r\n",
                           OfcFsofcFlowEntry.MibObject.u4FsofcContextId,
                           OfcFsofcFlowEntry.MibObject.u4FsofcTableIndex,
                           OfcFsofcFlowEntry.MibObject.u4FsofcFlowIndex,
                           OfcFsofcFlowEntry.MibObject.au1FsofcFlowMatchField,
                           OfcFsofcFlowEntry.MibObject.au1FsofcFlowOutputAction,
                           OfcFsofcFlowEntry.MibObject.u4FsofcFlowIdleTimeout,
                           OfcFsofcFlowEntry.MibObject.u4FsofcFlowHardTimeout,
                           au1ByteCount,
                           au1PktCount,
                           OfcFsofcFlowEntry.MibObject.u4FsofcFlowDurationSec);

            }
        }
    }
    return CLI_SUCCESS;
}

/****************************************************************************
 Function    :  OfcCliShowFsofcGroupTable
 Input       :            
 Description :  This Routine Take the Indices &
                Gets the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
OfcCliShowFsofcGroupTable (tCliHandle CliHandle, UINT4 *pu4FsofcContextId,
                           UINT4 *pu4FsofcGroupIndex)
{
    INT4                i4RetVal = SNMP_FAILURE;
    tOfcFsofcGroupEntry OfcFsofcGroupEntry;
    UINT4               u4FsofcGroupIndex = OFC_ZERO;
    UINT4               u4NextFsofcGroupIndex = OFC_ZERO;
    UINT4               u4FsofcContextId = OFC_ZERO;
    UINT4               u4NextFsofcContextId = OFC_ZERO;
    FS_UINT8            u8PktCount;
    FS_UINT8            u8ByteCount;

    UINT1               au1ByteCount[OFC_MAX_COUNT_LEN];
    UINT1               au1PktCount[OFC_MAX_COUNT_LEN];

    MEMSET (&OfcFsofcGroupEntry, OFC_ZERO, sizeof (tOfcFsofcGroupEntry));
    MEMSET (&u8PktCount, OFC_ZERO, sizeof (FS_UINT8));
    MEMSET (&u8ByteCount, OFC_ZERO, sizeof (FS_UINT8));
    MEMSET (au1ByteCount, OFC_ZERO, OFC_MAX_COUNT_LEN);
    MEMSET (au1PktCount, OFC_ZERO, OFC_MAX_COUNT_LEN);

    if ((pu4FsofcContextId == NULL) && (pu4FsofcGroupIndex == NULL))

    {
        i4RetVal =
            nmhGetFirstIndexFsofcGroupTable (&u4FsofcContextId,
                                             &u4FsofcGroupIndex);
        while (SNMP_SUCCESS == i4RetVal)
        {
            OfcFsofcGroupEntry.MibObject.u4FsofcContextId = u4FsofcContextId;

            OfcFsofcGroupEntry.MibObject.u4FsofcGroupIndex = u4FsofcGroupIndex;

            if (OfcGetAllFsofcGroupTable (&OfcFsofcGroupEntry) == OSIX_SUCCESS)
            {
                FSAP_U8_CLR (&u8PktCount);
                FSAP_U8_CLR (&u8ByteCount);
                MEMSET (au1PktCount, OFC_ZERO, OFC_MAX_COUNT_LEN);
                MEMSET (au1ByteCount, OFC_ZERO, OFC_MAX_COUNT_LEN);
                FSAP_U8_ASSIGN_HI (&u8ByteCount,
                                   OfcFsofcGroupEntry.MibObject.
                                   u8FsofcGroupByteCount.msn);
                FSAP_U8_ASSIGN_LO (&u8ByteCount,
                                   OfcFsofcGroupEntry.MibObject.
                                   u8FsofcGroupByteCount.lsn);
                FSAP_U8_ASSIGN_HI (&u8PktCount,
                                   OfcFsofcGroupEntry.MibObject.
                                   u8FsofcGroupPacketCount.msn);
                FSAP_U8_ASSIGN_LO (&u8PktCount,
                                   OfcFsofcGroupEntry.MibObject.
                                   u8FsofcGroupPacketCount.lsn);
                FSAP_U8_2STR (&u8ByteCount, (CHR1 *) & au1ByteCount);
                FSAP_U8_2STR (&u8PktCount, (CHR1 *) & au1PktCount);

                CliPrintf (CliHandle, "Openflow-Client Group Table info\r\n");
                CliPrintf (CliHandle,
                           "------------------------------------------------------------------\r\n");
                CliPrintf (CliHandle,
                           "CxtID %2d GrpID %3d GrpType %2d Actions %16s ByteCount %s PacketCount %s DurSec %4d\r\n",
                           OfcFsofcGroupEntry.MibObject.u4FsofcContextId,
                           OfcFsofcGroupEntry.MibObject.u4FsofcGroupIndex,
                           OfcFsofcGroupEntry.MibObject.i4FsofcGroupType,
                           OfcFsofcGroupEntry.MibObject.
                           au1FsofcGroupActionBuckets,
                           au1ByteCount,
                           au1PktCount,
                           OfcFsofcGroupEntry.MibObject.
                           u4FsofcGroupDurationSec);

            }
            i4RetVal =
                nmhGetNextIndexFsofcGroupTable (u4FsofcContextId,
                                                &u4NextFsofcContextId,
                                                u4FsofcGroupIndex,
                                                &u4NextFsofcGroupIndex);
            u4FsofcContextId = u4NextFsofcContextId;
            u4FsofcGroupIndex = u4NextFsofcGroupIndex;
        }
    }
    else
    {
        if ((pu4FsofcContextId != NULL) && (pu4FsofcGroupIndex != NULL))
        {
            OfcFsofcGroupEntry.MibObject.u4FsofcContextId = *pu4FsofcContextId;

            OfcFsofcGroupEntry.MibObject.u4FsofcGroupIndex =
                *pu4FsofcGroupIndex;

            if (OfcGetAllFsofcGroupTable (&OfcFsofcGroupEntry) == OSIX_SUCCESS)
            {
                FSAP_U8_CLR (&u8PktCount);
                FSAP_U8_CLR (&u8ByteCount);
                MEMSET (au1PktCount, OFC_ZERO, OFC_MAX_COUNT_LEN);
                MEMSET (au1ByteCount, OFC_ZERO, OFC_MAX_COUNT_LEN);
                FSAP_U8_ASSIGN_HI (&u8ByteCount,
                                   OfcFsofcGroupEntry.MibObject.
                                   u8FsofcGroupByteCount.msn);
                FSAP_U8_ASSIGN_LO (&u8ByteCount,
                                   OfcFsofcGroupEntry.MibObject.
                                   u8FsofcGroupByteCount.lsn);
                FSAP_U8_ASSIGN_HI (&u8PktCount,
                                   OfcFsofcGroupEntry.MibObject.
                                   u8FsofcGroupPacketCount.msn);
                FSAP_U8_ASSIGN_LO (&u8PktCount,
                                   OfcFsofcGroupEntry.MibObject.
                                   u8FsofcGroupPacketCount.lsn);
                FSAP_U8_2STR (&u8ByteCount, (CHR1 *) & au1ByteCount);
                FSAP_U8_2STR (&u8PktCount, (CHR1 *) & au1PktCount);
                CliPrintf (CliHandle, "Openflow-Client Group Table info\r\n");
                CliPrintf (CliHandle,
                           "------------------------------------------------------------------\r\n");
                CliPrintf (CliHandle,
                           "CxtID %2d GrpID %3d GrpType %2d Actions %16s ByteCount %s PacketCount %s DurSec %4d\r\n",
                           OfcFsofcGroupEntry.MibObject.u4FsofcContextId,
                           OfcFsofcGroupEntry.MibObject.u4FsofcGroupIndex,
                           OfcFsofcGroupEntry.MibObject.i4FsofcGroupType,
                           OfcFsofcGroupEntry.MibObject.
                           au1FsofcGroupActionBuckets, au1ByteCount,
                           au1PktCount,
                           OfcFsofcGroupEntry.MibObject.
                           u4FsofcGroupDurationSec);

            }
        }
    }
    return CLI_SUCCESS;
}

/****************************************************************************
 Function    :  OfcCliShowFsofcMeterTable
 Input       :            
 Description :  This Routine Take the Indices &
                Gets the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
OfcCliShowFsofcMeterTable (tCliHandle CliHandle, UINT4 *pu4FsofcContextId,
                           UINT4 *pu4FsofcMeterIndex)
{
    INT4                i4RetVal = SNMP_FAILURE;
    tOfcFsofcMeterEntry OfcFsofcMeterEntry;
    UINT4               u4FsofcMeterIndex = OFC_ZERO;
    UINT4               u4NextFsofcMeterIndex = OFC_ZERO;

    UINT4               u4FsofcContextId = OFC_ZERO;
    UINT4               u4NextFsofcContextId = OFC_ZERO;

    MEMSET (&OfcFsofcMeterEntry, OFC_ZERO, sizeof (tOfcFsofcMeterEntry));
    if ((pu4FsofcContextId == NULL) && (pu4FsofcMeterIndex == NULL))

    {
        i4RetVal =
            nmhGetFirstIndexFsofcMeterTable (&u4FsofcContextId,
                                             &u4FsofcMeterIndex);
        while (SNMP_SUCCESS == i4RetVal)
        {
            OfcFsofcMeterEntry.MibObject.u4FsofcContextId = u4FsofcContextId;

            OfcFsofcMeterEntry.MibObject.u4FsofcMeterIndex = u4FsofcMeterIndex;

            if (OfcGetAllFsofcMeterTable (&OfcFsofcMeterEntry) == OSIX_SUCCESS)
            {
                CliPrintf (CliHandle, "Openflow-Client Meter Table info\r\n");
                CliPrintf (CliHandle,
                           "-----------------------------------------------------------------\r\n");
                CliPrintf (CliHandle,
                           "CxtID %2d MeterID %6d BandInfo %16s FlowCount %6d ByteCount %6d PacketCount %6d DurSec %6d\r\n",
                           OfcFsofcMeterEntry.MibObject.u4FsofcContextId,
                           OfcFsofcMeterEntry.MibObject.u4FsofcMeterIndex,
                           OfcFsofcMeterEntry.MibObject.au1FsofcMeterBandInfo,
                           OfcFsofcMeterEntry.MibObject.u4FsofcMeterFlowCount,
                           OfcFsofcMeterEntry.MibObject.u4FsofcMeterFlowCount,
                           OfcFsofcMeterEntry.MibObject.u4FsofcMeterFlowCount,
                           OfcFsofcMeterEntry.MibObject.
                           u4FsofcMeterDurationSec);

            }
            i4RetVal =
                nmhGetNextIndexFsofcMeterTable (u4FsofcContextId,
                                                &u4NextFsofcContextId,
                                                u4FsofcMeterIndex,
                                                &u4NextFsofcMeterIndex);
            u4FsofcContextId = u4NextFsofcContextId;
            u4FsofcMeterIndex = u4NextFsofcMeterIndex;
        }
    }
    else
    {
        if ((pu4FsofcContextId != NULL) && (pu4FsofcMeterIndex != NULL))
        {

            OfcFsofcMeterEntry.MibObject.u4FsofcContextId = *pu4FsofcContextId;

            OfcFsofcMeterEntry.MibObject.u4FsofcMeterIndex =
                *pu4FsofcMeterIndex;

            if (OfcGetAllFsofcMeterTable (&OfcFsofcMeterEntry) == OSIX_SUCCESS)
            {
                CliPrintf (CliHandle, "Openflow-Client Meter Table info\r\n");
                CliPrintf (CliHandle,
                           "-----------------------------------------------------------------\r\n");
                CliPrintf (CliHandle,
                           "CxtID %2d MeterID %6d BandInfo %16s FlowCount %6d ByteCount %6d PacketCount %6d DurSec %6d\r\n",
                           OfcFsofcMeterEntry.MibObject.u4FsofcContextId,
                           OfcFsofcMeterEntry.MibObject.u4FsofcMeterIndex,
                           OfcFsofcMeterEntry.MibObject.au1FsofcMeterBandInfo,
                           OfcFsofcMeterEntry.MibObject.u4FsofcMeterFlowCount,
                           OfcFsofcMeterEntry.MibObject.u4FsofcMeterFlowCount,
                           OfcFsofcMeterEntry.MibObject.u4FsofcMeterFlowCount,
                           OfcFsofcMeterEntry.MibObject.
                           u4FsofcMeterDurationSec);

            }
        }
    }
    return CLI_SUCCESS;
}
#endif
