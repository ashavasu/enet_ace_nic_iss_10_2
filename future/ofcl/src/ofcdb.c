/********************************************************************
* Copyright (C) 2013 Aricent Inc . All Rights Reserved
*
* $Id: ofcdb.c,v 1.8 2014/03/01 11:36:47 siva Exp $
*
* Description: This file contains the routines for the protocol
*              Database Access for the module Ofc 
*********************************************************************/

#include "ofcinc.h"
#include "ofccli.h"
#include "ofccntlr.h"
#include "ofcapi.h"
#include "utilcli.h"
#include "ofchybrid.h"

/****************************************************************************
 Function    :  OfcTestAllFsofcCfgTable
 Input       :  pu4ErrorCode
                pOfcSetFsofcCfgEntry
                pOfcIsSetFsofcCfgEntry
 Description :  This Routine Take the Indices &
                Test the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

INT4
OfcTestAllFsofcCfgTable (UINT4 *pu4ErrorCode,
                         tOfcFsofcCfgEntry * pOfcSetFsofcCfgEntry,
                         tOfcIsSetFsofcCfgEntry * pOfcIsSetFsofcCfgEntry,
                         INT4 i4RowStatusLogic, INT4 i4RowCreateOption)
{
    tOfcFsofcCfgEntry  *pOfcFsofcCfgEntry = NULL;
    tOfcFsofcIfEntry   *pOfcGetFsofcIfEntry = NULL;
    tOfcFsofcControllerConnEntry *pOfcGetFsofcConnEntry = NULL;
    tOfcFsofcControllerConnEntry OfcGetFsofcConnEntry;
    tOfcFsofcIfEntry    OfcGetFsofcIfEntry;
    tOfcFsofcCfgEntry   OfcFsofcCfgEntry;

    MEMSET (&OfcFsofcCfgEntry, OFC_ZERO, sizeof (tOfcFsofcCfgEntry));
    MEMSET (&OfcGetFsofcIfEntry, OFC_ZERO, sizeof (tOfcFsofcIfEntry));
    MEMSET (&OfcGetFsofcConnEntry, OFC_ZERO,
            sizeof (tOfcFsofcControllerConnEntry));

    if (pOfcIsSetFsofcCfgEntry->bFsofcModuleStatus != OSIX_FALSE)
    {
        if (pOfcIsSetFsofcCfgEntry->bFsofcSupportedVersion == OSIX_FALSE &&
            pOfcIsSetFsofcCfgEntry->bFsofcDefaultFlowMissBehaviour ==
            OSIX_FALSE)
        {
            OfcFsofcCfgEntry.MibObject.u4FsofcContextId =
                pOfcSetFsofcCfgEntry->MibObject.u4FsofcContextId;

            pOfcFsofcCfgEntry = RBTreeGet (gOfcGlobals.OfcGlbMib.FsofcCfgTable,
                                           (tRBElem *) (&OfcFsofcCfgEntry));
            if (pOfcFsofcCfgEntry == NULL)
            {
                CLI_SET_ERR (CLI_OFC_INVALID_CONTEXT_ID);
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return OSIX_FAILURE;
            }
        }

        OfcFsofcCfgEntry.MibObject.u4FsofcContextId =
            pOfcSetFsofcCfgEntry->MibObject.u4FsofcContextId;

        pOfcFsofcCfgEntry = RBTreeGet (gOfcGlobals.OfcGlbMib.FsofcCfgTable,
                                       (tRBElem *) (&OfcFsofcCfgEntry));

        if ((pOfcFsofcCfgEntry != NULL) &&
            ((pOfcFsofcCfgEntry->MibObject.i4FsofcModuleStatus !=
              pOfcSetFsofcCfgEntry->MibObject.i4FsofcModuleStatus) &&
             ((pOfcFsofcCfgEntry->MibObject.i4FsofcModuleStatus !=
               OPENFLOW_NONE) &&
              (pOfcSetFsofcCfgEntry->MibObject.i4FsofcModuleStatus !=
               OPENFLOW_NONE))))
        {
            CLI_SET_ERR (CLI_OFC_HYBRID_STATUS_ERROR);
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return OSIX_FAILURE;
        }
    }

    if (pOfcIsSetFsofcCfgEntry->bFsofcSupportedVersion != OSIX_FALSE)
    {
        /* 
         * Currently Supported version is a read-only Object in MIB.
         * So setting this value is not supported 
         */
        if ((pOfcIsSetFsofcCfgEntry->bFsofcIpReassembleStatus == OSIX_FALSE) ||
            (pOfcIsSetFsofcCfgEntry->bFsofcSwitchModeOnConnFailure ==
             OSIX_FALSE))
        {
            CLI_SET_ERR (CLI_OFC_UNSUPPORTED_SET_REQ);
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return OSIX_FAILURE;
        }
    }

    if (pOfcIsSetFsofcCfgEntry->bFsofcDefaultFlowMissBehaviour != OSIX_FALSE)
    {
        /*Fill your check condition */
    }
    if (pOfcIsSetFsofcCfgEntry->bFsofcControlPktBuffering != OSIX_FALSE)
    {
        /*Fill your check condition */
    }
    if (pOfcIsSetFsofcCfgEntry->bFsofcIpReassembleStatus != OSIX_FALSE)
    {
        /*Fill your check condition */
    }
    if (pOfcIsSetFsofcCfgEntry->bFsofcPortStpStatus != OSIX_FALSE)
    {
        /*Fill your check condition */
    }
    if (pOfcIsSetFsofcCfgEntry->bFsofcTraceEnable != OSIX_FALSE)
    {
        /*Fill your check condition */
    }
    if (pOfcIsSetFsofcCfgEntry->bFsofcSwitchModeOnConnFailure != OSIX_FALSE)
    {
        if (pOfcSetFsofcCfgEntry->MibObject.i4FsofcSwitchModeOnConnFailure ==
            OPENFLOW_FAIL_STANDALONE)
        {
            pOfcFsofcCfgEntry = RBTreeGet (gOfcGlobals.OfcGlbMib.FsofcCfgTable,
                                           (tRBElem *) (pOfcSetFsofcCfgEntry));
            if (pOfcFsofcCfgEntry == NULL)
            {
                CLI_SET_ERR (CLI_OFC_INVALID_CONTEXT_ID);
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return OSIX_FAILURE;
            }

            if (pOfcFsofcCfgEntry->MibObject.i4FsofcModuleStatus ==
                OPENFLOW_NONE)
            {
                CLI_SET_ERR (CLI_OFC_HYBRID_MODE_ERROR);
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return OSIX_FAILURE;
            }
        }
    }

    if (pOfcIsSetFsofcCfgEntry->bFsofcSwitchEntryStatus != OSIX_FALSE)
    {
        /* 
         * Value "0" is default context and it will be created automatically 
         * at system start-up. Other than Default context can not be created  
         * by user for Ver 1.0.0 
         */
        if ((pOfcSetFsofcCfgEntry->MibObject.i4FsofcSwitchEntryStatus !=
             DESTROY)
            && (pOfcSetFsofcCfgEntry->MibObject.u4FsofcContextId !=
                OFC_DEFAULT_CONTEXT))
        {
            pOfcFsofcCfgEntry = NULL;
            pOfcFsofcCfgEntry = (tOfcFsofcCfgEntry *)
                RBTreeGetFirst (gOfcGlobals.OfcGlbMib.FsofcCfgTable);
            if (pOfcFsofcCfgEntry == NULL)
            {
                CLI_SET_ERR (CLI_OFC_INVALID_ENTRY);
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                return OSIX_FAILURE;
            }

            if (pOfcFsofcCfgEntry->MibObject.i4FsofcSupportedVersion ==
                OFC_VERSION_V100)
            {
                CLI_SET_ERR (CLI_OFC_UNSUPPORTED_SET_REQ);
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return OSIX_FAILURE;
            }
        }

        /* 
         * Validate the Cfg Entry 
         */
        if ((pOfcSetFsofcCfgEntry->MibObject.i4FsofcSwitchEntryStatus ==
             DESTROY)
            && (pOfcSetFsofcCfgEntry->MibObject.u4FsofcContextId !=
                OFC_DEFAULT_CONTEXT))
        {
            pOfcFsofcCfgEntry = NULL;
            pOfcFsofcCfgEntry = (tOfcFsofcCfgEntry *)
                RBTreeGet (gOfcGlobals.OfcGlbMib.FsofcCfgTable,
                           (tRBElem *) pOfcSetFsofcCfgEntry);
            if (pOfcFsofcCfgEntry == NULL)
            {
                CLI_SET_ERR (CLI_OFC_INVALID_ENTRY);
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                return OSIX_FAILURE;
            }
        }

        /* 
         * Value "0" is default context and it will be created automatically at system
         * start-up. Default context can not be destroyed by user. 
         */
        if ((pOfcSetFsofcCfgEntry->MibObject.i4FsofcSwitchEntryStatus ==
             DESTROY)
            && (pOfcSetFsofcCfgEntry->MibObject.u4FsofcContextId ==
                OFC_DEFAULT_CONTEXT))
        {
            CLI_SET_ERR (CLI_OFC_UNSUPPORTED_SET_REQ);
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return OSIX_FAILURE;
        }

        if (pOfcSetFsofcCfgEntry->MibObject.i4FsofcSwitchEntryStatus == DESTROY)
        {
            /* 
             * Cfg Entry deletion is not allowed, if any interfaces are mapped to given
             * Context. Unmap all the mapped interfaces first to destroy CFG Entry. 
             */
            pOfcGetFsofcIfEntry = &OfcGetFsofcIfEntry;
            pOfcGetFsofcIfEntry->MibObject.u4FsofcContextId =
                pOfcSetFsofcCfgEntry->MibObject.u4FsofcContextId;

            while ((pOfcGetFsofcIfEntry =
                    RBTreeGetNext (gOfcGlobals.OfcGlbMib.FsofcIfTable,
                                   pOfcGetFsofcIfEntry, NULL)) != NULL)
            {
                if (pOfcGetFsofcIfEntry->MibObject.u4FsofcContextId ==
                    pOfcSetFsofcCfgEntry->MibObject.u4FsofcContextId)
                {
                    CLI_SET_ERR (CLI_OFC_INTF_MAPPING_EXISTS);
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                    return OFC_FAILURE;
                }
            }

            /* 
             * Cfg Entry deletion is not allowed, if any controllers are mapped to given
             * Context. Unmap all the mapped controllers first to destroy CFG Entry. 
             */
            pOfcGetFsofcConnEntry = &OfcGetFsofcConnEntry;
            pOfcGetFsofcConnEntry->MibObject.u4FsofcContextId =
                pOfcSetFsofcCfgEntry->MibObject.u4FsofcContextId;

            while ((pOfcGetFsofcConnEntry =
                    RBTreeGetNext (gOfcGlobals.OfcGlbMib.
                                   FsofcControllerConnTable,
                                   pOfcGetFsofcConnEntry, NULL)) != NULL)
            {
                if (pOfcGetFsofcConnEntry->MibObject.u4FsofcContextId ==
                    pOfcSetFsofcCfgEntry->MibObject.u4FsofcContextId)
                {
                    CLI_SET_ERR (CLI_OFC_INTF_MAPPING_EXISTS);
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                    return OFC_FAILURE;
                }
            }
        }
    }

    if (pOfcIsSetFsofcCfgEntry->bFsofcHybrid != OSIX_FALSE)
    {
        OfcFsofcCfgEntry.MibObject.u4FsofcContextId =
            pOfcSetFsofcCfgEntry->MibObject.u4FsofcContextId;

        pOfcFsofcCfgEntry = RBTreeGet (gOfcGlobals.OfcGlbMib.FsofcCfgTable,
                                       (tRBElem *) (&OfcFsofcCfgEntry));
        if ((pOfcFsofcCfgEntry != NULL) &&
            (pOfcFsofcCfgEntry->MibObject.i4FsofcModuleStatus ==
             pOfcSetFsofcCfgEntry->MibObject.i4FsofcModuleStatus) &&
            (pOfcFsofcCfgEntry->MibObject.i4FsofcHybrid !=
             pOfcSetFsofcCfgEntry->MibObject.i4FsofcHybrid))
        {
            CLI_SET_ERR (CLI_OFC_HYBRID_STATUS_ERROR);
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return OSIX_FAILURE;
        }

        if (OfcHybridPortCheck (OfcFsofcCfgEntry.MibObject.u4FsofcContextId,
                                pOfcSetFsofcCfgEntry->MibObject.
                                i4FsofcModuleStatus,
                                pOfcSetFsofcCfgEntry->MibObject.
                                i4FsofcHybrid) != OFC_SUCCESS)
        {
            CLI_SET_ERR (CLI_OFC_HYBRID_PORT_ERROR);
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return OSIX_FAILURE;
        }
    }

    UNUSED_PARAM (i4RowStatusLogic);
    UNUSED_PARAM (i4RowCreateOption);
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  OfcTestAllFsofcControllerConnTable
 Input       :  pu4ErrorCode
                pOfcSetFsofcControllerConnEntry
                pOfcIsSetFsofcControllerConnEntry
 Description :  This Routine Take the Indices &
                Test the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

INT4
OfcTestAllFsofcControllerConnTable (UINT4 *pu4ErrorCode,
                                    tOfcFsofcControllerConnEntry *
                                    pOfcSetFsofcControllerConnEntry,
                                    tOfcIsSetFsofcControllerConnEntry *
                                    pOfcIsSetFsofcControllerConnEntry,
                                    INT4 i4RowStatusLogic,
                                    INT4 i4RowCreateOption)
{
    tOfcFsofcControllerConnEntry OfcSetFsofcControllerConnEntry;
    tOfcFsofcControllerConnEntry OfcGetFsofcControllerConnEntry;
    tOfcFsofcControllerConnEntry *pOfcFsofcControllerConnEntry = NULL;
    tOfcFsofcCfgEntry  *pOfcFsofcCfgEntry = NULL;

    UINT1               au1TmpAddr[OFC_MAX_IP_LENGTH];
    UINT4               u4TmpAddr = OFC_ZERO;

    MEMSET (&OfcSetFsofcControllerConnEntry, OFC_ZERO,
            sizeof (tOfcFsofcControllerConnEntry));

    MEMSET (&OfcGetFsofcControllerConnEntry, OFC_ZERO,
            sizeof (tOfcFsofcControllerConnEntry));

    MEMSET (au1TmpAddr, OFC_ZERO, OFC_MAX_IP_LENGTH);

    /* 
     * Verify the Main-Controller Connection Status before allowing Auxilary 
     * connection creations 
     */
    if (pOfcSetFsofcControllerConnEntry->MibObject.i4FsofcControllerConnAuxId !=
        OFC_ZERO)
    {
        MEMCPY (&OfcSetFsofcControllerConnEntry,
                pOfcSetFsofcControllerConnEntry,
                sizeof (tOfcFsofcControllerConnEntry));
        OfcSetFsofcControllerConnEntry.MibObject.i4FsofcControllerConnAuxId =
            OFC_ZERO;

        /* 
         * Check whether the Primary Conn Entry is already present, for
         * Auxilizry Conn Entires. 
         */
        pOfcFsofcControllerConnEntry =
            RBTreeGet (gOfcGlobals.OfcGlbMib.FsofcControllerConnTable,
                       (tRBElem *) & OfcSetFsofcControllerConnEntry);

        if (pOfcFsofcControllerConnEntry == NULL)
        {
            CLI_SET_ERR (CLI_OFC_PRIMARY_CONN_ERROR);
            *pu4ErrorCode = SNMP_ERR_NO_CREATION;
            return OSIX_FAILURE;
        }
    }

    if (pOfcIsSetFsofcControllerConnEntry->bFsofcControllerConnPort !=
        OSIX_FALSE)
    {
        if (pOfcSetFsofcControllerConnEntry->MibObject.
            i4FsofcControllerConnPort == OFC_ZERO)
        {
            CLI_SET_ERR (CLI_OFC_NOT_SUPPORTED_VALUE);
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return OSIX_FAILURE;
        }

        if (pOfcIsSetFsofcControllerConnEntry->
            bFsofcControllerConnProtocol == OSIX_FALSE)
        {
            /* 
             * Check whether the Controller is in CONNECTED_STATE. If "YES"
             * This might introduce the traffic hit. Do not allow port number
             * update. User to make sure proper socket closure by closing the
             * existing connection entry , and to create a new connection entry 
             * with new port number
             */
            OfcGetFsofcControllerConnEntry.
                MibObject.i4FsofcControllerIpAddrType =
                pOfcSetFsofcControllerConnEntry->
                MibObject.i4FsofcControllerIpAddrType;

            OfcGetFsofcControllerConnEntry.
                MibObject.i4FsofcControllerIpAddressLen =
                pOfcSetFsofcControllerConnEntry->
                MibObject.i4FsofcControllerIpAddressLen;

            MEMCPY (OfcGetFsofcControllerConnEntry.
                    MibObject.au1FsofcControllerIpAddress,
                    pOfcSetFsofcControllerConnEntry->
                    MibObject.au1FsofcControllerIpAddress,
                    OfcGetFsofcControllerConnEntry.
                    MibObject.i4FsofcControllerIpAddressLen);

            OfcGetFsofcControllerConnEntry.
                MibObject.i4FsofcControllerConnAuxId =
                pOfcSetFsofcControllerConnEntry->
                MibObject.i4FsofcControllerConnAuxId;

            OfcGetFsofcControllerConnEntry.MibObject.u4FsofcContextId =
                pOfcSetFsofcControllerConnEntry->MibObject.u4FsofcContextId;

            /* 
             * Check whether the Conn Entry  is present or not 
             */
            pOfcFsofcControllerConnEntry =
                RBTreeGet (gOfcGlobals.OfcGlbMib.FsofcControllerConnTable,
                           (tRBElem *) & OfcGetFsofcControllerConnEntry);

            if (pOfcFsofcControllerConnEntry == NULL)
            {
                CLI_SET_ERR (CLI_OFC_INVALID_ENTRY);
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                return OSIX_FAILURE;
            }

            if (pOfcFsofcControllerConnEntry->
                MibObject.i4FsofcControllerConnState != OFC_NOT_CONNECTED)
            {
                CLI_SET_ERR (CLI_OFC_CNTLR_CONN_STATE);
                *pu4ErrorCode = SNMP_ERR_NO_ACCESS;
                return OSIX_FAILURE;
            }
        }
    }

    if (pOfcIsSetFsofcControllerConnEntry->bFsofcControllerConnProtocol !=
        OSIX_FALSE)
    {
        if (pOfcIsSetFsofcControllerConnEntry->
            bFsofcControllerConnPort == OSIX_FALSE)
        {
            /* 
             * Check whether the Controller is in CONNECTED_STATE. If "YES"
             * This might introduce the traffic hit. Do not allow protocal
             * update. User to make sure proper socket closure by closing the
             * existing connection entry , and to create a new connection entry 
             * with new protocol value.
             */
            OfcGetFsofcControllerConnEntry.
                MibObject.i4FsofcControllerIpAddrType =
                pOfcSetFsofcControllerConnEntry->
                MibObject.i4FsofcControllerIpAddrType;

            OfcGetFsofcControllerConnEntry.
                MibObject.i4FsofcControllerIpAddressLen =
                pOfcSetFsofcControllerConnEntry->
                MibObject.i4FsofcControllerIpAddressLen;

            MEMCPY (OfcGetFsofcControllerConnEntry.
                    MibObject.au1FsofcControllerIpAddress,
                    pOfcSetFsofcControllerConnEntry->
                    MibObject.au1FsofcControllerIpAddress,
                    OfcGetFsofcControllerConnEntry.
                    MibObject.i4FsofcControllerIpAddressLen);

            OfcGetFsofcControllerConnEntry.
                MibObject.i4FsofcControllerConnAuxId =
                pOfcSetFsofcControllerConnEntry->
                MibObject.i4FsofcControllerConnAuxId;

            OfcGetFsofcControllerConnEntry.MibObject.u4FsofcContextId =
                pOfcSetFsofcControllerConnEntry->MibObject.u4FsofcContextId;

            /* 
             * Check whether the Conn Entry  is present or not 
             */
            pOfcFsofcControllerConnEntry =
                RBTreeGet (gOfcGlobals.OfcGlbMib.FsofcControllerConnTable,
                           (tRBElem *) & OfcGetFsofcControllerConnEntry);

            if (pOfcFsofcControllerConnEntry == NULL)
            {
                CLI_SET_ERR (CLI_OFC_INVALID_ENTRY);
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                return OSIX_FAILURE;
            }

            if (pOfcFsofcControllerConnEntry->
                MibObject.i4FsofcControllerConnState != OFC_NOT_CONNECTED)
            {
                CLI_SET_ERR (CLI_OFC_CNTLR_CONN_STATE);
                *pu4ErrorCode = SNMP_ERR_NO_ACCESS;
                return OSIX_FAILURE;
            }
        }
    }

    if (pOfcIsSetFsofcControllerConnEntry->bFsofcControllerRole != OSIX_FALSE)
    {
        /*Fill your check condition */
    }

    if (pOfcIsSetFsofcControllerConnEntry->bFsofcControllerConnEchoReqCount !=
        OSIX_FALSE)
    {
        /*Fill your check condition */
    }

    if (pOfcIsSetFsofcControllerConnEntry->bFsofcControllerIpAddress !=
        OSIX_FALSE)
    {
        CliStrToIp4Addr (pOfcSetFsofcControllerConnEntry->MibObject.
                         au1FsofcControllerIpAddress, au1TmpAddr);
        u4TmpAddr = INET_ADDR (au1TmpAddr);
        if (NetIpv4IfIsOurAddress (u4TmpAddr) == NETIPV4_SUCCESS)
        {
            CLI_SET_ERR (CLI_OFC_SELF_IP);
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return OSIX_FAILURE;
        }
    }

    if (pOfcIsSetFsofcControllerConnEntry->bFsofcControllerConnEntryStatus !=
        OSIX_FALSE)
    {
        if (pOfcSetFsofcControllerConnEntry->MibObject.
            i4FsofcControllerConnEntryStatus == ACTIVE)
        {
            MEMCPY (&OfcSetFsofcControllerConnEntry,
                    pOfcSetFsofcControllerConnEntry,
                    sizeof (tOfcFsofcControllerConnEntry));
            OfcSetFsofcControllerConnEntry.MibObject.
                i4FsofcControllerConnAuxId = OFC_ZERO;

            /* 
             * Check whether the Corresponding Primary node is already present 
             */
            pOfcFsofcControllerConnEntry =
                RBTreeGet (gOfcGlobals.OfcGlbMib.FsofcControllerConnTable,
                           (tRBElem *) & OfcSetFsofcControllerConnEntry);

            if ((pOfcFsofcControllerConnEntry == NULL) &&
                (pOfcSetFsofcControllerConnEntry->MibObject.
                 i4FsofcControllerConnAuxId == OFC_ZERO))
            {
                pOfcFsofcCfgEntry =
                    OfcGetFsofcCfgEntry (pOfcSetFsofcControllerConnEntry->
                                         MibObject.u4FsofcContextId);
                if (pOfcFsofcCfgEntry == NULL)
                {
                    CLI_SET_ERR (CLI_OFC_INVALID_CONTEXT_ID);
                    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                    return OSIX_FAILURE;
                }

                /* 
                 * Maximum number of controllers are per Context, Verify the number of entries
                 * Exceeded in that context. 
                 */
                if (pOfcFsofcCfgEntry->u4CurrConnEntries >= OFC_MAX_CONTROLLERS)
                {
                    CLI_SET_ERR (CLI_OFC_MAX_CNTLR_CONN_EXCEEDS);
                    *pu4ErrorCode = SNMP_ERR_NO_CREATION;
                    return OSIX_FAILURE;
                }
            }

            /* 
             * Check whether the node is already present 
             */
            MEMSET (&OfcGetFsofcControllerConnEntry, OFC_ZERO,
                    sizeof (tOfcFsofcControllerConnEntry));

            OfcGetFsofcControllerConnEntry.
                MibObject.i4FsofcControllerIpAddrType =
                pOfcSetFsofcControllerConnEntry->
                MibObject.i4FsofcControllerIpAddrType;

            OfcGetFsofcControllerConnEntry.
                MibObject.i4FsofcControllerIpAddressLen =
                pOfcSetFsofcControllerConnEntry->
                MibObject.i4FsofcControllerIpAddressLen;

            MEMCPY (OfcGetFsofcControllerConnEntry.
                    MibObject.au1FsofcControllerIpAddress,
                    pOfcSetFsofcControllerConnEntry->
                    MibObject.au1FsofcControllerIpAddress,
                    OfcGetFsofcControllerConnEntry.
                    MibObject.i4FsofcControllerIpAddressLen);

            OfcGetFsofcControllerConnEntry.
                MibObject.i4FsofcControllerConnAuxId =
                pOfcSetFsofcControllerConnEntry->
                MibObject.i4FsofcControllerConnAuxId;

            OfcGetFsofcControllerConnEntry.MibObject.u4FsofcContextId =
                pOfcSetFsofcControllerConnEntry->MibObject.u4FsofcContextId;

            pOfcFsofcControllerConnEntry =
                RBTreeGet (gOfcGlobals.OfcGlbMib.FsofcControllerConnTable,
                           (tRBElem *) & OfcGetFsofcControllerConnEntry);

            if ((pOfcFsofcControllerConnEntry != NULL) &&
                ((MEMCMP
                  (pOfcFsofcControllerConnEntry,
                   pOfcSetFsofcControllerConnEntry,
                   sizeof (tOfcFsofcControllerConnEntry)) != OFC_ZERO)))
            {
                CLI_SET_ERR (CLI_OFC_CNTLR_CONN_STATE);
                *pu4ErrorCode = SNMP_ERR_NO_ACCESS;
                return OSIX_FAILURE;
            }
        }
        else if (pOfcSetFsofcControllerConnEntry->MibObject.
                 i4FsofcControllerConnEntryStatus == DESTROY)
        {

            OfcGetFsofcControllerConnEntry.
                MibObject.i4FsofcControllerIpAddrType =
                pOfcSetFsofcControllerConnEntry->
                MibObject.i4FsofcControllerIpAddrType;

            OfcGetFsofcControllerConnEntry.
                MibObject.i4FsofcControllerIpAddressLen =
                pOfcSetFsofcControllerConnEntry->
                MibObject.i4FsofcControllerIpAddressLen;

            MEMCPY (OfcGetFsofcControllerConnEntry.
                    MibObject.au1FsofcControllerIpAddress,
                    pOfcSetFsofcControllerConnEntry->
                    MibObject.au1FsofcControllerIpAddress,
                    OfcGetFsofcControllerConnEntry.
                    MibObject.i4FsofcControllerIpAddressLen);

            OfcGetFsofcControllerConnEntry.
                MibObject.i4FsofcControllerConnAuxId =
                pOfcSetFsofcControllerConnEntry->
                MibObject.i4FsofcControllerConnAuxId;

            OfcGetFsofcControllerConnEntry.MibObject.u4FsofcContextId =
                pOfcSetFsofcControllerConnEntry->MibObject.u4FsofcContextId;

            /* 
             * Check whether the node is present or not 
             */
            pOfcFsofcControllerConnEntry =
                RBTreeGet (gOfcGlobals.OfcGlbMib.FsofcControllerConnTable,
                           (tRBElem *) & OfcGetFsofcControllerConnEntry);

            if (pOfcFsofcControllerConnEntry == NULL)
            {
                CLI_SET_ERR (CLI_OFC_INVALID_ENTRY);
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                return OSIX_FAILURE;
            }

            /* 
             * If the ConnEntryStatus is Destroy and the Entry is a Primary Connection, verify
             * there were no auxilary connections atached with this before deleting the primary
             * Primary Connection .
             */
            if (pOfcSetFsofcControllerConnEntry->MibObject.
                i4FsofcControllerConnAuxId == OFC_ZERO)
            {
                /* 
                 * Check whether the auxiliary nodes are already present 
                 */
                pOfcFsofcControllerConnEntry = pOfcSetFsofcControllerConnEntry;
                do
                {
                    if ((pOfcFsofcControllerConnEntry->MibObject.
                         u4FsofcContextId ==
                         pOfcSetFsofcControllerConnEntry->MibObject.
                         u4FsofcContextId)
                        &&
                        (MEMCMP
                         (pOfcFsofcControllerConnEntry->MibObject.
                          au1FsofcControllerIpAddress,
                          pOfcSetFsofcControllerConnEntry->MibObject.
                          au1FsofcControllerIpAddress,
                          sizeof (pOfcFsofcControllerConnEntry->MibObject.
                                  au1FsofcControllerIpAddress)) == OFC_ZERO))
                    {
                        if (pOfcFsofcControllerConnEntry->MibObject.
                            i4FsofcControllerConnAuxId == OFC_ZERO)
                        {
                            continue;
                        }
                        else
                        {
                            CLI_SET_ERR (CLI_OFC_AUXILARY_CONN_ERROR);
                            *pu4ErrorCode = SNMP_ERR_AUTHORIZATION_ERROR;
                            return OSIX_FAILURE;
                        }
                    }
                    else
                    {
                        break;
                    }
                }
                while ((pOfcFsofcControllerConnEntry =
                        RBTreeGetNext (gOfcGlobals.OfcGlbMib.
                                       FsofcControllerConnTable,
                                       pOfcFsofcControllerConnEntry,
                                       NULL)) != NULL);
            }
        }
    }

    if (pOfcIsSetFsofcControllerConnEntry->bFsofcControllerConnBand !=
        OSIX_FALSE)
    {
        /*Fill your check condition */
    }

    UNUSED_PARAM (i4RowStatusLogic);
    UNUSED_PARAM (i4RowCreateOption);
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  OfcTestAllFsofcIfTable
 Input       :  pu4ErrorCode
                pOfcSetFsofcIfEntry
                pOfcIsSetFsofcIfEntry
 Description :  This Routine Take the Indices &
                Test the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

INT4
OfcTestAllFsofcIfTable (UINT4 *pu4ErrorCode,
                        tOfcFsofcIfEntry * pOfcSetFsofcIfEntry,
                        tOfcIsSetFsofcIfEntry * pOfcIsSetFsofcIfEntry)
{
    tOfcFsofcCfgEntry  *pOfcFsofcCfgEntry = NULL;
    tOfcFsofcCfgEntry   OfcFsofcCfgEntry;
    tOfcFsofcIfEntry   *pOfcGetFsofcIfEntry = NULL;

    MEMSET (&OfcFsofcCfgEntry, OFC_ZERO, sizeof (tOfcFsofcCfgEntry));

    if (pOfcIsSetFsofcIfEntry->bFsofcVlanEgressPorts != OSIX_FALSE)
    {
        /*Fill your check condition */
    }
    if (pOfcIsSetFsofcIfEntry->bFsofcVlanUntaggedPorts != OSIX_FALSE)
    {
        /*Fill your check condition */
    }
    if (pOfcIsSetFsofcIfEntry->bFsofcIfContextId != OSIX_FALSE)
    {
        /*
         * Skip below validations if the Command is for Unmapping except
         * the checks which are inside if conditions
         */
        if (pOfcSetFsofcIfEntry->MibObject.u4FsofcIfContextId ==
            OFC_INVALID_CONTEXT)
        {
            OfcFsofcCfgEntry.MibObject.u4FsofcContextId =
                pOfcSetFsofcIfEntry->MibObject.u4FsofcContextId;
            pOfcFsofcCfgEntry = RBTreeGet (gOfcGlobals.OfcGlbMib.FsofcCfgTable,
                                           (tRBElem *) & OfcFsofcCfgEntry);
            if (pOfcFsofcCfgEntry == NULL)
            {
                CLI_SET_ERR (CLI_OFC_INVALID_CONTEXT_ID);
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return OSIX_FAILURE;
            }

            pOfcGetFsofcIfEntry = RBTreeGet (gOfcGlobals.OfcGlbMib.FsofcIfTable,
                                             (tRBElem *) (pOfcSetFsofcIfEntry));
            if ((pOfcGetFsofcIfEntry == NULL) ||
                ((pOfcGetFsofcIfEntry != NULL) &&
                 (pOfcGetFsofcIfEntry->MibObject.u4FsofcIfContextId !=
                  pOfcSetFsofcIfEntry->MibObject.u4FsofcContextId)))
            {
                CLI_SET_ERR (CLI_OFC_INVALID_CONTEXT_ID);
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return OSIX_FAILURE;
            }

            /*   
             * Validate the Port mapping for the Vlan, if the Port is logical Port 
             * Throw error for un mapping of context, if ports are mapped
             */
            if (OfcCheckOfcVlanInterfaceMapping (pOfcSetFsofcIfEntry) !=
                OFC_SUCCESS)
            {
                CLI_SET_ERR (CLI_OFC_PORT_MAPPING_EXISTS);
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                return OSIX_FAILURE;
            }

            return OSIX_SUCCESS;
        }
        /* 
         * Validate the Conext-ID to which the mapping to be done 
         * Return Error the the Given Context is not created and we are trying
         * to map the interface to this context 
         */
        if (OfcCheckInterfaceContextId (pOfcSetFsofcIfEntry) != OFC_SUCCESS)
        {
            CLI_SET_ERR (CLI_OFC_INVALID_CONTEXT_ID);
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return OSIX_FAILURE;
        }
        /* 
         * Validate the Interface is already mapped to different context 
         * Return Error if the Interface is already mapped to different Context 
         */
        if (OfcCheckInterfaceIsMapped (pOfcSetFsofcIfEntry) != OFC_SUCCESS)
        {
            CLI_SET_ERR (CLI_OFC_INVALID_MAPPING);
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return OSIX_FAILURE;
        }
    }

    return OSIX_SUCCESS;
}
