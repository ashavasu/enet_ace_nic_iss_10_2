/*****************************************************************************
 * Copyright (C) 2013 Aricent Inc . All Rights Reserved
 *
 * $Id: ofc131flow.c,v 1.14 2016/03/10 11:42:32 siva Exp $
 *
 * Description: This file contains routines for the flow processing for v1.3.1
 *****************************************************************************/

#include "ofcflow.h"
#include "ofccli.h"
#include "ofcacts.h"
#include "ofcmpmsg.h"
#include "cfa.h"
#include "ofcapi.h"
#include "ofcdefn.h"
#include "ofcsz.h"
#include "ofcidxsz.h"
#include "ofcidxgl.h"

#ifdef NPAPI_WANTED
#include "ofcnp.h"
#ifdef OPENFLOW_TCAM
#ifdef DPA_WANTED
#include "ofc131dpa.h"
#endif /* DPA_WANTED */
#endif /* OPENFLOW_TCAM */
#endif /* NPAPI_WANTED */

/*****************************************************************************
 * Function Name :  Ofc131FlowExactEntryAdd 
 * Description   :  This routine performs the addition of node
 *                  in SLL Match List of descending order of priority
 * Input         :  pExactEntryList - pointer to Flow Entry List to be sorted
 *                  pFlow           - pointer to Flow Entry to be added 
 * Output        :  NONE
 * Return        :  NONE
 *****************************************************************************/
PRIVATE VOID
Ofc131FlowExactEntryAdd (tOfcSll * pExactEntryList, tOfcFsofcFlowEntry * pFlow)
{
    tOfcFsofcFlowEntry *pPrevFlow = NULL;
    tOfcFsofcFlowEntry *pTempFlow = NULL;

    OFC_TRC_FUNC ((OFC_FN_ENTRY, "Ofc131FlowExactEntryAdd Entry\n"));

    TMO_SLL_Scan (pExactEntryList, pTempFlow, tOfcFsofcFlowEntry *)
    {
        if (pTempFlow != NULL)
        {
            if (pTempFlow->u2Priority < pFlow->u2Priority)
            {
                /* high priority than current node */
                break;
            }
            else
            {
                pPrevFlow = pTempFlow;
            }
        }
    }

    /* now as we got the position, insert into the list */
    if (pPrevFlow == NULL)
    {
        /* Node to be inserted in first position */
        TMO_SLL_Insert (pExactEntryList, NULL, &(pFlow->Node));
    }
    else if (pTempFlow == NULL)
    {
        /* Node to be inserted in last position */
        TMO_SLL_Insert (pExactEntryList, &(pPrevFlow->Node), &(pFlow->Node));
    }
    else
    {
        /* Node to be inserted in middle */
        TMO_SLL_Insert_In_Middle (pExactEntryList, &(pPrevFlow->Node),
                                  &(pFlow->Node), &(pTempFlow->Node));
    }

    OFC_TRC_FUNC ((OFC_FN_EXIT, "Ofc131FlowExactEntryAdd Exit\n"));
}

/*****************************************************************************
 * Function Name :  Ofc131FlowExactSort 
 * Description   :  This routine performs the sort for the SLL Match List and
 *                  updates the Exact Match String 
 * Input         :  pMatchList    - pointer to Flow Match List to be sorted
 *                  pu1ExactMatch - pointer to Exact Match to be sorted 
 * Output        :  NONE
 * Return        :  OFC_FAILURE/OFC_SUCCESS
 *****************************************************************************/
UINT1
Ofc131FlowExactSort (tOfcSll * pMatchList, UINT1 *pu1ExactMatch)
{
    tOfcWcFlowMatch    *pFlowMatch[OFC_FORTY];
    tOfcWcFlowMatch    *pMatchPtr = NULL;
    UINT2               u2ExLen = OFC_ZERO;    /* Exact Length */
    UINT2               u2TmpLen = OFC_ZERO;    /* Temp Length  */
    UINT1               u1Idx = OFC_ZERO;    /* Match Index  */
    UINT1               u1TmpIdx = OFC_ZERO;    /* Temp Index   */
    UINT1               u1SrtIdx = OFC_ZERO;    /* Sort Index   */
    INT1                i1SwpIdx = OFC_ZERO;    /* Swap Index   */

    OFC_TRC_FUNC ((OFC_FN_ENTRY, "Ofc131FlowExactSort Entry\n"));

    MEMSET (pFlowMatch, OFC_ZERO, sizeof (pFlowMatch));

    /* 
     * scan through the list and update the pointers
     * for each match field into the array
     */
    TMO_SLL_Scan (pMatchList, pMatchPtr, tOfcWcFlowMatch *)
    {
        pFlowMatch[u1Idx] = pMatchPtr;
        u1Idx++;
    }

    pMatchPtr = NULL;
    /* sort the array holding the pointers based on the type */
    for (u1SrtIdx = OFC_ONE; u1SrtIdx < u1Idx; u1SrtIdx++)
    {
        pMatchPtr = pFlowMatch[u1SrtIdx];
        for (i1SwpIdx = (INT1) (u1SrtIdx - OFC_ONE); (i1SwpIdx >= OFC_ZERO) &&
             (pMatchPtr->u2Type < pFlowMatch[i1SwpIdx]->u2Type); i1SwpIdx--)
        {
            pFlowMatch[i1SwpIdx + OFC_ONE] = pFlowMatch[i1SwpIdx];
        }
        pFlowMatch[i1SwpIdx + OFC_ONE] = pMatchPtr;
    }

    /* now that the array is sorted, copy the match fields to exact match */
    for (u1TmpIdx = OFC_ZERO; u1TmpIdx < u1Idx; u1TmpIdx++)
    {
        u2TmpLen = pFlowMatch[u1TmpIdx]->u1Length;
        u2ExLen = (UINT2) (u2ExLen + u2TmpLen);
        if (u2ExLen > MAX_OFC_EXACT_LENGTH)
        {
            OFC_TRC_FUNC ((OFC_FLOWTBL_TRC, "Exact Match exceeding "
                           " max length\n"));
            return OFC_FAILURE;
        }
        MEMCPY (pu1ExactMatch, pFlowMatch[u1TmpIdx]->FldValue.au1Fld, u2TmpLen);
        pu1ExactMatch = pu1ExactMatch + u2TmpLen;
    }
    OFC_TRC_FUNC ((OFC_FN_EXIT, "Ofc131FlowExactSort Exit\n"));
    return OFC_SUCCESS;
}

/*****************************************************************************
 * Function Name :  Ofc131FlowIsFlowMatched 
 * Description   :  This routine performs the comparison between the input Key
 *                  flow match and given flow entry flow match.
 * Input         :  u1MatchCount  - Number of Match Fields in the Key.
 *                  pKeyMatchList - pointer to Flow Match List to be matched
 *                  pFlow         - pointer to Flow entry, where the flow 
 *                                  match to match with exists.
 * Output        :  NONE
 * Return        :  OFC_FAILURE/OFC_SUCCESS
 *****************************************************************************/
UINT1
Ofc131FlowIsFlowMatched (UINT1 u1MatchCount, tOfcSll * pKeyMatchList,
                         tOfcFsofcFlowEntry * pFlow)
{
    tOfcWcFlowMatch     FlowMatch;    /* Flow Match */
    tOfcWcFlowMatch     KeyFlowMatch;    /* Input Flow Match */
    tOfcWcFlowMatch    *pFlowMatch = NULL;
    tOfcWcFlowMatch    *pKeyFlowMatch = NULL;
    UINT1               u1Lp = OFC_ZERO;
    UINT1               u1FlowMatchCntr = OFC_ZERO;

    OFC_TRC_FUNC ((OFC_FN_ENTRY, "FUNC:Ofc131FlowIsFlowMatched\n"));

    /*
     * This matching logic is simple, it picks up each field (OXM) 
     * from the input key and compares it with every field in the Flow Entry.
     * If there is a match, next field is picked from the pKey and
     * same process is continued. If all the fields as part of the key 
     * are matched successfully or all the match fields part of the Flow Entry
     * are matched successfully the match is found, else return failure.
     */
    /* If the match count is zero, it has to match all the flows */
    if (u1MatchCount == OFC_ZERO)
    {
        /* its matching */
        return OFC_SUCCESS;

    }

    /* For each flow match field in Input Key */
    TMO_SLL_Scan (pKeyMatchList, pKeyFlowMatch, tOfcWcFlowMatch *)
    {
        MEMSET (&FlowMatch, OFC_ZERO, sizeof (tOfcWcFlowMatch));
        MEMSET (&KeyFlowMatch, OFC_ZERO, sizeof (tOfcWcFlowMatch));

        KeyFlowMatch.u2Type = pKeyFlowMatch->u2Type;
        /* For each flow match field in Flow entry */
        TMO_SLL_Scan (&pFlow->MatchList, pFlowMatch, tOfcWcFlowMatch *)
        {
            FlowMatch.u2Type = pFlowMatch->u2Type;
            /* If type doesn't match, continue */
            if (KeyFlowMatch.u2Type != FlowMatch.u2Type)
            {
                continue;
            }

            for (u1Lp = OFC_ZERO; u1Lp < OFC_SIXTEEN; u1Lp++)
            {
                /* Mask and store the value from flow */
                FlowMatch.FldValue.au1Fld[u1Lp] =
                    pFlowMatch->FldValue.au1Fld[u1Lp] &
                    pFlowMatch->FldMask.au1Fld[u1Lp];

                /* Mask and store the value from input match */
                KeyFlowMatch.FldValue.au1Fld[u1Lp] =
                    pKeyFlowMatch->FldValue.au1Fld[u1Lp] &
                    pFlowMatch->FldMask.au1Fld[u1Lp];
            }

            /* Compare the values in key and flow entry flow match field */
            if (MEMCMP (FlowMatch.FldValue.au1Fld,
                        KeyFlowMatch.FldValue.au1Fld, OFC_SIXTEEN) == OFC_ZERO)
            {
                u1FlowMatchCntr++;
            }
            else
            {
                /* Not Matching */
                return OFC_FAILURE;
            }

            if (u1FlowMatchCntr == pFlow->u1FlowMatchCount)
            {
                /* all fields in the Flow Entry are matched */
                return OFC_SUCCESS;
            }

            if (u1FlowMatchCntr == u1MatchCount)
            {
                /* verified all the match fields as part of input */
                return OFC_SUCCESS;
            }
        }
        /*
         * If the key field is not present in the flow entry,
         * it means it is wildcarded. verify next field in key.
         */
    }
    OFC_TRC_FUNC ((OFC_FN_EXIT, "FUNC:Ofc131FlowIsFlowMatched\n"));
    return OFC_FAILURE;
}

/*****************************************************************************
 * Function Name :  Ofc131FlowMatchListFree 
 * Description   :  This routine performs deletion of MatchList from the 
 *                  Flow Entry
 * Input         :  i1TableId      - Table Id
 *                  pFlowMatchList - pointer to the Flow Match SLL
 * Output        :  None
 * Return        :  None
 *****************************************************************************/
VOID
Ofc131FlowMatchListFree (INT1 i1TableId, tOfcSll * pFlowMatchList)
{
    tOfcWcFlowMatch    *pFlowMatch = NULL;
    tOfcWcFlowMatch    *pNextFlowMatch = NULL;

    if (TMO_SLL_Count (pFlowMatchList) == 0)
    {
        return;
    }

    TMO_DYN_SLL_Scan (pFlowMatchList, pFlowMatch, pNextFlowMatch,
                      tOfcWcFlowMatch *)
    {
        TMO_SLL_Delete (pFlowMatchList, (tTMO_SLL_NODE *) pFlowMatch);
        OfcMemFreeForFlowMatch (i1TableId, (UINT1 *) pFlowMatch);
        if (NULL == pNextFlowMatch)
        {
            break;
        }
    }
}

/*****************************************************************************
 * Function Name :  Ofc131FlowEntryCleanUp 
 * Description   :  This routine performs deletion of FlowEntry from the 
 *                  Flow Table
 * Input         :  pFlow - pointer to the Flow entry
 * Output        :  None
 * Return        :  None
 *****************************************************************************/
PUBLIC VOID
Ofc131FlowEntryCleanUp (tOfcFsofcFlowEntry * pFlow)
{
    tOfcInstr          *pOfcInstr = NULL;
    tOfcInstr          *pOfcNextInstr = NULL;
    tOfcActs           *pOfcActs = NULL;
    tOfcActs           *pOfcNextActs = NULL;
    tOfcSetFld         *pOfcSetFld = NULL;
    tOfcSetFld         *pOfcNextSet = NULL;
    UINT1               u1TableId = OFC_ZERO;

    OFC_TRC_FUNC ((OFC_FN_ENTRY, "FUNC:Ofc131FlowEntryCleanUp\n"));

    u1TableId = (UINT1) pFlow->u4TableIndex;

    TMO_DYN_SLL_Scan (&pFlow->InstrList, pOfcInstr, pOfcNextInstr, tOfcInstr *)
    {
        if ((pOfcInstr->u2Type == OFCIT_WRITE_ACTIONS) ||
            (pOfcInstr->u2Type == OFCIT_APPLY_ACTIONS))
        {
            TMO_DYN_SLL_Scan (&pOfcInstr->Instr.Actions.ActList, pOfcActs,
                              pOfcNextActs, tOfcActs *)
            {
                if (pOfcActs->u2Type == OFCAT_SET_FIELD)
                {
                    TMO_DYN_SLL_Scan (&pOfcActs->Acts.SetFld.SetFldList,
                                      pOfcSetFld, pOfcNextSet, tOfcSetFld *)
                    {
                        TMO_SLL_Delete (&pOfcActs->Acts.SetFld.SetFldList,
                                        (tTMO_SLL_NODE *) pOfcSetFld);
                        OfcMemFreeForSetField (u1TableId, (UINT1 *) pOfcSetFld);
                        if (NULL == pOfcNextSet)
                        {
                            break;
                        }
                    }
                }
                TMO_SLL_Delete (&pOfcInstr->Instr.Actions.ActList,
                                (tTMO_SLL_NODE *) pOfcActs);
                OfcMemFreeForAction (u1TableId, (UINT1 *) pOfcActs);
                if (NULL == pOfcNextActs)
                {
                    break;
                }
            }
        }
        TMO_SLL_Delete (&pFlow->InstrList, (tTMO_SLL_NODE *) pOfcInstr);
        OfcMemFreeForInstruction (u1TableId, (UINT1 *) pOfcInstr);
        if (NULL == pOfcNextInstr)
        {
            break;
        }
    }
    OFC_TRC_FUNC ((OFC_FN_EXIT, "FUNC:Ofc131FlowEntryCleanUp\n"));
}

/*****************************************************************************
 * Function Name :  Ofc131FlowAddFlowEntry 
 * Description   :  This routine performs addition of FlowEntry into the 
 *                  Flow Table
 * Input         :  pFlow - pointer to the Flow entry
 * Output        :  None
 * Return        :  OFC_FAILURE/OFC_SUCESS 
 *****************************************************************************/
PRIVATE INT4
Ofc131FlowAddFlowEntry (tOfcFsofcFlowEntry * pFlow)
{
    static CHR1         au1ActionString[OFC_ACTION_STRING_MAX_LEN];

    OFC_TRC_FUNC ((OFC_FN_ENTRY, "FUNC:Ofc131FlowAddFlowEntry\n"));

    /* Fill-in the string for instructions, actions and set fields */
    MEMSET (au1ActionString, OFC_ZERO, OFC_ACTION_STRING_MAX_LEN);
    Ofc131InstructActionStructToStr (au1ActionString, &pFlow->InstrList);
    MEMCPY (pFlow->MibObject.au1FsofcFlowOutputAction, au1ActionString,
            OFC_ACTION_STRING_MAX_LEN);
    pFlow->MibObject.i4FsofcFlowOutputActionLen = OFC_ACTION_STRING_MAX_LEN;

    /* Fill-in the string for the match fields */
    MEMSET (au1ActionString, OFC_ZERO, OFC_ACTION_STRING_MAX_LEN);
    Ofc131MatchFieldsStructToStr (au1ActionString, &pFlow->MatchList);
    MEMCPY (pFlow->MibObject.au1FsofcFlowMatchField, au1ActionString,
            OFC_ACTION_STRING_MAX_LEN);
    pFlow->MibObject.i4FsofcFlowMatchFieldLen = OFC_ACTION_STRING_MAX_LEN;

    pFlow->MibObject.u4FsofcFlowDurationSec = OsixGetSysUpTime ();

    if (RBTreeAdd (gOfcGlobals.OfcGlbMib.FsofcFlowTable, (tRBElem *) pFlow)
        != RB_SUCCESS)
    {
        OFC_TRC_FUNC ((OFC_FLOWTBL_TRC, "Add failed in Flow MIB Table\n"));
        return OFC_FAILURE;
    }
    OFC_TRC_FUNC ((OFC_FN_EXIT, "FUNC:Ofc131FlowAddFlowEntry\n"));
    return OFC_SUCCESS;
}

/*****************************************************************************
 * Function Name :  Ofc131FlowDeleteFlow 
 * Description   :  This routine handles Flow Deletion request 
 * Input         :  pOfc            - Pointer to Context Entry
 *                  pFlow           - pointer to the Flow Entry
 *                  u1FlowRemReason - Reason to delete the Flow 
 * Output        :  None
 * Return        :  OFC_FAILURE/OFC_SUCESS 
 *****************************************************************************/
INT4
Ofc131FlowDeleteFlow (tOfcFsofcCfgEntry * pOfc, tOfcFsofcFlowEntry * pFlow,
                      UINT1 u1FlowRemReason)
{
    tOfcFsofcControllerConnEntry CntrlConnEntry;
    tSNMP_OCTET_STRING_TYPE CntrlIpAddrSrc;
    tSNMP_OCTET_STRING_TYPE NextCntrlIpAddr;
    tOfcFsofcFlowTable *pFlowTable = NULL;
    tOfp131Match       *pOfcMatch = NULL;
    tOfp131FlowRem     *pFlowRemMsg = NULL;
    UINT4               u4HashIndex = OFC_ZERO;
    UINT4               u4ContextId = OFC_ZERO;
    UINT4               u4NextContextId = OFC_ZERO;
    UINT4               u4DurationSec = OFC_ZERO;
    INT4                i4RetValue = OFC_ZERO;
    INT4                i4CntrlRole = OFC_ZERO;
    INT4                i4CntrlIpAddrType = OFC_ZERO;
    INT4                i4NextCntrlIpAddrType = OFC_ZERO;
    INT4                i4CntrlConnAuxId = OFC_ZERO;
    INT4                i4NextCntrlConnAuxId = OFC_ZERO;
    UINT1               au1CntrlIpAddr[OFC_TWO_FIFTY_SIX];
    UINT1               au1NextCntrlIpAddr[OFC_TWO_FIFTY_SIX];

#ifdef NPAPI_WANTED
#ifdef OPENFLOW_TCAM
    tOfcHwInfo          OfcHwInfo;
    MEMSET (&OfcHwInfo, OFC_ZERO, sizeof (tOfcHwInfo));
#ifdef DPA_WANTED
    UINT1               u1DpaTabId = OFC_ZERO;
#endif /* DPA_WANTED */
#endif /* OPENFLOW_TCAM */
#endif /* NPAPI_WANTED */

    OFC_TRC_FUNC ((OFC_FN_ENTRY, "FUNC:Ofc131FlowDeleteFlow\n"));

    MEMSET (au1CntrlIpAddr, OFC_ZERO, OFC_TWO_FIFTY_SIX);
    MEMSET (au1NextCntrlIpAddr, OFC_ZERO, OFC_TWO_FIFTY_SIX);

    CntrlIpAddrSrc.pu1_OctetList = au1CntrlIpAddr;
    NextCntrlIpAddr.pu1_OctetList = au1NextCntrlIpAddr;

    /* Stop Timers if running */
    if (pFlow->OfcHardTimer.tmrNode.u2Flags & OFC_TMR_RUNNING)
    {
        OfcTmrStopTmr (&(pFlow->OfcHardTimer));
    }

    if (pFlow->OfcIdleTimer.tmrNode.u2Flags & OFC_TMR_RUNNING)
    {
        OfcTmrStopTmr (&(pFlow->OfcIdleTimer));
    }

    /* Get the Flow Table entry */
    pFlowTable = (tOfcFsofcFlowTable *)
        OfcGetFlowTableEntry (pOfc->MibObject.u4FsofcContextId,
                              pFlow->u4TableIndex);

    if (pFlowTable == NULL)
    {
        return OFC_FAILURE;
    }

    if (pFlow->u1IsFlowWild)
    {
        /* Deleting Entry from HASH Tables */
        u4HashIndex = pFlow->u2Priority % OFC_MAX_FLOWS;
        TMO_HASH_Delete_Node (pFlowTable->pFlowtable, &(pFlow->Node),
                              u4HashIndex);
    }
    else                        /* it is exact match */
    {
        /* RBTree deletion will be taken by the caller */
    }

    /* Delete SNMP Tables Entry */
    RBTreeRem (gOfcGlobals.OfcGlbMib.FsofcFlowTable, pFlow);
    OfcIndexMgrRelIndex (OFC_ONE, pFlow->MibObject.u4FsofcFlowIndex);

    OFC_TRC_FUNC ((OFC_FLOWTBL_TRC, "Calling NPAPI for Delete Flow"
                   " in HW Tables\n"));
#ifdef NPAPI_WANTED
#ifdef OPENFLOW_TCAM
    OfcHwInfo.OfcVer = OFC_VER_131;
    OfcHwInfo.OfcCmd = OFC_NP_FLOW_DELETE;
#ifdef DPA_WANTED
    /* Get the DPA table Id from Local Table ID */
    if (OfcGetDpaTableId ((UINT1) pFlow->u4TableIndex,
                          &u1DpaTabId) != OFC_SUCCESS)
    {
        return OFC_FAILURE;
    }
    OfcHwInfo.OfcHwEntry.OfcHwFlowInfo.u1TableId = u1DpaTabId;
#else
    OfcHwInfo.OfcHwEntry.OfcHwFlowInfo.u1TableId = (UINT1) pFlow->u4TableIndex;
#endif /* DPA_WANTED */
    OfcHwInfo.OfcHwEntry.OfcHwFlowInfo.u2Priority = pFlow->u2Priority;
    OfcHwInfo.OfcHwEntry.OfcHwFlowInfo.u8Cookie.u4Hi = pFlow->u8Cookie.u4Hi;
    OfcHwInfo.OfcHwEntry.OfcHwFlowInfo.u8Cookie.u4Lo = pFlow->u8Cookie.u4Lo;

    /* Instructions and Timeouts are not required for delete flow */
    OfcHwInfo.OfcHwEntry.OfcHwFlowInfo.pMatchList = &pFlow->MatchList;

    i4RetValue = OfcFsNpHwConfigOfcInfo (&OfcHwInfo);
    if (i4RetValue == FNP_FAILURE)
    {
        OFC_TRC_FUNC ((OFC_FLOWTBL_TRC,
                       "Failed to Delete Flow in HW Tables\n"));
        return OFC_FAILURE;
    }
    return OFC_SUCCESS;
#endif /* OPENFLOW_TCAM */
#endif /* NPAPI_WANTED */

    /*
     * No need to send the flow removal notifications for the entries deleted
     * as part of add request.
     */
    if ((pFlow->u2Flags & OFPFF_SEND_FLOW_REM) && (u1FlowRemReason != OFC_FIVE))
    {
        /* Allocate memory from mem-pool */
        pFlowRemMsg =
            (tOfp131FlowRem *) MemAllocMemBlk (OFC_FLOW_REM_MSG_POOLID);
        if (pFlowRemMsg == NULL)
        {
            /* MemPool Allocation failed */
            OFC_TRC_FUNC ((OFC_FLOWTBL_TRC, "MemPool Allocated Failed for"
                           "pFlowRemMsg, return FAILURE\n"));
            /* Free FlowMatch SLL */
            Ofc131FlowMatchListFree ((INT1) pFlow->u4TableIndex,
                                     &pFlow->MatchList);
            Ofc131FlowEntryCleanUp (pFlow);
            OfcMemFreeForFlowEntry ((UINT1) pFlow->u4TableIndex,
                                    (UINT1 *) pFlow);
            return OFC_FAILURE;
        }

        MEMSET (pFlowRemMsg, OFC_ZERO, sizeof (tOfp131FlowRem));

        u4DurationSec = OsixGetSysUpTime ();
        pFlowRemMsg->u2Priority = pFlow->u2Priority;
        pFlowRemMsg->u1Reason = u1FlowRemReason;
        pFlowRemMsg->u4NSec = OFC_ZERO;
        pFlowRemMsg->u4Sec = ((u4DurationSec) -
                              (pFlow->MibObject.u4FsofcFlowDurationSec));
        pFlowRemMsg->u2IdleTimeOut =
            (UINT2) pFlow->MibObject.u4FsofcFlowIdleTimeout;
        pFlowRemMsg->u2HardTimeOut =
            (UINT2) pFlow->MibObject.u4FsofcFlowHardTimeout;
        /* Changed the Byte Count and Packet count size based on the 1.3.1 Opneflow Spec */
        pFlowRemMsg->u8ByteCount.u4Lo = pFlow->MibObject.u8FsofcFlowByteCount.lsn;
        pFlowRemMsg->u8ByteCount.u4Hi = pFlow->MibObject.u8FsofcFlowByteCount.msn;
        pFlowRemMsg->u8PktCount.u4Lo = pFlow->MibObject.u8FsofcFlowPacketCount.lsn;
        pFlowRemMsg->u8PktCount.u4Hi = pFlow->MibObject.u8FsofcFlowPacketCount.msn;
        pFlowRemMsg->u8Cookie.u4Lo = pFlow->u8Cookie.u4Lo;
        pFlowRemMsg->u8Cookie.u4Hi = pFlow->u8Cookie.u4Hi;

#ifdef DPA_WANTED
        if (OfcGetDpaTableId ((UINT1) pFlow->MibObject.u4FsofcTableIndex,
                              &pFlowRemMsg->u1TableId) == OFC_FAILURE)
        {
            return OFC_FAILURE;
        }
#else
        pFlowRemMsg->u1TableId = (UINT1) pFlow->MibObject.u4FsofcTableIndex;
#endif
        pFlowRemMsg->u2Priority = pFlow->u2Priority;

        /* Allocate from Flow Match Mempool */
        pOfcMatch = (tOfp131Match *) MemAllocMemBlk (OFC_FLOW_MATCH_TLV_POOLID);
        if (pOfcMatch == NULL)
        {
            /* MemPool Allocation failed */
            OFC_TRC_FUNC ((OFC_FLOWTBL_TRC, "MemPool Allocated Failed for"
                           "pOfcMatch, return FAILURE\n"));
            MemReleaseMemBlock (OFC_FLOW_REM_MSG_POOLID, (UINT1 *) pFlowRemMsg);
            /* Free FlowMatch SLL */
            Ofc131FlowMatchListFree ((INT1) pFlow->u4TableIndex,
                                     &pFlow->MatchList);
            Ofc131FlowEntryCleanUp (pFlow);
            OfcMemFreeForFlowEntry ((UINT1) pFlow->u4TableIndex,
                                    (UINT1 *) pFlow);
            return OFC_FAILURE;
        }
        Ofc131PktMatchStructToTlv (&pFlow->MatchList, pFlow->u1FlowMatchCount,
                                   pOfcMatch);

        /* Copy the Flow Match */
        MEMCPY (&(pFlowRemMsg->OfpMatch), pOfcMatch,
                OSIX_HTONS (pOfcMatch->u2Length));

        i4RetValue = nmhGetFirstIndexFsofcControllerConnTable (&u4ContextId,
                                                               &i4CntrlIpAddrType,
                                                               &CntrlIpAddrSrc,
                                                               &i4CntrlConnAuxId);

        while (SNMP_SUCCESS == i4RetValue)
        {
            MEMSET (&CntrlConnEntry, OFC_ZERO,
                    sizeof (tOfcFsofcControllerConnEntry));

            if (pOfc->MibObject.u4FsofcContextId != u4ContextId)
            {
                /*
                 * Send the Flow Removal Async msgs to controllers which belongs 
                 * to current context Only
                 */
                continue;
            }

            CntrlConnEntry.MibObject.u4FsofcContextId = u4ContextId;
            CntrlConnEntry.MibObject.i4FsofcControllerIpAddrType =
                i4CntrlIpAddrType;
            MEMCPY (CntrlConnEntry.MibObject.au1FsofcControllerIpAddress,
                    CntrlIpAddrSrc.pu1_OctetList, CntrlIpAddrSrc.i4_Length);
            CntrlConnEntry.MibObject.i4FsofcControllerIpAddressLen =
                CntrlIpAddrSrc.i4_Length;
            CntrlConnEntry.MibObject.i4FsofcControllerConnAuxId =
                i4CntrlConnAuxId;

            if (OfcGetAllFsofcControllerConnTable (&CntrlConnEntry)
                == OSIX_SUCCESS)
            {
                i4CntrlRole = CntrlConnEntry.MibObject.i4FsofcControllerRole;

                if ((OFC_CONNROLE_MASTER == i4CntrlRole) ||
                    (OFC_CONNROLE_EQUAL == i4CntrlRole))
                {
                    if ((CntrlConnEntry.au4FlowRemovedMask[OFC_ZERO] >>
                         u1FlowRemReason) & OFC_ONE)
                    {
                        OFC_TRC_FUNC ((OFC_FLOWTBL_TRC, "Notification for Flow"
                                       "Removal\n"));
                        Ofc131PktFlowRemSend (pFlowRemMsg, &CntrlConnEntry,
                                              OSIX_HTONS (pOfcMatch->u2Length));
                    }
                }
                else if (OFC_CONNROLE_SLAVE == i4CntrlRole)
                {
                    if ((CntrlConnEntry.au4FlowRemovedMask[OFC_ONE] >>
                         u1FlowRemReason) & OFC_ONE)
                    {
                        /* */
                        OFC_TRC_FUNC ((OFC_FLOWTBL_TRC, "Notification for Flow"
                                       " Removal\n"));
                        Ofc131PktFlowRemSend (pFlowRemMsg, &CntrlConnEntry,
                                              OSIX_HTONS (pOfcMatch->u2Length));
                    }
                }
            }
            i4RetValue =
                nmhGetNextIndexFsofcControllerConnTable (u4ContextId,
                                                         &u4NextContextId,
                                                         i4CntrlIpAddrType,
                                                         &i4NextCntrlIpAddrType,
                                                         &CntrlIpAddrSrc,
                                                         &NextCntrlIpAddr,
                                                         i4CntrlConnAuxId,
                                                         &i4NextCntrlConnAuxId);
            u4ContextId = u4NextContextId;
            i4CntrlIpAddrType = i4NextCntrlIpAddrType;
            i4CntrlConnAuxId = i4NextCntrlConnAuxId;
            MEMCPY (CntrlIpAddrSrc.pu1_OctetList, NextCntrlIpAddr.pu1_OctetList,
                    CntrlIpAddrSrc.i4_Length);
            CntrlIpAddrSrc.i4_Length = NextCntrlIpAddr.i4_Length;
        }
        MemReleaseMemBlock (OFC_FLOW_MATCH_TLV_POOLID, (UINT1 *) pOfcMatch);
        MemReleaseMemBlock (OFC_FLOW_REM_MSG_POOLID, (UINT1 *) pFlowRemMsg);
    }
    /* Free FlowMatch SLL */
    Ofc131FlowMatchListFree ((INT1) pFlow->u4TableIndex, &pFlow->MatchList);
    Ofc131FlowEntryCleanUp (pFlow);
    OfcMemFreeForFlowEntry ((UINT1) pFlow->u4TableIndex, (UINT1 *) pFlow);
    OFC_TRC_FUNC ((OFC_FN_EXIT, "FUNC:Ofc131FlowDeleteFlow\n"));
    return OFC_SUCCESS;
}

/******************************************************************************
 * Function Name :  Ofc131FlowDeleteAllFlows
 * Description   :  This routine deletes all flow entries from all flow tables
 * Input         :  pOfc - Pointer to Context Id
 * Output        :  None
 * Return        :  None 
 *****************************************************************************/
VOID
Ofc131FlowDeleteAllFlows (tOfcFsofcCfgEntry * pOfc)
{
    tRBExactFlowEntry  *pExactFlowEntry = NULL;
    tOfcFsofcFlowTable *pFlowTable = NULL;
    tOfcFsofcFlowEntry *pFlow = NULL;
    tOfcFsofcFlowEntry *pTempFlow = NULL;
    UINT4               u4HashIndex = OFC_ZERO;
    UINT1               u1TableId = OFC_ZERO;

    OFC_TRC_FUNC ((OFC_FN_ENTRY, "FUNC:Ofc131FlowDeleteAllFlows\n"));

    for (u1TableId = OFC_ZERO; u1TableId < pOfc->u2NumOfTables; u1TableId++)
    {
        pFlowTable = (tOfcFsofcFlowTable *)
            OfcGetFlowTableEntry (pOfc->MibObject.u4FsofcContextId, u1TableId);

        if (pFlowTable == NULL)
        {
            /* continue checking in other tables */
            continue;
        }

        /* delete all the flows from the RB tree, exact match */
        pExactFlowEntry = (tRBExactFlowEntry *)
            RBTreeGetFirst (pFlowTable->ExactFlowEntry);
        while (pExactFlowEntry != NULL)
        {
            /* for ever RB node, traverse the SLL */
            TMO_DYN_SLL_Scan (&pExactFlowEntry->FlowEntryList,
                              pFlow, pTempFlow, tOfcFsofcFlowEntry *)
            {
                TMO_SLL_Delete (&pExactFlowEntry->FlowEntryList,
                                (tTMO_SLL_NODE *) pFlow);
                Ofc131FlowDeleteFlow (pOfc, pFlow, OFPRR_DELETE);
            }
            /* remove the RBTree node */
            RBTreeRemove (pFlowTable->ExactFlowEntry,
                          (tRBElem *) pExactFlowEntry);
            /* release the mempool */
            MemReleaseMemBlock (OFC_EXACT_ENTRY_POOLID,
                                (UINT1 *) pExactFlowEntry);
            /* get the current first entry */
            pExactFlowEntry = (tRBExactFlowEntry *)
                RBTreeGetFirst (pFlowTable->ExactFlowEntry);
        }

        pFlow = NULL;
        pTempFlow = NULL;
        /* delete all the flows from the Hash Table, wild carded */
        TMO_HASH_Scan_Table (pFlowTable->pFlowtable, u4HashIndex)
        {
            TMO_HASH_DYN_Scan_Bucket (pFlowTable->pFlowtable, u4HashIndex,
                                      pFlow, pTempFlow, tOfcFsofcFlowEntry *)
            {
                /*
                 * The return value is not checked intentionally here, as
                 * there is no need to generate an error message when the
                 * flow delete fails. We hope the timer expiry will take
                 * care of deletion later on.
                 */
                Ofc131FlowDeleteFlow (pOfc, pFlow, OFPRR_DELETE);
            }
        }
    }
    OFC_TRC_FUNC ((OFC_FN_EXIT, "FUNC:Ofc131FlowDeleteAllFlows\n"));
}

/*****************************************************************************
 * Function Name :  Ofc131FlowLookupForPipeLine
 * Description   :  This routine searches for the least wildcard matching Flow
 *                  Entry (Best Match) for the given input flow match.
 * Input         :  pOfc             - Pointer to the OFC Client Instance
 *                  pMatchKeyList    - Pointer to the input Flow Match
 *                  pu1KeyExactMatch - Pointer to the Exact Match 
 *                  u1TableIndex     - Table Index to be looked up                
 * Output        :  None                                                      
 * Return        :  Pointer to Matching Flow entry                            
 *****************************************************************************/
tOfcFsofcFlowEntry *
Ofc131FlowLookupForPipeLine (tOfcFsofcCfgEntry * pOfc, tOfcSll * pMatchKeyList,
                             UINT1 *pu1KeyExactMatch, UINT1 u1TableIndex)
{
    tRBExactFlowEntry   ExactFlowEntry;
    tOfcFsofcFlowEntry  FlowEntry;
    tRBExactFlowEntry  *pExactFlowEntry = NULL;
    tOfcFsofcFlowTable *pFlowTable = NULL;
    tOfcFsofcFlowEntry *pFlow = NULL;
    tOfcFsofcFlowEntry *pLessWcFlow = NULL;    /* Flow with least wild cards */
    UINT4               u4HashIndex = OFC_ZERO;

    OFC_TRC_FUNC ((OFC_FN_ENTRY, "FUNC:Ofc131FlowLookupForPipeLine\n"));

    MEMSET (&ExactFlowEntry, OFC_ZERO, sizeof (tRBExactFlowEntry));
    MEMSET (&FlowEntry, OFC_ZERO, sizeof (tOfcFsofcFlowEntry));

    /*
     * This look-up is used for pipeline process alone. we will not be getting
     * the priority, only flow match key along with the table Id to search 
     * is passed. The criteria is to fetch the best match (matching Flow entry
     * with least wild cards and highest priority).
     */
    pFlowTable = (tOfcFsofcFlowTable *)
        OfcGetFlowTableEntry (pOfc->MibObject.u4FsofcContextId, u1TableIndex);

    if (pFlowTable == NULL)
    {
        /* Table doesn't exist */
        return NULL;
    }

    MEMCPY (ExactFlowEntry.au1ExactMatch, pu1KeyExactMatch,
            MAX_OFC_EXACT_LENGTH);
    pExactFlowEntry = RBTreeGet (pFlowTable->ExactFlowEntry,
                                 (tRBElem *) & ExactFlowEntry);

    if (pExactFlowEntry != NULL)
    {
        /* we got a match! */
        /* get only first node, as the highest priority stays first */
        pFlow = TMO_SLL_First (&pExactFlowEntry->FlowEntryList);
        if (pFlow != NULL)
        {
            /* exact match flow */
            return pFlow;
        }
        else
        {
            /* this should never happen */
            return NULL;
        }
    }

    pFlow = NULL;
    /* start scanning the table for each hash bucket */
    TMO_HASH_Scan_Table (pFlowTable->pFlowtable, u4HashIndex)
    {
        /* for each flow entry, continue matching */
        TMO_HASH_Scan_Bucket (pFlowTable->pFlowtable, u4HashIndex, pFlow,
                              tOfcFsofcFlowEntry *)
        {
            /* if no match fields, it is a table miss */
            if (pFlow->u1FlowMatchCount == OFC_ZERO)
            {
                /* only update when there are no other matching fields */
                if (pLessWcFlow == NULL)
                {
                    pLessWcFlow = pFlow;
                    continue;
                }
            }

            if (Ofc131FlowIsFlowMatched (pFlowTable->u1NumMatch,
                                         pMatchKeyList, pFlow) == OFC_SUCCESS)
            {
                if (pLessWcFlow == NULL)
                {
                    /* first matching entry */
                    pLessWcFlow = pFlow;
                    continue;
                }

                /*
                 * If the new flow entry has less wild cards matching,
                 * replace our existing flow entry with it.
                 */
                if (pLessWcFlow->u1FlowMatchCount < pFlow->u1FlowMatchCount)
                {
                    pLessWcFlow = pFlow;
                    continue;
                }

                if (pLessWcFlow->u1FlowMatchCount == pFlow->u1FlowMatchCount)
                {
                    /*
                     * If both the entries have same matching fields,
                     * use priority to distinguish the best match
                     */
                    if (pFlow->u2Priority > pLessWcFlow->u2Priority)
                    {
                        pLessWcFlow = pFlow;
                    }
                }
            }
        }
    }
    OFC_TRC_FUNC ((OFC_FN_EXIT, "FUNC:Ofc131FlowLookupForPipeLine\n"));
    return pLessWcFlow;
}

/******************************************************************************
 * Function Name :  Ofc131FlowAddFlow
 * Description   :  This routine adds the flow entry into the SNMP and
 *                  HASH Tables.
 * Input         :  pFlowTable - Pointer to the Flow Table Entry.
 *                  pFlow      - Pointer to the Flow Entry to be added.
 * Output        :  None
 * Return        :  OFC_FAILURE/OFC_SUCCESS
 *****************************************************************************/
PRIVATE UINT4
Ofc131FlowAddFlow (tOfcFsofcFlowTable * pFlowTable, tOfcFsofcFlowEntry * pFlow)
{
    UINT4               u4HashIndex = OFC_ZERO;
    UINT4               u4FlowIndex = OFC_ZERO;

#ifdef NPAPI_WANTED
#ifdef OPENFLOW_TCAM
    tOfcHwInfo          OfcHwInfo;
    INT4                i4RetValue = OFC_FAILURE;
    MEMSET (&OfcHwInfo, OFC_ZERO, sizeof (tOfcHwInfo));
#ifdef DPA_WANTED
    UINT1               u1DpaTabId = OFC_ZERO;
#endif /* DPA_WANTED */
#endif /* OPENFLOW_WANTED */
#endif /* NPAPI_WANTED */

    OFC_TRC_FUNC ((OFC_FN_ENTRY, "FUNC:Ofc131FlowAddFlow\n"));

    if ((u4FlowIndex = OfcIndexMgrGetIndexBasedOnFlag (OFC_ONE, TRUE))
        == OFC_ZERO)
    {
        return OFC_FAILURE;
    }
    pFlow->MibObject.u4FsofcFlowIndex = u4FlowIndex;

    OFC_TRC_FUNC ((OFC_FLOWTBL_TRC, "Calling NPAPI for Add Flow"
                   " in HW Tables\n"));

#ifdef NPAPI_WANTED
#ifdef OPENFLOW_TCAM
    OfcHwInfo.OfcVer = OFC_VER_131;
    OfcHwInfo.OfcCmd = OFC_NP_FLOW_ADD;
#ifdef DPA_WANTED
    /* Get the DPA table Id from Local Table ID */
    if (OfcGetDpaTableId ((UINT1) pFlow->u4TableIndex,
                          &u1DpaTabId) != OFC_SUCCESS)
    {
        return OFC_FAILURE;
    }
    OfcHwInfo.OfcHwEntry.OfcHwFlowInfo.u1TableId = u1DpaTabId;
#else
    OfcHwInfo.OfcHwEntry.OfcHwFlowInfo.u1TableId = (UINT1) pFlow->u4TableIndex;
#endif /* DPA_WANTED */
    OfcHwInfo.OfcHwEntry.OfcHwFlowInfo.u2Priority = pFlow->u2Priority;
    OfcHwInfo.OfcHwEntry.OfcHwFlowInfo.u8Cookie.u4Hi = pFlow->u8Cookie.u4Hi;
    OfcHwInfo.OfcHwEntry.OfcHwFlowInfo.u8Cookie.u4Lo = pFlow->u8Cookie.u4Lo;
    OfcHwInfo.OfcHwEntry.OfcHwFlowInfo.u2IdleTimeout =
        pFlow->MibObject.u4FsofcFlowIdleTimeout;
    OfcHwInfo.OfcHwEntry.OfcHwFlowInfo.u2HardTimeout =
        pFlow->MibObject.u4FsofcFlowHardTimeout;
    OfcHwInfo.OfcHwEntry.OfcHwFlowInfo.pMatchList = &pFlow->MatchList;
    OfcHwInfo.OfcHwEntry.OfcHwFlowInfo.pInstrList = &pFlow->InstrList;

    i4RetValue = OfcFsNpHwConfigOfcInfo (&OfcHwInfo);
    if (i4RetValue == FNP_FAILURE)
    {
        OFC_TRC_FUNC ((OFC_FLOWTBL_TRC, "Failed to Add Flow in HW Tables\n"));
        return OFC_FAILURE;
    }
#endif /* OPENFLOW_TCAM */
#endif /* NPAPI_WANTED */

    /* Adding Entry to SNMP Tables */
    if (Ofc131FlowAddFlowEntry (pFlow) != OFC_SUCCESS)
    {
        OfcIndexMgrRelIndex (OFC_ONE, u4FlowIndex);
        /* clean-up happens in the caller */
        return OFC_FAILURE;
    }

    if (pFlow->u1IsFlowWild)
    {
        /* Adding Entry to HASH Tables */
        u4HashIndex = pFlow->u2Priority % OFC_MAX_FLOWS;
        TMO_HASH_Add_Node (pFlowTable->pFlowtable, &(pFlow->Node),
                           u4HashIndex, NULL);
    }

    /* Starting the Timers */
    /*
     * On Successful Flow Add start the Idle timer & Hard Timer
     * for this flow for the configured values. 
     * If both the timers are equal, invoke only the Hard Timer.
     * Timers will not be invoked, if the configured timeout
     * values are ZERO.
     */
    if ((pFlow->MibObject.u4FsofcFlowHardTimeout != OFC_ZERO) &&
        (pFlow->MibObject.u4FsofcFlowIdleTimeout ==
         pFlow->MibObject.u4FsofcFlowHardTimeout))
    {
        /* Invoke only the hard timer */
        OfcTmrStartTmr (&(pFlow->OfcHardTimer), OFC_HARD_TIMER,
                        pFlow->MibObject.u4FsofcFlowHardTimeout);
    }
    else
    {
        /* Invoke both the timers */
        if (pFlow->MibObject.u4FsofcFlowHardTimeout != OFC_ZERO)
        {
            OfcTmrStartTmr (&(pFlow->OfcHardTimer), OFC_HARD_TIMER,
                            pFlow->MibObject.u4FsofcFlowHardTimeout);
        }

        if (pFlow->MibObject.u4FsofcFlowIdleTimeout != OFC_ZERO)
        {
            OfcTmrStartTmr (&(pFlow->OfcIdleTimer), OFC_IDLE_TIMER,
                            pFlow->MibObject.u4FsofcFlowIdleTimeout);
        }
    }
    OFC_TRC_FUNC ((OFC_FN_EXIT, "FUNC:Ofc131FlowAddFlow\n"));
    return OFC_SUCCESS;
}

/*****************************************************************************
 * Function Name :  Ofc131FlowHandleAddFlow  
 * Description   :  This routine handles the flow mod message for flow addtion.
 * Input         :  pConnPtr       - Connection Pointer.
 *                  pOfp131FlowMod - Pointer to the Flow Mod Message
 * Output        :  NONE
 * Return        :  OFC_FAILURE/OFC_SUCCESS
 *****************************************************************************/
PRIVATE UINT4
Ofc131FlowHandleAddFlow (tOfcFsofcCfgEntry * pOfc, VOID *pConnPtr,
                         tOfp131FlowMod * pOfp131FlowMod, UINT4 *pu4Error)
{
    tRBExactFlowEntry  *pExactFlowEntry = NULL;
    tRBExactFlowEntry  *pTempExactEntry = NULL;
    tOfcFsofcFlowTable *pFlowTable = NULL;
    tOfcFsofcFlowEntry *pFlow = NULL;
    tOfcFsofcFlowEntry *pNextFlow = NULL;
    tOfcFsofcFlowEntry *pTempFlow = NULL;
    tOfcFsofcFlowEntry *pNewFlow = NULL;
    tOfp131Match       *pOfpMatch = NULL;    /* Match as TLVs */
    UINT4               u4HashIndex = OFC_ZERO;
    UINT4               u4InstrLen = OFC_ZERO;
    UINT1               u1FlowMatchCnt = OFC_ZERO;
    UINT1               u1FlowFound = OFC_ZERO;
    UINT2               u2Length = OFC_ZERO;
    tOfcFsofcControllerConnEntry *pLocal = pConnPtr;

    OFC_TRC_FUNC ((OFC_FN_ENTRY, "FUNC:Ofc131FlowHandleAddFlow\n"));

    pFlowTable = (tOfcFsofcFlowTable *)
        OfcGetFlowTableEntry (pOfc->MibObject.u4FsofcContextId,
                              pOfp131FlowMod->u1TableId);

    if (pFlowTable == NULL)
    {
        return OFC_FAILURE;
    }

    /* Allocate memory for the flow entry */
    pNewFlow = OfcMemAllocForFlowEntry (pOfp131FlowMod->u1TableId);
    if (pNewFlow == NULL)
    {
        OFC131_TRC_FUNC ((OFC_PKT_TRC, "MemAllocMemBlk Failed for Flow entry"));
        return OFC_FAILURE;
    }
    MEMSET (pNewFlow, OFC_ZERO, sizeof (tOfcFsofcFlowEntry));

    pOfpMatch = (tOfp131Match *) ((VOID *) (pLocal->pRcvPkt +
                                            OFC131_FLOWMOD_OFPMATCH_OFFSET));

    /* Copy the TLV flowmatch fields into flow match SLL */
    if (Ofc131PktOxmParseMatchTlv ((INT1) pOfp131FlowMod->u1TableId, pOfpMatch,
                                   &pNewFlow->MatchList, &u1FlowMatchCnt,
                                   &pNewFlow->u1IsFlowWild,
                                   pu4Error) != OFC_SUCCESS)
    {
        OFC131_TRC_FUNC ((OFC_FLOWTBL_TRC, "Parsing Flow Match TLV Failed\n"));
        /* Free Match List */
        Ofc131FlowMatchListFree ((INT1) pOfp131FlowMod->u1TableId,
                                 &pNewFlow->MatchList);
        /* Release the mem-pools */
        OfcMemFreeForFlowEntry (pOfp131FlowMod->u1TableId, (UINT1 *) pNewFlow);
        return OFC_FAILURE;
    }

    /* set the wild card flag if number of match fields doesn't match */
    if (u1FlowMatchCnt != pFlowTable->u1NumMatch)
    {
        pNewFlow->u1IsFlowWild = OFC_ONE;
    }

    if (pNewFlow->u1IsFlowWild)
    {
        /* Check for overlapping and identical entries */
        u4HashIndex = pOfp131FlowMod->u2Priority % OFC_MAX_FLOWS;
        TMO_HASH_DYN_Scan_Bucket (pFlowTable->pFlowtable, u4HashIndex,
                                  pFlow, pTempFlow, tOfcFsofcFlowEntry *)
        {
            /* always check for priority */
            if (pFlow->u2Priority != pOfp131FlowMod->u2Priority)
            {
                continue;
            }

            /*
             * If no check_overlap flag, check for the flow entry with
             * identical match fields (all the match fields matching)
             */
            if (!(pOfp131FlowMod->u2Flags & OFC_CHECK_OVERLAP) &&
                (u1FlowMatchCnt != pFlow->u1FlowMatchCount))
            {
                continue;
            }

            if (Ofc131FlowIsFlowMatched (u1FlowMatchCnt, &pNewFlow->MatchList,
                                         pFlow) == OFC_SUCCESS)
            {
                /*
                 * If the check_overlap flag is set,
                 * check for the flow entries which can match the
                 * single packet as the new flow entry matches,
                 * with same priority.
                 * If there are such matches, flow entry addition has to fail.
                 */
                if (pOfp131FlowMod->u2Flags & OFC_CHECK_OVERLAP)
                {
                    /* Overlapping Flow Entry Exists, Send Error Msg */
                    /* Free Match List */
                    Ofc131FlowMatchListFree ((INT1) pOfp131FlowMod->u1TableId,
                                             &pNewFlow->MatchList);
                    /* Release the mem-pools */
                    OfcMemFreeForFlowEntry (pOfp131FlowMod->u1TableId,
                                            (UINT1 *) pNewFlow);
                    *pu4Error = OFC131_FLOWMOD_OVERLAP_EXIST_ERR;
                    return OFC_FAILURE;
                }
                u1FlowFound = OFC_ONE;
                break;
            }
        }
    }
    else
    {
        /* search the RB tree for possible identical */
        pExactFlowEntry = (tRBExactFlowEntry *)
            MemAllocMemBlk (OFC_EXACT_ENTRY_POOLID);
        if (pExactFlowEntry == NULL)
        {
            /* memalloc failed */
            Ofc131FlowEntryCleanUp (pNewFlow);
            OfcMemFreeForFlowEntry (pOfp131FlowMod->u1TableId,
                                    (UINT1 *) pNewFlow);
            return OFC_FAILURE;
        }
        MEMSET (pExactFlowEntry, OFC_ZERO, sizeof (tRBExactFlowEntry));

        /* parse the flow match SLL to fill in the exact match */
        if (Ofc131FlowExactSort (&pNewFlow->MatchList,
                                 pExactFlowEntry->au1ExactMatch) != OFC_SUCCESS)
        {
            /* updating exact match string failed */
            OFC131_TRC_FUNC ((OFC_PKT_TRC, "updating exact match string "
                              "failed for the new flow to be added\n"));
            /* Free FlowMatch SLL */
            Ofc131FlowMatchListFree ((INT1) pOfp131FlowMod->u1TableId,
                                     &pNewFlow->MatchList);
            Ofc131FlowEntryCleanUp (pNewFlow);
            OfcMemFreeForFlowEntry (pOfp131FlowMod->u1TableId,
                                    (UINT1 *) pNewFlow);
            return OFC_FAILURE;
        }

        pTempExactEntry = RBTreeGet (pFlowTable->ExactFlowEntry,
                                     (tRBElem *) pExactFlowEntry);

        if (pTempExactEntry != NULL)
        {
            /* for ever RB node, traverse the SLL */
            /* scan through the list and match the priority */
            TMO_DYN_SLL_Scan (&pTempExactEntry->FlowEntryList, pFlow,
                              pNextFlow, tOfcFsofcFlowEntry *)
            {
                /* always check for priority */
                if (pFlow->u2Priority != pOfp131FlowMod->u2Priority)
                {
                    continue;
                }

                if (pOfp131FlowMod->u2Flags & OFC_CHECK_OVERLAP)
                {
                    /* Overlapping Flow Entry Exists, Send Error Msg */
                    /* Free Match List */
                    Ofc131FlowMatchListFree ((INT1) pOfp131FlowMod->u1TableId,
                                             &pNewFlow->MatchList);
                    /* Release the mem-pools */
                    OfcMemFreeForFlowEntry (pOfp131FlowMod->u1TableId,
                                            (UINT1 *) pNewFlow);
                    *pu4Error = OFC131_FLOWMOD_OVERLAP_EXIST_ERR;
                    return OFC_FAILURE;
                }
                TMO_SLL_Delete (&pExactFlowEntry->FlowEntryList,
                                (tTMO_SLL_NODE *) pFlow);
                u1FlowFound = OFC_ONE;
                break;
            }
            /*
             * considering that matcing ones are deleted,
             * add this to the exact match SLL
             */
            Ofc131FlowExactEntryAdd (&pTempExactEntry->FlowEntryList, pNewFlow);
            MemReleaseMemBlock (OFC_EXACT_ENTRY_POOLID,
                                (UINT1 *) pExactFlowEntry);
        }
        else
        {
            TMO_SLL_Init (&pExactFlowEntry->FlowEntryList);
            Ofc131FlowExactEntryAdd (&pExactFlowEntry->FlowEntryList, pNewFlow);
            /* first node, add it to the RB Tree for this match */
            if (RBTreeAdd (pFlowTable->ExactFlowEntry,
                           (tRBElem *) pExactFlowEntry) != RB_SUCCESS)
            {
                /* Free Match List */
                Ofc131FlowMatchListFree ((INT1) pOfp131FlowMod->u1TableId,
                                         &pNewFlow->MatchList);
                /* Release the mem-pools */
                OfcMemFreeForFlowEntry (pOfp131FlowMod->u1TableId,
                                        (UINT1 *) pNewFlow);
                MemReleaseMemBlock (OFC_EXACT_ENTRY_POOLID,
                                    (UINT1 *) pExactFlowEntry);
                return OFC_FAILURE;
            }
        }
    }

    /* If there is an existing entry update counts */
    if (u1FlowFound)
    {
        if (!(pOfp131FlowMod->u2Flags & OFC_RESET_COUNTS))
        {
            MEMCPY (&(pNewFlow->MibObject.u4FsofcFlowDurationSec),
                    &(pFlow->MibObject.u4FsofcFlowDurationSec), OFC_FOUR);
            MEMCPY (&(pNewFlow->MibObject.u8FsofcFlowPacketCount),
                    &(pFlow->MibObject.u8FsofcFlowPacketCount), OFC_EIGHT);
            MEMCPY (&(pNewFlow->MibObject.u8FsofcFlowByteCount.msn),
                    &(pFlow->MibObject.u8FsofcFlowByteCount), OFC_EIGHT);
        }

        /* Found the identical entry, delete it */
        Ofc131FlowDeleteFlow (pOfc, pFlow, OFC_FIVE);
    }
    pNewFlow->u2Priority = pOfp131FlowMod->u2Priority;
    pNewFlow->u8Cookie = pOfp131FlowMod->u8Cookie;
    pNewFlow->u2Flags = pOfp131FlowMod->u2Flags;
    pNewFlow->MibObject.u4FsofcFlowIdleTimeout = pOfp131FlowMod->u2IdleTimeout;
    pNewFlow->MibObject.u4FsofcFlowHardTimeout = pOfp131FlowMod->u2HardTimeout;
    pNewFlow->MibObject.u4FsofcTableIndex = pOfp131FlowMod->u1TableId;
    pNewFlow->u4TableIndex = pOfp131FlowMod->u1TableId;

    pNewFlow->u1FlowMatchCount = u1FlowMatchCnt;

    u2Length = OSIX_NTOHS (pOfpMatch->u2Length);
    /* pad to 64 bits */
    if ((u2Length % OFC_EIGHT) != OFC_ZERO)
    {
        u2Length = (UINT2) (u2Length + OFC_EIGHT - (u2Length % OFC_EIGHT));
    }

    /* Copy instructions, actions and Set fields into Flow Entry */
    u4InstrLen = (UINT4) (pLocal->u4RxLength -
                          (UINT4) (OFC131_FLOWMOD_OFPMATCH_OFFSET + u2Length));
    if (Ofc131PktParseInstr (pConnPtr, u2Length, pNewFlow, u4InstrLen,
                             pu4Error) != OFC_SUCCESS)
    {
        OFC131_TRC_FUNC ((OFC_PKT_TRC, "Adding Instruction, Actions and "
                          "Set Fields to Flow Entry Failed\n"));

        if (!(pNewFlow->u1IsFlowWild))
        {
            if (pTempExactEntry != NULL)
            {
                TMO_SLL_Delete (&pTempExactEntry->FlowEntryList,
                                (tTMO_SLL_NODE *) pNewFlow);
            }
            else
            {
                /* remove the RBTree node */
                RBTreeRemove (pFlowTable->ExactFlowEntry,
                              (tRBElem *) pExactFlowEntry);
                /* release the mempool */
                MemReleaseMemBlock (OFC_EXACT_ENTRY_POOLID,
                                    (UINT1 *) pExactFlowEntry);
            }
        }
        /* Free FlowMatch SLL */
        Ofc131FlowMatchListFree ((INT1) pOfp131FlowMod->u1TableId,
                                 &pNewFlow->MatchList);
        Ofc131FlowEntryCleanUp (pNewFlow);
        OfcMemFreeForFlowEntry (pOfp131FlowMod->u1TableId, (UINT1 *) pNewFlow);
        return OFC_FAILURE;
    }

    pNewFlow->MibObject.u4FsofcContextId = pOfc->MibObject.u4FsofcContextId;
    /* Add the Flow Entry into the data base */
    if (Ofc131FlowAddFlow (pFlowTable, pNewFlow) != OFC_SUCCESS)
    {
        /* The only reason the addition fails is if the table is full */
        *pu4Error = OFC131_FLOWMOD_TABLE_FULL_ERR;
        if (!(pNewFlow->u1IsFlowWild))
        {
            if (pTempExactEntry != NULL)
            {
                TMO_SLL_Delete (&pTempExactEntry->FlowEntryList,
                                (tTMO_SLL_NODE *) pNewFlow);
            }
            else
            {
                /* remove the RBTree node */
                RBTreeRemove (pFlowTable->ExactFlowEntry,
                              (tRBElem *) pExactFlowEntry);
                /* release the mempool */
                MemReleaseMemBlock (OFC_EXACT_ENTRY_POOLID,
                                    (UINT1 *) pExactFlowEntry);
            }
        }
        /* Free FlowMatch SLL */
        Ofc131FlowMatchListFree ((INT1) pOfp131FlowMod->u1TableId,
                                 &pNewFlow->MatchList);
        Ofc131FlowEntryCleanUp (pNewFlow);
        OfcMemFreeForFlowEntry (pOfp131FlowMod->u1TableId, (UINT1 *) pNewFlow);
        return OFC_FAILURE;
    }
    OFC_TRC_FUNC ((OFC_FN_EXIT, "FUNC:Ofc131FlowHandleAddFlow\n"));
    return OFC_SUCCESS;
}

/******************************************************************************
 * Function Name :  Ofc131FlowModifyFlow
 * Description   :  This routine modifies actions in the existing flow
 * Input         :  pConnPtr  - Connection Pointer.
 *                  pFlow     - Pointer to the Flow Entry
 * Output        :  pu4Error  - Pointer to Error, in case 
 * Return        :  OFC_FAILURE/OFC_SUCCESS
 *****************************************************************************/
PRIVATE UINT4
Ofc131FlowModifyFlow (VOID *pConnPtr, tOfcFsofcFlowEntry * pFlow,
                      UINT4 *pu4Error)
{
    tOfp131Match       *pOfpMatch = NULL;    /* Match as TLVs */
    UINT4               u4InstrLen = OFC_ZERO;
    UINT2               u2Length = OFC_ZERO;
    static CHR1         au1ActionString[OFC_ACTION_STRING_MAX_LEN];
    tOfcFsofcControllerConnEntry *pLocal = pConnPtr;

    OFC_TRC_FUNC ((OFC_FN_ENTRY, "FUNC:Ofc131FlowModifyFlow\n"));

    /*
     * Modify Flow needs to update the instruction field (which may include
     * actions and set fields). In the existing flow entry, the instructions,
     * actions and set fields are cleaned.
     */
    Ofc131FlowEntryCleanUp (pFlow);

    pOfpMatch = (tOfp131Match *) ((VOID *) (pLocal->pRcvPkt +
                                            OFC131_FLOWMOD_OFPMATCH_OFFSET));
    u2Length = OSIX_NTOHS (pOfpMatch->u2Length);
    /* pad to 64 bits */
    if ((u2Length % OFC_EIGHT) != OFC_ZERO)
    {
        u2Length = (UINT2) (u2Length + OFC_EIGHT - (u2Length % OFC_EIGHT));
    }

    /* Copy instructions, actions and Set fields into Flow Entry */
    u4InstrLen = (UINT4) (pLocal->u4RxLength -
                          (UINT4) (OFC131_FLOWMOD_OFPMATCH_OFFSET + u2Length));
    if (Ofc131PktParseInstr (pConnPtr, u2Length, pFlow, u4InstrLen,
                             pu4Error) != OFC_SUCCESS)
    {
        OFC131_TRC_FUNC ((OFC_PKT_TRC, "Modifying Instruction, Actions and "
                          "Set Fields to Flow Entry Failed\n"));
        /* Free FlowMatch SLL */
        Ofc131FlowMatchListFree ((INT1) pFlow->u4TableIndex, &pFlow->MatchList);
        OfcMemFreeForFlowEntry ((UINT1) (pFlow->u4TableIndex), (UINT1 *) pFlow);
        return OFC_FAILURE;

    }

    /* Update the Instruction String as part of Mib Object */
    MEMSET (&au1ActionString, OFC_ZERO, OFC_ACTION_STRING_MAX_LEN);
    Ofc131InstructActionStructToStr (au1ActionString, &pFlow->InstrList);
    MEMCPY (pFlow->MibObject.au1FsofcFlowOutputAction, au1ActionString,
            sizeof (au1ActionString));
    pFlow->MibObject.i4FsofcFlowOutputActionLen = OFC_ACTION_STRING_MAX_LEN;

    /* Update the Match Field String as part of Mib Object */
    MEMSET (&au1ActionString, OFC_ZERO, OFC_ACTION_STRING_MAX_LEN);
    Ofc131MatchFieldsStructToStr (au1ActionString, &pFlow->MatchList);
    MEMCPY (pFlow->MibObject.au1FsofcFlowMatchField, au1ActionString,
            sizeof (au1ActionString));
    pFlow->MibObject.i4FsofcFlowMatchFieldLen = OFC_ACTION_STRING_MAX_LEN;
    OFC_TRC_FUNC ((OFC_FN_EXIT, "FUNC:Ofc131FlowModifyFlow\n"));
    return OFC_SUCCESS;
}

/*****************************************************************************
 * Function Name :  Ofc131FlowHandleModifyFlow 
 * Description   :  This routine handles the flow mod message for
 *                  flow modification.
 * Input         :  pConnPtr       - Connection Pointer.
 *                  pOfp131FlowMod - Pointer to the Flow Mod Message
 * Output        :  pu4Error       - Pointer to Error, in case 
 * Return        :  OFC_FAILURE/OFC_SUCCESS
 *****************************************************************************/
PRIVATE UINT4
Ofc131FlowHandleModifyFlow (tOfcFsofcCfgEntry * pOfc, VOID *pConnPtr,
                            tOfp131FlowMod * pOfp131FlowMod, UINT4 *pu4Error)
{
    tRBExactFlowEntry   ExactFlowEntry;
    tOfcFsofcFlowEntry  FlowEntry;
    tOfcSll             KeyMatchList;
    tRBExactFlowEntry  *pExactFlowEntry = NULL;
    tOfcFsofcFlowTable *pFlowTable = NULL;
    tOfcFsofcFlowEntry *pFlow = NULL;
    tOfp131Match       *pOfpMatch = NULL;    /* Match as TLVs */
    FS_UINT8            u8InpCookie = { OFC_ZERO, OFC_ZERO };
    FS_UINT8            u8InpCookieMsk = { OFC_ZERO, OFC_ZERO };
    UINT4               u4HashIndex = OFC_ZERO;
    UINT1               u1FlowMatchCnt = OFC_ZERO;
    UINT1               u1IsFlowWild = OFC_ZERO;
    UINT1               au1ExactMatch[MAX_OFC_EXACT_LENGTH];
    tOfcFsofcControllerConnEntry *pLocal = pConnPtr;
#ifdef NPAPI_WANTED
#ifdef OPENFLOW_TCAM
    INT4                i4RetValue = OFC_ZERO;
    tOfcHwInfo          OfcHwInfo;
    MEMSET (&OfcHwInfo, OFC_ZERO, sizeof (tOfcHwInfo));
#ifdef DPA_WANTED
    UINT1               u1DpaTabId = OFC_ZERO;
#endif /* DPA_WANTED */
#endif /* OPENFLOW_TCAM */
#endif /* NPAPI_WANTED */

    OFC_TRC_FUNC ((OFC_FN_ENTRY, "FUNC:Ofc131FlowHandleModifyFlow\n"));

    MEMSET (&ExactFlowEntry, OFC_ZERO, sizeof (tRBExactFlowEntry));
    MEMSET (&FlowEntry, OFC_ZERO, sizeof (tOfcFsofcFlowEntry));
    MEMSET (au1ExactMatch, OFC_ZERO, MAX_OFC_EXACT_LENGTH);

    /* mask and store the cookie for matching */
    u8InpCookieMsk.u4Hi = pOfp131FlowMod->u8CookieMask.u4Hi;
    u8InpCookieMsk.u4Lo = pOfp131FlowMod->u8CookieMask.u4Lo;
    u8InpCookie.u4Hi = pOfp131FlowMod->u8Cookie.u4Hi & u8InpCookieMsk.u4Hi;
    u8InpCookie.u4Lo = pOfp131FlowMod->u8Cookie.u4Lo & u8InpCookieMsk.u4Lo;

    pOfpMatch = (tOfp131Match *) ((VOID *) (pLocal->pRcvPkt +
                                            OFC131_FLOWMOD_OFPMATCH_OFFSET));

    /* Copy the TLV flowmatch fields into flow match structure */
    if (Ofc131PktOxmParseMatchTlv (OFC_MINUS_ONE, pOfpMatch, &KeyMatchList,
                                   &u1FlowMatchCnt, &u1IsFlowWild,
                                   pu4Error) != OFC_SUCCESS)
    {
        OFC131_TRC_FUNC ((OFC_FLOWTBL_TRC, "Parsing Flow Match TLV Failed\n"));
        /* Free FlowMatch SLL */
        Ofc131FlowMatchListFree (OFC_MINUS_ONE, &KeyMatchList);
        return OFC_FAILURE;
    }

    pFlowTable = (tOfcFsofcFlowTable *)
        OfcGetFlowTableEntry (pOfc->MibObject.u4FsofcContextId,
                              pOfp131FlowMod->u1TableId);

    if (pFlowTable == NULL)
    {
        /* Free FlowMatch SLL */
        Ofc131FlowMatchListFree (OFC_MINUS_ONE, &KeyMatchList);
        return OFC_FAILURE;
    }

    if (u1FlowMatchCnt != pFlowTable->u1NumMatch)
    {
        u1IsFlowWild = OFC_ONE;
    }

    if (u1IsFlowWild)
    {
        /* search in the hash for wildcard entries */
        TMO_HASH_Scan_Table (pFlowTable->pFlowtable, u4HashIndex)
        {
            if (pOfp131FlowMod->u1Command == OFPFC_MODIFY_STRICT)
            {
                u4HashIndex = pOfp131FlowMod->u2Priority % OFC_MAX_FLOWS;
            }

            TMO_HASH_Scan_Bucket (pFlowTable->pFlowtable, u4HashIndex, pFlow,
                                  tOfcFsofcFlowEntry *)
            {
                /*
                 * If it is strict,  all match fields should be matched and
                 * priority should be considered.
                 */
                if (pOfp131FlowMod->u1Command == OFPFC_MODIFY_STRICT)
                {
                    if ((u1FlowMatchCnt != pFlow->u1FlowMatchCount) ||
                        (pOfp131FlowMod->u2Priority != pFlow->u2Priority))
                    {
                        continue;
                    }
                }

                /* Check if cookie matches */
                if (!(((pFlow->u8Cookie.u4Hi & u8InpCookieMsk.u4Hi)
                       == u8InpCookie.u4Hi) &&
                      ((pFlow->u8Cookie.u4Lo & u8InpCookieMsk.u4Lo)
                       == u8InpCookie.u4Lo)))
                {
                    continue;
                }

                /* Check if flow matches */
                if (Ofc131FlowIsFlowMatched (u1FlowMatchCnt, &KeyMatchList,
                                             pFlow) != OFC_SUCCESS)
                {
                    continue;
                }

                /* Modify Flow */
                Ofc131FlowModifyFlow (pConnPtr, pFlow, pu4Error);

                /* reset the counters, if the flag is set */
                if (pOfp131FlowMod->u2Flags & OFC_RESET_COUNTS)
                {
                    pFlow->MibObject.u4FsofcFlowDurationSec = OFC_ZERO;
                    pFlow->MibObject.u8FsofcFlowPacketCount.msn = OFC_ZERO;
                    pFlow->MibObject.u8FsofcFlowPacketCount.lsn = OFC_ZERO;
                    pFlow->MibObject.u8FsofcFlowByteCount.msn = OFC_ZERO;
                    pFlow->MibObject.u8FsofcFlowByteCount.lsn = OFC_ZERO;
                }

                OFC_TRC_FUNC ((OFC_FLOWTBL_TRC, "Calling NPAPI for Delete Flow"
                               " in HW Tables\n"));

#ifdef NPAPI_WANTED
#ifdef OPENFLOW_TCAM
                OfcHwInfo.OfcVer = OFC_VER_131;
                OfcHwInfo.OfcCmd = OFC_NP_FLOW_MODIFY;
#ifdef DPA_WANTED
                /* Get the DPA table Id from Local Table ID */
                if (OfcGetDpaTableId ((UINT1) pFlow->u4TableIndex,
                                      &u1DpaTabId) != OFC_SUCCESS)
                {
                    return OFC_FAILURE;
                }
                OfcHwInfo.OfcHwEntry.OfcHwFlowInfo.u1TableId = u1DpaTabId;
#else
                OfcHwInfo.OfcHwEntry.OfcHwFlowInfo.u1TableId =
                    (UINT1) pFlow->u4TableIndex;
#endif /* DPA_WANTED */
                OfcHwInfo.OfcHwEntry.OfcHwFlowInfo.u2Priority =
                    pFlow->u2Priority;
                OfcHwInfo.OfcHwEntry.OfcHwFlowInfo.u8Cookie.u4Hi =
                    pFlow->u8Cookie.u4Hi;
                OfcHwInfo.OfcHwEntry.OfcHwFlowInfo.u8Cookie.u4Lo =
                    pFlow->u8Cookie.u4Lo;
                OfcHwInfo.OfcHwEntry.OfcHwFlowInfo.u2IdleTimeout =
                    pFlow->MibObject.u4FsofcFlowIdleTimeout;
                OfcHwInfo.OfcHwEntry.OfcHwFlowInfo.u2HardTimeout =
                    pFlow->MibObject.u4FsofcFlowHardTimeout;
                OfcHwInfo.OfcHwEntry.OfcHwFlowInfo.pMatchList =
                    &pFlow->MatchList;
                OfcHwInfo.OfcHwEntry.OfcHwFlowInfo.pInstrList =
                    &pFlow->InstrList;

                i4RetValue = OfcFsNpHwConfigOfcInfo (&OfcHwInfo);
                if (i4RetValue == FNP_FAILURE)
                {
                    OFC_TRC_FUNC ((OFC_FLOWTBL_TRC,
                                   "Failed to Modify Flow in HW Tables\n"));
                    return OFC_FAILURE;
                }
                return OFC_SUCCESS;
#endif /* OPENFLOW_TCAM */
#endif /* NPAPI_WANTED */
            }

            /* If it is strict, no need to go for other priorities */
            if (pOfp131FlowMod->u1Command == OFPFC_MODIFY_STRICT)
            {
                break;
            }
        }
    }
    else
    {
        /* parse the flow match SLL to fill in the exact match */
        if (Ofc131FlowExactSort (&KeyMatchList,
                                 ExactFlowEntry.au1ExactMatch) != OFC_SUCCESS)
        {
            /* updating exact match string failed */
            OFC131_TRC_FUNC ((OFC_FLOWTBL_TRC, "updating exact match string "
                              "failed in flow modify\n"));
            /* Free FlowMatch SLL */
            Ofc131FlowMatchListFree (OFC_MINUS_ONE, &KeyMatchList);
            return OFC_FAILURE;
        }

        pExactFlowEntry = RBTreeGet (pFlowTable->ExactFlowEntry,
                                     (tRBElem *) & ExactFlowEntry);

        if (pExactFlowEntry == NULL)
        {
            /* no matching flow, throw error */
            /* Free FlowMatch SLL */
            Ofc131FlowMatchListFree (OFC_MINUS_ONE, &KeyMatchList);
            return OFC_FAILURE;
        }

        TMO_SLL_Scan (&pExactFlowEntry->FlowEntryList, pFlow,
                      tOfcFsofcFlowEntry *)
        {
            /* always check for priority */
            if ((pOfp131FlowMod->u1Command == OFPFC_MODIFY_STRICT) &&
                (pFlow->u2Priority != pOfp131FlowMod->u2Priority))
            {
                continue;
            }

            /* Check if cookie matches */
            if (!(((pFlow->u8Cookie.u4Hi & u8InpCookieMsk.u4Hi)
                   == u8InpCookie.u4Hi) &&
                  ((pFlow->u8Cookie.u4Lo & u8InpCookieMsk.u4Lo)
                   == u8InpCookie.u4Lo)))
            {
                continue;
            }

            /* Modify Flow */
            Ofc131FlowModifyFlow (pConnPtr, pFlow, pu4Error);

            /* reset the counters, if the flag is set */
            if (pOfp131FlowMod->u2Flags & OFC_RESET_COUNTS)
            {
                pFlow->MibObject.u4FsofcFlowDurationSec = OFC_ZERO;
                pFlow->MibObject.u8FsofcFlowPacketCount.msn = OFC_ZERO;
                pFlow->MibObject.u8FsofcFlowPacketCount.lsn = OFC_ZERO;
                pFlow->MibObject.u8FsofcFlowByteCount.msn = OFC_ZERO;
                pFlow->MibObject.u8FsofcFlowByteCount.lsn = OFC_ZERO;
            }
        }
    }
    /* Free FlowMatch SLL */
    Ofc131FlowMatchListFree (OFC_MINUS_ONE, &KeyMatchList);
    OFC_TRC_FUNC ((OFC_FN_EXIT, "FUNC:Ofc131FlowHandleModifyFlow\n"));
    return OFC_SUCCESS;
}

/*****************************************************************************
 * Function Name :  Ofc131FlowHandleDeleteFlow 
 * Description   :  This routine handles flow mod message for flow deletion
 * Input         :  pOfc           - Pointer to Context
 *                  pConnPtr       - Connection Pointer
 *                  pOfp131FlowMod - Pointer to the Flow Mod Message
 * Output        :  NONE
 * Return        :  OFC_FAILURE/OFC_SUCCESS
 *****************************************************************************/
PRIVATE UINT4
Ofc131FlowHandleDeleteFlow (tOfcFsofcCfgEntry * pOfc, VOID *pConnPtr,
                            tOfp131FlowMod * pOfp131FlowMod)
{
    tRBExactFlowEntry   ExactFlowEntry;
    tOfcSll             KeyMatchList;
    tRBExactFlowEntry  *pExactFlowEntry = NULL;
    tOfcFsofcFlowTable *pFlowTable = NULL;
    tOfcFsofcFlowEntry *pFlow = NULL;
    tOfcFsofcFlowEntry *pTempFlow = NULL;
    tOfp131Match       *pOfpMatch = NULL;    /* Match as TLVs */
    FS_UINT8            u8InpCookie = { OFC_ZERO, OFC_ZERO };
    FS_UINT8            u8InpCookieMsk = { OFC_ZERO, OFC_ZERO };
    UINT4               u4HashIndex = OFC_ZERO;
    UINT4               u4Error = OFC_ZERO;
    UINT1               u1TableId = OFC_ZERO;
    UINT1               u1FlowMatchCnt = OFC_ZERO;
    UINT1               u1IsFlowWild = OFC_ZERO;
    UINT1               au1ExactMatch[MAX_OFC_EXACT_LENGTH];
    tOfcFsofcControllerConnEntry *pLocal = pConnPtr;

    OFC_TRC_FUNC ((OFC_FN_ENTRY, "FUNC:Ofc131FlowHandleDeleteFlow\n"));

    MEMSET (&ExactFlowEntry, OFC_ZERO, sizeof (tRBExactFlowEntry));
    MEMSET (au1ExactMatch, OFC_ZERO, MAX_OFC_EXACT_LENGTH);

    pOfpMatch = (tOfp131Match *) ((VOID *) (pLocal->pRcvPkt +
                                            OFC131_FLOWMOD_OFPMATCH_OFFSET));

    /* mask and store the cookie for matching */
    u8InpCookieMsk.u4Hi = pOfp131FlowMod->u8CookieMask.u4Hi;
    u8InpCookieMsk.u4Lo = pOfp131FlowMod->u8CookieMask.u4Lo;
    u8InpCookie.u4Hi = pOfp131FlowMod->u8Cookie.u4Hi & u8InpCookieMsk.u4Hi;
    u8InpCookie.u4Lo = pOfp131FlowMod->u8Cookie.u4Lo & u8InpCookieMsk.u4Lo;

    /* Copy the TLV flowmatch fields into flow match structure */
    if (Ofc131PktOxmParseMatchTlv (OFC_MINUS_ONE, pOfpMatch, &KeyMatchList,
                                   &u1FlowMatchCnt, &u1IsFlowWild,
                                   &u4Error) != OFC_SUCCESS)
    {
        OFC131_TRC_FUNC ((OFC_FLOWTBL_TRC, "Parsing Flow Match TLV Failed\n"));
        /* Free FlowMatch SLL */
        Ofc131FlowMatchListFree (OFC_MINUS_ONE, &KeyMatchList);
        return OSIX_FAILURE;
    }

    for (u1TableId = OFC_ZERO; u1TableId < pOfc->u2NumOfTables; u1TableId++)
    {
        if (pOfp131FlowMod->u1TableId != OFPTT_ALL)
        {
            u1TableId = pOfp131FlowMod->u1TableId;
        }

        pFlowTable = (tOfcFsofcFlowTable *)
            OfcGetFlowTableEntry (pOfc->MibObject.u4FsofcContextId, u1TableId);

        if (pFlowTable == NULL)
        {
            if (pOfp131FlowMod->u1TableId != OFPTT_ALL)
            {
                /* Free FlowMatch SLL */
                Ofc131FlowMatchListFree (OFC_MINUS_ONE, &KeyMatchList);
                return OFC_FAILURE;
            }
            else
            {
                /* continue checking in other tables */
                continue;
            }
        }

        if ((pOfp131FlowMod->u1TableId != OFPTT_ALL) &&
            (pFlowTable->u1NumMatch == u1FlowMatchCnt) &&
            (u1IsFlowWild == OFC_ZERO))
        {
            /* exact match in this table */
            /* parse the flow match SLL to fill in the exact match */
            if (Ofc131FlowExactSort (&KeyMatchList,
                                     ExactFlowEntry.au1ExactMatch) !=
                OFC_SUCCESS)
            {
                /* updating exact match string failed */
                OFC131_TRC_FUNC ((OFC_FLOWTBL_TRC, "updating exact match "
                                  " string failed in flow delete\n"));
                /* Free FlowMatch SLL */
                Ofc131FlowMatchListFree (OFC_MINUS_ONE, &KeyMatchList);
                return OFC_FAILURE;
            }

            pExactFlowEntry = RBTreeGet (pFlowTable->ExactFlowEntry,
                                         (tRBElem *) & ExactFlowEntry);

            if (pExactFlowEntry == NULL)
            {
                /* no matching flow, throw error */
                /* Free FlowMatch SLL */
                Ofc131FlowMatchListFree (OFC_MINUS_ONE, &KeyMatchList);
                return OFC_FAILURE;
            }

            TMO_DYN_SLL_Scan (&pExactFlowEntry->FlowEntryList, pFlow,
                              pTempFlow, tOfcFsofcFlowEntry *)
            {
                /* always check for priority */
                if ((pOfp131FlowMod->u1Command == OFPFC_DELETE_STRICT) &&
                    (pOfp131FlowMod->u2Priority != pFlow->u2Priority))
                {
                    continue;
                }

                /* Check if output port matches */
                if ((pOfp131FlowMod->u4OutPort != OFPP_ANY) &&
                    (pOfp131FlowMod->u4OutPort != pFlow->u4OutPort))
                {
                    continue;
                }

                /* Check if output group matches */
                if ((pOfp131FlowMod->u4OutGroup != OFPG_ANY) &&
                    (pOfp131FlowMod->u4OutGroup != pFlow->u4OutGrp))
                {
                    continue;
                }

                /* Check if cookie matches */
                if (!((pFlow->u8Cookie.u4Hi & u8InpCookieMsk.u4Hi)
                      == u8InpCookie.u4Hi) &&
                    ((pFlow->u8Cookie.u4Lo & u8InpCookieMsk.u4Lo)
                     == u8InpCookie.u4Lo))
                {
                    continue;
                }

                /* now found the match, first delete it from the list */
                TMO_SLL_Delete (&pExactFlowEntry->FlowEntryList,
                                (tTMO_SLL_NODE *) pFlow);
                Ofc131FlowDeleteFlow (pOfc, pFlow, OFPRR_DELETE);
            }
            /*
             * if all the flows as part of the list are deleted,
             * it is unneccessary to maintain the RBTree node.
             * So remove and free it too.
             */
            if (TMO_SLL_Count (&pExactFlowEntry->FlowEntryList) == OFC_ZERO)
            {
                /* remove the RBTree node */
                RBTreeRemove (pFlowTable->ExactFlowEntry,
                              (tRBElem *) pExactFlowEntry);
                /* release the mempool */
                MemReleaseMemBlock (OFC_EXACT_ENTRY_POOLID,
                                    (UINT1 *) pExactFlowEntry);
            }
        }
        else
        {
            TMO_HASH_Scan_Table (pFlowTable->pFlowtable, u4HashIndex)
            {
                /*
                 *  for "strict" version of flow delete, flow entries matching
                 * along with the priority should be deleted.
                 */
                if (pOfp131FlowMod->u1Command == OFPFC_DELETE_STRICT)
                {
                    u4HashIndex = pOfp131FlowMod->u2Priority % OFC_MAX_FLOWS;
                }

                TMO_HASH_DYN_Scan_Bucket (pFlowTable->pFlowtable, u4HashIndex,
                                          pFlow, pTempFlow,
                                          tOfcFsofcFlowEntry *)
                {
                    /* If it is strict,  all match fields should be matched */
                    if ((pOfp131FlowMod->u1Command == OFPFC_DELETE_STRICT) &&
                        (u1FlowMatchCnt != pFlow->u1FlowMatchCount))
                    {
                        continue;
                    }

                    /* Check if priority matches */
                    if (pOfp131FlowMod->u2Priority != pFlow->u2Priority)
                    {
                        continue;
                    }

                    /* Check if output port matches */
                    if ((pOfp131FlowMod->u4OutPort != OFPP_ANY) &&
                        (pOfp131FlowMod->u4OutPort != pFlow->u4OutPort))
                    {
                        continue;
                    }

                    /* Check if output group matches */
                    if ((pOfp131FlowMod->u4OutGroup != OFPG_ANY) &&
                        (pOfp131FlowMod->u4OutGroup != pFlow->u4OutGrp))
                    {
                        continue;
                    }

                    /* Check if cookie matches */
                    if (!((pFlow->u8Cookie.u4Hi & u8InpCookieMsk.u4Hi)
                          == u8InpCookie.u4Hi) &&
                        ((pFlow->u8Cookie.u4Lo & u8InpCookieMsk.u4Lo)
                         == u8InpCookie.u4Lo))
                    {
                        continue;
                    }

                    /* Check if flow matches */
                    if (Ofc131FlowIsFlowMatched (u1FlowMatchCnt, &KeyMatchList,
                                                 pFlow) != OFC_SUCCESS)
                    {
                        continue;
                    }

                    /*
                     * The return value is not checked intentionally here, as
                     * there is no need to generate an error message when the
                     * flow delete fails. We hope the timer expiry will take
                     * care of deletion later on.
                     */
                    Ofc131FlowDeleteFlow (pOfc, pFlow, OFPRR_DELETE);
                }
                /* If it is strict, no need to go for other priorities */
                if (pOfp131FlowMod->u1Command == OFPFC_DELETE_STRICT)
                {
                    break;
                }
            }
            /* If it is for specific table, no need to go for other tables */
            if (pOfp131FlowMod->u1TableId != OFPTT_ALL)
            {
                break;
            }
        }
    }
    /* Free FlowMatch SLL */
    Ofc131FlowMatchListFree (OFC_MINUS_ONE, &KeyMatchList);
    OFC_TRC_FUNC ((OFC_FN_EXIT, "FUNC:Ofc131FlowHandleDeleteFlow\n"));
    return OFC_SUCCESS;
}

/******************************************************************************
 * Function Name :  Ofc131FlowHandleFlowModMsg 
 * Description   :  This routine handles the flow mod message, wrapper.
 * Input         :  pOfc           - Pointer to Ofc Context
 *                  pConnPtr       - Connection Pointer
 *                  pOfp131FlowMod - Pointer to the Flow Mod Message
 * Output        :  pu4Error       - Pointer to Error, in case 
 * Return        :  OFC_FAILURE/OFC_SUCCESS
 *****************************************************************************/
UINT4
Ofc131FlowHandleFlowModMsg (tOfcFsofcCfgEntry * pOfc, VOID *pConnPtr,
                            tOfp131FlowMod * pOfp131FlowMod, UINT4 *pu4Error)
{
    OFC_TRC_FUNC ((OFC_FN_ENTRY, "FUNC:Ofc131FlowHandleFlowModMsg\n"));

    /* Validate the table index */
    if ((pOfp131FlowMod->u1Command == OFPFC_DELETE) ||
        (pOfp131FlowMod->u1Command == OFPFC_DELETE_STRICT))
    {
        /* OFPTT_ALL is valid for delete */
        if ((pOfp131FlowMod->u1TableId != OFPTT_ALL) &&
            (pOfp131FlowMod->u1TableId >= pOfc->u2NumOfTables))
        {
            *pu4Error = OFC131_FLOWMOD_INVALID_TABLE_ERR;
            return OFC_FAILURE;
        }
    }
    else
    {
        if (pOfp131FlowMod->u1TableId >= pOfc->u2NumOfTables)
        {
            *pu4Error = OFC131_FLOWMOD_INVALID_TABLE_ERR;
            return OFC_FAILURE;
        }
    }

    /* validate the flags */
    if (pOfp131FlowMod->u2Flags & OFC131_INVALID_FLOWMOD_FLAGS)
    {
        *pu4Error = OFC131_FLOWMOD_INVALID_FLAGS_ERR;
        return OFC_FAILURE;
    }

    switch (pOfp131FlowMod->u1Command)
    {
        case OFPFC_ADD:
            if (Ofc131FlowHandleAddFlow (pOfc, pConnPtr, pOfp131FlowMod,
                                         pu4Error) != OFC_SUCCESS)
            {
                /* Flow Addtion Failed */
                OFC_TRC_FUNC ((OFC_FLOWTBL_TRC,
                               "Flow Addtion Failed, Error - %d \n",
                               *pu4Error));
                return OFC_FAILURE;
            }
            break;

        case OFPFC_MODIFY:
        case OFPFC_MODIFY_STRICT:
            Ofc131FlowHandleModifyFlow (pOfc, pConnPtr, pOfp131FlowMod,
                                        pu4Error);
            break;

        case OFPFC_DELETE:
        case OFPFC_DELETE_STRICT:
            Ofc131FlowHandleDeleteFlow (pOfc, pConnPtr, pOfp131FlowMod);
            break;

        default:
            /* unknown command, return failure */
            *pu4Error = OFC131_FLOWMOD_UNKNOWN_COMMAND_ERR;
            return OFC_FAILURE;
    }
    OFC_TRC_FUNC ((OFC_FN_EXIT, "FUNC:Ofc131FlowHandleFlowModMsg\n"));
    return OFC_SUCCESS;
}
