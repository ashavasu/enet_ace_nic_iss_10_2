/********************************************************************
* Copyright (C) 2013 Aricent Inc . All Rights Reserved
*
* $Id: ofcdbg.c,v 1.5 2014/03/18 12:09:41 siva Exp $
*
* Description: This file contains the routines for the protocol Database Access for the module Ofc 
*********************************************************************/

#include "ofcinc.h"
#include "ofcsz.h"
#include "msr.h"

/****************************************************************************
 Function    :  OfcGetAllFsofcCfgTable
 Input       :  pOfcGetFsofcCfgEntry
 Description :  This Routine Take the Indices &
                Gets the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
OfcGetAllFsofcCfgTable (tOfcFsofcCfgEntry * pOfcGetFsofcCfgEntry)
{
    tOfcFsofcCfgEntry  *pOfcFsofcCfgEntry = NULL;

    /* Check whether the node is already present */
    pOfcFsofcCfgEntry =
        RBTreeGet (gOfcGlobals.OfcGlbMib.FsofcCfgTable,
                   (tRBElem *) pOfcGetFsofcCfgEntry);

    if (pOfcFsofcCfgEntry == NULL)
    {
        OFC_TRC ((OFC_UTIL_TRC,
                  "OfcGetAllFsofcCfgTable: Entry doesn't exist\r\n"));
        return OSIX_FAILURE;
    }

    /* Assign values from the existing node */
    pOfcGetFsofcCfgEntry->MibObject.i4FsofcModuleStatus =
        pOfcFsofcCfgEntry->MibObject.i4FsofcModuleStatus;

    pOfcGetFsofcCfgEntry->MibObject.i4FsofcSupportedVersion =
        pOfcFsofcCfgEntry->MibObject.i4FsofcSupportedVersion;

    pOfcGetFsofcCfgEntry->MibObject.i4FsofcDefaultFlowMissBehaviour =
        pOfcFsofcCfgEntry->MibObject.i4FsofcDefaultFlowMissBehaviour;

    pOfcGetFsofcCfgEntry->MibObject.i4FsofcControlPktBuffering =
        pOfcFsofcCfgEntry->MibObject.i4FsofcControlPktBuffering;

    pOfcGetFsofcCfgEntry->MibObject.i4FsofcIpReassembleStatus =
        pOfcFsofcCfgEntry->MibObject.i4FsofcIpReassembleStatus;

    pOfcGetFsofcCfgEntry->MibObject.i4FsofcPortStpStatus =
        pOfcFsofcCfgEntry->MibObject.i4FsofcPortStpStatus;

    pOfcGetFsofcCfgEntry->MibObject.u4FsofcTraceEnable =
        pOfcFsofcCfgEntry->MibObject.u4FsofcTraceEnable;

    pOfcGetFsofcCfgEntry->MibObject.i4FsofcSwitchModeOnConnFailure =
        pOfcFsofcCfgEntry->MibObject.i4FsofcSwitchModeOnConnFailure;

    pOfcGetFsofcCfgEntry->MibObject.i4FsofcSwitchEntryStatus =
        pOfcFsofcCfgEntry->MibObject.i4FsofcSwitchEntryStatus;

    pOfcGetFsofcCfgEntry->MibObject.i4FsofcHybrid =
        pOfcFsofcCfgEntry->MibObject.i4FsofcHybrid;

    if (OfcGetAllUtlFsofcCfgTable (pOfcGetFsofcCfgEntry, pOfcFsofcCfgEntry) ==
        OSIX_FAILURE)

    {
        OFC_TRC ((OFC_UTIL_TRC, "OfcGetAllFsofcCfgTable:"
                  "OfcGetAllUtlFsofcCfgTable Returns Failure\r\n"));

        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  OfcGetAllFsofcControllerConnTable
 Input       :  pOfcGetFsofcControllerConnEntry
 Description :  This Routine Take the Indices &
                Gets the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
OfcGetAllFsofcControllerConnTable (tOfcFsofcControllerConnEntry *
                                   pOfcGetFsofcControllerConnEntry)
{
    tOfcFsofcControllerConnEntry *pOfcFsofcControllerConnEntry = NULL;

    /* Check whether the node is already present */
    pOfcFsofcControllerConnEntry =
        RBTreeGet (gOfcGlobals.OfcGlbMib.FsofcControllerConnTable,
                   (tRBElem *) pOfcGetFsofcControllerConnEntry);

    if (pOfcFsofcControllerConnEntry == NULL)
    {
        OFC_TRC ((OFC_UTIL_TRC,
                  "OfcGetAllFsofcControllerConnTable: Entry doesn't exist\r\n"));
        return OSIX_FAILURE;
    }

    /* Assign values from the existing node */
    pOfcGetFsofcControllerConnEntry->MibObject.i4FsofcControllerConnPort =
        pOfcFsofcControllerConnEntry->MibObject.i4FsofcControllerConnPort;

    pOfcGetFsofcControllerConnEntry->MibObject.i4FsofcControllerConnProtocol =
        pOfcFsofcControllerConnEntry->MibObject.i4FsofcControllerConnProtocol;

    pOfcGetFsofcControllerConnEntry->MibObject.i4FsofcControllerRole =
        pOfcFsofcControllerConnEntry->MibObject.i4FsofcControllerRole;

    pOfcGetFsofcControllerConnEntry->MibObject.i4FsofcControllerConnState =
        pOfcFsofcControllerConnEntry->MibObject.i4FsofcControllerConnState;

    pOfcGetFsofcControllerConnEntry->
        MibObject.i4FsofcControllerConnEchoReqCount =
        pOfcFsofcControllerConnEntry->
        MibObject.i4FsofcControllerConnEchoReqCount;

    pOfcGetFsofcControllerConnEntry->
        MibObject.i4FsofcControllerConnEchoReplyCount =
        pOfcFsofcControllerConnEntry->
        MibObject.i4FsofcControllerConnEchoReplyCount;

    pOfcGetFsofcControllerConnEntry->
        MibObject.i4FsofcControllerConnEntryStatus =
        pOfcFsofcControllerConnEntry->
        MibObject.i4FsofcControllerConnEntryStatus;

    pOfcGetFsofcControllerConnEntry->MibObject.i4FsofcControllerConnBand =
        pOfcFsofcControllerConnEntry->MibObject.i4FsofcControllerConnBand;

    if (OfcGetAllUtlFsofcControllerConnTable
        (pOfcGetFsofcControllerConnEntry,
         pOfcFsofcControllerConnEntry) == OSIX_FAILURE)

    {
        OFC_TRC ((OFC_UTIL_TRC, "OfcGetAllFsofcControllerConnTable:"
                  "OfcGetAllUtlFsofcControllerConnTable Returns Failure\r\n"));

        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  OfcGetAllFsofcIfTable
 Input       :  pOfcGetFsofcIfEntry
 Description :  This Routine Take the Indices &
                Gets the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
OfcGetAllFsofcIfTable (tOfcFsofcIfEntry * pOfcGetFsofcIfEntry)
{
    tOfcFsofcIfEntry   *pOfcFsofcIfEntry = NULL;

    /* Check whether the node is already present */
    pOfcFsofcIfEntry =
        RBTreeGet (gOfcGlobals.OfcGlbMib.FsofcIfTable,
                   (tRBElem *) pOfcGetFsofcIfEntry);

    if (pOfcFsofcIfEntry == NULL)
    {
        OFC_TRC ((OFC_UTIL_TRC,
                  "OfcGetAllFsofcIfTable: Entry doesn't exist\r\n"));
        return OSIX_FAILURE;
    }

    /* Assign values from the existing node */
    pOfcGetFsofcIfEntry->MibObject.i4FsofcIfType =
        pOfcFsofcIfEntry->MibObject.i4FsofcIfType;

    MEMCPY (pOfcGetFsofcIfEntry->MibObject.au1FsofcIfAlias,
            pOfcFsofcIfEntry->MibObject.au1FsofcIfAlias,
            pOfcFsofcIfEntry->MibObject.i4FsofcIfAliasLen);

    pOfcGetFsofcIfEntry->MibObject.i4FsofcIfAliasLen =
        pOfcFsofcIfEntry->MibObject.i4FsofcIfAliasLen;

    pOfcGetFsofcIfEntry->MibObject.i4FsofcIfOperStatus =
        pOfcFsofcIfEntry->MibObject.i4FsofcIfOperStatus;

    MEMCPY (pOfcGetFsofcIfEntry->MibObject.au1FsofcVlanEgressPorts,
            pOfcFsofcIfEntry->MibObject.au1FsofcVlanEgressPorts,
            pOfcFsofcIfEntry->MibObject.i4FsofcVlanEgressPortsLen);

    pOfcGetFsofcIfEntry->MibObject.i4FsofcVlanEgressPortsLen =
        pOfcFsofcIfEntry->MibObject.i4FsofcVlanEgressPortsLen;

    MEMCPY (pOfcGetFsofcIfEntry->MibObject.au1FsofcVlanUntaggedPorts,
            pOfcFsofcIfEntry->MibObject.au1FsofcVlanUntaggedPorts,
            pOfcFsofcIfEntry->MibObject.i4FsofcVlanUntaggedPortsLen);

    pOfcGetFsofcIfEntry->MibObject.i4FsofcVlanUntaggedPortsLen =
        pOfcFsofcIfEntry->MibObject.i4FsofcVlanUntaggedPortsLen;

    pOfcGetFsofcIfEntry->MibObject.u4FsofcVlanInFrames =
        pOfcFsofcIfEntry->MibObject.u4FsofcVlanInFrames;

    pOfcGetFsofcIfEntry->MibObject.u4FsofcVlanOutFrames =
        pOfcFsofcIfEntry->MibObject.u4FsofcVlanOutFrames;

    pOfcGetFsofcIfEntry->MibObject.u4FsofcIfContextId =
        pOfcFsofcIfEntry->MibObject.u4FsofcIfContextId;

    if (OfcGetAllUtlFsofcIfTable (pOfcGetFsofcIfEntry, pOfcFsofcIfEntry) ==
        OSIX_FAILURE)

    {
        OFC_TRC ((OFC_UTIL_TRC, "OfcGetAllFsofcIfTable:"
                  "OfcGetAllUtlFsofcIfTable Returns Failure\r\n"));

        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  OfcGetAllFsofcFlowTable
 Input       :  pOfcGetFsofcFlowEntry
 Description :  This Routine Take the Indices &
                Gets the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
OfcGetAllFsofcFlowTable (tOfcFsofcFlowEntry * pOfcGetFsofcFlowEntry)
{
    tOfcFsofcFlowEntry *pOfcFsofcFlowEntry = NULL;

    /* Check whether the node is already present */
    pOfcFsofcFlowEntry =
        RBTreeGet (gOfcGlobals.OfcGlbMib.FsofcFlowTable,
                   (tRBElem *) pOfcGetFsofcFlowEntry);

    if (pOfcFsofcFlowEntry == NULL)
    {
        OFC_TRC ((OFC_UTIL_TRC,
                  "OfcGetAllFsofcFlowTable: Entry doesn't exist\r\n"));
        return OSIX_FAILURE;
    }

    /* Assign values from the existing node */
    MEMCPY (pOfcGetFsofcFlowEntry->MibObject.au1FsofcFlowMatchField,
            pOfcFsofcFlowEntry->MibObject.au1FsofcFlowMatchField,
            pOfcFsofcFlowEntry->MibObject.i4FsofcFlowMatchFieldLen);

    pOfcGetFsofcFlowEntry->MibObject.i4FsofcFlowMatchFieldLen =
        pOfcFsofcFlowEntry->MibObject.i4FsofcFlowMatchFieldLen;

    MEMCPY (pOfcGetFsofcFlowEntry->MibObject.au1FsofcFlowOutputAction,
            pOfcFsofcFlowEntry->MibObject.au1FsofcFlowOutputAction,
            pOfcFsofcFlowEntry->MibObject.i4FsofcFlowOutputActionLen);

    pOfcGetFsofcFlowEntry->MibObject.i4FsofcFlowOutputActionLen =
        pOfcFsofcFlowEntry->MibObject.i4FsofcFlowOutputActionLen;

    pOfcGetFsofcFlowEntry->MibObject.u4FsofcFlowIdleTimeout =
        pOfcFsofcFlowEntry->MibObject.u4FsofcFlowIdleTimeout;

    pOfcGetFsofcFlowEntry->MibObject.u4FsofcFlowHardTimeout =
        pOfcFsofcFlowEntry->MibObject.u4FsofcFlowHardTimeout;

    pOfcGetFsofcFlowEntry->MibObject.u8FsofcFlowPacketCount =
        pOfcFsofcFlowEntry->MibObject.u8FsofcFlowPacketCount;

    pOfcGetFsofcFlowEntry->MibObject.u8FsofcFlowByteCount =
        pOfcFsofcFlowEntry->MibObject.u8FsofcFlowByteCount;

    pOfcGetFsofcFlowEntry->MibObject.u4FsofcFlowDurationSec =
        pOfcFsofcFlowEntry->MibObject.u4FsofcFlowDurationSec;

    if (OfcGetAllUtlFsofcFlowTable (pOfcGetFsofcFlowEntry, pOfcFsofcFlowEntry)
        == OSIX_FAILURE)

    {
        OFC_TRC ((OFC_UTIL_TRC, "OfcGetAllFsofcFlowTable:"
                  "OfcGetAllUtlFsofcFlowTable Returns Failure\r\n"));

        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  OfcGetAllFsofcGroupTable
 Input       :  pOfcGetFsofcGroupEntry
 Description :  This Routine Take the Indices &
                Gets the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
OfcGetAllFsofcGroupTable (tOfcFsofcGroupEntry * pOfcGetFsofcGroupEntry)
{
    tOfcFsofcGroupEntry *pOfcFsofcGroupEntry = NULL;

    /* Check whether the node is already present */
    pOfcFsofcGroupEntry =
        RBTreeGet (gOfcGlobals.OfcGlbMib.FsofcGroupTable,
                   (tRBElem *) pOfcGetFsofcGroupEntry);

    if (pOfcFsofcGroupEntry == NULL)
    {
        OFC_TRC ((OFC_UTIL_TRC,
                  "OfcGetAllFsofcGroupTable: Entry doesn't exist\r\n"));
        return OSIX_FAILURE;
    }

    /* Assign values from the existing node */
    pOfcGetFsofcGroupEntry->MibObject.i4FsofcGroupType =
        pOfcFsofcGroupEntry->MibObject.i4FsofcGroupType;

    MEMCPY (pOfcGetFsofcGroupEntry->MibObject.au1FsofcGroupActionBuckets,
            pOfcFsofcGroupEntry->MibObject.au1FsofcGroupActionBuckets,
            pOfcFsofcGroupEntry->MibObject.i4FsofcGroupActionBucketsLen);

    pOfcGetFsofcGroupEntry->MibObject.i4FsofcGroupActionBucketsLen =
        pOfcFsofcGroupEntry->MibObject.i4FsofcGroupActionBucketsLen;

    pOfcGetFsofcGroupEntry->MibObject.u8FsofcGroupPacketCount =
        pOfcFsofcGroupEntry->MibObject.u8FsofcGroupPacketCount;

    pOfcGetFsofcGroupEntry->MibObject.u8FsofcGroupByteCount =
        pOfcFsofcGroupEntry->MibObject.u8FsofcGroupByteCount;

    pOfcGetFsofcGroupEntry->MibObject.u4FsofcGroupDurationSec =
        pOfcFsofcGroupEntry->MibObject.u4FsofcGroupDurationSec;

    if (OfcGetAllUtlFsofcGroupTable
        (pOfcGetFsofcGroupEntry, pOfcFsofcGroupEntry) == OSIX_FAILURE)

    {
        OFC_TRC ((OFC_UTIL_TRC, "OfcGetAllFsofcGroupTable:"
                  "OfcGetAllUtlFsofcGroupTable Returns Failure\r\n"));

        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  OfcGetAllFsofcMeterTable
 Input       :  pOfcGetFsofcMeterEntry
 Description :  This Routine Take the Indices &
                Gets the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
OfcGetAllFsofcMeterTable (tOfcFsofcMeterEntry * pOfcGetFsofcMeterEntry)
{
    tOfcFsofcMeterEntry *pOfcFsofcMeterEntry = NULL;

    /* Check whether the node is already present */
    pOfcFsofcMeterEntry =
        RBTreeGet (gOfcGlobals.OfcGlbMib.FsofcMeterTable,
                   (tRBElem *) pOfcGetFsofcMeterEntry);

    if (pOfcFsofcMeterEntry == NULL)
    {
        OFC_TRC ((OFC_UTIL_TRC,
                  "OfcGetAllFsofcMeterTable: Entry doesn't exist\r\n"));
        return OSIX_FAILURE;
    }

    /* Assign values from the existing node */
    MEMCPY (pOfcGetFsofcMeterEntry->MibObject.au1FsofcMeterBandInfo,
            pOfcFsofcMeterEntry->MibObject.au1FsofcMeterBandInfo,
            pOfcFsofcMeterEntry->MibObject.i4FsofcMeterBandInfoLen);

    pOfcGetFsofcMeterEntry->MibObject.i4FsofcMeterBandInfoLen =
        pOfcFsofcMeterEntry->MibObject.i4FsofcMeterBandInfoLen;

    pOfcGetFsofcMeterEntry->MibObject.u4FsofcMeterFlowCount =
        pOfcFsofcMeterEntry->MibObject.u4FsofcMeterFlowCount;

    pOfcGetFsofcMeterEntry->MibObject.u8FsofcMeterPacketInCount =
        pOfcFsofcMeterEntry->MibObject.u8FsofcMeterPacketInCount;

    pOfcGetFsofcMeterEntry->MibObject.u8FsofcMeterByteInCount =
        pOfcFsofcMeterEntry->MibObject.u8FsofcMeterByteInCount;

    pOfcGetFsofcMeterEntry->MibObject.u4FsofcMeterDurationSec =
        pOfcFsofcMeterEntry->MibObject.u4FsofcMeterDurationSec;

    if (OfcGetAllUtlFsofcMeterTable
        (pOfcGetFsofcMeterEntry, pOfcFsofcMeterEntry) == OSIX_FAILURE)

    {
        OFC_TRC ((OFC_UTIL_TRC, "OfcGetAllFsofcMeterTable:"
                  "OfcGetAllUtlFsofcMeterTable Returns Failure\r\n"));

        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  OfcSetAllFsofcCfgTable
 Input       :  pOfcSetFsofcCfgEntry
                pOfcIsSetFsofcCfgEntry
                i4RowStatusLogic
                i4RowCreateOption
 Description :  This Routine Take the Indices &
                Sets the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
OfcSetAllFsofcCfgTable (tOfcFsofcCfgEntry * pOfcSetFsofcCfgEntry,
                        tOfcIsSetFsofcCfgEntry * pOfcIsSetFsofcCfgEntry,
                        INT4 i4RowStatusLogic, INT4 i4RowCreateOption)
{
    tOfcFsofcCfgEntry  *pOfcFsofcCfgEntry = NULL;
    tOfcFsofcCfgEntry  *pOfcOldFsofcCfgEntry = NULL;
    tOfcFsofcCfgEntry  *pOfcTrgFsofcCfgEntry = NULL;
    tOfcIsSetFsofcCfgEntry *pOfcTrgIsSetFsofcCfgEntry = NULL;
    INT4                i4RowMakeActive = FALSE;

    pOfcOldFsofcCfgEntry =
        (tOfcFsofcCfgEntry *) MemAllocMemBlk (OFC_FSOFCCFGTABLE_POOLID);
    if (pOfcOldFsofcCfgEntry == NULL)
    {
        return OSIX_FAILURE;
    }
    pOfcTrgFsofcCfgEntry =
        (tOfcFsofcCfgEntry *) MemAllocMemBlk (OFC_FSOFCCFGTABLE_POOLID);
    if (pOfcTrgFsofcCfgEntry == NULL)
    {
        MemReleaseMemBlock (OFC_FSOFCCFGTABLE_POOLID,
                            (UINT1 *) pOfcOldFsofcCfgEntry);
        return OSIX_FAILURE;
    }
    pOfcTrgIsSetFsofcCfgEntry =
        (tOfcIsSetFsofcCfgEntry *)
        MemAllocMemBlk (OFC_FSOFCCFGTABLE_ISSET_POOLID);
    if (pOfcTrgIsSetFsofcCfgEntry == NULL)
    {
        MemReleaseMemBlock (OFC_FSOFCCFGTABLE_POOLID,
                            (UINT1 *) pOfcOldFsofcCfgEntry);
        MemReleaseMemBlock (OFC_FSOFCCFGTABLE_POOLID,
                            (UINT1 *) pOfcTrgFsofcCfgEntry);
        return OSIX_FAILURE;
    }
    MEMSET (pOfcOldFsofcCfgEntry, 0, sizeof (tOfcFsofcCfgEntry));
    MEMSET (pOfcTrgFsofcCfgEntry, 0, sizeof (tOfcFsofcCfgEntry));
    MEMSET (pOfcTrgIsSetFsofcCfgEntry, 0, sizeof (tOfcIsSetFsofcCfgEntry));

    /* Check whether the node is already present */
    pOfcFsofcCfgEntry =
        RBTreeGet (gOfcGlobals.OfcGlbMib.FsofcCfgTable,
                   (tRBElem *) pOfcSetFsofcCfgEntry);

    if (pOfcFsofcCfgEntry == NULL)
    {
        /* Create the node if the RowStatus given is CREATE_AND_WAIT or CREATE_AND_GO */
        if ((pOfcSetFsofcCfgEntry->MibObject.i4FsofcSwitchEntryStatus ==
             CREATE_AND_WAIT)
            || (pOfcSetFsofcCfgEntry->MibObject.i4FsofcSwitchEntryStatus ==
                CREATE_AND_GO)
            ||
            ((pOfcSetFsofcCfgEntry->MibObject.i4FsofcSwitchEntryStatus ==
              ACTIVE) && (i4RowCreateOption == 1)))
        {
            /* Allocate memory for the new node */
            pOfcFsofcCfgEntry =
                (tOfcFsofcCfgEntry *) MemAllocMemBlk (OFC_FSOFCCFGTABLE_POOLID);
            if (pOfcFsofcCfgEntry == NULL)
            {
                if (OfcSetAllFsofcCfgTableTrigger (pOfcSetFsofcCfgEntry,
                                                   pOfcIsSetFsofcCfgEntry,
                                                   SNMP_FAILURE) !=
                    OSIX_SUCCESS)

                {
                    OFC_TRC ((OFC_UTIL_TRC,
                              "OfcSetAllFsofcCfgTable:OfcSetAllFsofcCfgTableTrigger function fails\r\n"));

                }
                OFC_TRC ((OFC_UTIL_TRC,
                          "OfcSetAllFsofcCfgTable: Fail to Allocate Memory.\r\n"));
                MemReleaseMemBlock (OFC_FSOFCCFGTABLE_POOLID,
                                    (UINT1 *) pOfcOldFsofcCfgEntry);
                MemReleaseMemBlock (OFC_FSOFCCFGTABLE_POOLID,
                                    (UINT1 *) pOfcTrgFsofcCfgEntry);
                MemReleaseMemBlock (OFC_FSOFCCFGTABLE_ISSET_POOLID,
                                    (UINT1 *) pOfcTrgIsSetFsofcCfgEntry);
                return OSIX_FAILURE;
            }

            MEMSET (pOfcFsofcCfgEntry, 0, sizeof (tOfcFsofcCfgEntry));
            if ((OfcInitializeFsofcCfgTable (pOfcFsofcCfgEntry)) ==
                OSIX_FAILURE)
            {
                if (OfcSetAllFsofcCfgTableTrigger (pOfcSetFsofcCfgEntry,
                                                   pOfcIsSetFsofcCfgEntry,
                                                   SNMP_FAILURE) !=
                    OSIX_SUCCESS)

                {
                    OFC_TRC ((OFC_UTIL_TRC,
                              "OfcSetAllFsofcCfgTable:OfcSetAllFsofcCfgTableTrigger function fails\r\n"));

                }
                OFC_TRC ((OFC_UTIL_TRC,
                          "OfcSetAllFsofcCfgTable: Fail to Initialize the Objects.\r\n"));

                MemReleaseMemBlock (OFC_FSOFCCFGTABLE_POOLID,
                                    (UINT1 *) pOfcFsofcCfgEntry);
                MemReleaseMemBlock (OFC_FSOFCCFGTABLE_POOLID,
                                    (UINT1 *) pOfcOldFsofcCfgEntry);
                MemReleaseMemBlock (OFC_FSOFCCFGTABLE_POOLID,
                                    (UINT1 *) pOfcTrgFsofcCfgEntry);
                MemReleaseMemBlock (OFC_FSOFCCFGTABLE_ISSET_POOLID,
                                    (UINT1 *) pOfcTrgIsSetFsofcCfgEntry);
                return OSIX_FAILURE;
            }
            /* Assign values for the new node */
            if (pOfcIsSetFsofcCfgEntry->bFsofcContextId != OSIX_FALSE)
            {
                pOfcFsofcCfgEntry->MibObject.u4FsofcContextId =
                    pOfcSetFsofcCfgEntry->MibObject.u4FsofcContextId;
            }

            if (pOfcIsSetFsofcCfgEntry->bFsofcModuleStatus != OSIX_FALSE)
            {
                pOfcFsofcCfgEntry->MibObject.i4FsofcModuleStatus =
                    pOfcSetFsofcCfgEntry->MibObject.i4FsofcModuleStatus;
            }

            if (pOfcIsSetFsofcCfgEntry->bFsofcSupportedVersion != OSIX_FALSE)
            {
                pOfcFsofcCfgEntry->MibObject.i4FsofcSupportedVersion =
                    pOfcSetFsofcCfgEntry->MibObject.i4FsofcSupportedVersion;
            }

            if (pOfcIsSetFsofcCfgEntry->bFsofcDefaultFlowMissBehaviour !=
                OSIX_FALSE)
            {
                pOfcFsofcCfgEntry->MibObject.i4FsofcDefaultFlowMissBehaviour =
                    pOfcSetFsofcCfgEntry->
                    MibObject.i4FsofcDefaultFlowMissBehaviour;
            }

            if (pOfcIsSetFsofcCfgEntry->bFsofcControlPktBuffering != OSIX_FALSE)
            {
                pOfcFsofcCfgEntry->MibObject.i4FsofcControlPktBuffering =
                    pOfcSetFsofcCfgEntry->MibObject.i4FsofcControlPktBuffering;
            }

            if (pOfcIsSetFsofcCfgEntry->bFsofcIpReassembleStatus != OSIX_FALSE)
            {
                pOfcFsofcCfgEntry->MibObject.i4FsofcIpReassembleStatus =
                    pOfcSetFsofcCfgEntry->MibObject.i4FsofcIpReassembleStatus;
            }

            if (pOfcIsSetFsofcCfgEntry->bFsofcPortStpStatus != OSIX_FALSE)
            {
                pOfcFsofcCfgEntry->MibObject.i4FsofcPortStpStatus =
                    pOfcSetFsofcCfgEntry->MibObject.i4FsofcPortStpStatus;
            }

            if (pOfcIsSetFsofcCfgEntry->bFsofcTraceEnable != OSIX_FALSE)
            {
                pOfcFsofcCfgEntry->MibObject.u4FsofcTraceEnable =
                    pOfcSetFsofcCfgEntry->MibObject.u4FsofcTraceEnable;
            }

            if (pOfcIsSetFsofcCfgEntry->bFsofcSwitchModeOnConnFailure !=
                OSIX_FALSE)
            {
                pOfcFsofcCfgEntry->MibObject.i4FsofcSwitchModeOnConnFailure =
                    pOfcSetFsofcCfgEntry->
                    MibObject.i4FsofcSwitchModeOnConnFailure;
            }

            if (pOfcIsSetFsofcCfgEntry->bFsofcSwitchEntryStatus != OSIX_FALSE)
            {
                pOfcFsofcCfgEntry->MibObject.i4FsofcSwitchEntryStatus =
                    pOfcSetFsofcCfgEntry->MibObject.i4FsofcSwitchEntryStatus;
            }

            if (pOfcIsSetFsofcCfgEntry->bFsofcHybrid != OSIX_FALSE)
            {
                pOfcFsofcCfgEntry->MibObject.i4FsofcHybrid =
                    pOfcSetFsofcCfgEntry->MibObject.i4FsofcHybrid;
            }

            if ((pOfcSetFsofcCfgEntry->MibObject.i4FsofcSwitchEntryStatus ==
                 CREATE_AND_GO) || ((i4RowCreateOption == 1)
                                    && (pOfcSetFsofcCfgEntry->
                                        MibObject.i4FsofcSwitchEntryStatus ==
                                        ACTIVE)))
            {
                pOfcFsofcCfgEntry->MibObject.i4FsofcSwitchEntryStatus = ACTIVE;
            }
            else if (pOfcSetFsofcCfgEntry->MibObject.i4FsofcSwitchEntryStatus ==
                     CREATE_AND_WAIT)
            {
                pOfcFsofcCfgEntry->MibObject.i4FsofcSwitchEntryStatus =
                    NOT_READY;
            }

            /* Add the new node to the database */
            if (RBTreeAdd
                (gOfcGlobals.OfcGlbMib.FsofcCfgTable,
                 (tRBElem *) pOfcFsofcCfgEntry) != RB_SUCCESS)
            {
                if (OfcSetAllFsofcCfgTableTrigger (pOfcSetFsofcCfgEntry,
                                                   pOfcIsSetFsofcCfgEntry,
                                                   SNMP_FAILURE) !=
                    OSIX_SUCCESS)

                {
                    OFC_TRC ((OFC_UTIL_TRC,
                              "OfcSetAllFsofcCfgTable: OfcSetAllFsofcCfgTableTrigger function returns failure.\r\n"));
                }
                OFC_TRC ((OFC_UTIL_TRC,
                          "OfcSetAllFsofcCfgTable: RBTreeAdd is failed.\r\n"));

                MemReleaseMemBlock (OFC_FSOFCCFGTABLE_POOLID,
                                    (UINT1 *) pOfcFsofcCfgEntry);
                MemReleaseMemBlock (OFC_FSOFCCFGTABLE_POOLID,
                                    (UINT1 *) pOfcOldFsofcCfgEntry);
                MemReleaseMemBlock (OFC_FSOFCCFGTABLE_POOLID,
                                    (UINT1 *) pOfcTrgFsofcCfgEntry);
                MemReleaseMemBlock (OFC_FSOFCCFGTABLE_ISSET_POOLID,
                                    (UINT1 *) pOfcTrgIsSetFsofcCfgEntry);
                return OSIX_FAILURE;
            }
            if (OfcUtilUpdateFsofcCfgTable
                (NULL, pOfcFsofcCfgEntry,
                 pOfcIsSetFsofcCfgEntry) != OSIX_SUCCESS)
            {
                OFC_TRC ((OFC_UTIL_TRC,
                          "OfcSetAllFsofcCfgTable: OfcUtilUpdateFsofcCfgTable function returns failure.\r\n"));

                if (OfcSetAllFsofcCfgTableTrigger (pOfcSetFsofcCfgEntry,
                                                   pOfcIsSetFsofcCfgEntry,
                                                   SNMP_FAILURE) !=
                    OSIX_SUCCESS)

                {
                    OFC_TRC ((OFC_UTIL_TRC,
                              "OfcSetAllFsofcCfgTable: OfcSetAllFsofcCfgTableTrigger function returns failure.\r\n"));

                }
                RBTreeRem (gOfcGlobals.OfcGlbMib.FsofcCfgTable,
                           pOfcFsofcCfgEntry);
                MemReleaseMemBlock (OFC_FSOFCCFGTABLE_POOLID,
                                    (UINT1 *) pOfcFsofcCfgEntry);
                MemReleaseMemBlock (OFC_FSOFCCFGTABLE_POOLID,
                                    (UINT1 *) pOfcOldFsofcCfgEntry);
                MemReleaseMemBlock (OFC_FSOFCCFGTABLE_POOLID,
                                    (UINT1 *) pOfcTrgFsofcCfgEntry);
                MemReleaseMemBlock (OFC_FSOFCCFGTABLE_ISSET_POOLID,
                                    (UINT1 *) pOfcTrgIsSetFsofcCfgEntry);
                return OSIX_FAILURE;
            }

            if ((pOfcSetFsofcCfgEntry->MibObject.i4FsofcSwitchEntryStatus ==
                 CREATE_AND_GO) || ((i4RowCreateOption == 1)
                                    && (pOfcSetFsofcCfgEntry->
                                        MibObject.i4FsofcSwitchEntryStatus ==
                                        ACTIVE)))
            {

                /* For MSR and RM Trigger */
                pOfcTrgFsofcCfgEntry->MibObject.u4FsofcContextId =
                    pOfcSetFsofcCfgEntry->MibObject.u4FsofcContextId;
                pOfcTrgFsofcCfgEntry->MibObject.i4FsofcSwitchEntryStatus =
                    CREATE_AND_WAIT;
                pOfcTrgIsSetFsofcCfgEntry->bFsofcSwitchEntryStatus = OSIX_TRUE;

                if (OfcSetAllFsofcCfgTableTrigger (pOfcTrgFsofcCfgEntry,
                                                   pOfcTrgIsSetFsofcCfgEntry,
                                                   SNMP_FAILURE) !=
                    OSIX_SUCCESS)
                {
                    OFC_TRC ((OFC_UTIL_TRC,
                              "OfcSetAllFsofcCfgTable: OfcSetAllFsofcCfgTableTrigger function returns failure.\r\n"));

                    MemReleaseMemBlock (OFC_FSOFCCFGTABLE_POOLID,
                                        (UINT1 *) pOfcFsofcCfgEntry);
                    MemReleaseMemBlock (OFC_FSOFCCFGTABLE_POOLID,
                                        (UINT1 *) pOfcOldFsofcCfgEntry);
                    MemReleaseMemBlock (OFC_FSOFCCFGTABLE_POOLID,
                                        (UINT1 *) pOfcTrgFsofcCfgEntry);
                    MemReleaseMemBlock (OFC_FSOFCCFGTABLE_ISSET_POOLID,
                                        (UINT1 *) pOfcTrgIsSetFsofcCfgEntry);
                    return OSIX_FAILURE;
                }
            }
            else if (pOfcSetFsofcCfgEntry->MibObject.i4FsofcSwitchEntryStatus ==
                     CREATE_AND_WAIT)
            {

                /* For MSR and RM Trigger */
                pOfcTrgFsofcCfgEntry->MibObject.i4FsofcSwitchEntryStatus =
                    CREATE_AND_WAIT;
                pOfcTrgIsSetFsofcCfgEntry->bFsofcSwitchEntryStatus = OSIX_TRUE;

                if (OfcSetAllFsofcCfgTableTrigger (pOfcTrgFsofcCfgEntry,
                                                   pOfcTrgIsSetFsofcCfgEntry,
                                                   SNMP_FAILURE) !=
                    OSIX_SUCCESS)
                {
                    OFC_TRC ((OFC_UTIL_TRC,
                              "OfcSetAllFsofcCfgTable: OfcSetAllFsofcCfgTableTrigger function returns failure.\r\n"));

                    MemReleaseMemBlock (OFC_FSOFCCFGTABLE_POOLID,
                                        (UINT1 *) pOfcFsofcCfgEntry);
                    MemReleaseMemBlock (OFC_FSOFCCFGTABLE_POOLID,
                                        (UINT1 *) pOfcOldFsofcCfgEntry);
                    MemReleaseMemBlock (OFC_FSOFCCFGTABLE_POOLID,
                                        (UINT1 *) pOfcTrgFsofcCfgEntry);
                    MemReleaseMemBlock (OFC_FSOFCCFGTABLE_ISSET_POOLID,
                                        (UINT1 *) pOfcTrgIsSetFsofcCfgEntry);
                    return OSIX_FAILURE;
                }
            }

            if (pOfcSetFsofcCfgEntry->MibObject.i4FsofcSwitchEntryStatus ==
                CREATE_AND_GO)
            {
                pOfcSetFsofcCfgEntry->MibObject.i4FsofcSwitchEntryStatus =
                    ACTIVE;
            }

            if (OfcSetAllFsofcCfgTableTrigger (pOfcSetFsofcCfgEntry,
                                               pOfcIsSetFsofcCfgEntry,
                                               SNMP_SUCCESS) != OSIX_SUCCESS)
            {
                OFC_TRC ((OFC_UTIL_TRC,
                          "OfcSetAllFsofcCfgTable:  OfcSetAllFsofcCfgTableTrigger function returns failure.\r\n"));
            }
            MemReleaseMemBlock (OFC_FSOFCCFGTABLE_POOLID,
                                (UINT1 *) pOfcOldFsofcCfgEntry);
            MemReleaseMemBlock (OFC_FSOFCCFGTABLE_POOLID,
                                (UINT1 *) pOfcTrgFsofcCfgEntry);
            MemReleaseMemBlock (OFC_FSOFCCFGTABLE_ISSET_POOLID,
                                (UINT1 *) pOfcTrgIsSetFsofcCfgEntry);
            return OSIX_SUCCESS;

        }
        else
        {
            if (OfcSetAllFsofcCfgTableTrigger (pOfcSetFsofcCfgEntry,
                                               pOfcIsSetFsofcCfgEntry,
                                               SNMP_FAILURE) != OSIX_SUCCESS)

            {
                OFC_TRC ((OFC_UTIL_TRC,
                          "OfcSetAllFsofcCfgTable: OfcSetAllFsofcCfgTableTrigger function returns failure.\r\n"));
            }
            OFC_TRC ((OFC_UTIL_TRC, "OfcSetAllFsofcCfgTable: Failure.\r\n"));
            MemReleaseMemBlock (OFC_FSOFCCFGTABLE_POOLID,
                                (UINT1 *) pOfcOldFsofcCfgEntry);
            MemReleaseMemBlock (OFC_FSOFCCFGTABLE_POOLID,
                                (UINT1 *) pOfcTrgFsofcCfgEntry);
            MemReleaseMemBlock (OFC_FSOFCCFGTABLE_ISSET_POOLID,
                                (UINT1 *) pOfcTrgIsSetFsofcCfgEntry);
            return OSIX_FAILURE;
        }
    }
    else if ((pOfcSetFsofcCfgEntry->MibObject.i4FsofcSwitchEntryStatus ==
              CREATE_AND_WAIT)
             || (pOfcSetFsofcCfgEntry->MibObject.i4FsofcSwitchEntryStatus ==
                 CREATE_AND_GO))
    {
        if (OfcSetAllFsofcCfgTableTrigger (pOfcSetFsofcCfgEntry,
                                           pOfcIsSetFsofcCfgEntry,
                                           SNMP_FAILURE) != OSIX_SUCCESS)

        {
            OFC_TRC ((OFC_UTIL_TRC,
                      "OfcSetAllFsofcCfgTable: OfcSetAllFsofcCfgTableTrigger function returns failure.\r\n"));
        }
        OFC_TRC ((OFC_UTIL_TRC,
                  "OfcSetAllFsofcCfgTable: The row is already present.\r\n"));
        MemReleaseMemBlock (OFC_FSOFCCFGTABLE_POOLID,
                            (UINT1 *) pOfcOldFsofcCfgEntry);
        MemReleaseMemBlock (OFC_FSOFCCFGTABLE_POOLID,
                            (UINT1 *) pOfcTrgFsofcCfgEntry);
        MemReleaseMemBlock (OFC_FSOFCCFGTABLE_ISSET_POOLID,
                            (UINT1 *) pOfcTrgIsSetFsofcCfgEntry);
        return OSIX_FAILURE;
    }
    /* Copy the previous values before setting the new values */
    MEMCPY (pOfcOldFsofcCfgEntry, pOfcFsofcCfgEntry,
            sizeof (tOfcFsofcCfgEntry));

    /*Delete the node from the database if the RowStatus given is DESTROY */
    if (pOfcSetFsofcCfgEntry->MibObject.i4FsofcSwitchEntryStatus == DESTROY)
    {
        pOfcFsofcCfgEntry->MibObject.i4FsofcSwitchEntryStatus = DESTROY;

        if (OfcUtilUpdateFsofcCfgTable (pOfcOldFsofcCfgEntry,
                                        pOfcFsofcCfgEntry,
                                        pOfcIsSetFsofcCfgEntry) != OSIX_SUCCESS)
        {

            if (OfcSetAllFsofcCfgTableTrigger (pOfcSetFsofcCfgEntry,
                                               pOfcIsSetFsofcCfgEntry,
                                               SNMP_FAILURE) != OSIX_SUCCESS)

            {
                OFC_TRC ((OFC_UTIL_TRC,
                          "OfcSetAllFsofcCfgTable: OfcSetAllFsofcCfgTableTrigger function returns failure.\r\n"));
            }
            OFC_TRC ((OFC_UTIL_TRC,
                      "OfcSetAllFsofcCfgTable: OfcUtilUpdateFsofcCfgTable function returns failure.\r\n"));
        }
        RBTreeRem (gOfcGlobals.OfcGlbMib.FsofcCfgTable, pOfcFsofcCfgEntry);
        if (OfcSetAllFsofcCfgTableTrigger (pOfcSetFsofcCfgEntry,
                                           pOfcIsSetFsofcCfgEntry,
                                           SNMP_SUCCESS) != OSIX_SUCCESS)
        {
            OFC_TRC ((OFC_UTIL_TRC,
                      "OfcSetAllFsofcCfgTable: OfcSetAllFsofcCfgTableTrigger function returns failure.\r\n"));
            MemReleaseMemBlock (OFC_FSOFCCFGTABLE_POOLID,
                                (UINT1 *) pOfcOldFsofcCfgEntry);
            MemReleaseMemBlock (OFC_FSOFCCFGTABLE_POOLID,
                                (UINT1 *) pOfcTrgFsofcCfgEntry);
            MemReleaseMemBlock (OFC_FSOFCCFGTABLE_ISSET_POOLID,
                                (UINT1 *) pOfcTrgIsSetFsofcCfgEntry);
            return OSIX_FAILURE;
        }
        MemReleaseMemBlock (OFC_FSOFCCFGTABLE_POOLID,
                            (UINT1 *) pOfcFsofcCfgEntry);
        MemReleaseMemBlock (OFC_FSOFCCFGTABLE_POOLID,
                            (UINT1 *) pOfcOldFsofcCfgEntry);
        MemReleaseMemBlock (OFC_FSOFCCFGTABLE_POOLID,
                            (UINT1 *) pOfcTrgFsofcCfgEntry);
        MemReleaseMemBlock (OFC_FSOFCCFGTABLE_ISSET_POOLID,
                            (UINT1 *) pOfcTrgIsSetFsofcCfgEntry);
        return OSIX_SUCCESS;
    }

    /*Function to check whether the given input is same as there in database */
    if (FsofcCfgTableFilterInputs (pOfcFsofcCfgEntry, pOfcSetFsofcCfgEntry,
                                   pOfcIsSetFsofcCfgEntry) != OSIX_TRUE)
    {
        MemReleaseMemBlock (OFC_FSOFCCFGTABLE_POOLID,
                            (UINT1 *) pOfcOldFsofcCfgEntry);
        MemReleaseMemBlock (OFC_FSOFCCFGTABLE_POOLID,
                            (UINT1 *) pOfcTrgFsofcCfgEntry);
        MemReleaseMemBlock (OFC_FSOFCCFGTABLE_ISSET_POOLID,
                            (UINT1 *) pOfcTrgIsSetFsofcCfgEntry);
        return OSIX_SUCCESS;
    }

    /*This condtion is to make the row NOT_IN_SERVICE before modifying the values */
    if ((i4RowStatusLogic == TRUE) &&
        (pOfcFsofcCfgEntry->MibObject.i4FsofcSwitchEntryStatus == ACTIVE) &&
        (pOfcSetFsofcCfgEntry->MibObject.i4FsofcSwitchEntryStatus !=
         NOT_IN_SERVICE))
    {
        pOfcFsofcCfgEntry->MibObject.i4FsofcSwitchEntryStatus = NOT_IN_SERVICE;
        i4RowMakeActive = TRUE;

        /* For MSR and RM Trigger */
        pOfcTrgFsofcCfgEntry->MibObject.i4FsofcSwitchEntryStatus =
            NOT_IN_SERVICE;
        pOfcTrgIsSetFsofcCfgEntry->bFsofcSwitchEntryStatus = OSIX_TRUE;

        if (OfcUtilUpdateFsofcCfgTable (pOfcOldFsofcCfgEntry,
                                        pOfcFsofcCfgEntry,
                                        pOfcIsSetFsofcCfgEntry) != OSIX_SUCCESS)
        {
            /*Restore back with previous values */
            MEMCPY (pOfcFsofcCfgEntry, pOfcOldFsofcCfgEntry,
                    sizeof (tOfcFsofcCfgEntry));
            OFC_TRC ((OFC_UTIL_TRC,
                      "OfcSetAllFsofcCfgTable:                 OfcUtilUpdateFsofcCfgTable Function returns failure.\r\n"));

            if (OfcSetAllFsofcCfgTableTrigger (pOfcSetFsofcCfgEntry,
                                               pOfcIsSetFsofcCfgEntry,
                                               SNMP_FAILURE) != OSIX_SUCCESS)

            {
                OFC_TRC ((OFC_UTIL_TRC,
                          "OfcSetAllFsofcCfgTable: OfcSetAllFsofcCfgTableTrigger function returns failure.\r\n"));
            }
            MemReleaseMemBlock (OFC_FSOFCCFGTABLE_POOLID,
                                (UINT1 *) pOfcOldFsofcCfgEntry);
            MemReleaseMemBlock (OFC_FSOFCCFGTABLE_POOLID,
                                (UINT1 *) pOfcTrgFsofcCfgEntry);
            MemReleaseMemBlock (OFC_FSOFCCFGTABLE_ISSET_POOLID,
                                (UINT1 *) pOfcTrgIsSetFsofcCfgEntry);
            return OSIX_FAILURE;
        }

        if (OfcSetAllFsofcCfgTableTrigger (pOfcTrgFsofcCfgEntry,
                                           pOfcTrgIsSetFsofcCfgEntry,
                                           SNMP_FAILURE) != OSIX_SUCCESS)
        {
            OFC_TRC ((OFC_UTIL_TRC,
                      "OfcSetAllFsofcCfgTable: OfcSetAllFsofcCfgTableTrigger function returns failure.\r\n"));
        }
    }

    if (pOfcSetFsofcCfgEntry->MibObject.i4FsofcSwitchEntryStatus == ACTIVE)
    {
        i4RowMakeActive = TRUE;
    }

    /* Assign values for the existing node */
    if (pOfcIsSetFsofcCfgEntry->bFsofcModuleStatus != OSIX_FALSE)
    {
        pOfcFsofcCfgEntry->MibObject.i4FsofcModuleStatus =
            pOfcSetFsofcCfgEntry->MibObject.i4FsofcModuleStatus;
    }
    if (pOfcIsSetFsofcCfgEntry->bFsofcSupportedVersion != OSIX_FALSE)
    {
        pOfcFsofcCfgEntry->MibObject.i4FsofcSupportedVersion =
            pOfcSetFsofcCfgEntry->MibObject.i4FsofcSupportedVersion;
    }
    if (pOfcIsSetFsofcCfgEntry->bFsofcDefaultFlowMissBehaviour != OSIX_FALSE)
    {
        pOfcFsofcCfgEntry->MibObject.i4FsofcDefaultFlowMissBehaviour =
            pOfcSetFsofcCfgEntry->MibObject.i4FsofcDefaultFlowMissBehaviour;
    }
    if (pOfcIsSetFsofcCfgEntry->bFsofcControlPktBuffering != OSIX_FALSE)
    {
        pOfcFsofcCfgEntry->MibObject.i4FsofcControlPktBuffering =
            pOfcSetFsofcCfgEntry->MibObject.i4FsofcControlPktBuffering;
    }
    if (pOfcIsSetFsofcCfgEntry->bFsofcIpReassembleStatus != OSIX_FALSE)
    {
        pOfcFsofcCfgEntry->MibObject.i4FsofcIpReassembleStatus =
            pOfcSetFsofcCfgEntry->MibObject.i4FsofcIpReassembleStatus;
    }
    if (pOfcIsSetFsofcCfgEntry->bFsofcPortStpStatus != OSIX_FALSE)
    {
        pOfcFsofcCfgEntry->MibObject.i4FsofcPortStpStatus =
            pOfcSetFsofcCfgEntry->MibObject.i4FsofcPortStpStatus;
    }
    if (pOfcIsSetFsofcCfgEntry->bFsofcTraceEnable != OSIX_FALSE)
    {
        pOfcFsofcCfgEntry->MibObject.u4FsofcTraceEnable =
            pOfcSetFsofcCfgEntry->MibObject.u4FsofcTraceEnable;
    }
    if (pOfcIsSetFsofcCfgEntry->bFsofcSwitchModeOnConnFailure != OSIX_FALSE)
    {
        pOfcFsofcCfgEntry->MibObject.i4FsofcSwitchModeOnConnFailure =
            pOfcSetFsofcCfgEntry->MibObject.i4FsofcSwitchModeOnConnFailure;
    }
    if (pOfcIsSetFsofcCfgEntry->bFsofcSwitchEntryStatus != OSIX_FALSE)
    {
        pOfcFsofcCfgEntry->MibObject.i4FsofcSwitchEntryStatus =
            pOfcSetFsofcCfgEntry->MibObject.i4FsofcSwitchEntryStatus;
    }
    if (pOfcIsSetFsofcCfgEntry->bFsofcHybrid != OSIX_FALSE)
    {
        pOfcFsofcCfgEntry->MibObject.i4FsofcHybrid =
            pOfcSetFsofcCfgEntry->MibObject.i4FsofcHybrid;
    }

    /*This condtion is to make the row back to ACTIVE after modifying the values */
    if (i4RowMakeActive == TRUE)
    {
        pOfcFsofcCfgEntry->MibObject.i4FsofcSwitchEntryStatus = ACTIVE;
    }

    if (OfcUtilUpdateFsofcCfgTable (pOfcOldFsofcCfgEntry,
                                    pOfcFsofcCfgEntry,
                                    pOfcIsSetFsofcCfgEntry) != OSIX_SUCCESS)
    {

        if (OfcSetAllFsofcCfgTableTrigger (pOfcSetFsofcCfgEntry,
                                           pOfcIsSetFsofcCfgEntry,
                                           SNMP_FAILURE) != OSIX_SUCCESS)

        {
            OFC_TRC ((OFC_UTIL_TRC,
                      "OfcSetAllFsofcCfgTable: OfcSetAllFsofcCfgTableTrigger function returns failure.\r\n"));

        }
        OFC_TRC ((OFC_UTIL_TRC,
                  "OfcSetAllFsofcCfgTable: OfcUtilUpdateFsofcCfgTable function returns failure.\r\n"));
        /*Restore back with previous values */
        MEMCPY (pOfcFsofcCfgEntry, pOfcOldFsofcCfgEntry,
                sizeof (tOfcFsofcCfgEntry));
        MemReleaseMemBlock (OFC_FSOFCCFGTABLE_POOLID,
                            (UINT1 *) pOfcOldFsofcCfgEntry);
        MemReleaseMemBlock (OFC_FSOFCCFGTABLE_POOLID,
                            (UINT1 *) pOfcTrgFsofcCfgEntry);
        MemReleaseMemBlock (OFC_FSOFCCFGTABLE_ISSET_POOLID,
                            (UINT1 *) pOfcTrgIsSetFsofcCfgEntry);
        return OSIX_FAILURE;

    }
    if (OfcSetAllFsofcCfgTableTrigger (pOfcSetFsofcCfgEntry,
                                       pOfcIsSetFsofcCfgEntry,
                                       SNMP_SUCCESS) != OSIX_SUCCESS)
    {

        OFC_TRC ((OFC_UTIL_TRC,
                  "OfcSetAllFsofcCfgTable: OfcSetAllFsofcCfgTableTrigger function returns failure.\r\n"));
    }

    MemReleaseMemBlock (OFC_FSOFCCFGTABLE_POOLID,
                        (UINT1 *) pOfcOldFsofcCfgEntry);
    MemReleaseMemBlock (OFC_FSOFCCFGTABLE_POOLID,
                        (UINT1 *) pOfcTrgFsofcCfgEntry);
    MemReleaseMemBlock (OFC_FSOFCCFGTABLE_ISSET_POOLID,
                        (UINT1 *) pOfcTrgIsSetFsofcCfgEntry);
    return OSIX_SUCCESS;

}

/****************************************************************************
 Function    :  OfcSetAllFsofcControllerConnTable
 Input       :  pOfcSetFsofcControllerConnEntry
                pOfcIsSetFsofcControllerConnEntry
                i4RowStatusLogic
                i4RowCreateOption
 Description :  This Routine Take the Indices &
                Sets the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
OfcSetAllFsofcControllerConnTable (tOfcFsofcControllerConnEntry *
                                   pOfcSetFsofcControllerConnEntry,
                                   tOfcIsSetFsofcControllerConnEntry *
                                   pOfcIsSetFsofcControllerConnEntry,
                                   INT4 i4RowStatusLogic,
                                   INT4 i4RowCreateOption)
{
    tOfcFsofcControllerConnEntry *pOfcFsofcControllerConnEntry = NULL;
    tOfcFsofcControllerConnEntry *pOfcOldFsofcControllerConnEntry = NULL;
    tOfcFsofcControllerConnEntry *pOfcTrgFsofcControllerConnEntry = NULL;
    tOfcIsSetFsofcControllerConnEntry *pOfcTrgIsSetFsofcControllerConnEntry =
        NULL;
    INT4                i4RowMakeActive = FALSE;

    pOfcOldFsofcControllerConnEntry =
        (tOfcFsofcControllerConnEntry *)
        MemAllocMemBlk (OFC_FSOFCCONTROLLERCONNTABLE_POOLID);
    if (pOfcOldFsofcControllerConnEntry == NULL)
    {
        return OSIX_FAILURE;
    }
    pOfcTrgFsofcControllerConnEntry =
        (tOfcFsofcControllerConnEntry *)
        MemAllocMemBlk (OFC_FSOFCCONTROLLERCONNTABLE_POOLID);
    if (pOfcTrgFsofcControllerConnEntry == NULL)
    {
        MemReleaseMemBlock (OFC_FSOFCCONTROLLERCONNTABLE_POOLID,
                            (UINT1 *) pOfcOldFsofcControllerConnEntry);
        return OSIX_FAILURE;
    }
    pOfcTrgIsSetFsofcControllerConnEntry =
        (tOfcIsSetFsofcControllerConnEntry *)
        MemAllocMemBlk (OFC_FSOFCCONTROLLERCONNTABLE_ISSET_POOLID);
    if (pOfcTrgIsSetFsofcControllerConnEntry == NULL)
    {
        MemReleaseMemBlock (OFC_FSOFCCONTROLLERCONNTABLE_POOLID,
                            (UINT1 *) pOfcOldFsofcControllerConnEntry);
        MemReleaseMemBlock (OFC_FSOFCCONTROLLERCONNTABLE_POOLID,
                            (UINT1 *) pOfcTrgFsofcControllerConnEntry);
        return OSIX_FAILURE;
    }
    MEMSET (pOfcOldFsofcControllerConnEntry, 0,
            sizeof (tOfcFsofcControllerConnEntry));
    MEMSET (pOfcTrgFsofcControllerConnEntry, 0,
            sizeof (tOfcFsofcControllerConnEntry));
    MEMSET (pOfcTrgIsSetFsofcControllerConnEntry, 0,
            sizeof (tOfcIsSetFsofcControllerConnEntry));

    /* Check whether the node is already present */
    pOfcFsofcControllerConnEntry =
        RBTreeGet (gOfcGlobals.OfcGlbMib.FsofcControllerConnTable,
                   (tRBElem *) pOfcSetFsofcControllerConnEntry);

    if (pOfcFsofcControllerConnEntry == NULL)
    {
        /* Create the node if the RowStatus given is CREATE_AND_WAIT or CREATE_AND_GO */
        if ((pOfcSetFsofcControllerConnEntry->
             MibObject.i4FsofcControllerConnEntryStatus == CREATE_AND_WAIT)
            || (pOfcSetFsofcControllerConnEntry->
                MibObject.i4FsofcControllerConnEntryStatus == CREATE_AND_GO)
            ||
            ((pOfcSetFsofcControllerConnEntry->
              MibObject.i4FsofcControllerConnEntryStatus == ACTIVE)
             && (i4RowCreateOption == 1)))
        {
            /* Allocate memory for the new node */
            pOfcFsofcControllerConnEntry =
                (tOfcFsofcControllerConnEntry *)
                MemAllocMemBlk (OFC_FSOFCCONTROLLERCONNTABLE_POOLID);
            if (pOfcFsofcControllerConnEntry == NULL)
            {
                if (OfcSetAllFsofcControllerConnTableTrigger
                    (pOfcSetFsofcControllerConnEntry,
                     pOfcIsSetFsofcControllerConnEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)

                {
                    OFC_TRC ((OFC_UTIL_TRC,
                              "OfcSetAllFsofcControllerConnTable:OfcSetAllFsofcControllerConnTableTrigger function fails\r\n"));

                }
                OFC_TRC ((OFC_UTIL_TRC,
                          "OfcSetAllFsofcControllerConnTable: Fail to Allocate Memory.\r\n"));
                MemReleaseMemBlock (OFC_FSOFCCONTROLLERCONNTABLE_POOLID,
                                    (UINT1 *) pOfcOldFsofcControllerConnEntry);
                MemReleaseMemBlock (OFC_FSOFCCONTROLLERCONNTABLE_POOLID,
                                    (UINT1 *) pOfcTrgFsofcControllerConnEntry);
                MemReleaseMemBlock (OFC_FSOFCCONTROLLERCONNTABLE_ISSET_POOLID,
                                    (UINT1 *)
                                    pOfcTrgIsSetFsofcControllerConnEntry);
                return OSIX_FAILURE;
            }

            MEMSET (pOfcFsofcControllerConnEntry, 0,
                    sizeof (tOfcFsofcControllerConnEntry));
            if ((OfcInitializeFsofcControllerConnTable
                 (pOfcFsofcControllerConnEntry)) == OSIX_FAILURE)
            {
                if (OfcSetAllFsofcControllerConnTableTrigger
                    (pOfcSetFsofcControllerConnEntry,
                     pOfcIsSetFsofcControllerConnEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)

                {
                    OFC_TRC ((OFC_UTIL_TRC,
                              "OfcSetAllFsofcControllerConnTable:OfcSetAllFsofcControllerConnTableTrigger function fails\r\n"));

                }
                OFC_TRC ((OFC_UTIL_TRC,
                          "OfcSetAllFsofcControllerConnTable: Fail to Initialize the Objects.\r\n"));

                MemReleaseMemBlock (OFC_FSOFCCONTROLLERCONNTABLE_POOLID,
                                    (UINT1 *) pOfcFsofcControllerConnEntry);
                MemReleaseMemBlock (OFC_FSOFCCONTROLLERCONNTABLE_POOLID,
                                    (UINT1 *) pOfcOldFsofcControllerConnEntry);
                MemReleaseMemBlock (OFC_FSOFCCONTROLLERCONNTABLE_POOLID,
                                    (UINT1 *) pOfcTrgFsofcControllerConnEntry);
                MemReleaseMemBlock (OFC_FSOFCCONTROLLERCONNTABLE_ISSET_POOLID,
                                    (UINT1 *)
                                    pOfcTrgIsSetFsofcControllerConnEntry);
                return OSIX_FAILURE;
            }
            /* Assign values for the new node */
            if (pOfcIsSetFsofcControllerConnEntry->bFsofcControllerIpAddrType !=
                OSIX_FALSE)
            {
                pOfcFsofcControllerConnEntry->
                    MibObject.i4FsofcControllerIpAddrType =
                    pOfcSetFsofcControllerConnEntry->
                    MibObject.i4FsofcControllerIpAddrType;
            }

            if (pOfcIsSetFsofcControllerConnEntry->bFsofcControllerIpAddress !=
                OSIX_FALSE)
            {
                MEMCPY (pOfcFsofcControllerConnEntry->
                        MibObject.au1FsofcControllerIpAddress,
                        pOfcSetFsofcControllerConnEntry->
                        MibObject.au1FsofcControllerIpAddress,
                        pOfcSetFsofcControllerConnEntry->
                        MibObject.i4FsofcControllerIpAddressLen);

                pOfcFsofcControllerConnEntry->
                    MibObject.i4FsofcControllerIpAddressLen =
                    pOfcSetFsofcControllerConnEntry->
                    MibObject.i4FsofcControllerIpAddressLen;
            }

            if (pOfcIsSetFsofcControllerConnEntry->bFsofcControllerConnAuxId !=
                OSIX_FALSE)
            {
                pOfcFsofcControllerConnEntry->
                    MibObject.i4FsofcControllerConnAuxId =
                    pOfcSetFsofcControllerConnEntry->
                    MibObject.i4FsofcControllerConnAuxId;
            }

            if (pOfcIsSetFsofcControllerConnEntry->bFsofcControllerConnPort !=
                OSIX_FALSE)
            {
                pOfcFsofcControllerConnEntry->
                    MibObject.i4FsofcControllerConnPort =
                    pOfcSetFsofcControllerConnEntry->
                    MibObject.i4FsofcControllerConnPort;
            }

            if (pOfcIsSetFsofcControllerConnEntry->bFsofcControllerConnProtocol
                != OSIX_FALSE)
            {
                pOfcFsofcControllerConnEntry->
                    MibObject.i4FsofcControllerConnProtocol =
                    pOfcSetFsofcControllerConnEntry->
                    MibObject.i4FsofcControllerConnProtocol;
            }

            if (pOfcIsSetFsofcControllerConnEntry->bFsofcControllerRole !=
                OSIX_FALSE)
            {
                pOfcFsofcControllerConnEntry->MibObject.i4FsofcControllerRole =
                    pOfcSetFsofcControllerConnEntry->
                    MibObject.i4FsofcControllerRole;
            }

            if (pOfcIsSetFsofcControllerConnEntry->
                bFsofcControllerConnEchoReqCount != OSIX_FALSE)
            {
                pOfcFsofcControllerConnEntry->
                    MibObject.i4FsofcControllerConnEchoReqCount =
                    pOfcSetFsofcControllerConnEntry->
                    MibObject.i4FsofcControllerConnEchoReqCount;
            }

            if (pOfcIsSetFsofcControllerConnEntry->
                bFsofcControllerConnEntryStatus != OSIX_FALSE)
            {
                pOfcFsofcControllerConnEntry->
                    MibObject.i4FsofcControllerConnEntryStatus =
                    pOfcSetFsofcControllerConnEntry->
                    MibObject.i4FsofcControllerConnEntryStatus;
            }

            if (pOfcIsSetFsofcControllerConnEntry->bFsofcControllerConnBand !=
                OSIX_FALSE)
            {
                pOfcFsofcControllerConnEntry->
                    MibObject.i4FsofcControllerConnBand =
                    pOfcSetFsofcControllerConnEntry->
                    MibObject.i4FsofcControllerConnBand;
            }

            if (pOfcIsSetFsofcControllerConnEntry->bFsofcContextId !=
                OSIX_FALSE)
            {
                pOfcFsofcControllerConnEntry->MibObject.u4FsofcContextId =
                    pOfcSetFsofcControllerConnEntry->MibObject.u4FsofcContextId;
            }

            if ((pOfcSetFsofcControllerConnEntry->
                 MibObject.i4FsofcControllerConnEntryStatus == CREATE_AND_GO)
                || ((i4RowCreateOption == 1)
                    && (pOfcSetFsofcControllerConnEntry->
                        MibObject.i4FsofcControllerConnEntryStatus == ACTIVE)))
            {
                pOfcFsofcControllerConnEntry->
                    MibObject.i4FsofcControllerConnEntryStatus = ACTIVE;
            }
            else if (pOfcSetFsofcControllerConnEntry->
                     MibObject.i4FsofcControllerConnEntryStatus ==
                     CREATE_AND_WAIT)
            {
                pOfcFsofcControllerConnEntry->
                    MibObject.i4FsofcControllerConnEntryStatus = NOT_READY;
            }

            /* Add the new node to the database */
            if (RBTreeAdd
                (gOfcGlobals.OfcGlbMib.FsofcControllerConnTable,
                 (tRBElem *) pOfcFsofcControllerConnEntry) != RB_SUCCESS)
            {
                if (OfcSetAllFsofcControllerConnTableTrigger
                    (pOfcSetFsofcControllerConnEntry,
                     pOfcIsSetFsofcControllerConnEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)

                {
                    OFC_TRC ((OFC_UTIL_TRC,
                              "OfcSetAllFsofcControllerConnTable: OfcSetAllFsofcControllerConnTableTrigger function returns failure.\r\n"));
                }
                OFC_TRC ((OFC_UTIL_TRC,
                          "OfcSetAllFsofcControllerConnTable: RBTreeAdd is failed.\r\n"));

                MemReleaseMemBlock (OFC_FSOFCCONTROLLERCONNTABLE_POOLID,
                                    (UINT1 *) pOfcFsofcControllerConnEntry);
                MemReleaseMemBlock (OFC_FSOFCCONTROLLERCONNTABLE_POOLID,
                                    (UINT1 *) pOfcOldFsofcControllerConnEntry);
                MemReleaseMemBlock (OFC_FSOFCCONTROLLERCONNTABLE_POOLID,
                                    (UINT1 *) pOfcTrgFsofcControllerConnEntry);
                MemReleaseMemBlock (OFC_FSOFCCONTROLLERCONNTABLE_ISSET_POOLID,
                                    (UINT1 *)
                                    pOfcTrgIsSetFsofcControllerConnEntry);
                return OSIX_FAILURE;
            }
            if (OfcUtilUpdateFsofcControllerConnTable
                (NULL, pOfcFsofcControllerConnEntry,
                 pOfcIsSetFsofcControllerConnEntry) != OSIX_SUCCESS)
            {
                OFC_TRC ((OFC_UTIL_TRC,
                          "OfcSetAllFsofcControllerConnTable: OfcUtilUpdateFsofcControllerConnTable function returns failure.\r\n"));

                if (OfcSetAllFsofcControllerConnTableTrigger
                    (pOfcSetFsofcControllerConnEntry,
                     pOfcIsSetFsofcControllerConnEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)

                {
                    OFC_TRC ((OFC_UTIL_TRC,
                              "OfcSetAllFsofcControllerConnTable: OfcSetAllFsofcControllerConnTableTrigger function returns failure.\r\n"));

                }
                RBTreeRem (gOfcGlobals.OfcGlbMib.FsofcControllerConnTable,
                           pOfcFsofcControllerConnEntry);
                MemReleaseMemBlock (OFC_FSOFCCONTROLLERCONNTABLE_POOLID,
                                    (UINT1 *) pOfcFsofcControllerConnEntry);
                MemReleaseMemBlock (OFC_FSOFCCONTROLLERCONNTABLE_POOLID,
                                    (UINT1 *) pOfcOldFsofcControllerConnEntry);
                MemReleaseMemBlock (OFC_FSOFCCONTROLLERCONNTABLE_POOLID,
                                    (UINT1 *) pOfcTrgFsofcControllerConnEntry);
                MemReleaseMemBlock (OFC_FSOFCCONTROLLERCONNTABLE_ISSET_POOLID,
                                    (UINT1 *)
                                    pOfcTrgIsSetFsofcControllerConnEntry);
                return OSIX_FAILURE;
            }

            if ((pOfcSetFsofcControllerConnEntry->
                 MibObject.i4FsofcControllerConnEntryStatus == CREATE_AND_GO)
                || ((i4RowCreateOption == 1)
                    && (pOfcSetFsofcControllerConnEntry->
                        MibObject.i4FsofcControllerConnEntryStatus == ACTIVE)))
            {

                /* For MSR and RM Trigger */
                pOfcTrgFsofcControllerConnEntry->
                    MibObject.i4FsofcControllerIpAddrType =
                    pOfcSetFsofcControllerConnEntry->
                    MibObject.i4FsofcControllerIpAddrType;
                MEMCPY (pOfcTrgFsofcControllerConnEntry->
                        MibObject.au1FsofcControllerIpAddress,
                        pOfcSetFsofcControllerConnEntry->
                        MibObject.au1FsofcControllerIpAddress,
                        pOfcSetFsofcControllerConnEntry->
                        MibObject.i4FsofcControllerIpAddressLen);

                pOfcTrgFsofcControllerConnEntry->
                    MibObject.i4FsofcControllerIpAddressLen =
                    pOfcSetFsofcControllerConnEntry->
                    MibObject.i4FsofcControllerIpAddressLen;
                pOfcTrgFsofcControllerConnEntry->
                    MibObject.i4FsofcControllerConnAuxId =
                    pOfcSetFsofcControllerConnEntry->
                    MibObject.i4FsofcControllerConnAuxId;
                pOfcTrgFsofcControllerConnEntry->MibObject.u4FsofcContextId =
                    pOfcSetFsofcControllerConnEntry->MibObject.u4FsofcContextId;
                pOfcTrgFsofcControllerConnEntry->
                    MibObject.i4FsofcControllerConnEntryStatus =
                    CREATE_AND_WAIT;
                pOfcTrgIsSetFsofcControllerConnEntry->
                    bFsofcControllerConnEntryStatus = OSIX_TRUE;

                if (OfcSetAllFsofcControllerConnTableTrigger
                    (pOfcTrgFsofcControllerConnEntry,
                     pOfcTrgIsSetFsofcControllerConnEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)
                {
                    OFC_TRC ((OFC_UTIL_TRC,
                              "OfcSetAllFsofcControllerConnTable: OfcSetAllFsofcControllerConnTableTrigger function returns failure.\r\n"));

                    MemReleaseMemBlock (OFC_FSOFCCONTROLLERCONNTABLE_POOLID,
                                        (UINT1 *) pOfcFsofcControllerConnEntry);
                    MemReleaseMemBlock (OFC_FSOFCCONTROLLERCONNTABLE_POOLID,
                                        (UINT1 *)
                                        pOfcOldFsofcControllerConnEntry);
                    MemReleaseMemBlock (OFC_FSOFCCONTROLLERCONNTABLE_POOLID,
                                        (UINT1 *)
                                        pOfcTrgFsofcControllerConnEntry);
                    MemReleaseMemBlock
                        (OFC_FSOFCCONTROLLERCONNTABLE_ISSET_POOLID,
                         (UINT1 *) pOfcTrgIsSetFsofcControllerConnEntry);
                    return OSIX_FAILURE;
                }
            }
            else if (pOfcSetFsofcControllerConnEntry->
                     MibObject.i4FsofcControllerConnEntryStatus ==
                     CREATE_AND_WAIT)
            {

                /* For MSR and RM Trigger */
                pOfcTrgFsofcControllerConnEntry->
                    MibObject.i4FsofcControllerConnEntryStatus =
                    CREATE_AND_WAIT;
                pOfcTrgIsSetFsofcControllerConnEntry->
                    bFsofcControllerConnEntryStatus = OSIX_TRUE;

                if (OfcSetAllFsofcControllerConnTableTrigger
                    (pOfcTrgFsofcControllerConnEntry,
                     pOfcTrgIsSetFsofcControllerConnEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)
                {
                    OFC_TRC ((OFC_UTIL_TRC,
                              "OfcSetAllFsofcControllerConnTable: OfcSetAllFsofcControllerConnTableTrigger function returns failure.\r\n"));

                    MemReleaseMemBlock (OFC_FSOFCCONTROLLERCONNTABLE_POOLID,
                                        (UINT1 *) pOfcFsofcControllerConnEntry);
                    MemReleaseMemBlock (OFC_FSOFCCONTROLLERCONNTABLE_POOLID,
                                        (UINT1 *)
                                        pOfcOldFsofcControllerConnEntry);
                    MemReleaseMemBlock (OFC_FSOFCCONTROLLERCONNTABLE_POOLID,
                                        (UINT1 *)
                                        pOfcTrgFsofcControllerConnEntry);
                    MemReleaseMemBlock
                        (OFC_FSOFCCONTROLLERCONNTABLE_ISSET_POOLID,
                         (UINT1 *) pOfcTrgIsSetFsofcControllerConnEntry);
                    return OSIX_FAILURE;
                }
            }

            if (pOfcSetFsofcControllerConnEntry->
                MibObject.i4FsofcControllerConnEntryStatus == CREATE_AND_GO)
            {
                pOfcSetFsofcControllerConnEntry->
                    MibObject.i4FsofcControllerConnEntryStatus = ACTIVE;
            }

            if (OfcSetAllFsofcControllerConnTableTrigger
                (pOfcSetFsofcControllerConnEntry,
                 pOfcIsSetFsofcControllerConnEntry,
                 SNMP_SUCCESS) != OSIX_SUCCESS)
            {
                OFC_TRC ((OFC_UTIL_TRC,
                          "OfcSetAllFsofcControllerConnTable:  OfcSetAllFsofcControllerConnTableTrigger function returns failure.\r\n"));
            }
            MemReleaseMemBlock (OFC_FSOFCCONTROLLERCONNTABLE_POOLID,
                                (UINT1 *) pOfcOldFsofcControllerConnEntry);
            MemReleaseMemBlock (OFC_FSOFCCONTROLLERCONNTABLE_POOLID,
                                (UINT1 *) pOfcTrgFsofcControllerConnEntry);
            MemReleaseMemBlock (OFC_FSOFCCONTROLLERCONNTABLE_ISSET_POOLID,
                                (UINT1 *) pOfcTrgIsSetFsofcControllerConnEntry);
            return OSIX_SUCCESS;

        }
        else
        {
            if (OfcSetAllFsofcControllerConnTableTrigger
                (pOfcSetFsofcControllerConnEntry,
                 pOfcIsSetFsofcControllerConnEntry,
                 SNMP_FAILURE) != OSIX_SUCCESS)

            {
                OFC_TRC ((OFC_UTIL_TRC,
                          "OfcSetAllFsofcControllerConnTable: OfcSetAllFsofcControllerConnTableTrigger function returns failure.\r\n"));
            }
            OFC_TRC ((OFC_UTIL_TRC,
                      "OfcSetAllFsofcControllerConnTable: Failure.\r\n"));
            MemReleaseMemBlock (OFC_FSOFCCONTROLLERCONNTABLE_POOLID,
                                (UINT1 *) pOfcOldFsofcControllerConnEntry);
            MemReleaseMemBlock (OFC_FSOFCCONTROLLERCONNTABLE_POOLID,
                                (UINT1 *) pOfcTrgFsofcControllerConnEntry);
            MemReleaseMemBlock (OFC_FSOFCCONTROLLERCONNTABLE_ISSET_POOLID,
                                (UINT1 *) pOfcTrgIsSetFsofcControllerConnEntry);
            return OSIX_FAILURE;
        }
    }
    else if ((pOfcSetFsofcControllerConnEntry->
              MibObject.i4FsofcControllerConnEntryStatus == CREATE_AND_WAIT)
             || (pOfcSetFsofcControllerConnEntry->
                 MibObject.i4FsofcControllerConnEntryStatus == CREATE_AND_GO))
    {
        if (OfcSetAllFsofcControllerConnTableTrigger
            (pOfcSetFsofcControllerConnEntry, pOfcIsSetFsofcControllerConnEntry,
             SNMP_FAILURE) != OSIX_SUCCESS)

        {
            OFC_TRC ((OFC_UTIL_TRC,
                      "OfcSetAllFsofcControllerConnTable: OfcSetAllFsofcControllerConnTableTrigger function returns failure.\r\n"));
        }
        OFC_TRC ((OFC_UTIL_TRC,
                  "OfcSetAllFsofcControllerConnTable: The row is already present.\r\n"));
        MemReleaseMemBlock (OFC_FSOFCCONTROLLERCONNTABLE_POOLID,
                            (UINT1 *) pOfcOldFsofcControllerConnEntry);
        MemReleaseMemBlock (OFC_FSOFCCONTROLLERCONNTABLE_POOLID,
                            (UINT1 *) pOfcTrgFsofcControllerConnEntry);
        MemReleaseMemBlock (OFC_FSOFCCONTROLLERCONNTABLE_ISSET_POOLID,
                            (UINT1 *) pOfcTrgIsSetFsofcControllerConnEntry);
        return OSIX_FAILURE;
    }
    /* Copy the previous values before setting the new values */
    MEMCPY (pOfcOldFsofcControllerConnEntry, pOfcFsofcControllerConnEntry,
            sizeof (tOfcFsofcControllerConnEntry));

    /*Delete the node from the database if the RowStatus given is DESTROY */
    if (pOfcSetFsofcControllerConnEntry->
        MibObject.i4FsofcControllerConnEntryStatus == DESTROY)
    {
        pOfcFsofcControllerConnEntry->
            MibObject.i4FsofcControllerConnEntryStatus = DESTROY;

        if (OfcUtilUpdateFsofcControllerConnTable
            (pOfcOldFsofcControllerConnEntry, pOfcFsofcControllerConnEntry,
             pOfcIsSetFsofcControllerConnEntry) != OSIX_SUCCESS)
        {

            if (OfcSetAllFsofcControllerConnTableTrigger
                (pOfcSetFsofcControllerConnEntry,
                 pOfcIsSetFsofcControllerConnEntry,
                 SNMP_FAILURE) != OSIX_SUCCESS)

            {
                OFC_TRC ((OFC_UTIL_TRC,
                          "OfcSetAllFsofcControllerConnTable: OfcSetAllFsofcControllerConnTableTrigger function returns failure.\r\n"));
            }
            OFC_TRC ((OFC_UTIL_TRC,
                      "OfcSetAllFsofcControllerConnTable: OfcUtilUpdateFsofcControllerConnTable function returns failure.\r\n"));
        }
        RBTreeRem (gOfcGlobals.OfcGlbMib.FsofcControllerConnTable,
                   pOfcFsofcControllerConnEntry);
        if (OfcSetAllFsofcControllerConnTableTrigger
            (pOfcSetFsofcControllerConnEntry, pOfcIsSetFsofcControllerConnEntry,
             SNMP_SUCCESS) != OSIX_SUCCESS)
        {
            OFC_TRC ((OFC_UTIL_TRC,
                      "OfcSetAllFsofcControllerConnTable: OfcSetAllFsofcControllerConnTableTrigger function returns failure.\r\n"));
            MemReleaseMemBlock (OFC_FSOFCCONTROLLERCONNTABLE_POOLID,
                                (UINT1 *) pOfcOldFsofcControllerConnEntry);
            MemReleaseMemBlock (OFC_FSOFCCONTROLLERCONNTABLE_POOLID,
                                (UINT1 *) pOfcTrgFsofcControllerConnEntry);
            MemReleaseMemBlock (OFC_FSOFCCONTROLLERCONNTABLE_ISSET_POOLID,
                                (UINT1 *) pOfcTrgIsSetFsofcControllerConnEntry);
            return OSIX_FAILURE;
        }
        MemReleaseMemBlock (OFC_FSOFCCONTROLLERCONNTABLE_POOLID,
                            (UINT1 *) pOfcFsofcControllerConnEntry);
        MemReleaseMemBlock (OFC_FSOFCCONTROLLERCONNTABLE_POOLID,
                            (UINT1 *) pOfcOldFsofcControllerConnEntry);
        MemReleaseMemBlock (OFC_FSOFCCONTROLLERCONNTABLE_POOLID,
                            (UINT1 *) pOfcTrgFsofcControllerConnEntry);
        MemReleaseMemBlock (OFC_FSOFCCONTROLLERCONNTABLE_ISSET_POOLID,
                            (UINT1 *) pOfcTrgIsSetFsofcControllerConnEntry);
        return OSIX_SUCCESS;
    }

    /*Function to check whether the given input is same as there in database */
    if (FsofcControllerConnTableFilterInputs
        (pOfcFsofcControllerConnEntry, pOfcSetFsofcControllerConnEntry,
         pOfcIsSetFsofcControllerConnEntry) != OSIX_TRUE)
    {
        MemReleaseMemBlock (OFC_FSOFCCONTROLLERCONNTABLE_POOLID,
                            (UINT1 *) pOfcOldFsofcControllerConnEntry);
        MemReleaseMemBlock (OFC_FSOFCCONTROLLERCONNTABLE_POOLID,
                            (UINT1 *) pOfcTrgFsofcControllerConnEntry);
        MemReleaseMemBlock (OFC_FSOFCCONTROLLERCONNTABLE_ISSET_POOLID,
                            (UINT1 *) pOfcTrgIsSetFsofcControllerConnEntry);
        return OSIX_SUCCESS;
    }

    /*This condtion is to make the row NOT_IN_SERVICE before modifying the values */
    if ((i4RowStatusLogic == TRUE) &&
        (pOfcFsofcControllerConnEntry->
         MibObject.i4FsofcControllerConnEntryStatus == ACTIVE)
        && (pOfcSetFsofcControllerConnEntry->
            MibObject.i4FsofcControllerConnEntryStatus != NOT_IN_SERVICE))
    {
        pOfcFsofcControllerConnEntry->
            MibObject.i4FsofcControllerConnEntryStatus = NOT_IN_SERVICE;
        i4RowMakeActive = TRUE;

        /* For MSR and RM Trigger */
        pOfcTrgFsofcControllerConnEntry->
            MibObject.i4FsofcControllerConnEntryStatus = NOT_IN_SERVICE;
        pOfcTrgIsSetFsofcControllerConnEntry->bFsofcControllerConnEntryStatus =
            OSIX_TRUE;

        if (OfcUtilUpdateFsofcControllerConnTable
            (pOfcOldFsofcControllerConnEntry, pOfcFsofcControllerConnEntry,
             pOfcIsSetFsofcControllerConnEntry) != OSIX_SUCCESS)
        {
            /*Restore back with previous values */
            MEMCPY (pOfcFsofcControllerConnEntry,
                    pOfcOldFsofcControllerConnEntry,
                    sizeof (tOfcFsofcControllerConnEntry));
            OFC_TRC ((OFC_UTIL_TRC,
                      "OfcSetAllFsofcControllerConnTable:                 OfcUtilUpdateFsofcControllerConnTable Function returns failure.\r\n"));

            if (OfcSetAllFsofcControllerConnTableTrigger
                (pOfcSetFsofcControllerConnEntry,
                 pOfcIsSetFsofcControllerConnEntry,
                 SNMP_FAILURE) != OSIX_SUCCESS)

            {
                OFC_TRC ((OFC_UTIL_TRC,
                          "OfcSetAllFsofcControllerConnTable: OfcSetAllFsofcControllerConnTableTrigger function returns failure.\r\n"));
            }
            MemReleaseMemBlock (OFC_FSOFCCONTROLLERCONNTABLE_POOLID,
                                (UINT1 *) pOfcOldFsofcControllerConnEntry);
            MemReleaseMemBlock (OFC_FSOFCCONTROLLERCONNTABLE_POOLID,
                                (UINT1 *) pOfcTrgFsofcControllerConnEntry);
            MemReleaseMemBlock (OFC_FSOFCCONTROLLERCONNTABLE_ISSET_POOLID,
                                (UINT1 *) pOfcTrgIsSetFsofcControllerConnEntry);
            return OSIX_FAILURE;
        }

        if (OfcSetAllFsofcControllerConnTableTrigger
            (pOfcTrgFsofcControllerConnEntry,
             pOfcTrgIsSetFsofcControllerConnEntry,
             SNMP_FAILURE) != OSIX_SUCCESS)
        {
            OFC_TRC ((OFC_UTIL_TRC,
                      "OfcSetAllFsofcControllerConnTable: OfcSetAllFsofcControllerConnTableTrigger function returns failure.\r\n"));
        }
    }

    if (pOfcSetFsofcControllerConnEntry->
        MibObject.i4FsofcControllerConnEntryStatus == ACTIVE)
    {
        i4RowMakeActive = TRUE;
    }

    /* Assign values for the existing node */
    if (pOfcIsSetFsofcControllerConnEntry->bFsofcControllerConnPort !=
        OSIX_FALSE)
    {
        pOfcFsofcControllerConnEntry->MibObject.i4FsofcControllerConnPort =
            pOfcSetFsofcControllerConnEntry->
            MibObject.i4FsofcControllerConnPort;
    }
    if (pOfcIsSetFsofcControllerConnEntry->bFsofcControllerConnProtocol !=
        OSIX_FALSE)
    {
        pOfcFsofcControllerConnEntry->MibObject.i4FsofcControllerConnProtocol =
            pOfcSetFsofcControllerConnEntry->
            MibObject.i4FsofcControllerConnProtocol;
    }
    if (pOfcIsSetFsofcControllerConnEntry->bFsofcControllerRole != OSIX_FALSE)
    {
        pOfcFsofcControllerConnEntry->MibObject.i4FsofcControllerRole =
            pOfcSetFsofcControllerConnEntry->MibObject.i4FsofcControllerRole;
    }
    if (pOfcIsSetFsofcControllerConnEntry->bFsofcControllerConnEchoReqCount !=
        OSIX_FALSE)
    {
        pOfcFsofcControllerConnEntry->
            MibObject.i4FsofcControllerConnEchoReqCount =
            pOfcSetFsofcControllerConnEntry->
            MibObject.i4FsofcControllerConnEchoReqCount;
    }
    if (pOfcIsSetFsofcControllerConnEntry->bFsofcControllerConnEntryStatus !=
        OSIX_FALSE)
    {
        pOfcFsofcControllerConnEntry->
            MibObject.i4FsofcControllerConnEntryStatus =
            pOfcSetFsofcControllerConnEntry->
            MibObject.i4FsofcControllerConnEntryStatus;
    }
    if (pOfcIsSetFsofcControllerConnEntry->bFsofcControllerConnBand !=
        OSIX_FALSE)
    {
        pOfcFsofcControllerConnEntry->MibObject.i4FsofcControllerConnBand =
            pOfcSetFsofcControllerConnEntry->
            MibObject.i4FsofcControllerConnBand;
    }

    /*This condtion is to make the row back to ACTIVE after modifying the values */
    if (i4RowMakeActive == TRUE)
    {
        pOfcFsofcControllerConnEntry->
            MibObject.i4FsofcControllerConnEntryStatus = ACTIVE;
    }

    if (OfcUtilUpdateFsofcControllerConnTable (pOfcOldFsofcControllerConnEntry,
                                               pOfcFsofcControllerConnEntry,
                                               pOfcIsSetFsofcControllerConnEntry)
        != OSIX_SUCCESS)
    {

        if (OfcSetAllFsofcControllerConnTableTrigger
            (pOfcSetFsofcControllerConnEntry, pOfcIsSetFsofcControllerConnEntry,
             SNMP_FAILURE) != OSIX_SUCCESS)

        {
            OFC_TRC ((OFC_UTIL_TRC,
                      "OfcSetAllFsofcControllerConnTable: OfcSetAllFsofcControllerConnTableTrigger function returns failure.\r\n"));

        }
        OFC_TRC ((OFC_UTIL_TRC,
                  "OfcSetAllFsofcControllerConnTable: OfcUtilUpdateFsofcControllerConnTable function returns failure.\r\n"));
        /*Restore back with previous values */
        MEMCPY (pOfcFsofcControllerConnEntry, pOfcOldFsofcControllerConnEntry,
                sizeof (tOfcFsofcControllerConnEntry));
        MemReleaseMemBlock (OFC_FSOFCCONTROLLERCONNTABLE_POOLID,
                            (UINT1 *) pOfcOldFsofcControllerConnEntry);
        MemReleaseMemBlock (OFC_FSOFCCONTROLLERCONNTABLE_POOLID,
                            (UINT1 *) pOfcTrgFsofcControllerConnEntry);
        MemReleaseMemBlock (OFC_FSOFCCONTROLLERCONNTABLE_ISSET_POOLID,
                            (UINT1 *) pOfcTrgIsSetFsofcControllerConnEntry);
        return OSIX_FAILURE;

    }
    if (OfcSetAllFsofcControllerConnTableTrigger
        (pOfcSetFsofcControllerConnEntry, pOfcIsSetFsofcControllerConnEntry,
         SNMP_SUCCESS) != OSIX_SUCCESS)
    {

        OFC_TRC ((OFC_UTIL_TRC,
                  "OfcSetAllFsofcControllerConnTable: OfcSetAllFsofcControllerConnTableTrigger function returns failure.\r\n"));
    }

    MemReleaseMemBlock (OFC_FSOFCCONTROLLERCONNTABLE_POOLID,
                        (UINT1 *) pOfcOldFsofcControllerConnEntry);
    MemReleaseMemBlock (OFC_FSOFCCONTROLLERCONNTABLE_POOLID,
                        (UINT1 *) pOfcTrgFsofcControllerConnEntry);
    MemReleaseMemBlock (OFC_FSOFCCONTROLLERCONNTABLE_ISSET_POOLID,
                        (UINT1 *) pOfcTrgIsSetFsofcControllerConnEntry);
    return OSIX_SUCCESS;

}

/****************************************************************************
 Function    :  OfcSetAllFsofcIfTable
 Input       :  pOfcSetFsofcIfEntry
                pOfcIsSetFsofcIfEntry
 Description :  This Routine Take the Indices &
                Sets the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
OfcSetAllFsofcIfTable (tOfcFsofcIfEntry * pOfcSetFsofcIfEntry,
                       tOfcIsSetFsofcIfEntry * pOfcIsSetFsofcIfEntry)
{
    tOfcFsofcIfEntry   *pOfcFsofcIfEntry = NULL;
    tOfcFsofcIfEntry    OfcOldFsofcIfEntry;
    tOfcFsofcIfEntry    OfcGetFsofcIfEntry;

    MEMSET (&OfcOldFsofcIfEntry, 0, sizeof (tOfcFsofcIfEntry));
    MEMSET (&OfcGetFsofcIfEntry, 0, sizeof (tOfcFsofcIfEntry));

    /* Check whether the node is already present */
    if (MsrGetRestorationStatus () == ISS_TRUE)
    {
        /* Copy the previous values before setting the new values */
        MEMCPY (&OfcGetFsofcIfEntry, pOfcSetFsofcIfEntry,
                sizeof (tOfcFsofcIfEntry));
        OfcGetFsofcIfEntry.MibObject.u4FsofcContextId = OFC_DEFAULT_CONTEXT;
        pOfcFsofcIfEntry =
            RBTreeGet (gOfcGlobals.OfcGlbMib.FsofcIfTable,
                       (tRBElem *) & OfcGetFsofcIfEntry);
    }
    else
    {
        pOfcFsofcIfEntry =
            RBTreeGet (gOfcGlobals.OfcGlbMib.FsofcIfTable,
                       (tRBElem *) pOfcSetFsofcIfEntry);
    }

    if (pOfcFsofcIfEntry == NULL)
    {
        OFC_TRC ((OFC_UTIL_TRC,
                  "OfcSetAllFsofcIfTable: Entry doesn't exist\r\n"));
        return OSIX_FAILURE;
    }

    /*Function to check whether the given input is same as there in database */
    if (FsofcIfTableFilterInputs (pOfcFsofcIfEntry, pOfcSetFsofcIfEntry,
                                  pOfcIsSetFsofcIfEntry) != OSIX_TRUE)
    {
        return OSIX_SUCCESS;
    }

    /* Copy the previous values before setting the new values */
    MEMCPY (&OfcOldFsofcIfEntry, pOfcFsofcIfEntry, sizeof (tOfcFsofcIfEntry));

    /* Assign values for the existing node */
    if (pOfcIsSetFsofcIfEntry->bFsofcVlanEgressPorts != OSIX_FALSE)
    {
        MEMCPY (pOfcFsofcIfEntry->MibObject.au1FsofcVlanEgressPorts,
                pOfcSetFsofcIfEntry->MibObject.au1FsofcVlanEgressPorts,
                pOfcSetFsofcIfEntry->MibObject.i4FsofcVlanEgressPortsLen);

        pOfcFsofcIfEntry->MibObject.i4FsofcVlanEgressPortsLen =
            pOfcSetFsofcIfEntry->MibObject.i4FsofcVlanEgressPortsLen;
    }
    if (pOfcIsSetFsofcIfEntry->bFsofcVlanUntaggedPorts != OSIX_FALSE)
    {
        MEMCPY (pOfcFsofcIfEntry->MibObject.au1FsofcVlanUntaggedPorts,
                pOfcSetFsofcIfEntry->MibObject.au1FsofcVlanUntaggedPorts,
                pOfcSetFsofcIfEntry->MibObject.i4FsofcVlanUntaggedPortsLen);

        pOfcFsofcIfEntry->MibObject.i4FsofcVlanUntaggedPortsLen =
            pOfcSetFsofcIfEntry->MibObject.i4FsofcVlanUntaggedPortsLen;
    }
    if (pOfcIsSetFsofcIfEntry->bFsofcIfContextId != OSIX_FALSE)
    {
        pOfcFsofcIfEntry->MibObject.u4FsofcIfContextId =
            pOfcSetFsofcIfEntry->MibObject.u4FsofcIfContextId;
    }

    if (OfcUtilUpdateFsofcIfTable
        (&OfcOldFsofcIfEntry, pOfcFsofcIfEntry,
         pOfcIsSetFsofcIfEntry) != OSIX_SUCCESS)
    {
        if (OfcSetAllFsofcIfTableTrigger (pOfcSetFsofcIfEntry,
                                          pOfcIsSetFsofcIfEntry,
                                          SNMP_FAILURE) != OSIX_SUCCESS)

        {
            return OSIX_FAILURE;
        }
        /*Restore back with previous values */
        MEMCPY (pOfcFsofcIfEntry, &OfcOldFsofcIfEntry,
                sizeof (tOfcFsofcIfEntry));
        return OSIX_FAILURE;
    }

    if (OfcSetAllFsofcIfTableTrigger (pOfcSetFsofcIfEntry,
                                      pOfcIsSetFsofcIfEntry,
                                      SNMP_SUCCESS) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}
