/*****************************************************************************
 * Copyright (C) 2013 Aricent Inc . All Rights Reserved
 *
 * $Id: ofcnpapi.c,v 1.2 2014/08/14 11:13:34 siva Exp $
 *
 * Description: This file contains the OFC NPAPIs that will be used for
 *              programming the hardware chipsets.
 *****************************************************************************/

#include "ofcinc.h"
#include "nputil.h"

/***************************************************************************
 *
 *    Function Name       : OfcFsNpHwConfigOfcInfo
 *
 *    Description         : This function using its arguments populates the
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsNpHwConfigOfcInfo
 *
 *    Input(s)            : Arguments of FsNpHwConfigOfcInfo
 *
 *    Output(s)           : None
 *
 *    Global Var Referred : None
 *
 *    Global Var Modified : None.
 *
 *    Use of Recursion    : None.
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE
 *
 *****************************************************************************/

UINT1
OfcFsNpHwConfigOfcInfo (tOfcHwInfo * pOfcHwOfcInfo)
{
    tFsHwNp             FsHwNp;
    tOfcNpModInfo      *pOfcNpModInfo = NULL;
    tOfcNpWrFsNpHwConfigOfcInfo *pEntry = NULL;

    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_OFC_MOD,    /* Module ID */
                         FS_NP_HW_CONFIG_OFC_INFO,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pOfcNpModInfo = &(FsHwNp.OfcNpModInfo);
    pEntry = &pOfcNpModInfo->OfcNpFsNpHwConfigOfcInfo;

    pEntry->pOfcHwOfcInfo = pOfcHwOfcInfo;

#ifdef DPA_WANTED
    if (FNP_FAILURE == OfcNpWrHwProgram (&FsHwNp))
#else
    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
#endif
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}
