/*****************************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: ofcmain.c,v 1.10 2014/09/30 12:25:52 siva Exp $
 *
 * Description: This file contains the ofc task main loop
 *              and the initialization routines.
 *****************************************************************************/

#define __OFCMAIN_C__

#include "ofcinc.h"
#include "ofccli.h"
#include "ofcinput.h"
#include "ofcflow.h"
#include "ofcl.h"
#include "ofcsz.h"
#include "ofcidxmg.h"
#include "ofcapi.h"
#include "ofcacts.h"
#ifdef NPAPI_WANTED
#include "ofcnp.h"
#if defined (OPENFLOW_TCAM) && defined (DPA_WANTED)
#include "fssocket.h"
#include "ofc131dpa.h"
#endif /* OPENFLOW_TCAM && DPA_WANTED */
#endif /* NPAPI_WANTED */

tOfcFsofcCfgEntry   gOfc;
/*
 * gu4FlowType is by default supports exact and wildcard matching.
 * It can be also used to disable support of exact (only wildcard matching)
 */

UINT4               gu4FlowType;

/*****************************************************************************
 * Function    :  OfcMainTask 
 * Description :  Main Function for OFC
 * Input       :  None
 * Output      :  None 
 * Returns     :  None 
 *****************************************************************************/
VOID
OfcMainTask ()
{
    UINT4               u4Event = OFC_ZERO;

    OFC_TRC_FUNC ((OFC_FN_ENTRY, "FUNC:OfcTaskMain\n"));

    if (TmrCreateTimerList ((CONST UINT1 *) OFC_TASK_NAME, OFC_TIMER_EVENT,
                            NULL, (tTimerListId *) & (gOfcGlobals.ofcTmrLst))
        == TMR_FAILURE)
    {
        OFC_TRC ((OFC_MAIN_TRC, "OFC Timer List Creation Failed\n"));
        return;
    }

    while (OSIX_TRUE == OSIX_TRUE)
    {
        OsixReceiveEvent (OFC_TIMER_EVENT | OFC_QUEUE_EVENT |
                          OFC_DPA_FP_EVENT | OFC_DPA_PKT_EVENT,
                          OSIX_WAIT, OFC_ZERO, &u4Event);
        OfcMainTaskLock ();

        OFC_TRC ((OFC_MAIN_TRC, "Rx Event %d\n", u4Event));

        if ((u4Event & OFC_TIMER_EVENT) == OFC_TIMER_EVENT)
        {
            OFC_TRC ((OFC_MAIN_TRC, "OFC_TIMER_EVENT\n"));
            OfcTmrHandleExpiry ();
        }

        if ((u4Event & OFC_QUEUE_EVENT) == OFC_QUEUE_EVENT)
        {
            OFC_TRC ((OFC_MAIN_TRC, "OFC_QUEUE_EVENT\n"));
            OfcQueProcessMsgs ();
        }

#ifdef NPAPI_WANTED
#if defined (OPENFLOW_TCAM) && defined (DPA_WANTED)
        if ((u4Event & OFC_DPA_FP_EVENT) == OFC_DPA_FP_EVENT)
        {
            OFC_TRC ((OFC_MAIN_TRC, "DPA - Flow or Port event\n"));
            Ofc131DpaEventProcess ();
        }

        if ((u4Event & OFC_DPA_PKT_EVENT) == OFC_DPA_PKT_EVENT)
        {
            OFC_TRC ((OFC_MAIN_TRC, "DPA - Data Packet event\n"));
            Ofc131DpaPktProcess ();
        }
#endif /* OPENFLOW_TCAM && DPA_WANTED */
#endif /* NPAPI_WANTED */

        /* Mutual exclusion flag OFF */
        OfcMainTaskUnLock ();
    }
    OFC_TRC_FUNC ((OFC_FN_EXIT, "FUNC:OfcTaskMain\n"));
}

/*****************************************************************************
 * Function    :  OfcHashNodeDeleteFunc 
 * Description :  This function releases the memory for the given hashnode
 * Input       :  pHashNode - Pointer to the HashNode 
 * Output      :  None 
 * Returns     :  None 
 *****************************************************************************/
PRIVATE VOID
OfcHashNodeDeleteFunc (tTMO_HASH_NODE * pHashNode)
{
    UNUSED_PARAM (pHashNode);
}

/*****************************************************************************
 * Function    :  Ofc100MainMemInit 
 * Description :  This function allocates the memory(pools) required for v100
 * Input       :  tOfcFsofcCfgEntry *pOfcFsofcCfgEntry - Pointer to Context
 *                Entry
 * Output      :  None 
 * Returns     :  OFC_SUCCESS/OFC_FAILURE 
 *****************************************************************************/
PUBLIC UINT4
Ofc100MainMemInit (tOfcFsofcCfgEntry * pOfcFsofcCfgEntry)
{
    OFC_TRC_FUNC ((OFC_FN_ENTRY, "FUNC:Ofc100MainMemInit\n"));

    pOfcFsofcCfgEntry->MibObject.i4FsofcModuleStatus = OPENFLOW_NONE;
    pOfcFsofcCfgEntry->MibObject.i4FsofcDefaultFlowMissBehaviour =
        OPENFLOW_CONTROLLER;
    pOfcFsofcCfgEntry->MibObject.i4FsofcControlPktBuffering = OPENFLOW_DISABLE;
    pOfcFsofcCfgEntry->MibObject.i4FsofcIpReassembleStatus = OPENFLOW_DISABLE;
    pOfcFsofcCfgEntry->MibObject.i4FsofcPortStpStatus = OPENFLOW_DISABLE;
    pOfcFsofcCfgEntry->MibObject.i4FsofcSwitchModeOnConnFailure =
        OPENFLOW_FAIL_SECURE;
    pOfcFsofcCfgEntry->MibObject.u4FsofcTraceEnable = CLI_OFC_NO_DEBUG;
    pOfcFsofcCfgEntry->MibObject.i4FsofcHybrid = OPENFLOW_MINUS_ONE;
    pOfcFsofcCfgEntry->u1GenerationIsDefined = FALSE;
    pOfcFsofcCfgEntry->u4OfcClientState = OFC_NOT_CONNECTED;

    /* Create the HashTable for Flow table Records */
    pOfcFsofcCfgEntry->pFlowtable =
        TMO_HASH_Create_Table (OFC_MAX_FLOWS, NULL, TRUE);

    if (pOfcFsofcCfgEntry->pFlowtable == NULL)
    {
        OFC_TRC ((OFC_MAIN_TRC, "Hash Table for Flows Creation failed"
                  "for OFC Context:%d\n",
                  pOfcFsofcCfgEntry->MibObject.u4FsofcContextId));
        return OSIX_FAILURE;
    }
    OFC_TRC_FUNC ((OFC_FN_EXIT, "EXIT:Ofc100MainMemInit\n"));
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function    :  Ofc131MainMemInit 
 * Description :  This function allocates the memory(pools) required for v131
 * Input       :  tOfcFsofcCfgEntry *pOfcFsofcCfgEntry - Pointer to Context
 *                Entry
 * Output      :  None 
 * Returns     :  OSIX_SUCCESS/OSIX_FAILURE 
 *****************************************************************************/
PUBLIC UINT4
Ofc131MainMemInit (tOfcFsofcCfgEntry * pOfcFsofcCfgEntry)
{
    tOfcFsofcFlowTable *pFlowTable = NULL;
    UINT4               u4RBNodeOffset = OFC_ZERO;
    UINT1               u1Iter = OFC_ZERO;

    OFC_TRC_FUNC ((OFC_FN_ENTRY, "FUNC:Ofc131MainMemInit\n"));

    pOfcFsofcCfgEntry->MibObject.i4FsofcModuleStatus = OPENFLOW_NONE;
    pOfcFsofcCfgEntry->MibObject.i4FsofcDefaultFlowMissBehaviour =
        OPENFLOW_CONTROLLER;
    pOfcFsofcCfgEntry->MibObject.i4FsofcControlPktBuffering = OPENFLOW_DISABLE;
    pOfcFsofcCfgEntry->MibObject.i4FsofcIpReassembleStatus = OPENFLOW_DISABLE;
    pOfcFsofcCfgEntry->MibObject.i4FsofcPortStpStatus = OPENFLOW_DISABLE;
    pOfcFsofcCfgEntry->MibObject.i4FsofcSwitchModeOnConnFailure =
        OPENFLOW_FAIL_SECURE;
    pOfcFsofcCfgEntry->MibObject.i4FsofcHybrid = OPENFLOW_MINUS_ONE;
    pOfcFsofcCfgEntry->MibObject.u4FsofcTraceEnable = CLI_OFC_NO_DEBUG;
    pOfcFsofcCfgEntry->u1GenerationIsDefined = FALSE;
    pOfcFsofcCfgEntry->u2NumOfTables = OFC_USRMAX_TABLES;
    pOfcFsofcCfgEntry->u4OfcClientState = OFC_NOT_CONNECTED;

    if (pOfcFsofcCfgEntry->u2NumOfTables > MAX_OFC_FLOW_TABLES)
    {
        /* 
         * we don't have enough memory to support the user requested number of
         * tables. 
         */
        OFC_TRC ((OFC_MAIN_TRC, "Insufficient memory to create user requested"
                  " flow tables\n"));
        return OSIX_FAILURE;
    }

    /*
     * Create the RBTree for Flow Tables in the given Context
     */
    if (OfcFsofcFTableCreate (pOfcFsofcCfgEntry) == OSIX_FAILURE)
    {
        OFC_TRC ((OFC_MAIN_TRC,
                  "RBTree Creation for Flow Table Failed - V1.3.1\n"));
        return (OSIX_FAILURE);
    }

    /*
     * Only loop for the user given max tables, this cannot be altered by
     * controller.
     */
    for (u1Iter = OFC_ZERO; u1Iter < OFC_USRMAX_TABLES; u1Iter++)
    {
        pFlowTable =
            (tOfcFsofcFlowTable *) MemAllocMemBlk (OFC_FLOW_TABLE_POOLID);

        if (pFlowTable == NULL)
        {
            OFC_TRC_FUNC ((OFC_MAIN_TRC,
                           "MemAlloc for Flow Table Failed V1.3.1\n"));
            return OSIX_FAILURE;
        }

        MEMSET (pFlowTable, OFC_ZERO, sizeof (tOfcFsofcFlowTable));
        pFlowTable->u4ContextId = pOfcFsofcCfgEntry->MibObject.u4FsofcContextId;
        pFlowTable->u4TableIndex = u1Iter;

        /* Fill in the Instructions BitMap */
        pFlowTable->u1InstrsBitMap = (UINT1)
            au1FlowMaskInfo[u1Iter].u4Instructions;
        /* Fill in the Next Table BitMap */
        pFlowTable->u4NxtTblsBitMap = au1FlowMaskInfo[u1Iter].u4NextTables;
        /* Fill in the Write Actions BitMap */
        pFlowTable->u4WrtActsBitMap = au1FlowMaskInfo[u1Iter].u4WriteActions;
        /* Fill in the Apply Actions BitMap */
        pFlowTable->u4AppActsBitMap = au1FlowMaskInfo[u1Iter].u4ApplyActions;
        /* Fill in the Flow Match BitMap */
        pFlowTable->u8FlowMatchBitMap.u4Hi =
            (UINT4) (au1FlowMaskInfo[u1Iter].u8Match >> OFC_THIRTY_TWO);
        pFlowTable->u8FlowMatchBitMap.u4Lo =
            (UINT4) (au1FlowMaskInfo[u1Iter].u8Match);
        Ofc131ActsComputeNumBitsSet (pFlowTable->u8FlowMatchBitMap,
                                     &(pFlowTable->u1NumMatch));

        /* Fill in the WildCards BitMap */
        pFlowTable->u8WildCardsBitMap.u4Hi =
            (UINT4) (au1FlowMaskInfo[u1Iter].u8WildCards >> OFC_THIRTY_TWO);
        pFlowTable->u8WildCardsBitMap.u4Lo =
            (UINT4) (au1FlowMaskInfo[u1Iter].u8WildCards);
        /* Fill in the Write Actions - Set Fields BitMap */
        pFlowTable->u8WrtStFldsBitMap.u4Hi =
            (UINT4) (au1FlowMaskInfo[u1Iter].u8WriteSetFields >>
                     OFC_THIRTY_TWO);
        pFlowTable->u8WrtStFldsBitMap.u4Lo =
            (UINT4) (au1FlowMaskInfo[u1Iter].u8WriteSetFields);
        /* Fill in the Apply Actions - Set Fields BitMap */
        pFlowTable->u8AppStFldsBitMap.u4Hi =
            (UINT4) (au1FlowMaskInfo[u1Iter].u8ApplySetFields >>
                     OFC_THIRTY_TWO);
        pFlowTable->u8AppStFldsBitMap.u4Lo =
            (UINT4) (au1FlowMaskInfo[u1Iter].u8ApplySetFields);

        /* Create the HashTable for Flow table Records */
        pFlowTable->pFlowtable = TMO_HASH_Create_Table (OFC_MAX_FLOWS,
                                                        NULL, FALSE);
        if (pFlowTable->pFlowtable == NULL)
        {
            OFC_TRC ((OFC_MAIN_TRC, "Hash Table Creation failed for"
                      " OFC Flow  Table\n"));
            MemReleaseMemBlock (OFC_FLOW_TABLE_POOLID, (UINT1 *) pFlowTable);
            return OSIX_FAILURE;
        }

        /* Create RBTree for each Flow Table to hold exact flow entry lists */
        u4RBNodeOffset = FSAP_OFFSETOF (tRBExactFlowEntry, RBNode);
        if ((pFlowTable->ExactFlowEntry = RBTreeCreateEmbedded (u4RBNodeOffset,
                                                                FsOfcExactFlowEntryRBComp))
            == NULL)
        {
            OFC_TRC ((OFC_MAIN_TRC, "RBTree create failed for Exact Flow "
                      "Entry\n"));
            MemReleaseMemBlock (OFC_FLOW_TABLE_POOLID, (UINT1 *) pFlowTable);
            return OSIX_FAILURE;
        }

        if (RBTreeAdd (pOfcFsofcCfgEntry->OfcFlowtable, (tRBElem *) pFlowTable)
            != RB_SUCCESS)
        {
            TMO_HASH_Delete_Table (pFlowTable->pFlowtable,
                                   OfcHashNodeDeleteFunc);
            MemReleaseMemBlock (OFC_FLOW_TABLE_POOLID, (UINT1 *) pFlowTable);
            OFC_TRC ((OFC_MAIN_TRC, "RBTree Add Failed for"
                      "OFC Flow Table\n"));
            return OSIX_FAILURE;
        }
        KW_FALSEPOSITIVE_FIX (pFlowTable->pFlowtable);
        KW_FALSEPOSITIVE_FIX1 (pFlowTable);
    }
    OFC_TRC_FUNC ((OFC_FN_EXIT, "EXIT:Ofc131MainMemInit\n"));
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function    :  OfcMainMemInit 
 * Description :  This function allocates the memory(pools) required
 * Input       :  None
 * Output      :  None 
 * Returns     :  OFC_SUCCESS/OFC_FAILURE 
 *****************************************************************************/
PRIVATE UINT4
OfcMainMemInit (VOID)
{
    OFC_TRC_FUNC ((OFC_FN_ENTRY, "FUNC:OfcMainMemInit\n"));

    if (OfcSizingMemCreateMemPools () == OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    OFC_TRC_FUNC ((OFC_FN_EXIT, "EXIT:OfcMainMemInit\n"));
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function    :  OfcMainTaskInit 
 * Description :  Initialization routine for OFC
 * Input       :  None
 * Output      :  None 
 * Returns     :  OSIX_SUCCESS/OSIX_FAILURE 
 *****************************************************************************/
UINT4
OfcMainTaskInit (VOID)
{
    UINT4               u4Version = OFC_ZERO;
#ifdef NPAPI_WANTED
    tOfcHwInfo          OfcHwInfo;
    INT4                i4RetValue = FNP_FAILURE;
#endif /* NPAPI_WANTED */

    OFC_TRC_FUNC ((OFC_FN_ENTRY, "FUNC:OfcMainTaskInit\n"));

    MEMSET (&gOfcGlobals, OFC_ZERO, sizeof (gOfcGlobals));
    MEMCPY (gOfcGlobals.au1TaskSemName, OFC_MUT_EXCL_SEM_NAME, OSIX_NAME_LEN);

    if (OsixCreateSem (OFC_MUT_EXCL_SEM_NAME, OFC_SEM_CREATE_INIT_CNT, OFC_ZERO,
                       &gOfcGlobals.ofcTaskSemId) == OSIX_FAILURE)
    {
        OFC_TRC ((OFC_MAIN_TRC, "Seamphore Creation failure for %s \n",
                  OFC_MUT_EXCL_SEM_NAME));
        return OSIX_FAILURE;
    }

    if (OfcUtlCreateRBTree () == OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    /* Create buffer pools for data structures */
    if (OfcMainMemInit () == OSIX_FAILURE)
    {
        OFC_TRC ((OFC_MAIN_TRC, "Memory Pool Creation Failed\n"));
        return OSIX_FAILURE;
    }

    if (OsixCreateQ (OFC_QUEUE_NAME, OFC_QUEUE_DEPTH, (UINT4) OFC_ZERO,
                     (tOsixQId *) & (gOfcGlobals.ofcQueId)) == OSIX_FAILURE)
    {
        OFC_TRC ((OFC_MAIN_TRC, "IAPQ Creation Failed\n"));
        return OSIX_FAILURE;
    }

    if (OfcIndexMgrInitWithSem () == OFC_FAILURE)
    {
        return OSIX_FAILURE;
    }

    /*
     * Read the PARAM SIZE Values for Group and Meter Table.
     * If it is > 1 Set the version to V131,
     * else Set the Version to V100
     */
    if (FsOFCSizingParams[MAX_OFC_VERSION_SIZING_ID].u4PreAllocatedUnits >
        OFC_ONE)
    {
        u4Version = (UINT4) OFC_VERSION_V131;
    }
    else
    {
        u4Version = (UINT4) OFC_VERSION_V100;
    }

    /* check the flow type set */
    if (FsOFCSizingParams[MAX_OFC_FLOW_TYPE_SIZING_ID].u4PreAllocatedUnits >
        OFC_ONE)
    {
        gu4FlowType = (UINT4) OFC_FLOW_TYPE_WILD;
    }
    else
    {
        /* go with both exact and wild card matching */
    }

    gOfc.MibObject.u4FsofcContextId = OFC_DEFAULT_CONTEXT;
    /* Initialize the CFG Context */
    if (OfcMainCfgContextInit (&gOfc, u4Version) == OSIX_FAILURE)
    {
        OFC_TRC ((OFC_MAIN_TRC, "\r\nOfc Cfg Context Init Failure\n"));
        return OSIX_FAILURE;
    }

    /* 
     * The following routine initialises the timer descriptor structure 
     */
    OfcTmrInitTmrDesc ();

    if (gOfc.MibObject.i4FsofcSupportedVersion == OFC_VERSION_V100)
    {
        /* 
         * Create Hash Table and Buffer pools for data structures  - V1.0.0 
         */
        if (Ofc100MainMemInit (&gOfc) == OSIX_FAILURE)
        {
            OFC_TRC ((OFC_MAIN_TRC, "Memory Pool Creation Failed - V1.0.0\n"));
            return OSIX_FAILURE;
        }

        OFC_TRC ((OFC_MAIN_TRC,
                  "\r\nFUNC:OfcMainTaskInit: Calling NPAPI to Initialize HW Tables\r\n"));
#ifdef NPAPI_WANTED
        /* 
         * Calling HW function to initialise/reserve ACL Filters for OFCL
         */
        MEMSET (&OfcHwInfo, OFC_ZERO, sizeof (tOfcHwInfo));

        OfcHwInfo.OfcVer = OFC_VER_100;
        OfcHwInfo.OfcCmd = OFC_NP_INIT;
        OfcHwInfo.OfcHwEntry.OfcHwRsrvInfo.OfcRsrvParam = OFC_HW_ACL_FILTERS;
        OfcHwInfo.OfcHwEntry.OfcHwRsrvInfo.u4Entries = OFC_MAX_FLOWS;

        i4RetValue = OfcFsNpHwConfigOfcInfo (&OfcHwInfo);
        if (i4RetValue != FNP_SUCCESS)
        {
            OFC_TRC ((OFC_MAIN_TRC,
                      "FUNC:OfcMainTaskInit: NPAPI failed For HW Init - 100 ACL Filters\n"));
            return OSIX_FAILURE;
        }
#endif
    }
    else
    {
        /* 
         * Create Hash Table and Buffer pools for data structures  - V1.3.1 
         */
        if (Ofc131MainMemInit (&gOfc) == OSIX_FAILURE)
        {
            OFC_TRC ((OFC_MAIN_TRC, "Memory Pool Creation Failed - V1.3.1\n"));
            return OSIX_FAILURE;
        }

        OFC_TRC ((OFC_MAIN_TRC,
                  "\r\nFUNC:OfcMainTaskInit: Calling NPAPI to Initialize HW Tables\r\n"));
#ifdef NPAPI_WANTED
        /* 
         * Calling HW function to initialise/reserve ACL Filters for OFCL
         */
        MEMSET (&OfcHwInfo, OFC_ZERO, sizeof (tOfcHwInfo));

        OfcHwInfo.OfcVer = OFC_VER_131;
        OfcHwInfo.OfcCmd = OFC_NP_INIT;
        OfcHwInfo.OfcHwEntry.OfcHwRsrvInfo.OfcRsrvParam = OFC_HW_ACL_FILTERS;
        OfcHwInfo.OfcHwEntry.OfcHwRsrvInfo.u4Entries = OFC_MAX_FLOWS;

        i4RetValue = OfcFsNpHwConfigOfcInfo (&OfcHwInfo);
        if (i4RetValue != FNP_SUCCESS)
        {
            OFC_TRC ((OFC_MAIN_TRC,
                      "FUNC:OfcMainTaskInit: NPAPI failed For HW Init - 131 ACL Filters\n"));
            return OSIX_FAILURE;
        }

        /* 
         * Calling HW function to initialise/reserve the OFCL HW Meter Entries
         */
        MEMSET (&OfcHwInfo, OFC_ZERO, sizeof (tOfcHwInfo));

        OfcHwInfo.OfcVer = OFC_VER_131;
        OfcHwInfo.OfcCmd = OFC_NP_INIT;
        OfcHwInfo.OfcHwEntry.OfcHwRsrvInfo.OfcRsrvParam = OFC_HW_METER_ENTRIES;
        OfcHwInfo.OfcHwEntry.OfcHwRsrvInfo.u4Entries = OFC_MAX_METERS;

        i4RetValue = OfcFsNpHwConfigOfcInfo (&OfcHwInfo);
        if (i4RetValue != FNP_SUCCESS)
        {
            OFC_TRC ((OFC_MAIN_TRC,
                      "FUNC:OfcMainTaskInit: NPAPI failed For HW Init - 131 Meters\n"));
            return OSIX_FAILURE;
        }

#ifdef DPA_WANTED
        if (ofdpaClientInitialize ("Aricent-OFCL") != OFDPA_E_NONE)
        {
            return OSIX_FAILURE;
        }

        /* Add the Event and Packet Sockets */
        SelAddFd (ofdpaClientEventSockFdGet (), Ofc131DpaEventSockProcess);
        SelAddFd (ofdpaClientPktSockFdGet (), Ofc131DpaPktSockProcess);
#endif /* DPA_WANTED */
#endif /* NPAPI_WANTED */
    }

    /*
     * Initialize the SSL related environmental varaibles for OFCL
     */
#ifdef SSL_WANTED
    SslArLibInit ();
#endif

    OFC_TRC_FUNC ((OFC_FN_EXIT, "FUNC:OfcMainTaskInit\n"));
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function    :  Ofc100MainMemClear 
 * Description :  Clears all the memory(pools) alllocated for v100
 * Input       :  None
 * Output      :  None 
 * Returns     :  None 
 *****************************************************************************/
PUBLIC VOID
Ofc100MainMemClear (tOfcFsofcCfgEntry * pOfcFsofcCfgEntry)
{
    OFC_TRC_FUNC ((OFC_FN_ENTRY, "FUNC:Ofc100MainMemClear\n"));

    if (pOfcFsofcCfgEntry->pFlowtable != NULL)
    {
        TMO_HASH_Delete_Table (pOfcFsofcCfgEntry->pFlowtable,
                               OfcHashNodeDeleteFunc);
    }
    pOfcFsofcCfgEntry->pFlowtable = NULL;
    MemReleaseMemBlock (OFC_BUFFER_POOLID,
                        (UINT1 *) (pOfcFsofcCfgEntry->pDataBuf));
    OFC_TRC_FUNC ((OFC_FN_EXIT, "EXIT:Ofc100MainMemClear\n"));
}

/*****************************************************************************
 * Function    :  Ofc131MainMemClear 
 * Description :  Clears all the memory(pools) alllocated for v131
 * Input       :  None
 * Output      :  None 
 * Returns     :  None 
 *****************************************************************************/
PUBLIC VOID
Ofc131MainMemClear (tOfcFsofcCfgEntry * pOfcFsofcCfgEntry)
{
    tOfcFsofcFlowTable *pFlowTable = NULL;
    UINT1               u1Iter = OFC_ZERO;

    OFC_TRC_FUNC ((OFC_FN_ENTRY, "FUNC:Ofc131MainMemClear\n"));

    for (u1Iter = OFC_ZERO; u1Iter < OFC_USRMAX_TABLES; u1Iter++)
    {
        /*
         * Get the Flow Table Entry from the Context ID and Table Index
         * from the Given FlowEntry pKey.
         * If no Table Entry Found, lookup will return NULL
         */
        pFlowTable = (tOfcFsofcFlowTable *)
            OfcGetFlowTableEntry (pOfcFsofcCfgEntry->MibObject.u4FsofcContextId,
                                  u1Iter);

        if ((pFlowTable != NULL))
        {
            if ((pFlowTable->pFlowtable != NULL))
            {
                TMO_HASH_Delete_Table (pFlowTable->pFlowtable,
                                       OfcHashNodeDeleteFunc);
                pFlowTable->pFlowtable = NULL;
            }

            RBTreeRem (pOfcFsofcCfgEntry->OfcFlowtable, (tRBElem *) pFlowTable);
            MemReleaseMemBlock (OFC_FLOW_TABLE_POOLID, (UINT1 *) pFlowTable);
            pFlowTable = NULL;
        }
    }
    MemReleaseMemBlock (OFC_BUFFER_POOLID,
                        (UINT1 *) (pOfcFsofcCfgEntry->pDataBuf));
    OFC_TRC_FUNC ((OFC_FN_EXIT, "EXIT:Ofc131MainMemClear\n"));
}

/*****************************************************************************
 * Function    :  OfcMainMemClear 
 * Description :  Clears all the memory(pools) alllocated
 * Input       :  None
 * Output      :  None 
 * Returns     :  None 
 *****************************************************************************/
PRIVATE VOID
OfcMainMemClear (VOID)
{
    OFC_TRC_FUNC ((OFC_FN_ENTRY, "FUNC:OfcMainMemClear\n"));

    if (gOfc.MibObject.i4FsofcSupportedVersion == OFC_VERSION_V100)
    {
        Ofc100MainMemClear (&gOfc);
    }
    else
    {
        Ofc131MainMemClear (&gOfc);
    }
    OfcSizingMemDeleteMemPools ();
    OFC_TRC_FUNC ((OFC_FN_EXIT, "EXIT:OfcMainMemClear\n"));
}

/*****************************************************************************
 * Function    :  OfcMainDeInit 
 * Description :  Clean-up routine to free resources allocated during
 *                initialization 
 * Input       :  None
 * Output      :  None 
 * Returns     :  None 
 *****************************************************************************/
PUBLIC VOID
OfcMainDeInit (VOID)
{
    OFC_TRC_FUNC ((OFC_FN_ENTRY, "FUNC:OfcMainDeInit\n"));

    OfcMainMemClear ();

    if (gOfcGlobals.ofcTaskSemId)
    {
        OsixSemDel (gOfcGlobals.ofcTaskSemId);
    }

    if (gOfcGlobals.ofcQueId)
    {
        OsixQueDel (gOfcGlobals.ofcQueId);
    }

    if (gOfcGlobals.ofcTmrLst)
    {
        TmrDeleteTimerList (gOfcGlobals.ofcTmrLst);
    }
    OFC_TRC_FUNC ((OFC_FN_EXIT, "FUNC:OfcMainDeInit\n"));
}

/*****************************************************************************
 * Function    :  OfcMainTaskLock 
 * Description :  OFC Main Task Lock
 * Input       :  None
 * Output      :  None 
 * Returns     :  SNMP_SUCCESS/SNMP_FAILURE 
 *****************************************************************************/
PUBLIC INT4
OfcMainTaskLock (VOID)
{
    OFC_TRC_FUNC ((OFC_FN_ENTRY, "FUNC:OfcMainTaskLock\n"));

    if (OsixSemTake (gOfcGlobals.ofcTaskSemId) == OSIX_FAILURE)
    {
        OFC_TRC ((OFC_MAIN_TRC, "TakeSem failure for %s \n",
                  OFC_MUT_EXCL_SEM_NAME));
        return SNMP_FAILURE;
    }
    OFC_TRC_FUNC ((OFC_FN_EXIT, "EXIT:OfcMainTaskLock\n"));
    return SNMP_SUCCESS;
}

/*****************************************************************************
 * Function    :  OfcMainTaskUnLock 
 * Description :  OFC Main Task UnLock
 * Input       :  None
 * Output      :  None 
 * Returns     :  SNMP_SUCCESS/SNMP_FAILURE 
 *****************************************************************************/
PUBLIC INT4
OfcMainTaskUnLock (VOID)
{
    OFC_TRC_FUNC ((OFC_FN_ENTRY, "FUNC:OfcMainTaskUnLock\n"));

    OsixSemGive (gOfcGlobals.ofcTaskSemId);
    OFC_TRC_FUNC ((OFC_FN_EXIT, "EXIT:OfcMainTaskUnLock\n"));
    return SNMP_SUCCESS;
}

/*****************************************************************************
 * Function    :  OfcMainCfgContextInit
 * Description :  OFC Main Cfg Context Initialization
 * Input       :  tOfcFsofcCfgEntry *pOfcFsofcCfgEntry
 *                UINT4 u4Version
 * Output      :  None
 * Returns     :  OSIX_SUCCESS/OSIX_FAILURE
 ****************************************************************************/
PUBLIC INT4
OfcMainCfgContextInit (tOfcFsofcCfgEntry * pOfcFsofcCfgEntry, UINT4 u4Version)
{
    OFC_TRC_FUNC ((OFC_FN_ENTRY, "\r\nFUNC:OfcMainCfgContextInit\n"));

    pOfcFsofcCfgEntry->pDataBuf = (UINT1 *) MemAllocMemBlk (OFC_BUFFER_POOLID);
    if (NULL == pOfcFsofcCfgEntry->pDataBuf)
    {
        return OSIX_FAILURE;
    }
    MEMSET (pOfcFsofcCfgEntry->pDataBuf, OFC_ZERO, OFC_MAX_DATA_FRAME_SIZE);

    /*
     * Read the u4Version Values
     * If it is > 1 Set the version to V131,
     * else Set the Version to V100
     */
    if (u4Version > OFC_ONE)
    {
        pOfcFsofcCfgEntry->MibObject.i4FsofcSupportedVersion =
            (INT4) OFC_VERSION_V131;
    }
    else
    {
        pOfcFsofcCfgEntry->MibObject.i4FsofcSupportedVersion =
            (INT4) OFC_VERSION_V100;
    }

    pOfcFsofcCfgEntry->MibObject.i4FsofcSwitchEntryStatus = ACTIVE;
    pOfcFsofcCfgEntry->u2NumOfTables = OFC_USRMAX_TABLES;

    /* 
     * Add the Given Context Entry to RBTree. From OfcMainInitTask This will be
     * invoked for Default Context. This RBTree ADD is not required if this 
     * function is invoked for CFG entries otehr than default, as it will be taken
     * CLI/SNMP itself.
     */

    if (pOfcFsofcCfgEntry->MibObject.u4FsofcContextId == OFC_DEFAULT_CONTEXT)
    {
        if (RBTreeAdd (gOfcGlobals.OfcGlbMib.FsofcCfgTable,
                       (tRBElem *) pOfcFsofcCfgEntry) != RB_SUCCESS)
        {
            OFC_TRC_FUNC ((OFC_MAIN_TRC,
                           "RBTree Add failed For Cfg Context:%d\n",
                           pOfcFsofcCfgEntry->MibObject.u4FsofcContextId));
            return OSIX_FAILURE;
        }
    }

    OFC_TRC_FUNC ((OFC_FN_EXIT, "\r\nEXIT:OfcMainCfgContextInit\n"));
    return OSIX_SUCCESS;
}
