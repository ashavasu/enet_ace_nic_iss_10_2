/**********************************************************************************************
 * * Copyright (C) 2013 Aricent Inc . All Rights Reserved
 * *
 * $Id: ofcapi.c,v 1.8 2017/09/11 13:33:51 siva Exp $
 * *
 * * Description: This file contains the routines for conversion of instruction to actionstring
 * *********************************************************************************************/

#include "ofcinc.h"
#include "ofcinstr.h"
#include "ofcapi.h"
#include "utilcli.h"
#include "ofcflow.h"
#include "ofcmeter.h"

/*****************************************************************************
 * Function    :  Ofcinputhton64 
 * Description :  This routine converts the 64bit number into its 
 *                Network Byte Order
 * Input       :  pInput      - Pointer to 64bit number, to be converted
 * Output      :  None 
 * Returns     :  u8NwByOrNum - Value in Network Byte Order 
 *****************************************************************************/
PUBLIC              AR_UINT8
Ofcinputhton64 (AR_UINT8 * pInput)
{
    AR_UINT8            u8NwByOrNum;
    UINT4               u4EndianCheck = OFC_ONE;
    UINT1              *pdata = NULL;
    OFC_TRC_FUNC ((OFC_FN_ENTRY, "FUNC:Ofcinputhton64\n"));

    if (*(CHR1 *) (&u4EndianCheck) == OFC_ONE)
    {
        /* This is little endian, lsb first */
        pdata = (UINT1 *) &u8NwByOrNum;
        pdata[OFC_ZERO] = (UINT1) (*pInput >> OFC_FIFTY_SIX);
        pdata[OFC_ONE] = (UINT1) (*pInput >> OFC_FORTY_EIGHT);
        pdata[OFC_TWO] = (UINT1) (*pInput >> OFC_FORTY);
        pdata[OFC_THREE] = (UINT1) (*pInput >> OFC_THIRTY_TWO);
        pdata[OFC_FOUR] = (UINT1) (*pInput >> OFC_TWENTY_FOUR);
        pdata[OFC_FIVE] = (UINT1) (*pInput >> OFC_SIXTEEN);
        pdata[OFC_SIX] = (UINT1) (*pInput >> OFC_EIGHT);
        pdata[OFC_SEVEN] = (UINT1) (*pInput >> OFC_ZERO);
    }
    else
    {
        /* This is big endian, msb first */
        u8NwByOrNum = *pInput;
    }
    OFC_TRC_FUNC ((OFC_FN_EXIT, "FUNC:Ofcinputhton64\n"));

    return u8NwByOrNum;
}

/*****************************************************************************
 * Function    :  CliIPV6ToStr 
 * Description :  This routine converts the IPv6 string to the dotted format
 * Input       :  pIpv6Addr - Pointer to IPv6 string
 * Output      :  pu1Temp   - Pointer to the dotted format 
 * Returns     :  None 
 *****************************************************************************/
PRIVATE VOID
CliIPV6ToStr (UINT1 *pIpv6Addr, UINT1 *pu1Temp)
{
    UINT1               u1MacLen;
    UINT1               au1MacAddr[OFC_SIXTEEN];

    OFC_TRC_FUNC ((OFC_FN_ENTRY, "FUNC:CliIPV6ToStr\n"));

    if (!(pIpv6Addr) || !(pu1Temp))
    {
        return;
    }

    MEMCPY (au1MacAddr, pIpv6Addr, OFC_SIXTEEN);
    for (u1MacLen = OFC_ZERO; u1MacLen < OFC_SIXTEEN; u1MacLen++)
    {
        if (au1MacAddr[u1MacLen] < OFC_SIXTEEN)
        {
            SPRINTF ((CHR1 *) pu1Temp, "0%x", au1MacAddr[u1MacLen]);
        }
        else
        {
            SPRINTF ((CHR1 *) pu1Temp, "%x", au1MacAddr[u1MacLen]);
        }
        pu1Temp += OFC_TWO;

        if (u1MacLen != (OFC_FIFTEEN))
        {
            STRCPY (pu1Temp, ".");
            pu1Temp++;
        }
    }
    OFC_TRC_FUNC ((OFC_FN_EXIT, "FUNC:CliIPV6ToStr\n"));
}

/*****************************************************************************
 * Function    :  Ofc131ActionsSetFieldStructTostr 
 * Description :  This routine parses the SetFieldActions into string
 * Input       :  pSetFldList - Pointer to the SetFields SLL
 * Output      :  pStr        - Pointer to the Actions Display String 
 * Returns     :  OFC_SUCCESS/OFC_FAILURE
 *****************************************************************************/
PRIVATE INT4
Ofc131ActionsSetFieldStructTostr (char *pStr, tOfcSll * pSetFldList)
{
    tOfcSetFld         *pSetFld = NULL;
    tOxmFld            *pOxmFld = NULL;
    tTMO_SLL_NODE      *pNext = NULL;
    UINT4               u4TempVar = OFC_ZERO;
    UINT1               u1TempVar = OFC_ZERO;
    UINT2               u2TempVar = OFC_ZERO;
    UINT4               u4StrLen = OFC_ACTION_STRING_MAX_LEN;
    AR_UINT8            u8TempVar = OFC_ZERO;
    CHR1                au1TempBuff[OFC_FIVE_TWELVE];

    OFC_TRC_FUNC ((OFC_FN_ENTRY, "FUNC:Ofc131ActionsSetFieldStructTostr\n"));

    TMO_SLL_Scan (pSetFldList, pSetFld, tOfcSetFld *)
    {
        pOxmFld = &pSetFld->SetFld;
        if (STRLEN (pStr) >= OFC_ACTION_STRING_MAX_LEN)
        {
            break;
        }

        switch ((pSetFld->u2Type))
        {
            case OFCXMT_OFB_ETH_DST:
                SNPRINTF (pStr + STRLEN (pStr), u4StrLen - STRLEN (pStr),
                          "%s", "EtDs=");
                MEMSET (au1TempBuff, OFC_ZERO, sizeof (au1TempBuff));
                CliMacToStr (pOxmFld->au1Fld, (UINT1 *) au1TempBuff);

                if (OFC_THREE < STRLEN (au1TempBuff))
                {
                    au1TempBuff[STRLEN (au1TempBuff) - OFC_THREE] = '\0';
                }

                if (OFC_ZERO < STRLEN (au1TempBuff))
                {
                    SNPRINTF (pStr + STRLEN (pStr), u4StrLen - STRLEN (pStr),
                              "%s", au1TempBuff);
                }
                break;

            case OFCXMT_OFB_ETH_SRC:
                SNPRINTF (pStr + STRLEN (pStr), u4StrLen - STRLEN (pStr),
                          "%s", "Etsr=");
                MEMSET (au1TempBuff, OFC_ZERO, sizeof (au1TempBuff));
                CliMacToStr (pOxmFld->au1Fld, (UINT1 *) au1TempBuff);

                if (OFC_THREE < STRLEN (au1TempBuff))
                {
                    au1TempBuff[STRLEN (au1TempBuff) - OFC_THREE] = '\0';
                }

                if (OFC_ZERO < STRLEN (au1TempBuff))
                {
                    SNPRINTF (pStr + STRLEN (pStr), u4StrLen - STRLEN (pStr),
                              "%s", au1TempBuff);
                }
                break;

            case OFCXMT_OFB_ETH_TYPE:
                SNPRINTF (pStr + STRLEN (pStr), u4StrLen - STRLEN (pStr),
                          "%s", "EtTy=");
                MEMSET (au1TempBuff, OFC_ZERO, sizeof (au1TempBuff));
                u2TempVar = OFC_ZERO;
                MEMCPY (&u2TempVar, pOxmFld->au1Fld, OFC_TWO);
                u2TempVar = OSIX_NTOHS (u2TempVar);
                SNPRINTF (au1TempBuff, sizeof (au1TempBuff), "%u", u2TempVar);
                SNPRINTF (pStr + STRLEN (pStr), u4StrLen - STRLEN (pStr),
                          "%s", au1TempBuff);
                break;

            case OFCXMT_OFB_VLAN_VID:
                SNPRINTF (pStr + STRLEN (pStr), u4StrLen - STRLEN (pStr),
                          "%s", "Vlanid=");
                MEMSET (au1TempBuff, OFC_ZERO, sizeof (au1TempBuff));
                u2TempVar = OFC_ZERO;
                MEMCPY (&u2TempVar, pOxmFld->au1Fld, OFC_TWO);
                u2TempVar = OSIX_NTOHS (u2TempVar);
                u2TempVar &= 0xFFF;
                SNPRINTF (au1TempBuff, sizeof (au1TempBuff), "%u", u2TempVar);
                SNPRINTF (pStr + STRLEN (pStr), u4StrLen - STRLEN (pStr),
                          "%s", au1TempBuff);
                break;

            case OFCXMT_OFB_VLAN_PCP:
                SNPRINTF (pStr + STRLEN (pStr), u4StrLen - STRLEN (pStr),
                          "%s", "VlanPcp=");
                MEMSET (au1TempBuff, OFC_ZERO, sizeof (au1TempBuff));
                u1TempVar = OFC_ZERO;
                MEMCPY (&u1TempVar, pOxmFld->au1Fld, OFC_ONE);
                SNPRINTF (au1TempBuff, sizeof (au1TempBuff), "%u", u1TempVar);
                SNPRINTF (pStr + STRLEN (pStr), u4StrLen - STRLEN (pStr),
                          "%s", au1TempBuff);
                break;

            case OFCXMT_OFB_IP_DSCP:
                SNPRINTF (pStr + STRLEN (pStr), u4StrLen - STRLEN (pStr),
                          "%s", "IpDscp=");
                MEMSET (au1TempBuff, OFC_ZERO, sizeof (au1TempBuff));
                u1TempVar = OFC_ZERO;
                MEMCPY (&u1TempVar, pOxmFld->au1Fld, OFC_ONE);
                SNPRINTF (au1TempBuff, sizeof (au1TempBuff), "%u", u1TempVar);
                SNPRINTF (pStr + STRLEN (pStr), u4StrLen - STRLEN (pStr),
                          "%s", au1TempBuff);
                break;

            case OFCXMT_OFB_IP_ECN:
                SNPRINTF (pStr + STRLEN (pStr), u4StrLen - STRLEN (pStr),
                          "%s", "IpECN=");
                MEMSET (au1TempBuff, OFC_ZERO, sizeof (au1TempBuff));
                u1TempVar = OFC_ZERO;
                MEMCPY (&u1TempVar, pOxmFld->au1Fld, OFC_ONE);
                SNPRINTF (au1TempBuff, sizeof (au1TempBuff), "%u", u1TempVar);
                SNPRINTF (pStr + STRLEN (pStr), u4StrLen - STRLEN (pStr),
                          "%s", au1TempBuff);
                break;

            case OFCXMT_OFB_IP_PROTO:
                SNPRINTF (pStr + STRLEN (pStr), u4StrLen - STRLEN (pStr),
                          "%s", "IpPrt=");
                MEMSET (au1TempBuff, OFC_ZERO, sizeof (au1TempBuff));
                u1TempVar = OFC_ZERO;
                MEMCPY (&u1TempVar, pOxmFld->au1Fld, OFC_ONE);
                SNPRINTF (au1TempBuff, sizeof (au1TempBuff), "%u", u1TempVar);
                SNPRINTF (pStr + STRLEN (pStr), u4StrLen - STRLEN (pStr),
                          "%s", au1TempBuff);
                break;

            case OFCXMT_OFB_IPV4_SRC:
                SNPRINTF (pStr + STRLEN (pStr), u4StrLen - STRLEN (pStr),
                          "%s", "Ip4Sr=");
                MEMSET (au1TempBuff, OFC_ZERO, sizeof (au1TempBuff));
                CliStrToIp4Addr (pOxmFld->au1Fld, (UINT1 *) au1TempBuff);

                if (OFC_ZERO < STRLEN (au1TempBuff))
                {
                    SNPRINTF (pStr + STRLEN (pStr), u4StrLen - STRLEN (pStr),
                              "%s", au1TempBuff);
                }
                break;

            case OFCXMT_OFB_IPV4_DST:
                SNPRINTF (pStr + STRLEN (pStr), u4StrLen - STRLEN (pStr),
                          "%s", "Ip4Ds=");
                MEMSET (au1TempBuff, OFC_ZERO, sizeof (au1TempBuff));
                CliStrToIp4Addr (pOxmFld->au1Fld, (UINT1 *) au1TempBuff);

                if (OFC_ZERO < STRLEN (au1TempBuff))
                {
                    SNPRINTF (pStr + STRLEN (pStr), u4StrLen - STRLEN (pStr),
                              "%s", au1TempBuff);
                }
                break;

            case OFCXMT_OFB_TCP_SRC:
                SNPRINTF (pStr + STRLEN (pStr), u4StrLen - STRLEN (pStr),
                          "%s", "TcpSr=");
                MEMSET (au1TempBuff, OFC_ZERO, sizeof (au1TempBuff));
                u2TempVar = OFC_ZERO;
                MEMCPY (&u2TempVar, pOxmFld->au1Fld, OFC_TWO);
                u2TempVar = OSIX_NTOHS (u2TempVar);
                SNPRINTF (au1TempBuff, sizeof (au1TempBuff), "%u", u2TempVar);
                SNPRINTF (pStr + STRLEN (pStr), u4StrLen - STRLEN (pStr),
                          "%s", au1TempBuff);
                break;

            case OFCXMT_OFB_TCP_DST:
                SNPRINTF (pStr + STRLEN (pStr), u4StrLen - STRLEN (pStr),
                          "%s", "TcpDt=");
                MEMSET (au1TempBuff, OFC_ZERO, sizeof (au1TempBuff));
                u2TempVar = OFC_ZERO;
                MEMCPY (&u2TempVar, pOxmFld->au1Fld, OFC_TWO);
                u2TempVar = OSIX_NTOHS (u2TempVar);
                SNPRINTF (au1TempBuff, sizeof (au1TempBuff), "%u", u2TempVar);
                SNPRINTF (pStr + STRLEN (pStr), u4StrLen - STRLEN (pStr),
                          "%s", au1TempBuff);
                break;

            case OFCXMT_OFB_UDP_SRC:
                SNPRINTF (pStr + STRLEN (pStr), u4StrLen - STRLEN (pStr),
                          "%s", "UdpSr=");
                MEMSET (au1TempBuff, OFC_ZERO, sizeof (au1TempBuff));
                u2TempVar = OFC_ZERO;
                MEMCPY (&u2TempVar, pOxmFld->au1Fld, OFC_TWO);
                u2TempVar = OSIX_NTOHS (u2TempVar);
                SNPRINTF (au1TempBuff, sizeof (au1TempBuff), "%u", u2TempVar);
                SNPRINTF (pStr + STRLEN (pStr), u4StrLen - STRLEN (pStr),
                          "%s", au1TempBuff);
                break;

            case OFCXMT_OFB_UDP_DST:
                SNPRINTF (pStr + STRLEN (pStr), u4StrLen - STRLEN (pStr),
                          "%s", "UdpDt=");
                MEMSET (au1TempBuff, OFC_ZERO, sizeof (au1TempBuff));
                u2TempVar = OFC_ZERO;
                MEMCPY (&u2TempVar, pOxmFld->au1Fld, OFC_TWO);
                u2TempVar = OSIX_NTOHS (u2TempVar);
                SNPRINTF (au1TempBuff, sizeof (au1TempBuff), "%u", u2TempVar);
                SNPRINTF (pStr + STRLEN (pStr), u4StrLen - STRLEN (pStr),
                          "%s", au1TempBuff);
                break;

            case OFCXMT_OFB_SCTP_SRC:
                SNPRINTF (pStr + STRLEN (pStr), u4StrLen - STRLEN (pStr),
                          "%s", "SctpSr=");
                MEMSET (au1TempBuff, OFC_ZERO, sizeof (au1TempBuff));
                u2TempVar = OFC_ZERO;
                MEMCPY (&u2TempVar, pOxmFld->au1Fld, OFC_TWO);
                u2TempVar = OSIX_NTOHS (u2TempVar);
                SNPRINTF (au1TempBuff, sizeof (au1TempBuff), "%u", u2TempVar);
                SNPRINTF (pStr + STRLEN (pStr), u4StrLen - STRLEN (pStr),
                          "%s", au1TempBuff);
                break;

            case OFCXMT_OFB_SCTP_DST:
                SNPRINTF (pStr + STRLEN (pStr), u4StrLen - STRLEN (pStr),
                          "%s", "SctpDt=");
                MEMSET (au1TempBuff, OFC_ZERO, sizeof (au1TempBuff));
                u2TempVar = OFC_TWO;
                MEMCPY (&u2TempVar, pOxmFld->au1Fld, OFC_TWO);
                u2TempVar = OSIX_NTOHS (u2TempVar);
                SNPRINTF (au1TempBuff, sizeof (au1TempBuff), "%u", u2TempVar);
                SNPRINTF (pStr + STRLEN (pStr), u4StrLen - STRLEN (pStr),
                          "%s", au1TempBuff);
                break;

            case OFCXMT_OFB_ICMPV4_TYPE:
                SNPRINTF (pStr + STRLEN (pStr), u4StrLen - STRLEN (pStr),
                          "%s", "Icmp4typ=");
                MEMSET (au1TempBuff, OFC_ZERO, sizeof (au1TempBuff));
                MEMCPY (&u1TempVar, pOxmFld->au1Fld, OFC_ONE);
                SNPRINTF (au1TempBuff, sizeof (au1TempBuff), "%u", u2TempVar);
                SNPRINTF (pStr + STRLEN (pStr), u4StrLen - STRLEN (pStr),
                          "%s", au1TempBuff);
                break;

            case OFCXMT_OFB_ICMPV4_CODE:
                SNPRINTF (pStr + STRLEN (pStr), u4StrLen - STRLEN (pStr),
                          "%s", "Icmp4Cde=");
                MEMSET (au1TempBuff, OFC_ZERO, sizeof (au1TempBuff));
                u1TempVar = OFC_ZERO;
                MEMCPY (&u1TempVar, pOxmFld->au1Fld, OFC_ONE);
                SNPRINTF (au1TempBuff, sizeof (au1TempBuff), "%u", u1TempVar);
                SNPRINTF (pStr + STRLEN (pStr), u4StrLen - STRLEN (pStr),
                          "%s", au1TempBuff);
                break;

            case OFCXMT_OFB_ARP_OP:
                SNPRINTF (pStr + STRLEN (pStr), u4StrLen - STRLEN (pStr),
                          "%s", "ArpOP=");
                MEMSET (au1TempBuff, OFC_ZERO, sizeof (au1TempBuff));
                u2TempVar = OFC_ZERO;
                MEMCPY (&u2TempVar, pOxmFld->au1Fld, OFC_TWO);
                u2TempVar = OSIX_NTOHS (u2TempVar);
                SNPRINTF (au1TempBuff, sizeof (au1TempBuff), "%u", u2TempVar);
                SNPRINTF (pStr + STRLEN (pStr), u4StrLen - STRLEN (pStr),
                          "%s", au1TempBuff);
                break;

            case OFCXMT_OFB_ARP_SPA:
                SNPRINTF (pStr + STRLEN (pStr), u4StrLen - STRLEN (pStr),
                          "%s", "ArpSp=");
                MEMSET (au1TempBuff, OFC_ZERO, sizeof (au1TempBuff));
                CliStrToIp4Addr (pOxmFld->au1Fld, (UINT1 *) au1TempBuff);
                SNPRINTF (pStr + STRLEN (pStr), u4StrLen - STRLEN (pStr),
                          "%s", au1TempBuff);
                break;

            case OFCXMT_OFB_ARP_TPA:
                SNPRINTF (pStr + STRLEN (pStr), u4StrLen - STRLEN (pStr),
                          "%s", "ArpTP=");
                MEMSET (au1TempBuff, OFC_ZERO, sizeof (au1TempBuff));
                CliStrToIp4Addr (pOxmFld->au1Fld, (UINT1 *) au1TempBuff);
                SNPRINTF (pStr + STRLEN (pStr), u4StrLen - STRLEN (pStr),
                          "%s", au1TempBuff);
                break;

            case OFCXMT_OFB_ARP_SHA:
                SNPRINTF (pStr + STRLEN (pStr), u4StrLen - STRLEN (pStr),
                          "%s", "ArpSH=");
                MEMSET (au1TempBuff, OFC_ZERO, sizeof (au1TempBuff));
                CliMacToStr (pOxmFld->au1Fld, (UINT1 *) au1TempBuff);
                SNPRINTF (pStr + STRLEN (pStr), u4StrLen - STRLEN (pStr),
                          "%s", au1TempBuff);
                break;

            case OFCXMT_OFB_ARP_THA:
                SNPRINTF (pStr + STRLEN (pStr), u4StrLen - STRLEN (pStr),
                          "%s", "ArpTH=");
                MEMSET (au1TempBuff, OFC_ZERO, sizeof (au1TempBuff));
                CliMacToStr (pOxmFld->au1Fld, (UINT1 *) au1TempBuff);
                SNPRINTF (pStr + STRLEN (pStr), u4StrLen - STRLEN (pStr),
                          "%s", au1TempBuff);
                break;

            case OFCXMT_OFB_IPV6_SRC:
                SNPRINTF (pStr + STRLEN (pStr), u4StrLen - STRLEN (pStr),
                          "%s", "Ip6Sr=");
                MEMSET (au1TempBuff, OFC_ZERO, sizeof (au1TempBuff));
                CliIPV6ToStr (pOxmFld->au1Fld, (UINT1 *) au1TempBuff);
                SNPRINTF (pStr + STRLEN (pStr), u4StrLen - STRLEN (pStr),
                          "%s", au1TempBuff);
                break;

            case OFCXMT_OFB_IPV6_DST:
                SNPRINTF (pStr + STRLEN (pStr), u4StrLen - STRLEN (pStr),
                          "%s", "IPv6Dt=");
                MEMSET (au1TempBuff, OFC_ZERO, sizeof (au1TempBuff));
                CliIPV6ToStr (pOxmFld->au1Fld, (UINT1 *) au1TempBuff);
                SNPRINTF (pStr + STRLEN (pStr), u4StrLen - STRLEN (pStr),
                          "%s", au1TempBuff);
                break;

            case OFCXMT_OFB_IPV6_FLABEL:
                SNPRINTF (pStr + STRLEN (pStr), u4StrLen - STRLEN (pStr),
                          "%s", "Ip6lbl=");
                MEMSET (au1TempBuff, OFC_ZERO, sizeof (au1TempBuff));
                u4TempVar = OFC_ZERO;
                MEMCPY (&u4TempVar, pOxmFld->au1Fld, OFC_FOUR);
                u4TempVar = OSIX_NTOHL (u4TempVar);
                SNPRINTF (au1TempBuff, sizeof (au1TempBuff), "%u", u4TempVar);
                SNPRINTF (pStr + STRLEN (pStr), u4StrLen - STRLEN (pStr),
                          "%s", au1TempBuff);
                break;

            case OFCXMT_OFB_ICMPV6_TYPE:
                SNPRINTF (pStr + STRLEN (pStr), u4StrLen - STRLEN (pStr),
                          "%s", "Icmp6Typ=");
                MEMSET (au1TempBuff, OFC_ZERO, sizeof (au1TempBuff));
                u1TempVar = OFC_ZERO;
                MEMCPY (&u1TempVar, pOxmFld->au1Fld, OFC_ONE);
                SNPRINTF (au1TempBuff, sizeof (au1TempBuff), "%u", u1TempVar);
                SNPRINTF (pStr + STRLEN (pStr), u4StrLen - STRLEN (pStr),
                          "%s", au1TempBuff);
                break;

            case OFCXMT_OFB_ICMPV6_CODE:
                SNPRINTF (pStr + STRLEN (pStr), u4StrLen - STRLEN (pStr),
                          "%s", "Icmp6code=");
                MEMSET (au1TempBuff, OFC_ZERO, sizeof (au1TempBuff));
                u1TempVar = OFC_ZERO;
                MEMCPY (&u1TempVar, pOxmFld->au1Fld, OFC_ONE);
                SNPRINTF (au1TempBuff, sizeof (au1TempBuff), "%u", u1TempVar);
                SNPRINTF (pStr + STRLEN (pStr), u4StrLen - STRLEN (pStr),
                          "%s", au1TempBuff);
                break;

            case OFCXMT_OFB_IPV6_ND_TARGET:
                SNPRINTF (pStr + STRLEN (pStr), u4StrLen - STRLEN (pStr),
                          "%s", "IP6Trg=");
                MEMSET (au1TempBuff, OFC_ZERO, sizeof (au1TempBuff));
                CliIPV6ToStr (pOxmFld->au1Fld, (UINT1 *) au1TempBuff);
                SNPRINTF (pStr + STRLEN (pStr), u4StrLen - STRLEN (pStr),
                          "%s", au1TempBuff);
                break;

            case OFCXMT_OFB_IPV6_ND_SLL:
                SNPRINTF (pStr + STRLEN (pStr), u4StrLen - STRLEN (pStr),
                          "%s", "IP6SLL=");
                MEMSET (au1TempBuff, OFC_ZERO, sizeof (au1TempBuff));
                CliMacToStr (pOxmFld->au1Fld, (UINT1 *) au1TempBuff);
                SNPRINTF (pStr + STRLEN (pStr), u4StrLen - STRLEN (pStr),
                          "%s", au1TempBuff);
                break;

            case OFCXMT_OFB_IPV6_ND_TLL:
                SNPRINTF (pStr + STRLEN (pStr), u4StrLen - STRLEN (pStr),
                          "%s", "Ip6TTL=");
                MEMSET (au1TempBuff, OFC_ZERO, sizeof (au1TempBuff));
                CliMacToStr (pOxmFld->au1Fld, (UINT1 *) au1TempBuff);
                SNPRINTF (pStr + STRLEN (pStr), u4StrLen - STRLEN (pStr),
                          "%s", au1TempBuff);
                break;

            case OFCXMT_OFB_MPLS_LABEL:
                SNPRINTF (pStr + STRLEN (pStr), u4StrLen - STRLEN (pStr),
                          "%s", "MplsLbl=");
                MEMSET (au1TempBuff, OFC_ZERO, sizeof (au1TempBuff));
                u4TempVar = OFC_ZERO;
                MEMCPY (&u4TempVar, pOxmFld->au1Fld, OFC_FOUR);
                u4TempVar = OSIX_NTOHL (u4TempVar);
                SNPRINTF (au1TempBuff, sizeof (au1TempBuff), "%u", u4TempVar);
                SNPRINTF (pStr + STRLEN (pStr), u4StrLen - STRLEN (pStr),
                          "%s", au1TempBuff);
                break;

            case OFCXMT_OFB_MPLS_TC:
                SNPRINTF (pStr + STRLEN (pStr), u4StrLen - STRLEN (pStr),
                          "%s", "MplsTC=");
                MEMSET (au1TempBuff, OFC_ZERO, sizeof (au1TempBuff));
                u1TempVar = OFC_ZERO;
                MEMCPY (&u1TempVar, pOxmFld->au1Fld, OFC_ONE);
                SNPRINTF (au1TempBuff, sizeof (au1TempBuff), "%u", u1TempVar);
                SNPRINTF (pStr + STRLEN (pStr), u4StrLen - STRLEN (pStr),
                          "%s", au1TempBuff);
                break;

            case OFCXMT_OFB_MPLS_BOS:
                SNPRINTF (pStr + STRLEN (pStr), u4StrLen - STRLEN (pStr),
                          "%s", "MplsBOS=");
                MEMSET (au1TempBuff, OFC_ZERO, sizeof (au1TempBuff));
                u1TempVar = OFC_ZERO;
                MEMCPY (&u1TempVar, pOxmFld->au1Fld, OFC_ONE);
                SNPRINTF (au1TempBuff, sizeof (au1TempBuff), "%u", u1TempVar);
                SNPRINTF (pStr + STRLEN (pStr), u4StrLen - STRLEN (pStr),
                          "%s", au1TempBuff);
                break;

            case OFCXMT_OFB_PBB_ISID:
                SNPRINTF (pStr + STRLEN (pStr), u4StrLen - STRLEN (pStr),
                          "%s", "PBBId=");
                MEMSET (au1TempBuff, OFC_ZERO, sizeof (au1TempBuff));
                u4TempVar = OFC_ZERO;
                MEMCPY (&u4TempVar, pOxmFld->au1Fld, OFC_FOUR);
                u4TempVar = OSIX_NTOHL (u4TempVar);
                SNPRINTF (au1TempBuff, sizeof (au1TempBuff), "%u", u4TempVar);
                SNPRINTF (pStr + STRLEN (pStr), u4StrLen - STRLEN (pStr),
                          "%s", au1TempBuff);
                break;

            case OFCXMT_OFB_TUNNEL_ID:
                SNPRINTF (pStr + STRLEN (pStr), u4StrLen - STRLEN (pStr),
                          "%s", "Tnlid=");
                MEMSET (au1TempBuff, OFC_ZERO, sizeof (au1TempBuff));
                MEMCPY (&u8TempVar, pOxmFld->au1Fld, OFC_EIGHT);
                u8TempVar = Ofcinputhton64 (&u8TempVar);
                SNPRINTF (au1TempBuff, sizeof (au1TempBuff), "%u", u8TempVar);
                SNPRINTF (pStr + STRLEN (pStr), u4StrLen - STRLEN (pStr),
                          "%s", au1TempBuff);
                break;

            case OFCXMT_OFB_IPV6_EXTHDR:
                SNPRINTF (pStr + STRLEN (pStr), u4StrLen - STRLEN (pStr),
                          "%s", "IP6Ex=");
                MEMSET (au1TempBuff, OFC_ZERO, sizeof (au1TempBuff));
                u2TempVar = OFC_ZERO;
                MEMCPY (&u2TempVar, pOxmFld->au1Fld, OFC_TWO);
                u2TempVar = OSIX_NTOHS (u2TempVar);
                SNPRINTF (au1TempBuff, sizeof (au1TempBuff), "%u", u2TempVar);
                SNPRINTF (pStr + STRLEN (pStr), u4StrLen - STRLEN (pStr),
                          "%s", au1TempBuff);
                break;

            default:
                /* Unknown Set Field */
                return OFC_FAILURE;
        }
        pNext = TMO_SLL_Next (pSetFldList, (tOfcSllNode *) pSetFld);
        if (NULL != pNext)
        {
            SNPRINTF (pStr + STRLEN (pStr), u4StrLen - STRLEN (pStr), "%s",
                      ", ");
        }
    }
    OFC_TRC_FUNC ((OFC_FN_EXIT, "FUNC:Ofc131ActionsSetFieldStructTostr\n"));
    return OFC_SUCCESS;
}

/*****************************************************************************
 * Function    :  Ofc131ActionsStructTostr 
 * Description :  This routine parses the Actions List into string
 * Input       :  pActList - Pointer to the Actions SLL
 * Output      :  pStr     - Pointer to the Actions Display String 
 * Returns     :  OFC_SUCCESS/OFC_FAILURE
 *****************************************************************************/
PUBLIC INT4
Ofc131ActionsStructTostr (char *pStr, tOfcSll * pActList)
{
    tOfcActs           *pOfcActs = NULL;
    tTMO_SLL_NODE      *pNext = NULL;
    INT4                i4RetValue = OFC_ZERO;
    UINT4               u4StrLen = OFC_ACTION_STRING_MAX_LEN;
    char                au1TempBuff[OFC_TWO_FIFTY_SIX];

    OFC_TRC_FUNC ((OFC_FN_ENTRY, "FUNC:Ofc131ActionsStructTostr\n"));

    TMO_SLL_Scan (pActList, pOfcActs, tOfcActs *)
    {
        if (STRLEN (pStr) >= OFC_ACTION_STRING_MAX_LEN)
        {
            break;
        }

        switch (pOfcActs->u2Type)
        {
            case OFCAT_OUTPUT:
                SNPRINTF (pStr + STRLEN (pStr), u4StrLen - STRLEN (pStr),
                          "%s", "OutPr=");
                MEMSET (au1TempBuff, OFC_ZERO, sizeof (au1TempBuff));
                SNPRINTF (au1TempBuff, sizeof (au1TempBuff),
                          "%u", pOfcActs->Acts.Output.u4Port);
                SNPRINTF (pStr + STRLEN (pStr), u4StrLen - STRLEN (pStr),
                          "%s", au1TempBuff);
                break;

            case OFCAT_COPY_TTL_OUT:
                SNPRINTF (pStr + STRLEN (pStr), u4StrLen - STRLEN (pStr),
                          "%s", "CpyTTLOt");
                break;

            case OFCAT_COPY_TTL_IN:
                SNPRINTF (pStr + STRLEN (pStr), u4StrLen - STRLEN (pStr),
                          "%s", "CpyTTLIn");
                break;

            case OFCAT_SET_MPLS_TTL:
                SNPRINTF (pStr + STRLEN (pStr), u4StrLen - STRLEN (pStr),
                          "%s", "StMpls=");
                MEMSET (au1TempBuff, OFC_ZERO, sizeof (au1TempBuff));
                SNPRINTF (au1TempBuff, sizeof (au1TempBuff),
                          "%u", pOfcActs->Acts.SetMplsTtl.u1MplsTtl);
                SNPRINTF (pStr + STRLEN (pStr), u4StrLen - STRLEN (pStr),
                          "%s", au1TempBuff);
                break;

            case OFCAT_DEC_MPLS_TTL:
                SNPRINTF (pStr + STRLEN (pStr), u4StrLen - STRLEN (pStr),
                          "%s", "DecMpls");
                break;

            case OFCAT_PUSH_VLAN:
                SNPRINTF (pStr + STRLEN (pStr), u4StrLen - STRLEN (pStr),
                          "%s", "PshVlan=");
                MEMSET (au1TempBuff, OFC_ZERO, sizeof (au1TempBuff));
                SNPRINTF (au1TempBuff, sizeof (au1TempBuff),
                          "%x", pOfcActs->Acts.Push.u2EthType);
                SNPRINTF (pStr + STRLEN (pStr), u4StrLen - STRLEN (pStr),
                          "%s", au1TempBuff);
                break;

            case OFCAT_POP_VLAN:
                SNPRINTF (pStr + STRLEN (pStr), u4StrLen - STRLEN (pStr),
                          "%s", "PopVlan");
                break;

            case OFCAT_PUSH_MPLS:
                SNPRINTF (pStr + STRLEN (pStr), u4StrLen - STRLEN (pStr),
                          "%s", "PshMpls=");
                MEMSET (au1TempBuff, OFC_ZERO, sizeof (au1TempBuff));
                SNPRINTF (au1TempBuff, sizeof (au1TempBuff),
                          "%u", pOfcActs->Acts.Push.u2EthType);
                SNPRINTF (pStr + STRLEN (pStr), u4StrLen - STRLEN (pStr),
                          "%s", au1TempBuff);
                break;

            case OFCAT_POP_MPLS:
                SNPRINTF (pStr + STRLEN (pStr), u4StrLen - STRLEN (pStr),
                          "%s", "PopMpls=");
                MEMSET (au1TempBuff, OFC_ZERO, sizeof (au1TempBuff));
                SNPRINTF (au1TempBuff, sizeof (au1TempBuff),
                          "%u", pOfcActs->Acts.PopMpls.u2EthType);
                SNPRINTF (pStr + STRLEN (pStr), u4StrLen - STRLEN (pStr),
                          "%s", au1TempBuff);
                break;

            case OFCAT_SET_QUEUE:
                SNPRINTF (pStr + STRLEN (pStr), u4StrLen - STRLEN (pStr),
                          "%s", "StQ=");
                MEMSET (au1TempBuff, OFC_ZERO, sizeof (au1TempBuff));
                SNPRINTF (au1TempBuff, sizeof (au1TempBuff),
                          "%u", pOfcActs->Acts.SetQue.u4QueId);
                SNPRINTF (pStr + STRLEN (pStr), u4StrLen - STRLEN (pStr),
                          "%s", au1TempBuff);
                break;

            case OFCAT_GROUP:
                SNPRINTF (pStr + STRLEN (pStr), u4StrLen - STRLEN (pStr),
                          "%s", "Grp=");
                MEMSET (au1TempBuff, OFC_ZERO, sizeof (au1TempBuff));
                SNPRINTF (au1TempBuff, sizeof (au1TempBuff),
                          "%u", pOfcActs->Acts.Group.u4GroupId);
                SNPRINTF (pStr + STRLEN (pStr), u4StrLen - STRLEN (pStr),
                          "%s", au1TempBuff);
                break;

            case OFCAT_SET_NW_TTL:
                SNPRINTF (pStr + STRLEN (pStr), u4StrLen - STRLEN (pStr),
                          "%s", "StNw=");
                MEMSET (au1TempBuff, OFC_ZERO, sizeof (au1TempBuff));
                SNPRINTF (au1TempBuff, sizeof (au1TempBuff),
                          "%u", pOfcActs->Acts.SetNwTtl.u1NwTtl);
                SNPRINTF (pStr + STRLEN (pStr), u4StrLen - STRLEN (pStr),
                          "%s", au1TempBuff);
                break;

            case OFCAT_DEC_NW_TTL:
                SNPRINTF (pStr + STRLEN (pStr), u4StrLen - STRLEN (pStr),
                          "%s", "DecNw");
                break;

            case OFCAT_SET_FIELD:
                SNPRINTF (pStr + STRLEN (pStr), u4StrLen - STRLEN (pStr),
                          "%s", "StFdActs:");
                i4RetValue = Ofc131ActionsSetFieldStructTostr (pStr,
                                                               &(pOfcActs->Acts.
                                                                 SetFld.
                                                                 SetFldList));

                if (OFC_FAILURE == i4RetValue)
                {
                    return OFC_FAILURE;
                }
                break;

            case OFCAT_PUSH_PBB:
                SNPRINTF (pStr + STRLEN (pStr), u4StrLen - STRLEN (pStr),
                          "%s", "PshPBB=");
                MEMSET (au1TempBuff, OFC_ZERO, sizeof (au1TempBuff));
                SNPRINTF (au1TempBuff, sizeof (au1TempBuff),
                          "%u", pOfcActs->Acts.Push.u2EthType);
                SNPRINTF (pStr + STRLEN (pStr), u4StrLen - STRLEN (pStr),
                          "%s", au1TempBuff);
                break;

            case OFCAT_POP_PBB:
                SNPRINTF (pStr + STRLEN (pStr), u4StrLen - STRLEN (pStr),
                          "%s", "PopPBB");
                break;

            case OFCAT_EXPERIMENTER:
                SNPRINTF (pStr + STRLEN (pStr), u4StrLen - STRLEN (pStr),
                          "%s", "Exp");
                MEMSET (au1TempBuff, OFC_ZERO, sizeof (au1TempBuff));
                SNPRINTF (au1TempBuff, sizeof (au1TempBuff),
                          "%u", pOfcActs->Acts.ExprHdr.u4Experimenter);
                SNPRINTF (pStr + STRLEN (pStr), u4StrLen - STRLEN (pStr),
                          "%s", au1TempBuff);
                break;

            default:
                return OFC_FAILURE;
        }
        pNext = TMO_SLL_Next (pActList, (tOfcSllNode *) pOfcActs);
        if (NULL != pNext)
        {
            SNPRINTF (pStr + STRLEN (pStr), u4StrLen - STRLEN (pStr), "%s",
                      ", ");
        }
    }
    OFC_TRC_FUNC ((OFC_FN_EXIT, "FUNC:Ofc131ActionsStructTostr\n"));
    return OFC_SUCCESS;
}

/*****************************************************************************
 * Function    :  Ofc131InstructActionStructToStr 
 * Description :  This routine parses the Instructions into string
 * Input       :  pOfcSllList - Pointer to the Instructions SLL
 * Output      :  pStr        - Pointer to the Actions Display String 
 * Returns     :  OFC_SUCCESS/OFC_FAILURE
 *****************************************************************************/
PUBLIC INT4
Ofc131InstructActionStructToStr (char *pStr, tOfcSll * pOfcSllList)
{
    tOfcInstr          *pOfcInst = NULL;
    tTMO_SLL_NODE      *pNext = NULL;
    INT4                i4RetValue = OFC_ZERO;
    UINT4               u4TempVar = OFC_ZERO;
    UINT4               u4StrLen = OFC_ACTION_STRING_MAX_LEN;
    AR_UINT8            u8EightByte = OFC_ZERO;
    char                au1TempBuff[OFC_FIVE_TWELVE];

    MEMSET (au1TempBuff, OFC_ZERO, sizeof (au1TempBuff));

    OFC_TRC_FUNC ((OFC_FN_ENTRY, "FUNC:Ofc131InstructActionStructToStr\n"));

    TMO_SLL_Scan (pOfcSllList, pOfcInst, tOfcInstr *)
    {
        if (STRLEN (pStr) >= OFC_ACTION_STRING_MAX_LEN)
        {
            break;
        }

        switch (pOfcInst->u2Type)
        {
            case OFCIT_GOTO_TABLE:
                SNPRINTF (pStr + STRLEN (pStr), u4StrLen - STRLEN (pStr),
                          "%s", "GOTO:TblId=");
                MEMSET (au1TempBuff, OFC_ZERO, sizeof (au1TempBuff));
                SNPRINTF (au1TempBuff, sizeof (au1TempBuff),
                          "%u", pOfcInst->Instr.GotoTable.u1TableId);
                SNPRINTF (pStr + STRLEN (pStr), u4StrLen - STRLEN (pStr),
                          "%s", au1TempBuff);
                break;

            case OFCIT_WRITE_METADATA:
                SNPRINTF (pStr + STRLEN (pStr), u4StrLen - STRLEN (pStr),
                          "%s", "MTADATA=");
                MEMSET (au1TempBuff, OFC_ZERO, sizeof (au1TempBuff));

                MEMCPY (&u8EightByte,
                        (UINT1 *) &(pOfcInst->Instr.Metadata.u8Metadata),
                        OFC_EIGHT);
                u8EightByte = Ofcinputhton64 (&u8EightByte);
                SNPRINTF (au1TempBuff, u4StrLen - STRLEN (pStr), "%u",
                          u8EightByte);
                SNPRINTF (pStr + STRLEN (pStr), u4StrLen - STRLEN (pStr), "%s",
                          au1TempBuff);
                break;

            case OFCIT_WRITE_ACTIONS:
            case OFCIT_APPLY_ACTIONS:
                if (OFCIT_WRITE_ACTIONS == pOfcInst->u2Type)
                {
                    SNPRINTF (pStr + STRLEN (pStr), u4StrLen - STRLEN (pStr),
                              "%s", "WRITACTS:");
                }
                else
                {
                    SNPRINTF (pStr + STRLEN (pStr), u4StrLen - STRLEN (pStr),
                              "%s", "APLYACTS:");
                }
                i4RetValue = Ofc131ActionsStructTostr (pStr,
                                                       &(pOfcInst->Instr.
                                                         Actions.ActList));
                if (i4RetValue == OFC_FAILURE)
                {
                    return OFC_FAILURE;
                }
                break;

            case OFCIT_CLEAR_ACTIONS:
                SNPRINTF (pStr + STRLEN (pStr), u4StrLen - STRLEN (pStr),
                          "%s", "CLRACTS");
                break;

            case OFCIT_METER:
                SNPRINTF (pStr + STRLEN (pStr), u4StrLen - STRLEN (pStr),
                          "%s", "METERId=");
                MEMSET (au1TempBuff, OFC_ZERO, sizeof (au1TempBuff));
                MEMCPY (&u4TempVar, &pOfcInst->Instr.Meter.u4MeterId, OFC_FOUR);
                u4TempVar = OSIX_NTOHL (u4TempVar);
                SNPRINTF (au1TempBuff, sizeof (au1TempBuff), "%u", u4TempVar);
                SNPRINTF (pStr + STRLEN (pStr), u4StrLen - STRLEN (pStr),
                          "%s", au1TempBuff);
                break;

            case OFCIT_EXPERIMENTER:
                SNPRINTF (pStr + STRLEN (pStr), u4StrLen - STRLEN (pStr),
                          "%s", "EXP=");
                MEMSET (au1TempBuff, OFC_ZERO, sizeof (au1TempBuff));
                SNPRINTF (au1TempBuff, sizeof (au1TempBuff),
                          "%u", pOfcInst->Instr.Experimenter.u4Experimenter);
                SNPRINTF (pStr + STRLEN (pStr), u4StrLen - STRLEN (pStr),
                          "%s", au1TempBuff);
                break;

            default:
                /* Unknown Instruction */
                return OFC_FAILURE;
        }

        pNext = TMO_SLL_Next (pOfcSllList, (tOfcSllNode *) pOfcInst);
        if (NULL != pNext)
        {
            SNPRINTF (pStr + STRLEN (pStr), u4StrLen - STRLEN (pStr), "%s",
                      ", ");
        }
    }
    OFC_TRC_FUNC ((OFC_FN_EXIT, "FUNC:Ofc131InstructActionStructToStr\n"));
    return OFC_SUCCESS;
}

/*****************************************************************************
 * Function    :  Ofc131MatchFieldsStructTostr 
 * Description :  This routine parses the MatchFields into string
 * Input       :  pMatchList - Pointer to Flow Match SLL
 * Output      :  pStr       - Pointer to the Match Fields Display String 
 * Returns     :  OFC_SUCCESS/OFC_FAILURE
 *****************************************************************************/
PUBLIC UINT4
Ofc131MatchFieldsStructToStr (char *pStr, tOfcSll * pMatchList)
{
    tOfpErrMsg          OfpErrorMsg;
    tOfcWcFlowMatch    *pOfcFlowMatch = NULL;
    tTMO_SLL_NODE      *pNext = NULL;
    UINT4               u4StrLen = OFC_ACTION_STRING_MAX_LEN;
    UINT4               u4TempVar = OFC_ZERO;
    UINT1               u1TempVar = OFC_ZERO;
    UINT2               u2TempVar = OFC_ZERO;
    AR_UINT8            u8TempVar = OFC_ZERO;
    char                au1TempBuff[OFC_ACTION_STRING_MAX_LEN];

    OFC_TRC_FUNC ((OFC_FN_ENTRY, "FUNC:Ofc131MatchFieldsStructToStr\n"));

    MEMSET (&OfpErrorMsg, OFC_ZERO, sizeof (tOfpErrMsg));
    MEMSET (au1TempBuff, OFC_ZERO, sizeof (au1TempBuff));

    TMO_SLL_Scan (pMatchList, pOfcFlowMatch, tOfcWcFlowMatch *)
    {
        if (STRLEN (pStr) >= OFC_ACTION_STRING_MAX_LEN)
        {
            break;
        }

        switch (pOfcFlowMatch->u2Type)
        {
            case OFCXMT_OFB_IN_PORT:
                SNPRINTF (pStr + STRLEN (pStr), u4StrLen - STRLEN (pStr),
                          "%s", "InPrt=");
                MEMSET (au1TempBuff, OFC_ZERO, sizeof (au1TempBuff));
                u4TempVar = OFC_ZERO;
                MEMCPY (&u4TempVar, pOfcFlowMatch->FldValue.au1Fld, OFC_FOUR);
                u4TempVar = OSIX_NTOHL (u4TempVar);
                SNPRINTF (au1TempBuff, sizeof (au1TempBuff), "%u", u4TempVar);
                SNPRINTF (pStr + STRLEN (pStr), u4StrLen - STRLEN (pStr),
                          "%s", au1TempBuff);
                break;

            case OFCXMT_OFB_IN_PHY_PORT:
                SNPRINTF (pStr + STRLEN (pStr), u4StrLen - STRLEN (pStr),
                          "%s", "PhPrt=");
                MEMSET (au1TempBuff, OFC_ZERO, sizeof (au1TempBuff));
                u4TempVar = OFC_ZERO;
                MEMCPY (&u4TempVar, pOfcFlowMatch->FldValue.au1Fld, OFC_FOUR);
                u4TempVar = OSIX_NTOHL (u4TempVar);
                SNPRINTF (au1TempBuff, OFC_EIGHT, "%u", u4TempVar);
                SNPRINTF (pStr + STRLEN (pStr), u4StrLen - STRLEN (pStr),
                          "%s", au1TempBuff);
                break;

            case OFCXMT_OFB_METADATA:
                SNPRINTF (pStr + STRLEN (pStr), u4StrLen - STRLEN (pStr),
                          "%s", "MtaDta=");
                MEMSET (au1TempBuff, OFC_ZERO, sizeof (au1TempBuff));
                u8TempVar = OFC_ZERO;
                MEMCPY (&u8TempVar, pOfcFlowMatch->FldValue.au1Fld, OFC_EIGHT);
                u8TempVar = Ofcinputhton64 (&u8TempVar);
                SNPRINTF (au1TempBuff, u4StrLen - STRLEN (pStr), "%u",
                          u8TempVar);
                SNPRINTF (pStr + STRLEN (pStr), u4StrLen - STRLEN (pStr), "%s",
                          au1TempBuff);
                break;

            case OFCXMT_OFB_ETH_DST:
                SNPRINTF (pStr + STRLEN (pStr), u4StrLen - STRLEN (pStr),
                          "%s", "EthDst=");
                MEMSET (au1TempBuff, OFC_ZERO, sizeof (au1TempBuff));
                CliMacToStr (pOfcFlowMatch->FldValue.au1Fld,
                             (UINT1 *) au1TempBuff);
                if (OFC_THREE < STRLEN (au1TempBuff))
                {
                    au1TempBuff[STRLEN (au1TempBuff) - OFC_THREE] = '\0';
                }
                SNPRINTF (pStr + STRLEN (pStr), u4StrLen - STRLEN (pStr),
                          "%s", au1TempBuff);
                break;

            case OFCXMT_OFB_ETH_SRC:
                SNPRINTF (pStr + STRLEN (pStr), u4StrLen - STRLEN (pStr),
                          "%s", "EthSrc=");
                MEMSET (au1TempBuff, OFC_ZERO, sizeof (au1TempBuff));
                CliMacToStr (pOfcFlowMatch->FldValue.au1Fld,
                             (UINT1 *) au1TempBuff);
                if (OFC_THREE < STRLEN (au1TempBuff))
                {
                    au1TempBuff[STRLEN (au1TempBuff) - OFC_THREE] = '\0';
                }
                SNPRINTF (pStr + STRLEN (pStr), u4StrLen - STRLEN (pStr),
                          "%s", au1TempBuff);
                break;

            case OFCXMT_OFB_ETH_TYPE:
                SNPRINTF (pStr + STRLEN (pStr), u4StrLen - STRLEN (pStr),
                          "%s", "EthTyp=");
                MEMSET (au1TempBuff, OFC_ZERO, sizeof (au1TempBuff));
                u2TempVar = OFC_ZERO;
                MEMCPY (&u2TempVar, pOfcFlowMatch->FldValue.au1Fld, OFC_TWO);
                u2TempVar = OSIX_NTOHS (u2TempVar);
                SNPRINTF (au1TempBuff, sizeof (au1TempBuff), "%x", u2TempVar);
                SNPRINTF (pStr + STRLEN (pStr), u4StrLen - STRLEN (pStr),
                          "%s", "0x");
                SNPRINTF (pStr + STRLEN (pStr), u4StrLen - STRLEN (pStr),
                          "%s", au1TempBuff);
                break;

            case OFCXMT_OFB_VLAN_VID:
                SNPRINTF (pStr + STRLEN (pStr), u4StrLen - STRLEN (pStr),
                          "%s", "Vlan=");
                MEMSET (au1TempBuff, OFC_ZERO, sizeof (au1TempBuff));
                u2TempVar = OFC_ZERO;
                MEMCPY (&u2TempVar, pOfcFlowMatch->FldValue.au1Fld, OFC_TWO);
                u2TempVar = OSIX_NTOHS (u2TempVar);
                SNPRINTF (au1TempBuff, sizeof (au1TempBuff), "%u", u2TempVar);
                SNPRINTF (pStr + STRLEN (pStr), u4StrLen - STRLEN (pStr),
                          "%s", au1TempBuff);
                break;

            case OFCXMT_OFB_VLAN_PCP:
                SNPRINTF (pStr + STRLEN (pStr), u4StrLen - STRLEN (pStr),
                          "%s", "VlanPcp=");
                MEMSET (au1TempBuff, OFC_ZERO, sizeof (au1TempBuff));
                u1TempVar = OFC_ZERO;
                MEMCPY (&u1TempVar, pOfcFlowMatch->FldValue.au1Fld, OFC_ONE);
                SNPRINTF (au1TempBuff, sizeof (au1TempBuff), "%u", u1TempVar);
                SNPRINTF (pStr + STRLEN (pStr), u4StrLen - STRLEN (pStr),
                          "%s", au1TempBuff);
                break;

            case OFCXMT_OFB_IP_DSCP:
                SNPRINTF (pStr + STRLEN (pStr), u4StrLen - STRLEN (pStr),
                          "%s", "IpDscp=");
                MEMSET (au1TempBuff, OFC_ZERO, sizeof (au1TempBuff));
                u1TempVar = OFC_ZERO;
                MEMCPY (&u1TempVar, pOfcFlowMatch->FldValue.au1Fld, OFC_ONE);
                SNPRINTF (au1TempBuff, sizeof (au1TempBuff), "%u", u1TempVar);
                SNPRINTF (pStr + STRLEN (pStr), u4StrLen - STRLEN (pStr),
                          "%s", au1TempBuff);
                break;

            case OFCXMT_OFB_IP_ECN:
                SNPRINTF (pStr + STRLEN (pStr), u4StrLen - STRLEN (pStr),
                          "%s", "IpECN=");
                MEMSET (au1TempBuff, OFC_ZERO, sizeof (au1TempBuff));
                u1TempVar = OFC_ZERO;
                MEMCPY (&u1TempVar, pOfcFlowMatch->FldValue.au1Fld, OFC_ONE);
                SNPRINTF (au1TempBuff, sizeof (au1TempBuff), "%u", u1TempVar);
                SNPRINTF (pStr + STRLEN (pStr), u4StrLen - STRLEN (pStr),
                          "%s", au1TempBuff);
                break;

            case OFCXMT_OFB_IP_PROTO:
                SNPRINTF (pStr + STRLEN (pStr), u4StrLen - STRLEN (pStr),
                          "%s", "IpPrto=");
                MEMSET (au1TempBuff, OFC_ZERO, sizeof (au1TempBuff));
                u1TempVar = OFC_ZERO;
                MEMCPY (&u1TempVar, pOfcFlowMatch->FldValue.au1Fld, OFC_ONE);
                SNPRINTF (au1TempBuff, sizeof (au1TempBuff), "%u", u1TempVar);
                SNPRINTF (pStr + STRLEN (pStr), u4StrLen - STRLEN (pStr),
                          "%s", au1TempBuff);
                break;

            case OFCXMT_OFB_IPV4_SRC:
                SNPRINTF (pStr + STRLEN (pStr), u4StrLen - STRLEN (pStr),
                          "%s", "Ip4Sr=");
                MEMSET (au1TempBuff, OFC_ZERO, sizeof (au1TempBuff));
                CliStrToIp4Addr (pOfcFlowMatch->FldValue.au1Fld,
                                 (UINT1 *) au1TempBuff);
                SNPRINTF (pStr + STRLEN (pStr), u4StrLen - STRLEN (pStr),
                          "%s", au1TempBuff);
                break;

            case OFCXMT_OFB_IPV4_DST:
                SNPRINTF (pStr + STRLEN (pStr), u4StrLen - STRLEN (pStr),
                          "%s", "Ip4Ds=");
                MEMSET (au1TempBuff, OFC_ZERO, sizeof (au1TempBuff));
                CliStrToIp4Addr (pOfcFlowMatch->FldValue.au1Fld,
                                 (UINT1 *) au1TempBuff);
                SNPRINTF (pStr + STRLEN (pStr), u4StrLen - STRLEN (pStr),
                          "%s", au1TempBuff);
                break;

            case OFCXMT_OFB_TCP_SRC:
                SNPRINTF (pStr + STRLEN (pStr), u4StrLen - STRLEN (pStr),
                          "%s", "TcpSr=");
                MEMSET (au1TempBuff, OFC_ZERO, sizeof (au1TempBuff));
                u2TempVar = OFC_ZERO;
                MEMCPY (&u2TempVar, pOfcFlowMatch->FldValue.au1Fld, OFC_TWO);
                u2TempVar = OSIX_NTOHS (u2TempVar);
                SNPRINTF (au1TempBuff, sizeof (au1TempBuff), "%u", u2TempVar);
                SNPRINTF (pStr + STRLEN (pStr), u4StrLen - STRLEN (pStr),
                          "%s", au1TempBuff);
                break;

            case OFCXMT_OFB_TCP_DST:
                SNPRINTF (pStr + STRLEN (pStr), u4StrLen - STRLEN (pStr),
                          "%s", "TcpDs=");
                MEMSET (au1TempBuff, OFC_ZERO, sizeof (au1TempBuff));
                u2TempVar = OFC_ZERO;
                MEMCPY (&u2TempVar, pOfcFlowMatch->FldValue.au1Fld, OFC_TWO);
                u2TempVar = OSIX_NTOHS (u2TempVar);
                SNPRINTF (au1TempBuff, sizeof (au1TempBuff), "%u", u2TempVar);
                SNPRINTF (pStr + STRLEN (pStr), u4StrLen - STRLEN (pStr),
                          "%s", au1TempBuff);
                break;

            case OFCXMT_OFB_UDP_SRC:
                SNPRINTF (pStr + STRLEN (pStr), u4StrLen - STRLEN (pStr),
                          "%s", "UdpSr=");
                MEMSET (au1TempBuff, OFC_ZERO, sizeof (au1TempBuff));
                u2TempVar = OFC_ZERO;
                MEMCPY (&u2TempVar, pOfcFlowMatch->FldValue.au1Fld, OFC_TWO);
                u2TempVar = OSIX_NTOHS (u2TempVar);
                SNPRINTF (au1TempBuff, sizeof (au1TempBuff), "%u", u2TempVar);
                SNPRINTF (pStr + STRLEN (pStr), u4StrLen - STRLEN (pStr),
                          "%s", au1TempBuff);
                break;

            case OFCXMT_OFB_UDP_DST:
                SNPRINTF (pStr + STRLEN (pStr), u4StrLen - STRLEN (pStr),
                          "%s", "UdpDs=");
                MEMSET (au1TempBuff, OFC_ZERO, sizeof (au1TempBuff));
                u2TempVar = OFC_ZERO;
                MEMCPY (&u2TempVar, pOfcFlowMatch->FldValue.au1Fld, OFC_TWO);
                u2TempVar = OSIX_NTOHS (u2TempVar);
                SNPRINTF (au1TempBuff, sizeof (au1TempBuff), "%u", u2TempVar);
                SNPRINTF (pStr + STRLEN (pStr), u4StrLen - STRLEN (pStr),
                          "%s", au1TempBuff);
                break;

            case OFCXMT_OFB_SCTP_SRC:
                SNPRINTF (pStr + STRLEN (pStr), u4StrLen - STRLEN (pStr),
                          "%s", "SctpSrc=");
                MEMSET (au1TempBuff, OFC_ZERO, sizeof (au1TempBuff));
                u2TempVar = OFC_ZERO;
                MEMCPY (&u2TempVar, pOfcFlowMatch->FldValue.au1Fld, OFC_TWO);
                u2TempVar = OSIX_NTOHS (u2TempVar);
                SNPRINTF (au1TempBuff, sizeof (au1TempBuff), "%u", u2TempVar);
                SNPRINTF (pStr + STRLEN (pStr), u4StrLen - STRLEN (pStr),
                          "%s", au1TempBuff);
                break;

            case OFCXMT_OFB_SCTP_DST:
                SNPRINTF (pStr + STRLEN (pStr), u4StrLen - STRLEN (pStr),
                          "%s", "SctpDst=");
                MEMSET (au1TempBuff, OFC_ZERO, sizeof (au1TempBuff));
                u2TempVar = OFC_ZERO;
                MEMCPY (&u2TempVar, pOfcFlowMatch->FldValue.au1Fld, OFC_TWO);
                u2TempVar = OSIX_NTOHS (u2TempVar);
                SNPRINTF (au1TempBuff, sizeof (au1TempBuff), "%u", u2TempVar);
                SNPRINTF (pStr + STRLEN (pStr), u4StrLen - STRLEN (pStr),
                          "%s", au1TempBuff);
                break;

            case OFCXMT_OFB_ICMPV4_TYPE:
                SNPRINTF (pStr + STRLEN (pStr), u4StrLen - STRLEN (pStr),
                          "%s", "Icmp4typ=");
                MEMSET (au1TempBuff, OFC_ZERO, sizeof (au1TempBuff));
                u1TempVar = OFC_ZERO;
                MEMCPY (&u1TempVar, pOfcFlowMatch->FldValue.au1Fld, OFC_ONE);
                SNPRINTF (au1TempBuff, sizeof (au1TempBuff), "%u", u1TempVar);
                SNPRINTF (pStr + STRLEN (pStr), u4StrLen - STRLEN (pStr),
                          "%s", au1TempBuff);
                break;

            case OFCXMT_OFB_ICMPV4_CODE:
                SNPRINTF (pStr + STRLEN (pStr), u4StrLen - STRLEN (pStr),
                          "%s", "Icmp4Cde=");
                MEMSET (au1TempBuff, OFC_ZERO, sizeof (au1TempBuff));
                u1TempVar = OFC_ZERO;
                MEMCPY (&u1TempVar, pOfcFlowMatch->FldValue.au1Fld, OFC_ONE);
                SNPRINTF (au1TempBuff, sizeof (au1TempBuff), "%u", u1TempVar);
                SNPRINTF (pStr + STRLEN (pStr), u4StrLen - STRLEN (pStr),
                          "%s", au1TempBuff);
                break;

            case OFCXMT_OFB_ARP_OP:
                SNPRINTF (pStr + STRLEN (pStr), u4StrLen - STRLEN (pStr),
                          "%s", "ArpOP=");
                MEMSET (au1TempBuff, OFC_ZERO, sizeof (au1TempBuff));
                u2TempVar = OFC_ZERO;
                MEMCPY (&u2TempVar, pOfcFlowMatch->FldValue.au1Fld, OFC_TWO);
                u2TempVar = OSIX_NTOHS (u2TempVar);
                SNPRINTF (au1TempBuff, sizeof (au1TempBuff), "%u", u2TempVar);
                SNPRINTF (pStr + STRLEN (pStr), u4StrLen - STRLEN (pStr),
                          "%s", au1TempBuff);
                break;

            case OFCXMT_OFB_ARP_SPA:
                SNPRINTF (pStr + STRLEN (pStr), u4StrLen - STRLEN (pStr),
                          "%s", "ArpSpa=");
                MEMSET (au1TempBuff, OFC_ZERO, sizeof (au1TempBuff));
                CliStrToIp4Addr (pOfcFlowMatch->FldValue.au1Fld,
                                 (UINT1 *) au1TempBuff);
                SNPRINTF (pStr + STRLEN (pStr), u4StrLen - STRLEN (pStr),
                          "%s", au1TempBuff);
                break;

            case OFCXMT_OFB_ARP_TPA:
                SNPRINTF (pStr + STRLEN (pStr), u4StrLen - STRLEN (pStr),
                          "%s", "ArpTPA=");
                MEMSET (au1TempBuff, OFC_ZERO, sizeof (au1TempBuff));
                CliStrToIp4Addr (pOfcFlowMatch->FldValue.au1Fld,
                                 (UINT1 *) au1TempBuff);
                SNPRINTF (pStr + STRLEN (pStr), u4StrLen - STRLEN (pStr),
                          "%s", au1TempBuff);
                break;

            case OFCXMT_OFB_ARP_SHA:
                SNPRINTF (pStr + STRLEN (pStr), u4StrLen - STRLEN (pStr),
                          "%s", "ArpSHA=");
                MEMSET (au1TempBuff, OFC_ZERO, sizeof (au1TempBuff));
                CliMacToStr (pOfcFlowMatch->FldValue.au1Fld,
                             (UINT1 *) au1TempBuff);
                SNPRINTF (pStr + STRLEN (pStr), u4StrLen - STRLEN (pStr),
                          "%s", au1TempBuff);
                break;

            case OFCXMT_OFB_ARP_THA:
                SNPRINTF (pStr + STRLEN (pStr), u4StrLen - STRLEN (pStr),
                          "%s", "ArpTHA=");
                MEMSET (au1TempBuff, OFC_ZERO, sizeof (au1TempBuff));
                CliMacToStr (pOfcFlowMatch->FldValue.au1Fld,
                             (UINT1 *) au1TempBuff);
                SNPRINTF (pStr + STRLEN (pStr), u4StrLen - STRLEN (pStr),
                          "%s", au1TempBuff);
                break;

            case OFCXMT_OFB_IPV6_SRC:
                SNPRINTF (pStr + STRLEN (pStr), u4StrLen - STRLEN (pStr),
                          "%s", "Ip6Src=");
                MEMSET (au1TempBuff, OFC_ZERO, sizeof (au1TempBuff));
                CliIPV6ToStr (pOfcFlowMatch->FldValue.au1Fld,
                              (UINT1 *) au1TempBuff);
                SNPRINTF (pStr + STRLEN (pStr), u4StrLen - STRLEN (pStr),
                          "%s", au1TempBuff);
                break;

            case OFCXMT_OFB_IPV6_DST:
                SNPRINTF (pStr + STRLEN (pStr), u4StrLen - STRLEN (pStr),
                          "%s", "IP6Dst=");
                MEMSET (au1TempBuff, OFC_ZERO, sizeof (au1TempBuff));
                CliIPV6ToStr (pOfcFlowMatch->FldValue.au1Fld,
                              (UINT1 *) au1TempBuff);
                SNPRINTF (pStr + STRLEN (pStr), u4StrLen - STRLEN (pStr),
                          "%s", au1TempBuff);
                break;

            case OFCXMT_OFB_IPV6_FLABEL:
                SNPRINTF (pStr + STRLEN (pStr), u4StrLen - STRLEN (pStr),
                          "%s", "Ip6Lbl=");
                MEMSET (au1TempBuff, OFC_ZERO, sizeof (au1TempBuff));
                u4TempVar = OFC_ZERO;
                MEMCPY (&u4TempVar, pOfcFlowMatch->FldValue.au1Fld, OFC_FOUR);
                u4TempVar = OSIX_NTOHL (u4TempVar);
                SNPRINTF (au1TempBuff, sizeof (au1TempBuff), "%u", u4TempVar);
                SNPRINTF (pStr + STRLEN (pStr), u4StrLen - STRLEN (pStr),
                          "%s", au1TempBuff);
                break;

            case OFCXMT_OFB_ICMPV6_TYPE:
                SNPRINTF (pStr + STRLEN (pStr), u4StrLen - STRLEN (pStr),
                          "%s", "Icmp6Typ=");
                MEMSET (au1TempBuff, OFC_ZERO, sizeof (au1TempBuff));
                u1TempVar = OFC_ZERO;
                MEMCPY (&u1TempVar, pOfcFlowMatch->FldValue.au1Fld, OFC_ONE);
                SNPRINTF (au1TempBuff, sizeof (au1TempBuff), "%u", u1TempVar);
                SNPRINTF (pStr + STRLEN (pStr), u4StrLen - STRLEN (pStr),
                          "%s", au1TempBuff);
                break;

            case OFCXMT_OFB_ICMPV6_CODE:
                SNPRINTF (pStr + STRLEN (pStr), u4StrLen - STRLEN (pStr),
                          "%s", "Icmp6cde=");
                MEMSET (au1TempBuff, OFC_ZERO, sizeof (au1TempBuff));
                u1TempVar = OFC_ZERO;
                MEMCPY (&u1TempVar, pOfcFlowMatch->FldValue.au1Fld, OFC_ONE);
                SNPRINTF (au1TempBuff, sizeof (au1TempBuff), "%u", u1TempVar);
                SNPRINTF (pStr + STRLEN (pStr), u4StrLen - STRLEN (pStr),
                          "%s", au1TempBuff);
                break;

            case OFCXMT_OFB_IPV6_ND_TARGET:
                SNPRINTF (pStr + STRLEN (pStr), u4StrLen - STRLEN (pStr),
                          "%s", "IP6Tgt=");
                MEMSET (au1TempBuff, OFC_ZERO, sizeof (au1TempBuff));
                CliIPV6ToStr (pOfcFlowMatch->FldValue.au1Fld,
                              (UINT1 *) au1TempBuff);
                SNPRINTF (pStr + STRLEN (pStr), u4StrLen - STRLEN (pStr),
                          "%s", au1TempBuff);
                break;

            case OFCXMT_OFB_IPV6_ND_SLL:
                SNPRINTF (pStr + STRLEN (pStr), u4StrLen - STRLEN (pStr),
                          "%s", "IP6SLL=");
                MEMSET (au1TempBuff, OFC_ZERO, sizeof (au1TempBuff));
                CliMacToStr (pOfcFlowMatch->FldValue.au1Fld,
                             (UINT1 *) au1TempBuff);
                SNPRINTF (pStr + STRLEN (pStr), u4StrLen - STRLEN (pStr),
                          "%s", au1TempBuff);
                break;

            case OFCXMT_OFB_IPV6_ND_TLL:
                SNPRINTF (pStr + STRLEN (pStr), u4StrLen - STRLEN (pStr),
                          "%s", "Ip6TTL=");
                MEMSET (au1TempBuff, OFC_ZERO, sizeof (au1TempBuff));
                CliMacToStr (pOfcFlowMatch->FldValue.au1Fld,
                             (UINT1 *) au1TempBuff);
                SNPRINTF (pStr + STRLEN (pStr), u4StrLen - STRLEN (pStr),
                          "%s", au1TempBuff);
                break;

            case OFCXMT_OFB_MPLS_LABEL:
                SNPRINTF (pStr + STRLEN (pStr), u4StrLen - STRLEN (pStr),
                          "%s", "MplsLbl=");
                MEMSET (au1TempBuff, OFC_ZERO, sizeof (au1TempBuff));
                u4TempVar = OFC_ZERO;
                MEMCPY (&u4TempVar, pOfcFlowMatch->FldValue.au1Fld, OFC_FOUR);
                u4TempVar = OSIX_NTOHL (u4TempVar);
                SNPRINTF (au1TempBuff, sizeof (au1TempBuff), "%u", u4TempVar);
                SNPRINTF (pStr + STRLEN (pStr), u4StrLen - STRLEN (pStr),
                          "%s", au1TempBuff);
                break;

            case OFCXMT_OFB_MPLS_TC:
                SNPRINTF (pStr + STRLEN (pStr), u4StrLen - STRLEN (pStr),
                          "%s", "MplsTC=");
                MEMSET (au1TempBuff, OFC_ZERO, sizeof (au1TempBuff));
                u1TempVar = OFC_ZERO;
                MEMCPY (&u1TempVar, pOfcFlowMatch->FldValue.au1Fld, OFC_ONE);
                SNPRINTF (au1TempBuff, sizeof (au1TempBuff), "%u", u1TempVar);
                SNPRINTF (pStr + STRLEN (pStr), u4StrLen - STRLEN (pStr),
                          "%s", au1TempBuff);
                break;

            case OFCXMT_OFB_MPLS_BOS:
                SNPRINTF (pStr + STRLEN (pStr), u4StrLen - STRLEN (pStr),
                          "%s", "MplsBOS=");
                MEMSET (au1TempBuff, OFC_ZERO, sizeof (au1TempBuff));
                u1TempVar = OFC_ZERO;
                MEMCPY (&u1TempVar, pOfcFlowMatch->FldValue.au1Fld, OFC_ONE);
                SNPRINTF (au1TempBuff, sizeof (au1TempBuff), "%u", u1TempVar);
                SNPRINTF (pStr + STRLEN (pStr), u4StrLen - STRLEN (pStr),
                          "%s", au1TempBuff);
                break;

            case OFCXMT_OFB_PBB_ISID:
                SNPRINTF (pStr + STRLEN (pStr), u4StrLen - STRLEN (pStr),
                          "%s", "PBBId=");
                MEMSET (au1TempBuff, OFC_ZERO, sizeof (au1TempBuff));
                u4TempVar = OFC_ZERO;
                MEMCPY (&u4TempVar, pOfcFlowMatch->FldValue.au1Fld, OFC_FOUR);
                u4TempVar = OSIX_NTOHL (u4TempVar);
                SNPRINTF (au1TempBuff, sizeof (au1TempBuff), "%u", u4TempVar);
                SNPRINTF (pStr + STRLEN (pStr), u4StrLen - STRLEN (pStr),
                          "%s", au1TempBuff);
                break;

            case OFCXMT_OFB_TUNNEL_ID:
                SNPRINTF (pStr + STRLEN (pStr), u4StrLen - STRLEN (pStr),
                          "%s", "TunlId=");
                MEMSET (au1TempBuff, OFC_ZERO, sizeof (au1TempBuff));
                MEMCPY (&u8TempVar, pOfcFlowMatch->FldValue.au1Fld, OFC_EIGHT);
                u8TempVar = Ofcinputhton64 (&u8TempVar);
                SNPRINTF (au1TempBuff, sizeof (au1TempBuff), "%u", u8TempVar);
                SNPRINTF (pStr + STRLEN (pStr), u4StrLen - STRLEN (pStr),
                          "%s", au1TempBuff);
                break;

            case OFCXMT_OFB_IPV6_EXTHDR:
                SNPRINTF (pStr + STRLEN (pStr), u4StrLen - STRLEN (pStr),
                          "%s", "IP6Ex=");
                MEMSET (au1TempBuff, OFC_ZERO, sizeof (au1TempBuff));
                u2TempVar = OFC_ZERO;
                MEMCPY (&u2TempVar, pOfcFlowMatch->FldValue.au1Fld, OFC_TWO);
                u2TempVar = OSIX_NTOHS (u2TempVar);
                SNPRINTF (au1TempBuff, sizeof (au1TempBuff), "%u", u2TempVar);
                SNPRINTF (pStr + STRLEN (pStr), sizeof (au1TempBuff),
                          "%s", au1TempBuff);
                break;

            default:
                OfpErrorMsg.u2Type = OFPET_131_BAD_ACTION;
                OfpErrorMsg.u2Code = OFPBAC_BAD_TYPE;
                OfcErrMsgPktSend (&OfpErrorMsg, NULL);
                return OFC_FAILURE;
        }
        pNext = TMO_SLL_Next (pMatchList, (tOfcSllNode *) pOfcFlowMatch);
        if (NULL != pNext)
        {
            SNPRINTF (pStr + STRLEN (pStr), u4StrLen - STRLEN (pStr), "%s",
                      ", ");
        }
    }
    OFC_TRC_FUNC ((OFC_FN_EXIT, "FUNC:Ofc131MatchFieldsStructToStr\n"));
    return OFC_SUCCESS;
}

/*****************************************************************************
 * Function    :  Ofc131GroupActionStructTostr 
 * Description :  This routine parses the Actions in Group Bucket into String
 * Input       :  pBucketList - Pointer to Bucket List
 * Output      :  pStr        - Pointer to the Actions Display String 
 * Returns     :  OFC_SUCCESS/OFC_FAILURE
 *****************************************************************************/
PUBLIC INT4
Ofc131GroupActionStructToStr (char *pStr, tOfcSll * pBucketList)
{
    tOfcGroupBucket    *pOfcGroupBucket = NULL;
    INT4                i4RetValue = OFC_ZERO;

    OFC_TRC_FUNC ((OFC_FN_ENTRY, "FUNC:Ofc131GroupActionStructToStr\n"));

    TMO_SLL_Scan (pBucketList, pOfcGroupBucket, tOfcGroupBucket *)
    {
        i4RetValue = Ofc131ActionsStructTostr (pStr, &pOfcGroupBucket->ActList);
        if (i4RetValue == OFC_FAILURE)
        {
            return OFC_FAILURE;
        }
    }
    OFC_TRC_FUNC ((OFC_FN_EXIT, "FUNC:Ofc131GroupActionStructToStr\n"));
    return OFC_SUCCESS;
}

/*****************************************************************************
 * Function    :  Ofc131MeterBandListToStr 
 * Description :  This routine parses the Meter Actions into String
 * Input       :  pBandList - Pointer to Meter Band List
 * Output      :  pStr      - Pointer to the Meter Actions Display String 
 * Returns     :  OFC_SUCCESS/OFC_FAILURE
 *****************************************************************************/
PUBLIC INT4
Ofc131MeterBandListToStr (char *pStr, tOfcSll * pBandList)
{
    tOfcMeterBand      *pOfcMeterBand = NULL;
    tTMO_SLL_NODE      *pNext = NULL;
    UINT4               u4StrLen = OFC_MAX_ACTION_STR_LEN;
    CHR1                au1TempBuff[OFC_FIVE_TWELVE];

    OFC_TRC_FUNC ((OFC_FN_ENTRY, "FUNC:Ofc131MeterBandListToStr\n"));

    TMO_SLL_Scan (pBandList, pOfcMeterBand, tOfcMeterBand *)
    {
        switch (pOfcMeterBand->u2Type)
        {
            case OFPMBT_DROP:
                SNPRINTF (pStr + STRLEN (pStr), u4StrLen - STRLEN (pStr),
                          "%s", "DROPBand:");
                MEMSET (au1TempBuff, OFC_ZERO, sizeof (au1TempBuff));
                SNPRINTF (pStr + STRLEN (pStr), u4StrLen - STRLEN (pStr),
                          "%s", "Rate:");
                SNPRINTF (au1TempBuff, sizeof (au1TempBuff),
                          "%u", pOfcMeterBand->u4Rate);
                SNPRINTF (pStr + STRLEN (pStr), u4StrLen - STRLEN (pStr),
                          "%s", au1TempBuff);
                SNPRINTF (pStr + STRLEN (pStr), u4StrLen - STRLEN (pStr),
                          "%s", ",");
                SNPRINTF (pStr + STRLEN (pStr), u4StrLen - STRLEN (pStr),
                          "%s", "BurstSize=");
                MEMSET (au1TempBuff, OFC_ZERO, sizeof (au1TempBuff));
                SNPRINTF (au1TempBuff, sizeof (au1TempBuff),
                          "%u", pOfcMeterBand->u4BurstSize);
                SNPRINTF (pStr + STRLEN (pStr), u4StrLen - STRLEN (pStr),
                          "%s", au1TempBuff);
                break;

            case OFPMBT_DSCP_REMARK:
                SNPRINTF (pStr + STRLEN (pStr), u4StrLen - STRLEN (pStr),
                          "%s", "DSCPBand:");
                MEMSET (au1TempBuff, OFC_ZERO, sizeof (au1TempBuff));
                SNPRINTF (pStr + STRLEN (pStr), u4StrLen - STRLEN (pStr),
                          "%s", "Rate:");
                SNPRINTF (au1TempBuff, sizeof (au1TempBuff), "%u",
                          pOfcMeterBand->u4Rate);
                SNPRINTF (pStr + STRLEN (pStr), u4StrLen - STRLEN (pStr),
                          "%s", au1TempBuff);
                SNPRINTF (pStr + STRLEN (pStr), u4StrLen - STRLEN (pStr),
                          "%s", ",");
                MEMSET (au1TempBuff, OFC_ZERO, sizeof (au1TempBuff));
                SNPRINTF (pStr + STRLEN (pStr), u4StrLen - STRLEN (pStr),
                          "%s", "BurstSize=");
                SNPRINTF (au1TempBuff, sizeof (au1TempBuff),
                          "%u", pOfcMeterBand->u4BurstSize);
                SNPRINTF (pStr + STRLEN (pStr), u4StrLen - STRLEN (pStr),
                          "%s", au1TempBuff);
                SNPRINTF (pStr + STRLEN (pStr), u4StrLen - STRLEN (pStr),
                          "%s", ",");
                MEMSET (au1TempBuff, OFC_ZERO, sizeof (au1TempBuff));
                SNPRINTF (pStr + STRLEN (pStr), u4StrLen - STRLEN (pStr),
                          "%s", "PrecLevel=");
                SNPRINTF (au1TempBuff, sizeof (au1TempBuff),
                          "%u", pOfcMeterBand->BandFlds.u1PrecLvl);
                SNPRINTF (pStr + STRLEN (pStr), u4StrLen - STRLEN (pStr),
                          "%s", au1TempBuff);
                break;

            case OFPMBT_EXPERIMENTER:
                SNPRINTF (pStr + STRLEN (pStr), u4StrLen - STRLEN (pStr),
                          "%s", "EXPRBand:");
                MEMSET (au1TempBuff, OFC_ZERO, sizeof (au1TempBuff));
                SNPRINTF (pStr + STRLEN (pStr), u4StrLen - STRLEN (pStr),
                          "%s", "Rate=");
                SNPRINTF (au1TempBuff, sizeof (au1TempBuff),
                          "%u", pOfcMeterBand->u4Rate);
                SNPRINTF (pStr + STRLEN (pStr), u4StrLen - STRLEN (pStr),
                          "%s", au1TempBuff);
                SNPRINTF (pStr + STRLEN (pStr), u4StrLen - STRLEN (pStr),
                          "%s", ",");

                MEMSET (au1TempBuff, OFC_ZERO, sizeof (au1TempBuff));
                SNPRINTF (pStr + STRLEN (pStr), u4StrLen - STRLEN (pStr),
                          "%s", "BurstSize=");
                SNPRINTF (au1TempBuff, sizeof (au1TempBuff),
                          "%u", pOfcMeterBand->u4BurstSize);
                SNPRINTF (pStr + STRLEN (pStr), u4StrLen - STRLEN (pStr),
                          "%s", au1TempBuff);
                SNPRINTF (pStr + STRLEN (pStr), u4StrLen - STRLEN (pStr),
                          "%s", ",");

                MEMSET (au1TempBuff, OFC_ZERO, sizeof (au1TempBuff));
                SNPRINTF (pStr + STRLEN (pStr), u4StrLen - STRLEN (pStr),
                          "%s", "Experimenter=");
                SNPRINTF (au1TempBuff, sizeof (au1TempBuff),
                          "%u", pOfcMeterBand->BandFlds.u4Experimenter);
                SNPRINTF (pStr + STRLEN (pStr), u4StrLen - STRLEN (pStr),
                          "%s", au1TempBuff);
                break;

            default:
                OFC_TRC_FUNC ((OFC_UTIL_TRC, "Exiting with Default case\n"));
                return OFC_FAILURE;
        }

        pNext = TMO_SLL_Next (pBandList, (tOfcSllNode *) pOfcMeterBand);
        if (NULL != pNext)
        {
            SNPRINTF (pStr + STRLEN (pStr), u4StrLen - STRLEN (pStr), "%s",
                      ",");
        }
    }
    OFC_TRC_FUNC ((OFC_FN_EXIT, "FUNC:Ofc131MeterBandListToStr\n"));
    return OFC_SUCCESS;
}

/*****************************************************************************
 * Function    :  OfcFlowMatchToStr 
 * Description :  This routine parses Flow Match Fields into String
 * Input       :  pOfcFlowMatch - Pointer to Flow Match Fields
 * Output      :  pStr          - Pointer to the Meter Actions Display String 
 * Returns     :  OFC_SUCCESS/OFC_FAILURE
 *****************************************************************************/
PUBLIC UINT4
OfcFlowMatchToStr (CHR1 * pStr, tOfcFlowMatch * pOfcFlowMatch)
{

    UINT4               u4StrLen = OFC_ACTION_STRING_MAX_LEN;
    CHR1                au1TempBuff[OFC_TWO_FIFTY_SIX];

    OFC_TRC_FUNC ((OFC_FN_ENTRY, "FUNC:OfcFlowMatchToStr\n"));

    MEMSET (au1TempBuff, OFC_ZERO, sizeof (au1TempBuff));
    STRNCAT (pStr, "WildCard=", OFC_NINE);
    SNPRINTF (au1TempBuff, sizeof (au1TempBuff),
              "%d", pOfcFlowMatch->u4WildCard);
    SNPRINTF (pStr + STRLEN (pStr), u4StrLen - STRLEN (pStr),
              "%s", au1TempBuff);
    STRNCAT (pStr, ",", OFC_ONE);

    MEMSET (au1TempBuff, OFC_ZERO, sizeof (au1TempBuff));
    STRNCAT (pStr, "InputPort=", OFC_TEN);
    SNPRINTF (au1TempBuff, sizeof (au1TempBuff),
              "%d", pOfcFlowMatch->u4InIfIndex);
    SNPRINTF (pStr + STRLEN (pStr), u4StrLen - STRLEN (pStr),
              "%s", au1TempBuff);
    STRNCAT (pStr, ",", OFC_ONE);

    MEMSET (au1TempBuff, OFC_ZERO, sizeof (au1TempBuff));
    STRNCAT (pStr, "SrcMAC=", OFC_SEVEN);
    MEMSET (au1TempBuff, OFC_ZERO, sizeof (au1TempBuff));
    CliMacToStr (pOfcFlowMatch->au1SrcMacAddr, (UINT1 *) (&(au1TempBuff)));
    if (OFC_THREE < STRLEN (au1TempBuff))
    {
        au1TempBuff[STRLEN (au1TempBuff) - OFC_THREE] = '\0';
    }
    SNPRINTF (pStr + STRLEN (pStr), u4StrLen - STRLEN (pStr),
              "%s", au1TempBuff);
    STRNCAT (pStr, ",", OFC_ONE);

    MEMSET (au1TempBuff, OFC_ZERO, sizeof (au1TempBuff));
    STRNCAT (pStr, "DstMAC=", OFC_SEVEN);
    MEMSET (au1TempBuff, OFC_ZERO, sizeof (au1TempBuff));
    CliMacToStr (pOfcFlowMatch->au1DstMacAddr, (UINT1 *) (&(au1TempBuff)));
    if (OFC_THREE < STRLEN (au1TempBuff))
    {
        au1TempBuff[STRLEN (au1TempBuff) - OFC_THREE] = '\0';
    }
    SNPRINTF (pStr + STRLEN (pStr), u4StrLen - STRLEN (pStr),
              "%s", au1TempBuff);
    STRNCAT (pStr, ",", OFC_ONE);

    MEMSET (au1TempBuff, OFC_ZERO, sizeof (au1TempBuff));
    STRNCAT (pStr, "Ethertype=", OFC_ELEVEN);
    SNPRINTF (au1TempBuff, sizeof (au1TempBuff),
              "%d", pOfcFlowMatch->u2EthType);
    SNPRINTF (pStr + STRLEN (pStr), u4StrLen - STRLEN (pStr),
              "%s", au1TempBuff);
    STRNCAT (pStr, ",", OFC_ONE);

    MEMSET (au1TempBuff, OFC_ZERO, sizeof (au1TempBuff));
    STRNCAT (pStr, "IpProtocol=", OFC_ELEVEN);
    SNPRINTF (au1TempBuff, sizeof (au1TempBuff),
              "%d", pOfcFlowMatch->u1IpProto);
    SNPRINTF (pStr + STRLEN (pStr), u4StrLen - STRLEN (pStr),
              "%s", au1TempBuff);
    STRNCAT (pStr, ",", OFC_ONE);

    MEMSET (au1TempBuff, OFC_ZERO, sizeof (au1TempBuff));
    STRNCAT (pStr, "SrcIPv4=", OFC_EIGHT);
    SNPRINTF (au1TempBuff, sizeof (au1TempBuff), "%d", pOfcFlowMatch->u4IpSrc);
    SNPRINTF (pStr + STRLEN (pStr), u4StrLen - STRLEN (pStr), "%s",
              au1TempBuff);
    STRNCAT (pStr, ",", OFC_ONE);

    MEMSET (au1TempBuff, OFC_ZERO, sizeof (au1TempBuff));
    STRNCAT (pStr, "DstIpv4=", OFC_EIGHT);
    SNPRINTF (au1TempBuff, sizeof (au1TempBuff), "%d", pOfcFlowMatch->u4IpDst);
    SNPRINTF (pStr + STRLEN (pStr), u4StrLen - STRLEN (pStr), "%s",
              au1TempBuff);
    STRNCAT (pStr, ",", OFC_ONE);

    MEMSET (au1TempBuff, OFC_ZERO, sizeof (au1TempBuff));
    STRNCAT (pStr, "SrcIpv6=", OFC_EIGHT);
    SNPRINTF (pStr + STRLEN (pStr), u4StrLen - STRLEN (pStr),
              "%s", pOfcFlowMatch->au1Ip6Src);
    STRNCAT (pStr, ",", OFC_ONE);

    MEMSET (au1TempBuff, OFC_ZERO, sizeof (au1TempBuff));
    STRNCAT (pStr, "DstIpv6=", OFC_EIGHT);
    SNPRINTF (pStr + STRLEN (pStr), u4StrLen - STRLEN (pStr),
              "%s", pOfcFlowMatch->au1Ip6Dst);
    STRNCAT (pStr, ",", OFC_ONE);

    MEMSET (au1TempBuff, OFC_ZERO, sizeof (au1TempBuff));
    STRNCAT (pStr, "TpSrcPort=", OFC_TEN);
    SNPRINTF (au1TempBuff, sizeof (au1TempBuff),
              "%d", pOfcFlowMatch->u2TpSrcPort);
    SNPRINTF (pStr + STRLEN (pStr), u4StrLen - STRLEN (pStr),
              "%s", au1TempBuff);
    STRNCAT (pStr, ",", OFC_ONE);

    MEMSET (au1TempBuff, OFC_ZERO, sizeof (au1TempBuff));
    STRNCAT (pStr, "TPDstPort=", OFC_TEN);
    SNPRINTF (au1TempBuff, sizeof (au1TempBuff), "%d",
              pOfcFlowMatch->u2TpDstPort);
    SNPRINTF (pStr + STRLEN (pStr), u4StrLen - STRLEN (pStr),
              "%s", au1TempBuff);
    STRNCAT (pStr, ",", OFC_ONE);

    OFC_TRC_FUNC ((OFC_FN_EXIT, "FUNC:OfcFlowMatchToStr\n"));
    return OFC_SUCCESS;
}
