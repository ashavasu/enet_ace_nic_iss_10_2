/*****************************************************************************/
/* Copyright (C) 2013 Aricent Inc . All Rights Reserved
 ******************************************************************************
 *    FILE  NAME             : ofcflow.c
 *    PRINCIPAL AUTHOR       : Aricent Inc.
 *    SUBSYSTEM NAME         : OFC
 *    MODULE NAME            : OFC-FLOWTABLE
 *    LANGUAGE               : ANSI-C
 *    TARGET ENVIRONMENT     : Linux (Portable)
 *    DATE OF FIRST RELEASE  :
 *    DESCRIPTION            : This file contains routines for flow table 
 *                             implementations.
 *
* $Id: ofcflow.c,v 1.4 2014/08/14 11:11:23 siva Exp $
 *---------------------------------------------------------------------------*/
#include "ofcflow.h"
#include "ofcacts.h"
#include "ofcapi.h"
#include "ofcsz.h"
#include "cfa.h"
#include "ofcidxsz.h"
#include "ofcidxgl.h"
#ifdef NPAPI_WANTED
#include"ofcnp.h"
#endif

/*****************************************************************************/
/************************* GLOBAL/EXTERN DECLARATIONS ************************/
/*****************************************************************************/
static UINT4        gu4FlowIndex;

/*****************************************************************************/
/********************** PRIVATE FUNCTION DECLARATIONS ************************/
/*****************************************************************************/
/* OFC Add Flow Entry Functions */
PRIVATE INT4        OfcFlowAddFlow (tOfcFsofcCfgEntry * pOfc,
                                    tOfcFlowEntry * pFlow);
PRIVATE INT4        OfcFlowAddFlowEntry (tOfcFsofcCfgEntry * pOfc,
                                         tOfcFlowEntry * pFlow);

/* OFC Modify Flow Entry Functions */
PRIVATE INT4        OfcFlowModifyFlow (tOfcFsofcCfgEntry * pOfc,
                                       tOfcFlowEntry * pFlowMsg, UINT1 u1Cmd);
PRIVATE INT4        OfcFlowModifyFlowEntry (tOfcFsofcCfgEntry * pOfc,
                                            tOfcFlowEntry * pFlow);

/* OFC Delete Flow Entry Functions */
PRIVATE INT4        OfcFlowDeleteFlowEntry (tOfcFsofcCfgEntry * pOfc,
                                            tOfcFlowEntry * pFlow);

/* Conversion Functions/ Utility Functions */
PRIVATE VOID        OfcFlowModMsgToFlowEntry (tOfcFlowEntry * pFlowMsg,
                                              tOfcFlowEntry * pFlow);
PRIVATE UINT4       OfcWildCardToNetMask (UINT4 u4WildCard);

/*****************************************************************************/
/************************* FUNCTION DECLARATIONS *****************************/
/*****************************************************************************/

/*****************************************************************************/
/* Function Name : OfcFlowMatchToKey                                         */
/* Description   : This function extracts matching fields from FlowModMsg to */
/*                 tOfcFlowMatch Structure-object                            */
/* Input(s)      : *pOfcMatch - MatchingKey from the FlowModMsg              */
/* Output(s)     : *pKey - MatchingKey from the FlowEntry Object             */
/* Return(s)     : None                                                      */
/*****************************************************************************/

PUBLIC VOID
OfcFlowMatchToKey (tOfpMatch * pOfpMatch, tOfcFlowMatch * pKey)
{
    UINT4               u4WildCard = OFC_ZERO;

    OFC_TRC_FUNC ((OFC_FN_ENTRY, "FUNC:OfcFlowMatchToKey\n"));

    u4WildCard = pKey->u4WildCard = OSIX_NTOHL (pOfpMatch->u4WildCard);

    if (!(u4WildCard & OFPFW_IN_PORT))
    {
        pKey->u4InIfIndex = (UINT4) OSIX_NTOHS (pOfpMatch->u2InPort);
    }
    if (!(u4WildCard & OFPFW_DL_SRC))
    {
        MEMCPY (&(pKey->au1SrcMacAddr), &(pOfpMatch->au1DlSrc),
                sizeof (pKey->au1SrcMacAddr));
    }
    if (!(u4WildCard & OFPFW_DL_DST))
    {
        MEMCPY (&(pKey->au1DstMacAddr), &(pOfpMatch->au1DlDst),
                sizeof (pKey->au1DstMacAddr));
    }
    if (!(u4WildCard & OFPFW_DL_TYPE))
    {
        pKey->u2EthType = OSIX_NTOHS (pOfpMatch->u2DlType);
    }
    if (!(u4WildCard & OFPFW_NW_PROTO))
    {
        MEMCPY (&(pKey->u1IpProto), &(pOfpMatch->u1NwProto),
                sizeof (pKey->u1IpProto));
    }
    MEMSET (&(pKey->au1Ip6Src), OFC_ZERO, sizeof (pKey->au1Ip6Src));
    MEMSET (&(pKey->au1Ip6Dst), OFC_ZERO, sizeof (pKey->au1Ip6Dst));

    pKey->u4IpSrc = OSIX_NTOHL (pOfpMatch->u4NwSrc);
    pKey->u4IpDst = OSIX_NTOHL (pOfpMatch->u4NwDst);
    pKey->u4IpSrc &= OfcWildCardToNetMask (u4WildCard >> OFPFW_NW_SRC_SHIFT);
    pKey->u4IpDst &= OfcWildCardToNetMask (u4WildCard >> OFPFW_NW_DST_SHIFT);

    if (!(u4WildCard & OFPFW_TP_SRC))
    {
        pKey->u2TpSrcPort = OSIX_NTOHS (pOfpMatch->u4TpSrc);
    }
    if (!(u4WildCard & OFPFW_TP_DST))
    {
        pKey->u2TpDstPort = OSIX_NTOHS (pOfpMatch->u4TpDst);
    }

    OFC_TRC_FUNC ((OFC_FN_EXIT, "FUNC:OfcFlowMatchToKey\n"));
}

/*****************************************************************************/
/* Function Name : OfcKeyToFlowMatch                                         */
/* Description   : This function extracts matching fields from Key and copies*/
/*                 to tOfpMatch Structure-object                             */
/* Input(s)      : *pOfcMatch - MatchingKey from the FlowModMsg              */
/* Output(s)     : *pKey - MatchingKey from the FlowEntry Object             */
/* Return(s)     : None                                                      */
/*****************************************************************************/

PUBLIC VOID
OfcKeyToFlowMatch (tOfcFlowMatch * pKey, tOfpMatch * pOfpMatch)
{
    UINT4               u4WildCard = OFC_ZERO;
    OFC_TRC_FUNC ((OFC_FN_ENTRY, "FUNC:OfcKeyToFlowMatch\n"));

    u4WildCard = pOfpMatch->u4WildCard = OSIX_HTONL (pKey->u4WildCard);

    if (!(u4WildCard & OFPFW_IN_PORT))
    {
        pOfpMatch->u2InPort = OSIX_HTONS ((UINT2) pKey->u4InIfIndex);
    }

    if (!(u4WildCard & OFPFW_DL_SRC))
    {
        MEMCPY (&(pOfpMatch->au1DlSrc), &(pKey->au1SrcMacAddr),
                sizeof (pKey->au1SrcMacAddr));
    }
    if (!(u4WildCard & OFPFW_DL_DST))
    {
        MEMCPY (&(pOfpMatch->au1DlDst), &(pKey->au1DstMacAddr),
                sizeof (pKey->au1DstMacAddr));
    }
    if (!(u4WildCard & OFPFW_DL_VLAN))
    {
        pOfpMatch->u2DlVlan = OFC_ZERO;
    }
    if (!(u4WildCard & OFPFW_DL_VLAN_PCP))
    {
        pOfpMatch->u1DlVlanPcap = OFC_ZERO;
    }
    if (!(u4WildCard & OFPFW_DL_TYPE))
    {
        pOfpMatch->u2DlType = OSIX_HTONS (pKey->u2EthType);
    }
    if (!(u4WildCard & OFPFW_NW_TOS))
    {
        pOfpMatch->u1NwTos = OFC_ZERO;
    }
    if (!(u4WildCard & OFPFW_NW_PROTO))
    {
        pOfpMatch->u1NwProto = pKey->u1IpProto;
    }
    pOfpMatch->u4NwSrc = pKey->u4IpSrc;
    pOfpMatch->u4NwDst = pKey->u4IpDst;
    pOfpMatch->u4NwSrc &=
        OfcWildCardToNetMask (u4WildCard >> OFPFW_NW_SRC_SHIFT);
    pOfpMatch->u4NwDst &=
        OfcWildCardToNetMask (u4WildCard >> OFPFW_NW_DST_SHIFT);
    pOfpMatch->u4NwSrc = OSIX_HTONL (pKey->u4IpSrc);
    pOfpMatch->u4NwDst = OSIX_HTONL (pKey->u4IpDst);

    if (!(u4WildCard & OFPFW_TP_SRC))
    {
        pOfpMatch->u4TpSrc = OSIX_HTONS (pKey->u2TpSrcPort);
    }
    if (!(u4WildCard & OFPFW_TP_DST))
    {
        pOfpMatch->u4TpDst = OSIX_HTONS (pKey->u2TpDstPort);
    }
    OFC_TRC_FUNC ((OFC_FN_EXIT, "FUNC:OfcKeyToFlowMatch\n"));
}

/*****************************************************************************/
/* Function Name : OfcFlowModMsgToFlowEntry                                  */
/* Description   : This function extracts flow entry fields from FlowModMsg  */
/*                 to tOfcFlowEntry Structure-object                         */
/* Input(s)      : *pFlowMsg - Incoming FlowMsg from controller              */
/* Output(s)     : *pFlow - Flow Entry Object                                */
/* Return(s)     : None                                                      */
/*****************************************************************************/

PRIVATE VOID
OfcFlowModMsgToFlowEntry (tOfcFlowEntry * pFlowMsg, tOfcFlowEntry * pFlow)
{
    OFC_TRC_FUNC ((OFC_FN_ENTRY, "FUNC:OfcFlowModMsgToFlowEntry\n"));

    pFlow->u8Cookie = pFlowMsg->u8Cookie;
    pFlow->u2Flags = pFlowMsg->u2Flags;
    pFlow->u2IdleTimeout = pFlowMsg->u2IdleTimeout;
    pFlow->u2HardTimeout = pFlowMsg->u2HardTimeout;
    pFlow->u2Priority = pFlowMsg->u2Priority;
    pFlow->u2Command = pFlowMsg->u2Command;
    pFlow->u4OutPort = pFlowMsg->u4OutPort;
    pFlow->u4BufId = pFlowMsg->u4BufId;

    OFC_TRC_FUNC ((OFC_FN_EXIT, "FUNC:OfcFlowModMsgToFlowEntry\n"));
}

PRIVATE UINT4
OfcWildCardToNetMask (UINT4 u4WildCard)
{
    u4WildCard &= 0x3f;
    return u4WildCard <
        OFC_THIRTY_TWO ? OSIX_HTONL (~((1u << u4WildCard) - OFC_ONE)) :
        OFC_ZERO;
}

PRIVATE VOID
OfcApplyWildcardOnKey (tOfcFlowMatch * pKey, UINT4 u4WildCard,
                       tOfcFlowMatch * pOutKey)
{
    MEMCPY (pOutKey, pKey, sizeof (tOfcFlowMatch));
    if (u4WildCard & OFPFW_IN_PORT)
    {
        pOutKey->u4InIfIndex = OFC_ZERO;
    }
    if (u4WildCard & OFPFW_DL_SRC)
    {
        MEMSET (pOutKey->au1SrcMacAddr, OFC_ZERO, OFC_SIX);
    }
    if (u4WildCard & OFPFW_DL_DST)
    {
        MEMSET (pOutKey->au1DstMacAddr, OFC_ZERO, OFC_SIX);
    }
    if (u4WildCard & OFPFW_DL_TYPE)
    {
        pOutKey->u2EthType = OFC_ZERO;
    }

    if (u4WildCard & OFPFW_NW_PROTO)
    {
        pOutKey->u1IpProto = OFC_ZERO;
    }

    if (u4WildCard & OFPFW_TP_SRC)
    {
        pOutKey->u2TpSrcPort = OFC_ZERO;
    }

    if (u4WildCard & OFPFW_TP_DST)
    {
        pOutKey->u2TpDstPort = OFC_ZERO;
    }

    pOutKey->u4IpSrc &= OfcWildCardToNetMask (u4WildCard >> OFPFW_NW_SRC_SHIFT);
    pOutKey->u4IpDst &= OfcWildCardToNetMask (u4WildCard >> OFPFW_NW_DST_SHIFT);
    pOutKey->u4WildCard = u4WildCard;
}

/*****************************************************************************/
/* Function Name : OfcFlowLookup                                             */
/* Description   : Look up function to hash table which finds an mapping     */
/*                 flow entry from pKey                                      */
/* Input(s)      : *pOfc - OFC Client Instance                               */
/*                 *pKey - Matching fields                                   */
/* Output(s)     : None                                                      */
/* Return(s)     : Pointer to Matching Flow entry                            */
/*****************************************************************************/

tOfcFlowEntry      *
OfcFlowLookup (tOfcFsofcCfgEntry * pOfc, tOfcFlowMatch * pKey)
{
    tOfcFlowEntry      *pFlow = NULL;
    UINT4               u4HashIndex = OFC_ZERO;
    tOfcFlowMatch       Wildcardkey;

    OFC_TRC_FUNC ((OFC_FN_ENTRY, "FUNC:OfcFlowLookup\n"));

    MEMSET (&Wildcardkey, OFC_ZERO, sizeof (tOfcFlowMatch));

    TMO_HASH_Scan_Table (pOfc->pFlowtable, u4HashIndex)
    {
        TMO_HASH_Scan_Bucket (pOfc->pFlowtable, u4HashIndex, pFlow,
                              tOfcFlowEntry *)
        {
            MEMSET (&Wildcardkey, OFC_ZERO, sizeof (tOfcFlowMatch));
            OfcApplyWildcardOnKey (pKey, pFlow->key.u4WildCard, &Wildcardkey);
            if (memcmp (&(pFlow->key), &Wildcardkey, sizeof (tOfcFlowMatch)) ==
                OFC_ZERO)
            {
                return pFlow;
            }
        }
    }

    OFC_TRC_FUNC ((OFC_FN_EXIT, "FUNC:OfcFlowLookup\n"));
    return pFlow;
}

/*****************************************************************************/
/* Function Name : OfcFlowHandleFlowModMsgs                                  */
/* Description   : Function to handle the FlowModify request messages from   */
/*                 Packet Processing Module                                  */
/* Input(s)      : *pOfc - Open Flow Client Instance                         */
/*                 *pFlowMsg - Incoming FlowMsg from controller              */
/* Output(s)     : None                                                      */
/* Return(s)     : OFC_FAILURE/OFC_SUCCESS                                   */
/*****************************************************************************/

INT4
OfcFlowHandleFlowModMsgs (tOfcFsofcCfgEntry * pOfc, tOfcFlowEntry * pFlowMsg)
{
    tOfpErrMsg          OfpErrorMsg;

    OFC_TRC_FUNC ((OFC_FN_ENTRY, "FUNC:OfcFlowHandleFlowModMsgs\n"));
    MEMSET (&OfpErrorMsg, OFC_ZERO, sizeof (tOfpErrMsg));

    switch (pFlowMsg->u2Command)
    {
        case OFPFC_ADD:
            OfcFlowAddFlow (pOfc, pFlowMsg);
            break;
        case OFPFC_MODIFY:
            OfcFlowModifyFlow (pOfc, pFlowMsg, OFPFC_MODIFY);
            break;
        case OFPFC_MODIFY_STRICT:
            OfcFlowModifyFlow (pOfc, pFlowMsg, OFPFC_MODIFY_STRICT);
            break;
        case OFPFC_DELETE:
            OfcFlowDeleteFlow (pOfc, pFlowMsg, OFPFC_DELETE, OFPRR_DELETE);
            break;
        case OFPFC_DELETE_STRICT:
            OfcFlowDeleteFlow (pOfc, pFlowMsg, OFPFC_DELETE_STRICT,
                               OFPRR_DELETE);
            break;
        default:
            OFC_TRC_FUNC ((OFC_FLOWTBL_TRC, "FUNC:OfcFlowHandleFlowModMsgs: "
                           "Default Case, Nothing ToDo, Returning Failure\n"));
            OfpErrorMsg.u2Type = OFPET_FLOW_MOD_FAILED;
            OfpErrorMsg.u2Code = OFPFMFC_BAD_COMMAND;

            /* Calling Pkt TX/RX API */
            OFC_TRC_FUNC ((OFC_FLOWTBL_TRC,
                           "FUNC:OfcFlowHandleFlowModMsgs: Sending Reply to Controller through Pkt Tx/Rx API\n"));
            OfcErrMsgPktSend (&OfpErrorMsg, NULL);

            OFC_TRC_FUNC ((OFC_FLOWTBL_TRC,
                           "FUNC:OfcFlowHandleFlowModMsgs: Send Reply to Controller Success, through Pkt Tx/Rx API\n"));
            return OFC_FAILURE;
    }

    OFC_TRC_FUNC ((OFC_FN_ENTRY, "FUNC:OfcHandleFlowModMsgs\n"));
    return OFC_SUCCESS;
}

/*****************************************************************************/
/* Function Name : OfcFlowAddFlow                                            */
/* Description   : Function to handle the AddFlowInfo request from Packet    */
/*                 Proccessing module                                        */
/* Input(s)      : *pOfc - Open Flow Client Instance                         */
/*                 *pFlowMsg - Incoming FlowMsg from controller              */
/* Output(s)     : None                                                      */
/* Return(s)     : OFC_FAILURE/OFC_SUCCESS                                   */
/*****************************************************************************/
PRIVATE INT4
OfcFlowAddFlow (tOfcFsofcCfgEntry * pOfc, tOfcFlowEntry * pFlowMsg)
{
    tOfcFlowEntry      *pFlow = NULL;
    tOfpErrMsg          OfpErrorMsg;
    UINT4               u4HashIndex = OFC_ZERO;
    UINT1               u1Flag = OFC_NO_RESET;
    UINT4               u4FlowIndex = OFC_ZERO;
    UINT4               u4Used = OFC_ZERO;
    UINT4               u4PktCount = OFC_ZERO;
    UINT4               u4ByteCount = OFC_ZERO;
    INT4                i4RetValue = OFC_ZERO;

#ifdef NPAPI_WANTED
    tOfcHwInfo          OfcHwInfo;
    tOfcActOutPort     *pOutPort = NULL;
    tOfcFsofcIfEntry   *pOfcFsofcIfEntry = NULL;
#endif

    OFC_TRC_FUNC ((OFC_FN_ENTRY, "FUNC:OfcFlowAddFlow\n"));

#ifdef NPAPI_WANTED
    MEMSET (&OfcHwInfo, OFC_ZERO, sizeof (tOfcHwInfo));
#endif
    MEMSET (&OfpErrorMsg, OFC_ZERO, sizeof (tOfpErrMsg));

    pFlow = OfcFlowLookup (pOfc, &pFlowMsg->key);
    if (pFlow != NULL)
    {
        if (pFlowMsg->u2Flags & OFPFF_CHECK_OVERLAP)
        {
            OFC_TRC_FUNC ((OFC_FLOWTBL_TRC,
                           "FUNC:OfcFlowAddFlow: Add Failed due to OVERLAP Restrict Check\n"));

            OfpErrorMsg.u2Type = OFPET_FLOW_MOD_FAILED;
            OfpErrorMsg.u2Code = OFPFMFC_OVERLAP;

            /* Calling Pkt TX/RX API */
            OFC_TRC_FUNC ((OFC_FLOWTBL_TRC,
                           "FUNC:OfcFlowAddFlow: Sending Error Message to Controller through Pkt Tx/Rx API\n"));
            i4RetValue = OfcErrMsgPktSend (&OfpErrorMsg, NULL);

            OFC_TRC_FUNC ((OFC_FLOWTBL_TRC,
                           "FUNC:OfcFlowAddFlow:Send Error Message to Controller Success, through Pkt Tx/Rx API\n"));

            return OFC_FAILURE;
        }
        else
        {
            u4FlowIndex = pFlow->u4FlowIndex;
            if (pFlowMsg->u2Flags & OFPFF_RESET_COUNTS)
            {
                pFlow->u1TableId = OFC_ZERO;
                if (OfcFlowDeleteFlow
                    (pOfc, pFlow, OFPFC_DELETE_STRICT,
                     OFPRR_ERROR_DELETE) != OFC_SUCCESS)
                {
                    OFC_TRC_FUNC ((OFC_FLOWTBL_TRC,
                                   "FUNC:OfcFlowAddFlow: Delete Failed in MIB Flow Tables\n"));
                }
                u1Flag = OFC_RESET_WITH_STATS;
            }
            else
            {
                u4Used = pFlow->u4Used;
                u4PktCount = pFlow->u4PktCount;
                u4ByteCount = pFlow->u4ByteCount;

                pFlow->u1TableId = OFC_ZERO;
                if (OfcFlowDeleteFlow
                    (pOfc, pFlow, OFPFC_DELETE_STRICT,
                     OFPRR_ERROR_DELETE) != OFC_SUCCESS)
                {
                    OFC_TRC_FUNC ((OFC_FLOWTBL_TRC,
                                   "FUNC:OfcFlowAddFlow: Delete Failed in MIB Flow Tables\n"));
                }
                u1Flag = OFC_RESET_WITHOUT_STATS;
            }
        }
    }

    if ((pFlow == NULL) ||
        ((pFlow != NULL)
         && ((u1Flag == OFC_RESET_WITH_STATS)
             || (u1Flag == OFC_RESET_WITHOUT_STATS))))
    {
        pFlow = (tOfcFlowEntry *) MemAllocMemBlk (OFC_FLOWMSGS_POOLID);
        if (pFlow == NULL)
        {
            /* MemPool Allocation failed */
            OFC_TRC_FUNC ((OFC_FLOWTBL_TRC,
                           "FUNC:OfcFlowAddFlow: Memory Allocation Failed for pFlow\n"));

            OfpErrorMsg.u2Type = OFPET_FLOW_MOD_FAILED;
            OfpErrorMsg.u2Code = OFPFMFC_TABLE_FULL;

            /* Calling Pkt TX/RX API */
            OFC_TRC_FUNC ((OFC_FLOWTBL_TRC,
                           "FUNC:OfcFlowAddFlow: Sending Reply to Controller through Pkt Tx/Rx API\n"));
            i4RetValue = OfcErrMsgPktSend (&OfpErrorMsg, NULL);

            OFC_TRC_FUNC ((OFC_FLOWTBL_TRC,
                           "FUNC:OfcFlowAddFlow: Send Reply to Controller Success, through Pkt Tx/Rx API\n"));

            return OFC_FAILURE;
        }

        if (pFlowMsg->u4NumActs > OFC_MAX_ACTIONS)
        {
            OfpErrorMsg.u2Type = OFPET_BAD_ACTION;
            OfpErrorMsg.u2Code = OFPBAC_TOO_MANY;

            i4RetValue = OfcErrMsgPktSend (&OfpErrorMsg, NULL);
            OFC_TRC_FUNC ((OFC_FLOWTBL_TRC,
                           "FUNC:OfcFlowAddFlow: Send Error Message to Controller when there are                                   more actions\n"));
            MemReleaseMemBlock (OFC_FLOWMSGS_POOLID, (UINT1 *) pFlow);
            return OFC_FAILURE;
        }

        MEMSET (pFlow, OFC_ZERO, sizeof (tOfcFlowEntry));
        MEMSET (pFlow->OfcActs, OFC_ZERO,
                ((sizeof (unOfcActs)) * (pFlowMsg->u4NumActs)));

        if (u1Flag == OFC_RESET_WITH_STATS)
        {
            pFlow->u4FlowIndex = u4FlowIndex;
        }
        else if (u1Flag == OFC_RESET_WITHOUT_STATS)
        {
            pFlow->u4FlowIndex = u4FlowIndex;
            pFlow->u4Used = u4Used;
            pFlow->u4PktCount = u4PktCount;
            pFlow->u4ByteCount = u4ByteCount;
        }
        else
        {
            gu4FlowIndex = OfcIndexMgrGetIndexBasedOnFlag (OFC_ONE, TRUE);
            pFlow->u4FlowIndex = gu4FlowIndex;
        }

        MEMCPY (&(pFlow->key), &(pFlowMsg->key), sizeof (tOfcFlowMatch));
        MEMCPY (pFlow->OfcActs, pFlowMsg->OfcActs,
                ((sizeof (unOfcActs)) * (pFlowMsg->u4NumActs)));
        pFlow->u4NumActs = pFlowMsg->u4NumActs;
        OfcFlowModMsgToFlowEntry (pFlowMsg, pFlow);

        pFlow->u4Used = OsixGetSysUpTime ();
        u4HashIndex = pFlow->u2Priority % OFC_MAX_FLOWS;
        TMO_HASH_Add_Node (pOfc->pFlowtable, &(pFlow->node), u4HashIndex, NULL);

        pFlow->u1TableId = OFC_ZERO;
        i4RetValue = OfcFlowAddFlowEntry (pOfc, pFlow);
        if (i4RetValue == OFC_SUCCESS)
        {
            OFC_TRC_FUNC ((OFC_FLOWTBL_TRC,
                           "\r\nFUNC:Ofc131FlowAddFlow: Calling NPAPI for Add Flow in HW Tables\r\n"));
#ifdef NPAPI_WANTED
            pOutPort = (tOfcActOutPort *) (&(pFlow->OfcActs[0]));
            OfcHwInfo.OfcVer = OFC_VER_100;
            OfcHwInfo.OfcCmd = OFC_NP_FLOW_ADD;
            OfcHwInfo.OfcHwEntry.OfcHwFlowInfo.u1TableId = pFlow->u1TableId;
            OfcHwInfo.OfcHwEntry.OfcHwFlowInfo.u4FlowIndex = pFlow->u4FlowIndex;
            OfcHwInfo.OfcHwEntry.OfcHwFlowInfo.u2Priority = pFlow->u2Priority;
            OfcHwInfo.OfcHwEntry.OfcHwFlowInfo.ExactKey.u4InIfIndex =
                pFlowMsg->key.u4InIfIndex;
            MEMCPY (&(OfcHwInfo.OfcHwEntry.OfcHwFlowInfo.ExactKey.au1Ip6Src),
                    &(pFlowMsg->key.au1Ip6Src),
                    sizeof (pFlowMsg->key.au1Ip6Src));
            MEMCPY (&(OfcHwInfo.OfcHwEntry.OfcHwFlowInfo.ExactKey.au1Ip6Dst),
                    &(pFlowMsg->key.au1Ip6Dst),
                    sizeof (pFlowMsg->key.au1Ip6Dst));
            MEMCPY (&
                    (OfcHwInfo.OfcHwEntry.OfcHwFlowInfo.ExactKey.au1SrcMacAddr),
                    &(pFlowMsg->key.au1SrcMacAddr),
                    sizeof (pFlowMsg->key.au1SrcMacAddr));
            MEMCPY (&
                    (OfcHwInfo.OfcHwEntry.OfcHwFlowInfo.ExactKey.au1DstMacAddr),
                    &(pFlowMsg->key.au1DstMacAddr),
                    sizeof (pFlowMsg->key.au1DstMacAddr));
            OfcHwInfo.OfcHwEntry.OfcHwFlowInfo.ExactKey.u4Ip4Src =
                pFlowMsg->key.u4IpSrc;
            OfcHwInfo.OfcHwEntry.OfcHwFlowInfo.ExactKey.u4Ip4Dst =
                pFlowMsg->key.u4IpDst;
            OfcHwInfo.OfcHwEntry.OfcHwFlowInfo.ExactKey.u2EthType =
                pFlowMsg->key.u2EthType;
            OfcHwInfo.OfcHwEntry.OfcHwFlowInfo.ExactKey.u2TpSrc =
                pFlowMsg->key.u2TpSrcPort;
            OfcHwInfo.OfcHwEntry.OfcHwFlowInfo.ExactKey.u2TpDst =
                pFlowMsg->key.u2TpDstPort;
            OfcHwInfo.OfcHwEntry.OfcHwFlowInfo.ExactKey.u1IpProto =
                pFlowMsg->key.u1IpProto;
            MEMCPY (&(OfcHwInfo.OfcHwEntry.OfcHwFlowInfo.OfcActs),
                    pFlowMsg->OfcActs,
                    ((sizeof (unOfcActs)) * (pFlowMsg->u4NumActs)));
            OfcHwInfo.OfcHwEntry.OfcHwFlowInfo.u4ActionParam =
                pOutPort->u2OutPort;

            if (pOutPort->u2OutPort == OFC_ZERO)
            {
                OfcHwInfo.OfcHwEntry.OfcHwFlowInfo.u4Action =
                    ISS_DROP_COPYTOCPU;
            }
            else if (pOutPort->u2OutPort == OFC_CONTROLLER)
            {
                OfcHwInfo.OfcHwEntry.OfcHwFlowInfo.u4Action =
                    ISS_SWITCH_COPYTOCPU;
            }
            else if (pOutPort->u2OutPort == OFC_FLOOD)
            {
                pOfcFsofcIfEntry =
                    (tOfcFsofcIfEntry *) RBTreeGetFirst (gOfcGlobals.OfcGlbMib.
                                                         FsofcIfTable);

                if (pOfcFsofcIfEntry != NULL)
                {
                    do
                    {
                        if (pOfcFsofcIfEntry->MibObject.i4FsofcIfIndex !=
                            (INT4) pFlowMsg->key.u4InIfIndex)
                        {
                            OSIX_BITLIST_SET_BIT (OfcHwInfo.OfcHwEntry.
                                                  OfcHwFlowInfo.
                                                  RedirectPortList,
                                                  pOfcFsofcIfEntry->MibObject.
                                                  i4FsofcIfIndex,
                                                  sizeof (tPortList));
                        }
                    }
                    while ((pOfcFsofcIfEntry =
                            (tOfcFsofcIfEntry *) RBTreeGetNext (gOfcGlobals.
                                                                OfcGlbMib.
                                                                FsofcIfTable,
                                                                pOfcFsofcIfEntry,
                                                                NULL)) != NULL);
                }
                OfcHwInfo.OfcHwEntry.OfcHwFlowInfo.u4Action = ISS_ALLOW;
            }
            else
            {
                OfcHwInfo.OfcHwEntry.OfcHwFlowInfo.u4Action = ISS_REDIRECT_TO;
            }
            i4RetValue = OfcFsNpHwConfigOfcInfo (&OfcHwInfo);
            if (i4RetValue != FNP_SUCCESS)
            {
                OFC_TRC_FUNC ((OFC_FLOWTBL_TRC,
                               "FUNC:OfcFsNpHwConfigOfcInfo: NPAPI failed in flow ADD\n"));
            }

#endif
        }
        else
        {
            u4HashIndex = pFlow->u2Priority % OFC_MAX_FLOWS;
            TMO_HASH_Delete_Node (pOfc->pFlowtable, &(pFlow->node),
                                  u4HashIndex);
            MemReleaseMemBlock (OFC_FLOWMSGS_POOLID, (UINT1 *) pFlow);

            OFC_TRC_FUNC ((OFC_FLOWTBL_TRC,
                           "FUNC:OfcFlowAddFlow: Deletion Failed, due to non-existing entry\n"));

            OfpErrorMsg.u2Type = OFPET_FLOW_MOD_FAILED;
            OfpErrorMsg.u2Code = OFPFMFC_TABLE_FULL;

            /* Calling Pkt TX/RX API */
            OFC_TRC_FUNC ((OFC_FLOWTBL_TRC,
                           "FUNC:OfcFlowAddFlow: Sending Reply to Controller through Pkt Tx/Rx API\n"));
            i4RetValue = OfcErrMsgPktSend (&OfpErrorMsg, NULL);
            OFC_TRC_FUNC ((OFC_FLOWTBL_TRC,
                           "FUNC:OfcFlowAddFlow: Send Reply to Controller Success, through Pkt Tx/Rx API\n"));
            return OFC_FAILURE;
        }

        /* Starting the Timers */
        /* On Successful Flow Add start the Idle timer & Hard Timer for this flow for
           the configured values. Timers will not be invoked, if the configured timeout
           values are ZERO. */
        if (pFlow->u2HardTimeout != OFC_ZERO)
        {
            OfcTmrStartTmr (&(pFlow->OfcHardTimer), OFC_HARD_TIMER,
                            pFlow->u2HardTimeout);
        }
        if (pFlow->u2IdleTimeout != OFC_ZERO)
        {
            OfcTmrStartTmr (&(pFlow->OfcIdleTimer), OFC_IDLE_TIMER,
                            pFlow->u2IdleTimeout);
        }

        if (u1Flag == OFC_RESET_WITH_STATS)
        {
            OFC_TRC_FUNC ((OFC_FLOWTBL_TRC,
                           "FUNC:OfcFlowAddFlow: Success, With Overlap and Counters Reset\n"));
        }
        else if (u1Flag == OFC_RESET_WITHOUT_STATS)
        {
            OFC_TRC_FUNC ((OFC_FLOWTBL_TRC,
                           "FUNC:OfcFlowAddFlow: Success, With Overlap and Without Counters Reset\n"));
        }
        else
        {
            OFC_TRC_FUNC ((OFC_FLOWTBL_TRC, "FUNC:OfcFlowAddFlow: Success\n"));
        }
        KW_FALSEPOSITIVE_FIX (pFlow);
        return OFC_SUCCESS;
    }

    OFC_TRC_FUNC ((OFC_FLOWTBL_TRC,
                   "FUNC:OfcFlowAddFlow: Invalid Code Flow\n"));
    return OFC_FAILURE;
}

/*****************************************************************************/
/* Function Name : OfcFlowModifyFlow                                         */
/* Description   : Function to handle the ModifyFlowInfo request from Packet */
/*                 Proccessing module                                        */
/* Input(s)      : *pOfc - Open Flow Client Instance                         */
/*                 *pFlowMsg - Incoming FlowMsg from controller              */
/* Output(s)     : None                                                      */
/* Return(s)     : OFC_FAILURE/OFC_SUCCESS                                   */
/*****************************************************************************/
PRIVATE INT4
OfcFlowModifyFlow (tOfcFsofcCfgEntry * pOfc, tOfcFlowEntry * pFlowMsg,
                   UINT1 u1Cmd)
{
    tOfcFlowEntry      *pFlow = NULL;
    UINT4               u4HashIndex = OFC_ZERO;
    INT4                i4RetValue = OFC_ZERO;
    tOfpErrMsg          OfpErrorMsg;

#ifdef NPAPI_WANTED
    tOfcHwInfo          OfcHwInfo;
#endif

    OFC_TRC_FUNC ((OFC_FN_ENTRY, "FUNC:OfcFlowModifyFlow\n"));

#ifdef NPAPI_WANTED
    MEMSET (&OfcHwInfo, OFC_ZERO, sizeof (tOfcHwInfo));
#endif
    MEMSET (&OfpErrorMsg, OFC_ZERO, sizeof (tOfpErrMsg));

    pFlow = OfcFlowLookup (pOfc, &pFlowMsg->key);
    if (pFlow == NULL)
    {
        OFC_TRC_FUNC ((OFC_FLOWTBL_TRC,
                       "FUNC:OfcFlowModifyFlow: Modify Failed due to no Matching Flow Entry\n"));
        return OFC_SUCCESS;
    }
    else
    {
        if (u1Cmd == OFPFC_MODIFY_STRICT)
        {
            if (pFlow->u2Priority != pFlowMsg->u2Priority)
            {
                OFC_TRC_FUNC ((OFC_FLOWTBL_TRC,
                               "FUNC:OfcFlowModifyFlow: ModifyStrict Failed due to no Matching Flow Entry"));
                return OFC_SUCCESS;
            }
        }

        if (pFlowMsg->u4NumActs > OFC_MAX_ACTIONS)
        {
            OfpErrorMsg.u2Type = OFPET_BAD_ACTION;
            OfpErrorMsg.u2Code = OFPBAC_TOO_MANY;

            i4RetValue = OfcErrMsgPktSend (&OfpErrorMsg, NULL);
            OFC_TRC_FUNC ((OFC_FLOWTBL_TRC,
                           "FUNC:OfcFlowAddFlow: Send Error Message to Controller when there are                                   more actions\n"));
            MemReleaseMemBlock (OFC_FLOWMSGS_POOLID, (UINT1 *) pFlow);
            return OFC_FAILURE;
        }

        MEMSET (pFlow->OfcActs, 0,
                ((sizeof (unOfcActs)) * (pFlowMsg->u4NumActs)));
        MEMCPY (pFlow->OfcActs, pFlowMsg->OfcActs,
                ((sizeof (unOfcActs)) * (pFlowMsg->u4NumActs)));
        pFlow->u4NumActs = pFlowMsg->u4NumActs;

        pFlow->u1TableId = OFC_ZERO;
        i4RetValue = OfcFlowModifyFlowEntry (pOfc, pFlow);

        if (i4RetValue != OFC_SUCCESS)
        {
            u4HashIndex = pFlow->u2Priority % OFC_MAX_FLOWS;
            TMO_HASH_Delete_Node (pOfc->pFlowtable, &pFlow->node, u4HashIndex);
            MemReleaseMemBlock (OFC_FLOWMSGS_POOLID, (UINT1 *) pFlow);

            return OFC_FAILURE;
        }

        if (pFlowMsg->u2Flags & OFPFF_RESET_COUNTS)
        {
            pFlow->u4Used = OFC_ZERO;
            pFlow->u4PktCount = OFC_ZERO;
            pFlow->u4ByteCount = OFC_ZERO;
            OFC_TRC_FUNC ((OFC_FLOWTBL_TRC,
                           "FUNC:OfcFlowModifyFlow: Success, Counters Cleared"));
        }

        OFC_TRC_FUNC ((OFC_FLOWTBL_TRC,
                       "\r\nFUNC:Ofc131FlowDeleteFlow: Calling NPAPI for Modify Flow in HW Tables\n"));
#ifdef NPAPI_WANTED
        OfcHwInfo.OfcVer = OFC_VER_100;
        OfcHwInfo.OfcCmd = OFC_NP_FLOW_MODIFY;
        OfcHwInfo.OfcHwEntry.OfcHwFlowInfo.u1TableId = pFlow->u1TableId;
        OfcHwInfo.OfcHwEntry.OfcHwFlowInfo.u4FlowIndex = pFlow->u4FlowIndex;
        OfcHwInfo.OfcHwEntry.OfcHwFlowInfo.ExactKey.u4InIfIndex =
            pFlowMsg->key.u4InIfIndex;
        OfcHwInfo.OfcHwEntry.OfcHwFlowInfo.u2Priority = pFlow->u2Priority;
        MEMCPY (&(OfcHwInfo.OfcHwEntry.OfcHwFlowInfo.ExactKey.au1Ip6Src),
                &(pFlowMsg->key.au1Ip6Src), sizeof (pFlowMsg->key.au1Ip6Src));
        MEMCPY (&(OfcHwInfo.OfcHwEntry.OfcHwFlowInfo.ExactKey.au1Ip6Dst),
                &(pFlowMsg->key.au1Ip6Dst), sizeof (pFlowMsg->key.au1Ip6Dst));
        MEMCPY (&(OfcHwInfo.OfcHwEntry.OfcHwFlowInfo.ExactKey.au1SrcMacAddr),
                &(pFlowMsg->key.au1SrcMacAddr),
                sizeof (pFlowMsg->key.au1SrcMacAddr));
        MEMCPY (&(OfcHwInfo.OfcHwEntry.OfcHwFlowInfo.ExactKey.au1DstMacAddr),
                &(pFlowMsg->key.au1DstMacAddr),
                sizeof (pFlowMsg->key.au1DstMacAddr));
        OfcHwInfo.OfcHwEntry.OfcHwFlowInfo.ExactKey.u4Ip4Src =
            pFlowMsg->key.u4IpSrc;
        OfcHwInfo.OfcHwEntry.OfcHwFlowInfo.ExactKey.u4Ip4Dst =
            pFlowMsg->key.u4IpDst;
        OfcHwInfo.OfcHwEntry.OfcHwFlowInfo.ExactKey.u2EthType =
            pFlowMsg->key.u2EthType;
        OfcHwInfo.OfcHwEntry.OfcHwFlowInfo.ExactKey.u2TpSrc =
            pFlowMsg->key.u2TpSrcPort;
        OfcHwInfo.OfcHwEntry.OfcHwFlowInfo.ExactKey.u2TpDst =
            pFlowMsg->key.u2TpDstPort;
        OfcHwInfo.OfcHwEntry.OfcHwFlowInfo.ExactKey.u1IpProto =
            pFlowMsg->key.u1IpProto;
        MEMCPY (&(OfcHwInfo.OfcHwEntry.OfcHwFlowInfo.OfcActs),
                pFlowMsg->OfcActs,
                ((sizeof (unOfcActs)) * (pFlowMsg->u4NumActs)));
        i4RetValue = OfcFsNpHwConfigOfcInfo (&OfcHwInfo);
        if (i4RetValue != FNP_SUCCESS)
        {
            OFC_TRC_FUNC ((OFC_FLOWTBL_TRC,
                           "FUNC:OfcFsNpHwConfigOfcInfo: NPAPI failed in flow MODIFY\n"));
        }
#endif
        /* Re-Starting the Timers */
        /* On Successful Flow Modify restart the Idle timer & Hard Timer for this flow for
           the configured values. Timers will not be invoked, if the configured timeout
           values are ZERO. */
        if (pFlow->u2HardTimeout != OFC_ZERO)
        {
            OfcTmrRestartTmr (&(pFlow->OfcHardTimer), OFC_HARD_TIMER,
                              pFlow->u2HardTimeout);
        }
        if (pFlow->u2IdleTimeout != OFC_ZERO)
        {
            OfcTmrRestartTmr (&(pFlow->OfcIdleTimer), OFC_IDLE_TIMER,
                              pFlow->u2IdleTimeout);
        }

        OFC_TRC_FUNC ((OFC_FLOWTBL_TRC,
                       "FUNC:OfcFlowModifyFlow: Success, Actions Modified\n"));
        return OFC_SUCCESS;
    }
}

/*****************************************************************************/
/* Function Name : OfcFlowDeleteFlow                                         */
/* Description   : Function to handle the DeleteFlowInfo request from Packet */
/*                 Proccessing module                                        */
/* Input(s)      : *pOfc - Open Flow Client Instance                         */
/*                 *pFlowMsg - Incoming FlowMsg from controller              */
/* Output(s)     : None                                                      */
/* Return(s)     : OFC_FAILURE/OFC_SUCCESS                                   */
/*****************************************************************************/
INT4
OfcFlowDeleteFlow (tOfcFsofcCfgEntry * pOfc, tOfcFlowEntry * pFlowMsg,
                   UINT1 u1Cmd, UINT1 u1Flag)
{
    tOfcFlowEntry      *pFlow = NULL;
    tOfpFlowRem         FlowRemMsg;
    UINT4               u4HashIndex = OFC_ZERO;
    INT4                i4RetValue = OFC_ZERO;

#ifdef NPAPI_WANTED
    tOfcHwInfo          OfcHwInfo;
#endif

    OFC_TRC_FUNC ((OFC_FN_ENTRY, "FUNC:OfcFlowDeleteFlow\n"));

#ifdef NPAPI_WANTED
    MEMSET (&OfcHwInfo, 0, sizeof (tOfcHwInfo));
#endif
    MEMSET (&FlowRemMsg, OFC_ZERO, sizeof (tOfpFlowRem));

    pFlow = OfcFlowLookup (pOfc, &pFlowMsg->key);
    if (pFlow == NULL)
    {
        OFC_TRC_FUNC ((OFC_FLOWTBL_TRC,
                       "FUNC:OfcFlowDeleteFlow: Delete Failed, no matching entry found in Lookup"));
        return OFC_SUCCESS;
    }

    if (u1Cmd == OFPFC_DELETE_STRICT)
    {
        if (pFlow->u2Priority != pFlowMsg->u2Priority)
        {
            OFC_TRC_FUNC ((OFC_FLOWTBL_TRC,
                           "FUNC:OfcFlowDeleteFlow: DeleteStrict Failed, no matching entry found in Lookup"));
            return OFC_SUCCESS;
        }
    }

    pFlow->u1TableId = OFC_ZERO;
    if ((OfcFlowDeleteFlowEntry (pOfc, pFlow)) != OFC_SUCCESS)
    {
        OFC_TRC_FUNC ((OFC_FLOWTBL_TRC,
                       "FUNC:OfcFlowDeleteFlow: Delete Failed, no matching entry found in MIB Tables"));
    }

    if (pFlow->u2Flags & OFPFF_SEND_FLOW_REM)
    {
        OfcKeyToFlowMatch (&pFlow->key, &FlowRemMsg.Match);
        FlowRemMsg.u2Priority = pFlow->u2Priority;
        if (u1Flag == OFPRR_HARD_TIMEOUT)
        {
            FlowRemMsg.u1Reason = OFPRR_HARD_TIMEOUT;
        }
        else if (u1Flag == OFPRR_IDLE_TIMEOUT)
        {
            FlowRemMsg.u1Reason = OFPRR_IDLE_TIMEOUT;
        }
        else if (u1Flag == OFPRR_DELETE)
        {
            FlowRemMsg.u1Reason = OFPRR_DELETE;
        }

        FlowRemMsg.u4DurationSec = (UINT4) pFlow->u2HardTimeout;
        FlowRemMsg.u4DurationNSec = (UINT4) pFlow->u2HardTimeout;
        FlowRemMsg.u2IdleTimeOut = pFlow->u2IdleTimeout;

        /* Calling Pkt TX/RX API */
        OFC_TRC_FUNC ((OFC_FLOWTBL_TRC,
                       "FUNC:OfcFlowDeleteFlow: Triggering Notification for Flow Removal/Expiry"));
        i4RetValue = OfcFlowRemPktSend (&FlowRemMsg, NULL);
        if (OSIX_SUCCESS == i4RetValue)
        {
            OFC_TRC_FUNC ((OFC_FLOWTBL_TRC,
                           "FUNC:OfcFlowDeleteFlow: Notification Success for Flow Removal/Expiry"));
        }
    }

    /* Stop Timers if running */
    if (pFlow->OfcHardTimer.tmrNode.u2Flags & OFC_TMR_RUNNING)
    {
        OfcTmrStopTmr (&(pFlow->OfcHardTimer));
    }
    if (pFlow->OfcIdleTimer.tmrNode.u2Flags & OFC_TMR_RUNNING)
    {
        OfcTmrStopTmr (&(pFlow->OfcIdleTimer));
    }

    u4HashIndex = pFlow->u2Priority % OFC_MAX_FLOWS;
    TMO_HASH_Delete_Node (pOfc->pFlowtable, &(pFlow->node), u4HashIndex);

    OFC_TRC_FUNC ((OFC_FLOWTBL_TRC,
                   "\r\nFUNC:Ofc131FlowDeleteFlow: Calling NPAPI for Delete Flow in HW Tables\n"));
#ifdef NPAPI_WANTED
    OfcHwInfo.OfcVer = OFC_VER_100;
    OfcHwInfo.OfcCmd = OFC_NP_FLOW_DELETE;
    OfcHwInfo.OfcHwEntry.OfcHwFlowInfo.u1TableId = pFlow->u1TableId;
    OfcHwInfo.OfcHwEntry.OfcHwFlowInfo.u4FlowIndex = pFlow->u4FlowIndex;
    OfcHwInfo.OfcHwEntry.OfcHwFlowInfo.ExactKey.u4InIfIndex =
        pFlowMsg->key.u4InIfIndex;
    OfcHwInfo.OfcHwEntry.OfcHwFlowInfo.u2Priority = pFlow->u2Priority;
    MEMCPY (&(OfcHwInfo.OfcHwEntry.OfcHwFlowInfo.ExactKey.au1Ip6Src),
            &(pFlowMsg->key.au1Ip6Src), sizeof (pFlowMsg->key.au1Ip6Src));
    MEMCPY (&(OfcHwInfo.OfcHwEntry.OfcHwFlowInfo.ExactKey.au1Ip6Dst),
            &(pFlowMsg->key.au1Ip6Dst), sizeof (pFlowMsg->key.au1Ip6Dst));
    MEMCPY (&(OfcHwInfo.OfcHwEntry.OfcHwFlowInfo.ExactKey.au1SrcMacAddr),
            &(pFlowMsg->key.au1SrcMacAddr),
            sizeof (pFlowMsg->key.au1SrcMacAddr));
    MEMCPY (&(OfcHwInfo.OfcHwEntry.OfcHwFlowInfo.ExactKey.au1DstMacAddr),
            &(pFlowMsg->key.au1DstMacAddr),
            sizeof (pFlowMsg->key.au1DstMacAddr));
    OfcHwInfo.OfcHwEntry.OfcHwFlowInfo.ExactKey.u4Ip4Src =
        pFlowMsg->key.u4IpSrc;
    OfcHwInfo.OfcHwEntry.OfcHwFlowInfo.ExactKey.u4Ip4Dst =
        pFlowMsg->key.u4IpDst;
    OfcHwInfo.OfcHwEntry.OfcHwFlowInfo.ExactKey.u2EthType =
        pFlowMsg->key.u2EthType;
    OfcHwInfo.OfcHwEntry.OfcHwFlowInfo.ExactKey.u2TpSrc =
        pFlowMsg->key.u2TpSrcPort;
    OfcHwInfo.OfcHwEntry.OfcHwFlowInfo.ExactKey.u2TpDst =
        pFlowMsg->key.u2TpDstPort;
    OfcHwInfo.OfcHwEntry.OfcHwFlowInfo.ExactKey.u1IpProto =
        pFlowMsg->key.u1IpProto;
    MEMCPY (&(OfcHwInfo.OfcHwEntry.OfcHwFlowInfo.OfcActs), pFlowMsg->OfcActs,
            ((sizeof (unOfcActs)) * (pFlowMsg->u4NumActs)));
    i4RetValue = OfcFsNpHwConfigOfcInfo (&OfcHwInfo);
    if (i4RetValue != FNP_SUCCESS)
    {
        OFC_TRC_FUNC ((OFC_FLOWTBL_TRC,
                       "FUNC:OfcFsNpHwConfigOfcInfo: NPAPI failed in flow DELETE\n"));
    }
#endif
    MemReleaseMemBlock (OFC_FLOWMSGS_POOLID, (UINT1 *) pFlow);

    OFC_TRC_FUNC ((OFC_FLOWTBL_TRC, "FUNC:OfcFlowDeleteFlow: Success\n"));
    OFC_TRC_FUNC ((OFC_FN_EXIT, "FUNC:OfcFlowDeleteFlow\n"));
    return OFC_SUCCESS;
}

/*****************************************************************************/
/* Function Name : OfcFlowAddFlowEntry                                       */
/* Description   : This function handles flow entry add to Version 1.0.0     */
/*                 Flow Table of the OFC Client system                       */
/*                 to tOfcFlowEntry Structure-object                         */
/* Input(s)      : *pOfc - Open Flow Client Instance                         */
/*                 *pFlow - Flow Entry Object                                */
/* Output(s)     : None                                                      */
/* Return(s)     : OFC_FAILURE/OFC_SUCESS                                    */
/*****************************************************************************/
PRIVATE INT4
OfcFlowAddFlowEntry (tOfcFsofcCfgEntry * pOfc, tOfcFlowEntry * pFlow)
{
    static tOfcFsofcFlowEntry OfcFsofcFlowEntry;
    tOfpErrMsg          OfpErrorMsg;
    tOfcFsofcFlowEntry *pOfcFsofcFlowEntry = NULL;
    INT4                i4RetValue = OFC_ZERO;

    OFC_TRC_FUNC ((OFC_FN_ENTRY, "FUNC:OfcFlowAddFlowEntry\n"));

    /* FLOW TABLE ADD */
    MEMSET (&OfcFsofcFlowEntry, OFC_ZERO, sizeof (tOfcFsofcFlowEntry));
    MEMSET (&OfcFsofcFlowEntry.MibObject, OFC_ZERO,
            sizeof (OfcFsofcFlowEntry.MibObject));
    MEMSET (&OfpErrorMsg, OFC_ZERO, sizeof (tOfpErrMsg));

    OfcFsofcFlowEntry.MibObject.u4FsofcContextId =
        pOfc->MibObject.u4FsofcContextId;
    OfcFsofcFlowEntry.MibObject.u4FsofcTableIndex = (UINT4) pFlow->u1TableId;
    OfcFsofcFlowEntry.MibObject.u4FsofcFlowIndex = pFlow->u4FlowIndex;

    /* Calling Conversion API to get String form of Flow Match Fields */
    OfcFlowMatchToStr ((CHR1 *) OfcFsofcFlowEntry.MibObject.
                       au1FsofcFlowMatchField, &(pFlow->key));
    OfcFsofcFlowEntry.MibObject.i4FsofcFlowMatchFieldLen =
        OFC_ACTION_STRING_MAX_LEN;
    /* Calling Conversion API to get String form of Action/Instructions set */
    i4RetValue =
        OfcParseStructToStr ((CHR1 *) OfcFsofcFlowEntry.MibObject.
                             au1FsofcFlowOutputAction, pFlow);
    if (OFC_FAILURE == i4RetValue)
    {
        OFC_TRC_FUNC ((OFC_FLOWTBL_TRC, "FUNC:OfcAddFlowEntry: "
                       "Add failed due to invalid action type\n"));
        OfpErrorMsg.u2Type = OFPET_FLOW_MOD_FAILED;
        OfpErrorMsg.u2Code = OFPFMFC_UNSUPPORTED;
        /* Calling Pkt TX/RX API */
        OFC_TRC_FUNC ((OFC_FLOWTBL_TRC,
                       "FUNC:OfcFlowAddFlowEntry: Sending Reply to Controller through Pkt Tx/Rx API\n"));
        i4RetValue = OfcErrMsgPktSend (&OfpErrorMsg, NULL);

        OFC_TRC_FUNC ((OFC_FLOWTBL_TRC,
                       "FUNC:OfcFlowAddFlowEntry: Send Reply to Controller Success, through Pkt Tx/Rx API\n"));
        return OFC_FAILURE;
    }
    OfcFsofcFlowEntry.MibObject.i4FsofcFlowOutputActionLen =
        OFC_ACTION_STRING_MAX_LEN;
    if ((pOfcFsofcFlowEntry =
         OfcFsofcFlowTableCreateApi (&OfcFsofcFlowEntry)) == NULL)
    {
        OFC_TRC_FUNC ((OFC_FLOWTBL_TRC, "FUNC:OfcAddFlowEntry: "
                       "Add failed in Flow MIB Table\n"));
        return OFC_FAILURE;
    }

    OFC_TRC_FUNC ((OFC_FLOWTBL_TRC,
                   "\r\nFUNC:OfcFlowAddFlowEntry: Flow Entry with Flow Index: %d is added\n",
                   pOfcFsofcFlowEntry->MibObject.u4FsofcFlowIndex));
    OFC_TRC_FUNC ((OFC_FN_EXIT, "FUNC:OfcFlowAddFlowEntry\n"));
    return OFC_SUCCESS;
}

/*****************************************************************************/
/* Function Name : OfcFlowModifyFlowEntry                                    */
/* Description   : This function handles flow entry modification from V 1.0.0*/
/*                 Flow Table of the OFC Client system                       */
/*                 to tOfcFlowEntry Structure-object                         */
/* Input(s)      : *pOfc - Open Flow Client Instance                         */
/*                 *pFlow - Flow Entry Object                                */
/* Output(s)     : None                                                      */
/* Return(s)     : OFC_FAILURE/OFC_SUCESS                                    */
/*****************************************************************************/

PRIVATE INT4
OfcFlowModifyFlowEntry (tOfcFsofcCfgEntry * pOfc, tOfcFlowEntry * pFlow)
{
    tOfcFsofcFlowEntry *pOfcFsofcFlowEntry = NULL;
    static tOfcFsofcFlowEntry OfcFsofcFlowEntry;
    tOfpErrMsg          OfpErrorMsg;
    INT4                i4RetValue = OFC_ZERO;

    OFC_TRC_FUNC ((OFC_FN_ENTRY, "FUNC:OfcFlowModifyFlowEntry\n"));
    MEMSET (&OfpErrorMsg, OFC_ZERO, sizeof (tOfpErrMsg));

    /* FLOW TABLE UPDATE */
    MEMSET (&OfcFsofcFlowEntry, OFC_ZERO, sizeof (tOfcFsofcFlowEntry));
    MEMSET (&OfcFsofcFlowEntry.MibObject, OFC_ZERO,
            sizeof (OfcFsofcFlowEntry.MibObject));
    OfcFsofcFlowEntry.MibObject.u4FsofcContextId =
        pOfc->MibObject.u4FsofcContextId;
    OfcFsofcFlowEntry.MibObject.u4FsofcTableIndex = (UINT4) pFlow->u1TableId;
    OfcFsofcFlowEntry.MibObject.u4FsofcFlowIndex = pFlow->u4FlowIndex;

    /* Get the node, which is already present in Table */
    pOfcFsofcFlowEntry =
        RBTreeGet (gOfcGlobals.OfcGlbMib.FsofcFlowTable,
                   (tRBElem *) & (OfcFsofcFlowEntry));

    if (pOfcFsofcFlowEntry != NULL)
    {
        /* Call Conversion API to get String form of Action/Instructions set */
        MEMSET (pOfcFsofcFlowEntry->MibObject.au1FsofcFlowOutputAction,
                OFC_ZERO, OFC_ACTION_STRING_MAX_LEN);
        i4RetValue =
            OfcParseStructToStr ((CHR1 *) pOfcFsofcFlowEntry->MibObject.
                                 au1FsofcFlowOutputAction, pFlow);
        if (OFC_FAILURE == i4RetValue)
        {
            OFC_TRC_FUNC ((OFC_FLOWTBL_TRC, "FUNC:OfcModifyFlowEntry: "
                           "Modify failed due to invalid action type\n"));
            OfpErrorMsg.u2Type = OFPET_FLOW_MOD_FAILED;
            OfpErrorMsg.u2Code = OFPFMFC_UNSUPPORTED;
            /* Calling Pkt TX/RX API */
            OFC_TRC_FUNC ((OFC_FLOWTBL_TRC,
                           "FUNC:OfcFlowModifyFlowEntry: Sending Reply to Controller through Pkt Tx/Rx API\n"));
            i4RetValue = OfcErrMsgPktSend (&OfpErrorMsg, NULL);

            OFC_TRC_FUNC ((OFC_FLOWTBL_TRC,
                           "FUNC:OfcFlowModifyFlowEntry: Send Reply to Controller Success, through Pkt Tx/Rx API\n"));
            return OFC_FAILURE;
        }
    }
    else
    {
        OFC_TRC_FUNC ((OFC_FLOWTBL_TRC,
                       "FUNC:OfcFlowModifyFlowEntry: Modify Failed due to Update Fail in MIB Table\n"));

        OfpErrorMsg.u2Type = OFPET_FLOW_MOD_FAILED;
        OfpErrorMsg.u2Code = OFPFMFC_UNKNOWN;

        /* Calling Pkt TX/RX API */
        i4RetValue = OfcErrMsgPktSend (&OfpErrorMsg, NULL);
        OFC_TRC_FUNC ((OFC_FLOWTBL_TRC,
                       "FUNC:OfcFlowModifyFlowEntry: Modify Failed in MIB Table Flow Entry"));
        return OFC_FAILURE;
    }

    OFC_TRC_FUNC ((OFC_FN_EXIT, "FUNC:OfcFlowModifyFlowEntry\n"));
    return OFC_SUCCESS;
}

/*****************************************************************************/
/* Function Name : OfcFlowDeleteFlowEntry                                    */
/* Description   : This function handles flow entry deletion from Ver 1.0.0  */
/*                 Flow Table of the OFC Client system                       */
/*                 to tOfcFlowEntry Structure-object                         */
/* Input(s)      : *pOfc - Open Flow Client Instance                         */
/*                 *pFlow - Flow Entry Object                                */
/* Output(s)     : None                                                      */
/* Return(s)     : OFC_FAILURE/OFC_SUCESS                                    */
/*****************************************************************************/

PRIVATE INT4
OfcFlowDeleteFlowEntry (tOfcFsofcCfgEntry * pOfc, tOfcFlowEntry * pFlow)
{
    static tOfcFsofcFlowEntry OfcFsofcFlowEntry;
    tOfcFsofcFlowEntry *pOfcFsofcFlowEntry = NULL;

    OFC_TRC_FUNC ((OFC_FN_ENTRY, "FUNC:OfcFlowDeleteFlowEntry\n"));

    MEMSET (&OfcFsofcFlowEntry, OFC_ZERO, sizeof (OfcFsofcFlowEntry));
    MEMSET (&OfcFsofcFlowEntry.MibObject, OFC_ZERO,
            sizeof (OfcFsofcFlowEntry.MibObject));
    OfcFsofcFlowEntry.MibObject.u4FsofcContextId =
        pOfc->MibObject.u4FsofcContextId;
    OfcFsofcFlowEntry.MibObject.u4FsofcTableIndex = (UINT4) pFlow->u1TableId;
    OfcFsofcFlowEntry.MibObject.u4FsofcFlowIndex = pFlow->u4FlowIndex;

    pOfcFsofcFlowEntry =
        RBTreeGet (gOfcGlobals.OfcGlbMib.FsofcFlowTable,
                   (tRBElem *) & (OfcFsofcFlowEntry));

    RBTreeRem (gOfcGlobals.OfcGlbMib.FsofcFlowTable, pOfcFsofcFlowEntry);
    OfcIndexMgrRelIndex (OFC_ONE, pFlow->u4FlowIndex);

    OFC_TRC_FUNC ((OFC_FN_EXIT, "FUNC:OfcFlowDeleteFlowEntry\n"));
    return OFC_SUCCESS;
}
