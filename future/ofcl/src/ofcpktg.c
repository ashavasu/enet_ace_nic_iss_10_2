/********************************************************************
 * Copyright (C) 2013 Aricent Inc . All Rights Reserved
 *
 * $Id: ofcpktg.c,v 1.3 2013/10/22 10:46:57 siva Exp $
 *
 * Description: This file contains the Ofc pkt process related routines
 *********************************************************************/

#include "ofcinc.h"
#include "ofcpkt.h"

/****************************************************************************
 * Function    :  OfcOfpParse
 * Description :  This function is for Ofp Header Parsing  
 * Input       :  pOfpPkt Pointer to the header and pConnPtr Conn Ptr
 * Output      :  pu4Error - Error on failure 
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE 
 ****************************************************************************/
INT4
OfcOfpPktParse (tOfpPkt * pOfpPkt, UINT4 *pu4Error, VOID *pConnPtr)
{
    tOfcFsofcControllerConnEntry *pConn = NULL;

    if (pConnPtr == NULL)
    {
        return OSIX_FAILURE;
    }
    else
    {

        pConn = (tOfcFsofcControllerConnEntry *) pConnPtr;
    }

    if (pConn != NULL)
    {
        OfcTmrRestartTmr (&(pConn->EchoInterTimer),
                          OFC_ECHOINTER_TMR, OFC_ECHOINTER_INTERVAL);
    }
    if (OfcOfpu1VersionValidate (pOfpPkt, pu4Error, pConnPtr) == OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    if (OfcOfpu1TypeValidate (pOfpPkt, pu4Error, pConnPtr) == OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    pOfpPkt->u2Length = OSIX_NTOHS (pOfpPkt->u2Length);
    if (OfcOfpu2LengthValidate (pOfpPkt, pu4Error, pConnPtr) == OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    pOfpPkt->u4Xid = OSIX_NTOHL (pOfpPkt->u4Xid);
    if (OfcOfpu4XidValidate (pOfpPkt, pu4Error, pConnPtr) == OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    if (OfcOfpu1VersionAction (pOfpPkt, pu4Error, pConnPtr) == OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    if (OfcOfpu2LengthAction (pOfpPkt, pu4Error, pConnPtr) == OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    if (OfcOfpu4XidAction (pOfpPkt, pu4Error, pConnPtr) == OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    if (OfcOfpu1TypeAction (pOfpPkt, pu4Error, pConnPtr) == OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  OfcHelloParse
 * Description :  This function is for Hello Header Parsing  
 * Input       :  pOfpHello Pointer to the header and pConnPtr Conn Ptr
 * Output      :  pu4Error - Error on failure 
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE 
 ****************************************************************************/
INT4
OfcHelloPktParse (tOfpHello * pOfpHello, UINT4 *pu4Error, VOID *pConnPtr)
{
    if (OfcHelloau1DummyValidate (pOfpHello, pu4Error, pConnPtr) ==
        OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    if (OfcHelloau1DummyAction (pOfpHello, pu4Error, pConnPtr) == OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  OfcEchoReqMsgParse
 * Description :  This function is for EchoReqMsg Header Parsing  
 * Input       :  pOfpEchoReqMsg Pointer to the header and pConnPtr Conn Ptr
 * Output      :  pu4Error - Error on failure 
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE 
 ****************************************************************************/
INT4
OfcEchoReqMsgPktParse (tOfpEchoReqMsg * pOfpEchoReqMsg, UINT4 *pu4Error,
                       VOID *pConnPtr)
{
    if (OfcEchoReqMsgau1DataValidate (pOfpEchoReqMsg, pu4Error, pConnPtr) ==
        OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    if (OfcEchoReqMsgau1DataAction (pOfpEchoReqMsg, pu4Error, pConnPtr) ==
        OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  OfcEchoRplyMsgParse
 * Description :  This function is for EchoRplyMsg Header Parsing  
 * Input       :  pOfpEchoRplyMsg Pointer to the header and pConnPtr Conn Ptr
 * Output      :  pu4Error - Error on failure 
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE 
 ****************************************************************************/
INT4
OfcEchoRplyMsgPktParse (tOfpEchoRplyMsg * pOfpEchoRplyMsg, UINT4 *pu4Error,
                        VOID *pConnPtr)
{
    if (OfcEchoRplyMsgau1DataValidate (pOfpEchoRplyMsg, pu4Error, pConnPtr) ==
        OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    if (OfcEchoRplyMsgau1DataAction (pOfpEchoRplyMsg, pu4Error, pConnPtr) ==
        OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    ((tOfcFsofcControllerConnEntry *) (pConnPtr))->u4IsRecieved = TRUE;
    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  OfcExperimenterHdrParse
 * Description :  This function is for ExperimenterHdr Header Parsing  
 * Input       :  pOfpExperimenterHdr Pointer to the header and pConnPtr Conn Ptr
 * Output      :  pu4Error - Error on failure 
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE 
 ****************************************************************************/
INT4
OfcExperimenterHdrPktParse (tOfpExperimenterHdr * pOfpExperimenterHdr,
                            UINT4 *pu4Error, VOID *pConnPtr)
{
    pOfpExperimenterHdr->u4Experimenter =
        OSIX_NTOHL (pOfpExperimenterHdr->u4Experimenter);
    if (OfcExperimenterHdru4ExperimenterValidate
        (pOfpExperimenterHdr, pu4Error, pConnPtr) == OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    if (OfcExperimenterHdru4ExperimenterAction
        (pOfpExperimenterHdr, pu4Error, pConnPtr) == OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  OfcFeatureReqParse
 * Description :  This function is for FeatureReq Header Parsing  
 * Input       :  pOfpFeatureReq Pointer to the header and pConnPtr Conn Ptr
 * Output      :  pu4Error - Error on failure 
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE 
 ****************************************************************************/
INT4
OfcFeatureReqPktParse (tOfpFeatureReq * pOfpFeatureReq, UINT4 *pu4Error,
                       VOID *pConnPtr)
{
    if (OfcFeatureReqau1DummyValidate (pOfpFeatureReq, pu4Error, pConnPtr) ==
        OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    if (OfcFeatureReqau1DummyAction (pOfpFeatureReq, pu4Error, pConnPtr) ==
        OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  OfcConfigGetParse
 * Description :  This function is for ConfigGet Header Parsing  
 * Input       :  pOfpConfigGet Pointer to the header and pConnPtr Conn Ptr
 * Output      :  pu4Error - Error on failure 
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE 
 ****************************************************************************/
INT4
OfcConfigGetPktParse (tOfpConfigGet * pOfpConfigGet, UINT4 *pu4Error,
                      VOID *pConnPtr)
{
    if (OfcConfigGetau1DummyValidate (pOfpConfigGet, pu4Error, pConnPtr) ==
        OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    if (OfcConfigGetau1DummyAction (pOfpConfigGet, pu4Error, pConnPtr) ==
        OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  OfcConfigSetParse
 * Description :  This function is for ConfigSet Header Parsing  
 * Input       :  pOfpConfigSet Pointer to the header and pConnPtr Conn Ptr
 * Output      :  pu4Error - Error on failure 
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE 
 ****************************************************************************/
INT4
OfcConfigSetPktParse (tOfpConfigSet * pOfpConfigSet, UINT4 *pu4Error,
                      VOID *pConnPtr)
{
    pOfpConfigSet->u2Flags = OSIX_NTOHS (pOfpConfigSet->u2Flags);
    if (OfcConfigSetu2FlagsValidate (pOfpConfigSet, pu4Error, pConnPtr) ==
        OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    pOfpConfigSet->u2MissSendLen = OSIX_NTOHS (pOfpConfigSet->u2MissSendLen);
    if (OfcConfigSetu2MissSendLenValidate (pOfpConfigSet, pu4Error, pConnPtr) ==
        OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    if (OfcConfigSetu2FlagsAction (pOfpConfigSet, pu4Error, pConnPtr) ==
        OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    if (OfcConfigSetu2MissSendLenAction (pOfpConfigSet, pu4Error, pConnPtr) ==
        OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  OfcPktOutParse
 * Description :  This function is for PktOut Header Parsing  
 * Input       :  pOfpPktOut Pointer to the header and pConnPtr Conn Ptr
 * Output      :  pu4Error - Error on failure 
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE 
 ****************************************************************************/
INT4
OfcPktOutPktParse (tOfpPktOut * pOfpPktOut, UINT4 *pu4Error, VOID *pConnPtr)
{
    pOfpPktOut->u4BufferId = OSIX_NTOHL (pOfpPktOut->u4BufferId);
    if (OfcPktOutu4BufferIdValidate (pOfpPktOut, pu4Error, pConnPtr) ==
        OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    pOfpPktOut->u2InPort = OSIX_NTOHS (pOfpPktOut->u2InPort);
    if (OfcPktOutu2InPortValidate (pOfpPktOut, pu4Error, pConnPtr) ==
        OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    pOfpPktOut->u2ActionsLen = OSIX_NTOHS (pOfpPktOut->u2ActionsLen);
    if (OfcPktOutu2ActionsLenValidate (pOfpPktOut, pu4Error, pConnPtr) ==
        OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    if (OfcPktOutActionsValidate (pOfpPktOut, pu4Error, pConnPtr) ==
        OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    if (OfcPktOutu4BufferIdAction (pOfpPktOut, pu4Error, pConnPtr) ==
        OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    if (OfcPktOutu2InPortAction (pOfpPktOut, pu4Error, pConnPtr) ==
        OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    if (OfcPktOutu2ActionsLenAction (pOfpPktOut, pu4Error, pConnPtr) ==
        OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    if (OfcPktOutActionsAction (pOfpPktOut, pu4Error, pConnPtr) == OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  OfcFlowModParse
 * Description :  This function is for FlowMod Header Parsing  
 * Input       :  pOfpFlowMod Pointer to the header and pConnPtr Conn Ptr
 * Output      :  pu4Error - Error on failure 
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE 
 ****************************************************************************/
INT4
OfcFlowModPktParse (tOfpFlowMod * pOfpFlowMod, UINT4 *pu4Error, VOID *pConnPtr)
{
    if (OfcFlowModMatchValidate (pOfpFlowMod, pu4Error, pConnPtr) ==
        OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    if (OfcFlowModu8CookieValidate (pOfpFlowMod, pu4Error, pConnPtr) ==
        OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    pOfpFlowMod->u2Command = OSIX_NTOHS (pOfpFlowMod->u2Command);
    if (OfcFlowModu2CommandValidate (pOfpFlowMod, pu4Error, pConnPtr) ==
        OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    pOfpFlowMod->u2IdleTimeout = OSIX_NTOHS (pOfpFlowMod->u2IdleTimeout);
    if (OfcFlowModu2IdleTimeoutValidate (pOfpFlowMod, pu4Error, pConnPtr) ==
        OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    pOfpFlowMod->u2HardTimeout = OSIX_NTOHS (pOfpFlowMod->u2HardTimeout);
    if (OfcFlowModu2HardTimeoutValidate (pOfpFlowMod, pu4Error, pConnPtr) ==
        OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    pOfpFlowMod->u2Priority = OSIX_NTOHS (pOfpFlowMod->u2Priority);
    if (OfcFlowModu2PriorityValidate (pOfpFlowMod, pu4Error, pConnPtr) ==
        OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    pOfpFlowMod->u4BufId = OSIX_NTOHL (pOfpFlowMod->u4BufId);
    if (OfcFlowModu4BufIdValidate (pOfpFlowMod, pu4Error, pConnPtr) ==
        OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    pOfpFlowMod->u2OutPort = OSIX_NTOHS (pOfpFlowMod->u2OutPort);
    if (OfcFlowModu2OutPortValidate (pOfpFlowMod, pu4Error, pConnPtr) ==
        OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    pOfpFlowMod->u2Flags = OSIX_NTOHS (pOfpFlowMod->u2Flags);
    if (OfcFlowModu2FlagsValidate (pOfpFlowMod, pu4Error, pConnPtr) ==
        OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    if (OfcFlowModActionsValidate (pOfpFlowMod, pu4Error, pConnPtr) ==
        OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    if (OfcFlowModMatchAction (pOfpFlowMod, pu4Error, pConnPtr) == OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    if (OfcFlowModu8CookieAction (pOfpFlowMod, pu4Error, pConnPtr) ==
        OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    if (OfcFlowModu2CommandAction (pOfpFlowMod, pu4Error, pConnPtr) ==
        OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    if (OfcFlowModu2IdleTimeoutAction (pOfpFlowMod, pu4Error, pConnPtr) ==
        OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    if (OfcFlowModu2HardTimeoutAction (pOfpFlowMod, pu4Error, pConnPtr) ==
        OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    if (OfcFlowModu2PriorityAction (pOfpFlowMod, pu4Error, pConnPtr) ==
        OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    if (OfcFlowModu4BufIdAction (pOfpFlowMod, pu4Error, pConnPtr) ==
        OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    if (OfcFlowModu2OutPortAction (pOfpFlowMod, pu4Error, pConnPtr) ==
        OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    if (OfcFlowModu2FlagsAction (pOfpFlowMod, pu4Error, pConnPtr) ==
        OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    if (OfcFlowModActionsAction (pOfpFlowMod, pu4Error, pConnPtr) ==
        OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  OfcPortModParse
 * Description :  This function is for PortMod Header Parsing  
 * Input       :  pOfpPortMod Pointer to the header and pConnPtr Conn Ptr
 * Output      :  pu4Error - Error on failure 
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE 
 ****************************************************************************/
INT4
OfcPortModPktParse (tOfpPortMod * pOfpPortMod, UINT4 *pu4Error, VOID *pConnPtr)
{
    pOfpPortMod->u2PortNo = OSIX_NTOHS (pOfpPortMod->u2PortNo);
    if (OfcPortModu4PortNoValidate (pOfpPortMod, pu4Error, pConnPtr) ==
        OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    if (OfcPortModau1PadValidate (pOfpPortMod, pu4Error, pConnPtr) ==
        OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    if (OfcPortModau1HwAddrValidate (pOfpPortMod, pu4Error, pConnPtr) ==
        OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    if (OfcPortModau1Pad2Validate (pOfpPortMod, pu4Error, pConnPtr) ==
        OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    pOfpPortMod->u4Config = OSIX_NTOHL (pOfpPortMod->u4Config);
    if (OfcPortModu4ConfigValidate (pOfpPortMod, pu4Error, pConnPtr) ==
        OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    pOfpPortMod->u4Mask = OSIX_NTOHL (pOfpPortMod->u4Mask);
    if (OfcPortModu4MaskValidate (pOfpPortMod, pu4Error, pConnPtr) ==
        OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    pOfpPortMod->u4Advertise = OSIX_NTOHL (pOfpPortMod->u4Advertise);
    if (OfcPortModu4AdvertiseValidate (pOfpPortMod, pu4Error, pConnPtr) ==
        OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    if (OfcPortModau1Pad3Validate (pOfpPortMod, pu4Error, pConnPtr) ==
        OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    if (OfcPortModu4PortNoAction (pOfpPortMod, pu4Error, pConnPtr) ==
        OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    if (OfcPortModau1PadAction (pOfpPortMod, pu4Error, pConnPtr) ==
        OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    if (OfcPortModau1HwAddrAction (pOfpPortMod, pu4Error, pConnPtr) ==
        OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    if (OfcPortModau1Pad2Action (pOfpPortMod, pu4Error, pConnPtr) ==
        OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    if (OfcPortModu4ConfigAction (pOfpPortMod, pu4Error, pConnPtr) ==
        OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    if (OfcPortModu4MaskAction (pOfpPortMod, pu4Error, pConnPtr) ==
        OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    if (OfcPortModu4AdvertiseAction (pOfpPortMod, pu4Error, pConnPtr) ==
        OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    if (OfcPortModau1Pad3Action (pOfpPortMod, pu4Error, pConnPtr) ==
        OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  OfcStatsReqParse
 * Description :  This function is for StatsReq Header Parsing  
 * Input       :  pOfpStatsReq Pointer to the header and pConnPtr Conn Ptr
 * Output      :  pu4Error - Error on failure 
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE 
 ****************************************************************************/
INT4
OfcStatsReqPktParse (tOfpStatsReq * pOfpStatsReq, UINT4 *pu4Error,
                     VOID *pConnPtr)
{
    pOfpStatsReq->u2Type = OSIX_NTOHS (pOfpStatsReq->u2Type);
    if (OfcStatsRequ2TypeValidate (pOfpStatsReq, pu4Error, pConnPtr) ==
        OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    pOfpStatsReq->u2Flags = OSIX_NTOHS (pOfpStatsReq->u2Flags);
    if (OfcStatsRequ2FlagsValidate (pOfpStatsReq, pu4Error, pConnPtr) ==
        OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    if (OfcStatsReqau1BodyValidate (pOfpStatsReq, pu4Error, pConnPtr) ==
        OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    if (OfcStatsRequ2TypeAction (pOfpStatsReq, pu4Error, pConnPtr) ==
        OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    if (OfcStatsRequ2FlagsAction (pOfpStatsReq, pu4Error, pConnPtr) ==
        OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    if (OfcStatsReqau1BodyAction (pOfpStatsReq, pu4Error, pConnPtr) ==
        OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  OfcBarrierReqParse
 * Description :  This function is for BarrierReq Header Parsing  
 * Input       :  pOfpBarrierReq Pointer to the header and pConnPtr Conn Ptr
 * Output      :  pu4Error - Error on failure 
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE 
 ****************************************************************************/
INT4
OfcBarrierReqPktParse (tOfpBarrierReq * pOfpBarrierReq, UINT4 *pu4Error,
                       VOID *pConnPtr)
{
    if (OfcBarrierReqau1DummyAction (pOfpBarrierReq, pu4Error, pConnPtr) ==
        OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  OfcQueueReqParse
 * Description :  This function is for QueueReq Header Parsing  
 * Input       :  pOfpQueueReq Pointer to the header and pConnPtr Conn Ptr
 * Output      :  pu4Error - Error on failure 
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE 
 ****************************************************************************/
INT4
OfcQueueReqPktParse (tOfpQueueReq * pOfpQueueReq, UINT4 *pu4Error,
                     VOID *pConnPtr)
{
    pOfpQueueReq->u2Port = OSIX_NTOHS (pOfpQueueReq->u2Port);
    if (OfcQueueRequ2PortValidate (pOfpQueueReq, pu4Error, pConnPtr) ==
        OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    if (OfcQueueReqau1PadValidate (pOfpQueueReq, pu4Error, pConnPtr) ==
        OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    if (OfcQueueRequ2PortAction (pOfpQueueReq, pu4Error, pConnPtr) ==
        OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    if (OfcQueueReqau1PadAction (pOfpQueueReq, pu4Error, pConnPtr) ==
        OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  OfcPktParse
 * Description :  This function is for Module Pkt Processing entry function 
 * Input       :  Pointer to pBuf
 * Output      :  if any error, output Error code  
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE 
 ****************************************************************************/
INT4
OfcPktParse (UINT1 *pBuf, VOID *pConnPtr)
{
    UINT4               u4Error = 0;
    tOfpPkt            *pOfpPkt = (tOfpPkt *) (VOID *) pBuf;
    if (OfcOfpPktParse (pOfpPkt, &u4Error, pConnPtr) == OSIX_FAILURE)
    {
        OfcPktErrorHandle (pOfpPkt, u4Error, pConnPtr);
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}
