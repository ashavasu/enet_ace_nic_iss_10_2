/*****************************************************************************
 * Copyright (C) 2013 Aricent Inc . All Rights Reserved
 *  
 * $Id: ofctrc.c,v 1.2 2014/08/14 11:11:23 siva Exp $
 *  
 * Description: This file contains routines for the flow processing for v1.3.1
 ******************************************************************************/

#include <stdio.h>
#include <string.h>
#include <stdarg.h>
#include "ofcinc.h"

/****************************************************************************
*                                                                           *
* Function     : OfcTrcPrint                                               *
*                                                                           *
* Description  :  prints the trace - with filename and line no              *
*                                                                           *
* Input        : fname   - File name                                        *
*                u4Line  - Line no                                          *
*                s       - strong to be printed                             *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : Returns string                                             *
*                                                                           *
*****************************************************************************/

VOID
OfcTrcPrint (const char *fname, UINT4 u4Line, const char *s)
{

    tOsixSysTime        sysTime = 0;
    const char         *pc1Fname = fname;

    if (s == NULL)
    {
        return;
    }
    while (*fname != '\0')
    {
        if (*fname == '/')
            pc1Fname = (fname + 1);
        fname++;
    }
    OsixGetSysTime (&sysTime);
    printf ("OFC: %d:%s:%d:   %s", sysTime, pc1Fname, u4Line, s);
}

/****************************************************************************
*                                                                           *
* Function     : OfcTrcWrite                                               *
*                                                                           *
* Description  :  prints the trace - without filename and line no ,         *
*                 Useful for dumping packets                                *
*                                                                           *
* Input        : s - string to be printed                                   *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : Returns string                                             *
*                                                                           *
*****************************************************************************/

VOID
OfcTrcWrite (CHR1 * s)
{
    if (s != NULL)
    {
        printf ("%s", s);
    }
}

/****************************************************************************
*                                                                           *
* Function     : OfcTrc                                                    *
*                                                                           *
* Description  : converts variable argument in to string depending on flag  *
*                                                                           *
* Input        : u4Flags  - Trace flag                                      *
*                fmt  - format strong, variable argument
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : Returns string                                             *
*                                                                           *
*****************************************************************************/

CHR1               *
OfcTrc (UINT4 u4Flags, const char *fmt, ...)
{
    va_list             ap;
#define OFC_TRC_BUF_SIZE    2000
    static CHR1         buf[OFC_TRC_BUF_SIZE];
    MEMSET (&ap, OFC_ZERO, sizeof (va_list));
    MEMSET (&buf, OFC_ZERO, OFC_TRC_BUF_SIZE);
    if ((u4Flags & OFC_TRC_FLAG) == 0)
    {
        return (NULL);
    }
    va_start (ap, fmt);
    vsprintf (&buf[0], fmt, ap);
    va_end (ap);

    return (&buf[0]);
}

/*-----------------------------------------------------------------------*/
/*                       End of the file  ofctrc.c                      */
/*-----------------------------------------------------------------------*/
