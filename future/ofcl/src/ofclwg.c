/********************************************************************
* Copyright (C) 2013 Aricent Inc . All Rights Reserved
*
* $Id: ofclwg.c,v 1.4 2014/04/30 10:10:25 siva Exp $
*
* Description: This file contains the low level routines
*               for the module Ofc 
*********************************************************************/

#include "ofcinc.h"
#include "ofcsz.h"

/****************************************************************************
 Function    :  nmhGetFirstIndexFsofcCfgTable
 Input       :  The Indices
                FsofcContextId
 Output      :  The Get First Routines gets the Lexicographically
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsofcCfgTable (UINT4 *pu4FsofcContextId)
{

    tOfcFsofcCfgEntry  *pOfcFsofcCfgEntry = NULL;

    pOfcFsofcCfgEntry = OfcGetFirstFsofcCfgTable ();

    if (pOfcFsofcCfgEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    /* Assign the index */
    *pu4FsofcContextId = pOfcFsofcCfgEntry->MibObject.u4FsofcContextId;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsofcControllerConnTable
 Input       :  The Indices
                FsofcContextId
                FsofcControllerIpAddrType
                FsofcControllerIpAddress
                FsofcControllerConnAuxId
 Output      :  The Get First Routines gets the Lexicographically
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsofcControllerConnTable (UINT4 *pu4FsofcContextId,
                                          INT4 *pi4FsofcControllerIpAddrType,
                                          tSNMP_OCTET_STRING_TYPE *
                                          pFsofcControllerIpAddress,
                                          INT4 *pi4FsofcControllerConnAuxId)
{

    tOfcFsofcControllerConnEntry *pOfcFsofcControllerConnEntry = NULL;

    pOfcFsofcControllerConnEntry = OfcGetFirstFsofcControllerConnTable ();

    if (pOfcFsofcControllerConnEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    /* Assign the index */
    *pu4FsofcContextId =
        pOfcFsofcControllerConnEntry->MibObject.u4FsofcContextId;

    *pi4FsofcControllerIpAddrType =
        pOfcFsofcControllerConnEntry->MibObject.i4FsofcControllerIpAddrType;

    MEMCPY (pFsofcControllerIpAddress->pu1_OctetList,
            pOfcFsofcControllerConnEntry->MibObject.au1FsofcControllerIpAddress,
            pOfcFsofcControllerConnEntry->
            MibObject.i4FsofcControllerIpAddressLen);
    pFsofcControllerIpAddress->i4_Length =
        pOfcFsofcControllerConnEntry->MibObject.i4FsofcControllerIpAddressLen;

    *pi4FsofcControllerConnAuxId =
        pOfcFsofcControllerConnEntry->MibObject.i4FsofcControllerConnAuxId;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsofcIfTable
 Input       :  The Indices
                FsofcContextId
                FsofcIfIndex
 Output      :  The Get First Routines gets the Lexicographically
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsofcIfTable (UINT4 *pu4FsofcContextId, INT4 *pi4FsofcIfIndex)
{

    tOfcFsofcIfEntry   *pOfcFsofcIfEntry = NULL;

    pOfcFsofcIfEntry = OfcGetFirstFsofcIfTable ();

    if (pOfcFsofcIfEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    /* Assign the index */
    *pu4FsofcContextId = pOfcFsofcIfEntry->MibObject.u4FsofcContextId;

    *pi4FsofcIfIndex = pOfcFsofcIfEntry->MibObject.i4FsofcIfIndex;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsofcFlowTable
 Input       :  The Indices
                FsofcContextId
                FsofcTableIndex
                FsofcFlowIndex
 Output      :  The Get First Routines gets the Lexicographically
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsofcFlowTable (UINT4 *pu4FsofcContextId,
                                UINT4 *pu4FsofcTableIndex,
                                UINT4 *pu4FsofcFlowIndex)
{

    tOfcFsofcFlowEntry *pOfcFsofcFlowEntry = NULL;

    pOfcFsofcFlowEntry = OfcGetFirstFsofcFlowTable ();

    if (pOfcFsofcFlowEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    /* Assign the index */
    *pu4FsofcContextId = pOfcFsofcFlowEntry->MibObject.u4FsofcContextId;

    *pu4FsofcTableIndex = pOfcFsofcFlowEntry->MibObject.u4FsofcTableIndex;

    *pu4FsofcFlowIndex = pOfcFsofcFlowEntry->MibObject.u4FsofcFlowIndex;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsofcGroupTable
 Input       :  The Indices
                FsofcContextId
                FsofcGroupIndex
 Output      :  The Get First Routines gets the Lexicographically
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsofcGroupTable (UINT4 *pu4FsofcContextId,
                                 UINT4 *pu4FsofcGroupIndex)
{

    tOfcFsofcGroupEntry *pOfcFsofcGroupEntry = NULL;

    pOfcFsofcGroupEntry = OfcGetFirstFsofcGroupTable ();

    if (pOfcFsofcGroupEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    /* Assign the index */
    *pu4FsofcContextId = pOfcFsofcGroupEntry->MibObject.u4FsofcContextId;

    *pu4FsofcGroupIndex = pOfcFsofcGroupEntry->MibObject.u4FsofcGroupIndex;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsofcMeterTable
 Input       :  The Indices
                FsofcContextId
                FsofcMeterIndex
 Output      :  The Get First Routines gets the Lexicographically
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsofcMeterTable (UINT4 *pu4FsofcContextId,
                                 UINT4 *pu4FsofcMeterIndex)
{

    tOfcFsofcMeterEntry *pOfcFsofcMeterEntry = NULL;

    pOfcFsofcMeterEntry = OfcGetFirstFsofcMeterTable ();

    if (pOfcFsofcMeterEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    /* Assign the index */
    *pu4FsofcContextId = pOfcFsofcMeterEntry->MibObject.u4FsofcContextId;

    *pu4FsofcMeterIndex = pOfcFsofcMeterEntry->MibObject.u4FsofcMeterIndex;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsofcCfgTable
 Input       :  The Indices
                FsofcContextId
                nextFsofcContextId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine. */

INT1
nmhGetNextIndexFsofcCfgTable (UINT4 u4FsofcContextId,
                              UINT4 *pu4NextFsofcContextId)
{

    tOfcFsofcCfgEntry   OfcFsofcCfgEntry;
    tOfcFsofcCfgEntry  *pNextOfcFsofcCfgEntry = NULL;

    MEMSET (&OfcFsofcCfgEntry, OFC_ZERO, sizeof (tOfcFsofcCfgEntry));

    /* Assign the index */
    OfcFsofcCfgEntry.MibObject.u4FsofcContextId = u4FsofcContextId;

    pNextOfcFsofcCfgEntry = OfcGetNextFsofcCfgTable (&OfcFsofcCfgEntry);

    if (pNextOfcFsofcCfgEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    /* Assign the next index */
    *pu4NextFsofcContextId = pNextOfcFsofcCfgEntry->MibObject.u4FsofcContextId;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsofcControllerConnTable
 Input       :  The Indices
                FsofcContextId
                nextFsofcContextId
                FsofcControllerIpAddrType
                nextFsofcControllerIpAddrType
                FsofcControllerIpAddress
                nextFsofcControllerIpAddress
                FsofcControllerConnAuxId
                nextFsofcControllerConnAuxId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine. */

INT1
nmhGetNextIndexFsofcControllerConnTable (UINT4 u4FsofcContextId,
                                         UINT4 *pu4NextFsofcContextId,
                                         INT4 i4FsofcControllerIpAddrType,
                                         INT4 *pi4NextFsofcControllerIpAddrType,
                                         tSNMP_OCTET_STRING_TYPE *
                                         pFsofcControllerIpAddress,
                                         tSNMP_OCTET_STRING_TYPE *
                                         pNextFsofcControllerIpAddress,
                                         INT4 i4FsofcControllerConnAuxId,
                                         INT4 *pi4NextFsofcControllerConnAuxId)
{

    tOfcFsofcControllerConnEntry OfcFsofcControllerConnEntry;
    tOfcFsofcControllerConnEntry *pNextOfcFsofcControllerConnEntry = NULL;

    MEMSET (&OfcFsofcControllerConnEntry, OFC_ZERO,
            sizeof (tOfcFsofcControllerConnEntry));

    /* Assign the index */
    OfcFsofcControllerConnEntry.MibObject.u4FsofcContextId = u4FsofcContextId;

    OfcFsofcControllerConnEntry.MibObject.i4FsofcControllerIpAddrType =
        i4FsofcControllerIpAddrType;

    MEMCPY (OfcFsofcControllerConnEntry.MibObject.au1FsofcControllerIpAddress,
            pFsofcControllerIpAddress->pu1_OctetList,
            pFsofcControllerIpAddress->i4_Length);

    OfcFsofcControllerConnEntry.MibObject.i4FsofcControllerIpAddressLen =
        pFsofcControllerIpAddress->i4_Length;
    OfcFsofcControllerConnEntry.MibObject.i4FsofcControllerConnAuxId =
        i4FsofcControllerConnAuxId;

    pNextOfcFsofcControllerConnEntry =
        OfcGetNextFsofcControllerConnTable (&OfcFsofcControllerConnEntry);

    if (pNextOfcFsofcControllerConnEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    /* Assign the next index */
    *pu4NextFsofcContextId =
        pNextOfcFsofcControllerConnEntry->MibObject.u4FsofcContextId;

    *pi4NextFsofcControllerIpAddrType =
        pNextOfcFsofcControllerConnEntry->MibObject.i4FsofcControllerIpAddrType;

    MEMCPY (pNextFsofcControllerIpAddress->pu1_OctetList,
            pNextOfcFsofcControllerConnEntry->
            MibObject.au1FsofcControllerIpAddress,
            pNextOfcFsofcControllerConnEntry->
            MibObject.i4FsofcControllerIpAddressLen);
    pNextFsofcControllerIpAddress->i4_Length =
        pNextOfcFsofcControllerConnEntry->
        MibObject.i4FsofcControllerIpAddressLen;

    *pi4NextFsofcControllerConnAuxId =
        pNextOfcFsofcControllerConnEntry->MibObject.i4FsofcControllerConnAuxId;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsofcIfTable
 Input       :  The Indices
                FsofcContextId
                nextFsofcContextId
                FsofcIfIndex
                nextFsofcIfIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine. */

INT1
nmhGetNextIndexFsofcIfTable (UINT4 u4FsofcContextId,
                             UINT4 *pu4NextFsofcContextId, INT4 i4FsofcIfIndex,
                             INT4 *pi4NextFsofcIfIndex)
{

    tOfcFsofcIfEntry    OfcFsofcIfEntry;
    tOfcFsofcIfEntry   *pNextOfcFsofcIfEntry = NULL;

    MEMSET (&OfcFsofcIfEntry, OFC_ZERO, sizeof (tOfcFsofcIfEntry));

    /* Assign the index */
    OfcFsofcIfEntry.MibObject.u4FsofcContextId = u4FsofcContextId;

    OfcFsofcIfEntry.MibObject.i4FsofcIfIndex = i4FsofcIfIndex;

    pNextOfcFsofcIfEntry = OfcGetNextFsofcIfTable (&OfcFsofcIfEntry);

    if (pNextOfcFsofcIfEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    /* Assign the next index */
    *pu4NextFsofcContextId = pNextOfcFsofcIfEntry->MibObject.u4FsofcContextId;

    *pi4NextFsofcIfIndex = pNextOfcFsofcIfEntry->MibObject.i4FsofcIfIndex;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsofcFlowTable
 Input       :  The Indices
                FsofcContextId
                nextFsofcContextId
                FsofcTableIndex
                nextFsofcTableIndex
                FsofcFlowIndex
                nextFsofcFlowIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine. */

INT1
nmhGetNextIndexFsofcFlowTable (UINT4 u4FsofcContextId,
                               UINT4 *pu4NextFsofcContextId,
                               UINT4 u4FsofcTableIndex,
                               UINT4 *pu4NextFsofcTableIndex,
                               UINT4 u4FsofcFlowIndex,
                               UINT4 *pu4NextFsofcFlowIndex)
{

    tOfcFsofcFlowEntry  OfcFsofcFlowEntry;
    tOfcFsofcFlowEntry *pNextOfcFsofcFlowEntry = NULL;

    MEMSET (&OfcFsofcFlowEntry, OFC_ZERO, sizeof (tOfcFsofcFlowEntry));

    /* Assign the index */
    OfcFsofcFlowEntry.MibObject.u4FsofcContextId = u4FsofcContextId;

    OfcFsofcFlowEntry.MibObject.u4FsofcTableIndex = u4FsofcTableIndex;

    OfcFsofcFlowEntry.MibObject.u4FsofcFlowIndex = u4FsofcFlowIndex;

    pNextOfcFsofcFlowEntry = OfcGetNextFsofcFlowTable (&OfcFsofcFlowEntry);

    if (pNextOfcFsofcFlowEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    /* Assign the next index */
    *pu4NextFsofcContextId = pNextOfcFsofcFlowEntry->MibObject.u4FsofcContextId;
    *pu4NextFsofcTableIndex =
        pNextOfcFsofcFlowEntry->MibObject.u4FsofcTableIndex;
    *pu4NextFsofcFlowIndex = pNextOfcFsofcFlowEntry->MibObject.u4FsofcFlowIndex;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsofcGroupTable
 Input       :  The Indices
                FsofcContextId
                nextFsofcContextId
                FsofcGroupIndex
                nextFsofcGroupIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine. */

INT1
nmhGetNextIndexFsofcGroupTable (UINT4 u4FsofcContextId,
                                UINT4 *pu4NextFsofcContextId,
                                UINT4 u4FsofcGroupIndex,
                                UINT4 *pu4NextFsofcGroupIndex)
{

    tOfcFsofcGroupEntry OfcFsofcGroupEntry;
    tOfcFsofcGroupEntry *pNextOfcFsofcGroupEntry = NULL;

    MEMSET (&OfcFsofcGroupEntry, OFC_ZERO, sizeof (tOfcFsofcGroupEntry));

    /* Assign the index */
    OfcFsofcGroupEntry.MibObject.u4FsofcContextId = u4FsofcContextId;

    OfcFsofcGroupEntry.MibObject.u4FsofcGroupIndex = u4FsofcGroupIndex;

    pNextOfcFsofcGroupEntry = OfcGetNextFsofcGroupTable (&OfcFsofcGroupEntry);

    if (pNextOfcFsofcGroupEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    /* Assign the next index */
    *pu4NextFsofcContextId =
        pNextOfcFsofcGroupEntry->MibObject.u4FsofcContextId;

    *pu4NextFsofcGroupIndex =
        pNextOfcFsofcGroupEntry->MibObject.u4FsofcGroupIndex;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsofcMeterTable
 Input       :  The Indices
                FsofcContextId
                nextFsofcContextId
                FsofcMeterIndex
                nextFsofcMeterIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine. */

INT1
nmhGetNextIndexFsofcMeterTable (UINT4 u4FsofcContextId,
                                UINT4 *pu4NextFsofcContextId,
                                UINT4 u4FsofcMeterIndex,
                                UINT4 *pu4NextFsofcMeterIndex)
{

    tOfcFsofcMeterEntry OfcFsofcMeterEntry;
    tOfcFsofcMeterEntry *pNextOfcFsofcMeterEntry = NULL;

    MEMSET (&OfcFsofcMeterEntry, OFC_ZERO, sizeof (tOfcFsofcMeterEntry));

    /* Assign the index */
    OfcFsofcMeterEntry.MibObject.u4FsofcContextId = u4FsofcContextId;

    OfcFsofcMeterEntry.MibObject.u4FsofcMeterIndex = u4FsofcMeterIndex;

    pNextOfcFsofcMeterEntry = OfcGetNextFsofcMeterTable (&OfcFsofcMeterEntry);

    if (pNextOfcFsofcMeterEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    /* Assign the next index */
    *pu4NextFsofcContextId =
        pNextOfcFsofcMeterEntry->MibObject.u4FsofcContextId;

    *pu4NextFsofcMeterIndex =
        pNextOfcFsofcMeterEntry->MibObject.u4FsofcMeterIndex;

    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsofcModuleStatus
 Input       :  The Indices
                FsofcContextId

                The Object 
                INT4 *pi4RetValFsofcModuleStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsofcModuleStatus (UINT4 u4FsofcContextId,
                         INT4 *pi4RetValFsofcModuleStatus)
{
    tOfcFsofcCfgEntry  *pOfcFsofcCfgEntry = NULL;

    pOfcFsofcCfgEntry =
        (tOfcFsofcCfgEntry *) MemAllocMemBlk (OFC_FSOFCCFGTABLE_POOLID);

    if (pOfcFsofcCfgEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pOfcFsofcCfgEntry, OFC_ZERO, sizeof (tOfcFsofcCfgEntry));

    /* Assign the index */
    pOfcFsofcCfgEntry->MibObject.u4FsofcContextId = u4FsofcContextId;

    if (OfcGetAllFsofcCfgTable (pOfcFsofcCfgEntry) != OSIX_SUCCESS)
    {
        MemReleaseMemBlock (OFC_FSOFCCFGTABLE_POOLID,
                            (UINT1 *) pOfcFsofcCfgEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pi4RetValFsofcModuleStatus =
        pOfcFsofcCfgEntry->MibObject.i4FsofcModuleStatus;

    MemReleaseMemBlock (OFC_FSOFCCFGTABLE_POOLID, (UINT1 *) pOfcFsofcCfgEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsofcSupportedVersion
 Input       :  The Indices
                FsofcContextId

                The Object 
                INT4 *pi4RetValFsofcSupportedVersion
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsofcSupportedVersion (UINT4 u4FsofcContextId,
                             INT4 *pi4RetValFsofcSupportedVersion)
{
    tOfcFsofcCfgEntry  *pOfcFsofcCfgEntry = NULL;

    pOfcFsofcCfgEntry =
        (tOfcFsofcCfgEntry *) MemAllocMemBlk (OFC_FSOFCCFGTABLE_POOLID);

    if (pOfcFsofcCfgEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pOfcFsofcCfgEntry, OFC_ZERO, sizeof (tOfcFsofcCfgEntry));

    /* Assign the index */
    pOfcFsofcCfgEntry->MibObject.u4FsofcContextId = u4FsofcContextId;

    if (OfcGetAllFsofcCfgTable (pOfcFsofcCfgEntry) != OSIX_SUCCESS)
    {
        MemReleaseMemBlock (OFC_FSOFCCFGTABLE_POOLID,
                            (UINT1 *) pOfcFsofcCfgEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pi4RetValFsofcSupportedVersion =
        pOfcFsofcCfgEntry->MibObject.i4FsofcSupportedVersion;

    MemReleaseMemBlock (OFC_FSOFCCFGTABLE_POOLID, (UINT1 *) pOfcFsofcCfgEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsofcDefaultFlowMissBehaviour
 Input       :  The Indices
                FsofcContextId

                The Object 
                INT4 *pi4RetValFsofcDefaultFlowMissBehaviour
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsofcDefaultFlowMissBehaviour (UINT4 u4FsofcContextId,
                                     INT4
                                     *pi4RetValFsofcDefaultFlowMissBehaviour)
{
    tOfcFsofcCfgEntry  *pOfcFsofcCfgEntry = NULL;

    pOfcFsofcCfgEntry =
        (tOfcFsofcCfgEntry *) MemAllocMemBlk (OFC_FSOFCCFGTABLE_POOLID);

    if (pOfcFsofcCfgEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pOfcFsofcCfgEntry, OFC_ZERO, sizeof (tOfcFsofcCfgEntry));

    /* Assign the index */
    pOfcFsofcCfgEntry->MibObject.u4FsofcContextId = u4FsofcContextId;

    if (OfcGetAllFsofcCfgTable (pOfcFsofcCfgEntry) != OSIX_SUCCESS)
    {
        MemReleaseMemBlock (OFC_FSOFCCFGTABLE_POOLID,
                            (UINT1 *) pOfcFsofcCfgEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pi4RetValFsofcDefaultFlowMissBehaviour =
        pOfcFsofcCfgEntry->MibObject.i4FsofcDefaultFlowMissBehaviour;

    MemReleaseMemBlock (OFC_FSOFCCFGTABLE_POOLID, (UINT1 *) pOfcFsofcCfgEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsofcControlPktBuffering
 Input       :  The Indices
                FsofcContextId

                The Object 
                INT4 *pi4RetValFsofcControlPktBuffering
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsofcControlPktBuffering (UINT4 u4FsofcContextId,
                                INT4 *pi4RetValFsofcControlPktBuffering)
{
    tOfcFsofcCfgEntry  *pOfcFsofcCfgEntry = NULL;

    pOfcFsofcCfgEntry =
        (tOfcFsofcCfgEntry *) MemAllocMemBlk (OFC_FSOFCCFGTABLE_POOLID);

    if (pOfcFsofcCfgEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pOfcFsofcCfgEntry, OFC_ZERO, sizeof (tOfcFsofcCfgEntry));

    /* Assign the index */
    pOfcFsofcCfgEntry->MibObject.u4FsofcContextId = u4FsofcContextId;

    if (OfcGetAllFsofcCfgTable (pOfcFsofcCfgEntry) != OSIX_SUCCESS)
    {
        MemReleaseMemBlock (OFC_FSOFCCFGTABLE_POOLID,
                            (UINT1 *) pOfcFsofcCfgEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pi4RetValFsofcControlPktBuffering =
        pOfcFsofcCfgEntry->MibObject.i4FsofcControlPktBuffering;

    MemReleaseMemBlock (OFC_FSOFCCFGTABLE_POOLID, (UINT1 *) pOfcFsofcCfgEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsofcIpReassembleStatus
 Input       :  The Indices
                FsofcContextId

                The Object 
                INT4 *pi4RetValFsofcIpReassembleStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsofcIpReassembleStatus (UINT4 u4FsofcContextId,
                               INT4 *pi4RetValFsofcIpReassembleStatus)
{
    tOfcFsofcCfgEntry  *pOfcFsofcCfgEntry = NULL;

    pOfcFsofcCfgEntry =
        (tOfcFsofcCfgEntry *) MemAllocMemBlk (OFC_FSOFCCFGTABLE_POOLID);

    if (pOfcFsofcCfgEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pOfcFsofcCfgEntry, OFC_ZERO, sizeof (tOfcFsofcCfgEntry));

    /* Assign the index */
    pOfcFsofcCfgEntry->MibObject.u4FsofcContextId = u4FsofcContextId;

    if (OfcGetAllFsofcCfgTable (pOfcFsofcCfgEntry) != OSIX_SUCCESS)
    {
        MemReleaseMemBlock (OFC_FSOFCCFGTABLE_POOLID,
                            (UINT1 *) pOfcFsofcCfgEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pi4RetValFsofcIpReassembleStatus =
        pOfcFsofcCfgEntry->MibObject.i4FsofcIpReassembleStatus;

    MemReleaseMemBlock (OFC_FSOFCCFGTABLE_POOLID, (UINT1 *) pOfcFsofcCfgEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsofcPortStpStatus
 Input       :  The Indices
                FsofcContextId

                The Object 
                INT4 *pi4RetValFsofcPortStpStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsofcPortStpStatus (UINT4 u4FsofcContextId,
                          INT4 *pi4RetValFsofcPortStpStatus)
{
    tOfcFsofcCfgEntry  *pOfcFsofcCfgEntry = NULL;

    pOfcFsofcCfgEntry =
        (tOfcFsofcCfgEntry *) MemAllocMemBlk (OFC_FSOFCCFGTABLE_POOLID);

    if (pOfcFsofcCfgEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pOfcFsofcCfgEntry, OFC_ZERO, sizeof (tOfcFsofcCfgEntry));

    /* Assign the index */
    pOfcFsofcCfgEntry->MibObject.u4FsofcContextId = u4FsofcContextId;

    if (OfcGetAllFsofcCfgTable (pOfcFsofcCfgEntry) != OSIX_SUCCESS)
    {
        MemReleaseMemBlock (OFC_FSOFCCFGTABLE_POOLID,
                            (UINT1 *) pOfcFsofcCfgEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pi4RetValFsofcPortStpStatus =
        pOfcFsofcCfgEntry->MibObject.i4FsofcPortStpStatus;

    MemReleaseMemBlock (OFC_FSOFCCFGTABLE_POOLID, (UINT1 *) pOfcFsofcCfgEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsofcTraceEnable
 Input       :  The Indices
                FsofcContextId

                The Object 
                UINT4 *pu4RetValFsofcTraceEnable
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsofcTraceEnable (UINT4 u4FsofcContextId,
                        UINT4 *pu4RetValFsofcTraceEnable)
{
    tOfcFsofcCfgEntry  *pOfcFsofcCfgEntry = NULL;

    pOfcFsofcCfgEntry =
        (tOfcFsofcCfgEntry *) MemAllocMemBlk (OFC_FSOFCCFGTABLE_POOLID);

    if (pOfcFsofcCfgEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pOfcFsofcCfgEntry, OFC_ZERO, sizeof (tOfcFsofcCfgEntry));

    /* Assign the index */
    pOfcFsofcCfgEntry->MibObject.u4FsofcContextId = u4FsofcContextId;

    if (OfcGetAllFsofcCfgTable (pOfcFsofcCfgEntry) != OSIX_SUCCESS)
    {
        MemReleaseMemBlock (OFC_FSOFCCFGTABLE_POOLID,
                            (UINT1 *) pOfcFsofcCfgEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pu4RetValFsofcTraceEnable =
        pOfcFsofcCfgEntry->MibObject.u4FsofcTraceEnable;

    MemReleaseMemBlock (OFC_FSOFCCFGTABLE_POOLID, (UINT1 *) pOfcFsofcCfgEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsofcSwitchModeOnConnFailure
 Input       :  The Indices
                FsofcContextId

                The Object 
                INT4 *pi4RetValFsofcSwitchModeOnConnFailure
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsofcSwitchModeOnConnFailure (UINT4 u4FsofcContextId,
                                    INT4 *pi4RetValFsofcSwitchModeOnConnFailure)
{
    tOfcFsofcCfgEntry  *pOfcFsofcCfgEntry = NULL;

    pOfcFsofcCfgEntry =
        (tOfcFsofcCfgEntry *) MemAllocMemBlk (OFC_FSOFCCFGTABLE_POOLID);

    if (pOfcFsofcCfgEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pOfcFsofcCfgEntry, OFC_ZERO, sizeof (tOfcFsofcCfgEntry));

    /* Assign the index */
    pOfcFsofcCfgEntry->MibObject.u4FsofcContextId = u4FsofcContextId;

    if (OfcGetAllFsofcCfgTable (pOfcFsofcCfgEntry) != OSIX_SUCCESS)
    {
        MemReleaseMemBlock (OFC_FSOFCCFGTABLE_POOLID,
                            (UINT1 *) pOfcFsofcCfgEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pi4RetValFsofcSwitchModeOnConnFailure =
        pOfcFsofcCfgEntry->MibObject.i4FsofcSwitchModeOnConnFailure;

    MemReleaseMemBlock (OFC_FSOFCCFGTABLE_POOLID, (UINT1 *) pOfcFsofcCfgEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsofcSwitchEntryStatus
 Input       :  The Indices
                FsofcContextId

                The Object 
                INT4 *pi4RetValFsofcSwitchEntryStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsofcSwitchEntryStatus (UINT4 u4FsofcContextId,
                              INT4 *pi4RetValFsofcSwitchEntryStatus)
{
    tOfcFsofcCfgEntry  *pOfcFsofcCfgEntry = NULL;

    pOfcFsofcCfgEntry =
        (tOfcFsofcCfgEntry *) MemAllocMemBlk (OFC_FSOFCCFGTABLE_POOLID);

    if (pOfcFsofcCfgEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pOfcFsofcCfgEntry, OFC_ZERO, sizeof (tOfcFsofcCfgEntry));

    /* Assign the index */
    pOfcFsofcCfgEntry->MibObject.u4FsofcContextId = u4FsofcContextId;

    if (OfcGetAllFsofcCfgTable (pOfcFsofcCfgEntry) != OSIX_SUCCESS)
    {
        MemReleaseMemBlock (OFC_FSOFCCFGTABLE_POOLID,
                            (UINT1 *) pOfcFsofcCfgEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pi4RetValFsofcSwitchEntryStatus =
        pOfcFsofcCfgEntry->MibObject.i4FsofcSwitchEntryStatus;

    MemReleaseMemBlock (OFC_FSOFCCFGTABLE_POOLID, (UINT1 *) pOfcFsofcCfgEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsofcHybrid
 Input       :  The Indices
                FsofcContextId

                The Object 
                INT4 *pi4RetValFsofcHybrid
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsofcHybrid (UINT4 u4FsofcContextId, INT4 *pi4RetValFsofcHybrid)
{
    tOfcFsofcCfgEntry  *pOfcFsofcCfgEntry = NULL;

    pOfcFsofcCfgEntry =
        (tOfcFsofcCfgEntry *) MemAllocMemBlk (OFC_FSOFCCFGTABLE_POOLID);

    if (pOfcFsofcCfgEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pOfcFsofcCfgEntry, 0, sizeof (tOfcFsofcCfgEntry));

    /* Assign the index */
    pOfcFsofcCfgEntry->MibObject.u4FsofcContextId = u4FsofcContextId;

    if (OfcGetAllFsofcCfgTable (pOfcFsofcCfgEntry) != OSIX_SUCCESS)
    {
        MemReleaseMemBlock (OFC_FSOFCCFGTABLE_POOLID,
                            (UINT1 *) pOfcFsofcCfgEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pi4RetValFsofcHybrid = pOfcFsofcCfgEntry->MibObject.i4FsofcHybrid;

    MemReleaseMemBlock (OFC_FSOFCCFGTABLE_POOLID, (UINT1 *) pOfcFsofcCfgEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsofcControllerConnPort
 Input       :  The Indices
                FsofcContextId
                FsofcControllerIpAddrType
                FsofcControllerIpAddress
                FsofcControllerConnAuxId

                The Object 
                INT4 *pi4RetValFsofcControllerConnPort
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsofcControllerConnPort (UINT4 u4FsofcContextId,
                               INT4 i4FsofcControllerIpAddrType,
                               tSNMP_OCTET_STRING_TYPE *
                               pFsofcControllerIpAddress,
                               INT4 i4FsofcControllerConnAuxId,
                               INT4 *pi4RetValFsofcControllerConnPort)
{
    tOfcFsofcControllerConnEntry *pOfcFsofcControllerConnEntry = NULL;

    pOfcFsofcControllerConnEntry =
        (tOfcFsofcControllerConnEntry *)
        MemAllocMemBlk (OFC_FSOFCCONTROLLERCONNTABLE_POOLID);

    if (pOfcFsofcControllerConnEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pOfcFsofcControllerConnEntry, OFC_ZERO,
            sizeof (tOfcFsofcControllerConnEntry));

    /* Assign the index */
    pOfcFsofcControllerConnEntry->MibObject.u4FsofcContextId = u4FsofcContextId;

    pOfcFsofcControllerConnEntry->MibObject.i4FsofcControllerIpAddrType =
        i4FsofcControllerIpAddrType;

    MEMCPY (pOfcFsofcControllerConnEntry->MibObject.au1FsofcControllerIpAddress,
            pFsofcControllerIpAddress->pu1_OctetList,
            pFsofcControllerIpAddress->i4_Length);

    pOfcFsofcControllerConnEntry->MibObject.i4FsofcControllerIpAddressLen =
        pFsofcControllerIpAddress->i4_Length;
    pOfcFsofcControllerConnEntry->MibObject.i4FsofcControllerConnAuxId =
        i4FsofcControllerConnAuxId;

    if (OfcGetAllFsofcControllerConnTable (pOfcFsofcControllerConnEntry) !=
        OSIX_SUCCESS)
    {
        MemReleaseMemBlock (OFC_FSOFCCONTROLLERCONNTABLE_POOLID,
                            (UINT1 *) pOfcFsofcControllerConnEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pi4RetValFsofcControllerConnPort =
        pOfcFsofcControllerConnEntry->MibObject.i4FsofcControllerConnPort;

    MemReleaseMemBlock (OFC_FSOFCCONTROLLERCONNTABLE_POOLID,
                        (UINT1 *) pOfcFsofcControllerConnEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsofcControllerConnProtocol
 Input       :  The Indices
                FsofcContextId
                FsofcControllerIpAddrType
                FsofcControllerIpAddress
                FsofcControllerConnAuxId

                The Object 
                INT4 *pi4RetValFsofcControllerConnProtocol
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsofcControllerConnProtocol (UINT4 u4FsofcContextId,
                                   INT4 i4FsofcControllerIpAddrType,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pFsofcControllerIpAddress,
                                   INT4 i4FsofcControllerConnAuxId,
                                   INT4 *pi4RetValFsofcControllerConnProtocol)
{
    tOfcFsofcControllerConnEntry *pOfcFsofcControllerConnEntry = NULL;

    pOfcFsofcControllerConnEntry =
        (tOfcFsofcControllerConnEntry *)
        MemAllocMemBlk (OFC_FSOFCCONTROLLERCONNTABLE_POOLID);

    if (pOfcFsofcControllerConnEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pOfcFsofcControllerConnEntry, OFC_ZERO,
            sizeof (tOfcFsofcControllerConnEntry));

    /* Assign the index */
    pOfcFsofcControllerConnEntry->MibObject.u4FsofcContextId = u4FsofcContextId;

    pOfcFsofcControllerConnEntry->MibObject.i4FsofcControllerIpAddrType =
        i4FsofcControllerIpAddrType;

    MEMCPY (pOfcFsofcControllerConnEntry->MibObject.au1FsofcControllerIpAddress,
            pFsofcControllerIpAddress->pu1_OctetList,
            pFsofcControllerIpAddress->i4_Length);

    pOfcFsofcControllerConnEntry->MibObject.i4FsofcControllerIpAddressLen =
        pFsofcControllerIpAddress->i4_Length;
    pOfcFsofcControllerConnEntry->MibObject.i4FsofcControllerConnAuxId =
        i4FsofcControllerConnAuxId;

    if (OfcGetAllFsofcControllerConnTable (pOfcFsofcControllerConnEntry) !=
        OSIX_SUCCESS)
    {
        MemReleaseMemBlock (OFC_FSOFCCONTROLLERCONNTABLE_POOLID,
                            (UINT1 *) pOfcFsofcControllerConnEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pi4RetValFsofcControllerConnProtocol =
        pOfcFsofcControllerConnEntry->MibObject.i4FsofcControllerConnProtocol;

    MemReleaseMemBlock (OFC_FSOFCCONTROLLERCONNTABLE_POOLID,
                        (UINT1 *) pOfcFsofcControllerConnEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsofcControllerRole
 Input       :  The Indices
                FsofcContextId
                FsofcControllerIpAddrType
                FsofcControllerIpAddress
                FsofcControllerConnAuxId

                The Object 
                INT4 *pi4RetValFsofcControllerRole
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsofcControllerRole (UINT4 u4FsofcContextId,
                           INT4 i4FsofcControllerIpAddrType,
                           tSNMP_OCTET_STRING_TYPE * pFsofcControllerIpAddress,
                           INT4 i4FsofcControllerConnAuxId,
                           INT4 *pi4RetValFsofcControllerRole)
{
    tOfcFsofcControllerConnEntry *pOfcFsofcControllerConnEntry = NULL;

    pOfcFsofcControllerConnEntry =
        (tOfcFsofcControllerConnEntry *)
        MemAllocMemBlk (OFC_FSOFCCONTROLLERCONNTABLE_POOLID);

    if (pOfcFsofcControllerConnEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pOfcFsofcControllerConnEntry, OFC_ZERO,
            sizeof (tOfcFsofcControllerConnEntry));

    /* Assign the index */
    pOfcFsofcControllerConnEntry->MibObject.u4FsofcContextId = u4FsofcContextId;

    pOfcFsofcControllerConnEntry->MibObject.i4FsofcControllerIpAddrType =
        i4FsofcControllerIpAddrType;

    MEMCPY (pOfcFsofcControllerConnEntry->MibObject.au1FsofcControllerIpAddress,
            pFsofcControllerIpAddress->pu1_OctetList,
            pFsofcControllerIpAddress->i4_Length);

    pOfcFsofcControllerConnEntry->MibObject.i4FsofcControllerIpAddressLen =
        pFsofcControllerIpAddress->i4_Length;
    pOfcFsofcControllerConnEntry->MibObject.i4FsofcControllerConnAuxId =
        i4FsofcControllerConnAuxId;

    if (OfcGetAllFsofcControllerConnTable (pOfcFsofcControllerConnEntry) !=
        OSIX_SUCCESS)
    {
        MemReleaseMemBlock (OFC_FSOFCCONTROLLERCONNTABLE_POOLID,
                            (UINT1 *) pOfcFsofcControllerConnEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pi4RetValFsofcControllerRole =
        pOfcFsofcControllerConnEntry->MibObject.i4FsofcControllerRole;

    MemReleaseMemBlock (OFC_FSOFCCONTROLLERCONNTABLE_POOLID,
                        (UINT1 *) pOfcFsofcControllerConnEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsofcControllerConnState
 Input       :  The Indices
                FsofcContextId
                FsofcControllerIpAddrType
                FsofcControllerIpAddress
                FsofcControllerConnAuxId

                The Object 
                INT4 *pi4RetValFsofcControllerConnState
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsofcControllerConnState (UINT4 u4FsofcContextId,
                                INT4 i4FsofcControllerIpAddrType,
                                tSNMP_OCTET_STRING_TYPE *
                                pFsofcControllerIpAddress,
                                INT4 i4FsofcControllerConnAuxId,
                                INT4 *pi4RetValFsofcControllerConnState)
{
    tOfcFsofcControllerConnEntry *pOfcFsofcControllerConnEntry = NULL;

    pOfcFsofcControllerConnEntry =
        (tOfcFsofcControllerConnEntry *)
        MemAllocMemBlk (OFC_FSOFCCONTROLLERCONNTABLE_POOLID);

    if (pOfcFsofcControllerConnEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pOfcFsofcControllerConnEntry, OFC_ZERO,
            sizeof (tOfcFsofcControllerConnEntry));

    /* Assign the index */
    pOfcFsofcControllerConnEntry->MibObject.u4FsofcContextId = u4FsofcContextId;

    pOfcFsofcControllerConnEntry->MibObject.i4FsofcControllerIpAddrType =
        i4FsofcControllerIpAddrType;

    MEMCPY (pOfcFsofcControllerConnEntry->MibObject.au1FsofcControllerIpAddress,
            pFsofcControllerIpAddress->pu1_OctetList,
            pFsofcControllerIpAddress->i4_Length);

    pOfcFsofcControllerConnEntry->MibObject.i4FsofcControllerIpAddressLen =
        pFsofcControllerIpAddress->i4_Length;
    pOfcFsofcControllerConnEntry->MibObject.i4FsofcControllerConnAuxId =
        i4FsofcControllerConnAuxId;

    if (OfcGetAllFsofcControllerConnTable (pOfcFsofcControllerConnEntry) !=
        OSIX_SUCCESS)
    {
        MemReleaseMemBlock (OFC_FSOFCCONTROLLERCONNTABLE_POOLID,
                            (UINT1 *) pOfcFsofcControllerConnEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pi4RetValFsofcControllerConnState =
        pOfcFsofcControllerConnEntry->MibObject.i4FsofcControllerConnState;

    MemReleaseMemBlock (OFC_FSOFCCONTROLLERCONNTABLE_POOLID,
                        (UINT1 *) pOfcFsofcControllerConnEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsofcControllerConnEchoReqCount
 Input       :  The Indices
                FsofcContextId
                FsofcControllerIpAddrType
                FsofcControllerIpAddress
                FsofcControllerConnAuxId

                The Object 
                INT4 *pi4RetValFsofcControllerConnEchoReqCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsofcControllerConnEchoReqCount (UINT4 u4FsofcContextId,
                                       INT4 i4FsofcControllerIpAddrType,
                                       tSNMP_OCTET_STRING_TYPE *
                                       pFsofcControllerIpAddress,
                                       INT4 i4FsofcControllerConnAuxId,
                                       INT4
                                       *pi4RetValFsofcControllerConnEchoReqCount)
{
    tOfcFsofcControllerConnEntry *pOfcFsofcControllerConnEntry = NULL;

    pOfcFsofcControllerConnEntry =
        (tOfcFsofcControllerConnEntry *)
        MemAllocMemBlk (OFC_FSOFCCONTROLLERCONNTABLE_POOLID);

    if (pOfcFsofcControllerConnEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pOfcFsofcControllerConnEntry, OFC_ZERO,
            sizeof (tOfcFsofcControllerConnEntry));

    /* Assign the index */
    pOfcFsofcControllerConnEntry->MibObject.u4FsofcContextId = u4FsofcContextId;

    pOfcFsofcControllerConnEntry->MibObject.i4FsofcControllerIpAddrType =
        i4FsofcControllerIpAddrType;

    MEMCPY (pOfcFsofcControllerConnEntry->MibObject.au1FsofcControllerIpAddress,
            pFsofcControllerIpAddress->pu1_OctetList,
            pFsofcControllerIpAddress->i4_Length);

    pOfcFsofcControllerConnEntry->MibObject.i4FsofcControllerIpAddressLen =
        pFsofcControllerIpAddress->i4_Length;
    pOfcFsofcControllerConnEntry->MibObject.i4FsofcControllerConnAuxId =
        i4FsofcControllerConnAuxId;

    if (OfcGetAllFsofcControllerConnTable (pOfcFsofcControllerConnEntry) !=
        OSIX_SUCCESS)
    {
        MemReleaseMemBlock (OFC_FSOFCCONTROLLERCONNTABLE_POOLID,
                            (UINT1 *) pOfcFsofcControllerConnEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pi4RetValFsofcControllerConnEchoReqCount =
        pOfcFsofcControllerConnEntry->
        MibObject.i4FsofcControllerConnEchoReqCount;

    MemReleaseMemBlock (OFC_FSOFCCONTROLLERCONNTABLE_POOLID,
                        (UINT1 *) pOfcFsofcControllerConnEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsofcControllerConnEchoReplyCount
 Input       :  The Indices
                FsofcContextId
                FsofcControllerIpAddrType
                FsofcControllerIpAddress
                FsofcControllerConnAuxId

                The Object 
                INT4 *pi4RetValFsofcControllerConnEchoReplyCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsofcControllerConnEchoReplyCount (UINT4 u4FsofcContextId,
                                         INT4 i4FsofcControllerIpAddrType,
                                         tSNMP_OCTET_STRING_TYPE *
                                         pFsofcControllerIpAddress,
                                         INT4 i4FsofcControllerConnAuxId,
                                         INT4
                                         *pi4RetValFsofcControllerConnEchoReplyCount)
{
    tOfcFsofcControllerConnEntry *pOfcFsofcControllerConnEntry = NULL;

    pOfcFsofcControllerConnEntry =
        (tOfcFsofcControllerConnEntry *)
        MemAllocMemBlk (OFC_FSOFCCONTROLLERCONNTABLE_POOLID);

    if (pOfcFsofcControllerConnEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pOfcFsofcControllerConnEntry, OFC_ZERO,
            sizeof (tOfcFsofcControllerConnEntry));

    /* Assign the index */
    pOfcFsofcControllerConnEntry->MibObject.u4FsofcContextId = u4FsofcContextId;

    pOfcFsofcControllerConnEntry->MibObject.i4FsofcControllerIpAddrType =
        i4FsofcControllerIpAddrType;

    MEMCPY (pOfcFsofcControllerConnEntry->MibObject.au1FsofcControllerIpAddress,
            pFsofcControllerIpAddress->pu1_OctetList,
            pFsofcControllerIpAddress->i4_Length);

    pOfcFsofcControllerConnEntry->MibObject.i4FsofcControllerIpAddressLen =
        pFsofcControllerIpAddress->i4_Length;
    pOfcFsofcControllerConnEntry->MibObject.i4FsofcControllerConnAuxId =
        i4FsofcControllerConnAuxId;

    if (OfcGetAllFsofcControllerConnTable (pOfcFsofcControllerConnEntry) !=
        OSIX_SUCCESS)
    {
        MemReleaseMemBlock (OFC_FSOFCCONTROLLERCONNTABLE_POOLID,
                            (UINT1 *) pOfcFsofcControllerConnEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pi4RetValFsofcControllerConnEchoReplyCount =
        pOfcFsofcControllerConnEntry->
        MibObject.i4FsofcControllerConnEchoReplyCount;

    MemReleaseMemBlock (OFC_FSOFCCONTROLLERCONNTABLE_POOLID,
                        (UINT1 *) pOfcFsofcControllerConnEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsofcControllerConnEntryStatus
 Input       :  The Indices
                FsofcContextId
                FsofcControllerIpAddrType
                FsofcControllerIpAddress
                FsofcControllerConnAuxId

                The Object 
                INT4 *pi4RetValFsofcControllerConnEntryStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsofcControllerConnEntryStatus (UINT4 u4FsofcContextId,
                                      INT4 i4FsofcControllerIpAddrType,
                                      tSNMP_OCTET_STRING_TYPE *
                                      pFsofcControllerIpAddress,
                                      INT4 i4FsofcControllerConnAuxId,
                                      INT4
                                      *pi4RetValFsofcControllerConnEntryStatus)
{
    tOfcFsofcControllerConnEntry *pOfcFsofcControllerConnEntry = NULL;

    pOfcFsofcControllerConnEntry =
        (tOfcFsofcControllerConnEntry *)
        MemAllocMemBlk (OFC_FSOFCCONTROLLERCONNTABLE_POOLID);

    if (pOfcFsofcControllerConnEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pOfcFsofcControllerConnEntry, OFC_ZERO,
            sizeof (tOfcFsofcControllerConnEntry));

    /* Assign the index */
    pOfcFsofcControllerConnEntry->MibObject.u4FsofcContextId = u4FsofcContextId;

    pOfcFsofcControllerConnEntry->MibObject.i4FsofcControllerIpAddrType =
        i4FsofcControllerIpAddrType;

    MEMCPY (pOfcFsofcControllerConnEntry->MibObject.au1FsofcControllerIpAddress,
            pFsofcControllerIpAddress->pu1_OctetList,
            pFsofcControllerIpAddress->i4_Length);

    pOfcFsofcControllerConnEntry->MibObject.i4FsofcControllerIpAddressLen =
        pFsofcControllerIpAddress->i4_Length;
    pOfcFsofcControllerConnEntry->MibObject.i4FsofcControllerConnAuxId =
        i4FsofcControllerConnAuxId;

    if (OfcGetAllFsofcControllerConnTable (pOfcFsofcControllerConnEntry) !=
        OSIX_SUCCESS)
    {
        MemReleaseMemBlock (OFC_FSOFCCONTROLLERCONNTABLE_POOLID,
                            (UINT1 *) pOfcFsofcControllerConnEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pi4RetValFsofcControllerConnEntryStatus =
        pOfcFsofcControllerConnEntry->
        MibObject.i4FsofcControllerConnEntryStatus;

    MemReleaseMemBlock (OFC_FSOFCCONTROLLERCONNTABLE_POOLID,
                        (UINT1 *) pOfcFsofcControllerConnEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsofcControllerConnBand
 Input       :  The Indices
                FsofcContextId
                FsofcControllerIpAddrType
                FsofcControllerIpAddress
                FsofcControllerConnAuxId

                The Object 
                INT4 *pi4RetValFsofcControllerConnBand
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsofcControllerConnBand (UINT4 u4FsofcContextId,
                               INT4 i4FsofcControllerIpAddrType,
                               tSNMP_OCTET_STRING_TYPE *
                               pFsofcControllerIpAddress,
                               INT4 i4FsofcControllerConnAuxId,
                               INT4 *pi4RetValFsofcControllerConnBand)
{
    tOfcFsofcControllerConnEntry *pOfcFsofcControllerConnEntry = NULL;

    pOfcFsofcControllerConnEntry =
        (tOfcFsofcControllerConnEntry *)
        MemAllocMemBlk (OFC_FSOFCCONTROLLERCONNTABLE_POOLID);

    if (pOfcFsofcControllerConnEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pOfcFsofcControllerConnEntry, 0,
            sizeof (tOfcFsofcControllerConnEntry));

    /* Assign the index */
    pOfcFsofcControllerConnEntry->MibObject.u4FsofcContextId = u4FsofcContextId;

    pOfcFsofcControllerConnEntry->MibObject.i4FsofcControllerIpAddrType =
        i4FsofcControllerIpAddrType;

    MEMCPY (pOfcFsofcControllerConnEntry->MibObject.au1FsofcControllerIpAddress,
            pFsofcControllerIpAddress->pu1_OctetList,
            pFsofcControllerIpAddress->i4_Length);

    pOfcFsofcControllerConnEntry->MibObject.i4FsofcControllerIpAddressLen =
        pFsofcControllerIpAddress->i4_Length;
    pOfcFsofcControllerConnEntry->MibObject.i4FsofcControllerConnAuxId =
        i4FsofcControllerConnAuxId;

    if (OfcGetAllFsofcControllerConnTable (pOfcFsofcControllerConnEntry) !=
        OSIX_SUCCESS)
    {
        MemReleaseMemBlock (OFC_FSOFCCONTROLLERCONNTABLE_POOLID,
                            (UINT1 *) pOfcFsofcControllerConnEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pi4RetValFsofcControllerConnBand =
        pOfcFsofcControllerConnEntry->MibObject.i4FsofcControllerConnBand;

    MemReleaseMemBlock (OFC_FSOFCCONTROLLERCONNTABLE_POOLID,
                        (UINT1 *) pOfcFsofcControllerConnEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsofcIfType
 Input       :  The Indices
                FsofcContextId
                FsofcIfIndex

                The Object 
                INT4 *pi4RetValFsofcIfType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsofcIfType (UINT4 u4FsofcContextId, INT4 i4FsofcIfIndex,
                   INT4 *pi4RetValFsofcIfType)
{
    tOfcFsofcIfEntry   *pOfcFsofcIfEntry = NULL;

    pOfcFsofcIfEntry =
        (tOfcFsofcIfEntry *) MemAllocMemBlk (OFC_FSOFCIFTABLE_POOLID);

    if (pOfcFsofcIfEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pOfcFsofcIfEntry, OFC_ZERO, sizeof (tOfcFsofcIfEntry));

    /* Assign the index */
    pOfcFsofcIfEntry->MibObject.u4FsofcContextId = u4FsofcContextId;

    pOfcFsofcIfEntry->MibObject.i4FsofcIfIndex = i4FsofcIfIndex;

    if (OfcGetAllFsofcIfTable (pOfcFsofcIfEntry) != OSIX_SUCCESS)
    {
        MemReleaseMemBlock (OFC_FSOFCIFTABLE_POOLID,
                            (UINT1 *) pOfcFsofcIfEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pi4RetValFsofcIfType = pOfcFsofcIfEntry->MibObject.i4FsofcIfType;

    MemReleaseMemBlock (OFC_FSOFCIFTABLE_POOLID, (UINT1 *) pOfcFsofcIfEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsofcIfAlias
 Input       :  The Indices
                FsofcContextId
                FsofcIfIndex

                The Object 
                tSNMP_OCTET_STRING_TYPE * pRetValFsofcIfAlias
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsofcIfAlias (UINT4 u4FsofcContextId, INT4 i4FsofcIfIndex,
                    tSNMP_OCTET_STRING_TYPE * pRetValFsofcIfAlias)
{
    tOfcFsofcIfEntry   *pOfcFsofcIfEntry = NULL;

    pOfcFsofcIfEntry =
        (tOfcFsofcIfEntry *) MemAllocMemBlk (OFC_FSOFCIFTABLE_POOLID);

    if (pOfcFsofcIfEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pOfcFsofcIfEntry, OFC_ZERO, sizeof (tOfcFsofcIfEntry));

    /* Assign the index */
    pOfcFsofcIfEntry->MibObject.u4FsofcContextId = u4FsofcContextId;

    pOfcFsofcIfEntry->MibObject.i4FsofcIfIndex = i4FsofcIfIndex;

    if (OfcGetAllFsofcIfTable (pOfcFsofcIfEntry) != OSIX_SUCCESS)
    {
        MemReleaseMemBlock (OFC_FSOFCIFTABLE_POOLID,
                            (UINT1 *) pOfcFsofcIfEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    MEMCPY (pRetValFsofcIfAlias->pu1_OctetList,
            pOfcFsofcIfEntry->MibObject.au1FsofcIfAlias,
            pOfcFsofcIfEntry->MibObject.i4FsofcIfAliasLen);
    pRetValFsofcIfAlias->i4_Length =
        pOfcFsofcIfEntry->MibObject.i4FsofcIfAliasLen;

    MemReleaseMemBlock (OFC_FSOFCIFTABLE_POOLID, (UINT1 *) pOfcFsofcIfEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsofcIfOperStatus
 Input       :  The Indices
                FsofcContextId
                FsofcIfIndex

                The Object 
                INT4 *pi4RetValFsofcIfOperStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsofcIfOperStatus (UINT4 u4FsofcContextId, INT4 i4FsofcIfIndex,
                         INT4 *pi4RetValFsofcIfOperStatus)
{
    tOfcFsofcIfEntry   *pOfcFsofcIfEntry = NULL;

    pOfcFsofcIfEntry =
        (tOfcFsofcIfEntry *) MemAllocMemBlk (OFC_FSOFCIFTABLE_POOLID);

    if (pOfcFsofcIfEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pOfcFsofcIfEntry, OFC_ZERO, sizeof (tOfcFsofcIfEntry));

    /* Assign the index */
    pOfcFsofcIfEntry->MibObject.u4FsofcContextId = u4FsofcContextId;

    pOfcFsofcIfEntry->MibObject.i4FsofcIfIndex = i4FsofcIfIndex;

    if (OfcGetAllFsofcIfTable (pOfcFsofcIfEntry) != OSIX_SUCCESS)
    {
        MemReleaseMemBlock (OFC_FSOFCIFTABLE_POOLID,
                            (UINT1 *) pOfcFsofcIfEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pi4RetValFsofcIfOperStatus =
        pOfcFsofcIfEntry->MibObject.i4FsofcIfOperStatus;

    MemReleaseMemBlock (OFC_FSOFCIFTABLE_POOLID, (UINT1 *) pOfcFsofcIfEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsofcVlanEgressPorts
 Input       :  The Indices
                FsofcContextId
                FsofcIfIndex

                The Object 
                tSNMP_OCTET_STRING_TYPE * pRetValFsofcVlanEgressPorts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsofcVlanEgressPorts (UINT4 u4FsofcContextId, INT4 i4FsofcIfIndex,
                            tSNMP_OCTET_STRING_TYPE *
                            pRetValFsofcVlanEgressPorts)
{
    tOfcFsofcIfEntry   *pOfcFsofcIfEntry = NULL;

    pOfcFsofcIfEntry =
        (tOfcFsofcIfEntry *) MemAllocMemBlk (OFC_FSOFCIFTABLE_POOLID);

    if (pOfcFsofcIfEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pOfcFsofcIfEntry, OFC_ZERO, sizeof (tOfcFsofcIfEntry));

    /* Assign the index */
    pOfcFsofcIfEntry->MibObject.u4FsofcContextId = u4FsofcContextId;

    pOfcFsofcIfEntry->MibObject.i4FsofcIfIndex = i4FsofcIfIndex;

    if (OfcGetAllFsofcIfTable (pOfcFsofcIfEntry) != OSIX_SUCCESS)
    {
        MemReleaseMemBlock (OFC_FSOFCIFTABLE_POOLID,
                            (UINT1 *) pOfcFsofcIfEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    MEMCPY (pRetValFsofcVlanEgressPorts->pu1_OctetList,
            pOfcFsofcIfEntry->MibObject.au1FsofcVlanEgressPorts,
            pOfcFsofcIfEntry->MibObject.i4FsofcVlanEgressPortsLen);
    pRetValFsofcVlanEgressPorts->i4_Length =
        pOfcFsofcIfEntry->MibObject.i4FsofcVlanEgressPortsLen;

    MemReleaseMemBlock (OFC_FSOFCIFTABLE_POOLID, (UINT1 *) pOfcFsofcIfEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsofcVlanUntaggedPorts
 Input       :  The Indices
                FsofcContextId
                FsofcIfIndex

                The Object 
                tSNMP_OCTET_STRING_TYPE * pRetValFsofcVlanUntaggedPorts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsofcVlanUntaggedPorts (UINT4 u4FsofcContextId, INT4 i4FsofcIfIndex,
                              tSNMP_OCTET_STRING_TYPE *
                              pRetValFsofcVlanUntaggedPorts)
{
    tOfcFsofcIfEntry   *pOfcFsofcIfEntry = NULL;

    pOfcFsofcIfEntry =
        (tOfcFsofcIfEntry *) MemAllocMemBlk (OFC_FSOFCIFTABLE_POOLID);

    if (pOfcFsofcIfEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pOfcFsofcIfEntry, OFC_ZERO, sizeof (tOfcFsofcIfEntry));

    /* Assign the index */
    pOfcFsofcIfEntry->MibObject.u4FsofcContextId = u4FsofcContextId;

    pOfcFsofcIfEntry->MibObject.i4FsofcIfIndex = i4FsofcIfIndex;

    if (OfcGetAllFsofcIfTable (pOfcFsofcIfEntry) != OSIX_SUCCESS)
    {
        MemReleaseMemBlock (OFC_FSOFCIFTABLE_POOLID,
                            (UINT1 *) pOfcFsofcIfEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    MEMCPY (pRetValFsofcVlanUntaggedPorts->pu1_OctetList,
            pOfcFsofcIfEntry->MibObject.au1FsofcVlanUntaggedPorts,
            pOfcFsofcIfEntry->MibObject.i4FsofcVlanUntaggedPortsLen);
    pRetValFsofcVlanUntaggedPorts->i4_Length =
        pOfcFsofcIfEntry->MibObject.i4FsofcVlanUntaggedPortsLen;

    MemReleaseMemBlock (OFC_FSOFCIFTABLE_POOLID, (UINT1 *) pOfcFsofcIfEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsofcVlanInFrames
 Input       :  The Indices
                FsofcContextId
                FsofcIfIndex

                The Object 
                UINT4 *pu4RetValFsofcVlanInFrames
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsofcVlanInFrames (UINT4 u4FsofcContextId, INT4 i4FsofcIfIndex,
                         UINT4 *pu4RetValFsofcVlanInFrames)
{
    tOfcFsofcIfEntry   *pOfcFsofcIfEntry = NULL;

    pOfcFsofcIfEntry =
        (tOfcFsofcIfEntry *) MemAllocMemBlk (OFC_FSOFCIFTABLE_POOLID);

    if (pOfcFsofcIfEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pOfcFsofcIfEntry, OFC_ZERO, sizeof (tOfcFsofcIfEntry));

    /* Assign the index */
    pOfcFsofcIfEntry->MibObject.u4FsofcContextId = u4FsofcContextId;

    pOfcFsofcIfEntry->MibObject.i4FsofcIfIndex = i4FsofcIfIndex;

    if (OfcGetAllFsofcIfTable (pOfcFsofcIfEntry) != OSIX_SUCCESS)
    {
        MemReleaseMemBlock (OFC_FSOFCIFTABLE_POOLID,
                            (UINT1 *) pOfcFsofcIfEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pu4RetValFsofcVlanInFrames =
        pOfcFsofcIfEntry->MibObject.u4FsofcVlanInFrames;

    MemReleaseMemBlock (OFC_FSOFCIFTABLE_POOLID, (UINT1 *) pOfcFsofcIfEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsofcVlanOutFrames
 Input       :  The Indices
                FsofcContextId
                FsofcIfIndex

                The Object 
                UINT4 *pu4RetValFsofcVlanOutFrames
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsofcVlanOutFrames (UINT4 u4FsofcContextId, INT4 i4FsofcIfIndex,
                          UINT4 *pu4RetValFsofcVlanOutFrames)
{
    tOfcFsofcIfEntry   *pOfcFsofcIfEntry = NULL;

    pOfcFsofcIfEntry =
        (tOfcFsofcIfEntry *) MemAllocMemBlk (OFC_FSOFCIFTABLE_POOLID);

    if (pOfcFsofcIfEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pOfcFsofcIfEntry, OFC_ZERO, sizeof (tOfcFsofcIfEntry));

    /* Assign the index */
    pOfcFsofcIfEntry->MibObject.u4FsofcContextId = u4FsofcContextId;

    pOfcFsofcIfEntry->MibObject.i4FsofcIfIndex = i4FsofcIfIndex;

    if (OfcGetAllFsofcIfTable (pOfcFsofcIfEntry) != OSIX_SUCCESS)
    {
        MemReleaseMemBlock (OFC_FSOFCIFTABLE_POOLID,
                            (UINT1 *) pOfcFsofcIfEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pu4RetValFsofcVlanOutFrames =
        pOfcFsofcIfEntry->MibObject.u4FsofcVlanOutFrames;

    MemReleaseMemBlock (OFC_FSOFCIFTABLE_POOLID, (UINT1 *) pOfcFsofcIfEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsofcIfContextId
 Input       :  The Indices
                FsofcContextId
                FsofcIfIndex

                The Object 
                UINT4 *pu4RetValFsofcIfContextId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsofcIfContextId (UINT4 u4FsofcContextId, INT4 i4FsofcIfIndex,
                        UINT4 *pu4RetValFsofcIfContextId)
{
    tOfcFsofcIfEntry   *pOfcFsofcIfEntry = NULL;

    pOfcFsofcIfEntry =
        (tOfcFsofcIfEntry *) MemAllocMemBlk (OFC_FSOFCIFTABLE_POOLID);

    if (pOfcFsofcIfEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pOfcFsofcIfEntry, 0, sizeof (tOfcFsofcIfEntry));

    /* Assign the index */
    pOfcFsofcIfEntry->MibObject.u4FsofcContextId = u4FsofcContextId;

    pOfcFsofcIfEntry->MibObject.i4FsofcIfIndex = i4FsofcIfIndex;

    if (OfcGetAllFsofcIfTable (pOfcFsofcIfEntry) != OSIX_SUCCESS)
    {
        MemReleaseMemBlock (OFC_FSOFCIFTABLE_POOLID,
                            (UINT1 *) pOfcFsofcIfEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pu4RetValFsofcIfContextId = pOfcFsofcIfEntry->MibObject.u4FsofcIfContextId;

    MemReleaseMemBlock (OFC_FSOFCIFTABLE_POOLID, (UINT1 *) pOfcFsofcIfEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsofcFlowMatchField
 Input       :  The Indices
                FsofcContextId
                FsofcTableIndex
                FsofcFlowIndex

                The Object 
                tSNMP_OCTET_STRING_TYPE * pRetValFsofcFlowMatchField
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsofcFlowMatchField (UINT4 u4FsofcContextId, UINT4 u4FsofcTableIndex,
                           UINT4 u4FsofcFlowIndex,
                           tSNMP_OCTET_STRING_TYPE * pRetValFsofcFlowMatchField)
{
    tOfcFsofcFlowEntry  OfcFsofcFlowEntry;

    MEMSET (&OfcFsofcFlowEntry, OFC_ZERO, sizeof (tOfcFsofcFlowEntry));

    /* Assign the index */
    OfcFsofcFlowEntry.MibObject.u4FsofcContextId = u4FsofcContextId;
    OfcFsofcFlowEntry.MibObject.u4FsofcTableIndex = u4FsofcTableIndex;
    OfcFsofcFlowEntry.MibObject.u4FsofcFlowIndex = u4FsofcFlowIndex;

    if (OfcGetAllFsofcFlowTable (&OfcFsofcFlowEntry) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    /* Assign the value */
    MEMCPY (pRetValFsofcFlowMatchField->pu1_OctetList,
            OfcFsofcFlowEntry.MibObject.au1FsofcFlowMatchField,
            OfcFsofcFlowEntry.MibObject.i4FsofcFlowMatchFieldLen);
    pRetValFsofcFlowMatchField->i4_Length =
        OfcFsofcFlowEntry.MibObject.i4FsofcFlowMatchFieldLen;

    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsofcFlowOutputAction
 Input       :  The Indices
                FsofcContextId
                FsofcTableIndex
                FsofcFlowIndex

                The Object 
                tSNMP_OCTET_STRING_TYPE * pRetValFsofcFlowOutputAction
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsofcFlowOutputAction (UINT4 u4FsofcContextId, UINT4 u4FsofcTableIndex,
                             UINT4 u4FsofcFlowIndex,
                             tSNMP_OCTET_STRING_TYPE *
                             pRetValFsofcFlowOutputAction)
{
    tOfcFsofcFlowEntry  OfcFsofcFlowEntry;

    MEMSET (&OfcFsofcFlowEntry, OFC_ZERO, sizeof (tOfcFsofcFlowEntry));

    /* Assign the index */
    OfcFsofcFlowEntry.MibObject.u4FsofcContextId = u4FsofcContextId;
    OfcFsofcFlowEntry.MibObject.u4FsofcTableIndex = u4FsofcTableIndex;
    OfcFsofcFlowEntry.MibObject.u4FsofcFlowIndex = u4FsofcFlowIndex;

    if (OfcGetAllFsofcFlowTable (&OfcFsofcFlowEntry) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    /* Assign the value */
    MEMCPY (pRetValFsofcFlowOutputAction->pu1_OctetList,
            OfcFsofcFlowEntry.MibObject.au1FsofcFlowOutputAction,
            OfcFsofcFlowEntry.MibObject.i4FsofcFlowOutputActionLen);
    pRetValFsofcFlowOutputAction->i4_Length =
        OfcFsofcFlowEntry.MibObject.i4FsofcFlowOutputActionLen;

    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsofcFlowIdleTimeout
 Input       :  The Indices
                FsofcContextId
                FsofcTableIndex
                FsofcFlowIndex

                The Object 
                UINT4 *pu4RetValFsofcFlowIdleTimeout
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsofcFlowIdleTimeout (UINT4 u4FsofcContextId, UINT4 u4FsofcTableIndex,
                            UINT4 u4FsofcFlowIndex,
                            UINT4 *pu4RetValFsofcFlowIdleTimeout)
{
    tOfcFsofcFlowEntry  OfcFsofcFlowEntry;

    MEMSET (&OfcFsofcFlowEntry, OFC_ZERO, sizeof (tOfcFsofcFlowEntry));

    /* Assign the index */
    OfcFsofcFlowEntry.MibObject.u4FsofcContextId = u4FsofcContextId;
    OfcFsofcFlowEntry.MibObject.u4FsofcTableIndex = u4FsofcTableIndex;
    OfcFsofcFlowEntry.MibObject.u4FsofcFlowIndex = u4FsofcFlowIndex;

    if (OfcGetAllFsofcFlowTable (&OfcFsofcFlowEntry) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    /* Assign the value */
    *pu4RetValFsofcFlowIdleTimeout =
        OfcFsofcFlowEntry.MibObject.u4FsofcFlowIdleTimeout;

    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsofcFlowHardTimeout
 Input       :  The Indices
                FsofcContextId
                FsofcTableIndex
                FsofcFlowIndex

                The Object 
                UINT4 *pu4RetValFsofcFlowHardTimeout
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsofcFlowHardTimeout (UINT4 u4FsofcContextId, UINT4 u4FsofcTableIndex,
                            UINT4 u4FsofcFlowIndex,
                            UINT4 *pu4RetValFsofcFlowHardTimeout)
{
    tOfcFsofcFlowEntry  OfcFsofcFlowEntry;

    MEMSET (&OfcFsofcFlowEntry, OFC_ZERO, sizeof (tOfcFsofcFlowEntry));

    /* Assign the index */
    OfcFsofcFlowEntry.MibObject.u4FsofcContextId = u4FsofcContextId;
    OfcFsofcFlowEntry.MibObject.u4FsofcTableIndex = u4FsofcTableIndex;
    OfcFsofcFlowEntry.MibObject.u4FsofcFlowIndex = u4FsofcFlowIndex;

    if (OfcGetAllFsofcFlowTable (&OfcFsofcFlowEntry) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    /* Assign the value */
    *pu4RetValFsofcFlowHardTimeout =
        OfcFsofcFlowEntry.MibObject.u4FsofcFlowHardTimeout;

    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsofcFlowPacketCount
 Input       :  The Indices
                FsofcContextId
                FsofcTableIndex
                FsofcFlowIndex

                The Object 
                tSNMP_COUNTER64_TYPE *pu8RetValFsofcFlowPacketCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsofcFlowPacketCount (UINT4 u4FsofcContextId, UINT4 u4FsofcTableIndex,
                            UINT4 u4FsofcFlowIndex,
                            tSNMP_COUNTER64_TYPE *
                            pu8RetValFsofcFlowPacketCount)
{
    tOfcFsofcFlowEntry  OfcFsofcFlowEntry;

    MEMSET (&OfcFsofcFlowEntry, OFC_ZERO, sizeof (tOfcFsofcFlowEntry));

    /* Assign the index */
    OfcFsofcFlowEntry.MibObject.u4FsofcContextId = u4FsofcContextId;
    OfcFsofcFlowEntry.MibObject.u4FsofcTableIndex = u4FsofcTableIndex;
    OfcFsofcFlowEntry.MibObject.u4FsofcFlowIndex = u4FsofcFlowIndex;

    if (OfcGetAllFsofcFlowTable (&OfcFsofcFlowEntry) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    /* Assign the value */
    *pu8RetValFsofcFlowPacketCount =
        OfcFsofcFlowEntry.MibObject.u8FsofcFlowPacketCount;

    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsofcFlowByteCount
 Input       :  The Indices
                FsofcContextId
                FsofcTableIndex
                FsofcFlowIndex

                The Object 
                tSNMP_COUNTER64_TYPE *pu8RetValFsofcFlowByteCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsofcFlowByteCount (UINT4 u4FsofcContextId, UINT4 u4FsofcTableIndex,
                          UINT4 u4FsofcFlowIndex,
                          tSNMP_COUNTER64_TYPE * pu8RetValFsofcFlowByteCount)
{
    tOfcFsofcFlowEntry  OfcFsofcFlowEntry;

    MEMSET (&OfcFsofcFlowEntry, OFC_ZERO, sizeof (tOfcFsofcFlowEntry));

    /* Assign the index */
    OfcFsofcFlowEntry.MibObject.u4FsofcContextId = u4FsofcContextId;
    OfcFsofcFlowEntry.MibObject.u4FsofcTableIndex = u4FsofcTableIndex;
    OfcFsofcFlowEntry.MibObject.u4FsofcFlowIndex = u4FsofcFlowIndex;

    if (OfcGetAllFsofcFlowTable (&OfcFsofcFlowEntry) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    /* Assign the value */
    *pu8RetValFsofcFlowByteCount =
        OfcFsofcFlowEntry.MibObject.u8FsofcFlowByteCount;

    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsofcFlowDurationSec
 Input       :  The Indices
                FsofcContextId
                FsofcTableIndex
                FsofcFlowIndex

                The Object 
                UINT4 *pu4RetValFsofcFlowDurationSec
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsofcFlowDurationSec (UINT4 u4FsofcContextId, UINT4 u4FsofcTableIndex,
                            UINT4 u4FsofcFlowIndex,
                            UINT4 *pu4RetValFsofcFlowDurationSec)
{
    tOfcFsofcFlowEntry  OfcFsofcFlowEntry;

    MEMSET (&OfcFsofcFlowEntry, OFC_ZERO, sizeof (tOfcFsofcFlowEntry));

    /* Assign the index */
    OfcFsofcFlowEntry.MibObject.u4FsofcContextId = u4FsofcContextId;
    OfcFsofcFlowEntry.MibObject.u4FsofcTableIndex = u4FsofcTableIndex;
    OfcFsofcFlowEntry.MibObject.u4FsofcFlowIndex = u4FsofcFlowIndex;

    if (OfcGetAllFsofcFlowTable (&OfcFsofcFlowEntry) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    /* Assign the value */
    *pu4RetValFsofcFlowDurationSec =
        OfcFsofcFlowEntry.MibObject.u4FsofcFlowDurationSec;

    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsofcGroupType
 Input       :  The Indices
                FsofcContextId
                FsofcGroupIndex

                The Object 
                INT4 *pi4RetValFsofcGroupType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsofcGroupType (UINT4 u4FsofcContextId, UINT4 u4FsofcGroupIndex,
                      INT4 *pi4RetValFsofcGroupType)
{
    tOfcFsofcGroupEntry *pOfcFsofcGroupEntry = NULL;

    pOfcFsofcGroupEntry =
        (tOfcFsofcGroupEntry *) MemAllocMemBlk (OFC_FSOFCGROUPTABLE_POOLID);

    if (pOfcFsofcGroupEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pOfcFsofcGroupEntry, OFC_ZERO, sizeof (tOfcFsofcGroupEntry));

    /* Assign the index */
    pOfcFsofcGroupEntry->MibObject.u4FsofcContextId = u4FsofcContextId;

    pOfcFsofcGroupEntry->MibObject.u4FsofcGroupIndex = u4FsofcGroupIndex;

    if (OfcGetAllFsofcGroupTable (pOfcFsofcGroupEntry) != OSIX_SUCCESS)
    {
        MemReleaseMemBlock (OFC_FSOFCGROUPTABLE_POOLID,
                            (UINT1 *) pOfcFsofcGroupEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pi4RetValFsofcGroupType = pOfcFsofcGroupEntry->MibObject.i4FsofcGroupType;

    MemReleaseMemBlock (OFC_FSOFCGROUPTABLE_POOLID,
                        (UINT1 *) pOfcFsofcGroupEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsofcGroupActionBuckets
 Input       :  The Indices
                FsofcContextId
                FsofcGroupIndex

                The Object 
                tSNMP_OCTET_STRING_TYPE * pRetValFsofcGroupActionBuckets
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsofcGroupActionBuckets (UINT4 u4FsofcContextId, UINT4 u4FsofcGroupIndex,
                               tSNMP_OCTET_STRING_TYPE *
                               pRetValFsofcGroupActionBuckets)
{
    tOfcFsofcGroupEntry *pOfcFsofcGroupEntry = NULL;

    pOfcFsofcGroupEntry =
        (tOfcFsofcGroupEntry *) MemAllocMemBlk (OFC_FSOFCGROUPTABLE_POOLID);

    if (pOfcFsofcGroupEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pOfcFsofcGroupEntry, OFC_ZERO, sizeof (tOfcFsofcGroupEntry));

    /* Assign the index */
    pOfcFsofcGroupEntry->MibObject.u4FsofcContextId = u4FsofcContextId;

    pOfcFsofcGroupEntry->MibObject.u4FsofcGroupIndex = u4FsofcGroupIndex;

    if (OfcGetAllFsofcGroupTable (pOfcFsofcGroupEntry) != OSIX_SUCCESS)
    {
        MemReleaseMemBlock (OFC_FSOFCGROUPTABLE_POOLID,
                            (UINT1 *) pOfcFsofcGroupEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    MEMCPY (pRetValFsofcGroupActionBuckets->pu1_OctetList,
            pOfcFsofcGroupEntry->MibObject.au1FsofcGroupActionBuckets,
            pOfcFsofcGroupEntry->MibObject.i4FsofcGroupActionBucketsLen);
    pRetValFsofcGroupActionBuckets->i4_Length =
        pOfcFsofcGroupEntry->MibObject.i4FsofcGroupActionBucketsLen;

    MemReleaseMemBlock (OFC_FSOFCGROUPTABLE_POOLID,
                        (UINT1 *) pOfcFsofcGroupEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsofcGroupPacketCount
 Input       :  The Indices
                FsofcContextId
                FsofcGroupIndex

                The Object 
                tSNMP_COUNTER64_TYPE *pu8RetValFsofcGroupPacketCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsofcGroupPacketCount (UINT4 u4FsofcContextId, UINT4 u4FsofcGroupIndex,
                             tSNMP_COUNTER64_TYPE *
                             pu8RetValFsofcGroupPacketCount)
{
    tOfcFsofcGroupEntry *pOfcFsofcGroupEntry = NULL;

    pOfcFsofcGroupEntry =
        (tOfcFsofcGroupEntry *) MemAllocMemBlk (OFC_FSOFCGROUPTABLE_POOLID);

    if (pOfcFsofcGroupEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pOfcFsofcGroupEntry, OFC_ZERO, sizeof (tOfcFsofcGroupEntry));

    /* Assign the index */
    pOfcFsofcGroupEntry->MibObject.u4FsofcContextId = u4FsofcContextId;

    pOfcFsofcGroupEntry->MibObject.u4FsofcGroupIndex = u4FsofcGroupIndex;

    if (OfcGetAllFsofcGroupTable (pOfcFsofcGroupEntry) != OSIX_SUCCESS)
    {
        MemReleaseMemBlock (OFC_FSOFCGROUPTABLE_POOLID,
                            (UINT1 *) pOfcFsofcGroupEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pu8RetValFsofcGroupPacketCount =
        pOfcFsofcGroupEntry->MibObject.u8FsofcGroupPacketCount;

    MemReleaseMemBlock (OFC_FSOFCGROUPTABLE_POOLID,
                        (UINT1 *) pOfcFsofcGroupEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsofcGroupByteCount
 Input       :  The Indices
                FsofcContextId
                FsofcGroupIndex

                The Object 
                tSNMP_COUNTER64_TYPE *pu8RetValFsofcGroupByteCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsofcGroupByteCount (UINT4 u4FsofcContextId, UINT4 u4FsofcGroupIndex,
                           tSNMP_COUNTER64_TYPE * pu8RetValFsofcGroupByteCount)
{
    tOfcFsofcGroupEntry *pOfcFsofcGroupEntry = NULL;

    pOfcFsofcGroupEntry =
        (tOfcFsofcGroupEntry *) MemAllocMemBlk (OFC_FSOFCGROUPTABLE_POOLID);

    if (pOfcFsofcGroupEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pOfcFsofcGroupEntry, OFC_ZERO, sizeof (tOfcFsofcGroupEntry));

    /* Assign the index */
    pOfcFsofcGroupEntry->MibObject.u4FsofcContextId = u4FsofcContextId;

    pOfcFsofcGroupEntry->MibObject.u4FsofcGroupIndex = u4FsofcGroupIndex;

    if (OfcGetAllFsofcGroupTable (pOfcFsofcGroupEntry) != OSIX_SUCCESS)
    {
        MemReleaseMemBlock (OFC_FSOFCGROUPTABLE_POOLID,
                            (UINT1 *) pOfcFsofcGroupEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pu8RetValFsofcGroupByteCount =
        pOfcFsofcGroupEntry->MibObject.u8FsofcGroupByteCount;

    MemReleaseMemBlock (OFC_FSOFCGROUPTABLE_POOLID,
                        (UINT1 *) pOfcFsofcGroupEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsofcGroupDurationSec
 Input       :  The Indices
                FsofcContextId
                FsofcGroupIndex

                The Object 
                UINT4 *pu4RetValFsofcGroupDurationSec
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsofcGroupDurationSec (UINT4 u4FsofcContextId, UINT4 u4FsofcGroupIndex,
                             UINT4 *pu4RetValFsofcGroupDurationSec)
{
    tOfcFsofcGroupEntry *pOfcFsofcGroupEntry = NULL;

    pOfcFsofcGroupEntry =
        (tOfcFsofcGroupEntry *) MemAllocMemBlk (OFC_FSOFCGROUPTABLE_POOLID);

    if (pOfcFsofcGroupEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pOfcFsofcGroupEntry, OFC_ZERO, sizeof (tOfcFsofcGroupEntry));

    /* Assign the index */
    pOfcFsofcGroupEntry->MibObject.u4FsofcContextId = u4FsofcContextId;

    pOfcFsofcGroupEntry->MibObject.u4FsofcGroupIndex = u4FsofcGroupIndex;

    if (OfcGetAllFsofcGroupTable (pOfcFsofcGroupEntry) != OSIX_SUCCESS)
    {
        MemReleaseMemBlock (OFC_FSOFCGROUPTABLE_POOLID,
                            (UINT1 *) pOfcFsofcGroupEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pu4RetValFsofcGroupDurationSec =
        pOfcFsofcGroupEntry->MibObject.u4FsofcGroupDurationSec;

    MemReleaseMemBlock (OFC_FSOFCGROUPTABLE_POOLID,
                        (UINT1 *) pOfcFsofcGroupEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsofcMeterBandInfo
 Input       :  The Indices
                FsofcContextId
                FsofcMeterIndex

                The Object 
                tSNMP_OCTET_STRING_TYPE * pRetValFsofcMeterBandInfo
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsofcMeterBandInfo (UINT4 u4FsofcContextId, UINT4 u4FsofcMeterIndex,
                          tSNMP_OCTET_STRING_TYPE * pRetValFsofcMeterBandInfo)
{
    tOfcFsofcMeterEntry *pOfcFsofcMeterEntry = NULL;

    pOfcFsofcMeterEntry =
        (tOfcFsofcMeterEntry *) MemAllocMemBlk (OFC_FSOFCMETERTABLE_POOLID);

    if (pOfcFsofcMeterEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pOfcFsofcMeterEntry, OFC_ZERO, sizeof (tOfcFsofcMeterEntry));

    /* Assign the index */
    pOfcFsofcMeterEntry->MibObject.u4FsofcContextId = u4FsofcContextId;

    pOfcFsofcMeterEntry->MibObject.u4FsofcMeterIndex = u4FsofcMeterIndex;

    if (OfcGetAllFsofcMeterTable (pOfcFsofcMeterEntry) != OSIX_SUCCESS)
    {
        MemReleaseMemBlock (OFC_FSOFCMETERTABLE_POOLID,
                            (UINT1 *) pOfcFsofcMeterEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    MEMCPY (pRetValFsofcMeterBandInfo->pu1_OctetList,
            pOfcFsofcMeterEntry->MibObject.au1FsofcMeterBandInfo,
            pOfcFsofcMeterEntry->MibObject.i4FsofcMeterBandInfoLen);
    pRetValFsofcMeterBandInfo->i4_Length =
        pOfcFsofcMeterEntry->MibObject.i4FsofcMeterBandInfoLen;

    MemReleaseMemBlock (OFC_FSOFCMETERTABLE_POOLID,
                        (UINT1 *) pOfcFsofcMeterEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsofcMeterFlowCount
 Input       :  The Indices
                FsofcContextId
                FsofcMeterIndex

                The Object 
                UINT4 *pu4RetValFsofcMeterFlowCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsofcMeterFlowCount (UINT4 u4FsofcContextId, UINT4 u4FsofcMeterIndex,
                           UINT4 *pu4RetValFsofcMeterFlowCount)
{
    tOfcFsofcMeterEntry *pOfcFsofcMeterEntry = NULL;

    pOfcFsofcMeterEntry =
        (tOfcFsofcMeterEntry *) MemAllocMemBlk (OFC_FSOFCMETERTABLE_POOLID);

    if (pOfcFsofcMeterEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pOfcFsofcMeterEntry, OFC_ZERO, sizeof (tOfcFsofcMeterEntry));

    /* Assign the index */
    pOfcFsofcMeterEntry->MibObject.u4FsofcContextId = u4FsofcContextId;

    pOfcFsofcMeterEntry->MibObject.u4FsofcMeterIndex = u4FsofcMeterIndex;

    if (OfcGetAllFsofcMeterTable (pOfcFsofcMeterEntry) != OSIX_SUCCESS)
    {
        MemReleaseMemBlock (OFC_FSOFCMETERTABLE_POOLID,
                            (UINT1 *) pOfcFsofcMeterEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pu4RetValFsofcMeterFlowCount =
        pOfcFsofcMeterEntry->MibObject.u4FsofcMeterFlowCount;

    MemReleaseMemBlock (OFC_FSOFCMETERTABLE_POOLID,
                        (UINT1 *) pOfcFsofcMeterEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsofcMeterPacketInCount
 Input       :  The Indices
                FsofcContextId
                FsofcMeterIndex

                The Object 
                tSNMP_COUNTER64_TYPE *pu8RetValFsofcMeterPacketInCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsofcMeterPacketInCount (UINT4 u4FsofcContextId, UINT4 u4FsofcMeterIndex,
                               tSNMP_COUNTER64_TYPE *
                               pu8RetValFsofcMeterPacketInCount)
{
    tOfcFsofcMeterEntry *pOfcFsofcMeterEntry = NULL;

    pOfcFsofcMeterEntry =
        (tOfcFsofcMeterEntry *) MemAllocMemBlk (OFC_FSOFCMETERTABLE_POOLID);

    if (pOfcFsofcMeterEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pOfcFsofcMeterEntry, OFC_ZERO, sizeof (tOfcFsofcMeterEntry));

    /* Assign the index */
    pOfcFsofcMeterEntry->MibObject.u4FsofcContextId = u4FsofcContextId;

    pOfcFsofcMeterEntry->MibObject.u4FsofcMeterIndex = u4FsofcMeterIndex;

    if (OfcGetAllFsofcMeterTable (pOfcFsofcMeterEntry) != OSIX_SUCCESS)
    {
        MemReleaseMemBlock (OFC_FSOFCMETERTABLE_POOLID,
                            (UINT1 *) pOfcFsofcMeterEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pu8RetValFsofcMeterPacketInCount =
        pOfcFsofcMeterEntry->MibObject.u8FsofcMeterPacketInCount;

    MemReleaseMemBlock (OFC_FSOFCMETERTABLE_POOLID,
                        (UINT1 *) pOfcFsofcMeterEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsofcMeterByteInCount
 Input       :  The Indices
                FsofcContextId
                FsofcMeterIndex

                The Object 
                tSNMP_COUNTER64_TYPE *pu8RetValFsofcMeterByteInCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsofcMeterByteInCount (UINT4 u4FsofcContextId, UINT4 u4FsofcMeterIndex,
                             tSNMP_COUNTER64_TYPE *
                             pu8RetValFsofcMeterByteInCount)
{
    tOfcFsofcMeterEntry *pOfcFsofcMeterEntry = NULL;

    pOfcFsofcMeterEntry =
        (tOfcFsofcMeterEntry *) MemAllocMemBlk (OFC_FSOFCMETERTABLE_POOLID);

    if (pOfcFsofcMeterEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pOfcFsofcMeterEntry, OFC_ZERO, sizeof (tOfcFsofcMeterEntry));

    /* Assign the index */
    pOfcFsofcMeterEntry->MibObject.u4FsofcContextId = u4FsofcContextId;

    pOfcFsofcMeterEntry->MibObject.u4FsofcMeterIndex = u4FsofcMeterIndex;

    if (OfcGetAllFsofcMeterTable (pOfcFsofcMeterEntry) != OSIX_SUCCESS)
    {
        MemReleaseMemBlock (OFC_FSOFCMETERTABLE_POOLID,
                            (UINT1 *) pOfcFsofcMeterEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pu8RetValFsofcMeterByteInCount =
        pOfcFsofcMeterEntry->MibObject.u8FsofcMeterByteInCount;

    MemReleaseMemBlock (OFC_FSOFCMETERTABLE_POOLID,
                        (UINT1 *) pOfcFsofcMeterEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsofcMeterDurationSec
 Input       :  The Indices
                FsofcContextId
                FsofcMeterIndex

                The Object 
                UINT4 *pu4RetValFsofcMeterDurationSec
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsofcMeterDurationSec (UINT4 u4FsofcContextId, UINT4 u4FsofcMeterIndex,
                             UINT4 *pu4RetValFsofcMeterDurationSec)
{
    tOfcFsofcMeterEntry *pOfcFsofcMeterEntry = NULL;

    pOfcFsofcMeterEntry =
        (tOfcFsofcMeterEntry *) MemAllocMemBlk (OFC_FSOFCMETERTABLE_POOLID);

    if (pOfcFsofcMeterEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pOfcFsofcMeterEntry, OFC_ZERO, sizeof (tOfcFsofcMeterEntry));

    /* Assign the index */
    pOfcFsofcMeterEntry->MibObject.u4FsofcContextId = u4FsofcContextId;

    pOfcFsofcMeterEntry->MibObject.u4FsofcMeterIndex = u4FsofcMeterIndex;

    if (OfcGetAllFsofcMeterTable (pOfcFsofcMeterEntry) != OSIX_SUCCESS)
    {
        MemReleaseMemBlock (OFC_FSOFCMETERTABLE_POOLID,
                            (UINT1 *) pOfcFsofcMeterEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pu4RetValFsofcMeterDurationSec =
        pOfcFsofcMeterEntry->MibObject.u4FsofcMeterDurationSec;

    MemReleaseMemBlock (OFC_FSOFCMETERTABLE_POOLID,
                        (UINT1 *) pOfcFsofcMeterEntry);
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsofcModuleStatus
 Input       :  The Indices
                FsofcContextId

                The Object 
             :  INT4 i4SetValFsofcModuleStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsofcModuleStatus (UINT4 u4FsofcContextId, INT4 i4SetValFsofcModuleStatus)
{
    tOfcFsofcCfgEntry  *pOfcFsofcCfgEntry = NULL;
    tOfcIsSetFsofcCfgEntry *pOfcIsSetFsofcCfgEntry = NULL;

    pOfcFsofcCfgEntry =
        (tOfcFsofcCfgEntry *) MemAllocMemBlk (OFC_FSOFCCFGTABLE_POOLID);
    if (pOfcFsofcCfgEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    pOfcIsSetFsofcCfgEntry =
        (tOfcIsSetFsofcCfgEntry *)
        MemAllocMemBlk (OFC_FSOFCCFGTABLE_ISSET_POOLID);
    if (pOfcIsSetFsofcCfgEntry == NULL)
    {
        MemReleaseMemBlock (OFC_FSOFCCFGTABLE_POOLID,
                            (UINT1 *) pOfcFsofcCfgEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pOfcFsofcCfgEntry, OFC_ZERO, sizeof (tOfcFsofcCfgEntry));
    MEMSET (pOfcIsSetFsofcCfgEntry, OFC_ZERO, sizeof (tOfcIsSetFsofcCfgEntry));

    /* Assign the index */
    pOfcFsofcCfgEntry->MibObject.u4FsofcContextId = u4FsofcContextId;
    pOfcIsSetFsofcCfgEntry->bFsofcContextId = OSIX_TRUE;

    /* Assign the value */
    pOfcFsofcCfgEntry->MibObject.i4FsofcModuleStatus =
        i4SetValFsofcModuleStatus;
    pOfcIsSetFsofcCfgEntry->bFsofcModuleStatus = OSIX_TRUE;

    if (OfcSetAllFsofcCfgTable (pOfcFsofcCfgEntry, pOfcIsSetFsofcCfgEntry,
                                OSIX_FALSE, OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (OFC_FSOFCCFGTABLE_POOLID,
                            (UINT1 *) pOfcFsofcCfgEntry);
        MemReleaseMemBlock (OFC_FSOFCCFGTABLE_ISSET_POOLID,
                            (UINT1 *) pOfcIsSetFsofcCfgEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (OFC_FSOFCCFGTABLE_POOLID, (UINT1 *) pOfcFsofcCfgEntry);
    MemReleaseMemBlock (OFC_FSOFCCFGTABLE_ISSET_POOLID,
                        (UINT1 *) pOfcIsSetFsofcCfgEntry);
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsofcSupportedVersion
 Input       :  The Indices
                FsofcContextId

                The Object 
             :  INT4 i4SetValFsofcSupportedVersion
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsofcSupportedVersion (UINT4 u4FsofcContextId,
                             INT4 i4SetValFsofcSupportedVersion)
{
    tOfcFsofcCfgEntry  *pOfcFsofcCfgEntry = NULL;
    tOfcIsSetFsofcCfgEntry *pOfcIsSetFsofcCfgEntry = NULL;

    pOfcFsofcCfgEntry =
        (tOfcFsofcCfgEntry *) MemAllocMemBlk (OFC_FSOFCCFGTABLE_POOLID);
    if (pOfcFsofcCfgEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    pOfcIsSetFsofcCfgEntry =
        (tOfcIsSetFsofcCfgEntry *)
        MemAllocMemBlk (OFC_FSOFCCFGTABLE_ISSET_POOLID);
    if (pOfcIsSetFsofcCfgEntry == NULL)
    {
        MemReleaseMemBlock (OFC_FSOFCCFGTABLE_POOLID,
                            (UINT1 *) pOfcFsofcCfgEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pOfcFsofcCfgEntry, OFC_ZERO, sizeof (tOfcFsofcCfgEntry));
    MEMSET (pOfcIsSetFsofcCfgEntry, OFC_ZERO, sizeof (tOfcIsSetFsofcCfgEntry));

    /* Assign the index */
    pOfcFsofcCfgEntry->MibObject.u4FsofcContextId = u4FsofcContextId;
    pOfcIsSetFsofcCfgEntry->bFsofcContextId = OSIX_TRUE;

    /* Assign the value */
    pOfcFsofcCfgEntry->MibObject.i4FsofcSupportedVersion =
        i4SetValFsofcSupportedVersion;
    pOfcIsSetFsofcCfgEntry->bFsofcSupportedVersion = OSIX_TRUE;

    if (OfcSetAllFsofcCfgTable (pOfcFsofcCfgEntry, pOfcIsSetFsofcCfgEntry,
                                OSIX_FALSE, OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (OFC_FSOFCCFGTABLE_POOLID,
                            (UINT1 *) pOfcFsofcCfgEntry);
        MemReleaseMemBlock (OFC_FSOFCCFGTABLE_ISSET_POOLID,
                            (UINT1 *) pOfcIsSetFsofcCfgEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (OFC_FSOFCCFGTABLE_POOLID, (UINT1 *) pOfcFsofcCfgEntry);
    MemReleaseMemBlock (OFC_FSOFCCFGTABLE_ISSET_POOLID,
                        (UINT1 *) pOfcIsSetFsofcCfgEntry);
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsofcDefaultFlowMissBehaviour
 Input       :  The Indices
                FsofcContextId

                The Object 
             :  INT4 i4SetValFsofcDefaultFlowMissBehaviour
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsofcDefaultFlowMissBehaviour (UINT4 u4FsofcContextId,
                                     INT4 i4SetValFsofcDefaultFlowMissBehaviour)
{
    tOfcFsofcCfgEntry  *pOfcFsofcCfgEntry = NULL;
    tOfcIsSetFsofcCfgEntry *pOfcIsSetFsofcCfgEntry = NULL;

    pOfcFsofcCfgEntry =
        (tOfcFsofcCfgEntry *) MemAllocMemBlk (OFC_FSOFCCFGTABLE_POOLID);
    if (pOfcFsofcCfgEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    pOfcIsSetFsofcCfgEntry =
        (tOfcIsSetFsofcCfgEntry *)
        MemAllocMemBlk (OFC_FSOFCCFGTABLE_ISSET_POOLID);
    if (pOfcIsSetFsofcCfgEntry == NULL)
    {
        MemReleaseMemBlock (OFC_FSOFCCFGTABLE_POOLID,
                            (UINT1 *) pOfcFsofcCfgEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pOfcFsofcCfgEntry, OFC_ZERO, sizeof (tOfcFsofcCfgEntry));
    MEMSET (pOfcIsSetFsofcCfgEntry, OFC_ZERO, sizeof (tOfcIsSetFsofcCfgEntry));

    /* Assign the index */
    pOfcFsofcCfgEntry->MibObject.u4FsofcContextId = u4FsofcContextId;
    pOfcIsSetFsofcCfgEntry->bFsofcContextId = OSIX_TRUE;

    /* Assign the value */
    pOfcFsofcCfgEntry->MibObject.i4FsofcDefaultFlowMissBehaviour =
        i4SetValFsofcDefaultFlowMissBehaviour;
    pOfcIsSetFsofcCfgEntry->bFsofcDefaultFlowMissBehaviour = OSIX_TRUE;

    if (OfcSetAllFsofcCfgTable (pOfcFsofcCfgEntry, pOfcIsSetFsofcCfgEntry,
                                OSIX_FALSE, OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (OFC_FSOFCCFGTABLE_POOLID,
                            (UINT1 *) pOfcFsofcCfgEntry);
        MemReleaseMemBlock (OFC_FSOFCCFGTABLE_ISSET_POOLID,
                            (UINT1 *) pOfcIsSetFsofcCfgEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (OFC_FSOFCCFGTABLE_POOLID, (UINT1 *) pOfcFsofcCfgEntry);
    MemReleaseMemBlock (OFC_FSOFCCFGTABLE_ISSET_POOLID,
                        (UINT1 *) pOfcIsSetFsofcCfgEntry);
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsofcControlPktBuffering
 Input       :  The Indices
                FsofcContextId

                The Object 
             :  INT4 i4SetValFsofcControlPktBuffering
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsofcControlPktBuffering (UINT4 u4FsofcContextId,
                                INT4 i4SetValFsofcControlPktBuffering)
{
    tOfcFsofcCfgEntry  *pOfcFsofcCfgEntry = NULL;
    tOfcIsSetFsofcCfgEntry *pOfcIsSetFsofcCfgEntry = NULL;

    pOfcFsofcCfgEntry =
        (tOfcFsofcCfgEntry *) MemAllocMemBlk (OFC_FSOFCCFGTABLE_POOLID);
    if (pOfcFsofcCfgEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    pOfcIsSetFsofcCfgEntry =
        (tOfcIsSetFsofcCfgEntry *)
        MemAllocMemBlk (OFC_FSOFCCFGTABLE_ISSET_POOLID);
    if (pOfcIsSetFsofcCfgEntry == NULL)
    {
        MemReleaseMemBlock (OFC_FSOFCCFGTABLE_POOLID,
                            (UINT1 *) pOfcFsofcCfgEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pOfcFsofcCfgEntry, OFC_ZERO, sizeof (tOfcFsofcCfgEntry));
    MEMSET (pOfcIsSetFsofcCfgEntry, OFC_ZERO, sizeof (tOfcIsSetFsofcCfgEntry));

    /* Assign the index */
    pOfcFsofcCfgEntry->MibObject.u4FsofcContextId = u4FsofcContextId;
    pOfcIsSetFsofcCfgEntry->bFsofcContextId = OSIX_TRUE;

    /* Assign the value */
    pOfcFsofcCfgEntry->MibObject.i4FsofcControlPktBuffering =
        i4SetValFsofcControlPktBuffering;
    pOfcIsSetFsofcCfgEntry->bFsofcControlPktBuffering = OSIX_TRUE;

    if (OfcSetAllFsofcCfgTable (pOfcFsofcCfgEntry, pOfcIsSetFsofcCfgEntry,
                                OSIX_FALSE, OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (OFC_FSOFCCFGTABLE_POOLID,
                            (UINT1 *) pOfcFsofcCfgEntry);
        MemReleaseMemBlock (OFC_FSOFCCFGTABLE_ISSET_POOLID,
                            (UINT1 *) pOfcIsSetFsofcCfgEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (OFC_FSOFCCFGTABLE_POOLID, (UINT1 *) pOfcFsofcCfgEntry);
    MemReleaseMemBlock (OFC_FSOFCCFGTABLE_ISSET_POOLID,
                        (UINT1 *) pOfcIsSetFsofcCfgEntry);
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsofcIpReassembleStatus
 Input       :  The Indices
                FsofcContextId

                The Object 
             :  INT4 i4SetValFsofcIpReassembleStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsofcIpReassembleStatus (UINT4 u4FsofcContextId,
                               INT4 i4SetValFsofcIpReassembleStatus)
{
    tOfcFsofcCfgEntry  *pOfcFsofcCfgEntry = NULL;
    tOfcIsSetFsofcCfgEntry *pOfcIsSetFsofcCfgEntry = NULL;

    pOfcFsofcCfgEntry =
        (tOfcFsofcCfgEntry *) MemAllocMemBlk (OFC_FSOFCCFGTABLE_POOLID);
    if (pOfcFsofcCfgEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    pOfcIsSetFsofcCfgEntry =
        (tOfcIsSetFsofcCfgEntry *)
        MemAllocMemBlk (OFC_FSOFCCFGTABLE_ISSET_POOLID);
    if (pOfcIsSetFsofcCfgEntry == NULL)
    {
        MemReleaseMemBlock (OFC_FSOFCCFGTABLE_POOLID,
                            (UINT1 *) pOfcFsofcCfgEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pOfcFsofcCfgEntry, OFC_ZERO, sizeof (tOfcFsofcCfgEntry));
    MEMSET (pOfcIsSetFsofcCfgEntry, OFC_ZERO, sizeof (tOfcIsSetFsofcCfgEntry));

    /* Assign the index */
    pOfcFsofcCfgEntry->MibObject.u4FsofcContextId = u4FsofcContextId;
    pOfcIsSetFsofcCfgEntry->bFsofcContextId = OSIX_TRUE;

    /* Assign the value */
    pOfcFsofcCfgEntry->MibObject.i4FsofcIpReassembleStatus =
        i4SetValFsofcIpReassembleStatus;
    pOfcIsSetFsofcCfgEntry->bFsofcIpReassembleStatus = OSIX_TRUE;

    if (OfcSetAllFsofcCfgTable (pOfcFsofcCfgEntry, pOfcIsSetFsofcCfgEntry,
                                OSIX_FALSE, OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (OFC_FSOFCCFGTABLE_POOLID,
                            (UINT1 *) pOfcFsofcCfgEntry);
        MemReleaseMemBlock (OFC_FSOFCCFGTABLE_ISSET_POOLID,
                            (UINT1 *) pOfcIsSetFsofcCfgEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (OFC_FSOFCCFGTABLE_POOLID, (UINT1 *) pOfcFsofcCfgEntry);
    MemReleaseMemBlock (OFC_FSOFCCFGTABLE_ISSET_POOLID,
                        (UINT1 *) pOfcIsSetFsofcCfgEntry);
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsofcPortStpStatus
 Input       :  The Indices
                FsofcContextId

                The Object 
             :  INT4 i4SetValFsofcPortStpStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsofcPortStpStatus (UINT4 u4FsofcContextId,
                          INT4 i4SetValFsofcPortStpStatus)
{
    tOfcFsofcCfgEntry  *pOfcFsofcCfgEntry = NULL;
    tOfcIsSetFsofcCfgEntry *pOfcIsSetFsofcCfgEntry = NULL;

    pOfcFsofcCfgEntry =
        (tOfcFsofcCfgEntry *) MemAllocMemBlk (OFC_FSOFCCFGTABLE_POOLID);
    if (pOfcFsofcCfgEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    pOfcIsSetFsofcCfgEntry =
        (tOfcIsSetFsofcCfgEntry *)
        MemAllocMemBlk (OFC_FSOFCCFGTABLE_ISSET_POOLID);
    if (pOfcIsSetFsofcCfgEntry == NULL)
    {
        MemReleaseMemBlock (OFC_FSOFCCFGTABLE_POOLID,
                            (UINT1 *) pOfcFsofcCfgEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pOfcFsofcCfgEntry, OFC_ZERO, sizeof (tOfcFsofcCfgEntry));
    MEMSET (pOfcIsSetFsofcCfgEntry, OFC_ZERO, sizeof (tOfcIsSetFsofcCfgEntry));

    /* Assign the index */
    pOfcFsofcCfgEntry->MibObject.u4FsofcContextId = u4FsofcContextId;
    pOfcIsSetFsofcCfgEntry->bFsofcContextId = OSIX_TRUE;

    /* Assign the value */
    pOfcFsofcCfgEntry->MibObject.i4FsofcPortStpStatus =
        i4SetValFsofcPortStpStatus;
    pOfcIsSetFsofcCfgEntry->bFsofcPortStpStatus = OSIX_TRUE;

    if (OfcSetAllFsofcCfgTable (pOfcFsofcCfgEntry, pOfcIsSetFsofcCfgEntry,
                                OSIX_FALSE, OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (OFC_FSOFCCFGTABLE_POOLID,
                            (UINT1 *) pOfcFsofcCfgEntry);
        MemReleaseMemBlock (OFC_FSOFCCFGTABLE_ISSET_POOLID,
                            (UINT1 *) pOfcIsSetFsofcCfgEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (OFC_FSOFCCFGTABLE_POOLID, (UINT1 *) pOfcFsofcCfgEntry);
    MemReleaseMemBlock (OFC_FSOFCCFGTABLE_ISSET_POOLID,
                        (UINT1 *) pOfcIsSetFsofcCfgEntry);
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsofcTraceEnable
 Input       :  The Indices
                FsofcContextId

                The Object 
             :  UINT4 u4SetValFsofcTraceEnable
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsofcTraceEnable (UINT4 u4FsofcContextId, UINT4 u4SetValFsofcTraceEnable)
{
    tOfcFsofcCfgEntry  *pOfcFsofcCfgEntry = NULL;
    tOfcIsSetFsofcCfgEntry *pOfcIsSetFsofcCfgEntry = NULL;

    pOfcFsofcCfgEntry =
        (tOfcFsofcCfgEntry *) MemAllocMemBlk (OFC_FSOFCCFGTABLE_POOLID);
    if (pOfcFsofcCfgEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    pOfcIsSetFsofcCfgEntry =
        (tOfcIsSetFsofcCfgEntry *)
        MemAllocMemBlk (OFC_FSOFCCFGTABLE_ISSET_POOLID);
    if (pOfcIsSetFsofcCfgEntry == NULL)
    {
        MemReleaseMemBlock (OFC_FSOFCCFGTABLE_POOLID,
                            (UINT1 *) pOfcFsofcCfgEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pOfcFsofcCfgEntry, OFC_ZERO, sizeof (tOfcFsofcCfgEntry));
    MEMSET (pOfcIsSetFsofcCfgEntry, OFC_ZERO, sizeof (tOfcIsSetFsofcCfgEntry));

    /* Assign the index */
    pOfcFsofcCfgEntry->MibObject.u4FsofcContextId = u4FsofcContextId;
    pOfcIsSetFsofcCfgEntry->bFsofcContextId = OSIX_TRUE;

    /* Assign the value */
    pOfcFsofcCfgEntry->MibObject.u4FsofcTraceEnable = u4SetValFsofcTraceEnable;
    pOfcIsSetFsofcCfgEntry->bFsofcTraceEnable = OSIX_TRUE;

    if (OfcSetAllFsofcCfgTable (pOfcFsofcCfgEntry, pOfcIsSetFsofcCfgEntry,
                                OSIX_FALSE, OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (OFC_FSOFCCFGTABLE_POOLID,
                            (UINT1 *) pOfcFsofcCfgEntry);
        MemReleaseMemBlock (OFC_FSOFCCFGTABLE_ISSET_POOLID,
                            (UINT1 *) pOfcIsSetFsofcCfgEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (OFC_FSOFCCFGTABLE_POOLID, (UINT1 *) pOfcFsofcCfgEntry);
    MemReleaseMemBlock (OFC_FSOFCCFGTABLE_ISSET_POOLID,
                        (UINT1 *) pOfcIsSetFsofcCfgEntry);
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsofcSwitchModeOnConnFailure
 Input       :  The Indices
                FsofcContextId

                The Object 
             :  INT4 i4SetValFsofcSwitchModeOnConnFailure
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsofcSwitchModeOnConnFailure (UINT4 u4FsofcContextId,
                                    INT4 i4SetValFsofcSwitchModeOnConnFailure)
{
    tOfcFsofcCfgEntry  *pOfcFsofcCfgEntry = NULL;
    tOfcIsSetFsofcCfgEntry *pOfcIsSetFsofcCfgEntry = NULL;

    pOfcFsofcCfgEntry =
        (tOfcFsofcCfgEntry *) MemAllocMemBlk (OFC_FSOFCCFGTABLE_POOLID);
    if (pOfcFsofcCfgEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    pOfcIsSetFsofcCfgEntry =
        (tOfcIsSetFsofcCfgEntry *)
        MemAllocMemBlk (OFC_FSOFCCFGTABLE_ISSET_POOLID);
    if (pOfcIsSetFsofcCfgEntry == NULL)
    {
        MemReleaseMemBlock (OFC_FSOFCCFGTABLE_POOLID,
                            (UINT1 *) pOfcFsofcCfgEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pOfcFsofcCfgEntry, OFC_ZERO, sizeof (tOfcFsofcCfgEntry));
    MEMSET (pOfcIsSetFsofcCfgEntry, OFC_ZERO, sizeof (tOfcIsSetFsofcCfgEntry));

    /* Assign the index */
    pOfcFsofcCfgEntry->MibObject.u4FsofcContextId = u4FsofcContextId;
    pOfcIsSetFsofcCfgEntry->bFsofcContextId = OSIX_TRUE;

    /* Assign the value */
    pOfcFsofcCfgEntry->MibObject.i4FsofcSwitchModeOnConnFailure =
        i4SetValFsofcSwitchModeOnConnFailure;
    pOfcIsSetFsofcCfgEntry->bFsofcSwitchModeOnConnFailure = OSIX_TRUE;

    if (OfcSetAllFsofcCfgTable (pOfcFsofcCfgEntry, pOfcIsSetFsofcCfgEntry,
                                OSIX_FALSE, OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (OFC_FSOFCCFGTABLE_POOLID,
                            (UINT1 *) pOfcFsofcCfgEntry);
        MemReleaseMemBlock (OFC_FSOFCCFGTABLE_ISSET_POOLID,
                            (UINT1 *) pOfcIsSetFsofcCfgEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (OFC_FSOFCCFGTABLE_POOLID, (UINT1 *) pOfcFsofcCfgEntry);
    MemReleaseMemBlock (OFC_FSOFCCFGTABLE_ISSET_POOLID,
                        (UINT1 *) pOfcIsSetFsofcCfgEntry);
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsofcSwitchEntryStatus
 Input       :  The Indices
                FsofcContextId

                The Object 
             :  INT4 i4SetValFsofcSwitchEntryStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsofcSwitchEntryStatus (UINT4 u4FsofcContextId,
                              INT4 i4SetValFsofcSwitchEntryStatus)
{
    tOfcFsofcCfgEntry  *pOfcFsofcCfgEntry = NULL;
    tOfcIsSetFsofcCfgEntry *pOfcIsSetFsofcCfgEntry = NULL;

    pOfcFsofcCfgEntry =
        (tOfcFsofcCfgEntry *) MemAllocMemBlk (OFC_FSOFCCFGTABLE_POOLID);
    if (pOfcFsofcCfgEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    pOfcIsSetFsofcCfgEntry =
        (tOfcIsSetFsofcCfgEntry *)
        MemAllocMemBlk (OFC_FSOFCCFGTABLE_ISSET_POOLID);
    if (pOfcIsSetFsofcCfgEntry == NULL)
    {
        MemReleaseMemBlock (OFC_FSOFCCFGTABLE_POOLID,
                            (UINT1 *) pOfcFsofcCfgEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pOfcFsofcCfgEntry, OFC_ZERO, sizeof (tOfcFsofcCfgEntry));
    MEMSET (pOfcIsSetFsofcCfgEntry, OFC_ZERO, sizeof (tOfcIsSetFsofcCfgEntry));

    /* Assign the index */
    pOfcFsofcCfgEntry->MibObject.u4FsofcContextId = u4FsofcContextId;
    pOfcIsSetFsofcCfgEntry->bFsofcContextId = OSIX_TRUE;

    /* Assign the value */
    pOfcFsofcCfgEntry->MibObject.i4FsofcSwitchEntryStatus =
        i4SetValFsofcSwitchEntryStatus;
    pOfcIsSetFsofcCfgEntry->bFsofcSwitchEntryStatus = OSIX_TRUE;

    if (OfcSetAllFsofcCfgTable (pOfcFsofcCfgEntry, pOfcIsSetFsofcCfgEntry,
                                OSIX_FALSE, OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (OFC_FSOFCCFGTABLE_POOLID,
                            (UINT1 *) pOfcFsofcCfgEntry);
        MemReleaseMemBlock (OFC_FSOFCCFGTABLE_ISSET_POOLID,
                            (UINT1 *) pOfcIsSetFsofcCfgEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (OFC_FSOFCCFGTABLE_POOLID, (UINT1 *) pOfcFsofcCfgEntry);
    MemReleaseMemBlock (OFC_FSOFCCFGTABLE_ISSET_POOLID,
                        (UINT1 *) pOfcIsSetFsofcCfgEntry);
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsofcHybrid
 Input       :  The Indices
                FsofcContextId

                The Object 
             :  INT4 i4SetValFsofcHybrid
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsofcHybrid (UINT4 u4FsofcContextId, INT4 i4SetValFsofcHybrid)
{
    tOfcFsofcCfgEntry  *pOfcFsofcCfgEntry = NULL;
    tOfcIsSetFsofcCfgEntry *pOfcIsSetFsofcCfgEntry = NULL;

    pOfcFsofcCfgEntry =
        (tOfcFsofcCfgEntry *) MemAllocMemBlk (OFC_FSOFCCFGTABLE_POOLID);
    if (pOfcFsofcCfgEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    pOfcIsSetFsofcCfgEntry =
        (tOfcIsSetFsofcCfgEntry *)
        MemAllocMemBlk (OFC_FSOFCCFGTABLE_ISSET_POOLID);
    if (pOfcIsSetFsofcCfgEntry == NULL)
    {
        MemReleaseMemBlock (OFC_FSOFCCFGTABLE_POOLID,
                            (UINT1 *) pOfcFsofcCfgEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pOfcFsofcCfgEntry, 0, sizeof (tOfcFsofcCfgEntry));
    MEMSET (pOfcIsSetFsofcCfgEntry, 0, sizeof (tOfcIsSetFsofcCfgEntry));

    /* Assign the index */
    pOfcFsofcCfgEntry->MibObject.u4FsofcContextId = u4FsofcContextId;
    pOfcIsSetFsofcCfgEntry->bFsofcContextId = OSIX_TRUE;

    /* Assign the value */
    pOfcFsofcCfgEntry->MibObject.i4FsofcHybrid = i4SetValFsofcHybrid;
    pOfcIsSetFsofcCfgEntry->bFsofcHybrid = OSIX_TRUE;

    if (OfcSetAllFsofcCfgTable (pOfcFsofcCfgEntry, pOfcIsSetFsofcCfgEntry,
                                OSIX_FALSE, OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (OFC_FSOFCCFGTABLE_POOLID,
                            (UINT1 *) pOfcFsofcCfgEntry);
        MemReleaseMemBlock (OFC_FSOFCCFGTABLE_ISSET_POOLID,
                            (UINT1 *) pOfcIsSetFsofcCfgEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (OFC_FSOFCCFGTABLE_POOLID, (UINT1 *) pOfcFsofcCfgEntry);
    MemReleaseMemBlock (OFC_FSOFCCFGTABLE_ISSET_POOLID,
                        (UINT1 *) pOfcIsSetFsofcCfgEntry);
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsofcControllerConnPort
 Input       :  The Indices
                FsofcContextId
                FsofcControllerIpAddrType
                FsofcControllerIpAddress
                FsofcControllerConnAuxId

                The Object 
             :  INT4 i4SetValFsofcControllerConnPort
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsofcControllerConnPort (UINT4 u4FsofcContextId,
                               INT4 i4FsofcControllerIpAddrType,
                               tSNMP_OCTET_STRING_TYPE *
                               pFsofcControllerIpAddress,
                               INT4 i4FsofcControllerConnAuxId,
                               INT4 i4SetValFsofcControllerConnPort)
{
    tOfcFsofcControllerConnEntry *pOfcFsofcControllerConnEntry = NULL;
    tOfcIsSetFsofcControllerConnEntry *pOfcIsSetFsofcControllerConnEntry = NULL;

    pOfcFsofcControllerConnEntry =
        (tOfcFsofcControllerConnEntry *)
        MemAllocMemBlk (OFC_FSOFCCONTROLLERCONNTABLE_POOLID);
    if (pOfcFsofcControllerConnEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    pOfcIsSetFsofcControllerConnEntry =
        (tOfcIsSetFsofcControllerConnEntry *)
        MemAllocMemBlk (OFC_FSOFCCONTROLLERCONNTABLE_ISSET_POOLID);
    if (pOfcIsSetFsofcControllerConnEntry == NULL)
    {
        MemReleaseMemBlock (OFC_FSOFCCONTROLLERCONNTABLE_POOLID,
                            (UINT1 *) pOfcFsofcControllerConnEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pOfcFsofcControllerConnEntry, OFC_ZERO,
            sizeof (tOfcFsofcControllerConnEntry));
    MEMSET (pOfcIsSetFsofcControllerConnEntry, OFC_ZERO,
            sizeof (tOfcIsSetFsofcControllerConnEntry));

    /* Assign the index */
    pOfcFsofcControllerConnEntry->MibObject.u4FsofcContextId = u4FsofcContextId;
    pOfcIsSetFsofcControllerConnEntry->bFsofcContextId = OSIX_TRUE;

    pOfcFsofcControllerConnEntry->MibObject.i4FsofcControllerIpAddrType =
        i4FsofcControllerIpAddrType;
    pOfcIsSetFsofcControllerConnEntry->bFsofcControllerIpAddrType = OSIX_TRUE;

    MEMCPY (pOfcFsofcControllerConnEntry->MibObject.au1FsofcControllerIpAddress,
            pFsofcControllerIpAddress->pu1_OctetList,
            pFsofcControllerIpAddress->i4_Length);

    pOfcFsofcControllerConnEntry->MibObject.i4FsofcControllerIpAddressLen =
        pFsofcControllerIpAddress->i4_Length;
    pOfcIsSetFsofcControllerConnEntry->bFsofcControllerIpAddress = OSIX_TRUE;

    pOfcFsofcControllerConnEntry->MibObject.i4FsofcControllerConnAuxId =
        i4FsofcControllerConnAuxId;
    pOfcIsSetFsofcControllerConnEntry->bFsofcControllerConnAuxId = OSIX_TRUE;

    /* Assign the value */
    pOfcFsofcControllerConnEntry->MibObject.i4FsofcControllerConnPort =
        i4SetValFsofcControllerConnPort;
    pOfcIsSetFsofcControllerConnEntry->bFsofcControllerConnPort = OSIX_TRUE;

    if (OfcSetAllFsofcControllerConnTable
        (pOfcFsofcControllerConnEntry, pOfcIsSetFsofcControllerConnEntry,
         OSIX_FALSE, OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (OFC_FSOFCCONTROLLERCONNTABLE_POOLID,
                            (UINT1 *) pOfcFsofcControllerConnEntry);
        MemReleaseMemBlock (OFC_FSOFCCONTROLLERCONNTABLE_ISSET_POOLID,
                            (UINT1 *) pOfcIsSetFsofcControllerConnEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (OFC_FSOFCCONTROLLERCONNTABLE_POOLID,
                        (UINT1 *) pOfcFsofcControllerConnEntry);
    MemReleaseMemBlock (OFC_FSOFCCONTROLLERCONNTABLE_ISSET_POOLID,
                        (UINT1 *) pOfcIsSetFsofcControllerConnEntry);
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsofcControllerConnProtocol
 Input       :  The Indices
                FsofcContextId
                FsofcControllerIpAddrType
                FsofcControllerIpAddress
                FsofcControllerConnAuxId

                The Object 
             :  INT4 i4SetValFsofcControllerConnProtocol
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsofcControllerConnProtocol (UINT4 u4FsofcContextId,
                                   INT4 i4FsofcControllerIpAddrType,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pFsofcControllerIpAddress,
                                   INT4 i4FsofcControllerConnAuxId,
                                   INT4 i4SetValFsofcControllerConnProtocol)
{
    tOfcFsofcControllerConnEntry *pOfcFsofcControllerConnEntry = NULL;
    tOfcIsSetFsofcControllerConnEntry *pOfcIsSetFsofcControllerConnEntry = NULL;

    pOfcFsofcControllerConnEntry =
        (tOfcFsofcControllerConnEntry *)
        MemAllocMemBlk (OFC_FSOFCCONTROLLERCONNTABLE_POOLID);
    if (pOfcFsofcControllerConnEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    pOfcIsSetFsofcControllerConnEntry =
        (tOfcIsSetFsofcControllerConnEntry *)
        MemAllocMemBlk (OFC_FSOFCCONTROLLERCONNTABLE_ISSET_POOLID);
    if (pOfcIsSetFsofcControllerConnEntry == NULL)
    {
        MemReleaseMemBlock (OFC_FSOFCCONTROLLERCONNTABLE_POOLID,
                            (UINT1 *) pOfcFsofcControllerConnEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pOfcFsofcControllerConnEntry, OFC_ZERO,
            sizeof (tOfcFsofcControllerConnEntry));
    MEMSET (pOfcIsSetFsofcControllerConnEntry, OFC_ZERO,
            sizeof (tOfcIsSetFsofcControllerConnEntry));

    /* Assign the index */
    pOfcFsofcControllerConnEntry->MibObject.u4FsofcContextId = u4FsofcContextId;
    pOfcIsSetFsofcControllerConnEntry->bFsofcContextId = OSIX_TRUE;

    pOfcFsofcControllerConnEntry->MibObject.i4FsofcControllerIpAddrType =
        i4FsofcControllerIpAddrType;
    pOfcIsSetFsofcControllerConnEntry->bFsofcControllerIpAddrType = OSIX_TRUE;

    MEMCPY (pOfcFsofcControllerConnEntry->MibObject.au1FsofcControllerIpAddress,
            pFsofcControllerIpAddress->pu1_OctetList,
            pFsofcControllerIpAddress->i4_Length);

    pOfcFsofcControllerConnEntry->MibObject.i4FsofcControllerIpAddressLen =
        pFsofcControllerIpAddress->i4_Length;
    pOfcIsSetFsofcControllerConnEntry->bFsofcControllerIpAddress = OSIX_TRUE;

    pOfcFsofcControllerConnEntry->MibObject.i4FsofcControllerConnAuxId =
        i4FsofcControllerConnAuxId;
    pOfcIsSetFsofcControllerConnEntry->bFsofcControllerConnAuxId = OSIX_TRUE;

    /* Assign the value */
    pOfcFsofcControllerConnEntry->MibObject.i4FsofcControllerConnProtocol =
        i4SetValFsofcControllerConnProtocol;
    pOfcIsSetFsofcControllerConnEntry->bFsofcControllerConnProtocol = OSIX_TRUE;

    if (OfcSetAllFsofcControllerConnTable
        (pOfcFsofcControllerConnEntry, pOfcIsSetFsofcControllerConnEntry,
         OSIX_FALSE, OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (OFC_FSOFCCONTROLLERCONNTABLE_POOLID,
                            (UINT1 *) pOfcFsofcControllerConnEntry);
        MemReleaseMemBlock (OFC_FSOFCCONTROLLERCONNTABLE_ISSET_POOLID,
                            (UINT1 *) pOfcIsSetFsofcControllerConnEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (OFC_FSOFCCONTROLLERCONNTABLE_POOLID,
                        (UINT1 *) pOfcFsofcControllerConnEntry);
    MemReleaseMemBlock (OFC_FSOFCCONTROLLERCONNTABLE_ISSET_POOLID,
                        (UINT1 *) pOfcIsSetFsofcControllerConnEntry);
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsofcControllerRole
 Input       :  The Indices
                FsofcContextId
                FsofcControllerIpAddrType
                FsofcControllerIpAddress
                FsofcControllerConnAuxId

                The Object 
             :  INT4 i4SetValFsofcControllerRole
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsofcControllerRole (UINT4 u4FsofcContextId,
                           INT4 i4FsofcControllerIpAddrType,
                           tSNMP_OCTET_STRING_TYPE * pFsofcControllerIpAddress,
                           INT4 i4FsofcControllerConnAuxId,
                           INT4 i4SetValFsofcControllerRole)
{
    tOfcFsofcControllerConnEntry *pOfcFsofcControllerConnEntry = NULL;
    tOfcIsSetFsofcControllerConnEntry *pOfcIsSetFsofcControllerConnEntry = NULL;

    pOfcFsofcControllerConnEntry =
        (tOfcFsofcControllerConnEntry *)
        MemAllocMemBlk (OFC_FSOFCCONTROLLERCONNTABLE_POOLID);
    if (pOfcFsofcControllerConnEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    pOfcIsSetFsofcControllerConnEntry =
        (tOfcIsSetFsofcControllerConnEntry *)
        MemAllocMemBlk (OFC_FSOFCCONTROLLERCONNTABLE_ISSET_POOLID);
    if (pOfcIsSetFsofcControllerConnEntry == NULL)
    {
        MemReleaseMemBlock (OFC_FSOFCCONTROLLERCONNTABLE_POOLID,
                            (UINT1 *) pOfcFsofcControllerConnEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pOfcFsofcControllerConnEntry, OFC_ZERO,
            sizeof (tOfcFsofcControllerConnEntry));
    MEMSET (pOfcIsSetFsofcControllerConnEntry, OFC_ZERO,
            sizeof (tOfcIsSetFsofcControllerConnEntry));

    /* Assign the index */
    pOfcFsofcControllerConnEntry->MibObject.u4FsofcContextId = u4FsofcContextId;
    pOfcIsSetFsofcControllerConnEntry->bFsofcContextId = OSIX_TRUE;

    pOfcFsofcControllerConnEntry->MibObject.i4FsofcControllerIpAddrType =
        i4FsofcControllerIpAddrType;
    pOfcIsSetFsofcControllerConnEntry->bFsofcControllerIpAddrType = OSIX_TRUE;

    MEMCPY (pOfcFsofcControllerConnEntry->MibObject.au1FsofcControllerIpAddress,
            pFsofcControllerIpAddress->pu1_OctetList,
            pFsofcControllerIpAddress->i4_Length);

    pOfcFsofcControllerConnEntry->MibObject.i4FsofcControllerIpAddressLen =
        pFsofcControllerIpAddress->i4_Length;
    pOfcIsSetFsofcControllerConnEntry->bFsofcControllerIpAddress = OSIX_TRUE;

    pOfcFsofcControllerConnEntry->MibObject.i4FsofcControllerConnAuxId =
        i4FsofcControllerConnAuxId;
    pOfcIsSetFsofcControllerConnEntry->bFsofcControllerConnAuxId = OSIX_TRUE;

    /* Assign the value */
    pOfcFsofcControllerConnEntry->MibObject.i4FsofcControllerRole =
        i4SetValFsofcControllerRole;
    pOfcIsSetFsofcControllerConnEntry->bFsofcControllerRole = OSIX_TRUE;

    if (OfcSetAllFsofcControllerConnTable
        (pOfcFsofcControllerConnEntry, pOfcIsSetFsofcControllerConnEntry,
         OSIX_FALSE, OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (OFC_FSOFCCONTROLLERCONNTABLE_POOLID,
                            (UINT1 *) pOfcFsofcControllerConnEntry);
        MemReleaseMemBlock (OFC_FSOFCCONTROLLERCONNTABLE_ISSET_POOLID,
                            (UINT1 *) pOfcIsSetFsofcControllerConnEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (OFC_FSOFCCONTROLLERCONNTABLE_POOLID,
                        (UINT1 *) pOfcFsofcControllerConnEntry);
    MemReleaseMemBlock (OFC_FSOFCCONTROLLERCONNTABLE_ISSET_POOLID,
                        (UINT1 *) pOfcIsSetFsofcControllerConnEntry);
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsofcControllerConnEchoReqCount
 Input       :  The Indices
                FsofcContextId
                FsofcControllerIpAddrType
                FsofcControllerIpAddress
                FsofcControllerConnAuxId

                The Object 
             :  INT4 i4SetValFsofcControllerConnEchoReqCount
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsofcControllerConnEchoReqCount (UINT4 u4FsofcContextId,
                                       INT4 i4FsofcControllerIpAddrType,
                                       tSNMP_OCTET_STRING_TYPE *
                                       pFsofcControllerIpAddress,
                                       INT4 i4FsofcControllerConnAuxId,
                                       INT4
                                       i4SetValFsofcControllerConnEchoReqCount)
{
    tOfcFsofcControllerConnEntry *pOfcFsofcControllerConnEntry = NULL;
    tOfcIsSetFsofcControllerConnEntry *pOfcIsSetFsofcControllerConnEntry = NULL;

    pOfcFsofcControllerConnEntry =
        (tOfcFsofcControllerConnEntry *)
        MemAllocMemBlk (OFC_FSOFCCONTROLLERCONNTABLE_POOLID);
    if (pOfcFsofcControllerConnEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    pOfcIsSetFsofcControllerConnEntry =
        (tOfcIsSetFsofcControllerConnEntry *)
        MemAllocMemBlk (OFC_FSOFCCONTROLLERCONNTABLE_ISSET_POOLID);
    if (pOfcIsSetFsofcControllerConnEntry == NULL)
    {
        MemReleaseMemBlock (OFC_FSOFCCONTROLLERCONNTABLE_POOLID,
                            (UINT1 *) pOfcFsofcControllerConnEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pOfcFsofcControllerConnEntry, OFC_ZERO,
            sizeof (tOfcFsofcControllerConnEntry));
    MEMSET (pOfcIsSetFsofcControllerConnEntry, OFC_ZERO,
            sizeof (tOfcIsSetFsofcControllerConnEntry));

    /* Assign the index */
    pOfcFsofcControllerConnEntry->MibObject.u4FsofcContextId = u4FsofcContextId;
    pOfcIsSetFsofcControllerConnEntry->bFsofcContextId = OSIX_TRUE;

    pOfcFsofcControllerConnEntry->MibObject.i4FsofcControllerIpAddrType =
        i4FsofcControllerIpAddrType;
    pOfcIsSetFsofcControllerConnEntry->bFsofcControllerIpAddrType = OSIX_TRUE;

    MEMCPY (pOfcFsofcControllerConnEntry->MibObject.au1FsofcControllerIpAddress,
            pFsofcControllerIpAddress->pu1_OctetList,
            pFsofcControllerIpAddress->i4_Length);

    pOfcFsofcControllerConnEntry->MibObject.i4FsofcControllerIpAddressLen =
        pFsofcControllerIpAddress->i4_Length;
    pOfcIsSetFsofcControllerConnEntry->bFsofcControllerIpAddress = OSIX_TRUE;

    pOfcFsofcControllerConnEntry->MibObject.i4FsofcControllerConnAuxId =
        i4FsofcControllerConnAuxId;
    pOfcIsSetFsofcControllerConnEntry->bFsofcControllerConnAuxId = OSIX_TRUE;

    /* Assign the value */
    pOfcFsofcControllerConnEntry->MibObject.i4FsofcControllerConnEchoReqCount =
        i4SetValFsofcControllerConnEchoReqCount;
    pOfcIsSetFsofcControllerConnEntry->bFsofcControllerConnEchoReqCount =
        OSIX_TRUE;

    if (OfcSetAllFsofcControllerConnTable
        (pOfcFsofcControllerConnEntry, pOfcIsSetFsofcControllerConnEntry,
         OSIX_FALSE, OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (OFC_FSOFCCONTROLLERCONNTABLE_POOLID,
                            (UINT1 *) pOfcFsofcControllerConnEntry);
        MemReleaseMemBlock (OFC_FSOFCCONTROLLERCONNTABLE_ISSET_POOLID,
                            (UINT1 *) pOfcIsSetFsofcControllerConnEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (OFC_FSOFCCONTROLLERCONNTABLE_POOLID,
                        (UINT1 *) pOfcFsofcControllerConnEntry);
    MemReleaseMemBlock (OFC_FSOFCCONTROLLERCONNTABLE_ISSET_POOLID,
                        (UINT1 *) pOfcIsSetFsofcControllerConnEntry);
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsofcControllerConnEntryStatus
 Input       :  The Indices
                FsofcContextId
                FsofcControllerIpAddrType
                FsofcControllerIpAddress
                FsofcControllerConnAuxId

                The Object 
             :  INT4 i4SetValFsofcControllerConnEntryStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsofcControllerConnEntryStatus (UINT4 u4FsofcContextId,
                                      INT4 i4FsofcControllerIpAddrType,
                                      tSNMP_OCTET_STRING_TYPE *
                                      pFsofcControllerIpAddress,
                                      INT4 i4FsofcControllerConnAuxId,
                                      INT4
                                      i4SetValFsofcControllerConnEntryStatus)
{
    tOfcFsofcControllerConnEntry *pOfcFsofcControllerConnEntry = NULL;
    tOfcIsSetFsofcControllerConnEntry *pOfcIsSetFsofcControllerConnEntry = NULL;

    pOfcFsofcControllerConnEntry =
        (tOfcFsofcControllerConnEntry *)
        MemAllocMemBlk (OFC_FSOFCCONTROLLERCONNTABLE_POOLID);
    if (pOfcFsofcControllerConnEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    pOfcIsSetFsofcControllerConnEntry =
        (tOfcIsSetFsofcControllerConnEntry *)
        MemAllocMemBlk (OFC_FSOFCCONTROLLERCONNTABLE_ISSET_POOLID);
    if (pOfcIsSetFsofcControllerConnEntry == NULL)
    {
        MemReleaseMemBlock (OFC_FSOFCCONTROLLERCONNTABLE_POOLID,
                            (UINT1 *) pOfcFsofcControllerConnEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pOfcFsofcControllerConnEntry, OFC_ZERO,
            sizeof (tOfcFsofcControllerConnEntry));
    MEMSET (pOfcIsSetFsofcControllerConnEntry, OFC_ZERO,
            sizeof (tOfcIsSetFsofcControllerConnEntry));

    /* Assign the index */
    pOfcFsofcControllerConnEntry->MibObject.u4FsofcContextId = u4FsofcContextId;
    pOfcIsSetFsofcControllerConnEntry->bFsofcContextId = OSIX_TRUE;

    pOfcFsofcControllerConnEntry->MibObject.i4FsofcControllerIpAddrType =
        i4FsofcControllerIpAddrType;
    pOfcIsSetFsofcControllerConnEntry->bFsofcControllerIpAddrType = OSIX_TRUE;

    MEMCPY (pOfcFsofcControllerConnEntry->MibObject.au1FsofcControllerIpAddress,
            pFsofcControllerIpAddress->pu1_OctetList,
            pFsofcControllerIpAddress->i4_Length);

    pOfcFsofcControllerConnEntry->MibObject.i4FsofcControllerIpAddressLen =
        pFsofcControllerIpAddress->i4_Length;
    pOfcIsSetFsofcControllerConnEntry->bFsofcControllerIpAddress = OSIX_TRUE;

    pOfcFsofcControllerConnEntry->MibObject.i4FsofcControllerConnAuxId =
        i4FsofcControllerConnAuxId;
    pOfcIsSetFsofcControllerConnEntry->bFsofcControllerConnAuxId = OSIX_TRUE;

    /* Assign the value */
    pOfcFsofcControllerConnEntry->MibObject.i4FsofcControllerConnEntryStatus =
        i4SetValFsofcControllerConnEntryStatus;
    pOfcIsSetFsofcControllerConnEntry->bFsofcControllerConnEntryStatus =
        OSIX_TRUE;

    if (OfcSetAllFsofcControllerConnTable
        (pOfcFsofcControllerConnEntry, pOfcIsSetFsofcControllerConnEntry,
         OSIX_FALSE, OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (OFC_FSOFCCONTROLLERCONNTABLE_POOLID,
                            (UINT1 *) pOfcFsofcControllerConnEntry);
        MemReleaseMemBlock (OFC_FSOFCCONTROLLERCONNTABLE_ISSET_POOLID,
                            (UINT1 *) pOfcIsSetFsofcControllerConnEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (OFC_FSOFCCONTROLLERCONNTABLE_POOLID,
                        (UINT1 *) pOfcFsofcControllerConnEntry);
    MemReleaseMemBlock (OFC_FSOFCCONTROLLERCONNTABLE_ISSET_POOLID,
                        (UINT1 *) pOfcIsSetFsofcControllerConnEntry);
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsofcControllerConnBand
 Input       :  The Indices
                FsofcContextId
                FsofcControllerIpAddrType
                FsofcControllerIpAddress
                FsofcControllerConnAuxId

                The Object 
             :  INT4 i4SetValFsofcControllerConnBand
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsofcControllerConnBand (UINT4 u4FsofcContextId,
                               INT4 i4FsofcControllerIpAddrType,
                               tSNMP_OCTET_STRING_TYPE *
                               pFsofcControllerIpAddress,
                               INT4 i4FsofcControllerConnAuxId,
                               INT4 i4SetValFsofcControllerConnBand)
{
    tOfcFsofcControllerConnEntry *pOfcFsofcControllerConnEntry = NULL;
    tOfcIsSetFsofcControllerConnEntry *pOfcIsSetFsofcControllerConnEntry = NULL;

    pOfcFsofcControllerConnEntry =
        (tOfcFsofcControllerConnEntry *)
        MemAllocMemBlk (OFC_FSOFCCONTROLLERCONNTABLE_POOLID);
    if (pOfcFsofcControllerConnEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    pOfcIsSetFsofcControllerConnEntry =
        (tOfcIsSetFsofcControllerConnEntry *)
        MemAllocMemBlk (OFC_FSOFCCONTROLLERCONNTABLE_ISSET_POOLID);
    if (pOfcIsSetFsofcControllerConnEntry == NULL)
    {
        MemReleaseMemBlock (OFC_FSOFCCONTROLLERCONNTABLE_POOLID,
                            (UINT1 *) pOfcFsofcControllerConnEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pOfcFsofcControllerConnEntry, 0,
            sizeof (tOfcFsofcControllerConnEntry));
    MEMSET (pOfcIsSetFsofcControllerConnEntry, 0,
            sizeof (tOfcIsSetFsofcControllerConnEntry));

    /* Assign the index */
    pOfcFsofcControllerConnEntry->MibObject.u4FsofcContextId = u4FsofcContextId;
    pOfcIsSetFsofcControllerConnEntry->bFsofcContextId = OSIX_TRUE;

    pOfcFsofcControllerConnEntry->MibObject.i4FsofcControllerIpAddrType =
        i4FsofcControllerIpAddrType;
    pOfcIsSetFsofcControllerConnEntry->bFsofcControllerIpAddrType = OSIX_TRUE;

    MEMCPY (pOfcFsofcControllerConnEntry->MibObject.au1FsofcControllerIpAddress,
            pFsofcControllerIpAddress->pu1_OctetList,
            pFsofcControllerIpAddress->i4_Length);

    pOfcFsofcControllerConnEntry->MibObject.i4FsofcControllerIpAddressLen =
        pFsofcControllerIpAddress->i4_Length;
    pOfcIsSetFsofcControllerConnEntry->bFsofcControllerIpAddress = OSIX_TRUE;

    pOfcFsofcControllerConnEntry->MibObject.i4FsofcControllerConnAuxId =
        i4FsofcControllerConnAuxId;
    pOfcIsSetFsofcControllerConnEntry->bFsofcControllerConnAuxId = OSIX_TRUE;

    /* Assign the value */
    pOfcFsofcControllerConnEntry->MibObject.i4FsofcControllerConnBand =
        i4SetValFsofcControllerConnBand;
    pOfcIsSetFsofcControllerConnEntry->bFsofcControllerConnBand = OSIX_TRUE;

    if (OfcSetAllFsofcControllerConnTable
        (pOfcFsofcControllerConnEntry, pOfcIsSetFsofcControllerConnEntry,
         OSIX_FALSE, OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (OFC_FSOFCCONTROLLERCONNTABLE_POOLID,
                            (UINT1 *) pOfcFsofcControllerConnEntry);
        MemReleaseMemBlock (OFC_FSOFCCONTROLLERCONNTABLE_ISSET_POOLID,
                            (UINT1 *) pOfcIsSetFsofcControllerConnEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (OFC_FSOFCCONTROLLERCONNTABLE_POOLID,
                        (UINT1 *) pOfcFsofcControllerConnEntry);
    MemReleaseMemBlock (OFC_FSOFCCONTROLLERCONNTABLE_ISSET_POOLID,
                        (UINT1 *) pOfcIsSetFsofcControllerConnEntry);
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsofcVlanEgressPorts
 Input       :  The Indices
                FsofcContextId
                FsofcIfIndex

                The Object 
             :  tSNMP_OCTET_STRING_TYPE *pSetValFsofcVlanEgressPorts
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsofcVlanEgressPorts (UINT4 u4FsofcContextId, INT4 i4FsofcIfIndex,
                            tSNMP_OCTET_STRING_TYPE *
                            pSetValFsofcVlanEgressPorts)
{
    tOfcFsofcIfEntry   *pOfcFsofcIfEntry = NULL;
    tOfcIsSetFsofcIfEntry *pOfcIsSetFsofcIfEntry = NULL;

    pOfcFsofcIfEntry =
        (tOfcFsofcIfEntry *) MemAllocMemBlk (OFC_FSOFCIFTABLE_POOLID);
    if (pOfcFsofcIfEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    pOfcIsSetFsofcIfEntry =
        (tOfcIsSetFsofcIfEntry *)
        MemAllocMemBlk (OFC_FSOFCIFTABLE_ISSET_POOLID);
    if (pOfcIsSetFsofcIfEntry == NULL)
    {
        MemReleaseMemBlock (OFC_FSOFCIFTABLE_POOLID,
                            (UINT1 *) pOfcFsofcIfEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pOfcFsofcIfEntry, 0, sizeof (tOfcFsofcIfEntry));
    MEMSET (pOfcIsSetFsofcIfEntry, 0, sizeof (tOfcIsSetFsofcIfEntry));

    /* Assign the index */
    pOfcFsofcIfEntry->MibObject.u4FsofcContextId = u4FsofcContextId;
    pOfcIsSetFsofcIfEntry->bFsofcContextId = OSIX_TRUE;

    pOfcFsofcIfEntry->MibObject.i4FsofcIfIndex = i4FsofcIfIndex;
    pOfcIsSetFsofcIfEntry->bFsofcIfIndex = OSIX_TRUE;

    /* Assign the value */
    MEMCPY (pOfcFsofcIfEntry->MibObject.au1FsofcVlanEgressPorts,
            pSetValFsofcVlanEgressPorts->pu1_OctetList,
            pSetValFsofcVlanEgressPorts->i4_Length);
    pOfcFsofcIfEntry->MibObject.i4FsofcVlanEgressPortsLen =
        pSetValFsofcVlanEgressPorts->i4_Length;

    pOfcIsSetFsofcIfEntry->bFsofcVlanEgressPorts = OSIX_TRUE;

    if (OfcSetAllFsofcIfTable (pOfcFsofcIfEntry, pOfcIsSetFsofcIfEntry) !=
        OSIX_SUCCESS)
    {

        MemReleaseMemBlock (OFC_FSOFCIFTABLE_POOLID,
                            (UINT1 *) pOfcFsofcIfEntry);
        MemReleaseMemBlock (OFC_FSOFCIFTABLE_ISSET_POOLID,
                            (UINT1 *) pOfcIsSetFsofcIfEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (OFC_FSOFCIFTABLE_POOLID, (UINT1 *) pOfcFsofcIfEntry);
    MemReleaseMemBlock (OFC_FSOFCIFTABLE_ISSET_POOLID,
                        (UINT1 *) pOfcIsSetFsofcIfEntry);
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsofcVlanUntaggedPorts
 Input       :  The Indices
                FsofcContextId
                FsofcIfIndex

                The Object 
             :  tSNMP_OCTET_STRING_TYPE *pSetValFsofcVlanUntaggedPorts
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsofcVlanUntaggedPorts (UINT4 u4FsofcContextId, INT4 i4FsofcIfIndex,
                              tSNMP_OCTET_STRING_TYPE *
                              pSetValFsofcVlanUntaggedPorts)
{
    tOfcFsofcIfEntry   *pOfcFsofcIfEntry = NULL;
    tOfcIsSetFsofcIfEntry *pOfcIsSetFsofcIfEntry = NULL;

    pOfcFsofcIfEntry =
        (tOfcFsofcIfEntry *) MemAllocMemBlk (OFC_FSOFCIFTABLE_POOLID);
    if (pOfcFsofcIfEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    pOfcIsSetFsofcIfEntry =
        (tOfcIsSetFsofcIfEntry *)
        MemAllocMemBlk (OFC_FSOFCIFTABLE_ISSET_POOLID);
    if (pOfcIsSetFsofcIfEntry == NULL)
    {
        MemReleaseMemBlock (OFC_FSOFCIFTABLE_POOLID,
                            (UINT1 *) pOfcFsofcIfEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pOfcFsofcIfEntry, 0, sizeof (tOfcFsofcIfEntry));
    MEMSET (pOfcIsSetFsofcIfEntry, 0, sizeof (tOfcIsSetFsofcIfEntry));

    /* Assign the index */
    pOfcFsofcIfEntry->MibObject.u4FsofcContextId = u4FsofcContextId;
    pOfcIsSetFsofcIfEntry->bFsofcContextId = OSIX_TRUE;

    pOfcFsofcIfEntry->MibObject.i4FsofcIfIndex = i4FsofcIfIndex;
    pOfcIsSetFsofcIfEntry->bFsofcIfIndex = OSIX_TRUE;

    /* Assign the value */
    MEMCPY (pOfcFsofcIfEntry->MibObject.au1FsofcVlanUntaggedPorts,
            pSetValFsofcVlanUntaggedPorts->pu1_OctetList,
            pSetValFsofcVlanUntaggedPorts->i4_Length);
    pOfcFsofcIfEntry->MibObject.i4FsofcVlanUntaggedPortsLen =
        pSetValFsofcVlanUntaggedPorts->i4_Length;

    pOfcIsSetFsofcIfEntry->bFsofcVlanUntaggedPorts = OSIX_TRUE;

    if (OfcSetAllFsofcIfTable (pOfcFsofcIfEntry, pOfcIsSetFsofcIfEntry) !=
        OSIX_SUCCESS)
    {

        MemReleaseMemBlock (OFC_FSOFCIFTABLE_POOLID,
                            (UINT1 *) pOfcFsofcIfEntry);
        MemReleaseMemBlock (OFC_FSOFCIFTABLE_ISSET_POOLID,
                            (UINT1 *) pOfcIsSetFsofcIfEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (OFC_FSOFCIFTABLE_POOLID, (UINT1 *) pOfcFsofcIfEntry);
    MemReleaseMemBlock (OFC_FSOFCIFTABLE_ISSET_POOLID,
                        (UINT1 *) pOfcIsSetFsofcIfEntry);
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsofcIfContextId
 Input       :  The Indices
                FsofcContextId
                FsofcIfIndex

                The Object 
             :  UINT4 u4SetValFsofcIfContextId
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsofcIfContextId (UINT4 u4FsofcContextId, INT4 i4FsofcIfIndex,
                        UINT4 u4SetValFsofcIfContextId)
{
    tOfcFsofcIfEntry   *pOfcFsofcIfEntry = NULL;
    tOfcIsSetFsofcIfEntry *pOfcIsSetFsofcIfEntry = NULL;

    pOfcFsofcIfEntry =
        (tOfcFsofcIfEntry *) MemAllocMemBlk (OFC_FSOFCIFTABLE_POOLID);
    if (pOfcFsofcIfEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    pOfcIsSetFsofcIfEntry =
        (tOfcIsSetFsofcIfEntry *)
        MemAllocMemBlk (OFC_FSOFCIFTABLE_ISSET_POOLID);
    if (pOfcIsSetFsofcIfEntry == NULL)
    {
        MemReleaseMemBlock (OFC_FSOFCIFTABLE_POOLID,
                            (UINT1 *) pOfcFsofcIfEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pOfcFsofcIfEntry, 0, sizeof (tOfcFsofcIfEntry));
    MEMSET (pOfcIsSetFsofcIfEntry, 0, sizeof (tOfcIsSetFsofcIfEntry));

    /* Assign the index */
    pOfcFsofcIfEntry->MibObject.u4FsofcContextId = u4FsofcContextId;
    pOfcIsSetFsofcIfEntry->bFsofcContextId = OSIX_TRUE;

    pOfcFsofcIfEntry->MibObject.i4FsofcIfIndex = i4FsofcIfIndex;
    pOfcIsSetFsofcIfEntry->bFsofcIfIndex = OSIX_TRUE;

    /* Assign the value */
    pOfcFsofcIfEntry->MibObject.u4FsofcIfContextId = u4SetValFsofcIfContextId;
    pOfcIsSetFsofcIfEntry->bFsofcIfContextId = OSIX_TRUE;

    if (OfcSetAllFsofcIfTable (pOfcFsofcIfEntry, pOfcIsSetFsofcIfEntry) !=
        OSIX_SUCCESS)
    {

        MemReleaseMemBlock (OFC_FSOFCIFTABLE_POOLID,
                            (UINT1 *) pOfcFsofcIfEntry);
        MemReleaseMemBlock (OFC_FSOFCIFTABLE_ISSET_POOLID,
                            (UINT1 *) pOfcIsSetFsofcIfEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (OFC_FSOFCIFTABLE_POOLID, (UINT1 *) pOfcFsofcIfEntry);
    MemReleaseMemBlock (OFC_FSOFCIFTABLE_ISSET_POOLID,
                        (UINT1 *) pOfcIsSetFsofcIfEntry);
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsofcModuleStatus
 Input       :  The Indices
                FsofcContextId

                The Object 
                testValFsofcModuleStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsofcModuleStatus (UINT4 *pu4ErrorCode, UINT4 u4FsofcContextId,
                            INT4 i4TestValFsofcModuleStatus)
{
    tOfcFsofcCfgEntry  *pOfcFsofcCfgEntry = NULL;
    tOfcIsSetFsofcCfgEntry *pOfcIsSetFsofcCfgEntry = NULL;

    pOfcFsofcCfgEntry =
        (tOfcFsofcCfgEntry *) MemAllocMemBlk (OFC_FSOFCCFGTABLE_POOLID);

    if (pOfcFsofcCfgEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pOfcIsSetFsofcCfgEntry =
        (tOfcIsSetFsofcCfgEntry *)
        MemAllocMemBlk (OFC_FSOFCCFGTABLE_ISSET_POOLID);

    if (pOfcIsSetFsofcCfgEntry == NULL)
    {
        MemReleaseMemBlock (OFC_FSOFCCFGTABLE_POOLID,
                            (UINT1 *) pOfcFsofcCfgEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pOfcFsofcCfgEntry, OFC_ZERO, sizeof (tOfcFsofcCfgEntry));
    MEMSET (pOfcIsSetFsofcCfgEntry, OFC_ZERO, sizeof (tOfcIsSetFsofcCfgEntry));

    /* Assign the index */
    pOfcFsofcCfgEntry->MibObject.u4FsofcContextId = u4FsofcContextId;
    pOfcIsSetFsofcCfgEntry->bFsofcContextId = OSIX_TRUE;

    /* Assign the value */
    pOfcFsofcCfgEntry->MibObject.i4FsofcModuleStatus =
        i4TestValFsofcModuleStatus;
    pOfcIsSetFsofcCfgEntry->bFsofcModuleStatus = OSIX_TRUE;

    if (OfcTestAllFsofcCfgTable (pu4ErrorCode, pOfcFsofcCfgEntry,
                                 pOfcIsSetFsofcCfgEntry, OSIX_FALSE,
                                 OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (OFC_FSOFCCFGTABLE_POOLID,
                            (UINT1 *) pOfcFsofcCfgEntry);
        MemReleaseMemBlock (OFC_FSOFCCFGTABLE_ISSET_POOLID,
                            (UINT1 *) pOfcIsSetFsofcCfgEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (OFC_FSOFCCFGTABLE_POOLID, (UINT1 *) pOfcFsofcCfgEntry);
    MemReleaseMemBlock (OFC_FSOFCCFGTABLE_ISSET_POOLID,
                        (UINT1 *) pOfcIsSetFsofcCfgEntry);
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsofcSupportedVersion
 Input       :  The Indices
                FsofcContextId

                The Object 
                testValFsofcSupportedVersion
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsofcSupportedVersion (UINT4 *pu4ErrorCode, UINT4 u4FsofcContextId,
                                INT4 i4TestValFsofcSupportedVersion)
{
    tOfcFsofcCfgEntry  *pOfcFsofcCfgEntry = NULL;
    tOfcIsSetFsofcCfgEntry *pOfcIsSetFsofcCfgEntry = NULL;

    pOfcFsofcCfgEntry =
        (tOfcFsofcCfgEntry *) MemAllocMemBlk (OFC_FSOFCCFGTABLE_POOLID);

    if (pOfcFsofcCfgEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pOfcIsSetFsofcCfgEntry =
        (tOfcIsSetFsofcCfgEntry *)
        MemAllocMemBlk (OFC_FSOFCCFGTABLE_ISSET_POOLID);

    if (pOfcIsSetFsofcCfgEntry == NULL)
    {
        MemReleaseMemBlock (OFC_FSOFCCFGTABLE_POOLID,
                            (UINT1 *) pOfcFsofcCfgEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pOfcFsofcCfgEntry, OFC_ZERO, sizeof (tOfcFsofcCfgEntry));
    MEMSET (pOfcIsSetFsofcCfgEntry, OFC_ZERO, sizeof (tOfcIsSetFsofcCfgEntry));

    /* Assign the index */
    pOfcFsofcCfgEntry->MibObject.u4FsofcContextId = u4FsofcContextId;
    pOfcIsSetFsofcCfgEntry->bFsofcContextId = OSIX_TRUE;

    /* Assign the value */
    pOfcFsofcCfgEntry->MibObject.i4FsofcSupportedVersion =
        i4TestValFsofcSupportedVersion;
    pOfcIsSetFsofcCfgEntry->bFsofcSupportedVersion = OSIX_TRUE;

    if (OfcTestAllFsofcCfgTable (pu4ErrorCode, pOfcFsofcCfgEntry,
                                 pOfcIsSetFsofcCfgEntry, OSIX_FALSE,
                                 OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (OFC_FSOFCCFGTABLE_POOLID,
                            (UINT1 *) pOfcFsofcCfgEntry);
        MemReleaseMemBlock (OFC_FSOFCCFGTABLE_ISSET_POOLID,
                            (UINT1 *) pOfcIsSetFsofcCfgEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (OFC_FSOFCCFGTABLE_POOLID, (UINT1 *) pOfcFsofcCfgEntry);
    MemReleaseMemBlock (OFC_FSOFCCFGTABLE_ISSET_POOLID,
                        (UINT1 *) pOfcIsSetFsofcCfgEntry);
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsofcDefaultFlowMissBehaviour
 Input       :  The Indices
                FsofcContextId

                The Object 
                testValFsofcDefaultFlowMissBehaviour
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsofcDefaultFlowMissBehaviour (UINT4 *pu4ErrorCode,
                                        UINT4 u4FsofcContextId,
                                        INT4
                                        i4TestValFsofcDefaultFlowMissBehaviour)
{
    tOfcFsofcCfgEntry  *pOfcFsofcCfgEntry = NULL;
    tOfcIsSetFsofcCfgEntry *pOfcIsSetFsofcCfgEntry = NULL;

    pOfcFsofcCfgEntry =
        (tOfcFsofcCfgEntry *) MemAllocMemBlk (OFC_FSOFCCFGTABLE_POOLID);

    if (pOfcFsofcCfgEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pOfcIsSetFsofcCfgEntry =
        (tOfcIsSetFsofcCfgEntry *)
        MemAllocMemBlk (OFC_FSOFCCFGTABLE_ISSET_POOLID);

    if (pOfcIsSetFsofcCfgEntry == NULL)
    {
        MemReleaseMemBlock (OFC_FSOFCCFGTABLE_POOLID,
                            (UINT1 *) pOfcFsofcCfgEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pOfcFsofcCfgEntry, OFC_ZERO, sizeof (tOfcFsofcCfgEntry));
    MEMSET (pOfcIsSetFsofcCfgEntry, OFC_ZERO, sizeof (tOfcIsSetFsofcCfgEntry));

    /* Assign the index */
    pOfcFsofcCfgEntry->MibObject.u4FsofcContextId = u4FsofcContextId;
    pOfcIsSetFsofcCfgEntry->bFsofcContextId = OSIX_TRUE;

    /* Assign the value */
    pOfcFsofcCfgEntry->MibObject.i4FsofcDefaultFlowMissBehaviour =
        i4TestValFsofcDefaultFlowMissBehaviour;
    pOfcIsSetFsofcCfgEntry->bFsofcDefaultFlowMissBehaviour = OSIX_TRUE;

    if (OfcTestAllFsofcCfgTable (pu4ErrorCode, pOfcFsofcCfgEntry,
                                 pOfcIsSetFsofcCfgEntry, OSIX_FALSE,
                                 OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (OFC_FSOFCCFGTABLE_POOLID,
                            (UINT1 *) pOfcFsofcCfgEntry);
        MemReleaseMemBlock (OFC_FSOFCCFGTABLE_ISSET_POOLID,
                            (UINT1 *) pOfcIsSetFsofcCfgEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (OFC_FSOFCCFGTABLE_POOLID, (UINT1 *) pOfcFsofcCfgEntry);
    MemReleaseMemBlock (OFC_FSOFCCFGTABLE_ISSET_POOLID,
                        (UINT1 *) pOfcIsSetFsofcCfgEntry);
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsofcControlPktBuffering
 Input       :  The Indices
                FsofcContextId

                The Object 
                testValFsofcControlPktBuffering
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsofcControlPktBuffering (UINT4 *pu4ErrorCode, UINT4 u4FsofcContextId,
                                   INT4 i4TestValFsofcControlPktBuffering)
{
    tOfcFsofcCfgEntry  *pOfcFsofcCfgEntry = NULL;
    tOfcIsSetFsofcCfgEntry *pOfcIsSetFsofcCfgEntry = NULL;

    pOfcFsofcCfgEntry =
        (tOfcFsofcCfgEntry *) MemAllocMemBlk (OFC_FSOFCCFGTABLE_POOLID);

    if (pOfcFsofcCfgEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pOfcIsSetFsofcCfgEntry =
        (tOfcIsSetFsofcCfgEntry *)
        MemAllocMemBlk (OFC_FSOFCCFGTABLE_ISSET_POOLID);

    if (pOfcIsSetFsofcCfgEntry == NULL)
    {
        MemReleaseMemBlock (OFC_FSOFCCFGTABLE_POOLID,
                            (UINT1 *) pOfcFsofcCfgEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pOfcFsofcCfgEntry, OFC_ZERO, sizeof (tOfcFsofcCfgEntry));
    MEMSET (pOfcIsSetFsofcCfgEntry, OFC_ZERO, sizeof (tOfcIsSetFsofcCfgEntry));

    /* Assign the index */
    pOfcFsofcCfgEntry->MibObject.u4FsofcContextId = u4FsofcContextId;
    pOfcIsSetFsofcCfgEntry->bFsofcContextId = OSIX_TRUE;

    /* Assign the value */
    pOfcFsofcCfgEntry->MibObject.i4FsofcControlPktBuffering =
        i4TestValFsofcControlPktBuffering;
    pOfcIsSetFsofcCfgEntry->bFsofcControlPktBuffering = OSIX_TRUE;

    if (OfcTestAllFsofcCfgTable (pu4ErrorCode, pOfcFsofcCfgEntry,
                                 pOfcIsSetFsofcCfgEntry, OSIX_FALSE,
                                 OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (OFC_FSOFCCFGTABLE_POOLID,
                            (UINT1 *) pOfcFsofcCfgEntry);
        MemReleaseMemBlock (OFC_FSOFCCFGTABLE_ISSET_POOLID,
                            (UINT1 *) pOfcIsSetFsofcCfgEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (OFC_FSOFCCFGTABLE_POOLID, (UINT1 *) pOfcFsofcCfgEntry);
    MemReleaseMemBlock (OFC_FSOFCCFGTABLE_ISSET_POOLID,
                        (UINT1 *) pOfcIsSetFsofcCfgEntry);
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsofcIpReassembleStatus
 Input       :  The Indices
                FsofcContextId

                The Object 
                testValFsofcIpReassembleStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsofcIpReassembleStatus (UINT4 *pu4ErrorCode, UINT4 u4FsofcContextId,
                                  INT4 i4TestValFsofcIpReassembleStatus)
{
    tOfcFsofcCfgEntry  *pOfcFsofcCfgEntry = NULL;
    tOfcIsSetFsofcCfgEntry *pOfcIsSetFsofcCfgEntry = NULL;

    pOfcFsofcCfgEntry =
        (tOfcFsofcCfgEntry *) MemAllocMemBlk (OFC_FSOFCCFGTABLE_POOLID);

    if (pOfcFsofcCfgEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pOfcIsSetFsofcCfgEntry =
        (tOfcIsSetFsofcCfgEntry *)
        MemAllocMemBlk (OFC_FSOFCCFGTABLE_ISSET_POOLID);

    if (pOfcIsSetFsofcCfgEntry == NULL)
    {
        MemReleaseMemBlock (OFC_FSOFCCFGTABLE_POOLID,
                            (UINT1 *) pOfcFsofcCfgEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pOfcFsofcCfgEntry, OFC_ZERO, sizeof (tOfcFsofcCfgEntry));
    MEMSET (pOfcIsSetFsofcCfgEntry, OFC_ZERO, sizeof (tOfcIsSetFsofcCfgEntry));

    /* Assign the index */
    pOfcFsofcCfgEntry->MibObject.u4FsofcContextId = u4FsofcContextId;
    pOfcIsSetFsofcCfgEntry->bFsofcContextId = OSIX_TRUE;

    /* Assign the value */
    pOfcFsofcCfgEntry->MibObject.i4FsofcIpReassembleStatus =
        i4TestValFsofcIpReassembleStatus;
    pOfcIsSetFsofcCfgEntry->bFsofcIpReassembleStatus = OSIX_TRUE;

    if (OfcTestAllFsofcCfgTable (pu4ErrorCode, pOfcFsofcCfgEntry,
                                 pOfcIsSetFsofcCfgEntry, OSIX_FALSE,
                                 OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (OFC_FSOFCCFGTABLE_POOLID,
                            (UINT1 *) pOfcFsofcCfgEntry);
        MemReleaseMemBlock (OFC_FSOFCCFGTABLE_ISSET_POOLID,
                            (UINT1 *) pOfcIsSetFsofcCfgEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (OFC_FSOFCCFGTABLE_POOLID, (UINT1 *) pOfcFsofcCfgEntry);
    MemReleaseMemBlock (OFC_FSOFCCFGTABLE_ISSET_POOLID,
                        (UINT1 *) pOfcIsSetFsofcCfgEntry);
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsofcPortStpStatus
 Input       :  The Indices
                FsofcContextId

                The Object 
                testValFsofcPortStpStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsofcPortStpStatus (UINT4 *pu4ErrorCode, UINT4 u4FsofcContextId,
                             INT4 i4TestValFsofcPortStpStatus)
{
    tOfcFsofcCfgEntry  *pOfcFsofcCfgEntry = NULL;
    tOfcIsSetFsofcCfgEntry *pOfcIsSetFsofcCfgEntry = NULL;

    pOfcFsofcCfgEntry =
        (tOfcFsofcCfgEntry *) MemAllocMemBlk (OFC_FSOFCCFGTABLE_POOLID);

    if (pOfcFsofcCfgEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pOfcIsSetFsofcCfgEntry =
        (tOfcIsSetFsofcCfgEntry *)
        MemAllocMemBlk (OFC_FSOFCCFGTABLE_ISSET_POOLID);

    if (pOfcIsSetFsofcCfgEntry == NULL)
    {
        MemReleaseMemBlock (OFC_FSOFCCFGTABLE_POOLID,
                            (UINT1 *) pOfcFsofcCfgEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pOfcFsofcCfgEntry, OFC_ZERO, sizeof (tOfcFsofcCfgEntry));
    MEMSET (pOfcIsSetFsofcCfgEntry, OFC_ZERO, sizeof (tOfcIsSetFsofcCfgEntry));

    /* Assign the index */
    pOfcFsofcCfgEntry->MibObject.u4FsofcContextId = u4FsofcContextId;
    pOfcIsSetFsofcCfgEntry->bFsofcContextId = OSIX_TRUE;

    /* Assign the value */
    pOfcFsofcCfgEntry->MibObject.i4FsofcPortStpStatus =
        i4TestValFsofcPortStpStatus;
    pOfcIsSetFsofcCfgEntry->bFsofcPortStpStatus = OSIX_TRUE;

    if (OfcTestAllFsofcCfgTable (pu4ErrorCode, pOfcFsofcCfgEntry,
                                 pOfcIsSetFsofcCfgEntry, OSIX_FALSE,
                                 OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (OFC_FSOFCCFGTABLE_POOLID,
                            (UINT1 *) pOfcFsofcCfgEntry);
        MemReleaseMemBlock (OFC_FSOFCCFGTABLE_ISSET_POOLID,
                            (UINT1 *) pOfcIsSetFsofcCfgEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (OFC_FSOFCCFGTABLE_POOLID, (UINT1 *) pOfcFsofcCfgEntry);
    MemReleaseMemBlock (OFC_FSOFCCFGTABLE_ISSET_POOLID,
                        (UINT1 *) pOfcIsSetFsofcCfgEntry);
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsofcTraceEnable
 Input       :  The Indices
                FsofcContextId

                The Object 
                testValFsofcTraceEnable
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsofcTraceEnable (UINT4 *pu4ErrorCode, UINT4 u4FsofcContextId,
                           UINT4 u4TestValFsofcTraceEnable)
{
    tOfcFsofcCfgEntry  *pOfcFsofcCfgEntry = NULL;
    tOfcIsSetFsofcCfgEntry *pOfcIsSetFsofcCfgEntry = NULL;

    pOfcFsofcCfgEntry =
        (tOfcFsofcCfgEntry *) MemAllocMemBlk (OFC_FSOFCCFGTABLE_POOLID);

    if (pOfcFsofcCfgEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pOfcIsSetFsofcCfgEntry =
        (tOfcIsSetFsofcCfgEntry *)
        MemAllocMemBlk (OFC_FSOFCCFGTABLE_ISSET_POOLID);

    if (pOfcIsSetFsofcCfgEntry == NULL)
    {
        MemReleaseMemBlock (OFC_FSOFCCFGTABLE_POOLID,
                            (UINT1 *) pOfcFsofcCfgEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pOfcFsofcCfgEntry, OFC_ZERO, sizeof (tOfcFsofcCfgEntry));
    MEMSET (pOfcIsSetFsofcCfgEntry, OFC_ZERO, sizeof (tOfcIsSetFsofcCfgEntry));

    /* Assign the index */
    pOfcFsofcCfgEntry->MibObject.u4FsofcContextId = u4FsofcContextId;
    pOfcIsSetFsofcCfgEntry->bFsofcContextId = OSIX_TRUE;

    /* Assign the value */
    pOfcFsofcCfgEntry->MibObject.u4FsofcTraceEnable = u4TestValFsofcTraceEnable;
    pOfcIsSetFsofcCfgEntry->bFsofcTraceEnable = OSIX_TRUE;

    if (OfcTestAllFsofcCfgTable (pu4ErrorCode, pOfcFsofcCfgEntry,
                                 pOfcIsSetFsofcCfgEntry, OSIX_FALSE,
                                 OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (OFC_FSOFCCFGTABLE_POOLID,
                            (UINT1 *) pOfcFsofcCfgEntry);
        MemReleaseMemBlock (OFC_FSOFCCFGTABLE_ISSET_POOLID,
                            (UINT1 *) pOfcIsSetFsofcCfgEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (OFC_FSOFCCFGTABLE_POOLID, (UINT1 *) pOfcFsofcCfgEntry);
    MemReleaseMemBlock (OFC_FSOFCCFGTABLE_ISSET_POOLID,
                        (UINT1 *) pOfcIsSetFsofcCfgEntry);
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsofcSwitchModeOnConnFailure
 Input       :  The Indices
                FsofcContextId

                The Object 
                testValFsofcSwitchModeOnConnFailure
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsofcSwitchModeOnConnFailure (UINT4 *pu4ErrorCode,
                                       UINT4 u4FsofcContextId,
                                       INT4
                                       i4TestValFsofcSwitchModeOnConnFailure)
{
    tOfcFsofcCfgEntry  *pOfcFsofcCfgEntry = NULL;
    tOfcIsSetFsofcCfgEntry *pOfcIsSetFsofcCfgEntry = NULL;

    pOfcFsofcCfgEntry =
        (tOfcFsofcCfgEntry *) MemAllocMemBlk (OFC_FSOFCCFGTABLE_POOLID);

    if (pOfcFsofcCfgEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pOfcIsSetFsofcCfgEntry =
        (tOfcIsSetFsofcCfgEntry *)
        MemAllocMemBlk (OFC_FSOFCCFGTABLE_ISSET_POOLID);

    if (pOfcIsSetFsofcCfgEntry == NULL)
    {
        MemReleaseMemBlock (OFC_FSOFCCFGTABLE_POOLID,
                            (UINT1 *) pOfcFsofcCfgEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pOfcFsofcCfgEntry, OFC_ZERO, sizeof (tOfcFsofcCfgEntry));
    MEMSET (pOfcIsSetFsofcCfgEntry, OFC_ZERO, sizeof (tOfcIsSetFsofcCfgEntry));

    /* Assign the index */
    pOfcFsofcCfgEntry->MibObject.u4FsofcContextId = u4FsofcContextId;
    pOfcIsSetFsofcCfgEntry->bFsofcContextId = OSIX_TRUE;

    /* Assign the value */
    pOfcFsofcCfgEntry->MibObject.i4FsofcSwitchModeOnConnFailure =
        i4TestValFsofcSwitchModeOnConnFailure;
    pOfcIsSetFsofcCfgEntry->bFsofcSwitchModeOnConnFailure = OSIX_TRUE;

    if (OfcTestAllFsofcCfgTable (pu4ErrorCode, pOfcFsofcCfgEntry,
                                 pOfcIsSetFsofcCfgEntry, OSIX_FALSE,
                                 OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (OFC_FSOFCCFGTABLE_POOLID,
                            (UINT1 *) pOfcFsofcCfgEntry);
        MemReleaseMemBlock (OFC_FSOFCCFGTABLE_ISSET_POOLID,
                            (UINT1 *) pOfcIsSetFsofcCfgEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (OFC_FSOFCCFGTABLE_POOLID, (UINT1 *) pOfcFsofcCfgEntry);
    MemReleaseMemBlock (OFC_FSOFCCFGTABLE_ISSET_POOLID,
                        (UINT1 *) pOfcIsSetFsofcCfgEntry);
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsofcSwitchEntryStatus
 Input       :  The Indices
                FsofcContextId

                The Object 
                testValFsofcSwitchEntryStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsofcSwitchEntryStatus (UINT4 *pu4ErrorCode, UINT4 u4FsofcContextId,
                                 INT4 i4TestValFsofcSwitchEntryStatus)
{
    tOfcFsofcCfgEntry  *pOfcFsofcCfgEntry = NULL;
    tOfcIsSetFsofcCfgEntry *pOfcIsSetFsofcCfgEntry = NULL;

    pOfcFsofcCfgEntry =
        (tOfcFsofcCfgEntry *) MemAllocMemBlk (OFC_FSOFCCFGTABLE_POOLID);

    if (pOfcFsofcCfgEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pOfcIsSetFsofcCfgEntry =
        (tOfcIsSetFsofcCfgEntry *)
        MemAllocMemBlk (OFC_FSOFCCFGTABLE_ISSET_POOLID);

    if (pOfcIsSetFsofcCfgEntry == NULL)
    {
        MemReleaseMemBlock (OFC_FSOFCCFGTABLE_POOLID,
                            (UINT1 *) pOfcFsofcCfgEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pOfcFsofcCfgEntry, OFC_ZERO, sizeof (tOfcFsofcCfgEntry));
    MEMSET (pOfcIsSetFsofcCfgEntry, OFC_ZERO, sizeof (tOfcIsSetFsofcCfgEntry));

    /* Assign the index */
    pOfcFsofcCfgEntry->MibObject.u4FsofcContextId = u4FsofcContextId;
    pOfcIsSetFsofcCfgEntry->bFsofcContextId = OSIX_TRUE;

    /* Assign the value */
    pOfcFsofcCfgEntry->MibObject.i4FsofcSwitchEntryStatus =
        i4TestValFsofcSwitchEntryStatus;
    pOfcIsSetFsofcCfgEntry->bFsofcSwitchEntryStatus = OSIX_TRUE;

    if (OfcTestAllFsofcCfgTable (pu4ErrorCode, pOfcFsofcCfgEntry,
                                 pOfcIsSetFsofcCfgEntry, OSIX_FALSE,
                                 OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (OFC_FSOFCCFGTABLE_POOLID,
                            (UINT1 *) pOfcFsofcCfgEntry);
        MemReleaseMemBlock (OFC_FSOFCCFGTABLE_ISSET_POOLID,
                            (UINT1 *) pOfcIsSetFsofcCfgEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (OFC_FSOFCCFGTABLE_POOLID, (UINT1 *) pOfcFsofcCfgEntry);
    MemReleaseMemBlock (OFC_FSOFCCFGTABLE_ISSET_POOLID,
                        (UINT1 *) pOfcIsSetFsofcCfgEntry);
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsofcHybrid
 Input       :  The Indices
                FsofcContextId

                The Object 
                testValFsofcHybrid
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsofcHybrid (UINT4 *pu4ErrorCode, UINT4 u4FsofcContextId,
                      INT4 i4TestValFsofcHybrid)
{
    tOfcFsofcCfgEntry  *pOfcFsofcCfgEntry = NULL;
    tOfcIsSetFsofcCfgEntry *pOfcIsSetFsofcCfgEntry = NULL;

    pOfcFsofcCfgEntry =
        (tOfcFsofcCfgEntry *) MemAllocMemBlk (OFC_FSOFCCFGTABLE_POOLID);

    if (pOfcFsofcCfgEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pOfcIsSetFsofcCfgEntry =
        (tOfcIsSetFsofcCfgEntry *)
        MemAllocMemBlk (OFC_FSOFCCFGTABLE_ISSET_POOLID);

    if (pOfcIsSetFsofcCfgEntry == NULL)
    {
        MemReleaseMemBlock (OFC_FSOFCCFGTABLE_POOLID,
                            (UINT1 *) pOfcFsofcCfgEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pOfcFsofcCfgEntry, 0, sizeof (tOfcFsofcCfgEntry));
    MEMSET (pOfcIsSetFsofcCfgEntry, 0, sizeof (tOfcIsSetFsofcCfgEntry));

    /* Assign the index */
    pOfcFsofcCfgEntry->MibObject.u4FsofcContextId = u4FsofcContextId;
    pOfcIsSetFsofcCfgEntry->bFsofcContextId = OSIX_TRUE;

    /* Assign the value */
    pOfcFsofcCfgEntry->MibObject.i4FsofcHybrid = i4TestValFsofcHybrid;
    pOfcIsSetFsofcCfgEntry->bFsofcHybrid = OSIX_TRUE;

    if (OfcTestAllFsofcCfgTable (pu4ErrorCode, pOfcFsofcCfgEntry,
                                 pOfcIsSetFsofcCfgEntry, OSIX_FALSE,
                                 OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (OFC_FSOFCCFGTABLE_POOLID,
                            (UINT1 *) pOfcFsofcCfgEntry);
        MemReleaseMemBlock (OFC_FSOFCCFGTABLE_ISSET_POOLID,
                            (UINT1 *) pOfcIsSetFsofcCfgEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (OFC_FSOFCCFGTABLE_POOLID, (UINT1 *) pOfcFsofcCfgEntry);
    MemReleaseMemBlock (OFC_FSOFCCFGTABLE_ISSET_POOLID,
                        (UINT1 *) pOfcIsSetFsofcCfgEntry);
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsofcControllerConnPort
 Input       :  The Indices
                FsofcContextId
                FsofcControllerIpAddrType
                FsofcControllerIpAddress
                FsofcControllerConnAuxId

                The Object 
                testValFsofcControllerConnPort
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsofcControllerConnPort (UINT4 *pu4ErrorCode, UINT4 u4FsofcContextId,
                                  INT4 i4FsofcControllerIpAddrType,
                                  tSNMP_OCTET_STRING_TYPE *
                                  pFsofcControllerIpAddress,
                                  INT4 i4FsofcControllerConnAuxId,
                                  INT4 i4TestValFsofcControllerConnPort)
{
    tOfcFsofcControllerConnEntry *pOfcFsofcControllerConnEntry = NULL;
    tOfcIsSetFsofcControllerConnEntry *pOfcIsSetFsofcControllerConnEntry = NULL;

    pOfcFsofcControllerConnEntry =
        (tOfcFsofcControllerConnEntry *)
        MemAllocMemBlk (OFC_FSOFCCONTROLLERCONNTABLE_POOLID);

    if (pOfcFsofcControllerConnEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pOfcIsSetFsofcControllerConnEntry =
        (tOfcIsSetFsofcControllerConnEntry *)
        MemAllocMemBlk (OFC_FSOFCCONTROLLERCONNTABLE_ISSET_POOLID);

    if (pOfcIsSetFsofcControllerConnEntry == NULL)
    {
        MemReleaseMemBlock (OFC_FSOFCCONTROLLERCONNTABLE_POOLID,
                            (UINT1 *) pOfcFsofcControllerConnEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pOfcFsofcControllerConnEntry, OFC_ZERO,
            sizeof (tOfcFsofcControllerConnEntry));
    MEMSET (pOfcIsSetFsofcControllerConnEntry, OFC_ZERO,
            sizeof (tOfcIsSetFsofcControllerConnEntry));

    /* Assign the index */
    pOfcFsofcControllerConnEntry->MibObject.u4FsofcContextId = u4FsofcContextId;
    pOfcIsSetFsofcControllerConnEntry->bFsofcContextId = OSIX_TRUE;

    pOfcFsofcControllerConnEntry->MibObject.i4FsofcControllerIpAddrType =
        i4FsofcControllerIpAddrType;
    pOfcIsSetFsofcControllerConnEntry->bFsofcControllerIpAddrType = OSIX_TRUE;

    MEMCPY (pOfcFsofcControllerConnEntry->MibObject.au1FsofcControllerIpAddress,
            pFsofcControllerIpAddress->pu1_OctetList,
            pFsofcControllerIpAddress->i4_Length);

    pOfcFsofcControllerConnEntry->MibObject.i4FsofcControllerIpAddressLen =
        pFsofcControllerIpAddress->i4_Length;
    pOfcIsSetFsofcControllerConnEntry->bFsofcControllerIpAddress = OSIX_TRUE;

    pOfcFsofcControllerConnEntry->MibObject.i4FsofcControllerConnAuxId =
        i4FsofcControllerConnAuxId;
    pOfcIsSetFsofcControllerConnEntry->bFsofcControllerConnAuxId = OSIX_TRUE;

    /* Assign the value */
    pOfcFsofcControllerConnEntry->MibObject.i4FsofcControllerConnPort =
        i4TestValFsofcControllerConnPort;
    pOfcIsSetFsofcControllerConnEntry->bFsofcControllerConnPort = OSIX_TRUE;

    if (OfcTestAllFsofcControllerConnTable
        (pu4ErrorCode, pOfcFsofcControllerConnEntry,
         pOfcIsSetFsofcControllerConnEntry, OSIX_FALSE,
         OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (OFC_FSOFCCONTROLLERCONNTABLE_POOLID,
                            (UINT1 *) pOfcFsofcControllerConnEntry);
        MemReleaseMemBlock (OFC_FSOFCCONTROLLERCONNTABLE_ISSET_POOLID,
                            (UINT1 *) pOfcIsSetFsofcControllerConnEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (OFC_FSOFCCONTROLLERCONNTABLE_POOLID,
                        (UINT1 *) pOfcFsofcControllerConnEntry);
    MemReleaseMemBlock (OFC_FSOFCCONTROLLERCONNTABLE_ISSET_POOLID,
                        (UINT1 *) pOfcIsSetFsofcControllerConnEntry);
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsofcControllerConnProtocol
 Input       :  The Indices
                FsofcContextId
                FsofcControllerIpAddrType
                FsofcControllerIpAddress
                FsofcControllerConnAuxId

                The Object 
                testValFsofcControllerConnProtocol
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsofcControllerConnProtocol (UINT4 *pu4ErrorCode,
                                      UINT4 u4FsofcContextId,
                                      INT4 i4FsofcControllerIpAddrType,
                                      tSNMP_OCTET_STRING_TYPE *
                                      pFsofcControllerIpAddress,
                                      INT4 i4FsofcControllerConnAuxId,
                                      INT4 i4TestValFsofcControllerConnProtocol)
{
    tOfcFsofcControllerConnEntry *pOfcFsofcControllerConnEntry = NULL;
    tOfcIsSetFsofcControllerConnEntry *pOfcIsSetFsofcControllerConnEntry = NULL;

    pOfcFsofcControllerConnEntry =
        (tOfcFsofcControllerConnEntry *)
        MemAllocMemBlk (OFC_FSOFCCONTROLLERCONNTABLE_POOLID);

    if (pOfcFsofcControllerConnEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pOfcIsSetFsofcControllerConnEntry =
        (tOfcIsSetFsofcControllerConnEntry *)
        MemAllocMemBlk (OFC_FSOFCCONTROLLERCONNTABLE_ISSET_POOLID);

    if (pOfcIsSetFsofcControllerConnEntry == NULL)
    {
        MemReleaseMemBlock (OFC_FSOFCCONTROLLERCONNTABLE_POOLID,
                            (UINT1 *) pOfcFsofcControllerConnEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pOfcFsofcControllerConnEntry, OFC_ZERO,
            sizeof (tOfcFsofcControllerConnEntry));
    MEMSET (pOfcIsSetFsofcControllerConnEntry, OFC_ZERO,
            sizeof (tOfcIsSetFsofcControllerConnEntry));

    /* Assign the index */
    pOfcFsofcControllerConnEntry->MibObject.u4FsofcContextId = u4FsofcContextId;
    pOfcIsSetFsofcControllerConnEntry->bFsofcContextId = OSIX_TRUE;

    pOfcFsofcControllerConnEntry->MibObject.i4FsofcControllerIpAddrType =
        i4FsofcControllerIpAddrType;
    pOfcIsSetFsofcControllerConnEntry->bFsofcControllerIpAddrType = OSIX_TRUE;

    MEMCPY (pOfcFsofcControllerConnEntry->MibObject.au1FsofcControllerIpAddress,
            pFsofcControllerIpAddress->pu1_OctetList,
            pFsofcControllerIpAddress->i4_Length);

    pOfcFsofcControllerConnEntry->MibObject.i4FsofcControllerIpAddressLen =
        pFsofcControllerIpAddress->i4_Length;
    pOfcIsSetFsofcControllerConnEntry->bFsofcControllerIpAddress = OSIX_TRUE;

    pOfcFsofcControllerConnEntry->MibObject.i4FsofcControllerConnAuxId =
        i4FsofcControllerConnAuxId;
    pOfcIsSetFsofcControllerConnEntry->bFsofcControllerConnAuxId = OSIX_TRUE;

    /* Assign the value */
    pOfcFsofcControllerConnEntry->MibObject.i4FsofcControllerConnProtocol =
        i4TestValFsofcControllerConnProtocol;
    pOfcIsSetFsofcControllerConnEntry->bFsofcControllerConnProtocol = OSIX_TRUE;

    if (OfcTestAllFsofcControllerConnTable
        (pu4ErrorCode, pOfcFsofcControllerConnEntry,
         pOfcIsSetFsofcControllerConnEntry, OSIX_FALSE,
         OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (OFC_FSOFCCONTROLLERCONNTABLE_POOLID,
                            (UINT1 *) pOfcFsofcControllerConnEntry);
        MemReleaseMemBlock (OFC_FSOFCCONTROLLERCONNTABLE_ISSET_POOLID,
                            (UINT1 *) pOfcIsSetFsofcControllerConnEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (OFC_FSOFCCONTROLLERCONNTABLE_POOLID,
                        (UINT1 *) pOfcFsofcControllerConnEntry);
    MemReleaseMemBlock (OFC_FSOFCCONTROLLERCONNTABLE_ISSET_POOLID,
                        (UINT1 *) pOfcIsSetFsofcControllerConnEntry);
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsofcControllerRole
 Input       :  The Indices
                FsofcContextId
                FsofcControllerIpAddrType
                FsofcControllerIpAddress
                FsofcControllerConnAuxId

                The Object 
                testValFsofcControllerRole
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsofcControllerRole (UINT4 *pu4ErrorCode, UINT4 u4FsofcContextId,
                              INT4 i4FsofcControllerIpAddrType,
                              tSNMP_OCTET_STRING_TYPE *
                              pFsofcControllerIpAddress,
                              INT4 i4FsofcControllerConnAuxId,
                              INT4 i4TestValFsofcControllerRole)
{
    tOfcFsofcControllerConnEntry *pOfcFsofcControllerConnEntry = NULL;
    tOfcIsSetFsofcControllerConnEntry *pOfcIsSetFsofcControllerConnEntry = NULL;

    pOfcFsofcControllerConnEntry =
        (tOfcFsofcControllerConnEntry *)
        MemAllocMemBlk (OFC_FSOFCCONTROLLERCONNTABLE_POOLID);

    if (pOfcFsofcControllerConnEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pOfcIsSetFsofcControllerConnEntry =
        (tOfcIsSetFsofcControllerConnEntry *)
        MemAllocMemBlk (OFC_FSOFCCONTROLLERCONNTABLE_ISSET_POOLID);

    if (pOfcIsSetFsofcControllerConnEntry == NULL)
    {
        MemReleaseMemBlock (OFC_FSOFCCONTROLLERCONNTABLE_POOLID,
                            (UINT1 *) pOfcFsofcControllerConnEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pOfcFsofcControllerConnEntry, OFC_ZERO,
            sizeof (tOfcFsofcControllerConnEntry));
    MEMSET (pOfcIsSetFsofcControllerConnEntry, OFC_ZERO,
            sizeof (tOfcIsSetFsofcControllerConnEntry));

    /* Assign the index */
    pOfcFsofcControllerConnEntry->MibObject.u4FsofcContextId = u4FsofcContextId;
    pOfcIsSetFsofcControllerConnEntry->bFsofcContextId = OSIX_TRUE;

    pOfcFsofcControllerConnEntry->MibObject.i4FsofcControllerIpAddrType =
        i4FsofcControllerIpAddrType;
    pOfcIsSetFsofcControllerConnEntry->bFsofcControllerIpAddrType = OSIX_TRUE;

    MEMCPY (pOfcFsofcControllerConnEntry->MibObject.au1FsofcControllerIpAddress,
            pFsofcControllerIpAddress->pu1_OctetList,
            pFsofcControllerIpAddress->i4_Length);

    pOfcFsofcControllerConnEntry->MibObject.i4FsofcControllerIpAddressLen =
        pFsofcControllerIpAddress->i4_Length;
    pOfcIsSetFsofcControllerConnEntry->bFsofcControllerIpAddress = OSIX_TRUE;

    pOfcFsofcControllerConnEntry->MibObject.i4FsofcControllerConnAuxId =
        i4FsofcControllerConnAuxId;
    pOfcIsSetFsofcControllerConnEntry->bFsofcControllerConnAuxId = OSIX_TRUE;

    /* Assign the value */
    pOfcFsofcControllerConnEntry->MibObject.i4FsofcControllerRole =
        i4TestValFsofcControllerRole;
    pOfcIsSetFsofcControllerConnEntry->bFsofcControllerRole = OSIX_TRUE;

    if (OfcTestAllFsofcControllerConnTable
        (pu4ErrorCode, pOfcFsofcControllerConnEntry,
         pOfcIsSetFsofcControllerConnEntry, OSIX_FALSE,
         OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (OFC_FSOFCCONTROLLERCONNTABLE_POOLID,
                            (UINT1 *) pOfcFsofcControllerConnEntry);
        MemReleaseMemBlock (OFC_FSOFCCONTROLLERCONNTABLE_ISSET_POOLID,
                            (UINT1 *) pOfcIsSetFsofcControllerConnEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (OFC_FSOFCCONTROLLERCONNTABLE_POOLID,
                        (UINT1 *) pOfcFsofcControllerConnEntry);
    MemReleaseMemBlock (OFC_FSOFCCONTROLLERCONNTABLE_ISSET_POOLID,
                        (UINT1 *) pOfcIsSetFsofcControllerConnEntry);
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsofcControllerConnEchoReqCount
 Input       :  The Indices
                FsofcContextId
                FsofcControllerIpAddrType
                FsofcControllerIpAddress
                FsofcControllerConnAuxId

                The Object 
                testValFsofcControllerConnEchoReqCount
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsofcControllerConnEchoReqCount (UINT4 *pu4ErrorCode,
                                          UINT4 u4FsofcContextId,
                                          INT4 i4FsofcControllerIpAddrType,
                                          tSNMP_OCTET_STRING_TYPE *
                                          pFsofcControllerIpAddress,
                                          INT4 i4FsofcControllerConnAuxId,
                                          INT4
                                          i4TestValFsofcControllerConnEchoReqCount)
{
    tOfcFsofcControllerConnEntry *pOfcFsofcControllerConnEntry = NULL;
    tOfcIsSetFsofcControllerConnEntry *pOfcIsSetFsofcControllerConnEntry = NULL;

    pOfcFsofcControllerConnEntry =
        (tOfcFsofcControllerConnEntry *)
        MemAllocMemBlk (OFC_FSOFCCONTROLLERCONNTABLE_POOLID);

    if (pOfcFsofcControllerConnEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pOfcIsSetFsofcControllerConnEntry =
        (tOfcIsSetFsofcControllerConnEntry *)
        MemAllocMemBlk (OFC_FSOFCCONTROLLERCONNTABLE_ISSET_POOLID);

    if (pOfcIsSetFsofcControllerConnEntry == NULL)
    {
        MemReleaseMemBlock (OFC_FSOFCCONTROLLERCONNTABLE_POOLID,
                            (UINT1 *) pOfcFsofcControllerConnEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pOfcFsofcControllerConnEntry, OFC_ZERO,
            sizeof (tOfcFsofcControllerConnEntry));
    MEMSET (pOfcIsSetFsofcControllerConnEntry, OFC_ZERO,
            sizeof (tOfcIsSetFsofcControllerConnEntry));

    /* Assign the index */
    pOfcFsofcControllerConnEntry->MibObject.u4FsofcContextId = u4FsofcContextId;
    pOfcIsSetFsofcControllerConnEntry->bFsofcContextId = OSIX_TRUE;

    pOfcFsofcControllerConnEntry->MibObject.i4FsofcControllerIpAddrType =
        i4FsofcControllerIpAddrType;
    pOfcIsSetFsofcControllerConnEntry->bFsofcControllerIpAddrType = OSIX_TRUE;

    MEMCPY (pOfcFsofcControllerConnEntry->MibObject.au1FsofcControllerIpAddress,
            pFsofcControllerIpAddress->pu1_OctetList,
            pFsofcControllerIpAddress->i4_Length);

    pOfcFsofcControllerConnEntry->MibObject.i4FsofcControllerIpAddressLen =
        pFsofcControllerIpAddress->i4_Length;
    pOfcIsSetFsofcControllerConnEntry->bFsofcControllerIpAddress = OSIX_TRUE;

    pOfcFsofcControllerConnEntry->MibObject.i4FsofcControllerConnAuxId =
        i4FsofcControllerConnAuxId;
    pOfcIsSetFsofcControllerConnEntry->bFsofcControllerConnAuxId = OSIX_TRUE;

    /* Assign the value */
    pOfcFsofcControllerConnEntry->MibObject.i4FsofcControllerConnEchoReqCount =
        i4TestValFsofcControllerConnEchoReqCount;
    pOfcIsSetFsofcControllerConnEntry->bFsofcControllerConnEchoReqCount =
        OSIX_TRUE;

    if (OfcTestAllFsofcControllerConnTable
        (pu4ErrorCode, pOfcFsofcControllerConnEntry,
         pOfcIsSetFsofcControllerConnEntry, OSIX_FALSE,
         OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (OFC_FSOFCCONTROLLERCONNTABLE_POOLID,
                            (UINT1 *) pOfcFsofcControllerConnEntry);
        MemReleaseMemBlock (OFC_FSOFCCONTROLLERCONNTABLE_ISSET_POOLID,
                            (UINT1 *) pOfcIsSetFsofcControllerConnEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (OFC_FSOFCCONTROLLERCONNTABLE_POOLID,
                        (UINT1 *) pOfcFsofcControllerConnEntry);
    MemReleaseMemBlock (OFC_FSOFCCONTROLLERCONNTABLE_ISSET_POOLID,
                        (UINT1 *) pOfcIsSetFsofcControllerConnEntry);
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsofcControllerConnEntryStatus
 Input       :  The Indices
                FsofcContextId
                FsofcControllerIpAddrType
                FsofcControllerIpAddress
                FsofcControllerConnAuxId

                The Object 
                testValFsofcControllerConnEntryStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsofcControllerConnEntryStatus (UINT4 *pu4ErrorCode,
                                         UINT4 u4FsofcContextId,
                                         INT4 i4FsofcControllerIpAddrType,
                                         tSNMP_OCTET_STRING_TYPE *
                                         pFsofcControllerIpAddress,
                                         INT4 i4FsofcControllerConnAuxId,
                                         INT4
                                         i4TestValFsofcControllerConnEntryStatus)
{
    tOfcFsofcControllerConnEntry *pOfcFsofcControllerConnEntry = NULL;
    tOfcIsSetFsofcControllerConnEntry *pOfcIsSetFsofcControllerConnEntry = NULL;

    pOfcFsofcControllerConnEntry =
        (tOfcFsofcControllerConnEntry *)
        MemAllocMemBlk (OFC_FSOFCCONTROLLERCONNTABLE_POOLID);

    if (pOfcFsofcControllerConnEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pOfcIsSetFsofcControllerConnEntry =
        (tOfcIsSetFsofcControllerConnEntry *)
        MemAllocMemBlk (OFC_FSOFCCONTROLLERCONNTABLE_ISSET_POOLID);

    if (pOfcIsSetFsofcControllerConnEntry == NULL)
    {
        MemReleaseMemBlock (OFC_FSOFCCONTROLLERCONNTABLE_POOLID,
                            (UINT1 *) pOfcFsofcControllerConnEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pOfcFsofcControllerConnEntry, OFC_ZERO,
            sizeof (tOfcFsofcControllerConnEntry));
    MEMSET (pOfcIsSetFsofcControllerConnEntry, OFC_ZERO,
            sizeof (tOfcIsSetFsofcControllerConnEntry));

    /* Assign the index */
    pOfcFsofcControllerConnEntry->MibObject.u4FsofcContextId = u4FsofcContextId;
    pOfcIsSetFsofcControllerConnEntry->bFsofcContextId = OSIX_TRUE;

    pOfcFsofcControllerConnEntry->MibObject.i4FsofcControllerIpAddrType =
        i4FsofcControllerIpAddrType;
    pOfcIsSetFsofcControllerConnEntry->bFsofcControllerIpAddrType = OSIX_TRUE;

    MEMCPY (pOfcFsofcControllerConnEntry->MibObject.au1FsofcControllerIpAddress,
            pFsofcControllerIpAddress->pu1_OctetList,
            pFsofcControllerIpAddress->i4_Length);

    pOfcFsofcControllerConnEntry->MibObject.i4FsofcControllerIpAddressLen =
        pFsofcControllerIpAddress->i4_Length;
    pOfcIsSetFsofcControllerConnEntry->bFsofcControllerIpAddress = OSIX_TRUE;

    pOfcFsofcControllerConnEntry->MibObject.i4FsofcControllerConnAuxId =
        i4FsofcControllerConnAuxId;
    pOfcIsSetFsofcControllerConnEntry->bFsofcControllerConnAuxId = OSIX_TRUE;

    /* Assign the value */
    pOfcFsofcControllerConnEntry->MibObject.i4FsofcControllerConnEntryStatus =
        i4TestValFsofcControllerConnEntryStatus;
    pOfcIsSetFsofcControllerConnEntry->bFsofcControllerConnEntryStatus =
        OSIX_TRUE;

    if (OfcTestAllFsofcControllerConnTable
        (pu4ErrorCode, pOfcFsofcControllerConnEntry,
         pOfcIsSetFsofcControllerConnEntry, OSIX_FALSE,
         OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (OFC_FSOFCCONTROLLERCONNTABLE_POOLID,
                            (UINT1 *) pOfcFsofcControllerConnEntry);
        MemReleaseMemBlock (OFC_FSOFCCONTROLLERCONNTABLE_ISSET_POOLID,
                            (UINT1 *) pOfcIsSetFsofcControllerConnEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (OFC_FSOFCCONTROLLERCONNTABLE_POOLID,
                        (UINT1 *) pOfcFsofcControllerConnEntry);
    MemReleaseMemBlock (OFC_FSOFCCONTROLLERCONNTABLE_ISSET_POOLID,
                        (UINT1 *) pOfcIsSetFsofcControllerConnEntry);
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsofcControllerConnBand
 Input       :  The Indices
                FsofcContextId
                FsofcControllerIpAddrType
                FsofcControllerIpAddress
                FsofcControllerConnAuxId

                The Object 
                testValFsofcControllerConnBand
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsofcControllerConnBand (UINT4 *pu4ErrorCode, UINT4 u4FsofcContextId,
                                  INT4 i4FsofcControllerIpAddrType,
                                  tSNMP_OCTET_STRING_TYPE *
                                  pFsofcControllerIpAddress,
                                  INT4 i4FsofcControllerConnAuxId,
                                  INT4 i4TestValFsofcControllerConnBand)
{
    tOfcFsofcControllerConnEntry *pOfcFsofcControllerConnEntry = NULL;
    tOfcIsSetFsofcControllerConnEntry *pOfcIsSetFsofcControllerConnEntry = NULL;

    pOfcFsofcControllerConnEntry =
        (tOfcFsofcControllerConnEntry *)
        MemAllocMemBlk (OFC_FSOFCCONTROLLERCONNTABLE_POOLID);

    if (pOfcFsofcControllerConnEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pOfcIsSetFsofcControllerConnEntry =
        (tOfcIsSetFsofcControllerConnEntry *)
        MemAllocMemBlk (OFC_FSOFCCONTROLLERCONNTABLE_ISSET_POOLID);

    if (pOfcIsSetFsofcControllerConnEntry == NULL)
    {
        MemReleaseMemBlock (OFC_FSOFCCONTROLLERCONNTABLE_POOLID,
                            (UINT1 *) pOfcFsofcControllerConnEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pOfcFsofcControllerConnEntry, 0,
            sizeof (tOfcFsofcControllerConnEntry));
    MEMSET (pOfcIsSetFsofcControllerConnEntry, 0,
            sizeof (tOfcIsSetFsofcControllerConnEntry));

    /* Assign the index */
    pOfcFsofcControllerConnEntry->MibObject.u4FsofcContextId = u4FsofcContextId;
    pOfcIsSetFsofcControllerConnEntry->bFsofcContextId = OSIX_TRUE;

    pOfcFsofcControllerConnEntry->MibObject.i4FsofcControllerIpAddrType =
        i4FsofcControllerIpAddrType;
    pOfcIsSetFsofcControllerConnEntry->bFsofcControllerIpAddrType = OSIX_TRUE;

    MEMCPY (pOfcFsofcControllerConnEntry->MibObject.au1FsofcControllerIpAddress,
            pFsofcControllerIpAddress->pu1_OctetList,
            pFsofcControllerIpAddress->i4_Length);

    pOfcFsofcControllerConnEntry->MibObject.i4FsofcControllerIpAddressLen =
        pFsofcControllerIpAddress->i4_Length;
    pOfcIsSetFsofcControllerConnEntry->bFsofcControllerIpAddress = OSIX_TRUE;

    pOfcFsofcControllerConnEntry->MibObject.i4FsofcControllerConnAuxId =
        i4FsofcControllerConnAuxId;
    pOfcIsSetFsofcControllerConnEntry->bFsofcControllerConnAuxId = OSIX_TRUE;

    /* Assign the value */
    pOfcFsofcControllerConnEntry->MibObject.i4FsofcControllerConnBand =
        i4TestValFsofcControllerConnBand;
    pOfcIsSetFsofcControllerConnEntry->bFsofcControllerConnBand = OSIX_TRUE;

    if (OfcTestAllFsofcControllerConnTable
        (pu4ErrorCode, pOfcFsofcControllerConnEntry,
         pOfcIsSetFsofcControllerConnEntry, OSIX_FALSE,
         OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (OFC_FSOFCCONTROLLERCONNTABLE_POOLID,
                            (UINT1 *) pOfcFsofcControllerConnEntry);
        MemReleaseMemBlock (OFC_FSOFCCONTROLLERCONNTABLE_ISSET_POOLID,
                            (UINT1 *) pOfcIsSetFsofcControllerConnEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (OFC_FSOFCCONTROLLERCONNTABLE_POOLID,
                        (UINT1 *) pOfcFsofcControllerConnEntry);
    MemReleaseMemBlock (OFC_FSOFCCONTROLLERCONNTABLE_ISSET_POOLID,
                        (UINT1 *) pOfcIsSetFsofcControllerConnEntry);
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsofcVlanEgressPorts
 Input       :  The Indices
                FsofcContextId
                FsofcIfIndex

                The Object 
                testValFsofcVlanEgressPorts
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsofcVlanEgressPorts (UINT4 *pu4ErrorCode, UINT4 u4FsofcContextId,
                               INT4 i4FsofcIfIndex,
                               tSNMP_OCTET_STRING_TYPE *
                               pTestValFsofcVlanEgressPorts)
{
    tOfcFsofcIfEntry   *pOfcFsofcIfEntry = NULL;
    tOfcIsSetFsofcIfEntry *pOfcIsSetFsofcIfEntry = NULL;

    pOfcFsofcIfEntry =
        (tOfcFsofcIfEntry *) MemAllocMemBlk (OFC_FSOFCIFTABLE_POOLID);

    if (pOfcFsofcIfEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pOfcIsSetFsofcIfEntry =
        (tOfcIsSetFsofcIfEntry *)
        MemAllocMemBlk (OFC_FSOFCIFTABLE_ISSET_POOLID);

    if (pOfcIsSetFsofcIfEntry == NULL)
    {
        MemReleaseMemBlock (OFC_FSOFCIFTABLE_POOLID,
                            (UINT1 *) pOfcFsofcIfEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pOfcFsofcIfEntry, 0, sizeof (tOfcFsofcIfEntry));
    MEMSET (pOfcIsSetFsofcIfEntry, 0, sizeof (tOfcIsSetFsofcIfEntry));

    /* Assign the index */
    pOfcFsofcIfEntry->MibObject.u4FsofcContextId = u4FsofcContextId;
    pOfcIsSetFsofcIfEntry->bFsofcContextId = OSIX_TRUE;

    pOfcFsofcIfEntry->MibObject.i4FsofcIfIndex = i4FsofcIfIndex;
    pOfcIsSetFsofcIfEntry->bFsofcIfIndex = OSIX_TRUE;

    /* Assign the value */
    MEMCPY (pOfcFsofcIfEntry->MibObject.au1FsofcVlanEgressPorts,
            pTestValFsofcVlanEgressPorts->pu1_OctetList,
            pTestValFsofcVlanEgressPorts->i4_Length);
    pOfcFsofcIfEntry->MibObject.i4FsofcVlanEgressPortsLen =
        pTestValFsofcVlanEgressPorts->i4_Length;
    pOfcIsSetFsofcIfEntry->bFsofcVlanEgressPorts = OSIX_TRUE;

    if (OfcTestAllFsofcIfTable (pu4ErrorCode, pOfcFsofcIfEntry,
                                pOfcIsSetFsofcIfEntry) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (OFC_FSOFCIFTABLE_POOLID,
                            (UINT1 *) pOfcFsofcIfEntry);
        MemReleaseMemBlock (OFC_FSOFCIFTABLE_ISSET_POOLID,
                            (UINT1 *) pOfcIsSetFsofcIfEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (OFC_FSOFCIFTABLE_POOLID, (UINT1 *) pOfcFsofcIfEntry);
    MemReleaseMemBlock (OFC_FSOFCIFTABLE_ISSET_POOLID,
                        (UINT1 *) pOfcIsSetFsofcIfEntry);
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsofcVlanUntaggedPorts
 Input       :  The Indices
                FsofcContextId
                FsofcIfIndex

                The Object 
                testValFsofcVlanUntaggedPorts
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsofcVlanUntaggedPorts (UINT4 *pu4ErrorCode, UINT4 u4FsofcContextId,
                                 INT4 i4FsofcIfIndex,
                                 tSNMP_OCTET_STRING_TYPE *
                                 pTestValFsofcVlanUntaggedPorts)
{
    tOfcFsofcIfEntry   *pOfcFsofcIfEntry = NULL;
    tOfcIsSetFsofcIfEntry *pOfcIsSetFsofcIfEntry = NULL;

    pOfcFsofcIfEntry =
        (tOfcFsofcIfEntry *) MemAllocMemBlk (OFC_FSOFCIFTABLE_POOLID);

    if (pOfcFsofcIfEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pOfcIsSetFsofcIfEntry =
        (tOfcIsSetFsofcIfEntry *)
        MemAllocMemBlk (OFC_FSOFCIFTABLE_ISSET_POOLID);

    if (pOfcIsSetFsofcIfEntry == NULL)
    {
        MemReleaseMemBlock (OFC_FSOFCIFTABLE_POOLID,
                            (UINT1 *) pOfcFsofcIfEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pOfcFsofcIfEntry, 0, sizeof (tOfcFsofcIfEntry));
    MEMSET (pOfcIsSetFsofcIfEntry, 0, sizeof (tOfcIsSetFsofcIfEntry));

    /* Assign the index */
    pOfcFsofcIfEntry->MibObject.u4FsofcContextId = u4FsofcContextId;
    pOfcIsSetFsofcIfEntry->bFsofcContextId = OSIX_TRUE;

    pOfcFsofcIfEntry->MibObject.i4FsofcIfIndex = i4FsofcIfIndex;
    pOfcIsSetFsofcIfEntry->bFsofcIfIndex = OSIX_TRUE;

    /* Assign the value */
    MEMCPY (pOfcFsofcIfEntry->MibObject.au1FsofcVlanUntaggedPorts,
            pTestValFsofcVlanUntaggedPorts->pu1_OctetList,
            pTestValFsofcVlanUntaggedPorts->i4_Length);
    pOfcFsofcIfEntry->MibObject.i4FsofcVlanUntaggedPortsLen =
        pTestValFsofcVlanUntaggedPorts->i4_Length;
    pOfcIsSetFsofcIfEntry->bFsofcVlanUntaggedPorts = OSIX_TRUE;

    if (OfcTestAllFsofcIfTable (pu4ErrorCode, pOfcFsofcIfEntry,
                                pOfcIsSetFsofcIfEntry) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (OFC_FSOFCIFTABLE_POOLID,
                            (UINT1 *) pOfcFsofcIfEntry);
        MemReleaseMemBlock (OFC_FSOFCIFTABLE_ISSET_POOLID,
                            (UINT1 *) pOfcIsSetFsofcIfEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (OFC_FSOFCIFTABLE_POOLID, (UINT1 *) pOfcFsofcIfEntry);
    MemReleaseMemBlock (OFC_FSOFCIFTABLE_ISSET_POOLID,
                        (UINT1 *) pOfcIsSetFsofcIfEntry);
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsofcIfContextId
 Input       :  The Indices
                FsofcContextId
                FsofcIfIndex

                The Object 
                testValFsofcIfContextId
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsofcIfContextId (UINT4 *pu4ErrorCode, UINT4 u4FsofcContextId,
                           INT4 i4FsofcIfIndex, UINT4 u4TestValFsofcIfContextId)
{
    tOfcFsofcIfEntry   *pOfcFsofcIfEntry = NULL;
    tOfcIsSetFsofcIfEntry *pOfcIsSetFsofcIfEntry = NULL;

    pOfcFsofcIfEntry =
        (tOfcFsofcIfEntry *) MemAllocMemBlk (OFC_FSOFCIFTABLE_POOLID);

    if (pOfcFsofcIfEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pOfcIsSetFsofcIfEntry =
        (tOfcIsSetFsofcIfEntry *)
        MemAllocMemBlk (OFC_FSOFCIFTABLE_ISSET_POOLID);

    if (pOfcIsSetFsofcIfEntry == NULL)
    {
        MemReleaseMemBlock (OFC_FSOFCIFTABLE_POOLID,
                            (UINT1 *) pOfcFsofcIfEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pOfcFsofcIfEntry, 0, sizeof (tOfcFsofcIfEntry));
    MEMSET (pOfcIsSetFsofcIfEntry, 0, sizeof (tOfcIsSetFsofcIfEntry));

    /* Assign the index */
    pOfcFsofcIfEntry->MibObject.u4FsofcContextId = u4FsofcContextId;
    pOfcIsSetFsofcIfEntry->bFsofcContextId = OSIX_TRUE;

    pOfcFsofcIfEntry->MibObject.i4FsofcIfIndex = i4FsofcIfIndex;
    pOfcIsSetFsofcIfEntry->bFsofcIfIndex = OSIX_TRUE;

    /* Assign the value */
    pOfcFsofcIfEntry->MibObject.u4FsofcIfContextId = u4TestValFsofcIfContextId;
    pOfcIsSetFsofcIfEntry->bFsofcIfContextId = OSIX_TRUE;

    if (OfcTestAllFsofcIfTable (pu4ErrorCode, pOfcFsofcIfEntry,
                                pOfcIsSetFsofcIfEntry) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (OFC_FSOFCIFTABLE_POOLID,
                            (UINT1 *) pOfcFsofcIfEntry);
        MemReleaseMemBlock (OFC_FSOFCIFTABLE_ISSET_POOLID,
                            (UINT1 *) pOfcIsSetFsofcIfEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (OFC_FSOFCIFTABLE_POOLID, (UINT1 *) pOfcFsofcIfEntry);
    MemReleaseMemBlock (OFC_FSOFCIFTABLE_ISSET_POOLID,
                        (UINT1 *) pOfcIsSetFsofcIfEntry);
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhDepv2FsofcCfgTable
 Input       :  The Indices
                FsofcContextId
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsofcCfgTable (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                       tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsofcControllerConnTable
 Input       :  The Indices
                FsofcContextId
                FsofcControllerIpAddrType
                FsofcControllerIpAddress
                FsofcControllerConnAuxId
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsofcControllerConnTable (UINT4 *pu4ErrorCode,
                                  tSnmpIndexList * pSnmpIndexList,
                                  tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsofcIfTable
 Input       :  The Indices
                FsofcContextId
                FsofcIfIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsofcIfTable (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                      tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}
