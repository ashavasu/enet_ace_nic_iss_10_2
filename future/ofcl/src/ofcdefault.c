/********************************************************************
* Copyright (C) 2013 Aricent Inc . All Rights Reserved
*
* Id: ofcdefault.c 
*
* Description: This file contains the routines to initialize the
*              protocol structure for the module Ofc 
*********************************************************************/

#include "ofcinc.h"

/****************************************************************************
* Function    : OfcInitializeFsofcCfgTable
* Input       : pOfcFsofcCfgEntry
* Description : It Intialize the given structure
* Output      : None
* Returns     : OSIX_SUCCESS or OSIX_FAILURE
*****************************************************************************/

INT4
OfcInitializeFsofcCfgTable (tOfcFsofcCfgEntry * pOfcFsofcCfgEntry)
{
    if (pOfcFsofcCfgEntry == NULL)
    {
        return OSIX_FAILURE;
    }
    if ((OfcInitializeMibFsofcCfgTable (pOfcFsofcCfgEntry)) == OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
* Function    : OfcInitializeFsofcControllerConnTable
* Input       : pOfcFsofcControllerConnEntry
* Description : It Intialize the given structure
* Output      : None
* Returns     : OSIX_SUCCESS or OSIX_FAILURE
*****************************************************************************/

INT4
OfcInitializeFsofcControllerConnTable (tOfcFsofcControllerConnEntry *
                                       pOfcFsofcControllerConnEntry)
{
    if (pOfcFsofcControllerConnEntry == NULL)
    {
        return OSIX_FAILURE;
    }
    if ((OfcInitializeMibFsofcControllerConnTable
         (pOfcFsofcControllerConnEntry)) == OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    /* Memset to 0xffffffff to set all bits by default*/
    MEMSET(pOfcFsofcControllerConnEntry->au4PktInMask, 0xffffffff,sizeof(pOfcFsofcControllerConnEntry->au4PktInMask));
    MEMSET(pOfcFsofcControllerConnEntry->au4PortStatusMask, 0xffffffff,sizeof(pOfcFsofcControllerConnEntry->au4PktInMask));
    MEMSET(pOfcFsofcControllerConnEntry->au4FlowRemovedMask, 0xffffffff,sizeof(pOfcFsofcControllerConnEntry->au4PktInMask));
    return OSIX_SUCCESS;
}
