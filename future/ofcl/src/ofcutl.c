/********************************************************************
* Copyright (C) 2013 Aricent Inc . All Rights Reserved
*
* $Id: ofcutl.c,v 1.7 2014/03/15 14:29:47 siva Exp $
*
* Description: This file contains utility functions used by protocol Ofc
*********************************************************************/

#include "ofcinc.h"
#include "ofcsrc.h"
#include "ofcpkt.h"
#include "ofcflow.h"
#include "ofcintf.h"
#include "ofcvlan.h"
#include "ofcinband.h"
#include "ofchybrid.h"
#include "ofcsz.h"
#include "fssocket.h"
#include "ofcapi.h"
#ifdef NPAPI_WANTED
#include "ofcnp.h"
#endif

/****************************************************************************
 Function    :  OfcGetAllUtlFsofcCfgTable
 Input       :  pOfcGetFsofcCfgEntry
                pOfcdsFsofcCfgEntry
 Description :  This routine Gets the 
                value of needed.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
OfcGetAllUtlFsofcCfgTable (tOfcFsofcCfgEntry * pOfcGetFsofcCfgEntry,
                           tOfcFsofcCfgEntry * pOfcdsFsofcCfgEntry)
{
    pOfcGetFsofcCfgEntry->u4OfcClientState =
        pOfcdsFsofcCfgEntry->u4OfcClientState;
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  OfcGetAllUtlFsofcControllerConnTable
 Input       :  pOfcGetFsofcControllerConnEntry
                pOfcdsFsofcControllerConnEntry
 Description :  This routine Gets the 
                value of needed.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
OfcGetAllUtlFsofcControllerConnTable (tOfcFsofcControllerConnEntry *
                                      pOfcGetFsofcControllerConnEntry,
                                      tOfcFsofcControllerConnEntry *
                                      pOfcdsFsofcControllerConnEntry)
{
    pOfcGetFsofcControllerConnEntry->i4SocketDesc =
        pOfcdsFsofcControllerConnEntry->i4SocketDesc;
    pOfcGetFsofcControllerConnEntry->i4ConnRetryCount =
        pOfcdsFsofcControllerConnEntry->i4ConnRetryCount;
    pOfcGetFsofcControllerConnEntry->u4RxXid =
        pOfcdsFsofcControllerConnEntry->u4RxXid;
    pOfcGetFsofcControllerConnEntry->u4TxXid =
        pOfcdsFsofcControllerConnEntry->u4TxXid;
    pOfcGetFsofcControllerConnEntry->u4RxLength =
        pOfcdsFsofcControllerConnEntry->u4RxLength;
    pOfcGetFsofcControllerConnEntry->u4TxDataLength =
        pOfcdsFsofcControllerConnEntry->u4TxDataLength;
    pOfcGetFsofcControllerConnEntry->i4SocketDesc =
        pOfcdsFsofcControllerConnEntry->i4SocketDesc;
    pOfcGetFsofcControllerConnEntry->i4SocketDesc =
        pOfcdsFsofcControllerConnEntry->i4SocketDesc;

    MEMCPY (&pOfcGetFsofcControllerConnEntry->au4PktInMask,
            &pOfcdsFsofcControllerConnEntry->au4PktInMask,
            sizeof (pOfcdsFsofcControllerConnEntry->au4PktInMask));
    MEMCPY (&pOfcGetFsofcControllerConnEntry->au4PortStatusMask,
            &pOfcdsFsofcControllerConnEntry->au4PortStatusMask,
            sizeof (pOfcdsFsofcControllerConnEntry->au4PortStatusMask));
    MEMCPY (&pOfcGetFsofcControllerConnEntry->au4FlowRemovedMask,
            &pOfcdsFsofcControllerConnEntry->au4FlowRemovedMask,
            sizeof (pOfcdsFsofcControllerConnEntry->au4FlowRemovedMask));

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  OfcGetAllUtlFsofcIfTable
 Input       :  pOfcGetFsofcIfEntry
                pOfcdsFsofcIfEntry
 Description :  This routine Gets the 
                value of needed.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
OfcGetAllUtlFsofcIfTable (tOfcFsofcIfEntry * pOfcGetFsofcIfEntry,
                          tOfcFsofcIfEntry * pOfcdsFsofcIfEntry)
{
    pOfcGetFsofcIfEntry->u4Config = pOfcdsFsofcIfEntry->u4Config;
    pOfcGetFsofcIfEntry->u4VlanId = pOfcdsFsofcIfEntry->u4VlanId;
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  OfcGetAllUtlFsofcFlowTable
 Input       :  pOfcGetFsofcFlowEntry
                pOfcdsFsofcFlowEntry
 Description :  This routine Gets the 
                value of needed.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
OfcGetAllUtlFsofcFlowTable (tOfcFsofcFlowEntry * pOfcGetFsofcFlowEntry,
                            tOfcFsofcFlowEntry * pOfcdsFsofcFlowEntry)
{
    UNUSED_PARAM (pOfcGetFsofcFlowEntry);
    UNUSED_PARAM (pOfcdsFsofcFlowEntry);
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  OfcGetAllUtlFsofcGroupTable
 Input       :  pOfcGetFsofcGroupEntry
                pOfcdsFsofcGroupEntry
 Description :  This routine Gets the 
                value of needed.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
OfcGetAllUtlFsofcGroupTable (tOfcFsofcGroupEntry * pOfcGetFsofcGroupEntry,
                             tOfcFsofcGroupEntry * pOfcdsFsofcGroupEntry)
{
    UNUSED_PARAM (pOfcGetFsofcGroupEntry);
    UNUSED_PARAM (pOfcdsFsofcGroupEntry);
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  OfcGetAllUtlFsofcMeterTable
 Input       :  pOfcGetFsofcMeterEntry
                pOfcdsFsofcMeterEntry
 Description :  This routine Gets the 
                value of needed.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
OfcGetAllUtlFsofcMeterTable (tOfcFsofcMeterEntry * pOfcGetFsofcMeterEntry,
                             tOfcFsofcMeterEntry * pOfcdsFsofcMeterEntry)
{
    UNUSED_PARAM (pOfcGetFsofcMeterEntry);
    UNUSED_PARAM (pOfcdsFsofcMeterEntry);
    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  OfcUtilUpdateFsofcCfgTable
 * Input       :   pOfcOldFsofcCfgEntry
                   pOfcFsofcCfgEntry
                   pOfcIsSetFsofcCfgEntry
 * Descritpion :  This Routine checks set value 
                  with that of the value in database
                  and do the necessary protocol operation
 * Output      :  None
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
OfcUtilUpdateFsofcCfgTable (tOfcFsofcCfgEntry * pOfcOldFsofcCfgEntry,
                            tOfcFsofcCfgEntry * pOfcFsofcCfgEntry,
                            tOfcIsSetFsofcCfgEntry * pOfcIsSetFsofcCfgEntry)
{
    tOfcFsofcCfgEntry  *pOfcDfltFsofcCfgEntry = NULL;
#ifdef NPAPI_WANTED
    tOfcHwInfo          OfcHwInfo;
    INT4                i4RetValue = OFC_ZERO;
    MEMSET (&OfcHwInfo, OFC_ZERO, sizeof (tOfcHwInfo));
#endif

    if (pOfcIsSetFsofcCfgEntry->bFsofcSupportedVersion != OSIX_FALSE)
    {
        if (pOfcFsofcCfgEntry->MibObject.i4FsofcSupportedVersion == OFC_ZERO)
        {
            pOfcDfltFsofcCfgEntry = (tOfcFsofcCfgEntry *)
                RBTreeGetFirst (gOfcGlobals.OfcGlbMib.FsofcCfgTable);
            if (pOfcDfltFsofcCfgEntry == NULL)
            {
                OFC_TRC_FUNC ((OFC_UTIL_TRC,
                               "\r\nFUNC:OfcUtilUpdateFsofcCfgTable: Get Cfg Entry FAILURE"));
                return OSIX_FAILURE;
            }

            pOfcFsofcCfgEntry->MibObject.i4FsofcSupportedVersion =
                pOfcDfltFsofcCfgEntry->MibObject.i4FsofcSupportedVersion;
        }
    }

    if (pOfcIsSetFsofcCfgEntry->bFsofcTraceEnable != OSIX_FALSE)
    {
        gOfcGlobals.u4OfcTrc = gOfcGlobals.u4OfcTrc |
            pOfcFsofcCfgEntry->MibObject.u4FsofcTraceEnable;

        if (pOfcFsofcCfgEntry->MibObject.u4FsofcTraceEnable == CLI_OFC_NO_DEBUG)
        {
            gOfcGlobals.u4OfcTrc = CLI_OFC_NO_DEBUG;
        }
    }

    if (pOfcIsSetFsofcCfgEntry->bFsofcModuleStatus != OSIX_FALSE)
    {
        if (pOfcFsofcCfgEntry->MibObject.i4FsofcModuleStatus != OPENFLOW_NONE)
        {
            pOfcFsofcCfgEntry->MibObject.i4FsofcSwitchModeOnConnFailure =
                OPENFLOW_FAIL_STANDALONE;
            OfcHybridPortCreate (pOfcFsofcCfgEntry->MibObject.u4FsofcContextId,
                                 pOfcFsofcCfgEntry->MibObject.
                                 i4FsofcModuleStatus,
                                 pOfcFsofcCfgEntry->MibObject.i4FsofcHybrid);
        }
        else
        {
            pOfcFsofcCfgEntry->MibObject.i4FsofcSwitchModeOnConnFailure =
                OPENFLOW_FAIL_SECURE;
            OfcHybridPortDelete (pOfcFsofcCfgEntry->MibObject.u4FsofcContextId);
        }
    }

    if ((pOfcIsSetFsofcCfgEntry->bFsofcControlPktBuffering
         != OSIX_FALSE) ||
        (pOfcIsSetFsofcCfgEntry->bFsofcIpReassembleStatus
         != OSIX_FALSE) ||
        (pOfcIsSetFsofcCfgEntry->bFsofcPortStpStatus != OSIX_FALSE))
    {
        OFC_TRC_FUNC ((OFC_UTIL_TRC,
                       "FUNC:OfcUtilUpdateFsofcCfgTable: Invoking NPAPI for Update\n"));
#ifdef NPAPI_WANTED
        OfcHwInfo.OfcCmd = OFC_NP_CONFIG_PARAM;
        if (pOfcIsSetFsofcCfgEntry->bFsofcControlPktBuffering != OSIX_FALSE)
        {
            OFC_TRC_FUNC ((OFC_UTIL_TRC,
                           "\r\nFUNC:OfcUtilUpdateFsofcCfgTable: NPAPI Update for PktBuf"));
            OfcHwInfo.OfcHwEntry.OfcHwCfgInfo.OfcCfgParam = OFC_PKT_BUFFERING;
            OfcHwInfo.OfcHwEntry.OfcHwCfgInfo.u4ParamValue =
                (UINT4) pOfcFsofcCfgEntry->MibObject.i4FsofcControlPktBuffering;
        }

        if (pOfcIsSetFsofcCfgEntry->bFsofcIpReassembleStatus != OSIX_FALSE)
        {
            OFC_TRC_FUNC ((OFC_UTIL_TRC,
                           "\r\nFUNC:OfcUtilUpdateFsofcCfgTable: NPAPI Update for IP-ReAsem"));
            OfcHwInfo.OfcHwEntry.OfcHwCfgInfo.OfcCfgParam = OFC_IP_REASSEMBLE;
            OfcHwInfo.OfcHwEntry.OfcHwCfgInfo.u4ParamValue =
                (UINT4) pOfcFsofcCfgEntry->MibObject.i4FsofcIpReassembleStatus;
        }

        if (pOfcIsSetFsofcCfgEntry->bFsofcPortStpStatus != OSIX_FALSE)
        {
            OFC_TRC_FUNC ((OFC_UTIL_TRC,
                           "\r\nFUNC:OfcUtilUpdateFsofcCfgTable: NPAPI Update for STP-Status"));
            OfcHwInfo.OfcHwEntry.OfcHwCfgInfo.OfcCfgParam = OFC_STP_STATUS;
            OfcHwInfo.OfcHwEntry.OfcHwCfgInfo.u4ParamValue =
                (UINT4) pOfcFsofcCfgEntry->MibObject.i4FsofcPortStpStatus;
        }
        i4RetValue = OfcFsNpHwConfigOfcInfo (&OfcHwInfo);
        if (i4RetValue != FNP_SUCCESS)
        {
            OFC_TRC_FUNC ((OFC_UTIL_TRC,
                           "\r\nFUNC:OfcUtilUpdateFsofcCfgTable: NPAPI FAILURE"));
        }
#endif
    }

    if (pOfcIsSetFsofcCfgEntry->bFsofcSwitchEntryStatus != OSIX_FALSE)
    {
        /*
         * Get the Default Cfg Entry 
         */
        pOfcDfltFsofcCfgEntry = (tOfcFsofcCfgEntry *)
            RBTreeGetFirst (gOfcGlobals.OfcGlbMib.FsofcCfgTable);
        if (pOfcDfltFsofcCfgEntry == NULL)
        {
            OFC_TRC_FUNC ((OFC_UTIL_TRC,
                           "\r\nFUNC:OfcUtilUpdateFsofcCfgTable: Get Cfg Entry FAILURE"));
            return OSIX_FAILURE;
        }

        if (pOfcFsofcCfgEntry->MibObject.i4FsofcSwitchEntryStatus == ACTIVE)
        {
            /*
             * Initialize the Cfg Entry Flags, Max Values and other Data 
             */
            if (OfcMainCfgContextInit (pOfcFsofcCfgEntry,
                                       (UINT4) pOfcDfltFsofcCfgEntry->MibObject.
                                       i4FsofcSupportedVersion) ==
                (INT4) OSIX_FAILURE)
            {
                OFC_TRC_FUNC ((OFC_UTIL_TRC,
                               "\r\nFUNC:OfcUtilUpdateFsofcCfgTable: Cfg Entry Init Failure"));
                return OSIX_FAILURE;
            }

            /*
             * Initialize the Cfg Entry Memory Pools 
             */
            if (pOfcDfltFsofcCfgEntry->MibObject.i4FsofcSupportedVersion ==
                OFC_VERSION_V100)
            {
                /*
                 * Create Hash Table and Buffer pools for data structures  - V1.0.0
                 */
                if (Ofc100MainMemInit (pOfcFsofcCfgEntry) == OSIX_FAILURE)
                {
                    OFC_TRC ((OFC_UTIL_TRC,
                              "\r\nFUNC:OfcUtilUpdateFsofcCfgTable: "
                              "Memory Pool Creation Failed - V1.0.0\n"));
                    return OSIX_FAILURE;
                }
            }
            else
            {
                /*
                 * Create Hash Table and Buffer pools for data structures  - V1.3.1
                 */
                if (Ofc131MainMemInit (pOfcFsofcCfgEntry) == OSIX_FAILURE)
                {
                    OFC_TRC ((OFC_MAIN_TRC,
                              "\r\nFUNC:OfcUtilUpdateFsofcCfgTable: "
                              "Memory Pool Creation Failed - V1.3.1\n"));
                    return OSIX_FAILURE;
                }
            }
        }
        else if (pOfcFsofcCfgEntry->MibObject.i4FsofcSwitchEntryStatus ==
                 DESTROY)
        {
            /*
             * De-Initialize the Cfg Entry Memory Pools 
             */
            if (pOfcDfltFsofcCfgEntry->MibObject.i4FsofcSupportedVersion ==
                OFC_VERSION_V100)
            {
                /*
                 * Delete Hash Table and Buffer pools and related Data  - V1.0.0
                 */
                Ofc100MainMemClear (pOfcFsofcCfgEntry);
            }
            else
            {
                /*
                 * Delete Hash Table and Buffer pools and related Data  - V1.3.1
                 */
                Ofc131MainMemClear (pOfcFsofcCfgEntry);
            }
        }
    }

    UNUSED_PARAM (pOfcOldFsofcCfgEntry);
    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  OfcUtilUpdateFsofcControllerConnTable
 * Input       :   pOfcOldFsofcControllerConnEntry
                   pOfcFsofcControllerConnEntry
                   pOfcIsSetFsofcControllerConnEntry
 * Descritpion :  This Routine checks set value 
                  with that of the value in database
                  and do the necessary protocol operation
 * Output      :  None
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
OfcUtilUpdateFsofcControllerConnTable (tOfcFsofcControllerConnEntry *
                                       pOfcOldFsofcControllerConnEntry,
                                       tOfcFsofcControllerConnEntry *
                                       pOfcFsofcControllerConnEntry,
                                       tOfcIsSetFsofcControllerConnEntry *
                                       pOfcIsSetFsofcControllerConnEntry)
{
    tOfcFsofcCfgEntry  *pOfcFsofcCfgEntry = NULL;
    INT4                i4RetValue = OFC_ZERO;

    /*
     * Dynamic controller entries will be created here 
     */
    if (pOfcIsSetFsofcControllerConnEntry->bFsofcControllerConnEntryStatus
        != OSIX_FALSE)
    {
        pOfcFsofcCfgEntry =
            OfcGetFsofcCfgEntry (pOfcFsofcControllerConnEntry->MibObject.
                                 u4FsofcContextId);
        if (pOfcFsofcCfgEntry == NULL)
        {
            OFC_TRC_FUNC ((OFC_UTIL_TRC,
                           "\r\nFUNC:OfcUtilUpdateFsofcControllerConnTable: "
                           "Get CFG Entry FAILURE\n"));
            return OSIX_FAILURE;
        }

        if (pOfcFsofcControllerConnEntry->MibObject.
            i4FsofcControllerConnEntryStatus == ACTIVE)
        {
            if ((pOfcFsofcControllerConnEntry->MibObject.
                 i4FsofcControllerConnAuxId == OFC_ZERO))
            {
                pOfcFsofcCfgEntry->u4CurrConnEntries++;
            }
            OfcCreateController (pOfcFsofcControllerConnEntry);
        }
        else if (pOfcFsofcControllerConnEntry->MibObject.
                 i4FsofcControllerConnEntryStatus == DESTROY)
        {
            if ((pOfcFsofcControllerConnEntry->MibObject.
                 i4FsofcControllerConnAuxId == OFC_ZERO))
            {
                pOfcFsofcCfgEntry->u4CurrConnEntries--;
            }
            if (pOfcFsofcControllerConnEntry->ConnRetryTimer.tmrNode.
                u2Flags & OFC_TMR_RUNNING)
            {
                OfcTmrStopTmr (&(pOfcFsofcControllerConnEntry->ConnRetryTimer));
            }
            if (pOfcFsofcControllerConnEntry->EchoReqTimer.tmrNode.
                u2Flags & OFC_TMR_RUNNING)
            {
                OfcTmrStopTmr (&(pOfcFsofcControllerConnEntry->EchoReqTimer));
            }
            if (pOfcFsofcControllerConnEntry->EchoInterTimer.tmrNode.
                u2Flags & OFC_TMR_RUNNING)
            {
                OfcTmrStopTmr (&(pOfcFsofcControllerConnEntry->EchoInterTimer));
            }
            SelRemoveFd (pOfcFsofcControllerConnEntry->i4SocketDesc);
            close (pOfcFsofcControllerConnEntry->i4SocketDesc);
            pOfcFsofcControllerConnEntry->i4SocketDesc = -1;
            pOfcFsofcControllerConnEntry->MibObject.i4FsofcControllerConnState =
                OFC_NOT_CONNECTED;
            OfcUpdateCfgConnState (pOfcFsofcControllerConnEntry);

            /*
             * Delete all the flows if the connection failure mode is failstand alone
             * and Cfg Entry is in not connected state
             */

            if ((pOfcFsofcCfgEntry->MibObject.i4FsofcSwitchModeOnConnFailure ==
                 OFC_SWITCHMODE_FAIL_STANDALONE) &&
                (pOfcFsofcCfgEntry->u4OfcClientState == OFC_NOT_CONNECTED))
            {
                Ofc131FlowDeleteAllFlows (pOfcFsofcCfgEntry);
                /*
                 * Make the openflow ports visible to the ISS context
                 */
                i4RetValue =
                    OfcHandleOpenflowPorts (pOfcFsofcCfgEntry->MibObject.
                                            u4FsofcContextId,
                                            CFA_SUBTYPE_SISP_INTERFACE);

                if (i4RetValue == OFC_FAILURE)
                {
                    return OSIX_FAILURE;
                }
            }

            if (pOfcFsofcControllerConnEntry->pRcvPkt != NULL)
            {
                MemReleaseMemBlock (OFC_BUFFER_POOLID,
                                    (UINT1 *) pOfcFsofcControllerConnEntry->
                                    pRcvPkt);
            }

            /*
             *  If InBand Controller,
             *  Delete the Controller Entry from InBand Database
             */
            if (pOfcFsofcControllerConnEntry->MibObject.
                i4FsofcControllerConnBand == OPENFLOW_CONNBAND_INB)
            {
                OfcInBandDelCntrlInDataBase (pOfcFsofcControllerConnEntry);
            }
        }
    }

    UNUSED_PARAM (pOfcOldFsofcControllerConnEntry);
    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  OfclUtilUpdateFsofcIfTable
 * Input       :   pOfclOldFsofcIfEntry
                   pOfclFsofcIfEntry
                   pOfclIsSetFsofcIfEntry
 * Descritpion :  This Routine checks set value
                  with that of the value in database
                  and do the necessary protocol operation
 * Output      :  None
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
OfcUtilUpdateFsofcIfTable (tOfcFsofcIfEntry * pOfcOldFsofcIfEntry,
                           tOfcFsofcIfEntry * pOfcFsofcIfEntry,
                           tOfcIsSetFsofcIfEntry * pOfcIsSetFsofcIfEntry)
{
    tSNMP_OCTET_STRING_TYPE EgressPorts;
    UINT1              *pau1EgressPortList = NULL;

    UNUSED_PARAM (pOfcOldFsofcIfEntry);
    MEMSET (&EgressPorts, OFC_ZERO, sizeof (tSNMP_OCTET_STRING_TYPE));

    pau1EgressPortList = UtilPlstAllocLocalPortList (OFC_VLAN_PORTS_LEN);
    if (pau1EgressPortList == NULL)
    {
        OFC_TRC ((OFC_UTIL_TRC, "\r\nOfcUtilUpdateFsofcIfTable: "
                  "Error in allocating memory for pau1OldEgressPortList\n"));

        return OSIX_FAILURE;
    }

    MEMSET (pau1EgressPortList, 0, OFC_VLAN_PORTS_LEN);
    EgressPorts.i4_Length = OFC_VLAN_PORTS_LEN;
    EgressPorts.pu1_OctetList = pau1EgressPortList;

    if (pOfcIsSetFsofcIfEntry->bFsofcVlanEgressPorts != OSIX_FALSE)
    {
        nmhGetFsofcVlanEgressPorts (OFC_DEFAULT_CONTEXT,
                                    pOfcFsofcIfEntry->MibObject.i4FsofcIfIndex,
                                    &EgressPorts);

        /* 
         * Check if the member port list is not  empty and update oper status 
         */
        if (FsUtilBitListIsAllZeros (EgressPorts.pu1_OctetList,
                                     sizeof (tPortList)) != OSIX_TRUE)
        {
            if (pOfcFsofcIfEntry->MibObject.i4FsofcIfOperStatus ==
                OFC_VLAN_IF_DOWN)
            {
                pOfcFsofcIfEntry->MibObject.i4FsofcIfOperStatus =
                    OFC_VLAN_IF_UP;
            }
        }
    }

    if (pOfcIsSetFsofcIfEntry->bFsofcIfContextId != OSIX_FALSE)
    {
        if ((pOfcOldFsofcIfEntry->MibObject.u4FsofcIfContextId ==
             OFC_INVALID_CONTEXT) &&
            (pOfcFsofcIfEntry->MibObject.u4FsofcIfContextId !=
             OFC_INVALID_CONTEXT))
        {
            if (OpenflowPortMapUnmap ((UINT4) pOfcFsofcIfEntry->MibObject.
                                      i4FsofcIfIndex,
                                      pOfcFsofcIfEntry->MibObject.
                                      u4FsofcIfContextId,
                                      OSIX_TRUE) != OSIX_SUCCESS)
            {
                OFC_TRC ((OFC_UTIL_TRC, "\r\nOfcUtilUpdateFsofcIfTable: "
                          "Error in Mapping the Interface:%d to Context:%d\n",
                          pOfcFsofcIfEntry->MibObject.i4FsofcIfIndex,
                          pOfcFsofcIfEntry->MibObject.u4FsofcIfContextId));
                UtilPlstReleaseLocalPortList (pau1EgressPortList);
                return OSIX_FAILURE;
            }
        }
        else if ((pOfcOldFsofcIfEntry->MibObject.u4FsofcIfContextId !=
                  OFC_INVALID_CONTEXT) &&
                 (pOfcFsofcIfEntry->MibObject.u4FsofcIfContextId ==
                  OFC_INVALID_CONTEXT))
        {
            if (OpenflowPortMapUnmap ((UINT4) pOfcOldFsofcIfEntry->MibObject.
                                      i4FsofcIfIndex,
                                      pOfcOldFsofcIfEntry->MibObject.
                                      u4FsofcIfContextId,
                                      OSIX_FALSE) != OSIX_SUCCESS)
            {
                OFC_TRC ((OFC_UTIL_TRC, "\r\nOfcUtilUpdateFsofcIfTable: Error"
                          "in UnMapping the Interface:%d from Context:%d\n",
                          pOfcOldFsofcIfEntry->MibObject.i4FsofcIfIndex,
                          pOfcOldFsofcIfEntry->MibObject.u4FsofcIfContextId));
                UtilPlstReleaseLocalPortList (pau1EgressPortList);
                return CLI_FAILURE;
            }
        }
        else
        {
            OFC_TRC ((OFC_UTIL_TRC, "\r\nOfcUtilUpdateFsofcIfTable: "
                      "Invalid Code path, returning FAILURE\n"));
            UtilPlstReleaseLocalPortList (pau1EgressPortList);
            return OSIX_FAILURE;
        }
    }

    UtilPlstReleaseLocalPortList (pau1EgressPortList);
    return OSIX_SUCCESS;
}

/****************************************************************************
* Function    : OfcUtlConversionForFsofcControllerIpAddress
* Input       : pFsofcControllerIpAddress,
*               pOfcFsofcControllerConnEntry,
* Output      : None
* Returns     : NONE
*****************************************************************************/
void
OfcUtlConversionForFsofcControllerIpAddress (CHR1 * pFsofcControllerIpAddress,
                                             tOfcFsofcControllerConnEntry *
                                             pOfcFsofcControllerConnEntry)
{
    UNUSED_PARAM (pFsofcControllerIpAddress);
    UNUSED_PARAM (pOfcFsofcControllerConnEntry);
}
