/********************************************************************
* Copyright (C) 2013 Aricent Inc . All Rights Reserved
*
* $Id: ofcutlg.c,v 1.4 2014/04/30 10:10:25 siva Exp $
*
* Description: This file contains utility functions used by protocol Ofc
*********************************************************************/

#include "ofcinc.h"
#include "ofcsz.h"

/****************************************************************************
 Function    :  OfcUtlCreateRBTree
 Input       :  None
 Description :  This function creates all 
                the RBTree required.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
PUBLIC INT4
OfcUtlCreateRBTree ()
{

    if (OfcFsofcCfgTableCreate () == OSIX_FAILURE)

    {
        return (OSIX_FAILURE);
    }

    if (OfcFsofcControllerConnTableCreate () == OSIX_FAILURE)

    {
        return (OSIX_FAILURE);
    }

    if (OfcFsofcIfTableCreate () == OSIX_FAILURE)

    {
        return (OSIX_FAILURE);
    }

    if (OfcFsofcFlowTableCreate () == OSIX_FAILURE)

    {
        return (OSIX_FAILURE);
    }

    if (OfcFsofcGroupTableCreate () == OSIX_FAILURE)

    {
        return (OSIX_FAILURE);
    }

    if (OfcFsofcMeterTableCreate () == OSIX_FAILURE)

    {
        return (OSIX_FAILURE);
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  OfcFsofcCfgTableCreate
 Input       :  None
 Description :  This function creates the FsofcCfgTable
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

PUBLIC INT4
OfcFsofcCfgTableCreate ()
{
    UINT4               u4RBNodeOffset = 0;
    u4RBNodeOffset =
        FSAP_OFFSETOF (tOfcFsofcCfgEntry, MibObject.FsofcCfgTableNode);

    if ((gOfcGlobals.OfcGlbMib.FsofcCfgTable =
         RBTreeCreateEmbedded (u4RBNodeOffset, FsofcCfgTableRBCmp)) == NULL)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  OfcFsofcControllerConnTableCreate
 Input       :  None
 Description :  This function creates the FsofcControllerConnTable
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

PUBLIC INT4
OfcFsofcControllerConnTableCreate ()
{
    UINT4               u4RBNodeOffset = 0;
    u4RBNodeOffset =
        FSAP_OFFSETOF (tOfcFsofcControllerConnEntry,
                       MibObject.FsofcControllerConnTableNode);

    if ((gOfcGlobals.OfcGlbMib.FsofcControllerConnTable =
         RBTreeCreateEmbedded (u4RBNodeOffset,
                               FsofcControllerConnTableRBCmp)) == NULL)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  OfcFsofcIfTableCreate
 Input       :  None
 Description :  This function creates the FsofcIfTable
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

PUBLIC INT4
OfcFsofcIfTableCreate ()
{
    UINT4               u4RBNodeOffset = 0;
    u4RBNodeOffset =
        FSAP_OFFSETOF (tOfcFsofcIfEntry, MibObject.FsofcIfTableNode);

    if ((gOfcGlobals.OfcGlbMib.FsofcIfTable =
         RBTreeCreateEmbedded (u4RBNodeOffset, FsofcIfTableRBCmp)) == NULL)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  OfcFsofcFlowTableCreate
 Input       :  None
 Description :  This function creates the FsofcFlowTable
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

PUBLIC INT4
OfcFsofcFlowTableCreate ()
{
    UINT4               u4RBNodeOffset = 0;
    u4RBNodeOffset =
        FSAP_OFFSETOF (tOfcFsofcFlowEntry, MibObject.FsofcFlowTableNode);

    if ((gOfcGlobals.OfcGlbMib.FsofcFlowTable =
         RBTreeCreateEmbedded (u4RBNodeOffset, FsofcFlowTableRBCmp)) == NULL)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  OfcFsofcGroupTableCreate
 Input       :  None
 Description :  This function creates the FsofcGroupTable
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

PUBLIC INT4
OfcFsofcGroupTableCreate ()
{
    UINT4               u4RBNodeOffset = 0;
    u4RBNodeOffset =
        FSAP_OFFSETOF (tOfcFsofcGroupEntry, MibObject.FsofcGroupTableNode);

    if ((gOfcGlobals.OfcGlbMib.FsofcGroupTable =
         RBTreeCreateEmbedded (u4RBNodeOffset, FsofcGroupTableRBCmp)) == NULL)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  OfcFsofcMeterTableCreate
 Input       :  None
 Description :  This function creates the FsofcMeterTable
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

PUBLIC INT4
OfcFsofcMeterTableCreate ()
{
    UINT4               u4RBNodeOffset = 0;
    u4RBNodeOffset =
        FSAP_OFFSETOF (tOfcFsofcMeterEntry, MibObject.FsofcMeterTableNode);

    if ((gOfcGlobals.OfcGlbMib.FsofcMeterTable =
         RBTreeCreateEmbedded (u4RBNodeOffset, FsofcMeterTableRBCmp)) == NULL)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  OfcFsofcFTableCreate
 Input       :  None
 Description :  This function creates the Flow Table
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

PUBLIC INT4
OfcFsofcFTableCreate (tOfcFsofcCfgEntry * pOfcFsofcCfgEntry)
{
    UINT4               u4RBNodeOffset = 0;
    u4RBNodeOffset = FSAP_OFFSETOF (tOfcFsofcFlowTable, RBNode);

    if ((pOfcFsofcCfgEntry->OfcFlowtable =
         RBTreeCreateEmbedded (u4RBNodeOffset, FsofcFTableRBCmp)) == NULL)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  FsofcCfgTableRBCmp
 Input       :  pRBElem1 - Pointer to the node1 for comparision
                pRBElem2 - Pointer to the node2 for comparision
 Description :  RBTree Compare function for
                FsofcCfgTable table
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  1,-1 0
****************************************************************************/

PUBLIC INT4
FsofcCfgTableRBCmp (tRBElem * pRBElem1, tRBElem * pRBElem2)
{

    tOfcFsofcCfgEntry  *pFsofcCfgEntry1 = (tOfcFsofcCfgEntry *) pRBElem1;
    tOfcFsofcCfgEntry  *pFsofcCfgEntry2 = (tOfcFsofcCfgEntry *) pRBElem2;
    INT4                i4MaxLength = 0;

    if (pFsofcCfgEntry1->MibObject.u4FsofcContextId >
        pFsofcCfgEntry2->MibObject.u4FsofcContextId)
    {
        return 1;
    }
    else if (pFsofcCfgEntry1->MibObject.u4FsofcContextId <
             pFsofcCfgEntry2->MibObject.u4FsofcContextId)
    {
        return -1;
    }

    UNUSED_PARAM (i4MaxLength);

    return 0;
}

/****************************************************************************
 Function    :  FsofcControllerConnTableRBCmp
 Input       :  pRBElem1 - Pointer to the node1 for comparision
                pRBElem2 - Pointer to the node2 for comparision
 Description :  RBTree Compare function for
                FsofcControllerConnTable table
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  1,-1 0
****************************************************************************/

PUBLIC INT4
FsofcControllerConnTableRBCmp (tRBElem * pRBElem1, tRBElem * pRBElem2)
{

    tOfcFsofcControllerConnEntry *pFsofcControllerConnEntry1 =
        (tOfcFsofcControllerConnEntry *) pRBElem1;
    tOfcFsofcControllerConnEntry *pFsofcControllerConnEntry2 =
        (tOfcFsofcControllerConnEntry *) pRBElem2;
    INT4                i4MaxLength = 0;

    if (pFsofcControllerConnEntry1->MibObject.i4FsofcControllerIpAddrType >
        pFsofcControllerConnEntry2->MibObject.i4FsofcControllerIpAddrType)
    {
        return 1;
    }
    else if (pFsofcControllerConnEntry1->MibObject.i4FsofcControllerIpAddrType <
             pFsofcControllerConnEntry2->MibObject.i4FsofcControllerIpAddrType)
    {
        return -1;
    }

    i4MaxLength = 0;
    if (pFsofcControllerConnEntry1->MibObject.i4FsofcControllerIpAddressLen >
        pFsofcControllerConnEntry2->MibObject.i4FsofcControllerIpAddressLen)
    {
        i4MaxLength =
            pFsofcControllerConnEntry1->MibObject.i4FsofcControllerIpAddressLen;
    }
    else
    {
        i4MaxLength =
            pFsofcControllerConnEntry2->MibObject.i4FsofcControllerIpAddressLen;
    }

    if (MEMCMP
        (pFsofcControllerConnEntry1->MibObject.au1FsofcControllerIpAddress,
         pFsofcControllerConnEntry2->MibObject.au1FsofcControllerIpAddress,
         i4MaxLength) > 0)
    {
        return 1;
    }
    else if (MEMCMP
             (pFsofcControllerConnEntry1->MibObject.au1FsofcControllerIpAddress,
              pFsofcControllerConnEntry2->MibObject.au1FsofcControllerIpAddress,
              i4MaxLength) < 0)
    {
        return -1;
    }

    if (pFsofcControllerConnEntry1->MibObject.i4FsofcControllerIpAddressLen >
        pFsofcControllerConnEntry2->MibObject.i4FsofcControllerIpAddressLen)
    {
        return 1;
    }
    else if (pFsofcControllerConnEntry1->
             MibObject.i4FsofcControllerIpAddressLen <
             pFsofcControllerConnEntry2->
             MibObject.i4FsofcControllerIpAddressLen)
    {
        return -1;
    }

    if (pFsofcControllerConnEntry1->MibObject.i4FsofcControllerConnAuxId >
        pFsofcControllerConnEntry2->MibObject.i4FsofcControllerConnAuxId)
    {
        return 1;
    }
    else if (pFsofcControllerConnEntry1->MibObject.i4FsofcControllerConnAuxId <
             pFsofcControllerConnEntry2->MibObject.i4FsofcControllerConnAuxId)
    {
        return -1;
    }

    if (pFsofcControllerConnEntry1->MibObject.u4FsofcContextId >
        pFsofcControllerConnEntry2->MibObject.u4FsofcContextId)
    {
        return 1;
    }
    else if (pFsofcControllerConnEntry1->MibObject.u4FsofcContextId <
             pFsofcControllerConnEntry2->MibObject.u4FsofcContextId)
    {
        return -1;
    }

    return 0;
}

/****************************************************************************
 Function    :  FsofcIfTableRBCmp
 Input       :  pRBElem1 - Pointer to the node1 for comparision
                pRBElem2 - Pointer to the node2 for comparision
 Description :  RBTree Compare function for
                FsofcIfTable table
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  1,-1 0
****************************************************************************/

PUBLIC INT4
FsofcIfTableRBCmp (tRBElem * pRBElem1, tRBElem * pRBElem2)
{

    tOfcFsofcIfEntry   *pFsofcIfEntry1 = (tOfcFsofcIfEntry *) pRBElem1;
    tOfcFsofcIfEntry   *pFsofcIfEntry2 = (tOfcFsofcIfEntry *) pRBElem2;
    INT4                i4MaxLength = 0;

    if (pFsofcIfEntry1->MibObject.i4FsofcIfIndex >
        pFsofcIfEntry2->MibObject.i4FsofcIfIndex)
    {
        return 1;
    }
    else if (pFsofcIfEntry1->MibObject.i4FsofcIfIndex <
             pFsofcIfEntry2->MibObject.i4FsofcIfIndex)
    {
        return -1;
    }

    if (pFsofcIfEntry1->MibObject.u4FsofcContextId >
        pFsofcIfEntry2->MibObject.u4FsofcContextId)
    {
        return 1;
    }
    else if (pFsofcIfEntry1->MibObject.u4FsofcContextId <
             pFsofcIfEntry2->MibObject.u4FsofcContextId)
    {
        return -1;
    }

    UNUSED_PARAM (i4MaxLength);

    return 0;
}

/****************************************************************************
 Function    :  FsofcFlowTableRBCmp
 Input       :  pRBElem1 - Pointer to the node1 for comparision
                pRBElem2 - Pointer to the node2 for comparision
 Description :  RBTree Compare function for
                FsofcFlowTable table
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  1,-1 0
****************************************************************************/

PUBLIC INT4
FsofcFlowTableRBCmp (tRBElem * pRBElem1, tRBElem * pRBElem2)
{

    tOfcFsofcFlowEntry *pFsofcFlowEntry1 = (tOfcFsofcFlowEntry *) pRBElem1;
    tOfcFsofcFlowEntry *pFsofcFlowEntry2 = (tOfcFsofcFlowEntry *) pRBElem2;
    INT4                i4MaxLength = 0;

    if (pFsofcFlowEntry1->MibObject.u4FsofcTableIndex >
        pFsofcFlowEntry2->MibObject.u4FsofcTableIndex)
    {
        return 1;
    }
    else if (pFsofcFlowEntry1->MibObject.u4FsofcTableIndex <
             pFsofcFlowEntry2->MibObject.u4FsofcTableIndex)
    {
        return -1;
    }

    if (pFsofcFlowEntry1->MibObject.u4FsofcFlowIndex >
        pFsofcFlowEntry2->MibObject.u4FsofcFlowIndex)
    {
        return 1;
    }
    else if (pFsofcFlowEntry1->MibObject.u4FsofcFlowIndex <
             pFsofcFlowEntry2->MibObject.u4FsofcFlowIndex)
    {
        return -1;
    }

    if (pFsofcFlowEntry1->MibObject.u4FsofcContextId >
        pFsofcFlowEntry2->MibObject.u4FsofcContextId)
    {
        return 1;
    }
    else if (pFsofcFlowEntry1->MibObject.u4FsofcContextId <
             pFsofcFlowEntry2->MibObject.u4FsofcContextId)
    {
        return -1;
    }

    UNUSED_PARAM (i4MaxLength);

    return 0;
}

/****************************************************************************
 Function    :  FsofcGroupTableRBCmp
 Input       :  pRBElem1 - Pointer to the node1 for comparision
                pRBElem2 - Pointer to the node2 for comparision
 Description :  RBTree Compare function for
                FsofcGroupTable table
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  1,-1 0
****************************************************************************/

PUBLIC INT4
FsofcGroupTableRBCmp (tRBElem * pRBElem1, tRBElem * pRBElem2)
{

    tOfcFsofcGroupEntry *pFsofcGroupEntry1 = (tOfcFsofcGroupEntry *) pRBElem1;
    tOfcFsofcGroupEntry *pFsofcGroupEntry2 = (tOfcFsofcGroupEntry *) pRBElem2;
    INT4                i4MaxLength = 0;

    if (pFsofcGroupEntry1->MibObject.u4FsofcGroupIndex >
        pFsofcGroupEntry2->MibObject.u4FsofcGroupIndex)
    {
        return 1;
    }
    else if (pFsofcGroupEntry1->MibObject.u4FsofcGroupIndex <
             pFsofcGroupEntry2->MibObject.u4FsofcGroupIndex)
    {
        return -1;
    }

    if (pFsofcGroupEntry1->MibObject.u4FsofcContextId >
        pFsofcGroupEntry2->MibObject.u4FsofcContextId)
    {
        return 1;
    }
    else if (pFsofcGroupEntry1->MibObject.u4FsofcContextId <
             pFsofcGroupEntry2->MibObject.u4FsofcContextId)
    {
        return -1;
    }

    UNUSED_PARAM (i4MaxLength);

    return 0;
}

/****************************************************************************
 Function    :  FsofcMeterTableRBCmp
 Input       :  pRBElem1 - Pointer to the node1 for comparision
                pRBElem2 - Pointer to the node2 for comparision
 Description :  RBTree Compare function for
                FsofcMeterTable table
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  1,-1 0
****************************************************************************/

PUBLIC INT4
FsofcMeterTableRBCmp (tRBElem * pRBElem1, tRBElem * pRBElem2)
{

    tOfcFsofcMeterEntry *pFsofcMeterEntry1 = (tOfcFsofcMeterEntry *) pRBElem1;
    tOfcFsofcMeterEntry *pFsofcMeterEntry2 = (tOfcFsofcMeterEntry *) pRBElem2;
    INT4                i4MaxLength = 0;

    if (pFsofcMeterEntry1->MibObject.u4FsofcMeterIndex >
        pFsofcMeterEntry2->MibObject.u4FsofcMeterIndex)
    {
        return 1;
    }
    else if (pFsofcMeterEntry1->MibObject.u4FsofcMeterIndex <
             pFsofcMeterEntry2->MibObject.u4FsofcMeterIndex)
    {
        return -1;
    }

    if (pFsofcMeterEntry1->MibObject.u4FsofcContextId >
        pFsofcMeterEntry2->MibObject.u4FsofcContextId)
    {
        return 1;
    }
    else if (pFsofcMeterEntry1->MibObject.u4FsofcContextId <
             pFsofcMeterEntry2->MibObject.u4FsofcContextId)
    {
        return -1;
    }

    UNUSED_PARAM (i4MaxLength);

    return 0;
}

/****************************************************************************
 Function    :  FsofcFTableRBCmp
 Input       :  pRBElem1 - Pointer to the node1 for comparision
                pRBElem2 - Pointer to the node2 for comparision
 Description :  RBTree Compare function for
                FsofcFTable table
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  1,-1 0
****************************************************************************/

PUBLIC INT4
FsofcFTableRBCmp (tRBElem * pRBElem1, tRBElem * pRBElem2)
{
    tOfcFsofcFlowTable *pFsofcFEntry1 = (tOfcFsofcFlowTable *) pRBElem1;
    tOfcFsofcFlowTable *pFsofcFEntry2 = (tOfcFsofcFlowTable *) pRBElem2;
    INT4                i4MaxLength = 0;

    if (pFsofcFEntry1->u4TableIndex > pFsofcFEntry2->u4TableIndex)
    {
        return 1;
    }
    else if (pFsofcFEntry1->u4TableIndex < pFsofcFEntry2->u4TableIndex)
    {
        return -1;
    }

    if (pFsofcFEntry1->u4ContextId > pFsofcFEntry2->u4ContextId)
    {
        return 1;
    }
    else if (pFsofcFEntry1->u4ContextId < pFsofcFEntry2->u4ContextId)
    {
        return -1;
    }

    UNUSED_PARAM (i4MaxLength);

    return 0;
}

/****************************************************************************
 Function    :  FsofcCfgTableFilterInputs
 Input       :  The Indices
                pOfcFsofcCfgEntry
                pOfcSetFsofcCfgEntry
                pOfcIsSetFsofcCfgEntry
 Description :  This Routine checks set value 
                with that of the value in database
                to find whether the same input is given
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/
INT4
FsofcCfgTableFilterInputs (tOfcFsofcCfgEntry * pOfcFsofcCfgEntry,
                           tOfcFsofcCfgEntry * pOfcSetFsofcCfgEntry,
                           tOfcIsSetFsofcCfgEntry * pOfcIsSetFsofcCfgEntry)
{
    if (pOfcIsSetFsofcCfgEntry->bFsofcContextId == OSIX_TRUE)
    {
        if (pOfcFsofcCfgEntry->MibObject.u4FsofcContextId ==
            pOfcSetFsofcCfgEntry->MibObject.u4FsofcContextId)
            pOfcIsSetFsofcCfgEntry->bFsofcContextId = OSIX_FALSE;
    }
    if (pOfcIsSetFsofcCfgEntry->bFsofcModuleStatus == OSIX_TRUE)
    {
        if (pOfcFsofcCfgEntry->MibObject.i4FsofcModuleStatus ==
            pOfcSetFsofcCfgEntry->MibObject.i4FsofcModuleStatus)
            pOfcIsSetFsofcCfgEntry->bFsofcModuleStatus = OSIX_FALSE;
    }
    if (pOfcIsSetFsofcCfgEntry->bFsofcSupportedVersion == OSIX_TRUE)
    {
        if (pOfcFsofcCfgEntry->MibObject.i4FsofcSupportedVersion ==
            pOfcSetFsofcCfgEntry->MibObject.i4FsofcSupportedVersion)
            pOfcIsSetFsofcCfgEntry->bFsofcSupportedVersion = OSIX_FALSE;
    }
    if (pOfcIsSetFsofcCfgEntry->bFsofcDefaultFlowMissBehaviour == OSIX_TRUE)
    {
        if (pOfcFsofcCfgEntry->MibObject.i4FsofcDefaultFlowMissBehaviour ==
            pOfcSetFsofcCfgEntry->MibObject.i4FsofcDefaultFlowMissBehaviour)
            pOfcIsSetFsofcCfgEntry->bFsofcDefaultFlowMissBehaviour = OSIX_FALSE;
    }
    if (pOfcIsSetFsofcCfgEntry->bFsofcControlPktBuffering == OSIX_TRUE)
    {
        if (pOfcFsofcCfgEntry->MibObject.i4FsofcControlPktBuffering ==
            pOfcSetFsofcCfgEntry->MibObject.i4FsofcControlPktBuffering)
            pOfcIsSetFsofcCfgEntry->bFsofcControlPktBuffering = OSIX_FALSE;
    }
    if (pOfcIsSetFsofcCfgEntry->bFsofcIpReassembleStatus == OSIX_TRUE)
    {
        if (pOfcFsofcCfgEntry->MibObject.i4FsofcIpReassembleStatus ==
            pOfcSetFsofcCfgEntry->MibObject.i4FsofcIpReassembleStatus)
            pOfcIsSetFsofcCfgEntry->bFsofcIpReassembleStatus = OSIX_FALSE;
    }
    if (pOfcIsSetFsofcCfgEntry->bFsofcPortStpStatus == OSIX_TRUE)
    {
        if (pOfcFsofcCfgEntry->MibObject.i4FsofcPortStpStatus ==
            pOfcSetFsofcCfgEntry->MibObject.i4FsofcPortStpStatus)
            pOfcIsSetFsofcCfgEntry->bFsofcPortStpStatus = OSIX_FALSE;
    }
    if (pOfcIsSetFsofcCfgEntry->bFsofcTraceEnable == OSIX_TRUE)
    {
        if (pOfcFsofcCfgEntry->MibObject.u4FsofcTraceEnable ==
            pOfcSetFsofcCfgEntry->MibObject.u4FsofcTraceEnable)
            pOfcIsSetFsofcCfgEntry->bFsofcTraceEnable = OSIX_FALSE;
    }
    if (pOfcIsSetFsofcCfgEntry->bFsofcSwitchModeOnConnFailure == OSIX_TRUE)
    {
        if (pOfcFsofcCfgEntry->MibObject.i4FsofcSwitchModeOnConnFailure ==
            pOfcSetFsofcCfgEntry->MibObject.i4FsofcSwitchModeOnConnFailure)
            pOfcIsSetFsofcCfgEntry->bFsofcSwitchModeOnConnFailure = OSIX_FALSE;
    }
    if (pOfcIsSetFsofcCfgEntry->bFsofcSwitchEntryStatus == OSIX_TRUE)
    {
        if (pOfcFsofcCfgEntry->MibObject.i4FsofcSwitchEntryStatus ==
            pOfcSetFsofcCfgEntry->MibObject.i4FsofcSwitchEntryStatus)
            pOfcIsSetFsofcCfgEntry->bFsofcSwitchEntryStatus = OSIX_FALSE;
    }
    if (pOfcIsSetFsofcCfgEntry->bFsofcHybrid == OSIX_TRUE)
    {
        if (pOfcFsofcCfgEntry->MibObject.i4FsofcHybrid ==
            pOfcSetFsofcCfgEntry->MibObject.i4FsofcHybrid)
            pOfcIsSetFsofcCfgEntry->bFsofcHybrid = OSIX_FALSE;
    }
    if ((pOfcIsSetFsofcCfgEntry->bFsofcContextId == OSIX_FALSE)
        && (pOfcIsSetFsofcCfgEntry->bFsofcModuleStatus == OSIX_FALSE)
        && (pOfcIsSetFsofcCfgEntry->bFsofcSupportedVersion == OSIX_FALSE)
        && (pOfcIsSetFsofcCfgEntry->bFsofcDefaultFlowMissBehaviour ==
            OSIX_FALSE)
        && (pOfcIsSetFsofcCfgEntry->bFsofcControlPktBuffering == OSIX_FALSE)
        && (pOfcIsSetFsofcCfgEntry->bFsofcIpReassembleStatus == OSIX_FALSE)
        && (pOfcIsSetFsofcCfgEntry->bFsofcPortStpStatus == OSIX_FALSE)
        && (pOfcIsSetFsofcCfgEntry->bFsofcTraceEnable == OSIX_FALSE)
        && (pOfcIsSetFsofcCfgEntry->bFsofcSwitchModeOnConnFailure == OSIX_FALSE)
        && (pOfcIsSetFsofcCfgEntry->bFsofcSwitchEntryStatus == OSIX_FALSE)
        && (pOfcIsSetFsofcCfgEntry->bFsofcHybrid == OSIX_FALSE))
    {
        return OSIX_FALSE;
    }
    else
    {
        return OSIX_TRUE;
    }
}

/****************************************************************************
 Function    :  FsofcControllerConnTableFilterInputs
 Input       :  The Indices
                pOfcFsofcControllerConnEntry
                pOfcSetFsofcControllerConnEntry
                pOfcIsSetFsofcControllerConnEntry
 Description :  This Routine checks set value 
                with that of the value in database
                to find whether the same input is given
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/
INT4
FsofcControllerConnTableFilterInputs (tOfcFsofcControllerConnEntry *
                                      pOfcFsofcControllerConnEntry,
                                      tOfcFsofcControllerConnEntry *
                                      pOfcSetFsofcControllerConnEntry,
                                      tOfcIsSetFsofcControllerConnEntry *
                                      pOfcIsSetFsofcControllerConnEntry)
{
    if (pOfcIsSetFsofcControllerConnEntry->bFsofcControllerIpAddrType ==
        OSIX_TRUE)
    {
        if (pOfcFsofcControllerConnEntry->
            MibObject.i4FsofcControllerIpAddrType ==
            pOfcSetFsofcControllerConnEntry->
            MibObject.i4FsofcControllerIpAddrType)
            pOfcIsSetFsofcControllerConnEntry->bFsofcControllerIpAddrType =
                OSIX_FALSE;
    }
    if (pOfcIsSetFsofcControllerConnEntry->bFsofcControllerIpAddress ==
        OSIX_TRUE)
    {
        if ((MEMCMP
             (pOfcFsofcControllerConnEntry->
              MibObject.au1FsofcControllerIpAddress,
              pOfcSetFsofcControllerConnEntry->
              MibObject.au1FsofcControllerIpAddress,
              pOfcSetFsofcControllerConnEntry->
              MibObject.i4FsofcControllerIpAddressLen) == 0)
            && (pOfcFsofcControllerConnEntry->
                MibObject.i4FsofcControllerIpAddressLen ==
                pOfcSetFsofcControllerConnEntry->
                MibObject.i4FsofcControllerIpAddressLen))
            pOfcIsSetFsofcControllerConnEntry->bFsofcControllerIpAddress =
                OSIX_FALSE;
    }
    if (pOfcIsSetFsofcControllerConnEntry->bFsofcControllerConnAuxId ==
        OSIX_TRUE)
    {
        if (pOfcFsofcControllerConnEntry->
            MibObject.i4FsofcControllerConnAuxId ==
            pOfcSetFsofcControllerConnEntry->
            MibObject.i4FsofcControllerConnAuxId)
            pOfcIsSetFsofcControllerConnEntry->bFsofcControllerConnAuxId =
                OSIX_FALSE;
    }
    if (pOfcIsSetFsofcControllerConnEntry->bFsofcControllerConnPort ==
        OSIX_TRUE)
    {
        if (pOfcFsofcControllerConnEntry->MibObject.i4FsofcControllerConnPort ==
            pOfcSetFsofcControllerConnEntry->
            MibObject.i4FsofcControllerConnPort)
            pOfcIsSetFsofcControllerConnEntry->bFsofcControllerConnPort =
                OSIX_FALSE;
    }
    if (pOfcIsSetFsofcControllerConnEntry->bFsofcControllerConnProtocol ==
        OSIX_TRUE)
    {
        if (pOfcFsofcControllerConnEntry->
            MibObject.i4FsofcControllerConnProtocol ==
            pOfcSetFsofcControllerConnEntry->
            MibObject.i4FsofcControllerConnProtocol)
            pOfcIsSetFsofcControllerConnEntry->bFsofcControllerConnProtocol =
                OSIX_FALSE;
    }
    if (pOfcIsSetFsofcControllerConnEntry->bFsofcControllerRole == OSIX_TRUE)
    {
        if (pOfcFsofcControllerConnEntry->MibObject.i4FsofcControllerRole ==
            pOfcSetFsofcControllerConnEntry->MibObject.i4FsofcControllerRole)
            pOfcIsSetFsofcControllerConnEntry->bFsofcControllerRole =
                OSIX_FALSE;
    }
    if (pOfcIsSetFsofcControllerConnEntry->bFsofcControllerConnEchoReqCount ==
        OSIX_TRUE)
    {
        if (pOfcFsofcControllerConnEntry->
            MibObject.i4FsofcControllerConnEchoReqCount ==
            pOfcSetFsofcControllerConnEntry->
            MibObject.i4FsofcControllerConnEchoReqCount)
            pOfcIsSetFsofcControllerConnEntry->bFsofcControllerConnEchoReqCount
                = OSIX_FALSE;
    }
    if (pOfcIsSetFsofcControllerConnEntry->bFsofcControllerConnEntryStatus ==
        OSIX_TRUE)
    {
        if (pOfcFsofcControllerConnEntry->
            MibObject.i4FsofcControllerConnEntryStatus ==
            pOfcSetFsofcControllerConnEntry->
            MibObject.i4FsofcControllerConnEntryStatus)
            pOfcIsSetFsofcControllerConnEntry->bFsofcControllerConnEntryStatus =
                OSIX_FALSE;
    }
    if (pOfcIsSetFsofcControllerConnEntry->bFsofcControllerConnBand ==
        OSIX_TRUE)
    {
        if (pOfcFsofcControllerConnEntry->MibObject.i4FsofcControllerConnBand ==
            pOfcSetFsofcControllerConnEntry->
            MibObject.i4FsofcControllerConnBand)
            pOfcIsSetFsofcControllerConnEntry->bFsofcControllerConnBand =
                OSIX_FALSE;
    }
    if (pOfcIsSetFsofcControllerConnEntry->bFsofcContextId == OSIX_TRUE)
    {
        if (pOfcFsofcControllerConnEntry->MibObject.u4FsofcContextId ==
            pOfcSetFsofcControllerConnEntry->MibObject.u4FsofcContextId)
            pOfcIsSetFsofcControllerConnEntry->bFsofcContextId = OSIX_FALSE;
    }
    if ((pOfcIsSetFsofcControllerConnEntry->bFsofcControllerIpAddrType ==
         OSIX_FALSE)
        && (pOfcIsSetFsofcControllerConnEntry->bFsofcControllerIpAddress ==
            OSIX_FALSE)
        && (pOfcIsSetFsofcControllerConnEntry->bFsofcControllerConnAuxId ==
            OSIX_FALSE)
        && (pOfcIsSetFsofcControllerConnEntry->bFsofcControllerConnPort ==
            OSIX_FALSE)
        && (pOfcIsSetFsofcControllerConnEntry->bFsofcControllerConnProtocol ==
            OSIX_FALSE)
        && (pOfcIsSetFsofcControllerConnEntry->bFsofcControllerRole ==
            OSIX_FALSE)
        && (pOfcIsSetFsofcControllerConnEntry->bFsofcControllerConnEchoReqCount
            == OSIX_FALSE)
        && (pOfcIsSetFsofcControllerConnEntry->bFsofcControllerConnEntryStatus
            == OSIX_FALSE)
        && (pOfcIsSetFsofcControllerConnEntry->bFsofcControllerConnBand ==
            OSIX_FALSE)
        && (pOfcIsSetFsofcControllerConnEntry->bFsofcContextId == OSIX_FALSE))
    {
        return OSIX_FALSE;
    }
    else
    {
        return OSIX_TRUE;
    }
}

/****************************************************************************
 Function    :  FsofcIfTableFilterInputs
 Input       :  The Indices
                pOfcFsofcIfEntry
                pOfcSetFsofcIfEntry
                pOfcIsSetFsofcIfEntry
 Description :  This Routine checks set value 
                with that of the value in database
                to find whether the same input is given
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/
INT4
FsofcIfTableFilterInputs (tOfcFsofcIfEntry * pOfcFsofcIfEntry,
                          tOfcFsofcIfEntry * pOfcSetFsofcIfEntry,
                          tOfcIsSetFsofcIfEntry * pOfcIsSetFsofcIfEntry)
{
    if (pOfcIsSetFsofcIfEntry->bFsofcIfIndex == OSIX_TRUE)
    {
        if (pOfcFsofcIfEntry->MibObject.i4FsofcIfIndex ==
            pOfcSetFsofcIfEntry->MibObject.i4FsofcIfIndex)
            pOfcIsSetFsofcIfEntry->bFsofcIfIndex = OSIX_FALSE;
    }
    if (pOfcIsSetFsofcIfEntry->bFsofcVlanEgressPorts == OSIX_TRUE)
    {
        if ((MEMCMP (pOfcFsofcIfEntry->MibObject.au1FsofcVlanEgressPorts,
                     pOfcSetFsofcIfEntry->MibObject.au1FsofcVlanEgressPorts,
                     pOfcSetFsofcIfEntry->
                     MibObject.i4FsofcVlanEgressPortsLen) == 0)
            && (pOfcFsofcIfEntry->MibObject.i4FsofcVlanEgressPortsLen ==
                pOfcSetFsofcIfEntry->MibObject.i4FsofcVlanEgressPortsLen))
            pOfcIsSetFsofcIfEntry->bFsofcVlanEgressPorts = OSIX_FALSE;
    }
    if (pOfcIsSetFsofcIfEntry->bFsofcVlanUntaggedPorts == OSIX_TRUE)
    {
        if ((MEMCMP (pOfcFsofcIfEntry->MibObject.au1FsofcVlanUntaggedPorts,
                     pOfcSetFsofcIfEntry->MibObject.au1FsofcVlanUntaggedPorts,
                     pOfcSetFsofcIfEntry->
                     MibObject.i4FsofcVlanUntaggedPortsLen) == 0)
            && (pOfcFsofcIfEntry->MibObject.i4FsofcVlanUntaggedPortsLen ==
                pOfcSetFsofcIfEntry->MibObject.i4FsofcVlanUntaggedPortsLen))
            pOfcIsSetFsofcIfEntry->bFsofcVlanUntaggedPorts = OSIX_FALSE;
    }
    if (pOfcIsSetFsofcIfEntry->bFsofcIfContextId == OSIX_TRUE)
    {
        if (pOfcFsofcIfEntry->MibObject.u4FsofcIfContextId ==
            pOfcSetFsofcIfEntry->MibObject.u4FsofcIfContextId)
            pOfcIsSetFsofcIfEntry->bFsofcIfContextId = OSIX_FALSE;
    }
    if (pOfcIsSetFsofcIfEntry->bFsofcContextId == OSIX_TRUE)
    {
        if (pOfcFsofcIfEntry->MibObject.u4FsofcContextId ==
            pOfcSetFsofcIfEntry->MibObject.u4FsofcContextId)
            pOfcIsSetFsofcIfEntry->bFsofcContextId = OSIX_FALSE;
    }
    if ((pOfcIsSetFsofcIfEntry->bFsofcIfIndex == OSIX_FALSE)
        && (pOfcIsSetFsofcIfEntry->bFsofcVlanEgressPorts == OSIX_FALSE)
        && (pOfcIsSetFsofcIfEntry->bFsofcVlanUntaggedPorts == OSIX_FALSE)
        && (pOfcIsSetFsofcIfEntry->bFsofcIfContextId == OSIX_FALSE)
        && (pOfcIsSetFsofcIfEntry->bFsofcContextId == OSIX_FALSE))
    {
        return OSIX_FALSE;
    }
    else
    {
        return OSIX_TRUE;
    }
}

/****************************************************************************
 Function    :  OfcSetAllFsofcCfgTableTrigger
 Input       :  The Indices
                pOfcSetFsofcCfgEntry
                pOfcIsSetFsofcCfgEntry
 Description :  This Routine is used to send 
                MSR and RM indication
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/
INT4
OfcSetAllFsofcCfgTableTrigger (tOfcFsofcCfgEntry * pOfcSetFsofcCfgEntry,
                               tOfcIsSetFsofcCfgEntry * pOfcIsSetFsofcCfgEntry,
                               INT4 i4SetOption)
{

    if (pOfcIsSetFsofcCfgEntry->bFsofcContextId == OSIX_TRUE)
    {
        nmhSetCmnNew (FsofcContextId, 13, OfcMainTaskLock, OfcMainTaskUnLock, 0,
                      0, 1, i4SetOption, "%u %u",
                      pOfcSetFsofcCfgEntry->MibObject.u4FsofcContextId,
                      pOfcSetFsofcCfgEntry->MibObject.u4FsofcContextId);
    }
    if (pOfcIsSetFsofcCfgEntry->bFsofcModuleStatus == OSIX_TRUE)
    {
        nmhSetCmnNew (FsofcModuleStatus, 13, OfcMainTaskLock, OfcMainTaskUnLock,
                      0, 0, 1, i4SetOption, "%u %i",
                      pOfcSetFsofcCfgEntry->MibObject.u4FsofcContextId,
                      pOfcSetFsofcCfgEntry->MibObject.i4FsofcModuleStatus);
    }
    if (pOfcIsSetFsofcCfgEntry->bFsofcSupportedVersion == OSIX_TRUE)
    {
        nmhSetCmnNew (FsofcSupportedVersion, 13, OfcMainTaskLock,
                      OfcMainTaskUnLock, 0, 0, 1, i4SetOption, "%u %i",
                      pOfcSetFsofcCfgEntry->MibObject.u4FsofcContextId,
                      pOfcSetFsofcCfgEntry->MibObject.i4FsofcSupportedVersion);
    }
    if (pOfcIsSetFsofcCfgEntry->bFsofcDefaultFlowMissBehaviour == OSIX_TRUE)
    {
        nmhSetCmnNew (FsofcDefaultFlowMissBehaviour, 13, OfcMainTaskLock,
                      OfcMainTaskUnLock, 0, 0, 1, i4SetOption, "%u %i",
                      pOfcSetFsofcCfgEntry->MibObject.u4FsofcContextId,
                      pOfcSetFsofcCfgEntry->
                      MibObject.i4FsofcDefaultFlowMissBehaviour);
    }
    if (pOfcIsSetFsofcCfgEntry->bFsofcControlPktBuffering == OSIX_TRUE)
    {
        nmhSetCmnNew (FsofcControlPktBuffering, 13, OfcMainTaskLock,
                      OfcMainTaskUnLock, 0, 0, 1, i4SetOption, "%u %i",
                      pOfcSetFsofcCfgEntry->MibObject.u4FsofcContextId,
                      pOfcSetFsofcCfgEntry->
                      MibObject.i4FsofcControlPktBuffering);
    }
    if (pOfcIsSetFsofcCfgEntry->bFsofcIpReassembleStatus == OSIX_TRUE)
    {
        nmhSetCmnNew (FsofcIpReassembleStatus, 13, OfcMainTaskLock,
                      OfcMainTaskUnLock, 0, 0, 1, i4SetOption, "%u %i",
                      pOfcSetFsofcCfgEntry->MibObject.u4FsofcContextId,
                      pOfcSetFsofcCfgEntry->
                      MibObject.i4FsofcIpReassembleStatus);
    }
    if (pOfcIsSetFsofcCfgEntry->bFsofcPortStpStatus == OSIX_TRUE)
    {
        nmhSetCmnNew (FsofcPortStpStatus, 13, OfcMainTaskLock,
                      OfcMainTaskUnLock, 0, 0, 1, i4SetOption, "%u %i",
                      pOfcSetFsofcCfgEntry->MibObject.u4FsofcContextId,
                      pOfcSetFsofcCfgEntry->MibObject.i4FsofcPortStpStatus);
    }
    if (pOfcIsSetFsofcCfgEntry->bFsofcTraceEnable == OSIX_TRUE)
    {
        nmhSetCmnNew (FsofcTraceEnable, 13, OfcMainTaskLock, OfcMainTaskUnLock,
                      0, 0, 1, i4SetOption, "%u %u",
                      pOfcSetFsofcCfgEntry->MibObject.u4FsofcContextId,
                      pOfcSetFsofcCfgEntry->MibObject.u4FsofcTraceEnable);
    }
    if (pOfcIsSetFsofcCfgEntry->bFsofcSwitchModeOnConnFailure == OSIX_TRUE)
    {
        nmhSetCmnNew (FsofcSwitchModeOnConnFailure, 13, OfcMainTaskLock,
                      OfcMainTaskUnLock, 0, 0, 1, i4SetOption, "%u %i",
                      pOfcSetFsofcCfgEntry->MibObject.u4FsofcContextId,
                      pOfcSetFsofcCfgEntry->
                      MibObject.i4FsofcSwitchModeOnConnFailure);
    }
    if (pOfcIsSetFsofcCfgEntry->bFsofcSwitchEntryStatus == OSIX_TRUE)
    {
        nmhSetCmnNew (FsofcSwitchEntryStatus, 13, OfcMainTaskLock,
                      OfcMainTaskUnLock, 0, 1, 1, i4SetOption, "%u %i",
                      pOfcSetFsofcCfgEntry->MibObject.u4FsofcContextId,
                      pOfcSetFsofcCfgEntry->MibObject.i4FsofcSwitchEntryStatus);
    }
    if (pOfcIsSetFsofcCfgEntry->bFsofcHybrid == OSIX_TRUE)
    {
        nmhSetCmnNew (FsofcHybrid, 13, OfcMainTaskLock, OfcMainTaskUnLock, 0, 0,
                      1, i4SetOption, "%u %i",
                      pOfcSetFsofcCfgEntry->MibObject.u4FsofcContextId,
                      pOfcSetFsofcCfgEntry->MibObject.i4FsofcHybrid);
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  OfcSetAllFsofcControllerConnTableTrigger
 Input       :  The Indices
                pOfcSetFsofcControllerConnEntry
                pOfcIsSetFsofcControllerConnEntry
 Description :  This Routine is used to send 
                MSR and RM indication
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/
INT4
OfcSetAllFsofcControllerConnTableTrigger (tOfcFsofcControllerConnEntry *
                                          pOfcSetFsofcControllerConnEntry,
                                          tOfcIsSetFsofcControllerConnEntry *
                                          pOfcIsSetFsofcControllerConnEntry,
                                          INT4 i4SetOption)
{
    tSNMP_OCTET_STRING_TYPE FsofcControllerIpAddressVal;
    UINT1               au1FsofcControllerIpAddressVal[128];

    MEMSET (au1FsofcControllerIpAddressVal, 0,
            sizeof (au1FsofcControllerIpAddressVal));
    FsofcControllerIpAddressVal.pu1_OctetList = au1FsofcControllerIpAddressVal;
    FsofcControllerIpAddressVal.i4_Length = 0;

    if (pOfcIsSetFsofcControllerConnEntry->bFsofcControllerIpAddrType ==
        OSIX_TRUE)
    {
        nmhSetCmnNew (FsofcControllerIpAddrType, 13, OfcMainTaskLock,
                      OfcMainTaskUnLock, 0, 0, 4, i4SetOption, "%u %i %s %i %i",
                      pOfcSetFsofcControllerConnEntry->
                      MibObject.u4FsofcContextId,
                      pOfcSetFsofcControllerConnEntry->
                      MibObject.i4FsofcControllerIpAddrType,
                      &FsofcControllerIpAddressVal,
                      pOfcSetFsofcControllerConnEntry->
                      MibObject.i4FsofcControllerConnAuxId,
                      pOfcSetFsofcControllerConnEntry->
                      MibObject.i4FsofcControllerIpAddrType);
    }
    if (pOfcIsSetFsofcControllerConnEntry->bFsofcControllerIpAddress ==
        OSIX_TRUE)
    {
        MEMCPY (FsofcControllerIpAddressVal.pu1_OctetList,
                pOfcSetFsofcControllerConnEntry->
                MibObject.au1FsofcControllerIpAddress,
                pOfcSetFsofcControllerConnEntry->
                MibObject.i4FsofcControllerIpAddressLen);
        FsofcControllerIpAddressVal.i4_Length =
            pOfcSetFsofcControllerConnEntry->
            MibObject.i4FsofcControllerIpAddressLen;

        nmhSetCmnNew (FsofcControllerIpAddress, 13, OfcMainTaskLock,
                      OfcMainTaskUnLock, 0, 0, 4, i4SetOption, "%u %i %s %i %s",
                      pOfcSetFsofcControllerConnEntry->
                      MibObject.u4FsofcContextId,
                      pOfcSetFsofcControllerConnEntry->
                      MibObject.i4FsofcControllerIpAddrType,
                      &FsofcControllerIpAddressVal,
                      pOfcSetFsofcControllerConnEntry->
                      MibObject.i4FsofcControllerConnAuxId,
                      &FsofcControllerIpAddressVal);
    }
    if (pOfcIsSetFsofcControllerConnEntry->bFsofcControllerConnAuxId ==
        OSIX_TRUE)
    {
        nmhSetCmnNew (FsofcControllerConnAuxId, 13, OfcMainTaskLock,
                      OfcMainTaskUnLock, 0, 0, 4, i4SetOption, "%u %i %s %i %i",
                      pOfcSetFsofcControllerConnEntry->
                      MibObject.u4FsofcContextId,
                      pOfcSetFsofcControllerConnEntry->
                      MibObject.i4FsofcControllerIpAddrType,
                      &FsofcControllerIpAddressVal,
                      pOfcSetFsofcControllerConnEntry->
                      MibObject.i4FsofcControllerConnAuxId,
                      pOfcSetFsofcControllerConnEntry->
                      MibObject.i4FsofcControllerConnAuxId);
    }
    if (pOfcIsSetFsofcControllerConnEntry->bFsofcControllerConnPort ==
        OSIX_TRUE)
    {
        nmhSetCmnNew (FsofcControllerConnPort, 13, OfcMainTaskLock,
                      OfcMainTaskUnLock, 0, 0, 4, i4SetOption, "%u %i %s %i %i",
                      pOfcSetFsofcControllerConnEntry->
                      MibObject.u4FsofcContextId,
                      pOfcSetFsofcControllerConnEntry->
                      MibObject.i4FsofcControllerIpAddrType,
                      &FsofcControllerIpAddressVal,
                      pOfcSetFsofcControllerConnEntry->
                      MibObject.i4FsofcControllerConnAuxId,
                      pOfcSetFsofcControllerConnEntry->
                      MibObject.i4FsofcControllerConnPort);
    }
    if (pOfcIsSetFsofcControllerConnEntry->bFsofcControllerConnProtocol ==
        OSIX_TRUE)
    {
        nmhSetCmnNew (FsofcControllerConnProtocol, 13, OfcMainTaskLock,
                      OfcMainTaskUnLock, 0, 0, 4, i4SetOption, "%u %i %s %i %i",
                      pOfcSetFsofcControllerConnEntry->
                      MibObject.u4FsofcContextId,
                      pOfcSetFsofcControllerConnEntry->
                      MibObject.i4FsofcControllerIpAddrType,
                      &FsofcControllerIpAddressVal,
                      pOfcSetFsofcControllerConnEntry->
                      MibObject.i4FsofcControllerConnAuxId,
                      pOfcSetFsofcControllerConnEntry->
                      MibObject.i4FsofcControllerConnProtocol);
    }
    if (pOfcIsSetFsofcControllerConnEntry->bFsofcControllerRole == OSIX_TRUE)
    {
        nmhSetCmnNew (FsofcControllerRole, 13, OfcMainTaskLock,
                      OfcMainTaskUnLock, 0, 0, 4, i4SetOption, "%u %i %s %i %i",
                      pOfcSetFsofcControllerConnEntry->
                      MibObject.u4FsofcContextId,
                      pOfcSetFsofcControllerConnEntry->
                      MibObject.i4FsofcControllerIpAddrType,
                      &FsofcControllerIpAddressVal,
                      pOfcSetFsofcControllerConnEntry->
                      MibObject.i4FsofcControllerConnAuxId,
                      pOfcSetFsofcControllerConnEntry->
                      MibObject.i4FsofcControllerRole);
    }
    if (pOfcIsSetFsofcControllerConnEntry->bFsofcControllerConnEchoReqCount ==
        OSIX_TRUE)
    {
        nmhSetCmnNew (FsofcControllerConnEchoReqCount, 13, OfcMainTaskLock,
                      OfcMainTaskUnLock, 0, 0, 4, i4SetOption, "%u %i %s %i %i",
                      pOfcSetFsofcControllerConnEntry->
                      MibObject.u4FsofcContextId,
                      pOfcSetFsofcControllerConnEntry->
                      MibObject.i4FsofcControllerIpAddrType,
                      &FsofcControllerIpAddressVal,
                      pOfcSetFsofcControllerConnEntry->
                      MibObject.i4FsofcControllerConnAuxId,
                      pOfcSetFsofcControllerConnEntry->
                      MibObject.i4FsofcControllerConnEchoReqCount);
    }
    if (pOfcIsSetFsofcControllerConnEntry->bFsofcControllerConnEntryStatus ==
        OSIX_TRUE)
    {
        nmhSetCmnNew (FsofcControllerConnEntryStatus, 13, OfcMainTaskLock,
                      OfcMainTaskUnLock, 0, 1, 4, i4SetOption, "%u %i %s %i %i",
                      pOfcSetFsofcControllerConnEntry->
                      MibObject.u4FsofcContextId,
                      pOfcSetFsofcControllerConnEntry->
                      MibObject.i4FsofcControllerIpAddrType,
                      &FsofcControllerIpAddressVal,
                      pOfcSetFsofcControllerConnEntry->
                      MibObject.i4FsofcControllerConnAuxId,
                      pOfcSetFsofcControllerConnEntry->
                      MibObject.i4FsofcControllerConnEntryStatus);
    }
    if (pOfcIsSetFsofcControllerConnEntry->bFsofcControllerConnBand ==
        OSIX_TRUE)
    {
        nmhSetCmnNew (FsofcControllerConnBand, 13, OfcMainTaskLock,
                      OfcMainTaskUnLock, 0, 0, 4, i4SetOption, "%u %i %s %i %i",
                      pOfcSetFsofcControllerConnEntry->
                      MibObject.u4FsofcContextId,
                      pOfcSetFsofcControllerConnEntry->
                      MibObject.i4FsofcControllerIpAddrType,
                      &FsofcControllerIpAddressVal,
                      pOfcSetFsofcControllerConnEntry->
                      MibObject.i4FsofcControllerConnAuxId,
                      pOfcSetFsofcControllerConnEntry->
                      MibObject.i4FsofcControllerConnBand);
    }
    if (pOfcIsSetFsofcControllerConnEntry->bFsofcContextId == OSIX_TRUE)
    {
        nmhSetCmnNew (FsofcContextId, 13, OfcMainTaskLock, OfcMainTaskUnLock, 0,
                      0, 4, i4SetOption, "%u %i %s %i %u",
                      pOfcSetFsofcControllerConnEntry->
                      MibObject.u4FsofcContextId,
                      pOfcSetFsofcControllerConnEntry->
                      MibObject.i4FsofcControllerIpAddrType,
                      &FsofcControllerIpAddressVal,
                      pOfcSetFsofcControllerConnEntry->
                      MibObject.i4FsofcControllerConnAuxId,
                      pOfcSetFsofcControllerConnEntry->
                      MibObject.u4FsofcContextId);
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  OfcSetAllFsofcIfTableTrigger
 Input       :  The Indices
                pOfcSetFsofcIfEntry
                pOfcIsSetFsofcIfEntry
 Description :  This Routine is used to send 
                MSR and RM indication
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/
INT4
OfcSetAllFsofcIfTableTrigger (tOfcFsofcIfEntry * pOfcSetFsofcIfEntry,
                              tOfcIsSetFsofcIfEntry * pOfcIsSetFsofcIfEntry,
                              INT4 i4SetOption)
{
    tSNMP_OCTET_STRING_TYPE FsofcVlanEgressPortsVal;
    UINT1               au1FsofcVlanEgressPortsVal[256];
    tSNMP_OCTET_STRING_TYPE FsofcVlanUntaggedPortsVal;
    UINT1               au1FsofcVlanUntaggedPortsVal[256];

    MEMSET (au1FsofcVlanEgressPortsVal, 0, sizeof (au1FsofcVlanEgressPortsVal));
    FsofcVlanEgressPortsVal.pu1_OctetList = au1FsofcVlanEgressPortsVal;
    FsofcVlanEgressPortsVal.i4_Length = 0;

    MEMSET (au1FsofcVlanUntaggedPortsVal, 0,
            sizeof (au1FsofcVlanUntaggedPortsVal));
    FsofcVlanUntaggedPortsVal.pu1_OctetList = au1FsofcVlanUntaggedPortsVal;
    FsofcVlanUntaggedPortsVal.i4_Length = 0;

    if (pOfcIsSetFsofcIfEntry->bFsofcIfIndex == OSIX_TRUE)
    {
        nmhSetCmnNew (FsofcIfIndex, 13, OfcMainTaskLock, OfcMainTaskUnLock, 0,
                      0, 2, i4SetOption, "%u %i %i",
                      pOfcSetFsofcIfEntry->MibObject.u4FsofcContextId,
                      pOfcSetFsofcIfEntry->MibObject.i4FsofcIfIndex,
                      pOfcSetFsofcIfEntry->MibObject.i4FsofcIfIndex);
    }
    if (pOfcIsSetFsofcIfEntry->bFsofcVlanEgressPorts == OSIX_TRUE)
    {
        MEMCPY (FsofcVlanEgressPortsVal.pu1_OctetList,
                pOfcSetFsofcIfEntry->MibObject.au1FsofcVlanEgressPorts,
                pOfcSetFsofcIfEntry->MibObject.i4FsofcVlanEgressPortsLen);
        FsofcVlanEgressPortsVal.i4_Length =
            pOfcSetFsofcIfEntry->MibObject.i4FsofcVlanEgressPortsLen;

        nmhSetCmnNew (FsofcVlanEgressPorts, 13, OfcMainTaskLock,
                      OfcMainTaskUnLock, 0, 0, 2, i4SetOption, "%u %i %s",
                      pOfcSetFsofcIfEntry->MibObject.u4FsofcContextId,
                      pOfcSetFsofcIfEntry->MibObject.i4FsofcIfIndex,
                      &FsofcVlanEgressPortsVal);
    }
    if (pOfcIsSetFsofcIfEntry->bFsofcVlanUntaggedPorts == OSIX_TRUE)
    {
        MEMCPY (FsofcVlanUntaggedPortsVal.pu1_OctetList,
                pOfcSetFsofcIfEntry->MibObject.au1FsofcVlanUntaggedPorts,
                pOfcSetFsofcIfEntry->MibObject.i4FsofcVlanUntaggedPortsLen);
        FsofcVlanUntaggedPortsVal.i4_Length =
            pOfcSetFsofcIfEntry->MibObject.i4FsofcVlanUntaggedPortsLen;

        nmhSetCmnNew (FsofcVlanUntaggedPorts, 13, OfcMainTaskLock,
                      OfcMainTaskUnLock, 0, 0, 2, i4SetOption, "%u %i %s",
                      pOfcSetFsofcIfEntry->MibObject.u4FsofcContextId,
                      pOfcSetFsofcIfEntry->MibObject.i4FsofcIfIndex,
                      &FsofcVlanUntaggedPortsVal);
    }
    if (pOfcIsSetFsofcIfEntry->bFsofcIfContextId == OSIX_TRUE)
    {
        nmhSetCmnNew (FsofcIfContextId, 13, OfcMainTaskLock, OfcMainTaskUnLock,
                      0, 0, 2, i4SetOption, "%u %i %u",
                      pOfcSetFsofcIfEntry->MibObject.u4FsofcContextId,
                      pOfcSetFsofcIfEntry->MibObject.i4FsofcIfIndex,
                      pOfcSetFsofcIfEntry->MibObject.u4FsofcIfContextId);
    }
    if (pOfcIsSetFsofcIfEntry->bFsofcContextId == OSIX_TRUE)
    {
        nmhSetCmnNew (FsofcContextId, 13, OfcMainTaskLock, OfcMainTaskUnLock,
                      0, 0, 2, i4SetOption, "%u %i %u",
                      pOfcSetFsofcIfEntry->MibObject.u4FsofcContextId,
                      pOfcSetFsofcIfEntry->MibObject.i4FsofcIfIndex,
                      pOfcSetFsofcIfEntry->MibObject.u4FsofcContextId);
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  OfcFsofcCfgTableCreateApi
 Input       :  pOfcFsofcCfgEntry
 Description :  This Routine Creates the memory 
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/

tOfcFsofcCfgEntry  *
OfcFsofcCfgTableCreateApi (tOfcFsofcCfgEntry * pSetOfcFsofcCfgEntry)
{
    tOfcFsofcCfgEntry  *pOfcFsofcCfgEntry = NULL;

    if (pSetOfcFsofcCfgEntry == NULL)
    {
        OFC_TRC ((OFC_UTIL_TRC,
                  "OfcFsofcCfgTableCreatApi: pSetOfcFsofcCfgEntry is NULL.\r\n"));
        return NULL;
    }
    /* Allocate memory for the new node */
    pOfcFsofcCfgEntry =
        (tOfcFsofcCfgEntry *) MemAllocMemBlk (OFC_FSOFCCFGTABLE_POOLID);
    if (pOfcFsofcCfgEntry == NULL)
    {
        OFC_TRC ((OFC_UTIL_TRC,
                  "OfcFsofcCfgTableCreatApi: Fail to Allocate Memory.\r\n"));
        return NULL;
    }
    else
    {
        MEMCPY (pOfcFsofcCfgEntry, pSetOfcFsofcCfgEntry,
                sizeof (tOfcFsofcCfgEntry));
        if (RBTreeAdd
            (gOfcGlobals.OfcGlbMib.FsofcCfgTable,
             (tRBElem *) pOfcFsofcCfgEntry) != RB_SUCCESS)
        {
            OFC_TRC ((OFC_UTIL_TRC,
                      "OfcFsofcCfgTableCreatApi: Fail to Add the node.\r\n"));
            MemReleaseMemBlock (OFC_FSOFCCFGTABLE_POOLID,
                                (UINT1 *) pOfcFsofcCfgEntry);
            return NULL;
        }
        return pOfcFsofcCfgEntry;
    }
}

/****************************************************************************
 Function    :  OfcFsofcControllerConnTableCreateApi
 Input       :  pOfcFsofcControllerConnEntry
 Description :  This Routine Creates the memory 
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/

tOfcFsofcControllerConnEntry *
OfcFsofcControllerConnTableCreateApi (tOfcFsofcControllerConnEntry *
                                      pSetOfcFsofcControllerConnEntry)
{
    tOfcFsofcControllerConnEntry *pOfcFsofcControllerConnEntry = NULL;

    if (pSetOfcFsofcControllerConnEntry == NULL)
    {
        OFC_TRC ((OFC_UTIL_TRC,
                  "OfcFsofcControllerConnTableCreatApi: pSetOfcFsofcControllerConnEntry is NULL.\r\n"));
        return NULL;
    }
    /* Allocate memory for the new node */
    pOfcFsofcControllerConnEntry =
        (tOfcFsofcControllerConnEntry *)
        MemAllocMemBlk (OFC_FSOFCCONTROLLERCONNTABLE_POOLID);
    if (pOfcFsofcControllerConnEntry == NULL)
    {
        OFC_TRC ((OFC_UTIL_TRC,
                  "OfcFsofcControllerConnTableCreatApi: Fail to Allocate Memory.\r\n"));
        return NULL;
    }
    else
    {
        MEMCPY (pOfcFsofcControllerConnEntry, pSetOfcFsofcControllerConnEntry,
                sizeof (tOfcFsofcControllerConnEntry));
        if (RBTreeAdd
            (gOfcGlobals.OfcGlbMib.FsofcControllerConnTable,
             (tRBElem *) pOfcFsofcControllerConnEntry) != RB_SUCCESS)
        {
            OFC_TRC ((OFC_UTIL_TRC,
                      "OfcFsofcControllerConnTableCreatApi: Fail to Add the node.\r\n"));
            MemReleaseMemBlock (OFC_FSOFCCONTROLLERCONNTABLE_POOLID,
                                (UINT1 *) pOfcFsofcControllerConnEntry);
            return NULL;
        }
        return pOfcFsofcControllerConnEntry;
    }
}

/****************************************************************************
 Function    :  OfcFsofcIfTableCreateApi
 Input       :  pOfcFsofcIfEntry
 Description :  This Routine Creates the memory 
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/

tOfcFsofcIfEntry   *
OfcFsofcIfTableCreateApi (tOfcFsofcIfEntry * pSetOfcFsofcIfEntry)
{
    tOfcFsofcIfEntry   *pOfcFsofcIfEntry = NULL;

    if (pSetOfcFsofcIfEntry == NULL)
    {
        OFC_TRC ((OFC_UTIL_TRC,
                  "OfcFsofcIfTableCreatApi: pSetOfcFsofcIfEntry is NULL.\r\n"));
        return NULL;
    }
    /* Allocate memory for the new node */
    pOfcFsofcIfEntry =
        (tOfcFsofcIfEntry *) MemAllocMemBlk (OFC_FSOFCIFTABLE_POOLID);
    if (pOfcFsofcIfEntry == NULL)
    {
        OFC_TRC ((OFC_UTIL_TRC,
                  "OfcFsofcIfTableCreatApi: Fail to Allocate Memory.\r\n"));
        return NULL;
    }
    else
    {
        MEMCPY (pOfcFsofcIfEntry, pSetOfcFsofcIfEntry,
                sizeof (tOfcFsofcIfEntry));
        if (RBTreeAdd
            (gOfcGlobals.OfcGlbMib.FsofcIfTable,
             (tRBElem *) pOfcFsofcIfEntry) != RB_SUCCESS)
        {
            OFC_TRC ((OFC_UTIL_TRC,
                      "OfcFsofcIfTableCreatApi: Fail to Add the node.\r\n"));
            MemReleaseMemBlock (OFC_FSOFCIFTABLE_POOLID,
                                (UINT1 *) pOfcFsofcIfEntry);
            return NULL;
        }
        return pOfcFsofcIfEntry;
    }
}

/****************************************************************************
 Function    :  OfcFsofcFlowTableCreateApi
 Input       :  pOfcFsofcFlowEntry
 Description :  This Routine Creates the memory 
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/

tOfcFsofcFlowEntry *
OfcFsofcFlowTableCreateApi (tOfcFsofcFlowEntry * pSetOfcFsofcFlowEntry)
{
    tOfcFsofcFlowEntry *pOfcFsofcFlowEntry = NULL;

    if (pSetOfcFsofcFlowEntry == NULL)
    {
        OFC_TRC ((OFC_UTIL_TRC,
                  "OfcFsofcFlowTableCreatApi: pSetOfcFsofcFlowEntry is NULL.\r\n"));
        return NULL;
    }
    /* Allocate memory for the new node */
    pOfcFsofcFlowEntry =
        (tOfcFsofcFlowEntry *) MemAllocMemBlk (OFC_FSOFCFLOWTABLE_POOLID);
    if (pOfcFsofcFlowEntry == NULL)
    {
        OFC_TRC ((OFC_UTIL_TRC,
                  "OfcFsofcFlowTableCreatApi: Fail to Allocate Memory.\r\n"));
        return NULL;
    }
    else
    {
        MEMCPY (pOfcFsofcFlowEntry, pSetOfcFsofcFlowEntry,
                sizeof (tOfcFsofcFlowEntry));
        if (RBTreeAdd
            (gOfcGlobals.OfcGlbMib.FsofcFlowTable,
             (tRBElem *) pOfcFsofcFlowEntry) != RB_SUCCESS)
        {
            OFC_TRC ((OFC_UTIL_TRC,
                      "OfcFsofcFlowTableCreatApi: Fail to Add the node.\r\n"));
            MemReleaseMemBlock (OFC_FSOFCFLOWTABLE_POOLID,
                                (UINT1 *) pOfcFsofcFlowEntry);
            return NULL;
        }
        return pOfcFsofcFlowEntry;
    }
}

/****************************************************************************
 Function    :  OfcFsofcGroupTableCreateApi
 Input       :  pOfcFsofcGroupEntry
 Description :  This Routine Creates the memory 
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/

tOfcFsofcGroupEntry *
OfcFsofcGroupTableCreateApi (tOfcFsofcGroupEntry * pSetOfcFsofcGroupEntry)
{
    tOfcFsofcGroupEntry *pOfcFsofcGroupEntry = NULL;

    if (pSetOfcFsofcGroupEntry == NULL)
    {
        OFC_TRC ((OFC_UTIL_TRC,
                  "OfcFsofcGroupTableCreatApi: pSetOfcFsofcGroupEntry is NULL.\r\n"));
        return NULL;
    }
    /* Allocate memory for the new node */
    pOfcFsofcGroupEntry =
        (tOfcFsofcGroupEntry *) MemAllocMemBlk (OFC_FSOFCGROUPTABLE_POOLID);
    if (pOfcFsofcGroupEntry == NULL)
    {
        OFC_TRC ((OFC_UTIL_TRC,
                  "OfcFsofcGroupTableCreatApi: Fail to Allocate Memory.\r\n"));
        return NULL;
    }
    else
    {
        MEMCPY (pOfcFsofcGroupEntry, pSetOfcFsofcGroupEntry,
                sizeof (tOfcFsofcGroupEntry));
        if (RBTreeAdd
            (gOfcGlobals.OfcGlbMib.FsofcGroupTable,
             (tRBElem *) pOfcFsofcGroupEntry) != RB_SUCCESS)
        {
            OFC_TRC ((OFC_UTIL_TRC,
                      "OfcFsofcGroupTableCreatApi: Fail to Add the node.\r\n"));
            MemReleaseMemBlock (OFC_FSOFCGROUPTABLE_POOLID,
                                (UINT1 *) pOfcFsofcGroupEntry);
            return NULL;
        }
        return pOfcFsofcGroupEntry;
    }
}

/****************************************************************************
 Function    :  OfcFsofcMeterTableCreateApi
 Input       :  pOfcFsofcMeterEntry
 Description :  This Routine Creates the memory 
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/

tOfcFsofcMeterEntry *
OfcFsofcMeterTableCreateApi (tOfcFsofcMeterEntry * pSetOfcFsofcMeterEntry)
{
    tOfcFsofcMeterEntry *pOfcFsofcMeterEntry = NULL;

    if (pSetOfcFsofcMeterEntry == NULL)
    {
        OFC_TRC ((OFC_UTIL_TRC,
                  "OfcFsofcMeterTableCreatApi: pSetOfcFsofcMeterEntry is NULL.\r\n"));
        return NULL;
    }
    /* Allocate memory for the new node */
    pOfcFsofcMeterEntry =
        (tOfcFsofcMeterEntry *) MemAllocMemBlk (OFC_FSOFCMETERTABLE_POOLID);
    if (pOfcFsofcMeterEntry == NULL)
    {
        OFC_TRC ((OFC_UTIL_TRC,
                  "OfcFsofcMeterTableCreatApi: Fail to Allocate Memory.\r\n"));
        return NULL;
    }
    else
    {
        MEMCPY (pOfcFsofcMeterEntry, pSetOfcFsofcMeterEntry,
                sizeof (tOfcFsofcMeterEntry));
        if (RBTreeAdd
            (gOfcGlobals.OfcGlbMib.FsofcMeterTable,
             (tRBElem *) pOfcFsofcMeterEntry) != RB_SUCCESS)
        {
            OFC_TRC ((OFC_UTIL_TRC,
                      "OfcFsofcMeterTableCreatApi: Fail to Add the node.\r\n"));
            MemReleaseMemBlock (OFC_FSOFCMETERTABLE_POOLID,
                                (UINT1 *) pOfcFsofcMeterEntry);
            return NULL;
        }
        return pOfcFsofcMeterEntry;
    }
}

/****************************************************************************
 Function    :  FsOfcExactFlowEntryRBComp 
 Input       :  pRBElem1 - Pointer to the node1 for comparision
                pRBElem2 - Pointer to the node2 for comparision
 Description :  RBTree Compare function for ExactMatchEntry table
 Output      :  None
 Returns     :  1,-1 0
****************************************************************************/
PUBLIC INT4
FsOfcExactFlowEntryRBComp (tRBElem * pRBElem1, tRBElem * pRBElem2)
{
    tRBExactFlowEntry  *pExact1 = (tRBExactFlowEntry *) pRBElem1;
    tRBExactFlowEntry  *pExact2 = (tRBExactFlowEntry *) pRBElem2;

    if (MEMCMP (pExact1->au1ExactMatch, pExact2->au1ExactMatch,
                MAX_OFC_EXACT_LENGTH) > 0)
    {
        return 1;
    }

    if (MEMCMP (pExact1->au1ExactMatch, pExact2->au1ExactMatch,
                MAX_OFC_EXACT_LENGTH) < 0)
    {
        return -1;
    }

    /* matched */
    return 0;
}
