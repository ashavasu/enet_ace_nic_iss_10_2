/******************************************************************************
 * Copyright (C) 2013 Aricent Inc . All Rights Reserved
 *
 * $Id: ofcinband.c,v 1.2 2014/03/23 12:21:20 siva Exp $
 * 
 * Description: This file contains routines for InBand Controller Processing 
 ******************************************************************************/

#include "ofcdefn.h"
#include "ofcflow.h"
#include "ofcl.h"
#include "utilcli.h"
#include "ofcapi.h"
#include "ofcinband.h"

static tOfcInBandCntrlDb gInBandCntrlDb[OFC_MAX_CONTROLLERS];
extern INT4         CfaGddEthWrite (UINT1 *pu1Buf, UINT2 u2IfIndex,
                                    UINT4 u4Size);

/******************************************************************************
 * Function    : OfcInBandAddCntrlInDataBase
 * Description : This routine adds the InBand Controller Entry
 *               into the DataBase 
 * Input       : pCntrlEntry - Pointer to Controller Entry
 * Output      : None 
 * Returns     : OFC_SUCCESS or OFC_FAILURE
 ******************************************************************************/
UINT4
OfcInBandAddCntrlInDataBase (tOfcFsofcControllerConnEntry * pCntrlEntry)
{
    UINT1               u1Iter = OFC_ZERO;
    UINT1               au1TmpAddr[OFC_MAX_IP_LENGTH];

    OFC_TRC_FUNC ((OFC_FN_ENTRY, "FUNC:OfcInBandAddCntrlInDataBase\n"));

    /* Pick a free entry to store */
    for (u1Iter = OFC_ZERO; u1Iter < OFC_MAX_CONTROLLERS; u1Iter++)
    {
        if (gInBandCntrlDb[u1Iter].u1EntryState == OFCINBD_CNTRL_ENTRY_EXIST)
        {
            /* Check if this is already added */
            continue;
        }
        else
        {
            MEMSET (au1TmpAddr, OFC_ZERO, OFC_MAX_IP_LENGTH);
            CliStrToIp4Addr (pCntrlEntry->MibObject.au1FsofcControllerIpAddress,
                             au1TmpAddr);
            gInBandCntrlDb[u1Iter].u4CntrlIpAddr =
                INET_ADDR (((const char *) au1TmpAddr));
            gInBandCntrlDb[u1Iter].u1EntryState = OFCINBD_CNTRL_ENTRY_EXIST;

            return OFC_SUCCESS;
        }
    }
    OFC_TRC_FUNC ((OFC_FN_EXIT, "FUNC:OfcInBandAddCntrlInDataBase\n"));
    return OFC_FAILURE;
}

/******************************************************************************
 * Function    : OfcInBandDelCntrlInDataBase
 * Description : This routine deletes the InBand Controller Entry
 *               from the DataBase 
 * Input       : pCntrlEntry - Pointer to Controller Entry 
 * Output      : None
 * Returns     : OFC_SUCCESS or OFC_FAILURE
 ******************************************************************************/
UINT4
OfcInBandDelCntrlInDataBase (tOfcFsofcControllerConnEntry * pCntrlEntry)
{
    UINT4               u4CntrlIpAddr = OFC_ZERO;
    UINT1               u1Iter = OFC_ZERO;
    UINT1               au1TmpAddr[OFC_MAX_IP_LENGTH];

    OFC_TRC_FUNC ((OFC_FN_ENTRY, "FUNC:OfcInBandDelCntrlInDataBase\n"));

    MEMSET (au1TmpAddr, OFC_ZERO, OFC_MAX_IP_LENGTH);
    CliStrToIp4Addr (pCntrlEntry->MibObject.au1FsofcControllerIpAddress,
                     au1TmpAddr);
    u4CntrlIpAddr = INET_ADDR (((const char *) au1TmpAddr));

    for (u1Iter = OFC_ZERO; u1Iter < OFC_MAX_CONTROLLERS; u1Iter++)
    {
        if (gInBandCntrlDb[u1Iter].u1EntryState == OFCINBD_CNTRL_ENTRY_FREE)
        {
            continue;
        }
        else
        {
            if (gInBandCntrlDb[u1Iter].u4CntrlIpAddr == u4CntrlIpAddr)
            {
                gInBandCntrlDb[u1Iter].u4CntrlIpAddr = OFC_ZERO;
                gInBandCntrlDb[u1Iter].u4IfToCntrl = OFC_ZERO;
                gInBandCntrlDb[u1Iter].u1EntryState = OFCINBD_CNTRL_ENTRY_FREE;

                return OFC_SUCCESS;
            }
            else
            {
                /* continue searching */
            }
        }
    }
    OFC_TRC_FUNC ((OFC_FN_EXIT, "FUNC:OfcInBandDelCntrlInDataBase\n"));
    return OFC_FAILURE;
}

/*****************************************************************************
 *  Function Name   : OfcInBandVerifyCntrlIPAddress
 *  Description     : This routine verifys if the IPAddress passed in is
 *                    of one of the Controllers we are connected to.
 *  Input           : U4IpAddr - Ipv4 Address
 *  Output          : Pointer to Controller Entry in InBand Database 
 *  Returns         : OFC_SUCCESS or OFC_FAILURE 
 *****************************************************************************/
UINT4
OfcInBandVerifyCntrlIPAddress (UINT4 u4IpAddr, UINT1 *pu1EntryIndex)
{
    UINT1               u1Iter = OFC_ZERO;

    OFC_TRC_FUNC ((OFC_FN_ENTRY, "FUNC:OfcInBandVerifyCntrlIPAddress\n"));
    for (u1Iter = OFC_ZERO; u1Iter < OFC_MAX_CONTROLLERS; u1Iter++)
    {
        if (gInBandCntrlDb[u1Iter].u1EntryState == OFCINBD_CNTRL_ENTRY_FREE)
        {
            continue;
        }
        else
        {
            /* fall through */
        }

        if (gInBandCntrlDb[u1Iter].u4CntrlIpAddr == u4IpAddr)
        {
            *pu1EntryIndex = u1Iter;
            return OFC_SUCCESS;
        }
        else
        {
            /* continue checking */
        }
    }
    OFC_TRC_FUNC ((OFC_FN_EXIT, "FUNC:OfcInBandVerifyCntrlIPAddress\n"));
    return OFC_FAILURE;
}

/******************************************************************************
 * Function    : OfcInbandCntlrTxPktCheck 
 * Description : This routine checks if the transmitted packet is inband
 *               controller packet
 * Input       : u4IfIndex  - Interface Index
 *               pu1DataBuf - Pointer to the Data Packet
 * Output      : None 
 * Returns     : OFC_SUCCESS or OFC_FAILURE
 ******************************************************************************/
UINT4
OfcInbandCntlrTxPktCheck (UINT1 *pu1DataBuf, UINT4 u4PktSize)
{
    tOfcFsofcIfEntry   *pIfEntry = NULL;
    UINT4               u4IpAddress = OFC_ZERO;
    UINT4               u4RetVal = OFC_ZERO;
    UINT2               u2ProtoType = OFC_ZERO;
    UINT1               u1EntryIndex = OFC_ZERO;

    /* Fetch the protocol type */
    u2ProtoType =
        (UINT2) *(UINT2 *) ((VOID *) (pu1DataBuf + OFC_ETHTYPE_OFFSET));
    u2ProtoType = OSIX_NTOHS (u2ProtoType);
    if (u2ProtoType == OFC_L3_IP4)
    {
        /*
         * Check if the destination IP address matches one of the connected
         * controllers.
         */
        u4IpAddress = (UINT4) *(UINT4 *) ((VOID *) (pu1DataBuf +
                                                    OFC_ETH_HDR_LEN +
                                                    OFC_IPV4_DST_OFFSET));
        u4IpAddress = OSIX_NTOHL (u4IpAddress);

        u4RetVal = OfcInBandVerifyCntrlIPAddress (u4IpAddress, &u1EntryIndex);
        if (u4RetVal == OFC_FAILURE)
        {
            /*
             * No Controller exists with this IP,
             */
            return OFC_FAILURE;
        }
        CfaGddEthWrite (pu1DataBuf,
                        (UINT2) gInBandCntrlDb[u1EntryIndex].u4IfToCntrl,
                        u4PktSize);
    }
    else if (u2ProtoType == OFC_L3_ARP)
    {
        /*
         * Check if the destination IP address matches one of the connected
         * controllers.
         */
        u4IpAddress = (UINT4) *(UINT4 *) ((VOID *) (pu1DataBuf +
                                                    OFC_ETH_HDR_LEN +
                                                    OFC_ARP_TPA_OFFSET));
        u4IpAddress = OSIX_NTOHL (u4IpAddress);

        u4RetVal = OfcInBandVerifyCntrlIPAddress (u4IpAddress, &u1EntryIndex);
        if (u4RetVal == OFC_FAILURE)
        {
            /*
             * No Controller exists with this IP,
             */
            return OFC_FAILURE;
        }

        /* Flood on all openflow ports */
        pIfEntry = (tOfcFsofcIfEntry *)
            RBTreeGetFirst (gOfcGlobals.OfcGlbMib.FsofcIfTable);

        if (pIfEntry != NULL)
        {
            do
            {
                CfaGddEthWrite (pu1DataBuf,
                                (UINT2) pIfEntry->MibObject.i4FsofcIfIndex,
                                u4PktSize);
            }
            while ((pIfEntry =
                    (tOfcFsofcIfEntry *) RBTreeGetNext (gOfcGlobals.OfcGlbMib.
                                                        FsofcIfTable, pIfEntry,
                                                        NULL)) != NULL);
        }
    }
    else
    {
        return OFC_FAILURE;
    }
    return OFC_SUCCESS;
}

/******************************************************************************
 * Function    : OfcInbandCntlrRxPktCheck 
 * Description : This routine checks if the received packet is inband
 *               controller packet
 * Input       : pu4IfIndex  - Pointer to the Interface Index
 *               pu1DataBuf  - Pointer to the Data Packet
 * Output      : None 
 * Returns     : OFC_SUCCESS or OFC_FAILURE
 ******************************************************************************/
UINT4
OfcInbandCntlrRxPktCheck (UINT4 *pu4IfIndex, tCRU_BUF_CHAIN_HEADER * pBuf)
{
    UINT1              *pu1DataBuf = NULL;
    UINT4               u4RetVal = OFC_FAILURE;
    UINT4               u4SrcIpAddress = OFC_ZERO;
    UINT4               u4DstIpAddress = OFC_ZERO;
    UINT4               u4NetMask = OFC_ZERO;
    UINT4               u4BcastAddr = OFC_ZERO;
    UINT2               u2ProtoType = OFC_ZERO;
    UINT1               u1EntryIndex = OFC_ZERO;
    UINT1               au1DataBuf[OFC_FORTY_TWO];    /* for headers */

    MEMSET (au1DataBuf, OFC_ZERO, OFC_FORTY_TWO);
    if (pBuf == NULL)
    {
        return OFC_FAILURE;
    }

    pu1DataBuf = au1DataBuf;
    if (CRU_BUF_Copy_FromBufChain (pBuf, pu1DataBuf, OFC_ZERO, OFC_FORTY_TWO) ==
        CRU_FAILURE)
    {
        return OFC_FAILURE;
    }

    /* Fetch the protocol type */
    u2ProtoType =
        (UINT2) *(UINT2 *) ((VOID *) (pu1DataBuf + OFC_ETHTYPE_OFFSET));
    u2ProtoType = OSIX_NTOHS (u2ProtoType);
    if (u2ProtoType == OFC_L3_IP4)
    {
        u4SrcIpAddress = (UINT4) *(UINT4 *) ((VOID *) (pu1DataBuf +
                                                       OFC_ETH_HDR_LEN +
                                                       OFC_IPV4_SRC_OFFSET));
        u4DstIpAddress = (UINT4) *(UINT4 *) ((VOID *) (pu1DataBuf +
                                                       OFC_ETH_HDR_LEN +
                                                       OFC_IPV4_DST_OFFSET));
    }
    else if (u2ProtoType == OFC_L3_ARP)
    {
        u4SrcIpAddress = (UINT4) *(UINT4 *) ((VOID *) (pu1DataBuf +
                                                       OFC_ETH_HDR_LEN +
                                                       OFC_ARP_SPA_OFFSET));
        u4DstIpAddress = (UINT4) *(UINT4 *) ((VOID *) (pu1DataBuf +
                                                       OFC_ETH_HDR_LEN +
                                                       OFC_ARP_TPA_OFFSET));
    }
    u4SrcIpAddress = OSIX_NTOHL (u4SrcIpAddress);
    u4DstIpAddress = OSIX_NTOHL (u4DstIpAddress);

    /* Check if the frame is from any of the connected controllers */
    u4RetVal = OfcInBandVerifyCntrlIPAddress (u4SrcIpAddress, &u1EntryIndex);
    if (u4RetVal == OFC_FAILURE)
    {
        /*
         * No Controller exists with this IP,
         */
        return OFC_FAILURE;
    }

    /* update the port connected to controller */
    if (gInBandCntrlDb[u1EntryIndex].u4IfToCntrl == OFC_ZERO)
    {
        gInBandCntrlDb[u1EntryIndex].u4IfToCntrl = *pu4IfIndex;
    }

    /* get the port mapped to the destination IP */
    if (CfaIpIfGetIpAddressInfo (u4DstIpAddress, &u4NetMask,
                                 &u4BcastAddr, pu4IfIndex) == CFA_FAILURE)
    {
        /* log failure */
        return OFC_FAILURE;
    }
    return OFC_SUCCESS;
}
