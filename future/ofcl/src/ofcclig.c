/********************************************************************
* Copyright (C) 2013  Aricent Inc . All Rights Reserved
*
* $Id: ofcclig.c,v 1.2 2014/01/31 13:07:25 siva Exp $
*
* Description: This file contains the Ofc CLI related routines 
*********************************************************************/

#ifndef __OFCCLIG_C__
#define __OFCCLIG_C__

#include "ofcinc.h"
#include "ofccli.h"
#include "ofcvlan.h"

/****************************************************************************
 * Function    :  cli_process_Ofc_cmd
 * Description :  This function is exported to CLI module to handle the
                  OFC cli commands to take the corresponding action.

 * Input       :  Variable arguments

 * Output      :  None 

 * Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
cli_process_Ofc_cmd (tCliHandle CliHandle, UINT4 u4Command, ...)
{
    va_list             ap;
    UINT4              *args[OFC_CLI_MAX_ARGS];
    INT1                argno = OFC_ZERO;
    UINT4               u4ErrCode = OFC_ZERO;
    UINT4               u4IfIndex = OFC_ZERO;
    INT4                i4RetStatus = CLI_FAILURE;
    UINT4               u4CmdType = OFC_ZERO;
    UINT4               u4PortId = OFC_ZERO;
    INT4                i4Inst = OFC_ZERO;

    tOfcFsofcCfgEntry   OfcSetFsofcCfgEntry;
    tOfcIsSetFsofcCfgEntry OfcIsSetFsofcCfgEntry;

    tOfcFsofcControllerConnEntry OfcSetFsofcControllerConnEntry;
    tOfcIsSetFsofcControllerConnEntry OfcIsSetFsofcControllerConnEntry;

    tOfcFsofcIfEntry    OfcSetFsofcIfEntry;
    tOfcIsSetFsofcIfEntry OfcIsSetFsofcIfEntry;

    UNUSED_PARAM (u4CmdType);
    CliRegisterLock (CliHandle, OfcMainTaskLock, OfcMainTaskUnLock);
    OFC_LOCK;

    va_start (ap, u4Command);

    i4Inst = va_arg (ap, INT4);

    if (i4Inst != 0)
    {
        u4IfIndex = (UINT4) i4Inst;
    }
    UNUSED_PARAM (u4IfIndex);
    while (1)
    {
        args[argno++] = va_arg (ap, UINT4 *);
        if (argno == OFC_CLI_MAX_ARGS)
            break;
    }

    va_end (ap);

    u4PortId = (UINT4) CLI_GET_IFINDEX ();

    switch (u4Command)
    {
        case CLI_OFC_FSOFCCFGTABLE:
            MEMSET (&OfcSetFsofcCfgEntry, 0, sizeof (tOfcFsofcCfgEntry));
            MEMSET (&OfcIsSetFsofcCfgEntry, 0, sizeof (tOfcIsSetFsofcCfgEntry));

            OFC_FILL_FSOFCCFGTABLE_ARGS ((&OfcSetFsofcCfgEntry),
                                         (&OfcIsSetFsofcCfgEntry), args[0],
                                         args[1], args[2], args[3], args[4],
                                         args[5], args[6], args[7], args[8],
                                         args[9], args[10]);

            i4RetStatus =
                OfcCliSetFsofcCfgTable (CliHandle, (&OfcSetFsofcCfgEntry),
                                        (&OfcIsSetFsofcCfgEntry));
            break;

        case CLI_OFC_FSOFCCONTROLLERCONNTABLE:
            MEMSET (&OfcSetFsofcControllerConnEntry, 0,
                    sizeof (tOfcFsofcControllerConnEntry));
            MEMSET (&OfcIsSetFsofcControllerConnEntry, 0,
                    sizeof (tOfcIsSetFsofcControllerConnEntry));

            OFC_FILL_FSOFCCONTROLLERCONNTABLE_ARGS ((&OfcSetFsofcControllerConnEntry), (&OfcIsSetFsofcControllerConnEntry), args[0], args[1], args[2], args[3], args[4], args[5], args[6], args[7], args[8], args[9], args[10]);

            i4RetStatus =
                OfcCliSetFsofcControllerConnTable (CliHandle,
                                                   (&OfcSetFsofcControllerConnEntry),
                                                   (&OfcIsSetFsofcControllerConnEntry));
            break;

        case CLI_OFC_FSOFCIFTABLE:
            MEMSET (&OfcSetFsofcIfEntry, 0, sizeof (tOfcFsofcIfEntry));
            MEMSET (&OfcIsSetFsofcIfEntry, 0, sizeof (tOfcIsSetFsofcIfEntry));

            OFC_FILL_FSOFCIFTABLE_ARGS ((&OfcSetFsofcIfEntry),
                                        (&OfcIsSetFsofcIfEntry), args[0],
                                        args[1], args[2], args[3], args[4],
                                        args[5], args[6]);

            i4RetStatus =
                OfcCliSetFsofcIfTable (CliHandle, (&OfcSetFsofcIfEntry),
                                       (&OfcIsSetFsofcIfEntry));
            break;

        case CLI_OFC_PORT_MAP:
            i4RetStatus =
                OfcCliMapInterfaceToContext (CliHandle, u4PortId,
                                             ((UINT4 *) args[0]));
            break;

        case CLI_OFC_NO_PORT_MAP:
            i4RetStatus =
                OfcCliUnMapInterfaceFromContext (CliHandle, u4PortId,
                                                 ((UINT4 *) args[0]));
            break;

        case CLI_OFC_VLAN_MAP:
            if (OfcCliGetVlanIfIndex (u4PortId, &u4IfIndex) == CLI_SUCCESS)
            {
                i4RetStatus =
                    OfcCliMapInterfaceToContext (CliHandle, u4IfIndex,
                                                 ((UINT4 *) args[0]));
            }
            break;

        case CLI_OFC_NO_VLAN_MAP:
            if (OfcCliGetVlanIfIndex (u4PortId, &u4IfIndex) == CLI_SUCCESS)
            {
                i4RetStatus =
                    OfcCliUnMapInterfaceFromContext (CliHandle, u4IfIndex,
                                                     ((UINT4 *) args[0]));
            }
            break;

        default:
            break;

    }
    if ((i4RetStatus == CLI_FAILURE)
        && (CLI_GET_ERR (&u4ErrCode) == CLI_SUCCESS))
    {
        if ((u4ErrCode > 0) && (u4ErrCode < CLI_OFC_MAX_ERR))
        {
            CliPrintf (CliHandle, "\r%s", OfcCliErrString[u4ErrCode]);
        }
        CLI_SET_ERR (0);
    }
    CLI_SET_CMD_STATUS ((UINT4) i4RetStatus);

    CliUnRegisterLock (CliHandle);

    OFC_UNLOCK;

    return i4RetStatus;

}

/****************************************************************************
* Function    :  OfcCliSetFsofcCfgTable
* Description :
* Input       :  CliHandle 
*            pOfcSetFsofcCfgEntry
*            pOfcIsSetFsofcCfgEntry
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
OfcCliSetFsofcCfgTable (tCliHandle CliHandle,
                        tOfcFsofcCfgEntry * pOfcSetFsofcCfgEntry,
                        tOfcIsSetFsofcCfgEntry * pOfcIsSetFsofcCfgEntry)
{
    UINT4               u4ErrorCode;

    if (OfcTestAllFsofcCfgTable (&u4ErrorCode, pOfcSetFsofcCfgEntry,
                                 pOfcIsSetFsofcCfgEntry, OSIX_TRUE,
                                 OSIX_TRUE) != OSIX_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (OfcSetAllFsofcCfgTable (pOfcSetFsofcCfgEntry, pOfcIsSetFsofcCfgEntry,
                                OSIX_TRUE, OSIX_TRUE) != OSIX_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/****************************************************************************
* Function    :  OfcCliSetFsofcControllerConnTable
* Description :
* Input       :  CliHandle 
*            pOfcSetFsofcControllerConnEntry
*            pOfcIsSetFsofcControllerConnEntry
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
OfcCliSetFsofcControllerConnTable (tCliHandle CliHandle,
                                   tOfcFsofcControllerConnEntry *
                                   pOfcSetFsofcControllerConnEntry,
                                   tOfcIsSetFsofcControllerConnEntry *
                                   pOfcIsSetFsofcControllerConnEntry)
{
    UINT4               u4ErrorCode;

    if (OfcTestAllFsofcControllerConnTable
        (&u4ErrorCode, pOfcSetFsofcControllerConnEntry,
         pOfcIsSetFsofcControllerConnEntry, OSIX_TRUE,
         OSIX_TRUE) != OSIX_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (OfcSetAllFsofcControllerConnTable
        (pOfcSetFsofcControllerConnEntry, pOfcIsSetFsofcControllerConnEntry,
         OSIX_TRUE, OSIX_TRUE) != OSIX_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/****************************************************************************
* Function    :  OfcCliSetFsofcIfTable
* Description :
* Input       :  CliHandle 
*            pOfcSetFsofcIfEntry
*            pOfcIsSetFsofcIfEntry
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
OfcCliSetFsofcIfTable (tCliHandle CliHandle,
                       tOfcFsofcIfEntry * pOfcSetFsofcIfEntry,
                       tOfcIsSetFsofcIfEntry * pOfcIsSetFsofcIfEntry)
{
    UINT4               u4ErrorCode;

    if (OfcTestAllFsofcIfTable (&u4ErrorCode, pOfcSetFsofcIfEntry,
                                pOfcIsSetFsofcIfEntry) != OSIX_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (OfcSetAllFsofcIfTable (pOfcSetFsofcIfEntry, pOfcIsSetFsofcIfEntry) !=
        OSIX_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/****************************************************************************
 * Function    :  OfcCliMapInterfaceToContext
 * Description :
 * Input       :  CliHandle
 *                u4IfIndex
 *                pu4ContextID
 * Output      :  None
 * Returns     :  CLI_SUCCESS/CLI_FAILURE
 ***************************************************************************/
INT4
OfcCliMapInterfaceToContext (tCliHandle CliHandle, UINT4 u4IfIndex,
                             UINT4 *pu4ContextID)
{
    UINT4               u4ErrorCode = OFC_ZERO;

    if (nmhTestv2FsofcIfContextId (&u4ErrorCode, OFC_DEFAULT_CONTEXT,
                                   (INT4) u4IfIndex, *pu4ContextID) !=
        SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsofcIfContextId (OFC_DEFAULT_CONTEXT, (INT4) u4IfIndex,
                                *pu4ContextID) != SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/****************************************************************************
 * Function    :  OfcCliUnMapInterfaceFromContext
 * Description :
 * Input       :  CliHandle
 *                u4IfIndex
 *                pu4ContextID
 * Output      :  None
 * Returns     :  CLI_SUCCESS/CLI_FAILURE
 ***************************************************************************/
INT4
OfcCliUnMapInterfaceFromContext (tCliHandle CliHandle, UINT4 u4IfIndex,
                                 UINT4 *pu4ContextID)
{
    UINT4               u4ErrorCode = OFC_ZERO;

    if (nmhTestv2FsofcIfContextId (&u4ErrorCode, *pu4ContextID,
                                   (INT4) u4IfIndex, OFC_INVALID_CONTEXT) !=
        SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsofcIfContextId (*pu4ContextID, (INT4) u4IfIndex,
                                OFC_INVALID_CONTEXT) != SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/****************************************************************************
 * Function    :  OfcCliGetVlanIfIndex
 * Description :
 * Input       :  CliHandle
 *                u4PortId
 *                pu4IfIndex
 * Output      :  None
 * Returns     :  CLI_SUCCESS/CLI_FAILURE
 ***************************************************************************/
INT4
OfcCliGetVlanIfIndex (UINT4 u4PortId, UINT4 *pu4IfIndex)
{
    UINT1               au1IfName[CFA_MAX_IFALIAS_LENGTH];
    UINT1              *pu1Alias = NULL;
    tOfcFsofcIfEntry   *pOfcGetFsofcIfEntry = NULL;

    pu1Alias = au1IfName;
    STRCPY (au1IfName, OPENFLOW_VLAN_ALIAS_PREFIX);
    SPRINTF ((CHR1 *) pu1Alias, "%s%u", au1IfName, u4PortId);

    pOfcGetFsofcIfEntry = RBTreeGetFirst (gOfcGlobals.OfcGlbMib.FsofcIfTable);

    do
    {
        if (pOfcGetFsofcIfEntry == NULL)
        {
            continue;
        }

        if (MEMCMP (pOfcGetFsofcIfEntry->MibObject.au1FsofcIfAlias,
                    pu1Alias, STRLEN (pu1Alias)) == 0)
        {
            *pu4IfIndex = (UINT4) pOfcGetFsofcIfEntry->MibObject.i4FsofcIfIndex;
            return CLI_SUCCESS;
        }
    }
    while ((pOfcGetFsofcIfEntry =
            RBTreeGetNext (gOfcGlobals.OfcGlbMib.FsofcIfTable,
                           pOfcGetFsofcIfEntry, NULL)) != NULL);
    return CLI_FAILURE;
}

#endif
