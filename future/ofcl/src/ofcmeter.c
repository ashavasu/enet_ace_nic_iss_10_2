/*****************************************************************************/
/* Copyright (C) 2013 Aricent Inc . All Rights Reserved
 * ****************************************************************************
 *    FILE  NAME             : ofcmeter.c                                      
 *    PRINCIPAL AUTHOR       : Aricent Inc.                                                  
 *    SUBSYSTEM NAME         : OFC                                              
 *    MODULE NAME            : OFC-METER TABLE                                  
 *    LANGUAGE               : ANSI-C                                          
 *    TARGET ENVIRONMENT     : Linux (Portable)                                                        
 *    DATE OF FIRST RELEASE  :                                                 
 *    DESCRIPTION            : This file contains routines for METER table     
 *                             implementations.                                                              
* $Id: ofcmeter.c,v 1.5 2014/04/07 12:14:05 siva Exp $
 *----------------------------------------------------------------------------*/
#include"ofcflow.h"
#include"ofcapi.h"
#include"ofcmeter.h"
#include"ofcsz.h"
#ifdef NPAPI_WANTED
#include"ofcnp.h"
#endif

PRIVATE INT4        Ofc131MeterAddMeterEntry (tOfcFsofcMeterEntry *
                                              pMeterEntry);
PRIVATE UINT4       Ofc131MeterModifyMeterEntry (tOfcFsofcMeterEntry *
                                                 pMeterEntry);
PRIVATE UINT4       Ofc131MeterDeleteMeterEntry (tOfcFsofcMeterEntry *
                                                 pMeterEntry);
PRIVATE UINT4       Ofc131MeterDeleteAllMeterEntry (UINT4 u4ContextId);
PRIVATE VOID        Ofc131MeterMemRelease (tOfcFsofcMeterEntry * pMeterEntry);
PRIVATE VOID        Ofc131MeterDeleteFlows (tOfcFsofcMeterEntry *
                                            pOfcMeterEntry);

/*****************************************************************************
* Function Name : Ofc131MeterMemReleaseBands                                        
* Description   : Function to handle the Deletion of the MeterBands          
*                                                                             
* Input(s)      : *pBandList - SLL to MeterBands                   
* Output(s)     : None                                                      
* Return(s)     : VOID                           
*****************************************************************************/
PUBLIC VOID
Ofc131MeterMemReleaseBands (tOfcSll * pBandList)
{
    tOfcMeterBand      *pOfcMeterBand = NULL;
    tOfcMeterBand      *pOfcMeterNext = NULL;

    OFC_TRC_FUNC ((OFC_FN_ENTRY, "FUNC:Ofc131MeterMemReleaseBands\n"));

    TMO_DYN_SLL_Scan (pBandList, pOfcMeterBand, pOfcMeterNext, tOfcMeterBand *)
    {
        TMO_SLL_Delete (pBandList, (tTMO_SLL_NODE *) pOfcMeterBand);
        MemReleaseMemBlock (OFC_METER_BAND_POOLID, (UINT1 *) pOfcMeterBand);
        if (NULL == pOfcMeterNext)
        {
            break;
        }
    }
    OFC_TRC_FUNC ((OFC_FN_EXIT, "FUNC:Ofc131MeterMemReleaseBands\n"));
}

/*****************************************************************************
* Function Name : Ofc131MeterMemRelease                                        
* Description   : Function to handle deletion of Meter Table          
*                                                                             
* Input(s)      : *pOfcMeterEntry -Pointer to the Meter Table                   
* Output(s)     : None                                                      
* Return(s)     : VOID                  
*****************************************************************************/
PRIVATE VOID
Ofc131MeterMemRelease (tOfcFsofcMeterEntry * pMeterEntry)
{

    OFC_TRC_FUNC ((OFC_FN_ENTRY, "FUNC:Ofc131MeterMemRelease\n"));

    Ofc131MeterMemReleaseBands (&pMeterEntry->BandList);
    MemReleaseMemBlock (OFC_FSOFCMETERTABLE_POOLID, (UINT1 *) pMeterEntry);

    OFC_TRC_FUNC ((OFC_FN_EXIT, "FUNC:Ofc131MeterMemRelease\n"));
}

/*****************************************************************************
* Function Name : Ofc131MeterHandleMeterModMsgs                                        
* Description   : Function to handle the MeterModify request messages          
*                                                                             
* Input(s)      : *pOfcMeterMod - Open Flow Client Instance                   
* Output(s)     : None                                                      
* Return(s)     : OFC_FAILURE/OFC_SUCCESS                                   
*****************************************************************************/
PUBLIC VOID
Ofc131MeterHandleMeterModMsgs (tOfcFsofcCfgEntry * pOfc,
                               tOfcFsofcMeterEntry * pMeterEntry, UINT2 u2cmd)
{
    tOfpErrMsg          OfpErrorMsg;

    OFC_TRC_FUNC ((OFC_FN_ENTRY, "FUNC:Ofc131MeterHandleMeterModMsgs\n"));

    UNUSED_PARAM (pOfc);
    MEMSET (&OfpErrorMsg, OFC_ZERO, sizeof (tOfpErrMsg));

    switch (u2cmd)
    {
        case OFPMC_ADD:
            Ofc131MeterAddMeterEntry (pMeterEntry);
            break;
        case OFPMC_MODIFY:
            Ofc131MeterModifyMeterEntry (pMeterEntry);
            break;
        case OFPMC_DELETE:
            Ofc131MeterDeleteMeterEntry (pMeterEntry);
            break;
        default:
            OFC_TRC_FUNC ((OFC_METER_TRC,
                           "FUNC:OfcMeterHandleMeterModMsgs:Default: "
                           "Sending Error with bad command option,"
                           "Returning FAILURE\n"));

            OfpErrorMsg.u2Type = OFPET_131_METER_MOD_FAILED;
            OfpErrorMsg.u2Code = OFPMMFC_BAD_COMMAND;
            OfcErrMsgPktSend (&OfpErrorMsg, NULL);
            Ofc131MeterMemRelease (pMeterEntry);
            break;
    }
    OFC_TRC_FUNC ((OFC_FN_EXIT, "FUNC:Ofc131MeterHandleMeterModMsgs\n"));
}

/*****************************************************************************
* Function Name : ofc131MeterAddMeterEntry                                     
* Description   : Function to handle the MeterAdd messages           
*                                                                           
* Input(s)      : *pOfcMeterMod - Open Flow Client Instance                 
* Output(s)     : None                                                      
* Return(s)     : OFC_FAILURE/OFC_SUCCESS                                   
*****************************************************************************/
PRIVATE INT4
Ofc131MeterAddMeterEntry (tOfcFsofcMeterEntry * pMeterEntry)
{
    static tOfcFsofcMeterEntry OfcFsofcMeterEntry;
    tOfcFsofcMeterEntry *pOfcFsofcMeterEntry = &OfcFsofcMeterEntry;
#ifdef NPAPI_WANTED
    tOfcHwInfo          OfcHwInfo;
#endif
    CHR1                au1ActionString[OFC_MAX_ACTION_STR_LEN];
    INT4                i4RetValue = OFC_ZERO;
    tOfpErrMsg          OfpErrorMsg;

    OFC_TRC_FUNC ((OFC_FN_ENTRY, "FUNC:Ofc131MeterAddMeterEntry\n"));

    MEMSET (&OfpErrorMsg, OFC_ZERO, sizeof (tOfpErrMsg));
    MEMSET (&au1ActionString, OFC_ZERO, OFC_MAX_ACTION_STR_LEN);
    MEMSET (&OfcFsofcMeterEntry, OFC_ZERO, sizeof (tOfcFsofcMeterEntry));
#ifdef NPAPI_WANTED
    MEMSET (&OfcHwInfo, OFC_ZERO, sizeof (tOfcHwInfo));
#endif

    pOfcFsofcMeterEntry->MibObject.u4FsofcMeterIndex =
        pMeterEntry->MibObject.u4FsofcMeterIndex;
    pOfcFsofcMeterEntry->MibObject.u4FsofcContextId =
        pMeterEntry->MibObject.u4FsofcContextId;

    i4RetValue = OfcGetAllFsofcMeterTable (pOfcFsofcMeterEntry);
    if (OSIX_SUCCESS == i4RetValue)
    {
        OfpErrorMsg.u2Type = OFPET_131_METER_MOD_FAILED;
        OfpErrorMsg.u2Code = OFPMMFC_METER_EXISTS;
        i4RetValue = OfcErrMsgPktSend (&OfpErrorMsg, NULL);

        OFC_TRC_FUNC ((OFC_METER_TRC,
                       "FUNC:Ofc131MeterAddMeterEntry: Send Error Message to Controller"
                       "Success, through Pkt Tx/Rx API\n"));

        Ofc131MeterMemRelease (pMeterEntry);
        return OFC_FAILURE;
    }

    i4RetValue =
        Ofc131MeterBandListToStr (au1ActionString, &(pMeterEntry->BandList));
    if (OFC_FAILURE == i4RetValue)
    {
        OFC_TRC_FUNC ((OFC_METER_TRC, "FUNC:Ofc131MeterAddMeterEntry: "
                       "Action values corrupted\n"));
        OfpErrorMsg.u2Type = OFPET_131_METER_MOD_FAILED;
        OfpErrorMsg.u2Code = OFPMMFC_BAD_BAND_VALUE;
        i4RetValue = OfcErrMsgPktSend (&OfpErrorMsg, NULL);
        Ofc131MeterMemRelease (pMeterEntry);
        return OFC_FAILURE;
    }

    MEMCPY (&(pMeterEntry->MibObject.au1FsofcMeterBandInfo), au1ActionString,
            OFC_MAX_ACTION_STR_LEN);
    pMeterEntry->MibObject.i4FsofcMeterBandInfoLen = OFC_MAX_ACTION_STR_LEN;
    pMeterEntry->MibObject.u4FsofcMeterDurationSec = OsixGetSysUpTime ();

    if (RBTreeAdd
        (gOfcGlobals.OfcGlbMib.FsofcMeterTable,
         (tRBElem *) pMeterEntry) != RB_SUCCESS)
    {
        OFC_TRC_FUNC ((OFC_METER_TRC,
                       "FUNC:Ofc131MeterAddMeterEntry: Error in updating the RBTRee\n"));

        OfpErrorMsg.u2Type = OFPET_131_METER_MOD_FAILED;
        OfpErrorMsg.u2Code = OFPMMFC_OUT_OF_METERS;
        i4RetValue = OfcErrMsgPktSend (&OfpErrorMsg, NULL);
        Ofc131MeterMemRelease (pMeterEntry);

        OFC_TRC_FUNC ((OFC_METER_TRC,
                       "FUNC:Ofc131MeterAddMeterEntry: Send Error Message to"
                       " Controller Success, through Pkt Tx/Rx API\n"));
        return OFC_FAILURE;
    }

    OFC_TRC_FUNC ((OFC_METER_TRC,
                   "FUNC:Ofc131MeterAddMeterEntry: Invoking NPAPI for ADD\n"));
#ifdef NPAPI_WANTED
    OfcHwInfo.OfcCmd = OFC_NP_METER_ADD;
    i4RetValue = OfcFsNpHwConfigOfcInfo (&OfcHwInfo);
    if (i4RetValue != FNP_SUCCESS)
    {
        OFC_TRC_FUNC ((OFC_METER_TRC,
                       "FUNC:OfcFsNpHwConfigOfcInfo: NPAPI FAILED in ADD\n"));
    }
#endif

    OFC_TRC_FUNC ((OFC_FN_EXIT, "FUNC:Ofc131MeterAddMeterEntry\n"));

    return OFC_SUCCESS;
}

/*****************************************************************************
* Function Name : Ofc131MeterModifyMeterEntry                                      
* Description   : Function to handle the Meter Modify messages           
*                                                                           
* Input(s)      : pMeterEntry->pointer to the Meter Entry            
*                           
* Output(s)     : None                                                      
* Return(s)     : OFC_FAILURE/OFC_SUCCESS                                   
*****************************************************************************/
PRIVATE UINT4
Ofc131MeterModifyMeterEntry (tOfcFsofcMeterEntry * pMeterEntry)
{
    static tOfcFsofcMeterEntry OfcFsofcMeterEntry;
    tOfcFsofcMeterEntry *pOfcFsofcMeterEntry = NULL;
    CHR1                au1ActionString[OFC_MAX_ACTION_STR_LEN];
    INT4                i4RetValue = OFC_ZERO;
    tOfpErrMsg          OfpErrorMsg;

    OFC_TRC_FUNC ((OFC_FN_ENTRY, "FUNC:Ofc131MeterModifyMeterEntry\n"));

    MEMSET (&OfpErrorMsg, OFC_ZERO, sizeof (tOfpErrMsg));

    MEMSET (&OfcFsofcMeterEntry, OFC_ZERO, sizeof (tOfcFsofcMeterEntry));
    MEMSET (&au1ActionString, OFC_ZERO, sizeof (au1ActionString));

    OfcFsofcMeterEntry.MibObject.u4FsofcContextId =
        pMeterEntry->MibObject.u4FsofcContextId;
    OfcFsofcMeterEntry.MibObject.u4FsofcMeterIndex =
        pMeterEntry->MibObject.u4FsofcMeterIndex;

    pOfcFsofcMeterEntry = RBTreeGet (gOfcGlobals.OfcGlbMib.FsofcMeterTable,
                                     (tRBElem *) & (OfcFsofcMeterEntry));
    if (NULL == pOfcFsofcMeterEntry)
    {
        OFC_TRC_FUNC ((OFC_METER_TRC, "FUNC:Ofc131MeterModifyMeterEntry: "
                       "Meter Does not Exists\n"));

        OfpErrorMsg.u2Type = OFPET_131_METER_MOD_FAILED;
        OfpErrorMsg.u2Code = OFPMMFC_UNKNOWN_METER;
        i4RetValue = OfcErrMsgPktSend (&OfpErrorMsg, NULL);

        /* Releasing the Memory */
        Ofc131MeterMemRelease (pMeterEntry);
        return OFC_SUCCESS;
    }
    else
    {
        OFC_TRC_FUNC ((OFC_METER_TRC, "FUNC:Ofc131MeterModifyMeterEntry: "
                       "Deleting the previously existing the entry\n"));
        RBTreeRem (gOfcGlobals.OfcGlbMib.FsofcMeterTable, pOfcFsofcMeterEntry);
        Ofc131MeterMemRelease (pOfcFsofcMeterEntry);
    }

    i4RetValue = Ofc131MeterAddMeterEntry (pMeterEntry);
    if (OFC_FAILURE == i4RetValue)
    {
        OfpErrorMsg.u2Type = OFPET_131_METER_MOD_FAILED;
        OfpErrorMsg.u2Code = OFPMMFC_UNKNOWN_METER;
        i4RetValue = OfcErrMsgPktSend (&OfpErrorMsg, NULL);

        OFC_TRC_FUNC ((OFC_METER_TRC, "FUNC:Ofc131MeterModifyMeterEntry: "
                       "Updating in the RBTree Fails\n"));
        return OFC_SUCCESS;
    }

    OFC_TRC_FUNC ((OFC_FN_EXIT, "FUNC:Ofc131MeterModifyMeterEntry\n"));
    return OFC_SUCCESS;
}

/*****************************************************************************
* Function Name : ofc131MeterDeleteMeterEntry                                         
* Description   : Function to handle the Meter Delete messages        
*                                                                          
* Input(s)      : *pMeterEntry - MeterEntry Message         
* Output(s)     : None                                                     
* Return(s)     : OFC_SUCCESS                                   
*****************************************************************************/
PRIVATE UINT4
Ofc131MeterDeleteMeterEntry (tOfcFsofcMeterEntry * pMeterEntry)
{
    static tOfcFsofcMeterEntry OfcFsofcMeterEntry;
    tOfcFsofcMeterEntry *pOfcFsofcMeterEntry = NULL;
#ifdef NPAPI_WANTED
    INT4                i4RetValue = OFC_ZERO;
    tOfcHwInfo          OfcHwInfo;
#endif

    OFC_TRC_FUNC ((OFC_FN_ENTRY, "FUNC:Ofc131MeterDeleteMeterEntry\n"));

    MEMSET (&OfcFsofcMeterEntry, OFC_ZERO, sizeof (tOfcFsofcMeterEntry));
#ifdef NPAPI_WANTED
    MEMSET (&OfcHwInfo, OFC_ZERO, sizeof (tOfcHwInfo));
#endif

    if (OFPM_ALL == pMeterEntry->MibObject.u4FsofcMeterIndex)
    {
        Ofc131MeterDeleteAllMeterEntry (pMeterEntry->MibObject.
                                        u4FsofcContextId);
    }
    else
    {
        OfcFsofcMeterEntry.MibObject.u4FsofcContextId =
            pMeterEntry->MibObject.u4FsofcContextId;
        OfcFsofcMeterEntry.MibObject.u4FsofcMeterIndex =
            pMeterEntry->MibObject.u4FsofcMeterIndex;

        /* Call the routine to delete the flows whichever has meter instruction,
           maps to the current MeterIndex */

        pOfcFsofcMeterEntry = RBTreeGet (gOfcGlobals.OfcGlbMib.FsofcMeterTable,
                                         (tRBElem *) & (OfcFsofcMeterEntry));
        if (NULL != pOfcFsofcMeterEntry)
        {
#ifdef NPAPI_WANTED
            OfcHwInfo.OfcCmd = OFC_NP_METER_DELETE;
            i4RetValue = OfcFsNpHwConfigOfcInfo (&OfcHwInfo);
            if (i4RetValue != FNP_SUCCESS)
            {
                OFC_TRC_FUNC ((OFC_METER_TRC,
                               "FUNC:OfcFsNpHwConfigOfcInfo: NPAPI FAILED in DELETE\n"));
            }
#endif
            Ofc131MeterDeleteFlows (&OfcFsofcMeterEntry);
            RBTreeRem (gOfcGlobals.OfcGlbMib.FsofcMeterTable,
                       pOfcFsofcMeterEntry);
            Ofc131MeterMemRelease (pOfcFsofcMeterEntry);

            OFC_TRC_FUNC ((OFC_METER_TRC, "FUNC:Ofc131MeterDeleteMeterEntry: "
                           "Invoking NPAPI for DELETE"));

        }
    }

    Ofc131MeterMemRelease (pMeterEntry);
    OFC_TRC_FUNC ((OFC_FN_EXIT, "FUNC:Ofc131MeterDeleteMeterEntry\n"));
    return OFC_SUCCESS;
}

/*****************************************************************************
 * * Function Name : Ofc131MeterDeleteAllMeterEntry
 * * Description   : Function to handle the Meter Delete messages
 * *
 * * Input(s)      : UINT4 u4ContextId - Context  to which the groupMsgs belongs
 * *                          
 * * Output(s)     : None
 * * Return(s)     : OFC_SUCCESS/OFC_FAILURE
 * **************************************************************************/
PRIVATE UINT4
Ofc131MeterDeleteAllMeterEntry (UINT4 u4ContextId)
{
    tOfcFsofcMeterEntry *pOfcCurMeterEntry = NULL;
    static tOfcFsofcMeterEntry OfcFsofcMeterEntry;
#ifdef NPAPI_WANTED
    INT4                i4RetValue = OFC_ZERO;
    tOfcHwInfo          OfcHwInfo;
#endif

    OFC_TRC_FUNC ((OFC_FN_ENTRY, "FUNC:Ofc131MeterDeleteAllMeterEntry\n"));

    MEMSET (&OfcFsofcMeterEntry, OFC_ZERO, sizeof (OfcFsofcMeterEntry));
#ifdef NPAPI_WANTED
    MEMSET (&OfcHwInfo, OFC_ZERO, sizeof (tOfcHwInfo));
#endif

    OfcFsofcMeterEntry.MibObject.u4FsofcContextId = u4ContextId;
    OfcFsofcMeterEntry.MibObject.u4FsofcMeterIndex = OFPM_ALL;

    /* since Meterindex is OFPM_ALL,all the flows which contain meter instruction should be deleted */
    Ofc131MeterDeleteFlows (&OfcFsofcMeterEntry);

    while ((pOfcCurMeterEntry = OfcGetFirstFsofcMeterTable ()) != NULL)
    {
#ifdef NPAPI_WANTED
        OfcHwInfo.OfcCmd = OFC_NP_METER_DELETE;
        i4RetValue = OfcFsNpHwConfigOfcInfo (&OfcHwInfo);
        if (i4RetValue != FNP_SUCCESS)
        {
            OFC_TRC_FUNC ((OFC_METER_TRC,
                           "FUNC:OfcFsNpHwConfigOfcInfo: NPAPI FAILED in DELETE\n"));
        }
#endif

        RBTreeRem (gOfcGlobals.OfcGlbMib.FsofcMeterTable, pOfcCurMeterEntry);
        Ofc131MeterMemRelease (pOfcCurMeterEntry);

        OFC_TRC_FUNC ((OFC_METER_TRC, "FUNC:Ofc131MeterDeleteAllMeterEntry: "
                       "Invoking NPAPI for DELETE\n"));
    }
    OFC_TRC_FUNC ((OFC_FN_EXIT, "FUNC:Ofc131MeterDeleteAllMeterEntry\n"));
    return OFC_SUCCESS;
}

/*****************************************************************************
 * * * Function Name : Ofc131MeterDeleteFlows
 * * * Description   : Function to handle the flows which contain particular 
 * * *                 meter id as  instruction
 * * *
 * * * Input(s)      : PMeterEntry->pointe to the MeterMsgs ,
 * * *
 * * * Output(s)     : None
 * * * Return(s)     : OFC_SUCCESS/OFC_FAILURE
 * **************************************************************************/

VOID
Ofc131MeterDeleteFlows (tOfcFsofcMeterEntry * pOfcMeterEntry)
{
    tOfcFsofcFlowEntry *pCurFlowEntry = NULL;
    tOfcFsofcCfgEntry  *pOfcFsofcCfgEntry = NULL;
    tOfcInstr          *pInstr = NULL;

    OFC_TRC_FUNC ((OFC_FN_ENTRY, "FUNC:Ofc131MeterDeleteFlows\n"));

    pCurFlowEntry = (tOfcFsofcFlowEntry *)
        RBTreeGetFirst (gOfcGlobals.OfcGlbMib.FsofcFlowTable);

    pOfcFsofcCfgEntry =
        OfcGetFsofcCfgEntry (pOfcMeterEntry->MibObject.u4FsofcContextId);
    if (pOfcFsofcCfgEntry == NULL)
    {
        OFC_TRC_FUNC ((OFC_METER_TRC, "\r\nOfc131MeterDeleteFlows: "
                       "Get CFG Entry Failed\n"));
        return;
    }

    while (NULL != pCurFlowEntry)
    {
        TMO_SLL_Scan (&pCurFlowEntry->InstrList, pInstr, tOfcInstr *)
        {
            if ((pInstr->u2Type == OFCIT_METER))
            {
                /*
                 * Verify whether METER_MOD mesasage's MeterIndex is similar
                 * to the FlowEntries MeterInstruction Index or when MeterMod's
                 * MeterIndex is OFPM_ALL, then All the flow Entries contain
                 * Meter Instruction be deleted.
                 */
                if ((OSIX_NTOHL (pInstr->Instr.Meter.u4MeterId) ==
                     pOfcMeterEntry->MibObject.u4FsofcMeterIndex) ||
                    (OSIX_NTOHL (pInstr->Instr.Meter.u4MeterId) == OFPM_ALL))
                {
                    /* Delete the flow entries from the flowtable whichever
                     * has instruction maps to the current meter index
                     */
                    Ofc131FlowDeleteFlow (pOfcFsofcCfgEntry, pCurFlowEntry,
                                          OFPRR_DELETE);
                }
            }
        }

        pCurFlowEntry = (tOfcFsofcFlowEntry *)
            RBTreeGetNext (gOfcGlobals.OfcGlbMib.FsofcFlowTable,
                           (tRBElem *) pCurFlowEntry, NULL);
    }                            /* End of While loop */

    OFC_TRC_FUNC ((OFC_FN_EXIT, "FUNC:Ofc131MeterDeleteFlows\n"));
}
