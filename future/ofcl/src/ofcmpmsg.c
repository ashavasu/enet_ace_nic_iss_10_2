/*****************************************************************************
 * Copyright (C) 2013 Aricent Inc . All Rights Reserved
 *
 * $Id: ofcmpmsg.c,v 1.13 2016/03/10 11:42:32 siva Exp $
 * 
 * Description: This file contains the routines for processing
 *              Multipart messages. 
 *****************************************************************************/

#include "ofcmpmsg.h"
#include "ofcflow.h"
#include "ofcmeter.h"
#include "ofcdefn.h"
#include "ofcapi.h"
#include "ofcacts.h"
#include "ofcsz.h"

#if defined (NPAPI_WANTED) && defined (OPENFLOW_TCAM)
#ifdef DPA_WANTED
#include "ofc131dpa.h"
#endif /* DPA_WANTED */
#endif /* NPAPI_WANTED && OPENFLOW_TCAM */

/*****************************************************************************
 * Function Name : Ofc131MultipartPortDesc
 * Description   : This routine sends a port description multi part reply to
 *                 the controller.
 * Input(s)      : pMultiPartReq       - MultiPart Request
 *                 pConnPtr            - Connection Pointer
 *                 pBuf                - CRU Buffer Pointer
 *                 pu4Error            - Pointer to Error type
 * Output(s)     : None
 * Return(s)     : OSIX_SUCCESS OR OSIX_FAILURE
 ****************************************************************************/
INT4
Ofc131MultipartPortDesc (tOfp131MultiPartReq * pMultipartReq,
                         VOID *pConnPtr, tCRU_BUF_CHAIN_HEADER * pBuf,
                         UINT4 *pu4Error)
{
    tOfcPortDescReply  *pPortDescReply = NULL;
    tOfp131MultiPartRply *pMultiPartRply = NULL;
    tOfcFsofcIfEntry   *pIfEntry = NULL;
    UINT4               u4Index = OFC_ZERO;
    UINT4               u4VarLen = OFC_ZERO;

    OFC_TRC_FUNC ((OFC_FN_ENTRY, "FUNC:Ofc131MultipartPortDesc\n"));

    pMultiPartRply =
        (tOfp131MultiPartRply *) MemAllocMemBlk (OFC_MULTIPART_POOLID);

    if (pMultiPartRply == NULL)
    {
        OFC_TRC_FUNC ((OFC_ACTS_TRC, "Memory Allocation Failed\n"));
        return OFC_FAILURE;
    }

    MEMSET(pMultiPartRply, OFC_ZERO, OFC_MAX_MULTIPART_MSG_SIZE);
    pPortDescReply = (tOfcPortDescReply *) ((VOID *) (pMultiPartRply->au1Body));

    pMultiPartRply->u2Type = pMultipartReq->u2Type;
    pMultiPartRply->u2Flags = pMultipartReq->u2Flags;

#if defined (NPAPI_WANTED) && defined (OPENFLOW_TCAM)
#ifdef DPA_WANTED
    if (Ofc131DpaPortDescGet (pPortDescReply, &u4VarLen) != OFC_SUCCESS)
    {
        Ofc131PktMultiPartRplySend (pMultiPartRply, pConnPtr, u4VarLen);
        MemReleaseMemBlock (OFC_MULTIPART_POOLID, (UINT1 *) pMultiPartRply);
        return OFC_FAILURE;
    }
    Ofc131PktMultiPartRplySend (pMultiPartRply, pConnPtr, u4VarLen);
    MemReleaseMemBlock (OFC_MULTIPART_POOLID, (UINT1 *) pMultiPartRply);
    return OFC_SUCCESS;
#endif /* DPA_WANTED */
#endif /* NPAPI_WANTED && OPENFLOW_TCAM */
    pIfEntry =
        (tOfcFsofcIfEntry *) RBTreeGetFirst (gOfcGlobals.OfcGlbMib.
                                             FsofcIfTable);

    if (pIfEntry == NULL)
    {
        Ofc131PktMultiPartRplySend (pMultiPartRply, pConnPtr, u4VarLen);
        MemReleaseMemBlock (OFC_MULTIPART_POOLID, (UINT1 *) pMultiPartRply);
        return OSIX_SUCCESS;
    }

    do
    {
        /* Read all the port description details */
        MEMSET (pPortDescReply, OFC_ZERO, sizeof (tOfcPortDescReply));
        if (pIfEntry->MibObject.i4FsofcIfOperStatus == CFA_IF_UP)
        {
           pPortDescReply->u4State = OFPPS_LINK_UP;
        }
        else if (pIfEntry->MibObject.i4FsofcIfOperStatus == CFA_IF_DOWN)
        {
           pPortDescReply->u4State = OSIX_HTONL(OFPPS_LINK_DOWN);
        }		   

        pPortDescReply->u4PortNum =
            OSIX_HTONL ((UINT4) pIfEntry->MibObject.i4FsofcIfIndex);
        MEMCPY (pPortDescReply->au1Name, pIfEntry->au1Name, OFC_SIXTEEN);
        MEMCPY (pPortDescReply->u1HwAddr, pIfEntry->au1HwAddr, OFC_SIX);

        /* 
         * If the size of PortInfo Buffer is more than the size of multipart msg
         * mempool size, only the interface info which fits in given memory will
         * be sent to Controller
         */
        if (OFC_MAX_MULTIPART_MSG_SIZE <
            (u4VarLen + sizeof (tOfcPortDescReply)))
        {
            break;
        }
        u4VarLen += sizeof (tOfcPortDescReply);
        pPortDescReply++;
    }
    while ((pIfEntry =
            (tOfcFsofcIfEntry *) RBTreeGetNext (gOfcGlobals.OfcGlbMib.
                                                FsofcIfTable, pIfEntry,
                                                NULL)) != NULL);
    /* Start updating the port description msg with reserved ports details */
    u4Index = OFPP_IN_PORT;
    while (u4Index != OFC_ZERO)
    {
        if ((u4Index == OFPP_NORMAL) ||
            (u4Index == OFPP_FLOOD) || (u4Index == OFPP_LOCAL))
        {
            /*
             * OFPP_NORMAL, OFPP_FLOOD, OFPP_LOCAL will be supported in
             * hybrid openflow switch only
             */
            u4Index++;
            continue;
        }
        MEMSET (pPortDescReply, OFC_ZERO, sizeof (tOfcPortDescReply));
        pPortDescReply->u4State = OSIX_HTONL (OFPPS_LINK_UP);
        pPortDescReply->u4PortNum = OSIX_HTONL (u4Index);

        /*
         * If the size of PortInfo Buffer is more than the size of multipart msg 
         * mempool size, only the interface info which fits in given memory will
         * be sent to Controller
         */
        if (OFC_MAX_MULTIPART_MSG_SIZE <
            (u4VarLen + sizeof (tOfcPortDescReply)))
        {
            break;
        }

        /*
         * Incrementing OFPP_ANY (0xffffffff) may result in u4Index value
         * to wrap around, so break.
         */
        if (u4Index == OFPP_ANY)
        {
            u4Index = OFC_ZERO;
        }
        else
        {
            u4Index++;
        }

        u4VarLen += sizeof (tOfcPortDescReply);
        pPortDescReply++;
    }
    /* Send routine */
    Ofc131PktMultiPartRplySend (pMultiPartRply, pConnPtr, u4VarLen);
    MemReleaseMemBlock (OFC_MULTIPART_POOLID, (UINT1 *) pMultiPartRply);
    UNUSED_PARAM (pu4Error);
    UNUSED_PARAM (pBuf);
    OFC_TRC_FUNC ((OFC_FN_EXIT, "FUNC:Ofc131MultipartPortDesc\n"));
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function Name : Ofc131MultipartPortStats
 * Description   : This routine sends a port statistics multi part reply to  
 *                 the controller.
 * Input(s)      : pMultiPartReq       - MultiPart Request
 *                 pConnPtr            - Connection Pointer
 *                 pBuf                - CRU Buffer Pointer
 *                 pu4Error            - Pointer to Error type
 * Output(s)     : None
 * Return(s)     : OSIX_SUCCESS OR OSIX_FAILURE
 *****************************************************************************/
INT4
Ofc131MultipartPortStats (tOfp131MultiPartReq * pMultipartReq,
                          VOID *pConnPtr, tCRU_BUF_CHAIN_HEADER * pBuf,
                          UINT4 *pu4Error)
{
    tOfcFsofcIfEntry    IfEntry;
    tIfCountersStruct   IfCounter;
    tOfcPortStatsReq   *pPortStatsReq = NULL;
    tOfcFsofcIfEntry   *pIfEntry = NULL;
    tOfp131MultiPartRply *pMultiPartRply = NULL;
    tOfcPortStatsReply *pPortStatsReply = NULL;
    INT4                i4RetVal = OFC_ZERO;
    UINT4               u4VarLen = OFC_ZERO;
    UINT1               au1Buf[OFC_BUFFER_SIZE];
    tOfcFsofcControllerConnEntry *pLocal = NULL;
    tOfcFsofcCfgEntry  *pOfcFsofcCfgEntry = NULL;
    FS_UINT8            u8Temp1;
    FS_UINT8            u8Temp2;
    FS_UINT8            u8Result;
    AR_UINT8            u8TempVar = OFC_ZERO;

    OFC_TRC_FUNC ((OFC_FN_ENTRY, "FUNC:Ofc131MultipartPortStats\n"));

    pLocal = (tOfcFsofcControllerConnEntry *) pConnPtr;

    MEMSET (&u8Temp1, OFC_ZERO, OFC_EIGHT);
    MEMSET (&u8Temp2, OFC_ZERO, OFC_EIGHT);
    MEMSET (&u8Result, OFC_ZERO, OFC_EIGHT);
    MEMSET (&IfEntry, OFC_ZERO, sizeof (tOfcFsofcIfEntry));
    MEMSET (&IfCounter, OFC_ZERO, sizeof (tIfCountersStruct));
    MEMSET (au1Buf, OFC_ZERO, OFC_BUFFER_SIZE);

    pMultiPartRply =
        (tOfp131MultiPartRply *) MemAllocMemBlk (OFC_MULTIPART_POOLID);

    if (pMultiPartRply == NULL)
    {
        OFC_TRC_FUNC ((OFC_ACTS_TRC, "Memory Allocation Failed\n"));
        return OFC_FAILURE;
    }

    pOfcFsofcCfgEntry =
        OfcGetFsofcCfgEntry (pLocal->MibObject.u4FsofcContextId);
    if (pOfcFsofcCfgEntry == NULL)
    {
        OFC_TRC_FUNC ((OFC_ACTS_TRC, "\r\nOfc131MultipartPortStats: "
                       "Get CFG Entry Failed\n"));
        MemReleaseMemBlock (OFC_MULTIPART_POOLID, (UINT1 *) pMultiPartRply);
        return OFC_FAILURE;
    }

    MEMSET(pMultiPartRply, OFC_ZERO, OFC_MAX_MULTIPART_MSG_SIZE);
    pMultiPartRply->u2Type = pMultipartReq->u2Type;
    pMultiPartRply->u2Flags = pMultipartReq->u2Flags;

    CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) au1Buf, OFC_ZERO,
                               sizeof (tOfp131MultiPartReq) + OFC_EIGHT +
                               sizeof (tOfcPortStatsReq));

    pPortStatsReq = (tOfcPortStatsReq *) ((VOID *) ((UINT1 *) au1Buf +
                                                    OFC_EIGHT +
                                                    sizeof
                                                    (tOfp131MultiPartReq)));

    pPortStatsReply =
        (tOfcPortStatsReply *) ((VOID *) (pMultiPartRply->au1Body));

    pPortStatsReq->u4PortNum = OSIX_HTONL (pPortStatsReq->u4PortNum);

#if defined (NPAPI_WANTED) && defined (OPENFLOW_TCAM)
#ifdef DPA_WANTED
    if (Ofc131DpaPortStatsGet (pPortStatsReq->u4PortNum, pPortStatsReply,
                               &u4VarLen) != OFC_SUCCESS)
    {
        Ofc131PktMultiPartRplySend (pMultiPartRply, pConnPtr, u4VarLen);
        MemReleaseMemBlock (OFC_MULTIPART_POOLID, (UINT1 *) pMultiPartRply);
        return OFC_FAILURE;
    }
    Ofc131PktMultiPartRplySend (pMultiPartRply, pConnPtr, u4VarLen);
    MemReleaseMemBlock (OFC_MULTIPART_POOLID, (UINT1 *) pMultiPartRply);
    return OFC_SUCCESS;
#endif /* DPA_WANTED */
#endif /* NPAPI_WANTED && OPENFLOW_TCAM */
    if (pPortStatsReq->u4PortNum != OFPP_ANY)
    {
        /* 
         * If the port stats is for the requested port then get statistics 
         * for that particular port
         */
        IfEntry.MibObject.i4FsofcIfIndex = (INT4) pPortStatsReq->u4PortNum;
        IfEntry.MibObject.u4FsofcContextId =
            pOfcFsofcCfgEntry->MibObject.u4FsofcContextId;

        i4RetVal = OfcGetAllFsofcIfTable (&IfEntry);
        pIfEntry = &IfEntry;
    }
    else
    {
        /* 
         * If Port stats is for all the ports,
         * then send statisics for all ports
         */
        pIfEntry =
            (tOfcFsofcIfEntry *) RBTreeGetFirst (gOfcGlobals.OfcGlbMib.
                                                 FsofcIfTable);
        if (pIfEntry == NULL)
        {
            i4RetVal = OSIX_FAILURE;
        }
    }

    if (i4RetVal == OSIX_SUCCESS)
    {
        do
        {
            MEMSET (pPortStatsReply, OFC_ZERO, sizeof (tOfcPortStatsReply));

            /*
             * Loop for all the interfaces in given OFC context only
             */
            if (pOfcFsofcCfgEntry->MibObject.u4FsofcContextId !=
                pIfEntry->MibObject.u4FsofcContextId)
            {
                continue;
            }

            pPortStatsReply->u4PortNum =
                OSIX_HTONL ((UINT4) pIfEntry->MibObject.i4FsofcIfIndex);
            if (CfaGetIfCounters ((UINT4) pIfEntry->MibObject.i4FsofcIfIndex,
                                  &IfCounter) != OSIX_SUCCESS)
            {
                MemReleaseMemBlock (OFC_MULTIPART_POOLID,
                                    (UINT1 *) pMultiPartRply);
                return OSIX_FAILURE;
            }
            /* received packets */
            u8Temp1.u4Lo = OSIX_HTONL (IfCounter.u4InUcastPkts);
            u8Temp2.u4Lo = OSIX_HTONL (IfCounter.u4InMulticastPkts);
            FSAP_U8_ADD (&u8Result, &u8Temp1, &u8Temp2);
            u8Temp1.u4Lo = OSIX_HTONL (IfCounter.u4InBroadcastPkts);
            FSAP_U8_ADD (&u8Result, &u8Temp1, &u8Result);
            MEMCPY (&pPortStatsReply->u8RxPkts, &u8Result, sizeof (FS_UINT8));
            /* transmitted packets */
            u8Temp1.u4Lo = OSIX_HTONL (IfCounter.u4OutUcastPkts);
            u8Temp2.u4Lo = OSIX_HTONL (IfCounter.u4OutMulticastPkts);
            FSAP_U8_ADD (&u8Result, &u8Temp1, &u8Temp2);
            u8Temp1.u4Lo = OSIX_HTONL (IfCounter.u4OutBroadcastPkts);
            FSAP_U8_ADD (&u8Result, &u8Temp1, &u8Result);
            MEMCPY (&pPortStatsReply->u8TxPkts, &u8Result, sizeof (FS_UINT8));
            /* received bytes */
            u8Temp1.u4Lo = OSIX_HTONL (IfCounter.u4InOctets);
            u8Temp1.u4Hi = OSIX_HTONL (IfCounter.u4HighInOctets);
            MEMCPY (&u8TempVar, &u8Temp1, sizeof (FS_UINT8));
            u8TempVar = Ofcinputhton64 (&u8TempVar);
            MEMCPY (&pPortStatsReply->u8RxBytes, &u8TempVar, sizeof (FS_UINT8));
            /* transmitted bytes */
            u8Temp1.u4Lo = OSIX_HTONL (IfCounter.u4OutOctets);
            u8Temp1.u4Hi = OSIX_HTONL (IfCounter.u4HighOutOctets);
            MEMCPY (&u8TempVar, &u8Temp1, sizeof (FS_UINT8));
            u8TempVar = Ofcinputhton64 (&u8TempVar);
            MEMCPY (&pPortStatsReply->u8TxBytes, &u8TempVar, sizeof (FS_UINT8));
            /* revceive errors */
            pPortStatsReply->u8RxErrors.u4Lo =
                OSIX_HTONL (IfCounter.u4InErrors);
            /* tranmit errors */
            pPortStatsReply->u8TxErrors.u4Lo =
                OSIX_HTONL (IfCounter.u4OutErrors);

            /* 
             * If the size of PortInfo Buffer is more than the size of 
             * multipart msg mempool size, only the interface info which fits
             * in given memory will be sent to Controller
             */
            if (OFC_MAX_MULTIPART_MSG_SIZE <
                (u4VarLen + sizeof (tOfcPortStatsReply)))
            {
                break;
            }
            u4VarLen += sizeof (tOfcPortStatsReply);
            pPortStatsReply++;

            if (pPortStatsReq->u4PortNum != OFPP_ANY)
            {
                /* request for one port only, break the loop */
                break;
            }
        }
        while ((pIfEntry =
                (tOfcFsofcIfEntry *) RBTreeGetNext (gOfcGlobals.OfcGlbMib.
                                                    FsofcIfTable, pIfEntry,
                                                    NULL)) != NULL);
    }
    /* Send Routine */
    Ofc131PktMultiPartRplySend (pMultiPartRply, pConnPtr, u4VarLen);
    MemReleaseMemBlock (OFC_MULTIPART_POOLID, (UINT1 *) pMultiPartRply);
    UNUSED_PARAM (pu4Error);
    OFC_TRC_FUNC ((OFC_FN_EXIT, "FUNC:Ofc131MultipartPortStats\n"));
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function Name : Ofc131MultipartOfcDesc
 * Description   : This routine sends a switch description multi part reply to
 *                 the controller.
 * Input(s)      : pMultiPartReq       - MultiPart Request
 *                 pConnPtr            - Connection Pointer
 *                 pBuf                - CRU Buffer Pointer
 *                 pu4Error            - Pointer to Error type
 * Output(s)     : None
 * Return(s)     : OSIX_SUCCESS OR OSIX_FAILURE
 *****************************************************************************/
INT4
Ofc131MultipartOfcDesc (tOfp131MultiPartReq * pMultipartReq,
                        VOID *pConnPtr, tCRU_BUF_CHAIN_HEADER * pBuf,
                        UINT4 *pu4Error)
{
    tOfp131MultiPartRply *pMultiPartRply = NULL;
    tOfcDesciReply     *pDescReply = NULL;
    UINT4               u4VarLen = OFC_ZERO;

    OFC_TRC_FUNC ((OFC_FN_ENTRY, "FUNC:Ofc131MultipartOfcDesc\n"));

    pMultiPartRply =
        (tOfp131MultiPartRply *) MemAllocMemBlk (OFC_MULTIPART_POOLID);

    if (pMultiPartRply == NULL)
    {
        OFC_TRC_FUNC ((OFC_ACTS_TRC, "Memory Allocation Failed\n"));
        return OFC_FAILURE;
    }
    pDescReply = (tOfcDesciReply *) (pMultiPartRply->au1Body);

    pMultiPartRply->u2Type = pMultipartReq->u2Type;
    pMultiPartRply->u2Flags = pMultipartReq->u2Flags;

    /* Get the switch description */
    OfcGetOfcDesc (pDescReply);

    /* send routine */
    u4VarLen = sizeof (tOfcDesciReply);
    Ofc131PktMultiPartRplySend (pMultiPartRply, pConnPtr, u4VarLen);
    MemReleaseMemBlock (OFC_MULTIPART_POOLID, (UINT1 *) pMultiPartRply);

    UNUSED_PARAM (pu4Error);
    UNUSED_PARAM (pBuf);
    OFC_TRC_FUNC ((OFC_FN_ENTRY, "FUNC:Ofc131MultipartOfcDesc\n"));
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function Name : Ofc131MultipartGroupFeatures
 * Description   : This routine sends a Group Feature multi part reply to
 *                 the controller.
 * Input(s)      : pMultiPartReq - MultiPart Request
 *                 pConnPtr      - Connection Pointer
 *                 pBuf          - CRU Buffer Pointer
 *                 pu4Error      - Pointer to Error type
 * Output(s)     : None
 * Return(s)     : OSIX_SUCCESS OR OSIX_FAILURE
 *****************************************************************************/
INT4
Ofc131MultipartGroupFeatures (tOfp131MultiPartReq * pMultipartReq,
                              VOID *pConnPtr, tCRU_BUF_CHAIN_HEADER * pBuf,
                              UINT4 *pu4Error)
{
    tOfcGroupFeatures  *pGroupFeatures = NULL;
    tOfp131MultiPartRply *pMultiPartRply = NULL;
    UINT4               u4VarLen = OFC_ZERO;

    OFC_TRC_FUNC ((OFC_FN_ENTRY, "FUNC:Ofc131MultipartGroupFeatures\n"));

    pMultiPartRply =
        (tOfp131MultiPartRply *) MemAllocMemBlk (OFC_MULTIPART_POOLID);

    if (pMultiPartRply == NULL)
    {
        OFC_TRC_FUNC ((OFC_ACTS_TRC, "Memory Allocation Failed\n"));
        return OFC_FAILURE;
    }
    pMultiPartRply->u2Type = pMultipartReq->u2Type;
    pMultiPartRply->u2Flags = pMultipartReq->u2Flags;

    pGroupFeatures = (tOfcGroupFeatures *) ((VOID *) (pMultiPartRply->au1Body));

    /* Get the group features of all the groups */
    MEMSET (pGroupFeatures, OFC_ZERO, sizeof (tOfcGroupFeatures));
    pGroupFeatures->u4Types = OSIX_HTONL (OFC_GROUP_TYPES_BITMASK);
    pGroupFeatures->u4Capabilities = OSIX_HTONL (OFC_GROUP_CAPS_BITMASK);
    /* max groups supported for each type */
    pGroupFeatures->au4MaxGroups[OFC_ZERO] =
        OSIX_HTONL ((MAX_OFC_FSOFCGROUPTABLE / OFC_FOUR));
    pGroupFeatures->au4MaxGroups[OFC_ONE] =
        OSIX_HTONL ((MAX_OFC_FSOFCGROUPTABLE / OFC_FOUR));
    pGroupFeatures->au4MaxGroups[OFC_TWO] =
        OSIX_HTONL ((MAX_OFC_FSOFCGROUPTABLE / OFC_FOUR));
    pGroupFeatures->au4MaxGroups[OFC_THREE] =
        OSIX_HTONL ((MAX_OFC_FSOFCGROUPTABLE / OFC_FOUR));
    /* actions supported for each type */
    pGroupFeatures->au4Actions[OFC_ZERO] = OSIX_HTONL (OFC_GROUP_ACTS_BITMASK);
    pGroupFeatures->au4Actions[OFC_ONE] = OSIX_HTONL (OFC_GROUP_ACTS_BITMASK);
    pGroupFeatures->au4Actions[OFC_TWO] = OSIX_HTONL (OFC_GROUP_ACTS_BITMASK);
    pGroupFeatures->au4Actions[OFC_THREE] = OSIX_HTONL (OFC_GROUP_ACTS_BITMASK);

    u4VarLen = sizeof (tOfcGroupFeatures);

    /* Send routine */
    Ofc131PktMultiPartRplySend (pMultiPartRply, pConnPtr, u4VarLen);
    MemReleaseMemBlock (OFC_MULTIPART_POOLID, (UINT1 *) pMultiPartRply);

    UNUSED_PARAM (pu4Error);
    UNUSED_PARAM (pBuf);
    OFC_TRC_FUNC ((OFC_FN_EXIT, "FUNC:Ofc131MultipartGroupFeatures\n"));
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function Name : Ofc131MultipartGroupStats
 * Description   : This routine sends a Group Statistics for particular group
 *                 or for all groups  multi part reply to the controller.
 * Input(s)      : pMultiPartReq - MultiPart Request
 *                 pConnPtr      - Connection Pointer
 *                 pBuf          - CRU Buffer Pointer
 *                 pu4Error      - Pointer to Error type
 * Output(s)     : None
 * Return(s)     : OSIX_SUCCESS OR OSIX_FAILURE
 *****************************************************************************/
INT4
Ofc131MultipartGroupStats (tOfp131MultiPartReq * pMultipartReq,
                           VOID *pConnPtr, tCRU_BUF_CHAIN_HEADER * pBuf,
                           UINT4 *pu4Error)
{
    tOfcFsofcGroupEntry GrpEntry;
    tOfcGroupStatsReply *pGrpStatsReply = NULL;
    tOfcGroupStatsReq  *pGrpStatsReq = NULL;
    tOfcFsofcGroupEntry *pGrpEntry = NULL;
    tOfp131MultiPartRply *pMultiPartRply = NULL;
    tOfcBucketCounter  *pBucketCounter = NULL;
    INT4                i4RetVal = OFC_ZERO;
    UINT4               u4VarLen = OFC_ZERO;
    UINT1               au1Buf[OFC_BUFFER_SIZE];
    tOfcFsofcControllerConnEntry *pLocal = NULL;
    tOfcFsofcCfgEntry  *pOfcFsofcCfgEntry = NULL;
    AR_UINT8            u8TempVal = OFC_ZERO;

#if defined (NPAPI_WANTED) && defined (OPENFLOW_TCAM)
    UINT1               u1RetVal = OFC_ZERO;
    tOfcHwInfo          OfcHwInfo;

    MEMSET (&OfcHwInfo, OFC_ZERO, sizeof (tOfcHwInfo));
#endif /* NPAPI_WANTED && OPENFLOW_TCAM */

    OFC_TRC_FUNC ((OFC_FN_ENTRY, "FUNC:Ofc131MultipartGroupStats\n"));

    pLocal = (tOfcFsofcControllerConnEntry *) pConnPtr;

    MEMSET (&GrpEntry, OFC_ZERO, sizeof (tOfcFsofcGroupEntry));

    pMultiPartRply =
        (tOfp131MultiPartRply *) MemAllocMemBlk (OFC_MULTIPART_POOLID);

    if (pMultiPartRply == NULL)
    {
        OFC_TRC_FUNC ((OFC_ACTS_TRC, "Memory Allocation Failed\n"));
        return OFC_FAILURE;
    }

    pOfcFsofcCfgEntry =
        OfcGetFsofcCfgEntry (pLocal->MibObject.u4FsofcContextId);
    if (pOfcFsofcCfgEntry == NULL)
    {
        OFC_TRC_FUNC ((OFC_ACTS_TRC, "\r\nOfc131MultipartGroupStats: "
                       "Get CFG Entry Failed\n"));
        MemReleaseMemBlock (OFC_MULTIPART_POOLID, (UINT1 *) pMultiPartRply);
        return OFC_FAILURE;
    }

    MEMSET (pMultiPartRply, OFC_ZERO, OFC_MAX_MULTIPART_MSG_SIZE);
    pMultiPartRply->u2Type = pMultipartReq->u2Type;
    pMultiPartRply->u2Flags = pMultipartReq->u2Flags;

    CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) au1Buf, OFC_ZERO,
                               sizeof (tOfp131MultiPartReq) + OFC_EIGHT +
                               sizeof (tOfcGroupStatsReq));
    pGrpStatsReq = (tOfcGroupStatsReq *) ((VOID *) ((UINT1 *) au1Buf +
                                                    sizeof (tOfp131MultiPartReq)
                                                    + OFC_EIGHT));

    pGrpStatsReply =
        (tOfcGroupStatsReply *) ((VOID *) (pMultiPartRply->au1Body));

    pGrpStatsReq->u4GroupId = OSIX_HTONL (pGrpStatsReq->u4GroupId);

    if (pGrpStatsReq->u4GroupId != OFPG_ALL)
    {
        GrpEntry.MibObject.u4FsofcContextId =
            pOfcFsofcCfgEntry->MibObject.u4FsofcContextId;
        GrpEntry.MibObject.u4FsofcGroupIndex = pGrpStatsReq->u4GroupId;
        i4RetVal = OfcGetAllFsofcGroupTable (&GrpEntry);

        if (OSIX_SUCCESS == i4RetVal)
        {
            pGrpStatsReply->u4GroupId = OSIX_HTONL (pGrpStatsReq->u4GroupId);
            pGrpStatsReply->u2Length = OSIX_HTONS ((sizeof (tOfcGroupStatsReply)
                                                    +
                                                    sizeof
                                                    (tOfcBucketCounter)));
#if defined (NPAPI_WANTED) && defined (OPENFLOW_TCAM)
            OfcHwInfo.OfcVer = OFC_VER_131;
            OfcHwInfo.OfcCmd = OFC_NP_GET_GROUP_STATS;
            OfcHwInfo.OfcHwEntry.OfcHwGroupInfo.u4FsofcGroupIndex =
                pGrpStatsReq->u4GroupId;

            u1RetVal = OfcFsNpHwConfigOfcInfo (&OfcHwInfo);
            if (u1RetVal == FNP_FAILURE)
            {
                /* no harm */
            }
            MEMCPY (&pGrpStatsReply->u8PktCount,
                    &OfcHwInfo.OfcHwEntry.OfcGroupStats.u8PktCount, OFC_EIGHT);
            MEMCPY (&pGrpStatsReply->u8ByteCount,
                    &OfcHwInfo.OfcHwEntry.OfcGroupStats.u8ByteCount, OFC_EIGHT);
            pGrpStatsReply->u4RefCount =
                OSIX_HTONL (OfcHwInfo.OfcHwEntry.OfcGroupStats.u4RefCount);
            pGrpStatsReply->u4DurSec =
                OSIX_HTONL (OfcHwInfo.OfcHwEntry.OfcGroupStats.u4DurSec);
#else
            MEMCPY (&pGrpStatsReply->u8PktCount,
                    &GrpEntry.MibObject.u8FsofcGroupPacketCount, OFC_EIGHT);
            MEMCPY (&pGrpStatsReply->u8ByteCount,
                    &GrpEntry.MibObject.u8FsofcGroupByteCount, OFC_EIGHT);
            pGrpStatsReply->u4RefCount = OFC_ZERO;
            pGrpStatsReply->u4DurSec =
                OSIX_HTONL (GrpEntry.MibObject.u4FsofcGroupDurationSec);
#endif /* NPAPI_WANTED && OPENFLOW_TCAM */
            /* Convert to Network Byte Order */
            MEMCPY (&u8TempVal, &pGrpStatsReply->u8PktCount, OFC_EIGHT);
            u8TempVal = Ofcinputhton64 (&u8TempVal);
            MEMCPY (&pGrpStatsReply->u8PktCount, &u8TempVal, OFC_EIGHT);

            MEMCPY (&u8TempVal, &pGrpStatsReply->u8ByteCount, OFC_EIGHT);
            u8TempVal = Ofcinputhton64 (&u8TempVal);
            MEMCPY (&pGrpStatsReply->u8ByteCount, &u8TempVal, OFC_EIGHT);

            pGrpStatsReply->u4DurNSec = OFC_ZERO;

            u4VarLen =
                sizeof (tOfcGroupStatsReply) + sizeof (tOfcBucketCounter);
        }
        else
        {
            u4VarLen = OFC_ZERO;
        }
    }
    else
    {
        /* Get the group statistics is for all the groups */
        pGrpEntry =
            (tOfcFsofcGroupEntry *) RBTreeGetFirst (gOfcGlobals.OfcGlbMib.
                                                    FsofcGroupTable);
        if (pGrpEntry == NULL)
        {
            /* Send routine (There are no groups in table)   */
            Ofc131PktMultiPartRplySend (pMultiPartRply, pConnPtr, u4VarLen);
            MemReleaseMemBlock (OFC_MULTIPART_POOLID, (UINT1 *) pMultiPartRply);
            return OSIX_SUCCESS;
        }

        do
        {
            pGrpStatsReply->u4GroupId =
                OSIX_HTONL (pGrpEntry->MibObject.u4FsofcGroupIndex);
            pGrpStatsReply->u2Length =
                OSIX_HTONS ((sizeof (tOfcGroupStatsReply) +
                             sizeof (tOfcBucketCounter)));
#if defined (NPAPI_WANTED) && defined (OPENFLOW_TCAM)
            OfcHwInfo.OfcVer = OFC_VER_131;
            OfcHwInfo.OfcCmd = OFC_NP_GET_GROUP_STATS;
            OfcHwInfo.OfcHwEntry.OfcHwGroupInfo.u4FsofcGroupIndex =
                pGrpStatsReq->u4GroupId;

            u1RetVal = OfcFsNpHwConfigOfcInfo (&OfcHwInfo);
            if (u1RetVal == FNP_FAILURE)
            {
                /* no harm */
            }
            MEMCPY (&pGrpStatsReply->u8PktCount,
                    &OfcHwInfo.OfcHwEntry.OfcGroupStats.u8PktCount, OFC_EIGHT);
            MEMCPY (&pGrpStatsReply->u8ByteCount,
                    &OfcHwInfo.OfcHwEntry.OfcGroupStats.u8ByteCount, OFC_EIGHT);
            pGrpStatsReply->u4RefCount =
                OSIX_HTONL (OfcHwInfo.OfcHwEntry.OfcGroupStats.u4RefCount);
            pGrpStatsReply->u4DurSec =
                OSIX_HTONL (OfcHwInfo.OfcHwEntry.OfcGroupStats.u4DurSec);
#else
            MEMCPY (&pGrpStatsReply->u8PktCount,
                    &GrpEntry.MibObject.u8FsofcGroupPacketCount, OFC_EIGHT);
            MEMCPY (&pGrpStatsReply->u8ByteCount,
                    &GrpEntry.MibObject.u8FsofcGroupByteCount, OFC_EIGHT);
            pGrpStatsReply->u4RefCount = OFC_ZERO;
            pGrpStatsReply->u4DurSec =
                OSIX_HTONL (GrpEntry.MibObject.u4FsofcGroupDurationSec);
#endif /* NPAPI_WANTED && OPENFLOW_TCAM */
            /* Convert to Network Byte Order */
            MEMCPY (&u8TempVal, &pGrpStatsReply->u8PktCount, OFC_EIGHT);
            u8TempVal = Ofcinputhton64 (&u8TempVal);
            MEMCPY (&pGrpStatsReply->u8PktCount, &u8TempVal, OFC_EIGHT);

            MEMCPY (&u8TempVal, &pGrpStatsReply->u8ByteCount, OFC_EIGHT);
            u8TempVal = Ofcinputhton64 (&u8TempVal);
            MEMCPY (&pGrpStatsReply->u8ByteCount, &u8TempVal, OFC_EIGHT);

            pGrpStatsReply->u4DurNSec = OFC_ZERO;

            pBucketCounter =
                (tOfcBucketCounter *) (pGrpStatsReply->BucketStats);
            MEMSET (pBucketCounter, OFC_ZERO, sizeof (tOfcBucketCounter));

            pBucketCounter++;
            u4VarLen +=
                (sizeof (tOfcGroupStatsReply) + sizeof (tOfcBucketCounter));
            pGrpStatsReply = (tOfcGroupStatsReply *) (pBucketCounter);
        }
        while ((pGrpEntry =
                (tOfcFsofcGroupEntry *) RBTreeGetNext (gOfcGlobals.OfcGlbMib.
                                                       FsofcGroupTable,
                                                       pGrpEntry,
                                                       NULL)) != NULL);
    }
    /* Send routine */
    Ofc131PktMultiPartRplySend (pMultiPartRply, pConnPtr, u4VarLen);
    MemReleaseMemBlock (OFC_MULTIPART_POOLID, (UINT1 *) pMultiPartRply);
    UNUSED_PARAM (pu4Error);

    OFC_TRC_FUNC ((OFC_FN_EXIT, "FUNC:Ofc131MultipartGroupStats\n"));

    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function Name : Ofc131MultipartGroupDesc
 * Description   : This routine sends a Group description of all the groups
 *                 multi part reply to the controller.
 * Input(s)      : pMultiPartReq - MultiPart Request
 *                 pConnPtr      - Connection Pointer
 *                 pBuf          - CRU Buffer Pointer
 *                 pu4Error      - Pointer to Error type
 * Output(s)     : None
 * Return(s)     : OSIX_SUCCESS OR OSIX_FAILURE
 *****************************************************************************/
INT4
Ofc131MultipartGroupDesc (tOfp131MultiPartReq * pMultipartReq,
                          VOID *pConnPtr, tCRU_BUF_CHAIN_HEADER * pBuf,
                          UINT4 *pu4Error)
{
    tOfcGroupDescStats *pGrpDescStats = NULL;
    tOfp131MultiPartRply *pMultiPartRply = NULL;
    tOfcFsofcGroupEntry *pGrpEntry = NULL;
    tOfp131Bucket      *pOfpGrpBucket = NULL;
    tOfp131ActionHdr   *pActHdr = NULL;
    tOfcActs           *pActs = NULL;
    tOfcGroupBucket    *pGrpBucket = NULL;
    tOfcActOutput      *pOfcActOutput = NULL;
    tOfcActSetMplsTtl  *pOfcActSetMplsTtl = NULL;
    tOfcActPush        *pOfcActPush = NULL;
    tOfcActPopMpls     *pOfcActPopMpls = NULL;
    tOfcActSetQue      *pOfcActSetQue = NULL;
    tOfcActGroup       *pOfcActGroup = NULL;
    tOfcActSetNwTtl    *pOfcActSetNwTtl = NULL;
    tOfcActExprHdr     *pOfcActExprHdr = NULL;
    tOxmTlv            *pOxmTlv = NULL;
    tOfcSetFld         *pOfcSetFld = NULL;
    INT4                i4BucketCount = OFC_ZERO;
    INT4                i4ActLen = OFC_ZERO;
    UINT4               u4VarLen = OFC_ZERO;
    UINT1               u1FldLen = OFC_ZERO;
    UINT1               u1PadBytes = OFC_ZERO;

    OFC_TRC_FUNC ((OFC_FN_ENTRY, "FUNC:Ofc131MultipartGroupDesc\n"));

    pMultiPartRply =
        (tOfp131MultiPartRply *) MemAllocMemBlk (OFC_MULTIPART_POOLID);

    if (pMultiPartRply == NULL)
    {
        OFC_TRC_FUNC ((OFC_ACTS_TRC, "Memory Allocation Failed\n"));
        return OFC_FAILURE;
    }
	MEMSET (pMultiPartRply, OFC_ZERO, OFC_MAX_MULTIPART_MSG_SIZE);

    pMultiPartRply->u2Type = pMultipartReq->u2Type;
    pMultiPartRply->u2Flags = pMultipartReq->u2Flags;

    pGrpEntry =
        (tOfcFsofcGroupEntry *) RBTreeGetFirst (gOfcGlobals.OfcGlbMib.
                                                FsofcGroupTable);

    pGrpDescStats = (tOfcGroupDescStats *) ((VOID *) (pMultiPartRply->au1Body));
	MEMSET (pGrpDescStats, OFC_ZERO, sizeof(tOfcGroupDescStats));

    if (pGrpEntry == NULL)
    {
        /* Send routine (there are no groups)  */
        Ofc131PktMultiPartRplySend (pMultiPartRply, pConnPtr, u4VarLen);
        MemReleaseMemBlock (OFC_MULTIPART_POOLID, (UINT1 *) pMultiPartRply);
        return OSIX_SUCCESS;
    }

    do
    {
        /* Get the group description for all the groups */
        i4BucketCount = OFC_ZERO;
        i4ActLen = OFC_ZERO;
        MEMSET (pGrpDescStats, OFC_ZERO, sizeof (tOfcGroupDescStats));

        pGrpDescStats->u1Type = (UINT1) pGrpEntry->MibObject.i4FsofcGroupType;
        pGrpDescStats->u4GroupId =
            OSIX_HTONL (pGrpEntry->MibObject.u4FsofcGroupIndex);
        pOfpGrpBucket = (tOfp131Bucket *) (pGrpDescStats->buckets);
        TMO_SLL_Scan (&pGrpEntry->BucketList, pGrpBucket, tOfcGroupBucket *)
        {
            i4BucketCount++;
			MEMSET (pOfpGrpBucket, OFC_ZERO, sizeof(tOfp131Bucket));
            pOfpGrpBucket->u4WatchPort = OSIX_HTONL (pGrpBucket->u4WatchPort);
            pOfpGrpBucket->u4WatchGroup = OSIX_HTONL (pGrpBucket->u4WatchGroup);
            pOfpGrpBucket->u2Weight = OSIX_HTONS (pGrpBucket->u2Weight);
            pActHdr = (tOfp131ActionHdr *) (pOfpGrpBucket->Actions);
            TMO_SLL_Scan (&pGrpBucket->ActList, pActs, tOfcActs *)
            {
                MEMSET (pActHdr, OFC_ZERO, sizeof (tOfp131ActionHdr));
                pActHdr->u2Type = (UINT2) OSIX_HTONS (pActs->u2Type);

                switch (pActs->u2Type)
                {
                    case OFCAT_OUTPUT:
                        pActHdr->u2Len = sizeof(tOfcActOutput) + sizeof(tOfp131ActionHdr);
                        pActHdr->u2Len = OSIX_HTONS (pActHdr->u2Len);
                        pOfcActOutput = (tOfcActOutput *) ((VOID *) (((UINT1 *) pActHdr) + OFC_FOUR));
                        pOfcActOutput->u4Port = OSIX_NTOHL (pActs->Acts.Output.u4Port);
                        pOfcActOutput->u2MaxLen = OSIX_NTOHS (pActs->Acts.Output.u2MaxLen);
                        break;
                    case OFCAT_SET_MPLS_TTL:
                        pActHdr->u2Len = sizeof(tOfcActSetMplsTtl) + sizeof(tOfp131ActionHdr);
                        pActHdr->u2Len = OSIX_HTONS (pActHdr->u2Len);
                        pOfcActSetMplsTtl = (tOfcActSetMplsTtl *) ((VOID *) (((UINT1 *) pActHdr) + OFC_FOUR));
                        pOfcActSetMplsTtl->u1MplsTtl = pActs->Acts.SetMplsTtl.u1MplsTtl;
                        break;
                    case OFCAT_COPY_TTL_OUT:
                    case OFCAT_COPY_TTL_IN: 
                    case OFCAT_DEC_MPLS_TTL:
                    case OFCAT_POP_VLAN:    
                    case OFCAT_DEC_NW_TTL:  
                    case OFCAT_POP_PBB:     
                        /* Action Header Length is modified to 8 as these actions can have 
                         * only Type and Length*/
                        pActHdr->u2Len = OSIX_HTONS (OFC_EIGHT);
                        break;
                    case OFCAT_POP_MPLS:    
                        pActHdr->u2Len = sizeof(tOfcActPopMpls) + sizeof(tOfp131ActionHdr);
                        pActHdr->u2Len = OSIX_HTONS (pActHdr->u2Len);
                        pOfcActPopMpls = (tOfcActPopMpls *) ((VOID *) (((UINT1 *) pActHdr) + OFC_FOUR));
                        pOfcActPopMpls->u2EthType = OSIX_NTOHS (pActs->Acts.PopMpls.u2EthType);
                        break;
                    case OFCAT_SET_QUEUE:   
                        pActHdr->u2Len = sizeof(tOfcActSetQue) + sizeof(tOfp131ActionHdr);
                        pActHdr->u2Len = OSIX_HTONS (pActHdr->u2Len);
                        pOfcActSetQue = (tOfcActSetQue *) ((VOID *) (((UINT1 *) pActHdr) + OFC_FOUR));
                        pOfcActSetQue->u4QueId = OSIX_NTOHL (pActs->Acts.SetQue.u4QueId);
                        break;
                    case OFCAT_GROUP:       
                        pActHdr->u2Len = sizeof(tOfcActGroup) + sizeof(tOfp131ActionHdr);
                        pActHdr->u2Len = OSIX_HTONS (pActHdr->u2Len);
                        pOfcActGroup = (tOfcActGroup *) ((VOID *) (((UINT1 *) pActHdr) + OFC_FOUR));
                        pOfcActGroup->u4GroupId = OSIX_NTOHL (pActs->Acts.Group.u4GroupId);
                        break;
                    case OFCAT_SET_NW_TTL:
                        pActHdr->u2Len = sizeof(tOfcActSetNwTtl) + sizeof(tOfp131ActionHdr);
                        pActHdr->u2Len = OSIX_HTONS (pActHdr->u2Len);
                        pOfcActSetNwTtl = (tOfcActSetNwTtl *) ((VOID *) (((UINT1 *) pActHdr) + OFC_FOUR));
                        MEMCPY(pOfcActSetNwTtl, &pActs->Acts.SetNwTtl, sizeof(tOfcActSetNwTtl));
                        break;
                    case OFCAT_SET_FIELD:   
                        pActHdr->u2Len = sizeof(tOfp131ActionHdr);
                        pOxmTlv = (tOxmTlv *) ((VOID *) (((UINT1 *) pActHdr) + OFC_FOUR));
                        TMO_SLL_Scan (&pActs->Acts.SetFld.SetFldList, pOfcSetFld, tOfcSetFld *)
                        {
                            pActHdr->u2Len = (UINT2) (pActHdr->u2Len + sizeof(tOxmTlv));
                            pOxmTlv->u1Field = (UINT1) (pOfcSetFld->u2Type);
                            switch (pOxmTlv->u1Field)
                            {
                                case OFCXMT_OFB_VLAN_PCP:    /* VLAN Priority */
                                case OFCXMT_OFB_IP_DSCP:    /* IP DSCP (6 bits in ToS field) */
                                case OFCXMT_OFB_IP_ECN:    /* IP ECN (2 bits in ToS field) */
                                case OFCXMT_OFB_IP_PROTO:    /* IPv4 Protocol */
                                case OFCXMT_OFB_ICMPV4_TYPE:    /* ICMP Type */
                                case OFCXMT_OFB_ICMPV4_CODE:    /* ICMP Code */
                                case OFCXMT_OFB_ICMPV6_TYPE:    /* ICMPv6 Type */
                                case OFCXMT_OFB_ICMPV6_CODE:    /* ICMPv6 Code */
                                case OFCXMT_OFB_MPLS_TC:    /* MPLS TC */
                                case OFCXMT_OFB_MPLS_BOS:    /* MPLS BoS bit */
                                    u1FldLen = OFC_ONE;
                                    break;

                                case OFCXMT_OFB_ETH_TYPE:    /* Ethernet Type */
                                case OFCXMT_OFB_VLAN_VID:    /* VLAN ID */
                                case OFCXMT_OFB_TCP_SRC:    /* TCP Source Port */
                                case OFCXMT_OFB_TCP_DST:    /* TCP Destination Port */
                                case OFCXMT_OFB_UDP_SRC:    /* UDP Source Port */
                                case OFCXMT_OFB_UDP_DST:    /* UDP Destination Port */
                                case OFCXMT_OFB_SCTP_SRC:    /* SCTP Source Port */
                                case OFCXMT_OFB_SCTP_DST:    /* SCTP Destination Port */
                                case OFCXMT_OFB_ARP_OP:    /* ARP OpCode */
                                case OFCXMT_OFB_IPV6_EXTHDR:    /* IPv6 Ext Header pseudo-field */
                                    u1FldLen = OFC_TWO;
                                    break;

                                case OFCXMT_OFB_IPV6_FLABEL:    /* IPv6 Flow Label */
                                case OFCXMT_OFB_MPLS_LABEL:    /* MPLS Label */
                                case OFCXMT_OFB_PBB_ISID:    /* PBB I-SID */
                                    u1FldLen = OFC_THREE;
                                    break;

                                case OFCXMT_OFB_IN_PORT:    /* Switch input port */
                                case OFCXMT_OFB_IN_PHY_PORT:    /* Switch physical input port */
                                case OFCXMT_OFB_IPV4_SRC:    /* IPv4 Source Address */
                                case OFCXMT_OFB_IPV4_DST:    /* IPv4 Destination Address */
                                case OFCXMT_OFB_ARP_SPA:    /* ARP Source IPv4 Address */
                                case OFCXMT_OFB_ARP_TPA:    /* ARP Target IPv4 Address */
                                    u1FldLen = OFC_FOUR;
                                    break;

                                case OFCXMT_OFB_ETH_DST:    /* Ethernet Destination Address */
                                case OFCXMT_OFB_ETH_SRC:    /* Ethernet Source Address */
                                case OFCXMT_OFB_ARP_SHA:    /* ARP Source Hardware Address */
                                case OFCXMT_OFB_ARP_THA:    /* ARP Destination Hardware Address */
                                case OFCXMT_OFB_IPV6_ND_SLL:    /* Source Link Layer for ND */
                                case OFCXMT_OFB_IPV6_ND_TLL:    /* Target Link Layer for ND */
                                    u1FldLen = OFC_SIX;
                                    break;

                                case OFCXMT_OFB_METADATA:    /* Metadata passed between tables */
                                case OFCXMT_OFB_TUNNEL_ID:    /* Logical Port Metadata */
                                    u1FldLen = OFC_EIGHT;
                                    break;

                                case OFCXMT_OFB_IPV6_SRC:    /* IPv6 Source Address */
                                case OFCXMT_OFB_IPV6_DST:    /* IPv6 Destination Address */
                                case OFCXMT_OFB_IPV6_ND_TARGET:    /* Target Address for ND */
                                    u1FldLen = OFC_SIXTEEN;
                                    break;

                                default:
                                    /* we should not be getting this */
                                    break;
                            }
                            MEMSET (pOxmTlv, OFC_ZERO, (sizeof(tOxmTlv) + u1FldLen));					     
                            pOxmTlv->u2Class = OSIX_HTONS (OFPXMC_OPENFLOW_BASIC);
                            pOxmTlv->u1Field = (UINT1) (pOfcSetFld->u2Type << OFC_ONE);
                            pOxmTlv->u1Len = u1FldLen;
                            MEMCPY(pOxmTlv->au1Value, pOfcSetFld->SetFld.au1Fld, u1FldLen);
                            pActHdr->u2Len = (UINT2) (pActHdr->u2Len + (UINT2) (u1FldLen));

                            /* A.2.3.1 Check if padding is required in ofp_match to make it multiple of 8 */
                            u1PadBytes = (UINT1) (OFC_EIGHT - (pActHdr->u2Len % OFC_EIGHT)); 
                            if(u1PadBytes)
                            {
                                MEMSET(pOxmTlv + u1FldLen, OFC_ZERO, u1PadBytes);
                                pActHdr->u2Len = (UINT2) (pActHdr->u2Len + (UINT2) (u1PadBytes));
                            }

                            pOxmTlv = (tOxmTlv *) ((VOID *) ((UINT1 *)(pOxmTlv) + u1FldLen + sizeof(tOxmTlv)));
                        }
                        pActHdr->u2Len = OSIX_HTONS (pActHdr->u2Len);
                        break;
                    case OFCAT_PUSH_PBB:    
                    case OFCAT_PUSH_VLAN:
                    case OFCAT_PUSH_MPLS:   
                        pActHdr->u2Len = sizeof(tOfcActPush) + sizeof(tOfp131ActionHdr);
                        pActHdr->u2Len = OSIX_HTONS (pActHdr->u2Len);
                        pOfcActPush = (tOfcActPush *) ((VOID *) (((UINT1 *) pActHdr) + OFC_FOUR));
                        pOfcActPush->u2EthType = OSIX_NTOHS (pActs->Acts.Push.u2EthType);
                        break;
                    case OFCAT_EXPERIMENTER:
                        pActHdr->u2Len = sizeof(tOfcActExprHdr) + sizeof(tOfp131ActionHdr);
                        pActHdr->u2Len = OSIX_HTONS (pActHdr->u2Len);
                        pOfcActExprHdr = (tOfcActExprHdr *) ((VOID *) (((UINT1 *) pActHdr) + OFC_FOUR));
                        pOfcActExprHdr->u4Experimenter = OSIX_NTOHL (pActs->Acts.ExprHdr.u4Experimenter);
                        break;
                    default:
                        break;
                }
                i4ActLen = i4ActLen + OSIX_NTOHS(pActHdr->u2Len);
                pActHdr = (tOfp131ActionHdr *) ((VOID *) ((UINT1 *) pActHdr + OSIX_NTOHS(pActHdr->u2Len)));
            }
            pOfpGrpBucket->u2Len = (UINT2) OSIX_HTONS ((UINT2)
                                                       (sizeof (tOfp131Bucket) + (UINT2) i4ActLen));
            pOfpGrpBucket = (tOfp131Bucket *) (VOID *) pActHdr;
        }
        pGrpDescStats->u2Length =
            OSIX_HTONS (((UINT2) sizeof (tOfcGroupDescStats) +
                         (UINT2) (i4BucketCount *
                                  (INT4) sizeof (tOfp131Bucket)) +
                         (UINT2) i4ActLen));
        u4VarLen =
            (UINT4) (u4VarLen +
                     (sizeof (tOfcGroupDescStats) +
                      ((UINT4) i4BucketCount * sizeof (tOfp131Bucket)) +
                      (UINT4) (i4ActLen)));
        pGrpDescStats = (tOfcGroupDescStats *) pOfpGrpBucket;
    }
    while ((pGrpEntry =
            (tOfcFsofcGroupEntry *) RBTreeGetNext (gOfcGlobals.OfcGlbMib.
                                                   FsofcGroupTable, pGrpEntry,
                                                   NULL)) != NULL);
    /* Send routine */
    Ofc131PktMultiPartRplySend (pMultiPartRply, pConnPtr, u4VarLen);
    MemReleaseMemBlock (OFC_MULTIPART_POOLID, (UINT1 *) pMultiPartRply);
    UNUSED_PARAM (pu4Error);
    UNUSED_PARAM (pBuf);
    OFC_TRC_FUNC ((OFC_FN_EXIT, "FUNC:Ofc131MultipartGroupDesc\n"));
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function Name : Ofc131MultipartTableFeatures
 * Description   : This routine sends a table features of all the tables
                   multi part reply to the controller
 * Input(s)      : pMultiPartReq - MultiPart Request
 *                 pConnPtr      - Connection Pointer
 *                 pBuf          - CRU Buffer pointer
 *                 pu4Error      - Pointer to Error type
 * Output(s)     : None
 * Return(s)     : OSIX_SUCCESS OR OSIX_FAILURE
 *****************************************************************************/
INT4
Ofc131MultipartTableFeatures (tOfp131MultiPartReq * pMultipartReq,
                              VOID *pConnPtr, tCRU_BUF_CHAIN_HEADER * pBuf,
                              UINT4 *pu4Error)
{
    tOfp131ErrMsg       Ofp131ErrorMsg;
    FS_UINT8            u8ValBitMap;
    tOfp131MultiPartRply *pMultiPartRply = NULL;
    tOfcTableFeatures  *pTableFeatures = NULL;
    tOfcTableFeatPropHdr *pTablePropHdr = NULL;
    tOfcTableFeatPropInstr *pTablePropInstr = NULL;
    tOfcTableFeatPropNxtTable *pTablePropNxtTbl = NULL;
    tOfcTableFeatPropActs *pTablePropActs = NULL;
    tOfcTableFeatPropOxm *pTablePropOxm = NULL;
    tOfcFsofcFlowTable *pFlowTable = NULL;
    tOfcActionHdr      *pActionHdr = NULL;
    tOfcInstruction    *pInstr = NULL;
    tOxmTlv            *pOxmTlv = NULL;
    tOfp131Pkt         *pOfpHdr = NULL;
    UINT1              *pu1NextTable = NULL;
    UINT1               u1OneByteBitMap = OFC_ZERO;
    UINT4               u4FourByteBitMap = OFC_ZERO;
    UINT4               u4TempFourBitMap = OFC_ZERO;
    UINT2               u2TempLen = OFC_ZERO;
    UINT2               u2VarLen = OFC_ZERO;
    UINT2               u2FeatLen = OFC_ZERO;
    UINT2               u2PropLen = OFC_ZERO;
    UINT1               u1LpCount = OFC_ZERO;
    UINT1               u1BitMapCount = OFC_ZERO;
    UINT1               u1SetCount = OFC_ZERO;
    UINT1               u1Iter = OFC_ZERO;
    UINT1               u1ErrorFlag = OFC_ZERO;
    UINT1               u1ClearFlows = OFC_ZERO;
    UINT2               u2PadLength = OFC_ZERO;
    UINT1               u1PadBytes = OFC_ZERO;
    tOfcFsofcControllerConnEntry *pLocal = pConnPtr;
    tOfcFsofcCfgEntry  *pOfcFsofcCfgEntry = NULL;
#ifdef DPA_WANTED
    UINT1               u1LocalTabId = OFC_ZERO;
    UINT1               u1DpaTabId = OFC_ZERO;
#endif /* DPA_WANTED */

    OFC_TRC_FUNC ((OFC_FN_ENTRY, "FUNC:Ofc131MultipartTableFeatures\n"));

    pMultiPartRply =
        (tOfp131MultiPartRply *) MemAllocMemBlk (OFC_MULTIPART_POOLID);
    if (pMultiPartRply == NULL)
    {
        OFC_TRC_FUNC ((OFC_ACTS_TRC, "\r\nMemAlloc Failed for pMultiPartRply, "
                    "return FAILURE\n"));
        return OFC_FAILURE;
    }

    pOfcFsofcCfgEntry =
        OfcGetFsofcCfgEntry (pLocal->MibObject.u4FsofcContextId);
    if (pOfcFsofcCfgEntry == NULL)
    {
        OFC_TRC_FUNC ((OFC_ACTS_TRC, "\r\nOfc131MultipartTableFeatures: "
                    "Get CFG Entry Failed\n"));
        MemReleaseMemBlock (OFC_MULTIPART_POOLID, (UINT1 *) pMultiPartRply);
        return OFC_FAILURE;
    }

    MEMSET (pMultiPartRply, OFC_ZERO, OFC_MAX_MULTIPART_MSG_SIZE);
    pMultiPartRply->u2Type = pMultipartReq->u2Type;

    /* Verify if the features request is to get or set the features */
    pOfpHdr = (tOfp131Pkt *) (VOID *) (pLocal->pRcvPkt);
    u2VarLen = OSIX_NTOHS (pOfpHdr->u2Length);
    if (u2VarLen > OFC_SIXTEEN)
    {
        /* 
         * This is the set request and a successful configuration should
         * contain all the configurations of all the tables. 
         * Property BitMaps in each table should be replaced with new types
         */
        pTableFeatures = (tOfcTableFeatures *) (VOID *) (pLocal->pRcvPkt +
                OFC_SIXTEEN);
        /* update the length to exclude the ofp and multipart headers */
        u2VarLen = (UINT2) (u2VarLen - (OFC_SIXTEEN));
        while (u2VarLen > OFC_ZERO)
        {
            /* Fetch the Flow Table */
#ifdef DPA_WANTED
            /* Get Local Table ID frm DPA Table ID */
            if (OfcGetLocalTableId (pTableFeatures->u1TableId, &u1LocalTabId)
                    == OFC_FAILURE)
            {
                return OFC_FAILURE;
            }
            pTableFeatures->u1TableId = u1LocalTabId;
#endif /* DPA_WANTED */
            pFlowTable =
                OfcGetFlowTableEntry (pOfcFsofcCfgEntry->MibObject.
                        u4FsofcContextId,
                        pTableFeatures->u1TableId);
            if (pFlowTable == NULL)
            {
                /* error */
                u1ErrorFlag = OFC_ONE;
                break;
            }

            /* Update the table name */
            MEMCPY (pFlowTable->au1Name, pTableFeatures->u1Name,
                    OFC_MAX_TABLE_NAME_LEN);

            /* Update the bits of metadata, the table can match and write */
            pFlowTable->u8MetadataMatch.u4Hi =
                OSIX_NTOHL (pTableFeatures->u8MetadataMatch.u4Hi);
            pFlowTable->u8MetadataMatch.u4Lo =
                OSIX_HTONL (pTableFeatures->u8MetadataMatch.u4Lo);
            pFlowTable->u8MetadataWrite.u4Hi =
                OSIX_HTONL (pTableFeatures->u8MetadataWrite.u4Hi);
            pFlowTable->u8MetadataWrite.u4Lo =
                OSIX_HTONL (pTableFeatures->u8MetadataWrite.u4Lo);

            /* Update the types for each property */
            u2FeatLen = (UINT2) (OSIX_NTOHS (pTableFeatures->u2Length) -
                    (sizeof (tOfcTableFeatures)));
            pTablePropHdr = (tOfcTableFeatPropHdr *)
                (pTableFeatures->Properties);
            while (u2FeatLen > OFC_ZERO)
            {
                switch (OSIX_NTOHS (pTablePropHdr->u2Type))
                {
                    case OFCTFPT_INSTRUCTIONS:
                        pTablePropInstr = (tOfcTableFeatPropInstr *)
                            pTablePropHdr;
                        u2PropLen = (UINT2)
                            (OSIX_NTOHS (pTablePropInstr->u2Length) - OFC_FOUR);
                        pInstr = (tOfcInstruction *)
                            (pTablePropInstr->InstructionIds);
                        /* Clear the existing bit map */
                        u1OneByteBitMap = pFlowTable->u1InstrsBitMap;
                        pFlowTable->u1InstrsBitMap = OFC_ZERO;
                        /* update the bit map */
                        while (u2PropLen > OFC_ZERO)
                        {
                            if ((OSIX_NTOHS (pInstr->u2Type) == OFC_ZERO) ||
                                    (OSIX_NTOHS (pInstr->u2Type) > OFC_SIX))
                            {
                                u1ErrorFlag = OFC_ONE;
                                pFlowTable->u1InstrsBitMap = u1OneByteBitMap;
                                break;
                            }
                            pFlowTable->u1InstrsBitMap |= (UINT1) (OFC_ONE <<
                                    OSIX_NTOHS
                                    (pInstr->
                                     u2Type));
                            pInstr++;
                            if (u2PropLen < sizeof (tOfcInstruction))
                            {
                                /* OFPTFFC_BAD_LEN */
                                u1ErrorFlag = OFC_ONE;
                                break;
                            }
                            u2PropLen = (UINT2) (u2PropLen -
                                    (sizeof (tOfcInstruction)));
                        }
                        pFlowTable->u1InstrsBitMap |= u1OneByteBitMap;
                        break;

                    case OFCTFPT_NEXT_TABLES:
                        pTablePropNxtTbl = (tOfcTableFeatPropNxtTable *)
                            pTablePropHdr;
                        u2PropLen = (UINT2)
                            (OSIX_NTOHS (pTablePropNxtTbl->u2Length) -
                             OFC_FOUR);
                        pu1NextTable = pTablePropNxtTbl->au1NextTableIds;
                        /* Clear the existing bit map */
                        u4FourByteBitMap = pFlowTable->u4NxtTblsBitMap;
                        pFlowTable->u4NxtTblsBitMap = OFC_ZERO;
                        /* update the bit map */
                        while (u2PropLen > OFC_ZERO)
                        {
#ifdef DPA_WANTED
                            /* Get Local Table ID frm DPA Table ID */
                            if (OfcGetLocalTableId
                                    (*pu1NextTable, &u1LocalTabId) == OFC_FAILURE)
                            {
                                return OFC_FAILURE;
                            }
                            *pu1NextTable = u1LocalTabId;
#endif /* DPA_WANTED */
                            if (*pu1NextTable >
                                    pOfcFsofcCfgEntry->u2NumOfTables)
                            {
                                u1ErrorFlag = OFC_ONE;
                                pFlowTable->u4NxtTblsBitMap = u4FourByteBitMap;
                                break;
                            }
                            pFlowTable->u4NxtTblsBitMap |= (UINT4) (OFC_ONE <<
                                    *pu1NextTable);
                            pu1NextTable++;
                            u2PropLen--;
                        }
                        pFlowTable->u4NxtTblsBitMap |= u4FourByteBitMap;
                        break;

                    case OFCTFPT_WRITE_ACTIONS:
                        pTablePropActs = (tOfcTableFeatPropActs *)
                            pTablePropHdr;
                        u2PropLen = (UINT2)
                            (OSIX_NTOHS (pTablePropActs->u2Length) - OFC_FOUR);
                        pActionHdr = (tOfcActionHdr *)
                            (pTablePropActs->ActionIds);
                        /* Clear the existing bit map */
                        u4FourByteBitMap = pFlowTable->u4WrtActsBitMap;
                        pFlowTable->u4WrtActsBitMap = OFC_ZERO;
                        /* update the bit map */
                        while (u2PropLen > OFC_ZERO)
                        {
                            pActionHdr->u2Type =
                                OSIX_NTOHS (pActionHdr->u2Type);
                            if (pActionHdr->u2Type
                                    && ((pActionHdr->u2Type < OFC_ELEVEN)
                                        || (pActionHdr->u2Type > OFC_TWENTY_SEVEN)))
                            {
                                u1ErrorFlag = OFC_ONE;
                                pFlowTable->u4WrtActsBitMap = u4FourByteBitMap;
                                break;
                            }
                            pFlowTable->u4WrtActsBitMap |= (UINT4) (OFC_ONE <<
                                    pActionHdr->
                                    u2Type);
                            pActionHdr++;
                            if (u2PropLen < sizeof (tOfcActionHdr))
                            {
                                /* OFPTFFC_BAD_LEN */
                                u1ErrorFlag = OFC_ONE;
                                break;
                            }
                            u2PropLen = (UINT2) (u2PropLen -
                                    (sizeof (tOfcActionHdr)));
                        }
                        pFlowTable->u4WrtActsBitMap |= u4FourByteBitMap;
                        break;

                    case OFCTFPT_APPLY_ACTIONS:
                        pTablePropActs = (tOfcTableFeatPropActs *)
                            pTablePropHdr;
                        u2PropLen = (UINT2)
                            (OSIX_NTOHS (pTablePropActs->u2Length) - OFC_FOUR);
                        pActionHdr = (tOfcActionHdr *)
                            pTablePropActs->ActionIds;
                        /* Clear the existing bit map */
                        u4FourByteBitMap = pFlowTable->u4AppActsBitMap;
                        pFlowTable->u4AppActsBitMap = OFC_ZERO;
                        /* update the bit map */
                        while (u2PropLen > OFC_ZERO)
                        {
                            pActionHdr->u2Type =
                                OSIX_NTOHS (pActionHdr->u2Type);
                            if (pActionHdr->u2Type
                                    && ((pActionHdr->u2Type < OFC_ELEVEN)
                                        || (pActionHdr->u2Type > OFC_TWENTY_SEVEN)))
                            {
                                u1ErrorFlag = OFC_ONE;
                                pFlowTable->u4WrtActsBitMap = u4FourByteBitMap;
                                break;
                            }
                            pFlowTable->u4AppActsBitMap |= (UINT4) (OFC_ONE <<
                                    pActionHdr->
                                    u2Type);
                            pActionHdr++;
                            if (u2PropLen < sizeof (tOfcActionHdr))
                            {
                                /* OFPTFFC_BAD_LEN */
                                u1ErrorFlag = OFC_ONE;
                                break;
                            }
                            u2PropLen = (UINT2) (u2PropLen -
                                    (sizeof (tOfcActionHdr)));
                        }
                        pFlowTable->u4WrtActsBitMap |= u4FourByteBitMap;
                        break;

                    case OFCTFPT_MATCH:
                        pTablePropOxm = (tOfcTableFeatPropOxm *)
                            (VOID *) pTablePropHdr;
                        u2PropLen = (UINT2)
                            (OSIX_NTOHS (pTablePropOxm->u2Length) - OFC_FOUR);
                        pOxmTlv = (tOxmTlv *) (pTablePropOxm->au4OxmIds);
                        /* Clear the existing bit map */
                        u4FourByteBitMap = pFlowTable->u8FlowMatchBitMap.u4Lo;
                        u4TempFourBitMap = pFlowTable->u8FlowMatchBitMap.u4Hi;
                        pFlowTable->u8FlowMatchBitMap.u4Hi = OFC_ZERO;
                        pFlowTable->u8FlowMatchBitMap.u4Lo = OFC_ZERO;
                        /* update the bit map */
                        while (u2PropLen > OFC_ZERO)
                        {
                            u1Iter = (pOxmTlv->u1Field >> OFC_ONE);
                            if (u1Iter > OFC_THIRTY_NINE)
                            {
                                u1ErrorFlag = OFC_ONE;
                                pFlowTable->u8FlowMatchBitMap.u4Hi =
                                    u4TempFourBitMap;
                                pFlowTable->u8FlowMatchBitMap.u4Lo =
                                    u4FourByteBitMap;
                                break;
                            }
                            else if (u1Iter > OFC_THIRTY_ONE)
                            {
                                pFlowTable->u8FlowMatchBitMap.u4Hi |=
                                    (UINT4) (OFC_ONE <<
                                            (u1Iter - OFC_THIRTY_TWO));
                            }
                            else
                            {
                                pFlowTable->u8FlowMatchBitMap.u4Lo |=
                                    (UINT4) (OFC_ONE << u1Iter);
                            }
                            pOxmTlv++;
                            u1BitMapCount++;
                            if (u2PropLen < sizeof (tOxmTlv))
                            {
                                /* OFPTFFC_BAD_LEN */
                                u1ErrorFlag = OFC_ONE;
                                break;
                            }
                            u2PropLen =
                                (UINT2) (u2PropLen - (sizeof (tOxmTlv)));
                        }

                        if (u1BitMapCount > OFC_TEN)
                        {
                            /* set the error flag */
                            u1ErrorFlag = OFC_ONE;
                            pFlowTable->u8FlowMatchBitMap.u4Hi =
                                u4TempFourBitMap;
                            pFlowTable->u8FlowMatchBitMap.u4Lo =
                                u4FourByteBitMap;
                        }
                        else
                        {
                            /* update the num match in flow table */
                            pFlowTable->u1NumMatch = u1BitMapCount;
                        }
                        pFlowTable->u8FlowMatchBitMap.u4Hi |=
                            u4TempFourBitMap;
                        pFlowTable->u8FlowMatchBitMap.u4Lo |=
                            u4FourByteBitMap;
                        break;

                    case OFCTFPT_WILDCARDS:
                        pTablePropOxm = (tOfcTableFeatPropOxm *)
                            (VOID *) pTablePropHdr;
                        u2PropLen = (UINT2)
                            (OSIX_NTOHS (pTablePropOxm->u2Length) - OFC_FOUR);
                        pOxmTlv = (tOxmTlv *) (pTablePropOxm->au4OxmIds);
                        /* Clear the existing bit map */
                        u4FourByteBitMap = pFlowTable->u8WildCardsBitMap.u4Lo;
                        u4TempFourBitMap = pFlowTable->u8WildCardsBitMap.u4Hi;
                        pFlowTable->u8WildCardsBitMap.u4Hi = OFC_ZERO;
                        pFlowTable->u8WildCardsBitMap.u4Lo = OFC_ZERO;
                        /* update the bit map */
                        while (u2PropLen > OFC_ZERO)
                        {
                            u1Iter = (pOxmTlv->u1Field >> OFC_ONE);
                            if (u1Iter > OFC_THIRTY_NINE)
                            {
                                u1ErrorFlag = OFC_ONE;
                                pFlowTable->u8WildCardsBitMap.u4Hi =
                                    u4TempFourBitMap;
                                pFlowTable->u8WildCardsBitMap.u4Lo =
                                    u4FourByteBitMap;
                                break;
                            }
                            else if (u1Iter > OFC_THIRTY_ONE)
                            {
                                pFlowTable->u8WildCardsBitMap.u4Hi |=
                                    (UINT4) (OFC_ONE <<
                                            (u1Iter - OFC_THIRTY_TWO));
                            }
                            else
                            {
                                pFlowTable->u8WildCardsBitMap.u4Lo |=
                                    (UINT4) (OFC_ONE << u1Iter);
                            }
                            pOxmTlv++;
                            if (u2PropLen < sizeof (tOxmTlv))
                            {
                                /* OFPTFFC_BAD_LEN */
                                u1ErrorFlag = OFC_ONE;
                                break;
                            }
                            u2PropLen =
                                (UINT2) (u2PropLen - (sizeof (tOxmTlv)));
                        }
                        pFlowTable->u8WildCardsBitMap.u4Hi |=
                            u4TempFourBitMap;
                        pFlowTable->u8WildCardsBitMap.u4Lo |=
                            u4FourByteBitMap;
                        break;

                    case OFCTFPT_WRITE_SETFIELD:
                        pTablePropOxm = (tOfcTableFeatPropOxm *)
                            (VOID *) pTablePropHdr;
                        u2PropLen =
                            (UINT2) (OSIX_NTOHS (pTablePropOxm->u2Length) -
                                    OFC_FOUR);
                        pOxmTlv = (tOxmTlv *) (pTablePropOxm->au4OxmIds);
                        /* Clear the existing bit map */
                        u4FourByteBitMap = pFlowTable->u8WrtStFldsBitMap.u4Lo;
                        u4TempFourBitMap = pFlowTable->u8WrtStFldsBitMap.u4Hi;
                        pFlowTable->u8WrtStFldsBitMap.u4Hi = OFC_ZERO;
                        pFlowTable->u8WrtStFldsBitMap.u4Lo = OFC_ZERO;
                        /* update the bit map */
                        while (u2PropLen > OFC_ZERO)
                        {
                            u1Iter = (pOxmTlv->u1Field >> OFC_ONE);
                            if ((u1Iter < OFC_THREE)
                                    || (u1Iter > OFC_THIRTY_NINE))
                            {
                                u1ErrorFlag = OFC_ONE;
                                pFlowTable->u8WrtStFldsBitMap.u4Hi =
                                    u4TempFourBitMap;
                                pFlowTable->u8WrtStFldsBitMap.u4Lo =
                                    u4FourByteBitMap;
                                break;
                            }
                            else if (u1Iter > OFC_THIRTY_ONE)
                            {
                                pFlowTable->u8WrtStFldsBitMap.u4Hi |=
                                    (UINT4) (OFC_ONE <<
                                            (u1Iter - OFC_THIRTY_TWO));
                            }
                            else
                            {
                                pFlowTable->u8WrtStFldsBitMap.u4Lo |=
                                    (UINT4) (OFC_ONE << u1Iter);
                            }
                            pOxmTlv++;
                            if (u2PropLen < sizeof (tOxmTlv))
                            {
                                /* OFPTFFC_BAD_LEN */
                                u1ErrorFlag = OFC_ONE;
                                break;
                            }
                            u2PropLen =
                                (UINT2) (u2PropLen - (sizeof (tOxmTlv)));
                        }
                        pFlowTable->u8WrtStFldsBitMap.u4Hi |=
                            u4TempFourBitMap;
                        pFlowTable->u8WrtStFldsBitMap.u4Lo |=
                            u4FourByteBitMap;
                        break;

                    case OFCTFPT_APPLY_SETFIELD:
                        pTablePropOxm = (tOfcTableFeatPropOxm *)
                            (VOID *) pTablePropHdr;
                        u2PropLen =
                            (UINT2) (OSIX_NTOHS (pTablePropOxm->u2Length) -
                                    OFC_FOUR);
                        pOxmTlv = (tOxmTlv *) (pTablePropOxm->au4OxmIds);
                        /* Clear the existing bit map */
                        u4FourByteBitMap = pFlowTable->u8AppStFldsBitMap.u4Lo;
                        u4TempFourBitMap = pFlowTable->u8AppStFldsBitMap.u4Hi;
                        pFlowTable->u8AppStFldsBitMap.u4Hi = OFC_ZERO;
                        pFlowTable->u8AppStFldsBitMap.u4Lo = OFC_ZERO;
                        /* update the bit map */
                        while (u2PropLen > OFC_ZERO)
                        {
                            u1Iter = (pOxmTlv->u1Field >> OFC_ONE);
                            if ((u1Iter < OFC_THREE) ||
                                    (u1Iter > OFC_THIRTY_NINE))
                            {
                                u1ErrorFlag = OFC_ONE;
                                pFlowTable->u8AppStFldsBitMap.u4Hi |=
                                    u4TempFourBitMap;
                                pFlowTable->u8AppStFldsBitMap.u4Lo |=
                                    u4FourByteBitMap;
                                break;
                            }
                            else if (u1Iter > OFC_THIRTY_ONE)
                            {
                                pFlowTable->u8AppStFldsBitMap.u4Hi |=
                                    (UINT4) (OFC_ONE <<
                                            (u1Iter - OFC_THIRTY_TWO));
                            }
                            else
                            {
                                pFlowTable->u8AppStFldsBitMap.u4Lo |=
                                    (UINT4) (OFC_ONE << u1Iter);
                            }
                            pOxmTlv++;
                            if (u2PropLen < sizeof (tOxmTlv))
                            {
                                /* OFPTFFC_BAD_LEN */
                                u1ErrorFlag = OFC_ONE;
                                break;
                            }
                            u2PropLen =
                                (UINT2) (u2PropLen - (sizeof (tOxmTlv)));
                        }
                        pFlowTable->u8AppStFldsBitMap.u4Hi |=
                            u4TempFourBitMap;
                        pFlowTable->u8AppStFldsBitMap.u4Lo |=
                            u4FourByteBitMap;
                        break;

                        /* 
                         * for all the table miss properties, we consider
                         * respective regular flow entry properties. So need
                         * to store them explicitly 
                         */
                    case OFCTFPT_INSTRUCTIONS_MISS:
                    case OFCTFPT_NEXT_TABLES_MISS:
                    case OFCTFPT_WRITE_ACTIONS_MISS:
                    case OFCTFPT_APPLY_ACTIONS_MISS:
                    case OFCTFPT_WRITE_SETFIELD_MISS:
                    case OFCTFPT_APPLY_SETFIELD_MISS:
                        break;

                    case OFCTFPT_EXPERIMENTER:
                    case OFCTFPT_EXPERIMENTER_MISS:
                    default:
                        *pu4Error = OFC131_TABLE_FEAURES_TYPE_ERR;
                        u1ErrorFlag = OFC_ONE;
                        break;
                }
                if (u1ErrorFlag == OFC_ONE)
                    break;

                /* caliculate padding */
                u2PadLength = OSIX_NTOHS (pTablePropHdr->u2Length);
                u1PadBytes = u2PadLength % OFC_EIGHT;
                if (u1PadBytes)
                {
                    u1PadBytes = (UINT1) (OFC_EIGHT - u1PadBytes);
                }
                u2PadLength = (UINT2) (u2PadLength + u1PadBytes);

                /* Update the features length */
                if (u2FeatLen < u2PadLength)
                {
                    /* OFPTFFC_BAD_LEN */
                    u1ErrorFlag = OFC_ONE;
                    break;
                }
                u2FeatLen = (UINT2) (u2FeatLen - u2PadLength);
                pTablePropHdr = (tOfcTableFeatPropHdr *) (VOID *)
                    (((UINT1 *) pTablePropHdr) + u2PadLength);

            }
            if (u1ErrorFlag == OFC_ONE)
                break;

            /* Update the variable Length */
            if (u2VarLen < OSIX_NTOHS (pTableFeatures->u2Length))
            {
                /* OFPTFFC_BAD_LEN */
                u1ErrorFlag = OFC_ONE;
                break;
            }
            u2VarLen = (UINT2) (u2VarLen -
                    (OSIX_NTOHS (pTableFeatures->u2Length)));
            pTableFeatures = (tOfcTableFeatures *) (VOID *)
                (((UINT1 *) pTableFeatures) +
                 OSIX_NTOHS (pTableFeatures->u2Length));
        }
        /* Updation is completed, now send the reply */
        u2VarLen = (UINT2) (OSIX_NTOHS (pOfpHdr->u2Length) - OFC_SIXTEEN);
        if ((u1ErrorFlag == OFC_ZERO) &&
                (u2VarLen < OFC_MAX_MULTIPART_MSG_SIZE))
        {
            /* 
             * send the reply using the data in the request itself
             * to save proccessing.
             */
            MEMCPY (pMultiPartRply->au1Body, (pLocal->pRcvPkt + OFC_SIXTEEN),
                    u2VarLen);
            /* send routine */
            Ofc131PktMultiPartRplySend (pMultiPartRply, pConnPtr, u2VarLen);
        }
        /*
         * If this is the last message for this table features request, 
         * configuration is successfull and all the existing flows should
         * be deleted
         */
        if (pMultipartReq->u2Flags == OFC_ZERO)
            u1ClearFlows = OFC_ONE;
    }

    if (u1ErrorFlag == OFC_ONE)
    {
        /* send an error messsage before replying with supported features */
        MEMSET (&Ofp131ErrorMsg, OFC_ZERO, OFC_FOUR);
        Ofp131ErrorMsg.u2Type = OFPET_131_TABLE_FEATURES_FAILED;
        Ofp131ErrorMsg.u2Code = OFPTFFC_BAD_TYPE;
        Ofc131PktErrMsgSend (&Ofp131ErrorMsg, pConnPtr, OFC_ZERO);
    }

    /*
     * for get request or in case of error in handling set request.
     */
    if ((u2VarLen == OFC_SIXTEEN) || (u1ErrorFlag == OFC_ONE))
    {
        u2VarLen = sizeof (tOfp131MultiPartRply);
        pTableFeatures =
            (tOfcTableFeatures *) (VOID *) (pMultiPartRply->au1Body);
        for (u1LpCount = OFC_ZERO; u1LpCount < pOfcFsofcCfgEntry->u2NumOfTables;
                u1LpCount++)
        {
            /* Get the Flow Table entry */
            pFlowTable =
                OfcGetFlowTableEntry (pOfcFsofcCfgEntry->MibObject.
                        u4FsofcContextId, u1LpCount);

            if (pFlowTable != NULL)
            {
                /*
                 * check to not to cross the allocated memory
                 * 
                 * compute the number of instructions, next tables, write and
                 * apply actions, match and wild cards, write and apply set 
                 * fields. Then compute the required memory for all of them and
                 * if we dont have it now, send the response and start filling
                 * new data in next message. 
                 */
                u1SetCount = OFC_ZERO;
                u2TempLen = sizeof (tOfcTableFeatures);
                /* get the number of instructions */
                u1OneByteBitMap = pFlowTable->u1InstrsBitMap;
                while (u1OneByteBitMap)
                {
                    if (u1OneByteBitMap & OFC_TRUE)
                    {
                        u1SetCount++;
                    }
                    u1OneByteBitMap >>= OFC_ONE;
                }
                /* caliculate padding */
                u2PadLength = (UINT2) ((u1SetCount * sizeof (tOfcInstruction))
                        + OFC_FOUR);
                u1PadBytes = u2PadLength % OFC_EIGHT;
                if (u1PadBytes)
                {
                    u1PadBytes = (UINT1) (OFC_EIGHT - u1PadBytes);
                }
                u2PadLength = (UINT2) (u2PadLength + u1PadBytes);
                u2TempLen = (UINT2) (u2TempLen + u2PadLength);

                /* compute size for next tables */
                MEMSET (&u8ValBitMap, OFC_ZERO, OFC_EIGHT);
                u8ValBitMap.u4Lo = pFlowTable->u4NxtTblsBitMap;
                Ofc131ActsComputeNumBitsSet (u8ValBitMap, &u1SetCount);
                /* caliculate padding */
                u2PadLength = (UINT2) (u1SetCount + OFC_FOUR);
                u1PadBytes = u2PadLength % OFC_EIGHT;
                if (u1PadBytes)
                {
                    u1PadBytes = (UINT1) (OFC_EIGHT - u1PadBytes);
                }
                u2PadLength = (UINT2) (u2PadLength + u1PadBytes);
                u2TempLen = (UINT2) (u2TempLen + u2PadLength);

                /* compute size for write actions */
                MEMSET (&u8ValBitMap, OFC_ZERO, OFC_EIGHT);
                u8ValBitMap.u4Lo = pFlowTable->u4WrtActsBitMap;
                Ofc131ActsComputeNumBitsSet (u8ValBitMap, &u1SetCount);
                /* caliculate padding */
                u2PadLength = (UINT2) ((u1SetCount * sizeof (tOfcActionHdr)) +
                        OFC_FOUR);
                u1PadBytes = u2PadLength % OFC_EIGHT;
                if (u1PadBytes)
                {
                    u1PadBytes = (UINT1) (OFC_EIGHT - u1PadBytes);
                }
                u2PadLength = (UINT2) (u2PadLength + u1PadBytes);
                u2TempLen = (UINT2) (u2TempLen + u2PadLength);

                /* compute size for apply actions */
                MEMSET (&u8ValBitMap, OFC_ZERO, OFC_EIGHT);
                u8ValBitMap.u4Lo = pFlowTable->u4AppActsBitMap;
                Ofc131ActsComputeNumBitsSet (u8ValBitMap, &u1SetCount);
                /* caliculate padding */
                u2PadLength = (UINT2) ((u1SetCount * sizeof (tOfcActionHdr)) +
                        OFC_FOUR);
                u1PadBytes = u2PadLength % OFC_EIGHT;
                if (u1PadBytes)
                {
                    u1PadBytes = (UINT1) (OFC_EIGHT - u1PadBytes);
                }
                u2PadLength = (UINT2) (u2PadLength + u1PadBytes);
                u2TempLen = (UINT2) (u2TempLen + u2PadLength);

                /* compute size for match fields */
                MEMCPY (&u8ValBitMap, &pFlowTable->u8FlowMatchBitMap,
                        OFC_EIGHT);
                Ofc131ActsComputeNumBitsSet (u8ValBitMap, &u1SetCount);
                /* caliculate padding */
                u2PadLength = (UINT2) ((u1SetCount * sizeof (tOxmTlv)) +
                        OFC_FOUR);
                u1PadBytes = u2PadLength % OFC_EIGHT;
                if (u1PadBytes)
                {
                    u1PadBytes = (UINT1) (OFC_EIGHT - u1PadBytes);
                }
                u2PadLength = (UINT2) (u2PadLength + u1PadBytes);
                u2TempLen = (UINT2) (u2TempLen + u2PadLength);

                /* compute size for wildcards */
                MEMCPY (&u8ValBitMap, &pFlowTable->u8WildCardsBitMap,
                        OFC_EIGHT);
                Ofc131ActsComputeNumBitsSet (u8ValBitMap, &u1SetCount);
                /* caliculate padding */
                u2PadLength = (UINT2) ((u1SetCount * sizeof (tOxmTlv)) +
                        OFC_FOUR);
                u1PadBytes = u2PadLength % OFC_EIGHT;
                if (u1PadBytes)
                {
                    u1PadBytes = (UINT1) (OFC_EIGHT - u1PadBytes);
                }
                u2PadLength = (UINT2) (u2PadLength + u1PadBytes);
                u2TempLen = (UINT2) (u2TempLen + u2PadLength);

                /* compute size for write set fields */
                MEMCPY (&u8ValBitMap, &pFlowTable->u8WrtStFldsBitMap,
                        OFC_EIGHT);
                Ofc131ActsComputeNumBitsSet (u8ValBitMap, &u1SetCount);
                /* caliculate padding */
                u2PadLength = (UINT2) ((u1SetCount * sizeof (tOxmTlv)) +
                        OFC_FOUR);
                u1PadBytes = u2PadLength % OFC_EIGHT;
                if (u1PadBytes)
                {
                    u1PadBytes = (UINT1) (OFC_EIGHT - u1PadBytes);
                }
                u2PadLength = (UINT2) (u2PadLength + u1PadBytes);
                u2TempLen = (UINT2) (u2TempLen + u2PadLength);

                /* compute size for apply set fields */
                MEMCPY (&u8ValBitMap, &pFlowTable->u8AppStFldsBitMap,
                        OFC_EIGHT);
                Ofc131ActsComputeNumBitsSet (u8ValBitMap, &u1SetCount);
                /* caliculate padding */
                u2PadLength = (UINT2) ((u1SetCount * sizeof (tOxmTlv)) +
                        OFC_FOUR);
                u1PadBytes = u2PadLength % OFC_EIGHT;
                if (u1PadBytes)
                {
                    u1PadBytes = (UINT1) (OFC_EIGHT - u1PadBytes);
                }
                u2PadLength = (UINT2) (u2PadLength + u1PadBytes);
                u2TempLen = (UINT2) (u2TempLen + u2PadLength);

                if (OFC_MAX_MULTIPART_MSG_SIZE < (u2VarLen + u2TempLen))
                {
                    /* send the message  and then continue filling */
                    pMultiPartRply->u2Type = pMultipartReq->u2Type;
                    pMultiPartRply->u2Flags = OSIX_HTONS (OFC_ONE);
                    Ofc131PktMultiPartRplySend (pMultiPartRply, pConnPtr,
                            (UINT4) (u2VarLen - OFC_EIGHT));
                    MEMSET (pMultiPartRply, OFC_ZERO,
                            OFC_MAX_MULTIPART_MSG_SIZE);
                    u2VarLen = sizeof (tOfp131MultiPartRply);
                    pTableFeatures = (tOfcTableFeatures *) (VOID *)
                        pMultiPartRply->au1Body;

                }
                /* Read the table features */
                MEMSET (pTableFeatures, OFC_ZERO, sizeof (tOfcTableFeatures));
                pTableFeatures->u2Length = sizeof (tOfcTableFeatures);

#ifdef DPA_WANTED
                if (OfcGetDpaTableId (u1LpCount, &u1DpaTabId) == OFC_FAILURE)
                {
                    return OFC_FAILURE;
                }
                u1LpCount = u1DpaTabId;
#endif /* DPA_WANTED */
                pTableFeatures->u1TableId = u1LpCount;
                pTableFeatures->u8MetadataMatch.u4Hi =
                    OSIX_HTONL (pFlowTable->u8MetadataMatch.u4Hi);
                pTableFeatures->u8MetadataMatch.u4Lo =
                    OSIX_HTONL (pFlowTable->u8MetadataMatch.u4Lo);
                pTableFeatures->u8MetadataWrite.u4Hi =
                    OSIX_HTONL (pFlowTable->u8MetadataWrite.u4Hi);
                pTableFeatures->u8MetadataWrite.u4Lo =
                    OSIX_HTONL (pFlowTable->u8MetadataWrite.u4Lo);
                /* This remains zero */
                pTableFeatures->u4Config = OFC_ZERO;
                pTableFeatures->u4MaxEntries =
                    OSIX_HTONL (pFlowTable->u4MaxEntries);

                u1SetCount = OFC_ZERO;
                u1BitMapCount = OFC_ZERO;
                u1OneByteBitMap = OFC_ZERO;
                /* Fill the Instruction property */
                pTablePropInstr = (tOfcTableFeatPropInstr *)
                    (pTableFeatures->Properties);
                pTablePropInstr->u2Type = OSIX_HTONS (OFCTFPT_INSTRUCTIONS);
                pInstr = (tOfcInstruction *) (pTablePropInstr->InstructionIds);
                u1OneByteBitMap = pFlowTable->u1InstrsBitMap;
                /* Get the instruction ids of the Instruction property */
                while (u1OneByteBitMap)
                {
                    if (u1OneByteBitMap & OFC_TRUE)
                    {
                        pInstr->u2Type = u1BitMapCount;
                        pInstr->u2Type = OSIX_HTONS (pInstr->u2Type);
                        pInstr->u2Length =
                            OSIX_HTONS (sizeof (tOfcInstruction));
                        u1SetCount++;
                        pInstr++;
                    }
                    u1BitMapCount++;
                    u1OneByteBitMap >>= OFC_ONE;
                }
                pTablePropInstr->u2Length =
                    (UINT2) (sizeof (tOfcTableFeatPropInstr) +
                            (u1SetCount * sizeof (tOfcInstruction)));
                /* Add neccessary padding */
                u1PadBytes = pTablePropInstr->u2Length % OFC_EIGHT;
                if (u1PadBytes)
                {
                    u1PadBytes = (UINT1) (OFC_EIGHT - u1PadBytes);
                }
                pTableFeatures->u2Length = (UINT2) (pTableFeatures->u2Length +
                        pTablePropInstr->u2Length +
                        u1PadBytes);
                pTablePropInstr->u2Length =
                    OSIX_HTONS (pTablePropInstr->u2Length);

                u1SetCount = OFC_ZERO;
                u1BitMapCount = OFC_ZERO;
                u4FourByteBitMap = OFC_ZERO;
                /* Fill the Next Tables property */
                pTablePropNxtTbl = (tOfcTableFeatPropNxtTable *) (VOID *)
                    (((UINT1 *) pInstr) + u1PadBytes);
                pTablePropNxtTbl->u2Type = OSIX_HTONS (OFCTFPT_NEXT_TABLES);
                pu1NextTable = pTablePropNxtTbl->au1NextTableIds;
                u4FourByteBitMap = pFlowTable->u4NxtTblsBitMap;
                /* Get the array of next tables */
                while (u4FourByteBitMap)
                {
                    if (u4FourByteBitMap & OFC_TRUE)
                    {
#ifdef DPA_WANTED
                        if (OfcGetDpaTableId (u1BitMapCount, &u1DpaTabId)
                                == OFC_FAILURE)
                        {
                            return OFC_FAILURE;
                        }
                        *pu1NextTable = u1DpaTabId;
#else
                        *pu1NextTable = u1BitMapCount;
#endif /* DPA_WANTED */
                        u1SetCount++;
                        pu1NextTable++;
                    }
                    u1BitMapCount++;
                    u4FourByteBitMap >>= OFC_ONE;
                }
                pTablePropNxtTbl->u2Length =
                    (UINT2) (sizeof (tOfcTableFeatPropActs) + u1SetCount);
                /* Add neccessary padding */
                u1PadBytes = pTablePropNxtTbl->u2Length % OFC_EIGHT;
                if (u1PadBytes)
                {
                    u1PadBytes = (UINT1) (OFC_EIGHT - u1PadBytes);
                }
                pTableFeatures->u2Length =
                    (UINT2) (pTableFeatures->u2Length +
                            pTablePropNxtTbl->u2Length + u1PadBytes);
                pTablePropNxtTbl->u2Length =
                    OSIX_HTONS (pTablePropNxtTbl->u2Length);

                u1SetCount = OFC_ZERO;
                u1BitMapCount = OFC_ZERO;
                u4FourByteBitMap = OFC_ZERO;
                /* Fill the Write Actions property */
                pTablePropActs = (tOfcTableFeatPropActs *) (VOID *)
                    (((UINT1 *) pu1NextTable) + u1PadBytes);
                pTablePropActs->u2Type = OSIX_HTONS (OFCTFPT_WRITE_ACTIONS);
                pActionHdr = (tOfcActionHdr *) (pTablePropActs->ActionIds);
                u4FourByteBitMap = pFlowTable->u4WrtActsBitMap;
                /* Get the list of Write actions supported */
                while (u4FourByteBitMap && (u1SetCount != OFC_MAX_ACTIONS))
                {
                    if (u4FourByteBitMap & OFC_TRUE)
                    {
                        pActionHdr->u2Type = u1BitMapCount;
                        pActionHdr->u2Type = OSIX_HTONS (pActionHdr->u2Type);
                        pActionHdr->u2Length =
                            OSIX_HTONS (sizeof (tOfcActionHdr));
                        u1SetCount++;
                        pActionHdr++;
                    }
                    u1BitMapCount++;
                    u4FourByteBitMap >>= OFC_ONE;
                }
                pTablePropActs->u2Length =
                    (UINT2) (sizeof (tOfcTableFeatPropActs) +
                            (u1SetCount * sizeof (tOfcActionHdr)));
                /* Add neccessary padding */
                u1PadBytes = pTablePropActs->u2Length % OFC_EIGHT;
                if (u1PadBytes)
                {
                    u1PadBytes = (UINT1) (OFC_EIGHT - u1PadBytes);
                }
                pTableFeatures->u2Length =
                    (UINT2) (pTableFeatures->u2Length +
                            pTablePropActs->u2Length + u1PadBytes);
                pTablePropActs->u2Length =
                    OSIX_HTONS (pTablePropActs->u2Length);

                u1SetCount = OFC_ZERO;
                u1BitMapCount = OFC_ZERO;
                u4FourByteBitMap = OFC_ZERO;
                /* Fill the Apply Actions property */
                pTablePropActs = (tOfcTableFeatPropActs *) (VOID *)
                    (((UINT1 *) pActionHdr) + u1PadBytes);
                pTablePropActs->u2Type = OSIX_HTONS (OFCTFPT_APPLY_ACTIONS);
                pTablePropActs->u2Length =
                    OSIX_HTONS (sizeof (tOfcTableFeatPropActs));
                pActionHdr = (tOfcActionHdr *) (pTablePropActs->ActionIds);
                u4FourByteBitMap = pFlowTable->u4AppActsBitMap;

                while (u4FourByteBitMap && (u1SetCount != OFC_MAX_ACTIONS))
                {
                    if (u4FourByteBitMap & OFC_TRUE)
                    {
                        pActionHdr->u2Type = u1BitMapCount;
                        pActionHdr->u2Type = OSIX_HTONS (pActionHdr->u2Type);
                        pActionHdr->u2Length =
                            OSIX_HTONS (sizeof (tOfcActionHdr));
                        u1SetCount++;
                        pActionHdr++;
                    }
                    u1BitMapCount++;
                    u4FourByteBitMap >>= OFC_ONE;
                }
                pTablePropActs->u2Length =
                    (UINT2) (sizeof (tOfcTableFeatPropActs) +
                            (u1SetCount * sizeof (tOfcActionHdr)));
                /* Add neccessary padding */
                u1PadBytes = pTablePropActs->u2Length % OFC_EIGHT;
                if (u1PadBytes)
                {
                    u1PadBytes = (UINT1) (OFC_EIGHT - u1PadBytes);
                }
                pTableFeatures->u2Length =
                    (UINT2) (pTableFeatures->u2Length +
                            pTablePropActs->u2Length + u1PadBytes);
                pTablePropActs->u2Length =
                    OSIX_HTONS (pTablePropActs->u2Length);

                u1SetCount = OFC_ZERO;
                u1BitMapCount = OFC_ZERO;
                u4FourByteBitMap = OFC_ZERO;
                /* Fill the Match Fields Property */
                pTablePropOxm = (tOfcTableFeatPropOxm *) (VOID *)
                    (((UINT1 *) pActionHdr) + u1PadBytes);
                pTablePropOxm->u2Type = OSIX_HTONS (OFCTFPT_MATCH);
                pOxmTlv = (tOxmTlv *) (pTablePropOxm->au4OxmIds);

                for (u1Iter = OFC_ZERO; u1Iter < OFC_TWO; u1Iter++)
                {
                    if (u1Iter == OFC_ZERO)
                    {
                        u4FourByteBitMap = pFlowTable->u8FlowMatchBitMap.u4Lo;
                    }
                    else
                    {
                        u4FourByteBitMap = pFlowTable->u8FlowMatchBitMap.u4Hi;
                        u1BitMapCount = OFC_THIRTY_TWO;
                    }

                    while (u4FourByteBitMap)
                    {
                        if (u4FourByteBitMap & OFC_TRUE)
                        {
                            pOxmTlv->u2Class = OSIX_HTONS (OFPXMC_OPENFLOW_BASIC);
                            pOxmTlv->u1Field = (UINT1) (u1BitMapCount << OFC_ONE);
                            pOxmTlv->u1Len = (UINT1) sizeof (pOxmTlv->u1Field);
                            u1SetCount++;
                            pOxmTlv++;
                        }
                        else
                        {
                            /* go to next */
                        }
                        u4FourByteBitMap >>= OFC_ONE;
                        u1BitMapCount++;
                    }
                }
                pTablePropOxm->u2Length =
                    (UINT2) (sizeof (tOfcTableFeatPropOxm) +
                            (u1SetCount * sizeof (tOxmTlv)));
                /* Add neccessary padding */
                u1PadBytes = pTablePropOxm->u2Length % OFC_EIGHT;
                if (u1PadBytes)
                {
                    u1PadBytes = (UINT1) (OFC_EIGHT - u1PadBytes);
                }
                pTableFeatures->u2Length =
                    (UINT2) (pTableFeatures->u2Length +
                            pTablePropOxm->u2Length + u1PadBytes);
                pTablePropOxm->u2Length = OSIX_HTONS (pTablePropOxm->u2Length);

                u1SetCount = OFC_ZERO;
                u1BitMapCount = OFC_ZERO;
                u4FourByteBitMap = OFC_ZERO;
                /* Fill the WildCards Property */
                pTablePropOxm = (tOfcTableFeatPropOxm *) (VOID *)
                    (((UINT1 *) pOxmTlv) + u1PadBytes);
                pTablePropOxm->u2Type = OSIX_HTONS (OFCTFPT_WILDCARDS);
                pOxmTlv = (tOxmTlv *) (pTablePropOxm->au4OxmIds);

                for (u1Iter = OFC_ZERO; u1Iter < OFC_TWO; u1Iter++)
                {
                    if (u1Iter == OFC_ZERO)
                    {
                        u4FourByteBitMap = pFlowTable->u8WildCardsBitMap.u4Lo;
                    }
                    else
                    {
                        u4FourByteBitMap = pFlowTable->u8WildCardsBitMap.u4Hi;
                        u1BitMapCount = OFC_THIRTY_TWO;
                    }

                    while (u4FourByteBitMap)
                    {
                        if (u4FourByteBitMap & OFC_TRUE)
                        {
                            pOxmTlv->u2Class = OSIX_HTONS (OFPXMC_OPENFLOW_BASIC);
                            pOxmTlv->u1Field = (UINT1) (u1BitMapCount << OFC_ONE);
                            pOxmTlv->u1Len = (UINT1) sizeof (pOxmTlv->u1Field);
                            u1SetCount++;
                            pOxmTlv++;
                        }
                        else
                        {
                            /* go to next */
                        }
                        u4FourByteBitMap >>= OFC_ONE;
                        u1BitMapCount++;
                    }
                }
                pTablePropOxm->u2Length =
                    (UINT2) (sizeof (tOfcTableFeatPropOxm) +
                            (u1SetCount * sizeof (tOxmTlv)));
                /* Add neccessary padding */
                u1PadBytes = pTablePropOxm->u2Length % OFC_EIGHT;
                if (u1PadBytes)
                {
                    u1PadBytes = (UINT1) (OFC_EIGHT - u1PadBytes);
                }
                pTableFeatures->u2Length =
                    (UINT2) (pTableFeatures->u2Length +
                            pTablePropOxm->u2Length + u1PadBytes);
                pTablePropOxm->u2Length = OSIX_HTONS (pTablePropOxm->u2Length);

                u1SetCount = OFC_ZERO;
                u1BitMapCount = OFC_ZERO;
                u4FourByteBitMap = OFC_ZERO;
                /* Fill the Write Actions - set fields property */
                pTablePropOxm = (tOfcTableFeatPropOxm *) (VOID *)
                    (((UINT1 *) pOxmTlv) + u1PadBytes);
                pTablePropOxm->u2Type = OSIX_HTONS (OFCTFPT_WRITE_SETFIELD);
                pOxmTlv = (tOxmTlv *) (pTablePropOxm->au4OxmIds);

                for (u1Iter = OFC_ZERO; u1Iter < OFC_TWO; u1Iter++)
                {
                    if (u1Iter == OFC_ZERO)
                    {
                        u4FourByteBitMap = pFlowTable->u8WrtStFldsBitMap.u4Lo;
                    }
                    else
                    {
                        u4FourByteBitMap = pFlowTable->u8WrtStFldsBitMap.u4Hi;
                        u1BitMapCount = OFC_THIRTY_TWO;
                    }

                    while (u4FourByteBitMap)
                    {
                        if (u4FourByteBitMap & OFC_TRUE)
                        {
                            pOxmTlv->u2Class = OSIX_HTONS (OFPXMC_OPENFLOW_BASIC);
                            pOxmTlv->u1Field = (UINT1) (u1BitMapCount << OFC_ONE);
                            pOxmTlv->u1Len = (UINT1) sizeof (pOxmTlv->u1Field);
                            u1SetCount++;
                            pOxmTlv++;
                        }
                        else
                        {
                            /* go to next */
                        }
                        u4FourByteBitMap >>= OFC_ONE;
                        u1BitMapCount++;
                    }
                }
                pTablePropOxm->u2Length =
                    (UINT2) (sizeof (tOfcTableFeatPropOxm) +
                            (u1SetCount * sizeof (tOxmTlv)));
                /* Add neccessary padding */
                u1PadBytes = pTablePropOxm->u2Length % OFC_EIGHT;
                if (u1PadBytes)
                {
                    u1PadBytes = (UINT1) (OFC_EIGHT - u1PadBytes);
                }
                pTableFeatures->u2Length =
                    (UINT2) (pTableFeatures->u2Length +
                            pTablePropOxm->u2Length + u1PadBytes);
                pTablePropOxm->u2Length = OSIX_HTONS (pTablePropOxm->u2Length);

                u1SetCount = OFC_ZERO;
                u1BitMapCount = OFC_ZERO;
                u4FourByteBitMap = OFC_ZERO;
                /* Fill the Apply Actions - Set Fields property */
                pTablePropOxm = (tOfcTableFeatPropOxm *) (VOID *)
                    (((UINT1 *) pOxmTlv) + u1PadBytes);
                pTablePropOxm->u2Type = OSIX_HTONS (OFCTFPT_APPLY_SETFIELD);
                pOxmTlv = (tOxmTlv *) (pTablePropOxm->au4OxmIds);

                for (u1Iter = OFC_ZERO; u1Iter < OFC_TWO; u1Iter++)
                {
                    if (u1Iter == OFC_ZERO)
                    {
                        u4FourByteBitMap = pFlowTable->u8AppStFldsBitMap.u4Lo;
                    }
                    else
                    {
                        u4FourByteBitMap = pFlowTable->u8AppStFldsBitMap.u4Hi;
                        u1BitMapCount = OFC_THIRTY_TWO;
                    }

                    while (u4FourByteBitMap)
                    {
                        if (u4FourByteBitMap & OFC_TRUE)
                        {
                            pOxmTlv->u2Class = OSIX_HTONS (OFPXMC_OPENFLOW_BASIC);
                            pOxmTlv->u1Field = (UINT1) (u1BitMapCount << OFC_ONE);
                            pOxmTlv->u1Len = (UINT1) sizeof (pOxmTlv->u1Field);
                            u1SetCount++;
                            pOxmTlv++;
                        }
                        else
                        {
                            /* go to next */
                        }
                        u4FourByteBitMap >>= OFC_ONE;
                        u1BitMapCount++;
                    }
                }
                pTablePropOxm->u2Length =
                    (UINT2) (sizeof (tOfcTableFeatPropOxm) +
                            (u1SetCount * sizeof (tOxmTlv)));
                /* Add neccessary padding */
                u1PadBytes = pTablePropOxm->u2Length % OFC_EIGHT;
                if (u1PadBytes)
                {
                    u1PadBytes = (UINT1) (OFC_EIGHT - u1PadBytes);
                }
                pTableFeatures->u2Length =
                    (UINT2) (pTableFeatures->u2Length +
                            pTablePropOxm->u2Length + u1PadBytes);
                pTablePropOxm->u2Length = OSIX_HTONS (pTablePropOxm->u2Length);

                /*
                 * We are not filling the experimenter list as we don't support
                 * it as of now.
                 */

                /* Update the Variable Length */
                u2VarLen = (UINT2) (u2VarLen + pTableFeatures->u2Length);
                pTableFeatures->u2Length =
                    OSIX_HTONS (pTableFeatures->u2Length);
                pTableFeatures = (tOfcTableFeatures *) (VOID *)
                    (((UINT1 *) pOxmTlv) + u1PadBytes);
            }
            else
            {
                continue;
            }
        }
        /* send routine */
        pMultiPartRply->u2Type = pMultipartReq->u2Type;
        pMultiPartRply->u2Flags = OFC_ZERO;    /* last message */
        Ofc131PktMultiPartRplySend (pMultiPartRply, pConnPtr,
                (UINT4) (u2VarLen - OFC_EIGHT));
    }
    MemReleaseMemBlock (OFC_MULTIPART_POOLID, (UINT1 *) pMultiPartRply);

    if (u1ClearFlows == OFC_ONE)
    {
        /* delete all the flows in all the tables */
        Ofc131FlowDeleteAllFlows (pOfcFsofcCfgEntry);
    }
    UNUSED_PARAM (pBuf);
    OFC_TRC_FUNC ((OFC_FN_ENTRY, "FUNC:Ofc131MultipartTableFeatures\n"));
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function Name : Ofc131MultipartFillIndStats 
 * Description   : This routine fills  multi part reply from flow input
 * Input(s)      : pFlStReply - Pointer to flow stats reply
 *                 pFlow      - Pointer to flow
 * Output(s)     : None 
 * Return(s)     : None                              
 *****************************************************************************/
PRIVATE VOID
Ofc131MultipartFillIndStats (tOfcFlowStatsReply * pFlStReply,
                             tOfcFsofcFlowEntry * pFlow)
{
    AR_UINT8            u8TempVal = OFC_ZERO;
    UINT1               u1PadBytes = OFC_ZERO;
    UINT1               *pFlStReplyTemp = NULL;

    /* Read the statistics of each flow */
#if defined (NPAPI_WANTED) && defined (OPENFLOW_TCAM)
    UINT1               u1RetVal = OFC_ZERO;
    tOfcHwInfo          OfcHwInfo;

    MEMSET (&OfcHwInfo, OFC_ZERO, sizeof (tOfcHwInfo));

#ifdef DPA_WANTED
    if (OfcGetDpaTableId ((UINT1) pFlow->u4TableIndex, &pFlStReply->u1TableId)
        == OFC_FAILURE)
    {
        return;
    }
#else
    pFlStReply->u1TableId = (UINT1) (pFlow->u4TableIndex);
#endif /* DPA_WANTED */
    OfcHwInfo.OfcVer = OFC_VER_131;
    OfcHwInfo.OfcCmd = OFC_NP_GET_FLOW_STATS;
    OfcHwInfo.OfcHwEntry.OfcHwFlowInfo.u1TableId = pFlStReply->u1TableId;
    OfcHwInfo.OfcHwEntry.OfcHwFlowInfo.u2Priority = pFlow->u2Priority;
    OfcHwInfo.OfcHwEntry.OfcHwFlowInfo.u8Cookie.u4Hi = pFlow->u8Cookie.u4Hi;
    OfcHwInfo.OfcHwEntry.OfcHwFlowInfo.u8Cookie.u4Lo = pFlow->u8Cookie.u4Lo;
    OfcHwInfo.OfcHwEntry.OfcHwFlowInfo.pMatchList = &pFlow->MatchList;

    u1RetVal = OfcFsNpHwConfigOfcInfo (&OfcHwInfo);
    if (u1RetVal == FNP_FAILURE)
    {
        /* no harm */
    }
    pFlStReply->u4DurSec =
        OSIX_HTONL (OfcHwInfo.OfcHwEntry.OfcFlowStats.u4DurSec);
    MEMCPY (&pFlStReply->u8PktCount,
            &OfcHwInfo.OfcHwEntry.OfcFlowStats.u8PktCount, OFC_EIGHT);
    MEMCPY (&pFlStReply->u8ByteCount,
            &OfcHwInfo.OfcHwEntry.OfcFlowStats.u8ByteCount, OFC_EIGHT);
#else
    pFlStReply->u4DurSec = OSIX_HTONL (pFlow->MibObject.u4FsofcFlowDurationSec);
    pFlStReply->u8PktCount.u4Hi =
        OSIX_HTONL (pFlow->MibObject.u8FsofcFlowPacketCount.msn);
    pFlStReply->u8PktCount.u4Lo =
        OSIX_HTONL (pFlow->MibObject.u8FsofcFlowPacketCount.lsn);
    pFlStReply->u8ByteCount.u4Hi =
        OSIX_HTONL (pFlow->MibObject.u8FsofcFlowByteCount.msn);
    pFlStReply->u8ByteCount.u4Lo =
        OSIX_HTONL (pFlow->MibObject.u8FsofcFlowByteCount.lsn);
#endif /* OPENFLOW_TCAM && NPAPI_WANTED */

    pFlStReply->u8Cookie.u4Hi = OSIX_HTONL (pFlow->u8Cookie.u4Hi);
    pFlStReply->u8Cookie.u4Lo = OSIX_HTONL (pFlow->u8Cookie.u4Lo);
    pFlStReply->u4DurNSec = OFC_ZERO;
    pFlStReply->u2Priority = OSIX_HTONS (pFlow->u2Priority);
    pFlStReply->u2IdleTO =
        (UINT2) OSIX_HTONL (pFlow->MibObject.u4FsofcFlowIdleTimeout);
    pFlStReply->u2HardTO =
        (UINT2) OSIX_HTONL (pFlow->MibObject.u4FsofcFlowHardTimeout);
    pFlStReply->u2Flags = OSIX_HTONS (pFlow->u2Flags);

    /* Convert to Network Byte Order */
    MEMCPY (&u8TempVal, &pFlStReply->u8PktCount, OFC_EIGHT);
    u8TempVal = Ofcinputhton64 (&u8TempVal);
    MEMCPY (&pFlStReply->u8PktCount, &u8TempVal, OFC_EIGHT);

    MEMCPY (&u8TempVal, &pFlStReply->u8ByteCount, OFC_EIGHT);
    u8TempVal = Ofcinputhton64 (&u8TempVal);
    MEMCPY (&pFlStReply->u8ByteCount, &u8TempVal, OFC_EIGHT);

    pFlStReply->u2Length =
                 (UINT2) ((UINT2) sizeof (tOfcFlowStatsReply) - (UINT2) sizeof (tOfp131Match) +
                 OSIX_NTOHS (pFlStReply->Match.u2Length));

    /* A.2.3.1 Check if padding is required in ofp_match to make it multiple of 8 */
    u1PadBytes = (UINT1) (OFC_EIGHT - (OSIX_HTONS(pFlStReply->Match.u2Length) % OFC_EIGHT)); 
    if(u1PadBytes)
    {
        pFlStReplyTemp = (UINT1 *)pFlStReply;
        pFlStReplyTemp = pFlStReplyTemp + pFlStReply->u2Length;
        MEMSET(pFlStReplyTemp, OFC_ZERO, u1PadBytes);
        pFlStReply->u2Length = (UINT2) (pFlStReply->u2Length + (UINT2) u1PadBytes);
    }
    pFlStReply->u2Length = OSIX_HTONS (pFlStReply->u2Length);
}

/*****************************************************************************
 * Function Name : Ofc131MultipartIndividualFlowStats
 * Description   : This routine sends a multi part reply of flow statistics
 *                 to the controller.
 * Input(s)      : pMlPtReq - MultiPart Request                  
 *                 pConnPtr - Connection Pointer                               
 *                 pBuf     - CRU Buffer Pointer                               
 *                 pu4Error - Pointer to Error type                        
 * Output(s)     : None                                                    
 * Return(s)     : OFC_SUCCESS OR OFC_FAILURE                              
 *****************************************************************************/
INT4
Ofc131MultipartIndividualFlowStats (tOfp131MultiPartReq * pMlPtReq,
                                    VOID *pConnPtr,
                                    tCRU_BUF_CHAIN_HEADER * pBuf,
                                    UINT4 *pu4Error)
{
    tRBExactFlowEntry   ExactFlowEntry;
    tOfcFsofcFlowEntry  FlowEntry;
    tOfcFlowStatsReq    FlStReq;    /* Flow Stats Req */
    tOfcSll             KeyMatchList;
    tRBExactFlowEntry  *pExactFlowEntry = NULL;
    tOfcFlowStatsReply *pFlStReply = NULL;    /* Flow Stats Reply */
    tOfp131MultiPartRply *pMlPtReply = NULL;    /* Multipart Reply */
    tOfcFsofcFlowTable *pFlowTable = NULL;
    tOfcFsofcFlowEntry *pFlow = NULL;
    tOfp131Match       *pOfpMatch = NULL;    /* Match as TLVs */
    tOfp131Match       *pTempMatch = NULL;
    UINT4               u4HashIndex = OFC_ZERO;
    UINT4               u4VarLen = OFC_ZERO;
    UINT4               u4TableId = OFC_ZERO;
    UINT1               u1FlowMatchCnt = OFC_ZERO;
    UINT1               u1IsFlowWild = OFC_ZERO;
    UINT1               u1StopFilling = OFC_ZERO;
    tOfcFsofcControllerConnEntry *pLocal = pConnPtr;
    tOfcFsofcCfgEntry  *pOfcFsofcCfgEntry = NULL;
#if defined (NPAPI_WANTED) && defined (OPENFLOW_TCAM)
#ifdef DPA_WANTED
    UINT1               u1LocalTabId = OFC_ZERO;
#endif
#endif /* NPAPI_WANTED && OPENFLOW_TCAM */
    OFC_TRC_FUNC ((OFC_FN_ENTRY, "FUNC:Ofc131MultipartIndividualFlowStats\n"));

    MEMSET (&FlowEntry, OFC_ZERO, sizeof (tOfcFsofcFlowEntry));
    MEMSET (&ExactFlowEntry, OFC_ZERO, sizeof (tRBExactFlowEntry));

    pOfpMatch = (tOfp131Match *) (VOID *) (pLocal->pRcvPkt +
                                           OFC131_FLOWMOD_OFPMATCH_OFFSET);

    /* Allocate Memory for the Multipart reply to be sent */
    pMlPtReply = (tOfp131MultiPartRply *) MemAllocMemBlk (OFC_MULTIPART_POOLID);
    if (pMlPtReply == NULL)
    {
        OFC_TRC_FUNC ((OFC_ACTS_TRC, "MemAlloc Failed for pMPReply, "
                       "return FAILURE\n"));
        return OFC_FAILURE;
    }

    pOfcFsofcCfgEntry =
        OfcGetFsofcCfgEntry (pLocal->MibObject.u4FsofcContextId);
    if (pOfcFsofcCfgEntry == NULL)
    {
        OFC_TRC_FUNC ((OFC_ACTS_TRC, "\r\nOfc131MultipartIndividualFlowStats: "
                       "Get CFG Entry Failed\n"));
        MemReleaseMemBlock (OFC_MULTIPART_POOLID, (UINT1 *) pMlPtReply);
        return OFC_FAILURE;
    }

    MEMSET (pMlPtReply, OFC_ZERO, OFC_MAX_MULTIPART_MSG_SIZE);
    pMlPtReply->u2Type = pMlPtReq->u2Type;
    pMlPtReply->u2Flags = pMlPtReq->u2Flags;

    /* This memory is used for verifying we dont cross the allocated */
    pTempMatch = (tOfp131Match *) MemAllocMemBlk (OFC_FLOW_MATCH_TLV_POOLID);
    if (pTempMatch == NULL)
    {
        OFC_TRC_FUNC ((OFC_ACTS_TRC, "MemAlloc Failed for pTempMatch, "
                       "return FAILURE\n"));
        /* Release the mem-pools */
        MemReleaseMemBlock (OFC_MULTIPART_POOLID, (UINT1 *) pMlPtReply);
        return OFC_FAILURE;
    }
    MEMSET (pTempMatch, OFC_ZERO, OFC_MAX_FLOW_MATCH_TLV_SIZE);

    /* Copy the TLV flowmatch fields into flow match structure */
    if (Ofc131PktOxmParseMatchTlv (OFC_MINUS_ONE, pOfpMatch, &KeyMatchList,
                                   &u1FlowMatchCnt, &u1IsFlowWild,
                                   pu4Error) != OFC_SUCCESS)
    {
        OFC131_TRC_FUNC ((OFC_PKT_TRC, "Parsing Flow Match TLV Failed\n"));
        /* Free FlowMatch SLL */
        Ofc131FlowMatchListFree (OFC_MINUS_ONE, &KeyMatchList);
        /* Release the mem-pools */
        MemReleaseMemBlock (OFC_MULTIPART_POOLID, (UINT1 *) pMlPtReply);
        MemReleaseMemBlock (OFC_FLOW_MATCH_TLV_POOLID, (UINT1 *) pTempMatch);
        return OFC_FAILURE;
    }

    /*
     * Get the outport and outgroup, which will be used for flow matching
     * along with the flow match fields.
     */
    MEMSET (&FlStReq, OFC_ZERO, sizeof (tOfcFlowStatsReq));
    CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &FlStReq,
                               sizeof (tOfp131MultiPartReq) + OFC_EIGHT,
                               sizeof (tOfcFlowStatsReq));

    pFlStReply = (tOfcFlowStatsReply *) ((VOID *) pMlPtReply->au1Body);

    for (u4TableId = OFC_ZERO;
         (u4TableId < OFC_MAX_TABLES) && (u1StopFilling != OFC_ONE);
         u4TableId++)
    {
        if (FlStReq.u1TableId != OFPTT_ALL)
        {
            u4TableId = FlStReq.u1TableId;
#if defined (NPAPI_WANTED) && defined (OPENFLOW_TCAM)
#ifdef DPA_WANTED
            if (OfcGetLocalTableId (FlStReq.u1TableId, &u1LocalTabId)
                == OFC_FAILURE)
            {
                return OFC_FAILURE;
            }
            u4TableId = u1LocalTabId;
#endif /* DPA_WANTED */
#endif /* NPAPI_WANTED && OPENFLOW_TCAM */
        }

        pFlowTable = (tOfcFsofcFlowTable *)
            OfcGetFlowTableEntry (pOfcFsofcCfgEntry->MibObject.u4FsofcContextId,
                                  u4TableId);
        if (pFlowTable == NULL)
        {
            if (FlStReq.u1TableId != OFPTT_ALL)
            {
                MemReleaseMemBlock (OFC_MULTIPART_POOLID, (UINT1 *) pMlPtReply);
                MemReleaseMemBlock (OFC_FLOW_MATCH_TLV_POOLID,
                                    (UINT1 *) pTempMatch);
                return OFC_FAILURE;
            }
            else
            {
                /* continue checking in other tables */
                continue;
            }
        }

        /*
         * Exact Match can happen in one table alone and
         * match count should match
         */
        if ((FlStReq.u1TableId != OFPTT_ALL) &&
            (!(u1IsFlowWild)) && (u1FlowMatchCnt == pFlowTable->u1NumMatch))
        {
            /* parse the flow match SLL to fill in the exact match */
            if (Ofc131FlowExactSort (&KeyMatchList,
                                     ExactFlowEntry.au1ExactMatch) !=
                OFC_SUCCESS)
            {
                MemReleaseMemBlock (OFC_MULTIPART_POOLID, (UINT1 *) pMlPtReply);
                MemReleaseMemBlock (OFC_FLOW_MATCH_TLV_POOLID,
                                    (UINT1 *) pTempMatch);
                return OFC_FAILURE;
            }

            pExactFlowEntry = RBTreeGet (pFlowTable->ExactFlowEntry,
                                         (tRBElem *) & ExactFlowEntry);
            if (pExactFlowEntry != NULL)
            {
                /* scan through the list and match output port and group */
                TMO_SLL_Scan (&(pExactFlowEntry->FlowEntryList), pFlow,
                              tOfcFsofcFlowEntry *)
                {
                    /* Check if output port matches */
                    if ((FlStReq.u4OutPort != OSIX_HTONL (OFPP_ANY)) &&
                        (FlStReq.u4OutPort != OSIX_HTONL (pFlow->u4OutPort)))
                    {
                        continue;
                    }

                    /* Check if output group matches */
                    if ((FlStReq.u4Outgroup != OSIX_HTONL (OFPG_ANY)) &&
                        (FlStReq.u4Outgroup != OSIX_HTONL (pFlow->u4OutGrp)))
                    {
                        continue;
                    }

                    Ofc131PktMatchStructToTlv (&pFlow->MatchList,
                                               pFlow->u1FlowMatchCount,
                                               pTempMatch);

                    /* check if we actually have the memory to hold this stats */
                    if ((OFC_MAX_MULTIPART_MSG_SIZE < (u4VarLen +
                                                       sizeof
                                                       (tOfcFlowStatsReply) +
                                                       OSIX_NTOHS
                                                       (pTempMatch->u2Length))))
                    {
                        /*
                         * we ran out of memory to fill for further stats,
                         * send the stats which are accumulated till now.
                         */
                        u1StopFilling = OFC_ONE;
                        break;
                    }
                    MEMSET (pFlStReply, OFC_ZERO, sizeof (tOfcFlowStatsReply));
                    MEMCPY (&(pFlStReply->Match), pTempMatch,
                            OSIX_NTOHS (pTempMatch->u2Length));

                    Ofc131MultipartFillIndStats (pFlStReply, pFlow);

                    u4VarLen += OSIX_NTOHS (pFlStReply->u2Length);
                    pFlStReply = (tOfcFlowStatsReply *) ((VOID *) ((UINT1 *)
                                                                   pFlStReply +
                                                                   OSIX_NTOHS
                                                                   (pFlStReply->
                                                                    u2Length)));
                }
                u1StopFilling = OFC_ONE;
                continue;
            }
            else
            {
                /* exact match failed */
                MemReleaseMemBlock (OFC_MULTIPART_POOLID, (UINT1 *) pMlPtReply);
                MemReleaseMemBlock (OFC_FLOW_MATCH_TLV_POOLID,
                                    (UINT1 *) pTempMatch);
                return OFC_FAILURE;
            }
        }

        TMO_HASH_Scan_Table (pFlowTable->pFlowtable, u4HashIndex)
        {
            if (u1StopFilling)
            {
                break;
            }

            TMO_HASH_Scan_Bucket (pFlowTable->pFlowtable,
                                  u4HashIndex, pFlow, tOfcFsofcFlowEntry *)
            {
                /* Check if output port matches */
                if ((FlStReq.u4OutPort != OSIX_HTONL (OFPP_ANY)) &&
                    (FlStReq.u4OutPort != OSIX_HTONL (pFlow->u4OutPort)))
                {
                    continue;
                }

                /* Check if output group matches */
                if ((FlStReq.u4Outgroup != OSIX_HTONL (OFPG_ANY)) &&
                    (FlStReq.u4Outgroup != OSIX_HTONL (pFlow->u4OutGrp)))
                {
                    continue;
                }

                /* Check if flow matches */
                if (Ofc131FlowIsFlowMatched (u1FlowMatchCnt, &KeyMatchList,
                                             pFlow) != OFC_SUCCESS)
                {
                    continue;
                }

                Ofc131PktMatchStructToTlv (&pFlow->MatchList,
                                           pFlow->u1FlowMatchCount, pTempMatch);

                /* check if we actually have the memory to hold this stats */
                if ((OFC_MAX_MULTIPART_MSG_SIZE < (u4VarLen +
                                                   sizeof (tOfcFlowStatsReply) +
                                                   OSIX_NTOHS
                                                   (pTempMatch->u2Length))))
                {
                    /*
                     * we ran out of memory to fill for further stats,
                     * send the stats which are accumulated till now.
                     */
                    u1StopFilling = OFC_ONE;
                    break;
                }
                MEMSET (pFlStReply, OFC_ZERO, sizeof (tOfcFlowStatsReply));
                MEMCPY (&(pFlStReply->Match), pTempMatch,
                        OSIX_NTOHS (pTempMatch->u2Length));

                Ofc131MultipartFillIndStats (pFlStReply, pFlow);

                u4VarLen += OSIX_NTOHS (pFlStReply->u2Length);
                pFlStReply =
                    (tOfcFlowStatsReply
                     *) ((VOID *) ((UINT1 *) pFlStReply +
                                   OSIX_NTOHS (pFlStReply->u2Length)));
            }
        }

        if (FlStReq.u1TableId != OFPTT_ALL)
        {
            break;
        }
    }

    /*Send routine */
    Ofc131PktMultiPartRplySend (pMlPtReply, pConnPtr, u4VarLen);

    /* Free FlowMatch SLL */
    Ofc131FlowMatchListFree (OFC_MINUS_ONE, &KeyMatchList);
    MemReleaseMemBlock (OFC_FLOW_MATCH_TLV_POOLID, (UINT1 *) pTempMatch);
    MemReleaseMemBlock (OFC_MULTIPART_POOLID, (UINT1 *) pMlPtReply);

    UNUSED_PARAM (pu4Error);
    OFC_TRC_FUNC ((OFC_FN_EXIT, "FUNC:Ofc131MultipartIndividualFlowStats\n"));
    return OFC_SUCCESS;
}

/******************************************************************************
 * Function Name : Ofc131MultipartAggFlowStats
 * Description   : This routine sends a  multi part reply containing aggregate
 *                 flow information to the controller.
 * Input(s)      : pMlPtReq - MultiPartRequest
 *                 pConnPtr - Connection Pointer
 *                 pBuf     - CRU Buffer Pointer
 *                 pu4Error - Pointer to Error type
 * Output(s)     : None
 * Return(s)     : OSIX_SUCCESS OR OSIX_FAILURE
 *****************************************************************************/
INT4
Ofc131MultipartAggFlowStats (tOfp131MultiPartReq * pMlPtReq,
                             VOID *pConnPtr, tCRU_BUF_CHAIN_HEADER * pBuf,
                             UINT4 *pu4Error)
{
    tRBExactFlowEntry   ExactFlowEntry;
    tOfcFsofcFlowEntry  FlowEntry;
    tOfcAggStatsReq     AgStReq;    /* Agg. Stats Req */
    tOfcSll             KeyMatchList;
    tRBExactFlowEntry  *pExactFlowEntry = NULL;
    tOfcAggStatsReply  *pAgStReply = NULL;    /* Agg. Stats Reply */
    tOfp131MultiPartRply *pMlPtReply = NULL;    /* Multipart Reply */
    tOfcFsofcFlowTable *pFlowTable = NULL;
    tOfcFsofcFlowEntry *pFlow = NULL;
    tOfp131Match       *pOfpMatch = NULL;    /* Match as TLVs */
    AR_UINT8            u8TempVal = OFC_ZERO;
    UINT4               u4VarLen = OFC_ZERO;
    UINT4               u4TableId = OFC_ZERO;
    UINT4               u4HashIndex = OFC_ZERO;
    UINT1               u1FlowMatchCnt = OFC_ZERO;
    UINT1               u1IsFlowWild = OFC_ZERO;
    UINT1               au1ExactMatch[MAX_OFC_EXACT_LENGTH];
    tOfcFsofcControllerConnEntry *pLocal = pConnPtr;
    tOfcFsofcCfgEntry  *pOfcFsofcCfgEntry = NULL;

#if defined (NPAPI_WANTED) && defined (OPENFLOW_TCAM)
    UINT1               u1RetVal = OFC_ZERO;
    tOfcHwInfo          OfcHwInfo;
#ifdef DPA_WANTED
    UINT1               u1LocalTabId = OFC_ZERO;
    UINT1               u1DpaTabId = OFC_ZERO;
#endif /* DPA_WANTED */

    MEMSET (&OfcHwInfo, OFC_ZERO, sizeof (tOfcHwInfo));
#endif /* NPAPI_WANTED && OPENFLOW_TCAM */

    OFC_TRC_FUNC ((OFC_FN_ENTRY, "FUNC:Ofc131MultipartAggFlowStats\n"));

    MEMSET (&FlowEntry, OFC_ZERO, sizeof (tOfcFsofcFlowEntry));
    MEMSET (&ExactFlowEntry, OFC_ZERO, sizeof (tRBExactFlowEntry));
    MEMSET (au1ExactMatch, OFC_ZERO, MAX_OFC_EXACT_LENGTH);

    pOfpMatch = (tOfp131Match *) (VOID *) (pLocal->pRcvPkt +
                                           OFC131_FLOWMOD_OFPMATCH_OFFSET);

    /* Allocate Memory for the Multipart reply to be sent */
    pMlPtReply = (tOfp131MultiPartRply *) MemAllocMemBlk (OFC_MULTIPART_POOLID);
    if (pMlPtReply == NULL)
    {
        OFC_TRC_FUNC ((OFC_ACTS_TRC, "MemAlloc Failed for pMPReply, "
                       "return FAILURE\n"));
        return OFC_FAILURE;
    }

    pOfcFsofcCfgEntry =
        OfcGetFsofcCfgEntry (pLocal->MibObject.u4FsofcContextId);
    if (pOfcFsofcCfgEntry == NULL)
    {
        OFC_TRC_FUNC ((OFC_ACTS_TRC, "\r\nOfc131MultipartAggFlowStats: "
                       "Get CFG Entry Failed\n"));
        /* Release the mem-pool */
        MemReleaseMemBlock (OFC_MULTIPART_POOLID, (UINT1 *) pMlPtReply);
        return OFC_FAILURE;
    }

    MEMSET (pMlPtReply, OFC_ZERO, OFC_MAX_MULTIPART_MSG_SIZE);
    pMlPtReply->u2Type = pMlPtReq->u2Type;
    pMlPtReply->u2Flags = pMlPtReq->u2Flags;

    /* Copy the TLV flowmatch fields into flow match structure */
    if (Ofc131PktOxmParseMatchTlv (OFC_MINUS_ONE, pOfpMatch, &KeyMatchList,
                                   &u1FlowMatchCnt, &u1IsFlowWild,
                                   pu4Error) != OFC_SUCCESS)
    {
        OFC131_TRC_FUNC ((OFC_PKT_TRC, "Parsing Flow Match TLV Failed\n"));
        /* Free FlowMatch SLL */
        Ofc131FlowMatchListFree (OFC_MINUS_ONE, &KeyMatchList);
        /* Release the mem-pools */
        MemReleaseMemBlock (OFC_MULTIPART_POOLID, (UINT1 *) pMlPtReply);
        return OSIX_FAILURE;
    }

    /*
     * Get the outport and outgroup, which will be used for flow matching
     * along with the flow match fields.
     */
    MEMSET (&AgStReq, OFC_ZERO, sizeof (tOfcFlowStatsReq));
    CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &AgStReq,
                               sizeof (tOfp131MultiPartReq) + OFC_EIGHT,
                               sizeof (tOfcFlowStatsReq));

    pAgStReply = (tOfcAggStatsReply *) ((VOID *) pMlPtReply->au1Body);

    for (u4TableId = OFC_ZERO; u4TableId < OFC_MAX_TABLES; u4TableId++)
    {
        if (AgStReq.u1TableId != OFPTT_ALL)
        {
#if defined (NPAPI_WANTED) && defined (OPENFLOW_TCAM)
#ifdef DPA_WANTED
            /* Get Local Table ID frm DPA Table ID */
            if (OfcGetLocalTableId (AgStReq.u1TableId, &u1LocalTabId)
                == OFC_FAILURE)
            {
                return OFC_FAILURE;
            }
            u1DpaTabId = AgStReq.u1TableId;
            AgStReq.u1TableId = u1LocalTabId;
#endif /* DPA_WANTED */
#endif /* NPAPI_WANTED && OPENFLOW_TCAM */
            u4TableId = AgStReq.u1TableId;
        }
        pFlowTable = (tOfcFsofcFlowTable *)
            OfcGetFlowTableEntry (pOfcFsofcCfgEntry->MibObject.u4FsofcContextId,
                                  u4TableId);

        if (pFlowTable == NULL)
        {
            if (AgStReq.u1TableId != OFPTT_ALL)
            {
                MemReleaseMemBlock (OFC_MULTIPART_POOLID, (UINT1 *) pMlPtReply);
                return OFC_FAILURE;
            }

            /* else check for other tables */
            continue;
        }

        /*
         * Exact Match can happen in one table alone and
         * match count should match
         */
        if ((AgStReq.u1TableId != OFPTT_ALL) &&
            (u1FlowMatchCnt == pFlowTable->u1NumMatch))
        {
            /* parse the flow match SLL to fill in the exact match */
            if (Ofc131FlowExactSort (&KeyMatchList,
                                     ExactFlowEntry.au1ExactMatch) !=
                OFC_SUCCESS)
            {
                MemReleaseMemBlock (OFC_MULTIPART_POOLID, (UINT1 *) pMlPtReply);
                return OFC_FAILURE;
            }

            pExactFlowEntry = RBTreeGet (pFlowTable->ExactFlowEntry,
                                         (tRBElem *) & ExactFlowEntry);
            if (pExactFlowEntry != NULL)
            {
                /* scan through the list and match output port and group */
                TMO_SLL_Scan (&(pExactFlowEntry->FlowEntryList), pFlow,
                              tOfcFsofcFlowEntry *)
                {
                    /* Check if output port matches */
                    if ((AgStReq.u4OutPort != OSIX_HTONL (OFPP_ANY)) &&
                        (AgStReq.u4OutPort != OSIX_HTONL (pFlow->u4OutPort)))
                    {
                        continue;
                    }

                    /* Check if output group matches */
                    if ((AgStReq.u4OutGroup != OSIX_HTONL (OFPG_ANY)) &&
                        (AgStReq.u4OutGroup != OSIX_HTONL (pFlow->u4OutGrp)))
                    {
                        continue;
                    }

                    /* Read the statistics of each flow and cumulate the stats */
                    pAgStReply->u4FlowCount++;
#if defined (NPAPI_WANTED) && defined (OPENFLOW_TCAM)
                    OfcHwInfo.OfcVer = OFC_VER_131;
                    OfcHwInfo.OfcCmd = OFC_NP_GET_FLOW_STATS;
#ifdef DPA_WANTED
                    OfcHwInfo.OfcHwEntry.OfcHwFlowInfo.u1TableId = u1DpaTabId;
#else
                    OfcHwInfo.OfcHwEntry.OfcHwFlowInfo.u1TableId =
                        (UINT1) u4TableId;
#endif /* DPA_WANTED */
                    OfcHwInfo.OfcHwEntry.OfcHwFlowInfo.u2Priority =
                        pFlow->u2Priority;
                    OfcHwInfo.OfcHwEntry.OfcHwFlowInfo.u8Cookie.u4Hi =
                        pFlow->u8Cookie.u4Hi;
                    OfcHwInfo.OfcHwEntry.OfcHwFlowInfo.u8Cookie.u4Lo =
                        pFlow->u8Cookie.u4Lo;
                    OfcHwInfo.OfcHwEntry.OfcHwFlowInfo.pMatchList =
                        &pFlow->MatchList;

                    u1RetVal = OfcFsNpHwConfigOfcInfo (&OfcHwInfo);
                    if (u1RetVal == FNP_FAILURE)
                    {
                        /* no harm, continue */
                    }
                    MEMCPY (&pAgStReply->u8PktCount,
                            &OfcHwInfo.OfcHwEntry.OfcFlowStats.u8PktCount,
                            OFC_EIGHT);
                    MEMCPY (&pAgStReply->u8ByteCount,
                            &OfcHwInfo.OfcHwEntry.OfcFlowStats.u8ByteCount,
                            OFC_EIGHT);
#else
                    MEMCPY (&pAgStReply->u8PktCount,
                            &pFlow->MibObject.u8FsofcFlowPacketCount,
                            OFC_EIGHT);
                    MEMCPY (&pAgStReply->u8ByteCount,
                            &pFlow->MibObject.u8FsofcFlowPacketCount,
                            OFC_EIGHT);
#endif /* OPENFLOW_TCAM && NPAPI_WANTED */
                    /* Convert to Network Byte Order */
                    MEMCPY (&u8TempVal, &pAgStReply->u8PktCount, OFC_EIGHT);
                    u8TempVal = Ofcinputhton64 (&u8TempVal);
                    MEMCPY (&pAgStReply->u8PktCount, &u8TempVal, OFC_EIGHT);

                    MEMCPY (&u8TempVal, &pAgStReply->u8ByteCount, OFC_EIGHT);
                    u8TempVal = Ofcinputhton64 (&u8TempVal);
                    MEMCPY (&pAgStReply->u8ByteCount, &u8TempVal, OFC_EIGHT);
                }
                continue;
            }
            else
            {
                /* exact match failed */
                MemReleaseMemBlock (OFC_MULTIPART_POOLID, (UINT1 *) pMlPtReply);
                return OFC_FAILURE;
            }
        }

        TMO_HASH_Scan_Table (pFlowTable->pFlowtable, u4HashIndex)
        {
            TMO_HASH_Scan_Bucket (pFlowTable->pFlowtable,
                                  u4HashIndex, pFlow, tOfcFsofcFlowEntry *)
            {
                /* Check if flow matches */
                if (Ofc131FlowIsFlowMatched (u1FlowMatchCnt, &KeyMatchList,
                                             pFlow) != OFC_SUCCESS)
                {
                    continue;
                }

                /* Check if output port matches */
                if ((AgStReq.u4OutPort != OSIX_HTONL (OFPP_ANY)) &&
                    (AgStReq.u4OutPort != OSIX_HTONL (pFlow->u4OutPort)))
                {
                    continue;
                }

                /* Check if output group matches */
                if ((AgStReq.u4OutGroup != OSIX_HTONL (OFPG_ANY)) &&
                    (AgStReq.u4OutGroup != OSIX_HTONL (pFlow->u4OutGrp)))
                {
                    continue;
                }

                /* Read the statistics of each flow and cumulate the stats */
                pAgStReply->u4FlowCount++;
#if defined (NPAPI_WANTED) && defined (OPENFLOW_TCAM)
                OfcHwInfo.OfcVer = OFC_VER_131;
                OfcHwInfo.OfcCmd = OFC_NP_GET_FLOW_STATS;
#ifdef DPA_WANTED
                OfcHwInfo.OfcHwEntry.OfcHwFlowInfo.u1TableId = u1DpaTabId;
#else /* DPA_WANTED */
                OfcHwInfo.OfcHwEntry.OfcHwFlowInfo.u1TableId =
                    (UINT1) u4TableId;
#endif /* DPA_WANTED */
                OfcHwInfo.OfcHwEntry.OfcHwFlowInfo.u2Priority =
                    pFlow->u2Priority;
                OfcHwInfo.OfcHwEntry.OfcHwFlowInfo.u8Cookie.u4Hi =
                    pFlow->u8Cookie.u4Hi;
                OfcHwInfo.OfcHwEntry.OfcHwFlowInfo.u8Cookie.u4Lo =
                    pFlow->u8Cookie.u4Lo;
                OfcHwInfo.OfcHwEntry.OfcHwFlowInfo.pMatchList =
                    &pFlow->MatchList;

                u1RetVal = OfcFsNpHwConfigOfcInfo (&OfcHwInfo);
                if (u1RetVal == FNP_FAILURE)
                {
                    /* no harm, continue */
                }
                MEMCPY (&pAgStReply->u8PktCount,
                        &OfcHwInfo.OfcHwEntry.OfcFlowStats.u8PktCount,
                        OFC_EIGHT);
                MEMCPY (&pAgStReply->u8ByteCount,
                        &OfcHwInfo.OfcHwEntry.OfcFlowStats.u8ByteCount,
                        OFC_EIGHT);
#else /* NPAPI_WANTED && OPENFLOW_TCAM */
                pAgStReply->u8PktCount.u4Hi +=
                    OSIX_HTONL (pFlow->MibObject.u8FsofcFlowPacketCount.msn);
                pAgStReply->u8PktCount.u4Lo +=
                    OSIX_HTONL (pFlow->MibObject.u8FsofcFlowPacketCount.lsn);
                pAgStReply->u8ByteCount.u4Hi +=
                    OSIX_HTONL (pFlow->MibObject.u8FsofcFlowByteCount.msn);
                pAgStReply->u8ByteCount.u4Lo +=
                    OSIX_HTONL (pFlow->MibObject.u8FsofcFlowByteCount.lsn);
#endif /* NPAPI_WANTED && OPENFLOW_TCAM */
            }
        }

        if (AgStReq.u1TableId != OFPTT_ALL)
        {
            break;
        }
    }

    /* Send routine */
    u4VarLen = sizeof (tOfcAggStatsReply);
    Ofc131PktMultiPartRplySend (pMlPtReply, pConnPtr, u4VarLen);

    /* Free FlowMatch SLL */
    Ofc131FlowMatchListFree (OFC_MINUS_ONE, &KeyMatchList);
    /* release mempools */
    MemReleaseMemBlock (OFC_MULTIPART_POOLID, (UINT1 *) pMlPtReply);

    UNUSED_PARAM (pu4Error);
    OFC_TRC_FUNC ((OFC_FN_ENTRY, "FUNC:Ofc131MultipartAggFlowStats\n"));
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function Name : Ofc131MultipartFlowTableStats
 * Description   : This routine sends a multi part reply containing the flow
 *                 table statistics the controller.
 * Input(s)      : pMultiPartReq - MultiPart Request
 *                 pConnPtr      - Connection Pointer
 *                 pBuf          - CRU Buffer pointer
 *                 pu4Error      - Pointer to Error type
 * Output(s)     : None
 * Return(s)     : OSIX_SUCCESS OR OSIX_FAILURE
 *****************************************************************************/
INT4
Ofc131MultipartFlowTableStats (tOfp131MultiPartReq * pMultipartReq,
                               VOID *pConnPtr, tCRU_BUF_CHAIN_HEADER * pBuf,
                               UINT4 *pu4Error)
{
    tOfcFsofcControllerConnEntry *pLocal = NULL;
    tOfcFsofcCfgEntry  *pOfcFsofcCfgEntry = NULL;
    tOfcFsofcFlowTable *pFlowTable = NULL;
    tOfcTableStatsReply *pTableStatsReply = NULL;
    tOfp131MultiPartRply *pMultiPartRply = NULL;
    INT4                i4LoopCount = OFC_ZERO;
    INT4                i4FlowCount = OFC_ZERO;
    UINT4               u4VarLen = OFC_ZERO;

    OFC_TRC_FUNC ((OFC_FN_ENTRY, "FUNC:Ofc131MultipartFlowTableStats\n"));

    pLocal = (tOfcFsofcControllerConnEntry *) pConnPtr;
    pOfcFsofcCfgEntry =
        OfcGetFsofcCfgEntry (pLocal->MibObject.u4FsofcContextId);
    if (pOfcFsofcCfgEntry == NULL)
    {
        OFC_TRC_FUNC ((OFC_ACTS_TRC, "\r\nOfc131MultipartFlowTableStats: "
                       "Get CFG Entry Failed\n"));
        return OFC_FAILURE;
    }

    pMultiPartRply =
        (tOfp131MultiPartRply *) MemAllocMemBlk (OFC_MULTIPART_POOLID);
    if (pMultiPartRply == NULL)
    {
        OFC_TRC_FUNC ((OFC_ACTS_TRC, "Memory Allocation Failed\n"));
        return OFC_FAILURE;
    }
    pTableStatsReply =
        (tOfcTableStatsReply *) ((VOID *) (pMultiPartRply->au1Body));

    MEMSET (pTableStatsReply, OFC_ZERO, OFC_MAX_MULTIPART_MSG_SIZE);
    pMultiPartRply->u2Type = pMultipartReq->u2Type;
    pMultiPartRply->u2Flags = pMultipartReq->u2Flags;

    /* Looping to be done for number of tables */
    for (i4LoopCount = OFC_ZERO; i4LoopCount < pOfcFsofcCfgEntry->u2NumOfTables;
         i4LoopCount++)
    {
        /* Get the Flow Table entry */
        pFlowTable =
            OfcGetFlowTableEntry (pOfcFsofcCfgEntry->MibObject.u4FsofcContextId,
                                  (UINT4) i4LoopCount);
        if (pFlowTable != NULL)
        {
            /* Read the statistics for each table */
#ifdef DPA_WANTED
            /* Get Local Table ID frm DPA Table ID */
            if (OfcGetLocalTableId ((UINT1) pFlowTable->u4TableIndex,
                                    &pTableStatsReply->u1TableId) ==
                OFC_FAILURE)
            {
                return OFC_FAILURE;
            }
#else /* DPA_WANTED */
            pTableStatsReply->u1TableId =
                (UINT1) (pFlowTable->u4TableIndex);
#endif /* DPA_WANTED */
            pTableStatsReply->u4ActiveCount =
                OSIX_HTONL (pFlowTable->u4ActiveCount);
            pTableStatsReply->u8LookupCount.u4Hi =
                OSIX_HTONL (pFlowTable->u8LookupCount.u4Hi);
            pTableStatsReply->u8LookupCount.u4Lo =
                OSIX_HTONL (pFlowTable->u8LookupCount.u4Lo);
            pTableStatsReply->u8MatchedCount.u4Hi =
                OSIX_HTONL (pFlowTable->u8MatchedCount.u4Hi);
            pTableStatsReply->u8MatchedCount.u4Lo =
                OSIX_HTONL (pFlowTable->u8MatchedCount.u4Lo);
            pTableStatsReply++;
            i4FlowCount++;
        }
    }
    u4VarLen = (UINT4) (i4FlowCount * (INT4) (sizeof (tOfcTableStatsReply)));
    Ofc131PktMultiPartRplySend (pMultiPartRply, pConnPtr, u4VarLen);
    MemReleaseMemBlock (OFC_MULTIPART_POOLID, (UINT1 *) pMultiPartRply);
    UNUSED_PARAM (pu4Error);
    UNUSED_PARAM (pBuf);
    OFC_TRC_FUNC ((OFC_FN_EXIT, "FUNC:Ofc131MultipartFlowTableStats\n"));
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function Name : Ofc131MultipartMeterFeatures
 * Description   : This routine sends a multi part reply containing the
 *                 meter statistics the controller.
 * Input(s)      : pMultiPartReq - MultiPart Request
 *                 pConnPtr      - Connection Pointer
 *                 pBuf          - CRU Buffer Pointer
 *                 pu4Error      - Pointer to Error type
 * Output(s)     : None
 * Return(s)     : OSIX_SUCCESS OR OSIX_FAILURE
 *****************************************************************************/
INT4
Ofc131MultipartMeterFeatures (tOfp131MultiPartReq * pMultipartReq,
                              VOID *pConnPtr,
                              tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 *pu4Error)
{
    tOfcMeterFeatures  *pMeterFeatures = NULL;
    tOfp131MultiPartRply *pMultiPartRply = NULL;
    UINT4               u4VarLen = OFC_ZERO;

    OFC_TRC_FUNC ((OFC_FN_ENTRY, "FUNC:Ofc131MultipartMeterFeatures\n"));

    pMultiPartRply =
        (tOfp131MultiPartRply *) MemAllocMemBlk (OFC_MULTIPART_POOLID);

    if (pMultiPartRply == NULL)
    {
        /* Memory Allocation is failed */
        return OSIX_FAILURE;
    }
    pMultiPartRply->u2Type = pMultipartReq->u2Type;
    pMultiPartRply->u2Flags = pMultipartReq->u2Flags;
    pMeterFeatures = (tOfcMeterFeatures *) ((VOID *) (pMultiPartRply->au1Body));

    /* Get the Meter features of all the meters */
    MEMSET (pMeterFeatures, OFC_ZERO, sizeof (tOfcMeterFeatures));
    pMeterFeatures->u4BandTypes = OSIX_HTONL (OFC_METER_BANDTYPES);
    pMeterFeatures->u4Capabilities = OSIX_HTONL (OFC_METER_CAPABILITES);
    pMeterFeatures->u4MaxMeter = OSIX_HTONL ((UINT4) OFC_METER_MAXIMUM);
    pMeterFeatures->u1MaxBands = OFC_METER_MAX_BANDS;
    pMeterFeatures->u1Maxcolor = OFC_METER_MAX_COLOR;

    u4VarLen = sizeof (tOfcMeterFeatures);

    /* Multipart reply Send routine */
    Ofc131PktMultiPartRplySend (pMultiPartRply, pConnPtr, u4VarLen);
    MemReleaseMemBlock (OFC_MULTIPART_POOLID, (UINT1 *) pMultiPartRply);
    UNUSED_PARAM (pu4Error);
    UNUSED_PARAM (pBuf);
    OFC_TRC_FUNC ((OFC_FN_EXIT, "FUNC:Ofc131MultipartMeterFeatures\n"));
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function Name : Ofc131MultipartMeterConfig
 * Description   : This routine sends a multi part reply containing the
 *                 meter configuartion to the controller.
 * Input(s)      : pMultiPartReq - MultiPart Request
 *                 pConnPtr      - Connection Pointer
 *                 pBuf          - CRU Buffer Pointer
 *                 pu4Error      - Pointer to Error type
 * Output(s)     : None
 * Return(s)     : OSIX_SUCCESS OR OSIX_FAILURE
 *****************************************************************************/
INT4
Ofc131MultipartMeterConfig (tOfp131MultiPartReq * pMultipartReq,
                            VOID *pConnPtr,
                            tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 *pu4Error)
{
    tOfcFsofcMeterEntry MeterEntry;
    tOfcMeterReq        MeterReq;
    tOfcMeterConfig    *pMeterConfig = NULL;
    tOfp131MultiPartRply *pMultiPartRply = NULL;
    tOfcFsofcMeterEntry *pMeterEntry = NULL;
    tOfcMeterBand      *pMeterBand = NULL;
    tOfp131MeterBandHdr *pMeterHdr = NULL;
    UINT4               u4VarLen = OFC_ZERO;

    tOfcFsofcControllerConnEntry *pLocal = NULL;
    tOfcFsofcCfgEntry  *pOfcFsofcCfgEntry = NULL;

    OFC_TRC_FUNC ((OFC_FN_ENTRY, "FUNC:Ofc131MultipartMeterConfig\n"));

    MEMSET (&MeterEntry, OFC_ZERO, sizeof (tOfcFsofcMeterEntry));
    MEMSET (&MeterReq, OFC_ZERO, sizeof (tOfcMeterReq));

    pMultiPartRply =
        (tOfp131MultiPartRply *) MemAllocMemBlk (OFC_MULTIPART_POOLID);
    if (pMultiPartRply == NULL)
    {
        /* Memory Allocation is failed */
        return OSIX_FAILURE;
    }

    pLocal = (tOfcFsofcControllerConnEntry *) pConnPtr;
    pOfcFsofcCfgEntry =
        OfcGetFsofcCfgEntry (pLocal->MibObject.u4FsofcContextId);
    if (pOfcFsofcCfgEntry == NULL)
    {
        OFC_TRC_FUNC ((OFC_ACTS_TRC, "\r\nOfc131MultipartMeterConfig: "
                       "Get CFG Entry Failed\n"));
        MemReleaseMemBlock (OFC_MULTIPART_POOLID, (UINT1 *) pMultiPartRply);
        return OSIX_FAILURE;
    }

    pMultiPartRply->u2Type = pMultipartReq->u2Type;
    pMultiPartRply->u2Flags = pMultipartReq->u2Flags;

    /* 
     * Assigning the address of the body part of multipart
     * request to the Meter Request
     */
    CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &MeterReq,
                               sizeof (tOfp131MultiPartReq) + OFP_HEADER_LEN,
                               sizeof (tOfcMeterReq));

    pMeterConfig = (tOfcMeterConfig *) ((VOID *) (pMultiPartRply->au1Body));
    MEMSET (pMeterConfig, OFC_ZERO, sizeof (tOfcMeterConfig));

    MeterReq.u4MeterId = OSIX_NTOHL (MeterReq.u4MeterId);

    /*
     * Check whether the meter index is OFPM_ALL, if so,
     * send the meter configuations of the all the meter table.
     */
    if (MeterReq.u4MeterId == OFPM_ALL)
    {
        pMeterEntry =
            (tOfcFsofcMeterEntry *)
            RBTreeGetFirst (gOfcGlobals.OfcGlbMib.FsofcMeterTable);
        if (NULL == pMeterEntry)
        {
            Ofc131PktMultiPartRplySend (pMultiPartRply, pConnPtr, u4VarLen);
            MemReleaseMemBlock (OFC_MULTIPART_POOLID, (UINT1 *) pMultiPartRply);
            OFC_TRC_FUNC ((OFC_FN_EXIT, "FUNC:Ofc131MultipartMeterConfig\n"));
            return OSIX_SUCCESS;
        }
        else
        {
            pMeterHdr = (tOfp131MeterBandHdr *) ((VOID *) pMeterConfig->bands);
            do
            {
                /* 
                 * Make sure the amount of memory allocated is not crossing
                 * the boundary
                 */
                if (OFC_MAX_MULTIPART_MSG_SIZE < (u4VarLen +
                                                  sizeof (tOfcMeterConfig)))
                {
                    break;
                }
                /*
                 * Retrieve all the meters from the RBTree and
                 * send it to the controller
                 */
                pMeterConfig->u2Flags = OSIX_HTONS (pMeterEntry->u2Flags);
                pMeterConfig->u4Meterid =
                    OSIX_HTONL (pMeterEntry->MibObject.u4FsofcMeterIndex);
                pMeterConfig->u2Length = OFC_EIGHT;

                MEMSET (pMeterHdr, OFC_ZERO, sizeof (tOfp131MeterBandHdr));
                pMeterHdr = (tOfp131MeterBandHdr *) ((VOID *)
                                                     ((UINT1 *) pMeterConfig +
                                                      OFC_EIGHT));

                TMO_SLL_Scan (&pMeterEntry->BandList, pMeterBand,
                              tOfcMeterBand *)
                {
                    if (OFC_MAX_MULTIPART_MSG_SIZE < (u4VarLen +
                                                      pMeterConfig->u2Length +
                                                      sizeof
                                                      (tOfp131MeterBandHdr)))
                    {
                        break;
                    }
                    pMeterHdr->u2Type = OSIX_HTONS (pMeterBand->u2Type);
                    pMeterHdr->u2Len =
                        OSIX_HTONS (sizeof (tOfp131MeterBandHdr));
                    pMeterHdr->u4Rate = OSIX_HTONL (pMeterBand->u4Rate);
                    pMeterHdr->u4BurstSize =
                        OSIX_HTONL (pMeterBand->u4BurstSize);
                    pMeterConfig->u2Length =
                        (UINT2) ((pMeterConfig->u2Length) +
                                 (sizeof (tOfp131MeterBandHdr)));
                    pMeterHdr++;
                }

                /* 
                 * Update the total length of the mulipart message that needs to 
                 * to be sent to the controller
                 */
                u4VarLen += pMeterConfig->u2Length;

                pMeterConfig->u2Length = OSIX_HTONS (pMeterConfig->u2Length);

                pMeterConfig = (tOfcMeterConfig *) ((VOID *) pMeterHdr);
            }
            while ((pMeterEntry = (tOfcFsofcMeterEntry *) (RBTreeGetNext
                                                           (gOfcGlobals.
                                                            OfcGlbMib.
                                                            FsofcMeterTable,
                                                            pMeterEntry,
                                                            NULL))) != NULL);
        }
    }                            /* End of if condition for OFPM_ALL condition */
    else
    {
        /* 
         * Fetch the Particular meter information based on the 
         * meter index from the RBTree and then
         * send those configurations to the controller
         */
        MeterEntry.MibObject.u4FsofcMeterIndex = MeterReq.u4MeterId;
        MeterEntry.MibObject.u4FsofcContextId =
            pOfcFsofcCfgEntry->MibObject.u4FsofcContextId;
        pMeterEntry =
            RBTreeGet (gOfcGlobals.OfcGlbMib.FsofcMeterTable,
                       (tRBElem *) & MeterEntry);
        if (NULL != pMeterEntry)
        {
            /*
             * Make sure the amount of memory allocated is not
             * crossing the boundary
             */
            if ((OFC_MAX_MULTIPART_MSG_SIZE) < (u4VarLen +
                                                sizeof (tOfcMeterConfig)))
            {
                u4VarLen = OFC_ZERO;
            }
            else
            {
                pMeterConfig->u2Flags = OSIX_HTONS (pMeterEntry->u2Flags);
                pMeterConfig->u4Meterid =
                    OSIX_HTONL (pMeterEntry->MibObject.u4FsofcMeterIndex);
                pMeterConfig->u2Length = OFC_EIGHT;
                pMeterHdr = (tOfp131MeterBandHdr *) pMeterConfig->bands;
                TMO_SLL_Scan (&pMeterEntry->BandList, pMeterBand,
                              tOfcMeterBand *)
                {
                    if ((OFC_MAX_MULTIPART_MSG_SIZE) < ((u4VarLen +
                                                         pMeterConfig->u2Length
                                                         +
                                                         sizeof
                                                         (tOfp131MeterBandHdr))))
                    {
                        break;
                    }
                    MEMSET (pMeterHdr, OFC_ZERO, sizeof (tOfp131MeterBandHdr));
                    pMeterHdr->u2Type = OSIX_HTONS (pMeterBand->u2Type);
                    pMeterHdr->u2Len =
                        OSIX_HTONS (sizeof (tOfp131MeterBandHdr));
                    pMeterHdr->u4Rate = OSIX_HTONL (pMeterBand->u4Rate);
                    pMeterHdr->u4BurstSize =
                        OSIX_HTONL (pMeterBand->u4BurstSize);
                    pMeterConfig->u2Length = (UINT2) (pMeterConfig->u2Length +
                                                      (UINT2) (sizeof
                                                               (tOfp131MeterBandHdr)));
                    pMeterHdr++;
                }
                u4VarLen = pMeterConfig->u2Length;
                pMeterConfig->u2Length = OSIX_HTONS (pMeterConfig->u2Length);
            }                    /* End of Memory If condition */
        }
        else
        {
            u4VarLen = OFC_ZERO;
        }
    }
    Ofc131PktMultiPartRplySend (pMultiPartRply, pConnPtr, u4VarLen);
    MemReleaseMemBlock (OFC_MULTIPART_POOLID, (UINT1 *) pMultiPartRply);
    UNUSED_PARAM (pu4Error);
    OFC_TRC_FUNC ((OFC_FN_EXIT, "FUNC:Ofc131MultipartMeterConfig\n"));
    return OSIX_SUCCESS;
}

/******************************************************************************
 * Function Name : Ofc131MultipartMeterDesc
 * Description   : This routine sends a multi part reply containing the
 *                 meter Description to the controller.
 * Input(s)      : pMultiPartReq - MultiPart Request
 *                 pConnPtr      - Connection Pointer
 *                 pBuf          - CRU Buffer Pointer
 *                 pu4Error      - Pointer to Error type
 * Output(s)     : None                                                      
 * Return(s)     : OSIX_SUCCESS OR OSIX_FAILURE                              
 *****************************************************************************/
INT4
Ofc131MultipartMeterDesc (tOfp131MultiPartReq * pMultipartReq,
                          VOID *pConnPtr, tCRU_BUF_CHAIN_HEADER * pBuf,
                          UINT4 *pu4Error)
{
    tOfcFsofcMeterEntry MeterEntry;
    tOfcMeterReq        MeterReq;
    tOfcMeterBandStats *pMeterBandStats = NULL;
    tOfcMeterStats     *pMeterStats = NULL;
    tOfp131MultiPartRply *pMultiPartRply = NULL;
    tOfcFsofcMeterEntry *pMeterEntry = NULL;
    UINT4               u4VarLen = OFC_ZERO;

    tOfcFsofcControllerConnEntry *pLocal = NULL;
    tOfcFsofcCfgEntry  *pOfcFsofcCfgEntry = NULL;

    OFC_TRC_FUNC ((OFC_FN_ENTRY, "FUNC:Ofc131MultipartMeterDesc\n"));

    MEMSET (&MeterEntry, OFC_ZERO, sizeof (tOfcFsofcMeterEntry));
    MEMSET (&MeterReq, OFC_ZERO, sizeof (tOfcMeterReq));

    pMultiPartRply =
        (tOfp131MultiPartRply *) MemAllocMemBlk (OFC_MULTIPART_POOLID);

    if (pMultiPartRply == NULL)
    {
        /* Memory Allocation is failed */
        return OSIX_FAILURE;
    }

    pLocal = (tOfcFsofcControllerConnEntry *) pConnPtr;
    pOfcFsofcCfgEntry =
        OfcGetFsofcCfgEntry (pLocal->MibObject.u4FsofcContextId);
    if (pOfcFsofcCfgEntry == NULL)
    {
        OFC_TRC_FUNC ((OFC_ACTS_TRC, "\r\nOfc131MultipartMeterDesc: "
                       "Get CFG Entry Failed\n"));
        MemReleaseMemBlock (OFC_MULTIPART_POOLID, (UINT1 *) pMultiPartRply);
        return OSIX_FAILURE;
    }

    pMultiPartRply->u2Type = pMultipartReq->u2Type;
    pMultiPartRply->u2Flags = pMultipartReq->u2Flags;

    /* 
     * Assigning the address of the body part of multipart
     * request to the Meter Request
     */
    CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &MeterReq,
                               sizeof (tOfp131MultiPartReq) + OFP_HEADER_LEN,
                               sizeof (tOfcMeterReq));

    pMeterStats = (tOfcMeterStats *) ((VOID *) (pMultiPartRply->au1Body));
    MEMSET (pMeterStats, OFC_ZERO, sizeof (tOfcMeterStats));

    MeterReq.u4MeterId = OSIX_NTOHL (MeterReq.u4MeterId);

    /* 
     * Check whether the meter index is OFPM_ALL,
     * if so send the meter Descriptions of the all the meter tables
     */
    if (MeterReq.u4MeterId == OFPM_ALL)
    {
        pMeterEntry =
            (tOfcFsofcMeterEntry *)
            RBTreeGetFirst (gOfcGlobals.OfcGlbMib.FsofcMeterTable);
        if (NULL == pMeterEntry)
        {
            Ofc131PktMultiPartRplySend (pMultiPartRply, pConnPtr, u4VarLen);
            MemReleaseMemBlock (OFC_MULTIPART_POOLID, (UINT1 *) pMultiPartRply);
            return OSIX_SUCCESS;
        }
        else
        {
            do
            {
                /*
                 * Make sure the amount of memory allocated is not crossing 
                 * the boundary
                 */
                if (OFC_MAX_MULTIPART_MSG_SIZE < (u4VarLen +
                                                  sizeof (tOfcMeterStats) +
                                                  sizeof (tOfcMeterBandStats)))
                {
                    break;
                }

                /*
                 * Retrieve all the meters from the RBTree and 
                 * then send it to the controller
                 */
                MEMSET (pMeterStats, OFC_ZERO, sizeof (tOfcMeterStats));

                pMeterStats->u4Meterid =
                    OSIX_HTONL (pMeterEntry->MibObject.u4FsofcMeterIndex);
                pMeterStats->u4FlowCount =
                    OSIX_HTONL (pMeterEntry->MibObject.u4FsofcMeterFlowCount);

                pMeterStats->u8PacketInCount.u4Hi =
                    OSIX_HTONL (pMeterEntry->MibObject.
                                u8FsofcMeterPacketInCount.msn);
                pMeterStats->u8PacketInCount.u4Lo =
                    OSIX_HTONL (pMeterEntry->MibObject.
                                u8FsofcMeterPacketInCount.lsn);
                pMeterStats->u8ByteInCount.u4Hi =
                    OSIX_HTONL (pMeterEntry->MibObject.
                                u8FsofcMeterByteInCount.msn);
                pMeterStats->u8ByteInCount.u4Lo =
                    OSIX_HTONL (pMeterEntry->MibObject.
                                u8FsofcMeterByteInCount.lsn);

                pMeterStats->u4DurSec =
                    OSIX_HTONL (pMeterEntry->MibObject.u4FsofcMeterDurationSec);
                pMeterStats->u4DurNSec = OFC_ZERO;

                pMeterStats->u2Len = sizeof (tOfcMeterStats) +
                    sizeof (tOfcMeterBandStats);

                pMeterBandStats = (tOfcMeterBandStats *) pMeterStats->bandstats;
                MEMSET (pMeterBandStats, OFC_ZERO, sizeof (tOfcMeterBandStats));
                pMeterBandStats++;
                u4VarLen += pMeterStats->u2Len;
                pMeterStats->u2Len = OSIX_HTONS (pMeterStats->u2Len);

                pMeterStats = (tOfcMeterStats *) pMeterBandStats;

                /*
                 * Update the total length of the mulipart message that 
                 * needs to tbe sent to the controller
                 */
            }
            while ((pMeterEntry = (tOfcFsofcMeterEntry *)
                    (RBTreeGetNext (gOfcGlobals.OfcGlbMib.FsofcMeterTable,
                                    pMeterEntry, NULL))) != NULL);
        }
    }                            /* End of if condition for OFPM_ALL condition */
    else
    {
        /* 
         * Fetch the particular meter information based on the meter index
         * from the RBTree and then send those statisctics to the controller
         */
        MeterEntry.MibObject.u4FsofcMeterIndex = MeterReq.u4MeterId;
        MeterEntry.MibObject.u4FsofcContextId =
            pOfcFsofcCfgEntry->MibObject.u4FsofcContextId;
        pMeterEntry =
            RBTreeGet (gOfcGlobals.OfcGlbMib.FsofcMeterTable,
                       (tRBElem *) & MeterEntry);
        if (NULL != pMeterEntry)
        {
            /*
             * Make sure the amount of memory allcoated is not crossing
             * the boundary
             */
            if ((OFC_MAX_MULTIPART_MSG_SIZE) > ((u4VarLen) +
                                                (sizeof (tOfcMeterStats)) +
                                                (sizeof (tOfcMeterBandStats))))
            {
                MEMSET (pMeterStats, OFC_ZERO, sizeof (tOfcMeterStats));
                pMeterStats->u4Meterid =
                    OSIX_HTONL (pMeterEntry->MibObject.u4FsofcMeterIndex);
                pMeterStats->u4FlowCount =
                    OSIX_HTONL (pMeterEntry->MibObject.u4FsofcMeterFlowCount);
                pMeterStats->u8PacketInCount.u4Hi =
                    OSIX_HTONL (pMeterEntry->MibObject.
                                u8FsofcMeterPacketInCount.msn);
                pMeterStats->u8PacketInCount.u4Lo =
                    OSIX_HTONL (pMeterEntry->MibObject.
                                u8FsofcMeterPacketInCount.lsn);
                pMeterStats->u8ByteInCount.u4Hi =
                    OSIX_HTONL (pMeterEntry->MibObject.
                                u8FsofcMeterByteInCount.msn);
                pMeterStats->u8ByteInCount.u4Lo =
                    OSIX_HTONL (pMeterEntry->MibObject.
                                u8FsofcMeterByteInCount.lsn);
                pMeterStats->u4DurSec =
                    OSIX_HTONL (pMeterEntry->MibObject.u4FsofcMeterDurationSec);

                pMeterStats->u4DurNSec = OFC_ZERO;
                pMeterStats->u2Len = sizeof (tOfcMeterStats);
                pMeterBandStats = (tOfcMeterBandStats *) pMeterStats->bandstats;
                MEMSET (pMeterBandStats, OFC_ZERO, sizeof (tOfcMeterBandStats));
                pMeterStats->u2Len = (UINT2) ((pMeterStats->u2Len) +
                                              (UINT2) (sizeof
                                                       (tOfcMeterBandStats)));
                u4VarLen = pMeterStats->u2Len;
                pMeterStats->u2Len = OSIX_HTONS (pMeterStats->u2Len);
            }                    /* End of Memory check if condition */
        }
        else
        {
            u4VarLen = OFC_ZERO;
        }
    }
    Ofc131PktMultiPartRplySend (pMultiPartRply, pConnPtr, u4VarLen);
    MemReleaseMemBlock (OFC_MULTIPART_POOLID, (UINT1 *) pMultiPartRply);

    UNUSED_PARAM (pu4Error);
    OFC_TRC_FUNC ((OFC_FN_EXIT, "FUNC:Ofc131MultipartMeterConfig\n"));
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function    : Ofc131MultipartQueueStats  
 * Description : This routine sends reply multipart message to the QueueStats
 *               multipart request.
 * Input       : pMultipartReq - Pointer to the Multipart Request Header
 *               pPkt          - Pointer to the OpenFlow packet CRU buffer
 *               pConnPtr      - Controller Connection Pointer
 * Output      : pu4Error      - Pointer to error type, incase 
 * Returns     : OFC_SUCCESS/OFC_FAILURE
 *****************************************************************************/
INT4
Ofc131MultipartQueueStats (tOfp131MultiPartReq * pMultipartReq,
                           VOID *pConnPtr, tCRU_BUF_CHAIN_HEADER * pPkt,
                           UINT4 *pu4Error)
{
    tOfcQueStatsReq     QueStatsReq;
    tOfcFsofcIfEntry    IfEntry;
    tIfCountersStruct   IfCounter;
    tOfcQueStatsReply  *pQueStatsReply = NULL;
    tOfcFsofcIfEntry   *pIfEntry = NULL;
    tOfp131MultiPartRply *pMultiPartRply = NULL;
    UINT4               u4PortNum = OFC_ZERO;
    UINT4               u4TempPortNum = OFC_ZERO;
    UINT4               u4QueueId = OFC_ZERO;
    INT4                i4RetVal = OFC_ZERO;
    UINT4               u4VarLen = OFC_ZERO;
    tOfcFsofcControllerConnEntry *pLocal = NULL;
    tOfcFsofcCfgEntry  *pOfcFsofcCfgEntry = NULL;
    FS_UINT8            u8Temp1, u8Temp2, u8Result;
    AR_UINT8            u8TempVar = OFC_ZERO;

    OFC_TRC_FUNC ((OFC_FN_ENTRY, "FUNC:Ofc131MultipartQueueStats\n"));

    MEMSET (&IfEntry, OFC_ZERO, sizeof (tOfcFsofcIfEntry));
    MEMSET (&u8Temp1, OFC_ZERO, OFC_EIGHT);
    MEMSET (&u8Temp2, OFC_ZERO, OFC_EIGHT);
    MEMSET (&u8Result, OFC_ZERO, OFC_EIGHT);
    /* 
     * We do maintain only one Queue per Port of which only MIN_RATE is active,
     * MAX_RATE property exists but is disabled. The statistics are actually of
     * the port itself, Once the support to add more queues per port and actual
     * Queue configuration is possible, 
     *
     * Multiple queues are supported as part of DPA porting
     */
    pMultiPartRply =
        (tOfp131MultiPartRply *) MemAllocMemBlk (OFC_MULTIPART_POOLID);
    if (pMultiPartRply == NULL)
    {
        OFC_TRC_FUNC ((OFC_ACTS_TRC, "Memory Allocation for "
                       "pMultiPartRply Failed\n"));
        return OFC_FAILURE;
    }
    MEMSET (pMultiPartRply, OFC_ZERO, OFC_MAX_MULTIPART_MSG_SIZE);

    pLocal = (tOfcFsofcControllerConnEntry *) pConnPtr;
    pOfcFsofcCfgEntry =
        OfcGetFsofcCfgEntry (pLocal->MibObject.u4FsofcContextId);
    if (pOfcFsofcCfgEntry == NULL)
    {
        OFC_TRC_FUNC ((OFC_ACTS_TRC, "\r\nOfc131MultipartQueueStats: "
                       "Get CFG Entry Failed\n"));
        MemReleaseMemBlock (OFC_MULTIPART_POOLID, (UINT1 *) pMultiPartRply);
        return OFC_FAILURE;
    }

    pMultiPartRply->u2Type = pMultipartReq->u2Type;
    pMultiPartRply->u2Flags = pMultipartReq->u2Flags;

    MEMSET (&QueStatsReq, OFC_ZERO, sizeof (tOfcQueStatsReq));
    CRU_BUF_Copy_FromBufChain (pPkt, (UINT1 *) &QueStatsReq,
                               sizeof (tOfp131MultiPartReq) + OFC_EIGHT,
                               sizeof (tOfcQueStatsReq));

    pQueStatsReply = (tOfcQueStatsReply *) ((VOID *) (pMultiPartRply->au1Body));

    u4PortNum = OSIX_NTOHL (QueStatsReq.u4PortNum);
    u4QueueId = OSIX_NTOHL (QueStatsReq.u4QueId);

#if defined (NPAPI_WANTED) && defined (OPENFLOW_TCAM)
#ifdef DPA_WANTED
    if (Ofc131DpaQueueStatsGet (u4PortNum, u4QueueId, pQueStatsReply, &u4VarLen)
        == OFC_SUCCESS)
    {
        Ofc131PktMultiPartRplySend (pMultiPartRply, pConnPtr, u4VarLen);
    }
    MemReleaseMemBlock (OFC_MULTIPART_POOLID, (UINT1 *) pMultiPartRply);
    return OFC_SUCCESS;
#endif /* DPA_WANTED */
#endif /* NPAPI_WANTED && OPENFLOW_TCAM */

    /* Check the queue id, just in case */
    if ((u4QueueId != OFPQ_ALL) && (u4QueueId != OFC_DEFAULT_QUEUE))
    {
        /* Return Error */
        *pu4Error = OFC131_QUEUE_INVALID_QUEUE;
        MemReleaseMemBlock (OFC_MULTIPART_POOLID, (UINT1 *) pMultiPartRply);
        return OFC_FAILURE;
    }

    if (u4PortNum != OFPP_ANY)
        /* Queue Stats for one port */
    {
        IfEntry.MibObject.i4FsofcIfIndex = (INT4) u4PortNum;
        IfEntry.MibObject.u4FsofcContextId =
            pOfcFsofcCfgEntry->MibObject.u4FsofcContextId;
        i4RetVal = OfcGetAllFsofcIfTable (&IfEntry);

        if (OSIX_SUCCESS != i4RetVal)
        {
            /* Return Error */
            *pu4Error = OFC131_QUEUE_INVALID_PORT;
            MemReleaseMemBlock (OFC_MULTIPART_POOLID, (UINT1 *) pMultiPartRply);
            return OFC_FAILURE;
        }
        pIfEntry = &IfEntry;
    }
    else
    {
        /* Queue Statistics for all the ports */
        pIfEntry =
            (tOfcFsofcIfEntry *) RBTreeGetFirst (gOfcGlobals.OfcGlbMib.
                                                 FsofcIfTable);
        if (pIfEntry == NULL)
        {
            /* Return Error */
            *pu4Error = OFC131_QUEUE_INVALID_PORT;
            MemReleaseMemBlock (OFC_MULTIPART_POOLID, (UINT1 *) pMultiPartRply);
            return OFC_FAILURE;
        }
    }

    while (pIfEntry != NULL)
    {
        u4TempPortNum = (UINT4) pIfEntry->MibObject.i4FsofcIfIndex;

        pQueStatsReply->u4PortNum = OSIX_HTONL (u4TempPortNum);
        pQueStatsReply->u4QueId = OFC_DEFAULT_QUEUE;

        MEMSET (&IfCounter, OFC_ZERO, sizeof (tIfCountersStruct));
        CfaGetIfCountersFromSoftware (u4TempPortNum, &IfCounter);

        /* transmitted packets */
        u8Temp1.u4Lo = OSIX_HTONL (IfCounter.u4OutUcastPkts);
        u8Temp2.u4Lo = OSIX_HTONL (IfCounter.u4OutMulticastPkts);
        FSAP_U8_ADD (&u8Result, &u8Temp1, &u8Temp2);
        u8Temp1.u4Lo = OSIX_HTONL (IfCounter.u4OutBroadcastPkts);
        FSAP_U8_ADD (&u8Result, &u8Temp1, &u8Result);
        MEMCPY (&pQueStatsReply->u8TxPkts, &u8Result, sizeof (FS_UINT8));
        /* transmitted bytes */
        u8Temp1.u4Lo = OSIX_HTONL (IfCounter.u4OutOctets);
        u8Temp1.u4Hi = OSIX_HTONL (IfCounter.u4HighOutOctets);
        MEMCPY (&u8TempVar, &u8Temp1, sizeof (FS_UINT8));
        u8TempVar = Ofcinputhton64 (&u8TempVar);
        MEMCPY (&pQueStatsReply->u8TxBytes, &u8TempVar, sizeof (FS_UINT8));
        /* tranmit errors */
        pQueStatsReply->u8TxErrors.u4Lo = OSIX_HTONL (IfCounter.u4OutErrors);

        /* We dont maintain the durations, so set to zero */
        pQueStatsReply->u4DurSec = OFC_ZERO;
        pQueStatsReply->u4DurationNSec = OFC_ZERO;

        /* Increment Length */
        u4VarLen += sizeof (tOfcQueStatsReply);
        if (u4PortNum != OFPP_ANY)
        {
            /* only for one port */
            break;
        }
        pQueStatsReply++;

        /* Validate memory for filling next entry */
        if (OFC_MAX_MULTIPART_MSG_SIZE <
            (u4VarLen + sizeof (tOfcQueStatsReply)))
        {
            /* We can quit now, but sending what all we can is fine! */
            break;
        }
        /* Fetch the Next Port */
        pIfEntry = (tOfcFsofcIfEntry *)
            RBTreeGetNext (gOfcGlobals.OfcGlbMib.FsofcIfTable, pIfEntry, NULL);
    }
    Ofc131PktMultiPartRplySend (pMultiPartRply, pConnPtr, u4VarLen);
    MemReleaseMemBlock (OFC_MULTIPART_POOLID, (UINT1 *) pMultiPartRply);
    OFC_TRC_FUNC ((OFC_FN_ENTRY, "FUNC:Ofc131MultipartQueueStats\n"));
    return OFC_SUCCESS;
}

/*****************************************************************************
 * Function    : Ofc131MultipartExperimenter  
 * Description : This routine sends reply multipart message to the Experimenter 
 *               multipart request.
 * Input       : pMultipartReq - Pointer to the Multipart Request Header
 *               pPkt          - Pointer to the OpenFlow packet CRU buffer
 *               pConnPtr      - Controller Connection Pointer
 * Output      : pu4Error      - Pointer to error type, incase 
 * Returns     : OFC_SUCCESS/OFC_FAILURE
 *****************************************************************************/
INT4
Ofc131MultipartExperimenter (tOfp131MultiPartReq * pMultipartReq,
                             VOID *pConnPtr, tCRU_BUF_CHAIN_HEADER * pPkt,
                             UINT4 *pu4Error)
{
    UNUSED_PARAM (pMultipartReq);
    UNUSED_PARAM (pConnPtr);
    UNUSED_PARAM (pPkt);
    /* 
     * We don't support any experimenter messages for now. Any additions
     * can be appropriately included here.
     */
    *pu4Error = OFC131_EXPERIMENTERHDR_U4EXPTYPE_VALIDATE_ERR;

    return OFC_FAILURE;
}
