/*****************************************************************************
 * Copyright (C) 2013 Aricent Inc . All Rights Reserved
 *
 * $Id: ofccntlr.c,v 1.8 2016/03/10 11:42:32 siva Exp $
 *
 * Description: This file contains the socket functions and initialisation
 *              routines for Open flow controller connections.
 *****************************************************************************/

#include "ofcinc.h"
#include "ofccli.h"
#include "ofcl.h"
#include "fssocket.h"
#include "utilcli.h"
#include "ofcpkt.h"
#include "ofc131pkt.h"
#include "ofccntlr.h"
#include "ofcflow.h"
#include "ofcapi.h"
#include "ofcinband.h"
#include "ofcsz.h"

/*****************************************************************************
 * Function    :  OfcControllerEventSend 
 * Description :  This function sends Pkt Read Event to OFC Task 
 * Input       :  i4SockFd  - Socket Descriptor
 *                u4MsgType - Message Type  
 * Output      :  None
 * Returns     :  OFC_SUCCESS/OFC_FAILURE 
 * ***************************************************************************/
PRIVATE INT4
OfcControllerEventSend (INT4 i4SockFd, UINT4 u4MsgType)
{
    tOfcQueMsg         *pCntlrMsgInfo = NULL;

    OFC_TRC_FUNC ((OFC_FN_ENTRY, "OfcControllerEventSend\n"));

    pCntlrMsgInfo = (tOfcQueMsg *) MemAllocMemBlk (OFC_QUEUE_POOLID);
    if (pCntlrMsgInfo == NULL)
    {
        return OFC_FAILURE;
    }
    MEMSET (pCntlrMsgInfo, OFC_ZERO, sizeof (tOfcQueMsg));
    pCntlrMsgInfo->u4MsgType = u4MsgType;
    pCntlrMsgInfo->i4SockFd = i4SockFd;

    if (OsixQueSend (gOfcGlobals.ofcQueId, (UINT1 *) &pCntlrMsgInfo,
                     OSIX_DEF_MSG_LEN) == OSIX_FAILURE)
    {
        OFC_TRC ((OFC_TASK_TRC, "Send OFC_PKT_RECV_EVENT Queue Send Failed\n"));
        MemReleaseMemBlock (OFC_QUEUE_POOLID, (UINT1 *) pCntlrMsgInfo);
        return OFC_FAILURE;
    }

    if (OsixEvtSend (gOfcGlobals.ofcTaskId, OFC_QUEUE_EVENT) == OSIX_FAILURE)
    {
        OFC_TRC ((OFC_TASK_TRC, "Unable to post event to OFC\n"));
        MemReleaseMemBlock (OFC_QUEUE_POOLID, (UINT1 *) pCntlrMsgInfo);
        return OFC_FAILURE;
    }
    KW_FALSEPOSITIVE_FIX (pCntlrMsgInfo);
    OFC_TRC_FUNC ((OFC_FN_EXIT, "OfcControllerEventSend\n"));
    return OFC_SUCCESS;
}

/*****************************************************************************
 * Function    :  OfcControllerPktInSocket
 * Description :  This function is a call back function that gets hit when the
 *                packets are received at the connected TCP socket 
 * Input       :  i4SockDesc - Socket Descriptor
 * Output      :  None
 * Returns     :  None
 * ***************************************************************************/
VOID
OfcControllerPktInSocket (INT4 i4SockDesc)
{
    OFC_TRC_FUNC ((OFC_FN_ENTRY, "CALLBK FUNC:OfcControllerPktInSocket\n"));

    if (OfcControllerEventSend (i4SockDesc, OFC_READ_FD) == OFC_FAILURE)
    {
        if (SelAddFd (i4SockDesc, OfcControllerPktInSocket) != OSIX_SUCCESS)
        {
            close (i4SockDesc);
            i4SockDesc = OFC_MINUS_ONE;
            return;
        }
    }
    OFC_TRC_FUNC ((OFC_FN_EXIT, "EXIT:OfcControllerPktInSocket\n"));
}

/*****************************************************************************
 * Function    :  OfcControllerPktInSocket
 * Description :  This function does the SelAddFd 
 * Input       :  i4SockDesc - Socket Descriptor
 * Output      :  None
 * Returns     :  None
 * ***************************************************************************/
VOID
OfcControllerPktOutSocket (INT4 i4SockDesc)
{
    OFC_TRC_FUNC ((OFC_FN_ENTRY, "CALLBK FUNC:OfcControllerPktOutSocket\n"));

    if (OfcControllerEventSend (i4SockDesc, OFC_WRITE_FD) == OFC_FAILURE)
    {
        if (SelAddFd (i4SockDesc, OfcControllerPktOutSocket) != OSIX_SUCCESS)
        {
            close (i4SockDesc);
            i4SockDesc = OFC_MINUS_ONE;
            return;
        }
    }
    OFC_TRC_FUNC ((OFC_FN_EXIT, "EXIT:OfcControllerPktOutSocket\n"));
}

/*****************************************************************************
 * Function    :  OfcControllerProcessSocketWrite 
 * Description :  This function processes WriteFd event 
 * Input       :  i4SockDesc  - Socket Descriptor
 *                u4ContextId - Context Id  
 * Output      :  None
 * Returns     :  None
 * ***************************************************************************/
VOID
OfcControllerProcessSocketWrite (UINT4 u4ContextId, INT4 i4SockDesc)
{
    tOfcFsofcControllerConnEntry *pCntlrConnEntry = NULL;

    OFC_TRC_FUNC ((OFC_FN_ENTRY, "OfcControllerPktOutSocket\n"));

    pCntlrConnEntry = OfcGetControllerConnTableEntry (i4SockDesc);

    if (pCntlrConnEntry != NULL)
    {
        switch (pCntlrConnEntry->MibObject.i4FsofcControllerConnState)
        {
            case OFC_CONNECT_INPROGRESS:
#ifndef BSDCOMP_SLI_WANTED
                OfcControllerSockConnect (pCntlrConnEntry);
#else
                pCntlrConnEntry->MibObject.i4FsofcControllerConnState =
                    OFC_CONNECTED;
                OfcUpdateCfgConnState (pCntlrConnEntry);
                if (SelAddFd
                    (pCntlrConnEntry->i4SocketDesc,
                     OfcControllerPktInSocket) != OSIX_SUCCESS)
                {
                    close (pCntlrConnEntry->i4SocketDesc);
                    pCntlrConnEntry->i4SocketDesc = OFC_MINUS_ONE;
                    return;
                }
#endif
                break;

            case OFC_CONNECTED:
                /* 
                 * At present, this case  won't occur, so ignore it.
                 */
                break;

            default:
                /* Ignore Event */
                break;
        }
    }
    UNUSED_PARAM (u4ContextId);
    OFC_TRC_FUNC ((OFC_FN_EXIT, "OfcControllerPktOutSocket\n"));
}

/*****************************************************************************
 * Function    :  OfcProcessReceivePacket 
 * Description :  This function receives and processes packets from the
 *                Controller 
 * Input       :  i4SockFd    - Socket Descriptor
 *                u4ContextId - Context Id  
 * Output      :  None
 * Returns     :  OFC_SUCCESS/OFC_FAILURE 
 *****************************************************************************/
INT4
OfcProcessReceivePacket (UINT4 u4ContextId, INT4 i4SockFd)
{
    tOfcFsofcControllerConnEntry *pCntlrEntry = NULL;
    tOfcFsofcCfgEntry  *pOfcFsofcCfgEntry = NULL;

    tCRU_BUF_CHAIN_HEADER *pPkt = NULL;
    INT4                i4OuterRcvBytes = OFC_ZERO;
    INT4                i4InnerRcvBytes = OFC_ZERO;
#ifdef SSL_WANTED
    UINT4               u4NumBytes = OFC_ZERO;
#endif
    UINT2               u2TempLen = OFC_ZERO;
    UINT1              *pTempPkt = NULL;
    OFC_TRC_FUNC ((OFC_FN_ENTRY, "CALLBK FUNC:OfcProcessReceivePacket\n"));

    pCntlrEntry = OfcGetControllerConnTableEntry (i4SockFd);
    while ((pCntlrEntry != NULL) && (pCntlrEntry->pRcvPkt != NULL))
    {
        if (pCntlrEntry->u4ReceivedLen < OFC131_HELLO_OFFSET)
        {
#ifdef SSL_WANTED
            if (pCntlrEntry->pssl != NULL)
            {
                u4NumBytes = OFC131_HELLO_OFFSET - pCntlrEntry->u4ReceivedLen;
                if (SSL_SUCCESS ==
                    SslArRead (pCntlrEntry->pssl,
                               (pCntlrEntry->pRcvPkt +
                                pCntlrEntry->u4ReceivedLen), &u4NumBytes))
                {
                    i4OuterRcvBytes = (INT4) u4NumBytes;
                }
                else
                {
                    OFC_TRC ((OFC_PKT_TRC, "SSL Read fails\n"));
                    if (SelAddFd (i4SockFd, OfcControllerPktInSocket) !=
                        OSIX_SUCCESS)
                    {
                        close (i4SockFd);
                        i4SockFd = OFC_MINUS_ONE;
                        return OSIX_FAILURE;
                    }
                    return OFC_FAILURE;
                }
            }
            else
#endif
            {
                i4OuterRcvBytes = recv (i4SockFd, (pCntlrEntry->pRcvPkt +
                                                   pCntlrEntry->u4ReceivedLen),
                                        (OFC131_HELLO_OFFSET -
                                         (INT4) pCntlrEntry->u4ReceivedLen),
                                        MSG_NOSIGNAL);
            }

            if (i4OuterRcvBytes < OFC_ZERO)
            {
                OFC_TRC ((OFC_PKT_TRC, "Receive fails\n"));
                if (SelAddFd (i4SockFd, OfcControllerPktInSocket) !=
                    OSIX_SUCCESS)
                {
                    close (i4SockFd);
                    i4SockFd = OFC_MINUS_ONE;
                    return OSIX_FAILURE;
                }
                return OFC_SUCCESS;
            }

            if (i4OuterRcvBytes == OFC_ZERO)
            {
                OFC_TRC ((OFC_PKT_TRC, "nothing to read\n"));
                return OFC_SUCCESS;
            }

            pCntlrEntry->u4ReceivedLen += (UINT4) i4OuterRcvBytes;
            if (pCntlrEntry->u4ReceivedLen == OFC131_HELLO_OFFSET)
            {
                pTempPkt = pCntlrEntry->pRcvPkt + OFC_TWO;
                MEMCPY (&u2TempLen, pTempPkt, OFC_TWO);
                pCntlrEntry->u4PendingLen =
                    (UINT4) ((UINT4) OSIX_NTOHS (u2TempLen) -
                             (UINT4) OFC131_HELLO_OFFSET);

                if (pCntlrEntry->u4PendingLen >
                    (OFC_MAX_CONTROLLER_PKT_LEN - OFC131_HELLO_OFFSET))
                {
                    OFC_TRC ((OFC_PKT_TRC, "Closing the connection\n"));
                    /* Stop all the running timers */
                    if (pCntlrEntry->ConnRetryTimer.tmrNode.u2Flags &
                        OFC_TMR_RUNNING)
                    {
                        OfcTmrStopTmr (&(pCntlrEntry->ConnRetryTimer));
                    }

                    if (pCntlrEntry->EchoReqTimer.tmrNode.u2Flags &
                        OFC_TMR_RUNNING)
                    {
                        OfcTmrStopTmr (&(pCntlrEntry->EchoReqTimer));
                    }

                    if (pCntlrEntry->EchoInterTimer.tmrNode.u2Flags &
                        OFC_TMR_RUNNING)
                    {
                        OfcTmrStopTmr (&(pCntlrEntry->EchoInterTimer));
                    }
                    SelRemoveFd (pCntlrEntry->i4SocketDesc);
                    close (pCntlrEntry->i4SocketDesc);
                    pCntlrEntry->i4SocketDesc = OFC_MINUS_ONE;
                    return OFC_SUCCESS;
                }
            }
            else
            {
                if (SelAddFd (i4SockFd, OfcControllerPktInSocket) !=
                    OSIX_SUCCESS)
                {
                    close (i4SockFd);
                    i4SockFd = OFC_MINUS_ONE;
                    return OSIX_FAILURE;
                }
                return OFC_SUCCESS;
            }
        }

        if (pCntlrEntry->u4ReceivedLen >= OFC131_HELLO_OFFSET)
        {
            if (pCntlrEntry->u4PendingLen > OFC_ZERO)
            {
#ifdef SSL_WANTED
                if (pCntlrEntry->pssl != NULL)
                {
                    u4NumBytes = pCntlrEntry->u4PendingLen;
                    if (SSL_SUCCESS ==
                        SslArRead (pCntlrEntry->pssl,
                                   (pCntlrEntry->pRcvPkt +
                                    pCntlrEntry->u4ReceivedLen), &u4NumBytes))
                    {
                        i4InnerRcvBytes = (INT4) u4NumBytes;
                    }
                    else
                    {
                        OFC_TRC ((OFC_PKT_TRC, "SSL Read fails\n"));
                        if (SelAddFd (i4SockFd, OfcControllerPktInSocket) !=
                            OSIX_SUCCESS)
                        {
                            close (i4SockFd);
                            i4SockFd = OFC_MINUS_ONE;
                            return OSIX_FAILURE;
                        }
                        return OFC_FAILURE;
                    }
                }
                else
#endif
                {
                    i4InnerRcvBytes = recv (i4SockFd, (pCntlrEntry->pRcvPkt +
                                                       pCntlrEntry->
                                                       u4ReceivedLen),
                                            (INT4) pCntlrEntry->u4PendingLen,
                                            MSG_NOSIGNAL);
                }
                if (i4InnerRcvBytes <= OFC_ZERO)
                {
                    OFC_TRC ((OFC_PKT_TRC, "Receive socket fails\n"));
                    if (SelAddFd (i4SockFd, OfcControllerPktInSocket) !=
                        OSIX_SUCCESS)
                    {
                        close (i4SockFd);
                        i4SockFd = OFC_MINUS_ONE;
                        return OSIX_FAILURE;
                    }
                    return OFC_SUCCESS;
                }

                if ((UINT4) i4InnerRcvBytes < pCntlrEntry->u4PendingLen)
                {
                    OFC_TRC ((OFC_PKT_TRC,
                              "Recvd pkt is less than expected\n"));
                    pCntlrEntry->u4ReceivedLen += (UINT4) i4InnerRcvBytes;
                    pCntlrEntry->u4PendingLen -= (UINT4) i4InnerRcvBytes;
                    if (SelAddFd (i4SockFd, OfcControllerPktInSocket) !=
                        OSIX_SUCCESS)
                    {
                        close (i4SockFd);
                        i4SockFd = OFC_MINUS_ONE;
                        return OSIX_FAILURE;
                    }
                    return OFC_SUCCESS;
                }
            }
            pCntlrEntry->u4RxLength = pCntlrEntry->u4ReceivedLen +
                (UINT4) i4InnerRcvBytes;
            pCntlrEntry->u4PendingLen = OFC_ZERO;
            pCntlrEntry->u4ReceivedLen = OFC_ZERO;

            /* processing packet received from the socket */
            /* Copy the recvd linear buf to cru buf */
            if ((pPkt = CRU_BUF_Allocate_MsgBufChain (pCntlrEntry->u4RxLength,
                                                      OFC_ZERO)) == NULL)
            {
                OFC_TRC ((OFC_PKT_TRC, "OfcProcessReceivePacket : "
                          " CRU Buf allocation failed\n"));
                return OSIX_FAILURE;
            }

            if (CRU_BUF_Copy_OverBufChain (pPkt, pCntlrEntry->pRcvPkt, OFC_ZERO,
                                           pCntlrEntry->u4RxLength) ==
                CRU_FAILURE)
            {
                OFC_TRC ((OFC_PKT_TRC, "OfcProcessReceivePacket : "
                          "Copying Linear Buffer to CRU Buffer failed\n"));
                CRU_BUF_Release_MsgBufChain (pPkt, FALSE);
                return OSIX_FAILURE;
            }

            pOfcFsofcCfgEntry =
                OfcGetFsofcCfgEntry (pCntlrEntry->MibObject.u4FsofcContextId);
            if (pOfcFsofcCfgEntry == NULL)
            {
                OFC_TRC ((OFC_PKT_TRC, "\r\nOfcProcessReceivePacket: "
                          "Get CFG Entry Failed\n"));
                CRU_BUF_Release_MsgBufChain (pPkt, FALSE);
                return OSIX_FAILURE;
            }

            if (pOfcFsofcCfgEntry->MibObject.i4FsofcSupportedVersion ==
                OFC_VERSION_V100)
            {
                OfcPktParse (pCntlrEntry->pRcvPkt, pCntlrEntry);
            }
            else
            {
                Ofc131PktParse (pPkt, pCntlrEntry);
            }
            CRU_BUF_Release_MsgBufChain (pPkt, FALSE);
            MEMSET (pCntlrEntry->pRcvPkt, OFC_ZERO, OFC_MAX_CONTROLLER_PKT_LEN);
        }
    }
    if (SelAddFd (i4SockFd, OfcControllerPktInSocket) != OSIX_SUCCESS)
    {
        close (i4SockFd);
        i4SockFd = OFC_MINUS_ONE;
        return OSIX_FAILURE;
    }

    UNUSED_PARAM (u4ContextId);
    OFC_TRC_FUNC ((OFC_FN_EXIT, "EXIT:OfcProcessReceivePacket\n"));

    return OFC_SUCCESS;
}

/*****************************************************************************
 * Function    :  OfcCreateController 
 * Description :  This function creates a TCP socket and connects to the 
 *                Controller and waits for Packet-Receive event 
 * Input       :  pCntlrConnEntry - Pointer to Controller Connection
 *                                  Entry 
 * Output      :  None
 * Returns     :  OFC_SUCCESS/OFC_FAILURE 
 *****************************************************************************/
INT4
OfcCreateController (tOfcFsofcControllerConnEntry * pCntlrConnEntry)
{
    tOfcFsofcControllerConnEntry PmrCntlrConnEntry;
    tOfcFsofcControllerConnEntry *pTempCntlrConnEntry = NULL;
    tOfcFsofcControllerConnEntry *pPmrCntlrConnEntry = NULL;
    INT4                i4SockDesc = OFC_ZERO;

    OFC_TRC_FUNC ((OFC_FN_ENTRY, "FUNC:OfcCreateController\n"));

    MEMSET (&PmrCntlrConnEntry, OFC_ZERO,
            sizeof (tOfcFsofcControllerConnEntry));

    pTempCntlrConnEntry = OfcGetFsofcControllerConnTable (pCntlrConnEntry);

    if (pTempCntlrConnEntry == NULL)
    {
        OFC_TRC ((OFC_TASK_TRC, "Controller Create Failed\n"));
        return OFC_FAILURE;
    }

    if (pTempCntlrConnEntry->MibObject.i4FsofcControllerConnBand ==
        OPENFLOW_CONNBAND_INB)
    {
        if (pTempCntlrConnEntry->MibObject.i4FsofcControllerConnAuxId ==
            OFC_ZERO)
        {

            /* Add this controller to the local inband database */
            OfcInBandAddCntrlInDataBase (pTempCntlrConnEntry);
        }
    }
    else
    {
        /* its out of band */
    }

    i4SockDesc = socket (AF_INET, SOCK_STREAM, IPPROTO_TCP);
    if (OFC_ZERO > i4SockDesc)
    {
        OFC_TRC ((OFC_TASK_TRC, "OFC CONTROLLER MANAGER TASK INIT FAILURE\n"));
        return OFC_FAILURE;
    }

    if (fcntl (i4SockDesc, F_SETFL, O_NONBLOCK) < OFC_ZERO)
    {
        OFC_TRC ((OFC_TASK_TRC, "Can't Set Sckt in NON BLK\n"));
        return OFC_FAILURE;
    }
    pTempCntlrConnEntry->i4SocketDesc = i4SockDesc;
    pTempCntlrConnEntry->u4IsRecieved = FALSE;
    pTempCntlrConnEntry->i4ConnRetryCount = OFC_ZERO;
    pTempCntlrConnEntry->MibObject.i4FsofcControllerConnState =
        OFC_NOT_CONNECTED;
    pTempCntlrConnEntry->pssl = NULL;
    if (pTempCntlrConnEntry->pRcvPkt == NULL)
    {
        pTempCntlrConnEntry->pRcvPkt =
            (UINT1 *) MemAllocMemBlk (OFC_BUFFER_POOLID);
        if (NULL == pTempCntlrConnEntry->pRcvPkt)
        {
            OFC_TRC ((OFC_PKT_TRC, "Memory allocation failed for"
                      "Receive Packet Buffer\n"));
            return OFC_FAILURE;
        }
    }

    if (pTempCntlrConnEntry->MibObject.i4FsofcControllerConnAuxId != OFC_ZERO)
    {
        MEMCPY (&PmrCntlrConnEntry, pTempCntlrConnEntry,
                sizeof (tOfcFsofcControllerConnEntry));
        PmrCntlrConnEntry.MibObject.i4FsofcControllerConnAuxId = OFC_ZERO;

        /* Check Whether the pramiary connection is present & up */
        pPmrCntlrConnEntry =
            RBTreeGet (gOfcGlobals.OfcGlbMib.FsofcControllerConnTable,
                       (tRBElem *) & PmrCntlrConnEntry);

        if ((pPmrCntlrConnEntry == NULL) ||
            (pPmrCntlrConnEntry->MibObject.i4FsofcControllerConnState !=
             OFC_CONNECTED))
        {
            return OFC_SUCCESS;
        }
    }
    OfcControllerSockConnect (pTempCntlrConnEntry);
    return OFC_SUCCESS;
}

/*****************************************************************************
 * Function    :  OfcControllerSockConnect 
 * Description :  This function issues a connect call for the given socket 
 * Input       :  pCntlrConnEntry - Pointer to Controller Connection
 *                                  Entry 
 * Output      :  None
 * Returns     :  None 
 *****************************************************************************/
VOID
OfcControllerSockConnect (tOfcFsofcControllerConnEntry * pCntlrConnEntry)
{

    struct sockaddr_in  ControllerAddr;
    INT4                i4ConnState = OFC_ZERO;
    INT4                i4RetValue = OFC_ZERO;
    UINT4               u4Optval = 0xffff;
    UINT1               au1TmpAddr[OFC_MAX_IP_LENGTH];
    tOfcFsofcCfgEntry  *pOfcFsofcCfgEntry = NULL;
    tOfcFsofcCfgEntry   OfcFsofcCfgEntry;
    tOfp131Hello        Ofp131Hello;
    tOfpHello           OfpHello;

    OFC_TRC_FUNC ((OFC_FN_ENTRY, "OfcControllerSockConnect\n"));

    MEMSET (&ControllerAddr, OFC_ZERO, sizeof (struct sockaddr_in));
    MEMSET (au1TmpAddr, OFC_ZERO, OFC_MAX_IP_LENGTH);
    MEMSET (&OfcFsofcCfgEntry, OFC_ZERO, sizeof (tOfcFsofcCfgEntry));

    CliStrToIp4Addr (pCntlrConnEntry->MibObject.au1FsofcControllerIpAddress,
                     au1TmpAddr);
    ControllerAddr.sin_addr.s_addr = INET_ADDR (((const char *) au1TmpAddr));
    ControllerAddr.sin_addr.s_addr =
        OSIX_NTOHL (ControllerAddr.sin_addr.s_addr);
    ControllerAddr.sin_family = AF_INET;
    ControllerAddr.sin_port =
        OSIX_HTONS (pCntlrConnEntry->MibObject.i4FsofcControllerConnPort);
    if (connect (pCntlrConnEntry->i4SocketDesc,
                 (struct sockaddr *) &ControllerAddr,
                 sizeof (ControllerAddr)) < OFC_ZERO)
    {
        if (errno == EINPROGRESS)
        {
            OFC_TRC ((OFC_TASK_TRC, "Connection in Progress\n"));
            i4ConnState = OFC_CONNECT_INPROGRESS;
            SelAddWrFd (pCntlrConnEntry->i4SocketDesc,
                        OfcControllerPktOutSocket);
        }
        else if ((errno == EISCONN) || (errno == EALREADY))
        {
            OFC_TRC ((OFC_TASK_TRC, "Socket already connected \n"));
            i4ConnState = OFC_CONNECTED;
        }
        else
        {
            OFC_TRC ((OFC_TASK_TRC, "Controller connect Failed\n"));
            i4ConnState = OFC_NOT_CONNECTED;
        }
    }
    else
    {
        i4ConnState = OFC_CONNECTED;
    }

    pCntlrConnEntry->MibObject.i4FsofcControllerConnState = i4ConnState;
    if (i4ConnState == OFC_CONNECTED)
    {
#ifdef SSL_WANTED
        if (pCntlrConnEntry->MibObject.
            i4FsofcControllerConnProtocol == OPENFLOW_CONNPROTO_SSL)
        {
            OfcControllerSockConnectSSL (pCntlrConnEntry);
            if (NULL == pCntlrConnEntry->pssl)
            {
                OFC_TRC ((OFC_TASK_TRC,
                          " OFC CONTROLLER SOCKCONNECT SSL FAILURE\n"));
                SelRemoveFd (pCntlrConnEntry->i4SocketDesc);
                close (pCntlrConnEntry->i4SocketDesc);
                pCntlrConnEntry->MibObject.i4FsofcControllerConnState =
                    OFC_NOT_CONNECTED;
                pCntlrConnEntry->i4SocketDesc = OFC_MINUS_ONE;
                return;
            }
            else
            {
                OFC_TRC ((OFC_TASK_TRC,
                          "OFC CONTROLLER SOCKCONNECT SSL SUCCESS\n"));
            }
        }
#endif

        if (setsockopt (pCntlrConnEntry->i4SocketDesc, SOL_SOCKET, SO_RCVBUF,
                        &(u4Optval), OFC_FOUR) < OFC_ZERO)
        {
            OFC_TRC ((OFC_TASK_TRC, "OFC CONTROLLER SETSOCKOPT FAILURE\n"));
        }
        pCntlrConnEntry->u4ReceivedLen = OFC_ZERO;
        pCntlrConnEntry->u4PendingLen = OFC_ZERO;

        if (SelAddFd (pCntlrConnEntry->i4SocketDesc, OfcControllerPktInSocket)
            != OSIX_SUCCESS)
        {
            close (pCntlrConnEntry->i4SocketDesc);
            pCntlrConnEntry->i4SocketDesc = OFC_MINUS_ONE;
            return;
        }

        /* No need to start timers for auxilary connections */
        if (pCntlrConnEntry->MibObject.i4FsofcControllerConnAuxId == OFC_ZERO)
        {
            OfcTmrStartTmr (&(pCntlrConnEntry->EchoInterTimer),
                            OFC_ECHOINTER_TMR, OFC_ECHOINTER_INTERVAL);

            /* For INFINITE RETRY */
            /* Update Auxiliary connections too */
            OfcUpdateControllerConnState (pCntlrConnEntry, OFC_CONNECTED);
        }

        /* Update the Client's Connection state */
        OfcUpdateCfgConnState (pCntlrConnEntry);
        OfcFsofcCfgEntry.MibObject.u4FsofcContextId =
            pCntlrConnEntry->MibObject.u4FsofcContextId;

        pOfcFsofcCfgEntry =
            RBTreeGet (gOfcGlobals.OfcGlbMib.FsofcCfgTable,
                       (tRBElem *) & (OfcFsofcCfgEntry));
        if (pOfcFsofcCfgEntry == NULL)
        {
            OFC_TRC_FUNC ((OFC_UTIL_TRC, "\r\nFUNC:TmrConnRetryTimer:"
                           "Get CFG Entry FAiled\n"));
            return;
        }

        if ((pOfcFsofcCfgEntry->u4OfcClientState == OFC_CONNECTED) &&
            (pOfcFsofcCfgEntry->MibObject.i4FsofcSwitchModeOnConnFailure ==
             OFC_SWITCHMODE_FAIL_STANDALONE))
        {
            /* 
             * Make the ports visible to the Openflow context 
             */
            i4RetValue =
                OfcHandleOpenflowPorts (OfcFsofcCfgEntry.MibObject.
                                        u4FsofcContextId,
                                        CFA_SUBTYPE_OPENFLOW_INTERFACE);

            if (i4RetValue == OFC_FAILURE)
            {
                return;
            }
        }
        /* Switch will send the Hello Message to the controller 
         * instead of waiting for the Hello mesage from the controller */
        if (pOfcFsofcCfgEntry->MibObject.i4FsofcSupportedVersion ==
                OFC_VERSION_V131)
        {
            Ofc131PktHelloSend (&Ofp131Hello, pCntlrConnEntry, OFC_FOUR);
        }
        else
        {
            OfcHelloPktSend (&OfpHello, pCntlrConnEntry);
        }
    }
    else
    {
        /* No need to start timers for auxilary connections */
        if (pCntlrConnEntry->MibObject.i4FsofcControllerConnAuxId == OFC_ZERO)
        {
            /*
             * On connection failure start the Retry timer for this controller
             * and increment the Retry count
             */
            if (!(pCntlrConnEntry->ConnRetryTimer.tmrNode.u2Flags &
                  OFC_TMR_RUNNING))
            {
                OfcTmrStartTmr (&(pCntlrConnEntry->ConnRetryTimer),
                                OFC_CONTROLLER_TMR, OFC_CONTROLLER_INTERVAL);
            }
            pCntlrConnEntry->i4ConnRetryCount++;
        }
    }
    OFC_TRC_FUNC ((OFC_FN_EXIT, "OfcControllerSockConnect\n"));
}

/*****************************************************************************
 * Function    :  OfcConnectionRoleHandler 
 * Description :  This function handles role of the Controller 
 * Input       :  pRoleRequest - Pointer to the Role Request
 *                pConnPtr        - Connection Pointer
 * Output      :  pu4Role         - Controller Role 
 * Returns     :  OFC_SUCCESS/OFC_FAILURE 
 *****************************************************************************/
UINT4
OfcConnectionRoleHandler (tOfp131RoleReq * pRoleRequest, VOID *pConnPtr,
                          UINT4 *pu4Role)
{
    tOfpErrMsg          OfpErrorMsg;
    tOfcFsofcControllerConnEntry *pCntlrConnEntry = NULL;

    OFC_TRC_FUNC ((OFC_FN_ENTRY, "FUNC:OfcConnectionRoleHandler\n"));

    pCntlrConnEntry = (tOfcFsofcControllerConnEntry *) pConnPtr;
    if (pCntlrConnEntry != NULL)
    {
        pCntlrConnEntry =
            OfcGetControllerConnTableEntry (pCntlrConnEntry->i4SocketDesc);
    }

    MEMSET (&OfpErrorMsg, OFC_ZERO, sizeof (tOfpErrMsg));
    switch (pRoleRequest->u4Role)
    {
            /*
             * Role is MASTER,
             * If pass updates the Mibobject else return the current role
             */
        case OFPCR_ROLE_MASTER:
            if (pCntlrConnEntry != NULL)
            {
                if (OfcUpdateControllerConnRole (pRoleRequest,
                                                 pCntlrConnEntry->MibObject.
                                                 u4FsofcContextId) ==
                    OSIX_FAILURE)
                {
                    OfpErrorMsg.u2Type = OFPET_131_ROLE_REQUEST_FAILED;
                    OfpErrorMsg.u2Code = OFPRRFC_131_STALE;
                    OfcErrMsgPktSend (&OfpErrorMsg, pConnPtr);
                    return OFC_FAILURE;
                }

                pCntlrConnEntry->MibObject.i4FsofcControllerRole =
                    (INT4) pRoleRequest->u4Role;
            }
            break;

            /*
             * Role is EQUAL ,
             * updates the Mibobject
             */
        case OFPCR_ROLE_EQUAL:
            if (pCntlrConnEntry != NULL)
            {
                pCntlrConnEntry->MibObject.i4FsofcControllerRole =
                    (INT4) pRoleRequest->u4Role;
            }
            break;

            /*
             * Role is  SLAVE,
             * Check the Generation ID and update the Mibobject
             */
        case OFPCR_ROLE_SLAVE:
            if (pCntlrConnEntry != NULL)
            {
                if (FsofcCheckGenerationId (pRoleRequest,
                                            pCntlrConnEntry->MibObject.
                                            u4FsofcContextId) != OFC_FAILURE)
                {
                    pCntlrConnEntry->MibObject.i4FsofcControllerRole =
                        (INT4) pRoleRequest->u4Role;
                }
                else
                {
                    OfpErrorMsg.u2Type = OFPET_131_ROLE_REQUEST_FAILED;
                    OfpErrorMsg.u2Code = OFPRRFC_131_STALE;
                    OfcErrMsgPktSend (&OfpErrorMsg, pConnPtr);
                    return OFC_FAILURE;
                }
            }
            break;

            /*
             * Role is NO_CHANGE,
             * returns the current role
             */
        default:
            if (pCntlrConnEntry != NULL)
            {
                *pu4Role =
                    (UINT4) pCntlrConnEntry->MibObject.i4FsofcControllerRole;
            }
            return OFC_SUCCESS;
    }
    *pu4Role = pRoleRequest->u4Role;
    OFC_TRC_FUNC ((OFC_FN_EXIT, "EXIT:OfcConnectionRoleHandler\n"));
    return OFC_SUCCESS;
}

#ifdef SSL_WANTED
/***************************************************************************
 * Function    :  OfcControllerSockConnectSSL
 * Input       :  pOfcControllerConnEntry
 * Description :  This Routine creates a SSL socket over a tcp socket and
 *                connects to the Controller and waits for Packet-Receive event
 * Output      :  None
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE
 * *************************************************************************/
VOID
OfcControllerSockConnectSSL (tOfcFsofcControllerConnEntry *
                             pOfcControllerConnEntry)
{
    VOID               *pSSL = NULL;
    OFC_TRC_FUNC ((OFC_FN_ENTRY, "FUNC:OfcControllerSockConnectSSL\n"));
    OFC_TRC ((OFC_TASK_TRC, "!!Calling SSLConnect sock=%d!!\n",
              pOfcControllerConnEntry->i4SocketDesc));
    pSSL = SslArConnect (pOfcControllerConnEntry->i4SocketDesc);
    pOfcControllerConnEntry->pssl = pSSL;
    if (pSSL == NULL)
    {
        OFC_TRC ((OFC_TASK_TRC, "SSL Connect Failed \n"));
    }
    OFC_TRC_FUNC ((OFC_FN_EXIT, "EXIT:OfcControllerSockConnectSSL\n"));

}
#endif
