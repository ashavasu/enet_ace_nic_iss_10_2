/*****************************************************************************
 * Copyright (C) 2013 Aricent Inc . All Rights Reserved
 *
 * $Id: ofcnpwr.c,v 1.2 2014/08/14 11:13:34 siva Exp $
 *
 * Description: This file contains the wrapper for OFC Client NPAPIs
 *              that will be used for programming the hardware chipsets.
 *****************************************************************************/

#include "ofcinc.h"

/***************************************************************************
 *
 *    Function Name       : OfcNpWrHwProgram
 *
 *    Description         : This function takes care of calling appropriate
 *                          Np call using the tOfcNpModInfo
 *
 *    Input(s)            : FsHwNpParam of type tfsHwNp
 *
 *    Output(s)           : None
 *
 *    Global Var Referred : None
 *
 *    Global Var Modified : None.
 *
 *    Use of Recursion    : None.
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE
 *
 *****************************************************************************/

PUBLIC UINT1
OfcNpWrHwProgram (tFsHwNp * pFsHwNp)
{
    tOfcNpWrFsNpHwConfigOfcInfo *pEntry = NULL;
    tOfcNpModInfo      *pOfcNpModInfo = NULL;
    tOfcHwInfo         *pOfcHwInfo = NULL;
    tOfcHwFlowInfo     *pFlow = NULL;
    tOfcHwFlowInfo      OfcFlow;
    INT4                i4IfIndex = 0;
    UINT1               u1Cmd = 0;

    /*PRINTF("\r\nOfcNpWrHwProgram - Entry"); */
    pOfcNpModInfo = &(pFsHwNp->OfcNpModInfo);
    pEntry = &pOfcNpModInfo->OfcNpFsNpHwConfigOfcInfo;
    pOfcHwInfo = pEntry->pOfcHwOfcInfo;

    if (NULL == pOfcHwInfo)
    {
        return FNP_FAILURE;
    }

    i4IfIndex = (INT4) pOfcHwInfo->OfcHwEntry.OfcHwIfInfo.u4IfIndex;
    pFlow = &OfcFlow;
    u1Cmd = pOfcHwInfo->OfcCmd;

    switch (u1Cmd)
    {
        case OFC_NP_PORT_ADD:
            FsOfcHwInitFilter ((UINT4) i4IfIndex, pFlow);
            pFlow->u4FlowIndex = pOfcHwInfo->OfcHwEntry.OfcHwIfInfo.u4FlowIndex;
            if (FsOfcHwOpenflowPortUpdate ((UINT4) i4IfIndex, OFC_NP_PORT_ADD)
                == FNP_SUCCESS)
            {
                if (FsOfcHwUpdateExactFlowEntry (pFlow, OFC_NP_FLOW_ADD) !=
                    FNP_SUCCESS)
                {
                    PRINTF ("Flow Add Failed\r\n");
                    return FNP_FAILURE;
                }
            }
            break;
        case OFC_NP_PORT_DELETE:
            FsOfcHwInitFilter ((UINT4) i4IfIndex, pFlow);
            pFlow->u4FlowIndex = pOfcHwInfo->OfcHwEntry.OfcHwIfInfo.u4FlowIndex;
            if (FsOfcHwOpenflowPortUpdate
                ((UINT4) i4IfIndex, OFC_NP_PORT_DELETE) == FNP_SUCCESS)
            {
                if (FsOfcHwUpdateExactFlowEntry (pFlow, OFC_NP_FLOW_DELETE) !=
                    FNP_SUCCESS)
                {
                    PRINTF ("Flow Delete Failed\r\n");
                    return FNP_FAILURE;
                }
            }
            break;
        case OFC_NP_PORT_UP:
            FsOfcHwOpenflowPortUpdate ((UINT4) i4IfIndex, OFC_NP_PORT_UP);
            break;
        case OFC_NP_PORT_DOWN:
            FsOfcHwOpenflowPortUpdate ((UINT4) i4IfIndex, OFC_NP_PORT_DOWN);
            break;

        case OFC_NP_FLOW_ADD:
            if (pOfcHwInfo->OfcVer == OFC_VER_100)
            {
                FsOfcHwUpdateExactFlowEntry (&pOfcHwInfo->OfcHwEntry.
                                             OfcHwFlowInfo, pOfcHwInfo->OfcCmd);
            }
            else
            {
#ifdef OPENFLOW_TCAM            /* || DPA_WANTED */
                if (FsOfcHwUpdateWCFlowEntry
                    (&pOfcHwInfo->OfcHwEntry.OfcHwFlowInfo,
                     pOfcHwInfo->OfcCmd) == FNP_FAILURE)
                {
                    return FNP_FAILURE;
                }
#else
                FsOfcHwUpdateExactFlowEntry (&pOfcHwInfo->OfcHwEntry.
                                             OfcHwFlowInfo, pOfcHwInfo->OfcCmd);
#endif /* OPENFLOW_TCAM */
            }
            break;

        case OFC_NP_FLOW_DELETE:
            if (pOfcHwInfo->OfcVer == OFC_VER_100)
            {
                FsOfcHwUpdateExactFlowEntry (&pOfcHwInfo->OfcHwEntry.
                                             OfcHwFlowInfo, pOfcHwInfo->OfcCmd);
            }
            else
            {
#ifdef OPENFLOW_TCAM            /* || DPA_WANTED */
                if (FsOfcHwUpdateWCFlowEntry
                    (&pOfcHwInfo->OfcHwEntry.OfcHwFlowInfo,
                     pOfcHwInfo->OfcCmd) == FNP_FAILURE)
                {
                    return FNP_FAILURE;
                }
#else
                FsOfcHwUpdateExactFlowEntry (&pOfcHwInfo->OfcHwEntry.
                                             OfcHwFlowInfo, pOfcHwInfo->OfcCmd);
#endif /* OPENFLOW_TCAM */
            }
            break;

        case OFC_NP_FLOW_MODIFY:
            if (pOfcHwInfo->OfcVer == OFC_VER_100)
            {
                FsOfcHwUpdateExactFlowEntry (&pOfcHwInfo->OfcHwEntry.
                                             OfcHwFlowInfo, pOfcHwInfo->OfcCmd);
            }
            else
            {
#ifdef OPENFLOW_TCAM            /* || DPA_WANTED */
                if (FsOfcHwUpdateWCFlowEntry
                    (&pOfcHwInfo->OfcHwEntry.OfcHwFlowInfo,
                     pOfcHwInfo->OfcCmd) == FNP_FAILURE)
                {
                    return FNP_FAILURE;
                }
#else
                FsOfcHwUpdateExactFlowEntry (&pOfcHwInfo->OfcHwEntry.
                                             OfcHwFlowInfo, pOfcHwInfo->OfcCmd);
#endif /* OPENFLOW_TCAM */
            }
            break;

        case OFC_NP_GROUP_ADD:
        case OFC_NP_GROUP_DELETE:
        case OFC_NP_GROUP_MODIFY:
            if (pOfcHwInfo->OfcVer == OFC_VER_131)
            {
                FsOfcHwUpdateGroupEntry (&pOfcHwInfo->OfcHwEntry.OfcHwGroupInfo,
                                         pOfcHwInfo->OfcCmd);
            }
            else
            {
                PRINTF ("Group Provisioning not supported in V1.0.0\r\n");
            }
            break;
        case OFC_NP_METER_ADD:
        case OFC_NP_METER_DELETE:
        case OFC_NP_METER_MODIFY:
            if (pOfcHwInfo->OfcVer == OFC_VER_131)
            {
                FsOfcHwUpdateMeterEntry (&pOfcHwInfo->OfcHwEntry.OfcHwMeterInfo,
                                         pOfcHwInfo->OfcCmd);
            }
            else
            {
                PRINTF ("Meter Provisioning not supported in V1.0.0\r\n");
            }
            break;
        case OFC_NP_CONFIG_PARAM:
            FsOfcHwOpenflowClientCfgParamsUpdate (pOfcHwInfo->OfcHwEntry.
                                                  OfcHwCfgInfo.OfcCfgParam,
                                                  pOfcHwInfo->OfcHwEntry.
                                                  OfcHwCfgInfo.u4ParamValue);
            break;

        case OFC_NP_GET_FLOW_STATS:
            if (pOfcHwInfo->OfcVer == OFC_VER_131)
            {
#ifdef OPENFLOW_TCAM
                FsOfcHwGetWCFlowStats (&pOfcHwInfo->OfcHwEntry.OfcHwFlowInfo,
                                       &pOfcHwInfo->OfcHwEntry.OfcFlowStats);
#endif
            }
            else
            {
                FsOfcHwGetExactFlowStats (NULL,
                                          &pOfcHwInfo->OfcHwEntry.OfcFlowStats);
            }
            break;

        case OFC_NP_GET_GROUP_STATS:
            if (pOfcHwInfo->OfcVer == OFC_VER_131)
            {
                FsOfcHwGetGroupStats (&pOfcHwInfo->OfcHwEntry.OfcHwGroupInfo,
                                      &pOfcHwInfo->OfcHwEntry.OfcGroupStats);
            }
            else
            {
                PRINTF ("Groups are not supported in V1.0.0\r\n");
            }
            break;
        case OFC_NP_GET_METER_STATS:
            if (pOfcHwInfo->OfcVer == OFC_VER_131)
            {
                FsOfcHwGetMeterStats (&pOfcHwInfo->OfcHwEntry.OfcHwMeterInfo,
                                      &pOfcHwInfo->OfcHwEntry.OfcMeterStats);
            }
            else
            {
                PRINTF ("Meters are not supported in V1.0.0\r\n");
            }
            break;
        case OFC_NP_VLAN_ADD:
            if (pOfcHwInfo->OfcVer == OFC_VER_131)
            {
                FsOfcHwAddVlanEntry (pOfcHwInfo->OfcHwEntry.OfcHwVlanInfo.
                                     u4ContextId,
                                     pOfcHwInfo->OfcHwEntry.OfcHwVlanInfo.
                                     u4VlanId,
                                     pOfcHwInfo->OfcHwEntry.OfcHwVlanInfo.
                                     pu1EgressPorts,
                                     pOfcHwInfo->OfcHwEntry.OfcHwVlanInfo.
                                     pu1UntagPorts);
            }
            else
            {
                PRINTF ("Vlan Provisioning not supported in V1.0.0\r\n");
            }
            break;
        case OFC_NP_VLAN_DELETE:
            if (pOfcHwInfo->OfcVer == OFC_VER_131)
            {
                FsOfcHwDelVlanEntry (pOfcHwInfo->OfcHwEntry.OfcHwVlanInfo.
                                     u4ContextId,
                                     pOfcHwInfo->OfcHwEntry.OfcHwVlanInfo.
                                     u4VlanId);
            }
            else
            {
                PRINTF ("Vlan Provisioning not supported in V1.0.0\r\n");
            }
            break;
        case OFC_NP_VLAN_SET:
            if (pOfcHwInfo->OfcVer == OFC_VER_131)
            {
                FsOfcHwSetVlanMemberPorts (pOfcHwInfo->OfcHwEntry.OfcHwVlanInfo.
                                           u4ContextId,
                                           pOfcHwInfo->OfcHwEntry.OfcHwVlanInfo.
                                           u4VlanId,
                                           pOfcHwInfo->OfcHwEntry.OfcHwVlanInfo.
                                           pu1EgressPorts,
                                           pOfcHwInfo->OfcHwEntry.OfcHwVlanInfo.
                                           pu1UntagPorts);
            }
            else
            {
                PRINTF ("Vlan Provisioning not supported in V1.0.0\r\n");
            }
            break;
        case OFC_NP_VLAN_RESET:
            if (pOfcHwInfo->OfcVer == OFC_VER_131)
            {
                FsOfcHwResetVlanMemberPorts (pOfcHwInfo->OfcHwEntry.
                                             OfcHwVlanInfo.u4ContextId,
                                             pOfcHwInfo->OfcHwEntry.
                                             OfcHwVlanInfo.u4VlanId,
                                             pOfcHwInfo->OfcHwEntry.
                                             OfcHwVlanInfo.pu1EgressPorts,
                                             pOfcHwInfo->OfcHwEntry.
                                             OfcHwVlanInfo.pu1UntagPorts);
            }
            else
            {
                PRINTF ("Vlan Provisioning not supported in V1.0.0\r\n");
            }
            break;
        case OFC_NP_INIT:
            /*
             * Init/Reserve the number of HW ACL-Filters/Meters required for Openflow
             * Version 1.0.0/1.3.1 Client
             */
            if (pOfcHwInfo->OfcHwEntry.OfcHwRsrvInfo.OfcRsrvParam ==
                OFC_HW_ACL_FILTERS)
            {
                FsOfcHwOpenflowInitAclFilters (pOfcHwInfo->OfcHwEntry.
                                               OfcHwRsrvInfo.u4Entries);
            }
            else if
                (pOfcHwInfo->OfcHwEntry.OfcHwRsrvInfo.OfcRsrvParam ==
                 OFC_HW_METER_ENTRIES)
            {
                FsOfcHwOpenflowInitMeterEntries (pOfcHwInfo->OfcHwEntry.
                                                 OfcHwRsrvInfo.u4Entries);
            }
            break;
        default:
            PRINTF ("\r\nOfcNpWrHwProgram - Exiting with Return Value Failure, "
                    " Due to DEFUALT Case");
            return FNP_FAILURE;
    }

    /*       
     * PRINTF("\r\nOfcNpWrHwProgram - Exit with Return Value SUCCESS");
     */
    return FNP_SUCCESS;
}
