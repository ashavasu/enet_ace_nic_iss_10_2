/********************************************************************
 * Copyright (C) 2013 Aricent Inc . All Rights Reserved
 *
 * $Id: ofcvlan.c,v 1.7 2014/10/13 12:05:37 siva Exp $
 *
 * Description: This file contains procedures for openflow vlan configuration
 *
 *******************************************************************/
#include "ofcinc.h"
#include "ofcsz.h"
#include "ofcvlan.h"
#include "ofcl.h"
#include "ofcapi.h"
#ifdef NPAPI_WANTED
#include"ofcnp.h"
#endif

PRIVATE INT4        OfcVlanUpdatePorts (UINT4 u4ContextId, UINT4 u4VlanId,
                                        UINT1 *pu1Ports, INT4 i4Len,
                                        INT4 i4Flag);
PRIVATE INT4        OfcVlanInterfaceEventSend (UINT4 u4VlanId, INT4 i4Evt,
                                               UINT1 *pu1MemberPorts,
                                               UINT1 *pu1UntaggedPorts,
                                               UINT4 u4Flag);
/*****************************************************************************
 *
 *    Function Name       : OfcVlanCreate
 *
 *    Description         : This function creates vlan interface 
 *                          
 *    Input(s)            : VlanId
 *
 *    Output(s)           : None.
 *
 *    Global Variables Referred : None.
 *               
 *    Global Variables Modified : None. 
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            : OSIX_SUCCESS/OSIX_FAILURE
 *                         
 *
 *****************************************************************************/
INT4
OfcVlanCreate (UINT4 u4VlanId)
{
    UINT4               u4IfIndex = OFC_ZERO;
    UINT1               au1IfName[CFA_MAX_IFALIAS_LENGTH];
    UINT1              *pu1Alias = NULL;
#ifdef NPAPI_WANTED
    tOfcHwInfo          OfcHwInfo;
    INT4                i4RetValue = OFC_ZERO;
#endif

    OFC_TRC_FUNC ((OFC_FN_ENTRY, "Entry :: OpenflowNotifyPortCreateDelete \n"));

#ifdef NPAPI_WANTED
    MEMSET (&OfcHwInfo, OFC_ZERO, sizeof (tOfcHwInfo));
#endif

    pu1Alias = au1IfName;
    STRCPY (au1IfName, OPENFLOW_VLAN_ALIAS_PREFIX);
    SPRINTF ((CHR1 *) pu1Alias, "%s%u", au1IfName, u4VlanId);

    if (OfcGetInterfaceIndexFromName (pu1Alias, &u4IfIndex) == OSIX_SUCCESS)
    {
        OFC_TRC_FUNC ((OFC_FN_EXIT, "Openflow Vlan already exists \n"));

        return OSIX_SUCCESS;

    }
    if (CfaCreateOpenflowVlan (u4VlanId, &u4IfIndex) == OSIX_FAILURE)
    {
        OFC_TRC_FUNC ((OFC_FN_EXIT, "CfaCreateOpenflowVlan Failed \n"));

        return OSIX_FAILURE;

    }
#ifdef NPAPI_WANTED
    OfcHwInfo.OfcCmd = OFC_NP_VLAN_ADD;
    i4RetValue = OfcFsNpHwConfigOfcInfo (&OfcHwInfo);
    if (i4RetValue != FNP_SUCCESS)
    {
        OFC_TRC ((OFC_TASK_TRC, "OfcFsNpHwConfigOfcInfo:  "
                  "NPAPI Failure in Creating VLAN\r\n"));
    }
#endif
    return OSIX_SUCCESS;
}

/*****************************************************************************
 *
 *    Function Name       : OfcVlanDelete
 *
 *    Description         : This function deletes vlan interface 
 *                          
 *    Input(s)            : VlanId
 *
 *    Output(s)           : None.
 *
 *    Global Variables Referred : None.
 *               
 *    Global Variables Modified : None. 
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            : OSIX_SUCCESS/OSIX_FAILURE
 *                         
 *
 *****************************************************************************/

INT4
OfcVlanDelete (UINT4 u4VlanId)
{
    UINT4               u4IfIndex = OFC_ZERO;
    UINT1              *pau1OldEgressPortList = NULL;
    UINT1              *pau1OldUntaggedPortList = NULL;
    tSNMP_OCTET_STRING_TYPE OldEgressPorts;
    tSNMP_OCTET_STRING_TYPE OldUntaggedPorts;
    tOfcFsofcIfEntry    lOfcFsofcIfEntry;
    tOfcFsofcIfEntry   *pOfcFsofcIfEntry = NULL;

#ifdef NPAPI_WANTED
    tOfcHwInfo          OfcHwInfo;
    INT4                i4RetValue = OFC_ZERO;
#endif
    MEMSET (&lOfcFsofcIfEntry.MibObject, OFC_ZERO,
            sizeof (lOfcFsofcIfEntry.MibObject));
#ifdef NPAPI_WANTED
    MEMSET (&OfcHwInfo, OFC_ZERO, sizeof (tOfcHwInfo));
#endif

    OFC_TRC_FUNC ((OFC_FN_ENTRY, "Entry :: OpenflowNotifyPortCreateDelete \n"));

    if (CfaDeleteOpenflowVlan (u4VlanId, &u4IfIndex) == OSIX_FAILURE)
    {
        OFC_TRC_FUNC ((OFC_FN_EXIT, "CfaCreateOpenflowVlan Failed \n"));

        return OSIX_FAILURE;

    }

    /*Reset Membership of openflow ports */
    pau1OldEgressPortList = UtilPlstAllocLocalPortList (OFC_VLAN_PORTS_LEN);
    if (pau1OldEgressPortList == NULL)
    {
        OFC_TRC ((OFC_TASK_TRC,
                  "OfcVlanConfigurePorts: Error in allocating memory for pau1OldEgressPortList\r\n"));
        return OSIX_FAILURE;
    }
    MEMSET (pau1OldEgressPortList, OFC_ZERO, OFC_VLAN_PORTS_LEN);

    pau1OldUntaggedPortList = UtilPlstAllocLocalPortList (OFC_VLAN_PORTS_LEN);
    if (pau1OldUntaggedPortList == NULL)
    {
        OFC_TRC ((OFC_TASK_TRC,
                  "OfcVlanConfigurePorts: Error in allocating memory "
                  "for pau1OldUntaggedPortList\r\n"));
        UtilPlstReleaseLocalPortList (pau1OldEgressPortList);
        return OSIX_FAILURE;
    }
    MEMSET (pau1OldUntaggedPortList, OFC_ZERO, OFC_VLAN_PORTS_LEN);

    /* OldxxxPorts are used to restore the objects in case of any failure. */
    OldEgressPorts.i4_Length = OFC_VLAN_PORTS_LEN;
    OldEgressPorts.pu1_OctetList = pau1OldEgressPortList;

    OldUntaggedPorts.i4_Length = OFC_VLAN_PORTS_LEN;
    OldUntaggedPorts.pu1_OctetList = pau1OldUntaggedPortList;

    lOfcFsofcIfEntry.MibObject.u4FsofcContextId = (INT4) OFC_DEFAULT_CONTEXT;
    lOfcFsofcIfEntry.MibObject.i4FsofcIfIndex = (INT4) (u4IfIndex);
    pOfcFsofcIfEntry = RBTreeGet (gOfcGlobals.OfcGlbMib.FsofcIfTable,
                                  (tRBElem *) & (lOfcFsofcIfEntry));
    if (pOfcFsofcIfEntry != NULL)
    {
        /* Reset Openflow Membership for previously configured member ports */
        /* Test for member ports to be openflow ports */
        nmhGetFsofcVlanEgressPorts (OFC_DEFAULT_CONTEXT, (INT4) u4IfIndex,
                                    &OldEgressPorts);
        nmhGetFsofcVlanUntaggedPorts (OFC_DEFAULT_CONTEXT, (INT4) u4IfIndex,
                                      &OldUntaggedPorts);
        if (OfcVlanUpdatePorts
            (OFC_DEFAULT_CONTEXT, u4VlanId, OldEgressPorts.pu1_OctetList,
             OldEgressPorts.i4_Length, VLAN_DELETE) == OFC_FALSE)
        {
            OFC_TRC ((OFC_TASK_TRC, "OfcVlanConfigurePorts:  "
                      "Invalid EgressPortList\r\n"));
            UtilPlstReleaseLocalPortList (pau1OldEgressPortList);
            UtilPlstReleaseLocalPortList (pau1OldUntaggedPortList);
            return OSIX_FAILURE;
        }

        if (OfcVlanUpdatePorts
            (OFC_DEFAULT_CONTEXT, u4VlanId, OldUntaggedPorts.pu1_OctetList,
             OldUntaggedPorts.i4_Length, VLAN_DELETE) == OFC_FALSE)
        {
            OFC_TRC ((OFC_TASK_TRC, "OfcVlanConfigurePorts:  "
                      "Invalid UntaggedPortList\r\n"));
            UtilPlstReleaseLocalPortList (pau1OldEgressPortList);
            UtilPlstReleaseLocalPortList (pau1OldUntaggedPortList);
            return OSIX_FAILURE;
        }

        /*
         * Remove the Entry from Ofcl IfTable
         */
        RBTreeRem (gOfcGlobals.OfcGlbMib.FsofcIfTable, pOfcFsofcIfEntry);
        MemReleaseMemBlock (OFC_FSOFCIFTABLE_POOLID,
                            (UINT1 *) pOfcFsofcIfEntry);
    }

#ifdef NPAPI_WANTED
    OfcHwInfo.OfcCmd = OFC_NP_VLAN_RESET;
    i4RetValue = OfcFsNpHwConfigOfcInfo (&OfcHwInfo);
    if (i4RetValue != FNP_SUCCESS)
    {
        OFC_TRC ((OFC_TASK_TRC, "OfcFsNpHwConfigOfcInfo:  "
                  "Reset Member Ports failed in NPAPI\r\n"));
    }
    OfcHwInfo.OfcCmd = OFC_NP_VLAN_DELETE;
    i4RetValue = OfcFsNpHwConfigOfcInfo (&OfcHwInfo);
    if (i4RetValue != FNP_SUCCESS)
    {
        OFC_TRC ((OFC_TASK_TRC, "OfcFsNpHwConfigOfcInfo:  "
                  "NPAPI Failure in Deleting VLAN\r\n"));
    }
#endif

    UtilPlstReleaseLocalPortList (pau1OldEgressPortList);
    UtilPlstReleaseLocalPortList (pau1OldUntaggedPortList);
    return (OSIX_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/* Function     : OfcVlanConfigurePorts                                      */
/*                                                                           */
/* Description  : Populates the portlist for the openflow vlan               */
/*                                                                           */
/* INPUT        : pu1MemberPorts    - Contains member ports                  */
/*                pu1UntaggedPorts  - Contains untagged ports                */
/*                pu1ForbiddenPorts - Contains forbidden ports               */
/*                u4VlanId          - Vlan ID                                */
/*                u4Flag            - Flag to indicate                       */
/*                                    addition/removal of ports              */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSIX_SUCCESS or OSIX_FAILURE                               */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/
INT4
OfcVlanConfigurePorts (UINT4 u4VlanId, UINT1 *pu1MemberPorts,
                       UINT1 *pu1UntaggedPorts, UINT4 u4Flag)
{
    UINT1              *pau1OldEgressPortList = NULL;
    UINT1              *pau1OldUntaggedPortList = NULL;
    UINT1              *pu1Alias = NULL;
    UINT1               au1IfName[CFA_MAX_IFALIAS_LENGTH];

    tSNMP_OCTET_STRING_TYPE OldEgressPorts;
    tSNMP_OCTET_STRING_TYPE OldUntaggedPorts;
    tSNMP_OCTET_STRING_TYPE EgressPorts;
    tSNMP_OCTET_STRING_TYPE UntaggedPorts;

    UINT4               u4IfIndex = OFC_ZERO;
    UINT4               u4OperStatus = OFC_VLAN_IF_UP;
    UINT4               u4ContextId = OFC_ZERO;
    tOfcFsofcIfEntry    lOfcFsofcIfEntry;
    tOfcFsofcIfEntry   *pOfcFsofcIfEntry = NULL;
    tCfaIfInfo          CfaIfInfo;
#ifdef NPAPI_WANTED
    tOfcHwInfo          OfcHwInfo;
    INT4                i4RetValue = OFC_ZERO;
#endif

    pu1Alias = au1IfName;
    pau1OldEgressPortList = UtilPlstAllocLocalPortList (OFC_VLAN_PORTS_LEN);
    if (pau1OldEgressPortList == NULL)
    {
        OFC_TRC_FUNC ((OFC_FN_EXIT,
                       "OfcVlanConfigurePorts: Error in allocating memory for pau1OldEgressPortList\r\n"));
        return OSIX_FAILURE;
    }
    MEMSET (pau1OldEgressPortList, OFC_ZERO, OFC_VLAN_PORTS_LEN);
#ifdef NPAPI_WANTED
    MEMSET (&OfcHwInfo, OFC_ZERO, sizeof (tOfcHwInfo));
#endif

    pau1OldUntaggedPortList = UtilPlstAllocLocalPortList (OFC_VLAN_PORTS_LEN);
    if (pau1OldUntaggedPortList == NULL)
    {
        OFC_TRC_FUNC ((OFC_FN_EXIT,
                       "OfcVlanConfigurePorts: Error in allocating memory "
                       "for pau1OldUntaggedPortList\r\n"));
        UtilPlstReleaseLocalPortList (pau1OldEgressPortList);
        return OSIX_FAILURE;
    }
    MEMSET (pau1OldUntaggedPortList, OFC_ZERO, OFC_VLAN_PORTS_LEN);

    EgressPorts.i4_Length = OFC_VLAN_PORTS_LEN;
    EgressPorts.pu1_OctetList = pu1MemberPorts;

    UntaggedPorts.i4_Length = OFC_VLAN_PORTS_LEN;
    UntaggedPorts.pu1_OctetList = pu1UntaggedPorts;

    /* OldxxxPorts are used to restore the objects in case of any failure. */
    OldEgressPorts.i4_Length = OFC_VLAN_PORTS_LEN;
    OldEgressPorts.pu1_OctetList = pau1OldEgressPortList;

    OldUntaggedPorts.i4_Length = OFC_VLAN_PORTS_LEN;
    OldUntaggedPorts.pu1_OctetList = pau1OldUntaggedPortList;

    STRCPY (au1IfName, OPENFLOW_VLAN_ALIAS_PREFIX);
    SPRINTF ((CHR1 *) pu1Alias, "%s%u", au1IfName, u4VlanId);

    OfcGetInterfaceIndexFromName (pu1Alias, &u4IfIndex);

    /* Get the Context ID for the InPut IfIndex */
    if (OfcGetContextIdFromIfIndex (u4IfIndex, &u4ContextId) != OFC_SUCCESS)
    {
        OFC_TRC_FUNC ((OFC_FN_EXIT, "\r\nGet Context ID failed\n"));
        UtilPlstReleaseLocalPortList (pau1OldEgressPortList);
        UtilPlstReleaseLocalPortList (pau1OldUntaggedPortList);
        return OSIX_FAILURE;
    }

    /*
     * Return Failure if the Openflow Vlan is not mapped to any context before
     * updating the ports to it.
     */
    if (u4ContextId == OFC_INVALID_CONTEXT)
    {
        OFC_TRC_FUNC ((OFC_FN_EXIT, "\r\nPort Maaping/Unmapping Failed, "
                       "Update Openflow vlan Context Fisrt\n"));
        UtilPlstReleaseLocalPortList (pau1OldEgressPortList);
        UtilPlstReleaseLocalPortList (pau1OldUntaggedPortList);
        return OSIX_FAILURE;
    }

    MEMSET (&lOfcFsofcIfEntry.MibObject, OFC_ZERO,
            sizeof (lOfcFsofcIfEntry.MibObject));
    lOfcFsofcIfEntry.MibObject.u4FsofcContextId = u4ContextId;
    lOfcFsofcIfEntry.MibObject.i4FsofcIfIndex = (INT4) (u4IfIndex);

    pOfcFsofcIfEntry = RBTreeGet (gOfcGlobals.OfcGlbMib.FsofcIfTable,
                                  (tRBElem *) & (lOfcFsofcIfEntry));

    if (pOfcFsofcIfEntry == NULL)
    {
        OFC_TRC_FUNC ((OFC_FN_EXIT, "\r\nModify Failed, "
                       "due to non-existing If MIB Table Entry \n"));
        UtilPlstReleaseLocalPortList (pau1OldEgressPortList);
        UtilPlstReleaseLocalPortList (pau1OldUntaggedPortList);
        return OSIX_FAILURE;
    }

    nmhGetFsofcVlanEgressPorts (pOfcFsofcIfEntry->MibObject.u4FsofcIfContextId,
                                (INT4) u4IfIndex, &OldEgressPorts);
    nmhGetFsofcVlanUntaggedPorts (pOfcFsofcIfEntry->MibObject.
                                  u4FsofcIfContextId, (INT4) u4IfIndex,
                                  &OldUntaggedPorts);

    if (u4Flag == VLAN_UPDATE)
    {
        /* Add new member ports to existing member portlist
         * EgressPorts = OldEgressPorts + EgressPorts
         * UntaggedPorts = OldUntaggedports + UntaggedPorts
         * ForbiddenPorts = OldForbiddenPorts + ForbiddenPorts
         * */

        OFC_VLAN_ADD_PORT_LIST (EgressPorts.pu1_OctetList,
                                OldEgressPorts.pu1_OctetList);
        OFC_VLAN_ADD_PORT_LIST (UntaggedPorts.pu1_OctetList,
                                OldUntaggedPorts.pu1_OctetList);
    }

    if (u4Flag == VLAN_DELETE)
    {
        /* Reset Openflow Membership for previously configured member ports */
        /* Test for member ports to be openflow ports */

        if (FsUtilBitListIsAllZeros (EgressPorts.pu1_OctetList,
                                     (UINT4) EgressPorts.i4_Length) ==
            OSIX_TRUE)
        {
            /* Delete all Egress Ports */
            MEMCPY (EgressPorts.pu1_OctetList, OldEgressPorts.pu1_OctetList,
                    OldEgressPorts.i4_Length);
            EgressPorts.i4_Length = OldEgressPorts.i4_Length;
        }
        if (FsUtilBitListIsAllZeros (UntaggedPorts.pu1_OctetList,
                                     (UINT4) UntaggedPorts.i4_Length) ==
            OSIX_TRUE)
        {
            /* Delete all Untagged Ports */
            MEMCPY (UntaggedPorts.pu1_OctetList, OldUntaggedPorts.pu1_OctetList,
                    OldUntaggedPorts.i4_Length);
            UntaggedPorts.i4_Length = OldUntaggedPorts.i4_Length;
        }

        if (OfcVlanUpdatePorts (pOfcFsofcIfEntry->MibObject.u4FsofcIfContextId,
                                u4VlanId,
                                EgressPorts.pu1_OctetList,
                                EgressPorts.i4_Length,
                                VLAN_DELETE) == OFC_FALSE)
        {
            OFC_TRC_FUNC ((OFC_FN_EXIT, "\r\nOfcVlanConfigurePorts: "
                           "Invalid EgressPortList\n"));
            UtilPlstReleaseLocalPortList (pau1OldEgressPortList);
            UtilPlstReleaseLocalPortList (pau1OldUntaggedPortList);
            return OSIX_FAILURE;
        }

        if (OfcVlanUpdatePorts (pOfcFsofcIfEntry->MibObject.u4FsofcIfContextId,
                                u4VlanId,
                                UntaggedPorts.pu1_OctetList,
                                UntaggedPorts.i4_Length,
                                VLAN_DELETE) == OFC_FALSE)
        {
            OFC_TRC_FUNC ((OFC_FN_EXIT, "\r\nOfcVlanConfigurePorts: "
                           "Invalid UntaggedPortList\n"));
            UtilPlstReleaseLocalPortList (pau1OldEgressPortList);
            UtilPlstReleaseLocalPortList (pau1OldUntaggedPortList);
            return OSIX_FAILURE;
        }

        OFC_VLAN_RESET_PORT_LIST (OldEgressPorts.pu1_OctetList,
                                  EgressPorts.pu1_OctetList);
        OFC_VLAN_RESET_PORT_LIST (OldUntaggedPorts.pu1_OctetList,
                                  UntaggedPorts.pu1_OctetList);
        MEMCPY (EgressPorts.pu1_OctetList, OldEgressPorts.pu1_OctetList,
                OFC_VLAN_PORTS_LEN);
        MEMCPY (UntaggedPorts.pu1_OctetList, OldUntaggedPorts.pu1_OctetList,
                OFC_VLAN_PORTS_LEN);

    }
    else
    {
        /* Test for member ports to be openflow ports */
        if (OfcVlanUpdatePorts (pOfcFsofcIfEntry->MibObject.u4FsofcIfContextId,
                                u4VlanId,
                                EgressPorts.pu1_OctetList,
                                EgressPorts.i4_Length,
                                VLAN_CREATE) == OFC_FALSE)
        {
            OFC_TRC ((OFC_TASK_TRC, "\r\nOfcVlanConfigurePorts: "
                      "Invalid EgressPortList\n"));
            if (OfcVlanUpdatePorts
                (pOfcFsofcIfEntry->MibObject.u4FsofcIfContextId, u4VlanId,
                 EgressPorts.pu1_OctetList, EgressPorts.i4_Length,
                 VLAN_DELETE) == OFC_FALSE)
            {
                OFC_TRC ((OFC_TASK_TRC, "OfcVlanConfigurePorts:  "
                          "Invalid EgressPortList\r\n"));
                UtilPlstReleaseLocalPortList (pau1OldEgressPortList);
                UtilPlstReleaseLocalPortList (pau1OldUntaggedPortList);
                return OSIX_FAILURE;
            }
            UtilPlstReleaseLocalPortList (pau1OldEgressPortList);
            UtilPlstReleaseLocalPortList (pau1OldUntaggedPortList);
            return OSIX_FAILURE;
        }

        if (OfcVlanUpdatePorts (pOfcFsofcIfEntry->MibObject.u4FsofcIfContextId,
                                u4VlanId,
                                UntaggedPorts.pu1_OctetList,
                                UntaggedPorts.i4_Length,
                                VLAN_CREATE) == OFC_FALSE)
        {
            OFC_TRC ((OFC_TASK_TRC, "\r\nOfcVlanConfigurePorts: "
                      "Invalid UntaggedPortList\n"));
            if (OfcVlanUpdatePorts
                (pOfcFsofcIfEntry->MibObject.u4FsofcIfContextId, u4VlanId,
                 EgressPorts.pu1_OctetList, EgressPorts.i4_Length,
                 VLAN_DELETE) == OFC_FALSE)
            {
                OFC_TRC ((OFC_TASK_TRC, "OfcVlanConfigurePorts:  "
                          "Invalid EgressPortList\r\n"));
                UtilPlstReleaseLocalPortList (pau1OldEgressPortList);
                UtilPlstReleaseLocalPortList (pau1OldUntaggedPortList);
                return OSIX_FAILURE;
            }
            if (OfcVlanUpdatePorts
                (pOfcFsofcIfEntry->MibObject.u4FsofcIfContextId, u4VlanId,
                 UntaggedPorts.pu1_OctetList, UntaggedPorts.i4_Length,
                 VLAN_DELETE) == OFC_FALSE)
            {
                OFC_TRC ((OFC_TASK_TRC, "OfcVlanConfigurePorts:  "
                          "Invalid UntaggedPortList\r\n"));
                UtilPlstReleaseLocalPortList (pau1OldEgressPortList);
                UtilPlstReleaseLocalPortList (pau1OldUntaggedPortList);
                return OSIX_FAILURE;
            }
            UtilPlstReleaseLocalPortList (pau1OldEgressPortList);
            UtilPlstReleaseLocalPortList (pau1OldUntaggedPortList);
            return OSIX_FAILURE;
        }
    }

#ifdef NPAPI_WANTED
    OfcHwInfo.OfcCmd = OFC_NP_VLAN_SET;
    i4RetValue = OfcFsNpHwConfigOfcInfo (&OfcHwInfo);
    if (i4RetValue != FNP_SUCCESS)
    {
        OFC_TRC ((OFC_TASK_TRC, "\r\nOfcFsNpHwConfigOfcInfo: "
                  "Set Member Ports failed in NPAPI\n"));
    }
#endif

    /* Updating Openflow data structure */
    MEMCPY (pOfcFsofcIfEntry->MibObject.au1FsofcVlanEgressPorts,
            EgressPorts.pu1_OctetList, OFC_SIXTY_FOUR);
    pOfcFsofcIfEntry->MibObject.i4FsofcVlanEgressPortsLen =
        EgressPorts.i4_Length;

    MEMCPY (pOfcFsofcIfEntry->MibObject.au1FsofcVlanUntaggedPorts,
            UntaggedPorts.pu1_OctetList, OFC_SIXTY_FOUR);
    pOfcFsofcIfEntry->MibObject.i4FsofcVlanUntaggedPortsLen =
        UntaggedPorts.i4_Length;

    /* Check if the member port list is empty */
    if (FsUtilBitListIsAllZeros (EgressPorts.pu1_OctetList,
                                 (UINT4) EgressPorts.i4_Length) == OSIX_TRUE)
    {
        u4OperStatus = OFC_VLAN_IF_DOWN;

    }

    /* Check the port list to set the oper status */
    if (pOfcFsofcIfEntry->MibObject.i4FsofcIfOperStatus != (INT4) u4OperStatus)
    {
        MEMSET (&CfaIfInfo, OFC_ZERO, sizeof (CfaIfInfo));

        CfaIfInfo.u1IfOperStatus = (UINT1) u4OperStatus;

        if (CfaSetIfInfo (IF_OPER_STATUS, u4IfIndex, &CfaIfInfo) !=
            OSIX_SUCCESS)
        {
            OFC_TRC ((OFC_TASK_TRC,
                      "OfcVlanConfigurePorts:  " "CfaSetIfInfo Failed\r\n"));
            UtilPlstReleaseLocalPortList (pau1OldEgressPortList);
            UtilPlstReleaseLocalPortList (pau1OldUntaggedPortList);
            return OSIX_FAILURE;
        }
        pOfcFsofcIfEntry->MibObject.i4FsofcIfOperStatus = (INT4) u4OperStatus;
        OpenflowNotifyPortStatus (u4IfIndex, (INT1) u4OperStatus);
    }
    UtilPlstReleaseLocalPortList (pau1OldEgressPortList);
    UtilPlstReleaseLocalPortList (pau1OldUntaggedPortList);
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function Name       : OfcVlanUpdatePorts ()                               */
/*                                                                           */
/* Description         : This function checks if the ports in the portlist   */
/*                       is valid.                                           */
/*                                                                           */
/* Input(s)            : pu1Ports - Array of bytes used as Portlist bitmap.  */
/*                       i4Len    - Size of 'pu1Ports' in bytes              */
/*                                                                           */
/* Output(s)           : None                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred            : Openflow Port Table                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified            : None.                                               */
/*                                                                           */
/* Exceptions or OS                                                          */
/* Error Handling      : None.                                               */
/*                                                                           */
/* Use of Recursion    : None.                                               */
/*                                                                           */
/* Returns             : OFC_TRUE if all the ports in the 'pu1Ports' are     */
/*                                 valid                                     */
/*                       OFC_FALSE otherwise                                 */
/*****************************************************************************/
INT4
OfcVlanUpdatePorts (UINT4 u4ContextId, UINT4 u4VlanId, UINT1 *pu1Ports,
                    INT4 i4Len, INT4 i4Flag)
{
    tCfaIfInfo          cfaifInfo;
    tOfcFsofcIfEntry    lOfcFsofcIfEntry;
    tOfcFsofcIfEntry   *pOfcFsofcIfEntry = NULL;
    UINT2               u2Port = OFC_ZERO;
    UINT2               u2ByteInd = OFC_ZERO;
    UINT2               u2BitIndex = OFC_ZERO;
    UINT1               u1PortFlag = OFC_ZERO;

    for (u2ByteInd = OFC_ZERO; u2ByteInd < i4Len; u2ByteInd++)
    {
        if (pu1Ports[u2ByteInd] == OFC_ZERO)
        {
            continue;
        }
        u1PortFlag = pu1Ports[u2ByteInd];

        for (u2BitIndex = OFC_ZERO;
             ((u2BitIndex < OFC_VLAN_PORTS_PER_BYTE)
              && (u1PortFlag != OFC_ZERO)); u2BitIndex++)
        {
            if ((u1PortFlag & OFC_VLAN_BIT8) == OFC_ZERO)
            {
                u1PortFlag = (UINT1) (u1PortFlag << OFC_ONE);
                continue;
            }
            u2Port =
                (UINT2) ((u2ByteInd * OFC_VLAN_PORTS_PER_BYTE) + u2BitIndex +
                         OFC_ONE);

            if (u2Port >= OFC_VLAN_MAX_PORTS + OFC_ONE)
            {
                break;
            }
            MEMSET (&cfaifInfo, OFC_ZERO, sizeof (tCfaIfInfo));

            if (CfaGetIfInfo ((UINT4) u2Port, &cfaifInfo) == CFA_FAILURE)
            {
                return OFC_FALSE;
            }
            if (cfaifInfo.u1IfSubType != CFA_SUBTYPE_OPENFLOW_INTERFACE)
            {
                OFC_TRC ((OFC_TASK_TRC,
                          "Port %d is not a Openflow port.So it is not configured as Member of Openflow vlan\r\n",
                          u2Port));
                return OFC_FALSE;    /* Continue to configure all member ports */
            }

            MEMSET (&lOfcFsofcIfEntry.MibObject, OFC_ZERO,
                    sizeof (lOfcFsofcIfEntry.MibObject));
            lOfcFsofcIfEntry.MibObject.u4FsofcContextId = u4ContextId;

            lOfcFsofcIfEntry.MibObject.i4FsofcIfIndex = (INT4) (u2Port);
            pOfcFsofcIfEntry = RBTreeGet (gOfcGlobals.OfcGlbMib.FsofcIfTable,
                                          (tRBElem *) & (lOfcFsofcIfEntry));

            if (pOfcFsofcIfEntry == NULL)
            {
                OFC_TRC_FUNC ((OFC_FN_EXIT,
                               "\n VlanUpdate  Failed, due to non-existing If MIB Table Entry \n"));
                return OFC_FALSE;
            }
            if (i4Flag == VLAN_CREATE)
            {
                pOfcFsofcIfEntry->u4VlanId = u4VlanId;
            }
            else
            {
                pOfcFsofcIfEntry->u4VlanId = OFC_ZERO;
            }

            u1PortFlag = (UINT1) (u1PortFlag << OFC_ONE);
        }
    }

    return OFC_TRUE;
}

/*****************************************************************************
 *
 *    Function Name        : OfcVlanCreateOpenflowVlan 
 *
 *    Description          : This function creates openflow vlan in OFC
 *
 *    Input(s)             : VlanId
 *
 *    Output(s)            : None.
 *
 *    Returns            :OSIX_SUCCESS if vlan creation is successful
 *                        OSIX_FAILURE if vlan creation is not successful
 *****************************************************************************/

INT4
OfcVlanCreateOpenflowVlan (UINT4 u4VlanId)
{
    return (OfcVlanInterfaceEventSend
            (u4VlanId, OFC_VLAN_CREATE, NULL, NULL, OFC_ZERO));

}

/*****************************************************************************
 *
 *    Function Name        : OfcVlanDeleteOpenflowVlan 
 *
 *    Description          : This function deletes openflow vlan in OFC
 *
 *    Input(s)             : VlanId
 *
 *    Output(s)            : None.
 *
 *    Returns              :OSIX_SUCCESS if vlan deletion is successful
 *                          OSIX_FAILURE if vlan deletion is not successful
 *****************************************************************************/

INT4
OfcVlanDeleteOpenflowVlan (UINT4 u4VlanId)
{
    UINT1              *pu1Alias = NULL;
    UINT1               au1IfName[CFA_MAX_IFALIAS_LENGTH];
    UINT4               u4IfIndex = OFC_ZERO;
    UINT4               u4ContextId = OFC_ZERO;

    MEMSET (&au1IfName, OFC_ZERO, CFA_MAX_IFALIAS_LENGTH);

    pu1Alias = au1IfName;
    STRCPY (au1IfName, OPENFLOW_VLAN_ALIAS_PREFIX);
    SPRINTF ((CHR1 *) pu1Alias, "%s%u", au1IfName, u4VlanId);

    OfcGetInterfaceIndexFromName (pu1Alias, &u4IfIndex);

    /* Get the Context ID for the InPut IfIndex */
    if (OfcGetContextIdFromIfIndex (u4IfIndex, &u4ContextId) != OFC_SUCCESS)
    {
        OFC_TRC_FUNC ((OFC_FN_EXIT, "\r\nNo Context Mapping Found\n"));
        return OSIX_FAILURE;
    }

    if (u4ContextId != OFC_INVALID_CONTEXT)
    {
        OFC_TRC_FUNC ((OFC_FN_EXIT, "\r\nContext Mapping Found\n"));
        return OSIX_FAILURE;
    }

    return (OfcVlanInterfaceEventSend
            (u4VlanId, OFC_VLAN_DELETE, NULL, NULL, OFC_ZERO));

}

/*****************************************************************************
 *
 *    Function Name        : OfcVlanSetPorts
 *
 *    Description          : This function add member ports to openflow vlan in OFC
 *
 *    Input(s)             : VlanId
 *
 *    Output(s)            : None.
 *
 *    Returns            :OSIX_SUCCESS if vlan creation is successful
 *                        OSIX_FAILURE if vlan creation is not successful
 *****************************************************************************/

INT4
OfcVlanSetPorts (UINT4 u4VlanId, UINT1 *pu1MemberPorts,
                 UINT1 *pu1UntaggedPorts, UINT4 u4Flag)
{
    return (OfcVlanInterfaceEventSend
            (u4VlanId, OFC_VLAN_ADD_MEMBER, pu1MemberPorts, pu1UntaggedPorts,
             u4Flag));

}

/**********************************************************************************************/
/* Function    :  OfcVlanInterfaceEventSend                                                   */
/* INPUT        : pu1MemberPorts    - Contains member ports                                   */
/*                pu1UntaggedPorts  - Contains untagged ports                                 */
/*                u4VlanId          - Vlan ID                                                 */
/*                u4Flag            - Flag to indicate                                        */
/*                                    addition/removal of ports                               */
/*                i4Evt - Event Type                                                          */
/* Description :  This Routine is used to send  Vlan interface Create/Delete Event to OFC Task*/
/* Output      :  None                                                                        */
/* Returns     :  OSIX_SUCCESS or OSIX_FAILURE                                                */
/**********************************************************************************************/
INT4
OfcVlanInterfaceEventSend (UINT4 u4VlanId, INT4 i4Evt, UINT1 *pu1MemberPorts,
                           UINT1 *pu1UntaggedPorts, UINT4 u4Flag)
{
    tOfcQueMsg         *pOfcVlanMsgInfo = NULL;
    UINT1               au1IfName[CFA_MAX_IFALIAS_LENGTH];
    UINT1              *pu1Alias = NULL;
    UINT4               u4ContextId = OFC_ZERO;
    UINT4               u4IfIndex = OFC_ZERO;

    OFC_TRC_FUNC ((OFC_FN_ENTRY, "Entry :: OfcVlanInterfaceEventSend\n"));

    pOfcVlanMsgInfo = (tOfcQueMsg *) MemAllocMemBlk (OFC_QUEUE_POOLID);
    if (pOfcVlanMsgInfo == NULL)
    {
        return OSIX_FAILURE;
    }

    MEMSET (pOfcVlanMsgInfo, OFC_ZERO, sizeof (tOfcQueMsg));

    if (i4Evt == OFC_VLAN_ADD_MEMBER)
    {
        pOfcVlanMsgInfo->pu1MemberPorts =
            UtilPlstAllocLocalPortList (OFC_VLAN_PORTS_LEN);
        if (pOfcVlanMsgInfo->pu1MemberPorts == NULL)
        {
            OFC_TRC ((OFC_TASK_TRC,
                      "OfcVlanInterfaceEventSend :: Memory allocation for port list failed\r\n"));
            FsUtilReleaseBitList ((UINT1 *) pOfcVlanMsgInfo->pu1MemberPorts);
            MemReleaseMemBlock (OFC_QUEUE_POOLID, (UINT1 *) pOfcVlanMsgInfo);
            return OSIX_FAILURE;
        }
        pOfcVlanMsgInfo->pu1UntaggedPorts =
            UtilPlstAllocLocalPortList (OFC_VLAN_PORTS_LEN);
        if (pOfcVlanMsgInfo->pu1UntaggedPorts == NULL)
        {
            OFC_TRC ((OFC_TASK_TRC,
                      "OfcVlanInterfaceEventSend :: Memory allocation for port list failed\r\n"));
            FsUtilReleaseBitList ((UINT1 *) pu1UntaggedPorts);
            FsUtilReleaseBitList ((UINT1 *) pOfcVlanMsgInfo->pu1MemberPorts);
            MemReleaseMemBlock (OFC_QUEUE_POOLID, (UINT1 *) pOfcVlanMsgInfo);
            return OSIX_FAILURE;
        }
        MEMSET (pOfcVlanMsgInfo->pu1MemberPorts, OFC_ZERO, OFC_VLAN_PORTS_LEN);
        MEMSET (pOfcVlanMsgInfo->pu1UntaggedPorts, OFC_ZERO,
                OFC_VLAN_PORTS_LEN);
        MEMCPY (pOfcVlanMsgInfo->pu1MemberPorts, pu1MemberPorts,
                OFC_VLAN_PORTS_LEN);
        MEMCPY (pOfcVlanMsgInfo->pu1UntaggedPorts, pu1UntaggedPorts,
                OFC_VLAN_PORTS_LEN);
        pOfcVlanMsgInfo->u4Flag = u4Flag;

        pu1Alias = au1IfName;
        STRCPY (au1IfName, OPENFLOW_VLAN_ALIAS_PREFIX);
        SPRINTF ((CHR1 *) pu1Alias, "%s%u", au1IfName, u4VlanId);

        /* Get the OFCL Interfce Index for the InPut VlanID */
        OfcGetInterfaceIndexFromName (pu1Alias, &u4IfIndex);

        /* Get the Context ID for the InPut IfIndex */
        if (OfcGetContextIdFromIfIndex (u4IfIndex, &u4ContextId) != OFC_SUCCESS)
        {
            OFC_TRC_FUNC ((OFC_FN_EXIT, "\r\nInvalid Context Mapping\n"));
            MemReleaseMemBlock (OFC_QUEUE_POOLID, (UINT1 *) pOfcVlanMsgInfo);
            return OSIX_FAILURE;
        }

        /*
         * Return Failure if the Openflow Vlan is not mapped to any context before
         * updating the ports to it.
         */

        if (u4ContextId == OFC_INVALID_CONTEXT)
        {
            OFC_TRC_FUNC ((OFC_FN_EXIT, "\r\nPort Mapping/Unmapping Failed, "
                           "Update Openflow vlan Context Fisrt\n"));
            MemReleaseMemBlock (OFC_QUEUE_POOLID, (UINT1 *) pOfcVlanMsgInfo);
            return OSIX_FAILURE;
        }

        /*
         * Check the Interface Mapping. Make sure the ports are not mapped to any
         * other vlan. Also make sure that the ports belongs to same openflow context
         */
        /*
         * Invoking API for tagged ports
         */

        if (OfcValidatePortMappingToOfcVlan
            (pu1MemberPorts, u4ContextId, u4VlanId) == OFC_FAILURE)
        {
            OFC_TRC_FUNC ((OFC_FN_EXIT,
                           "\r\nPort Validation for Openflow VLAN Failed\n"));
            MemReleaseMemBlock (OFC_QUEUE_POOLID, (UINT1 *) pOfcVlanMsgInfo);
            return OSIX_FAILURE;
        }

        /*
         * Invoking API for un-tagged ports
         */
        if (OfcValidatePortMappingToOfcVlan
            (pu1UntaggedPorts, u4ContextId, u4VlanId) == OFC_FAILURE)
        {
            OFC_TRC_FUNC ((OFC_FN_EXIT,
                           "\r\nPort Validation for Openflow VLAN Failed\n"));
            MemReleaseMemBlock (OFC_QUEUE_POOLID, (UINT1 *) pOfcVlanMsgInfo);
            return OSIX_FAILURE;
        }
    }
    pOfcVlanMsgInfo->u4VlanId = u4VlanId;
    pOfcVlanMsgInfo->u4MsgType = (UINT4) i4Evt;

    if (OsixQueSend (gOfcGlobals.ofcQueId, (UINT1 *) &(pOfcVlanMsgInfo),
                     OSIX_DEF_MSG_LEN) == OSIX_FAILURE)
    {
        OFC_TRC ((OFC_TASK_TRC,
                  "OfcVlanInterfaceEventSend :: Queue send failed\r\n"));
        MemReleaseMemBlock (OFC_QUEUE_POOLID, (UINT1 *) pOfcVlanMsgInfo);
        return OSIX_FAILURE;
    }

    if (OsixEvtSend (gOfcGlobals.ofcTaskId, OFC_QUEUE_EVENT) == OSIX_FAILURE)
    {
        OFC_TRC ((OFC_TASK_TRC, " Unable to post event to OFC \n"));
        MemReleaseMemBlock (OFC_QUEUE_POOLID, (UINT1 *) pOfcVlanMsgInfo);
        return OFC_FAILURE;
    }

    OFC_TRC_FUNC ((OFC_FN_EXIT, "EXIT :: OfcVlanInterfaceEventSend\n"));
    KW_FALSEPOSITIVE_FIX (pOfcVlanMsgInfo);
    return OSIX_SUCCESS;

}
