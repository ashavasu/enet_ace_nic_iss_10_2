/******************************************************************************
 * Copyright (C) Future Software Limited,2005
 *
 * $Id: ofcutil.c,v 1.9 2014/10/10 11:57:29 siva Exp $
 * Description : This file contains the utility 
 *               functions of the OFC module
 *****************************************************************************/

#include "ofcinc.h"
#include "ofccli.h"
#include "ofccntlr.h"
#include "ofcflow.h"
#include "ofcapi.h"
#include "ofcl.h"
#include "ofcmpmsg.h"
#include "fssocket.h"
#include "ofcvlan.h"

#if defined (NPAPI_WANTED) && defined (OPENFLOW_TCAM)
#ifdef DPA_WANTED
#include "ofc131dpa.h"

UINT1               gau1MapTable[OFCDPA_MAX_TABLES] =
    { 0, 10, 20, 24, 25, 30, 31, 32, 38, 39, 40, 50, 60 };
#endif /* DPA_WANTED */
#endif /* NPAPI_WANTED && OPENFLOW_TCAM */

/************************************************************************
 *  Function Name   : OfcGetFlowTableEntry
 *  Description     : Function to get Flow Table Entry from Context ID
 *                    and Table Index
 *  Input           : Conext ID and Table Index
 *  Output          : None
 *  Returns         : Pointer to tOfcFsofcFlowTable or NULL
 ************************************************************************/

tOfcFsofcFlowTable *
OfcGetFlowTableEntry (UINT4 u4ContextId, UINT4 u4TableIndex)
{
    tOfcFsofcFlowTable *pTableEntry = NULL;
    tOfcFsofcCfgEntry  *pOfcFsofcCfgEntry = NULL;
    tOfcFsofcFlowTable  TableEntry;

    pOfcFsofcCfgEntry = OfcGetFsofcCfgEntry (u4ContextId);
    if (pOfcFsofcCfgEntry == NULL)
    {
        return NULL;
    }

    if (pOfcFsofcCfgEntry->OfcFlowtable == NULL)
    {
        return NULL;
    }
    pTableEntry = RBTreeGetFirst (pOfcFsofcCfgEntry->OfcFlowtable);
    if (pTableEntry == NULL)
    {
        return NULL;
    }

    do
    {
        if ((pTableEntry->u4ContextId == u4ContextId)
            && (pTableEntry->u4TableIndex == u4TableIndex))
        {
            return pTableEntry;
        }

        MEMSET (&TableEntry, OFC_ZERO, sizeof (tOfcFsofcFlowTable));
        TableEntry.u4TableIndex = pTableEntry->u4TableIndex;
        TableEntry.u4ContextId = pTableEntry->u4ContextId;
    }
    while ((pTableEntry = RBTreeGetNext (pOfcFsofcCfgEntry->OfcFlowtable,
                                         &TableEntry, NULL)) != NULL);
    return NULL;
}

/************************************************************************
 *  Function Name   :OfcGetControllerConnTableEntry
 *  Description     :Function to get ControllerEntry Entry from SocketDesc
 *  Input           : SocketDesc
 *  Output          : None
 *  Returns         : Pointer to ControllerEntry or NULL
 ************************************************************************/

tOfcFsofcControllerConnEntry *
OfcGetControllerConnTableEntry (INT4 i4SocketDesc)
{
    tOfcFsofcControllerConnEntry *pControllerEntry = NULL;

    if (gOfcGlobals.OfcGlbMib.FsofcControllerConnTable == NULL)
    {
        return NULL;
    }
    pControllerEntry =
        RBTreeGetFirst (gOfcGlobals.OfcGlbMib.FsofcControllerConnTable);
    if (pControllerEntry == NULL)
    {
        return NULL;
    }
    do
    {
        if (pControllerEntry->i4SocketDesc == i4SocketDesc)
        {
            return pControllerEntry;
        }
    }
    while ((pControllerEntry =
            RBTreeGetNext (gOfcGlobals.OfcGlbMib.FsofcControllerConnTable,
                           pControllerEntry, NULL)) != NULL);
    return NULL;

}

/*****************************************************************************/
/* Function Name : OfcUpdateControllerConnRole                               */
/* Description   : This routine updates the role of the controller if the    */
/*                 requested role is master                                  */
/* Input(s)      : tOfp131RoleReq *pOfcRoleRequest                           */
/*                 UINT4 u4ContextId                                         */
/* Output(s)     : None                                                      */
/* Return(s)     : OSIX_SUCCESS OR OSIX_FAILURE                              */
/*****************************************************************************/

INT4
OfcUpdateControllerConnRole (tOfp131RoleReq * pOfcRoleRequest,
                             UINT4 u4ContextId)
{
    tOfcFsofcControllerConnEntry *pOfcGetFsofcControllerConnEntry = NULL;

    /* Check the generation-id in the role request with the largest 
       generation-id generated so far */
    if (FsofcCheckGenerationId (pOfcRoleRequest, u4ContextId) == OFC_FAILURE)
    {
        return OSIX_FAILURE;
    }

    /* Make the controller which are master to slave */
    pOfcGetFsofcControllerConnEntry =
        RBTreeGetFirst (gOfcGlobals.OfcGlbMib.FsofcControllerConnTable);
    if (pOfcGetFsofcControllerConnEntry == NULL)
    {
        return OSIX_FAILURE;
    }
    do
    {
        if (pOfcGetFsofcControllerConnEntry->MibObject.
            i4FsofcControllerRole == OFC_CONNROLE_MASTER)
        {
            pOfcGetFsofcControllerConnEntry->MibObject.
                i4FsofcControllerRole = OFC_CONNROLE_SLAVE;
        }
    }
    while ((pOfcGetFsofcControllerConnEntry =
            RBTreeGetNext (gOfcGlobals.OfcGlbMib.FsofcControllerConnTable,
                           pOfcGetFsofcControllerConnEntry, NULL)) != NULL);

    return OSIX_SUCCESS;
}

/*****************************************************************************/
/* Function Name : OfcUpdateControllerConnState                              */
/* Description   : This routine updates the role of the controller if the    */
/*                 requested role is master                                  */
/* Input(s)      : tOfcFsofcControllerConnEntry                              */
/*                    *pOfcGetFsofcControllerConnEntry                       */
/* Output(s)     : None                                                      */
/* Return(s)     : OSIX_SUCCESS OR OSIX_FAILURE                              */
/*****************************************************************************/

INT4
OfcUpdateControllerConnState (tOfcFsofcControllerConnEntry
                              * pOfcFsofcControllerConnEntry,
                              INT4 i4FsofcControllerConnState)
{
    tOfcFsofcControllerConnEntry *pOfcGetFsofcControllerConnEntry = NULL;

    /* Update the Connection states for Auxilary connections, if the Primary 
       connection goes down */
    pOfcGetFsofcControllerConnEntry =
        RBTreeGetNext (gOfcGlobals.OfcGlbMib.FsofcControllerConnTable,
                       pOfcFsofcControllerConnEntry, NULL);
    if (pOfcGetFsofcControllerConnEntry == NULL)
    {
        return OSIX_FAILURE;
    }
    do
    {
        if ((pOfcGetFsofcControllerConnEntry->MibObject.u4FsofcContextId ==
             pOfcFsofcControllerConnEntry->MibObject.u4FsofcContextId) &&
            (MEMCMP
             (pOfcGetFsofcControllerConnEntry->MibObject.
              au1FsofcControllerIpAddress,
              pOfcFsofcControllerConnEntry->MibObject.
              au1FsofcControllerIpAddress,
              sizeof (pOfcGetFsofcControllerConnEntry->MibObject.
                      au1FsofcControllerIpAddress)) == OFC_ZERO))
        {
            if (pOfcGetFsofcControllerConnEntry->MibObject.
                i4FsofcControllerConnAuxId != OFC_ZERO)
            {
                if (i4FsofcControllerConnState == OFC_NOT_CONNECTED)
                {
                    /* Update the client state. Close the socket and clear the datastructure */
                    pOfcGetFsofcControllerConnEntry->MibObject.
                        i4FsofcControllerConnState = OFC_NOT_CONNECTED;
                    SelRemoveFd (pOfcGetFsofcControllerConnEntry->i4SocketDesc);
                    if (pOfcGetFsofcControllerConnEntry->i4SocketDesc !=
                        OFC_MINUS_ONE)
                    {
                        close (pOfcGetFsofcControllerConnEntry->i4SocketDesc);
                    }
                    pOfcGetFsofcControllerConnEntry->i4SocketDesc = -1;
                }
                else if (i4FsofcControllerConnState == OFC_CONNECTED)
                {
                    /* Recreate the auxilary connection */
                    OfcCreateController (pOfcGetFsofcControllerConnEntry);
                }
            }
            else
            {
                continue;
            }
        }
        else
        {
            break;
        }
    }
    while ((pOfcGetFsofcControllerConnEntry =
            RBTreeGetNext (gOfcGlobals.OfcGlbMib.FsofcControllerConnTable,
                           pOfcGetFsofcControllerConnEntry, NULL)) != NULL);

    return OSIX_SUCCESS;
}

/*****************************************************************************/
/* Function Name : OfcUpdateFlowEntryMibObjectStats                          */
/* Description   : This routine updates the stats of the Flow Entry Object   */
/*                 For Version 1.0.0                                         */
/* Input(s)      : tOfcFlowEntry *pOfcFsofcFlowEntry                         */
/* Output(s)     : None                                                      */
/* Return(s)     : None                                                      */
/*****************************************************************************/

VOID
OfcUpdateFlowEntryMibObjectStats (tOfcFlowEntry * pOfcFsofcFlowEntry)
{
    tOfcFsofcFlowEntry *pOfcGetFsofcFlowEntry = NULL;
    tOfcFsofcCfgEntry  *pOfcFsofcCfgEntry = NULL;

    pOfcFsofcCfgEntry = (tOfcFsofcCfgEntry *)
        RBTreeGetFirst (gOfcGlobals.OfcGlbMib.FsofcCfgTable);
    if (pOfcFsofcCfgEntry == NULL)
    {
        return;
    }

    pOfcGetFsofcFlowEntry =
        RBTreeGetFirst (gOfcGlobals.OfcGlbMib.FsofcFlowTable);
    do
    {
        if (pOfcGetFsofcFlowEntry == NULL)
        {
            return;
        }

        if ((pOfcGetFsofcFlowEntry->MibObject.u4FsofcTableIndex == OFC_ZERO) &&
            (pOfcGetFsofcFlowEntry->MibObject.u4FsofcContextId ==
             pOfcFsofcCfgEntry->MibObject.u4FsofcContextId) &&
            (pOfcGetFsofcFlowEntry->MibObject.u4FsofcFlowIndex ==
             pOfcFsofcFlowEntry->u4FlowIndex))
        {
            pOfcGetFsofcFlowEntry->MibObject.u8FsofcFlowPacketCount.lsn =
                pOfcFsofcFlowEntry->u4PktCount;
            pOfcGetFsofcFlowEntry->MibObject.u8FsofcFlowPacketCount.msn =
                OFC_ZERO;
            pOfcGetFsofcFlowEntry->MibObject.u8FsofcFlowByteCount.lsn =
                pOfcFsofcFlowEntry->u4ByteCount;
            pOfcGetFsofcFlowEntry->MibObject.u8FsofcFlowByteCount.msn =
                OFC_ZERO;
            return;
        }
    }
    while ((pOfcGetFsofcFlowEntry =
            RBTreeGetNext (gOfcGlobals.OfcGlbMib.FsofcFlowTable,
                           pOfcGetFsofcFlowEntry, NULL)) != NULL);
    return;
}

/********************************************************************************/
/* Function Name : FsofcCheckGenerationId                                       */
/* Description   : This routine checks for the Generation id in the role request*/
/* Input(s)      : tOfp131RoleReq  - pOfcRoleRequest                            */
/*                 UINT4  - u4ContextId                                         */
/* Output(s)     : None                                                         */
/* Return(s)     : OFC_SUCCESS OR OFC_FAILURE                                   */
/********************************************************************************/

INT4
FsofcCheckGenerationId (tOfp131RoleReq * pOfcRoleRequest, UINT4 u4ContextId)
{
    tOfcFsofcCfgEntry  *pOfcFsofcCfgEntry = NULL;

    FS_UINT8            u8Tmp1, u8Tmp2;
    FS_UINT8           *pu8Tmp1, *pu8Tmp2;

    pu8Tmp1 = &u8Tmp1;
    pu8Tmp2 = &u8Tmp2;

    pOfcFsofcCfgEntry = OfcGetFsofcCfgEntry (u4ContextId);
    if (pOfcFsofcCfgEntry == NULL)
    {
        return OFC_FAILURE;
    }

    u8Tmp1.u4Lo = pOfcRoleRequest->u8GenId.u4Lo;
    u8Tmp1.u4Hi = pOfcRoleRequest->u8GenId.u4Hi;

    u8Tmp2.u4Lo = pOfcFsofcCfgEntry->u8GenerationId.u4Lo;
    u8Tmp2.u4Hi = pOfcFsofcCfgEntry->u8GenerationId.u4Hi;

    /* If generation is defined, then compare the generation id in the request with
       the largest generation-id */
    if (pOfcFsofcCfgEntry->u1GenerationIsDefined)
    {
        if (FSAP_U8_CMP (pu8Tmp1, pu8Tmp2) >= OFC_ZERO)
        {
            pOfcFsofcCfgEntry->u8GenerationId.u4Hi =
                pOfcRoleRequest->u8GenId.u4Hi;
            pOfcFsofcCfgEntry->u8GenerationId.u4Lo =
                pOfcRoleRequest->u8GenId.u4Lo;
            return OFC_SUCCESS;
        }
        else
        {
            return OFC_FAILURE;
        }
    }
    else
    {
        /* If generation is not defined, then make the generation-id to be defined */
        pOfcFsofcCfgEntry->u8GenerationId.u4Hi = pOfcRoleRequest->u8GenId.u4Hi;
        pOfcFsofcCfgEntry->u8GenerationId.u4Lo = pOfcRoleRequest->u8GenId.u4Lo;
        pOfcFsofcCfgEntry->u1GenerationIsDefined = TRUE;
        return OFC_SUCCESS;
    }
}

/*****************************************************************************/
/* Function Name : OfcGetOfcDesc                                             */
/* Description   : This routine updates the switch description in the reply  */
/* Input(s)      : tOfcDesciReply  - pOfcDesciReply                          */
/* Output(s)     : None                                                      */
/* Return(s)     : OSIX_SUCCESS                                              */
/*****************************************************************************/

INT4
OfcGetOfcDesc (tOfcDesciReply * pOfcDesciReply)
{
    STRNCPY (pOfcDesciReply->au1MfrDesc, "Aricent",
             sizeof (pOfcDesciReply->au1MfrDesc));
    STRNCPY (pOfcDesciReply->au1HwDesc, "PC Linux",
             sizeof (pOfcDesciReply->au1HwDesc));
    STRNCPY (pOfcDesciReply->au1SwDesc, "ISS",
             sizeof (pOfcDesciReply->au1SwDesc));
    STRNCPY (pOfcDesciReply->au1SerialNum, "1.3.1",
             sizeof (pOfcDesciReply->au1SerialNum));
    STRNCPY (pOfcDesciReply->au1DpDesc, "Aricent OFC",
             sizeof (pOfcDesciReply->au1DpDesc));
    return OSIX_SUCCESS;
}

/********************************************************************************/
/* Function Name : OfcCheckInterfaceContextId                                   */
/* Description   : This routine checks for the Context id Validity              */
/* Input(s)      : tOfcFsofcIfEntry - pOfcFsofcIfEntry                          */
/* Output(s)     : None                                                         */
/* Return(s)     : OFC_SUCCESS OR OFC_FAILURE                                   */
/********************************************************************************/

INT4
OfcCheckInterfaceContextId (tOfcFsofcIfEntry * pOfcFsofcIfEntry)
{
    tOfcFsofcCfgEntry  *pOfcGetFsofcCfgEntry = NULL;

    pOfcGetFsofcCfgEntry = RBTreeGetFirst (gOfcGlobals.OfcGlbMib.FsofcCfgTable);
    do
    {
        if (pOfcGetFsofcCfgEntry == NULL)
        {
            return OFC_FAILURE;
        }

        if (pOfcGetFsofcCfgEntry->MibObject.u4FsofcContextId ==
            pOfcFsofcIfEntry->MibObject.u4FsofcIfContextId)

        {
            return OFC_SUCCESS;
        }
    }
    while ((pOfcGetFsofcCfgEntry =
            RBTreeGetNext (gOfcGlobals.OfcGlbMib.FsofcCfgTable,
                           pOfcGetFsofcCfgEntry, NULL)) != NULL);

    return OFC_FAILURE;
}

/********************************************************************************/
/* Function Name : OfcCheckInterfaceIsMapped                                    */
/* Description   : This routine checks for the Interface mapping and returns    */
/*                 Success in interface is not mapped to any Context, else      */
/*                 returns Failure                                              */
/* Input(s)      : tOfcFsofcIfEntry - pOfcFsofcIfEntry                          */
/* Output(s)     : None                                                         */
/* Return(s)     : OFC_SUCCESS OR OFC_FAILURE                                   */
/********************************************************************************/

INT4
OfcCheckInterfaceIsMapped (tOfcFsofcIfEntry * pOfcFsofcIfEntry)
{
    tOfcFsofcIfEntry   *pOfcGetFsofcIfEntry = NULL;

    pOfcGetFsofcIfEntry = RBTreeGetFirst (gOfcGlobals.OfcGlbMib.FsofcIfTable);

    do
    {
        if (pOfcGetFsofcIfEntry == NULL)
        {
            return OFC_FAILURE;
        }
        /*
         * Return Success if the Interface is already mapped to same context
         */
        if ((pOfcGetFsofcIfEntry->MibObject.i4FsofcIfIndex ==
             pOfcFsofcIfEntry->MibObject.i4FsofcIfIndex) &&
            pOfcGetFsofcIfEntry->MibObject.u4FsofcIfContextId ==
            pOfcFsofcIfEntry->MibObject.u4FsofcIfContextId)
        {
            return OFC_SUCCESS;
        }
        if ((pOfcGetFsofcIfEntry->MibObject.i4FsofcIfIndex ==
             pOfcFsofcIfEntry->MibObject.i4FsofcIfIndex) &&
            pOfcGetFsofcIfEntry->MibObject.u4FsofcIfContextId !=
            OFC_INVALID_CONTEXT)
        {
            return OFC_FAILURE;
        }
    }
    while ((pOfcGetFsofcIfEntry =
            RBTreeGetNext (gOfcGlobals.OfcGlbMib.FsofcIfTable,
                           pOfcGetFsofcIfEntry, NULL)) != NULL);

    return OFC_SUCCESS;
}

/*****************************************************************************/
/* Function Name : OfcUpdateInterfaceMapping                                 */
/* Description   : This routine updates the mapping of the Interface Entry   */
/* Input(s)      : tOfcFsofcIfEntry *pOfcIfEntry                             */
/* Output(s)     : None                                                      */
/* Return(s)     : OSIX_SUCCESS OR OSIX_FAILURE                              */
/*****************************************************************************/

INT4
OfcUpdateInterfaceMapping (tOfcFsofcIfEntry * pOfcFsofcIfEntry)
{
    tOfcFsofcIfEntry   *pOfcGetFsofcIfEntry = NULL;

    pOfcGetFsofcIfEntry = RBTreeGetFirst (gOfcGlobals.OfcGlbMib.FsofcIfTable);
    do
    {
        if (pOfcGetFsofcIfEntry == NULL)
        {
            return OSIX_FAILURE;
        }

        if ((pOfcGetFsofcIfEntry->MibObject.u4FsofcContextId ==
             pOfcFsofcIfEntry->MibObject.u4FsofcContextId) &&
            (pOfcGetFsofcIfEntry->MibObject.i4FsofcIfIndex ==
             pOfcFsofcIfEntry->MibObject.i4FsofcIfIndex))
        {
            pOfcGetFsofcIfEntry->MibObject.u4FsofcIfContextId =
                pOfcFsofcIfEntry->MibObject.u4FsofcIfContextId;
            return OSIX_SUCCESS;
        }
    }
    while ((pOfcGetFsofcIfEntry =
            RBTreeGetNext (gOfcGlobals.OfcGlbMib.FsofcIfTable,
                           pOfcGetFsofcIfEntry, NULL)) != NULL);

    return OSIX_FAILURE;
}

/************************************************************************
 *  Function Name   : OfcGetFsofcCfgEntry
 *  Description     : Function to get Cfg Entry from Context ID
 *  Input           : UINT4 u4ContextId
 *  Output          : None
 *  Returns         : Pointer to CfgEntry or NULL
 ************************************************************************/

tOfcFsofcCfgEntry  *
OfcGetFsofcCfgEntry (UINT4 u4ContextId)
{
    tOfcFsofcCfgEntry  *pOfcFsofcCfgEntry = NULL;

    if (gOfcGlobals.OfcGlbMib.FsofcCfgTable == NULL)
    {
        return NULL;
    }

    pOfcFsofcCfgEntry = RBTreeGetFirst (gOfcGlobals.OfcGlbMib.FsofcCfgTable);
    if (pOfcFsofcCfgEntry == NULL)
    {
        return NULL;
    }
    do
    {
        if (pOfcFsofcCfgEntry->MibObject.u4FsofcContextId == u4ContextId)
        {
            return pOfcFsofcCfgEntry;
        }
    }
    while ((pOfcFsofcCfgEntry =
            RBTreeGetNext (gOfcGlobals.OfcGlbMib.FsofcCfgTable,
                           pOfcFsofcCfgEntry, NULL)) != NULL);
    return NULL;
}

/************************************************************************
 *  Function Name   : OfcGetContextIdFromIfIndex
 *  Description     : Function to get Cfg Context ID from Interface Index
 *  Input           : UINT4 *pu4ContextId
 *                    UINT4  u4IfIndex
 *  Output          : None
 *  Return(s)       : OFC_SUCCESS OR OFC_FAILURE    
 ***********************************************************************/

INT4
OfcGetContextIdFromIfIndex (UINT4 u4IfIndex, UINT4 *pu4ContextId)
{
    tOfcFsofcIfEntry   *pOfcFsofcIfEntry = NULL;

    if (gOfcGlobals.OfcGlbMib.FsofcIfTable == NULL)
    {
        return OFC_FAILURE;
    }

    pOfcFsofcIfEntry = RBTreeGetFirst (gOfcGlobals.OfcGlbMib.FsofcIfTable);

    if (pOfcFsofcIfEntry == NULL)
    {
        return OFC_FAILURE;
    }
    do
    {
        if (pOfcFsofcIfEntry->MibObject.i4FsofcIfIndex == (INT4) u4IfIndex)
        {
            *pu4ContextId = pOfcFsofcIfEntry->MibObject.u4FsofcIfContextId;
            return OFC_SUCCESS;
        }
    }
    while ((pOfcFsofcIfEntry =
            RBTreeGetNext (gOfcGlobals.OfcGlbMib.FsofcIfTable,
                           pOfcFsofcIfEntry, NULL)) != NULL);
    return OFC_FAILURE;
}

/*****************************************************************************/
/* Function Name : OfcUpdateCfgConnState                                     */
/* Description   : This routine updates the Conn State of the Client Context */
/* Input(s)      : tOfcFsofcControllerConnEntry *pControllerEntry            */
/* Output(s)     : None                                                      */
/* Return(s)     : OSIX_SUCCESS                                              */
/*****************************************************************************/

INT4
OfcUpdateCfgConnState (tOfcFsofcControllerConnEntry * pControllerEntry)
{
    tOfcFsofcCfgEntry  *pCfgEntry = NULL;
    tOfcFsofcCfgEntry   lCfgEntry;

    MEMSET (&lCfgEntry, OFC_ZERO, sizeof (tOfcFsofcCfgEntry));

    lCfgEntry.MibObject.u4FsofcContextId =
        pControllerEntry->MibObject.u4FsofcContextId;

    pCfgEntry =
        RBTreeGet (gOfcGlobals.OfcGlbMib.FsofcCfgTable,
                   (tRBElem *) & (lCfgEntry));
    if (pCfgEntry != NULL)
    {
        if (pControllerEntry->MibObject.i4FsofcControllerConnState ==
            OFC_CONNECTED)
        {
            pCfgEntry->u4OfcClientState = OFC_CONNECTED;
            return OSIX_SUCCESS;
        }
        else if ((pControllerEntry->MibObject.i4FsofcControllerConnState ==
                  OFC_NOT_CONNECTED)
                 && (pControllerEntry->MibObject.i4FsofcControllerRole ==
                     OFC_CONNROLE_MASTER))
        {
            pCfgEntry->u4OfcClientState = OFC_NOT_CONNECTED;
        }

        else
        {
            /* 
             * Move the client to disconnected state, when the controller 
             * with EQUAL role is DISCONNECTED  and all controllers are in disconnected state
             * or else move the client to the connected state
             */
            while ((pControllerEntry =
                    RBTreeGetNext (gOfcGlobals.OfcGlbMib.
                                   FsofcControllerConnTable, pControllerEntry,
                                   NULL)) != NULL)
            {
                if (pCfgEntry->MibObject.u4FsofcContextId !=
                    pControllerEntry->MibObject.u4FsofcContextId)
                {
                    break;
                }

                if (pControllerEntry->MibObject.i4FsofcControllerConnState ==
                    OFC_CONNECTED)
                {
                    pCfgEntry->u4OfcClientState = OFC_CONNECTED;
                    return OSIX_SUCCESS;
                }
            }
            pCfgEntry->u4OfcClientState = OFC_NOT_CONNECTED;
        }
    }
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/* Function Name : OfcHandleOpenflowSwModeProcess                            */
/* Description   : This routine handles the Sw Mode Process                  */
/* Input(s)      : UINT4 - u4IfIndex                                         */
/* Output(s)     : None                                                      */
/* Return(s)     : OFC_SUCCESS or OFC_FAILURE                                */
/*****************************************************************************/

INT4
OfcHandleOpenflowSwModeProcess (UINT4 *pu4IfIndex, tCRU_BUF_CHAIN_HEADER * pBuf)
{
    UINT4               u4ContextId = OFC_INVALID_CONTEXT;
    INT4                i4RetValue = OFC_FAILURE;
    tOfcFsofcCfgEntry   CfgEntry;

    MEMSET (&CfgEntry, OFC_ZERO, sizeof (tOfcFsofcCfgEntry));

    /* Check for the inband controller packets */
    if (OfcInbandCntlrRxPktCheck (pu4IfIndex, pBuf) == OFC_SUCCESS)
    {
        /* this is inband controller packet */
        return OFC_SUCCESS;
    }

    /* Check for the hybrid packets from ISS */
    if (OfcHybridRxPktFromIssCheck (*pu4IfIndex, &u4ContextId) == OFC_SUCCESS)
    {
        /* these are hybrid packets from ISS to Openflow */
        return OFC_SUCCESS;
    }

    /*
     * Get the Context-id using the If Index 
     */
    i4RetValue = OfcGetContextIdFromIfIndex (*pu4IfIndex, &u4ContextId);

    if ((i4RetValue != OFC_SUCCESS)
        || (u4ContextId != (UINT4) OFC_INVALID_CONTEXT))
    {
        CfgEntry.MibObject.u4FsofcContextId = u4ContextId;
        if ((OfcGetAllFsofcCfgTable (&CfgEntry) != OFC_SUCCESS))
        {
            OFC_TRC_FUNC ((OFC_ACTS_TRC, "OfcGetAllFsofcCfgTable Failed :"
                           "return FAILURE\n"));
            return OFC_FAILURE;
        }
    }

    /* 
     * If the client state is not connected and connection failure mode is in stand alone mode,
     * then return SUCCESS else return FAILURE
     */
    if ((CfgEntry.u4OfcClientState == OFC_NOT_CONNECTED) &&
        (CfgEntry.MibObject.i4FsofcSwitchModeOnConnFailure ==
         OFC_SWITCHMODE_FAIL_STANDALONE))
    {
        return OFC_SUCCESS;
    }

    return OFC_FAILURE;
}

/*****************************************************************************/
/* Function Name : OfcHandleOpenflowPorts                                    */
/* Description   : This routine makes the openflow ports visible to the ISS  */
/*                 and vice-versa                                            */
/* Input(s)      : UINT4 - u4ContextId                                       */
/*                 UINT1 - u1SubType                                         */
/* Output(s)     : None                                                      */
/* Return(s)     : OFC_SUCCESS or OFC_FAILURE                                */
/*****************************************************************************/

INT4
OfcHandleOpenflowPorts (UINT4 u4ContextId, UINT1 u1SubType)
{
    tOfcFsofcIfEntry   *pOfcFsofcIfEntry = NULL;
    tOfcFsofcIfEntry    lOfcFsofcIfEntry;

    MEMSET (&lOfcFsofcIfEntry, OFC_ZERO, sizeof (tOfcFsofcIfEntry));

    /* 
     * Retrieve the openflow ports associated with the corresponding context-id
     * and make the openflow ports visible to the ISS context and vice-versa
     */

    lOfcFsofcIfEntry.MibObject.u4FsofcContextId = u4ContextId;
    pOfcFsofcIfEntry = &lOfcFsofcIfEntry;

    while ((pOfcFsofcIfEntry =
            RBTreeGetNext (gOfcGlobals.OfcGlbMib.FsofcIfTable,
                           pOfcFsofcIfEntry, NULL)) != NULL)
    {
        if (lOfcFsofcIfEntry.MibObject.u4FsofcContextId !=
            pOfcFsofcIfEntry->MibObject.u4FsofcContextId)
        {
            break;
        }

        if ((CfaIfmUpdateExtSubType
             ((UINT4) pOfcFsofcIfEntry->MibObject.i4FsofcIfIndex,
              (UINT2) u1SubType, FALSE)) != OFC_SUCCESS)
        {
            OFC_TRC ((OFC_QUE_TRC, "\r\nCfaIfmUpdateExtSubType failed\n"));
            return OFC_FAILURE;
        }
    }

    return OFC_SUCCESS;
}

/*****************************************************************************/
/* Function Name : OfcValidatePortMappingToOfcVlan                           */
/* Description   : This routine validates the ports to be added to the       */
/*                 openflow vlan                                             */
/* Input(s)      : UINT4 - u4ContextId                                       */
/*               : UINT4 - u4VlanId                                          */
/*                 UINT1 - *pu1Ports                                         */
/* Output(s)     : None                                                      */
/* Return(s)     : OFC_SUCCESS or OFC_FAILURE                                */
/*****************************************************************************/

INT4
OfcValidatePortMappingToOfcVlan (UINT1 *pu1Ports, UINT4 u4ContextId,
                                 UINT4 u4VlanId)
{
    tCfaIfInfo          cfaifInfo;
    tOfcFsofcIfEntry   *pOfcFsofcIfEntry = NULL;
    UINT2               u2Port = OFC_ZERO;
    UINT2               u2ByteInd = OFC_ZERO;
    UINT2               u2BitIndex = OFC_ZERO;
    UINT1               u1PortFlag = OFC_ZERO;

    MEMSET (&cfaifInfo, OFC_ZERO, sizeof (tCfaIfInfo));
    /* Retrieving the port given in cli */
    for (u2ByteInd = OFC_ZERO; u2ByteInd < OFC_VLAN_PORTS_LEN; u2ByteInd++)
    {
        if (pu1Ports[u2ByteInd] == OFC_ZERO)
        {
            continue;
        }

        u1PortFlag = pu1Ports[u2ByteInd];

        for (u2BitIndex = OFC_ZERO;
             ((u2BitIndex < OFC_VLAN_PORTS_PER_BYTE)
              && (u1PortFlag != OFC_ZERO)); u2BitIndex++)
        {
            if ((u1PortFlag & OFC_VLAN_BIT8) == OFC_ZERO)
            {
                u1PortFlag = (UINT1) (u1PortFlag << OFC_ONE);
                continue;
            }

            u2Port =
                (UINT2) ((u2ByteInd * OFC_VLAN_PORTS_PER_BYTE) + u2BitIndex +
                         OFC_ONE);

            if (u2Port >= OFC_VLAN_MAX_PORTS + OFC_ONE)
            {
                break;
            }

            if (CfaGetIfInfo ((UINT4) u2Port, &cfaifInfo) == CFA_FAILURE)
            {
                return OFC_FAILURE;
            }

            /* Port specified is not an openflow port, so return FAILURE */
            if (cfaifInfo.u1IfSubType != CFA_SUBTYPE_OPENFLOW_INTERFACE)
            {
                return OFC_FAILURE;
            }
            pOfcFsofcIfEntry =
                RBTreeGetFirst (gOfcGlobals.OfcGlbMib.FsofcIfTable);

            /* The port added is not an openflow port so return FAILURE */
            if (pOfcFsofcIfEntry == NULL)
            {
                return OFC_FAILURE;
            }

            do
            {
                /* Do the search only for the openflow ports */
                if (pOfcFsofcIfEntry->MibObject.i4FsofcIfType == CFA_L2VLAN)
                {
                    continue;
                }

                /* If the Port is not mapped to any other vlan and if it's context-id 
                 * is same as vlan return SUCCESS, else return FAILURE
                 */
                if ((INT4) u2Port == pOfcFsofcIfEntry->MibObject.i4FsofcIfIndex)
                {
                    if ((pOfcFsofcIfEntry->u4VlanId == OFC_ZERO) &&
                        (pOfcFsofcIfEntry->MibObject.u4FsofcContextId ==
                         u4ContextId))
                    {
                        return OFC_SUCCESS;
                    }
                    else if ((pOfcFsofcIfEntry->u4VlanId != OFC_ZERO) &&
                             (pOfcFsofcIfEntry->u4VlanId == u4VlanId))
                    {
                        return OFC_SUCCESS;
                    }
                }
            }
            while ((pOfcFsofcIfEntry =
                    RBTreeGetNext (gOfcGlobals.OfcGlbMib.FsofcIfTable,
                                   pOfcFsofcIfEntry, NULL)) != NULL);
        }
    }
    return OFC_FAILURE;
}

/*****************************************************************************/
/* Function Name : OfcHandleVlanPkt                                          */
/* Description   : This routine validates the vlan-id and give the If Index  */
/*                 of the Vlan                                               */
/* Input(s)      : UINT4 - u4VlanId                                          */
/*                 UINT1 - *pu4VlanIfIndex                                   */
/* Output(s)     : IfIndex of OFC VLAN                                       */
/* Return(s)     : OSIX_SUCCESS or OSIX_FAILURE                              */
/*****************************************************************************/
INT4
OfcHandleVlanPkt (tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 *pu4VlanIfIndex)
{
    UINT1              *pu1Alias = NULL;
    INT4                i4Result = OSIX_FAILURE;
    UINT2               u2VlanId = OFC_ZERO;
    UINT2               u2EthType = OFC_ZERO;
    UINT1               au1IfName[CFA_MAX_IFALIAS_LENGTH];

    CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &u2EthType, VLAN_TAG_OFFSET,
                               OFC_TWO);
    u2EthType = OSIX_HTONS (u2EthType);

    if ((u2EthType != VLAN_PROTOCOL_ID) &&
        (u2EthType != VLAN_PROVIDER_PROTOCOL_ID))
    {
        return i4Result;
    }

    CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &u2VlanId,
                               VLAN_TAG_VLANID_OFFSET, OFC_TWO);
    u2VlanId = OSIX_HTONS (u2VlanId);

    pu1Alias = au1IfName;
    STRCPY (au1IfName, OPENFLOW_VLAN_ALIAS_PREFIX);
    SPRINTF ((CHR1 *) pu1Alias, "%s%u", au1IfName, (UINT4) u2VlanId);

    /*
     * Get the VLAN IF Index from Alias 
     */
    if (OfcGetInterfaceIndexFromName (pu1Alias, pu4VlanIfIndex) == OSIX_FAILURE)
    {
        return i4Result;
    }

    /*
     * Check whether the context associated with the IF Index
     * is in NOT connected state and in fail stand alone mode
     */
    i4Result = OfcHandleOpenflowSwModeProcess (pu4VlanIfIndex, NULL);
    return i4Result;
}

/*****************************************************************************/
/* Function Name : OfcGetInterfaceIndexFromName                              */
/* Description   : This routine returns the vlan index based on the alias    */
/* Input(s)      : UINT1 - *pu1Alias                                         */
/*                 UINT4 - *pu4IfIndex                                       */
/* Output(s)     : *pu4IfIndex                                               */
/* Return(s)     : OSIX_SUCCESS or OSIX_FAILURE                                */
/*****************************************************************************/

INT4
OfcGetInterfaceIndexFromName (UINT1 *pu1Alias, UINT4 *pu4IfIndex)
{
    tOfcFsofcIfEntry   *pOfcFsofcIfEntry = NULL;
    pOfcFsofcIfEntry = RBTreeGetFirst (gOfcGlobals.OfcGlbMib.FsofcIfTable);

    if (pOfcFsofcIfEntry == NULL)
    {
        return OSIX_FAILURE;
    }
    do
    {
        /* Search through the OFC IF database to get the IfIndex associated with the 
         * Alias name
         */

        if (STRNCMP (pu1Alias, pOfcFsofcIfEntry->MibObject.au1FsofcIfAlias,
                     STRLEN (pu1Alias)) == OFC_ZERO)
        {
            *pu4IfIndex = (UINT4) pOfcFsofcIfEntry->MibObject.i4FsofcIfIndex;
            return OSIX_SUCCESS;
        }
    }
    while ((pOfcFsofcIfEntry =
            RBTreeGetNext (gOfcGlobals.OfcGlbMib.FsofcIfTable, pOfcFsofcIfEntry,
                           NULL)) != NULL);
    return OSIX_FAILURE;
}

/*****************************************************************************/
/* Function Name : OfcCheckOfcVlanInterfaceMapping                           */
/* Description   : This routine validates the ports to be added to the       */
/*                 openflow vlan                                             */
/* Input(s)      : tOfcFsofcIfEntry - *pOfcSetFsofcIfEntry                   */
/* Output(s)     : None                                                      */
/* Return(s)     : OFC_SUCCESS or OFC_FAILURE                                */
/*****************************************************************************/

INT4
OfcCheckOfcVlanInterfaceMapping (tOfcFsofcIfEntry * pOfcSetFsofcIfEntry)
{
    UINT4               u4VlanId = OFC_ZERO;
    UINT1               u1TempPrefix[OFC_SIX];
    tOfcFsofcIfEntry   *pOfcFsofcIfEntry = NULL;

    MEMSET (u1TempPrefix, OFC_ZERO, OFC_SIX);

    /* Get the IFEntry details to find the vlanid from the IfIndex */
    pOfcSetFsofcIfEntry = RBTreeGet (gOfcGlobals.OfcGlbMib.FsofcIfTable,
                                     (tRBElem *) pOfcSetFsofcIfEntry);

    if (pOfcSetFsofcIfEntry == NULL)
    {
        return OFC_SUCCESS;
    }

    /* Get the VlanId from the Alias */
    SSCANF ((CHR1 *) pOfcSetFsofcIfEntry->MibObject.au1FsofcIfAlias, "%6s%d",
            u1TempPrefix, &u4VlanId);

    UNUSED_PARAM (u1TempPrefix);

    pOfcFsofcIfEntry = RBTreeGetFirst (gOfcGlobals.OfcGlbMib.FsofcIfTable);
    if (pOfcFsofcIfEntry == NULL)
    {
        return OFC_SUCCESS;
    }

    do
    {
        /* Do the search only for the openflow ports */
        if (pOfcFsofcIfEntry->MibObject.i4FsofcIfType == CFA_L2VLAN)
        {
            continue;
        }

        /* 
         * If the Port is mapped to vlan the vlan-id of the port will be same as the vlan passed 
         * return FAILURE, else return SUCCESS
         */
        if (u4VlanId == pOfcFsofcIfEntry->u4VlanId)
        {
            return OFC_FAILURE;
        }
    }
    while ((pOfcFsofcIfEntry =
            RBTreeGetNext (gOfcGlobals.OfcGlbMib.FsofcIfTable, pOfcFsofcIfEntry,
                           NULL)) != NULL);
    return OFC_SUCCESS;
}

#if defined (NPAPI_WANTED) && defined (OPENFLOW_TCAM)
#ifdef DPA_WANTED

/*****************************************************************************/
/* Function Name : OfcGetLocalTableId                                        */
/* Description   : This routine gives the local Table Id based on DPA        */
/*                 TableId                                                   */
/* Input(s)      : u1DpaTabId - DPA Table ID                                 */
/*                 pu1LocalTabId - Ofc Table ID                              */
/* Output(s)     : *pu1LocalTabId - Local Table ID                           */
/* Return(s)     : OFC_SUCCESS or OFC_FAILURE                                */
/*****************************************************************************/
UINT1
OfcGetLocalTableId (UINT1 u1DpaTabId, UINT1 *pu1LocalTabId)
{
    UINT1               u1Iter = OFC_ZERO;

    for (u1Iter = OFC_ZERO; u1Iter < OFCDPA_MAX_TABLES; u1Iter++)
    {
        if (u1DpaTabId == gau1MapTable[u1Iter])
        {
            *pu1LocalTabId = u1Iter;
            return OFC_SUCCESS;
        }
    }

    return OFC_FAILURE;
}

/*****************************************************************************/
/* Function Name : OfcGetDpaTableId                                          */
/* Description   : This routine uses the local Table Id to give DPA          */
/*                 TableId                                                   */
/* Input(s)      : u1DpaTabId - DPA Table ID                                 */
/*                 u1LocalTabId - Ofc Table ID                               */
/* Output(s)     : *pu1DpaTabId - DPA Table ID                               */
/* Return(s)     : OFC_SUCCESS or OFC_FAILURE                                */
/*****************************************************************************/
UINT1
OfcGetDpaTableId (UINT1 u1LocalTabId, UINT1 *pu1DpaTabId)
{
    if (u1LocalTabId >= OFCDPA_MAX_TABLES)
    {
        return OFC_FAILURE;
    }
    else
    {
        *pu1DpaTabId = gau1MapTable[u1LocalTabId];
    }

    return OFC_SUCCESS;
}

#endif /* DPA_WANTED */
#endif /* NPAPI_WANTED && OPENFLOW_TCAM */
