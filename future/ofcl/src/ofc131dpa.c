/*****************************************************************************
 * Copyright (C) 2014 Aricent Inc . All Rights Reserved
 *
 * $Id: ofc131dpa.c,v 1.2 2014/09/09 10:08:39 siva Exp $
 *
 * Description: This file contains routines for DPA processing 
 *****************************************************************************/
#include "ofcapi.h"
#include "ofcacts.h"
#include "fssocket.h"
#include "ofcsz.h"
#include "ofcintf.h"
#include "ofc131dpa.h"

/*****************************************************************************
 * Function Name : Ofc131DpaFillPortDesc 
 * Description   : This routine fills the Port Description details by calling
 *                 DPA APIs 
 * Input         : u4Port    - Port
 * Output        : pPortDesc - Pointer to Port Description
 * Return        : OFC_FAILURE/OFC_SUCCESS
 *****************************************************************************/
UINT1
Ofc131DpaFillPortDesc (UINT4 u4Port, tOfp131PortDesc * pPortDesc)
{
    ofdpa_buffdesc      BufferDesc;
    ofdpaPortFeature_t  PortFeature;
    OFDPA_PORT_CONFIG_t eConfig;
    OFDPA_PORT_STATE_t  eState;

    MEMSET (&BufferDesc, OFC_ZERO, sizeof (ofdpa_buffdesc));
    MEMSET (&PortFeature, OFC_ZERO, sizeof (ofdpaPortFeature_t));

    ofdpaPortMacGet (u4Port, (ofdpaMacAddr_t *) pPortDesc->au1HwAddr);

    BufferDesc.size = OFC_SIXTEEN;
    BufferDesc.pstart = (char *) pPortDesc->au1Name;
    ofdpaPortNameGet (u4Port, &BufferDesc);

    if (ofdpaPortConfigGet (u4Port, &eConfig) == OFDPA_E_NONE)
    {
        pPortDesc->u4Config = eConfig;
    }

    if (ofdpaPortStateGet (u4Port, &eState) == OFDPA_E_NONE)
    {
        pPortDesc->u4State = eState;
    }

    if (ofdpaPortFeatureGet (u4Port, &PortFeature) == OFDPA_E_NONE)
    {
        pPortDesc->u4Curr = PortFeature.curr;
        pPortDesc->u4Advertised = PortFeature.advertised;
        pPortDesc->u4Supported = PortFeature.supported;
        pPortDesc->u4Peer = PortFeature.peer;
    }

    ofdpaPortCurrSpeedGet (u4Port, &pPortDesc->u4CurrSpeed);
    ofdpaPortMaxSpeedGet (u4Port, &pPortDesc->u4MaxSpeed);

    return OFC_SUCCESS;
}

/*****************************************************************************
 * Function Name : Ofc131DpaPortDescGet 
 * Description   : This routine fetches Port Description for all the ports
 * Input         : pPortDescReply - Pointer to Port Description
 * Output        : pu4VarLen      - Pointer to length of message filled 
 * Return        : OFC_FAILURE/OFC_SUCCESS
 *****************************************************************************/
UINT1
Ofc131DpaPortDescGet (tOfcPortDescReply * pPortDescReply, UINT4 *pu4VarLen)
{
    UINT4               u4Port = OFC_ONE;

    do
    {
        if (OFC_MAX_MULTIPART_MSG_SIZE <
            (*pu4VarLen + sizeof (tOfcPortDescReply)))
        {
            break;
        }

        Ofc131DpaFillPortDesc (u4Port, (tOfp131PortDesc *) pPortDescReply);
        *pu4VarLen += sizeof (tOfcPortDescReply);
        pPortDescReply++;

    }
    while (ofdpaPortNextGet (u4Port, &u4Port) == OFDPA_E_NONE);

    return OFC_SUCCESS;
}

/*****************************************************************************
 * Function Name : Ofc131DpaPortUpdate 
 * Description   : This routine updates the port admin state and advertised 
 *                 features for the given port
 * Input         : u4Port    - Port to be updated
 *                 u1State   - Port State to be updated
 *                 u4AdvFeat - Advertise features to be updated
 * Output        : NONE
 * Return        : OFC_FAILURE/OFC_SUCCESS 
 *****************************************************************************/
UINT1
Ofc131DpaPortUpdate (UINT4 u4Port, UINT1 u1State, UINT4 u4AdvFeat)
{
    OFDPA_ERROR_t       eErr = OFDPA_E_NONE;

    eErr = ofdpaPortConfigSet (u4Port, u1State);
    if (eErr != OFDPA_E_NONE)
    {
        OFC_TRC_FUNC ((OFC_DPA_TRC, "Failed to set admin state"
                       "for port - %d, with error - %d\n", u4Port, eErr));
        return OFC_FAILURE;
    }

    eErr = ofdpaPortAdvertiseFeatureSet (u4Port, u4AdvFeat);
    if (eErr != OFDPA_E_NONE)
    {
        OFC_TRC_FUNC ((OFC_DPA_TRC, "Failed to set advertise features"
                       "for port - %d, with error - %d\n", u4Port, eErr));
        return OFC_FAILURE;
    }

    return OFC_SUCCESS;
}

/*****************************************************************************
 * Function Name : Ofc131DpaPortStatsGet 
 * Description   : This routine fetches the port stats to be sent as part
 *                 of multipart reply
 * Input         : u4Port          - Port for which stats is requested
 * Output        : pPortStatsReply - Pointer to port stats reply
 *                 u4VarLen        - Variable length of message filled
 * Return        : OFC_FAILURE/OFC_SUCCESS 
 *****************************************************************************/
UINT1
Ofc131DpaPortStatsGet (UINT4 u4Port, tOfcPortStatsReply * pPortStatsReply,
                       UINT4 *pu4VarLen)
{
    ofdpaPortStats_t    DpaPortStats;
    UINT4               u4TempPort = OFC_ONE;

    if (u4Port != OFPP_ANY)
    {
        u4TempPort = u4Port;
    }

    do
    {
        MEMSET (&DpaPortStats, OFC_ZERO, sizeof (ofdpaPortStats_t));

        if (OFC_MAX_MULTIPART_MSG_SIZE <
            (*pu4VarLen + sizeof (tOfcPortStatsReply)))
        {
            break;
        }

        if (ofdpaPortStatsGet (u4TempPort, &DpaPortStats) != OFDPA_E_NONE)
        {
            if (u4Port != OFPP_ANY)
            {
                break;
            }
            /* continue filling for other ports */
            continue;
        }

        /* received packets */
        MEMCPY (&pPortStatsReply->u8RxPkts, &DpaPortStats.rx_packets,
                OFC_EIGHT);
        /* transmitted packets */
        MEMCPY (&pPortStatsReply->u8TxPkts, &DpaPortStats.tx_packets,
                OFC_EIGHT);

        /* received bytes */
        MEMCPY (&pPortStatsReply->u8RxBytes, &DpaPortStats.rx_bytes, OFC_EIGHT);
        /* transmitted bytes */
        MEMCPY (&pPortStatsReply->u8TxBytes, &DpaPortStats.tx_bytes, OFC_EIGHT);

        /* received packets dropped */
        MEMCPY (&pPortStatsReply->u8RxDropped,
                &DpaPortStats.rx_drops, OFC_EIGHT);
        /* trasmit packets dropped */
        MEMCPY (&pPortStatsReply->u8TxDropped,
                &DpaPortStats.tx_drops, OFC_EIGHT);

        /* receive errors */
        MEMCPY (&pPortStatsReply->u8RxErrors,
                &DpaPortStats.rx_errors, OFC_EIGHT);
        /* transmit errors */
        MEMCPY (&pPortStatsReply->u8TxErrors,
                &DpaPortStats.tx_errors, OFC_EIGHT);

        /* receive frame alignment errors */
        MEMCPY (&pPortStatsReply->u8RxFrameErr,
                &DpaPortStats.rx_frame_err, OFC_EIGHT);
        /* receive overrun errors */
        MEMCPY (&pPortStatsReply->u8RxOverErr,
                &DpaPortStats.rx_over_err, OFC_EIGHT);
        /* receive CRC errors */
        MEMCPY (&pPortStatsReply->u8RxCrcErr,
                &DpaPortStats.rx_crc_err, OFC_EIGHT);

        /* transmit collisions */
        MEMCPY (&pPortStatsReply->u8Collisions,
                &DpaPortStats.collisions, OFC_EIGHT);

        /* duration */
        pPortStatsReply->u4DurSec = DpaPortStats.duration_seconds;
        pPortStatsReply->u4DurationNSec = OFC_ZERO;

        *pu4VarLen += sizeof (tOfcPortStatsReply);
        if (u4Port != OFPP_ANY)
        {
            break;
        }
        pPortStatsReply++;
    }
    while (ofdpaPortNextGet (u4TempPort, &u4TempPort) == OFDPA_E_NONE);

    return OFC_SUCCESS;
}

/*****************************************************************************
 * Function Name : Ofc131DpaQueueConfigGet 
 * Description   : This routine fetches queue configuration for the specified
 *                 or all the ports
 * Input         : u4Port    - Port
 * Output        : pQueReply - Pointer to Queue config reply
 *                 pu4VarLen - Pointer to length of message filled 
 * Return        : OFC_FAILURE/OFC_SUCCESS
 *****************************************************************************/
UINT1
Ofc131DpaQueueConfigGet (UINT4 u4Port, tOfp131QueueRply * pQueReply,
                         UINT4 *pu4VarLen)
{
    tOfp131PktQueue    *pPktQue = NULL;
    tOfp131QueueHdr    *pQueHdr = NULL;
    tOfp131QueueProp   *pQueProp = NULL;
    UINT4               u4TempPort = OFC_ONE;
    UINT4               u4NumQues = OFC_ZERO;
    UINT4               u4QueIter = OFC_ZERO;
    UINT4               u4MinRate = OFC_ZERO;
    UINT4               u4MaxRate = OFC_ZERO;
    OFDPA_ERROR_t       eErr = OFDPA_E_NONE;

    if (u4Port != OFPP_ANY)
    {
        u4TempPort = u4Port;
    }

    do
    {
        eErr = ofdpaNumQueuesGet (u4TempPort, &u4NumQues);
        if (eErr != OFDPA_E_NONE)
        {
            OFC_TRC_FUNC ((OFC_DPA_TRC, "Failed to get number of queues for "
                           "port - %d, with error - %d\n", u4TempPort, eErr));
            return OFC_FAILURE;
        }

        if (OFC_MAX_MULTIPART_MSG_SIZE < (*pu4VarLen +
                                          sizeof (tOfp131QueueRply)))
        {
            break;
        }
        *pu4VarLen += sizeof (tOfp131QueueRply);

        pQueReply->u4Port = u4TempPort;
        pPktQue = (tOfp131PktQueue *) ((VOID *)
                                       (((UINT1 *) pQueReply) +
                                        sizeof (tOfp131QueueRply)));
        for (u4QueIter = OFC_ZERO; u4QueIter < u4NumQues; u4QueIter++)
        {
            if (OFC_MAX_MULTIPART_MSG_SIZE < (*pu4VarLen +
                                              OFC131_QUEUE_DESCRIPTION_SIZE))
            {
                break;
            }

            /* Fill in Queue Information */
            pPktQue->u4QueueId = OSIX_HTONL (u4QueIter);
            pPktQue->u4Port = OSIX_HTONL (u4TempPort);
            pPktQue->u2Len = OSIX_HTONS (OFC131_QUEUE_DESCRIPTION_SIZE);

            eErr = ofdpaQueueRateGet (u4TempPort, u4QueIter, &u4MinRate,
                                      &u4MaxRate);
            if (eErr != OFDPA_E_NONE)
            {
                OFC_TRC_FUNC ((OFC_DPA_TRC, "Failed to get rates for queue - %d"
                               "in port - %d, with error - %d\n",
                               u4QueIter, u4TempPort, eErr));
                return OFC_FAILURE;
            }

            /* Fill in the Queue Properties - MIN RATE */
            pQueHdr = (tOfp131QueueHdr *) ((VOID *)
                                           (((UINT1 *) pPktQue) +
                                            sizeof (tOfp131PktQueue)));
            pQueHdr->u2Property = OSIX_HTONS (OFCQT_MIN_RATE);
            pQueHdr->u2Len = OSIX_HTONS (OFC131_QUEUE_PROPERTY_SIZE);

            /* Fill in the Min-Rate */
            pQueProp = (tOfp131QueueProp *) ((VOID *)
                                             (((UINT1 *) pQueHdr) +
                                              sizeof (tOfp131QueueHdr)));
            pQueProp->u2Rate = OSIX_HTONS ((UINT2) u4MinRate);

            /* Fill in the Queue Properties - MAX RATE */
            pQueHdr = (tOfp131QueueHdr *) ((VOID *)
                                           (((UINT1 *) pQueProp) +
                                            OFC131_QUEUE_PROPERTY_SIZE));
            pQueHdr->u2Property = OSIX_HTONS (OFCQT_MAX_RATE);
            pQueHdr->u2Len = OSIX_HTONS (OFC131_QUEUE_PROPERTY_SIZE);

            /* Fill in the Max-Rate */
            pQueProp = (tOfp131QueueProp *) ((VOID *)
                                             (((UINT1 *) pQueHdr) +
                                              sizeof (tOfp131QueueHdr)));
            pQueProp->u2Rate = OSIX_HTONS ((UINT2) u4MinRate);

            *pu4VarLen +=
                sizeof (tOfp131QueueRply) + OFC131_QUEUE_DESCRIPTION_SIZE;
            pQueReply =
                (tOfp131QueueRply
                 *) ((VOID *) (((UINT1 *) pQueReply) + *pu4VarLen));

        }

        if (u4Port != OFPP_ANY)
        {
            break;
        }
    }
    while (ofdpaPortNextGet (u4TempPort, &u4TempPort) == OFDPA_E_NONE);

    return OFC_SUCCESS;
}

/*****************************************************************************
 * Function Name : Ofc131DpaQueueStatsGet 
 * Description   : This routine fetches the queue stats to be sent as part
 *                 of multipart reply
 * Input         : u4Port         - Port for which stats is requested
 *                 u4QueueId      - Queue for which stats is requested
 * Output        : pQueStatsReply - Pointer to queue stats reply
 *                 u4VarLen       - Variable length of message filled
 * Return        : OFC_FAILURE/OFC_SUCCESS 
 *****************************************************************************/
UINT1
Ofc131DpaQueueStatsGet (UINT4 u4Port, UINT4 u4QueueId,
                        tOfcQueStatsReply * pQueStatsReply, UINT4 *pu4VarLen)
{
    ofdpaPortQueueStats_t DpaQueStats;
    UINT4               u4TempPort = OFC_ONE;
    UINT4               u4NumQues = OFC_ZERO;
    UINT4               u4QueIter = OFC_ZERO;
    OFDPA_ERROR_t       eErr = OFDPA_E_NONE;

    if (u4Port != OFPP_ANY)
    {
        u4TempPort = u4Port;
    }

    do
    {
        MEMSET (&DpaQueStats, OFC_ZERO, sizeof (ofdpaPortQueueStats_t));

        if (OFC_MAX_MULTIPART_MSG_SIZE <
            (*pu4VarLen + sizeof (tOfcQueStatsReply)))
        {
            break;
        }

        eErr = ofdpaNumQueuesGet (u4TempPort, &u4NumQues);
        if (eErr != OFDPA_E_NONE)
        {
            OFC_TRC_FUNC ((OFC_DPA_TRC, "Failed to get number of queues for "
                           "port - %d, with error - %d\n", u4TempPort, eErr));
            return OFC_FAILURE;
        }

        for (u4QueIter = OFC_ZERO; u4QueIter < u4NumQues; u4QueIter++)
        {
            if (OFC_MAX_MULTIPART_MSG_SIZE <
                (*pu4VarLen + sizeof (tOfcQueStatsReply)))
            {
                break;
            }

            if (u4QueueId != OFPQ_ALL)
            {
                /* requested for specific queue */
                u4QueIter = u4QueueId;
            }

            eErr = ofdpaQueueStatsGet (u4TempPort, u4QueIter, &DpaQueStats);
            if (eErr != OFDPA_E_NONE)
            {
                OFC_TRC_FUNC ((OFC_DPA_TRC, "Failed to queues stats for "
                               "queue - %d in port - %d, with error - %d\n",
                               u4QueIter, u4TempPort, eErr));
                return OFC_FAILURE;
            }

            /* fill the stats for this queue */
            pQueStatsReply->u4PortNum = u4TempPort;
            pQueStatsReply->u4QueId = u4QueIter;

            MEMCPY (&pQueStatsReply->u8TxBytes, &DpaQueStats.txBytes,
                    OFC_EIGHT);
            MEMCPY (&pQueStatsReply->u8TxPkts, &DpaQueStats.txPkts, OFC_EIGHT);

            pQueStatsReply->u4DurSec = DpaQueStats.duration_seconds;

            *pu4VarLen += sizeof (pQueStatsReply);
            pQueStatsReply++;
            if (u4QueueId != OFPQ_ALL)
            {
                /* requested for specific queue */
                break;
            }
        }

        if (u4Port != OFPP_ANY)
        {
            /* request for only one port */
            break;
        }
    }
    while (ofdpaPortNextGet (u4TempPort, &u4TempPort) == OFDPA_E_NONE);
    return OFC_SUCCESS;
}

/*****************************************************************************
 * Function Name : Ofc131DpaFlowEventProcess 
 * Description   : This routine processes the Flow Events - Timer expiry
 *                 events from the DPA
 * Input         : NONE 
 * Output        : NONE
 * Return        : NONE
 *****************************************************************************/
PRIVATE VOID
Ofc131DpaFlowEventProcess (VOID)
{
    ofdpaFlowEvent_t    FlowEvent;
    UINT1               u1Iter = OFC_ZERO;

    MEMSET (&FlowEvent, OFC_ZERO, sizeof (ofdpaFlowEvent_t));

    /* scan through all the tables  */
    u1Iter = OFDPA_FLOW_TABLE_ID_INGRESS_PORT;
    while (u1Iter <= OFDPA_FLOW_TABLE_ID_ACL_POLICY)
    {
        FlowEvent.flowMatch.tableId = u1Iter;
        while (ofdpaFlowEventNextGet (&FlowEvent) == OFDPA_E_NONE)
        {
            if ((FlowEvent.eventMask & OFDPA_FLOW_EVENT_HARD_TIMEOUT) ||
                (FlowEvent.eventMask & OFDPA_FLOW_EVENT_IDLE_TIMEOUT))
            {
                ofdpaFlowByCookieDelete (FlowEvent.flowMatch.cookie);
            }
        }
        /* goto next table */
        u1Iter += OFC_TEN;
    }
}

/*****************************************************************************
 * Function Name : Ofc131DpaPortEventProcess 
 * Description   : This routine processes the Port Events - creation &
 *                 deletions and status change events from the DPA
 * Input         : NONE 
 * Output        : NONE
 * Return        : NONE
 *****************************************************************************/
PRIVATE VOID
Ofc131DpaPortEventProcess (VOID)
{
    tOfcFsofcIfEntry    IfEntry;
    ofdpaPortEvent_t    PortEvent;
    tOfcFsofcIfEntry   *pIfEntry = NULL;
    tOfp131PortStatus  *pPortStatus = NULL;
    UINT4               u4Port = OFC_ZERO;
    UINT4               u4Type = OFC_ZERO;
    UINT4               u1Reason = OFC_ZERO;
    UINT1               u1OperStatus = OFC_ZERO;
    OFDPA_PORT_CONFIG_t eConfig = OFC_ZERO;

    MEMSET (&IfEntry, OFC_ZERO, sizeof (tOfcFsofcIfEntry));
    MEMSET (&PortEvent, OFC_ZERO, sizeof (ofdpaPortEvent_t));

    while (ofdpaPortEventNextGet (&PortEvent) == OFDPA_E_NONE)
    {
        u4Port = PortEvent.portNum;
        if (PortEvent.eventMask & OFDPA_EVENT_PORT_CREATE)
        {
            /* port added to openflow */
            u1Reason = OFPPR_ADD;

            MEMSET (&IfEntry, OFC_ZERO, sizeof (tOfcFsofcIfEntry));
            IfEntry.MibObject.i4FsofcIfIndex = (INT4) u4Port;

            ofdpaPortTypeGet (u4Port, &u4Type);
            IfEntry.MibObject.i4FsofcIfType = (INT4) u4Type;

            if (ofdpaPortConfigGet (u4Port, &eConfig) != OFDPA_E_NONE)
            {
                OFC_TRC_FUNC ((OFC_DPA_TRC, "Failed to get Port Configuration"
                               "for port - %d\n", u4Port));
                return;
            }
            IfEntry.MibObject.i4FsofcIfOperStatus =
                ((eConfig ==
                  OFDPA_PORT_STATE_LINK_DOWN) ? CFA_IF_DOWN : CFA_IF_UP);

            IfEntry.MibObject.u4FsofcContextId = OFC_DEFAULT_CONTEXT;

            pIfEntry = OfcFsofcIfTableCreateApi (&IfEntry);
            if (pIfEntry == NULL)
            {
                OFC_TRC_FUNC ((OFC_DPA_TRC,
                               "Interface entry creation failed\n"));
                return;
            }
        }
        else if (PortEvent.eventMask & OFDPA_EVENT_PORT_DELETE)
        {
            /* port deleted from openflow */
            u1Reason = OFPPR_DELETE;

            /* Delete the port information */
            MEMSET (&IfEntry, OFC_ZERO, sizeof (tOfcFsofcIfEntry));
            IfEntry.MibObject.i4FsofcIfIndex = (INT4) u4Port;
            IfEntry.MibObject.u4FsofcContextId = OFC_DEFAULT_CONTEXT;

            pIfEntry = RBTreeGet (gOfcGlobals.OfcGlbMib.FsofcIfTable,
                                  (tRBElem *) & IfEntry);
            if (pIfEntry != NULL)
            {
                RBTreeRem (gOfcGlobals.OfcGlbMib.FsofcIfTable, pIfEntry);
                MemReleaseMemBlock (OFC_FSOFCIFTABLE_POOLID,
                                    (UINT1 *) pIfEntry);
            }
            else
            {
                /* no port exists */
            }
        }
        else if (PortEvent.eventMask & OFDPA_EVENT_PORT_STATE)
        {
            u1Reason = OFPPR_DELETE;
            if (PortEvent.state == OFDPA_PORT_STATE_LINK_DOWN)
            {
                /* port went down */
                u1OperStatus = (UINT1) CFA_IF_DOWN;
            }
            else
            {
                /* port came up */
                u1OperStatus = (UINT1) CFA_IF_UP;
            }
        }
        else
        {
            /* unhandled event */
            continue;
        }

        /* send the port status message */
        pPortStatus =
            (tOfp131PortStatus *) MemAllocMemBlk (OFC_PORT_STATUS_POOLID);
        if (pPortStatus == NULL)
        {
            OFC_TRC_FUNC ((OFC_DPA_TRC, "Memory allocation failed,"
                           "unable to send port status message\n"));
            return;
        }
        MEMSET (pPortStatus, OFC_ZERO, sizeof (tOfp131PortStatus));

        pPortStatus->u1Reason = u1Reason;
        pPortStatus->PortDesc.u4PortNo = u4Port;

        /* fill in the port description */
        if (Ofc131DpaFillPortDesc (u4Port, &pPortStatus->PortDesc)
            != OFC_SUCCESS)
        {
            OFC_TRC_FUNC ((OFC_DPA_TRC, "Error in filling Port Description"
                           "for port - %d\n", u4Port));
            MemReleaseMemBlock (OFC_PORT_STATUS_POOLID, (UINT1 *) pPortStatus);
            return;
        }

        /* send the port status */
        if (OfcInterfacePortStatusEventSend (pPortStatus, OFC_DEFAULT_CONTEXT)
            != OFC_SUCCESS)
        {
            OFC_TRC_FUNC ((OFC_DPA_TRC, "Unable to send port status message"
                           "for port - %d\n", u4Port));
            MemReleaseMemBlock (OFC_PORT_STATUS_POOLID, (UINT1 *) pPortStatus);
            return;
        }
    }
}

/*****************************************************************************
 * Function Name : Ofc131DpaEventProcess 
 * Description   : This routine processes the Port Events - creation &
 *                 deletions and status change events from the DPA
 * Input         : NONE 
 * Output        : NONE
 * Return        : NONE
 *****************************************************************************/
VOID
Ofc131DpaEventProcess (VOID)
{
    struct timeval      TimeOut;

    TimeOut.tv_sec = OFC_ZERO;
    TimeOut.tv_usec = OFC_ZERO;

    while ((ofdpaEventReceive (&TimeOut)) == OFDPA_E_NONE)
    {
        /* Process the Flow events */
        Ofc131DpaFlowEventProcess ();

        /* Process the Port events */
        Ofc131DpaPortEventProcess ();
    }
}

/*****************************************************************************
 * Function Name : Ofc131DpaPktProcess 
 * Description   : This routine processes the data packets received by DPA
 *                 and forwards them to controller as Packet-In
 * Input         : NONE 
 * Output        : NONE
 * Return        : NONE
 *****************************************************************************/
VOID
Ofc131DpaPktProcess (VOID)
{
    ofdpaPacket_t       DataPkt;
    struct timeval      TimeOut;
    tCRU_BUF_CHAIN_HEADER *pBuf = NULL;
    tOfp131Match       *pOfcMatch = NULL;
    tOfcFsofcControllerConnEntry *pCntlrEntry = NULL;
    CHR1               *pc1DpaPkt = NULL;
    FS_UINT8            u8Cookie;
    UINT4               u4DataPktSize = OFC_ZERO;

    MEMSET (&DataPkt, OFC_ZERO, sizeof (ofdpaPacket_t));
    TimeOut.tv_sec = OFC_ZERO;
    TimeOut.tv_usec = OFC_ZERO;
    u8Cookie.u4Lo = OFC_UINT4_ALL_MASK;
    u8Cookie.u4Hi = OFC_UINT4_ALL_MASK;

    pc1DpaPkt = (CHR1 *) MemAllocMemBlk (OFC_DPA_PKT_POOLID);
    if (pc1DpaPkt == NULL)
    {
        OFC_TRC_FUNC ((OFC_DPA_TRC,
                       "Memory allocation failed in pkt process\n"));
        return;
    }
    MEMSET (pc1DpaPkt, OFC_ZERO, OFC_DPA_MAX_PKT_SIZE);

    DataPkt.pktData.pstart = pc1DpaPkt;
    while (ofdpaPktReceive (&TimeOut, &DataPkt) == OFDPA_E_NONE)
    {
        u4DataPktSize = DataPkt.pktData.size;

        /* allocate the CRU */
        if ((pBuf = CRU_BUF_Allocate_MsgBufChain (u4DataPktSize, OFC_ZERO))
            == NULL)
        {
            OFC_TRC_FUNC ((OFC_DPA_TRC, "CRU Buffer allocation failed,"
                           "for processing the data packet received\n"));
            MemReleaseMemBlock (OFC_DPA_PKT_POOLID, (UINT1 *) pc1DpaPkt);
            return;
        }

        /* copy the received data packet to the Cru */
        if (CRU_BUF_Copy_OverBufChain
            (pBuf, (UINT1 *) DataPkt.pktData.pstart, 0,
             u4DataPktSize) == CRU_FAILURE)
        {
            CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
            continue;
        }

        /* Allocate from Flow Match Mempool */
        pOfcMatch = (tOfp131Match *) MemAllocMemBlk (OFC_FLOW_MATCH_TLV_POOLID);
        if (pOfcMatch == NULL)
        {
            /* MemPool Allocation failed */
            OFC_TRC_FUNC ((OFC_DPA_TRC, "Memory allocation for match failed,"
                           "for processing the data packet received\n"));
            CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
            MemReleaseMemBlock (OFC_DPA_PKT_POOLID, (UINT1 *) pc1DpaPkt);
            return;
        }
        MEMSET (pOfcMatch, OFC_ZERO, OFC_MAX_FLOW_MATCH_TLV_SIZE);

        if (gOfcGlobals.OfcGlbMib.FsofcControllerConnTable != NULL)
        {
            pCntlrEntry = RBTreeGetFirst (gOfcGlobals.OfcGlbMib.
                                          FsofcControllerConnTable);
            if (pCntlrEntry == NULL)
            {
                continue;
            }

            do
            {
                /* send only if the controller wants packet-in */
                if ((pCntlrEntry->au4PktInMask[OFC_ONE] >> OFCR_NO_MATCH)
                    & OFC_ONE)
                {

                    Ofc131ActsSendPktIn (DataPkt.inPortNum, OFC_ZERO,
                                         OFCR_NO_MATCH, u8Cookie,
                                         DataPkt.tableId, pOfcMatch, pBuf,
                                         pCntlrEntry);
                }
                pCntlrEntry = RBTreeGetNext (gOfcGlobals.OfcGlbMib.
                                             FsofcControllerConnTable,
                                             pCntlrEntry, NULL);
            }
            while (pCntlrEntry != NULL);
            MemReleaseMemBlock (OFC_DPA_PKT_POOLID, (UINT1 *) pc1DpaPkt);
            MemReleaseMemBlock (OFC_FLOW_MATCH_TLV_POOLID, (UINT1 *) pOfcMatch);
            CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
        }
    }
}

/*****************************************************************************
 * Function Name : Ofc131DpaPacketSend 
 * Description   : This routine forwards the packet send as part of Packet
 *                 Out message to DPA 
 * Input         : pBuf      - Pointer to CRU Buffer
 *                 u4InPort  - Interface on which this packet is received
 *                 u4OutPort - Interface on which this packet to be send
 *                 u4PktSize - Data Packet Size
 * Output        : NONE
 * Return        : OFC_FAILURE/OFC_SUCCESS 
 *****************************************************************************/
UINT1
Ofc131DpaPacketSend (tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 u4InPort,
                     UINT4 u4OutPort, UINT4 u4PktSize)
{
    ofdpa_buffdesc      DataPkt;
    UINT4              *pu4NextPort = NULL;
    CHR1               *pc1DpaPkt = NULL;
    OFDPA_ERROR_t       eErr = OFDPA_E_NONE;
    UINT4               u4Port = OFC_ZERO;

    MEMSET (&DataPkt, OFC_ZERO, sizeof (ofdpa_buffdesc));

    pc1DpaPkt = (CHR1 *) MemAllocMemBlk (OFC_DPA_PKT_POOLID);
    if (pc1DpaPkt == NULL)
    {
        OFC_TRC_FUNC ((OFC_DPA_TRC,
                       "Memory allocation failed in pkt process\n"));
        return OFC_FAILURE;
    }
    MEMSET (pc1DpaPkt, OFC_ZERO, OFC_DPA_MAX_PKT_SIZE);

    if (CRU_BUF_Copy_FromBufChain
        (pBuf, (UINT1 *) pc1DpaPkt, OFC_ZERO, u4PktSize) == CRU_FAILURE)
    {
        OFC_TRC_FUNC ((OFC_DPA_TRC, "Copying data packet from CRU Buf failed,"
                       "for sending packet-out packet\n"));
        MemReleaseMemBlock (OFC_DPA_PKT_POOLID, (UINT1 *) pc1DpaPkt);
        return OFC_FAILURE;
    }

    /* Fill in DPA structure */
    DataPkt.size = u4PktSize;
    DataPkt.pstart = pc1DpaPkt;

    if (u4OutPort == OFPP_TABLE)
    {
        /* send to DPA pipeline */
        eErr = ofdpaPktSend (&DataPkt, OFDPA_PKT_LOOKUP, u4OutPort, u4InPort);
        if (eErr != OFDPA_E_NONE)
        {
            OFC_TRC_FUNC ((OFC_DPA_TRC, "Delivering the data packet to DPA"
                           "pipeline failed as part of packet-out, "
                           "with error - %d\n", eErr));
            MemReleaseMemBlock (OFC_DPA_PKT_POOLID, (UINT1 *) pc1DpaPkt);
            return OFC_FAILURE;
        }
    }
    else if (u4OutPort == OFPP_FLOOD)
    {
        /* loop and send on all interfaces */
        do
        {
            if (pu4NextPort != NULL)
            {
                u4Port = *pu4NextPort;
            }

            if (ofdpaPktSend (&DataPkt, OFC_ZERO, u4OutPort, OFC_ZERO)
                != OFDPA_E_NONE)
            {
                /* continue, sending on remaining ports */
            }
        }
        while (ofdpaPortNextGet (u4Port, pu4NextPort) == OFDPA_E_NONE);
    }
    else
    {
        /* send to DPA Port */
        eErr = ofdpaPktSend (&DataPkt, OFC_ZERO, u4OutPort, OFC_ZERO);
        if (eErr != OFDPA_E_NONE)
        {
            OFC_TRC_FUNC ((OFC_DPA_TRC, "Delivering the data packet to"
                           "output port failed as part of packet-out,"
                           "with error - %d\n", eErr));
            MemReleaseMemBlock (OFC_DPA_PKT_POOLID, (UINT1 *) pc1DpaPkt);
            return OFC_FAILURE;
        }
    }
    MemReleaseMemBlock (OFC_DPA_PKT_POOLID, (UINT1 *) pc1DpaPkt);

    return OFC_SUCCESS;
}

/*****************************************************************************
 * Function Name : Ofc131DpaEventSockProcess 
 * Description   : This routine posts DPA Flow and Port Events to OFCL queue
 * Input         : i4SockDesc - Socket Descriptor for Flow & Port Events 
 * Output        : NONE
 * Return        : NONE
 *****************************************************************************/
VOID
Ofc131DpaEventSockProcess (INT4 i4SockDesc)
{
    if (OsixEvtSend (gOfcGlobals.ofcTaskId, OFC_DPA_FP_EVENT) == OSIX_FAILURE)
    {
        SelAddFd (i4SockDesc, Ofc131DpaEventSockProcess);
    }
}

/*****************************************************************************
 * Function Name : Ofc131DpaPktSockProcess 
 * Description   : This routine posts DPA Packet Events to OFCL queue 
 * Input         : i4SockDesc - Socket Descriptor for Packet Events 
 * Output        : NONE
 * Return        : NONE
 *****************************************************************************/
VOID
Ofc131DpaPktSockProcess (INT4 i4SockDesc)
{
    if (OsixEvtSend (gOfcGlobals.ofcTaskId, OFC_DPA_PKT_EVENT) == OSIX_FAILURE)
    {
        SelAddFd (i4SockDesc, Ofc131DpaPktSockProcess);
    }
}
