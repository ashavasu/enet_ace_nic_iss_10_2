/*****************************************************************************/
/* Copyright (C) 2013 Aricent Inc . All Rights Reserved
 * $Id: ofcmeter.h,v 1.2 2014/01/31 13:03:56 siva Exp $
 ******************************************************************************
 *    FILE  NAME             : ofcmeter.h
 *    PRINCIPAL AUTHOR       : Aricent Inc.
 *    SUBSYSTEM NAME         : OFC
 *    MODULE NAME            : OFC METER MODULE
 *    LANGUAGE               : ANSI-C
 *    DATE OF FIRST RELEASE  :
 *    DESCRIPTION            : Definition of constants and structures used
 *                             across Meter Related Information 
 *---------------------------------------------------------------------------*/

#ifndef __OFCMETER_H__
#define __OFCMETER_H__

#include "ofcinc.h"
#include "ofc131pkt.h"
#include "ofcapi.h"

/* Meter commands */
enum OFC_METER_MOD_COMMAND {
 OFPMC_ADD = 0,    /* New Meter. */
 OFPMC_MODIFY = 1,   /* Modify all matching Meters. */
 OFPMC_DELETE = 2,   /* Delete all matching Meters. */
 };

/* Meter configuration flags */
enum OFC_MTER_FLAGS {
    OFPMF_KBPS = 1 << 0,  /* Rate value in kb/s (kilo-bit per second). */
    OFPMF_PKTPS = 1 << 1, /* Rate value in packet/sec. */
    OFPMF_BURST = 1 << 2, /* Do burst size. */
    OFPMF_STATS = 1 << 3, /* Collect statistics. */
};

/* Meter band types */
enum OFC_METER_BAND_TYPE {
    OFPMBT_DROP = 1, /* Drop packet. */
    OFPMBT_DSCP_REMARK = 2, /* Remark DSCP in the IP header. */
    OFPMBT_EXPERIMENTER = 0xFFFF /* Experimenter meter band. */
};

/* Meter numbering. Flow meters can use any number up to OFPM_MAX. */
enum OFC_METER {
    OFPM_MAX = 0xffff0000,        /* Last usable meter. */

    /* Virtual meters. */
    OFPM_SLOWPATH = 0xfffffffd,   /* Meter for slow datapath. */
    OFPM_CONTROLLER = 0xfffffffe, /* Meter for controller connection. */
    OFPM_ALL = 0xffffffff,        /* Represents all meters for stat requests commands. */
};

/* ofp_error_msg 'code' values for OFPET_METER_MOD_FAILED. 'data' contains
* at least the first 64 bytes of the failed request. */
enum OFC_METER_MOD_FAILED_CODE {
    OFPMMFC_UNKNOWN = 0, /* Unspecified error. */
    OFPMMFC_METER_EXISTS = 1,     /* Meter not added because a Meter ADD
                                   * attempted to replace an existing Meter. */
    OFPMMFC_INVALID_METER = 2,    /* Meter not added because Meter specified
                                   * is invalid. */
    OFPMMFC_UNKNOWN_METER = 3,    /* Meter not modified because a Meter
                                   * MODIFY attempted to modify a non-existent
                                   * Meter. */
    OFPMMFC_BAD_COMMAND = 4,      /* Unsupported or unknown command. */
    OFPMMFC_BAD_FLAGS = 5,        /* Flag configuration unsupported. */
    OFPMMFC_BAD_RATE = 6,         /* Rate unsupported. */
    OFPMMFC_BAD_BURST = 7,        /* Burst size unsupported. */
    OFPMMFC_BAD_BAND = 8,         /* Band unsupported. */
    OFPMMFC_BAD_BAND_VALUE = 9,   /* Band value unsupported. */
    OFPMMFC_OUT_OF_METERS = 10,   /* No more meters available. */
    OFPMMFC_OUT_OF_BANDS = 11,    /* The maximum number of properties
                                   * for a meter has been exceeded. */
};

/* OFPMBT_DROP band - drop packets */
typedef struct OFP_METER_BAND_DROP {
    UINT2 u2Type;       /* OFPMBT_DROP. */
    UINT2 u2Len;        /* Length in bytes of this band. */
    UINT4 u4Rate;       /* Rate for dropping packets. */
    UINT4 u4BurstSize; /* Size of bursts. */
    UINT1 au1Pad[4];
}tOfpMtrDrop;
typedef struct OFP_METER_BAND_DSCP {
    UINT2 u2Type; /* OFPMBT_DSCP_REMARK. */
    UINT2 u2Len; /* Length in bytes of this band. */
    UINT4 u4Rate; /* Rate for remarking packets. */
    UINT4 u4BurstSize; /* Size of bursts. */
    UINT1 au1PrecLevel; /* Number of precendence level to substract. */
    UINT1 au1Pad[3];
}tOfpMtrDscp;

typedef struct OFP_METER_BAND_EXP {
   UINT2 u2Type; /* One of OFPMBT_*. */
   UINT2 u2Len; /* Length in bytes of this band. */
   UINT4 u4Rate; /* Rate for this band. */
   UINT4 u4BurstSize; /* Size of bursts. */
   UINT4 u4Experimenter; /* Experimenter ID which takes the same*/
}tOfpMtrExpr;

PUBLIC VOID
Ofc131MeterHandleMeterModMsgs(tOfcFsofcCfgEntry *pOfc, tOfcFsofcMeterEntry *pMeterEntry,UINT2 u2cmd);

PUBLIC 
VOID Ofc131MeterMemReleaseBands(tOfcSll *pBandList);

#endif  /* __OFCMETER_H__  */
/*-----------------------------------------------------------------------*/
/*                       End of the file  ofcmeter.h                     */
/*-----------------------------------------------------------------------*/

