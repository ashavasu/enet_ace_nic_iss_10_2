/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: ofcprot.h,v 1.4 2014/08/14 11:13:33 siva Exp $
 *
 * Description: This file contains declaration of function 
 *              prototypes of ofc module.
 *******************************************************************/

#ifndef __OFCPROT_H__
#define __OFCPROT_H__ 

/* Add Prototypes here */

/**************************ofcmain.c*******************************/
PUBLIC VOID OfcMainTask           PROTO ((VOID));
PUBLIC UINT4 OfcMainTaskInit      PROTO ((VOID));
PUBLIC VOID OfcMainDeInit         PROTO ((VOID));
PUBLIC INT4 OfcMainTaskLock       PROTO ((VOID));
PUBLIC INT4 OfcMainTaskUnLock     PROTO ((VOID));
PUBLIC UINT4 Ofc100MainMemInit    PROTO ((tOfcFsofcCfgEntry * pOfcFsofcCfgEntry));
PUBLIC UINT4 Ofc131MainMemInit    PROTO ((tOfcFsofcCfgEntry * pOfcFsofcCfgEntry));
PUBLIC VOID Ofc100MainMemClear    PROTO ((tOfcFsofcCfgEntry * pOfcFsofcCfgEntry));
PUBLIC VOID Ofc131MainMemClear    PROTO ((tOfcFsofcCfgEntry * pOfcFsofcCfgEntry));
PUBLIC INT4 OfcMainCfgContextInit PROTO 
                      ((tOfcFsofcCfgEntry *pOfcFsofcCfgEntry, UINT4 u4Version));

/**************************ofcque.c********************************/
PUBLIC VOID OfcQueProcessMsgs        PROTO ((VOID));
PUBLIC INT4 OfcQueSendMsg            PROTO ((tOfcQueMsg *pQueMsg));
/**************************ofctmr.c*******************************/
PUBLIC VOID  OfcTmrInitTmrDesc      PROTO(( VOID));
PUBLIC VOID  OfcTmrStartTmr         PROTO((tOfcTmr * pOfcTmr, 
                       enOfcTmrId eOfcTmrId, 
         UINT4 u4Secs));
PUBLIC VOID  OfcTmrRestartTmr       PROTO((tOfcTmr * pOfcTmr, 
                 enOfcTmrId eOfcTmrId, 
          UINT4 u4Secs));
PUBLIC VOID  OfcTmrStopTmr          PROTO((tOfcTmr * pOfcTmr));
PUBLIC VOID  OfcTmrHandleExpiry     PROTO((VOID));

/**************************ofctask.c*******************************/
PUBLIC INT4 OfcTaskRegisterOfcMibs (VOID);

/**************************ofccntrl.c******************************/
PUBLIC INT4 OfcCreateController(tOfcFsofcControllerConnEntry *);
VOID OfcControllerPktInSocket(INT4 i4SockDesc);
VOID OfcControllerPktOutSocket (INT4 i4SockDesc);
VOID OfcControllerSockConnect (tOfcFsofcControllerConnEntry *pOfcFsofcControllerConnEntry);
VOID OfcControllerSockConnectSSL (tOfcFsofcControllerConnEntry *pOfcFsofcControllerConnEntry);
VOID OfcControllerProcessSocketWrite (UINT4 u4ContextId ,INT4 i4SockDesc);
INT4 OfcProcessReceivePacket (UINT4 u4ContextId, INT4 i4SockFd);

/**************************ofcutil.c******************************/
tOfcFsofcControllerConnEntry * OfcGetControllerConnTableEntry(INT4 i4SocketDesc);
tOfcFsofcFlowTable * OfcGetFlowTableEntry(UINT4 u4ContextId, UINT4 u4TableIndex);
INT4
OfcInterfacePacketSend (tCRU_BUF_CHAIN_HEADER *pBuf, UINT4 u4IfIndex, UINT4 u4PktSize);
UINT4 OfcConnectionRoleHandler(tOfp131RoleReq *pOfcRoleRequest, 
                               VOID *pConnPtr, UINT4 *pu4Role);
tOfcFsofcCfgEntry * OfcGetFsofcCfgEntry(UINT4 u4ContextId);
INT4
OfcUpdateInterfaceMapping(tOfcFsofcIfEntry * pOfcFsofcIfEntry);
INT4
OfcCheckInterfaceContextId(tOfcFsofcIfEntry *pOfcFsofcIfEntry);
INT4
OfcCheckInterfaceIsMapped(tOfcFsofcIfEntry *pOfcFsofcIfEntry);

#endif   /* __OFCPROT_H__ */

/*-----------------------------------------------------------------------*/
/*                       End of the file  ofcprot.h                     */
/*-----------------------------------------------------------------------*/
