/*****************************************************************************/
/* Copyright (C) 2013 Aricent Inc . All Rights Reserved
 * $Id: ofcinstr.h,v 1.5 2016/03/10 11:42:31 siva Exp $
 ******************************************************************************
 *    FILE  NAME             : ofcinstr.h
 *    PRINCIPAL AUTHOR       : Aricent Inc.
 *    SUBSYSTEM NAME         : OFC
 *    MODULE NAME            : PIPELINE PROCESSING
 *    LANGUAGE               : ANSI-C
 *    DATE OF FIRST RELEASE  :
 *    DESCRIPTION            : Definition of constants and structures used
 *                             across Flow Match/Instructions/Actions 
 *---------------------------------------------------------------------------*/

#ifndef __OFCINSTR_H__
#define __OFCINSTR_H__

#include "lr.h"

/* Ver 1.3.1 Structures */
typedef tTMO_SLL            tOfcSll;
typedef tTMO_SLL_NODE       tOfcSllNode;

typedef tTMO_DLL            tOfcDll;
typedef tTMO_DLL_NODE       tOfcDllNode;

/* Reserved ports */
typedef enum {
    OFPP_IN_PORT    = 0xfffffff8,               /* Deprecated. */
    OFPP_TABLE      = 0xfffffff9,              /* OpenFlow Extensible Match */
    OFPP_NORMAL     = 0xfffffffa,
    OFPP_FLOOD      = 0xfffffffb,
    OFPP_ALL        = 0xfffffffc,
    OFPP_CONTROLLER = 0xfffffffd,
    OFPP_LOCAL      = 0xfffffffe,
    OFPP_ANY        = 0xffffffff
}eOfcReservedPortType;
/**************************** FLOW MATCH - OXMs ******************************/
/* OXM MATCH TYPEs */
typedef enum {
    OFCMT_STANDARD = 0,               /* Deprecated. */
    OFCMT_OXM      = 1,               /* OpenFlow Extensible Match */
}eOfcMatchType;

/* OXM CLASS IDs*/
typedef enum {
    OFCXMC_NXM_0          = 0x0000,   /* Backward compatibility with NXM */
    OFCXMC_NXM_1          = 0x0001,   /* Backward compatibility with NXM */
    OFCXMC_OPENFLOW_BASIC = 0x8000,   /* Basic class for OpenFlow */
    OFCXMC_EXPERIMENTER   = 0xFFFF,   /* Experimenter class */
}eOfcOxmClass;

/* Ver 1.3.1 OXM FLOW MATCH FIELD TYPES FOR OPENfLOW BASIC CLASS. */
typedef enum {
    OFCXMT_OFB_IN_PORT         = 0,   /* Switch input port */
    OFCXMT_OFB_IN_PHY_PORT     = 1,   /* Switch physical input port */
    OFCXMT_OFB_METADATA        = 2,   /* Metadata passed between tables */
    OFCXMT_OFB_ETH_DST         = 3,   /* Ethernet Destination Address */
    OFCXMT_OFB_ETH_SRC         = 4,   /* Ethernet Source Address */
    OFCXMT_OFB_ETH_TYPE        = 5,   /* Ethernet Type */
    OFCXMT_OFB_VLAN_VID        = 6,   /* VLAN ID */  
    OFCXMT_OFB_VLAN_PCP        = 7,   /* VLAN Priority */
    OFCXMT_OFB_IP_DSCP         = 8,   /* IP DSCP (6 bits in ToS field) */
    OFCXMT_OFB_IP_ECN          = 9,   /* IP ECN (2 bits in ToS field) */
    OFCXMT_OFB_IP_PROTO        = 10,  /* IPv4 Protocol */
    OFCXMT_OFB_IPV4_SRC        = 11,  /* IPv4 Source Address */
    OFCXMT_OFB_IPV4_DST        = 12,  /* IPv4 Destination Address */
    OFCXMT_OFB_TCP_SRC         = 13,  /* TCP Source Port */ 
    OFCXMT_OFB_TCP_DST         = 14,  /* TCP Destination Port */
    OFCXMT_OFB_UDP_SRC         = 15,  /* UDP Source Port */
    OFCXMT_OFB_UDP_DST         = 16,  /* UDP Destination Port */
    OFCXMT_OFB_SCTP_SRC        = 17,  /* SCTP Source Port */
    OFCXMT_OFB_SCTP_DST        = 18,  /* SCTP Destination Port */ 
    OFCXMT_OFB_ICMPV4_TYPE     = 19,  /* ICMP Type */
    OFCXMT_OFB_ICMPV4_CODE     = 20,  /* ICMP Code */
    OFCXMT_OFB_ARP_OP          = 21,  /* ARP OpCode */
    OFCXMT_OFB_ARP_SPA         = 22,  /* ARP Source IPv4 Address */
    OFCXMT_OFB_ARP_TPA         = 23,  /* ARP Target IPv4 Address */
    OFCXMT_OFB_ARP_SHA         = 24,  /* ARP Source Hardware Address */
    OFCXMT_OFB_ARP_THA         = 25,  /* ARP Destination Hardware Address */
    OFCXMT_OFB_IPV6_SRC        = 26,  /* IPv6 Source Address */
    OFCXMT_OFB_IPV6_DST        = 27,  /* IPv6 Destination Address */
    OFCXMT_OFB_IPV6_FLABEL     = 28,  /* IPv6 Flow Label */  
    OFCXMT_OFB_ICMPV6_TYPE     = 29,  /* ICMPv6 Type */
    OFCXMT_OFB_ICMPV6_CODE     = 30,  /* ICMPv6 Code */
    OFCXMT_OFB_IPV6_ND_TARGET  = 31,  /* Target Address for ND */
    OFCXMT_OFB_IPV6_ND_SLL     = 32,  /* Source Link Layer for ND */
    OFCXMT_OFB_IPV6_ND_TLL     = 33,  /* Target Link Layer for ND */
    OFCXMT_OFB_MPLS_LABEL      = 34,  /* MPLS Label */
    OFCXMT_OFB_MPLS_TC         = 35,  /* MPLS TC */
    OFCXMT_OFB_MPLS_BOS        = 36,  /* MPLS BoS bit */
    OFCXMT_OFB_PBB_ISID        = 37,  /* PBB I-SID */
    OFCXMT_OFB_TUNNEL_ID       = 38,  /* Logical Port Metadata */
    OFCXMT_OFB_IPV6_EXTHDR     = 39,  /* IPv6 Extension Header pseudo-field */
    OFCXMT_OFB_MAX             = 40
}eOfcOxmOfbMatchFlds;

/* Ver 1.3.1 FLOW MATCH HEADER */
typedef struct {
    UINT2  u2Type;     /* One of OFCMT_* */
    UINT2  u2Len;
}tOfcFlowMatchHdr;

/* OXM TLV HEADER */
typedef struct {
    UINT2  u2Class;
    UINT1  u1Fld;    /* First bit is HM */
    UINT1  u1Len;
}tOfcOxmHdr;

/* Structure for OXM Field */
typedef struct {
    UINT1 au1Fld[16];
}tOxmFld;

/* Structure for OXM Field */
typedef struct
{
    UINT1  au1Fld[16];
} tOxmField;

/* Ver 1.3.1 Flow Match structure for TCAM Chipsets */
typedef struct
{
    tOfcSllNode  Node;
    UINT2        u2Type;
    UINT1        u1Length;
    UINT1        u1HasMask;
    tOxmField    FldValue;
    tOxmField    FldMask;
} tOfcWcFlowMatch;

/* SLL NODE FOR SET FIELDs */
typedef struct {
    tOfcSllNode     Node;
    UINT2           u2Type;
    UINT1           au1Pad[2];
    tOxmFld         SetFld;   /* Max - IPv6 */
}tOfcSetFld;

/* Bit definitions for IPv6 Extension Header pseudo-field. */
typedef enum {
    OFCIEH_NONEXT   = 1 << 0,   /* "No next header" encountered. */
    OFCIEH_ESP      = 1 << 1,   /* Encrypted Sec Payload header present. */
    OFCIEH_AUTH     = 1 << 2,   /* Authentication header present. */
    OFCIEH_DEST     = 1 << 3,   /* 1 or 2 dest headers present. */
    OFCIEH_FRAG     = 1 << 4,   /* Fragment header present. */
    OFCIEH_ROUTER   = 1 << 5,   /* Router header present. */
    OFCIEH_HOP      = 1 << 6,   /* Hop-by-hop header present. */
    OFCIEH_UNREP    = 1 << 7,   /* Unexpected repeats encountered. */
    OFCIEH_UNSEQ    = 1 << 8,   /* Unexpected sequencing encountered. */
}eOfcIpv6ExtHdrFlags;

/* Extension Header format - for most IPv6 Ext Headers */
typedef struct {
    UINT1 u1NxtHdr;
    UINT1 u1HdrLen;
    UINT1 u1Pad[2];
}tOfcExtHdr;

/********************************* ACTIONS ***********************************/
typedef enum {
    OFCAT_OUTPUT        = 0,      /* Output to switch port. */
    OFCAT_COPY_TTL_OUT  = 11,     /* Copy TTL "outwards" from next-to-outermost
                                     to outermost */
    OFCAT_COPY_TTL_IN   = 12,     /* Copy TTL "inwards"  from outermost to
                                     next-to-outermost */
    OFCAT_SET_MPLS_TTL  = 15,     /* MPLS TTL */
    OFCAT_DEC_MPLS_TTL  = 16,     /* Decrement MPLS TTL */
    OFCAT_PUSH_VLAN     = 17,     /* Push a new VLAN tag */
    OFCAT_POP_VLAN      = 18,     /* Pop the outer VLAN tag */ 
    OFCAT_PUSH_MPLS     = 19,     /* Push a new MPLS tag */
    OFCAT_POP_MPLS      = 20,     /* Pop the outer MPLS tag */
    OFCAT_SET_QUEUE     = 21,     /* Set queue id when outputting to a port */
    OFCAT_GROUP         = 22,     /* Apply group. */
    OFCAT_SET_NW_TTL    = 23,     /* IP TTL. */
    OFCAT_DEC_NW_TTL    = 24,     /* Decrement IP TTL */
    OFCAT_SET_FIELD     = 25,     /* Set a header field using OXM TLV format */
    OFCAT_PUSH_PBB      = 26,     /* Push a new PBB service tag (I-TAG) */
    OFCAT_POP_PBB       = 27,     /* Pop the outer PBB service tag (I-TAG) */
    OFCAT_EXPERIMENTER  = 0xFFFF
}eOfc131ActsType;

typedef enum {
    OFCCML_MAX          = 0xffe5, /* maximum max_len value which can be
                                     to request a specific byte length.*/
    OFCCML_NO_BUFFER    = 0xffff  /* indicates that no buffering should
                                     applied and the whole packet is to
                                     sent to the controller. */
}eOfcCtrlMaxLen;

/* ACTION HEADER */
typedef struct {
    UINT2  u2Type;      /* One of OFCAT_*. */
    UINT2  u2Len;       /* Length of action */
}tOfcActsHdr;

/* OFCAT_OUTPUT */
typedef struct {
    UINT4  u4Port;      /* Output port. */
    UINT2  u2MaxLen;    /* Max length to send to controller. */
    UINT1  au1Pad[6];   /*  Since size of tOfp131ActionHdr is 4, padding is handled here */
}tOfcActOutput;

/* SLL to handle multiple output Ports in the flows */
typedef struct {
    tOfcSllNode Node; 
 tOfcActOutput OfcOutput;
}tOfcActOutputPort;

/* OFCAT_GROUP. */
typedef struct ofp_action_group {
    UINT4  u4GroupId;   /* Group identifier. */
}tOfcActGroup;

/* OFCAT_SET_QUEUE */
typedef struct ofp_action_set_queue {
    UINT4  u4QueId;     /* Queue id for the packets */
}tOfcActSetQue;

/* OFCAT_SET_MPLS_TTL. */
typedef struct {
    UINT1  u1MplsTtl;   /* MPLS TTL */
    UINT1  au1Pad[3];   /*  Since size of tOfp131ActionHdr is 4, padding is handled here */
}tOfcActSetMplsTtl;

/* OFCAT_SET_NW_TTL. */
typedef struct {
    UINT1  u1NwTtl;     /* IP TTL */
    UINT1  au1Pad[3];   /*  Since size of tOfp131ActionHdr is 4, padding is handled here */
}tOfcActSetNwTtl;

/* OFCAT_PUSH_VLAN/MPLS/PBB. */
typedef struct {
    UINT2  u2EthType; /* Ethertype */
    UINT1  au1Pad[2]; /*  Since size of tOfp131ActionHdr is 4, padding is handled here */
}tOfcActPush;

/* OFCAT_POP_MPLS. */
typedef struct {
    UINT2  u2EthType; /* Ethertype */
    UINT1  au1Pad[2]; /*  Since size of tOfp131ActionHdr is 4, padding is handled here */
}tOfcActPopMpls;

/* OFCAT_SET_FIELD. */
typedef struct {
    tOfcSll  SetFldList;   /* Head of the Set Fields  of tOfcSetFld  */ 
}tOfcActSetFld;

/* Action header for OFCAT_EXPERIMENTER.
 * The rest of the body is experimenter-defined. */
typedef struct {
    UINT4  u4Experimenter; /* Experimenter ID which takes the same form as in
                              struct ofp_experimenter_header. */
}tOfcActExprHdr;

/* UNION OF ALL ACTIONS */
typedef union {
    tOfcActOutput      Output;
    tOfcActGroup       Group;
    tOfcActSetQue      SetQue;
    tOfcActSetMplsTtl  SetMplsTtl;
    tOfcActSetNwTtl    SetNwTtl;
    tOfcActPush        Push;
    tOfcActPopMpls     PopMpls;
    tOfcActSetFld      SetFld;
    tOfcActExprHdr     ExprHdr;
}unOfc131Acts;

/* SLL NODE FOR ACTIONS */
typedef struct {
    tOfcSllNode       Node;
    eOfc131ActsType   u2Type;
    unOfc131Acts      Acts;
}tOfcActs;

/******************************* INSTRUCTIONS ********************************/
typedef enum {
    OFCIT_GOTO_TABLE      = 1,      /* Setup next table in the lookup */
    OFCIT_WRITE_METADATA  = 2,      /* Setup metadata field for later use */
    OFCIT_WRITE_ACTIONS   = 3,      /* Write the action(s) onto the action set */
    OFCIT_APPLY_ACTIONS   = 4,      /* Applies the action(s) immediately */
    OFCIT_CLEAR_ACTIONS   = 5,      /* Clears all actions from the action set */
    OFCIT_METER           = 6,      /* Apply meter (rate limiter) */
    OFCIT_EXPERIMENTER    = 0xFFFF  /* Experimenter instruction */
}eOfcInstrType;

/* INSTRUCTION HEADER */ 
typedef struct {
    UINT2  u2Type;        /* Instruction type */
    UINT2  u2Len;         /* Length of this struct in bytes. */
}tOfcInstrHdr;

/* OFCIT_GOTO_TABLE */
typedef struct {
    UINT1  u1TableId;     /* Set next table in the lookup pipeline */
}tOfcInstrGotoTable;

/* OFCIT_WRITE_METADATA */
typedef struct {
    FS_UINT8  u8Metadata;     /* Metadata value to write */
    FS_UINT8  u8MetadataMask; /* Metadata write bitmask */
}tOfcInstrWrMetadata;

/* OFCIT_WRITE/APPLY/CLEAR_ACTIONS */
typedef struct {
    tOfcSll  ActList;  /* Head of the Actions of tOfcActs  */ 
}tOfcInstrActions;

/* OFCIT_METER */
typedef struct {
    UINT4  u4MeterId;      /* Meter instance. */
}tOfcInstrMeter;

/* EXPERIMENTER */
typedef struct {
    UINT4  u4Experimenter; /* Experimenter ID which takes the same form
                             as in struct ofp_experimenter_header. */
                           /* Experimenter-defined arbitrary additional data. */
}tOfcInstrExpr;

/* UNION OF ALL INSTRUCTIONS */
typedef union {
    tOfcInstrGotoTable   GotoTable;
    tOfcInstrWrMetadata  Metadata; 
    tOfcInstrActions     Actions;
    tOfcInstrMeter       Meter;
    tOfcInstrExpr        Experimenter;
}unOfcInstr;

/* SLL NODE FOR INSTRUCTION */
typedef struct {
    tOfcSllNode    Node;
    eOfcInstrType  u2Type;
    unOfcInstr     Instr;
}tOfcInstr;

/* ACTION SET ORDER ENUM */
typedef enum {
    OFC_ASO_POP_MPLS       = 0,
    OFC_ASO_PUSH_MPLS      = 1,
    OFC_ASO_PUSH_PBB       = 2,
    OFC_ASO_PUSH_VLAN      = 3,
    OFC_ASO_SET_MPLS_TTL   = 4,
    OFC_ASO_SET_NW_TTL     = 5,
    OFC_ASO_SET_QUEUE      = 6,
    OFC_ASO_GROUP          = 7,
    OFC_ASO_OUTPUT         = 8,
    OFC_ASO_EXPERIMENTER   = 9,
    OFC_ASO_MAX            = 10
}eOfcActionSetOrder;

/* ACTION SET */
typedef struct {
    unOfc131Acts  Acts[OFC_ASO_MAX]; 
    UINT1      au1ActsBitMap[32]; /* based on eOfc131ActsType */
 
   tOxmFld    SetFld[OFCXMT_OFB_MAX];
    UINT1      au1SetFldBitMap[OFCXMT_OFB_MAX];
}tOfcActionSet;

/* Function Prototype Declarations for Pipeline Processing */
INT4 
Ofc131ActsPipelineProcess(UINT4 u4IfIndex, UINT4 u4ContextId, 
                          tCRU_BUF_CHAIN_HEADER *pPkt);


#endif  /* __OFCINSTR_H__  */
/*-----------------------------------------------------------------------*/
/*                       End of the file  ofcinstr.h                     */
/*-----------------------------------------------------------------------*/

