/********************************************************************
* Copyright (C) 2013 Aricent Inc . All Rights Reserved
*
* $Id: fsofcdb.h,v 1.4 2014/03/01 11:36:44 siva Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/


#ifndef _FSOFCDB_H
#define _FSOFCDB_H

UINT1 FsofcCfgTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32};
UINT1 FsofcControllerConnTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32,SNMP_DATA_TYPE_INTEGER,SNMP_DATA_TYPE_OCTET_PRIM,SNMP_DATA_TYPE_INTEGER32};
UINT1 FsofcIfTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32,SNMP_DATA_TYPE_INTEGER};
UINT1 FsofcFlowTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32,SNMP_DATA_TYPE_UNSIGNED32,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 FsofcGroupTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 FsofcMeterTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32,SNMP_DATA_TYPE_UNSIGNED32};

UINT4 fsofc [] ={1,3,6,1,4,1,29601,2,81};
tSNMP_OID_TYPE fsofcOID = {9, fsofc};


UINT4 FsofcContextId [ ] ={1,3,6,1,4,1,29601,2,81,1,1,1,1};
UINT4 FsofcModuleStatus [ ] ={1,3,6,1,4,1,29601,2,81,1,1,1,2};
UINT4 FsofcSupportedVersion [ ] ={1,3,6,1,4,1,29601,2,81,1,1,1,3};
UINT4 FsofcDefaultFlowMissBehaviour [ ] ={1,3,6,1,4,1,29601,2,81,1,1,1,4};
UINT4 FsofcControlPktBuffering [ ] ={1,3,6,1,4,1,29601,2,81,1,1,1,5};
UINT4 FsofcIpReassembleStatus [ ] ={1,3,6,1,4,1,29601,2,81,1,1,1,6};
UINT4 FsofcPortStpStatus [ ] ={1,3,6,1,4,1,29601,2,81,1,1,1,7};
UINT4 FsofcTraceEnable [ ] ={1,3,6,1,4,1,29601,2,81,1,1,1,8};
UINT4 FsofcSwitchModeOnConnFailure [ ] ={1,3,6,1,4,1,29601,2,81,1,1,1,9};
UINT4 FsofcSwitchEntryStatus [ ] ={1,3,6,1,4,1,29601,2,81,1,1,1,10};
UINT4 FsofcHybrid [ ] ={1,3,6,1,4,1,29601,2,81,1,1,1,11};
UINT4 FsofcControllerIpAddrType [ ] ={1,3,6,1,4,1,29601,2,81,2,1,1,1};
UINT4 FsofcControllerIpAddress [ ] ={1,3,6,1,4,1,29601,2,81,2,1,1,2};
UINT4 FsofcControllerConnAuxId [ ] ={1,3,6,1,4,1,29601,2,81,2,1,1,3};
UINT4 FsofcControllerConnPort [ ] ={1,3,6,1,4,1,29601,2,81,2,1,1,4};
UINT4 FsofcControllerConnProtocol [ ] ={1,3,6,1,4,1,29601,2,81,2,1,1,5};
UINT4 FsofcControllerRole [ ] ={1,3,6,1,4,1,29601,2,81,2,1,1,6};
UINT4 FsofcControllerConnState [ ] ={1,3,6,1,4,1,29601,2,81,2,1,1,7};
UINT4 FsofcControllerConnEchoReqCount [ ] ={1,3,6,1,4,1,29601,2,81,2,1,1,8};
UINT4 FsofcControllerConnEchoReplyCount [ ] ={1,3,6,1,4,1,29601,2,81,2,1,1,9};
UINT4 FsofcControllerConnEntryStatus [ ] ={1,3,6,1,4,1,29601,2,81,2,1,1,10};
UINT4 FsofcControllerConnBand [ ] ={1,3,6,1,4,1,29601,2,81,2,1,1,11};
UINT4 FsofcIfIndex [ ] ={1,3,6,1,4,1,29601,2,81,3,1,1,1};
UINT4 FsofcIfType [ ] ={1,3,6,1,4,1,29601,2,81,3,1,1,2};
UINT4 FsofcIfAlias [ ] ={1,3,6,1,4,1,29601,2,81,3,1,1,3};
UINT4 FsofcIfOperStatus [ ] ={1,3,6,1,4,1,29601,2,81,3,1,1,4};
UINT4 FsofcVlanEgressPorts [ ] ={1,3,6,1,4,1,29601,2,81,3,1,1,5};
UINT4 FsofcVlanUntaggedPorts [ ] ={1,3,6,1,4,1,29601,2,81,3,1,1,6};
UINT4 FsofcVlanInFrames [ ] ={1,3,6,1,4,1,29601,2,81,3,1,1,7};
UINT4 FsofcVlanOutFrames [ ] ={1,3,6,1,4,1,29601,2,81,3,1,1,8};
UINT4 FsofcIfContextId [ ] ={1,3,6,1,4,1,29601,2,81,3,1,1,9};
UINT4 FsofcTableIndex [ ] ={1,3,6,1,4,1,29601,2,81,4,1,1,1};
UINT4 FsofcFlowIndex [ ] ={1,3,6,1,4,1,29601,2,81,4,1,1,2};
UINT4 FsofcFlowMatchField [ ] ={1,3,6,1,4,1,29601,2,81,4,1,1,3};
UINT4 FsofcFlowOutputAction [ ] ={1,3,6,1,4,1,29601,2,81,4,1,1,4};
UINT4 FsofcFlowIdleTimeout [ ] ={1,3,6,1,4,1,29601,2,81,4,1,1,5};
UINT4 FsofcFlowHardTimeout [ ] ={1,3,6,1,4,1,29601,2,81,4,1,1,6};
UINT4 FsofcFlowPacketCount [ ] ={1,3,6,1,4,1,29601,2,81,4,1,1,7};
UINT4 FsofcFlowByteCount [ ] ={1,3,6,1,4,1,29601,2,81,4,1,1,8};
UINT4 FsofcFlowDurationSec [ ] ={1,3,6,1,4,1,29601,2,81,4,1,1,9};
UINT4 FsofcGroupIndex [ ] ={1,3,6,1,4,1,29601,2,81,5,1,1,1};
UINT4 FsofcGroupType [ ] ={1,3,6,1,4,1,29601,2,81,5,1,1,2};
UINT4 FsofcGroupActionBuckets [ ] ={1,3,6,1,4,1,29601,2,81,5,1,1,3};
UINT4 FsofcGroupPacketCount [ ] ={1,3,6,1,4,1,29601,2,81,5,1,1,5};
UINT4 FsofcGroupByteCount [ ] ={1,3,6,1,4,1,29601,2,81,5,1,1,6};
UINT4 FsofcGroupDurationSec [ ] ={1,3,6,1,4,1,29601,2,81,5,1,1,7};
UINT4 FsofcMeterIndex [ ] ={1,3,6,1,4,1,29601,2,81,6,1,1,1};
UINT4 FsofcMeterBandInfo [ ] ={1,3,6,1,4,1,29601,2,81,6,1,1,2};
UINT4 FsofcMeterFlowCount [ ] ={1,3,6,1,4,1,29601,2,81,6,1,1,3};
UINT4 FsofcMeterPacketInCount [ ] ={1,3,6,1,4,1,29601,2,81,6,1,1,4};
UINT4 FsofcMeterByteInCount [ ] ={1,3,6,1,4,1,29601,2,81,6,1,1,5};
UINT4 FsofcMeterDurationSec [ ] ={1,3,6,1,4,1,29601,2,81,6,1,1,6};

tMbDbEntry fsofcMibEntry[]= {
{{13,FsofcContextId}, GetNextIndexFsofcCfgTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, FsofcCfgTableINDEX, 1, 0, 0, ""},

{{13,FsofcModuleStatus}, GetNextIndexFsofcCfgTable, FsofcModuleStatusGet, FsofcModuleStatusSet, FsofcModuleStatusTest, FsofcCfgTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsofcCfgTableINDEX, 1, 0, 0, "0"},

{{13,FsofcSupportedVersion}, GetNextIndexFsofcCfgTable, FsofcSupportedVersionGet, FsofcSupportedVersionSet, FsofcSupportedVersionTest, FsofcCfgTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsofcCfgTableINDEX, 1, 0, 0, "2"},

{{13,FsofcDefaultFlowMissBehaviour}, GetNextIndexFsofcCfgTable, FsofcDefaultFlowMissBehaviourGet, FsofcDefaultFlowMissBehaviourSet, FsofcDefaultFlowMissBehaviourTest, FsofcCfgTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsofcCfgTableINDEX, 1, 0, 0, "2"},

{{13,FsofcControlPktBuffering}, GetNextIndexFsofcCfgTable, FsofcControlPktBufferingGet, FsofcControlPktBufferingSet, FsofcControlPktBufferingTest, FsofcCfgTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsofcCfgTableINDEX, 1, 0, 0, "2"},

{{13,FsofcIpReassembleStatus}, GetNextIndexFsofcCfgTable, FsofcIpReassembleStatusGet, FsofcIpReassembleStatusSet, FsofcIpReassembleStatusTest, FsofcCfgTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsofcCfgTableINDEX, 1, 0, 0, "2"},

{{13,FsofcPortStpStatus}, GetNextIndexFsofcCfgTable, FsofcPortStpStatusGet, FsofcPortStpStatusSet, FsofcPortStpStatusTest, FsofcCfgTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsofcCfgTableINDEX, 1, 0, 0, "2"},

{{13,FsofcTraceEnable}, GetNextIndexFsofcCfgTable, FsofcTraceEnableGet, FsofcTraceEnableSet, FsofcTraceEnableTest, FsofcCfgTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsofcCfgTableINDEX, 1, 0, 0, ""},

{{13,FsofcSwitchModeOnConnFailure}, GetNextIndexFsofcCfgTable, FsofcSwitchModeOnConnFailureGet, FsofcSwitchModeOnConnFailureSet, FsofcSwitchModeOnConnFailureTest, FsofcCfgTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsofcCfgTableINDEX, 1, 0, 0, "2"},

{{13,FsofcSwitchEntryStatus}, GetNextIndexFsofcCfgTable, FsofcSwitchEntryStatusGet, FsofcSwitchEntryStatusSet, FsofcSwitchEntryStatusTest, FsofcCfgTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsofcCfgTableINDEX, 1, 0, 1, ""},

{{13,FsofcHybrid}, GetNextIndexFsofcCfgTable, FsofcHybridGet, FsofcHybridSet, FsofcHybridTest, FsofcCfgTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsofcCfgTableINDEX, 1, 0, 0, "-1"},

{{13,FsofcControllerIpAddrType}, GetNextIndexFsofcControllerConnTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsofcControllerConnTableINDEX, 4, 0, 0, ""},

{{13,FsofcControllerIpAddress}, GetNextIndexFsofcControllerConnTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, FsofcControllerConnTableINDEX, 4, 0, 0, ""},

{{13,FsofcControllerConnAuxId}, GetNextIndexFsofcControllerConnTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsofcControllerConnTableINDEX, 4, 0, 0, ""},

{{13,FsofcControllerConnPort}, GetNextIndexFsofcControllerConnTable, FsofcControllerConnPortGet, FsofcControllerConnPortSet, FsofcControllerConnPortTest, FsofcControllerConnTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsofcControllerConnTableINDEX, 4, 0, 0, "6633"},

{{13,FsofcControllerConnProtocol}, GetNextIndexFsofcControllerConnTable, FsofcControllerConnProtocolGet, FsofcControllerConnProtocolSet, FsofcControllerConnProtocolTest, FsofcControllerConnTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsofcControllerConnTableINDEX, 4, 0, 0, "1"},

{{13,FsofcControllerRole}, GetNextIndexFsofcControllerConnTable, FsofcControllerRoleGet, FsofcControllerRoleSet, FsofcControllerRoleTest, FsofcControllerConnTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsofcControllerConnTableINDEX, 4, 0, 0, "1"},

{{13,FsofcControllerConnState}, GetNextIndexFsofcControllerConnTable, FsofcControllerConnStateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsofcControllerConnTableINDEX, 4, 0, 0, ""},

{{13,FsofcControllerConnEchoReqCount}, GetNextIndexFsofcControllerConnTable, FsofcControllerConnEchoReqCountGet, FsofcControllerConnEchoReqCountSet, FsofcControllerConnEchoReqCountTest, FsofcControllerConnTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsofcControllerConnTableINDEX, 4, 0, 0, "0"},

{{13,FsofcControllerConnEchoReplyCount}, GetNextIndexFsofcControllerConnTable, FsofcControllerConnEchoReplyCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsofcControllerConnTableINDEX, 4, 0, 0, ""},

{{13,FsofcControllerConnEntryStatus}, GetNextIndexFsofcControllerConnTable, FsofcControllerConnEntryStatusGet, FsofcControllerConnEntryStatusSet, FsofcControllerConnEntryStatusTest, FsofcControllerConnTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsofcControllerConnTableINDEX, 4, 0, 1, ""},

{{13,FsofcControllerConnBand}, GetNextIndexFsofcControllerConnTable, FsofcControllerConnBandGet, FsofcControllerConnBandSet, FsofcControllerConnBandTest, FsofcControllerConnTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsofcControllerConnTableINDEX, 4, 0, 0, "1"},

{{13,FsofcIfIndex}, GetNextIndexFsofcIfTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsofcIfTableINDEX, 2, 0, 0, ""},

{{13,FsofcIfType}, GetNextIndexFsofcIfTable, FsofcIfTypeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsofcIfTableINDEX, 2, 0, 0, ""},

{{13,FsofcIfAlias}, GetNextIndexFsofcIfTable, FsofcIfAliasGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsofcIfTableINDEX, 2, 0, 0, ""},

{{13,FsofcIfOperStatus}, GetNextIndexFsofcIfTable, FsofcIfOperStatusGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsofcIfTableINDEX, 2, 0, 0, ""},

{{13,FsofcVlanEgressPorts}, GetNextIndexFsofcIfTable, FsofcVlanEgressPortsGet, FsofcVlanEgressPortsSet, FsofcVlanEgressPortsTest, FsofcIfTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsofcIfTableINDEX, 2, 0, 0, NULL},

{{13,FsofcVlanUntaggedPorts}, GetNextIndexFsofcIfTable, FsofcVlanUntaggedPortsGet, FsofcVlanUntaggedPortsSet, FsofcVlanUntaggedPortsTest, FsofcIfTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsofcIfTableINDEX, 2, 0, 0, NULL},

{{13,FsofcVlanInFrames}, GetNextIndexFsofcIfTable, FsofcVlanInFramesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsofcIfTableINDEX, 2, 0, 0, ""},

{{13,FsofcVlanOutFrames}, GetNextIndexFsofcIfTable, FsofcVlanOutFramesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsofcIfTableINDEX, 2, 0, 0, ""},

{{13,FsofcIfContextId}, GetNextIndexFsofcIfTable, FsofcIfContextIdGet, FsofcIfContextIdSet, FsofcIfContextIdTest, FsofcIfTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsofcIfTableINDEX, 2, 0, 0, ""},

{{13,FsofcTableIndex}, GetNextIndexFsofcFlowTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, FsofcFlowTableINDEX, 3, 0, 0, ""},

{{13,FsofcFlowIndex}, GetNextIndexFsofcFlowTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, FsofcFlowTableINDEX, 3, 0, 0, ""},

{{13,FsofcFlowMatchField}, GetNextIndexFsofcFlowTable, FsofcFlowMatchFieldGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsofcFlowTableINDEX, 3, 0, 0, ""},

{{13,FsofcFlowOutputAction}, GetNextIndexFsofcFlowTable, FsofcFlowOutputActionGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsofcFlowTableINDEX, 3, 0, 0, ""},

{{13,FsofcFlowIdleTimeout}, GetNextIndexFsofcFlowTable, FsofcFlowIdleTimeoutGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsofcFlowTableINDEX, 3, 0, 0, ""},

{{13,FsofcFlowHardTimeout}, GetNextIndexFsofcFlowTable, FsofcFlowHardTimeoutGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsofcFlowTableINDEX, 3, 0, 0, ""},

{{13,FsofcFlowPacketCount}, GetNextIndexFsofcFlowTable, FsofcFlowPacketCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER64, SNMP_READONLY, FsofcFlowTableINDEX, 3, 0, 0, ""},

{{13,FsofcFlowByteCount}, GetNextIndexFsofcFlowTable, FsofcFlowByteCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER64, SNMP_READONLY, FsofcFlowTableINDEX, 3, 0, 0, ""},

{{13,FsofcFlowDurationSec}, GetNextIndexFsofcFlowTable, FsofcFlowDurationSecGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsofcFlowTableINDEX, 3, 0, 0, ""},

{{13,FsofcGroupIndex}, GetNextIndexFsofcGroupTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, FsofcGroupTableINDEX, 2, 0, 0, ""},

{{13,FsofcGroupType}, GetNextIndexFsofcGroupTable, FsofcGroupTypeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsofcGroupTableINDEX, 2, 0, 0, ""},

{{13,FsofcGroupActionBuckets}, GetNextIndexFsofcGroupTable, FsofcGroupActionBucketsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsofcGroupTableINDEX, 2, 0, 0, ""},

{{13,FsofcGroupPacketCount}, GetNextIndexFsofcGroupTable, FsofcGroupPacketCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER64, SNMP_READONLY, FsofcGroupTableINDEX, 2, 0, 0, ""},

{{13,FsofcGroupByteCount}, GetNextIndexFsofcGroupTable, FsofcGroupByteCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER64, SNMP_READONLY, FsofcGroupTableINDEX, 2, 0, 0, ""},

{{13,FsofcGroupDurationSec}, GetNextIndexFsofcGroupTable, FsofcGroupDurationSecGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsofcGroupTableINDEX, 2, 0, 0, ""},

{{13,FsofcMeterIndex}, GetNextIndexFsofcMeterTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, FsofcMeterTableINDEX, 2, 0, 0, ""},

{{13,FsofcMeterBandInfo}, GetNextIndexFsofcMeterTable, FsofcMeterBandInfoGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsofcMeterTableINDEX, 2, 0, 0, ""},

{{13,FsofcMeterFlowCount}, GetNextIndexFsofcMeterTable, FsofcMeterFlowCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsofcMeterTableINDEX, 2, 0, 0, ""},

{{13,FsofcMeterPacketInCount}, GetNextIndexFsofcMeterTable, FsofcMeterPacketInCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER64, SNMP_READONLY, FsofcMeterTableINDEX, 2, 0, 0, ""},

{{13,FsofcMeterByteInCount}, GetNextIndexFsofcMeterTable, FsofcMeterByteInCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER64, SNMP_READONLY, FsofcMeterTableINDEX, 2, 0, 0, ""},

{{13,FsofcMeterDurationSec}, GetNextIndexFsofcMeterTable, FsofcMeterDurationSecGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsofcMeterTableINDEX, 2, 0, 0, ""},

};
tMibData fsofcEntry = { 52, fsofcMibEntry };

#endif /* _FSOFCDB_H */


