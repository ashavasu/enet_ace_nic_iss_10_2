/********************************************************************
* Copyright (C) 2013 Aricent Inc . All Rights Reserved
*
* $Id: ofcsrc.h,v 1.2 2014/01/31 13:03:55 siva Exp $
*
* Description: This file holds the functions for the ShowRunningConfig
*     for Ofc module 
*********************************************************************/

#include "ofccli.h"
INT4 OfcShowRunningConfig (tCliHandle CliHandle, UINT4 u4Module, ...);
INT4 OfcShowRunningConfigScalar (tCliHandle CliHandle, UINT4 u4Module, ...);
INT4 OfcShowRunningConfigFsofcControllerConnTable(tCliHandle CliHandle, UINT4 u4Module, ...);
INT4 OfcShowRunningConfigFsofcCfgTable(tCliHandle CliHandle, UINT4 u4Module, ...);
void OfcUtlConversionForFsofcControllerIpAddress (CHR1 *pFsofcControllerIpAddress, tOfcFsofcControllerConnEntry *pOfcFsofcControllerConnEntry);

