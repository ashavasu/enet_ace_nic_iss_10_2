/********************************************************************
 * Copyright (C) 2013 Aricent Inc . All Rights Reserved
 *
 * $Id: ofcapi.h,v 1.11 2015/02/27 10:54:25 siva Exp $
 *
 * Description: APIs Related to Match Structures and Action Sturctures
 **********************************************************************/

#ifndef __OFCAPI_H__
#define __OFCAPI_H__

#include "lr.h"
#include "ofcl.h"
#include "ofcfproc.h"
#include "ip.h"

#define OFC_ALL_ONE 0xff /* to be used in memset */
#define OFC_UINT4_ALL_MASK 0xffffffff
#define OFCXMT_OFB_IPV6_ND_TARGET_MASK 0x80000000
#define OFC_VERSION_ONE 0x01
#define OFC_MINUS_ONE -1

INT4
Ofc131PktParseInstr (VOID *pConnPtr, UINT2 u2MatchLen,
                     tOfcFsofcFlowEntry *pFlowEntry , UINT4 u4InstrLen,UINT4 *pu4Error);

PUBLIC
UINT4 OfcFlowMatchToStr(CHR1 *pStr,tOfcFlowMatch *pOfcFlowMatch);

PUBLIC INT4
Ofc131InstructActionStructToStr(char *pStr, tOfcSll *pOfcSllList);

PUBLIC UINT4
Ofc131MatchFieldsStructToStr(char *pStr, tOfcSll *pMatchList);

PUBLIC INT4
Ofc131ActionsStructTostr(char *pStr, tOfcSll *pActList);

PUBLIC INT4 
Ofc131GroupActionStructToStr(char *pStr,tOfcSll  *pBucketList);

VOID
Ofc131PktMatchStructToTlv (tOfcSll *pMatchList, UINT1 u1NumMatch,
                            tOfp131Match *pu1MatchTlv);

INT4
Ofc131PktOxmParseMatchTlv (INT1 i1TableId, tOfp131Match *pOfpMatch,
                               tOfcSll  *pMatchList, UINT1 *FlowMatchCnt,
                               UINT1 *pu1IsFlowWild, UINT4 *pu4Error);
PUBLIC INT4 
Ofc131MeterBandListToStr(char *pStr,tOfcSll  *pBandList);

VOID
OfcUpdateFlowEntryMibObjectStats(tOfcFlowEntry *pOfcFsofcFlowEntry);

INT4
OfcUpdateCfgConnState(tOfcFsofcControllerConnEntry *pCntlrConnEntry);

INT4
OfcHandleOpenflowPorts (UINT4 u4ContextId, UINT1 u1Flag);

INT4
OfcValidatePortMappingToOfcVlan(UINT1 *pu1MemberPorts, UINT4 u4ContextId,
                                UINT4 u4VlanId);

INT4
OfcGetInterfaceIndexFromName (UINT1 *pu1Alias, UINT4 *pu4IfIndex);

INT4
OfcCheckOfcVlanInterfaceMapping(tOfcFsofcIfEntry *pOfcSetFsofcIfEntry);

/* OFCL MEM APIs */
VOID OfcMemFreeForFlowEntry (UINT1 u1TableId, UINT1 *pMemPool);
VOID OfcMemFreeForFlowMatch (INT1 i1TableId, UINT1 *pMemPool);
VOID OfcMemFreeForInstruction (UINT1 u1TableId, UINT1 *pMemPool);
VOID OfcMemFreeForAction (UINT1 u1TableId, UINT1 *pMemPool);
VOID OfcMemFreeForSetField (UINT1 u1TableId, UINT1 *pMemPool);
VOID OfcMemFreeForExactMatch (UINT1 u1TableId, UINT1 *pMemPool);

tOfcFsofcFlowEntry * OfcMemAllocForFlowEntry (UINT1 u1TableId);
tOfcWcFlowMatch * OfcMemAllocForFlowMatch (INT1 i1TableId);
tOfcInstr * OfcMemAllocForInstruction (UINT1 u1TableId);
tOfcActs * OfcMemAllocForAction (UINT1 u1TableId);
tOfcSetFld * OfcMemAllocForSetField (UINT1 u1TableId);
VOID OfcActOutputPortListFree (tOfcSll *pActOutPortList);

UINT1 * OfcMemAllocForExactMatch (UINT1 u1TableId);

AR_UINT8 Ofcinputhton64 (AR_UINT8 * pinput);

UINT1 OfcGetDpaTableId (UINT1 u1LocalTabId, UINT1 *pu1DpaTabId);
UINT1 OfcGetLocalTableId (UINT1 u1DpaTabId, UINT1 *pu1LocalTabId);

#endif  /* __OFCAPI_H__  */
