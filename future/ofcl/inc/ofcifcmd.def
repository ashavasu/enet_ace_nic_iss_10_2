/********************************************************************
* Copyright (C) 2014 Aricent Inc . All Rights Reserved               
*                                                                    
* $Id: ofcifcmd.def,v 1.5 2015/01/13 11:32:57 siva Exp $                                                         
*                                                                    
*********************************************************************/
/***********************************************************************************/
/*                    OPENFLOW CLIENT CONFIGURATION COMMANDS                       */
/***********************************************************************************/

DEFINE GROUP: OFC_PHY_INTF_CMDS

COMMAND : openflowport
ACTION  : cli_process_interface_cmd (CliHandle, CLI_CFA_PORT_MODE_OF, NULL);
SYNTAX  : openflowport
PRVID   : 15
HELP    : To set the interface to the openflow-interface status and to
          erase all Layer 2 & 3 configurations
CXT_HELP : openflowport Configures port as openflow port |
           <CR> To set the interface to the openflow-interface status and to erase all Layer 2 & 3 configurations

END GROUP

DEFINE GROUP: OFC_INTF_MODE_CMDS
COMMAND : no openflowport
ACTION  : cli_process_interface_cmd (CliHandle, CLI_CFA_NO_PORT_MODE_OF, NULL);
SYNTAX  : no openflowport
PRVID   : 15
HELP    : To set the interface back to switch/router-port mode and
          all Layer 2 cnd Layer 3 configurations can be applied back
CXT_HELP : no Disables the configuration / deletes the entry / resets to default value |
           openflowport Port type related configuration |
           <CR> To set the interface to the switch/router-port mode and all Layer 2/3 configurations are applied

COMMAND : shutdown
ACTION  : cli_process_interface_cmd ( CliHandle, CLI_CFA_INTERFACE_ADMIN_STATUS,
                                                             NULL, CFA_IF_DOWN);
SYNTAX  : shutdown
PRVID   : 15
HELP    : Disables a physical interface.
CXT_HELP : shutdown Shuts down the feature |
           <CR> Disables a physical interface

COMMAND : no shutdown
ACTION  : cli_process_interface_cmd ( CliHandle, CLI_CFA_INTERFACE_ADMIN_STATUS,
                                                              NULL, CFA_IF_UP);
SYNTAX  : no shutdown
PRVID   : 15
HELP    : Enables a physical interface.
CXT_HELP : no Disables the configuration / deletes the entry / resets to default value |
           shutdown Shuts down the feature |
           <CR> Enables a physical interface

END GROUP

DEFINE GROUP: OFC_VLAN_INTF_CMDS

COMMAND : openflowvlan
ACTION  : cli_process_vlan_cmd (CliHandle, CLI_VLAN_MODE_OF, NULL);
SYNTAX  : openflowvlan
PRVID   : 15
HELP    : To set the vlan as openflow logical interface and to
          erase all Layer 2 & 3 configurations
CXT_HELP : openflowvlan Configures vlan as openflow vlan |
           <CR> To set the vlan as openflow logical interface and to erase all Layer 2 & 3 configurations

COMMAND : no openflowvlan
ACTION  : cli_process_vlan_cmd (CliHandle, CLI_VLAN_NO_MODE_OF, NULL);
SYNTAX  : no openflowvlan
PRVID   : 15
HELP    : To set the vlan back to switch vlan mode and
          all Layer 2 and Layer 3 configurations can be applied back
CXT_HELP : no Disables the configuration / deletes the entry / resets to default value |
           openflowvlan Layer 2 vlan related configuration |
           <CR> To set the vlan back  to the switch vlan mode and all Layer 2/3 configurations are applied

END GROUP

DEFINE GROUP: OFC_INTF_MAP_CMDS

COMMAND : map openflow-context <integer(0-65535)>
ACTION  : cli_process_Ofc_cmd (CliHandle, CLI_OFC_PORT_MAP, NULL, $2);
SYNTAX  : map openflow-context <contextid>
PRVID   : 15
HELP    : Mapping the openflow interface to the given context
CXT_HELP : map Maps the port to context |
           openflow-context Virtual context or component of Openflow Client |
           <integer(0-65535)> Context-ID |
           <CR> mapping the openflow interface to the given context

COMMAND : no map openflow-context <integer(0-65535)>
ACTION  : cli_process_Ofc_cmd (CliHandle, CLI_OFC_NO_PORT_MAP, NULL, $3);
SYNTAX  : no map openflow-context <contextid>
PRVID   : 15
HELP    : Un mapping the openflow interface from given context
CXT_HELP : no Disables the configuration / deletes the entry / resets to default value |
           map Maps the port to context |
           openflow-context Virtual context or component of Openflow Client |
           <integer(0-65535)> Context-ID |
           <CR> un-mapping the openflow interface from given context

END GROUP

DEFINE GROUP: OFC_VLAN_MAP_CMDS

COMMAND : map openflowvlan-context <integer(0-65535)>
ACTION  : cli_process_Ofc_cmd (CliHandle, CLI_OFC_VLAN_MAP, NULL, $2);
SYNTAX  : map openflowvlan-context <contextid>
PRVID   : 15
HELP    : Mapping the openflow vlan to the given context
CXT_HELP : map Maps the Openflow Vlan to context |
           openflowvlan-context Virtual context or component of Openflow Client |
           <integer(0-65535)> Context-ID |
           <CR> mapping the openflow vlan to the given context

COMMAND : no map openflowvlan-context <integer(0-65535)>
ACTION  : cli_process_Ofc_cmd (CliHandle, CLI_OFC_NO_VLAN_MAP, NULL, $3);
SYNTAX  : no map openflowvlan-context <contextid>
PRVID   : 15
HELP    : Un mapping the openflow vlan from given context
CXT_HELP : no Disables the configuration / deletes the entry / resets to default value |
           map Maps the openflow vlan to context |
           openflowvlan-context Virtual context or component of Openflow Client |
           <integer(0-65535)> Context-ID |
           <CR> un-mapping the openflow vlan from given context

END GROUP

