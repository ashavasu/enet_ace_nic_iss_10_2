/********************************************************************
* Copyright (C) 2013 Aricent Inc . All Rights Reserved
*
* $Id: ofctdfsg.h,v 1.4 2014/10/13 12:05:37 siva Exp $ 
*
* Description: This file contains data structures defined for Ofc module.
*********************************************************************/
#include "ofcdefn.h"

/* Structure used by CLI to indicate which 
 all objects to be set in FsofcCfgEntry */

typedef struct
{
 BOOL1  bFsofcContextId;
 BOOL1  bFsofcModuleStatus;
 BOOL1  bFsofcSupportedVersion;
 BOOL1  bFsofcDefaultFlowMissBehaviour;
 BOOL1  bFsofcControlPktBuffering;
 BOOL1  bFsofcIpReassembleStatus;
 BOOL1  bFsofcPortStpStatus;
 BOOL1  bFsofcTraceEnable;
 BOOL1  bFsofcSwitchModeOnConnFailure;
 BOOL1  bFsofcSwitchEntryStatus;
 BOOL1  bFsofcHybrid;
} tOfcIsSetFsofcCfgEntry;


/* Structure used by CLI to indicate which 
 all objects to be set in FsofcControllerConnEntry */

typedef struct
{
 BOOL1  bFsofcControllerIpAddrType;
 BOOL1  bFsofcControllerIpAddress;
 BOOL1  bFsofcControllerConnAuxId;
 BOOL1  bFsofcControllerConnPort;
 BOOL1  bFsofcControllerConnProtocol;
 BOOL1  bFsofcControllerRole;
 BOOL1  bFsofcControllerConnEchoReqCount;
 BOOL1  bFsofcControllerConnEntryStatus;
 BOOL1  bFsofcControllerConnBand;
 BOOL1  bFsofcContextId;
} tOfcIsSetFsofcControllerConnEntry;


/* Structure used by CLI to indicate which 
 all objects to be set in FsofcIfEntry */

typedef struct
{
 BOOL1  bFsofcIfIndex;
 BOOL1  bFsofcVlanEgressPorts;
 BOOL1  bFsofcVlanUntaggedPorts;
 BOOL1  bFsofcIfContextId;
 BOOL1  bFsofcContextId;
} tOfcIsSetFsofcIfEntry;



/* Structure used by Ofc protocol for FsofcCfgEntry */

typedef struct
{
 tRBNodeEmbd  FsofcCfgTableNode;
 UINT4 u4FsofcContextId;
 UINT4 u4FsofcTraceEnable;
 INT4 i4FsofcModuleStatus;
 INT4 i4FsofcSupportedVersion;
 INT4 i4FsofcDefaultFlowMissBehaviour;
 INT4 i4FsofcControlPktBuffering;
 INT4 i4FsofcIpReassembleStatus;
 INT4 i4FsofcPortStpStatus;
 INT4 i4FsofcSwitchModeOnConnFailure;
 INT4 i4FsofcSwitchEntryStatus;
 INT4 i4FsofcHybrid;
} tOfcMibFsofcCfgEntry;

/* Structure used by Ofc protocol for FsofcControllerConnEntry */

typedef struct
{
 tRBNodeEmbd  FsofcControllerConnTableNode;
 UINT4 u4FsofcContextId;
 INT4 i4FsofcControllerIpAddrType;
 INT4 i4FsofcControllerConnAuxId;
 INT4 i4FsofcControllerConnPort;
 INT4 i4FsofcControllerConnProtocol;
 INT4 i4FsofcControllerRole;
 INT4 i4FsofcControllerConnState;
 INT4 i4FsofcControllerConnEchoReqCount;
 INT4 i4FsofcControllerConnEchoReplyCount;
 INT4 i4FsofcControllerConnEntryStatus;
 INT4 i4FsofcControllerConnBand;
 INT4 i4FsofcControllerIpAddressLen;
 UINT1 au1FsofcControllerIpAddress[128];
} tOfcMibFsofcControllerConnEntry;

/* Structure used by Ofc protocol for FsofcIfEntry */

typedef struct
{
 tRBNodeEmbd  FsofcIfTableNode;
 UINT4 u4FsofcVlanInFrames;
 UINT4 u4FsofcVlanOutFrames;
 UINT4 u4FsofcIfContextId;
 UINT4 u4FsofcContextId;
 INT4 i4FsofcIfIndex;
 INT4 i4FsofcIfType;
 INT4 i4FsofcIfOperStatus;
 INT4 i4FsofcIfAliasLen;
 INT4 i4FsofcVlanEgressPortsLen;
 INT4 i4FsofcVlanUntaggedPortsLen;
 UINT1 au1FsofcIfAlias[64];
 UINT1 au1FsofcVlanEgressPorts[OFC_SIXTY_FOUR];
 UINT1 au1FsofcVlanUntaggedPorts[OFC_SIXTY_FOUR];
} tOfcMibFsofcIfEntry;

/* Structure used by Ofc protocol for FsofcFlowEntry */

typedef struct
{
 tRBNodeEmbd  FsofcFlowTableNode;
 UINT4 u4FsofcTableIndex;
 UINT4 u4FsofcFlowIndex;
 UINT4 u4FsofcFlowIdleTimeout;
 UINT4 u4FsofcFlowHardTimeout;
 tSNMP_COUNTER64_TYPE u8FsofcFlowPacketCount;
 tSNMP_COUNTER64_TYPE u8FsofcFlowByteCount;
 UINT4 u4FsofcFlowDurationSec;
 UINT4 u4FsofcContextId;
 INT4 i4FsofcFlowMatchFieldLen;
 INT4 i4FsofcFlowOutputActionLen;
 UINT1 au1FsofcFlowMatchField[OFC_ACTION_STRING_MAX_LEN];
 UINT1 au1FsofcFlowOutputAction[OFC_ACTION_STRING_MAX_LEN];
} tOfcMibFsofcFlowEntry;

/* Structure used by Ofc protocol for FsofcGroupEntry */

typedef struct
{
 tRBNodeEmbd  FsofcGroupTableNode;
 UINT4 u4FsofcGroupIndex;
 tSNMP_COUNTER64_TYPE u8FsofcGroupPacketCount;
 tSNMP_COUNTER64_TYPE u8FsofcGroupByteCount;
 UINT4 u4FsofcGroupDurationSec;
 UINT4 u4FsofcContextId;
 INT4 i4FsofcGroupType;
 INT4 i4FsofcGroupActionBucketsLen;
 UINT1 au1FsofcGroupActionBuckets[256];
} tOfcMibFsofcGroupEntry;

/* Structure used by Ofc protocol for FsofcMeterEntry */

typedef struct
{
 tRBNodeEmbd  FsofcMeterTableNode;
 UINT4 u4FsofcMeterIndex;
 UINT4 u4FsofcMeterFlowCount;
 tSNMP_COUNTER64_TYPE u8FsofcMeterPacketInCount;
 tSNMP_COUNTER64_TYPE u8FsofcMeterByteInCount;
 UINT4 u4FsofcMeterDurationSec;
 UINT4 u4FsofcContextId;
 INT4 i4FsofcMeterBandInfoLen;
 UINT1 au1FsofcMeterBandInfo[256];
} tOfcMibFsofcMeterEntry;
