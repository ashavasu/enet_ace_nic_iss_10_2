/********************************************************************
 * Copyright (C) 2013 Aricent Inc . All Rights Reserved
 *
 * $Id: ofcinband.h,v 1.1 2014/01/31 13:06:01 siva Exp $
 *
 * Description: Inband related macros and structures 
 **********************************************************************/

#include "lr.h"

/* Inband Entry State in Controller DataBase */
typedef enum {
    OFCINBD_CNTRL_ENTRY_FREE,
    OFCINBD_CNTRL_ENTRY_EXIST
}eOfcInBandEntryState;

/* InBand Controller DataBase */
typedef struct {
    UINT4 u4CntrlIpAddr;       /* Controller Ip Address */
    UINT4 u4IfToCntrl;         /* Physical Port connected to Controller */
    UINT1 u1EntryState;        /* Entry State */
    UINT1 au1Pad[3];
}tOfcInBandCntrlDb;

UINT4 OfcInBandAddCntrlInDataBase (tOfcFsofcControllerConnEntry *pCntrlEntry);
UINT4 OfcInBandDelCntrlInDataBase (tOfcFsofcControllerConnEntry *pCntrlEntry);
UINT4 OfcInBandVerifyCntrlIPAddress (UINT4 u4IpAddr, UINT1 *pu1EntryIndex);
