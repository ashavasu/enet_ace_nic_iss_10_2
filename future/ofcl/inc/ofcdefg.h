/********************************************************************
* Copyright (C) 2013 Aricent Inc . All Rights Reserved
*
* $Id: ofcdefg.h,v 1.3 2014/01/31 13:03:56 siva Exp $ 
*
* Description: Macros used to fill the CLI structure and 
              index value in the respective structure.
*********************************************************************/


#define OFC_FSOFCCFGTABLE_POOLID   OFCMemPoolIds[MAX_OFC_FSOFCCFGTABLE_SIZING_ID]

#define OFC_FSOFCCFGTABLE_ISSET_POOLID   OFCMemPoolIds[MAX_OFC_FSOFCCFGTABLE_ISSET_SIZING_ID]

#define OFC_FSOFCCONTROLLERCONNTABLE_POOLID   OFCMemPoolIds[MAX_OFC_FSOFCCONTROLLERCONNTABLE_SIZING_ID]

#define OFC_FSOFCCONTROLLERCONNTABLE_ISSET_POOLID   OFCMemPoolIds[MAX_OFC_FSOFCCONTROLLERCONNTABLE_ISSET_SIZING_ID]

#define OFC_FSOFCIFTABLE_POOLID   OFCMemPoolIds[MAX_OFC_FSOFCIFTABLE_SIZING_ID]
#define OFC_FSOFCIFTABLE_ISSET_POOLID   OFCMemPoolIds[MAX_OFC_FSOFCIFTABLE_ISSET_SIZING_ID]

#define OFC_FSOFCFLOWTABLE_POOLID   OFCMemPoolIds[MAX_OFC_FSOFCFLOWTABLE_SIZING_ID]

#define OFC_FSOFCGROUPTABLE_POOLID   OFCMemPoolIds[MAX_OFC_FSOFCGROUPTABLE_SIZING_ID]

#define OFC_FSOFCMETERTABLE_POOLID   OFCMemPoolIds[MAX_OFC_FSOFCMETERTABLE_SIZING_ID]

/* Macro used to fill the CLI structure for FsofcCfgEntry 
 using the input given in def file */

#define  OFC_FILL_FSOFCCFGTABLE_ARGS(pOfcFsofcCfgEntry,\
   pOfcIsSetFsofcCfgEntry,\
   pau1FsofcContextId,\
   pau1FsofcModuleStatus,\
   pau1FsofcSupportedVersion,\
   pau1FsofcDefaultFlowMissBehaviour,\
   pau1FsofcControlPktBuffering,\
   pau1FsofcIpReassembleStatus,\
   pau1FsofcPortStpStatus,\
   pau1FsofcTraceEnable,\
   pau1FsofcSwitchModeOnConnFailure,\
   pau1FsofcSwitchEntryStatus,\
   pau1FsofcHybrid)\
  {\
  if (pau1FsofcContextId != NULL)\
  {\
   pOfcFsofcCfgEntry->MibObject.u4FsofcContextId = *(UINT4 *) (pau1FsofcContextId);\
   pOfcIsSetFsofcCfgEntry->bFsofcContextId = OSIX_TRUE;\
  }\
  else\
  {\
   pOfcIsSetFsofcCfgEntry->bFsofcContextId = OSIX_FALSE;\
  }\
  if (pau1FsofcModuleStatus != NULL)\
  {\
   pOfcFsofcCfgEntry->MibObject.i4FsofcModuleStatus = *(INT4 *) (pau1FsofcModuleStatus);\
   pOfcIsSetFsofcCfgEntry->bFsofcModuleStatus = OSIX_TRUE;\
  }\
  else\
  {\
   pOfcIsSetFsofcCfgEntry->bFsofcModuleStatus = OSIX_FALSE;\
  }\
  if (pau1FsofcSupportedVersion != NULL)\
  {\
   pOfcFsofcCfgEntry->MibObject.i4FsofcSupportedVersion = *(INT4 *) (pau1FsofcSupportedVersion);\
   pOfcIsSetFsofcCfgEntry->bFsofcSupportedVersion = OSIX_TRUE;\
  }\
  else\
  {\
   pOfcIsSetFsofcCfgEntry->bFsofcSupportedVersion = OSIX_FALSE;\
  }\
  if (pau1FsofcDefaultFlowMissBehaviour != NULL)\
  {\
   pOfcFsofcCfgEntry->MibObject.i4FsofcDefaultFlowMissBehaviour = *(INT4 *) (pau1FsofcDefaultFlowMissBehaviour);\
   pOfcIsSetFsofcCfgEntry->bFsofcDefaultFlowMissBehaviour = OSIX_TRUE;\
  }\
  else\
  {\
   pOfcIsSetFsofcCfgEntry->bFsofcDefaultFlowMissBehaviour = OSIX_FALSE;\
  }\
  if (pau1FsofcControlPktBuffering != NULL)\
  {\
   pOfcFsofcCfgEntry->MibObject.i4FsofcControlPktBuffering = *(INT4 *) (pau1FsofcControlPktBuffering);\
   pOfcIsSetFsofcCfgEntry->bFsofcControlPktBuffering = OSIX_TRUE;\
  }\
  else\
  {\
   pOfcIsSetFsofcCfgEntry->bFsofcControlPktBuffering = OSIX_FALSE;\
  }\
  if (pau1FsofcIpReassembleStatus != NULL)\
  {\
   pOfcFsofcCfgEntry->MibObject.i4FsofcIpReassembleStatus = *(INT4 *) (pau1FsofcIpReassembleStatus);\
   pOfcIsSetFsofcCfgEntry->bFsofcIpReassembleStatus = OSIX_TRUE;\
  }\
  else\
  {\
   pOfcIsSetFsofcCfgEntry->bFsofcIpReassembleStatus = OSIX_FALSE;\
  }\
  if (pau1FsofcPortStpStatus != NULL)\
  {\
   pOfcFsofcCfgEntry->MibObject.i4FsofcPortStpStatus = *(INT4 *) (pau1FsofcPortStpStatus);\
   pOfcIsSetFsofcCfgEntry->bFsofcPortStpStatus = OSIX_TRUE;\
  }\
  else\
  {\
   pOfcIsSetFsofcCfgEntry->bFsofcPortStpStatus = OSIX_FALSE;\
  }\
  if (pau1FsofcTraceEnable != NULL)\
  {\
   pOfcFsofcCfgEntry->MibObject.u4FsofcTraceEnable = *(UINT4 *) (pau1FsofcTraceEnable);\
   pOfcIsSetFsofcCfgEntry->bFsofcTraceEnable = OSIX_TRUE;\
  }\
  else\
  {\
   pOfcIsSetFsofcCfgEntry->bFsofcTraceEnable = OSIX_FALSE;\
  }\
  if (pau1FsofcSwitchModeOnConnFailure != NULL)\
  {\
   pOfcFsofcCfgEntry->MibObject.i4FsofcSwitchModeOnConnFailure = *(INT4 *) (pau1FsofcSwitchModeOnConnFailure);\
   pOfcIsSetFsofcCfgEntry->bFsofcSwitchModeOnConnFailure = OSIX_TRUE;\
  }\
  else\
  {\
   pOfcIsSetFsofcCfgEntry->bFsofcSwitchModeOnConnFailure = OSIX_FALSE;\
  }\
  if (pau1FsofcSwitchEntryStatus != NULL)\
  {\
   pOfcFsofcCfgEntry->MibObject.i4FsofcSwitchEntryStatus = *(INT4 *) (pau1FsofcSwitchEntryStatus);\
   pOfcIsSetFsofcCfgEntry->bFsofcSwitchEntryStatus = OSIX_TRUE;\
  }\
  else\
  {\
   pOfcIsSetFsofcCfgEntry->bFsofcSwitchEntryStatus = OSIX_FALSE;\
  }\
  if (pau1FsofcHybrid != NULL)\
  {\
   pOfcFsofcCfgEntry->MibObject.i4FsofcHybrid = *(INT4 *) (pau1FsofcHybrid);\
   pOfcIsSetFsofcCfgEntry->bFsofcHybrid = OSIX_TRUE;\
  }\
  else\
  {\
   pOfcIsSetFsofcCfgEntry->bFsofcHybrid = OSIX_FALSE;\
  }\
  }
/* Macro used to fill the CLI structure for FsofcControllerConnEntry 
 using the input given in def file */

#define  OFC_FILL_FSOFCCONTROLLERCONNTABLE_ARGS(pOfcFsofcControllerConnEntry,\
   pOfcIsSetFsofcControllerConnEntry,\
   pau1FsofcControllerIpAddrType,\
   pau1FsofcControllerIpAddress,\
   pau1FsofcControllerIpAddressLen,\
   pau1FsofcControllerConnAuxId,\
   pau1FsofcControllerConnPort,\
   pau1FsofcControllerConnProtocol,\
   pau1FsofcControllerRole,\
   pau1FsofcControllerConnEchoReqCount,\
   pau1FsofcControllerConnEntryStatus,\
   pau1FsofcControllerConnBand,\
   pau1FsofcContextId)\
  {\
  if (pau1FsofcControllerIpAddrType != NULL)\
  {\
   pOfcFsofcControllerConnEntry->MibObject.i4FsofcControllerIpAddrType = *(INT4 *) (pau1FsofcControllerIpAddrType);\
   pOfcIsSetFsofcControllerConnEntry->bFsofcControllerIpAddrType = OSIX_TRUE;\
  }\
  else\
  {\
   pOfcIsSetFsofcControllerConnEntry->bFsofcControllerIpAddrType = OSIX_FALSE;\
  }\
  if (pau1FsofcControllerIpAddress != NULL)\
  {\
   MEMCPY (pOfcFsofcControllerConnEntry->MibObject.au1FsofcControllerIpAddress, pau1FsofcControllerIpAddress, *(INT4 *)pau1FsofcControllerIpAddressLen);\
   pOfcFsofcControllerConnEntry->MibObject.i4FsofcControllerIpAddressLen = *(INT4 *)pau1FsofcControllerIpAddressLen;\
   pOfcIsSetFsofcControllerConnEntry->bFsofcControllerIpAddress = OSIX_TRUE;\
  }\
  else\
  {\
   pOfcIsSetFsofcControllerConnEntry->bFsofcControllerIpAddress = OSIX_FALSE;\
  }\
  if (pau1FsofcControllerConnAuxId != NULL)\
  {\
   pOfcFsofcControllerConnEntry->MibObject.i4FsofcControllerConnAuxId = *(INT4 *) (pau1FsofcControllerConnAuxId);\
   pOfcIsSetFsofcControllerConnEntry->bFsofcControllerConnAuxId = OSIX_TRUE;\
  }\
  else\
  {\
   pOfcIsSetFsofcControllerConnEntry->bFsofcControllerConnAuxId = OSIX_FALSE;\
  }\
  if (pau1FsofcControllerConnPort != NULL)\
  {\
   pOfcFsofcControllerConnEntry->MibObject.i4FsofcControllerConnPort = *(INT4 *) (pau1FsofcControllerConnPort);\
   pOfcIsSetFsofcControllerConnEntry->bFsofcControllerConnPort = OSIX_TRUE;\
  }\
  else\
  {\
   pOfcIsSetFsofcControllerConnEntry->bFsofcControllerConnPort = OSIX_FALSE;\
  }\
  if (pau1FsofcControllerConnProtocol != NULL)\
  {\
   pOfcFsofcControllerConnEntry->MibObject.i4FsofcControllerConnProtocol = *(INT4 *) (pau1FsofcControllerConnProtocol);\
   pOfcIsSetFsofcControllerConnEntry->bFsofcControllerConnProtocol = OSIX_TRUE;\
  }\
  else\
  {\
   pOfcIsSetFsofcControllerConnEntry->bFsofcControllerConnProtocol = OSIX_FALSE;\
  }\
  if (pau1FsofcControllerRole != NULL)\
  {\
   pOfcFsofcControllerConnEntry->MibObject.i4FsofcControllerRole = *(INT4 *) (pau1FsofcControllerRole);\
   pOfcIsSetFsofcControllerConnEntry->bFsofcControllerRole = OSIX_TRUE;\
  }\
  else\
  {\
   pOfcIsSetFsofcControllerConnEntry->bFsofcControllerRole = OSIX_FALSE;\
  }\
  if (pau1FsofcControllerConnEchoReqCount != NULL)\
  {\
   pOfcFsofcControllerConnEntry->MibObject.i4FsofcControllerConnEchoReqCount = *(INT4 *) (pau1FsofcControllerConnEchoReqCount);\
   pOfcIsSetFsofcControllerConnEntry->bFsofcControllerConnEchoReqCount = OSIX_TRUE;\
  }\
  else\
  {\
   pOfcIsSetFsofcControllerConnEntry->bFsofcControllerConnEchoReqCount = OSIX_FALSE;\
  }\
  if (pau1FsofcControllerConnEntryStatus != NULL)\
  {\
   pOfcFsofcControllerConnEntry->MibObject.i4FsofcControllerConnEntryStatus = *(INT4 *) (pau1FsofcControllerConnEntryStatus);\
   pOfcIsSetFsofcControllerConnEntry->bFsofcControllerConnEntryStatus = OSIX_TRUE;\
  }\
  else\
  {\
   pOfcIsSetFsofcControllerConnEntry->bFsofcControllerConnEntryStatus = OSIX_FALSE;\
  }\
  if (pau1FsofcControllerConnBand != NULL)\
  {\
   pOfcFsofcControllerConnEntry->MibObject.i4FsofcControllerConnBand = *(INT4 *) (pau1FsofcControllerConnBand);\
   pOfcIsSetFsofcControllerConnEntry->bFsofcControllerConnBand = OSIX_TRUE;\
  }\
  else\
  {\
   pOfcIsSetFsofcControllerConnEntry->bFsofcControllerConnBand = OSIX_FALSE;\
  }\
  if (pau1FsofcContextId != NULL)\
  {\
   pOfcFsofcControllerConnEntry->MibObject.u4FsofcContextId = *(UINT4 *) (pau1FsofcContextId);\
   pOfcIsSetFsofcControllerConnEntry->bFsofcContextId = OSIX_TRUE;\
  }\
  else\
  {\
   pOfcIsSetFsofcControllerConnEntry->bFsofcContextId = OSIX_FALSE;\
  }\
  }
/* Macro used to fill the CLI structure for FsofcIfEntry 
 using the input given in def file */

#define  OFC_FILL_FSOFCIFTABLE_ARGS(pOfcFsofcIfEntry,\
   pOfcIsSetFsofcIfEntry,\
   pau1FsofcIfIndex,\
   pau1FsofcVlanEgressPorts,\
   pau1FsofcVlanEgressPortsLen,\
   pau1FsofcVlanUntaggedPorts,\
   pau1FsofcVlanUntaggedPortsLen,\
   pau1FsofcIfContextId,\
   pau1FsofcContextId)\
  {\
  if (pau1FsofcIfIndex != NULL)\
  {\
   pOfcFsofcIfEntry->MibObject.i4FsofcIfIndex = *(INT4 *) (pau1FsofcIfIndex);\
   pOfcIsSetFsofcIfEntry->bFsofcIfIndex = OSIX_TRUE;\
  }\
  else\
  {\
   pOfcIsSetFsofcIfEntry->bFsofcIfIndex = OSIX_FALSE;\
  }\
  if (pau1FsofcVlanEgressPorts != NULL)\
  {\
   MEMCPY (pOfcFsofcIfEntry->MibObject.au1FsofcVlanEgressPorts, pau1FsofcVlanEgressPorts, *(INT4 *)pau1FsofcVlanEgressPortsLen);\
   pOfcFsofcIfEntry->MibObject.i4FsofcVlanEgressPortsLen = *(INT4 *)pau1FsofcVlanEgressPortsLen;\
   pOfcIsSetFsofcIfEntry->bFsofcVlanEgressPorts = OSIX_TRUE;\
  }\
  else\
  {\
   pOfcIsSetFsofcIfEntry->bFsofcVlanEgressPorts = OSIX_FALSE;\
  }\
  if (pau1FsofcVlanUntaggedPorts != NULL)\
  {\
   MEMCPY (pOfcFsofcIfEntry->MibObject.au1FsofcVlanUntaggedPorts, pau1FsofcVlanUntaggedPorts, *(INT4 *)pau1FsofcVlanUntaggedPortsLen);\
   pOfcFsofcIfEntry->MibObject.i4FsofcVlanUntaggedPortsLen = *(INT4 *)pau1FsofcVlanUntaggedPortsLen;\
   pOfcIsSetFsofcIfEntry->bFsofcVlanUntaggedPorts = OSIX_TRUE;\
  }\
  else\
  {\
   pOfcIsSetFsofcIfEntry->bFsofcVlanUntaggedPorts = OSIX_FALSE;\
  }\
  if (pau1FsofcIfContextId != NULL)\
  {\
   pOfcFsofcIfEntry->MibObject.u4FsofcIfContextId = *(UINT4 *) (pau1FsofcIfContextId);\
   pOfcIsSetFsofcIfEntry->bFsofcIfContextId = OSIX_TRUE;\
  }\
  else\
  {\
   pOfcIsSetFsofcIfEntry->bFsofcIfContextId = OSIX_FALSE;\
  }\
  if (pau1FsofcContextId != NULL)\
  {\
   pOfcFsofcIfEntry->MibObject.u4FsofcContextId = *(UINT4 *) (pau1FsofcContextId);\
   pOfcIsSetFsofcIfEntry->bFsofcContextId = OSIX_TRUE;\
  }\
  else\
  {\
   pOfcIsSetFsofcIfEntry->bFsofcContextId = OSIX_FALSE;\
  }\
  }
/* Macro used to fill the index values in  structure for FsofcCfgEntry 
 using the input given in def file */

#define  OFC_FILL_FSOFCCFGTABLE_INDEX(pOfcFsofcCfgEntry,\
   pau1FsofcContextId)\
  {\
  if (pau1FsofcContextId != NULL)\
  {\
   pOfcFsofcCfgEntry->MibObject.u4FsofcContextId = *(UINT4 *) (pau1FsofcContextId);\
  }\
 }
/* Macro used to fill the index values in  structure for FsofcControllerConnEntry 
 using the input given in def file */

#define  OFC_FILL_FSOFCCONTROLLERCONNTABLE_INDEX(pOfcFsofcControllerConnEntry,\
   pau1FsofcContextId,\
   pau1FsofcControllerIpAddrType,\
   pau1FsofcControllerIpAddress,\
   pau1FsofcControllerConnAuxId)\
  {\
  if (pau1FsofcContextId != NULL)\
  {\
   pOfcFsofcControllerConnEntry->MibObject.u4FsofcContextId = *(UINT4 *) (pau1FsofcContextId);\
  }\
  if (pau1FsofcControllerIpAddrType != NULL)\
  {\
   pOfcFsofcControllerConnEntry->MibObject.i4FsofcControllerIpAddrType = *(INT4 *) (pau1FsofcControllerIpAddrType);\
  }\
  if (pau1FsofcControllerIpAddress != NULL)\
  {\
   MEMCPY (pOfcFsofcControllerConnEntry->MibObject.au1FsofcControllerIpAddress, pau1FsofcControllerIpAddress, *(INT4 *)pau1FsofcControllerIpAddressLen);\
   pOfcFsofcControllerConnEntry->MibObject.i4FsofcControllerIpAddressLen = *(INT4 *)pau1FsofcControllerIpAddressLen;\
  }\
  if (pau1FsofcControllerConnAuxId != NULL)\
  {\
   pOfcFsofcControllerConnEntry->MibObject.i4FsofcControllerConnAuxId = *(INT4 *) (pau1FsofcControllerConnAuxId);\
  }\
 }
/* Macro used to fill the index values in  structure for FsofcIfEntry 
 using the input given in def file */

#define  OFC_FILL_FSOFCIFTABLE_INDEX(pOfcFsofcIfEntry,\
   pau1FsofcContextId,\
   pau1FsofcIfIndex)\
  {\
  if (pau1FsofcContextId != NULL)\
  {\
   pOfcFsofcIfEntry->MibObject.u4FsofcContextId = *(UINT4 *) (pau1FsofcContextId);\
  }\
  if (pau1FsofcIfIndex != NULL)\
  {\
   pOfcFsofcIfEntry->MibObject.i4FsofcIfIndex = *(INT4 *) (pau1FsofcIfIndex);\
  }\
 }
