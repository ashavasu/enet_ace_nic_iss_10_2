/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * Description: This file contains extern declaration of global 
 *              variables of the ofc module.
 *******************************************************************/

#ifndef __OFCEXTN_H__
#define __OFCEXTN_H__

PUBLIC tOfcGlobals gOfcGlobals;

#endif/*__OFCEXTN_H__*/
   
/*-----------------------------------------------------------------------*/
/*                       End of the file ofcextn.h                      */
/*-----------------------------------------------------------------------*/

