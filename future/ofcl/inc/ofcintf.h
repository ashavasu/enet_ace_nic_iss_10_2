/********************************************************************
 ** Copyright (C) 2006 Aricent Inc . All Rights Reserved
 **
 ** $Id: ofcintf.h,v 1.3 2014/08/14 11:13:33 siva Exp $
 **
 ** Description: This file contains declaration of function
 **              prototypes of ofc module.
 ********************************************************************/

#ifndef __OFCINTF_H__
#define __OFCINTF_H__

#include "ofcpkt.h"

/* What changed about the physical port */
enum OFC_PORT_REASON {
    OFPPR_ADD, /* The port was added. */
    OFPPR_DELETE, /* The port was removed. */
    OFPPR_MODIFY /* Some attribute of the port has changed. */
};

INT4
OpenflowPortMapUnmap(UINT4 u4IfIndex, UINT4 u4Context, INT1 i1action);
INT4
OfcInterfacePortStatusEventSend (VOID *pPortStatus, UINT4 u4ContextId);

#endif   /* __OFCINTF_H__ */

/*-----------------------------------------------------------------------*/
/*                       End of the file  ofcintf.h                      */
/*-----------------------------------------------------------------------*/

