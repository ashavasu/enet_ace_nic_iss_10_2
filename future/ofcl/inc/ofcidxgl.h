
/********************************************************************
 *                                                                  *
 * $Id: ofcidxgl.h,v 1.1 2014/01/31 13:14:30 siva Exp $
 *                                                                  *
 ********************************************************************/

/* Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *---------------------------------------------------------------------------
 *    FILE  NAME             : ofcidxgl.h
 *    PRINCIPAL AUTHOR       : Aricent Inc.
 *    SUBSYSTEM NAME         : OFC - Utility    
 *    MODULE NAME            : Flow Index Tbl Mgr : Flow Table Module
 *    LANGUAGE               : ANSI-C
 *    DATE OF FIRST RELEASE  :
 *    DESCRIPTION            : This file contains global declarations that 
 *                             are associated with Flow Index Table Manager
 *                             
 *-------------------------------------------------------------------------*/

#ifndef _OFCIDXGL_H
#define _OFCIDXGL_H

#include "ofcdefn.h"

/* Global ptr to the table of index groups */
/* Global Pool Id for Memory Pool of bitmap chunks */
#define CHUNK_TBL_POOL_ID  OFCINDEXMGRMemPoolIds[MAX_INDEXMGR_INDEX_TBL_CHUNK_SIZING_ID]
#define GROUP_INFO_POOL_ID OFCINDEXMGRMemPoolIds[MAX_INDEXMGR_INDEX_MGR_GRP_SIZING_ID]
#define CHUNK_INFO_POOL_ID OFCINDEXMGRMemPoolIds[MAX_INDEXMGR_INDEX_MGR_CHUNK_SIZING_ID]
/* Index manager Global Semaphore */
extern tOsixSemId   gOfcIndexMgrSemId;
#define     OFC_INDEXMGR_LOCK OfcIndexMgrLock
#define     OFC_INDEXMGR_UNLOCK OfcIndexMgrUnLock
PUBLIC VOID OfcIndexMgrLock(VOID);
PUBLIC VOID OfcIndexMgrUnLock(VOID);
/* ==== Start : To add a group, define it & append it to the array ==== */
/* The foll. 3 constants need to be configured as per requirement */



/* Define new entries here... */
extern tIndexMgrGrpTable OfcIndexMgrGrpEntry[];
extern UINT4 au4OfcGrpMaxIndices[];
                
                          /* Append newly defined entries here, in this array */
                          /* Max Groups supported is MAXUINT1 = 255*/
      
                          /* Last one is same as Max Indices for
                             Grp No. INDEXMGR_MAX_GRPS_SPRTD */


/* ==== End : To add a group, define it & append it to the array ====== */

/* Global Variables used by Index Resource Manager */
/* Global Array Variable initialised with Key available indication bit maps */

extern UINT4 aOfcAvlByteBmap[];

extern UINT4 aOfcAvlBitmap[];

extern UINT4 aOfcAsgnBitmap[];

#endif /*_OFCIDXGL_H */

/*-------------------------------------------------------------------------
*                        End of file ofcidxgl.h                             
-------------------------------------------------------------------------*/
