/********************************************************************
* Copyright (C) 2013 Aricent Inc . All Rights Reserved
*
* $Id: ofcsrcdefn.h,v 1.3 2014/07/21 11:36:16 siva Exp $
*
* Description: This file contains the default values for Ofc module.
*********************************************************************/


#ifndef __OFCSRCDEFN_H__
#define __OFCSRCDEFN_H__
#include "ofcapi.h"

#define  OFC_TASK_PRIORITY              100
#define  OFC_QUEUE_NAME                 (const UINT1 *) "OFCQ"
#define  OFC_MUT_EXCL_SEM_NAME          (const UINT1 *) "OFCM"
#define  OFC_QUEUE_DEPTH                100
#define  OFC_SEM_CREATE_INIT_CNT        1


#define  OFC_ENABLED                    1
#define  OFC_DISABLED                   2

#define  OFC_TIMER_EVENT             0x00000001
#define  OFC_QUEUE_EVENT             0x00000002
#define  MAX_OFC_DUMMY                   1

#define  OFC_SRC_ZERO                   OFC_ZERO
#define  OFC_SRC_ONE                    OFC_ONE
#define  OFC_SRC_TWO                    OFC_TWO
/*********** macros for scalar objects*********************/


/*********** macros for tabular objects*********************/

#define OFC_DEF_FSOFCMODULESTATUS             0
#define OFC_DEF_FSOFCSUPPORTEDVERSION             2
#define OFC_DEF_FSOFCDEFAULTFLOWMISSBEHAVIOUR             2
#define OFC_DEF_FSOFCCONTROLPKTBUFFERING             2
#define OFC_DEF_FSOFCIPREASSEMBLESTATUS             2
#define OFC_DEF_FSOFCPORTSTPSTATUS             2
#define OFC_DEF_FSOFCSWITCHMODEONCONNFAILURE             1
#define OFC_DEF_FSOFCHYBRID             2
#define OFC_DEF_FSOFCCONTROLLERCONNPORT             6633
#define OFC_DEF_FSOFCCONTROLLERCONNPROTOCOL             1
#define OFC_DEF_FSOFCCONTROLLERROLE             1
#define OFC_DEF_FSOFCCONTROLLERCONNECHOREQCOUNT             0
#define OFC_DEF_FSOFCCONTROLLERCONNBAND             1

#endif  /* __OFCDEFN_H__*/
/*-----------------------------------------------------------------------*/
/*                       End of the file ofcdefn.h                      */
/*-----------------------------------------------------------------------*/

