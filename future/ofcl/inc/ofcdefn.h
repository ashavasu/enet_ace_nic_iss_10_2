/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: ofcdefn.h,v 1.9 2015/02/27 10:54:25 siva Exp $
 *
 * Description: This file contains definitions for ofc module.
 *******************************************************************/

#ifndef __OFCDEFN_H__
#define __OFCDEFN_H__


#define  MAX_OFC_DUMMY                   1
#define  OFC_ZERO                        0
#define  OFC_BUFFER_SIZE                 256
#define  OFC_TRUE                        1
#define  OFC_FALSE                       0
#define  OFC_MAX_PKTIN_SIZE              2400
#define  OFC_MAX_FLOW_MATCH_SIZE         1500
#define  OFC_MAX_FLOW_MATCH_TLV_SIZE     1024
#define  OFC_MAX_MULTIPART_MSG_SIZE      2800
#define  OFC_ACTION_STRING_MAX_LEN       128

/* Numeric Macros */
#define OFC_ONE                 1
#define OFC_TWO                 2
#define OFC_THREE               3
#define OFC_FOUR                4
#define OFC_FIVE                5
#define OFC_SIX                 6
#define OFC_SEVEN               7
#define OFC_EIGHT               8
#define OFC_NINE                9
#define OFC_TEN                10
#define OFC_ELEVEN             11
#define OFC_TWELVE             12
#define OFC_THIRTEEN           13
#define OFC_FOURTEEN           14
#define OFC_FIFTEEN            15
#define OFC_SIXTEEN            16
#define OFC_EIGHTEEN           18
#define OFC_NINETEEN           19
#define OFC_TWENTY             20
#define OFC_TWENTY_ONE         21
#define OFC_TWENTY_FOUR        24
#define OFC_TWENTY_SEVEN       27
#define OFC_TWENTY_NINE        29
#define OFC_THIRTY_ONE         31
#define OFC_THIRTY_TWO         32
#define OFC_THIRTY_NINE        39
#define OFC_FORTY              40
#define OFC_FORTY_TWO          42
#define OFC_FORTY_EIGHT        48
#define OFC_FIFTY_SIX          56
#define OFC_SIXTY_FOUR         64
#define OFC_HUNDRED           100
#define OFC_ONE_TWENTY_EIGHT  128
#define OFC_TWO_FIFTY_SIX     256
#define OFC_FIVE_TWELVE       512

#define  OFC_QUEUE_POOLID  OFCMemPoolIds[MAX_OFC_QUEUE_MSG_SIZING_ID]

/* MEM POOL IDs FOR VER 100 */
#define  OFC_FLOWMSGS_POOLID     OFCMemPoolIds[MAX_OFC_FLOWMSGS_SIZING_ID]

/* MEM POOL IDs FOR VER 131 */
#define  OFC_MULTIPART_POOLID   OFCMemPoolIds[MAX_OFC_MULTIPARTREPLY_MSG_SIZING_ID]
#define  OFC_NON_TCAM_POOLID        OFCMemPoolIds[MAX_OFC_NON_TCAM_MSG_SIZING_ID]
#define  OFC_ACTION_BUCKET_POOLID   OFCMemPoolIds[MAX_OFC_ACTION_BUCKET_SIZING_ID]
#define  OFC_FLOW_MATCH_TLV_POOLID  OFCMemPoolIds[MAX_OFC_FLOW_MATCH_TLV_SIZING_ID]
#define  OFC_PKTIN_POOLID           OFCMemPoolIds[MAX_OFC_PKTIN_SIZING_ID]
#define  OFC_FLOW_TABLE_POOLID      OFCMemPoolIds[MAX_OFC_FLOW_TABLES_SIZING_ID]
#define  OFC_GROUP_ENTRY_POOLID     OFCMemPoolIds[MAX_OFC_FSOFCGROUPTABLE_SIZING_ID]
#define  OFC_FLOW_REM_MSG_POOLID    OFCMemPoolIds[MAX_OFC_FLOW_REM_MSG_SIZING_ID]
#define  OFC_METER_BAND_POOLID      OFCMemPoolIds[MAX_OFC_METER_BAND_SIZING_ID]
#define  OFC_PORT_STATUS_POOLID     OFCMemPoolIds[MAX_OFC_PORT_STATUS_SIZING_ID]
#define  OFC_BUFFER_POOLID          OFCMemPoolIds[MAX_OFC_BUFFER_SIZING_ID]
#define  OFC_DESC_STATS_POOLID      OFCMemPoolIds[MAX_OFC_DESC_STATS_SIZING_ID]
#define  OFC_EXACT_ENTRY_POOLID     OFCMemPoolIds[MAX_OFC_EXACT_ENTRY_SIZING_ID]
#define  OFC_OUT_PORT_POOLID        OFCMemPoolIds[MAX_OFC_OUT_PORT_SIZING_ID]
#ifdef DPA_WANTED
#define  OFC_DPA_PKT_POOLID         OFCMemPoolIds[MAX_OFC_DPA_PKT_SIZING_ID]
#endif /* DPA_WANTED */

#define  OFC_DEFAULT_CONTEXT             0
#define  OFC_VERSION_V100                1
#define  OFC_VERSION_V131                2
#define  OFC_FLOW_TYPE_EXACT_N_WILD      1
#define  OFC_FLOW_TYPE_WILD              2

#define  OFC_TASK_PRIORITY               100
#define  OFC_TASK_NAME                   "OFCL"
#define  OFC_QUEUE_NAME                  (const UINT1 *) "OFCQ"
#define  OFC_MUT_EXCL_SEM_NAME           (const UINT1 *) "OFCM"
#define  OFC_QUEUE_DEPTH                 100
#define  OFC_SEM_CREATE_INIT_CNT         1

#define  OFC_ETH_ADDR_LEN                6
#define  OFC_MAX_PORT_NAME_LEN           16
#define  OFC_MAX_ACTION_STR_LEN          128
#define  OFC_MAX_SETFIELDS               40
#define  OFC_MAX_TABLES                  30
#define  OFC_MAX_FLOWS                   1000
#define  OFC_MAX_GROUPS                  100
#define  OFC_MAX_METERS                  100
#define  OFC_MAX_BUCKETS                 100
#define  OFC_MAX_ACTIONS                 17
#define  OFC_MAX_INSTRUCTIONS            7
#define  OFC_ENABLED                     1
#define  OFC_DISABLED                    2

#define  OFC_ADD 0
#define  OFC_DELETE 1
#define  OFC_MODIFY 2

#define OFC_INVALID_PORT                 0
#define OFC131_INVALID_PORT              0
#define OFC_INVALID_GROUP                -1
#define OFC_INVALID_METER                -1

#define  OFC_MAX_IP_LENGTH               20
/* EVENTS */
#define  OFC_TIMER_EVENT                 0x00000001 
#define  OFC_QUEUE_EVENT                 0x00000002
#define  OFC_DPA_FP_EVENT          0x00000004
#define  OFC_DPA_PKT_EVENT         0x00000008
/* MESG TYPES */
#define  OFC_READ_FD                     0x00000004
#define  OFC_WRITE_FD                    0x00000008
#define  OFC_PORT_STATUS_SEND            0x00000010
#define  OFC_VLAN_CREATE                 0x00000020
#define  OFC_VLAN_DELETE                 0x00000040
#define  OFC_VLAN_ADD_MEMBER             0x00000080
#define  OFC_PIPELINE_NOTIFY_SEND        0x00000100
#define  OFC_MAX_CONTROLLERS             6
#define  OFC_MAX_AUX_CONNECTIONS         4
#define  OFC_CONNECTED                   1
#define  OFC_NOT_CONNECTED               2
#define  OFC_CONNECT_INPROGRESS          3
#define  OFC_MAX_CONN_RETRIES            3
#define  OFC_CONTROLLER_INTERVAL         60
#define  OFC_ECHOREQ_INTERVAL            120 
#define  OFC_ECHOINTER_INTERVAL          120
#define  OFC_MAX_CONTROLLER_PKT_LEN      2000
#define  OFC_MAX_DATA_FRAME_SIZE         3000
#define  OFC_MAX_IP_ADDRESS_ARRAY_LENGTH 128
#define  OFC_SWITCHMODE_FAIL_SECURE      1 
#define  OFC_SWITCHMODE_FAIL_STANDALONE  2 
#define  OFC_CONNPROTO_TCP               1
#define  OFC_CONNPROTO_SSL               2
#define  OFC_CONNROLE_EQUAL              1
#define  OFC_CONNROLE_MASTER             2
#define  OFC_CONNROLE_SLAVE              3
#define  OFC_GET_OFFSET(x,y)             FSAP_OFFSETOF(x,y)
#endif  /* __OFCDEFN_H__*/

/*-----------------------------------------------------------------------*/
/*                       End of the file ofcdefn.h                      */
/*-----------------------------------------------------------------------*/

