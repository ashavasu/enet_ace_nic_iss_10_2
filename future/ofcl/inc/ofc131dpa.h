/*****************************************************************************
 * Copyright (C) 2014 Aricent Inc . All Rights Reserved
 *
 *  $Id: ofc131dpa.h,v 1.1 2014/08/14 11:15:53 siva Exp $
 *
 * Description: This file contains declarations for DPA processing 
 *****************************************************************************/
#ifndef INCLUDE_OFC131DPA_H
#define INCLUDE_OFC131DPA_H 

#include "ofcmpmsg.h"
#include "ofdpa_datatypes.h"
#include "ofdpa_api.h"

#define OFCDPA_MAX_TABLES 13

VOID Ofc131DpaEventSockProcess (INT4);
VOID Ofc131DpaPktSockProcess (INT4);
VOID Ofc131DpaEventProcess (VOID);
VOID Ofc131DpaPktProcess (VOID);

UINT1 Ofc131DpaPacketSend (tCRU_BUF_CHAIN_HEADER *pBuf, UINT4 u4InPort,
                           UINT4 u4OutPort, UINT4 u4PktSize);
UINT1 Ofc131DpaFillPortDesc (UINT4 u4Port, tOfp131PortDesc *pPortDesc);
UINT1 Ofc131DpaPortDescGet (tOfcPortDescReply *pPortDescReply, UINT4 *pu4VarLen);
UINT1 Ofc131DpaPortUpdate (UINT4 u4PortNum, UINT1 u1PortState, UINT4 u4AdvFeat);
UINT1 Ofc131DpaPortStatsGet (UINT4 u4Port, tOfcPortStatsReply *pPortStatsReply,
                             UINT4 *pu4VarLen);
UINT1 Ofc131DpaQueueConfigGet (UINT4 u4Port, tOfp131QueueRply *pQueReply,
                               UINT4 *pu4VarLen);
UINT1 Ofc131DpaQueueStatsGet (UINT4 u4Port, UINT4 u4QueueId,
                              tOfcQueStatsReply *pQueStatsReply,
                              UINT4 *pu4VarLen);

#endif /* INCLUDE_OFC131DPA_H */
