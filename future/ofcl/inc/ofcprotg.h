/********************************************************************
* Copyright (C) 2013 Aricent Inc . All Rights Reserved
*
 Aricent Inc . All Rights Reserved
*
* $Id: ofcprotg.h,v 1.4 2014/04/30 10:10:26 siva Exp $
*
* This file contains prototypes for functions defined in Ofc.
*********************************************************************/


#include "cli.h"

INT4 OfcCliSetFsofcCfgTable (tCliHandle CliHandle,tOfcFsofcCfgEntry * pOfcSetFsofcCfgEntry,tOfcIsSetFsofcCfgEntry * pOfcIsSetFsofcCfgEntry);

INT4 OfcCliSetFsofcControllerConnTable (tCliHandle CliHandle,tOfcFsofcControllerConnEntry * pOfcSetFsofcControllerConnEntry,tOfcIsSetFsofcControllerConnEntry * pOfcIsSetFsofcControllerConnEntry);

INT4 OfcCliSetFsofcIfTable (tCliHandle CliHandle,tOfcFsofcIfEntry * pOfcSetFsofcIfEntry,tOfcIsSetFsofcIfEntry * pOfcIsSetFsofcIfEntry);


PUBLIC INT4 OfcUtlCreateRBTree PROTO ((VOID));

PUBLIC INT4 OfcLock (VOID);

PUBLIC INT4 OfcUnLock (VOID);
PUBLIC INT4 FsofcCfgTableRBCmp PROTO ((tRBElem *, tRBElem *));

PUBLIC INT4 OfcFsofcCfgTableCreate PROTO ((VOID));

tOfcFsofcCfgEntry * OfcFsofcCfgTableCreateApi PROTO ((tOfcFsofcCfgEntry *));

PUBLIC INT4 FsofcControllerConnTableRBCmp PROTO ((tRBElem *, tRBElem *));

PUBLIC INT4 OfcFsofcControllerConnTableCreate PROTO ((VOID));

tOfcFsofcControllerConnEntry * OfcFsofcControllerConnTableCreateApi PROTO ((tOfcFsofcControllerConnEntry *));

PUBLIC INT4 FsofcIfTableRBCmp PROTO ((tRBElem *, tRBElem *));

PUBLIC INT4 OfcFsofcIfTableCreate PROTO ((VOID));

tOfcFsofcIfEntry * OfcFsofcIfTableCreateApi PROTO ((tOfcFsofcIfEntry *));

PUBLIC INT4 FsofcFlowTableRBCmp PROTO ((tRBElem *, tRBElem *));

PUBLIC INT4 OfcFsofcFlowTableCreate PROTO ((VOID));

tOfcFsofcFlowEntry * OfcFsofcFlowTableCreateApi PROTO ((tOfcFsofcFlowEntry *));

PUBLIC INT4 FsofcGroupTableRBCmp PROTO ((tRBElem *, tRBElem *));

PUBLIC INT4 OfcFsofcGroupTableCreate PROTO ((VOID));

tOfcFsofcGroupEntry * OfcFsofcGroupTableCreateApi PROTO ((tOfcFsofcGroupEntry *));

PUBLIC INT4 FsofcMeterTableRBCmp PROTO ((tRBElem *, tRBElem *));

PUBLIC INT4 OfcFsofcMeterTableCreate PROTO ((VOID));

PUBLIC INT4 FsofcFTableRBCmp PROTO ((tRBElem *, tRBElem *));

PUBLIC INT4 OfcFsofcFTableCreate PROTO ((tOfcFsofcCfgEntry *));

tOfcFsofcMeterEntry * OfcFsofcMeterTableCreateApi PROTO ((tOfcFsofcMeterEntry *));
INT4 OfcGetAllFsofcCfgTable PROTO ((tOfcFsofcCfgEntry *));
INT4 OfcGetAllUtlFsofcCfgTable PROTO((tOfcFsofcCfgEntry *, tOfcFsofcCfgEntry *));
INT4 OfcGetAllFsofcControllerConnTable PROTO ((tOfcFsofcControllerConnEntry *));
INT4 OfcGetAllUtlFsofcControllerConnTable PROTO((tOfcFsofcControllerConnEntry *, tOfcFsofcControllerConnEntry *));
INT4 OfcGetAllFsofcIfTable PROTO ((tOfcFsofcIfEntry *));
INT4 OfcGetAllUtlFsofcIfTable PROTO((tOfcFsofcIfEntry *, tOfcFsofcIfEntry *));
INT4 OfcGetAllFsofcFlowTable PROTO ((tOfcFsofcFlowEntry *));
INT4 OfcGetAllUtlFsofcFlowTable PROTO((tOfcFsofcFlowEntry *, tOfcFsofcFlowEntry *));
INT4 OfcGetAllFsofcGroupTable PROTO ((tOfcFsofcGroupEntry *));
INT4 OfcGetAllUtlFsofcGroupTable PROTO((tOfcFsofcGroupEntry *, tOfcFsofcGroupEntry *));
INT4 OfcGetAllFsofcMeterTable PROTO ((tOfcFsofcMeterEntry *));
INT4 OfcGetAllUtlFsofcMeterTable PROTO((tOfcFsofcMeterEntry *, tOfcFsofcMeterEntry *));
INT4 OfcSetAllFsofcCfgTable PROTO ((tOfcFsofcCfgEntry *, tOfcIsSetFsofcCfgEntry *, INT4 , INT4 ));
INT4 OfcSetAllFsofcControllerConnTable PROTO ((tOfcFsofcControllerConnEntry *, tOfcIsSetFsofcControllerConnEntry *, INT4 , INT4 ));

INT4 OfcSetAllFsofcIfTable PROTO ((tOfcFsofcIfEntry *, tOfcIsSetFsofcIfEntry *));
INT4 OfcInitializeFsofcCfgTable PROTO ((tOfcFsofcCfgEntry *));

INT4 OfcInitializeMibFsofcCfgTable PROTO ((tOfcFsofcCfgEntry *));

INT4 OfcInitializeFsofcControllerConnTable PROTO ((tOfcFsofcControllerConnEntry *));

INT4 OfcInitializeMibFsofcControllerConnTable PROTO ((tOfcFsofcControllerConnEntry *));
INT4 OfcTestAllFsofcCfgTable PROTO ((UINT4 *, tOfcFsofcCfgEntry *, tOfcIsSetFsofcCfgEntry *, INT4 , INT4));
INT4 OfcTestAllFsofcControllerConnTable PROTO ((UINT4 *, tOfcFsofcControllerConnEntry *, tOfcIsSetFsofcControllerConnEntry *, INT4 , INT4));
INT4 OfcTestAllFsofcIfTable PROTO ((UINT4 *, tOfcFsofcIfEntry *, tOfcIsSetFsofcIfEntry *));
tOfcFsofcCfgEntry * OfcGetFsofcCfgTable PROTO ((tOfcFsofcCfgEntry *));
tOfcFsofcControllerConnEntry * OfcGetFsofcControllerConnTable PROTO ((tOfcFsofcControllerConnEntry *));
tOfcFsofcIfEntry * OfcGetFsofcIfTable PROTO ((tOfcFsofcIfEntry *));
tOfcFsofcFlowEntry * OfcGetFsofcFlowTable PROTO ((tOfcFsofcFlowEntry *));
tOfcFsofcGroupEntry * OfcGetFsofcGroupTable PROTO ((tOfcFsofcGroupEntry *));
tOfcFsofcMeterEntry * OfcGetFsofcMeterTable PROTO ((tOfcFsofcMeterEntry *));
tOfcFsofcCfgEntry * OfcGetFirstFsofcCfgTable PROTO ((VOID));
tOfcFsofcControllerConnEntry * OfcGetFirstFsofcControllerConnTable PROTO ((VOID));
tOfcFsofcIfEntry * OfcGetFirstFsofcIfTable PROTO ((VOID));
tOfcFsofcFlowEntry * OfcGetFirstFsofcFlowTable PROTO ((VOID));
tOfcFsofcGroupEntry * OfcGetFirstFsofcGroupTable PROTO ((VOID));
tOfcFsofcMeterEntry * OfcGetFirstFsofcMeterTable PROTO ((VOID));
tOfcFsofcCfgEntry * OfcGetNextFsofcCfgTable PROTO ((tOfcFsofcCfgEntry *));
tOfcFsofcControllerConnEntry * OfcGetNextFsofcControllerConnTable PROTO ((tOfcFsofcControllerConnEntry *));
tOfcFsofcIfEntry * OfcGetNextFsofcIfTable PROTO ((tOfcFsofcIfEntry *));
tOfcFsofcFlowEntry * OfcGetNextFsofcFlowTable PROTO ((tOfcFsofcFlowEntry *));
tOfcFsofcGroupEntry * OfcGetNextFsofcGroupTable PROTO ((tOfcFsofcGroupEntry *));
tOfcFsofcMeterEntry * OfcGetNextFsofcMeterTable PROTO ((tOfcFsofcMeterEntry *));
INT4 FsofcCfgTableFilterInputs PROTO ((tOfcFsofcCfgEntry *, tOfcFsofcCfgEntry *, tOfcIsSetFsofcCfgEntry *));
INT4 FsofcControllerConnTableFilterInputs PROTO ((tOfcFsofcControllerConnEntry *, tOfcFsofcControllerConnEntry *, tOfcIsSetFsofcControllerConnEntry *));
INT4 FsofcIfTableFilterInputs PROTO ((tOfcFsofcIfEntry *, tOfcFsofcIfEntry *, tOfcIsSetFsofcIfEntry *));
INT4 OfcUtilUpdateFsofcCfgTable PROTO ((tOfcFsofcCfgEntry *, tOfcFsofcCfgEntry *, tOfcIsSetFsofcCfgEntry *));
INT4 OfcUtilUpdateFsofcControllerConnTable PROTO ((tOfcFsofcControllerConnEntry *, tOfcFsofcControllerConnEntry *, tOfcIsSetFsofcControllerConnEntry *));
INT4 OfcUtilUpdateFsofcIfTable PROTO ((tOfcFsofcIfEntry *, tOfcFsofcIfEntry *, tOfcIsSetFsofcIfEntry *));
INT4 OfcSetAllFsofcCfgTableTrigger PROTO ((tOfcFsofcCfgEntry *, tOfcIsSetFsofcCfgEntry *, INT4));
INT4 OfcSetAllFsofcControllerConnTableTrigger PROTO ((tOfcFsofcControllerConnEntry *, tOfcIsSetFsofcControllerConnEntry *, INT4));
INT4 OfcSetAllFsofcIfTableTrigger PROTO ((tOfcFsofcIfEntry *, tOfcIsSetFsofcIfEntry *, INT4));

INT4 OfcCliShowFsofcIfTable (tCliHandle CliHandle, UINT4 *pu4FsofcContextId,INT4   *pi4FsofcIfIndex);
INT4 OfcCliShowFsofcCfgTable (tCliHandle CliHandle, UINT4 *pu4FsofcContextId);
INT4 OfcCliShowFsofcControllerConnTable (tCliHandle CliHandle, UINT4 *pu4FsofcContextId);
INT4 OfcCliShowFsofcFlowTable (tCliHandle CliHandle, UINT4 *pu4FsofcContextId, UINT4 *pu4FsofcTableIndex, UINT4 *pu4FsofcFlowIndex);
INT4 OfcCliShowFsofcGroupTable (tCliHandle CliHandle, UINT4 *pu4FsofcContextId, UINT4 *pu4FsofcGroupIndex);
INT4 OfcCliShowFsofcMeterTable (tCliHandle CliHandle, UINT4 *pu4FsofcContextId, UINT4 *pu4FsofcMeterIndex);

INT4 OfcCliMapInterfaceToContext (tCliHandle CliHandle, UINT4 u4IfIndex, UINT4 * pu4ContextID);
INT4 OfcCliUnMapInterfaceFromContext (tCliHandle CliHandle, UINT4 u4IfIndex, UINT4 * pu4ContextID);
INT4 FsOfcExactFlowEntryRBComp (tRBElem *pRBElem1, tRBElem *pRBElem2);
