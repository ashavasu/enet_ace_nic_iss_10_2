/********************************************************************
* Copyright (C) 2013 Aricent Inc . All Rights Reserved
*
* $Id: ofcmibclig.h,v 1.3 2014/01/31 13:03:56 siva Exp $
*
* Description: Extern Declaration of the Mib objects
*********************************************************************/


/* extern declaration corresponding to OID variable present in protocol db.h */

extern UINT4 FsofcContextId[13];

extern UINT4 FsofcModuleStatus[13];

extern UINT4 FsofcSupportedVersion[13];

extern UINT4 FsofcDefaultFlowMissBehaviour[13];

extern UINT4 FsofcControlPktBuffering[13];

extern UINT4 FsofcIpReassembleStatus[13];

extern UINT4 FsofcPortStpStatus[13];

extern UINT4 FsofcTraceEnable[13];

extern UINT4 FsofcSwitchModeOnConnFailure[13];

extern UINT4 FsofcSwitchEntryStatus[13];

extern UINT4 FsofcHybrid[13];

extern UINT4 FsofcControllerIpAddrType[13];

extern UINT4 FsofcControllerIpAddress[13];

extern UINT4 FsofcControllerConnAuxId[13];

extern UINT4 FsofcControllerConnPort[13];

extern UINT4 FsofcControllerConnProtocol[13];

extern UINT4 FsofcControllerRole[13];

extern UINT4 FsofcControllerConnEchoReqCount[13];

extern UINT4 FsofcControllerConnEntryStatus[13];

extern UINT4 FsofcControllerConnBand[13];

extern UINT4 FsofcIfIndex[13];

extern UINT4 FsofcVlanEgressPorts[13];

extern UINT4 FsofcVlanUntaggedPorts[13];

extern UINT4 FsofcIfContextId[13];

extern UINT4 FsofcTableIndex[13];

extern UINT4 FsofcFlowIndex[13];

extern UINT4 FsofcGroupIndex[13];

extern UINT4 FsofcMeterIndex[13];
