/*****************************************************************************/
/* Copyright (C) 2013 Aricent Inc . All Rights Reserved
 * Copyright (C) 2013 Aricent Inc . All Rights Reserved
 ******************************************************************************
 *    FILE  NAME             : ofcfproc.h
 *    PRINCIPAL AUTHOR       : Aricent Inc.
 *    SUBSYSTEM NAME         : OFC
 *    MODULE NAME            : FLOWTABLE
 *    LANGUAGE               : ANSI-C
 *    TARGET ENVIRONMENT     : Linux (Portable)
 *    DATE OF FIRST RELEASE  :
 *    DESCRIPTION            : Definition of constants and structures used 
 *                             across Flow Table/Actions and other sub modules
 *---------------------------------------------------------------------------*/

#ifndef __OFCFPROC_H__
#define __OFCFPROC_H__

#include "ofcinc.h"

/***********************************************************************/
/***********************************************************************/
/************************** VERSION 1.0.0 ******************************/
/***********************************************************************/
/***********************************************************************/

/* Ver 1.0.0. Flow wildcards. */
typedef enum {
    OFPFW_IN_PORT = 1 << 0,            /* Switch input port. */
    OFPFW_DL_VLAN = 1 << 1,            /* VLAN id. */
    OFPFW_DL_SRC = 1 << 2,             /* Ethernet source address. */
    OFPFW_DL_DST = 1 << 3,             /* Ethernet destination address. */
    OFPFW_DL_TYPE = 1 << 4,            /* Ethernet frame type. */
    OFPFW_NW_PROTO = 1 << 5,           /* IP protocol. */
    OFPFW_TP_SRC = 1 << 6,             /* TCP/UDP source port. */
    OFPFW_TP_DST = 1 << 7,             /* TCP/UDP destination port. */
        /* IP source address wildcard bit count. 0 is exact match, 1 ignores the
         * LSB, 2 ignores the 2 least-significant bits, ..., 32 and higher wildcard
         * the entire field. This is the *opposite* of the usual convention where
         * e.g. /24 indicates that 8 bits (not 24 bits) are wildcarded. */
    OFPFW_NW_SRC_SHIFT = 8,
    OFPFW_NW_SRC_BITS = 6,
    OFPFW_NW_SRC_MASK = ((1 << OFPFW_NW_SRC_BITS) - 1) << OFPFW_NW_SRC_SHIFT,
    OFPFW_NW_SRC_ALL = 32 << OFPFW_NW_SRC_SHIFT,
    OFPFW_NW_DST_SHIFT = 14,           /* IP destination address wildcard
                                        * bit count. Same format as source. */
    OFPFW_NW_DST_BITS = 6,
    OFPFW_NW_DST_MASK = ((1 << OFPFW_NW_DST_BITS) - 1) << OFPFW_NW_DST_SHIFT,
    OFPFW_NW_DST_ALL = 32 << OFPFW_NW_DST_SHIFT,
    OFPFW_DL_VLAN_PCP = 1 << 20,       /* VLAN priority. */
    OFPFW_NW_TOS = 1 << 21,            /* IP ToS (DSCP field, 6 bits). */
    OFPFW_ALL = ((1 << 22) - 1)        /* Wildcard all fields. */
}eOfcFlowWildCards;

/* Ver 1.0.0 - Why was this flow removed? */
typedef enum {
    OFPRR_IDLE_TIMEOUT = 0, /* Flow idle time exceeded idle_timeout. */
    OFPRR_HARD_TIMEOUT = 1, /* Time exceeded hard_timeout. */
    OFPRR_DELETE = 2,       /* Evicted by a DELETE flow mod. */
    OFPRR_GROUP_DELETE = 3, /* Group was removed. */
    OFPRR_ERROR_DELETE = 4  /* Failure during remove. */
}eOfcFlowRemoveReason;

/* Ver 1.0.0 - Action Header */ 
typedef struct OFC_ACTION_HEADER {
    UINT2 u2Type;
    UINT2 u2Len;
    UINT1 au1Pad[4];
}tOfcActHdr;

/* Ver 1.0.0
 * Action structure for OFCAT_OUTPUT, which sends packets out 'port'.
 * When the 'port' is the OFCP_CONTROLLER, 'max_len' indicates the max
 * number of bytes to send. A 'max_len' of zero means no bytes of the
 * packet should be sent.
 */
typedef struct OFC_ACTION_OUTPUT {
    UINT2 u2Type;
    UINT2 u2Len;
    UINT2 u2OutPort;     /* Output port. */
    UINT2 u2MaxLen;      /* Max length to send to controller. */
}tOfcActOutPort;
typedef struct OFC_ACTION_DL_ADDR {
    UINT2 u2Type;                  /* OFCAT_SET_DL_SRC/DST. */
    UINT2 u2Len;                   /* Length is 16. */
    UINT1 au1DlAddr[6]; /* Ethernet address. */
    UINT1 au1Pad[6];
}tOfcActDlAddr;
/* Union of all actions */
typedef union OFC_ACTIONS {
    tOfcActHdr   ActHdr;
    tOfcActOutPort  ActOutport;
    tOfcActDlAddr   ActDlAddr;
}unOfcActs;

typedef struct OFCFLOWMATCH
{
    UINT4 u4WildCard;
    UINT4 u4InIfIndex;
    UINT1 au1SrcMacAddr[6];
    UINT1 au1DstMacAddr[6];
    UINT2 u2EthType;
    UINT1 u1IpProto;
    UINT1 u1Pad[1];
    UINT4 u4IpSrc;
    UINT4 u4IpDst;
    tIp6Addr au1Ip6Src;
    tIp6Addr au1Ip6Dst;
    UINT2 u2TpSrcPort;
    UINT2 u2TpDstPort;
}tOfcFlowMatch;

typedef struct OFCFLOWENTRY
{
    tTMO_HASH_NODE  node;
    tOfcFlowMatch   key;
    tOfcTmr         OfcHardTimer;
    tOfcTmr         OfcIdleTimer;
    UINT4           u4FlowIndex;

    FS_UINT8        u8Cookie;  /* Opaque controller-issued identifier. */
    FS_UINT8        u8CookieMask;
                               /* Mask used to restrict the cookie bits
                                * that must match when the command is
                                * OFPFC_MODIFY* or OFPFC_DELETE*. A value
                                * of 0 indicates no restriction. */
    UINT1 u1TableId;           /* ID of the table to put the flow in.
                                * For OFPFC_DELETE_* commands, OFPTT_ALL
                                * can also be used to delete matching
                                * flows from all tables. */
    UINT1  u1Pad[3];
    UINT2  u2Command;
    UINT1  u1Pad1[2];

    /* Statistics. */
    UINT4  u4Used;             /* Last used time, in monotonic msecs. */
    UINT4  u4PktCount;         /* Number of packets matched. */
    UINT4  u4ByteCount;        /* Number of bytes matched. */

    UINT2  u2Flags;            /* Bitwise-OR of flags values. */
    UINT2  u2Priority;
    UINT2  u2IdleTimeout;      /* Idle time before discarding (seconds). */
    UINT2  u2HardTimeout;      /* Max time before discarding (seconds). */

    UINT4  u4BufId;            /* Buffered packet to apply to, or
                                * OFP_NO_BUFFER.
                                * Not meaningful for OFPFC_DELETE*. */
    UINT4  u4OutPort;          /* For OFPFC_DELETE* commands, require
                                * matching entries to include this as an
                                * output port. A value of OFPP_ANY
                                * indicates no restriction. */

    /* Actions. */
    UINT4     u4NumActs;       /* Number of Instructions */
    unOfcActs OfcActs[OFC_MAX_ACTIONS];       /* Instruction set */
}tOfcFlowEntry;

/***********************************************************************/
/***********************************************************************/
/************************** VERSION 1.3.1 ******************************/
/***********************************************************************/
/***********************************************************************/
typedef enum{
    OFC_SEND_FLOW_REM = 1<<0,
    OFC_CHECK_OVERLAP = 1<<1,        
    OFC_RESET_COUNTS  = 1<<2,
    OFC_NO_PKT_COUNTS = 1<<3,
    OFC_NO_BYT_COUNTS = 1<<4
}eOfc131FlowModFlags;

/* Table numbering. Tables can use any number up to OFPT_MAX. */
enum OFP_TABLE {
    OFPTT_MAX = 0xfe, /* Last usable table number. */

    /* Fake tables. */
    OFPTT_ALL = 0xff /* Wildcard table used for table config,
                        flow stats and flow deletes. */
};

/* Ver 1.3.1 Exact Flow Match structure for non-TCAM Chipsets */
typedef struct OFCEXACTFLOWMATCH
{
    UINT4    u4InIfIndex;  
    tIp6Addr au1Ip6Src;
    tIp6Addr au1Ip6Dst;
    UINT1 au1SrcMacAddr[6];
    UINT1 au1DstMacAddr[6];
    UINT4 u4Ip4Src;
    UINT4 u4Ip4Dst;
    UINT4 u4InPort;
    UINT2 u2EthType;
    UINT2 u2TpSrc;
    UINT2 u2TpDst;
    UINT1 u1IpProto;
    UINT1 au1pad[1];
}tOfcExactFlowMatch;

/* Ver 1.3.1 Exact Flow Match Entry structure for non-TCAM Chipsets */
typedef struct OFCEXACTFLOWENTRY
{
    tTMO_DLL_NODE       Node;
    tOfcExactFlowMatch  OfcFlowMatch;
    tOfcTmr         	OfcIdleTimer;        /* timer structure for Idel time out field */
    FS_UINT8        	u8PktCount;          /* Number of packets matched. */
    FS_UINT8        	u8ByteCount;         /* Number of bytes matched. */
    UINT2           	u2IdleTimeout;       /* Idle time before discarding (seconds). */
    UINT1           	au1pad[2];
}tOfcExactFlowEntry;
#endif  /* __OFCFPROC_H__  */
/*-----------------------------------------------------------------------*/
/*                       End of the file  ofcfproc.h                     */
/*-----------------------------------------------------------------------*/

