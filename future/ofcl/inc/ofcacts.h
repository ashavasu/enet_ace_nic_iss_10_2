/********************************************************************
 * Copyright (C) 2013 Aricent Inc . All Rights Reserved
 *
 * $Id: ofcacts.h,v 1.4 2015/02/27 10:54:25 siva Exp $
 *
 * Description: Related Protocol Structures and Macros
 **********************************************************************/

#ifndef __OFCACTS_H__
#define __OFCACTS_H__

#include "lr.h"
#include "ofcl.h"
#include "ofcfproc.h"
#include "ip.h"

/* CONTROLLER RELATED MACROS */
#define OFC_FLOOD           0xfffb
#define OFC_CONTROLLER      0xfffd
#define OFC_NO_BUFFER       0xffffffff

/* PROTOCOL RELATED MACROS */
#define IP_FRAG_OFF_MASK    0x1fff
#define NH_ICMP6            58     /* ICMP6 header */

/* PACKET-IN REASON */
typedef enum OFC_PACKET_IN_REASON {
    OFCR_NO_MATCH, /* No matching flow. */
    OFCR_ACTION    /* Action explicitly output to controller. */
}enOfcPktInReason;

/* ARP HEADER */
typedef struct
{
    UINT2 u2HdwrType;  /* Hardware Type */
    UINT2 u2ProtoType; /* Protocol Type */
    UINT1 u1HdwrSize;  /* Hardware Size */
    UINT1 u1ProtoSize; /* Protocol Size */
    UINT2 u2OpCode;    /* Request/Response */
}tArpHdr;

/* TCP HEADER */ 
typedef struct
{
    UINT2 u2Src;
    UINT2 u2Dst;
    UINT4 u4Seq;
    UINT4 u4Ack;
    UINT2 u2Ctl;
    UINT2 u2WinSz;
    UINT2 u2ChkSum;
    UINT2 u2Urg;
}tTcpHdr;

/* UDP HEADER */
typedef struct
{
    UINT2 u2Src;
    UINT2 u2Dst;
    UINT2 u2Len;
    UINT2 u2ChkSum;
}tUdpHdr;

/* ICMP HEADER */ 
typedef struct
{
    UINT1 u1Type;
    UINT1 u1Code;
    UINT2 u2ChkSum;
}tIcmpHdr;

INT4 OfcPipelineProcess(UINT4 u4IfIndex, UINT4 u4ContextId, tCRU_BUF_CHAIN_HEADER *pBuf);
INT4 OfcParseStructToStr(CHR1 *pStr, tOfcFlowEntry *pFlowEntry);
INT4 OfcSendPktIn(UINT4 u4IfIndex, UINT4 u4MsgType, tCRU_BUF_CHAIN_HEADER *pBuf,
                  tOfcFsofcControllerConnEntry *pOfcFsofcControllerConnEntry);
INT4 Ofc131ActsSendPktIn(UINT4 u4IfIndex, UINT4 u4LogicPort, UINT4 u4MsgType,
                         FS_UINT8 u8Cookie, UINT1 u1TableId,
                         tOfp131Match *pOfcMatch, tCRU_BUF_CHAIN_HEADER *pPkt,
                         tOfcFsofcControllerConnEntry *pOfcFsofcControllerConnEntry);
PUBLIC VOID
Ofc131ActsComputeNumBitsSet(FS_UINT8 u8Bitmap, UINT1 *pu1NumBits);

PUBLIC UINT4
Ofc131ActsApplyActionList (tCRU_BUF_CHAIN_HEADER *pPkt, tOfcSll *pActList,
                            INT4 *pi4GroupId, tOfcSll *pOutput);


#endif  /* __OFCACTS_H__  */
