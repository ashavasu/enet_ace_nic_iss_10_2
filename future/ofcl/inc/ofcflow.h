/*****************************************************************************
 * Copyright (C) 2013 Aricent Inc . All Rights Reserved
 *****************************************************************************
 * $Id: ofcflow.h,v 1.9 2018/01/11 11:18:34 siva Exp $
 *
 *    FILE  NAME             : ofcflow.h
 *    PRINCIPAL AUTHOR       : Aricent Inc.
 *    SUBSYSTEM NAME         : OFC
 *    MODULE NAME            : FLOWTABLE
 *    LANGUAGE               : ANSI-C
 *    TARGET ENVIRONMENT     : Linux (Portable)
 *    DATE OF FIRST RELEASE  :
 *    DESCRIPTION            : Definition of constants and structures used 
 *                             across Flow Table and other sub modules
 ******************************************************************************/

#ifndef __OFCFLOW_H__
#define __OFCFLOW_H__

#include "ofcfproc.h"
#include "ofcpkt.h"

#define OFC131_INVALID_FLOWMOD_FLAGS  0xFFE0
#define OFC131_INVALID_METERMOD_FLAGS 0xFFF0

#define OFC_LOWEST_FILTER_PRIORITY  1
#define OFC_DEFAULT_FILTER_PRIORITY 10
#define OFC_FLOW_INDEX_START        0

#define OFC_NO_RESET            0
#define OFC_RESET_WITH_STATS    1
#define OFC_RESET_WITHOUT_STATS 2

#define OFC_L4PROTO_TCP    6
#define OFC_L4PROTO_UDP    17
#define OFC_L4PROTO_ICMP   1
#define OFC_L4PROTO_SCTP   132
#define OFC_L4PROTO_ICMP6  58

#define OFC_IP6EXT_HOP_HDR        0
#define OFC_IP6EXT_ROUTE_HDR     43 
#define OFC_IP6EXT_FRAG_HDR      44
#define OFC_IP6EXT_ESP_HDR       50 
#define OFC_IP6EXT_AUTH_HDR      51
#define OFC_IP6EXT_NO_NEXT_HDR   59 
#define OFC_IP6EXT_DEST_HDR      60
#define OFC_IP6EXT_MOBILITY_HDR  62

#define OFC_L2EXT_CVLAN     0x8100
#define OFC_L2EXT_SVLAN     0x88a8
#define OFC_L2EXT_MPLS_1    0x8847
#define OFC_L2EXT_MPLS_2    0x8848
#define OFC_DISCOVERY       0x8942
#define OFC_LLDP            0x88cc
#define OFC_L2EXT_PBB       0x88e7

#define OFC_MAX_HDR_LEN          512
#define OFC_ETH_HDR_LEN           14
#define OFC_ETH_SRC_OFFSET         6
#define OFC_ETHTYPE_OFFSET        12
#define OFC_ETHTYPE_SIZE           2
#define OFC_ETH_ADDR_LEN           6
#define OFC_VLAN_TAG_OFFSET       12
#define OFC_VLAN_PROTO_SIZE        2
#define OFC_VLAN_TAG_LEN           4
#define OFC_ISID_TAGGED_HDR_SIZE  18
#define OFC_MAX_IPV4_HDR_SIZE     60
#define OFC_IPV4_PROTO_OFFSET      9
#define OFC_IPV4_CHKSUM_OFFSET    10
#define OFC_IPV4_SRC_OFFSET       12
#define OFC_IPV4_DST_OFFSET       16
#define OFC_IPV4_ADDR_LEN          4
#define OFC_IPV4_WORD_SIZE         4
#define OFC_IPV4_TTL_OFFSET        8
#define OFC_IPV4_DSCP_OFFSET       1

#define OFC_IPV6_PROTO_OFFSET      6
#define OFC_IPV6_HDR_SIZE         40
#define OFC_IPV6_SRC_OFFSET        8
#define OFC_IPV6_DST_OFFSET       24
#define OFC_IPV6_ADDR_LEN         16
#define OFC_IPV6_TTL_OFFSET        7

#define OFC_MPLS_TTL_OFFSET            3
#define OFC_MPLS_LABEL16_TC_BOS_OFFSET 2

#define OFC_L4_DST_OFFSET          2
#define OFC_L4_ADDR_LEN            2

#define OFC_ARP_OP_OFFSET          6
#define OFC_ARP_HTYPE_OFFSET       0
#define OFC_ARP_PTYPE_OFFSET       2
#define OFC_ARP_SHA_OFFSET         8
#define OFC_ARP_THA_OFFSET        18
#define OFC_ARP_SPA_OFFSET        14
#define OFC_ARP_TPA_OFFSET        24
#define OFC_ARP_OP_LEN             2
#define OFC_ARP_PA_LEN             4
#define OFC_ARP_HA_LEN             6

#define OFC_ND_RS_OPTION_OFFSET    8
#define OFC_ND_RA_OPTION_OFFSET   16
#define OFC_ND_NS_OPTION_OFFSET   24
#define OFC_ND_NA_OPTION_OFFSET   24
#define OFC_ND_RD_OPTION_OFFSET   40
#define OFC_ND_TARGET_OFFSET       8

#define OFC_ICMP4_CODE_OFFSET      1
#define OFC_ICMP6_CODE_OFFSET      1

#define OFC_PBB_ISID_OFFSET        2
#define OFC_PBB_ISID_SIZE          4


#define OFC_VLAN_VID_UNMASK       0xF000
#define OFC_VLAN_PCP_UNMASK       0x1F
#define OFC_IPV4_IHL_UNMASK       0x0F
#define OFC_IPV4_DSCP_UNMASK      0x03
#define OFC_IPV4_ECN_UNMASK       0xFC
#define OFC_IPV6_DSCP_UNMASK      0xF03F
#define OFC_IPV6_ECN_UNMASK       0xFFCF
#define OFC_IPV6_FLABEL_UNMASK    0xFFF00000
#define OFC_MPLS_LABEL_UNMASK     0x000000FF
#define OFC_MPLS_TC_UNMASK        0xF1
#define OFC_MPLS_BOS_UNMASK       0xFE
#define OFC_PBB_ISID_UNMASK       0xFF000000

#define OFC_MPLS_BOS_CHCK_MASK  0x01 

#define OFC_PK_VLAN_VID_MASK     0x0FFF
#define OFC_PK_IPV6_DSCP_MASK    0x3F
#define OFC_PK_IPV4_ECN_MASK     0x03
#define OFC_PK_IPV6_ECN_MASK     0x03
#define OFC_PK_IPV6_FLABEL_MASK  0x0FFFFF 
#define OFC_PK_MPLS_TC_MASK      0x0E
#define OFC_PK_MPLS_BOS_MASK     0x01
#define OFC_PK_PBB_ISID_MASK     0xFFFFFF



#define OFC_L3_IP4    0x800
#define OFC_L3_IP6    0x86DD
#define OFC_L3_ARP    0x806
#define OFC_L3_ND     0x809

/* Defined only for OFC internal usage */
#define OFC_IP_PROTO        4
#define OFC_MPLS_PROTO      2
#define OFC_VLAN_PROTO      3

#define OFC_IP_HEADER_VER_MASK        0xf0
#define OFC_IP_HDR_VER_OFFSET_BITS    4

#define ROUTER_SOLICITATION                 133
#define ROUTER_ADVERTISEMENT                134
#define NEIGHBOR_SOLICITATION               135
#define NEIGHBOR_ADVERTISEMENT              136
#define REDIRECT                            137
#define ND_SLL                                1
#define ND_TLL                                2

#define FLOW_N_REGS   8
#define FLOW_SIG_SIZE 64 
#define OFC_MAX_PROTO_HDRS_LEN  128

#define OFCFLOW_HASH_INDEX(Index)  Index % OFC_MAX_FLOW_ENTRIES

/* Bit-masks for u2Flags field of tTmrAppTimer */
enum OFC_TIMERS
{
    OFC_TMR_RUNNING = 0x1,
    OFC_TMR_EXPD = 0x2
};

enum OFC_FLOW_MOD_FLAGS {
    OFPFF_SEND_FLOW_REM = 1 << 0, /* Send flow removed message when flow
                                     expires or is deleted. */
    OFPFF_CHECK_OVERLAP = 1 << 1, /* Check for overlapping entries first. */
    OFPFF_RESET_COUNTS = 1 << 2, /* Reset flow packet and byte counts. */
    OFPFF_NO_PKT_COUNTS = 1 << 3, /* Don't keep track of packet count. */
    OFPFF_NO_BYT_COUNTS = 1 << 4, /* Don't keep track of byte count. */
};

enum OFC_FLOW_MOD_CMD {
    OFPFC_ADD,              /* New flow. */
    OFPFC_MODIFY,           /* Modify all matching flows. */
    OFPFC_MODIFY_STRICT,    /* Modify entry strictly matching wildcards */
    OFPFC_DELETE,           /* Delete all matching flows. */
    OFPFC_DELETE_STRICT,    /* Strictly match wildcards and priority. */
    OFPFC_GET               /* New flow. */
};

/* Values for 'type' in ofp_error_message. These values are immutable: they
* will not change in future versions of the protocol (although new values may
* be added). */
enum OFC_ERROR_TYPE {
    OFPET_HELLO_FAILED, /* Hello protocol failed. */
    OFPET_BAD_REQUEST, /* Request was not understood. */
    OFPET_BAD_ACTION, /* Error in action description. */
    OFPET_FLOW_MOD_FAILED, /* Problem modifying flow entry. */
    OFPET_PORT_MOD_FAILED, /* Port mod request failed. */
    OFPET_QUEUE_OP_FAILED, /* Queue operation failed. */
    OFPET_ROLE_REQUEST_FAILED
};

/* ofp_error_msg 'code' values for OFPET_BAD_REQUEST. 'data' contains at least
 * * the first 64 bytes of the failed request. */
enum OFPET_BAD_REQUEST_CODE {
    OFPBRC_BAD_VERSION, /* ofp_header.version not supported. */
    OFPBRC_BAD_TYPE, /* ofp_header.type not supported. */
    OFPBRC_BAD_STAT, /* ofp_stats_request.type not supported. */
    OFPBRC_BAD_VENDOR, /* Vendor not supported (in ofp_vendor_heade  * or ofp_stats_request or ofp_stats_reply). */
    OFPBRC_BAD_SUBTYPE, /* Vendor subtype not supported. */
    OFPBRC_EPERM, /* Permissions error. */
    OFPBRC_BAD_LEN, /* Wrong request length for type. */
    OFPBRC_BUFFER_EMPTY, /* Specified buffer has already been used. */
    OFPBRC_BUFFER_UNKNOWN, /* Specified buffer does not exist. */
    OFPBRC_BAD_TABLE_ID, /* Specified table-id invalid or does not exist. */
    OFPBRC_IS_SLAVE, /* Denied because controller is slave. */
};

/* ofp_error_msg 'code' values for OFPET_FLOW_MOD_FAILED. 'data' contains
* at least the first 64 bytes of the failed request. */
enum OFC_FLOW_MOD_FAILED_CODE {
    OFPFMFC_TABLE_FULL, /* Flow not added because table was full. */
    OFPFMFC_OVERLAP, /* Attempted to add overlapping flow with
                        CHECK_OVERLAP flag set. */
    OFPFMFC_EPERM, /* Permissions error. */
    OFPFMFC_BAD_TIMEOUT, /* Flow not added because of unsupported
                            idle/hard timeout. */
    OFPFMFC_BAD_COMMAND, /* Unsupported or unknown command. */
    OFPFMFC_UNSUPPORTED, /* Unspecified error. */
    OFPFMFC_UNKNOWN      /* Unspecified error. */
};

/*********************************************************************/
/***************** VER 1.0.0 FUNCTION PROTOTYPES *********************/
/*********************************************************************/
INT4 
OfcFlowHandleFlowModMsgs(tOfcFsofcCfgEntry *pOfc, tOfcFlowEntry *pFlowMsg);
PUBLIC VOID 
OfcFlowMatchToKey(tOfpMatch *pOfpMatch, tOfcFlowMatch *pKey);
PUBLIC 
VOID OfcKeyToFlowMatch(tOfcFlowMatch *pKey, tOfpMatch *pOfpMatch);
INT4 
OfcFlowDeleteFlow(tOfcFsofcCfgEntry *pOfc, tOfcFlowEntry *pFlowMsg, UINT1 u1Cmd, UINT1 u1Flag);

/*HASH LOOKUP  FUNCTIONS*/
tOfcFlowEntry * OfcFlowLookup(tOfcFsofcCfgEntry *pOfc, tOfcFlowMatch *pKey);



/*********************************************************************/
/***************** VER 1.3.1 DATA STRUCTURES *************************/
/*********************************************************************/

enum OFC_131_FLOW_MOD_FAILED_CODE {
    OFPFMFC_131_UNKNOWN = 0, /* Unspecified error. */
    OFPFMFC_131_TABLE_FULL = 1, /* Flow not added because table was full. */
    OFPFMFC_131_BAD_TABLE_ID = 2, /* Table does not exist */
    OFPFMFC_131_OVERLAP = 3, /* Attempted to add overlapping flow with
                            CHECK_OVERLAP flag set. */
    OFPFMFC_131_EPERM = 4, /* Permissions error. */
    OFPFMFC_131_BAD_TIMEOUT = 5, /* Flow not added because of unsupported
                                idle/hard timeout. */
    OFPFMFC_131_BAD_COMMAND = 6, /* Unsupported or unknown command. */
    OFPFMFC_131_BAD_FLAGS = 7, /* Unsupported or unknown flags. */
};

enum OFC_131_ERROR_TYPE {
    OFPET_131_HELLO_FAILED = 0, /* Hello protocol failed. */
    OFPET_131_BAD_REQUEST = 1, /* Request was not understood. */
    OFPET_131_BAD_ACTION = 2, /* Error in action description. */
    OFPET_131_BAD_INSTRUCTION = 3, /* Error in instruction list. */
    OFPET_131_BAD_MATCH = 4, /* Error in match. */
    OFPET_131_FLOW_MOD_FAILED = 5, /* Problem modifying flow entry. */
    OFPET_131_GROUP_MOD_FAILED = 6, /* Problem modifying group entry. */
    OFPET_131_PORT_MOD_FAILED = 7, /* Port mod request failed. */
    OFPET_131_TABLE_MOD_FAILED = 8, /* Table mod request failed. */
    OFPET_131_QUEUE_OP_FAILED = 9, /* Queue operation failed. */
    OFPET_131_SWITCH_CONFIG_FAILED = 10, /* Switch config request failed. */
    OFPET_131_ROLE_REQUEST_FAILED = 11, /* Controller Role request failed. */
    OFPET_131_METER_MOD_FAILED = 12, /* Error in meter. */
    OFPET_131_TABLE_FEATURES_FAILED = 13, /* Setting table features failed. */
    OFPET_131_EXPERIMENTER = 0xffff /* Experimenter error messages. */
};

typedef enum {
    OFPRRFC_131_STALE = 0,    /* Stale Message: old generation_id. */
    OFPRRFC_131_UNSUP = 1,    /* Controller role change unsupported. */
    OFPRRFC_131_BAD_ROLE = 2, /* Invalid role. */
}eOfp131RoleReqFailedCode;

enum OFC_GROUP_MOD_FAILED_CODE {
    ISS_OFPGMFC_GROUP_EXISTS = 0, /* Group not added because a group ADD  attempted to replace already-present group. */
    ISS_OFPGMFC_INVALID_GROUP = 1, /* Group not added because Group specified is invalid. */
    ISS_OFPGMFC_WEIGHT_UNSUPPORTED = 2, /* Switch does not support unequal load sharing with select groups. */
    ISS_OFPGMFC_OUT_OF_GROUPS = 3, /* The group table is full. */
    ISS_OFPGMFC_OUT_OF_BUCKETS = 4, /* The maximum number of action buckets i or a group has been exceeded. */
    ISS_OFPGMFC_CHAINING_UNSUPPORTED = 5, /* Switch does not support groups that forward to groups. */
    ISS_OFPGMFC_WATCH_UNSUPPORTED = 6, /* This group cannot watch the watch_port or watch_group specified. */
    ISS_OFPGMFC_LOOP = 7, /* Group entry would cause a loop. */  
    ISS_OFPGMFC_UNKNOWN_GROUP = 8, /* Group not modified because a group MODIFY attempted to modify a non-existent group. */ 
    ISS_OFPGMFC_CHAINED_GROUP = 9, /* Group not deleted because another  group is forwarding to it. */  
    ISS_OFPGMFC_BAD_TYPE = 10, /* Unsupported or unknown group type. */ 
    ISS_OFPGMFC_BAD_COMMAND = 11, /* Unsupported or unknown command. */
    ISS_OFPGMFC_BAD_BUCKET = 12, /* Error in bucket. */
    ISS_OFPGMFC_BAD_WATCH = 13, /* Error in watch port/group. */
    ISS_OFPGMFC_EPERM = 14, /* Permissions error. */
};

enum OFC_ACTION_ERR_CODE {
    OFPBAC_BAD_TYPE = 0, /* Unknown action type. */
    OFPBAC_BAD_LEN = 1, /* Length problem in actions. */
    OFPBAC_BAD_EXPERIMENTER = 2, /* Unknown experimenter id specified. */
    OFPBAC_BAD_EXP_TYPE = 3, /* Unknown action for experimenter id. */
    OFPBAC_BAD_OUT_PORT = 4, /* Problem validating output port. */
    OFPBAC_BAD_ARGUMENT = 5, /* Bad action argument. */
    OFPBAC_EPERM = 6, /* Permissions error. */
    OFPBAC_TOO_MANY = 7, /* Can't handle this many actions. */
    OFPBAC_BAD_QUEUE = 8, /* Problem validating output queue. */
    OFPBAC_BAD_OUT_GROUP = 9, /* Invalid group id in forward action. */
    OFPBAC_MATCH_INCONSISTENT = 10, /* Action can't apply for this match, or Set-Field missing prerequisite. */
    OFPBAC_UNSUPPORTED_ORDER = 11, /* Action order is unsupported for the action list in an Apply-Actions instruction */
    OFPBAC_BAD_TAG = 12, /* Actions uses an unsupported tag/encap. */
    OFPBAC_BAD_SET_TYPE = 13, /* Unsupported type in SET_FIELD action. */
    OFPBAC_BAD_SET_LEN = 14, /* Length problem in SET_FIELD action. */
    OFPBAC_BAD_SET_ARGUMENT = 15, /* Bad argument in SET_FIELD action. */
};

enum OFC_BAD_INSTRUCTION_CODE {
    OFPBIC_UNKNOWN_INST = 0,     /* Unknown instruction. */
    OFPBIC_UNSUP_INST = 1,       /* Switch or table does not support the instruction. */
    OFPBIC_BAD_TABLE_ID = 2,     /* Invalid Table-ID specified. */
    OFPBIC_UNSUP_METADATA = 3,   /* Metadata value unsupported by datapath. */
    OFPBIC_UNSUP_METADATA_MASK = 4, /* Metadata mask value unsupported by datapath. */
    OFPBIC_BAD_EXPERIMENTER = 5, /* Unknown experimenter id specified. */
    OFPBIC_BAD_EXP_TYPE = 6,     /* Unknown instruction for experimenter id. */
    OFPBIC_BAD_LEN = 7,          /* Length problem in instructions. */
    OFPBIC_EPERM = 8,            /* Permissions error. */
};

enum OFP_BAD_MATCH_CODE {
    OFPBMC_BAD_TYPE = 0, /* Unsupported match type specified by the match */
    OFPBMC_BAD_LEN = 1,  /* Length problem in match. */
    OFPBMC_BAD_TAG = 2,  /* Match uses an unsupported tag/encap. */
    OFPBMC_BAD_DL_ADDR_MASK = 3, /* Unsupported datalink addr mask - 
                                    switch does not support arbitrary datalink address mask. */
    OFPBMC_BAD_NW_ADDR_MASK = 4, /* Unsupported network addr mask - 
                                    switch does not support arbitrary network address mask. */
    OFPBMC_BAD_WILDCARDS = 5,    /* Unsupported combination of fields masked or omitted in the match. */
    OFPBMC_BAD_FIELD = 6,   /* Unsupported field type in the match. */
    OFPBMC_BAD_VALUE = 7,   /* Unsupported value in a match field. */
    OFPBMC_BAD_MASK = 8,    /* Unsupported mask specified in the match, field is not dl-address or nw-address. */
    OFPBMC_BAD_PREREQ = 9,  /* A prerequisite was not met. */
    OFPBMC_DUP_FIELD = 10,  /* A field type was duplicated. */
    OFPBMC_EPERM = 11,      /* Permissions error. */
};

enum OFP_PORT_MOD_FAILED_CODE {
    OFPPMFC_BAD_PORT = 0,    /* Specified port number does not exist. */
    OFPPMFC_BAD_HW_ADDR = 1, /* Specified hardware address does not
                                match the port number. */
    OFPPMFC_BAD_CONFIG = 2,  /* Specified config is invalid. */
    OFPPMFC_BAD_ADVERTISE = 3, /* Specified advertise is invalid. */
    OFPPMFC_EPERM = 4,       /* Permissions error. */
};

enum OFP_TABLE_FEATURES_FAILED_CODE {
    OFPTFFC_BAD_TABLE     = 0, /* Specified table does not exist. */
    OFPTFFC_BAD_METADATA  = 1, /* Invalid metadata mask. */
    OFPTFFC_BAD_TYPE      = 2, /* Unknown property type. */
    OFPTFFC_BAD_LEN       = 3, /* Length problem in properties. */
    OFPTFFC_BAD_ARGUMENT  = 4, /* Unsupported property value. */
    OFPTFFC_EPERM         = 5, /* Permissions error. */
};

/*********************************************************************/
/***************** VER 1.3.1 FUNCTION PROTOTYPES *********************/
/*********************************************************************/
INT4
Ofc131FlowDeleteFlow (tOfcFsofcCfgEntry *pOfc, tOfcFsofcFlowEntry *pFlow,
                      UINT1 u1FlowRemReason);
UINT4
Ofc131FlowHandleFlowModMsg (tOfcFsofcCfgEntry *pOfc, VOID *pConnPtr,
                            tOfp131FlowMod *pOfp131FlowMod, 
                            UINT4 *pu4Error);

VOID
Ofc131FlowDeleteAllFlows (tOfcFsofcCfgEntry *pOfc);
/*HASH LOOKUP  FUNCTIONS*/
UINT1
Ofc131FlowIsFlowMatched (UINT1 u1MatchCount, tOfcSll *pKeyMatchList,
                             tOfcFsofcFlowEntry *pFlow);
tOfcFsofcFlowEntry *
Ofc131FlowLookupForPipeLine(tOfcFsofcCfgEntry *pOfc, tOfcSll *pKeyMatchList,
                                UINT1 *pu1KeyExactMatch, UINT1 u1TableIndex);
VOID
Ofc131FlowMatchListFree (INT1 u1TableId, tOfcSll  *pFlowMatchList);

PUBLIC VOID
Ofc131FlowEntryCleanUp(tOfcFsofcFlowEntry *pFlow);

/* RBTree look-up funcs. for exact match */
UINT1 Ofc131FlowExactSort (tOfcSll *pMatchList, UINT1 *pu1ExactMatch);

#endif  /* __OFCFLOW_H__  */
/*-----------------------------------------------------------------------*/
/*                       End of the file  ofcflow.h                      */
/*-----------------------------------------------------------------------*/

