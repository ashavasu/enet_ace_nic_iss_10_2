/*****************************************************************************/
/* Copyright (C) 2013 Aricent Inc . All Rights Reserved
 * $Id: ofcmpmsg.h,v 1.5 2017/09/11 13:33:51 siva Exp $
 ******************************************************************************
 *    FILE  NAME             : ofcmpmsg.h
 *    PRINCIPAL AUTHOR       : Aricent Inc.
 *    SUBSYSTEM NAME         : OFC
 *    MODULE NAME            : MULTI PART MSGS
 *    LANGUAGE               : ANSI-C
 *    DATE OF FIRST RELEASE  :
 *    DESCRIPTION            : Definition of constants and structures used
 *                             across Multi Part Messages
 *---------------------------------------------------------------------------*/

#ifndef _OFCMPMSG_H_
#define _OFCMPMSG_H_

#include "ofcinc.h"
#include "ofcpkt.h"
#include "ofc131pkt.h"

/************************* MULTIPART MESSAGES *******************************/
#define OFC_DESC_STR_LEN               256
#define OFC_MAX_TABLE_NAME_LEN         32
#define OFC_SERIAL_NUM_LEN             32
#define OFPP_ANY                       0xffffffff
#define OFPG_ANY                       0xffffffff
#define OFPG_ALL                       0xfffffffc
#define OFPQ_ALL                       0xffffffff
#define OFC_FLOW_STATS_LEN             32

#define OFC_DEFAULT_QUEUE              0
#define OFC_GROUP_CAPS_BITMASK         15 
#define OFC_GROUP_TYPES_BITMASK        15 
#define OFC_GROUP_ACTS_BITMASK         268408833

#define OFC_METER_BANDTYPES              0x00000006
#define OFC_METER_CAPABILITES            0x0000000F
#define OFC_METER_MAX_BANDS              0x04
#define OFC_METER_MAX_COLOR              0x00
#define OFC_METER_MAXIMUM                0x0000FFFF

typedef enum {
    OFCMPF_REQ_MORE       = 1 << 0 /* More requests to follow */
}eOfcMultipartReqFlags;

typedef enum {
    OFCMPF_REPLY_MORE     = 1 << 0 /* More replies to follow */
}eOfcMultipartReplyFlags;

/*
 * Table Feature property types.
 * Low order bit cleared indicates a property for a regular Flow Entry.
 * Low order bit set indicates a property for the Table-Miss Flow Entry.
 */
typedef enum  {
    OFCTFPT_INSTRUCTIONS         = 0,      /* Instructions property */
    OFCTFPT_INSTRUCTIONS_MISS    = 1,      /* Instructions for table-miss */
    OFCTFPT_NEXT_TABLES          = 2,      /* Next Table property */
    OFCTFPT_NEXT_TABLES_MISS     = 3,      /* Next Table for table-miss */
    OFCTFPT_WRITE_ACTIONS        = 4,      /* Write Actions property */
    OFCTFPT_WRITE_ACTIONS_MISS   = 5,      /* Write Actions for table-miss */
    OFCTFPT_APPLY_ACTIONS        = 6,      /* Apply Actions property */
    OFCTFPT_APPLY_ACTIONS_MISS   = 7,      /* Apply Actions for table-miss */
    OFCTFPT_MATCH                = 8,      /* Match property */
    OFCTFPT_WILDCARDS            = 10,     /* Wildcards property */
    OFCTFPT_WRITE_SETFIELD       = 12,     /* Write Set-Field property */
    OFCTFPT_WRITE_SETFIELD_MISS  = 13,     /* Write Set-Field for table-miss */
    OFCTFPT_APPLY_SETFIELD       = 14,     /* Apply Set-Field property */
    OFCTFPT_APPLY_SETFIELD_MISS  = 15,     /* Apply Set-Field for table-miss */
    OFCTFPT_EXPERIMENTER         = 0xFFFE, /* Experimenter property */
    OFCTFPT_EXPERIMENTER_MISS    = 0xFFFF, /* Experimenter for table-miss */
}tOfcTabFtrsPropType;

/* Group configuration flags */
typedef enum {
    OFCGFC_SELECT_WEIGHT   = 1 << 0, /* Support weight for select groups */
    OFCGFC_SELECT_LIVENESS = 1 << 1, /* Support liveness for select groups */
    OFCGFC_CHAINING        = 1 << 2, /* Support chaining groups */
    OFCGFC_CHAINING_CHECKS = 1 << 3, /* Check chaining for loops and delete */
}eOfcGroupCfgFlags;


/***************************** DESCRIPTION ******************************/
/* Body of reply to OFCMP_DESC request */
/* Each entry is a NULL-terminated ASCII string */
typedef struct {
    UINT1 au1MfrDesc[OFC_DESC_STR_LEN];     /* Manufacturer description. */
    UINT1 au1HwDesc[OFC_DESC_STR_LEN];      /* Hardware description. */
    UINT1 au1SwDesc[OFC_DESC_STR_LEN];      /* Software description. */
    UINT1 au1SerialNum[OFC_SERIAL_NUM_LEN]; /* Serial number. */
    UINT1 au1DpDesc[OFC_DESC_STR_LEN];      /* Human readable description of datapath. */
}tOfcDesciReply;

/************************* INDIVIDUAL FLOW STATISTICS ***********************/
/* Body for ofc_multipart_request of type OFCMP_FLOW. */
typedef struct {
    UINT1 u1TableId;          /* ID of table to read (from ofc_table_stats),
                                 OFCTT_ALL for all tables. */
    UINT1 au1Pad[3];          /* Align to 32 bits. */
    UINT4 u4OutPort;          /* Require matching entries to include this
                                 as an output port. A value of OFCP_ANY
                                 indicates no restriction */
    UINT4 u4Outgroup;         /* Require matching entries to include this
                                 as an output group. A value of OFCG_ANY
                                 indicates no restriction */
    UINT1 au1Pad2[4];         /* Align to 64 bits */
    FS_UINT8 u8Cookie;        /* Require matching entries to contain this
                                 cookie value */
    FS_UINT8 u8CookieMask;    /* Mask used to restrict the cookie bits that must
                                 match. A value of 0 indicates no restriction */
    tOfp131Match  Match;      /* Fields to match. Variable size. */
}tOfcFlowStatsReq;

/* Body of reply to OFCMP_FLOW request. */
typedef struct {
    UINT2 u2Length;           /* Length of this entry. */
    UINT1 u1TableId;          /* ID of table flow came from. */
    UINT1 au1Pad;
    UINT4 u4DurSec;           /* Time flow has been alive in seconds. */
    UINT4 u4DurNSec;          /* Time flow has been alive in nanoseconds beyond
                                 duration_sec. */
    UINT2 u2Priority;         /* Priority of the entry. */
    UINT2 u2IdleTO;           /* Number of seconds idle before expiration */
    UINT2 u2HardTO;           /* Number of seconds before expiration. */
    UINT2 u2Flags;            /* One of OFCFF_*. */
    UINT1 au1Pad2[4];         /* Align to 64-bits. */
    FS_UINT8 u8Cookie;           /* Opaque controller-issued identifier. */
    FS_UINT8 u8PktCount;         /* Number of packets in flow. */
    FS_UINT8 u8ByteCount;        /* Number of bytes in flow. */
    tOfp131Match Match;          /* Description of fields. Variable size. */
    /* struct ofc_instruction instructions[0]; *//* Instruction set. */
}tOfcFlowStatsReply;

/* Body for ofc_multipart_request of type OFCMP_AGGREGATE. */
typedef struct {
    UINT1 u1TableId;     /* ID of table to read (from ofc_table_stats)
                            OFCTT_ALL for all tables. */
    UINT1 au1Pad[3];     /* Align to 32 bits. */
    UINT4 u4OutPort;     /* Require matching entries to include this
                            as an output port. A value of OFCP_ANY
                            indicates no restriction. */
    UINT4 u4OutGroup;    /* Require matching entries to include this
                            as an output group. A value of OFCG_ANY
                            indicates no restriction. */
    UINT1 au1Pad2[4];    /* Align to 64 bits. */
    FS_UINT8 u8Cookie;      /* Require matching entries to contain this
                            cookie value */
    FS_UINT8 u8CookieMask;  /* Mask used to restrict the cookie bits that
                            must match. A value of 0 indicates
                            no restriction. */
    tOfp131Match Match; /* Fields to match. Variable size. */
}tOfcAggStatsReq;


/********************** AGGREGATE FLOW STATISTICS **************************/
/* Body of reply to OFCMP_AGGREGATE request. */
typedef struct ofc_aggregate_stats_reply {
    FS_UINT8 u8PktCount;   /* Number of packets in flows. */
    FS_UINT8 u8ByteCount;  /* Number of bytes in flows. */
    UINT4 u4FlowCount;  /* Number of flows. */
    UINT1 au1Pad[4];    /* Align to 64 bits. */
}tOfcAggStatsReply;


/************************** TABLE STATISTICS ******************************/
/* Body of reply to OFCMP_TABLE request. */
typedef struct {
    UINT1 u1TableId;      /* Identifier of table. Lower numbered tables
                             are consulted first. */
    UINT1 au1Pad[3];      /* Align to 32-bits. */
    UINT4 u4ActiveCount;  /* Number of active entries. */
    FS_UINT8 u8LookupCount;  /* Number of packets looked up in table. */
    FS_UINT8 u8MatchedCount; /* Number of packets that hit table. */
}tOfcTableStatsReply;


/******************** TABLE FEATURES MULTIPART ******************************/
/* Common header for all Table Feature Properties */
typedef struct {
    UINT2 u2Type;       /* One of OFCTFPT_*. */
    UINT2 u2Length;     /* Length in bytes of this property. */
}tOfcTableFeatPropHdr;

/* Body for ofc_multipart_request of type OFCMP_TABLE_FEATURES */
/* Body of reply to OFCMP_TABLE_FEATURES request */
typedef struct {
    UINT2 u2Length;     /* Length is au1Padded to 64 bits. */
    UINT1 u1TableId;     /* Identifier of table. Lower numbered tables are consulted first. */
    UINT1                au1Pad[5];       /* Align to 64-bits. */
    UINT1                u1Name[OFC_MAX_TABLE_NAME_LEN];
    FS_UINT8                u8MetadataMatch; /* Bits of metadata table can match */
    FS_UINT8                u8MetadataWrite; /* Bits of metadata table can write */
    UINT4                u4Config;        /* Bitmap of OFCTC_* values */
    UINT4                u4MaxEntries;    /* Max number of entries supported */
    tOfcTableFeatPropHdr Properties[0];   /* Table Feature Property list */
}tOfcTableFeatures;

typedef struct {
    UINT2 u2Type; /* Instruction type */
    UINT2 u2Length; /* Length of this struct in bytes. */
}tOfcInstruction;

/* Instructions property */
typedef struct {
    UINT2 u2Type;            /* One of OFCTFPT_INSTRUCTIONS,
                                  OFCTFPT_INSTRUCTIONS_MISS. */
    UINT2 u2Length;          /* Length in bytes of this property. */
    /* Followed by:
     * - Exactly (length - 4) bytes containing the instruction ids, then
     * - Exactly (length + 7)/8*8 - (length) (between 0 and 7)
     * bytes of all-zero bytes */
     tOfcInstruction InstructionIds[0]; /* List of instructions */
}tOfcTableFeatPropInstr;

/* Next Tables property */
typedef struct {
    UINT2 u2Type;    /* One of OFCTFPT_NEXT_TABLES,
                        OFCTFPT_NEXT_TABLES_MISS. */
    UINT2 u2Length;  /* Length in bytes of this property. */
    /* Followed by:
    * - Exactly (length - 4) bytes containing the table_ids, then
    * - Exactly (length + 7)/8*8 - (length) (between 0 and 7)
    * bytes of all-zero bytes */
    UINT1 au1NextTableIds[0];
}tOfcTableFeatPropNxtTable;


typedef struct {
    UINT2 u2Type;   /* One of OFPAT_*. */
    UINT2 u2Length; /* Length of action, including thisheader. This is the length of action, including         any padding to make it 64-bit aligned. */

}tOfcActionHdr;

/* Actions property */
typedef struct {
   UINT2 u2Type;   /* One of OFCTFPT_WRITE_ACTIONS,
                    OFCTFPT_WRITE_ACTIONS_MISS,
                    OFCTFPT_APPLY_ACTIONS,
                    OFCTFPT_APPLY_ACTIONS_MISS. */
   UINT2 u2Length; /* Length in bytes of this property. */
  /* Followed by:
   * - Exactly (length - 4) bytes containing the action_ids, then
   * - Exactly (length + 7)/8*8 - (length) (between 0 and 7)
   * bytes of all-zero bytes */
   tOfcActionHdr ActionIds[0]; /* List of actions */
}tOfcTableFeatPropActs;

/* Match, Wildcard or Set-Field property */
typedef struct {
    UINT2 u2Type;        /* One of OFCTFPT_MATCH,
                            OFCTFPT_WILDCARDS,
                            OFCTFPT_WRITE_SETFIELD,
                            OFCTFPT_WRITE_SETFIELD_MISS,
                            OFCTFPT_APPLY_SETFIELD,
                            OFCTFPT_APPLY_SETFIELD_MISS. */
    UINT2 u2Length;      /* Length in bytes of this property. */
    /* Followed by:
     * - Exactly (length - 4) bytes containing the oxm_ids, then
     * - Exactly (length + 7)/8*8 - (length) (between 0 and 7)
     * bytes of all-zero bytes */
    UINT4 au4OxmIds[0]; /* Array of OXM headers */
}tOfcTableFeatPropOxm;

/* Experimenter table feature property */
typedef struct {
    UINT2 u2Type;          /* One of OFCTFPT_EXPERIMENTER,
                              OFCTFPT_EXPERIMENTER_MISS. */
    UINT2 u2Length;        /* Length in bytes of this property. */
    UINT4 u4Experimenter;  /* Experimenter ID which takes the same
                              form as in struct
                              ofc_experimenter_header */
    UINT4 u4ExprType;      /* Experimenter defined. */
    /* Followed by:
     * - Exactly (length - 12) bytes containing the experimenter data, then
     * - Exactly (length + 7)/8*8 - (length) (between 0 and 7)
     * bytes of all-zero bytes */
    UINT4 au4ExperimenterData[0];
}tOfcTableFeatPropExpr;

/**************************** PORT STATS *******************************/
typedef struct {
    UINT4 u4PortNum;       /* OFCMP_PORT message must request statistics
                              either for a single port (specified in port_no)
                              or for all ports (if port_no == OFCP_ANY). */
    UINT1 au1Pad[4];
}tOfcPortStatsReq;

/* Body of reply to OFCMP_PORT request */
typedef struct {
    UINT4 u4PortNum;
    UINT1 au1Pad[4];      /* Align to 64-bits. */
    FS_UINT8 u8RxPkts;       /* Number of received packets. */
    FS_UINT8 u8TxPkts;       /* Number of transmitted packets. */
    FS_UINT8 u8RxBytes;      /* Number of received bytes. */
    FS_UINT8 u8TxBytes;      /* Number of transmitted bytes. */
    FS_UINT8 u8RxDropped;    /* Number of packets dropped by RX. */
    FS_UINT8 u8TxDropped;    /* Number of packets dropped by TX. */
    FS_UINT8 u8RxErrors;    /* Number of receive errors. This is a super-set
                             of more specific receive errors and should be
                             greater than or equal to the sum of all
                             rx_*_err values. */
    FS_UINT8 u8TxErrors;     /* Number of transmit errors. This is a super-set
                             of more specific transmit errors and should be
                             greater than or equal to the sum of all
                             tx_*_err values (none currently defined.) */
    FS_UINT8 u8RxFrameErr;   /* Number of frame alignment errors. */
    FS_UINT8 u8RxOverErr;    /* Number of packets with RX overrun. */
    FS_UINT8 u8RxCrcErr;     /* Number of CRC errors. */
    FS_UINT8 u8Collisions;   /* Number of collisions. */
    UINT4 u4DurSec;       /* Time port has been alive in seconds. */
    UINT4 u4DurationNSec; /* Time port has been alive in nanoseconds beyond
                             duration_sec. */
}tOfcPortStatsReply;      /* Array of this structure */


/*************************** PORT DESCRIPTION  *************************/
/* Description of a port */
typedef struct {
    UINT4 u4PortNum;
    UINT1 au1Pad[4];
    UINT1 u1HwAddr[OFC_ETH_ADDR_LEN];
    UINT1 au1Pad2[2]; /* Align to 64 bits. */
    UINT1 au1Name[OFC_MAX_PORT_NAME_LEN]; /* Null-terminated */
    UINT4 u4Config; /* Bitmap of OFCPC_* flags. */
    UINT4 u4State;        /* Bitmap of OFCPS_* flags. */
    /* 
     * Bitmaps of OFCPF_* that describe features.
     * All bits zeroed if unsupported or unavailable. 
     */
    UINT4 u4CurrFeatures;  /* Current features. */
    UINT4 u4Advertised;    /* Features being advertised by the port. */
    UINT4 u4Supported;     /* Features supported by the port. */
    UINT4 u4Peer;          /* Features advertised by peer. */
    UINT4 u4CurrSpeed;     /* Current port bitrate in kbps. */
    UINT4 u4MaxSpeed;      /* Max port bitrate in kbps */
}tOfcPortDescReply;        /* Array of this structure */


/****************************** QUEUE STATS *******************************/
typedef struct {
    UINT4 u4PortNum;       /* All ports if OFCP_ANY. */
    UINT4 u4QueId;         /* All queues if OFCQ_ALL. */
}tOfcQueStatsReq;

typedef struct {
    UINT4 u4PortNum;
    UINT4 u4QueId;         /* Queue i.d */
    FS_UINT8 u8TxBytes;       /* Number of transmitted bytes. */
    FS_UINT8 u8TxPkts;        /* Number of transmitted packets. */
    FS_UINT8 u8TxErrors;      /* Number of packets dropped due to overrun. */
    UINT4 u4DurSec;        /* Time queue has been alive in seconds. */
    UINT4 u4DurationNSec;  /* Time queue has been alive in nanoseconds beyond
                              duration_sec. */
}tOfcQueStatsReply;


/****************************** GROUP STATS ****************************/
/* Body of OFCMP_GROUP request. */
typedef struct {
    UINT4 u4GroupId;       /* All groups if OFCG_ALL. */
    UINT1 au1Pad[4];       /* Align to 64 bits. */
}tOfcGroupStatsReq;

/* Used in group stats replies. */
typedef struct {
    FS_UINT8 u8PktCount;      /* Number of packets processed by bucket. */
    FS_UINT8 u8ByteCount;     /* Number of bytes processed by bucket. */
}tOfcBucketCounter;

/* Body of reply to OFCMP_GROUP request. */
typedef struct {
    UINT2             u2Length;     /* Length of this entry. */
    UINT1             au1Pad[2];    /* Align to 64 bits. */
    UINT4             u4GroupId;    /* Group identifier. */
    UINT4             u4RefCount;   /* Number of flows or groups that directly
                                       forward to this group. */
    UINT1             au1Pad2[4];   /* Align to 64 bits. */
    FS_UINT8             u8PktCount;   /* Number of packets processed by group. */
    FS_UINT8             u8ByteCount;  /* Number of bytes processed by group. */
    UINT4             u4DurSec;     /* Time group has been alive in seconds. */
    UINT4             u4DurNSec;    /* Time group has been alive in nanoseconds
                                       beyond duration_sec. */
    tOfcBucketCounter BucketStats[0];
}tOfcGroupStatsReply;


/************************** GROUP DESCRIPTION *****************************/
/* Body of reply to OFCMP_GROUP_DESC request. */
typedef struct {
    UINT2 u2Length;      /* Length of this entry. */
    UINT1 u1Type;        /* One of OFCGT_*. */
    UINT1 au1Pad;        /* Pad to 64 bits. */
    UINT4 u4GroupId;     /* Group identifier. */
    tOfp131Bucket  buckets[0];
}tOfcGroupDescStats;


/**************************** GROUP FEATURES ******************************/
/* Body of reply to OFCMP_GROUP_FEATURES request. Group features. */
typedef struct {
    UINT4 u4Types;           /* Bitmap of OFCGT_* values supported. */
    UINT4 u4Capabilities;    /* Bitmap of OFCGFC_* capability supported. */
    UINT4 au4MaxGroups[4];   /* Maximum number of groups for each type. */
    UINT4 au4Actions[4];     /* Bitmaps of OFCAT_* that are supported. */
}tOfcGroupFeatures;

/**************************** METER FEATURE ******************************/
typedef struct OFP_METER_FEATURES {
    UINT4 u4MaxMeter; /* Maximum number of meters. */
    UINT4 u4BandTypes; /* Bitmaps of OFPMBT_* values supported. */
    UINT4 u4Capabilities; /* Bitmaps of "ofp_meter_flags". */
    UINT1 u1MaxBands; /* Maximum bands per meters */
    UINT1 u1Maxcolor; /* Maximum color value */
    UINT1 au1Pad[2];
}tOfcMeterFeatures;

/**************************** METER CONFIG ******************************/
/* Body of reply to OFCMP_METER_CONFIG request. Meter features. */
typedef struct OFP_METER_CONFIG {
    UINT2 u2Length;  /* Length of this entry. */
    UINT2 u2Flags;   /* All OFPMC_* that apply. */
    UINT4 u4Meterid; /* Meter instance. */
    tOfp131MeterBandHdr bands[0];/* The bands length is inferred from the length field. */
}tOfcMeterConfig;

/**************************** METER STATISTICS ******************************/
typedef struct OFP_METER_BAND_STATS {
    FS_UINT8 u8PacketBandCount; /* Number of packets in band. */
    FS_UINT8 u8ByteBandCount; /* Number of bytes in band. */
}tOfcMeterBandStats;

typedef struct OFP_METER_REQUEST {
    UINT4 u4MeterId; /* Meter instance, or OFPM_ALL. */
    UINT1 au1Pad[4];
}tOfcMeterReq;

typedef struct OFP_METER_STATS {
    UINT4 u4Meterid; /* Meter instance. */
    UINT2 u2Len; /* Length in bytes of this stats. */
    UINT1 au1pad[6];
    UINT4 u4FlowCount; /* Number of flows bound to meter. */
    FS_UINT8 u8PacketInCount; /* Number of packets in input. */
    FS_UINT8 u8ByteInCount; /* Number of bytes in input. */
    UINT4 u4DurSec; /* Time meter has been alive in seconds. */
    UINT4 u4DurNSec; /* Time meter has been alive in nanoseconds beyond duration_sec. */
    tOfcMeterBandStats bandstats[0]; /* The band_stats length is inferred from the length field. */
}tOfcMeterStats;

/**************************** INSTRUCTIONS ******************************/
enum ofp_instruction_type {
   OFPIT_GOTO_TABLE = 1,       /* Setup the next table in the lookup pipeline */
   OFPIT_WRITE_METADATA = 2,   /* Setup the metadata field for use later in pipeline */
   OFPIT_WRITE_ACTIONS = 3,    /* Write the action(s) onto the datapath action
          set */
   OFPIT_APPLY_ACTIONS = 4,    /* Applies the action(s) immediately */
   OFPIT_CLEAR_ACTIONS = 5,    /* Clears all actions from the datapath
          action set */
   OFPIT_METER = 6,            /* Apply meter (rate limiter) */
   OFPIT_EXPERIMENTER = 0xFFFF /* Experimenter instruction */
};

typedef struct {
    UINT2 u2Type; /* One of OFPMT_* */
    UINT2 u2Length; /* Length of ofp_match (excluding padding) */
 /* Followed by:
  * * - Exactly (length - 4) (possibly 0) bytes containing OXM TLVs, then
  * * - Exactly ((length + 7)/8*8 - length) (between 0 and 7) bytes of
  * * all-zero bytes
  * * In summary, ofp_match is padded as needed, to make its overall size
  * * a multiple of 8, to preserve alignement in structures using it.
  * */
 UINT2 OxmFields[4]; /* OXMs start here - Make compiler happy */
}tOfcMatch;

INT4 OfcGetOfcDesc(tOfcDesciReply *pOfcDesciReply);
/*********************             FUNCTION PROTOTYPES           *************************************/
INT4 Ofc131MultipartPortDesc(tOfp131MultiPartReq *pOfc131MultipartReq, VOID *pConnPtr,                                              tCRU_BUF_CHAIN_HEADER *pBuf, UINT4 *pu4Error);
INT4 Ofc131MultipartPortStats(tOfp131MultiPartReq *pOfc131MultipartReq, VOID *pConnPtr,                                              tCRU_BUF_CHAIN_HEADER *pBuf, UINT4 *pu4Error);
INT4 Ofc131MultipartOfcDesc(tOfp131MultiPartReq *pOfc131MultipartReq, VOID *pConnPtr,                                              tCRU_BUF_CHAIN_HEADER *pBuf, UINT4 *pu4Error);
INT4 Ofc131MultipartGroupFeatures(tOfp131MultiPartReq *pOfc131MultipartReq, VOID *pConnPtr,                                              tCRU_BUF_CHAIN_HEADER *pBuf, UINT4 *pu4Error);
INT4 Ofc131MultipartGroupStats(tOfp131MultiPartReq *pOfc131MultipartReq, VOID *pConnPtr,                                              tCRU_BUF_CHAIN_HEADER *pBuf, UINT4 *pu4Error);
INT4 Ofc131MultipartGroupDesc(tOfp131MultiPartReq *pOfc131MultipartReq, VOID *pConnPtr,                                              tCRU_BUF_CHAIN_HEADER *pBuf, UINT4 *pu4Error);
INT4 Ofc131MultipartFlowTableStats(tOfp131MultiPartReq *pOfc131MultipartReq, VOID *pConnPtr,                                              tCRU_BUF_CHAIN_HEADER *pBuf, UINT4 *pu4Error);
INT4 Ofc131MultipartAggFlowStats(tOfp131MultiPartReq *pOfc131MultipartReq, VOID *pConnPtr,                                              tCRU_BUF_CHAIN_HEADER *pBuf, UINT4 *pu4Error);

INT4 Ofc131MultipartIndividualFlowStats(tOfp131MultiPartReq *pOfc131MultipartReq, VOID *pConnPtr,                                                                          tCRU_BUF_CHAIN_HEADER *pBuf, UINT4 *pu4Error);

INT4 Ofc131MultipartTableFeatures(tOfp131MultiPartReq *pOfc131MultipartReq, VOID *pConnPtr,                                                                                tCRU_BUF_CHAIN_HEADER *pBuf, UINT4 *pu4Error);

INT4 Ofc131MultipartQueueStats(tOfp131MultiPartReq *pOfc131MultipartReq,
                                VOID *pConnPtr, tCRU_BUF_CHAIN_HEADER *pBuf,
                                UINT4 *pu4Error);

INT4 Ofc131MultipartExperimenter(tOfp131MultiPartReq *pOfc131MultipartReq,
                                  VOID *pConnPtr, tCRU_BUF_CHAIN_HEADER *pBuf,
                                  UINT4 *pu4Error);

INT4 Ofc131MultipartMeterDesc(tOfp131MultiPartReq *pOfc131MultipartReq, VOID *pConnPtr, 
                              tCRU_BUF_CHAIN_HEADER *pBuf, UINT4 *pu4Error);
INT4 Ofc131MultipartMeterConfig (tOfp131MultiPartReq *pOfc131MultipartReq, VOID *pConnPtr, 
                                 tCRU_BUF_CHAIN_HEADER *pBuf, UINT4 *pu4Error);
INT4 Ofc131MultipartMeterFeatures(tOfp131MultiPartReq *pOfc131MultipartReq, VOID *pConnPtr, 
                                  tCRU_BUF_CHAIN_HEADER *pBuf, UINT4 *pu4Error);

#endif
