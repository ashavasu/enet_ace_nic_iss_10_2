/********************************************************************
 * Copyright (C) Future Software Limited,1997-98,2001
 * 
 * $Id: ofctrc.h,v 1.4 2014/08/14 11:13:33 siva Exp $
 *
 * Description:This file contains procedures and definitions 
 *             used for debugging.
 *******************************************************************/


#ifndef __OFCTRC_H__
#define __OFCTRC_H__

#define  OFC_TRC_FLAG  gOfcGlobals.u4OfcTrc
#define  OFC_NAME      "OFC"               

#ifdef OFC_TRACE_WANTED



#define  OFC_TRC(x)       OfcTrcPrint( __FILE__, __LINE__, OfcTrc x)
#define  OFC_TRC_FUNC(x)  OfcTrcPrint( __FILE__, __LINE__, OfcTrc x)
#define  OFC_TRC_CRIT(x)  OfcTrcPrint( __FILE__, __LINE__, OfcTrc x)
#define  OFC_TRC_PKT(x)   OfcTrcWrite( OfcTrc x)




#else /* OFC_TRACE_WANTED */

#define  OFC_TRC(x) 
#define  OFC_TRC_FUNC(x)
#define  OFC_TRC_CRIT(x)
#define  OFC_TRC_PKT(x)

#endif /* OFC_TRACE_WANTED */


#define  OFC_FN_ENTRY  (0x1 << 9)
#define  OFC_FN_EXIT   (0x1 << 10)
#define  OFC_CLI_TRC   (0x1 << 11)
#define  OFC_MAIN_TRC  (0x1 << 12)
#define  OFC_PKT_TRC   (0x1 << 13)
#define  OFC_QUE_TRC   (0x1 << 14)
#define  OFC_TASK_TRC  (0x1 << 15)
#define  OFC_TMR_TRC   (0x1 << 16)
#define  OFC_UTIL_TRC  (0x1 << 17)
#define  OFC_FLOWTBL_TRC  (0x1 << 18)
#define  OFC_ACTS_TRC  (0x1 << 19)
#define  OFC_GRPTBL_TRC  (0x1 << 20)
#define  OFC_METER_TRC  (0x1 << 21)
#define  OFC_HYBRID_TRC  (0x1 << 22)
#define  OFC_DPA_TRC  (0x1 << 23)
#define  OFC_ALL_TRC  OFC_FN_ENTRY |\
                      OFC_FN_EXIT  |\
                      OFC_CLI_TRC  |\
                      OFC_MAIN_TRC |\
                      OFC_PKT_TRC  |\
                      OFC_QUE_TRC  |\
                      OFC_TASK_TRC |\
                      OFC_TMR_TRC  |\
                      OFC_UTIL_TRC  |\
                      OFC_FLOWTBL_TRC  |\
                      OFC_GRPTBL_TRC  |\
                      OFC_METER_TRC  |\
                      OFC_HYBRID_TRC |\
                      OFC_DPA_TRC |\
                      OFC_ACTS_TRC

#define  OFC_NO_FN_ENTRY  (0xfffe << 9)
#define  OFC_NO_FN_EXIT   (0xfffe << 10)
#define  OFC_NO_CLI_TRC   (0xfffe << 11)
#define  OFC_NO_MAIN_TRC  (0xfffe << 12)
#define  OFC_NO_PKT_TRC   (0xfffe << 13)
#define  OFC_NO_QUE_TRC   (0xfffe << 14)
#define  OFC_NO_TASK_TRC  (0xfffe << 15)
#define  OFC_NO_TMR_TRC   (0xfffe << 16)
#define  OFC_NO_UTIL_TRC  (0xfffe << 17)
#define  OFC_NO_FLOWTBL_TRC  (0xfffe << 18)
#define  OFC_NO_ACTS_TRC  (0xfffe << 19)
#define  OFC_NO_GRPTBL_TRC  (0xfffe << 20)
#define  OFC_NO_METER_TRC  (0xfffe << 21)
#define  OFC_NO_HYBRID_TRC (0xfffe << 22)

CHR1  *OfcTrc      PROTO(( UINT4 u4Trc,  const char *fmt, ...));
VOID   OfcTrcPrint PROTO(( const char *fname, UINT4 u4Line, const char *s));
VOID   OfcTrcWrite PROTO(( CHR1 *s));

#endif /* _OFCTRC_H */


/*-----------------------------------------------------------------------*/
/*                       End of the file  ofctrc.h                      */
/*-----------------------------------------------------------------------*/
