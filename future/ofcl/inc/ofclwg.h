/********************************************************************
* Copyright (C) 20132006 Aricent Inc . All Rights Reserved
*
* $Id: ofclwg.h,v 1.3 2014/01/31 13:03:56 siva Exp $ 
*
* Description: Proto types for Low Level  Routines
*********************************************************************/

/* Proto Validate Index Instance for FsofcCfgTable. */
INT1
nmhValidateIndexInstanceFsofcCfgTable ARG_LIST((UINT4 ));

/* Proto Validate Index Instance for FsofcControllerConnTable. */
INT1
nmhValidateIndexInstanceFsofcControllerConnTable ARG_LIST((UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4 ));

/* Proto Validate Index Instance for FsofcIfTable. */
INT1
nmhValidateIndexInstanceFsofcIfTable ARG_LIST((UINT4  , INT4 ));

/* Proto Validate Index Instance for FsofcFlowTable. */
INT1
nmhValidateIndexInstanceFsofcFlowTable ARG_LIST((UINT4  , UINT4  , UINT4 ));

/* Proto Validate Index Instance for FsofcGroupTable. */
INT1
nmhValidateIndexInstanceFsofcGroupTable ARG_LIST((UINT4  , UINT4 ));

/* Proto Validate Index Instance for FsofcMeterTable. */
INT1
nmhValidateIndexInstanceFsofcMeterTable ARG_LIST((UINT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsofcCfgTable  */

INT1
nmhGetFirstIndexFsofcCfgTable ARG_LIST((UINT4 *));

/* Proto Type for Low Level GET FIRST fn for FsofcControllerConnTable  */

INT1
nmhGetFirstIndexFsofcControllerConnTable ARG_LIST((UINT4 * , INT4 * , tSNMP_OCTET_STRING_TYPE *  , INT4 *));

/* Proto Type for Low Level GET FIRST fn for FsofcIfTable  */

INT1
nmhGetFirstIndexFsofcIfTable ARG_LIST((UINT4 * , INT4 *));

/* Proto Type for Low Level GET FIRST fn for FsofcFlowTable  */

INT1
nmhGetFirstIndexFsofcFlowTable ARG_LIST((UINT4 * , UINT4 * , UINT4 *));

/* Proto Type for Low Level GET FIRST fn for FsofcGroupTable  */

INT1
nmhGetFirstIndexFsofcGroupTable ARG_LIST((UINT4 * , UINT4 *));

/* Proto Type for Low Level GET FIRST fn for FsofcMeterTable  */

INT1
nmhGetFirstIndexFsofcMeterTable ARG_LIST((UINT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsofcCfgTable ARG_LIST((UINT4 , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsofcControllerConnTable ARG_LIST((UINT4 , UINT4 * , INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , INT4 , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsofcIfTable ARG_LIST((UINT4 , UINT4 * , INT4 , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsofcFlowTable ARG_LIST((UINT4 , UINT4 * , UINT4 , UINT4 * , UINT4 , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsofcGroupTable ARG_LIST((UINT4 , UINT4 * , UINT4 , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsofcMeterTable ARG_LIST((UINT4 , UINT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsofcModuleStatus ARG_LIST((UINT4 ,INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsofcSupportedVersion ARG_LIST((UINT4 ,INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsofcDefaultFlowMissBehaviour ARG_LIST((UINT4 ,INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsofcControlPktBuffering ARG_LIST((UINT4 ,INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsofcIpReassembleStatus ARG_LIST((UINT4 ,INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsofcPortStpStatus ARG_LIST((UINT4 ,INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsofcTraceEnable ARG_LIST((UINT4 ,UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsofcSwitchModeOnConnFailure ARG_LIST((UINT4 ,INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsofcSwitchEntryStatus ARG_LIST((UINT4 ,INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsofcHybrid ARG_LIST((UINT4 ,INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsofcControllerConnPort ARG_LIST((UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4 ,INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsofcControllerConnProtocol ARG_LIST((UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4 ,INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsofcControllerRole ARG_LIST((UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4 ,INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsofcControllerConnState ARG_LIST((UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4 ,INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsofcControllerConnEchoReqCount ARG_LIST((UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4 ,INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsofcControllerConnEchoReplyCount ARG_LIST((UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4 ,INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsofcControllerConnEntryStatus ARG_LIST((UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4 ,INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsofcControllerConnBand ARG_LIST((UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4 ,INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsofcIfType ARG_LIST((UINT4  , INT4 ,INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsofcIfAlias ARG_LIST((UINT4  , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsofcIfOperStatus ARG_LIST((UINT4  , INT4 ,INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsofcVlanEgressPorts ARG_LIST((UINT4  , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsofcVlanUntaggedPorts ARG_LIST((UINT4  , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsofcVlanInFrames ARG_LIST((UINT4  , INT4 ,UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsofcVlanOutFrames ARG_LIST((UINT4  , INT4 ,UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsofcIfContextId ARG_LIST((UINT4  , INT4 ,UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsofcFlowMatchField ARG_LIST((UINT4  , UINT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsofcFlowOutputAction ARG_LIST((UINT4  , UINT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsofcFlowIdleTimeout ARG_LIST((UINT4  , UINT4  , UINT4 ,UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsofcFlowHardTimeout ARG_LIST((UINT4  , UINT4  , UINT4 ,UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsofcFlowPacketCount ARG_LIST((UINT4  , UINT4  , UINT4 ,tSNMP_COUNTER64_TYPE *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsofcFlowByteCount ARG_LIST((UINT4  , UINT4  , UINT4 ,tSNMP_COUNTER64_TYPE *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsofcFlowDurationSec ARG_LIST((UINT4  , UINT4  , UINT4 ,UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsofcGroupType ARG_LIST((UINT4  , UINT4 ,INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsofcGroupActionBuckets ARG_LIST((UINT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsofcGroupPacketCount ARG_LIST((UINT4  , UINT4 ,tSNMP_COUNTER64_TYPE *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsofcGroupByteCount ARG_LIST((UINT4  , UINT4 ,tSNMP_COUNTER64_TYPE *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsofcGroupDurationSec ARG_LIST((UINT4  , UINT4 ,UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsofcMeterBandInfo ARG_LIST((UINT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsofcMeterFlowCount ARG_LIST((UINT4  , UINT4 ,UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsofcMeterPacketInCount ARG_LIST((UINT4  , UINT4 ,tSNMP_COUNTER64_TYPE *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsofcMeterByteInCount ARG_LIST((UINT4  , UINT4 ,tSNMP_COUNTER64_TYPE *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsofcMeterDurationSec ARG_LIST((UINT4  , UINT4 ,UINT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsofcModuleStatus ARG_LIST((UINT4  ,INT4 ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsofcSupportedVersion ARG_LIST((UINT4  ,INT4 ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsofcDefaultFlowMissBehaviour ARG_LIST((UINT4  ,INT4 ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsofcControlPktBuffering ARG_LIST((UINT4  ,INT4 ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsofcIpReassembleStatus ARG_LIST((UINT4  ,INT4 ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsofcPortStpStatus ARG_LIST((UINT4  ,INT4 ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsofcTraceEnable ARG_LIST((UINT4  ,UINT4 ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsofcSwitchModeOnConnFailure ARG_LIST((UINT4  ,INT4 ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsofcSwitchEntryStatus ARG_LIST((UINT4  ,INT4 ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsofcHybrid ARG_LIST((UINT4  ,INT4 ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsofcControllerConnPort ARG_LIST((UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  ,INT4 ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsofcControllerConnProtocol ARG_LIST((UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  ,INT4 ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsofcControllerRole ARG_LIST((UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  ,INT4 ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsofcControllerConnEchoReqCount ARG_LIST((UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  ,INT4 ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsofcControllerConnEntryStatus ARG_LIST((UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  ,INT4 ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsofcControllerConnBand ARG_LIST((UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  ,INT4 ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsofcVlanEgressPorts ARG_LIST((UINT4  , INT4  ,tSNMP_OCTET_STRING_TYPE *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsofcVlanUntaggedPorts ARG_LIST((UINT4  , INT4  ,tSNMP_OCTET_STRING_TYPE *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsofcIfContextId ARG_LIST((UINT4  , INT4  ,UINT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsofcModuleStatus ARG_LIST((UINT4 * , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsofcSupportedVersion ARG_LIST((UINT4 * , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsofcDefaultFlowMissBehaviour ARG_LIST((UINT4 * , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsofcControlPktBuffering ARG_LIST((UINT4 * , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsofcIpReassembleStatus ARG_LIST((UINT4 * , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsofcPortStpStatus ARG_LIST((UINT4 * , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsofcTraceEnable ARG_LIST((UINT4 * , UINT4  ,UINT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsofcSwitchModeOnConnFailure ARG_LIST((UINT4 * , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsofcSwitchEntryStatus ARG_LIST((UINT4 * , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsofcHybrid ARG_LIST((UINT4 * , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsofcControllerConnPort ARG_LIST((UINT4 * , UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsofcControllerConnProtocol ARG_LIST((UINT4 * , UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsofcControllerRole ARG_LIST((UINT4 * , UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsofcControllerConnEchoReqCount ARG_LIST((UINT4 * , UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsofcControllerConnEntryStatus ARG_LIST((UINT4 * , UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsofcControllerConnBand ARG_LIST((UINT4 * , UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsofcVlanEgressPorts ARG_LIST((UINT4 * , UINT4  , INT4  ,tSNMP_OCTET_STRING_TYPE *));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsofcVlanUntaggedPorts ARG_LIST((UINT4 * , UINT4  , INT4  ,tSNMP_OCTET_STRING_TYPE *));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsofcIfContextId ARG_LIST((UINT4 * , UINT4  , INT4  ,UINT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsofcCfgTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsofcControllerConnTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));
/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsofcIfTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));
