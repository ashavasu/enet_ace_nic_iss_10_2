/********************************************************************
 * Copyright (C) Future Software Limited, 2001
 *
 * Description: This file contains definitions for ofc Timer
 *******************************************************************/

#ifndef __OFCTMR_H__
#define __OFCTMR_H__



/* constants for timer types */
typedef enum {
	OFC_CONTROLLER_TMR =0,
        OFC_HARD_TIMER = 1,
        OFC_IDLE_TIMER = 2,
	OFC_ECHOREQ_TMR = 3,
	OFC_ECHOINTER_TMR = 4,
        OFC_NONTCAM_IDLE_TMR = 5,
	OFC_MAX_TMRS = 6
} enOfcTmrId;

#define     NO_OF_TICKS_PER_SEC             SYS_NUM_OF_TIME_UNITS_IN_A_SEC

typedef struct _OFC_TMR_DESC {
    VOID                (*pTmrExpFunc)(VOID *);
    INT2                i2Offset;
                            /* If this field is -1 then the fn takes no
                             * parameter
                             */
    UINT2               u2Pad;
                            /* Included for 4-byte Alignment
                             */
} tOfcTmrDesc;

typedef struct _OFC_TIMER {
       tTmrAppTimer    tmrNode;
       enOfcTmrId     eOfcTmrId;
} tOfcTmr;




#endif  /* __OFCTMR_H__  */


/*-----------------------------------------------------------------------*/
/*                       End of the file  ofctmr.h                      */
/*-----------------------------------------------------------------------*/
