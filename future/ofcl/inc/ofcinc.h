/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: ofcinc.h,v 1.3 2014/01/31 13:03:56 siva Exp $
 *
 * Description: This file contains include files of ofc module.
 *******************************************************************/

#ifndef __OFCINC_H__
#define __OFCINC_H__ 

#include "lr.h"
#include "cli.h"
#include "cfa.h"
#include "ofcl.h"
#include "l2iwf.h" 
#include "fssnmp.h"
#include "params.h"
#include "snmcdefn.h"
#include "snmctdfs.h"
#include "snmccons.h"
#include "ofcdefn.h"
#include "ofctdfsg.h"
#include "ofctmr.h"
#include "ofctdfs.h"
#include "ofctrc.h"
#include "iss.h"
#ifdef __OFCMAIN_C__

#include "ofcglob.h"
#include "ofclwg.h"
#include "ofcdefg.h"
#include "ofcwrg.h"

#else

#include "ofcextn.h"
#include "ofcmibclig.h"
#include "ofclwg.h"
#include "ofcdefg.h"
#include "ofcwrg.h"

#endif /* __OFCMAIN_C__*/

#include "ofcprot.h"
#include "ofcprotg.h"

#ifdef NPAPI_WANTED
#include "npapi.h"
#include "ofcnp.h"
#include "nputil.h"
#endif

#ifdef SSL_WANTED
#include "fsssl.h"
#endif 

#endif   /* __OFCINC_H__*/

/*-----------------------------------------------------------------------*/
/*                       End of the file ofcinc.h                       */
/*-----------------------------------------------------------------------*/

