/*
 * Automatically generated by FuturePostmosy
 * Do not Edit!!!
 * $Id: ofc.h,v 1.2 2014/01/31 13:03:56 siva Exp $
 */
#ifndef _SNMP_MIB_H
#define _SNMP_MIB_H

/* SNMP-MIB translation table. */

static struct MIB_OID orig_mib_oid_table[] = {
"ccitt",        "0",
"iso",        "1",
"lldpExtensions",        "1.0.8802.1.1.2.1.5",
"lldpV2Xdot1MIB",        "1.0.8802.1.1.2.1.5.40000",
"org",        "1.3",
"dod",        "1.3.6",
"internet",        "1.3.6.1",
"directory",        "1.3.6.1.1",
"mgmt",        "1.3.6.1.2",
"mib-2",        "1.3.6.1.2.1",
"ip",        "1.3.6.1.2.1.4",
"transmission",        "1.3.6.1.2.1.10",
"mplsStdMIB",        "1.3.6.1.2.1.10.166",
"rmon",        "1.3.6.1.2.1.16",
"statistics",        "1.3.6.1.2.1.16.1",
"history",        "1.3.6.1.2.1.16.2",
"hosts",        "1.3.6.1.2.1.16.4",
"matrix",        "1.3.6.1.2.1.16.6",
"filter",        "1.3.6.1.2.1.16.7",
"tokenRing",        "1.3.6.1.2.1.16.10",
"dot1dBridge",        "1.3.6.1.2.1.17",
"dot1dStp",        "1.3.6.1.2.1.17.2",
"dot1dTp",        "1.3.6.1.2.1.17.4",
"vrrpOperEntry",        "1.3.6.1.2.1.18.1.3.1",
"experimental",        "1.3.6.1.3",
"private",        "1.3.6.1.4",
"enterprises",        "1.3.6.1.4.1",
"issExt",        "1.3.6.1.4.1.2076.81.8",
"fsDot1dBridge",        "1.3.6.1.4.1.2076.116",
"fsDot1dStp",        "1.3.6.1.4.1.2076.116.2",
"fsDot1dTp",        "1.3.6.1.4.1.2076.116.4",
"fsofc",        "1.3.6.1.4.1.29601.2.81",
"fsofcCfgGroup",        "1.3.6.1.4.1.29601.2.81.1",
"fsofcCfgTable",        "1.3.6.1.4.1.29601.2.81.1.1",
"fsofcCfgEntry",        "1.3.6.1.4.1.29601.2.81.1.1.1",
"fsofcContextId",        "1.3.6.1.4.1.29601.2.81.1.1.1.1",
"fsofcModuleStatus",        "1.3.6.1.4.1.29601.2.81.1.1.1.2",
"fsofcSupportedVersion",        "1.3.6.1.4.1.29601.2.81.1.1.1.3",
"fsofcDefaultFlowMissBehaviour",        "1.3.6.1.4.1.29601.2.81.1.1.1.4",
"fsofcControlPktBuffering",        "1.3.6.1.4.1.29601.2.81.1.1.1.5",
"fsofcIpReassembleStatus",        "1.3.6.1.4.1.29601.2.81.1.1.1.6",
"fsofcPortStpStatus",        "1.3.6.1.4.1.29601.2.81.1.1.1.7",
"fsofcTraceEnable",        "1.3.6.1.4.1.29601.2.81.1.1.1.8",
"fsofcSwitchModeOnConnFailure",        "1.3.6.1.4.1.29601.2.81.1.1.1.9",
"fsofcSwitchEntryStatus",        "1.3.6.1.4.1.29601.2.81.1.1.1.10",
"fsofcHybrid",        "1.3.6.1.4.1.29601.2.81.1.1.1.11",
"fsofcControllerCfgGroup",        "1.3.6.1.4.1.29601.2.81.2",
"fsofcControllerConnTable",        "1.3.6.1.4.1.29601.2.81.2.1",
"fsofcControllerConnEntry",        "1.3.6.1.4.1.29601.2.81.2.1.1",
"fsofcControllerIpAddrType",        "1.3.6.1.4.1.29601.2.81.2.1.1.1",
"fsofcControllerIpAddress",        "1.3.6.1.4.1.29601.2.81.2.1.1.2",
"fsofcControllerConnAuxId",        "1.3.6.1.4.1.29601.2.81.2.1.1.3",
"fsofcControllerConnPort",        "1.3.6.1.4.1.29601.2.81.2.1.1.4",
"fsofcControllerConnProtocol",        "1.3.6.1.4.1.29601.2.81.2.1.1.5",
"fsofcControllerRole",        "1.3.6.1.4.1.29601.2.81.2.1.1.6",
"fsofcControllerConnState",        "1.3.6.1.4.1.29601.2.81.2.1.1.7",
"fsofcControllerConnEchoReqCount",        "1.3.6.1.4.1.29601.2.81.2.1.1.8",
"fsofcControllerConnEchoReplyCount",        "1.3.6.1.4.1.29601.2.81.2.1.1.9",
"fsofcControllerConnEntryStatus",        "1.3.6.1.4.1.29601.2.81.2.1.1.10",
"fsofcControllerConnBand",        "1.3.6.1.4.1.29601.2.81.2.1.1.11",
"fsofcInterfaceGroup",        "1.3.6.1.4.1.29601.2.81.3",
"fsofcIfTable",        "1.3.6.1.4.1.29601.2.81.3.1",
"fsofcIfEntry",        "1.3.6.1.4.1.29601.2.81.3.1.1",
"fsofcIfIndex",        "1.3.6.1.4.1.29601.2.81.3.1.1.1",
"fsofcIfType",        "1.3.6.1.4.1.29601.2.81.3.1.1.2",
"fsofcIfAlias",        "1.3.6.1.4.1.29601.2.81.3.1.1.3",
"fsofcIfOperStatus",        "1.3.6.1.4.1.29601.2.81.3.1.1.4",
"fsofcVlanEgressPorts",        "1.3.6.1.4.1.29601.2.81.3.1.1.5",
"fsofcVlanUntaggedPorts",        "1.3.6.1.4.1.29601.2.81.3.1.1.6",
"fsofcVlanInFrames",        "1.3.6.1.4.1.29601.2.81.3.1.1.7",
"fsofcVlanOutFrames",        "1.3.6.1.4.1.29601.2.81.3.1.1.8",
"fsofcIfContextId",        "1.3.6.1.4.1.29601.2.81.3.1.1.9",
"fsofcFlowGroup",        "1.3.6.1.4.1.29601.2.81.4",
"fsofcFlowTable",        "1.3.6.1.4.1.29601.2.81.4.1",
"fsofcFlowEntry",        "1.3.6.1.4.1.29601.2.81.4.1.1",
"fsofcTableIndex",        "1.3.6.1.4.1.29601.2.81.4.1.1.1",
"fsofcFlowIndex",        "1.3.6.1.4.1.29601.2.81.4.1.1.2",
"fsofcFlowMatchField",        "1.3.6.1.4.1.29601.2.81.4.1.1.3",
"fsofcFlowOutputAction",        "1.3.6.1.4.1.29601.2.81.4.1.1.4",
"fsofcFlowIdleTimeout",        "1.3.6.1.4.1.29601.2.81.4.1.1.5",
"fsofcFlowHardTimeout",        "1.3.6.1.4.1.29601.2.81.4.1.1.6",
"fsofcFlowPacketCount",        "1.3.6.1.4.1.29601.2.81.4.1.1.7",
"fsofcFlowByteCount",        "1.3.6.1.4.1.29601.2.81.4.1.1.8",
"fsofcFlowDurationSec",        "1.3.6.1.4.1.29601.2.81.4.1.1.9",
"fsofcGrpGroup",        "1.3.6.1.4.1.29601.2.81.5",
"fsofcGroupTable",        "1.3.6.1.4.1.29601.2.81.5.1",
"fsofcGroupEntry",        "1.3.6.1.4.1.29601.2.81.5.1.1",
"fsofcGroupIndex",        "1.3.6.1.4.1.29601.2.81.5.1.1.1",
"fsofcGroupType",        "1.3.6.1.4.1.29601.2.81.5.1.1.2",
"fsofcGroupActionBuckets",        "1.3.6.1.4.1.29601.2.81.5.1.1.3",
"fsofcGroupPacketCount",        "1.3.6.1.4.1.29601.2.81.5.1.1.5",
"fsofcGroupByteCount",        "1.3.6.1.4.1.29601.2.81.5.1.1.6",
"fsofcGroupDurationSec",        "1.3.6.1.4.1.29601.2.81.5.1.1.7",
"fsofcMeterGroup",        "1.3.6.1.4.1.29601.2.81.6",
"fsofcMeterTable",        "1.3.6.1.4.1.29601.2.81.6.1",
"fsofcMeterEntry",        "1.3.6.1.4.1.29601.2.81.6.1.1",
"fsofcMeterIndex",        "1.3.6.1.4.1.29601.2.81.6.1.1.1",
"fsofcMeterBandInfo",        "1.3.6.1.4.1.29601.2.81.6.1.1.2",
"fsofcMeterFlowCount",        "1.3.6.1.4.1.29601.2.81.6.1.1.3",
"fsofcMeterPacketInCount",        "1.3.6.1.4.1.29601.2.81.6.1.1.4",
"fsofcMeterByteInCount",        "1.3.6.1.4.1.29601.2.81.6.1.1.5",
"fsofcMeterDurationSec",        "1.3.6.1.4.1.29601.2.81.6.1.1.6",
"security",        "1.3.6.1.5",
"snmpV2",        "1.3.6.1.6",
"snmpDomains",        "1.3.6.1.6.1",
"snmpProxys",        "1.3.6.1.6.2",
"snmpModules",        "1.3.6.1.6.3",
"snmpAuthProtocols",        "1.3.6.1.6.3.10.1.1",
"snmpPrivProtocols",        "1.3.6.1.6.3.10.1.2",
"ieee802dot1mibs",        "1.111.2.802.1",
"joint-iso-ccitt",        "2",
0,0
};

#endif /* _SNMP_MIB_H */
