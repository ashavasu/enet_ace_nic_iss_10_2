/*****************************************************************************/
/* Copyright (C) 2013 Aricent Inc . All Rights Reserved
 * $Id: ofcgroup.h,v 1.2 2014/01/31 13:03:55 siva Exp $
 ******************************************************************************
 *    FILE  NAME             : ofcgroup.h
 *    PRINCIPAL AUTHOR       : Aricent Inc.
 *    SUBSYSTEM NAME         : OFC
 *    MODULE NAME            : OFC GROUP MODULE
 *    LANGUAGE               : ANSI-C
 *    DATE OF FIRST RELEASE  :
 *    DESCRIPTION            : Definition of constants and structures used
 *                             across Flow Match/Instructions/Actions
 *---------------------------------------------------------------------------*/

#ifndef __OFCGROUP_H__
#define __OFCGROUP_H__

#include "ofcinc.h"
#include "ofc131pkt.h"
#include "ofcapi.h"
/* Group commands */
enum OFC_GROUP_MOD_COMMAND {
 OFCGC_ADD = 0,    /* New group. */
 OFCGC_MODIFY = 1,   /* Modify all matching groups. */
 OFCGC_DELETE = 2,   /* Delete all matching groups. */
 };

/* Group types. Values in the range [128, 255] are reserved for experimental   use. */

enum OFC_GROUP_TYPE {
 OFCGT_ALL = 0,    /* All (multicast/broadcast) group. */
 OFCGT_SELECT = 1,   /* Select group. */
 OFCGT_INDIRECT = 2,          /* Indirect group. */
 OFCGT_FF = 3,   /* Fast failover group. */
};                                      /* Group numbering. Groups can use any number up to OFCG_MAX. */
enum OFC_GROUP {
 OFCG_MAX = 0xffffff00,   /* Fake groups. */
 OFCG_ALL = 0xfffffffc,   /* Represents all groups for group delete commands. */
 OFCG_ANY = 0xffffffff   /* Wildcard group used only for flow stats  requests. Selects all flows regardless of group (including flows with no group).*/

};        /* Bucket for use in groups. */





PUBLIC VOID
Ofc131GroupHandleGroupModMsgs(tOfcFsofcCfgEntry *pOfc, tOfcFsofcGroupEntry *pGrpEntry,UINT2 u2cmd);

PUBLIC 
VOID Ofc131GrpMemReleaseBuckets(tOfcSll *pBucketList);

#endif  /* __OFCGROUP_H__  */
/*-----------------------------------------------------------------------*/
/*                       End of the file  ofcgroup.h                     */
/*-----------------------------------------------------------------------*/

