
/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved                      */
/* $Id: ofcvlan.h,v 1.3 2014/02/14 14:16:13 siva Exp $                                          */
/*                                                                           */
/*  FILE NAME             : ofcvlan.h                                        */
/*  PRINCIPAL AUTHOR      : Aricent Inc.                                     */
/*  SUBSYSTEM NAME        : OFC                                              */
/*  MODULE NAME           : OFC                                              */
/*  LANGUAGE              : C                                                */
/*  TARGET ENVIRONMENT    : Any                                              */
/*  DESCRIPTION           : This file contains definitions of macros used    */
/*                          in the OFC  Vlan module.                         */
/*                                                                           */
/*****************************************************************************/

#define OFC_VLAN_PORTS_PER_BYTE            8
#define OFC_VLAN_VLANS_PER_BYTE            8
#define OFC_VLAN_BIT8                      0x80
#define OFC_VLAN_MAX_PORTS                 SYS_DEF_MAX_PORTS_PER_CONTEXT
#define OFC_VLAN_IF_UP                     1
#define OFC_VLAN_IF_DOWN                   2
#define OPENFLOW_VLAN_ALIAS_PREFIX         ((const char *)"ofvlan")
#define  OFC_VLAN_PORTS_LEN                CONTEXT_PORT_LIST_SIZE

/* Adds the port list au1List2 to au1List1 - ie adds members of 
 * au1List2 to au1List1*/             
#define OFC_VLAN_ADD_PORT_LIST(au1List1, au1List2) \
{\
 UINT2 u2ByteIndex;\
 \
 for (u2ByteIndex = 0;\
   u2ByteIndex < VLAN_PORT_LIST_SIZE;\
   u2ByteIndex++) {\
  au1List1[u2ByteIndex] |= au1List2[u2ByteIndex];\
 }\
}

/* Removes members of au1List2 from au1List1 */
#define OFC_VLAN_RESET_PORT_LIST(au1List1, au1List2) \
              {\
         UINT2 u2ByteIndex;\
  \
  for (u2ByteIndex = 0;\
       u2ByteIndex < VLAN_PORT_LIST_SIZE;\
              u2ByteIndex++) {\
     au1List1[u2ByteIndex] &= (UINT1)~au1List2[u2ByteIndex];\
            }\
  }

#define OFC_VLAN_ADD_PORT_LENGTH(i4Length1, i4Length2) \
{\
 i4Length2 = i4Length1;\
}

INT4 OfcVlanCreate(UINT4 u4VlanId);
INT4 OfcVlanDelete(UINT4 u4VlanId);
INT4 OfcVlanConfigurePorts ( UINT4 u4VlanId , UINT1 *pu1MemberPorts ,
                  UINT1 *pu1UntaggedPorts , UINT4 u4Flag);

