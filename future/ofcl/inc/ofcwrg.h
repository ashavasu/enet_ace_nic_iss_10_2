/********************************************************************
* Copyright (C) 2013 Aricent Inc . All Rights Reserved
*
* $Id: ofcwrg.h,v 1.3 2014/01/31 13:03:56 siva Exp $
*
* Description: This file contains the prototype(wr) for Ofc 
*********************************************************************/
#ifndef _OFCWR_H
#define _OFCWR_H


INT4 GetNextIndexFsofcCfgTable(tSnmpIndex *, tSnmpIndex *);

INT4 GetNextIndexFsofcControllerConnTable(tSnmpIndex *, tSnmpIndex *);

INT4 GetNextIndexFsofcIfTable(tSnmpIndex *, tSnmpIndex *);

INT4 GetNextIndexFsofcFlowTable(tSnmpIndex *, tSnmpIndex *);

INT4 GetNextIndexFsofcGroupTable(tSnmpIndex *, tSnmpIndex *);

INT4 GetNextIndexFsofcMeterTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsofcModuleStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsofcSupportedVersionGet(tSnmpIndex *, tRetVal *);
INT4 FsofcDefaultFlowMissBehaviourGet(tSnmpIndex *, tRetVal *);
INT4 FsofcControlPktBufferingGet(tSnmpIndex *, tRetVal *);
INT4 FsofcIpReassembleStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsofcPortStpStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsofcTraceEnableGet(tSnmpIndex *, tRetVal *);
INT4 FsofcSwitchModeOnConnFailureGet(tSnmpIndex *, tRetVal *);
INT4 FsofcSwitchEntryStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsofcHybridGet(tSnmpIndex *, tRetVal *);
INT4 FsofcControllerConnPortGet(tSnmpIndex *, tRetVal *);
INT4 FsofcControllerConnProtocolGet(tSnmpIndex *, tRetVal *);
INT4 FsofcControllerRoleGet(tSnmpIndex *, tRetVal *);
INT4 FsofcControllerConnStateGet(tSnmpIndex *, tRetVal *);
INT4 FsofcControllerConnEchoReqCountGet(tSnmpIndex *, tRetVal *);
INT4 FsofcControllerConnEchoReplyCountGet(tSnmpIndex *, tRetVal *);
INT4 FsofcControllerConnEntryStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsofcControllerConnBandGet(tSnmpIndex *, tRetVal *);
INT4 FsofcIfTypeGet(tSnmpIndex *, tRetVal *);
INT4 FsofcIfAliasGet(tSnmpIndex *, tRetVal *);
INT4 FsofcIfOperStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsofcVlanEgressPortsGet(tSnmpIndex *, tRetVal *);
INT4 FsofcVlanUntaggedPortsGet(tSnmpIndex *, tRetVal *);
INT4 FsofcVlanInFramesGet(tSnmpIndex *, tRetVal *);
INT4 FsofcVlanOutFramesGet(tSnmpIndex *, tRetVal *);
INT4 FsofcIfContextIdGet(tSnmpIndex *, tRetVal *);
INT4 FsofcIfContextIdSet(tSnmpIndex *, tRetVal *);
INT4 FsofcVlanEgressPortsSet(tSnmpIndex *, tRetVal *);
INT4 FsofcVlanUntaggedPortsSet(tSnmpIndex *, tRetVal *);
INT4 FsofcVlanEgressPortsTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsofcVlanUntaggedPortsTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsofcFlowMatchFieldGet(tSnmpIndex *, tRetVal *);
INT4 FsofcFlowOutputActionGet(tSnmpIndex *, tRetVal *);
INT4 FsofcFlowIdleTimeoutGet(tSnmpIndex *, tRetVal *);
INT4 FsofcFlowHardTimeoutGet(tSnmpIndex *, tRetVal *);
INT4 FsofcFlowPacketCountGet(tSnmpIndex *, tRetVal *);
INT4 FsofcFlowByteCountGet(tSnmpIndex *, tRetVal *);
INT4 FsofcFlowDurationSecGet(tSnmpIndex *, tRetVal *);
INT4 FsofcGroupTypeGet(tSnmpIndex *, tRetVal *);
INT4 FsofcGroupActionBucketsGet(tSnmpIndex *, tRetVal *);
INT4 FsofcGroupPacketCountGet(tSnmpIndex *, tRetVal *);
INT4 FsofcGroupByteCountGet(tSnmpIndex *, tRetVal *);
INT4 FsofcGroupDurationSecGet(tSnmpIndex *, tRetVal *);
INT4 FsofcMeterBandInfoGet(tSnmpIndex *, tRetVal *);
INT4 FsofcMeterFlowCountGet(tSnmpIndex *, tRetVal *);
INT4 FsofcMeterPacketInCountGet(tSnmpIndex *, tRetVal *);
INT4 FsofcMeterByteInCountGet(tSnmpIndex *, tRetVal *);
INT4 FsofcMeterDurationSecGet(tSnmpIndex *, tRetVal *);
INT4 FsofcModuleStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsofcSupportedVersionTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsofcDefaultFlowMissBehaviourTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsofcControlPktBufferingTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsofcIpReassembleStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsofcPortStpStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsofcTraceEnableTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsofcSwitchModeOnConnFailureTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsofcSwitchEntryStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsofcHybridTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsofcControllerConnPortTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsofcControllerConnProtocolTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsofcControllerRoleTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsofcControllerConnEchoReqCountTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsofcControllerConnEntryStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsofcControllerConnBandTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsofcIfContextIdTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsofcModuleStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsofcSupportedVersionSet(tSnmpIndex *, tRetVal *);
INT4 FsofcDefaultFlowMissBehaviourSet(tSnmpIndex *, tRetVal *);
INT4 FsofcControlPktBufferingSet(tSnmpIndex *, tRetVal *);
INT4 FsofcIpReassembleStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsofcPortStpStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsofcTraceEnableSet(tSnmpIndex *, tRetVal *);
INT4 FsofcSwitchModeOnConnFailureSet(tSnmpIndex *, tRetVal *);
INT4 FsofcSwitchEntryStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsofcHybridSet(tSnmpIndex *, tRetVal *);
INT4 FsofcControllerConnPortSet(tSnmpIndex *, tRetVal *);
INT4 FsofcControllerConnProtocolSet(tSnmpIndex *, tRetVal *);
INT4 FsofcControllerRoleSet(tSnmpIndex *, tRetVal *);
INT4 FsofcControllerConnEchoReqCountSet(tSnmpIndex *, tRetVal *);
INT4 FsofcControllerConnEntryStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsofcControllerConnBandSet(tSnmpIndex *, tRetVal *);
INT4 FsofcCfgTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 FsofcControllerConnTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 FsofcIfTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);

VOID  RegisterFSOFC PROTO ((VOID));
VOID  UnRegisterFSOFC PROTO ((VOID));

#endif /* _OFCWR_H */
