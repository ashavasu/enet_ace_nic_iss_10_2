/******************************************************************************
 * Copyright (C) 2013 Aricent Inc . All Rights Reserved
 *
 * $Id: ofchybrid.h,v 1.2 2014/03/01 11:36:44 siva Exp $
 *
 * Description: This file is specific to hybrid processing in OpenFlow 
 ******************************************************************************/

#define OFC_HYBRID_PORT_L2  1
#define OFC_HYBRID_PORT_L3  2

typedef struct
{
    UINT4 u4HybridPort;    /* Hybrid Port Number */
    UINT1 u1IntfType;      /* Interface type - L2 or L3 */
    UINT1 u1Set;           /* CLI Successful */
    UINT1 au1Pad[2];       /* Future Use */
}tOfcHybridPortDb;

UINT4 OfcHybridPortCheck (UINT4 u4ContextId, INT4 i4IntfType, INT4 i4IntfIdx);
UINT4 OfcHybridPortCreate (UINT4 u4ContextId, INT4 i4IntfType, INT4 i4IntfIdx);
UINT4 OfcHybridPortDelete (UINT4 u4ContextId);
UINT4 OfcHybridSendPktToIss (UINT4 u4ContextId, tCRU_BUF_CHAIN_HEADER *pBuf);
UINT4 OfcHybridFloodOnAllPorts (tOfcFsofcCfgEntry *pOfcFsofcCfgEntry,
                                UINT4 u4IfIndex, tCRU_BUF_CHAIN_HEADER *pBuf);
