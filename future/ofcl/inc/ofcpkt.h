/********************************************************************
* Copyright (C) 2013 Aricent Inc . All Rights Reserved
*
* $Id: ofcpkt.h,v 1.2 2014/01/31 13:03:56 siva Exp $
*
* Description: This file contains the Ofc pkt process related routines
*********************************************************************/
#ifndef _OFCPKT_H_
#define _OFCPKT_H_

#define OFC_OFP_U1VERSION_VALIDATE_ERR 0
#define OFC_OFP_U1TYPE_VALIDATE_ERR 1
#define OFC_OFP_U2LENGTH_VALIDATE_ERR 2
#define OFC_OFP_U4XID_VALIDATE_ERR 3
#define OFC_HELLO_AU1DUMMY_VALIDATE_ERR 4
#define OFC_ECHOREQMSG_AU1DATA_VALIDATE_ERR 5
#define OFC_ECHORPLYMSG_AU1DATA_VALIDATE_ERR 6
#define OFC_EXPERIMENTERHDR_U4EXPERIMENTER_VALIDATE_ERR 7
#define OFC_FEATUREREQ_AU1DUMMY_VALIDATE_ERR 8
#define OFC_CONFIGGET_AU1DUMMY_VALIDATE_ERR 9
#define OFC_CONFIGSET_U2FLAGS_VALIDATE_ERR 10
#define OFC_CONFIGSET_U2MISSSENDLEN_VALIDATE_ERR 11
#define OFC_PKTOUT_U4BUFFERID_VALIDATE_ERR 12
#define OFC_PKTOUT_U2INPORT_VALIDATE_ERR 13
#define OFC_PKTOUT_U2ACTIONSLEN_VALIDATE_ERR 14
#define OFC_PKTOUT_ACTIONS_VALIDATE_ERR 15
#define OFC_FLOWMOD_MATCH_VALIDATE_ERR 16
#define OFC_FLOWMOD_U8COOKIE_VALIDATE_ERR 17
#define OFC_FLOWMOD_U2COMMAND_VALIDATE_ERR 18
#define OFC_FLOWMOD_U2IDLETIMEOUT_VALIDATE_ERR 19
#define OFC_FLOWMOD_U2HARDTIMEOUT_VALIDATE_ERR 20
#define OFC_FLOWMOD_U2PRIORITY_VALIDATE_ERR 21
#define OFC_FLOWMOD_U4BUFID_VALIDATE_ERR 22
#define OFC_FLOWMOD_U2OUTPORT_VALIDATE_ERR 23
#define OFC_FLOWMOD_U2FLAGS_VALIDATE_ERR 24
#define OFC_FLOWMOD_ACTIONS_VALIDATE_ERR 25
#define OFC_PORTMOD_U4PORTNO_VALIDATE_ERR 26
#define OFC_PORTMOD_AU1PAD_VALIDATE_ERR 27
#define OFC_PORTMOD_AU1HWADDR_VALIDATE_ERR 28
#define OFC_PORTMOD_AU1PAD2_VALIDATE_ERR 29
#define OFC_PORTMOD_U4CONFIG_VALIDATE_ERR 30
#define OFC_PORTMOD_U4MASK_VALIDATE_ERR 31
#define OFC_PORTMOD_U4ADVERTISE_VALIDATE_ERR 32
#define OFC_PORTMOD_AU1PAD3_VALIDATE_ERR 33
#define OFC_STATSREQ_U2TYPE_VALIDATE_ERR 34
#define OFC_STATSREQ_U2FLAGS_VALIDATE_ERR 35
#define OFC_STATSREQ_AU1BODY_VALIDATE_ERR 36
#define OFC_BARRIERREQ_AU1DUMMY_VALIDATE_ERR 37
#define OFC_QUEUEREQ_U2PORT_VALIDATE_ERR 38
#define OFC_QUEUEREQ_AU1PAD_VALIDATE_ERR 39

#define OFPBAC_BAD_VENDOR 2 

typedef struct _Hello {
 UINT1 au1Dummy[0];
}tOfpHello;
typedef struct _ErrMsg {
 UINT2 u2Type;
 UINT2 u2Code;
 UINT1 au1Data[0];
}tOfpErrMsg;
typedef struct _EchoReqMsg {
 UINT1 au1Data[0];
}tOfpEchoReqMsg;
typedef struct _EchoRplyMsg {
 UINT1 au1Data[0];
}tOfpEchoRplyMsg;
typedef struct _ExperimenterHdr {
 UINT4 u4Experimenter;
}tOfpExperimenterHdr;
typedef struct _FeatureReq {
 UINT1 au1Dummy[0];
}tOfpFeatureReq;
typedef struct _PhyPort {
 UINT2 u2Port;
 UINT1 au1HwAddr[6];
 UINT1 au1Name[16];
 UINT4 u4Config;
 UINT4 u4State;
 UINT4 u4Curr;
 UINT4 u4Advertised;
 UINT4 u4Supported;
 UINT4 u4Peer;
}tOfpPhyPort;
typedef struct _FeatureRply {
 FS_UINT8 u8DatapathiId;
 UINT4 u4NoOfBuf;
 UINT1 u1NoOfTables;
 UINT1 au1Pad[3];
 UINT4 u4Capabilities;
 UINT4 u4Actions;
 tOfpPhyPort PhyPort[0];
}tOfpFeatureRply;
typedef struct _ConfigGet {
 UINT1 au1Dummy[0];
}tOfpConfigGet;
typedef struct _ConfigRply {
 UINT2 u2Flags;
 UINT2 u2MissSendLen;
}tOfpConfigRply;
typedef struct _ConfigSet {
 UINT2 u2Flags;
 UINT2 u2MissSendLen;
}tOfpConfigSet;
typedef struct _PktIn {
 UINT4 u4BufId;
 UINT2 u2TotLen;
 UINT2 u2InPort;
 UINT1 u1Reason;
 UINT1 u1Pad;
 UINT1 au1Data[2];
}tOfpPktIn;
typedef struct _Match {
 UINT4 u4WildCard;
 UINT2 u2InPort;
 UINT1 au1DlSrc[6];
 UINT1 au1DlDst[6];
 UINT2 u2DlVlan;
 UINT1 u1DlVlanPcap;
 UINT1 au1Pad1[1];
 UINT2 u2DlType;
 UINT1 u1NwTos;
 UINT1 u1NwProto;
 UINT1 au1Pad2[2];
 UINT4 u4NwSrc;
 UINT4 u4NwDst;
 UINT2 u4TpSrc;
 UINT2 u4TpDst;
}tOfpMatch;
typedef struct _FlowRem {
 tOfpMatch Match;
 FS_UINT8 u8Cookie;
 UINT2 u2Priority;
 UINT1 u1Reason;
 UINT1 au1Pad[1];
 UINT4 u4DurationSec;
 UINT4 u4DurationNSec;
 UINT2 u2IdleTimeOut;
 UINT1 au1Pad2[2];
 FS_UINT8 u8PacketCount;
 FS_UINT8 u8ByteCount;
}tOfpFlowRem;
typedef struct _PortStatus {
 UINT1 u1Reason;
 UINT1 au1Pad[7];
 tOfpPhyPort PhyPort;
}tOfpPortStatus;
typedef struct _ActionHdr {
 UINT2 u2Type;
 UINT2 u2Len;
 UINT1 au1Pad[4];
}tOfpActionHdr;
typedef struct _PktOut {
 UINT4 u4BufferId;
 UINT2 u2InPort;
 UINT2 u2ActionsLen;
 tOfpActionHdr Actions[0];
}tOfpPktOut;
typedef struct _OfpInstruction {
 UINT2 u2Type;
 UINT2 u2Len;
}tOfpInstruction;
typedef struct _FlowMod {
 tOfpMatch Match;
 FS_UINT8 u8Cookie;
 UINT2 u2Command;
 UINT2 u2IdleTimeout;
 UINT2 u2HardTimeout;
 UINT2 u2Priority;
 UINT4 u4BufId;
 UINT2 u2OutPort;
 UINT2 u2Flags;
 tOfpActionHdr Actions[0];
}tOfpFlowMod;
typedef struct _PortMod {
 UINT2 u2PortNo;
 UINT1 au1Pad[2];
 UINT1 au1HwAddr[6];
 UINT1 au1Pad2[2];
 UINT4 u4Config;
 UINT4 u4Mask;
 UINT4 u4Advertise;
 UINT1 au1Pad3[4];
}tOfpPortMod;
typedef struct _StatsReq {
 UINT2 u2Type;
 UINT2 u2Flags;
 UINT1 au1Body[0];
}tOfpStatsReq;
typedef struct _StatsRply {
 UINT2 u2Type;
 UINT2 u2Flags;
 UINT1 au1Body[0];
}tOfpStatsRply;
typedef struct _BarrierReq {
 UINT1 au1Dummy[0];
}tOfpBarrierReq;
typedef struct _BarrierRply {
 UINT1 au1Dummy[0];
}tOfpBarrierRply;
typedef struct _QueueReq {
 UINT2 u2Port;
 UINT1 au1Pad[2];
}tOfpQueueReq;
typedef struct _QueueHdr {
 UINT2 u2Property;
 UINT2 u2Len;
 UINT1 au1Pad[4];
}tOfpQueueHdr;
typedef struct _PktQueue {
 UINT4 u4QueueId;
 UINT2 u2Len;
 UINT1 au1Pad[2];
 tOfpQueueHdr OfpQueueHdr[0];
}tOfpPktQueue;
typedef struct _QueueRply {
 UINT2 u2Port;
 UINT1 au1Pad[6];
 tOfpPktQueue PktQueues[0];
}tOfpQueueRply;
typedef struct _Ofp {
 UINT1 u1Version;
 UINT1 u1Type;
 UINT2 u2Length;
 UINT4 u4Xid;
 union {
  tOfpHello Hello;
  tOfpErrMsg ErrMsg;
  tOfpEchoReqMsg EchoReqMsg;
  tOfpEchoRplyMsg EchoRplyMsg;
  tOfpExperimenterHdr ExperimenterHdr;
  tOfpFeatureReq FeatureReq;
  tOfpFeatureRply FeatureRply;
  tOfpConfigGet ConfigGet;
  tOfpConfigRply ConfigRply;
  tOfpConfigSet ConfigSet;
  tOfpPktIn PktIn;
  tOfpFlowRem FlowRem;
  tOfpPortStatus PortStatus;
  tOfpPktOut PktOut;
  tOfpFlowMod FlowMod;
  tOfpPortMod PortMod;
  tOfpStatsReq StatsReq;
  tOfpStatsRply StatsRply;
  tOfpBarrierReq BarrierReq;
  tOfpBarrierRply BarrierRply;
  tOfpQueueReq QueueReq;
  tOfpQueueRply QueueRply;
 }hl;
}tOfpPkt;

/* Rx Validate Proto types */
INT4 OfcOfpu1VersionValidate(tOfpPkt *,UINT4 *,VOID *);
INT4 OfcOfpu1TypeValidate(tOfpPkt *,UINT4 *,VOID *);
INT4 OfcOfpu2LengthValidate(tOfpPkt *,UINT4 *,VOID *);
INT4 OfcOfpu4XidValidate(tOfpPkt *,UINT4 *,VOID *);
INT4 OfcHelloau1DummyValidate(tOfpHello *,UINT4 *,VOID *);
INT4 OfcEchoReqMsgau1DataValidate(tOfpEchoReqMsg *,UINT4 *,VOID *);
INT4 OfcEchoRplyMsgau1DataValidate(tOfpEchoRplyMsg *,UINT4 *,VOID *);
INT4 OfcExperimenterHdru4ExperimenterValidate(tOfpExperimenterHdr *,UINT4 *,VOID *);
INT4 OfcFeatureReqau1DummyValidate(tOfpFeatureReq *,UINT4 *,VOID *);
INT4 OfcConfigGetau1DummyValidate(tOfpConfigGet *,UINT4 *,VOID *);
INT4 OfcConfigSetu2FlagsValidate(tOfpConfigSet *,UINT4 *,VOID *);
INT4 OfcConfigSetu2MissSendLenValidate(tOfpConfigSet *,UINT4 *,VOID *);
INT4 OfcPktOutu4BufferIdValidate(tOfpPktOut *,UINT4 *,VOID *);
INT4 OfcPktOutu2InPortValidate(tOfpPktOut *,UINT4 *,VOID *);
INT4 OfcPktOutu2ActionsLenValidate(tOfpPktOut *,UINT4 *,VOID *);
INT4 OfcPktOutActionsValidate(tOfpPktOut *,UINT4 *,VOID *);
INT4 OfcFlowModMatchValidate(tOfpFlowMod *,UINT4 *,VOID *);
INT4 OfcFlowModu8CookieValidate(tOfpFlowMod *,UINT4 *,VOID *);
INT4 OfcFlowModu2CommandValidate(tOfpFlowMod *,UINT4 *,VOID *);
INT4 OfcFlowModu2IdleTimeoutValidate(tOfpFlowMod *,UINT4 *,VOID *);
INT4 OfcFlowModu2HardTimeoutValidate(tOfpFlowMod *,UINT4 *,VOID *);
INT4 OfcFlowModu2PriorityValidate(tOfpFlowMod *,UINT4 *,VOID *);
INT4 OfcFlowModu4BufIdValidate(tOfpFlowMod *,UINT4 *,VOID *);
INT4 OfcFlowModu2OutPortValidate(tOfpFlowMod *,UINT4 *,VOID *);
INT4 OfcFlowModu2FlagsValidate(tOfpFlowMod *,UINT4 *,VOID *);
INT4 OfcFlowModActionsValidate(tOfpFlowMod *,UINT4 *,VOID *);
INT4 OfcPortModu4PortNoValidate(tOfpPortMod *,UINT4 *,VOID *);
INT4 OfcPortModau1PadValidate(tOfpPortMod *,UINT4 *,VOID *);
INT4 OfcPortModau1HwAddrValidate(tOfpPortMod *,UINT4 *,VOID *);
INT4 OfcPortModau1Pad2Validate(tOfpPortMod *,UINT4 *,VOID *);
INT4 OfcPortModu4ConfigValidate(tOfpPortMod *,UINT4 *,VOID *);
INT4 OfcPortModu4MaskValidate(tOfpPortMod *,UINT4 *,VOID *);
INT4 OfcPortModu4AdvertiseValidate(tOfpPortMod *,UINT4 *,VOID *);
INT4 OfcPortModau1Pad3Validate(tOfpPortMod *,UINT4 *,VOID *);
INT4 OfcStatsRequ2TypeValidate(tOfpStatsReq *,UINT4 *,VOID *);
INT4 OfcStatsRequ2FlagsValidate(tOfpStatsReq *,UINT4 *,VOID *);
INT4 OfcStatsReqau1BodyValidate(tOfpStatsReq *,UINT4 *,VOID *);
INT4 OfcBarrierReqau1DummyValidate(tOfpBarrierReq *,UINT4 *,VOID *);
INT4 OfcQueueRequ2PortValidate(tOfpQueueReq *,UINT4 *,VOID *);
INT4 OfcQueueReqau1PadValidate(tOfpQueueReq *,UINT4 *,VOID *);

/* Rx Action Proto types */
INT4 OfcOfpu1VersionAction(tOfpPkt *,UINT4 *,VOID *);
INT4 OfcOfpu1TypeAction(tOfpPkt *,UINT4 *,VOID *);
INT4 OfcOfpu2LengthAction(tOfpPkt *,UINT4 *,VOID *);
INT4 OfcOfpu4XidAction(tOfpPkt *,UINT4 *,VOID *);
INT4 OfcHelloau1DummyAction(tOfpHello *,UINT4 *,VOID *);
INT4 OfcEchoReqMsgau1DataAction(tOfpEchoReqMsg *,UINT4 *,VOID *);
INT4 OfcEchoRplyMsgau1DataAction(tOfpEchoRplyMsg *,UINT4 *,VOID *);
INT4 OfcExperimenterHdru4ExperimenterAction(tOfpExperimenterHdr *,UINT4 *,VOID *);
INT4 OfcFeatureReqau1DummyAction(tOfpFeatureReq *,UINT4 *,VOID *);
INT4 OfcConfigGetau1DummyAction(tOfpConfigGet *,UINT4 *,VOID *);
INT4 OfcConfigSetu2FlagsAction(tOfpConfigSet *,UINT4 *,VOID *);
INT4 OfcConfigSetu2MissSendLenAction(tOfpConfigSet *,UINT4 *,VOID *);
INT4 OfcPktOutu4BufferIdAction(tOfpPktOut *,UINT4 *,VOID *);
INT4 OfcPktOutu2InPortAction(tOfpPktOut *,UINT4 *,VOID *);
INT4 OfcPktOutu2ActionsLenAction(tOfpPktOut *,UINT4 *,VOID *);
INT4 OfcPktOutActionsAction(tOfpPktOut *,UINT4 *,VOID *);
INT4 OfcFlowModMatchAction(tOfpFlowMod *,UINT4 *,VOID *);
INT4 OfcFlowModu8CookieAction(tOfpFlowMod *,UINT4 *,VOID *);
INT4 OfcFlowModu2CommandAction(tOfpFlowMod *,UINT4 *,VOID *);
INT4 OfcFlowModu2IdleTimeoutAction(tOfpFlowMod *,UINT4 *,VOID *);
INT4 OfcFlowModu2HardTimeoutAction(tOfpFlowMod *,UINT4 *,VOID *);
INT4 OfcFlowModu2PriorityAction(tOfpFlowMod *,UINT4 *,VOID *);
INT4 OfcFlowModu4BufIdAction(tOfpFlowMod *,UINT4 *,VOID *);
INT4 OfcFlowModu2OutPortAction(tOfpFlowMod *,UINT4 *,VOID *);
INT4 OfcFlowModu2FlagsAction(tOfpFlowMod *,UINT4 *,VOID *);
INT4 OfcFlowModActionsAction(tOfpFlowMod *,UINT4 *,VOID *);
INT4 OfcPortModu4PortNoAction(tOfpPortMod *,UINT4 *,VOID *);
INT4 OfcPortModau1PadAction(tOfpPortMod *,UINT4 *,VOID *);
INT4 OfcPortModau1HwAddrAction(tOfpPortMod *,UINT4 *,VOID *);
INT4 OfcPortModau1Pad2Action(tOfpPortMod *,UINT4 *,VOID *);
INT4 OfcPortModu4ConfigAction(tOfpPortMod *,UINT4 *,VOID *);
INT4 OfcPortModu4MaskAction(tOfpPortMod *,UINT4 *,VOID *);
INT4 OfcPortModu4AdvertiseAction(tOfpPortMod *,UINT4 *,VOID *);
INT4 OfcPortModau1Pad3Action(tOfpPortMod *,UINT4 *,VOID *);
INT4 OfcStatsRequ2TypeAction(tOfpStatsReq *,UINT4 *,VOID *);
INT4 OfcStatsRequ2FlagsAction(tOfpStatsReq *,UINT4 *,VOID *);
INT4 OfcStatsReqau1BodyAction(tOfpStatsReq *,UINT4 *,VOID *);
INT4 OfcBarrierReqau1DummyAction(tOfpBarrierReq *,UINT4 *,VOID *);
INT4 OfcQueueRequ2PortAction(tOfpQueueReq *,UINT4 *,VOID *);
INT4 OfcQueueReqau1PadAction(tOfpQueueReq *,UINT4 *,VOID *);

/* Rx Parse Proto types */
INT4 OfcOfpPktParse(tOfpPkt *,UINT4 *,VOID *);
INT4 OfcHelloPktParse(tOfpHello *,UINT4 *,VOID *);
INT4 OfcEchoReqMsgPktParse(tOfpEchoReqMsg *,UINT4 *,VOID *);
INT4 OfcEchoRplyMsgPktParse(tOfpEchoRplyMsg *,UINT4 *,VOID *);
INT4 OfcExperimenterHdrPktParse(tOfpExperimenterHdr *,UINT4 *,VOID *);
INT4 OfcFeatureReqPktParse(tOfpFeatureReq *,UINT4 *,VOID *);
INT4 OfcConfigGetPktParse(tOfpConfigGet *,UINT4 *,VOID *);
INT4 OfcConfigSetPktParse(tOfpConfigSet *,UINT4 *,VOID *);
INT4 OfcPktOutPktParse(tOfpPktOut *,UINT4 *,VOID *);
INT4 OfcFlowModPktParse(tOfpFlowMod *,UINT4 *,VOID *);
INT4 OfcPortModPktParse(tOfpPortMod *,UINT4 *,VOID *);
INT4 OfcStatsReqPktParse(tOfpStatsReq *,UINT4 *,VOID *);
INT4 OfcBarrierReqPktParse(tOfpBarrierReq *,UINT4 *,VOID *);
INT4 OfcQueueReqPktParse(tOfpQueueReq *,UINT4 *,VOID *);
INT4 OfcPktParse(UINT1* pBuf,VOID *);
VOID OfcPktErrorHandle(tOfpPkt*,UINT4,VOID *);

/* Tx Send Proto types */
INT4 OfcOfpPktSend(tOfpPkt *,VOID *);
INT4 OfcHelloPktSend(tOfpHello *,VOID *);
INT4 OfcErrMsgPktSend(tOfpErrMsg *,VOID *);
INT4 OfcEchoReqMsgPktSend(tOfpEchoReqMsg *,VOID *);
INT4 OfcEchoRplyMsgPktSend(tOfpEchoRplyMsg *,VOID *);
INT4 OfcExperimenterHdrPktSend(tOfpExperimenterHdr *,VOID *);
INT4 OfcFeatureRplyPktSend(tOfpFeatureRply *,VOID *);
INT4 OfcConfigRplyPktSend(tOfpConfigRply *,VOID *);
INT4 OfcPktInPktSend(tOfpPktIn *,VOID *);
INT4 OfcFlowRemPktSend(tOfpFlowRem *,VOID *);
INT4 OfcPortStatusPktSend(tOfpPortStatus *,VOID *);
INT4 OfcStatsRplyPktSend(tOfpStatsRply *,VOID *);
INT4 OfcBarrierRplyPktSend(tOfpBarrierRply *,VOID *);
INT4 OfcPktSend(UINT1 *,UINT4,VOID *);

/********************** manual code ***************/
#define OFP_VERSION        0x1
#define OFP_HEADER_LEN     8
#define OFP_MAX_BUF_CACHE  0
#define OFP_MAX_NO_TABLES  1 

enum OfpType {
 /* Immutable messages. */
 OFPT_HELLO, /* Symmetric message */
 OFPT_ERROR, /* Symmetric message */
 OFPT_ECHO_REQUEST, /* Symmetric message */
 OFPT_ECHO_REPLY, /* Symmetric message */
 OFPT_VENDOR, /* Symmetric message */
 /* Switch configuration messages. */
 OFPT_FEATURES_REQUEST, /* Controller/switch message */
 OFPT_FEATURES_REPLY, /* Controller/switch message */
 OFPT_GET_CONFIG_REQUEST, /* Controller/switch message */
 OFPT_GET_CONFIG_REPLY, /* Controller/switch message */
 OFPT_SET_CONFIG, /* Controller/switch message */
 /* Asynchronous messages. */
 OFPT_PACKET_IN, /* Async message */
 OFPT_FLOW_REMOVED, /* Async message */
 OFPT_PORT_STATUS, /* Async message */
 /* Controller command messages. */
 OFPT_PACKET_OUT, /* Controller/switch message */
 OFPT_FLOW_MOD, /* Controller/switch message */
 OFPT_PORT_MOD, /* Controller/switch message */
 /* Statistics messages. */
 OFPT_STATS_REQUEST, /* Controller/switch message */
 OFPT_STATS_REPLY, /* Controller/switch message */
 /* Barrier messages. */
 OFPT_BARRIER_REQUEST, /* Controller/switch message */
 OFPT_BARRIER_REPLY, /* Controller/switch message */
 /* Queue Configuration messages. */
 OFPT_QUEUE_GET_CONFIG_REQUEST, /* Controller/switch message */
 OFPT_QUEUE_GET_CONFIG_REPLY /* Controller/switch message */
};

enum OfpCapabilities {
 OFPC_FLOW_STATS = 1 << 0, /* Flow statistics. */
 OFPC_TABLE_STATS = 1 << 1, /* Table statistics. */
 OFPC_PORT_STATS = 1 << 2, /* Port statistics. */
 OFPC_STP = 1 << 3, /* 802.1d spanning tree. */
 OFPC_RESERVED = 1 << 4, /* Reserved, must be zero. */
 OFPC_IP_REASM = 1 << 5, /* Can reassemble IP fragments. */
 OFPC_QUEUE_STATS = 1 << 6, /* Queue statistics. */
 OFPC_ARP_MATCH_IP = 1 << 7 /* Match IP addresses in ARP pkts. */
};

enum ofpActionType {
 OFPAT_OUTPUT, /* Output to switch port. */
 OFPAT_SET_VLAN_VID, /* Set the 802.1q VLAN id. */
 OFPAT_SET_VLAN_PCP, /* Set the 802.1q priority. */
 OFPAT_STRIP_VLAN, /* Strip the 802.1q header. */
 OFPAT_SET_DL_SRC, /* Ethernet source address. */
 OFPAT_SET_DL_DST, /* Ethernet destination address. */
 OFPAT_SET_NW_SRC, /* IP source address. */
 OFPAT_SET_NW_DST, /* IP destination address. */
 OFPAT_SET_NW_TOS, /* IP ToS (DSCP field, 6 bits). */
 OFPAT_SET_TP_SRC, /* TCP/UDP source port. */
 OFPAT_SET_TP_DST, /* TCP/UDP destination port. */
 OFPAT_ENQUEUE /* Output to queue. */
};

enum OfpPortConf {
OFPPC_PORT_DOWN = 1 << 0, /* Port is administratively down. */
OFPPC_NO_STP = 1 << 1, /* Disable 802.1D spanning tree on port. */
OFPPC_NO_RECV = 1 << 2, /* Drop all packets except 802.1D spanning tree packets. */
OFPPC_NO_RECV_STP = 1 << 3, /* Drop received 802.1D STP packets. */
OFPPC_NO_FLOOD = 1 << 4, /* Do not include this port when flooding. */
OFPPC_NO_FWD = 1 << 5, /* Drop packets forwarded to port. */
OFPPC_NO_PACKET_IN = 1 << 6 /* Do not send packet-in msgs for port. */
#define OFC_PORT_CONFIG_ALL (OFPPC_PORT_DOWN | OFPPC_NO_STP | OFPPC_NO_RECV | OFPPC_NO_RECV_STP |\
                             OFPPC_NO_FLOOD | OFPPC_NO_FWD | OFPPC_NO_PACKET_IN)
};


enum OfpPortState {
OFPPS_LINK_UP =  0,       /* physical link present. */
OFPPS_LINK_DOWN = 1 << 0, /* No physical link present. */
OFPPS_STP_LISTEN = 0 << 8, /* Not learning or relaying frames. */
OFPPS_STP_LEARN = 1 << 8, /* Learning but not relaying frames. */
OFPPS_STP_FORWARD = 2 << 8, /* Learning and relaying frames. */
OFPPS_STP_BLOCK = 3 << 8, /* Not part of spanning tree. */
OFPPS_STP_MASK = 3 << 8 /* Bit mask for OFPPS_STP_* values. */
};

enum OfpConfigFlags {
OFPC_FRAG_NORMAL = 0, /* No special handling for fragments. */
OFPC_FRAG_DROP = 1, /* Drop fragments. */
OFPC_FRAG_REASM = 2, /* Reassemble (only if OFPC_IP_REASM set). */
OFPC_FRAG_MASK = 3
};



enum OfpStatsTypes {
OFPST_DESC,
OFPST_FLOW,
OFPST_AGGREGATE,
OFPST_TABLE,
OFPST_PORT,
OFPST_QUEUE,
OFPST_VENDOR = 0xffff
};

#define OFP_DESC_STR_LEN   256
#define OFP_SERIAL_NUM_LEN 32

typedef struct _OFPDESCSTATS {
UINT1 au1MfrDesc[OFP_DESC_STR_LEN]; /* Manufacturer description. */
UINT1 au1HwDesc[OFP_DESC_STR_LEN]; /* Hardware description. */
UINT1 au1SwDesc[OFP_DESC_STR_LEN]; /* Software description. */
UINT1 au1SerialNum[OFP_SERIAL_NUM_LEN]; /* Serial number. */
UINT1 au1DpDesc[OFP_DESC_STR_LEN]; /* Human readable description of datapath. */
}tOfpDescStats;


typedef struct _OFPFLOWSTATSREQ {
 tOfpMatch Match;
        UINT1 u1TableId; 
        UINT1 u1Pad; 
        UINT2 u2OutPort;
}tOfpFlowStatsReq;

typedef struct _OFPFLOWSTATSRPLY {
UINT2 u2Length; /* Length of this entry. */
UINT1 u1TableId; /* ID of table flow came from. */
UINT1 u1pad;
tOfpMatch Match;
UINT4 u4DurationSec; /* Time flow has been alive in seconds. */
UINT4 u4DurationNSec; 
UINT2 u2Priority; 
UINT2 u2IdleTimeout; /* Number of seconds idle before expiration. */
UINT2 u2HardTimeout; /* Number of seconds before expiration. */
UINT1 au1Pad2[6]; /* Align to 64-bits. */
FS_UINT8 u8Cookie; /* Opaque controller-issued identifier. */
FS_UINT8 u8PktCount; /* Number of packets in flow. */
FS_UINT8 u8ByteCount; /* Number of bytes in flow. */
tOfpActionHdr Actions[1];
}tOfpFlowStatsRply;

typedef struct _OFPPORTSTATSREQ {
UINT2 u2PortNo;
UINT1 au1Pad[6];
}tOfpPortStatsReq;

typedef struct _OFPPORTSTATS {
UINT2 u2PortNo;
UINT1 au1Pad[6];
FS_UINT8 u8RxPkts; 
FS_UINT8 u8TxPkts;
FS_UINT8 u8RxBytes; 
FS_UINT8 u8TxBytes; 
FS_UINT8 u8RxDropped; 
FS_UINT8 u8TxDropped;
FS_UINT8 u8RxErrors; 
FS_UINT8 u8TxErrors;
FS_UINT8 u8RxFrameErr; 
FS_UINT8 u8RxOverErr;
FS_UINT8 u8RxCrcErr; 
FS_UINT8 u8Collisions; 
}tOfpPortStats;

typedef struct _OFPAGGSTATSRPLY {
FS_UINT8 u8PktCount;
FS_UINT8 u8ByteCount;
UINT4    u4FlowCount;
UINT1    au1Pad[4];
}tOfpAggStatsRply;

#endif /* _OFCPKT_H_ */
