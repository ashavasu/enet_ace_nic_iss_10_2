/*****************************************************************************
 * Copyright (C) 2013 Aricent Inc . All Rights Reserved
 *
 * $Id: ofctdfs.h,v 1.7 2015/02/27 10:54:25 siva Exp $
 *
 * Description: This file contains type definitions for Ofc module.
 *****************************************************************************/

#ifndef INCLUDE_OFCTDFS_H
#define INCLUDE_OFCTDFS_H

#include "ofcdefn.h"
#include "ofcpkt.h"
#include "ofcinstr.h"
#include "ofc131pkt.h"

#define OFCQ_MIN_RATE_UNCF 0xffff
#define OFCQ_MAX_RATE_UNCF 0xffff

typedef struct
{
    tRBTree  FsofcCfgTable;
    tRBTree  FsofcControllerConnTable;
    tRBTree  FsofcIfTable;
    tRBTree  FsofcFlowTable;
    tRBTree  FsofcGroupTable;
    tRBTree  FsofcMeterTable;
} tOfcGlbMib;

typedef struct
{
    tOfcMibFsofcCfgEntry  MibObject;
    tTMO_HASH_TABLE      *pFlowtable;       /* v100 */
    UINT4                 u4OfcClientState;
    tRBTree               OfcFlowtable;     /* v131 - Entries with Wildcards */
    tTMO_DLL              OfcExactFlowList; /* v131 - Exact Matching Entries */
    UINT1                 u1GenerationIsDefined;
    UINT1                 au1Pad[1];
    UINT2                 u2NumOfTables;    /* Flow tables in this Context */
    FS_UINT8              u8GenerationId;
    UINT1                *pDataBuf;
    UINT4                 u4CurrConnEntries;/* Current Connection entries */
} tOfcFsofcCfgEntry;

typedef struct
{
    tOfcMibFsofcControllerConnEntry  MibObject;
    tOfcTmr                          ConnRetryTimer;
    tOfcTmr                          EchoReqTimer;
    tOfcTmr                          EchoInterTimer;
    INT4                             i4SocketDesc;
    VOID                            *pssl;
    INT4                             i4ConnRetryCount;
    UINT4                            u4IsRecieved;
    UINT4                            u4RxXid;
    UINT4                            u4TxXid;
    UINT4                            u4RxLength;
    UINT4                            u4TxDataLength; /* Excluded OFP header */
    UINT1                           *pRcvPkt;
    UINT4                            u4PendingLen;
    UINT4                            u4ReceivedLen;
    UINT4                            au4PktInMask[2];
    UINT4                            au4PortStatusMask[2];
    UINT4                            au4FlowRemovedMask[2];
} tOfcFsofcControllerConnEntry;

typedef enum {
    OFCQT_MIN_RATE      = 1,     /* Minimum datarate guaranteed. */
    OFCQT_MAX_RATE      = 2,     /* Maximum datarate. */
    OFCQT_EXPERIMENTER  = 0xffff /* Experimenter defined property. */
} eOfcQueProperties;

typedef struct {
    UINT2  u2Property;
    UINT2  u2Rate;
}tOfcQueueProp;

typedef struct {
    UINT4          u4QueueId;
    tOfcQueueProp  QueueProp[2]; /* Only Min and Max rate for now */
}tOfcQueue;

typedef struct
{
    tOfcMibFsofcIfEntry  MibObject;
    UINT4                u4Config; 
    UINT4                u4VlanId;
    UINT1                au1HwAddr[OFC_ETH_ADDR_LEN];
    UINT1                au1Pad[2];
    UINT1                au1Name[OFC_MAX_PORT_NAME_LEN]; 
    tOfcQueue            Queue[1]; /* Only one Queue for now */
#ifdef NPAPI_WANTED
    UINT4                u4DflFlowIndex;
#endif
} tOfcFsofcIfEntry;

typedef struct
{
    tRBNodeEmbd       RBNode;
    tTMO_HASH_TABLE  *pFlowtable;        /* V131 */
    tRBTree           ExactFlowEntry;    /* RB Tree for Flow Entry */
    UINT4             u4TableIndex;
    UINT1             au1Pad[2];
    UINT1             u1NumMatch;
    UINT1             u1InstrsBitMap;    /* Instructions BitMap */
    UINT4             u4NxtTblsBitMap;   /* Next Tables BitMap */
    UINT4             u4WrtActsBitMap;   /* Write Actions BitMap */
    UINT4             u4AppActsBitMap;   /* Apply Actions BitMap */
    FS_UINT8          u8FlowMatchBitMap; /* Flow Match BitMap */
    FS_UINT8          u8WildCardsBitMap; /* WildCards BitMap */
    FS_UINT8          u8WrtStFldsBitMap; /* AS SetField BitMap */
    FS_UINT8          u8AppStFldsBitMap; /* AS SetField BitMap */
    UINT4             u4ActiveCount;     /* Active entries */
    FS_UINT8          u8LookupCount;     /* Packets looked up in table */
    FS_UINT8          u8MatchedCount;    /* Packets that hit table */
    FS_UINT8          u8MetadataMatch;   /* Metadata bits to match */
    FS_UINT8          u8MetadataWrite;   /* Metadata bits to write */
    UINT4             u4MaxEntries;      /* Max entries supported */
    UINT4             u4ContextId;       /* BackPointer to Context */ 
    UINT1             au1Name[32];       /* Table Name */
} tOfcFsofcFlowTable;

typedef struct
{
    tRBNodeEmbd   RBNode;
    tOfcSll       FlowEntryList;
    UINT1         au1ExactMatch[MAX_OFC_EXACT_LENGTH];
} tRBExactFlowEntry;

typedef struct
{
    tTMO_SLL_NODE          Node; /* SLL node for FlowEntryList or Hash Table */
    tOfcMibFsofcFlowEntry  MibObject;
    tOfcTmr                OfcHardTimer;
    tOfcTmr                OfcIdleTimer;
    FS_UINT8               u8Cookie;
    FS_UINT8               u8CookieMask;
    UINT2                  u2Flags;
    UINT2                  u2Priority;
    UINT4                  u4BufId;
    UINT4                  u4OutPort;
    UINT4                  u4OutGrp;
    UINT4                  u4TableIndex;
    UINT1                  au1Pad[2];
    UINT1                  u1IsFlowWild;
    UINT1                  u1FlowMatchCount;
    tOfcSll                MatchList;
    tOfcSll                InstrList;
    tRBExactFlowEntry     *pRBExactFlowEntry; /* back pointer */
    tOfcFsofcFlowTable    *pFlowTableEntry;   /* back pointer */ 
} tOfcFsofcFlowEntry;

typedef struct
{
    tOfcMibFsofcGroupEntry  MibObject;
    tOfcSll                 BucketList; /* tOfcGroupBucket */
} tOfcFsofcGroupEntry;

typedef struct
{
    tOfcSllNode   Node;
    UINT4         u4WatchPort;   /* Only required for fast failover groups */
    UINT4         u4WatchGroup;  /* Only required for fast failover groups */
    UINT2         u2Weight;      /* Only defined for select groups */
    UINT1         u1SelectFlag;
    UINT1         au1pad[1];
    UINT4         u4NumActs;
    tOfcSll       ActList;       /* SLL to action list tOfcActs */
} tOfcGroupBucket;

typedef struct
{
    tOfcMibFsofcMeterEntry  MibObject;
    UINT2                   u2Flags;
    UINT1                   au1Pad[2];
    tOfcSll                 BandList;  /* tOfcMeterBand */
} tOfcFsofcMeterEntry;

typedef union
{
    UINT1  u1PrecLvl;
    UINT4  u4Experimenter;
} unOfcBandFlds;

typedef struct
{
    tOfcSllNode    Node;
    UINT2          u2Type;       /* One of OFPMBT_* */
    UINT1          au1pad[2];
    UINT4          u4Rate;       /* Rate for this band */
    UINT4          u4BurstSize;  /* One of OFPMBT_* */
    unOfcBandFlds  BandFlds; 
} tOfcMeterBand;

/*
 * Queue Message structure
 */
typedef struct
{
    VOID                   *pPortStat;
    UINT4                   u4VlanId;
    UINT4                   u4MsgType;    /* Message type */
    UINT4                   u4IfIndex;
    UINT4                   u4Index;      /* logical index */
    UINT4                   u4ContextId;
    INT4                    i4SockFd;
    UINT1                  *pu1MemberPorts;
    UINT1                  *pu1UntaggedPorts;
    UINT4                   u4Flag;
    UINT1                   u1TableId;
    UINT1                   au1Pad[3];
    FS_UINT8                u8Cookie;
    tCRU_BUF_CHAIN_HEADER  *pBuf;
    tOfp131Match           *pOfcMatch;
} tOfcQueMsg;

typedef struct OFC_GLOBALS
{
    tTimerListId  ofcTmrLst;
    UINT4         u4OfcTrc;
    tOsixTaskId   ofcTaskId;
    UINT1         au1TaskSemName[8];
    tOsixSemId    ofcTaskSemId;
    tOsixQId      ofcQueId;
    tOfcGlbMib    OfcGlbMib;
} tOfcGlobals;

#endif  /* INCLUDE_OFCTDFS_H */
