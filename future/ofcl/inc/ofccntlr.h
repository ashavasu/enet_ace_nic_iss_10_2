/********************************************************************
 * Copyright (C) 2013 Aricent Inc . All Rights Reserved
 *
 * $Id: ofccntlr.h,v 1.2 2014/01/31 13:03:56 siva Exp $
 *
 * Description: Related Protocol Structures and Macros
 **********************************************************************/

#ifndef __OFCCNTLR_H__
#define __OFCCNTLR_H__

#include "ofcpkt.h"

enum ofp_controller_role {
    OFPCR_ROLE_NOCHANGE = 0, /* Don't change current role. */
    OFPCR_ROLE_EQUAL    = 1, /* Default role, full access. */
    OFPCR_ROLE_MASTER   = 2, /* Full access, at most one master. */
    OFPCR_ROLE_SLAVE    = 3, /* Read-only access. */
};


INT4 OfcUpdateControllerConnRole(tOfp131RoleReq *pOfcRoleRequest, UINT4 u4ContextId);
INT4 OfcUpdateControllerConnState(tOfcFsofcControllerConnEntry *pOfcFsofcControllerConnEntry,
                                  INT4 i4FsofcControllerConnState);
INT4 FsofcCheckGenerationId(tOfp131RoleReq *OfcRoleRequest, UINT4 u4ContextId);


#endif  /* __OFCCNTLR_H__  */
