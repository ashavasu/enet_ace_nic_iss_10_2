/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: ofcidxsz.h,v 1.2 2014/04/30 10:10:26 siva Exp $
*
* Description: This file contains sizing related MACROS
*
*******************************************************************/
#include "ofcinc.h"
#include "ofcidxmg.h"
extern tIndexMgrGrpInfo *gpOfcIndexMgrGrpInfo;
enum {
    MAX_INDEXMGR_INDEX_MGR_CHUNK_SIZING_ID,
    MAX_INDEXMGR_INDEX_MGR_GRP_SIZING_ID,
    MAX_INDEXMGR_INDEX_TBL_CHUNK_SIZING_ID,
    INDEXMGR_MAX_SIZING_ID
};


#ifdef  _OFCIDXSZ_C
tMemPoolId OFCINDEXMGRMemPoolIds[INDEXMGR_MAX_SIZING_ID];
INT4  OfcIndexmgrSizingMemCreateMemPools(VOID);
VOID  OfcIndexmgrSizingMemDeleteMemPools(VOID);
INT4  OfcIndexmgrSzRegisterModuleSizingParams( CHR1 *pu1ModName);
#else  /*  _OFCIDXSZ_C  */
extern tMemPoolId OFCINDEXMGRMemPoolIds[ ];
extern INT4  OfcIndexmgrSizingMemCreateMemPools(VOID);
extern VOID  OfcIndexmgrSizingMemDeleteMemPools(VOID);
extern INT4  OfcIndexmgrSzRegisterModuleSizingParams( CHR1 *pu1ModName); 
#endif /*  _OFCIDXSZ_C  */


#ifdef  _OFCIDXSZ_C
tFsModSizingParams FsOFCINDEXMGRSizingParams [] = {
{ "tIndexMgrChunkInfoSize", "MAX_OFC_INDEXMGR_INDEX_MGR_CHUNK", sizeof(tIndexMgrChunkInfoSize),MAX_OFC_INDEXMGR_INDEX_MGR_CHUNK, MAX_OFC_INDEXMGR_INDEX_MGR_CHUNK,0 },
{ "tIndexMgrGrpInfoSize", "MAX_OFC_INDEXMGR_INDEX_MGR_GRP", sizeof(tIndexMgrGrpInfoSize),MAX_OFC_INDEXMGR_INDEX_MGR_GRP, MAX_OFC_INDEXMGR_INDEX_MGR_GRP,0 },
{ "tIndexMgrChunkSize", "MAX_OFC_INDEXMGR_INDEX_TBL_CHUNK", sizeof(tIndexMgrChunkSize),MAX_OFC_INDEXMGR_INDEX_TBL_CHUNK, MAX_OFC_INDEXMGR_INDEX_TBL_CHUNK,0 },
{"\0","\0",0,0,0,0}
};
#else  /*  _OFCIDXSZ_C  */
extern tFsModSizingParams FsOFCINDEXMGRSizingParams [];
#endif /*  _OFCIDXSZ_C  */


