/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * Description: This file contains declaration of global variables 
 *              of ofc module.
 *******************************************************************/

#ifndef __OFCGLOBAL_H__
#define __OFCGLOBAL_H__

tOfcGlobals gOfcGlobals;

#endif  /* __OFCGLOBAL_H__ */


/*-----------------------------------------------------------------------*/
/*                       End of the file ofcglob.h                      */
/*-----------------------------------------------------------------------*/
