/**********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: secv4main.c,v 1.12 2015/01/19 11:35:15 siva Exp $
 *
 * Description: This has functions for SecMain SubModule 
 *
 ***********************************************************************/

#include "secv4esp.h"
#include "secv4com.h"
#include "secv4frgtyp.h"

 /*****************************************************************************/
 /*  Function Name : Secv4InMain                                              */
 /*  Description   : This function gets the incomming packet and              */
 /*                : calls either one of the Sec Decoder(AH/ESP)              */
 /*                : algorithm based on SA Entry                              */
 /*  Input(s)      : pBufDesc  -  Buffer contains Secured IP packet           */
 /*                : pSaEntry  - Pointer to a SA Entry                        */
 /*                : u4ProcessedLen - Length upto which the pkt is processed  */
 /*                : u4IfIndex - The interafce index on which the pkt arrived */
 /*  Output(s)     : pBuf - buffer contains IP packet                         */
 /*  Return Values : SEC_SUCCESS or SEC_FAILURE                               */
 /*****************************************************************************/

UINT1
Secv4InMain (tCRU_BUF_CHAIN_HEADER * pCBuf, tSecv4Assoc * pSaEntry,
             UINT4 u4ProcessedLen, UINT4 u4IfIndex, t_IP_HEADER * pIpHdr)
{
    UINT4               u4CallBackFunctionIndex = SEC_MAX_FUNCTIONS;
    UINT1               u1RetVal = SEC_FAILURE;

    /* Call Decoder based on protocol  and Mode specified in SA Entry */
    if (((pSaEntry->u1SecAssocProtocol != SEC_AH)
         && (pSaEntry->u1SecAssocProtocol != SEC_ESP))
        || ((pSaEntry->u1SecAssocMode != SEC_TRANSPORT)
            && (pSaEntry->u1SecAssocMode != SEC_TUNNEL)))
    {
        SECv4_TRC (SECv4_DATA_PATH,
                   "Secv4InMain : Either Protocol or Mode is invalid \n");
        return SEC_FAILURE;
    }

    u4CallBackFunctionIndex = gau1GetProcessFnPtr
        [pSaEntry->u1SecAssocProtocol -
         SEC_PROTO_OFFSET_FOR_INDEX]
        [pSaEntry->u1SecAssocMode - SEC_MODE_OFFSET_FOR_INDEX];

    if (u4CallBackFunctionIndex >= SEC_MAX_FUNCTIONS)
    {
        SECv4_TRC (SECv4_DATA_PATH,
                   "Secv4InMain : Callback function index is out of bounds \n");
        return SEC_FAILURE;
    }
    u1RetVal =
        (UINT1) (*DecodeProcedure[u4CallBackFunctionIndex]) (pCBuf, pSaEntry,
                                                             u4ProcessedLen,
                                                             u4IfIndex, pIpHdr);
    return (u1RetVal);
}

 /****************************************************************************/
 /*  Function Name : Secv4OutMain                                            */
 /*  Description   : This function gets the Outgoing packet and              */
 /*                : calls either one of the Sec Encoder(AH/ESP)             */
 /*                : algorithm based on SA Entry                             */
 /*  Input(s)      : pBufDesc     -  Pointer to Buffer containing IP packet  */
 /*                : pSaEntry     -  Pointer to a SA Entry                   */
 /*                : u4IfIndex    -  The IfIndex on which the Pkt arrived    */
 /*                :                 and after processing by Algo            */
 /*  Output(s)     : pBuf          - Buffer contains Secured IP packet       */
 /*  Return Values : SEC_SUCCESS or SEC_FAILURE                              */
 /****************************************************************************/

UINT1
Secv4OutMain (tCRU_BUF_CHAIN_HEADER * pCBuf, tSecv4Assoc * pSaEntry,
              UINT4 u4IfIndex, t_IP_HEADER * pIpHdr)
{
    UINT4               u4CallBackFunctionIndex = SEC_MAX_FUNCTIONS;
    UINT1               u1RetVal = SEC_FAILURE;
    UINT1               u1NextHdr = 0;

    /* Call Decoder based on protocol  and Mode specified in SA Entry */
    if (((pSaEntry->u1SecAssocProtocol != SEC_AH)
         && (pSaEntry->u1SecAssocProtocol != SEC_ESP))
        || ((pSaEntry->u1SecAssocMode != SEC_TRANSPORT)
            && (pSaEntry->u1SecAssocMode != SEC_TUNNEL)))
    {
        SECv4_TRC (SECv4_DATA_PATH,
                   "Secv4OutMain: Either Protocol or Mode is invalid \n");
        return SEC_FAILURE;
    }

    if (pSaEntry->u1SecAssocMode == SEC_TUNNEL)
    {
        u1NextHdr = SEC_IP_IN_IP;
    }
    else
    {
        u1NextHdr = pIpHdr->u1Proto;
    }

    u4CallBackFunctionIndex = gau1GetProcessFnPtr
        [pSaEntry->u1SecAssocProtocol -
         SEC_PROTO_OFFSET_FOR_INDEX]
        [pSaEntry->u1SecAssocMode - SEC_MODE_OFFSET_FOR_INDEX];

    if (u4CallBackFunctionIndex >= SEC_MAX_FUNCTIONS)
    {
        SECv4_TRC (SECv4_DATA_PATH,
                   "Secv4OutMain : Callback function "
                   "index is out of bounds \n");
        return SEC_FAILURE;
    }
    /* Changing The CallBack Function Index, to point to UDP Encap Tun Mode */
    if (((pSaEntry->IkeNattInfo.u1NattFlags & IKE_USE_NATT_PORT) ==
         IKE_USE_NATT_PORT) &&
        (((pSaEntry->IkeNattInfo.u1NattFlags & IKE_PEER_BEHIND_NAT) ==
          IKE_PEER_BEHIND_NAT) ||
         ((pSaEntry->IkeNattInfo.u1NattFlags & IKE_BEHIND_NAT) ==
          IKE_BEHIND_NAT)))
    {
        if (u4CallBackFunctionIndex == SEC_ESP_TUNNEL_ENCODE_INDEX)
        {
            u4CallBackFunctionIndex = SEC_ESP_UDP_TUNNEL_ENCODE_INDEX;
        }
    }

    u1RetVal =
        (UINT1) (*EncodeProcedure[u4CallBackFunctionIndex]) (pCBuf, pSaEntry,
                                                             u4IfIndex, pIpHdr,
                                                             u1NextHdr);
    if (u1RetVal == SEC_FAILURE)
    {
        SECv4_TRC (SECv4_DATA_PATH, "Secv4OutMain: Encode function failed \n");
        return SEC_FAILURE;
    }
    return (u1RetVal);
}

/****************************************************************************/
/*                                                                          */
/* Function     : Secv4Lock                                                 */
/*                                                                          */
/* Description  : Takes the IPsec Lock                                      */
/*                                                                          */
/* Input        : None                                                      */
/*                                                                          */
/*                pu4Flags  - Pointer to the Flags for the lock             */
/* Output       : None                                                      */
/*                                                                          */
/* Returns      : None.                                                     */
/*                                                                          */
/****************************************************************************/
VOID
Secv4Lock ()
{
#ifndef SECURITY_KERNEL_WANTED
    OsixSemTake (gSecv4SemId);
#endif
    return;
}

/****************************************************************************/
/*                                                                          */
/* Function     : Secv4UnLock                                               */
/*                                                                          */
/* Description  : Releases the IPSec Lock                                   */
/*                                                                          */
/* Input        : None                                                      */
/*                                                                          */
/*                pu4Flags  - Pointer to the Flags for the lock             */
/* Output       : None                                                      */
/*                                                                          */
/* Returns      : None.                                                     */
/*                                                                          */
/****************************************************************************/
VOID
Secv4UnLock ()
{
#ifndef SECURITY_KERNEL_WANTED
    OsixSemGive (gSecv4SemId);
#endif
    return;
}

/****************************************************************************/
/*                                                                          */
/* Function     : Secv4KernelLock                                           */
/*                                                                          */
/* Description  : Takes the IPsec kernel lock                               */
/*                                                                          */
/* Input        : None                                                      */
/*                                                                          */
/*                pu4Flags  - Pointer to the Flags for the lock             */
/* Output       : None                                                      */
/*                                                                          */
/* Returns      : None.                                                     */
/*                                                                          */
/****************************************************************************/
VOID
Secv4KernelLock ()
{
    /* When compiling for kernel, the Secv4Lock(), locks need to be taken
     * at different places. This function will be called for Kernel Lock.
     */
#ifdef SECURITY_KERNEL_WANTED
    OsixSemTake (gSecv4SemId);
#endif
    return;
}

/****************************************************************************/
/*                                                                          */
/* Function     : Secv4KernelUnLock                                         */
/*                                                                          */
/* Description  : Releases the IPSec kernel Lock                            */
/*                                                                          */
/* Input        : None                                                      */
/*                                                                          */
/*                pu4Flags  - Pointer to the Flags for the lock             */
/* Output       : None                                                      */
/*                                                                          */
/* Returns      : None.                                                     */
/*                                                                          */
/****************************************************************************/
VOID
Secv4KernelUnLock ()
{
    /* When compiling for kernel, the Secv4UnLock(), locks need to be released
     * at different places. This function will be called for Kernel Lock.
     */
#ifdef SECURITY_KERNEL_WANTED
    OsixSemGive (gSecv4SemId);
#endif
    return;
}

/****************************************************************************/
/*                                                                          */
/* Function     : Secv4HandleDummyPktGenTmrExpiry                           */
/*                                                                          */
/* Description  : This function handles the dummy packet gen tmr expiry     */
/*                                                                          */
/* Input        : None                                                      */
/*                                                                          */
/* Output       : None                                                      */
/*                                                                          */
/* Returns      : None.                                                     */
/*                                                                          */
/****************************************************************************/
VOID
Secv4HandleDummyPktGenTmrExpiry ()
{
    tSecv4Selector     *pSelEntry = NULL;
    tSecv4Policy       *pPolicyEntry = NULL;
    tSecv4Assoc        *pSaEntry = NULL;
#ifdef SECURITY_KERNEL_WANTED
    tCRU_BUF_CHAIN_HEADER *pBuf = NULL;
    tSecQueMsg          SecQueMsg;
    tSecv4DummyMsg     *pSecv4DummyMsg = NULL;
    INT1                i1RetVal = 0;
#endif
    UINT2               u2Count = 0;

    TMO_SLL_Scan (&Secv4SelFormattedList, pSelEntry, tSecv4Selector *)
    {
        if (pSelEntry != NULL)
        {
            pPolicyEntry = pSelEntry->pPolicyEntry;
        }
        if ((pPolicyEntry != NULL) &&
            (pPolicyEntry->u1SaCount < SEC_MAX_BUNDLE_SA))
        {
            for (u2Count = 0; u2Count < pPolicyEntry->u1SaCount; u2Count++)
            {
                Secv4Lock ();
                /* Unlock in case of failure/given to user from kernel. 
                 * In success case, Secv4AlgoProcessPkt will Unlock  */
                pSaEntry = pPolicyEntry->pau1SaEntry[u2Count];
                if ((pSaEntry != NULL) &&
                    (SecUtilIpIfIsOurAddress (pSaEntry->u4SecAssocSrcAddr) ==
                     TRUE))
                {
#ifdef SECURITY_KERNEL_WANTED
                    /* IPSECv4 is in kernel, hence */
                    /* post to user in order to resolve the dest IP addr */
                    SecQueMsg.u1MsgType = SEC_MSG_TO_SECV4_FROM_KERNEL_DUMMY;
                    pSecv4DummyMsg = &SecQueMsg.ModuleParam.Secv4DummyMsg;
                    pSecv4DummyMsg->pSaEntry = (UINT4 *) pSaEntry;
                    pSecv4DummyMsg->u4Addr = pSaEntry->u4SecAssocDestAddr;
                    pSecv4DummyMsg->u4IfIndex = pSelEntry->u4IfIndex;
                    MEMSET (pSecv4DummyMsg->au1MacAddr, 0, sizeof (tMacAddr));

                    pBuf =
                        SEC_CRU_BUF_Allocate_MsgBufChain (sizeof (tSecQueMsg), 0);
                    if (pBuf == NULL)
                    {
                        return;
                    }

                    CRU_BUF_Copy_OverBufChain (pBuf, (UINT1 *) &SecQueMsg, 0,
                                               sizeof (tSecQueMsg));

                    /* Post the buffer to CFA queue */
                    i1RetVal = OsixQueSend (gu4SecReadModIdx,
                                            (UINT1 *) &pBuf, OSIX_DEF_MSG_LEN);

                    if (i1RetVal != OSIX_SUCCESS)
                    {
                        SEC_CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
                        return;
                    }
                    Secv4UnLock ();
#else
                    if (Secv4DummyPktGen (pSaEntry, pSelEntry->u4IfIndex) ==
                        SEC_FAILURE)
                    {
                        Secv4UnLock ();
                    }
#endif
                }
                else
                {
                    Secv4UnLock ();
                }
            }
        }
    }
    Secv4StartTimer (&gSecv4TmrDummyPkt, SECv4_DUMMY_PKT_TIMER_ID,
                     SECv4_DUMMY_TMR_INTERVAL);
    return;
}

/****************************************************************************/
/*                                                                          */
/* Function     : Secv4DummyPktGen                                          */
/*                                                                          */
/* Description  : This function is to facilitate the rapid generation of    */
/*                the padding traffic in support of traffic                 */
/*                flow confidentiality                                      */
/*                                                                          */
/* Input        : Sa Entry - For which Dummy pkt has to be generated        */
/*                If Index - Appropriate IfIndex where dummy has to be sent */
/*                                                                          */
/* Output       : None                                                      */
/*                                                                          */
/* Returns      : None.                                                     */
/*                                                                          */
/****************************************************************************/
UINT4
Secv4DummyPktGen (tSecv4Assoc * pSaEntry, UINT4 u4IfIndex)
{
    tCRU_BUF_CHAIN_HEADER *pBuf = NULL;
    t_IP_HEADER         IpHdr;
    tCfaIfInfo          sCfaIfInfo;
    UINT1               au1Buf[SEC_MAX_PAD_LEN];
    UINT4               u4BufSize = 0;
    UINT4               u4DataOffset = 0;
    UINT4               u4DummyPktSize = 0;
    UINT4               u4CallBackFunctionIndex = 0;
    UINT2               u2Count = 0;
    UINT1               u1RetVal = 0;
    UINT1               u1NextHdr = 0;
    UINT1               u1EncapType = 0;

    IPSEC_MEMSET (&IpHdr, 0, sizeof (t_IP_HEADER));
    IPSEC_MEMSET (au1Buf, 0, sizeof (au1Buf));

    if (pSaEntry == NULL)
    {
        return SEC_FAILURE;
    }

    /* Get a new CRU Buffer for the dummy packet */

    u4BufSize = ISS_CUST_DEFAULT_MTU_SIZE;
    u4DataOffset = SECv4_MAC_HDR_LEN - IP_HDR_LEN;

    pBuf = CRU_BUF_Allocate_MsgBufChain (u4BufSize, u4DataOffset);
    if (pBuf == NULL)
    {
        return SEC_FAILURE;
    }

    SecUtilGetIfInfo (u4IfIndex, &sCfaIfInfo);
    MEMCPY (SEC_GET_MODULE_DATA_PTR (pBuf)->DestMACAddr, sCfaIfInfo.au1MacAddr,
            CFA_ENET_ADDR_LEN);
    MEMCPY (SEC_GET_MODULE_DATA_PTR (pBuf)->SrcMACAddr, sCfaIfInfo.au1MacAddr,
            CFA_ENET_ADDR_LEN);

    if (Secv4ArpResolve (pSaEntry->u4SecAssocDestAddr,
                         (INT1 *) SEC_GET_MODULE_DATA_PTR (pBuf)->DestMACAddr,
                         &u1EncapType) == SEC_FAILURE)
    {
        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
        return SEC_FAILURE;
    }

    SEC_GET_MODULE_DATA_PTR (pBuf)->u2LenOrType = CFA_ENET_IPV4;

    /* Packet type has Dummy */
    u1NextHdr = SEC_NO_NEXT_HDR;

    /* Packet Size hardcode */
    u4DummyPktSize = 25;

    /* Fill the Dummy packet with sequence numbers */
    for (u2Count = 0; u2Count < u4DummyPktSize; u2Count++)
    {
        au1Buf[u2Count] = (UINT1) u2Count;
    }

    if (CRU_BUF_Copy_OverBufChain (pBuf, au1Buf, 0,
                                   u4DummyPktSize) == CRU_FAILURE)
    {
        SECv4_TRC (SECv4_CONTROL_PLANE, "\n Secv4DummyPktGen: -"
                   "Copy over cru buffer failed for adding,"
                   "Dummy Payload \n");
        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
        return SEC_FAILURE;
    }

    IpHdr.u1Ver_hdrlen = SEC_VERS_AND_HLEN (SEC_V4, 0);
    IpHdr.u1Tos = 0;
    IpHdr.u2Totlen =
        (UINT2) (IPSEC_HTONS ((u4DummyPktSize + SEC_IPV4_HEADER_SIZE)));
    IpHdr.u2Id = 0;
    IpHdr.u2Fl_offs = 0;
    IpHdr.u1Ttl = SEC_MAX_HOP_LIMIT;
    IpHdr.u1Proto = pSaEntry->u1SecAssocProtocol;
    IpHdr.u2Cksum = 0;
    IpHdr.u4Src = IPSEC_HTONL (pSaEntry->u4SecAssocSrcAddr);
    IpHdr.u4Dest = IPSEC_HTONL (pSaEntry->u4SecAssocDestAddr);
    IpHdr.u2Cksum = Secv4IPCalcChkSum ((UINT1 *) &IpHdr, SEC_IPV4_HEADER_SIZE);
    IpHdr.u2Cksum = IPSEC_HTONS (IpHdr.u2Cksum);

    if (IPSEC_BUF_Prepend (pBuf, (UINT1 *) &IpHdr,
                           SEC_IPV4_HEADER_SIZE) == BUF_FAILURE)
    {
        SECv4_TRC (SECv4_CONTROL_PLANE, "\nSecv4DummyPktGen :-"
                   "Prepend buffer failed to IP header for dummy pkt\n");
        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
        return (SEC_FAILURE);
    }
    /* Call Encoder based on protocol  and Mode specified in SA Entry */
    if (((pSaEntry->u1SecAssocProtocol != SEC_AH)
         && (pSaEntry->u1SecAssocProtocol != SEC_ESP))
        || ((pSaEntry->u1SecAssocMode != SEC_TRANSPORT)
            && (pSaEntry->u1SecAssocMode != SEC_TUNNEL)))
    {
        SECv4_TRC (SECv4_DATA_PATH,
                   "Secv4DummyPktGen: Either Protocol or Mode is invalid \n");
        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
        return SEC_FAILURE;
    }

    u4CallBackFunctionIndex = gau1GetProcessFnPtr
        [pSaEntry->u1SecAssocProtocol -
         SEC_PROTO_OFFSET_FOR_INDEX]
        [pSaEntry->u1SecAssocMode - SEC_MODE_OFFSET_FOR_INDEX];

    if (u4CallBackFunctionIndex >= SEC_MAX_FUNCTIONS)
    {
        SECv4_TRC (SECv4_DATA_PATH,
                   "Secv4DummyPktGen : Callback function "
                   "index is out of bounds \n");
        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
        return SEC_FAILURE;
    }
    /* Changing The CallBack Function Index, to point to UDP Encap Tun Mode */
    if (((pSaEntry->IkeNattInfo.u1NattFlags & IKE_USE_NATT_PORT) ==
         IKE_USE_NATT_PORT) &&
        (((pSaEntry->IkeNattInfo.u1NattFlags & IKE_PEER_BEHIND_NAT) ==
          IKE_PEER_BEHIND_NAT) ||
         ((pSaEntry->IkeNattInfo.u1NattFlags & IKE_BEHIND_NAT) ==
          IKE_BEHIND_NAT)))
    {
        if (u4CallBackFunctionIndex == SEC_ESP_TUNNEL_ENCODE_INDEX)
        {
            u4CallBackFunctionIndex = SEC_ESP_UDP_TUNNEL_ENCODE_INDEX;
        }
    }

    u1RetVal =
        (UINT1) (*EncodeProcedure[u4CallBackFunctionIndex]) (pBuf, pSaEntry,
                                                             u4IfIndex, &IpHdr,
                                                             u1NextHdr);
    if (u1RetVal == SEC_FAILURE)
    {
        SECv4_TRC (SECv4_DATA_PATH,
                   "Secv4DummyPktGen: Encode function failed \n");
        return SEC_FAILURE;
    }
    return (u1RetVal);
}

/****************************************************************************/
/*                                                                          */
/* Function     : Secv4DummyPktGenKern                                      */
/*                                                                          */
/* Description  : This function is to facilitate the rapid generation of    */
/*                the padding traffic in support of traffic                 */
/*                flow confidentiality                                      */
/*                                                                          */
/* Input        : Sa Entry - For which Dummy pkt has to be generated        */
/*                If Index - Appropriate IfIndex where dummy has to be sent */
/*                                                                          */
/* Output       : None                                                      */
/*                                                                          */
/* Returns      : None.                                                     */
/*                                                                          */
/****************************************************************************/

UINT4
Secv4DummyPktGenKern (tSecv4Assoc * pSaEntry, UINT4 u4IfIndex,
                      tMacAddr au1DestAddr)
{
    tCRU_BUF_CHAIN_HEADER *pBuf = NULL;
    t_IP_HEADER         IpHdr;
    tCfaIfInfo          sCfaIfInfo;
    UINT1               au1Buf[SEC_MAX_PAD_LEN];
    UINT4               u4BufSize = 0;
    UINT4               u4DataOffset = 0;
    UINT4               u4DummyPktSize = 0;
    UINT4               u4CallBackFunctionIndex = 0;
    UINT2               u2Count = 0;
    UINT1               u1RetVal = 0;
    UINT1               u1NextHdr = 0;

    IPSEC_MEMSET (&IpHdr, 0, sizeof (t_IP_HEADER));
    IPSEC_MEMSET (au1Buf, 0, sizeof (au1Buf));

    if (pSaEntry == NULL)
    {
        SECv4_TRC (SECv4_CONTROL_PLANE, "\n Secv4DummyPktGen: -"
                   "SA entry is NULL \n\r");
        return SEC_FAILURE;
    }

    /* Get a new CRU Buffer for the dummy packet */

    u4BufSize = ISS_CUST_DEFAULT_MTU_SIZE;
    u4DataOffset = SECv4_MAC_HDR_LEN - IP_HDR_LEN;

    pBuf = CRU_BUF_Allocate_MsgBufChain (u4BufSize, u4DataOffset);
    if (pBuf == NULL)
    {
        return SEC_FAILURE;
    }

    SecUtilGetIfInfo (u4IfIndex, &sCfaIfInfo);
    MEMCPY (SEC_GET_MODULE_DATA_PTR (pBuf)->DestMACAddr, au1DestAddr,
            CFA_ENET_ADDR_LEN);
    MEMCPY (SEC_GET_MODULE_DATA_PTR (pBuf)->SrcMACAddr, sCfaIfInfo.au1MacAddr,
            CFA_ENET_ADDR_LEN);

    SEC_GET_MODULE_DATA_PTR (pBuf)->u2LenOrType = CFA_ENET_IPV4;
    SEC_GET_DIRECTION (pBuf) = SEC_OUTBOUND;
    SEC_GET_IFINDEX (pBuf) = u4IfIndex;

    /* Packet type has Dummy */
    u1NextHdr = SEC_NO_NEXT_HDR;

    /* Packet Size hardcode */
    u4DummyPktSize = gSecv4DummyPktLength;

    /* Fill the Dummy packet with sequence numbers */
    for (u2Count = 0; u2Count < u4DummyPktSize; u2Count++)
    {
        au1Buf[u2Count] = (UINT1) u2Count;
    }

    if (CRU_BUF_Copy_OverBufChain (pBuf, au1Buf, 0,
                                   u4DummyPktSize) == CRU_FAILURE)
    {
        SECv4_TRC (SECv4_CONTROL_PLANE, "\n Secv4DummyPktGen: -"
                   "Copy over cru buffer failed for adding,"
                   "Dummy Payload \n");
        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
        return SEC_FAILURE;
    }

    IpHdr.u1Ver_hdrlen = SEC_VERS_AND_HLEN (SEC_V4, 0);
    IpHdr.u1Tos = 0;
    IpHdr.u2Totlen =
        (UINT2) (IPSEC_HTONS ((u4DummyPktSize + SEC_IPV4_HEADER_SIZE)));
    IpHdr.u2Id = 0;
    IpHdr.u2Fl_offs = 0;
    IpHdr.u1Ttl = SEC_MAX_HOP_LIMIT;
    IpHdr.u1Proto = pSaEntry->u1SecAssocProtocol;
    IpHdr.u2Cksum = 0;
    IpHdr.u4Src = IPSEC_HTONL (pSaEntry->u4SecAssocSrcAddr);
    IpHdr.u4Dest = IPSEC_HTONL (pSaEntry->u4SecAssocDestAddr);
    IpHdr.u2Cksum = Secv4IPCalcChkSum ((UINT1 *) &IpHdr, SEC_IPV4_HEADER_SIZE);
    IpHdr.u2Cksum = IPSEC_HTONS (IpHdr.u2Cksum);

    if (IPSEC_BUF_Prepend (pBuf, (UINT1 *) &IpHdr,
                           SEC_IPV4_HEADER_SIZE) == BUF_FAILURE)
    {
        SECv4_TRC (SECv4_CONTROL_PLANE, "\nSecv4DummyPktGen :-"
                   "Prepend buffer failed to IP header for dummy pkt\n");
        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
        return (SEC_FAILURE);
    }
    /* Call Encoder based on protocol  and Mode specified in SA Entry */
    if (((pSaEntry->u1SecAssocProtocol != SEC_AH)
         && (pSaEntry->u1SecAssocProtocol != SEC_ESP))
        || ((pSaEntry->u1SecAssocMode != SEC_TRANSPORT)
            && (pSaEntry->u1SecAssocMode != SEC_TUNNEL)))
    {
        SECv4_TRC (SECv4_DATA_PATH,
                   "Secv4DummyPktGen: Either Protocol or Mode is invalid \n");
        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
        return SEC_FAILURE;
    }

    u4CallBackFunctionIndex = gau1GetProcessFnPtr
        [pSaEntry->u1SecAssocProtocol -
         SEC_PROTO_OFFSET_FOR_INDEX]
        [pSaEntry->u1SecAssocMode - SEC_MODE_OFFSET_FOR_INDEX];

    if (u4CallBackFunctionIndex >= SEC_MAX_FUNCTIONS)
    {
        SECv4_TRC (SECv4_DATA_PATH,
                   "Secv4DummyPktGen : Callback function "
                   "index is out of bounds \n");
        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
        return SEC_FAILURE;
    }
    /* Changing The CallBack Function Index, to point to UDP Encap Tun Mode */
    if (((pSaEntry->IkeNattInfo.u1NattFlags & IKE_USE_NATT_PORT) ==
         IKE_USE_NATT_PORT) &&
        (((pSaEntry->IkeNattInfo.u1NattFlags & IKE_PEER_BEHIND_NAT) ==
          IKE_PEER_BEHIND_NAT) ||
         ((pSaEntry->IkeNattInfo.u1NattFlags & IKE_BEHIND_NAT) ==
          IKE_BEHIND_NAT)))
    {
        if (u4CallBackFunctionIndex == SEC_ESP_TUNNEL_ENCODE_INDEX)
        {
            u4CallBackFunctionIndex = SEC_ESP_UDP_TUNNEL_ENCODE_INDEX;
        }
    }

    u1RetVal =
        (UINT1) (*EncodeProcedure[u4CallBackFunctionIndex]) (pBuf, pSaEntry,
                                                             u4IfIndex, &IpHdr,
                                                             u1NextHdr);
    if (u1RetVal == SEC_FAILURE)
    {
        SECv4_TRC (SECv4_DATA_PATH,
                   "Secv4DummyPktGen: Encode function failed \n");
        return SEC_FAILURE;
    }
    return (u1RetVal);
}
