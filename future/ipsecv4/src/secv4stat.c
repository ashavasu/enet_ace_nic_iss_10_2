/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: secv4stat.c,v 1.4 2011/10/24 11:16:45 siva Exp $
 *
 * Description:This file contains the Statistics handler for the
 *             IPSEC  Module.
 *
 *******************************************************************/
#include "secv4com.h"

/************************************************************************/
/*  Function Name   : Secv4UpdateIfStats                                */
/*  Description     : This function is used to update the statistics    */
/*                  : Counter for the Sec Packets                       */
/*                  :                                                   */
/*  Input(s)        : u4Index:Refers to the Interface on which the      */
/*                  : arrives                                           */
/*                  : u1Direction:The direction of the Packet arrival   */
/*                  : u1Flag:Refers to the Policy                       */
/*                  :                                                   */
/*                  :                                                   */
/*  Output(s)       :                                                   */
/*  Returns         : Returns the no of SA's                            */
/************************************************************************/
VOID
Secv4UpdateIfStats (UINT4 u4Index, UINT1 u1Direction, UINT1 u1Flag)
{

    /* u4Index is assinged to 0 for making the statistics as global.
     * The statistics can be made interface specific using the u4Index
     * as policy interface index */
    u4Index = 0;
    gatIpsecv4Stat[u4Index].u4Index = u4Index;
    /* Increment the IN/OUT counters only for secured/bypassed packets. */
    if (u1Flag != SEC_BYPASS)
    {
        if (u1Direction == SEC_INBOUND)
        {
            /* increment the Inpkts counter */
            gatIpsecv4Stat[u4Index].u4IfInPkts += 1;
        }
        else
        {
            /* increment the outpkts counter */
            gatIpsecv4Stat[u4Index].u4IfOutPkts += 1;
        }
    }
    switch (u1Flag)
    {
        case SEC_APPLY:
            /* increment the ifapply counter */
            gatIpsecv4Stat[u4Index].u4IfPktsApply += 1;
            break;
        case SEC_FILTER:
            /* increment the filter counter */
            gatIpsecv4Stat[u4Index].u4IfPktsDiscard += 1;
            break;
        case SEC_BYPASS:
            /* increment the bypass-counter */
            gatIpsecv4Stat[u4Index].u4IfPktsBypass += 1;
            break;
        default:
            break;
    }
}

/************************************************************************/
/*  Function Name   : Secv4UpdateAhEspStats                             */
/*  Description     : This function is used to update the statistics    */
/*                  : Counter for the Sec Packets                       */
/*                  :                                                   */
/*  Input(s)        : u4Index:Refers to the Interface on which the      */
/*                  : arrives                                           */
/*                  : u1Direction:The direction of the Packet arrival   */
/*                  : u1Flag:Refers to the Policy                       */
/*                  : u1Protocol:Refers to the protocol                 */
/*                  :                                                   */
/*                  :                                                   */
/*  Output(s)       :                                                   */
/*  Returns         : Returns the no of SA's                            */
/************************************************************************/
VOID
Secv4UpdateAhEspStats (UINT4 u4Index, UINT1 u1Protocol, UINT1 u1Direction,
                       UINT1 u1Flag)
{
    /* u4Index is assinged to 0 for making the statistics as global.
     * The statistics can be made interface specific using the u4Index
     * as policy interface index */
    u4Index = 0;

    gatIpsecv4AhEspStat[u4Index].u4Index = u4Index;

    if (u1Protocol == SEC_AH)
    {
        if (u1Direction == SEC_INBOUND)
        {
            /* increment the incoming ahpkts count */
            gatIpsecv4AhEspStat[u4Index].u4InAhPkts += 1;
        }
        else
        {
            /* increment the outgoining ahpkts count */
            gatIpsecv4AhEspStat[u4Index].u4OutAhPkts += 1;
        }
        switch (u1Flag)
        {
            case SEC_ALLOW:
                /* increment the ahpkts allow  count */
                gatIpsecv4AhEspStat[u4Index].u4AhPktsAllow += 1;
                break;
            case SEC_FILTER:
                /* increment the ahpktsdisacrd count */
                gatIpsecv4AhEspStat[u4Index].u4AhPktsDiscard += 1;
                break;
            default:
                break;
        }
    }
    if (u1Protocol == SEC_ESP)
    {
        if (u1Direction == SEC_INBOUND)
        {
            /* increment the incoming esp-pkts count */
            gatIpsecv4AhEspStat[u4Index].u4InEspPkts += 1;
        }
        else
        {
            /* increment the outgoining esp-pkts count */
            gatIpsecv4AhEspStat[u4Index].u4OutEspPkts += 1;
        }
        switch (u1Flag)
        {
            case SEC_ALLOW:
                /* increment the esp-pkts allow count */
                gatIpsecv4AhEspStat[u4Index].u4EspPktsAllow += 1;
                break;
            case SEC_FILTER:
                /* increment the esp-pkts discard count */
                gatIpsecv4AhEspStat[u4Index].u4EspPktsDiscard += 1;
                break;
            default:
                break;
        }
    }
}

/************************************************************************/
/*  Function Name   : Secv4UpdateAhEspIntruStats                        */
/*  Description     : This function is used to update the statistics    */
/*                  : Counter for the Sec Packets                       */
/*                  :                                                   */
/*  Input(s)        : u4Index:Refers to the Interface on which the      */
/*                  : arrives                                           */
/*                  : u4SrcAddr:Refers to the source address            */
/*                  : DestAddr:Refers to the destination address        */
/*                  : u1Prot:Refers to the protocol :AH/ESP             */
/*                  :u1Time:Refers to the time at which intrusion took  */
/*                  :Place                                              */
/*                  :                                                   */
/*                  :                                                   */
/*  Output(s)       :                                                   */
/*  Returns         :                                                   */
/************************************************************************/
VOID
Secv4UpdateAhEspIntruStats (UINT4 u4Index, UINT4 u4SrcAddr, UINT4 u4DestAddr,
                            UINT1 u1Prot, UINT4 u4Time)
{

    /* check whether the count  has exceeded the max-val */
    if (u4Secv4AhEspIntruStatCount >= SEC_MAX_STAT_COUNT)
    {
        u4Secv4AhEspIntruStatCount = 0;
    }

    /* u4Index is assinged to 0 for making the statistics as global.
     * The statistics can be made interface specific using the u4Index
     * as policy interface index */
    u4Index = 0;
    gatIpsecv4AhEspIntruStat[u4Secv4AhEspIntruStatCount].u4Index =
        u4Secv4AhEspIntruStatCount + 1;

    /*  Copy the Interface index */
    gatIpsecv4AhEspIntruStat[u4Secv4AhEspIntruStatCount].u4IfIndex = u4Index;
    /* copy the intru-src addr */
    gatIpsecv4AhEspIntruStat[u4Secv4AhEspIntruStatCount].
        u4AhEspIntruSrcAddr = u4SrcAddr;
    /* copy the intru-dest addr */
    gatIpsecv4AhEspIntruStat[u4Secv4AhEspIntruStatCount].
        u4AhEspIntruDestAddr = u4DestAddr;
    /* update the intruder-protocol */
    gatIpsecv4AhEspIntruStat[u4Secv4AhEspIntruStatCount].u4AhEspIntruProt =
        u1Prot;
    /* update the intruder-time */
    gatIpsecv4AhEspIntruStat[u4Secv4AhEspIntruStatCount].u4AhEspIntruTime =
        u4Time;
    /* increment the ahesp intru count */
    u4Secv4AhEspIntruStatCount++;

}
