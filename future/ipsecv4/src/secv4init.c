/**********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: secv4init.c,v 1.10 2015/04/04 10:33:54 siva Exp $
 *
 * Description: This has functions for Sec Init and Sec SNMP Register. 
 *
 ***********************************************************************/
#define  _SECV4INIT_C_
#define  _SECINIT_C_

#include "secv4com.h"
#include "secv4soft.h"
#include "fssecv4wr.h"
#include "vpn.h"

/************************************************************************/
/*  Function Name   : Secv4Initialize                                   */
/*  Description     : This function Initializes Lists and Arrays used   */
/*                  : used for Configuration and Statistics purpose     */
/*                  :                                                   */
/*  Input(s)        : None                                              */
/*  Output(s)       : None                                              */
/*  Returns         : SEC_SUCCESS or SEC_FAILURE                        */
/************************************************************************/
INT1
Secv4Initialize (VOID)
{

    UINT4               u4Count = 0;

    if (OsixSemCrt ((UINT1 *) "sec4", &(gSecv4SemId)) == OSIX_FAILURE)
    {
        SECv4_TRC (SECv4_OS_RESOURCE,
                   "Secv4Initialize: Semaphore creation Failed\n");
        return SEC_FAILURE;
    }

    /* For lnxkernel compilation, Sem_init is set to 1,
     * Hence calling OsixSemGive isnt necessary*/
#ifndef SECURITY_KERNEL_WANTED
    OsixSemGive (gSecv4SemId);
#endif

    Secv4InitTimer ();

    if (Secv4MemPoolInit () != SEC_SUCCESS)
    {
        SECv4_TRC (SECv4_OS_RESOURCE,
                   "Secv4Initialize: Memory Initialization Failed\n");
        lrInitComplete (OSIX_FAILURE);
        return SEC_FAILURE;
    }

    TMO_SLL_Init (&Secv4SelList);
    TMO_SLL_Init (&Secv4AccessList);
    TMO_SLL_Init (&Secv4PolicyList);
    TMO_SLL_Init (&Secv4AssocList);
    TMO_SLL_Init (&Secv4SelFormattedList);
    TMO_DLL_Init ((tTMO_DLL *) & Secv4ReasmList);

    gSecv4Status = SEC_DISABLE;
    gSecv4Version = SEC_V4;
    u4Secv4AhEspIntruStatCount = 0;

    for (u4Count = 0; u4Count < SEC_MAX_STAT_COUNT; u4Count++)
    {
        gatIpsecv4Stat[u4Count].u4Index = 0;
        gatIpsecv4AhEspStat[u4Count].u4Index = 0;
        gatIpsecv4AhEspIntruStat[u4Count].u4Index = 0;
        gatSecv4DFrag[u4Count].u4IfIndex = u4Count;
        gatSecv4DFrag[u4Count].u1DFragBitStatus = SEC_DFBIT_COPY;
    }

    lrInitComplete (OSIX_SUCCESS);
    return SEC_SUCCESS;
}

/************************************************************************/
/*  Function Name   : Secv4MemPoolInit                                  */
/*  Description     : This function allocates and Initializes Memory    */
/*                  : Pool required by this module                      */
/*  Input(s)        : None                                              */
/*  Output(s)       : None                                              */
/*  Returns         : SEC_SUCCESS or SEC_FAILURE                        */
/************************************************************************/
INT1
Secv4MemPoolInit (VOID)
{
/* allocate Memory Pool for selector table */

/* Twice the number of MAX_SA is bcos of two slector lists
   being maintained selector and formatted selector */
    if (MemCreateMemPool (sizeof (tSecv4Selector), 2 * MAX_NUMBER_OF_SA,
                          MEM_DEFAULT_MEMORY_TYPE,
                          &SECv4_SEL_MEMPOOL) != MEM_SUCCESS)
    {
        SECv4_TRC (SECv4_OS_RESOURCE,
                   "Secv4MemPoolInit: Unable to allocate memory for Selector Table\n");
        Secv4MemPoolDeInit ();
        return (SEC_FAILURE);
    }

/* allocate Memory Pool for Access Lists.*/

    if (MemCreateMemPool (sizeof (tSecv4Access), MAX_NUMBER_OF_SA,
                          MEM_DEFAULT_MEMORY_TYPE,
                          &SECv4_ACC_MEMPOOL) != MEM_SUCCESS)
    {
        SECv4_TRC (SECv4_OS_RESOURCE,
                   "Secv4MemPoolInit: Unable to allocate memory for access list\n");
        Secv4MemPoolDeInit ();
        return (SEC_FAILURE);
    }

/* allocate Memory Pool for Policy Table */
    if (MemCreateMemPool (sizeof (tSecv4Policy), MAX_NUMBER_OF_SA,
                          MEM_DEFAULT_MEMORY_TYPE,
                          &SECv4_POL_MEMPOOL) != MEM_SUCCESS)
    {
        SECv4_TRC (SECv4_OS_RESOURCE,
                   "Secv4MemPoolInit: Unable to allocate memory for Policy table\n");
        Secv4MemPoolDeInit ();
        return (SEC_FAILURE);
    }

/* allocate Memory Pool for SAD */
    if (MemCreateMemPool (sizeof (tSecv4Assoc), MAX_NUMBER_OF_SA,
                          MEM_DEFAULT_MEMORY_TYPE,
                          &SECv4_SAD_MEMPOOL) != MEM_SUCCESS)
    {
        SECv4_TRC (SECv4_OS_RESOURCE,
                   "Secv4MemPoolInit: Unable to allocate memory for SAD\n");
        Secv4MemPoolDeInit ();
        return (SEC_FAILURE);
    }

    /* allocate Memory Pool for Ah Key  and key1 (key and key1) 
       max size of key length + 1 byte (EOS) + 2 bytes for padding. */
    if (MemCreateMemPool (SEC_AH_KEY_LENGTH, 2 * MAX_NUMBER_OF_SA,
                          MEM_DEFAULT_MEMORY_TYPE,
                          &SECv4_SAD_AH_MEMPOOL) != MEM_SUCCESS)
    {
        SECv4_TRC (SECv4_OS_RESOURCE,
                   "Secv4MemPoolInit: Unable to allocate memory for SAD AH key\n");
        Secv4MemPoolDeInit ();
        return (SEC_FAILURE);
    }

/* allocate Memory Pool for Esp Key( 3DES-CBC:-3Keys)  and init vector. 
   Amount of memory allocated is 4 times the (3keys and int vector) max 
   size of key length + 1 byte (EOS) + padding bytes.*/
    if (MemCreateMemPool (SEC_ESP_KEY_LENGTH, 4 * MAX_NUMBER_OF_SA,
                          MEM_DEFAULT_MEMORY_TYPE,
                          &SECv4_SAD_ESP_MEMPOOL) != MEM_SUCCESS)
    {
        SECv4_TRC (SECv4_OS_RESOURCE,
                   "Secv4MemPoolInit: Unable to allocate memory for SAD ESP key\n");
        Secv4MemPoolDeInit ();
        return (SEC_FAILURE);
    }
    /* Allocate Memory for Storing the Recievd IP Fragments */
    if (MemCreateMemPool (sizeof (tSecv4Frag),
                          (SECv4_MAX_FRAGMENTS_IN_DATAGRAM *
                           SECv4_MAX_DATAGRAMS_FOR_REASSEMBLY),
                          MEM_DEFAULT_MEMORY_TYPE,
                          &SECv4_IP_FRAG_MEMPOOL) != MEM_SUCCESS)
    {
        SECv4_TRC (SECv4_OS_RESOURCE,
                   "Secv4MemPoolInit: Unable to allocate memory for Fragments\n");
        Secv4MemPoolDeInit ();
        return (SEC_FAILURE);
    }

    /* Allocate Memory for  tSecv4Reassm */
    if (MemCreateMemPool
        (sizeof (tSecv4Reassm), SECv4_MAX_DATAGRAMS_FOR_REASSEMBLY,
         MEM_DEFAULT_MEMORY_TYPE, &SECv4_IP_REASSM_MEMPOOL) != MEM_SUCCESS)
    {
        SECv4_TRC (SECv4_OS_RESOURCE,
                   "Secv4MemPoolInit: Unable to allocate memory for tSecv4Reassm\n");
        Secv4MemPoolDeInit ();
        return (SEC_FAILURE);
    }

    /* Allocate Memory for notification packets */
    if (MemCreateMemPool
        (sizeof (tSecv4Notify), SECv4_MAX_NTFY_PKTS,
         MEM_DEFAULT_MEMORY_TYPE, &Secv4NtfyQMemPoolId) != MEM_SUCCESS)
    {
        SECv4_TRC (SECv4_OS_RESOURCE,
                   "Secv4MemPoolInit: Unable to allocate memory for "
                   "notification packets\n");
        Secv4MemPoolDeInit ();
        return (SEC_FAILURE);
    }

    /* Allocate Memory for Storing the Recievd IP Options */
    if (MemCreateMemPool (SEC_IPV4_MAX_OPT_LEN, SEC_MAX_NO_OF_BUFFER,
                          MEM_DEFAULT_MEMORY_TYPE,
                          &SECv4_RCVD_IPOPTS_MEMPOOL) != MEM_SUCCESS)
    {
        SECv4_TRC (SECv4_OS_RESOURCE,
                   "Secv4MemPoolInit: Unable to allocate "
                   "memory for Buffer descriptors\n");
        Secv4MemPoolDeInit ();
        return (SEC_FAILURE);
    }

    /* Allocate Memory for taskletdata */
    if (MemCreateMemPool (sizeof (tSecv4TaskletData), SEC_MAX_NO_OF_BUFFER,
                          MEM_DEFAULT_MEMORY_TYPE,
                          &SECv4_TASKLET_MEMPOOl) != MEM_SUCCESS)
    {
        SECv4_TRC (SECv4_OS_RESOURCE,
                   "Secv4MemPoolInit: Unable to allocate "
                   " memory for Buffer descriptors\n");
        Secv4MemPoolDeInit ();
        return (SEC_FAILURE);
    }

    if (Secv4MemInitCallBackCxt () == SEC_FAILURE)
    {
        Secv4MemPoolDeInit ();
        return (SEC_FAILURE);
    }
    return (SEC_SUCCESS);
}

/************************************************************************/
/*  Function Name   : Secv4MemPoolDeInit                                */
/*  Description     : This function De-Initializes Memory Pool allocated*/
/*                  : for this module.                                  */
/*                  :                                                   */
/*  Input(s)        : None                                              */
/*  Output(s)       : None                                              */
/*  Returns         : None                                              */
/************************************************************************/
VOID
Secv4DeInit (VOID)
{
    /* Delete All the IPSec Databases */
    Secv4DeleteIPSecDataBase ();
    /* Delete the Timer */
    Secv4DeInitTimer ();
    Secv4MemPoolDeInit ();
    return;
}

/************************************************************************/
/*  Function Name   : Secv4MemPoolDeInit                                */
/*  Description     : This function De-Initializes Memory Pool allocated*/
/*                  : for this module.                                  */
/*                  :                                                   */
/*  Input(s)        : None                                              */
/*  Output(s)       : None                                              */
/*  Returns         : None                                              */
/************************************************************************/
VOID
Secv4MemPoolDeInit (VOID)
{

    /* Release Selector Memory */
    MemDeleteMemPool (SECv4_SEL_MEMPOOL);

    /* Release Access List Memory */
    MemDeleteMemPool (SECv4_ACC_MEMPOOL);

    /* Release Policy Memory */
    MemDeleteMemPool (SECv4_POL_MEMPOOL);

    /* Release Security association Memory */
    MemDeleteMemPool (SECv4_SAD_MEMPOOL);

    /* Release Authentication key Memory */
    MemDeleteMemPool (SECv4_SAD_AH_MEMPOOL);

    /* Release ESP key Memory */
    MemDeleteMemPool (SECv4_SAD_ESP_MEMPOOL);

    /* Cleanup Reassembly Timers and Pool */
    Secv4ReassemblyCleanup ();

    /* Release the IP Fragmentation  Memory */
    MemDeleteMemPool (SECv4_IP_FRAG_MEMPOOL);

    /* Release the Reassembly  Memory */
    MemDeleteMemPool (SECv4_IP_REASSM_MEMPOOL);

    /* Release the Rcvd IPOPTS  Memory */
    MemDeleteMemPool (SECv4_RCVD_IPOPTS_MEMPOOL);

    /* Release the tasklet  Memory */
    MemDeleteMemPool (SECv4_TASKLET_MEMPOOl);

    /* Release the memory from the call back pool */
    Secv4MemDeInitCallBackCxt ();
}
