/**********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: secv4io.c,v 1.11 2013/10/24 09:56:37 siva Exp $
 *
 * Description: This has functions for SecIO SubModule
 *
 ***********************************************************************/

#include "secv4com.h"
#include "secv4io.h"
#include "cli.h"
#ifdef FLOWMGR_WANTED
#include "flowmgr.h"
#endif

/***********************************************************************/
/*  Function Name : Secv4InProcess                                     */
/*  Description   : This function gets the incomming packet            */
/*                : from Driver and converts it into linear buffer and */
/*                : invokes the respective function of IPSEC           */
/*                :                                                    */
/*  Input(s)      : pCBuf - Buffer contains IP Secured packet.          */
/*                : u4Port - Interface on which the Packet arrives.    */
/*                :                                                    */
/*  Output(s)     : pCBuf - Buffer contains Decoded IP packet.          */
/*                :                                                    */
/*  Return Values : None                                               */
/***********************************************************************/

UINT1
Secv4InProcess (tIP_BUF_CHAIN_HEADER ** ppCBuf, UINT4 u4Port)
{

    UINT1               u1RetVal = 0;

    Secv4Lock ();                /* Unlock in case of the failure/bypass.                                          Success case Secv4AlgoProcessPkt will Unlock  */
    if (gSecv4Status == SEC_DISABLE)
    {
        /* In case of User space, this case will never arise 
         * as these checks are already performed and Secv4InProcess
         * will be called only in case of IPSEC enabled and pkt received 
         * on WAN interface */
        Secv4UnLock ();
        return (SEC_BYPASS);
    }

    u1RetVal = Secv4HandleIncomingPkt (ppCBuf, u4Port);
    if (u1RetVal == SEC_FAILURE)
    {
        SECv4_TRC (SECv4_DATA_PATH,
                   "Secv4InProcess: Secv4 Handle Incoming Packet Failed\n");
        /* For the Packets posted to IP, which were not destined to IPSec
         * the buffer release will happen in correpsponding posting functions
         */
        Secv4UpdateIfStats (u4Port, SEC_INBOUND, SEC_FILTER);
        Secv4UnLock ();
        return (SEC_FAILURE);
    }

    if (u1RetVal != SEC_SUCCESS)
    {
        Secv4UnLock ();
    }

    return (u1RetVal);
}

/***********************************************************************/
/*  Function Name : Secv4HandleIncomingPkt                             */
/*  Description   : This function gets the incoming packet and         */
/*                : calls SecInMain based with the appropraite SA.     */
/*                :                                                    */
/*  Input(s)      : pBufDesc  - Buffer contains Secured packet.        */
/*                : u4IfIndex - Interface on which the Packet arrives. */
/*  Output(s)     : pCBuf      - Buffer contains Secured IP packet.     */
/*  Return Values : None                                               */
/***********************************************************************/

UINT1
Secv4HandleIncomingPkt (tIP_BUF_CHAIN_HEADER ** ppCBuf, UINT4 u4IfIndex)
{
    tIP_BUF_CHAIN_HEADER *pCBuf = *ppCBuf;
    tSecv4Assoc        *pSaEntry = NULL;
    UINT4               u4ProcessedLen = 0;
    UINT4               u4SpiOffset = 0;
    UINT4               u4OptLen = 0;
    UINT4               u4PktSize = 0;
    UINT4               u4Spi = 0;
    UINT1              *pu1Buf = NULL;
    UINT1               u1RetVal = SEC_FAILURE;
    UINT4               u4RetValue = VPN_OTHER_PACKET;

    t_IP_HEADER         IpHdr;
    t_IP_HEADER        *pIpHdr = NULL;
    INT1                i1ByteCheck = 0;

    pIpHdr =
        (t_IP_HEADER *) (void *) CRU_BUF_Get_DataPtr_IfLinear (pCBuf, 0,
                                                               SEC_IPV4_HEADER_SIZE);
    if (pIpHdr == NULL)
    {
        IPSEC_MEMSET (&IpHdr, 0, sizeof (t_IP_HEADER));
        IPSEC_COPY_FROM_BUF (pCBuf, (UINT1 *) &IpHdr, 0, SEC_IPV4_HEADER_SIZE);
        pIpHdr = &IpHdr;
    }

    if (SecUtilIpIfIsOurAddress (IPSEC_NTOHL (pIpHdr->u4Dest)) != TRUE)
    {
        /* Packet not destined to us, it may be ipsec pass through
         * so no need to re-assemble or decrypt it. This case will 
         * never arise if Called from CfaVpnProcessIncomingPkt  */
        return (SEC_BYPASS);
    }
    if ((pIpHdr->u1Proto == SEC_AH) || (pIpHdr->u1Proto == SEC_ESP) ||
        (pIpHdr->u1Proto == SEC_UDP))
    {
        if ((pCBuf =
             Secv4Reassemble (pIpHdr, pCBuf, (UINT2) u4IfIndex)) == NULL)
        {
            /* Pkt Yet to be Re-assembled. Wait for the 
             * next fragment  */
            Secv4UnLock ();        /* Release lock took at the Secv4InProcess */
            return (SEC_SUCCESS);
        }
        else
        {
            *ppCBuf = pCBuf;
            /* Re-assembly is successful, now get the IP header */
            pIpHdr =
                (t_IP_HEADER *) (void *) CRU_BUF_Get_DataPtr_IfLinear (pCBuf, 0,
                                                                       SEC_IPV4_HEADER_SIZE);
            if (pIpHdr == NULL)
            {
                IPSEC_MEMSET (&IpHdr, 0, sizeof (t_IP_HEADER));
                IPSEC_COPY_FROM_BUF (pCBuf, (UINT1 *) &IpHdr, 0,
                                     SEC_IPV4_HEADER_SIZE);
                pIpHdr = &IpHdr;
            }
        }
    }

    /* In Case of Kernel, Secv4InProcess is directly called for all packets
     * so we need to see if this packet is IPSEC (AH/ESP), else if IKE post 
     * it to Linux IP, else it is a plain traffic which needs to be 
     * bypassed */
    if ((pIpHdr->u1Proto != SEC_AH) && (pIpHdr->u1Proto != SEC_ESP))
    {
        /* If its an IKE Pkt and destined to us, Post the Pkt to IKE (Give it 
         * to IP layer ), else it is a plain packet no need of decrypting it */
        u4RetValue = VpnIsIpsecTraffic (pCBuf, VPN_INBOUND);
        if (u4RetValue == VPN_DROP_PACKET)
        {
            /* This packet is marked to be dropped 
             * by IPSec Policy */
            return SEC_FAILURE;
        }
        if (u4RetValue == VPN_IKE_PACKET)
        {
            /* This is an IKE packet received on NATT Port (4500)
             * Since the packet is modified to remove the UDP header, 
             * re-assign the skb with the buffer details. This packet 
             * is to be directly given to IP layer without flowing 
             * through Firewall/NAT etc */
            return (SEC_IKE);
        }
        if (u4RetValue == VPN_OTHER_PACKET)
        {
            /* If its an IKE Pkt and destined to us, Post the Pkt to IKE (Give it
             * to IP layer), else it is a plain packet no need of decrypting it */
            pIpHdr->u4Src = IPSEC_NTOHL (pIpHdr->u4Src);
            pIpHdr->u4Dest = IPSEC_NTOHL (pIpHdr->u4Dest);

            if (VpnIsIkeTraffic (pIpHdr, pCBuf) == OSIX_SUCCESS)
            {
                pIpHdr->u4Src = IPSEC_HTONL (pIpHdr->u4Src);
                pIpHdr->u4Dest = IPSEC_HTONL (pIpHdr->u4Dest);
                /* Give the Packet to Linux. */
                return (SEC_IKE);
            }
            /* Revert back the src and dest IP in the pkt in network order. 
             * This is a plane packet pass it on to firewall/NAT  */
            pIpHdr->u4Src = IPSEC_HTONL (pIpHdr->u4Src);
            pIpHdr->u4Dest = IPSEC_HTONL (pIpHdr->u4Dest);
            return (SEC_BYPASS);
        }
    }

    u4PktSize = CRU_BUF_Get_ChainValidByteCount (pCBuf);
    if (u4PktSize == 0)
    {
        SECv4_TRC (SECv4_DATA_PATH,
                   "Secv4HandleIncomingPkt: recvd Packet with  Zero Size\n");
        return (SEC_FAILURE);
    }
    pu1Buf = CRU_BUF_Get_DataPtr_IfLinear (pCBuf, 0, u4PktSize);

    /* Check if options are present and update the spi offset */
    u4OptLen = (UINT4) IPSEC_OLEN (pIpHdr->u1Ver_hdrlen);
    u4ProcessedLen = u4OptLen + SEC_IPV4_HEADER_SIZE;

    /* Calculate Spi offset Based on the Protocol */
    switch (pIpHdr->u1Proto)
    {
        case SEC_AH:
            u4SpiOffset = u4OptLen + SEC_AH_SPI_OFFSET;
            break;

        case SEC_ESP:
            u4SpiOffset = u4OptLen + SEC_IPV4_HEADER_SIZE;
            break;

        default:
            SECv4_TRC (SECv4_DATA_PATH,
                       "Secv4HandleIncomingPkt: Invalid protocol\n");
            return (SEC_FAILURE);
    }

    /* Read SPI value */

    if (pu1Buf != NULL)
    {
        u4Spi = *((UINT4 *) (void *) (pu1Buf + u4SpiOffset));
    }
    else
    {
        IPSEC_COPY_FROM_BUF (pCBuf, (UINT1 *) &u4Spi, u4SpiOffset,
                             sizeof (UINT4));
    }
    /* Find SA Entry Pointer */
    pSaEntry = Secv4GetAssocEntry (IPSEC_NTOHL (u4Spi),
                                   IPSEC_NTOHL (pIpHdr->u4Dest),
                                   pIpHdr->u1Proto);

    if (pSaEntry == NULL)
    {
#ifdef IKE_WANTED
        /* If Ike is Enabled intimate IKE of the Absence of SA for 
           Decoding the packet */
        Secv4IntimateIkeInvalidSpi (IPSEC_NTOHL (u4Spi),
                                    IPSEC_NTOHL (pIpHdr->u4Dest),
                                    IPSEC_NTOHL (pIpHdr->u4Src),
                                    pIpHdr->u1Proto);
#endif
        SECv4_TRC (SECv4_DATA_PATH,
                   "Secv4HandleIncomingPkt: No SA Available For "
                   "Decoding the Pkt\n");
        return (SEC_FAILURE);
    }

#ifdef IKE_WANTED
    i1ByteCheck = Secv4CheckNoOfInBoundBytesSecured (pSaEntry, pCBuf);
    if (i1ByteCheck == SEC_BYTES_SECURED_EXCEEDED)
    {
        Secv4DeleteSa (pSaEntry->u4SecAssocIndex);
        SECv4_TRC (SECv4_DATA_PATH,
                   "Bytes Decoded exceeded the Configured LifeTimeInBytes\n");
        return (SEC_FAILURE);
    }
    else if (i1ByteCheck == SEC_THRESHOLD_REACHED)
    {
        if (!pSaEntry->u1DelSaFlag)
        {
            Secv4IntimateIkeInvalidSpi (IPSEC_NTOHL (u4Spi),
                                        IPSEC_NTOHL (pIpHdr->u4Dest),
                                        IPSEC_NTOHL (pIpHdr->u4Src),
                                        pIpHdr->u1Proto);
            pSaEntry->u1DelSaFlag = 1;
        }
        return (SEC_FAILURE);
    }
#endif

    u1RetVal = Secv4InMain (pCBuf, pSaEntry, u4ProcessedLen, u4IfIndex, pIpHdr);

    if (u1RetVal == SEC_FAILURE)
    {
        SECv4_TRC (SECv4_DATA_PATH,
                   "Secv4HandleIncomingPkt: Secv4InMain Fails\n");
        Secv4UpdateAhEspStats (u4IfIndex, pSaEntry->u1SecAssocProtocol,
                               SEC_INBOUND, SEC_FILTER);
        return (SEC_FAILURE);
    }
    return (u1RetVal);
}

/************************************************************************/
/*  Function Name : Secv4OutProcess                                     */
/*  Description   : This function gets the outgoing packet              */
/*                : from Driver and converts it into linear buffer and  */
/*                : passes to corresponding function in IPSEC           */
/*                :                                                     */
/*  Input(s)      : pBuf - Buffer contains IP packet.                   */
/*                : u4Port - The interface on which the packet arrived. */
/*                :                                                     */
/*  Output(s)     : pBuf - Buffer contains IP Secured packet.           */
/*                :                                                     */
/*  Return Values : SEC_SUCCESS or SEC_FAILURE                          */
/************************************************************************/

UINT1
Secv4OutProcess (tIP_BUF_CHAIN_HEADER * pCBuf, UINT4 u4Port)
{
    UINT1               u1Retval = 0;

    Secv4Lock ();                /* Unlock in case of the failure/bypass.
                                   Success case Secv4AlgoProcessPkt will Unlock  */
    if (gSecv4Status == SEC_DISABLE)
    {
        Secv4UnLock ();
        return (SEC_SUCCESS);
    }

    u1Retval = Secv4HandleOutgoingPkt (pCBuf, u4Port);
    if (u1Retval == SEC_FAILURE)
    {
        Secv4UpdateIfStats (u4Port, SEC_OUTBOUND, SEC_FILTER);
        SECv4_TRC (SECv4_DATA_PATH,
                   "Secv4OutProcess: Secv4 Handle Outgoing Pkt Failed");
        Secv4UnLock ();
        return (SEC_FAILURE);
    }
    else if (u1Retval == SEC_BYPASS)
    {
        Secv4UpdateIfStats (u4Port, SEC_OUTBOUND, SEC_BYPASS);
        Secv4UnLock ();
        return (SEC_BYPASS);
    }

    return (SEC_SUCCESS);
}

/***********************************************************************/
/*  Function Name : Secv4HandleOutgoingPkt                             */
/*  Description   : This function handles the outgoing packet.         */
/*                :                                                    */
/*  Input(s)      : pBuf - Buffer contains IP Secured packet.          */
/*                : u4IfIndex - Interface on which the Packet arrives. */
/*                :                                                    */
/*  Output(s)     : pBuf - Buffer contains IP packet.                  */
/*                :                                                    */
/*  Return Values : None                                               */
/***********************************************************************/
UINT1
Secv4HandleOutgoingPkt (tIP_BUF_CHAIN_HEADER * pCBuf, UINT4 u4IfIndex)
{
    t_IP_HEADER         IpHdr;
    tSecv4Policy       *pPolicy = NULL;
    t_IP_HEADER        *pTmpIpHdr = NULL;
    UINT1              *pu1Buf = NULL;
    UINT4               u4OptLen = 0;
    INT4                i4Count = 0;
    UINT4               u4PktSize = 0;
    UINT4               u4AccessIndex = 0;
    UINT2               u2DstPortNumber = SEC_ANY_PORT;
    UINT2               u2SrcPortNumber = SEC_ANY_PORT;
    UINT1               u1RetVal = SEC_FAILURE;
    INT1                i1ByteCheck = 0;

    u4PktSize = CRU_BUF_Get_ChainValidByteCount (pCBuf);

    pu1Buf = CRU_BUF_Get_DataPtr_IfLinear (pCBuf, 0, u4PktSize);

    if (pu1Buf == NULL)
    {
        IPSEC_COPY_FROM_BUF (pCBuf, (UINT1 *) &IpHdr, 0, SEC_IPV4_HEADER_SIZE);
        pTmpIpHdr = &IpHdr;
        u4OptLen = (UINT4) IPSEC_OLEN (pTmpIpHdr->u1Ver_hdrlen);
        if (pTmpIpHdr->u1Proto == SEC_TCP || pTmpIpHdr->u1Proto == SEC_UDP)
        {
            CRU_BUF_Copy_FromBufChain (pCBuf, (UINT1 *) &u2DstPortNumber,
                                       (SEC_PORT_NUMBER_OFFSET + u4OptLen),
                                       sizeof (UINT2));
            CRU_BUF_Copy_FromBufChain (pCBuf, (UINT1 *) &u2SrcPortNumber,
                                       (SEC_SRC_PORT_NUMBER_OFFSET + u4OptLen),
                                       sizeof (UINT2));
        }
    }
    else
    {
        pTmpIpHdr = (t_IP_HEADER *) (void *) pu1Buf;
        u4OptLen = (UINT4) IPSEC_OLEN (pTmpIpHdr->u1Ver_hdrlen);
        if (pTmpIpHdr->u1Proto == SEC_TCP || pTmpIpHdr->u1Proto == SEC_UDP)
        {
            u2DstPortNumber =
                *((UINT2 *) (void *) (pu1Buf +
                                      (SEC_PORT_NUMBER_OFFSET + u4OptLen)));
            u2DstPortNumber = OSIX_NTOHS (u2DstPortNumber);
            u2SrcPortNumber =
                *((UINT2 *) (void *) (pu1Buf +
                                      (SEC_SRC_PORT_NUMBER_OFFSET + u4OptLen)));
            u2SrcPortNumber = OSIX_NTOHS (u2SrcPortNumber);
        }
    }

    /* Get Association Entry(Bundle) from Data base */
    i4Count =
        Secv4AssocOutLookUp (IPSEC_NTOHL (pTmpIpHdr->u4Src),
                             IPSEC_NTOHL (pTmpIpHdr->u4Dest),
                             (UINT2) pTmpIpHdr->u1Proto, u4IfIndex,
                             &pPolicy, u2SrcPortNumber, u2DstPortNumber);

    if (i4Count == SEC_FAILURE)
    {
        return (SEC_BYPASS);
    }

    if (i4Count == SEC_REJECT)
    {
        SECv4_TRC (SECv4_DATA_PATH,
                   "Secv4HandleOutgoingPkt: Packet Filtered\n");
        return (SEC_FAILURE);
    }

    i1ByteCheck = Secv4CheckNoOfOutBoundBytesSecured (pPolicy, pCBuf);
    if (i1ByteCheck == SEC_BYTES_SECURED_EXCEEDED)
    {
        SECv4_TRC (SECv4_CONTROL_PLANE,
                   "Secv4HandleOutgoingPkt: No Of Bytes Secured Exceeded\n");
        Secv4IntimateIkeDeletionOfSa (pPolicy,
                                      pPolicy->pau1SaEntry[0]->u4SecAssocIndex);
        /* Here IKE is intimated to delete the SA but this
         * packet cannot be dropped because of that so sending
         * as failure */
        return SEC_FAILURE;
    }
    else if (i1ByteCheck == SEC_THRESHOLD_REACHED)
    {

        /* Check whether the packet Src and Dest Addr  is within the range
           Configured in the Access List */
        u4AccessIndex = Secv4GetAccessIndex (IPSEC_NTOHL (pTmpIpHdr->u4Src),
                                             IPSEC_NTOHL (pTmpIpHdr->u4Dest),
                                             u2SrcPortNumber, u2DstPortNumber,
                                             (UINT2) pTmpIpHdr->u1Proto);

        if (u4AccessIndex != SEC_FAILURE)
        {
            SECv4_TRC (SECv4_CONTROL_PLANE,
                       "Secv4HandleOutgoingPkt:  Request Ike For RE-KEY\n");
            if (!pPolicy->pau1SaEntry[0]->u1DelSaFlag)
            {
                Secv4RequeStIkeForNewOrReKey (u4IfIndex,
                                              (UINT4) SEC_ANY_PROTOCOL,
                                              (UINT4) u2DstPortNumber,
                                              u4AccessIndex, SEC_OUTBOUND,
                                              SEC_RE_KEY, pPolicy);

                pPolicy->pau1SaEntry[0]->u1DelSaFlag = 1;
                pPolicy->pau1SaEntry[0]->Secv4AssocHardTimer.u4Param1 =
                    pPolicy->u4PolicyIndex;
                pPolicy->pau1SaEntry[0]->Secv4AssocHardTimer.u4SaIndex =
                    pPolicy->pau1SaEntry[0]->u4SecAssocIndex;
                Secv4StartTimer (&
                                 (pPolicy->pau1SaEntry[0]->Secv4AssocHardTimer),
                                 SECv4_SA_HARD_TIMER_ID,
                                 SEC_MIN_LIFE_TIME_VALUE);
            }
        }
    }

#ifdef FLOWMGR_WANTED
    FlUtilUpdateFlowTypeFlowData (pCBuf, FLOW_CTRL_FLOW);
#endif

    /* Apply Security to the packet based on SA Entry */
    u1RetVal =
        Secv4OutMain (pCBuf, pPolicy->pau1SaEntry[0], u4IfIndex, pTmpIpHdr);
    if (u1RetVal == SEC_FAILURE)
    {
        SECv4_TRC (SECv4_DATA_PATH,
                   "Secv4HandleOutgoingPkt: Secv4OutMain Fails \n");
        Secv4UpdateAhEspStats (u4IfIndex,
                               pPolicy->pau1SaEntry[0]->u1SecAssocProtocol,
                               SEC_OUTBOUND, SEC_FILTER);
        return (SEC_FAILURE);
    }
    return (u1RetVal);

}
