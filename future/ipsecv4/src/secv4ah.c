/**********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: secv4ah.c,v 1.13 2014/03/19 13:30:02 siva Exp $
 *
 * Description: This has functions for AH SubModule
 *
 ***********************************************************************/
#ifndef _SECV4AH_C_
#define _SECV4AH_C_

#include "secv4com.h"
#include "secv4ah.h"
#include "msr.h"
/***********************************************************************/
/*  Function Name : Secv4AhTunnelEncode                                */
/*  Description   : This function gets the outgoing packet and         */
/*                : does tunnel mode encode based on SA Entry          */
/*                :                                                    */
/*  Input(s)      : pCBuf    - Buffer contains IP packet to be secured */
/*                : pSaEntry - Pointer to a SA Entry                   */
/*                : u4IfIndex- Interface on which the packet arrives   */
/*                :                                                    */
/*  Output(s)     : pCBuf    - buffer contains IP Secured packet       */
/*  Return        : SEC_SUCCESS or SEC_FAILURE                         */
/***********************************************************************/
UINT1
Secv4AhTunnelEncode (tIP_BUF_CHAIN_HEADER * pCBuf, tSecv4Assoc * pSaEntry,
                     UINT4 u4IfIndex, t_IP_HEADER * pIpHdr, UINT1 u1NextHdr)
{
    tSecv4TaskletData  *pSecv4TaskletData = NULL;
    t_IP_HEADER         NewIpHdr;
    tSecv4AuthHdr       AuthHdr;
    UINT4               u4PktSize = 0;

    if ((pSaEntry->u1SecAssocAhAlgo != SEC_HMACSHA1) &&
        (pSaEntry->u1SecAssocAhAlgo != SEC_HMACMD5) &&
        (pSaEntry->u1SecAssocAhAlgo != SEC_XCBCMAC) &&
        (pSaEntry->u1SecAssocAhAlgo != HMAC_SHA_256) &&
        (pSaEntry->u1SecAssocAhAlgo != HMAC_SHA_512) &&
        (pSaEntry->u1SecAssocAhAlgo != HMAC_SHA_384))
    {
        SECv4_TRC (SECv4_DATA_PATH,
                   "Secv4AhTunnelEncode:  Unknown Auth Algo\n");
        return SEC_FAILURE;
    }

    u4PktSize = IPSEC_BUF_GetValidBytes (pCBuf);
    if (u4PktSize == 0)
    {
        SECv4_TRC (SECv4_DATA_PATH,
                   "Secv4AhTunnelEncode: Invalid packet size\n");
        return SEC_FAILURE;
    }

    /* Construct New IPv4 Header with all mutable fields to zero */
    NewIpHdr.u1Ver_hdrlen = SEC_VERS_AND_HLEN (SEC_V4, 0);
    NewIpHdr.u1Tos = 0;
    NewIpHdr.u2Totlen = (UINT2) (IPSEC_HTONS ((u4PktSize +
                                               SEC_AUTH_HEADER_SIZE +
                                               pSaEntry->u1SecAhAlgoDigestSize +
                                               SEC_IPV4_HEADER_SIZE)));
    NewIpHdr.u2Id = pIpHdr->u2Id;
    NewIpHdr.u2Fl_offs = 0;
    NewIpHdr.u1Ttl = 0;
    NewIpHdr.u1Proto = SEC_AH;
    NewIpHdr.u2Cksum = 0;
    NewIpHdr.u4Src = IPSEC_HTONL (pSaEntry->u4SecAssocSrcAddr);
    NewIpHdr.u4Dest = IPSEC_HTONL (pSaEntry->u4SecAssocDestAddr);

    /* Construct AH Header */
    AuthHdr.u1NxtHdr = u1NextHdr;
    AuthHdr.u1Len =
        (UINT1) (((SEC_AUTH_HEADER_SIZE +
                   pSaEntry->u1SecAhAlgoDigestSize) / SEC_AH_FACTOR) - 2);
    AuthHdr.u4SPI = IPSEC_HTONL (pSaEntry->u4SecAssocSpi);
    AuthHdr.u2Rsvd = SEC_AUTH_RSVD;
    pSaEntry->u4SecAssocSeqNumber = pSaEntry->u4SecAssocSeqNumber + 1;
    AuthHdr.u4SeqNumber = IPSEC_HTONL (pSaEntry->u4SecAssocSeqNumber);

    /* Copy New Ipv4 Header, Auth Header and padding to linear buf */
    if (IPSEC_BUF_Prepend (pCBuf, gau1v4AuthPadding,
                           pSaEntry->u1SecAhAlgoDigestSize) == BUF_FAILURE)
    {
        SECv4_TRC (SECv4_DATA_PATH,
                   "Secv4AhTunnelEncode: Prepend of AuthPadding failed\n");
        return SEC_FAILURE;
    }
    if (IPSEC_BUF_Prepend (pCBuf, (UINT1 *) &AuthHdr,
                           SEC_AUTH_HEADER_SIZE) == BUF_FAILURE)
    {
        SECv4_TRC (SECv4_DATA_PATH,
                   "Secv4AhTunnelEncode: Prepend of AuthHdr failed\n");
        return SEC_FAILURE;
    }
    if (IPSEC_BUF_Prepend (pCBuf, (UINT1 *) &NewIpHdr,
                           SEC_IPV4_HEADER_SIZE) == BUF_FAILURE)
    {
        SECv4_TRC (SECv4_DATA_PATH,
                   "Secv4AhTunnelEncode: Prepend of new IPHdr failed\n");
        return SEC_FAILURE;
    }

    u4PktSize =
        u4PktSize + pSaEntry->u1SecAhAlgoDigestSize + SEC_AUTH_HEADER_SIZE +
        SEC_IPV4_HEADER_SIZE;

    /* Construct AlgoMsg with Information required for and after algo
     * Processing */
    if (MemAllocateMemBlock (SECv4_TASKLET_MEMPOOl,
                             (UINT1 **) (VOID *) &pSecv4TaskletData) ==
        MEM_FAILURE)
    {
        SECv4_TRC (SECv4_ALL_FAILURE,
                   "Secv4AhTunnelEncode :- Failed to Allocating memblock for "
                   " Secv4LocalTaskletData \n");
        return SEC_FAILURE;
    }
    if (pSecv4TaskletData == NULL)
    {
        SECv4_TRC (SECv4_ALL_FAILURE,
                   "Secv4AhTunnelEncode :- Failed to Allocating memblock for "
                   " Secv4LocalTaskletData \n");
        return SEC_FAILURE;
    }

    IPSEC_MEMSET (pSecv4TaskletData, 0, sizeof (tSecv4TaskletData));
    pSecv4TaskletData->pSaEntry = pSaEntry;
    pSecv4TaskletData->u4SecAssocIndex = pSaEntry->u4SecAssocIndex;

    pSecv4TaskletData->pCBuf = pCBuf;
    pSecv4TaskletData->u4AuthOffset = 0;
    pSecv4TaskletData->u4AuthBufSize = u4PktSize;
    pSecv4TaskletData->u4EncrOffset = 0;
    pSecv4TaskletData->u4EncrBufSize = 0;
    pSecv4TaskletData->u4IcvOffset =
        SEC_IPV4_HEADER_SIZE + SEC_AUTH_HEADER_SIZE;
    pSecv4TaskletData->u4Mode = AUTH;
    pSecv4TaskletData->u4Action = SEC_ENCODE;
    pSecv4TaskletData->u4IfIndex = u4IfIndex;
    pSecv4TaskletData->u4BufSize = u4PktSize;
    /* to store the orginal IP header and options. 
     * Used only in case of AH */
    pSecv4TaskletData->u1Tos = pIpHdr->u1Tos;

    if (Secv4AlgoProcessPkt (pSecv4TaskletData, AUTH, SEC_ENCODE) ==
        SEC_FAILURE)
    {
        SECv4_TRC (SECv4_CONTROL_PLANE, "\nSecv4AhTunnelEncode:-"
                   "Secv4AlgoProcessPkt failed \n");
        MemReleaseMemBlock (SECv4_TASKLET_MEMPOOl, (UINT1 *) pSecv4TaskletData);
        return SEC_FAILURE;
    }

    return SEC_SUCCESS;
}

/************************************************************************/
/*  Function Name : Secv4AhTunnelDecode                                 */
/*  Description   : This function gets the incomming packet and         */
/*                : does tunnel mode decode based on SA Entry           */
/*                :                                                     */
/*  Input(s)      : pCBuf - buffer contains IP Secured packet        */
/*                : pSaEntry - Pointer to a SA Entry                    */
/*                : pu4ProcessedLen : Inicates the length upto which    */
/*                : packet is processed                                 */
/*                : u4IfIndex - The Interafce Index on which the packet */
/*                : arrived                                             */
/*  Output(s)     : pCBuf - Buffer contains Decoded IP packet        */
/*  Return Values : SEC_SUCCESS or SEC_FAILURE                          */
/************************************************************************/
UINT1
Secv4AhTunnelDecode (tIP_BUF_CHAIN_HEADER * pCBuf, tSecv4Assoc * pSaEntry,
                     UINT4 u4ProcessedLen, UINT4 u4IfIndex,
                     t_IP_HEADER * pIpHdr)
{
    tSecv4TaskletData  *pSecv4TaskletData = NULL;
    UINT4               u4PktSize = 0;
    UINT4               u4SeqNumber;
    UINT1              *pu1Buf = NULL;
    INT1                ai1IpAddr[100];
    IPADDR_HEX2STR (pIpHdr->u4Src, ai1IpAddr);

    if ((pSaEntry->u1SecAssocAhAlgo != SEC_HMACMD5) &&
        (pSaEntry->u1SecAssocAhAlgo != SEC_HMACSHA1) &&
        (pSaEntry->u1SecAssocAhAlgo != SEC_XCBCMAC) &&
        (pSaEntry->u1SecAssocAhAlgo != HMAC_SHA_256) &&
        (pSaEntry->u1SecAssocAhAlgo != HMAC_SHA_512) &&
        (pSaEntry->u1SecAssocAhAlgo != HMAC_SHA_384))
    {
        SECv4_TRC (SECv4_DATA_PATH,
                   "Secv4AhTunnelDecode:\tUnknown Auth Algo\n");
        return SEC_FAILURE;
    }

    pu1Buf =
        CRU_BUF_Get_DataPtr_IfLinear (pCBuf, 0,
                                      (u4ProcessedLen + SEC_AUTH_HEADER_SIZE));

    /* Verify Sequential Number */
    if (pSaEntry->u1AntiReplayStatus == SEC_ANTI_REPLAY_ENABLE)
    {
        if (pu1Buf == NULL)
        {
            CRU_BUF_Copy_FromBufChain (pCBuf, (UINT1 *) &u4SeqNumber,
                                       (u4ProcessedLen +
                                        SEC_AUTH_SEQ_NUM_OFFSET),
                                       sizeof (UINT4));
            u4SeqNumber = IPSEC_NTOHL (u4SeqNumber);
        }
        else
        {
            u4SeqNumber = IPSEC_NTOHL (*((UINT4 *) (void *) (pu1Buf +
                                                             u4ProcessedLen +
                                                             SEC_AUTH_SEQ_NUM_OFFSET)));
        }
        if (Secv4SeqNumberVerification (u4SeqNumber, pSaEntry) == SEC_FAILURE)
        {
            SECv4_TRC1 (SECv4_DATA_PATH,
                        "Secv4AhTunnelDecode:\tReplay attack from %s\r\n",
                        ai1IpAddr);
            return SEC_FAILURE;
        }
    }

    /* Mute all mutable fields in the IP header */
    pIpHdr->u1Tos = 0;
    pIpHdr->u2Fl_offs = 0;
    pIpHdr->u1Ttl = 0;
    pIpHdr->u2Cksum = 0;
    if (pu1Buf == NULL)
    {
        if (IPSEC_COPY_OVER_BUF (pCBuf, (UINT1 *) pIpHdr, 0,
                                 SEC_IPV4_HEADER_SIZE) == BUF_FAILURE)
        {
            SECv4_TRC (SECv4_DATA_PATH,
                       "Secv4AhTunnelDecode: Copy over Buf failed\n");
            return SEC_FAILURE;
        }
    }

    u4PktSize = IPSEC_BUF_GetValidBytes (pCBuf);
    if (u4PktSize == 0)
    {
        SECv4_TRC (SECv4_DATA_PATH,
                   "Secv4AhTunnelDecode: Invalid packet size\n");
        return SEC_FAILURE;
    }

    if (MemAllocateMemBlock (SECv4_TASKLET_MEMPOOl,
                             (UINT1 **) (VOID *) &pSecv4TaskletData) ==
        MEM_FAILURE)
    {
        SECv4_TRC (SECv4_ALL_FAILURE,
                   "Secv4AhTunnelDecode:- Failed to Allocating memblock for "
                   " Secv4LocalTaskletData \n");
        return SEC_FAILURE;
    }
    if (pSecv4TaskletData == NULL)
    {
        SECv4_TRC (SECv4_ALL_FAILURE,
                   "Secv4AhTunnelDecode :- Failed to Allocating memblock for "
                   " Secv4LocalTaskletData \n");
        return SEC_FAILURE;
    }

    IPSEC_MEMSET (pSecv4TaskletData, 0, sizeof (tSecv4TaskletData));
    pSecv4TaskletData->pSaEntry = pSaEntry;
    pSecv4TaskletData->u4SecAssocIndex = pSaEntry->u4SecAssocIndex;

    pSecv4TaskletData->pCBuf = pCBuf;
    pSecv4TaskletData->u4AuthOffset = 0;
    pSecv4TaskletData->u4EncrOffset = 0;
    pSecv4TaskletData->u4AuthBufSize = u4PktSize;
    pSecv4TaskletData->u4EncrBufSize = 0;
    pSecv4TaskletData->u4IcvOffset = SEC_IPV4_HEADER_SIZE +
        SEC_AUTH_HEADER_SIZE;
    pSecv4TaskletData->u4Mode = AUTH;
    pSecv4TaskletData->u4Action = SEC_DECODE;
    pSecv4TaskletData->u4IfIndex = u4IfIndex;
    pSecv4TaskletData->u4BufSize = u4PktSize;
    if (Secv4AlgoProcessPkt (pSecv4TaskletData, AUTH,
                             SEC_DECODE) == SEC_FAILURE)
    {
        SECv4_TRC (SECv4_CONTROL_PLANE, "\nSecv4AhTunnelDecode:-"
                   "Secv4AlgoProcessPkt failed \n");
        MemReleaseMemBlock (SECv4_TASKLET_MEMPOOl, (UINT1 *) pSecv4TaskletData);
        return SEC_FAILURE;
    }
    return SEC_SUCCESS;

}

/**********************************************************************/
/*  Function Name : Secv4AhTransportEncode                            */
/*  Description   : This function gets the outgoing packet and        */
/*                : does transport mode encode based on sa entry      */
/*                :                                                   */
/*  Input(s)      : pCBuf - buffer contains IP packet to be secured   */
/*                : pSaEntry - Pointer to a SA Entry                  */
/*                : u4IfIndex - The interface index on which the pkt  */
/*                : arrived                                           */
/*  Output(s)     : pCBuf    - buffer contains secured Ip packet      */
/*  Return Values : SEC_SUCCESS or SEC_FAILURE                        */
/***********************************************************************/

UINT1
Secv4AhTransportEncode (tIP_BUF_CHAIN_HEADER * pCBuf, tSecv4Assoc * pSaEntry,
                        UINT4 u4IfIndex, t_IP_HEADER * pIpHdr, UINT1 u1NextHdr)
{
    tSecv4TaskletData  *pSecv4TaskletData = NULL;
    tSecv4AuthHdr       AuthHdr;
    t_IP_HEADER         IpHdr;
    UINT1              *pu1Buf = NULL;
    UINT4               u4OptLen = 0;
    UINT2               u2Len = 0;
    UINT4               u4PktSize = 0;

    if ((pSaEntry->u1SecAssocAhAlgo != SEC_HMACSHA1) &&
        (pSaEntry->u1SecAssocAhAlgo != SEC_HMACMD5) &&
        (pSaEntry->u1SecAssocAhAlgo != SEC_XCBCMAC) &&
        (pSaEntry->u1SecAssocAhAlgo != HMAC_SHA_256) &&
        (pSaEntry->u1SecAssocAhAlgo != HMAC_SHA_512) &&
        (pSaEntry->u1SecAssocAhAlgo != HMAC_SHA_384))

    {
        SECv4_TRC (SECv4_DATA_PATH,
                   "Secv4AhTransportEncode:- Currently Null Authentication"
                   "algorithm not supported\n");
        return (SEC_FAILURE);
    }

    if (SecUtilIpIfIsOurAddress (IPSEC_NTOHL (pIpHdr->u4Src)) != TRUE)
    {
        SECv4_TRC (SECv4_DATA_PATH,
                   "Secv4AhTransportEncode Attempting to Apply Transport"
                   "Mode SA for the System acting as a Gateway \n");
        return SEC_FAILURE;
    }

    u4OptLen = (UINT4) IPSEC_OLEN (pIpHdr->u1Ver_hdrlen);
    pSaEntry->u4SecAssocSeqNumber = pSaEntry->u4SecAssocSeqNumber + 1;

    /* Form AH header  */
    AuthHdr.u1NxtHdr = u1NextHdr;
    AuthHdr.u1Len =
        (UINT1) (((SEC_AUTH_HEADER_SIZE +
                   pSaEntry->u1SecAhAlgoDigestSize) / SEC_AH_FACTOR) - 2);
    AuthHdr.u2Rsvd = SEC_AUTH_RSVD;
    AuthHdr.u4SPI = IPSEC_HTONL (pSaEntry->u4SecAssocSpi);
    AuthHdr.u4SeqNumber = IPSEC_HTONL (pSaEntry->u4SecAssocSeqNumber);

    /* Add AuthHdr and Padding to pCBuf  */
    if (Secv4AddExtraHeaderToIP (pCBuf, (UINT1 *) &AuthHdr, pIpHdr,
                                 pSaEntry->u1SecAhAlgoDigestSize) ==
        SEC_FAILURE)
    {
        SECv4_TRC (SECv4_DATA_PATH,
                   "Secv4AhTransportEncode: Unable to add Auth header\n");
        return SEC_FAILURE;
    }

    pu1Buf = CRU_BUF_Get_DataPtr_IfLinear (pCBuf, 0, SEC_IPV4_HEADER_SIZE);

    if (pu1Buf == NULL)
    {
        IPSEC_COPY_FROM_BUF (pCBuf, (UINT1 *) &IpHdr, 0, SEC_IPV4_HEADER_SIZE);
    }
    else
    {
        pIpHdr = (t_IP_HEADER *) (void *) pu1Buf;
    }
    pIpHdr->u1Proto = SEC_AH;
    u2Len = (UINT2) IPSEC_BUF_GetValidBytes (pCBuf);
    if (u2Len == 0)
    {
        SECv4_TRC (SECv4_DATA_PATH,
                   "Secv4AhTransportEncode: Invalid packet size\n");
        return SEC_FAILURE;
    }
    /* Copy the computed length of the Packet in the u2TotLen  field of
     * the IP Header */
    pIpHdr->u2Totlen = IPSEC_HTONS (u2Len);

    if (MemAllocateMemBlock (SECv4_TASKLET_MEMPOOl,
                             (UINT1 **) (VOID *) &pSecv4TaskletData) ==
        MEM_FAILURE)
    {
        SECv4_TRC (SECv4_ALL_FAILURE,
                   "Secv4AhTransportEncode :- Failed to Allocating memblock for "
                   " Secv4LocalTaskletData \n");
        return SEC_FAILURE;
    }
    if (pSecv4TaskletData == NULL)
    {
        SECv4_TRC (SECv4_ALL_FAILURE,
                   "Secv4AhTransportEncode :- Failed to Allocating memblock for "
                   " Secv4LocalTaskletData \n");
        return SEC_FAILURE;
    }
    IPSEC_MEMSET (pSecv4TaskletData, 0, sizeof (tSecv4TaskletData));
    pSecv4TaskletData->u1Tos = pIpHdr->u1Tos;
    pSecv4TaskletData->u2Fl_offs = pIpHdr->u2Fl_offs;
    pSecv4TaskletData->u1Ttl = pIpHdr->u1Ttl;

    /* Mute all mutable fields in the IP header */
    Secv4MuteIpHdr (pIpHdr);

    /* Allocate Memory for storing Options if there are options
     *        in the Rcvd Pkt */
    if (u4OptLen != 0)
    {
        pSecv4TaskletData->pu1OptBuf = Secv4AllocateRcvdIPOpts ();
        if (pSecv4TaskletData->pu1OptBuf == NULL)
        {
            SECv4_TRC (SECv4_DATA_PATH,
                       "Secv4AhTransportEncode: Allocation for received Options in "
                       " Pkts failed\n");
            MemReleaseMemBlock (SECv4_TASKLET_MEMPOOl,
                                (UINT1 *) pSecv4TaskletData);
            return SEC_FAILURE;
        }

        if (IPSEC_COPY_FROM_BUF (pCBuf,
                                 (UINT1 *) pSecv4TaskletData->pu1OptBuf,
                                 SEC_IPV4_HEADER_SIZE, u4OptLen) == BUF_FAILURE)
        {
            Secv4ReleaseRcvdIPOpts (pSecv4TaskletData->pu1OptBuf);
            MemReleaseMemBlock (SECv4_TASKLET_MEMPOOl,
                                (UINT1 *) pSecv4TaskletData);
            return SEC_FAILURE;
        }
        /* mute the mutable fields in the options field */
        if (Secv4ProcessOptionFields
            (pCBuf, u4OptLen, pSecv4TaskletData->pu1OptBuf) != SEC_SUCCESS)
        {
            SECv4_TRC (SECv4_DATA_PATH,
                       "Secv4AhTransportEncode: Muting of IP Options Failed\n");
            Secv4ReleaseRcvdIPOpts (pSecv4TaskletData->pu1OptBuf);
            MemReleaseMemBlock (SECv4_TASKLET_MEMPOOl,
                                (UINT1 *) pSecv4TaskletData);
            return SEC_FAILURE;
        }
    }
    if (pu1Buf == NULL)
    {
        if (IPSEC_COPY_OVER_BUF (pCBuf, (UINT1 *) pIpHdr, 0,
                                 SEC_IPV4_HEADER_SIZE) == BUF_FAILURE)
        {
            if (pSecv4TaskletData->pu1OptBuf != NULL)
            {
                Secv4ReleaseRcvdIPOpts (pSecv4TaskletData->pu1OptBuf);
            }
            MemReleaseMemBlock (SECv4_TASKLET_MEMPOOl,
                                (UINT1 *) pSecv4TaskletData);
            return SEC_FAILURE;
        }
    }

    u4PktSize = IPSEC_BUF_GetValidBytes (pCBuf);
    if (u4PktSize == 0)
    {
        SECv4_TRC (SECv4_DATA_PATH,
                   "Secv4AhTransportEncode: Invalid packet size\n");
        if (pSecv4TaskletData->pu1OptBuf != NULL)
        {
            Secv4ReleaseRcvdIPOpts (pSecv4TaskletData->pu1OptBuf);
        }
        MemReleaseMemBlock (SECv4_TASKLET_MEMPOOl, (UINT1 *) pSecv4TaskletData);
        return SEC_FAILURE;
    }
    pSecv4TaskletData->pSaEntry = pSaEntry;
    pSecv4TaskletData->u4SecAssocIndex = pSaEntry->u4SecAssocIndex;

    pSecv4TaskletData->pCBuf = pCBuf;
    pSecv4TaskletData->u4AuthOffset = 0;
    pSecv4TaskletData->u4EncrOffset = 0;
    pSecv4TaskletData->u4AuthBufSize = u4PktSize;
    pSecv4TaskletData->u4EncrBufSize = 0;
    pSecv4TaskletData->u4IcvOffset = SEC_IPV4_HEADER_SIZE + u4OptLen +
        SEC_AUTH_HEADER_SIZE;
    pSecv4TaskletData->u4Mode = AUTH;
    pSecv4TaskletData->u4Action = SEC_ENCODE;
    pSecv4TaskletData->u4IfIndex = u4IfIndex;
    pSecv4TaskletData->u4BufSize = u4PktSize;

    if (Secv4AlgoProcessPkt (pSecv4TaskletData, AUTH,
                             SEC_ENCODE) == SEC_FAILURE)
    {
        if (pSecv4TaskletData->pu1OptBuf != NULL)
        {
            Secv4ReleaseRcvdIPOpts (pSecv4TaskletData->pu1OptBuf);
        }
        MemReleaseMemBlock (SECv4_TASKLET_MEMPOOl, (UINT1 *) pSecv4TaskletData);
        SECv4_TRC (SECv4_CONTROL_PLANE, "\nSecv4AhTransportEncode:-"
                   "Secv4AlgoProcessPkt failed \n");
        return SEC_FAILURE;
    }
    return SEC_SUCCESS;
}

/********************************************************************/
/*  Function Name : Secv4AhTransportDecode                          */
/*  Description   : This function gets the incomming packet and     */
/*                : does transport mode decode based on SA Entry    */
/*                :                                                 */
/*  Input(s)      : pCBuf    - buffer contains IP Secured packet    */
/*                : pSaEntry - Pointer to a SA Entry                */
/*                : u4ProcessedLen - Length of the Pkt so far       */
/*                : processed                                       */
/*                : u4IfIndex - The Interface index on which the pkt*/
/*                : arrived                                         */
/*  Output(s)     : pCBuf    - buffer contains Decoded IP packet    */
/*  Return Values : SEC_SUCCESS or SEC_FAILURE                      */
/*********************************************************************/

UINT1
Secv4AhTransportDecode (tIP_BUF_CHAIN_HEADER * pCBuf, tSecv4Assoc * pSaEntry,
                        UINT4 u4ProcessedLen, UINT4 u4IfIndex,
                        t_IP_HEADER * pIpHdr)
{
    tSecv4TaskletData  *pSecv4TaskletData = NULL;
    UINT1              *pu1Buf = NULL;
    UINT4               u4OptLen = 0;
    UINT4               u4PktSize = 0;
    UINT4               u4SeqNumber = 0;
    INT1                ai1IpAddr[100];
    IPADDR_HEX2STR (pIpHdr->u4Src, ai1IpAddr);

    if (pSaEntry->u1AntiReplayStatus == SEC_ANTI_REPLAY_ENABLE)
    {
        CRU_BUF_Copy_FromBufChain (pCBuf, (UINT1 *) &u4SeqNumber,
                                   (u4ProcessedLen + SEC_AUTH_SEQ_NUM_OFFSET),
                                   sizeof (UINT4));

        u4SeqNumber = IPSEC_NTOHL (u4SeqNumber);
        if (Secv4SeqNumberVerification (u4SeqNumber, pSaEntry) == SEC_FAILURE)
        {
            SECv4_TRC1 (SECv4_DATA_PATH,
                        "Secv4AhTransportDecode:\tReplay attack from %s\n",
                        ai1IpAddr);
            return SEC_FAILURE;
        }
    }
    pu1Buf = CRU_BUF_Get_DataPtr_IfLinear (pCBuf, 0, SEC_IPV4_HEADER_SIZE);

    if (MemAllocateMemBlock (SECv4_TASKLET_MEMPOOl,
                             (UINT1 **) (VOID *) &pSecv4TaskletData) ==
        MEM_FAILURE)
    {
        SECv4_TRC (SECv4_ALL_FAILURE,
                   "Secv4AhTransportDecode:- Failed to Allocating memblock for "
                   " Secv4LocalTaskletData \n");
        return SEC_FAILURE;
    }
    if (pSecv4TaskletData == NULL)
    {
        SECv4_TRC (SECv4_ALL_FAILURE,
                   "Secv4AhTransportDecode :- Failed to Allocating memblock for "
                   " Secv4LocalTaskletData \n");
        return SEC_FAILURE;
    }
    IPSEC_MEMSET (pSecv4TaskletData, 0, sizeof (tSecv4TaskletData));
    pSecv4TaskletData->u1Tos = pIpHdr->u1Tos;
    pSecv4TaskletData->u2Fl_offs = pIpHdr->u2Fl_offs;
    pSecv4TaskletData->u1Ttl = pIpHdr->u1Ttl;

    u4OptLen = (UINT4) IPSEC_OLEN (pIpHdr->u1Ver_hdrlen);

    /* IP Option Processing if there are options in the Rcvd Pkt */
    if (u4OptLen != 0)
    {
        pSecv4TaskletData->pu1OptBuf = Secv4AllocateRcvdIPOpts ();
        if (pSecv4TaskletData->pu1OptBuf == NULL)
        {
            SECv4_TRC (SECv4_DATA_PATH,
                       "Secv4AhTransportDecode: Allocating for Rcvd Options Failed \n");
            MemReleaseMemBlock (SECv4_TASKLET_MEMPOOl,
                                (UINT1 *) pSecv4TaskletData);
            return SEC_FAILURE;
        }
        if (IPSEC_COPY_FROM_BUF (pCBuf, (UINT1 *) pSecv4TaskletData->pu1OptBuf,
                                 u4ProcessedLen, u4OptLen) == BUF_FAILURE)
        {
            Secv4ReleaseRcvdIPOpts (pSecv4TaskletData->pu1OptBuf);
            MemReleaseMemBlock (SECv4_TASKLET_MEMPOOl,
                                (UINT1 *) pSecv4TaskletData);
            return SEC_FAILURE;
        }
        if (Secv4ProcessOptionFields
            (pCBuf, u4OptLen, pSecv4TaskletData->pu1OptBuf) != SEC_SUCCESS)
        {
            SECv4_TRC (SECv4_DATA_PATH,
                       "Secv4AhTransportDecode: Muting of IP Options Failed\n");
            Secv4ReleaseRcvdIPOpts (pSecv4TaskletData->pu1OptBuf);
            MemReleaseMemBlock (SECv4_TASKLET_MEMPOOl,
                                (UINT1 *) pSecv4TaskletData);
            return SEC_FAILURE;
        }

    }
    /* Mute all mutable fields in the IP header */
    pIpHdr->u1Tos = 0;
    pIpHdr->u2Fl_offs = 0;
    pIpHdr->u1Ttl = 0;
    pIpHdr->u2Cksum = 0;
    u4PktSize = IPSEC_BUF_GetValidBytes (pCBuf);
    if (u4PktSize == 0)
    {
        if (pSecv4TaskletData->pu1OptBuf != NULL)
        {
            Secv4ReleaseRcvdIPOpts (pSecv4TaskletData->pu1OptBuf);
        }
        MemReleaseMemBlock (SECv4_TASKLET_MEMPOOl, (UINT1 *) pSecv4TaskletData);
        SECv4_TRC (SECv4_DATA_PATH,
                   "Secv4AhTransportDecode: Invalid packet size\n");
        return SEC_FAILURE;
    }
    if (pu1Buf == NULL)
    {
        if (IPSEC_COPY_OVER_BUF (pCBuf, (UINT1 *) pIpHdr, 0,
                                 SEC_IPV4_HEADER_SIZE) == BUF_FAILURE)
        {
            if (pSecv4TaskletData->pu1OptBuf != NULL)
            {
                Secv4ReleaseRcvdIPOpts (pSecv4TaskletData->pu1OptBuf);
            }
            MemReleaseMemBlock (SECv4_TASKLET_MEMPOOl,
                                (UINT1 *) pSecv4TaskletData);
            return SEC_FAILURE;
        }
    }
    pSecv4TaskletData->pSaEntry = pSaEntry;
    pSecv4TaskletData->u4SecAssocIndex = pSaEntry->u4SecAssocIndex;

    pSecv4TaskletData->pCBuf = pCBuf;
    pSecv4TaskletData->u4AuthOffset = 0;
    pSecv4TaskletData->u4EncrOffset = 0;
    pSecv4TaskletData->u4AuthBufSize = u4PktSize;
    pSecv4TaskletData->u4EncrBufSize = 0;
    pSecv4TaskletData->u4IcvOffset = SEC_IPV4_HEADER_SIZE + u4OptLen +
        SEC_AUTH_HEADER_SIZE;
    pSecv4TaskletData->u4Mode = AUTH;
    pSecv4TaskletData->u4Action = SEC_DECODE;
    pSecv4TaskletData->u4IfIndex = u4IfIndex;
    pSecv4TaskletData->u4BufSize = u4PktSize;

    if (Secv4AlgoProcessPkt (pSecv4TaskletData, AUTH,
                             SEC_DECODE) == SEC_FAILURE)
    {
        SECv4_TRC (SECv4_CONTROL_PLANE, "\nSecv4AhTransportDecode:-"
                   "Secv4AlgoProcessPkt failed \n");
        if (pSecv4TaskletData->pu1OptBuf != NULL)
        {
            Secv4ReleaseRcvdIPOpts (pSecv4TaskletData->pu1OptBuf);
        }
        MemReleaseMemBlock (SECv4_TASKLET_MEMPOOl, (UINT1 *) pSecv4TaskletData);
        return SEC_FAILURE;
    }
    return SEC_SUCCESS;
}

/****************************************************************************/
/* Function Name : Secv4AhOutGoingPerformCB                                 */
/* Description   : This fuction handles all the outging Ah packets          */
/*               : after the algorithm processing by softwarwe or Hardare   */
/*               : accelerator                                              */
/* Input(s)      : pData -  Contains all the information needed to          */
/*               : process after the algorithm                              */
/* Output(s)     : None                                                     */
/* Return Values : None                                                     */
/****************************************************************************/
VOID
Secv4AhOutGoingPerformCB (VOID *pData)
{
    tSecv4TaskletData  *pTemp = NULL;

    pTemp = (tSecv4TaskletData *) pData;
    if (pTemp == NULL)
    {
        return;
    }

    if (pTemp->pSaEntry->u1SecAssocMode == SEC_TRANSPORT)
    {
        Secv4AhOutGoingTransport (pData);
    }
    else if (pTemp->pSaEntry->u1SecAssocMode == SEC_TUNNEL)
    {
        Secv4AhOutGoingTunnel (pData);
    }
    return;
}

/****************************************************************************/
/* Function Name : Secv4AhIncomingPerformCB                                 */
/* Description   : This fuction handles all the incoming Ah packets         */
/*               : after the algorithm processing by softwarwe or Hardare   */
/*               : accelerator                                              */
/* Input(s)      : pData -  Contains all the information needed to          */
/*               : process after the algorithm                              */
/* Output(s)     : None                                                     */
/* Return Values : None                                                     */
/****************************************************************************/
VOID
Secv4AhIncomingPerformCB (VOID *pData)
{
    tSecv4TaskletData  *pTemp = NULL;

    pTemp = (tSecv4TaskletData *) pData;

    if (pTemp->pSaEntry->u1SecAssocMode == SEC_TRANSPORT)
    {
        Secv4AhIncomingTransport (pData);
    }
    else
    {
        Secv4AhIncomingTunnel (pData);
    }
}
#endif
