/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: secv4conf.c,v 1.6 2013/10/24 09:56:37 siva Exp $
 *
 * Description:This file contains the configuration handler for the
 *  IPSEC  Module.
 *
 *******************************************************************/
#include "secv4com.h"
#include "secv4soft.h"

UINT4               gu4IkeTrigStrtTime = 0;
/************************************************************************/
/*  Function Name   : Secv4AssocVerify                                  */
/*  Description     : This function returns the Policy Flag  from the   */
/*                  : Policy List or SEC_FILTER using the Input         */
/*                  : Parameters                                        */
/*                  :                                                   */
/*  Input(s)        : u4SrcAddr  :Refers to Ipv4 src Addr               */
/*                  : u4DestAddr :Refers to Ipv4 Dest Addr              */
/*                  : u4Protocol :Refers to the Incoming Protocol       */
/*                  : u4Index    :Refers to the Index on which the      */
/*                  :             Packet                                */
/*                  : u4Port     :The Port no refers to the port no of  */
/*                  : applications of IP                                */
/*                  :                                                   */
/*  Output(s)       :                                                   */
/*  Returns         : SEC_FILTER or Policy Flag                         */
/************************************************************************/
INT4
Secv4AssocVerify (UINT4 u4SrcAddr, UINT4 u4DestAddr, UINT4 u4Protocol,
                  UINT4 u4Index, UINT2 u2SrcPort, UINT2 u2DstPort)
{
    tSecv4Selector     *pSelEntry = NULL;
    UINT4               u4AccessIndex = 0;
    UINT1               Flag = 0;
    UINT1               u1PktDirection = SEC_INBOUND;

    /* Get the Index of the Access Data Base Entry for which the
       the given SrcAddr and DestAddr fits in */

    u4AccessIndex = Secv4GetAccessIndex (u4SrcAddr, u4DestAddr,
                                         u2SrcPort, u2DstPort,
                                         (UINT2) u4Protocol);

    if (u4AccessIndex == SEC_FAILURE)
    {
        SECv4_TRC (SECv4_CONTROL_PLANE,
                   "Secv4AssocVerify: Access-List Look-Up Failed\n");
        return (SEC_FILTER);
    }

    pSelEntry =
        Secv4SelGetFormattedEntry (u4Index, u4Protocol,
                                   u4AccessIndex, u2DstPort, u1PktDirection);
    if (pSelEntry == NULL)
    {
        SECv4_TRC (SECv4_CONTROL_PLANE,
                   "Secv4AssocVerify: Formatted selector Look-Up Failed\n");
        return (SEC_FILTER);
    }

    if (pSelEntry->u1SelFilterFlag == SEC_FILTER)
    {
        SECv4_TRC (SECv4_CONTROL_PLANE,
                   "Secv4AssocVerify: Packet Filtered As Per Selector Data Base\n");
        return (SEC_FILTER);
    }

    /* Get the Policy Flag Form the Policy Data Base */
    Flag = pSelEntry->pPolicyEntry->u1PolicyFlag;

    return ((INT4) Flag);
}

/************************************************************************/
/*  Function Name   : Secv4AssocOutLookUp                               */
/*  Description     : This function returns the no of SA and the pointer*/
/*                  : to the SA Database                                */
/*                  :                                                   */
/*  Input(s)        : u4SrcAddr:Refers  to Src  Addr of the pkt         */
/*                  : u4DestAddr:Refers to Dest Addr of the pkt         */
/*                  : u2Protocol:Refers to the Protocol of the Packet   */
/*                  : u4Index   :Refers to the Index on which the PAcket*/
/*                  :            Arrives                                */
/*                  : pPolicy   :Pointers to the Policy  Entry          */
/*                  : u2SrcPort :Refers to the SrcPort                  */
/*                  : u2DstPort :Refers to the DstPort                  */
/*                  :                                                   */
/*  Output(s)       : Pointer to Policy Data Base                       */
/*  Returns         : Returns the no of SA's                            */
/************************************************************************/
INT1
Secv4AssocOutLookUp (UINT4 u4SrcAddr, UINT4 u4DestAddr, UINT4 u4Protocol,
                     UINT4 u4Index, tSecv4Policy ** pPolicy,
                     UINT2 u2SrcPort, UINT2 u2DstPort)
{
    tSecv4Selector     *pSelEntry = NULL;
    UINT1               u1PktDirection = SEC_OUTBOUND;
    UINT4               u4AccessIndex = SEC_FAILURE;
    UINT1               u1MatchIndex = SEC_FAILURE;
    UINT1               Flag = SEC_FAILURE;
#ifdef IKE_WANTED
    UINT4               u4IkeTrigCurrTime = 0;
    UINT4               u4Diff = 0;
#endif

    /* Bypass IKE packets */
#ifdef IKE_WANTED
    if ((u4Protocol == SEC_UDP) && ((u2DstPort == IKE_UDP_PORT) ||
                                    (u2DstPort == IKE_NATT_UDP_PORT)))
    {
        SECv4_TRC (SECv4_CONTROL_PLANE,
                   "Traffic  matched with IKE : Bypassed\n");
        return SEC_FAILURE;
    }
#endif

    /* Check whether the packet Src and Dest Addr  is within the range
       Configured in the Access List */
    u4AccessIndex = Secv4GetAccessIndex (u4SrcAddr, u4DestAddr,
                                         u2SrcPort, u2DstPort,
                                         (UINT2) u4Protocol);
    if (u4AccessIndex == SEC_FAILURE)
    {

        /* Incase of no sa's and 
           Check whether Dest Addr is matches with range between start & end VPN RA Address Pool's,
           if it matches then drops the packets */
        u1MatchIndex = VpnUtilRaAddrDbLookUp (u4DestAddr);

        if (u1MatchIndex == SEC_SUCCESS)
        {
            SECv4_TRC (SECv4_CONTROL_PLANE,
                       "Ra Dest Addr pool exist:\t Drop the packet\n");
            return SEC_REJECT;
        }
        else
        {
            SECv4_TRC (SECv4_CONTROL_PLANE,
                       "Access look-up:\t address out of range\n");
            return SEC_FAILURE;
        }
    }

    pSelEntry =
        Secv4SelGetFormattedEntry (u4Index, u4Protocol,
                                   u4AccessIndex, u2DstPort, u1PktDirection);
    if (pSelEntry == NULL)
    {
        SECv4_TRC (SECv4_CONTROL_PLANE, "Formatted selector look-up failed\n");
        return SEC_FAILURE;
    }

    if (pSelEntry->u1SelFilterFlag == SEC_FILTER)
    {
        SECv4_TRC (SECv4_CONTROL_PLANE,
                   "Secv4AssocOutLookUp: Packet Filtered As Per Selector "
                   "DataBase\n");
        return ((INT1) SEC_REJECT);
    }

    /* Get the Policy Flag from the Policy DataBase */

    if (pSelEntry->pPolicyEntry == NULL)
    {

        SECv4_TRC (SECv4_ALL_FAILURE, "Secv4AssocOutLookUp: No Policy Entry\n");
        return ((INT1) SEC_REJECT);
    }

    *pPolicy = pSelEntry->pPolicyEntry;
    Flag = pSelEntry->pPolicyEntry->u1PolicyFlag;
    if (Flag == SEC_BYPASS)
    {
        SECv4_TRC (SECv4_CONTROL_PLANE,
                   "Secv4AssocOutLookUp:  Packet Bypassed As Per Policy Data Base\n");
        return SEC_FAILURE;
    }

    /* Get the no of SecAssoc to be applied from the SecAssoc Bundle
       of the Policy DataBase */

    Flag = pSelEntry->pPolicyEntry->u1SaCount;

    /* Filter the packet if there are no SA's for applying security */
    if ((Flag == 0) && (pSelEntry->pPolicyEntry->u1PolicyMode == SEC_MANUAL))
    {
        SECv4_TRC (SECv4_CONTROL_PLANE,
                   "Secv4AssocOutLookUp: No SA's For Securing the Pkt\n");
        return ((INT1) SEC_REJECT);
    }
#ifdef IKE_WANTED
    else if ((Flag == 0)
             && (pSelEntry->pPolicyEntry->u1PolicyMode == SEC_AUTOMATIC))
    {

        if (pSelEntry->pPolicyEntry->u1ReqIkeFlag != SEC_NEW_KEY)
        {
            SECv4_TRC (SECv4_CONTROL_PLANE,
                       "Secv4AssocOutLookUp: Request Ike For Negotiating SA\n");
            pSelEntry->pPolicyEntry->u1ReqIkeFlag = SEC_NEW_KEY;
            Secv4RequeStIkeForNewOrReKey (u4Index, pSelEntry->u4ProtoId,
                                          u2DstPort, u4AccessIndex,
                                          SEC_OUTBOUND, SEC_NEW_KEY,
                                          pSelEntry->pPolicyEntry);
        }
        OsixGetSysTime (&u4IkeTrigCurrTime);
        if (u4IkeTrigCurrTime > gu4IkeTrigStrtTime)
        {
            u4Diff = u4IkeTrigCurrTime - gu4IkeTrigStrtTime;
            if ((u4Diff / SYS_NUM_OF_TIME_UNITS_IN_A_SEC) >=
                SEC_TRIGGER_IKE_TIME_INTERVAL)
            {
                pSelEntry->pPolicyEntry->u1ReqIkeFlag = SEC_RE_KEY;
                gu4IkeTrigStrtTime = u4IkeTrigCurrTime;
            }
        }
        return ((INT1) SEC_REJECT);
    }
#else
#endif
    return (INT1) (Flag);
}

/************************************************************************/
/*  Function Name   : Secv4GetSecAssocOverHead                          */
/*  Description     : This function returns the overhead added by IPSec */
/*                  :                                                   */
/*  Input(s)        : u4DestAddr:Refers to Ipv4 dest Addr               */
/*                  : u4SrcAddr :Refers to Ipv4 src  Addr               */
/*                  : u4Index:Refers to the InterfaceIndex              */
/*                  : u1Protocol Refers to the Protocol of IP Pkt       */
/*  Output(s)       :                                                   */
/*  Returns         : Returns OverHead  added by IPSec                  */
/************************************************************************/
UINT2
Secv4GetSecAssocOverHead (UINT4 u4SrcAddr, UINT4 u4DestAddr, UINT2 u2Index,
                          UINT1 u1Protocol)
{
    tSecv4Selector     *pSelEntry = NULL;
    tSecv4Assoc        *pSaEntry = NULL;
    tSecv4Policy       *pPolicyEntry = NULL;
    UINT4               u4AccessIndex;
    UINT2               u2Size = 0, u2Count = 0;

    if (gSecv4Status != SEC_ENABLE)
    {
        return (u2Size);
    }

    u4AccessIndex =
        Secv4GetAccessIndex (u4SrcAddr, u4DestAddr, 0, 0, u1Protocol);

    if (u4AccessIndex == SEC_FAILURE)
    {
        return (u2Size);
    }
    TMO_SLL_Scan (&Secv4SelFormattedList, pSelEntry, tSecv4Selector *)
    {
        if ((pSelEntry->u4IfIndex == (UINT4) u2Index) &&
            (pSelEntry->u4ProtoId == (UINT4) u1Protocol) &&
            (pSelEntry->u4SelAccessIndex == u4AccessIndex))
        {

            if (pSelEntry != NULL)
            {
                pPolicyEntry = pSelEntry->pPolicyEntry;
            }
            if ((pPolicyEntry != NULL) &&
                (pPolicyEntry->u1SaCount < SEC_MAX_BUNDLE_SA))
            {
                for (u2Count = 0; u2Count < pPolicyEntry->u1SaCount; u2Count++)
                {
                    pSaEntry = pPolicyEntry->pau1SaEntry[u2Count];
                    if (pSaEntry != NULL)
                    {
                        u2Size = (UINT2) (u2Size + pSaEntry->u2Size);
                    }
                }
            }
        }

    }

    return (u2Size);
}

/************************************************************************/
/*  Function Name   : Secv4GetDfBitStatus                               */
/*  Description     : This function returns the df bit status taken     */
/*                  : from the respective Df Bit database               */
/*                  :                                                   */
/*  Input(s)        : u4IfIndex:Refers to the InterfaceIndex            */
/*                  :                                                   */
/*  Output(s)       :                                                   */
/*  Returns         : Returns the configured info for DF Bit            */
/************************************************************************/
UINT1
Secv4GetDfBitStatus (UINT4 u4IfIndex)
{
    if (u4IfIndex >= SEC_MAX_STAT_COUNT)
    {
        SECv4_TRC (SECv4_CONTROL_PLANE,
                   "Secv4GetDfBitStatus: DF Bit Status Not Configured For Given Index\n");
        return SEC_INVALID_INTERFACE;
    }
    return (gatSecv4DFrag[u4IfIndex].u1DFragBitStatus);
}

/************************************************************************/
/*  Function Name   : Secv4CheckPolicySaBundle                          */
/*  Description     : This function checks whether corrcet set of SA's  */
/*                  : are applied for decoding the packet               */
/*                  :                                                   */
/*  Input(s)        : pPolicy      : Pointer to Policy Data Base        */
/*                  : pu4SaIndexes :Pointer to the Array of SA Indexes  */
/*                  : which are used forf decoding the packet           */
/*                  :                                                   */
/*  Output(s)       :                                                   */
/*  Returns         : Returns NULL on wrong SA's applied for decoding   */
/*                  : the packet                                        */
/************************************************************************/
/* This routine is not used in the code anywhere */
#if 0
UINT1
Secv4CheckPolicySaBundle (tSecv4Policy * pEntry, UINT4 *pu4SaIndexes)
{
    tSecv4Assoc        *pSecAssoc = NULL;
    UINT1               u1Count = 0;
    UINT1               u1Flag = SEC_FOUND;
    UINT4              *pu4TempSaIndexes = NULL;
    UINT1               u1MaxSaReKeyCount = 0;

    pu4TempSaIndexes = pu4SaIndexes;

    if (pEntry == NULL)
    {
        SECv4_TRC (SECv4_ALL_FAILURE,
                   "Secv4CheckPolicySaBundle: No Policy Entry \n");
        return ((UINT1) NULL);
    }

    /* Verify the SA's applied for decoding the Pkt are same as
       that in the Policy Data Base */
    while (((*pu4SaIndexes) != 0) && (u1Count < SEC_MAX_BUNDLE_SA))
    {
        pSecAssoc = pEntry->pau1SaEntry[u1Count];
        if (pSecAssoc == NULL)
        {
            u1Flag = SEC_NOT_FOUND;
            break;
        }
        if ((*pu4SaIndexes) != pSecAssoc->u4SecAssocIndex)
        {
            u1Flag = SEC_NOT_FOUND;
            break;
        }
        u1Count++;
        pu4SaIndexes++;
    }

    if (u1Flag == SEC_NOT_FOUND)
    {
        for (u1MaxSaReKeyCount = 0; u1MaxSaReKeyCount < SEC_MAX_SA_TO_POLICY;
             u1MaxSaReKeyCount++)
        {
            u1Count = 0;
            pSecAssoc = NULL;
            u1Flag = SEC_FOUND;
            pu4SaIndexes = pu4TempSaIndexes;
            while (((*pu4SaIndexes) != 0) && (u1Count < SEC_MAX_BUNDLE_SA))
            {
                pSecAssoc = pEntry->pau1NewSaEntry[u1MaxSaReKeyCount][u1Count];
                if (pSecAssoc == NULL)
                {
                    u1Flag = SEC_NOT_FOUND;
                    break;
                }
                if ((*pu4SaIndexes) != pSecAssoc->u4SecAssocIndex)
                {
                    u1Flag = SEC_NOT_FOUND;
                }
                u1Count++;
                pu4SaIndexes++;
            }
            if ((u1Count > 0) && (u1Flag == SEC_FOUND))
            {
                break;
            }
        }
    }
    if (u1Flag == SEC_NOT_FOUND)
    {
        SECv4_TRC (SECv4_CONTROL_PLANE, "Decoded With Wrong SA\n");
    }
    return (u1Count);
}
#endif

/*****************************************************************************/
/*  Function Name   : Secv4GetAccessIndex                                    */
/*  Description     : This function is used return the AccessIndex of the    */
/*                  : matching entry of the Access Data Base                 */
/*                  :                                                        */
/*                  :                                                        */
/*  Input(s)        : u4SrcAddr:Refers to the Source Address of the Pkt      */
/*                  : u4DestAddr:Refers to the Destination Address the Pkt   */
/*                  : u2SrcPort :Refers to the Src Port Present in the Pkt   */
/*                  : u2DstPort :Refers to the Dst Port Present in the Pkt   */
/*                  : u2Protocol: Refers to the Protocol Present in the Pkt  */
/*                  :                                                        */
/*  Output(s)       :                                                        */
/*  Returns         :Returns the Index Access Data Base Entry                */
/*****************************************************************************/
UINT4
Secv4GetAccessIndex (UINT4 u4SrcAddr, UINT4 u4DestAddr, UINT2 u2SrcPort,
                     UINT2 u2DstPort, UINT2 u2Protocol)
{

    tSecv4Access       *pEntry = NULL;

    TMO_SLL_Scan (&Secv4AccessList, pEntry, tSecv4Access *)
    {
        /*
         * Checking whether received u4SrcAddr and u4DestAddr is present
         * in the Entry List.
         * Checking whether u2SrcPort lies between u2LocalStartPort
         * and u2LocalEndPort.
         * Checking whether u2DstPort lies between u2RemoteStartPort
         * and u2RemoteEndPort.
         */
        if (((u4SrcAddr & pEntry->u4SrcMask) ==
             (pEntry->u4SrcAddress & pEntry->u4SrcMask)) &&
            ((u4DestAddr & pEntry->u4DestMask) ==
             (pEntry->u4DestAddress & pEntry->u4DestMask)) &&
            ((u2Protocol == pEntry->u4Protocol) ||
             (pEntry->u4Protocol == SEC_ANY_PROTOCOL)) &&
            (u2SrcPort >= pEntry->u2LocalStartPort) &&
            (u2SrcPort <= pEntry->u2LocalEndPort) &&
            (u2DstPort >= pEntry->u2RemoteStartPort) &&
            (u2DstPort <= pEntry->u2RemoteEndPort))
        {
            return (pEntry->u4GroupIndex);
        }
        else if (((pEntry->u4SrcAddress == 0) &&
                  (pEntry->u4SrcMask == 0xffffffff)) &&
                 ((pEntry->u4DestAddress == 0) &&
                  (pEntry->u4DestMask == 0xffffffff)))
        {
            return (pEntry->u4GroupIndex);
        }
    }
    return (SEC_FAILURE);
}

#ifdef NAT_WANTED
/*****************************************************************************/
/*  Function Name   : Secv4GetAccessIndexWithGlobalAddr                      */
/*  Description     : This function is used return the AccessIndex of the    */
/*                  : matching entry of the Access Data Base                 */
/*                  :                                                        */
/*                  :                                                        */
/*  Input(s)        : u4GlobalIpAddr:Refers to the Global Address of the Pkt */
/*                  : u2DestPort:Refers to the Global Port of the Pkt        */
/*                  : u4IpAddr:Refers to the dest addr for outbound direction*/
/*                  :          Refers to the Src addr for inbound direction  */
/*                  : u1Direction:Refers to the direction of the Pkt         */
/*                  :                                                        */
/*  Output(s)       :                                                        */
/*  Returns         : Returns the Index Access Data Base Entry               */
/*****************************************************************************/
UINT4
Secv4GetAccessIndexWithGlobalAddr (UINT4 u4GlobalIpAddr, UINT2 u2DestPort,
                                   UINT4 u4IpAddr, UINT1 u1Direction)
{
    tSecv4Access       *pEntry = NULL;
    tSecv4GlobalAddr   *pGlobal = NULL;

    TMO_SLL_Scan (&Secv4AccessList, pEntry, tSecv4Access *)
    {
        pGlobal = NULL;
        TMO_SLL_Scan (&pEntry->Secv4GlobalAddrList, pGlobal, tSecv4GlobalAddr *)
        {
            if ((u4GlobalIpAddr == pGlobal->u4GlobalIpAddr) &&
                (u2DestPort == pGlobal->u2DestPort))
            {
                break;
            }
        }
        if (pGlobal == NULL)
        {
            continue;
        }
        if (u1Direction == SEC_OUTBOUND)
        {
            if ((u4IpAddr & pEntry->u4DestMask) ==
                (pEntry->u4DestAddress & pEntry->u4DestMask))
            {
                return (pEntry->u4GroupIndex);
            }
        }
        else
        {
            if ((u4IpAddr & pEntry->u4SrcMask) ==
                (pEntry->u4SrcAddress & pEntry->u4SrcMask))
            {
                return (pEntry->u4GroupIndex);
            }
        }
    }
    return (SEC_FAILURE);
}
#endif
/************************************************************************************/
/*  Function Name : Secv4AllocateRcvdIPOpts                                         */
/*  Description   : This function Allocates memory for storing the Rcvd IP Options  */
/*  Input(s)      : None                                                            */
/*  Output(s)     : None                                                            */
/*  Return Values : pu1RcvdIpOpts                                                   */
/************************************************************************************/

UINT1              *
Secv4AllocateRcvdIPOpts (VOID)
{
    UINT1              *pu1RcvdIPOpts = NULL;

    if (MemAllocateMemBlock
        (SECv4_RCVD_IPOPTS_MEMPOOL, (UINT1 **) &pu1RcvdIPOpts) != MEM_SUCCESS)
    {
        SECv4_PRINT_MEM_ALLOC_FAILURE;
        return NULL;
    }

    IPSEC_MEMSET (pu1RcvdIPOpts, 0, SEC_IPV4_MAX_OPT_LEN);
    return (pu1RcvdIPOpts);
}

/***************************************************************************/
/*  Function Name : Secv4ReleaseRcvdIPOpts                                 */
/*  Description   : This function releases the memory used for the         */
/*                : the Rcvd IP Options                                    */
/*                :                                                        */
/*  Input(s)      : pu1RcvdIPOpts - Points to the Rcvd IP Options          */
/*                :                                                        */
/*  Output(s)     :                                                        */
/*  Return Values : None.                                                  */
/****************************************************************************/
VOID
Secv4ReleaseRcvdIPOpts (UINT1 *pu1RcvdIPOpts)
{

    if (pu1RcvdIPOpts != NULL)
    {
        if (MemReleaseMemBlock
            (SECv4_RCVD_IPOPTS_MEMPOOL, pu1RcvdIPOpts) != MEM_SUCCESS)
        {
            SECv4_TRC (SECv4_OS_RESOURCE,
                       "Secv4ReleaseRcvdIPOpts: Unable to Release Rcvd IP Options\n");
            return;
        }

    }
    return;
}
