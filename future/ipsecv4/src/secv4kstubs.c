/**********************************************************************
 * Copyright (C) 2011 Aricent Inc . All Rights Reserved
 *
 * $Id: secv4kstubs.c,v 1.1 2011/05/17 11:44:31 siva Exp $
 *
 * Description: This has stub functions for IPSECv4 module in kernel 
 ***********************************************************************/
#ifndef _SECV4KSTUBS_C_
#define _SECV4KSTUBS_C_

#include "secv4com.h"
#include "secv4soft.h"

/***********************************************************************/
/*  Function Name :  Secv4HwInitForKernel                              */
/*  Description   : This function gets the Initializes the list for    */
/*                : storing crypto requests in case of kernel tasklet  */
/*                :                                                    */
/*  Input(s)      : VOID - Input Nothing                               */
/*                :                                                    */
/*  Output(s)     : None                                               */
/*  Return        : None                                               */
/***********************************************************************/
VOID
Secv4HwInitForKernel (VOID)
{
    return;
}

VOID
Secv4TaskletSchedFnInKernel (tSecv4TaskletData * pSecv4TskletData)
{
    UNUSED_PARAM (pSecv4TskletData);
    return;
}

#endif
